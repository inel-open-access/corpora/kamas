<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDFA7C07D4-5DEB-F998-DEDA-B08463A35004">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_19700821_09339-2a.wav" />
         <referenced-file url="PKZ_19700821_09339-2a.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_19700821_09339-2a\PKZ_19700821_09339-2a.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1531</ud-information>
            <ud-information attribute-name="# HIAT:w">1033</ud-information>
            <ud-information attribute-name="# e">544</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">4</ud-information>
            <ud-information attribute-name="# HIAT:u">156</ud-information>
            <ud-information attribute-name="# sc">66</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KA">
            <abbreviation>KA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="1.08" type="appl" />
         <tli id="T1" time="10.09" type="appl" />
         <tli id="T2" time="16.2" type="appl" />
         <tli id="T3" time="18.939918686435654" />
         <tli id="T4" time="19.692" type="appl" />
         <tli id="T5" time="20.444" type="appl" />
         <tli id="T6" time="21.196" type="appl" />
         <tli id="T7" time="21.948" type="appl" />
         <tli id="T8" time="22.7" type="appl" />
         <tli id="T9" time="23.358" type="appl" />
         <tli id="T10" time="23.875" type="appl" />
         <tli id="T11" time="24.392" type="appl" />
         <tli id="T12" time="24.91" type="appl" />
         <tli id="T13" time="25.428" type="appl" />
         <tli id="T14" time="25.945" type="appl" />
         <tli id="T15" time="26.462" type="appl" />
         <tli id="T16" time="26.98" type="appl" />
         <tli id="T17" time="27.498" type="appl" />
         <tli id="T18" time="28.015" type="appl" />
         <tli id="T19" time="28.532" type="appl" />
         <tli id="T20" time="29.05" type="appl" />
         <tli id="T21" time="29.634" type="appl" />
         <tli id="T22" time="30.188" type="appl" />
         <tli id="T23" time="30.742" type="appl" />
         <tli id="T24" time="31.296" type="appl" />
         <tli id="T25" time="31.85" type="appl" />
         <tli id="T26" time="32.404" type="appl" />
         <tli id="T27" time="32.958" type="appl" />
         <tli id="T28" time="33.512" type="appl" />
         <tli id="T29" time="34.066" type="appl" />
         <tli id="T30" time="34.62" type="appl" />
         <tli id="T31" time="35.174" type="appl" />
         <tli id="T32" time="35.728" type="appl" />
         <tli id="T33" time="36.282" type="appl" />
         <tli id="T34" time="36.836" type="appl" />
         <tli id="T35" time="37.39" type="appl" />
         <tli id="T36" time="37.944" type="appl" />
         <tli id="T37" time="38.498" type="appl" />
         <tli id="T38" time="39.052" type="appl" />
         <tli id="T39" time="39.606" type="appl" />
         <tli id="T40" time="40.16" type="appl" />
         <tli id="T41" time="40.657" type="appl" />
         <tli id="T42" time="41.093" type="appl" />
         <tli id="T43" time="41.53" type="appl" />
         <tli id="T44" time="41.967" type="appl" />
         <tli id="T45" time="42.403" type="appl" />
         <tli id="T46" time="42.84" type="appl" />
         <tli id="T47" time="43.277" type="appl" />
         <tli id="T48" time="43.713" type="appl" />
         <tli id="T49" time="44.15" type="appl" />
         <tli id="T50" time="45.185" type="appl" />
         <tli id="T51" time="45.731" type="appl" />
         <tli id="T52" time="46.276" type="appl" />
         <tli id="T53" time="46.821" type="appl" />
         <tli id="T54" time="47.367" type="appl" />
         <tli id="T55" time="47.912" type="appl" />
         <tli id="T56" time="48.457" type="appl" />
         <tli id="T57" time="49.003" type="appl" />
         <tli id="T58" time="49.548" type="appl" />
         <tli id="T59" time="50.093" type="appl" />
         <tli id="T60" time="50.639" type="appl" />
         <tli id="T61" time="51.184" type="appl" />
         <tli id="T62" time="51.729" type="appl" />
         <tli id="T63" time="52.275" type="appl" />
         <tli id="T64" time="52.82" type="appl" />
         <tli id="T65" time="53.577" type="appl" />
         <tli id="T66" time="54.204" type="appl" />
         <tli id="T67" time="54.831" type="appl" />
         <tli id="T68" time="55.458" type="appl" />
         <tli id="T69" time="56.085" type="appl" />
         <tli id="T70" time="56.712" type="appl" />
         <tli id="T71" time="57.339" type="appl" />
         <tli id="T72" time="57.966" type="appl" />
         <tli id="T73" time="58.593" type="appl" />
         <tli id="T74" time="59.22" type="appl" />
         <tli id="T75" time="59.847" type="appl" />
         <tli id="T76" time="60.474" type="appl" />
         <tli id="T77" time="61.101" type="appl" />
         <tli id="T78" time="61.728" type="appl" />
         <tli id="T79" time="62.355" type="appl" />
         <tli id="T80" time="62.982" type="appl" />
         <tli id="T81" time="63.609" type="appl" />
         <tli id="T82" time="64.236" type="appl" />
         <tli id="T83" time="64.863" type="appl" />
         <tli id="T84" time="68.43303953405912" />
         <tli id="T85" time="69.338" type="appl" />
         <tli id="T86" time="70.247" type="appl" />
         <tli id="T87" time="71.155" type="appl" />
         <tli id="T88" time="72.063" type="appl" />
         <tli id="T89" time="72.972" type="appl" />
         <tli id="T90" time="73.88" type="appl" />
         <tli id="T91" time="74.852" type="appl" />
         <tli id="T92" time="75.824" type="appl" />
         <tli id="T93" time="76.796" type="appl" />
         <tli id="T94" time="77.768" type="appl" />
         <tli id="T95" time="78.74" type="appl" />
         <tli id="T96" time="79.765" type="appl" />
         <tli id="T97" time="80.56" type="appl" />
         <tli id="T98" time="81.355" type="appl" />
         <tli id="T99" time="82.15" type="appl" />
         <tli id="T100" time="82.945" type="appl" />
         <tli id="T101" time="83.74" type="appl" />
         <tli id="T102" time="84.535" type="appl" />
         <tli id="T103" time="85.33" type="appl" />
         <tli id="T104" time="86.125" type="appl" />
         <tli id="T105" time="86.92" type="appl" />
         <tli id="T106" time="87.715" type="appl" />
         <tli id="T107" time="88.61295289690344" />
         <tli id="T108" time="89.443" type="appl" />
         <tli id="T109" time="90.287" type="appl" />
         <tli id="T110" time="91.99960502386907" />
         <tli id="T111" time="92.942" type="appl" />
         <tli id="T112" time="93.743" type="appl" />
         <tli id="T113" time="94.545" type="appl" />
         <tli id="T114" time="95.347" type="appl" />
         <tli id="T115" time="96.148" type="appl" />
         <tli id="T116" time="96.95" type="appl" />
         <tli id="T117" time="97.743" type="appl" />
         <tli id="T118" time="98.536" type="appl" />
         <tli id="T119" time="99.329" type="appl" />
         <tli id="T120" time="100.121" type="appl" />
         <tli id="T121" time="100.914" type="appl" />
         <tli id="T122" time="101.707" type="appl" />
         <tli id="T123" time="103.412889357265" />
         <tli id="T124" time="104.76" type="appl" />
         <tli id="T125" time="121.37333308215236" />
         <tli id="T126" time="122.35999551284313" />
         <tli id="T127" time="122.612" type="appl" />
         <tli id="T128" time="123.003" type="appl" />
         <tli id="T129" time="123.395" type="appl" />
         <tli id="T130" time="123.787" type="appl" />
         <tli id="T131" time="124.178" type="appl" />
         <tli id="T132" time="124.49279885621237" />
         <tli id="T133" time="124.912" type="appl" />
         <tli id="T134" time="125.254" type="appl" />
         <tli id="T135" time="125.596" type="appl" />
         <tli id="T136" time="125.938" type="appl" />
         <tli id="T137" time="126.28" type="appl" />
         <tli id="T138" time="126.622" type="appl" />
         <tli id="T139" time="126.964" type="appl" />
         <tli id="T140" time="127.306" type="appl" />
         <tli id="T141" time="127.648" type="appl" />
         <tli id="T142" time="128.02611702017256" />
         <tli id="T143" time="128.85278013777835" />
         <tli id="T144" time="129.1594454878579" />
         <tli id="T145" time="129.35277799116892" />
         <tli id="T146" time="129.5727770466608" />
         <tli id="T147" time="129.73277635974577" />
         <tli id="T148" time="130.902" type="appl" />
         <tli id="T149" time="131.33766530291015" />
         <tli id="T150" time="131.83332723741586" />
         <tli id="T151" time="131.83417358794892" />
         <tli id="T152" time="131.84834019379502" />
         <tli id="T153" time="131.86250679964107" />
         <tli id="T154" time="132.23000522188318" />
         <tli id="T155" time="132.67" type="appl" />
         <tli id="T156" time="133.07" type="appl" />
         <tli id="T157" time="133.18166666666667" type="intp" />
         <tli id="T158" time="133.29333333333335" type="intp" />
         <tli id="T159" time="133.405" type="appl" />
         <tli id="T160" time="133.81" type="appl" />
         <tli id="T161" time="134.17" type="appl" />
         <tli id="T162" time="134.58" type="appl" />
         <tli id="T163" time="134.9" type="appl" />
         <tli id="T164" time="135.29275248944919" />
         <tli id="T165" time="135.54" type="appl" />
         <tli id="T166" time="135.92666903872836" />
         <tli id="T167" time="136.005" type="appl" />
         <tli id="T168" time="136.68" type="appl" />
         <tli id="T169" time="137.30666311408643" />
         <tli id="T170" time="137.648" type="appl" />
         <tli id="T171" time="138.075" type="appl" />
         <tli id="T172" time="138.5219964380388" />
         <tli id="T173" time="138.93" type="appl" />
         <tli id="T174" time="139.4" type="appl" />
         <tli id="T175" time="140.22" type="appl" />
         <tli id="T176" time="141.04" type="appl" />
         <tli id="T177" time="141.7800032839758" />
         <tli id="T178" time="141.995" type="appl" />
         <tli id="T179" time="142.29" type="appl" />
         <tli id="T180" time="142.585" type="appl" />
         <tli id="T181" time="142.88" type="appl" />
         <tli id="T182" time="143.175" type="appl" />
         <tli id="T183" time="143.47" type="appl" />
         <tli id="T184" time="143.765" type="appl" />
         <tli id="T185" time="144.06" type="appl" />
         <tli id="T186" time="144.708" type="appl" />
         <tli id="T187" time="145.007" type="appl" />
         <tli id="T188" time="145.305" type="appl" />
         <tli id="T189" time="145.603" type="appl" />
         <tli id="T190" time="145.902" type="appl" />
         <tli id="T191" time="146.2" type="appl" />
         <tli id="T192" time="146.292" type="appl" />
         <tli id="T193" time="146.974" type="appl" />
         <tli id="T194" time="147.656" type="appl" />
         <tli id="T195" time="148.338" type="appl" />
         <tli id="T196" time="149.02" type="appl" />
         <tli id="T197" time="149.387" type="appl" />
         <tli id="T198" time="149.784" type="appl" />
         <tli id="T199" time="150.181" type="appl" />
         <tli id="T200" time="150.578" type="appl" />
         <tli id="T201" time="150.975" type="appl" />
         <tli id="T202" time="151.372" type="appl" />
         <tli id="T203" time="151.769" type="appl" />
         <tli id="T204" time="152.166" type="appl" />
         <tli id="T205" time="152.563" type="appl" />
         <tli id="T206" time="152.97999426225735" />
         <tli id="T207" time="153.475" type="appl" />
         <tli id="T208" time="153.98" type="appl" />
         <tli id="T209" time="154.485" type="appl" />
         <tli id="T210" time="154.99" type="appl" />
         <tli id="T211" time="155.495" type="appl" />
         <tli id="T212" time="156.2059960372664" />
         <tli id="T213" time="157.315" type="appl" />
         <tli id="T214" time="159.71264764904575" />
         <tli id="T215" time="160.067" type="appl" />
         <tli id="T216" time="160.453" type="appl" />
         <tli id="T217" time="160.84" type="appl" />
         <tli id="T218" time="161.227" type="appl" />
         <tli id="T219" time="161.23930776139835" />
         <tli id="T220" time="162.0" type="appl" />
         <tli id="T221" time="162.065" type="intp" />
         <tli id="T222" time="162.13" type="intp" />
         <tli id="T223" time="162.195" type="appl" />
         <tli id="T224" time="162.497" type="appl" />
         <tli id="T225" time="162.798" type="appl" />
         <tli id="T226" time="163.1" type="appl" />
         <tli id="T227" time="163.402" type="appl" />
         <tli id="T228" time="163.703" type="appl" />
         <tli id="T229" time="164.005" type="appl" />
         <tli id="T230" time="164.307" type="appl" />
         <tli id="T231" time="164.608" type="appl" />
         <tli id="T232" time="164.79666228049808" />
         <tli id="T233" time="165.055" type="appl" />
         <tli id="T234" time="165.39" type="appl" />
         <tli id="T235" time="165.725" type="appl" />
         <tli id="T236" time="166.08000312497805" />
         <tli id="T237" time="166.505" type="appl" />
         <tli id="T238" time="166.95" type="appl" />
         <tli id="T239" time="167.222" type="appl" />
         <tli id="T240" time="167.494" type="appl" />
         <tli id="T241" time="167.766" type="appl" />
         <tli id="T242" time="168.038" type="appl" />
         <tli id="T243" time="168.3299934652357" />
         <tli id="T244" time="168.664" type="appl" />
         <tli id="T245" time="169.018" type="appl" />
         <tli id="T246" time="169.372" type="appl" />
         <tli id="T247" time="169.726" type="appl" />
         <tli id="T248" time="170.52593455837294" />
         <tli id="T249" time="171.276" type="appl" />
         <tli id="T250" time="171.601" type="appl" />
         <tli id="T251" time="171.927" type="appl" />
         <tli id="T252" time="172.253" type="appl" />
         <tli id="T253" time="172.579" type="appl" />
         <tli id="T254" time="172.904" type="appl" />
         <tli id="T255" time="173.23" type="appl" />
         <tli id="T256" time="173.556" type="appl" />
         <tli id="T257" time="173.881" type="appl" />
         <tli id="T258" time="174.207" type="appl" />
         <tli id="T259" time="174.533" type="appl" />
         <tli id="T260" time="174.859" type="appl" />
         <tli id="T261" time="175.184" type="appl" />
         <tli id="T262" time="175.51" type="appl" />
         <tli id="T263" time="175.967" type="appl" />
         <tli id="T264" time="176.363" type="appl" />
         <tli id="T265" time="176.76" type="appl" />
         <tli id="T266" time="177.157" type="appl" />
         <tli id="T267" time="177.553" type="appl" />
         <tli id="T268" time="177.95" type="appl" />
         <tli id="T269" time="178.628" type="appl" />
         <tli id="T270" time="179.255" type="appl" />
         <tli id="T271" time="179.882" type="appl" />
         <tli id="T272" time="180.51" type="appl" />
         <tli id="T273" time="181.765" type="appl" />
         <tli id="T274" time="182.93" type="appl" />
         <tli id="T275" time="183.622" type="appl" />
         <tli id="T276" time="184.244" type="appl" />
         <tli id="T277" time="184.866" type="appl" />
         <tli id="T278" time="185.488" type="appl" />
         <tli id="T279" time="186.11" type="appl" />
         <tli id="T280" time="186.732" type="appl" />
         <tli id="T281" time="187.354" type="appl" />
         <tli id="T282" time="187.976" type="appl" />
         <tli id="T283" time="188.598" type="appl" />
         <tli id="T284" time="189.3858535882661" />
         <tli id="T285" time="189.992" type="appl" />
         <tli id="T286" time="190.415" type="appl" />
         <tli id="T287" time="190.837" type="appl" />
         <tli id="T288" time="191.26" type="appl" />
         <tli id="T289" time="191.682" type="appl" />
         <tli id="T290" time="192.104" type="appl" />
         <tli id="T291" time="192.527" type="appl" />
         <tli id="T292" time="192.949" type="appl" />
         <tli id="T293" time="193.372" type="appl" />
         <tli id="T294" time="193.794" type="appl" />
         <tli id="T295" time="194.217" type="appl" />
         <tli id="T296" time="196.8791547510798" />
         <tli id="T297" time="197.422" type="appl" />
         <tli id="T298" time="197.824" type="appl" />
         <tli id="T299" time="198.226" type="appl" />
         <tli id="T300" time="198.628" type="appl" />
         <tli id="T301" time="198.91667204520132" />
         <tli id="T302" time="199.495" type="appl" />
         <tli id="T303" time="199.941" type="appl" />
         <tli id="T304" time="200.386" type="appl" />
         <tli id="T305" time="200.832" type="appl" />
         <tli id="T306" time="201.3658021555047" />
         <tli id="T307" time="201.978" type="appl" />
         <tli id="T308" time="202.525" type="appl" />
         <tli id="T309" time="203.072" type="appl" />
         <tli id="T310" time="203.62" type="appl" />
         <tli id="T311" time="204.016" type="appl" />
         <tli id="T312" time="204.411" type="appl" />
         <tli id="T313" time="204.807" type="appl" />
         <tli id="T314" time="205.203" type="appl" />
         <tli id="T315" time="205.599" type="appl" />
         <tli id="T316" time="205.994" type="appl" />
         <tli id="T317" time="206.39" type="appl" />
         <tli id="T318" time="207.015" type="appl" />
         <tli id="T319" time="207.58" type="appl" />
         <tli id="T320" time="208.145" type="appl" />
         <tli id="T321" time="208.71" type="appl" />
         <tli id="T322" time="209.274" type="appl" />
         <tli id="T323" time="209.839" type="appl" />
         <tli id="T324" time="210.404" type="appl" />
         <tli id="T325" time="210.969" type="appl" />
         <tli id="T326" time="211.534" type="appl" />
         <tli id="T327" time="212.33908837791694" />
         <tli id="T328" time="214.21241366862034" />
         <tli id="T329" time="216.65" type="appl" />
         <tli id="T330" time="217.01" type="appl" />
         <tli id="T331" time="217.37" type="appl" />
         <tli id="T332" time="217.73" type="appl" />
         <tli id="T333" time="218.09" type="appl" />
         <tli id="T334" time="218.45" type="appl" />
         <tli id="T335" time="218.81" type="appl" />
         <tli id="T336" time="219.17" type="appl" />
         <tli id="T337" time="219.53" type="appl" />
         <tli id="T338" time="219.89" type="appl" />
         <tli id="T339" time="220.25" type="appl" />
         <tli id="T340" time="220.61" type="appl" />
         <tli id="T341" time="220.97" type="appl" />
         <tli id="T342" time="221.33" type="appl" />
         <tli id="T343" time="221.69" type="appl" />
         <tli id="T344" time="222.05" type="appl" />
         <tli id="T345" time="222.41" type="appl" />
         <tli id="T346" time="222.77" type="appl" />
         <tli id="T347" time="223.37" type="appl" />
         <tli id="T348" time="223.97" type="appl" />
         <tli id="T349" time="224.57" type="appl" />
         <tli id="T350" time="225.17" type="appl" />
         <tli id="T351" time="225.816" type="appl" />
         <tli id="T352" time="226.342" type="appl" />
         <tli id="T353" time="226.868" type="appl" />
         <tli id="T354" time="227.394" type="appl" />
         <tli id="T355" time="228.48568572341046" />
         <tli id="T356" time="229.408" type="appl" />
         <tli id="T357" time="229.956" type="appl" />
         <tli id="T358" time="230.504" type="appl" />
         <tli id="T359" time="231.052" type="appl" />
         <tli id="T360" time="234.01899530093303" />
         <tli id="T361" time="235.006" type="appl" />
         <tli id="T362" time="235.401" type="appl" />
         <tli id="T363" time="235.797" type="appl" />
         <tli id="T364" time="236.193" type="appl" />
         <tli id="T365" time="236.5256512059312" />
         <tli id="T366" time="236.984" type="appl" />
         <tli id="T367" time="237.39231415180822" />
         <tli id="T368" time="239.28563935664727" />
         <tli id="T369" time="240.282" type="appl" />
         <tli id="T370" time="241.094" type="appl" />
         <tli id="T371" time="241.907" type="appl" />
         <tli id="T372" time="242.719" type="appl" />
         <tli id="T373" time="243.531" type="appl" />
         <tli id="T374" time="244.343" type="appl" />
         <tli id="T375" time="245.156" type="appl" />
         <tli id="T376" time="245.968" type="appl" />
         <tli id="T377" time="246.78" type="appl" />
         <tli id="T378" time="247.207" type="appl" />
         <tli id="T379" time="247.603" type="appl" />
         <tli id="T380" time="248.0" type="appl" />
         <tli id="T381" time="248.792" type="appl" />
         <tli id="T382" time="249.585" type="appl" />
         <tli id="T383" time="250.378" type="appl" />
         <tli id="T384" time="251.64558629246272" />
         <tli id="T385" time="252.532" type="appl" />
         <tli id="T386" time="253.294" type="appl" />
         <tli id="T387" time="254.056" type="appl" />
         <tli id="T388" time="254.818" type="appl" />
         <tli id="T389" time="255.58" type="appl" />
         <tli id="T390" time="256.05" type="appl" />
         <tli id="T391" time="256.5" type="appl" />
         <tli id="T392" time="256.95" type="appl" />
         <tli id="T393" time="257.4" type="appl" />
         <tli id="T394" time="257.85" type="appl" />
         <tli id="T395" time="258.3" type="appl" />
         <tli id="T396" time="258.75" type="appl" />
         <tli id="T397" time="259.2" type="appl" />
         <tli id="T398" time="260.7322139480811" />
         <tli id="T399" time="261.688" type="appl" />
         <tli id="T400" time="262.496" type="appl" />
         <tli id="T401" time="263.304" type="appl" />
         <tli id="T402" time="264.112" type="appl" />
         <tli id="T403" time="265.1121951437826" />
         <tli id="T404" time="266.872" type="appl" />
         <tli id="T405" time="268.375" type="appl" />
         <tli id="T406" time="269.878" type="appl" />
         <tli id="T407" time="271.7388333607193" />
         <tli id="T408" time="273.11216079803216" />
         <tli id="T409" time="273.934" type="appl" />
         <tli id="T410" time="274.678" type="appl" />
         <tli id="T411" time="275.422" type="appl" />
         <tli id="T412" time="276.166" type="appl" />
         <tli id="T413" time="279.02546874413156" />
         <tli id="T414" time="279.904" type="appl" />
         <tli id="T415" time="280.799" type="appl" />
         <tli id="T416" time="281.693" type="appl" />
         <tli id="T417" time="282.587" type="appl" />
         <tli id="T418" time="283.481" type="appl" />
         <tli id="T419" time="284.376" type="appl" />
         <tli id="T420" time="285.27" type="appl" />
         <tli id="T421" time="286.164" type="appl" />
         <tli id="T422" time="287.059" type="appl" />
         <tli id="T423" time="287.953" type="appl" />
         <tli id="T424" time="288.847" type="appl" />
         <tli id="T425" time="289.742" type="appl" />
         <tli id="T426" time="290.636" type="appl" />
         <tli id="T427" time="291.53" type="appl" />
         <tli id="T428" time="292.424" type="appl" />
         <tli id="T429" time="293.319" type="appl" />
         <tli id="T430" time="294.27967408754233" />
         <tli id="T431" time="295.674" type="appl" />
         <tli id="T432" time="297.009" type="appl" />
         <tli id="T433" time="298.343" type="appl" />
         <tli id="T434" time="299.677" type="appl" />
         <tli id="T435" time="301.012" type="appl" />
         <tli id="T436" time="302.57870095785114" />
         <tli id="T437" time="303.258" type="appl" />
         <tli id="T438" time="303.963" type="appl" />
         <tli id="T439" time="304.668" type="appl" />
         <tli id="T440" time="305.373" type="appl" />
         <tli id="T441" time="306.078" type="appl" />
         <tli id="T442" time="306.783" type="appl" />
         <tli id="T443" time="307.488" type="appl" />
         <tli id="T444" time="308.5520086463574" />
         <tli id="T445" time="309.462" type="appl" />
         <tli id="T446" time="310.152" type="appl" />
         <tli id="T447" time="310.843" type="appl" />
         <tli id="T448" time="311.533" type="appl" />
         <tli id="T449" time="312.223" type="appl" />
         <tli id="T450" time="312.913" type="appl" />
         <tli id="T451" time="313.603" type="appl" />
         <tli id="T452" time="314.294" type="appl" />
         <tli id="T453" time="314.984" type="appl" />
         <tli id="T454" time="316.6119740430137" />
         <tli id="T455" time="317.723" type="appl" />
         <tli id="T456" time="318.913" type="appl" />
         <tli id="T457" time="320.103" type="appl" />
         <tli id="T458" time="321.293" type="appl" />
         <tli id="T459" time="322.484" type="appl" />
         <tli id="T460" time="323.674" type="appl" />
         <tli id="T461" time="324.864" type="appl" />
         <tli id="T462" time="326.054" type="appl" />
         <tli id="T463" time="328.1452578612234" />
         <tli id="T464" time="329.685" type="appl" />
         <tli id="T465" time="331.277" type="appl" />
         <tli id="T466" time="332.869" type="appl" />
         <tli id="T467" time="334.461" type="appl" />
         <tli id="T468" time="336.053" type="appl" />
         <tli id="T469" time="337.346" type="appl" />
         <tli id="T470" time="338.586" type="appl" />
         <tli id="T471" time="341.89186551044213" />
         <tli id="T472" time="342.852" type="appl" />
         <tli id="T473" time="343.772" type="appl" />
         <tli id="T474" time="344.93851909710213" />
         <tli id="T475" time="346.086" type="appl" />
         <tli id="T476" time="346.987" type="appl" />
         <tli id="T477" time="347.889" type="appl" />
         <tli id="T478" time="348.79" type="appl" />
         <tli id="T479" time="350.0518304777766" />
         <tli id="T480" time="351.017" type="appl" />
         <tli id="T481" time="352.05" type="appl" />
         <tli id="T482" time="353.082" type="appl" />
         <tli id="T483" time="354.114" type="appl" />
         <tli id="T484" time="355.146" type="appl" />
         <tli id="T485" time="356.179" type="appl" />
         <tli id="T486" time="357.3851323275053" />
         <tli id="T487" time="358.282" type="appl" />
         <tli id="T488" time="358.978" type="appl" />
         <tli id="T489" time="359.674" type="appl" />
         <tli id="T490" time="360.371" type="appl" />
         <tli id="T491" time="361.286" type="appl" />
         <tli id="T492" time="362.25177810050707" />
         <tli id="T493" time="363.033" type="appl" />
         <tli id="T494" time="363.773" type="appl" />
         <tli id="T495" time="364.513" type="appl" />
         <tli id="T496" time="366.52509308748535" />
         <tli id="T497" time="367.612" type="appl" />
         <tli id="T498" time="368.772" type="appl" />
         <tli id="T499" time="369.7784124535467" />
         <tli id="T500" time="371.19173971913085" />
         <tli id="T501" time="373.41839682623026" />
         <tli id="T502" time="373.813" type="appl" />
         <tli id="T503" time="374.242" type="appl" />
         <tli id="T504" time="374.67" type="appl" />
         <tli id="T505" time="375.098" type="appl" />
         <tli id="T506" time="375.527" type="appl" />
         <tli id="T507" time="375.955" type="appl" />
         <tli id="T508" time="376.384" type="appl" />
         <tli id="T509" time="376.812" type="appl" />
         <tli id="T510" time="377.33" type="appl" />
         <tli id="T511" time="377.809" type="appl" />
         <tli id="T512" time="378.287" type="appl" />
         <tli id="T513" time="378.766" type="appl" />
         <tli id="T514" time="379.244" type="appl" />
         <tli id="T515" time="379.722" type="appl" />
         <tli id="T516" time="380.201" type="appl" />
         <tli id="T517" time="381.07836394017414" />
         <tli id="T518" time="381.379" type="appl" />
         <tli id="T519" time="381.773" type="appl" />
         <tli id="T520" time="382.166" type="appl" />
         <tli id="T521" time="382.559" type="appl" />
         <tli id="T522" time="382.952" type="appl" />
         <tli id="T523" time="383.346" type="appl" />
         <tli id="T524" time="383.739" type="appl" />
         <tli id="T525" time="384.132" type="appl" />
         <tli id="T526" time="384.526" type="appl" />
         <tli id="T527" time="385.03168030098243" />
         <tli id="T528" time="385.468" type="appl" />
         <tli id="T529" time="385.843" type="appl" />
         <tli id="T530" time="386.219" type="appl" />
         <tli id="T531" time="386.594" type="appl" />
         <tli id="T532" time="386.97" type="appl" />
         <tli id="T533" time="387.346" type="appl" />
         <tli id="T534" time="387.721" type="appl" />
         <tli id="T535" time="388.097" type="appl" />
         <tli id="T536" time="388.472" type="appl" />
         <tli id="T537" time="388.848" type="appl" />
         <tli id="T538" time="389.223" type="appl" />
         <tli id="T539" time="389.9256697065915" />
         <tli id="T540" time="390.386" type="appl" />
         <tli id="T541" time="390.799" type="appl" />
         <tli id="T542" time="391.212" type="appl" />
         <tli id="T543" time="391.5460012917903" />
         <tli id="T544" time="391.819" type="appl" />
         <tli id="T545" time="391.999" type="appl" />
         <tli id="T546" time="392.179" type="appl" />
         <tli id="T547" time="392.359" type="appl" />
         <tli id="T548" time="392.539" type="appl" />
         <tli id="T549" time="392.8056573421213" />
         <tli id="T550" time="392.981" type="appl" />
         <tli id="T551" time="393.243" type="appl" />
         <tli id="T552" time="393.505" type="appl" />
         <tli id="T553" time="393.768" type="appl" />
         <tli id="T554" time="394.03" type="appl" />
         <tli id="T555" time="394.292" type="appl" />
         <tli id="T556" time="394.554" type="appl" />
         <tli id="T557" time="394.816" type="appl" />
         <tli id="T558" time="395.078" type="appl" />
         <tli id="T559" time="395.34" type="appl" />
         <tli id="T560" time="395.602" type="appl" />
         <tli id="T561" time="395.864" type="appl" />
         <tli id="T562" time="396.127" type="appl" />
         <tli id="T563" time="396.389" type="appl" />
         <tli id="T564" time="396.651" type="appl" />
         <tli id="T565" time="396.913" type="appl" />
         <tli id="T566" time="397.675" type="appl" />
         <tli id="T567" time="398.257" type="appl" />
         <tli id="T568" time="398.84" type="appl" />
         <tli id="T569" time="399.422" type="appl" />
         <tli id="T570" time="400.004" type="appl" />
         <tli id="T571" time="400.586" type="appl" />
         <tli id="T572" time="401.169" type="appl" />
         <tli id="T573" time="401.751" type="appl" />
         <tli id="T574" time="402.333" type="appl" />
         <tli id="T575" time="403.112" type="appl" />
         <tli id="T576" time="403.666" type="appl" />
         <tli id="T577" time="404.402" type="appl" />
         <tli id="T578" time="405.097" type="appl" />
         <tli id="T579" time="405.793" type="appl" />
         <tli id="T580" time="406.489" type="appl" />
         <tli id="T581" time="407.185" type="appl" />
         <tli id="T582" time="407.88" type="appl" />
         <tli id="T583" time="408.73824518974175" />
         <tli id="T584" time="409.441" type="appl" />
         <tli id="T585" time="410.03" type="appl" />
         <tli id="T586" time="410.343" type="appl" />
         <tli id="T587" time="410.647" type="appl" />
         <tli id="T588" time="410.951" type="appl" />
         <tli id="T589" time="411.255" type="appl" />
         <tli id="T590" time="411.559" type="appl" />
         <tli id="T591" time="411.864" type="appl" />
         <tli id="T592" time="412.168" type="appl" />
         <tli id="T593" time="412.472" type="appl" />
         <tli id="T594" time="412.776" type="appl" />
         <tli id="T595" time="413.08" type="appl" />
         <tli id="T596" time="413.384" type="appl" />
         <tli id="T597" time="413.688" type="appl" />
         <tli id="T598" time="414.3" type="appl" />
         <tli id="T599" time="414.786" type="appl" />
         <tli id="T600" time="415.273" type="appl" />
         <tli id="T601" time="415.76" type="appl" />
         <tli id="T602" time="416.246" type="appl" />
         <tli id="T603" time="416.733" type="appl" />
         <tli id="T604" time="417.294" type="appl" />
         <tli id="T605" time="417.736" type="appl" />
         <tli id="T606" time="418.177" type="appl" />
         <tli id="T607" time="418.618" type="appl" />
         <tli id="T608" time="419.06" type="appl" />
         <tli id="T609" time="419.501" type="appl" />
         <tli id="T610" time="420.341" type="appl" />
         <tli id="T611" time="420.908" type="appl" />
         <tli id="T612" time="421.476" type="appl" />
         <tli id="T613" time="422.043" type="appl" />
         <tli id="T614" time="422.611" type="appl" />
         <tli id="T615" time="423.178" type="appl" />
         <tli id="T616" time="424.11817916003633" />
         <tli id="T617" time="424.748" type="appl" />
         <tli id="T618" time="425.203" type="appl" />
         <tli id="T619" time="425.658" type="appl" />
         <tli id="T620" time="426.113" type="appl" />
         <tli id="T621" time="426.568" type="appl" />
         <tli id="T622" time="427.023" type="appl" />
         <tli id="T623" time="427.478" type="appl" />
         <tli id="T624" time="428.0848287969351" />
         <tli id="T625" time="428.64" type="appl" />
         <tli id="T626" time="429.2" type="appl" />
         <tli id="T627" time="429.76" type="appl" />
         <tli id="T628" time="430.32" type="appl" />
         <tli id="T629" time="430.88" type="appl" />
         <tli id="T630" time="431.196" type="appl" />
         <tli id="T631" time="431.513" type="appl" />
         <tli id="T632" time="431.83" type="appl" />
         <tli id="T633" time="432.146" type="appl" />
         <tli id="T634" time="432.682" type="appl" />
         <tli id="T635" time="433.204" type="appl" />
         <tli id="T636" time="433.726" type="appl" />
         <tli id="T637" time="434.248" type="appl" />
         <tli id="T638" time="434.77" type="appl" />
         <tli id="T639" time="435.292" type="appl" />
         <tli id="T640" time="435.814" type="appl" />
         <tli id="T641" time="436.335" type="appl" />
         <tli id="T642" time="436.857" type="appl" />
         <tli id="T643" time="437.379" type="appl" />
         <tli id="T644" time="437.901" type="appl" />
         <tli id="T645" time="438.423" type="appl" />
         <tli id="T646" time="438.945" type="appl" />
         <tli id="T647" time="440.13811038267096" />
         <tli id="T648" time="441.005" type="appl" />
         <tli id="T649" time="441.664" type="appl" />
         <tli id="T650" time="442.323" type="appl" />
         <tli id="T651" time="442.983" type="appl" />
         <tli id="T652" time="443.642" type="appl" />
         <tli id="T653" time="444.301" type="appl" />
         <tli id="T654" time="444.96" type="appl" />
         <tli id="T655" time="445.572" type="appl" />
         <tli id="T656" time="445.985" type="appl" />
         <tli id="T657" time="446.397" type="appl" />
         <tli id="T658" time="446.809" type="appl" />
         <tli id="T659" time="447.222" type="appl" />
         <tli id="T660" time="447.634" type="appl" />
         <tli id="T661" time="448.046" type="appl" />
         <tli id="T662" time="448.458" type="appl" />
         <tli id="T663" time="448.871" type="appl" />
         <tli id="T664" time="449.283" type="appl" />
         <tli id="T665" time="449.695" type="appl" />
         <tli id="T666" time="450.108" type="appl" />
         <tli id="T667" time="451.0647301387668" />
         <tli id="T668" time="452.026" type="appl" />
         <tli id="T669" time="452.546" type="appl" />
         <tli id="T670" time="453.066" type="appl" />
         <tli id="T671" time="453.586" type="appl" />
         <tli id="T672" time="454.106" type="appl" />
         <tli id="T673" time="454.626" type="appl" />
         <tli id="T674" time="455.278" type="appl" />
         <tli id="T675" time="455.863" type="appl" />
         <tli id="T676" time="456.448" type="appl" />
         <tli id="T677" time="457.033" type="appl" />
         <tli id="T678" time="457.619" type="appl" />
         <tli id="T679" time="458.204" type="appl" />
         <tli id="T680" time="458.789" type="appl" />
         <tli id="T681" time="459.374" type="appl" />
         <tli id="T682" time="460.1913576226563" />
         <tli id="T683" time="460.759" type="appl" />
         <tli id="T684" time="461.119" type="appl" />
         <tli id="T685" time="461.479" type="appl" />
         <tli id="T686" time="461.839" type="appl" />
         <tli id="T687" time="462.199" type="appl" />
         <tli id="T688" time="462.559" type="appl" />
         <tli id="T689" time="462.919" type="appl" />
         <tli id="T690" time="463.279" type="appl" />
         <tli id="T691" time="463.639" type="appl" />
         <tli id="T692" time="463.999" type="appl" />
         <tli id="T693" time="464.359" type="appl" />
         <tli id="T694" time="464.719" type="appl" />
         <tli id="T695" time="465.079" type="appl" />
         <tli id="T696" time="465.439" type="appl" />
         <tli id="T697" time="466.316" type="appl" />
         <tli id="T698" time="467.14" type="appl" />
         <tli id="T699" time="467.963" type="appl" />
         <tli id="T700" time="468.786" type="appl" />
         <tli id="T701" time="469.246" type="appl" />
         <tli id="T702" time="469.652" type="appl" />
         <tli id="T703" time="470.059" type="appl" />
         <tli id="T704" time="470.465" type="appl" />
         <tli id="T705" time="470.871" type="appl" />
         <tli id="T706" time="471.277" type="appl" />
         <tli id="T707" time="471.684" type="appl" />
         <tli id="T708" time="472.09" type="appl" />
         <tli id="T709" time="472.496" type="appl" />
         <tli id="T710" time="472.902" type="appl" />
         <tli id="T711" time="473.308" type="appl" />
         <tli id="T712" time="473.715" type="appl" />
         <tli id="T713" time="474.121" type="appl" />
         <tli id="T714" time="474.94462761670144" />
         <tli id="T715" time="475.52" type="appl" />
         <tli id="T716" time="475.906" type="appl" />
         <tli id="T717" time="476.293" type="appl" />
         <tli id="T718" time="476.68" type="appl" />
         <tli id="T719" time="477.066" type="appl" />
         <tli id="T720" time="477.453" type="appl" />
         <tli id="T721" time="477.839" type="appl" />
         <tli id="T722" time="478.226" type="appl" />
         <tli id="T723" time="478.613" type="appl" />
         <tli id="T724" time="478.999" type="appl" />
         <tli id="T725" time="479.61794088639215" />
         <tli id="T726" time="480.285" type="appl" />
         <tli id="T727" time="480.719" type="appl" />
         <tli id="T728" time="481.152" type="appl" />
         <tli id="T729" time="481.586" type="appl" />
         <tli id="T730" time="482.02" type="appl" />
         <tli id="T731" time="482.453" type="appl" />
         <tli id="T732" time="482.886" type="appl" />
         <tli id="T733" time="483.32" type="appl" />
         <tli id="T734" time="483.736" type="appl" />
         <tli id="T735" time="484.1" type="appl" />
         <tli id="T736" time="484.464" type="appl" />
         <tli id="T737" time="484.828" type="appl" />
         <tli id="T738" time="485.192" type="appl" />
         <tli id="T739" time="485.557" type="appl" />
         <tli id="T740" time="485.921" type="appl" />
         <tli id="T741" time="486.285" type="appl" />
         <tli id="T742" time="486.649" type="appl" />
         <tli id="T743" time="487.013" type="appl" />
         <tli id="T744" time="487.348" type="appl" />
         <tli id="T745" time="487.63" type="appl" />
         <tli id="T746" time="487.912" type="appl" />
         <tli id="T747" time="488.195" type="appl" />
         <tli id="T748" time="488.477" type="appl" />
         <tli id="T749" time="488.759" type="appl" />
         <tli id="T750" time="489.041" type="appl" />
         <tli id="T751" time="489.323" type="appl" />
         <tli id="T752" time="489.606" type="appl" />
         <tli id="T753" time="489.888" type="appl" />
         <tli id="T754" time="490.17" type="appl" />
         <tli id="T755" time="490.71789323166325" />
         <tli id="T756" time="492.296" type="appl" />
         <tli id="T757" time="492.859" type="appl" />
         <tli id="T758" time="493.422" type="appl" />
         <tli id="T759" time="493.986" type="appl" />
         <tli id="T760" time="494.549" type="appl" />
         <tli id="T761" time="495.112" type="appl" />
         <tli id="T762" time="495.675" type="appl" />
         <tli id="T763" time="496.238" type="appl" />
         <tli id="T764" time="496.687" type="appl" />
         <tli id="T765" time="496.988" type="appl" />
         <tli id="T766" time="497.288" type="appl" />
         <tli id="T767" time="497.589" type="appl" />
         <tli id="T768" time="497.89" type="appl" />
         <tli id="T769" time="498.191" type="appl" />
         <tli id="T770" time="498.491" type="appl" />
         <tli id="T771" time="498.792" type="appl" />
         <tli id="T772" time="499.093" type="appl" />
         <tli id="T773" time="499.457" type="appl" />
         <tli id="T774" time="499.808" type="appl" />
         <tli id="T775" time="500.159" type="appl" />
         <tli id="T776" time="500.51" type="appl" />
         <tli id="T777" time="500.862" type="appl" />
         <tli id="T778" time="501.213" type="appl" />
         <tli id="T779" time="501.564" type="appl" />
         <tli id="T780" time="501.915" type="appl" />
         <tli id="T781" time="502.266" type="appl" />
         <tli id="T782" time="502.805" type="appl" />
         <tli id="T783" time="503.185" type="appl" />
         <tli id="T784" time="503.564" type="appl" />
         <tli id="T785" time="503.943" type="appl" />
         <tli id="T786" time="504.323" type="appl" />
         <tli id="T787" time="504.702" type="appl" />
         <tli id="T788" time="505.081" type="appl" />
         <tli id="T789" time="505.461" type="appl" />
         <tli id="T790" time="505.84" type="appl" />
         <tli id="T791" time="506.219" type="appl" />
         <tli id="T792" time="506.598" type="appl" />
         <tli id="T793" time="506.978" type="appl" />
         <tli id="T794" time="507.357" type="appl" />
         <tli id="T795" time="507.736" type="appl" />
         <tli id="T796" time="508.116" type="appl" />
         <tli id="T797" time="508.495" type="appl" />
         <tli id="T798" time="508.874" type="appl" />
         <tli id="T799" time="509.254" type="appl" />
         <tli id="T800" time="510.04447692472104" />
         <tli id="T801" time="510.478" type="appl" />
         <tli id="T802" time="510.917" type="appl" />
         <tli id="T803" time="511.356" type="appl" />
         <tli id="T804" time="511.795" type="appl" />
         <tli id="T805" time="512.234" type="appl" />
         <tli id="T806" time="512.672" type="appl" />
         <tli id="T807" time="513.111" type="appl" />
         <tli id="T808" time="513.55" type="appl" />
         <tli id="T809" time="513.989" type="appl" />
         <tli id="T810" time="514.428" type="appl" />
         <tli id="T811" time="514.867" type="appl" />
         <tli id="T812" time="515.306" type="appl" />
         <tli id="T813" time="515.745" type="appl" />
         <tli id="T814" time="516.184" type="appl" />
         <tli id="T815" time="516.623" type="appl" />
         <tli id="T816" time="517.062" type="appl" />
         <tli id="T817" time="517.5" type="appl" />
         <tli id="T818" time="517.939" type="appl" />
         <tli id="T819" time="518.378" type="appl" />
         <tli id="T820" time="518.817" type="appl" />
         <tli id="T821" time="519.256" type="appl" />
         <tli id="T822" time="519.695" type="appl" />
         <tli id="T823" time="520.087" type="appl" />
         <tli id="T824" time="520.415" type="appl" />
         <tli id="T825" time="520.743" type="appl" />
         <tli id="T826" time="521.071" type="appl" />
         <tli id="T827" time="521.398" type="appl" />
         <tli id="T828" time="521.726" type="appl" />
         <tli id="T829" time="522.054" type="appl" />
         <tli id="T830" time="522.382" type="appl" />
         <tli id="T831" time="522.71" type="appl" />
         <tli id="T832" time="523.038" type="appl" />
         <tli id="T833" time="523.579" type="appl" />
         <tli id="T834" time="523.958" type="appl" />
         <tli id="T835" time="524.338" type="appl" />
         <tli id="T836" time="524.717" type="appl" />
         <tli id="T837" time="525.097" type="appl" />
         <tli id="T838" time="525.476" type="appl" />
         <tli id="T839" time="525.856" type="appl" />
         <tli id="T840" time="526.236" type="appl" />
         <tli id="T841" time="526.615" type="appl" />
         <tli id="T842" time="526.995" type="appl" />
         <tli id="T843" time="527.374" type="appl" />
         <tli id="T844" time="527.754" type="appl" />
         <tli id="T845" time="528.133" type="appl" />
         <tli id="T846" time="528.513" type="appl" />
         <tli id="T847" time="528.892" type="appl" />
         <tli id="T848" time="529.6377261395871" />
         <tli id="T849" time="530.512" type="appl" />
         <tli id="T850" time="531.6643841053302" />
         <tli id="T851" time="532.102" type="appl" />
         <tli id="T852" time="532.484" type="appl" />
         <tli id="T853" time="532.867" type="appl" />
         <tli id="T854" time="533.249" type="appl" />
         <tli id="T855" time="533.631" type="appl" />
         <tli id="T856" time="534.013" type="appl" />
         <tli id="T857" time="534.645" type="appl" />
         <tli id="T858" time="535.13" type="appl" />
         <tli id="T859" time="535.616" type="appl" />
         <tli id="T860" time="536.101" type="appl" />
         <tli id="T861" time="536.7710288479595" />
         <tli id="T862" time="537.666" type="appl" />
         <tli id="T863" time="538.066" type="appl" />
         <tli id="T864" time="538.7776868995671" />
         <tli id="T865" time="539.239" type="appl" />
         <tli id="T866" time="539.639" type="appl" />
         <tli id="T867" time="540.039" type="appl" />
         <tli id="T868" time="540.439" type="appl" />
         <tli id="T869" time="540.839" type="appl" />
         <tli id="T870" time="541.24" type="appl" />
         <tli id="T871" time="541.64" type="appl" />
         <tli id="T872" time="542.04" type="appl" />
         <tli id="T873" time="542.44" type="appl" />
         <tli id="T874" time="542.84" type="appl" />
         <tli id="T875" time="543.24" type="appl" />
         <tli id="T876" time="543.64" type="appl" />
         <tli id="T877" time="544.106" type="appl" />
         <tli id="T878" time="545.107" type="appl" />
         <tli id="T879" time="545.315" type="appl" />
         <tli id="T880" time="545.711" type="appl" />
         <tli id="T881" time="546.107" type="appl" />
         <tli id="T882" time="546.504" type="appl" />
         <tli id="T883" time="546.9" type="appl" />
         <tli id="T884" time="547.296" type="appl" />
         <tli id="T885" time="547.8909811073663" />
         <tli id="T886" time="548.663" type="appl" />
         <tli id="T887" time="549.154" type="appl" />
         <tli id="T888" time="549.646" type="appl" />
         <tli id="T889" time="550.137" type="appl" />
         <tli id="T890" time="550.628" type="appl" />
         <tli id="T891" time="551.119" type="appl" />
         <tli id="T892" time="552.12" type="appl" />
         <tli id="T893" time="552.332" type="appl" />
         <tli id="T894" time="553.1442918869901" />
         <tli id="T895" time="553.642" type="appl" />
         <tli id="T896" time="554.099" type="appl" />
         <tli id="T897" time="554.555" type="appl" />
         <tli id="T898" time="555.012" type="appl" />
         <tli id="T899" time="555.624" type="appl" />
         <tli id="T900" time="555.983" type="appl" />
         <tli id="T901" time="556.341" type="appl" />
         <tli id="T902" time="556.699" type="appl" />
         <tli id="T903" time="557.058" type="appl" />
         <tli id="T904" time="557.416" type="appl" />
         <tli id="T905" time="557.775" type="appl" />
         <tli id="T906" time="558.133" type="appl" />
         <tli id="T907" time="558.491" type="appl" />
         <tli id="T908" time="558.85" type="appl" />
         <tli id="T909" time="559.208" type="appl" />
         <tli id="T910" time="559.939" type="appl" />
         <tli id="T911" time="560.305" type="appl" />
         <tli id="T912" time="560.671" type="appl" />
         <tli id="T913" time="561.037" type="appl" />
         <tli id="T914" time="561.403" type="appl" />
         <tli id="T915" time="561.769" type="appl" />
         <tli id="T916" time="562.135" type="appl" />
         <tli id="T917" time="562.501" type="appl" />
         <tli id="T918" time="562.867" type="appl" />
         <tli id="T919" time="563.233" type="appl" />
         <tli id="T920" time="563.6" type="appl" />
         <tli id="T921" time="563.966" type="appl" />
         <tli id="T922" time="564.332" type="appl" />
         <tli id="T923" time="564.698" type="appl" />
         <tli id="T924" time="565.064" type="appl" />
         <tli id="T925" time="565.43" type="appl" />
         <tli id="T926" time="565.796" type="appl" />
         <tli id="T927" time="566.162" type="appl" />
         <tli id="T928" time="566.7775666894403" />
         <tli id="T929" time="566.894" type="appl" />
         <tli id="T930" time="567.26" type="appl" />
         <tli id="T931" time="567.626" type="appl" />
         <tli id="T932" time="567.7073333333333" type="intp" />
         <tli id="T933" time="567.7886666666666" type="intp" />
         <tli id="T934" time="567.87" type="appl" />
         <tli id="T935" time="568.249" type="appl" />
         <tli id="T936" time="568.628" type="appl" />
         <tli id="T937" time="569.007" type="appl" />
         <tli id="T938" time="569.386" type="appl" />
         <tli id="T939" time="569.531" type="intp" />
         <tli id="T940" time="569.6759999999999" type="intp" />
         <tli id="T941" time="569.821" type="appl" />
         <tli id="T942" time="570.383" type="appl" />
         <tli id="T943" time="571.610879272216" />
         <tli id="T944" time="572.971" type="appl" />
         <tli id="T945" time="573.715" type="appl" />
         <tli id="T946" time="574.46" type="appl" />
         <tli id="T947" time="575.204" type="appl" />
         <tli id="T948" time="575.949" type="appl" />
         <tli id="T949" time="576.694" type="appl" />
         <tli id="T950" time="577.438" type="appl" />
         <tli id="T951" time="578.183" type="appl" />
         <tli id="T952" time="578.928" type="appl" />
         <tli id="T953" time="579.672" type="appl" />
         <tli id="T954" time="580.417" type="appl" />
         <tli id="T955" time="581.161" type="appl" />
         <tli id="T956" time="584.337491300518" />
         <tli id="T957" time="585.393" type="appl" />
         <tli id="T958" time="586.227" type="appl" />
         <tli id="T959" time="587.06" type="appl" />
         <tli id="T960" time="587.893" type="appl" />
         <tli id="T961" time="588.727" type="appl" />
         <tli id="T962" time="589.56" type="appl" />
         <tli id="T963" time="590.257" type="appl" />
         <tli id="T964" time="590.913" type="appl" />
         <tli id="T965" time="591.57" type="appl" />
         <tli id="T966" time="592.227" type="appl" />
         <tli id="T967" time="592.883" type="appl" />
         <tli id="T968" time="593.54" type="appl" />
         <tli id="T969" time="594.197" type="appl" />
         <tli id="T970" time="594.853" type="appl" />
         <tli id="T971" time="595.51" type="appl" />
         <tli id="T972" time="596.167" type="appl" />
         <tli id="T973" time="596.823" type="appl" />
         <tli id="T974" time="598.5307636987656" />
         <tli id="T975" time="599.866" type="appl" />
         <tli id="T976" time="600.946" type="appl" />
         <tli id="T977" time="601.928" type="appl" />
         <tli id="T978" time="602.71" type="appl" />
         <tli id="T979" time="603.493" type="appl" />
         <tli id="T980" time="604.275" type="appl" />
         <tli id="T981" time="605.057" type="appl" />
         <tli id="T982" time="605.839" type="appl" />
         <tli id="T983" time="606.622" type="appl" />
         <tli id="T984" time="607.404" type="appl" />
         <tli id="T985" time="608.186" type="appl" />
         <tli id="T986" time="609.2" type="appl" />
         <tli id="T987" time="610.066" type="appl" />
         <tli id="T988" time="610.933" type="appl" />
         <tli id="T989" time="611.799" type="appl" />
         <tli id="T990" time="612.666" type="appl" />
         <tli id="T991" time="613.532" type="appl" />
         <tli id="T992" time="614.399" type="appl" />
         <tli id="T993" time="615.009" type="appl" />
         <tli id="T994" time="615.619" type="appl" />
         <tli id="T995" time="616.229" type="appl" />
         <tli id="T996" time="616.839" type="appl" />
         <tli id="T997" time="617.617" type="appl" />
         <tli id="T998" time="618.341" type="appl" />
         <tli id="T999" time="619.065" type="appl" />
         <tli id="T1000" time="619.788" type="appl" />
         <tli id="T1001" time="620.512" type="appl" />
         <tli id="T1002" time="621.236" type="appl" />
         <tli id="T1003" time="621.96" type="appl" />
         <tli id="T1004" time="622.667" type="appl" />
         <tli id="T1005" time="623.33" type="appl" />
         <tli id="T1006" time="625.1173162230547" />
         <tli id="T1007" time="626.084" type="appl" />
         <tli id="T1008" time="627.068" type="appl" />
         <tli id="T1009" time="628.053" type="appl" />
         <tli id="T1010" time="629.038" type="appl" />
         <tli id="T1011" time="630.023" type="appl" />
         <tli id="T1012" time="631.007" type="appl" />
         <tli id="T1013" time="632.1053070552854" />
         <tli id="T1014" time="632.652" type="appl" />
         <tli id="T1015" time="633.312" type="appl" />
         <tli id="T1016" time="634.365" type="appl" />
         <tli id="T1017" time="634.965" type="appl" />
         <tli id="T1018" time="636.1306022737381" />
         <tli id="T1019" time="637.088" type="appl" />
         <tli id="T1020" time="637.837" type="appl" />
         <tli id="T1021" time="638.587" type="appl" />
         <tli id="T1022" time="639.336" type="appl" />
         <tli id="T1023" time="640.085" type="appl" />
         <tli id="T1024" time="640.886" type="appl" />
         <tli id="T1025" time="641.626" type="appl" />
         <tli id="T1026" time="642.366" type="appl" />
         <tli id="T1027" time="643.106" type="appl" />
         <tli id="T1028" time="643.846" type="appl" />
         <tli id="T1029" time="644.586" type="appl" />
         <tli id="T1030" time="645.326" type="appl" />
         <tli id="T1031" time="646.066" type="appl" />
         <tli id="T1032" time="646.806" type="appl" />
         <tli id="T1033" time="647.546" type="appl" />
         <tli id="T1034" time="648.286" type="appl" />
         <tli id="T1035" time="649.026" type="appl" />
         <tli id="T1036" time="649.766" type="appl" />
         <tli id="T1037" time="650.810539249286" />
         <tli id="T1038" time="653.01" type="appl" />
         <tli id="T1039" time="654.981" type="appl" />
         <tli id="T1040" time="656.951" type="appl" />
         <tli id="T1041" time="658.922" type="appl" />
         <tli id="T1042" time="660.893" type="appl" />
         <tli id="T1043" time="662.014" type="appl" />
         <tli id="T1044" time="662.896" type="appl" />
         <tli id="T1045" time="663.778" type="appl" />
         <tli id="T1046" time="664.659" type="appl" />
         <tli id="T1047" time="665.541" type="appl" />
         <tli id="T1048" time="666.423" type="appl" />
         <tli id="T1049" time="667.305" type="appl" />
         <tli id="T1050" time="668.151" type="appl" />
         <tli id="T1051" time="668.836" type="appl" />
         <tli id="T1052" time="669.522" type="appl" />
         <tli id="T1053" time="670.207" type="appl" />
         <tli id="T1054" time="670.892" type="appl" />
         <tli id="T1055" time="674.51" />
         <tli id="T1056" time="674.595" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-KA"
                      id="tx-KA"
                      speaker="KA"
                      type="t">
         <timeline-fork end="T1" start="T0">
            <tli id="T0.tx-KA.1" />
            <tli id="T0.tx-KA.2" />
            <tli id="T0.tx-KA.3" />
            <tli id="T0.tx-KA.4" />
            <tli id="T0.tx-KA.5" />
            <tli id="T0.tx-KA.6" />
            <tli id="T0.tx-KA.7" />
            <tli id="T0.tx-KA.8" />
            <tli id="T0.tx-KA.9" />
            <tli id="T0.tx-KA.10" />
            <tli id="T0.tx-KA.11" />
         </timeline-fork>
         <timeline-fork end="T2" start="T1">
            <tli id="T1.tx-KA.1" />
            <tli id="T1.tx-KA.2" />
         </timeline-fork>
         <timeline-fork end="T132" start="T126">
            <tli id="T126.tx-KA.1" />
            <tli id="T126.tx-KA.2" />
            <tli id="T126.tx-KA.3" />
            <tli id="T126.tx-KA.4" />
            <tli id="T126.tx-KA.5" />
         </timeline-fork>
         <timeline-fork end="T142" start="T132">
            <tli id="T132.tx-KA.1" />
            <tli id="T132.tx-KA.2" />
            <tli id="T132.tx-KA.3" />
            <tli id="T132.tx-KA.4" />
            <tli id="T132.tx-KA.5" />
            <tli id="T132.tx-KA.6" />
            <tli id="T132.tx-KA.7" />
            <tli id="T132.tx-KA.8" />
            <tli id="T132.tx-KA.9" />
         </timeline-fork>
         <timeline-fork end="T150" start="T142">
            <tli id="T142.tx-KA.1" />
            <tli id="T142.tx-KA.2" />
            <tli id="T142.tx-KA.3" />
            <tli id="T142.tx-KA.4" />
            <tli id="T142.tx-KA.5" />
            <tli id="T142.tx-KA.6" />
            <tli id="T142.tx-KA.7" />
         </timeline-fork>
         <timeline-fork end="T160" start="T154">
            <tli id="T154.tx-KA.1" />
            <tli id="T154.tx-KA.2" />
            <tli id="T154.tx-KA.3" />
         </timeline-fork>
         <timeline-fork end="T166" start="T161">
            <tli id="T161.tx-KA.1" />
            <tli id="T161.tx-KA.2" />
            <tli id="T161.tx-KA.3" />
            <tli id="T161.tx-KA.4" />
         </timeline-fork>
         <timeline-fork end="T173" start="T169">
            <tli id="T169.tx-KA.1" />
            <tli id="T169.tx-KA.2" />
            <tli id="T169.tx-KA.3" />
         </timeline-fork>
         <timeline-fork end="T191" start="T185">
            <tli id="T185.tx-KA.1" />
            <tli id="T185.tx-KA.2" />
            <tli id="T185.tx-KA.3" />
            <tli id="T185.tx-KA.4" />
            <tli id="T185.tx-KA.5" />
         </timeline-fork>
         <timeline-fork end="T206" start="T196">
            <tli id="T196.tx-KA.1" />
            <tli id="T196.tx-KA.2" />
            <tli id="T196.tx-KA.3" />
            <tli id="T196.tx-KA.4" />
            <tli id="T196.tx-KA.5" />
            <tli id="T196.tx-KA.6" />
            <tli id="T196.tx-KA.7" />
            <tli id="T196.tx-KA.8" />
            <tli id="T196.tx-KA.9" />
         </timeline-fork>
         <timeline-fork end="T212" start="T206">
            <tli id="T206.tx-KA.1" />
            <tli id="T206.tx-KA.2" />
            <tli id="T206.tx-KA.3" />
            <tli id="T206.tx-KA.4" />
            <tli id="T206.tx-KA.5" />
         </timeline-fork>
         <timeline-fork end="T232" start="T219">
            <tli id="T219.tx-KA.1" />
            <tli id="T219.tx-KA.2" />
            <tli id="T219.tx-KA.3" />
            <tli id="T219.tx-KA.4" />
            <tli id="T219.tx-KA.5" />
            <tli id="T219.tx-KA.6" />
            <tli id="T219.tx-KA.7" />
            <tli id="T219.tx-KA.8" />
            <tli id="T219.tx-KA.9" />
         </timeline-fork>
         <timeline-fork end="T243" start="T238">
            <tli id="T238.tx-KA.1" />
            <tli id="T238.tx-KA.2" />
            <tli id="T238.tx-KA.3" />
            <tli id="T238.tx-KA.4" />
         </timeline-fork>
         <timeline-fork end="T501" start="T500">
            <tli id="T500.tx-KA.1" />
         </timeline-fork>
         <timeline-fork end="T509" start="T501">
            <tli id="T501.tx-KA.1" />
            <tli id="T501.tx-KA.2" />
            <tli id="T501.tx-KA.3" />
            <tli id="T501.tx-KA.4" />
            <tli id="T501.tx-KA.5" />
            <tli id="T501.tx-KA.6" />
            <tli id="T501.tx-KA.7" />
         </timeline-fork>
         <timeline-fork end="T517" start="T509">
            <tli id="T509.tx-KA.1" />
            <tli id="T509.tx-KA.2" />
            <tli id="T509.tx-KA.3" />
            <tli id="T509.tx-KA.4" />
            <tli id="T509.tx-KA.5" />
            <tli id="T509.tx-KA.6" />
            <tli id="T509.tx-KA.7" />
         </timeline-fork>
         <timeline-fork end="T539" start="T527">
            <tli id="T527.tx-KA.1" />
            <tli id="T527.tx-KA.2" />
            <tli id="T527.tx-KA.3" />
            <tli id="T527.tx-KA.4" />
            <tli id="T527.tx-KA.5" />
            <tli id="T527.tx-KA.6" />
            <tli id="T527.tx-KA.7" />
            <tli id="T527.tx-KA.8" />
            <tli id="T527.tx-KA.9" />
            <tli id="T527.tx-KA.10" />
            <tli id="T527.tx-KA.11" />
         </timeline-fork>
         <timeline-fork end="T543" start="T539">
            <tli id="T539.tx-KA.1" />
            <tli id="T539.tx-KA.2" />
            <tli id="T539.tx-KA.3" />
         </timeline-fork>
         <timeline-fork end="T549" start="T543">
            <tli id="T543.tx-KA.1" />
            <tli id="T543.tx-KA.2" />
            <tli id="T543.tx-KA.3" />
         </timeline-fork>
         <timeline-fork end="T565" start="T549">
            <tli id="T549.tx-KA.1" />
            <tli id="T549.tx-KA.2" />
            <tli id="T549.tx-KA.3" />
            <tli id="T549.tx-KA.4" />
            <tli id="T549.tx-KA.5" />
            <tli id="T549.tx-KA.6" />
            <tli id="T549.tx-KA.7" />
            <tli id="T549.tx-KA.8" />
            <tli id="T549.tx-KA.9" />
            <tli id="T549.tx-KA.10" />
            <tli id="T549.tx-KA.11" />
         </timeline-fork>
         <timeline-fork end="T909" start="T898">
            <tli id="T898.tx-KA.1" />
            <tli id="T898.tx-KA.2" />
            <tli id="T898.tx-KA.3" />
            <tli id="T898.tx-KA.4" />
            <tli id="T898.tx-KA.5" />
            <tli id="T898.tx-KA.6" />
            <tli id="T898.tx-KA.7" />
            <tli id="T898.tx-KA.8" />
         </timeline-fork>
         <timeline-fork end="T931" start="T909">
            <tli id="T909.tx-KA.1" />
            <tli id="T909.tx-KA.2" />
            <tli id="T909.tx-KA.3" />
            <tli id="T909.tx-KA.4" />
            <tli id="T909.tx-KA.5" />
            <tli id="T909.tx-KA.6" />
            <tli id="T909.tx-KA.7" />
            <tli id="T909.tx-KA.8" />
            <tli id="T909.tx-KA.9" />
            <tli id="T909.tx-KA.10" />
            <tli id="T909.tx-KA.11" />
            <tli id="T909.tx-KA.12" />
            <tli id="T909.tx-KA.13" />
            <tli id="T909.tx-KA.14" />
            <tli id="T909.tx-KA.15" />
            <tli id="T909.tx-KA.16" />
            <tli id="T909.tx-KA.17" />
            <tli id="T909.tx-KA.18" />
            <tli id="T909.tx-KA.19" />
            <tli id="T909.tx-KA.20" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KA">
            <ts e="T3" id="Seg_0" n="sc" s="T0">
               <ts e="T1" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T0.tx-KA.1" id="Seg_4" n="HIAT:w" s="T0">Ago</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-KA.2" id="Seg_7" n="HIAT:w" s="T0.tx-KA.1">Künnap</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-KA.3" id="Seg_10" n="HIAT:w" s="T0.tx-KA.2">haastelee</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-KA.4" id="Seg_13" n="HIAT:w" s="T0.tx-KA.3">Klaudija</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-KA.5" id="Seg_16" n="HIAT:w" s="T0.tx-KA.4">Plotnikovaa</ts>
                  <nts id="Seg_17" n="HIAT:ip">,</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-KA.6" id="Seg_20" n="HIAT:w" s="T0.tx-KA.5">75-vuotias</ts>
                  <nts id="Seg_21" n="HIAT:ip">,</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-KA.7" id="Seg_24" n="HIAT:w" s="T0.tx-KA.6">kotoisin</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-KA.8" id="Seg_27" n="HIAT:w" s="T0.tx-KA.7">Abalakovon</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-KA.9" id="Seg_30" n="HIAT:w" s="T0.tx-KA.8">kylästä</ts>
                  <nts id="Seg_31" n="HIAT:ip">,</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-KA.10" id="Seg_34" n="HIAT:w" s="T0.tx-KA.9">Länsi-Siperiasta</ts>
                  <nts id="Seg_35" n="HIAT:ip">,</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-KA.11" id="Seg_38" n="HIAT:w" s="T0.tx-KA.10">Sajaanin</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1" id="Seg_41" n="HIAT:w" s="T0.tx-KA.11">vuoristolta</ts>
                  <nts id="Seg_42" n="HIAT:ip">.</nts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1.tx-KA.1" id="Seg_45" n="HIAT:u" s="T1">
                  <ts e="T1.tx-KA.1" id="Seg_47" n="HIAT:w" s="T1">21</ts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
               </ts>
               <ts e="T1.tx-KA.2" id="Seg_50" n="HIAT:u" s="T1.tx-KA.1">
                  <ts e="T1.tx-KA.2" id="Seg_52" n="HIAT:w" s="T1.tx-KA.1">08</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
               </ts>
               <ts e="T2" id="Seg_55" n="HIAT:u" s="T1.tx-KA.2">
                  <ts e="T2" id="Seg_57" n="HIAT:w" s="T1.tx-KA.2">1970</ts>
                  <nts id="Seg_58" n="HIAT:ip">.</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3" id="Seg_61" n="HIAT:u" s="T2">
                  <ts e="T3" id="Seg_63" n="HIAT:w" s="T2">Прошу</ts>
                  <nts id="Seg_64" n="HIAT:ip">.</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T150" id="Seg_66" n="sc" s="T124">
               <ts e="T125" id="Seg_68" n="HIAT:u" s="T124">
                  <nts id="Seg_69" n="HIAT:ip">(</nts>
                  <nts id="Seg_70" n="HIAT:ip">(</nts>
                  <ats e="T125" id="Seg_71" n="HIAT:non-pho" s="T124">…</ats>
                  <nts id="Seg_72" n="HIAT:ip">)</nts>
                  <nts id="Seg_73" n="HIAT:ip">)</nts>
                  <nts id="Seg_74" n="HIAT:ip">.</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_77" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_79" n="HIAT:w" s="T125">Так</ts>
                  <nts id="Seg_80" n="HIAT:ip">.</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_83" n="HIAT:u" s="T126">
                  <ts e="T126.tx-KA.1" id="Seg_85" n="HIAT:w" s="T126">Это</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126.tx-KA.2" id="Seg_88" n="HIAT:w" s="T126.tx-KA.1">было</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126.tx-KA.3" id="Seg_91" n="HIAT:w" s="T126.tx-KA.2">очень</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126.tx-KA.4" id="Seg_94" n="HIAT:w" s="T126.tx-KA.3">интересно</ts>
                  <nts id="Seg_95" n="HIAT:ip">,</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126.tx-KA.5" id="Seg_98" n="HIAT:w" s="T126.tx-KA.4">прекрасно</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_101" n="HIAT:w" s="T126.tx-KA.5">услышать</ts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_105" n="HIAT:u" s="T132">
                  <ts e="T132.tx-KA.1" id="Seg_107" n="HIAT:w" s="T132">Ну</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132.tx-KA.2" id="Seg_110" n="HIAT:w" s="T132.tx-KA.1">как</ts>
                  <nts id="Seg_111" n="HIAT:ip">,</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132.tx-KA.3" id="Seg_114" n="HIAT:w" s="T132.tx-KA.2">может</ts>
                  <nts id="Seg_115" n="HIAT:ip">,</nts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132.tx-KA.4" id="Seg_118" n="HIAT:w" s="T132.tx-KA.3">тогда</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132.tx-KA.5" id="Seg_121" n="HIAT:w" s="T132.tx-KA.4">расскажешь</ts>
                  <nts id="Seg_122" n="HIAT:ip">,</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132.tx-KA.6" id="Seg_125" n="HIAT:w" s="T132.tx-KA.5">как</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132.tx-KA.7" id="Seg_128" n="HIAT:w" s="T132.tx-KA.6">сегодня</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132.tx-KA.8" id="Seg_131" n="HIAT:w" s="T132.tx-KA.7">ходила</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132.tx-KA.9" id="Seg_134" n="HIAT:w" s="T132.tx-KA.8">у</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_137" n="HIAT:w" s="T132.tx-KA.9">нас</ts>
                  <nts id="Seg_138" n="HIAT:ip">?</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_141" n="HIAT:u" s="T142">
                  <ts e="T142.tx-KA.1" id="Seg_143" n="HIAT:w" s="T142">Как</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142.tx-KA.2" id="Seg_146" n="HIAT:w" s="T142.tx-KA.1">понравилось</ts>
                  <nts id="Seg_147" n="HIAT:ip">,</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142.tx-KA.3" id="Seg_150" n="HIAT:w" s="T142.tx-KA.2">какие</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142.tx-KA.4" id="Seg_153" n="HIAT:w" s="T142.tx-KA.3">люди</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_155" n="HIAT:ip">(</nts>
                  <nts id="Seg_156" n="HIAT:ip">(</nts>
                  <ats e="T142.tx-KA.5" id="Seg_157" n="HIAT:non-pho" s="T142.tx-KA.4">…</ats>
                  <nts id="Seg_158" n="HIAT:ip">)</nts>
                  <nts id="Seg_159" n="HIAT:ip">)</nts>
                  <nts id="Seg_160" n="HIAT:ip">,</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142.tx-KA.6" id="Seg_163" n="HIAT:w" s="T142.tx-KA.5">что</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142.tx-KA.7" id="Seg_166" n="HIAT:w" s="T142.tx-KA.6">и</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_169" n="HIAT:w" s="T142.tx-KA.7">как</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T160" id="Seg_172" n="sc" s="T154">
               <ts e="T160" id="Seg_174" n="HIAT:u" s="T154">
                  <ts e="T154.tx-KA.1" id="Seg_176" n="HIAT:w" s="T154">Тебе</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_178" n="HIAT:ip">(</nts>
                  <ts e="T154.tx-KA.2" id="Seg_180" n="HIAT:w" s="T154.tx-KA.1">очень</ts>
                  <nts id="Seg_181" n="HIAT:ip">)</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154.tx-KA.3" id="Seg_184" n="HIAT:w" s="T154.tx-KA.2">понравилось</ts>
                  <nts id="Seg_185" n="HIAT:ip">,</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_188" n="HIAT:w" s="T154.tx-KA.3">нет</ts>
                  <nts id="Seg_189" n="HIAT:ip">?</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T166" id="Seg_191" n="sc" s="T161">
               <ts e="T166" id="Seg_193" n="HIAT:u" s="T161">
                  <ts e="T161.tx-KA.1" id="Seg_195" n="HIAT:w" s="T161">Как</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161.tx-KA.2" id="Seg_198" n="HIAT:w" s="T161.tx-KA.1">понравилось</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161.tx-KA.3" id="Seg_201" n="HIAT:w" s="T161.tx-KA.2">там</ts>
                  <nts id="Seg_202" n="HIAT:ip">,</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161.tx-KA.4" id="Seg_205" n="HIAT:w" s="T161.tx-KA.3">на</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_208" n="HIAT:w" s="T161.tx-KA.4">Конгрессе</ts>
                  <nts id="Seg_209" n="HIAT:ip">?</nts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T173" id="Seg_211" n="sc" s="T168">
               <ts e="T169" id="Seg_213" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_215" n="HIAT:w" s="T168">Ничего</ts>
                  <nts id="Seg_216" n="HIAT:ip">?</nts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_219" n="HIAT:u" s="T169">
                  <ts e="T169.tx-KA.1" id="Seg_221" n="HIAT:w" s="T169">Люди</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169.tx-KA.2" id="Seg_224" n="HIAT:w" s="T169.tx-KA.1">добрые</ts>
                  <nts id="Seg_225" n="HIAT:ip">,</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169.tx-KA.3" id="Seg_228" n="HIAT:w" s="T169.tx-KA.2">не</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_231" n="HIAT:w" s="T169.tx-KA.3">плохие</ts>
                  <nts id="Seg_232" n="HIAT:ip">?</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T191" id="Seg_234" n="sc" s="T177">
               <ts e="T185" id="Seg_236" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_238" n="HIAT:w" s="T177">Да</ts>
                  <nts id="Seg_239" n="HIAT:ip">,</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_242" n="HIAT:w" s="T178">они</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_245" n="HIAT:w" s="T179">были</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_248" n="HIAT:w" s="T180">очень</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_251" n="HIAT:w" s="T181">рады</ts>
                  <nts id="Seg_252" n="HIAT:ip">,</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_255" n="HIAT:w" s="T182">что</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_258" n="HIAT:w" s="T183">ты</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_261" n="HIAT:w" s="T184">приехала</ts>
                  <nts id="Seg_262" n="HIAT:ip">.</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T191" id="Seg_265" n="HIAT:u" s="T185">
                  <ts e="T185.tx-KA.1" id="Seg_267" n="HIAT:w" s="T185">Они</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185.tx-KA.2" id="Seg_270" n="HIAT:w" s="T185.tx-KA.1">долго</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185.tx-KA.3" id="Seg_273" n="HIAT:w" s="T185.tx-KA.2">тебя</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185.tx-KA.4" id="Seg_276" n="HIAT:w" s="T185.tx-KA.3">уже</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185.tx-KA.5" id="Seg_279" n="HIAT:w" s="T185.tx-KA.4">там</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_282" n="HIAT:w" s="T185.tx-KA.5">ждали</ts>
                  <nts id="Seg_283" n="HIAT:ip">.</nts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T214" id="Seg_285" n="sc" s="T196">
               <ts e="T206" id="Seg_287" n="HIAT:u" s="T196">
                  <ts e="T196.tx-KA.1" id="Seg_289" n="HIAT:w" s="T196">Каждый</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196.tx-KA.2" id="Seg_292" n="HIAT:w" s="T196.tx-KA.1">день</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196.tx-KA.3" id="Seg_295" n="HIAT:w" s="T196.tx-KA.2">меня</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196.tx-KA.4" id="Seg_298" n="HIAT:w" s="T196.tx-KA.3">расспрашивали</ts>
                  <nts id="Seg_299" n="HIAT:ip">,</nts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196.tx-KA.5" id="Seg_302" n="HIAT:w" s="T196.tx-KA.4">когда</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196.tx-KA.6" id="Seg_305" n="HIAT:w" s="T196.tx-KA.5">это</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196.tx-KA.7" id="Seg_308" n="HIAT:w" s="T196.tx-KA.6">наша</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196.tx-KA.8" id="Seg_311" n="HIAT:w" s="T196.tx-KA.7">последняя</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196.tx-KA.9" id="Seg_314" n="HIAT:w" s="T196.tx-KA.8">камасинка</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_317" n="HIAT:w" s="T196.tx-KA.9">придет</ts>
                  <nts id="Seg_318" n="HIAT:ip">.</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_321" n="HIAT:u" s="T206">
                  <ts e="T206.tx-KA.1" id="Seg_323" n="HIAT:w" s="T206">Я</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206.tx-KA.2" id="Seg_326" n="HIAT:w" s="T206.tx-KA.1">говорю:</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_328" n="HIAT:ip">"</nts>
                  <ts e="T206.tx-KA.3" id="Seg_330" n="HIAT:w" s="T206.tx-KA.2">Подождите</ts>
                  <nts id="Seg_331" n="HIAT:ip">,</nts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206.tx-KA.4" id="Seg_334" n="HIAT:w" s="T206.tx-KA.3">подождите</ts>
                  <nts id="Seg_335" n="HIAT:ip">,</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206.tx-KA.5" id="Seg_338" n="HIAT:w" s="T206.tx-KA.4">сейчас</ts>
                  <nts id="Seg_339" n="HIAT:ip">,</nts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_342" n="HIAT:w" s="T206.tx-KA.5">сейчас</ts>
                  <nts id="Seg_343" n="HIAT:ip">"</nts>
                  <nts id="Seg_344" n="HIAT:ip">.</nts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_347" n="HIAT:u" s="T212">
                  <ts e="T214" id="Seg_349" n="HIAT:w" s="T212">Ннн</ts>
                  <nts id="Seg_350" n="HIAT:ip">…</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T232" id="Seg_352" n="sc" s="T219">
               <ts e="T232" id="Seg_354" n="HIAT:u" s="T219">
                  <ts e="T219.tx-KA.1" id="Seg_356" n="HIAT:w" s="T219">Ну</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219.tx-KA.2" id="Seg_359" n="HIAT:w" s="T219.tx-KA.1">давай</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219.tx-KA.3" id="Seg_362" n="HIAT:w" s="T219.tx-KA.2">сначала</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219.tx-KA.4" id="Seg_365" n="HIAT:w" s="T219.tx-KA.3">по-русски</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219.tx-KA.5" id="Seg_368" n="HIAT:w" s="T219.tx-KA.4">расскажи</ts>
                  <nts id="Seg_369" n="HIAT:ip">,</nts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219.tx-KA.6" id="Seg_372" n="HIAT:w" s="T219.tx-KA.5">а</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219.tx-KA.7" id="Seg_375" n="HIAT:w" s="T219.tx-KA.6">потом</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219.tx-KA.8" id="Seg_378" n="HIAT:w" s="T219.tx-KA.7">переведешь</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219.tx-KA.9" id="Seg_381" n="HIAT:w" s="T219.tx-KA.8">на</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_384" n="HIAT:w" s="T219.tx-KA.9">камасинский</ts>
                  <nts id="Seg_385" n="HIAT:ip">.</nts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T248" id="Seg_387" n="sc" s="T236">
               <ts e="T238" id="Seg_389" n="HIAT:u" s="T236">
                  <ts e="T237" id="Seg_391" n="HIAT:w" s="T236">Да</ts>
                  <nts id="Seg_392" n="HIAT:ip">,</nts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_395" n="HIAT:w" s="T237">работает</ts>
                  <nts id="Seg_396" n="HIAT:ip">.</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_399" n="HIAT:u" s="T238">
                  <ts e="T238.tx-KA.1" id="Seg_401" n="HIAT:w" s="T238">У</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238.tx-KA.2" id="Seg_404" n="HIAT:w" s="T238.tx-KA.1">них</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238.tx-KA.3" id="Seg_407" n="HIAT:w" s="T238.tx-KA.2">пленки</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238.tx-KA.4" id="Seg_410" n="HIAT:w" s="T238.tx-KA.3">сколько</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_413" n="HIAT:w" s="T238.tx-KA.4">угодно</ts>
                  <nts id="Seg_414" n="HIAT:ip">.</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T248" id="Seg_417" n="HIAT:u" s="T243">
                  <ts e="T244" id="Seg_419" n="HIAT:w" s="T243">Все</ts>
                  <nts id="Seg_420" n="HIAT:ip">,</nts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_423" n="HIAT:w" s="T244">что</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_426" n="HIAT:w" s="T245">болтаем</ts>
                  <nts id="Seg_427" n="HIAT:ip">,</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_430" n="HIAT:w" s="T246">пускай</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_433" n="HIAT:w" s="T247">пишут</ts>
                  <nts id="Seg_434" n="HIAT:ip">.</nts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T368" id="Seg_436" n="sc" s="T365">
               <ts e="T368" id="Seg_438" n="HIAT:u" s="T365">
                  <ts e="T368" id="Seg_440" n="HIAT:w" s="T365">Ага</ts>
                  <nts id="Seg_441" n="HIAT:ip">.</nts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T565" id="Seg_443" n="sc" s="T500">
               <ts e="T501" id="Seg_445" n="HIAT:u" s="T500">
                  <ts e="T500.tx-KA.1" id="Seg_447" n="HIAT:w" s="T500">Ага</ts>
                  <nts id="Seg_448" n="HIAT:ip">,</nts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_451" n="HIAT:w" s="T500.tx-KA.1">хорошо</ts>
                  <nts id="Seg_452" n="HIAT:ip">.</nts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T509" id="Seg_455" n="HIAT:u" s="T501">
                  <ts e="T501.tx-KA.1" id="Seg_457" n="HIAT:w" s="T501">Ну</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501.tx-KA.2" id="Seg_460" n="HIAT:w" s="T501.tx-KA.1">так</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501.tx-KA.3" id="Seg_463" n="HIAT:w" s="T501.tx-KA.2">это</ts>
                  <nts id="Seg_464" n="HIAT:ip">,</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501.tx-KA.4" id="Seg_467" n="HIAT:w" s="T501.tx-KA.3">было</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501.tx-KA.5" id="Seg_470" n="HIAT:w" s="T501.tx-KA.4">очень</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501.tx-KA.6" id="Seg_473" n="HIAT:w" s="T501.tx-KA.5">интересно</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501.tx-KA.7" id="Seg_476" n="HIAT:w" s="T501.tx-KA.6">послушать</ts>
                  <nts id="Seg_477" n="HIAT:ip">,</nts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_480" n="HIAT:w" s="T501.tx-KA.7">конечно</ts>
                  <nts id="Seg_481" n="HIAT:ip">.</nts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T517" id="Seg_484" n="HIAT:u" s="T509">
                  <ts e="T509.tx-KA.1" id="Seg_486" n="HIAT:w" s="T509">Ну</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509.tx-KA.2" id="Seg_489" n="HIAT:w" s="T509.tx-KA.1">вот</ts>
                  <nts id="Seg_490" n="HIAT:ip">,</nts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509.tx-KA.3" id="Seg_493" n="HIAT:w" s="T509.tx-KA.2">тут</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509.tx-KA.4" id="Seg_496" n="HIAT:w" s="T509.tx-KA.3">вчера</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509.tx-KA.5" id="Seg_499" n="HIAT:w" s="T509.tx-KA.4">Арпад</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509.tx-KA.6" id="Seg_502" n="HIAT:w" s="T509.tx-KA.5">ведь</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509.tx-KA.7" id="Seg_505" n="HIAT:w" s="T509.tx-KA.6">поздно</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_508" n="HIAT:w" s="T509.tx-KA.7">приезжал</ts>
                  <nts id="Seg_509" n="HIAT:ip">.</nts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T527" id="Seg_512" n="HIAT:u" s="T517">
                  <ts e="T518" id="Seg_514" n="HIAT:w" s="T517">Может</ts>
                  <nts id="Seg_515" n="HIAT:ip">,</nts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_518" n="HIAT:w" s="T518">об</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_521" n="HIAT:w" s="T519">этом</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_524" n="HIAT:w" s="T520">расскажешь:</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_527" n="HIAT:w" s="T521">как</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_530" n="HIAT:w" s="T522">он</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_533" n="HIAT:w" s="T523">приехал</ts>
                  <nts id="Seg_534" n="HIAT:ip">,</nts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_537" n="HIAT:w" s="T524">что</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_540" n="HIAT:w" s="T525">говорил</ts>
                  <nts id="Seg_541" n="HIAT:ip">…</nts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T539" id="Seg_544" n="HIAT:u" s="T527">
                  <ts e="T527.tx-KA.1" id="Seg_546" n="HIAT:w" s="T527">Как</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527.tx-KA.2" id="Seg_549" n="HIAT:w" s="T527.tx-KA.1">договорились</ts>
                  <nts id="Seg_550" n="HIAT:ip">,</nts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527.tx-KA.3" id="Seg_553" n="HIAT:w" s="T527.tx-KA.2">что</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527.tx-KA.4" id="Seg_556" n="HIAT:w" s="T527.tx-KA.3">сегодня</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527.tx-KA.5" id="Seg_559" n="HIAT:w" s="T527.tx-KA.4">поедем</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527.tx-KA.6" id="Seg_562" n="HIAT:w" s="T527.tx-KA.5">там</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527.tx-KA.7" id="Seg_565" n="HIAT:w" s="T527.tx-KA.6">на</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527.tx-KA.8" id="Seg_568" n="HIAT:w" s="T527.tx-KA.7">конгресс</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527.tx-KA.9" id="Seg_571" n="HIAT:w" s="T527.tx-KA.8">еще</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_573" n="HIAT:ip">(</nts>
                  <ts e="T527.tx-KA.10" id="Seg_575" n="HIAT:w" s="T527.tx-KA.9">такое</ts>
                  <nts id="Seg_576" n="HIAT:ip">)</nts>
                  <nts id="Seg_577" n="HIAT:ip">,</nts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527.tx-KA.11" id="Seg_580" n="HIAT:w" s="T527.tx-KA.10">на</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_583" n="HIAT:w" s="T527.tx-KA.11">конференцию</ts>
                  <nts id="Seg_584" n="HIAT:ip">.</nts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T543" id="Seg_587" n="HIAT:u" s="T539">
                  <ts e="T539.tx-KA.1" id="Seg_589" n="HIAT:w" s="T539">Тоже</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539.tx-KA.2" id="Seg_592" n="HIAT:w" s="T539.tx-KA.1">было</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539.tx-KA.3" id="Seg_595" n="HIAT:w" s="T539.tx-KA.2">бы</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_598" n="HIAT:w" s="T539.tx-KA.3">интересно</ts>
                  <nts id="Seg_599" n="HIAT:ip">.</nts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T549" id="Seg_602" n="HIAT:u" s="T543">
                  <ts e="T543.tx-KA.1" id="Seg_604" n="HIAT:w" s="T543">Сначала</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543.tx-KA.2" id="Seg_607" n="HIAT:w" s="T543.tx-KA.1">опять</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543.tx-KA.3" id="Seg_610" n="HIAT:w" s="T543.tx-KA.2">можно</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_613" n="HIAT:w" s="T543.tx-KA.3">по-русски</ts>
                  <nts id="Seg_614" n="HIAT:ip">.</nts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T565" id="Seg_617" n="HIAT:u" s="T549">
                  <ts e="T549.tx-KA.1" id="Seg_619" n="HIAT:w" s="T549">Просто</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549.tx-KA.2" id="Seg_622" n="HIAT:w" s="T549.tx-KA.1">расскажи</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549.tx-KA.3" id="Seg_625" n="HIAT:w" s="T549.tx-KA.2">по-русски</ts>
                  <nts id="Seg_626" n="HIAT:ip">,</nts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549.tx-KA.4" id="Seg_629" n="HIAT:w" s="T549.tx-KA.3">как</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_631" n="HIAT:ip">(</nts>
                  <ts e="T549.tx-KA.5" id="Seg_633" n="HIAT:w" s="T549.tx-KA.4">все</ts>
                  <nts id="Seg_634" n="HIAT:ip">)</nts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549.tx-KA.6" id="Seg_637" n="HIAT:w" s="T549.tx-KA.5">было</ts>
                  <nts id="Seg_638" n="HIAT:ip">,</nts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549.tx-KA.7" id="Seg_641" n="HIAT:w" s="T549.tx-KA.6">а</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549.tx-KA.8" id="Seg_644" n="HIAT:w" s="T549.tx-KA.7">потом</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549.tx-KA.9" id="Seg_647" n="HIAT:w" s="T549.tx-KA.8">уж</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549.tx-KA.10" id="Seg_650" n="HIAT:w" s="T549.tx-KA.9">подумаешь</ts>
                  <nts id="Seg_651" n="HIAT:ip">,</nts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549.tx-KA.11" id="Seg_654" n="HIAT:w" s="T549.tx-KA.10">поговоришь</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_657" n="HIAT:w" s="T549.tx-KA.11">по-своему</ts>
                  <nts id="Seg_658" n="HIAT:ip">.</nts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T864" id="Seg_660" n="sc" s="T861">
               <ts e="T864" id="Seg_662" n="HIAT:u" s="T861">
                  <ts e="T862" id="Seg_664" n="HIAT:w" s="T861">Нет</ts>
                  <nts id="Seg_665" n="HIAT:ip">,</nts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_668" n="HIAT:w" s="T862">не</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_671" n="HIAT:w" s="T863">был</ts>
                  <nts id="Seg_672" n="HIAT:ip">.</nts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T878" id="Seg_674" n="sc" s="T876">
               <ts e="T877" id="Seg_676" n="HIAT:u" s="T876">
                  <ts e="T877" id="Seg_678" n="HIAT:w" s="T876">Ага</ts>
                  <nts id="Seg_679" n="HIAT:ip">.</nts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T878" id="Seg_682" n="HIAT:u" s="T877">
                  <ts e="T878" id="Seg_684" n="HIAT:w" s="T877">Ага</ts>
                  <nts id="Seg_685" n="HIAT:ip">.</nts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T892" id="Seg_687" n="sc" s="T891">
               <ts e="T892" id="Seg_689" n="HIAT:u" s="T891">
                  <ts e="T892" id="Seg_691" n="HIAT:w" s="T891">Ага</ts>
                  <nts id="Seg_692" n="HIAT:ip">.</nts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T931" id="Seg_694" n="sc" s="T894">
               <ts e="T898" id="Seg_696" n="HIAT:u" s="T894">
                  <ts e="T895" id="Seg_698" n="HIAT:w" s="T894">Так</ts>
                  <nts id="Seg_699" n="HIAT:ip">,</nts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_702" n="HIAT:w" s="T895">ну</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_705" n="HIAT:w" s="T896">что</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_708" n="HIAT:w" s="T897">ж</ts>
                  <nts id="Seg_709" n="HIAT:ip">.</nts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T909" id="Seg_712" n="HIAT:u" s="T898">
                  <ts e="T898.tx-KA.1" id="Seg_714" n="HIAT:w" s="T898">Это</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898.tx-KA.2" id="Seg_717" n="HIAT:w" s="T898.tx-KA.1">трудно</ts>
                  <nts id="Seg_718" n="HIAT:ip">,</nts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898.tx-KA.3" id="Seg_721" n="HIAT:w" s="T898.tx-KA.2">конечно</ts>
                  <nts id="Seg_722" n="HIAT:ip">,</nts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898.tx-KA.4" id="Seg_725" n="HIAT:w" s="T898.tx-KA.3">но</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898.tx-KA.5" id="Seg_728" n="HIAT:w" s="T898.tx-KA.4">придется</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898.tx-KA.6" id="Seg_731" n="HIAT:w" s="T898.tx-KA.5">все</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898.tx-KA.7" id="Seg_734" n="HIAT:w" s="T898.tx-KA.6">повторять</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_736" n="HIAT:ip">(</nts>
                  <ts e="T898.tx-KA.8" id="Seg_738" n="HIAT:w" s="T898.tx-KA.7">ею</ts>
                  <nts id="Seg_739" n="HIAT:ip">)</nts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_742" n="HIAT:w" s="T898.tx-KA.8">по-своему</ts>
                  <nts id="Seg_743" n="HIAT:ip">.</nts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T931" id="Seg_746" n="HIAT:u" s="T909">
                  <ts e="T909.tx-KA.1" id="Seg_748" n="HIAT:w" s="T909">Ты</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909.tx-KA.2" id="Seg_751" n="HIAT:w" s="T909.tx-KA.1">сначала</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909.tx-KA.3" id="Seg_754" n="HIAT:w" s="T909.tx-KA.2">рассказала</ts>
                  <nts id="Seg_755" n="HIAT:ip">,</nts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909.tx-KA.4" id="Seg_758" n="HIAT:w" s="T909.tx-KA.3">значит</ts>
                  <nts id="Seg_759" n="HIAT:ip">,</nts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909.tx-KA.5" id="Seg_762" n="HIAT:w" s="T909.tx-KA.4">насчет</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909.tx-KA.6" id="Seg_765" n="HIAT:w" s="T909.tx-KA.5">того</ts>
                  <nts id="Seg_766" n="HIAT:ip">,</nts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909.tx-KA.7" id="Seg_769" n="HIAT:w" s="T909.tx-KA.6">как</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909.tx-KA.8" id="Seg_772" n="HIAT:w" s="T909.tx-KA.7">с</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909.tx-KA.9" id="Seg_775" n="HIAT:w" s="T909.tx-KA.8">Фридой</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909.tx-KA.10" id="Seg_778" n="HIAT:w" s="T909.tx-KA.9">ждали</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909.tx-KA.11" id="Seg_781" n="HIAT:w" s="T909.tx-KA.10">Арпада</ts>
                  <nts id="Seg_782" n="HIAT:ip">,</nts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909.tx-KA.12" id="Seg_785" n="HIAT:w" s="T909.tx-KA.11">чтобы</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909.tx-KA.13" id="Seg_788" n="HIAT:w" s="T909.tx-KA.12">туда</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909.tx-KA.14" id="Seg_791" n="HIAT:w" s="T909.tx-KA.13">не</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909.tx-KA.15" id="Seg_794" n="HIAT:w" s="T909.tx-KA.14">ехать</ts>
                  <nts id="Seg_795" n="HIAT:ip">,</nts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909.tx-KA.16" id="Seg_798" n="HIAT:w" s="T909.tx-KA.15">а</ts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909.tx-KA.17" id="Seg_801" n="HIAT:w" s="T909.tx-KA.16">потом</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909.tx-KA.18" id="Seg_804" n="HIAT:w" s="T909.tx-KA.17">уже</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909.tx-KA.19" id="Seg_807" n="HIAT:w" s="T909.tx-KA.18">когда</ts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909.tx-KA.20" id="Seg_810" n="HIAT:w" s="T909.tx-KA.19">поехали</ts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T931" id="Seg_813" n="HIAT:w" s="T909.tx-KA.20">с</ts>
                  <nts id="Seg_814" n="HIAT:ip">…</nts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T943" id="Seg_816" n="sc" s="T935">
               <ts e="T943" id="Seg_818" n="HIAT:u" s="T935">
                  <ts e="T939" id="Seg_820" n="HIAT:w" s="T935">Ага</ts>
                  <nts id="Seg_821" n="HIAT:ip">,</nts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T940" id="Seg_824" n="HIAT:w" s="T939">ага</ts>
                  <nts id="Seg_825" n="HIAT:ip">,</nts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_828" n="HIAT:w" s="T940">хорошо</ts>
                  <nts id="Seg_829" n="HIAT:ip">,</nts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_832" n="HIAT:w" s="T941">ну</ts>
                  <nts id="Seg_833" n="HIAT:ip">…</nts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KA">
            <ts e="T3" id="Seg_835" n="sc" s="T0">
               <ts e="T1" id="Seg_837" n="e" s="T0">Ago Künnap haastelee Klaudija Plotnikovaa, 75-vuotias, kotoisin Abalakovon kylästä, Länsi-Siperiasta, Sajaanin vuoristolta. </ts>
               <ts e="T2" id="Seg_839" n="e" s="T1">21.08.1970. </ts>
               <ts e="T3" id="Seg_841" n="e" s="T2">Прошу. </ts>
            </ts>
            <ts e="T150" id="Seg_842" n="sc" s="T124">
               <ts e="T125" id="Seg_844" n="e" s="T124">((…)). </ts>
               <ts e="T126" id="Seg_846" n="e" s="T125">Так. </ts>
               <ts e="T132" id="Seg_848" n="e" s="T126">Это было очень интересно, прекрасно услышать. </ts>
               <ts e="T142" id="Seg_850" n="e" s="T132">Ну как, может, тогда расскажешь, как сегодня ходила у нас? </ts>
               <ts e="T150" id="Seg_852" n="e" s="T142">Как понравилось, какие люди ((…)), что и как. </ts>
            </ts>
            <ts e="T160" id="Seg_853" n="sc" s="T154">
               <ts e="T160" id="Seg_855" n="e" s="T154">Тебе (очень) понравилось, нет? </ts>
            </ts>
            <ts e="T166" id="Seg_856" n="sc" s="T161">
               <ts e="T166" id="Seg_858" n="e" s="T161">Как понравилось там, на Конгрессе? </ts>
            </ts>
            <ts e="T173" id="Seg_859" n="sc" s="T168">
               <ts e="T169" id="Seg_861" n="e" s="T168">Ничего? </ts>
               <ts e="T173" id="Seg_863" n="e" s="T169">Люди добрые, не плохие? </ts>
            </ts>
            <ts e="T191" id="Seg_864" n="sc" s="T177">
               <ts e="T178" id="Seg_866" n="e" s="T177">Да, </ts>
               <ts e="T179" id="Seg_868" n="e" s="T178">они </ts>
               <ts e="T180" id="Seg_870" n="e" s="T179">были </ts>
               <ts e="T181" id="Seg_872" n="e" s="T180">очень </ts>
               <ts e="T182" id="Seg_874" n="e" s="T181">рады, </ts>
               <ts e="T183" id="Seg_876" n="e" s="T182">что </ts>
               <ts e="T184" id="Seg_878" n="e" s="T183">ты </ts>
               <ts e="T185" id="Seg_880" n="e" s="T184">приехала. </ts>
               <ts e="T191" id="Seg_882" n="e" s="T185">Они долго тебя уже там ждали. </ts>
            </ts>
            <ts e="T214" id="Seg_883" n="sc" s="T196">
               <ts e="T206" id="Seg_885" n="e" s="T196">Каждый день меня расспрашивали, когда это наша последняя камасинка придет. </ts>
               <ts e="T212" id="Seg_887" n="e" s="T206">Я говорю: "Подождите, подождите, сейчас, сейчас". </ts>
               <ts e="T214" id="Seg_889" n="e" s="T212">Ннн… </ts>
            </ts>
            <ts e="T232" id="Seg_890" n="sc" s="T219">
               <ts e="T232" id="Seg_892" n="e" s="T219">Ну давай сначала по-русски расскажи, а потом переведешь на камасинский. </ts>
            </ts>
            <ts e="T248" id="Seg_893" n="sc" s="T236">
               <ts e="T237" id="Seg_895" n="e" s="T236">Да, </ts>
               <ts e="T238" id="Seg_897" n="e" s="T237">работает. </ts>
               <ts e="T243" id="Seg_899" n="e" s="T238">У них пленки сколько угодно. </ts>
               <ts e="T244" id="Seg_901" n="e" s="T243">Все, </ts>
               <ts e="T245" id="Seg_903" n="e" s="T244">что </ts>
               <ts e="T246" id="Seg_905" n="e" s="T245">болтаем, </ts>
               <ts e="T247" id="Seg_907" n="e" s="T246">пускай </ts>
               <ts e="T248" id="Seg_909" n="e" s="T247">пишут. </ts>
            </ts>
            <ts e="T368" id="Seg_910" n="sc" s="T365">
               <ts e="T368" id="Seg_912" n="e" s="T365">Ага. </ts>
            </ts>
            <ts e="T565" id="Seg_913" n="sc" s="T500">
               <ts e="T501" id="Seg_915" n="e" s="T500">Ага, хорошо. </ts>
               <ts e="T509" id="Seg_917" n="e" s="T501">Ну так это, было очень интересно послушать, конечно. </ts>
               <ts e="T517" id="Seg_919" n="e" s="T509">Ну вот, тут вчера Арпад ведь поздно приезжал. </ts>
               <ts e="T518" id="Seg_921" n="e" s="T517">Может, </ts>
               <ts e="T519" id="Seg_923" n="e" s="T518">об </ts>
               <ts e="T520" id="Seg_925" n="e" s="T519">этом </ts>
               <ts e="T521" id="Seg_927" n="e" s="T520">расскажешь: </ts>
               <ts e="T522" id="Seg_929" n="e" s="T521">как </ts>
               <ts e="T523" id="Seg_931" n="e" s="T522">он </ts>
               <ts e="T524" id="Seg_933" n="e" s="T523">приехал, </ts>
               <ts e="T525" id="Seg_935" n="e" s="T524">что </ts>
               <ts e="T527" id="Seg_937" n="e" s="T525">говорил… </ts>
               <ts e="T539" id="Seg_939" n="e" s="T527">Как договорились, что сегодня поедем там на конгресс еще (такое), на конференцию. </ts>
               <ts e="T543" id="Seg_941" n="e" s="T539">Тоже было бы интересно. </ts>
               <ts e="T549" id="Seg_943" n="e" s="T543">Сначала опять можно по-русски. </ts>
               <ts e="T565" id="Seg_945" n="e" s="T549">Просто расскажи по-русски, как (все) было, а потом уж подумаешь, поговоришь по-своему. </ts>
            </ts>
            <ts e="T864" id="Seg_946" n="sc" s="T861">
               <ts e="T862" id="Seg_948" n="e" s="T861">Нет, </ts>
               <ts e="T863" id="Seg_950" n="e" s="T862">не </ts>
               <ts e="T864" id="Seg_952" n="e" s="T863">был. </ts>
            </ts>
            <ts e="T878" id="Seg_953" n="sc" s="T876">
               <ts e="T877" id="Seg_955" n="e" s="T876">Ага. </ts>
               <ts e="T878" id="Seg_957" n="e" s="T877">Ага. </ts>
            </ts>
            <ts e="T892" id="Seg_958" n="sc" s="T891">
               <ts e="T892" id="Seg_960" n="e" s="T891">Ага. </ts>
            </ts>
            <ts e="T931" id="Seg_961" n="sc" s="T894">
               <ts e="T895" id="Seg_963" n="e" s="T894">Так, </ts>
               <ts e="T896" id="Seg_965" n="e" s="T895">ну </ts>
               <ts e="T897" id="Seg_967" n="e" s="T896">что </ts>
               <ts e="T898" id="Seg_969" n="e" s="T897">ж. </ts>
               <ts e="T909" id="Seg_971" n="e" s="T898">Это трудно, конечно, но придется все повторять (ею) по-своему. </ts>
               <ts e="T931" id="Seg_973" n="e" s="T909">Ты сначала рассказала, значит, насчет того, как с Фридой ждали Арпада, чтобы туда не ехать, а потом уже когда поехали с… </ts>
            </ts>
            <ts e="T943" id="Seg_974" n="sc" s="T935">
               <ts e="T939" id="Seg_976" n="e" s="T935">Ага, </ts>
               <ts e="T940" id="Seg_978" n="e" s="T939">ага, </ts>
               <ts e="T941" id="Seg_980" n="e" s="T940">хорошо, </ts>
               <ts e="T943" id="Seg_982" n="e" s="T941">ну… </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KA">
            <ta e="T1" id="Seg_983" s="T0">PKZ_19700821_09339-2a.KA.001 (001)</ta>
            <ta e="T2" id="Seg_984" s="T1">PKZ_19700821_09339-2a.KA.002 (002)</ta>
            <ta e="T3" id="Seg_985" s="T2">PKZ_19700821_09339-2a.KA.003 (003)</ta>
            <ta e="T125" id="Seg_986" s="T124">PKZ_19700821_09339-2a.KA.004 (017)</ta>
            <ta e="T126" id="Seg_987" s="T125">PKZ_19700821_09339-2a.KA.005 (018)</ta>
            <ta e="T132" id="Seg_988" s="T126">PKZ_19700821_09339-2a.KA.006 (019)</ta>
            <ta e="T142" id="Seg_989" s="T132">PKZ_19700821_09339-2a.KA.007 (020)</ta>
            <ta e="T150" id="Seg_990" s="T142">PKZ_19700821_09339-2a.KA.008 (021)</ta>
            <ta e="T160" id="Seg_991" s="T154">PKZ_19700821_09339-2a.KA.009 (024)</ta>
            <ta e="T166" id="Seg_992" s="T161">PKZ_19700821_09339-2a.KA.010 (026)</ta>
            <ta e="T169" id="Seg_993" s="T168">PKZ_19700821_09339-2a.KA.011 (028)</ta>
            <ta e="T173" id="Seg_994" s="T169">PKZ_19700821_09339-2a.KA.012 (029)</ta>
            <ta e="T185" id="Seg_995" s="T177">PKZ_19700821_09339-2a.KA.013 (031)</ta>
            <ta e="T191" id="Seg_996" s="T185">PKZ_19700821_09339-2a.KA.014 (032)</ta>
            <ta e="T206" id="Seg_997" s="T196">PKZ_19700821_09339-2a.KA.015 (034)</ta>
            <ta e="T212" id="Seg_998" s="T206">PKZ_19700821_09339-2a.KA.016 (035)</ta>
            <ta e="T214" id="Seg_999" s="T212">PKZ_19700821_09339-2a.KA.017 (036)</ta>
            <ta e="T232" id="Seg_1000" s="T219">PKZ_19700821_09339-2a.KA.018 (038)</ta>
            <ta e="T238" id="Seg_1001" s="T236">PKZ_19700821_09339-2a.KA.019 (040)</ta>
            <ta e="T243" id="Seg_1002" s="T238">PKZ_19700821_09339-2a.KA.020 (041)</ta>
            <ta e="T248" id="Seg_1003" s="T243">PKZ_19700821_09339-2a.KA.021 (042)</ta>
            <ta e="T368" id="Seg_1004" s="T365">PKZ_19700821_09339-2a.KA.022 (060)</ta>
            <ta e="T501" id="Seg_1005" s="T500">PKZ_19700821_09339-2a.KA.023 (085)</ta>
            <ta e="T509" id="Seg_1006" s="T501">PKZ_19700821_09339-2a.KA.024 (086)</ta>
            <ta e="T517" id="Seg_1007" s="T509">PKZ_19700821_09339-2a.KA.025 (087)</ta>
            <ta e="T527" id="Seg_1008" s="T517">PKZ_19700821_09339-2a.KA.026 (088)</ta>
            <ta e="T539" id="Seg_1009" s="T527">PKZ_19700821_09339-2a.KA.027 (089)</ta>
            <ta e="T543" id="Seg_1010" s="T539">PKZ_19700821_09339-2a.KA.028 (090)</ta>
            <ta e="T549" id="Seg_1011" s="T543">PKZ_19700821_09339-2a.KA.029 (091)</ta>
            <ta e="T565" id="Seg_1012" s="T549">PKZ_19700821_09339-2a.KA.030 (092)</ta>
            <ta e="T864" id="Seg_1013" s="T861">PKZ_19700821_09339-2a.KA.031 (126)</ta>
            <ta e="T877" id="Seg_1014" s="T876">PKZ_19700821_09339-2a.KA.032 (128)</ta>
            <ta e="T878" id="Seg_1015" s="T877">PKZ_19700821_09339-2a.KA.033 (129)</ta>
            <ta e="T892" id="Seg_1016" s="T891">PKZ_19700821_09339-2a.KA.034 (132)</ta>
            <ta e="T898" id="Seg_1017" s="T894">PKZ_19700821_09339-2a.KA.035 (134)</ta>
            <ta e="T909" id="Seg_1018" s="T898">PKZ_19700821_09339-2a.KA.036 (135)</ta>
            <ta e="T931" id="Seg_1019" s="T909">PKZ_19700821_09339-2a.KA.037 (136)</ta>
            <ta e="T943" id="Seg_1020" s="T935">PKZ_19700821_09339-2a.KA.038 (138)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KA">
            <ta e="T1" id="Seg_1021" s="T0">Ago Künnap haastelee Klaudija Plotnikovaa, 75-vuotias, kotoisin Abalakovon kylästä, Länsi-Siperiasta, Sajaanin vuoristolta.</ta>
            <ta e="T2" id="Seg_1022" s="T1">21.08.1970.</ta>
            <ta e="T3" id="Seg_1023" s="T2">Прошу. </ta>
            <ta e="T125" id="Seg_1024" s="T124">((…)). </ta>
            <ta e="T126" id="Seg_1025" s="T125">Так. </ta>
            <ta e="T132" id="Seg_1026" s="T126">Это было очень интересно, прекрасно услышать. </ta>
            <ta e="T142" id="Seg_1027" s="T132">Ну как, может, тогда расскажешь, как сегодня ходила у нас? </ta>
            <ta e="T150" id="Seg_1028" s="T142">Как понравилось, какие люди ((…)), что и как. </ta>
            <ta e="T160" id="Seg_1029" s="T154">Тебе (очень) понравилось, нет? </ta>
            <ta e="T166" id="Seg_1030" s="T161">Как понравилось там, на Конгрессе? </ta>
            <ta e="T169" id="Seg_1031" s="T168">Ничего? </ta>
            <ta e="T173" id="Seg_1032" s="T169">Люди добрые, не плохие? </ta>
            <ta e="T185" id="Seg_1033" s="T177">Да, они были очень рады, что ты приехала. </ta>
            <ta e="T191" id="Seg_1034" s="T185">Они долго тебя уже там ждали. </ta>
            <ta e="T206" id="Seg_1035" s="T196">Каждый день меня расспрашивали, когда это наша последняя камасинка придет. </ta>
            <ta e="T212" id="Seg_1036" s="T206">Я говорю: "Подождите, подождите, сейчас, сейчас". </ta>
            <ta e="T214" id="Seg_1037" s="T212">Ннн… </ta>
            <ta e="T232" id="Seg_1038" s="T219">Ну давай сначала по-русски расскажи, а потом переведешь на камасинский. </ta>
            <ta e="T238" id="Seg_1039" s="T236">Да, работает. </ta>
            <ta e="T243" id="Seg_1040" s="T238">У них пленки сколько угодно. </ta>
            <ta e="T248" id="Seg_1041" s="T243">Все, что болтаем, пускай пишут. </ta>
            <ta e="T368" id="Seg_1042" s="T365">Ага. </ta>
            <ta e="T501" id="Seg_1043" s="T500">Ага, хорошо. </ta>
            <ta e="T509" id="Seg_1044" s="T501">Ну так это, было очень интересно послушать, конечно. </ta>
            <ta e="T517" id="Seg_1045" s="T509">Ну вот, тут вчера Арпад ведь поздно приезжал. </ta>
            <ta e="T527" id="Seg_1046" s="T517">Может, об этом расскажешь: как он приехал, что говорил … </ta>
            <ta e="T539" id="Seg_1047" s="T527">Как договорились, что сегодня поедем там на конгресс еще (такое), на конференцию. </ta>
            <ta e="T543" id="Seg_1048" s="T539">Тоже было бы интересно. </ta>
            <ta e="T549" id="Seg_1049" s="T543">Сначала опять можно по-русски. </ta>
            <ta e="T565" id="Seg_1050" s="T549">Просто расскажи по-русски, как (все) было, а потом уж подумаешь, поговоришь по-своему. </ta>
            <ta e="T864" id="Seg_1051" s="T861">Нет, не был. </ta>
            <ta e="T877" id="Seg_1052" s="T876">Ага. </ta>
            <ta e="T878" id="Seg_1053" s="T877">Ага. </ta>
            <ta e="T892" id="Seg_1054" s="T891">Ага. </ta>
            <ta e="T898" id="Seg_1055" s="T894">Так, ну что ж. </ta>
            <ta e="T909" id="Seg_1056" s="T898">Это трудно, конечно, но придется все повторять (ею) по-своему. </ta>
            <ta e="T931" id="Seg_1057" s="T909">Ты сначала рассказала, значит, насчет того, как с Фридой ждали Арпада, чтобы туда не ехать, а потом уже когда поехали с… </ta>
            <ta e="T943" id="Seg_1058" s="T935">Ага, ага, хорошо, ну … </ta>
         </annotation>
         <annotation name="CS" tierref="CS-KA">
            <ta e="T1" id="Seg_1059" s="T0">FIN:ext</ta>
            <ta e="T2" id="Seg_1060" s="T1">FIN:ext </ta>
            <ta e="T3" id="Seg_1061" s="T2">RUS:ext</ta>
            <ta e="T125" id="Seg_1062" s="T124">FIN:ext</ta>
            <ta e="T126" id="Seg_1063" s="T125">RUS:ext</ta>
            <ta e="T132" id="Seg_1064" s="T126">RUS:ext</ta>
            <ta e="T142" id="Seg_1065" s="T132">RUS:ext</ta>
            <ta e="T150" id="Seg_1066" s="T142">RUS:ext</ta>
            <ta e="T160" id="Seg_1067" s="T154">RUS:ext</ta>
            <ta e="T166" id="Seg_1068" s="T161">RUS:ext</ta>
            <ta e="T169" id="Seg_1069" s="T168">RUS:ext</ta>
            <ta e="T173" id="Seg_1070" s="T169">RUS:ext</ta>
            <ta e="T185" id="Seg_1071" s="T177">RUS:ext</ta>
            <ta e="T191" id="Seg_1072" s="T185">RUS:ext</ta>
            <ta e="T206" id="Seg_1073" s="T196">RUS:ext</ta>
            <ta e="T212" id="Seg_1074" s="T206">RUS:ext</ta>
            <ta e="T214" id="Seg_1075" s="T212">RUS:ext</ta>
            <ta e="T232" id="Seg_1076" s="T219">RUS:ext</ta>
            <ta e="T238" id="Seg_1077" s="T236">RUS:ext</ta>
            <ta e="T243" id="Seg_1078" s="T238">RUS:ext</ta>
            <ta e="T248" id="Seg_1079" s="T243">RUS:ext</ta>
            <ta e="T368" id="Seg_1080" s="T365">RUS:ext</ta>
            <ta e="T501" id="Seg_1081" s="T500">RUS:ext</ta>
            <ta e="T509" id="Seg_1082" s="T501">RUS:ext</ta>
            <ta e="T517" id="Seg_1083" s="T509">RUS:ext</ta>
            <ta e="T527" id="Seg_1084" s="T517">RUS:ext</ta>
            <ta e="T539" id="Seg_1085" s="T527">RUS:ext</ta>
            <ta e="T543" id="Seg_1086" s="T539">RUS:ext</ta>
            <ta e="T549" id="Seg_1087" s="T543">RUS:ext</ta>
            <ta e="T565" id="Seg_1088" s="T549">RUS:ext</ta>
            <ta e="T864" id="Seg_1089" s="T861">RUS:ext</ta>
            <ta e="T877" id="Seg_1090" s="T876">RUS:ext</ta>
            <ta e="T878" id="Seg_1091" s="T877">RUS:ext</ta>
            <ta e="T892" id="Seg_1092" s="T891">RUS:ext</ta>
            <ta e="T898" id="Seg_1093" s="T894">RUS:ext</ta>
            <ta e="T909" id="Seg_1094" s="T898">RUS:ext</ta>
            <ta e="T931" id="Seg_1095" s="T909">RUS:ext</ta>
            <ta e="T943" id="Seg_1096" s="T935">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KA" />
         <annotation name="fe" tierref="fe-KA">
            <ta e="T1" id="Seg_1097" s="T0">Ago Künnap is interviewing Klaudiya Plotnikova, 75 years old, from the village of Abalakovo, from Western Siberia, from the Sayan mountains.</ta>
            <ta e="T2" id="Seg_1098" s="T1">21.08.1970.</ta>
            <ta e="T3" id="Seg_1099" s="T2">Please. </ta>
            <ta e="T126" id="Seg_1100" s="T125">Well.</ta>
            <ta e="T132" id="Seg_1101" s="T126">It was very interesting, great to hear.</ta>
            <ta e="T142" id="Seg_1102" s="T132">Well, maybe then tell me how today you went with us?</ta>
            <ta e="T150" id="Seg_1103" s="T142">What did you like, which people (…), what and how.</ta>
            <ta e="T160" id="Seg_1104" s="T154">You (really) liked it, no?</ta>
            <ta e="T166" id="Seg_1105" s="T161">How did you like it there at the congress?</ta>
            <ta e="T169" id="Seg_1106" s="T168">Okay?</ta>
            <ta e="T173" id="Seg_1107" s="T169">The people were good, not bad?</ta>
            <ta e="T185" id="Seg_1108" s="T177">Yes, they were very glad you came.</ta>
            <ta e="T191" id="Seg_1109" s="T185">They have been waiting for you there for a long time.</ta>
            <ta e="T206" id="Seg_1110" s="T196">Every day I was asked when is our last Kamssian coming.</ta>
            <ta e="T212" id="Seg_1111" s="T206">I say, "Wait, wait, now, now."</ta>
            <ta e="T214" id="Seg_1112" s="T212">Nnn …</ta>
            <ta e="T232" id="Seg_1113" s="T219">Well, first tell us in Russian, and then translate into Kamass.</ta>
            <ta e="T238" id="Seg_1114" s="T236">Yes it works.</ta>
            <ta e="T243" id="Seg_1115" s="T238">They have any number of tampes.</ta>
            <ta e="T248" id="Seg_1116" s="T243">Everything we say will be recorded.</ta>
            <ta e="T368" id="Seg_1117" s="T365">Yes.</ta>
            <ta e="T501" id="Seg_1118" s="T500">Yes, well.</ta>
            <ta e="T509" id="Seg_1119" s="T501">Well, it was very interesting to listen, of course.</ta>
            <ta e="T517" id="Seg_1120" s="T509">Well, here yesterday Arpad came too late.</ta>
            <ta e="T527" id="Seg_1121" s="T517">Maybe you can talk about it: how he arrived, what he said …</ta>
            <ta e="T539" id="Seg_1122" s="T527">As agreed, today we’ll go there for the congress (such), to the conference.</ta>
            <ta e="T543" id="Seg_1123" s="T539">It would also be interesting.</ta>
            <ta e="T549" id="Seg_1124" s="T543">First again, you can start in Russian.</ta>
            <ta e="T565" id="Seg_1125" s="T549">Just tell in Russian how (everything) was, and then think about it, talk in your own way.</ta>
            <ta e="T864" id="Seg_1126" s="T861">No, I haven't been there.</ta>
            <ta e="T877" id="Seg_1127" s="T876">Yes.</ta>
            <ta e="T878" id="Seg_1128" s="T877">Yes.</ta>
            <ta e="T892" id="Seg_1129" s="T891">Yes.</ta>
            <ta e="T898" id="Seg_1130" s="T894">Well then.</ta>
            <ta e="T909" id="Seg_1131" s="T898">This is difficult, of course, but you will have to repeated everything in your own way.</ta>
            <ta e="T931" id="Seg_1132" s="T909">You first told me, then, about how you waited with Frida for Arpad, er did not come, and then when you went with …</ta>
            <ta e="T943" id="Seg_1133" s="T935">Yes, yes, well then…</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KA">
            <ta e="T1" id="Seg_1134" s="T0">Ago Künnap interviewt Klaudija Plotnikova, 75-jährig, aus dem Dorf Abalakovo, aus Westsibirien, aus dem Sajangebirge.</ta>
            <ta e="T2" id="Seg_1135" s="T1">21.08.1970.</ta>
            <ta e="T3" id="Seg_1136" s="T2">Bitte. </ta>
            <ta e="T126" id="Seg_1137" s="T125">Nun.</ta>
            <ta e="T132" id="Seg_1138" s="T126">Es war sehr interessant zu hören.</ta>
            <ta e="T142" id="Seg_1139" s="T132">Nun denn, vielleicht kannst du mir dann erzählen wie du heute mit uns gegangen bist. </ta>
            <ta e="T150" id="Seg_1140" s="T142">Was hat die gefallen, welche Menschen (…), was und wie.</ta>
            <ta e="T160" id="Seg_1141" s="T154">Es hat die (sehr) gefallen, nicht?</ta>
            <ta e="T166" id="Seg_1142" s="T161">Wie hat es dir auf dem Kongress gefallen?</ta>
            <ta e="T169" id="Seg_1143" s="T168">Einigermaßen?</ta>
            <ta e="T173" id="Seg_1144" s="T169">Die Leute waren gut, nicht schlecht?</ta>
            <ta e="T185" id="Seg_1145" s="T177">Ja, sie waren sehr froh, dass du gekommen bist.</ta>
            <ta e="T191" id="Seg_1146" s="T185">Sie haben lange auf dich gewartet.</ta>
            <ta e="T206" id="Seg_1147" s="T196">Jeden Tag wurde ich gefragt, wann unsere letzte Kamassischsprecherin kommen würde. </ta>
            <ta e="T212" id="Seg_1148" s="T206">Ich sagte: "Wartet, wartet, jetzt, jetzt".</ta>
            <ta e="T214" id="Seg_1149" s="T212">Hmm…</ta>
            <ta e="T232" id="Seg_1150" s="T219">Nun, lass uns auf Russisch beginnen und dann übersetzt du ins Kamassische.</ta>
            <ta e="T238" id="Seg_1151" s="T236">Ja, er ist an.</ta>
            <ta e="T243" id="Seg_1152" s="T238">Wir haben so viele Bänder wie wir wollen.</ta>
            <ta e="T248" id="Seg_1153" s="T243">Alles, was wir plaudern, wird aufgezeichnet.</ta>
            <ta e="T368" id="Seg_1154" s="T365">Ja.</ta>
            <ta e="T501" id="Seg_1155" s="T500">Ja, gut.</ta>
            <ta e="T509" id="Seg_1156" s="T501">Nun, das war natürlich sehr interessant zu hören.</ta>
            <ta e="T517" id="Seg_1157" s="T509">Nun, gestern kam Arpad zu spät.</ta>
            <ta e="T527" id="Seg_1158" s="T517">Vielleicht kannst du darüber erzählen: wie er angekommen ist, was er gesagt hat …</ta>
            <ta e="T539" id="Seg_1159" s="T527">Wie vereinbart, werden wir heute zum Kongress dorthin gehen, zur Konferenz.</ta>
            <ta e="T543" id="Seg_1160" s="T539">Es wäre auch interessant.</ta>
            <ta e="T549" id="Seg_1161" s="T543">Erst kannst du noch mal auf Russisch anfangen.</ta>
            <ta e="T565" id="Seg_1162" s="T549">Sag es einfach auf Russisch, wie (alles) war, und dann denk nach und sag es auf deine eigene Weise.</ta>
            <ta e="T864" id="Seg_1163" s="T861">Nein, war ich nicht.</ta>
            <ta e="T877" id="Seg_1164" s="T876">Ja.</ta>
            <ta e="T878" id="Seg_1165" s="T877">Ja.</ta>
            <ta e="T892" id="Seg_1166" s="T891">Ja.</ta>
            <ta e="T898" id="Seg_1167" s="T894">Also gut.</ta>
            <ta e="T909" id="Seg_1168" s="T898">Es ist natürlich schwierig, aber du musst alles auf Ihre eigene Art und Weise wiederholen.</ta>
            <ta e="T931" id="Seg_1169" s="T909">Du hast angefangen zu erzählen, dass du mit Frida auf Arpad gewartet hast, er ist nicht gekommen, und dann seid ihr gegangen mit… </ta>
            <ta e="T943" id="Seg_1170" s="T935">Ja, ja, gut, nun…</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KA" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-PKZ"
                      id="tx-PKZ"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T8" start="T3">
            <tli id="T3.tx-PKZ.1" />
            <tli id="T3.tx-PKZ.2" />
            <tli id="T3.tx-PKZ.3" />
            <tli id="T3.tx-PKZ.4" />
         </timeline-fork>
         <timeline-fork end="T20" start="T8">
            <tli id="T8.tx-PKZ.1" />
            <tli id="T8.tx-PKZ.2" />
            <tli id="T8.tx-PKZ.3" />
            <tli id="T8.tx-PKZ.4" />
            <tli id="T8.tx-PKZ.5" />
            <tli id="T8.tx-PKZ.6" />
            <tli id="T8.tx-PKZ.7" />
            <tli id="T8.tx-PKZ.8" />
            <tli id="T8.tx-PKZ.9" />
            <tli id="T8.tx-PKZ.10" />
         </timeline-fork>
         <timeline-fork end="T64" start="T49">
            <tli id="T49.tx-PKZ.1" />
            <tli id="T49.tx-PKZ.2" />
            <tli id="T49.tx-PKZ.3" />
            <tli id="T49.tx-PKZ.4" />
            <tli id="T49.tx-PKZ.5" />
            <tli id="T49.tx-PKZ.6" />
            <tli id="T49.tx-PKZ.7" />
            <tli id="T49.tx-PKZ.8" />
            <tli id="T49.tx-PKZ.9" />
            <tli id="T49.tx-PKZ.10" />
            <tli id="T49.tx-PKZ.11" />
            <tli id="T49.tx-PKZ.12" />
            <tli id="T49.tx-PKZ.13" />
            <tli id="T49.tx-PKZ.14" />
         </timeline-fork>
         <timeline-fork end="T84" start="T64">
            <tli id="T64.tx-PKZ.1" />
            <tli id="T64.tx-PKZ.2" />
            <tli id="T64.tx-PKZ.3" />
            <tli id="T64.tx-PKZ.4" />
            <tli id="T64.tx-PKZ.5" />
            <tli id="T64.tx-PKZ.6" />
            <tli id="T64.tx-PKZ.7" />
            <tli id="T64.tx-PKZ.8" />
            <tli id="T64.tx-PKZ.9" />
            <tli id="T64.tx-PKZ.10" />
            <tli id="T64.tx-PKZ.11" />
            <tli id="T64.tx-PKZ.12" />
            <tli id="T64.tx-PKZ.13" />
            <tli id="T64.tx-PKZ.14" />
            <tli id="T64.tx-PKZ.15" />
            <tli id="T64.tx-PKZ.16" />
            <tli id="T64.tx-PKZ.17" />
         </timeline-fork>
         <timeline-fork end="T90" start="T89">
            <tli id="T89.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T107" start="T106">
            <tli id="T106.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T123" start="T122">
            <tli id="T122.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T149" start="T147">
            <tli id="T147.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T156" start="T153">
            <tli id="T153.tx-PKZ.1" />
            <tli id="T153.tx-PKZ.2" />
         </timeline-fork>
         <timeline-fork end="T177" start="T172">
            <tli id="T172.tx-PKZ.1" />
            <tli id="T172.tx-PKZ.2" />
            <tli id="T172.tx-PKZ.3" />
         </timeline-fork>
         <timeline-fork end="T220" start="T214">
            <tli id="T214.tx-PKZ.1" />
            <tli id="T214.tx-PKZ.2" />
            <tli id="T214.tx-PKZ.3" />
         </timeline-fork>
         <timeline-fork end="T236" start="T232">
            <tli id="T232.tx-PKZ.1" />
            <tli id="T232.tx-PKZ.2" />
            <tli id="T232.tx-PKZ.3" />
         </timeline-fork>
         <timeline-fork end="T262" start="T248">
            <tli id="T248.tx-PKZ.1" />
            <tli id="T248.tx-PKZ.2" />
            <tli id="T248.tx-PKZ.3" />
            <tli id="T248.tx-PKZ.4" />
            <tli id="T248.tx-PKZ.5" />
            <tli id="T248.tx-PKZ.6" />
            <tli id="T248.tx-PKZ.7" />
            <tli id="T248.tx-PKZ.8" />
            <tli id="T248.tx-PKZ.9" />
         </timeline-fork>
         <timeline-fork end="T268" start="T262">
            <tli id="T262.tx-PKZ.1" />
            <tli id="T262.tx-PKZ.2" />
            <tli id="T262.tx-PKZ.3" />
            <tli id="T262.tx-PKZ.4" />
         </timeline-fork>
         <timeline-fork end="T272" start="T268">
            <tli id="T268.tx-PKZ.1" />
            <tli id="T268.tx-PKZ.2" />
            <tli id="T268.tx-PKZ.3" />
         </timeline-fork>
         <timeline-fork end="T296" start="T284">
            <tli id="T284.tx-PKZ.1" />
            <tli id="T284.tx-PKZ.2" />
            <tli id="T284.tx-PKZ.3" />
            <tli id="T284.tx-PKZ.4" />
            <tli id="T284.tx-PKZ.5" />
            <tli id="T284.tx-PKZ.6" />
            <tli id="T284.tx-PKZ.7" />
            <tli id="T284.tx-PKZ.8" />
            <tli id="T284.tx-PKZ.9" />
            <tli id="T284.tx-PKZ.10" />
         </timeline-fork>
         <timeline-fork end="T306" start="T301">
            <tli id="T301.tx-PKZ.1" />
            <tli id="T301.tx-PKZ.2" />
            <tli id="T301.tx-PKZ.3" />
            <tli id="T301.tx-PKZ.4" />
         </timeline-fork>
         <timeline-fork end="T310" start="T306">
            <tli id="T306.tx-PKZ.1" />
            <tli id="T306.tx-PKZ.2" />
            <tli id="T306.tx-PKZ.3" />
         </timeline-fork>
         <timeline-fork end="T317" start="T310">
            <tli id="T310.tx-PKZ.1" />
            <tli id="T310.tx-PKZ.2" />
            <tli id="T310.tx-PKZ.3" />
            <tli id="T310.tx-PKZ.4" />
            <tli id="T310.tx-PKZ.5" />
            <tli id="T310.tx-PKZ.6" />
         </timeline-fork>
         <timeline-fork end="T327" start="T317">
            <tli id="T317.tx-PKZ.1" />
            <tli id="T317.tx-PKZ.2" />
            <tli id="T317.tx-PKZ.3" />
            <tli id="T317.tx-PKZ.4" />
            <tli id="T317.tx-PKZ.5" />
            <tli id="T317.tx-PKZ.6" />
            <tli id="T317.tx-PKZ.7" />
            <tli id="T317.tx-PKZ.8" />
            <tli id="T317.tx-PKZ.9" />
         </timeline-fork>
         <timeline-fork end="T346" start="T328">
            <tli id="T328.tx-PKZ.1" />
            <tli id="T328.tx-PKZ.2" />
            <tli id="T328.tx-PKZ.3" />
            <tli id="T328.tx-PKZ.4" />
            <tli id="T328.tx-PKZ.5" />
            <tli id="T328.tx-PKZ.6" />
            <tli id="T328.tx-PKZ.7" />
            <tli id="T328.tx-PKZ.8" />
            <tli id="T328.tx-PKZ.9" />
            <tli id="T328.tx-PKZ.10" />
            <tli id="T328.tx-PKZ.11" />
            <tli id="T328.tx-PKZ.12" />
            <tli id="T328.tx-PKZ.13" />
            <tli id="T328.tx-PKZ.14" />
            <tli id="T328.tx-PKZ.15" />
            <tli id="T328.tx-PKZ.16" />
            <tli id="T328.tx-PKZ.17" />
         </timeline-fork>
         <timeline-fork end="T350" start="T346">
            <tli id="T346.tx-PKZ.1" />
            <tli id="T346.tx-PKZ.2" />
            <tli id="T346.tx-PKZ.3" />
         </timeline-fork>
         <timeline-fork end="T355" start="T350">
            <tli id="T350.tx-PKZ.1" />
            <tli id="T350.tx-PKZ.2" />
            <tli id="T350.tx-PKZ.3" />
            <tli id="T350.tx-PKZ.4" />
         </timeline-fork>
         <timeline-fork end="T360" start="T355">
            <tli id="T355.tx-PKZ.1" />
            <tli id="T355.tx-PKZ.2" />
            <tli id="T355.tx-PKZ.3" />
            <tli id="T355.tx-PKZ.4" />
         </timeline-fork>
         <timeline-fork end="T373" start="T372">
            <tli id="T372.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T574" start="T565">
            <tli id="T565.tx-PKZ.1" />
            <tli id="T565.tx-PKZ.2" />
            <tli id="T565.tx-PKZ.3" />
            <tli id="T565.tx-PKZ.4" />
            <tli id="T565.tx-PKZ.5" />
            <tli id="T565.tx-PKZ.6" />
         </timeline-fork>
         <timeline-fork end="T583" start="T576">
            <tli id="T576.tx-PKZ.1" />
            <tli id="T576.tx-PKZ.2" />
            <tli id="T576.tx-PKZ.3" />
            <tli id="T576.tx-PKZ.4" />
            <tli id="T576.tx-PKZ.5" />
            <tli id="T576.tx-PKZ.6" />
         </timeline-fork>
         <timeline-fork end="T597" start="T585">
            <tli id="T585.tx-PKZ.1" />
            <tli id="T585.tx-PKZ.2" />
            <tli id="T585.tx-PKZ.3" />
            <tli id="T585.tx-PKZ.4" />
            <tli id="T585.tx-PKZ.5" />
            <tli id="T585.tx-PKZ.6" />
            <tli id="T585.tx-PKZ.7" />
            <tli id="T585.tx-PKZ.8" />
            <tli id="T585.tx-PKZ.9" />
            <tli id="T585.tx-PKZ.10" />
            <tli id="T585.tx-PKZ.11" />
         </timeline-fork>
         <timeline-fork end="T603" start="T597">
            <tli id="T597.tx-PKZ.1" />
            <tli id="T597.tx-PKZ.2" />
            <tli id="T597.tx-PKZ.3" />
            <tli id="T597.tx-PKZ.4" />
            <tli id="T597.tx-PKZ.5" />
         </timeline-fork>
         <timeline-fork end="T609" start="T603">
            <tli id="T603.tx-PKZ.1" />
            <tli id="T603.tx-PKZ.2" />
            <tli id="T603.tx-PKZ.3" />
            <tli id="T603.tx-PKZ.4" />
            <tli id="T603.tx-PKZ.5" />
         </timeline-fork>
         <timeline-fork end="T616" start="T609">
            <tli id="T609.tx-PKZ.1" />
            <tli id="T609.tx-PKZ.2" />
            <tli id="T609.tx-PKZ.3" />
            <tli id="T609.tx-PKZ.4" />
            <tli id="T609.tx-PKZ.5" />
            <tli id="T609.tx-PKZ.6" />
         </timeline-fork>
         <timeline-fork end="T624" start="T616">
            <tli id="T616.tx-PKZ.1" />
            <tli id="T616.tx-PKZ.2" />
            <tli id="T616.tx-PKZ.3" />
            <tli id="T616.tx-PKZ.4" />
            <tli id="T616.tx-PKZ.5" />
            <tli id="T616.tx-PKZ.6" />
            <tli id="T616.tx-PKZ.7" />
         </timeline-fork>
         <timeline-fork end="T629" start="T624">
            <tli id="T624.tx-PKZ.1" />
            <tli id="T624.tx-PKZ.2" />
            <tli id="T624.tx-PKZ.3" />
            <tli id="T624.tx-PKZ.4" />
         </timeline-fork>
         <timeline-fork end="T633" start="T629">
            <tli id="T629.tx-PKZ.1" />
            <tli id="T629.tx-PKZ.2" />
            <tli id="T629.tx-PKZ.3" />
         </timeline-fork>
         <timeline-fork end="T654" start="T647">
            <tli id="T647.tx-PKZ.1" />
            <tli id="T647.tx-PKZ.2" />
            <tli id="T647.tx-PKZ.3" />
            <tli id="T647.tx-PKZ.4" />
            <tli id="T647.tx-PKZ.5" />
            <tli id="T647.tx-PKZ.6" />
         </timeline-fork>
         <timeline-fork end="T667" start="T654">
            <tli id="T654.tx-PKZ.1" />
            <tli id="T654.tx-PKZ.2" />
            <tli id="T654.tx-PKZ.3" />
            <tli id="T654.tx-PKZ.4" />
            <tli id="T654.tx-PKZ.5" />
            <tli id="T654.tx-PKZ.6" />
            <tli id="T654.tx-PKZ.7" />
            <tli id="T654.tx-PKZ.8" />
            <tli id="T654.tx-PKZ.9" />
            <tli id="T654.tx-PKZ.10" />
            <tli id="T654.tx-PKZ.11" />
            <tli id="T654.tx-PKZ.12" />
         </timeline-fork>
         <timeline-fork end="T682" start="T673">
            <tli id="T673.tx-PKZ.1" />
            <tli id="T673.tx-PKZ.2" />
            <tli id="T673.tx-PKZ.3" />
            <tli id="T673.tx-PKZ.4" />
            <tli id="T673.tx-PKZ.5" />
            <tli id="T673.tx-PKZ.6" />
            <tli id="T673.tx-PKZ.7" />
            <tli id="T673.tx-PKZ.8" />
         </timeline-fork>
         <timeline-fork end="T714" start="T700">
            <tli id="T700.tx-PKZ.1" />
            <tli id="T700.tx-PKZ.2" />
            <tli id="T700.tx-PKZ.3" />
            <tli id="T700.tx-PKZ.4" />
            <tli id="T700.tx-PKZ.5" />
            <tli id="T700.tx-PKZ.6" />
            <tli id="T700.tx-PKZ.7" />
            <tli id="T700.tx-PKZ.8" />
            <tli id="T700.tx-PKZ.9" />
            <tli id="T700.tx-PKZ.10" />
            <tli id="T700.tx-PKZ.11" />
         </timeline-fork>
         <timeline-fork end="T733" start="T725">
            <tli id="T725.tx-PKZ.1" />
            <tli id="T725.tx-PKZ.2" />
            <tli id="T725.tx-PKZ.3" />
            <tli id="T725.tx-PKZ.4" />
            <tli id="T725.tx-PKZ.5" />
            <tli id="T725.tx-PKZ.6" />
            <tli id="T725.tx-PKZ.7" />
         </timeline-fork>
         <timeline-fork end="T763" start="T755">
            <tli id="T755.tx-PKZ.1" />
            <tli id="T755.tx-PKZ.2" />
            <tli id="T755.tx-PKZ.3" />
            <tli id="T755.tx-PKZ.4" />
            <tli id="T755.tx-PKZ.5" />
            <tli id="T755.tx-PKZ.6" />
            <tli id="T755.tx-PKZ.7" />
         </timeline-fork>
         <timeline-fork end="T772" start="T763">
            <tli id="T763.tx-PKZ.1" />
            <tli id="T763.tx-PKZ.2" />
            <tli id="T763.tx-PKZ.3" />
            <tli id="T763.tx-PKZ.4" />
            <tli id="T763.tx-PKZ.5" />
            <tli id="T763.tx-PKZ.6" />
            <tli id="T763.tx-PKZ.7" />
            <tli id="T763.tx-PKZ.8" />
         </timeline-fork>
         <timeline-fork end="T781" start="T772">
            <tli id="T772.tx-PKZ.1" />
            <tli id="T772.tx-PKZ.2" />
            <tli id="T772.tx-PKZ.3" />
            <tli id="T772.tx-PKZ.4" />
            <tli id="T772.tx-PKZ.5" />
            <tli id="T772.tx-PKZ.6" />
            <tli id="T772.tx-PKZ.7" />
            <tli id="T772.tx-PKZ.8" />
         </timeline-fork>
         <timeline-fork end="T800" start="T781">
            <tli id="T781.tx-PKZ.1" />
            <tli id="T781.tx-PKZ.2" />
            <tli id="T781.tx-PKZ.3" />
            <tli id="T781.tx-PKZ.4" />
            <tli id="T781.tx-PKZ.5" />
            <tli id="T781.tx-PKZ.6" />
            <tli id="T781.tx-PKZ.7" />
            <tli id="T781.tx-PKZ.8" />
            <tli id="T781.tx-PKZ.9" />
            <tli id="T781.tx-PKZ.10" />
            <tli id="T781.tx-PKZ.11" />
            <tli id="T781.tx-PKZ.12" />
            <tli id="T781.tx-PKZ.13" />
            <tli id="T781.tx-PKZ.14" />
            <tli id="T781.tx-PKZ.15" />
            <tli id="T781.tx-PKZ.16" />
            <tli id="T781.tx-PKZ.17" />
            <tli id="T781.tx-PKZ.18" />
         </timeline-fork>
         <timeline-fork end="T822" start="T800">
            <tli id="T800.tx-PKZ.1" />
            <tli id="T800.tx-PKZ.2" />
            <tli id="T800.tx-PKZ.3" />
            <tli id="T800.tx-PKZ.4" />
            <tli id="T800.tx-PKZ.5" />
            <tli id="T800.tx-PKZ.6" />
            <tli id="T800.tx-PKZ.7" />
            <tli id="T800.tx-PKZ.8" />
            <tli id="T800.tx-PKZ.9" />
            <tli id="T800.tx-PKZ.10" />
            <tli id="T800.tx-PKZ.11" />
            <tli id="T800.tx-PKZ.12" />
            <tli id="T800.tx-PKZ.13" />
            <tli id="T800.tx-PKZ.14" />
            <tli id="T800.tx-PKZ.15" />
            <tli id="T800.tx-PKZ.16" />
            <tli id="T800.tx-PKZ.17" />
            <tli id="T800.tx-PKZ.18" />
            <tli id="T800.tx-PKZ.19" />
            <tli id="T800.tx-PKZ.20" />
            <tli id="T800.tx-PKZ.21" />
         </timeline-fork>
         <timeline-fork end="T832" start="T822">
            <tli id="T822.tx-PKZ.1" />
            <tli id="T822.tx-PKZ.2" />
            <tli id="T822.tx-PKZ.3" />
            <tli id="T822.tx-PKZ.4" />
            <tli id="T822.tx-PKZ.5" />
            <tli id="T822.tx-PKZ.6" />
            <tli id="T822.tx-PKZ.7" />
            <tli id="T822.tx-PKZ.8" />
            <tli id="T822.tx-PKZ.9" />
         </timeline-fork>
         <timeline-fork end="T848" start="T832">
            <tli id="T832.tx-PKZ.1" />
            <tli id="T832.tx-PKZ.2" />
            <tli id="T832.tx-PKZ.3" />
            <tli id="T832.tx-PKZ.4" />
            <tli id="T832.tx-PKZ.5" />
            <tli id="T832.tx-PKZ.6" />
            <tli id="T832.tx-PKZ.7" />
            <tli id="T832.tx-PKZ.8" />
            <tli id="T832.tx-PKZ.9" />
            <tli id="T832.tx-PKZ.10" />
            <tli id="T832.tx-PKZ.11" />
            <tli id="T832.tx-PKZ.12" />
            <tli id="T832.tx-PKZ.13" />
            <tli id="T832.tx-PKZ.14" />
            <tli id="T832.tx-PKZ.15" />
         </timeline-fork>
         <timeline-fork end="T850" start="T848">
            <tli id="T848.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T861" start="T856">
            <tli id="T856.tx-PKZ.1" />
            <tli id="T856.tx-PKZ.2" />
            <tli id="T856.tx-PKZ.3" />
            <tli id="T856.tx-PKZ.4" />
         </timeline-fork>
         <timeline-fork end="T876" start="T864">
            <tli id="T864.tx-PKZ.1" />
            <tli id="T864.tx-PKZ.2" />
            <tli id="T864.tx-PKZ.3" />
            <tli id="T864.tx-PKZ.4" />
            <tli id="T864.tx-PKZ.5" />
            <tli id="T864.tx-PKZ.6" />
            <tli id="T864.tx-PKZ.7" />
            <tli id="T864.tx-PKZ.8" />
            <tli id="T864.tx-PKZ.9" />
            <tli id="T864.tx-PKZ.10" />
            <tli id="T864.tx-PKZ.11" />
         </timeline-fork>
         <timeline-fork end="T891" start="T885">
            <tli id="T885.tx-PKZ.1" />
            <tli id="T885.tx-PKZ.2" />
            <tli id="T885.tx-PKZ.3" />
            <tli id="T885.tx-PKZ.4" />
            <tli id="T885.tx-PKZ.5" />
         </timeline-fork>
         <timeline-fork end="T938" start="T928">
            <tli id="T928.tx-PKZ.1" />
            <tli id="T928.tx-PKZ.2" />
            <tli id="T928.tx-PKZ.3" />
            <tli id="T928.tx-PKZ.4" />
         </timeline-fork>
         <timeline-fork end="T1009" start="T1008">
            <tli id="T1008.tx-PKZ.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PKZ">
            <ts e="T124" id="Seg_1171" n="sc" s="T3">
               <ts e="T8" id="Seg_1173" n="HIAT:u" s="T3">
                  <ts e="T3.tx-PKZ.1" id="Seg_1175" n="HIAT:w" s="T3">Американский</ts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3.tx-PKZ.2" id="Seg_1178" n="HIAT:w" s="T3.tx-PKZ.1">наставник</ts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3.tx-PKZ.3" id="Seg_1181" n="HIAT:w" s="T3.tx-PKZ.2">прислал</ts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3.tx-PKZ.4" id="Seg_1184" n="HIAT:w" s="T3.tx-PKZ.3">нам</ts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_1187" n="HIAT:w" s="T3.tx-PKZ.4">бумагу</ts>
                  <nts id="Seg_1188" n="HIAT:ip">.</nts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_1191" n="HIAT:u" s="T8">
                  <ts e="T8.tx-PKZ.1" id="Seg_1193" n="HIAT:w" s="T8">В</ts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8.tx-PKZ.2" id="Seg_1196" n="HIAT:w" s="T8.tx-PKZ.1">нашей</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8.tx-PKZ.3" id="Seg_1199" n="HIAT:w" s="T8.tx-PKZ.2">местности</ts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8.tx-PKZ.4" id="Seg_1202" n="HIAT:w" s="T8.tx-PKZ.3">гнали</ts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1204" n="HIAT:ip">(</nts>
                  <ts e="T8.tx-PKZ.5" id="Seg_1206" n="HIAT:w" s="T8.tx-PKZ.4">все-</ts>
                  <nts id="Seg_1207" n="HIAT:ip">)</nts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8.tx-PKZ.6" id="Seg_1210" n="HIAT:w" s="T8.tx-PKZ.5">верующих</ts>
                  <nts id="Seg_1211" n="HIAT:ip">,</nts>
                  <nts id="Seg_1212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8.tx-PKZ.7" id="Seg_1214" n="HIAT:w" s="T8.tx-PKZ.6">шибко</ts>
                  <nts id="Seg_1215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8.tx-PKZ.8" id="Seg_1217" n="HIAT:w" s="T8.tx-PKZ.7">притесняли</ts>
                  <nts id="Seg_1218" n="HIAT:ip">,</nts>
                  <nts id="Seg_1219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8.tx-PKZ.9" id="Seg_1221" n="HIAT:w" s="T8.tx-PKZ.8">молиться</ts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8.tx-PKZ.10" id="Seg_1224" n="HIAT:w" s="T8.tx-PKZ.9">не</ts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_1227" n="HIAT:w" s="T8.tx-PKZ.10">давали</ts>
                  <nts id="Seg_1228" n="HIAT:ip">.</nts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_1231" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_1233" n="HIAT:w" s="T20">Ну</ts>
                  <nts id="Seg_1234" n="HIAT:ip">,</nts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_1237" n="HIAT:w" s="T21">он</ts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_1240" n="HIAT:w" s="T22">услыхал</ts>
                  <nts id="Seg_1241" n="HIAT:ip">,</nts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_1244" n="HIAT:w" s="T23">что</ts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_1247" n="HIAT:w" s="T24">так</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_1250" n="HIAT:w" s="T25">тут</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_1253" n="HIAT:w" s="T26">притеснения</ts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_1256" n="HIAT:w" s="T27">в</ts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_1259" n="HIAT:w" s="T28">нашей</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_1262" n="HIAT:w" s="T29">русской</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_1265" n="HIAT:w" s="T30">земле</ts>
                  <nts id="Seg_1266" n="HIAT:ip">,</nts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_1269" n="HIAT:w" s="T31">стал</ts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_1272" n="HIAT:w" s="T32">просить</ts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_1275" n="HIAT:w" s="T33">у</ts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_1278" n="HIAT:w" s="T34">нашего</ts>
                  <nts id="Seg_1279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_1281" n="HIAT:w" s="T35">правителя:</ts>
                  <nts id="Seg_1282" n="HIAT:ip">"</nts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_1285" n="HIAT:w" s="T36">Отдай</ts>
                  <nts id="Seg_1286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_1288" n="HIAT:w" s="T37">мне</ts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_1291" n="HIAT:w" s="T38">верующих</ts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_1294" n="HIAT:w" s="T39">людей</ts>
                  <nts id="Seg_1295" n="HIAT:ip">"</nts>
                  <nts id="Seg_1296" n="HIAT:ip">.</nts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_1299" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_1301" n="HIAT:w" s="T40">Ну</ts>
                  <nts id="Seg_1302" n="HIAT:ip">,</nts>
                  <nts id="Seg_1303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_1305" n="HIAT:w" s="T41">бумага</ts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_1308" n="HIAT:w" s="T42">пришла</ts>
                  <nts id="Seg_1309" n="HIAT:ip">,</nts>
                  <nts id="Seg_1310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_1312" n="HIAT:w" s="T43">даже</ts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_1315" n="HIAT:w" s="T44">у</ts>
                  <nts id="Seg_1316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_1318" n="HIAT:w" s="T45">нас</ts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_1321" n="HIAT:w" s="T46">в</ts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_1324" n="HIAT:w" s="T47">Абалакове</ts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_1327" n="HIAT:w" s="T48">была</ts>
                  <nts id="Seg_1328" n="HIAT:ip">.</nts>
                  <nts id="Seg_1329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_1331" n="HIAT:u" s="T49">
                  <ts e="T49.tx-PKZ.1" id="Seg_1333" n="HIAT:w" s="T49">Стали</ts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49.tx-PKZ.2" id="Seg_1336" n="HIAT:w" s="T49.tx-PKZ.1">писать</ts>
                  <nts id="Seg_1337" n="HIAT:ip">,</nts>
                  <nts id="Seg_1338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49.tx-PKZ.3" id="Seg_1340" n="HIAT:w" s="T49.tx-PKZ.2">и</ts>
                  <nts id="Seg_1341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49.tx-PKZ.4" id="Seg_1343" n="HIAT:w" s="T49.tx-PKZ.3">которые</ts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49.tx-PKZ.5" id="Seg_1346" n="HIAT:w" s="T49.tx-PKZ.4">не</ts>
                  <nts id="Seg_1347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49.tx-PKZ.6" id="Seg_1349" n="HIAT:w" s="T49.tx-PKZ.5">веровали</ts>
                  <nts id="Seg_1350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49.tx-PKZ.7" id="Seg_1352" n="HIAT:w" s="T49.tx-PKZ.6">и</ts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49.tx-PKZ.8" id="Seg_1355" n="HIAT:w" s="T49.tx-PKZ.7">ругали</ts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49.tx-PKZ.9" id="Seg_1358" n="HIAT:w" s="T49.tx-PKZ.8">бога</ts>
                  <nts id="Seg_1359" n="HIAT:ip">,</nts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49.tx-PKZ.10" id="Seg_1362" n="HIAT:w" s="T49.tx-PKZ.9">и</ts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49.tx-PKZ.11" id="Seg_1365" n="HIAT:w" s="T49.tx-PKZ.10">те</ts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49.tx-PKZ.12" id="Seg_1368" n="HIAT:w" s="T49.tx-PKZ.11">записались</ts>
                  <nts id="Seg_1369" n="HIAT:ip">,</nts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49.tx-PKZ.13" id="Seg_1372" n="HIAT:w" s="T49.tx-PKZ.12">что</ts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49.tx-PKZ.14" id="Seg_1375" n="HIAT:w" s="T49.tx-PKZ.13">они</ts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_1378" n="HIAT:w" s="T49.tx-PKZ.14">верующие</ts>
                  <nts id="Seg_1379" n="HIAT:ip">.</nts>
                  <nts id="Seg_1380" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_1382" n="HIAT:u" s="T64">
                  <ts e="T64.tx-PKZ.1" id="Seg_1384" n="HIAT:w" s="T64">Тады</ts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64.tx-PKZ.2" id="Seg_1387" n="HIAT:w" s="T64.tx-PKZ.1">наш</ts>
                  <nts id="Seg_1388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64.tx-PKZ.3" id="Seg_1390" n="HIAT:w" s="T64.tx-PKZ.2">наставник</ts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1392" n="HIAT:ip">(</nts>
                  <ts e="T64.tx-PKZ.4" id="Seg_1394" n="HIAT:w" s="T64.tx-PKZ.3">погляд-</ts>
                  <nts id="Seg_1395" n="HIAT:ip">)</nts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64.tx-PKZ.5" id="Seg_1398" n="HIAT:w" s="T64.tx-PKZ.4">поглядел</ts>
                  <nts id="Seg_1399" n="HIAT:ip">,</nts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64.tx-PKZ.6" id="Seg_1402" n="HIAT:w" s="T64.tx-PKZ.5">что</ts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64.tx-PKZ.7" id="Seg_1405" n="HIAT:w" s="T64.tx-PKZ.6">все</ts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64.tx-PKZ.8" id="Seg_1408" n="HIAT:w" s="T64.tx-PKZ.7">люди</ts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64.tx-PKZ.9" id="Seg_1411" n="HIAT:w" s="T64.tx-PKZ.8">стали</ts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64.tx-PKZ.10" id="Seg_1414" n="HIAT:w" s="T64.tx-PKZ.9">верующие</ts>
                  <nts id="Seg_1415" n="HIAT:ip">,</nts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64.tx-PKZ.11" id="Seg_1418" n="HIAT:w" s="T64.tx-PKZ.10">и</ts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1420" n="HIAT:ip">(</nts>
                  <ts e="T64.tx-PKZ.12" id="Seg_1422" n="HIAT:w" s="T64.tx-PKZ.11">н-</ts>
                  <nts id="Seg_1423" n="HIAT:ip">)</nts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64.tx-PKZ.13" id="Seg_1426" n="HIAT:w" s="T64.tx-PKZ.12">не</ts>
                  <nts id="Seg_1427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64.tx-PKZ.14" id="Seg_1429" n="HIAT:w" s="T64.tx-PKZ.13">отдал</ts>
                  <nts id="Seg_1430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64.tx-PKZ.15" id="Seg_1432" n="HIAT:w" s="T64.tx-PKZ.14">в</ts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64.tx-PKZ.16" id="Seg_1435" n="HIAT:w" s="T64.tx-PKZ.15">Америку</ts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64.tx-PKZ.17" id="Seg_1438" n="HIAT:w" s="T64.tx-PKZ.16">людей</ts>
                  <nts id="Seg_1439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_1441" n="HIAT:w" s="T64.tx-PKZ.17">своих</ts>
                  <nts id="Seg_1442" n="HIAT:ip">.</nts>
                  <nts id="Seg_1443" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_1445" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_1447" n="HIAT:w" s="T84">A</ts>
                  <nts id="Seg_1448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1449" n="HIAT:ip">(</nts>
                  <ts e="T86" id="Seg_1451" n="HIAT:w" s="T85">miʔnʼibeʔ</ts>
                  <nts id="Seg_1452" n="HIAT:ip">)</nts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_1455" n="HIAT:w" s="T86">ej</ts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_1458" n="HIAT:w" s="T87">mĭbiʔi</ts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_1461" n="HIAT:w" s="T88">kudajdə</ts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89.tx-PKZ.1" id="Seg_1464" n="HIAT:w" s="T89">numan</ts>
                  <nts id="Seg_1465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_1467" n="HIAT:w" s="T89.tx-PKZ.1">üzəsʼtə</ts>
                  <nts id="Seg_1468" n="HIAT:ip">.</nts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_1471" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_1473" n="HIAT:w" s="T90">Dĭgəttə</ts>
                  <nts id="Seg_1474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_1476" n="HIAT:w" s="T91">pʼaŋdəbi</ts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1478" n="HIAT:ip">(</nts>
                  <ts e="T93" id="Seg_1480" n="HIAT:w" s="T92">s-</ts>
                  <nts id="Seg_1481" n="HIAT:ip">)</nts>
                  <nts id="Seg_1482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_1484" n="HIAT:w" s="T93">sazən</ts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_1487" n="HIAT:w" s="T94">Америка</ts>
                  <nts id="Seg_1488" n="HIAT:ip">.</nts>
                  <nts id="Seg_1489" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_1491" n="HIAT:u" s="T95">
                  <nts id="Seg_1492" n="HIAT:ip">"</nts>
                  <nts id="Seg_1493" n="HIAT:ip">(</nts>
                  <ts e="T96" id="Seg_1495" n="HIAT:w" s="T95">Mĭndəd-</ts>
                  <nts id="Seg_1496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_1498" n="HIAT:w" s="T96">mĭ-</ts>
                  <nts id="Seg_1499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_1501" n="HIAT:w" s="T97">mĭ-</ts>
                  <nts id="Seg_1502" n="HIAT:ip">)</nts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1504" n="HIAT:ip">(</nts>
                  <ts e="T99" id="Seg_1506" n="HIAT:w" s="T98">Mĭbəleʔ</ts>
                  <nts id="Seg_1507" n="HIAT:ip">)</nts>
                  <nts id="Seg_1508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_1510" n="HIAT:w" s="T99">măna</ts>
                  <nts id="Seg_1511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_1513" n="HIAT:w" s="T100">dĭ</ts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_1516" n="HIAT:w" s="T101">il</ts>
                  <nts id="Seg_1517" n="HIAT:ip">,</nts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_1520" n="HIAT:w" s="T102">girgit</ts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_1523" n="HIAT:w" s="T103">il</ts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_1526" n="HIAT:w" s="T104">kudajdə</ts>
                  <nts id="Seg_1527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1528" n="HIAT:ip">(</nts>
                  <ts e="T106" id="Seg_1530" n="HIAT:w" s="T105">uman-</ts>
                  <nts id="Seg_1531" n="HIAT:ip">)</nts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106.tx-PKZ.1" id="Seg_1534" n="HIAT:w" s="T106">numan</ts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_1537" n="HIAT:w" s="T106.tx-PKZ.1">üzəlieʔi</ts>
                  <nts id="Seg_1538" n="HIAT:ip">"</nts>
                  <nts id="Seg_1539" n="HIAT:ip">.</nts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_1542" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_1544" n="HIAT:w" s="T107">Dĭgəttə</ts>
                  <nts id="Seg_1545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_1547" n="HIAT:w" s="T108">sazən</ts>
                  <nts id="Seg_1548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_1550" n="HIAT:w" s="T109">šobi</ts>
                  <nts id="Seg_1551" n="HIAT:ip">.</nts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_1554" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_1556" n="HIAT:w" s="T110">Pʼaŋdəbiʔi</ts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_1559" n="HIAT:w" s="T111">šindi</ts>
                  <nts id="Seg_1560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1561" n="HIAT:ip">(</nts>
                  <ts e="T113" id="Seg_1563" n="HIAT:w" s="T112">i-</ts>
                  <nts id="Seg_1564" n="HIAT:ip">)</nts>
                  <nts id="Seg_1565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_1567" n="HIAT:w" s="T113">kudajdə</ts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1569" n="HIAT:ip">(</nts>
                  <ts e="T115" id="Seg_1571" n="HIAT:w" s="T114">kudonz-</ts>
                  <nts id="Seg_1572" n="HIAT:ip">)</nts>
                  <nts id="Seg_1573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_1575" n="HIAT:w" s="T115">kudolbi</ts>
                  <nts id="Seg_1576" n="HIAT:ip">.</nts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_1579" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_1581" n="HIAT:w" s="T116">I</ts>
                  <nts id="Seg_1582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_1584" n="HIAT:w" s="T117">dĭbər</ts>
                  <nts id="Seg_1585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_1587" n="HIAT:w" s="T118">sazəndə</ts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_1590" n="HIAT:w" s="T119">pʼaŋdəbi</ts>
                  <nts id="Seg_1591" n="HIAT:ip">,</nts>
                  <nts id="Seg_1592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_1594" n="HIAT:w" s="T120">što</ts>
                  <nts id="Seg_1595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_1597" n="HIAT:w" s="T121">kudajdə</ts>
                  <nts id="Seg_1598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122.tx-PKZ.1" id="Seg_1600" n="HIAT:w" s="T122">numan</ts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_1603" n="HIAT:w" s="T122.tx-PKZ.1">üzleʔbəm</ts>
                  <nts id="Seg_1604" n="HIAT:ip">.</nts>
                  <nts id="Seg_1605" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_1607" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_1609" n="HIAT:w" s="T123">Kabarləj</ts>
                  <nts id="Seg_1610" n="HIAT:ip">.</nts>
                  <nts id="Seg_1611" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T149" id="Seg_1612" n="sc" s="T147">
               <ts e="T149" id="Seg_1614" n="HIAT:u" s="T147">
                  <ts e="T147.tx-PKZ.1" id="Seg_1616" n="HIAT:w" s="T147">Можно</ts>
                  <nts id="Seg_1617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_1619" n="HIAT:w" s="T147.tx-PKZ.1">рассказать</ts>
                  <nts id="Seg_1620" n="HIAT:ip">…</nts>
                  <nts id="Seg_1621" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T156" id="Seg_1622" n="sc" s="T153">
               <ts e="T156" id="Seg_1624" n="HIAT:u" s="T153">
                  <ts e="T153.tx-PKZ.1" id="Seg_1626" n="HIAT:w" s="T153">Можно</ts>
                  <nts id="Seg_1627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153.tx-PKZ.2" id="Seg_1629" n="HIAT:w" s="T153.tx-PKZ.1">и</ts>
                  <nts id="Seg_1630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_1632" n="HIAT:w" s="T153.tx-PKZ.2">это</ts>
                  <nts id="Seg_1633" n="HIAT:ip">.</nts>
                  <nts id="Seg_1634" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T161" id="Seg_1635" n="sc" s="T160">
               <ts e="T161" id="Seg_1637" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_1639" n="HIAT:w" s="T160">А</ts>
                  <nts id="Seg_1640" n="HIAT:ip">?</nts>
                  <nts id="Seg_1641" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T168" id="Seg_1642" n="sc" s="T164">
               <ts e="T168" id="Seg_1644" n="HIAT:u" s="T164">
                  <ts e="T167" id="Seg_1646" n="HIAT:w" s="T164">Ну</ts>
                  <nts id="Seg_1647" n="HIAT:ip">,</nts>
                  <nts id="Seg_1648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_1650" n="HIAT:w" s="T167">ничего</ts>
                  <nts id="Seg_1651" n="HIAT:ip">.</nts>
                  <nts id="Seg_1652" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T177" id="Seg_1653" n="sc" s="T172">
               <ts e="T177" id="Seg_1655" n="HIAT:u" s="T172">
                  <ts e="T172.tx-PKZ.1" id="Seg_1657" n="HIAT:w" s="T172">Все</ts>
                  <nts id="Seg_1658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172.tx-PKZ.2" id="Seg_1660" n="HIAT:w" s="T172.tx-PKZ.1">хорошие</ts>
                  <nts id="Seg_1661" n="HIAT:ip">,</nts>
                  <nts id="Seg_1662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172.tx-PKZ.3" id="Seg_1664" n="HIAT:w" s="T172.tx-PKZ.2">приветствуют</ts>
                  <nts id="Seg_1665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_1667" n="HIAT:w" s="T172.tx-PKZ.3">хорошо</ts>
                  <nts id="Seg_1668" n="HIAT:ip">.</nts>
                  <nts id="Seg_1669" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T196" id="Seg_1670" n="sc" s="T189">
               <ts e="T196" id="Seg_1672" n="HIAT:u" s="T189">
                  <nts id="Seg_1673" n="HIAT:ip">(</nts>
                  <ts e="T192" id="Seg_1675" n="HIAT:w" s="T189">Все=</ts>
                  <nts id="Seg_1676" n="HIAT:ip">)</nts>
                  <nts id="Seg_1677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_1679" n="HIAT:w" s="T192">Все</ts>
                  <nts id="Seg_1680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_1682" n="HIAT:w" s="T193">обнимали</ts>
                  <nts id="Seg_1683" n="HIAT:ip">,</nts>
                  <nts id="Seg_1684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_1686" n="HIAT:w" s="T194">целовали</ts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_1689" n="HIAT:w" s="T195">даже</ts>
                  <nts id="Seg_1690" n="HIAT:ip">.</nts>
                  <nts id="Seg_1691" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T220" id="Seg_1692" n="sc" s="T214">
               <ts e="T220" id="Seg_1694" n="HIAT:u" s="T214">
                  <ts e="T214.tx-PKZ.1" id="Seg_1696" n="HIAT:w" s="T214">Ну</ts>
                  <nts id="Seg_1697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214.tx-PKZ.2" id="Seg_1699" n="HIAT:w" s="T214.tx-PKZ.1">как</ts>
                  <nts id="Seg_1700" n="HIAT:ip">,</nts>
                  <nts id="Seg_1701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214.tx-PKZ.3" id="Seg_1703" n="HIAT:w" s="T214.tx-PKZ.2">по-русски</ts>
                  <nts id="Seg_1704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_1706" n="HIAT:w" s="T214.tx-PKZ.3">рассказывать</ts>
                  <nts id="Seg_1707" n="HIAT:ip">?</nts>
                  <nts id="Seg_1708" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T236" id="Seg_1709" n="sc" s="T232">
               <ts e="T236" id="Seg_1711" n="HIAT:u" s="T232">
                  <ts e="T232.tx-PKZ.1" id="Seg_1713" n="HIAT:w" s="T232">Там</ts>
                  <nts id="Seg_1714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232.tx-PKZ.2" id="Seg_1716" n="HIAT:w" s="T232.tx-PKZ.1">он</ts>
                  <nts id="Seg_1717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232.tx-PKZ.3" id="Seg_1719" n="HIAT:w" s="T232.tx-PKZ.2">говорит</ts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_1722" n="HIAT:w" s="T232.tx-PKZ.3">уже</ts>
                  <nts id="Seg_1723" n="HIAT:ip">?</nts>
                  <nts id="Seg_1724" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T367" id="Seg_1725" n="sc" s="T248">
               <ts e="T262" id="Seg_1727" n="HIAT:u" s="T248">
                  <ts e="T248.tx-PKZ.1" id="Seg_1729" n="HIAT:w" s="T248">Када</ts>
                  <nts id="Seg_1730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248.tx-PKZ.2" id="Seg_1732" n="HIAT:w" s="T248.tx-PKZ.1">я</ts>
                  <nts id="Seg_1733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248.tx-PKZ.3" id="Seg_1735" n="HIAT:w" s="T248.tx-PKZ.2">на</ts>
                  <nts id="Seg_1736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1737" n="HIAT:ip">(</nts>
                  <ts e="T248.tx-PKZ.4" id="Seg_1739" n="HIAT:w" s="T248.tx-PKZ.3">конгре-</ts>
                  <nts id="Seg_1740" n="HIAT:ip">)</nts>
                  <nts id="Seg_1741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248.tx-PKZ.5" id="Seg_1743" n="HIAT:w" s="T248.tx-PKZ.4">на</ts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248.tx-PKZ.6" id="Seg_1746" n="HIAT:w" s="T248.tx-PKZ.5">конгрет</ts>
                  <nts id="Seg_1747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248.tx-PKZ.7" id="Seg_1749" n="HIAT:w" s="T248.tx-PKZ.6">пришла</ts>
                  <nts id="Seg_1750" n="HIAT:ip">,</nts>
                  <nts id="Seg_1751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248.tx-PKZ.8" id="Seg_1753" n="HIAT:w" s="T248.tx-PKZ.7">все</ts>
                  <nts id="Seg_1754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248.tx-PKZ.9" id="Seg_1756" n="HIAT:w" s="T248.tx-PKZ.8">такие</ts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1759" n="HIAT:w" s="T248.tx-PKZ.9">радые</ts>
                  <nts id="Seg_1760" n="HIAT:ip">.</nts>
                  <nts id="Seg_1761" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T268" id="Seg_1763" n="HIAT:u" s="T262">
                  <ts e="T262.tx-PKZ.1" id="Seg_1765" n="HIAT:w" s="T262">Все</ts>
                  <nts id="Seg_1766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262.tx-PKZ.2" id="Seg_1768" n="HIAT:w" s="T262.tx-PKZ.1">люди</ts>
                  <nts id="Seg_1769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262.tx-PKZ.3" id="Seg_1771" n="HIAT:w" s="T262.tx-PKZ.2">хороши</ts>
                  <nts id="Seg_1772" n="HIAT:ip">,</nts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262.tx-PKZ.4" id="Seg_1775" n="HIAT:w" s="T262.tx-PKZ.3">приняли</ts>
                  <nts id="Seg_1776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1778" n="HIAT:w" s="T262.tx-PKZ.4">меня</ts>
                  <nts id="Seg_1779" n="HIAT:ip">.</nts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T272" id="Seg_1782" n="HIAT:u" s="T268">
                  <ts e="T268.tx-PKZ.1" id="Seg_1784" n="HIAT:w" s="T268">Все</ts>
                  <nts id="Seg_1785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268.tx-PKZ.2" id="Seg_1787" n="HIAT:w" s="T268.tx-PKZ.1">ученые</ts>
                  <nts id="Seg_1788" n="HIAT:ip">,</nts>
                  <nts id="Seg_1789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268.tx-PKZ.3" id="Seg_1791" n="HIAT:w" s="T268.tx-PKZ.2">умные</ts>
                  <nts id="Seg_1792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1794" n="HIAT:w" s="T268.tx-PKZ.3">люди</ts>
                  <nts id="Seg_1795" n="HIAT:ip">.</nts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T274" id="Seg_1798" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_1800" n="HIAT:w" s="T272">Интересные</ts>
                  <nts id="Seg_1801" n="HIAT:ip">,</nts>
                  <nts id="Seg_1802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1804" n="HIAT:w" s="T273">красивые</ts>
                  <nts id="Seg_1805" n="HIAT:ip">.</nts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T284" id="Seg_1808" n="HIAT:u" s="T274">
                  <ts e="T275" id="Seg_1810" n="HIAT:w" s="T274">Ну</ts>
                  <nts id="Seg_1811" n="HIAT:ip">,</nts>
                  <nts id="Seg_1812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1814" n="HIAT:w" s="T275">красивше</ts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1817" n="HIAT:w" s="T276">всех</ts>
                  <nts id="Seg_1818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1820" n="HIAT:w" s="T277">мой</ts>
                  <nts id="Seg_1821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1823" n="HIAT:w" s="T278">Агафон</ts>
                  <nts id="Seg_1824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1826" n="HIAT:w" s="T279">Иванович</ts>
                  <nts id="Seg_1827" n="HIAT:ip">,</nts>
                  <nts id="Seg_1828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1830" n="HIAT:w" s="T280">я</ts>
                  <nts id="Seg_1831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1833" n="HIAT:w" s="T281">его</ts>
                  <nts id="Seg_1834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1836" n="HIAT:w" s="T282">люблю</ts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1839" n="HIAT:w" s="T283">шибко</ts>
                  <nts id="Seg_1840" n="HIAT:ip">.</nts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T296" id="Seg_1843" n="HIAT:u" s="T284">
                  <ts e="T284.tx-PKZ.1" id="Seg_1845" n="HIAT:w" s="T284">Он</ts>
                  <nts id="Seg_1846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284.tx-PKZ.2" id="Seg_1848" n="HIAT:w" s="T284.tx-PKZ.1">мой</ts>
                  <nts id="Seg_1849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284.tx-PKZ.3" id="Seg_1851" n="HIAT:w" s="T284.tx-PKZ.2">ученик</ts>
                  <nts id="Seg_1852" n="HIAT:ip">,</nts>
                  <nts id="Seg_1853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284.tx-PKZ.4" id="Seg_1855" n="HIAT:w" s="T284.tx-PKZ.3">я</ts>
                  <nts id="Seg_1856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284.tx-PKZ.5" id="Seg_1858" n="HIAT:w" s="T284.tx-PKZ.4">его</ts>
                  <nts id="Seg_1859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284.tx-PKZ.6" id="Seg_1861" n="HIAT:w" s="T284.tx-PKZ.5">учила</ts>
                  <nts id="Seg_1862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1863" n="HIAT:ip">(</nts>
                  <ts e="T284.tx-PKZ.7" id="Seg_1865" n="HIAT:w" s="T284.tx-PKZ.6">на</ts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284.tx-PKZ.8" id="Seg_1868" n="HIAT:w" s="T284.tx-PKZ.7">с-</ts>
                  <nts id="Seg_1869" n="HIAT:ip">)</nts>
                  <nts id="Seg_1870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284.tx-PKZ.9" id="Seg_1872" n="HIAT:w" s="T284.tx-PKZ.8">на</ts>
                  <nts id="Seg_1873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284.tx-PKZ.10" id="Seg_1875" n="HIAT:w" s="T284.tx-PKZ.9">свое</ts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1878" n="HIAT:w" s="T284.tx-PKZ.10">наречие</ts>
                  <nts id="Seg_1879" n="HIAT:ip">.</nts>
                  <nts id="Seg_1880" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T301" id="Seg_1882" n="HIAT:u" s="T296">
                  <ts e="T297" id="Seg_1884" n="HIAT:w" s="T296">Ну</ts>
                  <nts id="Seg_1885" n="HIAT:ip">,</nts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1888" n="HIAT:w" s="T297">и</ts>
                  <nts id="Seg_1889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1891" n="HIAT:w" s="T298">поглянулись</ts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1894" n="HIAT:w" s="T299">и</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1897" n="HIAT:w" s="T300">все</ts>
                  <nts id="Seg_1898" n="HIAT:ip">.</nts>
                  <nts id="Seg_1899" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T306" id="Seg_1901" n="HIAT:u" s="T301">
                  <ts e="T301.tx-PKZ.1" id="Seg_1903" n="HIAT:w" s="T301">А</ts>
                  <nts id="Seg_1904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1905" n="HIAT:ip">(</nts>
                  <ts e="T301.tx-PKZ.2" id="Seg_1907" n="HIAT:w" s="T301.tx-PKZ.1">теперь</ts>
                  <nts id="Seg_1908" n="HIAT:ip">)</nts>
                  <nts id="Seg_1909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301.tx-PKZ.3" id="Seg_1911" n="HIAT:w" s="T301.tx-PKZ.2">поехала</ts>
                  <nts id="Seg_1912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301.tx-PKZ.4" id="Seg_1914" n="HIAT:w" s="T301.tx-PKZ.3">на</ts>
                  <nts id="Seg_1915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1917" n="HIAT:w" s="T301.tx-PKZ.4">кладбище</ts>
                  <nts id="Seg_1918" n="HIAT:ip">.</nts>
                  <nts id="Seg_1919" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T310" id="Seg_1921" n="HIAT:u" s="T306">
                  <ts e="T306.tx-PKZ.1" id="Seg_1923" n="HIAT:w" s="T306">Там</ts>
                  <nts id="Seg_1924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306.tx-PKZ.2" id="Seg_1926" n="HIAT:w" s="T306.tx-PKZ.1">тоже</ts>
                  <nts id="Seg_1927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306.tx-PKZ.3" id="Seg_1929" n="HIAT:w" s="T306.tx-PKZ.2">хорошо</ts>
                  <nts id="Seg_1930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1932" n="HIAT:w" s="T306.tx-PKZ.3">было</ts>
                  <nts id="Seg_1933" n="HIAT:ip">.</nts>
                  <nts id="Seg_1934" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T317" id="Seg_1936" n="HIAT:u" s="T310">
                  <ts e="T310.tx-PKZ.1" id="Seg_1938" n="HIAT:w" s="T310">И</ts>
                  <nts id="Seg_1939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310.tx-PKZ.2" id="Seg_1941" n="HIAT:w" s="T310.tx-PKZ.1">там</ts>
                  <nts id="Seg_1942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310.tx-PKZ.3" id="Seg_1944" n="HIAT:w" s="T310.tx-PKZ.2">я</ts>
                  <nts id="Seg_1945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310.tx-PKZ.4" id="Seg_1947" n="HIAT:w" s="T310.tx-PKZ.3">на</ts>
                  <nts id="Seg_1948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310.tx-PKZ.5" id="Seg_1950" n="HIAT:w" s="T310.tx-PKZ.4">свое</ts>
                  <nts id="Seg_1951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310.tx-PKZ.6" id="Seg_1953" n="HIAT:w" s="T310.tx-PKZ.5">наречие</ts>
                  <nts id="Seg_1954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1956" n="HIAT:w" s="T310.tx-PKZ.6">пела</ts>
                  <nts id="Seg_1957" n="HIAT:ip">.</nts>
                  <nts id="Seg_1958" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_1960" n="HIAT:u" s="T317">
                  <ts e="T317.tx-PKZ.1" id="Seg_1962" n="HIAT:w" s="T317">Божественные</ts>
                  <nts id="Seg_1963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317.tx-PKZ.2" id="Seg_1965" n="HIAT:w" s="T317.tx-PKZ.1">молитвы</ts>
                  <nts id="Seg_1966" n="HIAT:ip">,</nts>
                  <nts id="Seg_1967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317.tx-PKZ.3" id="Seg_1969" n="HIAT:w" s="T317.tx-PKZ.2">все</ts>
                  <nts id="Seg_1970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317.tx-PKZ.4" id="Seg_1972" n="HIAT:w" s="T317.tx-PKZ.3">тоже</ts>
                  <nts id="Seg_1973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317.tx-PKZ.5" id="Seg_1975" n="HIAT:w" s="T317.tx-PKZ.4">такие</ts>
                  <nts id="Seg_1976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317.tx-PKZ.6" id="Seg_1978" n="HIAT:w" s="T317.tx-PKZ.5">рады</ts>
                  <nts id="Seg_1979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317.tx-PKZ.7" id="Seg_1981" n="HIAT:w" s="T317.tx-PKZ.6">были</ts>
                  <nts id="Seg_1982" n="HIAT:ip">,</nts>
                  <nts id="Seg_1983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317.tx-PKZ.8" id="Seg_1985" n="HIAT:w" s="T317.tx-PKZ.7">все</ts>
                  <nts id="Seg_1986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317.tx-PKZ.9" id="Seg_1988" n="HIAT:w" s="T317.tx-PKZ.8">меня</ts>
                  <nts id="Seg_1989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1991" n="HIAT:w" s="T317.tx-PKZ.9">приветствовали</ts>
                  <nts id="Seg_1992" n="HIAT:ip">.</nts>
                  <nts id="Seg_1993" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T328" id="Seg_1995" n="HIAT:u" s="T327">
                  <ts e="T328" id="Seg_1997" n="HIAT:w" s="T327">Приглашали</ts>
                  <nts id="Seg_1998" n="HIAT:ip">.</nts>
                  <nts id="Seg_1999" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_2001" n="HIAT:u" s="T328">
                  <ts e="T328.tx-PKZ.1" id="Seg_2003" n="HIAT:w" s="T328">Там</ts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328.tx-PKZ.2" id="Seg_2006" n="HIAT:w" s="T328.tx-PKZ.1">я</ts>
                  <nts id="Seg_2007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328.tx-PKZ.3" id="Seg_2009" n="HIAT:w" s="T328.tx-PKZ.2">видала</ts>
                  <nts id="Seg_2010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328.tx-PKZ.4" id="Seg_2012" n="HIAT:w" s="T328.tx-PKZ.3">много</ts>
                  <nts id="Seg_2013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328.tx-PKZ.5" id="Seg_2015" n="HIAT:w" s="T328.tx-PKZ.4">белок</ts>
                  <nts id="Seg_2016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328.tx-PKZ.6" id="Seg_2018" n="HIAT:w" s="T328.tx-PKZ.5">на</ts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328.tx-PKZ.7" id="Seg_2021" n="HIAT:w" s="T328.tx-PKZ.6">кладбище</ts>
                  <nts id="Seg_2022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328.tx-PKZ.8" id="Seg_2024" n="HIAT:w" s="T328.tx-PKZ.7">бегают</ts>
                  <nts id="Seg_2025" n="HIAT:ip">,</nts>
                  <nts id="Seg_2026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2027" n="HIAT:ip">(</nts>
                  <ts e="T328.tx-PKZ.9" id="Seg_2029" n="HIAT:w" s="T328.tx-PKZ.8">а</ts>
                  <nts id="Seg_2030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328.tx-PKZ.10" id="Seg_2032" n="HIAT:w" s="T328.tx-PKZ.9">у=</ts>
                  <nts id="Seg_2033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328.tx-PKZ.11" id="Seg_2035" n="HIAT:w" s="T328.tx-PKZ.10">а</ts>
                  <nts id="Seg_2036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328.tx-PKZ.12" id="Seg_2038" n="HIAT:w" s="T328.tx-PKZ.11">у</ts>
                  <nts id="Seg_2039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328.tx-PKZ.13" id="Seg_2041" n="HIAT:w" s="T328.tx-PKZ.12">нас=</ts>
                  <nts id="Seg_2042" n="HIAT:ip">)</nts>
                  <nts id="Seg_2043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328.tx-PKZ.14" id="Seg_2045" n="HIAT:w" s="T328.tx-PKZ.13">а</ts>
                  <nts id="Seg_2046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328.tx-PKZ.15" id="Seg_2048" n="HIAT:w" s="T328.tx-PKZ.14">у</ts>
                  <nts id="Seg_2049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328.tx-PKZ.16" id="Seg_2051" n="HIAT:w" s="T328.tx-PKZ.15">нас</ts>
                  <nts id="Seg_2052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328.tx-PKZ.17" id="Seg_2054" n="HIAT:w" s="T328.tx-PKZ.16">там</ts>
                  <nts id="Seg_2055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_2057" n="HIAT:w" s="T328.tx-PKZ.17">нету</ts>
                  <nts id="Seg_2058" n="HIAT:ip">.</nts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T350" id="Seg_2061" n="HIAT:u" s="T346">
                  <ts e="T346.tx-PKZ.1" id="Seg_2063" n="HIAT:w" s="T346">Ну</ts>
                  <nts id="Seg_2064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346.tx-PKZ.2" id="Seg_2066" n="HIAT:w" s="T346.tx-PKZ.1">наши</ts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346.tx-PKZ.3" id="Seg_2069" n="HIAT:w" s="T346.tx-PKZ.2">охотники</ts>
                  <nts id="Seg_2070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_2072" n="HIAT:w" s="T346.tx-PKZ.3">убивают</ts>
                  <nts id="Seg_2073" n="HIAT:ip">.</nts>
                  <nts id="Seg_2074" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T355" id="Seg_2076" n="HIAT:u" s="T350">
                  <ts e="T350.tx-PKZ.1" id="Seg_2078" n="HIAT:w" s="T350">А</ts>
                  <nts id="Seg_2079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350.tx-PKZ.2" id="Seg_2081" n="HIAT:w" s="T350.tx-PKZ.1">тут</ts>
                  <nts id="Seg_2082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350.tx-PKZ.3" id="Seg_2084" n="HIAT:w" s="T350.tx-PKZ.2">не</ts>
                  <nts id="Seg_2085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350.tx-PKZ.4" id="Seg_2087" n="HIAT:w" s="T350.tx-PKZ.3">убивают</ts>
                  <nts id="Seg_2088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_2090" n="HIAT:w" s="T350.tx-PKZ.4">белок</ts>
                  <nts id="Seg_2091" n="HIAT:ip">.</nts>
                  <nts id="Seg_2092" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T360" id="Seg_2094" n="HIAT:u" s="T355">
                  <ts e="T355.tx-PKZ.1" id="Seg_2096" n="HIAT:w" s="T355">Я</ts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2098" n="HIAT:ip">(</nts>
                  <ts e="T355.tx-PKZ.2" id="Seg_2100" n="HIAT:w" s="T355.tx-PKZ.1">имя</ts>
                  <nts id="Seg_2101" n="HIAT:ip">)</nts>
                  <nts id="Seg_2102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355.tx-PKZ.3" id="Seg_2104" n="HIAT:w" s="T355.tx-PKZ.2">давала</ts>
                  <nts id="Seg_2105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355.tx-PKZ.4" id="Seg_2107" n="HIAT:w" s="T355.tx-PKZ.3">кушать</ts>
                  <nts id="Seg_2108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_2110" n="HIAT:w" s="T355.tx-PKZ.4">там</ts>
                  <nts id="Seg_2111" n="HIAT:ip">.</nts>
                  <nts id="Seg_2112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T367" id="Seg_2114" n="HIAT:u" s="T360">
                  <ts e="T361" id="Seg_2116" n="HIAT:w" s="T360">Ну</ts>
                  <nts id="Seg_2117" n="HIAT:ip">,</nts>
                  <nts id="Seg_2118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_2120" n="HIAT:w" s="T361">теперь</ts>
                  <nts id="Seg_2121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_2123" n="HIAT:w" s="T362">надо</ts>
                  <nts id="Seg_2124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_2126" n="HIAT:w" s="T363">по</ts>
                  <nts id="Seg_2127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_2129" n="HIAT:w" s="T364">-</ts>
                  <nts id="Seg_2130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_2132" n="HIAT:w" s="T365">своему</ts>
                  <nts id="Seg_2133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_2135" n="HIAT:w" s="T366">сказать</ts>
                  <nts id="Seg_2136" n="HIAT:ip">.</nts>
                  <nts id="Seg_2137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T500" id="Seg_2138" n="sc" s="T368">
               <ts e="T377" id="Seg_2140" n="HIAT:u" s="T368">
                  <ts e="T369" id="Seg_2142" n="HIAT:w" s="T368">Măn</ts>
                  <nts id="Seg_2143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_2145" n="HIAT:w" s="T369">ertən</ts>
                  <nts id="Seg_2146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_2148" n="HIAT:w" s="T370">uʔbdəbiam</ts>
                  <nts id="Seg_2149" n="HIAT:ip">,</nts>
                  <nts id="Seg_2150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_2152" n="HIAT:w" s="T371">kudajdə</ts>
                  <nts id="Seg_2153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372.tx-PKZ.1" id="Seg_2155" n="HIAT:w" s="T372">numan</ts>
                  <nts id="Seg_2156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_2158" n="HIAT:w" s="T372.tx-PKZ.1">üzəbiem</ts>
                  <nts id="Seg_2159" n="HIAT:ip">,</nts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_2162" n="HIAT:w" s="T373">edəʔleʔbəm</ts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2164" n="HIAT:ip">(</nts>
                  <ts e="T375" id="Seg_2166" n="HIAT:w" s="T374">Aga-</ts>
                  <nts id="Seg_2167" n="HIAT:ip">)</nts>
                  <nts id="Seg_2168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_2170" n="HIAT:w" s="T375">Agafon</ts>
                  <nts id="Seg_2171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_2173" n="HIAT:w" s="T376">Ivanovičəm</ts>
                  <nts id="Seg_2174" n="HIAT:ip">.</nts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T380" id="Seg_2177" n="HIAT:u" s="T377">
                  <ts e="T378" id="Seg_2179" n="HIAT:w" s="T377">Dĭ</ts>
                  <nts id="Seg_2180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_2182" n="HIAT:w" s="T378">šobi</ts>
                  <nts id="Seg_2183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_2185" n="HIAT:w" s="T379">măna</ts>
                  <nts id="Seg_2186" n="HIAT:ip">.</nts>
                  <nts id="Seg_2187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T384" id="Seg_2189" n="HIAT:u" s="T380">
                  <ts e="T381" id="Seg_2191" n="HIAT:w" s="T380">Dĭgəttə</ts>
                  <nts id="Seg_2192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_2194" n="HIAT:w" s="T381">măn</ts>
                  <nts id="Seg_2195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_2197" n="HIAT:w" s="T382">dĭzi</ts>
                  <nts id="Seg_2198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_2200" n="HIAT:w" s="T383">kambiam</ts>
                  <nts id="Seg_2201" n="HIAT:ip">.</nts>
                  <nts id="Seg_2202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T389" id="Seg_2204" n="HIAT:u" s="T384">
                  <ts e="T385" id="Seg_2206" n="HIAT:w" s="T384">Šobiam</ts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_2209" n="HIAT:w" s="T385">dĭbər</ts>
                  <nts id="Seg_2210" n="HIAT:ip">,</nts>
                  <nts id="Seg_2211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2212" n="HIAT:ip">(</nts>
                  <ts e="T387" id="Seg_2214" n="HIAT:w" s="T386">dĭ=</ts>
                  <nts id="Seg_2215" n="HIAT:ip">)</nts>
                  <nts id="Seg_2216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_2218" n="HIAT:w" s="T387">dĭ</ts>
                  <nts id="Seg_2219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_2221" n="HIAT:w" s="T388">kănferentsianə</ts>
                  <nts id="Seg_2222" n="HIAT:ip">.</nts>
                  <nts id="Seg_2223" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T397" id="Seg_2225" n="HIAT:u" s="T389">
                  <ts e="T390" id="Seg_2227" n="HIAT:w" s="T389">Iʔgö</ts>
                  <nts id="Seg_2228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2229" n="HIAT:ip">(</nts>
                  <ts e="T391" id="Seg_2231" n="HIAT:w" s="T390">i-</ts>
                  <nts id="Seg_2232" n="HIAT:ip">)</nts>
                  <nts id="Seg_2233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_2235" n="HIAT:w" s="T391">il</ts>
                  <nts id="Seg_2236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2237" n="HIAT:ip">(</nts>
                  <ts e="T393" id="Seg_2239" n="HIAT:w" s="T392">dĭn</ts>
                  <nts id="Seg_2240" n="HIAT:ip">)</nts>
                  <nts id="Seg_2241" n="HIAT:ip">,</nts>
                  <nts id="Seg_2242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_2244" n="HIAT:w" s="T393">bar</ts>
                  <nts id="Seg_2245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2246" n="HIAT:ip">(</nts>
                  <ts e="T395" id="Seg_2248" n="HIAT:w" s="T394">uʔ-</ts>
                  <nts id="Seg_2249" n="HIAT:ip">)</nts>
                  <nts id="Seg_2250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_2252" n="HIAT:w" s="T395">kuvas</ts>
                  <nts id="Seg_2253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_2255" n="HIAT:w" s="T396">ugandə</ts>
                  <nts id="Seg_2256" n="HIAT:ip">.</nts>
                  <nts id="Seg_2257" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T398" id="Seg_2259" n="HIAT:u" s="T397">
                  <ts e="T398" id="Seg_2261" n="HIAT:w" s="T397">Jakšeʔi</ts>
                  <nts id="Seg_2262" n="HIAT:ip">.</nts>
                  <nts id="Seg_2263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T403" id="Seg_2265" n="HIAT:u" s="T398">
                  <ts e="T399" id="Seg_2267" n="HIAT:w" s="T398">Pʼaŋzittə</ts>
                  <nts id="Seg_2268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_2270" n="HIAT:w" s="T399">tăŋ</ts>
                  <nts id="Seg_2271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_2273" n="HIAT:w" s="T400">tĭmneʔi</ts>
                  <nts id="Seg_2274" n="HIAT:ip">,</nts>
                  <nts id="Seg_2275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_2277" n="HIAT:w" s="T401">šagəstə</ts>
                  <nts id="Seg_2278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_2280" n="HIAT:w" s="T402">iʔgö</ts>
                  <nts id="Seg_2281" n="HIAT:ip">.</nts>
                  <nts id="Seg_2282" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T407" id="Seg_2284" n="HIAT:u" s="T403">
                  <ts e="T404" id="Seg_2286" n="HIAT:w" s="T403">Dĭgəttə</ts>
                  <nts id="Seg_2287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_2289" n="HIAT:w" s="T404">bar</ts>
                  <nts id="Seg_2290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_2292" n="HIAT:w" s="T405">măna</ts>
                  <nts id="Seg_2293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_2295" n="HIAT:w" s="T406">pănarlaʔbəʔjə</ts>
                  <nts id="Seg_2296" n="HIAT:ip">.</nts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T408" id="Seg_2299" n="HIAT:u" s="T407">
                  <ts e="T408" id="Seg_2301" n="HIAT:w" s="T407">Nezeŋ</ts>
                  <nts id="Seg_2302" n="HIAT:ip">.</nts>
                  <nts id="Seg_2303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T413" id="Seg_2305" n="HIAT:u" s="T408">
                  <ts e="T409" id="Seg_2307" n="HIAT:w" s="T408">A</ts>
                  <nts id="Seg_2308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_2310" n="HIAT:w" s="T409">tibizeŋ</ts>
                  <nts id="Seg_2311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2312" n="HIAT:ip">(</nts>
                  <ts e="T411" id="Seg_2314" n="HIAT:w" s="T410">uda-</ts>
                  <nts id="Seg_2315" n="HIAT:ip">)</nts>
                  <nts id="Seg_2316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_2318" n="HIAT:w" s="T411">udazaŋdə</ts>
                  <nts id="Seg_2319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_2321" n="HIAT:w" s="T412">nuleʔbəʔjə</ts>
                  <nts id="Seg_2322" n="HIAT:ip">.</nts>
                  <nts id="Seg_2323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T430" id="Seg_2325" n="HIAT:u" s="T413">
                  <ts e="T414" id="Seg_2327" n="HIAT:w" s="T413">Dĭgəttə</ts>
                  <nts id="Seg_2328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_2330" n="HIAT:w" s="T414">dĭgəʔ</ts>
                  <nts id="Seg_2331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2332" n="HIAT:ip">(</nts>
                  <ts e="T416" id="Seg_2334" n="HIAT:w" s="T415">š-</ts>
                  <nts id="Seg_2335" n="HIAT:ip">)</nts>
                  <nts id="Seg_2336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_2338" n="HIAT:w" s="T416">kambiam</ts>
                  <nts id="Seg_2339" n="HIAT:ip">,</nts>
                  <nts id="Seg_2340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_2342" n="HIAT:w" s="T417">mašinanə</ts>
                  <nts id="Seg_2343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2344" n="HIAT:ip">(</nts>
                  <ts e="T419" id="Seg_2346" n="HIAT:w" s="T418">amnubi=</ts>
                  <nts id="Seg_2347" n="HIAT:ip">)</nts>
                  <nts id="Seg_2348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_2350" n="HIAT:w" s="T419">amnobiam</ts>
                  <nts id="Seg_2351" n="HIAT:ip">,</nts>
                  <nts id="Seg_2352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2353" n="HIAT:ip">(</nts>
                  <ts e="T421" id="Seg_2355" n="HIAT:w" s="T420">ibiem</ts>
                  <nts id="Seg_2356" n="HIAT:ip">)</nts>
                  <nts id="Seg_2357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_2359" n="HIAT:w" s="T421">dĭn</ts>
                  <nts id="Seg_2360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2361" n="HIAT:ip">(</nts>
                  <ts e="T423" id="Seg_2363" n="HIAT:w" s="T422">gi-</ts>
                  <nts id="Seg_2364" n="HIAT:ip">)</nts>
                  <nts id="Seg_2365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_2367" n="HIAT:w" s="T423">gijən</ts>
                  <nts id="Seg_2368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_2370" n="HIAT:w" s="T424">sazən</ts>
                  <nts id="Seg_2371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2372" n="HIAT:ip">(</nts>
                  <ts e="T426" id="Seg_2374" n="HIAT:w" s="T425">aliaʔi</ts>
                  <nts id="Seg_2375" n="HIAT:ip">)</nts>
                  <nts id="Seg_2376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_2378" n="HIAT:w" s="T426">i</ts>
                  <nts id="Seg_2379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_2381" n="HIAT:w" s="T427">knižkaʔi</ts>
                  <nts id="Seg_2382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_2384" n="HIAT:w" s="T428">gijən</ts>
                  <nts id="Seg_2385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2386" n="HIAT:ip">(</nts>
                  <ts e="T430" id="Seg_2388" n="HIAT:w" s="T429">enləʔbəʔjə</ts>
                  <nts id="Seg_2389" n="HIAT:ip">)</nts>
                  <nts id="Seg_2390" n="HIAT:ip">.</nts>
                  <nts id="Seg_2391" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T436" id="Seg_2393" n="HIAT:u" s="T430">
                  <ts e="T431" id="Seg_2395" n="HIAT:w" s="T430">Dĭn</ts>
                  <nts id="Seg_2396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_2398" n="HIAT:w" s="T431">măna</ts>
                  <nts id="Seg_2399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_2401" n="HIAT:w" s="T432">tože</ts>
                  <nts id="Seg_2402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2403" n="HIAT:ip">(</nts>
                  <ts e="T434" id="Seg_2405" n="HIAT:w" s="T433">kü-</ts>
                  <nts id="Seg_2406" n="HIAT:ip">)</nts>
                  <nts id="Seg_2407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_2409" n="HIAT:w" s="T434">kürbiʔi</ts>
                  <nts id="Seg_2410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_2412" n="HIAT:w" s="T435">kartačkanə</ts>
                  <nts id="Seg_2413" n="HIAT:ip">.</nts>
                  <nts id="Seg_2414" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T444" id="Seg_2416" n="HIAT:u" s="T436">
                  <ts e="T437" id="Seg_2418" n="HIAT:w" s="T436">Dĭn</ts>
                  <nts id="Seg_2419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_2421" n="HIAT:w" s="T437">amnobiam</ts>
                  <nts id="Seg_2422" n="HIAT:ip">,</nts>
                  <nts id="Seg_2423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_2425" n="HIAT:w" s="T438">dʼăbaktərbiam</ts>
                  <nts id="Seg_2426" n="HIAT:ip">,</nts>
                  <nts id="Seg_2427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_2429" n="HIAT:w" s="T439">dĭn</ts>
                  <nts id="Seg_2430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_2432" n="HIAT:w" s="T440">tože</ts>
                  <nts id="Seg_2433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_2435" n="HIAT:w" s="T441">ugandə</ts>
                  <nts id="Seg_2436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_2438" n="HIAT:w" s="T442">jakše</ts>
                  <nts id="Seg_2439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_2441" n="HIAT:w" s="T443">il</ts>
                  <nts id="Seg_2442" n="HIAT:ip">.</nts>
                  <nts id="Seg_2443" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T454" id="Seg_2445" n="HIAT:u" s="T444">
                  <ts e="T445" id="Seg_2447" n="HIAT:w" s="T444">Dĭgəttə</ts>
                  <nts id="Seg_2448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2449" n="HIAT:ip">(</nts>
                  <ts e="T446" id="Seg_2451" n="HIAT:w" s="T445">dĭzeŋ=</ts>
                  <nts id="Seg_2452" n="HIAT:ip">)</nts>
                  <nts id="Seg_2453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_2455" n="HIAT:w" s="T446">dĭzeŋdə</ts>
                  <nts id="Seg_2456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_2458" n="HIAT:w" s="T447">mămbiam:</ts>
                  <nts id="Seg_2459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2460" n="HIAT:ip">"</nts>
                  <nts id="Seg_2461" n="HIAT:ip">(</nts>
                  <ts e="T449" id="Seg_2463" n="HIAT:w" s="T448">kudaj=</ts>
                  <nts id="Seg_2464" n="HIAT:ip">)</nts>
                  <nts id="Seg_2465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_2467" n="HIAT:w" s="T449">kudaj</ts>
                  <nts id="Seg_2468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_2470" n="HIAT:w" s="T450">šiʔsi</ts>
                  <nts id="Seg_2471" n="HIAT:ip">"</nts>
                  <nts id="Seg_2472" n="HIAT:ip">,</nts>
                  <nts id="Seg_2473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_2475" n="HIAT:w" s="T451">i</ts>
                  <nts id="Seg_2476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2477" n="HIAT:ip">(</nts>
                  <ts e="T453" id="Seg_2479" n="HIAT:w" s="T452">bosəltə</ts>
                  <nts id="Seg_2480" n="HIAT:ip">)</nts>
                  <nts id="Seg_2481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_2483" n="HIAT:w" s="T453">kambiam</ts>
                  <nts id="Seg_2484" n="HIAT:ip">.</nts>
                  <nts id="Seg_2485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T463" id="Seg_2487" n="HIAT:u" s="T454">
                  <ts e="T455" id="Seg_2489" n="HIAT:w" s="T454">Dĭgəttə</ts>
                  <nts id="Seg_2490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_2492" n="HIAT:w" s="T455">dĭn</ts>
                  <nts id="Seg_2493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_2495" n="HIAT:w" s="T456">nʼüʔnən</ts>
                  <nts id="Seg_2496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2497" n="HIAT:ip">(</nts>
                  <ts e="T458" id="Seg_2499" n="HIAT:w" s="T457">ibie-</ts>
                  <nts id="Seg_2500" n="HIAT:ip">)</nts>
                  <nts id="Seg_2501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_2503" n="HIAT:w" s="T458">ibiem</ts>
                  <nts id="Seg_2504" n="HIAT:ip">,</nts>
                  <nts id="Seg_2505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_2507" n="HIAT:w" s="T459">dĭgəttə</ts>
                  <nts id="Seg_2508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_2510" n="HIAT:w" s="T460">nubibaʔ</ts>
                  <nts id="Seg_2511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_2513" n="HIAT:w" s="T461">dʼünə</ts>
                  <nts id="Seg_2514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2515" n="HIAT:ip">(</nts>
                  <ts e="T463" id="Seg_2517" n="HIAT:w" s="T462">öʔlüʔpiʔi</ts>
                  <nts id="Seg_2518" n="HIAT:ip">)</nts>
                  <nts id="Seg_2519" n="HIAT:ip">.</nts>
                  <nts id="Seg_2520" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T468" id="Seg_2522" n="HIAT:u" s="T463">
                  <ts e="T464" id="Seg_2524" n="HIAT:w" s="T463">Gəttə</ts>
                  <nts id="Seg_2525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_2527" n="HIAT:w" s="T464">kambibaʔ</ts>
                  <nts id="Seg_2528" n="HIAT:ip">,</nts>
                  <nts id="Seg_2529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_2531" n="HIAT:w" s="T465">gijen</ts>
                  <nts id="Seg_2532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2533" n="HIAT:ip">(</nts>
                  <ts e="T467" id="Seg_2535" n="HIAT:w" s="T466">k-</ts>
                  <nts id="Seg_2536" n="HIAT:ip">)</nts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_2539" n="HIAT:w" s="T467">krospaʔi</ts>
                  <nts id="Seg_2540" n="HIAT:ip">.</nts>
                  <nts id="Seg_2541" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T471" id="Seg_2543" n="HIAT:u" s="T468">
                  <ts e="T469" id="Seg_2545" n="HIAT:w" s="T468">Dĭn</ts>
                  <nts id="Seg_2546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_2548" n="HIAT:w" s="T469">tože</ts>
                  <nts id="Seg_2549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_2551" n="HIAT:w" s="T470">mĭmbibeʔ</ts>
                  <nts id="Seg_2552" n="HIAT:ip">.</nts>
                  <nts id="Seg_2553" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T474" id="Seg_2555" n="HIAT:u" s="T471">
                  <ts e="T472" id="Seg_2557" n="HIAT:w" s="T471">Iʔgö</ts>
                  <nts id="Seg_2558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_2560" n="HIAT:w" s="T472">nezeŋ</ts>
                  <nts id="Seg_2561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_2563" n="HIAT:w" s="T473">ibiʔi</ts>
                  <nts id="Seg_2564" n="HIAT:ip">.</nts>
                  <nts id="Seg_2565" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T479" id="Seg_2567" n="HIAT:u" s="T474">
                  <ts e="T475" id="Seg_2569" n="HIAT:w" s="T474">I</ts>
                  <nts id="Seg_2570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2571" n="HIAT:ip">(</nts>
                  <ts e="T476" id="Seg_2573" n="HIAT:w" s="T475">onʼiʔ=</ts>
                  <nts id="Seg_2574" n="HIAT:ip">)</nts>
                  <nts id="Seg_2575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_2577" n="HIAT:w" s="T476">šide</ts>
                  <nts id="Seg_2578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_2580" n="HIAT:w" s="T477">tibi</ts>
                  <nts id="Seg_2581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_2583" n="HIAT:w" s="T478">ibiʔi</ts>
                  <nts id="Seg_2584" n="HIAT:ip">.</nts>
                  <nts id="Seg_2585" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T486" id="Seg_2587" n="HIAT:u" s="T479">
                  <ts e="T480" id="Seg_2589" n="HIAT:w" s="T479">Tože</ts>
                  <nts id="Seg_2590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_2592" n="HIAT:w" s="T480">bar</ts>
                  <nts id="Seg_2593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_2595" n="HIAT:w" s="T481">măna</ts>
                  <nts id="Seg_2596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_2598" n="HIAT:w" s="T482">pănarbiʔi</ts>
                  <nts id="Seg_2599" n="HIAT:ip">,</nts>
                  <nts id="Seg_2600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_2602" n="HIAT:w" s="T483">udaʔi</ts>
                  <nts id="Seg_2603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2604" n="HIAT:ip">(</nts>
                  <ts e="T485" id="Seg_2606" n="HIAT:w" s="T484">mĭmbiʔi=</ts>
                  <nts id="Seg_2607" n="HIAT:ip">)</nts>
                  <nts id="Seg_2608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_2610" n="HIAT:w" s="T485">mĭbiʔi</ts>
                  <nts id="Seg_2611" n="HIAT:ip">.</nts>
                  <nts id="Seg_2612" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T490" id="Seg_2614" n="HIAT:u" s="T486">
                  <ts e="T487" id="Seg_2616" n="HIAT:w" s="T486">Măndəʔi:</ts>
                  <nts id="Seg_2617" n="HIAT:ip">"</nts>
                  <nts id="Seg_2618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_2620" n="HIAT:w" s="T487">Jakše</ts>
                  <nts id="Seg_2621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_2623" n="HIAT:w" s="T488">igel</ts>
                  <nts id="Seg_2624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_2626" n="HIAT:w" s="T489">ugandə</ts>
                  <nts id="Seg_2627" n="HIAT:ip">.</nts>
                  <nts id="Seg_2628" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T492" id="Seg_2630" n="HIAT:u" s="T490">
                  <ts e="T491" id="Seg_2632" n="HIAT:w" s="T490">Kuŋgə</ts>
                  <nts id="Seg_2633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_2635" n="HIAT:w" s="T491">šobial</ts>
                  <nts id="Seg_2636" n="HIAT:ip">?</nts>
                  <nts id="Seg_2637" n="HIAT:ip">"</nts>
                  <nts id="Seg_2638" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T496" id="Seg_2640" n="HIAT:u" s="T492">
                  <ts e="T493" id="Seg_2642" n="HIAT:w" s="T492">Măn</ts>
                  <nts id="Seg_2643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_2645" n="HIAT:w" s="T493">mămbiam:</ts>
                  <nts id="Seg_2646" n="HIAT:ip">"</nts>
                  <nts id="Seg_2647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_2649" n="HIAT:w" s="T494">Kuŋgə</ts>
                  <nts id="Seg_2650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_2652" n="HIAT:w" s="T495">šobiam</ts>
                  <nts id="Seg_2653" n="HIAT:ip">"</nts>
                  <nts id="Seg_2654" n="HIAT:ip">.</nts>
                  <nts id="Seg_2655" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T499" id="Seg_2657" n="HIAT:u" s="T496">
                  <nts id="Seg_2658" n="HIAT:ip">(</nts>
                  <ts e="T497" id="Seg_2660" n="HIAT:w" s="T496">Măngoliaʔi</ts>
                  <nts id="Seg_2661" n="HIAT:ip">)</nts>
                  <nts id="Seg_2662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_2664" n="HIAT:w" s="T497">tondə</ts>
                  <nts id="Seg_2665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_2667" n="HIAT:w" s="T498">amnobiam</ts>
                  <nts id="Seg_2668" n="HIAT:ip">.</nts>
                  <nts id="Seg_2669" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T500" id="Seg_2671" n="HIAT:u" s="T499">
                  <ts e="T500" id="Seg_2673" n="HIAT:w" s="T499">Kabarləj</ts>
                  <nts id="Seg_2674" n="HIAT:ip">.</nts>
                  <nts id="Seg_2675" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T861" id="Seg_2676" n="sc" s="T565">
               <ts e="T574" id="Seg_2678" n="HIAT:u" s="T565">
                  <ts e="T565.tx-PKZ.1" id="Seg_2680" n="HIAT:w" s="T565">Мы</ts>
                  <nts id="Seg_2681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565.tx-PKZ.2" id="Seg_2683" n="HIAT:w" s="T565.tx-PKZ.1">с</ts>
                  <nts id="Seg_2684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565.tx-PKZ.3" id="Seg_2686" n="HIAT:w" s="T565.tx-PKZ.2">этой</ts>
                  <nts id="Seg_2687" n="HIAT:ip">,</nts>
                  <nts id="Seg_2688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565.tx-PKZ.4" id="Seg_2690" n="HIAT:w" s="T565.tx-PKZ.3">с</ts>
                  <nts id="Seg_2691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565.tx-PKZ.5" id="Seg_2693" n="HIAT:w" s="T565.tx-PKZ.4">Ридой</ts>
                  <nts id="Seg_2694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565.tx-PKZ.6" id="Seg_2696" n="HIAT:w" s="T565.tx-PKZ.5">ждали-ждали</ts>
                  <nts id="Seg_2697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2699" n="HIAT:w" s="T565.tx-PKZ.6">Арпита</ts>
                  <nts id="Seg_2700" n="HIAT:ip">.</nts>
                  <nts id="Seg_2701" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T576" id="Seg_2703" n="HIAT:u" s="T574">
                  <ts e="T575" id="Seg_2705" n="HIAT:w" s="T574">Нету</ts>
                  <nts id="Seg_2706" n="HIAT:ip">,</nts>
                  <nts id="Seg_2707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2709" n="HIAT:w" s="T575">нету</ts>
                  <nts id="Seg_2710" n="HIAT:ip">.</nts>
                  <nts id="Seg_2711" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T583" id="Seg_2713" n="HIAT:u" s="T576">
                  <ts e="T576.tx-PKZ.1" id="Seg_2715" n="HIAT:w" s="T576">Потом</ts>
                  <nts id="Seg_2716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576.tx-PKZ.2" id="Seg_2718" n="HIAT:w" s="T576.tx-PKZ.1">она</ts>
                  <nts id="Seg_2719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576.tx-PKZ.3" id="Seg_2721" n="HIAT:w" s="T576.tx-PKZ.2">сходила</ts>
                  <nts id="Seg_2722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576.tx-PKZ.4" id="Seg_2724" n="HIAT:w" s="T576.tx-PKZ.3">туды</ts>
                  <nts id="Seg_2725" n="HIAT:ip">,</nts>
                  <nts id="Seg_2726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576.tx-PKZ.5" id="Seg_2728" n="HIAT:w" s="T576.tx-PKZ.4">к</ts>
                  <nts id="Seg_2729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576.tx-PKZ.6" id="Seg_2731" n="HIAT:w" s="T576.tx-PKZ.5">евонной</ts>
                  <nts id="Seg_2732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2734" n="HIAT:w" s="T576.tx-PKZ.6">теще</ts>
                  <nts id="Seg_2735" n="HIAT:ip">.</nts>
                  <nts id="Seg_2736" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T585" id="Seg_2738" n="HIAT:u" s="T583">
                  <nts id="Seg_2739" n="HIAT:ip">"</nts>
                  <ts e="T584" id="Seg_2741" n="HIAT:w" s="T583">Пойду</ts>
                  <nts id="Seg_2742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2744" n="HIAT:w" s="T584">узнаю</ts>
                  <nts id="Seg_2745" n="HIAT:ip">"</nts>
                  <nts id="Seg_2746" n="HIAT:ip">.</nts>
                  <nts id="Seg_2747" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T597" id="Seg_2749" n="HIAT:u" s="T585">
                  <ts e="T585.tx-PKZ.1" id="Seg_2751" n="HIAT:w" s="T585">А</ts>
                  <nts id="Seg_2752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585.tx-PKZ.2" id="Seg_2754" n="HIAT:w" s="T585.tx-PKZ.1">я</ts>
                  <nts id="Seg_2755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585.tx-PKZ.3" id="Seg_2757" n="HIAT:w" s="T585.tx-PKZ.2">ее</ts>
                  <nts id="Seg_2758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585.tx-PKZ.4" id="Seg_2760" n="HIAT:w" s="T585.tx-PKZ.3">не</ts>
                  <nts id="Seg_2761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585.tx-PKZ.5" id="Seg_2763" n="HIAT:w" s="T585.tx-PKZ.4">пускаю:</ts>
                  <nts id="Seg_2764" n="HIAT:ip">"</nts>
                  <nts id="Seg_2765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585.tx-PKZ.6" id="Seg_2767" n="HIAT:w" s="T585.tx-PKZ.5">Не</ts>
                  <nts id="Seg_2768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585.tx-PKZ.7" id="Seg_2770" n="HIAT:w" s="T585.tx-PKZ.6">ходи</ts>
                  <nts id="Seg_2771" n="HIAT:ip">,</nts>
                  <nts id="Seg_2772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585.tx-PKZ.8" id="Seg_2774" n="HIAT:w" s="T585.tx-PKZ.7">говорю</ts>
                  <nts id="Seg_2775" n="HIAT:ip">,</nts>
                  <nts id="Seg_2776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585.tx-PKZ.9" id="Seg_2778" n="HIAT:w" s="T585.tx-PKZ.8">все</ts>
                  <nts id="Seg_2779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585.tx-PKZ.10" id="Seg_2781" n="HIAT:w" s="T585.tx-PKZ.9">равно</ts>
                  <nts id="Seg_2782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585.tx-PKZ.11" id="Seg_2784" n="HIAT:w" s="T585.tx-PKZ.10">он</ts>
                  <nts id="Seg_2785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2787" n="HIAT:w" s="T585.tx-PKZ.11">придет</ts>
                  <nts id="Seg_2788" n="HIAT:ip">.</nts>
                  <nts id="Seg_2789" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T603" id="Seg_2791" n="HIAT:u" s="T597">
                  <ts e="T597.tx-PKZ.1" id="Seg_2793" n="HIAT:w" s="T597">Разве</ts>
                  <nts id="Seg_2794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597.tx-PKZ.2" id="Seg_2796" n="HIAT:w" s="T597.tx-PKZ.1">только</ts>
                  <nts id="Seg_2797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597.tx-PKZ.3" id="Seg_2799" n="HIAT:w" s="T597.tx-PKZ.2">заболел</ts>
                  <nts id="Seg_2800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597.tx-PKZ.4" id="Seg_2802" n="HIAT:w" s="T597.tx-PKZ.3">дак</ts>
                  <nts id="Seg_2803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597.tx-PKZ.5" id="Seg_2805" n="HIAT:w" s="T597.tx-PKZ.4">не</ts>
                  <nts id="Seg_2806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2808" n="HIAT:w" s="T597.tx-PKZ.5">придет</ts>
                  <nts id="Seg_2809" n="HIAT:ip">.</nts>
                  <nts id="Seg_2810" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T609" id="Seg_2812" n="HIAT:u" s="T603">
                  <ts e="T603.tx-PKZ.1" id="Seg_2814" n="HIAT:w" s="T603">А</ts>
                  <nts id="Seg_2815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603.tx-PKZ.2" id="Seg_2817" n="HIAT:w" s="T603.tx-PKZ.1">то</ts>
                  <nts id="Seg_2818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603.tx-PKZ.3" id="Seg_2820" n="HIAT:w" s="T603.tx-PKZ.2">придет</ts>
                  <nts id="Seg_2821" n="HIAT:ip">,</nts>
                  <nts id="Seg_2822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603.tx-PKZ.4" id="Seg_2824" n="HIAT:w" s="T603.tx-PKZ.3">обманывать</ts>
                  <nts id="Seg_2825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603.tx-PKZ.5" id="Seg_2827" n="HIAT:w" s="T603.tx-PKZ.4">не</ts>
                  <nts id="Seg_2828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2830" n="HIAT:w" s="T603.tx-PKZ.5">будет</ts>
                  <nts id="Seg_2831" n="HIAT:ip">.</nts>
                  <nts id="Seg_2832" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T616" id="Seg_2834" n="HIAT:u" s="T609">
                  <ts e="T609.tx-PKZ.1" id="Seg_2836" n="HIAT:w" s="T609">Он</ts>
                  <nts id="Seg_2837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609.tx-PKZ.2" id="Seg_2839" n="HIAT:w" s="T609.tx-PKZ.1">же</ts>
                  <nts id="Seg_2840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609.tx-PKZ.3" id="Seg_2842" n="HIAT:w" s="T609.tx-PKZ.2">верующий</ts>
                  <nts id="Seg_2843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609.tx-PKZ.4" id="Seg_2845" n="HIAT:w" s="T609.tx-PKZ.3">человек</ts>
                  <nts id="Seg_2846" n="HIAT:ip">,</nts>
                  <nts id="Seg_2847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609.tx-PKZ.5" id="Seg_2849" n="HIAT:w" s="T609.tx-PKZ.4">ему</ts>
                  <nts id="Seg_2850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609.tx-PKZ.6" id="Seg_2852" n="HIAT:w" s="T609.tx-PKZ.5">нельзя</ts>
                  <nts id="Seg_2853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2855" n="HIAT:w" s="T609.tx-PKZ.6">обмануть</ts>
                  <nts id="Seg_2856" n="HIAT:ip">.</nts>
                  <nts id="Seg_2857" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T629" id="Seg_2859" n="HIAT:u" s="T616">
                  <ts e="T616.tx-PKZ.1" id="Seg_2861" n="HIAT:w" s="T616">Как</ts>
                  <nts id="Seg_2862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616.tx-PKZ.2" id="Seg_2864" n="HIAT:w" s="T616.tx-PKZ.1">он</ts>
                  <nts id="Seg_2865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616.tx-PKZ.3" id="Seg_2867" n="HIAT:w" s="T616.tx-PKZ.2">ко</ts>
                  <nts id="Seg_2868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616.tx-PKZ.4" id="Seg_2870" n="HIAT:w" s="T616.tx-PKZ.3">мне</ts>
                  <nts id="Seg_2871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616.tx-PKZ.5" id="Seg_2873" n="HIAT:w" s="T616.tx-PKZ.4">приехал</ts>
                  <nts id="Seg_2874" n="HIAT:ip">,</nts>
                  <nts id="Seg_2875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616.tx-PKZ.6" id="Seg_2877" n="HIAT:w" s="T616.tx-PKZ.5">я</ts>
                  <nts id="Seg_2878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616.tx-PKZ.7" id="Seg_2880" n="HIAT:w" s="T616.tx-PKZ.6">тоже</ts>
                  <nts id="Seg_2881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2883" n="HIAT:w" s="T616.tx-PKZ.7">писала:</ts>
                  <nts id="Seg_2884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624.tx-PKZ.1" id="Seg_2886" n="HIAT:w" s="T624">Когда</ts>
                  <nts id="Seg_2887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624.tx-PKZ.2" id="Seg_2889" n="HIAT:w" s="T624.tx-PKZ.1">приедет</ts>
                  <nts id="Seg_2890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624.tx-PKZ.3" id="Seg_2892" n="HIAT:w" s="T624.tx-PKZ.2">Агафон</ts>
                  <nts id="Seg_2893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624.tx-PKZ.4" id="Seg_2895" n="HIAT:w" s="T624.tx-PKZ.3">Иванович</ts>
                  <nts id="Seg_2896" n="HIAT:ip">,</nts>
                  <nts id="Seg_2897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2899" n="HIAT:w" s="T624.tx-PKZ.4">приеду</ts>
                  <nts id="Seg_2900" n="HIAT:ip">.</nts>
                  <nts id="Seg_2901" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T633" id="Seg_2903" n="HIAT:u" s="T629">
                  <ts e="T629.tx-PKZ.1" id="Seg_2905" n="HIAT:w" s="T629">Он</ts>
                  <nts id="Seg_2906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629.tx-PKZ.2" id="Seg_2908" n="HIAT:w" s="T629.tx-PKZ.1">приехал</ts>
                  <nts id="Seg_2909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629.tx-PKZ.3" id="Seg_2911" n="HIAT:w" s="T629.tx-PKZ.2">ко</ts>
                  <nts id="Seg_2912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2914" n="HIAT:w" s="T629.tx-PKZ.3">мне</ts>
                  <nts id="Seg_2915" n="HIAT:ip">.</nts>
                  <nts id="Seg_2916" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T647" id="Seg_2918" n="HIAT:u" s="T633">
                  <ts e="T634" id="Seg_2920" n="HIAT:w" s="T633">Ну</ts>
                  <nts id="Seg_2921" n="HIAT:ip">,</nts>
                  <nts id="Seg_2922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2924" n="HIAT:w" s="T634">я</ts>
                  <nts id="Seg_2925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2927" n="HIAT:w" s="T635">раз</ts>
                  <nts id="Seg_2928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2930" n="HIAT:w" s="T636">обещалась</ts>
                  <nts id="Seg_2931" n="HIAT:ip">,</nts>
                  <nts id="Seg_2932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2934" n="HIAT:w" s="T637">ни</ts>
                  <nts id="Seg_2935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2937" n="HIAT:w" s="T638">на</ts>
                  <nts id="Seg_2938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2940" n="HIAT:w" s="T639">что</ts>
                  <nts id="Seg_2941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2943" n="HIAT:w" s="T640">не</ts>
                  <nts id="Seg_2944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2946" n="HIAT:w" s="T641">погляделась</ts>
                  <nts id="Seg_2947" n="HIAT:ip">,</nts>
                  <nts id="Seg_2948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2950" n="HIAT:w" s="T642">собралась</ts>
                  <nts id="Seg_2951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2953" n="HIAT:w" s="T643">и</ts>
                  <nts id="Seg_2954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2956" n="HIAT:w" s="T644">с</ts>
                  <nts id="Seg_2957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2959" n="HIAT:w" s="T645">им</ts>
                  <nts id="Seg_2960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2962" n="HIAT:w" s="T646">поехала</ts>
                  <nts id="Seg_2963" n="HIAT:ip">.</nts>
                  <nts id="Seg_2964" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T654" id="Seg_2966" n="HIAT:u" s="T647">
                  <ts e="T647.tx-PKZ.1" id="Seg_2968" n="HIAT:w" s="T647">Приехали</ts>
                  <nts id="Seg_2969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647.tx-PKZ.2" id="Seg_2971" n="HIAT:w" s="T647.tx-PKZ.1">в</ts>
                  <nts id="Seg_2972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647.tx-PKZ.3" id="Seg_2974" n="HIAT:w" s="T647.tx-PKZ.2">Агинска</ts>
                  <nts id="Seg_2975" n="HIAT:ip">,</nts>
                  <nts id="Seg_2976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647.tx-PKZ.4" id="Seg_2978" n="HIAT:w" s="T647.tx-PKZ.3">там</ts>
                  <nts id="Seg_2979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647.tx-PKZ.5" id="Seg_2981" n="HIAT:w" s="T647.tx-PKZ.4">тоже</ts>
                  <nts id="Seg_2982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647.tx-PKZ.6" id="Seg_2984" n="HIAT:w" s="T647.tx-PKZ.5">наши</ts>
                  <nts id="Seg_2985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2987" n="HIAT:w" s="T647.tx-PKZ.6">верующие</ts>
                  <nts id="Seg_2988" n="HIAT:ip">.</nts>
                  <nts id="Seg_2989" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T667" id="Seg_2991" n="HIAT:u" s="T654">
                  <ts e="T654.tx-PKZ.1" id="Seg_2993" n="HIAT:w" s="T654">Там</ts>
                  <nts id="Seg_2994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654.tx-PKZ.2" id="Seg_2996" n="HIAT:w" s="T654.tx-PKZ.1">я</ts>
                  <nts id="Seg_2997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654.tx-PKZ.3" id="Seg_2999" n="HIAT:w" s="T654.tx-PKZ.2">ему</ts>
                  <nts id="Seg_3000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654.tx-PKZ.4" id="Seg_3002" n="HIAT:w" s="T654.tx-PKZ.3">предлагала</ts>
                  <nts id="Seg_3003" n="HIAT:ip">,</nts>
                  <nts id="Seg_3004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654.tx-PKZ.5" id="Seg_3006" n="HIAT:w" s="T654.tx-PKZ.4">говорю:</ts>
                  <nts id="Seg_3007" n="HIAT:ip">"</nts>
                  <nts id="Seg_3008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654.tx-PKZ.6" id="Seg_3010" n="HIAT:w" s="T654.tx-PKZ.5">Приезжай</ts>
                  <nts id="Seg_3011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654.tx-PKZ.7" id="Seg_3013" n="HIAT:w" s="T654.tx-PKZ.6">к</ts>
                  <nts id="Seg_3014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654.tx-PKZ.8" id="Seg_3016" n="HIAT:w" s="T654.tx-PKZ.7">нам</ts>
                  <nts id="Seg_3017" n="HIAT:ip">,</nts>
                  <nts id="Seg_3018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654.tx-PKZ.9" id="Seg_3020" n="HIAT:w" s="T654.tx-PKZ.8">мы</ts>
                  <nts id="Seg_3021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654.tx-PKZ.10" id="Seg_3023" n="HIAT:w" s="T654.tx-PKZ.9">тебе</ts>
                  <nts id="Seg_3024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654.tx-PKZ.11" id="Seg_3026" n="HIAT:w" s="T654.tx-PKZ.10">тут</ts>
                  <nts id="Seg_3027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654.tx-PKZ.12" id="Seg_3029" n="HIAT:w" s="T654.tx-PKZ.11">дом</ts>
                  <nts id="Seg_3030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_3032" n="HIAT:w" s="T654.tx-PKZ.12">купим</ts>
                  <nts id="Seg_3033" n="HIAT:ip">"</nts>
                  <nts id="Seg_3034" n="HIAT:ip">.</nts>
                  <nts id="Seg_3035" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T673" id="Seg_3037" n="HIAT:u" s="T667">
                  <ts e="T668" id="Seg_3039" n="HIAT:w" s="T667">Ну</ts>
                  <nts id="Seg_3040" n="HIAT:ip">,</nts>
                  <nts id="Seg_3041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_3043" n="HIAT:w" s="T668">оттель</ts>
                  <nts id="Seg_3044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_3046" n="HIAT:w" s="T669">мы</ts>
                  <nts id="Seg_3047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_3049" n="HIAT:w" s="T670">пять</ts>
                  <nts id="Seg_3050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_3052" n="HIAT:w" s="T671">часов</ts>
                  <nts id="Seg_3053" n="HIAT:ip">…</nts>
                  <nts id="Seg_3054" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T682" id="Seg_3056" n="HIAT:u" s="T673">
                  <ts e="T673.tx-PKZ.1" id="Seg_3058" n="HIAT:w" s="T673">Нас</ts>
                  <nts id="Seg_3059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673.tx-PKZ.2" id="Seg_3061" n="HIAT:w" s="T673.tx-PKZ.1">проводили</ts>
                  <nts id="Seg_3062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673.tx-PKZ.3" id="Seg_3064" n="HIAT:w" s="T673.tx-PKZ.2">тоже</ts>
                  <nts id="Seg_3065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673.tx-PKZ.4" id="Seg_3067" n="HIAT:w" s="T673.tx-PKZ.3">сестры</ts>
                  <nts id="Seg_3068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673.tx-PKZ.5" id="Seg_3070" n="HIAT:w" s="T673.tx-PKZ.4">верующие</ts>
                  <nts id="Seg_3071" n="HIAT:ip">,</nts>
                  <nts id="Seg_3072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673.tx-PKZ.6" id="Seg_3074" n="HIAT:w" s="T673.tx-PKZ.5">мы</ts>
                  <nts id="Seg_3075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673.tx-PKZ.7" id="Seg_3077" n="HIAT:w" s="T673.tx-PKZ.6">приехали</ts>
                  <nts id="Seg_3078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673.tx-PKZ.8" id="Seg_3080" n="HIAT:w" s="T673.tx-PKZ.7">в</ts>
                  <nts id="Seg_3081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_3083" n="HIAT:w" s="T673.tx-PKZ.8">Уяр</ts>
                  <nts id="Seg_3084" n="HIAT:ip">.</nts>
                  <nts id="Seg_3085" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T696" id="Seg_3087" n="HIAT:u" s="T682">
                  <ts e="T683" id="Seg_3089" n="HIAT:w" s="T682">Ну</ts>
                  <nts id="Seg_3090" n="HIAT:ip">,</nts>
                  <nts id="Seg_3091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_3093" n="HIAT:w" s="T683">он</ts>
                  <nts id="Seg_3094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_3096" n="HIAT:w" s="T684">-</ts>
                  <nts id="Seg_3097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_3099" n="HIAT:w" s="T685">то</ts>
                  <nts id="Seg_3100" n="HIAT:ip">,</nts>
                  <nts id="Seg_3101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_3103" n="HIAT:w" s="T686">конечно</ts>
                  <nts id="Seg_3104" n="HIAT:ip">,</nts>
                  <nts id="Seg_3105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_3107" n="HIAT:w" s="T687">не</ts>
                  <nts id="Seg_3108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_3110" n="HIAT:w" s="T688">знал</ts>
                  <nts id="Seg_3111" n="HIAT:ip">,</nts>
                  <nts id="Seg_3112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_3114" n="HIAT:w" s="T689">где</ts>
                  <nts id="Seg_3115" n="HIAT:ip">,</nts>
                  <nts id="Seg_3116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_3118" n="HIAT:w" s="T690">а</ts>
                  <nts id="Seg_3119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_3121" n="HIAT:w" s="T691">я</ts>
                  <nts id="Seg_3122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_3124" n="HIAT:w" s="T692">там</ts>
                  <nts id="Seg_3125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_3127" n="HIAT:w" s="T693">знаю</ts>
                  <nts id="Seg_3128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_3130" n="HIAT:w" s="T694">своих</ts>
                  <nts id="Seg_3131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_3133" n="HIAT:w" s="T695">верующих</ts>
                  <nts id="Seg_3134" n="HIAT:ip">.</nts>
                  <nts id="Seg_3135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T700" id="Seg_3137" n="HIAT:u" s="T696">
                  <ts e="T697" id="Seg_3139" n="HIAT:w" s="T696">Ну</ts>
                  <nts id="Seg_3140" n="HIAT:ip">,</nts>
                  <nts id="Seg_3141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_3143" n="HIAT:w" s="T697">пошли</ts>
                  <nts id="Seg_3144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_3146" n="HIAT:w" s="T698">с</ts>
                  <nts id="Seg_3147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_3149" n="HIAT:w" s="T699">им</ts>
                  <nts id="Seg_3150" n="HIAT:ip">.</nts>
                  <nts id="Seg_3151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T714" id="Seg_3153" n="HIAT:u" s="T700">
                  <ts e="T700.tx-PKZ.1" id="Seg_3155" n="HIAT:w" s="T700">У</ts>
                  <nts id="Seg_3156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700.tx-PKZ.2" id="Seg_3158" n="HIAT:w" s="T700.tx-PKZ.1">людей</ts>
                  <nts id="Seg_3159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700.tx-PKZ.3" id="Seg_3161" n="HIAT:w" s="T700.tx-PKZ.2">спрашиваю</ts>
                  <nts id="Seg_3162" n="HIAT:ip">,</nts>
                  <nts id="Seg_3163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700.tx-PKZ.4" id="Seg_3165" n="HIAT:w" s="T700.tx-PKZ.3">а</ts>
                  <nts id="Seg_3166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700.tx-PKZ.5" id="Seg_3168" n="HIAT:w" s="T700.tx-PKZ.4">фамилие-то</ts>
                  <nts id="Seg_3169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700.tx-PKZ.6" id="Seg_3171" n="HIAT:w" s="T700.tx-PKZ.5">я</ts>
                  <nts id="Seg_3172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700.tx-PKZ.7" id="Seg_3174" n="HIAT:w" s="T700.tx-PKZ.6">забыла</ts>
                  <nts id="Seg_3175" n="HIAT:ip">,</nts>
                  <nts id="Seg_3176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700.tx-PKZ.8" id="Seg_3178" n="HIAT:w" s="T700.tx-PKZ.7">только</ts>
                  <nts id="Seg_3179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700.tx-PKZ.9" id="Seg_3181" n="HIAT:w" s="T700.tx-PKZ.8">знаю:</ts>
                  <nts id="Seg_3182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700.tx-PKZ.10" id="Seg_3184" n="HIAT:w" s="T700.tx-PKZ.9">Илюша</ts>
                  <nts id="Seg_3185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700.tx-PKZ.11" id="Seg_3187" n="HIAT:w" s="T700.tx-PKZ.10">и</ts>
                  <nts id="Seg_3188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_3190" n="HIAT:w" s="T700.tx-PKZ.11">Маруся</ts>
                  <nts id="Seg_3191" n="HIAT:ip">.</nts>
                  <nts id="Seg_3192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T725" id="Seg_3194" n="HIAT:u" s="T714">
                  <ts e="T715" id="Seg_3196" n="HIAT:w" s="T714">Но</ts>
                  <nts id="Seg_3197" n="HIAT:ip">,</nts>
                  <nts id="Seg_3198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3199" n="HIAT:ip">(</nts>
                  <ts e="T716" id="Seg_3201" n="HIAT:w" s="T715">зн</ts>
                  <nts id="Seg_3202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_3204" n="HIAT:w" s="T716">-</ts>
                  <nts id="Seg_3205" n="HIAT:ip">)</nts>
                  <nts id="Seg_3206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_3208" n="HIAT:w" s="T717">место</ts>
                  <nts id="Seg_3209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_3211" n="HIAT:w" s="T718">-</ts>
                  <nts id="Seg_3212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_3214" n="HIAT:w" s="T719">то</ts>
                  <nts id="Seg_3215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_3217" n="HIAT:w" s="T720">знаю</ts>
                  <nts id="Seg_3218" n="HIAT:ip">,</nts>
                  <nts id="Seg_3219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_3221" n="HIAT:w" s="T721">заметно</ts>
                  <nts id="Seg_3222" n="HIAT:ip">,</nts>
                  <nts id="Seg_3223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_3225" n="HIAT:w" s="T722">была</ts>
                  <nts id="Seg_3226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_3228" n="HIAT:w" s="T723">я</ts>
                  <nts id="Seg_3229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_3231" n="HIAT:w" s="T724">там</ts>
                  <nts id="Seg_3232" n="HIAT:ip">.</nts>
                  <nts id="Seg_3233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T733" id="Seg_3235" n="HIAT:u" s="T725">
                  <ts e="T725.tx-PKZ.1" id="Seg_3237" n="HIAT:w" s="T725">Арпиту</ts>
                  <nts id="Seg_3238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725.tx-PKZ.2" id="Seg_3240" n="HIAT:w" s="T725.tx-PKZ.1">говорю:</ts>
                  <nts id="Seg_3241" n="HIAT:ip">"</nts>
                  <nts id="Seg_3242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725.tx-PKZ.3" id="Seg_3244" n="HIAT:w" s="T725.tx-PKZ.2">Вы</ts>
                  <nts id="Seg_3245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725.tx-PKZ.4" id="Seg_3247" n="HIAT:w" s="T725.tx-PKZ.3">постойте</ts>
                  <nts id="Seg_3248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725.tx-PKZ.5" id="Seg_3250" n="HIAT:w" s="T725.tx-PKZ.4">тут</ts>
                  <nts id="Seg_3251" n="HIAT:ip">,</nts>
                  <nts id="Seg_3252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725.tx-PKZ.6" id="Seg_3254" n="HIAT:w" s="T725.tx-PKZ.5">а</ts>
                  <nts id="Seg_3255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725.tx-PKZ.7" id="Seg_3257" n="HIAT:w" s="T725.tx-PKZ.6">я</ts>
                  <nts id="Seg_3258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_3260" n="HIAT:w" s="T725.tx-PKZ.7">зайду</ts>
                  <nts id="Seg_3261" n="HIAT:ip">"</nts>
                  <nts id="Seg_3262" n="HIAT:ip">.</nts>
                  <nts id="Seg_3263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T743" id="Seg_3265" n="HIAT:u" s="T733">
                  <nts id="Seg_3266" n="HIAT:ip">(</nts>
                  <ts e="T734" id="Seg_3268" n="HIAT:w" s="T733">За</ts>
                  <nts id="Seg_3269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_3271" n="HIAT:w" s="T734">-</ts>
                  <nts id="Seg_3272" n="HIAT:ip">)</nts>
                  <nts id="Seg_3273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_3275" n="HIAT:w" s="T735">Захожу</ts>
                  <nts id="Seg_3276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_3278" n="HIAT:w" s="T736">в</ts>
                  <nts id="Seg_3279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_3281" n="HIAT:w" s="T737">первую</ts>
                  <nts id="Seg_3282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3283" n="HIAT:ip">(</nts>
                  <ts e="T739" id="Seg_3285" n="HIAT:w" s="T738">и</ts>
                  <nts id="Seg_3286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_3288" n="HIAT:w" s="T739">-</ts>
                  <nts id="Seg_3289" n="HIAT:ip">)</nts>
                  <nts id="Seg_3290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_3292" n="HIAT:w" s="T740">в</ts>
                  <nts id="Seg_3293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_3295" n="HIAT:w" s="T741">перво</ts>
                  <nts id="Seg_3296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_3298" n="HIAT:w" s="T742">помещение</ts>
                  <nts id="Seg_3299" n="HIAT:ip">.</nts>
                  <nts id="Seg_3300" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T755" id="Seg_3302" n="HIAT:u" s="T743">
                  <nts id="Seg_3303" n="HIAT:ip">"</nts>
                  <nts id="Seg_3304" n="HIAT:ip">(</nts>
                  <ts e="T744" id="Seg_3306" n="HIAT:w" s="T743">Не</ts>
                  <nts id="Seg_3307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_3309" n="HIAT:w" s="T744">з</ts>
                  <nts id="Seg_3310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_3312" n="HIAT:w" s="T745">-</ts>
                  <nts id="Seg_3313" n="HIAT:ip">)</nts>
                  <nts id="Seg_3314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_3316" n="HIAT:w" s="T746">Не</ts>
                  <nts id="Seg_3317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_3319" n="HIAT:w" s="T747">заходи</ts>
                  <nts id="Seg_3320" n="HIAT:ip">,</nts>
                  <nts id="Seg_3321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_3323" n="HIAT:w" s="T748">тут</ts>
                  <nts id="Seg_3324" n="HIAT:ip">,</nts>
                  <nts id="Seg_3325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_3327" n="HIAT:w" s="T749">-</ts>
                  <nts id="Seg_3328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_3330" n="HIAT:w" s="T750">говорит</ts>
                  <nts id="Seg_3331" n="HIAT:ip">,</nts>
                  <nts id="Seg_3332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_3334" n="HIAT:w" s="T751">-</ts>
                  <nts id="Seg_3335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_3337" n="HIAT:w" s="T752">никто</ts>
                  <nts id="Seg_3338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_3340" n="HIAT:w" s="T753">не</ts>
                  <nts id="Seg_3341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_3343" n="HIAT:w" s="T754">живет</ts>
                  <nts id="Seg_3344" n="HIAT:ip">"</nts>
                  <nts id="Seg_3345" n="HIAT:ip">.</nts>
                  <nts id="Seg_3346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T772" id="Seg_3348" n="HIAT:u" s="T755">
                  <ts e="T755.tx-PKZ.1" id="Seg_3350" n="HIAT:w" s="T755">Потом</ts>
                  <nts id="Seg_3351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755.tx-PKZ.2" id="Seg_3353" n="HIAT:w" s="T755.tx-PKZ.1">туды</ts>
                  <nts id="Seg_3354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755.tx-PKZ.3" id="Seg_3356" n="HIAT:w" s="T755.tx-PKZ.2">дальше</ts>
                  <nts id="Seg_3357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755.tx-PKZ.4" id="Seg_3359" n="HIAT:w" s="T755.tx-PKZ.3">пошла</ts>
                  <nts id="Seg_3360" n="HIAT:ip">,</nts>
                  <nts id="Seg_3361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755.tx-PKZ.5" id="Seg_3363" n="HIAT:w" s="T755.tx-PKZ.4">оттель</ts>
                  <nts id="Seg_3364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755.tx-PKZ.6" id="Seg_3366" n="HIAT:w" s="T755.tx-PKZ.5">спросила</ts>
                  <nts id="Seg_3367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755.tx-PKZ.7" id="Seg_3369" n="HIAT:w" s="T755.tx-PKZ.6">у</ts>
                  <nts id="Seg_3370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_3372" n="HIAT:w" s="T755.tx-PKZ.7">человека:</ts>
                  <nts id="Seg_3373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763.tx-PKZ.1" id="Seg_3375" n="HIAT:w" s="T763">Я</ts>
                  <nts id="Seg_3376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763.tx-PKZ.2" id="Seg_3378" n="HIAT:w" s="T763.tx-PKZ.1">говорю:</ts>
                  <nts id="Seg_3379" n="HIAT:ip">"</nts>
                  <nts id="Seg_3380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763.tx-PKZ.3" id="Seg_3382" n="HIAT:w" s="T763.tx-PKZ.2">Знаете</ts>
                  <nts id="Seg_3383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763.tx-PKZ.4" id="Seg_3385" n="HIAT:w" s="T763.tx-PKZ.3">Илюшу</ts>
                  <nts id="Seg_3386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3387" n="HIAT:ip">(</nts>
                  <ts e="T763.tx-PKZ.5" id="Seg_3389" n="HIAT:w" s="T763.tx-PKZ.4">и</ts>
                  <nts id="Seg_3390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763.tx-PKZ.6" id="Seg_3392" n="HIAT:w" s="T763.tx-PKZ.5">М</ts>
                  <nts id="Seg_3393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763.tx-PKZ.7" id="Seg_3395" n="HIAT:w" s="T763.tx-PKZ.6">-</ts>
                  <nts id="Seg_3396" n="HIAT:ip">)</nts>
                  <nts id="Seg_3397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763.tx-PKZ.8" id="Seg_3399" n="HIAT:w" s="T763.tx-PKZ.7">с</ts>
                  <nts id="Seg_3400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_3402" n="HIAT:w" s="T763.tx-PKZ.8">Марусей</ts>
                  <nts id="Seg_3403" n="HIAT:ip">?</nts>
                  <nts id="Seg_3404" n="HIAT:ip">"</nts>
                  <nts id="Seg_3405" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T781" id="Seg_3407" n="HIAT:u" s="T772">
                  <ts e="T772.tx-PKZ.1" id="Seg_3409" n="HIAT:w" s="T772">Он</ts>
                  <nts id="Seg_3410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772.tx-PKZ.2" id="Seg_3412" n="HIAT:w" s="T772.tx-PKZ.1">говорит:</ts>
                  <nts id="Seg_3413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3414" n="HIAT:ip">"</nts>
                  <ts e="T772.tx-PKZ.3" id="Seg_3416" n="HIAT:w" s="T772.tx-PKZ.2">Оне</ts>
                  <nts id="Seg_3417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772.tx-PKZ.4" id="Seg_3419" n="HIAT:w" s="T772.tx-PKZ.3">со</ts>
                  <nts id="Seg_3420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772.tx-PKZ.5" id="Seg_3422" n="HIAT:w" s="T772.tx-PKZ.4">мной</ts>
                  <nts id="Seg_3423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772.tx-PKZ.6" id="Seg_3425" n="HIAT:w" s="T772.tx-PKZ.5">работают</ts>
                  <nts id="Seg_3426" n="HIAT:ip">"</nts>
                  <nts id="Seg_3427" n="HIAT:ip">,</nts>
                  <nts id="Seg_3428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772.tx-PKZ.7" id="Seg_3430" n="HIAT:w" s="T772.tx-PKZ.6">и</ts>
                  <nts id="Seg_3431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772.tx-PKZ.8" id="Seg_3433" n="HIAT:w" s="T772.tx-PKZ.7">меня</ts>
                  <nts id="Seg_3434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_3436" n="HIAT:w" s="T772.tx-PKZ.8">повел</ts>
                  <nts id="Seg_3437" n="HIAT:ip">.</nts>
                  <nts id="Seg_3438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T800" id="Seg_3440" n="HIAT:u" s="T781">
                  <ts e="T781.tx-PKZ.1" id="Seg_3442" n="HIAT:w" s="T781">А</ts>
                  <nts id="Seg_3443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781.tx-PKZ.2" id="Seg_3445" n="HIAT:w" s="T781.tx-PKZ.1">я</ts>
                  <nts id="Seg_3446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781.tx-PKZ.3" id="Seg_3448" n="HIAT:w" s="T781.tx-PKZ.2">брату</ts>
                  <nts id="Seg_3449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781.tx-PKZ.4" id="Seg_3451" n="HIAT:w" s="T781.tx-PKZ.3">сказала:</ts>
                  <nts id="Seg_3452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3453" n="HIAT:ip">"</nts>
                  <nts id="Seg_3454" n="HIAT:ip">(</nts>
                  <ts e="T781.tx-PKZ.5" id="Seg_3456" n="HIAT:w" s="T781.tx-PKZ.4">И</ts>
                  <nts id="Seg_3457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781.tx-PKZ.6" id="Seg_3459" n="HIAT:w" s="T781.tx-PKZ.5">-</ts>
                  <nts id="Seg_3460" n="HIAT:ip">)</nts>
                  <nts id="Seg_3461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781.tx-PKZ.7" id="Seg_3463" n="HIAT:w" s="T781.tx-PKZ.6">Иди</ts>
                  <nts id="Seg_3464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781.tx-PKZ.8" id="Seg_3466" n="HIAT:w" s="T781.tx-PKZ.7">сюда</ts>
                  <nts id="Seg_3467" n="HIAT:ip">"</nts>
                  <nts id="Seg_3468" n="HIAT:ip">,</nts>
                  <nts id="Seg_3469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781.tx-PKZ.9" id="Seg_3471" n="HIAT:w" s="T781.tx-PKZ.8">и</ts>
                  <nts id="Seg_3472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781.tx-PKZ.10" id="Seg_3474" n="HIAT:w" s="T781.tx-PKZ.9">он</ts>
                  <nts id="Seg_3475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781.tx-PKZ.11" id="Seg_3477" n="HIAT:w" s="T781.tx-PKZ.10">пришел</ts>
                  <nts id="Seg_3478" n="HIAT:ip">,</nts>
                  <nts id="Seg_3479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781.tx-PKZ.12" id="Seg_3481" n="HIAT:w" s="T781.tx-PKZ.11">и</ts>
                  <nts id="Seg_3482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781.tx-PKZ.13" id="Seg_3484" n="HIAT:w" s="T781.tx-PKZ.12">как</ts>
                  <nts id="Seg_3485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781.tx-PKZ.14" id="Seg_3487" n="HIAT:w" s="T781.tx-PKZ.13">раз</ts>
                  <nts id="Seg_3488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781.tx-PKZ.15" id="Seg_3490" n="HIAT:w" s="T781.tx-PKZ.14">она</ts>
                  <nts id="Seg_3491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781.tx-PKZ.16" id="Seg_3493" n="HIAT:w" s="T781.tx-PKZ.15">идет</ts>
                  <nts id="Seg_3494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781.tx-PKZ.17" id="Seg_3496" n="HIAT:w" s="T781.tx-PKZ.16">тут</ts>
                  <nts id="Seg_3497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781.tx-PKZ.18" id="Seg_3499" n="HIAT:w" s="T781.tx-PKZ.17">с</ts>
                  <nts id="Seg_3500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_3502" n="HIAT:w" s="T781.tx-PKZ.18">водой</ts>
                  <nts id="Seg_3503" n="HIAT:ip">.</nts>
                  <nts id="Seg_3504" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T822" id="Seg_3506" n="HIAT:u" s="T800">
                  <ts e="T800.tx-PKZ.1" id="Seg_3508" n="HIAT:w" s="T800">И</ts>
                  <nts id="Seg_3509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800.tx-PKZ.2" id="Seg_3511" n="HIAT:w" s="T800.tx-PKZ.1">мы</ts>
                  <nts id="Seg_3512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800.tx-PKZ.3" id="Seg_3514" n="HIAT:w" s="T800.tx-PKZ.2">тут</ts>
                  <nts id="Seg_3515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800.tx-PKZ.4" id="Seg_3517" n="HIAT:w" s="T800.tx-PKZ.3">тоже</ts>
                  <nts id="Seg_3518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800.tx-PKZ.5" id="Seg_3520" n="HIAT:w" s="T800.tx-PKZ.4">долго</ts>
                  <nts id="Seg_3521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800.tx-PKZ.6" id="Seg_3523" n="HIAT:w" s="T800.tx-PKZ.5">беседовали</ts>
                  <nts id="Seg_3524" n="HIAT:ip">,</nts>
                  <nts id="Seg_3525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800.tx-PKZ.7" id="Seg_3527" n="HIAT:w" s="T800.tx-PKZ.6">там</ts>
                  <nts id="Seg_3528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800.tx-PKZ.8" id="Seg_3530" n="HIAT:w" s="T800.tx-PKZ.7">еще</ts>
                  <nts id="Seg_3531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800.tx-PKZ.9" id="Seg_3533" n="HIAT:w" s="T800.tx-PKZ.8">один</ts>
                  <nts id="Seg_3534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800.tx-PKZ.10" id="Seg_3536" n="HIAT:w" s="T800.tx-PKZ.9">наш</ts>
                  <nts id="Seg_3537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800.tx-PKZ.11" id="Seg_3539" n="HIAT:w" s="T800.tx-PKZ.10">брат</ts>
                  <nts id="Seg_3540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800.tx-PKZ.12" id="Seg_3542" n="HIAT:w" s="T800.tx-PKZ.11">приходил</ts>
                  <nts id="Seg_3543" n="HIAT:ip">,</nts>
                  <nts id="Seg_3544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800.tx-PKZ.13" id="Seg_3546" n="HIAT:w" s="T800.tx-PKZ.12">такой</ts>
                  <nts id="Seg_3547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800.tx-PKZ.14" id="Seg_3549" n="HIAT:w" s="T800.tx-PKZ.13">же</ts>
                  <nts id="Seg_3550" n="HIAT:ip">,</nts>
                  <nts id="Seg_3551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800.tx-PKZ.15" id="Seg_3553" n="HIAT:w" s="T800.tx-PKZ.14">как</ts>
                  <nts id="Seg_3554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800.tx-PKZ.16" id="Seg_3556" n="HIAT:w" s="T800.tx-PKZ.15">брат</ts>
                  <nts id="Seg_3557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800.tx-PKZ.17" id="Seg_3559" n="HIAT:w" s="T800.tx-PKZ.16">Арпит</ts>
                  <nts id="Seg_3560" n="HIAT:ip">,</nts>
                  <nts id="Seg_3561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3562" n="HIAT:ip">(</nts>
                  <ts e="T800.tx-PKZ.18" id="Seg_3564" n="HIAT:w" s="T800.tx-PKZ.17">он=</ts>
                  <nts id="Seg_3565" n="HIAT:ip">)</nts>
                  <nts id="Seg_3566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800.tx-PKZ.19" id="Seg_3568" n="HIAT:w" s="T800.tx-PKZ.18">я</ts>
                  <nts id="Seg_3569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800.tx-PKZ.20" id="Seg_3571" n="HIAT:w" s="T800.tx-PKZ.19">там</ts>
                  <nts id="Seg_3572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800.tx-PKZ.21" id="Seg_3574" n="HIAT:w" s="T800.tx-PKZ.20">часто</ts>
                  <nts id="Seg_3575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_3577" n="HIAT:w" s="T800.tx-PKZ.21">бываю</ts>
                  <nts id="Seg_3578" n="HIAT:ip">.</nts>
                  <nts id="Seg_3579" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T832" id="Seg_3581" n="HIAT:u" s="T822">
                  <ts e="T822.tx-PKZ.1" id="Seg_3583" n="HIAT:w" s="T822">Потому</ts>
                  <nts id="Seg_3584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822.tx-PKZ.2" id="Seg_3586" n="HIAT:w" s="T822.tx-PKZ.1">что</ts>
                  <nts id="Seg_3587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822.tx-PKZ.3" id="Seg_3589" n="HIAT:w" s="T822.tx-PKZ.2">у</ts>
                  <nts id="Seg_3590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822.tx-PKZ.4" id="Seg_3592" n="HIAT:w" s="T822.tx-PKZ.3">нас</ts>
                  <nts id="Seg_3593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822.tx-PKZ.5" id="Seg_3595" n="HIAT:w" s="T822.tx-PKZ.4">в</ts>
                  <nts id="Seg_3596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822.tx-PKZ.6" id="Seg_3598" n="HIAT:w" s="T822.tx-PKZ.5">деревне</ts>
                  <nts id="Seg_3599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822.tx-PKZ.7" id="Seg_3601" n="HIAT:w" s="T822.tx-PKZ.6">таких</ts>
                  <nts id="Seg_3602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822.tx-PKZ.8" id="Seg_3604" n="HIAT:w" s="T822.tx-PKZ.7">нету</ts>
                  <nts id="Seg_3605" n="HIAT:ip">,</nts>
                  <nts id="Seg_3606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822.tx-PKZ.9" id="Seg_3608" n="HIAT:w" s="T822.tx-PKZ.8">как</ts>
                  <nts id="Seg_3609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3611" n="HIAT:w" s="T822.tx-PKZ.9">я</ts>
                  <nts id="Seg_3612" n="HIAT:ip">.</nts>
                  <nts id="Seg_3613" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T848" id="Seg_3615" n="HIAT:u" s="T832">
                  <ts e="T832.tx-PKZ.1" id="Seg_3617" n="HIAT:w" s="T832">Ну</ts>
                  <nts id="Seg_3618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832.tx-PKZ.2" id="Seg_3620" n="HIAT:w" s="T832.tx-PKZ.1">я</ts>
                  <nts id="Seg_3621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832.tx-PKZ.3" id="Seg_3623" n="HIAT:w" s="T832.tx-PKZ.2">ли</ts>
                  <nts id="Seg_3624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832.tx-PKZ.4" id="Seg_3626" n="HIAT:w" s="T832.tx-PKZ.3">езжу</ts>
                  <nts id="Seg_3627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832.tx-PKZ.5" id="Seg_3629" n="HIAT:w" s="T832.tx-PKZ.4">в</ts>
                  <nts id="Seg_3630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832.tx-PKZ.6" id="Seg_3632" n="HIAT:w" s="T832.tx-PKZ.5">Красноярска</ts>
                  <nts id="Seg_3633" n="HIAT:ip">,</nts>
                  <nts id="Seg_3634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832.tx-PKZ.7" id="Seg_3636" n="HIAT:w" s="T832.tx-PKZ.6">и</ts>
                  <nts id="Seg_3637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832.tx-PKZ.8" id="Seg_3639" n="HIAT:w" s="T832.tx-PKZ.7">там</ts>
                  <nts id="Seg_3640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832.tx-PKZ.9" id="Seg_3642" n="HIAT:w" s="T832.tx-PKZ.8">у</ts>
                  <nts id="Seg_3643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832.tx-PKZ.10" id="Seg_3645" n="HIAT:w" s="T832.tx-PKZ.9">нас</ts>
                  <nts id="Seg_3646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832.tx-PKZ.11" id="Seg_3648" n="HIAT:w" s="T832.tx-PKZ.10">еще</ts>
                  <nts id="Seg_3649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832.tx-PKZ.12" id="Seg_3651" n="HIAT:w" s="T832.tx-PKZ.11">есть</ts>
                  <nts id="Seg_3652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832.tx-PKZ.13" id="Seg_3654" n="HIAT:w" s="T832.tx-PKZ.12">разъезд</ts>
                  <nts id="Seg_3655" n="HIAT:ip">,</nts>
                  <nts id="Seg_3656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832.tx-PKZ.14" id="Seg_3658" n="HIAT:w" s="T832.tx-PKZ.13">туда</ts>
                  <nts id="Seg_3659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832.tx-PKZ.15" id="Seg_3661" n="HIAT:w" s="T832.tx-PKZ.14">я</ts>
                  <nts id="Seg_3662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_3664" n="HIAT:w" s="T832.tx-PKZ.15">езжу</ts>
                  <nts id="Seg_3665" n="HIAT:ip">.</nts>
                  <nts id="Seg_3666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T850" id="Seg_3668" n="HIAT:u" s="T848">
                  <ts e="T848.tx-PKZ.1" id="Seg_3670" n="HIAT:w" s="T848">Заозерка</ts>
                  <nts id="Seg_3671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_3673" n="HIAT:w" s="T848.tx-PKZ.1">станция</ts>
                  <nts id="Seg_3674" n="HIAT:ip">.</nts>
                  <nts id="Seg_3675" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T856" id="Seg_3677" n="HIAT:u" s="T850">
                  <ts e="T851" id="Seg_3679" n="HIAT:w" s="T850">Ты</ts>
                  <nts id="Seg_3680" n="HIAT:ip">,</nts>
                  <nts id="Seg_3681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_3683" n="HIAT:w" s="T851">наверное</ts>
                  <nts id="Seg_3684" n="HIAT:ip">,</nts>
                  <nts id="Seg_3685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_3687" n="HIAT:w" s="T852">может</ts>
                  <nts id="Seg_3688" n="HIAT:ip">,</nts>
                  <nts id="Seg_3689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_3691" n="HIAT:w" s="T853">был</ts>
                  <nts id="Seg_3692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_3694" n="HIAT:w" s="T854">там</ts>
                  <nts id="Seg_3695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3696" n="HIAT:ip">(</nts>
                  <ts e="T856" id="Seg_3698" n="HIAT:w" s="T855">эка</ts>
                  <nts id="Seg_3699" n="HIAT:ip">)</nts>
                  <nts id="Seg_3700" n="HIAT:ip">?</nts>
                  <nts id="Seg_3701" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T861" id="Seg_3703" n="HIAT:u" s="T856">
                  <ts e="T856.tx-PKZ.1" id="Seg_3705" n="HIAT:w" s="T856">Не</ts>
                  <nts id="Seg_3706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856.tx-PKZ.2" id="Seg_3708" n="HIAT:w" s="T856.tx-PKZ.1">был</ts>
                  <nts id="Seg_3709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856.tx-PKZ.3" id="Seg_3711" n="HIAT:w" s="T856.tx-PKZ.2">на</ts>
                  <nts id="Seg_3712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856.tx-PKZ.4" id="Seg_3714" n="HIAT:w" s="T856.tx-PKZ.3">станции</ts>
                  <nts id="Seg_3715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_3717" n="HIAT:w" s="T856.tx-PKZ.4">Заозерки</ts>
                  <nts id="Seg_3718" n="HIAT:ip">?</nts>
                  <nts id="Seg_3719" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T876" id="Seg_3720" n="sc" s="T864">
               <ts e="T876" id="Seg_3722" n="HIAT:u" s="T864">
                  <ts e="T864.tx-PKZ.1" id="Seg_3724" n="HIAT:w" s="T864">Оттель</ts>
                  <nts id="Seg_3725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864.tx-PKZ.2" id="Seg_3727" n="HIAT:w" s="T864.tx-PKZ.1">тоже</ts>
                  <nts id="Seg_3728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864.tx-PKZ.3" id="Seg_3730" n="HIAT:w" s="T864.tx-PKZ.2">можно</ts>
                  <nts id="Seg_3731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864.tx-PKZ.4" id="Seg_3733" n="HIAT:w" s="T864.tx-PKZ.3">в</ts>
                  <nts id="Seg_3734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864.tx-PKZ.5" id="Seg_3736" n="HIAT:w" s="T864.tx-PKZ.4">Красноярска</ts>
                  <nts id="Seg_3737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864.tx-PKZ.6" id="Seg_3739" n="HIAT:w" s="T864.tx-PKZ.5">уехать</ts>
                  <nts id="Seg_3740" n="HIAT:ip">,</nts>
                  <nts id="Seg_3741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864.tx-PKZ.7" id="Seg_3743" n="HIAT:w" s="T864.tx-PKZ.6">и</ts>
                  <nts id="Seg_3744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864.tx-PKZ.8" id="Seg_3746" n="HIAT:w" s="T864.tx-PKZ.7">в</ts>
                  <nts id="Seg_3747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864.tx-PKZ.9" id="Seg_3749" n="HIAT:w" s="T864.tx-PKZ.8">Канска</ts>
                  <nts id="Seg_3750" n="HIAT:ip">,</nts>
                  <nts id="Seg_3751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864.tx-PKZ.10" id="Seg_3753" n="HIAT:w" s="T864.tx-PKZ.9">в</ts>
                  <nts id="Seg_3754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864.tx-PKZ.11" id="Seg_3756" n="HIAT:w" s="T864.tx-PKZ.10">город</ts>
                  <nts id="Seg_3757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_3759" n="HIAT:w" s="T864.tx-PKZ.11">Канск</ts>
                  <nts id="Seg_3760" n="HIAT:ip">.</nts>
                  <nts id="Seg_3761" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T891" id="Seg_3762" n="sc" s="T878">
               <ts e="T885" id="Seg_3764" n="HIAT:u" s="T878">
                  <ts e="T879" id="Seg_3766" n="HIAT:w" s="T878">Ну</ts>
                  <nts id="Seg_3767" n="HIAT:ip">,</nts>
                  <nts id="Seg_3768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T880" id="Seg_3770" n="HIAT:w" s="T879">я</ts>
                  <nts id="Seg_3771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T881" id="Seg_3773" n="HIAT:w" s="T880">в</ts>
                  <nts id="Seg_3774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_3776" n="HIAT:w" s="T881">Канске</ts>
                  <nts id="Seg_3777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_3779" n="HIAT:w" s="T882">тоже</ts>
                  <nts id="Seg_3780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_3782" n="HIAT:w" s="T883">была</ts>
                  <nts id="Seg_3783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_3785" n="HIAT:w" s="T884">там</ts>
                  <nts id="Seg_3786" n="HIAT:ip">.</nts>
                  <nts id="Seg_3787" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T891" id="Seg_3789" n="HIAT:u" s="T885">
                  <ts e="T885.tx-PKZ.1" id="Seg_3791" n="HIAT:w" s="T885">Тоже</ts>
                  <nts id="Seg_3792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885.tx-PKZ.2" id="Seg_3794" n="HIAT:w" s="T885.tx-PKZ.1">там</ts>
                  <nts id="Seg_3795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885.tx-PKZ.3" id="Seg_3797" n="HIAT:w" s="T885.tx-PKZ.2">есть</ts>
                  <nts id="Seg_3798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885.tx-PKZ.4" id="Seg_3800" n="HIAT:w" s="T885.tx-PKZ.3">верующие</ts>
                  <nts id="Seg_3801" n="HIAT:ip">,</nts>
                  <nts id="Seg_3802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885.tx-PKZ.5" id="Seg_3804" n="HIAT:w" s="T885.tx-PKZ.4">там</ts>
                  <nts id="Seg_3805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_3807" n="HIAT:w" s="T885.tx-PKZ.5">была</ts>
                  <nts id="Seg_3808" n="HIAT:ip">.</nts>
                  <nts id="Seg_3809" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T894" id="Seg_3810" n="sc" s="T892">
               <ts e="T894" id="Seg_3812" n="HIAT:u" s="T892">
                  <ts e="T894" id="Seg_3814" n="HIAT:w" s="T892">Так</ts>
                  <nts id="Seg_3815" n="HIAT:ip">…</nts>
                  <nts id="Seg_3816" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T938" id="Seg_3817" n="sc" s="T928">
               <ts e="T938" id="Seg_3819" n="HIAT:u" s="T928">
                  <ts e="T928.tx-PKZ.1" id="Seg_3821" n="HIAT:w" s="T928">Я</ts>
                  <nts id="Seg_3822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T928.tx-PKZ.2" id="Seg_3824" n="HIAT:w" s="T928.tx-PKZ.1">уже</ts>
                  <nts id="Seg_3825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T928.tx-PKZ.3" id="Seg_3827" n="HIAT:w" s="T928.tx-PKZ.2">много</ts>
                  <nts id="Seg_3828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T928.tx-PKZ.4" id="Seg_3830" n="HIAT:w" s="T928.tx-PKZ.3">наговорила</ts>
                  <nts id="Seg_3831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T938" id="Seg_3833" n="HIAT:w" s="T928.tx-PKZ.4">по-русски</ts>
                  <nts id="Seg_3834" n="HIAT:ip">.</nts>
                  <nts id="Seg_3835" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1054" id="Seg_3836" n="sc" s="T943">
               <ts e="T956" id="Seg_3838" n="HIAT:u" s="T943">
                  <ts e="T944" id="Seg_3840" n="HIAT:w" s="T943">Nu</ts>
                  <nts id="Seg_3841" n="HIAT:ip">,</nts>
                  <nts id="Seg_3842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T945" id="Seg_3844" n="HIAT:w" s="T944">miʔ</ts>
                  <nts id="Seg_3845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_3847" n="HIAT:w" s="T945">taldʼen</ts>
                  <nts id="Seg_3848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3849" n="HIAT:ip">(</nts>
                  <ts e="T947" id="Seg_3851" n="HIAT:w" s="T946">Vidazi</ts>
                  <nts id="Seg_3852" n="HIAT:ip">)</nts>
                  <nts id="Seg_3853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_3855" n="HIAT:w" s="T947">edəʔleʔbəbeʔ</ts>
                  <nts id="Seg_3856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T949" id="Seg_3858" n="HIAT:w" s="T948">üge</ts>
                  <nts id="Seg_3859" n="HIAT:ip">,</nts>
                  <nts id="Seg_3860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T950" id="Seg_3862" n="HIAT:w" s="T949">kagam</ts>
                  <nts id="Seg_3863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T951" id="Seg_3865" n="HIAT:w" s="T950">edəʔleʔbə</ts>
                  <nts id="Seg_3866" n="HIAT:ip">,</nts>
                  <nts id="Seg_3867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3868" n="HIAT:ip">(</nts>
                  <ts e="T952" id="Seg_3870" n="HIAT:w" s="T951">gə</ts>
                  <nts id="Seg_3871" n="HIAT:ip">)</nts>
                  <nts id="Seg_3872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T953" id="Seg_3874" n="HIAT:w" s="T952">naga</ts>
                  <nts id="Seg_3875" n="HIAT:ip">,</nts>
                  <nts id="Seg_3876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T954" id="Seg_3878" n="HIAT:w" s="T953">naga</ts>
                  <nts id="Seg_3879" n="HIAT:ip">,</nts>
                  <nts id="Seg_3880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T956" id="Seg_3882" n="HIAT:w" s="T954">dĭgəttə</ts>
                  <nts id="Seg_3883" n="HIAT:ip">…</nts>
                  <nts id="Seg_3884" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T962" id="Seg_3886" n="HIAT:u" s="T956">
                  <ts e="T957" id="Seg_3888" n="HIAT:w" s="T956">Bazaj</ts>
                  <nts id="Seg_3889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T958" id="Seg_3891" n="HIAT:w" s="T957">aʔtʼi</ts>
                  <nts id="Seg_3892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3893" n="HIAT:ip">(</nts>
                  <ts e="T959" id="Seg_3895" n="HIAT:w" s="T958">küzürleʔbə</ts>
                  <nts id="Seg_3896" n="HIAT:ip">)</nts>
                  <nts id="Seg_3897" n="HIAT:ip">,</nts>
                  <nts id="Seg_3898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T960" id="Seg_3900" n="HIAT:w" s="T959">măn</ts>
                  <nts id="Seg_3901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T961" id="Seg_3903" n="HIAT:w" s="T960">măndəm:</ts>
                  <nts id="Seg_3904" n="HIAT:ip">"</nts>
                  <nts id="Seg_3905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T962" id="Seg_3907" n="HIAT:w" s="T961">Šonəga</ts>
                  <nts id="Seg_3908" n="HIAT:ip">"</nts>
                  <nts id="Seg_3909" n="HIAT:ip">.</nts>
                  <nts id="Seg_3910" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T974" id="Seg_3912" n="HIAT:u" s="T962">
                  <ts e="T963" id="Seg_3914" n="HIAT:w" s="T962">Nu</ts>
                  <nts id="Seg_3915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3916" n="HIAT:ip">(</nts>
                  <ts e="T964" id="Seg_3918" n="HIAT:w" s="T963">n-</ts>
                  <nts id="Seg_3919" n="HIAT:ip">)</nts>
                  <nts id="Seg_3920" n="HIAT:ip">,</nts>
                  <nts id="Seg_3921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_3923" n="HIAT:w" s="T964">dĭ</ts>
                  <nts id="Seg_3924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T966" id="Seg_3926" n="HIAT:w" s="T965">šobi</ts>
                  <nts id="Seg_3927" n="HIAT:ip">,</nts>
                  <nts id="Seg_3928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T967" id="Seg_3930" n="HIAT:w" s="T966">možet</ts>
                  <nts id="Seg_3931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T968" id="Seg_3933" n="HIAT:w" s="T967">iandə</ts>
                  <nts id="Seg_3934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T969" id="Seg_3936" n="HIAT:w" s="T968">kambi</ts>
                  <nts id="Seg_3937" n="HIAT:ip">,</nts>
                  <nts id="Seg_3938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T970" id="Seg_3940" n="HIAT:w" s="T969">dĭn</ts>
                  <nts id="Seg_3941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T971" id="Seg_3943" n="HIAT:w" s="T970">ambi</ts>
                  <nts id="Seg_3944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T972" id="Seg_3946" n="HIAT:w" s="T971">da</ts>
                  <nts id="Seg_3947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3948" n="HIAT:ip">(</nts>
                  <ts e="T973" id="Seg_3950" n="HIAT:w" s="T972">iʔbi-</ts>
                  <nts id="Seg_3951" n="HIAT:ip">)</nts>
                  <nts id="Seg_3952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T974" id="Seg_3954" n="HIAT:w" s="T973">iʔbəbi</ts>
                  <nts id="Seg_3955" n="HIAT:ip">.</nts>
                  <nts id="Seg_3956" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T976" id="Seg_3958" n="HIAT:u" s="T974">
                  <nts id="Seg_3959" n="HIAT:ip">(</nts>
                  <ts e="T975" id="Seg_3961" n="HIAT:w" s="T974">Šalaʔjə</ts>
                  <nts id="Seg_3962" n="HIAT:ip">)</nts>
                  <nts id="Seg_3963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T976" id="Seg_3965" n="HIAT:w" s="T975">kunolzittə</ts>
                  <nts id="Seg_3966" n="HIAT:ip">.</nts>
                  <nts id="Seg_3967" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T985" id="Seg_3969" n="HIAT:u" s="T976">
                  <ts e="T977" id="Seg_3971" n="HIAT:w" s="T976">Dĭgəttə</ts>
                  <nts id="Seg_3972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T978" id="Seg_3974" n="HIAT:w" s="T977">amnobibaʔ</ts>
                  <nts id="Seg_3975" n="HIAT:ip">,</nts>
                  <nts id="Seg_3976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T979" id="Seg_3978" n="HIAT:w" s="T978">i</ts>
                  <nts id="Seg_3979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T980" id="Seg_3981" n="HIAT:w" s="T979">deʔpibeʔ</ts>
                  <nts id="Seg_3982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3983" n="HIAT:ip">(</nts>
                  <ts e="T981" id="Seg_3985" n="HIAT:w" s="T980">deʔpibeʔ</ts>
                  <nts id="Seg_3986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T982" id="Seg_3988" n="HIAT:w" s="T981">ig-</ts>
                  <nts id="Seg_3989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T983" id="Seg_3991" n="HIAT:w" s="T982">li-</ts>
                  <nts id="Seg_3992" n="HIAT:ip">)</nts>
                  <nts id="Seg_3993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T984" id="Seg_3995" n="HIAT:w" s="T983">iʔbəbeʔ</ts>
                  <nts id="Seg_3996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T985" id="Seg_3998" n="HIAT:w" s="T984">kunolzittə</ts>
                  <nts id="Seg_3999" n="HIAT:ip">.</nts>
                  <nts id="Seg_4000" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T992" id="Seg_4002" n="HIAT:u" s="T985">
                  <ts e="T986" id="Seg_4004" n="HIAT:w" s="T985">Tolʼko</ts>
                  <nts id="Seg_4005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4006" n="HIAT:ip">(</nts>
                  <ts e="T987" id="Seg_4008" n="HIAT:w" s="T986">iʔbəbeʔ=</ts>
                  <nts id="Seg_4009" n="HIAT:ip">)</nts>
                  <nts id="Seg_4010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T988" id="Seg_4012" n="HIAT:w" s="T987">iʔpibeʔ</ts>
                  <nts id="Seg_4013" n="HIAT:ip">,</nts>
                  <nts id="Seg_4014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T989" id="Seg_4016" n="HIAT:w" s="T988">măn</ts>
                  <nts id="Seg_4017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T990" id="Seg_4019" n="HIAT:w" s="T989">aʔpi</ts>
                  <nts id="Seg_4020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T991" id="Seg_4022" n="HIAT:w" s="T990">i</ts>
                  <nts id="Seg_4023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T992" id="Seg_4025" n="HIAT:w" s="T991">kunolbiam</ts>
                  <nts id="Seg_4026" n="HIAT:ip">.</nts>
                  <nts id="Seg_4027" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T996" id="Seg_4029" n="HIAT:u" s="T992">
                  <ts e="T993" id="Seg_4031" n="HIAT:w" s="T992">Nu</ts>
                  <nts id="Seg_4032" n="HIAT:ip">,</nts>
                  <nts id="Seg_4033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T994" id="Seg_4035" n="HIAT:w" s="T993">днем</ts>
                  <nts id="Seg_4036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T995" id="Seg_4038" n="HIAT:w" s="T994">šonəga</ts>
                  <nts id="Seg_4039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T996" id="Seg_4041" n="HIAT:w" s="T995">šindidə</ts>
                  <nts id="Seg_4042" n="HIAT:ip">.</nts>
                  <nts id="Seg_4043" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1003" id="Seg_4045" n="HIAT:u" s="T996">
                  <ts e="T997" id="Seg_4047" n="HIAT:w" s="T996">Šolambi</ts>
                  <nts id="Seg_4048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4049" n="HIAT:ip">(</nts>
                  <nts id="Seg_4050" n="HIAT:ip">(</nts>
                  <ats e="T998" id="Seg_4051" n="HIAT:non-pho" s="T997">…</ats>
                  <nts id="Seg_4052" n="HIAT:ip">)</nts>
                  <nts id="Seg_4053" n="HIAT:ip">)</nts>
                  <nts id="Seg_4054" n="HIAT:ip">,</nts>
                  <nts id="Seg_4055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T999" id="Seg_4057" n="HIAT:w" s="T998">măn</ts>
                  <nts id="Seg_4058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1000" id="Seg_4060" n="HIAT:w" s="T999">suʔmiluʔpiam</ts>
                  <nts id="Seg_4061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1001" id="Seg_4063" n="HIAT:w" s="T1000">da</ts>
                  <nts id="Seg_4064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1002" id="Seg_4066" n="HIAT:w" s="T1001">dĭbər</ts>
                  <nts id="Seg_4067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1003" id="Seg_4069" n="HIAT:w" s="T1002">nuʔməluʔpiam</ts>
                  <nts id="Seg_4070" n="HIAT:ip">.</nts>
                  <nts id="Seg_4071" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1006" id="Seg_4073" n="HIAT:u" s="T1003">
                  <ts e="T1004" id="Seg_4075" n="HIAT:w" s="T1003">Dĭnə</ts>
                  <nts id="Seg_4076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1005" id="Seg_4078" n="HIAT:w" s="T1004">udam</ts>
                  <nts id="Seg_4079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1006" id="Seg_4081" n="HIAT:w" s="T1005">mĭbiem</ts>
                  <nts id="Seg_4082" n="HIAT:ip">.</nts>
                  <nts id="Seg_4083" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1013" id="Seg_4085" n="HIAT:u" s="T1006">
                  <nts id="Seg_4086" n="HIAT:ip">(</nts>
                  <ts e="T1007" id="Seg_4088" n="HIAT:w" s="T1006">Dʼăbaktərbibeʔ</ts>
                  <nts id="Seg_4089" n="HIAT:ip">)</nts>
                  <nts id="Seg_4090" n="HIAT:ip">,</nts>
                  <nts id="Seg_4091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4092" n="HIAT:ip">(</nts>
                  <ts e="T1008" id="Seg_4094" n="HIAT:w" s="T1007">dĭ</ts>
                  <nts id="Seg_4095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1008.tx-PKZ.1" id="Seg_4097" n="HIAT:w" s="T1008">măndə</ts>
                  <nts id="Seg_4098" n="HIAT:ip">)</nts>
                  <ts e="T1009" id="Seg_4100" n="HIAT:w" s="T1008.tx-PKZ.1">:</ts>
                  <nts id="Seg_4101" n="HIAT:ip">"</nts>
                  <nts id="Seg_4102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1010" id="Seg_4104" n="HIAT:w" s="T1009">Măn</ts>
                  <nts id="Seg_4105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1011" id="Seg_4107" n="HIAT:w" s="T1010">băzəjdlam</ts>
                  <nts id="Seg_4108" n="HIAT:ip">,</nts>
                  <nts id="Seg_4109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1012" id="Seg_4111" n="HIAT:w" s="T1011">vannan</ts>
                  <nts id="Seg_4112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1013" id="Seg_4114" n="HIAT:w" s="T1012">kalam</ts>
                  <nts id="Seg_4115" n="HIAT:ip">"</nts>
                  <nts id="Seg_4116" n="HIAT:ip">.</nts>
                  <nts id="Seg_4117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1015" id="Seg_4119" n="HIAT:u" s="T1013">
                  <nts id="Seg_4120" n="HIAT:ip">"</nts>
                  <ts e="T1014" id="Seg_4122" n="HIAT:w" s="T1013">No</ts>
                  <nts id="Seg_4123" n="HIAT:ip">,</nts>
                  <nts id="Seg_4124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1015" id="Seg_4126" n="HIAT:w" s="T1014">kanaʔ</ts>
                  <nts id="Seg_4127" n="HIAT:ip">"</nts>
                  <nts id="Seg_4128" n="HIAT:ip">.</nts>
                  <nts id="Seg_4129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1018" id="Seg_4131" n="HIAT:u" s="T1015">
                  <ts e="T1016" id="Seg_4133" n="HIAT:w" s="T1015">Dĭgəttə</ts>
                  <nts id="Seg_4134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1017" id="Seg_4136" n="HIAT:w" s="T1016">dĭ</ts>
                  <nts id="Seg_4137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1018" id="Seg_4139" n="HIAT:w" s="T1017">kambi</ts>
                  <nts id="Seg_4140" n="HIAT:ip">.</nts>
                  <nts id="Seg_4141" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1023" id="Seg_4143" n="HIAT:u" s="T1018">
                  <ts e="T1019" id="Seg_4145" n="HIAT:w" s="T1018">Măn</ts>
                  <nts id="Seg_4146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1020" id="Seg_4148" n="HIAT:w" s="T1019">mămbiam:</ts>
                  <nts id="Seg_4149" n="HIAT:ip">"</nts>
                  <nts id="Seg_4150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1021" id="Seg_4152" n="HIAT:w" s="T1020">Amoraʔ</ts>
                  <nts id="Seg_4153" n="HIAT:ip">,</nts>
                  <nts id="Seg_4154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1022" id="Seg_4156" n="HIAT:w" s="T1021">dĭgəttə</ts>
                  <nts id="Seg_4157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1023" id="Seg_4159" n="HIAT:w" s="T1022">kalal</ts>
                  <nts id="Seg_4160" n="HIAT:ip">"</nts>
                  <nts id="Seg_4161" n="HIAT:ip">.</nts>
                  <nts id="Seg_4162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1037" id="Seg_4164" n="HIAT:u" s="T1023">
                  <ts e="T1024" id="Seg_4166" n="HIAT:w" s="T1023">Dĭgəttə</ts>
                  <nts id="Seg_4167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1025" id="Seg_4169" n="HIAT:w" s="T1024">dĭ</ts>
                  <nts id="Seg_4170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4171" n="HIAT:ip">(</nts>
                  <ts e="T1026" id="Seg_4173" n="HIAT:w" s="T1025">ejümləbi</ts>
                  <nts id="Seg_4174" n="HIAT:ip">)</nts>
                  <nts id="Seg_4175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1027" id="Seg_4177" n="HIAT:w" s="T1026">mĭj</ts>
                  <nts id="Seg_4178" n="HIAT:ip">,</nts>
                  <nts id="Seg_4179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1028" id="Seg_4181" n="HIAT:w" s="T1027">segi</ts>
                  <nts id="Seg_4182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1029" id="Seg_4184" n="HIAT:w" s="T1028">bü</ts>
                  <nts id="Seg_4185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4186" n="HIAT:ip">(</nts>
                  <nts id="Seg_4187" n="HIAT:ip">(</nts>
                  <ats e="T1030" id="Seg_4188" n="HIAT:non-pho" s="T1029">…</ats>
                  <nts id="Seg_4189" n="HIAT:ip">)</nts>
                  <nts id="Seg_4190" n="HIAT:ip">)</nts>
                  <nts id="Seg_4191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4192" n="HIAT:ip">(</nts>
                  <ts e="T1031" id="Seg_4194" n="HIAT:w" s="T1030">dĭ</ts>
                  <nts id="Seg_4195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1032" id="Seg_4197" n="HIAT:w" s="T1031">ambi</ts>
                  <nts id="Seg_4198" n="HIAT:ip">)</nts>
                  <nts id="Seg_4199" n="HIAT:ip">,</nts>
                  <nts id="Seg_4200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1033" id="Seg_4202" n="HIAT:w" s="T1032">dĭgəttə</ts>
                  <nts id="Seg_4203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4204" n="HIAT:ip">(</nts>
                  <ts e="T1034" id="Seg_4206" n="HIAT:w" s="T1033">ka-</ts>
                  <nts id="Seg_4207" n="HIAT:ip">)</nts>
                  <nts id="Seg_4208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1035" id="Seg_4210" n="HIAT:w" s="T1034">kambi</ts>
                  <nts id="Seg_4211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1036" id="Seg_4213" n="HIAT:w" s="T1035">băzəjdəsʼtə</ts>
                  <nts id="Seg_4214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1037" id="Seg_4216" n="HIAT:w" s="T1036">vannan</ts>
                  <nts id="Seg_4217" n="HIAT:ip">.</nts>
                  <nts id="Seg_4218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1042" id="Seg_4220" n="HIAT:u" s="T1037">
                  <ts e="T1038" id="Seg_4222" n="HIAT:w" s="T1037">Dĭgəttə</ts>
                  <nts id="Seg_4223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1039" id="Seg_4225" n="HIAT:w" s="T1038">dĭ</ts>
                  <nts id="Seg_4226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1040" id="Seg_4228" n="HIAT:w" s="T1039">dĭʔnə</ts>
                  <nts id="Seg_4229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1041" id="Seg_4231" n="HIAT:w" s="T1040">iʔbəsʼtə</ts>
                  <nts id="Seg_4232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1042" id="Seg_4234" n="HIAT:w" s="T1041">dʼajirbi</ts>
                  <nts id="Seg_4235" n="HIAT:ip">.</nts>
                  <nts id="Seg_4236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1049" id="Seg_4238" n="HIAT:u" s="T1042">
                  <ts e="T1043" id="Seg_4240" n="HIAT:w" s="T1042">Dön</ts>
                  <nts id="Seg_4241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1044" id="Seg_4243" n="HIAT:w" s="T1043">nuldəbi</ts>
                  <nts id="Seg_4244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4245" n="HIAT:ip">(</nts>
                  <ts e="T1045" id="Seg_4247" n="HIAT:w" s="T1044">кровать</ts>
                  <nts id="Seg_4248" n="HIAT:ip">,</nts>
                  <nts id="Seg_4249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1046" id="Seg_4251" n="HIAT:w" s="T1045">dĭ</ts>
                  <nts id="Seg_4252" n="HIAT:ip">)</nts>
                  <nts id="Seg_4253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1047" id="Seg_4255" n="HIAT:w" s="T1046">iʔbolaʔpi</ts>
                  <nts id="Seg_4256" n="HIAT:ip">,</nts>
                  <nts id="Seg_4257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1048" id="Seg_4259" n="HIAT:w" s="T1047">i</ts>
                  <nts id="Seg_4260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1049" id="Seg_4262" n="HIAT:w" s="T1048">kunollubibaʔ</ts>
                  <nts id="Seg_4263" n="HIAT:ip">.</nts>
                  <nts id="Seg_4264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1054" id="Seg_4266" n="HIAT:u" s="T1049">
                  <ts e="T1050" id="Seg_4268" n="HIAT:w" s="T1049">Ertən</ts>
                  <nts id="Seg_4269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1051" id="Seg_4271" n="HIAT:w" s="T1050">bazo</ts>
                  <nts id="Seg_4272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4273" n="HIAT:ip">(</nts>
                  <ts e="T1052" id="Seg_4275" n="HIAT:w" s="T1051">uʔbdə-</ts>
                  <nts id="Seg_4276" n="HIAT:ip">)</nts>
                  <nts id="Seg_4277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1054" id="Seg_4279" n="HIAT:w" s="T1052">uʔbdəbibaʔ</ts>
                  <nts id="Seg_4280" n="HIAT:ip">…</nts>
                  <nts id="Seg_4281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PKZ">
            <ts e="T124" id="Seg_4282" n="sc" s="T3">
               <ts e="T8" id="Seg_4284" n="e" s="T3">Американский наставник прислал нам бумагу. </ts>
               <ts e="T20" id="Seg_4286" n="e" s="T8">В нашей местности гнали (все-) верующих, шибко притесняли, молиться не давали. </ts>
               <ts e="T21" id="Seg_4288" n="e" s="T20">Ну, </ts>
               <ts e="T22" id="Seg_4290" n="e" s="T21">он </ts>
               <ts e="T23" id="Seg_4292" n="e" s="T22">услыхал, </ts>
               <ts e="T24" id="Seg_4294" n="e" s="T23">что </ts>
               <ts e="T25" id="Seg_4296" n="e" s="T24">так </ts>
               <ts e="T26" id="Seg_4298" n="e" s="T25">тут </ts>
               <ts e="T27" id="Seg_4300" n="e" s="T26">притеснения </ts>
               <ts e="T28" id="Seg_4302" n="e" s="T27">в </ts>
               <ts e="T29" id="Seg_4304" n="e" s="T28">нашей </ts>
               <ts e="T30" id="Seg_4306" n="e" s="T29">русской </ts>
               <ts e="T31" id="Seg_4308" n="e" s="T30">земле, </ts>
               <ts e="T32" id="Seg_4310" n="e" s="T31">стал </ts>
               <ts e="T33" id="Seg_4312" n="e" s="T32">просить </ts>
               <ts e="T34" id="Seg_4314" n="e" s="T33">у </ts>
               <ts e="T35" id="Seg_4316" n="e" s="T34">нашего </ts>
               <ts e="T36" id="Seg_4318" n="e" s="T35">правителя:" </ts>
               <ts e="T37" id="Seg_4320" n="e" s="T36">Отдай </ts>
               <ts e="T38" id="Seg_4322" n="e" s="T37">мне </ts>
               <ts e="T39" id="Seg_4324" n="e" s="T38">верующих </ts>
               <ts e="T40" id="Seg_4326" n="e" s="T39">людей". </ts>
               <ts e="T41" id="Seg_4328" n="e" s="T40">Ну, </ts>
               <ts e="T42" id="Seg_4330" n="e" s="T41">бумага </ts>
               <ts e="T43" id="Seg_4332" n="e" s="T42">пришла, </ts>
               <ts e="T44" id="Seg_4334" n="e" s="T43">даже </ts>
               <ts e="T45" id="Seg_4336" n="e" s="T44">у </ts>
               <ts e="T46" id="Seg_4338" n="e" s="T45">нас </ts>
               <ts e="T47" id="Seg_4340" n="e" s="T46">в </ts>
               <ts e="T48" id="Seg_4342" n="e" s="T47">Абалакове </ts>
               <ts e="T49" id="Seg_4344" n="e" s="T48">была. </ts>
               <ts e="T64" id="Seg_4346" n="e" s="T49">Стали писать, и которые не веровали и ругали бога, и те записались, что они верующие. </ts>
               <ts e="T84" id="Seg_4348" n="e" s="T64">Тады наш наставник (погляд-) поглядел, что все люди стали верующие, и (н-) не отдал в Америку людей своих. </ts>
               <ts e="T85" id="Seg_4350" n="e" s="T84">A </ts>
               <ts e="T86" id="Seg_4352" n="e" s="T85">(miʔnʼibeʔ) </ts>
               <ts e="T87" id="Seg_4354" n="e" s="T86">ej </ts>
               <ts e="T88" id="Seg_4356" n="e" s="T87">mĭbiʔi </ts>
               <ts e="T89" id="Seg_4358" n="e" s="T88">kudajdə </ts>
               <ts e="T90" id="Seg_4360" n="e" s="T89">numan üzəsʼtə. </ts>
               <ts e="T91" id="Seg_4362" n="e" s="T90">Dĭgəttə </ts>
               <ts e="T92" id="Seg_4364" n="e" s="T91">pʼaŋdəbi </ts>
               <ts e="T93" id="Seg_4366" n="e" s="T92">(s-) </ts>
               <ts e="T94" id="Seg_4368" n="e" s="T93">sazən </ts>
               <ts e="T95" id="Seg_4370" n="e" s="T94">Америка. </ts>
               <ts e="T96" id="Seg_4372" n="e" s="T95">"(Mĭndəd- </ts>
               <ts e="T97" id="Seg_4374" n="e" s="T96">mĭ- </ts>
               <ts e="T98" id="Seg_4376" n="e" s="T97">mĭ-) </ts>
               <ts e="T99" id="Seg_4378" n="e" s="T98">(Mĭbəleʔ) </ts>
               <ts e="T100" id="Seg_4380" n="e" s="T99">măna </ts>
               <ts e="T101" id="Seg_4382" n="e" s="T100">dĭ </ts>
               <ts e="T102" id="Seg_4384" n="e" s="T101">il, </ts>
               <ts e="T103" id="Seg_4386" n="e" s="T102">girgit </ts>
               <ts e="T104" id="Seg_4388" n="e" s="T103">il </ts>
               <ts e="T105" id="Seg_4390" n="e" s="T104">kudajdə </ts>
               <ts e="T106" id="Seg_4392" n="e" s="T105">(uman-) </ts>
               <ts e="T107" id="Seg_4394" n="e" s="T106">numan üzəlieʔi". </ts>
               <ts e="T108" id="Seg_4396" n="e" s="T107">Dĭgəttə </ts>
               <ts e="T109" id="Seg_4398" n="e" s="T108">sazən </ts>
               <ts e="T110" id="Seg_4400" n="e" s="T109">šobi. </ts>
               <ts e="T111" id="Seg_4402" n="e" s="T110">Pʼaŋdəbiʔi </ts>
               <ts e="T112" id="Seg_4404" n="e" s="T111">šindi </ts>
               <ts e="T113" id="Seg_4406" n="e" s="T112">(i-) </ts>
               <ts e="T114" id="Seg_4408" n="e" s="T113">kudajdə </ts>
               <ts e="T115" id="Seg_4410" n="e" s="T114">(kudonz-) </ts>
               <ts e="T116" id="Seg_4412" n="e" s="T115">kudolbi. </ts>
               <ts e="T117" id="Seg_4414" n="e" s="T116">I </ts>
               <ts e="T118" id="Seg_4416" n="e" s="T117">dĭbər </ts>
               <ts e="T119" id="Seg_4418" n="e" s="T118">sazəndə </ts>
               <ts e="T120" id="Seg_4420" n="e" s="T119">pʼaŋdəbi, </ts>
               <ts e="T121" id="Seg_4422" n="e" s="T120">što </ts>
               <ts e="T122" id="Seg_4424" n="e" s="T121">kudajdə </ts>
               <ts e="T123" id="Seg_4426" n="e" s="T122">numan üzleʔbəm. </ts>
               <ts e="T124" id="Seg_4428" n="e" s="T123">Kabarləj. </ts>
            </ts>
            <ts e="T149" id="Seg_4429" n="sc" s="T147">
               <ts e="T149" id="Seg_4431" n="e" s="T147">Можно рассказать… </ts>
            </ts>
            <ts e="T156" id="Seg_4432" n="sc" s="T153">
               <ts e="T156" id="Seg_4434" n="e" s="T153">Можно и это. </ts>
            </ts>
            <ts e="T161" id="Seg_4435" n="sc" s="T160">
               <ts e="T161" id="Seg_4437" n="e" s="T160">А? </ts>
            </ts>
            <ts e="T168" id="Seg_4438" n="sc" s="T164">
               <ts e="T167" id="Seg_4440" n="e" s="T164">Ну, </ts>
               <ts e="T168" id="Seg_4442" n="e" s="T167">ничего. </ts>
            </ts>
            <ts e="T177" id="Seg_4443" n="sc" s="T172">
               <ts e="T177" id="Seg_4445" n="e" s="T172">Все хорошие, приветствуют хорошо. </ts>
            </ts>
            <ts e="T196" id="Seg_4446" n="sc" s="T189">
               <ts e="T192" id="Seg_4448" n="e" s="T189">(Все=) </ts>
               <ts e="T193" id="Seg_4450" n="e" s="T192">Все </ts>
               <ts e="T194" id="Seg_4452" n="e" s="T193">обнимали, </ts>
               <ts e="T195" id="Seg_4454" n="e" s="T194">целовали </ts>
               <ts e="T196" id="Seg_4456" n="e" s="T195">даже. </ts>
            </ts>
            <ts e="T220" id="Seg_4457" n="sc" s="T214">
               <ts e="T220" id="Seg_4459" n="e" s="T214">Ну как, по-русски рассказывать? </ts>
            </ts>
            <ts e="T236" id="Seg_4460" n="sc" s="T232">
               <ts e="T236" id="Seg_4462" n="e" s="T232">Там он говорит уже? </ts>
            </ts>
            <ts e="T367" id="Seg_4463" n="sc" s="T248">
               <ts e="T262" id="Seg_4465" n="e" s="T248">Када я на (конгре-) на конгрет пришла, все такие радые. </ts>
               <ts e="T268" id="Seg_4467" n="e" s="T262">Все люди хороши, приняли меня. </ts>
               <ts e="T272" id="Seg_4469" n="e" s="T268">Все ученые, умные люди. </ts>
               <ts e="T273" id="Seg_4471" n="e" s="T272">Интересные, </ts>
               <ts e="T274" id="Seg_4473" n="e" s="T273">красивые. </ts>
               <ts e="T275" id="Seg_4475" n="e" s="T274">Ну, </ts>
               <ts e="T276" id="Seg_4477" n="e" s="T275">красивше </ts>
               <ts e="T277" id="Seg_4479" n="e" s="T276">всех </ts>
               <ts e="T278" id="Seg_4481" n="e" s="T277">мой </ts>
               <ts e="T279" id="Seg_4483" n="e" s="T278">Агафон </ts>
               <ts e="T280" id="Seg_4485" n="e" s="T279">Иванович, </ts>
               <ts e="T281" id="Seg_4487" n="e" s="T280">я </ts>
               <ts e="T282" id="Seg_4489" n="e" s="T281">его </ts>
               <ts e="T283" id="Seg_4491" n="e" s="T282">люблю </ts>
               <ts e="T284" id="Seg_4493" n="e" s="T283">шибко. </ts>
               <ts e="T296" id="Seg_4495" n="e" s="T284">Он мой ученик, я его учила (на с-) на свое наречие. </ts>
               <ts e="T297" id="Seg_4497" n="e" s="T296">Ну, </ts>
               <ts e="T298" id="Seg_4499" n="e" s="T297">и </ts>
               <ts e="T299" id="Seg_4501" n="e" s="T298">поглянулись </ts>
               <ts e="T300" id="Seg_4503" n="e" s="T299">и </ts>
               <ts e="T301" id="Seg_4505" n="e" s="T300">все. </ts>
               <ts e="T306" id="Seg_4507" n="e" s="T301">А (теперь) поехала на кладбище. </ts>
               <ts e="T310" id="Seg_4509" n="e" s="T306">Там тоже хорошо было. </ts>
               <ts e="T317" id="Seg_4511" n="e" s="T310">И там я на свое наречие пела. </ts>
               <ts e="T327" id="Seg_4513" n="e" s="T317">Божественные молитвы, все тоже такие рады были, все меня приветствовали. </ts>
               <ts e="T328" id="Seg_4515" n="e" s="T327">Приглашали. </ts>
               <ts e="T346" id="Seg_4517" n="e" s="T328">Там я видала много белок на кладбище бегают, (а у= а у нас=) а у нас там нету. </ts>
               <ts e="T350" id="Seg_4519" n="e" s="T346">Ну наши охотники убивают. </ts>
               <ts e="T355" id="Seg_4521" n="e" s="T350">А тут не убивают белок. </ts>
               <ts e="T360" id="Seg_4523" n="e" s="T355">Я (имя) давала кушать там. </ts>
               <ts e="T361" id="Seg_4525" n="e" s="T360">Ну, </ts>
               <ts e="T362" id="Seg_4527" n="e" s="T361">теперь </ts>
               <ts e="T363" id="Seg_4529" n="e" s="T362">надо </ts>
               <ts e="T364" id="Seg_4531" n="e" s="T363">по </ts>
               <ts e="T365" id="Seg_4533" n="e" s="T364">- </ts>
               <ts e="T366" id="Seg_4535" n="e" s="T365">своему </ts>
               <ts e="T367" id="Seg_4537" n="e" s="T366">сказать. </ts>
            </ts>
            <ts e="T500" id="Seg_4538" n="sc" s="T368">
               <ts e="T369" id="Seg_4540" n="e" s="T368">Măn </ts>
               <ts e="T370" id="Seg_4542" n="e" s="T369">ertən </ts>
               <ts e="T371" id="Seg_4544" n="e" s="T370">uʔbdəbiam, </ts>
               <ts e="T372" id="Seg_4546" n="e" s="T371">kudajdə </ts>
               <ts e="T373" id="Seg_4548" n="e" s="T372">numan üzəbiem, </ts>
               <ts e="T374" id="Seg_4550" n="e" s="T373">edəʔleʔbəm </ts>
               <ts e="T375" id="Seg_4552" n="e" s="T374">(Aga-) </ts>
               <ts e="T376" id="Seg_4554" n="e" s="T375">Agafon </ts>
               <ts e="T377" id="Seg_4556" n="e" s="T376">Ivanovičəm. </ts>
               <ts e="T378" id="Seg_4558" n="e" s="T377">Dĭ </ts>
               <ts e="T379" id="Seg_4560" n="e" s="T378">šobi </ts>
               <ts e="T380" id="Seg_4562" n="e" s="T379">măna. </ts>
               <ts e="T381" id="Seg_4564" n="e" s="T380">Dĭgəttə </ts>
               <ts e="T382" id="Seg_4566" n="e" s="T381">măn </ts>
               <ts e="T383" id="Seg_4568" n="e" s="T382">dĭzi </ts>
               <ts e="T384" id="Seg_4570" n="e" s="T383">kambiam. </ts>
               <ts e="T385" id="Seg_4572" n="e" s="T384">Šobiam </ts>
               <ts e="T386" id="Seg_4574" n="e" s="T385">dĭbər, </ts>
               <ts e="T387" id="Seg_4576" n="e" s="T386">(dĭ=) </ts>
               <ts e="T388" id="Seg_4578" n="e" s="T387">dĭ </ts>
               <ts e="T389" id="Seg_4580" n="e" s="T388">kănferentsianə. </ts>
               <ts e="T390" id="Seg_4582" n="e" s="T389">Iʔgö </ts>
               <ts e="T391" id="Seg_4584" n="e" s="T390">(i-) </ts>
               <ts e="T392" id="Seg_4586" n="e" s="T391">il </ts>
               <ts e="T393" id="Seg_4588" n="e" s="T392">(dĭn), </ts>
               <ts e="T394" id="Seg_4590" n="e" s="T393">bar </ts>
               <ts e="T395" id="Seg_4592" n="e" s="T394">(uʔ-) </ts>
               <ts e="T396" id="Seg_4594" n="e" s="T395">kuvas </ts>
               <ts e="T397" id="Seg_4596" n="e" s="T396">ugandə. </ts>
               <ts e="T398" id="Seg_4598" n="e" s="T397">Jakšeʔi. </ts>
               <ts e="T399" id="Seg_4600" n="e" s="T398">Pʼaŋzittə </ts>
               <ts e="T400" id="Seg_4602" n="e" s="T399">tăŋ </ts>
               <ts e="T401" id="Seg_4604" n="e" s="T400">tĭmneʔi, </ts>
               <ts e="T402" id="Seg_4606" n="e" s="T401">šagəstə </ts>
               <ts e="T403" id="Seg_4608" n="e" s="T402">iʔgö. </ts>
               <ts e="T404" id="Seg_4610" n="e" s="T403">Dĭgəttə </ts>
               <ts e="T405" id="Seg_4612" n="e" s="T404">bar </ts>
               <ts e="T406" id="Seg_4614" n="e" s="T405">măna </ts>
               <ts e="T407" id="Seg_4616" n="e" s="T406">pănarlaʔbəʔjə. </ts>
               <ts e="T408" id="Seg_4618" n="e" s="T407">Nezeŋ. </ts>
               <ts e="T409" id="Seg_4620" n="e" s="T408">A </ts>
               <ts e="T410" id="Seg_4622" n="e" s="T409">tibizeŋ </ts>
               <ts e="T411" id="Seg_4624" n="e" s="T410">(uda-) </ts>
               <ts e="T412" id="Seg_4626" n="e" s="T411">udazaŋdə </ts>
               <ts e="T413" id="Seg_4628" n="e" s="T412">nuleʔbəʔjə. </ts>
               <ts e="T414" id="Seg_4630" n="e" s="T413">Dĭgəttə </ts>
               <ts e="T415" id="Seg_4632" n="e" s="T414">dĭgəʔ </ts>
               <ts e="T416" id="Seg_4634" n="e" s="T415">(š-) </ts>
               <ts e="T417" id="Seg_4636" n="e" s="T416">kambiam, </ts>
               <ts e="T418" id="Seg_4638" n="e" s="T417">mašinanə </ts>
               <ts e="T419" id="Seg_4640" n="e" s="T418">(amnubi=) </ts>
               <ts e="T420" id="Seg_4642" n="e" s="T419">amnobiam, </ts>
               <ts e="T421" id="Seg_4644" n="e" s="T420">(ibiem) </ts>
               <ts e="T422" id="Seg_4646" n="e" s="T421">dĭn </ts>
               <ts e="T423" id="Seg_4648" n="e" s="T422">(gi-) </ts>
               <ts e="T424" id="Seg_4650" n="e" s="T423">gijən </ts>
               <ts e="T425" id="Seg_4652" n="e" s="T424">sazən </ts>
               <ts e="T426" id="Seg_4654" n="e" s="T425">(aliaʔi) </ts>
               <ts e="T427" id="Seg_4656" n="e" s="T426">i </ts>
               <ts e="T428" id="Seg_4658" n="e" s="T427">knižkaʔi </ts>
               <ts e="T429" id="Seg_4660" n="e" s="T428">gijən </ts>
               <ts e="T430" id="Seg_4662" n="e" s="T429">(enləʔbəʔjə). </ts>
               <ts e="T431" id="Seg_4664" n="e" s="T430">Dĭn </ts>
               <ts e="T432" id="Seg_4666" n="e" s="T431">măna </ts>
               <ts e="T433" id="Seg_4668" n="e" s="T432">tože </ts>
               <ts e="T434" id="Seg_4670" n="e" s="T433">(kü-) </ts>
               <ts e="T435" id="Seg_4672" n="e" s="T434">kürbiʔi </ts>
               <ts e="T436" id="Seg_4674" n="e" s="T435">kartačkanə. </ts>
               <ts e="T437" id="Seg_4676" n="e" s="T436">Dĭn </ts>
               <ts e="T438" id="Seg_4678" n="e" s="T437">amnobiam, </ts>
               <ts e="T439" id="Seg_4680" n="e" s="T438">dʼăbaktərbiam, </ts>
               <ts e="T440" id="Seg_4682" n="e" s="T439">dĭn </ts>
               <ts e="T441" id="Seg_4684" n="e" s="T440">tože </ts>
               <ts e="T442" id="Seg_4686" n="e" s="T441">ugandə </ts>
               <ts e="T443" id="Seg_4688" n="e" s="T442">jakše </ts>
               <ts e="T444" id="Seg_4690" n="e" s="T443">il. </ts>
               <ts e="T445" id="Seg_4692" n="e" s="T444">Dĭgəttə </ts>
               <ts e="T446" id="Seg_4694" n="e" s="T445">(dĭzeŋ=) </ts>
               <ts e="T447" id="Seg_4696" n="e" s="T446">dĭzeŋdə </ts>
               <ts e="T448" id="Seg_4698" n="e" s="T447">mămbiam: </ts>
               <ts e="T449" id="Seg_4700" n="e" s="T448">"(kudaj=) </ts>
               <ts e="T450" id="Seg_4702" n="e" s="T449">kudaj </ts>
               <ts e="T451" id="Seg_4704" n="e" s="T450">šiʔsi", </ts>
               <ts e="T452" id="Seg_4706" n="e" s="T451">i </ts>
               <ts e="T453" id="Seg_4708" n="e" s="T452">(bosəltə) </ts>
               <ts e="T454" id="Seg_4710" n="e" s="T453">kambiam. </ts>
               <ts e="T455" id="Seg_4712" n="e" s="T454">Dĭgəttə </ts>
               <ts e="T456" id="Seg_4714" n="e" s="T455">dĭn </ts>
               <ts e="T457" id="Seg_4716" n="e" s="T456">nʼüʔnən </ts>
               <ts e="T458" id="Seg_4718" n="e" s="T457">(ibie-) </ts>
               <ts e="T459" id="Seg_4720" n="e" s="T458">ibiem, </ts>
               <ts e="T460" id="Seg_4722" n="e" s="T459">dĭgəttə </ts>
               <ts e="T461" id="Seg_4724" n="e" s="T460">nubibaʔ </ts>
               <ts e="T462" id="Seg_4726" n="e" s="T461">dʼünə </ts>
               <ts e="T463" id="Seg_4728" n="e" s="T462">(öʔlüʔpiʔi). </ts>
               <ts e="T464" id="Seg_4730" n="e" s="T463">Gəttə </ts>
               <ts e="T465" id="Seg_4732" n="e" s="T464">kambibaʔ, </ts>
               <ts e="T466" id="Seg_4734" n="e" s="T465">gijen </ts>
               <ts e="T467" id="Seg_4736" n="e" s="T466">(k-) </ts>
               <ts e="T468" id="Seg_4738" n="e" s="T467">krospaʔi. </ts>
               <ts e="T469" id="Seg_4740" n="e" s="T468">Dĭn </ts>
               <ts e="T470" id="Seg_4742" n="e" s="T469">tože </ts>
               <ts e="T471" id="Seg_4744" n="e" s="T470">mĭmbibeʔ. </ts>
               <ts e="T472" id="Seg_4746" n="e" s="T471">Iʔgö </ts>
               <ts e="T473" id="Seg_4748" n="e" s="T472">nezeŋ </ts>
               <ts e="T474" id="Seg_4750" n="e" s="T473">ibiʔi. </ts>
               <ts e="T475" id="Seg_4752" n="e" s="T474">I </ts>
               <ts e="T476" id="Seg_4754" n="e" s="T475">(onʼiʔ=) </ts>
               <ts e="T477" id="Seg_4756" n="e" s="T476">šide </ts>
               <ts e="T478" id="Seg_4758" n="e" s="T477">tibi </ts>
               <ts e="T479" id="Seg_4760" n="e" s="T478">ibiʔi. </ts>
               <ts e="T480" id="Seg_4762" n="e" s="T479">Tože </ts>
               <ts e="T481" id="Seg_4764" n="e" s="T480">bar </ts>
               <ts e="T482" id="Seg_4766" n="e" s="T481">măna </ts>
               <ts e="T483" id="Seg_4768" n="e" s="T482">pănarbiʔi, </ts>
               <ts e="T484" id="Seg_4770" n="e" s="T483">udaʔi </ts>
               <ts e="T485" id="Seg_4772" n="e" s="T484">(mĭmbiʔi=) </ts>
               <ts e="T486" id="Seg_4774" n="e" s="T485">mĭbiʔi. </ts>
               <ts e="T487" id="Seg_4776" n="e" s="T486">Măndəʔi:" </ts>
               <ts e="T488" id="Seg_4778" n="e" s="T487">Jakše </ts>
               <ts e="T489" id="Seg_4780" n="e" s="T488">igel </ts>
               <ts e="T490" id="Seg_4782" n="e" s="T489">ugandə. </ts>
               <ts e="T491" id="Seg_4784" n="e" s="T490">Kuŋgə </ts>
               <ts e="T492" id="Seg_4786" n="e" s="T491">šobial?" </ts>
               <ts e="T493" id="Seg_4788" n="e" s="T492">Măn </ts>
               <ts e="T494" id="Seg_4790" n="e" s="T493">mămbiam:" </ts>
               <ts e="T495" id="Seg_4792" n="e" s="T494">Kuŋgə </ts>
               <ts e="T496" id="Seg_4794" n="e" s="T495">šobiam". </ts>
               <ts e="T497" id="Seg_4796" n="e" s="T496">(Măngoliaʔi) </ts>
               <ts e="T498" id="Seg_4798" n="e" s="T497">tondə </ts>
               <ts e="T499" id="Seg_4800" n="e" s="T498">amnobiam. </ts>
               <ts e="T500" id="Seg_4802" n="e" s="T499">Kabarləj. </ts>
            </ts>
            <ts e="T861" id="Seg_4803" n="sc" s="T565">
               <ts e="T574" id="Seg_4805" n="e" s="T565">Мы с этой, с Ридой ждали-ждали Арпита. </ts>
               <ts e="T575" id="Seg_4807" n="e" s="T574">Нету, </ts>
               <ts e="T576" id="Seg_4809" n="e" s="T575">нету. </ts>
               <ts e="T583" id="Seg_4811" n="e" s="T576">Потом она сходила туды, к евонной теще. </ts>
               <ts e="T584" id="Seg_4813" n="e" s="T583">"Пойду </ts>
               <ts e="T585" id="Seg_4815" n="e" s="T584">узнаю". </ts>
               <ts e="T597" id="Seg_4817" n="e" s="T585">А я ее не пускаю:" Не ходи, говорю, все равно он придет. </ts>
               <ts e="T603" id="Seg_4819" n="e" s="T597">Разве только заболел дак не придет. </ts>
               <ts e="T609" id="Seg_4821" n="e" s="T603">А то придет, обманывать не будет. </ts>
               <ts e="T616" id="Seg_4823" n="e" s="T609">Он же верующий человек, ему нельзя обмануть. </ts>
               <ts e="T624" id="Seg_4825" n="e" s="T616">Как он ко мне приехал, я тоже писала: </ts>
               <ts e="T629" id="Seg_4827" n="e" s="T624">Когда приедет Агафон Иванович, приеду. </ts>
               <ts e="T633" id="Seg_4829" n="e" s="T629">Он приехал ко мне. </ts>
               <ts e="T634" id="Seg_4831" n="e" s="T633">Ну, </ts>
               <ts e="T635" id="Seg_4833" n="e" s="T634">я </ts>
               <ts e="T636" id="Seg_4835" n="e" s="T635">раз </ts>
               <ts e="T637" id="Seg_4837" n="e" s="T636">обещалась, </ts>
               <ts e="T638" id="Seg_4839" n="e" s="T637">ни </ts>
               <ts e="T639" id="Seg_4841" n="e" s="T638">на </ts>
               <ts e="T640" id="Seg_4843" n="e" s="T639">что </ts>
               <ts e="T641" id="Seg_4845" n="e" s="T640">не </ts>
               <ts e="T642" id="Seg_4847" n="e" s="T641">погляделась, </ts>
               <ts e="T643" id="Seg_4849" n="e" s="T642">собралась </ts>
               <ts e="T644" id="Seg_4851" n="e" s="T643">и </ts>
               <ts e="T645" id="Seg_4853" n="e" s="T644">с </ts>
               <ts e="T646" id="Seg_4855" n="e" s="T645">им </ts>
               <ts e="T647" id="Seg_4857" n="e" s="T646">поехала. </ts>
               <ts e="T654" id="Seg_4859" n="e" s="T647">Приехали в Агинска, там тоже наши верующие. </ts>
               <ts e="T667" id="Seg_4861" n="e" s="T654">Там я ему предлагала, говорю:" Приезжай к нам, мы тебе тут дом купим". </ts>
               <ts e="T668" id="Seg_4863" n="e" s="T667">Ну, </ts>
               <ts e="T669" id="Seg_4865" n="e" s="T668">оттель </ts>
               <ts e="T670" id="Seg_4867" n="e" s="T669">мы </ts>
               <ts e="T671" id="Seg_4869" n="e" s="T670">пять </ts>
               <ts e="T673" id="Seg_4871" n="e" s="T671">часов… </ts>
               <ts e="T682" id="Seg_4873" n="e" s="T673">Нас проводили тоже сестры верующие, мы приехали в Уяр. </ts>
               <ts e="T683" id="Seg_4875" n="e" s="T682">Ну, </ts>
               <ts e="T684" id="Seg_4877" n="e" s="T683">он </ts>
               <ts e="T685" id="Seg_4879" n="e" s="T684">- </ts>
               <ts e="T686" id="Seg_4881" n="e" s="T685">то, </ts>
               <ts e="T687" id="Seg_4883" n="e" s="T686">конечно, </ts>
               <ts e="T688" id="Seg_4885" n="e" s="T687">не </ts>
               <ts e="T689" id="Seg_4887" n="e" s="T688">знал, </ts>
               <ts e="T690" id="Seg_4889" n="e" s="T689">где, </ts>
               <ts e="T691" id="Seg_4891" n="e" s="T690">а </ts>
               <ts e="T692" id="Seg_4893" n="e" s="T691">я </ts>
               <ts e="T693" id="Seg_4895" n="e" s="T692">там </ts>
               <ts e="T694" id="Seg_4897" n="e" s="T693">знаю </ts>
               <ts e="T695" id="Seg_4899" n="e" s="T694">своих </ts>
               <ts e="T696" id="Seg_4901" n="e" s="T695">верующих. </ts>
               <ts e="T697" id="Seg_4903" n="e" s="T696">Ну, </ts>
               <ts e="T698" id="Seg_4905" n="e" s="T697">пошли </ts>
               <ts e="T699" id="Seg_4907" n="e" s="T698">с </ts>
               <ts e="T700" id="Seg_4909" n="e" s="T699">им. </ts>
               <ts e="T714" id="Seg_4911" n="e" s="T700">У людей спрашиваю, а фамилие-то я забыла, только знаю: Илюша и Маруся. </ts>
               <ts e="T715" id="Seg_4913" n="e" s="T714">Но, </ts>
               <ts e="T716" id="Seg_4915" n="e" s="T715">(зн </ts>
               <ts e="T717" id="Seg_4917" n="e" s="T716">-) </ts>
               <ts e="T718" id="Seg_4919" n="e" s="T717">место </ts>
               <ts e="T719" id="Seg_4921" n="e" s="T718">- </ts>
               <ts e="T720" id="Seg_4923" n="e" s="T719">то </ts>
               <ts e="T721" id="Seg_4925" n="e" s="T720">знаю, </ts>
               <ts e="T722" id="Seg_4927" n="e" s="T721">заметно, </ts>
               <ts e="T723" id="Seg_4929" n="e" s="T722">была </ts>
               <ts e="T724" id="Seg_4931" n="e" s="T723">я </ts>
               <ts e="T725" id="Seg_4933" n="e" s="T724">там. </ts>
               <ts e="T733" id="Seg_4935" n="e" s="T725">Арпиту говорю:" Вы постойте тут, а я зайду". </ts>
               <ts e="T734" id="Seg_4937" n="e" s="T733">(За </ts>
               <ts e="T735" id="Seg_4939" n="e" s="T734">-) </ts>
               <ts e="T736" id="Seg_4941" n="e" s="T735">Захожу </ts>
               <ts e="T737" id="Seg_4943" n="e" s="T736">в </ts>
               <ts e="T738" id="Seg_4945" n="e" s="T737">первую </ts>
               <ts e="T739" id="Seg_4947" n="e" s="T738">(и </ts>
               <ts e="T740" id="Seg_4949" n="e" s="T739">-) </ts>
               <ts e="T741" id="Seg_4951" n="e" s="T740">в </ts>
               <ts e="T742" id="Seg_4953" n="e" s="T741">перво </ts>
               <ts e="T743" id="Seg_4955" n="e" s="T742">помещение. </ts>
               <ts e="T744" id="Seg_4957" n="e" s="T743">"(Не </ts>
               <ts e="T745" id="Seg_4959" n="e" s="T744">з </ts>
               <ts e="T746" id="Seg_4961" n="e" s="T745">-) </ts>
               <ts e="T747" id="Seg_4963" n="e" s="T746">Не </ts>
               <ts e="T748" id="Seg_4965" n="e" s="T747">заходи, </ts>
               <ts e="T749" id="Seg_4967" n="e" s="T748">тут, </ts>
               <ts e="T750" id="Seg_4969" n="e" s="T749">- </ts>
               <ts e="T751" id="Seg_4971" n="e" s="T750">говорит, </ts>
               <ts e="T752" id="Seg_4973" n="e" s="T751">- </ts>
               <ts e="T753" id="Seg_4975" n="e" s="T752">никто </ts>
               <ts e="T754" id="Seg_4977" n="e" s="T753">не </ts>
               <ts e="T755" id="Seg_4979" n="e" s="T754">живет". </ts>
               <ts e="T763" id="Seg_4981" n="e" s="T755">Потом туды дальше пошла, оттель спросила у человека: </ts>
               <ts e="T772" id="Seg_4983" n="e" s="T763">Я говорю:" Знаете Илюшу (и М -) с Марусей?" </ts>
               <ts e="T781" id="Seg_4985" n="e" s="T772">Он говорит: "Оне со мной работают", и меня повел. </ts>
               <ts e="T800" id="Seg_4987" n="e" s="T781">А я брату сказала: "(И -) Иди сюда", и он пришел, и как раз она идет тут с водой. </ts>
               <ts e="T822" id="Seg_4989" n="e" s="T800">И мы тут тоже долго беседовали, там еще один наш брат приходил, такой же, как брат Арпит, (он=) я там часто бываю. </ts>
               <ts e="T832" id="Seg_4991" n="e" s="T822">Потому что у нас в деревне таких нету, как я. </ts>
               <ts e="T848" id="Seg_4993" n="e" s="T832">Ну я ли езжу в Красноярска, и там у нас еще есть разъезд, туда я езжу. </ts>
               <ts e="T850" id="Seg_4995" n="e" s="T848">Заозерка станция. </ts>
               <ts e="T851" id="Seg_4997" n="e" s="T850">Ты, </ts>
               <ts e="T852" id="Seg_4999" n="e" s="T851">наверное, </ts>
               <ts e="T853" id="Seg_5001" n="e" s="T852">может, </ts>
               <ts e="T854" id="Seg_5003" n="e" s="T853">был </ts>
               <ts e="T855" id="Seg_5005" n="e" s="T854">там </ts>
               <ts e="T856" id="Seg_5007" n="e" s="T855">(эка)? </ts>
               <ts e="T861" id="Seg_5009" n="e" s="T856">Не был на станции Заозерки? </ts>
            </ts>
            <ts e="T876" id="Seg_5010" n="sc" s="T864">
               <ts e="T876" id="Seg_5012" n="e" s="T864">Оттель тоже можно в Красноярска уехать, и в Канска, в город Канск. </ts>
            </ts>
            <ts e="T891" id="Seg_5013" n="sc" s="T878">
               <ts e="T879" id="Seg_5015" n="e" s="T878">Ну, </ts>
               <ts e="T880" id="Seg_5017" n="e" s="T879">я </ts>
               <ts e="T881" id="Seg_5019" n="e" s="T880">в </ts>
               <ts e="T882" id="Seg_5021" n="e" s="T881">Канске </ts>
               <ts e="T883" id="Seg_5023" n="e" s="T882">тоже </ts>
               <ts e="T884" id="Seg_5025" n="e" s="T883">была </ts>
               <ts e="T885" id="Seg_5027" n="e" s="T884">там. </ts>
               <ts e="T891" id="Seg_5029" n="e" s="T885">Тоже там есть верующие, там была. </ts>
            </ts>
            <ts e="T894" id="Seg_5030" n="sc" s="T892">
               <ts e="T894" id="Seg_5032" n="e" s="T892">Так… </ts>
            </ts>
            <ts e="T938" id="Seg_5033" n="sc" s="T928">
               <ts e="T938" id="Seg_5035" n="e" s="T928">Я уже много наговорила по-русски. </ts>
            </ts>
            <ts e="T1054" id="Seg_5036" n="sc" s="T943">
               <ts e="T944" id="Seg_5038" n="e" s="T943">Nu, </ts>
               <ts e="T945" id="Seg_5040" n="e" s="T944">miʔ </ts>
               <ts e="T946" id="Seg_5042" n="e" s="T945">taldʼen </ts>
               <ts e="T947" id="Seg_5044" n="e" s="T946">(Vidazi) </ts>
               <ts e="T948" id="Seg_5046" n="e" s="T947">edəʔleʔbəbeʔ </ts>
               <ts e="T949" id="Seg_5048" n="e" s="T948">üge, </ts>
               <ts e="T950" id="Seg_5050" n="e" s="T949">kagam </ts>
               <ts e="T951" id="Seg_5052" n="e" s="T950">edəʔleʔbə, </ts>
               <ts e="T952" id="Seg_5054" n="e" s="T951">(gə) </ts>
               <ts e="T953" id="Seg_5056" n="e" s="T952">naga, </ts>
               <ts e="T954" id="Seg_5058" n="e" s="T953">naga, </ts>
               <ts e="T956" id="Seg_5060" n="e" s="T954">dĭgəttə… </ts>
               <ts e="T957" id="Seg_5062" n="e" s="T956">Bazaj </ts>
               <ts e="T958" id="Seg_5064" n="e" s="T957">aʔtʼi </ts>
               <ts e="T959" id="Seg_5066" n="e" s="T958">(küzürleʔbə), </ts>
               <ts e="T960" id="Seg_5068" n="e" s="T959">măn </ts>
               <ts e="T961" id="Seg_5070" n="e" s="T960">măndəm:" </ts>
               <ts e="T962" id="Seg_5072" n="e" s="T961">Šonəga". </ts>
               <ts e="T963" id="Seg_5074" n="e" s="T962">Nu </ts>
               <ts e="T964" id="Seg_5076" n="e" s="T963">(n-), </ts>
               <ts e="T965" id="Seg_5078" n="e" s="T964">dĭ </ts>
               <ts e="T966" id="Seg_5080" n="e" s="T965">šobi, </ts>
               <ts e="T967" id="Seg_5082" n="e" s="T966">možet </ts>
               <ts e="T968" id="Seg_5084" n="e" s="T967">iandə </ts>
               <ts e="T969" id="Seg_5086" n="e" s="T968">kambi, </ts>
               <ts e="T970" id="Seg_5088" n="e" s="T969">dĭn </ts>
               <ts e="T971" id="Seg_5090" n="e" s="T970">ambi </ts>
               <ts e="T972" id="Seg_5092" n="e" s="T971">da </ts>
               <ts e="T973" id="Seg_5094" n="e" s="T972">(iʔbi-) </ts>
               <ts e="T974" id="Seg_5096" n="e" s="T973">iʔbəbi. </ts>
               <ts e="T975" id="Seg_5098" n="e" s="T974">(Šalaʔjə) </ts>
               <ts e="T976" id="Seg_5100" n="e" s="T975">kunolzittə. </ts>
               <ts e="T977" id="Seg_5102" n="e" s="T976">Dĭgəttə </ts>
               <ts e="T978" id="Seg_5104" n="e" s="T977">amnobibaʔ, </ts>
               <ts e="T979" id="Seg_5106" n="e" s="T978">i </ts>
               <ts e="T980" id="Seg_5108" n="e" s="T979">deʔpibeʔ </ts>
               <ts e="T981" id="Seg_5110" n="e" s="T980">(deʔpibeʔ </ts>
               <ts e="T982" id="Seg_5112" n="e" s="T981">ig- </ts>
               <ts e="T983" id="Seg_5114" n="e" s="T982">li-) </ts>
               <ts e="T984" id="Seg_5116" n="e" s="T983">iʔbəbeʔ </ts>
               <ts e="T985" id="Seg_5118" n="e" s="T984">kunolzittə. </ts>
               <ts e="T986" id="Seg_5120" n="e" s="T985">Tolʼko </ts>
               <ts e="T987" id="Seg_5122" n="e" s="T986">(iʔbəbeʔ=) </ts>
               <ts e="T988" id="Seg_5124" n="e" s="T987">iʔpibeʔ, </ts>
               <ts e="T989" id="Seg_5126" n="e" s="T988">măn </ts>
               <ts e="T990" id="Seg_5128" n="e" s="T989">aʔpi </ts>
               <ts e="T991" id="Seg_5130" n="e" s="T990">i </ts>
               <ts e="T992" id="Seg_5132" n="e" s="T991">kunolbiam. </ts>
               <ts e="T993" id="Seg_5134" n="e" s="T992">Nu, </ts>
               <ts e="T994" id="Seg_5136" n="e" s="T993">днем </ts>
               <ts e="T995" id="Seg_5138" n="e" s="T994">šonəga </ts>
               <ts e="T996" id="Seg_5140" n="e" s="T995">šindidə. </ts>
               <ts e="T997" id="Seg_5142" n="e" s="T996">Šolambi </ts>
               <ts e="T998" id="Seg_5144" n="e" s="T997">((…)), </ts>
               <ts e="T999" id="Seg_5146" n="e" s="T998">măn </ts>
               <ts e="T1000" id="Seg_5148" n="e" s="T999">suʔmiluʔpiam </ts>
               <ts e="T1001" id="Seg_5150" n="e" s="T1000">da </ts>
               <ts e="T1002" id="Seg_5152" n="e" s="T1001">dĭbər </ts>
               <ts e="T1003" id="Seg_5154" n="e" s="T1002">nuʔməluʔpiam. </ts>
               <ts e="T1004" id="Seg_5156" n="e" s="T1003">Dĭnə </ts>
               <ts e="T1005" id="Seg_5158" n="e" s="T1004">udam </ts>
               <ts e="T1006" id="Seg_5160" n="e" s="T1005">mĭbiem. </ts>
               <ts e="T1007" id="Seg_5162" n="e" s="T1006">(Dʼăbaktərbibeʔ), </ts>
               <ts e="T1008" id="Seg_5164" n="e" s="T1007">(dĭ </ts>
               <ts e="T1009" id="Seg_5166" n="e" s="T1008">măndə):" </ts>
               <ts e="T1010" id="Seg_5168" n="e" s="T1009">Măn </ts>
               <ts e="T1011" id="Seg_5170" n="e" s="T1010">băzəjdlam, </ts>
               <ts e="T1012" id="Seg_5172" n="e" s="T1011">vannan </ts>
               <ts e="T1013" id="Seg_5174" n="e" s="T1012">kalam". </ts>
               <ts e="T1014" id="Seg_5176" n="e" s="T1013">"No, </ts>
               <ts e="T1015" id="Seg_5178" n="e" s="T1014">kanaʔ". </ts>
               <ts e="T1016" id="Seg_5180" n="e" s="T1015">Dĭgəttə </ts>
               <ts e="T1017" id="Seg_5182" n="e" s="T1016">dĭ </ts>
               <ts e="T1018" id="Seg_5184" n="e" s="T1017">kambi. </ts>
               <ts e="T1019" id="Seg_5186" n="e" s="T1018">Măn </ts>
               <ts e="T1020" id="Seg_5188" n="e" s="T1019">mămbiam:" </ts>
               <ts e="T1021" id="Seg_5190" n="e" s="T1020">Amoraʔ, </ts>
               <ts e="T1022" id="Seg_5192" n="e" s="T1021">dĭgəttə </ts>
               <ts e="T1023" id="Seg_5194" n="e" s="T1022">kalal". </ts>
               <ts e="T1024" id="Seg_5196" n="e" s="T1023">Dĭgəttə </ts>
               <ts e="T1025" id="Seg_5198" n="e" s="T1024">dĭ </ts>
               <ts e="T1026" id="Seg_5200" n="e" s="T1025">(ejümləbi) </ts>
               <ts e="T1027" id="Seg_5202" n="e" s="T1026">mĭj, </ts>
               <ts e="T1028" id="Seg_5204" n="e" s="T1027">segi </ts>
               <ts e="T1029" id="Seg_5206" n="e" s="T1028">bü </ts>
               <ts e="T1030" id="Seg_5208" n="e" s="T1029">((…)) </ts>
               <ts e="T1031" id="Seg_5210" n="e" s="T1030">(dĭ </ts>
               <ts e="T1032" id="Seg_5212" n="e" s="T1031">ambi), </ts>
               <ts e="T1033" id="Seg_5214" n="e" s="T1032">dĭgəttə </ts>
               <ts e="T1034" id="Seg_5216" n="e" s="T1033">(ka-) </ts>
               <ts e="T1035" id="Seg_5218" n="e" s="T1034">kambi </ts>
               <ts e="T1036" id="Seg_5220" n="e" s="T1035">băzəjdəsʼtə </ts>
               <ts e="T1037" id="Seg_5222" n="e" s="T1036">vannan. </ts>
               <ts e="T1038" id="Seg_5224" n="e" s="T1037">Dĭgəttə </ts>
               <ts e="T1039" id="Seg_5226" n="e" s="T1038">dĭ </ts>
               <ts e="T1040" id="Seg_5228" n="e" s="T1039">dĭʔnə </ts>
               <ts e="T1041" id="Seg_5230" n="e" s="T1040">iʔbəsʼtə </ts>
               <ts e="T1042" id="Seg_5232" n="e" s="T1041">dʼajirbi. </ts>
               <ts e="T1043" id="Seg_5234" n="e" s="T1042">Dön </ts>
               <ts e="T1044" id="Seg_5236" n="e" s="T1043">nuldəbi </ts>
               <ts e="T1045" id="Seg_5238" n="e" s="T1044">(кровать, </ts>
               <ts e="T1046" id="Seg_5240" n="e" s="T1045">dĭ) </ts>
               <ts e="T1047" id="Seg_5242" n="e" s="T1046">iʔbolaʔpi, </ts>
               <ts e="T1048" id="Seg_5244" n="e" s="T1047">i </ts>
               <ts e="T1049" id="Seg_5246" n="e" s="T1048">kunollubibaʔ. </ts>
               <ts e="T1050" id="Seg_5248" n="e" s="T1049">Ertən </ts>
               <ts e="T1051" id="Seg_5250" n="e" s="T1050">bazo </ts>
               <ts e="T1052" id="Seg_5252" n="e" s="T1051">(uʔbdə-) </ts>
               <ts e="T1054" id="Seg_5254" n="e" s="T1052">uʔbdəbibaʔ… </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PKZ">
            <ta e="T8" id="Seg_5255" s="T3">PKZ_19700821_09339-2a.PKZ.001 (004)</ta>
            <ta e="T20" id="Seg_5256" s="T8">PKZ_19700821_09339-2a.PKZ.002 (005)</ta>
            <ta e="T40" id="Seg_5257" s="T20">PKZ_19700821_09339-2a.PKZ.003 (006)</ta>
            <ta e="T49" id="Seg_5258" s="T40">PKZ_19700821_09339-2a.PKZ.004 (007)</ta>
            <ta e="T64" id="Seg_5259" s="T49">PKZ_19700821_09339-2a.PKZ.005 (008)</ta>
            <ta e="T84" id="Seg_5260" s="T64">PKZ_19700821_09339-2a.PKZ.006 (009)</ta>
            <ta e="T90" id="Seg_5261" s="T84">PKZ_19700821_09339-2a.PKZ.007 (010)</ta>
            <ta e="T95" id="Seg_5262" s="T90">PKZ_19700821_09339-2a.PKZ.008 (011)</ta>
            <ta e="T107" id="Seg_5263" s="T95">PKZ_19700821_09339-2a.PKZ.009 (012)</ta>
            <ta e="T110" id="Seg_5264" s="T107">PKZ_19700821_09339-2a.PKZ.010 (013)</ta>
            <ta e="T116" id="Seg_5265" s="T110">PKZ_19700821_09339-2a.PKZ.011 (014)</ta>
            <ta e="T123" id="Seg_5266" s="T116">PKZ_19700821_09339-2a.PKZ.012 (015)</ta>
            <ta e="T124" id="Seg_5267" s="T123">PKZ_19700821_09339-2a.PKZ.013 (016)</ta>
            <ta e="T149" id="Seg_5268" s="T147">PKZ_19700821_09339-2a.PKZ.014 (022)</ta>
            <ta e="T156" id="Seg_5269" s="T153">PKZ_19700821_09339-2a.PKZ.015 (023)</ta>
            <ta e="T161" id="Seg_5270" s="T160">PKZ_19700821_09339-2a.PKZ.016 (025)</ta>
            <ta e="T168" id="Seg_5271" s="T164">PKZ_19700821_09339-2a.PKZ.017 (027)</ta>
            <ta e="T177" id="Seg_5272" s="T172">PKZ_19700821_09339-2a.PKZ.018 (030)</ta>
            <ta e="T196" id="Seg_5273" s="T189">PKZ_19700821_09339-2a.PKZ.019 (033)</ta>
            <ta e="T220" id="Seg_5274" s="T214">PKZ_19700821_09339-2a.PKZ.020 (037)</ta>
            <ta e="T236" id="Seg_5275" s="T232">PKZ_19700821_09339-2a.PKZ.021 (039)</ta>
            <ta e="T262" id="Seg_5276" s="T248">PKZ_19700821_09339-2a.PKZ.022 (043)</ta>
            <ta e="T268" id="Seg_5277" s="T262">PKZ_19700821_09339-2a.PKZ.023 (044)</ta>
            <ta e="T272" id="Seg_5278" s="T268">PKZ_19700821_09339-2a.PKZ.024 (045)</ta>
            <ta e="T274" id="Seg_5279" s="T272">PKZ_19700821_09339-2a.PKZ.025 (046)</ta>
            <ta e="T284" id="Seg_5280" s="T274">PKZ_19700821_09339-2a.PKZ.026 (047)</ta>
            <ta e="T296" id="Seg_5281" s="T284">PKZ_19700821_09339-2a.PKZ.027 (048)</ta>
            <ta e="T301" id="Seg_5282" s="T296">PKZ_19700821_09339-2a.PKZ.028 (049)</ta>
            <ta e="T306" id="Seg_5283" s="T301">PKZ_19700821_09339-2a.PKZ.029 (050)</ta>
            <ta e="T310" id="Seg_5284" s="T306">PKZ_19700821_09339-2a.PKZ.030 (051)</ta>
            <ta e="T317" id="Seg_5285" s="T310">PKZ_19700821_09339-2a.PKZ.031 (052)</ta>
            <ta e="T327" id="Seg_5286" s="T317">PKZ_19700821_09339-2a.PKZ.032 (053)</ta>
            <ta e="T328" id="Seg_5287" s="T327">PKZ_19700821_09339-2a.PKZ.033 (054)</ta>
            <ta e="T346" id="Seg_5288" s="T328">PKZ_19700821_09339-2a.PKZ.034 (055)</ta>
            <ta e="T350" id="Seg_5289" s="T346">PKZ_19700821_09339-2a.PKZ.035 (056)</ta>
            <ta e="T355" id="Seg_5290" s="T350">PKZ_19700821_09339-2a.PKZ.036 (057)</ta>
            <ta e="T360" id="Seg_5291" s="T355">PKZ_19700821_09339-2a.PKZ.037 (058)</ta>
            <ta e="T367" id="Seg_5292" s="T360">PKZ_19700821_09339-2a.PKZ.038 (059)</ta>
            <ta e="T377" id="Seg_5293" s="T368">PKZ_19700821_09339-2a.PKZ.039 (061)</ta>
            <ta e="T380" id="Seg_5294" s="T377">PKZ_19700821_09339-2a.PKZ.040 (062)</ta>
            <ta e="T384" id="Seg_5295" s="T380">PKZ_19700821_09339-2a.PKZ.041 (063)</ta>
            <ta e="T389" id="Seg_5296" s="T384">PKZ_19700821_09339-2a.PKZ.042 (064)</ta>
            <ta e="T397" id="Seg_5297" s="T389">PKZ_19700821_09339-2a.PKZ.043 (065)</ta>
            <ta e="T398" id="Seg_5298" s="T397">PKZ_19700821_09339-2a.PKZ.044 (066)</ta>
            <ta e="T403" id="Seg_5299" s="T398">PKZ_19700821_09339-2a.PKZ.045 (067)</ta>
            <ta e="T407" id="Seg_5300" s="T403">PKZ_19700821_09339-2a.PKZ.046 (068)</ta>
            <ta e="T408" id="Seg_5301" s="T407">PKZ_19700821_09339-2a.PKZ.047 (069)</ta>
            <ta e="T413" id="Seg_5302" s="T408">PKZ_19700821_09339-2a.PKZ.048 (070)</ta>
            <ta e="T430" id="Seg_5303" s="T413">PKZ_19700821_09339-2a.PKZ.049 (071)</ta>
            <ta e="T436" id="Seg_5304" s="T430">PKZ_19700821_09339-2a.PKZ.050 (072)</ta>
            <ta e="T444" id="Seg_5305" s="T436">PKZ_19700821_09339-2a.PKZ.051 (073)</ta>
            <ta e="T454" id="Seg_5306" s="T444">PKZ_19700821_09339-2a.PKZ.052 (074)</ta>
            <ta e="T463" id="Seg_5307" s="T454">PKZ_19700821_09339-2a.PKZ.053 (075)</ta>
            <ta e="T468" id="Seg_5308" s="T463">PKZ_19700821_09339-2a.PKZ.054 (076)</ta>
            <ta e="T471" id="Seg_5309" s="T468">PKZ_19700821_09339-2a.PKZ.055 (077)</ta>
            <ta e="T474" id="Seg_5310" s="T471">PKZ_19700821_09339-2a.PKZ.056 (078)</ta>
            <ta e="T479" id="Seg_5311" s="T474">PKZ_19700821_09339-2a.PKZ.057 (079)</ta>
            <ta e="T486" id="Seg_5312" s="T479">PKZ_19700821_09339-2a.PKZ.058 (080)</ta>
            <ta e="T490" id="Seg_5313" s="T486">PKZ_19700821_09339-2a.PKZ.059 (081)</ta>
            <ta e="T492" id="Seg_5314" s="T490">PKZ_19700821_09339-2a.PKZ.060 (082)</ta>
            <ta e="T496" id="Seg_5315" s="T492">PKZ_19700821_09339-2a.PKZ.061 (083)</ta>
            <ta e="T499" id="Seg_5316" s="T496">PKZ_19700821_09339-2a.PKZ.062 (084)</ta>
            <ta e="T500" id="Seg_5317" s="T499">PKZ_19700821_09339-2a.PKZ.063 (084)</ta>
            <ta e="T574" id="Seg_5318" s="T565">PKZ_19700821_09339-2a.PKZ.064 (093)</ta>
            <ta e="T576" id="Seg_5319" s="T574">PKZ_19700821_09339-2a.PKZ.065 (094)</ta>
            <ta e="T583" id="Seg_5320" s="T576">PKZ_19700821_09339-2a.PKZ.066 (095)</ta>
            <ta e="T585" id="Seg_5321" s="T583">PKZ_19700821_09339-2a.PKZ.067 (096)</ta>
            <ta e="T597" id="Seg_5322" s="T585">PKZ_19700821_09339-2a.PKZ.068 (097)</ta>
            <ta e="T603" id="Seg_5323" s="T597">PKZ_19700821_09339-2a.PKZ.069 (098)</ta>
            <ta e="T609" id="Seg_5324" s="T603">PKZ_19700821_09339-2a.PKZ.070 (099)</ta>
            <ta e="T616" id="Seg_5325" s="T609">PKZ_19700821_09339-2a.PKZ.071 (100)</ta>
            <ta e="T629" id="Seg_5326" s="T616">PKZ_19700821_09339-2a.PKZ.072 (101)</ta>
            <ta e="T633" id="Seg_5327" s="T629">PKZ_19700821_09339-2a.PKZ.073 (103)</ta>
            <ta e="T647" id="Seg_5328" s="T633">PKZ_19700821_09339-2a.PKZ.074 (104)</ta>
            <ta e="T654" id="Seg_5329" s="T647">PKZ_19700821_09339-2a.PKZ.075 (105)</ta>
            <ta e="T667" id="Seg_5330" s="T654">PKZ_19700821_09339-2a.PKZ.076 (106)</ta>
            <ta e="T673" id="Seg_5331" s="T667">PKZ_19700821_09339-2a.PKZ.077 (107)</ta>
            <ta e="T682" id="Seg_5332" s="T673">PKZ_19700821_09339-2a.PKZ.078 (108)</ta>
            <ta e="T696" id="Seg_5333" s="T682">PKZ_19700821_09339-2a.PKZ.079 (109)</ta>
            <ta e="T700" id="Seg_5334" s="T696">PKZ_19700821_09339-2a.PKZ.080 (110)</ta>
            <ta e="T714" id="Seg_5335" s="T700">PKZ_19700821_09339-2a.PKZ.081 (111)</ta>
            <ta e="T725" id="Seg_5336" s="T714">PKZ_19700821_09339-2a.PKZ.082 (112)</ta>
            <ta e="T733" id="Seg_5337" s="T725">PKZ_19700821_09339-2a.PKZ.083 (113)</ta>
            <ta e="T743" id="Seg_5338" s="T733">PKZ_19700821_09339-2a.PKZ.084 (114)</ta>
            <ta e="T755" id="Seg_5339" s="T743">PKZ_19700821_09339-2a.PKZ.085 (115)</ta>
            <ta e="T772" id="Seg_5340" s="T755">PKZ_19700821_09339-2a.PKZ.086 (116) </ta>
            <ta e="T781" id="Seg_5341" s="T772">PKZ_19700821_09339-2a.PKZ.087 (118)</ta>
            <ta e="T800" id="Seg_5342" s="T781">PKZ_19700821_09339-2a.PKZ.088 (119)</ta>
            <ta e="T822" id="Seg_5343" s="T800">PKZ_19700821_09339-2a.PKZ.089 (120)</ta>
            <ta e="T832" id="Seg_5344" s="T822">PKZ_19700821_09339-2a.PKZ.090 (121)</ta>
            <ta e="T848" id="Seg_5345" s="T832">PKZ_19700821_09339-2a.PKZ.091 (122)</ta>
            <ta e="T850" id="Seg_5346" s="T848">PKZ_19700821_09339-2a.PKZ.092 (123)</ta>
            <ta e="T856" id="Seg_5347" s="T850">PKZ_19700821_09339-2a.PKZ.093 (124)</ta>
            <ta e="T861" id="Seg_5348" s="T856">PKZ_19700821_09339-2a.PKZ.094 (125)</ta>
            <ta e="T876" id="Seg_5349" s="T864">PKZ_19700821_09339-2a.PKZ.095 (127)</ta>
            <ta e="T885" id="Seg_5350" s="T878">PKZ_19700821_09339-2a.PKZ.096 (130)</ta>
            <ta e="T891" id="Seg_5351" s="T885">PKZ_19700821_09339-2a.PKZ.097 (131)</ta>
            <ta e="T894" id="Seg_5352" s="T892">PKZ_19700821_09339-2a.PKZ.098 (133)</ta>
            <ta e="T938" id="Seg_5353" s="T928">PKZ_19700821_09339-2a.PKZ.099 (137)</ta>
            <ta e="T956" id="Seg_5354" s="T943">PKZ_19700821_09339-2a.PKZ.100 (139)</ta>
            <ta e="T962" id="Seg_5355" s="T956">PKZ_19700821_09339-2a.PKZ.101 (140)</ta>
            <ta e="T974" id="Seg_5356" s="T962">PKZ_19700821_09339-2a.PKZ.102 (141)</ta>
            <ta e="T976" id="Seg_5357" s="T974">PKZ_19700821_09339-2a.PKZ.103 (142)</ta>
            <ta e="T985" id="Seg_5358" s="T976">PKZ_19700821_09339-2a.PKZ.104 (143)</ta>
            <ta e="T992" id="Seg_5359" s="T985">PKZ_19700821_09339-2a.PKZ.105 (144)</ta>
            <ta e="T996" id="Seg_5360" s="T992">PKZ_19700821_09339-2a.PKZ.106 (145)</ta>
            <ta e="T1003" id="Seg_5361" s="T996">PKZ_19700821_09339-2a.PKZ.107 (146)</ta>
            <ta e="T1006" id="Seg_5362" s="T1003">PKZ_19700821_09339-2a.PKZ.108 (147)</ta>
            <ta e="T1013" id="Seg_5363" s="T1006">PKZ_19700821_09339-2a.PKZ.109 (148)</ta>
            <ta e="T1015" id="Seg_5364" s="T1013">PKZ_19700821_09339-2a.PKZ.110 (149)</ta>
            <ta e="T1018" id="Seg_5365" s="T1015">PKZ_19700821_09339-2a.PKZ.111 (150)</ta>
            <ta e="T1023" id="Seg_5366" s="T1018">PKZ_19700821_09339-2a.PKZ.112 (151)</ta>
            <ta e="T1037" id="Seg_5367" s="T1023">PKZ_19700821_09339-2a.PKZ.113 (152)</ta>
            <ta e="T1042" id="Seg_5368" s="T1037">PKZ_19700821_09339-2a.PKZ.114 (153)</ta>
            <ta e="T1049" id="Seg_5369" s="T1042">PKZ_19700821_09339-2a.PKZ.115 (154)</ta>
            <ta e="T1054" id="Seg_5370" s="T1049">PKZ_19700821_09339-2a.PKZ.116 (155)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PKZ">
            <ta e="T8" id="Seg_5371" s="T3">Американский наставник прислал нам бумагу. </ta>
            <ta e="T20" id="Seg_5372" s="T8">В нашей местности гнали (все-) верующих, шибко притесняли, молиться не давали. </ta>
            <ta e="T40" id="Seg_5373" s="T20">Ну, он услыхал, что так тут притеснения в нашей русской земле, стал просить у нашего правителя: "Отдай мне верующих людей". </ta>
            <ta e="T49" id="Seg_5374" s="T40">Ну, бумага пришла, даже у нас в Абалакове была. </ta>
            <ta e="T64" id="Seg_5375" s="T49">Стали писать, и которые не веровали и ругали бога, и те записались, что они верующие. </ta>
            <ta e="T84" id="Seg_5376" s="T64">Тады наш наставник (погляд-) поглядел, что все люди стали верующие, и (н-) не отдал в Америку людей своих. </ta>
            <ta e="T90" id="Seg_5377" s="T84">A (miʔnʼibeʔ) ej mĭbiʔi kudajdə numan üzəsʼtə ((NOISE)). </ta>
            <ta e="T95" id="Seg_5378" s="T90">Dĭgəttə ((NOISE)) pʼaŋdəbi (s-) sazən Америка. </ta>
            <ta e="T107" id="Seg_5379" s="T95">"(Mĭndəd- mĭ- mĭ-) (Mĭbəleʔ) măna dĭ il, girgit il kudajdə (uman-) numan üzəlieʔi". </ta>
            <ta e="T110" id="Seg_5380" s="T107">Dĭgəttə sazən šobi. </ta>
            <ta e="T116" id="Seg_5381" s="T110">Pʼaŋdəbiʔi šindi (i-) kudajdə (kudonz-) kudolbi. </ta>
            <ta e="T123" id="Seg_5382" s="T116">I dĭbər sazəndə pʼaŋdəbi, što kudajdə numan üzleʔbəm. </ta>
            <ta e="T124" id="Seg_5383" s="T123">Kabarləj. </ta>
            <ta e="T149" id="Seg_5384" s="T147">Можно рассказать … </ta>
            <ta e="T156" id="Seg_5385" s="T153">Можно и это. </ta>
            <ta e="T161" id="Seg_5386" s="T160">А? </ta>
            <ta e="T168" id="Seg_5387" s="T164">Ну, ничего. </ta>
            <ta e="T177" id="Seg_5388" s="T172">Все хорошие, приветствуют хорошо. </ta>
            <ta e="T196" id="Seg_5389" s="T189">(Все=) Все обнимали, целовали даже. </ta>
            <ta e="T220" id="Seg_5390" s="T214">Ну как, по-русски рассказывать? </ta>
            <ta e="T236" id="Seg_5391" s="T232">Там он говорит уже? </ta>
            <ta e="T262" id="Seg_5392" s="T248">Када я на (конгре-) на конгрет пришла, все такие радые. </ta>
            <ta e="T268" id="Seg_5393" s="T262">Все люди хороши, приняли меня. </ta>
            <ta e="T272" id="Seg_5394" s="T268">Все ученые, умные люди. </ta>
            <ta e="T274" id="Seg_5395" s="T272">Интересные, красивые. </ta>
            <ta e="T284" id="Seg_5396" s="T274">Ну, красивше всех мой Агафон Иванович, я его люблю шибко. </ta>
            <ta e="T296" id="Seg_5397" s="T284">Он мой ученик, я его учила (на с-) на свое наречие. </ta>
            <ta e="T301" id="Seg_5398" s="T296">Ну, и поглянулись и все. </ta>
            <ta e="T306" id="Seg_5399" s="T301">А (теперь) поехала на кладбище. </ta>
            <ta e="T310" id="Seg_5400" s="T306">Там тоже хорошо было. </ta>
            <ta e="T317" id="Seg_5401" s="T310">И там я на свое наречие пела. </ta>
            <ta e="T327" id="Seg_5402" s="T317">Божественные молитвы, все тоже такие рады были, все меня приветствовали. </ta>
            <ta e="T328" id="Seg_5403" s="T327">Приглашали. </ta>
            <ta e="T346" id="Seg_5404" s="T328">Там я видала много белок на кладбище бегают, (а у= а у нас=) а у нас там нету. </ta>
            <ta e="T350" id="Seg_5405" s="T346">Ну наши охотники убивают. </ta>
            <ta e="T355" id="Seg_5406" s="T350">А тут не убивают белок. </ta>
            <ta e="T360" id="Seg_5407" s="T355">Я (имя) давала кушать там. </ta>
            <ta e="T367" id="Seg_5408" s="T360">Ну, теперь надо по - своему сказать. </ta>
            <ta e="T377" id="Seg_5409" s="T368">Măn ertən uʔbdəbiam, kudajdə numan üzəbiem, edəʔleʔbəm (Aga-) Agafon Ivanovičəm. </ta>
            <ta e="T380" id="Seg_5410" s="T377">Dĭ šobi măna. </ta>
            <ta e="T384" id="Seg_5411" s="T380">Dĭgəttə măn dĭzi kambiam. </ta>
            <ta e="T389" id="Seg_5412" s="T384">Šobiam dĭbər, (dĭ=) dĭ kănferentsianə. </ta>
            <ta e="T397" id="Seg_5413" s="T389">Iʔgö (i-) il (dĭn), bar (uʔ-) kuvas ugandə. </ta>
            <ta e="T398" id="Seg_5414" s="T397">Jakšeʔi. </ta>
            <ta e="T403" id="Seg_5415" s="T398">Pʼaŋzittə tăŋ tĭmneʔi, šagəstə iʔgö. </ta>
            <ta e="T407" id="Seg_5416" s="T403">Dĭgəttə bar măna pănarlaʔbəʔjə. </ta>
            <ta e="T408" id="Seg_5417" s="T407">Nezeŋ. </ta>
            <ta e="T413" id="Seg_5418" s="T408">A tibizeŋ (uda-) udazaŋdə nuleʔbəʔjə. </ta>
            <ta e="T430" id="Seg_5419" s="T413">Dĭgəttə dĭgəʔ (š-) kambiam, mašinanə (amnubi=) amnobiam, (ibiem) dĭn (gi-) gijən sazən (aliaʔi) i knižkaʔi gijən (enləʔbəʔjə). </ta>
            <ta e="T436" id="Seg_5420" s="T430">Dĭn măna tože (kü-) kürbiʔi kartačkanə. </ta>
            <ta e="T444" id="Seg_5421" s="T436">Dĭn amnobiam, dʼăbaktərbiam, dĭn tože ugandə jakše il. </ta>
            <ta e="T454" id="Seg_5422" s="T444">Dĭgəttə (dĭzeŋ=) dĭzeŋdə mămbiam: "(kudaj=) kudaj šiʔsi", i (bosəltə) kambiam. </ta>
            <ta e="T463" id="Seg_5423" s="T454">Dĭgəttə dĭn nʼüʔnən (ibie-) ibiem, dĭgəttə nubibaʔ dʼünə (öʔlüʔpiʔi). </ta>
            <ta e="T468" id="Seg_5424" s="T463">Gəttə kambibaʔ, gijen (k-) krospaʔi. </ta>
            <ta e="T471" id="Seg_5425" s="T468">Dĭn tože mĭmbibeʔ. </ta>
            <ta e="T474" id="Seg_5426" s="T471">Iʔgö nezeŋ ibiʔi. </ta>
            <ta e="T479" id="Seg_5427" s="T474">I (onʼiʔ=) šide tibi ibiʔi. </ta>
            <ta e="T486" id="Seg_5428" s="T479">Tože bar măna pănarbiʔi, udaʔi (mĭmbiʔi=) mĭbiʔi. </ta>
            <ta e="T490" id="Seg_5429" s="T486">Măndəʔi:" Jakše igel ugandə. </ta>
            <ta e="T492" id="Seg_5430" s="T490">Kuŋgə šobial?" </ta>
            <ta e="T496" id="Seg_5431" s="T492">Măn mămbiam:" Kuŋgə šobiam". </ta>
            <ta e="T499" id="Seg_5432" s="T496">(Măngoliaʔi) tondə amnobiam. </ta>
            <ta e="T500" id="Seg_5433" s="T499">Kabarləj. </ta>
            <ta e="T574" id="Seg_5434" s="T565">Мы с этой, с Ридой ждали-ждали Арпита. </ta>
            <ta e="T576" id="Seg_5435" s="T574">Нету, нету. </ta>
            <ta e="T583" id="Seg_5436" s="T576">Потом она сходила туды, к евонной теще. </ta>
            <ta e="T585" id="Seg_5437" s="T583">"Пойду узнаю". </ta>
            <ta e="T597" id="Seg_5438" s="T585">А я ее не пускаю:" Не ходи, говорю, все равно он придет. </ta>
            <ta e="T603" id="Seg_5439" s="T597">Разве только заболел дак не придет. </ta>
            <ta e="T609" id="Seg_5440" s="T603">А то придет, обманывать не будет. </ta>
            <ta e="T616" id="Seg_5441" s="T609">Он же верующий человек, ему нельзя обмануть. </ta>
            <ta e="T629" id="Seg_5442" s="T616">Как он ко мне приехал, я тоже писала: Когда приедет Агафон Иванович, приеду. </ta>
            <ta e="T633" id="Seg_5443" s="T629">Он приехал ко мне. </ta>
            <ta e="T647" id="Seg_5444" s="T633">Ну, я раз обещалась, ни на что не погляделась, собралась и с им поехала. </ta>
            <ta e="T654" id="Seg_5445" s="T647">Приехали в Агинска, там тоже наши верующие. </ta>
            <ta e="T667" id="Seg_5446" s="T654">Там я ему предлагала, говорю:" Приезжай к нам, мы тебе тут дом купим". </ta>
            <ta e="T673" id="Seg_5447" s="T667">Ну, оттель мы пять часов … </ta>
            <ta e="T682" id="Seg_5448" s="T673">Нас проводили тоже сестры верующие, мы приехали в Уяр. </ta>
            <ta e="T696" id="Seg_5449" s="T682">Ну, он - то, конечно, не знал, где, а я там знаю своих верующих. </ta>
            <ta e="T700" id="Seg_5450" s="T696">Ну, пошли с им. </ta>
            <ta e="T714" id="Seg_5451" s="T700">У людей спрашиваю, а фамилие-то я забыла, только знаю: Илюша и Маруся. </ta>
            <ta e="T725" id="Seg_5452" s="T714">Но, (зн -) место - то знаю, заметно, была я там. </ta>
            <ta e="T733" id="Seg_5453" s="T725">Арпиту говорю:" Вы постойте тут, а я зайду". </ta>
            <ta e="T743" id="Seg_5454" s="T733">(За -) Захожу в первую (и -) в перво помещение. </ta>
            <ta e="T755" id="Seg_5455" s="T743">"(Не з -) Не заходи, тут, - говорит, - никто не живет". </ta>
            <ta e="T772" id="Seg_5456" s="T755">Потом туды дальше пошла, оттель спросила у человека: Я говорю:" Знаете Илюшу (и М -) с Марусей?" </ta>
            <ta e="T781" id="Seg_5457" s="T772">Он говорит: "Оне со мной работают", и меня повел. </ta>
            <ta e="T800" id="Seg_5458" s="T781">А я брату сказала: "(И -) Иди сюда", и он пришел, и как раз она идет тут с водой. </ta>
            <ta e="T822" id="Seg_5459" s="T800">И мы тут тоже долго беседовали, там еще один наш брат приходил, такой же, как брат Арпит, (он=) я там часто бываю. </ta>
            <ta e="T832" id="Seg_5460" s="T822">Потому что у нас в деревне таких нету, как я. </ta>
            <ta e="T848" id="Seg_5461" s="T832">Ну я ли езжу в Красноярска, и там у нас еще есть разъезд, туда я езжу. </ta>
            <ta e="T850" id="Seg_5462" s="T848">Заозерка станция. </ta>
            <ta e="T856" id="Seg_5463" s="T850">Ты, наверное, может, был там (эка)? </ta>
            <ta e="T861" id="Seg_5464" s="T856">Не был на станции Заозерки? </ta>
            <ta e="T876" id="Seg_5465" s="T864">Оттель тоже можно в Красноярска уехать, и в Канска, в город Канск. </ta>
            <ta e="T885" id="Seg_5466" s="T878">Ну, я в Канске тоже была там. </ta>
            <ta e="T891" id="Seg_5467" s="T885">Тоже там есть верующие, там была. </ta>
            <ta e="T894" id="Seg_5468" s="T892">Так… </ta>
            <ta e="T938" id="Seg_5469" s="T928">Я уже много наговорила по-русски. </ta>
            <ta e="T956" id="Seg_5470" s="T943">Nu, miʔ taldʼen (Vidazi) edəʔleʔbəbeʔ üge, kagam edəʔleʔbə, (gə) naga, naga, dĭgəttə … </ta>
            <ta e="T962" id="Seg_5471" s="T956">Bazaj aʔtʼi (küzürleʔbə), măn măndəm:" Šonəga". </ta>
            <ta e="T974" id="Seg_5472" s="T962">Nu (n-), dĭ šobi, možet iandə kambi, dĭn ambi da (iʔbi-) iʔbəbi. </ta>
            <ta e="T976" id="Seg_5473" s="T974">(Šalaʔjə) kunolzittə. </ta>
            <ta e="T985" id="Seg_5474" s="T976">Dĭgəttə amnobibaʔ, i deʔpibeʔ (deʔpibeʔ ig- li-) iʔbəbeʔ kunolzittə. </ta>
            <ta e="T992" id="Seg_5475" s="T985">Tolʼko (iʔbəbeʔ=) iʔpibeʔ, măn aʔpi i kunolbiam. </ta>
            <ta e="T996" id="Seg_5476" s="T992">Nu, днем šonəga šindidə. </ta>
            <ta e="T1003" id="Seg_5477" s="T996">Šolambi ((…)), măn suʔmiluʔpiam da dĭbər nuʔməluʔpiam. </ta>
            <ta e="T1006" id="Seg_5478" s="T1003">Dĭnə udam mĭbiem. </ta>
            <ta e="T1013" id="Seg_5479" s="T1006">(Dʼăbaktərbibeʔ), (dĭ măndə):" Măn băzəjdlam, vannan kalam". </ta>
            <ta e="T1015" id="Seg_5480" s="T1013">"No, kanaʔ". </ta>
            <ta e="T1018" id="Seg_5481" s="T1015">Dĭgəttə dĭ kambi. </ta>
            <ta e="T1023" id="Seg_5482" s="T1018">Măn mămbiam:" Amoraʔ, dĭgəttə kalal". </ta>
            <ta e="T1037" id="Seg_5483" s="T1023">Dĭgəttə dĭ (ejümləbi) mĭj, segi bü ((…)) (dĭ ambi), dĭgəttə (ka-) kambi băzəjdəsʼtə vannan. </ta>
            <ta e="T1042" id="Seg_5484" s="T1037">Dĭgəttə dĭ dĭʔnə iʔbəsʼtə dʼajirbi. </ta>
            <ta e="T1049" id="Seg_5485" s="T1042">Dön nuldəbi (кровать, dĭ) iʔbolaʔpi, i kunollubibaʔ. </ta>
            <ta e="T1054" id="Seg_5486" s="T1049">Ertən bazo (uʔbdə-) uʔbdəbibaʔ … </ta>
         </annotation>
         <annotation name="mb" tierref="mb-PKZ">
            <ta e="T85" id="Seg_5487" s="T84">a</ta>
            <ta e="T86" id="Seg_5488" s="T85">miʔnʼibeʔ</ta>
            <ta e="T87" id="Seg_5489" s="T86">ej</ta>
            <ta e="T88" id="Seg_5490" s="T87">mĭ-bi-ʔi</ta>
            <ta e="T89" id="Seg_5491" s="T88">kudaj-də</ta>
            <ta e="T90" id="Seg_5492" s="T89">numan üzə-sʼtə</ta>
            <ta e="T91" id="Seg_5493" s="T90">dĭgəttə</ta>
            <ta e="T92" id="Seg_5494" s="T91">pʼaŋdə-bi</ta>
            <ta e="T94" id="Seg_5495" s="T93">sazən</ta>
            <ta e="T99" id="Seg_5496" s="T98">mĭ-bə-leʔ</ta>
            <ta e="T100" id="Seg_5497" s="T99">măna</ta>
            <ta e="T101" id="Seg_5498" s="T100">dĭ</ta>
            <ta e="T102" id="Seg_5499" s="T101">il</ta>
            <ta e="T103" id="Seg_5500" s="T102">girgit</ta>
            <ta e="T104" id="Seg_5501" s="T103">il</ta>
            <ta e="T105" id="Seg_5502" s="T104">kudaj-də</ta>
            <ta e="T107" id="Seg_5503" s="T106">numan üzə-lie-ʔi</ta>
            <ta e="T108" id="Seg_5504" s="T107">dĭgəttə</ta>
            <ta e="T109" id="Seg_5505" s="T108">sazən</ta>
            <ta e="T110" id="Seg_5506" s="T109">šo-bi</ta>
            <ta e="T111" id="Seg_5507" s="T110">pʼaŋdə-bi-ʔi</ta>
            <ta e="T112" id="Seg_5508" s="T111">šindi</ta>
            <ta e="T114" id="Seg_5509" s="T113">kudaj-də</ta>
            <ta e="T116" id="Seg_5510" s="T115">kudo-l-bi</ta>
            <ta e="T117" id="Seg_5511" s="T116">i</ta>
            <ta e="T118" id="Seg_5512" s="T117">dĭbər</ta>
            <ta e="T119" id="Seg_5513" s="T118">sazən-də</ta>
            <ta e="T120" id="Seg_5514" s="T119">pʼaŋdə-bi</ta>
            <ta e="T121" id="Seg_5515" s="T120">što</ta>
            <ta e="T122" id="Seg_5516" s="T121">kudaj-də</ta>
            <ta e="T123" id="Seg_5517" s="T122">numan üz-leʔbə-m</ta>
            <ta e="T124" id="Seg_5518" s="T123">kabarləj</ta>
            <ta e="T369" id="Seg_5519" s="T368">măn</ta>
            <ta e="T370" id="Seg_5520" s="T369">ertə-n</ta>
            <ta e="T371" id="Seg_5521" s="T370">uʔbdə-bia-m</ta>
            <ta e="T372" id="Seg_5522" s="T371">kudaj-də</ta>
            <ta e="T373" id="Seg_5523" s="T372">numan üzə-bie-m</ta>
            <ta e="T374" id="Seg_5524" s="T373">edəʔ-leʔbə-m</ta>
            <ta e="T376" id="Seg_5525" s="T375">Agafon</ta>
            <ta e="T377" id="Seg_5526" s="T376">Ivanovič-əm</ta>
            <ta e="T378" id="Seg_5527" s="T377">dĭ</ta>
            <ta e="T379" id="Seg_5528" s="T378">šo-bi</ta>
            <ta e="T380" id="Seg_5529" s="T379">măna</ta>
            <ta e="T381" id="Seg_5530" s="T380">dĭgəttə</ta>
            <ta e="T382" id="Seg_5531" s="T381">măn</ta>
            <ta e="T383" id="Seg_5532" s="T382">dĭ-zi</ta>
            <ta e="T384" id="Seg_5533" s="T383">kam-bia-m</ta>
            <ta e="T385" id="Seg_5534" s="T384">šo-bia-m</ta>
            <ta e="T386" id="Seg_5535" s="T385">dĭbər</ta>
            <ta e="T387" id="Seg_5536" s="T386">dĭ</ta>
            <ta e="T388" id="Seg_5537" s="T387">dĭ</ta>
            <ta e="T389" id="Seg_5538" s="T388">kănferentsia-nə</ta>
            <ta e="T390" id="Seg_5539" s="T389">iʔgö</ta>
            <ta e="T392" id="Seg_5540" s="T391">il</ta>
            <ta e="T393" id="Seg_5541" s="T392">dĭn</ta>
            <ta e="T394" id="Seg_5542" s="T393">bar</ta>
            <ta e="T396" id="Seg_5543" s="T395">kuvas</ta>
            <ta e="T397" id="Seg_5544" s="T396">ugandə</ta>
            <ta e="T398" id="Seg_5545" s="T397">jakše-ʔi</ta>
            <ta e="T399" id="Seg_5546" s="T398">pʼaŋ-zittə</ta>
            <ta e="T400" id="Seg_5547" s="T399">tăŋ</ta>
            <ta e="T401" id="Seg_5548" s="T400">tĭmne-ʔi</ta>
            <ta e="T402" id="Seg_5549" s="T401">šagəstə</ta>
            <ta e="T403" id="Seg_5550" s="T402">iʔgö</ta>
            <ta e="T404" id="Seg_5551" s="T403">dĭgəttə</ta>
            <ta e="T405" id="Seg_5552" s="T404">bar</ta>
            <ta e="T406" id="Seg_5553" s="T405">măna</ta>
            <ta e="T407" id="Seg_5554" s="T406">pănar-laʔbə-ʔjə</ta>
            <ta e="T408" id="Seg_5555" s="T407">ne-zeŋ</ta>
            <ta e="T409" id="Seg_5556" s="T408">a</ta>
            <ta e="T410" id="Seg_5557" s="T409">tibi-zeŋ</ta>
            <ta e="T412" id="Seg_5558" s="T411">uda-zaŋ-də</ta>
            <ta e="T413" id="Seg_5559" s="T412">nu-leʔbə-ʔjə</ta>
            <ta e="T414" id="Seg_5560" s="T413">dĭgəttə</ta>
            <ta e="T415" id="Seg_5561" s="T414">dĭ-gəʔ</ta>
            <ta e="T417" id="Seg_5562" s="T416">kam-bia-m</ta>
            <ta e="T418" id="Seg_5563" s="T417">mašina-nə</ta>
            <ta e="T420" id="Seg_5564" s="T419">amno-bia-m</ta>
            <ta e="T421" id="Seg_5565" s="T420">i-bie-m</ta>
            <ta e="T422" id="Seg_5566" s="T421">dĭn</ta>
            <ta e="T424" id="Seg_5567" s="T423">gijən</ta>
            <ta e="T425" id="Seg_5568" s="T424">sazən</ta>
            <ta e="T426" id="Seg_5569" s="T425">a-lia-ʔi</ta>
            <ta e="T427" id="Seg_5570" s="T426">i</ta>
            <ta e="T428" id="Seg_5571" s="T427">knižka-ʔi</ta>
            <ta e="T429" id="Seg_5572" s="T428">gijən</ta>
            <ta e="T430" id="Seg_5573" s="T429">en-ləʔbə-ʔjə</ta>
            <ta e="T431" id="Seg_5574" s="T430">dĭn</ta>
            <ta e="T432" id="Seg_5575" s="T431">măna</ta>
            <ta e="T433" id="Seg_5576" s="T432">tože</ta>
            <ta e="T435" id="Seg_5577" s="T434">kür-bi-ʔi</ta>
            <ta e="T436" id="Seg_5578" s="T435">kartačka-nə</ta>
            <ta e="T437" id="Seg_5579" s="T436">dĭn</ta>
            <ta e="T438" id="Seg_5580" s="T437">amno-bia-m</ta>
            <ta e="T439" id="Seg_5581" s="T438">dʼăbaktər-bia-m</ta>
            <ta e="T440" id="Seg_5582" s="T439">dĭn</ta>
            <ta e="T441" id="Seg_5583" s="T440">tože</ta>
            <ta e="T442" id="Seg_5584" s="T441">ugandə</ta>
            <ta e="T443" id="Seg_5585" s="T442">jakše</ta>
            <ta e="T444" id="Seg_5586" s="T443">il</ta>
            <ta e="T445" id="Seg_5587" s="T444">dĭgəttə</ta>
            <ta e="T446" id="Seg_5588" s="T445">dĭ-zeŋ</ta>
            <ta e="T447" id="Seg_5589" s="T446">dĭ-zeŋ-də</ta>
            <ta e="T448" id="Seg_5590" s="T447">măm-bia-m</ta>
            <ta e="T449" id="Seg_5591" s="T448">kudaj</ta>
            <ta e="T450" id="Seg_5592" s="T449">kudaj</ta>
            <ta e="T451" id="Seg_5593" s="T450">šiʔ-si</ta>
            <ta e="T452" id="Seg_5594" s="T451">i</ta>
            <ta e="T453" id="Seg_5595" s="T452">bosəltə</ta>
            <ta e="T454" id="Seg_5596" s="T453">kam-bia-m</ta>
            <ta e="T455" id="Seg_5597" s="T454">dĭgəttə</ta>
            <ta e="T456" id="Seg_5598" s="T455">dĭn</ta>
            <ta e="T457" id="Seg_5599" s="T456">nʼüʔnən</ta>
            <ta e="T459" id="Seg_5600" s="T458">i-bie-m</ta>
            <ta e="T460" id="Seg_5601" s="T459">dĭgəttə</ta>
            <ta e="T461" id="Seg_5602" s="T460">nu-bi-baʔ</ta>
            <ta e="T462" id="Seg_5603" s="T461">dʼü-nə</ta>
            <ta e="T463" id="Seg_5604" s="T462">öʔlüʔ-pi-ʔi</ta>
            <ta e="T464" id="Seg_5605" s="T463">gəttə</ta>
            <ta e="T465" id="Seg_5606" s="T464">kam-bi-baʔ</ta>
            <ta e="T466" id="Seg_5607" s="T465">gijen</ta>
            <ta e="T468" id="Seg_5608" s="T467">krospa-ʔi</ta>
            <ta e="T469" id="Seg_5609" s="T468">dĭn</ta>
            <ta e="T470" id="Seg_5610" s="T469">tože</ta>
            <ta e="T471" id="Seg_5611" s="T470">mĭm-bi-beʔ</ta>
            <ta e="T472" id="Seg_5612" s="T471">iʔgö</ta>
            <ta e="T473" id="Seg_5613" s="T472">ne-zeŋ</ta>
            <ta e="T474" id="Seg_5614" s="T473">i-bi-ʔi</ta>
            <ta e="T475" id="Seg_5615" s="T474">i</ta>
            <ta e="T476" id="Seg_5616" s="T475">onʼiʔ</ta>
            <ta e="T477" id="Seg_5617" s="T476">šide</ta>
            <ta e="T478" id="Seg_5618" s="T477">tibi</ta>
            <ta e="T479" id="Seg_5619" s="T478">i-bi-ʔi</ta>
            <ta e="T480" id="Seg_5620" s="T479">tože</ta>
            <ta e="T481" id="Seg_5621" s="T480">bar</ta>
            <ta e="T482" id="Seg_5622" s="T481">măna</ta>
            <ta e="T483" id="Seg_5623" s="T482">pănar-bi-ʔi</ta>
            <ta e="T484" id="Seg_5624" s="T483">uda-ʔi</ta>
            <ta e="T485" id="Seg_5625" s="T484">mĭm-bi-ʔi</ta>
            <ta e="T486" id="Seg_5626" s="T485">mĭ-bi-ʔi</ta>
            <ta e="T487" id="Seg_5627" s="T486">măn-də-ʔi</ta>
            <ta e="T488" id="Seg_5628" s="T487">jakše</ta>
            <ta e="T489" id="Seg_5629" s="T488">i-ge-l</ta>
            <ta e="T490" id="Seg_5630" s="T489">ugandə</ta>
            <ta e="T491" id="Seg_5631" s="T490">kuŋgə</ta>
            <ta e="T492" id="Seg_5632" s="T491">šo-bia-l</ta>
            <ta e="T493" id="Seg_5633" s="T492">măn</ta>
            <ta e="T494" id="Seg_5634" s="T493">măm-bia-m</ta>
            <ta e="T495" id="Seg_5635" s="T494">kuŋgə</ta>
            <ta e="T496" id="Seg_5636" s="T495">šo-bia-m</ta>
            <ta e="T497" id="Seg_5637" s="T496">Măngolia-ʔi</ta>
            <ta e="T498" id="Seg_5638" s="T497">to-ndə</ta>
            <ta e="T499" id="Seg_5639" s="T498">amno-bia-m</ta>
            <ta e="T500" id="Seg_5640" s="T499">kabarləj</ta>
            <ta e="T944" id="Seg_5641" s="T943">nu</ta>
            <ta e="T945" id="Seg_5642" s="T944">miʔ</ta>
            <ta e="T946" id="Seg_5643" s="T945">taldʼen</ta>
            <ta e="T947" id="Seg_5644" s="T946">Vida-zi</ta>
            <ta e="T948" id="Seg_5645" s="T947">edəʔ-leʔbə-beʔ</ta>
            <ta e="T949" id="Seg_5646" s="T948">üge</ta>
            <ta e="T950" id="Seg_5647" s="T949">kaga-m</ta>
            <ta e="T951" id="Seg_5648" s="T950">edəʔ-leʔbə</ta>
            <ta e="T953" id="Seg_5649" s="T952">naga</ta>
            <ta e="T954" id="Seg_5650" s="T953">naga</ta>
            <ta e="T956" id="Seg_5651" s="T954">dĭgəttə</ta>
            <ta e="T957" id="Seg_5652" s="T956">baza-j</ta>
            <ta e="T958" id="Seg_5653" s="T957">aʔtʼi</ta>
            <ta e="T959" id="Seg_5654" s="T958">küzür-leʔbə</ta>
            <ta e="T960" id="Seg_5655" s="T959">măn</ta>
            <ta e="T961" id="Seg_5656" s="T960">măn-də-m</ta>
            <ta e="T962" id="Seg_5657" s="T961">šonə-ga</ta>
            <ta e="T963" id="Seg_5658" s="T962">nu</ta>
            <ta e="T965" id="Seg_5659" s="T964">dĭ</ta>
            <ta e="T966" id="Seg_5660" s="T965">šo-bi</ta>
            <ta e="T967" id="Seg_5661" s="T966">možet</ta>
            <ta e="T968" id="Seg_5662" s="T967">ia-ndə</ta>
            <ta e="T969" id="Seg_5663" s="T968">kam-bi</ta>
            <ta e="T970" id="Seg_5664" s="T969">dĭn</ta>
            <ta e="T971" id="Seg_5665" s="T970">am-bi</ta>
            <ta e="T972" id="Seg_5666" s="T971">da</ta>
            <ta e="T974" id="Seg_5667" s="T973">iʔbə-bi</ta>
            <ta e="T975" id="Seg_5668" s="T974">ša-la-ʔjə</ta>
            <ta e="T976" id="Seg_5669" s="T975">kunol-zittə</ta>
            <ta e="T977" id="Seg_5670" s="T976">dĭgəttə</ta>
            <ta e="T978" id="Seg_5671" s="T977">amno-bi-baʔ</ta>
            <ta e="T979" id="Seg_5672" s="T978">i</ta>
            <ta e="T980" id="Seg_5673" s="T979">deʔ-pi-beʔ</ta>
            <ta e="T981" id="Seg_5674" s="T980">deʔ-pi-beʔ</ta>
            <ta e="T984" id="Seg_5675" s="T983">iʔbə-beʔ</ta>
            <ta e="T985" id="Seg_5676" s="T984">kunol-zittə</ta>
            <ta e="T986" id="Seg_5677" s="T985">tolʼko</ta>
            <ta e="T987" id="Seg_5678" s="T986">iʔbə-beʔ</ta>
            <ta e="T988" id="Seg_5679" s="T987">iʔ-pi-beʔ</ta>
            <ta e="T989" id="Seg_5680" s="T988">măn</ta>
            <ta e="T990" id="Seg_5681" s="T989">aʔpi</ta>
            <ta e="T991" id="Seg_5682" s="T990">i</ta>
            <ta e="T992" id="Seg_5683" s="T991">kunol-bia-m</ta>
            <ta e="T993" id="Seg_5684" s="T992">nu</ta>
            <ta e="T995" id="Seg_5685" s="T994">šonə-ga</ta>
            <ta e="T996" id="Seg_5686" s="T995">šindi=də</ta>
            <ta e="T997" id="Seg_5687" s="T996">šo-lam-bi</ta>
            <ta e="T999" id="Seg_5688" s="T998">măn</ta>
            <ta e="T1000" id="Seg_5689" s="T999">suʔmi-luʔ-pia-m</ta>
            <ta e="T1001" id="Seg_5690" s="T1000">da</ta>
            <ta e="T1002" id="Seg_5691" s="T1001">dĭbər</ta>
            <ta e="T1003" id="Seg_5692" s="T1002">nuʔmə-luʔ-pia-m</ta>
            <ta e="T1004" id="Seg_5693" s="T1003">dĭ-nə</ta>
            <ta e="T1005" id="Seg_5694" s="T1004">uda-m</ta>
            <ta e="T1006" id="Seg_5695" s="T1005">mĭ-bie-m</ta>
            <ta e="T1007" id="Seg_5696" s="T1006">dʼăbaktər-bi-beʔ</ta>
            <ta e="T1008" id="Seg_5697" s="T1007">dĭ</ta>
            <ta e="T1009" id="Seg_5698" s="T1008">măn-də</ta>
            <ta e="T1010" id="Seg_5699" s="T1009">măn</ta>
            <ta e="T1011" id="Seg_5700" s="T1010">băzə-jd-la-m</ta>
            <ta e="T1012" id="Seg_5701" s="T1011">vanna-n</ta>
            <ta e="T1013" id="Seg_5702" s="T1012">ka-la-m</ta>
            <ta e="T1014" id="Seg_5703" s="T1013">no</ta>
            <ta e="T1015" id="Seg_5704" s="T1014">kan-a-ʔ</ta>
            <ta e="T1016" id="Seg_5705" s="T1015">dĭgəttə</ta>
            <ta e="T1017" id="Seg_5706" s="T1016">dĭ</ta>
            <ta e="T1018" id="Seg_5707" s="T1017">kam-bi</ta>
            <ta e="T1019" id="Seg_5708" s="T1018">măn</ta>
            <ta e="T1020" id="Seg_5709" s="T1019">măm-bia-m</ta>
            <ta e="T1021" id="Seg_5710" s="T1020">amor-a-ʔ</ta>
            <ta e="T1022" id="Seg_5711" s="T1021">dĭgəttə</ta>
            <ta e="T1023" id="Seg_5712" s="T1022">ka-la-l</ta>
            <ta e="T1024" id="Seg_5713" s="T1023">dĭgəttə</ta>
            <ta e="T1025" id="Seg_5714" s="T1024">dĭ</ta>
            <ta e="T1026" id="Seg_5715" s="T1025">ejüm-lə-bi</ta>
            <ta e="T1027" id="Seg_5716" s="T1026">mĭj</ta>
            <ta e="T1028" id="Seg_5717" s="T1027">segi</ta>
            <ta e="T1029" id="Seg_5718" s="T1028">bü</ta>
            <ta e="T1031" id="Seg_5719" s="T1030">dĭ</ta>
            <ta e="T1032" id="Seg_5720" s="T1031">am-bi</ta>
            <ta e="T1033" id="Seg_5721" s="T1032">dĭgəttə</ta>
            <ta e="T1035" id="Seg_5722" s="T1034">kam-bi</ta>
            <ta e="T1036" id="Seg_5723" s="T1035">băzə-jdə-sʼtə</ta>
            <ta e="T1037" id="Seg_5724" s="T1036">vanna-n</ta>
            <ta e="T1038" id="Seg_5725" s="T1037">dĭgəttə</ta>
            <ta e="T1039" id="Seg_5726" s="T1038">dĭ</ta>
            <ta e="T1040" id="Seg_5727" s="T1039">dĭʔ-nə</ta>
            <ta e="T1041" id="Seg_5728" s="T1040">iʔbə-sʼtə</ta>
            <ta e="T1042" id="Seg_5729" s="T1041">dʼajir-bi</ta>
            <ta e="T1043" id="Seg_5730" s="T1042">dön</ta>
            <ta e="T1044" id="Seg_5731" s="T1043">nuldə-bi</ta>
            <ta e="T1046" id="Seg_5732" s="T1045">dĭ</ta>
            <ta e="T1047" id="Seg_5733" s="T1046">iʔbo-laʔ-pi</ta>
            <ta e="T1048" id="Seg_5734" s="T1047">i</ta>
            <ta e="T1049" id="Seg_5735" s="T1048">kunol-lu-bi-baʔ</ta>
            <ta e="T1050" id="Seg_5736" s="T1049">ertə-n</ta>
            <ta e="T1051" id="Seg_5737" s="T1050">bazo</ta>
            <ta e="T1054" id="Seg_5738" s="T1052">uʔbdə-bi-baʔ</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PKZ">
            <ta e="T85" id="Seg_5739" s="T84">a</ta>
            <ta e="T86" id="Seg_5740" s="T85">miʔnʼibeʔ</ta>
            <ta e="T87" id="Seg_5741" s="T86">ej</ta>
            <ta e="T88" id="Seg_5742" s="T87">mĭ-bi-jəʔ</ta>
            <ta e="T89" id="Seg_5743" s="T88">kudaj-Tə</ta>
            <ta e="T90" id="Seg_5744" s="T89">numan üzə-zittə</ta>
            <ta e="T91" id="Seg_5745" s="T90">dĭgəttə</ta>
            <ta e="T92" id="Seg_5746" s="T91">pʼaŋdə-bi</ta>
            <ta e="T94" id="Seg_5747" s="T93">sazən</ta>
            <ta e="T99" id="Seg_5748" s="T98">mĭ-bə-lAʔ</ta>
            <ta e="T100" id="Seg_5749" s="T99">măna</ta>
            <ta e="T101" id="Seg_5750" s="T100">dĭ</ta>
            <ta e="T102" id="Seg_5751" s="T101">il</ta>
            <ta e="T103" id="Seg_5752" s="T102">girgit</ta>
            <ta e="T104" id="Seg_5753" s="T103">il</ta>
            <ta e="T105" id="Seg_5754" s="T104">kudaj-Tə</ta>
            <ta e="T107" id="Seg_5755" s="T106">numan üzə-liA-jəʔ</ta>
            <ta e="T108" id="Seg_5756" s="T107">dĭgəttə</ta>
            <ta e="T109" id="Seg_5757" s="T108">sazən</ta>
            <ta e="T110" id="Seg_5758" s="T109">šo-bi</ta>
            <ta e="T111" id="Seg_5759" s="T110">pʼaŋdə-bi-jəʔ</ta>
            <ta e="T112" id="Seg_5760" s="T111">šində</ta>
            <ta e="T114" id="Seg_5761" s="T113">kudaj-Tə</ta>
            <ta e="T116" id="Seg_5762" s="T115">kudo-l-bi</ta>
            <ta e="T117" id="Seg_5763" s="T116">i</ta>
            <ta e="T118" id="Seg_5764" s="T117">dĭbər</ta>
            <ta e="T119" id="Seg_5765" s="T118">sazən-Tə</ta>
            <ta e="T120" id="Seg_5766" s="T119">pʼaŋdə-bi</ta>
            <ta e="T121" id="Seg_5767" s="T120">što</ta>
            <ta e="T122" id="Seg_5768" s="T121">kudaj-Tə</ta>
            <ta e="T123" id="Seg_5769" s="T122">numan üzə-laʔbə-m</ta>
            <ta e="T124" id="Seg_5770" s="T123">kabarləj</ta>
            <ta e="T369" id="Seg_5771" s="T368">măn</ta>
            <ta e="T370" id="Seg_5772" s="T369">ertə-n</ta>
            <ta e="T371" id="Seg_5773" s="T370">uʔbdə-bi-m</ta>
            <ta e="T372" id="Seg_5774" s="T371">kudaj-Tə</ta>
            <ta e="T373" id="Seg_5775" s="T372">numan üzə-bi-m</ta>
            <ta e="T374" id="Seg_5776" s="T373">edəʔ-laʔbə-m</ta>
            <ta e="T376" id="Seg_5777" s="T375">Agafon</ta>
            <ta e="T377" id="Seg_5778" s="T376">Ivanovič-m</ta>
            <ta e="T378" id="Seg_5779" s="T377">dĭ</ta>
            <ta e="T379" id="Seg_5780" s="T378">šo-bi</ta>
            <ta e="T380" id="Seg_5781" s="T379">măna</ta>
            <ta e="T381" id="Seg_5782" s="T380">dĭgəttə</ta>
            <ta e="T382" id="Seg_5783" s="T381">măn</ta>
            <ta e="T383" id="Seg_5784" s="T382">dĭ-ziʔ</ta>
            <ta e="T384" id="Seg_5785" s="T383">kan-bi-m</ta>
            <ta e="T385" id="Seg_5786" s="T384">šo-bi-m</ta>
            <ta e="T386" id="Seg_5787" s="T385">dĭbər</ta>
            <ta e="T387" id="Seg_5788" s="T386">dĭ</ta>
            <ta e="T388" id="Seg_5789" s="T387">dĭ</ta>
            <ta e="T389" id="Seg_5790" s="T388">kănferentsia-Tə</ta>
            <ta e="T390" id="Seg_5791" s="T389">iʔgö</ta>
            <ta e="T392" id="Seg_5792" s="T391">il</ta>
            <ta e="T393" id="Seg_5793" s="T392">dĭn</ta>
            <ta e="T394" id="Seg_5794" s="T393">bar</ta>
            <ta e="T396" id="Seg_5795" s="T395">kuvas</ta>
            <ta e="T397" id="Seg_5796" s="T396">ugaːndə</ta>
            <ta e="T398" id="Seg_5797" s="T397">jakšə-jəʔ</ta>
            <ta e="T399" id="Seg_5798" s="T398">pʼaŋdə-zittə</ta>
            <ta e="T400" id="Seg_5799" s="T399">tăŋ</ta>
            <ta e="T401" id="Seg_5800" s="T400">tĭmne-jəʔ</ta>
            <ta e="T402" id="Seg_5801" s="T401">sagəštə</ta>
            <ta e="T403" id="Seg_5802" s="T402">iʔgö</ta>
            <ta e="T404" id="Seg_5803" s="T403">dĭgəttə</ta>
            <ta e="T405" id="Seg_5804" s="T404">bar</ta>
            <ta e="T406" id="Seg_5805" s="T405">măna</ta>
            <ta e="T407" id="Seg_5806" s="T406">panar-laʔbə-jəʔ</ta>
            <ta e="T408" id="Seg_5807" s="T407">ne-zAŋ</ta>
            <ta e="T409" id="Seg_5808" s="T408">a</ta>
            <ta e="T410" id="Seg_5809" s="T409">tibi-zAŋ</ta>
            <ta e="T412" id="Seg_5810" s="T411">uda-zAŋ-də</ta>
            <ta e="T413" id="Seg_5811" s="T412">nu-laʔbə-jəʔ</ta>
            <ta e="T414" id="Seg_5812" s="T413">dĭgəttə</ta>
            <ta e="T415" id="Seg_5813" s="T414">dĭ-gəʔ</ta>
            <ta e="T417" id="Seg_5814" s="T416">kan-bi-m</ta>
            <ta e="T418" id="Seg_5815" s="T417">mašina-Tə</ta>
            <ta e="T420" id="Seg_5816" s="T419">amnə-bi-m</ta>
            <ta e="T421" id="Seg_5817" s="T420">i-bi-m</ta>
            <ta e="T422" id="Seg_5818" s="T421">dĭn</ta>
            <ta e="T424" id="Seg_5819" s="T423">gijen</ta>
            <ta e="T425" id="Seg_5820" s="T424">sazən</ta>
            <ta e="T426" id="Seg_5821" s="T425">a-liA-jəʔ</ta>
            <ta e="T427" id="Seg_5822" s="T426">i</ta>
            <ta e="T428" id="Seg_5823" s="T427">knižka-jəʔ</ta>
            <ta e="T429" id="Seg_5824" s="T428">gijen</ta>
            <ta e="T430" id="Seg_5825" s="T429">hen-laʔbə-jəʔ</ta>
            <ta e="T431" id="Seg_5826" s="T430">dĭn</ta>
            <ta e="T432" id="Seg_5827" s="T431">măna</ta>
            <ta e="T433" id="Seg_5828" s="T432">tože</ta>
            <ta e="T435" id="Seg_5829" s="T434">kür-bi-jəʔ</ta>
            <ta e="T436" id="Seg_5830" s="T435">kartačka-Tə</ta>
            <ta e="T437" id="Seg_5831" s="T436">dĭn</ta>
            <ta e="T438" id="Seg_5832" s="T437">amno-bi-m</ta>
            <ta e="T439" id="Seg_5833" s="T438">tʼăbaktər-bi-m</ta>
            <ta e="T440" id="Seg_5834" s="T439">dĭn</ta>
            <ta e="T441" id="Seg_5835" s="T440">tože</ta>
            <ta e="T442" id="Seg_5836" s="T441">ugaːndə</ta>
            <ta e="T443" id="Seg_5837" s="T442">jakšə</ta>
            <ta e="T444" id="Seg_5838" s="T443">il</ta>
            <ta e="T445" id="Seg_5839" s="T444">dĭgəttə</ta>
            <ta e="T446" id="Seg_5840" s="T445">dĭ-zAŋ</ta>
            <ta e="T447" id="Seg_5841" s="T446">dĭ-zAŋ-Tə</ta>
            <ta e="T448" id="Seg_5842" s="T447">măn-bi-m</ta>
            <ta e="T449" id="Seg_5843" s="T448">kudaj</ta>
            <ta e="T450" id="Seg_5844" s="T449">kudaj</ta>
            <ta e="T451" id="Seg_5845" s="T450">šiʔ-ziʔ</ta>
            <ta e="T452" id="Seg_5846" s="T451">i</ta>
            <ta e="T453" id="Seg_5847" s="T452">bosəltə</ta>
            <ta e="T454" id="Seg_5848" s="T453">kan-bi-m</ta>
            <ta e="T455" id="Seg_5849" s="T454">dĭgəttə</ta>
            <ta e="T456" id="Seg_5850" s="T455">dĭn</ta>
            <ta e="T457" id="Seg_5851" s="T456">nʼuʔnun</ta>
            <ta e="T459" id="Seg_5852" s="T458">i-bi-m</ta>
            <ta e="T460" id="Seg_5853" s="T459">dĭgəttə</ta>
            <ta e="T461" id="Seg_5854" s="T460">nu-bi-bAʔ</ta>
            <ta e="T462" id="Seg_5855" s="T461">tʼo-Tə</ta>
            <ta e="T463" id="Seg_5856" s="T462">öʔlüʔ-bi-jəʔ</ta>
            <ta e="T464" id="Seg_5857" s="T463">dĭgəttə</ta>
            <ta e="T465" id="Seg_5858" s="T464">kan-bi-bAʔ</ta>
            <ta e="T466" id="Seg_5859" s="T465">gijen</ta>
            <ta e="T468" id="Seg_5860" s="T467">krospa-jəʔ</ta>
            <ta e="T469" id="Seg_5861" s="T468">dĭn</ta>
            <ta e="T470" id="Seg_5862" s="T469">tože</ta>
            <ta e="T471" id="Seg_5863" s="T470">mĭn-bi-bAʔ</ta>
            <ta e="T472" id="Seg_5864" s="T471">iʔgö</ta>
            <ta e="T473" id="Seg_5865" s="T472">ne-zAŋ</ta>
            <ta e="T474" id="Seg_5866" s="T473">i-bi-jəʔ</ta>
            <ta e="T475" id="Seg_5867" s="T474">i</ta>
            <ta e="T476" id="Seg_5868" s="T475">onʼiʔ</ta>
            <ta e="T477" id="Seg_5869" s="T476">šide</ta>
            <ta e="T478" id="Seg_5870" s="T477">tibi</ta>
            <ta e="T479" id="Seg_5871" s="T478">i-bi-jəʔ</ta>
            <ta e="T480" id="Seg_5872" s="T479">tože</ta>
            <ta e="T481" id="Seg_5873" s="T480">bar</ta>
            <ta e="T482" id="Seg_5874" s="T481">măna</ta>
            <ta e="T483" id="Seg_5875" s="T482">panar-bi-jəʔ</ta>
            <ta e="T484" id="Seg_5876" s="T483">uda-jəʔ</ta>
            <ta e="T485" id="Seg_5877" s="T484">mĭn-bi-jəʔ</ta>
            <ta e="T486" id="Seg_5878" s="T485">mĭ-bi-jəʔ</ta>
            <ta e="T487" id="Seg_5879" s="T486">măn-ntə-jəʔ</ta>
            <ta e="T488" id="Seg_5880" s="T487">jakšə</ta>
            <ta e="T489" id="Seg_5881" s="T488">i-gA-l</ta>
            <ta e="T490" id="Seg_5882" s="T489">ugaːndə</ta>
            <ta e="T491" id="Seg_5883" s="T490">kuŋgə</ta>
            <ta e="T492" id="Seg_5884" s="T491">šo-bi-l</ta>
            <ta e="T493" id="Seg_5885" s="T492">măn</ta>
            <ta e="T494" id="Seg_5886" s="T493">măn-bi-m</ta>
            <ta e="T495" id="Seg_5887" s="T494">kuŋgə</ta>
            <ta e="T496" id="Seg_5888" s="T495">šo-bi-m</ta>
            <ta e="T497" id="Seg_5889" s="T496">Măngolia-jəʔ</ta>
            <ta e="T498" id="Seg_5890" s="T497">toʔ-gəndə</ta>
            <ta e="T499" id="Seg_5891" s="T498">amno-bi-m</ta>
            <ta e="T500" id="Seg_5892" s="T499">kabarləj</ta>
            <ta e="T944" id="Seg_5893" s="T943">nu</ta>
            <ta e="T945" id="Seg_5894" s="T944">miʔ</ta>
            <ta e="T946" id="Seg_5895" s="T945">taldʼen</ta>
            <ta e="T947" id="Seg_5896" s="T946">Vida-ziʔ</ta>
            <ta e="T948" id="Seg_5897" s="T947">edəʔ-laʔbə-bAʔ</ta>
            <ta e="T949" id="Seg_5898" s="T948">üge</ta>
            <ta e="T950" id="Seg_5899" s="T949">kaga-m</ta>
            <ta e="T951" id="Seg_5900" s="T950">edəʔ-laʔbə</ta>
            <ta e="T953" id="Seg_5901" s="T952">naga</ta>
            <ta e="T954" id="Seg_5902" s="T953">naga</ta>
            <ta e="T956" id="Seg_5903" s="T954">dĭgəttə</ta>
            <ta e="T957" id="Seg_5904" s="T956">baza-j</ta>
            <ta e="T958" id="Seg_5905" s="T957">aʔtʼi</ta>
            <ta e="T959" id="Seg_5906" s="T958">kuzur-laʔbə</ta>
            <ta e="T960" id="Seg_5907" s="T959">măn</ta>
            <ta e="T961" id="Seg_5908" s="T960">măn-ntə-m</ta>
            <ta e="T962" id="Seg_5909" s="T961">šonə-gA</ta>
            <ta e="T963" id="Seg_5910" s="T962">nu</ta>
            <ta e="T965" id="Seg_5911" s="T964">dĭ</ta>
            <ta e="T966" id="Seg_5912" s="T965">šo-bi</ta>
            <ta e="T967" id="Seg_5913" s="T966">možet</ta>
            <ta e="T968" id="Seg_5914" s="T967">ija-gəndə</ta>
            <ta e="T969" id="Seg_5915" s="T968">kan-bi</ta>
            <ta e="T970" id="Seg_5916" s="T969">dĭn</ta>
            <ta e="T971" id="Seg_5917" s="T970">am-bi</ta>
            <ta e="T972" id="Seg_5918" s="T971">da</ta>
            <ta e="T974" id="Seg_5919" s="T973">iʔbə-bi</ta>
            <ta e="T975" id="Seg_5920" s="T974">šaʔ-lV-jəʔ</ta>
            <ta e="T976" id="Seg_5921" s="T975">kunol-zittə</ta>
            <ta e="T977" id="Seg_5922" s="T976">dĭgəttə</ta>
            <ta e="T978" id="Seg_5923" s="T977">amnə-bi-bAʔ</ta>
            <ta e="T979" id="Seg_5924" s="T978">i</ta>
            <ta e="T980" id="Seg_5925" s="T979">det-bi-bAʔ</ta>
            <ta e="T981" id="Seg_5926" s="T980">det-bi-bAʔ</ta>
            <ta e="T984" id="Seg_5927" s="T983">iʔbə-bAʔ</ta>
            <ta e="T985" id="Seg_5928" s="T984">kunol-zittə</ta>
            <ta e="T986" id="Seg_5929" s="T985">tolʼko</ta>
            <ta e="T987" id="Seg_5930" s="T986">iʔbə-bAʔ</ta>
            <ta e="T988" id="Seg_5931" s="T987">iʔbə-bi-bAʔ</ta>
            <ta e="T989" id="Seg_5932" s="T988">măn</ta>
            <ta e="T990" id="Seg_5933" s="T989">aʔpi</ta>
            <ta e="T991" id="Seg_5934" s="T990">i</ta>
            <ta e="T992" id="Seg_5935" s="T991">kunol-bi-m</ta>
            <ta e="T993" id="Seg_5936" s="T992">nu</ta>
            <ta e="T995" id="Seg_5937" s="T994">šonə-gA</ta>
            <ta e="T996" id="Seg_5938" s="T995">šində=də</ta>
            <ta e="T997" id="Seg_5939" s="T996">šo-laːm-bi</ta>
            <ta e="T999" id="Seg_5940" s="T998">măn</ta>
            <ta e="T1000" id="Seg_5941" s="T999">süʔmə-luʔbdə-bi-m</ta>
            <ta e="T1001" id="Seg_5942" s="T1000">da</ta>
            <ta e="T1002" id="Seg_5943" s="T1001">dĭbər</ta>
            <ta e="T1003" id="Seg_5944" s="T1002">nuʔmə-luʔbdə-bi-m</ta>
            <ta e="T1004" id="Seg_5945" s="T1003">dĭ-Tə</ta>
            <ta e="T1005" id="Seg_5946" s="T1004">uda-m</ta>
            <ta e="T1006" id="Seg_5947" s="T1005">mĭ-bi-m</ta>
            <ta e="T1007" id="Seg_5948" s="T1006">tʼăbaktər-bi-bAʔ</ta>
            <ta e="T1008" id="Seg_5949" s="T1007">dĭ</ta>
            <ta e="T1009" id="Seg_5950" s="T1008">măn-ntə</ta>
            <ta e="T1010" id="Seg_5951" s="T1009">măn</ta>
            <ta e="T1011" id="Seg_5952" s="T1010">bazə-jd-lV-m</ta>
            <ta e="T1012" id="Seg_5953" s="T1011">vanna-Tə</ta>
            <ta e="T1013" id="Seg_5954" s="T1012">kan-lV-m</ta>
            <ta e="T1014" id="Seg_5955" s="T1013">no</ta>
            <ta e="T1015" id="Seg_5956" s="T1014">kan-ə-ʔ</ta>
            <ta e="T1016" id="Seg_5957" s="T1015">dĭgəttə</ta>
            <ta e="T1017" id="Seg_5958" s="T1016">dĭ</ta>
            <ta e="T1018" id="Seg_5959" s="T1017">kan-bi</ta>
            <ta e="T1019" id="Seg_5960" s="T1018">măn</ta>
            <ta e="T1020" id="Seg_5961" s="T1019">măn-bi-m</ta>
            <ta e="T1021" id="Seg_5962" s="T1020">amor-ə-ʔ</ta>
            <ta e="T1022" id="Seg_5963" s="T1021">dĭgəttə</ta>
            <ta e="T1023" id="Seg_5964" s="T1022">kan-lV-l</ta>
            <ta e="T1024" id="Seg_5965" s="T1023">dĭgəttə</ta>
            <ta e="T1025" id="Seg_5966" s="T1024">dĭ</ta>
            <ta e="T1026" id="Seg_5967" s="T1025">ejüm-lə-bi</ta>
            <ta e="T1027" id="Seg_5968" s="T1026">mĭje</ta>
            <ta e="T1028" id="Seg_5969" s="T1027">segi</ta>
            <ta e="T1029" id="Seg_5970" s="T1028">bü</ta>
            <ta e="T1031" id="Seg_5971" s="T1030">dĭ</ta>
            <ta e="T1032" id="Seg_5972" s="T1031">am-bi</ta>
            <ta e="T1033" id="Seg_5973" s="T1032">dĭgəttə</ta>
            <ta e="T1035" id="Seg_5974" s="T1034">kan-bi</ta>
            <ta e="T1036" id="Seg_5975" s="T1035">bazə-jd-zittə</ta>
            <ta e="T1037" id="Seg_5976" s="T1036">vanna-Tə</ta>
            <ta e="T1038" id="Seg_5977" s="T1037">dĭgəttə</ta>
            <ta e="T1039" id="Seg_5978" s="T1038">dĭ</ta>
            <ta e="T1040" id="Seg_5979" s="T1039">dĭ-Tə</ta>
            <ta e="T1041" id="Seg_5980" s="T1040">iʔbə-zittə</ta>
            <ta e="T1042" id="Seg_5981" s="T1041">dʼajir-bi</ta>
            <ta e="T1043" id="Seg_5982" s="T1042">dön</ta>
            <ta e="T1044" id="Seg_5983" s="T1043">nuldə-bi</ta>
            <ta e="T1046" id="Seg_5984" s="T1045">dĭ</ta>
            <ta e="T1047" id="Seg_5985" s="T1046">iʔbö-laʔbə-bi</ta>
            <ta e="T1048" id="Seg_5986" s="T1047">i</ta>
            <ta e="T1049" id="Seg_5987" s="T1048">kunol-lV-bi-bAʔ</ta>
            <ta e="T1050" id="Seg_5988" s="T1049">ertə-n</ta>
            <ta e="T1051" id="Seg_5989" s="T1050">bazoʔ</ta>
            <ta e="T1054" id="Seg_5990" s="T1052">uʔbdə-bi-bAʔ</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PKZ">
            <ta e="T85" id="Seg_5991" s="T84">and</ta>
            <ta e="T86" id="Seg_5992" s="T85">we.LAT</ta>
            <ta e="T87" id="Seg_5993" s="T86">NEG</ta>
            <ta e="T88" id="Seg_5994" s="T87">give-PST-3PL</ta>
            <ta e="T89" id="Seg_5995" s="T88">God-LAT</ta>
            <ta e="T90" id="Seg_5996" s="T89">pray-INF.LAT</ta>
            <ta e="T91" id="Seg_5997" s="T90">then</ta>
            <ta e="T92" id="Seg_5998" s="T91">write-PST.[3SG]</ta>
            <ta e="T94" id="Seg_5999" s="T93">paper.[NOM.SG]</ta>
            <ta e="T99" id="Seg_6000" s="T98">give-%%-CVB</ta>
            <ta e="T100" id="Seg_6001" s="T99">I.LAT</ta>
            <ta e="T101" id="Seg_6002" s="T100">this</ta>
            <ta e="T102" id="Seg_6003" s="T101">people.[NOM.SG]</ta>
            <ta e="T103" id="Seg_6004" s="T102">what.kind</ta>
            <ta e="T104" id="Seg_6005" s="T103">people.[NOM.SG]</ta>
            <ta e="T105" id="Seg_6006" s="T104">God-LAT</ta>
            <ta e="T107" id="Seg_6007" s="T106">pray-PRS-3PL</ta>
            <ta e="T108" id="Seg_6008" s="T107">then</ta>
            <ta e="T109" id="Seg_6009" s="T108">paper.[NOM.SG]</ta>
            <ta e="T110" id="Seg_6010" s="T109">come-PST.[3SG]</ta>
            <ta e="T111" id="Seg_6011" s="T110">write-PST-3PL</ta>
            <ta e="T112" id="Seg_6012" s="T111">who.[NOM.SG]</ta>
            <ta e="T114" id="Seg_6013" s="T113">God-LAT</ta>
            <ta e="T116" id="Seg_6014" s="T115">scold-FRQ-PST.[3SG]</ta>
            <ta e="T117" id="Seg_6015" s="T116">and</ta>
            <ta e="T118" id="Seg_6016" s="T117">there</ta>
            <ta e="T119" id="Seg_6017" s="T118">paper-LAT</ta>
            <ta e="T120" id="Seg_6018" s="T119">write-PST.[3SG]</ta>
            <ta e="T121" id="Seg_6019" s="T120">that</ta>
            <ta e="T122" id="Seg_6020" s="T121">God-LAT</ta>
            <ta e="T123" id="Seg_6021" s="T122">pray-DUR-1SG</ta>
            <ta e="T124" id="Seg_6022" s="T123">enough</ta>
            <ta e="T369" id="Seg_6023" s="T368">I.NOM</ta>
            <ta e="T370" id="Seg_6024" s="T369">morning-LOC.ADV</ta>
            <ta e="T371" id="Seg_6025" s="T370">get.up-PST-1SG</ta>
            <ta e="T372" id="Seg_6026" s="T371">God-LAT</ta>
            <ta e="T373" id="Seg_6027" s="T372">pray-PST-1SG</ta>
            <ta e="T374" id="Seg_6028" s="T373">wait-DUR-1SG</ta>
            <ta e="T376" id="Seg_6029" s="T375">Agafon</ta>
            <ta e="T377" id="Seg_6030" s="T376">Ivanovich-ACC</ta>
            <ta e="T378" id="Seg_6031" s="T377">this.[NOM.SG]</ta>
            <ta e="T379" id="Seg_6032" s="T378">come-PST.[3SG]</ta>
            <ta e="T380" id="Seg_6033" s="T379">I.LAT</ta>
            <ta e="T381" id="Seg_6034" s="T380">then</ta>
            <ta e="T382" id="Seg_6035" s="T381">I.NOM</ta>
            <ta e="T383" id="Seg_6036" s="T382">this-COM</ta>
            <ta e="T384" id="Seg_6037" s="T383">go-PST-1SG</ta>
            <ta e="T385" id="Seg_6038" s="T384">come-PST-1SG</ta>
            <ta e="T386" id="Seg_6039" s="T385">there</ta>
            <ta e="T387" id="Seg_6040" s="T386">this</ta>
            <ta e="T388" id="Seg_6041" s="T387">this</ta>
            <ta e="T389" id="Seg_6042" s="T388">conference-LAT</ta>
            <ta e="T390" id="Seg_6043" s="T389">many</ta>
            <ta e="T392" id="Seg_6044" s="T391">people.[NOM.SG]</ta>
            <ta e="T393" id="Seg_6045" s="T392">there</ta>
            <ta e="T394" id="Seg_6046" s="T393">PTCL</ta>
            <ta e="T396" id="Seg_6047" s="T395">beautiful.[NOM.SG]</ta>
            <ta e="T397" id="Seg_6048" s="T396">very</ta>
            <ta e="T398" id="Seg_6049" s="T397">good-PL</ta>
            <ta e="T399" id="Seg_6050" s="T398">write-INF.LAT</ta>
            <ta e="T400" id="Seg_6051" s="T399">strongly</ta>
            <ta e="T401" id="Seg_6052" s="T400">know-3PL</ta>
            <ta e="T402" id="Seg_6053" s="T401">silly.[NOM.SG]</ta>
            <ta e="T403" id="Seg_6054" s="T402">many</ta>
            <ta e="T404" id="Seg_6055" s="T403">then</ta>
            <ta e="T405" id="Seg_6056" s="T404">PTCL</ta>
            <ta e="T406" id="Seg_6057" s="T405">I.ACC</ta>
            <ta e="T407" id="Seg_6058" s="T406">kiss-DUR-3PL</ta>
            <ta e="T408" id="Seg_6059" s="T407">woman-PL</ta>
            <ta e="T409" id="Seg_6060" s="T408">and</ta>
            <ta e="T410" id="Seg_6061" s="T409">man-PL</ta>
            <ta e="T412" id="Seg_6062" s="T411">hand-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T413" id="Seg_6063" s="T412">stand-DUR-3PL</ta>
            <ta e="T414" id="Seg_6064" s="T413">then</ta>
            <ta e="T415" id="Seg_6065" s="T414">this-ABL</ta>
            <ta e="T417" id="Seg_6066" s="T416">go-PST-1SG</ta>
            <ta e="T418" id="Seg_6067" s="T417">machine-LAT</ta>
            <ta e="T420" id="Seg_6068" s="T419">sit.down-PST-1SG</ta>
            <ta e="T421" id="Seg_6069" s="T420">be-PST-1SG</ta>
            <ta e="T422" id="Seg_6070" s="T421">there</ta>
            <ta e="T424" id="Seg_6071" s="T423">where</ta>
            <ta e="T425" id="Seg_6072" s="T424">paper.[NOM.SG]</ta>
            <ta e="T426" id="Seg_6073" s="T425">make-PRS-3PL</ta>
            <ta e="T427" id="Seg_6074" s="T426">and</ta>
            <ta e="T428" id="Seg_6075" s="T427">book-NOM/GEN/ACC.3PL</ta>
            <ta e="T429" id="Seg_6076" s="T428">where</ta>
            <ta e="T430" id="Seg_6077" s="T429">put-DUR-3PL</ta>
            <ta e="T431" id="Seg_6078" s="T430">there</ta>
            <ta e="T432" id="Seg_6079" s="T431">I.ACC</ta>
            <ta e="T433" id="Seg_6080" s="T432">also</ta>
            <ta e="T435" id="Seg_6081" s="T434">take.picture-PST-3PL</ta>
            <ta e="T436" id="Seg_6082" s="T435">photograph-LAT</ta>
            <ta e="T437" id="Seg_6083" s="T436">there</ta>
            <ta e="T438" id="Seg_6084" s="T437">sit-PST-1SG</ta>
            <ta e="T439" id="Seg_6085" s="T438">speak-PST-1SG</ta>
            <ta e="T440" id="Seg_6086" s="T439">there</ta>
            <ta e="T441" id="Seg_6087" s="T440">also</ta>
            <ta e="T442" id="Seg_6088" s="T441">very</ta>
            <ta e="T443" id="Seg_6089" s="T442">good</ta>
            <ta e="T444" id="Seg_6090" s="T443">people.[NOM.SG]</ta>
            <ta e="T445" id="Seg_6091" s="T444">then</ta>
            <ta e="T446" id="Seg_6092" s="T445">this-PL</ta>
            <ta e="T447" id="Seg_6093" s="T446">this-PL-LAT</ta>
            <ta e="T448" id="Seg_6094" s="T447">say-PST-1SG</ta>
            <ta e="T449" id="Seg_6095" s="T448">God.[NOM.SG]</ta>
            <ta e="T450" id="Seg_6096" s="T449">God.[NOM.SG]</ta>
            <ta e="T451" id="Seg_6097" s="T450">you.PL-COM</ta>
            <ta e="T452" id="Seg_6098" s="T451">and</ta>
            <ta e="T453" id="Seg_6099" s="T452">%%</ta>
            <ta e="T454" id="Seg_6100" s="T453">go-PST-1SG</ta>
            <ta e="T455" id="Seg_6101" s="T454">then</ta>
            <ta e="T456" id="Seg_6102" s="T455">there</ta>
            <ta e="T457" id="Seg_6103" s="T456">%above</ta>
            <ta e="T459" id="Seg_6104" s="T458">be-PST-1SG</ta>
            <ta e="T460" id="Seg_6105" s="T459">then</ta>
            <ta e="T461" id="Seg_6106" s="T460">stand-PST-1PL</ta>
            <ta e="T462" id="Seg_6107" s="T461">earth-LAT</ta>
            <ta e="T463" id="Seg_6108" s="T462">%%-PST-3PL</ta>
            <ta e="T464" id="Seg_6109" s="T463">then</ta>
            <ta e="T465" id="Seg_6110" s="T464">go-PST-1PL</ta>
            <ta e="T466" id="Seg_6111" s="T465">where</ta>
            <ta e="T468" id="Seg_6112" s="T467">cross-PL</ta>
            <ta e="T469" id="Seg_6113" s="T468">there</ta>
            <ta e="T470" id="Seg_6114" s="T469">also</ta>
            <ta e="T471" id="Seg_6115" s="T470">go-PST-1PL</ta>
            <ta e="T472" id="Seg_6116" s="T471">many</ta>
            <ta e="T473" id="Seg_6117" s="T472">woman-PL</ta>
            <ta e="T474" id="Seg_6118" s="T473">be-PST-3PL</ta>
            <ta e="T475" id="Seg_6119" s="T474">and</ta>
            <ta e="T476" id="Seg_6120" s="T475">one.[NOM.SG]</ta>
            <ta e="T477" id="Seg_6121" s="T476">two.[NOM.SG]</ta>
            <ta e="T478" id="Seg_6122" s="T477">man.[NOM.SG]</ta>
            <ta e="T479" id="Seg_6123" s="T478">be-PST-3PL</ta>
            <ta e="T480" id="Seg_6124" s="T479">also</ta>
            <ta e="T481" id="Seg_6125" s="T480">PTCL</ta>
            <ta e="T482" id="Seg_6126" s="T481">I.ACC</ta>
            <ta e="T483" id="Seg_6127" s="T482">kiss-PST-3PL</ta>
            <ta e="T484" id="Seg_6128" s="T483">hand-PL</ta>
            <ta e="T485" id="Seg_6129" s="T484">go-PST-3PL</ta>
            <ta e="T486" id="Seg_6130" s="T485">give-PST-3PL</ta>
            <ta e="T487" id="Seg_6131" s="T486">say-IPFVZ-3PL</ta>
            <ta e="T488" id="Seg_6132" s="T487">good</ta>
            <ta e="T489" id="Seg_6133" s="T488">be-PRS-2SG</ta>
            <ta e="T490" id="Seg_6134" s="T489">very</ta>
            <ta e="T491" id="Seg_6135" s="T490">far</ta>
            <ta e="T492" id="Seg_6136" s="T491">come-PST-2SG</ta>
            <ta e="T493" id="Seg_6137" s="T492">I.NOM</ta>
            <ta e="T494" id="Seg_6138" s="T493">say-PST-1SG</ta>
            <ta e="T495" id="Seg_6139" s="T494">far</ta>
            <ta e="T496" id="Seg_6140" s="T495">come-PST-1SG</ta>
            <ta e="T497" id="Seg_6141" s="T496">Mongolia-PL</ta>
            <ta e="T498" id="Seg_6142" s="T497">edge-LAT/LOC.3SG</ta>
            <ta e="T499" id="Seg_6143" s="T498">live-PST-1SG</ta>
            <ta e="T500" id="Seg_6144" s="T499">enough</ta>
            <ta e="T944" id="Seg_6145" s="T943">well</ta>
            <ta e="T945" id="Seg_6146" s="T944">we.NOM</ta>
            <ta e="T946" id="Seg_6147" s="T945">yesterday</ta>
            <ta e="T947" id="Seg_6148" s="T946">Frida-COM</ta>
            <ta e="T948" id="Seg_6149" s="T947">wait-DUR-1PL</ta>
            <ta e="T949" id="Seg_6150" s="T948">always</ta>
            <ta e="T950" id="Seg_6151" s="T949">brother-NOM/GEN/ACC.1SG</ta>
            <ta e="T951" id="Seg_6152" s="T950">wait-DUR.[3SG]</ta>
            <ta e="T953" id="Seg_6153" s="T952">NEG.EX</ta>
            <ta e="T954" id="Seg_6154" s="T953">NEG.EX</ta>
            <ta e="T956" id="Seg_6155" s="T954">then</ta>
            <ta e="T957" id="Seg_6156" s="T956">iron-ADJZ.[NOM.SG]</ta>
            <ta e="T958" id="Seg_6157" s="T957">road.[NOM.SG]</ta>
            <ta e="T959" id="Seg_6158" s="T958">rumble-DUR.[3SG]</ta>
            <ta e="T960" id="Seg_6159" s="T959">I.NOM</ta>
            <ta e="T961" id="Seg_6160" s="T960">say-IPFVZ-1SG</ta>
            <ta e="T962" id="Seg_6161" s="T961">come-PRS.[3SG]</ta>
            <ta e="T963" id="Seg_6162" s="T962">well</ta>
            <ta e="T965" id="Seg_6163" s="T964">this</ta>
            <ta e="T966" id="Seg_6164" s="T965">come-PST.[3SG]</ta>
            <ta e="T967" id="Seg_6165" s="T966">maybe</ta>
            <ta e="T968" id="Seg_6166" s="T967">mother-LAT/LOC.3SG</ta>
            <ta e="T969" id="Seg_6167" s="T968">go-PST.[3SG]</ta>
            <ta e="T970" id="Seg_6168" s="T969">there</ta>
            <ta e="T971" id="Seg_6169" s="T970">eat-PST.[3SG]</ta>
            <ta e="T972" id="Seg_6170" s="T971">and</ta>
            <ta e="T974" id="Seg_6171" s="T973">lie.down-PST.[3SG]</ta>
            <ta e="T975" id="Seg_6172" s="T974">spend.night-FUT-3PL</ta>
            <ta e="T976" id="Seg_6173" s="T975">sleep-INF.LAT</ta>
            <ta e="T977" id="Seg_6174" s="T976">then</ta>
            <ta e="T978" id="Seg_6175" s="T977">sit.down-PST-1PL</ta>
            <ta e="T979" id="Seg_6176" s="T978">and</ta>
            <ta e="T980" id="Seg_6177" s="T979">bring-PST-1PL</ta>
            <ta e="T981" id="Seg_6178" s="T980">bring-PST-1PL</ta>
            <ta e="T984" id="Seg_6179" s="T983">lie.down-1PL</ta>
            <ta e="T985" id="Seg_6180" s="T984">sleep-INF.LAT</ta>
            <ta e="T986" id="Seg_6181" s="T985">only</ta>
            <ta e="T987" id="Seg_6182" s="T986">lie.down-1PL</ta>
            <ta e="T988" id="Seg_6183" s="T987">lie.down-PST-1PL</ta>
            <ta e="T989" id="Seg_6184" s="T988">I.NOM</ta>
            <ta e="T990" id="Seg_6185" s="T989">%nearly</ta>
            <ta e="T991" id="Seg_6186" s="T990">and</ta>
            <ta e="T992" id="Seg_6187" s="T991">sleep-PST-1SG</ta>
            <ta e="T993" id="Seg_6188" s="T992">well</ta>
            <ta e="T995" id="Seg_6189" s="T994">come-PRS.[3SG]</ta>
            <ta e="T996" id="Seg_6190" s="T995">who.[NOM.SG]=INDEF</ta>
            <ta e="T997" id="Seg_6191" s="T996">come-RES-PST.[3SG]</ta>
            <ta e="T999" id="Seg_6192" s="T998">I.NOM</ta>
            <ta e="T1000" id="Seg_6193" s="T999">jump-MOM-PST-1SG</ta>
            <ta e="T1001" id="Seg_6194" s="T1000">and</ta>
            <ta e="T1002" id="Seg_6195" s="T1001">there</ta>
            <ta e="T1003" id="Seg_6196" s="T1002">run-MOM-PST-1SG</ta>
            <ta e="T1004" id="Seg_6197" s="T1003">this-LAT</ta>
            <ta e="T1005" id="Seg_6198" s="T1004">hand-NOM/GEN/ACC.1SG</ta>
            <ta e="T1006" id="Seg_6199" s="T1005">give-PST-1SG</ta>
            <ta e="T1007" id="Seg_6200" s="T1006">speak-PST-1PL</ta>
            <ta e="T1008" id="Seg_6201" s="T1007">this</ta>
            <ta e="T1009" id="Seg_6202" s="T1008">say-IPFVZ.[3SG]</ta>
            <ta e="T1010" id="Seg_6203" s="T1009">I.NOM</ta>
            <ta e="T1011" id="Seg_6204" s="T1010">wash-DRV-FUT-1SG</ta>
            <ta e="T1012" id="Seg_6205" s="T1011">bathroom-LAT</ta>
            <ta e="T1013" id="Seg_6206" s="T1012">go-FUT-1SG</ta>
            <ta e="T1014" id="Seg_6207" s="T1013">well</ta>
            <ta e="T1015" id="Seg_6208" s="T1014">go-EP-IMP.2SG</ta>
            <ta e="T1016" id="Seg_6209" s="T1015">then</ta>
            <ta e="T1017" id="Seg_6210" s="T1016">this</ta>
            <ta e="T1018" id="Seg_6211" s="T1017">go-PST.[3SG]</ta>
            <ta e="T1019" id="Seg_6212" s="T1018">I.NOM</ta>
            <ta e="T1020" id="Seg_6213" s="T1019">say-PST-1SG</ta>
            <ta e="T1021" id="Seg_6214" s="T1020">eat-EP-IMP.2SG</ta>
            <ta e="T1022" id="Seg_6215" s="T1021">then</ta>
            <ta e="T1023" id="Seg_6216" s="T1022">go-FUT-2SG</ta>
            <ta e="T1024" id="Seg_6217" s="T1023">then</ta>
            <ta e="T1025" id="Seg_6218" s="T1024">this</ta>
            <ta e="T1026" id="Seg_6219" s="T1025">become.warm-TR-PST.[3SG]</ta>
            <ta e="T1027" id="Seg_6220" s="T1026">soup.[NOM.SG]</ta>
            <ta e="T1028" id="Seg_6221" s="T1027">yellow.[NOM.SG]</ta>
            <ta e="T1029" id="Seg_6222" s="T1028">water.[NOM.SG]</ta>
            <ta e="T1031" id="Seg_6223" s="T1030">this</ta>
            <ta e="T1032" id="Seg_6224" s="T1031">eat-PST.[3SG]</ta>
            <ta e="T1033" id="Seg_6225" s="T1032">then</ta>
            <ta e="T1035" id="Seg_6226" s="T1034">go-PST.[3SG]</ta>
            <ta e="T1036" id="Seg_6227" s="T1035">wash-DRV-INF.LAT</ta>
            <ta e="T1037" id="Seg_6228" s="T1036">bathroom-LAT</ta>
            <ta e="T1038" id="Seg_6229" s="T1037">then</ta>
            <ta e="T1039" id="Seg_6230" s="T1038">this</ta>
            <ta e="T1040" id="Seg_6231" s="T1039">this-LAT</ta>
            <ta e="T1041" id="Seg_6232" s="T1040">lie.down-INF.LAT</ta>
            <ta e="T1042" id="Seg_6233" s="T1041">%%-PST.[3SG]</ta>
            <ta e="T1043" id="Seg_6234" s="T1042">here</ta>
            <ta e="T1044" id="Seg_6235" s="T1043">place-PST.[3SG]</ta>
            <ta e="T1046" id="Seg_6236" s="T1045">this</ta>
            <ta e="T1047" id="Seg_6237" s="T1046">lie-DUR-PST.[3SG]</ta>
            <ta e="T1048" id="Seg_6238" s="T1047">and</ta>
            <ta e="T1049" id="Seg_6239" s="T1048">sleep-FUT-PST-1PL</ta>
            <ta e="T1050" id="Seg_6240" s="T1049">morning-LOC.ADV</ta>
            <ta e="T1051" id="Seg_6241" s="T1050">again</ta>
            <ta e="T1054" id="Seg_6242" s="T1052">get.up-PST-1PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PKZ">
            <ta e="T85" id="Seg_6243" s="T84">а</ta>
            <ta e="T86" id="Seg_6244" s="T85">мы.LAT</ta>
            <ta e="T87" id="Seg_6245" s="T86">NEG</ta>
            <ta e="T88" id="Seg_6246" s="T87">дать-PST-3PL</ta>
            <ta e="T89" id="Seg_6247" s="T88">Бог-LAT</ta>
            <ta e="T90" id="Seg_6248" s="T89">молиться-INF.LAT</ta>
            <ta e="T91" id="Seg_6249" s="T90">тогда</ta>
            <ta e="T92" id="Seg_6250" s="T91">писать-PST.[3SG]</ta>
            <ta e="T94" id="Seg_6251" s="T93">бумага.[NOM.SG]</ta>
            <ta e="T99" id="Seg_6252" s="T98">дать-%%-CVB</ta>
            <ta e="T100" id="Seg_6253" s="T99">я.LAT</ta>
            <ta e="T101" id="Seg_6254" s="T100">этот</ta>
            <ta e="T102" id="Seg_6255" s="T101">люди.[NOM.SG]</ta>
            <ta e="T103" id="Seg_6256" s="T102">какой</ta>
            <ta e="T104" id="Seg_6257" s="T103">люди.[NOM.SG]</ta>
            <ta e="T105" id="Seg_6258" s="T104">Бог-LAT</ta>
            <ta e="T107" id="Seg_6259" s="T106">молиться-PRS-3PL</ta>
            <ta e="T108" id="Seg_6260" s="T107">тогда</ta>
            <ta e="T109" id="Seg_6261" s="T108">бумага.[NOM.SG]</ta>
            <ta e="T110" id="Seg_6262" s="T109">прийти-PST.[3SG]</ta>
            <ta e="T111" id="Seg_6263" s="T110">писать-PST-3PL</ta>
            <ta e="T112" id="Seg_6264" s="T111">кто.[NOM.SG]</ta>
            <ta e="T114" id="Seg_6265" s="T113">Бог-LAT</ta>
            <ta e="T116" id="Seg_6266" s="T115">ругать-FRQ-PST.[3SG]</ta>
            <ta e="T117" id="Seg_6267" s="T116">и</ta>
            <ta e="T118" id="Seg_6268" s="T117">там</ta>
            <ta e="T119" id="Seg_6269" s="T118">бумага-LAT</ta>
            <ta e="T120" id="Seg_6270" s="T119">писать-PST.[3SG]</ta>
            <ta e="T121" id="Seg_6271" s="T120">что</ta>
            <ta e="T122" id="Seg_6272" s="T121">Бог-LAT</ta>
            <ta e="T123" id="Seg_6273" s="T122">молиться-DUR-1SG</ta>
            <ta e="T124" id="Seg_6274" s="T123">хватит</ta>
            <ta e="T369" id="Seg_6275" s="T368">я.NOM</ta>
            <ta e="T370" id="Seg_6276" s="T369">утро-LOC.ADV</ta>
            <ta e="T371" id="Seg_6277" s="T370">встать-PST-1SG</ta>
            <ta e="T372" id="Seg_6278" s="T371">Бог-LAT</ta>
            <ta e="T373" id="Seg_6279" s="T372">молиться-PST-1SG</ta>
            <ta e="T374" id="Seg_6280" s="T373">ждать-DUR-1SG</ta>
            <ta e="T376" id="Seg_6281" s="T375">Агафон</ta>
            <ta e="T377" id="Seg_6282" s="T376">Иванович-ACC</ta>
            <ta e="T378" id="Seg_6283" s="T377">этот.[NOM.SG]</ta>
            <ta e="T379" id="Seg_6284" s="T378">прийти-PST.[3SG]</ta>
            <ta e="T380" id="Seg_6285" s="T379">я.LAT</ta>
            <ta e="T381" id="Seg_6286" s="T380">тогда</ta>
            <ta e="T382" id="Seg_6287" s="T381">я.NOM</ta>
            <ta e="T383" id="Seg_6288" s="T382">этот-COM</ta>
            <ta e="T384" id="Seg_6289" s="T383">пойти-PST-1SG</ta>
            <ta e="T385" id="Seg_6290" s="T384">прийти-PST-1SG</ta>
            <ta e="T386" id="Seg_6291" s="T385">там</ta>
            <ta e="T387" id="Seg_6292" s="T386">этот</ta>
            <ta e="T388" id="Seg_6293" s="T387">этот</ta>
            <ta e="T389" id="Seg_6294" s="T388">конференция-LAT</ta>
            <ta e="T390" id="Seg_6295" s="T389">много</ta>
            <ta e="T392" id="Seg_6296" s="T391">люди.[NOM.SG]</ta>
            <ta e="T393" id="Seg_6297" s="T392">там</ta>
            <ta e="T394" id="Seg_6298" s="T393">PTCL</ta>
            <ta e="T396" id="Seg_6299" s="T395">красивый.[NOM.SG]</ta>
            <ta e="T397" id="Seg_6300" s="T396">очень</ta>
            <ta e="T398" id="Seg_6301" s="T397">хороший-PL</ta>
            <ta e="T399" id="Seg_6302" s="T398">писать-INF.LAT</ta>
            <ta e="T400" id="Seg_6303" s="T399">сильно</ta>
            <ta e="T401" id="Seg_6304" s="T400">знать-3PL</ta>
            <ta e="T402" id="Seg_6305" s="T401">глупый.[NOM.SG]</ta>
            <ta e="T403" id="Seg_6306" s="T402">много</ta>
            <ta e="T404" id="Seg_6307" s="T403">тогда</ta>
            <ta e="T405" id="Seg_6308" s="T404">PTCL</ta>
            <ta e="T406" id="Seg_6309" s="T405">я.ACC</ta>
            <ta e="T407" id="Seg_6310" s="T406">целовать-DUR-3PL</ta>
            <ta e="T408" id="Seg_6311" s="T407">женщина-PL</ta>
            <ta e="T409" id="Seg_6312" s="T408">а</ta>
            <ta e="T410" id="Seg_6313" s="T409">мужчина-PL</ta>
            <ta e="T412" id="Seg_6314" s="T411">рука-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T413" id="Seg_6315" s="T412">стоять-DUR-3PL</ta>
            <ta e="T414" id="Seg_6316" s="T413">тогда</ta>
            <ta e="T415" id="Seg_6317" s="T414">этот-ABL</ta>
            <ta e="T417" id="Seg_6318" s="T416">пойти-PST-1SG</ta>
            <ta e="T418" id="Seg_6319" s="T417">машина-LAT</ta>
            <ta e="T420" id="Seg_6320" s="T419">сесть-PST-1SG</ta>
            <ta e="T421" id="Seg_6321" s="T420">быть-PST-1SG</ta>
            <ta e="T422" id="Seg_6322" s="T421">там</ta>
            <ta e="T424" id="Seg_6323" s="T423">где</ta>
            <ta e="T425" id="Seg_6324" s="T424">бумага.[NOM.SG]</ta>
            <ta e="T426" id="Seg_6325" s="T425">делать-PRS-3PL</ta>
            <ta e="T427" id="Seg_6326" s="T426">и</ta>
            <ta e="T428" id="Seg_6327" s="T427">книжка-NOM/GEN/ACC.3PL</ta>
            <ta e="T429" id="Seg_6328" s="T428">где</ta>
            <ta e="T430" id="Seg_6329" s="T429">класть-DUR-3PL</ta>
            <ta e="T431" id="Seg_6330" s="T430">там</ta>
            <ta e="T432" id="Seg_6331" s="T431">я.ACC</ta>
            <ta e="T433" id="Seg_6332" s="T432">тоже</ta>
            <ta e="T435" id="Seg_6333" s="T434">фотографировать-PST-3PL</ta>
            <ta e="T436" id="Seg_6334" s="T435">фотокарточка-LAT</ta>
            <ta e="T437" id="Seg_6335" s="T436">там</ta>
            <ta e="T438" id="Seg_6336" s="T437">сидеть-PST-1SG</ta>
            <ta e="T439" id="Seg_6337" s="T438">говорить-PST-1SG</ta>
            <ta e="T440" id="Seg_6338" s="T439">там</ta>
            <ta e="T441" id="Seg_6339" s="T440">тоже</ta>
            <ta e="T442" id="Seg_6340" s="T441">очень</ta>
            <ta e="T443" id="Seg_6341" s="T442">хороший</ta>
            <ta e="T444" id="Seg_6342" s="T443">люди.[NOM.SG]</ta>
            <ta e="T445" id="Seg_6343" s="T444">тогда</ta>
            <ta e="T446" id="Seg_6344" s="T445">этот-PL</ta>
            <ta e="T447" id="Seg_6345" s="T446">этот-PL-LAT</ta>
            <ta e="T448" id="Seg_6346" s="T447">сказать-PST-1SG</ta>
            <ta e="T449" id="Seg_6347" s="T448">бог.[NOM.SG]</ta>
            <ta e="T450" id="Seg_6348" s="T449">бог.[NOM.SG]</ta>
            <ta e="T451" id="Seg_6349" s="T450">вы-COM</ta>
            <ta e="T452" id="Seg_6350" s="T451">и</ta>
            <ta e="T453" id="Seg_6351" s="T452">%%</ta>
            <ta e="T454" id="Seg_6352" s="T453">пойти-PST-1SG</ta>
            <ta e="T455" id="Seg_6353" s="T454">тогда</ta>
            <ta e="T456" id="Seg_6354" s="T455">там</ta>
            <ta e="T457" id="Seg_6355" s="T456">наверху</ta>
            <ta e="T459" id="Seg_6356" s="T458">быть-PST-1SG</ta>
            <ta e="T460" id="Seg_6357" s="T459">тогда</ta>
            <ta e="T461" id="Seg_6358" s="T460">стоять-PST-1PL</ta>
            <ta e="T462" id="Seg_6359" s="T461">земля-LAT</ta>
            <ta e="T463" id="Seg_6360" s="T462">%%-PST-3PL</ta>
            <ta e="T464" id="Seg_6361" s="T463">тогда</ta>
            <ta e="T465" id="Seg_6362" s="T464">пойти-PST-1PL</ta>
            <ta e="T466" id="Seg_6363" s="T465">где</ta>
            <ta e="T468" id="Seg_6364" s="T467">крест-PL</ta>
            <ta e="T469" id="Seg_6365" s="T468">там</ta>
            <ta e="T470" id="Seg_6366" s="T469">тоже</ta>
            <ta e="T471" id="Seg_6367" s="T470">идти-PST-1PL</ta>
            <ta e="T472" id="Seg_6368" s="T471">много</ta>
            <ta e="T473" id="Seg_6369" s="T472">женщина-PL</ta>
            <ta e="T474" id="Seg_6370" s="T473">быть-PST-3PL</ta>
            <ta e="T475" id="Seg_6371" s="T474">и</ta>
            <ta e="T476" id="Seg_6372" s="T475">один.[NOM.SG]</ta>
            <ta e="T477" id="Seg_6373" s="T476">два.[NOM.SG]</ta>
            <ta e="T478" id="Seg_6374" s="T477">мужчина.[NOM.SG]</ta>
            <ta e="T479" id="Seg_6375" s="T478">быть-PST-3PL</ta>
            <ta e="T480" id="Seg_6376" s="T479">тоже</ta>
            <ta e="T481" id="Seg_6377" s="T480">PTCL</ta>
            <ta e="T482" id="Seg_6378" s="T481">я.ACC</ta>
            <ta e="T483" id="Seg_6379" s="T482">целовать-PST-3PL</ta>
            <ta e="T484" id="Seg_6380" s="T483">рука-PL</ta>
            <ta e="T485" id="Seg_6381" s="T484">идти-PST-3PL</ta>
            <ta e="T486" id="Seg_6382" s="T485">дать-PST-3PL</ta>
            <ta e="T487" id="Seg_6383" s="T486">сказать-IPFVZ-3PL</ta>
            <ta e="T488" id="Seg_6384" s="T487">хороший</ta>
            <ta e="T489" id="Seg_6385" s="T488">быть-PRS-2SG</ta>
            <ta e="T490" id="Seg_6386" s="T489">очень</ta>
            <ta e="T491" id="Seg_6387" s="T490">далеко</ta>
            <ta e="T492" id="Seg_6388" s="T491">прийти-PST-2SG</ta>
            <ta e="T493" id="Seg_6389" s="T492">я.NOM</ta>
            <ta e="T494" id="Seg_6390" s="T493">сказать-PST-1SG</ta>
            <ta e="T495" id="Seg_6391" s="T494">далеко</ta>
            <ta e="T496" id="Seg_6392" s="T495">прийти-PST-1SG</ta>
            <ta e="T497" id="Seg_6393" s="T496">Монголия-PL</ta>
            <ta e="T498" id="Seg_6394" s="T497">край-LAT/LOC.3SG</ta>
            <ta e="T499" id="Seg_6395" s="T498">жить-PST-1SG</ta>
            <ta e="T500" id="Seg_6396" s="T499">хватит</ta>
            <ta e="T944" id="Seg_6397" s="T943">ну</ta>
            <ta e="T945" id="Seg_6398" s="T944">мы.NOM</ta>
            <ta e="T946" id="Seg_6399" s="T945">вчера</ta>
            <ta e="T947" id="Seg_6400" s="T946">Фрида-COM</ta>
            <ta e="T948" id="Seg_6401" s="T947">ждать-DUR-1PL</ta>
            <ta e="T949" id="Seg_6402" s="T948">всегда</ta>
            <ta e="T950" id="Seg_6403" s="T949">брат-NOM/GEN/ACC.1SG</ta>
            <ta e="T951" id="Seg_6404" s="T950">ждать-DUR.[3SG]</ta>
            <ta e="T953" id="Seg_6405" s="T952">NEG.EX</ta>
            <ta e="T954" id="Seg_6406" s="T953">NEG.EX</ta>
            <ta e="T956" id="Seg_6407" s="T954">тогда</ta>
            <ta e="T957" id="Seg_6408" s="T956">железо-ADJZ.[NOM.SG]</ta>
            <ta e="T958" id="Seg_6409" s="T957">дорога.[NOM.SG]</ta>
            <ta e="T959" id="Seg_6410" s="T958">греметь-DUR.[3SG]</ta>
            <ta e="T960" id="Seg_6411" s="T959">я.NOM</ta>
            <ta e="T961" id="Seg_6412" s="T960">сказать-IPFVZ-1SG</ta>
            <ta e="T962" id="Seg_6413" s="T961">прийти-PRS.[3SG]</ta>
            <ta e="T963" id="Seg_6414" s="T962">ну</ta>
            <ta e="T965" id="Seg_6415" s="T964">этот</ta>
            <ta e="T966" id="Seg_6416" s="T965">прийти-PST.[3SG]</ta>
            <ta e="T967" id="Seg_6417" s="T966">может.быть</ta>
            <ta e="T968" id="Seg_6418" s="T967">мать-LAT/LOC.3SG</ta>
            <ta e="T969" id="Seg_6419" s="T968">пойти-PST.[3SG]</ta>
            <ta e="T970" id="Seg_6420" s="T969">там</ta>
            <ta e="T971" id="Seg_6421" s="T970">съесть-PST.[3SG]</ta>
            <ta e="T972" id="Seg_6422" s="T971">и</ta>
            <ta e="T974" id="Seg_6423" s="T973">ложиться-PST.[3SG]</ta>
            <ta e="T975" id="Seg_6424" s="T974">ночевать-FUT-3PL</ta>
            <ta e="T976" id="Seg_6425" s="T975">спать-INF.LAT</ta>
            <ta e="T977" id="Seg_6426" s="T976">тогда</ta>
            <ta e="T978" id="Seg_6427" s="T977">сесть-PST-1PL</ta>
            <ta e="T979" id="Seg_6428" s="T978">и</ta>
            <ta e="T980" id="Seg_6429" s="T979">принести-PST-1PL</ta>
            <ta e="T981" id="Seg_6430" s="T980">принести-PST-1PL</ta>
            <ta e="T984" id="Seg_6431" s="T983">ложиться-1PL</ta>
            <ta e="T985" id="Seg_6432" s="T984">спать-INF.LAT</ta>
            <ta e="T986" id="Seg_6433" s="T985">только</ta>
            <ta e="T987" id="Seg_6434" s="T986">ложиться-1PL</ta>
            <ta e="T988" id="Seg_6435" s="T987">ложиться-PST-1PL</ta>
            <ta e="T989" id="Seg_6436" s="T988">я.NOM</ta>
            <ta e="T990" id="Seg_6437" s="T989">%едва</ta>
            <ta e="T991" id="Seg_6438" s="T990">и</ta>
            <ta e="T992" id="Seg_6439" s="T991">спать-PST-1SG</ta>
            <ta e="T993" id="Seg_6440" s="T992">ну</ta>
            <ta e="T995" id="Seg_6441" s="T994">прийти-PRS.[3SG]</ta>
            <ta e="T996" id="Seg_6442" s="T995">кто.[NOM.SG]=INDEF</ta>
            <ta e="T997" id="Seg_6443" s="T996">прийти-RES-PST.[3SG]</ta>
            <ta e="T999" id="Seg_6444" s="T998">я.NOM</ta>
            <ta e="T1000" id="Seg_6445" s="T999">прыгнуть-MOM-PST-1SG</ta>
            <ta e="T1001" id="Seg_6446" s="T1000">и</ta>
            <ta e="T1002" id="Seg_6447" s="T1001">там</ta>
            <ta e="T1003" id="Seg_6448" s="T1002">бежать-MOM-PST-1SG</ta>
            <ta e="T1004" id="Seg_6449" s="T1003">этот-LAT</ta>
            <ta e="T1005" id="Seg_6450" s="T1004">рука-NOM/GEN/ACC.1SG</ta>
            <ta e="T1006" id="Seg_6451" s="T1005">дать-PST-1SG</ta>
            <ta e="T1007" id="Seg_6452" s="T1006">говорить-PST-1PL</ta>
            <ta e="T1008" id="Seg_6453" s="T1007">этот</ta>
            <ta e="T1009" id="Seg_6454" s="T1008">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1010" id="Seg_6455" s="T1009">я.NOM</ta>
            <ta e="T1011" id="Seg_6456" s="T1010">мыть-DRV-FUT-1SG</ta>
            <ta e="T1012" id="Seg_6457" s="T1011">ванная-LAT</ta>
            <ta e="T1013" id="Seg_6458" s="T1012">пойти-FUT-1SG</ta>
            <ta e="T1014" id="Seg_6459" s="T1013">ну</ta>
            <ta e="T1015" id="Seg_6460" s="T1014">пойти-EP-IMP.2SG</ta>
            <ta e="T1016" id="Seg_6461" s="T1015">тогда</ta>
            <ta e="T1017" id="Seg_6462" s="T1016">этот</ta>
            <ta e="T1018" id="Seg_6463" s="T1017">пойти-PST.[3SG]</ta>
            <ta e="T1019" id="Seg_6464" s="T1018">я.NOM</ta>
            <ta e="T1020" id="Seg_6465" s="T1019">сказать-PST-1SG</ta>
            <ta e="T1021" id="Seg_6466" s="T1020">есть-EP-IMP.2SG</ta>
            <ta e="T1022" id="Seg_6467" s="T1021">тогда</ta>
            <ta e="T1023" id="Seg_6468" s="T1022">пойти-FUT-2SG</ta>
            <ta e="T1024" id="Seg_6469" s="T1023">тогда</ta>
            <ta e="T1025" id="Seg_6470" s="T1024">этот</ta>
            <ta e="T1026" id="Seg_6471" s="T1025">нагреться-TR-PST.[3SG]</ta>
            <ta e="T1027" id="Seg_6472" s="T1026">суп.[NOM.SG]</ta>
            <ta e="T1028" id="Seg_6473" s="T1027">желтый.[NOM.SG]</ta>
            <ta e="T1029" id="Seg_6474" s="T1028">вода.[NOM.SG]</ta>
            <ta e="T1031" id="Seg_6475" s="T1030">этот</ta>
            <ta e="T1032" id="Seg_6476" s="T1031">съесть-PST.[3SG]</ta>
            <ta e="T1033" id="Seg_6477" s="T1032">тогда</ta>
            <ta e="T1035" id="Seg_6478" s="T1034">пойти-PST.[3SG]</ta>
            <ta e="T1036" id="Seg_6479" s="T1035">мыть-DRV-INF.LAT</ta>
            <ta e="T1037" id="Seg_6480" s="T1036">ванная-LAT</ta>
            <ta e="T1038" id="Seg_6481" s="T1037">тогда</ta>
            <ta e="T1039" id="Seg_6482" s="T1038">этот</ta>
            <ta e="T1040" id="Seg_6483" s="T1039">этот-LAT</ta>
            <ta e="T1041" id="Seg_6484" s="T1040">ложиться-INF.LAT</ta>
            <ta e="T1042" id="Seg_6485" s="T1041">%%-PST.[3SG]</ta>
            <ta e="T1043" id="Seg_6486" s="T1042">здесь</ta>
            <ta e="T1044" id="Seg_6487" s="T1043">поставить-PST.[3SG]</ta>
            <ta e="T1046" id="Seg_6488" s="T1045">этот</ta>
            <ta e="T1047" id="Seg_6489" s="T1046">лежать-DUR-PST.[3SG]</ta>
            <ta e="T1048" id="Seg_6490" s="T1047">и</ta>
            <ta e="T1049" id="Seg_6491" s="T1048">спать-FUT-PST-1PL</ta>
            <ta e="T1050" id="Seg_6492" s="T1049">утро-LOC.ADV</ta>
            <ta e="T1051" id="Seg_6493" s="T1050">опять</ta>
            <ta e="T1054" id="Seg_6494" s="T1052">встать-PST-1PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PKZ">
            <ta e="T85" id="Seg_6495" s="T84">conj</ta>
            <ta e="T86" id="Seg_6496" s="T85">pers</ta>
            <ta e="T87" id="Seg_6497" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_6498" s="T87">v-v:tense-v:pn</ta>
            <ta e="T89" id="Seg_6499" s="T88">n-n:case</ta>
            <ta e="T90" id="Seg_6500" s="T89">v-v:n.fin</ta>
            <ta e="T91" id="Seg_6501" s="T90">adv</ta>
            <ta e="T92" id="Seg_6502" s="T91">v-v:tense-v:pn</ta>
            <ta e="T94" id="Seg_6503" s="T93">n-n:case</ta>
            <ta e="T99" id="Seg_6504" s="T98">v-%%-v:n.fin</ta>
            <ta e="T100" id="Seg_6505" s="T99">pers</ta>
            <ta e="T101" id="Seg_6506" s="T100">dempro</ta>
            <ta e="T102" id="Seg_6507" s="T101">n-n:case</ta>
            <ta e="T103" id="Seg_6508" s="T102">que</ta>
            <ta e="T104" id="Seg_6509" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_6510" s="T104">n-n:case</ta>
            <ta e="T107" id="Seg_6511" s="T106">v-v:tense-v:pn</ta>
            <ta e="T108" id="Seg_6512" s="T107">adv</ta>
            <ta e="T109" id="Seg_6513" s="T108">n-n:case</ta>
            <ta e="T110" id="Seg_6514" s="T109">v-v:tense-v:pn</ta>
            <ta e="T111" id="Seg_6515" s="T110">v-v:tense-v:pn</ta>
            <ta e="T112" id="Seg_6516" s="T111">que-n:case</ta>
            <ta e="T114" id="Seg_6517" s="T113">n-n:case</ta>
            <ta e="T116" id="Seg_6518" s="T115">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T117" id="Seg_6519" s="T116">conj</ta>
            <ta e="T118" id="Seg_6520" s="T117">adv</ta>
            <ta e="T119" id="Seg_6521" s="T118">n-n:case</ta>
            <ta e="T120" id="Seg_6522" s="T119">v-v:tense-v:pn</ta>
            <ta e="T121" id="Seg_6523" s="T120">conj</ta>
            <ta e="T122" id="Seg_6524" s="T121">n-n:case</ta>
            <ta e="T123" id="Seg_6525" s="T122">v-v&gt;v-v:pn</ta>
            <ta e="T124" id="Seg_6526" s="T123">ptcl</ta>
            <ta e="T369" id="Seg_6527" s="T368">pers</ta>
            <ta e="T370" id="Seg_6528" s="T369">n-n:case</ta>
            <ta e="T371" id="Seg_6529" s="T370">v-v:tense-v:pn</ta>
            <ta e="T372" id="Seg_6530" s="T371">n-n:case</ta>
            <ta e="T373" id="Seg_6531" s="T372">v-v:tense-v:pn</ta>
            <ta e="T374" id="Seg_6532" s="T373">v-v&gt;v-v:pn</ta>
            <ta e="T376" id="Seg_6533" s="T375">propr</ta>
            <ta e="T377" id="Seg_6534" s="T376">propr-n:case</ta>
            <ta e="T378" id="Seg_6535" s="T377">dempro-n:case</ta>
            <ta e="T379" id="Seg_6536" s="T378">v-v:tense-v:pn</ta>
            <ta e="T380" id="Seg_6537" s="T379">pers</ta>
            <ta e="T381" id="Seg_6538" s="T380">adv</ta>
            <ta e="T382" id="Seg_6539" s="T381">pers</ta>
            <ta e="T383" id="Seg_6540" s="T382">dempro-n:case</ta>
            <ta e="T384" id="Seg_6541" s="T383">v-v:tense-v:pn</ta>
            <ta e="T385" id="Seg_6542" s="T384">v-v:tense-v:pn</ta>
            <ta e="T386" id="Seg_6543" s="T385">adv</ta>
            <ta e="T387" id="Seg_6544" s="T386">dempro</ta>
            <ta e="T388" id="Seg_6545" s="T387">dempro</ta>
            <ta e="T389" id="Seg_6546" s="T388">n-n:case</ta>
            <ta e="T390" id="Seg_6547" s="T389">quant</ta>
            <ta e="T392" id="Seg_6548" s="T391">n-n:case</ta>
            <ta e="T393" id="Seg_6549" s="T392">adv</ta>
            <ta e="T394" id="Seg_6550" s="T393">ptcl</ta>
            <ta e="T396" id="Seg_6551" s="T395">adj-n:case</ta>
            <ta e="T397" id="Seg_6552" s="T396">adv</ta>
            <ta e="T398" id="Seg_6553" s="T397">adj-n:num</ta>
            <ta e="T399" id="Seg_6554" s="T398">v-v:n.fin</ta>
            <ta e="T400" id="Seg_6555" s="T399">adv</ta>
            <ta e="T401" id="Seg_6556" s="T400">v-v:pn</ta>
            <ta e="T402" id="Seg_6557" s="T401">adj-n:case</ta>
            <ta e="T403" id="Seg_6558" s="T402">quant</ta>
            <ta e="T404" id="Seg_6559" s="T403">adv</ta>
            <ta e="T405" id="Seg_6560" s="T404">ptcl</ta>
            <ta e="T406" id="Seg_6561" s="T405">pers</ta>
            <ta e="T407" id="Seg_6562" s="T406">v-v&gt;v-v:pn</ta>
            <ta e="T408" id="Seg_6563" s="T407">n-n:num</ta>
            <ta e="T409" id="Seg_6564" s="T408">conj</ta>
            <ta e="T410" id="Seg_6565" s="T409">n-n:num</ta>
            <ta e="T412" id="Seg_6566" s="T411">n-n:num-n:case.poss</ta>
            <ta e="T413" id="Seg_6567" s="T412">v-v&gt;v-v:pn</ta>
            <ta e="T414" id="Seg_6568" s="T413">adv</ta>
            <ta e="T415" id="Seg_6569" s="T414">dempro-n:case</ta>
            <ta e="T417" id="Seg_6570" s="T416">v-v:tense-v:pn</ta>
            <ta e="T418" id="Seg_6571" s="T417">n-n:case</ta>
            <ta e="T420" id="Seg_6572" s="T419">v-v:tense-v:pn</ta>
            <ta e="T421" id="Seg_6573" s="T420">v-v:tense-v:pn</ta>
            <ta e="T422" id="Seg_6574" s="T421">adv</ta>
            <ta e="T424" id="Seg_6575" s="T423">que</ta>
            <ta e="T425" id="Seg_6576" s="T424">n-n:case</ta>
            <ta e="T426" id="Seg_6577" s="T425">v-v:tense-v:pn</ta>
            <ta e="T427" id="Seg_6578" s="T426">conj</ta>
            <ta e="T428" id="Seg_6579" s="T427">n-n:case.poss</ta>
            <ta e="T429" id="Seg_6580" s="T428">que</ta>
            <ta e="T430" id="Seg_6581" s="T429">v-v&gt;v-v:pn</ta>
            <ta e="T431" id="Seg_6582" s="T430">adv</ta>
            <ta e="T432" id="Seg_6583" s="T431">pers</ta>
            <ta e="T433" id="Seg_6584" s="T432">ptcl</ta>
            <ta e="T435" id="Seg_6585" s="T434">v-v:tense-v:pn</ta>
            <ta e="T436" id="Seg_6586" s="T435">n-n:case</ta>
            <ta e="T437" id="Seg_6587" s="T436">adv</ta>
            <ta e="T438" id="Seg_6588" s="T437">v-v:tense-v:pn</ta>
            <ta e="T439" id="Seg_6589" s="T438">v-v:tense-v:pn</ta>
            <ta e="T440" id="Seg_6590" s="T439">adv</ta>
            <ta e="T441" id="Seg_6591" s="T440">ptcl</ta>
            <ta e="T442" id="Seg_6592" s="T441">adv</ta>
            <ta e="T443" id="Seg_6593" s="T442">adj</ta>
            <ta e="T444" id="Seg_6594" s="T443">n-n:case</ta>
            <ta e="T445" id="Seg_6595" s="T444">adv</ta>
            <ta e="T446" id="Seg_6596" s="T445">dempro-n:num</ta>
            <ta e="T447" id="Seg_6597" s="T446">dempro-n:num-n:case</ta>
            <ta e="T448" id="Seg_6598" s="T447">v-v:tense-v:pn</ta>
            <ta e="T449" id="Seg_6599" s="T448">n-n:case</ta>
            <ta e="T450" id="Seg_6600" s="T449">n-n:case</ta>
            <ta e="T451" id="Seg_6601" s="T450">pers-n:case</ta>
            <ta e="T452" id="Seg_6602" s="T451">conj</ta>
            <ta e="T453" id="Seg_6603" s="T452">%%</ta>
            <ta e="T454" id="Seg_6604" s="T453">v-v:tense-v:pn</ta>
            <ta e="T455" id="Seg_6605" s="T454">adv</ta>
            <ta e="T456" id="Seg_6606" s="T455">adv</ta>
            <ta e="T457" id="Seg_6607" s="T456">adv</ta>
            <ta e="T459" id="Seg_6608" s="T458">v-v:tense-v:pn</ta>
            <ta e="T460" id="Seg_6609" s="T459">adv</ta>
            <ta e="T461" id="Seg_6610" s="T460">v-v:tense-v:pn</ta>
            <ta e="T462" id="Seg_6611" s="T461">n-n:case</ta>
            <ta e="T463" id="Seg_6612" s="T462">v-v:tense-v:pn</ta>
            <ta e="T464" id="Seg_6613" s="T463">adv</ta>
            <ta e="T465" id="Seg_6614" s="T464">v-v:tense-v:pn</ta>
            <ta e="T466" id="Seg_6615" s="T465">que</ta>
            <ta e="T468" id="Seg_6616" s="T467">n-n:num</ta>
            <ta e="T469" id="Seg_6617" s="T468">adv</ta>
            <ta e="T470" id="Seg_6618" s="T469">ptcl</ta>
            <ta e="T471" id="Seg_6619" s="T470">v-v:tense-v:pn</ta>
            <ta e="T472" id="Seg_6620" s="T471">quant</ta>
            <ta e="T473" id="Seg_6621" s="T472">n-n:num</ta>
            <ta e="T474" id="Seg_6622" s="T473">v-v:tense-v:pn</ta>
            <ta e="T475" id="Seg_6623" s="T474">conj</ta>
            <ta e="T476" id="Seg_6624" s="T475">num-n:case</ta>
            <ta e="T477" id="Seg_6625" s="T476">num-n:case</ta>
            <ta e="T478" id="Seg_6626" s="T477">n-n:case</ta>
            <ta e="T479" id="Seg_6627" s="T478">v-v:tense-v:pn</ta>
            <ta e="T480" id="Seg_6628" s="T479">ptcl</ta>
            <ta e="T481" id="Seg_6629" s="T480">ptcl</ta>
            <ta e="T482" id="Seg_6630" s="T481">pers</ta>
            <ta e="T483" id="Seg_6631" s="T482">v-v:tense-v:pn</ta>
            <ta e="T484" id="Seg_6632" s="T483">n-n:num</ta>
            <ta e="T485" id="Seg_6633" s="T484">v-v:tense-v:pn</ta>
            <ta e="T486" id="Seg_6634" s="T485">v-v:tense-v:pn</ta>
            <ta e="T487" id="Seg_6635" s="T486">v-v&gt;v-v:pn</ta>
            <ta e="T488" id="Seg_6636" s="T487">adj</ta>
            <ta e="T489" id="Seg_6637" s="T488">v-v:tense-v:pn</ta>
            <ta e="T490" id="Seg_6638" s="T489">adv</ta>
            <ta e="T491" id="Seg_6639" s="T490">adv</ta>
            <ta e="T492" id="Seg_6640" s="T491">v-v:tense-v:pn</ta>
            <ta e="T493" id="Seg_6641" s="T492">pers</ta>
            <ta e="T494" id="Seg_6642" s="T493">v-v:tense-v:pn</ta>
            <ta e="T495" id="Seg_6643" s="T494">adv</ta>
            <ta e="T496" id="Seg_6644" s="T495">v-v:tense-v:pn</ta>
            <ta e="T497" id="Seg_6645" s="T496">propr-n:num</ta>
            <ta e="T498" id="Seg_6646" s="T497">n-n:case.poss</ta>
            <ta e="T499" id="Seg_6647" s="T498">v-v:tense-v:pn</ta>
            <ta e="T500" id="Seg_6648" s="T499">ptcl</ta>
            <ta e="T944" id="Seg_6649" s="T943">ptcl</ta>
            <ta e="T945" id="Seg_6650" s="T944">pers</ta>
            <ta e="T946" id="Seg_6651" s="T945">adv</ta>
            <ta e="T947" id="Seg_6652" s="T946">propr-n:case</ta>
            <ta e="T948" id="Seg_6653" s="T947">v-v&gt;v-v:pn</ta>
            <ta e="T949" id="Seg_6654" s="T948">adv</ta>
            <ta e="T950" id="Seg_6655" s="T949">n-n:case.poss</ta>
            <ta e="T951" id="Seg_6656" s="T950">v-v&gt;v-v:pn</ta>
            <ta e="T953" id="Seg_6657" s="T952">v</ta>
            <ta e="T954" id="Seg_6658" s="T953">v</ta>
            <ta e="T956" id="Seg_6659" s="T954">adv</ta>
            <ta e="T957" id="Seg_6660" s="T956">n-n&gt;adj-n:case</ta>
            <ta e="T958" id="Seg_6661" s="T957">n-n:case</ta>
            <ta e="T959" id="Seg_6662" s="T958">v-v&gt;v-v:pn</ta>
            <ta e="T960" id="Seg_6663" s="T959">pers</ta>
            <ta e="T961" id="Seg_6664" s="T960">v-v&gt;v-v:pn</ta>
            <ta e="T962" id="Seg_6665" s="T961">v-v:tense-v:pn</ta>
            <ta e="T963" id="Seg_6666" s="T962">ptcl</ta>
            <ta e="T965" id="Seg_6667" s="T964">dempro</ta>
            <ta e="T966" id="Seg_6668" s="T965">v-v:tense-v:pn</ta>
            <ta e="T967" id="Seg_6669" s="T966">ptcl</ta>
            <ta e="T968" id="Seg_6670" s="T967">n-n:case.poss</ta>
            <ta e="T969" id="Seg_6671" s="T968">v-v:tense-v:pn</ta>
            <ta e="T970" id="Seg_6672" s="T969">adv</ta>
            <ta e="T971" id="Seg_6673" s="T970">v-v:tense-v:pn</ta>
            <ta e="T972" id="Seg_6674" s="T971">conj</ta>
            <ta e="T974" id="Seg_6675" s="T973">v-v:tense-v:pn</ta>
            <ta e="T975" id="Seg_6676" s="T974">v-v:tense-v:pn</ta>
            <ta e="T976" id="Seg_6677" s="T975">v-v:n.fin</ta>
            <ta e="T977" id="Seg_6678" s="T976">adv</ta>
            <ta e="T978" id="Seg_6679" s="T977">v-v:tense-v:pn</ta>
            <ta e="T979" id="Seg_6680" s="T978">conj</ta>
            <ta e="T980" id="Seg_6681" s="T979">v-v:tense-v:pn</ta>
            <ta e="T981" id="Seg_6682" s="T980">v-v:tense-v:pn</ta>
            <ta e="T984" id="Seg_6683" s="T983">v-v:pn</ta>
            <ta e="T985" id="Seg_6684" s="T984">v-v:n.fin</ta>
            <ta e="T986" id="Seg_6685" s="T985">adv</ta>
            <ta e="T987" id="Seg_6686" s="T986">v-v:pn</ta>
            <ta e="T988" id="Seg_6687" s="T987">v-v:tense-v:pn</ta>
            <ta e="T989" id="Seg_6688" s="T988">pers</ta>
            <ta e="T990" id="Seg_6689" s="T989">adv</ta>
            <ta e="T991" id="Seg_6690" s="T990">conj</ta>
            <ta e="T992" id="Seg_6691" s="T991">v-v:tense-v:pn</ta>
            <ta e="T993" id="Seg_6692" s="T992">ptcl</ta>
            <ta e="T995" id="Seg_6693" s="T994">v-v:tense-v:pn</ta>
            <ta e="T996" id="Seg_6694" s="T995">que-n:case=ptcl</ta>
            <ta e="T997" id="Seg_6695" s="T996">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T999" id="Seg_6696" s="T998">pers</ta>
            <ta e="T1000" id="Seg_6697" s="T999">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1001" id="Seg_6698" s="T1000">conj</ta>
            <ta e="T1002" id="Seg_6699" s="T1001">adv</ta>
            <ta e="T1003" id="Seg_6700" s="T1002">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1004" id="Seg_6701" s="T1003">dempro-n:case</ta>
            <ta e="T1005" id="Seg_6702" s="T1004">n-n:case.poss</ta>
            <ta e="T1006" id="Seg_6703" s="T1005">v-v:tense-v:pn</ta>
            <ta e="T1007" id="Seg_6704" s="T1006">v-v:tense-v:pn</ta>
            <ta e="T1008" id="Seg_6705" s="T1007">dempro</ta>
            <ta e="T1009" id="Seg_6706" s="T1008">v-v&gt;v-v:pn</ta>
            <ta e="T1010" id="Seg_6707" s="T1009">pers</ta>
            <ta e="T1011" id="Seg_6708" s="T1010">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1012" id="Seg_6709" s="T1011">n-n:case</ta>
            <ta e="T1013" id="Seg_6710" s="T1012">v-v:tense-v:pn</ta>
            <ta e="T1014" id="Seg_6711" s="T1013">ptcl</ta>
            <ta e="T1015" id="Seg_6712" s="T1014">v-v:ins-v:mood.pn</ta>
            <ta e="T1016" id="Seg_6713" s="T1015">adv</ta>
            <ta e="T1017" id="Seg_6714" s="T1016">dempro</ta>
            <ta e="T1018" id="Seg_6715" s="T1017">v-v:tense-v:pn</ta>
            <ta e="T1019" id="Seg_6716" s="T1018">pers</ta>
            <ta e="T1020" id="Seg_6717" s="T1019">v-v:tense-v:pn</ta>
            <ta e="T1021" id="Seg_6718" s="T1020">v-v:ins-v:mood.pn</ta>
            <ta e="T1022" id="Seg_6719" s="T1021">adv</ta>
            <ta e="T1023" id="Seg_6720" s="T1022">v-v:tense-v:pn</ta>
            <ta e="T1024" id="Seg_6721" s="T1023">adv</ta>
            <ta e="T1025" id="Seg_6722" s="T1024">dempro</ta>
            <ta e="T1026" id="Seg_6723" s="T1025">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1027" id="Seg_6724" s="T1026">n-n:case</ta>
            <ta e="T1028" id="Seg_6725" s="T1027">adj-n:case</ta>
            <ta e="T1029" id="Seg_6726" s="T1028">n-n:case</ta>
            <ta e="T1031" id="Seg_6727" s="T1030">dempro</ta>
            <ta e="T1032" id="Seg_6728" s="T1031">v-v:tense-v:pn</ta>
            <ta e="T1033" id="Seg_6729" s="T1032">adv</ta>
            <ta e="T1035" id="Seg_6730" s="T1034">v-v:tense-v:pn</ta>
            <ta e="T1036" id="Seg_6731" s="T1035">v-v&gt;v-v:n.fin</ta>
            <ta e="T1037" id="Seg_6732" s="T1036">n-n:case</ta>
            <ta e="T1038" id="Seg_6733" s="T1037">adv</ta>
            <ta e="T1039" id="Seg_6734" s="T1038">dempro</ta>
            <ta e="T1040" id="Seg_6735" s="T1039">dempro-n:case</ta>
            <ta e="T1041" id="Seg_6736" s="T1040">v-v:n.fin</ta>
            <ta e="T1042" id="Seg_6737" s="T1041">v-v:tense-v:pn</ta>
            <ta e="T1043" id="Seg_6738" s="T1042">adv</ta>
            <ta e="T1044" id="Seg_6739" s="T1043">v-v:tense-v:pn</ta>
            <ta e="T1046" id="Seg_6740" s="T1045">dempro</ta>
            <ta e="T1047" id="Seg_6741" s="T1046">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1048" id="Seg_6742" s="T1047">conj</ta>
            <ta e="T1049" id="Seg_6743" s="T1048">v-v:tense-v:tense-v:pn</ta>
            <ta e="T1050" id="Seg_6744" s="T1049">n-n:case</ta>
            <ta e="T1051" id="Seg_6745" s="T1050">adv</ta>
            <ta e="T1054" id="Seg_6746" s="T1052">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PKZ">
            <ta e="T85" id="Seg_6747" s="T84">conj</ta>
            <ta e="T86" id="Seg_6748" s="T85">pers</ta>
            <ta e="T87" id="Seg_6749" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_6750" s="T87">v</ta>
            <ta e="T89" id="Seg_6751" s="T88">n</ta>
            <ta e="T90" id="Seg_6752" s="T89">v</ta>
            <ta e="T91" id="Seg_6753" s="T90">adv</ta>
            <ta e="T92" id="Seg_6754" s="T91">v</ta>
            <ta e="T94" id="Seg_6755" s="T93">n</ta>
            <ta e="T99" id="Seg_6756" s="T98">v</ta>
            <ta e="T100" id="Seg_6757" s="T99">pers</ta>
            <ta e="T101" id="Seg_6758" s="T100">dempro</ta>
            <ta e="T102" id="Seg_6759" s="T101">n</ta>
            <ta e="T103" id="Seg_6760" s="T102">que</ta>
            <ta e="T104" id="Seg_6761" s="T103">n</ta>
            <ta e="T105" id="Seg_6762" s="T104">n</ta>
            <ta e="T107" id="Seg_6763" s="T106">v</ta>
            <ta e="T108" id="Seg_6764" s="T107">adv</ta>
            <ta e="T109" id="Seg_6765" s="T108">n</ta>
            <ta e="T110" id="Seg_6766" s="T109">v</ta>
            <ta e="T111" id="Seg_6767" s="T110">v</ta>
            <ta e="T112" id="Seg_6768" s="T111">que</ta>
            <ta e="T114" id="Seg_6769" s="T113">n</ta>
            <ta e="T116" id="Seg_6770" s="T115">v</ta>
            <ta e="T117" id="Seg_6771" s="T116">conj</ta>
            <ta e="T118" id="Seg_6772" s="T117">adv</ta>
            <ta e="T119" id="Seg_6773" s="T118">n</ta>
            <ta e="T120" id="Seg_6774" s="T119">v</ta>
            <ta e="T121" id="Seg_6775" s="T120">conj</ta>
            <ta e="T122" id="Seg_6776" s="T121">n</ta>
            <ta e="T123" id="Seg_6777" s="T122">v</ta>
            <ta e="T124" id="Seg_6778" s="T123">ptcl</ta>
            <ta e="T369" id="Seg_6779" s="T368">pers</ta>
            <ta e="T370" id="Seg_6780" s="T369">n</ta>
            <ta e="T371" id="Seg_6781" s="T370">v</ta>
            <ta e="T372" id="Seg_6782" s="T371">n</ta>
            <ta e="T373" id="Seg_6783" s="T372">v</ta>
            <ta e="T374" id="Seg_6784" s="T373">v</ta>
            <ta e="T376" id="Seg_6785" s="T375">propr</ta>
            <ta e="T377" id="Seg_6786" s="T376">propr</ta>
            <ta e="T378" id="Seg_6787" s="T377">dempro</ta>
            <ta e="T379" id="Seg_6788" s="T378">v</ta>
            <ta e="T380" id="Seg_6789" s="T379">pers</ta>
            <ta e="T381" id="Seg_6790" s="T380">adv</ta>
            <ta e="T382" id="Seg_6791" s="T381">pers</ta>
            <ta e="T383" id="Seg_6792" s="T382">dempro</ta>
            <ta e="T384" id="Seg_6793" s="T383">v</ta>
            <ta e="T385" id="Seg_6794" s="T384">v</ta>
            <ta e="T386" id="Seg_6795" s="T385">adv</ta>
            <ta e="T387" id="Seg_6796" s="T386">dempro</ta>
            <ta e="T388" id="Seg_6797" s="T387">dempro</ta>
            <ta e="T389" id="Seg_6798" s="T388">n</ta>
            <ta e="T390" id="Seg_6799" s="T389">quant</ta>
            <ta e="T392" id="Seg_6800" s="T391">n</ta>
            <ta e="T393" id="Seg_6801" s="T392">adv</ta>
            <ta e="T394" id="Seg_6802" s="T393">ptcl</ta>
            <ta e="T396" id="Seg_6803" s="T395">adj</ta>
            <ta e="T397" id="Seg_6804" s="T396">adv</ta>
            <ta e="T398" id="Seg_6805" s="T397">adj</ta>
            <ta e="T399" id="Seg_6806" s="T398">v</ta>
            <ta e="T400" id="Seg_6807" s="T399">adv</ta>
            <ta e="T401" id="Seg_6808" s="T400">v</ta>
            <ta e="T402" id="Seg_6809" s="T401">adj</ta>
            <ta e="T403" id="Seg_6810" s="T402">quant</ta>
            <ta e="T404" id="Seg_6811" s="T403">adv</ta>
            <ta e="T405" id="Seg_6812" s="T404">ptcl</ta>
            <ta e="T406" id="Seg_6813" s="T405">pers</ta>
            <ta e="T407" id="Seg_6814" s="T406">v</ta>
            <ta e="T408" id="Seg_6815" s="T407">n</ta>
            <ta e="T409" id="Seg_6816" s="T408">conj</ta>
            <ta e="T410" id="Seg_6817" s="T409">n</ta>
            <ta e="T412" id="Seg_6818" s="T411">n</ta>
            <ta e="T413" id="Seg_6819" s="T412">v</ta>
            <ta e="T414" id="Seg_6820" s="T413">adv</ta>
            <ta e="T415" id="Seg_6821" s="T414">dempro</ta>
            <ta e="T417" id="Seg_6822" s="T416">v</ta>
            <ta e="T418" id="Seg_6823" s="T417">v</ta>
            <ta e="T420" id="Seg_6824" s="T419">v</ta>
            <ta e="T421" id="Seg_6825" s="T420">v</ta>
            <ta e="T422" id="Seg_6826" s="T421">adv</ta>
            <ta e="T424" id="Seg_6827" s="T423">que</ta>
            <ta e="T425" id="Seg_6828" s="T424">n</ta>
            <ta e="T426" id="Seg_6829" s="T425">v</ta>
            <ta e="T427" id="Seg_6830" s="T426">conj</ta>
            <ta e="T428" id="Seg_6831" s="T427">n</ta>
            <ta e="T429" id="Seg_6832" s="T428">que</ta>
            <ta e="T430" id="Seg_6833" s="T429">v</ta>
            <ta e="T431" id="Seg_6834" s="T430">adv</ta>
            <ta e="T432" id="Seg_6835" s="T431">pers</ta>
            <ta e="T433" id="Seg_6836" s="T432">ptcl</ta>
            <ta e="T435" id="Seg_6837" s="T434">v</ta>
            <ta e="T436" id="Seg_6838" s="T435">n</ta>
            <ta e="T437" id="Seg_6839" s="T436">adv</ta>
            <ta e="T438" id="Seg_6840" s="T437">v</ta>
            <ta e="T439" id="Seg_6841" s="T438">v</ta>
            <ta e="T440" id="Seg_6842" s="T439">adv</ta>
            <ta e="T441" id="Seg_6843" s="T440">ptcl</ta>
            <ta e="T442" id="Seg_6844" s="T441">adv</ta>
            <ta e="T443" id="Seg_6845" s="T442">adj</ta>
            <ta e="T444" id="Seg_6846" s="T443">n</ta>
            <ta e="T445" id="Seg_6847" s="T444">adv</ta>
            <ta e="T446" id="Seg_6848" s="T445">dempro</ta>
            <ta e="T447" id="Seg_6849" s="T446">dempro</ta>
            <ta e="T448" id="Seg_6850" s="T447">v</ta>
            <ta e="T449" id="Seg_6851" s="T448">n</ta>
            <ta e="T450" id="Seg_6852" s="T449">n</ta>
            <ta e="T451" id="Seg_6853" s="T450">pers</ta>
            <ta e="T452" id="Seg_6854" s="T451">conj</ta>
            <ta e="T454" id="Seg_6855" s="T453">v</ta>
            <ta e="T455" id="Seg_6856" s="T454">adv</ta>
            <ta e="T456" id="Seg_6857" s="T455">adv</ta>
            <ta e="T457" id="Seg_6858" s="T456">adv</ta>
            <ta e="T459" id="Seg_6859" s="T458">v</ta>
            <ta e="T460" id="Seg_6860" s="T459">adv</ta>
            <ta e="T461" id="Seg_6861" s="T460">v</ta>
            <ta e="T462" id="Seg_6862" s="T461">n</ta>
            <ta e="T463" id="Seg_6863" s="T462">v</ta>
            <ta e="T464" id="Seg_6864" s="T463">adv</ta>
            <ta e="T465" id="Seg_6865" s="T464">v</ta>
            <ta e="T466" id="Seg_6866" s="T465">que</ta>
            <ta e="T468" id="Seg_6867" s="T467">n</ta>
            <ta e="T469" id="Seg_6868" s="T468">adv</ta>
            <ta e="T470" id="Seg_6869" s="T469">ptcl</ta>
            <ta e="T471" id="Seg_6870" s="T470">v</ta>
            <ta e="T472" id="Seg_6871" s="T471">quant</ta>
            <ta e="T473" id="Seg_6872" s="T472">n</ta>
            <ta e="T474" id="Seg_6873" s="T473">v</ta>
            <ta e="T475" id="Seg_6874" s="T474">conj</ta>
            <ta e="T476" id="Seg_6875" s="T475">num</ta>
            <ta e="T477" id="Seg_6876" s="T476">num</ta>
            <ta e="T478" id="Seg_6877" s="T477">n</ta>
            <ta e="T479" id="Seg_6878" s="T478">v</ta>
            <ta e="T480" id="Seg_6879" s="T479">ptcl</ta>
            <ta e="T481" id="Seg_6880" s="T480">ptcl</ta>
            <ta e="T482" id="Seg_6881" s="T481">pers</ta>
            <ta e="T483" id="Seg_6882" s="T482">v</ta>
            <ta e="T484" id="Seg_6883" s="T483">n</ta>
            <ta e="T485" id="Seg_6884" s="T484">v</ta>
            <ta e="T486" id="Seg_6885" s="T485">v</ta>
            <ta e="T487" id="Seg_6886" s="T486">v</ta>
            <ta e="T488" id="Seg_6887" s="T487">adj</ta>
            <ta e="T489" id="Seg_6888" s="T488">v</ta>
            <ta e="T490" id="Seg_6889" s="T489">adv</ta>
            <ta e="T491" id="Seg_6890" s="T490">adv</ta>
            <ta e="T492" id="Seg_6891" s="T491">v</ta>
            <ta e="T493" id="Seg_6892" s="T492">pers</ta>
            <ta e="T494" id="Seg_6893" s="T493">v</ta>
            <ta e="T495" id="Seg_6894" s="T494">adv</ta>
            <ta e="T496" id="Seg_6895" s="T495">v</ta>
            <ta e="T497" id="Seg_6896" s="T496">propr</ta>
            <ta e="T498" id="Seg_6897" s="T497">n</ta>
            <ta e="T499" id="Seg_6898" s="T498">v</ta>
            <ta e="T500" id="Seg_6899" s="T499">ptcl</ta>
            <ta e="T944" id="Seg_6900" s="T943">ptcl</ta>
            <ta e="T945" id="Seg_6901" s="T944">pers</ta>
            <ta e="T946" id="Seg_6902" s="T945">adv</ta>
            <ta e="T947" id="Seg_6903" s="T946">propr</ta>
            <ta e="T948" id="Seg_6904" s="T947">v</ta>
            <ta e="T949" id="Seg_6905" s="T948">adv</ta>
            <ta e="T950" id="Seg_6906" s="T949">n</ta>
            <ta e="T951" id="Seg_6907" s="T950">v</ta>
            <ta e="T953" id="Seg_6908" s="T952">v</ta>
            <ta e="T954" id="Seg_6909" s="T953">v</ta>
            <ta e="T956" id="Seg_6910" s="T954">adv</ta>
            <ta e="T957" id="Seg_6911" s="T956">n</ta>
            <ta e="T958" id="Seg_6912" s="T957">n</ta>
            <ta e="T959" id="Seg_6913" s="T958">v</ta>
            <ta e="T960" id="Seg_6914" s="T959">pers</ta>
            <ta e="T961" id="Seg_6915" s="T960">v</ta>
            <ta e="T962" id="Seg_6916" s="T961">v</ta>
            <ta e="T963" id="Seg_6917" s="T962">ptcl</ta>
            <ta e="T965" id="Seg_6918" s="T964">dempro</ta>
            <ta e="T966" id="Seg_6919" s="T965">v</ta>
            <ta e="T967" id="Seg_6920" s="T966">ptcl</ta>
            <ta e="T968" id="Seg_6921" s="T967">n</ta>
            <ta e="T969" id="Seg_6922" s="T968">v</ta>
            <ta e="T970" id="Seg_6923" s="T969">adv</ta>
            <ta e="T971" id="Seg_6924" s="T970">v</ta>
            <ta e="T972" id="Seg_6925" s="T971">conj</ta>
            <ta e="T974" id="Seg_6926" s="T973">v</ta>
            <ta e="T975" id="Seg_6927" s="T974">v</ta>
            <ta e="T976" id="Seg_6928" s="T975">v</ta>
            <ta e="T977" id="Seg_6929" s="T976">adv</ta>
            <ta e="T978" id="Seg_6930" s="T977">v</ta>
            <ta e="T979" id="Seg_6931" s="T978">conj</ta>
            <ta e="T980" id="Seg_6932" s="T979">v</ta>
            <ta e="T981" id="Seg_6933" s="T980">v</ta>
            <ta e="T984" id="Seg_6934" s="T983">v</ta>
            <ta e="T985" id="Seg_6935" s="T984">v</ta>
            <ta e="T986" id="Seg_6936" s="T985">adv</ta>
            <ta e="T987" id="Seg_6937" s="T986">v</ta>
            <ta e="T988" id="Seg_6938" s="T987">v</ta>
            <ta e="T989" id="Seg_6939" s="T988">pers</ta>
            <ta e="T990" id="Seg_6940" s="T989">adv</ta>
            <ta e="T991" id="Seg_6941" s="T990">conj</ta>
            <ta e="T992" id="Seg_6942" s="T991">v</ta>
            <ta e="T993" id="Seg_6943" s="T992">ptcl</ta>
            <ta e="T995" id="Seg_6944" s="T994">v</ta>
            <ta e="T996" id="Seg_6945" s="T995">que</ta>
            <ta e="T997" id="Seg_6946" s="T996">v</ta>
            <ta e="T999" id="Seg_6947" s="T998">pers</ta>
            <ta e="T1000" id="Seg_6948" s="T999">v</ta>
            <ta e="T1001" id="Seg_6949" s="T1000">conj</ta>
            <ta e="T1002" id="Seg_6950" s="T1001">adv</ta>
            <ta e="T1003" id="Seg_6951" s="T1002">v</ta>
            <ta e="T1004" id="Seg_6952" s="T1003">dempro</ta>
            <ta e="T1005" id="Seg_6953" s="T1004">n</ta>
            <ta e="T1006" id="Seg_6954" s="T1005">v</ta>
            <ta e="T1007" id="Seg_6955" s="T1006">v</ta>
            <ta e="T1008" id="Seg_6956" s="T1007">dempro</ta>
            <ta e="T1009" id="Seg_6957" s="T1008">v</ta>
            <ta e="T1010" id="Seg_6958" s="T1009">pers</ta>
            <ta e="T1011" id="Seg_6959" s="T1010">v</ta>
            <ta e="T1012" id="Seg_6960" s="T1011">n</ta>
            <ta e="T1013" id="Seg_6961" s="T1012">v</ta>
            <ta e="T1014" id="Seg_6962" s="T1013">ptcl</ta>
            <ta e="T1015" id="Seg_6963" s="T1014">v</ta>
            <ta e="T1016" id="Seg_6964" s="T1015">adv</ta>
            <ta e="T1017" id="Seg_6965" s="T1016">dempro</ta>
            <ta e="T1018" id="Seg_6966" s="T1017">v</ta>
            <ta e="T1019" id="Seg_6967" s="T1018">pers</ta>
            <ta e="T1020" id="Seg_6968" s="T1019">v</ta>
            <ta e="T1021" id="Seg_6969" s="T1020">v</ta>
            <ta e="T1022" id="Seg_6970" s="T1021">adv</ta>
            <ta e="T1023" id="Seg_6971" s="T1022">v</ta>
            <ta e="T1024" id="Seg_6972" s="T1023">adv</ta>
            <ta e="T1025" id="Seg_6973" s="T1024">dempro</ta>
            <ta e="T1026" id="Seg_6974" s="T1025">v</ta>
            <ta e="T1027" id="Seg_6975" s="T1026">n</ta>
            <ta e="T1028" id="Seg_6976" s="T1027">adj</ta>
            <ta e="T1029" id="Seg_6977" s="T1028">n</ta>
            <ta e="T1031" id="Seg_6978" s="T1030">dempro</ta>
            <ta e="T1032" id="Seg_6979" s="T1031">v</ta>
            <ta e="T1033" id="Seg_6980" s="T1032">adv</ta>
            <ta e="T1035" id="Seg_6981" s="T1034">v</ta>
            <ta e="T1036" id="Seg_6982" s="T1035">v</ta>
            <ta e="T1037" id="Seg_6983" s="T1036">n</ta>
            <ta e="T1038" id="Seg_6984" s="T1037">adv</ta>
            <ta e="T1039" id="Seg_6985" s="T1038">dempro</ta>
            <ta e="T1040" id="Seg_6986" s="T1039">dempro</ta>
            <ta e="T1041" id="Seg_6987" s="T1040">v</ta>
            <ta e="T1042" id="Seg_6988" s="T1041">v</ta>
            <ta e="T1043" id="Seg_6989" s="T1042">adv</ta>
            <ta e="T1044" id="Seg_6990" s="T1043">n</ta>
            <ta e="T1046" id="Seg_6991" s="T1045">dempro</ta>
            <ta e="T1047" id="Seg_6992" s="T1046">v</ta>
            <ta e="T1048" id="Seg_6993" s="T1047">conj</ta>
            <ta e="T1049" id="Seg_6994" s="T1048">v</ta>
            <ta e="T1050" id="Seg_6995" s="T1049">n</ta>
            <ta e="T1051" id="Seg_6996" s="T1050">adv</ta>
            <ta e="T1054" id="Seg_6997" s="T1052">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PKZ">
            <ta e="T88" id="Seg_6998" s="T87">0.3.h:A</ta>
            <ta e="T89" id="Seg_6999" s="T88">np:R</ta>
            <ta e="T91" id="Seg_7000" s="T90">adv:Time</ta>
            <ta e="T92" id="Seg_7001" s="T91">0.3.h:A</ta>
            <ta e="T94" id="Seg_7002" s="T93">np:P</ta>
            <ta e="T100" id="Seg_7003" s="T99">pro.h:R</ta>
            <ta e="T102" id="Seg_7004" s="T101">np.h:Th</ta>
            <ta e="T104" id="Seg_7005" s="T103">np.h:A</ta>
            <ta e="T105" id="Seg_7006" s="T104">np:R</ta>
            <ta e="T108" id="Seg_7007" s="T107">adv:Time</ta>
            <ta e="T109" id="Seg_7008" s="T108">np:Th</ta>
            <ta e="T111" id="Seg_7009" s="T110">0.3.h:A</ta>
            <ta e="T112" id="Seg_7010" s="T111">pro.h:A</ta>
            <ta e="T114" id="Seg_7011" s="T113">np:R</ta>
            <ta e="T118" id="Seg_7012" s="T117">adv:L</ta>
            <ta e="T119" id="Seg_7013" s="T118">np:L</ta>
            <ta e="T120" id="Seg_7014" s="T119">0.3.h:A</ta>
            <ta e="T122" id="Seg_7015" s="T121">np:R</ta>
            <ta e="T123" id="Seg_7016" s="T122">0.1.h:A</ta>
            <ta e="T369" id="Seg_7017" s="T368">pro.h:A</ta>
            <ta e="T370" id="Seg_7018" s="T369">n:Time</ta>
            <ta e="T372" id="Seg_7019" s="T371">np:R</ta>
            <ta e="T373" id="Seg_7020" s="T372">0.1.h:A</ta>
            <ta e="T374" id="Seg_7021" s="T373">0.1.h:A</ta>
            <ta e="T377" id="Seg_7022" s="T376">np.h:Th</ta>
            <ta e="T378" id="Seg_7023" s="T377">pro.h:A</ta>
            <ta e="T380" id="Seg_7024" s="T379">pro:G</ta>
            <ta e="T381" id="Seg_7025" s="T380">adv:Time</ta>
            <ta e="T382" id="Seg_7026" s="T381">pro.h:A</ta>
            <ta e="T383" id="Seg_7027" s="T382">pro.h:Com</ta>
            <ta e="T385" id="Seg_7028" s="T384">0.1.h:A</ta>
            <ta e="T386" id="Seg_7029" s="T385">adv:L</ta>
            <ta e="T389" id="Seg_7030" s="T388">np:G</ta>
            <ta e="T392" id="Seg_7031" s="T391">np.h:Th</ta>
            <ta e="T393" id="Seg_7032" s="T392">adv:L</ta>
            <ta e="T401" id="Seg_7033" s="T400">0.3.h:E</ta>
            <ta e="T404" id="Seg_7034" s="T403">adv:Time</ta>
            <ta e="T406" id="Seg_7035" s="T405">pro.h:Th</ta>
            <ta e="T407" id="Seg_7036" s="T406">0.3.h:A</ta>
            <ta e="T410" id="Seg_7037" s="T409">np.h:A</ta>
            <ta e="T412" id="Seg_7038" s="T411">np:Th</ta>
            <ta e="T414" id="Seg_7039" s="T413">adv:Time</ta>
            <ta e="T415" id="Seg_7040" s="T414">pro:So</ta>
            <ta e="T417" id="Seg_7041" s="T416">0.1.h:A</ta>
            <ta e="T418" id="Seg_7042" s="T417">np:G</ta>
            <ta e="T420" id="Seg_7043" s="T419">0.1.h:A</ta>
            <ta e="T421" id="Seg_7044" s="T420">0.1.h:Th</ta>
            <ta e="T422" id="Seg_7045" s="T421">adv:L</ta>
            <ta e="T425" id="Seg_7046" s="T424">np:P</ta>
            <ta e="T426" id="Seg_7047" s="T425">0.3.h:A</ta>
            <ta e="T428" id="Seg_7048" s="T427">np:Th</ta>
            <ta e="T429" id="Seg_7049" s="T428">pro:L</ta>
            <ta e="T430" id="Seg_7050" s="T429">0.3.h:A</ta>
            <ta e="T431" id="Seg_7051" s="T430">adv:L</ta>
            <ta e="T432" id="Seg_7052" s="T431">pro.h:Th</ta>
            <ta e="T435" id="Seg_7053" s="T434">0.3.h:A</ta>
            <ta e="T436" id="Seg_7054" s="T435">np:G</ta>
            <ta e="T437" id="Seg_7055" s="T436">adv:L</ta>
            <ta e="T438" id="Seg_7056" s="T437">0.1.h:Th</ta>
            <ta e="T439" id="Seg_7057" s="T438">0.1.h:A</ta>
            <ta e="T440" id="Seg_7058" s="T439">adv:L</ta>
            <ta e="T444" id="Seg_7059" s="T443">np.h:Th</ta>
            <ta e="T445" id="Seg_7060" s="T444">adv:Time</ta>
            <ta e="T447" id="Seg_7061" s="T446">pro.h:R</ta>
            <ta e="T448" id="Seg_7062" s="T447">0.1.h:A</ta>
            <ta e="T450" id="Seg_7063" s="T449">np:A</ta>
            <ta e="T451" id="Seg_7064" s="T450">np:Com</ta>
            <ta e="T454" id="Seg_7065" s="T453">0.1.h:A</ta>
            <ta e="T455" id="Seg_7066" s="T454">adv:Time</ta>
            <ta e="T456" id="Seg_7067" s="T455">adv:L</ta>
            <ta e="T459" id="Seg_7068" s="T458">0.1.h:Th</ta>
            <ta e="T460" id="Seg_7069" s="T459">adv:Time</ta>
            <ta e="T461" id="Seg_7070" s="T460">0.1.h:A</ta>
            <ta e="T462" id="Seg_7071" s="T461">np:L</ta>
            <ta e="T464" id="Seg_7072" s="T463">adv:Time</ta>
            <ta e="T465" id="Seg_7073" s="T464">0.1.h:A</ta>
            <ta e="T466" id="Seg_7074" s="T465">pro:L</ta>
            <ta e="T468" id="Seg_7075" s="T467">np:Th</ta>
            <ta e="T469" id="Seg_7076" s="T468">adv:L</ta>
            <ta e="T471" id="Seg_7077" s="T470">0.1.h:A</ta>
            <ta e="T473" id="Seg_7078" s="T472">np.h:Th</ta>
            <ta e="T478" id="Seg_7079" s="T477">np.h:Th</ta>
            <ta e="T482" id="Seg_7080" s="T481">pro.h:Th</ta>
            <ta e="T483" id="Seg_7081" s="T482">0.3.h:A</ta>
            <ta e="T484" id="Seg_7082" s="T483">np:Th</ta>
            <ta e="T486" id="Seg_7083" s="T485">0.3.h:A</ta>
            <ta e="T487" id="Seg_7084" s="T486">0.3.h:A</ta>
            <ta e="T489" id="Seg_7085" s="T488">0.2.h:Th</ta>
            <ta e="T492" id="Seg_7086" s="T491">0.2.h:A</ta>
            <ta e="T493" id="Seg_7087" s="T492">pro.h:A</ta>
            <ta e="T496" id="Seg_7088" s="T495">0.1.h:A</ta>
            <ta e="T498" id="Seg_7089" s="T497">np:L</ta>
            <ta e="T499" id="Seg_7090" s="T498">0.1.h:E</ta>
            <ta e="T945" id="Seg_7091" s="T944">pro.h:A</ta>
            <ta e="T946" id="Seg_7092" s="T945">n:Time</ta>
            <ta e="T947" id="Seg_7093" s="T946">np.h:Com</ta>
            <ta e="T950" id="Seg_7094" s="T949">np.h:Th</ta>
            <ta e="T951" id="Seg_7095" s="T950">0.3.h:A</ta>
            <ta e="T956" id="Seg_7096" s="T954">adv:Time</ta>
            <ta e="T958" id="Seg_7097" s="T957">np:A</ta>
            <ta e="T960" id="Seg_7098" s="T959">pro.h:A</ta>
            <ta e="T962" id="Seg_7099" s="T961">0.3.h:A</ta>
            <ta e="T965" id="Seg_7100" s="T964">pro.h:A</ta>
            <ta e="T968" id="Seg_7101" s="T967">np:G</ta>
            <ta e="T969" id="Seg_7102" s="T968">0.3.h:A</ta>
            <ta e="T970" id="Seg_7103" s="T969">adv:L</ta>
            <ta e="T971" id="Seg_7104" s="T970">0.3.h:A</ta>
            <ta e="T974" id="Seg_7105" s="T973">0.3.h:A</ta>
            <ta e="T975" id="Seg_7106" s="T974">0.3.h:Th</ta>
            <ta e="T977" id="Seg_7107" s="T976">adv:Time</ta>
            <ta e="T978" id="Seg_7108" s="T977">0.1.h:A</ta>
            <ta e="T980" id="Seg_7109" s="T979">0.1.h:A</ta>
            <ta e="T984" id="Seg_7110" s="T983">0.1.h:A</ta>
            <ta e="T988" id="Seg_7111" s="T987">0.1.h:A</ta>
            <ta e="T989" id="Seg_7112" s="T988">pro.h:E</ta>
            <ta e="T996" id="Seg_7113" s="T995">pro.h:A</ta>
            <ta e="T997" id="Seg_7114" s="T996">0.3.h:A</ta>
            <ta e="T999" id="Seg_7115" s="T998">pro.h:A</ta>
            <ta e="T1002" id="Seg_7116" s="T1001">adv:L</ta>
            <ta e="T1003" id="Seg_7117" s="T1002">0.1.h:A</ta>
            <ta e="T1004" id="Seg_7118" s="T1003">pro.h:Poss</ta>
            <ta e="T1005" id="Seg_7119" s="T1004">np:Th</ta>
            <ta e="T1006" id="Seg_7120" s="T1005">0.1.h:A</ta>
            <ta e="T1010" id="Seg_7121" s="T1009">pro.h:A</ta>
            <ta e="T1012" id="Seg_7122" s="T1011">np:G</ta>
            <ta e="T1013" id="Seg_7123" s="T1012">0.1.h:A</ta>
            <ta e="T1015" id="Seg_7124" s="T1014">0.2.h:A</ta>
            <ta e="T1016" id="Seg_7125" s="T1015">adv:Time</ta>
            <ta e="T1017" id="Seg_7126" s="T1016">pro.h:A</ta>
            <ta e="T1019" id="Seg_7127" s="T1018">pro.h:A</ta>
            <ta e="T1021" id="Seg_7128" s="T1020">0.2.h:A</ta>
            <ta e="T1022" id="Seg_7129" s="T1021">adv:Time</ta>
            <ta e="T1023" id="Seg_7130" s="T1022">0.2.h:A</ta>
            <ta e="T1024" id="Seg_7131" s="T1023">adv:Time</ta>
            <ta e="T1025" id="Seg_7132" s="T1024">pro.h:A</ta>
            <ta e="T1027" id="Seg_7133" s="T1026">np:P</ta>
            <ta e="T1029" id="Seg_7134" s="T1028">np:P</ta>
            <ta e="T1033" id="Seg_7135" s="T1032">adv:Time</ta>
            <ta e="T1035" id="Seg_7136" s="T1034">0.3.h:A</ta>
            <ta e="T1037" id="Seg_7137" s="T1036">np:G</ta>
            <ta e="T1038" id="Seg_7138" s="T1037">adv:Time</ta>
            <ta e="T1039" id="Seg_7139" s="T1038">pro.h:A</ta>
            <ta e="T1040" id="Seg_7140" s="T1039">pro.h:B</ta>
            <ta e="T1043" id="Seg_7141" s="T1042">adv:L</ta>
            <ta e="T1044" id="Seg_7142" s="T1043">0.3.h:A</ta>
            <ta e="T1047" id="Seg_7143" s="T1046">0.3.h:A</ta>
            <ta e="T1049" id="Seg_7144" s="T1048">0.1.h:E</ta>
            <ta e="T1050" id="Seg_7145" s="T1049">n:Time</ta>
            <ta e="T1054" id="Seg_7146" s="T1052">0.1.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-PKZ">
            <ta e="T87" id="Seg_7147" s="T86">ptcl.neg</ta>
            <ta e="T88" id="Seg_7148" s="T87">v:pred 0.3.h:S</ta>
            <ta e="T90" id="Seg_7149" s="T89">s:purp</ta>
            <ta e="T92" id="Seg_7150" s="T91">v:pred 0.3.h:S</ta>
            <ta e="T94" id="Seg_7151" s="T93">np:O</ta>
            <ta e="T102" id="Seg_7152" s="T101">np.h:O</ta>
            <ta e="T103" id="Seg_7153" s="T102">s:rel</ta>
            <ta e="T104" id="Seg_7154" s="T103">np.h:S</ta>
            <ta e="T107" id="Seg_7155" s="T106">v:pred</ta>
            <ta e="T109" id="Seg_7156" s="T108">np:S</ta>
            <ta e="T110" id="Seg_7157" s="T109">v:pred</ta>
            <ta e="T111" id="Seg_7158" s="T110">v:pred 0.3.h:S</ta>
            <ta e="T112" id="Seg_7159" s="T111">s:rel pro.h:S</ta>
            <ta e="T116" id="Seg_7160" s="T115">v:pred</ta>
            <ta e="T120" id="Seg_7161" s="T119">v:pred 0.3.h:S</ta>
            <ta e="T123" id="Seg_7162" s="T122">v:pred 0.1.h:S</ta>
            <ta e="T369" id="Seg_7163" s="T368">pro.h:S</ta>
            <ta e="T371" id="Seg_7164" s="T370">v:pred</ta>
            <ta e="T373" id="Seg_7165" s="T372">v:pred 0.1.h:S</ta>
            <ta e="T374" id="Seg_7166" s="T373">v:pred 0.1.h:S</ta>
            <ta e="T377" id="Seg_7167" s="T376">np.h:O</ta>
            <ta e="T378" id="Seg_7168" s="T377">pro.h:S</ta>
            <ta e="T379" id="Seg_7169" s="T378">v:pred</ta>
            <ta e="T382" id="Seg_7170" s="T381">pro.h:S</ta>
            <ta e="T384" id="Seg_7171" s="T383">v:pred</ta>
            <ta e="T385" id="Seg_7172" s="T384">v:pred 0.1.h:S</ta>
            <ta e="T392" id="Seg_7173" s="T391">np.h:S</ta>
            <ta e="T396" id="Seg_7174" s="T395">adj:pred</ta>
            <ta e="T398" id="Seg_7175" s="T397">adj:pred</ta>
            <ta e="T399" id="Seg_7176" s="T398">s:purp</ta>
            <ta e="T401" id="Seg_7177" s="T400">v:pred 0.3.h:S</ta>
            <ta e="T406" id="Seg_7178" s="T405">pro.h:O</ta>
            <ta e="T407" id="Seg_7179" s="T406">v:pred 0.3.h:S</ta>
            <ta e="T410" id="Seg_7180" s="T409">np.h:S</ta>
            <ta e="T412" id="Seg_7181" s="T411">np:O</ta>
            <ta e="T413" id="Seg_7182" s="T412">v:pred</ta>
            <ta e="T417" id="Seg_7183" s="T416">v:pred 0.1.h:S</ta>
            <ta e="T420" id="Seg_7184" s="T419">v:pred 0.1.h:S</ta>
            <ta e="T421" id="Seg_7185" s="T420">v:pred 0.1.h:S</ta>
            <ta e="T425" id="Seg_7186" s="T424">np:O</ta>
            <ta e="T426" id="Seg_7187" s="T425">v:pred 0.3.h:S</ta>
            <ta e="T428" id="Seg_7188" s="T427">np:O</ta>
            <ta e="T430" id="Seg_7189" s="T429">v:pred 0.3.h:S</ta>
            <ta e="T432" id="Seg_7190" s="T431">pro.h:O</ta>
            <ta e="T435" id="Seg_7191" s="T434">v:pred 0.3.h:S</ta>
            <ta e="T438" id="Seg_7192" s="T437">v:pred 0.1.h:S</ta>
            <ta e="T439" id="Seg_7193" s="T438">v:pred 0.1.h:S</ta>
            <ta e="T443" id="Seg_7194" s="T442">adj:pred</ta>
            <ta e="T444" id="Seg_7195" s="T443">np.h:S</ta>
            <ta e="T448" id="Seg_7196" s="T447">v:pred 0.1.h:S</ta>
            <ta e="T450" id="Seg_7197" s="T449">np:S</ta>
            <ta e="T451" id="Seg_7198" s="T450">n:pred</ta>
            <ta e="T454" id="Seg_7199" s="T453">v:pred 0.1.h:S</ta>
            <ta e="T459" id="Seg_7200" s="T458">v:pred 0.1.h:S</ta>
            <ta e="T461" id="Seg_7201" s="T460">v:pred 0.1.h:S</ta>
            <ta e="T465" id="Seg_7202" s="T464">v:pred 0.1.h:S</ta>
            <ta e="T468" id="Seg_7203" s="T467">np:S</ta>
            <ta e="T471" id="Seg_7204" s="T470">v:pred 0.1.h:S</ta>
            <ta e="T473" id="Seg_7205" s="T472">np.h:S</ta>
            <ta e="T474" id="Seg_7206" s="T473">v:pred</ta>
            <ta e="T478" id="Seg_7207" s="T477">np.h:S</ta>
            <ta e="T479" id="Seg_7208" s="T478">v:pred</ta>
            <ta e="T482" id="Seg_7209" s="T481">pro.h:O</ta>
            <ta e="T483" id="Seg_7210" s="T482">v:pred 0.3.h:S</ta>
            <ta e="T484" id="Seg_7211" s="T483">np:O</ta>
            <ta e="T486" id="Seg_7212" s="T485">v:pred 0.3.h:S</ta>
            <ta e="T487" id="Seg_7213" s="T486">v:pred 0.3.h:S</ta>
            <ta e="T488" id="Seg_7214" s="T487">adj:pred</ta>
            <ta e="T489" id="Seg_7215" s="T488">cop 0.2.h:S</ta>
            <ta e="T492" id="Seg_7216" s="T491">v:pred 0.2.h:S</ta>
            <ta e="T493" id="Seg_7217" s="T492">pro.h:S</ta>
            <ta e="T494" id="Seg_7218" s="T493">v:pred</ta>
            <ta e="T496" id="Seg_7219" s="T495">v:pred 0.1.h:S</ta>
            <ta e="T499" id="Seg_7220" s="T498"> 0.1.h:S v:pred</ta>
            <ta e="T945" id="Seg_7221" s="T944">pro.h:S</ta>
            <ta e="T948" id="Seg_7222" s="T947">v:pred</ta>
            <ta e="T950" id="Seg_7223" s="T949">np.h:O</ta>
            <ta e="T951" id="Seg_7224" s="T950">v:pred 0.3.h:S</ta>
            <ta e="T953" id="Seg_7225" s="T952">v:pred</ta>
            <ta e="T954" id="Seg_7226" s="T953">v:pred</ta>
            <ta e="T958" id="Seg_7227" s="T957">np:S</ta>
            <ta e="T959" id="Seg_7228" s="T958">v:pred</ta>
            <ta e="T960" id="Seg_7229" s="T959">pro.h:S</ta>
            <ta e="T961" id="Seg_7230" s="T960">v:pred</ta>
            <ta e="T962" id="Seg_7231" s="T961">v:pred 0.3.h:S</ta>
            <ta e="T965" id="Seg_7232" s="T964">pro.h:S</ta>
            <ta e="T966" id="Seg_7233" s="T965">v:pred</ta>
            <ta e="T969" id="Seg_7234" s="T968">v:pred 0.3.h:S</ta>
            <ta e="T971" id="Seg_7235" s="T970">v:pred 0.3.h:S</ta>
            <ta e="T974" id="Seg_7236" s="T973">v:pred 0.3.h:S</ta>
            <ta e="T975" id="Seg_7237" s="T974">v:pred 0.3.h:S</ta>
            <ta e="T976" id="Seg_7238" s="T975">s:purp</ta>
            <ta e="T978" id="Seg_7239" s="T977">v:pred 0.1.h:S</ta>
            <ta e="T980" id="Seg_7240" s="T979">v:pred 0.1.h:S</ta>
            <ta e="T984" id="Seg_7241" s="T983">v:pred 0.1.h:S</ta>
            <ta e="T985" id="Seg_7242" s="T984">s:purp</ta>
            <ta e="T988" id="Seg_7243" s="T987">v:pred 0.1.h:S</ta>
            <ta e="T989" id="Seg_7244" s="T988">pro.h:S</ta>
            <ta e="T992" id="Seg_7245" s="T991">v:pred</ta>
            <ta e="T995" id="Seg_7246" s="T994">v:pred</ta>
            <ta e="T996" id="Seg_7247" s="T995">pro.h:S</ta>
            <ta e="T997" id="Seg_7248" s="T996">v:pred 0.3.h:S</ta>
            <ta e="T999" id="Seg_7249" s="T998">pro.h:S</ta>
            <ta e="T1000" id="Seg_7250" s="T999">v:pred</ta>
            <ta e="T1003" id="Seg_7251" s="T1002">v:pred 0.1.h:S</ta>
            <ta e="T1005" id="Seg_7252" s="T1004">np:O</ta>
            <ta e="T1006" id="Seg_7253" s="T1005">v:pred 0.1.h:S</ta>
            <ta e="T1010" id="Seg_7254" s="T1009">pro.h:S</ta>
            <ta e="T1011" id="Seg_7255" s="T1010">v:pred</ta>
            <ta e="T1013" id="Seg_7256" s="T1012">v:pred 0.1.h:S</ta>
            <ta e="T1015" id="Seg_7257" s="T1014">v:pred 0.2.h:S</ta>
            <ta e="T1017" id="Seg_7258" s="T1016">pro.h:S</ta>
            <ta e="T1018" id="Seg_7259" s="T1017">v:pred</ta>
            <ta e="T1019" id="Seg_7260" s="T1018">pro.h:S</ta>
            <ta e="T1020" id="Seg_7261" s="T1019">v:pred</ta>
            <ta e="T1021" id="Seg_7262" s="T1020">v:pred 0.2.h:S</ta>
            <ta e="T1023" id="Seg_7263" s="T1022">v:pred 0.2.h:S</ta>
            <ta e="T1025" id="Seg_7264" s="T1024">pro.h:S</ta>
            <ta e="T1026" id="Seg_7265" s="T1025">v:pred</ta>
            <ta e="T1027" id="Seg_7266" s="T1026">np:O</ta>
            <ta e="T1029" id="Seg_7267" s="T1028">np:O</ta>
            <ta e="T1035" id="Seg_7268" s="T1034">v:pred 0.3.h:S</ta>
            <ta e="T1036" id="Seg_7269" s="T1035">s:purp</ta>
            <ta e="T1039" id="Seg_7270" s="T1038">pro.h:S</ta>
            <ta e="T1041" id="Seg_7271" s="T1040">v:pred</ta>
            <ta e="T1044" id="Seg_7272" s="T1043">v:pred 0.3.h:S</ta>
            <ta e="T1047" id="Seg_7273" s="T1046">v:pred 0.3.h:S</ta>
            <ta e="T1049" id="Seg_7274" s="T1048">v:pred 0.1.h:S</ta>
            <ta e="T1054" id="Seg_7275" s="T1052">v:pred 0.1.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST-PKZ" />
         <annotation name="BOR" tierref="BOR-PKZ">
            <ta e="T85" id="Seg_7276" s="T84">RUS:gram</ta>
            <ta e="T89" id="Seg_7277" s="T88">TURK:cult</ta>
            <ta e="T105" id="Seg_7278" s="T104">TURK:cult</ta>
            <ta e="T114" id="Seg_7279" s="T113">TURK:cult</ta>
            <ta e="T117" id="Seg_7280" s="T116">RUS:gram</ta>
            <ta e="T121" id="Seg_7281" s="T120">RUS:gram</ta>
            <ta e="T122" id="Seg_7282" s="T121">TURK:cult</ta>
            <ta e="T372" id="Seg_7283" s="T371">TURK:cult</ta>
            <ta e="T389" id="Seg_7284" s="T388">RUS:cult</ta>
            <ta e="T394" id="Seg_7285" s="T393">TURK:disc</ta>
            <ta e="T398" id="Seg_7286" s="T397">TURK:core</ta>
            <ta e="T405" id="Seg_7287" s="T404">TURK:disc</ta>
            <ta e="T409" id="Seg_7288" s="T408">RUS:gram</ta>
            <ta e="T418" id="Seg_7289" s="T417">RUS:cult</ta>
            <ta e="T427" id="Seg_7290" s="T426">RUS:gram</ta>
            <ta e="T428" id="Seg_7291" s="T427">RUS:cult</ta>
            <ta e="T433" id="Seg_7292" s="T432">RUS:mod</ta>
            <ta e="T435" id="Seg_7293" s="T434">RUS:calq</ta>
            <ta e="T436" id="Seg_7294" s="T435">RUS:cult</ta>
            <ta e="T439" id="Seg_7295" s="T438">%TURK:core</ta>
            <ta e="T441" id="Seg_7296" s="T440">RUS:mod</ta>
            <ta e="T443" id="Seg_7297" s="T442">TURK:core</ta>
            <ta e="T452" id="Seg_7298" s="T451">RUS:gram</ta>
            <ta e="T470" id="Seg_7299" s="T469">RUS:mod</ta>
            <ta e="T475" id="Seg_7300" s="T474">RUS:gram</ta>
            <ta e="T480" id="Seg_7301" s="T479">RUS:mod</ta>
            <ta e="T481" id="Seg_7302" s="T480">TURK:disc</ta>
            <ta e="T488" id="Seg_7303" s="T487">TURK:core</ta>
            <ta e="T967" id="Seg_7304" s="T966">RUS:mod</ta>
            <ta e="T972" id="Seg_7305" s="T971">RUS:gram</ta>
            <ta e="T979" id="Seg_7306" s="T978">RUS:gram</ta>
            <ta e="T986" id="Seg_7307" s="T985">RUS:mod</ta>
            <ta e="T991" id="Seg_7308" s="T990">RUS:gram</ta>
            <ta e="T996" id="Seg_7309" s="T995">TURK:gram(INDEF)</ta>
            <ta e="T1001" id="Seg_7310" s="T1000">RUS:gram</ta>
            <ta e="T1007" id="Seg_7311" s="T1006">%TURK:core</ta>
            <ta e="T1012" id="Seg_7312" s="T1011">RUS:cult</ta>
            <ta e="T1014" id="Seg_7313" s="T1013">RUS:disc</ta>
            <ta e="T1037" id="Seg_7314" s="T1036">RUS:cult</ta>
            <ta e="T1048" id="Seg_7315" s="T1047">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-PKZ" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-PKZ">
            <ta e="T1037" id="Seg_7316" s="T1036">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS-PKZ">
            <ta e="T8" id="Seg_7317" s="T3">RUS:ext</ta>
            <ta e="T20" id="Seg_7318" s="T8">RUS:ext</ta>
            <ta e="T40" id="Seg_7319" s="T20">RUS:ext</ta>
            <ta e="T49" id="Seg_7320" s="T40">RUS:ext</ta>
            <ta e="T64" id="Seg_7321" s="T49">RUS:ext</ta>
            <ta e="T84" id="Seg_7322" s="T64">RUS:ext</ta>
            <ta e="T149" id="Seg_7323" s="T147">RUS:ext</ta>
            <ta e="T156" id="Seg_7324" s="T153">RUS:ext</ta>
            <ta e="T161" id="Seg_7325" s="T160">RUS:ext</ta>
            <ta e="T168" id="Seg_7326" s="T164">RUS:ext</ta>
            <ta e="T177" id="Seg_7327" s="T172">RUS:ext</ta>
            <ta e="T196" id="Seg_7328" s="T189">RUS:ext</ta>
            <ta e="T220" id="Seg_7329" s="T214">RUS:ext</ta>
            <ta e="T236" id="Seg_7330" s="T232">RUS:ext</ta>
            <ta e="T262" id="Seg_7331" s="T248">RUS:ext</ta>
            <ta e="T268" id="Seg_7332" s="T262">RUS:ext</ta>
            <ta e="T272" id="Seg_7333" s="T268">RUS:ext</ta>
            <ta e="T274" id="Seg_7334" s="T272">RUS:ext</ta>
            <ta e="T284" id="Seg_7335" s="T274">RUS:ext</ta>
            <ta e="T296" id="Seg_7336" s="T284">RUS:ext</ta>
            <ta e="T301" id="Seg_7337" s="T296">RUS:ext</ta>
            <ta e="T306" id="Seg_7338" s="T301">RUS:ext</ta>
            <ta e="T310" id="Seg_7339" s="T306">RUS:ext</ta>
            <ta e="T317" id="Seg_7340" s="T310">RUS:ext</ta>
            <ta e="T327" id="Seg_7341" s="T317">RUS:ext</ta>
            <ta e="T328" id="Seg_7342" s="T327">RUS:ext</ta>
            <ta e="T346" id="Seg_7343" s="T328">RUS:ext</ta>
            <ta e="T350" id="Seg_7344" s="T346">RUS:ext</ta>
            <ta e="T355" id="Seg_7345" s="T350">RUS:ext</ta>
            <ta e="T360" id="Seg_7346" s="T355">RUS:ext</ta>
            <ta e="T367" id="Seg_7347" s="T360">RUS:ext</ta>
            <ta e="T574" id="Seg_7348" s="T565">RUS:ext</ta>
            <ta e="T576" id="Seg_7349" s="T574">RUS:ext</ta>
            <ta e="T583" id="Seg_7350" s="T576">RUS:ext</ta>
            <ta e="T585" id="Seg_7351" s="T583">RUS:ext</ta>
            <ta e="T597" id="Seg_7352" s="T585">RUS:ext</ta>
            <ta e="T603" id="Seg_7353" s="T597">RUS:ext</ta>
            <ta e="T609" id="Seg_7354" s="T603">RUS:ext</ta>
            <ta e="T616" id="Seg_7355" s="T609">RUS:ext</ta>
            <ta e="T624" id="Seg_7356" s="T616">RUS:ext</ta>
            <ta e="T629" id="Seg_7357" s="T624">RUS:ext</ta>
            <ta e="T633" id="Seg_7358" s="T629">RUS:ext</ta>
            <ta e="T647" id="Seg_7359" s="T633">RUS:ext</ta>
            <ta e="T654" id="Seg_7360" s="T647">RUS:ext</ta>
            <ta e="T667" id="Seg_7361" s="T654">RUS:ext</ta>
            <ta e="T673" id="Seg_7362" s="T667">RUS:ext</ta>
            <ta e="T682" id="Seg_7363" s="T673">RUS:ext</ta>
            <ta e="T696" id="Seg_7364" s="T682">RUS:ext</ta>
            <ta e="T700" id="Seg_7365" s="T696">RUS:ext</ta>
            <ta e="T714" id="Seg_7366" s="T700">RUS:ext</ta>
            <ta e="T725" id="Seg_7367" s="T714">RUS:ext</ta>
            <ta e="T733" id="Seg_7368" s="T725">RUS:ext</ta>
            <ta e="T743" id="Seg_7369" s="T733">RUS:ext</ta>
            <ta e="T755" id="Seg_7370" s="T743">RUS:ext</ta>
            <ta e="T763" id="Seg_7371" s="T755">RUS:ext</ta>
            <ta e="T772" id="Seg_7372" s="T763">RUS:ext</ta>
            <ta e="T781" id="Seg_7373" s="T772">RUS:ext</ta>
            <ta e="T800" id="Seg_7374" s="T781">RUS:ext</ta>
            <ta e="T822" id="Seg_7375" s="T800">RUS:ext</ta>
            <ta e="T832" id="Seg_7376" s="T822">RUS:ext</ta>
            <ta e="T848" id="Seg_7377" s="T832">RUS:ext</ta>
            <ta e="T850" id="Seg_7378" s="T848">RUS:ext</ta>
            <ta e="T856" id="Seg_7379" s="T850">RUS:ext</ta>
            <ta e="T861" id="Seg_7380" s="T856">RUS:ext</ta>
            <ta e="T876" id="Seg_7381" s="T864">RUS:ext</ta>
            <ta e="T885" id="Seg_7382" s="T878">RUS:ext</ta>
            <ta e="T891" id="Seg_7383" s="T885">RUS:ext</ta>
            <ta e="T894" id="Seg_7384" s="T892">RUS:ext</ta>
            <ta e="T938" id="Seg_7385" s="T928">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-PKZ">
            <ta e="T90" id="Seg_7386" s="T84">А нам не давали молиться Богу.</ta>
            <ta e="T95" id="Seg_7387" s="T90">Тогда [кто-то] написал письмо [из] Америки.</ta>
            <ta e="T107" id="Seg_7388" s="T95">"Дайте(?) мне людей, которые молятся Богу".</ta>
            <ta e="T110" id="Seg_7389" s="T107">Потом пришло письмо.</ta>
            <ta e="T116" id="Seg_7390" s="T110">Те, кто ругал Бога, тоже написали.</ta>
            <ta e="T123" id="Seg_7391" s="T116">Тоже написали, что "я молюсь Богу".</ta>
            <ta e="T124" id="Seg_7392" s="T123">Хватит.</ta>
            <ta e="T262" id="Seg_7393" s="T248">Когда я на конгресс пришла, все так рады были. </ta>
            <ta e="T268" id="Seg_7394" s="T262">Все люди хорошие, приняли меня. </ta>
            <ta e="T377" id="Seg_7395" s="T368">Я утром встала, Богу помолилась, подождала Агафона Ивановича.</ta>
            <ta e="T380" id="Seg_7396" s="T377">Он пришел ко мне.</ta>
            <ta e="T384" id="Seg_7397" s="T380">Потом я с ним пошла.</ta>
            <ta e="T389" id="Seg_7398" s="T384">Пришла туда, на эту конференцию.</ta>
            <ta e="T397" id="Seg_7399" s="T389">Много людей, все очень красивые.</ta>
            <ta e="T398" id="Seg_7400" s="T397">Хорошие.</ta>
            <ta e="T403" id="Seg_7401" s="T398">Писать очень хорошо умеют, очень (умные?).</ta>
            <ta e="T407" id="Seg_7402" s="T403">Они меня целовали.</ta>
            <ta e="T408" id="Seg_7403" s="T407">Женщины.</ta>
            <ta e="T413" id="Seg_7404" s="T408">А мужчины пожимали руку.</ta>
            <ta e="T430" id="Seg_7405" s="T413">Потом я оттуда ушла, села в машину, была там, где пишут и книжки где (ставят?).</ta>
            <ta e="T436" id="Seg_7406" s="T430">Там тоже меня сфотографировали.</ta>
            <ta e="T444" id="Seg_7407" s="T436">Я там сидела, разговаривала, там тоже очень хорошие люди.</ta>
            <ta e="T454" id="Seg_7408" s="T444">Потом я сказала им: "Бог с вами", и (ушла?)</ta>
            <ta e="T463" id="Seg_7409" s="T454">Потом я была наверху, потом мы стояли, на землю (?).</ta>
            <ta e="T468" id="Seg_7410" s="T463">Потом мы пошли туда, где кресты [= на кладбище].</ta>
            <ta e="T471" id="Seg_7411" s="T468">Там мы тоже ходили.</ta>
            <ta e="T474" id="Seg_7412" s="T471">Было много женщин.</ta>
            <ta e="T479" id="Seg_7413" s="T474">И было двое мужчин.</ta>
            <ta e="T486" id="Seg_7414" s="T479">Тоже они целовали [меня] и жали руку.</ta>
            <ta e="T490" id="Seg_7415" s="T486">Они говорят: "Ты очень хорошая".</ta>
            <ta e="T492" id="Seg_7416" s="T490">Ты приехала [из]далека?"</ta>
            <ta e="T496" id="Seg_7417" s="T492">Я сказала: "Издалека приехала.</ta>
            <ta e="T499" id="Seg_7418" s="T496">Я живу рядом с (Монголией?)".</ta>
            <ta e="T956" id="Seg_7419" s="T943">Вчера мы с Фридой все ждали моего брата, [его] все нет и нет, потом…</ta>
            <ta e="T962" id="Seg_7420" s="T956">Был шум железной дорогие, я говорю: "Он едет".</ta>
            <ta e="T974" id="Seg_7421" s="T962">Он приехал; может быть, он пошел к матери, там поел и лег.</ta>
            <ta e="T976" id="Seg_7422" s="T974">(…) спать.</ta>
            <ta e="T985" id="Seg_7423" s="T976">Потом мы (сели?), (принесли?) легли спать.</ta>
            <ta e="T992" id="Seg_7424" s="T985">Только легли, и я (сразу?) заснула.</ta>
            <ta e="T996" id="Seg_7425" s="T992">Ну, днем кто-то пришел.</ta>
            <ta e="T1003" id="Seg_7426" s="T996">(…) пришел; я вскочила и побежала туда.</ta>
            <ta e="T1006" id="Seg_7427" s="T1003">Пожала ему руку.</ta>
            <ta e="T1013" id="Seg_7428" s="T1006">Мы поговорили, он говорит: "Я пойду в ванную помоюсь".</ta>
            <ta e="T1015" id="Seg_7429" s="T1013">"Ну, иди".</ta>
            <ta e="T1018" id="Seg_7430" s="T1015">Он пошел.</ta>
            <ta e="T1023" id="Seg_7431" s="T1018">Я говорю: "Поешь, потом пойдешь".</ta>
            <ta e="T1037" id="Seg_7432" s="T1023">Потом он(а) согрел(а) суп, чай, он поел, потом пошел мыться в ванную.</ta>
            <ta e="T1042" id="Seg_7433" s="T1037">Потом он(а) ему постелил(а). [?]</ta>
            <ta e="T1049" id="Seg_7434" s="T1042">Там поставил(а) кровать, он лег, и мы уснули.</ta>
            <ta e="T1054" id="Seg_7435" s="T1049">Утром мы рано проснулись.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-PKZ">
            <ta e="T8" id="Seg_7436" s="T3">An American mentor sent us a paper.</ta>
            <ta e="T20" id="Seg_7437" s="T8">In our area they drove away (all) believers, they were very oppressed, were not allowed to pray.</ta>
            <ta e="T40" id="Seg_7438" s="T20">Well, he heard that oppression in our Russian land was like that, he began to ask our ruler: "Give me the people of faith."</ta>
            <ta e="T49" id="Seg_7439" s="T40">Well, the letter came, even to us in Abalakov.</ta>
            <ta e="T64" id="Seg_7440" s="T49">They began to write, and those who did not believe and scolded God, and they went on record that they were believers.</ta>
            <ta e="T84" id="Seg_7441" s="T64">Our mentor (glanced) watched that all people became believers, and did not give his people to America.</ta>
            <ta e="T90" id="Seg_7442" s="T84">We were not allowed to pray to God.</ta>
            <ta e="T95" id="Seg_7443" s="T90">Then [someone] wrote a letter [from] America.</ta>
            <ta e="T107" id="Seg_7444" s="T95">"Give(?) me the people that pray to God."</ta>
            <ta e="T110" id="Seg_7445" s="T107">Then the letter came.</ta>
            <ta e="T116" id="Seg_7446" s="T110">Those, who scolded God, wrote, too.</ta>
            <ta e="T123" id="Seg_7447" s="T116">[They] also wrote, that I [= they] prayed to God.</ta>
            <ta e="T124" id="Seg_7448" s="T123">Enough.</ta>
            <ta e="T149" id="Seg_7449" s="T147">I can tell…</ta>
            <ta e="T156" id="Seg_7450" s="T153">I can.</ta>
            <ta e="T161" id="Seg_7451" s="T160">What?</ta>
            <ta e="T168" id="Seg_7452" s="T164">Well, it was okay.</ta>
            <ta e="T177" id="Seg_7453" s="T172">All are good, they welcomed me well.</ta>
            <ta e="T196" id="Seg_7454" s="T189">Everyone hugged me, they even kissed me.</ta>
            <ta e="T220" id="Seg_7455" s="T214">Well, tell in Russian?</ta>
            <ta e="T236" id="Seg_7456" s="T232">He speaks already?</ta>
            <ta e="T262" id="Seg_7457" s="T248">When I came to the congress, everyone was so happy.</ta>
            <ta e="T268" id="Seg_7458" s="T262">All people are good, accepted me.</ta>
            <ta e="T272" id="Seg_7459" s="T268">All scientists, smart people.</ta>
            <ta e="T274" id="Seg_7460" s="T272">Interesting, beautiful.</ta>
            <ta e="T284" id="Seg_7461" s="T274">Well, my most beautiful is Agafon Ivanovich, I love him very much.</ta>
            <ta e="T296" id="Seg_7462" s="T284">He is my student, I taught him in my own dialect.</ta>
            <ta e="T306" id="Seg_7463" s="T301">And (now) I went to the cemetery.</ta>
            <ta e="T310" id="Seg_7464" s="T306">It was good there too.</ta>
            <ta e="T317" id="Seg_7465" s="T310">And there I sang in my own dialect.</ta>
            <ta e="T327" id="Seg_7466" s="T317">Divine prayers, everyone was also so glad, everyone welcomed me.</ta>
            <ta e="T328" id="Seg_7467" s="T327">They invited me.</ta>
            <ta e="T346" id="Seg_7468" s="T328">There I saw a lot of squirrels running around in the cemetery, we don’t have them there.</ta>
            <ta e="T350" id="Seg_7469" s="T346">Well, our hunters are killing them.</ta>
            <ta e="T355" id="Seg_7470" s="T350">And here they don’t kill squirrels.</ta>
            <ta e="T360" id="Seg_7471" s="T355">I fed them.</ta>
            <ta e="T367" id="Seg_7472" s="T360">Well, now I have to say it my own way.</ta>
            <ta e="T377" id="Seg_7473" s="T368">In the morning I got up, prayed to God, waited for Agafon Ivanovich.</ta>
            <ta e="T380" id="Seg_7474" s="T377">He came to me.</ta>
            <ta e="T384" id="Seg_7475" s="T380">Then I went with him.</ta>
            <ta e="T389" id="Seg_7476" s="T384">I came here, to this conference.</ta>
            <ta e="T397" id="Seg_7477" s="T389">There are a lot of people, everybody are beautiful.</ta>
            <ta e="T398" id="Seg_7478" s="T397">[They are] good.</ta>
            <ta e="T403" id="Seg_7479" s="T398">They can write very well, they are very (clever?).</ta>
            <ta e="T407" id="Seg_7480" s="T403">They [were] kissing me.</ta>
            <ta e="T408" id="Seg_7481" s="T407">Women.</ta>
            <ta e="T413" id="Seg_7482" s="T408">And the men [were] shaking hands.</ta>
            <ta e="T430" id="Seg_7483" s="T413">Then I went from there, took a car and (was?) where [people] write and where books are (stored?).</ta>
            <ta e="T436" id="Seg_7484" s="T430">There, too, they made a picture of me.</ta>
            <ta e="T444" id="Seg_7485" s="T436">I was sitting and talking there, people there are very good, too.</ta>
            <ta e="T454" id="Seg_7486" s="T444">Then I said them: "God bless you", and went (off?).</ta>
            <ta e="T463" id="Seg_7487" s="T454">Then I was above, then we stood (?).</ta>
            <ta e="T468" id="Seg_7488" s="T463">Then we went where crosses are [= to the cemetery].</ta>
            <ta e="T471" id="Seg_7489" s="T468">We walked there, too.</ta>
            <ta e="T474" id="Seg_7490" s="T471">There were many women.</ta>
            <ta e="T479" id="Seg_7491" s="T474">And there were two men.</ta>
            <ta e="T486" id="Seg_7492" s="T479">They, too, kissed [me] and shook hands.</ta>
            <ta e="T490" id="Seg_7493" s="T486">They say: "You are very good.</ta>
            <ta e="T492" id="Seg_7494" s="T490">Did you come [from] far away?"</ta>
            <ta e="T496" id="Seg_7495" s="T492">I said: "I came from far away.</ta>
            <ta e="T499" id="Seg_7496" s="T496">I live near Mongolia." [?]</ta>
            <ta e="T574" id="Seg_7497" s="T565">I and Rida waited, waited for Arpad.</ta>
            <ta e="T576" id="Seg_7498" s="T574">No, no.</ta>
            <ta e="T583" id="Seg_7499" s="T576">Then she went to the mother-in-law of Evona.</ta>
            <ta e="T585" id="Seg_7500" s="T583">"I'll go find out."</ta>
            <ta e="T597" id="Seg_7501" s="T585">But I don’t let her go: "Don’t go, I say, he’ll come anyway.</ta>
            <ta e="T603" id="Seg_7502" s="T597">Only when he's sick, he won't come.</ta>
            <ta e="T609" id="Seg_7503" s="T603">And then he will come, he won’t lie.</ta>
            <ta e="T616" id="Seg_7504" s="T609">e is a believer, he cannot lie.</ta>
            <ta e="T624" id="Seg_7505" s="T616">As he came to me, I also wrote:</ta>
            <ta e="T629" id="Seg_7506" s="T624">When Agafon Ivanovich arrives, I’ll come.</ta>
            <ta e="T633" id="Seg_7507" s="T629">He came to me.</ta>
            <ta e="T647" id="Seg_7508" s="T633">Well, once I promised, I didn’t look at anything, packed up and went with him.</ta>
            <ta e="T654" id="Seg_7509" s="T647">We arrived in Aginsk, there are also our believers.</ta>
            <ta e="T667" id="Seg_7510" s="T654">There I offered him, I say: "Come to us, we will buy you a house here."</ta>
            <ta e="T673" id="Seg_7511" s="T667">Well, we are five hours from here…</ta>
            <ta e="T682" id="Seg_7512" s="T673">We were also led by fellow [female] believers, we arrived in Uyar.</ta>
            <ta e="T696" id="Seg_7513" s="T682">Well, he, of course, did not know where, but I know my believers there.</ta>
            <ta e="T700" id="Seg_7514" s="T696">Well, we went to them.</ta>
            <ta e="T714" id="Seg_7515" s="T700">I’m asking people, but I forgot their surname, I only know: Ilyusha and Marusya.</ta>
            <ta e="T725" id="Seg_7516" s="T714">But I know the place, I have been there. </ta>
            <ta e="T733" id="Seg_7517" s="T725">I say to Arpad: "You wait here, and I will come."</ta>
            <ta e="T743" id="Seg_7518" s="T733">I go into the first room.</ta>
            <ta e="T755" id="Seg_7519" s="T743">“Don’t come in, here,” he says, “no one lives here.”</ta>
            <ta e="T763" id="Seg_7520" s="T755">Then I went further, asked the people there: </ta>
            <ta e="T772" id="Seg_7521" s="T763">I say: "Do you know Ilyusha and Marusya?"</ta>
            <ta e="T781" id="Seg_7522" s="T772">He says: “They work with me,” and he led me.</ta>
            <ta e="T800" id="Seg_7523" s="T781">And I said to my brother: "Come here," and he came, and just she comes here with water.</ta>
            <ta e="T822" id="Seg_7524" s="T800">And we also talked for a long time, there another brother of ours came, the same as brother Arpad, I often go there.</ta>
            <ta e="T832" id="Seg_7525" s="T822">Because in our village there are people like me.</ta>
            <ta e="T848" id="Seg_7526" s="T832">Well, do I go to Krasnoyarsk, and there we still have a crossing, I go there.</ta>
            <ta e="T850" id="Seg_7527" s="T848">Zaozerka station.</ta>
            <ta e="T856" id="Seg_7528" s="T850">You probably could have been there already?</ta>
            <ta e="T861" id="Seg_7529" s="T856">Have you been to Zaozerki station?</ta>
            <ta e="T876" id="Seg_7530" s="T864">From there, you can also go to Krasnoyarsk, and to Kansk, to the city of Kansk.</ta>
            <ta e="T885" id="Seg_7531" s="T878">Well, I was in Kansk there too.</ta>
            <ta e="T891" id="Seg_7532" s="T885">There are also believers there.</ta>
            <ta e="T894" id="Seg_7533" s="T892">Well…</ta>
            <ta e="T938" id="Seg_7534" s="T928">I said a lot in Russian already.</ta>
            <ta e="T956" id="Seg_7535" s="T943">Yesterday, Frida and me, we were waiting for [my] brother, he was not coming, then…</ta>
            <ta e="T962" id="Seg_7536" s="T956">There was a noise of a train, I say: "He's coming."</ta>
            <ta e="T974" id="Seg_7537" s="T962">He came; maybe he went to his mother, ate there and lay down.</ta>
            <ta e="T976" id="Seg_7538" s="T974">(…) to sleep.</ta>
            <ta e="T985" id="Seg_7539" s="T976">Then we sat (down?), we gave (?) and we went to bed.</ta>
            <ta e="T992" id="Seg_7540" s="T985">We lay down, and I feel asleep right (away?).</ta>
            <ta e="T996" id="Seg_7541" s="T992">At noon somebody came.</ta>
            <ta e="T1003" id="Seg_7542" s="T996">(…) came; I jumped up and ran there.</ta>
            <ta e="T1006" id="Seg_7543" s="T1003">I shook his hand.</ta>
            <ta e="T1013" id="Seg_7544" s="T1006">We talked a while, [then] he says: "I'm [going] to wash myself, I'm going to the bath."</ta>
            <ta e="T1015" id="Seg_7545" s="T1013">"Well, go."</ta>
            <ta e="T1018" id="Seg_7546" s="T1015">He went.</ta>
            <ta e="T1023" id="Seg_7547" s="T1018">I said: "Eat, then you'll go."</ta>
            <ta e="T1037" id="Seg_7548" s="T1023">Then s/he warmed up soup, tea, he ate, then he came to wash himself to the bath.</ta>
            <ta e="T1042" id="Seg_7549" s="T1037">Then s/he (made?) him a bed. [?]</ta>
            <ta e="T1049" id="Seg_7550" s="T1042">S/he placed a bed there, he lay down, and we fell asleep.</ta>
            <ta e="T1054" id="Seg_7551" s="T1049">In the morning we got up again…</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PKZ">
            <ta e="T8" id="Seg_7552" s="T3">Ein amerikanischer (Lehrmeister?) hat uns das Brief geschrieben.</ta>
            <ta e="T20" id="Seg_7553" s="T8">In unserer Gegend wurden alle Gläubigen verfolgt, sie waren sehr unterdrückt und durften nicht beten.</ta>
            <ta e="T40" id="Seg_7554" s="T20">Nun, er hörte, dass die Bedrägnis in unserer russischen Land war so, er hat angefangen unseren Herrscher zu fragen: "Gib mir die Gläubigen." </ta>
            <ta e="T49" id="Seg_7555" s="T40">Nun, der Brief kam auch bei ins in Ablakove an.</ta>
            <ta e="T64" id="Seg_7556" s="T49">Sie begannen zu schreiben, und die, die nicht glaubten und Gott verfluchten, unterschrieben, dass sie Gläubige sein.</ta>
            <ta e="T84" id="Seg_7557" s="T64">Unser Lehrmeister schaute, dass alle Leute Gläubige wurden und gab sein Volk nicht nach Amerika. </ta>
            <ta e="T90" id="Seg_7558" s="T84">Wir durften nicht zu Gott beten.</ta>
            <ta e="T95" id="Seg_7559" s="T90">Dann schrieb [jemand] einen Brief [aus] Amerika.</ta>
            <ta e="T107" id="Seg_7560" s="T95">"Gib(?) mir die Leute, die zu Gott beten."</ta>
            <ta e="T110" id="Seg_7561" s="T107">Dann kam der Brief.</ta>
            <ta e="T116" id="Seg_7562" s="T110">Jene, die Gott verdammten, schrieben auch.</ta>
            <ta e="T123" id="Seg_7563" s="T116">[Sie] schrieben auch, dass ich [=sie] zu Gott bete[n].</ta>
            <ta e="T124" id="Seg_7564" s="T123">Genug.</ta>
            <ta e="T149" id="Seg_7565" s="T147">Ich kann erzählen…</ta>
            <ta e="T156" id="Seg_7566" s="T153">Ich kann das.</ta>
            <ta e="T161" id="Seg_7567" s="T160">Wie?</ta>
            <ta e="T168" id="Seg_7568" s="T164">Nun, einigermaßen.</ta>
            <ta e="T177" id="Seg_7569" s="T172">Alles gut, sie haben mich gut aufgenommen.</ta>
            <ta e="T196" id="Seg_7570" s="T189">Alle haben mich umarmt, sogar geküsst.</ta>
            <ta e="T220" id="Seg_7571" s="T214">Nun also, auf Russisch erzählen?</ta>
            <ta e="T236" id="Seg_7572" s="T232">[Lit:] Spricht er schon?</ta>
            <ta e="T262" id="Seg_7573" s="T248">Wenn ich auf den Kongress kam, waren alle glücklich.</ta>
            <ta e="T268" id="Seg_7574" s="T262">Alle Leute sind gut, akzeptieren mich.</ta>
            <ta e="T272" id="Seg_7575" s="T268">Alle Wissenschaftler, kluge Leute.</ta>
            <ta e="T274" id="Seg_7576" s="T272">Interessant, schön.</ta>
            <ta e="T284" id="Seg_7577" s="T274">Nun, am schönsten von allen war mein Agafon Ivanovich, ich liebe ihn sehr.</ta>
            <ta e="T296" id="Seg_7578" s="T284">Er ist mein Schüler, ich habe ihm meinen Dialekt beigebracht.</ta>
            <ta e="T306" id="Seg_7579" s="T301">Und dann ging ich auf den Friedhof.</ta>
            <ta e="T310" id="Seg_7580" s="T306">Das war auch gut.</ta>
            <ta e="T317" id="Seg_7581" s="T310">Und dort habe ich auf meinem Dialekt gesungen.</ta>
            <ta e="T327" id="Seg_7582" s="T317">Gebete, hier waren auch alle froh, alle begrüßten mich.</ta>
            <ta e="T328" id="Seg_7583" s="T327">Luden mich ein.</ta>
            <ta e="T346" id="Seg_7584" s="T328">Dort habe ich viele Eichhörnchen gesehen, die dort rumliefen, bei uns gibt es sie nicht.</ta>
            <ta e="T350" id="Seg_7585" s="T346">Nun, unsere Jäger töten sie.</ta>
            <ta e="T355" id="Seg_7586" s="T350">Aber hier töten sie keine Eichhörnchen.</ta>
            <ta e="T360" id="Seg_7587" s="T355">Ich gab ihnen Essen.</ta>
            <ta e="T367" id="Seg_7588" s="T360">Nun, jetzt müssen wir es auf unsere Weise sagen.</ta>
            <ta e="T377" id="Seg_7589" s="T368">Am Morgen stand ich auf, betete zu Gott, wartete auf Agafon Iwanowitsch.</ta>
            <ta e="T380" id="Seg_7590" s="T377">Er kam zu mir.</ta>
            <ta e="T384" id="Seg_7591" s="T380">Dann ging ich mit ihm.</ta>
            <ta e="T389" id="Seg_7592" s="T384">Ich kam hierher, zu dieser Konferenz.</ta>
            <ta e="T397" id="Seg_7593" s="T389">Dort sind viele Leute, alle sind schön.</ta>
            <ta e="T398" id="Seg_7594" s="T397">[Sie sind] gut.</ta>
            <ta e="T403" id="Seg_7595" s="T398">Sie können sehr gut schreiben, sie sind sehr (klug?).</ta>
            <ta e="T407" id="Seg_7596" s="T403">Sie küssten mich.</ta>
            <ta e="T408" id="Seg_7597" s="T407">Frauen.</ta>
            <ta e="T413" id="Seg_7598" s="T408">Und die Männer schüttelten die Hand.</ta>
            <ta e="T430" id="Seg_7599" s="T413">Dann ging ich weg von dort, stieg in ein Auto und (war?) wo [Leute] schreiben und wo Bücher (aufbewahrt?) werden.</ta>
            <ta e="T436" id="Seg_7600" s="T430">Dort machten sie auch ein Bild von mir.</ta>
            <ta e="T444" id="Seg_7601" s="T436">Ich saß dort und unterhielt mich, die Leute dort sind auch sehr gut.</ta>
            <ta e="T454" id="Seg_7602" s="T444">Dann sagte ich ihnen: "Gott segne euch", und ging (weg?).</ta>
            <ta e="T463" id="Seg_7603" s="T454">Dann war ich oben, dann standen wir (?).</ta>
            <ta e="T468" id="Seg_7604" s="T463">Dann gingen wir dorthin, wo Kreuze sind [= auf den Friedhof].</ta>
            <ta e="T471" id="Seg_7605" s="T468">Wir gingen auch dorthin.</ta>
            <ta e="T474" id="Seg_7606" s="T471">Dort waren viele Frauen.</ta>
            <ta e="T479" id="Seg_7607" s="T474">Und dort waren zwei Männer.</ta>
            <ta e="T486" id="Seg_7608" s="T479">Sie küssten [mich] aus und schüttelten die Hand.</ta>
            <ta e="T490" id="Seg_7609" s="T486">Sie sagen: "Du bist sehr gut.</ta>
            <ta e="T492" id="Seg_7610" s="T490">Bist du [von] weit weg gekommen?"</ta>
            <ta e="T496" id="Seg_7611" s="T492">Ich sagte: "Ich bin von weit weg gekommen.</ta>
            <ta e="T499" id="Seg_7612" s="T496">Ich lebe in der Nähe der Mongolei." [?]</ta>
            <ta e="T574" id="Seg_7613" s="T565">Ich und (Rida?) warten auf Arpad.</ta>
            <ta e="T576" id="Seg_7614" s="T574">Nein, nein.</ta>
            <ta e="T583" id="Seg_7615" s="T576">Dann ging sie zur Schwiegermutter von Evona.</ta>
            <ta e="T585" id="Seg_7616" s="T583">"Ich werde es herausfinden."</ta>
            <ta e="T597" id="Seg_7617" s="T585">Aber ich lasse sie nicht gehen: "Geh nicht", sag ich, "Er wird gleich kommen.</ta>
            <ta e="T603" id="Seg_7618" s="T597">Nur wenn er krank ist, kommt er nicht.</ta>
            <ta e="T609" id="Seg_7619" s="T603">Und dann kommt er, er lügt nicht.</ta>
            <ta e="T616" id="Seg_7620" s="T609">Er ist ein Gläubiger, er kann nicht lügen.</ta>
            <ta e="T624" id="Seg_7621" s="T616">Als er zu mir kam, schrieb ich auch:</ta>
            <ta e="T629" id="Seg_7622" s="T624">Wenn Agafon Iwanowitsch kommt, werde ich kommen.</ta>
            <ta e="T633" id="Seg_7623" s="T629">Er kam zu mir.</ta>
            <ta e="T647" id="Seg_7624" s="T633">Nun, ich habe es versprochen, ich habe zu nichts gesehen, habe gepackt und bin mit ihm gegangen.</ta>
            <ta e="T654" id="Seg_7625" s="T647">Wir kamen in Aginsk an, auch unsere Gläubigen.</ta>
            <ta e="T667" id="Seg_7626" s="T654">Dort bot ich ihm an, ich sagte: "Komm zu uns, wir kaufen dir hier ein Haus."</ta>
            <ta e="T673" id="Seg_7627" s="T667">Nun, wir sind fünf Stunden von hier…</ta>
            <ta e="T682" id="Seg_7628" s="T673">Wir wurden auch von Glaubensschwestern begleitet, wir kamen in Uyar an.</ta>
            <ta e="T696" id="Seg_7629" s="T682">Nun, er wusste natürlich nicht wo, aber ich kenne die Gläubigen dort.</ta>
            <ta e="T700" id="Seg_7630" s="T696">Nun, wir gingen zu ihnen.</ta>
            <ta e="T714" id="Seg_7631" s="T700">Wir fragten Leute, aber ich habe ihre Nachnamen vergessen, ich weiß nur: Ilya und Marusya.</ta>
            <ta e="T725" id="Seg_7632" s="T714">Aber ich kenne den Ort, offensichtlich war ich dort.</ta>
            <ta e="T733" id="Seg_7633" s="T725">Ich sage zu Arpad: "Sie bleiben hier, ich gehe hinein."</ta>
            <ta e="T743" id="Seg_7634" s="T733">Ich gehe in den ersten Raum.</ta>
            <ta e="T755" id="Seg_7635" s="T743">"Komm nicht hier rein", sagt er, "niemand lebt hier."</ta>
            <ta e="T763" id="Seg_7636" s="T755">Dann ging ich weiter, fragte die Leute dort.</ta>
            <ta e="T772" id="Seg_7637" s="T763">Ich sage: "Kennt ihr Ilja und Marusya?"</ta>
            <ta e="T781" id="Seg_7638" s="T772">Er sagt: "Sie arbeiten mit mir" und er führte mich.</ta>
            <ta e="T800" id="Seg_7639" s="T781">Und ich sagte zu meinem Bruder: "Komm her", und er kam, und eben geht sie hier mit Wasser.</ta>
            <ta e="T822" id="Seg_7640" s="T800">Und wir haben uns auch lange hier unterhalten, ein anderer unserer Brüder kam dorthin, genau wie Bruder Arpad. Ich gehe oft dorthin.</ta>
            <ta e="T832" id="Seg_7641" s="T822">Denn in unserem Dorf gibt es keine solchen Leute wie mich.</ta>
            <ta e="T848" id="Seg_7642" s="T832">Nun, ich gehe nach Krasnojarsk, und bei uns gibt es da noch eine Abfahrt, dorthin gehe ich.</ta>
            <ta e="T850" id="Seg_7643" s="T848">Zaozerka-Station.</ta>
            <ta e="T856" id="Seg_7644" s="T850">Du warst vielleicht schon einmal dort?</ta>
            <ta e="T861" id="Seg_7645" s="T856">Bist du nicht bei der Zaozerka-Station gewesen?</ta>
            <ta e="T876" id="Seg_7646" s="T864">Von dort können sie auch nach Krasnojarsk fahren, nach Kansk, in die Stadt Kansk. </ta>
            <ta e="T885" id="Seg_7647" s="T878">Nun, ich war schon in Kansk.</ta>
            <ta e="T891" id="Seg_7648" s="T885">Es gibt auch Gläubige dort.</ta>
            <ta e="T894" id="Seg_7649" s="T892">Also…</ta>
            <ta e="T938" id="Seg_7650" s="T928">Ich habe schon viel auf Russisch gesagt.</ta>
            <ta e="T956" id="Seg_7651" s="T943">Gestern warteten Frida und ich auf [meinen] Bruder, er kam nicht, dann…</ta>
            <ta e="T962" id="Seg_7652" s="T956">Die Eisenbahn lärmte, ich sage: "Er kommt."</ta>
            <ta e="T974" id="Seg_7653" s="T962">Er kam; vielleicht ging er zu seiner Mutter, aß dort und legte sich hin.</ta>
            <ta e="T976" id="Seg_7654" s="T974">(…) um zu schlafen.</ta>
            <ta e="T985" id="Seg_7655" s="T976">Dann setzten wir (uns?), wir gaben (?) und gingen zu Bett.</ta>
            <ta e="T992" id="Seg_7656" s="T985">Wir legten uns hin und schliefen (sofort?) ein.</ta>
            <ta e="T996" id="Seg_7657" s="T992">Am Tag kam jemand.</ta>
            <ta e="T1003" id="Seg_7658" s="T996">(…) kam; ich sprang auf und rannte dorthin.</ta>
            <ta e="T1006" id="Seg_7659" s="T1003">Ich schüttelte seine Hand.</ta>
            <ta e="T1013" id="Seg_7660" s="T1006">Wir sprachen eine Weile, [dann] sagt er: "Ich wasche mich, ich gehe ins Bad."</ta>
            <ta e="T1015" id="Seg_7661" s="T1013">"Gut, geh."</ta>
            <ta e="T1018" id="Seg_7662" s="T1015">Er ging.</ta>
            <ta e="T1023" id="Seg_7663" s="T1018">Ich sagte: "Iss, dann gehst du."</ta>
            <ta e="T1037" id="Seg_7664" s="T1023">Dann wärmte er/sie Suppe und Tee auf, er aß, dann kam er ins Bad, um sich zu waschen.</ta>
            <ta e="T1042" id="Seg_7665" s="T1037">Dann machte er/sie ihm ein Bett. [?]</ta>
            <ta e="T1049" id="Seg_7666" s="T1042">Er/sie machte dort ein Bett, er legte sich hin und wir schliefen ein.</ta>
            <ta e="T1054" id="Seg_7667" s="T1049">Am Morgen standen wir wieder auf…</ta>
         </annotation>
         <annotation name="nt" tierref="nt-PKZ">
            <ta e="T8" id="Seg_7668" s="T3">[GVY:] гумагу</ta>
            <ta e="T90" id="Seg_7669" s="T84">[GVY:] numan (uzu-) üzəsʼtə</ta>
            <ta e="T107" id="Seg_7670" s="T95">[GVY:] Mĭbəleʔ is most probably an erroneous form</ta>
            <ta e="T236" id="Seg_7671" s="T232">[GVY:] = is the recorder already on?</ta>
            <ta e="T301" id="Seg_7672" s="T296">[GVY:] Unclear.</ta>
            <ta e="T360" id="Seg_7673" s="T355">[GVY:] имя = им</ta>
            <ta e="T403" id="Seg_7674" s="T398">[GVY:] Or: "many [of them] are clever"? šagəstə 'silly' is evidently used by mistake.</ta>
            <ta e="T436" id="Seg_7675" s="T430">[GVY:] kartačka means 'snapshot' from the Russian (фото)карточка; however, the whole sentence may also be interpreted as 'they made a picture of me for a [library] card'.</ta>
            <ta e="T454" id="Seg_7676" s="T444">[GVY:] Maybe bosəltə from the root bos- 'self' has to do with the Russian восвояси, but the form is unclear.</ta>
            <ta e="T463" id="Seg_7677" s="T454">[GVY:] Unclear.</ta>
            <ta e="T490" id="Seg_7678" s="T486">[GVY:] = 'You are a good person' or 'You look well'?</ta>
            <ta e="T499" id="Seg_7679" s="T496">[GVY:] Măngoliaʔit?</ta>
            <ta e="T654" id="Seg_7680" s="T647">[GVY:] "в Агинска" = cf. below Красноярска, Ачинска.</ta>
            <ta e="T850" id="Seg_7681" s="T848">[GVY:] Zaozernaya railway station.</ta>
            <ta e="T962" id="Seg_7682" s="T956">[GVY:] küzürnleʔbə</ta>
            <ta e="T985" id="Seg_7683" s="T976">[GVY:] The middle part of the sentence is unclear.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T979" />
            <conversion-tli id="T980" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T985" />
            <conversion-tli id="T986" />
            <conversion-tli id="T987" />
            <conversion-tli id="T988" />
            <conversion-tli id="T989" />
            <conversion-tli id="T990" />
            <conversion-tli id="T991" />
            <conversion-tli id="T992" />
            <conversion-tli id="T993" />
            <conversion-tli id="T994" />
            <conversion-tli id="T995" />
            <conversion-tli id="T996" />
            <conversion-tli id="T997" />
            <conversion-tli id="T998" />
            <conversion-tli id="T999" />
            <conversion-tli id="T1000" />
            <conversion-tli id="T1001" />
            <conversion-tli id="T1002" />
            <conversion-tli id="T1003" />
            <conversion-tli id="T1004" />
            <conversion-tli id="T1005" />
            <conversion-tli id="T1006" />
            <conversion-tli id="T1007" />
            <conversion-tli id="T1008" />
            <conversion-tli id="T1009" />
            <conversion-tli id="T1010" />
            <conversion-tli id="T1011" />
            <conversion-tli id="T1012" />
            <conversion-tli id="T1013" />
            <conversion-tli id="T1014" />
            <conversion-tli id="T1015" />
            <conversion-tli id="T1016" />
            <conversion-tli id="T1017" />
            <conversion-tli id="T1018" />
            <conversion-tli id="T1019" />
            <conversion-tli id="T1020" />
            <conversion-tli id="T1021" />
            <conversion-tli id="T1022" />
            <conversion-tli id="T1023" />
            <conversion-tli id="T1024" />
            <conversion-tli id="T1025" />
            <conversion-tli id="T1026" />
            <conversion-tli id="T1027" />
            <conversion-tli id="T1028" />
            <conversion-tli id="T1029" />
            <conversion-tli id="T1030" />
            <conversion-tli id="T1031" />
            <conversion-tli id="T1032" />
            <conversion-tli id="T1033" />
            <conversion-tli id="T1034" />
            <conversion-tli id="T1035" />
            <conversion-tli id="T1036" />
            <conversion-tli id="T1037" />
            <conversion-tli id="T1038" />
            <conversion-tli id="T1039" />
            <conversion-tli id="T1040" />
            <conversion-tli id="T1041" />
            <conversion-tli id="T1042" />
            <conversion-tli id="T1043" />
            <conversion-tli id="T1044" />
            <conversion-tli id="T1045" />
            <conversion-tli id="T1046" />
            <conversion-tli id="T1047" />
            <conversion-tli id="T1048" />
            <conversion-tli id="T1049" />
            <conversion-tli id="T1050" />
            <conversion-tli id="T1051" />
            <conversion-tli id="T1052" />
            <conversion-tli id="T1053" />
            <conversion-tli id="T1054" />
            <conversion-tli id="T1055" />
            <conversion-tli id="T1056" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-KA"
                          name="ref"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KA"
                          name="ts"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KA"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KA"
                          type="t" />
         <conversion-tier category="CS"
                          display-name="CS-KA"
                          name="CS"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KA"
                          name="fr"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KA"
                          name="fe"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KA"
                          name="fg"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KA"
                          name="nt"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-PKZ"
                          name="ref"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PKZ"
                          name="ts"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PKZ"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PKZ"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PKZ"
                          name="mb"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PKZ"
                          name="mp"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PKZ"
                          name="ge"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PKZ"
                          name="gr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PKZ"
                          name="mc"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PKZ"
                          name="ps"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PKZ"
                          name="SeR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PKZ"
                          name="SyF"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PKZ"
                          name="IST"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PKZ"
                          name="BOR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PKZ"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PKZ"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PKZ"
                          name="CS"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PKZ"
                          name="fr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PKZ"
                          name="fe"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PKZ"
                          name="fg"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PKZ"
                          name="nt"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
