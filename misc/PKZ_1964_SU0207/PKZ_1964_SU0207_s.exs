<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID326DDD05-1775-4BF3-6D47-9C121F42884A">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_1964_SU0207.wav" />
         <referenced-file url="PKZ_1964_SU0207.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_1964_SU0207\PKZ_1964_SU0207.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1241</ud-information>
            <ud-information attribute-name="# HIAT:w">867</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">5</ud-information>
            <ud-information attribute-name="# e">820</ud-information>
            <ud-information attribute-name="# HIAT:u">223</ud-information>
            <ud-information attribute-name="# sc">84</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KA">
            <abbreviation>KA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.333" type="appl" />
         <tli id="T1" time="1.05" type="appl" />
         <tli id="T2" time="1.766" type="appl" />
         <tli id="T3" time="2.483" type="appl" />
         <tli id="T4" time="7.71996900584427" />
         <tli id="T5" time="8.116" type="appl" />
         <tli id="T6" time="8.706" type="appl" />
         <tli id="T7" time="9.297" type="appl" />
         <tli id="T8" time="9.888" type="appl" />
         <tli id="T9" time="10.479" type="appl" />
         <tli id="T10" time="11.07" type="appl" />
         <tli id="T11" time="11.66" type="appl" />
         <tli id="T12" time="12.251" type="appl" />
         <tli id="T13" time="13.084" type="appl" />
         <tli id="T14" time="13.917" type="appl" />
         <tli id="T15" time="16.42660071709869" />
         <tli id="T16" time="16.967" type="appl" />
         <tli id="T17" time="17.62" type="appl" />
         <tli id="T18" time="18.272" type="appl" />
         <tli id="T19" time="18.925" type="appl" />
         <tli id="T20" time="19.779920587512912" />
         <tli id="T21" time="20.412" type="appl" />
         <tli id="T22" time="20.956" type="appl" />
         <tli id="T23" time="21.499" type="appl" />
         <tli id="T24" time="22.043" type="appl" />
         <tli id="T25" time="22.586" type="appl" />
         <tli id="T26" time="23.13" type="appl" />
         <tli id="T27" time="23.673" type="appl" />
         <tli id="T28" time="24.399902039196917" />
         <tli id="T29" time="25.244" type="appl" />
         <tli id="T30" time="26.064" type="appl" />
         <tli id="T31" time="26.884" type="appl" />
         <tli id="T32" time="27.704" type="appl" />
         <tli id="T33" time="28.524" type="appl" />
         <tli id="T34" time="29.344" type="appl" />
         <tli id="T35" time="30.164" type="appl" />
         <tli id="T36" time="30.984" type="appl" />
         <tli id="T37" time="31.804" type="appl" />
         <tli id="T38" time="32.624" type="appl" />
         <tli id="T39" time="34.259862453396984" />
         <tli id="T40" time="34.946" type="appl" />
         <tli id="T41" time="35.4" type="appl" />
         <tli id="T42" time="35.855" type="appl" />
         <tli id="T43" time="36.31" type="appl" />
         <tli id="T44" time="36.873" type="appl" />
         <tli id="T45" time="37.436" type="appl" />
         <tli id="T46" time="37.999" type="appl" />
         <tli id="T47" time="38.598" type="appl" />
         <tli id="T48" time="39.198" type="appl" />
         <tli id="T49" time="39.797" type="appl" />
         <tli id="T50" time="40.768" type="appl" />
         <tli id="T51" time="41.484" type="appl" />
         <tli id="T52" time="42.199" type="appl" />
         <tli id="T53" time="42.915" type="appl" />
         <tli id="T54" time="43.72649111341327" />
         <tli id="T55" time="44.33" type="appl" />
         <tli id="T56" time="44.878" type="appl" />
         <tli id="T57" time="45.427" type="appl" />
         <tli id="T58" time="45.975" type="appl" />
         <tli id="T59" time="48.11314016854758" />
         <tli id="T60" time="49.077" type="appl" />
         <tli id="T61" time="50.85312916803116" />
         <tli id="T62" time="51.251" type="appl" />
         <tli id="T63" time="51.555" type="appl" />
         <tli id="T64" time="51.858" type="appl" />
         <tli id="T65" time="52.162" type="appl" />
         <tli id="T66" time="52.466" type="appl" />
         <tli id="T67" time="52.77" type="appl" />
         <tli id="T68" time="53.073" type="appl" />
         <tli id="T69" time="57.36643635171844" />
         <tli id="T881" time="57.49057726378883" type="intp" />
         <tli id="T882" time="57.61471817585922" type="intp" />
         <tli id="T883" time="57.73885908792961" type="intp" />
         <tli id="T70" time="57.863" type="appl" />
         <tli id="T71" time="58.499" type="appl" />
         <tli id="T884" time="58.8175" type="intp" />
         <tli id="T72" time="59.136" type="appl" />
         <tli id="T73" time="59.772" type="appl" />
         <tli id="T885" time="60.09" type="intp" />
         <tli id="T74" time="60.408" type="appl" />
         <tli id="T75" time="61.242" type="appl" />
         <tli id="T76" time="61.906" type="appl" />
         <tli id="T77" time="62.57" type="appl" />
         <tli id="T78" time="63.88641017530711" />
         <tli id="T79" time="64.521" type="appl" />
         <tli id="T80" time="64.708" type="appl" />
         <tli id="T81" time="64.895" type="appl" />
         <tli id="T82" time="65.082" type="appl" />
         <tli id="T83" time="65.92640198514161" />
         <tli id="T84" time="66.715" type="appl" />
         <tli id="T85" time="67.429" type="appl" />
         <tli id="T86" time="68.84639026196355" />
         <tli id="T87" time="69.609" type="appl" />
         <tli id="T88" time="70.252" type="appl" />
         <tli id="T89" time="70.894" type="appl" />
         <tli id="T90" time="71.83971157770108" />
         <tli id="T91" time="72.382" type="appl" />
         <tli id="T92" time="72.926" type="appl" />
         <tli id="T93" time="73.469" type="appl" />
         <tli id="T94" time="75.96636167668002" />
         <tli id="T95" time="76.871" type="appl" />
         <tli id="T96" time="77.554" type="appl" />
         <tli id="T97" time="78.237" type="appl" />
         <tli id="T98" time="78.88000232273643" />
         <tli id="T99" time="79.355" type="appl" />
         <tli id="T100" time="79.791" type="appl" />
         <tli id="T101" time="80.226" type="appl" />
         <tli id="T102" time="80.99300816235065" />
         <tli id="T103" time="81.567" type="appl" />
         <tli id="T104" time="82.014" type="appl" />
         <tli id="T105" time="82.462" type="appl" />
         <tli id="T106" time="82.909" type="appl" />
         <tli id="T107" time="85.23299113965372" />
         <tli id="T108" time="85.855" type="appl" />
         <tli id="T109" time="86.483" type="appl" />
         <tli id="T110" time="87.11" type="appl" />
         <tli id="T111" time="87.738" type="appl" />
         <tli id="T112" time="88.366" type="appl" />
         <tli id="T113" time="89.261" type="appl" />
         <tli id="T114" time="90.25963762532432" />
         <tli id="T115" time="91.508" type="appl" />
         <tli id="T116" time="92.476" type="appl" />
         <tli id="T117" time="95.6329493858688" />
         <tli id="T118" time="96.644" type="appl" />
         <tli id="T119" time="97.426" type="appl" />
         <tli id="T120" time="98.014" type="appl" />
         <tli id="T121" time="98.75293685973332" />
         <tli id="T122" time="99.489" type="appl" />
         <tli id="T123" time="100.236" type="appl" />
         <tli id="T124" time="101.43292610010411" />
         <tli id="T125" time="102.377" type="appl" />
         <tli id="T126" time="103.17" type="appl" />
         <tli id="T127" time="104.086" type="appl" />
         <tli id="T128" time="105.001" type="appl" />
         <tli id="T129" time="105.916" type="appl" />
         <tli id="T130" time="108.9195627093987" />
         <tli id="T131" time="109.692" type="appl" />
         <tli id="T132" time="110.406" type="appl" />
         <tli id="T133" time="111.40621939262832" />
         <tli id="T134" time="112.174" type="appl" />
         <tli id="T135" time="112.957" type="appl" />
         <tli id="T136" time="113.54666652832114" />
         <tli id="T137" time="114.435" type="appl" />
         <tli id="T138" time="115.129" type="appl" />
         <tli id="T139" time="115.824" type="appl" />
         <tli id="T140" time="116.519" type="appl" />
         <tli id="T141" time="117.214" type="appl" />
         <tli id="T142" time="117.908" type="appl" />
         <tli id="T143" time="118.97952232064135" />
         <tli id="T144" time="119.957" type="appl" />
         <tli id="T145" time="120.714" type="appl" />
         <tli id="T146" time="121.47" type="appl" />
         <tli id="T147" time="122.227" type="appl" />
         <tli id="T148" time="122.984" type="appl" />
         <tli id="T149" time="125.14616422945478" />
         <tli id="T150" time="125.976" type="appl" />
         <tli id="T151" time="126.872" type="appl" />
         <tli id="T152" time="129.38614720675787" />
         <tli id="T153" time="130.125" type="appl" />
         <tli id="T154" time="130.811" type="appl" />
         <tli id="T155" time="132.86613323529906" />
         <tli id="T156" time="133.653" type="appl" />
         <tli id="T157" time="134.255" type="appl" />
         <tli id="T158" time="134.858" type="appl" />
         <tli id="T159" time="135.366" type="appl" />
         <tli id="T160" time="135.873" type="appl" />
         <tli id="T161" time="136.42100177570174" />
         <tli id="T162" time="137.703" type="appl" />
         <tli id="T163" time="139.092" type="appl" />
         <tli id="T164" time="140.211" type="appl" />
         <tli id="T165" time="141.331" type="appl" />
         <tli id="T166" time="142.45" type="appl" />
         <tli id="T167" time="145.5594156076026" />
         <tli id="T168" time="146.263" type="appl" />
         <tli id="T169" time="146.996" type="appl" />
         <tli id="T170" time="147.729" type="appl" />
         <tli id="T171" time="148.462" type="appl" />
         <tli id="T172" time="149.195" type="appl" />
         <tli id="T173" time="151.39939216124645" />
         <tli id="T174" time="151.866" type="appl" />
         <tli id="T175" time="152.323" type="appl" />
         <tli id="T176" time="152.779" type="appl" />
         <tli id="T177" time="153.236" type="appl" />
         <tli id="T178" time="153.72604948684202" />
         <tli id="T179" time="154.172" type="appl" />
         <tli id="T180" time="154.652" type="appl" />
         <tli id="T181" time="155.132" type="appl" />
         <tli id="T182" time="155.612" type="appl" />
         <tli id="T183" time="159.09269460748502" />
         <tli id="T184" time="159.807" type="appl" />
         <tli id="T185" time="160.398" type="appl" />
         <tli id="T186" time="160.99" type="appl" />
         <tli id="T187" time="164.37934004931103" />
         <tli id="T188" time="164.964" type="appl" />
         <tli id="T189" time="165.683" type="appl" />
         <tli id="T190" time="166.402" type="appl" />
         <tli id="T191" time="166.854" type="appl" />
         <tli id="T192" time="167.307" type="appl" />
         <tli id="T193" time="167.759" type="appl" />
         <tli id="T194" time="168.212" type="appl" />
         <tli id="T195" time="172.25264177288796" />
         <tli id="T196" time="173.002" type="appl" />
         <tli id="T197" time="173.748" type="appl" />
         <tli id="T198" time="174.493" type="appl" />
         <tli id="T199" time="175.239" type="appl" />
         <tli id="T200" time="177.49928737530544" />
         <tli id="T201" time="178.059" type="appl" />
         <tli id="T202" time="178.698" type="appl" />
         <tli id="T203" time="179.336" type="appl" />
         <tli id="T204" time="179.975" type="appl" />
         <tli id="T205" time="180.7326077274941" />
         <tli id="T206" time="181.788" type="appl" />
         <tli id="T207" time="182.728" type="appl" />
         <tli id="T208" time="183.669" type="appl" />
         <tli id="T209" time="184.609" type="appl" />
         <tli id="T210" time="185.366" type="appl" />
         <tli id="T211" time="185.895" type="appl" />
         <tli id="T212" time="186.425" type="appl" />
         <tli id="T213" time="186.955" type="appl" />
         <tli id="T214" time="187.485" type="appl" />
         <tli id="T215" time="188.014" type="appl" />
         <tli id="T216" time="188.544" type="appl" />
         <tli id="T217" time="189.352" type="appl" />
         <tli id="T218" time="190.159" type="appl" />
         <tli id="T219" time="190.67" type="appl" />
         <tli id="T220" time="191.18" type="appl" />
         <tli id="T221" time="191.69" type="appl" />
         <tli id="T222" time="192.37922763527473" />
         <tli id="T223" time="193.171" type="appl" />
         <tli id="T224" time="195.86588030371735" />
         <tli id="T225" time="196.801" type="appl" />
         <tli id="T226" time="197.55" type="appl" />
         <tli id="T227" time="198.299" type="appl" />
         <tli id="T228" time="199.048" type="appl" />
         <tli id="T229" time="199.797" type="appl" />
         <tli id="T230" time="200.78586055096523" />
         <tli id="T231" time="201.151" type="appl" />
         <tli id="T232" time="201.68" type="appl" />
         <tli id="T233" time="202.208" type="appl" />
         <tli id="T234" time="203.87918146522406" />
         <tli id="T235" time="204.672" type="appl" />
         <tli id="T236" time="205.453" type="appl" />
         <tli id="T886" time="206.70608254244655" type="intp" />
         <tli id="T237" time="207.9591650848931" />
         <tli id="T238" time="208.508" type="appl" />
         <tli id="T239" time="209.179" type="appl" />
         <tli id="T240" time="209.85" type="appl" />
         <tli id="T241" time="211.13915231787038" />
         <tli id="T242" time="211.584" type="appl" />
         <tli id="T243" time="213.45247636366307" />
         <tli id="T244" time="213.76580843902983" />
         <tli id="T245" time="214.0258073951852" />
         <tli id="T246" time="214.81913754345416" />
         <tli id="T247" time="215.594" type="appl" />
         <tli id="T248" time="216.287" type="appl" />
         <tli id="T249" time="216.532" type="appl" />
         <tli id="T250" time="216.654" type="appl" />
         <tli id="T251" time="219.16578675917992" />
         <tli id="T252" time="219.63778486420046" type="intp" />
         <tli id="T253" time="220.109782969221" type="intp" />
         <tli id="T254" time="220.58178107424155" type="intp" />
         <tli id="T255" time="221.0537791792621" type="intp" />
         <tli id="T256" time="221.52577728428264" />
         <tli id="T257" time="222.642" type="appl" />
         <tli id="T258" time="224.59243163893578" />
         <tli id="T259" time="225.264" type="appl" />
         <tli id="T260" time="225.768" type="appl" />
         <tli id="T261" time="226.273" type="appl" />
         <tli id="T262" time="226.778" type="appl" />
         <tli id="T263" time="227.332" type="appl" />
         <tli id="T264" time="227.885" type="appl" />
         <tli id="T265" time="228.439" type="appl" />
         <tli id="T266" time="228.993" type="appl" />
         <tli id="T267" time="229.546" type="appl" />
         <tli id="T268" time="230.28574211474836" />
         <tli id="T269" time="231.5590703359196" />
         <tli id="T270" time="231.84" type="appl" />
         <tli id="T271" time="232.32" type="appl" />
         <tli id="T272" time="232.799" type="appl" />
         <tli id="T273" time="233.54572902654274" />
         <tli id="T274" time="234.167" type="appl" />
         <tli id="T275" time="234.849" type="appl" />
         <tli id="T276" time="235.532" type="appl" />
         <tli id="T277" time="236.214" type="appl" />
         <tli id="T278" time="238.49237583339632" />
         <tli id="T279" time="239.262" type="appl" />
         <tli id="T280" time="239.879" type="appl" />
         <tli id="T281" time="240.496" type="appl" />
         <tli id="T282" time="240.877" type="appl" />
         <tli id="T283" time="241.258" type="appl" />
         <tli id="T284" time="241.64" type="appl" />
         <tli id="T285" time="242.1143404585881" />
         <tli id="T286" time="242.734" type="appl" />
         <tli id="T287" time="243.448" type="appl" />
         <tli id="T288" time="244.162" type="appl" />
         <tli id="T289" time="245.67901364712696" />
         <tli id="T290" time="246.33" type="appl" />
         <tli id="T291" time="246.874" type="appl" />
         <tli id="T292" time="247.419" type="appl" />
         <tli id="T293" time="247.963" type="appl" />
         <tli id="T294" time="248.507" type="appl" />
         <tli id="T295" time="249.121" type="appl" />
         <tli id="T296" time="249.735" type="appl" />
         <tli id="T297" time="262.3322801208739" />
         <tli id="T298" time="263.438" type="appl" />
         <tli id="T299" time="264.132" type="appl" />
         <tli id="T300" time="264.827" type="appl" />
         <tli id="T301" time="265.551" type="appl" />
         <tli id="T302" time="266.274" type="appl" />
         <tli id="T303" time="266.998" type="appl" />
         <tli id="T304" time="269.67891729223874" />
         <tli id="T305" time="270.353" type="appl" />
         <tli id="T306" time="270.96" type="appl" />
         <tli id="T307" time="271.567" type="appl" />
         <tli id="T308" time="272.174" type="appl" />
         <tli id="T309" time="272.781" type="appl" />
         <tli id="T310" time="273.388" type="appl" />
         <tli id="T311" time="273.995" type="appl" />
         <tli id="T312" time="274.602" type="appl" />
         <tli id="T313" time="275.209" type="appl" />
         <tli id="T314" time="276.2455575951373" />
         <tli id="T315" time="277.073" type="appl" />
         <tli id="T316" time="278.1" type="appl" />
         <tli id="T317" time="279.35221178919903" />
         <tli id="T318" time="280.092" type="appl" />
         <tli id="T319" time="280.789" type="appl" />
         <tli id="T320" time="281.486" type="appl" />
         <tli id="T321" time="282.183" type="appl" />
         <tli id="T322" time="283.349" type="appl" />
         <tli id="T323" time="284.278" type="appl" />
         <tli id="T324" time="285.208" type="appl" />
         <tli id="T325" time="286.137" type="appl" />
         <tli id="T326" time="287.067" type="appl" />
         <tli id="T327" time="288.096" type="appl" />
         <tli id="T328" time="289.242" type="appl" />
         <tli id="T329" time="289.592" type="appl" />
         <tli id="T330" time="290.11883522999216" />
         <tli id="T331" time="291.28549721274067" />
         <tli id="T332" time="291.29174860637033" type="intp" />
         <tli id="T333" time="291.298" type="appl" />
         <tli id="T334" time="291.917" type="appl" />
         <tli id="T335" time="292.535" type="appl" />
         <tli id="T336" time="293.098" type="appl" />
         <tli id="T337" time="293.662" type="appl" />
         <tli id="T338" time="294.225" type="appl" />
         <tli id="T339" time="294.789" type="appl" />
         <tli id="T340" time="295.5721466693537" />
         <tli id="T341" time="296.119" type="appl" />
         <tli id="T342" time="296.587" type="appl" />
         <tli id="T343" time="297.055" type="appl" />
         <tli id="T344" time="300.1054618023192" />
         <tli id="T345" time="300.776" type="appl" />
         <tli id="T346" time="303.85211342691724" />
         <tli id="T347" time="305.632" type="appl" />
         <tli id="T348" time="307.186" type="appl" />
         <tli id="T349" time="308.74" type="appl" />
         <tli id="T350" time="310.293" type="appl" />
         <tli id="T351" time="311.847" type="appl" />
         <tli id="T352" time="313.401" type="appl" />
         <tli id="T353" time="314.949" type="appl" />
         <tli id="T354" time="315.496" type="appl" />
         <tli id="T355" time="316.232063723854" />
         <tli id="T356" time="317.298" type="appl" />
         <tli id="T357" time="318.59872088885805" />
         <tli id="T358" time="319.612" type="appl" />
         <tli id="T359" time="320.431" type="appl" />
         <tli id="T360" time="321.25" type="appl" />
         <tli id="T361" time="322.07" type="appl" />
         <tli id="T362" time="322.693" type="appl" />
         <tli id="T363" time="323.277" type="appl" />
         <tli id="T364" time="323.86" type="appl" />
         <tli id="T365" time="324.444" type="appl" />
         <tli id="T366" time="325.3253605493908" />
         <tli id="T367" time="326.517" type="appl" />
         <tli id="T368" time="329.4986771276796" />
         <tli id="T369" time="329.996" type="appl" />
         <tli id="T370" time="330.556" type="appl" />
         <tli id="T371" time="331.115" type="appl" />
         <tli id="T372" time="331.674" type="appl" />
         <tli id="T373" time="332.215" type="appl" />
         <tli id="T374" time="332.756" type="appl" />
         <tli id="T375" time="333.298" type="appl" />
         <tli id="T376" time="333.76566520489587" />
         <tli id="T377" time="334.609" type="appl" />
         <tli id="T378" time="335.379" type="appl" />
         <tli id="T379" time="337.68531092662334" />
         <tli id="T380" time="338.331" type="appl" />
         <tli id="T381" time="339.061" type="appl" />
         <tli id="T382" time="339.792" type="appl" />
         <tli id="T383" time="340.523" type="appl" />
         <tli id="T384" time="341.253" type="appl" />
         <tli id="T385" time="343.76528651671833" />
         <tli id="T386" time="346.31194295906073" />
         <tli id="T387" time="347.1" type="appl" />
         <tli id="T388" time="347.86" type="appl" />
         <tli id="T389" time="348.62" type="appl" />
         <tli id="T390" time="349.371" type="appl" />
         <tli id="T391" time="350.2652604206028" />
         <tli id="T392" time="351.14" type="appl" />
         <tli id="T393" time="351.857" type="appl" />
         <tli id="T394" time="352.533" type="appl" />
         <tli id="T395" time="353.21" type="appl" />
         <tli id="T396" time="354.98524147080803" />
         <tli id="T397" time="355.825" type="appl" />
         <tli id="T398" time="356.509" type="appl" />
         <tli id="T399" time="357.193" type="appl" />
         <tli id="T400" time="357.762" type="appl" />
         <tli id="T401" time="358.331" type="appl" />
         <tli id="T402" time="358.901" type="appl" />
         <tli id="T403" time="359.47" type="appl" />
         <tli id="T404" time="361.445215535284" />
         <tli id="T405" time="362.05" type="appl" />
         <tli id="T406" time="365.6051988337699" />
         <tli id="T887" time="365.90546588917994" type="intp" />
         <tli id="T888" time="366.20573294458995" type="intp" />
         <tli id="T407" time="366.506" type="appl" />
         <tli id="T408" time="366.963" type="appl" />
         <tli id="T889" time="367.3774283607179" type="intp" />
         <tli id="T409" time="367.79185672143575" />
         <tli id="T410" time="368.677" type="appl" />
         <tli id="T411" time="369.391" type="appl" />
         <tli id="T412" time="370.106" type="appl" />
         <tli id="T413" time="370.878" type="appl" />
         <tli id="T414" time="371.651" type="appl" />
         <tli id="T415" time="373.6251666351781" />
         <tli id="T416" time="374.202" type="appl" />
         <tli id="T417" time="374.78" type="appl" />
         <tli id="T418" time="375.359" type="appl" />
         <tli id="T419" time="376.318" type="appl" />
         <tli id="T420" time="377.6118172962273" />
         <tli id="T421" time="378.191" type="appl" />
         <tli id="T422" time="378.758" type="appl" />
         <tli id="T423" time="379.326" type="appl" />
         <tli id="T424" time="381.0518034853599" />
         <tli id="T425" time="381.61" type="appl" />
         <tli id="T426" time="382.167" type="appl" />
         <tli id="T427" time="382.87179617844754" />
         <tli id="T428" time="383.541" type="appl" />
         <tli id="T429" time="384.057" type="appl" />
         <tli id="T430" time="384.572" type="appl" />
         <tli id="T431" time="385.088" type="appl" />
         <tli id="T432" time="385.604" type="appl" />
         <tli id="T433" time="386.12" type="appl" />
         <tli id="T434" time="386.635" type="appl" />
         <tli id="T435" time="387.151" type="appl" />
         <tli id="T436" time="387.667" type="appl" />
         <tli id="T437" time="388.183" type="appl" />
         <tli id="T438" time="388.698" type="appl" />
         <tli id="T439" time="389.214" type="appl" />
         <tli id="T440" time="391.118429736504" />
         <tli id="T441" time="391.959" type="appl" />
         <tli id="T442" time="392.727" type="appl" />
         <tli id="T443" time="393.495" type="appl" />
         <tli id="T444" time="394.3829999632306" />
         <tli id="T445" time="396.0784098231604" />
         <tli id="T446" time="397.299" type="appl" />
         <tli id="T447" time="398.523" type="appl" />
         <tli id="T448" time="399.626" type="appl" />
         <tli id="T449" time="400.526" type="appl" />
         <tli id="T450" time="401.426" type="appl" />
         <tli id="T451" time="403.9117117073288" />
         <tli id="T452" time="404.75" type="appl" />
         <tli id="T453" time="405.619" type="appl" />
         <tli id="T454" time="406.494" type="appl" />
         <tli id="T455" time="408.45169348019584" />
         <tli id="T456" time="409.367" type="appl" />
         <tli id="T457" time="410.153" type="appl" />
         <tli id="T458" time="410.94" type="appl" />
         <tli id="T459" time="411.726" type="appl" />
         <tli id="T460" time="413.345007167838" />
         <tli id="T461" time="414.034" type="appl" />
         <tli id="T462" time="414.761" type="appl" />
         <tli id="T463" time="415.488" type="appl" />
         <tli id="T464" time="416.214" type="appl" />
         <tli id="T465" time="417.07" type="appl" />
         <tli id="T466" time="417.925" type="appl" />
         <tli id="T467" time="418.928" type="appl" />
         <tli id="T468" time="419.839" type="appl" />
         <tli id="T469" time="420.428" type="appl" />
         <tli id="T470" time="421.016" type="appl" />
         <tli id="T471" time="421.605" type="appl" />
         <tli id="T472" time="422.46" type="appl" />
         <tli id="T473" time="423.0510098668147" />
         <tli id="T474" time="423.797" type="appl" />
         <tli id="T475" time="424.524" type="appl" />
         <tli id="T476" time="425.25" type="appl" />
         <tli id="T477" time="425.976" type="appl" />
         <tli id="T478" time="427.26495128200287" />
         <tli id="T479" time="428.728" type="appl" />
         <tli id="T480" time="429.87827412335946" />
         <tli id="T481" time="430.965" type="appl" />
         <tli id="T482" time="432.17" type="appl" />
         <tli id="T483" time="433.375" type="appl" />
         <tli id="T484" time="434.477" type="appl" />
         <tli id="T485" time="435.216" type="appl" />
         <tli id="T486" time="435.956" type="appl" />
         <tli id="T487" time="437.181" type="appl" />
         <tli id="T488" time="437.784" type="appl" />
         <tli id="T489" time="439.01823742820613" />
         <tli id="T490" time="439.676" type="appl" />
         <tli id="T491" time="440.314" type="appl" />
         <tli id="T492" time="440.951" type="appl" />
         <tli id="T493" time="441.589" type="appl" />
         <tli id="T494" time="442.297" type="appl" />
         <tli id="T495" time="443.005" type="appl" />
         <tli id="T496" time="443.713" type="appl" />
         <tli id="T497" time="445.6382108503161" />
         <tli id="T498" time="446.155" type="appl" />
         <tli id="T499" time="446.608" type="appl" />
         <tli id="T500" time="447.062" type="appl" />
         <tli id="T501" time="447.515" type="appl" />
         <tli id="T502" time="447.968" type="appl" />
         <tli id="T503" time="448.421" type="appl" />
         <tli id="T504" time="448.875" type="appl" />
         <tli id="T505" time="449.328" type="appl" />
         <tli id="T506" time="449.95819350643626" />
         <tli id="T507" time="450.561" type="appl" />
         <tli id="T508" time="451.052" type="appl" />
         <tli id="T509" time="451.64402007150784" />
         <tli id="T510" time="452.266" type="appl" />
         <tli id="T511" time="452.826" type="appl" />
         <tli id="T512" time="453.385" type="appl" />
         <tli id="T513" time="453.944" type="appl" />
         <tli id="T514" time="454.504" type="appl" />
         <tli id="T515" time="455.063" type="appl" />
         <tli id="T516" time="455.623" type="appl" />
         <tli id="T517" time="457.53149643444925" />
         <tli id="T518" time="458.415" type="appl" />
         <tli id="T519" time="459.064" type="appl" />
         <tli id="T520" time="459.713" type="appl" />
         <tli id="T521" time="460.361" type="appl" />
         <tli id="T522" time="461.01" type="appl" />
         <tli id="T523" time="461.659" type="appl" />
         <tli id="T524" time="463.3648063481917" />
         <tli id="T525" time="463.892" type="appl" />
         <tli id="T526" time="464.414" type="appl" />
         <tli id="T527" time="464.937" type="appl" />
         <tli id="T528" time="465.46" type="appl" />
         <tli id="T529" time="465.982" type="appl" />
         <tli id="T530" time="467.9981210796786" />
         <tli id="T531" time="468.84" type="appl" />
         <tli id="T532" time="471.3647742298956" />
         <tli id="T533" time="472.216" type="appl" />
         <tli id="T534" time="472.716" type="appl" />
         <tli id="T535" time="473.215" type="appl" />
         <tli id="T536" time="475.34475825104334" />
         <tli id="T537" time="476.639" type="appl" />
         <tli id="T538" time="477.063" type="appl" />
         <tli id="T539" time="477.6314157372304" />
         <tli id="T540" time="478.24" type="appl" />
         <tli id="T541" time="478.831" type="appl" />
         <tli id="T542" time="479.5980745081493" />
         <tli id="T543" time="480.246" type="appl" />
         <tli id="T544" time="480.802" type="appl" />
         <tli id="T545" time="481.359" type="appl" />
         <tli id="T546" time="482.1713975100973" />
         <tli id="T547" time="483.262" type="appl" />
         <tli id="T548" time="485.8047162563712" />
         <tli id="T549" time="486.734" type="appl" />
         <tli id="T550" time="487.43" type="appl" />
         <tli id="T551" time="488.126" type="appl" />
         <tli id="T552" time="489.2647023652081" />
         <tli id="T553" time="490.289" type="appl" />
         <tli id="T554" time="491.063" type="appl" />
         <tli id="T555" time="491.838" type="appl" />
         <tli id="T556" time="492.612" type="appl" />
         <tli id="T557" time="493.386" type="appl" />
         <tli id="T558" time="495.0046793203307" />
         <tli id="T559" time="496.606" type="appl" />
         <tli id="T560" time="497.65" type="appl" />
         <tli id="T561" time="498.693" type="appl" />
         <tli id="T562" time="501.31132066707397" />
         <tli id="T563" time="502.459" type="appl" />
         <tli id="T564" time="503.287" type="appl" />
         <tli id="T565" time="504.116" type="appl" />
         <tli id="T566" time="504.944" type="appl" />
         <tli id="T567" time="506.59129946899844" />
         <tli id="T568" time="507.974" type="appl" />
         <tli id="T569" time="508.907" type="appl" />
         <tli id="T570" time="509.84" type="appl" />
         <tli id="T571" time="510.992" type="appl" />
         <tli id="T572" time="512.7646080177133" />
         <tli id="T573" time="513.474" type="appl" />
         <tli id="T574" time="514.058" type="appl" />
         <tli id="T575" time="514.642" type="appl" />
         <tli id="T576" time="515.227" type="appl" />
         <tli id="T577" time="515.812" type="appl" />
         <tli id="T578" time="516.3759997686864" />
         <tli id="T579" time="516.974" type="appl" />
         <tli id="T580" time="517.468" type="appl" />
         <tli id="T581" time="518.1179198585536" />
         <tli id="T582" time="520.7645758994172" />
         <tli id="T583" time="521.334" type="appl" />
         <tli id="T584" time="521.77" type="appl" />
         <tli id="T585" time="521.9112379624614" />
         <tli id="T586" time="522.262" type="appl" />
         <tli id="T890" time="522.4549999999999" type="intp" />
         <tli id="T587" time="522.648" type="appl" />
         <tli id="T891" time="522.7766666666666" type="intp" />
         <tli id="T892" time="522.9053333333334" type="intp" />
         <tli id="T588" time="523.034" type="appl" />
         <tli id="T893" time="523.2265" type="intp" />
         <tli id="T589" time="523.419" type="appl" />
         <tli id="T894" time="523.5476666666666" type="intp" />
         <tli id="T895" time="523.6763333333333" type="intp" />
         <tli id="T590" time="523.805" type="appl" />
         <tli id="T896" time="524.068" type="appl" />
         <tli id="T591" time="524.229978653174" />
         <tli id="T592" time="524.443489326587" type="intp" />
         <tli id="T593" time="524.657" type="appl" />
         <tli id="T594" time="525.246" type="appl" />
         <tli id="T595" time="528.9712096180651" />
         <tli id="T596" time="529.721" type="appl" />
         <tli id="T597" time="530.302" type="appl" />
         <tli id="T598" time="530.882" type="appl" />
         <tli id="T599" time="531.462" type="appl" />
         <tli id="T600" time="532.402" type="appl" />
         <tli id="T601" time="533.258" type="appl" />
         <tli id="T602" time="538.3845051588702" />
         <tli id="T603" time="538.923" type="appl" />
         <tli id="T604" time="539.478" type="appl" />
         <tli id="T605" time="540.034" type="appl" />
         <tli id="T606" time="540.589" type="appl" />
         <tli id="T607" time="541.144" type="appl" />
         <tli id="T608" time="541.7" type="appl" />
         <tli id="T609" time="542.255" type="appl" />
         <tli id="T610" time="542.81" type="appl" />
         <tli id="T611" time="543.908" type="appl" />
         <tli id="T612" time="544.881" type="appl" />
         <tli id="T613" time="545.562" type="appl" />
         <tli id="T614" time="546.179" type="appl" />
         <tli id="T615" time="546.796" type="appl" />
         <tli id="T616" time="547.414" type="appl" />
         <tli id="T617" time="548.307" type="appl" />
         <tli id="T618" time="549.059" type="appl" />
         <tli id="T619" time="550.3644570617217" />
         <tli id="T620" time="551.096" type="appl" />
         <tli id="T621" time="551.725" type="appl" />
         <tli id="T622" time="554.2777746838551" />
         <tli id="T623" time="554.796" type="appl" />
         <tli id="T624" time="555.12" type="appl" />
         <tli id="T625" time="555.444" type="appl" />
         <tli id="T626" time="555.9244347395058" />
         <tli id="T627" time="556.71" type="appl" />
         <tli id="T628" time="557.549" type="appl" />
         <tli id="T629" time="558.387" type="appl" />
         <tli id="T630" time="559.022" type="appl" />
         <tli id="T631" time="559.656" type="appl" />
         <tli id="T632" time="560.6644157094156" />
         <tli id="T633" time="561.59" type="appl" />
         <tli id="T634" time="562.375" type="appl" />
         <tli id="T635" time="563.16" type="appl" />
         <tli id="T636" time="563.945" type="appl" />
         <tli id="T637" time="565.1777309226767" />
         <tli id="T638" time="565.84" type="appl" />
         <tli id="T639" time="566.397" type="appl" />
         <tli id="T640" time="566.953" type="appl" />
         <tli id="T641" time="567.509" type="appl" />
         <tli id="T642" time="568.065" type="appl" />
         <tli id="T643" time="568.622" type="appl" />
         <tli id="T644" time="569.4577137393884" />
         <tli id="T645" time="570.249" type="appl" />
         <tli id="T646" time="570.883" type="appl" />
         <tli id="T647" time="571.517" type="appl" />
         <tli id="T648" time="572.8976999285211" />
         <tli id="T649" time="577.0176833875986" />
         <tli id="T650" time="577.574" type="appl" />
         <tli id="T651" time="578.065" type="appl" />
         <tli id="T652" time="578.557" type="appl" />
         <tli id="T653" time="579.048" type="appl" />
         <tli id="T654" time="579.539" type="appl" />
         <tli id="T655" time="580.03" type="appl" />
         <tli id="T656" time="580.521" type="appl" />
         <tli id="T657" time="581.013" type="appl" />
         <tli id="T658" time="581.504" type="appl" />
         <tli id="T659" time="581.995" type="appl" />
         <tli id="T660" time="582.774" type="appl" />
         <tli id="T661" time="583.448" type="appl" />
         <tli id="T662" time="584.123" type="appl" />
         <tli id="T663" time="584.798" type="appl" />
         <tli id="T664" time="585.473" type="appl" />
         <tli id="T665" time="586.147" type="appl" />
         <tli id="T666" time="588.3709711397166" />
         <tli id="T667" time="589.415" type="appl" />
         <tli id="T668" time="589.8976316771419" />
         <tli id="T669" time="590.6842951855094" />
         <tli id="T670" time="591.1466475927548" type="intp" />
         <tli id="T671" time="591.609" type="appl" />
         <tli id="T672" time="592.8709530731752" />
         <tli id="T673" time="594.259" type="appl" />
         <tli id="T674" time="596.6376046174775" />
         <tli id="T675" time="597.305" type="appl" />
         <tli id="T676" time="597.963" type="appl" />
         <tli id="T677" time="598.621" type="appl" />
         <tli id="T678" time="599.278" type="appl" />
         <tli id="T679" time="599.936" type="appl" />
         <tli id="T680" time="600.594" type="appl" />
         <tli id="T681" time="601.252" type="appl" />
         <tli id="T682" time="604.8642382558296" />
         <tli id="T683" time="605.452" type="appl" />
         <tli id="T684" time="606.8308970267485" />
         <tli id="T685" time="607.688" type="appl" />
         <tli id="T686" time="608.494" type="appl" />
         <tli id="T687" time="609.301" type="appl" />
         <tli id="T688" time="610.107" type="appl" />
         <tli id="T689" time="610.913" type="appl" />
         <tli id="T690" time="611.719" type="appl" />
         <tli id="T691" time="612.526" type="appl" />
         <tli id="T692" time="614.3908666749586" />
         <tli id="T693" time="614.93" type="appl" />
         <tli id="T694" time="615.4" type="appl" />
         <tli id="T695" time="615.871" type="appl" />
         <tli id="T696" time="616.6708575212442" />
         <tli id="T697" time="617.617" type="appl" />
         <tli id="T698" time="618.448" type="appl" />
         <tli id="T699" time="619.279" type="appl" />
         <tli id="T700" time="621.4308384108581" />
         <tli id="T701" time="622.058" type="appl" />
         <tli id="T702" time="622.715" type="appl" />
         <tli id="T703" time="623.372" type="appl" />
         <tli id="T704" time="624.03" type="appl" />
         <tli id="T705" time="624.687" type="appl" />
         <tli id="T706" time="625.5974883492456" />
         <tli id="T707" time="626.272" type="appl" />
         <tli id="T708" time="626.941" type="appl" />
         <tli id="T709" time="627.61" type="appl" />
         <tli id="T710" time="628.278" type="appl" />
         <tli id="T711" time="628.947" type="appl" />
         <tli id="T712" time="630.6774679541275" />
         <tli id="T713" time="631.174" type="appl" />
         <tli id="T714" time="631.787" type="appl" />
         <tli id="T715" time="632.401" type="appl" />
         <tli id="T716" time="633.015" type="appl" />
         <tli id="T717" time="633.629" type="appl" />
         <tli id="T718" time="634.242" type="appl" />
         <tli id="T719" time="635.6107814811783" />
         <tli id="T720" time="636.408" type="appl" />
         <tli id="T721" time="637.219" type="appl" />
         <tli id="T722" time="638.03" type="appl" />
         <tli id="T723" time="638.842" type="appl" />
         <tli id="T724" time="639.653" type="appl" />
         <tli id="T725" time="640.464" type="appl" />
         <tli id="T726" time="641.275" type="appl" />
         <tli id="T727" time="642.9507520126415" />
         <tli id="T728" time="643.671" type="appl" />
         <tli id="T729" time="644.432" type="appl" />
         <tli id="T730" time="645.194" type="appl" />
         <tli id="T731" time="645.956" type="appl" />
         <tli id="T732" time="646.717" type="appl" />
         <tli id="T733" time="648.7107288874683" />
         <tli id="T734" time="649.511" type="appl" />
         <tli id="T735" time="650.271" type="appl" />
         <tli id="T736" time="651.032" type="appl" />
         <tli id="T737" time="651.793" type="appl" />
         <tli id="T738" time="652.554" type="appl" />
         <tli id="T739" time="653.314" type="appl" />
         <tli id="T740" time="654.5573720810137" />
         <tli id="T741" time="655.014" type="appl" />
         <tli id="T742" time="655.572" type="appl" />
         <tli id="T743" time="656.129" type="appl" />
         <tli id="T744" time="656.687" type="appl" />
         <tli id="T745" time="660.4173485543619" />
         <tli id="T897" time="661.2272987608816" type="intp" />
         <tli id="T898" time="662.0372489674013" type="intp" />
         <tli id="T899" time="662.847199173921" type="intp" />
         <tli id="T900" time="663.6571493804407" type="intp" />
         <tli id="T901" time="664.4670995869604" type="intp" />
         <tli id="T902" time="665.2770497934803" type="intp" />
         <tli id="T746" time="666.087" type="appl" />
         <tli id="T903" time="666.32" type="appl" />
         <tli id="T747" time="666.553" type="appl" />
         <tli id="T748" time="667.018" type="appl" />
         <tli id="T749" time="667.484" type="appl" />
         <tli id="T904" time="667.717" type="appl" />
         <tli id="T750" time="667.95" type="appl" />
         <tli id="T751" time="668.2900800903433" type="intp" />
         <tli id="T752" time="668.6301601806865" type="intp" />
         <tli id="T880" time="668.9702402710298" type="intp" />
         <tli id="T753" time="669.3103203613729" type="intp" />
         <tli id="T905" time="669.650400451716" type="intp" />
         <tli id="T754" time="669.9904805420592" type="intp" />
         <tli id="T918" time="670.3305606324025" type="intp" />
         <tli id="T755" time="670.6706407227456" />
         <tli id="T919" time="670.788" />
         <tli id="T906" time="671.0844999999999" type="intp" />
         <tli id="T756" time="671.381" type="appl" />
         <tli id="T757" time="671.864" type="appl" />
         <tli id="T907" time="672.1073169889517" type="intp" />
         <tli id="T758" time="672.3506339779034" />
         <tli id="T759" time="673.455" type="appl" />
         <tli id="T760" time="674.562" type="appl" />
         <tli id="T761" time="675.6633290114183" />
         <tli id="T762" time="676.726" type="appl" />
         <tli id="T763" time="677.77" type="appl" />
         <tli id="T764" time="680.0839362635505" />
         <tli id="T765" time="680.966" type="appl" />
         <tli id="T766" time="681.795" type="appl" />
         <tli id="T767" time="684.2639194817409" />
         <tli id="T768" time="684.828" type="appl" />
         <tli id="T769" time="685.277" type="appl" />
         <tli id="T770" time="685.725" type="appl" />
         <tli id="T771" time="686.174" type="appl" />
         <tli id="T772" time="686.622" type="appl" />
         <tli id="T773" time="687.07" type="appl" />
         <tli id="T774" time="687.519" type="appl" />
         <tli id="T775" time="690.4038948309485" />
         <tli id="T908" time="690.6785965539657" type="intp" />
         <tli id="T909" time="690.9532982769829" type="intp" />
         <tli id="T776" time="691.228" type="appl" />
         <tli id="T910" time="691.5392778447783" type="intp" />
         <tli id="T777" time="691.8505556895568" />
         <tli id="T778" time="692.2004167671676" type="intp" />
         <tli id="T921" time="692.5502778447784" type="intp" />
         <tli id="T911" time="692.9001389223893" type="intp" />
         <tli id="T920" time="693.25" />
         <tli id="T779" time="693.6375" type="intp" />
         <tli id="T780" time="694.025" type="appl" />
         <tli id="T781" time="694.671" type="appl" />
         <tli id="T782" time="695.317" type="appl" />
         <tli id="T783" time="695.963" type="appl" />
         <tli id="T784" time="696.608" type="appl" />
         <tli id="T785" time="697.254" type="appl" />
         <tli id="T786" time="697.9" type="appl" />
         <tli id="T787" time="698.546" type="appl" />
         <tli id="T788" time="700.4038546830785" />
         <tli id="T789" time="701.157" type="appl" />
         <tli id="T790" time="701.748" type="appl" />
         <tli id="T791" time="702.339" type="appl" />
         <tli id="T792" time="702.93" type="appl" />
         <tli id="T793" time="703.521" type="appl" />
         <tli id="T794" time="704.112" type="appl" />
         <tli id="T795" time="704.704" type="appl" />
         <tli id="T796" time="705.295" type="appl" />
         <tli id="T797" time="705.886" type="appl" />
         <tli id="T798" time="706.477" type="appl" />
         <tli id="T799" time="707.068" type="appl" />
         <tli id="T800" time="708.7104880002478" />
         <tli id="T801" time="709.582" type="appl" />
         <tli id="T802" time="710.34" type="appl" />
         <tli id="T803" time="711.098" type="appl" />
         <tli id="T804" time="711.856" type="appl" />
         <tli id="T805" time="712.614" type="appl" />
         <tli id="T806" time="714.2704656780319" />
         <tli id="T807" time="714.997" type="appl" />
         <tli id="T808" time="715.747" type="appl" />
         <tli id="T809" time="716.497" type="appl" />
         <tli id="T810" time="717.247" type="appl" />
         <tli id="T811" time="717.997" type="appl" />
         <tli id="T812" time="718.747" type="appl" />
         <tli id="T813" time="719.996" type="appl" />
         <tli id="T814" time="720.549" type="appl" />
         <tli id="T815" time="721.102" type="appl" />
         <tli id="T816" time="728.5170751473663" />
         <tli id="T817" time="729.844" type="appl" />
         <tli id="T818" time="730.894" type="appl" />
         <tli id="T819" time="731.639" type="appl" />
         <tli id="T820" time="732.384" type="appl" />
         <tli id="T821" time="733.6303879517553" />
         <tli id="T822" time="734.7" type="appl" />
         <tli id="T823" time="735.9837118369567" />
         <tli id="T824" time="736.992" type="appl" />
         <tli id="T825" time="738.6303678778203" />
         <tli id="T826" time="739.855" type="appl" />
         <tli id="T827" time="740.838" type="appl" />
         <tli id="T828" time="741.822" type="appl" />
         <tli id="T829" time="742.805" type="appl" />
         <tli id="T830" time="744.2570119546187" />
         <tli id="T831" time="745.07" type="appl" />
         <tli id="T832" time="745.767" type="appl" />
         <tli id="T833" time="746.464" type="appl" />
         <tli id="T834" time="747.5036655866102" />
         <tli id="T912" time="747.9153327933051" type="intp" />
         <tli id="T835" time="748.327" type="appl" />
         <tli id="T836" time="748.85" type="appl" />
         <tli id="T913" time="749.1115" type="intp" />
         <tli id="T837" time="749.373" type="appl" />
         <tli id="T838" time="749.896" type="appl" />
         <tli id="T914" time="750.4764923270335" type="intp" />
         <tli id="T839" time="751.056984654067" />
         <tli id="T915" time="751.6536564360447" type="intp" />
         <tli id="T916" time="752.2503282180223" type="intp" />
         <tli id="T840" time="752.847" type="appl" />
         <tli id="T841" time="754.234" type="appl" />
         <tli id="T842" time="756.3102968963859" />
         <tli id="T843" time="757.582" type="appl" />
         <tli id="T844" time="758.813" type="appl" />
         <tli id="T845" time="760.044" type="appl" />
         <tli id="T846" time="761.275" type="appl" />
         <tli id="T847" time="762.7102712017492" />
         <tli id="T848" time="764.133" type="appl" />
         <tli id="T849" time="764.885" type="appl" />
         <tli id="T850" time="765.665" type="appl" />
         <tli id="T851" time="766.424" type="appl" />
         <tli id="T852" time="769.1769119061264" />
         <tli id="T853" time="769.752" type="appl" />
         <tli id="T854" time="770.382" type="appl" />
         <tli id="T855" time="771.013" type="appl" />
         <tli id="T856" time="773.3435618445138" />
         <tli id="T857" time="774.2253291377251" />
         <tli id="T858" time="775.127" type="appl" />
         <tli id="T859" time="776.4968825178855" />
         <tli id="T860" time="777.06" type="appl" />
         <tli id="T861" time="777.674" type="appl" />
         <tli id="T862" time="778.287" type="appl" />
         <tli id="T863" time="778.901" type="appl" />
         <tli id="T864" time="779.477" type="appl" />
         <tli id="T865" time="780.053" type="appl" />
         <tli id="T866" time="780.63" type="appl" />
         <tli id="T867" time="781.206" type="appl" />
         <tli id="T868" time="785.6968455818451" />
         <tli id="T869" time="786.4393426008656" />
         <tli id="T870" time="786.979" type="appl" />
         <tli id="T871" time="787.511" type="appl" />
         <tli id="T872" time="788.043" type="appl" />
         <tli id="T873" time="788.576" type="appl" />
         <tli id="T874" time="789.108" type="appl" />
         <tli id="T875" time="789.854" type="appl" />
         <tli id="T876" time="790.585" type="appl" />
         <tli id="T877" time="791.315" type="appl" />
         <tli id="T878" time="792.045" type="appl" />
         <tli id="T879" time="792.776" type="appl" />
         <tli id="T922" time="793.506" type="appl" />
         <tli id="T917" time="793.936" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-KA"
                      id="tx-KA"
                      speaker="KA"
                      type="t">
         <timeline-fork end="T4" start="T0">
            <tli id="T0.tx-KA.1" />
            <tli id="T0.tx-KA.2" />
            <tli id="T0.tx-KA.3" />
         </timeline-fork>
         <timeline-fork end="T61" start="T59">
            <tli id="T59.tx-KA.1" />
         </timeline-fork>
         <timeline-fork end="T246" start="T243">
            <tli id="T243.tx-KA.1" />
            <tli id="T243.tx-KA.2" />
         </timeline-fork>
         <timeline-fork end="T256" start="T251">
            <tli id="T251.tx-KA.1" />
            <tli id="T251.tx-KA.2" />
            <tli id="T251.tx-KA.3" />
            <tli id="T251.tx-KA.4" />
         </timeline-fork>
         <timeline-fork end="T335" start="T330">
            <tli id="T330.tx-KA.1" />
            <tli id="T330.tx-KA.2" />
         </timeline-fork>
         <timeline-fork end="T409" start="T406">
            <tli id="T406.tx-KA.1" />
            <tli id="T406.tx-KA.2" />
         </timeline-fork>
         <timeline-fork end="T539" start="T536">
            <tli id="T536.tx-KA.1" />
            <tli id="T536.tx-KA.2" />
         </timeline-fork>
         <timeline-fork end="T592" start="T585">
            <tli id="T585.tx-KA.1" />
            <tli id="T585.tx-KA.2" />
            <tli id="T585.tx-KA.3" />
            <tli id="T585.tx-KA.4" />
         </timeline-fork>
         <timeline-fork end="T626" start="T622">
            <tli id="T622.tx-KA.1" />
            <tli id="T622.tx-KA.2" />
         </timeline-fork>
         <timeline-fork end="T669" start="T666">
            <tli id="T666.tx-KA.1" />
            <tli id="T666.tx-KA.2" />
         </timeline-fork>
         <timeline-fork end="T684" start="T682">
            <tli id="T682.tx-KA.1" />
         </timeline-fork>
         <timeline-fork end="T901" start="T745">
            <tli id="T745.tx-KA.1" />
            <tli id="T745.tx-KA.2" />
            <tli id="T745.tx-KA.3" />
            <tli id="T745.tx-KA.4" />
            <tli id="T745.tx-KA.5" />
         </timeline-fork>
         <timeline-fork end="T758" start="T755">
            <tli id="T755.tx-KA.1" />
            <tli id="T755.tx-KA.2" />
         </timeline-fork>
         <timeline-fork end="T777" start="T775">
            <tli id="T775.tx-KA.1" />
         </timeline-fork>
         <timeline-fork end="T779" start="T777">
            <tli id="T777.tx-KA.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KA">
            <ts e="T4" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T0.tx-KA.1" id="Seg_4" n="HIAT:w" s="T0">Klavdia</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-KA.2" id="Seg_7" n="HIAT:w" s="T0.tx-KA.1">Plotnikova</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-KA.3" id="Seg_11" n="HIAT:w" s="T0.tx-KA.2">viies</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T0.tx-KA.3">lint</ts>
                  <nts id="Seg_15" n="HIAT:ip">.</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T39" id="Seg_17" n="sc" s="T38">
               <ts e="T39" id="Seg_19" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_21" n="HIAT:w" s="T38">Mhmh</ts>
                  <nts id="Seg_22" n="HIAT:ip">.</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T61" id="Seg_24" n="sc" s="T59">
               <ts e="T61" id="Seg_26" n="HIAT:u" s="T59">
                  <ts e="T59.tx-KA.1" id="Seg_28" n="HIAT:w" s="T59">Что</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_31" n="HIAT:w" s="T59.tx-KA.1">попросишь</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T246" id="Seg_34" n="sc" s="T243">
               <ts e="T246" id="Seg_36" n="HIAT:u" s="T243">
                  <ts e="T243.tx-KA.1" id="Seg_38" n="HIAT:w" s="T243">Не</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243.tx-KA.2" id="Seg_41" n="HIAT:w" s="T243.tx-KA.1">идет</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_44" n="HIAT:w" s="T243.tx-KA.2">она</ts>
                  <nts id="Seg_45" n="HIAT:ip">.</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T256" id="Seg_47" n="sc" s="T251">
               <ts e="T256" id="Seg_49" n="HIAT:u" s="T251">
                  <ts e="T251.tx-KA.1" id="Seg_51" n="HIAT:w" s="T251">Беда</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251.tx-KA.2" id="Seg_54" n="HIAT:w" s="T251.tx-KA.1">много</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251.tx-KA.3" id="Seg_57" n="HIAT:w" s="T251.tx-KA.2">силы</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251.tx-KA.4" id="Seg_60" n="HIAT:w" s="T251.tx-KA.3">у</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_63" n="HIAT:w" s="T251.tx-KA.4">нее</ts>
                  <nts id="Seg_64" n="HIAT:ip">.</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T269" id="Seg_66" n="sc" s="T268">
               <ts e="T269" id="Seg_68" n="HIAT:u" s="T268">
                  <ts e="T269" id="Seg_70" n="HIAT:w" s="T268">Mhmh</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T327" id="Seg_73" n="sc" s="T326">
               <ts e="T327" id="Seg_75" n="HIAT:u" s="T326">
                  <ts e="T327" id="Seg_77" n="HIAT:w" s="T326">Mhmh</ts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T335" id="Seg_80" n="sc" s="T330">
               <ts e="T330.tx-KA.1" id="Seg_82" n="HIAT:u" s="T330">
                  <ts e="T330.tx-KA.1" id="Seg_84" n="HIAT:w" s="T330">Э</ts>
                  <nts id="Seg_85" n="HIAT:ip">…</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T335" id="Seg_88" n="HIAT:u" s="T330.tx-KA.1">
                  <ts e="T330.tx-KA.2" id="Seg_90" n="HIAT:w" s="T330.tx-KA.1">Чем-то</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_93" n="HIAT:w" s="T330.tx-KA.2">пахнет</ts>
                  <nts id="Seg_94" n="HIAT:ip">.</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T346" id="Seg_96" n="sc" s="T344">
               <ts e="T346" id="Seg_98" n="HIAT:u" s="T344">
                  <ts e="T346" id="Seg_100" n="HIAT:w" s="T344">Cлышит</ts>
                  <nts id="Seg_101" n="HIAT:ip">…</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T409" id="Seg_103" n="sc" s="T406">
               <ts e="T409" id="Seg_105" n="HIAT:u" s="T406">
                  <ts e="T406.tx-KA.1" id="Seg_107" n="HIAT:w" s="T406">Скоро</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406.tx-KA.2" id="Seg_110" n="HIAT:w" s="T406.tx-KA.1">домой</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_113" n="HIAT:w" s="T406.tx-KA.2">приеду</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T478" id="Seg_116" n="sc" s="T477">
               <ts e="T478" id="Seg_118" n="HIAT:u" s="T477">
                  <ts e="T478" id="Seg_120" n="HIAT:w" s="T477">Mhmh</ts>
                  <nts id="Seg_121" n="HIAT:ip">.</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T539" id="Seg_123" n="sc" s="T536">
               <ts e="T539" id="Seg_125" n="HIAT:u" s="T536">
                  <ts e="T536.tx-KA.1" id="Seg_127" n="HIAT:w" s="T536">Ничего</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536.tx-KA.2" id="Seg_130" n="HIAT:w" s="T536.tx-KA.1">не</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_133" n="HIAT:w" s="T536.tx-KA.2">видать</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T592" id="Seg_136" n="sc" s="T585">
               <ts e="T592" id="Seg_138" n="HIAT:u" s="T585">
                  <ts e="T585.tx-KA.1" id="Seg_140" n="HIAT:w" s="T585">А</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585.tx-KA.2" id="Seg_143" n="HIAT:w" s="T585.tx-KA.1">то</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585.tx-KA.3" id="Seg_146" n="HIAT:w" s="T585.tx-KA.2">проколешь</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585.tx-KA.4" id="Seg_149" n="HIAT:w" s="T585.tx-KA.3">или</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_152" n="HIAT:w" s="T585.tx-KA.4">порежешь</ts>
                  <nts id="Seg_153" n="HIAT:ip">…</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T626" id="Seg_155" n="sc" s="T622">
               <ts e="T626" id="Seg_157" n="HIAT:u" s="T622">
                  <ts e="T622.tx-KA.1" id="Seg_159" n="HIAT:w" s="T622">Что</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622.tx-KA.2" id="Seg_162" n="HIAT:w" s="T622.tx-KA.1">тебе</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_165" n="HIAT:w" s="T622.tx-KA.2">говорить</ts>
                  <nts id="Seg_166" n="HIAT:ip">…</nts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T669" id="Seg_168" n="sc" s="T666">
               <ts e="T669" id="Seg_170" n="HIAT:u" s="T666">
                  <ts e="T666.tx-KA.1" id="Seg_172" n="HIAT:w" s="T666">Надо</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666.tx-KA.2" id="Seg_175" n="HIAT:w" s="T666.tx-KA.1">пойти</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_178" n="HIAT:w" s="T666.tx-KA.2">искать</ts>
                  <nts id="Seg_179" n="HIAT:ip">.</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T684" id="Seg_181" n="sc" s="T682">
               <ts e="T684" id="Seg_183" n="HIAT:u" s="T682">
                  <ts e="T682.tx-KA.1" id="Seg_185" n="HIAT:w" s="T682">Долго</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_188" n="HIAT:w" s="T682.tx-KA.1">была</ts>
                  <nts id="Seg_189" n="HIAT:ip">.</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T696" id="Seg_191" n="sc" s="T692">
               <ts e="T696" id="Seg_193" n="HIAT:u" s="T692">
                  <ts e="T693" id="Seg_195" n="HIAT:w" s="T692">Спала</ts>
                  <nts id="Seg_196" n="HIAT:ip">,</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_199" n="HIAT:w" s="T693">ничего</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_202" n="HIAT:w" s="T694">не</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_205" n="HIAT:w" s="T695">видела</ts>
                  <nts id="Seg_206" n="HIAT:ip">.</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T901" id="Seg_208" n="sc" s="T745">
               <ts e="T901" id="Seg_210" n="HIAT:u" s="T745">
                  <nts id="Seg_211" n="HIAT:ip">(</nts>
                  <ts e="T745.tx-KA.1" id="Seg_213" n="HIAT:w" s="T745">Этой</ts>
                  <nts id="Seg_214" n="HIAT:ip">)</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745.tx-KA.2" id="Seg_217" n="HIAT:w" s="T745.tx-KA.1">рукой</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745.tx-KA.3" id="Seg_220" n="HIAT:w" s="T745.tx-KA.2">ничего</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745.tx-KA.4" id="Seg_223" n="HIAT:w" s="T745.tx-KA.3">делать</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745.tx-KA.5" id="Seg_226" n="HIAT:w" s="T745.tx-KA.4">не</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_229" n="HIAT:w" s="T745.tx-KA.5">могу</ts>
                  <nts id="Seg_230" n="HIAT:ip">.</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T758" id="Seg_232" n="sc" s="T755">
               <ts e="T758" id="Seg_234" n="HIAT:u" s="T755">
                  <ts e="T755.tx-KA.1" id="Seg_236" n="HIAT:w" s="T755">Пока</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755.tx-KA.2" id="Seg_239" n="HIAT:w" s="T755.tx-KA.1">не</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_242" n="HIAT:w" s="T755.tx-KA.2">зарастёт</ts>
                  <nts id="Seg_243" n="HIAT:ip">.</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T779" id="Seg_245" n="sc" s="T775">
               <ts e="T777" id="Seg_247" n="HIAT:u" s="T775">
                  <ts e="T775.tx-KA.1" id="Seg_249" n="HIAT:w" s="T775">И</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_252" n="HIAT:w" s="T775.tx-KA.1">упала</ts>
                  <nts id="Seg_253" n="HIAT:ip">.</nts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T779" id="Seg_256" n="HIAT:u" s="T777">
                  <ts e="T777.tx-KA.1" id="Seg_258" n="HIAT:w" s="T777">И</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_261" n="HIAT:w" s="T777.tx-KA.1">упала</ts>
                  <nts id="Seg_262" n="HIAT:ip">.</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T857" id="Seg_264" n="sc" s="T856">
               <ts e="T857" id="Seg_266" n="HIAT:u" s="T856">
                  <ts e="T857" id="Seg_268" n="HIAT:w" s="T856">Kuŋgə</ts>
                  <nts id="Seg_269" n="HIAT:ip">.</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KA">
            <ts e="T4" id="Seg_271" n="sc" s="T0">
               <ts e="T4" id="Seg_273" n="e" s="T0">Klavdia Plotnikova, viies lint. </ts>
            </ts>
            <ts e="T39" id="Seg_274" n="sc" s="T38">
               <ts e="T39" id="Seg_276" n="e" s="T38">Mhmh. </ts>
            </ts>
            <ts e="T61" id="Seg_277" n="sc" s="T59">
               <ts e="T61" id="Seg_279" n="e" s="T59">Что попросишь. </ts>
            </ts>
            <ts e="T246" id="Seg_280" n="sc" s="T243">
               <ts e="T246" id="Seg_282" n="e" s="T243">Не идет она. </ts>
            </ts>
            <ts e="T256" id="Seg_283" n="sc" s="T251">
               <ts e="T256" id="Seg_285" n="e" s="T251">Беда много силы у нее. </ts>
            </ts>
            <ts e="T269" id="Seg_286" n="sc" s="T268">
               <ts e="T269" id="Seg_288" n="e" s="T268">Mhmh. </ts>
            </ts>
            <ts e="T327" id="Seg_289" n="sc" s="T326">
               <ts e="T327" id="Seg_291" n="e" s="T326">Mhmh. </ts>
            </ts>
            <ts e="T335" id="Seg_292" n="sc" s="T330">
               <ts e="T335" id="Seg_294" n="e" s="T330">Э… Чем-то пахнет. </ts>
            </ts>
            <ts e="T346" id="Seg_295" n="sc" s="T344">
               <ts e="T346" id="Seg_297" n="e" s="T344">Cлышит… </ts>
            </ts>
            <ts e="T409" id="Seg_298" n="sc" s="T406">
               <ts e="T409" id="Seg_300" n="e" s="T406">Скоро домой приеду. </ts>
            </ts>
            <ts e="T478" id="Seg_301" n="sc" s="T477">
               <ts e="T478" id="Seg_303" n="e" s="T477">Mhmh. </ts>
            </ts>
            <ts e="T539" id="Seg_304" n="sc" s="T536">
               <ts e="T539" id="Seg_306" n="e" s="T536">Ничего не видать. </ts>
            </ts>
            <ts e="T592" id="Seg_307" n="sc" s="T585">
               <ts e="T592" id="Seg_309" n="e" s="T585">А то проколешь или порежешь… </ts>
            </ts>
            <ts e="T626" id="Seg_310" n="sc" s="T622">
               <ts e="T626" id="Seg_312" n="e" s="T622">Что тебе говорить… </ts>
            </ts>
            <ts e="T669" id="Seg_313" n="sc" s="T666">
               <ts e="T669" id="Seg_315" n="e" s="T666">Надо пойти искать. </ts>
            </ts>
            <ts e="T684" id="Seg_316" n="sc" s="T682">
               <ts e="T684" id="Seg_318" n="e" s="T682">Долго была. </ts>
            </ts>
            <ts e="T696" id="Seg_319" n="sc" s="T692">
               <ts e="T693" id="Seg_321" n="e" s="T692">Спала, </ts>
               <ts e="T694" id="Seg_323" n="e" s="T693">ничего </ts>
               <ts e="T695" id="Seg_325" n="e" s="T694">не </ts>
               <ts e="T696" id="Seg_327" n="e" s="T695">видела. </ts>
            </ts>
            <ts e="T901" id="Seg_328" n="sc" s="T745">
               <ts e="T901" id="Seg_330" n="e" s="T745">(Этой) рукой ничего делать не могу. </ts>
            </ts>
            <ts e="T758" id="Seg_331" n="sc" s="T755">
               <ts e="T758" id="Seg_333" n="e" s="T755">Пока не зарастёт. </ts>
            </ts>
            <ts e="T779" id="Seg_334" n="sc" s="T775">
               <ts e="T777" id="Seg_336" n="e" s="T775">И упала. </ts>
               <ts e="T779" id="Seg_338" n="e" s="T777">И упала. </ts>
            </ts>
            <ts e="T857" id="Seg_339" n="sc" s="T856">
               <ts e="T857" id="Seg_341" n="e" s="T856">Kuŋgə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KA">
            <ta e="T4" id="Seg_342" s="T0">PKZ_1964_SU0207.KA.001 (001)</ta>
            <ta e="T39" id="Seg_343" s="T38">PKZ_1964_SU0207.KA.002 (003.004)</ta>
            <ta e="T61" id="Seg_344" s="T59">PKZ_1964_SU0207.KA.003 (004.006)</ta>
            <ta e="T246" id="Seg_345" s="T243">PKZ_1964_SU0207.KA.004 (012.002)</ta>
            <ta e="T256" id="Seg_346" s="T251">PKZ_1964_SU0207.KA.005 (012.004) </ta>
            <ta e="T269" id="Seg_347" s="T268">PKZ_1964_SU0207.KA.006 (014)</ta>
            <ta e="T327" id="Seg_348" s="T326">PKZ_1964_SU0207.KA.007 (019.005)</ta>
            <ta e="T335" id="Seg_349" s="T330">PKZ_1964_SU0207.KA.008 (020.002)</ta>
            <ta e="T346" id="Seg_350" s="T344">PKZ_1964_SU0207.KA.009 (020.005)</ta>
            <ta e="T409" id="Seg_351" s="T406">PKZ_1964_SU0207.KA.010 (024.008)</ta>
            <ta e="T478" id="Seg_352" s="T477">PKZ_1964_SU0207.KA.011 (028)</ta>
            <ta e="T539" id="Seg_353" s="T536">PKZ_1964_SU0207.KA.012 (032.003)</ta>
            <ta e="T592" id="Seg_354" s="T585">PKZ_1964_SU0207.KA.013 (034.012)</ta>
            <ta e="T626" id="Seg_355" s="T622">PKZ_1964_SU0207.KA.014 (037.001)</ta>
            <ta e="T669" id="Seg_356" s="T666">PKZ_1964_SU0207.KA.015 (040.003)</ta>
            <ta e="T684" id="Seg_357" s="T682">PKZ_1964_SU0207.KA.016 (041.002)</ta>
            <ta e="T696" id="Seg_358" s="T692">PKZ_1964_SU0207.KA.017 (041.004)</ta>
            <ta e="T901" id="Seg_359" s="T745">PKZ_1964_SU0207.KA.018 (044.004)</ta>
            <ta e="T758" id="Seg_360" s="T755">PKZ_1964_SU0207.KA.019 (044.006)</ta>
            <ta e="T777" id="Seg_361" s="T775">PKZ_1964_SU0207.KA.020 (046.002)</ta>
            <ta e="T779" id="Seg_362" s="T777">PKZ_1964_SU0207.KA.021 (046.003)</ta>
            <ta e="T857" id="Seg_363" s="T856">PKZ_1964_SU0207.KA.022 (055.002)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KA">
            <ta e="T4" id="Seg_364" s="T0">Klavdia Plotnikova, viies lint. </ta>
            <ta e="T39" id="Seg_365" s="T38">Mhmh. </ta>
            <ta e="T61" id="Seg_366" s="T59">Что попросишь. </ta>
            <ta e="T246" id="Seg_367" s="T243">Не идет она. </ta>
            <ta e="T256" id="Seg_368" s="T251">Беда много силы у нее. </ta>
            <ta e="T269" id="Seg_369" s="T268">Mhmh. ((DMG))</ta>
            <ta e="T327" id="Seg_370" s="T326">Mhmh. </ta>
            <ta e="T335" id="Seg_371" s="T330">Э… Чем-то пахнет. </ta>
            <ta e="T346" id="Seg_372" s="T344">слышит ((BRK)). </ta>
            <ta e="T409" id="Seg_373" s="T406">Скоро домой приеду. </ta>
            <ta e="T478" id="Seg_374" s="T477">Mhmh. </ta>
            <ta e="T539" id="Seg_375" s="T536">Ничего не видать. </ta>
            <ta e="T592" id="Seg_376" s="T585">А то проколешь или порежешь… </ta>
            <ta e="T626" id="Seg_377" s="T622">Что тебе говорить … </ta>
            <ta e="T669" id="Seg_378" s="T666">Надо пойти искать. </ta>
            <ta e="T684" id="Seg_379" s="T682">Долго была. </ta>
            <ta e="T696" id="Seg_380" s="T692">Спала, ничего не видела. </ta>
            <ta e="T901" id="Seg_381" s="T745">(Этой) рукой ничего делать не могу. </ta>
            <ta e="T758" id="Seg_382" s="T755">Пока не зарастёт. </ta>
            <ta e="T777" id="Seg_383" s="T775">И упала. </ta>
            <ta e="T779" id="Seg_384" s="T777">И упала. </ta>
            <ta e="T857" id="Seg_385" s="T856">Kuŋgə. </ta>
         </annotation>
         <annotation name="CS" tierref="CS-KA">
            <ta e="T4" id="Seg_386" s="T0">EST:ext</ta>
            <ta e="T61" id="Seg_387" s="T59">RUS:ext</ta>
            <ta e="T246" id="Seg_388" s="T243">RUS:ext</ta>
            <ta e="T256" id="Seg_389" s="T251">RUS:ext</ta>
            <ta e="T335" id="Seg_390" s="T330">RUS:ext</ta>
            <ta e="T346" id="Seg_391" s="T344">RUS:ext</ta>
            <ta e="T409" id="Seg_392" s="T406">RUS:ext</ta>
            <ta e="T539" id="Seg_393" s="T536">RUS:ext</ta>
            <ta e="T592" id="Seg_394" s="T585">RUS:ext</ta>
            <ta e="T626" id="Seg_395" s="T622">RUS:ext</ta>
            <ta e="T669" id="Seg_396" s="T666">RUS:ext</ta>
            <ta e="T684" id="Seg_397" s="T682">RUS:ext</ta>
            <ta e="T696" id="Seg_398" s="T692">RUS:ext</ta>
            <ta e="T901" id="Seg_399" s="T745">RUS:ext</ta>
            <ta e="T758" id="Seg_400" s="T755">RUS:ext</ta>
            <ta e="T779" id="Seg_401" s="T775">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KA">
            <ta e="T4" id="Seg_402" s="T0">Клавдия Плотникова, пятая плёнка. </ta>
            <ta e="T246" id="Seg_403" s="T243">Не идет она. </ta>
            <ta e="T256" id="Seg_404" s="T251">Беда много силы у нее. </ta>
            <ta e="T335" id="Seg_405" s="T330">Чем-то пахнет. </ta>
            <ta e="T409" id="Seg_406" s="T406">Скоро домой приеду. </ta>
            <ta e="T592" id="Seg_407" s="T585">А то проколешь или порежешь… </ta>
            <ta e="T901" id="Seg_408" s="T745">(Этой) рукой ничего делать не могу. </ta>
            <ta e="T758" id="Seg_409" s="T755">Пока не зарастёт. </ta>
            <ta e="T777" id="Seg_410" s="T775">И упала. </ta>
            <ta e="T779" id="Seg_411" s="T777">И упала. </ta>
            <ta e="T857" id="Seg_412" s="T856">Далеко.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-KA">
            <ta e="T4" id="Seg_413" s="T0">Klavdiya Plotnikova, fifth tape.</ta>
            <ta e="T61" id="Seg_414" s="T59">Whatever you ask.</ta>
            <ta e="T246" id="Seg_415" s="T243">It does not go.</ta>
            <ta e="T256" id="Seg_416" s="T251">It has so much strength.</ta>
            <ta e="T335" id="Seg_417" s="T330">It smells something.</ta>
            <ta e="T346" id="Seg_418" s="T344">It hears…</ta>
            <ta e="T409" id="Seg_419" s="T406">Soon I will come home.</ta>
            <ta e="T539" id="Seg_420" s="T536">One can't see anything.</ta>
            <ta e="T592" id="Seg_421" s="T585">Or else you prick or cut (yourself)…</ta>
            <ta e="T626" id="Seg_422" s="T622">What should I tell you?</ta>
            <ta e="T669" id="Seg_423" s="T666">I should go to look for (it?).</ta>
            <ta e="T684" id="Seg_424" s="T682">I stayed for a long time.</ta>
            <ta e="T696" id="Seg_425" s="T692">I slept, I did not see anything.</ta>
            <ta e="T901" id="Seg_426" s="T745">I cannot do anything with this arm.</ta>
            <ta e="T758" id="Seg_427" s="T755">Until it grows together.</ta>
            <ta e="T777" id="Seg_428" s="T775">And it fell.</ta>
            <ta e="T779" id="Seg_429" s="T777">And it fell.</ta>
            <ta e="T857" id="Seg_430" s="T856">Far.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KA">
            <ta e="T4" id="Seg_431" s="T0">Klavdiya Plotnikova, fünftes Band.</ta>
            <ta e="T61" id="Seg_432" s="T59">Was auch immer man verlangt.</ta>
            <ta e="T246" id="Seg_433" s="T243">Es will nicht gehen.</ta>
            <ta e="T256" id="Seg_434" s="T251">Es hat so viel Kraft.</ta>
            <ta e="T335" id="Seg_435" s="T330">Etwas riecht.</ta>
            <ta e="T346" id="Seg_436" s="T344">Es hört…</ta>
            <ta e="T409" id="Seg_437" s="T406">Bald werde ich nach Hause gehen.</ta>
            <ta e="T539" id="Seg_438" s="T536">Man kann nichts sehen.</ta>
            <ta e="T592" id="Seg_439" s="T585">Sonst stichst oder schneidest (du dich)…</ta>
            <ta e="T626" id="Seg_440" s="T622">Was soll ich dir erzählen?</ta>
            <ta e="T669" id="Seg_441" s="T666">Ich sollte es suchen (gehen?).</ta>
            <ta e="T684" id="Seg_442" s="T682">Ich blieb lange.</ta>
            <ta e="T696" id="Seg_443" s="T692">Ich schlief, ich habe nichts gesehen.</ta>
            <ta e="T901" id="Seg_444" s="T745">Ich kann mit diesem Arm nichts machen. </ta>
            <ta e="T758" id="Seg_445" s="T755">Bis er wieder zusammenwächst.</ta>
            <ta e="T777" id="Seg_446" s="T775">Und es fiel.</ta>
            <ta e="T779" id="Seg_447" s="T777">Und es fiel.</ta>
            <ta e="T857" id="Seg_448" s="T856">Weit weg.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KA">
            <ta e="T4" id="Seg_449" s="T0">[KlT:] This tape should be fourth?</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-PKZ"
                      id="tx-PKZ"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T83" start="T78">
            <tli id="T78.tx-PKZ.1" />
            <tli id="T78.tx-PKZ.2" />
            <tli id="T78.tx-PKZ.3" />
         </timeline-fork>
         <timeline-fork end="T304" start="T303">
            <tli id="T303.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T331" start="T327">
            <tli id="T327.tx-PKZ.1" />
            <tli id="T327.tx-PKZ.2" />
            <tli id="T327.tx-PKZ.3" />
            <tli id="T327.tx-PKZ.4" />
         </timeline-fork>
         <timeline-fork end="T421" start="T420">
            <tli id="T420.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T433" start="T432">
            <tli id="T432.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T489" start="T486">
            <tli id="T486.tx-PKZ.1" />
            <tli id="T486.tx-PKZ.2" />
         </timeline-fork>
         <timeline-fork end="T613" start="T612">
            <tli id="T612.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T752" start="T901">
            <tli id="T901.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T839" start="T834">
            <tli id="T834.tx-PKZ.1" />
            <tli id="T834.tx-PKZ.2" />
            <tli id="T834.tx-PKZ.3" />
            <tli id="T834.tx-PKZ.4" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PKZ">
            <ts e="T38" id="Seg_450" n="sc" s="T4">
               <ts e="T12" id="Seg_452" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_454" n="HIAT:w" s="T4">Măn</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_457" n="HIAT:w" s="T5">ej</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_460" n="HIAT:w" s="T6">moliam</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_462" n="HIAT:ip">(</nts>
                  <ts e="T8" id="Seg_464" n="HIAT:w" s="T7">mĭngz</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_467" n="HIAT:w" s="T8">-</ts>
                  <nts id="Seg_468" n="HIAT:ip">)</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_470" n="HIAT:ip">(</nts>
                  <ts e="T10" id="Seg_472" n="HIAT:w" s="T9">mĭŋgəlsʼtə</ts>
                  <nts id="Seg_473" n="HIAT:ip">)</nts>
                  <nts id="Seg_474" n="HIAT:ip">,</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_477" n="HIAT:w" s="T10">bar</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_480" n="HIAT:w" s="T11">ĭzemneʔbə</ts>
                  <nts id="Seg_481" n="HIAT:ip">.</nts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_484" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_486" n="HIAT:w" s="T12">Adnaka</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_489" n="HIAT:w" s="T13">măn</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_492" n="HIAT:w" s="T14">külaːmbiam</ts>
                  <nts id="Seg_493" n="HIAT:ip">.</nts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_496" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_498" n="HIAT:w" s="T15">Teinen</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_501" n="HIAT:w" s="T16">dĭ</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_504" n="HIAT:w" s="T17">koʔbdo</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_507" n="HIAT:w" s="T18">urgo</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_510" n="HIAT:w" s="T19">dʼala</ts>
                  <nts id="Seg_511" n="HIAT:ip">.</nts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_514" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_516" n="HIAT:w" s="T20">Dĭm</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_519" n="HIAT:w" s="T21">teinen</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_521" n="HIAT:ip">(</nts>
                  <ts e="T23" id="Seg_523" n="HIAT:w" s="T22">ia</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_526" n="HIAT:w" s="T23">-</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_529" n="HIAT:w" s="T24">iad</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_532" n="HIAT:w" s="T25">-</ts>
                  <nts id="Seg_533" n="HIAT:ip">)</nts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_536" n="HIAT:w" s="T26">ijad</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_539" n="HIAT:w" s="T27">deʔpi</ts>
                  <nts id="Seg_540" n="HIAT:ip">.</nts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_543" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_545" n="HIAT:w" s="T28">Dĭʔnə</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_547" n="HIAT:ip">(</nts>
                  <ts e="T30" id="Seg_549" n="HIAT:w" s="T29">bʼeʔ</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_552" n="HIAT:w" s="T30">š</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_555" n="HIAT:w" s="T31">-</ts>
                  <nts id="Seg_556" n="HIAT:ip">)</nts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_559" n="HIAT:w" s="T32">bʼeʔ</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_562" n="HIAT:w" s="T33">šide</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_565" n="HIAT:w" s="T34">pʼe</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_568" n="HIAT:w" s="T35">i</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_571" n="HIAT:w" s="T36">iššo</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_574" n="HIAT:w" s="T37">šide</ts>
                  <nts id="Seg_575" n="HIAT:ip">.</nts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T59" id="Seg_577" n="sc" s="T39">
               <ts e="T43" id="Seg_579" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_581" n="HIAT:w" s="T39">Găda</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_584" n="HIAT:w" s="T40">dĭ</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_587" n="HIAT:w" s="T41">kuza</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_590" n="HIAT:w" s="T42">marʼi</ts>
                  <nts id="Seg_591" n="HIAT:ip">.</nts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_594" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_596" n="HIAT:w" s="T43">Ĭmbidə</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_599" n="HIAT:w" s="T44">ej</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_602" n="HIAT:w" s="T45">ile</ts>
                  <nts id="Seg_603" n="HIAT:ip">.</nts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_606" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_608" n="HIAT:w" s="T46">Ĭmbidə</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_611" n="HIAT:w" s="T47">ej</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_614" n="HIAT:w" s="T48">mĭlej</ts>
                  <nts id="Seg_615" n="HIAT:ip">.</nts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_618" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_620" n="HIAT:w" s="T49">A</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_623" n="HIAT:w" s="T50">baška</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_626" n="HIAT:w" s="T51">kuza</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_629" n="HIAT:w" s="T52">ugaːndə</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_632" n="HIAT:w" s="T53">jakšə</ts>
                  <nts id="Seg_633" n="HIAT:ip">.</nts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_636" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_638" n="HIAT:w" s="T54">Bar</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_641" n="HIAT:w" s="T55">mĭlüʔləj</ts>
                  <nts id="Seg_642" n="HIAT:ip">,</nts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_644" n="HIAT:ip">(</nts>
                  <ts e="T57" id="Seg_646" n="HIAT:w" s="T56">ĭm</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_649" n="HIAT:w" s="T57">-</ts>
                  <nts id="Seg_650" n="HIAT:ip">)</nts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_653" n="HIAT:w" s="T58">ĭmbi</ts>
                  <nts id="Seg_654" n="HIAT:ip">.</nts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T243" id="Seg_656" n="sc" s="T61">
               <ts e="T69" id="Seg_658" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_660" n="HIAT:w" s="T61">Ĭmbi</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_663" n="HIAT:w" s="T62">ej</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_665" n="HIAT:ip">(</nts>
                  <ts e="T64" id="Seg_667" n="HIAT:w" s="T63">pʼer</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_670" n="HIAT:w" s="T64">-</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_673" n="HIAT:w" s="T65">m</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_676" n="HIAT:w" s="T66">-</ts>
                  <nts id="Seg_677" n="HIAT:ip">)</nts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_680" n="HIAT:w" s="T67">ĭmbi</ts>
                  <nts id="Seg_681" n="HIAT:ip">…</nts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_684" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_686" n="HIAT:w" s="T69">Ĭmbi</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_689" n="HIAT:w" s="T70">ej</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_692" n="HIAT:w" s="T71">pilel</ts>
                  <nts id="Seg_693" n="HIAT:ip">,</nts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_696" n="HIAT:w" s="T72">bar</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_699" n="HIAT:w" s="T73">mĭlej</ts>
                  <nts id="Seg_700" n="HIAT:ip">.</nts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_703" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_705" n="HIAT:w" s="T74">Dĭn</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_708" n="HIAT:w" s="T75">ugaːndə</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_711" n="HIAT:w" s="T76">sĭjdə</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_714" n="HIAT:w" s="T77">jakšə</ts>
                  <nts id="Seg_715" n="HIAT:ip">.</nts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_718" n="HIAT:u" s="T78">
                  <ts e="T78.tx-PKZ.1" id="Seg_720" n="HIAT:w" s="T78">Как</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78.tx-PKZ.2" id="Seg_723" n="HIAT:w" s="T78.tx-PKZ.1">это</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78.tx-PKZ.3" id="Seg_726" n="HIAT:w" s="T78.tx-PKZ.2">сразу</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_729" n="HIAT:w" s="T78.tx-PKZ.3">ты</ts>
                  <nts id="Seg_730" n="HIAT:ip">…</nts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_733" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_735" n="HIAT:w" s="T83">Ara</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_738" n="HIAT:w" s="T84">ej</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_741" n="HIAT:w" s="T85">bĭtlie</ts>
                  <nts id="Seg_742" n="HIAT:ip">.</nts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_745" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_747" n="HIAT:w" s="T86">Ĭmbi</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_750" n="HIAT:w" s="T87">ige</ts>
                  <nts id="Seg_751" n="HIAT:ip">,</nts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_754" n="HIAT:w" s="T88">bar</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_757" n="HIAT:w" s="T89">mĭləj</ts>
                  <nts id="Seg_758" n="HIAT:ip">.</nts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_761" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_763" n="HIAT:w" s="T90">Munujʔ</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_766" n="HIAT:w" s="T91">mĭlie</ts>
                  <nts id="Seg_767" n="HIAT:ip">,</nts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_769" n="HIAT:ip">(</nts>
                  <ts e="T93" id="Seg_771" n="HIAT:w" s="T92">uj</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_774" n="HIAT:w" s="T93">-</ts>
                  <nts id="Seg_775" n="HIAT:ip">)</nts>
                  <nts id="Seg_776" n="HIAT:ip">.</nts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_779" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_781" n="HIAT:w" s="T94">Kajaʔ</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_784" n="HIAT:w" s="T95">mĭlie</ts>
                  <nts id="Seg_785" n="HIAT:ip">,</nts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_788" n="HIAT:w" s="T96">ipek</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_791" n="HIAT:w" s="T97">mĭlie</ts>
                  <nts id="Seg_792" n="HIAT:ip">.</nts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_795" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_797" n="HIAT:w" s="T98">Uja</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_800" n="HIAT:w" s="T99">ige</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_803" n="HIAT:w" s="T100">dăk</ts>
                  <nts id="Seg_804" n="HIAT:ip">,</nts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_807" n="HIAT:w" s="T101">mĭləj</ts>
                  <nts id="Seg_808" n="HIAT:ip">.</nts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_811" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_813" n="HIAT:w" s="T102">Bar</ts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_816" n="HIAT:w" s="T103">ĭmbi</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_819" n="HIAT:w" s="T104">ige</ts>
                  <nts id="Seg_820" n="HIAT:ip">,</nts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_823" n="HIAT:w" s="T105">bar</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_826" n="HIAT:w" s="T106">mĭləj</ts>
                  <nts id="Seg_827" n="HIAT:ip">.</nts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_830" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_832" n="HIAT:w" s="T107">Ugaːndə</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_834" n="HIAT:ip">(</nts>
                  <ts e="T109" id="Seg_836" n="HIAT:w" s="T108">nʼu</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_839" n="HIAT:w" s="T109">-</ts>
                  <nts id="Seg_840" n="HIAT:ip">)</nts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_843" n="HIAT:w" s="T110">nʼuʔnən</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_846" n="HIAT:w" s="T111">kuzurie</ts>
                  <nts id="Seg_847" n="HIAT:ip">.</nts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_850" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_852" n="HIAT:w" s="T112">Kudaj</ts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_855" n="HIAT:w" s="T113">kuzurleʔbə</ts>
                  <nts id="Seg_856" n="HIAT:ip">.</nts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_859" n="HIAT:u" s="T114">
                  <nts id="Seg_860" n="HIAT:ip">(</nts>
                  <ts e="T115" id="Seg_862" n="HIAT:w" s="T114">Šindən-</ts>
                  <nts id="Seg_863" n="HIAT:ip">)</nts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_866" n="HIAT:w" s="T115">Šindi-nʼibudʼ</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_869" n="HIAT:w" s="T116">kutləj</ts>
                  <nts id="Seg_870" n="HIAT:ip">.</nts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_873" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_875" n="HIAT:w" s="T117">Raduga</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_878" n="HIAT:w" s="T118">nulaʔbə</ts>
                  <nts id="Seg_879" n="HIAT:ip">.</nts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T121" id="Seg_882" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_884" n="HIAT:w" s="T119">Bü</ts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_887" n="HIAT:w" s="T120">ileʔbə</ts>
                  <nts id="Seg_888" n="HIAT:ip">.</nts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_891" n="HIAT:u" s="T121">
                  <ts e="T122" id="Seg_893" n="HIAT:w" s="T121">Dĭgəttə</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_896" n="HIAT:w" s="T122">surno</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_899" n="HIAT:w" s="T123">kalləj</ts>
                  <nts id="Seg_900" n="HIAT:ip">.</nts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_903" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_905" n="HIAT:w" s="T124">Toskanak</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_908" n="HIAT:w" s="T125">kalləj</ts>
                  <nts id="Seg_909" n="HIAT:ip">.</nts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_912" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_914" n="HIAT:w" s="T126">Ugaːndə</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_917" n="HIAT:w" s="T127">tutše</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_920" n="HIAT:w" s="T128">sagər</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_923" n="HIAT:w" s="T129">šonəga</ts>
                  <nts id="Seg_924" n="HIAT:ip">.</nts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_927" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_929" n="HIAT:w" s="T130">Măn</ts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_932" n="HIAT:w" s="T131">nʼiʔdə</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_935" n="HIAT:w" s="T132">kambiam</ts>
                  <nts id="Seg_936" n="HIAT:ip">.</nts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_939" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_941" n="HIAT:w" s="T133">Kukuška</ts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_944" n="HIAT:w" s="T134">bar</ts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_947" n="HIAT:w" s="T135">nʼergölaʔbə</ts>
                  <nts id="Seg_948" n="HIAT:ip">.</nts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_951" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_953" n="HIAT:w" s="T136">Măndolam</ts>
                  <nts id="Seg_954" n="HIAT:ip">,</nts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_957" n="HIAT:w" s="T137">dĭ</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_960" n="HIAT:w" s="T138">turanə</ts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_963" n="HIAT:w" s="T139">amnəbi</ts>
                  <nts id="Seg_964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_966" n="HIAT:w" s="T140">bar</ts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_969" n="HIAT:w" s="T141">i</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_972" n="HIAT:w" s="T142">kirgarlaʔbə</ts>
                  <nts id="Seg_973" n="HIAT:ip">.</nts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_976" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_978" n="HIAT:w" s="T143">Măn</ts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_981" n="HIAT:w" s="T144">măndəm:</ts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_984" n="HIAT:w" s="T145">unnʼa</ts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_986" n="HIAT:ip">(</nts>
                  <ts e="T147" id="Seg_988" n="HIAT:w" s="T146">amnolaʔbə-</ts>
                  <nts id="Seg_989" n="HIAT:ip">)</nts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_992" n="HIAT:w" s="T147">amnolam</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_995" n="HIAT:w" s="T148">bar</ts>
                  <nts id="Seg_996" n="HIAT:ip">.</nts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_999" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_1001" n="HIAT:w" s="T149">Verno</ts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_1004" n="HIAT:w" s="T150">unnʼa</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_1007" n="HIAT:w" s="T151">amnolam</ts>
                  <nts id="Seg_1008" n="HIAT:ip">.</nts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_1011" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_1013" n="HIAT:w" s="T152">Pizʼiʔ</ts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_1016" n="HIAT:w" s="T153">bar</ts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_1019" n="HIAT:w" s="T154">barəʔpi</ts>
                  <nts id="Seg_1020" n="HIAT:ip">.</nts>
                  <nts id="Seg_1021" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_1023" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_1025" n="HIAT:w" s="T155">Pazʼiʔ</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_1028" n="HIAT:w" s="T156">bar</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_1031" n="HIAT:w" s="T157">barəʔpi</ts>
                  <nts id="Seg_1032" n="HIAT:ip">.</nts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_1035" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_1037" n="HIAT:w" s="T158">Dĭ</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_1040" n="HIAT:w" s="T159">uge</ts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_1043" n="HIAT:w" s="T160">amnolaʔbə</ts>
                  <nts id="Seg_1044" n="HIAT:ip">.</nts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T162" id="Seg_1047" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_1049" n="HIAT:w" s="T161">Kirgarlaʔbə</ts>
                  <nts id="Seg_1050" n="HIAT:ip">.</nts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_1053" n="HIAT:u" s="T162">
                  <ts e="T163" id="Seg_1055" n="HIAT:w" s="T162">Dĭgəttə</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_1058" n="HIAT:w" s="T163">nʼergölüʔpi</ts>
                  <nts id="Seg_1059" n="HIAT:ip">,</nts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_1062" n="HIAT:w" s="T164">gijen</ts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_1065" n="HIAT:w" s="T165">kuja</ts>
                  <nts id="Seg_1066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_1068" n="HIAT:w" s="T166">uʔbdlaʔbə</ts>
                  <nts id="Seg_1069" n="HIAT:ip">.</nts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_1072" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_1074" n="HIAT:w" s="T167">Ugaːndə</ts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_1077" n="HIAT:w" s="T168">külüʔsəbi</ts>
                  <nts id="Seg_1078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_1080" n="HIAT:w" s="T169">il</ts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_1083" n="HIAT:w" s="T170">bar</ts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_1086" n="HIAT:w" s="T171">pʼaŋdəbiʔi</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_1089" n="HIAT:w" s="T172">sazəndə</ts>
                  <nts id="Seg_1090" n="HIAT:ip">.</nts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_1093" n="HIAT:u" s="T173">
                  <ts e="T174" id="Seg_1095" n="HIAT:w" s="T173">Dĭ</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_1098" n="HIAT:w" s="T174">kuza</ts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_1101" n="HIAT:w" s="T175">ugaːndə</ts>
                  <nts id="Seg_1102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_1104" n="HIAT:w" s="T176">ej</ts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_1107" n="HIAT:w" s="T177">jakšə</ts>
                  <nts id="Seg_1108" n="HIAT:ip">.</nts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T183" id="Seg_1111" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_1113" n="HIAT:w" s="T178">Bar</ts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_1116" n="HIAT:w" s="T179">ĭmbi</ts>
                  <nts id="Seg_1117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_1119" n="HIAT:w" s="T180">ileʔbə</ts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_1122" n="HIAT:w" s="T181">da</ts>
                  <nts id="Seg_1123" n="HIAT:ip">…</nts>
                  <nts id="Seg_1124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T187" id="Seg_1126" n="HIAT:u" s="T183">
                  <ts e="T184" id="Seg_1128" n="HIAT:w" s="T183">Bar</ts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_1131" n="HIAT:w" s="T184">ĭmbi</ts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_1134" n="HIAT:w" s="T185">ileʔbə</ts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_1137" n="HIAT:w" s="T186">bar</ts>
                  <nts id="Seg_1138" n="HIAT:ip">.</nts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_1141" n="HIAT:u" s="T187">
                  <ts e="T188" id="Seg_1143" n="HIAT:w" s="T187">Ugaːndə</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_1146" n="HIAT:w" s="T188">toli</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_1149" n="HIAT:w" s="T189">kuza</ts>
                  <nts id="Seg_1150" n="HIAT:ip">.</nts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T195" id="Seg_1153" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_1155" n="HIAT:w" s="T190">Bar</ts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_1158" n="HIAT:w" s="T191">ĭmbi</ts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_1161" n="HIAT:w" s="T192">ilie</ts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_1164" n="HIAT:w" s="T193">dăk</ts>
                  <nts id="Seg_1165" n="HIAT:ip">…</nts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T200" id="Seg_1168" n="HIAT:u" s="T195">
                  <ts e="T196" id="Seg_1170" n="HIAT:w" s="T195">Bar</ts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_1173" n="HIAT:w" s="T196">ileʔbə</ts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_1176" n="HIAT:w" s="T197">bar</ts>
                  <nts id="Seg_1177" n="HIAT:ip">,</nts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_1180" n="HIAT:w" s="T198">kundlaʔbə</ts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_1183" n="HIAT:w" s="T199">maʔndə</ts>
                  <nts id="Seg_1184" n="HIAT:ip">.</nts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T205" id="Seg_1187" n="HIAT:u" s="T200">
                  <ts e="T201" id="Seg_1189" n="HIAT:w" s="T200">Măn</ts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_1192" n="HIAT:w" s="T201">teinen</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_1195" n="HIAT:w" s="T202">koʔbdonə</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_1198" n="HIAT:w" s="T203">platəm</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_1201" n="HIAT:w" s="T204">ibiem</ts>
                  <nts id="Seg_1202" n="HIAT:ip">.</nts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_1205" n="HIAT:u" s="T205">
                  <ts e="T206" id="Seg_1207" n="HIAT:w" s="T205">Dĭ</ts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_1210" n="HIAT:w" s="T206">kambi</ts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_1213" n="HIAT:w" s="T207">Malinovkanə</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_1216" n="HIAT:w" s="T208">tüšəlzittə</ts>
                  <nts id="Seg_1217" n="HIAT:ip">.</nts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T216" id="Seg_1220" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_1222" n="HIAT:w" s="T209">Dĭn</ts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_1225" n="HIAT:w" s="T210">nüke</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_1228" n="HIAT:w" s="T211">surarlaʔbə:</ts>
                  <nts id="Seg_1229" n="HIAT:ip">”</nts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_1232" n="HIAT:w" s="T212">Gijen</ts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_1235" n="HIAT:w" s="T213">tăn</ts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_1238" n="HIAT:w" s="T214">plat</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_1241" n="HIAT:w" s="T215">ibiel</ts>
                  <nts id="Seg_1242" n="HIAT:ip">?</nts>
                  <nts id="Seg_1243" n="HIAT:ip">”</nts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_1246" n="HIAT:u" s="T216">
                  <ts e="T217" id="Seg_1248" n="HIAT:w" s="T216">Urgaja</ts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_1251" n="HIAT:w" s="T217">mĭbi</ts>
                  <nts id="Seg_1252" n="HIAT:ip">.</nts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T222" id="Seg_1255" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_1257" n="HIAT:w" s="T218">A</ts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_1260" n="HIAT:w" s="T219">urgaja</ts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_1263" n="HIAT:w" s="T220">gijen</ts>
                  <nts id="Seg_1264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_1266" n="HIAT:w" s="T221">ibi</ts>
                  <nts id="Seg_1267" n="HIAT:ip">?</nts>
                  <nts id="Seg_1268" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_1270" n="HIAT:u" s="T222">
                  <ts e="T223" id="Seg_1272" n="HIAT:w" s="T222">Polʼa</ts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_1275" n="HIAT:w" s="T223">mĭbi</ts>
                  <nts id="Seg_1276" n="HIAT:ip">.</nts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T230" id="Seg_1279" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_1281" n="HIAT:w" s="T224">Dö</ts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_1284" n="HIAT:w" s="T225">ine</ts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_1287" n="HIAT:w" s="T226">ibiem</ts>
                  <nts id="Seg_1288" n="HIAT:ip">,</nts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_1291" n="HIAT:w" s="T227">iʔgö</ts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_1294" n="HIAT:w" s="T228">aktʼa</ts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_1297" n="HIAT:w" s="T229">mĭbiem</ts>
                  <nts id="Seg_1298" n="HIAT:ip">.</nts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_1301" n="HIAT:u" s="T230">
                  <ts e="T231" id="Seg_1303" n="HIAT:w" s="T230">A</ts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_1306" n="HIAT:w" s="T231">baška</ts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_1309" n="HIAT:w" s="T232">ine</ts>
                  <nts id="Seg_1310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_1312" n="HIAT:w" s="T233">ibiem</ts>
                  <nts id="Seg_1313" n="HIAT:ip">.</nts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_1316" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_1318" n="HIAT:w" s="T234">Amga</ts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_1321" n="HIAT:w" s="T235">mĭbiem</ts>
                  <nts id="Seg_1322" n="HIAT:ip">.</nts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T237" id="Seg_1325" n="HIAT:u" s="T236">
                  <ts e="T886" id="Seg_1327" n="HIAT:w" s="T236">Много</ts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_1330" n="HIAT:w" s="T886">отдала</ts>
                  <nts id="Seg_1331" n="HIAT:ip">.</nts>
                  <nts id="Seg_1332" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T241" id="Seg_1334" n="HIAT:u" s="T237">
                  <ts e="T238" id="Seg_1336" n="HIAT:w" s="T237">Dʼaʔpiam</ts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1338" n="HIAT:ip">(</nts>
                  <ts e="T239" id="Seg_1340" n="HIAT:w" s="T238">budo</ts>
                  <nts id="Seg_1341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_1343" n="HIAT:w" s="T239">-</ts>
                  <nts id="Seg_1344" n="HIAT:ip">)</nts>
                  <nts id="Seg_1345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_1347" n="HIAT:w" s="T240">buzo</ts>
                  <nts id="Seg_1348" n="HIAT:ip">.</nts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_1351" n="HIAT:u" s="T241">
                  <ts e="T243" id="Seg_1353" n="HIAT:w" s="T241">Bar</ts>
                  <nts id="Seg_1354" n="HIAT:ip">…</nts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T251" id="Seg_1356" n="sc" s="T246">
               <ts e="T248" id="Seg_1358" n="HIAT:u" s="T246">
                  <ts e="T247" id="Seg_1360" n="HIAT:w" s="T246">Ej</ts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_1363" n="HIAT:w" s="T247">kandəga</ts>
                  <nts id="Seg_1364" n="HIAT:ip">.</nts>
                  <nts id="Seg_1365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_1367" n="HIAT:u" s="T248">
                  <ts e="T249" id="Seg_1369" n="HIAT:w" s="T248">Măn</ts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_1372" n="HIAT:w" s="T249">bar</ts>
                  <nts id="Seg_1373" n="HIAT:ip">…</nts>
                  <nts id="Seg_1374" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T268" id="Seg_1375" n="sc" s="T256">
               <ts e="T258" id="Seg_1377" n="HIAT:u" s="T256">
                  <ts e="T257" id="Seg_1379" n="HIAT:w" s="T256">Ugaːndə</ts>
                  <nts id="Seg_1380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_1382" n="HIAT:w" s="T257">kuštu</ts>
                  <nts id="Seg_1383" n="HIAT:ip">.</nts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T262" id="Seg_1386" n="HIAT:u" s="T258">
                  <ts e="T259" id="Seg_1388" n="HIAT:w" s="T258">Ej</ts>
                  <nts id="Seg_1389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_1391" n="HIAT:w" s="T259">moliam</ts>
                  <nts id="Seg_1392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_1394" n="HIAT:w" s="T260">kunzittə</ts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1397" n="HIAT:w" s="T261">bar</ts>
                  <nts id="Seg_1398" n="HIAT:ip">.</nts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T268" id="Seg_1401" n="HIAT:u" s="T262">
                  <nts id="Seg_1402" n="HIAT:ip">(</nts>
                  <ts e="T263" id="Seg_1404" n="HIAT:w" s="T262">Kundla</ts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1407" n="HIAT:w" s="T263">-</ts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1410" n="HIAT:w" s="T264">kundəga</ts>
                  <nts id="Seg_1411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1413" n="HIAT:w" s="T265">-</ts>
                  <nts id="Seg_1414" n="HIAT:ip">)</nts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1417" n="HIAT:w" s="T266">Kandəgam</ts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1420" n="HIAT:w" s="T267">bar</ts>
                  <nts id="Seg_1421" n="HIAT:ip">.</nts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T326" id="Seg_1423" n="sc" s="T269">
               <ts e="T273" id="Seg_1425" n="HIAT:u" s="T269">
                  <ts e="T270" id="Seg_1427" n="HIAT:w" s="T269">Ej</ts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1430" n="HIAT:w" s="T270">jakšə</ts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1432" n="HIAT:ip">(</nts>
                  <ts e="T272" id="Seg_1434" n="HIAT:w" s="T271">nʼi</ts>
                  <nts id="Seg_1435" n="HIAT:ip">)</nts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1438" n="HIAT:w" s="T272">nʼi</ts>
                  <nts id="Seg_1439" n="HIAT:ip">.</nts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T278" id="Seg_1442" n="HIAT:u" s="T273">
                  <ts e="T274" id="Seg_1444" n="HIAT:w" s="T273">Üge</ts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1447" n="HIAT:w" s="T274">bar</ts>
                  <nts id="Seg_1448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1450" n="HIAT:w" s="T275">tajirlaʔbə</ts>
                  <nts id="Seg_1451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1453" n="HIAT:w" s="T276">da</ts>
                  <nts id="Seg_1454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1456" n="HIAT:w" s="T277">kundlaʔbə</ts>
                  <nts id="Seg_1457" n="HIAT:ip">.</nts>
                  <nts id="Seg_1458" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T281" id="Seg_1460" n="HIAT:u" s="T278">
                  <ts e="T279" id="Seg_1462" n="HIAT:w" s="T278">Măn</ts>
                  <nts id="Seg_1463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1465" n="HIAT:w" s="T279">dʼijegən</ts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1468" n="HIAT:w" s="T280">mĭmbiem</ts>
                  <nts id="Seg_1469" n="HIAT:ip">.</nts>
                  <nts id="Seg_1470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T285" id="Seg_1472" n="HIAT:u" s="T281">
                  <ts e="T282" id="Seg_1474" n="HIAT:w" s="T281">Ĭmbidə</ts>
                  <nts id="Seg_1475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1476" n="HIAT:ip">(</nts>
                  <ts e="T283" id="Seg_1478" n="HIAT:w" s="T282">ku</ts>
                  <nts id="Seg_1479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1481" n="HIAT:w" s="T283">-</ts>
                  <nts id="Seg_1482" n="HIAT:ip">)</nts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1485" n="HIAT:w" s="T284">kubiam</ts>
                  <nts id="Seg_1486" n="HIAT:ip">.</nts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T289" id="Seg_1489" n="HIAT:u" s="T285">
                  <ts e="T286" id="Seg_1491" n="HIAT:w" s="T285">Dĭgəttə</ts>
                  <nts id="Seg_1492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1494" n="HIAT:w" s="T286">măn</ts>
                  <nts id="Seg_1495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1497" n="HIAT:w" s="T287">nererlüʔpiem</ts>
                  <nts id="Seg_1498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1500" n="HIAT:w" s="T288">bar</ts>
                  <nts id="Seg_1501" n="HIAT:ip">.</nts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T294" id="Seg_1504" n="HIAT:u" s="T289">
                  <ts e="T290" id="Seg_1506" n="HIAT:w" s="T289">Dĭgəttə</ts>
                  <nts id="Seg_1507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1508" n="HIAT:ip">(</nts>
                  <ts e="T291" id="Seg_1510" n="HIAT:w" s="T290">kuž</ts>
                  <nts id="Seg_1511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1513" n="HIAT:w" s="T291">-</ts>
                  <nts id="Seg_1514" n="HIAT:ip">)</nts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1517" n="HIAT:w" s="T292">kudaj</ts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1520" n="HIAT:w" s="T293">kăštəbiam</ts>
                  <nts id="Seg_1521" n="HIAT:ip">.</nts>
                  <nts id="Seg_1522" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1524" n="HIAT:u" s="T294">
                  <ts e="T295" id="Seg_1526" n="HIAT:w" s="T294">Bar</ts>
                  <nts id="Seg_1527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1529" n="HIAT:w" s="T295">kalla</ts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1532" n="HIAT:w" s="T296">dʼürbiʔi</ts>
                  <nts id="Seg_1533" n="HIAT:ip">.</nts>
                  <nts id="Seg_1534" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_1536" n="HIAT:u" s="T297">
                  <ts e="T298" id="Seg_1538" n="HIAT:w" s="T297">Nada</ts>
                  <nts id="Seg_1539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1541" n="HIAT:w" s="T298">šiʔi</ts>
                  <nts id="Seg_1542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1544" n="HIAT:w" s="T299">păjdəsʼtə</ts>
                  <nts id="Seg_1545" n="HIAT:ip">.</nts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303" id="Seg_1548" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_1550" n="HIAT:w" s="T300">Nada</ts>
                  <nts id="Seg_1551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1553" n="HIAT:w" s="T301">varota</ts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1556" n="HIAT:w" s="T302">azittə</ts>
                  <nts id="Seg_1557" n="HIAT:ip">.</nts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303.tx-PKZ.1" id="Seg_1560" n="HIAT:u" s="T303">
                  <nts id="Seg_1561" n="HIAT:ip">(</nts>
                  <nts id="Seg_1562" n="HIAT:ip">(</nts>
                  <ats e="T303.tx-PKZ.1" id="Seg_1563" n="HIAT:non-pho" s="T303">a-</ats>
                  <nts id="Seg_1564" n="HIAT:ip">)</nts>
                  <nts id="Seg_1565" n="HIAT:ip">)</nts>
                  <nts id="Seg_1566" n="HIAT:ip">…</nts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T304" id="Seg_1569" n="HIAT:u" s="T303.tx-PKZ.1">
                  <nts id="Seg_1570" n="HIAT:ip">(</nts>
                  <nts id="Seg_1571" n="HIAT:ip">(</nts>
                  <ats e="T304" id="Seg_1572" n="HIAT:non-pho" s="T303.tx-PKZ.1">BRK</ats>
                  <nts id="Seg_1573" n="HIAT:ip">)</nts>
                  <nts id="Seg_1574" n="HIAT:ip">)</nts>
                  <nts id="Seg_1575" n="HIAT:ip">.</nts>
                  <nts id="Seg_1576" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T314" id="Seg_1578" n="HIAT:u" s="T304">
                  <nts id="Seg_1579" n="HIAT:ip">(</nts>
                  <ts e="T305" id="Seg_1581" n="HIAT:w" s="T304">Dĭ</ts>
                  <nts id="Seg_1582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1584" n="HIAT:w" s="T305">ši</ts>
                  <nts id="Seg_1585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1587" n="HIAT:w" s="T306">-</ts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1590" n="HIAT:w" s="T307">š</ts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1593" n="HIAT:w" s="T308">-</ts>
                  <nts id="Seg_1594" n="HIAT:ip">)</nts>
                  <nts id="Seg_1595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1597" n="HIAT:w" s="T309">Dĭ</ts>
                  <nts id="Seg_1598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1600" n="HIAT:w" s="T310">šiʔnə</ts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1603" n="HIAT:w" s="T311">nada</ts>
                  <nts id="Seg_1604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1606" n="HIAT:w" s="T312">paʔi</ts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1609" n="HIAT:w" s="T313">păʔsittə</ts>
                  <nts id="Seg_1610" n="HIAT:ip">.</nts>
                  <nts id="Seg_1611" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T317" id="Seg_1613" n="HIAT:u" s="T314">
                  <ts e="T315" id="Seg_1615" n="HIAT:w" s="T314">Dĭgəttə</ts>
                  <nts id="Seg_1616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1618" n="HIAT:w" s="T315">šeden</ts>
                  <nts id="Seg_1619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1621" n="HIAT:w" s="T316">kajləbəj</ts>
                  <nts id="Seg_1622" n="HIAT:ip">.</nts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T321" id="Seg_1625" n="HIAT:u" s="T317">
                  <ts e="T318" id="Seg_1627" n="HIAT:w" s="T317">Dĭgəttə</ts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1630" n="HIAT:w" s="T318">buzo</ts>
                  <nts id="Seg_1631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1633" n="HIAT:w" s="T319">ej</ts>
                  <nts id="Seg_1634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1636" n="HIAT:w" s="T320">nuʔmələj</ts>
                  <nts id="Seg_1637" n="HIAT:ip">.</nts>
                  <nts id="Seg_1638" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T326" id="Seg_1640" n="HIAT:u" s="T321">
                  <nts id="Seg_1641" n="HIAT:ip">(</nts>
                  <ts e="T322" id="Seg_1643" n="HIAT:w" s="T321">Šedengən</ts>
                  <nts id="Seg_1644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1646" n="HIAT:w" s="T322">amnolaʔ</ts>
                  <nts id="Seg_1647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1649" n="HIAT:w" s="T323">-</ts>
                  <nts id="Seg_1650" n="HIAT:ip">)</nts>
                  <nts id="Seg_1651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1653" n="HIAT:w" s="T324">Šedengən</ts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1656" n="HIAT:w" s="T325">nulaʔbə</ts>
                  <nts id="Seg_1657" n="HIAT:ip">.</nts>
                  <nts id="Seg_1658" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T331" id="Seg_1659" n="sc" s="T327">
               <ts e="T331" id="Seg_1661" n="HIAT:u" s="T327">
                  <nts id="Seg_1662" n="HIAT:ip">(</nts>
                  <nts id="Seg_1663" n="HIAT:ip">(</nts>
                  <ats e="T327.tx-PKZ.1" id="Seg_1664" n="HIAT:non-pho" s="T327">DMG</ats>
                  <nts id="Seg_1665" n="HIAT:ip">)</nts>
                  <nts id="Seg_1666" n="HIAT:ip">)</nts>
                  <nts id="Seg_1667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327.tx-PKZ.2" id="Seg_1669" n="HIAT:w" s="T327.tx-PKZ.1">Ну</ts>
                  <nts id="Seg_1670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327.tx-PKZ.3" id="Seg_1672" n="HIAT:w" s="T327.tx-PKZ.2">там</ts>
                  <nts id="Seg_1673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327.tx-PKZ.4" id="Seg_1675" n="HIAT:w" s="T327.tx-PKZ.3">скажи</ts>
                  <nts id="Seg_1676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1678" n="HIAT:w" s="T327.tx-PKZ.4">чего</ts>
                  <nts id="Seg_1679" n="HIAT:ip">.</nts>
                  <nts id="Seg_1680" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T344" id="Seg_1681" n="sc" s="T335">
               <ts e="T340" id="Seg_1683" n="HIAT:u" s="T335">
                  <nts id="Seg_1684" n="HIAT:ip">(</nts>
                  <ts e="T336" id="Seg_1686" n="HIAT:w" s="T335">Ĭm</ts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1689" n="HIAT:w" s="T336">-</ts>
                  <nts id="Seg_1690" n="HIAT:ip">)</nts>
                  <nts id="Seg_1691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1693" n="HIAT:w" s="T337">Ĭmbizʼidə</ts>
                  <nts id="Seg_1694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1696" n="HIAT:w" s="T338">bar</ts>
                  <nts id="Seg_1697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1699" n="HIAT:w" s="T339">putʼəmnia</ts>
                  <nts id="Seg_1700" n="HIAT:ip">.</nts>
                  <nts id="Seg_1701" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T344" id="Seg_1703" n="HIAT:u" s="T340">
                  <ts e="T341" id="Seg_1705" n="HIAT:w" s="T340">Tăn</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1708" n="HIAT:w" s="T341">püjel</ts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1711" n="HIAT:w" s="T342">ugaːndə</ts>
                  <nts id="Seg_1712" n="HIAT:ip">…</nts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T406" id="Seg_1714" n="sc" s="T346">
               <ts e="T352" id="Seg_1716" n="HIAT:u" s="T346">
                  <ts e="T347" id="Seg_1718" n="HIAT:w" s="T346">Tăn</ts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1721" n="HIAT:w" s="T347">püjel</ts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1724" n="HIAT:w" s="T348">ugaːndə</ts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1727" n="HIAT:w" s="T349">puʔmə</ts>
                  <nts id="Seg_1728" n="HIAT:ip">,</nts>
                  <nts id="Seg_1729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1731" n="HIAT:w" s="T350">bar</ts>
                  <nts id="Seg_1732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1734" n="HIAT:w" s="T351">nʼilgölaʔbə</ts>
                  <nts id="Seg_1735" n="HIAT:ip">.</nts>
                  <nts id="Seg_1736" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T355" id="Seg_1738" n="HIAT:u" s="T352">
                  <ts e="T353" id="Seg_1740" n="HIAT:w" s="T352">Tăn</ts>
                  <nts id="Seg_1741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1743" n="HIAT:w" s="T353">gijen</ts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1746" n="HIAT:w" s="T354">ibiel</ts>
                  <nts id="Seg_1747" n="HIAT:ip">?</nts>
                  <nts id="Seg_1748" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T357" id="Seg_1750" n="HIAT:u" s="T355">
                  <ts e="T356" id="Seg_1752" n="HIAT:w" s="T355">Büjleʔ</ts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1755" n="HIAT:w" s="T356">mĭmbiem</ts>
                  <nts id="Seg_1756" n="HIAT:ip">.</nts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T361" id="Seg_1759" n="HIAT:u" s="T357">
                  <ts e="T358" id="Seg_1761" n="HIAT:w" s="T357">Nada</ts>
                  <nts id="Seg_1762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1764" n="HIAT:w" s="T358">segi</ts>
                  <nts id="Seg_1765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1767" n="HIAT:w" s="T359">bü</ts>
                  <nts id="Seg_1768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1770" n="HIAT:w" s="T360">mĭnzərzittə</ts>
                  <nts id="Seg_1771" n="HIAT:ip">.</nts>
                  <nts id="Seg_1772" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T366" id="Seg_1774" n="HIAT:u" s="T361">
                  <ts e="T362" id="Seg_1776" n="HIAT:w" s="T361">Nada</ts>
                  <nts id="Seg_1777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1779" n="HIAT:w" s="T362">mĭje</ts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1781" n="HIAT:ip">(</nts>
                  <ts e="T364" id="Seg_1783" n="HIAT:w" s="T363">mĭnə</ts>
                  <nts id="Seg_1784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1786" n="HIAT:w" s="T364">-</ts>
                  <nts id="Seg_1787" n="HIAT:ip">)</nts>
                  <nts id="Seg_1788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1790" n="HIAT:w" s="T365">mĭnzərzittə</ts>
                  <nts id="Seg_1791" n="HIAT:ip">.</nts>
                  <nts id="Seg_1792" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T368" id="Seg_1794" n="HIAT:u" s="T366">
                  <ts e="T367" id="Seg_1796" n="HIAT:w" s="T366">Tibizeŋ</ts>
                  <nts id="Seg_1797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1799" n="HIAT:w" s="T367">bădəzʼittə</ts>
                  <nts id="Seg_1800" n="HIAT:ip">.</nts>
                  <nts id="Seg_1801" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T372" id="Seg_1803" n="HIAT:u" s="T368">
                  <ts e="T369" id="Seg_1805" n="HIAT:w" s="T368">Măn</ts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1808" n="HIAT:w" s="T369">bʼeʔ</ts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1811" n="HIAT:w" s="T370">aktʼa</ts>
                  <nts id="Seg_1812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1814" n="HIAT:w" s="T371">ige</ts>
                  <nts id="Seg_1815" n="HIAT:ip">.</nts>
                  <nts id="Seg_1816" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_1818" n="HIAT:u" s="T372">
                  <ts e="T373" id="Seg_1820" n="HIAT:w" s="T372">Nada</ts>
                  <nts id="Seg_1821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1823" n="HIAT:w" s="T373">tănan</ts>
                  <nts id="Seg_1824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1826" n="HIAT:w" s="T374">sumna</ts>
                  <nts id="Seg_1827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1829" n="HIAT:w" s="T375">mĭzittə</ts>
                  <nts id="Seg_1830" n="HIAT:ip">.</nts>
                  <nts id="Seg_1831" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T379" id="Seg_1833" n="HIAT:u" s="T376">
                  <ts e="T377" id="Seg_1835" n="HIAT:w" s="T376">Sumna</ts>
                  <nts id="Seg_1836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1838" n="HIAT:w" s="T377">boskəndə</ts>
                  <nts id="Seg_1839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1841" n="HIAT:w" s="T378">mazittə</ts>
                  <nts id="Seg_1842" n="HIAT:ip">.</nts>
                  <nts id="Seg_1843" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T385" id="Seg_1845" n="HIAT:u" s="T379">
                  <ts e="T380" id="Seg_1847" n="HIAT:w" s="T379">Možet</ts>
                  <nts id="Seg_1848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1850" n="HIAT:w" s="T380">giber-nʼibudʼ</ts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1853" n="HIAT:w" s="T381">kallal</ts>
                  <nts id="Seg_1854" n="HIAT:ip">,</nts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1857" n="HIAT:w" s="T382">aktʼa</ts>
                  <nts id="Seg_1858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1860" n="HIAT:w" s="T383">kereʔ</ts>
                  <nts id="Seg_1861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1863" n="HIAT:w" s="T384">moləj</ts>
                  <nts id="Seg_1864" n="HIAT:ip">.</nts>
                  <nts id="Seg_1865" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T386" id="Seg_1867" n="HIAT:u" s="T385">
                  <nts id="Seg_1868" n="HIAT:ip">(</nts>
                  <nts id="Seg_1869" n="HIAT:ip">(</nts>
                  <ats e="T386" id="Seg_1870" n="HIAT:non-pho" s="T385">DMG</ats>
                  <nts id="Seg_1871" n="HIAT:ip">)</nts>
                  <nts id="Seg_1872" n="HIAT:ip">)</nts>
                  <nts id="Seg_1873" n="HIAT:ip">.</nts>
                  <nts id="Seg_1874" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T389" id="Seg_1876" n="HIAT:u" s="T386">
                  <ts e="T387" id="Seg_1878" n="HIAT:w" s="T386">Matvejev</ts>
                  <nts id="Seg_1879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1881" n="HIAT:w" s="T387">bar</ts>
                  <nts id="Seg_1882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1884" n="HIAT:w" s="T388">amnolaʔbə</ts>
                  <nts id="Seg_1885" n="HIAT:ip">.</nts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T391" id="Seg_1888" n="HIAT:u" s="T389">
                  <ts e="T390" id="Seg_1890" n="HIAT:w" s="T389">Ej</ts>
                  <nts id="Seg_1891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1893" n="HIAT:w" s="T390">dʼăbaktəria</ts>
                  <nts id="Seg_1894" n="HIAT:ip">.</nts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T393" id="Seg_1897" n="HIAT:u" s="T391">
                  <ts e="T392" id="Seg_1899" n="HIAT:w" s="T391">Sĭjdə</ts>
                  <nts id="Seg_1900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1902" n="HIAT:w" s="T392">ĭzemnie</ts>
                  <nts id="Seg_1903" n="HIAT:ip">.</nts>
                  <nts id="Seg_1904" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T396" id="Seg_1906" n="HIAT:u" s="T393">
                  <ts e="T394" id="Seg_1908" n="HIAT:w" s="T393">Maʔnal</ts>
                  <nts id="Seg_1909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1911" n="HIAT:w" s="T394">sazən</ts>
                  <nts id="Seg_1912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1914" n="HIAT:w" s="T395">naga</ts>
                  <nts id="Seg_1915" n="HIAT:ip">.</nts>
                  <nts id="Seg_1916" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T399" id="Seg_1918" n="HIAT:u" s="T396">
                  <ts e="T397" id="Seg_1920" n="HIAT:w" s="T396">Net</ts>
                  <nts id="Seg_1921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1923" n="HIAT:w" s="T397">bar</ts>
                  <nts id="Seg_1924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1926" n="HIAT:w" s="T398">kuroluʔpi</ts>
                  <nts id="Seg_1927" n="HIAT:ip">.</nts>
                  <nts id="Seg_1928" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T404" id="Seg_1930" n="HIAT:u" s="T399">
                  <ts e="T400" id="Seg_1932" n="HIAT:w" s="T399">Ej</ts>
                  <nts id="Seg_1933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1935" n="HIAT:w" s="T400">pʼaŋdəlia</ts>
                  <nts id="Seg_1936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1937" n="HIAT:ip">(</nts>
                  <ts e="T402" id="Seg_1939" n="HIAT:w" s="T401">sažə</ts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1942" n="HIAT:w" s="T402">-</ts>
                  <nts id="Seg_1943" n="HIAT:ip">)</nts>
                  <nts id="Seg_1944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1946" n="HIAT:w" s="T403">sazən</ts>
                  <nts id="Seg_1947" n="HIAT:ip">.</nts>
                  <nts id="Seg_1948" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T406" id="Seg_1950" n="HIAT:u" s="T404">
                  <ts e="T405" id="Seg_1952" n="HIAT:w" s="T404">Iʔ</ts>
                  <nts id="Seg_1953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1955" n="HIAT:w" s="T405">kuroʔ</ts>
                  <nts id="Seg_1956" n="HIAT:ip">!</nts>
                  <nts id="Seg_1957" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T477" id="Seg_1958" n="sc" s="T409">
               <ts e="T412" id="Seg_1960" n="HIAT:u" s="T409">
                  <ts e="T410" id="Seg_1962" n="HIAT:w" s="T409">Büžü</ts>
                  <nts id="Seg_1963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1965" n="HIAT:w" s="T410">maʔnən</ts>
                  <nts id="Seg_1966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1968" n="HIAT:w" s="T411">šolam</ts>
                  <nts id="Seg_1969" n="HIAT:ip">.</nts>
                  <nts id="Seg_1970" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T415" id="Seg_1972" n="HIAT:u" s="T412">
                  <ts e="T413" id="Seg_1974" n="HIAT:w" s="T412">Ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_1975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1977" n="HIAT:w" s="T413">tănan</ts>
                  <nts id="Seg_1978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1980" n="HIAT:w" s="T414">detlem</ts>
                  <nts id="Seg_1981" n="HIAT:ip">.</nts>
                  <nts id="Seg_1982" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T418" id="Seg_1984" n="HIAT:u" s="T415">
                  <ts e="T416" id="Seg_1986" n="HIAT:w" s="T415">Tăŋ</ts>
                  <nts id="Seg_1987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1989" n="HIAT:w" s="T416">ej</ts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1992" n="HIAT:w" s="T417">dʼăbaktəraʔ</ts>
                  <nts id="Seg_1993" n="HIAT:ip">.</nts>
                  <nts id="Seg_1994" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T420" id="Seg_1996" n="HIAT:u" s="T418">
                  <ts e="T419" id="Seg_1998" n="HIAT:w" s="T418">Koldʼəŋ</ts>
                  <nts id="Seg_1999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_2001" n="HIAT:w" s="T419">dʼăbaktəraʔ</ts>
                  <nts id="Seg_2002" n="HIAT:ip">!</nts>
                  <nts id="Seg_2003" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T424" id="Seg_2005" n="HIAT:u" s="T420">
                  <ts e="T420.tx-PKZ.1" id="Seg_2007" n="HIAT:w" s="T420">A</ts>
                  <nts id="Seg_2008" n="HIAT:ip">_</nts>
                  <ts e="T421" id="Seg_2010" n="HIAT:w" s="T420.tx-PKZ.1">to</ts>
                  <nts id="Seg_2011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_2013" n="HIAT:w" s="T421">bar</ts>
                  <nts id="Seg_2014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_2016" n="HIAT:w" s="T422">il</ts>
                  <nts id="Seg_2017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_2019" n="HIAT:w" s="T423">nünieʔi</ts>
                  <nts id="Seg_2020" n="HIAT:ip">.</nts>
                  <nts id="Seg_2021" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T427" id="Seg_2023" n="HIAT:u" s="T424">
                  <ts e="T425" id="Seg_2025" n="HIAT:w" s="T424">Tăŋ</ts>
                  <nts id="Seg_2026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_2028" n="HIAT:w" s="T425">iʔ</ts>
                  <nts id="Seg_2029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_2031" n="HIAT:w" s="T426">kirgaraʔ</ts>
                  <nts id="Seg_2032" n="HIAT:ip">!</nts>
                  <nts id="Seg_2033" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T440" id="Seg_2035" n="HIAT:u" s="T427">
                  <ts e="T428" id="Seg_2037" n="HIAT:w" s="T427">A</ts>
                  <nts id="Seg_2038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2039" n="HIAT:ip">(</nts>
                  <ts e="T429" id="Seg_2041" n="HIAT:w" s="T428">iʔ</ts>
                  <nts id="Seg_2042" n="HIAT:ip">)</nts>
                  <nts id="Seg_2043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_2045" n="HIAT:w" s="T429">tăn</ts>
                  <nts id="Seg_2046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_2048" n="HIAT:w" s="T430">iʔ</ts>
                  <nts id="Seg_2049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_2051" n="HIAT:w" s="T431">kudonzaʔ</ts>
                  <nts id="Seg_2052" n="HIAT:ip">,</nts>
                  <nts id="Seg_2053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432.tx-PKZ.1" id="Seg_2055" n="HIAT:w" s="T432">a</ts>
                  <nts id="Seg_2056" n="HIAT:ip">_</nts>
                  <ts e="T433" id="Seg_2058" n="HIAT:w" s="T432.tx-PKZ.1">to</ts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_2061" n="HIAT:w" s="T433">il</ts>
                  <nts id="Seg_2062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_2064" n="HIAT:w" s="T434">bar</ts>
                  <nts id="Seg_2065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_2067" n="HIAT:w" s="T435">nünleʔbəʔjə</ts>
                  <nts id="Seg_2068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_2070" n="HIAT:w" s="T436">kăda</ts>
                  <nts id="Seg_2071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2072" n="HIAT:ip">(</nts>
                  <ts e="T438" id="Seg_2074" n="HIAT:w" s="T437">tăn</ts>
                  <nts id="Seg_2075" n="HIAT:ip">)</nts>
                  <nts id="Seg_2076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_2078" n="HIAT:w" s="T438">tăn</ts>
                  <nts id="Seg_2079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_2081" n="HIAT:w" s="T439">kudonzlaʔbəl</ts>
                  <nts id="Seg_2082" n="HIAT:ip">.</nts>
                  <nts id="Seg_2083" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T444" id="Seg_2085" n="HIAT:u" s="T440">
                  <ts e="T441" id="Seg_2087" n="HIAT:w" s="T440">Măn</ts>
                  <nts id="Seg_2088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_2090" n="HIAT:w" s="T441">teinen</ts>
                  <nts id="Seg_2091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_2093" n="HIAT:w" s="T442">oroma</ts>
                  <nts id="Seg_2094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_2096" n="HIAT:w" s="T443">kămnabiam</ts>
                  <nts id="Seg_2097" n="HIAT:ip">.</nts>
                  <nts id="Seg_2098" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T445" id="Seg_2100" n="HIAT:u" s="T444">
                  <ts e="T445" id="Seg_2102" n="HIAT:w" s="T444">Bĭlgarbiam</ts>
                  <nts id="Seg_2103" n="HIAT:ip">.</nts>
                  <nts id="Seg_2104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T447" id="Seg_2106" n="HIAT:u" s="T445">
                  <ts e="T446" id="Seg_2108" n="HIAT:w" s="T445">Krinkaʔi</ts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_2111" n="HIAT:w" s="T446">bəzəjdəbiam</ts>
                  <nts id="Seg_2112" n="HIAT:ip">.</nts>
                  <nts id="Seg_2113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T451" id="Seg_2115" n="HIAT:u" s="T447">
                  <ts e="T448" id="Seg_2117" n="HIAT:w" s="T447">Edəbiem</ts>
                  <nts id="Seg_2118" n="HIAT:ip">,</nts>
                  <nts id="Seg_2119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_2121" n="HIAT:w" s="T448">kuja</ts>
                  <nts id="Seg_2122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_2124" n="HIAT:w" s="T449">koʔlaʔbə</ts>
                  <nts id="Seg_2125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_2127" n="HIAT:w" s="T450">bar</ts>
                  <nts id="Seg_2128" n="HIAT:ip">.</nts>
                  <nts id="Seg_2129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T453" id="Seg_2131" n="HIAT:u" s="T451">
                  <ts e="T452" id="Seg_2133" n="HIAT:w" s="T451">Ĭmbi</ts>
                  <nts id="Seg_2134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_2136" n="HIAT:w" s="T452">deʔpiel</ts>
                  <nts id="Seg_2137" n="HIAT:ip">?</nts>
                  <nts id="Seg_2138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T455" id="Seg_2140" n="HIAT:u" s="T453">
                  <ts e="T454" id="Seg_2142" n="HIAT:w" s="T453">Pʼerdə</ts>
                  <nts id="Seg_2143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_2145" n="HIAT:w" s="T454">măna</ts>
                  <nts id="Seg_2146" n="HIAT:ip">.</nts>
                  <nts id="Seg_2147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T460" id="Seg_2149" n="HIAT:u" s="T455">
                  <nts id="Seg_2150" n="HIAT:ip">(</nts>
                  <ts e="T456" id="Seg_2152" n="HIAT:w" s="T455">Ibi</ts>
                  <nts id="Seg_2153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_2155" n="HIAT:w" s="T456">-</ts>
                  <nts id="Seg_2156" n="HIAT:ip">)</nts>
                  <nts id="Seg_2157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_2159" n="HIAT:w" s="T457">Ĭmbi</ts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_2162" n="HIAT:w" s="T458">dĭn</ts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_2165" n="HIAT:w" s="T459">iʔbölaʔbə</ts>
                  <nts id="Seg_2166" n="HIAT:ip">?</nts>
                  <nts id="Seg_2167" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T464" id="Seg_2169" n="HIAT:u" s="T460">
                  <ts e="T461" id="Seg_2171" n="HIAT:w" s="T460">Măn</ts>
                  <nts id="Seg_2172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_2174" n="HIAT:w" s="T461">teinen</ts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_2177" n="HIAT:w" s="T462">kalbajlaʔ</ts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_2180" n="HIAT:w" s="T463">mĭmbiem</ts>
                  <nts id="Seg_2181" n="HIAT:ip">.</nts>
                  <nts id="Seg_2182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T466" id="Seg_2184" n="HIAT:u" s="T464">
                  <ts e="T465" id="Seg_2186" n="HIAT:w" s="T464">Kalba</ts>
                  <nts id="Seg_2187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_2189" n="HIAT:w" s="T465">deʔpiem</ts>
                  <nts id="Seg_2190" n="HIAT:ip">.</nts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T468" id="Seg_2193" n="HIAT:u" s="T466">
                  <ts e="T467" id="Seg_2195" n="HIAT:w" s="T466">Dĭgəttə</ts>
                  <nts id="Seg_2196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_2198" n="HIAT:w" s="T467">dʼagarlam</ts>
                  <nts id="Seg_2199" n="HIAT:ip">.</nts>
                  <nts id="Seg_2200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T471" id="Seg_2202" n="HIAT:u" s="T468">
                  <ts e="T469" id="Seg_2204" n="HIAT:w" s="T468">Munujʔ</ts>
                  <nts id="Seg_2205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2206" n="HIAT:ip">(</nts>
                  <ts e="T470" id="Seg_2208" n="HIAT:w" s="T469">ene-</ts>
                  <nts id="Seg_2209" n="HIAT:ip">)</nts>
                  <nts id="Seg_2210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_2212" n="HIAT:w" s="T470">ellem</ts>
                  <nts id="Seg_2213" n="HIAT:ip">.</nts>
                  <nts id="Seg_2214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T473" id="Seg_2216" n="HIAT:u" s="T471">
                  <ts e="T472" id="Seg_2218" n="HIAT:w" s="T471">Oroma</ts>
                  <nts id="Seg_2219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_2221" n="HIAT:w" s="T472">ellem</ts>
                  <nts id="Seg_2222" n="HIAT:ip">.</nts>
                  <nts id="Seg_2223" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T477" id="Seg_2225" n="HIAT:u" s="T473">
                  <ts e="T474" id="Seg_2227" n="HIAT:w" s="T473">Dĭgəttə</ts>
                  <nts id="Seg_2228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_2230" n="HIAT:w" s="T474">pürzittə</ts>
                  <nts id="Seg_2231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_2233" n="HIAT:w" s="T475">pirogəʔi</ts>
                  <nts id="Seg_2234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_2236" n="HIAT:w" s="T476">molam</ts>
                  <nts id="Seg_2237" n="HIAT:ip">.</nts>
                  <nts id="Seg_2238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T536" id="Seg_2239" n="sc" s="T478">
               <ts e="T480" id="Seg_2241" n="HIAT:u" s="T478">
                  <ts e="T479" id="Seg_2243" n="HIAT:w" s="T478">Uja</ts>
                  <nts id="Seg_2244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_2246" n="HIAT:w" s="T479">mĭnzerbiem</ts>
                  <nts id="Seg_2247" n="HIAT:ip">.</nts>
                  <nts id="Seg_2248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T483" id="Seg_2250" n="HIAT:u" s="T480">
                  <ts e="T481" id="Seg_2252" n="HIAT:w" s="T480">Uja</ts>
                  <nts id="Seg_2253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_2255" n="HIAT:w" s="T481">ambiʔi</ts>
                  <nts id="Seg_2256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2257" n="HIAT:ip">(</nts>
                  <ts e="T483" id="Seg_2259" n="HIAT:w" s="T482">tĭmeizittə</ts>
                  <nts id="Seg_2260" n="HIAT:ip">)</nts>
                  <nts id="Seg_2261" n="HIAT:ip">.</nts>
                  <nts id="Seg_2262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T486" id="Seg_2264" n="HIAT:u" s="T483">
                  <ts e="T484" id="Seg_2266" n="HIAT:w" s="T483">Onʼiʔ</ts>
                  <nts id="Seg_2267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_2269" n="HIAT:w" s="T484">le</ts>
                  <nts id="Seg_2270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_2272" n="HIAT:w" s="T485">maluppi</ts>
                  <nts id="Seg_2273" n="HIAT:ip">.</nts>
                  <nts id="Seg_2274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T489" id="Seg_2276" n="HIAT:u" s="T486">
                  <ts e="T486.tx-PKZ.1" id="Seg_2278" n="HIAT:w" s="T486">Одни</ts>
                  <nts id="Seg_2279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486.tx-PKZ.2" id="Seg_2281" n="HIAT:w" s="T486.tx-PKZ.1">кости</ts>
                  <nts id="Seg_2282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_2284" n="HIAT:w" s="T486.tx-PKZ.2">остались</ts>
                  <nts id="Seg_2285" n="HIAT:ip">.</nts>
                  <nts id="Seg_2286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T493" id="Seg_2288" n="HIAT:u" s="T489">
                  <ts e="T490" id="Seg_2290" n="HIAT:w" s="T489">Menzeŋ</ts>
                  <nts id="Seg_2291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_2293" n="HIAT:w" s="T490">bar</ts>
                  <nts id="Seg_2294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_2296" n="HIAT:w" s="T491">le</ts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_2299" n="HIAT:w" s="T492">amnaʔbəʔjə</ts>
                  <nts id="Seg_2300" n="HIAT:ip">.</nts>
                  <nts id="Seg_2301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T497" id="Seg_2303" n="HIAT:u" s="T493">
                  <ts e="T494" id="Seg_2305" n="HIAT:w" s="T493">Ugaːndə</ts>
                  <nts id="Seg_2306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_2308" n="HIAT:w" s="T494">iʔgö</ts>
                  <nts id="Seg_2309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_2311" n="HIAT:w" s="T495">le</ts>
                  <nts id="Seg_2312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_2314" n="HIAT:w" s="T496">barəʔpibaʔ</ts>
                  <nts id="Seg_2315" n="HIAT:ip">.</nts>
                  <nts id="Seg_2316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T506" id="Seg_2318" n="HIAT:u" s="T497">
                  <nts id="Seg_2319" n="HIAT:ip">(</nts>
                  <ts e="T498" id="Seg_2321" n="HIAT:w" s="T497">Măn</ts>
                  <nts id="Seg_2322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_2324" n="HIAT:w" s="T498">ten</ts>
                  <nts id="Seg_2325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_2327" n="HIAT:w" s="T499">-</ts>
                  <nts id="Seg_2328" n="HIAT:ip">)</nts>
                  <nts id="Seg_2329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_2331" n="HIAT:w" s="T500">Măn</ts>
                  <nts id="Seg_2332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_2334" n="HIAT:w" s="T501">teinen</ts>
                  <nts id="Seg_2335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_2337" n="HIAT:w" s="T502">ipek</ts>
                  <nts id="Seg_2338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_2340" n="HIAT:w" s="T503">pürbiem</ts>
                  <nts id="Seg_2341" n="HIAT:ip">,</nts>
                  <nts id="Seg_2342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_2344" n="HIAT:w" s="T504">ej</ts>
                  <nts id="Seg_2345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_2347" n="HIAT:w" s="T505">namzəga</ts>
                  <nts id="Seg_2348" n="HIAT:ip">.</nts>
                  <nts id="Seg_2349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T509" id="Seg_2351" n="HIAT:u" s="T506">
                  <ts e="T507" id="Seg_2353" n="HIAT:w" s="T506">Ej</ts>
                  <nts id="Seg_2354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_2356" n="HIAT:w" s="T507">jakšə</ts>
                  <nts id="Seg_2357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_2359" n="HIAT:w" s="T508">bar</ts>
                  <nts id="Seg_2360" n="HIAT:ip">.</nts>
                  <nts id="Seg_2361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T517" id="Seg_2363" n="HIAT:u" s="T509">
                  <ts e="T510" id="Seg_2365" n="HIAT:w" s="T509">Măn</ts>
                  <nts id="Seg_2366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_2368" n="HIAT:w" s="T510">dĭ</ts>
                  <nts id="Seg_2369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_2371" n="HIAT:w" s="T511">ipek</ts>
                  <nts id="Seg_2372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_2374" n="HIAT:w" s="T512">dĭ</ts>
                  <nts id="Seg_2375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2376" n="HIAT:ip">(</nts>
                  <ts e="T514" id="Seg_2378" n="HIAT:w" s="T513">mendə</ts>
                  <nts id="Seg_2379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_2381" n="HIAT:w" s="T514">-</ts>
                  <nts id="Seg_2382" n="HIAT:ip">)</nts>
                  <nts id="Seg_2383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_2385" n="HIAT:w" s="T515">menzeŋdə</ts>
                  <nts id="Seg_2386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_2388" n="HIAT:w" s="T516">mĭbiem</ts>
                  <nts id="Seg_2389" n="HIAT:ip">.</nts>
                  <nts id="Seg_2390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T524" id="Seg_2392" n="HIAT:u" s="T517">
                  <ts e="T518" id="Seg_2394" n="HIAT:w" s="T517">Menzeŋ</ts>
                  <nts id="Seg_2395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_2397" n="HIAT:w" s="T518">bar</ts>
                  <nts id="Seg_2398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_2400" n="HIAT:w" s="T519">ipek</ts>
                  <nts id="Seg_2401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_2403" n="HIAT:w" s="T520">amnaʔbəʔjə</ts>
                  <nts id="Seg_2404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_2406" n="HIAT:w" s="T521">i</ts>
                  <nts id="Seg_2407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_2409" n="HIAT:w" s="T522">bar</ts>
                  <nts id="Seg_2410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_2412" n="HIAT:w" s="T523">dʼabərolaʔbəʔjə</ts>
                  <nts id="Seg_2413" n="HIAT:ip">.</nts>
                  <nts id="Seg_2414" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T530" id="Seg_2416" n="HIAT:u" s="T524">
                  <ts e="T525" id="Seg_2418" n="HIAT:w" s="T524">Men</ts>
                  <nts id="Seg_2419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_2421" n="HIAT:w" s="T525">bar</ts>
                  <nts id="Seg_2422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_2424" n="HIAT:w" s="T526">dĭ</ts>
                  <nts id="Seg_2425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_2427" n="HIAT:w" s="T527">mendə</ts>
                  <nts id="Seg_2428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_2430" n="HIAT:w" s="T528">timnet</ts>
                  <nts id="Seg_2431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_2433" n="HIAT:w" s="T529">bar</ts>
                  <nts id="Seg_2434" n="HIAT:ip">…</nts>
                  <nts id="Seg_2435" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T532" id="Seg_2437" n="HIAT:u" s="T530">
                  <ts e="T531" id="Seg_2439" n="HIAT:w" s="T530">Nudʼi</ts>
                  <nts id="Seg_2440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_2442" n="HIAT:w" s="T531">ibi</ts>
                  <nts id="Seg_2443" n="HIAT:ip">.</nts>
                  <nts id="Seg_2444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T536" id="Seg_2446" n="HIAT:u" s="T532">
                  <ts e="T533" id="Seg_2448" n="HIAT:w" s="T532">Nudʼi</ts>
                  <nts id="Seg_2449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_2451" n="HIAT:w" s="T533">ibi</ts>
                  <nts id="Seg_2452" n="HIAT:ip">,</nts>
                  <nts id="Seg_2453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_2455" n="HIAT:w" s="T534">ugaːndə</ts>
                  <nts id="Seg_2456" n="HIAT:ip">…</nts>
                  <nts id="Seg_2457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T585" id="Seg_2458" n="sc" s="T539">
               <ts e="T542" id="Seg_2460" n="HIAT:u" s="T539">
                  <ts e="T540" id="Seg_2462" n="HIAT:w" s="T539">Ĭmbidə</ts>
                  <nts id="Seg_2463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_2465" n="HIAT:w" s="T540">ej</ts>
                  <nts id="Seg_2466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2468" n="HIAT:w" s="T541">edlia</ts>
                  <nts id="Seg_2469" n="HIAT:ip">.</nts>
                  <nts id="Seg_2470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T546" id="Seg_2472" n="HIAT:u" s="T542">
                  <ts e="T543" id="Seg_2474" n="HIAT:w" s="T542">Măn</ts>
                  <nts id="Seg_2475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2477" n="HIAT:w" s="T543">dĭm</ts>
                  <nts id="Seg_2478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2480" n="HIAT:w" s="T544">ej</ts>
                  <nts id="Seg_2481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2483" n="HIAT:w" s="T545">tĭmnebiem</ts>
                  <nts id="Seg_2484" n="HIAT:ip">.</nts>
                  <nts id="Seg_2485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T548" id="Seg_2487" n="HIAT:u" s="T546">
                  <ts e="T547" id="Seg_2489" n="HIAT:w" s="T546">Šindi</ts>
                  <nts id="Seg_2490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2492" n="HIAT:w" s="T547">šonəga</ts>
                  <nts id="Seg_2493" n="HIAT:ip">.</nts>
                  <nts id="Seg_2494" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T552" id="Seg_2496" n="HIAT:u" s="T548">
                  <ts e="T549" id="Seg_2498" n="HIAT:w" s="T548">Măn</ts>
                  <nts id="Seg_2499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2501" n="HIAT:w" s="T549">abam</ts>
                  <nts id="Seg_2502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2504" n="HIAT:w" s="T550">tujeziʔi</ts>
                  <nts id="Seg_2505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2507" n="HIAT:w" s="T551">abi</ts>
                  <nts id="Seg_2508" n="HIAT:ip">.</nts>
                  <nts id="Seg_2509" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T558" id="Seg_2511" n="HIAT:u" s="T552">
                  <nts id="Seg_2512" n="HIAT:ip">(</nts>
                  <ts e="T553" id="Seg_2514" n="HIAT:w" s="T552">Kuba</ts>
                  <nts id="Seg_2515" n="HIAT:ip">)</nts>
                  <nts id="Seg_2516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2518" n="HIAT:w" s="T553">Kuba</ts>
                  <nts id="Seg_2519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2520" n="HIAT:ip">(</nts>
                  <ts e="T555" id="Seg_2522" n="HIAT:w" s="T554">kür-</ts>
                  <nts id="Seg_2523" n="HIAT:ip">)</nts>
                  <nts id="Seg_2524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2526" n="HIAT:w" s="T555">kürbi</ts>
                  <nts id="Seg_2527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2529" n="HIAT:w" s="T556">i</ts>
                  <nts id="Seg_2530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2532" n="HIAT:w" s="T557">alaʔbəbi</ts>
                  <nts id="Seg_2533" n="HIAT:ip">.</nts>
                  <nts id="Seg_2534" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T560" id="Seg_2536" n="HIAT:u" s="T558">
                  <ts e="T559" id="Seg_2538" n="HIAT:w" s="T558">Šojdʼo</ts>
                  <nts id="Seg_2539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2541" n="HIAT:w" s="T559">šöʔpiʔi</ts>
                  <nts id="Seg_2542" n="HIAT:ip">.</nts>
                  <nts id="Seg_2543" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T562" id="Seg_2545" n="HIAT:u" s="T560">
                  <ts e="T561" id="Seg_2547" n="HIAT:w" s="T560">Žilaʔizʼiʔ</ts>
                  <nts id="Seg_2548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2550" n="HIAT:w" s="T561">bar</ts>
                  <nts id="Seg_2551" n="HIAT:ip">.</nts>
                  <nts id="Seg_2552" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T567" id="Seg_2554" n="HIAT:u" s="T562">
                  <ts e="T563" id="Seg_2556" n="HIAT:w" s="T562">Nʼimizʼiʔ</ts>
                  <nts id="Seg_2557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2559" n="HIAT:w" s="T563">i</ts>
                  <nts id="Seg_2560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2562" n="HIAT:w" s="T564">jendak</ts>
                  <nts id="Seg_2563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2565" n="HIAT:w" s="T565">müjögən</ts>
                  <nts id="Seg_2566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2568" n="HIAT:w" s="T566">ibi</ts>
                  <nts id="Seg_2569" n="HIAT:ip">.</nts>
                  <nts id="Seg_2570" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T570" id="Seg_2572" n="HIAT:u" s="T567">
                  <ts e="T569" id="Seg_2574" n="HIAT:w" s="T567">Šojdʼoʔi</ts>
                  <nts id="Seg_2575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2577" n="HIAT:w" s="T569">šöʔpiʔi</ts>
                  <nts id="Seg_2578" n="HIAT:ip">.</nts>
                  <nts id="Seg_2579" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T572" id="Seg_2581" n="HIAT:u" s="T570">
                  <ts e="T571" id="Seg_2583" n="HIAT:w" s="T570">Bar</ts>
                  <nts id="Seg_2584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2586" n="HIAT:w" s="T571">žilaʔizʼiʔ</ts>
                  <nts id="Seg_2587" n="HIAT:ip">.</nts>
                  <nts id="Seg_2588" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T578" id="Seg_2590" n="HIAT:u" s="T572">
                  <ts e="T573" id="Seg_2592" n="HIAT:w" s="T572">I</ts>
                  <nts id="Seg_2593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2594" n="HIAT:ip">(</nts>
                  <ts e="T574" id="Seg_2596" n="HIAT:w" s="T573">jek</ts>
                  <nts id="Seg_2597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2599" n="HIAT:w" s="T574">-</ts>
                  <nts id="Seg_2600" n="HIAT:ip">)</nts>
                  <nts id="Seg_2601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2603" n="HIAT:w" s="T575">jendak</ts>
                  <nts id="Seg_2604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2606" n="HIAT:w" s="T576">šerbi</ts>
                  <nts id="Seg_2607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2609" n="HIAT:w" s="T577">müjönə</ts>
                  <nts id="Seg_2610" n="HIAT:ip">.</nts>
                  <nts id="Seg_2611" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T581" id="Seg_2613" n="HIAT:u" s="T578">
                  <ts e="T579" id="Seg_2615" n="HIAT:w" s="T578">I</ts>
                  <nts id="Seg_2616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2618" n="HIAT:w" s="T579">nʼimi</ts>
                  <nts id="Seg_2619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2621" n="HIAT:w" s="T580">ibi</ts>
                  <nts id="Seg_2622" n="HIAT:ip">.</nts>
                  <nts id="Seg_2623" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T582" id="Seg_2625" n="HIAT:u" s="T581">
                  <ts e="T582" id="Seg_2627" n="HIAT:w" s="T581">Šödörbi</ts>
                  <nts id="Seg_2628" n="HIAT:ip">.</nts>
                  <nts id="Seg_2629" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T585" id="Seg_2631" n="HIAT:u" s="T582">
                  <ts e="T583" id="Seg_2633" n="HIAT:w" s="T582">Всё</ts>
                  <nts id="Seg_2634" n="HIAT:ip">,</nts>
                  <nts id="Seg_2635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2637" n="HIAT:w" s="T583">всё</ts>
                  <nts id="Seg_2638" n="HIAT:ip">…</nts>
                  <nts id="Seg_2639" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T622" id="Seg_2640" n="sc" s="T591">
               <ts e="T595" id="Seg_2642" n="HIAT:u" s="T591">
                  <nts id="Seg_2643" n="HIAT:ip">(</nts>
                  <ts e="T592" id="Seg_2645" n="HIAT:w" s="T591">Müj-</ts>
                  <nts id="Seg_2646" n="HIAT:ip">)</nts>
                  <nts id="Seg_2647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2649" n="HIAT:w" s="T592">Müjöbə</ts>
                  <nts id="Seg_2650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2652" n="HIAT:w" s="T593">bar</ts>
                  <nts id="Seg_2653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2655" n="HIAT:w" s="T594">muʔləj</ts>
                  <nts id="Seg_2656" n="HIAT:ip">.</nts>
                  <nts id="Seg_2657" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T599" id="Seg_2659" n="HIAT:u" s="T595">
                  <ts e="T596" id="Seg_2661" n="HIAT:w" s="T595">Tăn</ts>
                  <nts id="Seg_2662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2664" n="HIAT:w" s="T596">bar</ts>
                  <nts id="Seg_2665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2667" n="HIAT:w" s="T597">nel</ts>
                  <nts id="Seg_2668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2670" n="HIAT:w" s="T598">ajirlaʔbəl</ts>
                  <nts id="Seg_2671" n="HIAT:ip">.</nts>
                  <nts id="Seg_2672" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T602" id="Seg_2674" n="HIAT:u" s="T599">
                  <ts e="T600" id="Seg_2676" n="HIAT:w" s="T599">Sĭjdə</ts>
                  <nts id="Seg_2677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2679" n="HIAT:w" s="T600">ĭzemnie</ts>
                  <nts id="Seg_2680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2682" n="HIAT:w" s="T601">ugaːndə</ts>
                  <nts id="Seg_2683" n="HIAT:ip">.</nts>
                  <nts id="Seg_2684" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T610" id="Seg_2686" n="HIAT:u" s="T602">
                  <ts e="T603" id="Seg_2688" n="HIAT:w" s="T602">Măn</ts>
                  <nts id="Seg_2689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2691" n="HIAT:w" s="T603">bar</ts>
                  <nts id="Seg_2692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2694" n="HIAT:w" s="T604">turam</ts>
                  <nts id="Seg_2695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2697" n="HIAT:w" s="T605">sagər</ts>
                  <nts id="Seg_2698" n="HIAT:ip">,</nts>
                  <nts id="Seg_2699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2700" n="HIAT:ip">(</nts>
                  <ts e="T607" id="Seg_2702" n="HIAT:w" s="T606">karə</ts>
                  <nts id="Seg_2703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2705" n="HIAT:w" s="T607">-</ts>
                  <nts id="Seg_2706" n="HIAT:ip">)</nts>
                  <nts id="Seg_2707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2709" n="HIAT:w" s="T608">karəldʼan</ts>
                  <nts id="Seg_2710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2712" n="HIAT:w" s="T609">bazlam</ts>
                  <nts id="Seg_2713" n="HIAT:ip">.</nts>
                  <nts id="Seg_2714" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T612" id="Seg_2716" n="HIAT:u" s="T610">
                  <ts e="T611" id="Seg_2718" n="HIAT:w" s="T610">Ugaːndə</ts>
                  <nts id="Seg_2719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2721" n="HIAT:w" s="T611">balgaš</ts>
                  <nts id="Seg_2722" n="HIAT:ip">.</nts>
                  <nts id="Seg_2723" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T616" id="Seg_2725" n="HIAT:u" s="T612">
                  <ts e="T612.tx-PKZ.1" id="Seg_2727" n="HIAT:w" s="T612">A</ts>
                  <nts id="Seg_2728" n="HIAT:ip">_</nts>
                  <ts e="T613" id="Seg_2730" n="HIAT:w" s="T612.tx-PKZ.1">to</ts>
                  <nts id="Seg_2731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2733" n="HIAT:w" s="T613">il</ts>
                  <nts id="Seg_2734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2736" n="HIAT:w" s="T614">bar</ts>
                  <nts id="Seg_2737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2739" n="HIAT:w" s="T615">mălleʔbəʔjə</ts>
                  <nts id="Seg_2740" n="HIAT:ip">.</nts>
                  <nts id="Seg_2741" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T619" id="Seg_2743" n="HIAT:u" s="T616">
                  <ts e="T617" id="Seg_2745" n="HIAT:w" s="T616">Măliaʔi:</ts>
                  <nts id="Seg_2746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2748" n="HIAT:w" s="T617">ɨrɨː</ts>
                  <nts id="Seg_2749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2751" n="HIAT:w" s="T618">nüke</ts>
                  <nts id="Seg_2752" n="HIAT:ip">.</nts>
                  <nts id="Seg_2753" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T622" id="Seg_2755" n="HIAT:u" s="T619">
                  <ts e="T620" id="Seg_2757" n="HIAT:w" s="T619">Tura</ts>
                  <nts id="Seg_2758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2760" n="HIAT:w" s="T620">ej</ts>
                  <nts id="Seg_2761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2763" n="HIAT:w" s="T621">bazəlia</ts>
                  <nts id="Seg_2764" n="HIAT:ip">.</nts>
                  <nts id="Seg_2765" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T666" id="Seg_2766" n="sc" s="T626">
               <ts e="T629" id="Seg_2768" n="HIAT:u" s="T626">
                  <ts e="T627" id="Seg_2770" n="HIAT:w" s="T626">Ĭmbi</ts>
                  <nts id="Seg_2771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2773" n="HIAT:w" s="T627">tănan</ts>
                  <nts id="Seg_2774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2776" n="HIAT:w" s="T628">dʼăbaktərzittə</ts>
                  <nts id="Seg_2777" n="HIAT:ip">?</nts>
                  <nts id="Seg_2778" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T632" id="Seg_2780" n="HIAT:u" s="T629">
                  <ts e="T630" id="Seg_2782" n="HIAT:w" s="T629">Măn</ts>
                  <nts id="Seg_2783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2785" n="HIAT:w" s="T630">bar</ts>
                  <nts id="Seg_2786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2788" n="HIAT:w" s="T631">nöməlbiom</ts>
                  <nts id="Seg_2789" n="HIAT:ip">.</nts>
                  <nts id="Seg_2790" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T637" id="Seg_2792" n="HIAT:u" s="T632">
                  <ts e="T633" id="Seg_2794" n="HIAT:w" s="T632">Teinen</ts>
                  <nts id="Seg_2795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2797" n="HIAT:w" s="T633">kunolzittə</ts>
                  <nts id="Seg_2798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2800" n="HIAT:w" s="T634">klopəʔi</ts>
                  <nts id="Seg_2801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2803" n="HIAT:w" s="T635">ej</ts>
                  <nts id="Seg_2804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2806" n="HIAT:w" s="T636">mĭbiʔi</ts>
                  <nts id="Seg_2807" n="HIAT:ip">.</nts>
                  <nts id="Seg_2808" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T644" id="Seg_2810" n="HIAT:u" s="T637">
                  <ts e="T638" id="Seg_2812" n="HIAT:w" s="T637">Dĭ</ts>
                  <nts id="Seg_2813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2815" n="HIAT:w" s="T638">kuza</ts>
                  <nts id="Seg_2816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2818" n="HIAT:w" s="T639">un</ts>
                  <nts id="Seg_2819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2821" n="HIAT:w" s="T640">pʼebi</ts>
                  <nts id="Seg_2822" n="HIAT:ip">,</nts>
                  <nts id="Seg_2823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2825" n="HIAT:w" s="T641">nada</ts>
                  <nts id="Seg_2826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2828" n="HIAT:w" s="T642">dĭʔnə</ts>
                  <nts id="Seg_2829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2831" n="HIAT:w" s="T643">mĭzittə</ts>
                  <nts id="Seg_2832" n="HIAT:ip">.</nts>
                  <nts id="Seg_2833" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T648" id="Seg_2835" n="HIAT:u" s="T644">
                  <ts e="T645" id="Seg_2837" n="HIAT:w" s="T644">Dĭn</ts>
                  <nts id="Seg_2838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2840" n="HIAT:w" s="T645">ipek</ts>
                  <nts id="Seg_2841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2843" n="HIAT:w" s="T646">naga</ts>
                  <nts id="Seg_2844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2846" n="HIAT:w" s="T647">amzittə</ts>
                  <nts id="Seg_2847" n="HIAT:ip">.</nts>
                  <nts id="Seg_2848" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T649" id="Seg_2850" n="HIAT:u" s="T648">
                  <ts e="T649" id="Seg_2852" n="HIAT:w" s="T648">Bar</ts>
                  <nts id="Seg_2853" n="HIAT:ip">.</nts>
                  <nts id="Seg_2854" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T659" id="Seg_2856" n="HIAT:u" s="T649">
                  <nts id="Seg_2857" n="HIAT:ip">(</nts>
                  <ts e="T650" id="Seg_2859" n="HIAT:w" s="T649">Dʼ</ts>
                  <nts id="Seg_2860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2862" n="HIAT:w" s="T650">-</ts>
                  <nts id="Seg_2863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2865" n="HIAT:w" s="T651">n</ts>
                  <nts id="Seg_2866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2868" n="HIAT:w" s="T652">-</ts>
                  <nts id="Seg_2869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2871" n="HIAT:w" s="T653">n</ts>
                  <nts id="Seg_2872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2874" n="HIAT:w" s="T654">-</ts>
                  <nts id="Seg_2875" n="HIAT:ip">)</nts>
                  <nts id="Seg_2876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2878" n="HIAT:w" s="T655">Gijen</ts>
                  <nts id="Seg_2879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2881" n="HIAT:w" s="T656">izittə</ts>
                  <nts id="Seg_2882" n="HIAT:ip">,</nts>
                  <nts id="Seg_2883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2885" n="HIAT:w" s="T657">ĭmbi</ts>
                  <nts id="Seg_2886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2888" n="HIAT:w" s="T658">dʼăbaktərzʼittə</ts>
                  <nts id="Seg_2889" n="HIAT:ip">.</nts>
                  <nts id="Seg_2890" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T666" id="Seg_2892" n="HIAT:u" s="T659">
                  <ts e="T660" id="Seg_2894" n="HIAT:w" s="T659">Dĭʔnə</ts>
                  <nts id="Seg_2895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2897" n="HIAT:w" s="T660">ugaːndə</ts>
                  <nts id="Seg_2898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2899" n="HIAT:ip">(</nts>
                  <ts e="T662" id="Seg_2901" n="HIAT:w" s="T661">nʼiʔgö</ts>
                  <nts id="Seg_2902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2904" n="HIAT:w" s="T662">-</ts>
                  <nts id="Seg_2905" n="HIAT:ip">)</nts>
                  <nts id="Seg_2906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2908" n="HIAT:w" s="T663">iʔgö</ts>
                  <nts id="Seg_2909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2911" n="HIAT:w" s="T664">nada</ts>
                  <nts id="Seg_2912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2914" n="HIAT:w" s="T665">dʼăbaktərzittə</ts>
                  <nts id="Seg_2915" n="HIAT:ip">.</nts>
                  <nts id="Seg_2916" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T682" id="Seg_2917" n="sc" s="T668">
               <ts e="T672" id="Seg_2919" n="HIAT:u" s="T668">
                  <ts e="T670" id="Seg_2921" n="HIAT:w" s="T668">Nada</ts>
                  <nts id="Seg_2922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2924" n="HIAT:w" s="T670">kanzittə</ts>
                  <nts id="Seg_2925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2927" n="HIAT:w" s="T671">măndərzittə</ts>
                  <nts id="Seg_2928" n="HIAT:ip">.</nts>
                  <nts id="Seg_2929" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T674" id="Seg_2931" n="HIAT:u" s="T672">
                  <ts e="T673" id="Seg_2933" n="HIAT:w" s="T672">Gijen-nʼibudʼ</ts>
                  <nts id="Seg_2934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2936" n="HIAT:w" s="T673">dʼăbaktərzittə</ts>
                  <nts id="Seg_2937" n="HIAT:ip">.</nts>
                  <nts id="Seg_2938" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T682" id="Seg_2940" n="HIAT:u" s="T674">
                  <ts e="T675" id="Seg_2942" n="HIAT:w" s="T674">Măn</ts>
                  <nts id="Seg_2943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2945" n="HIAT:w" s="T675">taldʼen</ts>
                  <nts id="Seg_2946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2948" n="HIAT:w" s="T676">kundʼo</ts>
                  <nts id="Seg_2949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2951" n="HIAT:w" s="T677">ej</ts>
                  <nts id="Seg_2952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2953" n="HIAT:ip">(</nts>
                  <ts e="T679" id="Seg_2955" n="HIAT:w" s="T678">iʔb</ts>
                  <nts id="Seg_2956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2958" n="HIAT:w" s="T679">-</ts>
                  <nts id="Seg_2959" n="HIAT:ip">)</nts>
                  <nts id="Seg_2960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2962" n="HIAT:w" s="T680">iʔbəbiem</ts>
                  <nts id="Seg_2963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2965" n="HIAT:w" s="T681">kunolzittə</ts>
                  <nts id="Seg_2966" n="HIAT:ip">.</nts>
                  <nts id="Seg_2967" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T692" id="Seg_2968" n="sc" s="T684">
               <ts e="T692" id="Seg_2970" n="HIAT:u" s="T684">
                  <ts e="T685" id="Seg_2972" n="HIAT:w" s="T684">Kundʼo</ts>
                  <nts id="Seg_2973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2974" n="HIAT:ip">(</nts>
                  <ts e="T686" id="Seg_2976" n="HIAT:w" s="T685">ej</ts>
                  <nts id="Seg_2977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2979" n="HIAT:w" s="T686">iʔbəlaʔb-</ts>
                  <nts id="Seg_2980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2982" n="HIAT:w" s="T687">ej</ts>
                  <nts id="Seg_2983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2985" n="HIAT:w" s="T688">iʔbə-</ts>
                  <nts id="Seg_2986" n="HIAT:ip">)</nts>
                  <nts id="Seg_2987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2989" n="HIAT:w" s="T689">ej</ts>
                  <nts id="Seg_2990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2992" n="HIAT:w" s="T690">iʔbəlem</ts>
                  <nts id="Seg_2993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2995" n="HIAT:w" s="T691">kunolzittə</ts>
                  <nts id="Seg_2996" n="HIAT:ip">.</nts>
                  <nts id="Seg_2997" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T745" id="Seg_2998" n="sc" s="T696">
               <ts e="T700" id="Seg_3000" n="HIAT:u" s="T696">
                  <ts e="T697" id="Seg_3002" n="HIAT:w" s="T696">Kunolbiam</ts>
                  <nts id="Seg_3003" n="HIAT:ip">,</nts>
                  <nts id="Seg_3004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_3006" n="HIAT:w" s="T697">ĭmbidə</ts>
                  <nts id="Seg_3007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_3009" n="HIAT:w" s="T698">ej</ts>
                  <nts id="Seg_3010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_3012" n="HIAT:w" s="T699">kubiam</ts>
                  <nts id="Seg_3013" n="HIAT:ip">.</nts>
                  <nts id="Seg_3014" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T706" id="Seg_3016" n="HIAT:u" s="T700">
                  <ts e="T701" id="Seg_3018" n="HIAT:w" s="T700">Dĭ</ts>
                  <nts id="Seg_3019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_3021" n="HIAT:w" s="T701">kuzagən</ts>
                  <nts id="Seg_3022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_3024" n="HIAT:w" s="T702">eʔbdət</ts>
                  <nts id="Seg_3025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_3027" n="HIAT:w" s="T703">bar</ts>
                  <nts id="Seg_3028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_3030" n="HIAT:w" s="T704">sagər</ts>
                  <nts id="Seg_3031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_3033" n="HIAT:w" s="T705">ibi</ts>
                  <nts id="Seg_3034" n="HIAT:ip">.</nts>
                  <nts id="Seg_3035" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T712" id="Seg_3037" n="HIAT:u" s="T706">
                  <ts e="T707" id="Seg_3039" n="HIAT:w" s="T706">Tüj</ts>
                  <nts id="Seg_3040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_3042" n="HIAT:w" s="T707">bar</ts>
                  <nts id="Seg_3043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_3045" n="HIAT:w" s="T708">ugaːndə</ts>
                  <nts id="Seg_3046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_3048" n="HIAT:w" s="T709">sĭre</ts>
                  <nts id="Seg_3049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_3051" n="HIAT:w" s="T710">molaːmbiʔi</ts>
                  <nts id="Seg_3052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_3054" n="HIAT:w" s="T711">eʔbdə</ts>
                  <nts id="Seg_3055" n="HIAT:ip">.</nts>
                  <nts id="Seg_3056" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T719" id="Seg_3058" n="HIAT:u" s="T712">
                  <ts e="T713" id="Seg_3060" n="HIAT:w" s="T712">Onʼiʔ</ts>
                  <nts id="Seg_3061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_3063" n="HIAT:w" s="T713">uda</ts>
                  <nts id="Seg_3064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_3066" n="HIAT:w" s="T714">ige</ts>
                  <nts id="Seg_3067" n="HIAT:ip">,</nts>
                  <nts id="Seg_3068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_3070" n="HIAT:w" s="T715">a</ts>
                  <nts id="Seg_3071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_3073" n="HIAT:w" s="T716">onʼiʔ</ts>
                  <nts id="Seg_3074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_3076" n="HIAT:w" s="T717">uda</ts>
                  <nts id="Seg_3077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_3079" n="HIAT:w" s="T718">nĭŋgəbiʔi</ts>
                  <nts id="Seg_3080" n="HIAT:ip">.</nts>
                  <nts id="Seg_3081" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T727" id="Seg_3083" n="HIAT:u" s="T719">
                  <ts e="T720" id="Seg_3085" n="HIAT:w" s="T719">Onʼiʔ</ts>
                  <nts id="Seg_3086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_3088" n="HIAT:w" s="T720">uda</ts>
                  <nts id="Seg_3089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_3091" n="HIAT:w" s="T721">ige</ts>
                  <nts id="Seg_3092" n="HIAT:ip">,</nts>
                  <nts id="Seg_3093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_3095" n="HIAT:w" s="T722">a</ts>
                  <nts id="Seg_3096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_3098" n="HIAT:w" s="T723">onʼiʔ</ts>
                  <nts id="Seg_3099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_3101" n="HIAT:w" s="T724">udabə</ts>
                  <nts id="Seg_3102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_3104" n="HIAT:w" s="T725">bar</ts>
                  <nts id="Seg_3105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_3107" n="HIAT:w" s="T726">sajnĭŋgəbiʔi</ts>
                  <nts id="Seg_3108" n="HIAT:ip">.</nts>
                  <nts id="Seg_3109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T733" id="Seg_3111" n="HIAT:u" s="T727">
                  <ts e="T728" id="Seg_3113" n="HIAT:w" s="T727">Măn</ts>
                  <nts id="Seg_3114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_3116" n="HIAT:w" s="T728">üzəbiem</ts>
                  <nts id="Seg_3117" n="HIAT:ip">,</nts>
                  <nts id="Seg_3118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3119" n="HIAT:ip">(</nts>
                  <ts e="T730" id="Seg_3121" n="HIAT:w" s="T729">один</ts>
                  <nts id="Seg_3122" n="HIAT:ip">)</nts>
                  <nts id="Seg_3123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_3125" n="HIAT:w" s="T730">onʼiʔ</ts>
                  <nts id="Seg_3126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_3128" n="HIAT:w" s="T731">udam</ts>
                  <nts id="Seg_3129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_3131" n="HIAT:w" s="T732">băldəbiam</ts>
                  <nts id="Seg_3132" n="HIAT:ip">.</nts>
                  <nts id="Seg_3133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T740" id="Seg_3135" n="HIAT:u" s="T733">
                  <nts id="Seg_3136" n="HIAT:ip">(</nts>
                  <ts e="T734" id="Seg_3138" n="HIAT:w" s="T733">Măs-</ts>
                  <nts id="Seg_3139" n="HIAT:ip">)</nts>
                  <nts id="Seg_3140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_3142" n="HIAT:w" s="T734">Măn</ts>
                  <nts id="Seg_3143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_3145" n="HIAT:w" s="T735">saʔməluʔpiam</ts>
                  <nts id="Seg_3146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_3148" n="HIAT:w" s="T736">i</ts>
                  <nts id="Seg_3149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_3151" n="HIAT:w" s="T737">udam</ts>
                  <nts id="Seg_3152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3153" n="HIAT:ip">(</nts>
                  <ts e="T739" id="Seg_3155" n="HIAT:w" s="T738">băldə-</ts>
                  <nts id="Seg_3156" n="HIAT:ip">)</nts>
                  <nts id="Seg_3157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_3159" n="HIAT:w" s="T739">băldəlbiam</ts>
                  <nts id="Seg_3160" n="HIAT:ip">.</nts>
                  <nts id="Seg_3161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T745" id="Seg_3163" n="HIAT:u" s="T740">
                  <ts e="T741" id="Seg_3165" n="HIAT:w" s="T740">Dĭgəttə</ts>
                  <nts id="Seg_3166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_3168" n="HIAT:w" s="T741">dĭ</ts>
                  <nts id="Seg_3169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_3171" n="HIAT:w" s="T742">udam</ts>
                  <nts id="Seg_3172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_3174" n="HIAT:w" s="T743">embiʔi</ts>
                  <nts id="Seg_3175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_3177" n="HIAT:w" s="T744">bar</ts>
                  <nts id="Seg_3178" n="HIAT:ip">.</nts>
                  <nts id="Seg_3179" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T755" id="Seg_3180" n="sc" s="T901">
               <ts e="T755" id="Seg_3182" n="HIAT:u" s="T901">
                  <nts id="Seg_3183" n="HIAT:ip">(</nts>
                  <nts id="Seg_3184" n="HIAT:ip">(</nts>
                  <ats e="T901.tx-PKZ.1" id="Seg_3185" n="HIAT:non-pho" s="T901">BRK</ats>
                  <nts id="Seg_3186" n="HIAT:ip">)</nts>
                  <nts id="Seg_3187" n="HIAT:ip">)</nts>
                  <nts id="Seg_3188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_3190" n="HIAT:w" s="T901.tx-PKZ.1">Dĭgəttə</ts>
                  <nts id="Seg_3191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_3193" n="HIAT:w" s="T752">udam</ts>
                  <nts id="Seg_3194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T918" id="Seg_3196" n="HIAT:w" s="T753">bar</ts>
                  <nts id="Seg_3197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_3199" n="HIAT:w" s="T918">sarbiʔi</ts>
                  <nts id="Seg_3200" n="HIAT:ip">.</nts>
                  <nts id="Seg_3201" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T775" id="Seg_3202" n="sc" s="T758">
               <ts e="T761" id="Seg_3204" n="HIAT:u" s="T758">
                  <ts e="T759" id="Seg_3206" n="HIAT:w" s="T758">Paka</ts>
                  <nts id="Seg_3207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_3209" n="HIAT:w" s="T759">ej</ts>
                  <nts id="Seg_3210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_3212" n="HIAT:w" s="T760">özerləj</ts>
                  <nts id="Seg_3213" n="HIAT:ip">.</nts>
                  <nts id="Seg_3214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T764" id="Seg_3216" n="HIAT:u" s="T761">
                  <ts e="T762" id="Seg_3218" n="HIAT:w" s="T761">Ĭmbidə</ts>
                  <nts id="Seg_3219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_3221" n="HIAT:w" s="T762">togonorzittə</ts>
                  <nts id="Seg_3222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_3224" n="HIAT:w" s="T763">nʼelʼzja</ts>
                  <nts id="Seg_3225" n="HIAT:ip">.</nts>
                  <nts id="Seg_3226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T767" id="Seg_3228" n="HIAT:u" s="T764">
                  <ts e="T765" id="Seg_3230" n="HIAT:w" s="T764">Ine</ts>
                  <nts id="Seg_3231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_3233" n="HIAT:w" s="T765">körerzittə</ts>
                  <nts id="Seg_3234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_3236" n="HIAT:w" s="T766">nada</ts>
                  <nts id="Seg_3237" n="HIAT:ip">.</nts>
                  <nts id="Seg_3238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T775" id="Seg_3240" n="HIAT:u" s="T767">
                  <nts id="Seg_3241" n="HIAT:ip">(</nts>
                  <ts e="T768" id="Seg_3243" n="HIAT:w" s="T767">I</ts>
                  <nts id="Seg_3244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_3246" n="HIAT:w" s="T768">-</ts>
                  <nts id="Seg_3247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_3249" n="HIAT:w" s="T769">in</ts>
                  <nts id="Seg_3250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_3252" n="HIAT:w" s="T770">-</ts>
                  <nts id="Seg_3253" n="HIAT:ip">)</nts>
                  <nts id="Seg_3254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_3256" n="HIAT:w" s="T771">Ine</ts>
                  <nts id="Seg_3257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3258" n="HIAT:ip">(</nts>
                  <ts e="T773" id="Seg_3260" n="HIAT:w" s="T772">nu-</ts>
                  <nts id="Seg_3261" n="HIAT:ip">)</nts>
                  <nts id="Seg_3262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_3264" n="HIAT:w" s="T773">nuʔməleʔpi</ts>
                  <nts id="Seg_3265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_3267" n="HIAT:w" s="T774">bar</ts>
                  <nts id="Seg_3268" n="HIAT:ip">.</nts>
                  <nts id="Seg_3269" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T856" id="Seg_3270" n="sc" s="T779">
               <ts e="T788" id="Seg_3272" n="HIAT:u" s="T779">
                  <nts id="Seg_3273" n="HIAT:ip">(</nts>
                  <ts e="T780" id="Seg_3275" n="HIAT:w" s="T779">I</ts>
                  <nts id="Seg_3276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_3278" n="HIAT:w" s="T780">-</ts>
                  <nts id="Seg_3279" n="HIAT:ip">)</nts>
                  <nts id="Seg_3280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_3282" n="HIAT:w" s="T781">Ine</ts>
                  <nts id="Seg_3283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_3285" n="HIAT:w" s="T782">nuʔməleʔpi</ts>
                  <nts id="Seg_3286" n="HIAT:ip">,</nts>
                  <nts id="Seg_3287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_3289" n="HIAT:w" s="T783">saʔməluʔpi</ts>
                  <nts id="Seg_3290" n="HIAT:ip">,</nts>
                  <nts id="Seg_3291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3292" n="HIAT:ip">(</nts>
                  <ts e="T785" id="Seg_3294" n="HIAT:w" s="T784">üjüt</ts>
                  <nts id="Seg_3295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_3297" n="HIAT:w" s="T785">băldə-</ts>
                  <nts id="Seg_3298" n="HIAT:ip">)</nts>
                  <nts id="Seg_3299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_3301" n="HIAT:w" s="T786">üjüt</ts>
                  <nts id="Seg_3302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_3304" n="HIAT:w" s="T787">băldəbi</ts>
                  <nts id="Seg_3305" n="HIAT:ip">.</nts>
                  <nts id="Seg_3306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T800" id="Seg_3308" n="HIAT:u" s="T788">
                  <ts e="T789" id="Seg_3310" n="HIAT:w" s="T788">Măn</ts>
                  <nts id="Seg_3311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_3313" n="HIAT:w" s="T789">udagən</ts>
                  <nts id="Seg_3314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_3316" n="HIAT:w" s="T790">sumna</ts>
                  <nts id="Seg_3317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_3319" n="HIAT:w" s="T791">müjözeŋbə</ts>
                  <nts id="Seg_3320" n="HIAT:ip">,</nts>
                  <nts id="Seg_3321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_3323" n="HIAT:w" s="T792">a</ts>
                  <nts id="Seg_3324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_3326" n="HIAT:w" s="T793">tăn</ts>
                  <nts id="Seg_3327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_3329" n="HIAT:w" s="T794">udagən</ts>
                  <nts id="Seg_3330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_3332" n="HIAT:w" s="T795">onʼiʔ</ts>
                  <nts id="Seg_3333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_3335" n="HIAT:w" s="T796">naga</ts>
                  <nts id="Seg_3336" n="HIAT:ip">,</nts>
                  <nts id="Seg_3337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_3339" n="HIAT:w" s="T797">baltuzʼiʔ</ts>
                  <nts id="Seg_3340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_3342" n="HIAT:w" s="T798">bar</ts>
                  <nts id="Seg_3343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_3345" n="HIAT:w" s="T799">jaʔpial</ts>
                  <nts id="Seg_3346" n="HIAT:ip">.</nts>
                  <nts id="Seg_3347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T806" id="Seg_3349" n="HIAT:u" s="T800">
                  <ts e="T801" id="Seg_3351" n="HIAT:w" s="T800">Teinen</ts>
                  <nts id="Seg_3352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_3354" n="HIAT:w" s="T801">esseŋ</ts>
                  <nts id="Seg_3355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_3357" n="HIAT:w" s="T802">iʔgö</ts>
                  <nts id="Seg_3358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_3360" n="HIAT:w" s="T803">bar</ts>
                  <nts id="Seg_3361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_3363" n="HIAT:w" s="T804">buzo</ts>
                  <nts id="Seg_3364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_3366" n="HIAT:w" s="T805">sürerleʔpiʔi</ts>
                  <nts id="Seg_3367" n="HIAT:ip">.</nts>
                  <nts id="Seg_3368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T812" id="Seg_3370" n="HIAT:u" s="T806">
                  <ts e="T807" id="Seg_3372" n="HIAT:w" s="T806">Dĭ</ts>
                  <nts id="Seg_3373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_3375" n="HIAT:w" s="T807">bar</ts>
                  <nts id="Seg_3376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_3378" n="HIAT:w" s="T808">parluʔpi</ts>
                  <nts id="Seg_3379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_3381" n="HIAT:w" s="T809">i</ts>
                  <nts id="Seg_3382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_3384" n="HIAT:w" s="T810">nuʔməluʔpi</ts>
                  <nts id="Seg_3385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_3387" n="HIAT:w" s="T811">maʔndə</ts>
                  <nts id="Seg_3388" n="HIAT:ip">.</nts>
                  <nts id="Seg_3389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T816" id="Seg_3391" n="HIAT:u" s="T812">
                  <ts e="T813" id="Seg_3393" n="HIAT:w" s="T812">Nüdʼin</ts>
                  <nts id="Seg_3394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_3396" n="HIAT:w" s="T813">kalla</ts>
                  <nts id="Seg_3397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_3399" n="HIAT:w" s="T814">dʼürbi</ts>
                  <nts id="Seg_3400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_3402" n="HIAT:w" s="T815">bar</ts>
                  <nts id="Seg_3403" n="HIAT:ip">.</nts>
                  <nts id="Seg_3404" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T818" id="Seg_3406" n="HIAT:u" s="T816">
                  <ts e="T817" id="Seg_3408" n="HIAT:w" s="T816">Nüdʼi</ts>
                  <nts id="Seg_3409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_3411" n="HIAT:w" s="T817">molaːndəga</ts>
                  <nts id="Seg_3412" n="HIAT:ip">.</nts>
                  <nts id="Seg_3413" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T821" id="Seg_3415" n="HIAT:u" s="T818">
                  <ts e="T819" id="Seg_3417" n="HIAT:w" s="T818">Kuja</ts>
                  <nts id="Seg_3418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_3420" n="HIAT:w" s="T819">bar</ts>
                  <nts id="Seg_3421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_3423" n="HIAT:w" s="T820">amnolaʔbə</ts>
                  <nts id="Seg_3424" n="HIAT:ip">.</nts>
                  <nts id="Seg_3425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T823" id="Seg_3427" n="HIAT:u" s="T821">
                  <ts e="T822" id="Seg_3429" n="HIAT:w" s="T821">Nüdʼi</ts>
                  <nts id="Seg_3430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_3432" n="HIAT:w" s="T822">molaːndəga</ts>
                  <nts id="Seg_3433" n="HIAT:ip">.</nts>
                  <nts id="Seg_3434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T825" id="Seg_3436" n="HIAT:u" s="T823">
                  <ts e="T824" id="Seg_3438" n="HIAT:w" s="T823">Kuja</ts>
                  <nts id="Seg_3439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3441" n="HIAT:w" s="T824">amnollaʔbə</ts>
                  <nts id="Seg_3442" n="HIAT:ip">.</nts>
                  <nts id="Seg_3443" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T830" id="Seg_3445" n="HIAT:u" s="T825">
                  <ts e="T826" id="Seg_3447" n="HIAT:w" s="T825">Kabarləj</ts>
                  <nts id="Seg_3448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_3450" n="HIAT:w" s="T826">dʼăbaktərzittə</ts>
                  <nts id="Seg_3451" n="HIAT:ip">,</nts>
                  <nts id="Seg_3452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_3454" n="HIAT:w" s="T827">nada</ts>
                  <nts id="Seg_3455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_3457" n="HIAT:w" s="T828">iʔbəsʼtə</ts>
                  <nts id="Seg_3458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_3460" n="HIAT:w" s="T829">kunolzittə</ts>
                  <nts id="Seg_3461" n="HIAT:ip">.</nts>
                  <nts id="Seg_3462" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T834" id="Seg_3464" n="HIAT:u" s="T830">
                  <ts e="T831" id="Seg_3466" n="HIAT:w" s="T830">Erten</ts>
                  <nts id="Seg_3467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3469" n="HIAT:w" s="T831">erte</ts>
                  <nts id="Seg_3470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_3472" n="HIAT:w" s="T832">nada</ts>
                  <nts id="Seg_3473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_3475" n="HIAT:w" s="T833">uʔsʼittə</ts>
                  <nts id="Seg_3476" n="HIAT:ip">.</nts>
                  <nts id="Seg_3477" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T839" id="Seg_3479" n="HIAT:u" s="T834">
                  <ts e="T834.tx-PKZ.1" id="Seg_3481" n="HIAT:w" s="T834">Утром</ts>
                  <nts id="Seg_3482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834.tx-PKZ.2" id="Seg_3484" n="HIAT:w" s="T834.tx-PKZ.1">рано</ts>
                  <nts id="Seg_3485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834.tx-PKZ.3" id="Seg_3487" n="HIAT:w" s="T834.tx-PKZ.2">надо</ts>
                  <nts id="Seg_3488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834.tx-PKZ.4" id="Seg_3490" n="HIAT:w" s="T834.tx-PKZ.3">вставать</ts>
                  <nts id="Seg_3491" n="HIAT:ip">,</nts>
                  <nts id="Seg_3492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_3494" n="HIAT:w" s="T834.tx-PKZ.4">говорю</ts>
                  <nts id="Seg_3495" n="HIAT:ip">.</nts>
                  <nts id="Seg_3496" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T842" id="Seg_3498" n="HIAT:u" s="T839">
                  <ts e="T840" id="Seg_3500" n="HIAT:w" s="T839">Kallam</ts>
                  <nts id="Seg_3501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_3503" n="HIAT:w" s="T840">svetokəʔi</ts>
                  <nts id="Seg_3504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_3506" n="HIAT:w" s="T841">nĭŋgəsʼtə</ts>
                  <nts id="Seg_3507" n="HIAT:ip">.</nts>
                  <nts id="Seg_3508" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T847" id="Seg_3510" n="HIAT:u" s="T842">
                  <ts e="T843" id="Seg_3512" n="HIAT:w" s="T842">Sĭre</ts>
                  <nts id="Seg_3513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_3515" n="HIAT:w" s="T843">pagən</ts>
                  <nts id="Seg_3516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_3518" n="HIAT:w" s="T844">bar</ts>
                  <nts id="Seg_3519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_3521" n="HIAT:w" s="T845">sazən</ts>
                  <nts id="Seg_3522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_3524" n="HIAT:w" s="T846">edleʔbə</ts>
                  <nts id="Seg_3525" n="HIAT:ip">.</nts>
                  <nts id="Seg_3526" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T849" id="Seg_3528" n="HIAT:u" s="T847">
                  <ts e="T848" id="Seg_3530" n="HIAT:w" s="T847">Ej</ts>
                  <nts id="Seg_3531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_3533" n="HIAT:w" s="T848">edlia</ts>
                  <nts id="Seg_3534" n="HIAT:ip">.</nts>
                  <nts id="Seg_3535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T852" id="Seg_3537" n="HIAT:u" s="T849">
                  <ts e="T850" id="Seg_3539" n="HIAT:w" s="T849">Ĭmbidə</ts>
                  <nts id="Seg_3540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_3542" n="HIAT:w" s="T850">ej</ts>
                  <nts id="Seg_3543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_3545" n="HIAT:w" s="T851">kuliom</ts>
                  <nts id="Seg_3546" n="HIAT:ip">.</nts>
                  <nts id="Seg_3547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T856" id="Seg_3549" n="HIAT:u" s="T852">
                  <ts e="T853" id="Seg_3551" n="HIAT:w" s="T852">Măn</ts>
                  <nts id="Seg_3552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_3554" n="HIAT:w" s="T853">tuganbə</ts>
                  <nts id="Seg_3555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_3557" n="HIAT:w" s="T854">ugaːndə</ts>
                  <nts id="Seg_3558" n="HIAT:ip">…</nts>
                  <nts id="Seg_3559" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T917" id="Seg_3560" n="sc" s="T857">
               <ts e="T859" id="Seg_3562" n="HIAT:u" s="T857">
                  <ts e="T858" id="Seg_3564" n="HIAT:w" s="T857">Kuŋgə</ts>
                  <nts id="Seg_3565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T859" id="Seg_3567" n="HIAT:w" s="T858">amnolaʔbə</ts>
                  <nts id="Seg_3568" n="HIAT:ip">.</nts>
                  <nts id="Seg_3569" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T863" id="Seg_3571" n="HIAT:u" s="T859">
                  <ts e="T860" id="Seg_3573" n="HIAT:w" s="T859">Nada</ts>
                  <nts id="Seg_3574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_3576" n="HIAT:w" s="T860">šide</ts>
                  <nts id="Seg_3577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_3579" n="HIAT:w" s="T861">dʼala</ts>
                  <nts id="Seg_3580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_3582" n="HIAT:w" s="T862">kanzittə</ts>
                  <nts id="Seg_3583" n="HIAT:ip">.</nts>
                  <nts id="Seg_3584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T868" id="Seg_3586" n="HIAT:u" s="T863">
                  <ts e="T864" id="Seg_3588" n="HIAT:w" s="T863">A</ts>
                  <nts id="Seg_3589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_3591" n="HIAT:w" s="T864">možet</ts>
                  <nts id="Seg_3592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_3594" n="HIAT:w" s="T865">onʼiʔ</ts>
                  <nts id="Seg_3595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_3597" n="HIAT:w" s="T866">dʼalanə</ts>
                  <nts id="Seg_3598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_3600" n="HIAT:w" s="T867">kallal</ts>
                  <nts id="Seg_3601" n="HIAT:ip">.</nts>
                  <nts id="Seg_3602" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T874" id="Seg_3604" n="HIAT:u" s="T868">
                  <ts e="T869" id="Seg_3606" n="HIAT:w" s="T868">Măn</ts>
                  <nts id="Seg_3607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_3609" n="HIAT:w" s="T869">dĭʔnə</ts>
                  <nts id="Seg_3610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_3612" n="HIAT:w" s="T870">šobiam</ts>
                  <nts id="Seg_3613" n="HIAT:ip">,</nts>
                  <nts id="Seg_3614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_3616" n="HIAT:w" s="T871">a</ts>
                  <nts id="Seg_3617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_3619" n="HIAT:w" s="T872">dĭ</ts>
                  <nts id="Seg_3620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_3622" n="HIAT:w" s="T873">naga</ts>
                  <nts id="Seg_3623" n="HIAT:ip">.</nts>
                  <nts id="Seg_3624" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T917" id="Seg_3626" n="HIAT:u" s="T874">
                  <ts e="T875" id="Seg_3628" n="HIAT:w" s="T874">Gibərdə</ts>
                  <nts id="Seg_3629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_3631" n="HIAT:w" s="T875">kalla</ts>
                  <nts id="Seg_3632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_3634" n="HIAT:w" s="T876">dʼürbi</ts>
                  <nts id="Seg_3635" n="HIAT:ip">,</nts>
                  <nts id="Seg_3636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_3638" n="HIAT:w" s="T877">măn</ts>
                  <nts id="Seg_3639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_3641" n="HIAT:w" s="T878">bar</ts>
                  <nts id="Seg_3642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T917" id="Seg_3644" n="HIAT:w" s="T879">parluʔpi</ts>
                  <nts id="Seg_3645" n="HIAT:ip">.</nts>
                  <nts id="Seg_3646" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PKZ">
            <ts e="T38" id="Seg_3647" n="sc" s="T4">
               <ts e="T5" id="Seg_3649" n="e" s="T4">Măn </ts>
               <ts e="T6" id="Seg_3651" n="e" s="T5">ej </ts>
               <ts e="T7" id="Seg_3653" n="e" s="T6">moliam </ts>
               <ts e="T8" id="Seg_3655" n="e" s="T7">(mĭngz </ts>
               <ts e="T9" id="Seg_3657" n="e" s="T8">-) </ts>
               <ts e="T10" id="Seg_3659" n="e" s="T9">(mĭŋgəlsʼtə), </ts>
               <ts e="T11" id="Seg_3661" n="e" s="T10">bar </ts>
               <ts e="T12" id="Seg_3663" n="e" s="T11">ĭzemneʔbə. </ts>
               <ts e="T13" id="Seg_3665" n="e" s="T12">Adnaka </ts>
               <ts e="T14" id="Seg_3667" n="e" s="T13">măn </ts>
               <ts e="T15" id="Seg_3669" n="e" s="T14">külaːmbiam. </ts>
               <ts e="T16" id="Seg_3671" n="e" s="T15">Teinen </ts>
               <ts e="T17" id="Seg_3673" n="e" s="T16">dĭ </ts>
               <ts e="T18" id="Seg_3675" n="e" s="T17">koʔbdo </ts>
               <ts e="T19" id="Seg_3677" n="e" s="T18">urgo </ts>
               <ts e="T20" id="Seg_3679" n="e" s="T19">dʼala. </ts>
               <ts e="T21" id="Seg_3681" n="e" s="T20">Dĭm </ts>
               <ts e="T22" id="Seg_3683" n="e" s="T21">teinen </ts>
               <ts e="T23" id="Seg_3685" n="e" s="T22">(ia </ts>
               <ts e="T24" id="Seg_3687" n="e" s="T23">- </ts>
               <ts e="T25" id="Seg_3689" n="e" s="T24">iad </ts>
               <ts e="T26" id="Seg_3691" n="e" s="T25">-) </ts>
               <ts e="T27" id="Seg_3693" n="e" s="T26">ijad </ts>
               <ts e="T28" id="Seg_3695" n="e" s="T27">deʔpi. </ts>
               <ts e="T29" id="Seg_3697" n="e" s="T28">Dĭʔnə </ts>
               <ts e="T30" id="Seg_3699" n="e" s="T29">(bʼeʔ </ts>
               <ts e="T31" id="Seg_3701" n="e" s="T30">š </ts>
               <ts e="T32" id="Seg_3703" n="e" s="T31">-) </ts>
               <ts e="T33" id="Seg_3705" n="e" s="T32">bʼeʔ </ts>
               <ts e="T34" id="Seg_3707" n="e" s="T33">šide </ts>
               <ts e="T35" id="Seg_3709" n="e" s="T34">pʼe </ts>
               <ts e="T36" id="Seg_3711" n="e" s="T35">i </ts>
               <ts e="T37" id="Seg_3713" n="e" s="T36">iššo </ts>
               <ts e="T38" id="Seg_3715" n="e" s="T37">šide. </ts>
            </ts>
            <ts e="T59" id="Seg_3716" n="sc" s="T39">
               <ts e="T40" id="Seg_3718" n="e" s="T39">Găda </ts>
               <ts e="T41" id="Seg_3720" n="e" s="T40">dĭ </ts>
               <ts e="T42" id="Seg_3722" n="e" s="T41">kuza </ts>
               <ts e="T43" id="Seg_3724" n="e" s="T42">marʼi. </ts>
               <ts e="T44" id="Seg_3726" n="e" s="T43">Ĭmbidə </ts>
               <ts e="T45" id="Seg_3728" n="e" s="T44">ej </ts>
               <ts e="T46" id="Seg_3730" n="e" s="T45">ile. </ts>
               <ts e="T47" id="Seg_3732" n="e" s="T46">Ĭmbidə </ts>
               <ts e="T48" id="Seg_3734" n="e" s="T47">ej </ts>
               <ts e="T49" id="Seg_3736" n="e" s="T48">mĭlej. </ts>
               <ts e="T50" id="Seg_3738" n="e" s="T49">A </ts>
               <ts e="T51" id="Seg_3740" n="e" s="T50">baška </ts>
               <ts e="T52" id="Seg_3742" n="e" s="T51">kuza </ts>
               <ts e="T53" id="Seg_3744" n="e" s="T52">ugaːndə </ts>
               <ts e="T54" id="Seg_3746" n="e" s="T53">jakšə. </ts>
               <ts e="T55" id="Seg_3748" n="e" s="T54">Bar </ts>
               <ts e="T56" id="Seg_3750" n="e" s="T55">mĭlüʔləj, </ts>
               <ts e="T57" id="Seg_3752" n="e" s="T56">(ĭm </ts>
               <ts e="T58" id="Seg_3754" n="e" s="T57">-) </ts>
               <ts e="T59" id="Seg_3756" n="e" s="T58">ĭmbi. </ts>
            </ts>
            <ts e="T243" id="Seg_3757" n="sc" s="T61">
               <ts e="T62" id="Seg_3759" n="e" s="T61">Ĭmbi </ts>
               <ts e="T63" id="Seg_3761" n="e" s="T62">ej </ts>
               <ts e="T64" id="Seg_3763" n="e" s="T63">(pʼer </ts>
               <ts e="T65" id="Seg_3765" n="e" s="T64">- </ts>
               <ts e="T66" id="Seg_3767" n="e" s="T65">m </ts>
               <ts e="T67" id="Seg_3769" n="e" s="T66">-) </ts>
               <ts e="T69" id="Seg_3771" n="e" s="T67">ĭmbi… </ts>
               <ts e="T70" id="Seg_3773" n="e" s="T69">Ĭmbi </ts>
               <ts e="T71" id="Seg_3775" n="e" s="T70">ej </ts>
               <ts e="T72" id="Seg_3777" n="e" s="T71">pilel, </ts>
               <ts e="T73" id="Seg_3779" n="e" s="T72">bar </ts>
               <ts e="T74" id="Seg_3781" n="e" s="T73">mĭlej. </ts>
               <ts e="T75" id="Seg_3783" n="e" s="T74">Dĭn </ts>
               <ts e="T76" id="Seg_3785" n="e" s="T75">ugaːndə </ts>
               <ts e="T77" id="Seg_3787" n="e" s="T76">sĭjdə </ts>
               <ts e="T78" id="Seg_3789" n="e" s="T77">jakšə. </ts>
               <ts e="T83" id="Seg_3791" n="e" s="T78">Как это сразу ты… </ts>
               <ts e="T84" id="Seg_3793" n="e" s="T83">Ara </ts>
               <ts e="T85" id="Seg_3795" n="e" s="T84">ej </ts>
               <ts e="T86" id="Seg_3797" n="e" s="T85">bĭtlie. </ts>
               <ts e="T87" id="Seg_3799" n="e" s="T86">Ĭmbi </ts>
               <ts e="T88" id="Seg_3801" n="e" s="T87">ige, </ts>
               <ts e="T89" id="Seg_3803" n="e" s="T88">bar </ts>
               <ts e="T90" id="Seg_3805" n="e" s="T89">mĭləj. </ts>
               <ts e="T91" id="Seg_3807" n="e" s="T90">Munujʔ </ts>
               <ts e="T92" id="Seg_3809" n="e" s="T91">mĭlie, </ts>
               <ts e="T93" id="Seg_3811" n="e" s="T92">(uj </ts>
               <ts e="T94" id="Seg_3813" n="e" s="T93">-). </ts>
               <ts e="T95" id="Seg_3815" n="e" s="T94">Kajaʔ </ts>
               <ts e="T96" id="Seg_3817" n="e" s="T95">mĭlie, </ts>
               <ts e="T97" id="Seg_3819" n="e" s="T96">ipek </ts>
               <ts e="T98" id="Seg_3821" n="e" s="T97">mĭlie. </ts>
               <ts e="T99" id="Seg_3823" n="e" s="T98">Uja </ts>
               <ts e="T100" id="Seg_3825" n="e" s="T99">ige </ts>
               <ts e="T101" id="Seg_3827" n="e" s="T100">dăk, </ts>
               <ts e="T102" id="Seg_3829" n="e" s="T101">mĭləj. </ts>
               <ts e="T103" id="Seg_3831" n="e" s="T102">Bar </ts>
               <ts e="T104" id="Seg_3833" n="e" s="T103">ĭmbi </ts>
               <ts e="T105" id="Seg_3835" n="e" s="T104">ige, </ts>
               <ts e="T106" id="Seg_3837" n="e" s="T105">bar </ts>
               <ts e="T107" id="Seg_3839" n="e" s="T106">mĭləj. </ts>
               <ts e="T108" id="Seg_3841" n="e" s="T107">Ugaːndə </ts>
               <ts e="T109" id="Seg_3843" n="e" s="T108">(nʼu </ts>
               <ts e="T110" id="Seg_3845" n="e" s="T109">-) </ts>
               <ts e="T111" id="Seg_3847" n="e" s="T110">nʼuʔnən </ts>
               <ts e="T112" id="Seg_3849" n="e" s="T111">kuzurie. </ts>
               <ts e="T113" id="Seg_3851" n="e" s="T112">Kudaj </ts>
               <ts e="T114" id="Seg_3853" n="e" s="T113">kuzurleʔbə. </ts>
               <ts e="T115" id="Seg_3855" n="e" s="T114">(Šindən-) </ts>
               <ts e="T116" id="Seg_3857" n="e" s="T115">Šindi-nʼibudʼ </ts>
               <ts e="T117" id="Seg_3859" n="e" s="T116">kutləj. </ts>
               <ts e="T118" id="Seg_3861" n="e" s="T117">Raduga </ts>
               <ts e="T119" id="Seg_3863" n="e" s="T118">nulaʔbə. </ts>
               <ts e="T120" id="Seg_3865" n="e" s="T119">Bü </ts>
               <ts e="T121" id="Seg_3867" n="e" s="T120">ileʔbə. </ts>
               <ts e="T122" id="Seg_3869" n="e" s="T121">Dĭgəttə </ts>
               <ts e="T123" id="Seg_3871" n="e" s="T122">surno </ts>
               <ts e="T124" id="Seg_3873" n="e" s="T123">kalləj. </ts>
               <ts e="T125" id="Seg_3875" n="e" s="T124">Toskanak </ts>
               <ts e="T126" id="Seg_3877" n="e" s="T125">kalləj. </ts>
               <ts e="T127" id="Seg_3879" n="e" s="T126">Ugaːndə </ts>
               <ts e="T128" id="Seg_3881" n="e" s="T127">tutše </ts>
               <ts e="T129" id="Seg_3883" n="e" s="T128">sagər </ts>
               <ts e="T130" id="Seg_3885" n="e" s="T129">šonəga. </ts>
               <ts e="T131" id="Seg_3887" n="e" s="T130">Măn </ts>
               <ts e="T132" id="Seg_3889" n="e" s="T131">nʼiʔdə </ts>
               <ts e="T133" id="Seg_3891" n="e" s="T132">kambiam. </ts>
               <ts e="T134" id="Seg_3893" n="e" s="T133">Kukuška </ts>
               <ts e="T135" id="Seg_3895" n="e" s="T134">bar </ts>
               <ts e="T136" id="Seg_3897" n="e" s="T135">nʼergölaʔbə. </ts>
               <ts e="T137" id="Seg_3899" n="e" s="T136">Măndolam, </ts>
               <ts e="T138" id="Seg_3901" n="e" s="T137">dĭ </ts>
               <ts e="T139" id="Seg_3903" n="e" s="T138">turanə </ts>
               <ts e="T140" id="Seg_3905" n="e" s="T139">amnəbi </ts>
               <ts e="T141" id="Seg_3907" n="e" s="T140">bar </ts>
               <ts e="T142" id="Seg_3909" n="e" s="T141">i </ts>
               <ts e="T143" id="Seg_3911" n="e" s="T142">kirgarlaʔbə. </ts>
               <ts e="T144" id="Seg_3913" n="e" s="T143">Măn </ts>
               <ts e="T145" id="Seg_3915" n="e" s="T144">măndəm: </ts>
               <ts e="T146" id="Seg_3917" n="e" s="T145">unnʼa </ts>
               <ts e="T147" id="Seg_3919" n="e" s="T146">(amnolaʔbə-) </ts>
               <ts e="T148" id="Seg_3921" n="e" s="T147">amnolam </ts>
               <ts e="T149" id="Seg_3923" n="e" s="T148">bar. </ts>
               <ts e="T150" id="Seg_3925" n="e" s="T149">Verno </ts>
               <ts e="T151" id="Seg_3927" n="e" s="T150">unnʼa </ts>
               <ts e="T152" id="Seg_3929" n="e" s="T151">amnolam. </ts>
               <ts e="T153" id="Seg_3931" n="e" s="T152">Pizʼiʔ </ts>
               <ts e="T154" id="Seg_3933" n="e" s="T153">bar </ts>
               <ts e="T155" id="Seg_3935" n="e" s="T154">barəʔpi. </ts>
               <ts e="T156" id="Seg_3937" n="e" s="T155">Pazʼiʔ </ts>
               <ts e="T157" id="Seg_3939" n="e" s="T156">bar </ts>
               <ts e="T158" id="Seg_3941" n="e" s="T157">barəʔpi. </ts>
               <ts e="T159" id="Seg_3943" n="e" s="T158">Dĭ </ts>
               <ts e="T160" id="Seg_3945" n="e" s="T159">uge </ts>
               <ts e="T161" id="Seg_3947" n="e" s="T160">amnolaʔbə. </ts>
               <ts e="T162" id="Seg_3949" n="e" s="T161">Kirgarlaʔbə. </ts>
               <ts e="T163" id="Seg_3951" n="e" s="T162">Dĭgəttə </ts>
               <ts e="T164" id="Seg_3953" n="e" s="T163">nʼergölüʔpi, </ts>
               <ts e="T165" id="Seg_3955" n="e" s="T164">gijen </ts>
               <ts e="T166" id="Seg_3957" n="e" s="T165">kuja </ts>
               <ts e="T167" id="Seg_3959" n="e" s="T166">uʔbdlaʔbə. </ts>
               <ts e="T168" id="Seg_3961" n="e" s="T167">Ugaːndə </ts>
               <ts e="T169" id="Seg_3963" n="e" s="T168">külüʔsəbi </ts>
               <ts e="T170" id="Seg_3965" n="e" s="T169">il </ts>
               <ts e="T171" id="Seg_3967" n="e" s="T170">bar </ts>
               <ts e="T172" id="Seg_3969" n="e" s="T171">pʼaŋdəbiʔi </ts>
               <ts e="T173" id="Seg_3971" n="e" s="T172">sazəndə. </ts>
               <ts e="T174" id="Seg_3973" n="e" s="T173">Dĭ </ts>
               <ts e="T175" id="Seg_3975" n="e" s="T174">kuza </ts>
               <ts e="T176" id="Seg_3977" n="e" s="T175">ugaːndə </ts>
               <ts e="T177" id="Seg_3979" n="e" s="T176">ej </ts>
               <ts e="T178" id="Seg_3981" n="e" s="T177">jakšə. </ts>
               <ts e="T179" id="Seg_3983" n="e" s="T178">Bar </ts>
               <ts e="T180" id="Seg_3985" n="e" s="T179">ĭmbi </ts>
               <ts e="T181" id="Seg_3987" n="e" s="T180">ileʔbə </ts>
               <ts e="T183" id="Seg_3989" n="e" s="T181">da… </ts>
               <ts e="T184" id="Seg_3991" n="e" s="T183">Bar </ts>
               <ts e="T185" id="Seg_3993" n="e" s="T184">ĭmbi </ts>
               <ts e="T186" id="Seg_3995" n="e" s="T185">ileʔbə </ts>
               <ts e="T187" id="Seg_3997" n="e" s="T186">bar. </ts>
               <ts e="T188" id="Seg_3999" n="e" s="T187">Ugaːndə </ts>
               <ts e="T189" id="Seg_4001" n="e" s="T188">toli </ts>
               <ts e="T190" id="Seg_4003" n="e" s="T189">kuza. </ts>
               <ts e="T191" id="Seg_4005" n="e" s="T190">Bar </ts>
               <ts e="T192" id="Seg_4007" n="e" s="T191">ĭmbi </ts>
               <ts e="T193" id="Seg_4009" n="e" s="T192">ilie </ts>
               <ts e="T195" id="Seg_4011" n="e" s="T193">dăk… </ts>
               <ts e="T196" id="Seg_4013" n="e" s="T195">Bar </ts>
               <ts e="T197" id="Seg_4015" n="e" s="T196">ileʔbə </ts>
               <ts e="T198" id="Seg_4017" n="e" s="T197">bar, </ts>
               <ts e="T199" id="Seg_4019" n="e" s="T198">kundlaʔbə </ts>
               <ts e="T200" id="Seg_4021" n="e" s="T199">maʔndə. </ts>
               <ts e="T201" id="Seg_4023" n="e" s="T200">Măn </ts>
               <ts e="T202" id="Seg_4025" n="e" s="T201">teinen </ts>
               <ts e="T203" id="Seg_4027" n="e" s="T202">koʔbdonə </ts>
               <ts e="T204" id="Seg_4029" n="e" s="T203">platəm </ts>
               <ts e="T205" id="Seg_4031" n="e" s="T204">ibiem. </ts>
               <ts e="T206" id="Seg_4033" n="e" s="T205">Dĭ </ts>
               <ts e="T207" id="Seg_4035" n="e" s="T206">kambi </ts>
               <ts e="T208" id="Seg_4037" n="e" s="T207">Malinovkanə </ts>
               <ts e="T209" id="Seg_4039" n="e" s="T208">tüšəlzittə. </ts>
               <ts e="T210" id="Seg_4041" n="e" s="T209">Dĭn </ts>
               <ts e="T211" id="Seg_4043" n="e" s="T210">nüke </ts>
               <ts e="T212" id="Seg_4045" n="e" s="T211">surarlaʔbə:” </ts>
               <ts e="T213" id="Seg_4047" n="e" s="T212">Gijen </ts>
               <ts e="T214" id="Seg_4049" n="e" s="T213">tăn </ts>
               <ts e="T215" id="Seg_4051" n="e" s="T214">plat </ts>
               <ts e="T216" id="Seg_4053" n="e" s="T215">ibiel?” </ts>
               <ts e="T217" id="Seg_4055" n="e" s="T216">Urgaja </ts>
               <ts e="T218" id="Seg_4057" n="e" s="T217">mĭbi. </ts>
               <ts e="T219" id="Seg_4059" n="e" s="T218">A </ts>
               <ts e="T220" id="Seg_4061" n="e" s="T219">urgaja </ts>
               <ts e="T221" id="Seg_4063" n="e" s="T220">gijen </ts>
               <ts e="T222" id="Seg_4065" n="e" s="T221">ibi? </ts>
               <ts e="T223" id="Seg_4067" n="e" s="T222">Polʼa </ts>
               <ts e="T224" id="Seg_4069" n="e" s="T223">mĭbi. </ts>
               <ts e="T225" id="Seg_4071" n="e" s="T224">Dö </ts>
               <ts e="T226" id="Seg_4073" n="e" s="T225">ine </ts>
               <ts e="T227" id="Seg_4075" n="e" s="T226">ibiem, </ts>
               <ts e="T228" id="Seg_4077" n="e" s="T227">iʔgö </ts>
               <ts e="T229" id="Seg_4079" n="e" s="T228">aktʼa </ts>
               <ts e="T230" id="Seg_4081" n="e" s="T229">mĭbiem. </ts>
               <ts e="T231" id="Seg_4083" n="e" s="T230">A </ts>
               <ts e="T232" id="Seg_4085" n="e" s="T231">baška </ts>
               <ts e="T233" id="Seg_4087" n="e" s="T232">ine </ts>
               <ts e="T234" id="Seg_4089" n="e" s="T233">ibiem. </ts>
               <ts e="T235" id="Seg_4091" n="e" s="T234">Amga </ts>
               <ts e="T236" id="Seg_4093" n="e" s="T235">mĭbiem. </ts>
               <ts e="T886" id="Seg_4095" n="e" s="T236">Много </ts>
               <ts e="T237" id="Seg_4097" n="e" s="T886">отдала. </ts>
               <ts e="T238" id="Seg_4099" n="e" s="T237">Dʼaʔpiam </ts>
               <ts e="T239" id="Seg_4101" n="e" s="T238">(budo </ts>
               <ts e="T240" id="Seg_4103" n="e" s="T239">-) </ts>
               <ts e="T241" id="Seg_4105" n="e" s="T240">buzo. </ts>
               <ts e="T243" id="Seg_4107" n="e" s="T241">Bar… </ts>
            </ts>
            <ts e="T251" id="Seg_4108" n="sc" s="T246">
               <ts e="T247" id="Seg_4110" n="e" s="T246">Ej </ts>
               <ts e="T248" id="Seg_4112" n="e" s="T247">kandəga. </ts>
               <ts e="T249" id="Seg_4114" n="e" s="T248">Măn </ts>
               <ts e="T251" id="Seg_4116" n="e" s="T249">bar… </ts>
            </ts>
            <ts e="T268" id="Seg_4117" n="sc" s="T256">
               <ts e="T257" id="Seg_4119" n="e" s="T256">Ugaːndə </ts>
               <ts e="T258" id="Seg_4121" n="e" s="T257">kuštu. </ts>
               <ts e="T259" id="Seg_4123" n="e" s="T258">Ej </ts>
               <ts e="T260" id="Seg_4125" n="e" s="T259">moliam </ts>
               <ts e="T261" id="Seg_4127" n="e" s="T260">kunzittə </ts>
               <ts e="T262" id="Seg_4129" n="e" s="T261">bar. </ts>
               <ts e="T263" id="Seg_4131" n="e" s="T262">(Kundla </ts>
               <ts e="T264" id="Seg_4133" n="e" s="T263">- </ts>
               <ts e="T265" id="Seg_4135" n="e" s="T264">kundəga </ts>
               <ts e="T266" id="Seg_4137" n="e" s="T265">-) </ts>
               <ts e="T267" id="Seg_4139" n="e" s="T266">Kandəgam </ts>
               <ts e="T268" id="Seg_4141" n="e" s="T267">bar. </ts>
            </ts>
            <ts e="T326" id="Seg_4142" n="sc" s="T269">
               <ts e="T270" id="Seg_4144" n="e" s="T269">Ej </ts>
               <ts e="T271" id="Seg_4146" n="e" s="T270">jakšə </ts>
               <ts e="T272" id="Seg_4148" n="e" s="T271">(nʼi) </ts>
               <ts e="T273" id="Seg_4150" n="e" s="T272">nʼi. </ts>
               <ts e="T274" id="Seg_4152" n="e" s="T273">Üge </ts>
               <ts e="T275" id="Seg_4154" n="e" s="T274">bar </ts>
               <ts e="T276" id="Seg_4156" n="e" s="T275">tajirlaʔbə </ts>
               <ts e="T277" id="Seg_4158" n="e" s="T276">da </ts>
               <ts e="T278" id="Seg_4160" n="e" s="T277">kundlaʔbə. </ts>
               <ts e="T279" id="Seg_4162" n="e" s="T278">Măn </ts>
               <ts e="T280" id="Seg_4164" n="e" s="T279">dʼijegən </ts>
               <ts e="T281" id="Seg_4166" n="e" s="T280">mĭmbiem. </ts>
               <ts e="T282" id="Seg_4168" n="e" s="T281">Ĭmbidə </ts>
               <ts e="T283" id="Seg_4170" n="e" s="T282">(ku </ts>
               <ts e="T284" id="Seg_4172" n="e" s="T283">-) </ts>
               <ts e="T285" id="Seg_4174" n="e" s="T284">kubiam. </ts>
               <ts e="T286" id="Seg_4176" n="e" s="T285">Dĭgəttə </ts>
               <ts e="T287" id="Seg_4178" n="e" s="T286">măn </ts>
               <ts e="T288" id="Seg_4180" n="e" s="T287">nererlüʔpiem </ts>
               <ts e="T289" id="Seg_4182" n="e" s="T288">bar. </ts>
               <ts e="T290" id="Seg_4184" n="e" s="T289">Dĭgəttə </ts>
               <ts e="T291" id="Seg_4186" n="e" s="T290">(kuž </ts>
               <ts e="T292" id="Seg_4188" n="e" s="T291">-) </ts>
               <ts e="T293" id="Seg_4190" n="e" s="T292">kudaj </ts>
               <ts e="T294" id="Seg_4192" n="e" s="T293">kăštəbiam. </ts>
               <ts e="T295" id="Seg_4194" n="e" s="T294">Bar </ts>
               <ts e="T296" id="Seg_4196" n="e" s="T295">kalla </ts>
               <ts e="T297" id="Seg_4198" n="e" s="T296">dʼürbiʔi. </ts>
               <ts e="T298" id="Seg_4200" n="e" s="T297">Nada </ts>
               <ts e="T299" id="Seg_4202" n="e" s="T298">šiʔi </ts>
               <ts e="T300" id="Seg_4204" n="e" s="T299">păjdəsʼtə. </ts>
               <ts e="T301" id="Seg_4206" n="e" s="T300">Nada </ts>
               <ts e="T302" id="Seg_4208" n="e" s="T301">varota </ts>
               <ts e="T303" id="Seg_4210" n="e" s="T302">azittə. </ts>
               <ts e="T304" id="Seg_4212" n="e" s="T303">((a-))… ((BRK)). </ts>
               <ts e="T305" id="Seg_4214" n="e" s="T304">(Dĭ </ts>
               <ts e="T306" id="Seg_4216" n="e" s="T305">ši </ts>
               <ts e="T307" id="Seg_4218" n="e" s="T306">- </ts>
               <ts e="T308" id="Seg_4220" n="e" s="T307">š </ts>
               <ts e="T309" id="Seg_4222" n="e" s="T308">-) </ts>
               <ts e="T310" id="Seg_4224" n="e" s="T309">Dĭ </ts>
               <ts e="T311" id="Seg_4226" n="e" s="T310">šiʔnə </ts>
               <ts e="T312" id="Seg_4228" n="e" s="T311">nada </ts>
               <ts e="T313" id="Seg_4230" n="e" s="T312">paʔi </ts>
               <ts e="T314" id="Seg_4232" n="e" s="T313">păʔsittə. </ts>
               <ts e="T315" id="Seg_4234" n="e" s="T314">Dĭgəttə </ts>
               <ts e="T316" id="Seg_4236" n="e" s="T315">šeden </ts>
               <ts e="T317" id="Seg_4238" n="e" s="T316">kajləbəj. </ts>
               <ts e="T318" id="Seg_4240" n="e" s="T317">Dĭgəttə </ts>
               <ts e="T319" id="Seg_4242" n="e" s="T318">buzo </ts>
               <ts e="T320" id="Seg_4244" n="e" s="T319">ej </ts>
               <ts e="T321" id="Seg_4246" n="e" s="T320">nuʔmələj. </ts>
               <ts e="T322" id="Seg_4248" n="e" s="T321">(Šedengən </ts>
               <ts e="T323" id="Seg_4250" n="e" s="T322">amnolaʔ </ts>
               <ts e="T324" id="Seg_4252" n="e" s="T323">-) </ts>
               <ts e="T325" id="Seg_4254" n="e" s="T324">Šedengən </ts>
               <ts e="T326" id="Seg_4256" n="e" s="T325">nulaʔbə. </ts>
            </ts>
            <ts e="T331" id="Seg_4257" n="sc" s="T327">
               <ts e="T331" id="Seg_4259" n="e" s="T327">((DMG)) Ну там скажи чего. </ts>
            </ts>
            <ts e="T344" id="Seg_4260" n="sc" s="T335">
               <ts e="T336" id="Seg_4262" n="e" s="T335">(Ĭm </ts>
               <ts e="T337" id="Seg_4264" n="e" s="T336">-) </ts>
               <ts e="T338" id="Seg_4266" n="e" s="T337">Ĭmbizʼidə </ts>
               <ts e="T339" id="Seg_4268" n="e" s="T338">bar </ts>
               <ts e="T340" id="Seg_4270" n="e" s="T339">putʼəmnia. </ts>
               <ts e="T341" id="Seg_4272" n="e" s="T340">Tăn </ts>
               <ts e="T342" id="Seg_4274" n="e" s="T341">püjel </ts>
               <ts e="T344" id="Seg_4276" n="e" s="T342">ugaːndə… </ts>
            </ts>
            <ts e="T406" id="Seg_4277" n="sc" s="T346">
               <ts e="T347" id="Seg_4279" n="e" s="T346">Tăn </ts>
               <ts e="T348" id="Seg_4281" n="e" s="T347">püjel </ts>
               <ts e="T349" id="Seg_4283" n="e" s="T348">ugaːndə </ts>
               <ts e="T350" id="Seg_4285" n="e" s="T349">puʔmə, </ts>
               <ts e="T351" id="Seg_4287" n="e" s="T350">bar </ts>
               <ts e="T352" id="Seg_4289" n="e" s="T351">nʼilgölaʔbə. </ts>
               <ts e="T353" id="Seg_4291" n="e" s="T352">Tăn </ts>
               <ts e="T354" id="Seg_4293" n="e" s="T353">gijen </ts>
               <ts e="T355" id="Seg_4295" n="e" s="T354">ibiel? </ts>
               <ts e="T356" id="Seg_4297" n="e" s="T355">Büjleʔ </ts>
               <ts e="T357" id="Seg_4299" n="e" s="T356">mĭmbiem. </ts>
               <ts e="T358" id="Seg_4301" n="e" s="T357">Nada </ts>
               <ts e="T359" id="Seg_4303" n="e" s="T358">segi </ts>
               <ts e="T360" id="Seg_4305" n="e" s="T359">bü </ts>
               <ts e="T361" id="Seg_4307" n="e" s="T360">mĭnzərzittə. </ts>
               <ts e="T362" id="Seg_4309" n="e" s="T361">Nada </ts>
               <ts e="T363" id="Seg_4311" n="e" s="T362">mĭje </ts>
               <ts e="T364" id="Seg_4313" n="e" s="T363">(mĭnə </ts>
               <ts e="T365" id="Seg_4315" n="e" s="T364">-) </ts>
               <ts e="T366" id="Seg_4317" n="e" s="T365">mĭnzərzittə. </ts>
               <ts e="T367" id="Seg_4319" n="e" s="T366">Tibizeŋ </ts>
               <ts e="T368" id="Seg_4321" n="e" s="T367">bădəzʼittə. </ts>
               <ts e="T369" id="Seg_4323" n="e" s="T368">Măn </ts>
               <ts e="T370" id="Seg_4325" n="e" s="T369">bʼeʔ </ts>
               <ts e="T371" id="Seg_4327" n="e" s="T370">aktʼa </ts>
               <ts e="T372" id="Seg_4329" n="e" s="T371">ige. </ts>
               <ts e="T373" id="Seg_4331" n="e" s="T372">Nada </ts>
               <ts e="T374" id="Seg_4333" n="e" s="T373">tănan </ts>
               <ts e="T375" id="Seg_4335" n="e" s="T374">sumna </ts>
               <ts e="T376" id="Seg_4337" n="e" s="T375">mĭzittə. </ts>
               <ts e="T377" id="Seg_4339" n="e" s="T376">Sumna </ts>
               <ts e="T378" id="Seg_4341" n="e" s="T377">boskəndə </ts>
               <ts e="T379" id="Seg_4343" n="e" s="T378">mazittə. </ts>
               <ts e="T380" id="Seg_4345" n="e" s="T379">Možet </ts>
               <ts e="T381" id="Seg_4347" n="e" s="T380">giber-nʼibudʼ </ts>
               <ts e="T382" id="Seg_4349" n="e" s="T381">kallal, </ts>
               <ts e="T383" id="Seg_4351" n="e" s="T382">aktʼa </ts>
               <ts e="T384" id="Seg_4353" n="e" s="T383">kereʔ </ts>
               <ts e="T385" id="Seg_4355" n="e" s="T384">moləj. </ts>
               <ts e="T386" id="Seg_4357" n="e" s="T385">((DMG)). </ts>
               <ts e="T387" id="Seg_4359" n="e" s="T386">Matvejev </ts>
               <ts e="T388" id="Seg_4361" n="e" s="T387">bar </ts>
               <ts e="T389" id="Seg_4363" n="e" s="T388">amnolaʔbə. </ts>
               <ts e="T390" id="Seg_4365" n="e" s="T389">Ej </ts>
               <ts e="T391" id="Seg_4367" n="e" s="T390">dʼăbaktəria. </ts>
               <ts e="T392" id="Seg_4369" n="e" s="T391">Sĭjdə </ts>
               <ts e="T393" id="Seg_4371" n="e" s="T392">ĭzemnie. </ts>
               <ts e="T394" id="Seg_4373" n="e" s="T393">Maʔnal </ts>
               <ts e="T395" id="Seg_4375" n="e" s="T394">sazən </ts>
               <ts e="T396" id="Seg_4377" n="e" s="T395">naga. </ts>
               <ts e="T397" id="Seg_4379" n="e" s="T396">Net </ts>
               <ts e="T398" id="Seg_4381" n="e" s="T397">bar </ts>
               <ts e="T399" id="Seg_4383" n="e" s="T398">kuroluʔpi. </ts>
               <ts e="T400" id="Seg_4385" n="e" s="T399">Ej </ts>
               <ts e="T401" id="Seg_4387" n="e" s="T400">pʼaŋdəlia </ts>
               <ts e="T402" id="Seg_4389" n="e" s="T401">(sažə </ts>
               <ts e="T403" id="Seg_4391" n="e" s="T402">-) </ts>
               <ts e="T404" id="Seg_4393" n="e" s="T403">sazən. </ts>
               <ts e="T405" id="Seg_4395" n="e" s="T404">Iʔ </ts>
               <ts e="T406" id="Seg_4397" n="e" s="T405">kuroʔ! </ts>
            </ts>
            <ts e="T477" id="Seg_4398" n="sc" s="T409">
               <ts e="T410" id="Seg_4400" n="e" s="T409">Büžü </ts>
               <ts e="T411" id="Seg_4402" n="e" s="T410">maʔnən </ts>
               <ts e="T412" id="Seg_4404" n="e" s="T411">šolam. </ts>
               <ts e="T413" id="Seg_4406" n="e" s="T412">Ĭmbi-nʼibudʼ </ts>
               <ts e="T414" id="Seg_4408" n="e" s="T413">tănan </ts>
               <ts e="T415" id="Seg_4410" n="e" s="T414">detlem. </ts>
               <ts e="T416" id="Seg_4412" n="e" s="T415">Tăŋ </ts>
               <ts e="T417" id="Seg_4414" n="e" s="T416">ej </ts>
               <ts e="T418" id="Seg_4416" n="e" s="T417">dʼăbaktəraʔ. </ts>
               <ts e="T419" id="Seg_4418" n="e" s="T418">Koldʼəŋ </ts>
               <ts e="T420" id="Seg_4420" n="e" s="T419">dʼăbaktəraʔ! </ts>
               <ts e="T421" id="Seg_4422" n="e" s="T420">A_to </ts>
               <ts e="T422" id="Seg_4424" n="e" s="T421">bar </ts>
               <ts e="T423" id="Seg_4426" n="e" s="T422">il </ts>
               <ts e="T424" id="Seg_4428" n="e" s="T423">nünieʔi. </ts>
               <ts e="T425" id="Seg_4430" n="e" s="T424">Tăŋ </ts>
               <ts e="T426" id="Seg_4432" n="e" s="T425">iʔ </ts>
               <ts e="T427" id="Seg_4434" n="e" s="T426">kirgaraʔ! </ts>
               <ts e="T428" id="Seg_4436" n="e" s="T427">A </ts>
               <ts e="T429" id="Seg_4438" n="e" s="T428">(iʔ) </ts>
               <ts e="T430" id="Seg_4440" n="e" s="T429">tăn </ts>
               <ts e="T431" id="Seg_4442" n="e" s="T430">iʔ </ts>
               <ts e="T432" id="Seg_4444" n="e" s="T431">kudonzaʔ, </ts>
               <ts e="T433" id="Seg_4446" n="e" s="T432">a_to </ts>
               <ts e="T434" id="Seg_4448" n="e" s="T433">il </ts>
               <ts e="T435" id="Seg_4450" n="e" s="T434">bar </ts>
               <ts e="T436" id="Seg_4452" n="e" s="T435">nünleʔbəʔjə </ts>
               <ts e="T437" id="Seg_4454" n="e" s="T436">kăda </ts>
               <ts e="T438" id="Seg_4456" n="e" s="T437">(tăn) </ts>
               <ts e="T439" id="Seg_4458" n="e" s="T438">tăn </ts>
               <ts e="T440" id="Seg_4460" n="e" s="T439">kudonzlaʔbəl. </ts>
               <ts e="T441" id="Seg_4462" n="e" s="T440">Măn </ts>
               <ts e="T442" id="Seg_4464" n="e" s="T441">teinen </ts>
               <ts e="T443" id="Seg_4466" n="e" s="T442">oroma </ts>
               <ts e="T444" id="Seg_4468" n="e" s="T443">kămnabiam. </ts>
               <ts e="T445" id="Seg_4470" n="e" s="T444">Bĭlgarbiam. </ts>
               <ts e="T446" id="Seg_4472" n="e" s="T445">Krinkaʔi </ts>
               <ts e="T447" id="Seg_4474" n="e" s="T446">bəzəjdəbiam. </ts>
               <ts e="T448" id="Seg_4476" n="e" s="T447">Edəbiem, </ts>
               <ts e="T449" id="Seg_4478" n="e" s="T448">kuja </ts>
               <ts e="T450" id="Seg_4480" n="e" s="T449">koʔlaʔbə </ts>
               <ts e="T451" id="Seg_4482" n="e" s="T450">bar. </ts>
               <ts e="T452" id="Seg_4484" n="e" s="T451">Ĭmbi </ts>
               <ts e="T453" id="Seg_4486" n="e" s="T452">deʔpiel? </ts>
               <ts e="T454" id="Seg_4488" n="e" s="T453">Pʼerdə </ts>
               <ts e="T455" id="Seg_4490" n="e" s="T454">măna. </ts>
               <ts e="T456" id="Seg_4492" n="e" s="T455">(Ibi </ts>
               <ts e="T457" id="Seg_4494" n="e" s="T456">-) </ts>
               <ts e="T458" id="Seg_4496" n="e" s="T457">Ĭmbi </ts>
               <ts e="T459" id="Seg_4498" n="e" s="T458">dĭn </ts>
               <ts e="T460" id="Seg_4500" n="e" s="T459">iʔbölaʔbə? </ts>
               <ts e="T461" id="Seg_4502" n="e" s="T460">Măn </ts>
               <ts e="T462" id="Seg_4504" n="e" s="T461">teinen </ts>
               <ts e="T463" id="Seg_4506" n="e" s="T462">kalbajlaʔ </ts>
               <ts e="T464" id="Seg_4508" n="e" s="T463">mĭmbiem. </ts>
               <ts e="T465" id="Seg_4510" n="e" s="T464">Kalba </ts>
               <ts e="T466" id="Seg_4512" n="e" s="T465">deʔpiem. </ts>
               <ts e="T467" id="Seg_4514" n="e" s="T466">Dĭgəttə </ts>
               <ts e="T468" id="Seg_4516" n="e" s="T467">dʼagarlam. </ts>
               <ts e="T469" id="Seg_4518" n="e" s="T468">Munujʔ </ts>
               <ts e="T470" id="Seg_4520" n="e" s="T469">(ene-) </ts>
               <ts e="T471" id="Seg_4522" n="e" s="T470">ellem. </ts>
               <ts e="T472" id="Seg_4524" n="e" s="T471">Oroma </ts>
               <ts e="T473" id="Seg_4526" n="e" s="T472">ellem. </ts>
               <ts e="T474" id="Seg_4528" n="e" s="T473">Dĭgəttə </ts>
               <ts e="T475" id="Seg_4530" n="e" s="T474">pürzittə </ts>
               <ts e="T476" id="Seg_4532" n="e" s="T475">pirogəʔi </ts>
               <ts e="T477" id="Seg_4534" n="e" s="T476">molam. </ts>
            </ts>
            <ts e="T536" id="Seg_4535" n="sc" s="T478">
               <ts e="T479" id="Seg_4537" n="e" s="T478">Uja </ts>
               <ts e="T480" id="Seg_4539" n="e" s="T479">mĭnzerbiem. </ts>
               <ts e="T481" id="Seg_4541" n="e" s="T480">Uja </ts>
               <ts e="T482" id="Seg_4543" n="e" s="T481">ambiʔi </ts>
               <ts e="T483" id="Seg_4545" n="e" s="T482">(tĭmeizittə). </ts>
               <ts e="T484" id="Seg_4547" n="e" s="T483">Onʼiʔ </ts>
               <ts e="T485" id="Seg_4549" n="e" s="T484">le </ts>
               <ts e="T486" id="Seg_4551" n="e" s="T485">maluppi. </ts>
               <ts e="T489" id="Seg_4553" n="e" s="T486">Одни кости остались. </ts>
               <ts e="T490" id="Seg_4555" n="e" s="T489">Menzeŋ </ts>
               <ts e="T491" id="Seg_4557" n="e" s="T490">bar </ts>
               <ts e="T492" id="Seg_4559" n="e" s="T491">le </ts>
               <ts e="T493" id="Seg_4561" n="e" s="T492">amnaʔbəʔjə. </ts>
               <ts e="T494" id="Seg_4563" n="e" s="T493">Ugaːndə </ts>
               <ts e="T495" id="Seg_4565" n="e" s="T494">iʔgö </ts>
               <ts e="T496" id="Seg_4567" n="e" s="T495">le </ts>
               <ts e="T497" id="Seg_4569" n="e" s="T496">barəʔpibaʔ. </ts>
               <ts e="T498" id="Seg_4571" n="e" s="T497">(Măn </ts>
               <ts e="T499" id="Seg_4573" n="e" s="T498">ten </ts>
               <ts e="T500" id="Seg_4575" n="e" s="T499">-) </ts>
               <ts e="T501" id="Seg_4577" n="e" s="T500">Măn </ts>
               <ts e="T502" id="Seg_4579" n="e" s="T501">teinen </ts>
               <ts e="T503" id="Seg_4581" n="e" s="T502">ipek </ts>
               <ts e="T504" id="Seg_4583" n="e" s="T503">pürbiem, </ts>
               <ts e="T505" id="Seg_4585" n="e" s="T504">ej </ts>
               <ts e="T506" id="Seg_4587" n="e" s="T505">namzəga. </ts>
               <ts e="T507" id="Seg_4589" n="e" s="T506">Ej </ts>
               <ts e="T508" id="Seg_4591" n="e" s="T507">jakšə </ts>
               <ts e="T509" id="Seg_4593" n="e" s="T508">bar. </ts>
               <ts e="T510" id="Seg_4595" n="e" s="T509">Măn </ts>
               <ts e="T511" id="Seg_4597" n="e" s="T510">dĭ </ts>
               <ts e="T512" id="Seg_4599" n="e" s="T511">ipek </ts>
               <ts e="T513" id="Seg_4601" n="e" s="T512">dĭ </ts>
               <ts e="T514" id="Seg_4603" n="e" s="T513">(mendə </ts>
               <ts e="T515" id="Seg_4605" n="e" s="T514">-) </ts>
               <ts e="T516" id="Seg_4607" n="e" s="T515">menzeŋdə </ts>
               <ts e="T517" id="Seg_4609" n="e" s="T516">mĭbiem. </ts>
               <ts e="T518" id="Seg_4611" n="e" s="T517">Menzeŋ </ts>
               <ts e="T519" id="Seg_4613" n="e" s="T518">bar </ts>
               <ts e="T520" id="Seg_4615" n="e" s="T519">ipek </ts>
               <ts e="T521" id="Seg_4617" n="e" s="T520">amnaʔbəʔjə </ts>
               <ts e="T522" id="Seg_4619" n="e" s="T521">i </ts>
               <ts e="T523" id="Seg_4621" n="e" s="T522">bar </ts>
               <ts e="T524" id="Seg_4623" n="e" s="T523">dʼabərolaʔbəʔjə. </ts>
               <ts e="T525" id="Seg_4625" n="e" s="T524">Men </ts>
               <ts e="T526" id="Seg_4627" n="e" s="T525">bar </ts>
               <ts e="T527" id="Seg_4629" n="e" s="T526">dĭ </ts>
               <ts e="T528" id="Seg_4631" n="e" s="T527">mendə </ts>
               <ts e="T529" id="Seg_4633" n="e" s="T528">timnet </ts>
               <ts e="T530" id="Seg_4635" n="e" s="T529">bar… </ts>
               <ts e="T531" id="Seg_4637" n="e" s="T530">Nudʼi </ts>
               <ts e="T532" id="Seg_4639" n="e" s="T531">ibi. </ts>
               <ts e="T533" id="Seg_4641" n="e" s="T532">Nudʼi </ts>
               <ts e="T534" id="Seg_4643" n="e" s="T533">ibi, </ts>
               <ts e="T536" id="Seg_4645" n="e" s="T534">ugaːndə… </ts>
            </ts>
            <ts e="T585" id="Seg_4646" n="sc" s="T539">
               <ts e="T540" id="Seg_4648" n="e" s="T539">Ĭmbidə </ts>
               <ts e="T541" id="Seg_4650" n="e" s="T540">ej </ts>
               <ts e="T542" id="Seg_4652" n="e" s="T541">edlia. </ts>
               <ts e="T543" id="Seg_4654" n="e" s="T542">Măn </ts>
               <ts e="T544" id="Seg_4656" n="e" s="T543">dĭm </ts>
               <ts e="T545" id="Seg_4658" n="e" s="T544">ej </ts>
               <ts e="T546" id="Seg_4660" n="e" s="T545">tĭmnebiem. </ts>
               <ts e="T547" id="Seg_4662" n="e" s="T546">Šindi </ts>
               <ts e="T548" id="Seg_4664" n="e" s="T547">šonəga. </ts>
               <ts e="T549" id="Seg_4666" n="e" s="T548">Măn </ts>
               <ts e="T550" id="Seg_4668" n="e" s="T549">abam </ts>
               <ts e="T551" id="Seg_4670" n="e" s="T550">tujeziʔi </ts>
               <ts e="T552" id="Seg_4672" n="e" s="T551">abi. </ts>
               <ts e="T553" id="Seg_4674" n="e" s="T552">(Kuba) </ts>
               <ts e="T554" id="Seg_4676" n="e" s="T553">Kuba </ts>
               <ts e="T555" id="Seg_4678" n="e" s="T554">(kür-) </ts>
               <ts e="T556" id="Seg_4680" n="e" s="T555">kürbi </ts>
               <ts e="T557" id="Seg_4682" n="e" s="T556">i </ts>
               <ts e="T558" id="Seg_4684" n="e" s="T557">alaʔbəbi. </ts>
               <ts e="T559" id="Seg_4686" n="e" s="T558">Šojdʼo </ts>
               <ts e="T560" id="Seg_4688" n="e" s="T559">šöʔpiʔi. </ts>
               <ts e="T561" id="Seg_4690" n="e" s="T560">Žilaʔizʼiʔ </ts>
               <ts e="T562" id="Seg_4692" n="e" s="T561">bar. </ts>
               <ts e="T563" id="Seg_4694" n="e" s="T562">Nʼimizʼiʔ </ts>
               <ts e="T564" id="Seg_4696" n="e" s="T563">i </ts>
               <ts e="T565" id="Seg_4698" n="e" s="T564">jendak </ts>
               <ts e="T566" id="Seg_4700" n="e" s="T565">müjögən </ts>
               <ts e="T567" id="Seg_4702" n="e" s="T566">ibi. </ts>
               <ts e="T569" id="Seg_4704" n="e" s="T567">Šojdʼoʔi </ts>
               <ts e="T570" id="Seg_4706" n="e" s="T569">šöʔpiʔi. </ts>
               <ts e="T571" id="Seg_4708" n="e" s="T570">Bar </ts>
               <ts e="T572" id="Seg_4710" n="e" s="T571">žilaʔizʼiʔ. </ts>
               <ts e="T573" id="Seg_4712" n="e" s="T572">I </ts>
               <ts e="T574" id="Seg_4714" n="e" s="T573">(jek </ts>
               <ts e="T575" id="Seg_4716" n="e" s="T574">-) </ts>
               <ts e="T576" id="Seg_4718" n="e" s="T575">jendak </ts>
               <ts e="T577" id="Seg_4720" n="e" s="T576">šerbi </ts>
               <ts e="T578" id="Seg_4722" n="e" s="T577">müjönə. </ts>
               <ts e="T579" id="Seg_4724" n="e" s="T578">I </ts>
               <ts e="T580" id="Seg_4726" n="e" s="T579">nʼimi </ts>
               <ts e="T581" id="Seg_4728" n="e" s="T580">ibi. </ts>
               <ts e="T582" id="Seg_4730" n="e" s="T581">Šödörbi. </ts>
               <ts e="T583" id="Seg_4732" n="e" s="T582">Всё, </ts>
               <ts e="T585" id="Seg_4734" n="e" s="T583">всё… </ts>
            </ts>
            <ts e="T622" id="Seg_4735" n="sc" s="T591">
               <ts e="T592" id="Seg_4737" n="e" s="T591">(Müj-) </ts>
               <ts e="T593" id="Seg_4739" n="e" s="T592">Müjöbə </ts>
               <ts e="T594" id="Seg_4741" n="e" s="T593">bar </ts>
               <ts e="T595" id="Seg_4743" n="e" s="T594">muʔləj. </ts>
               <ts e="T596" id="Seg_4745" n="e" s="T595">Tăn </ts>
               <ts e="T597" id="Seg_4747" n="e" s="T596">bar </ts>
               <ts e="T598" id="Seg_4749" n="e" s="T597">nel </ts>
               <ts e="T599" id="Seg_4751" n="e" s="T598">ajirlaʔbəl. </ts>
               <ts e="T600" id="Seg_4753" n="e" s="T599">Sĭjdə </ts>
               <ts e="T601" id="Seg_4755" n="e" s="T600">ĭzemnie </ts>
               <ts e="T602" id="Seg_4757" n="e" s="T601">ugaːndə. </ts>
               <ts e="T603" id="Seg_4759" n="e" s="T602">Măn </ts>
               <ts e="T604" id="Seg_4761" n="e" s="T603">bar </ts>
               <ts e="T605" id="Seg_4763" n="e" s="T604">turam </ts>
               <ts e="T606" id="Seg_4765" n="e" s="T605">sagər, </ts>
               <ts e="T607" id="Seg_4767" n="e" s="T606">(karə </ts>
               <ts e="T608" id="Seg_4769" n="e" s="T607">-) </ts>
               <ts e="T609" id="Seg_4771" n="e" s="T608">karəldʼan </ts>
               <ts e="T610" id="Seg_4773" n="e" s="T609">bazlam. </ts>
               <ts e="T611" id="Seg_4775" n="e" s="T610">Ugaːndə </ts>
               <ts e="T612" id="Seg_4777" n="e" s="T611">balgaš. </ts>
               <ts e="T613" id="Seg_4779" n="e" s="T612">A_to </ts>
               <ts e="T614" id="Seg_4781" n="e" s="T613">il </ts>
               <ts e="T615" id="Seg_4783" n="e" s="T614">bar </ts>
               <ts e="T616" id="Seg_4785" n="e" s="T615">mălleʔbəʔjə. </ts>
               <ts e="T617" id="Seg_4787" n="e" s="T616">Măliaʔi: </ts>
               <ts e="T618" id="Seg_4789" n="e" s="T617">ɨrɨː </ts>
               <ts e="T619" id="Seg_4791" n="e" s="T618">nüke. </ts>
               <ts e="T620" id="Seg_4793" n="e" s="T619">Tura </ts>
               <ts e="T621" id="Seg_4795" n="e" s="T620">ej </ts>
               <ts e="T622" id="Seg_4797" n="e" s="T621">bazəlia. </ts>
            </ts>
            <ts e="T666" id="Seg_4798" n="sc" s="T626">
               <ts e="T627" id="Seg_4800" n="e" s="T626">Ĭmbi </ts>
               <ts e="T628" id="Seg_4802" n="e" s="T627">tănan </ts>
               <ts e="T629" id="Seg_4804" n="e" s="T628">dʼăbaktərzittə? </ts>
               <ts e="T630" id="Seg_4806" n="e" s="T629">Măn </ts>
               <ts e="T631" id="Seg_4808" n="e" s="T630">bar </ts>
               <ts e="T632" id="Seg_4810" n="e" s="T631">nöməlbiom. </ts>
               <ts e="T633" id="Seg_4812" n="e" s="T632">Teinen </ts>
               <ts e="T634" id="Seg_4814" n="e" s="T633">kunolzittə </ts>
               <ts e="T635" id="Seg_4816" n="e" s="T634">klopəʔi </ts>
               <ts e="T636" id="Seg_4818" n="e" s="T635">ej </ts>
               <ts e="T637" id="Seg_4820" n="e" s="T636">mĭbiʔi. </ts>
               <ts e="T638" id="Seg_4822" n="e" s="T637">Dĭ </ts>
               <ts e="T639" id="Seg_4824" n="e" s="T638">kuza </ts>
               <ts e="T640" id="Seg_4826" n="e" s="T639">un </ts>
               <ts e="T641" id="Seg_4828" n="e" s="T640">pʼebi, </ts>
               <ts e="T642" id="Seg_4830" n="e" s="T641">nada </ts>
               <ts e="T643" id="Seg_4832" n="e" s="T642">dĭʔnə </ts>
               <ts e="T644" id="Seg_4834" n="e" s="T643">mĭzittə. </ts>
               <ts e="T645" id="Seg_4836" n="e" s="T644">Dĭn </ts>
               <ts e="T646" id="Seg_4838" n="e" s="T645">ipek </ts>
               <ts e="T647" id="Seg_4840" n="e" s="T646">naga </ts>
               <ts e="T648" id="Seg_4842" n="e" s="T647">amzittə. </ts>
               <ts e="T649" id="Seg_4844" n="e" s="T648">Bar. </ts>
               <ts e="T650" id="Seg_4846" n="e" s="T649">(Dʼ </ts>
               <ts e="T651" id="Seg_4848" n="e" s="T650">- </ts>
               <ts e="T652" id="Seg_4850" n="e" s="T651">n </ts>
               <ts e="T653" id="Seg_4852" n="e" s="T652">- </ts>
               <ts e="T654" id="Seg_4854" n="e" s="T653">n </ts>
               <ts e="T655" id="Seg_4856" n="e" s="T654">-) </ts>
               <ts e="T656" id="Seg_4858" n="e" s="T655">Gijen </ts>
               <ts e="T657" id="Seg_4860" n="e" s="T656">izittə, </ts>
               <ts e="T658" id="Seg_4862" n="e" s="T657">ĭmbi </ts>
               <ts e="T659" id="Seg_4864" n="e" s="T658">dʼăbaktərzʼittə. </ts>
               <ts e="T660" id="Seg_4866" n="e" s="T659">Dĭʔnə </ts>
               <ts e="T661" id="Seg_4868" n="e" s="T660">ugaːndə </ts>
               <ts e="T662" id="Seg_4870" n="e" s="T661">(nʼiʔgö </ts>
               <ts e="T663" id="Seg_4872" n="e" s="T662">-) </ts>
               <ts e="T664" id="Seg_4874" n="e" s="T663">iʔgö </ts>
               <ts e="T665" id="Seg_4876" n="e" s="T664">nada </ts>
               <ts e="T666" id="Seg_4878" n="e" s="T665">dʼăbaktərzittə. </ts>
            </ts>
            <ts e="T682" id="Seg_4879" n="sc" s="T668">
               <ts e="T670" id="Seg_4881" n="e" s="T668">Nada </ts>
               <ts e="T671" id="Seg_4883" n="e" s="T670">kanzittə </ts>
               <ts e="T672" id="Seg_4885" n="e" s="T671">măndərzittə. </ts>
               <ts e="T673" id="Seg_4887" n="e" s="T672">Gijen-nʼibudʼ </ts>
               <ts e="T674" id="Seg_4889" n="e" s="T673">dʼăbaktərzittə. </ts>
               <ts e="T675" id="Seg_4891" n="e" s="T674">Măn </ts>
               <ts e="T676" id="Seg_4893" n="e" s="T675">taldʼen </ts>
               <ts e="T677" id="Seg_4895" n="e" s="T676">kundʼo </ts>
               <ts e="T678" id="Seg_4897" n="e" s="T677">ej </ts>
               <ts e="T679" id="Seg_4899" n="e" s="T678">(iʔb </ts>
               <ts e="T680" id="Seg_4901" n="e" s="T679">-) </ts>
               <ts e="T681" id="Seg_4903" n="e" s="T680">iʔbəbiem </ts>
               <ts e="T682" id="Seg_4905" n="e" s="T681">kunolzittə. </ts>
            </ts>
            <ts e="T692" id="Seg_4906" n="sc" s="T684">
               <ts e="T685" id="Seg_4908" n="e" s="T684">Kundʼo </ts>
               <ts e="T686" id="Seg_4910" n="e" s="T685">(ej </ts>
               <ts e="T687" id="Seg_4912" n="e" s="T686">iʔbəlaʔb- </ts>
               <ts e="T688" id="Seg_4914" n="e" s="T687">ej </ts>
               <ts e="T689" id="Seg_4916" n="e" s="T688">iʔbə-) </ts>
               <ts e="T690" id="Seg_4918" n="e" s="T689">ej </ts>
               <ts e="T691" id="Seg_4920" n="e" s="T690">iʔbəlem </ts>
               <ts e="T692" id="Seg_4922" n="e" s="T691">kunolzittə. </ts>
            </ts>
            <ts e="T745" id="Seg_4923" n="sc" s="T696">
               <ts e="T697" id="Seg_4925" n="e" s="T696">Kunolbiam, </ts>
               <ts e="T698" id="Seg_4927" n="e" s="T697">ĭmbidə </ts>
               <ts e="T699" id="Seg_4929" n="e" s="T698">ej </ts>
               <ts e="T700" id="Seg_4931" n="e" s="T699">kubiam. </ts>
               <ts e="T701" id="Seg_4933" n="e" s="T700">Dĭ </ts>
               <ts e="T702" id="Seg_4935" n="e" s="T701">kuzagən </ts>
               <ts e="T703" id="Seg_4937" n="e" s="T702">eʔbdət </ts>
               <ts e="T704" id="Seg_4939" n="e" s="T703">bar </ts>
               <ts e="T705" id="Seg_4941" n="e" s="T704">sagər </ts>
               <ts e="T706" id="Seg_4943" n="e" s="T705">ibi. </ts>
               <ts e="T707" id="Seg_4945" n="e" s="T706">Tüj </ts>
               <ts e="T708" id="Seg_4947" n="e" s="T707">bar </ts>
               <ts e="T709" id="Seg_4949" n="e" s="T708">ugaːndə </ts>
               <ts e="T710" id="Seg_4951" n="e" s="T709">sĭre </ts>
               <ts e="T711" id="Seg_4953" n="e" s="T710">molaːmbiʔi </ts>
               <ts e="T712" id="Seg_4955" n="e" s="T711">eʔbdə. </ts>
               <ts e="T713" id="Seg_4957" n="e" s="T712">Onʼiʔ </ts>
               <ts e="T714" id="Seg_4959" n="e" s="T713">uda </ts>
               <ts e="T715" id="Seg_4961" n="e" s="T714">ige, </ts>
               <ts e="T716" id="Seg_4963" n="e" s="T715">a </ts>
               <ts e="T717" id="Seg_4965" n="e" s="T716">onʼiʔ </ts>
               <ts e="T718" id="Seg_4967" n="e" s="T717">uda </ts>
               <ts e="T719" id="Seg_4969" n="e" s="T718">nĭŋgəbiʔi. </ts>
               <ts e="T720" id="Seg_4971" n="e" s="T719">Onʼiʔ </ts>
               <ts e="T721" id="Seg_4973" n="e" s="T720">uda </ts>
               <ts e="T722" id="Seg_4975" n="e" s="T721">ige, </ts>
               <ts e="T723" id="Seg_4977" n="e" s="T722">a </ts>
               <ts e="T724" id="Seg_4979" n="e" s="T723">onʼiʔ </ts>
               <ts e="T725" id="Seg_4981" n="e" s="T724">udabə </ts>
               <ts e="T726" id="Seg_4983" n="e" s="T725">bar </ts>
               <ts e="T727" id="Seg_4985" n="e" s="T726">sajnĭŋgəbiʔi. </ts>
               <ts e="T728" id="Seg_4987" n="e" s="T727">Măn </ts>
               <ts e="T729" id="Seg_4989" n="e" s="T728">üzəbiem, </ts>
               <ts e="T730" id="Seg_4991" n="e" s="T729">(один) </ts>
               <ts e="T731" id="Seg_4993" n="e" s="T730">onʼiʔ </ts>
               <ts e="T732" id="Seg_4995" n="e" s="T731">udam </ts>
               <ts e="T733" id="Seg_4997" n="e" s="T732">băldəbiam. </ts>
               <ts e="T734" id="Seg_4999" n="e" s="T733">(Măs-) </ts>
               <ts e="T735" id="Seg_5001" n="e" s="T734">Măn </ts>
               <ts e="T736" id="Seg_5003" n="e" s="T735">saʔməluʔpiam </ts>
               <ts e="T737" id="Seg_5005" n="e" s="T736">i </ts>
               <ts e="T738" id="Seg_5007" n="e" s="T737">udam </ts>
               <ts e="T739" id="Seg_5009" n="e" s="T738">(băldə-) </ts>
               <ts e="T740" id="Seg_5011" n="e" s="T739">băldəlbiam. </ts>
               <ts e="T741" id="Seg_5013" n="e" s="T740">Dĭgəttə </ts>
               <ts e="T742" id="Seg_5015" n="e" s="T741">dĭ </ts>
               <ts e="T743" id="Seg_5017" n="e" s="T742">udam </ts>
               <ts e="T744" id="Seg_5019" n="e" s="T743">embiʔi </ts>
               <ts e="T745" id="Seg_5021" n="e" s="T744">bar. </ts>
            </ts>
            <ts e="T755" id="Seg_5022" n="sc" s="T901">
               <ts e="T752" id="Seg_5024" n="e" s="T901">((BRK)) Dĭgəttə </ts>
               <ts e="T753" id="Seg_5026" n="e" s="T752">udam </ts>
               <ts e="T918" id="Seg_5028" n="e" s="T753">bar </ts>
               <ts e="T755" id="Seg_5030" n="e" s="T918">sarbiʔi. </ts>
            </ts>
            <ts e="T775" id="Seg_5031" n="sc" s="T758">
               <ts e="T759" id="Seg_5033" n="e" s="T758">Paka </ts>
               <ts e="T760" id="Seg_5035" n="e" s="T759">ej </ts>
               <ts e="T761" id="Seg_5037" n="e" s="T760">özerləj. </ts>
               <ts e="T762" id="Seg_5039" n="e" s="T761">Ĭmbidə </ts>
               <ts e="T763" id="Seg_5041" n="e" s="T762">togonorzittə </ts>
               <ts e="T764" id="Seg_5043" n="e" s="T763">nʼelʼzja. </ts>
               <ts e="T765" id="Seg_5045" n="e" s="T764">Ine </ts>
               <ts e="T766" id="Seg_5047" n="e" s="T765">körerzittə </ts>
               <ts e="T767" id="Seg_5049" n="e" s="T766">nada. </ts>
               <ts e="T768" id="Seg_5051" n="e" s="T767">(I </ts>
               <ts e="T769" id="Seg_5053" n="e" s="T768">- </ts>
               <ts e="T770" id="Seg_5055" n="e" s="T769">in </ts>
               <ts e="T771" id="Seg_5057" n="e" s="T770">-) </ts>
               <ts e="T772" id="Seg_5059" n="e" s="T771">Ine </ts>
               <ts e="T773" id="Seg_5061" n="e" s="T772">(nu-) </ts>
               <ts e="T774" id="Seg_5063" n="e" s="T773">nuʔməleʔpi </ts>
               <ts e="T775" id="Seg_5065" n="e" s="T774">bar. </ts>
            </ts>
            <ts e="T856" id="Seg_5066" n="sc" s="T779">
               <ts e="T780" id="Seg_5068" n="e" s="T779">(I </ts>
               <ts e="T781" id="Seg_5070" n="e" s="T780">-) </ts>
               <ts e="T782" id="Seg_5072" n="e" s="T781">Ine </ts>
               <ts e="T783" id="Seg_5074" n="e" s="T782">nuʔməleʔpi, </ts>
               <ts e="T784" id="Seg_5076" n="e" s="T783">saʔməluʔpi, </ts>
               <ts e="T785" id="Seg_5078" n="e" s="T784">(üjüt </ts>
               <ts e="T786" id="Seg_5080" n="e" s="T785">băldə-) </ts>
               <ts e="T787" id="Seg_5082" n="e" s="T786">üjüt </ts>
               <ts e="T788" id="Seg_5084" n="e" s="T787">băldəbi. </ts>
               <ts e="T789" id="Seg_5086" n="e" s="T788">Măn </ts>
               <ts e="T790" id="Seg_5088" n="e" s="T789">udagən </ts>
               <ts e="T791" id="Seg_5090" n="e" s="T790">sumna </ts>
               <ts e="T792" id="Seg_5092" n="e" s="T791">müjözeŋbə, </ts>
               <ts e="T793" id="Seg_5094" n="e" s="T792">a </ts>
               <ts e="T794" id="Seg_5096" n="e" s="T793">tăn </ts>
               <ts e="T795" id="Seg_5098" n="e" s="T794">udagən </ts>
               <ts e="T796" id="Seg_5100" n="e" s="T795">onʼiʔ </ts>
               <ts e="T797" id="Seg_5102" n="e" s="T796">naga, </ts>
               <ts e="T798" id="Seg_5104" n="e" s="T797">baltuzʼiʔ </ts>
               <ts e="T799" id="Seg_5106" n="e" s="T798">bar </ts>
               <ts e="T800" id="Seg_5108" n="e" s="T799">jaʔpial. </ts>
               <ts e="T801" id="Seg_5110" n="e" s="T800">Teinen </ts>
               <ts e="T802" id="Seg_5112" n="e" s="T801">esseŋ </ts>
               <ts e="T803" id="Seg_5114" n="e" s="T802">iʔgö </ts>
               <ts e="T804" id="Seg_5116" n="e" s="T803">bar </ts>
               <ts e="T805" id="Seg_5118" n="e" s="T804">buzo </ts>
               <ts e="T806" id="Seg_5120" n="e" s="T805">sürerleʔpiʔi. </ts>
               <ts e="T807" id="Seg_5122" n="e" s="T806">Dĭ </ts>
               <ts e="T808" id="Seg_5124" n="e" s="T807">bar </ts>
               <ts e="T809" id="Seg_5126" n="e" s="T808">parluʔpi </ts>
               <ts e="T810" id="Seg_5128" n="e" s="T809">i </ts>
               <ts e="T811" id="Seg_5130" n="e" s="T810">nuʔməluʔpi </ts>
               <ts e="T812" id="Seg_5132" n="e" s="T811">maʔndə. </ts>
               <ts e="T813" id="Seg_5134" n="e" s="T812">Nüdʼin </ts>
               <ts e="T814" id="Seg_5136" n="e" s="T813">kalla </ts>
               <ts e="T815" id="Seg_5138" n="e" s="T814">dʼürbi </ts>
               <ts e="T816" id="Seg_5140" n="e" s="T815">bar. </ts>
               <ts e="T817" id="Seg_5142" n="e" s="T816">Nüdʼi </ts>
               <ts e="T818" id="Seg_5144" n="e" s="T817">molaːndəga. </ts>
               <ts e="T819" id="Seg_5146" n="e" s="T818">Kuja </ts>
               <ts e="T820" id="Seg_5148" n="e" s="T819">bar </ts>
               <ts e="T821" id="Seg_5150" n="e" s="T820">amnolaʔbə. </ts>
               <ts e="T822" id="Seg_5152" n="e" s="T821">Nüdʼi </ts>
               <ts e="T823" id="Seg_5154" n="e" s="T822">molaːndəga. </ts>
               <ts e="T824" id="Seg_5156" n="e" s="T823">Kuja </ts>
               <ts e="T825" id="Seg_5158" n="e" s="T824">amnollaʔbə. </ts>
               <ts e="T826" id="Seg_5160" n="e" s="T825">Kabarləj </ts>
               <ts e="T827" id="Seg_5162" n="e" s="T826">dʼăbaktərzittə, </ts>
               <ts e="T828" id="Seg_5164" n="e" s="T827">nada </ts>
               <ts e="T829" id="Seg_5166" n="e" s="T828">iʔbəsʼtə </ts>
               <ts e="T830" id="Seg_5168" n="e" s="T829">kunolzittə. </ts>
               <ts e="T831" id="Seg_5170" n="e" s="T830">Erten </ts>
               <ts e="T832" id="Seg_5172" n="e" s="T831">erte </ts>
               <ts e="T833" id="Seg_5174" n="e" s="T832">nada </ts>
               <ts e="T834" id="Seg_5176" n="e" s="T833">uʔsʼittə. </ts>
               <ts e="T839" id="Seg_5178" n="e" s="T834">Утром рано надо вставать, говорю. </ts>
               <ts e="T840" id="Seg_5180" n="e" s="T839">Kallam </ts>
               <ts e="T841" id="Seg_5182" n="e" s="T840">svetokəʔi </ts>
               <ts e="T842" id="Seg_5184" n="e" s="T841">nĭŋgəsʼtə. </ts>
               <ts e="T843" id="Seg_5186" n="e" s="T842">Sĭre </ts>
               <ts e="T844" id="Seg_5188" n="e" s="T843">pagən </ts>
               <ts e="T845" id="Seg_5190" n="e" s="T844">bar </ts>
               <ts e="T846" id="Seg_5192" n="e" s="T845">sazən </ts>
               <ts e="T847" id="Seg_5194" n="e" s="T846">edleʔbə. </ts>
               <ts e="T848" id="Seg_5196" n="e" s="T847">Ej </ts>
               <ts e="T849" id="Seg_5198" n="e" s="T848">edlia. </ts>
               <ts e="T850" id="Seg_5200" n="e" s="T849">Ĭmbidə </ts>
               <ts e="T851" id="Seg_5202" n="e" s="T850">ej </ts>
               <ts e="T852" id="Seg_5204" n="e" s="T851">kuliom. </ts>
               <ts e="T853" id="Seg_5206" n="e" s="T852">Măn </ts>
               <ts e="T854" id="Seg_5208" n="e" s="T853">tuganbə </ts>
               <ts e="T856" id="Seg_5210" n="e" s="T854">ugaːndə… </ts>
            </ts>
            <ts e="T917" id="Seg_5211" n="sc" s="T857">
               <ts e="T858" id="Seg_5213" n="e" s="T857">Kuŋgə </ts>
               <ts e="T859" id="Seg_5215" n="e" s="T858">amnolaʔbə. </ts>
               <ts e="T860" id="Seg_5217" n="e" s="T859">Nada </ts>
               <ts e="T861" id="Seg_5219" n="e" s="T860">šide </ts>
               <ts e="T862" id="Seg_5221" n="e" s="T861">dʼala </ts>
               <ts e="T863" id="Seg_5223" n="e" s="T862">kanzittə. </ts>
               <ts e="T864" id="Seg_5225" n="e" s="T863">A </ts>
               <ts e="T865" id="Seg_5227" n="e" s="T864">možet </ts>
               <ts e="T866" id="Seg_5229" n="e" s="T865">onʼiʔ </ts>
               <ts e="T867" id="Seg_5231" n="e" s="T866">dʼalanə </ts>
               <ts e="T868" id="Seg_5233" n="e" s="T867">kallal. </ts>
               <ts e="T869" id="Seg_5235" n="e" s="T868">Măn </ts>
               <ts e="T870" id="Seg_5237" n="e" s="T869">dĭʔnə </ts>
               <ts e="T871" id="Seg_5239" n="e" s="T870">šobiam, </ts>
               <ts e="T872" id="Seg_5241" n="e" s="T871">a </ts>
               <ts e="T873" id="Seg_5243" n="e" s="T872">dĭ </ts>
               <ts e="T874" id="Seg_5245" n="e" s="T873">naga. </ts>
               <ts e="T875" id="Seg_5247" n="e" s="T874">Gibərdə </ts>
               <ts e="T876" id="Seg_5249" n="e" s="T875">kalla </ts>
               <ts e="T877" id="Seg_5251" n="e" s="T876">dʼürbi, </ts>
               <ts e="T878" id="Seg_5253" n="e" s="T877">măn </ts>
               <ts e="T879" id="Seg_5255" n="e" s="T878">bar </ts>
               <ts e="T917" id="Seg_5257" n="e" s="T879">parluʔpi. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PKZ">
            <ta e="T12" id="Seg_5258" s="T4">PKZ_1964_SU0207.PKZ.001 (002.001)</ta>
            <ta e="T15" id="Seg_5259" s="T12">PKZ_1964_SU0207.PKZ.002 (002.002)</ta>
            <ta e="T20" id="Seg_5260" s="T15">PKZ_1964_SU0207.PKZ.003 (003.001)</ta>
            <ta e="T28" id="Seg_5261" s="T20">PKZ_1964_SU0207.PKZ.004 (003.002)</ta>
            <ta e="T38" id="Seg_5262" s="T28">PKZ_1964_SU0207.PKZ.005 (003.003)</ta>
            <ta e="T43" id="Seg_5263" s="T39">PKZ_1964_SU0207.PKZ.006 (004.001)</ta>
            <ta e="T46" id="Seg_5264" s="T43">PKZ_1964_SU0207.PKZ.007 (004.002)</ta>
            <ta e="T49" id="Seg_5265" s="T46">PKZ_1964_SU0207.PKZ.008 (004.003)</ta>
            <ta e="T54" id="Seg_5266" s="T49">PKZ_1964_SU0207.PKZ.009 (004.004)</ta>
            <ta e="T59" id="Seg_5267" s="T54">PKZ_1964_SU0207.PKZ.010 (004.005)</ta>
            <ta e="T69" id="Seg_5268" s="T61">PKZ_1964_SU0207.PKZ.011 (004.007)</ta>
            <ta e="T74" id="Seg_5269" s="T69">PKZ_1964_SU0207.PKZ.012 (004.008)</ta>
            <ta e="T78" id="Seg_5270" s="T74">PKZ_1964_SU0207.PKZ.013 (004.009)</ta>
            <ta e="T83" id="Seg_5271" s="T78">PKZ_1964_SU0207.PKZ.014 (004.010)</ta>
            <ta e="T86" id="Seg_5272" s="T83">PKZ_1964_SU0207.PKZ.015 (004.011)</ta>
            <ta e="T90" id="Seg_5273" s="T86">PKZ_1964_SU0207.PKZ.016 (004.012)</ta>
            <ta e="T94" id="Seg_5274" s="T90">PKZ_1964_SU0207.PKZ.017 (004.013)</ta>
            <ta e="T98" id="Seg_5275" s="T94">PKZ_1964_SU0207.PKZ.018 (004.014)</ta>
            <ta e="T102" id="Seg_5276" s="T98">PKZ_1964_SU0207.PKZ.019 (004.015)</ta>
            <ta e="T107" id="Seg_5277" s="T102">PKZ_1964_SU0207.PKZ.020 (004.016)</ta>
            <ta e="T112" id="Seg_5278" s="T107">PKZ_1964_SU0207.PKZ.021 (005.001)</ta>
            <ta e="T114" id="Seg_5279" s="T112">PKZ_1964_SU0207.PKZ.022 (005.002)</ta>
            <ta e="T117" id="Seg_5280" s="T114">PKZ_1964_SU0207.PKZ.023 (005.003)</ta>
            <ta e="T119" id="Seg_5281" s="T117">PKZ_1964_SU0207.PKZ.024 (005.004)</ta>
            <ta e="T121" id="Seg_5282" s="T119">PKZ_1964_SU0207.PKZ.025 (005.005)</ta>
            <ta e="T124" id="Seg_5283" s="T121">PKZ_1964_SU0207.PKZ.026 (005.006)</ta>
            <ta e="T126" id="Seg_5284" s="T124">PKZ_1964_SU0207.PKZ.027 (005.007)</ta>
            <ta e="T130" id="Seg_5285" s="T126">PKZ_1964_SU0207.PKZ.028 (005.008)</ta>
            <ta e="T133" id="Seg_5286" s="T130">PKZ_1964_SU0207.PKZ.029 (006.001)</ta>
            <ta e="T136" id="Seg_5287" s="T133">PKZ_1964_SU0207.PKZ.030 (006.002)</ta>
            <ta e="T143" id="Seg_5288" s="T136">PKZ_1964_SU0207.PKZ.031 (006.003)</ta>
            <ta e="T149" id="Seg_5289" s="T143">PKZ_1964_SU0207.PKZ.032 (006.004)</ta>
            <ta e="T152" id="Seg_5290" s="T149">PKZ_1964_SU0207.PKZ.033 (006.005)</ta>
            <ta e="T155" id="Seg_5291" s="T152">PKZ_1964_SU0207.PKZ.034 (006.006)</ta>
            <ta e="T158" id="Seg_5292" s="T155">PKZ_1964_SU0207.PKZ.035 (006.007)</ta>
            <ta e="T161" id="Seg_5293" s="T158">PKZ_1964_SU0207.PKZ.036 (006.008)</ta>
            <ta e="T162" id="Seg_5294" s="T161">PKZ_1964_SU0207.PKZ.037 (006.009)</ta>
            <ta e="T167" id="Seg_5295" s="T162">PKZ_1964_SU0207.PKZ.038 (006.010)</ta>
            <ta e="T173" id="Seg_5296" s="T167">PKZ_1964_SU0207.PKZ.039 (007)</ta>
            <ta e="T178" id="Seg_5297" s="T173">PKZ_1964_SU0207.PKZ.040 (008.001)</ta>
            <ta e="T183" id="Seg_5298" s="T178">PKZ_1964_SU0207.PKZ.041 (008.002)</ta>
            <ta e="T187" id="Seg_5299" s="T183">PKZ_1964_SU0207.PKZ.042 (008.003)</ta>
            <ta e="T190" id="Seg_5300" s="T187">PKZ_1964_SU0207.PKZ.043 (008.004)</ta>
            <ta e="T195" id="Seg_5301" s="T190">PKZ_1964_SU0207.PKZ.044 (008.005)</ta>
            <ta e="T200" id="Seg_5302" s="T195">PKZ_1964_SU0207.PKZ.045 (008.006)</ta>
            <ta e="T205" id="Seg_5303" s="T200">PKZ_1964_SU0207.PKZ.046 (009.001)</ta>
            <ta e="T209" id="Seg_5304" s="T205">PKZ_1964_SU0207.PKZ.047 (009.002)</ta>
            <ta e="T216" id="Seg_5305" s="T209">PKZ_1964_SU0207.PKZ.048 (009.003)</ta>
            <ta e="T218" id="Seg_5306" s="T216">PKZ_1964_SU0207.PKZ.049 (009.004)</ta>
            <ta e="T222" id="Seg_5307" s="T218">PKZ_1964_SU0207.PKZ.050 (009.005)</ta>
            <ta e="T224" id="Seg_5308" s="T222">PKZ_1964_SU0207.PKZ.051 (009.006)</ta>
            <ta e="T230" id="Seg_5309" s="T224">PKZ_1964_SU0207.PKZ.052 (010.001)</ta>
            <ta e="T234" id="Seg_5310" s="T230">PKZ_1964_SU0207.PKZ.053 (010.002)</ta>
            <ta e="T236" id="Seg_5311" s="T234">PKZ_1964_SU0207.PKZ.054 (010.003)</ta>
            <ta e="T237" id="Seg_5312" s="T236">PKZ_1964_SU0207.PKZ.055 (011)</ta>
            <ta e="T241" id="Seg_5313" s="T237">PKZ_1964_SU0207.PKZ.056 (012.001)</ta>
            <ta e="T243" id="Seg_5314" s="T241">PKZ_1964_SU0207.PKZ.057 (012.002)</ta>
            <ta e="T248" id="Seg_5315" s="T246">PKZ_1964_SU0207.PKZ.058 (012.003)</ta>
            <ta e="T251" id="Seg_5316" s="T248">PKZ_1964_SU0207.PKZ.059 (012.004)</ta>
            <ta e="T258" id="Seg_5317" s="T256">PKZ_1964_SU0207.PKZ.060 (012.005)</ta>
            <ta e="T262" id="Seg_5318" s="T258">PKZ_1964_SU0207.PKZ.061 (012.006)</ta>
            <ta e="T268" id="Seg_5319" s="T262">PKZ_1964_SU0207.PKZ.062 (013)</ta>
            <ta e="T273" id="Seg_5320" s="T269">PKZ_1964_SU0207.PKZ.063 (015.001)</ta>
            <ta e="T278" id="Seg_5321" s="T273">PKZ_1964_SU0207.PKZ.064 (015.002)</ta>
            <ta e="T281" id="Seg_5322" s="T278">PKZ_1964_SU0207.PKZ.065 (016.001)</ta>
            <ta e="T285" id="Seg_5323" s="T281">PKZ_1964_SU0207.PKZ.066 (016.002)</ta>
            <ta e="T289" id="Seg_5324" s="T285">PKZ_1964_SU0207.PKZ.067 (016.003)</ta>
            <ta e="T294" id="Seg_5325" s="T289">PKZ_1964_SU0207.PKZ.068 (016.004)</ta>
            <ta e="T297" id="Seg_5326" s="T294">PKZ_1964_SU0207.PKZ.069 (016.005)</ta>
            <ta e="T300" id="Seg_5327" s="T297">PKZ_1964_SU0207.PKZ.070 (017.001)</ta>
            <ta e="T303" id="Seg_5328" s="T300">PKZ_1964_SU0207.PKZ.071 (017.002)</ta>
            <ta e="T304" id="Seg_5329" s="T303">PKZ_1964_SU0207.PKZ.072 (018)</ta>
            <ta e="T314" id="Seg_5330" s="T304">PKZ_1964_SU0207.PKZ.073 (019.001)</ta>
            <ta e="T317" id="Seg_5331" s="T314">PKZ_1964_SU0207.PKZ.074 (019.002)</ta>
            <ta e="T321" id="Seg_5332" s="T317">PKZ_1964_SU0207.PKZ.075 (019.003)</ta>
            <ta e="T326" id="Seg_5333" s="T321">PKZ_1964_SU0207.PKZ.076 (019.004)</ta>
            <ta e="T331" id="Seg_5334" s="T327">PKZ_1964_SU0207.PKZ.077 (020.001)</ta>
            <ta e="T340" id="Seg_5335" s="T335">PKZ_1964_SU0207.PKZ.078 (020.003)</ta>
            <ta e="T344" id="Seg_5336" s="T340">PKZ_1964_SU0207.PKZ.079 (020.004)</ta>
            <ta e="T352" id="Seg_5337" s="T346">PKZ_1964_SU0207.PKZ.080 (020.006)</ta>
            <ta e="T355" id="Seg_5338" s="T352">PKZ_1964_SU0207.PKZ.081 (021.001)</ta>
            <ta e="T357" id="Seg_5339" s="T355">PKZ_1964_SU0207.PKZ.082 (021.002)</ta>
            <ta e="T361" id="Seg_5340" s="T357">PKZ_1964_SU0207.PKZ.083 (021.003)</ta>
            <ta e="T366" id="Seg_5341" s="T361">PKZ_1964_SU0207.PKZ.084 (021.004)</ta>
            <ta e="T368" id="Seg_5342" s="T366">PKZ_1964_SU0207.PKZ.085 (021.005)</ta>
            <ta e="T372" id="Seg_5343" s="T368">PKZ_1964_SU0207.PKZ.086 (022.001)</ta>
            <ta e="T376" id="Seg_5344" s="T372">PKZ_1964_SU0207.PKZ.087 (022.002)</ta>
            <ta e="T379" id="Seg_5345" s="T376">PKZ_1964_SU0207.PKZ.088 (022.003)</ta>
            <ta e="T385" id="Seg_5346" s="T379">PKZ_1964_SU0207.PKZ.089 (022.004)</ta>
            <ta e="T386" id="Seg_5347" s="T385">PKZ_1964_SU0207.PKZ.090 (023)</ta>
            <ta e="T389" id="Seg_5348" s="T386">PKZ_1964_SU0207.PKZ.091 (024.001)</ta>
            <ta e="T391" id="Seg_5349" s="T389">PKZ_1964_SU0207.PKZ.092 (024.002)</ta>
            <ta e="T393" id="Seg_5350" s="T391">PKZ_1964_SU0207.PKZ.093 (024.003)</ta>
            <ta e="T396" id="Seg_5351" s="T393">PKZ_1964_SU0207.PKZ.094 (024.004)</ta>
            <ta e="T399" id="Seg_5352" s="T396">PKZ_1964_SU0207.PKZ.095 (024.005)</ta>
            <ta e="T404" id="Seg_5353" s="T399">PKZ_1964_SU0207.PKZ.096 (024.006)</ta>
            <ta e="T406" id="Seg_5354" s="T404">PKZ_1964_SU0207.PKZ.097 (024.007)</ta>
            <ta e="T412" id="Seg_5355" s="T409">PKZ_1964_SU0207.PKZ.098 (024.009)</ta>
            <ta e="T415" id="Seg_5356" s="T412">PKZ_1964_SU0207.PKZ.099 (024.010)</ta>
            <ta e="T418" id="Seg_5357" s="T415">PKZ_1964_SU0207.PKZ.100 (025.001)</ta>
            <ta e="T420" id="Seg_5358" s="T418">PKZ_1964_SU0207.PKZ.101 (025.002)</ta>
            <ta e="T424" id="Seg_5359" s="T420">PKZ_1964_SU0207.PKZ.102 (025.003)</ta>
            <ta e="T427" id="Seg_5360" s="T424">PKZ_1964_SU0207.PKZ.103 (025.004)</ta>
            <ta e="T440" id="Seg_5361" s="T427">PKZ_1964_SU0207.PKZ.104 (025.005)</ta>
            <ta e="T444" id="Seg_5362" s="T440">PKZ_1964_SU0207.PKZ.105 (026.001)</ta>
            <ta e="T445" id="Seg_5363" s="T444">PKZ_1964_SU0207.PKZ.106 (026.002)</ta>
            <ta e="T447" id="Seg_5364" s="T445">PKZ_1964_SU0207.PKZ.107 (026.003)</ta>
            <ta e="T451" id="Seg_5365" s="T447">PKZ_1964_SU0207.PKZ.108 (026.004)</ta>
            <ta e="T453" id="Seg_5366" s="T451">PKZ_1964_SU0207.PKZ.109 (027.001)</ta>
            <ta e="T455" id="Seg_5367" s="T453">PKZ_1964_SU0207.PKZ.110 (027.002)</ta>
            <ta e="T460" id="Seg_5368" s="T455">PKZ_1964_SU0207.PKZ.111 (027.003)</ta>
            <ta e="T464" id="Seg_5369" s="T460">PKZ_1964_SU0207.PKZ.112 (027.004)</ta>
            <ta e="T466" id="Seg_5370" s="T464">PKZ_1964_SU0207.PKZ.113 (027.005)</ta>
            <ta e="T468" id="Seg_5371" s="T466">PKZ_1964_SU0207.PKZ.114 (027.006)</ta>
            <ta e="T471" id="Seg_5372" s="T468">PKZ_1964_SU0207.PKZ.115 (027.007)</ta>
            <ta e="T473" id="Seg_5373" s="T471">PKZ_1964_SU0207.PKZ.116 (027.008)</ta>
            <ta e="T477" id="Seg_5374" s="T473">PKZ_1964_SU0207.PKZ.117 (027.009)</ta>
            <ta e="T480" id="Seg_5375" s="T478">PKZ_1964_SU0207.PKZ.118 (029.001)</ta>
            <ta e="T483" id="Seg_5376" s="T480">PKZ_1964_SU0207.PKZ.119 (029.002)</ta>
            <ta e="T486" id="Seg_5377" s="T483">PKZ_1964_SU0207.PKZ.120 (029.003)</ta>
            <ta e="T489" id="Seg_5378" s="T486">PKZ_1964_SU0207.PKZ.121 (029.004)</ta>
            <ta e="T493" id="Seg_5379" s="T489">PKZ_1964_SU0207.PKZ.122 (029.005)</ta>
            <ta e="T497" id="Seg_5380" s="T493">PKZ_1964_SU0207.PKZ.123 (029.006)</ta>
            <ta e="T506" id="Seg_5381" s="T497">PKZ_1964_SU0207.PKZ.124 (030.001)</ta>
            <ta e="T509" id="Seg_5382" s="T506">PKZ_1964_SU0207.PKZ.125 (030.002)</ta>
            <ta e="T517" id="Seg_5383" s="T509">PKZ_1964_SU0207.PKZ.126 (030.003)</ta>
            <ta e="T524" id="Seg_5384" s="T517">PKZ_1964_SU0207.PKZ.127 (030.004)</ta>
            <ta e="T530" id="Seg_5385" s="T524">PKZ_1964_SU0207.PKZ.128 (031)</ta>
            <ta e="T532" id="Seg_5386" s="T530">PKZ_1964_SU0207.PKZ.129 (032.001)</ta>
            <ta e="T536" id="Seg_5387" s="T532">PKZ_1964_SU0207.PKZ.130 (032.002)</ta>
            <ta e="T542" id="Seg_5388" s="T539">PKZ_1964_SU0207.PKZ.131 (032.004)</ta>
            <ta e="T546" id="Seg_5389" s="T542">PKZ_1964_SU0207.PKZ.132 (033.001)</ta>
            <ta e="T548" id="Seg_5390" s="T546">PKZ_1964_SU0207.PKZ.133 (033.002)</ta>
            <ta e="T552" id="Seg_5391" s="T548">PKZ_1964_SU0207.PKZ.134 (034.001)</ta>
            <ta e="T558" id="Seg_5392" s="T552">PKZ_1964_SU0207.PKZ.135 (034.002)</ta>
            <ta e="T560" id="Seg_5393" s="T558">PKZ_1964_SU0207.PKZ.136 (034.003)</ta>
            <ta e="T562" id="Seg_5394" s="T560">PKZ_1964_SU0207.PKZ.137 (034.004)</ta>
            <ta e="T567" id="Seg_5395" s="T562">PKZ_1964_SU0207.PKZ.138 (034.005)</ta>
            <ta e="T570" id="Seg_5396" s="T567">PKZ_1964_SU0207.PKZ.139 (034.006)</ta>
            <ta e="T572" id="Seg_5397" s="T570">PKZ_1964_SU0207.PKZ.140 (034.007)</ta>
            <ta e="T578" id="Seg_5398" s="T572">PKZ_1964_SU0207.PKZ.141 (034.008)</ta>
            <ta e="T581" id="Seg_5399" s="T578">PKZ_1964_SU0207.PKZ.142 (034.009)</ta>
            <ta e="T582" id="Seg_5400" s="T581">PKZ_1964_SU0207.PKZ.143 (034.010)</ta>
            <ta e="T585" id="Seg_5401" s="T582">PKZ_1964_SU0207.PKZ.144 (034.011)</ta>
            <ta e="T595" id="Seg_5402" s="T591">PKZ_1964_SU0207.PKZ.145 (034.013)</ta>
            <ta e="T599" id="Seg_5403" s="T595">PKZ_1964_SU0207.PKZ.146 (035.001)</ta>
            <ta e="T602" id="Seg_5404" s="T599">PKZ_1964_SU0207.PKZ.147 (035.002)</ta>
            <ta e="T610" id="Seg_5405" s="T602">PKZ_1964_SU0207.PKZ.148 (036.001)</ta>
            <ta e="T612" id="Seg_5406" s="T610">PKZ_1964_SU0207.PKZ.149 (036.002)</ta>
            <ta e="T616" id="Seg_5407" s="T612">PKZ_1964_SU0207.PKZ.150 (036.003)</ta>
            <ta e="T619" id="Seg_5408" s="T616">PKZ_1964_SU0207.PKZ.151 (036.004)</ta>
            <ta e="T622" id="Seg_5409" s="T619">PKZ_1964_SU0207.PKZ.152 (036.005)</ta>
            <ta e="T629" id="Seg_5410" s="T626">PKZ_1964_SU0207.PKZ.153 (037.002)</ta>
            <ta e="T632" id="Seg_5411" s="T629">PKZ_1964_SU0207.PKZ.154 (037.003)</ta>
            <ta e="T637" id="Seg_5412" s="T632">PKZ_1964_SU0207.PKZ.155 (038)</ta>
            <ta e="T644" id="Seg_5413" s="T637">PKZ_1964_SU0207.PKZ.156 (039.001)</ta>
            <ta e="T648" id="Seg_5414" s="T644">PKZ_1964_SU0207.PKZ.157 (039.002)</ta>
            <ta e="T649" id="Seg_5415" s="T648">PKZ_1964_SU0207.PKZ.158 (039.003)</ta>
            <ta e="T659" id="Seg_5416" s="T649">PKZ_1964_SU0207.PKZ.159 (040.001)</ta>
            <ta e="T666" id="Seg_5417" s="T659">PKZ_1964_SU0207.PKZ.160 (040.002)</ta>
            <ta e="T672" id="Seg_5418" s="T668">PKZ_1964_SU0207.PKZ.161 (040.004)</ta>
            <ta e="T674" id="Seg_5419" s="T672">PKZ_1964_SU0207.PKZ.162 (040.005)</ta>
            <ta e="T682" id="Seg_5420" s="T674">PKZ_1964_SU0207.PKZ.163 (041.001)</ta>
            <ta e="T692" id="Seg_5421" s="T684">PKZ_1964_SU0207.PKZ.164 (041.003)</ta>
            <ta e="T700" id="Seg_5422" s="T696">PKZ_1964_SU0207.PKZ.165 (041.005)</ta>
            <ta e="T706" id="Seg_5423" s="T700">PKZ_1964_SU0207.PKZ.166 (042.001)</ta>
            <ta e="T712" id="Seg_5424" s="T706">PKZ_1964_SU0207.PKZ.167 (042.002)</ta>
            <ta e="T719" id="Seg_5425" s="T712">PKZ_1964_SU0207.PKZ.168 (043.001)</ta>
            <ta e="T727" id="Seg_5426" s="T719">PKZ_1964_SU0207.PKZ.169 (043.002)</ta>
            <ta e="T733" id="Seg_5427" s="T727">PKZ_1964_SU0207.PKZ.170 (044.001)</ta>
            <ta e="T740" id="Seg_5428" s="T733">PKZ_1964_SU0207.PKZ.171 (044.002)</ta>
            <ta e="T745" id="Seg_5429" s="T740">PKZ_1964_SU0207.PKZ.172 (044.003)</ta>
            <ta e="T755" id="Seg_5430" s="T901">PKZ_1964_SU0207.PKZ.173 (044.005)</ta>
            <ta e="T761" id="Seg_5431" s="T758">PKZ_1964_SU0207.PKZ.174 (044.007)</ta>
            <ta e="T764" id="Seg_5432" s="T761">PKZ_1964_SU0207.PKZ.175 (044.008)</ta>
            <ta e="T767" id="Seg_5433" s="T764">PKZ_1964_SU0207.PKZ.176 (045)</ta>
            <ta e="T775" id="Seg_5434" s="T767">PKZ_1964_SU0207.PKZ.177 (046.001)</ta>
            <ta e="T788" id="Seg_5435" s="T779">PKZ_1964_SU0207.PKZ.178 (046.004)</ta>
            <ta e="T800" id="Seg_5436" s="T788">PKZ_1964_SU0207.PKZ.179 (047)</ta>
            <ta e="T806" id="Seg_5437" s="T800">PKZ_1964_SU0207.PKZ.180 (048)</ta>
            <ta e="T812" id="Seg_5438" s="T806">PKZ_1964_SU0207.PKZ.181 (049)</ta>
            <ta e="T816" id="Seg_5439" s="T812">PKZ_1964_SU0207.PKZ.182 (050.001)</ta>
            <ta e="T818" id="Seg_5440" s="T816">PKZ_1964_SU0207.PKZ.183 (050.002)</ta>
            <ta e="T821" id="Seg_5441" s="T818">PKZ_1964_SU0207.PKZ.184 (050.003)</ta>
            <ta e="T823" id="Seg_5442" s="T821">PKZ_1964_SU0207.PKZ.185 (050.004)</ta>
            <ta e="T825" id="Seg_5443" s="T823">PKZ_1964_SU0207.PKZ.186 (050.005)</ta>
            <ta e="T830" id="Seg_5444" s="T825">PKZ_1964_SU0207.PKZ.187 (050.006)</ta>
            <ta e="T834" id="Seg_5445" s="T830">PKZ_1964_SU0207.PKZ.188 (050.007)</ta>
            <ta e="T839" id="Seg_5446" s="T834">PKZ_1964_SU0207.PKZ.189 (050.008)</ta>
            <ta e="T842" id="Seg_5447" s="T839">PKZ_1964_SU0207.PKZ.190 (051)</ta>
            <ta e="T847" id="Seg_5448" s="T842">PKZ_1964_SU0207.PKZ.191 (052)</ta>
            <ta e="T849" id="Seg_5449" s="T847">PKZ_1964_SU0207.PKZ.192 (053)</ta>
            <ta e="T852" id="Seg_5450" s="T849">PKZ_1964_SU0207.PKZ.193 (054)</ta>
            <ta e="T856" id="Seg_5451" s="T852">PKZ_1964_SU0207.PKZ.194 (055.001)</ta>
            <ta e="T859" id="Seg_5452" s="T857">PKZ_1964_SU0207.PKZ.195 (055.003)</ta>
            <ta e="T863" id="Seg_5453" s="T859">PKZ_1964_SU0207.PKZ.196 (055.004)</ta>
            <ta e="T868" id="Seg_5454" s="T863">PKZ_1964_SU0207.PKZ.197 (055.005)</ta>
            <ta e="T874" id="Seg_5455" s="T868">PKZ_1964_SU0207.PKZ.198 (056.001)</ta>
            <ta e="T917" id="Seg_5456" s="T874">PKZ_1964_SU0207.PKZ.199 (056.002)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PKZ">
            <ta e="T12" id="Seg_5457" s="T4">Măn ej moliam (mĭngz -) (mĭŋgəlsʼtə), bar ĭzemneʔbə. </ta>
            <ta e="T15" id="Seg_5458" s="T12">Adnaka măn külaːmbiam. </ta>
            <ta e="T20" id="Seg_5459" s="T15">Teinen dĭ koʔbdo urgo dʼala. </ta>
            <ta e="T28" id="Seg_5460" s="T20">Dĭm teinen (ia - iad -) ijad deʔpi. </ta>
            <ta e="T38" id="Seg_5461" s="T28">Dĭʔnə (bʼeʔ š -) bʼeʔ šide pʼe i iššo šide. </ta>
            <ta e="T43" id="Seg_5462" s="T39">Găda dĭ kuza marʼi. </ta>
            <ta e="T46" id="Seg_5463" s="T43">Ĭmbidə ej ile. </ta>
            <ta e="T49" id="Seg_5464" s="T46">Ĭmbidə ej mĭlej. </ta>
            <ta e="T54" id="Seg_5465" s="T49">A baška kuza ugaːndə jakšə. </ta>
            <ta e="T59" id="Seg_5466" s="T54">Bar mĭlüʔləj, (ĭm -) ĭmbi. </ta>
            <ta e="T69" id="Seg_5467" s="T61">Ĭmbi ej (pʼer - m -) ĭmbi … </ta>
            <ta e="T74" id="Seg_5468" s="T69">Ĭmbi ej pilel, bar mĭlej. </ta>
            <ta e="T78" id="Seg_5469" s="T74">Dĭn ugaːndə sĭjdə jakšə. </ta>
            <ta e="T83" id="Seg_5470" s="T78">Как это сразу ты … ((DMG))</ta>
            <ta e="T86" id="Seg_5471" s="T83">Ara ej bĭtlie. </ta>
            <ta e="T90" id="Seg_5472" s="T86">Ĭmbi ige, bar mĭləj. </ta>
            <ta e="T94" id="Seg_5473" s="T90">Munujʔ mĭlie, (uj -). </ta>
            <ta e="T98" id="Seg_5474" s="T94">Kajaʔ mĭlie, ipek mĭlie. </ta>
            <ta e="T102" id="Seg_5475" s="T98">Uja ige dăk, mĭləj. </ta>
            <ta e="T107" id="Seg_5476" s="T102">Bar ĭmbi ige, bar mĭləj. </ta>
            <ta e="T112" id="Seg_5477" s="T107">Ugaːndə (nʼu -) nʼuʔnən kuzurie. </ta>
            <ta e="T114" id="Seg_5478" s="T112">Kudaj kuzurleʔbə. </ta>
            <ta e="T117" id="Seg_5479" s="T114">(Šindən-) Šindi-nʼibudʼ kutləj. </ta>
            <ta e="T119" id="Seg_5480" s="T117">Радуга nulaʔbə. </ta>
            <ta e="T121" id="Seg_5481" s="T119">Bü ileʔbə. </ta>
            <ta e="T124" id="Seg_5482" s="T121">Dĭgəttə surno kalləj. </ta>
            <ta e="T126" id="Seg_5483" s="T124">Toskanak kalləj. </ta>
            <ta e="T130" id="Seg_5484" s="T126">Ugaːndə tutše sagər šonəga. </ta>
            <ta e="T133" id="Seg_5485" s="T130">Măn nʼiʔdə kambiam. </ta>
            <ta e="T136" id="Seg_5486" s="T133">Kukuška bar nʼergölaʔbə. </ta>
            <ta e="T143" id="Seg_5487" s="T136">Măndolam, dĭ turanə amnəbi bar i kirgarlaʔbə. </ta>
            <ta e="T149" id="Seg_5488" s="T143">Măn măndəm: unnʼa (amnolaʔbə-) amnolam bar. </ta>
            <ta e="T152" id="Seg_5489" s="T149">Verno unnʼa amnolam. </ta>
            <ta e="T155" id="Seg_5490" s="T152">Pizʼiʔ bar barəʔpi. </ta>
            <ta e="T158" id="Seg_5491" s="T155">Pazʼiʔ bar barəʔpi. </ta>
            <ta e="T161" id="Seg_5492" s="T158">Dĭ uge amnolaʔbə. </ta>
            <ta e="T162" id="Seg_5493" s="T161">Kirgarlaʔbə. </ta>
            <ta e="T167" id="Seg_5494" s="T162">Dĭgəttə nʼergölüʔpi, gijen kuja uʔbdlaʔbə. </ta>
            <ta e="T173" id="Seg_5495" s="T167">Ugaːndə külüʔsəbi il bar pʼaŋdəbiʔi sazəndə. </ta>
            <ta e="T178" id="Seg_5496" s="T173">Dĭ kuza ugaːndə ej jakšə. </ta>
            <ta e="T183" id="Seg_5497" s="T178">Bar ĭmbi ileʔbə da … </ta>
            <ta e="T187" id="Seg_5498" s="T183">Bar ĭmbi ileʔbə bar. </ta>
            <ta e="T190" id="Seg_5499" s="T187">Ugaːndə toli kuza. </ta>
            <ta e="T195" id="Seg_5500" s="T190">Bar ĭmbi ilie dăk … </ta>
            <ta e="T200" id="Seg_5501" s="T195">Bar ileʔbə bar, kundlaʔbə maʔndə. </ta>
            <ta e="T205" id="Seg_5502" s="T200">Măn teinen koʔbdonə platəm ibiem. </ta>
            <ta e="T209" id="Seg_5503" s="T205">Dĭ kambi Malinovkanə tüšəlzittə. </ta>
            <ta e="T216" id="Seg_5504" s="T209">Dĭn nüke surarlaʔbə:” Gijen tăn plat ibiel?”. </ta>
            <ta e="T218" id="Seg_5505" s="T216">Urgaja mĭbi. </ta>
            <ta e="T222" id="Seg_5506" s="T218">A urgaja gijen ibi? </ta>
            <ta e="T224" id="Seg_5507" s="T222">Polʼa mĭbi. </ta>
            <ta e="T230" id="Seg_5508" s="T224">Dö ine ibiem, iʔgö aktʼa mĭbiem. </ta>
            <ta e="T234" id="Seg_5509" s="T230">A baška ine ibiem. </ta>
            <ta e="T236" id="Seg_5510" s="T234">Amga mĭbiem. </ta>
            <ta e="T237" id="Seg_5511" s="T236">Много отдала. ((DMG))</ta>
            <ta e="T241" id="Seg_5512" s="T237">Dʼaʔpiam (budo -) buzo. </ta>
            <ta e="T243" id="Seg_5513" s="T241">Bar… </ta>
            <ta e="T248" id="Seg_5514" s="T246">Ej kandəga. </ta>
            <ta e="T251" id="Seg_5515" s="T248">Măn bar …</ta>
            <ta e="T258" id="Seg_5516" s="T256">Ugaːndə kuštu. </ta>
            <ta e="T262" id="Seg_5517" s="T258">Ej moliam kunzittə bar. </ta>
            <ta e="T268" id="Seg_5518" s="T262">(Kundla - kundəga -) Kandəgam bar. </ta>
            <ta e="T273" id="Seg_5519" s="T269">Ej jakšə (nʼi) nʼi. </ta>
            <ta e="T278" id="Seg_5520" s="T273">Üge bar tajirlaʔbə da kundlaʔbə. </ta>
            <ta e="T281" id="Seg_5521" s="T278">Măn dʼijegən mĭmbiem. </ta>
            <ta e="T285" id="Seg_5522" s="T281">Ĭmbidə (ku -) kubiam. </ta>
            <ta e="T289" id="Seg_5523" s="T285">Dĭgəttə măn nererlüʔpiem bar. </ta>
            <ta e="T294" id="Seg_5524" s="T289">Dĭgəttə (kuž -) kudaj kăštəbiam. </ta>
            <ta e="T297" id="Seg_5525" s="T294">Bar kalla dʼürbiʔi. </ta>
            <ta e="T300" id="Seg_5526" s="T297">((DMG)) Nada šiʔi păjdəsʼtə. </ta>
            <ta e="T303" id="Seg_5527" s="T300">Nada varota azittə. </ta>
            <ta e="T304" id="Seg_5528" s="T303">((a-))… ((BRK)). </ta>
            <ta e="T314" id="Seg_5529" s="T304">(Dĭ ši - š -) Dĭ šiʔnə nada paʔi păʔsittə. </ta>
            <ta e="T317" id="Seg_5530" s="T314">((DMG)) Dĭgəttə šeden kajləbəj. </ta>
            <ta e="T321" id="Seg_5531" s="T317">Dĭgəttə buzo ej nuʔmələj. </ta>
            <ta e="T326" id="Seg_5532" s="T321">(Šedengən amnolaʔ -) Šedengən nulaʔbə. </ta>
            <ta e="T331" id="Seg_5533" s="T327">((DMG)) Ну там скажи чего. </ta>
            <ta e="T340" id="Seg_5534" s="T335">(Ĭm -) Ĭmbizʼidə bar putʼəmnia. </ta>
            <ta e="T344" id="Seg_5535" s="T340">Tăn püjel ugaːndə … </ta>
            <ta e="T352" id="Seg_5536" s="T346">((DMG)) Tăn püjel ugaːndə puʔmə, bar nʼilgölaʔbə. </ta>
            <ta e="T355" id="Seg_5537" s="T352">Tăn gijen ibiel? </ta>
            <ta e="T357" id="Seg_5538" s="T355">Büjleʔ mĭmbiem. </ta>
            <ta e="T361" id="Seg_5539" s="T357">Nada segi bü mĭnzərzittə. </ta>
            <ta e="T366" id="Seg_5540" s="T361">Nada mĭje (mĭnə -) mĭnzərzittə. </ta>
            <ta e="T368" id="Seg_5541" s="T366">Tibizeŋ bădəzʼittə. </ta>
            <ta e="T372" id="Seg_5542" s="T368">Măn bʼeʔ aktʼa ige. </ta>
            <ta e="T376" id="Seg_5543" s="T372">Nada tănan sumna mĭzittə. </ta>
            <ta e="T379" id="Seg_5544" s="T376">Sumna boskəndə mazittə. </ta>
            <ta e="T385" id="Seg_5545" s="T379">Možet giber-nʼibudʼ kallal, aktʼa kereʔ moləj. </ta>
            <ta e="T386" id="Seg_5546" s="T385">((DMG)). </ta>
            <ta e="T389" id="Seg_5547" s="T386">Matvejev bar amnolaʔbə. </ta>
            <ta e="T391" id="Seg_5548" s="T389">Ej dʼăbaktəria. </ta>
            <ta e="T393" id="Seg_5549" s="T391">Sĭjdə ĭzemnie. </ta>
            <ta e="T396" id="Seg_5550" s="T393">Maʔnal sazən naga. </ta>
            <ta e="T399" id="Seg_5551" s="T396">Net bar kuroluʔpi. </ta>
            <ta e="T404" id="Seg_5552" s="T399">Ej pʼaŋdəlia (sažə -) sazən. </ta>
            <ta e="T406" id="Seg_5553" s="T404">Iʔ kuroʔ! </ta>
            <ta e="T412" id="Seg_5554" s="T409">Büžü maʔnən šolam. </ta>
            <ta e="T415" id="Seg_5555" s="T412">Ĭmbi-nʼibudʼ tănan detlem. </ta>
            <ta e="T418" id="Seg_5556" s="T415">Tăŋ ej dʼăbaktəraʔ. </ta>
            <ta e="T420" id="Seg_5557" s="T418">Koldʼəŋ dʼăbaktəraʔ! </ta>
            <ta e="T424" id="Seg_5558" s="T420">A to bar il nünieʔi. </ta>
            <ta e="T427" id="Seg_5559" s="T424">Tăŋ iʔ kirgaraʔ! </ta>
            <ta e="T440" id="Seg_5560" s="T427">A (iʔ) tăn iʔ kudonzaʔ, a to il bar nünleʔbəʔjə kăda (tăn) tăn kudonzlaʔbəl. </ta>
            <ta e="T444" id="Seg_5561" s="T440">Măn teinen oroma kămnabiam. </ta>
            <ta e="T445" id="Seg_5562" s="T444">Bĭlgarbiam. </ta>
            <ta e="T447" id="Seg_5563" s="T445">Krinkaʔi bəzəjdəbiam. </ta>
            <ta e="T451" id="Seg_5564" s="T447">Edəbiem, kuja koʔlaʔbə bar. </ta>
            <ta e="T453" id="Seg_5565" s="T451">Ĭmbi deʔpiel? </ta>
            <ta e="T455" id="Seg_5566" s="T453">Pʼerdə măna. </ta>
            <ta e="T460" id="Seg_5567" s="T455">(Ibi -) Ĭmbi dĭn iʔbölaʔbə? </ta>
            <ta e="T464" id="Seg_5568" s="T460">Măn teinen kalbajlaʔ mĭmbiem. </ta>
            <ta e="T466" id="Seg_5569" s="T464">Kalba deʔpiem. </ta>
            <ta e="T468" id="Seg_5570" s="T466">Dĭgəttə dʼagarlam. </ta>
            <ta e="T471" id="Seg_5571" s="T468">Munujʔ (ene-) ellem. </ta>
            <ta e="T473" id="Seg_5572" s="T471">Oroma ellem. </ta>
            <ta e="T477" id="Seg_5573" s="T473">Dĭgəttə pürzittə pirogəʔi molam. </ta>
            <ta e="T480" id="Seg_5574" s="T478">Uja mĭnzerbiem. </ta>
            <ta e="T483" id="Seg_5575" s="T480">Uja ambiʔi (tĭmeizittə). </ta>
            <ta e="T486" id="Seg_5576" s="T483">Onʼiʔ le maluppi. </ta>
            <ta e="T489" id="Seg_5577" s="T486">Одни кости остались. </ta>
            <ta e="T493" id="Seg_5578" s="T489">Menzeŋ bar le amnaʔbəʔjə. </ta>
            <ta e="T497" id="Seg_5579" s="T493">Ugaːndə iʔgö le barəʔpibaʔ. </ta>
            <ta e="T506" id="Seg_5580" s="T497">(Măn ten -) Măn teinen ipek pürbiem, ej namzəga. </ta>
            <ta e="T509" id="Seg_5581" s="T506">Ej jakšə bar. </ta>
            <ta e="T517" id="Seg_5582" s="T509">Măn dĭ ipek dĭ (mendə -) menzeŋdə mĭbiem. </ta>
            <ta e="T524" id="Seg_5583" s="T517">Menzeŋ bar ipek amnaʔbəʔjə i bar dʼabərolaʔbəʔjə. </ta>
            <ta e="T530" id="Seg_5584" s="T524">Men bar dĭ mendə timnet bar. </ta>
            <ta e="T532" id="Seg_5585" s="T530">Nudʼi ibi. </ta>
            <ta e="T536" id="Seg_5586" s="T532">Nudʼi ibi, ugaːndə … </ta>
            <ta e="T542" id="Seg_5587" s="T539">Ĭmbidə ej edlia. </ta>
            <ta e="T546" id="Seg_5588" s="T542">Măn dĭm ej tĭmnebiem. </ta>
            <ta e="T548" id="Seg_5589" s="T546">Šindi šonəga. </ta>
            <ta e="T552" id="Seg_5590" s="T548">Măn abam tujeziʔi abi. </ta>
            <ta e="T558" id="Seg_5591" s="T552">(Kuba) Kuba (kür-) kürbi i alaʔbəbi. </ta>
            <ta e="T560" id="Seg_5592" s="T558">((DMG)) Šojdʼo šöʔpiʔi. </ta>
            <ta e="T562" id="Seg_5593" s="T560">Žilaʔizʼiʔ bar. </ta>
            <ta e="T567" id="Seg_5594" s="T562">Nʼimizʼiʔ i jendak müjögən ibi. </ta>
            <ta e="T570" id="Seg_5595" s="T567">((DMG)) Šojdʼoʔi šöʔpiʔi. </ta>
            <ta e="T572" id="Seg_5596" s="T570">Bar žilaʔizʼiʔ. </ta>
            <ta e="T578" id="Seg_5597" s="T572">I (jek -) jendak šerbi müjönə. </ta>
            <ta e="T581" id="Seg_5598" s="T578">I nʼimi ibi. </ta>
            <ta e="T582" id="Seg_5599" s="T581">Šödörbi. </ta>
            <ta e="T585" id="Seg_5600" s="T582">Всё, всё … </ta>
            <ta e="T595" id="Seg_5601" s="T591">(Müj-) Müjöbə bar muʔləj. </ta>
            <ta e="T599" id="Seg_5602" s="T595">Tăn bar nel ajirlaʔbəl. </ta>
            <ta e="T602" id="Seg_5603" s="T599">Sĭjdə ĭzemnie ugaːndə. </ta>
            <ta e="T610" id="Seg_5604" s="T602">Măn bar turam sagər, (karə -) karəldʼan bazlam. </ta>
            <ta e="T612" id="Seg_5605" s="T610">Ugaːndə balgaš. </ta>
            <ta e="T616" id="Seg_5606" s="T612">A to il bar mălleʔbəʔjə. </ta>
            <ta e="T619" id="Seg_5607" s="T616">Măliaʔi: ɨrɨː nüke. </ta>
            <ta e="T622" id="Seg_5608" s="T619">Tura ej bazəlia. </ta>
            <ta e="T629" id="Seg_5609" s="T626">Ĭmbi tănan dʼăbaktərzittə? </ta>
            <ta e="T632" id="Seg_5610" s="T629">Măn bar nöməlbiom. </ta>
            <ta e="T637" id="Seg_5611" s="T632">Teinen kunolzittə klopəʔi ej mĭbiʔi. </ta>
            <ta e="T644" id="Seg_5612" s="T637">((DMG)) Dĭ kuza un pʼebi, nada dĭʔnə mĭzittə. </ta>
            <ta e="T648" id="Seg_5613" s="T644">Dĭn ipek naga amzittə. </ta>
            <ta e="T649" id="Seg_5614" s="T648">Bar. </ta>
            <ta e="T659" id="Seg_5615" s="T649">(Dʼ - n - n -) Gijen izittə, ĭmbi dʼăbaktərzʼittə. </ta>
            <ta e="T666" id="Seg_5616" s="T659">Dĭʔnə ugaːndə (nʼiʔgö -) iʔgö nada dʼăbaktərzittə. </ta>
            <ta e="T672" id="Seg_5617" s="T668">Nada kanzittə măndərzittə. </ta>
            <ta e="T674" id="Seg_5618" s="T672">Gijen-nʼibudʼ dʼăbaktərzittə. </ta>
            <ta e="T682" id="Seg_5619" s="T674">Măn taldʼen kundʼo ej (iʔb -) iʔbəbiem kunolzittə. </ta>
            <ta e="T692" id="Seg_5620" s="T684">Kundʼo (ej iʔbəlaʔb- ej iʔbə-) ej iʔbəlem kunolzittə. </ta>
            <ta e="T700" id="Seg_5621" s="T696">Kunolbiam, ĭmbidə ej kubiam. </ta>
            <ta e="T706" id="Seg_5622" s="T700">Dĭ kuzagən eʔbdət bar sagər ibi. </ta>
            <ta e="T712" id="Seg_5623" s="T706">Tüj bar ugaːndə sĭre molaːmbiʔi eʔbdə. </ta>
            <ta e="T719" id="Seg_5624" s="T712">Onʼiʔ uda ige, a onʼiʔ uda nĭŋgəbiʔi. </ta>
            <ta e="T727" id="Seg_5625" s="T719">Onʼiʔ uda ige, a onʼiʔ udabə bar sajnĭŋgəbiʔi. </ta>
            <ta e="T733" id="Seg_5626" s="T727">Măn üzəbiem, (один) onʼiʔ udam băldəbiam. </ta>
            <ta e="T740" id="Seg_5627" s="T733">(Măs-) Măn saʔməluʔpiam i udam (băldə-) băldəlbiam. </ta>
            <ta e="T745" id="Seg_5628" s="T740">Dĭgəttə dĭ udam embiʔi bar. </ta>
            <ta e="T755" id="Seg_5629" s="T901">((BRK)) Dĭgəttə udam bar sarbiʔi. </ta>
            <ta e="T761" id="Seg_5630" s="T758">Paka ej özerləj. </ta>
            <ta e="T764" id="Seg_5631" s="T761">Ĭmbidə togonorzittə nʼelʼzja. </ta>
            <ta e="T767" id="Seg_5632" s="T764">Ine körerzittə nada. </ta>
            <ta e="T775" id="Seg_5633" s="T767">(I - in -) Ine (nu-) nuʔməleʔpi bar. </ta>
            <ta e="T788" id="Seg_5634" s="T779">(I -) Ine nuʔməleʔpi, saʔməluʔpi, (üjüt băldə-) üjüt băldəbi. </ta>
            <ta e="T800" id="Seg_5635" s="T788">Măn udagən sumna müjözeŋbə, a tăn udagən onʼiʔ naga, baltuzʼiʔ bar jaʔpial. </ta>
            <ta e="T806" id="Seg_5636" s="T800">Teinen esseŋ iʔgö bar buzo sürerleʔpiʔi. </ta>
            <ta e="T812" id="Seg_5637" s="T806">Dĭ bar parluʔpi i nuʔməluʔpi maʔndə. </ta>
            <ta e="T816" id="Seg_5638" s="T812">((DMG)) Nüdʼin kalla dʼürbi bar. </ta>
            <ta e="T818" id="Seg_5639" s="T816">Nüdʼi molaːndəga. </ta>
            <ta e="T821" id="Seg_5640" s="T818">Kuja bar amnolaʔbə. </ta>
            <ta e="T823" id="Seg_5641" s="T821">Nüdʼi molaːndəga. </ta>
            <ta e="T825" id="Seg_5642" s="T823">Kuja amnollaʔbə. </ta>
            <ta e="T830" id="Seg_5643" s="T825">Kabarləj dʼăbaktərzittə, nada iʔbəsʼtə kunolzittə. </ta>
            <ta e="T834" id="Seg_5644" s="T830">Erten erte nada uʔsʼittə. </ta>
            <ta e="T839" id="Seg_5645" s="T834">Утром рано надо вставать, говорю. </ta>
            <ta e="T842" id="Seg_5646" s="T839">Kallam svetokəʔi nĭŋgəsʼtə. </ta>
            <ta e="T847" id="Seg_5647" s="T842">Sĭre pagən bar sazən edleʔbə. </ta>
            <ta e="T849" id="Seg_5648" s="T847">((DMG)) Ej edlia. </ta>
            <ta e="T852" id="Seg_5649" s="T849">Ĭmbidə ej kuliom. </ta>
            <ta e="T856" id="Seg_5650" s="T852">Măn tuganbə ugaːndə … </ta>
            <ta e="T859" id="Seg_5651" s="T857">Kuŋgə amnolaʔbə. </ta>
            <ta e="T863" id="Seg_5652" s="T859">Nada šide dʼala kanzittə. </ta>
            <ta e="T868" id="Seg_5653" s="T863">A možet onʼiʔ dʼalanə kallal. </ta>
            <ta e="T874" id="Seg_5654" s="T868">Măn dĭʔnə šobiam, a dĭ naga. </ta>
            <ta e="T917" id="Seg_5655" s="T874">Gibərdə kalla dʼürbi, măn bar parluʔpi. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-PKZ">
            <ta e="T5" id="Seg_5656" s="T4">măn</ta>
            <ta e="T6" id="Seg_5657" s="T5">ej</ta>
            <ta e="T7" id="Seg_5658" s="T6">mo-lia-m</ta>
            <ta e="T10" id="Seg_5659" s="T9">mĭŋgəl-sʼtə</ta>
            <ta e="T11" id="Seg_5660" s="T10">bar</ta>
            <ta e="T12" id="Seg_5661" s="T11">ĭzem-neʔbə</ta>
            <ta e="T13" id="Seg_5662" s="T12">adnaka</ta>
            <ta e="T14" id="Seg_5663" s="T13">măn</ta>
            <ta e="T15" id="Seg_5664" s="T14">kü-laːm-bia-m</ta>
            <ta e="T16" id="Seg_5665" s="T15">teinen</ta>
            <ta e="T17" id="Seg_5666" s="T16">dĭ</ta>
            <ta e="T18" id="Seg_5667" s="T17">koʔbdo</ta>
            <ta e="T19" id="Seg_5668" s="T18">urgo</ta>
            <ta e="T20" id="Seg_5669" s="T19">dʼala</ta>
            <ta e="T21" id="Seg_5670" s="T20">dĭ-m</ta>
            <ta e="T22" id="Seg_5671" s="T21">teinen</ta>
            <ta e="T27" id="Seg_5672" s="T26">ija-d</ta>
            <ta e="T28" id="Seg_5673" s="T27">deʔ-pi</ta>
            <ta e="T29" id="Seg_5674" s="T28">dĭʔ-nə</ta>
            <ta e="T33" id="Seg_5675" s="T32">bʼeʔ</ta>
            <ta e="T34" id="Seg_5676" s="T33">šide</ta>
            <ta e="T35" id="Seg_5677" s="T34">pʼe</ta>
            <ta e="T36" id="Seg_5678" s="T35">i</ta>
            <ta e="T37" id="Seg_5679" s="T36">iššo</ta>
            <ta e="T38" id="Seg_5680" s="T37">šide</ta>
            <ta e="T40" id="Seg_5681" s="T39">găda</ta>
            <ta e="T41" id="Seg_5682" s="T40">dĭ</ta>
            <ta e="T42" id="Seg_5683" s="T41">kuza</ta>
            <ta e="T43" id="Seg_5684" s="T42">marʼi</ta>
            <ta e="T44" id="Seg_5685" s="T43">ĭmbi=də</ta>
            <ta e="T45" id="Seg_5686" s="T44">ej</ta>
            <ta e="T46" id="Seg_5687" s="T45">i-le</ta>
            <ta e="T47" id="Seg_5688" s="T46">ĭmbi=də</ta>
            <ta e="T48" id="Seg_5689" s="T47">ej</ta>
            <ta e="T49" id="Seg_5690" s="T48">mĭ-le-j</ta>
            <ta e="T50" id="Seg_5691" s="T49">a</ta>
            <ta e="T51" id="Seg_5692" s="T50">baška</ta>
            <ta e="T52" id="Seg_5693" s="T51">kuza</ta>
            <ta e="T53" id="Seg_5694" s="T52">ugaːndə</ta>
            <ta e="T54" id="Seg_5695" s="T53">jakšə</ta>
            <ta e="T55" id="Seg_5696" s="T54">bar</ta>
            <ta e="T56" id="Seg_5697" s="T55">mĭ-lüʔ-lə-j</ta>
            <ta e="T59" id="Seg_5698" s="T58">ĭmbi</ta>
            <ta e="T62" id="Seg_5699" s="T61">ĭmbi</ta>
            <ta e="T63" id="Seg_5700" s="T62">ej</ta>
            <ta e="T69" id="Seg_5701" s="T67">ĭmbi</ta>
            <ta e="T70" id="Seg_5702" s="T69">ĭmbi</ta>
            <ta e="T71" id="Seg_5703" s="T70">ej</ta>
            <ta e="T72" id="Seg_5704" s="T71">pi-le-l</ta>
            <ta e="T73" id="Seg_5705" s="T72">bar</ta>
            <ta e="T74" id="Seg_5706" s="T73">mĭ-le-j</ta>
            <ta e="T75" id="Seg_5707" s="T74">dĭ-n</ta>
            <ta e="T76" id="Seg_5708" s="T75">ugaːndə</ta>
            <ta e="T77" id="Seg_5709" s="T76">sĭj-də</ta>
            <ta e="T78" id="Seg_5710" s="T77">jakšə</ta>
            <ta e="T84" id="Seg_5711" s="T83">ara</ta>
            <ta e="T85" id="Seg_5712" s="T84">ej</ta>
            <ta e="T86" id="Seg_5713" s="T85">bĭt-lie</ta>
            <ta e="T87" id="Seg_5714" s="T86">ĭmbi</ta>
            <ta e="T88" id="Seg_5715" s="T87">i-ge</ta>
            <ta e="T89" id="Seg_5716" s="T88">bar</ta>
            <ta e="T90" id="Seg_5717" s="T89">mĭ-lə-j</ta>
            <ta e="T91" id="Seg_5718" s="T90">munuj-ʔ</ta>
            <ta e="T92" id="Seg_5719" s="T91">mĭ-lie</ta>
            <ta e="T95" id="Seg_5720" s="T94">kajaʔ</ta>
            <ta e="T96" id="Seg_5721" s="T95">mĭ-lie</ta>
            <ta e="T97" id="Seg_5722" s="T96">ipek</ta>
            <ta e="T98" id="Seg_5723" s="T97">mĭ-lie</ta>
            <ta e="T99" id="Seg_5724" s="T98">uja</ta>
            <ta e="T100" id="Seg_5725" s="T99">i-ge</ta>
            <ta e="T101" id="Seg_5726" s="T100">dăk</ta>
            <ta e="T102" id="Seg_5727" s="T101">mĭ-lə-j</ta>
            <ta e="T103" id="Seg_5728" s="T102">bar</ta>
            <ta e="T104" id="Seg_5729" s="T103">ĭmbi</ta>
            <ta e="T105" id="Seg_5730" s="T104">i-ge</ta>
            <ta e="T106" id="Seg_5731" s="T105">bar</ta>
            <ta e="T107" id="Seg_5732" s="T106">mĭ-lə-j</ta>
            <ta e="T108" id="Seg_5733" s="T107">ugaːndə</ta>
            <ta e="T111" id="Seg_5734" s="T110">nʼuʔnən</ta>
            <ta e="T112" id="Seg_5735" s="T111">kuzur-ie</ta>
            <ta e="T113" id="Seg_5736" s="T112">kudaj</ta>
            <ta e="T114" id="Seg_5737" s="T113">kuzur-leʔbə</ta>
            <ta e="T116" id="Seg_5738" s="T115">šindi=nʼibudʼ</ta>
            <ta e="T117" id="Seg_5739" s="T116">kut-lə-j</ta>
            <ta e="T118" id="Seg_5740" s="T117">raduga</ta>
            <ta e="T119" id="Seg_5741" s="T118">nu-laʔbə</ta>
            <ta e="T120" id="Seg_5742" s="T119">bü</ta>
            <ta e="T121" id="Seg_5743" s="T120">i-leʔbə</ta>
            <ta e="T122" id="Seg_5744" s="T121">dĭgəttə</ta>
            <ta e="T123" id="Seg_5745" s="T122">surno</ta>
            <ta e="T124" id="Seg_5746" s="T123">kal-lə-j</ta>
            <ta e="T125" id="Seg_5747" s="T124">toskanak</ta>
            <ta e="T126" id="Seg_5748" s="T125">kal-lə-j</ta>
            <ta e="T127" id="Seg_5749" s="T126">ugaːndə</ta>
            <ta e="T128" id="Seg_5750" s="T127">tutše</ta>
            <ta e="T129" id="Seg_5751" s="T128">sagər</ta>
            <ta e="T130" id="Seg_5752" s="T129">šonə-ga</ta>
            <ta e="T131" id="Seg_5753" s="T130">măn</ta>
            <ta e="T132" id="Seg_5754" s="T131">nʼiʔdə</ta>
            <ta e="T133" id="Seg_5755" s="T132">kam-bia-m</ta>
            <ta e="T134" id="Seg_5756" s="T133">kukuška</ta>
            <ta e="T135" id="Seg_5757" s="T134">bar</ta>
            <ta e="T136" id="Seg_5758" s="T135">nʼergö-laʔbə</ta>
            <ta e="T137" id="Seg_5759" s="T136">măndo-la-m</ta>
            <ta e="T138" id="Seg_5760" s="T137">dĭ</ta>
            <ta e="T139" id="Seg_5761" s="T138">tura-nə</ta>
            <ta e="T140" id="Seg_5762" s="T139">amnə-bi</ta>
            <ta e="T141" id="Seg_5763" s="T140">bar</ta>
            <ta e="T142" id="Seg_5764" s="T141">i</ta>
            <ta e="T143" id="Seg_5765" s="T142">kirgar-laʔbə</ta>
            <ta e="T144" id="Seg_5766" s="T143">măn</ta>
            <ta e="T145" id="Seg_5767" s="T144">măn-də-m</ta>
            <ta e="T146" id="Seg_5768" s="T145">unnʼa</ta>
            <ta e="T147" id="Seg_5769" s="T146">amno-laʔbə</ta>
            <ta e="T148" id="Seg_5770" s="T147">amno-la-m</ta>
            <ta e="T149" id="Seg_5771" s="T148">bar</ta>
            <ta e="T150" id="Seg_5772" s="T149">verno</ta>
            <ta e="T151" id="Seg_5773" s="T150">unnʼa</ta>
            <ta e="T152" id="Seg_5774" s="T151">amno-la-m</ta>
            <ta e="T153" id="Seg_5775" s="T152">pi-zʼiʔ</ta>
            <ta e="T154" id="Seg_5776" s="T153">bar</ta>
            <ta e="T155" id="Seg_5777" s="T154">barəʔ-pi</ta>
            <ta e="T156" id="Seg_5778" s="T155">pa-zʼiʔ</ta>
            <ta e="T157" id="Seg_5779" s="T156">bar</ta>
            <ta e="T158" id="Seg_5780" s="T157">barəʔ-pi</ta>
            <ta e="T159" id="Seg_5781" s="T158">dĭ</ta>
            <ta e="T160" id="Seg_5782" s="T159">uge</ta>
            <ta e="T161" id="Seg_5783" s="T160">amno-laʔbə</ta>
            <ta e="T162" id="Seg_5784" s="T161">kirgar-laʔbə</ta>
            <ta e="T163" id="Seg_5785" s="T162">dĭgəttə</ta>
            <ta e="T164" id="Seg_5786" s="T163">nʼergö-lüʔ-pi</ta>
            <ta e="T165" id="Seg_5787" s="T164">gijen</ta>
            <ta e="T166" id="Seg_5788" s="T165">kuja</ta>
            <ta e="T167" id="Seg_5789" s="T166">uʔbd-laʔbə</ta>
            <ta e="T168" id="Seg_5790" s="T167">ugaːndə</ta>
            <ta e="T169" id="Seg_5791" s="T168">külüʔ-səbi</ta>
            <ta e="T170" id="Seg_5792" s="T169">il</ta>
            <ta e="T171" id="Seg_5793" s="T170">bar</ta>
            <ta e="T172" id="Seg_5794" s="T171">pʼaŋdə-bi-ʔi</ta>
            <ta e="T173" id="Seg_5795" s="T172">sazən-də</ta>
            <ta e="T174" id="Seg_5796" s="T173">dĭ</ta>
            <ta e="T175" id="Seg_5797" s="T174">kuza</ta>
            <ta e="T176" id="Seg_5798" s="T175">ugaːndə</ta>
            <ta e="T177" id="Seg_5799" s="T176">ej</ta>
            <ta e="T178" id="Seg_5800" s="T177">jakšə</ta>
            <ta e="T179" id="Seg_5801" s="T178">bar</ta>
            <ta e="T180" id="Seg_5802" s="T179">ĭmbi</ta>
            <ta e="T181" id="Seg_5803" s="T180">i-leʔbə</ta>
            <ta e="T183" id="Seg_5804" s="T181">da</ta>
            <ta e="T184" id="Seg_5805" s="T183">bar</ta>
            <ta e="T185" id="Seg_5806" s="T184">ĭmbi</ta>
            <ta e="T186" id="Seg_5807" s="T185">i-leʔbə</ta>
            <ta e="T187" id="Seg_5808" s="T186">bar</ta>
            <ta e="T188" id="Seg_5809" s="T187">ugaːndə</ta>
            <ta e="T189" id="Seg_5810" s="T188">toli</ta>
            <ta e="T190" id="Seg_5811" s="T189">kuza</ta>
            <ta e="T191" id="Seg_5812" s="T190">bar</ta>
            <ta e="T192" id="Seg_5813" s="T191">ĭmbi</ta>
            <ta e="T193" id="Seg_5814" s="T192">i-lie</ta>
            <ta e="T195" id="Seg_5815" s="T193">dăk</ta>
            <ta e="T196" id="Seg_5816" s="T195">bar</ta>
            <ta e="T197" id="Seg_5817" s="T196">i-leʔbə</ta>
            <ta e="T198" id="Seg_5818" s="T197">bar</ta>
            <ta e="T199" id="Seg_5819" s="T198">kun-d-laʔbə</ta>
            <ta e="T200" id="Seg_5820" s="T199">maʔ-ndə</ta>
            <ta e="T201" id="Seg_5821" s="T200">măn</ta>
            <ta e="T202" id="Seg_5822" s="T201">teinen</ta>
            <ta e="T203" id="Seg_5823" s="T202">koʔbdo-nə</ta>
            <ta e="T204" id="Seg_5824" s="T203">plat-ə-m</ta>
            <ta e="T205" id="Seg_5825" s="T204">i-bie-m</ta>
            <ta e="T206" id="Seg_5826" s="T205">dĭ</ta>
            <ta e="T207" id="Seg_5827" s="T206">kam-bi</ta>
            <ta e="T208" id="Seg_5828" s="T207">Malinovka-nə</ta>
            <ta e="T209" id="Seg_5829" s="T208">tüšəl-zittə</ta>
            <ta e="T210" id="Seg_5830" s="T209">dĭn</ta>
            <ta e="T211" id="Seg_5831" s="T210">nüke</ta>
            <ta e="T212" id="Seg_5832" s="T211">surar-laʔbə</ta>
            <ta e="T213" id="Seg_5833" s="T212">gijen</ta>
            <ta e="T214" id="Seg_5834" s="T213">tăn</ta>
            <ta e="T215" id="Seg_5835" s="T214">plat</ta>
            <ta e="T216" id="Seg_5836" s="T215">i-bie-l</ta>
            <ta e="T217" id="Seg_5837" s="T216">urgaja</ta>
            <ta e="T218" id="Seg_5838" s="T217">mĭ-bi</ta>
            <ta e="T219" id="Seg_5839" s="T218">a</ta>
            <ta e="T220" id="Seg_5840" s="T219">urgaja</ta>
            <ta e="T221" id="Seg_5841" s="T220">gijen</ta>
            <ta e="T222" id="Seg_5842" s="T221">i-bi</ta>
            <ta e="T223" id="Seg_5843" s="T222">Polʼa</ta>
            <ta e="T224" id="Seg_5844" s="T223">mĭ-bi</ta>
            <ta e="T225" id="Seg_5845" s="T224">dö</ta>
            <ta e="T226" id="Seg_5846" s="T225">ine</ta>
            <ta e="T227" id="Seg_5847" s="T226">i-bie-m</ta>
            <ta e="T228" id="Seg_5848" s="T227">iʔgö</ta>
            <ta e="T229" id="Seg_5849" s="T228">aktʼa</ta>
            <ta e="T230" id="Seg_5850" s="T229">mĭ-bie-m</ta>
            <ta e="T231" id="Seg_5851" s="T230">a</ta>
            <ta e="T232" id="Seg_5852" s="T231">baška</ta>
            <ta e="T233" id="Seg_5853" s="T232">ine</ta>
            <ta e="T234" id="Seg_5854" s="T233">i-bie-m</ta>
            <ta e="T235" id="Seg_5855" s="T234">amga</ta>
            <ta e="T236" id="Seg_5856" s="T235">mĭ-bie-m</ta>
            <ta e="T238" id="Seg_5857" s="T237">dʼaʔ-pia-m</ta>
            <ta e="T241" id="Seg_5858" s="T240">buzo</ta>
            <ta e="T243" id="Seg_5859" s="T241">bar</ta>
            <ta e="T247" id="Seg_5860" s="T246">ej</ta>
            <ta e="T248" id="Seg_5861" s="T247">kandə-ga</ta>
            <ta e="T249" id="Seg_5862" s="T248">măn</ta>
            <ta e="T251" id="Seg_5863" s="T249">bar</ta>
            <ta e="T257" id="Seg_5864" s="T256">ugaːndə</ta>
            <ta e="T258" id="Seg_5865" s="T257">kuštu</ta>
            <ta e="T259" id="Seg_5866" s="T258">ej</ta>
            <ta e="T260" id="Seg_5867" s="T259">mo-lia-m</ta>
            <ta e="T261" id="Seg_5868" s="T260">kun-zittə</ta>
            <ta e="T262" id="Seg_5869" s="T261">bar</ta>
            <ta e="T267" id="Seg_5870" s="T266">kan-də-ga-m</ta>
            <ta e="T268" id="Seg_5871" s="T267">bar</ta>
            <ta e="T270" id="Seg_5872" s="T269">ej</ta>
            <ta e="T271" id="Seg_5873" s="T270">jakšə</ta>
            <ta e="T272" id="Seg_5874" s="T271">nʼi</ta>
            <ta e="T273" id="Seg_5875" s="T272">nʼi</ta>
            <ta e="T274" id="Seg_5876" s="T273">üge</ta>
            <ta e="T275" id="Seg_5877" s="T274">bar</ta>
            <ta e="T276" id="Seg_5878" s="T275">tajir-laʔbə</ta>
            <ta e="T277" id="Seg_5879" s="T276">da</ta>
            <ta e="T278" id="Seg_5880" s="T277">kun-d-laʔbə</ta>
            <ta e="T279" id="Seg_5881" s="T278">măn</ta>
            <ta e="T280" id="Seg_5882" s="T279">dʼije-gən</ta>
            <ta e="T281" id="Seg_5883" s="T280">mĭm-bie-m</ta>
            <ta e="T282" id="Seg_5884" s="T281">ĭmbi=də</ta>
            <ta e="T285" id="Seg_5885" s="T284">ku-bia-m</ta>
            <ta e="T286" id="Seg_5886" s="T285">dĭgəttə</ta>
            <ta e="T287" id="Seg_5887" s="T286">măn</ta>
            <ta e="T288" id="Seg_5888" s="T287">nere-r-lüʔ-pie-m</ta>
            <ta e="T289" id="Seg_5889" s="T288">bar</ta>
            <ta e="T290" id="Seg_5890" s="T289">dĭgəttə</ta>
            <ta e="T293" id="Seg_5891" s="T292">kudaj</ta>
            <ta e="T294" id="Seg_5892" s="T293">kăštə-bia-m</ta>
            <ta e="T295" id="Seg_5893" s="T294">bar</ta>
            <ta e="T296" id="Seg_5894" s="T295">kal-la</ta>
            <ta e="T297" id="Seg_5895" s="T296">dʼür-bi-ʔi</ta>
            <ta e="T298" id="Seg_5896" s="T297">nada</ta>
            <ta e="T299" id="Seg_5897" s="T298">ši-ʔi</ta>
            <ta e="T300" id="Seg_5898" s="T299">păjdə-sʼtə</ta>
            <ta e="T301" id="Seg_5899" s="T300">nada</ta>
            <ta e="T302" id="Seg_5900" s="T301">varota</ta>
            <ta e="T303" id="Seg_5901" s="T302">a-zittə</ta>
            <ta e="T305" id="Seg_5902" s="T304">dĭ</ta>
            <ta e="T310" id="Seg_5903" s="T309">dĭ</ta>
            <ta e="T311" id="Seg_5904" s="T310">ši-ʔ-nə</ta>
            <ta e="T312" id="Seg_5905" s="T311">nada</ta>
            <ta e="T313" id="Seg_5906" s="T312">pa-ʔi</ta>
            <ta e="T314" id="Seg_5907" s="T313">păʔ-sittə</ta>
            <ta e="T315" id="Seg_5908" s="T314">dĭgəttə</ta>
            <ta e="T316" id="Seg_5909" s="T315">šeden</ta>
            <ta e="T317" id="Seg_5910" s="T316">kaj-lə-bəj</ta>
            <ta e="T318" id="Seg_5911" s="T317">dĭgəttə</ta>
            <ta e="T319" id="Seg_5912" s="T318">buzo</ta>
            <ta e="T320" id="Seg_5913" s="T319">ej</ta>
            <ta e="T321" id="Seg_5914" s="T320">nuʔmə-lə-j</ta>
            <ta e="T322" id="Seg_5915" s="T321">šeden-gən</ta>
            <ta e="T325" id="Seg_5916" s="T324">šeden-gən</ta>
            <ta e="T326" id="Seg_5917" s="T325">nu-laʔbə</ta>
            <ta e="T338" id="Seg_5918" s="T337">ĭmbi-zʼi=də</ta>
            <ta e="T339" id="Seg_5919" s="T338">bar</ta>
            <ta e="T340" id="Seg_5920" s="T339">putʼəm-nia</ta>
            <ta e="T341" id="Seg_5921" s="T340">tăn</ta>
            <ta e="T342" id="Seg_5922" s="T341">püje-l</ta>
            <ta e="T344" id="Seg_5923" s="T342">ugaːndə</ta>
            <ta e="T347" id="Seg_5924" s="T346">tăn</ta>
            <ta e="T348" id="Seg_5925" s="T347">püje-l</ta>
            <ta e="T349" id="Seg_5926" s="T348">ugaːndə</ta>
            <ta e="T350" id="Seg_5927" s="T349">puʔmə</ta>
            <ta e="T351" id="Seg_5928" s="T350">bar</ta>
            <ta e="T352" id="Seg_5929" s="T351">nʼilgö-laʔbə</ta>
            <ta e="T353" id="Seg_5930" s="T352">tăn</ta>
            <ta e="T354" id="Seg_5931" s="T353">gijen</ta>
            <ta e="T355" id="Seg_5932" s="T354">i-bie-l</ta>
            <ta e="T356" id="Seg_5933" s="T355">bü-j-leʔ</ta>
            <ta e="T357" id="Seg_5934" s="T356">mĭm-bie-m</ta>
            <ta e="T358" id="Seg_5935" s="T357">nada</ta>
            <ta e="T359" id="Seg_5936" s="T358">segi</ta>
            <ta e="T360" id="Seg_5937" s="T359">bü</ta>
            <ta e="T361" id="Seg_5938" s="T360">mĭnzər-zittə</ta>
            <ta e="T362" id="Seg_5939" s="T361">nada</ta>
            <ta e="T363" id="Seg_5940" s="T362">mĭje</ta>
            <ta e="T366" id="Seg_5941" s="T365">mĭnzər-zittə</ta>
            <ta e="T367" id="Seg_5942" s="T366">tibi-zeŋ</ta>
            <ta e="T368" id="Seg_5943" s="T367">bădə-zʼittə</ta>
            <ta e="T369" id="Seg_5944" s="T368">măn</ta>
            <ta e="T370" id="Seg_5945" s="T369">bʼeʔ</ta>
            <ta e="T371" id="Seg_5946" s="T370">aktʼa</ta>
            <ta e="T372" id="Seg_5947" s="T371">i-ge</ta>
            <ta e="T373" id="Seg_5948" s="T372">nada</ta>
            <ta e="T374" id="Seg_5949" s="T373">tănan</ta>
            <ta e="T375" id="Seg_5950" s="T374">sumna</ta>
            <ta e="T376" id="Seg_5951" s="T375">mĭ-zittə</ta>
            <ta e="T377" id="Seg_5952" s="T376">sumna</ta>
            <ta e="T378" id="Seg_5953" s="T377">bos-kəndə</ta>
            <ta e="T379" id="Seg_5954" s="T378">ma-zittə</ta>
            <ta e="T380" id="Seg_5955" s="T379">možet</ta>
            <ta e="T381" id="Seg_5956" s="T380">giber=nʼibudʼ</ta>
            <ta e="T382" id="Seg_5957" s="T381">kal-la-l</ta>
            <ta e="T383" id="Seg_5958" s="T382">aktʼa</ta>
            <ta e="T384" id="Seg_5959" s="T383">kereʔ</ta>
            <ta e="T385" id="Seg_5960" s="T384">mo-lə-j</ta>
            <ta e="T387" id="Seg_5961" s="T386">Matvejev</ta>
            <ta e="T388" id="Seg_5962" s="T387">bar</ta>
            <ta e="T389" id="Seg_5963" s="T388">amno-laʔbə</ta>
            <ta e="T390" id="Seg_5964" s="T389">ej</ta>
            <ta e="T391" id="Seg_5965" s="T390">dʼăbaktər-ia</ta>
            <ta e="T392" id="Seg_5966" s="T391">sĭj-də</ta>
            <ta e="T393" id="Seg_5967" s="T392">ĭzem-nie</ta>
            <ta e="T394" id="Seg_5968" s="T393">maʔ-na-l</ta>
            <ta e="T395" id="Seg_5969" s="T394">sazən</ta>
            <ta e="T396" id="Seg_5970" s="T395">naga</ta>
            <ta e="T397" id="Seg_5971" s="T396">ne-t</ta>
            <ta e="T398" id="Seg_5972" s="T397">bar</ta>
            <ta e="T399" id="Seg_5973" s="T398">kuro-luʔ-pi</ta>
            <ta e="T400" id="Seg_5974" s="T399">ej</ta>
            <ta e="T401" id="Seg_5975" s="T400">pʼaŋdə-lia</ta>
            <ta e="T404" id="Seg_5976" s="T403">sazən</ta>
            <ta e="T405" id="Seg_5977" s="T404">i-ʔ</ta>
            <ta e="T406" id="Seg_5978" s="T405">kuro-ʔ</ta>
            <ta e="T410" id="Seg_5979" s="T409">büžü</ta>
            <ta e="T411" id="Seg_5980" s="T410">maʔ-nə-n</ta>
            <ta e="T412" id="Seg_5981" s="T411">šo-la-m</ta>
            <ta e="T413" id="Seg_5982" s="T412">ĭmbi=nʼibudʼ</ta>
            <ta e="T414" id="Seg_5983" s="T413">tănan</ta>
            <ta e="T415" id="Seg_5984" s="T414">det-le-m</ta>
            <ta e="T416" id="Seg_5985" s="T415">tăŋ</ta>
            <ta e="T417" id="Seg_5986" s="T416">ej</ta>
            <ta e="T418" id="Seg_5987" s="T417">dʼăbaktər-a-ʔ</ta>
            <ta e="T419" id="Seg_5988" s="T418">koldʼəŋ</ta>
            <ta e="T420" id="Seg_5989" s="T419">dʼăbaktər-a-ʔ</ta>
            <ta e="T421" id="Seg_5990" s="T420">ato</ta>
            <ta e="T422" id="Seg_5991" s="T421">bar</ta>
            <ta e="T423" id="Seg_5992" s="T422">il</ta>
            <ta e="T424" id="Seg_5993" s="T423">nün-ie-ʔi</ta>
            <ta e="T425" id="Seg_5994" s="T424">tăŋ</ta>
            <ta e="T426" id="Seg_5995" s="T425">i-ʔ</ta>
            <ta e="T427" id="Seg_5996" s="T426">kirgar-a-ʔ</ta>
            <ta e="T428" id="Seg_5997" s="T427">a</ta>
            <ta e="T429" id="Seg_5998" s="T428">i-ʔ</ta>
            <ta e="T430" id="Seg_5999" s="T429">tăn</ta>
            <ta e="T431" id="Seg_6000" s="T430">i-ʔ</ta>
            <ta e="T432" id="Seg_6001" s="T431">kudo-nza-ʔ</ta>
            <ta e="T433" id="Seg_6002" s="T432">ato</ta>
            <ta e="T434" id="Seg_6003" s="T433">il</ta>
            <ta e="T435" id="Seg_6004" s="T434">bar</ta>
            <ta e="T436" id="Seg_6005" s="T435">nün-leʔbə-ʔjə</ta>
            <ta e="T437" id="Seg_6006" s="T436">kăda</ta>
            <ta e="T438" id="Seg_6007" s="T437">tăn</ta>
            <ta e="T439" id="Seg_6008" s="T438">tăn</ta>
            <ta e="T440" id="Seg_6009" s="T439">kudo-nz-laʔbə-l</ta>
            <ta e="T441" id="Seg_6010" s="T440">măn</ta>
            <ta e="T442" id="Seg_6011" s="T441">teinen</ta>
            <ta e="T443" id="Seg_6012" s="T442">oroma</ta>
            <ta e="T444" id="Seg_6013" s="T443">kămna-bia-m</ta>
            <ta e="T445" id="Seg_6014" s="T444">bĭlgar-bia-m</ta>
            <ta e="T446" id="Seg_6015" s="T445">krinka-ʔi</ta>
            <ta e="T447" id="Seg_6016" s="T446">bəzəj-də-bia-m</ta>
            <ta e="T448" id="Seg_6017" s="T447">edə-bie-m</ta>
            <ta e="T449" id="Seg_6018" s="T448">kuja</ta>
            <ta e="T450" id="Seg_6019" s="T449">koʔ-laʔbə</ta>
            <ta e="T451" id="Seg_6020" s="T450">bar</ta>
            <ta e="T452" id="Seg_6021" s="T451">ĭmbi</ta>
            <ta e="T453" id="Seg_6022" s="T452">deʔ-pie-l</ta>
            <ta e="T454" id="Seg_6023" s="T453">pʼer-də</ta>
            <ta e="T455" id="Seg_6024" s="T454">măna</ta>
            <ta e="T458" id="Seg_6025" s="T457">ĭmbi</ta>
            <ta e="T459" id="Seg_6026" s="T458">dĭn</ta>
            <ta e="T460" id="Seg_6027" s="T459">iʔbö-laʔbə</ta>
            <ta e="T461" id="Seg_6028" s="T460">măn</ta>
            <ta e="T462" id="Seg_6029" s="T461">teinen</ta>
            <ta e="T463" id="Seg_6030" s="T462">kalba-j-laʔ</ta>
            <ta e="T464" id="Seg_6031" s="T463">mĭm-bie-m</ta>
            <ta e="T465" id="Seg_6032" s="T464">kalba</ta>
            <ta e="T466" id="Seg_6033" s="T465">deʔ-pie-m</ta>
            <ta e="T467" id="Seg_6034" s="T466">dĭgəttə</ta>
            <ta e="T468" id="Seg_6035" s="T467">dʼagar-la-m</ta>
            <ta e="T469" id="Seg_6036" s="T468">munuj-ʔ</ta>
            <ta e="T471" id="Seg_6037" s="T470">el-le-m</ta>
            <ta e="T472" id="Seg_6038" s="T471">oroma</ta>
            <ta e="T473" id="Seg_6039" s="T472">el-le-m</ta>
            <ta e="T474" id="Seg_6040" s="T473">dĭgəttə</ta>
            <ta e="T475" id="Seg_6041" s="T474">pür-zittə</ta>
            <ta e="T476" id="Seg_6042" s="T475">pirog-əʔi</ta>
            <ta e="T477" id="Seg_6043" s="T476">mo-la-m</ta>
            <ta e="T479" id="Seg_6044" s="T478">uja</ta>
            <ta e="T480" id="Seg_6045" s="T479">mĭnzer-bie-m</ta>
            <ta e="T481" id="Seg_6046" s="T480">uja</ta>
            <ta e="T482" id="Seg_6047" s="T481">am-bi-ʔi</ta>
            <ta e="T483" id="Seg_6048" s="T482">tĭmei-zittə</ta>
            <ta e="T484" id="Seg_6049" s="T483">onʼiʔ</ta>
            <ta e="T485" id="Seg_6050" s="T484">le</ta>
            <ta e="T486" id="Seg_6051" s="T485">ma-lup-pi</ta>
            <ta e="T490" id="Seg_6052" s="T489">men-zeŋ</ta>
            <ta e="T491" id="Seg_6053" s="T490">bar</ta>
            <ta e="T492" id="Seg_6054" s="T491">le</ta>
            <ta e="T493" id="Seg_6055" s="T492">am-naʔbə-ʔjə</ta>
            <ta e="T494" id="Seg_6056" s="T493">ugaːndə</ta>
            <ta e="T495" id="Seg_6057" s="T494">iʔgö</ta>
            <ta e="T496" id="Seg_6058" s="T495">le</ta>
            <ta e="T497" id="Seg_6059" s="T496">barəʔ-pi-baʔ</ta>
            <ta e="T498" id="Seg_6060" s="T497">măn</ta>
            <ta e="T501" id="Seg_6061" s="T500">măn</ta>
            <ta e="T502" id="Seg_6062" s="T501">teinen</ta>
            <ta e="T503" id="Seg_6063" s="T502">ipek</ta>
            <ta e="T504" id="Seg_6064" s="T503">pür-bie-m</ta>
            <ta e="T505" id="Seg_6065" s="T504">ej</ta>
            <ta e="T506" id="Seg_6066" s="T505">namzəga</ta>
            <ta e="T507" id="Seg_6067" s="T506">ej</ta>
            <ta e="T508" id="Seg_6068" s="T507">jakšə</ta>
            <ta e="T509" id="Seg_6069" s="T508">bar</ta>
            <ta e="T510" id="Seg_6070" s="T509">măn</ta>
            <ta e="T511" id="Seg_6071" s="T510">dĭ</ta>
            <ta e="T512" id="Seg_6072" s="T511">ipek</ta>
            <ta e="T513" id="Seg_6073" s="T512">dĭ</ta>
            <ta e="T516" id="Seg_6074" s="T515">men-zeŋ-də</ta>
            <ta e="T517" id="Seg_6075" s="T516">mĭ-bie-m</ta>
            <ta e="T518" id="Seg_6076" s="T517">men-zeŋ</ta>
            <ta e="T519" id="Seg_6077" s="T518">bar</ta>
            <ta e="T520" id="Seg_6078" s="T519">ipek</ta>
            <ta e="T521" id="Seg_6079" s="T520">am-naʔbə-ʔjə</ta>
            <ta e="T522" id="Seg_6080" s="T521">i</ta>
            <ta e="T523" id="Seg_6081" s="T522">bar</ta>
            <ta e="T524" id="Seg_6082" s="T523">dʼabəro-laʔbə-ʔjə</ta>
            <ta e="T525" id="Seg_6083" s="T524">men</ta>
            <ta e="T526" id="Seg_6084" s="T525">bar</ta>
            <ta e="T527" id="Seg_6085" s="T526">dĭ</ta>
            <ta e="T528" id="Seg_6086" s="T527">men-də</ta>
            <ta e="T529" id="Seg_6087" s="T528">timne-t</ta>
            <ta e="T530" id="Seg_6088" s="T529">bar</ta>
            <ta e="T531" id="Seg_6089" s="T530">nudʼi</ta>
            <ta e="T532" id="Seg_6090" s="T531">i-bi</ta>
            <ta e="T533" id="Seg_6091" s="T532">nudʼi</ta>
            <ta e="T534" id="Seg_6092" s="T533">i-bi</ta>
            <ta e="T536" id="Seg_6093" s="T534">ugaːndə</ta>
            <ta e="T540" id="Seg_6094" s="T539">ĭmbi=də</ta>
            <ta e="T541" id="Seg_6095" s="T540">ej</ta>
            <ta e="T542" id="Seg_6096" s="T541">ed-lia</ta>
            <ta e="T543" id="Seg_6097" s="T542">măn</ta>
            <ta e="T544" id="Seg_6098" s="T543">dĭ-m</ta>
            <ta e="T545" id="Seg_6099" s="T544">ej</ta>
            <ta e="T546" id="Seg_6100" s="T545">tĭmne-bie-m</ta>
            <ta e="T547" id="Seg_6101" s="T546">šindi</ta>
            <ta e="T548" id="Seg_6102" s="T547">šonə-ga</ta>
            <ta e="T549" id="Seg_6103" s="T548">măn</ta>
            <ta e="T550" id="Seg_6104" s="T549">aba-m</ta>
            <ta e="T551" id="Seg_6105" s="T550">tujez-iʔi</ta>
            <ta e="T552" id="Seg_6106" s="T551">a-bi</ta>
            <ta e="T553" id="Seg_6107" s="T552">kuba</ta>
            <ta e="T554" id="Seg_6108" s="T553">kuba</ta>
            <ta e="T555" id="Seg_6109" s="T554">kür</ta>
            <ta e="T556" id="Seg_6110" s="T555">kür-bi</ta>
            <ta e="T557" id="Seg_6111" s="T556">i</ta>
            <ta e="T558" id="Seg_6112" s="T557">a-laʔbə-bi</ta>
            <ta e="T559" id="Seg_6113" s="T558">šojdʼo</ta>
            <ta e="T560" id="Seg_6114" s="T559">šöʔ-pi-ʔi</ta>
            <ta e="T561" id="Seg_6115" s="T560">žila-ʔi-zʼiʔ</ta>
            <ta e="T562" id="Seg_6116" s="T561">bar</ta>
            <ta e="T563" id="Seg_6117" s="T562">nʼimi-zʼiʔ</ta>
            <ta e="T564" id="Seg_6118" s="T563">i</ta>
            <ta e="T565" id="Seg_6119" s="T564">jendak</ta>
            <ta e="T566" id="Seg_6120" s="T565">müjö-gən</ta>
            <ta e="T567" id="Seg_6121" s="T566">i-bi</ta>
            <ta e="T569" id="Seg_6122" s="T567">šojdʼo-ʔi</ta>
            <ta e="T570" id="Seg_6123" s="T569">šöʔ-pi-ʔi</ta>
            <ta e="T571" id="Seg_6124" s="T570">bar</ta>
            <ta e="T572" id="Seg_6125" s="T571">žila-ʔi-zʼiʔ</ta>
            <ta e="T573" id="Seg_6126" s="T572">i</ta>
            <ta e="T576" id="Seg_6127" s="T575">jendak</ta>
            <ta e="T577" id="Seg_6128" s="T576">šer-bi</ta>
            <ta e="T578" id="Seg_6129" s="T577">müjö-nə</ta>
            <ta e="T579" id="Seg_6130" s="T578">i</ta>
            <ta e="T580" id="Seg_6131" s="T579">nʼimi</ta>
            <ta e="T581" id="Seg_6132" s="T580">i-bi</ta>
            <ta e="T582" id="Seg_6133" s="T581">šödör-bi</ta>
            <ta e="T593" id="Seg_6134" s="T592">müjö-bə</ta>
            <ta e="T594" id="Seg_6135" s="T593">bar</ta>
            <ta e="T595" id="Seg_6136" s="T594">muʔ-lə-j</ta>
            <ta e="T596" id="Seg_6137" s="T595">tăn</ta>
            <ta e="T597" id="Seg_6138" s="T596">bar</ta>
            <ta e="T598" id="Seg_6139" s="T597">ne-l</ta>
            <ta e="T599" id="Seg_6140" s="T598">ajir-laʔbə-l</ta>
            <ta e="T600" id="Seg_6141" s="T599">sĭj-də</ta>
            <ta e="T601" id="Seg_6142" s="T600">ĭzem-nie</ta>
            <ta e="T602" id="Seg_6143" s="T601">ugaːndə</ta>
            <ta e="T603" id="Seg_6144" s="T602">măn</ta>
            <ta e="T604" id="Seg_6145" s="T603">bar</ta>
            <ta e="T605" id="Seg_6146" s="T604">tura-m</ta>
            <ta e="T606" id="Seg_6147" s="T605">sagər</ta>
            <ta e="T609" id="Seg_6148" s="T608">karəldʼan</ta>
            <ta e="T610" id="Seg_6149" s="T609">baz-la-m</ta>
            <ta e="T611" id="Seg_6150" s="T610">ugaːndə</ta>
            <ta e="T612" id="Seg_6151" s="T611">balgaš</ta>
            <ta e="T613" id="Seg_6152" s="T612">ato</ta>
            <ta e="T614" id="Seg_6153" s="T613">il</ta>
            <ta e="T615" id="Seg_6154" s="T614">bar</ta>
            <ta e="T616" id="Seg_6155" s="T615">măl-leʔbə-ʔjə</ta>
            <ta e="T617" id="Seg_6156" s="T616">mă-lia-ʔi</ta>
            <ta e="T618" id="Seg_6157" s="T617">ɨrɨː</ta>
            <ta e="T619" id="Seg_6158" s="T618">nüke</ta>
            <ta e="T620" id="Seg_6159" s="T619">tura</ta>
            <ta e="T621" id="Seg_6160" s="T620">ej</ta>
            <ta e="T622" id="Seg_6161" s="T621">bazə-lia</ta>
            <ta e="T627" id="Seg_6162" s="T626">ĭmbi</ta>
            <ta e="T628" id="Seg_6163" s="T627">tănan</ta>
            <ta e="T629" id="Seg_6164" s="T628">dʼăbaktər-zittə</ta>
            <ta e="T630" id="Seg_6165" s="T629">măn</ta>
            <ta e="T631" id="Seg_6166" s="T630">bar</ta>
            <ta e="T632" id="Seg_6167" s="T631">nöməl-bio-m</ta>
            <ta e="T633" id="Seg_6168" s="T632">teinen</ta>
            <ta e="T634" id="Seg_6169" s="T633">kunol-zittə</ta>
            <ta e="T635" id="Seg_6170" s="T634">klop-əʔi</ta>
            <ta e="T636" id="Seg_6171" s="T635">ej</ta>
            <ta e="T637" id="Seg_6172" s="T636">mĭ-bi-ʔi</ta>
            <ta e="T638" id="Seg_6173" s="T637">dĭ</ta>
            <ta e="T639" id="Seg_6174" s="T638">kuza</ta>
            <ta e="T640" id="Seg_6175" s="T639">un</ta>
            <ta e="T641" id="Seg_6176" s="T640">pʼe-bi</ta>
            <ta e="T642" id="Seg_6177" s="T641">nada</ta>
            <ta e="T643" id="Seg_6178" s="T642">dĭʔ-nə</ta>
            <ta e="T644" id="Seg_6179" s="T643">mĭ-zittə</ta>
            <ta e="T645" id="Seg_6180" s="T644">dĭ-n</ta>
            <ta e="T646" id="Seg_6181" s="T645">ipek</ta>
            <ta e="T647" id="Seg_6182" s="T646">naga</ta>
            <ta e="T648" id="Seg_6183" s="T647">am-zittə</ta>
            <ta e="T649" id="Seg_6184" s="T648">bar</ta>
            <ta e="T656" id="Seg_6185" s="T655">gijen</ta>
            <ta e="T657" id="Seg_6186" s="T656">i-zittə</ta>
            <ta e="T658" id="Seg_6187" s="T657">ĭmbi</ta>
            <ta e="T659" id="Seg_6188" s="T658">dʼăbaktər-zʼittə</ta>
            <ta e="T660" id="Seg_6189" s="T659">dĭʔ-nə</ta>
            <ta e="T661" id="Seg_6190" s="T660">ugaːndə</ta>
            <ta e="T664" id="Seg_6191" s="T663">iʔgö</ta>
            <ta e="T665" id="Seg_6192" s="T664">nada</ta>
            <ta e="T666" id="Seg_6193" s="T665">dʼăbaktər-zittə</ta>
            <ta e="T670" id="Seg_6194" s="T668">nada</ta>
            <ta e="T671" id="Seg_6195" s="T670">kan-zittə</ta>
            <ta e="T672" id="Seg_6196" s="T671">măndə-r-zittə</ta>
            <ta e="T673" id="Seg_6197" s="T672">gijen=nʼibudʼ</ta>
            <ta e="T674" id="Seg_6198" s="T673">dʼăbaktər-zittə</ta>
            <ta e="T675" id="Seg_6199" s="T674">măn</ta>
            <ta e="T676" id="Seg_6200" s="T675">taldʼen</ta>
            <ta e="T677" id="Seg_6201" s="T676">kundʼo</ta>
            <ta e="T678" id="Seg_6202" s="T677">ej</ta>
            <ta e="T681" id="Seg_6203" s="T680">iʔbə-bie-m</ta>
            <ta e="T682" id="Seg_6204" s="T681">kunol-zittə</ta>
            <ta e="T685" id="Seg_6205" s="T684">kundʼo</ta>
            <ta e="T686" id="Seg_6206" s="T685">ej</ta>
            <ta e="T688" id="Seg_6207" s="T687">ej</ta>
            <ta e="T690" id="Seg_6208" s="T689">ej</ta>
            <ta e="T691" id="Seg_6209" s="T690">iʔbə-le-m</ta>
            <ta e="T692" id="Seg_6210" s="T691">kunol-zittə</ta>
            <ta e="T697" id="Seg_6211" s="T696">kunol-bia-m</ta>
            <ta e="T698" id="Seg_6212" s="T697">ĭmbi=də</ta>
            <ta e="T699" id="Seg_6213" s="T698">ej</ta>
            <ta e="T700" id="Seg_6214" s="T699">ku-bia-m</ta>
            <ta e="T701" id="Seg_6215" s="T700">dĭ</ta>
            <ta e="T702" id="Seg_6216" s="T701">kuza-gən</ta>
            <ta e="T703" id="Seg_6217" s="T702">eʔbdə-t</ta>
            <ta e="T704" id="Seg_6218" s="T703">bar</ta>
            <ta e="T705" id="Seg_6219" s="T704">sagər</ta>
            <ta e="T706" id="Seg_6220" s="T705">i-bi</ta>
            <ta e="T707" id="Seg_6221" s="T706">tüj</ta>
            <ta e="T708" id="Seg_6222" s="T707">bar</ta>
            <ta e="T709" id="Seg_6223" s="T708">ugaːndə</ta>
            <ta e="T710" id="Seg_6224" s="T709">sĭre</ta>
            <ta e="T711" id="Seg_6225" s="T710">mo-laːm-bi-ʔi</ta>
            <ta e="T712" id="Seg_6226" s="T711">eʔbdə</ta>
            <ta e="T713" id="Seg_6227" s="T712">onʼiʔ</ta>
            <ta e="T714" id="Seg_6228" s="T713">uda</ta>
            <ta e="T715" id="Seg_6229" s="T714">i-ge</ta>
            <ta e="T716" id="Seg_6230" s="T715">a</ta>
            <ta e="T717" id="Seg_6231" s="T716">onʼiʔ</ta>
            <ta e="T718" id="Seg_6232" s="T717">uda</ta>
            <ta e="T719" id="Seg_6233" s="T718">nĭŋgə-bi-ʔi</ta>
            <ta e="T720" id="Seg_6234" s="T719">onʼiʔ</ta>
            <ta e="T721" id="Seg_6235" s="T720">uda</ta>
            <ta e="T722" id="Seg_6236" s="T721">i-ge</ta>
            <ta e="T723" id="Seg_6237" s="T722">a</ta>
            <ta e="T724" id="Seg_6238" s="T723">onʼiʔ</ta>
            <ta e="T725" id="Seg_6239" s="T724">uda-bə</ta>
            <ta e="T726" id="Seg_6240" s="T725">bar</ta>
            <ta e="T727" id="Seg_6241" s="T726">sajnĭŋgə-bi-ʔi</ta>
            <ta e="T728" id="Seg_6242" s="T727">măn</ta>
            <ta e="T729" id="Seg_6243" s="T728">üzə-bie-m</ta>
            <ta e="T731" id="Seg_6244" s="T730">onʼiʔ</ta>
            <ta e="T732" id="Seg_6245" s="T731">uda-m</ta>
            <ta e="T733" id="Seg_6246" s="T732">băldə-bia-m</ta>
            <ta e="T735" id="Seg_6247" s="T734">măn</ta>
            <ta e="T736" id="Seg_6248" s="T735">saʔmə-luʔ-pia-m</ta>
            <ta e="T737" id="Seg_6249" s="T736">i</ta>
            <ta e="T738" id="Seg_6250" s="T737">uda-m</ta>
            <ta e="T739" id="Seg_6251" s="T738">băldə</ta>
            <ta e="T740" id="Seg_6252" s="T739">băldə-l-bia-m</ta>
            <ta e="T741" id="Seg_6253" s="T740">dĭgəttə</ta>
            <ta e="T742" id="Seg_6254" s="T741">dĭ</ta>
            <ta e="T743" id="Seg_6255" s="T742">uda-m</ta>
            <ta e="T744" id="Seg_6256" s="T743">em-bi-ʔi</ta>
            <ta e="T745" id="Seg_6257" s="T744">bar</ta>
            <ta e="T752" id="Seg_6258" s="T901">dĭgəttə</ta>
            <ta e="T753" id="Seg_6259" s="T752">uda-m</ta>
            <ta e="T918" id="Seg_6260" s="T753">bar</ta>
            <ta e="T755" id="Seg_6261" s="T918">sar-bi-ʔi</ta>
            <ta e="T759" id="Seg_6262" s="T758">paka</ta>
            <ta e="T760" id="Seg_6263" s="T759">ej</ta>
            <ta e="T761" id="Seg_6264" s="T760">özer-lə-j</ta>
            <ta e="T762" id="Seg_6265" s="T761">ĭmbi=də</ta>
            <ta e="T763" id="Seg_6266" s="T762">togonor-zittə</ta>
            <ta e="T764" id="Seg_6267" s="T763">nʼelʼzja</ta>
            <ta e="T765" id="Seg_6268" s="T764">ine</ta>
            <ta e="T766" id="Seg_6269" s="T765">körer-zittə</ta>
            <ta e="T767" id="Seg_6270" s="T766">nada</ta>
            <ta e="T772" id="Seg_6271" s="T771">ine</ta>
            <ta e="T774" id="Seg_6272" s="T773">nuʔmə-leʔpi</ta>
            <ta e="T775" id="Seg_6273" s="T774">bar</ta>
            <ta e="T782" id="Seg_6274" s="T781">ine</ta>
            <ta e="T783" id="Seg_6275" s="T782">nuʔmə-leʔpi</ta>
            <ta e="T784" id="Seg_6276" s="T783">saʔmə-luʔ-pi</ta>
            <ta e="T785" id="Seg_6277" s="T784">üjü-t</ta>
            <ta e="T786" id="Seg_6278" s="T785">băldə</ta>
            <ta e="T787" id="Seg_6279" s="T786">üjü-t</ta>
            <ta e="T788" id="Seg_6280" s="T787">băldə-bi</ta>
            <ta e="T789" id="Seg_6281" s="T788">măn</ta>
            <ta e="T790" id="Seg_6282" s="T789">uda-gən</ta>
            <ta e="T791" id="Seg_6283" s="T790">sumna</ta>
            <ta e="T792" id="Seg_6284" s="T791">müjö-zeŋ-bə</ta>
            <ta e="T793" id="Seg_6285" s="T792">a</ta>
            <ta e="T794" id="Seg_6286" s="T793">tăn</ta>
            <ta e="T795" id="Seg_6287" s="T794">uda-gən</ta>
            <ta e="T796" id="Seg_6288" s="T795">onʼiʔ</ta>
            <ta e="T797" id="Seg_6289" s="T796">naga</ta>
            <ta e="T798" id="Seg_6290" s="T797">baltu-zʼiʔ</ta>
            <ta e="T799" id="Seg_6291" s="T798">bar</ta>
            <ta e="T800" id="Seg_6292" s="T799">jaʔ-pia-l</ta>
            <ta e="T801" id="Seg_6293" s="T800">teinen</ta>
            <ta e="T802" id="Seg_6294" s="T801">es-seŋ</ta>
            <ta e="T803" id="Seg_6295" s="T802">iʔgö</ta>
            <ta e="T804" id="Seg_6296" s="T803">bar</ta>
            <ta e="T805" id="Seg_6297" s="T804">buzo</ta>
            <ta e="T806" id="Seg_6298" s="T805">sürer-leʔpi-ʔi</ta>
            <ta e="T807" id="Seg_6299" s="T806">dĭ</ta>
            <ta e="T808" id="Seg_6300" s="T807">bar</ta>
            <ta e="T809" id="Seg_6301" s="T808">par-luʔ-pi</ta>
            <ta e="T810" id="Seg_6302" s="T809">i</ta>
            <ta e="T811" id="Seg_6303" s="T810">nuʔmə-luʔ-pi</ta>
            <ta e="T812" id="Seg_6304" s="T811">maʔ-ndə</ta>
            <ta e="T813" id="Seg_6305" s="T812">nüdʼi-n</ta>
            <ta e="T814" id="Seg_6306" s="T813">kal-la</ta>
            <ta e="T815" id="Seg_6307" s="T814">dʼür-bi</ta>
            <ta e="T816" id="Seg_6308" s="T815">bar</ta>
            <ta e="T817" id="Seg_6309" s="T816">nüdʼi</ta>
            <ta e="T818" id="Seg_6310" s="T817">mo-laːndə-ga</ta>
            <ta e="T819" id="Seg_6311" s="T818">kuja</ta>
            <ta e="T820" id="Seg_6312" s="T819">bar</ta>
            <ta e="T821" id="Seg_6313" s="T820">amno-laʔbə</ta>
            <ta e="T822" id="Seg_6314" s="T821">nüdʼi</ta>
            <ta e="T823" id="Seg_6315" s="T822">mo-laːndə-ga</ta>
            <ta e="T824" id="Seg_6316" s="T823">kuja</ta>
            <ta e="T825" id="Seg_6317" s="T824">amnol-laʔbə</ta>
            <ta e="T826" id="Seg_6318" s="T825">kabarləj</ta>
            <ta e="T827" id="Seg_6319" s="T826">dʼăbaktər-zittə</ta>
            <ta e="T828" id="Seg_6320" s="T827">nada</ta>
            <ta e="T829" id="Seg_6321" s="T828">iʔbə-sʼtə</ta>
            <ta e="T830" id="Seg_6322" s="T829">kunol-zittə</ta>
            <ta e="T831" id="Seg_6323" s="T830">erte-n</ta>
            <ta e="T832" id="Seg_6324" s="T831">erte</ta>
            <ta e="T833" id="Seg_6325" s="T832">nada</ta>
            <ta e="T834" id="Seg_6326" s="T833">uʔ-sʼittə</ta>
            <ta e="T840" id="Seg_6327" s="T839">kal-la-m</ta>
            <ta e="T841" id="Seg_6328" s="T840">svetok-əʔi</ta>
            <ta e="T842" id="Seg_6329" s="T841">nĭŋgə-sʼtə</ta>
            <ta e="T843" id="Seg_6330" s="T842">sĭre</ta>
            <ta e="T844" id="Seg_6331" s="T843">pa-gən</ta>
            <ta e="T845" id="Seg_6332" s="T844">bar</ta>
            <ta e="T846" id="Seg_6333" s="T845">sazən</ta>
            <ta e="T847" id="Seg_6334" s="T846">ed-leʔbə</ta>
            <ta e="T848" id="Seg_6335" s="T847">ej</ta>
            <ta e="T849" id="Seg_6336" s="T848">ed-lia</ta>
            <ta e="T850" id="Seg_6337" s="T849">ĭmbi=də</ta>
            <ta e="T851" id="Seg_6338" s="T850">ej</ta>
            <ta e="T852" id="Seg_6339" s="T851">ku-lio-m</ta>
            <ta e="T853" id="Seg_6340" s="T852">măn</ta>
            <ta e="T854" id="Seg_6341" s="T853">tugan-bə</ta>
            <ta e="T856" id="Seg_6342" s="T854">ugaːndə</ta>
            <ta e="T858" id="Seg_6343" s="T857">kuŋgə</ta>
            <ta e="T859" id="Seg_6344" s="T858">amno-laʔbə</ta>
            <ta e="T860" id="Seg_6345" s="T859">nada</ta>
            <ta e="T861" id="Seg_6346" s="T860">šide</ta>
            <ta e="T862" id="Seg_6347" s="T861">dʼala</ta>
            <ta e="T863" id="Seg_6348" s="T862">kan-zittə</ta>
            <ta e="T864" id="Seg_6349" s="T863">a</ta>
            <ta e="T865" id="Seg_6350" s="T864">možet</ta>
            <ta e="T866" id="Seg_6351" s="T865">onʼiʔ</ta>
            <ta e="T867" id="Seg_6352" s="T866">dʼala-nə</ta>
            <ta e="T868" id="Seg_6353" s="T867">kal-la-l</ta>
            <ta e="T869" id="Seg_6354" s="T868">măn</ta>
            <ta e="T870" id="Seg_6355" s="T869">dĭʔ-nə</ta>
            <ta e="T871" id="Seg_6356" s="T870">šo-bia-m</ta>
            <ta e="T872" id="Seg_6357" s="T871">a</ta>
            <ta e="T873" id="Seg_6358" s="T872">dĭ</ta>
            <ta e="T874" id="Seg_6359" s="T873">naga</ta>
            <ta e="T875" id="Seg_6360" s="T874">gibər=də</ta>
            <ta e="T876" id="Seg_6361" s="T875">kal-la</ta>
            <ta e="T877" id="Seg_6362" s="T876">dʼür-bi</ta>
            <ta e="T878" id="Seg_6363" s="T877">măn</ta>
            <ta e="T879" id="Seg_6364" s="T878">bar</ta>
            <ta e="T917" id="Seg_6365" s="T879">par-luʔ-pi</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PKZ">
            <ta e="T5" id="Seg_6366" s="T4">măn</ta>
            <ta e="T6" id="Seg_6367" s="T5">ej</ta>
            <ta e="T7" id="Seg_6368" s="T6">mo-liA-m</ta>
            <ta e="T10" id="Seg_6369" s="T9">mĭŋgəl-zittə</ta>
            <ta e="T11" id="Seg_6370" s="T10">bar</ta>
            <ta e="T12" id="Seg_6371" s="T11">ĭzem-laʔbə</ta>
            <ta e="T13" id="Seg_6372" s="T12">adnaka</ta>
            <ta e="T14" id="Seg_6373" s="T13">măn</ta>
            <ta e="T15" id="Seg_6374" s="T14">kü-laːm-bi-m</ta>
            <ta e="T16" id="Seg_6375" s="T15">teinen</ta>
            <ta e="T17" id="Seg_6376" s="T16">dĭ</ta>
            <ta e="T18" id="Seg_6377" s="T17">koʔbdo</ta>
            <ta e="T19" id="Seg_6378" s="T18">urgo</ta>
            <ta e="T20" id="Seg_6379" s="T19">tʼala</ta>
            <ta e="T21" id="Seg_6380" s="T20">dĭ-m</ta>
            <ta e="T22" id="Seg_6381" s="T21">teinen</ta>
            <ta e="T27" id="Seg_6382" s="T26">ija-də</ta>
            <ta e="T28" id="Seg_6383" s="T27">det-bi</ta>
            <ta e="T29" id="Seg_6384" s="T28">dĭ-Tə</ta>
            <ta e="T33" id="Seg_6385" s="T32">biəʔ</ta>
            <ta e="T34" id="Seg_6386" s="T33">šide</ta>
            <ta e="T35" id="Seg_6387" s="T34">pʼe</ta>
            <ta e="T36" id="Seg_6388" s="T35">i</ta>
            <ta e="T37" id="Seg_6389" s="T36">ĭššo</ta>
            <ta e="T38" id="Seg_6390" s="T37">šide</ta>
            <ta e="T40" id="Seg_6391" s="T39">kădaʔ</ta>
            <ta e="T41" id="Seg_6392" s="T40">dĭ</ta>
            <ta e="T42" id="Seg_6393" s="T41">kuza</ta>
            <ta e="T43" id="Seg_6394" s="T42">marʼi</ta>
            <ta e="T44" id="Seg_6395" s="T43">ĭmbi=də</ta>
            <ta e="T45" id="Seg_6396" s="T44">ej</ta>
            <ta e="T46" id="Seg_6397" s="T45">i-lV</ta>
            <ta e="T47" id="Seg_6398" s="T46">ĭmbi=də</ta>
            <ta e="T48" id="Seg_6399" s="T47">ej</ta>
            <ta e="T49" id="Seg_6400" s="T48">mĭ-lV-j</ta>
            <ta e="T50" id="Seg_6401" s="T49">a</ta>
            <ta e="T51" id="Seg_6402" s="T50">baška</ta>
            <ta e="T52" id="Seg_6403" s="T51">kuza</ta>
            <ta e="T53" id="Seg_6404" s="T52">ugaːndə</ta>
            <ta e="T54" id="Seg_6405" s="T53">jakšə</ta>
            <ta e="T55" id="Seg_6406" s="T54">bar</ta>
            <ta e="T56" id="Seg_6407" s="T55">mĭ-luʔbdə-lV-j</ta>
            <ta e="T59" id="Seg_6408" s="T58">ĭmbi</ta>
            <ta e="T62" id="Seg_6409" s="T61">ĭmbi</ta>
            <ta e="T63" id="Seg_6410" s="T62">ej</ta>
            <ta e="T69" id="Seg_6411" s="T67">ĭmbi</ta>
            <ta e="T70" id="Seg_6412" s="T69">ĭmbi</ta>
            <ta e="T71" id="Seg_6413" s="T70">ej</ta>
            <ta e="T72" id="Seg_6414" s="T71">pi-lV-l</ta>
            <ta e="T73" id="Seg_6415" s="T72">bar</ta>
            <ta e="T74" id="Seg_6416" s="T73">mĭ-lV-j</ta>
            <ta e="T75" id="Seg_6417" s="T74">dĭ-n</ta>
            <ta e="T76" id="Seg_6418" s="T75">ugaːndə</ta>
            <ta e="T77" id="Seg_6419" s="T76">sĭj-də</ta>
            <ta e="T78" id="Seg_6420" s="T77">jakšə</ta>
            <ta e="T84" id="Seg_6421" s="T83">ara</ta>
            <ta e="T85" id="Seg_6422" s="T84">ej</ta>
            <ta e="T86" id="Seg_6423" s="T85">bĭs-liA</ta>
            <ta e="T87" id="Seg_6424" s="T86">ĭmbi</ta>
            <ta e="T88" id="Seg_6425" s="T87">i-gA</ta>
            <ta e="T89" id="Seg_6426" s="T88">bar</ta>
            <ta e="T90" id="Seg_6427" s="T89">mĭ-lV-j</ta>
            <ta e="T91" id="Seg_6428" s="T90">munəj-jəʔ</ta>
            <ta e="T92" id="Seg_6429" s="T91">mĭ-liA</ta>
            <ta e="T95" id="Seg_6430" s="T94">kajaʔ</ta>
            <ta e="T96" id="Seg_6431" s="T95">mĭ-liA</ta>
            <ta e="T97" id="Seg_6432" s="T96">ipek</ta>
            <ta e="T98" id="Seg_6433" s="T97">mĭ-liA</ta>
            <ta e="T99" id="Seg_6434" s="T98">uja</ta>
            <ta e="T100" id="Seg_6435" s="T99">i-gA</ta>
            <ta e="T101" id="Seg_6436" s="T100">tak</ta>
            <ta e="T102" id="Seg_6437" s="T101">mĭ-lV-j</ta>
            <ta e="T103" id="Seg_6438" s="T102">bar</ta>
            <ta e="T104" id="Seg_6439" s="T103">ĭmbi</ta>
            <ta e="T105" id="Seg_6440" s="T104">i-gA</ta>
            <ta e="T106" id="Seg_6441" s="T105">bar</ta>
            <ta e="T107" id="Seg_6442" s="T106">mĭ-lV-j</ta>
            <ta e="T108" id="Seg_6443" s="T107">ugaːndə</ta>
            <ta e="T111" id="Seg_6444" s="T110">nʼuʔnun</ta>
            <ta e="T112" id="Seg_6445" s="T111">kuzur-liA</ta>
            <ta e="T113" id="Seg_6446" s="T112">kudaj</ta>
            <ta e="T114" id="Seg_6447" s="T113">kuzur-laʔbə</ta>
            <ta e="T116" id="Seg_6448" s="T115">šində=nʼibudʼ</ta>
            <ta e="T117" id="Seg_6449" s="T116">kut-lV-j</ta>
            <ta e="T118" id="Seg_6450" s="T117">raduga</ta>
            <ta e="T119" id="Seg_6451" s="T118">nu-laʔbə</ta>
            <ta e="T120" id="Seg_6452" s="T119">bü</ta>
            <ta e="T121" id="Seg_6453" s="T120">i-laʔbə</ta>
            <ta e="T122" id="Seg_6454" s="T121">dĭgəttə</ta>
            <ta e="T123" id="Seg_6455" s="T122">surno</ta>
            <ta e="T124" id="Seg_6456" s="T123">kan-lV-j</ta>
            <ta e="T125" id="Seg_6457" s="T124">toskanak</ta>
            <ta e="T126" id="Seg_6458" s="T125">kan-lV-j</ta>
            <ta e="T127" id="Seg_6459" s="T126">ugaːndə</ta>
            <ta e="T128" id="Seg_6460" s="T127">tutše</ta>
            <ta e="T129" id="Seg_6461" s="T128">sagər</ta>
            <ta e="T130" id="Seg_6462" s="T129">šonə-gA</ta>
            <ta e="T131" id="Seg_6463" s="T130">măn</ta>
            <ta e="T132" id="Seg_6464" s="T131">nʼiʔdə</ta>
            <ta e="T133" id="Seg_6465" s="T132">kan-bi-m</ta>
            <ta e="T134" id="Seg_6466" s="T133">kukuška</ta>
            <ta e="T135" id="Seg_6467" s="T134">bar</ta>
            <ta e="T136" id="Seg_6468" s="T135">nʼergö-laʔbə</ta>
            <ta e="T137" id="Seg_6469" s="T136">măndo-lV-m</ta>
            <ta e="T138" id="Seg_6470" s="T137">dĭ</ta>
            <ta e="T139" id="Seg_6471" s="T138">tura-Tə</ta>
            <ta e="T140" id="Seg_6472" s="T139">amnə-bi</ta>
            <ta e="T141" id="Seg_6473" s="T140">bar</ta>
            <ta e="T142" id="Seg_6474" s="T141">i</ta>
            <ta e="T143" id="Seg_6475" s="T142">kirgaːr-laʔbə</ta>
            <ta e="T144" id="Seg_6476" s="T143">măn</ta>
            <ta e="T145" id="Seg_6477" s="T144">măn-ntə-m</ta>
            <ta e="T146" id="Seg_6478" s="T145">unʼə</ta>
            <ta e="T147" id="Seg_6479" s="T146">amno-laʔbə</ta>
            <ta e="T148" id="Seg_6480" s="T147">amno-lV-m</ta>
            <ta e="T149" id="Seg_6481" s="T148">bar</ta>
            <ta e="T150" id="Seg_6482" s="T149">verna</ta>
            <ta e="T151" id="Seg_6483" s="T150">unʼə</ta>
            <ta e="T152" id="Seg_6484" s="T151">amno-lV-m</ta>
            <ta e="T153" id="Seg_6485" s="T152">pi-ziʔ</ta>
            <ta e="T154" id="Seg_6486" s="T153">bar</ta>
            <ta e="T155" id="Seg_6487" s="T154">barəʔ-bi</ta>
            <ta e="T156" id="Seg_6488" s="T155">pa-ziʔ</ta>
            <ta e="T157" id="Seg_6489" s="T156">bar</ta>
            <ta e="T158" id="Seg_6490" s="T157">barəʔ-bi</ta>
            <ta e="T159" id="Seg_6491" s="T158">dĭ</ta>
            <ta e="T160" id="Seg_6492" s="T159">üge</ta>
            <ta e="T161" id="Seg_6493" s="T160">amno-laʔbə</ta>
            <ta e="T162" id="Seg_6494" s="T161">kirgaːr-laʔbə</ta>
            <ta e="T163" id="Seg_6495" s="T162">dĭgəttə</ta>
            <ta e="T164" id="Seg_6496" s="T163">nʼergö-luʔbdə-bi</ta>
            <ta e="T165" id="Seg_6497" s="T164">gijen</ta>
            <ta e="T166" id="Seg_6498" s="T165">kuja</ta>
            <ta e="T167" id="Seg_6499" s="T166">uʔbdə-laʔbə</ta>
            <ta e="T168" id="Seg_6500" s="T167">ugaːndə</ta>
            <ta e="T169" id="Seg_6501" s="T168">külük-zəbi</ta>
            <ta e="T170" id="Seg_6502" s="T169">il</ta>
            <ta e="T171" id="Seg_6503" s="T170">bar</ta>
            <ta e="T172" id="Seg_6504" s="T171">pʼaŋdə-bi-jəʔ</ta>
            <ta e="T173" id="Seg_6505" s="T172">sazən-Tə</ta>
            <ta e="T174" id="Seg_6506" s="T173">dĭ</ta>
            <ta e="T175" id="Seg_6507" s="T174">kuza</ta>
            <ta e="T176" id="Seg_6508" s="T175">ugaːndə</ta>
            <ta e="T177" id="Seg_6509" s="T176">ej</ta>
            <ta e="T178" id="Seg_6510" s="T177">jakšə</ta>
            <ta e="T179" id="Seg_6511" s="T178">bar</ta>
            <ta e="T180" id="Seg_6512" s="T179">ĭmbi</ta>
            <ta e="T181" id="Seg_6513" s="T180">i-laʔbə</ta>
            <ta e="T183" id="Seg_6514" s="T181">da</ta>
            <ta e="T184" id="Seg_6515" s="T183">bar</ta>
            <ta e="T185" id="Seg_6516" s="T184">ĭmbi</ta>
            <ta e="T186" id="Seg_6517" s="T185">i-laʔbə</ta>
            <ta e="T187" id="Seg_6518" s="T186">bar</ta>
            <ta e="T188" id="Seg_6519" s="T187">ugaːndə</ta>
            <ta e="T189" id="Seg_6520" s="T188">toli</ta>
            <ta e="T190" id="Seg_6521" s="T189">kuza</ta>
            <ta e="T191" id="Seg_6522" s="T190">bar</ta>
            <ta e="T192" id="Seg_6523" s="T191">ĭmbi</ta>
            <ta e="T193" id="Seg_6524" s="T192">i-liA</ta>
            <ta e="T195" id="Seg_6525" s="T193">tak</ta>
            <ta e="T196" id="Seg_6526" s="T195">bar</ta>
            <ta e="T197" id="Seg_6527" s="T196">i-laʔbə</ta>
            <ta e="T198" id="Seg_6528" s="T197">bar</ta>
            <ta e="T199" id="Seg_6529" s="T198">kun-də-laʔbə</ta>
            <ta e="T200" id="Seg_6530" s="T199">maʔ-gəndə</ta>
            <ta e="T201" id="Seg_6531" s="T200">măn</ta>
            <ta e="T202" id="Seg_6532" s="T201">teinen</ta>
            <ta e="T203" id="Seg_6533" s="T202">koʔbdo-nə</ta>
            <ta e="T204" id="Seg_6534" s="T203">plat-ə-m</ta>
            <ta e="T205" id="Seg_6535" s="T204">i-bi-m</ta>
            <ta e="T206" id="Seg_6536" s="T205">dĭ</ta>
            <ta e="T207" id="Seg_6537" s="T206">kan-bi</ta>
            <ta e="T208" id="Seg_6538" s="T207">Malinovka-Tə</ta>
            <ta e="T209" id="Seg_6539" s="T208">tüšəl-zittə</ta>
            <ta e="T210" id="Seg_6540" s="T209">dĭn</ta>
            <ta e="T211" id="Seg_6541" s="T210">nüke</ta>
            <ta e="T212" id="Seg_6542" s="T211">surar-laʔbə</ta>
            <ta e="T213" id="Seg_6543" s="T212">gijen</ta>
            <ta e="T214" id="Seg_6544" s="T213">tăn</ta>
            <ta e="T215" id="Seg_6545" s="T214">plat</ta>
            <ta e="T216" id="Seg_6546" s="T215">i-bi-l</ta>
            <ta e="T217" id="Seg_6547" s="T216">urgaja</ta>
            <ta e="T218" id="Seg_6548" s="T217">mĭ-bi</ta>
            <ta e="T219" id="Seg_6549" s="T218">a</ta>
            <ta e="T220" id="Seg_6550" s="T219">urgaja</ta>
            <ta e="T221" id="Seg_6551" s="T220">gijen</ta>
            <ta e="T222" id="Seg_6552" s="T221">i-bi</ta>
            <ta e="T223" id="Seg_6553" s="T222">Polʼa</ta>
            <ta e="T224" id="Seg_6554" s="T223">mĭ-bi</ta>
            <ta e="T225" id="Seg_6555" s="T224">dö</ta>
            <ta e="T226" id="Seg_6556" s="T225">ine</ta>
            <ta e="T227" id="Seg_6557" s="T226">i-bi-m</ta>
            <ta e="T228" id="Seg_6558" s="T227">iʔgö</ta>
            <ta e="T229" id="Seg_6559" s="T228">aktʼa</ta>
            <ta e="T230" id="Seg_6560" s="T229">mĭ-bi-m</ta>
            <ta e="T231" id="Seg_6561" s="T230">a</ta>
            <ta e="T232" id="Seg_6562" s="T231">baška</ta>
            <ta e="T233" id="Seg_6563" s="T232">ine</ta>
            <ta e="T234" id="Seg_6564" s="T233">i-bi-m</ta>
            <ta e="T235" id="Seg_6565" s="T234">amka</ta>
            <ta e="T236" id="Seg_6566" s="T235">mĭ-bi-m</ta>
            <ta e="T238" id="Seg_6567" s="T237">dʼabə-bi-m</ta>
            <ta e="T241" id="Seg_6568" s="T240">büzəj</ta>
            <ta e="T243" id="Seg_6569" s="T241">bar</ta>
            <ta e="T247" id="Seg_6570" s="T246">ej</ta>
            <ta e="T248" id="Seg_6571" s="T247">kandə-gA</ta>
            <ta e="T249" id="Seg_6572" s="T248">măn</ta>
            <ta e="T251" id="Seg_6573" s="T249">bar</ta>
            <ta e="T257" id="Seg_6574" s="T256">ugaːndə</ta>
            <ta e="T258" id="Seg_6575" s="T257">kuštu</ta>
            <ta e="T259" id="Seg_6576" s="T258">ej</ta>
            <ta e="T260" id="Seg_6577" s="T259">mo-liA-m</ta>
            <ta e="T261" id="Seg_6578" s="T260">kun-zittə</ta>
            <ta e="T262" id="Seg_6579" s="T261">bar</ta>
            <ta e="T267" id="Seg_6580" s="T266">kan-ntə-gA-m</ta>
            <ta e="T268" id="Seg_6581" s="T267">bar</ta>
            <ta e="T270" id="Seg_6582" s="T269">ej</ta>
            <ta e="T271" id="Seg_6583" s="T270">jakšə</ta>
            <ta e="T272" id="Seg_6584" s="T271">nʼi</ta>
            <ta e="T273" id="Seg_6585" s="T272">nʼi</ta>
            <ta e="T274" id="Seg_6586" s="T273">üge</ta>
            <ta e="T275" id="Seg_6587" s="T274">bar</ta>
            <ta e="T276" id="Seg_6588" s="T275">tojar-laʔbə</ta>
            <ta e="T277" id="Seg_6589" s="T276">da</ta>
            <ta e="T278" id="Seg_6590" s="T277">kun-də-laʔbə</ta>
            <ta e="T279" id="Seg_6591" s="T278">măn</ta>
            <ta e="T280" id="Seg_6592" s="T279">dʼije-Kən</ta>
            <ta e="T281" id="Seg_6593" s="T280">mĭn-bi-m</ta>
            <ta e="T282" id="Seg_6594" s="T281">ĭmbi=də</ta>
            <ta e="T285" id="Seg_6595" s="T284">ku-bi-m</ta>
            <ta e="T286" id="Seg_6596" s="T285">dĭgəttə</ta>
            <ta e="T287" id="Seg_6597" s="T286">măn</ta>
            <ta e="T288" id="Seg_6598" s="T287">nereʔ-r-luʔbdə-bi-m</ta>
            <ta e="T289" id="Seg_6599" s="T288">bar</ta>
            <ta e="T290" id="Seg_6600" s="T289">dĭgəttə</ta>
            <ta e="T293" id="Seg_6601" s="T292">kudaj</ta>
            <ta e="T294" id="Seg_6602" s="T293">kăštə-bi-m</ta>
            <ta e="T295" id="Seg_6603" s="T294">bar</ta>
            <ta e="T296" id="Seg_6604" s="T295">kan-lAʔ</ta>
            <ta e="T297" id="Seg_6605" s="T296">tʼür-bi-jəʔ</ta>
            <ta e="T298" id="Seg_6606" s="T297">nadə</ta>
            <ta e="T299" id="Seg_6607" s="T298">ši-jəʔ</ta>
            <ta e="T300" id="Seg_6608" s="T299">păjdə-zittə</ta>
            <ta e="T301" id="Seg_6609" s="T300">nadə</ta>
            <ta e="T302" id="Seg_6610" s="T301">varota</ta>
            <ta e="T303" id="Seg_6611" s="T302">a-zittə</ta>
            <ta e="T305" id="Seg_6612" s="T304">dĭ</ta>
            <ta e="T310" id="Seg_6613" s="T309">dĭ</ta>
            <ta e="T311" id="Seg_6614" s="T310">ši-jəʔ-Tə</ta>
            <ta e="T312" id="Seg_6615" s="T311">nadə</ta>
            <ta e="T313" id="Seg_6616" s="T312">pa-jəʔ</ta>
            <ta e="T314" id="Seg_6617" s="T313">păda-zittə</ta>
            <ta e="T315" id="Seg_6618" s="T314">dĭgəttə</ta>
            <ta e="T316" id="Seg_6619" s="T315">šeden</ta>
            <ta e="T317" id="Seg_6620" s="T316">kaj-lV-bəj</ta>
            <ta e="T318" id="Seg_6621" s="T317">dĭgəttə</ta>
            <ta e="T319" id="Seg_6622" s="T318">büzəj</ta>
            <ta e="T320" id="Seg_6623" s="T319">ej</ta>
            <ta e="T321" id="Seg_6624" s="T320">nuʔmə-lV-j</ta>
            <ta e="T322" id="Seg_6625" s="T321">šeden-Kən</ta>
            <ta e="T325" id="Seg_6626" s="T324">šeden-Kən</ta>
            <ta e="T326" id="Seg_6627" s="T325">nu-laʔbə</ta>
            <ta e="T338" id="Seg_6628" s="T337">ĭmbi-ziʔ=də</ta>
            <ta e="T339" id="Seg_6629" s="T338">bar</ta>
            <ta e="T340" id="Seg_6630" s="T339">putʼtʼəm-liA</ta>
            <ta e="T341" id="Seg_6631" s="T340">tăn</ta>
            <ta e="T342" id="Seg_6632" s="T341">püje-l</ta>
            <ta e="T344" id="Seg_6633" s="T342">ugaːndə</ta>
            <ta e="T347" id="Seg_6634" s="T346">tăn</ta>
            <ta e="T348" id="Seg_6635" s="T347">püje-l</ta>
            <ta e="T349" id="Seg_6636" s="T348">ugaːndə</ta>
            <ta e="T350" id="Seg_6637" s="T349">puʔmə</ta>
            <ta e="T351" id="Seg_6638" s="T350">bar</ta>
            <ta e="T352" id="Seg_6639" s="T351">nʼilgö-laʔbə</ta>
            <ta e="T353" id="Seg_6640" s="T352">tăn</ta>
            <ta e="T354" id="Seg_6641" s="T353">gijen</ta>
            <ta e="T355" id="Seg_6642" s="T354">i-bi-l</ta>
            <ta e="T356" id="Seg_6643" s="T355">bü-j-lAʔ</ta>
            <ta e="T357" id="Seg_6644" s="T356">mĭn-bi-m</ta>
            <ta e="T358" id="Seg_6645" s="T357">nadə</ta>
            <ta e="T359" id="Seg_6646" s="T358">segi</ta>
            <ta e="T360" id="Seg_6647" s="T359">bü</ta>
            <ta e="T361" id="Seg_6648" s="T360">mĭnzər-zittə</ta>
            <ta e="T362" id="Seg_6649" s="T361">nadə</ta>
            <ta e="T363" id="Seg_6650" s="T362">mĭje</ta>
            <ta e="T366" id="Seg_6651" s="T365">mĭnzər-zittə</ta>
            <ta e="T367" id="Seg_6652" s="T366">tibi-zAŋ</ta>
            <ta e="T368" id="Seg_6653" s="T367">bădə-zittə</ta>
            <ta e="T369" id="Seg_6654" s="T368">măn</ta>
            <ta e="T370" id="Seg_6655" s="T369">biəʔ</ta>
            <ta e="T371" id="Seg_6656" s="T370">aktʼa</ta>
            <ta e="T372" id="Seg_6657" s="T371">i-gA</ta>
            <ta e="T373" id="Seg_6658" s="T372">nadə</ta>
            <ta e="T374" id="Seg_6659" s="T373">tănan</ta>
            <ta e="T375" id="Seg_6660" s="T374">sumna</ta>
            <ta e="T376" id="Seg_6661" s="T375">mĭ-zittə</ta>
            <ta e="T377" id="Seg_6662" s="T376">sumna</ta>
            <ta e="T378" id="Seg_6663" s="T377">bos-gəndə</ta>
            <ta e="T379" id="Seg_6664" s="T378">ma-zittə</ta>
            <ta e="T380" id="Seg_6665" s="T379">možet</ta>
            <ta e="T381" id="Seg_6666" s="T380">gibər=nʼibudʼ</ta>
            <ta e="T382" id="Seg_6667" s="T381">kan-lV-l</ta>
            <ta e="T383" id="Seg_6668" s="T382">aktʼa</ta>
            <ta e="T384" id="Seg_6669" s="T383">kereʔ</ta>
            <ta e="T385" id="Seg_6670" s="T384">mo-lV-j</ta>
            <ta e="T387" id="Seg_6671" s="T386">Matvejev</ta>
            <ta e="T388" id="Seg_6672" s="T387">bar</ta>
            <ta e="T389" id="Seg_6673" s="T388">amno-laʔbə</ta>
            <ta e="T390" id="Seg_6674" s="T389">ej</ta>
            <ta e="T391" id="Seg_6675" s="T390">tʼăbaktər-liA</ta>
            <ta e="T392" id="Seg_6676" s="T391">sĭj-də</ta>
            <ta e="T393" id="Seg_6677" s="T392">ĭzem-liA</ta>
            <ta e="T394" id="Seg_6678" s="T393">maʔ-Tə-l</ta>
            <ta e="T395" id="Seg_6679" s="T394">sazən</ta>
            <ta e="T396" id="Seg_6680" s="T395">naga</ta>
            <ta e="T397" id="Seg_6681" s="T396">ne-t</ta>
            <ta e="T398" id="Seg_6682" s="T397">bar</ta>
            <ta e="T399" id="Seg_6683" s="T398">kuroː-luʔbdə-bi</ta>
            <ta e="T400" id="Seg_6684" s="T399">ej</ta>
            <ta e="T401" id="Seg_6685" s="T400">pʼaŋdə-liA</ta>
            <ta e="T404" id="Seg_6686" s="T403">sazən</ta>
            <ta e="T405" id="Seg_6687" s="T404">e-ʔ</ta>
            <ta e="T406" id="Seg_6688" s="T405">kuroː-ʔ</ta>
            <ta e="T410" id="Seg_6689" s="T409">büžü</ta>
            <ta e="T411" id="Seg_6690" s="T410">maʔ-Tə-n</ta>
            <ta e="T412" id="Seg_6691" s="T411">šo-lV-m</ta>
            <ta e="T413" id="Seg_6692" s="T412">ĭmbi=nʼibudʼ</ta>
            <ta e="T414" id="Seg_6693" s="T413">tănan</ta>
            <ta e="T415" id="Seg_6694" s="T414">det-lV-m</ta>
            <ta e="T416" id="Seg_6695" s="T415">tăŋ</ta>
            <ta e="T417" id="Seg_6696" s="T416">ej</ta>
            <ta e="T418" id="Seg_6697" s="T417">tʼăbaktər-ə-ʔ</ta>
            <ta e="T419" id="Seg_6698" s="T418">koldʼəŋ</ta>
            <ta e="T420" id="Seg_6699" s="T419">tʼăbaktər-ə-ʔ</ta>
            <ta e="T421" id="Seg_6700" s="T420">ato</ta>
            <ta e="T422" id="Seg_6701" s="T421">bar</ta>
            <ta e="T423" id="Seg_6702" s="T422">il</ta>
            <ta e="T424" id="Seg_6703" s="T423">nünə-liA-jəʔ</ta>
            <ta e="T425" id="Seg_6704" s="T424">tăŋ</ta>
            <ta e="T426" id="Seg_6705" s="T425">e-ʔ</ta>
            <ta e="T427" id="Seg_6706" s="T426">kirgaːr-ə-ʔ</ta>
            <ta e="T428" id="Seg_6707" s="T427">a</ta>
            <ta e="T429" id="Seg_6708" s="T428">e-ʔ</ta>
            <ta e="T430" id="Seg_6709" s="T429">tăn</ta>
            <ta e="T431" id="Seg_6710" s="T430">e-ʔ</ta>
            <ta e="T432" id="Seg_6711" s="T431">kudo-nzə-ʔ</ta>
            <ta e="T433" id="Seg_6712" s="T432">ato</ta>
            <ta e="T434" id="Seg_6713" s="T433">il</ta>
            <ta e="T435" id="Seg_6714" s="T434">bar</ta>
            <ta e="T436" id="Seg_6715" s="T435">nünə-laʔbə-jəʔ</ta>
            <ta e="T437" id="Seg_6716" s="T436">kădaʔ</ta>
            <ta e="T438" id="Seg_6717" s="T437">tăn</ta>
            <ta e="T439" id="Seg_6718" s="T438">tăn</ta>
            <ta e="T440" id="Seg_6719" s="T439">kudo-nzə-laʔbə-l</ta>
            <ta e="T441" id="Seg_6720" s="T440">măn</ta>
            <ta e="T442" id="Seg_6721" s="T441">teinen</ta>
            <ta e="T443" id="Seg_6722" s="T442">oroma</ta>
            <ta e="T444" id="Seg_6723" s="T443">kămnə-bi-m</ta>
            <ta e="T445" id="Seg_6724" s="T444">bĭlgar-bi-m</ta>
            <ta e="T446" id="Seg_6725" s="T445">krinka-jəʔ</ta>
            <ta e="T447" id="Seg_6726" s="T446">bazəj-də-bi-m</ta>
            <ta e="T448" id="Seg_6727" s="T447">edəʔ-bi-m</ta>
            <ta e="T449" id="Seg_6728" s="T448">kuja</ta>
            <ta e="T450" id="Seg_6729" s="T449">koʔ-laʔbə</ta>
            <ta e="T451" id="Seg_6730" s="T450">bar</ta>
            <ta e="T452" id="Seg_6731" s="T451">ĭmbi</ta>
            <ta e="T453" id="Seg_6732" s="T452">det-bi-l</ta>
            <ta e="T454" id="Seg_6733" s="T453">pʼer-t</ta>
            <ta e="T455" id="Seg_6734" s="T454">măna</ta>
            <ta e="T458" id="Seg_6735" s="T457">ĭmbi</ta>
            <ta e="T459" id="Seg_6736" s="T458">dĭn</ta>
            <ta e="T460" id="Seg_6737" s="T459">iʔbə-laʔbə</ta>
            <ta e="T461" id="Seg_6738" s="T460">măn</ta>
            <ta e="T462" id="Seg_6739" s="T461">teinen</ta>
            <ta e="T463" id="Seg_6740" s="T462">kalba-j-lAʔ</ta>
            <ta e="T464" id="Seg_6741" s="T463">mĭn-bi-m</ta>
            <ta e="T465" id="Seg_6742" s="T464">kalba</ta>
            <ta e="T466" id="Seg_6743" s="T465">det-bi-m</ta>
            <ta e="T467" id="Seg_6744" s="T466">dĭgəttə</ta>
            <ta e="T468" id="Seg_6745" s="T467">dʼagar-lV-m</ta>
            <ta e="T469" id="Seg_6746" s="T468">munəj-jəʔ</ta>
            <ta e="T471" id="Seg_6747" s="T470">hen-lV-m</ta>
            <ta e="T472" id="Seg_6748" s="T471">oroma</ta>
            <ta e="T473" id="Seg_6749" s="T472">hen-lV-m</ta>
            <ta e="T474" id="Seg_6750" s="T473">dĭgəttə</ta>
            <ta e="T475" id="Seg_6751" s="T474">pür-zittə</ta>
            <ta e="T476" id="Seg_6752" s="T475">pirog-jəʔ</ta>
            <ta e="T477" id="Seg_6753" s="T476">mo-lV-m</ta>
            <ta e="T479" id="Seg_6754" s="T478">uja</ta>
            <ta e="T480" id="Seg_6755" s="T479">mĭnzər-bi-m</ta>
            <ta e="T481" id="Seg_6756" s="T480">uja</ta>
            <ta e="T482" id="Seg_6757" s="T481">am-bi-jəʔ</ta>
            <ta e="T483" id="Seg_6758" s="T482">tĭmei-zittə</ta>
            <ta e="T484" id="Seg_6759" s="T483">onʼiʔ</ta>
            <ta e="T485" id="Seg_6760" s="T484">le</ta>
            <ta e="T486" id="Seg_6761" s="T485">ma-luʔbdə-bi</ta>
            <ta e="T490" id="Seg_6762" s="T489">men-zAŋ</ta>
            <ta e="T491" id="Seg_6763" s="T490">bar</ta>
            <ta e="T492" id="Seg_6764" s="T491">le</ta>
            <ta e="T493" id="Seg_6765" s="T492">am-laʔbə-jəʔ</ta>
            <ta e="T494" id="Seg_6766" s="T493">ugaːndə</ta>
            <ta e="T495" id="Seg_6767" s="T494">iʔgö</ta>
            <ta e="T496" id="Seg_6768" s="T495">le</ta>
            <ta e="T497" id="Seg_6769" s="T496">barəʔ-bi-bAʔ</ta>
            <ta e="T498" id="Seg_6770" s="T497">măn</ta>
            <ta e="T501" id="Seg_6771" s="T500">măn</ta>
            <ta e="T502" id="Seg_6772" s="T501">teinen</ta>
            <ta e="T503" id="Seg_6773" s="T502">ipek</ta>
            <ta e="T504" id="Seg_6774" s="T503">pür-bi-m</ta>
            <ta e="T505" id="Seg_6775" s="T504">ej</ta>
            <ta e="T506" id="Seg_6776" s="T505">namzəga</ta>
            <ta e="T507" id="Seg_6777" s="T506">ej</ta>
            <ta e="T508" id="Seg_6778" s="T507">jakšə</ta>
            <ta e="T509" id="Seg_6779" s="T508">bar</ta>
            <ta e="T510" id="Seg_6780" s="T509">măn</ta>
            <ta e="T511" id="Seg_6781" s="T510">dĭ</ta>
            <ta e="T512" id="Seg_6782" s="T511">ipek</ta>
            <ta e="T513" id="Seg_6783" s="T512">dĭ</ta>
            <ta e="T516" id="Seg_6784" s="T515">men-zAŋ-Tə</ta>
            <ta e="T517" id="Seg_6785" s="T516">mĭ-bi-m</ta>
            <ta e="T518" id="Seg_6786" s="T517">men-zAŋ</ta>
            <ta e="T519" id="Seg_6787" s="T518">bar</ta>
            <ta e="T520" id="Seg_6788" s="T519">ipek</ta>
            <ta e="T521" id="Seg_6789" s="T520">am-laʔbə-jəʔ</ta>
            <ta e="T522" id="Seg_6790" s="T521">i</ta>
            <ta e="T523" id="Seg_6791" s="T522">bar</ta>
            <ta e="T524" id="Seg_6792" s="T523">tʼabəro-laʔbə-jəʔ</ta>
            <ta e="T525" id="Seg_6793" s="T524">men</ta>
            <ta e="T526" id="Seg_6794" s="T525">bar</ta>
            <ta e="T527" id="Seg_6795" s="T526">dĭ</ta>
            <ta e="T528" id="Seg_6796" s="T527">men-də</ta>
            <ta e="T529" id="Seg_6797" s="T528">tĭmne-t</ta>
            <ta e="T530" id="Seg_6798" s="T529">bar</ta>
            <ta e="T531" id="Seg_6799" s="T530">nüdʼi</ta>
            <ta e="T532" id="Seg_6800" s="T531">i-bi</ta>
            <ta e="T533" id="Seg_6801" s="T532">nüdʼi</ta>
            <ta e="T534" id="Seg_6802" s="T533">i-bi</ta>
            <ta e="T536" id="Seg_6803" s="T534">ugaːndə</ta>
            <ta e="T540" id="Seg_6804" s="T539">ĭmbi=də</ta>
            <ta e="T541" id="Seg_6805" s="T540">ej</ta>
            <ta e="T542" id="Seg_6806" s="T541">ed-liA</ta>
            <ta e="T543" id="Seg_6807" s="T542">măn</ta>
            <ta e="T544" id="Seg_6808" s="T543">dĭ-m</ta>
            <ta e="T545" id="Seg_6809" s="T544">ej</ta>
            <ta e="T546" id="Seg_6810" s="T545">tĭmne-bi-m</ta>
            <ta e="T547" id="Seg_6811" s="T546">šində</ta>
            <ta e="T548" id="Seg_6812" s="T547">šonə-gA</ta>
            <ta e="T549" id="Seg_6813" s="T548">măn</ta>
            <ta e="T550" id="Seg_6814" s="T549">aba-m</ta>
            <ta e="T551" id="Seg_6815" s="T550">tujez-jəʔ</ta>
            <ta e="T552" id="Seg_6816" s="T551">a-bi</ta>
            <ta e="T553" id="Seg_6817" s="T552">kuba</ta>
            <ta e="T554" id="Seg_6818" s="T553">kuba</ta>
            <ta e="T555" id="Seg_6819" s="T554">kür</ta>
            <ta e="T556" id="Seg_6820" s="T555">kür-bi</ta>
            <ta e="T557" id="Seg_6821" s="T556">i</ta>
            <ta e="T558" id="Seg_6822" s="T557">a-laʔbə-bi</ta>
            <ta e="T559" id="Seg_6823" s="T558">šojdʼo</ta>
            <ta e="T560" id="Seg_6824" s="T559">šöʔ-bi-jəʔ</ta>
            <ta e="T561" id="Seg_6825" s="T560">žila-jəʔ-ziʔ</ta>
            <ta e="T562" id="Seg_6826" s="T561">bar</ta>
            <ta e="T563" id="Seg_6827" s="T562">nʼimi-ziʔ</ta>
            <ta e="T564" id="Seg_6828" s="T563">i</ta>
            <ta e="T565" id="Seg_6829" s="T564">ĭntak</ta>
            <ta e="T566" id="Seg_6830" s="T565">müjə-Kən</ta>
            <ta e="T567" id="Seg_6831" s="T566">i-bi</ta>
            <ta e="T569" id="Seg_6832" s="T567">šojdʼo-jəʔ</ta>
            <ta e="T570" id="Seg_6833" s="T569">šöʔ-bi-jəʔ</ta>
            <ta e="T571" id="Seg_6834" s="T570">bar</ta>
            <ta e="T572" id="Seg_6835" s="T571">žila-jəʔ-ziʔ</ta>
            <ta e="T573" id="Seg_6836" s="T572">i</ta>
            <ta e="T576" id="Seg_6837" s="T575">ĭntak</ta>
            <ta e="T577" id="Seg_6838" s="T576">šer-bi</ta>
            <ta e="T578" id="Seg_6839" s="T577">müjə-Tə</ta>
            <ta e="T579" id="Seg_6840" s="T578">i</ta>
            <ta e="T580" id="Seg_6841" s="T579">nʼimi</ta>
            <ta e="T581" id="Seg_6842" s="T580">i-bi</ta>
            <ta e="T582" id="Seg_6843" s="T581">šödör-bi</ta>
            <ta e="T593" id="Seg_6844" s="T592">müjə-bə</ta>
            <ta e="T594" id="Seg_6845" s="T593">bar</ta>
            <ta e="T595" id="Seg_6846" s="T594">müʔbdə-lV-j</ta>
            <ta e="T596" id="Seg_6847" s="T595">tăn</ta>
            <ta e="T597" id="Seg_6848" s="T596">bar</ta>
            <ta e="T598" id="Seg_6849" s="T597">ne-l</ta>
            <ta e="T599" id="Seg_6850" s="T598">ajir-laʔbə-l</ta>
            <ta e="T600" id="Seg_6851" s="T599">sĭj-də</ta>
            <ta e="T601" id="Seg_6852" s="T600">ĭzem-liA</ta>
            <ta e="T602" id="Seg_6853" s="T601">ugaːndə</ta>
            <ta e="T603" id="Seg_6854" s="T602">măn</ta>
            <ta e="T604" id="Seg_6855" s="T603">bar</ta>
            <ta e="T605" id="Seg_6856" s="T604">tura-m</ta>
            <ta e="T606" id="Seg_6857" s="T605">sagər</ta>
            <ta e="T609" id="Seg_6858" s="T608">karəldʼaːn</ta>
            <ta e="T610" id="Seg_6859" s="T609">bazə-lV-m</ta>
            <ta e="T611" id="Seg_6860" s="T610">ugaːndə</ta>
            <ta e="T612" id="Seg_6861" s="T611">balgaš</ta>
            <ta e="T613" id="Seg_6862" s="T612">ato</ta>
            <ta e="T614" id="Seg_6863" s="T613">il</ta>
            <ta e="T615" id="Seg_6864" s="T614">bar</ta>
            <ta e="T616" id="Seg_6865" s="T615">măn-laʔbə-jəʔ</ta>
            <ta e="T617" id="Seg_6866" s="T616">măn-liA-jəʔ</ta>
            <ta e="T618" id="Seg_6867" s="T617">ɨrɨː</ta>
            <ta e="T619" id="Seg_6868" s="T618">nüke</ta>
            <ta e="T620" id="Seg_6869" s="T619">tura</ta>
            <ta e="T621" id="Seg_6870" s="T620">ej</ta>
            <ta e="T622" id="Seg_6871" s="T621">bazə-liA</ta>
            <ta e="T627" id="Seg_6872" s="T626">ĭmbi</ta>
            <ta e="T628" id="Seg_6873" s="T627">tănan</ta>
            <ta e="T629" id="Seg_6874" s="T628">tʼăbaktər-zittə</ta>
            <ta e="T630" id="Seg_6875" s="T629">măn</ta>
            <ta e="T631" id="Seg_6876" s="T630">bar</ta>
            <ta e="T632" id="Seg_6877" s="T631">nöməl-bi-m</ta>
            <ta e="T633" id="Seg_6878" s="T632">teinen</ta>
            <ta e="T634" id="Seg_6879" s="T633">kunol-zittə</ta>
            <ta e="T635" id="Seg_6880" s="T634">klop-jəʔ</ta>
            <ta e="T636" id="Seg_6881" s="T635">ej</ta>
            <ta e="T637" id="Seg_6882" s="T636">mĭ-bi-jəʔ</ta>
            <ta e="T638" id="Seg_6883" s="T637">dĭ</ta>
            <ta e="T639" id="Seg_6884" s="T638">kuza</ta>
            <ta e="T640" id="Seg_6885" s="T639">un</ta>
            <ta e="T641" id="Seg_6886" s="T640">pi-bi</ta>
            <ta e="T642" id="Seg_6887" s="T641">nadə</ta>
            <ta e="T643" id="Seg_6888" s="T642">dĭ-Tə</ta>
            <ta e="T644" id="Seg_6889" s="T643">mĭ-zittə</ta>
            <ta e="T645" id="Seg_6890" s="T644">dĭ-n</ta>
            <ta e="T646" id="Seg_6891" s="T645">ipek</ta>
            <ta e="T647" id="Seg_6892" s="T646">naga</ta>
            <ta e="T648" id="Seg_6893" s="T647">am-zittə</ta>
            <ta e="T649" id="Seg_6894" s="T648">bar</ta>
            <ta e="T656" id="Seg_6895" s="T655">gijen</ta>
            <ta e="T657" id="Seg_6896" s="T656">i-zittə</ta>
            <ta e="T658" id="Seg_6897" s="T657">ĭmbi</ta>
            <ta e="T659" id="Seg_6898" s="T658">tʼăbaktər-zittə</ta>
            <ta e="T660" id="Seg_6899" s="T659">dĭ-Tə</ta>
            <ta e="T661" id="Seg_6900" s="T660">ugaːndə</ta>
            <ta e="T664" id="Seg_6901" s="T663">iʔgö</ta>
            <ta e="T665" id="Seg_6902" s="T664">nadə</ta>
            <ta e="T666" id="Seg_6903" s="T665">tʼăbaktər-zittə</ta>
            <ta e="T670" id="Seg_6904" s="T668">nadə</ta>
            <ta e="T671" id="Seg_6905" s="T670">kan-zittə</ta>
            <ta e="T672" id="Seg_6906" s="T671">măndo-r-zittə</ta>
            <ta e="T673" id="Seg_6907" s="T672">gijen=nʼibudʼ</ta>
            <ta e="T674" id="Seg_6908" s="T673">tʼăbaktər-zittə</ta>
            <ta e="T675" id="Seg_6909" s="T674">măn</ta>
            <ta e="T676" id="Seg_6910" s="T675">taldʼen</ta>
            <ta e="T677" id="Seg_6911" s="T676">kondʼo</ta>
            <ta e="T678" id="Seg_6912" s="T677">ej</ta>
            <ta e="T681" id="Seg_6913" s="T680">iʔbə-bi-m</ta>
            <ta e="T682" id="Seg_6914" s="T681">kunol-zittə</ta>
            <ta e="T685" id="Seg_6915" s="T684">kondʼo</ta>
            <ta e="T686" id="Seg_6916" s="T685">ej</ta>
            <ta e="T688" id="Seg_6917" s="T687">ej</ta>
            <ta e="T690" id="Seg_6918" s="T689">ej</ta>
            <ta e="T691" id="Seg_6919" s="T690">iʔbə-lV-m</ta>
            <ta e="T692" id="Seg_6920" s="T691">kunol-zittə</ta>
            <ta e="T697" id="Seg_6921" s="T696">kunol-bi-m</ta>
            <ta e="T698" id="Seg_6922" s="T697">ĭmbi=də</ta>
            <ta e="T699" id="Seg_6923" s="T698">ej</ta>
            <ta e="T700" id="Seg_6924" s="T699">ku-bi-m</ta>
            <ta e="T701" id="Seg_6925" s="T700">dĭ</ta>
            <ta e="T702" id="Seg_6926" s="T701">kuza-Kən</ta>
            <ta e="T703" id="Seg_6927" s="T702">eʔbdə-t</ta>
            <ta e="T704" id="Seg_6928" s="T703">bar</ta>
            <ta e="T705" id="Seg_6929" s="T704">sagər</ta>
            <ta e="T706" id="Seg_6930" s="T705">i-bi</ta>
            <ta e="T707" id="Seg_6931" s="T706">tüj</ta>
            <ta e="T708" id="Seg_6932" s="T707">bar</ta>
            <ta e="T709" id="Seg_6933" s="T708">ugaːndə</ta>
            <ta e="T710" id="Seg_6934" s="T709">sĭri</ta>
            <ta e="T711" id="Seg_6935" s="T710">mo-laːm-bi-jəʔ</ta>
            <ta e="T712" id="Seg_6936" s="T711">eʔbdə</ta>
            <ta e="T713" id="Seg_6937" s="T712">onʼiʔ</ta>
            <ta e="T714" id="Seg_6938" s="T713">uda</ta>
            <ta e="T715" id="Seg_6939" s="T714">i-gA</ta>
            <ta e="T716" id="Seg_6940" s="T715">a</ta>
            <ta e="T717" id="Seg_6941" s="T716">onʼiʔ</ta>
            <ta e="T718" id="Seg_6942" s="T717">uda</ta>
            <ta e="T719" id="Seg_6943" s="T718">nĭŋgə-bi-jəʔ</ta>
            <ta e="T720" id="Seg_6944" s="T719">onʼiʔ</ta>
            <ta e="T721" id="Seg_6945" s="T720">uda</ta>
            <ta e="T722" id="Seg_6946" s="T721">i-gA</ta>
            <ta e="T723" id="Seg_6947" s="T722">a</ta>
            <ta e="T724" id="Seg_6948" s="T723">onʼiʔ</ta>
            <ta e="T725" id="Seg_6949" s="T724">uda-bə</ta>
            <ta e="T726" id="Seg_6950" s="T725">bar</ta>
            <ta e="T727" id="Seg_6951" s="T726">sajnĭŋgə-bi-jəʔ</ta>
            <ta e="T728" id="Seg_6952" s="T727">măn</ta>
            <ta e="T729" id="Seg_6953" s="T728">üzə-bi-m</ta>
            <ta e="T731" id="Seg_6954" s="T730">onʼiʔ</ta>
            <ta e="T732" id="Seg_6955" s="T731">uda-m</ta>
            <ta e="T733" id="Seg_6956" s="T732">băldə-bi-m</ta>
            <ta e="T735" id="Seg_6957" s="T734">măn</ta>
            <ta e="T736" id="Seg_6958" s="T735">saʔmə-luʔbdə-bi-m</ta>
            <ta e="T737" id="Seg_6959" s="T736">i</ta>
            <ta e="T738" id="Seg_6960" s="T737">uda-m</ta>
            <ta e="T739" id="Seg_6961" s="T738">băldə</ta>
            <ta e="T740" id="Seg_6962" s="T739">băldə-lə-bi-m</ta>
            <ta e="T741" id="Seg_6963" s="T740">dĭgəttə</ta>
            <ta e="T742" id="Seg_6964" s="T741">dĭ</ta>
            <ta e="T743" id="Seg_6965" s="T742">uda-m</ta>
            <ta e="T744" id="Seg_6966" s="T743">hen-bi-jəʔ</ta>
            <ta e="T745" id="Seg_6967" s="T744">bar</ta>
            <ta e="T752" id="Seg_6968" s="T901">dĭgəttə</ta>
            <ta e="T753" id="Seg_6969" s="T752">uda-m</ta>
            <ta e="T918" id="Seg_6970" s="T753">bar</ta>
            <ta e="T755" id="Seg_6971" s="T918">sar-bi-jəʔ</ta>
            <ta e="T759" id="Seg_6972" s="T758">paka</ta>
            <ta e="T760" id="Seg_6973" s="T759">ej</ta>
            <ta e="T761" id="Seg_6974" s="T760">özer-lV-j</ta>
            <ta e="T762" id="Seg_6975" s="T761">ĭmbi=də</ta>
            <ta e="T763" id="Seg_6976" s="T762">togonər-zittə</ta>
            <ta e="T764" id="Seg_6977" s="T763">nʼelʼzja</ta>
            <ta e="T765" id="Seg_6978" s="T764">ine</ta>
            <ta e="T766" id="Seg_6979" s="T765">körer-zittə</ta>
            <ta e="T767" id="Seg_6980" s="T766">nadə</ta>
            <ta e="T772" id="Seg_6981" s="T771">ine</ta>
            <ta e="T774" id="Seg_6982" s="T773">nuʔmə-laʔpi</ta>
            <ta e="T775" id="Seg_6983" s="T774">bar</ta>
            <ta e="T782" id="Seg_6984" s="T781">ine</ta>
            <ta e="T783" id="Seg_6985" s="T782">nuʔmə-laʔpi</ta>
            <ta e="T784" id="Seg_6986" s="T783">saʔmə-luʔbdə-bi</ta>
            <ta e="T785" id="Seg_6987" s="T784">üjü-t</ta>
            <ta e="T786" id="Seg_6988" s="T785">băldə</ta>
            <ta e="T787" id="Seg_6989" s="T786">üjü-t</ta>
            <ta e="T788" id="Seg_6990" s="T787">băldə-bi</ta>
            <ta e="T789" id="Seg_6991" s="T788">măn</ta>
            <ta e="T790" id="Seg_6992" s="T789">uda-Kən</ta>
            <ta e="T791" id="Seg_6993" s="T790">sumna</ta>
            <ta e="T792" id="Seg_6994" s="T791">müjə-zAŋ-bə</ta>
            <ta e="T793" id="Seg_6995" s="T792">a</ta>
            <ta e="T794" id="Seg_6996" s="T793">tăn</ta>
            <ta e="T795" id="Seg_6997" s="T794">uda-Kən</ta>
            <ta e="T796" id="Seg_6998" s="T795">onʼiʔ</ta>
            <ta e="T797" id="Seg_6999" s="T796">naga</ta>
            <ta e="T798" id="Seg_7000" s="T797">baltu-ziʔ</ta>
            <ta e="T799" id="Seg_7001" s="T798">bar</ta>
            <ta e="T800" id="Seg_7002" s="T799">hʼaʔ-bi-l</ta>
            <ta e="T801" id="Seg_7003" s="T800">teinen</ta>
            <ta e="T802" id="Seg_7004" s="T801">ešši-zAŋ</ta>
            <ta e="T803" id="Seg_7005" s="T802">iʔgö</ta>
            <ta e="T804" id="Seg_7006" s="T803">bar</ta>
            <ta e="T805" id="Seg_7007" s="T804">büzəj</ta>
            <ta e="T806" id="Seg_7008" s="T805">sürer-laʔpi-jəʔ</ta>
            <ta e="T807" id="Seg_7009" s="T806">dĭ</ta>
            <ta e="T808" id="Seg_7010" s="T807">bar</ta>
            <ta e="T809" id="Seg_7011" s="T808">par-luʔbdə-bi</ta>
            <ta e="T810" id="Seg_7012" s="T809">i</ta>
            <ta e="T811" id="Seg_7013" s="T810">nuʔmə-luʔbdə-bi</ta>
            <ta e="T812" id="Seg_7014" s="T811">maʔ-gəndə</ta>
            <ta e="T813" id="Seg_7015" s="T812">nüdʼi-n</ta>
            <ta e="T814" id="Seg_7016" s="T813">kan-lAʔ</ta>
            <ta e="T815" id="Seg_7017" s="T814">tʼür-bi</ta>
            <ta e="T816" id="Seg_7018" s="T815">bar</ta>
            <ta e="T817" id="Seg_7019" s="T816">nüdʼi</ta>
            <ta e="T818" id="Seg_7020" s="T817">mo-laːndə-gA</ta>
            <ta e="T819" id="Seg_7021" s="T818">kuja</ta>
            <ta e="T820" id="Seg_7022" s="T819">bar</ta>
            <ta e="T821" id="Seg_7023" s="T820">amnə-laʔbə</ta>
            <ta e="T822" id="Seg_7024" s="T821">nüdʼi</ta>
            <ta e="T823" id="Seg_7025" s="T822">mo-laːndə-gA</ta>
            <ta e="T824" id="Seg_7026" s="T823">kuja</ta>
            <ta e="T825" id="Seg_7027" s="T824">amnol-laʔbə</ta>
            <ta e="T826" id="Seg_7028" s="T825">kabarləj</ta>
            <ta e="T827" id="Seg_7029" s="T826">tʼăbaktər-zittə</ta>
            <ta e="T828" id="Seg_7030" s="T827">nadə</ta>
            <ta e="T829" id="Seg_7031" s="T828">iʔbə-zittə</ta>
            <ta e="T830" id="Seg_7032" s="T829">kunol-zittə</ta>
            <ta e="T831" id="Seg_7033" s="T830">ertə-n</ta>
            <ta e="T832" id="Seg_7034" s="T831">ertə</ta>
            <ta e="T833" id="Seg_7035" s="T832">nadə</ta>
            <ta e="T834" id="Seg_7036" s="T833">uʔbdə-zittə</ta>
            <ta e="T840" id="Seg_7037" s="T839">kan-lV-m</ta>
            <ta e="T841" id="Seg_7038" s="T840">svetok-jəʔ</ta>
            <ta e="T842" id="Seg_7039" s="T841">nĭŋgə-zittə</ta>
            <ta e="T843" id="Seg_7040" s="T842">sĭri</ta>
            <ta e="T844" id="Seg_7041" s="T843">pa-Kən</ta>
            <ta e="T845" id="Seg_7042" s="T844">bar</ta>
            <ta e="T846" id="Seg_7043" s="T845">sazən</ta>
            <ta e="T847" id="Seg_7044" s="T846">edə-laʔbə</ta>
            <ta e="T848" id="Seg_7045" s="T847">ej</ta>
            <ta e="T849" id="Seg_7046" s="T848">ed-liA</ta>
            <ta e="T850" id="Seg_7047" s="T849">ĭmbi=də</ta>
            <ta e="T851" id="Seg_7048" s="T850">ej</ta>
            <ta e="T852" id="Seg_7049" s="T851">ku-liA-m</ta>
            <ta e="T853" id="Seg_7050" s="T852">măn</ta>
            <ta e="T854" id="Seg_7051" s="T853">tugan-m</ta>
            <ta e="T856" id="Seg_7052" s="T854">ugaːndə</ta>
            <ta e="T858" id="Seg_7053" s="T857">kuŋgə</ta>
            <ta e="T859" id="Seg_7054" s="T858">amno-laʔbə</ta>
            <ta e="T860" id="Seg_7055" s="T859">nadə</ta>
            <ta e="T861" id="Seg_7056" s="T860">šide</ta>
            <ta e="T862" id="Seg_7057" s="T861">tʼala</ta>
            <ta e="T863" id="Seg_7058" s="T862">kan-zittə</ta>
            <ta e="T864" id="Seg_7059" s="T863">a</ta>
            <ta e="T865" id="Seg_7060" s="T864">možet</ta>
            <ta e="T866" id="Seg_7061" s="T865">onʼiʔ</ta>
            <ta e="T867" id="Seg_7062" s="T866">tʼala-Tə</ta>
            <ta e="T868" id="Seg_7063" s="T867">kan-lV-l</ta>
            <ta e="T869" id="Seg_7064" s="T868">măn</ta>
            <ta e="T870" id="Seg_7065" s="T869">dĭ-Tə</ta>
            <ta e="T871" id="Seg_7066" s="T870">šo-bi-m</ta>
            <ta e="T872" id="Seg_7067" s="T871">a</ta>
            <ta e="T873" id="Seg_7068" s="T872">dĭ</ta>
            <ta e="T874" id="Seg_7069" s="T873">naga</ta>
            <ta e="T875" id="Seg_7070" s="T874">gibər=də</ta>
            <ta e="T876" id="Seg_7071" s="T875">kan-lAʔ</ta>
            <ta e="T877" id="Seg_7072" s="T876">tʼür-bi</ta>
            <ta e="T878" id="Seg_7073" s="T877">măn</ta>
            <ta e="T879" id="Seg_7074" s="T878">bar</ta>
            <ta e="T917" id="Seg_7075" s="T879">par-luʔbdə-bi</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PKZ">
            <ta e="T5" id="Seg_7076" s="T4">I.NOM</ta>
            <ta e="T6" id="Seg_7077" s="T5">NEG</ta>
            <ta e="T7" id="Seg_7078" s="T6">can-PRS-1SG</ta>
            <ta e="T10" id="Seg_7079" s="T9">move-INF.LAT</ta>
            <ta e="T11" id="Seg_7080" s="T10">all</ta>
            <ta e="T12" id="Seg_7081" s="T11">hurt-DUR.[3SG]</ta>
            <ta e="T13" id="Seg_7082" s="T12">however</ta>
            <ta e="T14" id="Seg_7083" s="T13">I.NOM</ta>
            <ta e="T15" id="Seg_7084" s="T14">die-RES-PST-1SG</ta>
            <ta e="T16" id="Seg_7085" s="T15">today</ta>
            <ta e="T17" id="Seg_7086" s="T16">this.[NOM.SG]</ta>
            <ta e="T18" id="Seg_7087" s="T17">girl.[NOM.SG]</ta>
            <ta e="T19" id="Seg_7088" s="T18">big.[NOM.SG]</ta>
            <ta e="T20" id="Seg_7089" s="T19">day.[NOM.SG]</ta>
            <ta e="T21" id="Seg_7090" s="T20">this-ACC</ta>
            <ta e="T22" id="Seg_7091" s="T21">today</ta>
            <ta e="T27" id="Seg_7092" s="T26">mother-NOM/GEN/ACC.3SG</ta>
            <ta e="T28" id="Seg_7093" s="T27">bring-PST.[3SG]</ta>
            <ta e="T29" id="Seg_7094" s="T28">this-LAT</ta>
            <ta e="T33" id="Seg_7095" s="T32">ten.[NOM.SG]</ta>
            <ta e="T34" id="Seg_7096" s="T33">two.[NOM.SG]</ta>
            <ta e="T35" id="Seg_7097" s="T34">year.[NOM.SG]</ta>
            <ta e="T36" id="Seg_7098" s="T35">and</ta>
            <ta e="T37" id="Seg_7099" s="T36">more</ta>
            <ta e="T38" id="Seg_7100" s="T37">two.[NOM.SG]</ta>
            <ta e="T40" id="Seg_7101" s="T39">how</ta>
            <ta e="T41" id="Seg_7102" s="T40">this.[NOM.SG]</ta>
            <ta e="T42" id="Seg_7103" s="T41">man.[NOM.SG]</ta>
            <ta e="T43" id="Seg_7104" s="T42">stingy.[NOM.SG]</ta>
            <ta e="T44" id="Seg_7105" s="T43">what.[NOM.SG]=INDEF</ta>
            <ta e="T45" id="Seg_7106" s="T44">NEG</ta>
            <ta e="T46" id="Seg_7107" s="T45">take-FUT.[3SG]</ta>
            <ta e="T47" id="Seg_7108" s="T46">what.[NOM.SG]=INDEF</ta>
            <ta e="T48" id="Seg_7109" s="T47">NEG</ta>
            <ta e="T49" id="Seg_7110" s="T48">give-FUT-3SG</ta>
            <ta e="T50" id="Seg_7111" s="T49">and</ta>
            <ta e="T51" id="Seg_7112" s="T50">another.[NOM.SG]</ta>
            <ta e="T52" id="Seg_7113" s="T51">man.[NOM.SG]</ta>
            <ta e="T53" id="Seg_7114" s="T52">very</ta>
            <ta e="T54" id="Seg_7115" s="T53">good.[NOM.SG]</ta>
            <ta e="T55" id="Seg_7116" s="T54">all</ta>
            <ta e="T56" id="Seg_7117" s="T55">give-MOM-FUT-3SG</ta>
            <ta e="T59" id="Seg_7118" s="T58">what.[NOM.SG]</ta>
            <ta e="T62" id="Seg_7119" s="T61">what.[NOM.SG]</ta>
            <ta e="T63" id="Seg_7120" s="T62">NEG</ta>
            <ta e="T69" id="Seg_7121" s="T67">what.[NOM.SG]</ta>
            <ta e="T70" id="Seg_7122" s="T69">what.[NOM.SG]</ta>
            <ta e="T71" id="Seg_7123" s="T70">NEG</ta>
            <ta e="T72" id="Seg_7124" s="T71">ask-FUT-2SG</ta>
            <ta e="T73" id="Seg_7125" s="T72">all</ta>
            <ta e="T74" id="Seg_7126" s="T73">give-FUT-3SG</ta>
            <ta e="T75" id="Seg_7127" s="T74">this-GEN</ta>
            <ta e="T76" id="Seg_7128" s="T75">very</ta>
            <ta e="T77" id="Seg_7129" s="T76">heart-NOM/GEN/ACC.3SG</ta>
            <ta e="T78" id="Seg_7130" s="T77">good.[NOM.SG]</ta>
            <ta e="T84" id="Seg_7131" s="T83">vodka.[NOM.SG]</ta>
            <ta e="T85" id="Seg_7132" s="T84">NEG</ta>
            <ta e="T86" id="Seg_7133" s="T85">drink-PRS.[3SG]</ta>
            <ta e="T87" id="Seg_7134" s="T86">what.[NOM.SG]</ta>
            <ta e="T88" id="Seg_7135" s="T87">be-PRS.[3SG]</ta>
            <ta e="T89" id="Seg_7136" s="T88">all</ta>
            <ta e="T90" id="Seg_7137" s="T89">give-FUT-3SG</ta>
            <ta e="T91" id="Seg_7138" s="T90">egg-PL</ta>
            <ta e="T92" id="Seg_7139" s="T91">give-PRS.[3SG]</ta>
            <ta e="T95" id="Seg_7140" s="T94">butter.[NOM.SG]</ta>
            <ta e="T96" id="Seg_7141" s="T95">give-PRS.[3SG]</ta>
            <ta e="T97" id="Seg_7142" s="T96">bread.[NOM.SG]</ta>
            <ta e="T98" id="Seg_7143" s="T97">give-PRS.[3SG]</ta>
            <ta e="T99" id="Seg_7144" s="T98">meat.[NOM.SG]</ta>
            <ta e="T100" id="Seg_7145" s="T99">be-PRS.[3SG]</ta>
            <ta e="T101" id="Seg_7146" s="T100">so</ta>
            <ta e="T102" id="Seg_7147" s="T101">give-FUT-3SG</ta>
            <ta e="T103" id="Seg_7148" s="T102">all</ta>
            <ta e="T104" id="Seg_7149" s="T103">what.[NOM.SG]</ta>
            <ta e="T105" id="Seg_7150" s="T104">be-PRS.[3SG]</ta>
            <ta e="T106" id="Seg_7151" s="T105">all</ta>
            <ta e="T107" id="Seg_7152" s="T106">give-FUT-3SG</ta>
            <ta e="T108" id="Seg_7153" s="T107">very</ta>
            <ta e="T111" id="Seg_7154" s="T110">%above</ta>
            <ta e="T112" id="Seg_7155" s="T111">rumble-PRS.[3SG]</ta>
            <ta e="T113" id="Seg_7156" s="T112">God.[NOM.SG]</ta>
            <ta e="T114" id="Seg_7157" s="T113">rumble-DUR.[3SG]</ta>
            <ta e="T116" id="Seg_7158" s="T115">who.[NOM.SG]=INDEF</ta>
            <ta e="T117" id="Seg_7159" s="T116">kill-FUT-3SG</ta>
            <ta e="T118" id="Seg_7160" s="T117">rainbow.[NOM.SG]</ta>
            <ta e="T119" id="Seg_7161" s="T118">stand-DUR.[3SG]</ta>
            <ta e="T120" id="Seg_7162" s="T119">water.[NOM.SG]</ta>
            <ta e="T121" id="Seg_7163" s="T120">take-DUR.[3SG]</ta>
            <ta e="T122" id="Seg_7164" s="T121">then</ta>
            <ta e="T123" id="Seg_7165" s="T122">rain.[NOM.SG]</ta>
            <ta e="T124" id="Seg_7166" s="T123">go-FUT-3SG</ta>
            <ta e="T125" id="Seg_7167" s="T124">hail.[NOM.SG]</ta>
            <ta e="T126" id="Seg_7168" s="T125">go-FUT-3SG</ta>
            <ta e="T127" id="Seg_7169" s="T126">very</ta>
            <ta e="T128" id="Seg_7170" s="T127">rain.cloud.[NOM.SG]</ta>
            <ta e="T129" id="Seg_7171" s="T128">black.[NOM.SG]</ta>
            <ta e="T130" id="Seg_7172" s="T129">come-PRS.[3SG]</ta>
            <ta e="T131" id="Seg_7173" s="T130">I.NOM</ta>
            <ta e="T132" id="Seg_7174" s="T131">outwards</ta>
            <ta e="T133" id="Seg_7175" s="T132">go-PST-1SG</ta>
            <ta e="T134" id="Seg_7176" s="T133">cuckoo.[NOM.SG]</ta>
            <ta e="T135" id="Seg_7177" s="T134">all</ta>
            <ta e="T136" id="Seg_7178" s="T135">fly-DUR.[3SG]</ta>
            <ta e="T137" id="Seg_7179" s="T136">look-FUT-1SG</ta>
            <ta e="T138" id="Seg_7180" s="T137">this.[NOM.SG]</ta>
            <ta e="T139" id="Seg_7181" s="T138">house-LAT</ta>
            <ta e="T140" id="Seg_7182" s="T139">sit.down-PST.[3SG]</ta>
            <ta e="T141" id="Seg_7183" s="T140">PTCL</ta>
            <ta e="T142" id="Seg_7184" s="T141">and</ta>
            <ta e="T143" id="Seg_7185" s="T142">shout-DUR.[3SG]</ta>
            <ta e="T144" id="Seg_7186" s="T143">I.NOM</ta>
            <ta e="T145" id="Seg_7187" s="T144">say-IPFVZ-1SG</ta>
            <ta e="T146" id="Seg_7188" s="T145">alone</ta>
            <ta e="T147" id="Seg_7189" s="T146">live-DUR</ta>
            <ta e="T148" id="Seg_7190" s="T147">live-FUT-1SG</ta>
            <ta e="T149" id="Seg_7191" s="T148">PTCL</ta>
            <ta e="T150" id="Seg_7192" s="T149">probably</ta>
            <ta e="T151" id="Seg_7193" s="T150">alone</ta>
            <ta e="T152" id="Seg_7194" s="T151">live-FUT-1SG</ta>
            <ta e="T153" id="Seg_7195" s="T152">stone-INS</ta>
            <ta e="T154" id="Seg_7196" s="T153">PTCL</ta>
            <ta e="T155" id="Seg_7197" s="T154">throw.away-PST.[3SG]</ta>
            <ta e="T156" id="Seg_7198" s="T155">tree-INS</ta>
            <ta e="T157" id="Seg_7199" s="T156">PTCL</ta>
            <ta e="T158" id="Seg_7200" s="T157">throw.away-PST.[3SG]</ta>
            <ta e="T159" id="Seg_7201" s="T158">this.[NOM.SG]</ta>
            <ta e="T160" id="Seg_7202" s="T159">always</ta>
            <ta e="T161" id="Seg_7203" s="T160">sit-DUR.[3SG]</ta>
            <ta e="T162" id="Seg_7204" s="T161">shout-DUR.[3SG]</ta>
            <ta e="T163" id="Seg_7205" s="T162">then</ta>
            <ta e="T164" id="Seg_7206" s="T163">fly-MOM-PST.[3SG]</ta>
            <ta e="T165" id="Seg_7207" s="T164">where</ta>
            <ta e="T166" id="Seg_7208" s="T165">sun.[NOM.SG]</ta>
            <ta e="T167" id="Seg_7209" s="T166">get.up-DUR.[3SG]</ta>
            <ta e="T168" id="Seg_7210" s="T167">very</ta>
            <ta e="T169" id="Seg_7211" s="T168">cunning-ADJZ.[NOM.SG]</ta>
            <ta e="T170" id="Seg_7212" s="T169">people.[NOM.SG]</ta>
            <ta e="T171" id="Seg_7213" s="T170">PTCL</ta>
            <ta e="T172" id="Seg_7214" s="T171">write-PST-3PL</ta>
            <ta e="T173" id="Seg_7215" s="T172">paper-LAT</ta>
            <ta e="T174" id="Seg_7216" s="T173">this.[NOM.SG]</ta>
            <ta e="T175" id="Seg_7217" s="T174">man.[NOM.SG]</ta>
            <ta e="T176" id="Seg_7218" s="T175">very</ta>
            <ta e="T177" id="Seg_7219" s="T176">NEG</ta>
            <ta e="T178" id="Seg_7220" s="T177">good.[NOM.SG]</ta>
            <ta e="T179" id="Seg_7221" s="T178">all</ta>
            <ta e="T180" id="Seg_7222" s="T179">what.[NOM.SG]</ta>
            <ta e="T181" id="Seg_7223" s="T180">take-DUR.[3SG]</ta>
            <ta e="T183" id="Seg_7224" s="T181">and</ta>
            <ta e="T184" id="Seg_7225" s="T183">all</ta>
            <ta e="T185" id="Seg_7226" s="T184">what.[NOM.SG]</ta>
            <ta e="T186" id="Seg_7227" s="T185">take-DUR.[3SG]</ta>
            <ta e="T187" id="Seg_7228" s="T186">all</ta>
            <ta e="T188" id="Seg_7229" s="T187">very</ta>
            <ta e="T189" id="Seg_7230" s="T188">thief.[NOM.SG]</ta>
            <ta e="T190" id="Seg_7231" s="T189">man.[NOM.SG]</ta>
            <ta e="T191" id="Seg_7232" s="T190">all</ta>
            <ta e="T192" id="Seg_7233" s="T191">what.[NOM.SG]</ta>
            <ta e="T193" id="Seg_7234" s="T192">take-PRS.[3SG]</ta>
            <ta e="T195" id="Seg_7235" s="T193">so</ta>
            <ta e="T196" id="Seg_7236" s="T195">all</ta>
            <ta e="T197" id="Seg_7237" s="T196">take-DUR.[3SG]</ta>
            <ta e="T198" id="Seg_7238" s="T197">all</ta>
            <ta e="T199" id="Seg_7239" s="T198">bring-TR-DUR.[3SG]</ta>
            <ta e="T200" id="Seg_7240" s="T199">tent-LAT/LOC.3SG</ta>
            <ta e="T201" id="Seg_7241" s="T200">I.NOM</ta>
            <ta e="T202" id="Seg_7242" s="T201">today</ta>
            <ta e="T203" id="Seg_7243" s="T202">daughter-GEN.1SG</ta>
            <ta e="T204" id="Seg_7244" s="T203">scarf-EP-ACC</ta>
            <ta e="T205" id="Seg_7245" s="T204">take-PST-1SG</ta>
            <ta e="T206" id="Seg_7246" s="T205">this.[NOM.SG]</ta>
            <ta e="T207" id="Seg_7247" s="T206">go-PST.[3SG]</ta>
            <ta e="T208" id="Seg_7248" s="T207">Malinovka-LAT</ta>
            <ta e="T209" id="Seg_7249" s="T208">study-INF.LAT</ta>
            <ta e="T210" id="Seg_7250" s="T209">there</ta>
            <ta e="T211" id="Seg_7251" s="T210">woman.[NOM.SG]</ta>
            <ta e="T212" id="Seg_7252" s="T211">ask-DUR.[3SG]</ta>
            <ta e="T213" id="Seg_7253" s="T212">where</ta>
            <ta e="T214" id="Seg_7254" s="T213">you.NOM</ta>
            <ta e="T215" id="Seg_7255" s="T214">scarf.[NOM.SG]</ta>
            <ta e="T216" id="Seg_7256" s="T215">take-PST-2SG</ta>
            <ta e="T217" id="Seg_7257" s="T216">grandmother.[NOM.SG]</ta>
            <ta e="T218" id="Seg_7258" s="T217">give-PST.[3SG]</ta>
            <ta e="T219" id="Seg_7259" s="T218">and</ta>
            <ta e="T220" id="Seg_7260" s="T219">grandmother.[NOM.SG]</ta>
            <ta e="T221" id="Seg_7261" s="T220">where</ta>
            <ta e="T222" id="Seg_7262" s="T221">take-PST.[3SG]</ta>
            <ta e="T223" id="Seg_7263" s="T222">Polya.[NOM.SG]</ta>
            <ta e="T224" id="Seg_7264" s="T223">give-PST.[3SG]</ta>
            <ta e="T225" id="Seg_7265" s="T224">that.[NOM.SG]</ta>
            <ta e="T226" id="Seg_7266" s="T225">horse.[NOM.SG]</ta>
            <ta e="T227" id="Seg_7267" s="T226">take-PST-1SG</ta>
            <ta e="T228" id="Seg_7268" s="T227">many</ta>
            <ta e="T229" id="Seg_7269" s="T228">money.[NOM.SG]</ta>
            <ta e="T230" id="Seg_7270" s="T229">give-PST-1SG</ta>
            <ta e="T231" id="Seg_7271" s="T230">and</ta>
            <ta e="T232" id="Seg_7272" s="T231">another.[NOM.SG]</ta>
            <ta e="T233" id="Seg_7273" s="T232">horse.[NOM.SG]</ta>
            <ta e="T234" id="Seg_7274" s="T233">take-PST-1SG</ta>
            <ta e="T235" id="Seg_7275" s="T234">few</ta>
            <ta e="T236" id="Seg_7276" s="T235">give-PST-1SG</ta>
            <ta e="T238" id="Seg_7277" s="T237">capture-PST-1SG</ta>
            <ta e="T241" id="Seg_7278" s="T240">calf.[NOM.SG]</ta>
            <ta e="T243" id="Seg_7279" s="T241">PTCL</ta>
            <ta e="T247" id="Seg_7280" s="T246">NEG</ta>
            <ta e="T248" id="Seg_7281" s="T247">walk-PRS.[3SG]</ta>
            <ta e="T249" id="Seg_7282" s="T248">I.NOM</ta>
            <ta e="T251" id="Seg_7283" s="T249">PTCL</ta>
            <ta e="T257" id="Seg_7284" s="T256">very</ta>
            <ta e="T258" id="Seg_7285" s="T257">powerful.[NOM.SG]</ta>
            <ta e="T259" id="Seg_7286" s="T258">NEG</ta>
            <ta e="T260" id="Seg_7287" s="T259">can-PRS-1SG</ta>
            <ta e="T261" id="Seg_7288" s="T260">bring-INF.LAT</ta>
            <ta e="T262" id="Seg_7289" s="T261">PTCL</ta>
            <ta e="T267" id="Seg_7290" s="T266">go-IPFVZ-PRS-1SG</ta>
            <ta e="T268" id="Seg_7291" s="T267">PTCL</ta>
            <ta e="T270" id="Seg_7292" s="T269">NEG</ta>
            <ta e="T271" id="Seg_7293" s="T270">good.[NOM.SG]</ta>
            <ta e="T272" id="Seg_7294" s="T271">boy.[NOM.SG]</ta>
            <ta e="T273" id="Seg_7295" s="T272">boy.[NOM.SG]</ta>
            <ta e="T274" id="Seg_7296" s="T273">always</ta>
            <ta e="T275" id="Seg_7297" s="T274">PTCL</ta>
            <ta e="T276" id="Seg_7298" s="T275">steal-DUR.[3SG]</ta>
            <ta e="T277" id="Seg_7299" s="T276">and</ta>
            <ta e="T278" id="Seg_7300" s="T277">bring-TR-DUR.[3SG]</ta>
            <ta e="T279" id="Seg_7301" s="T278">I.NOM</ta>
            <ta e="T280" id="Seg_7302" s="T279">forest-LOC</ta>
            <ta e="T281" id="Seg_7303" s="T280">go-PST-1SG</ta>
            <ta e="T282" id="Seg_7304" s="T281">what.[NOM.SG]=INDEF</ta>
            <ta e="T285" id="Seg_7305" s="T284">see-PST-1SG</ta>
            <ta e="T286" id="Seg_7306" s="T285">then</ta>
            <ta e="T287" id="Seg_7307" s="T286">I.NOM</ta>
            <ta e="T288" id="Seg_7308" s="T287">frighten-FRQ-MOM-PST-1SG</ta>
            <ta e="T289" id="Seg_7309" s="T288">PTCL</ta>
            <ta e="T290" id="Seg_7310" s="T289">then</ta>
            <ta e="T293" id="Seg_7311" s="T292">God.[NOM.SG]</ta>
            <ta e="T294" id="Seg_7312" s="T293">call-PST-1SG</ta>
            <ta e="T295" id="Seg_7313" s="T294">PTCL</ta>
            <ta e="T296" id="Seg_7314" s="T295">go-CVB</ta>
            <ta e="T297" id="Seg_7315" s="T296">disappear-PST-3PL</ta>
            <ta e="T298" id="Seg_7316" s="T297">one.should</ta>
            <ta e="T299" id="Seg_7317" s="T298">hole-NOM/GEN/ACC.3PL</ta>
            <ta e="T300" id="Seg_7318" s="T299">drill-INF.LAT</ta>
            <ta e="T301" id="Seg_7319" s="T300">one.should</ta>
            <ta e="T302" id="Seg_7320" s="T301">gate.[NOM.SG]</ta>
            <ta e="T303" id="Seg_7321" s="T302">make-INF.LAT</ta>
            <ta e="T305" id="Seg_7322" s="T304">this</ta>
            <ta e="T310" id="Seg_7323" s="T309">this</ta>
            <ta e="T311" id="Seg_7324" s="T310">hole-PL-LAT</ta>
            <ta e="T312" id="Seg_7325" s="T311">one.should</ta>
            <ta e="T313" id="Seg_7326" s="T312">tree-PL</ta>
            <ta e="T314" id="Seg_7327" s="T313">creep.into-INF.LAT</ta>
            <ta e="T315" id="Seg_7328" s="T314">then</ta>
            <ta e="T316" id="Seg_7329" s="T315">corral.[NOM.SG]</ta>
            <ta e="T317" id="Seg_7330" s="T316">close-FUT-1DU</ta>
            <ta e="T318" id="Seg_7331" s="T317">then</ta>
            <ta e="T319" id="Seg_7332" s="T318">calf.[NOM.SG]</ta>
            <ta e="T320" id="Seg_7333" s="T319">NEG</ta>
            <ta e="T321" id="Seg_7334" s="T320">run-FUT-3SG</ta>
            <ta e="T322" id="Seg_7335" s="T321">corral-LOC</ta>
            <ta e="T325" id="Seg_7336" s="T324">corral-LOC</ta>
            <ta e="T326" id="Seg_7337" s="T325">stand-DUR.[3SG]</ta>
            <ta e="T338" id="Seg_7338" s="T337">what-INS=INDEF</ta>
            <ta e="T339" id="Seg_7339" s="T338">PTCL</ta>
            <ta e="T340" id="Seg_7340" s="T339">smell-PRS.[3SG]</ta>
            <ta e="T341" id="Seg_7341" s="T340">you.GEN</ta>
            <ta e="T342" id="Seg_7342" s="T341">nose-NOM/GEN/ACC.2SG</ta>
            <ta e="T344" id="Seg_7343" s="T342">very</ta>
            <ta e="T347" id="Seg_7344" s="T346">you.GEN</ta>
            <ta e="T348" id="Seg_7345" s="T347">nose-NOM/GEN/ACC.2SG</ta>
            <ta e="T349" id="Seg_7346" s="T348">very</ta>
            <ta e="T350" id="Seg_7347" s="T349">sharp</ta>
            <ta e="T351" id="Seg_7348" s="T350">all</ta>
            <ta e="T352" id="Seg_7349" s="T351">listen-DUR.[3SG]</ta>
            <ta e="T353" id="Seg_7350" s="T352">you.NOM</ta>
            <ta e="T354" id="Seg_7351" s="T353">where</ta>
            <ta e="T355" id="Seg_7352" s="T354">be-PST-2SG</ta>
            <ta e="T356" id="Seg_7353" s="T355">water-VBLZ-CVB</ta>
            <ta e="T357" id="Seg_7354" s="T356">go-PST-1SG</ta>
            <ta e="T358" id="Seg_7355" s="T357">one.should</ta>
            <ta e="T359" id="Seg_7356" s="T358">yellow.[NOM.SG]</ta>
            <ta e="T360" id="Seg_7357" s="T359">water.[NOM.SG]</ta>
            <ta e="T361" id="Seg_7358" s="T360">boil-INF.LAT</ta>
            <ta e="T362" id="Seg_7359" s="T361">one.should</ta>
            <ta e="T363" id="Seg_7360" s="T362">soup.[NOM.SG]</ta>
            <ta e="T366" id="Seg_7361" s="T365">boil-INF.LAT</ta>
            <ta e="T367" id="Seg_7362" s="T366">man-PL</ta>
            <ta e="T368" id="Seg_7363" s="T367">feed-INF.LAT</ta>
            <ta e="T369" id="Seg_7364" s="T368">I.NOM</ta>
            <ta e="T370" id="Seg_7365" s="T369">ten.[NOM.SG]</ta>
            <ta e="T371" id="Seg_7366" s="T370">money.[NOM.SG]</ta>
            <ta e="T372" id="Seg_7367" s="T371">be-PRS.[3SG]</ta>
            <ta e="T373" id="Seg_7368" s="T372">one.should</ta>
            <ta e="T374" id="Seg_7369" s="T373">you.DAT</ta>
            <ta e="T375" id="Seg_7370" s="T374">five.[NOM.SG]</ta>
            <ta e="T376" id="Seg_7371" s="T375">give-INF.LAT</ta>
            <ta e="T377" id="Seg_7372" s="T376">five.[NOM.SG]</ta>
            <ta e="T378" id="Seg_7373" s="T377">self-LAT/LOC.3SG</ta>
            <ta e="T379" id="Seg_7374" s="T378">remain-INF.LAT</ta>
            <ta e="T380" id="Seg_7375" s="T379">maybe</ta>
            <ta e="T381" id="Seg_7376" s="T380">where.to=INDEF</ta>
            <ta e="T382" id="Seg_7377" s="T381">go-FUT-2SG</ta>
            <ta e="T383" id="Seg_7378" s="T382">money.[NOM.SG]</ta>
            <ta e="T384" id="Seg_7379" s="T383">one.needs</ta>
            <ta e="T385" id="Seg_7380" s="T384">become-FUT-3SG</ta>
            <ta e="T387" id="Seg_7381" s="T386">Matveev.[NOM.SG]</ta>
            <ta e="T388" id="Seg_7382" s="T387">PTCL</ta>
            <ta e="T389" id="Seg_7383" s="T388">sit-DUR.[3SG]</ta>
            <ta e="T390" id="Seg_7384" s="T389">NEG</ta>
            <ta e="T391" id="Seg_7385" s="T390">speak-PRS.[3SG]</ta>
            <ta e="T392" id="Seg_7386" s="T391">heart-NOM/GEN/ACC.3SG</ta>
            <ta e="T393" id="Seg_7387" s="T392">hurt-PRS.[3SG]</ta>
            <ta e="T394" id="Seg_7388" s="T393">tent-LAT-2SG</ta>
            <ta e="T395" id="Seg_7389" s="T394">paper.[NOM.SG]</ta>
            <ta e="T396" id="Seg_7390" s="T395">NEG.EX.[3SG]</ta>
            <ta e="T397" id="Seg_7391" s="T396">woman-NOM/GEN.3SG</ta>
            <ta e="T398" id="Seg_7392" s="T397">PTCL</ta>
            <ta e="T399" id="Seg_7393" s="T398">be.angry-MOM-PST.[3SG]</ta>
            <ta e="T400" id="Seg_7394" s="T399">NEG</ta>
            <ta e="T401" id="Seg_7395" s="T400">write-PRS.[3SG]</ta>
            <ta e="T404" id="Seg_7396" s="T403">paper.[NOM.SG]</ta>
            <ta e="T405" id="Seg_7397" s="T404">NEG.AUX-IMP.2SG</ta>
            <ta e="T406" id="Seg_7398" s="T405">be.angry-CNG</ta>
            <ta e="T410" id="Seg_7399" s="T409">soon</ta>
            <ta e="T411" id="Seg_7400" s="T410">tent-LAT-%%</ta>
            <ta e="T412" id="Seg_7401" s="T411">come-FUT-1SG</ta>
            <ta e="T413" id="Seg_7402" s="T412">what=INDEF</ta>
            <ta e="T414" id="Seg_7403" s="T413">you.DAT</ta>
            <ta e="T415" id="Seg_7404" s="T414">bring-FUT-1SG</ta>
            <ta e="T416" id="Seg_7405" s="T415">strongly</ta>
            <ta e="T417" id="Seg_7406" s="T416">NEG</ta>
            <ta e="T418" id="Seg_7407" s="T417">speak-EP-IMP.2SG</ta>
            <ta e="T419" id="Seg_7408" s="T418">softly</ta>
            <ta e="T420" id="Seg_7409" s="T419">speak-EP-IMP.2SG</ta>
            <ta e="T421" id="Seg_7410" s="T420">otherwise</ta>
            <ta e="T422" id="Seg_7411" s="T421">all</ta>
            <ta e="T423" id="Seg_7412" s="T422">people.[NOM.SG]</ta>
            <ta e="T424" id="Seg_7413" s="T423">hear-PRS-3PL</ta>
            <ta e="T425" id="Seg_7414" s="T424">strongly</ta>
            <ta e="T426" id="Seg_7415" s="T425">NEG.AUX-IMP.2SG</ta>
            <ta e="T427" id="Seg_7416" s="T426">shout-EP-CNG</ta>
            <ta e="T428" id="Seg_7417" s="T427">and</ta>
            <ta e="T429" id="Seg_7418" s="T428">NEG.AUX-IMP.2SG</ta>
            <ta e="T430" id="Seg_7419" s="T429">you.NOM</ta>
            <ta e="T431" id="Seg_7420" s="T430">NEG.AUX-IMP.2SG</ta>
            <ta e="T432" id="Seg_7421" s="T431">scold-DES-CNG</ta>
            <ta e="T433" id="Seg_7422" s="T432">otherwise</ta>
            <ta e="T434" id="Seg_7423" s="T433">people.[NOM.SG]</ta>
            <ta e="T435" id="Seg_7424" s="T434">all</ta>
            <ta e="T436" id="Seg_7425" s="T435">hear-DUR-3PL</ta>
            <ta e="T437" id="Seg_7426" s="T436">how</ta>
            <ta e="T438" id="Seg_7427" s="T437">you.GEN</ta>
            <ta e="T439" id="Seg_7428" s="T438">you.GEN</ta>
            <ta e="T440" id="Seg_7429" s="T439">scold-DES-DUR-2SG</ta>
            <ta e="T441" id="Seg_7430" s="T440">I.NOM</ta>
            <ta e="T442" id="Seg_7431" s="T441">today</ta>
            <ta e="T443" id="Seg_7432" s="T442">cream.[NOM.SG]</ta>
            <ta e="T444" id="Seg_7433" s="T443">pour-PST-1SG</ta>
            <ta e="T445" id="Seg_7434" s="T444">make.butter-PST-1SG</ta>
            <ta e="T446" id="Seg_7435" s="T445">milk.pot-PL</ta>
            <ta e="T447" id="Seg_7436" s="T446">wash.oneself-TR-PST-1SG</ta>
            <ta e="T448" id="Seg_7437" s="T447">wait-PST-1SG</ta>
            <ta e="T449" id="Seg_7438" s="T448">sun.[NOM.SG]</ta>
            <ta e="T450" id="Seg_7439" s="T449">dry-DUR.[3SG]</ta>
            <ta e="T451" id="Seg_7440" s="T450">all</ta>
            <ta e="T452" id="Seg_7441" s="T451">what.[NOM.SG]</ta>
            <ta e="T453" id="Seg_7442" s="T452">bring-PST-2SG</ta>
            <ta e="T454" id="Seg_7443" s="T453">show-IMP.2SG.O</ta>
            <ta e="T455" id="Seg_7444" s="T454">I.LAT</ta>
            <ta e="T458" id="Seg_7445" s="T457">what.[NOM.SG]</ta>
            <ta e="T459" id="Seg_7446" s="T458">there</ta>
            <ta e="T460" id="Seg_7447" s="T459">lie.down-DUR.[3SG]</ta>
            <ta e="T461" id="Seg_7448" s="T460">I.NOM</ta>
            <ta e="T462" id="Seg_7449" s="T461">today</ta>
            <ta e="T463" id="Seg_7450" s="T462">bear.leek-VBLZ-CVB</ta>
            <ta e="T464" id="Seg_7451" s="T463">go-PST-1SG</ta>
            <ta e="T465" id="Seg_7452" s="T464">bear.leek.[NOM.SG]</ta>
            <ta e="T466" id="Seg_7453" s="T465">bring-PST-1SG</ta>
            <ta e="T467" id="Seg_7454" s="T466">then</ta>
            <ta e="T468" id="Seg_7455" s="T467">stab-FUT-1SG</ta>
            <ta e="T469" id="Seg_7456" s="T468">egg-PL</ta>
            <ta e="T471" id="Seg_7457" s="T470">put-FUT-1SG</ta>
            <ta e="T472" id="Seg_7458" s="T471">cream.[NOM.SG]</ta>
            <ta e="T473" id="Seg_7459" s="T472">put-FUT-1SG</ta>
            <ta e="T474" id="Seg_7460" s="T473">then</ta>
            <ta e="T475" id="Seg_7461" s="T474">bake-INF.LAT</ta>
            <ta e="T476" id="Seg_7462" s="T475">pie-PL</ta>
            <ta e="T477" id="Seg_7463" s="T476">can-FUT-1SG</ta>
            <ta e="T479" id="Seg_7464" s="T478">meat.[NOM.SG]</ta>
            <ta e="T480" id="Seg_7465" s="T479">boil-PST-1SG</ta>
            <ta e="T481" id="Seg_7466" s="T480">meat.[NOM.SG]</ta>
            <ta e="T482" id="Seg_7467" s="T481">eat-PST-3PL</ta>
            <ta e="T483" id="Seg_7468" s="T482">%%-INF.LAT</ta>
            <ta e="T484" id="Seg_7469" s="T483">single.[NOM.SG]</ta>
            <ta e="T485" id="Seg_7470" s="T484">bone.[NOM.SG]</ta>
            <ta e="T486" id="Seg_7471" s="T485">remain-MOM-PST.[3SG]</ta>
            <ta e="T490" id="Seg_7472" s="T489">dog-PL</ta>
            <ta e="T491" id="Seg_7473" s="T490">PTCL</ta>
            <ta e="T492" id="Seg_7474" s="T491">bone.[NOM.SG]</ta>
            <ta e="T493" id="Seg_7475" s="T492">eat-DUR-3PL</ta>
            <ta e="T494" id="Seg_7476" s="T493">very</ta>
            <ta e="T495" id="Seg_7477" s="T494">many</ta>
            <ta e="T496" id="Seg_7478" s="T495">bone.[NOM.SG]</ta>
            <ta e="T497" id="Seg_7479" s="T496">throw.away-PST-1PL</ta>
            <ta e="T498" id="Seg_7480" s="T497">I.NOM</ta>
            <ta e="T501" id="Seg_7481" s="T500">I.NOM</ta>
            <ta e="T502" id="Seg_7482" s="T501">today</ta>
            <ta e="T503" id="Seg_7483" s="T502">bread.[NOM.SG]</ta>
            <ta e="T504" id="Seg_7484" s="T503">bake-PST-1SG</ta>
            <ta e="T505" id="Seg_7485" s="T504">NEG</ta>
            <ta e="T506" id="Seg_7486" s="T505">sour.[NOM.SG]</ta>
            <ta e="T507" id="Seg_7487" s="T506">NEG</ta>
            <ta e="T508" id="Seg_7488" s="T507">good.[NOM.SG]</ta>
            <ta e="T509" id="Seg_7489" s="T508">PTCL</ta>
            <ta e="T510" id="Seg_7490" s="T509">I.NOM</ta>
            <ta e="T511" id="Seg_7491" s="T510">this.[NOM.SG]</ta>
            <ta e="T512" id="Seg_7492" s="T511">bread.[NOM.SG]</ta>
            <ta e="T513" id="Seg_7493" s="T512">this.[NOM.SG]</ta>
            <ta e="T516" id="Seg_7494" s="T515">dog-PL-LAT</ta>
            <ta e="T517" id="Seg_7495" s="T516">give-PST-1SG</ta>
            <ta e="T518" id="Seg_7496" s="T517">dog-PL</ta>
            <ta e="T519" id="Seg_7497" s="T518">PTCL</ta>
            <ta e="T520" id="Seg_7498" s="T519">bread.[NOM.SG]</ta>
            <ta e="T521" id="Seg_7499" s="T520">eat-DUR-3PL</ta>
            <ta e="T522" id="Seg_7500" s="T521">and</ta>
            <ta e="T523" id="Seg_7501" s="T522">PTCL</ta>
            <ta e="T524" id="Seg_7502" s="T523">fight-DUR-3PL</ta>
            <ta e="T525" id="Seg_7503" s="T524">dog.[NOM.SG]</ta>
            <ta e="T526" id="Seg_7504" s="T525">PTCL</ta>
            <ta e="T527" id="Seg_7505" s="T526">this.[NOM.SG]</ta>
            <ta e="T528" id="Seg_7506" s="T527">dog-NOM/GEN/ACC.3SG</ta>
            <ta e="T529" id="Seg_7507" s="T528">know-3SG.O</ta>
            <ta e="T530" id="Seg_7508" s="T529">PTCL</ta>
            <ta e="T531" id="Seg_7509" s="T530">evening</ta>
            <ta e="T532" id="Seg_7510" s="T531">be-PST.[3SG]</ta>
            <ta e="T533" id="Seg_7511" s="T532">evening</ta>
            <ta e="T534" id="Seg_7512" s="T533">be-PST.[3SG]</ta>
            <ta e="T536" id="Seg_7513" s="T534">very</ta>
            <ta e="T540" id="Seg_7514" s="T539">what.[NOM.SG]=INDEF</ta>
            <ta e="T541" id="Seg_7515" s="T540">NEG</ta>
            <ta e="T542" id="Seg_7516" s="T541">be.visible-PRS.[3SG]</ta>
            <ta e="T543" id="Seg_7517" s="T542">I.NOM</ta>
            <ta e="T544" id="Seg_7518" s="T543">this-ACC</ta>
            <ta e="T545" id="Seg_7519" s="T544">NEG</ta>
            <ta e="T546" id="Seg_7520" s="T545">know-PST-1SG</ta>
            <ta e="T547" id="Seg_7521" s="T546">who.[NOM.SG]</ta>
            <ta e="T548" id="Seg_7522" s="T547">come-PRS.[3SG]</ta>
            <ta e="T549" id="Seg_7523" s="T548">I.NOM</ta>
            <ta e="T550" id="Seg_7524" s="T549">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T551" id="Seg_7525" s="T550">birchbark.vessel-PL</ta>
            <ta e="T552" id="Seg_7526" s="T551">make-PST.[3SG]</ta>
            <ta e="T553" id="Seg_7527" s="T552">skin.[NOM.SG]</ta>
            <ta e="T554" id="Seg_7528" s="T553">skin.[NOM.SG]</ta>
            <ta e="T555" id="Seg_7529" s="T554">peel.bark</ta>
            <ta e="T556" id="Seg_7530" s="T555">peel.bark-PST.[3SG]</ta>
            <ta e="T557" id="Seg_7531" s="T556">and</ta>
            <ta e="T558" id="Seg_7532" s="T557">make-DUR-PST.[3SG]</ta>
            <ta e="T559" id="Seg_7533" s="T558">birchbark.vessel.[NOM.SG]</ta>
            <ta e="T560" id="Seg_7534" s="T559">sew-PST-3PL</ta>
            <ta e="T561" id="Seg_7535" s="T560">tendon-PL-INS</ta>
            <ta e="T562" id="Seg_7536" s="T561">PTCL</ta>
            <ta e="T563" id="Seg_7537" s="T562">needle-INS</ta>
            <ta e="T564" id="Seg_7538" s="T563">and</ta>
            <ta e="T565" id="Seg_7539" s="T564">thimble.[NOM.SG]</ta>
            <ta e="T566" id="Seg_7540" s="T565">finger-LOC</ta>
            <ta e="T567" id="Seg_7541" s="T566">be-PST.[3SG]</ta>
            <ta e="T569" id="Seg_7542" s="T567">birchbark.vessel-PL</ta>
            <ta e="T570" id="Seg_7543" s="T569">sew-PST-3PL</ta>
            <ta e="T571" id="Seg_7544" s="T570">PTCL</ta>
            <ta e="T572" id="Seg_7545" s="T571">tendon-PL-INS</ta>
            <ta e="T573" id="Seg_7546" s="T572">and</ta>
            <ta e="T576" id="Seg_7547" s="T575">thimble.[NOM.SG]</ta>
            <ta e="T577" id="Seg_7548" s="T576">dress-PST.[3SG]</ta>
            <ta e="T578" id="Seg_7549" s="T577">finger-LAT</ta>
            <ta e="T579" id="Seg_7550" s="T578">and</ta>
            <ta e="T580" id="Seg_7551" s="T579">needle.[NOM.SG]</ta>
            <ta e="T581" id="Seg_7552" s="T580">take-PST.[3SG]</ta>
            <ta e="T582" id="Seg_7553" s="T581">sew-PST.[3SG]</ta>
            <ta e="T593" id="Seg_7554" s="T592">finger-ACC.3SG</ta>
            <ta e="T594" id="Seg_7555" s="T593">PTCL</ta>
            <ta e="T595" id="Seg_7556" s="T594">push-FUT-3SG</ta>
            <ta e="T596" id="Seg_7557" s="T595">you.GEN</ta>
            <ta e="T597" id="Seg_7558" s="T596">PTCL</ta>
            <ta e="T598" id="Seg_7559" s="T597">woman-NOM/GEN/ACC.2SG</ta>
            <ta e="T599" id="Seg_7560" s="T598">pity-DUR-2SG</ta>
            <ta e="T600" id="Seg_7561" s="T599">heart-NOM/GEN/ACC.3SG</ta>
            <ta e="T601" id="Seg_7562" s="T600">hurt-PRS.[3SG]</ta>
            <ta e="T602" id="Seg_7563" s="T601">very</ta>
            <ta e="T603" id="Seg_7564" s="T602">I.NOM</ta>
            <ta e="T604" id="Seg_7565" s="T603">PTCL</ta>
            <ta e="T605" id="Seg_7566" s="T604">house-NOM/GEN/ACC.1SG</ta>
            <ta e="T606" id="Seg_7567" s="T605">black.[NOM.SG]</ta>
            <ta e="T609" id="Seg_7568" s="T608">tomorrow</ta>
            <ta e="T610" id="Seg_7569" s="T609">wash-FUT-1SG</ta>
            <ta e="T611" id="Seg_7570" s="T610">very</ta>
            <ta e="T612" id="Seg_7571" s="T611">dirty.[NOM.SG]</ta>
            <ta e="T613" id="Seg_7572" s="T612">otherwise</ta>
            <ta e="T614" id="Seg_7573" s="T613">people.[NOM.SG]</ta>
            <ta e="T615" id="Seg_7574" s="T614">PTCL</ta>
            <ta e="T616" id="Seg_7575" s="T615">say-DUR-3PL</ta>
            <ta e="T617" id="Seg_7576" s="T616">say-PRS-3PL</ta>
            <ta e="T618" id="Seg_7577" s="T617">lazy.[NOM.SG]</ta>
            <ta e="T619" id="Seg_7578" s="T618">woman.[NOM.SG]</ta>
            <ta e="T620" id="Seg_7579" s="T619">house.[NOM.SG]</ta>
            <ta e="T621" id="Seg_7580" s="T620">NEG</ta>
            <ta e="T622" id="Seg_7581" s="T621">wash-PRS.[3SG]</ta>
            <ta e="T627" id="Seg_7582" s="T626">what.[NOM.SG]</ta>
            <ta e="T628" id="Seg_7583" s="T627">you.DAT</ta>
            <ta e="T629" id="Seg_7584" s="T628">speak-INF.LAT</ta>
            <ta e="T630" id="Seg_7585" s="T629">I.NOM</ta>
            <ta e="T631" id="Seg_7586" s="T630">all</ta>
            <ta e="T632" id="Seg_7587" s="T631">forget-PST-1SG</ta>
            <ta e="T633" id="Seg_7588" s="T632">today</ta>
            <ta e="T634" id="Seg_7589" s="T633">sleep-INF.LAT</ta>
            <ta e="T635" id="Seg_7590" s="T634">bedbug-PL</ta>
            <ta e="T636" id="Seg_7591" s="T635">NEG</ta>
            <ta e="T637" id="Seg_7592" s="T636">give-PST-3PL</ta>
            <ta e="T638" id="Seg_7593" s="T637">this.[NOM.SG]</ta>
            <ta e="T639" id="Seg_7594" s="T638">man.[NOM.SG]</ta>
            <ta e="T640" id="Seg_7595" s="T639">flour.[NOM.SG]</ta>
            <ta e="T641" id="Seg_7596" s="T640">ask-PST.[3SG]</ta>
            <ta e="T642" id="Seg_7597" s="T641">one.should</ta>
            <ta e="T643" id="Seg_7598" s="T642">this-LAT</ta>
            <ta e="T644" id="Seg_7599" s="T643">give-INF.LAT</ta>
            <ta e="T645" id="Seg_7600" s="T644">this-GEN</ta>
            <ta e="T646" id="Seg_7601" s="T645">bread.[NOM.SG]</ta>
            <ta e="T647" id="Seg_7602" s="T646">NEG.EX.[3SG]</ta>
            <ta e="T648" id="Seg_7603" s="T647">eat-INF.LAT</ta>
            <ta e="T649" id="Seg_7604" s="T648">all</ta>
            <ta e="T656" id="Seg_7605" s="T655">where</ta>
            <ta e="T657" id="Seg_7606" s="T656">take-INF.LAT</ta>
            <ta e="T658" id="Seg_7607" s="T657">what.[NOM.SG]</ta>
            <ta e="T659" id="Seg_7608" s="T658">speak-INF.LAT</ta>
            <ta e="T660" id="Seg_7609" s="T659">this-LAT</ta>
            <ta e="T661" id="Seg_7610" s="T660">very</ta>
            <ta e="T664" id="Seg_7611" s="T663">many</ta>
            <ta e="T665" id="Seg_7612" s="T664">one.should</ta>
            <ta e="T666" id="Seg_7613" s="T665">speak-INF.LAT</ta>
            <ta e="T670" id="Seg_7614" s="T668">one.should</ta>
            <ta e="T671" id="Seg_7615" s="T670">go-INF.LAT</ta>
            <ta e="T672" id="Seg_7616" s="T671">look-FRQ-INF.LAT</ta>
            <ta e="T673" id="Seg_7617" s="T672">where=INDEF</ta>
            <ta e="T674" id="Seg_7618" s="T673">speak-INF.LAT</ta>
            <ta e="T675" id="Seg_7619" s="T674">I.NOM</ta>
            <ta e="T676" id="Seg_7620" s="T675">yesterday</ta>
            <ta e="T677" id="Seg_7621" s="T676">long.time</ta>
            <ta e="T678" id="Seg_7622" s="T677">NEG</ta>
            <ta e="T681" id="Seg_7623" s="T680">lie.down-PST-1SG</ta>
            <ta e="T682" id="Seg_7624" s="T681">sleep-INF.LAT</ta>
            <ta e="T685" id="Seg_7625" s="T684">long.time</ta>
            <ta e="T686" id="Seg_7626" s="T685">NEG</ta>
            <ta e="T688" id="Seg_7627" s="T687">NEG</ta>
            <ta e="T690" id="Seg_7628" s="T689">NEG</ta>
            <ta e="T691" id="Seg_7629" s="T690">lie.down-FUT-1SG</ta>
            <ta e="T692" id="Seg_7630" s="T691">sleep-INF.LAT</ta>
            <ta e="T697" id="Seg_7631" s="T696">sleep-PST-1SG</ta>
            <ta e="T698" id="Seg_7632" s="T697">what.[NOM.SG]=INDEF</ta>
            <ta e="T699" id="Seg_7633" s="T698">NEG</ta>
            <ta e="T700" id="Seg_7634" s="T699">see-PST-1SG</ta>
            <ta e="T701" id="Seg_7635" s="T700">this.[NOM.SG]</ta>
            <ta e="T702" id="Seg_7636" s="T701">man-LOC</ta>
            <ta e="T703" id="Seg_7637" s="T702">hair-NOM/GEN.3SG</ta>
            <ta e="T704" id="Seg_7638" s="T703">all</ta>
            <ta e="T705" id="Seg_7639" s="T704">black.[NOM.SG]</ta>
            <ta e="T706" id="Seg_7640" s="T705">be-PST.[3SG]</ta>
            <ta e="T707" id="Seg_7641" s="T706">now</ta>
            <ta e="T708" id="Seg_7642" s="T707">all</ta>
            <ta e="T709" id="Seg_7643" s="T708">very</ta>
            <ta e="T710" id="Seg_7644" s="T709">white.[NOM.SG]</ta>
            <ta e="T711" id="Seg_7645" s="T710">become-RES-PST-3PL</ta>
            <ta e="T712" id="Seg_7646" s="T711">hair.[NOM.SG]</ta>
            <ta e="T713" id="Seg_7647" s="T712">single.[NOM.SG]</ta>
            <ta e="T714" id="Seg_7648" s="T713">hand.[NOM.SG]</ta>
            <ta e="T715" id="Seg_7649" s="T714">be-PRS.[3SG]</ta>
            <ta e="T716" id="Seg_7650" s="T715">and</ta>
            <ta e="T717" id="Seg_7651" s="T716">single.[NOM.SG]</ta>
            <ta e="T718" id="Seg_7652" s="T717">hand.[NOM.SG]</ta>
            <ta e="T719" id="Seg_7653" s="T718">tear-PST-3PL</ta>
            <ta e="T720" id="Seg_7654" s="T719">single.[NOM.SG]</ta>
            <ta e="T721" id="Seg_7655" s="T720">hand.[NOM.SG]</ta>
            <ta e="T722" id="Seg_7656" s="T721">be-PRS.[3SG]</ta>
            <ta e="T723" id="Seg_7657" s="T722">and</ta>
            <ta e="T724" id="Seg_7658" s="T723">single.[NOM.SG]</ta>
            <ta e="T725" id="Seg_7659" s="T724">hand-ACC.3SG</ta>
            <ta e="T726" id="Seg_7660" s="T725">PTCL</ta>
            <ta e="T727" id="Seg_7661" s="T726">tear.off-PST-3PL</ta>
            <ta e="T728" id="Seg_7662" s="T727">I.NOM</ta>
            <ta e="T729" id="Seg_7663" s="T728">fall-PST-1SG</ta>
            <ta e="T731" id="Seg_7664" s="T730">single.[NOM.SG]</ta>
            <ta e="T732" id="Seg_7665" s="T731">hand-NOM/GEN/ACC.1SG</ta>
            <ta e="T733" id="Seg_7666" s="T732">break-PST-1SG</ta>
            <ta e="T735" id="Seg_7667" s="T734">I.NOM</ta>
            <ta e="T736" id="Seg_7668" s="T735">fall-MOM-PST-1SG</ta>
            <ta e="T737" id="Seg_7669" s="T736">and</ta>
            <ta e="T738" id="Seg_7670" s="T737">hand-NOM/GEN/ACC.1SG</ta>
            <ta e="T739" id="Seg_7671" s="T738">break</ta>
            <ta e="T740" id="Seg_7672" s="T739">break-TR-PST-1SG</ta>
            <ta e="T741" id="Seg_7673" s="T740">then</ta>
            <ta e="T742" id="Seg_7674" s="T741">this.[NOM.SG]</ta>
            <ta e="T743" id="Seg_7675" s="T742">hand-NOM/GEN/ACC.1SG</ta>
            <ta e="T744" id="Seg_7676" s="T743">put-PST-3PL</ta>
            <ta e="T745" id="Seg_7677" s="T744">PTCL</ta>
            <ta e="T752" id="Seg_7678" s="T901">then</ta>
            <ta e="T753" id="Seg_7679" s="T752">hand-NOM/GEN/ACC.1SG</ta>
            <ta e="T918" id="Seg_7680" s="T753">PTCL</ta>
            <ta e="T755" id="Seg_7681" s="T918">bind-PST-3PL</ta>
            <ta e="T759" id="Seg_7682" s="T758">until</ta>
            <ta e="T760" id="Seg_7683" s="T759">NEG</ta>
            <ta e="T761" id="Seg_7684" s="T760">grow-FUT-3SG</ta>
            <ta e="T762" id="Seg_7685" s="T761">what.[NOM.SG]=INDEF</ta>
            <ta e="T763" id="Seg_7686" s="T762">work-INF.LAT</ta>
            <ta e="T764" id="Seg_7687" s="T763">one.cannot</ta>
            <ta e="T765" id="Seg_7688" s="T764">horse.[NOM.SG]</ta>
            <ta e="T766" id="Seg_7689" s="T765">harness-INF.LAT</ta>
            <ta e="T767" id="Seg_7690" s="T766">one.should</ta>
            <ta e="T772" id="Seg_7691" s="T771">horse.[NOM.SG]</ta>
            <ta e="T774" id="Seg_7692" s="T773">run-DUR.PST.[3SG]</ta>
            <ta e="T775" id="Seg_7693" s="T774">all</ta>
            <ta e="T782" id="Seg_7694" s="T781">horse.[NOM.SG]</ta>
            <ta e="T783" id="Seg_7695" s="T782">run-DUR.PST.[3SG]</ta>
            <ta e="T784" id="Seg_7696" s="T783">fall-MOM-PST.[3SG]</ta>
            <ta e="T785" id="Seg_7697" s="T784">foot-NOM/GEN.3SG</ta>
            <ta e="T786" id="Seg_7698" s="T785">break</ta>
            <ta e="T787" id="Seg_7699" s="T786">foot-NOM/GEN.3SG</ta>
            <ta e="T788" id="Seg_7700" s="T787">break-PST.[3SG]</ta>
            <ta e="T789" id="Seg_7701" s="T788">I.GEN</ta>
            <ta e="T790" id="Seg_7702" s="T789">hand-LOC</ta>
            <ta e="T791" id="Seg_7703" s="T790">five.[NOM.SG]</ta>
            <ta e="T792" id="Seg_7704" s="T791">finger-PL-ACC.3SG</ta>
            <ta e="T793" id="Seg_7705" s="T792">and</ta>
            <ta e="T794" id="Seg_7706" s="T793">you.GEN</ta>
            <ta e="T795" id="Seg_7707" s="T794">hand-LOC</ta>
            <ta e="T796" id="Seg_7708" s="T795">single.[NOM.SG]</ta>
            <ta e="T797" id="Seg_7709" s="T796">NEG.EX.[3SG]</ta>
            <ta e="T798" id="Seg_7710" s="T797">axe-INS</ta>
            <ta e="T799" id="Seg_7711" s="T798">PTCL</ta>
            <ta e="T800" id="Seg_7712" s="T799">cut-PST-2SG</ta>
            <ta e="T801" id="Seg_7713" s="T800">today</ta>
            <ta e="T802" id="Seg_7714" s="T801">child-PL</ta>
            <ta e="T803" id="Seg_7715" s="T802">many</ta>
            <ta e="T804" id="Seg_7716" s="T803">PTCL</ta>
            <ta e="T805" id="Seg_7717" s="T804">calf.[NOM.SG]</ta>
            <ta e="T806" id="Seg_7718" s="T805">drive-DUR.PST-3PL</ta>
            <ta e="T807" id="Seg_7719" s="T806">this.[NOM.SG]</ta>
            <ta e="T808" id="Seg_7720" s="T807">PTCL</ta>
            <ta e="T809" id="Seg_7721" s="T808">return-MOM-PST.[3SG]</ta>
            <ta e="T810" id="Seg_7722" s="T809">and</ta>
            <ta e="T811" id="Seg_7723" s="T810">run-MOM-PST.[3SG]</ta>
            <ta e="T812" id="Seg_7724" s="T811">tent-LAT/LOC.3SG</ta>
            <ta e="T813" id="Seg_7725" s="T812">evening-LOC.ADV</ta>
            <ta e="T814" id="Seg_7726" s="T813">go-CVB</ta>
            <ta e="T815" id="Seg_7727" s="T814">disappear-PST.[3SG]</ta>
            <ta e="T816" id="Seg_7728" s="T815">PTCL</ta>
            <ta e="T817" id="Seg_7729" s="T816">evening</ta>
            <ta e="T818" id="Seg_7730" s="T817">become-DUR-PRS.[3SG]</ta>
            <ta e="T819" id="Seg_7731" s="T818">sun.[NOM.SG]</ta>
            <ta e="T820" id="Seg_7732" s="T819">PTCL</ta>
            <ta e="T821" id="Seg_7733" s="T820">sit.down-DUR.[3SG]</ta>
            <ta e="T822" id="Seg_7734" s="T821">evening</ta>
            <ta e="T823" id="Seg_7735" s="T822">become-DUR-PRS.[3SG]</ta>
            <ta e="T824" id="Seg_7736" s="T823">sun.[NOM.SG]</ta>
            <ta e="T825" id="Seg_7737" s="T824">seat-DUR.[3SG]</ta>
            <ta e="T826" id="Seg_7738" s="T825">enough</ta>
            <ta e="T827" id="Seg_7739" s="T826">speak-INF.LAT</ta>
            <ta e="T828" id="Seg_7740" s="T827">one.should</ta>
            <ta e="T829" id="Seg_7741" s="T828">lie.down-INF.LAT</ta>
            <ta e="T830" id="Seg_7742" s="T829">sleep-INF.LAT</ta>
            <ta e="T831" id="Seg_7743" s="T830">morning-LOC.ADV</ta>
            <ta e="T832" id="Seg_7744" s="T831">morning</ta>
            <ta e="T833" id="Seg_7745" s="T832">one.should</ta>
            <ta e="T834" id="Seg_7746" s="T833">get.up-INF.LAT</ta>
            <ta e="T840" id="Seg_7747" s="T839">go-FUT-1SG</ta>
            <ta e="T841" id="Seg_7748" s="T840">flower-PL</ta>
            <ta e="T842" id="Seg_7749" s="T841">tear-INF.LAT</ta>
            <ta e="T843" id="Seg_7750" s="T842">snow.[NOM.SG]</ta>
            <ta e="T844" id="Seg_7751" s="T843">tree-LOC</ta>
            <ta e="T845" id="Seg_7752" s="T844">PTCL</ta>
            <ta e="T846" id="Seg_7753" s="T845">paper.[NOM.SG]</ta>
            <ta e="T847" id="Seg_7754" s="T846">hang.up-DUR.[3SG]</ta>
            <ta e="T848" id="Seg_7755" s="T847">NEG</ta>
            <ta e="T849" id="Seg_7756" s="T848">be.visible-PRS.[3SG]</ta>
            <ta e="T850" id="Seg_7757" s="T849">what.[NOM.SG]=INDEF</ta>
            <ta e="T851" id="Seg_7758" s="T850">NEG</ta>
            <ta e="T852" id="Seg_7759" s="T851">see-PRS-1SG</ta>
            <ta e="T853" id="Seg_7760" s="T852">I.NOM</ta>
            <ta e="T854" id="Seg_7761" s="T853">relative-NOM/GEN/ACC.1SG</ta>
            <ta e="T856" id="Seg_7762" s="T854">very</ta>
            <ta e="T858" id="Seg_7763" s="T857">far</ta>
            <ta e="T859" id="Seg_7764" s="T858">live-DUR.[3SG]</ta>
            <ta e="T860" id="Seg_7765" s="T859">one.should</ta>
            <ta e="T861" id="Seg_7766" s="T860">two.[NOM.SG]</ta>
            <ta e="T862" id="Seg_7767" s="T861">day.[NOM.SG]</ta>
            <ta e="T863" id="Seg_7768" s="T862">go-INF.LAT</ta>
            <ta e="T864" id="Seg_7769" s="T863">and</ta>
            <ta e="T865" id="Seg_7770" s="T864">maybe</ta>
            <ta e="T866" id="Seg_7771" s="T865">single.[NOM.SG]</ta>
            <ta e="T867" id="Seg_7772" s="T866">day-LAT</ta>
            <ta e="T868" id="Seg_7773" s="T867">go-FUT-2SG</ta>
            <ta e="T869" id="Seg_7774" s="T868">I.NOM</ta>
            <ta e="T870" id="Seg_7775" s="T869">this-LAT</ta>
            <ta e="T871" id="Seg_7776" s="T870">come-PST-1SG</ta>
            <ta e="T872" id="Seg_7777" s="T871">and</ta>
            <ta e="T873" id="Seg_7778" s="T872">this.[NOM.SG]</ta>
            <ta e="T874" id="Seg_7779" s="T873">NEG.EX.[3SG]</ta>
            <ta e="T875" id="Seg_7780" s="T874">where.to=INDEF</ta>
            <ta e="T876" id="Seg_7781" s="T875">go-CVB</ta>
            <ta e="T877" id="Seg_7782" s="T876">disappear-PST.[3SG]</ta>
            <ta e="T878" id="Seg_7783" s="T877">I.NOM</ta>
            <ta e="T879" id="Seg_7784" s="T878">PTCL</ta>
            <ta e="T917" id="Seg_7785" s="T879">return-MOM-PST.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PKZ">
            <ta e="T5" id="Seg_7786" s="T4">я.NOM</ta>
            <ta e="T6" id="Seg_7787" s="T5">NEG</ta>
            <ta e="T7" id="Seg_7788" s="T6">мочь-PRS-1SG</ta>
            <ta e="T10" id="Seg_7789" s="T9">двигаться-INF.LAT</ta>
            <ta e="T11" id="Seg_7790" s="T10">весь</ta>
            <ta e="T12" id="Seg_7791" s="T11">болеть-DUR.[3SG]</ta>
            <ta e="T13" id="Seg_7792" s="T12">однако</ta>
            <ta e="T14" id="Seg_7793" s="T13">я.NOM</ta>
            <ta e="T15" id="Seg_7794" s="T14">умереть-RES-PST-1SG</ta>
            <ta e="T16" id="Seg_7795" s="T15">сегодня</ta>
            <ta e="T17" id="Seg_7796" s="T16">этот.[NOM.SG]</ta>
            <ta e="T18" id="Seg_7797" s="T17">девушка.[NOM.SG]</ta>
            <ta e="T19" id="Seg_7798" s="T18">большой.[NOM.SG]</ta>
            <ta e="T20" id="Seg_7799" s="T19">день.[NOM.SG]</ta>
            <ta e="T21" id="Seg_7800" s="T20">этот-ACC</ta>
            <ta e="T22" id="Seg_7801" s="T21">сегодня</ta>
            <ta e="T27" id="Seg_7802" s="T26">мать-NOM/GEN/ACC.3SG</ta>
            <ta e="T28" id="Seg_7803" s="T27">принести-PST.[3SG]</ta>
            <ta e="T29" id="Seg_7804" s="T28">этот-LAT</ta>
            <ta e="T33" id="Seg_7805" s="T32">десять.[NOM.SG]</ta>
            <ta e="T34" id="Seg_7806" s="T33">два.[NOM.SG]</ta>
            <ta e="T35" id="Seg_7807" s="T34">год.[NOM.SG]</ta>
            <ta e="T36" id="Seg_7808" s="T35">и</ta>
            <ta e="T37" id="Seg_7809" s="T36">еще</ta>
            <ta e="T38" id="Seg_7810" s="T37">два.[NOM.SG]</ta>
            <ta e="T40" id="Seg_7811" s="T39">как</ta>
            <ta e="T41" id="Seg_7812" s="T40">этот.[NOM.SG]</ta>
            <ta e="T42" id="Seg_7813" s="T41">мужчина.[NOM.SG]</ta>
            <ta e="T43" id="Seg_7814" s="T42">жадный.[NOM.SG]</ta>
            <ta e="T44" id="Seg_7815" s="T43">что.[NOM.SG]=INDEF</ta>
            <ta e="T45" id="Seg_7816" s="T44">NEG</ta>
            <ta e="T46" id="Seg_7817" s="T45">взять-FUT.[3SG]</ta>
            <ta e="T47" id="Seg_7818" s="T46">что.[NOM.SG]=INDEF</ta>
            <ta e="T48" id="Seg_7819" s="T47">NEG</ta>
            <ta e="T49" id="Seg_7820" s="T48">дать-FUT-3SG</ta>
            <ta e="T50" id="Seg_7821" s="T49">а</ta>
            <ta e="T51" id="Seg_7822" s="T50">другой.[NOM.SG]</ta>
            <ta e="T52" id="Seg_7823" s="T51">мужчина.[NOM.SG]</ta>
            <ta e="T53" id="Seg_7824" s="T52">очень</ta>
            <ta e="T54" id="Seg_7825" s="T53">хороший.[NOM.SG]</ta>
            <ta e="T55" id="Seg_7826" s="T54">весь</ta>
            <ta e="T56" id="Seg_7827" s="T55">дать-MOM-FUT-3SG</ta>
            <ta e="T59" id="Seg_7828" s="T58">что.[NOM.SG]</ta>
            <ta e="T62" id="Seg_7829" s="T61">что.[NOM.SG]</ta>
            <ta e="T63" id="Seg_7830" s="T62">NEG</ta>
            <ta e="T69" id="Seg_7831" s="T67">что.[NOM.SG]</ta>
            <ta e="T70" id="Seg_7832" s="T69">что.[NOM.SG]</ta>
            <ta e="T71" id="Seg_7833" s="T70">NEG</ta>
            <ta e="T72" id="Seg_7834" s="T71">спросить-FUT-2SG</ta>
            <ta e="T73" id="Seg_7835" s="T72">весь</ta>
            <ta e="T74" id="Seg_7836" s="T73">дать-FUT-3SG</ta>
            <ta e="T75" id="Seg_7837" s="T74">этот-GEN</ta>
            <ta e="T76" id="Seg_7838" s="T75">очень</ta>
            <ta e="T77" id="Seg_7839" s="T76">сердце-NOM/GEN/ACC.3SG</ta>
            <ta e="T78" id="Seg_7840" s="T77">хороший.[NOM.SG]</ta>
            <ta e="T84" id="Seg_7841" s="T83">водка.[NOM.SG]</ta>
            <ta e="T85" id="Seg_7842" s="T84">NEG</ta>
            <ta e="T86" id="Seg_7843" s="T85">пить-PRS.[3SG]</ta>
            <ta e="T87" id="Seg_7844" s="T86">что.[NOM.SG]</ta>
            <ta e="T88" id="Seg_7845" s="T87">быть-PRS.[3SG]</ta>
            <ta e="T89" id="Seg_7846" s="T88">весь</ta>
            <ta e="T90" id="Seg_7847" s="T89">дать-FUT-3SG</ta>
            <ta e="T91" id="Seg_7848" s="T90">яйцо-PL</ta>
            <ta e="T92" id="Seg_7849" s="T91">дать-PRS.[3SG]</ta>
            <ta e="T95" id="Seg_7850" s="T94">масло.[NOM.SG]</ta>
            <ta e="T96" id="Seg_7851" s="T95">дать-PRS.[3SG]</ta>
            <ta e="T97" id="Seg_7852" s="T96">хлеб.[NOM.SG]</ta>
            <ta e="T98" id="Seg_7853" s="T97">дать-PRS.[3SG]</ta>
            <ta e="T99" id="Seg_7854" s="T98">мясо.[NOM.SG]</ta>
            <ta e="T100" id="Seg_7855" s="T99">быть-PRS.[3SG]</ta>
            <ta e="T101" id="Seg_7856" s="T100">так</ta>
            <ta e="T102" id="Seg_7857" s="T101">дать-FUT-3SG</ta>
            <ta e="T103" id="Seg_7858" s="T102">весь</ta>
            <ta e="T104" id="Seg_7859" s="T103">что.[NOM.SG]</ta>
            <ta e="T105" id="Seg_7860" s="T104">быть-PRS.[3SG]</ta>
            <ta e="T106" id="Seg_7861" s="T105">весь</ta>
            <ta e="T107" id="Seg_7862" s="T106">дать-FUT-3SG</ta>
            <ta e="T108" id="Seg_7863" s="T107">очень</ta>
            <ta e="T111" id="Seg_7864" s="T110">%наверху</ta>
            <ta e="T112" id="Seg_7865" s="T111">греметь-PRS.[3SG]</ta>
            <ta e="T113" id="Seg_7866" s="T112">бог.[NOM.SG]</ta>
            <ta e="T114" id="Seg_7867" s="T113">греметь-DUR.[3SG]</ta>
            <ta e="T116" id="Seg_7868" s="T115">кто.[NOM.SG]=INDEF</ta>
            <ta e="T117" id="Seg_7869" s="T116">убить-FUT-3SG</ta>
            <ta e="T118" id="Seg_7870" s="T117">радуга.[NOM.SG]</ta>
            <ta e="T119" id="Seg_7871" s="T118">стоять-DUR.[3SG]</ta>
            <ta e="T120" id="Seg_7872" s="T119">вода.[NOM.SG]</ta>
            <ta e="T121" id="Seg_7873" s="T120">взять-DUR.[3SG]</ta>
            <ta e="T122" id="Seg_7874" s="T121">тогда</ta>
            <ta e="T123" id="Seg_7875" s="T122">дождь.[NOM.SG]</ta>
            <ta e="T124" id="Seg_7876" s="T123">пойти-FUT-3SG</ta>
            <ta e="T125" id="Seg_7877" s="T124">град.[NOM.SG]</ta>
            <ta e="T126" id="Seg_7878" s="T125">пойти-FUT-3SG</ta>
            <ta e="T127" id="Seg_7879" s="T126">очень</ta>
            <ta e="T128" id="Seg_7880" s="T127">туча.[NOM.SG]</ta>
            <ta e="T129" id="Seg_7881" s="T128">черный.[NOM.SG]</ta>
            <ta e="T130" id="Seg_7882" s="T129">прийти-PRS.[3SG]</ta>
            <ta e="T131" id="Seg_7883" s="T130">я.NOM</ta>
            <ta e="T132" id="Seg_7884" s="T131">наружу</ta>
            <ta e="T133" id="Seg_7885" s="T132">пойти-PST-1SG</ta>
            <ta e="T134" id="Seg_7886" s="T133">кукушка.[NOM.SG]</ta>
            <ta e="T135" id="Seg_7887" s="T134">весь</ta>
            <ta e="T136" id="Seg_7888" s="T135">лететь-DUR.[3SG]</ta>
            <ta e="T137" id="Seg_7889" s="T136">смотреть-FUT-1SG</ta>
            <ta e="T138" id="Seg_7890" s="T137">этот.[NOM.SG]</ta>
            <ta e="T139" id="Seg_7891" s="T138">дом-LAT</ta>
            <ta e="T140" id="Seg_7892" s="T139">сесть-PST.[3SG]</ta>
            <ta e="T141" id="Seg_7893" s="T140">PTCL</ta>
            <ta e="T142" id="Seg_7894" s="T141">и</ta>
            <ta e="T143" id="Seg_7895" s="T142">кричать-DUR.[3SG]</ta>
            <ta e="T144" id="Seg_7896" s="T143">я.NOM</ta>
            <ta e="T145" id="Seg_7897" s="T144">сказать-IPFVZ-1SG</ta>
            <ta e="T146" id="Seg_7898" s="T145">один</ta>
            <ta e="T147" id="Seg_7899" s="T146">жить-DUR</ta>
            <ta e="T148" id="Seg_7900" s="T147">жить-FUT-1SG</ta>
            <ta e="T149" id="Seg_7901" s="T148">PTCL</ta>
            <ta e="T150" id="Seg_7902" s="T149">наверное</ta>
            <ta e="T151" id="Seg_7903" s="T150">один</ta>
            <ta e="T152" id="Seg_7904" s="T151">жить-FUT-1SG</ta>
            <ta e="T153" id="Seg_7905" s="T152">камень-INS</ta>
            <ta e="T154" id="Seg_7906" s="T153">PTCL</ta>
            <ta e="T155" id="Seg_7907" s="T154">выбросить-PST.[3SG]</ta>
            <ta e="T156" id="Seg_7908" s="T155">дерево-INS</ta>
            <ta e="T157" id="Seg_7909" s="T156">PTCL</ta>
            <ta e="T158" id="Seg_7910" s="T157">выбросить-PST.[3SG]</ta>
            <ta e="T159" id="Seg_7911" s="T158">этот.[NOM.SG]</ta>
            <ta e="T160" id="Seg_7912" s="T159">всегда</ta>
            <ta e="T161" id="Seg_7913" s="T160">сидеть-DUR.[3SG]</ta>
            <ta e="T162" id="Seg_7914" s="T161">кричать-DUR.[3SG]</ta>
            <ta e="T163" id="Seg_7915" s="T162">тогда</ta>
            <ta e="T164" id="Seg_7916" s="T163">лететь-MOM-PST.[3SG]</ta>
            <ta e="T165" id="Seg_7917" s="T164">где</ta>
            <ta e="T166" id="Seg_7918" s="T165">солнце.[NOM.SG]</ta>
            <ta e="T167" id="Seg_7919" s="T166">встать-DUR.[3SG]</ta>
            <ta e="T168" id="Seg_7920" s="T167">очень</ta>
            <ta e="T169" id="Seg_7921" s="T168">хитрый-ADJZ.[NOM.SG]</ta>
            <ta e="T170" id="Seg_7922" s="T169">люди.[NOM.SG]</ta>
            <ta e="T171" id="Seg_7923" s="T170">PTCL</ta>
            <ta e="T172" id="Seg_7924" s="T171">писать-PST-3PL</ta>
            <ta e="T173" id="Seg_7925" s="T172">бумага-LAT</ta>
            <ta e="T174" id="Seg_7926" s="T173">этот.[NOM.SG]</ta>
            <ta e="T175" id="Seg_7927" s="T174">мужчина.[NOM.SG]</ta>
            <ta e="T176" id="Seg_7928" s="T175">очень</ta>
            <ta e="T177" id="Seg_7929" s="T176">NEG</ta>
            <ta e="T178" id="Seg_7930" s="T177">хороший.[NOM.SG]</ta>
            <ta e="T179" id="Seg_7931" s="T178">весь</ta>
            <ta e="T180" id="Seg_7932" s="T179">что.[NOM.SG]</ta>
            <ta e="T181" id="Seg_7933" s="T180">взять-DUR.[3SG]</ta>
            <ta e="T183" id="Seg_7934" s="T181">и</ta>
            <ta e="T184" id="Seg_7935" s="T183">весь</ta>
            <ta e="T185" id="Seg_7936" s="T184">что.[NOM.SG]</ta>
            <ta e="T186" id="Seg_7937" s="T185">взять-DUR.[3SG]</ta>
            <ta e="T187" id="Seg_7938" s="T186">весь</ta>
            <ta e="T188" id="Seg_7939" s="T187">очень</ta>
            <ta e="T189" id="Seg_7940" s="T188">вор.[NOM.SG]</ta>
            <ta e="T190" id="Seg_7941" s="T189">мужчина.[NOM.SG]</ta>
            <ta e="T191" id="Seg_7942" s="T190">весь</ta>
            <ta e="T192" id="Seg_7943" s="T191">что.[NOM.SG]</ta>
            <ta e="T193" id="Seg_7944" s="T192">взять-PRS.[3SG]</ta>
            <ta e="T195" id="Seg_7945" s="T193">так</ta>
            <ta e="T196" id="Seg_7946" s="T195">весь</ta>
            <ta e="T197" id="Seg_7947" s="T196">взять-DUR.[3SG]</ta>
            <ta e="T198" id="Seg_7948" s="T197">весь</ta>
            <ta e="T199" id="Seg_7949" s="T198">нести-TR-DUR.[3SG]</ta>
            <ta e="T200" id="Seg_7950" s="T199">чум-LAT/LOC.3SG</ta>
            <ta e="T201" id="Seg_7951" s="T200">я.NOM</ta>
            <ta e="T202" id="Seg_7952" s="T201">сегодня</ta>
            <ta e="T203" id="Seg_7953" s="T202">дочь-GEN.1SG</ta>
            <ta e="T204" id="Seg_7954" s="T203">платок-EP-ACC</ta>
            <ta e="T205" id="Seg_7955" s="T204">взять-PST-1SG</ta>
            <ta e="T206" id="Seg_7956" s="T205">этот.[NOM.SG]</ta>
            <ta e="T207" id="Seg_7957" s="T206">пойти-PST.[3SG]</ta>
            <ta e="T208" id="Seg_7958" s="T207">Малиновка-LAT</ta>
            <ta e="T209" id="Seg_7959" s="T208">учиться-INF.LAT</ta>
            <ta e="T210" id="Seg_7960" s="T209">там</ta>
            <ta e="T211" id="Seg_7961" s="T210">женщина.[NOM.SG]</ta>
            <ta e="T212" id="Seg_7962" s="T211">спросить-DUR.[3SG]</ta>
            <ta e="T213" id="Seg_7963" s="T212">где</ta>
            <ta e="T214" id="Seg_7964" s="T213">ты.NOM</ta>
            <ta e="T215" id="Seg_7965" s="T214">платок.[NOM.SG]</ta>
            <ta e="T216" id="Seg_7966" s="T215">взять-PST-2SG</ta>
            <ta e="T217" id="Seg_7967" s="T216">бабушка.[NOM.SG]</ta>
            <ta e="T218" id="Seg_7968" s="T217">дать-PST.[3SG]</ta>
            <ta e="T219" id="Seg_7969" s="T218">а</ta>
            <ta e="T220" id="Seg_7970" s="T219">бабушка.[NOM.SG]</ta>
            <ta e="T221" id="Seg_7971" s="T220">где</ta>
            <ta e="T222" id="Seg_7972" s="T221">взять-PST.[3SG]</ta>
            <ta e="T223" id="Seg_7973" s="T222">Поля.[NOM.SG]</ta>
            <ta e="T224" id="Seg_7974" s="T223">дать-PST.[3SG]</ta>
            <ta e="T225" id="Seg_7975" s="T224">тот.[NOM.SG]</ta>
            <ta e="T226" id="Seg_7976" s="T225">лошадь.[NOM.SG]</ta>
            <ta e="T227" id="Seg_7977" s="T226">взять-PST-1SG</ta>
            <ta e="T228" id="Seg_7978" s="T227">много</ta>
            <ta e="T229" id="Seg_7979" s="T228">деньги.[NOM.SG]</ta>
            <ta e="T230" id="Seg_7980" s="T229">дать-PST-1SG</ta>
            <ta e="T231" id="Seg_7981" s="T230">а</ta>
            <ta e="T232" id="Seg_7982" s="T231">другой.[NOM.SG]</ta>
            <ta e="T233" id="Seg_7983" s="T232">лошадь.[NOM.SG]</ta>
            <ta e="T234" id="Seg_7984" s="T233">взять-PST-1SG</ta>
            <ta e="T235" id="Seg_7985" s="T234">мало</ta>
            <ta e="T236" id="Seg_7986" s="T235">дать-PST-1SG</ta>
            <ta e="T238" id="Seg_7987" s="T237">ловить-PST-1SG</ta>
            <ta e="T241" id="Seg_7988" s="T240">теленок.[NOM.SG]</ta>
            <ta e="T243" id="Seg_7989" s="T241">PTCL</ta>
            <ta e="T247" id="Seg_7990" s="T246">NEG</ta>
            <ta e="T248" id="Seg_7991" s="T247">идти-PRS.[3SG]</ta>
            <ta e="T249" id="Seg_7992" s="T248">я.NOM</ta>
            <ta e="T251" id="Seg_7993" s="T249">PTCL</ta>
            <ta e="T257" id="Seg_7994" s="T256">очень</ta>
            <ta e="T258" id="Seg_7995" s="T257">сильный.[NOM.SG]</ta>
            <ta e="T259" id="Seg_7996" s="T258">NEG</ta>
            <ta e="T260" id="Seg_7997" s="T259">мочь-PRS-1SG</ta>
            <ta e="T261" id="Seg_7998" s="T260">нести-INF.LAT</ta>
            <ta e="T262" id="Seg_7999" s="T261">PTCL</ta>
            <ta e="T267" id="Seg_8000" s="T266">пойти-IPFVZ-PRS-1SG</ta>
            <ta e="T268" id="Seg_8001" s="T267">PTCL</ta>
            <ta e="T270" id="Seg_8002" s="T269">NEG</ta>
            <ta e="T271" id="Seg_8003" s="T270">хороший.[NOM.SG]</ta>
            <ta e="T272" id="Seg_8004" s="T271">мальчик.[NOM.SG]</ta>
            <ta e="T273" id="Seg_8005" s="T272">мальчик.[NOM.SG]</ta>
            <ta e="T274" id="Seg_8006" s="T273">всегда</ta>
            <ta e="T275" id="Seg_8007" s="T274">PTCL</ta>
            <ta e="T276" id="Seg_8008" s="T275">украсть-DUR.[3SG]</ta>
            <ta e="T277" id="Seg_8009" s="T276">и</ta>
            <ta e="T278" id="Seg_8010" s="T277">нести-TR-DUR.[3SG]</ta>
            <ta e="T279" id="Seg_8011" s="T278">я.NOM</ta>
            <ta e="T280" id="Seg_8012" s="T279">лес-LOC</ta>
            <ta e="T281" id="Seg_8013" s="T280">идти-PST-1SG</ta>
            <ta e="T282" id="Seg_8014" s="T281">что.[NOM.SG]=INDEF</ta>
            <ta e="T285" id="Seg_8015" s="T284">видеть-PST-1SG</ta>
            <ta e="T286" id="Seg_8016" s="T285">тогда</ta>
            <ta e="T287" id="Seg_8017" s="T286">я.NOM</ta>
            <ta e="T288" id="Seg_8018" s="T287">пугать-FRQ-MOM-PST-1SG</ta>
            <ta e="T289" id="Seg_8019" s="T288">PTCL</ta>
            <ta e="T290" id="Seg_8020" s="T289">тогда</ta>
            <ta e="T293" id="Seg_8021" s="T292">бог.[NOM.SG]</ta>
            <ta e="T294" id="Seg_8022" s="T293">позвать-PST-1SG</ta>
            <ta e="T295" id="Seg_8023" s="T294">PTCL</ta>
            <ta e="T296" id="Seg_8024" s="T295">пойти-CVB</ta>
            <ta e="T297" id="Seg_8025" s="T296">исчезнуть-PST-3PL</ta>
            <ta e="T298" id="Seg_8026" s="T297">надо</ta>
            <ta e="T299" id="Seg_8027" s="T298">отверстие-NOM/GEN/ACC.3PL</ta>
            <ta e="T300" id="Seg_8028" s="T299">сверлить-INF.LAT</ta>
            <ta e="T301" id="Seg_8029" s="T300">надо</ta>
            <ta e="T302" id="Seg_8030" s="T301">ворота.[NOM.SG]</ta>
            <ta e="T303" id="Seg_8031" s="T302">делать-INF.LAT</ta>
            <ta e="T305" id="Seg_8032" s="T304">этот</ta>
            <ta e="T310" id="Seg_8033" s="T309">этот</ta>
            <ta e="T311" id="Seg_8034" s="T310">отверстие-PL-LAT</ta>
            <ta e="T312" id="Seg_8035" s="T311">надо</ta>
            <ta e="T313" id="Seg_8036" s="T312">дерево-PL</ta>
            <ta e="T314" id="Seg_8037" s="T313">ползти-INF.LAT</ta>
            <ta e="T315" id="Seg_8038" s="T314">тогда</ta>
            <ta e="T316" id="Seg_8039" s="T315">кораль.[NOM.SG]</ta>
            <ta e="T317" id="Seg_8040" s="T316">закрыть-FUT-1DU</ta>
            <ta e="T318" id="Seg_8041" s="T317">тогда</ta>
            <ta e="T319" id="Seg_8042" s="T318">теленок.[NOM.SG]</ta>
            <ta e="T320" id="Seg_8043" s="T319">NEG</ta>
            <ta e="T321" id="Seg_8044" s="T320">бежать-FUT-3SG</ta>
            <ta e="T322" id="Seg_8045" s="T321">кораль-LOC</ta>
            <ta e="T325" id="Seg_8046" s="T324">кораль-LOC</ta>
            <ta e="T326" id="Seg_8047" s="T325">стоять-DUR.[3SG]</ta>
            <ta e="T338" id="Seg_8048" s="T337">что-INS=INDEF</ta>
            <ta e="T339" id="Seg_8049" s="T338">PTCL</ta>
            <ta e="T340" id="Seg_8050" s="T339">пахнуть-PRS.[3SG]</ta>
            <ta e="T341" id="Seg_8051" s="T340">ты.GEN</ta>
            <ta e="T342" id="Seg_8052" s="T341">нос-NOM/GEN/ACC.2SG</ta>
            <ta e="T344" id="Seg_8053" s="T342">очень</ta>
            <ta e="T347" id="Seg_8054" s="T346">ты.GEN</ta>
            <ta e="T348" id="Seg_8055" s="T347">нос-NOM/GEN/ACC.2SG</ta>
            <ta e="T349" id="Seg_8056" s="T348">очень</ta>
            <ta e="T350" id="Seg_8057" s="T349">острый</ta>
            <ta e="T351" id="Seg_8058" s="T350">весь</ta>
            <ta e="T352" id="Seg_8059" s="T351">слушать-DUR.[3SG]</ta>
            <ta e="T353" id="Seg_8060" s="T352">ты.NOM</ta>
            <ta e="T354" id="Seg_8061" s="T353">где</ta>
            <ta e="T355" id="Seg_8062" s="T354">быть-PST-2SG</ta>
            <ta e="T356" id="Seg_8063" s="T355">вода-VBLZ-CVB</ta>
            <ta e="T357" id="Seg_8064" s="T356">идти-PST-1SG</ta>
            <ta e="T358" id="Seg_8065" s="T357">надо</ta>
            <ta e="T359" id="Seg_8066" s="T358">желтый.[NOM.SG]</ta>
            <ta e="T360" id="Seg_8067" s="T359">вода.[NOM.SG]</ta>
            <ta e="T361" id="Seg_8068" s="T360">кипятить-INF.LAT</ta>
            <ta e="T362" id="Seg_8069" s="T361">надо</ta>
            <ta e="T363" id="Seg_8070" s="T362">суп.[NOM.SG]</ta>
            <ta e="T366" id="Seg_8071" s="T365">кипятить-INF.LAT</ta>
            <ta e="T367" id="Seg_8072" s="T366">мужчина-PL</ta>
            <ta e="T368" id="Seg_8073" s="T367">кормить-INF.LAT</ta>
            <ta e="T369" id="Seg_8074" s="T368">я.NOM</ta>
            <ta e="T370" id="Seg_8075" s="T369">десять.[NOM.SG]</ta>
            <ta e="T371" id="Seg_8076" s="T370">деньги.[NOM.SG]</ta>
            <ta e="T372" id="Seg_8077" s="T371">быть-PRS.[3SG]</ta>
            <ta e="T373" id="Seg_8078" s="T372">надо</ta>
            <ta e="T374" id="Seg_8079" s="T373">ты.DAT</ta>
            <ta e="T375" id="Seg_8080" s="T374">пять.[NOM.SG]</ta>
            <ta e="T376" id="Seg_8081" s="T375">дать-INF.LAT</ta>
            <ta e="T377" id="Seg_8082" s="T376">пять.[NOM.SG]</ta>
            <ta e="T378" id="Seg_8083" s="T377">сам-LAT/LOC.3SG</ta>
            <ta e="T379" id="Seg_8084" s="T378">остаться-INF.LAT</ta>
            <ta e="T380" id="Seg_8085" s="T379">может.быть</ta>
            <ta e="T381" id="Seg_8086" s="T380">куда=INDEF</ta>
            <ta e="T382" id="Seg_8087" s="T381">пойти-FUT-2SG</ta>
            <ta e="T383" id="Seg_8088" s="T382">деньги.[NOM.SG]</ta>
            <ta e="T384" id="Seg_8089" s="T383">нужно</ta>
            <ta e="T385" id="Seg_8090" s="T384">стать-FUT-3SG</ta>
            <ta e="T387" id="Seg_8091" s="T386">Матвеев.[NOM.SG]</ta>
            <ta e="T388" id="Seg_8092" s="T387">PTCL</ta>
            <ta e="T389" id="Seg_8093" s="T388">сидеть-DUR.[3SG]</ta>
            <ta e="T390" id="Seg_8094" s="T389">NEG</ta>
            <ta e="T391" id="Seg_8095" s="T390">говорить-PRS.[3SG]</ta>
            <ta e="T392" id="Seg_8096" s="T391">сердце-NOM/GEN/ACC.3SG</ta>
            <ta e="T393" id="Seg_8097" s="T392">болеть-PRS.[3SG]</ta>
            <ta e="T394" id="Seg_8098" s="T393">чум-LAT-2SG</ta>
            <ta e="T395" id="Seg_8099" s="T394">бумага.[NOM.SG]</ta>
            <ta e="T396" id="Seg_8100" s="T395">NEG.EX.[3SG]</ta>
            <ta e="T397" id="Seg_8101" s="T396">женщина-NOM/GEN.3SG</ta>
            <ta e="T398" id="Seg_8102" s="T397">PTCL</ta>
            <ta e="T399" id="Seg_8103" s="T398">сердиться-MOM-PST.[3SG]</ta>
            <ta e="T400" id="Seg_8104" s="T399">NEG</ta>
            <ta e="T401" id="Seg_8105" s="T400">писать-PRS.[3SG]</ta>
            <ta e="T404" id="Seg_8106" s="T403">бумага.[NOM.SG]</ta>
            <ta e="T405" id="Seg_8107" s="T404">NEG.AUX-IMP.2SG</ta>
            <ta e="T406" id="Seg_8108" s="T405">сердиться-CNG</ta>
            <ta e="T410" id="Seg_8109" s="T409">скоро</ta>
            <ta e="T411" id="Seg_8110" s="T410">чум-LAT-%%</ta>
            <ta e="T412" id="Seg_8111" s="T411">прийти-FUT-1SG</ta>
            <ta e="T413" id="Seg_8112" s="T412">что=INDEF</ta>
            <ta e="T414" id="Seg_8113" s="T413">ты.DAT</ta>
            <ta e="T415" id="Seg_8114" s="T414">принести-FUT-1SG</ta>
            <ta e="T416" id="Seg_8115" s="T415">сильно</ta>
            <ta e="T417" id="Seg_8116" s="T416">NEG</ta>
            <ta e="T418" id="Seg_8117" s="T417">говорить-EP-IMP.2SG</ta>
            <ta e="T419" id="Seg_8118" s="T418">мягко</ta>
            <ta e="T420" id="Seg_8119" s="T419">говорить-EP-IMP.2SG</ta>
            <ta e="T421" id="Seg_8120" s="T420">а.то</ta>
            <ta e="T422" id="Seg_8121" s="T421">весь</ta>
            <ta e="T423" id="Seg_8122" s="T422">люди.[NOM.SG]</ta>
            <ta e="T424" id="Seg_8123" s="T423">слышать-PRS-3PL</ta>
            <ta e="T425" id="Seg_8124" s="T424">сильно</ta>
            <ta e="T426" id="Seg_8125" s="T425">NEG.AUX-IMP.2SG</ta>
            <ta e="T427" id="Seg_8126" s="T426">кричать-EP-CNG</ta>
            <ta e="T428" id="Seg_8127" s="T427">а</ta>
            <ta e="T429" id="Seg_8128" s="T428">NEG.AUX-IMP.2SG</ta>
            <ta e="T430" id="Seg_8129" s="T429">ты.NOM</ta>
            <ta e="T431" id="Seg_8130" s="T430">NEG.AUX-IMP.2SG</ta>
            <ta e="T432" id="Seg_8131" s="T431">ругать-DES-CNG</ta>
            <ta e="T433" id="Seg_8132" s="T432">а.то</ta>
            <ta e="T434" id="Seg_8133" s="T433">люди.[NOM.SG]</ta>
            <ta e="T435" id="Seg_8134" s="T434">весь</ta>
            <ta e="T436" id="Seg_8135" s="T435">слышать-DUR-3PL</ta>
            <ta e="T437" id="Seg_8136" s="T436">как</ta>
            <ta e="T438" id="Seg_8137" s="T437">ты.GEN</ta>
            <ta e="T439" id="Seg_8138" s="T438">ты.GEN</ta>
            <ta e="T440" id="Seg_8139" s="T439">ругать-DES-DUR-2SG</ta>
            <ta e="T441" id="Seg_8140" s="T440">я.NOM</ta>
            <ta e="T442" id="Seg_8141" s="T441">сегодня</ta>
            <ta e="T443" id="Seg_8142" s="T442">сметана.[NOM.SG]</ta>
            <ta e="T444" id="Seg_8143" s="T443">лить-PST-1SG</ta>
            <ta e="T445" id="Seg_8144" s="T444">сбивать.масло-PST-1SG</ta>
            <ta e="T446" id="Seg_8145" s="T445">кринка-PL</ta>
            <ta e="T447" id="Seg_8146" s="T446">мыться-TR-PST-1SG</ta>
            <ta e="T448" id="Seg_8147" s="T447">ждать-PST-1SG</ta>
            <ta e="T449" id="Seg_8148" s="T448">солнце.[NOM.SG]</ta>
            <ta e="T450" id="Seg_8149" s="T449">сушить-DUR.[3SG]</ta>
            <ta e="T451" id="Seg_8150" s="T450">весь</ta>
            <ta e="T452" id="Seg_8151" s="T451">что.[NOM.SG]</ta>
            <ta e="T453" id="Seg_8152" s="T452">принести-PST-2SG</ta>
            <ta e="T454" id="Seg_8153" s="T453">показать-IMP.2SG.O</ta>
            <ta e="T455" id="Seg_8154" s="T454">я.LAT</ta>
            <ta e="T458" id="Seg_8155" s="T457">что.[NOM.SG]</ta>
            <ta e="T459" id="Seg_8156" s="T458">там</ta>
            <ta e="T460" id="Seg_8157" s="T459">ложиться-DUR.[3SG]</ta>
            <ta e="T461" id="Seg_8158" s="T460">я.NOM</ta>
            <ta e="T462" id="Seg_8159" s="T461">сегодня</ta>
            <ta e="T463" id="Seg_8160" s="T462">черемша-VBLZ-CVB</ta>
            <ta e="T464" id="Seg_8161" s="T463">идти-PST-1SG</ta>
            <ta e="T465" id="Seg_8162" s="T464">черемша.[NOM.SG]</ta>
            <ta e="T466" id="Seg_8163" s="T465">принести-PST-1SG</ta>
            <ta e="T467" id="Seg_8164" s="T466">тогда</ta>
            <ta e="T468" id="Seg_8165" s="T467">зарезать-FUT-1SG</ta>
            <ta e="T469" id="Seg_8166" s="T468">яйцо-PL</ta>
            <ta e="T471" id="Seg_8167" s="T470">класть-FUT-1SG</ta>
            <ta e="T472" id="Seg_8168" s="T471">сметана.[NOM.SG]</ta>
            <ta e="T473" id="Seg_8169" s="T472">класть-FUT-1SG</ta>
            <ta e="T474" id="Seg_8170" s="T473">тогда</ta>
            <ta e="T475" id="Seg_8171" s="T474">печь-INF.LAT</ta>
            <ta e="T476" id="Seg_8172" s="T475">пирог-PL</ta>
            <ta e="T477" id="Seg_8173" s="T476">мочь-FUT-1SG</ta>
            <ta e="T479" id="Seg_8174" s="T478">мясо.[NOM.SG]</ta>
            <ta e="T480" id="Seg_8175" s="T479">кипятить-PST-1SG</ta>
            <ta e="T481" id="Seg_8176" s="T480">мясо.[NOM.SG]</ta>
            <ta e="T482" id="Seg_8177" s="T481">съесть-PST-3PL</ta>
            <ta e="T483" id="Seg_8178" s="T482">%%-INF.LAT</ta>
            <ta e="T484" id="Seg_8179" s="T483">один.[NOM.SG]</ta>
            <ta e="T485" id="Seg_8180" s="T484">кость.[NOM.SG]</ta>
            <ta e="T486" id="Seg_8181" s="T485">остаться-MOM-PST.[3SG]</ta>
            <ta e="T490" id="Seg_8182" s="T489">собака-PL</ta>
            <ta e="T491" id="Seg_8183" s="T490">PTCL</ta>
            <ta e="T492" id="Seg_8184" s="T491">кость.[NOM.SG]</ta>
            <ta e="T493" id="Seg_8185" s="T492">съесть-DUR-3PL</ta>
            <ta e="T494" id="Seg_8186" s="T493">очень</ta>
            <ta e="T495" id="Seg_8187" s="T494">много</ta>
            <ta e="T496" id="Seg_8188" s="T495">кость.[NOM.SG]</ta>
            <ta e="T497" id="Seg_8189" s="T496">выбросить-PST-1PL</ta>
            <ta e="T498" id="Seg_8190" s="T497">я.NOM</ta>
            <ta e="T501" id="Seg_8191" s="T500">я.NOM</ta>
            <ta e="T502" id="Seg_8192" s="T501">сегодня</ta>
            <ta e="T503" id="Seg_8193" s="T502">хлеб.[NOM.SG]</ta>
            <ta e="T504" id="Seg_8194" s="T503">печь-PST-1SG</ta>
            <ta e="T505" id="Seg_8195" s="T504">NEG</ta>
            <ta e="T506" id="Seg_8196" s="T505">кислый.[NOM.SG]</ta>
            <ta e="T507" id="Seg_8197" s="T506">NEG</ta>
            <ta e="T508" id="Seg_8198" s="T507">хороший.[NOM.SG]</ta>
            <ta e="T509" id="Seg_8199" s="T508">PTCL</ta>
            <ta e="T510" id="Seg_8200" s="T509">я.NOM</ta>
            <ta e="T511" id="Seg_8201" s="T510">этот.[NOM.SG]</ta>
            <ta e="T512" id="Seg_8202" s="T511">хлеб.[NOM.SG]</ta>
            <ta e="T513" id="Seg_8203" s="T512">этот.[NOM.SG]</ta>
            <ta e="T516" id="Seg_8204" s="T515">собака-PL-LAT</ta>
            <ta e="T517" id="Seg_8205" s="T516">дать-PST-1SG</ta>
            <ta e="T518" id="Seg_8206" s="T517">собака-PL</ta>
            <ta e="T519" id="Seg_8207" s="T518">PTCL</ta>
            <ta e="T520" id="Seg_8208" s="T519">хлеб.[NOM.SG]</ta>
            <ta e="T521" id="Seg_8209" s="T520">съесть-DUR-3PL</ta>
            <ta e="T522" id="Seg_8210" s="T521">и</ta>
            <ta e="T523" id="Seg_8211" s="T522">PTCL</ta>
            <ta e="T524" id="Seg_8212" s="T523">бороться-DUR-3PL</ta>
            <ta e="T525" id="Seg_8213" s="T524">собака.[NOM.SG]</ta>
            <ta e="T526" id="Seg_8214" s="T525">PTCL</ta>
            <ta e="T527" id="Seg_8215" s="T526">этот.[NOM.SG]</ta>
            <ta e="T528" id="Seg_8216" s="T527">собака-NOM/GEN/ACC.3SG</ta>
            <ta e="T529" id="Seg_8217" s="T528">знать-3SG.O</ta>
            <ta e="T530" id="Seg_8218" s="T529">PTCL</ta>
            <ta e="T531" id="Seg_8219" s="T530">вечер</ta>
            <ta e="T532" id="Seg_8220" s="T531">быть-PST.[3SG]</ta>
            <ta e="T533" id="Seg_8221" s="T532">вечер</ta>
            <ta e="T534" id="Seg_8222" s="T533">быть-PST.[3SG]</ta>
            <ta e="T536" id="Seg_8223" s="T534">очень</ta>
            <ta e="T540" id="Seg_8224" s="T539">что.[NOM.SG]=INDEF</ta>
            <ta e="T541" id="Seg_8225" s="T540">NEG</ta>
            <ta e="T542" id="Seg_8226" s="T541">быть.видным-PRS.[3SG]</ta>
            <ta e="T543" id="Seg_8227" s="T542">я.NOM</ta>
            <ta e="T544" id="Seg_8228" s="T543">этот-ACC</ta>
            <ta e="T545" id="Seg_8229" s="T544">NEG</ta>
            <ta e="T546" id="Seg_8230" s="T545">знать-PST-1SG</ta>
            <ta e="T547" id="Seg_8231" s="T546">кто.[NOM.SG]</ta>
            <ta e="T548" id="Seg_8232" s="T547">прийти-PRS.[3SG]</ta>
            <ta e="T549" id="Seg_8233" s="T548">я.NOM</ta>
            <ta e="T550" id="Seg_8234" s="T549">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T551" id="Seg_8235" s="T550">туес-PL</ta>
            <ta e="T552" id="Seg_8236" s="T551">делать-PST.[3SG]</ta>
            <ta e="T553" id="Seg_8237" s="T552">кожа.[NOM.SG]</ta>
            <ta e="T554" id="Seg_8238" s="T553">кожа.[NOM.SG]</ta>
            <ta e="T555" id="Seg_8239" s="T554">снимать.кору</ta>
            <ta e="T556" id="Seg_8240" s="T555">снимать.кору-PST.[3SG]</ta>
            <ta e="T557" id="Seg_8241" s="T556">и</ta>
            <ta e="T558" id="Seg_8242" s="T557">делать-DUR-PST.[3SG]</ta>
            <ta e="T559" id="Seg_8243" s="T558">туес.[NOM.SG]</ta>
            <ta e="T560" id="Seg_8244" s="T559">шить-PST-3PL</ta>
            <ta e="T561" id="Seg_8245" s="T560">жила-PL-INS</ta>
            <ta e="T562" id="Seg_8246" s="T561">PTCL</ta>
            <ta e="T563" id="Seg_8247" s="T562">игла-INS</ta>
            <ta e="T564" id="Seg_8248" s="T563">и</ta>
            <ta e="T565" id="Seg_8249" s="T564">наперсток.[NOM.SG]</ta>
            <ta e="T566" id="Seg_8250" s="T565">палец-LOC</ta>
            <ta e="T567" id="Seg_8251" s="T566">быть-PST.[3SG]</ta>
            <ta e="T569" id="Seg_8252" s="T567">туес-PL</ta>
            <ta e="T570" id="Seg_8253" s="T569">шить-PST-3PL</ta>
            <ta e="T571" id="Seg_8254" s="T570">PTCL</ta>
            <ta e="T572" id="Seg_8255" s="T571">жила-PL-INS</ta>
            <ta e="T573" id="Seg_8256" s="T572">и</ta>
            <ta e="T576" id="Seg_8257" s="T575">наперсток.[NOM.SG]</ta>
            <ta e="T577" id="Seg_8258" s="T576">надеть-PST.[3SG]</ta>
            <ta e="T578" id="Seg_8259" s="T577">палец-LAT</ta>
            <ta e="T579" id="Seg_8260" s="T578">и</ta>
            <ta e="T580" id="Seg_8261" s="T579">игла.[NOM.SG]</ta>
            <ta e="T581" id="Seg_8262" s="T580">взять-PST.[3SG]</ta>
            <ta e="T582" id="Seg_8263" s="T581">шить-PST.[3SG]</ta>
            <ta e="T593" id="Seg_8264" s="T592">палец-ACC.3SG</ta>
            <ta e="T594" id="Seg_8265" s="T593">PTCL</ta>
            <ta e="T595" id="Seg_8266" s="T594">вставить-FUT-3SG</ta>
            <ta e="T596" id="Seg_8267" s="T595">ты.GEN</ta>
            <ta e="T597" id="Seg_8268" s="T596">PTCL</ta>
            <ta e="T598" id="Seg_8269" s="T597">женщина-NOM/GEN/ACC.2SG</ta>
            <ta e="T599" id="Seg_8270" s="T598">жалеть-DUR-2SG</ta>
            <ta e="T600" id="Seg_8271" s="T599">сердце-NOM/GEN/ACC.3SG</ta>
            <ta e="T601" id="Seg_8272" s="T600">болеть-PRS.[3SG]</ta>
            <ta e="T602" id="Seg_8273" s="T601">очень</ta>
            <ta e="T603" id="Seg_8274" s="T602">я.NOM</ta>
            <ta e="T604" id="Seg_8275" s="T603">PTCL</ta>
            <ta e="T605" id="Seg_8276" s="T604">дом-NOM/GEN/ACC.1SG</ta>
            <ta e="T606" id="Seg_8277" s="T605">черный.[NOM.SG]</ta>
            <ta e="T609" id="Seg_8278" s="T608">завтра</ta>
            <ta e="T610" id="Seg_8279" s="T609">мыть-FUT-1SG</ta>
            <ta e="T611" id="Seg_8280" s="T610">очень</ta>
            <ta e="T612" id="Seg_8281" s="T611">грязный.[NOM.SG]</ta>
            <ta e="T613" id="Seg_8282" s="T612">а.то</ta>
            <ta e="T614" id="Seg_8283" s="T613">люди.[NOM.SG]</ta>
            <ta e="T615" id="Seg_8284" s="T614">PTCL</ta>
            <ta e="T616" id="Seg_8285" s="T615">сказать-DUR-3PL</ta>
            <ta e="T617" id="Seg_8286" s="T616">сказать-PRS-3PL</ta>
            <ta e="T618" id="Seg_8287" s="T617">ленивый.[NOM.SG]</ta>
            <ta e="T619" id="Seg_8288" s="T618">женщина.[NOM.SG]</ta>
            <ta e="T620" id="Seg_8289" s="T619">дом.[NOM.SG]</ta>
            <ta e="T621" id="Seg_8290" s="T620">NEG</ta>
            <ta e="T622" id="Seg_8291" s="T621">мыть-PRS.[3SG]</ta>
            <ta e="T627" id="Seg_8292" s="T626">что.[NOM.SG]</ta>
            <ta e="T628" id="Seg_8293" s="T627">ты.DAT</ta>
            <ta e="T629" id="Seg_8294" s="T628">говорить-INF.LAT</ta>
            <ta e="T630" id="Seg_8295" s="T629">я.NOM</ta>
            <ta e="T631" id="Seg_8296" s="T630">весь</ta>
            <ta e="T632" id="Seg_8297" s="T631">забыть-PST-1SG</ta>
            <ta e="T633" id="Seg_8298" s="T632">сегодня</ta>
            <ta e="T634" id="Seg_8299" s="T633">спать-INF.LAT</ta>
            <ta e="T635" id="Seg_8300" s="T634">клоп-PL</ta>
            <ta e="T636" id="Seg_8301" s="T635">NEG</ta>
            <ta e="T637" id="Seg_8302" s="T636">дать-PST-3PL</ta>
            <ta e="T638" id="Seg_8303" s="T637">этот.[NOM.SG]</ta>
            <ta e="T639" id="Seg_8304" s="T638">мужчина.[NOM.SG]</ta>
            <ta e="T640" id="Seg_8305" s="T639">мука.[NOM.SG]</ta>
            <ta e="T641" id="Seg_8306" s="T640">спросить-PST.[3SG]</ta>
            <ta e="T642" id="Seg_8307" s="T641">надо</ta>
            <ta e="T643" id="Seg_8308" s="T642">этот-LAT</ta>
            <ta e="T644" id="Seg_8309" s="T643">дать-INF.LAT</ta>
            <ta e="T645" id="Seg_8310" s="T644">этот-GEN</ta>
            <ta e="T646" id="Seg_8311" s="T645">хлеб.[NOM.SG]</ta>
            <ta e="T647" id="Seg_8312" s="T646">NEG.EX.[3SG]</ta>
            <ta e="T648" id="Seg_8313" s="T647">съесть-INF.LAT</ta>
            <ta e="T649" id="Seg_8314" s="T648">весь</ta>
            <ta e="T656" id="Seg_8315" s="T655">где</ta>
            <ta e="T657" id="Seg_8316" s="T656">взять-INF.LAT</ta>
            <ta e="T658" id="Seg_8317" s="T657">что.[NOM.SG]</ta>
            <ta e="T659" id="Seg_8318" s="T658">говорить-INF.LAT</ta>
            <ta e="T660" id="Seg_8319" s="T659">этот-LAT</ta>
            <ta e="T661" id="Seg_8320" s="T660">очень</ta>
            <ta e="T664" id="Seg_8321" s="T663">много</ta>
            <ta e="T665" id="Seg_8322" s="T664">надо</ta>
            <ta e="T666" id="Seg_8323" s="T665">говорить-INF.LAT</ta>
            <ta e="T670" id="Seg_8324" s="T668">надо</ta>
            <ta e="T671" id="Seg_8325" s="T670">пойти-INF.LAT</ta>
            <ta e="T672" id="Seg_8326" s="T671">смотреть-FRQ-INF.LAT</ta>
            <ta e="T673" id="Seg_8327" s="T672">где=INDEF</ta>
            <ta e="T674" id="Seg_8328" s="T673">говорить-INF.LAT</ta>
            <ta e="T675" id="Seg_8329" s="T674">я.NOM</ta>
            <ta e="T676" id="Seg_8330" s="T675">вчера</ta>
            <ta e="T677" id="Seg_8331" s="T676">долго</ta>
            <ta e="T678" id="Seg_8332" s="T677">NEG</ta>
            <ta e="T681" id="Seg_8333" s="T680">ложиться-PST-1SG</ta>
            <ta e="T682" id="Seg_8334" s="T681">спать-INF.LAT</ta>
            <ta e="T685" id="Seg_8335" s="T684">долго</ta>
            <ta e="T686" id="Seg_8336" s="T685">NEG</ta>
            <ta e="T688" id="Seg_8337" s="T687">NEG</ta>
            <ta e="T690" id="Seg_8338" s="T689">NEG</ta>
            <ta e="T691" id="Seg_8339" s="T690">ложиться-FUT-1SG</ta>
            <ta e="T692" id="Seg_8340" s="T691">спать-INF.LAT</ta>
            <ta e="T697" id="Seg_8341" s="T696">спать-PST-1SG</ta>
            <ta e="T698" id="Seg_8342" s="T697">что.[NOM.SG]=INDEF</ta>
            <ta e="T699" id="Seg_8343" s="T698">NEG</ta>
            <ta e="T700" id="Seg_8344" s="T699">видеть-PST-1SG</ta>
            <ta e="T701" id="Seg_8345" s="T700">этот.[NOM.SG]</ta>
            <ta e="T702" id="Seg_8346" s="T701">мужчина-LOC</ta>
            <ta e="T703" id="Seg_8347" s="T702">волосы-NOM/GEN.3SG</ta>
            <ta e="T704" id="Seg_8348" s="T703">весь</ta>
            <ta e="T705" id="Seg_8349" s="T704">черный.[NOM.SG]</ta>
            <ta e="T706" id="Seg_8350" s="T705">быть-PST.[3SG]</ta>
            <ta e="T707" id="Seg_8351" s="T706">сейчас</ta>
            <ta e="T708" id="Seg_8352" s="T707">весь</ta>
            <ta e="T709" id="Seg_8353" s="T708">очень</ta>
            <ta e="T710" id="Seg_8354" s="T709">белый.[NOM.SG]</ta>
            <ta e="T711" id="Seg_8355" s="T710">стать-RES-PST-3PL</ta>
            <ta e="T712" id="Seg_8356" s="T711">волосы.[NOM.SG]</ta>
            <ta e="T713" id="Seg_8357" s="T712">один.[NOM.SG]</ta>
            <ta e="T714" id="Seg_8358" s="T713">рука.[NOM.SG]</ta>
            <ta e="T715" id="Seg_8359" s="T714">быть-PRS.[3SG]</ta>
            <ta e="T716" id="Seg_8360" s="T715">а</ta>
            <ta e="T717" id="Seg_8361" s="T716">один.[NOM.SG]</ta>
            <ta e="T718" id="Seg_8362" s="T717">рука.[NOM.SG]</ta>
            <ta e="T719" id="Seg_8363" s="T718">рвать-PST-3PL</ta>
            <ta e="T720" id="Seg_8364" s="T719">один.[NOM.SG]</ta>
            <ta e="T721" id="Seg_8365" s="T720">рука.[NOM.SG]</ta>
            <ta e="T722" id="Seg_8366" s="T721">быть-PRS.[3SG]</ta>
            <ta e="T723" id="Seg_8367" s="T722">а</ta>
            <ta e="T724" id="Seg_8368" s="T723">один.[NOM.SG]</ta>
            <ta e="T725" id="Seg_8369" s="T724">рука-ACC.3SG</ta>
            <ta e="T726" id="Seg_8370" s="T725">PTCL</ta>
            <ta e="T727" id="Seg_8371" s="T726">оторвать-PST-3PL</ta>
            <ta e="T728" id="Seg_8372" s="T727">я.NOM</ta>
            <ta e="T729" id="Seg_8373" s="T728">упасть-PST-1SG</ta>
            <ta e="T731" id="Seg_8374" s="T730">один.[NOM.SG]</ta>
            <ta e="T732" id="Seg_8375" s="T731">рука-NOM/GEN/ACC.1SG</ta>
            <ta e="T733" id="Seg_8376" s="T732">сломать-PST-1SG</ta>
            <ta e="T735" id="Seg_8377" s="T734">я.NOM</ta>
            <ta e="T736" id="Seg_8378" s="T735">упасть-MOM-PST-1SG</ta>
            <ta e="T737" id="Seg_8379" s="T736">и</ta>
            <ta e="T738" id="Seg_8380" s="T737">рука-NOM/GEN/ACC.1SG</ta>
            <ta e="T739" id="Seg_8381" s="T738">сломать</ta>
            <ta e="T740" id="Seg_8382" s="T739">сломать-TR-PST-1SG</ta>
            <ta e="T741" id="Seg_8383" s="T740">тогда</ta>
            <ta e="T742" id="Seg_8384" s="T741">этот.[NOM.SG]</ta>
            <ta e="T743" id="Seg_8385" s="T742">рука-NOM/GEN/ACC.1SG</ta>
            <ta e="T744" id="Seg_8386" s="T743">класть-PST-3PL</ta>
            <ta e="T745" id="Seg_8387" s="T744">PTCL</ta>
            <ta e="T752" id="Seg_8388" s="T901">тогда</ta>
            <ta e="T753" id="Seg_8389" s="T752">рука-NOM/GEN/ACC.1SG</ta>
            <ta e="T918" id="Seg_8390" s="T753">PTCL</ta>
            <ta e="T755" id="Seg_8391" s="T918">завязать-PST-3PL</ta>
            <ta e="T759" id="Seg_8392" s="T758">пока</ta>
            <ta e="T760" id="Seg_8393" s="T759">NEG</ta>
            <ta e="T761" id="Seg_8394" s="T760">расти-FUT-3SG</ta>
            <ta e="T762" id="Seg_8395" s="T761">что.[NOM.SG]=INDEF</ta>
            <ta e="T763" id="Seg_8396" s="T762">работать-INF.LAT</ta>
            <ta e="T764" id="Seg_8397" s="T763">нельзя</ta>
            <ta e="T765" id="Seg_8398" s="T764">лошадь.[NOM.SG]</ta>
            <ta e="T766" id="Seg_8399" s="T765">запрячь-INF.LAT</ta>
            <ta e="T767" id="Seg_8400" s="T766">надо</ta>
            <ta e="T772" id="Seg_8401" s="T771">лошадь.[NOM.SG]</ta>
            <ta e="T774" id="Seg_8402" s="T773">бежать-DUR.PST.[3SG]</ta>
            <ta e="T775" id="Seg_8403" s="T774">весь</ta>
            <ta e="T782" id="Seg_8404" s="T781">лошадь.[NOM.SG]</ta>
            <ta e="T783" id="Seg_8405" s="T782">бежать-DUR.PST.[3SG]</ta>
            <ta e="T784" id="Seg_8406" s="T783">упасть-MOM-PST.[3SG]</ta>
            <ta e="T785" id="Seg_8407" s="T784">нога-NOM/GEN.3SG</ta>
            <ta e="T786" id="Seg_8408" s="T785">сломать</ta>
            <ta e="T787" id="Seg_8409" s="T786">нога-NOM/GEN.3SG</ta>
            <ta e="T788" id="Seg_8410" s="T787">сломать-PST.[3SG]</ta>
            <ta e="T789" id="Seg_8411" s="T788">я.GEN</ta>
            <ta e="T790" id="Seg_8412" s="T789">рука-LOC</ta>
            <ta e="T791" id="Seg_8413" s="T790">пять.[NOM.SG]</ta>
            <ta e="T792" id="Seg_8414" s="T791">палец-PL-ACC.3SG</ta>
            <ta e="T793" id="Seg_8415" s="T792">а</ta>
            <ta e="T794" id="Seg_8416" s="T793">ты.GEN</ta>
            <ta e="T795" id="Seg_8417" s="T794">рука-LOC</ta>
            <ta e="T796" id="Seg_8418" s="T795">один.[NOM.SG]</ta>
            <ta e="T797" id="Seg_8419" s="T796">NEG.EX.[3SG]</ta>
            <ta e="T798" id="Seg_8420" s="T797">топор-INS</ta>
            <ta e="T799" id="Seg_8421" s="T798">PTCL</ta>
            <ta e="T800" id="Seg_8422" s="T799">резать-PST-2SG</ta>
            <ta e="T801" id="Seg_8423" s="T800">сегодня</ta>
            <ta e="T802" id="Seg_8424" s="T801">ребенок-PL</ta>
            <ta e="T803" id="Seg_8425" s="T802">много</ta>
            <ta e="T804" id="Seg_8426" s="T803">PTCL</ta>
            <ta e="T805" id="Seg_8427" s="T804">теленок.[NOM.SG]</ta>
            <ta e="T806" id="Seg_8428" s="T805">гнать-DUR.PST-3PL</ta>
            <ta e="T807" id="Seg_8429" s="T806">этот.[NOM.SG]</ta>
            <ta e="T808" id="Seg_8430" s="T807">PTCL</ta>
            <ta e="T809" id="Seg_8431" s="T808">вернуться-MOM-PST.[3SG]</ta>
            <ta e="T810" id="Seg_8432" s="T809">и</ta>
            <ta e="T811" id="Seg_8433" s="T810">бежать-MOM-PST.[3SG]</ta>
            <ta e="T812" id="Seg_8434" s="T811">чум-LAT/LOC.3SG</ta>
            <ta e="T813" id="Seg_8435" s="T812">вечер-LOC.ADV</ta>
            <ta e="T814" id="Seg_8436" s="T813">пойти-CVB</ta>
            <ta e="T815" id="Seg_8437" s="T814">исчезнуть-PST.[3SG]</ta>
            <ta e="T816" id="Seg_8438" s="T815">PTCL</ta>
            <ta e="T817" id="Seg_8439" s="T816">вечер</ta>
            <ta e="T818" id="Seg_8440" s="T817">стать-DUR-PRS.[3SG]</ta>
            <ta e="T819" id="Seg_8441" s="T818">солнце.[NOM.SG]</ta>
            <ta e="T820" id="Seg_8442" s="T819">PTCL</ta>
            <ta e="T821" id="Seg_8443" s="T820">сесть-DUR.[3SG]</ta>
            <ta e="T822" id="Seg_8444" s="T821">вечер</ta>
            <ta e="T823" id="Seg_8445" s="T822">стать-DUR-PRS.[3SG]</ta>
            <ta e="T824" id="Seg_8446" s="T823">солнце.[NOM.SG]</ta>
            <ta e="T825" id="Seg_8447" s="T824">сажать-DUR.[3SG]</ta>
            <ta e="T826" id="Seg_8448" s="T825">хватит</ta>
            <ta e="T827" id="Seg_8449" s="T826">говорить-INF.LAT</ta>
            <ta e="T828" id="Seg_8450" s="T827">надо</ta>
            <ta e="T829" id="Seg_8451" s="T828">ложиться-INF.LAT</ta>
            <ta e="T830" id="Seg_8452" s="T829">спать-INF.LAT</ta>
            <ta e="T831" id="Seg_8453" s="T830">утро-LOC.ADV</ta>
            <ta e="T832" id="Seg_8454" s="T831">утро</ta>
            <ta e="T833" id="Seg_8455" s="T832">надо</ta>
            <ta e="T834" id="Seg_8456" s="T833">встать-INF.LAT</ta>
            <ta e="T840" id="Seg_8457" s="T839">пойти-FUT-1SG</ta>
            <ta e="T841" id="Seg_8458" s="T840">цветок-PL</ta>
            <ta e="T842" id="Seg_8459" s="T841">рвать-INF.LAT</ta>
            <ta e="T843" id="Seg_8460" s="T842">снег.[NOM.SG]</ta>
            <ta e="T844" id="Seg_8461" s="T843">дерево-LOC</ta>
            <ta e="T845" id="Seg_8462" s="T844">PTCL</ta>
            <ta e="T846" id="Seg_8463" s="T845">бумага.[NOM.SG]</ta>
            <ta e="T847" id="Seg_8464" s="T846">вешать-DUR.[3SG]</ta>
            <ta e="T848" id="Seg_8465" s="T847">NEG</ta>
            <ta e="T849" id="Seg_8466" s="T848">быть.видным-PRS.[3SG]</ta>
            <ta e="T850" id="Seg_8467" s="T849">что.[NOM.SG]=INDEF</ta>
            <ta e="T851" id="Seg_8468" s="T850">NEG</ta>
            <ta e="T852" id="Seg_8469" s="T851">видеть-PRS-1SG</ta>
            <ta e="T853" id="Seg_8470" s="T852">я.NOM</ta>
            <ta e="T854" id="Seg_8471" s="T853">родственник-NOM/GEN/ACC.1SG</ta>
            <ta e="T856" id="Seg_8472" s="T854">очень</ta>
            <ta e="T858" id="Seg_8473" s="T857">далеко</ta>
            <ta e="T859" id="Seg_8474" s="T858">жить-DUR.[3SG]</ta>
            <ta e="T860" id="Seg_8475" s="T859">надо</ta>
            <ta e="T861" id="Seg_8476" s="T860">два.[NOM.SG]</ta>
            <ta e="T862" id="Seg_8477" s="T861">день.[NOM.SG]</ta>
            <ta e="T863" id="Seg_8478" s="T862">пойти-INF.LAT</ta>
            <ta e="T864" id="Seg_8479" s="T863">а</ta>
            <ta e="T865" id="Seg_8480" s="T864">может.быть</ta>
            <ta e="T866" id="Seg_8481" s="T865">один.[NOM.SG]</ta>
            <ta e="T867" id="Seg_8482" s="T866">день-LAT</ta>
            <ta e="T868" id="Seg_8483" s="T867">пойти-FUT-2SG</ta>
            <ta e="T869" id="Seg_8484" s="T868">я.NOM</ta>
            <ta e="T870" id="Seg_8485" s="T869">этот-LAT</ta>
            <ta e="T871" id="Seg_8486" s="T870">прийти-PST-1SG</ta>
            <ta e="T872" id="Seg_8487" s="T871">а</ta>
            <ta e="T873" id="Seg_8488" s="T872">этот.[NOM.SG]</ta>
            <ta e="T874" id="Seg_8489" s="T873">NEG.EX.[3SG]</ta>
            <ta e="T875" id="Seg_8490" s="T874">куда=INDEF</ta>
            <ta e="T876" id="Seg_8491" s="T875">пойти-CVB</ta>
            <ta e="T877" id="Seg_8492" s="T876">исчезнуть-PST.[3SG]</ta>
            <ta e="T878" id="Seg_8493" s="T877">я.NOM</ta>
            <ta e="T879" id="Seg_8494" s="T878">PTCL</ta>
            <ta e="T917" id="Seg_8495" s="T879">вернуться-MOM-PST.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PKZ">
            <ta e="T5" id="Seg_8496" s="T4">pers</ta>
            <ta e="T6" id="Seg_8497" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_8498" s="T6">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_8499" s="T9">v-v:n.fin</ta>
            <ta e="T11" id="Seg_8500" s="T10">quant</ta>
            <ta e="T12" id="Seg_8501" s="T11">v-v&gt;v-v:pn</ta>
            <ta e="T13" id="Seg_8502" s="T12">adv</ta>
            <ta e="T14" id="Seg_8503" s="T13">pers</ta>
            <ta e="T15" id="Seg_8504" s="T14">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_8505" s="T15">adv</ta>
            <ta e="T17" id="Seg_8506" s="T16">dempro-n:case</ta>
            <ta e="T18" id="Seg_8507" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_8508" s="T18">adj-n:case</ta>
            <ta e="T20" id="Seg_8509" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_8510" s="T20">dempro-n:case</ta>
            <ta e="T22" id="Seg_8511" s="T21">adv</ta>
            <ta e="T27" id="Seg_8512" s="T26">n-n:case.poss</ta>
            <ta e="T28" id="Seg_8513" s="T27">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_8514" s="T28">dempro-n:case</ta>
            <ta e="T33" id="Seg_8515" s="T32">num-n:case</ta>
            <ta e="T34" id="Seg_8516" s="T33">num-n:case</ta>
            <ta e="T35" id="Seg_8517" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_8518" s="T35">conj</ta>
            <ta e="T37" id="Seg_8519" s="T36">adv</ta>
            <ta e="T38" id="Seg_8520" s="T37">num-n:case</ta>
            <ta e="T40" id="Seg_8521" s="T39">que</ta>
            <ta e="T41" id="Seg_8522" s="T40">dempro-n:case</ta>
            <ta e="T42" id="Seg_8523" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_8524" s="T42">adj-n:case</ta>
            <ta e="T44" id="Seg_8525" s="T43">que-n:case=ptcl</ta>
            <ta e="T45" id="Seg_8526" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_8527" s="T45">v-v:tense-v:pn</ta>
            <ta e="T47" id="Seg_8528" s="T46">que-n:case=ptcl</ta>
            <ta e="T48" id="Seg_8529" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_8530" s="T48">v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_8531" s="T49">conj</ta>
            <ta e="T51" id="Seg_8532" s="T50">adj-n:case</ta>
            <ta e="T52" id="Seg_8533" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_8534" s="T52">adv</ta>
            <ta e="T54" id="Seg_8535" s="T53">adj-n:case</ta>
            <ta e="T55" id="Seg_8536" s="T54">quant</ta>
            <ta e="T56" id="Seg_8537" s="T55">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_8538" s="T58">que-n:case</ta>
            <ta e="T62" id="Seg_8539" s="T61">que-n:case</ta>
            <ta e="T63" id="Seg_8540" s="T62">ptcl</ta>
            <ta e="T69" id="Seg_8541" s="T67">que-n:case</ta>
            <ta e="T70" id="Seg_8542" s="T69">que-n:case</ta>
            <ta e="T71" id="Seg_8543" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_8544" s="T71">v-v:tense-v:pn</ta>
            <ta e="T73" id="Seg_8545" s="T72">quant</ta>
            <ta e="T74" id="Seg_8546" s="T73">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_8547" s="T74">dempro-n:case</ta>
            <ta e="T76" id="Seg_8548" s="T75">adv</ta>
            <ta e="T77" id="Seg_8549" s="T76">n-n:case.poss</ta>
            <ta e="T78" id="Seg_8550" s="T77">adj-n:case</ta>
            <ta e="T84" id="Seg_8551" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_8552" s="T84">ptcl</ta>
            <ta e="T86" id="Seg_8553" s="T85">v-v:tense-v:pn</ta>
            <ta e="T87" id="Seg_8554" s="T86">que-n:case</ta>
            <ta e="T88" id="Seg_8555" s="T87">v-v:tense-v:pn</ta>
            <ta e="T89" id="Seg_8556" s="T88">quant</ta>
            <ta e="T90" id="Seg_8557" s="T89">v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_8558" s="T90">n-n:num</ta>
            <ta e="T92" id="Seg_8559" s="T91">v-v:tense-v:pn</ta>
            <ta e="T95" id="Seg_8560" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_8561" s="T95">v-v:tense-v:pn</ta>
            <ta e="T97" id="Seg_8562" s="T96">n-n:case</ta>
            <ta e="T98" id="Seg_8563" s="T97">v-v:tense-v:pn</ta>
            <ta e="T99" id="Seg_8564" s="T98">n-n:case</ta>
            <ta e="T100" id="Seg_8565" s="T99">v-v:tense-v:pn</ta>
            <ta e="T101" id="Seg_8566" s="T100">ptcl</ta>
            <ta e="T102" id="Seg_8567" s="T101">v-v:tense-v:pn</ta>
            <ta e="T103" id="Seg_8568" s="T102">quant</ta>
            <ta e="T104" id="Seg_8569" s="T103">que-n:case</ta>
            <ta e="T105" id="Seg_8570" s="T104">v-v:tense-v:pn</ta>
            <ta e="T106" id="Seg_8571" s="T105">quant</ta>
            <ta e="T107" id="Seg_8572" s="T106">v-v:tense-v:pn</ta>
            <ta e="T108" id="Seg_8573" s="T107">adv</ta>
            <ta e="T111" id="Seg_8574" s="T110">adv</ta>
            <ta e="T112" id="Seg_8575" s="T111">v-v:tense-v:pn</ta>
            <ta e="T113" id="Seg_8576" s="T112">n-n:case</ta>
            <ta e="T114" id="Seg_8577" s="T113">v-v&gt;v-v:pn</ta>
            <ta e="T116" id="Seg_8578" s="T115">que=ptcl</ta>
            <ta e="T117" id="Seg_8579" s="T116">v-v:tense-v:pn</ta>
            <ta e="T118" id="Seg_8580" s="T117">n-n:case</ta>
            <ta e="T119" id="Seg_8581" s="T118">v-v&gt;v-v:pn</ta>
            <ta e="T120" id="Seg_8582" s="T119">n-n:case</ta>
            <ta e="T121" id="Seg_8583" s="T120">v-v&gt;v-v:pn</ta>
            <ta e="T122" id="Seg_8584" s="T121">adv</ta>
            <ta e="T123" id="Seg_8585" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_8586" s="T123">v-v:tense-v:pn</ta>
            <ta e="T125" id="Seg_8587" s="T124">n-n:case</ta>
            <ta e="T126" id="Seg_8588" s="T125">v-v:tense-v:pn</ta>
            <ta e="T127" id="Seg_8589" s="T126">adv</ta>
            <ta e="T128" id="Seg_8590" s="T127">n-n:case</ta>
            <ta e="T129" id="Seg_8591" s="T128">adj-n:case</ta>
            <ta e="T130" id="Seg_8592" s="T129">v-v:tense-v:pn</ta>
            <ta e="T131" id="Seg_8593" s="T130">pers</ta>
            <ta e="T132" id="Seg_8594" s="T131">adv</ta>
            <ta e="T133" id="Seg_8595" s="T132">v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_8596" s="T133">n-n:case</ta>
            <ta e="T135" id="Seg_8597" s="T134">quant</ta>
            <ta e="T136" id="Seg_8598" s="T135">v-v&gt;v-v:pn</ta>
            <ta e="T137" id="Seg_8599" s="T136">v-v:tense-v:pn</ta>
            <ta e="T138" id="Seg_8600" s="T137">dempro-n:case</ta>
            <ta e="T139" id="Seg_8601" s="T138">n-n:case</ta>
            <ta e="T140" id="Seg_8602" s="T139">v-v:tense-v:pn</ta>
            <ta e="T141" id="Seg_8603" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_8604" s="T141">conj</ta>
            <ta e="T143" id="Seg_8605" s="T142">v-v&gt;v-v:pn</ta>
            <ta e="T144" id="Seg_8606" s="T143">pers</ta>
            <ta e="T145" id="Seg_8607" s="T144">v-v&gt;v-v:pn</ta>
            <ta e="T146" id="Seg_8608" s="T145">adv</ta>
            <ta e="T147" id="Seg_8609" s="T146">v-v&gt;v</ta>
            <ta e="T148" id="Seg_8610" s="T147">v-v:tense-v:pn</ta>
            <ta e="T149" id="Seg_8611" s="T148">ptcl</ta>
            <ta e="T150" id="Seg_8612" s="T149">adv</ta>
            <ta e="T151" id="Seg_8613" s="T150">adv</ta>
            <ta e="T152" id="Seg_8614" s="T151">v-v:tense-v:pn</ta>
            <ta e="T153" id="Seg_8615" s="T152">n-n:case</ta>
            <ta e="T154" id="Seg_8616" s="T153">ptcl</ta>
            <ta e="T155" id="Seg_8617" s="T154">v-v:tense-v:pn</ta>
            <ta e="T156" id="Seg_8618" s="T155">n-n:case</ta>
            <ta e="T157" id="Seg_8619" s="T156">ptcl</ta>
            <ta e="T158" id="Seg_8620" s="T157">v-v:tense-v:pn</ta>
            <ta e="T159" id="Seg_8621" s="T158">dempro-n:case</ta>
            <ta e="T160" id="Seg_8622" s="T159">adv</ta>
            <ta e="T161" id="Seg_8623" s="T160">v-v&gt;v-v:pn</ta>
            <ta e="T162" id="Seg_8624" s="T161">v-v&gt;v-v:pn</ta>
            <ta e="T163" id="Seg_8625" s="T162">adv</ta>
            <ta e="T164" id="Seg_8626" s="T163">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T165" id="Seg_8627" s="T164">que</ta>
            <ta e="T166" id="Seg_8628" s="T165">n-n:case</ta>
            <ta e="T167" id="Seg_8629" s="T166">v-v&gt;v-v:pn</ta>
            <ta e="T168" id="Seg_8630" s="T167">adv</ta>
            <ta e="T169" id="Seg_8631" s="T168">adj-n&gt;adj-n:case</ta>
            <ta e="T170" id="Seg_8632" s="T169">n-n:case</ta>
            <ta e="T171" id="Seg_8633" s="T170">ptcl</ta>
            <ta e="T172" id="Seg_8634" s="T171">v-v:tense-v:pn</ta>
            <ta e="T173" id="Seg_8635" s="T172">n-n:case</ta>
            <ta e="T174" id="Seg_8636" s="T173">dempro-n:case</ta>
            <ta e="T175" id="Seg_8637" s="T174">n-n:case</ta>
            <ta e="T176" id="Seg_8638" s="T175">adv</ta>
            <ta e="T177" id="Seg_8639" s="T176">ptcl</ta>
            <ta e="T178" id="Seg_8640" s="T177">adj-n:case</ta>
            <ta e="T179" id="Seg_8641" s="T178">quant</ta>
            <ta e="T180" id="Seg_8642" s="T179">que-n:case</ta>
            <ta e="T181" id="Seg_8643" s="T180">v-v&gt;v-v:pn</ta>
            <ta e="T183" id="Seg_8644" s="T181">conj</ta>
            <ta e="T184" id="Seg_8645" s="T183">quant</ta>
            <ta e="T185" id="Seg_8646" s="T184">que-n:case</ta>
            <ta e="T186" id="Seg_8647" s="T185">v-v&gt;v-v:pn</ta>
            <ta e="T187" id="Seg_8648" s="T186">quant</ta>
            <ta e="T188" id="Seg_8649" s="T187">adv</ta>
            <ta e="T189" id="Seg_8650" s="T188">n.[n:case]</ta>
            <ta e="T190" id="Seg_8651" s="T189">n-n:case</ta>
            <ta e="T191" id="Seg_8652" s="T190">quant</ta>
            <ta e="T192" id="Seg_8653" s="T191">que-n:case</ta>
            <ta e="T193" id="Seg_8654" s="T192">v-v:tense-v:pn</ta>
            <ta e="T195" id="Seg_8655" s="T193">ptcl</ta>
            <ta e="T196" id="Seg_8656" s="T195">quant</ta>
            <ta e="T197" id="Seg_8657" s="T196">v-v&gt;v-v:pn</ta>
            <ta e="T198" id="Seg_8658" s="T197">quant</ta>
            <ta e="T199" id="Seg_8659" s="T198">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T200" id="Seg_8660" s="T199">n-n:case.poss</ta>
            <ta e="T201" id="Seg_8661" s="T200">pers</ta>
            <ta e="T202" id="Seg_8662" s="T201">adv</ta>
            <ta e="T203" id="Seg_8663" s="T202">n-n:case.poss</ta>
            <ta e="T204" id="Seg_8664" s="T203">n-n:ins-n:case</ta>
            <ta e="T205" id="Seg_8665" s="T204">v-v:tense-v:pn</ta>
            <ta e="T206" id="Seg_8666" s="T205">dempro-n:case</ta>
            <ta e="T207" id="Seg_8667" s="T206">v-v:tense-v:pn</ta>
            <ta e="T208" id="Seg_8668" s="T207">propr-n:case</ta>
            <ta e="T209" id="Seg_8669" s="T208">v-v:n.fin</ta>
            <ta e="T210" id="Seg_8670" s="T209">adv</ta>
            <ta e="T211" id="Seg_8671" s="T210">n-n:case</ta>
            <ta e="T212" id="Seg_8672" s="T211">v-v&gt;v-v:pn</ta>
            <ta e="T213" id="Seg_8673" s="T212">que</ta>
            <ta e="T214" id="Seg_8674" s="T213">pers</ta>
            <ta e="T215" id="Seg_8675" s="T214">n-n:case</ta>
            <ta e="T216" id="Seg_8676" s="T215">v-v:tense-v:pn</ta>
            <ta e="T217" id="Seg_8677" s="T216">n-n:case</ta>
            <ta e="T218" id="Seg_8678" s="T217">v-v:tense-v:pn</ta>
            <ta e="T219" id="Seg_8679" s="T218">conj</ta>
            <ta e="T220" id="Seg_8680" s="T219">n-n:case</ta>
            <ta e="T221" id="Seg_8681" s="T220">que</ta>
            <ta e="T222" id="Seg_8682" s="T221">v-v:tense-v:pn</ta>
            <ta e="T223" id="Seg_8683" s="T222">propr-n:case</ta>
            <ta e="T224" id="Seg_8684" s="T223">v-v:tense-v:pn</ta>
            <ta e="T225" id="Seg_8685" s="T224">dempro-n:case</ta>
            <ta e="T226" id="Seg_8686" s="T225">n-n:case</ta>
            <ta e="T227" id="Seg_8687" s="T226">v-v:tense-v:pn</ta>
            <ta e="T228" id="Seg_8688" s="T227">quant</ta>
            <ta e="T229" id="Seg_8689" s="T228">n-n:case</ta>
            <ta e="T230" id="Seg_8690" s="T229">v-v:tense-v:pn</ta>
            <ta e="T231" id="Seg_8691" s="T230">conj</ta>
            <ta e="T232" id="Seg_8692" s="T231">adj-n:case</ta>
            <ta e="T233" id="Seg_8693" s="T232">n-n:case</ta>
            <ta e="T234" id="Seg_8694" s="T233">v-v:tense-v:pn</ta>
            <ta e="T235" id="Seg_8695" s="T234">adv</ta>
            <ta e="T236" id="Seg_8696" s="T235">v-v:tense-v:pn</ta>
            <ta e="T238" id="Seg_8697" s="T237">v-v:tense-v:pn</ta>
            <ta e="T241" id="Seg_8698" s="T240">n-n:case</ta>
            <ta e="T243" id="Seg_8699" s="T241">ptcl</ta>
            <ta e="T247" id="Seg_8700" s="T246">ptcl</ta>
            <ta e="T248" id="Seg_8701" s="T247">v-v:tense-v:pn</ta>
            <ta e="T249" id="Seg_8702" s="T248">pers</ta>
            <ta e="T251" id="Seg_8703" s="T249">ptcl</ta>
            <ta e="T257" id="Seg_8704" s="T256">adv</ta>
            <ta e="T258" id="Seg_8705" s="T257">adj-n:case</ta>
            <ta e="T259" id="Seg_8706" s="T258">ptcl</ta>
            <ta e="T260" id="Seg_8707" s="T259">v-v:tense-v:pn</ta>
            <ta e="T261" id="Seg_8708" s="T260">v-v:n.fin</ta>
            <ta e="T262" id="Seg_8709" s="T261">ptcl</ta>
            <ta e="T267" id="Seg_8710" s="T266">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T268" id="Seg_8711" s="T267">ptcl</ta>
            <ta e="T270" id="Seg_8712" s="T269">ptcl</ta>
            <ta e="T271" id="Seg_8713" s="T270">adj-n:case</ta>
            <ta e="T272" id="Seg_8714" s="T271">n-n:case</ta>
            <ta e="T273" id="Seg_8715" s="T272">n-n:case</ta>
            <ta e="T274" id="Seg_8716" s="T273">adv</ta>
            <ta e="T275" id="Seg_8717" s="T274">ptcl</ta>
            <ta e="T276" id="Seg_8718" s="T275">v-v&gt;v-v:pn</ta>
            <ta e="T277" id="Seg_8719" s="T276">conj</ta>
            <ta e="T278" id="Seg_8720" s="T277">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T279" id="Seg_8721" s="T278">pers</ta>
            <ta e="T280" id="Seg_8722" s="T279">n-n:case</ta>
            <ta e="T281" id="Seg_8723" s="T280">v-v:tense-v:pn</ta>
            <ta e="T282" id="Seg_8724" s="T281">que-n:case=ptcl</ta>
            <ta e="T285" id="Seg_8725" s="T284">v-v:tense-v:pn</ta>
            <ta e="T286" id="Seg_8726" s="T285">adv</ta>
            <ta e="T287" id="Seg_8727" s="T286">pers</ta>
            <ta e="T288" id="Seg_8728" s="T287">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T289" id="Seg_8729" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_8730" s="T289">adv</ta>
            <ta e="T293" id="Seg_8731" s="T292">n-n:case</ta>
            <ta e="T294" id="Seg_8732" s="T293">v-v:tense-v:pn</ta>
            <ta e="T295" id="Seg_8733" s="T294">ptcl</ta>
            <ta e="T296" id="Seg_8734" s="T295">v-v:n.fin</ta>
            <ta e="T297" id="Seg_8735" s="T296">v-v:tense-v:pn</ta>
            <ta e="T298" id="Seg_8736" s="T297">ptcl</ta>
            <ta e="T299" id="Seg_8737" s="T298">n-n:case.poss</ta>
            <ta e="T300" id="Seg_8738" s="T299">v-v:n.fin</ta>
            <ta e="T301" id="Seg_8739" s="T300">ptcl</ta>
            <ta e="T302" id="Seg_8740" s="T301">n-n:case</ta>
            <ta e="T303" id="Seg_8741" s="T302">v-v:n.fin</ta>
            <ta e="T305" id="Seg_8742" s="T304">dempro</ta>
            <ta e="T310" id="Seg_8743" s="T309">dempro</ta>
            <ta e="T311" id="Seg_8744" s="T310">n-n:num-n:case</ta>
            <ta e="T312" id="Seg_8745" s="T311">ptcl</ta>
            <ta e="T313" id="Seg_8746" s="T312">n-n:num</ta>
            <ta e="T314" id="Seg_8747" s="T313">v-v:n.fin</ta>
            <ta e="T315" id="Seg_8748" s="T314">adv</ta>
            <ta e="T316" id="Seg_8749" s="T315">n-n:case</ta>
            <ta e="T317" id="Seg_8750" s="T316">v-v:tense-v:pn</ta>
            <ta e="T318" id="Seg_8751" s="T317">adv</ta>
            <ta e="T319" id="Seg_8752" s="T318">n-n:case</ta>
            <ta e="T320" id="Seg_8753" s="T319">ptcl</ta>
            <ta e="T321" id="Seg_8754" s="T320">v-v:tense-v:pn</ta>
            <ta e="T322" id="Seg_8755" s="T321">n-n:case</ta>
            <ta e="T325" id="Seg_8756" s="T324">n-n:case</ta>
            <ta e="T326" id="Seg_8757" s="T325">v-v&gt;v-v:pn</ta>
            <ta e="T338" id="Seg_8758" s="T337">que-n:case=ptcl</ta>
            <ta e="T339" id="Seg_8759" s="T338">ptcl</ta>
            <ta e="T340" id="Seg_8760" s="T339">v-v:tense-v:pn</ta>
            <ta e="T341" id="Seg_8761" s="T340">pers</ta>
            <ta e="T342" id="Seg_8762" s="T341">n-n:case.poss</ta>
            <ta e="T344" id="Seg_8763" s="T342">adv</ta>
            <ta e="T347" id="Seg_8764" s="T346">pers</ta>
            <ta e="T348" id="Seg_8765" s="T347">n-n:case.poss</ta>
            <ta e="T349" id="Seg_8766" s="T348">adv</ta>
            <ta e="T350" id="Seg_8767" s="T349">adj</ta>
            <ta e="T351" id="Seg_8768" s="T350">quant</ta>
            <ta e="T352" id="Seg_8769" s="T351">v-v&gt;v-v:pn</ta>
            <ta e="T353" id="Seg_8770" s="T352">pers</ta>
            <ta e="T354" id="Seg_8771" s="T353">que</ta>
            <ta e="T355" id="Seg_8772" s="T354">v-v:tense-v:pn</ta>
            <ta e="T356" id="Seg_8773" s="T355">n-n&gt;v-v:n.fin</ta>
            <ta e="T357" id="Seg_8774" s="T356">v-v:tense-v:pn</ta>
            <ta e="T358" id="Seg_8775" s="T357">ptcl</ta>
            <ta e="T359" id="Seg_8776" s="T358">adj-n:case</ta>
            <ta e="T360" id="Seg_8777" s="T359">n-n:case</ta>
            <ta e="T361" id="Seg_8778" s="T360">v-v:n.fin</ta>
            <ta e="T362" id="Seg_8779" s="T361">ptcl</ta>
            <ta e="T363" id="Seg_8780" s="T362">n-n:case</ta>
            <ta e="T366" id="Seg_8781" s="T365">v-v:n.fin</ta>
            <ta e="T367" id="Seg_8782" s="T366">n-n:num</ta>
            <ta e="T368" id="Seg_8783" s="T367">v-v:n.fin</ta>
            <ta e="T369" id="Seg_8784" s="T368">pers</ta>
            <ta e="T370" id="Seg_8785" s="T369">num-n:case</ta>
            <ta e="T371" id="Seg_8786" s="T370">n-n:case</ta>
            <ta e="T372" id="Seg_8787" s="T371">v-v:tense-v:pn</ta>
            <ta e="T373" id="Seg_8788" s="T372">ptcl</ta>
            <ta e="T374" id="Seg_8789" s="T373">pers</ta>
            <ta e="T375" id="Seg_8790" s="T374">num-n:case</ta>
            <ta e="T376" id="Seg_8791" s="T375">v-v:n.fin</ta>
            <ta e="T377" id="Seg_8792" s="T376">num-n:case</ta>
            <ta e="T378" id="Seg_8793" s="T377">refl-n:case.poss</ta>
            <ta e="T379" id="Seg_8794" s="T378">v-v:n.fin</ta>
            <ta e="T380" id="Seg_8795" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_8796" s="T380">que=ptcl</ta>
            <ta e="T382" id="Seg_8797" s="T381">v-v:tense-v:pn</ta>
            <ta e="T383" id="Seg_8798" s="T382">n-n:case</ta>
            <ta e="T384" id="Seg_8799" s="T383">adv</ta>
            <ta e="T385" id="Seg_8800" s="T384">v-v:tense-v:pn</ta>
            <ta e="T387" id="Seg_8801" s="T386">propr-n:case</ta>
            <ta e="T388" id="Seg_8802" s="T387">ptcl</ta>
            <ta e="T389" id="Seg_8803" s="T388">v-v&gt;v-v:pn</ta>
            <ta e="T390" id="Seg_8804" s="T389">ptcl</ta>
            <ta e="T391" id="Seg_8805" s="T390">v-v:tense-v:pn</ta>
            <ta e="T392" id="Seg_8806" s="T391">n-n:case.poss</ta>
            <ta e="T393" id="Seg_8807" s="T392">v-v:tense-v:pn</ta>
            <ta e="T394" id="Seg_8808" s="T393">n-n:case-n:case.poss</ta>
            <ta e="T395" id="Seg_8809" s="T394">n-n:case</ta>
            <ta e="T396" id="Seg_8810" s="T395">v-v:pn</ta>
            <ta e="T397" id="Seg_8811" s="T396">n-n:case.poss</ta>
            <ta e="T398" id="Seg_8812" s="T397">ptcl</ta>
            <ta e="T399" id="Seg_8813" s="T398">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T400" id="Seg_8814" s="T399">ptcl</ta>
            <ta e="T401" id="Seg_8815" s="T400">v-v:tense-v:pn</ta>
            <ta e="T404" id="Seg_8816" s="T403">n-n:case</ta>
            <ta e="T405" id="Seg_8817" s="T404">aux-v:mood.pn</ta>
            <ta e="T406" id="Seg_8818" s="T405">v-v:n.fin</ta>
            <ta e="T410" id="Seg_8819" s="T409">adv</ta>
            <ta e="T411" id="Seg_8820" s="T410">n-n:case-%%</ta>
            <ta e="T412" id="Seg_8821" s="T411">v-v:tense-v:pn</ta>
            <ta e="T413" id="Seg_8822" s="T412">que=ptcl</ta>
            <ta e="T414" id="Seg_8823" s="T413">pers</ta>
            <ta e="T415" id="Seg_8824" s="T414">v-v:tense-v:pn</ta>
            <ta e="T416" id="Seg_8825" s="T415">adv</ta>
            <ta e="T417" id="Seg_8826" s="T416">ptcl</ta>
            <ta e="T418" id="Seg_8827" s="T417">v-v:ins-v:mood.pn</ta>
            <ta e="T419" id="Seg_8828" s="T418">adv</ta>
            <ta e="T420" id="Seg_8829" s="T419">v-v:ins-v:mood.pn</ta>
            <ta e="T421" id="Seg_8830" s="T420">ptcl</ta>
            <ta e="T422" id="Seg_8831" s="T421">quant</ta>
            <ta e="T423" id="Seg_8832" s="T422">n-n:case</ta>
            <ta e="T424" id="Seg_8833" s="T423">v-v:tense-v:pn</ta>
            <ta e="T425" id="Seg_8834" s="T424">adv</ta>
            <ta e="T426" id="Seg_8835" s="T425">aux-v:mood.pn</ta>
            <ta e="T427" id="Seg_8836" s="T426">v-v:ins-v:n.fin</ta>
            <ta e="T428" id="Seg_8837" s="T427">conj</ta>
            <ta e="T429" id="Seg_8838" s="T428">aux-v:mood.pn</ta>
            <ta e="T430" id="Seg_8839" s="T429">pers</ta>
            <ta e="T431" id="Seg_8840" s="T430">aux-v:mood.pn</ta>
            <ta e="T432" id="Seg_8841" s="T431">v-v&gt;v-v:n.fin</ta>
            <ta e="T433" id="Seg_8842" s="T432">ptcl</ta>
            <ta e="T434" id="Seg_8843" s="T433">n-n:case</ta>
            <ta e="T435" id="Seg_8844" s="T434">quant</ta>
            <ta e="T436" id="Seg_8845" s="T435">v-v&gt;v-v:pn</ta>
            <ta e="T437" id="Seg_8846" s="T436">que</ta>
            <ta e="T438" id="Seg_8847" s="T437">pers</ta>
            <ta e="T439" id="Seg_8848" s="T438">pers</ta>
            <ta e="T440" id="Seg_8849" s="T439">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T441" id="Seg_8850" s="T440">pers</ta>
            <ta e="T442" id="Seg_8851" s="T441">adv</ta>
            <ta e="T443" id="Seg_8852" s="T442">n-n:case</ta>
            <ta e="T444" id="Seg_8853" s="T443">v-v:tense-v:pn</ta>
            <ta e="T445" id="Seg_8854" s="T444">v-v:tense-v:pn</ta>
            <ta e="T446" id="Seg_8855" s="T445">n-n:num</ta>
            <ta e="T447" id="Seg_8856" s="T446">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T448" id="Seg_8857" s="T447">v-v:tense-v:pn</ta>
            <ta e="T449" id="Seg_8858" s="T448">n-n:case</ta>
            <ta e="T450" id="Seg_8859" s="T449">v-v&gt;v-v:pn</ta>
            <ta e="T451" id="Seg_8860" s="T450">quant</ta>
            <ta e="T452" id="Seg_8861" s="T451">que-n:case</ta>
            <ta e="T453" id="Seg_8862" s="T452">v-v:tense-v:pn</ta>
            <ta e="T454" id="Seg_8863" s="T453">v-v:mood.pn</ta>
            <ta e="T455" id="Seg_8864" s="T454">pers</ta>
            <ta e="T458" id="Seg_8865" s="T457">que-n:case</ta>
            <ta e="T459" id="Seg_8866" s="T458">adv</ta>
            <ta e="T460" id="Seg_8867" s="T459">v-v&gt;v-v:pn</ta>
            <ta e="T461" id="Seg_8868" s="T460">pers</ta>
            <ta e="T462" id="Seg_8869" s="T461">adv</ta>
            <ta e="T463" id="Seg_8870" s="T462">n-n&gt;v-v:n.fin</ta>
            <ta e="T464" id="Seg_8871" s="T463">v-v:tense-v:pn</ta>
            <ta e="T465" id="Seg_8872" s="T464">n-n:case</ta>
            <ta e="T466" id="Seg_8873" s="T465">v-v:tense-v:pn</ta>
            <ta e="T467" id="Seg_8874" s="T466">adv</ta>
            <ta e="T468" id="Seg_8875" s="T467">v-v:tense-v:pn</ta>
            <ta e="T469" id="Seg_8876" s="T468">n-n:num</ta>
            <ta e="T471" id="Seg_8877" s="T470">v-v:tense-v:pn</ta>
            <ta e="T472" id="Seg_8878" s="T471">n-n:case</ta>
            <ta e="T473" id="Seg_8879" s="T472">v-v:tense-v:pn</ta>
            <ta e="T474" id="Seg_8880" s="T473">adv</ta>
            <ta e="T475" id="Seg_8881" s="T474">v-v:n.fin</ta>
            <ta e="T476" id="Seg_8882" s="T475">n-n:num</ta>
            <ta e="T477" id="Seg_8883" s="T476">v-v:tense-v:pn</ta>
            <ta e="T479" id="Seg_8884" s="T478">n-n:case</ta>
            <ta e="T480" id="Seg_8885" s="T479">v-v:tense-v:pn</ta>
            <ta e="T481" id="Seg_8886" s="T480">n-n:case</ta>
            <ta e="T482" id="Seg_8887" s="T481">v-v:tense-v:pn</ta>
            <ta e="T483" id="Seg_8888" s="T482">v-v:n.fin</ta>
            <ta e="T484" id="Seg_8889" s="T483">adj-n:case</ta>
            <ta e="T485" id="Seg_8890" s="T484">n-n:case</ta>
            <ta e="T486" id="Seg_8891" s="T485">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T490" id="Seg_8892" s="T489">n-n:num</ta>
            <ta e="T491" id="Seg_8893" s="T490">ptcl</ta>
            <ta e="T492" id="Seg_8894" s="T491">n-n:case</ta>
            <ta e="T493" id="Seg_8895" s="T492">v-v&gt;v-v:pn</ta>
            <ta e="T494" id="Seg_8896" s="T493">adv</ta>
            <ta e="T495" id="Seg_8897" s="T494">quant</ta>
            <ta e="T496" id="Seg_8898" s="T495">n-n:case</ta>
            <ta e="T497" id="Seg_8899" s="T496">v-v:tense-v:pn</ta>
            <ta e="T498" id="Seg_8900" s="T497">pers</ta>
            <ta e="T501" id="Seg_8901" s="T500">pers</ta>
            <ta e="T502" id="Seg_8902" s="T501">adv</ta>
            <ta e="T503" id="Seg_8903" s="T502">n-n:case</ta>
            <ta e="T504" id="Seg_8904" s="T503">v-v:tense-v:pn</ta>
            <ta e="T505" id="Seg_8905" s="T504">ptcl</ta>
            <ta e="T506" id="Seg_8906" s="T505">adj-n:case</ta>
            <ta e="T507" id="Seg_8907" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_8908" s="T507">adj-n:case</ta>
            <ta e="T509" id="Seg_8909" s="T508">ptcl</ta>
            <ta e="T510" id="Seg_8910" s="T509">pers</ta>
            <ta e="T511" id="Seg_8911" s="T510">dempro-n:case</ta>
            <ta e="T512" id="Seg_8912" s="T511">n-n:case</ta>
            <ta e="T513" id="Seg_8913" s="T512">dempro-n:case</ta>
            <ta e="T516" id="Seg_8914" s="T515">n-n:num-n:case</ta>
            <ta e="T517" id="Seg_8915" s="T516">v-v:tense-v:pn</ta>
            <ta e="T518" id="Seg_8916" s="T517">n-n:num</ta>
            <ta e="T519" id="Seg_8917" s="T518">ptcl</ta>
            <ta e="T520" id="Seg_8918" s="T519">n-n:case</ta>
            <ta e="T521" id="Seg_8919" s="T520">v-v&gt;v-v:pn</ta>
            <ta e="T522" id="Seg_8920" s="T521">conj</ta>
            <ta e="T523" id="Seg_8921" s="T522">ptcl</ta>
            <ta e="T524" id="Seg_8922" s="T523">v-v&gt;v-v:pn</ta>
            <ta e="T525" id="Seg_8923" s="T524">n-n:case</ta>
            <ta e="T526" id="Seg_8924" s="T525">ptcl</ta>
            <ta e="T527" id="Seg_8925" s="T526">dempro-n:case</ta>
            <ta e="T528" id="Seg_8926" s="T527">n-n:case.poss</ta>
            <ta e="T529" id="Seg_8927" s="T528">v-v:pn</ta>
            <ta e="T530" id="Seg_8928" s="T529">ptcl</ta>
            <ta e="T531" id="Seg_8929" s="T530">n</ta>
            <ta e="T532" id="Seg_8930" s="T531">v-v:tense-v:pn</ta>
            <ta e="T533" id="Seg_8931" s="T532">n</ta>
            <ta e="T534" id="Seg_8932" s="T533">v-v:tense-v:pn</ta>
            <ta e="T536" id="Seg_8933" s="T534">adv</ta>
            <ta e="T540" id="Seg_8934" s="T539">que-n:case=ptcl</ta>
            <ta e="T541" id="Seg_8935" s="T540">ptcl</ta>
            <ta e="T542" id="Seg_8936" s="T541">v-v:tense-v:pn</ta>
            <ta e="T543" id="Seg_8937" s="T542">pers</ta>
            <ta e="T544" id="Seg_8938" s="T543">dempro-n:case</ta>
            <ta e="T545" id="Seg_8939" s="T544">ptcl</ta>
            <ta e="T546" id="Seg_8940" s="T545">v-v:tense-v:pn</ta>
            <ta e="T547" id="Seg_8941" s="T546">que</ta>
            <ta e="T548" id="Seg_8942" s="T547">v-v:tense-v:pn</ta>
            <ta e="T549" id="Seg_8943" s="T548">pers</ta>
            <ta e="T550" id="Seg_8944" s="T549">n-n:case.poss</ta>
            <ta e="T551" id="Seg_8945" s="T550">n-n:num</ta>
            <ta e="T552" id="Seg_8946" s="T551">v-v:tense-v:pn</ta>
            <ta e="T553" id="Seg_8947" s="T552">n-n:case</ta>
            <ta e="T554" id="Seg_8948" s="T553">n-n:case</ta>
            <ta e="T555" id="Seg_8949" s="T554">v</ta>
            <ta e="T556" id="Seg_8950" s="T555">v-v:tense-v:pn</ta>
            <ta e="T557" id="Seg_8951" s="T556">conj</ta>
            <ta e="T558" id="Seg_8952" s="T557">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T559" id="Seg_8953" s="T558">n-n:case</ta>
            <ta e="T560" id="Seg_8954" s="T559">v-v:tense-v:pn</ta>
            <ta e="T561" id="Seg_8955" s="T560">n-n:num-n:case</ta>
            <ta e="T562" id="Seg_8956" s="T561">ptcl</ta>
            <ta e="T563" id="Seg_8957" s="T562">n-n:case</ta>
            <ta e="T564" id="Seg_8958" s="T563">conj</ta>
            <ta e="T565" id="Seg_8959" s="T564">n-n:case</ta>
            <ta e="T566" id="Seg_8960" s="T565">n-n:case</ta>
            <ta e="T567" id="Seg_8961" s="T566">v-v:tense-v:pn</ta>
            <ta e="T569" id="Seg_8962" s="T567">n-n:num</ta>
            <ta e="T570" id="Seg_8963" s="T569">v-v:tense-v:pn</ta>
            <ta e="T571" id="Seg_8964" s="T570">ptcl</ta>
            <ta e="T572" id="Seg_8965" s="T571">n-n:num-n:case</ta>
            <ta e="T573" id="Seg_8966" s="T572">conj</ta>
            <ta e="T576" id="Seg_8967" s="T575">n-n:case</ta>
            <ta e="T577" id="Seg_8968" s="T576">v-v:tense-v:pn</ta>
            <ta e="T578" id="Seg_8969" s="T577">n-n:case</ta>
            <ta e="T579" id="Seg_8970" s="T578">conj</ta>
            <ta e="T580" id="Seg_8971" s="T579">n-n:case</ta>
            <ta e="T581" id="Seg_8972" s="T580">v-v:tense-v:pn</ta>
            <ta e="T582" id="Seg_8973" s="T581">v-v:tense-v:pn</ta>
            <ta e="T593" id="Seg_8974" s="T592">n-n:case.poss</ta>
            <ta e="T594" id="Seg_8975" s="T593">ptcl</ta>
            <ta e="T595" id="Seg_8976" s="T594">v-v:tense-v:pn</ta>
            <ta e="T596" id="Seg_8977" s="T595">pers</ta>
            <ta e="T597" id="Seg_8978" s="T596">ptcl</ta>
            <ta e="T598" id="Seg_8979" s="T597">n-n:case.poss</ta>
            <ta e="T599" id="Seg_8980" s="T598">v-v&gt;v-v:pn</ta>
            <ta e="T600" id="Seg_8981" s="T599">n-n:case.poss</ta>
            <ta e="T601" id="Seg_8982" s="T600">v-v:tense-v:pn</ta>
            <ta e="T602" id="Seg_8983" s="T601">adv</ta>
            <ta e="T603" id="Seg_8984" s="T602">pers</ta>
            <ta e="T604" id="Seg_8985" s="T603">ptcl</ta>
            <ta e="T605" id="Seg_8986" s="T604">n-n:case.poss</ta>
            <ta e="T606" id="Seg_8987" s="T605">adj-n:case</ta>
            <ta e="T609" id="Seg_8988" s="T608">adv</ta>
            <ta e="T610" id="Seg_8989" s="T609">v-v:tense-v:pn</ta>
            <ta e="T611" id="Seg_8990" s="T610">adv</ta>
            <ta e="T612" id="Seg_8991" s="T611">adj-n:case</ta>
            <ta e="T613" id="Seg_8992" s="T612">ptcl</ta>
            <ta e="T614" id="Seg_8993" s="T613">n-n:case</ta>
            <ta e="T615" id="Seg_8994" s="T614">ptcl</ta>
            <ta e="T616" id="Seg_8995" s="T615">v-v&gt;v-v:pn</ta>
            <ta e="T617" id="Seg_8996" s="T616">v-v:tense-v:pn</ta>
            <ta e="T618" id="Seg_8997" s="T617">adj-n:case</ta>
            <ta e="T619" id="Seg_8998" s="T618">n-n:case</ta>
            <ta e="T620" id="Seg_8999" s="T619">n-n:case</ta>
            <ta e="T621" id="Seg_9000" s="T620">ptcl</ta>
            <ta e="T622" id="Seg_9001" s="T621">v-v:tense-v:pn</ta>
            <ta e="T627" id="Seg_9002" s="T626">que-n:case</ta>
            <ta e="T628" id="Seg_9003" s="T627">pers</ta>
            <ta e="T629" id="Seg_9004" s="T628">v-v:n.fin</ta>
            <ta e="T630" id="Seg_9005" s="T629">pers</ta>
            <ta e="T631" id="Seg_9006" s="T630">quant</ta>
            <ta e="T632" id="Seg_9007" s="T631">v-v:tense-v:pn</ta>
            <ta e="T633" id="Seg_9008" s="T632">adv</ta>
            <ta e="T634" id="Seg_9009" s="T633">v-v:n.fin</ta>
            <ta e="T635" id="Seg_9010" s="T634">n-n:num</ta>
            <ta e="T636" id="Seg_9011" s="T635">ptcl</ta>
            <ta e="T637" id="Seg_9012" s="T636">v-v:tense-v:pn</ta>
            <ta e="T638" id="Seg_9013" s="T637">dempro-n:case</ta>
            <ta e="T639" id="Seg_9014" s="T638">n-n:case</ta>
            <ta e="T640" id="Seg_9015" s="T639">n-n:case</ta>
            <ta e="T641" id="Seg_9016" s="T640">v-v:tense-v:pn</ta>
            <ta e="T642" id="Seg_9017" s="T641">ptcl</ta>
            <ta e="T643" id="Seg_9018" s="T642">dempro-n:case</ta>
            <ta e="T644" id="Seg_9019" s="T643">v-v:n.fin</ta>
            <ta e="T645" id="Seg_9020" s="T644">dempro-n:case</ta>
            <ta e="T646" id="Seg_9021" s="T645">n-n:case</ta>
            <ta e="T647" id="Seg_9022" s="T646">v-v:pn</ta>
            <ta e="T648" id="Seg_9023" s="T647">v-v:n.fin</ta>
            <ta e="T649" id="Seg_9024" s="T648">quant</ta>
            <ta e="T656" id="Seg_9025" s="T655">que</ta>
            <ta e="T657" id="Seg_9026" s="T656">v-v:n.fin</ta>
            <ta e="T658" id="Seg_9027" s="T657">que-n:case</ta>
            <ta e="T659" id="Seg_9028" s="T658">v-v:n.fin</ta>
            <ta e="T660" id="Seg_9029" s="T659">dempro-n:case</ta>
            <ta e="T661" id="Seg_9030" s="T660">adv</ta>
            <ta e="T664" id="Seg_9031" s="T663">quant</ta>
            <ta e="T665" id="Seg_9032" s="T664">ptcl</ta>
            <ta e="T666" id="Seg_9033" s="T665">v-v:n.fin</ta>
            <ta e="T670" id="Seg_9034" s="T668">ptcl</ta>
            <ta e="T671" id="Seg_9035" s="T670">v-v:n.fin</ta>
            <ta e="T672" id="Seg_9036" s="T671">v-v&gt;v-v:n.fin</ta>
            <ta e="T673" id="Seg_9037" s="T672">que=ptcl</ta>
            <ta e="T674" id="Seg_9038" s="T673">v-v:n.fin</ta>
            <ta e="T675" id="Seg_9039" s="T674">pers</ta>
            <ta e="T676" id="Seg_9040" s="T675">adv</ta>
            <ta e="T677" id="Seg_9041" s="T676">adv</ta>
            <ta e="T678" id="Seg_9042" s="T677">ptcl</ta>
            <ta e="T681" id="Seg_9043" s="T680">v-v:tense-v:pn</ta>
            <ta e="T682" id="Seg_9044" s="T681">v-v:n.fin</ta>
            <ta e="T685" id="Seg_9045" s="T684">adv</ta>
            <ta e="T686" id="Seg_9046" s="T685">ptcl</ta>
            <ta e="T688" id="Seg_9047" s="T687">ptcl</ta>
            <ta e="T690" id="Seg_9048" s="T689">ptcl</ta>
            <ta e="T691" id="Seg_9049" s="T690">v-v:tense-v:pn</ta>
            <ta e="T692" id="Seg_9050" s="T691">v-v:n.fin</ta>
            <ta e="T697" id="Seg_9051" s="T696">v-v:tense-v:pn</ta>
            <ta e="T698" id="Seg_9052" s="T697">que-n:case=ptcl</ta>
            <ta e="T699" id="Seg_9053" s="T698">ptcl</ta>
            <ta e="T700" id="Seg_9054" s="T699">v-v:tense-v:pn</ta>
            <ta e="T701" id="Seg_9055" s="T700">dempro-n:case</ta>
            <ta e="T702" id="Seg_9056" s="T701">n-n:case</ta>
            <ta e="T703" id="Seg_9057" s="T702">n-n:case.poss</ta>
            <ta e="T704" id="Seg_9058" s="T703">quant</ta>
            <ta e="T705" id="Seg_9059" s="T704">adj-n:case</ta>
            <ta e="T706" id="Seg_9060" s="T705">v-v:tense-v:pn</ta>
            <ta e="T707" id="Seg_9061" s="T706">adv</ta>
            <ta e="T708" id="Seg_9062" s="T707">quant</ta>
            <ta e="T709" id="Seg_9063" s="T708">adv</ta>
            <ta e="T710" id="Seg_9064" s="T709">adj-n:case</ta>
            <ta e="T711" id="Seg_9065" s="T710">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T712" id="Seg_9066" s="T711">n-n:case</ta>
            <ta e="T713" id="Seg_9067" s="T712">adj-n:case</ta>
            <ta e="T714" id="Seg_9068" s="T713">n.[n:case]</ta>
            <ta e="T715" id="Seg_9069" s="T714">v-v:tense-v:pn</ta>
            <ta e="T716" id="Seg_9070" s="T715">conj</ta>
            <ta e="T717" id="Seg_9071" s="T716">adj-n:case</ta>
            <ta e="T718" id="Seg_9072" s="T717">n.[n:case]</ta>
            <ta e="T719" id="Seg_9073" s="T718">v-v:tense-v:pn</ta>
            <ta e="T720" id="Seg_9074" s="T719">adj-n:case</ta>
            <ta e="T721" id="Seg_9075" s="T720">n.[n:case]</ta>
            <ta e="T722" id="Seg_9076" s="T721">v-v:tense-v:pn</ta>
            <ta e="T723" id="Seg_9077" s="T722">conj</ta>
            <ta e="T724" id="Seg_9078" s="T723">adj-n:case</ta>
            <ta e="T725" id="Seg_9079" s="T724">n-n:case.poss</ta>
            <ta e="T726" id="Seg_9080" s="T725">ptcl</ta>
            <ta e="T727" id="Seg_9081" s="T726">v-v:tense-v:pn</ta>
            <ta e="T728" id="Seg_9082" s="T727">pers</ta>
            <ta e="T729" id="Seg_9083" s="T728">v-v:tense-v:pn</ta>
            <ta e="T731" id="Seg_9084" s="T730">adj-n:case</ta>
            <ta e="T732" id="Seg_9085" s="T731">n-n:case.poss</ta>
            <ta e="T733" id="Seg_9086" s="T732">v-v:tense-v:pn</ta>
            <ta e="T735" id="Seg_9087" s="T734">pers</ta>
            <ta e="T736" id="Seg_9088" s="T735">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T737" id="Seg_9089" s="T736">conj</ta>
            <ta e="T738" id="Seg_9090" s="T737">n-n:case.poss</ta>
            <ta e="T739" id="Seg_9091" s="T738">v</ta>
            <ta e="T740" id="Seg_9092" s="T739">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T741" id="Seg_9093" s="T740">adv</ta>
            <ta e="T742" id="Seg_9094" s="T741">dempro-n:case</ta>
            <ta e="T743" id="Seg_9095" s="T742">n-n:case.poss</ta>
            <ta e="T744" id="Seg_9096" s="T743">v-v:tense-v:pn</ta>
            <ta e="T745" id="Seg_9097" s="T744">ptcl</ta>
            <ta e="T752" id="Seg_9098" s="T901">adv</ta>
            <ta e="T753" id="Seg_9099" s="T752">n-n:case.poss</ta>
            <ta e="T918" id="Seg_9100" s="T753">ptcl</ta>
            <ta e="T755" id="Seg_9101" s="T918">v-v:tense-v:pn</ta>
            <ta e="T759" id="Seg_9102" s="T758">adv</ta>
            <ta e="T760" id="Seg_9103" s="T759">ptcl</ta>
            <ta e="T761" id="Seg_9104" s="T760">v-v:tense-v:pn</ta>
            <ta e="T762" id="Seg_9105" s="T761">que-n:case=ptcl</ta>
            <ta e="T763" id="Seg_9106" s="T762">v-v:n.fin</ta>
            <ta e="T764" id="Seg_9107" s="T763">ptcl</ta>
            <ta e="T765" id="Seg_9108" s="T764">n-n:case</ta>
            <ta e="T766" id="Seg_9109" s="T765">v-v:n.fin</ta>
            <ta e="T767" id="Seg_9110" s="T766">ptcl</ta>
            <ta e="T772" id="Seg_9111" s="T771">n-n:case</ta>
            <ta e="T774" id="Seg_9112" s="T773">v-v:tense-v:pn</ta>
            <ta e="T775" id="Seg_9113" s="T774">quant</ta>
            <ta e="T782" id="Seg_9114" s="T781">n-n:case</ta>
            <ta e="T783" id="Seg_9115" s="T782">v-v:tense-v:pn</ta>
            <ta e="T784" id="Seg_9116" s="T783">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T785" id="Seg_9117" s="T784">n-n:case.poss</ta>
            <ta e="T786" id="Seg_9118" s="T785">v</ta>
            <ta e="T787" id="Seg_9119" s="T786">n-n:case.poss</ta>
            <ta e="T788" id="Seg_9120" s="T787">v-v:tense-v:pn</ta>
            <ta e="T789" id="Seg_9121" s="T788">pers</ta>
            <ta e="T790" id="Seg_9122" s="T789">n-n:case</ta>
            <ta e="T791" id="Seg_9123" s="T790">num-n:case</ta>
            <ta e="T792" id="Seg_9124" s="T791">n-n:num-n:case.poss</ta>
            <ta e="T793" id="Seg_9125" s="T792">conj</ta>
            <ta e="T794" id="Seg_9126" s="T793">pers</ta>
            <ta e="T795" id="Seg_9127" s="T794">n-n:case</ta>
            <ta e="T796" id="Seg_9128" s="T795">adj-n:case</ta>
            <ta e="T797" id="Seg_9129" s="T796">v-v:pn</ta>
            <ta e="T798" id="Seg_9130" s="T797">n-n:case</ta>
            <ta e="T799" id="Seg_9131" s="T798">ptcl</ta>
            <ta e="T800" id="Seg_9132" s="T799">v-v:tense-v:pn</ta>
            <ta e="T801" id="Seg_9133" s="T800">adv</ta>
            <ta e="T802" id="Seg_9134" s="T801">n-n:num</ta>
            <ta e="T803" id="Seg_9135" s="T802">quant</ta>
            <ta e="T804" id="Seg_9136" s="T803">ptcl</ta>
            <ta e="T805" id="Seg_9137" s="T804">n-n:case</ta>
            <ta e="T806" id="Seg_9138" s="T805">v-v:tense-v:pn</ta>
            <ta e="T807" id="Seg_9139" s="T806">dempro-n:case</ta>
            <ta e="T808" id="Seg_9140" s="T807">ptcl</ta>
            <ta e="T809" id="Seg_9141" s="T808">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T810" id="Seg_9142" s="T809">conj</ta>
            <ta e="T811" id="Seg_9143" s="T810">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T812" id="Seg_9144" s="T811">n-n:case.poss</ta>
            <ta e="T813" id="Seg_9145" s="T812">n-adv:case</ta>
            <ta e="T814" id="Seg_9146" s="T813">v-v:n.fin</ta>
            <ta e="T815" id="Seg_9147" s="T814">v-v:tense-v:pn</ta>
            <ta e="T816" id="Seg_9148" s="T815">ptcl</ta>
            <ta e="T817" id="Seg_9149" s="T816">n</ta>
            <ta e="T818" id="Seg_9150" s="T817">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T819" id="Seg_9151" s="T818">n-n:case</ta>
            <ta e="T820" id="Seg_9152" s="T819">ptcl</ta>
            <ta e="T821" id="Seg_9153" s="T820">v-v&gt;v-v:pn</ta>
            <ta e="T822" id="Seg_9154" s="T821">n</ta>
            <ta e="T823" id="Seg_9155" s="T822">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T824" id="Seg_9156" s="T823">n-n:case</ta>
            <ta e="T825" id="Seg_9157" s="T824">v-v&gt;v-v:pn</ta>
            <ta e="T826" id="Seg_9158" s="T825">ptcl</ta>
            <ta e="T827" id="Seg_9159" s="T826">v-v:n.fin</ta>
            <ta e="T828" id="Seg_9160" s="T827">ptcl</ta>
            <ta e="T829" id="Seg_9161" s="T828">v-v:n.fin</ta>
            <ta e="T830" id="Seg_9162" s="T829">v-v:n.fin</ta>
            <ta e="T831" id="Seg_9163" s="T830">n-adv:case</ta>
            <ta e="T832" id="Seg_9164" s="T831">n</ta>
            <ta e="T833" id="Seg_9165" s="T832">ptcl</ta>
            <ta e="T834" id="Seg_9166" s="T833">v-v:n.fin</ta>
            <ta e="T840" id="Seg_9167" s="T839">v-v:tense-v:pn</ta>
            <ta e="T841" id="Seg_9168" s="T840">n-n:num</ta>
            <ta e="T842" id="Seg_9169" s="T841">v-v:n.fin</ta>
            <ta e="T843" id="Seg_9170" s="T842">n-n:case</ta>
            <ta e="T844" id="Seg_9171" s="T843">n-n:case</ta>
            <ta e="T845" id="Seg_9172" s="T844">ptcl</ta>
            <ta e="T846" id="Seg_9173" s="T845">n-n:case</ta>
            <ta e="T847" id="Seg_9174" s="T846">v-v&gt;v-v:pn</ta>
            <ta e="T848" id="Seg_9175" s="T847">ptcl</ta>
            <ta e="T849" id="Seg_9176" s="T848">v-v:tense-v:pn</ta>
            <ta e="T850" id="Seg_9177" s="T849">que-n:case=ptcl</ta>
            <ta e="T851" id="Seg_9178" s="T850">ptcl</ta>
            <ta e="T852" id="Seg_9179" s="T851">v-v:tense-v:pn</ta>
            <ta e="T853" id="Seg_9180" s="T852">pers</ta>
            <ta e="T854" id="Seg_9181" s="T853">n-n:case.poss</ta>
            <ta e="T856" id="Seg_9182" s="T854">adv</ta>
            <ta e="T858" id="Seg_9183" s="T857">adv</ta>
            <ta e="T859" id="Seg_9184" s="T858">v-v&gt;v-v:pn</ta>
            <ta e="T860" id="Seg_9185" s="T859">ptcl</ta>
            <ta e="T861" id="Seg_9186" s="T860">num-n:case</ta>
            <ta e="T862" id="Seg_9187" s="T861">n-n:case</ta>
            <ta e="T863" id="Seg_9188" s="T862">v-v:n.fin</ta>
            <ta e="T864" id="Seg_9189" s="T863">conj</ta>
            <ta e="T865" id="Seg_9190" s="T864">ptcl</ta>
            <ta e="T866" id="Seg_9191" s="T865">adj-n:case</ta>
            <ta e="T867" id="Seg_9192" s="T866">n-n:case</ta>
            <ta e="T868" id="Seg_9193" s="T867">v-v:tense-v:pn</ta>
            <ta e="T869" id="Seg_9194" s="T868">pers</ta>
            <ta e="T870" id="Seg_9195" s="T869">dempro-n:case</ta>
            <ta e="T871" id="Seg_9196" s="T870">v-v:tense-v:pn</ta>
            <ta e="T872" id="Seg_9197" s="T871">conj</ta>
            <ta e="T873" id="Seg_9198" s="T872">dempro-n:case</ta>
            <ta e="T874" id="Seg_9199" s="T873">v-v:pn</ta>
            <ta e="T875" id="Seg_9200" s="T874">que=ptcl</ta>
            <ta e="T876" id="Seg_9201" s="T875">v-v:n.fin</ta>
            <ta e="T877" id="Seg_9202" s="T876">v-v:tense-v:pn</ta>
            <ta e="T878" id="Seg_9203" s="T877">pers</ta>
            <ta e="T879" id="Seg_9204" s="T878">ptcl</ta>
            <ta e="T917" id="Seg_9205" s="T879">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PKZ">
            <ta e="T5" id="Seg_9206" s="T4">pers</ta>
            <ta e="T6" id="Seg_9207" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_9208" s="T6">v</ta>
            <ta e="T10" id="Seg_9209" s="T9">v</ta>
            <ta e="T11" id="Seg_9210" s="T10">quant</ta>
            <ta e="T12" id="Seg_9211" s="T11">v</ta>
            <ta e="T13" id="Seg_9212" s="T12">adv</ta>
            <ta e="T14" id="Seg_9213" s="T13">pers</ta>
            <ta e="T15" id="Seg_9214" s="T14">v</ta>
            <ta e="T16" id="Seg_9215" s="T15">adv</ta>
            <ta e="T17" id="Seg_9216" s="T16">dempro</ta>
            <ta e="T18" id="Seg_9217" s="T17">n</ta>
            <ta e="T19" id="Seg_9218" s="T18">adj</ta>
            <ta e="T20" id="Seg_9219" s="T19">n</ta>
            <ta e="T21" id="Seg_9220" s="T20">dempro</ta>
            <ta e="T22" id="Seg_9221" s="T21">adv</ta>
            <ta e="T27" id="Seg_9222" s="T26">n</ta>
            <ta e="T28" id="Seg_9223" s="T27">v</ta>
            <ta e="T29" id="Seg_9224" s="T28">dempro</ta>
            <ta e="T33" id="Seg_9225" s="T32">num</ta>
            <ta e="T34" id="Seg_9226" s="T33">num</ta>
            <ta e="T35" id="Seg_9227" s="T34">n</ta>
            <ta e="T36" id="Seg_9228" s="T35">conj</ta>
            <ta e="T37" id="Seg_9229" s="T36">adv</ta>
            <ta e="T38" id="Seg_9230" s="T37">num</ta>
            <ta e="T40" id="Seg_9231" s="T39">que</ta>
            <ta e="T41" id="Seg_9232" s="T40">dempro</ta>
            <ta e="T42" id="Seg_9233" s="T41">n</ta>
            <ta e="T43" id="Seg_9234" s="T42">adj</ta>
            <ta e="T44" id="Seg_9235" s="T43">que</ta>
            <ta e="T45" id="Seg_9236" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_9237" s="T45">v</ta>
            <ta e="T47" id="Seg_9238" s="T46">que</ta>
            <ta e="T48" id="Seg_9239" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_9240" s="T48">v</ta>
            <ta e="T50" id="Seg_9241" s="T49">conj</ta>
            <ta e="T51" id="Seg_9242" s="T50">adj</ta>
            <ta e="T52" id="Seg_9243" s="T51">n</ta>
            <ta e="T53" id="Seg_9244" s="T52">adv</ta>
            <ta e="T54" id="Seg_9245" s="T53">adj</ta>
            <ta e="T55" id="Seg_9246" s="T54">quant</ta>
            <ta e="T56" id="Seg_9247" s="T55">v</ta>
            <ta e="T59" id="Seg_9248" s="T58">que</ta>
            <ta e="T62" id="Seg_9249" s="T61">que</ta>
            <ta e="T63" id="Seg_9250" s="T62">ptcl</ta>
            <ta e="T69" id="Seg_9251" s="T67">que</ta>
            <ta e="T70" id="Seg_9252" s="T69">que</ta>
            <ta e="T71" id="Seg_9253" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_9254" s="T71">v</ta>
            <ta e="T73" id="Seg_9255" s="T72">quant</ta>
            <ta e="T74" id="Seg_9256" s="T73">v</ta>
            <ta e="T75" id="Seg_9257" s="T74">dempro</ta>
            <ta e="T76" id="Seg_9258" s="T75">adv</ta>
            <ta e="T77" id="Seg_9259" s="T76">n</ta>
            <ta e="T78" id="Seg_9260" s="T77">adj</ta>
            <ta e="T84" id="Seg_9261" s="T83">n</ta>
            <ta e="T85" id="Seg_9262" s="T84">ptcl</ta>
            <ta e="T86" id="Seg_9263" s="T85">v</ta>
            <ta e="T87" id="Seg_9264" s="T86">que</ta>
            <ta e="T88" id="Seg_9265" s="T87">v</ta>
            <ta e="T89" id="Seg_9266" s="T88">quant</ta>
            <ta e="T90" id="Seg_9267" s="T89">v</ta>
            <ta e="T91" id="Seg_9268" s="T90">n</ta>
            <ta e="T92" id="Seg_9269" s="T91">v</ta>
            <ta e="T95" id="Seg_9270" s="T94">n</ta>
            <ta e="T96" id="Seg_9271" s="T95">v</ta>
            <ta e="T97" id="Seg_9272" s="T96">n</ta>
            <ta e="T98" id="Seg_9273" s="T97">v</ta>
            <ta e="T99" id="Seg_9274" s="T98">n</ta>
            <ta e="T100" id="Seg_9275" s="T99">v</ta>
            <ta e="T101" id="Seg_9276" s="T100">ptcl</ta>
            <ta e="T102" id="Seg_9277" s="T101">v</ta>
            <ta e="T103" id="Seg_9278" s="T102">quant</ta>
            <ta e="T104" id="Seg_9279" s="T103">que</ta>
            <ta e="T105" id="Seg_9280" s="T104">v</ta>
            <ta e="T106" id="Seg_9281" s="T105">quant</ta>
            <ta e="T107" id="Seg_9282" s="T106">v</ta>
            <ta e="T108" id="Seg_9283" s="T107">adv</ta>
            <ta e="T111" id="Seg_9284" s="T110">adv</ta>
            <ta e="T112" id="Seg_9285" s="T111">v</ta>
            <ta e="T113" id="Seg_9286" s="T112">n</ta>
            <ta e="T114" id="Seg_9287" s="T113">v</ta>
            <ta e="T116" id="Seg_9288" s="T115">que</ta>
            <ta e="T117" id="Seg_9289" s="T116">v</ta>
            <ta e="T118" id="Seg_9290" s="T117">n</ta>
            <ta e="T119" id="Seg_9291" s="T118">v</ta>
            <ta e="T120" id="Seg_9292" s="T119">n</ta>
            <ta e="T121" id="Seg_9293" s="T120">v</ta>
            <ta e="T122" id="Seg_9294" s="T121">adv</ta>
            <ta e="T123" id="Seg_9295" s="T122">n</ta>
            <ta e="T124" id="Seg_9296" s="T123">v</ta>
            <ta e="T125" id="Seg_9297" s="T124">n</ta>
            <ta e="T126" id="Seg_9298" s="T125">v</ta>
            <ta e="T127" id="Seg_9299" s="T126">adv</ta>
            <ta e="T128" id="Seg_9300" s="T127">n</ta>
            <ta e="T129" id="Seg_9301" s="T128">adj</ta>
            <ta e="T130" id="Seg_9302" s="T129">v</ta>
            <ta e="T131" id="Seg_9303" s="T130">pers</ta>
            <ta e="T132" id="Seg_9304" s="T131">adv</ta>
            <ta e="T133" id="Seg_9305" s="T132">v</ta>
            <ta e="T134" id="Seg_9306" s="T133">n</ta>
            <ta e="T135" id="Seg_9307" s="T134">quant</ta>
            <ta e="T136" id="Seg_9308" s="T135">v</ta>
            <ta e="T137" id="Seg_9309" s="T136">v</ta>
            <ta e="T138" id="Seg_9310" s="T137">dempro</ta>
            <ta e="T139" id="Seg_9311" s="T138">n</ta>
            <ta e="T140" id="Seg_9312" s="T139">v</ta>
            <ta e="T141" id="Seg_9313" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_9314" s="T141">conj</ta>
            <ta e="T143" id="Seg_9315" s="T142">v</ta>
            <ta e="T144" id="Seg_9316" s="T143">pers</ta>
            <ta e="T145" id="Seg_9317" s="T144">v</ta>
            <ta e="T146" id="Seg_9318" s="T145">adv</ta>
            <ta e="T147" id="Seg_9319" s="T146">v</ta>
            <ta e="T148" id="Seg_9320" s="T147">v</ta>
            <ta e="T149" id="Seg_9321" s="T148">ptcl</ta>
            <ta e="T150" id="Seg_9322" s="T149">adv</ta>
            <ta e="T151" id="Seg_9323" s="T150">adv</ta>
            <ta e="T152" id="Seg_9324" s="T151">v</ta>
            <ta e="T153" id="Seg_9325" s="T152">n</ta>
            <ta e="T154" id="Seg_9326" s="T153">ptcl</ta>
            <ta e="T155" id="Seg_9327" s="T154">v</ta>
            <ta e="T156" id="Seg_9328" s="T155">n</ta>
            <ta e="T157" id="Seg_9329" s="T156">ptcl</ta>
            <ta e="T158" id="Seg_9330" s="T157">v</ta>
            <ta e="T159" id="Seg_9331" s="T158">dempro</ta>
            <ta e="T160" id="Seg_9332" s="T159">adv</ta>
            <ta e="T161" id="Seg_9333" s="T160">v</ta>
            <ta e="T162" id="Seg_9334" s="T161">v</ta>
            <ta e="T163" id="Seg_9335" s="T162">adv</ta>
            <ta e="T164" id="Seg_9336" s="T163">v</ta>
            <ta e="T165" id="Seg_9337" s="T164">que</ta>
            <ta e="T166" id="Seg_9338" s="T165">n</ta>
            <ta e="T167" id="Seg_9339" s="T166">v</ta>
            <ta e="T168" id="Seg_9340" s="T167">adv</ta>
            <ta e="T169" id="Seg_9341" s="T168">adj</ta>
            <ta e="T170" id="Seg_9342" s="T169">n</ta>
            <ta e="T171" id="Seg_9343" s="T170">ptcl</ta>
            <ta e="T172" id="Seg_9344" s="T171">v</ta>
            <ta e="T173" id="Seg_9345" s="T172">n</ta>
            <ta e="T174" id="Seg_9346" s="T173">dempro</ta>
            <ta e="T175" id="Seg_9347" s="T174">n</ta>
            <ta e="T176" id="Seg_9348" s="T175">adv</ta>
            <ta e="T177" id="Seg_9349" s="T176">ptcl</ta>
            <ta e="T178" id="Seg_9350" s="T177">adj</ta>
            <ta e="T179" id="Seg_9351" s="T178">quant</ta>
            <ta e="T180" id="Seg_9352" s="T179">que</ta>
            <ta e="T181" id="Seg_9353" s="T180">v</ta>
            <ta e="T183" id="Seg_9354" s="T181">conj</ta>
            <ta e="T184" id="Seg_9355" s="T183">quant</ta>
            <ta e="T185" id="Seg_9356" s="T184">que</ta>
            <ta e="T186" id="Seg_9357" s="T185">v</ta>
            <ta e="T187" id="Seg_9358" s="T186">quant</ta>
            <ta e="T188" id="Seg_9359" s="T187">adv</ta>
            <ta e="T189" id="Seg_9360" s="T188">n</ta>
            <ta e="T190" id="Seg_9361" s="T189">n</ta>
            <ta e="T191" id="Seg_9362" s="T190">quant</ta>
            <ta e="T192" id="Seg_9363" s="T191">que</ta>
            <ta e="T193" id="Seg_9364" s="T192">v</ta>
            <ta e="T195" id="Seg_9365" s="T193">ptcl</ta>
            <ta e="T196" id="Seg_9366" s="T195">quant</ta>
            <ta e="T197" id="Seg_9367" s="T196">v</ta>
            <ta e="T198" id="Seg_9368" s="T197">quant</ta>
            <ta e="T199" id="Seg_9369" s="T198">v</ta>
            <ta e="T200" id="Seg_9370" s="T199">n</ta>
            <ta e="T201" id="Seg_9371" s="T200">pers</ta>
            <ta e="T202" id="Seg_9372" s="T201">adv</ta>
            <ta e="T203" id="Seg_9373" s="T202">n</ta>
            <ta e="T204" id="Seg_9374" s="T203">n</ta>
            <ta e="T205" id="Seg_9375" s="T204">v</ta>
            <ta e="T206" id="Seg_9376" s="T205">dempro</ta>
            <ta e="T207" id="Seg_9377" s="T206">v</ta>
            <ta e="T208" id="Seg_9378" s="T207">propr</ta>
            <ta e="T209" id="Seg_9379" s="T208">v</ta>
            <ta e="T210" id="Seg_9380" s="T209">adv</ta>
            <ta e="T211" id="Seg_9381" s="T210">n</ta>
            <ta e="T212" id="Seg_9382" s="T211">v</ta>
            <ta e="T213" id="Seg_9383" s="T212">que</ta>
            <ta e="T214" id="Seg_9384" s="T213">pers</ta>
            <ta e="T215" id="Seg_9385" s="T214">n</ta>
            <ta e="T216" id="Seg_9386" s="T215">v</ta>
            <ta e="T217" id="Seg_9387" s="T216">n</ta>
            <ta e="T218" id="Seg_9388" s="T217">v</ta>
            <ta e="T219" id="Seg_9389" s="T218">conj</ta>
            <ta e="T220" id="Seg_9390" s="T219">n</ta>
            <ta e="T221" id="Seg_9391" s="T220">que</ta>
            <ta e="T222" id="Seg_9392" s="T221">v</ta>
            <ta e="T223" id="Seg_9393" s="T222">propr</ta>
            <ta e="T224" id="Seg_9394" s="T223">v</ta>
            <ta e="T225" id="Seg_9395" s="T224">dempro</ta>
            <ta e="T226" id="Seg_9396" s="T225">n</ta>
            <ta e="T227" id="Seg_9397" s="T226">v</ta>
            <ta e="T228" id="Seg_9398" s="T227">quant</ta>
            <ta e="T229" id="Seg_9399" s="T228">n</ta>
            <ta e="T230" id="Seg_9400" s="T229">v</ta>
            <ta e="T231" id="Seg_9401" s="T230">conj</ta>
            <ta e="T232" id="Seg_9402" s="T231">adj</ta>
            <ta e="T233" id="Seg_9403" s="T232">n</ta>
            <ta e="T234" id="Seg_9404" s="T233">v</ta>
            <ta e="T235" id="Seg_9405" s="T234">adv</ta>
            <ta e="T236" id="Seg_9406" s="T235">v</ta>
            <ta e="T238" id="Seg_9407" s="T237">v</ta>
            <ta e="T241" id="Seg_9408" s="T240">n</ta>
            <ta e="T243" id="Seg_9409" s="T241">ptcl</ta>
            <ta e="T247" id="Seg_9410" s="T246">ptcl</ta>
            <ta e="T248" id="Seg_9411" s="T247">v</ta>
            <ta e="T249" id="Seg_9412" s="T248">pers</ta>
            <ta e="T251" id="Seg_9413" s="T249">ptcl</ta>
            <ta e="T257" id="Seg_9414" s="T256">adv</ta>
            <ta e="T258" id="Seg_9415" s="T257">adj</ta>
            <ta e="T259" id="Seg_9416" s="T258">ptcl</ta>
            <ta e="T260" id="Seg_9417" s="T259">v</ta>
            <ta e="T261" id="Seg_9418" s="T260">v</ta>
            <ta e="T262" id="Seg_9419" s="T261">ptcl</ta>
            <ta e="T267" id="Seg_9420" s="T266">v</ta>
            <ta e="T268" id="Seg_9421" s="T267">ptcl</ta>
            <ta e="T270" id="Seg_9422" s="T269">ptcl</ta>
            <ta e="T271" id="Seg_9423" s="T270">adj</ta>
            <ta e="T272" id="Seg_9424" s="T271">n</ta>
            <ta e="T273" id="Seg_9425" s="T272">n</ta>
            <ta e="T274" id="Seg_9426" s="T273">adv</ta>
            <ta e="T275" id="Seg_9427" s="T274">ptcl</ta>
            <ta e="T276" id="Seg_9428" s="T275">v</ta>
            <ta e="T277" id="Seg_9429" s="T276">conj</ta>
            <ta e="T278" id="Seg_9430" s="T277">v</ta>
            <ta e="T279" id="Seg_9431" s="T278">pers</ta>
            <ta e="T280" id="Seg_9432" s="T279">n</ta>
            <ta e="T281" id="Seg_9433" s="T280">v</ta>
            <ta e="T282" id="Seg_9434" s="T281">que</ta>
            <ta e="T285" id="Seg_9435" s="T284">v</ta>
            <ta e="T286" id="Seg_9436" s="T285">adv</ta>
            <ta e="T287" id="Seg_9437" s="T286">pers</ta>
            <ta e="T288" id="Seg_9438" s="T287">v</ta>
            <ta e="T289" id="Seg_9439" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_9440" s="T289">adv</ta>
            <ta e="T293" id="Seg_9441" s="T292">n</ta>
            <ta e="T294" id="Seg_9442" s="T293">v</ta>
            <ta e="T295" id="Seg_9443" s="T294">ptcl</ta>
            <ta e="T296" id="Seg_9444" s="T295">v</ta>
            <ta e="T297" id="Seg_9445" s="T296">v</ta>
            <ta e="T298" id="Seg_9446" s="T297">ptcl</ta>
            <ta e="T299" id="Seg_9447" s="T298">n</ta>
            <ta e="T300" id="Seg_9448" s="T299">v</ta>
            <ta e="T301" id="Seg_9449" s="T300">ptcl</ta>
            <ta e="T302" id="Seg_9450" s="T301">n</ta>
            <ta e="T303" id="Seg_9451" s="T302">v</ta>
            <ta e="T305" id="Seg_9452" s="T304">dempro</ta>
            <ta e="T310" id="Seg_9453" s="T309">dempro</ta>
            <ta e="T311" id="Seg_9454" s="T310">n</ta>
            <ta e="T312" id="Seg_9455" s="T311">ptcl</ta>
            <ta e="T313" id="Seg_9456" s="T312">n</ta>
            <ta e="T314" id="Seg_9457" s="T313">v</ta>
            <ta e="T315" id="Seg_9458" s="T314">adv</ta>
            <ta e="T316" id="Seg_9459" s="T315">n</ta>
            <ta e="T317" id="Seg_9460" s="T316">v</ta>
            <ta e="T318" id="Seg_9461" s="T317">adv</ta>
            <ta e="T319" id="Seg_9462" s="T318">n</ta>
            <ta e="T320" id="Seg_9463" s="T319">ptcl</ta>
            <ta e="T321" id="Seg_9464" s="T320">v</ta>
            <ta e="T322" id="Seg_9465" s="T321">n</ta>
            <ta e="T325" id="Seg_9466" s="T324">n</ta>
            <ta e="T326" id="Seg_9467" s="T325">v</ta>
            <ta e="T338" id="Seg_9468" s="T337">que</ta>
            <ta e="T339" id="Seg_9469" s="T338">ptcl</ta>
            <ta e="T340" id="Seg_9470" s="T339">v</ta>
            <ta e="T341" id="Seg_9471" s="T340">pers</ta>
            <ta e="T342" id="Seg_9472" s="T341">n</ta>
            <ta e="T344" id="Seg_9473" s="T342">adv</ta>
            <ta e="T347" id="Seg_9474" s="T346">pers</ta>
            <ta e="T348" id="Seg_9475" s="T347">n</ta>
            <ta e="T349" id="Seg_9476" s="T348">adv</ta>
            <ta e="T350" id="Seg_9477" s="T349">adj</ta>
            <ta e="T351" id="Seg_9478" s="T350">quant</ta>
            <ta e="T352" id="Seg_9479" s="T351">v</ta>
            <ta e="T353" id="Seg_9480" s="T352">pers</ta>
            <ta e="T354" id="Seg_9481" s="T353">que</ta>
            <ta e="T355" id="Seg_9482" s="T354">v</ta>
            <ta e="T356" id="Seg_9483" s="T355">v</ta>
            <ta e="T357" id="Seg_9484" s="T356">v</ta>
            <ta e="T358" id="Seg_9485" s="T357">ptcl</ta>
            <ta e="T359" id="Seg_9486" s="T358">adj</ta>
            <ta e="T360" id="Seg_9487" s="T359">n</ta>
            <ta e="T361" id="Seg_9488" s="T360">v</ta>
            <ta e="T362" id="Seg_9489" s="T361">ptcl</ta>
            <ta e="T363" id="Seg_9490" s="T362">n</ta>
            <ta e="T366" id="Seg_9491" s="T365">v</ta>
            <ta e="T367" id="Seg_9492" s="T366">n</ta>
            <ta e="T368" id="Seg_9493" s="T367">v</ta>
            <ta e="T369" id="Seg_9494" s="T368">pers</ta>
            <ta e="T370" id="Seg_9495" s="T369">num</ta>
            <ta e="T371" id="Seg_9496" s="T370">n</ta>
            <ta e="T372" id="Seg_9497" s="T371">v</ta>
            <ta e="T373" id="Seg_9498" s="T372">ptcl</ta>
            <ta e="T374" id="Seg_9499" s="T373">pers</ta>
            <ta e="T375" id="Seg_9500" s="T374">num</ta>
            <ta e="T376" id="Seg_9501" s="T375">v</ta>
            <ta e="T377" id="Seg_9502" s="T376">num</ta>
            <ta e="T378" id="Seg_9503" s="T377">refl</ta>
            <ta e="T379" id="Seg_9504" s="T378">v</ta>
            <ta e="T380" id="Seg_9505" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_9506" s="T380">que</ta>
            <ta e="T382" id="Seg_9507" s="T381">v</ta>
            <ta e="T383" id="Seg_9508" s="T382">n</ta>
            <ta e="T384" id="Seg_9509" s="T383">adv</ta>
            <ta e="T385" id="Seg_9510" s="T384">v</ta>
            <ta e="T387" id="Seg_9511" s="T386">propr</ta>
            <ta e="T388" id="Seg_9512" s="T387">ptcl</ta>
            <ta e="T389" id="Seg_9513" s="T388">v</ta>
            <ta e="T390" id="Seg_9514" s="T389">ptcl</ta>
            <ta e="T391" id="Seg_9515" s="T390">v</ta>
            <ta e="T392" id="Seg_9516" s="T391">n</ta>
            <ta e="T393" id="Seg_9517" s="T392">v</ta>
            <ta e="T394" id="Seg_9518" s="T393">n</ta>
            <ta e="T395" id="Seg_9519" s="T394">n</ta>
            <ta e="T396" id="Seg_9520" s="T395">v</ta>
            <ta e="T397" id="Seg_9521" s="T396">n</ta>
            <ta e="T398" id="Seg_9522" s="T397">ptcl</ta>
            <ta e="T399" id="Seg_9523" s="T398">v</ta>
            <ta e="T400" id="Seg_9524" s="T399">ptcl</ta>
            <ta e="T401" id="Seg_9525" s="T400">v</ta>
            <ta e="T404" id="Seg_9526" s="T403">n</ta>
            <ta e="T405" id="Seg_9527" s="T404">aux</ta>
            <ta e="T406" id="Seg_9528" s="T405">v</ta>
            <ta e="T410" id="Seg_9529" s="T409">adv</ta>
            <ta e="T411" id="Seg_9530" s="T410">n</ta>
            <ta e="T412" id="Seg_9531" s="T411">v</ta>
            <ta e="T413" id="Seg_9532" s="T412">que</ta>
            <ta e="T414" id="Seg_9533" s="T413">pers</ta>
            <ta e="T415" id="Seg_9534" s="T414">v</ta>
            <ta e="T416" id="Seg_9535" s="T415">adv</ta>
            <ta e="T417" id="Seg_9536" s="T416">ptcl</ta>
            <ta e="T418" id="Seg_9537" s="T417">v</ta>
            <ta e="T419" id="Seg_9538" s="T418">adv</ta>
            <ta e="T420" id="Seg_9539" s="T419">v</ta>
            <ta e="T421" id="Seg_9540" s="T420">ptcl</ta>
            <ta e="T422" id="Seg_9541" s="T421">quant</ta>
            <ta e="T423" id="Seg_9542" s="T422">n</ta>
            <ta e="T424" id="Seg_9543" s="T423">v</ta>
            <ta e="T425" id="Seg_9544" s="T424">adv</ta>
            <ta e="T426" id="Seg_9545" s="T425">aux</ta>
            <ta e="T427" id="Seg_9546" s="T426">v</ta>
            <ta e="T428" id="Seg_9547" s="T427">conj</ta>
            <ta e="T429" id="Seg_9548" s="T428">aux</ta>
            <ta e="T430" id="Seg_9549" s="T429">pers</ta>
            <ta e="T431" id="Seg_9550" s="T430">aux</ta>
            <ta e="T432" id="Seg_9551" s="T431">v</ta>
            <ta e="T433" id="Seg_9552" s="T432">ptcl</ta>
            <ta e="T434" id="Seg_9553" s="T433">n</ta>
            <ta e="T435" id="Seg_9554" s="T434">quant</ta>
            <ta e="T436" id="Seg_9555" s="T435">v</ta>
            <ta e="T437" id="Seg_9556" s="T436">que</ta>
            <ta e="T438" id="Seg_9557" s="T437">pers</ta>
            <ta e="T439" id="Seg_9558" s="T438">pers</ta>
            <ta e="T440" id="Seg_9559" s="T439">v</ta>
            <ta e="T441" id="Seg_9560" s="T440">pers</ta>
            <ta e="T442" id="Seg_9561" s="T441">adv</ta>
            <ta e="T443" id="Seg_9562" s="T442">n</ta>
            <ta e="T444" id="Seg_9563" s="T443">v</ta>
            <ta e="T445" id="Seg_9564" s="T444">v</ta>
            <ta e="T446" id="Seg_9565" s="T445">n</ta>
            <ta e="T447" id="Seg_9566" s="T446">v</ta>
            <ta e="T448" id="Seg_9567" s="T447">v</ta>
            <ta e="T449" id="Seg_9568" s="T448">n</ta>
            <ta e="T450" id="Seg_9569" s="T449">v</ta>
            <ta e="T451" id="Seg_9570" s="T450">quant</ta>
            <ta e="T452" id="Seg_9571" s="T451">que</ta>
            <ta e="T453" id="Seg_9572" s="T452">v</ta>
            <ta e="T454" id="Seg_9573" s="T453">v</ta>
            <ta e="T455" id="Seg_9574" s="T454">pers</ta>
            <ta e="T458" id="Seg_9575" s="T457">que</ta>
            <ta e="T459" id="Seg_9576" s="T458">adv</ta>
            <ta e="T460" id="Seg_9577" s="T459">v</ta>
            <ta e="T461" id="Seg_9578" s="T460">pers</ta>
            <ta e="T462" id="Seg_9579" s="T461">adv</ta>
            <ta e="T463" id="Seg_9580" s="T462">v</ta>
            <ta e="T464" id="Seg_9581" s="T463">v</ta>
            <ta e="T465" id="Seg_9582" s="T464">n</ta>
            <ta e="T466" id="Seg_9583" s="T465">v</ta>
            <ta e="T467" id="Seg_9584" s="T466">adv</ta>
            <ta e="T468" id="Seg_9585" s="T467">v</ta>
            <ta e="T469" id="Seg_9586" s="T468">n</ta>
            <ta e="T471" id="Seg_9587" s="T470">v</ta>
            <ta e="T472" id="Seg_9588" s="T471">n</ta>
            <ta e="T473" id="Seg_9589" s="T472">v</ta>
            <ta e="T474" id="Seg_9590" s="T473">adv</ta>
            <ta e="T475" id="Seg_9591" s="T474">v</ta>
            <ta e="T476" id="Seg_9592" s="T475">n</ta>
            <ta e="T477" id="Seg_9593" s="T476">v</ta>
            <ta e="T479" id="Seg_9594" s="T478">n</ta>
            <ta e="T480" id="Seg_9595" s="T479">v</ta>
            <ta e="T481" id="Seg_9596" s="T480">n</ta>
            <ta e="T482" id="Seg_9597" s="T481">v</ta>
            <ta e="T483" id="Seg_9598" s="T482">v</ta>
            <ta e="T484" id="Seg_9599" s="T483">adj</ta>
            <ta e="T485" id="Seg_9600" s="T484">n</ta>
            <ta e="T486" id="Seg_9601" s="T485">v</ta>
            <ta e="T490" id="Seg_9602" s="T489">n</ta>
            <ta e="T491" id="Seg_9603" s="T490">ptcl</ta>
            <ta e="T492" id="Seg_9604" s="T491">n</ta>
            <ta e="T493" id="Seg_9605" s="T492">v</ta>
            <ta e="T494" id="Seg_9606" s="T493">adv</ta>
            <ta e="T495" id="Seg_9607" s="T494">quant</ta>
            <ta e="T496" id="Seg_9608" s="T495">n</ta>
            <ta e="T497" id="Seg_9609" s="T496">v</ta>
            <ta e="T498" id="Seg_9610" s="T497">pers</ta>
            <ta e="T501" id="Seg_9611" s="T500">pers</ta>
            <ta e="T502" id="Seg_9612" s="T501">adv</ta>
            <ta e="T503" id="Seg_9613" s="T502">n</ta>
            <ta e="T504" id="Seg_9614" s="T503">v</ta>
            <ta e="T505" id="Seg_9615" s="T504">ptcl</ta>
            <ta e="T506" id="Seg_9616" s="T505">adj</ta>
            <ta e="T507" id="Seg_9617" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_9618" s="T507">adj</ta>
            <ta e="T509" id="Seg_9619" s="T508">ptcl</ta>
            <ta e="T510" id="Seg_9620" s="T509">pers</ta>
            <ta e="T511" id="Seg_9621" s="T510">dempro</ta>
            <ta e="T512" id="Seg_9622" s="T511">n</ta>
            <ta e="T513" id="Seg_9623" s="T512">dempro</ta>
            <ta e="T516" id="Seg_9624" s="T515">n</ta>
            <ta e="T517" id="Seg_9625" s="T516">v</ta>
            <ta e="T518" id="Seg_9626" s="T517">n</ta>
            <ta e="T519" id="Seg_9627" s="T518">ptcl</ta>
            <ta e="T520" id="Seg_9628" s="T519">n</ta>
            <ta e="T521" id="Seg_9629" s="T520">v</ta>
            <ta e="T522" id="Seg_9630" s="T521">conj</ta>
            <ta e="T523" id="Seg_9631" s="T522">ptcl</ta>
            <ta e="T524" id="Seg_9632" s="T523">v</ta>
            <ta e="T525" id="Seg_9633" s="T524">n</ta>
            <ta e="T526" id="Seg_9634" s="T525">ptcl</ta>
            <ta e="T527" id="Seg_9635" s="T526">dempro</ta>
            <ta e="T528" id="Seg_9636" s="T527">n</ta>
            <ta e="T529" id="Seg_9637" s="T528">v</ta>
            <ta e="T530" id="Seg_9638" s="T529">ptcl</ta>
            <ta e="T531" id="Seg_9639" s="T530">n</ta>
            <ta e="T532" id="Seg_9640" s="T531">v</ta>
            <ta e="T533" id="Seg_9641" s="T532">n</ta>
            <ta e="T534" id="Seg_9642" s="T533">v</ta>
            <ta e="T536" id="Seg_9643" s="T534">adv</ta>
            <ta e="T540" id="Seg_9644" s="T539">que</ta>
            <ta e="T541" id="Seg_9645" s="T540">ptcl</ta>
            <ta e="T542" id="Seg_9646" s="T541">v</ta>
            <ta e="T543" id="Seg_9647" s="T542">pers</ta>
            <ta e="T544" id="Seg_9648" s="T543">dempro</ta>
            <ta e="T545" id="Seg_9649" s="T544">ptcl</ta>
            <ta e="T546" id="Seg_9650" s="T545">v</ta>
            <ta e="T547" id="Seg_9651" s="T546">que</ta>
            <ta e="T548" id="Seg_9652" s="T547">v</ta>
            <ta e="T549" id="Seg_9653" s="T548">pers</ta>
            <ta e="T550" id="Seg_9654" s="T549">n</ta>
            <ta e="T551" id="Seg_9655" s="T550">n</ta>
            <ta e="T552" id="Seg_9656" s="T551">v</ta>
            <ta e="T553" id="Seg_9657" s="T552">n</ta>
            <ta e="T554" id="Seg_9658" s="T553">n</ta>
            <ta e="T555" id="Seg_9659" s="T554">v</ta>
            <ta e="T556" id="Seg_9660" s="T555">v</ta>
            <ta e="T557" id="Seg_9661" s="T556">conj</ta>
            <ta e="T558" id="Seg_9662" s="T557">v</ta>
            <ta e="T559" id="Seg_9663" s="T558">n</ta>
            <ta e="T560" id="Seg_9664" s="T559">v</ta>
            <ta e="T561" id="Seg_9665" s="T560">n</ta>
            <ta e="T562" id="Seg_9666" s="T561">ptcl</ta>
            <ta e="T563" id="Seg_9667" s="T562">n</ta>
            <ta e="T564" id="Seg_9668" s="T563">conj</ta>
            <ta e="T565" id="Seg_9669" s="T564">n</ta>
            <ta e="T566" id="Seg_9670" s="T565">n</ta>
            <ta e="T567" id="Seg_9671" s="T566">v</ta>
            <ta e="T569" id="Seg_9672" s="T567">n</ta>
            <ta e="T570" id="Seg_9673" s="T569">v</ta>
            <ta e="T571" id="Seg_9674" s="T570">ptcl</ta>
            <ta e="T572" id="Seg_9675" s="T571">n</ta>
            <ta e="T573" id="Seg_9676" s="T572">conj</ta>
            <ta e="T576" id="Seg_9677" s="T575">n</ta>
            <ta e="T577" id="Seg_9678" s="T576">v</ta>
            <ta e="T578" id="Seg_9679" s="T577">n</ta>
            <ta e="T579" id="Seg_9680" s="T578">conj</ta>
            <ta e="T580" id="Seg_9681" s="T579">n</ta>
            <ta e="T581" id="Seg_9682" s="T580">v</ta>
            <ta e="T582" id="Seg_9683" s="T581">v</ta>
            <ta e="T593" id="Seg_9684" s="T592">n</ta>
            <ta e="T594" id="Seg_9685" s="T593">ptcl</ta>
            <ta e="T595" id="Seg_9686" s="T594">v</ta>
            <ta e="T596" id="Seg_9687" s="T595">pers</ta>
            <ta e="T597" id="Seg_9688" s="T596">ptcl</ta>
            <ta e="T598" id="Seg_9689" s="T597">n</ta>
            <ta e="T599" id="Seg_9690" s="T598">v</ta>
            <ta e="T600" id="Seg_9691" s="T599">n</ta>
            <ta e="T601" id="Seg_9692" s="T600">v</ta>
            <ta e="T602" id="Seg_9693" s="T601">adv</ta>
            <ta e="T603" id="Seg_9694" s="T602">pers</ta>
            <ta e="T604" id="Seg_9695" s="T603">ptcl</ta>
            <ta e="T605" id="Seg_9696" s="T604">n</ta>
            <ta e="T606" id="Seg_9697" s="T605">adj</ta>
            <ta e="T609" id="Seg_9698" s="T608">adv</ta>
            <ta e="T610" id="Seg_9699" s="T609">v</ta>
            <ta e="T611" id="Seg_9700" s="T610">adv</ta>
            <ta e="T612" id="Seg_9701" s="T611">adj</ta>
            <ta e="T613" id="Seg_9702" s="T612">ptcl</ta>
            <ta e="T614" id="Seg_9703" s="T613">n</ta>
            <ta e="T615" id="Seg_9704" s="T614">ptcl</ta>
            <ta e="T616" id="Seg_9705" s="T615">v</ta>
            <ta e="T617" id="Seg_9706" s="T616">v</ta>
            <ta e="T618" id="Seg_9707" s="T617">adj</ta>
            <ta e="T619" id="Seg_9708" s="T618">n</ta>
            <ta e="T620" id="Seg_9709" s="T619">n</ta>
            <ta e="T621" id="Seg_9710" s="T620">ptcl</ta>
            <ta e="T622" id="Seg_9711" s="T621">v</ta>
            <ta e="T627" id="Seg_9712" s="T626">que</ta>
            <ta e="T628" id="Seg_9713" s="T627">pers</ta>
            <ta e="T629" id="Seg_9714" s="T628">v</ta>
            <ta e="T630" id="Seg_9715" s="T629">pers</ta>
            <ta e="T631" id="Seg_9716" s="T630">quant</ta>
            <ta e="T632" id="Seg_9717" s="T631">v</ta>
            <ta e="T633" id="Seg_9718" s="T632">adv</ta>
            <ta e="T634" id="Seg_9719" s="T633">v</ta>
            <ta e="T635" id="Seg_9720" s="T634">n</ta>
            <ta e="T636" id="Seg_9721" s="T635">ptcl</ta>
            <ta e="T637" id="Seg_9722" s="T636">v</ta>
            <ta e="T638" id="Seg_9723" s="T637">dempro</ta>
            <ta e="T639" id="Seg_9724" s="T638">n</ta>
            <ta e="T640" id="Seg_9725" s="T639">n</ta>
            <ta e="T641" id="Seg_9726" s="T640">v</ta>
            <ta e="T642" id="Seg_9727" s="T641">ptcl</ta>
            <ta e="T643" id="Seg_9728" s="T642">dempro</ta>
            <ta e="T644" id="Seg_9729" s="T643">v</ta>
            <ta e="T645" id="Seg_9730" s="T644">dempro</ta>
            <ta e="T646" id="Seg_9731" s="T645">n</ta>
            <ta e="T647" id="Seg_9732" s="T646">v</ta>
            <ta e="T648" id="Seg_9733" s="T647">v</ta>
            <ta e="T649" id="Seg_9734" s="T648">quant</ta>
            <ta e="T656" id="Seg_9735" s="T655">que</ta>
            <ta e="T657" id="Seg_9736" s="T656">v</ta>
            <ta e="T658" id="Seg_9737" s="T657">que</ta>
            <ta e="T659" id="Seg_9738" s="T658">v</ta>
            <ta e="T660" id="Seg_9739" s="T659">dempro</ta>
            <ta e="T661" id="Seg_9740" s="T660">adv</ta>
            <ta e="T664" id="Seg_9741" s="T663">quant</ta>
            <ta e="T665" id="Seg_9742" s="T664">ptcl</ta>
            <ta e="T666" id="Seg_9743" s="T665">v</ta>
            <ta e="T670" id="Seg_9744" s="T668">ptcl</ta>
            <ta e="T671" id="Seg_9745" s="T670">v</ta>
            <ta e="T672" id="Seg_9746" s="T671">v</ta>
            <ta e="T673" id="Seg_9747" s="T672">que</ta>
            <ta e="T674" id="Seg_9748" s="T673">v</ta>
            <ta e="T675" id="Seg_9749" s="T674">pers</ta>
            <ta e="T676" id="Seg_9750" s="T675">adv</ta>
            <ta e="T677" id="Seg_9751" s="T676">adv</ta>
            <ta e="T678" id="Seg_9752" s="T677">ptcl</ta>
            <ta e="T681" id="Seg_9753" s="T680">v</ta>
            <ta e="T682" id="Seg_9754" s="T681">v</ta>
            <ta e="T685" id="Seg_9755" s="T684">adv</ta>
            <ta e="T686" id="Seg_9756" s="T685">ptcl</ta>
            <ta e="T688" id="Seg_9757" s="T687">ptcl</ta>
            <ta e="T690" id="Seg_9758" s="T689">ptcl</ta>
            <ta e="T691" id="Seg_9759" s="T690">v</ta>
            <ta e="T692" id="Seg_9760" s="T691">v</ta>
            <ta e="T697" id="Seg_9761" s="T696">v</ta>
            <ta e="T698" id="Seg_9762" s="T697">que</ta>
            <ta e="T699" id="Seg_9763" s="T698">ptcl</ta>
            <ta e="T700" id="Seg_9764" s="T699">v</ta>
            <ta e="T701" id="Seg_9765" s="T700">dempro</ta>
            <ta e="T702" id="Seg_9766" s="T701">n</ta>
            <ta e="T703" id="Seg_9767" s="T702">n</ta>
            <ta e="T704" id="Seg_9768" s="T703">quant</ta>
            <ta e="T705" id="Seg_9769" s="T704">adj</ta>
            <ta e="T706" id="Seg_9770" s="T705">v</ta>
            <ta e="T707" id="Seg_9771" s="T706">adv</ta>
            <ta e="T708" id="Seg_9772" s="T707">quant</ta>
            <ta e="T709" id="Seg_9773" s="T708">adv</ta>
            <ta e="T710" id="Seg_9774" s="T709">adj</ta>
            <ta e="T711" id="Seg_9775" s="T710">v</ta>
            <ta e="T712" id="Seg_9776" s="T711">n</ta>
            <ta e="T713" id="Seg_9777" s="T712">adj</ta>
            <ta e="T714" id="Seg_9778" s="T713">n</ta>
            <ta e="T715" id="Seg_9779" s="T714">v</ta>
            <ta e="T716" id="Seg_9780" s="T715">conj</ta>
            <ta e="T717" id="Seg_9781" s="T716">adj</ta>
            <ta e="T718" id="Seg_9782" s="T717">n</ta>
            <ta e="T719" id="Seg_9783" s="T718">v</ta>
            <ta e="T720" id="Seg_9784" s="T719">adj</ta>
            <ta e="T721" id="Seg_9785" s="T720">n</ta>
            <ta e="T722" id="Seg_9786" s="T721">v</ta>
            <ta e="T723" id="Seg_9787" s="T722">conj</ta>
            <ta e="T724" id="Seg_9788" s="T723">adj</ta>
            <ta e="T725" id="Seg_9789" s="T724">n</ta>
            <ta e="T726" id="Seg_9790" s="T725">ptcl</ta>
            <ta e="T727" id="Seg_9791" s="T726">v</ta>
            <ta e="T728" id="Seg_9792" s="T727">pers</ta>
            <ta e="T729" id="Seg_9793" s="T728">v</ta>
            <ta e="T731" id="Seg_9794" s="T730">adj</ta>
            <ta e="T732" id="Seg_9795" s="T731">n</ta>
            <ta e="T733" id="Seg_9796" s="T732">v</ta>
            <ta e="T735" id="Seg_9797" s="T734">pers</ta>
            <ta e="T736" id="Seg_9798" s="T735">v</ta>
            <ta e="T737" id="Seg_9799" s="T736">conj</ta>
            <ta e="T738" id="Seg_9800" s="T737">n</ta>
            <ta e="T739" id="Seg_9801" s="T738">v</ta>
            <ta e="T740" id="Seg_9802" s="T739">v</ta>
            <ta e="T741" id="Seg_9803" s="T740">adv</ta>
            <ta e="T742" id="Seg_9804" s="T741">dempro</ta>
            <ta e="T743" id="Seg_9805" s="T742">n</ta>
            <ta e="T744" id="Seg_9806" s="T743">v</ta>
            <ta e="T745" id="Seg_9807" s="T744">ptcl</ta>
            <ta e="T752" id="Seg_9808" s="T901">adv</ta>
            <ta e="T753" id="Seg_9809" s="T752">n</ta>
            <ta e="T918" id="Seg_9810" s="T753">ptcl</ta>
            <ta e="T755" id="Seg_9811" s="T918">v</ta>
            <ta e="T759" id="Seg_9812" s="T758">adv</ta>
            <ta e="T760" id="Seg_9813" s="T759">ptcl</ta>
            <ta e="T761" id="Seg_9814" s="T760">v</ta>
            <ta e="T762" id="Seg_9815" s="T761">que</ta>
            <ta e="T763" id="Seg_9816" s="T762">v</ta>
            <ta e="T764" id="Seg_9817" s="T763">ptcl</ta>
            <ta e="T765" id="Seg_9818" s="T764">n</ta>
            <ta e="T766" id="Seg_9819" s="T765">v</ta>
            <ta e="T767" id="Seg_9820" s="T766">ptcl</ta>
            <ta e="T772" id="Seg_9821" s="T771">n</ta>
            <ta e="T774" id="Seg_9822" s="T773">v</ta>
            <ta e="T775" id="Seg_9823" s="T774">quant</ta>
            <ta e="T782" id="Seg_9824" s="T781">n</ta>
            <ta e="T783" id="Seg_9825" s="T782">v</ta>
            <ta e="T784" id="Seg_9826" s="T783">v</ta>
            <ta e="T785" id="Seg_9827" s="T784">n</ta>
            <ta e="T786" id="Seg_9828" s="T785">v</ta>
            <ta e="T787" id="Seg_9829" s="T786">n</ta>
            <ta e="T788" id="Seg_9830" s="T787">v</ta>
            <ta e="T789" id="Seg_9831" s="T788">pers</ta>
            <ta e="T790" id="Seg_9832" s="T789">n</ta>
            <ta e="T791" id="Seg_9833" s="T790">num</ta>
            <ta e="T792" id="Seg_9834" s="T791">n</ta>
            <ta e="T793" id="Seg_9835" s="T792">conj</ta>
            <ta e="T794" id="Seg_9836" s="T793">pers</ta>
            <ta e="T795" id="Seg_9837" s="T794">n</ta>
            <ta e="T796" id="Seg_9838" s="T795">adj</ta>
            <ta e="T797" id="Seg_9839" s="T796">v</ta>
            <ta e="T798" id="Seg_9840" s="T797">n</ta>
            <ta e="T799" id="Seg_9841" s="T798">ptcl</ta>
            <ta e="T800" id="Seg_9842" s="T799">v</ta>
            <ta e="T801" id="Seg_9843" s="T800">adv</ta>
            <ta e="T802" id="Seg_9844" s="T801">n</ta>
            <ta e="T803" id="Seg_9845" s="T802">quant</ta>
            <ta e="T804" id="Seg_9846" s="T803">ptcl</ta>
            <ta e="T805" id="Seg_9847" s="T804">n</ta>
            <ta e="T806" id="Seg_9848" s="T805">v</ta>
            <ta e="T807" id="Seg_9849" s="T806">dempro</ta>
            <ta e="T808" id="Seg_9850" s="T807">ptcl</ta>
            <ta e="T809" id="Seg_9851" s="T808">v</ta>
            <ta e="T810" id="Seg_9852" s="T809">conj</ta>
            <ta e="T811" id="Seg_9853" s="T810">v</ta>
            <ta e="T812" id="Seg_9854" s="T811">n</ta>
            <ta e="T813" id="Seg_9855" s="T812">adv</ta>
            <ta e="T814" id="Seg_9856" s="T813">v</ta>
            <ta e="T815" id="Seg_9857" s="T814">v</ta>
            <ta e="T816" id="Seg_9858" s="T815">ptcl</ta>
            <ta e="T817" id="Seg_9859" s="T816">n</ta>
            <ta e="T818" id="Seg_9860" s="T817">v</ta>
            <ta e="T819" id="Seg_9861" s="T818">n</ta>
            <ta e="T820" id="Seg_9862" s="T819">ptcl</ta>
            <ta e="T821" id="Seg_9863" s="T820">v</ta>
            <ta e="T822" id="Seg_9864" s="T821">n</ta>
            <ta e="T823" id="Seg_9865" s="T822">v</ta>
            <ta e="T824" id="Seg_9866" s="T823">n</ta>
            <ta e="T825" id="Seg_9867" s="T824">v</ta>
            <ta e="T826" id="Seg_9868" s="T825">ptcl</ta>
            <ta e="T827" id="Seg_9869" s="T826">v</ta>
            <ta e="T828" id="Seg_9870" s="T827">ptcl</ta>
            <ta e="T829" id="Seg_9871" s="T828">v</ta>
            <ta e="T830" id="Seg_9872" s="T829">v</ta>
            <ta e="T831" id="Seg_9873" s="T830">adv</ta>
            <ta e="T832" id="Seg_9874" s="T831">n</ta>
            <ta e="T833" id="Seg_9875" s="T832">ptcl</ta>
            <ta e="T834" id="Seg_9876" s="T833">v</ta>
            <ta e="T840" id="Seg_9877" s="T839">v</ta>
            <ta e="T841" id="Seg_9878" s="T840">n</ta>
            <ta e="T842" id="Seg_9879" s="T841">v</ta>
            <ta e="T843" id="Seg_9880" s="T842">n</ta>
            <ta e="T844" id="Seg_9881" s="T843">n</ta>
            <ta e="T845" id="Seg_9882" s="T844">ptcl</ta>
            <ta e="T846" id="Seg_9883" s="T845">n</ta>
            <ta e="T847" id="Seg_9884" s="T846">v</ta>
            <ta e="T848" id="Seg_9885" s="T847">ptcl</ta>
            <ta e="T849" id="Seg_9886" s="T848">v</ta>
            <ta e="T850" id="Seg_9887" s="T849">que</ta>
            <ta e="T851" id="Seg_9888" s="T850">ptcl</ta>
            <ta e="T852" id="Seg_9889" s="T851">v</ta>
            <ta e="T853" id="Seg_9890" s="T852">pers</ta>
            <ta e="T854" id="Seg_9891" s="T853">n</ta>
            <ta e="T856" id="Seg_9892" s="T854">adv</ta>
            <ta e="T858" id="Seg_9893" s="T857">adv</ta>
            <ta e="T859" id="Seg_9894" s="T858">v</ta>
            <ta e="T860" id="Seg_9895" s="T859">ptcl</ta>
            <ta e="T861" id="Seg_9896" s="T860">num</ta>
            <ta e="T862" id="Seg_9897" s="T861">n</ta>
            <ta e="T863" id="Seg_9898" s="T862">v</ta>
            <ta e="T864" id="Seg_9899" s="T863">conj</ta>
            <ta e="T865" id="Seg_9900" s="T864">ptcl</ta>
            <ta e="T866" id="Seg_9901" s="T865">adj</ta>
            <ta e="T867" id="Seg_9902" s="T866">n</ta>
            <ta e="T868" id="Seg_9903" s="T867">v</ta>
            <ta e="T869" id="Seg_9904" s="T868">pers</ta>
            <ta e="T870" id="Seg_9905" s="T869">dempro</ta>
            <ta e="T871" id="Seg_9906" s="T870">v</ta>
            <ta e="T872" id="Seg_9907" s="T871">conj</ta>
            <ta e="T873" id="Seg_9908" s="T872">dempro</ta>
            <ta e="T874" id="Seg_9909" s="T873">v</ta>
            <ta e="T875" id="Seg_9910" s="T874">que</ta>
            <ta e="T876" id="Seg_9911" s="T875">v</ta>
            <ta e="T877" id="Seg_9912" s="T876">v</ta>
            <ta e="T878" id="Seg_9913" s="T877">pers</ta>
            <ta e="T879" id="Seg_9914" s="T878">ptcl</ta>
            <ta e="T917" id="Seg_9915" s="T879">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PKZ">
            <ta e="T5" id="Seg_9916" s="T4">pro.h:A</ta>
            <ta e="T11" id="Seg_9917" s="T10">np:E</ta>
            <ta e="T14" id="Seg_9918" s="T13">pro.h:P</ta>
            <ta e="T16" id="Seg_9919" s="T15">n:Time</ta>
            <ta e="T18" id="Seg_9920" s="T17">np.h:Poss</ta>
            <ta e="T20" id="Seg_9921" s="T19">np:Th</ta>
            <ta e="T21" id="Seg_9922" s="T20">pro.h:P</ta>
            <ta e="T22" id="Seg_9923" s="T21">adv:Time</ta>
            <ta e="T27" id="Seg_9924" s="T26">np.h:A 0.3.h:Poss</ta>
            <ta e="T29" id="Seg_9925" s="T28">pro.h:B</ta>
            <ta e="T35" id="Seg_9926" s="T34">np:Th</ta>
            <ta e="T42" id="Seg_9927" s="T41">np.h:Th</ta>
            <ta e="T44" id="Seg_9928" s="T43">pro:Th</ta>
            <ta e="T46" id="Seg_9929" s="T45">0.3.h:A</ta>
            <ta e="T47" id="Seg_9930" s="T46">pro:Th</ta>
            <ta e="T49" id="Seg_9931" s="T48">0.3.h:A</ta>
            <ta e="T52" id="Seg_9932" s="T51">np.h:Th</ta>
            <ta e="T55" id="Seg_9933" s="T54">np:Th</ta>
            <ta e="T56" id="Seg_9934" s="T55">0.3.h:A</ta>
            <ta e="T70" id="Seg_9935" s="T69">pro:Th</ta>
            <ta e="T72" id="Seg_9936" s="T71">0.2.h:A</ta>
            <ta e="T73" id="Seg_9937" s="T72">np:Th</ta>
            <ta e="T74" id="Seg_9938" s="T73">0.3.h:A</ta>
            <ta e="T75" id="Seg_9939" s="T74">pro.h:Poss</ta>
            <ta e="T77" id="Seg_9940" s="T76">np:Th</ta>
            <ta e="T84" id="Seg_9941" s="T83">np:P</ta>
            <ta e="T86" id="Seg_9942" s="T85">0.3.h:A</ta>
            <ta e="T87" id="Seg_9943" s="T86">pro:Th</ta>
            <ta e="T89" id="Seg_9944" s="T88">np:Th</ta>
            <ta e="T90" id="Seg_9945" s="T89">0.3.h:A</ta>
            <ta e="T91" id="Seg_9946" s="T90">np:Th</ta>
            <ta e="T92" id="Seg_9947" s="T91">0.3.h:A</ta>
            <ta e="T95" id="Seg_9948" s="T94">np:Th</ta>
            <ta e="T96" id="Seg_9949" s="T95">0.3.h:A</ta>
            <ta e="T97" id="Seg_9950" s="T96">np:Th</ta>
            <ta e="T98" id="Seg_9951" s="T97">0.3.h:A</ta>
            <ta e="T99" id="Seg_9952" s="T98">np:Th</ta>
            <ta e="T102" id="Seg_9953" s="T101">0.3.h:A</ta>
            <ta e="T104" id="Seg_9954" s="T103">pro:Th</ta>
            <ta e="T106" id="Seg_9955" s="T105">np:Th</ta>
            <ta e="T107" id="Seg_9956" s="T106">0.3.h:A</ta>
            <ta e="T113" id="Seg_9957" s="T112">np:A</ta>
            <ta e="T116" id="Seg_9958" s="T115">pro.h:P</ta>
            <ta e="T117" id="Seg_9959" s="T116">0.3:A</ta>
            <ta e="T118" id="Seg_9960" s="T117">np:Th</ta>
            <ta e="T120" id="Seg_9961" s="T119">np:Th</ta>
            <ta e="T121" id="Seg_9962" s="T120">0.3.h:A</ta>
            <ta e="T122" id="Seg_9963" s="T121">adv:Time</ta>
            <ta e="T123" id="Seg_9964" s="T122">np:A</ta>
            <ta e="T125" id="Seg_9965" s="T124">np:A</ta>
            <ta e="T128" id="Seg_9966" s="T127">np:A</ta>
            <ta e="T131" id="Seg_9967" s="T130">pro.h:A</ta>
            <ta e="T132" id="Seg_9968" s="T131">adv:Pth</ta>
            <ta e="T134" id="Seg_9969" s="T133">np:A</ta>
            <ta e="T137" id="Seg_9970" s="T136">0.1.h:A</ta>
            <ta e="T138" id="Seg_9971" s="T137">pro:A</ta>
            <ta e="T139" id="Seg_9972" s="T138">np:L</ta>
            <ta e="T143" id="Seg_9973" s="T142">0.3:A</ta>
            <ta e="T144" id="Seg_9974" s="T143">pro.h:A</ta>
            <ta e="T148" id="Seg_9975" s="T147">0.3.h:E</ta>
            <ta e="T152" id="Seg_9976" s="T151">0.1.h:E</ta>
            <ta e="T153" id="Seg_9977" s="T152">np:Ins</ta>
            <ta e="T155" id="Seg_9978" s="T154">0.3.h:A</ta>
            <ta e="T156" id="Seg_9979" s="T155">np:Ins</ta>
            <ta e="T158" id="Seg_9980" s="T157">0.3.h:A</ta>
            <ta e="T159" id="Seg_9981" s="T158">pro:Th</ta>
            <ta e="T162" id="Seg_9982" s="T161">0.3:E</ta>
            <ta e="T163" id="Seg_9983" s="T162">adv:Time</ta>
            <ta e="T164" id="Seg_9984" s="T163">0.3:A</ta>
            <ta e="T166" id="Seg_9985" s="T165">np:A</ta>
            <ta e="T170" id="Seg_9986" s="T169">np.h:A</ta>
            <ta e="T173" id="Seg_9987" s="T172">np:L</ta>
            <ta e="T175" id="Seg_9988" s="T174">np.h:Th</ta>
            <ta e="T180" id="Seg_9989" s="T179">pro:Th</ta>
            <ta e="T181" id="Seg_9990" s="T180">0.3.h:A</ta>
            <ta e="T185" id="Seg_9991" s="T184">pro:Th</ta>
            <ta e="T186" id="Seg_9992" s="T185">0.3.h:A</ta>
            <ta e="T192" id="Seg_9993" s="T191">pro:Th</ta>
            <ta e="T193" id="Seg_9994" s="T192">0.3.h:A</ta>
            <ta e="T197" id="Seg_9995" s="T196">0.3.h:A</ta>
            <ta e="T198" id="Seg_9996" s="T197">np:Th</ta>
            <ta e="T199" id="Seg_9997" s="T198">0.3.h:A</ta>
            <ta e="T200" id="Seg_9998" s="T199">np:G</ta>
            <ta e="T201" id="Seg_9999" s="T200">pro.h:A</ta>
            <ta e="T202" id="Seg_10000" s="T201">adv:Time</ta>
            <ta e="T203" id="Seg_10001" s="T202">np.h:B</ta>
            <ta e="T204" id="Seg_10002" s="T203">np:Th</ta>
            <ta e="T206" id="Seg_10003" s="T205">pro.h:A</ta>
            <ta e="T208" id="Seg_10004" s="T207">np:G</ta>
            <ta e="T210" id="Seg_10005" s="T209">adv:L</ta>
            <ta e="T211" id="Seg_10006" s="T210">np.h:A</ta>
            <ta e="T214" id="Seg_10007" s="T213">pro.h:Poss</ta>
            <ta e="T215" id="Seg_10008" s="T214">np:Th</ta>
            <ta e="T216" id="Seg_10009" s="T215">0.2.h:A</ta>
            <ta e="T217" id="Seg_10010" s="T216">np.h:A</ta>
            <ta e="T220" id="Seg_10011" s="T219">np.h:A</ta>
            <ta e="T221" id="Seg_10012" s="T220">pro:L</ta>
            <ta e="T223" id="Seg_10013" s="T222">np.h:A</ta>
            <ta e="T226" id="Seg_10014" s="T225">np:Th</ta>
            <ta e="T227" id="Seg_10015" s="T226">0.1.h:A</ta>
            <ta e="T229" id="Seg_10016" s="T228">np:Th</ta>
            <ta e="T230" id="Seg_10017" s="T229">0.1.h:A</ta>
            <ta e="T233" id="Seg_10018" s="T232">np:Th</ta>
            <ta e="T234" id="Seg_10019" s="T233">0.1.h:A</ta>
            <ta e="T235" id="Seg_10020" s="T234">np:Th</ta>
            <ta e="T236" id="Seg_10021" s="T235">0.1.h:A</ta>
            <ta e="T238" id="Seg_10022" s="T237">0.1.h:A</ta>
            <ta e="T241" id="Seg_10023" s="T240">np:Th</ta>
            <ta e="T248" id="Seg_10024" s="T247">0.3:A</ta>
            <ta e="T260" id="Seg_10025" s="T259">0.1.h:A</ta>
            <ta e="T267" id="Seg_10026" s="T266">0.1.h:A</ta>
            <ta e="T276" id="Seg_10027" s="T275">0.3.h:A</ta>
            <ta e="T278" id="Seg_10028" s="T277">0.3.h:A</ta>
            <ta e="T279" id="Seg_10029" s="T278">pro.h:A</ta>
            <ta e="T280" id="Seg_10030" s="T279">np:G</ta>
            <ta e="T282" id="Seg_10031" s="T281">pro:Th</ta>
            <ta e="T285" id="Seg_10032" s="T284">0.1.h:E</ta>
            <ta e="T286" id="Seg_10033" s="T285">adv:Time</ta>
            <ta e="T287" id="Seg_10034" s="T286">pro.h:E</ta>
            <ta e="T290" id="Seg_10035" s="T289">adv:Time</ta>
            <ta e="T293" id="Seg_10036" s="T292">np:R</ta>
            <ta e="T294" id="Seg_10037" s="T293">0.1.h:A</ta>
            <ta e="T297" id="Seg_10038" s="T296">0.3.h:A</ta>
            <ta e="T299" id="Seg_10039" s="T298">np:P</ta>
            <ta e="T302" id="Seg_10040" s="T301">np:P</ta>
            <ta e="T311" id="Seg_10041" s="T310">np:P</ta>
            <ta e="T313" id="Seg_10042" s="T312">np:Ins</ta>
            <ta e="T315" id="Seg_10043" s="T314">adv:Time</ta>
            <ta e="T316" id="Seg_10044" s="T315">np:Th</ta>
            <ta e="T317" id="Seg_10045" s="T316">0.1.h:A</ta>
            <ta e="T318" id="Seg_10046" s="T317">adv:Time</ta>
            <ta e="T319" id="Seg_10047" s="T318">np:A</ta>
            <ta e="T325" id="Seg_10048" s="T324">np:L</ta>
            <ta e="T326" id="Seg_10049" s="T325">0.3:Th</ta>
            <ta e="T338" id="Seg_10050" s="T337">pro:Th</ta>
            <ta e="T341" id="Seg_10051" s="T340">pro.h:Poss</ta>
            <ta e="T342" id="Seg_10052" s="T341">np:Th</ta>
            <ta e="T347" id="Seg_10053" s="T346">pro.h:Poss</ta>
            <ta e="T348" id="Seg_10054" s="T347">np:Th</ta>
            <ta e="T352" id="Seg_10055" s="T351">0.3:E</ta>
            <ta e="T353" id="Seg_10056" s="T352">pro.h:Th</ta>
            <ta e="T357" id="Seg_10057" s="T356">0.1.h:A</ta>
            <ta e="T360" id="Seg_10058" s="T359">np:P</ta>
            <ta e="T363" id="Seg_10059" s="T362">np:P</ta>
            <ta e="T367" id="Seg_10060" s="T366">np.h:R</ta>
            <ta e="T369" id="Seg_10061" s="T368">pro.h:Poss</ta>
            <ta e="T371" id="Seg_10062" s="T370">np:Th</ta>
            <ta e="T374" id="Seg_10063" s="T373">pro.h:R</ta>
            <ta e="T375" id="Seg_10064" s="T374">np:Th</ta>
            <ta e="T377" id="Seg_10065" s="T376">np:Th</ta>
            <ta e="T378" id="Seg_10066" s="T377">pro:L</ta>
            <ta e="T381" id="Seg_10067" s="T380">pro:L</ta>
            <ta e="T382" id="Seg_10068" s="T381">0.2.h:A</ta>
            <ta e="T383" id="Seg_10069" s="T382">np:Th</ta>
            <ta e="T387" id="Seg_10070" s="T386">np.h:Th</ta>
            <ta e="T391" id="Seg_10071" s="T390">0.3.h:A</ta>
            <ta e="T392" id="Seg_10072" s="T391">np:E 0.3.h:Poss</ta>
            <ta e="T394" id="Seg_10073" s="T393">np:L</ta>
            <ta e="T395" id="Seg_10074" s="T394">np:Th</ta>
            <ta e="T397" id="Seg_10075" s="T396">np.h:E 0.3.h:Poss</ta>
            <ta e="T401" id="Seg_10076" s="T400">0.3.h:A</ta>
            <ta e="T404" id="Seg_10077" s="T403">np:P</ta>
            <ta e="T405" id="Seg_10078" s="T404">0.2.h:E</ta>
            <ta e="T411" id="Seg_10079" s="T410">np:G</ta>
            <ta e="T412" id="Seg_10080" s="T411">0.1.h:A</ta>
            <ta e="T413" id="Seg_10081" s="T412">pro:Th</ta>
            <ta e="T414" id="Seg_10082" s="T413">pro.h:R</ta>
            <ta e="T415" id="Seg_10083" s="T414">0.1.h:A</ta>
            <ta e="T418" id="Seg_10084" s="T417">0.2.h:A</ta>
            <ta e="T420" id="Seg_10085" s="T419">0.2.h:A</ta>
            <ta e="T423" id="Seg_10086" s="T422">np.h:E</ta>
            <ta e="T426" id="Seg_10087" s="T425">0.2.h:A</ta>
            <ta e="T430" id="Seg_10088" s="T429">pro.h:A</ta>
            <ta e="T434" id="Seg_10089" s="T433">np.h:E</ta>
            <ta e="T440" id="Seg_10090" s="T439">0.2.h:A</ta>
            <ta e="T441" id="Seg_10091" s="T440">pro.h:A</ta>
            <ta e="T442" id="Seg_10092" s="T441">adv:Time</ta>
            <ta e="T443" id="Seg_10093" s="T442">np:Th</ta>
            <ta e="T445" id="Seg_10094" s="T444">0.1.h:A</ta>
            <ta e="T446" id="Seg_10095" s="T445">np:P</ta>
            <ta e="T447" id="Seg_10096" s="T446">0.1.h:A</ta>
            <ta e="T448" id="Seg_10097" s="T447">0.1.h:E</ta>
            <ta e="T449" id="Seg_10098" s="T448">np:A</ta>
            <ta e="T451" id="Seg_10099" s="T450">np:P</ta>
            <ta e="T452" id="Seg_10100" s="T451">pro:Th</ta>
            <ta e="T453" id="Seg_10101" s="T452">0.2.h:A</ta>
            <ta e="T454" id="Seg_10102" s="T453">0.2.h:A</ta>
            <ta e="T455" id="Seg_10103" s="T454">pro.h:R</ta>
            <ta e="T458" id="Seg_10104" s="T457">pro:Th</ta>
            <ta e="T459" id="Seg_10105" s="T458">adv:L</ta>
            <ta e="T461" id="Seg_10106" s="T460">pro.h:A</ta>
            <ta e="T462" id="Seg_10107" s="T461">adv:Time</ta>
            <ta e="T465" id="Seg_10108" s="T464">np:Th</ta>
            <ta e="T466" id="Seg_10109" s="T465">0.1.h:A</ta>
            <ta e="T467" id="Seg_10110" s="T466">adv:Time</ta>
            <ta e="T468" id="Seg_10111" s="T467">0.1.h:A</ta>
            <ta e="T469" id="Seg_10112" s="T468">np:Th</ta>
            <ta e="T471" id="Seg_10113" s="T470">0.1.h:A</ta>
            <ta e="T472" id="Seg_10114" s="T471">np:Th</ta>
            <ta e="T473" id="Seg_10115" s="T472">0.1.h:A</ta>
            <ta e="T474" id="Seg_10116" s="T473">adv:Time</ta>
            <ta e="T476" id="Seg_10117" s="T475">np:P</ta>
            <ta e="T477" id="Seg_10118" s="T476">0.1.h:A</ta>
            <ta e="T479" id="Seg_10119" s="T478">np:P</ta>
            <ta e="T480" id="Seg_10120" s="T479">0.1.h:A</ta>
            <ta e="T481" id="Seg_10121" s="T480">np:P</ta>
            <ta e="T482" id="Seg_10122" s="T481">0.3.h:A</ta>
            <ta e="T485" id="Seg_10123" s="T484">np:Th</ta>
            <ta e="T490" id="Seg_10124" s="T489">np:A</ta>
            <ta e="T492" id="Seg_10125" s="T491">np:P</ta>
            <ta e="T496" id="Seg_10126" s="T495">np:Th</ta>
            <ta e="T497" id="Seg_10127" s="T496">0.1.h:A</ta>
            <ta e="T501" id="Seg_10128" s="T500">pro.h:A</ta>
            <ta e="T502" id="Seg_10129" s="T501">adv:Time</ta>
            <ta e="T503" id="Seg_10130" s="T502">np:P</ta>
            <ta e="T510" id="Seg_10131" s="T509">pro.h:A</ta>
            <ta e="T512" id="Seg_10132" s="T511">np:Th</ta>
            <ta e="T516" id="Seg_10133" s="T515">np:R</ta>
            <ta e="T518" id="Seg_10134" s="T517">np:A</ta>
            <ta e="T520" id="Seg_10135" s="T519">np:P</ta>
            <ta e="T524" id="Seg_10136" s="T523">0.3.h:A</ta>
            <ta e="T525" id="Seg_10137" s="T524">np:E</ta>
            <ta e="T528" id="Seg_10138" s="T527">np:Th</ta>
            <ta e="T540" id="Seg_10139" s="T539">pro:Th</ta>
            <ta e="T543" id="Seg_10140" s="T542">pro.h:E</ta>
            <ta e="T544" id="Seg_10141" s="T543">pro.h:Th</ta>
            <ta e="T547" id="Seg_10142" s="T546">pro.h:A</ta>
            <ta e="T549" id="Seg_10143" s="T548">pro.h:Poss</ta>
            <ta e="T550" id="Seg_10144" s="T549">np.h:A</ta>
            <ta e="T551" id="Seg_10145" s="T550">np:P</ta>
            <ta e="T554" id="Seg_10146" s="T553">np:P</ta>
            <ta e="T556" id="Seg_10147" s="T555">0.3.h:A</ta>
            <ta e="T558" id="Seg_10148" s="T557">0.3.h:A</ta>
            <ta e="T559" id="Seg_10149" s="T558">np:P</ta>
            <ta e="T560" id="Seg_10150" s="T559">0.3.h:A</ta>
            <ta e="T561" id="Seg_10151" s="T560">np:Ins</ta>
            <ta e="T563" id="Seg_10152" s="T562">np:Ins</ta>
            <ta e="T565" id="Seg_10153" s="T564">np:Th</ta>
            <ta e="T566" id="Seg_10154" s="T565">np:L</ta>
            <ta e="T569" id="Seg_10155" s="T567">np:P</ta>
            <ta e="T570" id="Seg_10156" s="T569">0.3.h:A</ta>
            <ta e="T572" id="Seg_10157" s="T571">np:Ins</ta>
            <ta e="T576" id="Seg_10158" s="T575">np:Th</ta>
            <ta e="T577" id="Seg_10159" s="T576">0.3.h:A</ta>
            <ta e="T578" id="Seg_10160" s="T577">np:G</ta>
            <ta e="T580" id="Seg_10161" s="T579">np:Th</ta>
            <ta e="T581" id="Seg_10162" s="T580">0.3.h:A</ta>
            <ta e="T582" id="Seg_10163" s="T581">0.3.h:A</ta>
            <ta e="T593" id="Seg_10164" s="T592">np:Th</ta>
            <ta e="T595" id="Seg_10165" s="T594">0.3.h:A</ta>
            <ta e="T596" id="Seg_10166" s="T595">pro.h:Poss</ta>
            <ta e="T598" id="Seg_10167" s="T597">np.h:B</ta>
            <ta e="T599" id="Seg_10168" s="T598">0.2.h:E</ta>
            <ta e="T600" id="Seg_10169" s="T599">np:E</ta>
            <ta e="T603" id="Seg_10170" s="T602">pro.h:Poss</ta>
            <ta e="T605" id="Seg_10171" s="T604">np:Th</ta>
            <ta e="T609" id="Seg_10172" s="T608">adv:Time</ta>
            <ta e="T610" id="Seg_10173" s="T609">0.1.h:A</ta>
            <ta e="T614" id="Seg_10174" s="T613">np.h:A</ta>
            <ta e="T617" id="Seg_10175" s="T616">0.3.h:A</ta>
            <ta e="T620" id="Seg_10176" s="T619">np:P</ta>
            <ta e="T622" id="Seg_10177" s="T621">0.3.h:A</ta>
            <ta e="T627" id="Seg_10178" s="T626">pro:Th</ta>
            <ta e="T628" id="Seg_10179" s="T627">pro.h:R</ta>
            <ta e="T630" id="Seg_10180" s="T629">pro.h:E</ta>
            <ta e="T631" id="Seg_10181" s="T630">np:Th</ta>
            <ta e="T633" id="Seg_10182" s="T632">adv:Time</ta>
            <ta e="T635" id="Seg_10183" s="T634">np:A</ta>
            <ta e="T639" id="Seg_10184" s="T638">np.h:A</ta>
            <ta e="T640" id="Seg_10185" s="T639">np:Th</ta>
            <ta e="T643" id="Seg_10186" s="T642">pro.h:R</ta>
            <ta e="T645" id="Seg_10187" s="T644">pro.h:Poss</ta>
            <ta e="T646" id="Seg_10188" s="T645">np:Th</ta>
            <ta e="T658" id="Seg_10189" s="T657">pro:Th</ta>
            <ta e="T660" id="Seg_10190" s="T659">pro.h:R</ta>
            <ta e="T673" id="Seg_10191" s="T672">pro:L</ta>
            <ta e="T675" id="Seg_10192" s="T674">pro.h:A</ta>
            <ta e="T676" id="Seg_10193" s="T675">adv:Time</ta>
            <ta e="T691" id="Seg_10194" s="T690">0.1.h:A</ta>
            <ta e="T697" id="Seg_10195" s="T696">0.1.h:E</ta>
            <ta e="T698" id="Seg_10196" s="T697">pro:Th</ta>
            <ta e="T700" id="Seg_10197" s="T699">0.1.h:E</ta>
            <ta e="T702" id="Seg_10198" s="T701">np:L</ta>
            <ta e="T703" id="Seg_10199" s="T702">np:Th</ta>
            <ta e="T707" id="Seg_10200" s="T706">adv:Time</ta>
            <ta e="T712" id="Seg_10201" s="T711">np:Th</ta>
            <ta e="T714" id="Seg_10202" s="T713">np:Th</ta>
            <ta e="T718" id="Seg_10203" s="T717">np:P</ta>
            <ta e="T721" id="Seg_10204" s="T720">np:Th</ta>
            <ta e="T725" id="Seg_10205" s="T724">np:P</ta>
            <ta e="T728" id="Seg_10206" s="T727">pro.h:E</ta>
            <ta e="T732" id="Seg_10207" s="T731">np:P</ta>
            <ta e="T733" id="Seg_10208" s="T732">0.1.h:E</ta>
            <ta e="T735" id="Seg_10209" s="T734">pro.h:E</ta>
            <ta e="T738" id="Seg_10210" s="T737">np:P</ta>
            <ta e="T740" id="Seg_10211" s="T739">0.1.h:E</ta>
            <ta e="T741" id="Seg_10212" s="T740">adv:Time</ta>
            <ta e="T743" id="Seg_10213" s="T742">np:Th</ta>
            <ta e="T744" id="Seg_10214" s="T743">0.3.h:A</ta>
            <ta e="T752" id="Seg_10215" s="T901">adv:Time</ta>
            <ta e="T753" id="Seg_10216" s="T752">np:Th</ta>
            <ta e="T755" id="Seg_10217" s="T918">0.3.h:A</ta>
            <ta e="T761" id="Seg_10218" s="T760">0.3.h:P</ta>
            <ta e="T762" id="Seg_10219" s="T761">pro:Th</ta>
            <ta e="T765" id="Seg_10220" s="T764">np:Th</ta>
            <ta e="T772" id="Seg_10221" s="T771">np:A</ta>
            <ta e="T782" id="Seg_10222" s="T781">np:A</ta>
            <ta e="T784" id="Seg_10223" s="T783">0.3:E</ta>
            <ta e="T787" id="Seg_10224" s="T786">np:P</ta>
            <ta e="T789" id="Seg_10225" s="T788">pro.h:Poss</ta>
            <ta e="T790" id="Seg_10226" s="T789">np:L</ta>
            <ta e="T792" id="Seg_10227" s="T791">np:Th</ta>
            <ta e="T794" id="Seg_10228" s="T793">pro.h:Poss</ta>
            <ta e="T795" id="Seg_10229" s="T794">np:L</ta>
            <ta e="T796" id="Seg_10230" s="T795">np:Th</ta>
            <ta e="T798" id="Seg_10231" s="T797">np:Ins</ta>
            <ta e="T800" id="Seg_10232" s="T799">0.2.h:E</ta>
            <ta e="T801" id="Seg_10233" s="T800">adv:Time</ta>
            <ta e="T802" id="Seg_10234" s="T801">np.h:A</ta>
            <ta e="T805" id="Seg_10235" s="T804">np:Th</ta>
            <ta e="T807" id="Seg_10236" s="T806">pro:A</ta>
            <ta e="T811" id="Seg_10237" s="T810">0.3.h:A</ta>
            <ta e="T812" id="Seg_10238" s="T811">np:G</ta>
            <ta e="T813" id="Seg_10239" s="T812">n:Time</ta>
            <ta e="T815" id="Seg_10240" s="T814">0.3:A</ta>
            <ta e="T817" id="Seg_10241" s="T816">np:Th</ta>
            <ta e="T819" id="Seg_10242" s="T818">np:A</ta>
            <ta e="T822" id="Seg_10243" s="T821">np:Th</ta>
            <ta e="T824" id="Seg_10244" s="T823">np:A</ta>
            <ta e="T831" id="Seg_10245" s="T830">adv:Time</ta>
            <ta e="T832" id="Seg_10246" s="T831">n:Time</ta>
            <ta e="T840" id="Seg_10247" s="T839">0.1.h:A</ta>
            <ta e="T841" id="Seg_10248" s="T840">np:P</ta>
            <ta e="T844" id="Seg_10249" s="T843">np:L</ta>
            <ta e="T846" id="Seg_10250" s="T845">np:Th</ta>
            <ta e="T849" id="Seg_10251" s="T848">0.3:Th</ta>
            <ta e="T850" id="Seg_10252" s="T849">pro:Th</ta>
            <ta e="T852" id="Seg_10253" s="T851">0.1.h:E</ta>
            <ta e="T853" id="Seg_10254" s="T852">pro.h:Poss</ta>
            <ta e="T854" id="Seg_10255" s="T853">np.h:E</ta>
            <ta e="T868" id="Seg_10256" s="T867">0.2.h:A</ta>
            <ta e="T869" id="Seg_10257" s="T868">pro.h:A</ta>
            <ta e="T870" id="Seg_10258" s="T869">pro:G</ta>
            <ta e="T873" id="Seg_10259" s="T872">pro.h:Th</ta>
            <ta e="T875" id="Seg_10260" s="T874">pro:G</ta>
            <ta e="T878" id="Seg_10261" s="T877">pro.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-PKZ">
            <ta e="T5" id="Seg_10262" s="T4">pro.h:S</ta>
            <ta e="T6" id="Seg_10263" s="T5">ptcl.neg</ta>
            <ta e="T7" id="Seg_10264" s="T6">v:pred</ta>
            <ta e="T11" id="Seg_10265" s="T10">np:S</ta>
            <ta e="T12" id="Seg_10266" s="T11">v:pred</ta>
            <ta e="T14" id="Seg_10267" s="T13">pro.h:S</ta>
            <ta e="T15" id="Seg_10268" s="T14">v:pred</ta>
            <ta e="T16" id="Seg_10269" s="T15">np:S</ta>
            <ta e="T20" id="Seg_10270" s="T19">n:pred</ta>
            <ta e="T21" id="Seg_10271" s="T20">pro.h:O</ta>
            <ta e="T27" id="Seg_10272" s="T26">np.h:S</ta>
            <ta e="T28" id="Seg_10273" s="T27">v:pred</ta>
            <ta e="T29" id="Seg_10274" s="T28">pro:S</ta>
            <ta e="T35" id="Seg_10275" s="T34">np:O</ta>
            <ta e="T42" id="Seg_10276" s="T41">np.h:S</ta>
            <ta e="T43" id="Seg_10277" s="T42">adj:pred</ta>
            <ta e="T44" id="Seg_10278" s="T43">pro:O</ta>
            <ta e="T45" id="Seg_10279" s="T44">ptcl.neg</ta>
            <ta e="T46" id="Seg_10280" s="T45">v:pred 0.3.h:S</ta>
            <ta e="T47" id="Seg_10281" s="T46">pro:O</ta>
            <ta e="T48" id="Seg_10282" s="T47">ptcl.neg</ta>
            <ta e="T49" id="Seg_10283" s="T48">v:pred 0.3.h:S</ta>
            <ta e="T52" id="Seg_10284" s="T51">np.h:S</ta>
            <ta e="T54" id="Seg_10285" s="T53">adj:pred</ta>
            <ta e="T55" id="Seg_10286" s="T54">np:O</ta>
            <ta e="T56" id="Seg_10287" s="T55">v:pred 0.3.h:S</ta>
            <ta e="T63" id="Seg_10288" s="T62">ptcl.neg</ta>
            <ta e="T70" id="Seg_10289" s="T69">pro:O</ta>
            <ta e="T71" id="Seg_10290" s="T70">ptcl.neg</ta>
            <ta e="T72" id="Seg_10291" s="T71">v:pred 0.2.h:S</ta>
            <ta e="T73" id="Seg_10292" s="T72">np:O</ta>
            <ta e="T74" id="Seg_10293" s="T73">v:pred 0.3.h:S</ta>
            <ta e="T77" id="Seg_10294" s="T76">np:S</ta>
            <ta e="T78" id="Seg_10295" s="T77">adj:pred</ta>
            <ta e="T84" id="Seg_10296" s="T83">np:O</ta>
            <ta e="T85" id="Seg_10297" s="T84">ptcl.neg</ta>
            <ta e="T86" id="Seg_10298" s="T85">v:pred 0.3.h:S</ta>
            <ta e="T87" id="Seg_10299" s="T86">pro:S</ta>
            <ta e="T88" id="Seg_10300" s="T87">v:pred</ta>
            <ta e="T89" id="Seg_10301" s="T88">np:O</ta>
            <ta e="T90" id="Seg_10302" s="T89">v:pred 0.3.h:S</ta>
            <ta e="T91" id="Seg_10303" s="T90">np:O</ta>
            <ta e="T92" id="Seg_10304" s="T91">v:pred 0.3.h:S</ta>
            <ta e="T95" id="Seg_10305" s="T94">np:O</ta>
            <ta e="T96" id="Seg_10306" s="T95">v:pred 0.3.h:S</ta>
            <ta e="T97" id="Seg_10307" s="T96">np:O</ta>
            <ta e="T98" id="Seg_10308" s="T97">v:pred 0.3.h:S</ta>
            <ta e="T99" id="Seg_10309" s="T98">np:S</ta>
            <ta e="T100" id="Seg_10310" s="T99">v:pred</ta>
            <ta e="T102" id="Seg_10311" s="T101">v:pred 0.3.h:S</ta>
            <ta e="T104" id="Seg_10312" s="T103">pro:S</ta>
            <ta e="T105" id="Seg_10313" s="T104">v:pred</ta>
            <ta e="T106" id="Seg_10314" s="T105">np:O</ta>
            <ta e="T107" id="Seg_10315" s="T106">v:pred 0.3.h:S</ta>
            <ta e="T112" id="Seg_10316" s="T111">v:pred</ta>
            <ta e="T113" id="Seg_10317" s="T112">np:S</ta>
            <ta e="T114" id="Seg_10318" s="T113">v:pred</ta>
            <ta e="T116" id="Seg_10319" s="T115">pro.h:O</ta>
            <ta e="T117" id="Seg_10320" s="T116">v:pred 0.3:S</ta>
            <ta e="T118" id="Seg_10321" s="T117">np:S</ta>
            <ta e="T119" id="Seg_10322" s="T118">v:pred</ta>
            <ta e="T120" id="Seg_10323" s="T119">np:O</ta>
            <ta e="T121" id="Seg_10324" s="T120">v:pred 0.3.h:S</ta>
            <ta e="T123" id="Seg_10325" s="T122">np:S</ta>
            <ta e="T124" id="Seg_10326" s="T123">v:pred</ta>
            <ta e="T125" id="Seg_10327" s="T124">np:S</ta>
            <ta e="T126" id="Seg_10328" s="T125">v:pred</ta>
            <ta e="T128" id="Seg_10329" s="T127">np:S</ta>
            <ta e="T130" id="Seg_10330" s="T129">v:pred</ta>
            <ta e="T131" id="Seg_10331" s="T130">pro.h:S</ta>
            <ta e="T133" id="Seg_10332" s="T132">v:pred</ta>
            <ta e="T134" id="Seg_10333" s="T133">np:S</ta>
            <ta e="T136" id="Seg_10334" s="T135">v:pred</ta>
            <ta e="T137" id="Seg_10335" s="T136">v:pred 0.1.h:S</ta>
            <ta e="T138" id="Seg_10336" s="T137">pro:S</ta>
            <ta e="T140" id="Seg_10337" s="T139">v:pred</ta>
            <ta e="T143" id="Seg_10338" s="T142">v:pred 0.3:S</ta>
            <ta e="T144" id="Seg_10339" s="T143">pro.h:S</ta>
            <ta e="T145" id="Seg_10340" s="T144">v:pred</ta>
            <ta e="T148" id="Seg_10341" s="T147">v:pred 0.1.h:S</ta>
            <ta e="T152" id="Seg_10342" s="T151">v:pred 0.1.h:S</ta>
            <ta e="T155" id="Seg_10343" s="T154">v:pred 0.3.h:S</ta>
            <ta e="T158" id="Seg_10344" s="T157">v:pred 0.3.h:S</ta>
            <ta e="T159" id="Seg_10345" s="T158">pro:S</ta>
            <ta e="T161" id="Seg_10346" s="T160">v:pred</ta>
            <ta e="T162" id="Seg_10347" s="T161">v:pred 0.3:S</ta>
            <ta e="T164" id="Seg_10348" s="T163">v:pred 0.3:S</ta>
            <ta e="T166" id="Seg_10349" s="T165">np:S</ta>
            <ta e="T167" id="Seg_10350" s="T166">v:pred</ta>
            <ta e="T170" id="Seg_10351" s="T169">np.h:S</ta>
            <ta e="T172" id="Seg_10352" s="T171">v:pred</ta>
            <ta e="T175" id="Seg_10353" s="T174">np.h:S</ta>
            <ta e="T177" id="Seg_10354" s="T176">ptcl.neg</ta>
            <ta e="T178" id="Seg_10355" s="T177">adj:pred</ta>
            <ta e="T180" id="Seg_10356" s="T179">pro:O</ta>
            <ta e="T181" id="Seg_10357" s="T180">v:pred 0.3.h:S</ta>
            <ta e="T185" id="Seg_10358" s="T184">pro:O</ta>
            <ta e="T186" id="Seg_10359" s="T185">v:pred 0.3.h:S</ta>
            <ta e="T192" id="Seg_10360" s="T191">pro:O</ta>
            <ta e="T193" id="Seg_10361" s="T192">v:pred 0.3.h:S</ta>
            <ta e="T197" id="Seg_10362" s="T196">v:pred 0.3.h:S</ta>
            <ta e="T198" id="Seg_10363" s="T197">np:O</ta>
            <ta e="T199" id="Seg_10364" s="T198">v:pred 0.3.h:S</ta>
            <ta e="T201" id="Seg_10365" s="T200">pro.h:S</ta>
            <ta e="T204" id="Seg_10366" s="T203">np:O</ta>
            <ta e="T205" id="Seg_10367" s="T204">v:pred</ta>
            <ta e="T206" id="Seg_10368" s="T205">pro.h:S</ta>
            <ta e="T207" id="Seg_10369" s="T206">v:pred</ta>
            <ta e="T209" id="Seg_10370" s="T208">s:purp</ta>
            <ta e="T211" id="Seg_10371" s="T210">np.h:S</ta>
            <ta e="T212" id="Seg_10372" s="T211">v:pred</ta>
            <ta e="T215" id="Seg_10373" s="T214">np:O</ta>
            <ta e="T216" id="Seg_10374" s="T215">v:pred 0.2.h:S</ta>
            <ta e="T217" id="Seg_10375" s="T216">np.h:S</ta>
            <ta e="T218" id="Seg_10376" s="T217">v:pred</ta>
            <ta e="T220" id="Seg_10377" s="T219">np.h:S</ta>
            <ta e="T222" id="Seg_10378" s="T221">v:pred</ta>
            <ta e="T223" id="Seg_10379" s="T222">np.h:S</ta>
            <ta e="T224" id="Seg_10380" s="T223">v:pred</ta>
            <ta e="T226" id="Seg_10381" s="T225">np:O</ta>
            <ta e="T227" id="Seg_10382" s="T226">v:pred 0.1.h:S</ta>
            <ta e="T229" id="Seg_10383" s="T228">np:O</ta>
            <ta e="T230" id="Seg_10384" s="T229">v:pred 0.1.h:S</ta>
            <ta e="T233" id="Seg_10385" s="T232">np:O</ta>
            <ta e="T234" id="Seg_10386" s="T233">v:pred 0.1.h:S</ta>
            <ta e="T235" id="Seg_10387" s="T234">np:O</ta>
            <ta e="T236" id="Seg_10388" s="T235">v:pred 0.1.h:S</ta>
            <ta e="T238" id="Seg_10389" s="T237">v:pred 0.1.h:S</ta>
            <ta e="T241" id="Seg_10390" s="T240">np:O</ta>
            <ta e="T247" id="Seg_10391" s="T246">ptcl.neg</ta>
            <ta e="T248" id="Seg_10392" s="T247">v:pred 0.3:S</ta>
            <ta e="T258" id="Seg_10393" s="T257">adj:pred</ta>
            <ta e="T259" id="Seg_10394" s="T258">ptcl.neg</ta>
            <ta e="T260" id="Seg_10395" s="T259">v:pred 0.1.h:S</ta>
            <ta e="T267" id="Seg_10396" s="T266">v:pred 0.1.h:S</ta>
            <ta e="T270" id="Seg_10397" s="T269">ptcl.neg</ta>
            <ta e="T276" id="Seg_10398" s="T275">v:pred 0.3.h:S</ta>
            <ta e="T278" id="Seg_10399" s="T277">v:pred 0.3.h:S</ta>
            <ta e="T279" id="Seg_10400" s="T278">pro.h:S</ta>
            <ta e="T281" id="Seg_10401" s="T280">v:pred</ta>
            <ta e="T282" id="Seg_10402" s="T281">pro:O</ta>
            <ta e="T285" id="Seg_10403" s="T284">v:pred 0.1.h:S</ta>
            <ta e="T287" id="Seg_10404" s="T286">pro.h:S</ta>
            <ta e="T288" id="Seg_10405" s="T287">v:pred</ta>
            <ta e="T293" id="Seg_10406" s="T292">np:O</ta>
            <ta e="T294" id="Seg_10407" s="T293">v:pred 0.1.h:S</ta>
            <ta e="T296" id="Seg_10408" s="T295">conv:pred</ta>
            <ta e="T297" id="Seg_10409" s="T296">v:pred 0.3.h:S</ta>
            <ta e="T298" id="Seg_10410" s="T297">ptcl:pred</ta>
            <ta e="T299" id="Seg_10411" s="T298">np:O</ta>
            <ta e="T301" id="Seg_10412" s="T300">ptcl:pred</ta>
            <ta e="T302" id="Seg_10413" s="T301">np:O</ta>
            <ta e="T311" id="Seg_10414" s="T310">np:O</ta>
            <ta e="T312" id="Seg_10415" s="T311">ptcl:pred</ta>
            <ta e="T316" id="Seg_10416" s="T315">np:O</ta>
            <ta e="T317" id="Seg_10417" s="T316">v:pred 0.1.h:S</ta>
            <ta e="T319" id="Seg_10418" s="T318">np:S</ta>
            <ta e="T320" id="Seg_10419" s="T319">ptcl:pred</ta>
            <ta e="T321" id="Seg_10420" s="T320">v:pred</ta>
            <ta e="T326" id="Seg_10421" s="T325">v:pred 0.3:S</ta>
            <ta e="T338" id="Seg_10422" s="T337">pro:S</ta>
            <ta e="T340" id="Seg_10423" s="T339">v:pred</ta>
            <ta e="T342" id="Seg_10424" s="T341">np:S</ta>
            <ta e="T348" id="Seg_10425" s="T347">np:S</ta>
            <ta e="T350" id="Seg_10426" s="T349">adj:pred</ta>
            <ta e="T352" id="Seg_10427" s="T351">v:pred 0.3:S</ta>
            <ta e="T353" id="Seg_10428" s="T352">pro.h:S</ta>
            <ta e="T354" id="Seg_10429" s="T353">n:pred</ta>
            <ta e="T355" id="Seg_10430" s="T354">cop</ta>
            <ta e="T356" id="Seg_10431" s="T355">s:purp conv:pred</ta>
            <ta e="T357" id="Seg_10432" s="T356">0.1.h:S v:pred</ta>
            <ta e="T358" id="Seg_10433" s="T357">ptcl:pred</ta>
            <ta e="T360" id="Seg_10434" s="T359">np:O</ta>
            <ta e="T362" id="Seg_10435" s="T361">ptcl:pred</ta>
            <ta e="T363" id="Seg_10436" s="T362">np:O</ta>
            <ta e="T368" id="Seg_10437" s="T366">s:purp</ta>
            <ta e="T371" id="Seg_10438" s="T370">np:S</ta>
            <ta e="T372" id="Seg_10439" s="T371">v:pred</ta>
            <ta e="T373" id="Seg_10440" s="T372">ptcl:pred</ta>
            <ta e="T375" id="Seg_10441" s="T374">np:O</ta>
            <ta e="T377" id="Seg_10442" s="T376">np:S</ta>
            <ta e="T379" id="Seg_10443" s="T378">v:pred</ta>
            <ta e="T382" id="Seg_10444" s="T381">v:pred 0.2.h:S</ta>
            <ta e="T383" id="Seg_10445" s="T382">np:S</ta>
            <ta e="T384" id="Seg_10446" s="T383">adj:pred</ta>
            <ta e="T385" id="Seg_10447" s="T384">cop</ta>
            <ta e="T387" id="Seg_10448" s="T386">np.h:S</ta>
            <ta e="T389" id="Seg_10449" s="T388">v:pred</ta>
            <ta e="T390" id="Seg_10450" s="T389">ptcl.neg</ta>
            <ta e="T391" id="Seg_10451" s="T390">v:pred 0.3.h:S</ta>
            <ta e="T392" id="Seg_10452" s="T391">np:S</ta>
            <ta e="T393" id="Seg_10453" s="T392">v:pred</ta>
            <ta e="T395" id="Seg_10454" s="T394">np:S</ta>
            <ta e="T396" id="Seg_10455" s="T395">v:pred</ta>
            <ta e="T397" id="Seg_10456" s="T396">np.h:S</ta>
            <ta e="T399" id="Seg_10457" s="T398">v:pred</ta>
            <ta e="T400" id="Seg_10458" s="T399">ptcl.neg</ta>
            <ta e="T401" id="Seg_10459" s="T400">v:pred 0.3.h:S</ta>
            <ta e="T404" id="Seg_10460" s="T403">np:O</ta>
            <ta e="T405" id="Seg_10461" s="T404">v:pred 0.2.h:S</ta>
            <ta e="T412" id="Seg_10462" s="T411">v:pred 0.1.h:S</ta>
            <ta e="T413" id="Seg_10463" s="T412">pro:O</ta>
            <ta e="T415" id="Seg_10464" s="T414">v:pred 0.1.h:S</ta>
            <ta e="T417" id="Seg_10465" s="T416">ptcl.neg</ta>
            <ta e="T418" id="Seg_10466" s="T417">v:pred 0.2.h:S</ta>
            <ta e="T420" id="Seg_10467" s="T419">v:pred 0.2.h:S</ta>
            <ta e="T423" id="Seg_10468" s="T422">np.h:S</ta>
            <ta e="T424" id="Seg_10469" s="T423">v:pred</ta>
            <ta e="T426" id="Seg_10470" s="T425">v:pred 0.2.h:S</ta>
            <ta e="T430" id="Seg_10471" s="T429">pro.h:S</ta>
            <ta e="T431" id="Seg_10472" s="T430">v:pred</ta>
            <ta e="T434" id="Seg_10473" s="T433">np.h:S</ta>
            <ta e="T436" id="Seg_10474" s="T435">v:pred</ta>
            <ta e="T440" id="Seg_10475" s="T439">v:pred 0.2.h:S</ta>
            <ta e="T441" id="Seg_10476" s="T440">pro.h:S</ta>
            <ta e="T443" id="Seg_10477" s="T442">np:O</ta>
            <ta e="T444" id="Seg_10478" s="T443">v:pred</ta>
            <ta e="T445" id="Seg_10479" s="T444">v:pred 0.1.h:S</ta>
            <ta e="T446" id="Seg_10480" s="T445">np:O</ta>
            <ta e="T447" id="Seg_10481" s="T446">v:pred 0.1.h:S</ta>
            <ta e="T448" id="Seg_10482" s="T447">v:pred 0.1.h:S</ta>
            <ta e="T449" id="Seg_10483" s="T448">np:S</ta>
            <ta e="T450" id="Seg_10484" s="T449">v:pred</ta>
            <ta e="T451" id="Seg_10485" s="T450">np:O</ta>
            <ta e="T452" id="Seg_10486" s="T451">pro:O</ta>
            <ta e="T453" id="Seg_10487" s="T452">v:pred 0.2.h:S</ta>
            <ta e="T454" id="Seg_10488" s="T453">v:pred 0.2.h:S</ta>
            <ta e="T458" id="Seg_10489" s="T457">pro:S</ta>
            <ta e="T460" id="Seg_10490" s="T459">v:pred</ta>
            <ta e="T461" id="Seg_10491" s="T460">pro.h:S</ta>
            <ta e="T463" id="Seg_10492" s="T462">conv:pred</ta>
            <ta e="T464" id="Seg_10493" s="T463">v:pred</ta>
            <ta e="T465" id="Seg_10494" s="T464">np:O</ta>
            <ta e="T466" id="Seg_10495" s="T465">v:pred 0.1.h:S</ta>
            <ta e="T468" id="Seg_10496" s="T467">v:pred 0.1.h:S</ta>
            <ta e="T469" id="Seg_10497" s="T468">np:O</ta>
            <ta e="T471" id="Seg_10498" s="T470">v:pred 0.1.h:S</ta>
            <ta e="T472" id="Seg_10499" s="T471">np:O</ta>
            <ta e="T473" id="Seg_10500" s="T472">v:pred 0.1.h:S</ta>
            <ta e="T476" id="Seg_10501" s="T475">np:O</ta>
            <ta e="T477" id="Seg_10502" s="T476">v:pred 0.1.h:S</ta>
            <ta e="T479" id="Seg_10503" s="T478">np:O</ta>
            <ta e="T480" id="Seg_10504" s="T479">v:pred 0.1.h:S</ta>
            <ta e="T481" id="Seg_10505" s="T480">np:O</ta>
            <ta e="T482" id="Seg_10506" s="T481">v:pred 0.3.h:S</ta>
            <ta e="T485" id="Seg_10507" s="T484">np:S</ta>
            <ta e="T486" id="Seg_10508" s="T485">v:pred</ta>
            <ta e="T490" id="Seg_10509" s="T489">np:S</ta>
            <ta e="T492" id="Seg_10510" s="T491">np:O</ta>
            <ta e="T493" id="Seg_10511" s="T492">v:pred</ta>
            <ta e="T496" id="Seg_10512" s="T495">np:O</ta>
            <ta e="T497" id="Seg_10513" s="T496">v:pred 0.1.h:S</ta>
            <ta e="T501" id="Seg_10514" s="T500">pro.h:S</ta>
            <ta e="T503" id="Seg_10515" s="T502">np:O</ta>
            <ta e="T504" id="Seg_10516" s="T503">v:pred</ta>
            <ta e="T505" id="Seg_10517" s="T504">ptcl.neg</ta>
            <ta e="T506" id="Seg_10518" s="T505">adj:pred</ta>
            <ta e="T507" id="Seg_10519" s="T506">ptcl.neg</ta>
            <ta e="T508" id="Seg_10520" s="T507">adj:pred</ta>
            <ta e="T510" id="Seg_10521" s="T509">pro.h:S</ta>
            <ta e="T512" id="Seg_10522" s="T511">np:O</ta>
            <ta e="T517" id="Seg_10523" s="T516">v:pred</ta>
            <ta e="T518" id="Seg_10524" s="T517">np:S</ta>
            <ta e="T520" id="Seg_10525" s="T519">np:O</ta>
            <ta e="T521" id="Seg_10526" s="T520">v:pred</ta>
            <ta e="T524" id="Seg_10527" s="T523">v:pred 0.3.h:S</ta>
            <ta e="T528" id="Seg_10528" s="T527">np:O</ta>
            <ta e="T529" id="Seg_10529" s="T528">v:pred</ta>
            <ta e="T531" id="Seg_10530" s="T530">n:pred</ta>
            <ta e="T532" id="Seg_10531" s="T531">cop 0.3:S</ta>
            <ta e="T533" id="Seg_10532" s="T532">n:pred</ta>
            <ta e="T534" id="Seg_10533" s="T533">cop 0.3:S</ta>
            <ta e="T540" id="Seg_10534" s="T539">pro:S</ta>
            <ta e="T541" id="Seg_10535" s="T540">ptcl.neg</ta>
            <ta e="T542" id="Seg_10536" s="T541">v:pred</ta>
            <ta e="T543" id="Seg_10537" s="T542">pro.h:S</ta>
            <ta e="T544" id="Seg_10538" s="T543">pro.h:O</ta>
            <ta e="T545" id="Seg_10539" s="T544">ptcl.neg</ta>
            <ta e="T546" id="Seg_10540" s="T545">v:pred</ta>
            <ta e="T547" id="Seg_10541" s="T546">pro.h:S</ta>
            <ta e="T548" id="Seg_10542" s="T547">v:pred </ta>
            <ta e="T550" id="Seg_10543" s="T549">np.h:S</ta>
            <ta e="T551" id="Seg_10544" s="T550">np:O</ta>
            <ta e="T552" id="Seg_10545" s="T551">v:pred</ta>
            <ta e="T554" id="Seg_10546" s="T553">np:O</ta>
            <ta e="T556" id="Seg_10547" s="T555">v:pred 0.3.h:S</ta>
            <ta e="T558" id="Seg_10548" s="T557">v:pred 0.3.h:S</ta>
            <ta e="T559" id="Seg_10549" s="T558">np:O</ta>
            <ta e="T560" id="Seg_10550" s="T559">v:pred 0.3.h:S</ta>
            <ta e="T565" id="Seg_10551" s="T564">np:S</ta>
            <ta e="T567" id="Seg_10552" s="T566">v:pred</ta>
            <ta e="T569" id="Seg_10553" s="T567">np:O</ta>
            <ta e="T570" id="Seg_10554" s="T569">v:pred 0.3.h:S</ta>
            <ta e="T576" id="Seg_10555" s="T575">np:O</ta>
            <ta e="T577" id="Seg_10556" s="T576">v:pred 0.3.h:S</ta>
            <ta e="T580" id="Seg_10557" s="T579">np:O</ta>
            <ta e="T581" id="Seg_10558" s="T580">v:pred 0.3.h:S</ta>
            <ta e="T582" id="Seg_10559" s="T581">v:pred 0.3.h:S</ta>
            <ta e="T593" id="Seg_10560" s="T592">np:O</ta>
            <ta e="T595" id="Seg_10561" s="T594">v:pred 0.3.h:S</ta>
            <ta e="T598" id="Seg_10562" s="T597">np.h:O</ta>
            <ta e="T599" id="Seg_10563" s="T598">v:pred 0.2.h:S</ta>
            <ta e="T600" id="Seg_10564" s="T599">np:S</ta>
            <ta e="T601" id="Seg_10565" s="T600">v:pred</ta>
            <ta e="T605" id="Seg_10566" s="T604">np:S</ta>
            <ta e="T606" id="Seg_10567" s="T605">adj:pred</ta>
            <ta e="T610" id="Seg_10568" s="T609">v:pred 0.1.h:S</ta>
            <ta e="T612" id="Seg_10569" s="T611">adj:pred</ta>
            <ta e="T614" id="Seg_10570" s="T613">np.h:S</ta>
            <ta e="T616" id="Seg_10571" s="T615">v:pred</ta>
            <ta e="T617" id="Seg_10572" s="T616">v:pred 0.3.h:S</ta>
            <ta e="T620" id="Seg_10573" s="T619">np:O</ta>
            <ta e="T621" id="Seg_10574" s="T620">ptcl.neg</ta>
            <ta e="T622" id="Seg_10575" s="T621">v:pred 0.3.h:S</ta>
            <ta e="T627" id="Seg_10576" s="T626">pro:O</ta>
            <ta e="T629" id="Seg_10577" s="T628">v:pred</ta>
            <ta e="T630" id="Seg_10578" s="T629">pro.h:S</ta>
            <ta e="T631" id="Seg_10579" s="T630">np:O</ta>
            <ta e="T632" id="Seg_10580" s="T631">v:pred</ta>
            <ta e="T635" id="Seg_10581" s="T634">np:S</ta>
            <ta e="T636" id="Seg_10582" s="T635">ptcl.neg</ta>
            <ta e="T637" id="Seg_10583" s="T636">v:pred</ta>
            <ta e="T639" id="Seg_10584" s="T638">np.h:S</ta>
            <ta e="T640" id="Seg_10585" s="T639">np:O</ta>
            <ta e="T641" id="Seg_10586" s="T640">v:pred</ta>
            <ta e="T642" id="Seg_10587" s="T641">ptcl:pred</ta>
            <ta e="T646" id="Seg_10588" s="T645">np:S</ta>
            <ta e="T647" id="Seg_10589" s="T646">v:pred</ta>
            <ta e="T648" id="Seg_10590" s="T647">s:purp</ta>
            <ta e="T657" id="Seg_10591" s="T656">v:pred</ta>
            <ta e="T658" id="Seg_10592" s="T657">pro:O</ta>
            <ta e="T659" id="Seg_10593" s="T658">v:pred</ta>
            <ta e="T665" id="Seg_10594" s="T664">ptcl:pred</ta>
            <ta e="T670" id="Seg_10595" s="T668">ptcl:pred</ta>
            <ta e="T672" id="Seg_10596" s="T671">s:purp</ta>
            <ta e="T674" id="Seg_10597" s="T673">s:purp</ta>
            <ta e="T675" id="Seg_10598" s="T674">pro.h:S</ta>
            <ta e="T678" id="Seg_10599" s="T677">ptcl.neg</ta>
            <ta e="T681" id="Seg_10600" s="T680">v:pred</ta>
            <ta e="T682" id="Seg_10601" s="T681">s:purp</ta>
            <ta e="T690" id="Seg_10602" s="T689">ptcl.neg</ta>
            <ta e="T691" id="Seg_10603" s="T690">v:pred 0.1.h:S</ta>
            <ta e="T692" id="Seg_10604" s="T691">s:purp</ta>
            <ta e="T697" id="Seg_10605" s="T696">v:pred 0.1.h:S</ta>
            <ta e="T698" id="Seg_10606" s="T697">pro:O</ta>
            <ta e="T699" id="Seg_10607" s="T698">ptcl.neg</ta>
            <ta e="T700" id="Seg_10608" s="T699">v:pred 0.1.h:S</ta>
            <ta e="T703" id="Seg_10609" s="T702">np:S</ta>
            <ta e="T705" id="Seg_10610" s="T704">adj:pred</ta>
            <ta e="T706" id="Seg_10611" s="T705">cop</ta>
            <ta e="T710" id="Seg_10612" s="T709">adj:pred</ta>
            <ta e="T711" id="Seg_10613" s="T710">cop</ta>
            <ta e="T712" id="Seg_10614" s="T711">np:S</ta>
            <ta e="T714" id="Seg_10615" s="T713">np:S</ta>
            <ta e="T715" id="Seg_10616" s="T714">v:pred</ta>
            <ta e="T718" id="Seg_10617" s="T717">np:S</ta>
            <ta e="T719" id="Seg_10618" s="T718">v:pred</ta>
            <ta e="T721" id="Seg_10619" s="T720">np:S</ta>
            <ta e="T722" id="Seg_10620" s="T721">v:pred</ta>
            <ta e="T725" id="Seg_10621" s="T724">np:O</ta>
            <ta e="T727" id="Seg_10622" s="T726">v:pred 0.3.h:S</ta>
            <ta e="T728" id="Seg_10623" s="T727">pro.h:S</ta>
            <ta e="T729" id="Seg_10624" s="T728">v:pred</ta>
            <ta e="T732" id="Seg_10625" s="T731">np:O</ta>
            <ta e="T733" id="Seg_10626" s="T732">v:pred 0.1.h:S</ta>
            <ta e="T735" id="Seg_10627" s="T734">pro.h:S</ta>
            <ta e="T736" id="Seg_10628" s="T735">v:pred</ta>
            <ta e="T738" id="Seg_10629" s="T737">np:O</ta>
            <ta e="T740" id="Seg_10630" s="T739">v:pred 0.1.h:S</ta>
            <ta e="T743" id="Seg_10631" s="T742">np:O</ta>
            <ta e="T744" id="Seg_10632" s="T743">v:pred 0.3.h:S</ta>
            <ta e="T753" id="Seg_10633" s="T752">np:O</ta>
            <ta e="T755" id="Seg_10634" s="T918">v:pred 0.3.h:S</ta>
            <ta e="T760" id="Seg_10635" s="T759">ptcl.neg</ta>
            <ta e="T761" id="Seg_10636" s="T760">v:pred 0.3.h:S</ta>
            <ta e="T762" id="Seg_10637" s="T761">pro:O</ta>
            <ta e="T764" id="Seg_10638" s="T763">ptcl:pred</ta>
            <ta e="T765" id="Seg_10639" s="T764">np:O</ta>
            <ta e="T767" id="Seg_10640" s="T766">ptcl:pred</ta>
            <ta e="T772" id="Seg_10641" s="T771">np:S</ta>
            <ta e="T774" id="Seg_10642" s="T773">v:pred</ta>
            <ta e="T782" id="Seg_10643" s="T781">np:S</ta>
            <ta e="T783" id="Seg_10644" s="T782">v:pred</ta>
            <ta e="T784" id="Seg_10645" s="T783">v:pred 0.3:S</ta>
            <ta e="T787" id="Seg_10646" s="T786">np:S</ta>
            <ta e="T788" id="Seg_10647" s="T787">v:pred</ta>
            <ta e="T792" id="Seg_10648" s="T791">np:S</ta>
            <ta e="T796" id="Seg_10649" s="T795">np:S</ta>
            <ta e="T797" id="Seg_10650" s="T796">v:pred</ta>
            <ta e="T800" id="Seg_10651" s="T799">v:pred 0.2.h:S</ta>
            <ta e="T802" id="Seg_10652" s="T801">np.h:S</ta>
            <ta e="T805" id="Seg_10653" s="T804">np:O</ta>
            <ta e="T806" id="Seg_10654" s="T805">v:pred</ta>
            <ta e="T807" id="Seg_10655" s="T806">pro:S</ta>
            <ta e="T809" id="Seg_10656" s="T808">v:pred</ta>
            <ta e="T811" id="Seg_10657" s="T810">v:pred 0.3.h:S</ta>
            <ta e="T814" id="Seg_10658" s="T813">conv:pred</ta>
            <ta e="T815" id="Seg_10659" s="T814">v:pred 0.3:S</ta>
            <ta e="T817" id="Seg_10660" s="T816">n:pred</ta>
            <ta e="T818" id="Seg_10661" s="T817">cop 0.3:S</ta>
            <ta e="T819" id="Seg_10662" s="T818">np:S</ta>
            <ta e="T821" id="Seg_10663" s="T820">v:pred</ta>
            <ta e="T822" id="Seg_10664" s="T821">n:pred</ta>
            <ta e="T823" id="Seg_10665" s="T822">cop 0.3:S</ta>
            <ta e="T824" id="Seg_10666" s="T823">np:S</ta>
            <ta e="T825" id="Seg_10667" s="T824">v:pred</ta>
            <ta e="T828" id="Seg_10668" s="T827">ptcl:pred</ta>
            <ta e="T833" id="Seg_10669" s="T832">ptcl:pred</ta>
            <ta e="T840" id="Seg_10670" s="T839">v:pred 0.1.h:S</ta>
            <ta e="T841" id="Seg_10671" s="T840">np:O</ta>
            <ta e="T846" id="Seg_10672" s="T845">np:S</ta>
            <ta e="T847" id="Seg_10673" s="T846">v:pred</ta>
            <ta e="T848" id="Seg_10674" s="T847">ptcl.neg</ta>
            <ta e="T849" id="Seg_10675" s="T848">v:pred 0.3:S</ta>
            <ta e="T850" id="Seg_10676" s="T849">pro:O</ta>
            <ta e="T851" id="Seg_10677" s="T850">ptcl.neg</ta>
            <ta e="T852" id="Seg_10678" s="T851">v:pred 0.1.h:S</ta>
            <ta e="T854" id="Seg_10679" s="T853">np.h:S</ta>
            <ta e="T859" id="Seg_10680" s="T858">v:pred</ta>
            <ta e="T860" id="Seg_10681" s="T859">ptcl:pred</ta>
            <ta e="T868" id="Seg_10682" s="T867">v:pred 0.2.h:S</ta>
            <ta e="T869" id="Seg_10683" s="T868">pro.h:S</ta>
            <ta e="T871" id="Seg_10684" s="T870">v:pred</ta>
            <ta e="T873" id="Seg_10685" s="T872">pro.h:S</ta>
            <ta e="T874" id="Seg_10686" s="T873">v:pred</ta>
            <ta e="T876" id="Seg_10687" s="T875">conv:pred</ta>
            <ta e="T877" id="Seg_10688" s="T876">v:pred</ta>
            <ta e="T878" id="Seg_10689" s="T877">pro.h:S</ta>
            <ta e="T917" id="Seg_10690" s="T879">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST-PKZ" />
         <annotation name="BOR" tierref="BOR-PKZ">
            <ta e="T11" id="Seg_10691" s="T10">TURK:disc</ta>
            <ta e="T13" id="Seg_10692" s="T12">RUS:mod</ta>
            <ta e="T36" id="Seg_10693" s="T35">RUS:gram</ta>
            <ta e="T37" id="Seg_10694" s="T36">RUS:mod</ta>
            <ta e="T44" id="Seg_10695" s="T43">TURK:gram(INDEF)</ta>
            <ta e="T47" id="Seg_10696" s="T46">TURK:gram(INDEF)</ta>
            <ta e="T50" id="Seg_10697" s="T49">RUS:gram</ta>
            <ta e="T51" id="Seg_10698" s="T50">TURK:core</ta>
            <ta e="T54" id="Seg_10699" s="T53">TURK:core</ta>
            <ta e="T55" id="Seg_10700" s="T54">TURK:disc</ta>
            <ta e="T73" id="Seg_10701" s="T72">TURK:disc</ta>
            <ta e="T78" id="Seg_10702" s="T77">TURK:core</ta>
            <ta e="T84" id="Seg_10703" s="T83">TURK:cult</ta>
            <ta e="T89" id="Seg_10704" s="T88">TURK:disc</ta>
            <ta e="T101" id="Seg_10705" s="T100">RUS:gram</ta>
            <ta e="T103" id="Seg_10706" s="T102">TURK:disc</ta>
            <ta e="T106" id="Seg_10707" s="T105">TURK:disc</ta>
            <ta e="T116" id="Seg_10708" s="T115">RUS:gram(INDEF)</ta>
            <ta e="T118" id="Seg_10709" s="T117">RUS:core</ta>
            <ta e="T125" id="Seg_10710" s="T124">TURK:core</ta>
            <ta e="T128" id="Seg_10711" s="T127">RUS:core</ta>
            <ta e="T134" id="Seg_10712" s="T133">RUS:cult</ta>
            <ta e="T135" id="Seg_10713" s="T134">TURK:disc</ta>
            <ta e="T139" id="Seg_10714" s="T138">TAT:cult </ta>
            <ta e="T141" id="Seg_10715" s="T140">TURK:disc</ta>
            <ta e="T142" id="Seg_10716" s="T141">RUS:gram</ta>
            <ta e="T149" id="Seg_10717" s="T148">TURK:disc</ta>
            <ta e="T150" id="Seg_10718" s="T149">RUS:mod</ta>
            <ta e="T154" id="Seg_10719" s="T153">TURK:disc</ta>
            <ta e="T157" id="Seg_10720" s="T156">TURK:disc</ta>
            <ta e="T169" id="Seg_10721" s="T168">TURK:core</ta>
            <ta e="T171" id="Seg_10722" s="T170">TURK:disc</ta>
            <ta e="T178" id="Seg_10723" s="T177">TURK:core</ta>
            <ta e="T179" id="Seg_10724" s="T178">TURK:disc</ta>
            <ta e="T183" id="Seg_10725" s="T181">RUS:gram</ta>
            <ta e="T184" id="Seg_10726" s="T183">TURK:disc</ta>
            <ta e="T187" id="Seg_10727" s="T186">TURK:disc</ta>
            <ta e="T191" id="Seg_10728" s="T190">TURK:disc</ta>
            <ta e="T195" id="Seg_10729" s="T193">RUS:gram</ta>
            <ta e="T196" id="Seg_10730" s="T195">TURK:disc</ta>
            <ta e="T198" id="Seg_10731" s="T197">TURK:disc</ta>
            <ta e="T204" id="Seg_10732" s="T203">RUS:cult</ta>
            <ta e="T208" id="Seg_10733" s="T207">RUS:cult</ta>
            <ta e="T215" id="Seg_10734" s="T214">RUS:cult</ta>
            <ta e="T219" id="Seg_10735" s="T218">RUS:gram</ta>
            <ta e="T231" id="Seg_10736" s="T230">RUS:gram</ta>
            <ta e="T232" id="Seg_10737" s="T231">TURK:core</ta>
            <ta e="T243" id="Seg_10738" s="T241">TURK:disc</ta>
            <ta e="T251" id="Seg_10739" s="T249">TURK:disc</ta>
            <ta e="T262" id="Seg_10740" s="T261">TURK:disc</ta>
            <ta e="T268" id="Seg_10741" s="T267">TURK:disc</ta>
            <ta e="T271" id="Seg_10742" s="T270">TURK:core</ta>
            <ta e="T275" id="Seg_10743" s="T274">TURK:disc</ta>
            <ta e="T277" id="Seg_10744" s="T276">RUS:gram</ta>
            <ta e="T282" id="Seg_10745" s="T281">TURK:gram(INDEF)</ta>
            <ta e="T289" id="Seg_10746" s="T288">TURK:disc</ta>
            <ta e="T295" id="Seg_10747" s="T294">TURK:disc</ta>
            <ta e="T298" id="Seg_10748" s="T297">RUS:mod</ta>
            <ta e="T301" id="Seg_10749" s="T300">RUS:mod</ta>
            <ta e="T302" id="Seg_10750" s="T301">RUS:cult</ta>
            <ta e="T312" id="Seg_10751" s="T311">RUS:mod</ta>
            <ta e="T338" id="Seg_10752" s="T337">TURK:gram(INDEF)</ta>
            <ta e="T339" id="Seg_10753" s="T338">TURK:disc</ta>
            <ta e="T351" id="Seg_10754" s="T350">TURK:disc</ta>
            <ta e="T358" id="Seg_10755" s="T357">RUS:mod</ta>
            <ta e="T362" id="Seg_10756" s="T361">RUS:mod</ta>
            <ta e="T373" id="Seg_10757" s="T372">RUS:mod</ta>
            <ta e="T380" id="Seg_10758" s="T379">RUS:mod</ta>
            <ta e="T381" id="Seg_10759" s="T380">RUS:gram(INDEF)</ta>
            <ta e="T388" id="Seg_10760" s="T387">TURK:disc</ta>
            <ta e="T391" id="Seg_10761" s="T390">%TURK:core</ta>
            <ta e="T398" id="Seg_10762" s="T397">TURK:disc</ta>
            <ta e="T413" id="Seg_10763" s="T412">RUS:gram(INDEF)</ta>
            <ta e="T418" id="Seg_10764" s="T417">%TURK:core</ta>
            <ta e="T420" id="Seg_10765" s="T419">%TURK:core</ta>
            <ta e="T421" id="Seg_10766" s="T420">RUS:gram</ta>
            <ta e="T422" id="Seg_10767" s="T421">TURK:disc</ta>
            <ta e="T428" id="Seg_10768" s="T427">RUS:gram</ta>
            <ta e="T433" id="Seg_10769" s="T432">RUS:gram</ta>
            <ta e="T435" id="Seg_10770" s="T434">TURK:disc</ta>
            <ta e="T443" id="Seg_10771" s="T442">TURK:cult</ta>
            <ta e="T446" id="Seg_10772" s="T445">RUS:cult</ta>
            <ta e="T451" id="Seg_10773" s="T450">TURK:disc</ta>
            <ta e="T472" id="Seg_10774" s="T471">TURK:cult</ta>
            <ta e="T476" id="Seg_10775" s="T475">RUS:cult</ta>
            <ta e="T491" id="Seg_10776" s="T490">TURK:disc</ta>
            <ta e="T508" id="Seg_10777" s="T507">TURK:core</ta>
            <ta e="T509" id="Seg_10778" s="T508">TURK:disc</ta>
            <ta e="T519" id="Seg_10779" s="T518">TURK:disc</ta>
            <ta e="T522" id="Seg_10780" s="T521">RUS:gram</ta>
            <ta e="T523" id="Seg_10781" s="T522">TURK:disc</ta>
            <ta e="T526" id="Seg_10782" s="T525">TURK:disc</ta>
            <ta e="T530" id="Seg_10783" s="T529">TURK:disc</ta>
            <ta e="T540" id="Seg_10784" s="T539">TURK:gram(INDEF)</ta>
            <ta e="T551" id="Seg_10785" s="T550">RUS:cult</ta>
            <ta e="T557" id="Seg_10786" s="T556">RUS:gram</ta>
            <ta e="T561" id="Seg_10787" s="T560">RUS:core</ta>
            <ta e="T562" id="Seg_10788" s="T561">TURK:disc</ta>
            <ta e="T564" id="Seg_10789" s="T563">RUS:gram</ta>
            <ta e="T571" id="Seg_10790" s="T570">TURK:disc</ta>
            <ta e="T572" id="Seg_10791" s="T571">RUS:core</ta>
            <ta e="T573" id="Seg_10792" s="T572">RUS:gram</ta>
            <ta e="T579" id="Seg_10793" s="T578">RUS:gram</ta>
            <ta e="T594" id="Seg_10794" s="T593">TURK:disc</ta>
            <ta e="T597" id="Seg_10795" s="T596">TURK:disc</ta>
            <ta e="T604" id="Seg_10796" s="T603">TURK:disc</ta>
            <ta e="T605" id="Seg_10797" s="T604">TAT:cult</ta>
            <ta e="T613" id="Seg_10798" s="T612">RUS:gram</ta>
            <ta e="T615" id="Seg_10799" s="T614">TURK:disc</ta>
            <ta e="T620" id="Seg_10800" s="T619">TAT:cult</ta>
            <ta e="T629" id="Seg_10801" s="T628">%TURK:core</ta>
            <ta e="T631" id="Seg_10802" s="T630">TURK:disc</ta>
            <ta e="T635" id="Seg_10803" s="T634">RUS:cult</ta>
            <ta e="T642" id="Seg_10804" s="T641">RUS:mod</ta>
            <ta e="T646" id="Seg_10805" s="T645">TURK:cult </ta>
            <ta e="T649" id="Seg_10806" s="T648">TURK:disc</ta>
            <ta e="T659" id="Seg_10807" s="T658">%TURK:core</ta>
            <ta e="T665" id="Seg_10808" s="T664">RUS:mod</ta>
            <ta e="T666" id="Seg_10809" s="T665">%TURK:core</ta>
            <ta e="T670" id="Seg_10810" s="T668">RUS:mod</ta>
            <ta e="T673" id="Seg_10811" s="T672">RUS:gram(INDEF)</ta>
            <ta e="T674" id="Seg_10812" s="T673">%TURK:core</ta>
            <ta e="T698" id="Seg_10813" s="T697">TURK:gram(INDEF)</ta>
            <ta e="T704" id="Seg_10814" s="T703">TURK:disc</ta>
            <ta e="T708" id="Seg_10815" s="T707">TURK:disc</ta>
            <ta e="T716" id="Seg_10816" s="T715">RUS:gram</ta>
            <ta e="T723" id="Seg_10817" s="T722">RUS:gram</ta>
            <ta e="T726" id="Seg_10818" s="T725">TURK:disc</ta>
            <ta e="T737" id="Seg_10819" s="T736">RUS:gram</ta>
            <ta e="T745" id="Seg_10820" s="T744">TURK:disc</ta>
            <ta e="T918" id="Seg_10821" s="T753">TURK:disc</ta>
            <ta e="T759" id="Seg_10822" s="T758">RUS:mod</ta>
            <ta e="T762" id="Seg_10823" s="T761">TURK:gram(INDEF)</ta>
            <ta e="T764" id="Seg_10824" s="T763">RUS:mod</ta>
            <ta e="T767" id="Seg_10825" s="T766">RUS:mod</ta>
            <ta e="T775" id="Seg_10826" s="T774">TURK:disc</ta>
            <ta e="T793" id="Seg_10827" s="T792">RUS:gram</ta>
            <ta e="T799" id="Seg_10828" s="T798">TURK:disc</ta>
            <ta e="T804" id="Seg_10829" s="T803">TURK:disc</ta>
            <ta e="T808" id="Seg_10830" s="T807">TURK:disc</ta>
            <ta e="T810" id="Seg_10831" s="T809">RUS:gram</ta>
            <ta e="T816" id="Seg_10832" s="T815">TURK:disc</ta>
            <ta e="T820" id="Seg_10833" s="T819">TURK:disc</ta>
            <ta e="T827" id="Seg_10834" s="T826">%TURK:core</ta>
            <ta e="T828" id="Seg_10835" s="T827">RUS:mod</ta>
            <ta e="T833" id="Seg_10836" s="T832">RUS:mod</ta>
            <ta e="T841" id="Seg_10837" s="T840">RUS:core</ta>
            <ta e="T845" id="Seg_10838" s="T844">TURK:disc</ta>
            <ta e="T850" id="Seg_10839" s="T849">TURK:gram(INDEF)</ta>
            <ta e="T854" id="Seg_10840" s="T853">TURK:core</ta>
            <ta e="T860" id="Seg_10841" s="T859">RUS:mod</ta>
            <ta e="T864" id="Seg_10842" s="T863">RUS:gram</ta>
            <ta e="T865" id="Seg_10843" s="T864">RUS:mod</ta>
            <ta e="T872" id="Seg_10844" s="T871">RUS:gram</ta>
            <ta e="T875" id="Seg_10845" s="T874">TURK:gram(INDEF)</ta>
            <ta e="T879" id="Seg_10846" s="T878">TURK:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-PKZ" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-PKZ" />
         <annotation name="CS" tierref="CS-PKZ">
            <ta e="T489" id="Seg_10847" s="T486">RUS:ext</ta>
            <ta e="T730" id="Seg_10848" s="T729">RUS:int</ta>
            <ta e="T839" id="Seg_10849" s="T834">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-PKZ">
            <ta e="T12" id="Seg_10850" s="T4">Я не могу шевелиться ведь, хвораю. </ta>
            <ta e="T15" id="Seg_10851" s="T12">Однако я умру.</ta>
            <ta e="T20" id="Seg_10852" s="T15">Сегодня у этой девушки большой день.</ta>
            <ta e="T28" id="Seg_10853" s="T20">Её сегодня мать родила.</ta>
            <ta e="T38" id="Seg_10854" s="T28">Ей двадцать два года.</ta>
            <ta e="T43" id="Seg_10855" s="T39">Очень этот человек скупой.</ta>
            <ta e="T46" id="Seg_10856" s="T43">Ничего не подарит.</ta>
            <ta e="T49" id="Seg_10857" s="T46">Ничего не даст.</ta>
            <ta e="T54" id="Seg_10858" s="T49">А другой человек очень хороший.</ta>
            <ta e="T59" id="Seg_10859" s="T54">Всё отдаст.</ta>
            <ta e="T69" id="Seg_10860" s="T61">Всё.</ta>
            <ta e="T74" id="Seg_10861" s="T69">Что ни попросишь, всё отдаст.</ta>
            <ta e="T78" id="Seg_10862" s="T74">У него очень сердце хорошее.</ta>
            <ta e="T83" id="Seg_10863" s="T78">Как это сразу ты… </ta>
            <ta e="T86" id="Seg_10864" s="T83">Водку не пьёт.</ta>
            <ta e="T90" id="Seg_10865" s="T86">Что есть, всё даст.</ta>
            <ta e="T94" id="Seg_10866" s="T90">Яйца даёт.</ta>
            <ta e="T98" id="Seg_10867" s="T94">Масло даёт, хлеб даёт.</ta>
            <ta e="T102" id="Seg_10868" s="T98">Мясо есть, то даёт.</ta>
            <ta e="T107" id="Seg_10869" s="T102">Всё, что есть, всё даст.</ta>
            <ta e="T112" id="Seg_10870" s="T107">Наверху очень гремит.</ta>
            <ta e="T114" id="Seg_10871" s="T112">Бог гремит.</ta>
            <ta e="T117" id="Seg_10872" s="T114">Кого-нибудь убъет.</ta>
            <ta e="T119" id="Seg_10873" s="T117">Радуга стоит.</ta>
            <ta e="T121" id="Seg_10874" s="T119">Воды берёт.</ta>
            <ta e="T124" id="Seg_10875" s="T121">Тогда дождь пойдет.</ta>
            <ta e="T126" id="Seg_10876" s="T124">Град пойдет.</ta>
            <ta e="T130" id="Seg_10877" s="T126">Очень черная туча идет.</ta>
            <ta e="T133" id="Seg_10878" s="T130">Я на улицу пошла.</ta>
            <ta e="T136" id="Seg_10879" s="T133">Кукушка ведь летит.</ta>
            <ta e="T143" id="Seg_10880" s="T136">Гляжу, она на избу села ведь и кричит.</ta>
            <ta e="T149" id="Seg_10881" s="T143">Я говорю: Одна буду жить ведь.</ta>
            <ta e="T152" id="Seg_10882" s="T149">Верно одна буду жить.</ta>
            <ta e="T155" id="Seg_10883" s="T152">Камнем ведь кидала.</ta>
            <ta e="T158" id="Seg_10884" s="T155">Палкой ведь кидала.</ta>
            <ta e="T161" id="Seg_10885" s="T158">Она всё сидит.</ta>
            <ta e="T162" id="Seg_10886" s="T161">Кричит.</ta>
            <ta e="T167" id="Seg_10887" s="T162">Потом улетела, где солнце всходит.</ta>
            <ta e="T173" id="Seg_10888" s="T167">Очень мудрые люди ведь написали на бумаге.</ta>
            <ta e="T178" id="Seg_10889" s="T173">Этот человек очень нехороший.</ta>
            <ta e="T183" id="Seg_10890" s="T178">Он всё берет и…</ta>
            <ta e="T187" id="Seg_10891" s="T183">Он всё берет… </ta>
            <ta e="T190" id="Seg_10892" s="T187">Очень пакостный человек.</ta>
            <ta e="T195" id="Seg_10893" s="T190">Он всё берет… </ta>
            <ta e="T200" id="Seg_10894" s="T195">Он всё берет ведь, несет домой.</ta>
            <ta e="T205" id="Seg_10895" s="T200">Я сегодня девке платок купила.</ta>
            <ta e="T209" id="Seg_10896" s="T205">Она пошла в Малиновку учиться.</ta>
            <ta e="T216" id="Seg_10897" s="T209">У нее старуха спрашивала: "Где ты платок взяла?"</ta>
            <ta e="T218" id="Seg_10898" s="T216">"Бабушка дала."</ta>
            <ta e="T222" id="Seg_10899" s="T218">"А бабушка где взяла?"</ta>
            <ta e="T224" id="Seg_10900" s="T222">"Поля дала."</ta>
            <ta e="T230" id="Seg_10901" s="T224">Этого коня взяла, много денег отдала.</ta>
            <ta e="T234" id="Seg_10902" s="T230">А второго коня взяла,… </ta>
            <ta e="T236" id="Seg_10903" s="T234">Мало отдала.</ta>
            <ta e="T237" id="Seg_10904" s="T236">Много отдала.</ta>
            <ta e="T241" id="Seg_10905" s="T237">Поймала теленка.</ta>
            <ta e="T243" id="Seg_10906" s="T241">(?)…</ta>
            <ta e="T248" id="Seg_10907" s="T246">Не идет.</ta>
            <ta e="T251" id="Seg_10908" s="T248">Я ведь…</ta>
            <ta e="T258" id="Seg_10909" s="T256">Очень сильная.</ta>
            <ta e="T262" id="Seg_10910" s="T258">Не могу утащить ведь.</ta>
            <ta e="T268" id="Seg_10911" s="T262">Иду, всё.</ta>
            <ta e="T273" id="Seg_10912" s="T269">Нехороший парень.</ta>
            <ta e="T278" id="Seg_10913" s="T273">Всё ведь ворует да несёт.</ta>
            <ta e="T281" id="Seg_10914" s="T278">Я в тайгу ездила.</ta>
            <ta e="T285" id="Seg_10915" s="T281">Что-то я видела.</ta>
            <ta e="T289" id="Seg_10916" s="T285">Потом я испугалась.</ta>
            <ta e="T294" id="Seg_10917" s="T289">Тогда бога призывала .</ta>
            <ta e="T297" id="Seg_10918" s="T294">Все уехали.</ta>
            <ta e="T300" id="Seg_10919" s="T297">Надо дыры вертеть.</ta>
            <ta e="T303" id="Seg_10920" s="T300">Нало ворота делать.</ta>
            <ta e="T314" id="Seg_10921" s="T304">В эти дыры надо палки забить.</ta>
            <ta e="T317" id="Seg_10922" s="T314">Тогда кораль закроем.</ta>
            <ta e="T321" id="Seg_10923" s="T317">Тогда теленок не уйдет.</ta>
            <ta e="T326" id="Seg_10924" s="T321">Во дворе стоит.</ta>
            <ta e="T331" id="Seg_10925" s="T327">Ну там скажи чего. </ta>
            <ta e="T340" id="Seg_10926" s="T335">Чем-то пахнет.</ta>
            <ta e="T344" id="Seg_10927" s="T340">Твой нос очень …</ta>
            <ta e="T352" id="Seg_10928" s="T346">Твой нос очень острый, всё слышит.</ta>
            <ta e="T355" id="Seg_10929" s="T352">Ты где был?</ta>
            <ta e="T357" id="Seg_10930" s="T355">Воду брать ходил.</ta>
            <ta e="T361" id="Seg_10931" s="T357">Надо чай кипятить.</ta>
            <ta e="T366" id="Seg_10932" s="T361">Надо суп варить.</ta>
            <ta e="T368" id="Seg_10933" s="T366">Мужчин кормить.</ta>
            <ta e="T372" id="Seg_10934" s="T368">У меня десять рублей есть.</ta>
            <ta e="T376" id="Seg_10935" s="T372">Надо тебе пять дать.</ta>
            <ta e="T379" id="Seg_10936" s="T376">Пять себе оставить.</ta>
            <ta e="T385" id="Seg_10937" s="T379">Может куда-нибудь поедешь, деньги надо будет.</ta>
            <ta e="T389" id="Seg_10938" s="T386">Матвеев ведь сидит.</ta>
            <ta e="T391" id="Seg_10939" s="T389">Не разговаривает.</ta>
            <ta e="T393" id="Seg_10940" s="T391">Сердце болит.</ta>
            <ta e="T396" id="Seg_10941" s="T393">Нет бумаг дома.</ta>
            <ta e="T399" id="Seg_10942" s="T396">Жена ведь рассердилась.</ta>
            <ta e="T404" id="Seg_10943" s="T399">Она не пишет письма.</ta>
            <ta e="T406" id="Seg_10944" s="T404">Не сердись!</ta>
            <ta e="T412" id="Seg_10945" s="T409">Скоро домой приеду. </ta>
            <ta e="T415" id="Seg_10946" s="T412">Тебе что-нибудь привезу.</ta>
            <ta e="T418" id="Seg_10947" s="T415">Сильно не разговаривай!</ta>
            <ta e="T420" id="Seg_10948" s="T418">Тихонько разговаривай</ta>
            <ta e="T424" id="Seg_10949" s="T420">A то ведь люди слышат!</ta>
            <ta e="T427" id="Seg_10950" s="T424">Сильно не кричи!</ta>
            <ta e="T440" id="Seg_10951" s="T427">A то люди ведь слышат, как ты ругаешься!</ta>
            <ta e="T444" id="Seg_10952" s="T440">Я сегодня сметану вылила.</ta>
            <ta e="T445" id="Seg_10953" s="T444">Mешала.</ta>
            <ta e="T447" id="Seg_10954" s="T445">Kринки вымыла.</ta>
            <ta e="T451" id="Seg_10955" s="T447">Я ждала, солнце сушит ведь.</ta>
            <ta e="T453" id="Seg_10956" s="T451">Что принес?</ta>
            <ta e="T455" id="Seg_10957" s="T453">Покажи мне!</ta>
            <ta e="T460" id="Seg_10958" s="T455">Что там лежит?</ta>
            <ta e="T464" id="Seg_10959" s="T460">Я сегодня черемши взять ходила.</ta>
            <ta e="T466" id="Seg_10960" s="T464">Черемши принесла.</ta>
            <ta e="T468" id="Seg_10961" s="T466">Потом нарежу.</ta>
            <ta e="T471" id="Seg_10962" s="T468">Яичко положу.</ta>
            <ta e="T473" id="Seg_10963" s="T471">Сметану положу.</ta>
            <ta e="T477" id="Seg_10964" s="T473">Тогда печь пироги буду.</ta>
            <ta e="T480" id="Seg_10965" s="T478">Мясо сварила.</ta>
            <ta e="T483" id="Seg_10966" s="T480">Мясо ели зубами взять.</ta>
            <ta e="T486" id="Seg_10967" s="T483">Одни кости остались.</ta>
            <ta e="T489" id="Seg_10968" s="T486">Одни кости остались. </ta>
            <ta e="T493" id="Seg_10969" s="T489">Собаки всё кости едят.</ta>
            <ta e="T497" id="Seg_10970" s="T493">Очень много костей побросали.</ta>
            <ta e="T506" id="Seg_10971" s="T497">Я сегодня хлеб пекла, кислого нету.</ta>
            <ta e="T509" id="Seg_10972" s="T506">Нехороший ведь.</ta>
            <ta e="T517" id="Seg_10973" s="T509">Я этот хлеб собакам отдала.</ta>
            <ta e="T524" id="Seg_10974" s="T517">Собаки ведь хлеб едят и всё дерутся.</ta>
            <ta e="T530" id="Seg_10975" s="T524">Собака ведь эту собаку знает ведь.</ta>
            <ta e="T532" id="Seg_10976" s="T530">Ночь была.</ta>
            <ta e="T536" id="Seg_10977" s="T532">Ночь была.</ta>
            <ta e="T542" id="Seg_10978" s="T539">Ничего не видно.</ta>
            <ta e="T546" id="Seg_10979" s="T542">Я его не узнала.</ta>
            <ta e="T548" id="Seg_10980" s="T546">Кто идет?</ta>
            <ta e="T552" id="Seg_10981" s="T548">Мои отец туесы делал.</ta>
            <ta e="T558" id="Seg_10982" s="T552">Кору сдирал и делал.</ta>
            <ta e="T560" id="Seg_10983" s="T558">Чуманы шили.</ta>
            <ta e="T562" id="Seg_10984" s="T560">Жилами ведь.</ta>
            <ta e="T567" id="Seg_10985" s="T562">Иголкой, и напёрсток на пальце был.</ta>
            <ta e="T570" id="Seg_10986" s="T567">Чуманы шили.</ta>
            <ta e="T572" id="Seg_10987" s="T570">Ведь жилами.</ta>
            <ta e="T578" id="Seg_10988" s="T572">И напёрсток надел на палец.</ta>
            <ta e="T581" id="Seg_10989" s="T578">И иголку взял.</ta>
            <ta e="T582" id="Seg_10990" s="T581">Шил.</ta>
            <ta e="T585" id="Seg_10991" s="T582">Всё, всё… </ta>
            <ta e="T595" id="Seg_10992" s="T591">Палец ведь проткнет.</ta>
            <ta e="T599" id="Seg_10993" s="T595">Ты ведь жену жалеешь.</ta>
            <ta e="T602" id="Seg_10994" s="T599">Ее сердце болит очень.</ta>
            <ta e="T610" id="Seg_10995" s="T602">Моя ведь изба черная, завтра буду мыть.</ta>
            <ta e="T612" id="Seg_10996" s="T610">Очень грязная.</ta>
            <ta e="T616" id="Seg_10997" s="T612">А то люди ведь говорят… </ta>
            <ta e="T619" id="Seg_10998" s="T616">Скажут: ленивая старуха. </ta>
            <ta e="T622" id="Seg_10999" s="T619">Избу не моет!</ta>
            <ta e="T629" id="Seg_11000" s="T626">Что тебе говорить?</ta>
            <ta e="T632" id="Seg_11001" s="T629">Я ведь забыла.</ta>
            <ta e="T637" id="Seg_11002" s="T632">Сегодня слать клопы не дали.</ta>
            <ta e="T644" id="Seg_11003" s="T637">Этот человек муки принес, надо ему дать, хлеба. </ta>
            <ta e="T648" id="Seg_11004" s="T644">Хлеба нету [чтобы] есть.</ta>
            <ta e="T649" id="Seg_11005" s="T648">Всё!</ta>
            <ta e="T659" id="Seg_11006" s="T649">Где взять что разговаривать?</ta>
            <ta e="T666" id="Seg_11007" s="T659">Ему очень много надо говорить.</ta>
            <ta e="T672" id="Seg_11008" s="T668">Надо сходить, посмотреть.</ta>
            <ta e="T674" id="Seg_11009" s="T672">Где-нибудь говорить.</ta>
            <ta e="T682" id="Seg_11010" s="T674">Я вчера долго не ложилась спать.</ta>
            <ta e="T692" id="Seg_11011" s="T684">Долго не ложусь спать.</ta>
            <ta e="T700" id="Seg_11012" s="T696">Спала, ничего не видела.</ta>
            <ta e="T706" id="Seg_11013" s="T700">У этого человека волосы ведь чёрные были.</ta>
            <ta e="T712" id="Seg_11014" s="T706">Теперь ведь очень белые стали волосы</ta>
            <ta e="T719" id="Seg_11015" s="T712">Одна рука есть, а одну руку оторвали.</ta>
            <ta e="T727" id="Seg_11016" s="T719">Одна рука есть, а одну руку ведь совсем оторвали.</ta>
            <ta e="T733" id="Seg_11017" s="T727">Я упала и одну руку сломала.</ta>
            <ta e="T740" id="Seg_11018" s="T733">Я упала и руку сломала.</ta>
            <ta e="T745" id="Seg_11019" s="T740">Тогда эту руку положили. [?]</ta>
            <ta e="T755" id="Seg_11020" s="T901">Тогда руку ведь связали.</ta>
            <ta e="T761" id="Seg_11021" s="T758">Пока не срастётся.</ta>
            <ta e="T764" id="Seg_11022" s="T761">Ничего работать нельзя.</ta>
            <ta e="T767" id="Seg_11023" s="T764">Коня запрягать надо.</ta>
            <ta e="T775" id="Seg_11024" s="T767">Конь побежал.</ta>
            <ta e="T788" id="Seg_11025" s="T779">Конь побежал, упал и ногу сломал.</ta>
            <ta e="T800" id="Seg_11026" s="T788">У меня на руке пять пальцев, а у тебя на руке одного нет, топором ты отрубил.</ta>
            <ta e="T806" id="Seg_11027" s="T800">Сегодня ребятишек много ведь теленка гнали.</ta>
            <ta e="T812" id="Seg_11028" s="T806">Он ведь назад побежал и убежал домой.</ta>
            <ta e="T816" id="Seg_11029" s="T812">Вечером он ушёл.</ta>
            <ta e="T818" id="Seg_11030" s="T816">Ночь настает.</ta>
            <ta e="T821" id="Seg_11031" s="T818">Солнце ведь садится.</ta>
            <ta e="T823" id="Seg_11032" s="T821">Ночь настает.</ta>
            <ta e="T825" id="Seg_11033" s="T823">Солнце ведь садится.</ta>
            <ta e="T830" id="Seg_11034" s="T825">Хватит разговаривать, надо ложиться спать.</ta>
            <ta e="T834" id="Seg_11035" s="T830">Завтра рано надо вставать.</ta>
            <ta e="T839" id="Seg_11036" s="T834">Утром рано надо вставать, говорю. </ta>
            <ta e="T842" id="Seg_11037" s="T839">Пойду цветки рвать.</ta>
            <ta e="T847" id="Seg_11038" s="T842">На березе ведь бумага висит.</ta>
            <ta e="T849" id="Seg_11039" s="T847">Не видно.</ta>
            <ta e="T852" id="Seg_11040" s="T849">Ничего не вижу.</ta>
            <ta e="T856" id="Seg_11041" s="T852">Моя родня очень …</ta>
            <ta e="T859" id="Seg_11042" s="T857">… далеко живёт.</ta>
            <ta e="T863" id="Seg_11043" s="T859">Надо два дня идти.</ta>
            <ta e="T868" id="Seg_11044" s="T863">А может в один день дойдёшь.</ta>
            <ta e="T874" id="Seg_11045" s="T868">Я к нему пришла, а его нет.</ta>
            <ta e="T917" id="Seg_11046" s="T874">Он куда-то ушёл, я вернулась.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-PKZ">
            <ta e="T12" id="Seg_11047" s="T4">I cannot move, everything hurts.</ta>
            <ta e="T15" id="Seg_11048" s="T12">I am dying.</ta>
            <ta e="T20" id="Seg_11049" s="T15">Today is this girl's big day (=birthday).</ta>
            <ta e="T28" id="Seg_11050" s="T20">Her mother gave birth to her today.</ta>
            <ta e="T38" id="Seg_11051" s="T28">She is twelve years and two more.</ta>
            <ta e="T43" id="Seg_11052" s="T39">How is this man stingy.</ta>
            <ta e="T46" id="Seg_11053" s="T43">He won't take anything.</ta>
            <ta e="T49" id="Seg_11054" s="T46">He will not give anything!</ta>
            <ta e="T54" id="Seg_11055" s="T49">And the other man is very good.</ta>
            <ta e="T59" id="Seg_11056" s="T54">He will give everything, what [ever he has?].</ta>
            <ta e="T69" id="Seg_11057" s="T61">Whatever… </ta>
            <ta e="T74" id="Seg_11058" s="T69">Whatever you ask, he will give everything.</ta>
            <ta e="T78" id="Seg_11059" s="T74">He has a very good heart.</ta>
            <ta e="T83" id="Seg_11060" s="T78">How do you at once…</ta>
            <ta e="T86" id="Seg_11061" s="T83">He does not drink vodka.</ta>
            <ta e="T90" id="Seg_11062" s="T86">What he has, he will give everything.</ta>
            <ta e="T94" id="Seg_11063" s="T90">He gives eggs, (meat-).</ta>
            <ta e="T98" id="Seg_11064" s="T94">He gives butter, he gives bread.</ta>
            <ta e="T102" id="Seg_11065" s="T98">If he has meat, he will give it.</ta>
            <ta e="T107" id="Seg_11066" s="T102">Whatever he has, he will give everything.</ta>
            <ta e="T112" id="Seg_11067" s="T107">The thunder is rolling above!</ta>
            <ta e="T114" id="Seg_11068" s="T112">God is rumbling.</ta>
            <ta e="T117" id="Seg_11069" s="T114">He will kill someone. [?]</ta>
            <ta e="T119" id="Seg_11070" s="T117">A rainbow is standing.</ta>
            <ta e="T121" id="Seg_11071" s="T119">It/he is taking water</ta>
            <ta e="T124" id="Seg_11072" s="T121">Then the rain will come.</ta>
            <ta e="T126" id="Seg_11073" s="T124">It will hail.</ta>
            <ta e="T130" id="Seg_11074" s="T126">A very black cloud is coming.</ta>
            <ta e="T133" id="Seg_11075" s="T130">I went outside.</ta>
            <ta e="T136" id="Seg_11076" s="T133">A cuckoo is flying.</ta>
            <ta e="T143" id="Seg_11077" s="T136">I will look at it, it sat on the house and it is crying.</ta>
            <ta e="T149" id="Seg_11078" s="T143">I say: I will live alone.</ta>
            <ta e="T152" id="Seg_11079" s="T149">I will really live alone.</ta>
            <ta e="T155" id="Seg_11080" s="T152">S/he threw a stone at it.</ta>
            <ta e="T158" id="Seg_11081" s="T155">S/he threw a stick at it.</ta>
            <ta e="T161" id="Seg_11082" s="T158">It is still sitting.</ta>
            <ta e="T162" id="Seg_11083" s="T161">It is crying.</ta>
            <ta e="T167" id="Seg_11084" s="T162">Then it flew to where the sun gets up.</ta>
            <ta e="T173" id="Seg_11085" s="T167">Very wise people wrote on paper.</ta>
            <ta e="T178" id="Seg_11086" s="T173">This man is very bad.</ta>
            <ta e="T183" id="Seg_11087" s="T178">He takes everything and…</ta>
            <ta e="T187" id="Seg_11088" s="T183">He takes everything…</ta>
            <ta e="T190" id="Seg_11089" s="T187">Very thieving man.</ta>
            <ta e="T195" id="Seg_11090" s="T190">He takes everything…</ta>
            <ta e="T200" id="Seg_11091" s="T195">He takes everything, brings it home.</ta>
            <ta e="T205" id="Seg_11092" s="T200">I bought a shawl for the girl today.</ta>
            <ta e="T209" id="Seg_11093" s="T205">She went to Malinovka to study.</ta>
            <ta e="T216" id="Seg_11094" s="T209">There a woman asks her: "Where did you buy your shawl?"</ta>
            <ta e="T218" id="Seg_11095" s="T216">"Grandmother gave me."</ta>
            <ta e="T222" id="Seg_11096" s="T218">"But where did your grandmother buy it?"</ta>
            <ta e="T224" id="Seg_11097" s="T222">Polya gave it to me.</ta>
            <ta e="T230" id="Seg_11098" s="T224">I bought that horse, I gave a lot of money.</ta>
            <ta e="T234" id="Seg_11099" s="T230">But another horse I bought,…</ta>
            <ta e="T236" id="Seg_11100" s="T234">I gave little money.</ta>
            <ta e="T237" id="Seg_11101" s="T236">I gave a lot.</ta>
            <ta e="T241" id="Seg_11102" s="T237">I caught a calf.</ta>
            <ta e="T243" id="Seg_11103" s="T241">(?)…</ta>
            <ta e="T248" id="Seg_11104" s="T246">It does not go.</ta>
            <ta e="T251" id="Seg_11105" s="T248">I…</ta>
            <ta e="T258" id="Seg_11106" s="T256">It is very strong.</ta>
            <ta e="T262" id="Seg_11107" s="T258">I cannot lead it.</ta>
            <ta e="T268" id="Seg_11108" s="T262">I am going.</ta>
            <ta e="T273" id="Seg_11109" s="T269">Bad boy.</ta>
            <ta e="T278" id="Seg_11110" s="T273">He always steals and takes things.</ta>
            <ta e="T281" id="Seg_11111" s="T278">I went to the taiga.</ta>
            <ta e="T285" id="Seg_11112" s="T281">I saw something.</ta>
            <ta e="T289" id="Seg_11113" s="T285">Then I got scared.</ta>
            <ta e="T294" id="Seg_11114" s="T289">Then I called for God.</ta>
            <ta e="T297" id="Seg_11115" s="T294">They left. [?]</ta>
            <ta e="T300" id="Seg_11116" s="T297">I should drill holes.</ta>
            <ta e="T303" id="Seg_11117" s="T300">I should make the gate.</ta>
            <ta e="T314" id="Seg_11118" s="T304">I should fill these holes with wood.</ta>
            <ta e="T317" id="Seg_11119" s="T314">Then we will close the corral.</ta>
            <ta e="T321" id="Seg_11120" s="T317">Then the calf will not run [away].</ta>
            <ta e="T326" id="Seg_11121" s="T321">It will sit in the corral… it is standing in the corral.</ta>
            <ta e="T331" id="Seg_11122" s="T327">Go on, say what.</ta>
            <ta e="T340" id="Seg_11123" s="T335">Something smells.</ta>
            <ta e="T344" id="Seg_11124" s="T340">Your nose is very…</ta>
            <ta e="T352" id="Seg_11125" s="T346">Your nose is very sharp, it feels everything.</ta>
            <ta e="T355" id="Seg_11126" s="T352">Where were you?</ta>
            <ta e="T357" id="Seg_11127" s="T355">I went to bring water.</ta>
            <ta e="T361" id="Seg_11128" s="T357">I should boil tea.</ta>
            <ta e="T366" id="Seg_11129" s="T361">I should cook soup.</ta>
            <ta e="T368" id="Seg_11130" s="T366">To feed the men.</ta>
            <ta e="T372" id="Seg_11131" s="T368">I have ten rubles.</ta>
            <ta e="T376" id="Seg_11132" s="T372">I should give you five.</ta>
            <ta e="T379" id="Seg_11133" s="T376">And keep five for myself.</ta>
            <ta e="T385" id="Seg_11134" s="T379">Maybe you will go somewhere, you will need money.</ta>
            <ta e="T389" id="Seg_11135" s="T386">Matveyev is sitting.</ta>
            <ta e="T391" id="Seg_11136" s="T389">He does not speak.</ta>
            <ta e="T393" id="Seg_11137" s="T391">His heart aches.</ta>
            <ta e="T396" id="Seg_11138" s="T393">There is no paper at home.</ta>
            <ta e="T399" id="Seg_11139" s="T396">His wife got angry.</ta>
            <ta e="T404" id="Seg_11140" s="T399">She does not write a letter.</ta>
            <ta e="T406" id="Seg_11141" s="T404">Don't be angry!</ta>
            <ta e="T412" id="Seg_11142" s="T409">Soon I will come home.</ta>
            <ta e="T415" id="Seg_11143" s="T412">I will bring you something.</ta>
            <ta e="T418" id="Seg_11144" s="T415">Don't speak loudly!</ta>
            <ta e="T420" id="Seg_11145" s="T418">Speak softly!</ta>
            <ta e="T424" id="Seg_11146" s="T420">Otherwise all the people will hear.</ta>
            <ta e="T427" id="Seg_11147" s="T424">Don't shout loudly!</ta>
            <ta e="T440" id="Seg_11148" s="T427">And don't you swear, otherwise people will hear how you are swearing!</ta>
            <ta e="T444" id="Seg_11149" s="T440">I poured cream today.</ta>
            <ta e="T445" id="Seg_11150" s="T444">I churned.</ta>
            <ta e="T447" id="Seg_11151" s="T445">I washed the pots.</ta>
            <ta e="T451" id="Seg_11152" s="T447">I waited, the sun is drying everything.</ta>
            <ta e="T453" id="Seg_11153" s="T451">What did you bring?</ta>
            <ta e="T455" id="Seg_11154" s="T453">Show me!</ta>
            <ta e="T460" id="Seg_11155" s="T455">What is lying there?</ta>
            <ta e="T464" id="Seg_11156" s="T460">I went to pick bear leek today.</ta>
            <ta e="T466" id="Seg_11157" s="T464">I brought bear leek.</ta>
            <ta e="T468" id="Seg_11158" s="T466">Then I will chop it.</ta>
            <ta e="T471" id="Seg_11159" s="T468">I will put eggs.</ta>
            <ta e="T473" id="Seg_11160" s="T471">I will put sour cream.</ta>
            <ta e="T477" id="Seg_11161" s="T473">Then I can bake pies.</ta>
            <ta e="T480" id="Seg_11162" s="T478">I cooked meat.</ta>
            <ta e="T483" id="Seg_11163" s="T480">They ate the meat. [?]</ta>
            <ta e="T486" id="Seg_11164" s="T483">One bone remained.</ta>
            <ta e="T489" id="Seg_11165" s="T486">Only bones remained.</ta>
            <ta e="T493" id="Seg_11166" s="T489">The dogs are eating the bone.</ta>
            <ta e="T497" id="Seg_11167" s="T493">We threw away very many bones.</ta>
            <ta e="T506" id="Seg_11168" s="T497">I baked bread today, [it was] not sour.</ta>
            <ta e="T509" id="Seg_11169" s="T506">It is not good.</ta>
            <ta e="T517" id="Seg_11170" s="T509">I gave the bread to these dogs.</ta>
            <ta e="T524" id="Seg_11171" s="T517">The dogs are eating the bread and fighting.</ta>
            <ta e="T530" id="Seg_11172" s="T524">A dog knows this dog. [?]</ta>
            <ta e="T532" id="Seg_11173" s="T530">It was evening.</ta>
            <ta e="T536" id="Seg_11174" s="T532">It was evening, very…</ta>
            <ta e="T542" id="Seg_11175" s="T539">Nothing is visible.</ta>
            <ta e="T546" id="Seg_11176" s="T542">I did not know (him?).</ta>
            <ta e="T548" id="Seg_11177" s="T546">Who comes.</ta>
            <ta e="T552" id="Seg_11178" s="T548">My father made birchbark vessels.</ta>
            <ta e="T558" id="Seg_11179" s="T552">He peeled bark and made them.</ta>
            <ta e="T560" id="Seg_11180" s="T558">[People] sewed brichbark vessels.</ta>
            <ta e="T562" id="Seg_11181" s="T560">With tendons.</ta>
            <ta e="T567" id="Seg_11182" s="T562">With needle, and there was a thimble on the finger.</ta>
            <ta e="T570" id="Seg_11183" s="T567">He sewed birchbark vessels.</ta>
            <ta e="T572" id="Seg_11184" s="T570">With tendons.</ta>
            <ta e="T578" id="Seg_11185" s="T572">And he put a thimble on the finger.</ta>
            <ta e="T581" id="Seg_11186" s="T578">And took a needle.</ta>
            <ta e="T582" id="Seg_11187" s="T581">He sewed.</ta>
            <ta e="T585" id="Seg_11188" s="T582">That's all, that's all…</ta>
            <ta e="T595" id="Seg_11189" s="T591">He'll stick his finger.</ta>
            <ta e="T599" id="Seg_11190" s="T595">You care about your wife.</ta>
            <ta e="T602" id="Seg_11191" s="T599">Her heart aches very [much].</ta>
            <ta e="T610" id="Seg_11192" s="T602">My house is black, I will wash it tomorrow.</ta>
            <ta e="T612" id="Seg_11193" s="T610">Very dirty.</ta>
            <ta e="T616" id="Seg_11194" s="T612">Or else people will say…</ta>
            <ta e="T619" id="Seg_11195" s="T616">They say: lazy woman!</ta>
            <ta e="T622" id="Seg_11196" s="T619">She does not wash the house.</ta>
            <ta e="T629" id="Seg_11197" s="T626">What should I tell you?</ta>
            <ta e="T632" id="Seg_11198" s="T629">I forgot everything.</ta>
            <ta e="T637" id="Seg_11199" s="T632">Today the bedbugs did not let me sleep.</ta>
            <ta e="T644" id="Seg_11200" s="T637">This man asked for flour, I should give him some.</ta>
            <ta e="T648" id="Seg_11201" s="T644">He does not have bread to eat.</ta>
            <ta e="T649" id="Seg_11202" s="T648">That's all.</ta>
            <ta e="T659" id="Seg_11203" s="T649">Where to take, what to say.</ta>
            <ta e="T666" id="Seg_11204" s="T659">I need to speak to her/him a lot.</ta>
            <ta e="T672" id="Seg_11205" s="T668">I need to go to look for (it?).</ta>
            <ta e="T674" id="Seg_11206" s="T672">Somewhere to speak.</ta>
            <ta e="T682" id="Seg_11207" s="T674">Yesterday I did not lie down to sleep for a long time.</ta>
            <ta e="T692" id="Seg_11208" s="T684">For a long time I did not lie down to sleep.</ta>
            <ta e="T700" id="Seg_11209" s="T696">I slept, I did not see anything.</ta>
            <ta e="T706" id="Seg_11210" s="T700">This man’s hair were all black.</ta>
            <ta e="T712" id="Seg_11211" s="T706">Now his hair became very white.</ta>
            <ta e="T719" id="Seg_11212" s="T712">He has one hand, and one hand was torn (off).</ta>
            <ta e="T727" id="Seg_11213" s="T719">He has one hand, and one hand was torn off.</ta>
            <ta e="T733" id="Seg_11214" s="T727">I fell, I broke one of my arm.</ta>
            <ta e="T740" id="Seg_11215" s="T733">I fell and broke my arm.</ta>
            <ta e="T745" id="Seg_11216" s="T740">Then they put my arm. [?]</ta>
            <ta e="T755" id="Seg_11217" s="T901">Then they bandaged my arm.</ta>
            <ta e="T761" id="Seg_11218" s="T758">Until it grows.</ta>
            <ta e="T764" id="Seg_11219" s="T761">I must not do any work.</ta>
            <ta e="T767" id="Seg_11220" s="T764">I need to harness the horse.</ta>
            <ta e="T775" id="Seg_11221" s="T767">The horse was running.</ta>
            <ta e="T788" id="Seg_11222" s="T779">The horse started running, it fell, it broke its foot.</ta>
            <ta e="T800" id="Seg_11223" s="T788">My hand has five fingers, but your hand does not have one, you chopped it off with an axe.</ta>
            <ta e="T806" id="Seg_11224" s="T800">Today the children were chasing the calf a lot.</ta>
            <ta e="T812" id="Seg_11225" s="T806">It came back and ran home.</ta>
            <ta e="T816" id="Seg_11226" s="T812">In the evening it went away.</ta>
            <ta e="T818" id="Seg_11227" s="T816">Evening is coming.</ta>
            <ta e="T821" id="Seg_11228" s="T818">The sun is setting.</ta>
            <ta e="T823" id="Seg_11229" s="T821">Evening is coming.</ta>
            <ta e="T825" id="Seg_11230" s="T823">The sun is setting.</ta>
            <ta e="T830" id="Seg_11231" s="T825">Enough of talking, I need to lie down to sleep.</ta>
            <ta e="T834" id="Seg_11232" s="T830">In the morning I need to get up early.</ta>
            <ta e="T839" id="Seg_11233" s="T834">I say, in the morning I need to get up early.</ta>
            <ta e="T842" id="Seg_11234" s="T839">I will go to pick flowers.</ta>
            <ta e="T847" id="Seg_11235" s="T842">Paper is hanging on a birch.</ta>
            <ta e="T849" id="Seg_11236" s="T847">One doesn't see it.</ta>
            <ta e="T852" id="Seg_11237" s="T849">I don’t see anything.</ta>
            <ta e="T856" id="Seg_11238" s="T852">My relative is very…</ta>
            <ta e="T859" id="Seg_11239" s="T857">…lives very far.</ta>
            <ta e="T863" id="Seg_11240" s="T859">I need to go for two days.</ta>
            <ta e="T868" id="Seg_11241" s="T863">But maybe you will go for one day.</ta>
            <ta e="T874" id="Seg_11242" s="T868">I came to him/her, but s/he is not there.</ta>
            <ta e="T917" id="Seg_11243" s="T874">He went away somewhere, I came back.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PKZ">
            <ta e="T12" id="Seg_11244" s="T4">Ich kann mich nicht bewege, mir tut alles weh.</ta>
            <ta e="T15" id="Seg_11245" s="T12">Ich sterbe.</ta>
            <ta e="T20" id="Seg_11246" s="T15">Heute ist der große Tag dieses Mädchens (=Geburtstag).</ta>
            <ta e="T28" id="Seg_11247" s="T20">Ihre Mutter gebar sie heute.</ta>
            <ta e="T38" id="Seg_11248" s="T28">Sie ist zwölf Jahre und noch zwei.</ta>
            <ta e="T43" id="Seg_11249" s="T39">Wie geizig ist dieser Mann nur.</ta>
            <ta e="T46" id="Seg_11250" s="T43">Er nimmt nichts.</ta>
            <ta e="T49" id="Seg_11251" s="T46">Er gibt nichts her!</ta>
            <ta e="T54" id="Seg_11252" s="T49">Und der andere Mann ist so gut.</ta>
            <ta e="T59" id="Seg_11253" s="T54">Er gibt alles, was [er nur hat?].</ta>
            <ta e="T69" id="Seg_11254" s="T61">Was auch immer…</ta>
            <ta e="T74" id="Seg_11255" s="T69">Was auch immer man verlangt, er gibt es einem.</ta>
            <ta e="T78" id="Seg_11256" s="T74">Er hat ein sehr gutes Herz.</ta>
            <ta e="T83" id="Seg_11257" s="T78">Wie soll man auf einmal…</ta>
            <ta e="T86" id="Seg_11258" s="T83">Er trinkt kein Vodka.</ta>
            <ta e="T90" id="Seg_11259" s="T86">Was er hat, er gibt es alles.</ta>
            <ta e="T94" id="Seg_11260" s="T90">Er gibt Eier, (Fleisch-).</ta>
            <ta e="T98" id="Seg_11261" s="T94">Er gibt Butter, er gibt Brot.</ta>
            <ta e="T102" id="Seg_11262" s="T98">Wenn er Fleisch hat, gibt er es einem.</ta>
            <ta e="T107" id="Seg_11263" s="T102">Was auch immer er hat, er gibt es alles.</ta>
            <ta e="T112" id="Seg_11264" s="T107">Der Donner grollt oben!</ta>
            <ta e="T114" id="Seg_11265" s="T112">Gott grollt.</ta>
            <ta e="T117" id="Seg_11266" s="T114">Er wird jemand töten. [?]</ta>
            <ta e="T119" id="Seg_11267" s="T117">Ein Regenbogen steht.</ta>
            <ta e="T121" id="Seg_11268" s="T119">Der bringt Wasser.</ta>
            <ta e="T124" id="Seg_11269" s="T121">Dann kommt der Regen.</ta>
            <ta e="T126" id="Seg_11270" s="T124">Es wird hageln.</ta>
            <ta e="T130" id="Seg_11271" s="T126">Eine sehr schwarze Wolke kommt.</ta>
            <ta e="T133" id="Seg_11272" s="T130">Ich ging hinaus.</ta>
            <ta e="T136" id="Seg_11273" s="T133">Ein Kuckuck fliegt.</ta>
            <ta e="T143" id="Seg_11274" s="T136">Ich schau ihn mir an, er saß auf dem Haus, und er weint.</ta>
            <ta e="T149" id="Seg_11275" s="T143">Ich sage: Ich werde alleine leben.</ta>
            <ta e="T152" id="Seg_11276" s="T149">Ich werde wirklich alleine leben.</ta>
            <ta e="T155" id="Seg_11277" s="T152">Sie/Er warf einen Stein nach ihm.</ta>
            <ta e="T158" id="Seg_11278" s="T155">Sie/Er warf einen Stock nach ihm.</ta>
            <ta e="T161" id="Seg_11279" s="T158">Er sitzt immer noch.</ta>
            <ta e="T162" id="Seg_11280" s="T161">Er weint.</ta>
            <ta e="T167" id="Seg_11281" s="T162">Dann flog er dahin, wo die Sonne aufsteht.</ta>
            <ta e="T173" id="Seg_11282" s="T167">Sehr beflissene Leute schrieben auf Papier.</ta>
            <ta e="T178" id="Seg_11283" s="T173">Dieser Mann ist sehr böse.</ta>
            <ta e="T183" id="Seg_11284" s="T178">Er nimmt alles und…</ta>
            <ta e="T187" id="Seg_11285" s="T183">Er nimmt alles…</ta>
            <ta e="T190" id="Seg_11286" s="T187">Sehr dieberischer Mann.</ta>
            <ta e="T195" id="Seg_11287" s="T190">Er nimmt alles…</ta>
            <ta e="T200" id="Seg_11288" s="T195">Er nimmt alles, bringt es nach Hause.</ta>
            <ta e="T205" id="Seg_11289" s="T200">Ich kaufte heute dem Mädchen einen Schal.</ta>
            <ta e="T209" id="Seg_11290" s="T205">Sie ging nach Malinovka um zu studieren.</ta>
            <ta e="T216" id="Seg_11291" s="T209">Dort fragt sie eine Frau: „Wo hast du dein Schal gekauft!?“</ta>
            <ta e="T218" id="Seg_11292" s="T216">Großmutter schenkte ihn mir.</ta>
            <ta e="T222" id="Seg_11293" s="T218">Aber wo hat deine Großmutter ihn gekauft?</ta>
            <ta e="T224" id="Seg_11294" s="T222">Polya gab ihn mir.</ta>
            <ta e="T230" id="Seg_11295" s="T224">Ich kaufte jenes Pferd, ich gab eine Menge Geld her.</ta>
            <ta e="T234" id="Seg_11296" s="T230">Doch ein anderes Pferd, das ich kaufte,…</ta>
            <ta e="T236" id="Seg_11297" s="T234">Ich gab wenig Geld her.</ta>
            <ta e="T237" id="Seg_11298" s="T236">Ich gab eine Menge.</ta>
            <ta e="T241" id="Seg_11299" s="T237">Ich erwischte ein Kalb.</ta>
            <ta e="T243" id="Seg_11300" s="T241">(?)…</ta>
            <ta e="T248" id="Seg_11301" s="T246">Es will nicht gehen.</ta>
            <ta e="T251" id="Seg_11302" s="T248">Ich…</ta>
            <ta e="T258" id="Seg_11303" s="T256">Es ist sehr stark.</ta>
            <ta e="T262" id="Seg_11304" s="T258">Ich kann es nicht führen.</ta>
            <ta e="T268" id="Seg_11305" s="T262">Ich gehe.</ta>
            <ta e="T273" id="Seg_11306" s="T269">Böser Junge.</ta>
            <ta e="T278" id="Seg_11307" s="T273">Er klaut immer und nimmt sich Sachen.</ta>
            <ta e="T281" id="Seg_11308" s="T278">Ich ging auf die Taiga.</ta>
            <ta e="T285" id="Seg_11309" s="T281">Ich sah etwas.</ta>
            <ta e="T289" id="Seg_11310" s="T285">Dann bekam ich Angst.</ta>
            <ta e="T294" id="Seg_11311" s="T289">Dann rief ich nach Gott.</ta>
            <ta e="T297" id="Seg_11312" s="T294">Sie gingen. [?]</ta>
            <ta e="T300" id="Seg_11313" s="T297">Ich sollte Löcher bohren.</ta>
            <ta e="T303" id="Seg_11314" s="T300">Ich sollte die Pforte bauen.</ta>
            <ta e="T314" id="Seg_11315" s="T304">Ich sollte diese Löcher mit Holz füllen.</ta>
            <ta e="T317" id="Seg_11316" s="T314">Dann werden wir den Zwinger schliessen.</ta>
            <ta e="T321" id="Seg_11317" s="T317">Dann läuft das Kalb nicht [weg].</ta>
            <ta e="T326" id="Seg_11318" s="T321">Es wird im Zwinger sitzen… es steht im Zwinger.</ta>
            <ta e="T331" id="Seg_11319" s="T327">Mach schon, sag was.</ta>
            <ta e="T340" id="Seg_11320" s="T335">Etwas riecht.</ta>
            <ta e="T344" id="Seg_11321" s="T340">Deine Nase ist sehr…</ta>
            <ta e="T352" id="Seg_11322" s="T346">Deine Nase ist sehr spitz, sie spürt alles.</ta>
            <ta e="T355" id="Seg_11323" s="T352">Wo warst du?</ta>
            <ta e="T357" id="Seg_11324" s="T355">Ich ging Wasser holen.</ta>
            <ta e="T361" id="Seg_11325" s="T357">Ich sollte Tee kochen.</ta>
            <ta e="T366" id="Seg_11326" s="T361">Ich sollte Suppe kochen.</ta>
            <ta e="T368" id="Seg_11327" s="T366">Um den Männern Essen zu geben.</ta>
            <ta e="T372" id="Seg_11328" s="T368">Ich habe zehn Rubel.</ta>
            <ta e="T376" id="Seg_11329" s="T372">Ich sollte dir fünf geben.</ta>
            <ta e="T379" id="Seg_11330" s="T376">Und für mich fünf behalten.</ta>
            <ta e="T385" id="Seg_11331" s="T379">Vielleicht gehst du irgendwo hin, du wirst Geld brauchen.</ta>
            <ta e="T389" id="Seg_11332" s="T386">Matveyev sitzt.</ta>
            <ta e="T391" id="Seg_11333" s="T389">Er spricht nicht.</ta>
            <ta e="T393" id="Seg_11334" s="T391">Sein Herz tut ihm weh.</ta>
            <ta e="T396" id="Seg_11335" s="T393">Es ist zu Hause kein Papier.</ta>
            <ta e="T399" id="Seg_11336" s="T396">Seine Frau wurde wütend.</ta>
            <ta e="T404" id="Seg_11337" s="T399">Sie schreibt keinen Brief.</ta>
            <ta e="T406" id="Seg_11338" s="T404">Sei nicht wütend!</ta>
            <ta e="T412" id="Seg_11339" s="T409">Bald werde ich nach Hause kommen.</ta>
            <ta e="T415" id="Seg_11340" s="T412">Ich werde dir etwas bringen.</ta>
            <ta e="T418" id="Seg_11341" s="T415">Sprich nicht laut!</ta>
            <ta e="T420" id="Seg_11342" s="T418">Sprich leise!</ta>
            <ta e="T424" id="Seg_11343" s="T420">Sonst werden es alle Leute hören.</ta>
            <ta e="T427" id="Seg_11344" s="T424">Ruf nicht laut!</ta>
            <ta e="T440" id="Seg_11345" s="T427">Und fluche ja nicht, sonst werden Leute hören, wie du fluchst!</ta>
            <ta e="T444" id="Seg_11346" s="T440">Ich goss heute Sahne</ta>
            <ta e="T445" id="Seg_11347" s="T444">Ich schlug sie.</ta>
            <ta e="T447" id="Seg_11348" s="T445">Ich wusch die Töpfe.</ta>
            <ta e="T451" id="Seg_11349" s="T447">Ich wartete, die Sonne trocknet alles.</ta>
            <ta e="T453" id="Seg_11350" s="T451">Was hast du gebracht?</ta>
            <ta e="T455" id="Seg_11351" s="T453">Zeig her!</ta>
            <ta e="T460" id="Seg_11352" s="T455">Was liegt dort?</ta>
            <ta e="T464" id="Seg_11353" s="T460">Ich ging heute Bärlauch pflücken.</ta>
            <ta e="T466" id="Seg_11354" s="T464">Ich brachte Bärlauch</ta>
            <ta e="T468" id="Seg_11355" s="T466">Dann werde ich ihn schneiden.</ta>
            <ta e="T471" id="Seg_11356" s="T468">Ich werde Eier dazu tun.</ta>
            <ta e="T473" id="Seg_11357" s="T471">Ich werde sauere Sahne dazu tun.</ta>
            <ta e="T477" id="Seg_11358" s="T473">Dann kann ich Pasteten backen.</ta>
            <ta e="T480" id="Seg_11359" s="T478">Ich kochte Fleisch.</ta>
            <ta e="T483" id="Seg_11360" s="T480">Sie aßen das Fleisch. [?]</ta>
            <ta e="T486" id="Seg_11361" s="T483">Ein Knochen blieb übrig.</ta>
            <ta e="T489" id="Seg_11362" s="T486">Nur Knochen blieben übrig.</ta>
            <ta e="T493" id="Seg_11363" s="T489">Die Hunde fressen den Knochen.</ta>
            <ta e="T497" id="Seg_11364" s="T493">Wir warfen sehr viele Knochen weg.</ta>
            <ta e="T506" id="Seg_11365" s="T497">Ich backte heute Brot, es gab keine sauere Sahne.</ta>
            <ta e="T509" id="Seg_11366" s="T506">Es ist nicht gut.</ta>
            <ta e="T517" id="Seg_11367" s="T509">Ich gab diesen Hunden das Brot.</ta>
            <ta e="T524" id="Seg_11368" s="T517">Die Hunde fressen das Brot und kämpfen.</ta>
            <ta e="T530" id="Seg_11369" s="T524">Ein Hund kennt diesen Hund. [?]</ta>
            <ta e="T532" id="Seg_11370" s="T530">Es war abends.</ta>
            <ta e="T536" id="Seg_11371" s="T532">Es war abends, sehr…</ta>
            <ta e="T542" id="Seg_11372" s="T539">Nichts ist sichtbar.</ta>
            <ta e="T546" id="Seg_11373" s="T542">Ich kannte ihn (nicht?).</ta>
            <ta e="T548" id="Seg_11374" s="T546">Wer kommt.</ta>
            <ta e="T552" id="Seg_11375" s="T548">Mein Vater machte Birkenrinde-Gefäße.</ta>
            <ta e="T558" id="Seg_11376" s="T552">Er schälte die Rinde und machte sie.</ta>
            <ta e="T560" id="Seg_11377" s="T558">[Die Leute] nähten Birkenrinde-Gefäße.</ta>
            <ta e="T562" id="Seg_11378" s="T560">Mit Sehnen.</ta>
            <ta e="T567" id="Seg_11379" s="T562">Mit Nadel, und es war ein Fingerhut auf dem Finger.</ta>
            <ta e="T570" id="Seg_11380" s="T567">Er nähte Birkenrinde-Gefäße.</ta>
            <ta e="T572" id="Seg_11381" s="T570">Mit Sehnen.</ta>
            <ta e="T578" id="Seg_11382" s="T572">Und er setzte einen Fingerhut auf den Finger.</ta>
            <ta e="T581" id="Seg_11383" s="T578">Und nahm eine Nadel.</ta>
            <ta e="T582" id="Seg_11384" s="T581">Er nähte.</ta>
            <ta e="T585" id="Seg_11385" s="T582">Das ist alles, das ist alles…</ta>
            <ta e="T595" id="Seg_11386" s="T591">Her wird sich in den Finger stechen.</ta>
            <ta e="T599" id="Seg_11387" s="T595">Du kümmerst dich um deine Frau.</ta>
            <ta e="T602" id="Seg_11388" s="T599">Ihr Herz tut ihr sehr weh.</ta>
            <ta e="T610" id="Seg_11389" s="T602">Mein Haus ist schwarz, ich werde es morgen waschen.</ta>
            <ta e="T612" id="Seg_11390" s="T610">Sehr schmutzig.</ta>
            <ta e="T616" id="Seg_11391" s="T612">Sonst werden die Leute sagen…</ta>
            <ta e="T619" id="Seg_11392" s="T616">Sie sagen: faules Weib!</ta>
            <ta e="T622" id="Seg_11393" s="T619">Sie wäscht nicht das Haus.</ta>
            <ta e="T629" id="Seg_11394" s="T626">Was soll ich dir erzählen?</ta>
            <ta e="T632" id="Seg_11395" s="T629">Ich habe alles vergessen.</ta>
            <ta e="T637" id="Seg_11396" s="T632">Heute ließen die Bettwanzen mich nicht schlafen.</ta>
            <ta e="T644" id="Seg_11397" s="T637">Dieser Mann bat mir um Mehl, ich sollte ihm etwas geben.</ta>
            <ta e="T648" id="Seg_11398" s="T644">Er hat kein Brot zu essen.</ta>
            <ta e="T649" id="Seg_11399" s="T648">Das ist alles.</ta>
            <ta e="T659" id="Seg_11400" s="T649">Woher nehmen, was sagen.</ta>
            <ta e="T666" id="Seg_11401" s="T659">Ich muß viel mit ihr/ihm sprechen.</ta>
            <ta e="T672" id="Seg_11402" s="T668">Ich muß es suchen (gehen?).</ta>
            <ta e="T674" id="Seg_11403" s="T672">Irgendwo um zu sprechen.</ta>
            <ta e="T682" id="Seg_11404" s="T674">Gestern habe ich mich lange nicht schlafen gelegt.</ta>
            <ta e="T692" id="Seg_11405" s="T684">Lange Zeit habe ich mich nicht schlafen gelegt.</ta>
            <ta e="T700" id="Seg_11406" s="T696">Ich schlief, ich habe nichts gesehen.</ta>
            <ta e="T706" id="Seg_11407" s="T700">Die Haare von diesem Mann waren ganz schwarz.</ta>
            <ta e="T712" id="Seg_11408" s="T706">Nun ist sein Haar sehr weiß.</ta>
            <ta e="T719" id="Seg_11409" s="T712">Er hat eine Hand, und eine Hand wurde (ab)gerissen.</ta>
            <ta e="T727" id="Seg_11410" s="T719">Er hat eine Hand, und eine Hand wurde (ab)gerissen.</ta>
            <ta e="T733" id="Seg_11411" s="T727">Ich fiel, ich brach mir eines meinen Armen.</ta>
            <ta e="T740" id="Seg_11412" s="T733">Ich fiel und brach mir den Arm.</ta>
            <ta e="T745" id="Seg_11413" s="T740">Dann legten sie meinen Arm (?)</ta>
            <ta e="T755" id="Seg_11414" s="T901">Dann verbanden sie meinen Arm.</ta>
            <ta e="T761" id="Seg_11415" s="T758">Bis er wächst.</ta>
            <ta e="T764" id="Seg_11416" s="T761">Ich darf nichts arbeiten.</ta>
            <ta e="T767" id="Seg_11417" s="T764">Ich muß das Pferd zäumen.</ta>
            <ta e="T775" id="Seg_11418" s="T767">Das Pferd lief.</ta>
            <ta e="T788" id="Seg_11419" s="T779">Das Pferd fing an, zu laufen, es fiel und brach sich den Fuß.</ta>
            <ta e="T800" id="Seg_11420" s="T788">Meine Hand hat fünf Finger, aber deiner Hand fehlt einer, du hast ihn mit einer Axt abgeschlagen</ta>
            <ta e="T806" id="Seg_11421" s="T800">Heute haben die Kinder das Kalb lange gejagt.</ta>
            <ta e="T812" id="Seg_11422" s="T806">Es kam zurück und lief nach Hause.</ta>
            <ta e="T816" id="Seg_11423" s="T812">Am Abend ging es weg.</ta>
            <ta e="T818" id="Seg_11424" s="T816">Es wird Abend.</ta>
            <ta e="T821" id="Seg_11425" s="T818">Die Sonne geht unter.</ta>
            <ta e="T823" id="Seg_11426" s="T821">Es wird Abend.</ta>
            <ta e="T825" id="Seg_11427" s="T823">Die Sonne geht unter.</ta>
            <ta e="T830" id="Seg_11428" s="T825">Genug geredet, ich muß mich Schlafen legen.</ta>
            <ta e="T834" id="Seg_11429" s="T830">Morgen muß ich früh aufstehen.</ta>
            <ta e="T839" id="Seg_11430" s="T834">Ich sage, morgen muß ich früh aufstehen.</ta>
            <ta e="T842" id="Seg_11431" s="T839">Ich werde Blumen pflücken gehen.</ta>
            <ta e="T847" id="Seg_11432" s="T842">Papier hängt an der Birke.</ta>
            <ta e="T849" id="Seg_11433" s="T847">Man sieht es nicht.</ta>
            <ta e="T852" id="Seg_11434" s="T849">Ich sehe nichts.</ta>
            <ta e="T856" id="Seg_11435" s="T852">Mein(e) Verwandte(r) ist sehr…</ta>
            <ta e="T859" id="Seg_11436" s="T857">…lebt sehr weit weg.</ta>
            <ta e="T863" id="Seg_11437" s="T859">Ich muß zwei Tage gehen.</ta>
            <ta e="T868" id="Seg_11438" s="T863">Aber vielleicht gehst du einen Tag.</ta>
            <ta e="T874" id="Seg_11439" s="T868">Ich kam zu ihm/ihr, aber er/sie ist nicht da.</ta>
            <ta e="T917" id="Seg_11440" s="T874">Er ging irgendwo fort, ich kam zurück.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-PKZ">
            <ta e="T38" id="Seg_11441" s="T28">[KlT:] 14? [KA] двадцать лет, еще два</ta>
            <ta e="T121" id="Seg_11442" s="T119">[KlT]: Or "There is water."?</ta>
            <ta e="T126" id="Seg_11443" s="T124">[GVY]: Toskanak is maybe pronounced as toskan.</ta>
            <ta e="T130" id="Seg_11444" s="T126">[KlT]: Ru. туча ’(storm) cloud’.</ta>
            <ta e="T155" id="Seg_11445" s="T152">[KlT]: Probably barəʔpi = barəʔpim [-1SG]</ta>
            <ta e="T317" id="Seg_11446" s="T314">[KlT]: 1.DU? Really?</ta>
            <ta e="T379" id="Seg_11447" s="T376">[GVY]: ma-azittə?</ta>
            <ta e="T386" id="Seg_11448" s="T385">At the end it is possible to hear the perfect ending (-ʔ)-pi.</ta>
            <ta e="T420" id="Seg_11449" s="T418">[KlT]: Koldʼəŋ - only attested in KP’s speech.</ta>
            <ta e="T424" id="Seg_11450" s="T420">[KlT]: FUT or PRS?</ta>
            <ta e="T483" id="Seg_11451" s="T480">[KlT]: Shortened tĭmezʼiʔ izittə?</ta>
            <ta e="T530" id="Seg_11452" s="T524">[GVY]: Maybe Măn… 'I know this dog'?</ta>
            <ta e="T552" id="Seg_11453" s="T548">[KlT]: Ru. туес 'birchbark vessel'.</ta>
            <ta e="T562" id="Seg_11454" s="T560">[KlT]: Жила Ru. ’tendon’. </ta>
            <ta e="T637" id="Seg_11455" s="T632">[KlT]: Клоп ’bedbug’.</ta>
            <ta e="T706" id="Seg_11456" s="T700">[GVY]: LOC as in Russian "у этого человека".</ta>
            <ta e="T917" id="Seg_11457" s="T874"> KIT.: kallatʼürbi [WNB] agreement!</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T884" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T885" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T886" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T889" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T890" />
            <conversion-tli id="T587" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T588" />
            <conversion-tli id="T893" />
            <conversion-tli id="T589" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T590" />
            <conversion-tli id="T896" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T746" />
            <conversion-tli id="T903" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T904" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T880" />
            <conversion-tli id="T753" />
            <conversion-tli id="T905" />
            <conversion-tli id="T754" />
            <conversion-tli id="T918" />
            <conversion-tli id="T755" />
            <conversion-tli id="T919" />
            <conversion-tli id="T906" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T907" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T776" />
            <conversion-tli id="T910" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T921" />
            <conversion-tli id="T911" />
            <conversion-tli id="T920" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T912" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T913" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T914" />
            <conversion-tli id="T839" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T922" />
            <conversion-tli id="T917" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-KA"
                          name="ref"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KA"
                          name="ts"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KA"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KA"
                          type="t" />
         <conversion-tier category="CS"
                          display-name="CS-KA"
                          name="CS"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KA"
                          name="fr"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KA"
                          name="fe"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KA"
                          name="fg"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KA"
                          name="nt"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-PKZ"
                          name="ref"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PKZ"
                          name="ts"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PKZ"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PKZ"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PKZ"
                          name="mb"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PKZ"
                          name="mp"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PKZ"
                          name="ge"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PKZ"
                          name="gr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PKZ"
                          name="mc"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PKZ"
                          name="ps"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PKZ"
                          name="SeR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PKZ"
                          name="SyF"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PKZ"
                          name="IST"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PKZ"
                          name="BOR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PKZ"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PKZ"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PKZ"
                          name="CS"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PKZ"
                          name="fr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PKZ"
                          name="fe"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PKZ"
                          name="fg"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PKZ"
                          name="nt"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
