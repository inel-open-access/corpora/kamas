<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDDB0959EB-CCE4-F01C-0EFD-32E801FD2A5C">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_SU0191.wav" />
         <referenced-file url="PKZ_196X_SU0191.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_196X_SU0191\PKZ_196X_SU0191.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1609</ud-information>
            <ud-information attribute-name="# HIAT:w">1087</ud-information>
            <ud-information attribute-name="# e">1080</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">2</ud-information>
            <ud-information attribute-name="# HIAT:u">256</ud-information>
            <ud-information attribute-name="# sc">6</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="KA">
            <abbreviation>KA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1087" time="0.0" type="intp" />
         <tli id="T0" time="1.475" type="appl" />
         <tli id="T1" time="1.929" type="appl" />
         <tli id="T2" time="2.38899998566931" />
         <tli id="T3" time="2.84299998446607" />
         <tli id="T4" time="3.793317519414364" />
         <tli id="T5" time="4.206" type="appl" />
         <tli id="T6" time="4.805" type="appl" />
         <tli id="T7" time="5.405" type="appl" />
         <tli id="T8" time="6.004" type="appl" />
         <tli id="T9" time="6.604" type="appl" />
         <tli id="T10" time="7.739967732935109" />
         <tli id="T11" time="8.601" type="appl" />
         <tli id="T12" time="9.215" type="appl" />
         <tli id="T13" time="9.829" type="appl" />
         <tli id="T14" time="10.442" type="appl" />
         <tli id="T15" time="11.056" type="appl" />
         <tli id="T16" time="11.67" type="appl" />
         <tli id="T17" time="12.626614027716707" />
         <tli id="T18" time="13.52" type="appl" />
         <tli id="T19" time="14.465" type="appl" />
         <tli id="T20" time="15.41" type="appl" />
         <tli id="T21" time="16.356" type="appl" />
         <tli id="T22" time="17.302" type="appl" />
         <tli id="T23" time="18.619922375613918" />
         <tli id="T24" time="19.338" type="appl" />
         <tli id="T25" time="20.047" type="appl" />
         <tli id="T26" time="20.755" type="appl" />
         <tli id="T27" time="21.464" type="appl" />
         <tli id="T28" time="22.173" type="appl" />
         <tli id="T29" time="22.882" type="appl" />
         <tli id="T30" time="23.591" type="appl" />
         <tli id="T31" time="24.299" type="appl" />
         <tli id="T32" time="25.008" type="appl" />
         <tli id="T33" time="25.724000155798244" />
         <tli id="T34" time="26.463" type="appl" />
         <tli id="T35" time="27.178" type="appl" />
         <tli id="T36" time="27.893" type="appl" />
         <tli id="T37" time="28.608" type="appl" />
         <tli id="T38" time="29.323" type="appl" />
         <tli id="T39" time="30.038" type="appl" />
         <tli id="T40" time="30.753" type="appl" />
         <tli id="T41" time="31.47499877741551" />
         <tli id="T42" time="32.318" type="appl" />
         <tli id="T43" time="33.45319387068766" />
         <tli id="T44" time="34.649" type="appl" />
         <tli id="T45" time="35.881" type="appl" />
         <tli id="T46" time="37.114" type="appl" />
         <tli id="T47" time="38.346" type="appl" />
         <tli id="T48" time="39.578" type="appl" />
         <tli id="T49" time="41.679826241438676" />
         <tli id="T50" time="42.715" type="appl" />
         <tli id="T51" time="43.534" type="appl" />
         <tli id="T52" time="44.353" type="appl" />
         <tli id="T53" time="45.172" type="appl" />
         <tli id="T54" time="45.991" type="appl" />
         <tli id="T55" time="47.41980231211665" />
         <tli id="T56" time="48.019" type="appl" />
         <tli id="T57" time="48.57" type="appl" />
         <tli id="T58" time="49.121" type="appl" />
         <tli id="T59" time="49.671" type="appl" />
         <tli id="T60" time="50.222" type="appl" />
         <tli id="T61" time="50.773" type="appl" />
         <tli id="T62" time="51.32433356060168" />
         <tli id="T63" time="52.059" type="appl" />
         <tli id="T64" time="52.794" type="appl" />
         <tli id="T65" time="53.53" type="appl" />
         <tli id="T66" time="54.265" type="appl" />
         <tli id="T67" time="55.833100571345" />
         <tli id="T68" time="56.586" type="appl" />
         <tli id="T69" time="58.161" type="appl" />
         <tli id="T70" time="59.735" type="appl" />
         <tli id="T71" time="60.489" type="appl" />
         <tli id="T72" time="61.243" type="appl" />
         <tli id="T73" time="62.003999574052564" />
         <tli id="T74" time="62.452" type="appl" />
         <tli id="T75" time="62.906" type="appl" />
         <tli id="T76" time="63.361" type="appl" />
         <tli id="T77" time="63.816" type="appl" />
         <tli id="T78" time="64.271" type="appl" />
         <tli id="T79" time="64.726" type="appl" />
         <tli id="T80" time="65.18" type="appl" />
         <tli id="T81" time="65.64200322611654" />
         <tli id="T82" time="66.858" type="appl" />
         <tli id="T83" time="68.082" type="appl" />
         <tli id="T84" time="69.70637606853532" />
         <tli id="T85" time="70.849" type="appl" />
         <tli id="T86" time="72.9263626447693" />
         <tli id="T87" time="73.403" type="appl" />
         <tli id="T88" time="74.412" type="appl" />
         <tli id="T89" time="75.422" type="appl" />
         <tli id="T90" time="76.321" type="appl" />
         <tli id="T91" time="77.219" type="appl" />
         <tli id="T92" time="78.37967324385708" />
         <tli id="T93" time="79.085" type="appl" />
         <tli id="T94" time="80.053" type="appl" />
         <tli id="T95" time="81.3729940983686" />
         <tli id="T96" time="82.209" type="appl" />
         <tli id="T97" time="83.398" type="appl" />
         <tli id="T98" time="84.587" type="appl" />
         <tli id="T99" time="85.775" type="appl" />
         <tli id="T100" time="86.964" type="appl" />
         <tli id="T101" time="88.153" type="appl" />
         <tli id="T102" time="89.34899864002969" />
         <tli id="T103" time="90.26" type="appl" />
         <tli id="T104" time="91.179" type="appl" />
         <tli id="T105" time="92.097" type="appl" />
         <tli id="T106" time="93.016" type="appl" />
         <tli id="T107" time="93.934" type="appl" />
         <tli id="T108" time="94.853" type="appl" />
         <tli id="T109" time="95.77799916740021" />
         <tli id="T110" time="96.366" type="appl" />
         <tli id="T111" time="96.961" type="appl" />
         <tli id="T112" time="97.557" type="appl" />
         <tli id="T113" time="98.152" type="appl" />
         <tli id="T114" time="98.7539970063433" />
         <tli id="T115" time="99.384" type="appl" />
         <tli id="T116" time="100.02" type="appl" />
         <tli id="T117" time="100.76624658338861" />
         <tli id="T118" time="101.398" type="appl" />
         <tli id="T119" time="102.139" type="appl" />
         <tli id="T120" time="102.881" type="appl" />
         <tli id="T121" time="103.622" type="appl" />
         <tli id="T122" time="104.36999954364269" />
         <tli id="T123" time="105.067" type="appl" />
         <tli id="T124" time="105.771" type="appl" />
         <tli id="T125" time="106.475" type="appl" />
         <tli id="T126" time="107.178" type="appl" />
         <tli id="T127" time="107.882" type="appl" />
         <tli id="T128" time="108.586" type="appl" />
         <tli id="T129" time="109.29700115251984" />
         <tli id="T130" time="109.944" type="appl" />
         <tli id="T131" time="110.60500301425522" />
         <tli id="T132" time="111.203" type="appl" />
         <tli id="T133" time="111.792" type="appl" />
         <tli id="T134" time="112.38" type="appl" />
         <tli id="T135" time="112.969" type="appl" />
         <tli id="T136" time="113.56399987782366" />
         <tli id="T137" time="114.16" type="appl" />
         <tli id="T138" time="114.763" type="appl" />
         <tli id="T139" time="115.367" type="appl" />
         <tli id="T140" time="115.97" type="appl" />
         <tli id="T141" time="116.56600162119985" />
         <tli id="T142" time="117.358" type="appl" />
         <tli id="T143" time="118.143" type="appl" />
         <tli id="T144" time="118.557" type="appl" />
         <tli id="T145" time="118.97" type="appl" />
         <tli id="T146" time="119.384" type="appl" />
         <tli id="T147" time="119.798" type="appl" />
         <tli id="T148" time="120.211" type="appl" />
         <tli id="T149" time="120.625" type="appl" />
         <tli id="T150" time="121.038" type="appl" />
         <tli id="T151" time="122.19949056391089" />
         <tli id="T152" time="123.345" type="appl" />
         <tli id="T153" time="124.35281492027441" />
         <tli id="T154" time="125.017" type="appl" />
         <tli id="T155" time="125.81" type="appl" />
         <tli id="T156" time="126.604" type="appl" />
         <tli id="T157" time="127.397" type="appl" />
         <tli id="T158" time="127.984" type="appl" />
         <tli id="T159" time="128.572" type="appl" />
         <tli id="T160" time="129.16" type="appl" />
         <tli id="T161" time="129.747" type="appl" />
         <tli id="T162" time="130.334" type="appl" />
         <tli id="T163" time="131.8461170148645" />
         <tli id="T164" time="132.95" type="appl" />
         <tli id="T165" time="133.907" type="appl" />
         <tli id="T166" time="134.864" type="appl" />
         <tli id="T167" time="135.821" type="appl" />
         <tli id="T168" time="136.778" type="appl" />
         <tli id="T169" time="138.0994242788551" />
         <tli id="T170" time="138.952" type="appl" />
         <tli id="T171" time="139.768" type="appl" />
         <tli id="T172" time="140.585" type="appl" />
         <tli id="T173" time="141.401" type="appl" />
         <tli id="T174" time="143.15940318436566" />
         <tli id="T175" time="143.987" type="appl" />
         <tli id="T176" time="144.757" type="appl" />
         <tli id="T177" time="145.53999764367205" />
         <tli id="T178" time="146.251" type="appl" />
         <tli id="T179" time="146.974" type="appl" />
         <tli id="T180" time="147.696" type="appl" />
         <tli id="T181" time="148.419" type="appl" />
         <tli id="T182" time="149.15500520359276" />
         <tli id="T183" time="149.519" type="appl" />
         <tli id="T184" time="149.897" type="appl" />
         <tli id="T185" time="150.274" type="appl" />
         <tli id="T186" time="150.652" type="appl" />
         <tli id="T187" time="151.03699335334892" />
         <tli id="T188" time="151.698" type="appl" />
         <tli id="T189" time="152.217" type="appl" />
         <tli id="T190" time="152.74199909359575" />
         <tli id="T191" time="153.656" type="appl" />
         <tli id="T192" time="154.576" type="appl" />
         <tli id="T193" time="155.497" type="appl" />
         <tli id="T194" time="156.418" type="appl" />
         <tli id="T195" time="157.338" type="appl" />
         <tli id="T196" time="158.259" type="appl" />
         <tli id="T197" time="159.18" type="appl" />
         <tli id="T198" time="160.0929966268592" />
         <tli id="T199" time="161.73265908786024" />
         <tli id="T200" time="162.691" type="appl" />
         <tli id="T201" time="163.613" type="appl" />
         <tli id="T202" time="164.534" type="appl" />
         <tli id="T203" time="165.456" type="appl" />
         <tli id="T204" time="166.5726389105225" />
         <tli id="T205" time="167.393" type="appl" />
         <tli id="T206" time="168.333" type="appl" />
         <tli id="T207" time="169.274" type="appl" />
         <tli id="T208" time="170.091" type="appl" />
         <tli id="T209" time="170.745" type="appl" />
         <tli id="T210" time="171.398" type="appl" />
         <tli id="T211" time="172.052" type="appl" />
         <tli id="T212" time="173.39261047881934" />
         <tli id="T213" time="174.604" type="appl" />
         <tli id="T214" time="175.73" type="appl" />
         <tli id="T215" time="176.856" type="appl" />
         <tli id="T216" time="180.872579295661" />
         <tli id="T217" time="181.431" type="appl" />
         <tli id="T218" time="182.478" type="appl" />
         <tli id="T219" time="183.526" type="appl" />
         <tli id="T220" time="184.032" type="appl" />
         <tli id="T221" time="184.399" type="appl" />
         <tli id="T222" time="184.765" type="appl" />
         <tli id="T223" time="185.132" type="appl" />
         <tli id="T224" time="185.499" type="appl" />
         <tli id="T225" time="186.189" type="appl" />
         <tli id="T226" time="187.032553615413" />
         <tli id="T227" time="187.836" type="appl" />
         <tli id="T228" time="188.545" type="appl" />
         <tli id="T229" time="189.254" type="appl" />
         <tli id="T230" time="189.962" type="appl" />
         <tli id="T231" time="190.671" type="appl" />
         <tli id="T232" time="191.87253343807524" />
         <tli id="T233" time="192.258" type="appl" />
         <tli id="T234" time="193.137" type="appl" />
         <tli id="T235" time="194.02899425576308" />
         <tli id="T236" time="194.879" type="appl" />
         <tli id="T237" time="195.559" type="appl" />
         <tli id="T238" time="196.167" type="appl" />
         <tli id="T239" time="196.776" type="appl" />
         <tli id="T240" time="197.384" type="appl" />
         <tli id="T241" time="198.251" type="appl" />
         <tli id="T242" time="199.119" type="appl" />
         <tli id="T243" time="199.986" type="appl" />
         <tli id="T244" time="200.597" type="appl" />
         <tli id="T245" time="201.207" type="appl" />
         <tli id="T246" time="201.818" type="appl" />
         <tli id="T247" time="202.429" type="appl" />
         <tli id="T248" time="203.039" type="appl" />
         <tli id="T249" time="203.65" type="appl" />
         <tli id="T250" time="204.26" type="appl" />
         <tli id="T251" time="204.871" type="appl" />
         <tli id="T252" time="205.482" type="appl" />
         <tli id="T253" time="206.092" type="appl" />
         <tli id="T254" time="206.703" type="appl" />
         <tli id="T255" time="207.233" type="appl" />
         <tli id="T256" time="207.764" type="appl" />
         <tli id="T257" time="208.294" type="appl" />
         <tli id="T258" time="209.314" type="appl" />
         <tli id="T259" time="210.335" type="appl" />
         <tli id="T260" time="211.356" type="appl" />
         <tli id="T261" time="212.376" type="appl" />
         <tli id="T262" time="212.819" type="appl" />
         <tli id="T263" time="213.263" type="appl" />
         <tli id="T264" time="213.706" type="appl" />
         <tli id="T265" time="214.149" type="appl" />
         <tli id="T266" time="214.593" type="appl" />
         <tli id="T267" time="216.27909835648651" />
         <tli id="T268" time="216.66204917824325" type="intp" />
         <tli id="T269" time="217.045" type="appl" />
         <tli id="T270" time="218.97242046165942" />
         <tli id="T271" time="219.707" type="appl" />
         <tli id="T272" time="220.427" type="appl" />
         <tli id="T273" time="221.366" type="appl" />
         <tli id="T274" time="222.306" type="appl" />
         <tli id="T275" time="223.245" type="appl" />
         <tli id="T276" time="224.184" type="appl" />
         <tli id="T277" time="225.123" type="appl" />
         <tli id="T278" time="226.062" type="appl" />
         <tli id="T279" time="227.002" type="appl" />
         <tli id="T280" time="228.29238160777768" />
         <tli id="T281" time="228.829" type="appl" />
         <tli id="T282" time="229.718" type="appl" />
         <tli id="T283" time="230.606" type="appl" />
         <tli id="T284" time="231.065" type="appl" />
         <tli id="T285" time="231.524" type="appl" />
         <tli id="T286" time="231.984" type="appl" />
         <tli id="T287" time="232.443" type="appl" />
         <tli id="T288" time="233.071" type="appl" />
         <tli id="T289" time="233.699" type="appl" />
         <tli id="T290" time="234.71902148249725" />
         <tli id="T291" time="235.43" type="appl" />
         <tli id="T292" time="236.044" type="appl" />
         <tli id="T293" time="236.659" type="appl" />
         <tli id="T294" time="237.273" type="appl" />
         <tli id="T295" time="238.21900689144724" />
         <tli id="T296" time="239.046" type="appl" />
         <tli id="T297" time="239.806" type="appl" />
         <tli id="T298" time="240.565" type="appl" />
         <tli id="T299" time="241.324" type="appl" />
         <tli id="T300" time="242.084" type="appl" />
         <tli id="T301" time="242.843" type="appl" />
         <tli id="T302" time="243.602" type="appl" />
         <tli id="T303" time="244.362" type="appl" />
         <tli id="T304" time="245.121" type="appl" />
         <tli id="T305" time="245.592" type="appl" />
         <tli id="T306" time="246.064" type="appl" />
         <tli id="T307" time="246.56563876193368" />
         <tli id="T308" time="246.867" type="appl" />
         <tli id="T309" time="247.199" type="appl" />
         <tli id="T310" time="247.53" type="appl" />
         <tli id="T311" time="247.862" type="appl" />
         <tli id="T312" time="248.194" type="appl" />
         <tli id="T313" time="248.63" type="appl" />
         <tli id="T314" time="249.065" type="appl" />
         <tli id="T315" time="249.528" type="appl" />
         <tli id="T316" time="249.99" type="appl" />
         <tli id="T317" time="250.69" type="appl" />
         <tli id="T318" time="251.391" type="appl" />
         <tli id="T319" time="252.152" type="appl" />
         <tli id="T320" time="252.913" type="appl" />
         <tli id="T321" time="253.674" type="appl" />
         <tli id="T322" time="254.326" type="appl" />
         <tli id="T323" time="254.979" type="appl" />
         <tli id="T324" time="255.631" type="appl" />
         <tli id="T325" time="256.284" type="appl" />
         <tli id="T326" time="256.936" type="appl" />
         <tli id="T327" time="257.589" type="appl" />
         <tli id="T328" time="259.0255868177956" />
         <tli id="T329" time="259.817" type="appl" />
         <tli id="T330" time="261.393" type="appl" />
         <tli id="T331" time="262.969" type="appl" />
         <tli id="T332" time="265.372227026025" />
         <tli id="T333" time="266.237" type="appl" />
         <tli id="T334" time="267.001" type="appl" />
         <tli id="T335" time="267.921" type="appl" />
         <tli id="T336" time="268.718" type="appl" />
         <tli id="T337" time="269.516" type="appl" />
         <tli id="T338" time="270.4188726537871" />
         <tli id="T339" time="271.365" type="appl" />
         <tli id="T340" time="272.49219734366034" />
         <tli id="T341" time="273.155" type="appl" />
         <tli id="T342" time="273.764" type="appl" />
         <tli id="T343" time="274.372" type="appl" />
         <tli id="T344" time="275.01885347697856" />
         <tli id="T345" time="275.629" type="appl" />
         <tli id="T346" time="276.277" type="appl" />
         <tli id="T347" time="276.925" type="appl" />
         <tli id="T348" time="277.574" type="appl" />
         <tli id="T349" time="278.36550619182213" />
         <tli id="T350" time="279.309" type="appl" />
         <tli id="T351" time="280.4" type="appl" />
         <tli id="T352" time="281.49" type="appl" />
         <tli id="T353" time="282.58" type="appl" />
         <tli id="T354" time="283.671" type="appl" />
         <tli id="T355" time="284.761" type="appl" />
         <tli id="T356" time="285.724" type="appl" />
         <tli id="T357" time="286.635" type="appl" />
         <tli id="T358" time="287.39" type="appl" />
         <tli id="T359" time="288.145" type="appl" />
         <tli id="T360" time="288.9" type="appl" />
         <tli id="T361" time="289.655" type="appl" />
         <tli id="T362" time="290.83212088655824" />
         <tli id="T363" time="291.428" type="appl" />
         <tli id="T364" time="292.446" type="appl" />
         <tli id="T365" time="293.465" type="appl" />
         <tli id="T366" time="294.483" type="appl" />
         <tli id="T367" time="295.501" type="appl" />
         <tli id="T368" time="296.56" type="appl" />
         <tli id="T369" time="297.618" type="appl" />
         <tli id="T370" time="299.6187509227413" />
         <tli id="T371" time="300.217" type="appl" />
         <tli id="T372" time="300.617" type="appl" />
         <tli id="T373" time="301.018" type="appl" />
         <tli id="T374" time="301.419" type="appl" />
         <tli id="T375" time="301.82" type="appl" />
         <tli id="T376" time="302.22" type="appl" />
         <tli id="T377" time="302.621" type="appl" />
         <tli id="T378" time="303.449" type="appl" />
         <tli id="T379" time="304.277" type="appl" />
         <tli id="T380" time="305.142" type="appl" />
         <tli id="T381" time="306.006" type="appl" />
         <tli id="T382" time="306.871" type="appl" />
         <tli id="T383" time="307.736" type="appl" />
         <tli id="T384" time="308.6" type="appl" />
         <tli id="T385" time="309.465" type="appl" />
         <tli id="T386" time="310.231" type="appl" />
         <tli id="T387" time="310.997" type="appl" />
         <tli id="T388" time="311.763" type="appl" />
         <tli id="T389" time="312.25" type="appl" />
         <tli id="T390" time="312.737" type="appl" />
         <tli id="T391" time="313.224" type="appl" />
         <tli id="T392" time="313.711" type="appl" />
         <tli id="T393" time="314.198" type="appl" />
         <tli id="T394" time="315.53201791543387" />
         <tli id="T395" time="316.618" type="appl" />
         <tli id="T396" time="317.576" type="appl" />
         <tli id="T397" time="318.534" type="appl" />
         <tli id="T398" time="319.491" type="appl" />
         <tli id="T399" time="320.448" type="appl" />
         <tli id="T400" time="321.406" type="appl" />
         <tli id="T401" time="322.224" type="appl" />
         <tli id="T402" time="323.042" type="appl" />
         <tli id="T403" time="323.86" type="appl" />
         <tli id="T404" time="324.323" type="appl" />
         <tli id="T405" time="324.786" type="appl" />
         <tli id="T406" time="325.249" type="appl" />
         <tli id="T407" time="325.712" type="appl" />
         <tli id="T408" time="326.175" type="appl" />
         <tli id="T409" time="327.1186362787769" />
         <tli id="T410" time="328.101" type="appl" />
         <tli id="T411" time="328.873" type="appl" />
         <tli id="T412" time="329.9719577169304" />
         <tli id="T413" time="331.346" type="appl" />
         <tli id="T414" time="332.85" type="appl" />
         <tli id="T415" time="333.351" type="appl" />
         <tli id="T416" time="333.852" type="appl" />
         <tli id="T417" time="334.353" type="appl" />
         <tli id="T418" time="334.854" type="appl" />
         <tli id="T419" time="335.355" type="appl" />
         <tli id="T420" time="335.856" type="appl" />
         <tli id="T421" time="336.357" type="appl" />
         <tli id="T422" time="336.858" type="appl" />
         <tli id="T423" time="337.359" type="appl" />
         <tli id="T424" time="337.86" type="appl" />
         <tli id="T425" time="338.361" type="appl" />
         <tli id="T426" time="338.862" type="appl" />
         <tli id="T427" time="339.72525038987095" />
         <tli id="T428" time="340.136" type="appl" />
         <tli id="T429" time="340.515" type="appl" />
         <tli id="T430" time="340.895" type="appl" />
         <tli id="T431" time="341.275" type="appl" />
         <tli id="T432" time="341.654" type="appl" />
         <tli id="T433" time="342.034" type="appl" />
         <tli id="T434" time="342.569" type="appl" />
         <tli id="T435" time="343.104" type="appl" />
         <tli id="T436" time="343.638" type="appl" />
         <tli id="T437" time="344.173" type="appl" />
         <tli id="T438" time="344.998" type="appl" />
         <tli id="T439" time="345.823" type="appl" />
         <tli id="T440" time="346.45" type="appl" />
         <tli id="T441" time="346.989" type="appl" />
         <tli id="T442" time="348.5985467314185" />
         <tli id="T443" time="349.962" type="appl" />
         <tli id="T444" time="351.196" type="appl" />
         <tli id="T445" time="352.429" type="appl" />
         <tli id="T446" time="353.663" type="appl" />
         <tli id="T447" time="354.896" type="appl" />
         <tli id="T448" time="356.8651789354146" />
         <tli id="T449" time="357.56" type="appl" />
         <tli id="T450" time="358.224" type="appl" />
         <tli id="T451" time="358.888" type="appl" />
         <tli id="T452" time="359.552" type="appl" />
         <tli id="T453" time="360.216" type="appl" />
         <tli id="T454" time="360.9" type="appl" />
         <tli id="T455" time="361.584" type="appl" />
         <tli id="T456" time="362.7318211447022" />
         <tli id="T457" time="363.941" type="appl" />
         <tli id="T458" time="365.7384752769621" />
         <tli id="T459" time="366.425" type="appl" />
         <tli id="T460" time="367.093" type="appl" />
         <tli id="T461" time="367.761" type="appl" />
         <tli id="T462" time="368.429" type="appl" />
         <tli id="T463" time="369.06" type="appl" />
         <tli id="T464" time="369.9584576843246" />
         <tli id="T465" time="370.52" type="appl" />
         <tli id="T466" time="371.349" type="appl" />
         <tli id="T467" time="372.177" type="appl" />
         <tli id="T468" time="373.005" type="appl" />
         <tli id="T469" time="373.833" type="appl" />
         <tli id="T470" time="374.662" type="appl" />
         <tli id="T471" time="375.49" type="appl" />
         <tli id="T472" time="376.318" type="appl" />
         <tli id="T473" time="377.146" type="appl" />
         <tli id="T474" time="377.975" type="appl" />
         <tli id="T475" time="378.803" type="appl" />
         <tli id="T476" time="379.571" type="appl" />
         <tli id="T477" time="380.338" type="appl" />
         <tli id="T478" time="381.8450747970062" />
         <tli id="T479" time="382.663" type="appl" />
         <tli id="T480" time="383.48" type="appl" />
         <tli id="T481" time="384.298" type="appl" />
         <tli id="T482" time="385.115" type="appl" />
         <tli id="T483" time="385.933" type="appl" />
         <tli id="T484" time="386.75" type="appl" />
         <tli id="T485" time="387.568" type="appl" />
         <tli id="T486" time="388.385" type="appl" />
         <tli id="T487" time="389.58504252994135" />
         <tli id="T488" time="390.622" type="appl" />
         <tli id="T489" time="392.31169782948524" />
         <tli id="T490" time="392.976" type="appl" />
         <tli id="T491" time="393.787" type="appl" />
         <tli id="T492" time="394.597" type="appl" />
         <tli id="T493" time="395.408" type="appl" />
         <tli id="T494" time="396.218" type="appl" />
         <tli id="T495" time="397.029" type="appl" />
         <tli id="T496" time="397.839" type="appl" />
         <tli id="T497" time="398.65" type="appl" />
         <tli id="T498" time="400.13833186778487" />
         <tli id="T499" time="401.463" type="appl" />
         <tli id="T500" time="402.705" type="appl" />
         <tli id="T501" time="404.3916474695184" />
         <tli id="T502" time="405.114" type="appl" />
         <tli id="T503" time="405.687" type="appl" />
         <tli id="T504" time="406.261" type="appl" />
         <tli id="T505" time="406.834" type="appl" />
         <tli id="T506" time="407.408" type="appl" />
         <tli id="T507" time="407.981" type="appl" />
         <tli id="T508" time="408.555" type="appl" />
         <tli id="T509" time="409.848" type="appl" />
         <tli id="T510" time="410.942" type="appl" />
         <tli id="T511" time="412.3516142853017" />
         <tli id="T512" time="412.982" type="appl" />
         <tli id="T513" time="413.771" type="appl" />
         <tli id="T514" time="415.19160244570685" />
         <tli id="T515" time="415.935" type="appl" />
         <tli id="T516" time="416.614" type="appl" />
         <tli id="T517" time="417.292" type="appl" />
         <tli id="T518" time="417.971" type="appl" />
         <tli id="T519" time="418.75158760452456" />
         <tli id="T520" time="419.625" type="appl" />
         <tli id="T521" time="420.363" type="appl" />
         <tli id="T522" time="421.1" type="appl" />
         <tli id="T523" time="421.838" type="appl" />
         <tli id="T524" time="423.3849016220869" />
         <tli id="T525" time="424.287" type="appl" />
         <tli id="T526" time="425.15" type="appl" />
         <tli id="T527" time="426.013" type="appl" />
         <tli id="T528" time="426.876" type="appl" />
         <tli id="T529" time="427.739" type="appl" />
         <tli id="T530" time="428.602" type="appl" />
         <tli id="T531" time="429.465" type="appl" />
         <tli id="T532" time="430.328" type="appl" />
         <tli id="T533" time="431.7515334091959" />
         <tli id="T534" time="432.383" type="appl" />
         <tli id="T535" time="432.939" type="appl" />
         <tli id="T536" time="433.495" type="appl" />
         <tli id="T537" time="434.051" type="appl" />
         <tli id="T538" time="434.606" type="appl" />
         <tli id="T539" time="435.162" type="appl" />
         <tli id="T540" time="435.718" type="appl" />
         <tli id="T541" time="436.274" type="appl" />
         <tli id="T542" time="437.0515113141774" />
         <tli id="T543" time="437.79" type="appl" />
         <tli id="T544" time="438.384" type="appl" />
         <tli id="T545" time="438.978" type="appl" />
         <tli id="T546" time="439.573" type="appl" />
         <tli id="T547" time="440.168" type="appl" />
         <tli id="T548" time="440.762" type="appl" />
         <tli id="T549" time="441.356" type="appl" />
         <tli id="T550" time="441.951" type="appl" />
         <tli id="T551" time="442.546" type="appl" />
         <tli id="T552" time="443.14" type="appl" />
         <tli id="T553" time="444.096" type="appl" />
         <tli id="T554" time="445.051" type="appl" />
         <tli id="T555" time="446.007" type="appl" />
         <tli id="T556" time="446.962" type="appl" />
         <tli id="T557" time="447.918" type="appl" />
         <tli id="T558" time="448.873" type="appl" />
         <tli id="T559" time="449.829" type="appl" />
         <tli id="T560" time="450.749" type="appl" />
         <tli id="T561" time="451.669" type="appl" />
         <tli id="T562" time="452.588" type="appl" />
         <tli id="T563" time="453.508" type="appl" />
         <tli id="T564" time="454.428" type="appl" />
         <tli id="T565" time="455.8247663838976" />
         <tli id="T566" time="456.837" type="appl" />
         <tli id="T567" time="457.618" type="appl" />
         <tli id="T568" time="458.398" type="appl" />
         <tli id="T569" time="459.179" type="appl" />
         <tli id="T570" time="459.959" type="appl" />
         <tli id="T571" time="460.74" type="appl" />
         <tli id="T572" time="461.52" type="appl" />
         <tli id="T573" time="462.301" type="appl" />
         <tli id="T574" time="463.081" type="appl" />
         <tli id="T575" time="463.8" type="appl" />
         <tli id="T576" time="464.334" type="appl" />
         <tli id="T577" time="464.867" type="appl" />
         <tli id="T578" time="465.78472486193823" />
         <tli id="T579" time="466.394" type="appl" />
         <tli id="T580" time="466.995" type="appl" />
         <tli id="T581" time="467.596" type="appl" />
         <tli id="T582" time="468.198" type="appl" />
         <tli id="T583" time="468.799" type="appl" />
         <tli id="T584" time="469.4" type="appl" />
         <tli id="T585" time="470.001" type="appl" />
         <tli id="T586" time="471.069" type="appl" />
         <tli id="T587" time="471.976" type="appl" />
         <tli id="T588" time="472.884" type="appl" />
         <tli id="T589" time="473.792" type="appl" />
         <tli id="T590" time="474.703" type="appl" />
         <tli id="T591" time="475.468" type="appl" />
         <tli id="T592" time="476.234" type="appl" />
         <tli id="T593" time="476.999" type="appl" />
         <tli id="T594" time="477.765" type="appl" />
         <tli id="T595" time="478.607" type="appl" />
         <tli id="T596" time="479.238" type="appl" />
         <tli id="T597" time="479.869" type="appl" />
         <tli id="T598" time="480.499" type="appl" />
         <tli id="T599" time="481.13" type="appl" />
         <tli id="T600" time="482.35798910317556" />
         <tli id="T601" time="483.075" type="appl" />
         <tli id="T602" time="483.696" type="appl" />
         <tli id="T603" time="484.316" type="appl" />
         <tli id="T604" time="484.936" type="appl" />
         <tli id="T605" time="485.667" type="appl" />
         <tli id="T606" time="486.398" type="appl" />
         <tli id="T607" time="487.4246346475603" />
         <tli id="T608" time="488.57" type="appl" />
         <tli id="T609" time="489.628" type="appl" />
         <tli id="T610" time="490.685" type="appl" />
         <tli id="T611" time="491.743" type="appl" />
         <tli id="T612" time="493.11127760724486" />
         <tli id="T613" time="495.0379362418858" />
         <tli id="T614" time="496.124" type="appl" />
         <tli id="T615" time="497.27" type="appl" />
         <tli id="T616" time="498.4769844048765" />
         <tli id="T617" time="499.157" type="appl" />
         <tli id="T618" time="499.896" type="appl" />
         <tli id="T619" time="501.1712440061411" />
         <tli id="T620" time="501.631" type="appl" />
         <tli id="T621" time="502.044" type="appl" />
         <tli id="T622" time="502.456" type="appl" />
         <tli id="T623" time="502.868" type="appl" />
         <tli id="T624" time="503.735" type="appl" />
         <tli id="T625" time="504.441" type="appl" />
         <tli id="T626" time="505.9445574400615" />
         <tli id="T627" time="506.755" type="appl" />
         <tli id="T628" time="507.412" type="appl" />
         <tli id="T629" time="508.45788029563124" />
         <tli id="T630" time="509.341" type="appl" />
         <tli id="T631" time="510.294" type="appl" />
         <tli id="T632" time="511.247" type="appl" />
         <tli id="T633" time="513.104527590942" />
         <tli id="T634" time="513.784" type="appl" />
         <tli id="T635" time="514.475" type="appl" />
         <tli id="T636" time="515.166" type="appl" />
         <tli id="T637" time="515.857" type="appl" />
         <tli id="T638" time="516.8245120827403" />
         <tli id="T639" time="517.357" type="appl" />
         <tli id="T640" time="517.924" type="appl" />
         <tli id="T641" time="518.8711702171167" />
         <tli id="T642" time="520.321" type="appl" />
         <tli id="T643" time="521.791" type="appl" />
         <tli id="T644" time="523.5378174290499" />
         <tli id="T645" time="524.416" type="appl" />
         <tli id="T646" time="525.003" type="appl" />
         <tli id="T647" time="525.59" type="appl" />
         <tli id="T648" time="526.177" type="appl" />
         <tli id="T649" time="526.764" type="appl" />
         <tli id="T650" time="527.728" type="appl" />
         <tli id="T651" time="528.468" type="appl" />
         <tli id="T652" time="529.209" type="appl" />
         <tli id="T653" time="529.949" type="appl" />
         <tli id="T654" time="530.689" type="appl" />
         <tli id="T655" time="531.429" type="appl" />
         <tli id="T656" time="532.169" type="appl" />
         <tli id="T657" time="532.91" type="appl" />
         <tli id="T658" time="533.65" type="appl" />
         <tli id="T659" time="535.1311024312671" />
         <tli id="T660" time="535.8444327908055" type="intp" />
         <tli id="T661" time="536.5577631503439" type="intp" />
         <tli id="T662" time="537.2710935098823" />
         <tli id="T663" time="537.7780467549412" type="intp" />
         <tli id="T664" time="538.285" type="appl" />
         <tli id="T665" time="539.177" type="appl" />
         <tli id="T666" time="539.905" type="appl" />
         <tli id="T667" time="540.633" type="appl" />
         <tli id="T668" time="541.36" type="appl" />
         <tli id="T669" time="542.088" type="appl" />
         <tli id="T670" time="542.816" type="appl" />
         <tli id="T671" time="543.544" type="appl" />
         <tli id="T672" time="544.02" type="appl" />
         <tli id="T673" time="544.497" type="appl" />
         <tli id="T674" time="545.099" type="appl" />
         <tli id="T675" time="545.701" type="appl" />
         <tli id="T676" time="547.4910509040162" />
         <tli id="T677" time="548.602" type="appl" />
         <tli id="T678" time="549.9577072874667" />
         <tli id="T679" time="550.991" type="appl" />
         <tli id="T680" time="551.788" type="appl" />
         <tli id="T681" time="552.585" type="appl" />
         <tli id="T682" time="553.383" type="appl" />
         <tli id="T683" time="554.18" type="appl" />
         <tli id="T684" time="554.977" type="appl" />
         <tli id="T685" time="555.938" type="appl" />
         <tli id="T686" time="556.697" type="appl" />
         <tli id="T687" time="557.757" type="appl" />
         <tli id="T688" time="558.809" type="appl" />
         <tli id="T689" time="559.862" type="appl" />
         <tli id="T690" time="560.914" type="appl" />
         <tli id="T691" time="561.967" type="appl" />
         <tli id="T692" time="562.653" type="appl" />
         <tli id="T693" time="563.238" type="appl" />
         <tli id="T694" time="563.776" type="appl" />
         <tli id="T695" time="564.8309786157856" />
         <tli id="T696" time="565.668" type="appl" />
         <tli id="T697" time="566.287" type="appl" />
         <tli id="T698" time="566.906" type="appl" />
         <tli id="T699" time="567.526" type="appl" />
         <tli id="T700" time="568.145" type="appl" />
         <tli id="T701" time="568.764" type="appl" />
         <tli id="T702" time="569.6576251606995" />
         <tli id="T703" time="570.736" type="appl" />
         <tli id="T704" time="571.9909487666662" />
         <tli id="T705" time="573.172" type="appl" />
         <tli id="T706" time="574.36" type="appl" />
         <tli id="T707" time="575.549" type="appl" />
         <tli id="T708" time="576.737" type="appl" />
         <tli id="T709" time="578.3709221692664" />
         <tli id="T710" time="579.282" type="appl" />
         <tli id="T711" time="580.071" type="appl" />
         <tli id="T712" time="580.86" type="appl" />
         <tli id="T713" time="581.648" type="appl" />
         <tli id="T714" time="582.436" type="appl" />
         <tli id="T715" time="583.225" type="appl" />
         <tli id="T716" time="583.72" type="appl" />
         <tli id="T717" time="584.214" type="appl" />
         <tli id="T718" time="584.916" type="appl" />
         <tli id="T719" time="585.402" type="appl" />
         <tli id="T720" time="585.888" type="appl" />
         <tli id="T721" time="586.44" type="appl" />
         <tli id="T722" time="586.993" type="appl" />
         <tli id="T723" time="587.504" type="appl" />
         <tli id="T724" time="589.1642105065807" />
         <tli id="T725" time="590.065" type="appl" />
         <tli id="T726" time="591.082" type="appl" />
         <tli id="T727" time="592.324197332947" />
         <tli id="T728" time="593.145" type="appl" />
         <tli id="T729" time="593.978" type="appl" />
         <tli id="T730" time="594.811" type="appl" />
         <tli id="T731" time="595.644" type="appl" />
         <tli id="T732" time="596.477" type="appl" />
         <tli id="T733" time="597.64" type="appl" />
         <tli id="T734" time="598.619" type="appl" />
         <tli id="T735" time="599.598" type="appl" />
         <tli id="T736" time="600.577" type="appl" />
         <tli id="T737" time="601.128" type="appl" />
         <tli id="T738" time="601.53" type="appl" />
         <tli id="T739" time="602.138" type="appl" />
         <tli id="T740" time="602.745" type="appl" />
         <tli id="T741" time="603.352" type="appl" />
         <tli id="T742" time="604.4708133617221" />
         <tli id="T743" time="604.995" type="appl" />
         <tli id="T744" time="605.502" type="appl" />
         <tli id="T745" time="606.01" type="appl" />
         <tli id="T746" time="606.555" type="appl" />
         <tli id="T747" time="607.101" type="appl" />
         <tli id="T748" time="607.646" type="appl" />
         <tli id="T749" time="608.191" type="appl" />
         <tli id="T750" time="608.737" type="appl" />
         <tli id="T751" time="609.9841237106775" />
         <tli id="T752" time="611.054" type="appl" />
         <tli id="T753" time="611.922" type="appl" />
         <tli id="T754" time="612.79" type="appl" />
         <tli id="T755" time="613.658" type="appl" />
         <tli id="T756" time="614.526" type="appl" />
         <tli id="T757" time="615.395" type="appl" />
         <tli id="T758" time="616.263" type="appl" />
         <tli id="T759" time="617.131" type="appl" />
         <tli id="T760" time="617.999" type="appl" />
         <tli id="T761" time="618.867" type="appl" />
         <tli id="T762" time="619.659" type="appl" />
         <tli id="T763" time="620.451" type="appl" />
         <tli id="T764" time="621.243" type="appl" />
         <tli id="T765" time="622.035" type="appl" />
         <tli id="T766" time="622.781" type="appl" />
         <tli id="T767" time="623.527" type="appl" />
         <tli id="T768" time="624.365" type="appl" />
         <tli id="T769" time="625.141" type="appl" />
         <tli id="T770" time="625.918" type="appl" />
         <tli id="T771" time="626.694" type="appl" />
         <tli id="T772" time="627.47" type="appl" />
         <tli id="T773" time="628.021" type="appl" />
         <tli id="T774" time="628.571" type="appl" />
         <tli id="T775" time="629.122" type="appl" />
         <tli id="T776" time="629.672" type="appl" />
         <tli id="T777" time="630.265" type="appl" />
         <tli id="T778" time="630.859" type="appl" />
         <tli id="T779" time="631.452" type="appl" />
         <tli id="T780" time="632.045" type="appl" />
         <tli id="T781" time="632.639" type="appl" />
         <tli id="T782" time="633.232" type="appl" />
         <tli id="T783" time="634.815" type="appl" />
         <tli id="T784" time="638.6040043975771" />
         <tli id="T785" time="639.39" type="appl" />
         <tli id="T786" time="640.064" type="appl" />
         <tli id="T787" time="640.739" type="appl" />
         <tli id="T788" time="641.414" type="appl" />
         <tli id="T789" time="641.981" type="appl" />
         <tli id="T790" time="642.548" type="appl" />
         <tli id="T791" time="643.115" type="appl" />
         <tli id="T792" time="643.685" type="appl" />
         <tli id="T793" time="644.255" type="appl" />
         <tli id="T794" time="644.824" type="appl" />
         <tli id="T795" time="645.394" type="appl" />
         <tli id="T796" time="646.69063735197" />
         <tli id="T797" time="647.343" type="appl" />
         <tli id="T798" time="647.893" type="appl" />
         <tli id="T799" time="648.443" type="appl" />
         <tli id="T800" time="648.993" type="appl" />
         <tli id="T801" time="649.543" type="appl" />
         <tli id="T802" time="650.171" type="appl" />
         <tli id="T803" time="650.8" type="appl" />
         <tli id="T804" time="651.7839494518516" />
         <tli id="T805" time="652.492" type="appl" />
         <tli id="T806" time="653.174" type="appl" />
         <tli id="T807" time="653.857" type="appl" />
         <tli id="T808" time="654.539" type="appl" />
         <tli id="T809" time="655.428" type="appl" />
         <tli id="T810" time="656.134" type="appl" />
         <tli id="T811" time="656.84" type="appl" />
         <tli id="T812" time="657.9772569659746" />
         <tli id="T813" time="658.892" type="appl" />
         <tli id="T814" time="659.63" type="appl" />
         <tli id="T815" time="660.368" type="appl" />
         <tli id="T816" time="661.107" type="appl" />
         <tli id="T817" time="661.845" type="appl" />
         <tli id="T818" time="662.583" type="appl" />
         <tli id="T819" time="663.7838994253943" />
         <tli id="T820" time="664.965" type="appl" />
         <tli id="T821" time="666.077" type="appl" />
         <tli id="T822" time="667.189" type="appl" />
         <tli id="T823" time="668.255" type="appl" />
         <tli id="T824" time="669.32" type="appl" />
         <tli id="T825" time="670.386" type="appl" />
         <tli id="T826" time="671.452" type="appl" />
         <tli id="T827" time="672.532" type="appl" />
         <tli id="T828" time="673.484" type="appl" />
         <tli id="T829" time="674.554" type="appl" />
         <tli id="T830" time="675.623" type="appl" />
         <tli id="T831" time="676.692" type="appl" />
         <tli id="T832" time="677.762" type="appl" />
         <tli id="T833" time="679.367" type="appl" />
         <tli id="T834" time="680.973" type="appl" />
         <tli id="T835" time="682.578" type="appl" />
         <tli id="T836" time="683.797" type="appl" />
         <tli id="T837" time="684.866" type="appl" />
         <tli id="T838" time="685.936" type="appl" />
         <tli id="T839" time="687.005" type="appl" />
         <tli id="T840" time="688.075" type="appl" />
         <tli id="T841" time="689.223" type="appl" />
         <tli id="T842" time="690.371" type="appl" />
         <tli id="T843" time="691.126" type="appl" />
         <tli id="T844" time="691.88" type="appl" />
         <tli id="T845" time="692.635" type="appl" />
         <tli id="T846" time="693.389" type="appl" />
         <tli id="T847" time="694.6037709407769" />
         <tli id="T848" time="695.721" type="appl" />
         <tli id="T849" time="696.703" type="appl" />
         <tli id="T850" time="697.686" type="appl" />
         <tli id="T851" time="700.3370803725807" />
         <tli id="T852" time="701.282" type="appl" />
         <tli id="T853" time="701.835" type="appl" />
         <tli id="T854" time="702.388" type="appl" />
         <tli id="T855" time="702.927642489482" />
         <tli id="T856" time="703.521" type="appl" />
         <tli id="T857" time="704.101" type="appl" />
         <tli id="T858" time="704.681" type="appl" />
         <tli id="T859" time="705.7237245829265" />
         <tli id="T860" time="707.13" type="appl" />
         <tli id="T861" time="708.7037121596896" />
         <tli id="T862" time="709.812" type="appl" />
         <tli id="T863" time="710.769" type="appl" />
         <tli id="T864" time="711.725" type="appl" />
         <tli id="T865" time="712.575" type="appl" />
         <tli id="T866" time="713.377" type="appl" />
         <tli id="T867" time="714.179" type="appl" />
         <tli id="T868" time="715.532" type="appl" />
         <tli id="T869" time="716.878" type="appl" />
         <tli id="T870" time="718.225" type="appl" />
         <tli id="T871" time="718.805" type="appl" />
         <tli id="T872" time="719.386" type="appl" />
         <tli id="T873" time="719.966" type="appl" />
         <tli id="T874" time="720.502" type="appl" />
         <tli id="T875" time="721.039" type="appl" />
         <tli id="T876" time="722.1569894078061" />
         <tli id="T877" time="723.235" type="appl" />
         <tli id="T878" time="724.087" type="appl" />
         <tli id="T879" time="724.938" type="appl" />
         <tli id="T880" time="725.39" type="appl" />
         <tli id="T881" time="725.841" type="appl" />
         <tli id="T882" time="726.443" type="appl" />
         <tli id="T883" time="727.046" type="appl" />
         <tli id="T884" time="728.3036304498096" />
         <tli id="T885" time="729.108" type="appl" />
         <tli id="T886" time="729.817" type="appl" />
         <tli id="T887" time="730.525" type="appl" />
         <tli id="T888" time="731.233" type="appl" />
         <tli id="T889" time="731.942" type="appl" />
         <tli id="T890" time="732.65" type="appl" />
         <tli id="T891" time="733.358" type="appl" />
         <tli id="T892" time="734.067" type="appl" />
         <tli id="T893" time="734.775" type="appl" />
         <tli id="T894" time="736.09" type="appl" />
         <tli id="T895" time="737.369" type="appl" />
         <tli id="T896" time="738.647" type="appl" />
         <tli id="T897" time="739.926" type="appl" />
         <tli id="T898" time="741.204" type="appl" />
         <tli id="T899" time="742.036" type="appl" />
         <tli id="T900" time="742.9969025285253" />
         <tli id="T901" time="743.72" type="appl" />
         <tli id="T902" time="744.622" type="appl" />
         <tli id="T903" time="745.524" type="appl" />
         <tli id="T904" time="746.9835525752912" />
         <tli id="T905" time="747.927" type="appl" />
         <tli id="T906" time="748.642" type="appl" />
         <tli id="T907" time="749.357" type="appl" />
         <tli id="T908" time="750.072" type="appl" />
         <tli id="T909" time="750.787" type="appl" />
         <tli id="T910" time="751.502" type="appl" />
         <tli id="T911" time="752.217" type="appl" />
         <tli id="T912" time="752.926" type="appl" />
         <tli id="T913" time="753.475" type="appl" />
         <tli id="T914" time="754.024" type="appl" />
         <tli id="T915" time="754.943" type="appl" />
         <tli id="T916" time="755.447" type="appl" />
         <tli id="T917" time="755.95" type="appl" />
         <tli id="T918" time="757.166" type="appl" />
         <tli id="T919" time="758.383" type="appl" />
         <tli id="T920" time="759.6" type="appl" />
         <tli id="T921" time="760.816" type="appl" />
         <tli id="T922" time="761.569" type="appl" />
         <tli id="T923" time="762.4768213189099" />
         <tli id="T924" time="763.264" type="appl" />
         <tli id="T925" time="765.1101436742149" />
         <tli id="T926" time="765.812" type="appl" />
         <tli id="T927" time="766.277" type="appl" />
         <tli id="T928" time="766.742" type="appl" />
         <tli id="T929" time="767.207" type="appl" />
         <tli id="T930" time="767.672" type="appl" />
         <tli id="T931" time="768.137" type="appl" />
         <tli id="T932" time="768.602" type="appl" />
         <tli id="T933" time="769.067" type="appl" />
         <tli id="T934" time="769.532" type="appl" />
         <tli id="T935" time="769.997" type="appl" />
         <tli id="T936" time="771.1767850497283" />
         <tli id="T937" time="772.527" type="appl" />
         <tli id="T938" time="773.635" type="appl" />
         <tli id="T939" time="774.743" type="appl" />
         <tli id="T940" time="776.3767633715969" />
         <tli id="T941" time="777.783" type="appl" />
         <tli id="T942" time="778.819" type="appl" />
         <tli id="T943" time="779.855" type="appl" />
         <tli id="T944" time="780.891" type="appl" />
         <tli id="T945" time="781.927" type="appl" />
         <tli id="T946" time="782.963" type="appl" />
         <tli id="T947" time="783.999" type="appl" />
         <tli id="T948" time="785.3100594630121" />
         <tli id="T949" time="786.16" type="appl" />
         <tli id="T950" time="786.836" type="appl" />
         <tli id="T951" time="787.513" type="appl" />
         <tli id="T952" time="788.19" type="appl" />
         <tli id="T953" time="788.866" type="appl" />
         <tli id="T954" time="789.543" type="appl" />
         <tli id="T955" time="790.22" type="appl" />
         <tli id="T956" time="790.897" type="appl" />
         <tli id="T957" time="791.573" type="appl" />
         <tli id="T958" time="792.516696086012" />
         <tli id="T959" time="793.319" type="appl" />
         <tli id="T960" time="794.027" type="appl" />
         <tli id="T961" time="794.735" type="appl" />
         <tli id="T962" time="795.442" type="appl" />
         <tli id="T963" time="796.15" type="appl" />
         <tli id="T964" time="796.858" type="appl" />
         <tli id="T965" time="798.0900061848353" />
         <tli id="T966" time="798.462" type="appl" />
         <tli id="T967" time="799.359" type="appl" />
         <tli id="T968" time="800.4833295406696" />
         <tli id="T969" time="801.212" type="appl" />
         <tli id="T970" time="801.855" type="appl" />
         <tli id="T971" time="802.499" type="appl" />
         <tli id="T972" time="803.142" type="appl" />
         <tli id="T973" time="803.786" type="appl" />
         <tli id="T974" time="804.43" type="appl" />
         <tli id="T975" time="805.073" type="appl" />
         <tli id="T976" time="805.717" type="appl" />
         <tli id="T977" time="806.897" type="appl" />
         <tli id="T978" time="808.077" type="appl" />
         <tli id="T979" time="809.257" type="appl" />
         <tli id="T980" time="810.438" type="appl" />
         <tli id="T981" time="811.618" type="appl" />
         <tli id="T982" time="812.798" type="appl" />
         <tli id="T983" time="813.978" type="appl" />
         <tli id="T984" time="815.158" type="appl" />
         <tli id="T985" time="815.734" type="appl" />
         <tli id="T986" time="816.311" type="appl" />
         <tli id="T987" time="816.962" type="appl" />
         <tli id="T988" time="817.612" type="appl" />
         <tli id="T989" time="818.147" type="appl" />
         <tli id="T990" time="818.682" type="appl" />
         <tli id="T991" time="819.217" type="appl" />
         <tli id="T992" time="820.3432467468829" />
         <tli id="T993" time="820.978" type="appl" />
         <tli id="T994" time="821.589" type="appl" />
         <tli id="T995" time="822.201" type="appl" />
         <tli id="T996" time="822.812" type="appl" />
         <tli id="T997" time="823.423" type="appl" />
         <tli id="T998" time="823.962" type="appl" />
         <tli id="T999" time="824.5" type="appl" />
         <tli id="T1000" time="825.039" type="appl" />
         <tli id="T1001" time="825.578" type="appl" />
         <tli id="T1002" time="826.303" type="appl" />
         <tli id="T1003" time="827.027" type="appl" />
         <tli id="T1004" time="828.3898798680308" />
         <tli id="T1005" time="829.528" type="appl" />
         <tli id="T1006" time="830.432" type="appl" />
         <tli id="T1007" time="831.399" type="appl" />
         <tli id="T1008" time="832.19" type="appl" />
         <tli id="T1009" time="832.981" type="appl" />
         <tli id="T1010" time="833.772" type="appl" />
         <tli id="T1011" time="834.563" type="appl" />
         <tli id="T1012" time="835.545" type="appl" />
         <tli id="T1013" time="836.502" type="appl" />
         <tli id="T1014" time="837.46" type="appl" />
         <tli id="T1015" time="837.846" type="appl" />
         <tli id="T1016" time="838.232" type="appl" />
         <tli id="T1017" time="838.832" type="appl" />
         <tli id="T1018" time="839.432" type="appl" />
         <tli id="T1019" time="840.414" type="appl" />
         <tli id="T1020" time="841.26" type="appl" />
         <tli id="T1021" time="842.105" type="appl" />
         <tli id="T1022" time="842.95" type="appl" />
         <tli id="T1023" time="843.927" type="appl" />
         <tli id="T1024" time="844.801" type="appl" />
         <tli id="T1025" time="845.455" type="appl" />
         <tli id="T1026" time="846.108" type="appl" />
         <tli id="T1027" time="846.762" type="appl" />
         <tli id="T1028" time="847.415" type="appl" />
         <tli id="T1029" time="848.244" type="appl" />
         <tli id="T1030" time="849.073" type="appl" />
         <tli id="T1031" time="849.743" type="appl" />
         <tli id="T1032" time="850.9897856515363" />
         <tli id="T1033" time="851.075" type="appl" />
         <tli id="T1034" time="851.739" type="appl" />
         <tli id="T1035" time="852.402" type="appl" />
         <tli id="T1036" time="853.066" type="appl" />
         <tli id="T1037" time="853.729" type="appl" />
         <tli id="T1038" time="854.435" type="appl" />
         <tli id="T1039" time="855.14" type="appl" />
         <tli id="T1040" time="856.3230967508888" />
         <tli id="T1041" time="858.6030872458618" />
         <tli id="T1042" time="859.76" type="appl" />
         <tli id="T1043" time="860.7964114354704" />
         <tli id="T1044" time="862.382" type="appl" />
         <tli id="T1045" time="863.878" type="appl" />
         <tli id="T1046" time="864.404" type="appl" />
         <tli id="T1047" time="864.93" type="appl" />
         <tli id="T1048" time="865.683057730252" />
         <tli id="T1049" time="866.29" type="appl" />
         <tli id="T1050" time="866.931" type="appl" />
         <tli id="T1051" time="867.572" type="appl" />
         <tli id="T1052" time="868.004" type="appl" />
         <tli id="T1053" time="868.436" type="appl" />
         <tli id="T1054" time="869.214" type="appl" />
         <tli id="T1055" time="869.991" type="appl" />
         <tli id="T1056" time="870.768" type="appl" />
         <tli id="T1057" time="871.546" type="appl" />
         <tli id="T1058" time="872.208" type="appl" />
         <tli id="T1059" time="872.677" type="appl" />
         <tli id="T1060" time="873.368" type="appl" />
         <tli id="T1061" time="874.058" type="appl" />
         <tli id="T1062" time="875.215" type="appl" />
         <tli id="T1063" time="876.0363479018699" />
         <tli id="T1064" time="877.2096763437274" />
         <tli id="T1065" time="878.185" type="appl" />
         <tli id="T1066" time="878.943" type="appl" />
         <tli id="T1067" time="879.701" type="appl" />
         <tli id="T1068" time="880.335" type="appl" />
         <tli id="T1069" time="880.97" type="appl" />
         <tli id="T1070" time="882.1096559162573" />
         <tli id="T1071" time="882.757" type="appl" />
         <tli id="T1072" time="883.271" type="appl" />
         <tli id="T1073" time="883.997" type="appl" />
         <tli id="T1074" time="884.722" type="appl" />
         <tli id="T1075" time="886.1629723517651" />
         <tli id="T1076" time="887.313" type="appl" />
         <tli id="T1077" time="888.419" type="appl" />
         <tli id="T1078" time="889.524" type="appl" />
         <tli id="T1079" time="890.191" type="appl" />
         <tli id="T1080" time="890.857" type="appl" />
         <tli id="T1081" time="891.524" type="appl" />
         <tli id="T1082" time="892.042" type="appl" />
         <tli id="T1083" time="892.561" type="appl" />
         <tli id="T1084" time="893.079" type="appl" />
         <tli id="T1085" time="893.598" type="appl" />
         <tli id="T1086" time="894.116" type="appl" />
         <tli id="T1088" time="894.522" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-PKZ"
                      id="tx-PKZ"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T3" start="T0">
            <tli id="T0.tx-PKZ.1" />
            <tli id="T0.tx-PKZ.2" />
         </timeline-fork>
         <timeline-fork end="T632" start="T629">
            <tli id="T629.tx-PKZ.1" />
            <tli id="T629.tx-PKZ.2" />
            <tli id="T629.tx-PKZ.3" />
         </timeline-fork>
         <timeline-fork end="T633" start="T632">
            <tli id="T632.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T798" start="T797">
            <tli id="T797.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T838" start="T837">
            <tli id="T837.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T993" start="T992">
            <tli id="T992.tx-PKZ.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PKZ">
            <ts e="T3" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T0.tx-PKZ.1" id="Seg_4" n="HIAT:w" s="T0">Ты</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-PKZ.2" id="Seg_7" n="HIAT:w" s="T0.tx-PKZ.1">уж</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T0.tx-PKZ.2">отпустил</ts>
                  <nts id="Seg_11" n="HIAT:ip">?</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1086" id="Seg_13" n="sc" s="T4">
               <ts e="T10" id="Seg_15" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_17" n="HIAT:w" s="T4">Amnobi</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">büzʼe</ts>
                  <nts id="Seg_21" n="HIAT:ip">,</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_24" n="HIAT:w" s="T6">dĭn</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_27" n="HIAT:w" s="T7">nagur</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_30" n="HIAT:w" s="T8">nʼi</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_33" n="HIAT:w" s="T9">ibi</ts>
                  <nts id="Seg_34" n="HIAT:ip">.</nts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_37" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_39" n="HIAT:w" s="T10">Onʼiʔ</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">Gavrila</ts>
                  <nts id="Seg_43" n="HIAT:ip">,</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">onʼiʔ</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">Danʼila</ts>
                  <nts id="Seg_50" n="HIAT:ip">,</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">onʼiʔ</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">Vanʼuška</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">duračok</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_63" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">Dĭgəttə</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_67" n="HIAT:ip">(</nts>
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">dĭz-</ts>
                  <nts id="Seg_70" n="HIAT:ip">)</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">dĭ</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">büzʼe</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">kuʔlaʔpi</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">budəj</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_86" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">I</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">dĭ</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">šindidə</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_96" n="HIAT:ip">(</nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">ka-</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">n-</ts>
                  <nts id="Seg_102" n="HIAT:ip">)</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_105" n="HIAT:w" s="T28">nüdʼin</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_108" n="HIAT:w" s="T29">šoluʔjəʔ</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_111" n="HIAT:w" s="T30">da</ts>
                  <nts id="Seg_112" n="HIAT:ip">,</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_115" n="HIAT:w" s="T31">amnaʔbə</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_118" n="HIAT:w" s="T32">budəj</ts>
                  <nts id="Seg_119" n="HIAT:ip">.</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_122" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_124" n="HIAT:w" s="T33">Nada</ts>
                  <nts id="Seg_125" n="HIAT:ip">,</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_128" n="HIAT:w" s="T34">măndə</ts>
                  <nts id="Seg_129" n="HIAT:ip">,</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_132" n="HIAT:w" s="T35">kanzittə</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_134" n="HIAT:ip">(</nts>
                  <ts e="T37" id="Seg_136" n="HIAT:w" s="T36">mandər-</ts>
                  <nts id="Seg_137" n="HIAT:ip">)</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_140" n="HIAT:w" s="T37">mandəsʼtə</ts>
                  <nts id="Seg_141" n="HIAT:ip">,</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_144" n="HIAT:w" s="T38">šində</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_147" n="HIAT:w" s="T39">amnʼiat</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_150" n="HIAT:w" s="T40">budəj</ts>
                  <nts id="Seg_151" n="HIAT:ip">.</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_154" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_156" n="HIAT:w" s="T41">Nada</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_159" n="HIAT:w" s="T42">dʼabəsʼtə</ts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_163" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_165" n="HIAT:w" s="T43">Dĭgəttə</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_168" n="HIAT:w" s="T44">kambi</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_171" n="HIAT:w" s="T45">staršij</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_174" n="HIAT:w" s="T46">nʼit</ts>
                  <nts id="Seg_175" n="HIAT:ip">,</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_178" n="HIAT:w" s="T47">Gavrila</ts>
                  <nts id="Seg_179" n="HIAT:ip">,</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_182" n="HIAT:w" s="T48">dibər</ts>
                  <nts id="Seg_183" n="HIAT:ip">.</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_186" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_188" n="HIAT:w" s="T49">Kambi</ts>
                  <nts id="Seg_189" n="HIAT:ip">,</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_192" n="HIAT:w" s="T50">kunolbi</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_195" n="HIAT:w" s="T51">noʔgən</ts>
                  <nts id="Seg_196" n="HIAT:ip">,</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_199" n="HIAT:w" s="T52">i</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_202" n="HIAT:w" s="T53">šobi</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_205" n="HIAT:w" s="T54">maʔnə</ts>
                  <nts id="Seg_206" n="HIAT:ip">.</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_209" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_211" n="HIAT:w" s="T55">Amnobiam</ts>
                  <nts id="Seg_212" n="HIAT:ip">,</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_215" n="HIAT:w" s="T56">ej</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_218" n="HIAT:w" s="T57">kunolbiam</ts>
                  <nts id="Seg_219" n="HIAT:ip">,</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_222" n="HIAT:w" s="T58">a</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_225" n="HIAT:w" s="T59">šindidə</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_228" n="HIAT:w" s="T60">ej</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_231" n="HIAT:w" s="T61">kubiam</ts>
                  <nts id="Seg_232" n="HIAT:ip">.</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_235" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_237" n="HIAT:w" s="T62">Dĭgəttə</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_240" n="HIAT:w" s="T63">baška</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_243" n="HIAT:w" s="T64">nʼit</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_246" n="HIAT:w" s="T65">kandəga</ts>
                  <nts id="Seg_247" n="HIAT:ip">,</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_250" n="HIAT:w" s="T66">Danʼilat</ts>
                  <nts id="Seg_251" n="HIAT:ip">.</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_254" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_256" n="HIAT:w" s="T67">Kambi</ts>
                  <nts id="Seg_257" n="HIAT:ip">,</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_260" n="HIAT:w" s="T68">kunolbi</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_263" n="HIAT:w" s="T69">noʔgən</ts>
                  <nts id="Seg_264" n="HIAT:ip">.</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_267" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_269" n="HIAT:w" s="T70">Dĭgəttə</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_272" n="HIAT:w" s="T71">ertən</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_275" n="HIAT:w" s="T72">šobi</ts>
                  <nts id="Seg_276" n="HIAT:ip">.</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_279" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_281" n="HIAT:w" s="T73">Bar</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_284" n="HIAT:w" s="T74">nudʼi</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_287" n="HIAT:w" s="T75">ej</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_290" n="HIAT:w" s="T76">kunolbiam</ts>
                  <nts id="Seg_291" n="HIAT:ip">,</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_294" n="HIAT:w" s="T77">a</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_297" n="HIAT:w" s="T78">šinimdə</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_300" n="HIAT:w" s="T79">ej</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_303" n="HIAT:w" s="T80">kubiom</ts>
                  <nts id="Seg_304" n="HIAT:ip">.</nts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_307" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_309" n="HIAT:w" s="T81">Dĭgəttə</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_312" n="HIAT:w" s="T82">Vanʼuška</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_315" n="HIAT:w" s="T83">kambi</ts>
                  <nts id="Seg_316" n="HIAT:ip">.</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_319" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_321" n="HIAT:w" s="T84">Ibi</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_324" n="HIAT:w" s="T85">arkandə</ts>
                  <nts id="Seg_325" n="HIAT:ip">.</nts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_328" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_330" n="HIAT:w" s="T86">Dĭgəttə</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_333" n="HIAT:w" s="T87">amnobi</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_336" n="HIAT:w" s="T88">pinə</ts>
                  <nts id="Seg_337" n="HIAT:ip">.</nts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_340" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_342" n="HIAT:w" s="T89">Ej</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_345" n="HIAT:w" s="T90">kunolia</ts>
                  <nts id="Seg_346" n="HIAT:ip">,</nts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_349" n="HIAT:w" s="T91">amnolaʔbə</ts>
                  <nts id="Seg_350" n="HIAT:ip">.</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_353" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_355" n="HIAT:w" s="T92">Kuliat:</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_358" n="HIAT:w" s="T93">ine</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_361" n="HIAT:w" s="T94">šobi</ts>
                  <nts id="Seg_362" n="HIAT:ip">.</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_365" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_367" n="HIAT:w" s="T95">Vsʼaka</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_370" n="HIAT:w" s="T96">i</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_373" n="HIAT:w" s="T97">bar</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_376" n="HIAT:w" s="T98">dĭn</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_379" n="HIAT:w" s="T99">kubangən</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_382" n="HIAT:w" s="T100">tordə</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_385" n="HIAT:w" s="T101">kuvas</ts>
                  <nts id="Seg_386" n="HIAT:ip">.</nts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_389" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_391" n="HIAT:w" s="T102">Dĭ</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_394" n="HIAT:w" s="T103">bar</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_397" n="HIAT:w" s="T104">baʔluʔpi</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_400" n="HIAT:w" s="T105">arkandə</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_403" n="HIAT:w" s="T106">dĭʔnə</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_406" n="HIAT:w" s="T107">i</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_409" n="HIAT:w" s="T108">dʼabəluʔpi</ts>
                  <nts id="Seg_410" n="HIAT:ip">.</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_413" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_415" n="HIAT:w" s="T109">Dĭ</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_418" n="HIAT:w" s="T110">suʔmiluʔpi</ts>
                  <nts id="Seg_419" n="HIAT:ip">,</nts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_422" n="HIAT:w" s="T111">ej</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_425" n="HIAT:w" s="T112">molia</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_428" n="HIAT:w" s="T113">suʔmisʼtə</ts>
                  <nts id="Seg_429" n="HIAT:ip">.</nts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_432" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_434" n="HIAT:w" s="T114">Dĭgəttə:</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_437" n="HIAT:w" s="T115">Öʔləʔ</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_440" n="HIAT:w" s="T116">măna</ts>
                  <nts id="Seg_441" n="HIAT:ip">!</nts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_444" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_446" n="HIAT:w" s="T117">Măn</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_449" n="HIAT:w" s="T118">tănan</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_452" n="HIAT:w" s="T119">bar</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_455" n="HIAT:w" s="T120">ĭmbi</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_458" n="HIAT:w" s="T121">mĭləm</ts>
                  <nts id="Seg_459" n="HIAT:ip">.</nts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_462" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_464" n="HIAT:w" s="T122">No</ts>
                  <nts id="Seg_465" n="HIAT:ip">,</nts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_468" n="HIAT:w" s="T123">kădə</ts>
                  <nts id="Seg_469" n="HIAT:ip">,</nts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_472" n="HIAT:w" s="T124">nörbəʔ</ts>
                  <nts id="Seg_473" n="HIAT:ip">,</nts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_476" n="HIAT:w" s="T125">daška</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_479" n="HIAT:w" s="T126">ej</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_482" n="HIAT:w" s="T127">amnial</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_485" n="HIAT:w" s="T128">budəj</ts>
                  <nts id="Seg_486" n="HIAT:ip">?</nts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_489" n="HIAT:u" s="T129">
                  <ts e="T130" id="Seg_491" n="HIAT:w" s="T129">Ej</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_494" n="HIAT:w" s="T130">amnam</ts>
                  <nts id="Seg_495" n="HIAT:ip">!</nts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_498" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_500" n="HIAT:w" s="T131">A</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_503" n="HIAT:w" s="T132">kădə</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_506" n="HIAT:w" s="T133">šoləl</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_509" n="HIAT:w" s="T134">dărə</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_512" n="HIAT:w" s="T135">măna</ts>
                  <nts id="Seg_513" n="HIAT:ip">?</nts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_516" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_518" n="HIAT:w" s="T136">Šoʔ</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_521" n="HIAT:w" s="T137">döbər</ts>
                  <nts id="Seg_522" n="HIAT:ip">,</nts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_524" n="HIAT:ip">(</nts>
                  <ts e="T139" id="Seg_526" n="HIAT:w" s="T138">ənuʔ</ts>
                  <nts id="Seg_527" n="HIAT:ip">)</nts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_529" n="HIAT:ip">(</nts>
                  <ts e="T140" id="Seg_531" n="HIAT:w" s="T139">măn-</ts>
                  <nts id="Seg_532" n="HIAT:ip">)</nts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_535" n="HIAT:w" s="T140">măna</ts>
                  <nts id="Seg_536" n="HIAT:ip">.</nts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_539" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_541" n="HIAT:w" s="T141">No</ts>
                  <nts id="Seg_542" n="HIAT:ip">,</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_545" n="HIAT:w" s="T142">dĭrgit</ts>
                  <nts id="Seg_546" n="HIAT:ip">.</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_549" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_551" n="HIAT:w" s="T143">I</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_554" n="HIAT:w" s="T144">kalla</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_557" n="HIAT:w" s="T145">dʼürbi</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_560" n="HIAT:w" s="T146">inet</ts>
                  <nts id="Seg_561" n="HIAT:ip">,</nts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_564" n="HIAT:w" s="T147">a</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_567" n="HIAT:w" s="T148">dĭ</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_570" n="HIAT:w" s="T149">šobi</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_573" n="HIAT:w" s="T150">maːndə</ts>
                  <nts id="Seg_574" n="HIAT:ip">.</nts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_577" n="HIAT:u" s="T151">
                  <ts e="T152" id="Seg_579" n="HIAT:w" s="T151">Pʼešdə</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_581" n="HIAT:ip">(</nts>
                  <ts e="T153" id="Seg_583" n="HIAT:w" s="T152">šerluʔpi</ts>
                  <nts id="Seg_584" n="HIAT:ip">)</nts>
                  <nts id="Seg_585" n="HIAT:ip">.</nts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_588" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_590" n="HIAT:w" s="T153">Măn</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_593" n="HIAT:w" s="T154">dʼabəbiam</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_596" n="HIAT:w" s="T155">ine</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_599" n="HIAT:w" s="T156">dĭn</ts>
                  <nts id="Seg_600" n="HIAT:ip">.</nts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_603" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_605" n="HIAT:w" s="T157">Tuj</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_608" n="HIAT:w" s="T158">kaməndə</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_611" n="HIAT:w" s="T159">ej</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_614" n="HIAT:w" s="T160">šoləj</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_617" n="HIAT:w" s="T161">amzittə</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_620" n="HIAT:w" s="T162">budəj</ts>
                  <nts id="Seg_621" n="HIAT:ip">.</nts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T169" id="Seg_624" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_626" n="HIAT:w" s="T163">Dĭgəttə</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_629" n="HIAT:w" s="T164">koŋ</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_632" n="HIAT:w" s="T165">abi</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_635" n="HIAT:w" s="T166">štobɨ</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_638" n="HIAT:w" s="T167">il</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_641" n="HIAT:w" s="T168">šobiʔi</ts>
                  <nts id="Seg_642" n="HIAT:ip">.</nts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T174" id="Seg_645" n="HIAT:u" s="T169">
                  <nts id="Seg_646" n="HIAT:ip">(</nts>
                  <ts e="T170" id="Seg_648" n="HIAT:w" s="T169">Dĭ-</ts>
                  <nts id="Seg_649" n="HIAT:ip">)</nts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_652" n="HIAT:w" s="T170">Dĭn</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_655" n="HIAT:w" s="T171">koʔbdo</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_658" n="HIAT:w" s="T172">amnolaʔbə</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_661" n="HIAT:w" s="T173">tʼerebgən</ts>
                  <nts id="Seg_662" n="HIAT:ip">.</nts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_665" n="HIAT:u" s="T174">
                  <ts e="T175" id="Seg_667" n="HIAT:w" s="T174">Bar</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_670" n="HIAT:w" s="T175">il</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_673" n="HIAT:w" s="T176">kallaʔbəʔjə</ts>
                  <nts id="Seg_674" n="HIAT:ip">.</nts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_677" n="HIAT:u" s="T177">
                  <nts id="Seg_678" n="HIAT:ip">(</nts>
                  <ts e="T178" id="Seg_680" n="HIAT:w" s="T177">I=</ts>
                  <nts id="Seg_681" n="HIAT:ip">)</nts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_684" n="HIAT:w" s="T178">I</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_687" n="HIAT:w" s="T179">dĭn</ts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_690" n="HIAT:w" s="T180">kagaʔi</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_693" n="HIAT:w" s="T181">kandəgaʔi</ts>
                  <nts id="Seg_694" n="HIAT:ip">.</nts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T187" id="Seg_697" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_699" n="HIAT:w" s="T182">A</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_702" n="HIAT:w" s="T183">dĭ</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_705" n="HIAT:w" s="T184">măndə:</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_708" n="HIAT:w" s="T185">igəʔ</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_711" n="HIAT:w" s="T186">măna</ts>
                  <nts id="Seg_712" n="HIAT:ip">!</nts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_715" n="HIAT:u" s="T187">
                  <ts e="T188" id="Seg_717" n="HIAT:w" s="T187">Gibər</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_720" n="HIAT:w" s="T188">tăn</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_723" n="HIAT:w" s="T189">kallal</ts>
                  <nts id="Seg_724" n="HIAT:ip">?</nts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T199" id="Seg_727" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_729" n="HIAT:w" s="T190">Ĭmbidə</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_732" n="HIAT:w" s="T191">dĭn</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_735" n="HIAT:w" s="T192">ej</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_738" n="HIAT:w" s="T193">kubial</ts>
                  <nts id="Seg_739" n="HIAT:ip">,</nts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_742" n="HIAT:w" s="T194">štobɨ</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_745" n="HIAT:w" s="T195">il</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_748" n="HIAT:w" s="T196">püšterbiʔi</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_750" n="HIAT:ip">(</nts>
                  <ts e="T198" id="Seg_752" n="HIAT:w" s="T197">tăn-</ts>
                  <nts id="Seg_753" n="HIAT:ip">)</nts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_756" n="HIAT:w" s="T198">tănan</ts>
                  <nts id="Seg_757" n="HIAT:ip">.</nts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T204" id="Seg_760" n="HIAT:u" s="T199">
                  <ts e="T200" id="Seg_762" n="HIAT:w" s="T199">Dĭzeŋ</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_765" n="HIAT:w" s="T200">kalla</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_768" n="HIAT:w" s="T201">dʼürbiʔi</ts>
                  <nts id="Seg_769" n="HIAT:ip">,</nts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_772" n="HIAT:w" s="T202">dĭ</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_775" n="HIAT:w" s="T203">uʔbdəbi</ts>
                  <nts id="Seg_776" n="HIAT:ip">.</nts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_779" n="HIAT:u" s="T204">
                  <ts e="T205" id="Seg_781" n="HIAT:w" s="T204">Karzʼina</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_784" n="HIAT:w" s="T205">ibi</ts>
                  <nts id="Seg_785" n="HIAT:ip">,</nts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_788" n="HIAT:w" s="T206">kambi</ts>
                  <nts id="Seg_789" n="HIAT:ip">.</nts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_792" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_794" n="HIAT:w" s="T207">I</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_797" n="HIAT:w" s="T208">davaj</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_800" n="HIAT:w" s="T209">kirgarzittə</ts>
                  <nts id="Seg_801" n="HIAT:ip">,</nts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_804" n="HIAT:w" s="T210">inet</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_807" n="HIAT:w" s="T211">šobi</ts>
                  <nts id="Seg_808" n="HIAT:ip">.</nts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T216" id="Seg_811" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_813" n="HIAT:w" s="T212">Bar</ts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_816" n="HIAT:w" s="T213">döbər</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_818" n="HIAT:ip">(</nts>
                  <ts e="T215" id="Seg_820" n="HIAT:w" s="T214">modriagən</ts>
                  <nts id="Seg_821" n="HIAT:ip">)</nts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_824" n="HIAT:w" s="T215">šonugaʔi</ts>
                  <nts id="Seg_825" n="HIAT:ip">.</nts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T219" id="Seg_828" n="HIAT:u" s="T216">
                  <ts e="T217" id="Seg_830" n="HIAT:w" s="T216">Šö</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_833" n="HIAT:w" s="T217">kuzandə</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_836" n="HIAT:w" s="T218">šonuga</ts>
                  <nts id="Seg_837" n="HIAT:ip">.</nts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_840" n="HIAT:u" s="T219">
                  <ts e="T220" id="Seg_842" n="HIAT:w" s="T219">Dĭgəttə</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_845" n="HIAT:w" s="T220">dĭ</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_848" n="HIAT:w" s="T221">ine</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_851" n="HIAT:w" s="T222">măndə</ts>
                  <nts id="Seg_852" n="HIAT:ip">…</nts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_855" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_857" n="HIAT:w" s="T224">Nubi</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_860" n="HIAT:w" s="T225">dĭʔnə</ts>
                  <nts id="Seg_861" n="HIAT:ip">.</nts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T232" id="Seg_864" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_866" n="HIAT:w" s="T226">Măndə:</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_869" n="HIAT:w" s="T227">onʼiʔ</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_872" n="HIAT:w" s="T228">kunə</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_875" n="HIAT:w" s="T229">paʔkaʔ</ts>
                  <nts id="Seg_876" n="HIAT:ip">,</nts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_879" n="HIAT:w" s="T230">onʼiʔgəʔ</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_882" n="HIAT:w" s="T231">supsoʔ</ts>
                  <nts id="Seg_883" n="HIAT:ip">.</nts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T235" id="Seg_886" n="HIAT:u" s="T232">
                  <ts e="T233" id="Seg_888" n="HIAT:w" s="T232">Dĭ</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_891" n="HIAT:w" s="T233">paʔpi</ts>
                  <nts id="Seg_892" n="HIAT:ip">,</nts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_895" n="HIAT:w" s="T234">supsobi</ts>
                  <nts id="Seg_896" n="HIAT:ip">.</nts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T237" id="Seg_899" n="HIAT:u" s="T235">
                  <ts e="T236" id="Seg_901" n="HIAT:w" s="T235">Kuvas</ts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_904" n="HIAT:w" s="T236">molaːmbi</ts>
                  <nts id="Seg_905" n="HIAT:ip">.</nts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T240" id="Seg_908" n="HIAT:u" s="T237">
                  <ts e="T238" id="Seg_910" n="HIAT:w" s="T237">Dĭgəttə</ts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_913" n="HIAT:w" s="T238">dĭʔnə</ts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_916" n="HIAT:w" s="T239">amnobi</ts>
                  <nts id="Seg_917" n="HIAT:ip">.</nts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_920" n="HIAT:u" s="T240">
                  <ts e="T241" id="Seg_922" n="HIAT:w" s="T240">Dĭ</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_924" n="HIAT:ip">(</nts>
                  <ts e="T242" id="Seg_926" n="HIAT:w" s="T241">nuʔmə=</ts>
                  <nts id="Seg_927" n="HIAT:ip">)</nts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_930" n="HIAT:w" s="T242">nuʔməluʔpi</ts>
                  <nts id="Seg_931" n="HIAT:ip">.</nts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T254" id="Seg_934" n="HIAT:u" s="T243">
                  <ts e="T244" id="Seg_936" n="HIAT:w" s="T243">Dibər</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_939" n="HIAT:w" s="T244">il</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_942" n="HIAT:w" s="T245">iʔgö</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_945" n="HIAT:w" s="T246">nugaʔi</ts>
                  <nts id="Seg_946" n="HIAT:ip">,</nts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_949" n="HIAT:w" s="T247">a</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_952" n="HIAT:w" s="T248">šindidə</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_955" n="HIAT:w" s="T249">ej</ts>
                  <nts id="Seg_956" n="HIAT:ip">,</nts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_959" n="HIAT:w" s="T250">dĭ</ts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_962" n="HIAT:w" s="T251">koʔbdonə</ts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_965" n="HIAT:w" s="T252">ej</ts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_968" n="HIAT:w" s="T253">sʼaliaʔi</ts>
                  <nts id="Seg_969" n="HIAT:ip">.</nts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T257" id="Seg_972" n="HIAT:u" s="T254">
                  <ts e="T255" id="Seg_974" n="HIAT:w" s="T254">Dĭ</ts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_977" n="HIAT:w" s="T255">bar</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_980" n="HIAT:w" s="T256">suʔmiluʔpi</ts>
                  <nts id="Seg_981" n="HIAT:ip">.</nts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T261" id="Seg_984" n="HIAT:u" s="T257">
                  <ts e="T258" id="Seg_986" n="HIAT:w" s="T257">Šalaʔjə</ts>
                  <nts id="Seg_987" n="HIAT:ip">,</nts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_990" n="HIAT:w" s="T258">ej</ts>
                  <nts id="Seg_991" n="HIAT:ip">,</nts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_994" n="HIAT:w" s="T259">ej</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_997" n="HIAT:w" s="T260">dʼapi</ts>
                  <nts id="Seg_998" n="HIAT:ip">.</nts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T267" id="Seg_1001" n="HIAT:u" s="T261">
                  <ts e="T262" id="Seg_1003" n="HIAT:w" s="T261">Dĭgəttə</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1006" n="HIAT:w" s="T262">il</ts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1009" n="HIAT:w" s="T263">bar</ts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1012" n="HIAT:w" s="T264">kirgarlaʔbəʔjə:</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1015" n="HIAT:w" s="T265">Dʼabəʔ</ts>
                  <nts id="Seg_1016" n="HIAT:ip">,</nts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1019" n="HIAT:w" s="T266">dʼabəʔ</ts>
                  <nts id="Seg_1020" n="HIAT:ip">.</nts>
                  <nts id="Seg_1021" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T270" id="Seg_1023" n="HIAT:u" s="T267">
                  <ts e="T268" id="Seg_1025" n="HIAT:w" s="T267">A</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1028" n="HIAT:w" s="T268">dĭ</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1031" n="HIAT:w" s="T269">nuʔməluʔpi</ts>
                  <nts id="Seg_1032" n="HIAT:ip">.</nts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T272" id="Seg_1035" n="HIAT:u" s="T270">
                  <ts e="T271" id="Seg_1037" n="HIAT:w" s="T270">Inem</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1040" n="HIAT:w" s="T271">öʔlubi</ts>
                  <nts id="Seg_1041" n="HIAT:ip">.</nts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T280" id="Seg_1044" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_1046" n="HIAT:w" s="T272">Dĭgəttə</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1049" n="HIAT:w" s="T273">beškeʔi</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1052" n="HIAT:w" s="T274">oʔbdəbi</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1055" n="HIAT:w" s="T275">i</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1058" n="HIAT:w" s="T276">vsʼaka</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1060" n="HIAT:ip">(</nts>
                  <ts e="T278" id="Seg_1062" n="HIAT:w" s="T277">iʔgö</ts>
                  <nts id="Seg_1063" n="HIAT:ip">)</nts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1066" n="HIAT:w" s="T278">beške</ts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1069" n="HIAT:w" s="T279">deppi</ts>
                  <nts id="Seg_1070" n="HIAT:ip">.</nts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T283" id="Seg_1073" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_1075" n="HIAT:w" s="T280">A</ts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1078" n="HIAT:w" s="T281">nezeŋ</ts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1081" n="HIAT:w" s="T282">kudolaʔbəʔjə</ts>
                  <nts id="Seg_1082" n="HIAT:ip">.</nts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T287" id="Seg_1085" n="HIAT:u" s="T283">
                  <ts e="T284" id="Seg_1087" n="HIAT:w" s="T283">Ĭmbi</ts>
                  <nts id="Seg_1088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1090" n="HIAT:w" s="T284">dĭrgit</ts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1092" n="HIAT:ip">(</nts>
                  <ts e="T286" id="Seg_1094" n="HIAT:w" s="T285">deʔ-</ts>
                  <nts id="Seg_1095" n="HIAT:ip">)</nts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1098" n="HIAT:w" s="T286">deʔpiel</ts>
                  <nts id="Seg_1099" n="HIAT:ip">.</nts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T290" id="Seg_1102" n="HIAT:u" s="T287">
                  <ts e="T288" id="Seg_1104" n="HIAT:w" s="T287">Tolʼko</ts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1107" n="HIAT:w" s="T288">tănan</ts>
                  <nts id="Seg_1108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1110" n="HIAT:w" s="T289">amzittə</ts>
                  <nts id="Seg_1111" n="HIAT:ip">.</nts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T295" id="Seg_1114" n="HIAT:u" s="T290">
                  <ts e="T291" id="Seg_1116" n="HIAT:w" s="T290">Dĭ</ts>
                  <nts id="Seg_1117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1119" n="HIAT:w" s="T291">pʼešdə</ts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1121" n="HIAT:ip">(</nts>
                  <ts e="T293" id="Seg_1123" n="HIAT:w" s="T292">š-</ts>
                  <nts id="Seg_1124" n="HIAT:ip">)</nts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1127" n="HIAT:w" s="T293">sʼabi</ts>
                  <nts id="Seg_1128" n="HIAT:ip">,</nts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1131" n="HIAT:w" s="T294">iʔbölaʔbə</ts>
                  <nts id="Seg_1132" n="HIAT:ip">.</nts>
                  <nts id="Seg_1133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T304" id="Seg_1135" n="HIAT:u" s="T295">
                  <ts e="T296" id="Seg_1137" n="HIAT:w" s="T295">Dĭgəttə</ts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1139" n="HIAT:ip">(</nts>
                  <ts e="T297" id="Seg_1141" n="HIAT:w" s="T296">kazak-</ts>
                  <nts id="Seg_1142" n="HIAT:ip">)</nts>
                  <nts id="Seg_1143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1145" n="HIAT:w" s="T297">kagazaŋdə</ts>
                  <nts id="Seg_1146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1148" n="HIAT:w" s="T298">šobiʔi</ts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1151" n="HIAT:w" s="T299">bar</ts>
                  <nts id="Seg_1152" n="HIAT:ip">,</nts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1155" n="HIAT:w" s="T300">nörbəleʔbəʔjə</ts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1157" n="HIAT:ip">(</nts>
                  <ts e="T302" id="Seg_1159" n="HIAT:w" s="T301">dĭrgi-</ts>
                  <nts id="Seg_1160" n="HIAT:ip">)</nts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1163" n="HIAT:w" s="T302">kăda</ts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1166" n="HIAT:w" s="T303">ibi</ts>
                  <nts id="Seg_1167" n="HIAT:ip">.</nts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T307" id="Seg_1170" n="HIAT:u" s="T304">
                  <ts e="T305" id="Seg_1172" n="HIAT:w" s="T304">A</ts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1175" n="HIAT:w" s="T305">dĭ</ts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1178" n="HIAT:w" s="T306">iʔbölaʔbə</ts>
                  <nts id="Seg_1179" n="HIAT:ip">.</nts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T312" id="Seg_1182" n="HIAT:u" s="T307">
                  <ts e="T308" id="Seg_1184" n="HIAT:w" s="T307">Dĭ</ts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1187" n="HIAT:w" s="T308">măn</ts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1190" n="HIAT:w" s="T309">ulum</ts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1193" n="HIAT:w" s="T310">dĭn</ts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1196" n="HIAT:w" s="T311">ibi</ts>
                  <nts id="Seg_1197" n="HIAT:ip">.</nts>
                  <nts id="Seg_1198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T314" id="Seg_1200" n="HIAT:u" s="T312">
                  <ts e="T313" id="Seg_1202" n="HIAT:w" s="T312">Iʔ</ts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1205" n="HIAT:w" s="T313">šamaʔ</ts>
                  <nts id="Seg_1206" n="HIAT:ip">!</nts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T316" id="Seg_1209" n="HIAT:u" s="T314">
                  <ts e="T315" id="Seg_1211" n="HIAT:w" s="T314">Tăn</ts>
                  <nts id="Seg_1212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1214" n="HIAT:w" s="T315">ulum</ts>
                  <nts id="Seg_1215" n="HIAT:ip">.</nts>
                  <nts id="Seg_1216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T318" id="Seg_1218" n="HIAT:u" s="T316">
                  <ts e="T317" id="Seg_1220" n="HIAT:w" s="T316">Amnoʔ</ts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1223" n="HIAT:w" s="T317">už</ts>
                  <nts id="Seg_1224" n="HIAT:ip">.</nts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T321" id="Seg_1227" n="HIAT:u" s="T318">
                  <ts e="T319" id="Seg_1229" n="HIAT:w" s="T318">Ĭmbidə</ts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1232" n="HIAT:w" s="T319">iʔ</ts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1235" n="HIAT:w" s="T320">dʼăbaktəraʔ</ts>
                  <nts id="Seg_1236" n="HIAT:ip">.</nts>
                  <nts id="Seg_1237" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T328" id="Seg_1239" n="HIAT:u" s="T321">
                  <ts e="T322" id="Seg_1241" n="HIAT:w" s="T321">Dĭrgit</ts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1244" n="HIAT:w" s="T322">kuza</ts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1247" n="HIAT:w" s="T323">ibi</ts>
                  <nts id="Seg_1248" n="HIAT:ip">,</nts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1251" n="HIAT:w" s="T324">inet</ts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1254" n="HIAT:w" s="T325">kuvas</ts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1257" n="HIAT:w" s="T326">ibi</ts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1260" n="HIAT:w" s="T327">bostə</ts>
                  <nts id="Seg_1261" n="HIAT:ip">.</nts>
                  <nts id="Seg_1262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T332" id="Seg_1264" n="HIAT:u" s="T328">
                  <ts e="T329" id="Seg_1266" n="HIAT:w" s="T328">Dĭgəttə</ts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1269" n="HIAT:w" s="T329">karəldʼan</ts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1272" n="HIAT:w" s="T330">bazoʔ</ts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1275" n="HIAT:w" s="T331">kandəgaʔi</ts>
                  <nts id="Seg_1276" n="HIAT:ip">.</nts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T334" id="Seg_1279" n="HIAT:u" s="T332">
                  <ts e="T333" id="Seg_1281" n="HIAT:w" s="T332">Kalla</ts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1284" n="HIAT:w" s="T333">dʼürbiʔi</ts>
                  <nts id="Seg_1285" n="HIAT:ip">.</nts>
                  <nts id="Seg_1286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T338" id="Seg_1288" n="HIAT:u" s="T334">
                  <ts e="T335" id="Seg_1290" n="HIAT:w" s="T334">Dĭ</ts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1293" n="HIAT:w" s="T335">pʼešgəʔ</ts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1295" n="HIAT:ip">(</nts>
                  <ts e="T337" id="Seg_1297" n="HIAT:w" s="T336">š-</ts>
                  <nts id="Seg_1298" n="HIAT:ip">)</nts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1301" n="HIAT:w" s="T337">suʔmiluʔpi</ts>
                  <nts id="Seg_1302" n="HIAT:ip">.</nts>
                  <nts id="Seg_1303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T340" id="Seg_1305" n="HIAT:u" s="T338">
                  <ts e="T339" id="Seg_1307" n="HIAT:w" s="T338">Karzʼina</ts>
                  <nts id="Seg_1308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1310" n="HIAT:w" s="T339">ibi</ts>
                  <nts id="Seg_1311" n="HIAT:ip">.</nts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T344" id="Seg_1314" n="HIAT:u" s="T340">
                  <ts e="T341" id="Seg_1316" n="HIAT:w" s="T340">Kambi</ts>
                  <nts id="Seg_1317" n="HIAT:ip">,</nts>
                  <nts id="Seg_1318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1320" n="HIAT:w" s="T341">kambi</ts>
                  <nts id="Seg_1321" n="HIAT:ip">,</nts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1324" n="HIAT:w" s="T342">baruʔpi</ts>
                  <nts id="Seg_1325" n="HIAT:ip">.</nts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T347" id="Seg_1328" n="HIAT:u" s="T344">
                  <ts e="T345" id="Seg_1330" n="HIAT:w" s="T344">Davaj</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1333" n="HIAT:w" s="T345">inem</ts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1336" n="HIAT:w" s="T346">kirgarzittə</ts>
                  <nts id="Seg_1337" n="HIAT:ip">.</nts>
                  <nts id="Seg_1338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T349" id="Seg_1340" n="HIAT:u" s="T347">
                  <ts e="T348" id="Seg_1342" n="HIAT:w" s="T347">Inet</ts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1345" n="HIAT:w" s="T348">šobi</ts>
                  <nts id="Seg_1346" n="HIAT:ip">.</nts>
                  <nts id="Seg_1347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T355" id="Seg_1349" n="HIAT:u" s="T349">
                  <ts e="T350" id="Seg_1351" n="HIAT:w" s="T349">Bazoʔ</ts>
                  <nts id="Seg_1352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1354" n="HIAT:w" s="T350">onʼiʔ</ts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1357" n="HIAT:w" s="T351">kuʔtə</ts>
                  <nts id="Seg_1358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1359" n="HIAT:ip">(</nts>
                  <ts e="T353" id="Seg_1361" n="HIAT:w" s="T352">paʔlaːmbi</ts>
                  <nts id="Seg_1362" n="HIAT:ip">)</nts>
                  <nts id="Seg_1363" n="HIAT:ip">,</nts>
                  <nts id="Seg_1364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1366" n="HIAT:w" s="T353">onʼiʔ</ts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1369" n="HIAT:w" s="T354">supsolaːmbi</ts>
                  <nts id="Seg_1370" n="HIAT:ip">.</nts>
                  <nts id="Seg_1371" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T357" id="Seg_1373" n="HIAT:u" s="T355">
                  <ts e="T356" id="Seg_1375" n="HIAT:w" s="T355">Dĭgəttə</ts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1378" n="HIAT:w" s="T356">amnəbi</ts>
                  <nts id="Seg_1379" n="HIAT:ip">.</nts>
                  <nts id="Seg_1380" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T362" id="Seg_1382" n="HIAT:u" s="T357">
                  <nts id="Seg_1383" n="HIAT:ip">(</nts>
                  <ts e="T358" id="Seg_1385" n="HIAT:w" s="T357">Nuʔm-</ts>
                  <nts id="Seg_1386" n="HIAT:ip">)</nts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1389" n="HIAT:w" s="T358">Šonuga</ts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1392" n="HIAT:w" s="T359">il</ts>
                  <nts id="Seg_1393" n="HIAT:ip">,</nts>
                  <nts id="Seg_1394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1396" n="HIAT:w" s="T360">iʔgö</ts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1399" n="HIAT:w" s="T361">nugaʔi</ts>
                  <nts id="Seg_1400" n="HIAT:ip">.</nts>
                  <nts id="Seg_1401" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T367" id="Seg_1403" n="HIAT:u" s="T362">
                  <ts e="T363" id="Seg_1405" n="HIAT:w" s="T362">Dĭgəttə</ts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1408" n="HIAT:w" s="T363">šindidə</ts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1411" n="HIAT:w" s="T364">ej</ts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1414" n="HIAT:w" s="T365">nuʔməlie</ts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1417" n="HIAT:w" s="T366">dibər</ts>
                  <nts id="Seg_1418" n="HIAT:ip">.</nts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T370" id="Seg_1421" n="HIAT:u" s="T367">
                  <ts e="T368" id="Seg_1423" n="HIAT:w" s="T367">Dibər</ts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1425" n="HIAT:ip">(</nts>
                  <ts e="T369" id="Seg_1427" n="HIAT:w" s="T368">š-</ts>
                  <nts id="Seg_1428" n="HIAT:ip">)</nts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1431" n="HIAT:w" s="T369">suʔmiluʔpi</ts>
                  <nts id="Seg_1432" n="HIAT:ip">.</nts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T377" id="Seg_1435" n="HIAT:u" s="T370">
                  <ts e="T371" id="Seg_1437" n="HIAT:w" s="T370">Ibi</ts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1439" n="HIAT:ip">(</nts>
                  <ts e="T372" id="Seg_1441" n="HIAT:w" s="T371">e-</ts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1444" n="HIAT:w" s="T372">i</ts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1447" n="HIAT:w" s="T373">ej</ts>
                  <nts id="Seg_1448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1450" n="HIAT:w" s="T374">nʼi-</ts>
                  <nts id="Seg_1451" n="HIAT:ip">)</nts>
                  <nts id="Seg_1452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1454" n="HIAT:w" s="T375">ej</ts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1457" n="HIAT:w" s="T376">mobi</ts>
                  <nts id="Seg_1458" n="HIAT:ip">.</nts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T379" id="Seg_1461" n="HIAT:u" s="T377">
                  <ts e="T378" id="Seg_1463" n="HIAT:w" s="T377">Dĭgəttə</ts>
                  <nts id="Seg_1464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1466" n="HIAT:w" s="T378">parluʔpi</ts>
                  <nts id="Seg_1467" n="HIAT:ip">.</nts>
                  <nts id="Seg_1468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T385" id="Seg_1470" n="HIAT:u" s="T379">
                  <ts e="T380" id="Seg_1472" n="HIAT:w" s="T379">Kagazaŋdə</ts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1475" n="HIAT:w" s="T380">bar</ts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1478" n="HIAT:w" s="T381">münörbi</ts>
                  <nts id="Seg_1479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1481" n="HIAT:w" s="T382">i</ts>
                  <nts id="Seg_1482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1484" n="HIAT:w" s="T383">kalla</ts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1487" n="HIAT:w" s="T384">dʼürbi</ts>
                  <nts id="Seg_1488" n="HIAT:ip">.</nts>
                  <nts id="Seg_1489" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T388" id="Seg_1491" n="HIAT:u" s="T385">
                  <ts e="T386" id="Seg_1493" n="HIAT:w" s="T385">A</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1496" n="HIAT:w" s="T386">il</ts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1499" n="HIAT:w" s="T387">kirgarlaʔbəʔjə</ts>
                  <nts id="Seg_1500" n="HIAT:ip">.</nts>
                  <nts id="Seg_1501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T394" id="Seg_1503" n="HIAT:u" s="T388">
                  <nts id="Seg_1504" n="HIAT:ip">(</nts>
                  <ts e="T389" id="Seg_1506" n="HIAT:w" s="T388">Dʼabəʔ</ts>
                  <nts id="Seg_1507" n="HIAT:ip">)</nts>
                  <nts id="Seg_1508" n="HIAT:ip">,</nts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1510" n="HIAT:ip">(</nts>
                  <ts e="T390" id="Seg_1512" n="HIAT:w" s="T389">dʼabəʔ</ts>
                  <nts id="Seg_1513" n="HIAT:ip">)</nts>
                  <nts id="Seg_1514" n="HIAT:ip">,</nts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1517" n="HIAT:w" s="T390">da</ts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1520" n="HIAT:w" s="T391">dĭ</ts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1523" n="HIAT:w" s="T392">iʔ</ts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1526" n="HIAT:w" s="T393">məluʔpi</ts>
                  <nts id="Seg_1527" n="HIAT:ip">.</nts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T400" id="Seg_1530" n="HIAT:u" s="T394">
                  <ts e="T395" id="Seg_1532" n="HIAT:w" s="T394">Inebə</ts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1535" n="HIAT:w" s="T395">öʔluʔpi</ts>
                  <nts id="Seg_1536" n="HIAT:ip">,</nts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1539" n="HIAT:w" s="T396">a</ts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1542" n="HIAT:w" s="T397">bostə</ts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1545" n="HIAT:w" s="T398">bazoʔ</ts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1548" n="HIAT:w" s="T399">šobi</ts>
                  <nts id="Seg_1549" n="HIAT:ip">.</nts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T403" id="Seg_1552" n="HIAT:u" s="T400">
                  <ts e="T401" id="Seg_1554" n="HIAT:w" s="T400">Nezeŋdə</ts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1557" n="HIAT:w" s="T401">bazoʔ</ts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1560" n="HIAT:w" s="T402">kudolaʔbəʔjə</ts>
                  <nts id="Seg_1561" n="HIAT:ip">.</nts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T409" id="Seg_1564" n="HIAT:u" s="T403">
                  <ts e="T404" id="Seg_1566" n="HIAT:w" s="T403">A</ts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1569" n="HIAT:w" s="T404">dĭ</ts>
                  <nts id="Seg_1570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1572" n="HIAT:w" s="T405">pʼešdə</ts>
                  <nts id="Seg_1573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1575" n="HIAT:w" s="T406">sʼabi</ts>
                  <nts id="Seg_1576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1578" n="HIAT:w" s="T407">da</ts>
                  <nts id="Seg_1579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1581" n="HIAT:w" s="T408">iʔbölaʔbə</ts>
                  <nts id="Seg_1582" n="HIAT:ip">.</nts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T412" id="Seg_1585" n="HIAT:u" s="T409">
                  <nts id="Seg_1586" n="HIAT:ip">(</nts>
                  <ts e="T410" id="Seg_1588" n="HIAT:w" s="T409">Kaz-</ts>
                  <nts id="Seg_1589" n="HIAT:ip">)</nts>
                  <nts id="Seg_1590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1592" n="HIAT:w" s="T410">Kazaŋdə</ts>
                  <nts id="Seg_1593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1595" n="HIAT:w" s="T411">šobiʔi</ts>
                  <nts id="Seg_1596" n="HIAT:ip">.</nts>
                  <nts id="Seg_1597" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T414" id="Seg_1599" n="HIAT:u" s="T412">
                  <ts e="T413" id="Seg_1601" n="HIAT:w" s="T412">Abandə</ts>
                  <nts id="Seg_1602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1604" n="HIAT:w" s="T413">nörbəlieʔi</ts>
                  <nts id="Seg_1605" n="HIAT:ip">.</nts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T427" id="Seg_1608" n="HIAT:u" s="T414">
                  <ts e="T415" id="Seg_1610" n="HIAT:w" s="T414">Girgit</ts>
                  <nts id="Seg_1611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1613" n="HIAT:w" s="T415">kuza</ts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1616" n="HIAT:w" s="T416">ibi</ts>
                  <nts id="Seg_1617" n="HIAT:ip">,</nts>
                  <nts id="Seg_1618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1620" n="HIAT:w" s="T417">inet</ts>
                  <nts id="Seg_1621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1623" n="HIAT:w" s="T418">ugaːndə</ts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1626" n="HIAT:w" s="T419">kuvas</ts>
                  <nts id="Seg_1627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1629" n="HIAT:w" s="T420">i</ts>
                  <nts id="Seg_1630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1631" n="HIAT:ip">(</nts>
                  <ts e="T422" id="Seg_1633" n="HIAT:w" s="T421">bolʼš-</ts>
                  <nts id="Seg_1634" n="HIAT:ip">)</nts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1637" n="HIAT:w" s="T422">bostə</ts>
                  <nts id="Seg_1638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1640" n="HIAT:w" s="T423">kuvas</ts>
                  <nts id="Seg_1641" n="HIAT:ip">,</nts>
                  <nts id="Seg_1642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1644" n="HIAT:w" s="T424">a</ts>
                  <nts id="Seg_1645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1647" n="HIAT:w" s="T425">dĭ</ts>
                  <nts id="Seg_1648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1650" n="HIAT:w" s="T426">iʔbölaʔbə</ts>
                  <nts id="Seg_1651" n="HIAT:ip">.</nts>
                  <nts id="Seg_1652" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T433" id="Seg_1654" n="HIAT:u" s="T427">
                  <ts e="T428" id="Seg_1656" n="HIAT:w" s="T427">Ej</ts>
                  <nts id="Seg_1657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1659" n="HIAT:w" s="T428">măn</ts>
                  <nts id="Seg_1660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1662" n="HIAT:w" s="T429">li</ts>
                  <nts id="Seg_1663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1665" n="HIAT:w" s="T430">ulum</ts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1668" n="HIAT:w" s="T431">dĭn</ts>
                  <nts id="Seg_1669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1671" n="HIAT:w" s="T432">ibi</ts>
                  <nts id="Seg_1672" n="HIAT:ip">.</nts>
                  <nts id="Seg_1673" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T437" id="Seg_1675" n="HIAT:u" s="T433">
                  <ts e="T434" id="Seg_1677" n="HIAT:w" s="T433">Iʔ</ts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1680" n="HIAT:w" s="T434">šamaʔ</ts>
                  <nts id="Seg_1681" n="HIAT:ip">,</nts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1684" n="HIAT:w" s="T435">iʔbəʔ</ts>
                  <nts id="Seg_1685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1687" n="HIAT:w" s="T436">už</ts>
                  <nts id="Seg_1688" n="HIAT:ip">!</nts>
                  <nts id="Seg_1689" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T439" id="Seg_1691" n="HIAT:u" s="T437">
                  <ts e="T438" id="Seg_1693" n="HIAT:w" s="T437">Sagəššət</ts>
                  <nts id="Seg_1694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1696" n="HIAT:w" s="T438">nʼi</ts>
                  <nts id="Seg_1697" n="HIAT:ip">!</nts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T442" id="Seg_1700" n="HIAT:u" s="T439">
                  <ts e="T440" id="Seg_1702" n="HIAT:w" s="T439">Tăn</ts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1705" n="HIAT:w" s="T440">dĭn</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1708" n="HIAT:w" s="T441">ibiel</ts>
                  <nts id="Seg_1709" n="HIAT:ip">.</nts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T448" id="Seg_1712" n="HIAT:u" s="T442">
                  <ts e="T443" id="Seg_1714" n="HIAT:w" s="T442">Dĭgəttə</ts>
                  <nts id="Seg_1715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1717" n="HIAT:w" s="T443">bazoʔ</ts>
                  <nts id="Seg_1718" n="HIAT:ip">,</nts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1721" n="HIAT:w" s="T444">nagur</ts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1724" n="HIAT:w" s="T445">dʼala</ts>
                  <nts id="Seg_1725" n="HIAT:ip">,</nts>
                  <nts id="Seg_1726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1728" n="HIAT:w" s="T446">bazoʔ</ts>
                  <nts id="Seg_1729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1731" n="HIAT:w" s="T447">kandəgaʔi</ts>
                  <nts id="Seg_1732" n="HIAT:ip">.</nts>
                  <nts id="Seg_1733" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T453" id="Seg_1735" n="HIAT:u" s="T448">
                  <ts e="T449" id="Seg_1737" n="HIAT:w" s="T448">Kalla</ts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1740" n="HIAT:w" s="T449">dʼürbiʔi</ts>
                  <nts id="Seg_1741" n="HIAT:ip">,</nts>
                  <nts id="Seg_1742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1744" n="HIAT:w" s="T450">dĭ</ts>
                  <nts id="Seg_1745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1746" n="HIAT:ip">(</nts>
                  <ts e="T452" id="Seg_1748" n="HIAT:w" s="T451">s-</ts>
                  <nts id="Seg_1749" n="HIAT:ip">)</nts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1752" n="HIAT:w" s="T452">uʔbdəbi</ts>
                  <nts id="Seg_1753" n="HIAT:ip">.</nts>
                  <nts id="Seg_1754" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T456" id="Seg_1756" n="HIAT:u" s="T453">
                  <ts e="T454" id="Seg_1758" n="HIAT:w" s="T453">Karzʼinabə</ts>
                  <nts id="Seg_1759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1761" n="HIAT:w" s="T454">ibi</ts>
                  <nts id="Seg_1762" n="HIAT:ip">,</nts>
                  <nts id="Seg_1763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1765" n="HIAT:w" s="T455">kambi</ts>
                  <nts id="Seg_1766" n="HIAT:ip">.</nts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T458" id="Seg_1769" n="HIAT:u" s="T456">
                  <ts e="T457" id="Seg_1771" n="HIAT:w" s="T456">Beškeʔi</ts>
                  <nts id="Seg_1772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1774" n="HIAT:w" s="T457">oʔbšizittə</ts>
                  <nts id="Seg_1775" n="HIAT:ip">.</nts>
                  <nts id="Seg_1776" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T462" id="Seg_1778" n="HIAT:u" s="T458">
                  <ts e="T459" id="Seg_1780" n="HIAT:w" s="T458">Kambi</ts>
                  <nts id="Seg_1781" n="HIAT:ip">,</nts>
                  <nts id="Seg_1782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1784" n="HIAT:w" s="T459">davaj</ts>
                  <nts id="Seg_1785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1787" n="HIAT:w" s="T460">ine</ts>
                  <nts id="Seg_1788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1790" n="HIAT:w" s="T461">kirgarzittə</ts>
                  <nts id="Seg_1791" n="HIAT:ip">.</nts>
                  <nts id="Seg_1792" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T464" id="Seg_1794" n="HIAT:u" s="T462">
                  <ts e="T463" id="Seg_1796" n="HIAT:w" s="T462">Ine</ts>
                  <nts id="Seg_1797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1799" n="HIAT:w" s="T463">šobi</ts>
                  <nts id="Seg_1800" n="HIAT:ip">.</nts>
                  <nts id="Seg_1801" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T475" id="Seg_1803" n="HIAT:u" s="T464">
                  <ts e="T465" id="Seg_1805" n="HIAT:w" s="T464">Dĭ</ts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1808" n="HIAT:w" s="T465">bazoʔ</ts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1811" n="HIAT:w" s="T466">dăra</ts>
                  <nts id="Seg_1812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1813" n="HIAT:ip">(</nts>
                  <ts e="T468" id="Seg_1815" n="HIAT:w" s="T467">on-</ts>
                  <nts id="Seg_1816" n="HIAT:ip">)</nts>
                  <nts id="Seg_1817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1819" n="HIAT:w" s="T468">onʼiʔ</ts>
                  <nts id="Seg_1820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1822" n="HIAT:w" s="T469">kugəndə</ts>
                  <nts id="Seg_1823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1825" n="HIAT:w" s="T470">paʔlaːmbi</ts>
                  <nts id="Seg_1826" n="HIAT:ip">,</nts>
                  <nts id="Seg_1827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1829" n="HIAT:w" s="T471">onʼiʔ</ts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1832" n="HIAT:w" s="T472">supsolaːmbi</ts>
                  <nts id="Seg_1833" n="HIAT:ip">,</nts>
                  <nts id="Seg_1834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1836" n="HIAT:w" s="T473">amnobi</ts>
                  <nts id="Seg_1837" n="HIAT:ip">,</nts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1840" n="HIAT:w" s="T474">kambi</ts>
                  <nts id="Seg_1841" n="HIAT:ip">.</nts>
                  <nts id="Seg_1842" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T478" id="Seg_1844" n="HIAT:u" s="T475">
                  <ts e="T476" id="Seg_1846" n="HIAT:w" s="T475">Dĭgəttə</ts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1849" n="HIAT:w" s="T476">kak</ts>
                  <nts id="Seg_1850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1852" n="HIAT:w" s="T477">nuʔməluʔpi</ts>
                  <nts id="Seg_1853" n="HIAT:ip">.</nts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T487" id="Seg_1856" n="HIAT:u" s="T478">
                  <ts e="T479" id="Seg_1858" n="HIAT:w" s="T478">Dĭm</ts>
                  <nts id="Seg_1859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1861" n="HIAT:w" s="T479">panarbi</ts>
                  <nts id="Seg_1862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1864" n="HIAT:w" s="T480">i</ts>
                  <nts id="Seg_1865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1867" n="HIAT:w" s="T481">kalʼečkabə</ts>
                  <nts id="Seg_1868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1870" n="HIAT:w" s="T482">iluʔpi</ts>
                  <nts id="Seg_1871" n="HIAT:ip">,</nts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1874" n="HIAT:w" s="T483">bostə</ts>
                  <nts id="Seg_1875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1877" n="HIAT:w" s="T484">udandə</ts>
                  <nts id="Seg_1878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1879" n="HIAT:ip">(</nts>
                  <ts e="T486" id="Seg_1881" n="HIAT:w" s="T485">se-</ts>
                  <nts id="Seg_1882" n="HIAT:ip">)</nts>
                  <nts id="Seg_1883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1885" n="HIAT:w" s="T486">šerbi</ts>
                  <nts id="Seg_1886" n="HIAT:ip">.</nts>
                  <nts id="Seg_1887" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T489" id="Seg_1889" n="HIAT:u" s="T487">
                  <ts e="T488" id="Seg_1891" n="HIAT:w" s="T487">Kalla</ts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1894" n="HIAT:w" s="T488">dʼürbi</ts>
                  <nts id="Seg_1895" n="HIAT:ip">.</nts>
                  <nts id="Seg_1896" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T498" id="Seg_1898" n="HIAT:u" s="T489">
                  <ts e="T490" id="Seg_1900" n="HIAT:w" s="T489">Inebə</ts>
                  <nts id="Seg_1901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1903" n="HIAT:w" s="T490">öʔlubi</ts>
                  <nts id="Seg_1904" n="HIAT:ip">,</nts>
                  <nts id="Seg_1905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1907" n="HIAT:w" s="T491">bostə</ts>
                  <nts id="Seg_1908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1910" n="HIAT:w" s="T492">šobi</ts>
                  <nts id="Seg_1911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1913" n="HIAT:w" s="T493">maːndə</ts>
                  <nts id="Seg_1914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1916" n="HIAT:w" s="T494">i</ts>
                  <nts id="Seg_1917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1919" n="HIAT:w" s="T495">pʼešdə</ts>
                  <nts id="Seg_1920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1922" n="HIAT:w" s="T496">sʼabi</ts>
                  <nts id="Seg_1923" n="HIAT:ip">,</nts>
                  <nts id="Seg_1924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1926" n="HIAT:w" s="T497">iʔbölaʔbə</ts>
                  <nts id="Seg_1927" n="HIAT:ip">.</nts>
                  <nts id="Seg_1928" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T501" id="Seg_1930" n="HIAT:u" s="T498">
                  <ts e="T499" id="Seg_1932" n="HIAT:w" s="T498">Dĭgəttə</ts>
                  <nts id="Seg_1933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1935" n="HIAT:w" s="T499">kagazaŋdə</ts>
                  <nts id="Seg_1936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1938" n="HIAT:w" s="T500">šobiʔi</ts>
                  <nts id="Seg_1939" n="HIAT:ip">.</nts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T508" id="Seg_1942" n="HIAT:u" s="T501">
                  <ts e="T502" id="Seg_1944" n="HIAT:w" s="T501">Nu</ts>
                  <nts id="Seg_1945" n="HIAT:ip">,</nts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1948" n="HIAT:w" s="T502">girgitdə</ts>
                  <nts id="Seg_1949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1950" n="HIAT:ip">(</nts>
                  <ts e="T504" id="Seg_1952" n="HIAT:w" s="T503">nʼi</ts>
                  <nts id="Seg_1953" n="HIAT:ip">)</nts>
                  <nts id="Seg_1954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1956" n="HIAT:w" s="T504">nʼi</ts>
                  <nts id="Seg_1957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1959" n="HIAT:w" s="T505">kuvas</ts>
                  <nts id="Seg_1960" n="HIAT:ip">,</nts>
                  <nts id="Seg_1961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1963" n="HIAT:w" s="T506">inet</ts>
                  <nts id="Seg_1964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1966" n="HIAT:w" s="T507">kuvas</ts>
                  <nts id="Seg_1967" n="HIAT:ip">.</nts>
                  <nts id="Seg_1968" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T511" id="Seg_1970" n="HIAT:u" s="T508">
                  <ts e="T509" id="Seg_1972" n="HIAT:w" s="T508">Panarbi</ts>
                  <nts id="Seg_1973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1975" n="HIAT:w" s="T509">tsarskɨj</ts>
                  <nts id="Seg_1976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1978" n="HIAT:w" s="T510">koʔbdo</ts>
                  <nts id="Seg_1979" n="HIAT:ip">.</nts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T514" id="Seg_1982" n="HIAT:u" s="T511">
                  <nts id="Seg_1983" n="HIAT:ip">(</nts>
                  <ts e="T512" id="Seg_1985" n="HIAT:w" s="T511">Koŋ</ts>
                  <nts id="Seg_1986" n="HIAT:ip">)</nts>
                  <nts id="Seg_1987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1989" n="HIAT:w" s="T512">Koŋ</ts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1992" n="HIAT:w" s="T513">koʔbdo</ts>
                  <nts id="Seg_1993" n="HIAT:ip">.</nts>
                  <nts id="Seg_1994" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T519" id="Seg_1996" n="HIAT:u" s="T514">
                  <ts e="T515" id="Seg_1998" n="HIAT:w" s="T514">I</ts>
                  <nts id="Seg_1999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_2001" n="HIAT:w" s="T515">kalʼečkabə</ts>
                  <nts id="Seg_2002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_2004" n="HIAT:w" s="T516">ibi</ts>
                  <nts id="Seg_2005" n="HIAT:ip">,</nts>
                  <nts id="Seg_2006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_2008" n="HIAT:w" s="T517">kalla</ts>
                  <nts id="Seg_2009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_2011" n="HIAT:w" s="T518">dʼürbi</ts>
                  <nts id="Seg_2012" n="HIAT:ip">.</nts>
                  <nts id="Seg_2013" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T524" id="Seg_2015" n="HIAT:u" s="T519">
                  <ts e="T520" id="Seg_2017" n="HIAT:w" s="T519">Dĭgəttə</ts>
                  <nts id="Seg_2018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_2020" n="HIAT:w" s="T520">bazoʔ</ts>
                  <nts id="Seg_2021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_2023" n="HIAT:w" s="T521">dĭ</ts>
                  <nts id="Seg_2024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_2026" n="HIAT:w" s="T522">koŋ</ts>
                  <nts id="Seg_2027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_2029" n="HIAT:w" s="T523">abi</ts>
                  <nts id="Seg_2030" n="HIAT:ip">.</nts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T533" id="Seg_2033" n="HIAT:u" s="T524">
                  <ts e="T525" id="Seg_2035" n="HIAT:w" s="T524">Iʔgö</ts>
                  <nts id="Seg_2036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_2038" n="HIAT:w" s="T525">uja</ts>
                  <nts id="Seg_2039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_2041" n="HIAT:w" s="T526">munujʔ</ts>
                  <nts id="Seg_2042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_2044" n="HIAT:w" s="T527">embi</ts>
                  <nts id="Seg_2045" n="HIAT:ip">,</nts>
                  <nts id="Seg_2046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_2048" n="HIAT:w" s="T528">bar</ts>
                  <nts id="Seg_2049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_2051" n="HIAT:w" s="T529">ĭmbi</ts>
                  <nts id="Seg_2052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_2054" n="HIAT:w" s="T530">dĭn</ts>
                  <nts id="Seg_2055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_2057" n="HIAT:w" s="T531">ige</ts>
                  <nts id="Seg_2058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_2060" n="HIAT:w" s="T532">embi</ts>
                  <nts id="Seg_2061" n="HIAT:ip">.</nts>
                  <nts id="Seg_2062" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T542" id="Seg_2064" n="HIAT:u" s="T533">
                  <ts e="T534" id="Seg_2066" n="HIAT:w" s="T533">Dĭgəttə</ts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_2069" n="HIAT:w" s="T534">Šogaʔ</ts>
                  <nts id="Seg_2070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_2072" n="HIAT:w" s="T535">bargəʔ</ts>
                  <nts id="Seg_2073" n="HIAT:ip">,</nts>
                  <nts id="Seg_2074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_2076" n="HIAT:w" s="T536">šində</ts>
                  <nts id="Seg_2077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2079" n="HIAT:w" s="T537">ej</ts>
                  <nts id="Seg_2080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2082" n="HIAT:w" s="T538">šoləj</ts>
                  <nts id="Seg_2083" n="HIAT:ip">,</nts>
                  <nts id="Seg_2084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_2086" n="HIAT:w" s="T539">ulubə</ts>
                  <nts id="Seg_2087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2088" n="HIAT:ip">(</nts>
                  <ts e="T541" id="Seg_2090" n="HIAT:w" s="T540">š-</ts>
                  <nts id="Seg_2091" n="HIAT:ip">)</nts>
                  <nts id="Seg_2092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2094" n="HIAT:w" s="T541">sajjaʔlim</ts>
                  <nts id="Seg_2095" n="HIAT:ip">.</nts>
                  <nts id="Seg_2096" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T552" id="Seg_2098" n="HIAT:u" s="T542">
                  <ts e="T543" id="Seg_2100" n="HIAT:w" s="T542">Dĭgəttə</ts>
                  <nts id="Seg_2101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2103" n="HIAT:w" s="T543">dĭzeŋ</ts>
                  <nts id="Seg_2104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2105" n="HIAT:ip">(</nts>
                  <ts e="T545" id="Seg_2107" n="HIAT:w" s="T544">a-</ts>
                  <nts id="Seg_2108" n="HIAT:ip">)</nts>
                  <nts id="Seg_2109" n="HIAT:ip">,</nts>
                  <nts id="Seg_2110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2112" n="HIAT:w" s="T545">dĭn</ts>
                  <nts id="Seg_2113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2115" n="HIAT:w" s="T546">abat</ts>
                  <nts id="Seg_2116" n="HIAT:ip">,</nts>
                  <nts id="Seg_2117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2119" n="HIAT:w" s="T547">bargəʔ</ts>
                  <nts id="Seg_2120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2122" n="HIAT:w" s="T548">kalla</ts>
                  <nts id="Seg_2123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2125" n="HIAT:w" s="T549">dʼürbiʔi</ts>
                  <nts id="Seg_2126" n="HIAT:ip">,</nts>
                  <nts id="Seg_2127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2129" n="HIAT:w" s="T550">šobiʔi</ts>
                  <nts id="Seg_2130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2132" n="HIAT:w" s="T551">dĭzeŋ</ts>
                  <nts id="Seg_2133" n="HIAT:ip">.</nts>
                  <nts id="Seg_2134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T559" id="Seg_2136" n="HIAT:u" s="T552">
                  <ts e="T553" id="Seg_2138" n="HIAT:w" s="T552">Amnəlbiʔi</ts>
                  <nts id="Seg_2139" n="HIAT:ip">,</nts>
                  <nts id="Seg_2140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2141" n="HIAT:ip">(</nts>
                  <ts e="T554" id="Seg_2143" n="HIAT:w" s="T553">ba-</ts>
                  <nts id="Seg_2144" n="HIAT:ip">)</nts>
                  <nts id="Seg_2145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2147" n="HIAT:w" s="T554">bar</ts>
                  <nts id="Seg_2148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2150" n="HIAT:w" s="T555">bĭdlaʔbəʔjə</ts>
                  <nts id="Seg_2151" n="HIAT:ip">,</nts>
                  <nts id="Seg_2152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2153" n="HIAT:ip">(</nts>
                  <ts e="T557" id="Seg_2155" n="HIAT:w" s="T556">aja-</ts>
                  <nts id="Seg_2156" n="HIAT:ip">)</nts>
                  <nts id="Seg_2157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2159" n="HIAT:w" s="T557">ara</ts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2162" n="HIAT:w" s="T558">mĭleʔbəʔjə</ts>
                  <nts id="Seg_2163" n="HIAT:ip">.</nts>
                  <nts id="Seg_2164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T565" id="Seg_2166" n="HIAT:u" s="T559">
                  <ts e="T560" id="Seg_2168" n="HIAT:w" s="T559">Dĭgəttə</ts>
                  <nts id="Seg_2169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2171" n="HIAT:w" s="T560">šobi</ts>
                  <nts id="Seg_2172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2174" n="HIAT:w" s="T561">dĭ</ts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2177" n="HIAT:w" s="T562">poslʼednij</ts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2180" n="HIAT:w" s="T563">nʼit</ts>
                  <nts id="Seg_2181" n="HIAT:ip">,</nts>
                  <nts id="Seg_2182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2184" n="HIAT:w" s="T564">dĭ</ts>
                  <nts id="Seg_2185" n="HIAT:ip">.</nts>
                  <nts id="Seg_2186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T574" id="Seg_2188" n="HIAT:u" s="T565">
                  <ts e="T566" id="Seg_2190" n="HIAT:w" s="T565">Dĭ</ts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2193" n="HIAT:w" s="T566">bar</ts>
                  <nts id="Seg_2194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2196" n="HIAT:w" s="T567">sagər</ts>
                  <nts id="Seg_2197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2199" n="HIAT:w" s="T568">amnolaʔbə</ts>
                  <nts id="Seg_2200" n="HIAT:ip">,</nts>
                  <nts id="Seg_2201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2203" n="HIAT:w" s="T569">i</ts>
                  <nts id="Seg_2204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2206" n="HIAT:w" s="T570">udat</ts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2209" n="HIAT:w" s="T571">sagər</ts>
                  <nts id="Seg_2210" n="HIAT:ip">,</nts>
                  <nts id="Seg_2211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2213" n="HIAT:w" s="T572">trʼapkazʼiʔ</ts>
                  <nts id="Seg_2214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2216" n="HIAT:w" s="T573">sarona</ts>
                  <nts id="Seg_2217" n="HIAT:ip">.</nts>
                  <nts id="Seg_2218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T578" id="Seg_2220" n="HIAT:u" s="T574">
                  <ts e="T575" id="Seg_2222" n="HIAT:w" s="T574">Măndə:</ts>
                  <nts id="Seg_2223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2225" n="HIAT:w" s="T575">Ĭmbi</ts>
                  <nts id="Seg_2226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2228" n="HIAT:w" s="T576">tăn</ts>
                  <nts id="Seg_2229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2231" n="HIAT:w" s="T577">udandə</ts>
                  <nts id="Seg_2232" n="HIAT:ip">?</nts>
                  <nts id="Seg_2233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T585" id="Seg_2235" n="HIAT:u" s="T578">
                  <nts id="Seg_2236" n="HIAT:ip">(</nts>
                  <ts e="T579" id="Seg_2238" n="HIAT:w" s="T578">Kulʼ-</ts>
                  <nts id="Seg_2239" n="HIAT:ip">)</nts>
                  <nts id="Seg_2240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2241" n="HIAT:ip">(</nts>
                  <ts e="T580" id="Seg_2243" n="HIAT:w" s="T579">Kuliat</ts>
                  <nts id="Seg_2244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2246" n="HIAT:w" s="T580">bar</ts>
                  <nts id="Seg_2247" n="HIAT:ip">)</nts>
                  <nts id="Seg_2248" n="HIAT:ip">,</nts>
                  <nts id="Seg_2249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2251" n="HIAT:w" s="T581">bar</ts>
                  <nts id="Seg_2252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2254" n="HIAT:w" s="T582">dĭn</ts>
                  <nts id="Seg_2255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2257" n="HIAT:w" s="T583">kalʼečka</ts>
                  <nts id="Seg_2258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2260" n="HIAT:w" s="T584">amnolaʔbə</ts>
                  <nts id="Seg_2261" n="HIAT:ip">.</nts>
                  <nts id="Seg_2262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T589" id="Seg_2264" n="HIAT:u" s="T585">
                  <ts e="T586" id="Seg_2266" n="HIAT:w" s="T585">Dĭgəttə</ts>
                  <nts id="Seg_2267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2269" n="HIAT:w" s="T586">ilet</ts>
                  <nts id="Seg_2270" n="HIAT:ip">,</nts>
                  <nts id="Seg_2271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2273" n="HIAT:w" s="T587">abandə</ts>
                  <nts id="Seg_2274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2276" n="HIAT:w" s="T588">šoliat</ts>
                  <nts id="Seg_2277" n="HIAT:ip">.</nts>
                  <nts id="Seg_2278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T594" id="Seg_2280" n="HIAT:u" s="T589">
                  <nts id="Seg_2281" n="HIAT:ip">(</nts>
                  <ts e="T590" id="Seg_2283" n="HIAT:w" s="T589">Dö</ts>
                  <nts id="Seg_2284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2286" n="HIAT:w" s="T590">m-</ts>
                  <nts id="Seg_2287" n="HIAT:ip">)</nts>
                  <nts id="Seg_2288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2290" n="HIAT:w" s="T591">Dö</ts>
                  <nts id="Seg_2291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2293" n="HIAT:w" s="T592">măn</ts>
                  <nts id="Seg_2294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2296" n="HIAT:w" s="T593">ženʼixbə</ts>
                  <nts id="Seg_2297" n="HIAT:ip">.</nts>
                  <nts id="Seg_2298" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T600" id="Seg_2300" n="HIAT:u" s="T594">
                  <ts e="T595" id="Seg_2302" n="HIAT:w" s="T594">Dĭm</ts>
                  <nts id="Seg_2303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2304" n="HIAT:ip">(</nts>
                  <ts e="T596" id="Seg_2306" n="HIAT:w" s="T595">dĭ</ts>
                  <nts id="Seg_2307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2309" n="HIAT:w" s="T596">dĭ-</ts>
                  <nts id="Seg_2310" n="HIAT:ip">)</nts>
                  <nts id="Seg_2311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2313" n="HIAT:w" s="T597">dĭzeŋ</ts>
                  <nts id="Seg_2314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2316" n="HIAT:w" s="T598">dĭn</ts>
                  <nts id="Seg_2317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2319" n="HIAT:w" s="T599">bəzəbiʔi</ts>
                  <nts id="Seg_2320" n="HIAT:ip">.</nts>
                  <nts id="Seg_2321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T604" id="Seg_2323" n="HIAT:u" s="T600">
                  <ts e="T601" id="Seg_2325" n="HIAT:w" s="T600">Kuvas</ts>
                  <nts id="Seg_2326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2328" n="HIAT:w" s="T601">oldʼa</ts>
                  <nts id="Seg_2329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2330" n="HIAT:ip">(</nts>
                  <ts e="T603" id="Seg_2332" n="HIAT:w" s="T602">s-</ts>
                  <nts id="Seg_2333" n="HIAT:ip">)</nts>
                  <nts id="Seg_2334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2336" n="HIAT:w" s="T603">šerbiʔi</ts>
                  <nts id="Seg_2337" n="HIAT:ip">.</nts>
                  <nts id="Seg_2338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T607" id="Seg_2340" n="HIAT:u" s="T604">
                  <ts e="T605" id="Seg_2342" n="HIAT:w" s="T604">Ugaːndə</ts>
                  <nts id="Seg_2343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2345" n="HIAT:w" s="T605">kuvas</ts>
                  <nts id="Seg_2346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2348" n="HIAT:w" s="T606">molaːmbi</ts>
                  <nts id="Seg_2349" n="HIAT:ip">.</nts>
                  <nts id="Seg_2350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T612" id="Seg_2352" n="HIAT:u" s="T607">
                  <ts e="T608" id="Seg_2354" n="HIAT:w" s="T607">Dĭgəttə</ts>
                  <nts id="Seg_2355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2357" n="HIAT:w" s="T608">bazoʔ</ts>
                  <nts id="Seg_2358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2360" n="HIAT:w" s="T609">davaj</ts>
                  <nts id="Seg_2361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2363" n="HIAT:w" s="T610">ara</ts>
                  <nts id="Seg_2364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2366" n="HIAT:w" s="T611">bĭssittə</ts>
                  <nts id="Seg_2367" n="HIAT:ip">.</nts>
                  <nts id="Seg_2368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T613" id="Seg_2370" n="HIAT:u" s="T612">
                  <ts e="T613" id="Seg_2372" n="HIAT:w" s="T612">Sʼarzittə</ts>
                  <nts id="Seg_2373" n="HIAT:ip">.</nts>
                  <nts id="Seg_2374" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T616" id="Seg_2376" n="HIAT:u" s="T613">
                  <ts e="T614" id="Seg_2378" n="HIAT:w" s="T613">Kagazaŋdə</ts>
                  <nts id="Seg_2379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2381" n="HIAT:w" s="T614">bar</ts>
                  <nts id="Seg_2382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2384" n="HIAT:w" s="T615">măndlaʔbəʔjə</ts>
                  <nts id="Seg_2385" n="HIAT:ip">.</nts>
                  <nts id="Seg_2386" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T619" id="Seg_2388" n="HIAT:u" s="T616">
                  <ts e="T617" id="Seg_2390" n="HIAT:w" s="T616">Girgit</ts>
                  <nts id="Seg_2391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2393" n="HIAT:w" s="T617">kuvas</ts>
                  <nts id="Seg_2394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2396" n="HIAT:w" s="T618">molaːmbi</ts>
                  <nts id="Seg_2397" n="HIAT:ip">.</nts>
                  <nts id="Seg_2398" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T623" id="Seg_2400" n="HIAT:u" s="T619">
                  <ts e="T620" id="Seg_2402" n="HIAT:w" s="T619">I</ts>
                  <nts id="Seg_2403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2405" n="HIAT:w" s="T620">dĭn</ts>
                  <nts id="Seg_2406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2408" n="HIAT:w" s="T621">măn</ts>
                  <nts id="Seg_2409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2411" n="HIAT:w" s="T622">ibiem</ts>
                  <nts id="Seg_2412" n="HIAT:ip">.</nts>
                  <nts id="Seg_2413" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T626" id="Seg_2415" n="HIAT:u" s="T623">
                  <ts e="T624" id="Seg_2417" n="HIAT:w" s="T623">Ara</ts>
                  <nts id="Seg_2418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2420" n="HIAT:w" s="T624">bar</ts>
                  <nts id="Seg_2421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2423" n="HIAT:w" s="T625">mʼaŋŋaʔbə</ts>
                  <nts id="Seg_2424" n="HIAT:ip">.</nts>
                  <nts id="Seg_2425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T629" id="Seg_2427" n="HIAT:u" s="T626">
                  <ts e="T627" id="Seg_2429" n="HIAT:w" s="T626">Mĭbiʔi</ts>
                  <nts id="Seg_2430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2432" n="HIAT:w" s="T627">măna</ts>
                  <nts id="Seg_2433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2435" n="HIAT:w" s="T628">blin</ts>
                  <nts id="Seg_2436" n="HIAT:ip">.</nts>
                  <nts id="Seg_2437" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T633" id="Seg_2439" n="HIAT:u" s="T629">
                  <ts e="T629.tx-PKZ.1" id="Seg_2441" n="HIAT:w" s="T629">Три</ts>
                  <nts id="Seg_2442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629.tx-PKZ.2" id="Seg_2444" n="HIAT:w" s="T629.tx-PKZ.1">года</ts>
                  <nts id="Seg_2445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629.tx-PKZ.3" id="Seg_2447" n="HIAT:w" s="T629.tx-PKZ.2">у</ts>
                  <nts id="Seg_2448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2450" n="HIAT:w" s="T629.tx-PKZ.3">лоханки</ts>
                  <nts id="Seg_2451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2453" n="HIAT:ip">(</nts>
                  <nts id="Seg_2454" n="HIAT:ip">(</nts>
                  <ats e="T632.tx-PKZ.1" id="Seg_2455" n="HIAT:non-pho" s="T632">LAUGH</ats>
                  <nts id="Seg_2456" n="HIAT:ip">)</nts>
                  <nts id="Seg_2457" n="HIAT:ip">)</nts>
                  <nts id="Seg_2458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2459" n="HIAT:ip">(</nts>
                  <nts id="Seg_2460" n="HIAT:ip">(</nts>
                  <ats e="T633" id="Seg_2461" n="HIAT:non-pho" s="T632.tx-PKZ.1">BRK</ats>
                  <nts id="Seg_2462" n="HIAT:ip">)</nts>
                  <nts id="Seg_2463" n="HIAT:ip">)</nts>
                  <nts id="Seg_2464" n="HIAT:ip">.</nts>
                  <nts id="Seg_2465" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T638" id="Seg_2467" n="HIAT:u" s="T633">
                  <ts e="T634" id="Seg_2469" n="HIAT:w" s="T633">Amnobiʔi</ts>
                  <nts id="Seg_2470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2472" n="HIAT:w" s="T634">šidegöʔ</ts>
                  <nts id="Seg_2473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2475" n="HIAT:w" s="T635">kum</ts>
                  <nts id="Seg_2476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2478" n="HIAT:w" s="T636">da</ts>
                  <nts id="Seg_2479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2481" n="HIAT:w" s="T637">kuma</ts>
                  <nts id="Seg_2482" n="HIAT:ip">.</nts>
                  <nts id="Seg_2483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T641" id="Seg_2485" n="HIAT:u" s="T638">
                  <ts e="T639" id="Seg_2487" n="HIAT:w" s="T638">Volk</ts>
                  <nts id="Seg_2488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2490" n="HIAT:w" s="T639">da</ts>
                  <nts id="Seg_2491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2493" n="HIAT:w" s="T640">lʼisa</ts>
                  <nts id="Seg_2494" n="HIAT:ip">.</nts>
                  <nts id="Seg_2495" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T644" id="Seg_2497" n="HIAT:u" s="T641">
                  <ts e="T642" id="Seg_2499" n="HIAT:w" s="T641">Dĭgəttə</ts>
                  <nts id="Seg_2500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2502" n="HIAT:w" s="T642">kunolzittə</ts>
                  <nts id="Seg_2503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2505" n="HIAT:w" s="T643">iʔbəʔi</ts>
                  <nts id="Seg_2506" n="HIAT:ip">.</nts>
                  <nts id="Seg_2507" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T649" id="Seg_2509" n="HIAT:u" s="T644">
                  <ts e="T645" id="Seg_2511" n="HIAT:w" s="T644">A</ts>
                  <nts id="Seg_2512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2514" n="HIAT:w" s="T645">lʼisa</ts>
                  <nts id="Seg_2515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2517" n="HIAT:w" s="T646">uge</ts>
                  <nts id="Seg_2518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2520" n="HIAT:w" s="T647">kuzurleʔbə</ts>
                  <nts id="Seg_2521" n="HIAT:ip">,</nts>
                  <nts id="Seg_2522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2524" n="HIAT:w" s="T648">kuzurleʔbə</ts>
                  <nts id="Seg_2525" n="HIAT:ip">.</nts>
                  <nts id="Seg_2526" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T659" id="Seg_2528" n="HIAT:u" s="T649">
                  <ts e="T650" id="Seg_2530" n="HIAT:w" s="T649">A</ts>
                  <nts id="Seg_2531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2533" n="HIAT:w" s="T650">volk</ts>
                  <nts id="Seg_2534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2536" n="HIAT:w" s="T651">măndə:</ts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2539" n="HIAT:w" s="T652">Šindidə</ts>
                  <nts id="Seg_2540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2542" n="HIAT:w" s="T653">kuzurleʔbə</ts>
                  <nts id="Seg_2543" n="HIAT:ip">,</nts>
                  <nts id="Seg_2544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2546" n="HIAT:w" s="T654">măna</ts>
                  <nts id="Seg_2547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2549" n="HIAT:w" s="T655">kăštəliaʔi</ts>
                  <nts id="Seg_2550" n="HIAT:ip">,</nts>
                  <nts id="Seg_2551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2552" n="HIAT:ip">(</nts>
                  <ts e="T657" id="Seg_2554" n="HIAT:w" s="T656">ešši</ts>
                  <nts id="Seg_2555" n="HIAT:ip">)</nts>
                  <nts id="Seg_2556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2558" n="HIAT:w" s="T657">ešši</ts>
                  <nts id="Seg_2559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2561" n="HIAT:w" s="T658">pădəsʼtə</ts>
                  <nts id="Seg_2562" n="HIAT:ip">.</nts>
                  <nts id="Seg_2563" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T662" id="Seg_2565" n="HIAT:u" s="T659">
                  <ts e="T660" id="Seg_2567" n="HIAT:w" s="T659">Dĭgəttə</ts>
                  <nts id="Seg_2568" n="HIAT:ip">”</nts>
                  <nts id="Seg_2569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2571" n="HIAT:w" s="T660">No</ts>
                  <nts id="Seg_2572" n="HIAT:ip">,</nts>
                  <nts id="Seg_2573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2575" n="HIAT:w" s="T661">kanaʔ</ts>
                  <nts id="Seg_2576" n="HIAT:ip">!</nts>
                  <nts id="Seg_2577" n="HIAT:ip">"</nts>
                  <nts id="Seg_2578" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T664" id="Seg_2580" n="HIAT:u" s="T662">
                  <ts e="T663" id="Seg_2582" n="HIAT:w" s="T662">Dĭ</ts>
                  <nts id="Seg_2583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2585" n="HIAT:w" s="T663">kambi</ts>
                  <nts id="Seg_2586" n="HIAT:ip">.</nts>
                  <nts id="Seg_2587" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T671" id="Seg_2589" n="HIAT:u" s="T664">
                  <ts e="T665" id="Seg_2591" n="HIAT:w" s="T664">A</ts>
                  <nts id="Seg_2592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2594" n="HIAT:w" s="T665">dĭzeŋ</ts>
                  <nts id="Seg_2595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2597" n="HIAT:w" s="T666">nulaʔbi</ts>
                  <nts id="Seg_2598" n="HIAT:ip">,</nts>
                  <nts id="Seg_2599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2601" n="HIAT:w" s="T667">mʼod</ts>
                  <nts id="Seg_2602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2604" n="HIAT:w" s="T668">ambi</ts>
                  <nts id="Seg_2605" n="HIAT:ip">,</nts>
                  <nts id="Seg_2606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2608" n="HIAT:w" s="T669">ambi</ts>
                  <nts id="Seg_2609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2611" n="HIAT:w" s="T670">mʼod</ts>
                  <nts id="Seg_2612" n="HIAT:ip">.</nts>
                  <nts id="Seg_2613" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T673" id="Seg_2615" n="HIAT:u" s="T671">
                  <ts e="T672" id="Seg_2617" n="HIAT:w" s="T671">Dĭ</ts>
                  <nts id="Seg_2618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2620" n="HIAT:w" s="T672">šobi</ts>
                  <nts id="Seg_2621" n="HIAT:ip">.</nts>
                  <nts id="Seg_2622" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T676" id="Seg_2624" n="HIAT:u" s="T673">
                  <ts e="T674" id="Seg_2626" n="HIAT:w" s="T673">Šindi</ts>
                  <nts id="Seg_2627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2629" n="HIAT:w" s="T674">dĭn</ts>
                  <nts id="Seg_2630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2632" n="HIAT:w" s="T675">ige</ts>
                  <nts id="Seg_2633" n="HIAT:ip">?</nts>
                  <nts id="Seg_2634" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T678" id="Seg_2636" n="HIAT:u" s="T676">
                  <ts e="T677" id="Seg_2638" n="HIAT:w" s="T676">Veršok</ts>
                  <nts id="Seg_2639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2641" n="HIAT:w" s="T677">ige</ts>
                  <nts id="Seg_2642" n="HIAT:ip">.</nts>
                  <nts id="Seg_2643" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T684" id="Seg_2645" n="HIAT:u" s="T678">
                  <ts e="T679" id="Seg_2647" n="HIAT:w" s="T678">Dĭgəttə</ts>
                  <nts id="Seg_2648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2650" n="HIAT:w" s="T679">bazoʔ</ts>
                  <nts id="Seg_2651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2653" n="HIAT:w" s="T680">nüdʼin</ts>
                  <nts id="Seg_2654" n="HIAT:ip">,</nts>
                  <nts id="Seg_2655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2657" n="HIAT:w" s="T681">bazoʔ</ts>
                  <nts id="Seg_2658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2660" n="HIAT:w" s="T682">kuzurleʔbə</ts>
                  <nts id="Seg_2661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2663" n="HIAT:w" s="T683">lʼisʼitsa</ts>
                  <nts id="Seg_2664" n="HIAT:ip">.</nts>
                  <nts id="Seg_2665" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T686" id="Seg_2667" n="HIAT:u" s="T684">
                  <ts e="T685" id="Seg_2669" n="HIAT:w" s="T684">Šindi</ts>
                  <nts id="Seg_2670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2672" n="HIAT:w" s="T685">kuzurleʔbə</ts>
                  <nts id="Seg_2673" n="HIAT:ip">?</nts>
                  <nts id="Seg_2674" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T691" id="Seg_2676" n="HIAT:u" s="T686">
                  <ts e="T687" id="Seg_2678" n="HIAT:w" s="T686">Da</ts>
                  <nts id="Seg_2679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2681" n="HIAT:w" s="T687">măna</ts>
                  <nts id="Seg_2682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2684" n="HIAT:w" s="T688">kăštəlaʔbəʔjə</ts>
                  <nts id="Seg_2685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2687" n="HIAT:w" s="T689">ešši</ts>
                  <nts id="Seg_2688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2689" n="HIAT:ip">(</nts>
                  <ts e="T691" id="Seg_2691" n="HIAT:w" s="T690">ködərzittə</ts>
                  <nts id="Seg_2692" n="HIAT:ip">)</nts>
                  <nts id="Seg_2693" n="HIAT:ip">.</nts>
                  <nts id="Seg_2694" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T693" id="Seg_2696" n="HIAT:u" s="T691">
                  <ts e="T692" id="Seg_2698" n="HIAT:w" s="T691">No</ts>
                  <nts id="Seg_2699" n="HIAT:ip">,</nts>
                  <nts id="Seg_2700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2702" n="HIAT:w" s="T692">kanaʔ</ts>
                  <nts id="Seg_2703" n="HIAT:ip">!</nts>
                  <nts id="Seg_2704" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T695" id="Seg_2706" n="HIAT:u" s="T693">
                  <ts e="T694" id="Seg_2708" n="HIAT:w" s="T693">Dĭ</ts>
                  <nts id="Seg_2709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2711" n="HIAT:w" s="T694">kambi</ts>
                  <nts id="Seg_2712" n="HIAT:ip">.</nts>
                  <nts id="Seg_2713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T702" id="Seg_2715" n="HIAT:u" s="T695">
                  <ts e="T696" id="Seg_2717" n="HIAT:w" s="T695">Ambi</ts>
                  <nts id="Seg_2718" n="HIAT:ip">,</nts>
                  <nts id="Seg_2719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2721" n="HIAT:w" s="T696">ambi</ts>
                  <nts id="Seg_2722" n="HIAT:ip">,</nts>
                  <nts id="Seg_2723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2725" n="HIAT:w" s="T697">dĭgəttə</ts>
                  <nts id="Seg_2726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2728" n="HIAT:w" s="T698">šobi:</ts>
                  <nts id="Seg_2729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2731" n="HIAT:w" s="T699">Šindi</ts>
                  <nts id="Seg_2732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2734" n="HIAT:w" s="T700">dĭn</ts>
                  <nts id="Seg_2735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2737" n="HIAT:w" s="T701">šobi</ts>
                  <nts id="Seg_2738" n="HIAT:ip">?</nts>
                  <nts id="Seg_2739" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T704" id="Seg_2741" n="HIAT:u" s="T702">
                  <ts e="T703" id="Seg_2743" n="HIAT:w" s="T702">Sʼerʼodušok</ts>
                  <nts id="Seg_2744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2746" n="HIAT:w" s="T703">šobi</ts>
                  <nts id="Seg_2747" n="HIAT:ip">.</nts>
                  <nts id="Seg_2748" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T709" id="Seg_2750" n="HIAT:u" s="T704">
                  <ts e="T705" id="Seg_2752" n="HIAT:w" s="T704">Dĭgəttə</ts>
                  <nts id="Seg_2753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2755" n="HIAT:w" s="T705">nagurgit</ts>
                  <nts id="Seg_2756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2758" n="HIAT:w" s="T706">nüdʼin</ts>
                  <nts id="Seg_2759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2761" n="HIAT:w" s="T707">bazoʔ</ts>
                  <nts id="Seg_2762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2764" n="HIAT:w" s="T708">kuzurleʔbə</ts>
                  <nts id="Seg_2765" n="HIAT:ip">.</nts>
                  <nts id="Seg_2766" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T715" id="Seg_2768" n="HIAT:u" s="T709">
                  <ts e="T710" id="Seg_2770" n="HIAT:w" s="T709">Dĭgəttə:</ts>
                  <nts id="Seg_2771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2773" n="HIAT:w" s="T710">Šindidə</ts>
                  <nts id="Seg_2774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2776" n="HIAT:w" s="T711">kuzurleʔbə</ts>
                  <nts id="Seg_2777" n="HIAT:ip">,</nts>
                  <nts id="Seg_2778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2780" n="HIAT:w" s="T712">da</ts>
                  <nts id="Seg_2781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2783" n="HIAT:w" s="T713">măna</ts>
                  <nts id="Seg_2784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2786" n="HIAT:w" s="T714">kăštəlaʔbəʔjə</ts>
                  <nts id="Seg_2787" n="HIAT:ip">.</nts>
                  <nts id="Seg_2788" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T717" id="Seg_2790" n="HIAT:u" s="T715">
                  <ts e="T716" id="Seg_2792" n="HIAT:w" s="T715">No</ts>
                  <nts id="Seg_2793" n="HIAT:ip">,</nts>
                  <nts id="Seg_2794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2796" n="HIAT:w" s="T716">kanaʔ</ts>
                  <nts id="Seg_2797" n="HIAT:ip">!</nts>
                  <nts id="Seg_2798" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T720" id="Seg_2800" n="HIAT:u" s="T717">
                  <nts id="Seg_2801" n="HIAT:ip">(</nts>
                  <ts e="T718" id="Seg_2803" n="HIAT:w" s="T717">Dĭgəttə</ts>
                  <nts id="Seg_2804" n="HIAT:ip">)</nts>
                  <nts id="Seg_2805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2807" n="HIAT:w" s="T718">dĭ</ts>
                  <nts id="Seg_2808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2810" n="HIAT:w" s="T719">ambi</ts>
                  <nts id="Seg_2811" n="HIAT:ip">.</nts>
                  <nts id="Seg_2812" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T722" id="Seg_2814" n="HIAT:u" s="T720">
                  <ts e="T721" id="Seg_2816" n="HIAT:w" s="T720">Bar</ts>
                  <nts id="Seg_2817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2819" n="HIAT:w" s="T721">šobi</ts>
                  <nts id="Seg_2820" n="HIAT:ip">.</nts>
                  <nts id="Seg_2821" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T724" id="Seg_2823" n="HIAT:u" s="T722">
                  <ts e="T723" id="Seg_2825" n="HIAT:w" s="T722">Šindi</ts>
                  <nts id="Seg_2826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2828" n="HIAT:w" s="T723">dĭn</ts>
                  <nts id="Seg_2829" n="HIAT:ip">?</nts>
                  <nts id="Seg_2830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T727" id="Seg_2832" n="HIAT:u" s="T724">
                  <ts e="T725" id="Seg_2834" n="HIAT:w" s="T724">Dĭ</ts>
                  <nts id="Seg_2835" n="HIAT:ip">,</nts>
                  <nts id="Seg_2836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2838" n="HIAT:w" s="T725">skrʼobušok</ts>
                  <nts id="Seg_2839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2841" n="HIAT:w" s="T726">šobi</ts>
                  <nts id="Seg_2842" n="HIAT:ip">.</nts>
                  <nts id="Seg_2843" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T732" id="Seg_2845" n="HIAT:u" s="T727">
                  <ts e="T728" id="Seg_2847" n="HIAT:w" s="T727">Dĭgəttə</ts>
                  <nts id="Seg_2848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2850" n="HIAT:w" s="T728">ertən</ts>
                  <nts id="Seg_2851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2853" n="HIAT:w" s="T729">uʔbdəbiʔi</ts>
                  <nts id="Seg_2854" n="HIAT:ip">,</nts>
                  <nts id="Seg_2855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2857" n="HIAT:w" s="T730">dĭ</ts>
                  <nts id="Seg_2858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2860" n="HIAT:w" s="T731">ĭzemnuʔbi</ts>
                  <nts id="Seg_2861" n="HIAT:ip">.</nts>
                  <nts id="Seg_2862" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T736" id="Seg_2864" n="HIAT:u" s="T732">
                  <ts e="T733" id="Seg_2866" n="HIAT:w" s="T732">Măndə:</ts>
                  <nts id="Seg_2867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2869" n="HIAT:w" s="T733">Deʔ</ts>
                  <nts id="Seg_2870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2872" n="HIAT:w" s="T734">măna</ts>
                  <nts id="Seg_2873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2875" n="HIAT:w" s="T735">nʼamgaʔi</ts>
                  <nts id="Seg_2876" n="HIAT:ip">!</nts>
                  <nts id="Seg_2877" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T738" id="Seg_2879" n="HIAT:u" s="T736">
                  <ts e="T737" id="Seg_2881" n="HIAT:w" s="T736">Dĭ</ts>
                  <nts id="Seg_2882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2884" n="HIAT:w" s="T737">kambi</ts>
                  <nts id="Seg_2885" n="HIAT:ip">.</nts>
                  <nts id="Seg_2886" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T742" id="Seg_2888" n="HIAT:u" s="T738">
                  <ts e="T739" id="Seg_2890" n="HIAT:w" s="T738">Da</ts>
                  <nts id="Seg_2891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2893" n="HIAT:w" s="T739">naga</ts>
                  <nts id="Seg_2894" n="HIAT:ip">,</nts>
                  <nts id="Seg_2895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2897" n="HIAT:w" s="T740">šindidə</ts>
                  <nts id="Seg_2898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2900" n="HIAT:w" s="T741">amnuʔpi</ts>
                  <nts id="Seg_2901" n="HIAT:ip">!</nts>
                  <nts id="Seg_2902" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T745" id="Seg_2904" n="HIAT:u" s="T742">
                  <ts e="T743" id="Seg_2906" n="HIAT:w" s="T742">Da</ts>
                  <nts id="Seg_2907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2909" n="HIAT:w" s="T743">tăn</ts>
                  <nts id="Seg_2910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2912" n="HIAT:w" s="T744">ambial</ts>
                  <nts id="Seg_2913" n="HIAT:ip">!</nts>
                  <nts id="Seg_2914" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T751" id="Seg_2916" n="HIAT:u" s="T745">
                  <ts e="T746" id="Seg_2918" n="HIAT:w" s="T745">Dĭ</ts>
                  <nts id="Seg_2919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2921" n="HIAT:w" s="T746">bar:</ts>
                  <nts id="Seg_2922" n="HIAT:ip">"</nts>
                  <nts id="Seg_2923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2925" n="HIAT:w" s="T747">Ej</ts>
                  <nts id="Seg_2926" n="HIAT:ip">,</nts>
                  <nts id="Seg_2927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2929" n="HIAT:w" s="T748">măn</ts>
                  <nts id="Seg_2930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_2932" n="HIAT:w" s="T749">ej</ts>
                  <nts id="Seg_2933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_2935" n="HIAT:w" s="T750">ambiam</ts>
                  <nts id="Seg_2936" n="HIAT:ip">!</nts>
                  <nts id="Seg_2937" n="HIAT:ip">"</nts>
                  <nts id="Seg_2938" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T761" id="Seg_2940" n="HIAT:u" s="T751">
                  <nts id="Seg_2941" n="HIAT:ip">"</nts>
                  <ts e="T752" id="Seg_2943" n="HIAT:w" s="T751">Dĭgəttə</ts>
                  <nts id="Seg_2944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2946" n="HIAT:w" s="T752">davaj</ts>
                  <nts id="Seg_2947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_2949" n="HIAT:w" s="T753">kujanə</ts>
                  <nts id="Seg_2950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_2952" n="HIAT:w" s="T754">iʔbəbeʔ</ts>
                  <nts id="Seg_2953" n="HIAT:ip">,</nts>
                  <nts id="Seg_2954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_2956" n="HIAT:w" s="T755">šində</ts>
                  <nts id="Seg_2957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_2959" n="HIAT:w" s="T756">ambi</ts>
                  <nts id="Seg_2960" n="HIAT:ip">,</nts>
                  <nts id="Seg_2961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_2963" n="HIAT:w" s="T757">dĭn</ts>
                  <nts id="Seg_2964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_2966" n="HIAT:w" s="T758">nʼamga</ts>
                  <nts id="Seg_2967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_2969" n="HIAT:w" s="T759">nanat</ts>
                  <nts id="Seg_2970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_2972" n="HIAT:w" s="T760">moləj</ts>
                  <nts id="Seg_2973" n="HIAT:ip">.</nts>
                  <nts id="Seg_2974" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T765" id="Seg_2976" n="HIAT:u" s="T761">
                  <ts e="T762" id="Seg_2978" n="HIAT:w" s="T761">Dĭgəttə</ts>
                  <nts id="Seg_2979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_2981" n="HIAT:w" s="T762">kuliat</ts>
                  <nts id="Seg_2982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_2984" n="HIAT:w" s="T763">bostə</ts>
                  <nts id="Seg_2985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2987" n="HIAT:w" s="T764">nanəndə</ts>
                  <nts id="Seg_2988" n="HIAT:ip">.</nts>
                  <nts id="Seg_2989" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T767" id="Seg_2991" n="HIAT:u" s="T765">
                  <ts e="T766" id="Seg_2993" n="HIAT:w" s="T765">Nʼamga</ts>
                  <nts id="Seg_2994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_2996" n="HIAT:w" s="T766">ige</ts>
                  <nts id="Seg_2997" n="HIAT:ip">.</nts>
                  <nts id="Seg_2998" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T772" id="Seg_3000" n="HIAT:u" s="T767">
                  <ts e="T768" id="Seg_3002" n="HIAT:w" s="T767">Dĭ</ts>
                  <nts id="Seg_3003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_3005" n="HIAT:w" s="T768">davaj</ts>
                  <nts id="Seg_3006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_3008" n="HIAT:w" s="T769">dĭ</ts>
                  <nts id="Seg_3009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_3011" n="HIAT:w" s="T770">volktə</ts>
                  <nts id="Seg_3012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_3014" n="HIAT:w" s="T771">tʼuʔsittə</ts>
                  <nts id="Seg_3015" n="HIAT:ip">.</nts>
                  <nts id="Seg_3016" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T776" id="Seg_3018" n="HIAT:u" s="T772">
                  <ts e="T773" id="Seg_3020" n="HIAT:w" s="T772">Uʔbdəʔ</ts>
                  <nts id="Seg_3021" n="HIAT:ip">,</nts>
                  <nts id="Seg_3022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_3024" n="HIAT:w" s="T773">tăn</ts>
                  <nts id="Seg_3025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3026" n="HIAT:ip">(</nts>
                  <ts e="T775" id="Seg_3028" n="HIAT:w" s="T774">š-</ts>
                  <nts id="Seg_3029" n="HIAT:ip">)</nts>
                  <nts id="Seg_3030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_3032" n="HIAT:w" s="T775">ambial</ts>
                  <nts id="Seg_3033" n="HIAT:ip">!</nts>
                  <nts id="Seg_3034" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T782" id="Seg_3036" n="HIAT:u" s="T776">
                  <ts e="T777" id="Seg_3038" n="HIAT:w" s="T776">Dĭgəttə</ts>
                  <nts id="Seg_3039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_3041" n="HIAT:w" s="T777">dĭ</ts>
                  <nts id="Seg_3042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_3044" n="HIAT:w" s="T778">uʔbdəbi:</ts>
                  <nts id="Seg_3045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_3047" n="HIAT:w" s="T779">No</ts>
                  <nts id="Seg_3048" n="HIAT:ip">,</nts>
                  <nts id="Seg_3049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_3051" n="HIAT:w" s="T780">măn</ts>
                  <nts id="Seg_3052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_3054" n="HIAT:w" s="T781">ambiam</ts>
                  <nts id="Seg_3055" n="HIAT:ip">.</nts>
                  <nts id="Seg_3056" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T784" id="Seg_3058" n="HIAT:u" s="T782">
                  <ts e="T783" id="Seg_3060" n="HIAT:w" s="T782">Dĭgəttə</ts>
                  <nts id="Seg_3061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_3063" n="HIAT:w" s="T783">bar</ts>
                  <nts id="Seg_3064" n="HIAT:ip">.</nts>
                  <nts id="Seg_3065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T788" id="Seg_3067" n="HIAT:u" s="T784">
                  <ts e="T785" id="Seg_3069" n="HIAT:w" s="T784">Süjö</ts>
                  <nts id="Seg_3070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_3072" n="HIAT:w" s="T785">i</ts>
                  <nts id="Seg_3073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_3075" n="HIAT:w" s="T786">tʼetʼer</ts>
                  <nts id="Seg_3076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3077" n="HIAT:ip">(</nts>
                  <ts e="T788" id="Seg_3079" n="HIAT:w" s="T787">amnoʔjə</ts>
                  <nts id="Seg_3080" n="HIAT:ip">)</nts>
                  <nts id="Seg_3081" n="HIAT:ip">.</nts>
                  <nts id="Seg_3082" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T791" id="Seg_3084" n="HIAT:u" s="T788">
                  <ts e="T789" id="Seg_3086" n="HIAT:w" s="T788">Kăda</ts>
                  <nts id="Seg_3087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_3089" n="HIAT:w" s="T789">tura</ts>
                  <nts id="Seg_3090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_3092" n="HIAT:w" s="T790">jaʔsittə</ts>
                  <nts id="Seg_3093" n="HIAT:ip">.</nts>
                  <nts id="Seg_3094" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T796" id="Seg_3096" n="HIAT:u" s="T791">
                  <ts e="T792" id="Seg_3098" n="HIAT:w" s="T791">Baltu</ts>
                  <nts id="Seg_3099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_3101" n="HIAT:w" s="T792">naga</ts>
                  <nts id="Seg_3102" n="HIAT:ip">,</nts>
                  <nts id="Seg_3103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_3105" n="HIAT:w" s="T793">šindi</ts>
                  <nts id="Seg_3106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_3108" n="HIAT:w" s="T794">baltu</ts>
                  <nts id="Seg_3109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3110" n="HIAT:ip">(</nts>
                  <ts e="T796" id="Seg_3112" n="HIAT:w" s="T795">aləj</ts>
                  <nts id="Seg_3113" n="HIAT:ip">)</nts>
                  <nts id="Seg_3114" n="HIAT:ip">.</nts>
                  <nts id="Seg_3115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T801" id="Seg_3117" n="HIAT:u" s="T796">
                  <ts e="T797" id="Seg_3119" n="HIAT:w" s="T796">Da</ts>
                  <nts id="Seg_3120" n="HIAT:ip">,</nts>
                  <nts id="Seg_3121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797.tx-PKZ.1" id="Seg_3123" n="HIAT:w" s="T797">na</ts>
                  <nts id="Seg_3124" n="HIAT:ip">_</nts>
                  <ts e="T798" id="Seg_3126" n="HIAT:w" s="T797.tx-PKZ.1">što</ts>
                  <nts id="Seg_3127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_3129" n="HIAT:w" s="T798">măna</ts>
                  <nts id="Seg_3130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3131" n="HIAT:ip">(</nts>
                  <ts e="T800" id="Seg_3133" n="HIAT:w" s="T799">tăra-</ts>
                  <nts id="Seg_3134" n="HIAT:ip">)</nts>
                  <nts id="Seg_3135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_3137" n="HIAT:w" s="T800">tura</ts>
                  <nts id="Seg_3138" n="HIAT:ip">.</nts>
                  <nts id="Seg_3139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T804" id="Seg_3141" n="HIAT:u" s="T801">
                  <ts e="T802" id="Seg_3143" n="HIAT:w" s="T801">Măn</ts>
                  <nts id="Seg_3144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_3146" n="HIAT:w" s="T802">sĭregən</ts>
                  <nts id="Seg_3147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_3149" n="HIAT:w" s="T803">šalam</ts>
                  <nts id="Seg_3150" n="HIAT:ip">.</nts>
                  <nts id="Seg_3151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T808" id="Seg_3153" n="HIAT:u" s="T804">
                  <ts e="T805" id="Seg_3155" n="HIAT:w" s="T804">I</ts>
                  <nts id="Seg_3156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_3158" n="HIAT:w" s="T805">saʔməluʔpi</ts>
                  <nts id="Seg_3159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3160" n="HIAT:ip">(</nts>
                  <ts e="T807" id="Seg_3162" n="HIAT:w" s="T806">šĭr-</ts>
                  <nts id="Seg_3163" n="HIAT:ip">)</nts>
                  <nts id="Seg_3164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_3166" n="HIAT:w" s="T807">sĭrenə</ts>
                  <nts id="Seg_3167" n="HIAT:ip">.</nts>
                  <nts id="Seg_3168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T812" id="Seg_3170" n="HIAT:u" s="T808">
                  <ts e="T809" id="Seg_3172" n="HIAT:w" s="T808">Dĭn</ts>
                  <nts id="Seg_3173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_3175" n="HIAT:w" s="T809">šabi</ts>
                  <nts id="Seg_3176" n="HIAT:ip">,</nts>
                  <nts id="Seg_3177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_3179" n="HIAT:w" s="T810">ertən</ts>
                  <nts id="Seg_3180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_3182" n="HIAT:w" s="T811">uʔbdəbi</ts>
                  <nts id="Seg_3183" n="HIAT:ip">.</nts>
                  <nts id="Seg_3184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T819" id="Seg_3186" n="HIAT:u" s="T812">
                  <ts e="T813" id="Seg_3188" n="HIAT:w" s="T812">I</ts>
                  <nts id="Seg_3189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_3191" n="HIAT:w" s="T813">kambi</ts>
                  <nts id="Seg_3192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_3194" n="HIAT:w" s="T814">kuzittə</ts>
                  <nts id="Seg_3195" n="HIAT:ip">,</nts>
                  <nts id="Seg_3196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_3198" n="HIAT:w" s="T815">elezeŋdə</ts>
                  <nts id="Seg_3199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_3201" n="HIAT:w" s="T816">kubi</ts>
                  <nts id="Seg_3202" n="HIAT:ip">,</nts>
                  <nts id="Seg_3203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_3205" n="HIAT:w" s="T817">dĭn</ts>
                  <nts id="Seg_3206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_3208" n="HIAT:w" s="T818">sʼarbiʔi</ts>
                  <nts id="Seg_3209" n="HIAT:ip">.</nts>
                  <nts id="Seg_3210" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T822" id="Seg_3212" n="HIAT:u" s="T819">
                  <ts e="T820" id="Seg_3214" n="HIAT:w" s="T819">Suʔmiluʔpiʔi</ts>
                  <nts id="Seg_3215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3216" n="HIAT:ip">(</nts>
                  <ts e="T821" id="Seg_3218" n="HIAT:w" s="T820">paʔn-</ts>
                  <nts id="Seg_3219" n="HIAT:ip">)</nts>
                  <nts id="Seg_3220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_3222" n="HIAT:w" s="T821">paʔinə</ts>
                  <nts id="Seg_3223" n="HIAT:ip">.</nts>
                  <nts id="Seg_3224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T826" id="Seg_3226" n="HIAT:u" s="T822">
                  <ts e="T823" id="Seg_3228" n="HIAT:w" s="T822">Dĭgəttə</ts>
                  <nts id="Seg_3229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_3231" n="HIAT:w" s="T823">boskəndə</ts>
                  <nts id="Seg_3232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3234" n="HIAT:w" s="T824">gnʼozdaʔi</ts>
                  <nts id="Seg_3235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_3237" n="HIAT:w" s="T825">abiʔi</ts>
                  <nts id="Seg_3238" n="HIAT:ip">.</nts>
                  <nts id="Seg_3239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T828" id="Seg_3241" n="HIAT:u" s="T826">
                  <ts e="T827" id="Seg_3243" n="HIAT:w" s="T826">Munuʔi</ts>
                  <nts id="Seg_3244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_3246" n="HIAT:w" s="T827">deʔpiʔi</ts>
                  <nts id="Seg_3247" n="HIAT:ip">.</nts>
                  <nts id="Seg_3248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T832" id="Seg_3250" n="HIAT:u" s="T828">
                  <ts e="T829" id="Seg_3252" n="HIAT:w" s="T828">Esseŋdə</ts>
                  <nts id="Seg_3253" n="HIAT:ip">,</nts>
                  <nts id="Seg_3254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_3256" n="HIAT:w" s="T829">mobiʔi</ts>
                  <nts id="Seg_3257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T831" id="Seg_3259" n="HIAT:w" s="T830">dĭgəttə</ts>
                  <nts id="Seg_3260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3262" n="HIAT:w" s="T831">esseŋdə</ts>
                  <nts id="Seg_3263" n="HIAT:ip">.</nts>
                  <nts id="Seg_3264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T835" id="Seg_3266" n="HIAT:u" s="T832">
                  <ts e="T833" id="Seg_3268" n="HIAT:w" s="T832">Bădəbiʔi</ts>
                  <nts id="Seg_3269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_3271" n="HIAT:w" s="T833">maškaziʔ</ts>
                  <nts id="Seg_3272" n="HIAT:ip">,</nts>
                  <nts id="Seg_3273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_3275" n="HIAT:w" s="T834">červəʔiziʔ</ts>
                  <nts id="Seg_3276" n="HIAT:ip">.</nts>
                  <nts id="Seg_3277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T840" id="Seg_3279" n="HIAT:u" s="T835">
                  <ts e="T836" id="Seg_3281" n="HIAT:w" s="T835">Dĭgəttə</ts>
                  <nts id="Seg_3282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_3284" n="HIAT:w" s="T836">bazoʔ</ts>
                  <nts id="Seg_3285" n="HIAT:ip">,</nts>
                  <nts id="Seg_3286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837.tx-PKZ.1" id="Seg_3288" n="HIAT:w" s="T837">na</ts>
                  <nts id="Seg_3289" n="HIAT:ip">_</nts>
                  <ts e="T838" id="Seg_3291" n="HIAT:w" s="T837.tx-PKZ.1">što</ts>
                  <nts id="Seg_3292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_3294" n="HIAT:w" s="T838">miʔnʼibeʔ</ts>
                  <nts id="Seg_3295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_3297" n="HIAT:w" s="T839">maʔ</ts>
                  <nts id="Seg_3298" n="HIAT:ip">?</nts>
                  <nts id="Seg_3299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T842" id="Seg_3301" n="HIAT:u" s="T840">
                  <ts e="T841" id="Seg_3303" n="HIAT:w" s="T840">Sĭregən</ts>
                  <nts id="Seg_3304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_3306" n="HIAT:w" s="T841">kunolbibaʔ</ts>
                  <nts id="Seg_3307" n="HIAT:ip">.</nts>
                  <nts id="Seg_3308" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T847" id="Seg_3310" n="HIAT:u" s="T842">
                  <ts e="T843" id="Seg_3312" n="HIAT:w" s="T842">Onʼiʔ</ts>
                  <nts id="Seg_3313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_3315" n="HIAT:w" s="T843">nüdʼi</ts>
                  <nts id="Seg_3316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_3318" n="HIAT:w" s="T844">makən</ts>
                  <nts id="Seg_3319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_3321" n="HIAT:w" s="T845">amnolbaʔ</ts>
                  <nts id="Seg_3322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3323" n="HIAT:ip">(</nts>
                  <ts e="T847" id="Seg_3325" n="HIAT:w" s="T846">dĭ</ts>
                  <nts id="Seg_3326" n="HIAT:ip">)</nts>
                  <nts id="Seg_3327" n="HIAT:ip">.</nts>
                  <nts id="Seg_3328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T850" id="Seg_3330" n="HIAT:u" s="T847">
                  <ts e="T848" id="Seg_3332" n="HIAT:w" s="T847">Bar</ts>
                  <nts id="Seg_3333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_3335" n="HIAT:w" s="T848">vʼezʼdʼe</ts>
                  <nts id="Seg_3336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_3338" n="HIAT:w" s="T849">măndərlaʔbəbaʔ</ts>
                  <nts id="Seg_3339" n="HIAT:ip">.</nts>
                  <nts id="Seg_3340" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T851" id="Seg_3342" n="HIAT:u" s="T850">
                  <ts e="T851" id="Seg_3344" n="HIAT:w" s="T850">Kabarləj</ts>
                  <nts id="Seg_3345" n="HIAT:ip">.</nts>
                  <nts id="Seg_3346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T855" id="Seg_3348" n="HIAT:u" s="T851">
                  <ts e="T852" id="Seg_3350" n="HIAT:w" s="T851">Amnobiʔi</ts>
                  <nts id="Seg_3351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_3353" n="HIAT:w" s="T852">nükke</ts>
                  <nts id="Seg_3354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_3356" n="HIAT:w" s="T853">da</ts>
                  <nts id="Seg_3357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_3359" n="HIAT:w" s="T854">büzʼe</ts>
                  <nts id="Seg_3360" n="HIAT:ip">.</nts>
                  <nts id="Seg_3361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T859" id="Seg_3363" n="HIAT:u" s="T855">
                  <nts id="Seg_3364" n="HIAT:ip">(</nts>
                  <ts e="T856" id="Seg_3366" n="HIAT:w" s="T855">Dĭ-</ts>
                  <nts id="Seg_3367" n="HIAT:ip">)</nts>
                  <nts id="Seg_3368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_3370" n="HIAT:w" s="T856">Dĭzeŋ</ts>
                  <nts id="Seg_3371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_3373" n="HIAT:w" s="T857">ĭmbidə</ts>
                  <nts id="Seg_3374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T859" id="Seg_3376" n="HIAT:w" s="T858">nagobi</ts>
                  <nts id="Seg_3377" n="HIAT:ip">.</nts>
                  <nts id="Seg_3378" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T861" id="Seg_3380" n="HIAT:u" s="T859">
                  <ts e="T860" id="Seg_3382" n="HIAT:w" s="T859">Kambiʔi</ts>
                  <nts id="Seg_3383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_3385" n="HIAT:w" s="T860">dʼijenə</ts>
                  <nts id="Seg_3386" n="HIAT:ip">.</nts>
                  <nts id="Seg_3387" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T864" id="Seg_3389" n="HIAT:u" s="T861">
                  <ts e="T862" id="Seg_3391" n="HIAT:w" s="T861">Šiškaʔi</ts>
                  <nts id="Seg_3392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_3394" n="HIAT:w" s="T862">žoludʼi</ts>
                  <nts id="Seg_3395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_3397" n="HIAT:w" s="T863">oʔbdobiʔi</ts>
                  <nts id="Seg_3398" n="HIAT:ip">.</nts>
                  <nts id="Seg_3399" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T867" id="Seg_3401" n="HIAT:u" s="T864">
                  <ts e="T865" id="Seg_3403" n="HIAT:w" s="T864">Šobiʔi</ts>
                  <nts id="Seg_3404" n="HIAT:ip">,</nts>
                  <nts id="Seg_3405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_3407" n="HIAT:w" s="T865">amnaʔbə</ts>
                  <nts id="Seg_3408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_3410" n="HIAT:w" s="T866">bar</ts>
                  <nts id="Seg_3411" n="HIAT:ip">.</nts>
                  <nts id="Seg_3412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T870" id="Seg_3414" n="HIAT:u" s="T867">
                  <ts e="T868" id="Seg_3416" n="HIAT:w" s="T867">Onʼiʔ</ts>
                  <nts id="Seg_3417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_3419" n="HIAT:w" s="T868">saʔməluʔpi</ts>
                  <nts id="Seg_3420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_3422" n="HIAT:w" s="T869">patpolʼlʼagən</ts>
                  <nts id="Seg_3423" n="HIAT:ip">.</nts>
                  <nts id="Seg_3424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T873" id="Seg_3426" n="HIAT:u" s="T870">
                  <ts e="T871" id="Seg_3428" n="HIAT:w" s="T870">Dĭgəttə</ts>
                  <nts id="Seg_3429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_3431" n="HIAT:w" s="T871">davaj</ts>
                  <nts id="Seg_3432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_3434" n="HIAT:w" s="T872">özerzittə</ts>
                  <nts id="Seg_3435" n="HIAT:ip">.</nts>
                  <nts id="Seg_3436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T876" id="Seg_3438" n="HIAT:u" s="T873">
                  <nts id="Seg_3439" n="HIAT:ip">(</nts>
                  <ts e="T874" id="Seg_3441" n="HIAT:w" s="T873">Nuge</ts>
                  <nts id="Seg_3442" n="HIAT:ip">)</nts>
                  <nts id="Seg_3443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_3445" n="HIAT:w" s="T874">Nükke</ts>
                  <nts id="Seg_3446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_3448" n="HIAT:w" s="T875">măndə</ts>
                  <nts id="Seg_3449" n="HIAT:ip">.</nts>
                  <nts id="Seg_3450" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T879" id="Seg_3452" n="HIAT:u" s="T876">
                  <ts e="T877" id="Seg_3454" n="HIAT:w" s="T876">Baltuzʼiʔ</ts>
                  <nts id="Seg_3455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_3457" n="HIAT:w" s="T877">jaʔdə</ts>
                  <nts id="Seg_3458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_3460" n="HIAT:w" s="T878">poldə</ts>
                  <nts id="Seg_3461" n="HIAT:ip">.</nts>
                  <nts id="Seg_3462" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T881" id="Seg_3464" n="HIAT:u" s="T879">
                  <ts e="T880" id="Seg_3466" n="HIAT:w" s="T879">Dĭ</ts>
                  <nts id="Seg_3467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T881" id="Seg_3469" n="HIAT:w" s="T880">jaʔpi</ts>
                  <nts id="Seg_3470" n="HIAT:ip">.</nts>
                  <nts id="Seg_3471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T884" id="Seg_3473" n="HIAT:u" s="T881">
                  <ts e="T882" id="Seg_3475" n="HIAT:w" s="T881">Dĭ</ts>
                  <nts id="Seg_3476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_3478" n="HIAT:w" s="T882">döbər</ts>
                  <nts id="Seg_3479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_3481" n="HIAT:w" s="T883">özerbi</ts>
                  <nts id="Seg_3482" n="HIAT:ip">.</nts>
                  <nts id="Seg_3483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T893" id="Seg_3485" n="HIAT:u" s="T884">
                  <ts e="T885" id="Seg_3487" n="HIAT:w" s="T884">Tuj</ts>
                  <nts id="Seg_3488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_3490" n="HIAT:w" s="T885">dĭn</ts>
                  <nts id="Seg_3491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887" id="Seg_3493" n="HIAT:w" s="T886">nʼuʔdə</ts>
                  <nts id="Seg_3494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T888" id="Seg_3496" n="HIAT:w" s="T887">bar</ts>
                  <nts id="Seg_3497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3498" n="HIAT:ip">(</nts>
                  <ts e="T889" id="Seg_3500" n="HIAT:w" s="T888">oʔbdo=</ts>
                  <nts id="Seg_3501" n="HIAT:ip">)</nts>
                  <nts id="Seg_3502" n="HIAT:ip">,</nts>
                  <nts id="Seg_3503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_3505" n="HIAT:w" s="T889">dĭ</ts>
                  <nts id="Seg_3506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_3508" n="HIAT:w" s="T890">dibər</ts>
                  <nts id="Seg_3509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T892" id="Seg_3511" n="HIAT:w" s="T891">krɨšabə</ts>
                  <nts id="Seg_3512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_3514" n="HIAT:w" s="T892">snʼal</ts>
                  <nts id="Seg_3515" n="HIAT:ip">.</nts>
                  <nts id="Seg_3516" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T898" id="Seg_3518" n="HIAT:u" s="T893">
                  <ts e="T894" id="Seg_3520" n="HIAT:w" s="T893">Dibər</ts>
                  <nts id="Seg_3521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_3523" n="HIAT:w" s="T894">kuŋgeŋ</ts>
                  <nts id="Seg_3524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_3526" n="HIAT:w" s="T895">özerbi</ts>
                  <nts id="Seg_3527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_3529" n="HIAT:w" s="T896">daže</ts>
                  <nts id="Seg_3530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_3532" n="HIAT:w" s="T897">nʼuʔdə</ts>
                  <nts id="Seg_3533" n="HIAT:ip">.</nts>
                  <nts id="Seg_3534" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T904" id="Seg_3536" n="HIAT:u" s="T898">
                  <ts e="T899" id="Seg_3538" n="HIAT:w" s="T898">Dĭgəttə</ts>
                  <nts id="Seg_3539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_3541" n="HIAT:w" s="T899">büzʼe:</ts>
                  <nts id="Seg_3542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_3544" n="HIAT:w" s="T900">Nada</ts>
                  <nts id="Seg_3545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_3547" n="HIAT:w" s="T901">kanzittə</ts>
                  <nts id="Seg_3548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_3550" n="HIAT:w" s="T902">oʔbdəsʼtə</ts>
                  <nts id="Seg_3551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T904" id="Seg_3553" n="HIAT:w" s="T903">žoludʼi</ts>
                  <nts id="Seg_3554" n="HIAT:ip">.</nts>
                  <nts id="Seg_3555" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T911" id="Seg_3557" n="HIAT:u" s="T904">
                  <ts e="T905" id="Seg_3559" n="HIAT:w" s="T904">Sʼabi</ts>
                  <nts id="Seg_3560" n="HIAT:ip">,</nts>
                  <nts id="Seg_3561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_3563" n="HIAT:w" s="T905">sʼabi</ts>
                  <nts id="Seg_3564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3565" n="HIAT:ip">(</nts>
                  <ts e="T907" id="Seg_3567" n="HIAT:w" s="T906">uj-</ts>
                  <nts id="Seg_3568" n="HIAT:ip">)</nts>
                  <nts id="Seg_3569" n="HIAT:ip">,</nts>
                  <nts id="Seg_3570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_3572" n="HIAT:w" s="T907">dibər</ts>
                  <nts id="Seg_3573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_3575" n="HIAT:w" s="T908">nʼuʔdə</ts>
                  <nts id="Seg_3576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_3578" n="HIAT:w" s="T909">dĭn</ts>
                  <nts id="Seg_3579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_3581" n="HIAT:w" s="T910">kulaʔbə</ts>
                  <nts id="Seg_3582" n="HIAT:ip">.</nts>
                  <nts id="Seg_3583" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T914" id="Seg_3585" n="HIAT:u" s="T911">
                  <ts e="T912" id="Seg_3587" n="HIAT:w" s="T911">Tura</ts>
                  <nts id="Seg_3588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3589" n="HIAT:ip">(</nts>
                  <ts e="T913" id="Seg_3591" n="HIAT:w" s="T912">i-</ts>
                  <nts id="Seg_3592" n="HIAT:ip">)</nts>
                  <nts id="Seg_3593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T914" id="Seg_3595" n="HIAT:w" s="T913">nuga</ts>
                  <nts id="Seg_3596" n="HIAT:ip">.</nts>
                  <nts id="Seg_3597" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T917" id="Seg_3599" n="HIAT:u" s="T914">
                  <ts e="T915" id="Seg_3601" n="HIAT:w" s="T914">Dibər</ts>
                  <nts id="Seg_3602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_3604" n="HIAT:w" s="T915">turanə</ts>
                  <nts id="Seg_3605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T917" id="Seg_3607" n="HIAT:w" s="T916">šobi</ts>
                  <nts id="Seg_3608" n="HIAT:ip">.</nts>
                  <nts id="Seg_3609" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T921" id="Seg_3611" n="HIAT:u" s="T917">
                  <ts e="T918" id="Seg_3613" n="HIAT:w" s="T917">Dĭn</ts>
                  <nts id="Seg_3614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T919" id="Seg_3616" n="HIAT:w" s="T918">kurizən</ts>
                  <nts id="Seg_3617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T920" id="Seg_3619" n="HIAT:w" s="T919">tibi</ts>
                  <nts id="Seg_3620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T921" id="Seg_3622" n="HIAT:w" s="T920">amnolaʔbə</ts>
                  <nts id="Seg_3623" n="HIAT:ip">.</nts>
                  <nts id="Seg_3624" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T923" id="Seg_3626" n="HIAT:u" s="T921">
                  <ts e="T922" id="Seg_3628" n="HIAT:w" s="T921">Ulut</ts>
                  <nts id="Seg_3629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_3631" n="HIAT:w" s="T922">kuvas</ts>
                  <nts id="Seg_3632" n="HIAT:ip">.</nts>
                  <nts id="Seg_3633" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T925" id="Seg_3635" n="HIAT:u" s="T923">
                  <ts e="T924" id="Seg_3637" n="HIAT:w" s="T923">Komu</ts>
                  <nts id="Seg_3638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T925" id="Seg_3640" n="HIAT:w" s="T924">bar</ts>
                  <nts id="Seg_3641" n="HIAT:ip">.</nts>
                  <nts id="Seg_3642" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T936" id="Seg_3644" n="HIAT:u" s="T925">
                  <ts e="T926" id="Seg_3646" n="HIAT:w" s="T925">I</ts>
                  <nts id="Seg_3647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T927" id="Seg_3649" n="HIAT:w" s="T926">dĭn</ts>
                  <nts id="Seg_3650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3651" n="HIAT:ip">(</nts>
                  <ts e="T928" id="Seg_3653" n="HIAT:w" s="T927">žernovkazaŋdə</ts>
                  <nts id="Seg_3654" n="HIAT:ip">)</nts>
                  <nts id="Seg_3655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T929" id="Seg_3657" n="HIAT:w" s="T928">ige</ts>
                  <nts id="Seg_3658" n="HIAT:ip">,</nts>
                  <nts id="Seg_3659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_3661" n="HIAT:w" s="T929">dĭ</ts>
                  <nts id="Seg_3662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T931" id="Seg_3664" n="HIAT:w" s="T930">ibi</ts>
                  <nts id="Seg_3665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T932" id="Seg_3667" n="HIAT:w" s="T931">dĭm</ts>
                  <nts id="Seg_3668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T933" id="Seg_3670" n="HIAT:w" s="T932">i</ts>
                  <nts id="Seg_3671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T934" id="Seg_3673" n="HIAT:w" s="T933">šobi:</ts>
                  <nts id="Seg_3674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_3676" n="HIAT:w" s="T934">Na</ts>
                  <nts id="Seg_3677" n="HIAT:ip">,</nts>
                  <nts id="Seg_3678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T936" id="Seg_3680" n="HIAT:w" s="T935">tănan</ts>
                  <nts id="Seg_3681" n="HIAT:ip">.</nts>
                  <nts id="Seg_3682" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T940" id="Seg_3684" n="HIAT:u" s="T936">
                  <ts e="T937" id="Seg_3686" n="HIAT:w" s="T936">Kurizən</ts>
                  <nts id="Seg_3687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T938" id="Seg_3689" n="HIAT:w" s="T937">tibi</ts>
                  <nts id="Seg_3690" n="HIAT:ip">,</nts>
                  <nts id="Seg_3691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T939" id="Seg_3693" n="HIAT:w" s="T938">komu</ts>
                  <nts id="Seg_3694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T940" id="Seg_3696" n="HIAT:w" s="T939">ulut</ts>
                  <nts id="Seg_3697" n="HIAT:ip">.</nts>
                  <nts id="Seg_3698" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T948" id="Seg_3700" n="HIAT:u" s="T940">
                  <ts e="T941" id="Seg_3702" n="HIAT:w" s="T940">Dĭ</ts>
                  <nts id="Seg_3703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T942" id="Seg_3705" n="HIAT:w" s="T941">ibi</ts>
                  <nts id="Seg_3706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_3708" n="HIAT:w" s="T942">da</ts>
                  <nts id="Seg_3709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T944" id="Seg_3711" n="HIAT:w" s="T943">davaj</ts>
                  <nts id="Seg_3712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3713" n="HIAT:ip">(</nts>
                  <ts e="T945" id="Seg_3715" n="HIAT:w" s="T944">žernovka=</ts>
                  <nts id="Seg_3716" n="HIAT:ip">)</nts>
                  <nts id="Seg_3717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_3719" n="HIAT:w" s="T945">dĭ</ts>
                  <nts id="Seg_3720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947" id="Seg_3722" n="HIAT:w" s="T946">tʼerməndə</ts>
                  <nts id="Seg_3723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_3725" n="HIAT:w" s="T947">kurzittə</ts>
                  <nts id="Seg_3726" n="HIAT:ip">.</nts>
                  <nts id="Seg_3727" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T958" id="Seg_3729" n="HIAT:u" s="T948">
                  <ts e="T949" id="Seg_3731" n="HIAT:w" s="T948">Dĭn</ts>
                  <nts id="Seg_3732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T950" id="Seg_3734" n="HIAT:w" s="T949">blinʔi</ts>
                  <nts id="Seg_3735" n="HIAT:ip">,</nts>
                  <nts id="Seg_3736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T951" id="Seg_3738" n="HIAT:w" s="T950">pirogəʔi</ts>
                  <nts id="Seg_3739" n="HIAT:ip">,</nts>
                  <nts id="Seg_3740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T952" id="Seg_3742" n="HIAT:w" s="T951">blinʔi</ts>
                  <nts id="Seg_3743" n="HIAT:ip">,</nts>
                  <nts id="Seg_3744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T953" id="Seg_3746" n="HIAT:w" s="T952">pirogəʔi</ts>
                  <nts id="Seg_3747" n="HIAT:ip">,</nts>
                  <nts id="Seg_3748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T954" id="Seg_3750" n="HIAT:w" s="T953">büzʼe</ts>
                  <nts id="Seg_3751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T955" id="Seg_3753" n="HIAT:w" s="T954">ambi</ts>
                  <nts id="Seg_3754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T956" id="Seg_3756" n="HIAT:w" s="T955">i</ts>
                  <nts id="Seg_3757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T957" id="Seg_3759" n="HIAT:w" s="T956">nükke</ts>
                  <nts id="Seg_3760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T958" id="Seg_3762" n="HIAT:w" s="T957">ambi</ts>
                  <nts id="Seg_3763" n="HIAT:ip">.</nts>
                  <nts id="Seg_3764" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T965" id="Seg_3766" n="HIAT:u" s="T958">
                  <ts e="T959" id="Seg_3768" n="HIAT:w" s="T958">Uge</ts>
                  <nts id="Seg_3769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T960" id="Seg_3771" n="HIAT:w" s="T959">dăra</ts>
                  <nts id="Seg_3772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T961" id="Seg_3774" n="HIAT:w" s="T960">amnolaʔ</ts>
                  <nts id="Seg_3775" n="HIAT:ip">,</nts>
                  <nts id="Seg_3776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T962" id="Seg_3778" n="HIAT:w" s="T961">dĭgəttə</ts>
                  <nts id="Seg_3779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T963" id="Seg_3781" n="HIAT:w" s="T962">šobi</ts>
                  <nts id="Seg_3782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964" id="Seg_3784" n="HIAT:w" s="T963">dĭzeŋdə</ts>
                  <nts id="Seg_3785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_3787" n="HIAT:w" s="T964">barin</ts>
                  <nts id="Seg_3788" n="HIAT:ip">.</nts>
                  <nts id="Seg_3789" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T968" id="Seg_3791" n="HIAT:u" s="T965">
                  <ts e="T966" id="Seg_3793" n="HIAT:w" s="T965">Deʔkeʔ</ts>
                  <nts id="Seg_3794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T967" id="Seg_3796" n="HIAT:w" s="T966">măna</ts>
                  <nts id="Seg_3797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T968" id="Seg_3799" n="HIAT:w" s="T967">amzittə</ts>
                  <nts id="Seg_3800" n="HIAT:ip">.</nts>
                  <nts id="Seg_3801" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T976" id="Seg_3803" n="HIAT:u" s="T968">
                  <ts e="T969" id="Seg_3805" n="HIAT:w" s="T968">Dĭ</ts>
                  <nts id="Seg_3806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T970" id="Seg_3808" n="HIAT:w" s="T969">nünəbi</ts>
                  <nts id="Seg_3809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T971" id="Seg_3811" n="HIAT:w" s="T970">pravda</ts>
                  <nts id="Seg_3812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T972" id="Seg_3814" n="HIAT:w" s="T971">dĭ</ts>
                  <nts id="Seg_3815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3816" n="HIAT:ip">(</nts>
                  <ts e="T973" id="Seg_3818" n="HIAT:w" s="T972">di-</ts>
                  <nts id="Seg_3819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T974" id="Seg_3821" n="HIAT:w" s="T973">dĭ</ts>
                  <nts id="Seg_3822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T975" id="Seg_3824" n="HIAT:w" s="T974">zər-</ts>
                  <nts id="Seg_3825" n="HIAT:ip">)</nts>
                  <nts id="Seg_3826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T976" id="Seg_3828" n="HIAT:w" s="T975">tʼermən</ts>
                  <nts id="Seg_3829" n="HIAT:ip">.</nts>
                  <nts id="Seg_3830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T984" id="Seg_3832" n="HIAT:u" s="T976">
                  <ts e="T977" id="Seg_3834" n="HIAT:w" s="T976">Dĭgəttə</ts>
                  <nts id="Seg_3835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T978" id="Seg_3837" n="HIAT:w" s="T977">nüket</ts>
                  <nts id="Seg_3838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T979" id="Seg_3840" n="HIAT:w" s="T978">bar</ts>
                  <nts id="Seg_3841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T980" id="Seg_3843" n="HIAT:w" s="T979">davaj</ts>
                  <nts id="Seg_3844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T981" id="Seg_3846" n="HIAT:w" s="T980">tʼerməndə</ts>
                  <nts id="Seg_3847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T982" id="Seg_3849" n="HIAT:w" s="T981">kurzittə</ts>
                  <nts id="Seg_3850" n="HIAT:ip">,</nts>
                  <nts id="Seg_3851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T983" id="Seg_3853" n="HIAT:w" s="T982">blinʔi</ts>
                  <nts id="Seg_3854" n="HIAT:ip">,</nts>
                  <nts id="Seg_3855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T984" id="Seg_3857" n="HIAT:w" s="T983">pirogəʔi</ts>
                  <nts id="Seg_3858" n="HIAT:ip">.</nts>
                  <nts id="Seg_3859" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T986" id="Seg_3861" n="HIAT:u" s="T984">
                  <ts e="T985" id="Seg_3863" n="HIAT:w" s="T984">Dĭ</ts>
                  <nts id="Seg_3864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T986" id="Seg_3866" n="HIAT:w" s="T985">ambi</ts>
                  <nts id="Seg_3867" n="HIAT:ip">.</nts>
                  <nts id="Seg_3868" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T988" id="Seg_3870" n="HIAT:u" s="T986">
                  <ts e="T987" id="Seg_3872" n="HIAT:w" s="T986">Sadargaʔ</ts>
                  <nts id="Seg_3873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T988" id="Seg_3875" n="HIAT:w" s="T987">măna</ts>
                  <nts id="Seg_3876" n="HIAT:ip">.</nts>
                  <nts id="Seg_3877" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T992" id="Seg_3879" n="HIAT:u" s="T988">
                  <ts e="T989" id="Seg_3881" n="HIAT:w" s="T988">Dʼok</ts>
                  <nts id="Seg_3882" n="HIAT:ip">,</nts>
                  <nts id="Seg_3883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T990" id="Seg_3885" n="HIAT:w" s="T989">miʔ</ts>
                  <nts id="Seg_3886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T991" id="Seg_3888" n="HIAT:w" s="T990">ej</ts>
                  <nts id="Seg_3889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T992" id="Seg_3891" n="HIAT:w" s="T991">sadarlam</ts>
                  <nts id="Seg_3892" n="HIAT:ip">.</nts>
                  <nts id="Seg_3893" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T997" id="Seg_3895" n="HIAT:u" s="T992">
                  <ts e="T992.tx-PKZ.1" id="Seg_3897" n="HIAT:w" s="T992">A</ts>
                  <nts id="Seg_3898" n="HIAT:ip">_</nts>
                  <ts e="T993" id="Seg_3900" n="HIAT:w" s="T992.tx-PKZ.1">to</ts>
                  <nts id="Seg_3901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3902" n="HIAT:ip">(</nts>
                  <ts e="T994" id="Seg_3904" n="HIAT:w" s="T993">n-</ts>
                  <nts id="Seg_3905" n="HIAT:ip">)</nts>
                  <nts id="Seg_3906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T995" id="Seg_3908" n="HIAT:w" s="T994">miʔnʼibeʔ</ts>
                  <nts id="Seg_3909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T996" id="Seg_3911" n="HIAT:w" s="T995">amzittə</ts>
                  <nts id="Seg_3912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T997" id="Seg_3914" n="HIAT:w" s="T996">ĭmbi</ts>
                  <nts id="Seg_3915" n="HIAT:ip">?</nts>
                  <nts id="Seg_3916" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1001" id="Seg_3918" n="HIAT:u" s="T997">
                  <ts e="T998" id="Seg_3920" n="HIAT:w" s="T997">Dĭgəttə</ts>
                  <nts id="Seg_3921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T999" id="Seg_3923" n="HIAT:w" s="T998">dĭ</ts>
                  <nts id="Seg_3924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1000" id="Seg_3926" n="HIAT:w" s="T999">nüdʼin</ts>
                  <nts id="Seg_3927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1001" id="Seg_3929" n="HIAT:w" s="T1000">šobi</ts>
                  <nts id="Seg_3930" n="HIAT:ip">.</nts>
                  <nts id="Seg_3931" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1004" id="Seg_3933" n="HIAT:u" s="T1001">
                  <ts e="T1002" id="Seg_3935" n="HIAT:w" s="T1001">I</ts>
                  <nts id="Seg_3936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1003" id="Seg_3938" n="HIAT:w" s="T1002">tojirlaʔ</ts>
                  <nts id="Seg_3939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1004" id="Seg_3941" n="HIAT:w" s="T1003">iluʔpi</ts>
                  <nts id="Seg_3942" n="HIAT:ip">.</nts>
                  <nts id="Seg_3943" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1006" id="Seg_3945" n="HIAT:u" s="T1004">
                  <ts e="T1005" id="Seg_3947" n="HIAT:w" s="T1004">I</ts>
                  <nts id="Seg_3948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1006" id="Seg_3950" n="HIAT:w" s="T1005">konnaːmbi</ts>
                  <nts id="Seg_3951" n="HIAT:ip">.</nts>
                  <nts id="Seg_3952" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1011" id="Seg_3954" n="HIAT:u" s="T1006">
                  <ts e="T1007" id="Seg_3956" n="HIAT:w" s="T1006">Ertən</ts>
                  <nts id="Seg_3957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1008" id="Seg_3959" n="HIAT:w" s="T1007">dĭzeŋ</ts>
                  <nts id="Seg_3960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1009" id="Seg_3962" n="HIAT:w" s="T1008">uʔbdəbiʔi:</ts>
                  <nts id="Seg_3963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1010" id="Seg_3965" n="HIAT:w" s="T1009">naga</ts>
                  <nts id="Seg_3966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1011" id="Seg_3968" n="HIAT:w" s="T1010">tʼerməndə</ts>
                  <nts id="Seg_3969" n="HIAT:ip">.</nts>
                  <nts id="Seg_3970" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1016" id="Seg_3972" n="HIAT:u" s="T1011">
                  <ts e="T1012" id="Seg_3974" n="HIAT:w" s="T1011">Dĭgəttə</ts>
                  <nts id="Seg_3975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1013" id="Seg_3977" n="HIAT:w" s="T1012">tibi</ts>
                  <nts id="Seg_3978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1014" id="Seg_3980" n="HIAT:w" s="T1013">kuriz:</ts>
                  <nts id="Seg_3981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1015" id="Seg_3983" n="HIAT:w" s="T1014">Iʔ</ts>
                  <nts id="Seg_3984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1016" id="Seg_3986" n="HIAT:w" s="T1015">dʼorgaʔ</ts>
                  <nts id="Seg_3987" n="HIAT:ip">!</nts>
                  <nts id="Seg_3988" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1018" id="Seg_3990" n="HIAT:u" s="T1016">
                  <ts e="T1017" id="Seg_3992" n="HIAT:w" s="T1016">Măn</ts>
                  <nts id="Seg_3993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1018" id="Seg_3995" n="HIAT:w" s="T1017">kallam</ts>
                  <nts id="Seg_3996" n="HIAT:ip">.</nts>
                  <nts id="Seg_3997" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1022" id="Seg_3999" n="HIAT:u" s="T1018">
                  <nts id="Seg_4000" n="HIAT:ip">(</nts>
                  <ts e="T1019" id="Seg_4002" n="HIAT:w" s="T1018">Di-</ts>
                  <nts id="Seg_4003" n="HIAT:ip">)</nts>
                  <nts id="Seg_4004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1020" id="Seg_4006" n="HIAT:w" s="T1019">Detlim</ts>
                  <nts id="Seg_4007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1021" id="Seg_4009" n="HIAT:w" s="T1020">bazoʔ</ts>
                  <nts id="Seg_4010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1022" id="Seg_4012" n="HIAT:w" s="T1021">šiʔnʼileʔ</ts>
                  <nts id="Seg_4013" n="HIAT:ip">.</nts>
                  <nts id="Seg_4014" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1023" id="Seg_4016" n="HIAT:u" s="T1022">
                  <ts e="T1023" id="Seg_4018" n="HIAT:w" s="T1022">Kambi</ts>
                  <nts id="Seg_4019" n="HIAT:ip">.</nts>
                  <nts id="Seg_4020" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1028" id="Seg_4022" n="HIAT:u" s="T1023">
                  <ts e="T1024" id="Seg_4024" n="HIAT:w" s="T1023">Kandəga</ts>
                  <nts id="Seg_4025" n="HIAT:ip">,</nts>
                  <nts id="Seg_4026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1025" id="Seg_4028" n="HIAT:w" s="T1024">kandəga</ts>
                  <nts id="Seg_4029" n="HIAT:ip">,</nts>
                  <nts id="Seg_4030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1026" id="Seg_4032" n="HIAT:w" s="T1025">šonəga</ts>
                  <nts id="Seg_4033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1027" id="Seg_4035" n="HIAT:w" s="T1026">diʔnə</ts>
                  <nts id="Seg_4036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1028" id="Seg_4038" n="HIAT:w" s="T1027">lʼisʼitsa</ts>
                  <nts id="Seg_4039" n="HIAT:ip">.</nts>
                  <nts id="Seg_4040" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1030" id="Seg_4042" n="HIAT:u" s="T1028">
                  <ts e="T1029" id="Seg_4044" n="HIAT:w" s="T1028">Gibər</ts>
                  <nts id="Seg_4045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1030" id="Seg_4047" n="HIAT:w" s="T1029">kandəgal</ts>
                  <nts id="Seg_4048" n="HIAT:ip">?</nts>
                  <nts id="Seg_4049" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1032" id="Seg_4051" n="HIAT:u" s="T1030">
                  <ts e="T1031" id="Seg_4053" n="HIAT:w" s="T1030">Iʔ</ts>
                  <nts id="Seg_4054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1032" id="Seg_4056" n="HIAT:w" s="T1031">măna</ts>
                  <nts id="Seg_4057" n="HIAT:ip">!</nts>
                  <nts id="Seg_4058" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1037" id="Seg_4060" n="HIAT:u" s="T1032">
                  <ts e="T1033" id="Seg_4062" n="HIAT:w" s="T1032">Bădaʔ</ts>
                  <nts id="Seg_4063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1034" id="Seg_4065" n="HIAT:w" s="T1033">măna</ts>
                  <nts id="Seg_4066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1035" id="Seg_4068" n="HIAT:w" s="T1034">šüjöndə</ts>
                  <nts id="Seg_4069" n="HIAT:ip">,</nts>
                  <nts id="Seg_4070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1036" id="Seg_4072" n="HIAT:w" s="T1035">dĭ</ts>
                  <nts id="Seg_4073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1037" id="Seg_4075" n="HIAT:w" s="T1036">băʔpi</ts>
                  <nts id="Seg_4076" n="HIAT:ip">.</nts>
                  <nts id="Seg_4077" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1040" id="Seg_4079" n="HIAT:u" s="T1037">
                  <ts e="T1038" id="Seg_4081" n="HIAT:w" s="T1037">Dĭgəttə</ts>
                  <nts id="Seg_4082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1039" id="Seg_4084" n="HIAT:w" s="T1038">šonəga</ts>
                  <nts id="Seg_4085" n="HIAT:ip">,</nts>
                  <nts id="Seg_4086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1040" id="Seg_4088" n="HIAT:w" s="T1039">šonəga</ts>
                  <nts id="Seg_4089" n="HIAT:ip">.</nts>
                  <nts id="Seg_4090" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1041" id="Seg_4092" n="HIAT:u" s="T1040">
                  <ts e="T1041" id="Seg_4094" n="HIAT:w" s="T1040">Volkdə</ts>
                  <nts id="Seg_4095" n="HIAT:ip">.</nts>
                  <nts id="Seg_4096" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1043" id="Seg_4098" n="HIAT:u" s="T1041">
                  <ts e="T1042" id="Seg_4100" n="HIAT:w" s="T1041">Gibər</ts>
                  <nts id="Seg_4101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1043" id="Seg_4103" n="HIAT:w" s="T1042">kandəgal</ts>
                  <nts id="Seg_4104" n="HIAT:ip">?</nts>
                  <nts id="Seg_4105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1045" id="Seg_4107" n="HIAT:u" s="T1043">
                  <ts e="T1044" id="Seg_4109" n="HIAT:w" s="T1043">Tʼerməndə</ts>
                  <nts id="Seg_4110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1045" id="Seg_4112" n="HIAT:w" s="T1044">izittə</ts>
                  <nts id="Seg_4113" n="HIAT:ip">.</nts>
                  <nts id="Seg_4114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1048" id="Seg_4116" n="HIAT:u" s="T1045">
                  <nts id="Seg_4117" n="HIAT:ip">(</nts>
                  <ts e="T1046" id="Seg_4119" n="HIAT:w" s="T1045">Mă-</ts>
                  <nts id="Seg_4120" n="HIAT:ip">)</nts>
                  <nts id="Seg_4121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1047" id="Seg_4123" n="HIAT:w" s="T1046">Iʔ</ts>
                  <nts id="Seg_4124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1048" id="Seg_4126" n="HIAT:w" s="T1047">măna</ts>
                  <nts id="Seg_4127" n="HIAT:ip">!</nts>
                  <nts id="Seg_4128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1051" id="Seg_4130" n="HIAT:u" s="T1048">
                  <ts e="T1049" id="Seg_4132" n="HIAT:w" s="T1048">Bădaʔ</ts>
                  <nts id="Seg_4133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1050" id="Seg_4135" n="HIAT:w" s="T1049">măna</ts>
                  <nts id="Seg_4136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1051" id="Seg_4138" n="HIAT:w" s="T1050">šüjöndə</ts>
                  <nts id="Seg_4139" n="HIAT:ip">!</nts>
                  <nts id="Seg_4140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1053" id="Seg_4142" n="HIAT:u" s="T1051">
                  <ts e="T1052" id="Seg_4144" n="HIAT:w" s="T1051">Dĭ</ts>
                  <nts id="Seg_4145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1053" id="Seg_4147" n="HIAT:w" s="T1052">băʔpi</ts>
                  <nts id="Seg_4148" n="HIAT:ip">.</nts>
                  <nts id="Seg_4149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1057" id="Seg_4151" n="HIAT:u" s="T1053">
                  <ts e="T1054" id="Seg_4153" n="HIAT:w" s="T1053">Dĭgəttə</ts>
                  <nts id="Seg_4154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1055" id="Seg_4156" n="HIAT:w" s="T1054">šonəga</ts>
                  <nts id="Seg_4157" n="HIAT:ip">,</nts>
                  <nts id="Seg_4158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1056" id="Seg_4160" n="HIAT:w" s="T1055">urgaːba</ts>
                  <nts id="Seg_4161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1057" id="Seg_4163" n="HIAT:w" s="T1056">šonəga</ts>
                  <nts id="Seg_4164" n="HIAT:ip">.</nts>
                  <nts id="Seg_4165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1059" id="Seg_4167" n="HIAT:u" s="T1057">
                  <ts e="T1058" id="Seg_4169" n="HIAT:w" s="T1057">Bădaʔ</ts>
                  <nts id="Seg_4170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1059" id="Seg_4172" n="HIAT:w" s="T1058">măna</ts>
                  <nts id="Seg_4173" n="HIAT:ip">!</nts>
                  <nts id="Seg_4174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1061" id="Seg_4176" n="HIAT:u" s="T1059">
                  <ts e="T1060" id="Seg_4178" n="HIAT:w" s="T1059">Dĭgəttə</ts>
                  <nts id="Seg_4179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1061" id="Seg_4181" n="HIAT:w" s="T1060">šobi</ts>
                  <nts id="Seg_4182" n="HIAT:ip">.</nts>
                  <nts id="Seg_4183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1063" id="Seg_4185" n="HIAT:u" s="T1061">
                  <ts e="T1062" id="Seg_4187" n="HIAT:w" s="T1061">Davaj</ts>
                  <nts id="Seg_4188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1063" id="Seg_4190" n="HIAT:w" s="T1062">kirgarzittə</ts>
                  <nts id="Seg_4191" n="HIAT:ip">.</nts>
                  <nts id="Seg_4192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1064" id="Seg_4194" n="HIAT:u" s="T1063">
                  <ts e="T1064" id="Seg_4196" n="HIAT:w" s="T1063">Atdaj</ts>
                  <nts id="Seg_4197" n="HIAT:ip">!</nts>
                  <nts id="Seg_4198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1067" id="Seg_4200" n="HIAT:u" s="T1064">
                  <ts e="T1065" id="Seg_4202" n="HIAT:w" s="T1064">Dettə</ts>
                  <nts id="Seg_4203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1066" id="Seg_4205" n="HIAT:w" s="T1065">măn</ts>
                  <nts id="Seg_4206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1067" id="Seg_4208" n="HIAT:w" s="T1066">tʼerməndə</ts>
                  <nts id="Seg_4209" n="HIAT:ip">.</nts>
                  <nts id="Seg_4210" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1070" id="Seg_4212" n="HIAT:u" s="T1067">
                  <ts e="T1068" id="Seg_4214" n="HIAT:w" s="T1067">Dettə</ts>
                  <nts id="Seg_4215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1069" id="Seg_4217" n="HIAT:w" s="T1068">măn</ts>
                  <nts id="Seg_4218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1070" id="Seg_4220" n="HIAT:w" s="T1069">tʼerməndə</ts>
                  <nts id="Seg_4221" n="HIAT:ip">.</nts>
                  <nts id="Seg_4222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1072" id="Seg_4224" n="HIAT:u" s="T1070">
                  <ts e="T1071" id="Seg_4226" n="HIAT:w" s="T1070">Dĭ</ts>
                  <nts id="Seg_4227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1072" id="Seg_4229" n="HIAT:w" s="T1071">măndə</ts>
                  <nts id="Seg_4230" n="HIAT:ip">.</nts>
                  <nts id="Seg_4231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1075" id="Seg_4233" n="HIAT:u" s="T1072">
                  <nts id="Seg_4234" n="HIAT:ip">(</nts>
                  <ts e="T1073" id="Seg_4236" n="HIAT:w" s="T1072">Baʔluʔ-</ts>
                  <nts id="Seg_4237" n="HIAT:ip">)</nts>
                  <nts id="Seg_4238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1074" id="Seg_4240" n="HIAT:w" s="T1073">Baʔgaʔ</ts>
                  <nts id="Seg_4241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1075" id="Seg_4243" n="HIAT:w" s="T1074">dĭm</ts>
                  <nts id="Seg_4244" n="HIAT:ip">.</nts>
                  <nts id="Seg_4245" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1078" id="Seg_4247" n="HIAT:u" s="T1075">
                  <ts e="T1076" id="Seg_4249" n="HIAT:w" s="T1075">A</ts>
                  <nts id="Seg_4250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1077" id="Seg_4252" n="HIAT:w" s="T1076">nabəʔinə</ts>
                  <nts id="Seg_4253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1078" id="Seg_4255" n="HIAT:w" s="T1077">pušaj</ts>
                  <nts id="Seg_4256" n="HIAT:ip">.</nts>
                  <nts id="Seg_4257" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1081" id="Seg_4259" n="HIAT:u" s="T1078">
                  <ts e="T1079" id="Seg_4261" n="HIAT:w" s="T1078">Nabəʔi</ts>
                  <nts id="Seg_4262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1080" id="Seg_4264" n="HIAT:w" s="T1079">amnuʔbə</ts>
                  <nts id="Seg_4265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4266" n="HIAT:ip">(</nts>
                  <ts e="T1081" id="Seg_4268" n="HIAT:w" s="T1080">ləʔi</ts>
                  <nts id="Seg_4269" n="HIAT:ip">)</nts>
                  <nts id="Seg_4270" n="HIAT:ip">.</nts>
                  <nts id="Seg_4271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1086" id="Seg_4273" n="HIAT:u" s="T1081">
                  <ts e="T1082" id="Seg_4275" n="HIAT:w" s="T1081">Dĭm</ts>
                  <nts id="Seg_4276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1083" id="Seg_4278" n="HIAT:w" s="T1082">baʔluʔpi</ts>
                  <nts id="Seg_4279" n="HIAT:ip">,</nts>
                  <nts id="Seg_4280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1084" id="Seg_4282" n="HIAT:w" s="T1083">a</ts>
                  <nts id="Seg_4283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1085" id="Seg_4285" n="HIAT:w" s="T1084">dĭ</ts>
                  <nts id="Seg_4286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4287" n="HIAT:ip">(</nts>
                  <ts e="T1086" id="Seg_4289" n="HIAT:w" s="T1085">măndə</ts>
                  <nts id="Seg_4290" n="HIAT:ip">)</nts>
                  <nts id="Seg_4291" n="HIAT:ip">.</nts>
                  <nts id="Seg_4292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PKZ">
            <ts e="T3" id="Seg_4293" n="sc" s="T0">
               <ts e="T3" id="Seg_4295" n="e" s="T0">Ты уж отпустил? </ts>
            </ts>
            <ts e="T1086" id="Seg_4296" n="sc" s="T4">
               <ts e="T5" id="Seg_4298" n="e" s="T4">Amnobi </ts>
               <ts e="T6" id="Seg_4300" n="e" s="T5">büzʼe, </ts>
               <ts e="T7" id="Seg_4302" n="e" s="T6">dĭn </ts>
               <ts e="T8" id="Seg_4304" n="e" s="T7">nagur </ts>
               <ts e="T9" id="Seg_4306" n="e" s="T8">nʼi </ts>
               <ts e="T10" id="Seg_4308" n="e" s="T9">ibi. </ts>
               <ts e="T11" id="Seg_4310" n="e" s="T10">Onʼiʔ </ts>
               <ts e="T12" id="Seg_4312" n="e" s="T11">Gavrila, </ts>
               <ts e="T13" id="Seg_4314" n="e" s="T12">onʼiʔ </ts>
               <ts e="T14" id="Seg_4316" n="e" s="T13">Danʼila, </ts>
               <ts e="T15" id="Seg_4318" n="e" s="T14">onʼiʔ </ts>
               <ts e="T16" id="Seg_4320" n="e" s="T15">Vanʼuška </ts>
               <ts e="T17" id="Seg_4322" n="e" s="T16">duračok. </ts>
               <ts e="T18" id="Seg_4324" n="e" s="T17">Dĭgəttə </ts>
               <ts e="T19" id="Seg_4326" n="e" s="T18">(dĭz-) </ts>
               <ts e="T20" id="Seg_4328" n="e" s="T19">dĭ </ts>
               <ts e="T21" id="Seg_4330" n="e" s="T20">büzʼe </ts>
               <ts e="T22" id="Seg_4332" n="e" s="T21">kuʔlaʔpi </ts>
               <ts e="T23" id="Seg_4334" n="e" s="T22">budəj. </ts>
               <ts e="T24" id="Seg_4336" n="e" s="T23">I </ts>
               <ts e="T25" id="Seg_4338" n="e" s="T24">dĭ </ts>
               <ts e="T26" id="Seg_4340" n="e" s="T25">šindidə </ts>
               <ts e="T27" id="Seg_4342" n="e" s="T26">(ka- </ts>
               <ts e="T28" id="Seg_4344" n="e" s="T27">n-) </ts>
               <ts e="T29" id="Seg_4346" n="e" s="T28">nüdʼin </ts>
               <ts e="T30" id="Seg_4348" n="e" s="T29">šoluʔjəʔ </ts>
               <ts e="T31" id="Seg_4350" n="e" s="T30">da, </ts>
               <ts e="T32" id="Seg_4352" n="e" s="T31">amnaʔbə </ts>
               <ts e="T33" id="Seg_4354" n="e" s="T32">budəj. </ts>
               <ts e="T34" id="Seg_4356" n="e" s="T33">Nada, </ts>
               <ts e="T35" id="Seg_4358" n="e" s="T34">măndə, </ts>
               <ts e="T36" id="Seg_4360" n="e" s="T35">kanzittə </ts>
               <ts e="T37" id="Seg_4362" n="e" s="T36">(mandər-) </ts>
               <ts e="T38" id="Seg_4364" n="e" s="T37">mandəsʼtə, </ts>
               <ts e="T39" id="Seg_4366" n="e" s="T38">šində </ts>
               <ts e="T40" id="Seg_4368" n="e" s="T39">amnʼiat </ts>
               <ts e="T41" id="Seg_4370" n="e" s="T40">budəj. </ts>
               <ts e="T42" id="Seg_4372" n="e" s="T41">Nada </ts>
               <ts e="T43" id="Seg_4374" n="e" s="T42">dʼabəsʼtə. </ts>
               <ts e="T44" id="Seg_4376" n="e" s="T43">Dĭgəttə </ts>
               <ts e="T45" id="Seg_4378" n="e" s="T44">kambi </ts>
               <ts e="T46" id="Seg_4380" n="e" s="T45">staršij </ts>
               <ts e="T47" id="Seg_4382" n="e" s="T46">nʼit, </ts>
               <ts e="T48" id="Seg_4384" n="e" s="T47">Gavrila, </ts>
               <ts e="T49" id="Seg_4386" n="e" s="T48">dibər. </ts>
               <ts e="T50" id="Seg_4388" n="e" s="T49">Kambi, </ts>
               <ts e="T51" id="Seg_4390" n="e" s="T50">kunolbi </ts>
               <ts e="T52" id="Seg_4392" n="e" s="T51">noʔgən, </ts>
               <ts e="T53" id="Seg_4394" n="e" s="T52">i </ts>
               <ts e="T54" id="Seg_4396" n="e" s="T53">šobi </ts>
               <ts e="T55" id="Seg_4398" n="e" s="T54">maʔnə. </ts>
               <ts e="T56" id="Seg_4400" n="e" s="T55">Amnobiam, </ts>
               <ts e="T57" id="Seg_4402" n="e" s="T56">ej </ts>
               <ts e="T58" id="Seg_4404" n="e" s="T57">kunolbiam, </ts>
               <ts e="T59" id="Seg_4406" n="e" s="T58">a </ts>
               <ts e="T60" id="Seg_4408" n="e" s="T59">šindidə </ts>
               <ts e="T61" id="Seg_4410" n="e" s="T60">ej </ts>
               <ts e="T62" id="Seg_4412" n="e" s="T61">kubiam. </ts>
               <ts e="T63" id="Seg_4414" n="e" s="T62">Dĭgəttə </ts>
               <ts e="T64" id="Seg_4416" n="e" s="T63">baška </ts>
               <ts e="T65" id="Seg_4418" n="e" s="T64">nʼit </ts>
               <ts e="T66" id="Seg_4420" n="e" s="T65">kandəga, </ts>
               <ts e="T67" id="Seg_4422" n="e" s="T66">Danʼilat. </ts>
               <ts e="T68" id="Seg_4424" n="e" s="T67">Kambi, </ts>
               <ts e="T69" id="Seg_4426" n="e" s="T68">kunolbi </ts>
               <ts e="T70" id="Seg_4428" n="e" s="T69">noʔgən. </ts>
               <ts e="T71" id="Seg_4430" n="e" s="T70">Dĭgəttə </ts>
               <ts e="T72" id="Seg_4432" n="e" s="T71">ertən </ts>
               <ts e="T73" id="Seg_4434" n="e" s="T72">šobi. </ts>
               <ts e="T74" id="Seg_4436" n="e" s="T73">Bar </ts>
               <ts e="T75" id="Seg_4438" n="e" s="T74">nudʼi </ts>
               <ts e="T76" id="Seg_4440" n="e" s="T75">ej </ts>
               <ts e="T77" id="Seg_4442" n="e" s="T76">kunolbiam, </ts>
               <ts e="T78" id="Seg_4444" n="e" s="T77">a </ts>
               <ts e="T79" id="Seg_4446" n="e" s="T78">šinimdə </ts>
               <ts e="T80" id="Seg_4448" n="e" s="T79">ej </ts>
               <ts e="T81" id="Seg_4450" n="e" s="T80">kubiom. </ts>
               <ts e="T82" id="Seg_4452" n="e" s="T81">Dĭgəttə </ts>
               <ts e="T83" id="Seg_4454" n="e" s="T82">Vanʼuška </ts>
               <ts e="T84" id="Seg_4456" n="e" s="T83">kambi. </ts>
               <ts e="T85" id="Seg_4458" n="e" s="T84">Ibi </ts>
               <ts e="T86" id="Seg_4460" n="e" s="T85">arkandə. </ts>
               <ts e="T87" id="Seg_4462" n="e" s="T86">Dĭgəttə </ts>
               <ts e="T88" id="Seg_4464" n="e" s="T87">amnobi </ts>
               <ts e="T89" id="Seg_4466" n="e" s="T88">pinə. </ts>
               <ts e="T90" id="Seg_4468" n="e" s="T89">Ej </ts>
               <ts e="T91" id="Seg_4470" n="e" s="T90">kunolia, </ts>
               <ts e="T92" id="Seg_4472" n="e" s="T91">amnolaʔbə. </ts>
               <ts e="T93" id="Seg_4474" n="e" s="T92">Kuliat: </ts>
               <ts e="T94" id="Seg_4476" n="e" s="T93">ine </ts>
               <ts e="T95" id="Seg_4478" n="e" s="T94">šobi. </ts>
               <ts e="T96" id="Seg_4480" n="e" s="T95">Vsʼaka </ts>
               <ts e="T97" id="Seg_4482" n="e" s="T96">i </ts>
               <ts e="T98" id="Seg_4484" n="e" s="T97">bar </ts>
               <ts e="T99" id="Seg_4486" n="e" s="T98">dĭn </ts>
               <ts e="T100" id="Seg_4488" n="e" s="T99">kubangən </ts>
               <ts e="T101" id="Seg_4490" n="e" s="T100">tordə </ts>
               <ts e="T102" id="Seg_4492" n="e" s="T101">kuvas. </ts>
               <ts e="T103" id="Seg_4494" n="e" s="T102">Dĭ </ts>
               <ts e="T104" id="Seg_4496" n="e" s="T103">bar </ts>
               <ts e="T105" id="Seg_4498" n="e" s="T104">baʔluʔpi </ts>
               <ts e="T106" id="Seg_4500" n="e" s="T105">arkandə </ts>
               <ts e="T107" id="Seg_4502" n="e" s="T106">dĭʔnə </ts>
               <ts e="T108" id="Seg_4504" n="e" s="T107">i </ts>
               <ts e="T109" id="Seg_4506" n="e" s="T108">dʼabəluʔpi. </ts>
               <ts e="T110" id="Seg_4508" n="e" s="T109">Dĭ </ts>
               <ts e="T111" id="Seg_4510" n="e" s="T110">suʔmiluʔpi, </ts>
               <ts e="T112" id="Seg_4512" n="e" s="T111">ej </ts>
               <ts e="T113" id="Seg_4514" n="e" s="T112">molia </ts>
               <ts e="T114" id="Seg_4516" n="e" s="T113">suʔmisʼtə. </ts>
               <ts e="T115" id="Seg_4518" n="e" s="T114">Dĭgəttə: </ts>
               <ts e="T116" id="Seg_4520" n="e" s="T115">Öʔləʔ </ts>
               <ts e="T117" id="Seg_4522" n="e" s="T116">măna! </ts>
               <ts e="T118" id="Seg_4524" n="e" s="T117">Măn </ts>
               <ts e="T119" id="Seg_4526" n="e" s="T118">tănan </ts>
               <ts e="T120" id="Seg_4528" n="e" s="T119">bar </ts>
               <ts e="T121" id="Seg_4530" n="e" s="T120">ĭmbi </ts>
               <ts e="T122" id="Seg_4532" n="e" s="T121">mĭləm. </ts>
               <ts e="T123" id="Seg_4534" n="e" s="T122">No, </ts>
               <ts e="T124" id="Seg_4536" n="e" s="T123">kădə, </ts>
               <ts e="T125" id="Seg_4538" n="e" s="T124">nörbəʔ, </ts>
               <ts e="T126" id="Seg_4540" n="e" s="T125">daška </ts>
               <ts e="T127" id="Seg_4542" n="e" s="T126">ej </ts>
               <ts e="T128" id="Seg_4544" n="e" s="T127">amnial </ts>
               <ts e="T129" id="Seg_4546" n="e" s="T128">budəj? </ts>
               <ts e="T130" id="Seg_4548" n="e" s="T129">Ej </ts>
               <ts e="T131" id="Seg_4550" n="e" s="T130">amnam! </ts>
               <ts e="T132" id="Seg_4552" n="e" s="T131">A </ts>
               <ts e="T133" id="Seg_4554" n="e" s="T132">kădə </ts>
               <ts e="T134" id="Seg_4556" n="e" s="T133">šoləl </ts>
               <ts e="T135" id="Seg_4558" n="e" s="T134">dărə </ts>
               <ts e="T136" id="Seg_4560" n="e" s="T135">măna? </ts>
               <ts e="T137" id="Seg_4562" n="e" s="T136">Šoʔ </ts>
               <ts e="T138" id="Seg_4564" n="e" s="T137">döbər, </ts>
               <ts e="T139" id="Seg_4566" n="e" s="T138">(ənuʔ) </ts>
               <ts e="T140" id="Seg_4568" n="e" s="T139">(măn-) </ts>
               <ts e="T141" id="Seg_4570" n="e" s="T140">măna. </ts>
               <ts e="T142" id="Seg_4572" n="e" s="T141">No, </ts>
               <ts e="T143" id="Seg_4574" n="e" s="T142">dĭrgit. </ts>
               <ts e="T144" id="Seg_4576" n="e" s="T143">I </ts>
               <ts e="T145" id="Seg_4578" n="e" s="T144">kalla </ts>
               <ts e="T146" id="Seg_4580" n="e" s="T145">dʼürbi </ts>
               <ts e="T147" id="Seg_4582" n="e" s="T146">inet, </ts>
               <ts e="T148" id="Seg_4584" n="e" s="T147">a </ts>
               <ts e="T149" id="Seg_4586" n="e" s="T148">dĭ </ts>
               <ts e="T150" id="Seg_4588" n="e" s="T149">šobi </ts>
               <ts e="T151" id="Seg_4590" n="e" s="T150">maːndə. </ts>
               <ts e="T152" id="Seg_4592" n="e" s="T151">Pʼešdə </ts>
               <ts e="T153" id="Seg_4594" n="e" s="T152">(šerluʔpi). </ts>
               <ts e="T154" id="Seg_4596" n="e" s="T153">Măn </ts>
               <ts e="T155" id="Seg_4598" n="e" s="T154">dʼabəbiam </ts>
               <ts e="T156" id="Seg_4600" n="e" s="T155">ine </ts>
               <ts e="T157" id="Seg_4602" n="e" s="T156">dĭn. </ts>
               <ts e="T158" id="Seg_4604" n="e" s="T157">Tuj </ts>
               <ts e="T159" id="Seg_4606" n="e" s="T158">kaməndə </ts>
               <ts e="T160" id="Seg_4608" n="e" s="T159">ej </ts>
               <ts e="T161" id="Seg_4610" n="e" s="T160">šoləj </ts>
               <ts e="T162" id="Seg_4612" n="e" s="T161">amzittə </ts>
               <ts e="T163" id="Seg_4614" n="e" s="T162">budəj. </ts>
               <ts e="T164" id="Seg_4616" n="e" s="T163">Dĭgəttə </ts>
               <ts e="T165" id="Seg_4618" n="e" s="T164">koŋ </ts>
               <ts e="T166" id="Seg_4620" n="e" s="T165">abi </ts>
               <ts e="T167" id="Seg_4622" n="e" s="T166">štobɨ </ts>
               <ts e="T168" id="Seg_4624" n="e" s="T167">il </ts>
               <ts e="T169" id="Seg_4626" n="e" s="T168">šobiʔi. </ts>
               <ts e="T170" id="Seg_4628" n="e" s="T169">(Dĭ-) </ts>
               <ts e="T171" id="Seg_4630" n="e" s="T170">Dĭn </ts>
               <ts e="T172" id="Seg_4632" n="e" s="T171">koʔbdo </ts>
               <ts e="T173" id="Seg_4634" n="e" s="T172">amnolaʔbə </ts>
               <ts e="T174" id="Seg_4636" n="e" s="T173">tʼerebgən. </ts>
               <ts e="T175" id="Seg_4638" n="e" s="T174">Bar </ts>
               <ts e="T176" id="Seg_4640" n="e" s="T175">il </ts>
               <ts e="T177" id="Seg_4642" n="e" s="T176">kallaʔbəʔjə. </ts>
               <ts e="T178" id="Seg_4644" n="e" s="T177">(I=) </ts>
               <ts e="T179" id="Seg_4646" n="e" s="T178">I </ts>
               <ts e="T180" id="Seg_4648" n="e" s="T179">dĭn </ts>
               <ts e="T181" id="Seg_4650" n="e" s="T180">kagaʔi </ts>
               <ts e="T182" id="Seg_4652" n="e" s="T181">kandəgaʔi. </ts>
               <ts e="T183" id="Seg_4654" n="e" s="T182">A </ts>
               <ts e="T184" id="Seg_4656" n="e" s="T183">dĭ </ts>
               <ts e="T185" id="Seg_4658" n="e" s="T184">măndə: </ts>
               <ts e="T186" id="Seg_4660" n="e" s="T185">igəʔ </ts>
               <ts e="T187" id="Seg_4662" n="e" s="T186">măna! </ts>
               <ts e="T188" id="Seg_4664" n="e" s="T187">Gibər </ts>
               <ts e="T189" id="Seg_4666" n="e" s="T188">tăn </ts>
               <ts e="T190" id="Seg_4668" n="e" s="T189">kallal? </ts>
               <ts e="T191" id="Seg_4670" n="e" s="T190">Ĭmbidə </ts>
               <ts e="T192" id="Seg_4672" n="e" s="T191">dĭn </ts>
               <ts e="T193" id="Seg_4674" n="e" s="T192">ej </ts>
               <ts e="T194" id="Seg_4676" n="e" s="T193">kubial, </ts>
               <ts e="T195" id="Seg_4678" n="e" s="T194">štobɨ </ts>
               <ts e="T196" id="Seg_4680" n="e" s="T195">il </ts>
               <ts e="T197" id="Seg_4682" n="e" s="T196">püšterbiʔi </ts>
               <ts e="T198" id="Seg_4684" n="e" s="T197">(tăn-) </ts>
               <ts e="T199" id="Seg_4686" n="e" s="T198">tănan. </ts>
               <ts e="T200" id="Seg_4688" n="e" s="T199">Dĭzeŋ </ts>
               <ts e="T201" id="Seg_4690" n="e" s="T200">kalla </ts>
               <ts e="T202" id="Seg_4692" n="e" s="T201">dʼürbiʔi, </ts>
               <ts e="T203" id="Seg_4694" n="e" s="T202">dĭ </ts>
               <ts e="T204" id="Seg_4696" n="e" s="T203">uʔbdəbi. </ts>
               <ts e="T205" id="Seg_4698" n="e" s="T204">Karzʼina </ts>
               <ts e="T206" id="Seg_4700" n="e" s="T205">ibi, </ts>
               <ts e="T207" id="Seg_4702" n="e" s="T206">kambi. </ts>
               <ts e="T208" id="Seg_4704" n="e" s="T207">I </ts>
               <ts e="T209" id="Seg_4706" n="e" s="T208">davaj </ts>
               <ts e="T210" id="Seg_4708" n="e" s="T209">kirgarzittə, </ts>
               <ts e="T211" id="Seg_4710" n="e" s="T210">inet </ts>
               <ts e="T212" id="Seg_4712" n="e" s="T211">šobi. </ts>
               <ts e="T213" id="Seg_4714" n="e" s="T212">Bar </ts>
               <ts e="T214" id="Seg_4716" n="e" s="T213">döbər </ts>
               <ts e="T215" id="Seg_4718" n="e" s="T214">(modriagən) </ts>
               <ts e="T216" id="Seg_4720" n="e" s="T215">šonugaʔi. </ts>
               <ts e="T217" id="Seg_4722" n="e" s="T216">Šö </ts>
               <ts e="T218" id="Seg_4724" n="e" s="T217">kuzandə </ts>
               <ts e="T219" id="Seg_4726" n="e" s="T218">šonuga. </ts>
               <ts e="T220" id="Seg_4728" n="e" s="T219">Dĭgəttə </ts>
               <ts e="T221" id="Seg_4730" n="e" s="T220">dĭ </ts>
               <ts e="T222" id="Seg_4732" n="e" s="T221">ine </ts>
               <ts e="T224" id="Seg_4734" n="e" s="T222">măndə… </ts>
               <ts e="T225" id="Seg_4736" n="e" s="T224">Nubi </ts>
               <ts e="T226" id="Seg_4738" n="e" s="T225">dĭʔnə. </ts>
               <ts e="T227" id="Seg_4740" n="e" s="T226">Măndə: </ts>
               <ts e="T228" id="Seg_4742" n="e" s="T227">onʼiʔ </ts>
               <ts e="T229" id="Seg_4744" n="e" s="T228">kunə </ts>
               <ts e="T230" id="Seg_4746" n="e" s="T229">paʔkaʔ, </ts>
               <ts e="T231" id="Seg_4748" n="e" s="T230">onʼiʔgəʔ </ts>
               <ts e="T232" id="Seg_4750" n="e" s="T231">supsoʔ. </ts>
               <ts e="T233" id="Seg_4752" n="e" s="T232">Dĭ </ts>
               <ts e="T234" id="Seg_4754" n="e" s="T233">paʔpi, </ts>
               <ts e="T235" id="Seg_4756" n="e" s="T234">supsobi. </ts>
               <ts e="T236" id="Seg_4758" n="e" s="T235">Kuvas </ts>
               <ts e="T237" id="Seg_4760" n="e" s="T236">molaːmbi. </ts>
               <ts e="T238" id="Seg_4762" n="e" s="T237">Dĭgəttə </ts>
               <ts e="T239" id="Seg_4764" n="e" s="T238">dĭʔnə </ts>
               <ts e="T240" id="Seg_4766" n="e" s="T239">amnobi. </ts>
               <ts e="T241" id="Seg_4768" n="e" s="T240">Dĭ </ts>
               <ts e="T242" id="Seg_4770" n="e" s="T241">(nuʔmə=) </ts>
               <ts e="T243" id="Seg_4772" n="e" s="T242">nuʔməluʔpi. </ts>
               <ts e="T244" id="Seg_4774" n="e" s="T243">Dibər </ts>
               <ts e="T245" id="Seg_4776" n="e" s="T244">il </ts>
               <ts e="T246" id="Seg_4778" n="e" s="T245">iʔgö </ts>
               <ts e="T247" id="Seg_4780" n="e" s="T246">nugaʔi, </ts>
               <ts e="T248" id="Seg_4782" n="e" s="T247">a </ts>
               <ts e="T249" id="Seg_4784" n="e" s="T248">šindidə </ts>
               <ts e="T250" id="Seg_4786" n="e" s="T249">ej, </ts>
               <ts e="T251" id="Seg_4788" n="e" s="T250">dĭ </ts>
               <ts e="T252" id="Seg_4790" n="e" s="T251">koʔbdonə </ts>
               <ts e="T253" id="Seg_4792" n="e" s="T252">ej </ts>
               <ts e="T254" id="Seg_4794" n="e" s="T253">sʼaliaʔi. </ts>
               <ts e="T255" id="Seg_4796" n="e" s="T254">Dĭ </ts>
               <ts e="T256" id="Seg_4798" n="e" s="T255">bar </ts>
               <ts e="T257" id="Seg_4800" n="e" s="T256">suʔmiluʔpi. </ts>
               <ts e="T258" id="Seg_4802" n="e" s="T257">Šalaʔjə, </ts>
               <ts e="T259" id="Seg_4804" n="e" s="T258">ej, </ts>
               <ts e="T260" id="Seg_4806" n="e" s="T259">ej </ts>
               <ts e="T261" id="Seg_4808" n="e" s="T260">dʼapi. </ts>
               <ts e="T262" id="Seg_4810" n="e" s="T261">Dĭgəttə </ts>
               <ts e="T263" id="Seg_4812" n="e" s="T262">il </ts>
               <ts e="T264" id="Seg_4814" n="e" s="T263">bar </ts>
               <ts e="T265" id="Seg_4816" n="e" s="T264">kirgarlaʔbəʔjə: </ts>
               <ts e="T266" id="Seg_4818" n="e" s="T265">Dʼabəʔ, </ts>
               <ts e="T267" id="Seg_4820" n="e" s="T266">dʼabəʔ. </ts>
               <ts e="T268" id="Seg_4822" n="e" s="T267">A </ts>
               <ts e="T269" id="Seg_4824" n="e" s="T268">dĭ </ts>
               <ts e="T270" id="Seg_4826" n="e" s="T269">nuʔməluʔpi. </ts>
               <ts e="T271" id="Seg_4828" n="e" s="T270">Inem </ts>
               <ts e="T272" id="Seg_4830" n="e" s="T271">öʔlubi. </ts>
               <ts e="T273" id="Seg_4832" n="e" s="T272">Dĭgəttə </ts>
               <ts e="T274" id="Seg_4834" n="e" s="T273">beškeʔi </ts>
               <ts e="T275" id="Seg_4836" n="e" s="T274">oʔbdəbi </ts>
               <ts e="T276" id="Seg_4838" n="e" s="T275">i </ts>
               <ts e="T277" id="Seg_4840" n="e" s="T276">vsʼaka </ts>
               <ts e="T278" id="Seg_4842" n="e" s="T277">(iʔgö) </ts>
               <ts e="T279" id="Seg_4844" n="e" s="T278">beške </ts>
               <ts e="T280" id="Seg_4846" n="e" s="T279">deppi. </ts>
               <ts e="T281" id="Seg_4848" n="e" s="T280">A </ts>
               <ts e="T282" id="Seg_4850" n="e" s="T281">nezeŋ </ts>
               <ts e="T283" id="Seg_4852" n="e" s="T282">kudolaʔbəʔjə. </ts>
               <ts e="T284" id="Seg_4854" n="e" s="T283">Ĭmbi </ts>
               <ts e="T285" id="Seg_4856" n="e" s="T284">dĭrgit </ts>
               <ts e="T286" id="Seg_4858" n="e" s="T285">(deʔ-) </ts>
               <ts e="T287" id="Seg_4860" n="e" s="T286">deʔpiel. </ts>
               <ts e="T288" id="Seg_4862" n="e" s="T287">Tolʼko </ts>
               <ts e="T289" id="Seg_4864" n="e" s="T288">tănan </ts>
               <ts e="T290" id="Seg_4866" n="e" s="T289">amzittə. </ts>
               <ts e="T291" id="Seg_4868" n="e" s="T290">Dĭ </ts>
               <ts e="T292" id="Seg_4870" n="e" s="T291">pʼešdə </ts>
               <ts e="T293" id="Seg_4872" n="e" s="T292">(š-) </ts>
               <ts e="T294" id="Seg_4874" n="e" s="T293">sʼabi, </ts>
               <ts e="T295" id="Seg_4876" n="e" s="T294">iʔbölaʔbə. </ts>
               <ts e="T296" id="Seg_4878" n="e" s="T295">Dĭgəttə </ts>
               <ts e="T297" id="Seg_4880" n="e" s="T296">(kazak-) </ts>
               <ts e="T298" id="Seg_4882" n="e" s="T297">kagazaŋdə </ts>
               <ts e="T299" id="Seg_4884" n="e" s="T298">šobiʔi </ts>
               <ts e="T300" id="Seg_4886" n="e" s="T299">bar, </ts>
               <ts e="T301" id="Seg_4888" n="e" s="T300">nörbəleʔbəʔjə </ts>
               <ts e="T302" id="Seg_4890" n="e" s="T301">(dĭrgi-) </ts>
               <ts e="T303" id="Seg_4892" n="e" s="T302">kăda </ts>
               <ts e="T304" id="Seg_4894" n="e" s="T303">ibi. </ts>
               <ts e="T305" id="Seg_4896" n="e" s="T304">A </ts>
               <ts e="T306" id="Seg_4898" n="e" s="T305">dĭ </ts>
               <ts e="T307" id="Seg_4900" n="e" s="T306">iʔbölaʔbə. </ts>
               <ts e="T308" id="Seg_4902" n="e" s="T307">Dĭ </ts>
               <ts e="T309" id="Seg_4904" n="e" s="T308">măn </ts>
               <ts e="T310" id="Seg_4906" n="e" s="T309">ulum </ts>
               <ts e="T311" id="Seg_4908" n="e" s="T310">dĭn </ts>
               <ts e="T312" id="Seg_4910" n="e" s="T311">ibi. </ts>
               <ts e="T313" id="Seg_4912" n="e" s="T312">Iʔ </ts>
               <ts e="T314" id="Seg_4914" n="e" s="T313">šamaʔ! </ts>
               <ts e="T315" id="Seg_4916" n="e" s="T314">Tăn </ts>
               <ts e="T316" id="Seg_4918" n="e" s="T315">ulum. </ts>
               <ts e="T317" id="Seg_4920" n="e" s="T316">Amnoʔ </ts>
               <ts e="T318" id="Seg_4922" n="e" s="T317">už. </ts>
               <ts e="T319" id="Seg_4924" n="e" s="T318">Ĭmbidə </ts>
               <ts e="T320" id="Seg_4926" n="e" s="T319">iʔ </ts>
               <ts e="T321" id="Seg_4928" n="e" s="T320">dʼăbaktəraʔ. </ts>
               <ts e="T322" id="Seg_4930" n="e" s="T321">Dĭrgit </ts>
               <ts e="T323" id="Seg_4932" n="e" s="T322">kuza </ts>
               <ts e="T324" id="Seg_4934" n="e" s="T323">ibi, </ts>
               <ts e="T325" id="Seg_4936" n="e" s="T324">inet </ts>
               <ts e="T326" id="Seg_4938" n="e" s="T325">kuvas </ts>
               <ts e="T327" id="Seg_4940" n="e" s="T326">ibi </ts>
               <ts e="T328" id="Seg_4942" n="e" s="T327">bostə. </ts>
               <ts e="T329" id="Seg_4944" n="e" s="T328">Dĭgəttə </ts>
               <ts e="T330" id="Seg_4946" n="e" s="T329">karəldʼan </ts>
               <ts e="T331" id="Seg_4948" n="e" s="T330">bazoʔ </ts>
               <ts e="T332" id="Seg_4950" n="e" s="T331">kandəgaʔi. </ts>
               <ts e="T333" id="Seg_4952" n="e" s="T332">Kalla </ts>
               <ts e="T334" id="Seg_4954" n="e" s="T333">dʼürbiʔi. </ts>
               <ts e="T335" id="Seg_4956" n="e" s="T334">Dĭ </ts>
               <ts e="T336" id="Seg_4958" n="e" s="T335">pʼešgəʔ </ts>
               <ts e="T337" id="Seg_4960" n="e" s="T336">(š-) </ts>
               <ts e="T338" id="Seg_4962" n="e" s="T337">suʔmiluʔpi. </ts>
               <ts e="T339" id="Seg_4964" n="e" s="T338">Karzʼina </ts>
               <ts e="T340" id="Seg_4966" n="e" s="T339">ibi. </ts>
               <ts e="T341" id="Seg_4968" n="e" s="T340">Kambi, </ts>
               <ts e="T342" id="Seg_4970" n="e" s="T341">kambi, </ts>
               <ts e="T344" id="Seg_4972" n="e" s="T342">baruʔpi. </ts>
               <ts e="T345" id="Seg_4974" n="e" s="T344">Davaj </ts>
               <ts e="T346" id="Seg_4976" n="e" s="T345">inem </ts>
               <ts e="T347" id="Seg_4978" n="e" s="T346">kirgarzittə. </ts>
               <ts e="T348" id="Seg_4980" n="e" s="T347">Inet </ts>
               <ts e="T349" id="Seg_4982" n="e" s="T348">šobi. </ts>
               <ts e="T350" id="Seg_4984" n="e" s="T349">Bazoʔ </ts>
               <ts e="T351" id="Seg_4986" n="e" s="T350">onʼiʔ </ts>
               <ts e="T352" id="Seg_4988" n="e" s="T351">kuʔtə </ts>
               <ts e="T353" id="Seg_4990" n="e" s="T352">(paʔlaːmbi), </ts>
               <ts e="T354" id="Seg_4992" n="e" s="T353">onʼiʔ </ts>
               <ts e="T355" id="Seg_4994" n="e" s="T354">supsolaːmbi. </ts>
               <ts e="T356" id="Seg_4996" n="e" s="T355">Dĭgəttə </ts>
               <ts e="T357" id="Seg_4998" n="e" s="T356">amnəbi. </ts>
               <ts e="T358" id="Seg_5000" n="e" s="T357">(Nuʔm-) </ts>
               <ts e="T359" id="Seg_5002" n="e" s="T358">Šonuga </ts>
               <ts e="T360" id="Seg_5004" n="e" s="T359">il, </ts>
               <ts e="T361" id="Seg_5006" n="e" s="T360">iʔgö </ts>
               <ts e="T362" id="Seg_5008" n="e" s="T361">nugaʔi. </ts>
               <ts e="T363" id="Seg_5010" n="e" s="T362">Dĭgəttə </ts>
               <ts e="T364" id="Seg_5012" n="e" s="T363">šindidə </ts>
               <ts e="T365" id="Seg_5014" n="e" s="T364">ej </ts>
               <ts e="T366" id="Seg_5016" n="e" s="T365">nuʔməlie </ts>
               <ts e="T367" id="Seg_5018" n="e" s="T366">dibər. </ts>
               <ts e="T368" id="Seg_5020" n="e" s="T367">Dibər </ts>
               <ts e="T369" id="Seg_5022" n="e" s="T368">(š-) </ts>
               <ts e="T370" id="Seg_5024" n="e" s="T369">suʔmiluʔpi. </ts>
               <ts e="T371" id="Seg_5026" n="e" s="T370">Ibi </ts>
               <ts e="T372" id="Seg_5028" n="e" s="T371">(e- </ts>
               <ts e="T373" id="Seg_5030" n="e" s="T372">i </ts>
               <ts e="T374" id="Seg_5032" n="e" s="T373">ej </ts>
               <ts e="T375" id="Seg_5034" n="e" s="T374">nʼi-) </ts>
               <ts e="T376" id="Seg_5036" n="e" s="T375">ej </ts>
               <ts e="T377" id="Seg_5038" n="e" s="T376">mobi. </ts>
               <ts e="T378" id="Seg_5040" n="e" s="T377">Dĭgəttə </ts>
               <ts e="T379" id="Seg_5042" n="e" s="T378">parluʔpi. </ts>
               <ts e="T380" id="Seg_5044" n="e" s="T379">Kagazaŋdə </ts>
               <ts e="T381" id="Seg_5046" n="e" s="T380">bar </ts>
               <ts e="T382" id="Seg_5048" n="e" s="T381">münörbi </ts>
               <ts e="T383" id="Seg_5050" n="e" s="T382">i </ts>
               <ts e="T384" id="Seg_5052" n="e" s="T383">kalla </ts>
               <ts e="T385" id="Seg_5054" n="e" s="T384">dʼürbi. </ts>
               <ts e="T386" id="Seg_5056" n="e" s="T385">A </ts>
               <ts e="T387" id="Seg_5058" n="e" s="T386">il </ts>
               <ts e="T388" id="Seg_5060" n="e" s="T387">kirgarlaʔbəʔjə. </ts>
               <ts e="T389" id="Seg_5062" n="e" s="T388">(Dʼabəʔ), </ts>
               <ts e="T390" id="Seg_5064" n="e" s="T389">(dʼabəʔ), </ts>
               <ts e="T391" id="Seg_5066" n="e" s="T390">da </ts>
               <ts e="T392" id="Seg_5068" n="e" s="T391">dĭ </ts>
               <ts e="T393" id="Seg_5070" n="e" s="T392">iʔ </ts>
               <ts e="T394" id="Seg_5072" n="e" s="T393">məluʔpi. </ts>
               <ts e="T395" id="Seg_5074" n="e" s="T394">Inebə </ts>
               <ts e="T396" id="Seg_5076" n="e" s="T395">öʔluʔpi, </ts>
               <ts e="T397" id="Seg_5078" n="e" s="T396">a </ts>
               <ts e="T398" id="Seg_5080" n="e" s="T397">bostə </ts>
               <ts e="T399" id="Seg_5082" n="e" s="T398">bazoʔ </ts>
               <ts e="T400" id="Seg_5084" n="e" s="T399">šobi. </ts>
               <ts e="T401" id="Seg_5086" n="e" s="T400">Nezeŋdə </ts>
               <ts e="T402" id="Seg_5088" n="e" s="T401">bazoʔ </ts>
               <ts e="T403" id="Seg_5090" n="e" s="T402">kudolaʔbəʔjə. </ts>
               <ts e="T404" id="Seg_5092" n="e" s="T403">A </ts>
               <ts e="T405" id="Seg_5094" n="e" s="T404">dĭ </ts>
               <ts e="T406" id="Seg_5096" n="e" s="T405">pʼešdə </ts>
               <ts e="T407" id="Seg_5098" n="e" s="T406">sʼabi </ts>
               <ts e="T408" id="Seg_5100" n="e" s="T407">da </ts>
               <ts e="T409" id="Seg_5102" n="e" s="T408">iʔbölaʔbə. </ts>
               <ts e="T410" id="Seg_5104" n="e" s="T409">(Kaz-) </ts>
               <ts e="T411" id="Seg_5106" n="e" s="T410">Kazaŋdə </ts>
               <ts e="T412" id="Seg_5108" n="e" s="T411">šobiʔi. </ts>
               <ts e="T413" id="Seg_5110" n="e" s="T412">Abandə </ts>
               <ts e="T414" id="Seg_5112" n="e" s="T413">nörbəlieʔi. </ts>
               <ts e="T415" id="Seg_5114" n="e" s="T414">Girgit </ts>
               <ts e="T416" id="Seg_5116" n="e" s="T415">kuza </ts>
               <ts e="T417" id="Seg_5118" n="e" s="T416">ibi, </ts>
               <ts e="T418" id="Seg_5120" n="e" s="T417">inet </ts>
               <ts e="T419" id="Seg_5122" n="e" s="T418">ugaːndə </ts>
               <ts e="T420" id="Seg_5124" n="e" s="T419">kuvas </ts>
               <ts e="T421" id="Seg_5126" n="e" s="T420">i </ts>
               <ts e="T422" id="Seg_5128" n="e" s="T421">(bolʼš-) </ts>
               <ts e="T423" id="Seg_5130" n="e" s="T422">bostə </ts>
               <ts e="T424" id="Seg_5132" n="e" s="T423">kuvas, </ts>
               <ts e="T425" id="Seg_5134" n="e" s="T424">a </ts>
               <ts e="T426" id="Seg_5136" n="e" s="T425">dĭ </ts>
               <ts e="T427" id="Seg_5138" n="e" s="T426">iʔbölaʔbə. </ts>
               <ts e="T428" id="Seg_5140" n="e" s="T427">Ej </ts>
               <ts e="T429" id="Seg_5142" n="e" s="T428">măn </ts>
               <ts e="T430" id="Seg_5144" n="e" s="T429">li </ts>
               <ts e="T431" id="Seg_5146" n="e" s="T430">ulum </ts>
               <ts e="T432" id="Seg_5148" n="e" s="T431">dĭn </ts>
               <ts e="T433" id="Seg_5150" n="e" s="T432">ibi. </ts>
               <ts e="T434" id="Seg_5152" n="e" s="T433">Iʔ </ts>
               <ts e="T435" id="Seg_5154" n="e" s="T434">šamaʔ, </ts>
               <ts e="T436" id="Seg_5156" n="e" s="T435">iʔbəʔ </ts>
               <ts e="T437" id="Seg_5158" n="e" s="T436">už! </ts>
               <ts e="T438" id="Seg_5160" n="e" s="T437">Sagəššət </ts>
               <ts e="T439" id="Seg_5162" n="e" s="T438">nʼi! </ts>
               <ts e="T440" id="Seg_5164" n="e" s="T439">Tăn </ts>
               <ts e="T441" id="Seg_5166" n="e" s="T440">dĭn </ts>
               <ts e="T442" id="Seg_5168" n="e" s="T441">ibiel. </ts>
               <ts e="T443" id="Seg_5170" n="e" s="T442">Dĭgəttə </ts>
               <ts e="T444" id="Seg_5172" n="e" s="T443">bazoʔ, </ts>
               <ts e="T445" id="Seg_5174" n="e" s="T444">nagur </ts>
               <ts e="T446" id="Seg_5176" n="e" s="T445">dʼala, </ts>
               <ts e="T447" id="Seg_5178" n="e" s="T446">bazoʔ </ts>
               <ts e="T448" id="Seg_5180" n="e" s="T447">kandəgaʔi. </ts>
               <ts e="T449" id="Seg_5182" n="e" s="T448">Kalla </ts>
               <ts e="T450" id="Seg_5184" n="e" s="T449">dʼürbiʔi, </ts>
               <ts e="T451" id="Seg_5186" n="e" s="T450">dĭ </ts>
               <ts e="T452" id="Seg_5188" n="e" s="T451">(s-) </ts>
               <ts e="T453" id="Seg_5190" n="e" s="T452">uʔbdəbi. </ts>
               <ts e="T454" id="Seg_5192" n="e" s="T453">Karzʼinabə </ts>
               <ts e="T455" id="Seg_5194" n="e" s="T454">ibi, </ts>
               <ts e="T456" id="Seg_5196" n="e" s="T455">kambi. </ts>
               <ts e="T457" id="Seg_5198" n="e" s="T456">Beškeʔi </ts>
               <ts e="T458" id="Seg_5200" n="e" s="T457">oʔbšizittə. </ts>
               <ts e="T459" id="Seg_5202" n="e" s="T458">Kambi, </ts>
               <ts e="T460" id="Seg_5204" n="e" s="T459">davaj </ts>
               <ts e="T461" id="Seg_5206" n="e" s="T460">ine </ts>
               <ts e="T462" id="Seg_5208" n="e" s="T461">kirgarzittə. </ts>
               <ts e="T463" id="Seg_5210" n="e" s="T462">Ine </ts>
               <ts e="T464" id="Seg_5212" n="e" s="T463">šobi. </ts>
               <ts e="T465" id="Seg_5214" n="e" s="T464">Dĭ </ts>
               <ts e="T466" id="Seg_5216" n="e" s="T465">bazoʔ </ts>
               <ts e="T467" id="Seg_5218" n="e" s="T466">dăra </ts>
               <ts e="T468" id="Seg_5220" n="e" s="T467">(on-) </ts>
               <ts e="T469" id="Seg_5222" n="e" s="T468">onʼiʔ </ts>
               <ts e="T470" id="Seg_5224" n="e" s="T469">kugəndə </ts>
               <ts e="T471" id="Seg_5226" n="e" s="T470">paʔlaːmbi, </ts>
               <ts e="T472" id="Seg_5228" n="e" s="T471">onʼiʔ </ts>
               <ts e="T473" id="Seg_5230" n="e" s="T472">supsolaːmbi, </ts>
               <ts e="T474" id="Seg_5232" n="e" s="T473">amnobi, </ts>
               <ts e="T475" id="Seg_5234" n="e" s="T474">kambi. </ts>
               <ts e="T476" id="Seg_5236" n="e" s="T475">Dĭgəttə </ts>
               <ts e="T477" id="Seg_5238" n="e" s="T476">kak </ts>
               <ts e="T478" id="Seg_5240" n="e" s="T477">nuʔməluʔpi. </ts>
               <ts e="T479" id="Seg_5242" n="e" s="T478">Dĭm </ts>
               <ts e="T480" id="Seg_5244" n="e" s="T479">panarbi </ts>
               <ts e="T481" id="Seg_5246" n="e" s="T480">i </ts>
               <ts e="T482" id="Seg_5248" n="e" s="T481">kalʼečkabə </ts>
               <ts e="T483" id="Seg_5250" n="e" s="T482">iluʔpi, </ts>
               <ts e="T484" id="Seg_5252" n="e" s="T483">bostə </ts>
               <ts e="T485" id="Seg_5254" n="e" s="T484">udandə </ts>
               <ts e="T486" id="Seg_5256" n="e" s="T485">(se-) </ts>
               <ts e="T487" id="Seg_5258" n="e" s="T486">šerbi. </ts>
               <ts e="T488" id="Seg_5260" n="e" s="T487">Kalla </ts>
               <ts e="T489" id="Seg_5262" n="e" s="T488">dʼürbi. </ts>
               <ts e="T490" id="Seg_5264" n="e" s="T489">Inebə </ts>
               <ts e="T491" id="Seg_5266" n="e" s="T490">öʔlubi, </ts>
               <ts e="T492" id="Seg_5268" n="e" s="T491">bostə </ts>
               <ts e="T493" id="Seg_5270" n="e" s="T492">šobi </ts>
               <ts e="T494" id="Seg_5272" n="e" s="T493">maːndə </ts>
               <ts e="T495" id="Seg_5274" n="e" s="T494">i </ts>
               <ts e="T496" id="Seg_5276" n="e" s="T495">pʼešdə </ts>
               <ts e="T497" id="Seg_5278" n="e" s="T496">sʼabi, </ts>
               <ts e="T498" id="Seg_5280" n="e" s="T497">iʔbölaʔbə. </ts>
               <ts e="T499" id="Seg_5282" n="e" s="T498">Dĭgəttə </ts>
               <ts e="T500" id="Seg_5284" n="e" s="T499">kagazaŋdə </ts>
               <ts e="T501" id="Seg_5286" n="e" s="T500">šobiʔi. </ts>
               <ts e="T502" id="Seg_5288" n="e" s="T501">Nu, </ts>
               <ts e="T503" id="Seg_5290" n="e" s="T502">girgitdə </ts>
               <ts e="T504" id="Seg_5292" n="e" s="T503">(nʼi) </ts>
               <ts e="T505" id="Seg_5294" n="e" s="T504">nʼi </ts>
               <ts e="T506" id="Seg_5296" n="e" s="T505">kuvas, </ts>
               <ts e="T507" id="Seg_5298" n="e" s="T506">inet </ts>
               <ts e="T508" id="Seg_5300" n="e" s="T507">kuvas. </ts>
               <ts e="T509" id="Seg_5302" n="e" s="T508">Panarbi </ts>
               <ts e="T510" id="Seg_5304" n="e" s="T509">tsarskɨj </ts>
               <ts e="T511" id="Seg_5306" n="e" s="T510">koʔbdo. </ts>
               <ts e="T512" id="Seg_5308" n="e" s="T511">(Koŋ) </ts>
               <ts e="T513" id="Seg_5310" n="e" s="T512">Koŋ </ts>
               <ts e="T514" id="Seg_5312" n="e" s="T513">koʔbdo. </ts>
               <ts e="T515" id="Seg_5314" n="e" s="T514">I </ts>
               <ts e="T516" id="Seg_5316" n="e" s="T515">kalʼečkabə </ts>
               <ts e="T517" id="Seg_5318" n="e" s="T516">ibi, </ts>
               <ts e="T518" id="Seg_5320" n="e" s="T517">kalla </ts>
               <ts e="T519" id="Seg_5322" n="e" s="T518">dʼürbi. </ts>
               <ts e="T520" id="Seg_5324" n="e" s="T519">Dĭgəttə </ts>
               <ts e="T521" id="Seg_5326" n="e" s="T520">bazoʔ </ts>
               <ts e="T522" id="Seg_5328" n="e" s="T521">dĭ </ts>
               <ts e="T523" id="Seg_5330" n="e" s="T522">koŋ </ts>
               <ts e="T524" id="Seg_5332" n="e" s="T523">abi. </ts>
               <ts e="T525" id="Seg_5334" n="e" s="T524">Iʔgö </ts>
               <ts e="T526" id="Seg_5336" n="e" s="T525">uja </ts>
               <ts e="T527" id="Seg_5338" n="e" s="T526">munujʔ </ts>
               <ts e="T528" id="Seg_5340" n="e" s="T527">embi, </ts>
               <ts e="T529" id="Seg_5342" n="e" s="T528">bar </ts>
               <ts e="T530" id="Seg_5344" n="e" s="T529">ĭmbi </ts>
               <ts e="T531" id="Seg_5346" n="e" s="T530">dĭn </ts>
               <ts e="T532" id="Seg_5348" n="e" s="T531">ige </ts>
               <ts e="T533" id="Seg_5350" n="e" s="T532">embi. </ts>
               <ts e="T534" id="Seg_5352" n="e" s="T533">Dĭgəttə </ts>
               <ts e="T535" id="Seg_5354" n="e" s="T534">Šogaʔ </ts>
               <ts e="T536" id="Seg_5356" n="e" s="T535">bargəʔ, </ts>
               <ts e="T537" id="Seg_5358" n="e" s="T536">šində </ts>
               <ts e="T538" id="Seg_5360" n="e" s="T537">ej </ts>
               <ts e="T539" id="Seg_5362" n="e" s="T538">šoləj, </ts>
               <ts e="T540" id="Seg_5364" n="e" s="T539">ulubə </ts>
               <ts e="T541" id="Seg_5366" n="e" s="T540">(š-) </ts>
               <ts e="T542" id="Seg_5368" n="e" s="T541">sajjaʔlim. </ts>
               <ts e="T543" id="Seg_5370" n="e" s="T542">Dĭgəttə </ts>
               <ts e="T544" id="Seg_5372" n="e" s="T543">dĭzeŋ </ts>
               <ts e="T545" id="Seg_5374" n="e" s="T544">(a-), </ts>
               <ts e="T546" id="Seg_5376" n="e" s="T545">dĭn </ts>
               <ts e="T547" id="Seg_5378" n="e" s="T546">abat, </ts>
               <ts e="T548" id="Seg_5380" n="e" s="T547">bargəʔ </ts>
               <ts e="T549" id="Seg_5382" n="e" s="T548">kalla </ts>
               <ts e="T550" id="Seg_5384" n="e" s="T549">dʼürbiʔi, </ts>
               <ts e="T551" id="Seg_5386" n="e" s="T550">šobiʔi </ts>
               <ts e="T552" id="Seg_5388" n="e" s="T551">dĭzeŋ. </ts>
               <ts e="T553" id="Seg_5390" n="e" s="T552">Amnəlbiʔi, </ts>
               <ts e="T554" id="Seg_5392" n="e" s="T553">(ba-) </ts>
               <ts e="T555" id="Seg_5394" n="e" s="T554">bar </ts>
               <ts e="T556" id="Seg_5396" n="e" s="T555">bĭdlaʔbəʔjə, </ts>
               <ts e="T557" id="Seg_5398" n="e" s="T556">(aja-) </ts>
               <ts e="T558" id="Seg_5400" n="e" s="T557">ara </ts>
               <ts e="T559" id="Seg_5402" n="e" s="T558">mĭleʔbəʔjə. </ts>
               <ts e="T560" id="Seg_5404" n="e" s="T559">Dĭgəttə </ts>
               <ts e="T561" id="Seg_5406" n="e" s="T560">šobi </ts>
               <ts e="T562" id="Seg_5408" n="e" s="T561">dĭ </ts>
               <ts e="T563" id="Seg_5410" n="e" s="T562">poslʼednij </ts>
               <ts e="T564" id="Seg_5412" n="e" s="T563">nʼit, </ts>
               <ts e="T565" id="Seg_5414" n="e" s="T564">dĭ. </ts>
               <ts e="T566" id="Seg_5416" n="e" s="T565">Dĭ </ts>
               <ts e="T567" id="Seg_5418" n="e" s="T566">bar </ts>
               <ts e="T568" id="Seg_5420" n="e" s="T567">sagər </ts>
               <ts e="T569" id="Seg_5422" n="e" s="T568">amnolaʔbə, </ts>
               <ts e="T570" id="Seg_5424" n="e" s="T569">i </ts>
               <ts e="T571" id="Seg_5426" n="e" s="T570">udat </ts>
               <ts e="T572" id="Seg_5428" n="e" s="T571">sagər, </ts>
               <ts e="T573" id="Seg_5430" n="e" s="T572">trʼapkazʼiʔ </ts>
               <ts e="T574" id="Seg_5432" n="e" s="T573">sarona. </ts>
               <ts e="T575" id="Seg_5434" n="e" s="T574">Măndə: </ts>
               <ts e="T576" id="Seg_5436" n="e" s="T575">Ĭmbi </ts>
               <ts e="T577" id="Seg_5438" n="e" s="T576">tăn </ts>
               <ts e="T578" id="Seg_5440" n="e" s="T577">udandə? </ts>
               <ts e="T579" id="Seg_5442" n="e" s="T578">(Kulʼ-) </ts>
               <ts e="T580" id="Seg_5444" n="e" s="T579">(Kuliat </ts>
               <ts e="T581" id="Seg_5446" n="e" s="T580">bar), </ts>
               <ts e="T582" id="Seg_5448" n="e" s="T581">bar </ts>
               <ts e="T583" id="Seg_5450" n="e" s="T582">dĭn </ts>
               <ts e="T584" id="Seg_5452" n="e" s="T583">kalʼečka </ts>
               <ts e="T585" id="Seg_5454" n="e" s="T584">amnolaʔbə. </ts>
               <ts e="T586" id="Seg_5456" n="e" s="T585">Dĭgəttə </ts>
               <ts e="T587" id="Seg_5458" n="e" s="T586">ilet, </ts>
               <ts e="T588" id="Seg_5460" n="e" s="T587">abandə </ts>
               <ts e="T589" id="Seg_5462" n="e" s="T588">šoliat. </ts>
               <ts e="T590" id="Seg_5464" n="e" s="T589">(Dö </ts>
               <ts e="T591" id="Seg_5466" n="e" s="T590">m-) </ts>
               <ts e="T592" id="Seg_5468" n="e" s="T591">Dö </ts>
               <ts e="T593" id="Seg_5470" n="e" s="T592">măn </ts>
               <ts e="T594" id="Seg_5472" n="e" s="T593">ženʼixbə. </ts>
               <ts e="T595" id="Seg_5474" n="e" s="T594">Dĭm </ts>
               <ts e="T596" id="Seg_5476" n="e" s="T595">(dĭ </ts>
               <ts e="T597" id="Seg_5478" n="e" s="T596">dĭ-) </ts>
               <ts e="T598" id="Seg_5480" n="e" s="T597">dĭzeŋ </ts>
               <ts e="T599" id="Seg_5482" n="e" s="T598">dĭn </ts>
               <ts e="T600" id="Seg_5484" n="e" s="T599">bəzəbiʔi. </ts>
               <ts e="T601" id="Seg_5486" n="e" s="T600">Kuvas </ts>
               <ts e="T602" id="Seg_5488" n="e" s="T601">oldʼa </ts>
               <ts e="T603" id="Seg_5490" n="e" s="T602">(s-) </ts>
               <ts e="T604" id="Seg_5492" n="e" s="T603">šerbiʔi. </ts>
               <ts e="T605" id="Seg_5494" n="e" s="T604">Ugaːndə </ts>
               <ts e="T606" id="Seg_5496" n="e" s="T605">kuvas </ts>
               <ts e="T607" id="Seg_5498" n="e" s="T606">molaːmbi. </ts>
               <ts e="T608" id="Seg_5500" n="e" s="T607">Dĭgəttə </ts>
               <ts e="T609" id="Seg_5502" n="e" s="T608">bazoʔ </ts>
               <ts e="T610" id="Seg_5504" n="e" s="T609">davaj </ts>
               <ts e="T611" id="Seg_5506" n="e" s="T610">ara </ts>
               <ts e="T612" id="Seg_5508" n="e" s="T611">bĭssittə. </ts>
               <ts e="T613" id="Seg_5510" n="e" s="T612">Sʼarzittə. </ts>
               <ts e="T614" id="Seg_5512" n="e" s="T613">Kagazaŋdə </ts>
               <ts e="T615" id="Seg_5514" n="e" s="T614">bar </ts>
               <ts e="T616" id="Seg_5516" n="e" s="T615">măndlaʔbəʔjə. </ts>
               <ts e="T617" id="Seg_5518" n="e" s="T616">Girgit </ts>
               <ts e="T618" id="Seg_5520" n="e" s="T617">kuvas </ts>
               <ts e="T619" id="Seg_5522" n="e" s="T618">molaːmbi. </ts>
               <ts e="T620" id="Seg_5524" n="e" s="T619">I </ts>
               <ts e="T621" id="Seg_5526" n="e" s="T620">dĭn </ts>
               <ts e="T622" id="Seg_5528" n="e" s="T621">măn </ts>
               <ts e="T623" id="Seg_5530" n="e" s="T622">ibiem. </ts>
               <ts e="T624" id="Seg_5532" n="e" s="T623">Ara </ts>
               <ts e="T625" id="Seg_5534" n="e" s="T624">bar </ts>
               <ts e="T626" id="Seg_5536" n="e" s="T625">mʼaŋŋaʔbə. </ts>
               <ts e="T627" id="Seg_5538" n="e" s="T626">Mĭbiʔi </ts>
               <ts e="T628" id="Seg_5540" n="e" s="T627">măna </ts>
               <ts e="T629" id="Seg_5542" n="e" s="T628">blin. </ts>
               <ts e="T632" id="Seg_5544" n="e" s="T629">Три года у лоханки </ts>
               <ts e="T633" id="Seg_5546" n="e" s="T632"> ((LAUGH)) ((BRK)). </ts>
               <ts e="T634" id="Seg_5548" n="e" s="T633">Amnobiʔi </ts>
               <ts e="T635" id="Seg_5550" n="e" s="T634">šidegöʔ </ts>
               <ts e="T636" id="Seg_5552" n="e" s="T635">kum </ts>
               <ts e="T637" id="Seg_5554" n="e" s="T636">da </ts>
               <ts e="T638" id="Seg_5556" n="e" s="T637">kuma. </ts>
               <ts e="T639" id="Seg_5558" n="e" s="T638">Volk </ts>
               <ts e="T640" id="Seg_5560" n="e" s="T639">da </ts>
               <ts e="T641" id="Seg_5562" n="e" s="T640">lʼisa. </ts>
               <ts e="T642" id="Seg_5564" n="e" s="T641">Dĭgəttə </ts>
               <ts e="T643" id="Seg_5566" n="e" s="T642">kunolzittə </ts>
               <ts e="T644" id="Seg_5568" n="e" s="T643">iʔbəʔi. </ts>
               <ts e="T645" id="Seg_5570" n="e" s="T644">A </ts>
               <ts e="T646" id="Seg_5572" n="e" s="T645">lʼisa </ts>
               <ts e="T647" id="Seg_5574" n="e" s="T646">uge </ts>
               <ts e="T648" id="Seg_5576" n="e" s="T647">kuzurleʔbə, </ts>
               <ts e="T649" id="Seg_5578" n="e" s="T648">kuzurleʔbə. </ts>
               <ts e="T650" id="Seg_5580" n="e" s="T649">A </ts>
               <ts e="T651" id="Seg_5582" n="e" s="T650">volk </ts>
               <ts e="T652" id="Seg_5584" n="e" s="T651">măndə: </ts>
               <ts e="T653" id="Seg_5586" n="e" s="T652">Šindidə </ts>
               <ts e="T654" id="Seg_5588" n="e" s="T653">kuzurleʔbə, </ts>
               <ts e="T655" id="Seg_5590" n="e" s="T654">măna </ts>
               <ts e="T656" id="Seg_5592" n="e" s="T655">kăštəliaʔi, </ts>
               <ts e="T657" id="Seg_5594" n="e" s="T656">(ešši) </ts>
               <ts e="T658" id="Seg_5596" n="e" s="T657">ešši </ts>
               <ts e="T659" id="Seg_5598" n="e" s="T658">pădəsʼtə. </ts>
               <ts e="T660" id="Seg_5600" n="e" s="T659">Dĭgəttə” </ts>
               <ts e="T661" id="Seg_5602" n="e" s="T660">No, </ts>
               <ts e="T662" id="Seg_5604" n="e" s="T661">kanaʔ!" </ts>
               <ts e="T663" id="Seg_5606" n="e" s="T662">Dĭ </ts>
               <ts e="T664" id="Seg_5608" n="e" s="T663">kambi. </ts>
               <ts e="T665" id="Seg_5610" n="e" s="T664">A </ts>
               <ts e="T666" id="Seg_5612" n="e" s="T665">dĭzeŋ </ts>
               <ts e="T667" id="Seg_5614" n="e" s="T666">nulaʔbi, </ts>
               <ts e="T668" id="Seg_5616" n="e" s="T667">mʼod </ts>
               <ts e="T669" id="Seg_5618" n="e" s="T668">ambi, </ts>
               <ts e="T670" id="Seg_5620" n="e" s="T669">ambi </ts>
               <ts e="T671" id="Seg_5622" n="e" s="T670">mʼod. </ts>
               <ts e="T672" id="Seg_5624" n="e" s="T671">Dĭ </ts>
               <ts e="T673" id="Seg_5626" n="e" s="T672">šobi. </ts>
               <ts e="T674" id="Seg_5628" n="e" s="T673">Šindi </ts>
               <ts e="T675" id="Seg_5630" n="e" s="T674">dĭn </ts>
               <ts e="T676" id="Seg_5632" n="e" s="T675">ige? </ts>
               <ts e="T677" id="Seg_5634" n="e" s="T676">Veršok </ts>
               <ts e="T678" id="Seg_5636" n="e" s="T677">ige. </ts>
               <ts e="T679" id="Seg_5638" n="e" s="T678">Dĭgəttə </ts>
               <ts e="T680" id="Seg_5640" n="e" s="T679">bazoʔ </ts>
               <ts e="T681" id="Seg_5642" n="e" s="T680">nüdʼin, </ts>
               <ts e="T682" id="Seg_5644" n="e" s="T681">bazoʔ </ts>
               <ts e="T683" id="Seg_5646" n="e" s="T682">kuzurleʔbə </ts>
               <ts e="T684" id="Seg_5648" n="e" s="T683">lʼisʼitsa. </ts>
               <ts e="T685" id="Seg_5650" n="e" s="T684">Šindi </ts>
               <ts e="T686" id="Seg_5652" n="e" s="T685">kuzurleʔbə? </ts>
               <ts e="T687" id="Seg_5654" n="e" s="T686">Da </ts>
               <ts e="T688" id="Seg_5656" n="e" s="T687">măna </ts>
               <ts e="T689" id="Seg_5658" n="e" s="T688">kăštəlaʔbəʔjə </ts>
               <ts e="T690" id="Seg_5660" n="e" s="T689">ešši </ts>
               <ts e="T691" id="Seg_5662" n="e" s="T690">(ködərzittə). </ts>
               <ts e="T692" id="Seg_5664" n="e" s="T691">No, </ts>
               <ts e="T693" id="Seg_5666" n="e" s="T692">kanaʔ! </ts>
               <ts e="T694" id="Seg_5668" n="e" s="T693">Dĭ </ts>
               <ts e="T695" id="Seg_5670" n="e" s="T694">kambi. </ts>
               <ts e="T696" id="Seg_5672" n="e" s="T695">Ambi, </ts>
               <ts e="T697" id="Seg_5674" n="e" s="T696">ambi, </ts>
               <ts e="T698" id="Seg_5676" n="e" s="T697">dĭgəttə </ts>
               <ts e="T699" id="Seg_5678" n="e" s="T698">šobi: </ts>
               <ts e="T700" id="Seg_5680" n="e" s="T699">Šindi </ts>
               <ts e="T701" id="Seg_5682" n="e" s="T700">dĭn </ts>
               <ts e="T702" id="Seg_5684" n="e" s="T701">šobi? </ts>
               <ts e="T703" id="Seg_5686" n="e" s="T702">Sʼerʼodušok </ts>
               <ts e="T704" id="Seg_5688" n="e" s="T703">šobi. </ts>
               <ts e="T705" id="Seg_5690" n="e" s="T704">Dĭgəttə </ts>
               <ts e="T706" id="Seg_5692" n="e" s="T705">nagurgit </ts>
               <ts e="T707" id="Seg_5694" n="e" s="T706">nüdʼin </ts>
               <ts e="T708" id="Seg_5696" n="e" s="T707">bazoʔ </ts>
               <ts e="T709" id="Seg_5698" n="e" s="T708">kuzurleʔbə. </ts>
               <ts e="T710" id="Seg_5700" n="e" s="T709">Dĭgəttə: </ts>
               <ts e="T711" id="Seg_5702" n="e" s="T710">Šindidə </ts>
               <ts e="T712" id="Seg_5704" n="e" s="T711">kuzurleʔbə, </ts>
               <ts e="T713" id="Seg_5706" n="e" s="T712">da </ts>
               <ts e="T714" id="Seg_5708" n="e" s="T713">măna </ts>
               <ts e="T715" id="Seg_5710" n="e" s="T714">kăštəlaʔbəʔjə. </ts>
               <ts e="T716" id="Seg_5712" n="e" s="T715">No, </ts>
               <ts e="T717" id="Seg_5714" n="e" s="T716">kanaʔ! </ts>
               <ts e="T718" id="Seg_5716" n="e" s="T717">(Dĭgəttə) </ts>
               <ts e="T719" id="Seg_5718" n="e" s="T718">dĭ </ts>
               <ts e="T720" id="Seg_5720" n="e" s="T719">ambi. </ts>
               <ts e="T721" id="Seg_5722" n="e" s="T720">Bar </ts>
               <ts e="T722" id="Seg_5724" n="e" s="T721">šobi. </ts>
               <ts e="T723" id="Seg_5726" n="e" s="T722">Šindi </ts>
               <ts e="T724" id="Seg_5728" n="e" s="T723">dĭn? </ts>
               <ts e="T725" id="Seg_5730" n="e" s="T724">Dĭ, </ts>
               <ts e="T726" id="Seg_5732" n="e" s="T725">skrʼobušok </ts>
               <ts e="T727" id="Seg_5734" n="e" s="T726">šobi. </ts>
               <ts e="T728" id="Seg_5736" n="e" s="T727">Dĭgəttə </ts>
               <ts e="T729" id="Seg_5738" n="e" s="T728">ertən </ts>
               <ts e="T730" id="Seg_5740" n="e" s="T729">uʔbdəbiʔi, </ts>
               <ts e="T731" id="Seg_5742" n="e" s="T730">dĭ </ts>
               <ts e="T732" id="Seg_5744" n="e" s="T731">ĭzemnuʔbi. </ts>
               <ts e="T733" id="Seg_5746" n="e" s="T732">Măndə: </ts>
               <ts e="T734" id="Seg_5748" n="e" s="T733">Deʔ </ts>
               <ts e="T735" id="Seg_5750" n="e" s="T734">măna </ts>
               <ts e="T736" id="Seg_5752" n="e" s="T735">nʼamgaʔi! </ts>
               <ts e="T737" id="Seg_5754" n="e" s="T736">Dĭ </ts>
               <ts e="T738" id="Seg_5756" n="e" s="T737">kambi. </ts>
               <ts e="T739" id="Seg_5758" n="e" s="T738">Da </ts>
               <ts e="T740" id="Seg_5760" n="e" s="T739">naga, </ts>
               <ts e="T741" id="Seg_5762" n="e" s="T740">šindidə </ts>
               <ts e="T742" id="Seg_5764" n="e" s="T741">amnuʔpi! </ts>
               <ts e="T743" id="Seg_5766" n="e" s="T742">Da </ts>
               <ts e="T744" id="Seg_5768" n="e" s="T743">tăn </ts>
               <ts e="T745" id="Seg_5770" n="e" s="T744">ambial! </ts>
               <ts e="T746" id="Seg_5772" n="e" s="T745">Dĭ </ts>
               <ts e="T747" id="Seg_5774" n="e" s="T746">bar:" </ts>
               <ts e="T748" id="Seg_5776" n="e" s="T747">Ej, </ts>
               <ts e="T749" id="Seg_5778" n="e" s="T748">măn </ts>
               <ts e="T750" id="Seg_5780" n="e" s="T749">ej </ts>
               <ts e="T751" id="Seg_5782" n="e" s="T750">ambiam!" </ts>
               <ts e="T752" id="Seg_5784" n="e" s="T751">"Dĭgəttə </ts>
               <ts e="T753" id="Seg_5786" n="e" s="T752">davaj </ts>
               <ts e="T754" id="Seg_5788" n="e" s="T753">kujanə </ts>
               <ts e="T755" id="Seg_5790" n="e" s="T754">iʔbəbeʔ, </ts>
               <ts e="T756" id="Seg_5792" n="e" s="T755">šində </ts>
               <ts e="T757" id="Seg_5794" n="e" s="T756">ambi, </ts>
               <ts e="T758" id="Seg_5796" n="e" s="T757">dĭn </ts>
               <ts e="T759" id="Seg_5798" n="e" s="T758">nʼamga </ts>
               <ts e="T760" id="Seg_5800" n="e" s="T759">nanat </ts>
               <ts e="T761" id="Seg_5802" n="e" s="T760">moləj. </ts>
               <ts e="T762" id="Seg_5804" n="e" s="T761">Dĭgəttə </ts>
               <ts e="T763" id="Seg_5806" n="e" s="T762">kuliat </ts>
               <ts e="T764" id="Seg_5808" n="e" s="T763">bostə </ts>
               <ts e="T765" id="Seg_5810" n="e" s="T764">nanəndə. </ts>
               <ts e="T766" id="Seg_5812" n="e" s="T765">Nʼamga </ts>
               <ts e="T767" id="Seg_5814" n="e" s="T766">ige. </ts>
               <ts e="T768" id="Seg_5816" n="e" s="T767">Dĭ </ts>
               <ts e="T769" id="Seg_5818" n="e" s="T768">davaj </ts>
               <ts e="T770" id="Seg_5820" n="e" s="T769">dĭ </ts>
               <ts e="T771" id="Seg_5822" n="e" s="T770">volktə </ts>
               <ts e="T772" id="Seg_5824" n="e" s="T771">tʼuʔsittə. </ts>
               <ts e="T773" id="Seg_5826" n="e" s="T772">Uʔbdəʔ, </ts>
               <ts e="T774" id="Seg_5828" n="e" s="T773">tăn </ts>
               <ts e="T775" id="Seg_5830" n="e" s="T774">(š-) </ts>
               <ts e="T776" id="Seg_5832" n="e" s="T775">ambial! </ts>
               <ts e="T777" id="Seg_5834" n="e" s="T776">Dĭgəttə </ts>
               <ts e="T778" id="Seg_5836" n="e" s="T777">dĭ </ts>
               <ts e="T779" id="Seg_5838" n="e" s="T778">uʔbdəbi: </ts>
               <ts e="T780" id="Seg_5840" n="e" s="T779">No, </ts>
               <ts e="T781" id="Seg_5842" n="e" s="T780">măn </ts>
               <ts e="T782" id="Seg_5844" n="e" s="T781">ambiam. </ts>
               <ts e="T783" id="Seg_5846" n="e" s="T782">Dĭgəttə </ts>
               <ts e="T784" id="Seg_5848" n="e" s="T783">bar. </ts>
               <ts e="T785" id="Seg_5850" n="e" s="T784">Süjö </ts>
               <ts e="T786" id="Seg_5852" n="e" s="T785">i </ts>
               <ts e="T787" id="Seg_5854" n="e" s="T786">tʼetʼer </ts>
               <ts e="T788" id="Seg_5856" n="e" s="T787">(amnoʔjə). </ts>
               <ts e="T789" id="Seg_5858" n="e" s="T788">Kăda </ts>
               <ts e="T790" id="Seg_5860" n="e" s="T789">tura </ts>
               <ts e="T791" id="Seg_5862" n="e" s="T790">jaʔsittə. </ts>
               <ts e="T792" id="Seg_5864" n="e" s="T791">Baltu </ts>
               <ts e="T793" id="Seg_5866" n="e" s="T792">naga, </ts>
               <ts e="T794" id="Seg_5868" n="e" s="T793">šindi </ts>
               <ts e="T795" id="Seg_5870" n="e" s="T794">baltu </ts>
               <ts e="T796" id="Seg_5872" n="e" s="T795">(aləj). </ts>
               <ts e="T797" id="Seg_5874" n="e" s="T796">Da, </ts>
               <ts e="T798" id="Seg_5876" n="e" s="T797">na_što </ts>
               <ts e="T799" id="Seg_5878" n="e" s="T798">măna </ts>
               <ts e="T800" id="Seg_5880" n="e" s="T799">(tăra-) </ts>
               <ts e="T801" id="Seg_5882" n="e" s="T800">tura. </ts>
               <ts e="T802" id="Seg_5884" n="e" s="T801">Măn </ts>
               <ts e="T803" id="Seg_5886" n="e" s="T802">sĭregən </ts>
               <ts e="T804" id="Seg_5888" n="e" s="T803">šalam. </ts>
               <ts e="T805" id="Seg_5890" n="e" s="T804">I </ts>
               <ts e="T806" id="Seg_5892" n="e" s="T805">saʔməluʔpi </ts>
               <ts e="T807" id="Seg_5894" n="e" s="T806">(šĭr-) </ts>
               <ts e="T808" id="Seg_5896" n="e" s="T807">sĭrenə. </ts>
               <ts e="T809" id="Seg_5898" n="e" s="T808">Dĭn </ts>
               <ts e="T810" id="Seg_5900" n="e" s="T809">šabi, </ts>
               <ts e="T811" id="Seg_5902" n="e" s="T810">ertən </ts>
               <ts e="T812" id="Seg_5904" n="e" s="T811">uʔbdəbi. </ts>
               <ts e="T813" id="Seg_5906" n="e" s="T812">I </ts>
               <ts e="T814" id="Seg_5908" n="e" s="T813">kambi </ts>
               <ts e="T815" id="Seg_5910" n="e" s="T814">kuzittə, </ts>
               <ts e="T816" id="Seg_5912" n="e" s="T815">elezeŋdə </ts>
               <ts e="T817" id="Seg_5914" n="e" s="T816">kubi, </ts>
               <ts e="T818" id="Seg_5916" n="e" s="T817">dĭn </ts>
               <ts e="T819" id="Seg_5918" n="e" s="T818">sʼarbiʔi. </ts>
               <ts e="T820" id="Seg_5920" n="e" s="T819">Suʔmiluʔpiʔi </ts>
               <ts e="T821" id="Seg_5922" n="e" s="T820">(paʔn-) </ts>
               <ts e="T822" id="Seg_5924" n="e" s="T821">paʔinə. </ts>
               <ts e="T823" id="Seg_5926" n="e" s="T822">Dĭgəttə </ts>
               <ts e="T824" id="Seg_5928" n="e" s="T823">boskəndə </ts>
               <ts e="T825" id="Seg_5930" n="e" s="T824">gnʼozdaʔi </ts>
               <ts e="T826" id="Seg_5932" n="e" s="T825">abiʔi. </ts>
               <ts e="T827" id="Seg_5934" n="e" s="T826">Munuʔi </ts>
               <ts e="T828" id="Seg_5936" n="e" s="T827">deʔpiʔi. </ts>
               <ts e="T829" id="Seg_5938" n="e" s="T828">Esseŋdə, </ts>
               <ts e="T830" id="Seg_5940" n="e" s="T829">mobiʔi </ts>
               <ts e="T831" id="Seg_5942" n="e" s="T830">dĭgəttə </ts>
               <ts e="T832" id="Seg_5944" n="e" s="T831">esseŋdə. </ts>
               <ts e="T833" id="Seg_5946" n="e" s="T832">Bădəbiʔi </ts>
               <ts e="T834" id="Seg_5948" n="e" s="T833">maškaziʔ, </ts>
               <ts e="T835" id="Seg_5950" n="e" s="T834">červəʔiziʔ. </ts>
               <ts e="T836" id="Seg_5952" n="e" s="T835">Dĭgəttə </ts>
               <ts e="T837" id="Seg_5954" n="e" s="T836">bazoʔ, </ts>
               <ts e="T838" id="Seg_5956" n="e" s="T837">na_što </ts>
               <ts e="T839" id="Seg_5958" n="e" s="T838">miʔnʼibeʔ </ts>
               <ts e="T840" id="Seg_5960" n="e" s="T839">maʔ? </ts>
               <ts e="T841" id="Seg_5962" n="e" s="T840">Sĭregən </ts>
               <ts e="T842" id="Seg_5964" n="e" s="T841">kunolbibaʔ. </ts>
               <ts e="T843" id="Seg_5966" n="e" s="T842">Onʼiʔ </ts>
               <ts e="T844" id="Seg_5968" n="e" s="T843">nüdʼi </ts>
               <ts e="T845" id="Seg_5970" n="e" s="T844">makən </ts>
               <ts e="T846" id="Seg_5972" n="e" s="T845">amnolbaʔ </ts>
               <ts e="T847" id="Seg_5974" n="e" s="T846">(dĭ). </ts>
               <ts e="T848" id="Seg_5976" n="e" s="T847">Bar </ts>
               <ts e="T849" id="Seg_5978" n="e" s="T848">vʼezʼdʼe </ts>
               <ts e="T850" id="Seg_5980" n="e" s="T849">măndərlaʔbəbaʔ. </ts>
               <ts e="T851" id="Seg_5982" n="e" s="T850">Kabarləj. </ts>
               <ts e="T852" id="Seg_5984" n="e" s="T851">Amnobiʔi </ts>
               <ts e="T853" id="Seg_5986" n="e" s="T852">nükke </ts>
               <ts e="T854" id="Seg_5988" n="e" s="T853">da </ts>
               <ts e="T855" id="Seg_5990" n="e" s="T854">büzʼe. </ts>
               <ts e="T856" id="Seg_5992" n="e" s="T855">(Dĭ-) </ts>
               <ts e="T857" id="Seg_5994" n="e" s="T856">Dĭzeŋ </ts>
               <ts e="T858" id="Seg_5996" n="e" s="T857">ĭmbidə </ts>
               <ts e="T859" id="Seg_5998" n="e" s="T858">nagobi. </ts>
               <ts e="T860" id="Seg_6000" n="e" s="T859">Kambiʔi </ts>
               <ts e="T861" id="Seg_6002" n="e" s="T860">dʼijenə. </ts>
               <ts e="T862" id="Seg_6004" n="e" s="T861">Šiškaʔi </ts>
               <ts e="T863" id="Seg_6006" n="e" s="T862">žoludʼi </ts>
               <ts e="T864" id="Seg_6008" n="e" s="T863">oʔbdobiʔi. </ts>
               <ts e="T865" id="Seg_6010" n="e" s="T864">Šobiʔi, </ts>
               <ts e="T866" id="Seg_6012" n="e" s="T865">amnaʔbə </ts>
               <ts e="T867" id="Seg_6014" n="e" s="T866">bar. </ts>
               <ts e="T868" id="Seg_6016" n="e" s="T867">Onʼiʔ </ts>
               <ts e="T869" id="Seg_6018" n="e" s="T868">saʔməluʔpi </ts>
               <ts e="T870" id="Seg_6020" n="e" s="T869">patpolʼlʼagən. </ts>
               <ts e="T871" id="Seg_6022" n="e" s="T870">Dĭgəttə </ts>
               <ts e="T872" id="Seg_6024" n="e" s="T871">davaj </ts>
               <ts e="T873" id="Seg_6026" n="e" s="T872">özerzittə. </ts>
               <ts e="T874" id="Seg_6028" n="e" s="T873">(Nuge) </ts>
               <ts e="T875" id="Seg_6030" n="e" s="T874">Nükke </ts>
               <ts e="T876" id="Seg_6032" n="e" s="T875">măndə. </ts>
               <ts e="T877" id="Seg_6034" n="e" s="T876">Baltuzʼiʔ </ts>
               <ts e="T878" id="Seg_6036" n="e" s="T877">jaʔdə </ts>
               <ts e="T879" id="Seg_6038" n="e" s="T878">poldə. </ts>
               <ts e="T880" id="Seg_6040" n="e" s="T879">Dĭ </ts>
               <ts e="T881" id="Seg_6042" n="e" s="T880">jaʔpi. </ts>
               <ts e="T882" id="Seg_6044" n="e" s="T881">Dĭ </ts>
               <ts e="T883" id="Seg_6046" n="e" s="T882">döbər </ts>
               <ts e="T884" id="Seg_6048" n="e" s="T883">özerbi. </ts>
               <ts e="T885" id="Seg_6050" n="e" s="T884">Tuj </ts>
               <ts e="T886" id="Seg_6052" n="e" s="T885">dĭn </ts>
               <ts e="T887" id="Seg_6054" n="e" s="T886">nʼuʔdə </ts>
               <ts e="T888" id="Seg_6056" n="e" s="T887">bar </ts>
               <ts e="T889" id="Seg_6058" n="e" s="T888">(oʔbdo=), </ts>
               <ts e="T890" id="Seg_6060" n="e" s="T889">dĭ </ts>
               <ts e="T891" id="Seg_6062" n="e" s="T890">dibər </ts>
               <ts e="T892" id="Seg_6064" n="e" s="T891">krɨšabə </ts>
               <ts e="T893" id="Seg_6066" n="e" s="T892">snʼal. </ts>
               <ts e="T894" id="Seg_6068" n="e" s="T893">Dibər </ts>
               <ts e="T895" id="Seg_6070" n="e" s="T894">kuŋgeŋ </ts>
               <ts e="T896" id="Seg_6072" n="e" s="T895">özerbi </ts>
               <ts e="T897" id="Seg_6074" n="e" s="T896">daže </ts>
               <ts e="T898" id="Seg_6076" n="e" s="T897">nʼuʔdə. </ts>
               <ts e="T899" id="Seg_6078" n="e" s="T898">Dĭgəttə </ts>
               <ts e="T900" id="Seg_6080" n="e" s="T899">büzʼe: </ts>
               <ts e="T901" id="Seg_6082" n="e" s="T900">Nada </ts>
               <ts e="T902" id="Seg_6084" n="e" s="T901">kanzittə </ts>
               <ts e="T903" id="Seg_6086" n="e" s="T902">oʔbdəsʼtə </ts>
               <ts e="T904" id="Seg_6088" n="e" s="T903">žoludʼi. </ts>
               <ts e="T905" id="Seg_6090" n="e" s="T904">Sʼabi, </ts>
               <ts e="T906" id="Seg_6092" n="e" s="T905">sʼabi </ts>
               <ts e="T907" id="Seg_6094" n="e" s="T906">(uj-), </ts>
               <ts e="T908" id="Seg_6096" n="e" s="T907">dibər </ts>
               <ts e="T909" id="Seg_6098" n="e" s="T908">nʼuʔdə </ts>
               <ts e="T910" id="Seg_6100" n="e" s="T909">dĭn </ts>
               <ts e="T911" id="Seg_6102" n="e" s="T910">kulaʔbə. </ts>
               <ts e="T912" id="Seg_6104" n="e" s="T911">Tura </ts>
               <ts e="T913" id="Seg_6106" n="e" s="T912">(i-) </ts>
               <ts e="T914" id="Seg_6108" n="e" s="T913">nuga. </ts>
               <ts e="T915" id="Seg_6110" n="e" s="T914">Dibər </ts>
               <ts e="T916" id="Seg_6112" n="e" s="T915">turanə </ts>
               <ts e="T917" id="Seg_6114" n="e" s="T916">šobi. </ts>
               <ts e="T918" id="Seg_6116" n="e" s="T917">Dĭn </ts>
               <ts e="T919" id="Seg_6118" n="e" s="T918">kurizən </ts>
               <ts e="T920" id="Seg_6120" n="e" s="T919">tibi </ts>
               <ts e="T921" id="Seg_6122" n="e" s="T920">amnolaʔbə. </ts>
               <ts e="T922" id="Seg_6124" n="e" s="T921">Ulut </ts>
               <ts e="T923" id="Seg_6126" n="e" s="T922">kuvas. </ts>
               <ts e="T924" id="Seg_6128" n="e" s="T923">Komu </ts>
               <ts e="T925" id="Seg_6130" n="e" s="T924">bar. </ts>
               <ts e="T926" id="Seg_6132" n="e" s="T925">I </ts>
               <ts e="T927" id="Seg_6134" n="e" s="T926">dĭn </ts>
               <ts e="T928" id="Seg_6136" n="e" s="T927">(žernovkazaŋdə) </ts>
               <ts e="T929" id="Seg_6138" n="e" s="T928">ige, </ts>
               <ts e="T930" id="Seg_6140" n="e" s="T929">dĭ </ts>
               <ts e="T931" id="Seg_6142" n="e" s="T930">ibi </ts>
               <ts e="T932" id="Seg_6144" n="e" s="T931">dĭm </ts>
               <ts e="T933" id="Seg_6146" n="e" s="T932">i </ts>
               <ts e="T934" id="Seg_6148" n="e" s="T933">šobi: </ts>
               <ts e="T935" id="Seg_6150" n="e" s="T934">Na, </ts>
               <ts e="T936" id="Seg_6152" n="e" s="T935">tănan. </ts>
               <ts e="T937" id="Seg_6154" n="e" s="T936">Kurizən </ts>
               <ts e="T938" id="Seg_6156" n="e" s="T937">tibi, </ts>
               <ts e="T939" id="Seg_6158" n="e" s="T938">komu </ts>
               <ts e="T940" id="Seg_6160" n="e" s="T939">ulut. </ts>
               <ts e="T941" id="Seg_6162" n="e" s="T940">Dĭ </ts>
               <ts e="T942" id="Seg_6164" n="e" s="T941">ibi </ts>
               <ts e="T943" id="Seg_6166" n="e" s="T942">da </ts>
               <ts e="T944" id="Seg_6168" n="e" s="T943">davaj </ts>
               <ts e="T945" id="Seg_6170" n="e" s="T944">(žernovka=) </ts>
               <ts e="T946" id="Seg_6172" n="e" s="T945">dĭ </ts>
               <ts e="T947" id="Seg_6174" n="e" s="T946">tʼerməndə </ts>
               <ts e="T948" id="Seg_6176" n="e" s="T947">kurzittə. </ts>
               <ts e="T949" id="Seg_6178" n="e" s="T948">Dĭn </ts>
               <ts e="T950" id="Seg_6180" n="e" s="T949">blinʔi, </ts>
               <ts e="T951" id="Seg_6182" n="e" s="T950">pirogəʔi, </ts>
               <ts e="T952" id="Seg_6184" n="e" s="T951">blinʔi, </ts>
               <ts e="T953" id="Seg_6186" n="e" s="T952">pirogəʔi, </ts>
               <ts e="T954" id="Seg_6188" n="e" s="T953">büzʼe </ts>
               <ts e="T955" id="Seg_6190" n="e" s="T954">ambi </ts>
               <ts e="T956" id="Seg_6192" n="e" s="T955">i </ts>
               <ts e="T957" id="Seg_6194" n="e" s="T956">nükke </ts>
               <ts e="T958" id="Seg_6196" n="e" s="T957">ambi. </ts>
               <ts e="T959" id="Seg_6198" n="e" s="T958">Uge </ts>
               <ts e="T960" id="Seg_6200" n="e" s="T959">dăra </ts>
               <ts e="T961" id="Seg_6202" n="e" s="T960">amnolaʔ, </ts>
               <ts e="T962" id="Seg_6204" n="e" s="T961">dĭgəttə </ts>
               <ts e="T963" id="Seg_6206" n="e" s="T962">šobi </ts>
               <ts e="T964" id="Seg_6208" n="e" s="T963">dĭzeŋdə </ts>
               <ts e="T965" id="Seg_6210" n="e" s="T964">barin. </ts>
               <ts e="T966" id="Seg_6212" n="e" s="T965">Deʔkeʔ </ts>
               <ts e="T967" id="Seg_6214" n="e" s="T966">măna </ts>
               <ts e="T968" id="Seg_6216" n="e" s="T967">amzittə. </ts>
               <ts e="T969" id="Seg_6218" n="e" s="T968">Dĭ </ts>
               <ts e="T970" id="Seg_6220" n="e" s="T969">nünəbi </ts>
               <ts e="T971" id="Seg_6222" n="e" s="T970">pravda </ts>
               <ts e="T972" id="Seg_6224" n="e" s="T971">dĭ </ts>
               <ts e="T973" id="Seg_6226" n="e" s="T972">(di- </ts>
               <ts e="T974" id="Seg_6228" n="e" s="T973">dĭ </ts>
               <ts e="T975" id="Seg_6230" n="e" s="T974">zər-) </ts>
               <ts e="T976" id="Seg_6232" n="e" s="T975">tʼermən. </ts>
               <ts e="T977" id="Seg_6234" n="e" s="T976">Dĭgəttə </ts>
               <ts e="T978" id="Seg_6236" n="e" s="T977">nüket </ts>
               <ts e="T979" id="Seg_6238" n="e" s="T978">bar </ts>
               <ts e="T980" id="Seg_6240" n="e" s="T979">davaj </ts>
               <ts e="T981" id="Seg_6242" n="e" s="T980">tʼerməndə </ts>
               <ts e="T982" id="Seg_6244" n="e" s="T981">kurzittə, </ts>
               <ts e="T983" id="Seg_6246" n="e" s="T982">blinʔi, </ts>
               <ts e="T984" id="Seg_6248" n="e" s="T983">pirogəʔi. </ts>
               <ts e="T985" id="Seg_6250" n="e" s="T984">Dĭ </ts>
               <ts e="T986" id="Seg_6252" n="e" s="T985">ambi. </ts>
               <ts e="T987" id="Seg_6254" n="e" s="T986">Sadargaʔ </ts>
               <ts e="T988" id="Seg_6256" n="e" s="T987">măna. </ts>
               <ts e="T989" id="Seg_6258" n="e" s="T988">Dʼok, </ts>
               <ts e="T990" id="Seg_6260" n="e" s="T989">miʔ </ts>
               <ts e="T991" id="Seg_6262" n="e" s="T990">ej </ts>
               <ts e="T992" id="Seg_6264" n="e" s="T991">sadarlam. </ts>
               <ts e="T993" id="Seg_6266" n="e" s="T992">A_to </ts>
               <ts e="T994" id="Seg_6268" n="e" s="T993">(n-) </ts>
               <ts e="T995" id="Seg_6270" n="e" s="T994">miʔnʼibeʔ </ts>
               <ts e="T996" id="Seg_6272" n="e" s="T995">amzittə </ts>
               <ts e="T997" id="Seg_6274" n="e" s="T996">ĭmbi? </ts>
               <ts e="T998" id="Seg_6276" n="e" s="T997">Dĭgəttə </ts>
               <ts e="T999" id="Seg_6278" n="e" s="T998">dĭ </ts>
               <ts e="T1000" id="Seg_6280" n="e" s="T999">nüdʼin </ts>
               <ts e="T1001" id="Seg_6282" n="e" s="T1000">šobi. </ts>
               <ts e="T1002" id="Seg_6284" n="e" s="T1001">I </ts>
               <ts e="T1003" id="Seg_6286" n="e" s="T1002">tojirlaʔ </ts>
               <ts e="T1004" id="Seg_6288" n="e" s="T1003">iluʔpi. </ts>
               <ts e="T1005" id="Seg_6290" n="e" s="T1004">I </ts>
               <ts e="T1006" id="Seg_6292" n="e" s="T1005">konnaːmbi. </ts>
               <ts e="T1007" id="Seg_6294" n="e" s="T1006">Ertən </ts>
               <ts e="T1008" id="Seg_6296" n="e" s="T1007">dĭzeŋ </ts>
               <ts e="T1009" id="Seg_6298" n="e" s="T1008">uʔbdəbiʔi: </ts>
               <ts e="T1010" id="Seg_6300" n="e" s="T1009">naga </ts>
               <ts e="T1011" id="Seg_6302" n="e" s="T1010">tʼerməndə. </ts>
               <ts e="T1012" id="Seg_6304" n="e" s="T1011">Dĭgəttə </ts>
               <ts e="T1013" id="Seg_6306" n="e" s="T1012">tibi </ts>
               <ts e="T1014" id="Seg_6308" n="e" s="T1013">kuriz: </ts>
               <ts e="T1015" id="Seg_6310" n="e" s="T1014">Iʔ </ts>
               <ts e="T1016" id="Seg_6312" n="e" s="T1015">dʼorgaʔ! </ts>
               <ts e="T1017" id="Seg_6314" n="e" s="T1016">Măn </ts>
               <ts e="T1018" id="Seg_6316" n="e" s="T1017">kallam. </ts>
               <ts e="T1019" id="Seg_6318" n="e" s="T1018">(Di-) </ts>
               <ts e="T1020" id="Seg_6320" n="e" s="T1019">Detlim </ts>
               <ts e="T1021" id="Seg_6322" n="e" s="T1020">bazoʔ </ts>
               <ts e="T1022" id="Seg_6324" n="e" s="T1021">šiʔnʼileʔ. </ts>
               <ts e="T1023" id="Seg_6326" n="e" s="T1022">Kambi. </ts>
               <ts e="T1024" id="Seg_6328" n="e" s="T1023">Kandəga, </ts>
               <ts e="T1025" id="Seg_6330" n="e" s="T1024">kandəga, </ts>
               <ts e="T1026" id="Seg_6332" n="e" s="T1025">šonəga </ts>
               <ts e="T1027" id="Seg_6334" n="e" s="T1026">diʔnə </ts>
               <ts e="T1028" id="Seg_6336" n="e" s="T1027">lʼisʼitsa. </ts>
               <ts e="T1029" id="Seg_6338" n="e" s="T1028">Gibər </ts>
               <ts e="T1030" id="Seg_6340" n="e" s="T1029">kandəgal? </ts>
               <ts e="T1031" id="Seg_6342" n="e" s="T1030">Iʔ </ts>
               <ts e="T1032" id="Seg_6344" n="e" s="T1031">măna! </ts>
               <ts e="T1033" id="Seg_6346" n="e" s="T1032">Bădaʔ </ts>
               <ts e="T1034" id="Seg_6348" n="e" s="T1033">măna </ts>
               <ts e="T1035" id="Seg_6350" n="e" s="T1034">šüjöndə, </ts>
               <ts e="T1036" id="Seg_6352" n="e" s="T1035">dĭ </ts>
               <ts e="T1037" id="Seg_6354" n="e" s="T1036">băʔpi. </ts>
               <ts e="T1038" id="Seg_6356" n="e" s="T1037">Dĭgəttə </ts>
               <ts e="T1039" id="Seg_6358" n="e" s="T1038">šonəga, </ts>
               <ts e="T1040" id="Seg_6360" n="e" s="T1039">šonəga. </ts>
               <ts e="T1041" id="Seg_6362" n="e" s="T1040">Volkdə. </ts>
               <ts e="T1042" id="Seg_6364" n="e" s="T1041">Gibər </ts>
               <ts e="T1043" id="Seg_6366" n="e" s="T1042">kandəgal? </ts>
               <ts e="T1044" id="Seg_6368" n="e" s="T1043">Tʼerməndə </ts>
               <ts e="T1045" id="Seg_6370" n="e" s="T1044">izittə. </ts>
               <ts e="T1046" id="Seg_6372" n="e" s="T1045">(Mă-) </ts>
               <ts e="T1047" id="Seg_6374" n="e" s="T1046">Iʔ </ts>
               <ts e="T1048" id="Seg_6376" n="e" s="T1047">măna! </ts>
               <ts e="T1049" id="Seg_6378" n="e" s="T1048">Bădaʔ </ts>
               <ts e="T1050" id="Seg_6380" n="e" s="T1049">măna </ts>
               <ts e="T1051" id="Seg_6382" n="e" s="T1050">šüjöndə! </ts>
               <ts e="T1052" id="Seg_6384" n="e" s="T1051">Dĭ </ts>
               <ts e="T1053" id="Seg_6386" n="e" s="T1052">băʔpi. </ts>
               <ts e="T1054" id="Seg_6388" n="e" s="T1053">Dĭgəttə </ts>
               <ts e="T1055" id="Seg_6390" n="e" s="T1054">šonəga, </ts>
               <ts e="T1056" id="Seg_6392" n="e" s="T1055">urgaːba </ts>
               <ts e="T1057" id="Seg_6394" n="e" s="T1056">šonəga. </ts>
               <ts e="T1058" id="Seg_6396" n="e" s="T1057">Bădaʔ </ts>
               <ts e="T1059" id="Seg_6398" n="e" s="T1058">măna! </ts>
               <ts e="T1060" id="Seg_6400" n="e" s="T1059">Dĭgəttə </ts>
               <ts e="T1061" id="Seg_6402" n="e" s="T1060">šobi. </ts>
               <ts e="T1062" id="Seg_6404" n="e" s="T1061">Davaj </ts>
               <ts e="T1063" id="Seg_6406" n="e" s="T1062">kirgarzittə. </ts>
               <ts e="T1064" id="Seg_6408" n="e" s="T1063">Atdaj! </ts>
               <ts e="T1065" id="Seg_6410" n="e" s="T1064">Dettə </ts>
               <ts e="T1066" id="Seg_6412" n="e" s="T1065">măn </ts>
               <ts e="T1067" id="Seg_6414" n="e" s="T1066">tʼerməndə. </ts>
               <ts e="T1068" id="Seg_6416" n="e" s="T1067">Dettə </ts>
               <ts e="T1069" id="Seg_6418" n="e" s="T1068">măn </ts>
               <ts e="T1070" id="Seg_6420" n="e" s="T1069">tʼerməndə. </ts>
               <ts e="T1071" id="Seg_6422" n="e" s="T1070">Dĭ </ts>
               <ts e="T1072" id="Seg_6424" n="e" s="T1071">măndə. </ts>
               <ts e="T1073" id="Seg_6426" n="e" s="T1072">(Baʔluʔ-) </ts>
               <ts e="T1074" id="Seg_6428" n="e" s="T1073">Baʔgaʔ </ts>
               <ts e="T1075" id="Seg_6430" n="e" s="T1074">dĭm. </ts>
               <ts e="T1076" id="Seg_6432" n="e" s="T1075">A </ts>
               <ts e="T1077" id="Seg_6434" n="e" s="T1076">nabəʔinə </ts>
               <ts e="T1078" id="Seg_6436" n="e" s="T1077">pušaj. </ts>
               <ts e="T1079" id="Seg_6438" n="e" s="T1078">Nabəʔi </ts>
               <ts e="T1080" id="Seg_6440" n="e" s="T1079">amnuʔbə </ts>
               <ts e="T1081" id="Seg_6442" n="e" s="T1080">(ləʔi). </ts>
               <ts e="T1082" id="Seg_6444" n="e" s="T1081">Dĭm </ts>
               <ts e="T1083" id="Seg_6446" n="e" s="T1082">baʔluʔpi, </ts>
               <ts e="T1084" id="Seg_6448" n="e" s="T1083">a </ts>
               <ts e="T1085" id="Seg_6450" n="e" s="T1084">dĭ </ts>
               <ts e="T1086" id="Seg_6452" n="e" s="T1085">(măndə). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PKZ">
            <ta e="T3" id="Seg_6453" s="T0">PKZ_196X_SU0191.PKZ.001 (001)</ta>
            <ta e="T10" id="Seg_6454" s="T4">PKZ_196X_SU0191.PKZ.002 (003)</ta>
            <ta e="T17" id="Seg_6455" s="T10">PKZ_196X_SU0191.PKZ.003 (004)</ta>
            <ta e="T23" id="Seg_6456" s="T17">PKZ_196X_SU0191.PKZ.004 (005)</ta>
            <ta e="T33" id="Seg_6457" s="T23">PKZ_196X_SU0191.PKZ.005 (006)</ta>
            <ta e="T41" id="Seg_6458" s="T33">PKZ_196X_SU0191.PKZ.006 (007)</ta>
            <ta e="T43" id="Seg_6459" s="T41">PKZ_196X_SU0191.PKZ.007 (008)</ta>
            <ta e="T49" id="Seg_6460" s="T43">PKZ_196X_SU0191.PKZ.008 (009)</ta>
            <ta e="T55" id="Seg_6461" s="T49">PKZ_196X_SU0191.PKZ.009 (010)</ta>
            <ta e="T62" id="Seg_6462" s="T55">PKZ_196X_SU0191.PKZ.010 (011)</ta>
            <ta e="T67" id="Seg_6463" s="T62">PKZ_196X_SU0191.PKZ.011 (012)</ta>
            <ta e="T70" id="Seg_6464" s="T67">PKZ_196X_SU0191.PKZ.012 (013)</ta>
            <ta e="T73" id="Seg_6465" s="T70">PKZ_196X_SU0191.PKZ.013 (014)</ta>
            <ta e="T81" id="Seg_6466" s="T73">PKZ_196X_SU0191.PKZ.014 (015)</ta>
            <ta e="T84" id="Seg_6467" s="T81">PKZ_196X_SU0191.PKZ.015 (016)</ta>
            <ta e="T86" id="Seg_6468" s="T84">PKZ_196X_SU0191.PKZ.016 (017)</ta>
            <ta e="T89" id="Seg_6469" s="T86">PKZ_196X_SU0191.PKZ.017 (018)</ta>
            <ta e="T92" id="Seg_6470" s="T89">PKZ_196X_SU0191.PKZ.018 (019)</ta>
            <ta e="T95" id="Seg_6471" s="T92">PKZ_196X_SU0191.PKZ.019 (020)</ta>
            <ta e="T102" id="Seg_6472" s="T95">PKZ_196X_SU0191.PKZ.020 (021)</ta>
            <ta e="T109" id="Seg_6473" s="T102">PKZ_196X_SU0191.PKZ.021 (022)</ta>
            <ta e="T114" id="Seg_6474" s="T109">PKZ_196X_SU0191.PKZ.022 (023)</ta>
            <ta e="T117" id="Seg_6475" s="T114">PKZ_196X_SU0191.PKZ.023 (024)</ta>
            <ta e="T122" id="Seg_6476" s="T117">PKZ_196X_SU0191.PKZ.024 (025)</ta>
            <ta e="T129" id="Seg_6477" s="T122">PKZ_196X_SU0191.PKZ.025 (026)</ta>
            <ta e="T131" id="Seg_6478" s="T129">PKZ_196X_SU0191.PKZ.026 (027)</ta>
            <ta e="T136" id="Seg_6479" s="T131">PKZ_196X_SU0191.PKZ.027 (028)</ta>
            <ta e="T141" id="Seg_6480" s="T136">PKZ_196X_SU0191.PKZ.028 (029)</ta>
            <ta e="T143" id="Seg_6481" s="T141">PKZ_196X_SU0191.PKZ.029 (030)</ta>
            <ta e="T151" id="Seg_6482" s="T143">PKZ_196X_SU0191.PKZ.030 (031)</ta>
            <ta e="T153" id="Seg_6483" s="T151">PKZ_196X_SU0191.PKZ.031 (032)</ta>
            <ta e="T157" id="Seg_6484" s="T153">PKZ_196X_SU0191.PKZ.032 (033)</ta>
            <ta e="T163" id="Seg_6485" s="T157">PKZ_196X_SU0191.PKZ.033 (034)</ta>
            <ta e="T169" id="Seg_6486" s="T163">PKZ_196X_SU0191.PKZ.034 (035)</ta>
            <ta e="T174" id="Seg_6487" s="T169">PKZ_196X_SU0191.PKZ.035 (036)</ta>
            <ta e="T177" id="Seg_6488" s="T174">PKZ_196X_SU0191.PKZ.036 (037)</ta>
            <ta e="T182" id="Seg_6489" s="T177">PKZ_196X_SU0191.PKZ.037 (038)</ta>
            <ta e="T187" id="Seg_6490" s="T182">PKZ_196X_SU0191.PKZ.038 (039)</ta>
            <ta e="T190" id="Seg_6491" s="T187">PKZ_196X_SU0191.PKZ.039 (040)</ta>
            <ta e="T199" id="Seg_6492" s="T190">PKZ_196X_SU0191.PKZ.040 (041)</ta>
            <ta e="T204" id="Seg_6493" s="T199">PKZ_196X_SU0191.PKZ.041 (042)</ta>
            <ta e="T207" id="Seg_6494" s="T204">PKZ_196X_SU0191.PKZ.042 (043)</ta>
            <ta e="T212" id="Seg_6495" s="T207">PKZ_196X_SU0191.PKZ.043 (044)</ta>
            <ta e="T216" id="Seg_6496" s="T212">PKZ_196X_SU0191.PKZ.044 (045)</ta>
            <ta e="T219" id="Seg_6497" s="T216">PKZ_196X_SU0191.PKZ.045 (046)</ta>
            <ta e="T224" id="Seg_6498" s="T219">PKZ_196X_SU0191.PKZ.046 (047)</ta>
            <ta e="T226" id="Seg_6499" s="T224">PKZ_196X_SU0191.PKZ.047 (048)</ta>
            <ta e="T232" id="Seg_6500" s="T226">PKZ_196X_SU0191.PKZ.048 (049)</ta>
            <ta e="T235" id="Seg_6501" s="T232">PKZ_196X_SU0191.PKZ.049 (050)</ta>
            <ta e="T237" id="Seg_6502" s="T235">PKZ_196X_SU0191.PKZ.050 (051)</ta>
            <ta e="T240" id="Seg_6503" s="T237">PKZ_196X_SU0191.PKZ.051 (052)</ta>
            <ta e="T243" id="Seg_6504" s="T240">PKZ_196X_SU0191.PKZ.052 (053)</ta>
            <ta e="T254" id="Seg_6505" s="T243">PKZ_196X_SU0191.PKZ.053 (054)</ta>
            <ta e="T257" id="Seg_6506" s="T254">PKZ_196X_SU0191.PKZ.054 (055)</ta>
            <ta e="T261" id="Seg_6507" s="T257">PKZ_196X_SU0191.PKZ.055 (056)</ta>
            <ta e="T267" id="Seg_6508" s="T261">PKZ_196X_SU0191.PKZ.056 (057)</ta>
            <ta e="T270" id="Seg_6509" s="T267">PKZ_196X_SU0191.PKZ.057 (058)</ta>
            <ta e="T272" id="Seg_6510" s="T270">PKZ_196X_SU0191.PKZ.058 (059)</ta>
            <ta e="T280" id="Seg_6511" s="T272">PKZ_196X_SU0191.PKZ.059 (060)</ta>
            <ta e="T283" id="Seg_6512" s="T280">PKZ_196X_SU0191.PKZ.060 (061)</ta>
            <ta e="T287" id="Seg_6513" s="T283">PKZ_196X_SU0191.PKZ.061 (062)</ta>
            <ta e="T290" id="Seg_6514" s="T287">PKZ_196X_SU0191.PKZ.062 (063)</ta>
            <ta e="T295" id="Seg_6515" s="T290">PKZ_196X_SU0191.PKZ.063 (064)</ta>
            <ta e="T304" id="Seg_6516" s="T295">PKZ_196X_SU0191.PKZ.064 (065)</ta>
            <ta e="T307" id="Seg_6517" s="T304">PKZ_196X_SU0191.PKZ.065 (066)</ta>
            <ta e="T312" id="Seg_6518" s="T307">PKZ_196X_SU0191.PKZ.066 (067)</ta>
            <ta e="T314" id="Seg_6519" s="T312">PKZ_196X_SU0191.PKZ.067 (068)</ta>
            <ta e="T316" id="Seg_6520" s="T314">PKZ_196X_SU0191.PKZ.068 (069)</ta>
            <ta e="T318" id="Seg_6521" s="T316">PKZ_196X_SU0191.PKZ.069 (070)</ta>
            <ta e="T321" id="Seg_6522" s="T318">PKZ_196X_SU0191.PKZ.070 (071)</ta>
            <ta e="T328" id="Seg_6523" s="T321">PKZ_196X_SU0191.PKZ.071 (072)</ta>
            <ta e="T332" id="Seg_6524" s="T328">PKZ_196X_SU0191.PKZ.072 (073)</ta>
            <ta e="T334" id="Seg_6525" s="T332">PKZ_196X_SU0191.PKZ.073 (074)</ta>
            <ta e="T338" id="Seg_6526" s="T334">PKZ_196X_SU0191.PKZ.074 (075)</ta>
            <ta e="T340" id="Seg_6527" s="T338">PKZ_196X_SU0191.PKZ.075 (076)</ta>
            <ta e="T344" id="Seg_6528" s="T340">PKZ_196X_SU0191.PKZ.076 (077)</ta>
            <ta e="T347" id="Seg_6529" s="T344">PKZ_196X_SU0191.PKZ.077 (078)</ta>
            <ta e="T349" id="Seg_6530" s="T347">PKZ_196X_SU0191.PKZ.078 (079)</ta>
            <ta e="T355" id="Seg_6531" s="T349">PKZ_196X_SU0191.PKZ.079 (080)</ta>
            <ta e="T357" id="Seg_6532" s="T355">PKZ_196X_SU0191.PKZ.080 (081)</ta>
            <ta e="T362" id="Seg_6533" s="T357">PKZ_196X_SU0191.PKZ.081 (082)</ta>
            <ta e="T367" id="Seg_6534" s="T362">PKZ_196X_SU0191.PKZ.082 (083)</ta>
            <ta e="T370" id="Seg_6535" s="T367">PKZ_196X_SU0191.PKZ.083 (084)</ta>
            <ta e="T377" id="Seg_6536" s="T370">PKZ_196X_SU0191.PKZ.084 (085)</ta>
            <ta e="T379" id="Seg_6537" s="T377">PKZ_196X_SU0191.PKZ.085 (086)</ta>
            <ta e="T385" id="Seg_6538" s="T379">PKZ_196X_SU0191.PKZ.086 (087)</ta>
            <ta e="T388" id="Seg_6539" s="T385">PKZ_196X_SU0191.PKZ.087 (088)</ta>
            <ta e="T394" id="Seg_6540" s="T388">PKZ_196X_SU0191.PKZ.088 (089)</ta>
            <ta e="T400" id="Seg_6541" s="T394">PKZ_196X_SU0191.PKZ.089 (090)</ta>
            <ta e="T403" id="Seg_6542" s="T400">PKZ_196X_SU0191.PKZ.090 (091)</ta>
            <ta e="T409" id="Seg_6543" s="T403">PKZ_196X_SU0191.PKZ.091 (092)</ta>
            <ta e="T412" id="Seg_6544" s="T409">PKZ_196X_SU0191.PKZ.092 (093)</ta>
            <ta e="T414" id="Seg_6545" s="T412">PKZ_196X_SU0191.PKZ.093 (094)</ta>
            <ta e="T427" id="Seg_6546" s="T414">PKZ_196X_SU0191.PKZ.094 (095)</ta>
            <ta e="T433" id="Seg_6547" s="T427">PKZ_196X_SU0191.PKZ.095 (096)</ta>
            <ta e="T437" id="Seg_6548" s="T433">PKZ_196X_SU0191.PKZ.096 (097)</ta>
            <ta e="T439" id="Seg_6549" s="T437">PKZ_196X_SU0191.PKZ.097 (098)</ta>
            <ta e="T442" id="Seg_6550" s="T439">PKZ_196X_SU0191.PKZ.098 (099)</ta>
            <ta e="T448" id="Seg_6551" s="T442">PKZ_196X_SU0191.PKZ.099 (100)</ta>
            <ta e="T453" id="Seg_6552" s="T448">PKZ_196X_SU0191.PKZ.100 (101)</ta>
            <ta e="T456" id="Seg_6553" s="T453">PKZ_196X_SU0191.PKZ.101 (102)</ta>
            <ta e="T458" id="Seg_6554" s="T456">PKZ_196X_SU0191.PKZ.102 (103)</ta>
            <ta e="T462" id="Seg_6555" s="T458">PKZ_196X_SU0191.PKZ.103 (104)</ta>
            <ta e="T464" id="Seg_6556" s="T462">PKZ_196X_SU0191.PKZ.104 (105)</ta>
            <ta e="T475" id="Seg_6557" s="T464">PKZ_196X_SU0191.PKZ.105 (106)</ta>
            <ta e="T478" id="Seg_6558" s="T475">PKZ_196X_SU0191.PKZ.106 (107)</ta>
            <ta e="T487" id="Seg_6559" s="T478">PKZ_196X_SU0191.PKZ.107 (108)</ta>
            <ta e="T489" id="Seg_6560" s="T487">PKZ_196X_SU0191.PKZ.108 (109)</ta>
            <ta e="T498" id="Seg_6561" s="T489">PKZ_196X_SU0191.PKZ.109 (110)</ta>
            <ta e="T501" id="Seg_6562" s="T498">PKZ_196X_SU0191.PKZ.110 (111)</ta>
            <ta e="T508" id="Seg_6563" s="T501">PKZ_196X_SU0191.PKZ.111 (112)</ta>
            <ta e="T511" id="Seg_6564" s="T508">PKZ_196X_SU0191.PKZ.112 (113)</ta>
            <ta e="T514" id="Seg_6565" s="T511">PKZ_196X_SU0191.PKZ.113 (114)</ta>
            <ta e="T519" id="Seg_6566" s="T514">PKZ_196X_SU0191.PKZ.114 (115)</ta>
            <ta e="T524" id="Seg_6567" s="T519">PKZ_196X_SU0191.PKZ.115 (116)</ta>
            <ta e="T533" id="Seg_6568" s="T524">PKZ_196X_SU0191.PKZ.116 (117)</ta>
            <ta e="T542" id="Seg_6569" s="T533">PKZ_196X_SU0191.PKZ.117 (118)</ta>
            <ta e="T552" id="Seg_6570" s="T542">PKZ_196X_SU0191.PKZ.118 (119)</ta>
            <ta e="T559" id="Seg_6571" s="T552">PKZ_196X_SU0191.PKZ.119 (120)</ta>
            <ta e="T565" id="Seg_6572" s="T559">PKZ_196X_SU0191.PKZ.120 (121)</ta>
            <ta e="T574" id="Seg_6573" s="T565">PKZ_196X_SU0191.PKZ.121 (122)</ta>
            <ta e="T578" id="Seg_6574" s="T574">PKZ_196X_SU0191.PKZ.122 (123)</ta>
            <ta e="T585" id="Seg_6575" s="T578">PKZ_196X_SU0191.PKZ.123 (124)</ta>
            <ta e="T589" id="Seg_6576" s="T585">PKZ_196X_SU0191.PKZ.124 (125)</ta>
            <ta e="T594" id="Seg_6577" s="T589">PKZ_196X_SU0191.PKZ.125 (126)</ta>
            <ta e="T600" id="Seg_6578" s="T594">PKZ_196X_SU0191.PKZ.126 (127)</ta>
            <ta e="T604" id="Seg_6579" s="T600">PKZ_196X_SU0191.PKZ.127 (128)</ta>
            <ta e="T607" id="Seg_6580" s="T604">PKZ_196X_SU0191.PKZ.128 (129)</ta>
            <ta e="T612" id="Seg_6581" s="T607">PKZ_196X_SU0191.PKZ.129 (130)</ta>
            <ta e="T613" id="Seg_6582" s="T612">PKZ_196X_SU0191.PKZ.130 (131)</ta>
            <ta e="T616" id="Seg_6583" s="T613">PKZ_196X_SU0191.PKZ.131 (132)</ta>
            <ta e="T619" id="Seg_6584" s="T616">PKZ_196X_SU0191.PKZ.132 (133)</ta>
            <ta e="T623" id="Seg_6585" s="T619">PKZ_196X_SU0191.PKZ.133 (134)</ta>
            <ta e="T626" id="Seg_6586" s="T623">PKZ_196X_SU0191.PKZ.134 (135)</ta>
            <ta e="T629" id="Seg_6587" s="T626">PKZ_196X_SU0191.PKZ.135 (136)</ta>
            <ta e="T633" id="Seg_6588" s="T629">PKZ_196X_SU0191.PKZ.136 (137)</ta>
            <ta e="T638" id="Seg_6589" s="T633">PKZ_196X_SU0191.PKZ.137 (138)</ta>
            <ta e="T641" id="Seg_6590" s="T638">PKZ_196X_SU0191.PKZ.138 (139)</ta>
            <ta e="T644" id="Seg_6591" s="T641">PKZ_196X_SU0191.PKZ.139 (140)</ta>
            <ta e="T649" id="Seg_6592" s="T644">PKZ_196X_SU0191.PKZ.140 (141)</ta>
            <ta e="T659" id="Seg_6593" s="T649">PKZ_196X_SU0191.PKZ.141 (142)</ta>
            <ta e="T662" id="Seg_6594" s="T659">PKZ_196X_SU0191.PKZ.142 (143.001)</ta>
            <ta e="T664" id="Seg_6595" s="T662">PKZ_196X_SU0191.PKZ.143 (143.002)</ta>
            <ta e="T671" id="Seg_6596" s="T664">PKZ_196X_SU0191.PKZ.144 (144)</ta>
            <ta e="T673" id="Seg_6597" s="T671">PKZ_196X_SU0191.PKZ.145 (145)</ta>
            <ta e="T676" id="Seg_6598" s="T673">PKZ_196X_SU0191.PKZ.146 (146)</ta>
            <ta e="T678" id="Seg_6599" s="T676">PKZ_196X_SU0191.PKZ.147 (147)</ta>
            <ta e="T684" id="Seg_6600" s="T678">PKZ_196X_SU0191.PKZ.148 (148)</ta>
            <ta e="T686" id="Seg_6601" s="T684">PKZ_196X_SU0191.PKZ.149 (149)</ta>
            <ta e="T691" id="Seg_6602" s="T686">PKZ_196X_SU0191.PKZ.150 (150)</ta>
            <ta e="T693" id="Seg_6603" s="T691">PKZ_196X_SU0191.PKZ.151 (151)</ta>
            <ta e="T695" id="Seg_6604" s="T693">PKZ_196X_SU0191.PKZ.152 (152)</ta>
            <ta e="T702" id="Seg_6605" s="T695">PKZ_196X_SU0191.PKZ.153 (153)</ta>
            <ta e="T704" id="Seg_6606" s="T702">PKZ_196X_SU0191.PKZ.154 (154)</ta>
            <ta e="T709" id="Seg_6607" s="T704">PKZ_196X_SU0191.PKZ.155 (155)</ta>
            <ta e="T715" id="Seg_6608" s="T709">PKZ_196X_SU0191.PKZ.156 (156)</ta>
            <ta e="T717" id="Seg_6609" s="T715">PKZ_196X_SU0191.PKZ.157 (157)</ta>
            <ta e="T720" id="Seg_6610" s="T717">PKZ_196X_SU0191.PKZ.158 (158)</ta>
            <ta e="T722" id="Seg_6611" s="T720">PKZ_196X_SU0191.PKZ.159 (159)</ta>
            <ta e="T724" id="Seg_6612" s="T722">PKZ_196X_SU0191.PKZ.160 (160)</ta>
            <ta e="T727" id="Seg_6613" s="T724">PKZ_196X_SU0191.PKZ.161 (161)</ta>
            <ta e="T732" id="Seg_6614" s="T727">PKZ_196X_SU0191.PKZ.162 (162)</ta>
            <ta e="T736" id="Seg_6615" s="T732">PKZ_196X_SU0191.PKZ.163 (163)</ta>
            <ta e="T738" id="Seg_6616" s="T736">PKZ_196X_SU0191.PKZ.164 (164)</ta>
            <ta e="T742" id="Seg_6617" s="T738">PKZ_196X_SU0191.PKZ.165 (165)</ta>
            <ta e="T745" id="Seg_6618" s="T742">PKZ_196X_SU0191.PKZ.166 (166)</ta>
            <ta e="T751" id="Seg_6619" s="T745">PKZ_196X_SU0191.PKZ.167 (167)</ta>
            <ta e="T761" id="Seg_6620" s="T751">PKZ_196X_SU0191.PKZ.168 (168)</ta>
            <ta e="T765" id="Seg_6621" s="T761">PKZ_196X_SU0191.PKZ.169 (169)</ta>
            <ta e="T767" id="Seg_6622" s="T765">PKZ_196X_SU0191.PKZ.170 (170)</ta>
            <ta e="T772" id="Seg_6623" s="T767">PKZ_196X_SU0191.PKZ.171 (171)</ta>
            <ta e="T776" id="Seg_6624" s="T772">PKZ_196X_SU0191.PKZ.172 (172)</ta>
            <ta e="T782" id="Seg_6625" s="T776">PKZ_196X_SU0191.PKZ.173 (173)</ta>
            <ta e="T784" id="Seg_6626" s="T782">PKZ_196X_SU0191.PKZ.174 (174)</ta>
            <ta e="T788" id="Seg_6627" s="T784">PKZ_196X_SU0191.PKZ.175 (175)</ta>
            <ta e="T791" id="Seg_6628" s="T788">PKZ_196X_SU0191.PKZ.176 (176)</ta>
            <ta e="T796" id="Seg_6629" s="T791">PKZ_196X_SU0191.PKZ.177 (177)</ta>
            <ta e="T801" id="Seg_6630" s="T796">PKZ_196X_SU0191.PKZ.178 (178)</ta>
            <ta e="T804" id="Seg_6631" s="T801">PKZ_196X_SU0191.PKZ.179 (179)</ta>
            <ta e="T808" id="Seg_6632" s="T804">PKZ_196X_SU0191.PKZ.180 (180)</ta>
            <ta e="T812" id="Seg_6633" s="T808">PKZ_196X_SU0191.PKZ.181 (181)</ta>
            <ta e="T819" id="Seg_6634" s="T812">PKZ_196X_SU0191.PKZ.182 (182)</ta>
            <ta e="T822" id="Seg_6635" s="T819">PKZ_196X_SU0191.PKZ.183 (183)</ta>
            <ta e="T826" id="Seg_6636" s="T822">PKZ_196X_SU0191.PKZ.184 (184)</ta>
            <ta e="T828" id="Seg_6637" s="T826">PKZ_196X_SU0191.PKZ.185 (185)</ta>
            <ta e="T832" id="Seg_6638" s="T828">PKZ_196X_SU0191.PKZ.186 (186)</ta>
            <ta e="T835" id="Seg_6639" s="T832">PKZ_196X_SU0191.PKZ.187 (187)</ta>
            <ta e="T840" id="Seg_6640" s="T835">PKZ_196X_SU0191.PKZ.188 (188)</ta>
            <ta e="T842" id="Seg_6641" s="T840">PKZ_196X_SU0191.PKZ.189 (189)</ta>
            <ta e="T847" id="Seg_6642" s="T842">PKZ_196X_SU0191.PKZ.190 (190)</ta>
            <ta e="T850" id="Seg_6643" s="T847">PKZ_196X_SU0191.PKZ.191 (191)</ta>
            <ta e="T851" id="Seg_6644" s="T850">PKZ_196X_SU0191.PKZ.192 (192)</ta>
            <ta e="T855" id="Seg_6645" s="T851">PKZ_196X_SU0191.PKZ.193 (193)</ta>
            <ta e="T859" id="Seg_6646" s="T855">PKZ_196X_SU0191.PKZ.194 (194)</ta>
            <ta e="T861" id="Seg_6647" s="T859">PKZ_196X_SU0191.PKZ.195 (195)</ta>
            <ta e="T864" id="Seg_6648" s="T861">PKZ_196X_SU0191.PKZ.196 (196)</ta>
            <ta e="T867" id="Seg_6649" s="T864">PKZ_196X_SU0191.PKZ.197 (197)</ta>
            <ta e="T870" id="Seg_6650" s="T867">PKZ_196X_SU0191.PKZ.198 (198)</ta>
            <ta e="T873" id="Seg_6651" s="T870">PKZ_196X_SU0191.PKZ.199 (199)</ta>
            <ta e="T876" id="Seg_6652" s="T873">PKZ_196X_SU0191.PKZ.200 (200)</ta>
            <ta e="T879" id="Seg_6653" s="T876">PKZ_196X_SU0191.PKZ.201 (201)</ta>
            <ta e="T881" id="Seg_6654" s="T879">PKZ_196X_SU0191.PKZ.202 (202)</ta>
            <ta e="T884" id="Seg_6655" s="T881">PKZ_196X_SU0191.PKZ.203 (203)</ta>
            <ta e="T893" id="Seg_6656" s="T884">PKZ_196X_SU0191.PKZ.204 (204)</ta>
            <ta e="T898" id="Seg_6657" s="T893">PKZ_196X_SU0191.PKZ.205 (205)</ta>
            <ta e="T904" id="Seg_6658" s="T898">PKZ_196X_SU0191.PKZ.206 (206) </ta>
            <ta e="T911" id="Seg_6659" s="T904">PKZ_196X_SU0191.PKZ.207 (208)</ta>
            <ta e="T914" id="Seg_6660" s="T911">PKZ_196X_SU0191.PKZ.208 (209)</ta>
            <ta e="T917" id="Seg_6661" s="T914">PKZ_196X_SU0191.PKZ.209 (210)</ta>
            <ta e="T921" id="Seg_6662" s="T917">PKZ_196X_SU0191.PKZ.210 (211)</ta>
            <ta e="T923" id="Seg_6663" s="T921">PKZ_196X_SU0191.PKZ.211 (212)</ta>
            <ta e="T925" id="Seg_6664" s="T923">PKZ_196X_SU0191.PKZ.212 (213)</ta>
            <ta e="T936" id="Seg_6665" s="T925">PKZ_196X_SU0191.PKZ.213 (214)</ta>
            <ta e="T940" id="Seg_6666" s="T936">PKZ_196X_SU0191.PKZ.214 (215)</ta>
            <ta e="T948" id="Seg_6667" s="T940">PKZ_196X_SU0191.PKZ.215 (216)</ta>
            <ta e="T958" id="Seg_6668" s="T948">PKZ_196X_SU0191.PKZ.216 (217)</ta>
            <ta e="T965" id="Seg_6669" s="T958">PKZ_196X_SU0191.PKZ.217 (218)</ta>
            <ta e="T968" id="Seg_6670" s="T965">PKZ_196X_SU0191.PKZ.218 (219)</ta>
            <ta e="T976" id="Seg_6671" s="T968">PKZ_196X_SU0191.PKZ.219 (220)</ta>
            <ta e="T984" id="Seg_6672" s="T976">PKZ_196X_SU0191.PKZ.220 (221)</ta>
            <ta e="T986" id="Seg_6673" s="T984">PKZ_196X_SU0191.PKZ.221 (222)</ta>
            <ta e="T988" id="Seg_6674" s="T986">PKZ_196X_SU0191.PKZ.222 (223)</ta>
            <ta e="T992" id="Seg_6675" s="T988">PKZ_196X_SU0191.PKZ.223 (224)</ta>
            <ta e="T997" id="Seg_6676" s="T992">PKZ_196X_SU0191.PKZ.224 (225)</ta>
            <ta e="T1001" id="Seg_6677" s="T997">PKZ_196X_SU0191.PKZ.225 (226)</ta>
            <ta e="T1004" id="Seg_6678" s="T1001">PKZ_196X_SU0191.PKZ.226 (227)</ta>
            <ta e="T1006" id="Seg_6679" s="T1004">PKZ_196X_SU0191.PKZ.227 (228)</ta>
            <ta e="T1011" id="Seg_6680" s="T1006">PKZ_196X_SU0191.PKZ.228 (229)</ta>
            <ta e="T1016" id="Seg_6681" s="T1011">PKZ_196X_SU0191.PKZ.229 (230) </ta>
            <ta e="T1018" id="Seg_6682" s="T1016">PKZ_196X_SU0191.PKZ.230 (232)</ta>
            <ta e="T1022" id="Seg_6683" s="T1018">PKZ_196X_SU0191.PKZ.231 (233)</ta>
            <ta e="T1023" id="Seg_6684" s="T1022">PKZ_196X_SU0191.PKZ.232 (234)</ta>
            <ta e="T1028" id="Seg_6685" s="T1023">PKZ_196X_SU0191.PKZ.233 (235)</ta>
            <ta e="T1030" id="Seg_6686" s="T1028">PKZ_196X_SU0191.PKZ.234 (236)</ta>
            <ta e="T1032" id="Seg_6687" s="T1030">PKZ_196X_SU0191.PKZ.235 (237)</ta>
            <ta e="T1037" id="Seg_6688" s="T1032">PKZ_196X_SU0191.PKZ.236 (238)</ta>
            <ta e="T1040" id="Seg_6689" s="T1037">PKZ_196X_SU0191.PKZ.237 (239)</ta>
            <ta e="T1041" id="Seg_6690" s="T1040">PKZ_196X_SU0191.PKZ.238 (240)</ta>
            <ta e="T1043" id="Seg_6691" s="T1041">PKZ_196X_SU0191.PKZ.239 (241)</ta>
            <ta e="T1045" id="Seg_6692" s="T1043">PKZ_196X_SU0191.PKZ.240 (242)</ta>
            <ta e="T1048" id="Seg_6693" s="T1045">PKZ_196X_SU0191.PKZ.241 (243)</ta>
            <ta e="T1051" id="Seg_6694" s="T1048">PKZ_196X_SU0191.PKZ.242 (244)</ta>
            <ta e="T1053" id="Seg_6695" s="T1051">PKZ_196X_SU0191.PKZ.243 (245)</ta>
            <ta e="T1057" id="Seg_6696" s="T1053">PKZ_196X_SU0191.PKZ.244 (246)</ta>
            <ta e="T1059" id="Seg_6697" s="T1057">PKZ_196X_SU0191.PKZ.245 (247)</ta>
            <ta e="T1061" id="Seg_6698" s="T1059">PKZ_196X_SU0191.PKZ.246 (248)</ta>
            <ta e="T1063" id="Seg_6699" s="T1061">PKZ_196X_SU0191.PKZ.247 (249)</ta>
            <ta e="T1064" id="Seg_6700" s="T1063">PKZ_196X_SU0191.PKZ.248 (250)</ta>
            <ta e="T1067" id="Seg_6701" s="T1064">PKZ_196X_SU0191.PKZ.249 (251)</ta>
            <ta e="T1070" id="Seg_6702" s="T1067">PKZ_196X_SU0191.PKZ.250 (252)</ta>
            <ta e="T1072" id="Seg_6703" s="T1070">PKZ_196X_SU0191.PKZ.251 (253)</ta>
            <ta e="T1075" id="Seg_6704" s="T1072">PKZ_196X_SU0191.PKZ.252 (254)</ta>
            <ta e="T1078" id="Seg_6705" s="T1075">PKZ_196X_SU0191.PKZ.253 (255)</ta>
            <ta e="T1081" id="Seg_6706" s="T1078">PKZ_196X_SU0191.PKZ.254 (256)</ta>
            <ta e="T1086" id="Seg_6707" s="T1081">PKZ_196X_SU0191.PKZ.255 (257)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PKZ">
            <ta e="T3" id="Seg_6708" s="T0">Ты уж отпустил?</ta>
            <ta e="T10" id="Seg_6709" s="T4">Amnobi büzʼe, dĭn nagur nʼi ibi. </ta>
            <ta e="T17" id="Seg_6710" s="T10">Onʼiʔ Gavrila, onʼiʔ Danʼila, onʼiʔ Vanʼuška duračok. </ta>
            <ta e="T23" id="Seg_6711" s="T17">Dĭgəttə (dĭz-) dĭ büzʼe kuʔlaʔpi budəj. </ta>
            <ta e="T33" id="Seg_6712" s="T23">I dĭ šindidə (ka- n-) nüdʼin šoluʔjəʔ da, amnaʔbə budəj. </ta>
            <ta e="T41" id="Seg_6713" s="T33">"Nada, măndə, kanzittə (mandər-) mandəsʼtə, šində amnʼiat budəj. </ta>
            <ta e="T43" id="Seg_6714" s="T41">Nada dʼabəsʼtə". </ta>
            <ta e="T49" id="Seg_6715" s="T43">Dĭgəttə kambi staršij nʼit, Gavrila, dibər. </ta>
            <ta e="T55" id="Seg_6716" s="T49">Kambi, kunolbi noʔgən, i šobi maʔnə. </ta>
            <ta e="T62" id="Seg_6717" s="T55">"Amnobiam, ej kunolbiam, a šindidə ej kubiam." </ta>
            <ta e="T67" id="Seg_6718" s="T62">Dĭgəttə baška nʼit kandəga, Danʼilat. </ta>
            <ta e="T70" id="Seg_6719" s="T67">Kambi, kunolbi noʔgən. </ta>
            <ta e="T73" id="Seg_6720" s="T70">Dĭgəttə ertən šobi. </ta>
            <ta e="T81" id="Seg_6721" s="T73">"Bar nudʼi ej kunolbiam, a šinimdə ej kubiom." </ta>
            <ta e="T84" id="Seg_6722" s="T81">Dĭgəttə Vanʼuška kambi. </ta>
            <ta e="T86" id="Seg_6723" s="T84">Ibi arkandə. </ta>
            <ta e="T89" id="Seg_6724" s="T86">Dĭgəttə amnobi pinə. </ta>
            <ta e="T92" id="Seg_6725" s="T89">Ej kunolia, amnolaʔbə. </ta>
            <ta e="T95" id="Seg_6726" s="T92">Kuliat: ine šobi. </ta>
            <ta e="T102" id="Seg_6727" s="T95">Vsʼaka i bar dĭn kubangən tordə kuvas. </ta>
            <ta e="T109" id="Seg_6728" s="T102">Dĭ bar baʔluʔpi arkandə dĭʔnə i dʼabəluʔpi. </ta>
            <ta e="T114" id="Seg_6729" s="T109">Dĭ suʔmiluʔpi, ej molia suʔmisʼtə. </ta>
            <ta e="T117" id="Seg_6730" s="T114">Dĭgəttə:" Öʔləʔ măna! </ta>
            <ta e="T122" id="Seg_6731" s="T117">Măn tănan bar ĭmbi mĭləm." </ta>
            <ta e="T129" id="Seg_6732" s="T122">"No, kădə, nörbəʔ, daška ej amnial budəj?" </ta>
            <ta e="T131" id="Seg_6733" s="T129">"Ej amnam!" </ta>
            <ta e="T136" id="Seg_6734" s="T131">"A kădə šoləl dărə măna?"</ta>
            <ta e="T141" id="Seg_6735" s="T136">Šoʔ döbər, (ənuʔ) (măn-) măna. </ta>
            <ta e="T143" id="Seg_6736" s="T141">No, dĭrgit. </ta>
            <ta e="T151" id="Seg_6737" s="T143">I kalla dʼürbi inet, a dĭ šobi maːndə. </ta>
            <ta e="T153" id="Seg_6738" s="T151">Pʼešdə (šerluʔpi). </ta>
            <ta e="T157" id="Seg_6739" s="T153">"Măn dʼabəbiam ine dĭn. </ta>
            <ta e="T163" id="Seg_6740" s="T157">Tuj kaməndə ej šoləj amzittə budəj". </ta>
            <ta e="T169" id="Seg_6741" s="T163">Dĭgəttə koŋ abi štobɨ il šobiʔi. </ta>
            <ta e="T174" id="Seg_6742" s="T169">(Dĭ-) Dĭn koʔbdo amnolaʔbə tʼerebgən. </ta>
            <ta e="T177" id="Seg_6743" s="T174">Bar il kallaʔbəʔjə. </ta>
            <ta e="T182" id="Seg_6744" s="T177">(I=) I dĭn kagaʔi kandəgaʔi. </ta>
            <ta e="T187" id="Seg_6745" s="T182">A dĭ măndə: igəʔ măna! </ta>
            <ta e="T190" id="Seg_6746" s="T187">Gibər tăn kallal? </ta>
            <ta e="T199" id="Seg_6747" s="T190">Ĭmbidə dĭn ej kubial, štobɨ il püšterbiʔi (tăn-) tănan. </ta>
            <ta e="T204" id="Seg_6748" s="T199">Dĭzeŋ kalla dʼürbiʔi, dĭ uʔbdəbi. </ta>
            <ta e="T207" id="Seg_6749" s="T204">Karzʼina ibi, kambi. </ta>
            <ta e="T212" id="Seg_6750" s="T207">I davaj kirgarzittə, inet šobi. </ta>
            <ta e="T216" id="Seg_6751" s="T212">Bar döbər (modriagən) šonugaʔi. </ta>
            <ta e="T219" id="Seg_6752" s="T216">Šö kuzandə šonuga. </ta>
            <ta e="T224" id="Seg_6753" s="T219">Dĭgəttə dĭ ine măndə … </ta>
            <ta e="T226" id="Seg_6754" s="T224">Nubi dĭʔnə. </ta>
            <ta e="T232" id="Seg_6755" s="T226">Măndə: onʼiʔ kunə paʔkaʔ, onʼiʔgəʔ supsoʔ. </ta>
            <ta e="T235" id="Seg_6756" s="T232">Dĭ paʔpi, supsobi. </ta>
            <ta e="T237" id="Seg_6757" s="T235">Kuvas molaːmbi. </ta>
            <ta e="T240" id="Seg_6758" s="T237">Dĭgəttə dĭʔnə amnobi. </ta>
            <ta e="T243" id="Seg_6759" s="T240">Dĭ (nuʔmə=) nuʔməluʔpi. </ta>
            <ta e="T254" id="Seg_6760" s="T243">Dibər il iʔgö nugaʔi, a šindidə ej, dĭ koʔbdonə ej sʼaliaʔi. </ta>
            <ta e="T257" id="Seg_6761" s="T254">Dĭ bar suʔmiluʔpi. </ta>
            <ta e="T261" id="Seg_6762" s="T257">Šalaʔjə, ej, ej dʼapi. </ta>
            <ta e="T267" id="Seg_6763" s="T261">Dĭgəttə il bar kirgarlaʔbəʔjə:" Dʼabəʔ, dʼabəʔ". </ta>
            <ta e="T270" id="Seg_6764" s="T267">A dĭ nuʔməluʔpi. </ta>
            <ta e="T272" id="Seg_6765" s="T270">Inem öʔlubi. </ta>
            <ta e="T280" id="Seg_6766" s="T272">Dĭgəttə beškeʔi oʔbdəbi i vsʼaka (iʔgö) beške deppi. </ta>
            <ta e="T283" id="Seg_6767" s="T280">A nezeŋ kudolaʔbəʔjə. </ta>
            <ta e="T287" id="Seg_6768" s="T283">"Ĭmbi dĭrgit (deʔ-) deʔpiel. </ta>
            <ta e="T290" id="Seg_6769" s="T287">Tolʼko tănan amzittə." </ta>
            <ta e="T295" id="Seg_6770" s="T290">Dĭ pʼešdə (š-) sʼabi, iʔbölaʔbə. </ta>
            <ta e="T304" id="Seg_6771" s="T295">Dĭgəttə (kazak-) kagazaŋdə šobiʔi bar, nörbəleʔbəʔjə (dĭrgi-) kăda ibi. </ta>
            <ta e="T307" id="Seg_6772" s="T304">A dĭ iʔbölaʔbə. </ta>
            <ta e="T312" id="Seg_6773" s="T307">"Dĭ măn ulum dĭn ibi". </ta>
            <ta e="T314" id="Seg_6774" s="T312">"Iʔ šamaʔ! </ta>
            <ta e="T316" id="Seg_6775" s="T314">Tăn ulum. </ta>
            <ta e="T318" id="Seg_6776" s="T316">Amnoʔ už. </ta>
            <ta e="T321" id="Seg_6777" s="T318">Ĭmbidə ej dʼăbaktəraʔ. </ta>
            <ta e="T328" id="Seg_6778" s="T321">Dĭrgit kuza ibi, inet kuvas ibi bostə". </ta>
            <ta e="T332" id="Seg_6779" s="T328">Dĭgəttə karəldʼan bazoʔ kandəgaʔi. </ta>
            <ta e="T334" id="Seg_6780" s="T332">Kalla dʼürbiʔi. </ta>
            <ta e="T338" id="Seg_6781" s="T334">Dĭ pʼešgəʔ (š-) suʔmiluʔpi. </ta>
            <ta e="T340" id="Seg_6782" s="T338">Karzʼina ibi. </ta>
            <ta e="T344" id="Seg_6783" s="T340">Kambi, kambi, baruʔpi. </ta>
            <ta e="T347" id="Seg_6784" s="T344">Davaj inem kirgarzittə. </ta>
            <ta e="T349" id="Seg_6785" s="T347">Inet šobi. </ta>
            <ta e="T355" id="Seg_6786" s="T349">Bazoʔ onʼiʔ kuʔtə (paʔlaːmbi), onʼiʔ supsolaːmbi. </ta>
            <ta e="T357" id="Seg_6787" s="T355">Dĭgəttə amnəbi. </ta>
            <ta e="T362" id="Seg_6788" s="T357">(Nuʔm-) Šonuga il, iʔgö nugaʔi. </ta>
            <ta e="T367" id="Seg_6789" s="T362">Dĭgəttə šindidə ej nuʔməlie dibər. </ta>
            <ta e="T370" id="Seg_6790" s="T367">Dibər (š-) suʔmiluʔpi. </ta>
            <ta e="T377" id="Seg_6791" s="T370">Ibi (e- i ej nʼi-) ej mobi. </ta>
            <ta e="T379" id="Seg_6792" s="T377">Dĭgəttə parluʔpi. </ta>
            <ta e="T385" id="Seg_6793" s="T379">Kagazaŋdə bar münörbi i kalla dʼürbi. </ta>
            <ta e="T388" id="Seg_6794" s="T385">A il kirgarlaʔbəʔjə. </ta>
            <ta e="T394" id="Seg_6795" s="T388">(Dʼabəʔ), (dʼabəʔ), da dĭ iʔ məluʔpi. </ta>
            <ta e="T400" id="Seg_6796" s="T394">Inebə öʔluʔpi, a bostə bazoʔ šobi. </ta>
            <ta e="T403" id="Seg_6797" s="T400">Nezeŋdə bazoʔ kudolaʔbəʔjə. </ta>
            <ta e="T409" id="Seg_6798" s="T403">A dĭ pʼešdə sʼabi da iʔbölaʔbə. </ta>
            <ta e="T412" id="Seg_6799" s="T409">(Kaz-) Kazaŋdə šobiʔi. </ta>
            <ta e="T414" id="Seg_6800" s="T412">Abandə nörbəlieʔi. </ta>
            <ta e="T427" id="Seg_6801" s="T414">"Girgit kuza ibi, inet ugaːndə kuvas i (bolʼš-) bostə kuvas", a dĭ iʔbölaʔbə. </ta>
            <ta e="T433" id="Seg_6802" s="T427">"Ej măn li ulum dĭn ibi". </ta>
            <ta e="T437" id="Seg_6803" s="T433">"Iʔ šamaʔ, iʔbəʔ už! </ta>
            <ta e="T439" id="Seg_6804" s="T437">Sagəššət nʼi! </ta>
            <ta e="T442" id="Seg_6805" s="T439">Tăn dĭn ibiel". </ta>
            <ta e="T448" id="Seg_6806" s="T442">Dĭgəttə bazoʔ, nagur dʼala, bazoʔ kandəgaʔi. </ta>
            <ta e="T453" id="Seg_6807" s="T448">Kalla dʼürbiʔi, dĭ (s-) uʔbdəbi. </ta>
            <ta e="T456" id="Seg_6808" s="T453">Karzʼinabə ibi, kambi. </ta>
            <ta e="T458" id="Seg_6809" s="T456">Beškeʔi oʔbšizittə. </ta>
            <ta e="T462" id="Seg_6810" s="T458">Kambi, davaj ine kirgarzittə. </ta>
            <ta e="T464" id="Seg_6811" s="T462">Ine šobi. </ta>
            <ta e="T475" id="Seg_6812" s="T464">Dĭ bazoʔ dăra (on-) onʼiʔ kugəndə paʔlaːmbi, onʼiʔ supsolaːmbi, amnobi, kambi. </ta>
            <ta e="T478" id="Seg_6813" s="T475">Dĭgəttə kak nuʔməluʔpi. </ta>
            <ta e="T487" id="Seg_6814" s="T478">Dĭm panarbi i kalʼečkabə iluʔpi, bostə udandə (se-) šerbi. </ta>
            <ta e="T489" id="Seg_6815" s="T487">Kalla dʼürbi. </ta>
            <ta e="T498" id="Seg_6816" s="T489">Inebə öʔlubi, bostə šobi maːndə i pʼešdə sʼabi, iʔbölaʔbə. </ta>
            <ta e="T501" id="Seg_6817" s="T498">Dĭgəttə kagazaŋdə šobiʔi. </ta>
            <ta e="T508" id="Seg_6818" s="T501">Nu, girgitdə (nʼi) nʼi kuvas, inet kuvas. </ta>
            <ta e="T511" id="Seg_6819" s="T508">Panarbi tsarskɨj koʔbdo. </ta>
            <ta e="T514" id="Seg_6820" s="T511">(Koŋ) Koŋ koʔbdo. </ta>
            <ta e="T519" id="Seg_6821" s="T514">I kalʼečkabə ibi, kalla dʼürbi. </ta>
            <ta e="T524" id="Seg_6822" s="T519">Dĭgəttə bazoʔ dĭ koŋ abi. </ta>
            <ta e="T533" id="Seg_6823" s="T524">Iʔgö uja munujʔ embi, bar ĭmbi dĭn ige embi. </ta>
            <ta e="T542" id="Seg_6824" s="T533">Dĭgəttə" Šogaʔ bargəʔ, šində ej šoləj, ulubə (š-) sajjaʔlim". </ta>
            <ta e="T552" id="Seg_6825" s="T542">Dĭgəttə dĭzeŋ (a-), dĭn abat, bargəʔ kalla dʼürbiʔi, šobiʔi dĭzeŋ. </ta>
            <ta e="T559" id="Seg_6826" s="T552">Amnəlbiʔi, (ba-) bar bĭdlaʔbəʔjə, (aja-) ara mĭleʔbəʔjə. </ta>
            <ta e="T565" id="Seg_6827" s="T559">Dĭgəttə šobi dĭ poslʼednij nʼit, dĭ. </ta>
            <ta e="T574" id="Seg_6828" s="T565">Dĭ bar sagər amnolaʔbə, i udat sagər, trʼapkazʼiʔ sarona. </ta>
            <ta e="T578" id="Seg_6829" s="T574">Măndə:" Ĭmbi tăn udandə?" </ta>
            <ta e="T585" id="Seg_6830" s="T578">(Kulʼ-) (Kuliat bar), bar dĭn kalʼečka amnolaʔbə. </ta>
            <ta e="T589" id="Seg_6831" s="T585">Dĭgəttə ilet, abandə šoliat. </ta>
            <ta e="T594" id="Seg_6832" s="T589">(Dö m-) Dö măn ženʼixbə. </ta>
            <ta e="T600" id="Seg_6833" s="T594">Dĭm (dĭ dĭ-) dĭzeŋ dĭn bəzəbiʔi. </ta>
            <ta e="T604" id="Seg_6834" s="T600">Kuvas oldʼa (s-) šerbiʔi. </ta>
            <ta e="T607" id="Seg_6835" s="T604">Ugaːndə kuvas molaːmbi. </ta>
            <ta e="T612" id="Seg_6836" s="T607">Dĭgəttə bazoʔ davaj ara bĭssittə. </ta>
            <ta e="T613" id="Seg_6837" s="T612">Sʼarzittə. </ta>
            <ta e="T616" id="Seg_6838" s="T613">Kagazaŋdə bar măndlaʔbəʔjə. ((DMG))</ta>
            <ta e="T619" id="Seg_6839" s="T616">Girgit kuvas molaːmbi. </ta>
            <ta e="T623" id="Seg_6840" s="T619">I dĭn măn ibiem. </ta>
            <ta e="T626" id="Seg_6841" s="T623">Ara bar mʼaŋŋaʔbə. </ta>
            <ta e="T629" id="Seg_6842" s="T626">Mĭbiʔi măna blin. </ta>
            <ta e="T633" id="Seg_6843" s="T629">Три года у лоханки ((LAUGH)) ((BRK)). </ta>
            <ta e="T638" id="Seg_6844" s="T633">Amnobiʔi šidegöʔ kum da kuma. </ta>
            <ta e="T641" id="Seg_6845" s="T638">Volk da lʼisa. </ta>
            <ta e="T644" id="Seg_6846" s="T641">Dĭgəttə kunolzittə iʔbəʔi. </ta>
            <ta e="T649" id="Seg_6847" s="T644">A lʼisa uge kuzurleʔbə, kuzurleʔbə. </ta>
            <ta e="T659" id="Seg_6848" s="T649">A volk măndə:” Šindidə kuzurleʔbə, măna kăštəliaʔi, (ešši) ešši pădəsʼtə. </ta>
            <ta e="T662" id="Seg_6849" s="T659">Dĭgəttə ” No, kanaʔ!” </ta>
            <ta e="T664" id="Seg_6850" s="T662">Dĭ kambi. </ta>
            <ta e="T671" id="Seg_6851" s="T664">A dĭzeŋ nulaʔbi, mʼod ambi, ambi mʼod. </ta>
            <ta e="T673" id="Seg_6852" s="T671">Dĭ šobi. </ta>
            <ta e="T676" id="Seg_6853" s="T673">Šindi dĭn ige? </ta>
            <ta e="T678" id="Seg_6854" s="T676">Veršok ige. </ta>
            <ta e="T684" id="Seg_6855" s="T678">Dĭgəttə bazoʔ nüdʼin, bazoʔ kuzurleʔbə lʼisʼitsa. </ta>
            <ta e="T686" id="Seg_6856" s="T684">"Šindi kuzurleʔbə?" </ta>
            <ta e="T691" id="Seg_6857" s="T686">"Da măna kăštəlaʔbəʔjə ešši (ködərzittə)". </ta>
            <ta e="T693" id="Seg_6858" s="T691">"No, kanaʔ!" </ta>
            <ta e="T695" id="Seg_6859" s="T693">Dĭ kambi. </ta>
            <ta e="T702" id="Seg_6860" s="T695">Ambi, ambi, dĭgəttə šobi:" Šindi dĭn šobi?" </ta>
            <ta e="T704" id="Seg_6861" s="T702">Sʼerʼodušok šobi. </ta>
            <ta e="T709" id="Seg_6862" s="T704">Dĭgəttə nagurgit nüdʼin bazoʔ kuzurleʔbə. </ta>
            <ta e="T715" id="Seg_6863" s="T709">Dĭgəttə:" Šindidə kuzurleʔbə, da măna kăštəlaʔbəʔjə". </ta>
            <ta e="T717" id="Seg_6864" s="T715">"No, kanaʔ!" </ta>
            <ta e="T720" id="Seg_6865" s="T717">(Dĭgəttə) dĭ ambi. </ta>
            <ta e="T722" id="Seg_6866" s="T720">Bar šobi. </ta>
            <ta e="T724" id="Seg_6867" s="T722">Šindi dĭn? </ta>
            <ta e="T727" id="Seg_6868" s="T724">Dĭ, skrʼobušok šobi. </ta>
            <ta e="T732" id="Seg_6869" s="T727">Dĭgəttə ertən uʔbdəbiʔi, dĭ ĭzemnuʔbi. </ta>
            <ta e="T736" id="Seg_6870" s="T732">Măndə:" Deʔ măna nʼamgaʔi!" </ta>
            <ta e="T738" id="Seg_6871" s="T736">Dĭ kambi. </ta>
            <ta e="T742" id="Seg_6872" s="T738">Da naga, šindidə amnuʔpi! </ta>
            <ta e="T745" id="Seg_6873" s="T742">"Da tăn ambial!" </ta>
            <ta e="T751" id="Seg_6874" s="T745">Dĭ bar:” Ej, măn ej ambiam!” </ta>
            <ta e="T761" id="Seg_6875" s="T751">"Dĭgəttə davaj kujanə iʔbəbeʔ, šində ambi, dĭn nʼamga nanat moləj". </ta>
            <ta e="T765" id="Seg_6876" s="T761">Dĭgəttə kuliat bostə nanəndə. </ta>
            <ta e="T767" id="Seg_6877" s="T765">Nʼamga ige. </ta>
            <ta e="T772" id="Seg_6878" s="T767">Dĭ davaj dĭ volktə tʼuʔsittə. </ta>
            <ta e="T776" id="Seg_6879" s="T772">"Uʔbdəʔ, tăn (š-) ambial!" </ta>
            <ta e="T782" id="Seg_6880" s="T776">Dĭgəttə dĭ uʔbdəbi:" No, măn ambiam". </ta>
            <ta e="T784" id="Seg_6881" s="T782">Dĭgəttə bar. </ta>
            <ta e="T788" id="Seg_6882" s="T784">Süjö i tʼetʼer (amnoʔjə). </ta>
            <ta e="T791" id="Seg_6883" s="T788">Kăda tura jaʔsittə. </ta>
            <ta e="T796" id="Seg_6884" s="T791">Baltu naga, šindi baltu (aləj). </ta>
            <ta e="T801" id="Seg_6885" s="T796">Da, na što măna (tăra-) tura. </ta>
            <ta e="T804" id="Seg_6886" s="T801">Măn sĭregən šalam. </ta>
            <ta e="T808" id="Seg_6887" s="T804">I saʔməluʔpi (šĭr-) sĭrenə. </ta>
            <ta e="T812" id="Seg_6888" s="T808">Dĭn šabi, ertən uʔbdəbi. </ta>
            <ta e="T819" id="Seg_6889" s="T812">I kambi kuzittə, elezeŋdə kubi, dĭn sʼarbiʔi. </ta>
            <ta e="T822" id="Seg_6890" s="T819">Suʔmiluʔpiʔi (paʔn-) paʔinə. </ta>
            <ta e="T826" id="Seg_6891" s="T822">Dĭgəttə boskəndə gnʼozdaʔi abiʔi. </ta>
            <ta e="T828" id="Seg_6892" s="T826">Munuʔi deʔpiʔi. </ta>
            <ta e="T832" id="Seg_6893" s="T828">Esseŋdə, mobiʔi dĭgəttə esseŋdə. </ta>
            <ta e="T835" id="Seg_6894" s="T832">Bădəbiʔi maškaziʔ, červəʔiziʔ. </ta>
            <ta e="T840" id="Seg_6895" s="T835">Dĭgəttə bazoʔ, na što miʔnʼibeʔ maʔ? </ta>
            <ta e="T842" id="Seg_6896" s="T840">Sĭregən kunolbibaʔ. </ta>
            <ta e="T847" id="Seg_6897" s="T842">Onʼiʔ nüdʼi makən amnolbaʔ (dĭ). </ta>
            <ta e="T850" id="Seg_6898" s="T847">Bar vʼezʼdʼe măndərlaʔbəbaʔ. </ta>
            <ta e="T851" id="Seg_6899" s="T850">Kabarləj. </ta>
            <ta e="T855" id="Seg_6900" s="T851">Amnobiʔi nükke da büzʼe. </ta>
            <ta e="T859" id="Seg_6901" s="T855">(Dĭ-) Dĭzeŋ ĭmbidə nagobi. </ta>
            <ta e="T861" id="Seg_6902" s="T859">Kambiʔi dʼijenə. </ta>
            <ta e="T864" id="Seg_6903" s="T861">Šiškaʔi žoludʼi oʔbdobiʔi. </ta>
            <ta e="T867" id="Seg_6904" s="T864">Šobiʔi, amnaʔbə bar. </ta>
            <ta e="T870" id="Seg_6905" s="T867">Onʼiʔ saʔməluʔpi patpolʼlʼagən. </ta>
            <ta e="T873" id="Seg_6906" s="T870">Dĭgəttə davaj özerzittə. </ta>
            <ta e="T876" id="Seg_6907" s="T873">(Nuge) Nükke măndə. </ta>
            <ta e="T879" id="Seg_6908" s="T876">Baltuzʼiʔ jaʔdə poldə. </ta>
            <ta e="T881" id="Seg_6909" s="T879">Dĭ jaʔpi. </ta>
            <ta e="T884" id="Seg_6910" s="T881">Dĭ döbər özerbi. </ta>
            <ta e="T893" id="Seg_6911" s="T884">Tuj dĭn nʼuʔdə bar (oʔbdo=), dĭ dibər krɨšabə snʼal. </ta>
            <ta e="T898" id="Seg_6912" s="T893">Dibər kuŋgeŋ özerbi daže nʼuʔdə. </ta>
            <ta e="T904" id="Seg_6913" s="T898">Dĭgəttə büzʼe: "Nada kanzittə oʔbdəsʼtə žoludʼi". </ta>
            <ta e="T911" id="Seg_6914" s="T904">Sʼabi, sʼabi (uj-), dibər nʼuʔdə dĭn kulaʔbə. </ta>
            <ta e="T914" id="Seg_6915" s="T911">Tura (i-) nuga. </ta>
            <ta e="T917" id="Seg_6916" s="T914">Dibər turanə šobi. </ta>
            <ta e="T921" id="Seg_6917" s="T917">Dĭn kurizən tibi amnolaʔbə. </ta>
            <ta e="T923" id="Seg_6918" s="T921">Ulut kuvas. </ta>
            <ta e="T925" id="Seg_6919" s="T923">Komu bar. </ta>
            <ta e="T936" id="Seg_6920" s="T925">I dĭn (žernovkazaŋdə) ige, dĭ ibi dĭm i šobi:" Na, tănan". </ta>
            <ta e="T940" id="Seg_6921" s="T936">Kurizən tibi, komu ulut. </ta>
            <ta e="T948" id="Seg_6922" s="T940">Dĭ ibi da davaj (žernovka=) dĭ tʼerməndə kurzittə. </ta>
            <ta e="T958" id="Seg_6923" s="T948">Dĭn blinʔi, pirogəʔi, blinʔi, pirogəʔi, büzʼe ambi i nükke ambi. </ta>
            <ta e="T965" id="Seg_6924" s="T958">Uge dăra amnolaʔ, dĭgəttə šobi dĭzeŋdə barin. </ta>
            <ta e="T968" id="Seg_6925" s="T965">"Deʔkeʔ măna amzittə". </ta>
            <ta e="T976" id="Seg_6926" s="T968">Dĭ nünəbi pravda dĭ (di- dĭ zər-) tʼermən. </ta>
            <ta e="T984" id="Seg_6927" s="T976">Dĭgəttə nüket bar davaj tʼerməndə kurzittə, blinʔi, pirogəʔi. </ta>
            <ta e="T986" id="Seg_6928" s="T984">Dĭ ambi. </ta>
            <ta e="T988" id="Seg_6929" s="T986">"Sadargaʔ măna". </ta>
            <ta e="T992" id="Seg_6930" s="T988">"Dʼok, miʔ ej sadarlam. </ta>
            <ta e="T997" id="Seg_6931" s="T992">A to (n-) miʔnʼibeʔ amzittə ĭmbi?" </ta>
            <ta e="T1001" id="Seg_6932" s="T997">Dĭgəttə dĭ nüdʼin šobi. </ta>
            <ta e="T1004" id="Seg_6933" s="T1001">I tojirlaʔ iluʔpi. </ta>
            <ta e="T1006" id="Seg_6934" s="T1004">I konnaːmbi. </ta>
            <ta e="T1011" id="Seg_6935" s="T1006">Ertən dĭzeŋ uʔbdəbiʔi: naga tʼerməndə. </ta>
            <ta e="T1016" id="Seg_6936" s="T1011">Dĭgəttə tibi kuriz: "Iʔ dʼorgaʔ! </ta>
            <ta e="T1018" id="Seg_6937" s="T1016">Măn kallam. </ta>
            <ta e="T1022" id="Seg_6938" s="T1018">(Di-) Detlim bazoʔ šiʔnʼileʔ". </ta>
            <ta e="T1023" id="Seg_6939" s="T1022">Kambi. </ta>
            <ta e="T1028" id="Seg_6940" s="T1023">Kandəga, kandəga, šonəga diʔnə lʼisʼitsa. </ta>
            <ta e="T1030" id="Seg_6941" s="T1028">"Gibər kandəgal? </ta>
            <ta e="T1032" id="Seg_6942" s="T1030">Iʔ măna! </ta>
            <ta e="T1037" id="Seg_6943" s="T1032">Bădaʔ măna šüjöndə", dĭ băʔpi. </ta>
            <ta e="T1040" id="Seg_6944" s="T1037">Dĭgəttə šonəga, šonəga. </ta>
            <ta e="T1041" id="Seg_6945" s="T1040">Volkdə. </ta>
            <ta e="T1043" id="Seg_6946" s="T1041">Gibər kandəgal? </ta>
            <ta e="T1045" id="Seg_6947" s="T1043">((NOISE)) Tʼerməndə izittə. </ta>
            <ta e="T1048" id="Seg_6948" s="T1045">(Mă-) Iʔ măna! </ta>
            <ta e="T1051" id="Seg_6949" s="T1048">Bădaʔ măna šüjöndə!" </ta>
            <ta e="T1053" id="Seg_6950" s="T1051">Dĭ băʔpi. </ta>
            <ta e="T1057" id="Seg_6951" s="T1053">Dĭgəttə šonəga, urgaːba šonəga. </ta>
            <ta e="T1059" id="Seg_6952" s="T1057">"Bădaʔ măna!" </ta>
            <ta e="T1061" id="Seg_6953" s="T1059">Dĭgəttə šobi. </ta>
            <ta e="T1063" id="Seg_6954" s="T1061">Davaj kirgarzittə. </ta>
            <ta e="T1064" id="Seg_6955" s="T1063">"Atdaj! </ta>
            <ta e="T1067" id="Seg_6956" s="T1064">Dettə măn tʼerməndə. </ta>
            <ta e="T1070" id="Seg_6957" s="T1067">Dettə măn tʼerməndə". </ta>
            <ta e="T1072" id="Seg_6958" s="T1070">Dĭ măndə. </ta>
            <ta e="T1075" id="Seg_6959" s="T1072">(Baʔluʔ-) Baʔgaʔ dĭm. </ta>
            <ta e="T1078" id="Seg_6960" s="T1075">A nabəʔinə pušaj. </ta>
            <ta e="T1081" id="Seg_6961" s="T1078">Nabəʔi amnuʔbə (ləʔi). </ta>
            <ta e="T1086" id="Seg_6962" s="T1081">Dĭm baʔluʔpi, a dĭ (măndə). </ta>
         </annotation>
         <annotation name="mb" tierref="mb-PKZ">
            <ta e="T5" id="Seg_6963" s="T4">amno-bi</ta>
            <ta e="T6" id="Seg_6964" s="T5">büzʼe</ta>
            <ta e="T7" id="Seg_6965" s="T6">dĭ-n</ta>
            <ta e="T8" id="Seg_6966" s="T7">nagur</ta>
            <ta e="T9" id="Seg_6967" s="T8">nʼi</ta>
            <ta e="T10" id="Seg_6968" s="T9">i-bi</ta>
            <ta e="T11" id="Seg_6969" s="T10">onʼiʔ</ta>
            <ta e="T12" id="Seg_6970" s="T11">Gavrila</ta>
            <ta e="T13" id="Seg_6971" s="T12">onʼiʔ</ta>
            <ta e="T14" id="Seg_6972" s="T13">Danʼila</ta>
            <ta e="T15" id="Seg_6973" s="T14">onʼiʔ</ta>
            <ta e="T16" id="Seg_6974" s="T15">Vanʼuška</ta>
            <ta e="T17" id="Seg_6975" s="T16">duračok</ta>
            <ta e="T18" id="Seg_6976" s="T17">dĭgəttə</ta>
            <ta e="T20" id="Seg_6977" s="T19">dĭ</ta>
            <ta e="T21" id="Seg_6978" s="T20">büzʼe</ta>
            <ta e="T22" id="Seg_6979" s="T21">kuʔ-laʔpi</ta>
            <ta e="T23" id="Seg_6980" s="T22">budəj</ta>
            <ta e="T24" id="Seg_6981" s="T23">i</ta>
            <ta e="T25" id="Seg_6982" s="T24">dĭ</ta>
            <ta e="T26" id="Seg_6983" s="T25">šindi=də</ta>
            <ta e="T29" id="Seg_6984" s="T28">nüdʼi-n</ta>
            <ta e="T30" id="Seg_6985" s="T29">šo-luʔ-jəʔ</ta>
            <ta e="T31" id="Seg_6986" s="T30">da</ta>
            <ta e="T32" id="Seg_6987" s="T31">am-naʔbə</ta>
            <ta e="T33" id="Seg_6988" s="T32">budəj</ta>
            <ta e="T34" id="Seg_6989" s="T33">nada</ta>
            <ta e="T35" id="Seg_6990" s="T34">măn-də</ta>
            <ta e="T36" id="Seg_6991" s="T35">kan-zittə</ta>
            <ta e="T38" id="Seg_6992" s="T37">mandə-sʼtə</ta>
            <ta e="T39" id="Seg_6993" s="T38">šində</ta>
            <ta e="T40" id="Seg_6994" s="T39">am-nʼia-t</ta>
            <ta e="T41" id="Seg_6995" s="T40">budəj</ta>
            <ta e="T42" id="Seg_6996" s="T41">nada</ta>
            <ta e="T43" id="Seg_6997" s="T42">dʼabə-sʼtə</ta>
            <ta e="T44" id="Seg_6998" s="T43">dĭgəttə</ta>
            <ta e="T45" id="Seg_6999" s="T44">kam-bi</ta>
            <ta e="T46" id="Seg_7000" s="T45">staršij</ta>
            <ta e="T47" id="Seg_7001" s="T46">nʼi-t</ta>
            <ta e="T48" id="Seg_7002" s="T47">Gavrila</ta>
            <ta e="T49" id="Seg_7003" s="T48">dibər</ta>
            <ta e="T50" id="Seg_7004" s="T49">kam-bi</ta>
            <ta e="T51" id="Seg_7005" s="T50">kunol-bi</ta>
            <ta e="T52" id="Seg_7006" s="T51">noʔ-gən</ta>
            <ta e="T53" id="Seg_7007" s="T52">i</ta>
            <ta e="T54" id="Seg_7008" s="T53">šo-bi</ta>
            <ta e="T55" id="Seg_7009" s="T54">maʔ-nə</ta>
            <ta e="T56" id="Seg_7010" s="T55">amno-bia-m</ta>
            <ta e="T57" id="Seg_7011" s="T56">ej</ta>
            <ta e="T58" id="Seg_7012" s="T57">kunol-bia-m</ta>
            <ta e="T59" id="Seg_7013" s="T58">a</ta>
            <ta e="T60" id="Seg_7014" s="T59">šindi=də</ta>
            <ta e="T61" id="Seg_7015" s="T60">ej</ta>
            <ta e="T62" id="Seg_7016" s="T61">ku-bia-m</ta>
            <ta e="T63" id="Seg_7017" s="T62">dĭgəttə</ta>
            <ta e="T64" id="Seg_7018" s="T63">baška</ta>
            <ta e="T65" id="Seg_7019" s="T64">nʼi-t</ta>
            <ta e="T66" id="Seg_7020" s="T65">kandə-ga</ta>
            <ta e="T67" id="Seg_7021" s="T66">Danʼila-t</ta>
            <ta e="T68" id="Seg_7022" s="T67">kam-bi</ta>
            <ta e="T69" id="Seg_7023" s="T68">kunol-bi</ta>
            <ta e="T70" id="Seg_7024" s="T69">noʔ-gən</ta>
            <ta e="T71" id="Seg_7025" s="T70">dĭgəttə</ta>
            <ta e="T72" id="Seg_7026" s="T71">ertə-n</ta>
            <ta e="T73" id="Seg_7027" s="T72">šo-bi</ta>
            <ta e="T74" id="Seg_7028" s="T73">bar</ta>
            <ta e="T75" id="Seg_7029" s="T74">nudʼi</ta>
            <ta e="T76" id="Seg_7030" s="T75">ej</ta>
            <ta e="T77" id="Seg_7031" s="T76">kunol-bia-m</ta>
            <ta e="T78" id="Seg_7032" s="T77">a</ta>
            <ta e="T79" id="Seg_7033" s="T78">šini-m=də</ta>
            <ta e="T80" id="Seg_7034" s="T79">ej</ta>
            <ta e="T81" id="Seg_7035" s="T80">ku-bio-m</ta>
            <ta e="T82" id="Seg_7036" s="T81">dĭgəttə</ta>
            <ta e="T83" id="Seg_7037" s="T82">Vanʼuška</ta>
            <ta e="T84" id="Seg_7038" s="T83">kam-bi</ta>
            <ta e="T85" id="Seg_7039" s="T84">i-bi</ta>
            <ta e="T86" id="Seg_7040" s="T85">arkan-də</ta>
            <ta e="T87" id="Seg_7041" s="T86">dĭgəttə</ta>
            <ta e="T88" id="Seg_7042" s="T87">amno-bi</ta>
            <ta e="T89" id="Seg_7043" s="T88">pi-nə</ta>
            <ta e="T90" id="Seg_7044" s="T89">ej</ta>
            <ta e="T91" id="Seg_7045" s="T90">kunol-ia</ta>
            <ta e="T92" id="Seg_7046" s="T91">amno-laʔbə</ta>
            <ta e="T93" id="Seg_7047" s="T92">ku-lia-t</ta>
            <ta e="T94" id="Seg_7048" s="T93">ine</ta>
            <ta e="T95" id="Seg_7049" s="T94">šo-bi</ta>
            <ta e="T96" id="Seg_7050" s="T95">vsʼaka</ta>
            <ta e="T97" id="Seg_7051" s="T96">i</ta>
            <ta e="T98" id="Seg_7052" s="T97">bar</ta>
            <ta e="T99" id="Seg_7053" s="T98">dĭn</ta>
            <ta e="T100" id="Seg_7054" s="T99">kuban-gən</ta>
            <ta e="T101" id="Seg_7055" s="T100">tor-də</ta>
            <ta e="T102" id="Seg_7056" s="T101">kuvas</ta>
            <ta e="T103" id="Seg_7057" s="T102">dĭ</ta>
            <ta e="T104" id="Seg_7058" s="T103">bar</ta>
            <ta e="T105" id="Seg_7059" s="T104">baʔ-luʔ-pi</ta>
            <ta e="T106" id="Seg_7060" s="T105">arkan-də</ta>
            <ta e="T107" id="Seg_7061" s="T106">dĭʔ-nə</ta>
            <ta e="T108" id="Seg_7062" s="T107">i</ta>
            <ta e="T109" id="Seg_7063" s="T108">dʼabə-luʔ-pi</ta>
            <ta e="T110" id="Seg_7064" s="T109">dĭ</ta>
            <ta e="T111" id="Seg_7065" s="T110">suʔmi-luʔ-pi</ta>
            <ta e="T112" id="Seg_7066" s="T111">ej</ta>
            <ta e="T113" id="Seg_7067" s="T112">mo-lia</ta>
            <ta e="T114" id="Seg_7068" s="T113">suʔmi-sʼtə</ta>
            <ta e="T115" id="Seg_7069" s="T114">dĭgəttə</ta>
            <ta e="T116" id="Seg_7070" s="T115">öʔlə-ʔ</ta>
            <ta e="T117" id="Seg_7071" s="T116">măna</ta>
            <ta e="T118" id="Seg_7072" s="T117">măn</ta>
            <ta e="T119" id="Seg_7073" s="T118">tănan</ta>
            <ta e="T120" id="Seg_7074" s="T119">bar</ta>
            <ta e="T121" id="Seg_7075" s="T120">ĭmbi</ta>
            <ta e="T122" id="Seg_7076" s="T121">mĭ-lə-m</ta>
            <ta e="T123" id="Seg_7077" s="T122">no</ta>
            <ta e="T124" id="Seg_7078" s="T123">kădə</ta>
            <ta e="T125" id="Seg_7079" s="T124">nörbə-ʔ</ta>
            <ta e="T126" id="Seg_7080" s="T125">daška</ta>
            <ta e="T127" id="Seg_7081" s="T126">ej</ta>
            <ta e="T128" id="Seg_7082" s="T127">am-nia-l</ta>
            <ta e="T129" id="Seg_7083" s="T128">budəj</ta>
            <ta e="T130" id="Seg_7084" s="T129">ej</ta>
            <ta e="T131" id="Seg_7085" s="T130">am-na-m</ta>
            <ta e="T132" id="Seg_7086" s="T131">a</ta>
            <ta e="T133" id="Seg_7087" s="T132">kădə</ta>
            <ta e="T134" id="Seg_7088" s="T133">šo-lə-l</ta>
            <ta e="T135" id="Seg_7089" s="T134">dărə</ta>
            <ta e="T136" id="Seg_7090" s="T135">măna</ta>
            <ta e="T137" id="Seg_7091" s="T136">šo-ʔ</ta>
            <ta e="T138" id="Seg_7092" s="T137">döbər</ta>
            <ta e="T139" id="Seg_7093" s="T138">ənu-ʔ</ta>
            <ta e="T141" id="Seg_7094" s="T140">măna</ta>
            <ta e="T142" id="Seg_7095" s="T141">no</ta>
            <ta e="T143" id="Seg_7096" s="T142">dĭrgit</ta>
            <ta e="T144" id="Seg_7097" s="T143">i</ta>
            <ta e="T145" id="Seg_7098" s="T144">kal-la</ta>
            <ta e="T146" id="Seg_7099" s="T145">dʼür-bi</ta>
            <ta e="T147" id="Seg_7100" s="T146">ine-t</ta>
            <ta e="T148" id="Seg_7101" s="T147">a</ta>
            <ta e="T149" id="Seg_7102" s="T148">dĭ</ta>
            <ta e="T150" id="Seg_7103" s="T149">šo-bi</ta>
            <ta e="T151" id="Seg_7104" s="T150">maː-ndə</ta>
            <ta e="T152" id="Seg_7105" s="T151">pʼeš-də</ta>
            <ta e="T153" id="Seg_7106" s="T152">šer-luʔ-pi</ta>
            <ta e="T154" id="Seg_7107" s="T153">măn</ta>
            <ta e="T155" id="Seg_7108" s="T154">dʼabə-bia-m</ta>
            <ta e="T156" id="Seg_7109" s="T155">ine</ta>
            <ta e="T157" id="Seg_7110" s="T156">dĭn</ta>
            <ta e="T158" id="Seg_7111" s="T157">tuj</ta>
            <ta e="T159" id="Seg_7112" s="T158">kamən=də</ta>
            <ta e="T160" id="Seg_7113" s="T159">ej</ta>
            <ta e="T161" id="Seg_7114" s="T160">šo-lə-j</ta>
            <ta e="T162" id="Seg_7115" s="T161">am-zittə</ta>
            <ta e="T163" id="Seg_7116" s="T162">budəj</ta>
            <ta e="T164" id="Seg_7117" s="T163">dĭgəttə</ta>
            <ta e="T165" id="Seg_7118" s="T164">koŋ</ta>
            <ta e="T166" id="Seg_7119" s="T165">a-bi</ta>
            <ta e="T167" id="Seg_7120" s="T166">štobɨ</ta>
            <ta e="T168" id="Seg_7121" s="T167">il</ta>
            <ta e="T169" id="Seg_7122" s="T168">šo-bi-ʔi</ta>
            <ta e="T171" id="Seg_7123" s="T170">dĭ-n</ta>
            <ta e="T172" id="Seg_7124" s="T171">koʔbdo</ta>
            <ta e="T173" id="Seg_7125" s="T172">amno-laʔbə</ta>
            <ta e="T174" id="Seg_7126" s="T173">tʼereb-gən</ta>
            <ta e="T175" id="Seg_7127" s="T174">bar</ta>
            <ta e="T176" id="Seg_7128" s="T175">il</ta>
            <ta e="T177" id="Seg_7129" s="T176">kal-laʔbə-ʔjə</ta>
            <ta e="T178" id="Seg_7130" s="T177">i</ta>
            <ta e="T179" id="Seg_7131" s="T178">i</ta>
            <ta e="T180" id="Seg_7132" s="T179">dĭ-n</ta>
            <ta e="T181" id="Seg_7133" s="T180">kaga-ʔi</ta>
            <ta e="T182" id="Seg_7134" s="T181">kan-də-ga-ʔi</ta>
            <ta e="T183" id="Seg_7135" s="T182">a</ta>
            <ta e="T184" id="Seg_7136" s="T183">dĭ</ta>
            <ta e="T185" id="Seg_7137" s="T184">măn-də</ta>
            <ta e="T186" id="Seg_7138" s="T185">i-gəʔ</ta>
            <ta e="T187" id="Seg_7139" s="T186">măna</ta>
            <ta e="T188" id="Seg_7140" s="T187">gibər</ta>
            <ta e="T189" id="Seg_7141" s="T188">tăn</ta>
            <ta e="T190" id="Seg_7142" s="T189">kal-la-l</ta>
            <ta e="T191" id="Seg_7143" s="T190">ĭmbi=də</ta>
            <ta e="T192" id="Seg_7144" s="T191">dĭn</ta>
            <ta e="T193" id="Seg_7145" s="T192">ej</ta>
            <ta e="T194" id="Seg_7146" s="T193">ku-bia-l</ta>
            <ta e="T195" id="Seg_7147" s="T194">štobɨ</ta>
            <ta e="T196" id="Seg_7148" s="T195">il</ta>
            <ta e="T197" id="Seg_7149" s="T196">püšte-r-bi-ʔi</ta>
            <ta e="T199" id="Seg_7150" s="T198">tănan</ta>
            <ta e="T200" id="Seg_7151" s="T199">dĭ-zeŋ</ta>
            <ta e="T201" id="Seg_7152" s="T200">kal-la</ta>
            <ta e="T202" id="Seg_7153" s="T201">dʼür-bi-ʔi</ta>
            <ta e="T203" id="Seg_7154" s="T202">dĭ</ta>
            <ta e="T204" id="Seg_7155" s="T203">uʔbdə-bi</ta>
            <ta e="T205" id="Seg_7156" s="T204">karzʼina</ta>
            <ta e="T206" id="Seg_7157" s="T205">i-bi</ta>
            <ta e="T207" id="Seg_7158" s="T206">kam-bi</ta>
            <ta e="T208" id="Seg_7159" s="T207">i</ta>
            <ta e="T209" id="Seg_7160" s="T208">davaj</ta>
            <ta e="T210" id="Seg_7161" s="T209">kirgar-zittə</ta>
            <ta e="T211" id="Seg_7162" s="T210">ine-t</ta>
            <ta e="T212" id="Seg_7163" s="T211">šo-bi</ta>
            <ta e="T213" id="Seg_7164" s="T212">bar</ta>
            <ta e="T214" id="Seg_7165" s="T213">döbər</ta>
            <ta e="T215" id="Seg_7166" s="T214">modria-gən</ta>
            <ta e="T216" id="Seg_7167" s="T215">šonu-ga-ʔi</ta>
            <ta e="T217" id="Seg_7168" s="T216">šö</ta>
            <ta e="T218" id="Seg_7169" s="T217">kuza-ndə</ta>
            <ta e="T219" id="Seg_7170" s="T218">šonu-ga</ta>
            <ta e="T220" id="Seg_7171" s="T219">dĭgəttə</ta>
            <ta e="T221" id="Seg_7172" s="T220">dĭ</ta>
            <ta e="T222" id="Seg_7173" s="T221">ine</ta>
            <ta e="T224" id="Seg_7174" s="T222">măn-də</ta>
            <ta e="T225" id="Seg_7175" s="T224">nu-bi</ta>
            <ta e="T226" id="Seg_7176" s="T225">dĭʔ-nə</ta>
            <ta e="T227" id="Seg_7177" s="T226">măn-də</ta>
            <ta e="T228" id="Seg_7178" s="T227">onʼiʔ</ta>
            <ta e="T229" id="Seg_7179" s="T228">ku-nə</ta>
            <ta e="T230" id="Seg_7180" s="T229">paʔ-kaʔ</ta>
            <ta e="T231" id="Seg_7181" s="T230">onʼiʔ-gəʔ</ta>
            <ta e="T232" id="Seg_7182" s="T231">supso-ʔ</ta>
            <ta e="T233" id="Seg_7183" s="T232">dĭ</ta>
            <ta e="T234" id="Seg_7184" s="T233">paʔ-pi</ta>
            <ta e="T235" id="Seg_7185" s="T234">supso-bi</ta>
            <ta e="T236" id="Seg_7186" s="T235">kuvas</ta>
            <ta e="T237" id="Seg_7187" s="T236">mo-laːm-bi</ta>
            <ta e="T238" id="Seg_7188" s="T237">dĭgəttə</ta>
            <ta e="T239" id="Seg_7189" s="T238">dĭʔ-nə</ta>
            <ta e="T240" id="Seg_7190" s="T239">amno-bi</ta>
            <ta e="T241" id="Seg_7191" s="T240">dĭ</ta>
            <ta e="T242" id="Seg_7192" s="T241">nuʔmə</ta>
            <ta e="T243" id="Seg_7193" s="T242">nuʔmə-luʔ-pi</ta>
            <ta e="T244" id="Seg_7194" s="T243">dibər</ta>
            <ta e="T245" id="Seg_7195" s="T244">il</ta>
            <ta e="T246" id="Seg_7196" s="T245">iʔgö</ta>
            <ta e="T247" id="Seg_7197" s="T246">nu-ga-ʔi</ta>
            <ta e="T248" id="Seg_7198" s="T247">a</ta>
            <ta e="T249" id="Seg_7199" s="T248">šindi=də</ta>
            <ta e="T250" id="Seg_7200" s="T249">ej</ta>
            <ta e="T251" id="Seg_7201" s="T250">dĭ</ta>
            <ta e="T252" id="Seg_7202" s="T251">koʔbdo-nə</ta>
            <ta e="T253" id="Seg_7203" s="T252">ej</ta>
            <ta e="T254" id="Seg_7204" s="T253">sʼa-lia-ʔi</ta>
            <ta e="T255" id="Seg_7205" s="T254">dĭ</ta>
            <ta e="T256" id="Seg_7206" s="T255">bar</ta>
            <ta e="T257" id="Seg_7207" s="T256">suʔmi-luʔ-pi</ta>
            <ta e="T258" id="Seg_7208" s="T257">ša-la-ʔjə</ta>
            <ta e="T259" id="Seg_7209" s="T258">ej</ta>
            <ta e="T260" id="Seg_7210" s="T259">ej</ta>
            <ta e="T261" id="Seg_7211" s="T260">dʼa-pi</ta>
            <ta e="T262" id="Seg_7212" s="T261">dĭgəttə</ta>
            <ta e="T263" id="Seg_7213" s="T262">il</ta>
            <ta e="T264" id="Seg_7214" s="T263">bar</ta>
            <ta e="T265" id="Seg_7215" s="T264">kirgar-laʔbə-ʔjə</ta>
            <ta e="T266" id="Seg_7216" s="T265">dʼabə-ʔ</ta>
            <ta e="T267" id="Seg_7217" s="T266">dʼabə-ʔ</ta>
            <ta e="T268" id="Seg_7218" s="T267">a</ta>
            <ta e="T269" id="Seg_7219" s="T268">dĭ</ta>
            <ta e="T270" id="Seg_7220" s="T269">nuʔmə-luʔ-pi</ta>
            <ta e="T271" id="Seg_7221" s="T270">ine-m</ta>
            <ta e="T272" id="Seg_7222" s="T271">öʔlu-bi</ta>
            <ta e="T273" id="Seg_7223" s="T272">dĭgəttə</ta>
            <ta e="T274" id="Seg_7224" s="T273">beške-ʔi</ta>
            <ta e="T275" id="Seg_7225" s="T274">oʔbdə-bi</ta>
            <ta e="T276" id="Seg_7226" s="T275">i</ta>
            <ta e="T277" id="Seg_7227" s="T276">vsʼaka</ta>
            <ta e="T278" id="Seg_7228" s="T277">iʔgö</ta>
            <ta e="T279" id="Seg_7229" s="T278">beške</ta>
            <ta e="T280" id="Seg_7230" s="T279">dep-pi</ta>
            <ta e="T281" id="Seg_7231" s="T280">a</ta>
            <ta e="T282" id="Seg_7232" s="T281">ne-zeŋ</ta>
            <ta e="T283" id="Seg_7233" s="T282">kudo-laʔbə-ʔjə</ta>
            <ta e="T284" id="Seg_7234" s="T283">ĭmbi</ta>
            <ta e="T285" id="Seg_7235" s="T284">dĭrgit</ta>
            <ta e="T287" id="Seg_7236" s="T286">deʔ-pie-l</ta>
            <ta e="T288" id="Seg_7237" s="T287">tolʼko</ta>
            <ta e="T289" id="Seg_7238" s="T288">tănan</ta>
            <ta e="T290" id="Seg_7239" s="T289">am-zittə</ta>
            <ta e="T291" id="Seg_7240" s="T290">dĭ</ta>
            <ta e="T292" id="Seg_7241" s="T291">pʼeš-də</ta>
            <ta e="T294" id="Seg_7242" s="T293">sʼa-bi</ta>
            <ta e="T295" id="Seg_7243" s="T294">iʔbö-laʔbə</ta>
            <ta e="T296" id="Seg_7244" s="T295">dĭgəttə</ta>
            <ta e="T298" id="Seg_7245" s="T297">kaga-zaŋ-də</ta>
            <ta e="T299" id="Seg_7246" s="T298">šo-bi-ʔi</ta>
            <ta e="T300" id="Seg_7247" s="T299">bar</ta>
            <ta e="T301" id="Seg_7248" s="T300">nörbə-leʔbə-ʔjə</ta>
            <ta e="T303" id="Seg_7249" s="T302">kăda</ta>
            <ta e="T304" id="Seg_7250" s="T303">i-bi</ta>
            <ta e="T305" id="Seg_7251" s="T304">a</ta>
            <ta e="T306" id="Seg_7252" s="T305">dĭ</ta>
            <ta e="T307" id="Seg_7253" s="T306">iʔbö-laʔbə</ta>
            <ta e="T308" id="Seg_7254" s="T307">dĭ</ta>
            <ta e="T309" id="Seg_7255" s="T308">măn</ta>
            <ta e="T310" id="Seg_7256" s="T309">ulu-m</ta>
            <ta e="T311" id="Seg_7257" s="T310">dĭn</ta>
            <ta e="T312" id="Seg_7258" s="T311">i-bi</ta>
            <ta e="T313" id="Seg_7259" s="T312">i-ʔ</ta>
            <ta e="T314" id="Seg_7260" s="T313">šama-ʔ</ta>
            <ta e="T315" id="Seg_7261" s="T314">tăn</ta>
            <ta e="T316" id="Seg_7262" s="T315">ulu-m</ta>
            <ta e="T317" id="Seg_7263" s="T316">amno-ʔ</ta>
            <ta e="T318" id="Seg_7264" s="T317">už</ta>
            <ta e="T319" id="Seg_7265" s="T318">ĭmbi=də</ta>
            <ta e="T320" id="Seg_7266" s="T319">i-ʔ</ta>
            <ta e="T321" id="Seg_7267" s="T320">dʼăbaktər-a-ʔ</ta>
            <ta e="T322" id="Seg_7268" s="T321">dĭrgit</ta>
            <ta e="T323" id="Seg_7269" s="T322">kuza</ta>
            <ta e="T324" id="Seg_7270" s="T323">i-bi</ta>
            <ta e="T325" id="Seg_7271" s="T324">ine-t</ta>
            <ta e="T326" id="Seg_7272" s="T325">kuvas</ta>
            <ta e="T327" id="Seg_7273" s="T326">i-bi</ta>
            <ta e="T328" id="Seg_7274" s="T327">bos-tə</ta>
            <ta e="T329" id="Seg_7275" s="T328">dĭgəttə</ta>
            <ta e="T330" id="Seg_7276" s="T329">karəldʼan</ta>
            <ta e="T331" id="Seg_7277" s="T330">bazoʔ</ta>
            <ta e="T332" id="Seg_7278" s="T331">kan-də-ga-ʔi</ta>
            <ta e="T333" id="Seg_7279" s="T332">kal-la</ta>
            <ta e="T334" id="Seg_7280" s="T333">dʼür-bi-ʔi</ta>
            <ta e="T335" id="Seg_7281" s="T334">dĭ</ta>
            <ta e="T336" id="Seg_7282" s="T335">pʼeš-gəʔ</ta>
            <ta e="T338" id="Seg_7283" s="T337">suʔmi-luʔ-pi</ta>
            <ta e="T339" id="Seg_7284" s="T338">karzʼina</ta>
            <ta e="T340" id="Seg_7285" s="T339">i-bi</ta>
            <ta e="T341" id="Seg_7286" s="T340">kam-bi</ta>
            <ta e="T342" id="Seg_7287" s="T341">kam-bi</ta>
            <ta e="T344" id="Seg_7288" s="T342">baruʔ-pi</ta>
            <ta e="T345" id="Seg_7289" s="T344">davaj</ta>
            <ta e="T346" id="Seg_7290" s="T345">ine-m</ta>
            <ta e="T347" id="Seg_7291" s="T346">kirgar-zittə</ta>
            <ta e="T348" id="Seg_7292" s="T347">ine-t</ta>
            <ta e="T349" id="Seg_7293" s="T348">šo-bi</ta>
            <ta e="T350" id="Seg_7294" s="T349">bazoʔ</ta>
            <ta e="T351" id="Seg_7295" s="T350">onʼiʔ</ta>
            <ta e="T352" id="Seg_7296" s="T351">kuʔ-tə</ta>
            <ta e="T353" id="Seg_7297" s="T352">paʔ-laːm-bi</ta>
            <ta e="T354" id="Seg_7298" s="T353">onʼiʔ</ta>
            <ta e="T355" id="Seg_7299" s="T354">supso-laːm-bi</ta>
            <ta e="T356" id="Seg_7300" s="T355">dĭgəttə</ta>
            <ta e="T357" id="Seg_7301" s="T356">amnə-bi</ta>
            <ta e="T359" id="Seg_7302" s="T358">šonu-ga</ta>
            <ta e="T360" id="Seg_7303" s="T359">il</ta>
            <ta e="T361" id="Seg_7304" s="T360">iʔgö</ta>
            <ta e="T362" id="Seg_7305" s="T361">nu-ga-ʔi</ta>
            <ta e="T363" id="Seg_7306" s="T362">dĭgəttə</ta>
            <ta e="T364" id="Seg_7307" s="T363">šindi=də</ta>
            <ta e="T365" id="Seg_7308" s="T364">ej</ta>
            <ta e="T366" id="Seg_7309" s="T365">nuʔmə-lie</ta>
            <ta e="T367" id="Seg_7310" s="T366">dibər</ta>
            <ta e="T368" id="Seg_7311" s="T367">dibər</ta>
            <ta e="T370" id="Seg_7312" s="T369">suʔmi-luʔ-pi</ta>
            <ta e="T371" id="Seg_7313" s="T370">i-bi</ta>
            <ta e="T376" id="Seg_7314" s="T375">ej</ta>
            <ta e="T377" id="Seg_7315" s="T376">mo-bi</ta>
            <ta e="T378" id="Seg_7316" s="T377">dĭgəttə</ta>
            <ta e="T379" id="Seg_7317" s="T378">par-luʔ-pi</ta>
            <ta e="T380" id="Seg_7318" s="T379">kaga-zaŋ-də</ta>
            <ta e="T381" id="Seg_7319" s="T380">bar</ta>
            <ta e="T382" id="Seg_7320" s="T381">münör-bi</ta>
            <ta e="T383" id="Seg_7321" s="T382">i</ta>
            <ta e="T384" id="Seg_7322" s="T383">kal-la</ta>
            <ta e="T385" id="Seg_7323" s="T384">dʼür-bi</ta>
            <ta e="T386" id="Seg_7324" s="T385">a</ta>
            <ta e="T387" id="Seg_7325" s="T386">il</ta>
            <ta e="T388" id="Seg_7326" s="T387">kirgar-laʔbə-ʔjə</ta>
            <ta e="T389" id="Seg_7327" s="T388">dʼabə-ʔ</ta>
            <ta e="T390" id="Seg_7328" s="T389">dʼabə-ʔ</ta>
            <ta e="T391" id="Seg_7329" s="T390">da</ta>
            <ta e="T392" id="Seg_7330" s="T391">dĭ</ta>
            <ta e="T393" id="Seg_7331" s="T392">i-ʔ</ta>
            <ta e="T394" id="Seg_7332" s="T393">məluʔ-pi</ta>
            <ta e="T395" id="Seg_7333" s="T394">ine-bə</ta>
            <ta e="T396" id="Seg_7334" s="T395">öʔluʔ-pi</ta>
            <ta e="T397" id="Seg_7335" s="T396">a</ta>
            <ta e="T398" id="Seg_7336" s="T397">bos-tə</ta>
            <ta e="T399" id="Seg_7337" s="T398">bazoʔ</ta>
            <ta e="T400" id="Seg_7338" s="T399">šo-bi</ta>
            <ta e="T401" id="Seg_7339" s="T400">ne-zeŋ-də</ta>
            <ta e="T402" id="Seg_7340" s="T401">bazoʔ</ta>
            <ta e="T403" id="Seg_7341" s="T402">kudo-laʔbə-ʔjə</ta>
            <ta e="T404" id="Seg_7342" s="T403">a</ta>
            <ta e="T405" id="Seg_7343" s="T404">dĭ</ta>
            <ta e="T406" id="Seg_7344" s="T405">pʼeš-də</ta>
            <ta e="T407" id="Seg_7345" s="T406">sʼa-bi</ta>
            <ta e="T408" id="Seg_7346" s="T407">da</ta>
            <ta e="T409" id="Seg_7347" s="T408">iʔbö-laʔbə</ta>
            <ta e="T411" id="Seg_7348" s="T410">kazaŋ-də</ta>
            <ta e="T412" id="Seg_7349" s="T411">šo-bi-ʔi</ta>
            <ta e="T413" id="Seg_7350" s="T412">aba-ndə</ta>
            <ta e="T414" id="Seg_7351" s="T413">nörbə-lie-ʔi</ta>
            <ta e="T415" id="Seg_7352" s="T414">girgit</ta>
            <ta e="T416" id="Seg_7353" s="T415">kuza</ta>
            <ta e="T417" id="Seg_7354" s="T416">i-bi</ta>
            <ta e="T418" id="Seg_7355" s="T417">ine-t</ta>
            <ta e="T419" id="Seg_7356" s="T418">ugaːndə</ta>
            <ta e="T420" id="Seg_7357" s="T419">kuvas</ta>
            <ta e="T421" id="Seg_7358" s="T420">i</ta>
            <ta e="T423" id="Seg_7359" s="T422">bos-tə</ta>
            <ta e="T424" id="Seg_7360" s="T423">kuvas</ta>
            <ta e="T425" id="Seg_7361" s="T424">a</ta>
            <ta e="T426" id="Seg_7362" s="T425">dĭ</ta>
            <ta e="T427" id="Seg_7363" s="T426">iʔbö-laʔbə</ta>
            <ta e="T428" id="Seg_7364" s="T427">ej</ta>
            <ta e="T429" id="Seg_7365" s="T428">măn</ta>
            <ta e="T430" id="Seg_7366" s="T429">li</ta>
            <ta e="T431" id="Seg_7367" s="T430">ulu-m</ta>
            <ta e="T432" id="Seg_7368" s="T431">dĭn</ta>
            <ta e="T433" id="Seg_7369" s="T432">i-bi</ta>
            <ta e="T434" id="Seg_7370" s="T433">i-ʔ</ta>
            <ta e="T435" id="Seg_7371" s="T434">šama-ʔ</ta>
            <ta e="T436" id="Seg_7372" s="T435">iʔbə-ʔ</ta>
            <ta e="T437" id="Seg_7373" s="T436">už</ta>
            <ta e="T438" id="Seg_7374" s="T437">sagəš-šət</ta>
            <ta e="T439" id="Seg_7375" s="T438">nʼi</ta>
            <ta e="T440" id="Seg_7376" s="T439">tăn</ta>
            <ta e="T441" id="Seg_7377" s="T440">dĭn</ta>
            <ta e="T442" id="Seg_7378" s="T441">i-bie-l</ta>
            <ta e="T443" id="Seg_7379" s="T442">dĭgəttə</ta>
            <ta e="T444" id="Seg_7380" s="T443">bazoʔ</ta>
            <ta e="T445" id="Seg_7381" s="T444">nagur</ta>
            <ta e="T446" id="Seg_7382" s="T445">dʼala</ta>
            <ta e="T447" id="Seg_7383" s="T446">bazoʔ</ta>
            <ta e="T448" id="Seg_7384" s="T447">kan-də-ga-ʔi</ta>
            <ta e="T449" id="Seg_7385" s="T448">kal-la</ta>
            <ta e="T450" id="Seg_7386" s="T449">dʼür-bi-ʔi</ta>
            <ta e="T451" id="Seg_7387" s="T450">dĭ</ta>
            <ta e="T453" id="Seg_7388" s="T452">uʔbdə-bi</ta>
            <ta e="T454" id="Seg_7389" s="T453">karzʼina-bə</ta>
            <ta e="T455" id="Seg_7390" s="T454">i-bi</ta>
            <ta e="T456" id="Seg_7391" s="T455">kam-bi</ta>
            <ta e="T457" id="Seg_7392" s="T456">beške-ʔi</ta>
            <ta e="T458" id="Seg_7393" s="T457">oʔbši-zittə</ta>
            <ta e="T459" id="Seg_7394" s="T458">kam-bi</ta>
            <ta e="T460" id="Seg_7395" s="T459">davaj</ta>
            <ta e="T461" id="Seg_7396" s="T460">ine</ta>
            <ta e="T462" id="Seg_7397" s="T461">kirgar-zittə</ta>
            <ta e="T463" id="Seg_7398" s="T462">ine</ta>
            <ta e="T464" id="Seg_7399" s="T463">šo-bi</ta>
            <ta e="T465" id="Seg_7400" s="T464">dĭ</ta>
            <ta e="T466" id="Seg_7401" s="T465">bazoʔ</ta>
            <ta e="T467" id="Seg_7402" s="T466">dăra</ta>
            <ta e="T469" id="Seg_7403" s="T468">onʼiʔ</ta>
            <ta e="T470" id="Seg_7404" s="T469">ku-gəndə</ta>
            <ta e="T471" id="Seg_7405" s="T470">paʔ-laːm-bi</ta>
            <ta e="T472" id="Seg_7406" s="T471">onʼiʔ</ta>
            <ta e="T473" id="Seg_7407" s="T472">supso-laːm-bi</ta>
            <ta e="T474" id="Seg_7408" s="T473">amno-bi</ta>
            <ta e="T475" id="Seg_7409" s="T474">kam-bi</ta>
            <ta e="T476" id="Seg_7410" s="T475">dĭgəttə</ta>
            <ta e="T477" id="Seg_7411" s="T476">kak</ta>
            <ta e="T478" id="Seg_7412" s="T477">nuʔmə-luʔ-pi</ta>
            <ta e="T479" id="Seg_7413" s="T478">dĭ-m</ta>
            <ta e="T480" id="Seg_7414" s="T479">panar-bi</ta>
            <ta e="T481" id="Seg_7415" s="T480">i</ta>
            <ta e="T482" id="Seg_7416" s="T481">kalʼečka-bə</ta>
            <ta e="T483" id="Seg_7417" s="T482">i-luʔ-pi</ta>
            <ta e="T484" id="Seg_7418" s="T483">bos-tə</ta>
            <ta e="T485" id="Seg_7419" s="T484">uda-ndə</ta>
            <ta e="T487" id="Seg_7420" s="T486">šer-bi</ta>
            <ta e="T488" id="Seg_7421" s="T487">kal-la</ta>
            <ta e="T489" id="Seg_7422" s="T488">dʼür-bi</ta>
            <ta e="T490" id="Seg_7423" s="T489">ine-bə</ta>
            <ta e="T491" id="Seg_7424" s="T490">öʔlu-bi</ta>
            <ta e="T492" id="Seg_7425" s="T491">bos-tə</ta>
            <ta e="T493" id="Seg_7426" s="T492">šo-bi</ta>
            <ta e="T494" id="Seg_7427" s="T493">maː-ndə</ta>
            <ta e="T495" id="Seg_7428" s="T494">i</ta>
            <ta e="T496" id="Seg_7429" s="T495">pʼeš-də</ta>
            <ta e="T497" id="Seg_7430" s="T496">sʼa-bi</ta>
            <ta e="T498" id="Seg_7431" s="T497">iʔbö-laʔbə</ta>
            <ta e="T499" id="Seg_7432" s="T498">dĭgəttə</ta>
            <ta e="T500" id="Seg_7433" s="T499">kaga-zaŋ-də</ta>
            <ta e="T501" id="Seg_7434" s="T500">šo-bi-ʔi</ta>
            <ta e="T502" id="Seg_7435" s="T501">nu</ta>
            <ta e="T503" id="Seg_7436" s="T502">girgit=də</ta>
            <ta e="T504" id="Seg_7437" s="T503">nʼi</ta>
            <ta e="T505" id="Seg_7438" s="T504">nʼi</ta>
            <ta e="T506" id="Seg_7439" s="T505">kuvas</ta>
            <ta e="T507" id="Seg_7440" s="T506">ine-t</ta>
            <ta e="T508" id="Seg_7441" s="T507">kuvas</ta>
            <ta e="T509" id="Seg_7442" s="T508">panar-bi</ta>
            <ta e="T510" id="Seg_7443" s="T509">tsarskɨj</ta>
            <ta e="T511" id="Seg_7444" s="T510">koʔbdo</ta>
            <ta e="T512" id="Seg_7445" s="T511">koŋ</ta>
            <ta e="T513" id="Seg_7446" s="T512">koŋ</ta>
            <ta e="T514" id="Seg_7447" s="T513">koʔbdo</ta>
            <ta e="T515" id="Seg_7448" s="T514">i</ta>
            <ta e="T516" id="Seg_7449" s="T515">kalʼečka-bə</ta>
            <ta e="T517" id="Seg_7450" s="T516">i-bi</ta>
            <ta e="T518" id="Seg_7451" s="T517">kal-la</ta>
            <ta e="T519" id="Seg_7452" s="T518">dʼür-bi</ta>
            <ta e="T520" id="Seg_7453" s="T519">dĭgəttə</ta>
            <ta e="T521" id="Seg_7454" s="T520">bazoʔ</ta>
            <ta e="T522" id="Seg_7455" s="T521">dĭ</ta>
            <ta e="T523" id="Seg_7456" s="T522">koŋ</ta>
            <ta e="T524" id="Seg_7457" s="T523">a-bi</ta>
            <ta e="T525" id="Seg_7458" s="T524">iʔgö</ta>
            <ta e="T526" id="Seg_7459" s="T525">uja</ta>
            <ta e="T527" id="Seg_7460" s="T526">munuj-ʔ</ta>
            <ta e="T528" id="Seg_7461" s="T527">em-bi</ta>
            <ta e="T529" id="Seg_7462" s="T528">bar</ta>
            <ta e="T530" id="Seg_7463" s="T529">ĭmbi</ta>
            <ta e="T531" id="Seg_7464" s="T530">dĭn</ta>
            <ta e="T532" id="Seg_7465" s="T531">i-ge</ta>
            <ta e="T533" id="Seg_7466" s="T532">em-bi</ta>
            <ta e="T534" id="Seg_7467" s="T533">dĭgəttə</ta>
            <ta e="T535" id="Seg_7468" s="T534">šo-gaʔ</ta>
            <ta e="T536" id="Seg_7469" s="T535">bar-gəʔ</ta>
            <ta e="T537" id="Seg_7470" s="T536">šində</ta>
            <ta e="T538" id="Seg_7471" s="T537">ej</ta>
            <ta e="T539" id="Seg_7472" s="T538">šo-lə-j</ta>
            <ta e="T540" id="Seg_7473" s="T539">ulu-bə</ta>
            <ta e="T542" id="Seg_7474" s="T541">saj-jaʔ-li-m</ta>
            <ta e="T543" id="Seg_7475" s="T542">dĭgəttə</ta>
            <ta e="T544" id="Seg_7476" s="T543">dĭ-zeŋ</ta>
            <ta e="T546" id="Seg_7477" s="T545">dĭn</ta>
            <ta e="T547" id="Seg_7478" s="T546">aba-t</ta>
            <ta e="T548" id="Seg_7479" s="T547">bar-gəʔ</ta>
            <ta e="T549" id="Seg_7480" s="T548">kal-la</ta>
            <ta e="T550" id="Seg_7481" s="T549">dʼür-bi-ʔi</ta>
            <ta e="T551" id="Seg_7482" s="T550">šo-bi-ʔi</ta>
            <ta e="T552" id="Seg_7483" s="T551">dĭ-zeŋ</ta>
            <ta e="T553" id="Seg_7484" s="T552">amnəl-bi-ʔi</ta>
            <ta e="T555" id="Seg_7485" s="T554">bar</ta>
            <ta e="T556" id="Seg_7486" s="T555">bĭd-laʔbə-ʔjə</ta>
            <ta e="T558" id="Seg_7487" s="T557">ara</ta>
            <ta e="T559" id="Seg_7488" s="T558">mĭ-leʔbə-ʔjə</ta>
            <ta e="T560" id="Seg_7489" s="T559">dĭgəttə</ta>
            <ta e="T561" id="Seg_7490" s="T560">šo-bi</ta>
            <ta e="T562" id="Seg_7491" s="T561">dĭ</ta>
            <ta e="T563" id="Seg_7492" s="T562">poslʼednij</ta>
            <ta e="T564" id="Seg_7493" s="T563">nʼi-t</ta>
            <ta e="T565" id="Seg_7494" s="T564">dĭ</ta>
            <ta e="T566" id="Seg_7495" s="T565">dĭ</ta>
            <ta e="T567" id="Seg_7496" s="T566">bar</ta>
            <ta e="T568" id="Seg_7497" s="T567">sagər</ta>
            <ta e="T569" id="Seg_7498" s="T568">amno-laʔbə</ta>
            <ta e="T570" id="Seg_7499" s="T569">i</ta>
            <ta e="T571" id="Seg_7500" s="T570">uda-t</ta>
            <ta e="T572" id="Seg_7501" s="T571">sagər</ta>
            <ta e="T573" id="Seg_7502" s="T572">trʼapka-zʼiʔ</ta>
            <ta e="T574" id="Seg_7503" s="T573">sar-o-na</ta>
            <ta e="T575" id="Seg_7504" s="T574">măn-də</ta>
            <ta e="T576" id="Seg_7505" s="T575">ĭmbi</ta>
            <ta e="T577" id="Seg_7506" s="T576">tăn</ta>
            <ta e="T578" id="Seg_7507" s="T577">uda-ndə</ta>
            <ta e="T580" id="Seg_7508" s="T579">ku-lia-t</ta>
            <ta e="T581" id="Seg_7509" s="T580">bar</ta>
            <ta e="T582" id="Seg_7510" s="T581">bar</ta>
            <ta e="T583" id="Seg_7511" s="T582">dĭn</ta>
            <ta e="T584" id="Seg_7512" s="T583">kalʼečka</ta>
            <ta e="T585" id="Seg_7513" s="T584">amno-laʔbə</ta>
            <ta e="T586" id="Seg_7514" s="T585">dĭgəttə</ta>
            <ta e="T587" id="Seg_7515" s="T586">i-le-t</ta>
            <ta e="T588" id="Seg_7516" s="T587">aba-ndə</ta>
            <ta e="T589" id="Seg_7517" s="T588">šo-lia-t</ta>
            <ta e="T590" id="Seg_7518" s="T589">dö</ta>
            <ta e="T592" id="Seg_7519" s="T591">dö</ta>
            <ta e="T593" id="Seg_7520" s="T592">măn</ta>
            <ta e="T594" id="Seg_7521" s="T593">ženʼix-bə</ta>
            <ta e="T595" id="Seg_7522" s="T594">dĭ-m</ta>
            <ta e="T596" id="Seg_7523" s="T595">dĭ</ta>
            <ta e="T598" id="Seg_7524" s="T597">dĭ-zeŋ</ta>
            <ta e="T599" id="Seg_7525" s="T598">dĭn</ta>
            <ta e="T600" id="Seg_7526" s="T599">bəzə-bi-ʔi</ta>
            <ta e="T601" id="Seg_7527" s="T600">kuvas</ta>
            <ta e="T602" id="Seg_7528" s="T601">oldʼa</ta>
            <ta e="T604" id="Seg_7529" s="T603">šer-bi-ʔi</ta>
            <ta e="T605" id="Seg_7530" s="T604">ugaːndə</ta>
            <ta e="T606" id="Seg_7531" s="T605">kuvas</ta>
            <ta e="T607" id="Seg_7532" s="T606">mo-laːm-bi</ta>
            <ta e="T608" id="Seg_7533" s="T607">dĭgəttə</ta>
            <ta e="T609" id="Seg_7534" s="T608">bazoʔ</ta>
            <ta e="T610" id="Seg_7535" s="T609">davaj</ta>
            <ta e="T611" id="Seg_7536" s="T610">ara</ta>
            <ta e="T612" id="Seg_7537" s="T611">bĭs-sittə</ta>
            <ta e="T613" id="Seg_7538" s="T612">sʼar-zittə</ta>
            <ta e="T614" id="Seg_7539" s="T613">kaga-zaŋ-də</ta>
            <ta e="T615" id="Seg_7540" s="T614">bar</ta>
            <ta e="T616" id="Seg_7541" s="T615">mănd-laʔbə-ʔjə</ta>
            <ta e="T617" id="Seg_7542" s="T616">girgit</ta>
            <ta e="T618" id="Seg_7543" s="T617">kuvas</ta>
            <ta e="T619" id="Seg_7544" s="T618">mo-laːm-bi</ta>
            <ta e="T620" id="Seg_7545" s="T619">i</ta>
            <ta e="T621" id="Seg_7546" s="T620">dĭn</ta>
            <ta e="T622" id="Seg_7547" s="T621">măn</ta>
            <ta e="T623" id="Seg_7548" s="T622">i-bie-m</ta>
            <ta e="T624" id="Seg_7549" s="T623">ara</ta>
            <ta e="T625" id="Seg_7550" s="T624">bar</ta>
            <ta e="T626" id="Seg_7551" s="T625">mʼaŋ-ŋaʔbə</ta>
            <ta e="T627" id="Seg_7552" s="T626">mĭ-bi-ʔi</ta>
            <ta e="T628" id="Seg_7553" s="T627">măna</ta>
            <ta e="T629" id="Seg_7554" s="T628">blin</ta>
            <ta e="T634" id="Seg_7555" s="T633">amno-bi-ʔi</ta>
            <ta e="T635" id="Seg_7556" s="T634">šide-göʔ</ta>
            <ta e="T636" id="Seg_7557" s="T635">kum</ta>
            <ta e="T637" id="Seg_7558" s="T636">da</ta>
            <ta e="T638" id="Seg_7559" s="T637">kuma</ta>
            <ta e="T639" id="Seg_7560" s="T638">volk</ta>
            <ta e="T640" id="Seg_7561" s="T639">da</ta>
            <ta e="T641" id="Seg_7562" s="T640">lʼisa</ta>
            <ta e="T642" id="Seg_7563" s="T641">dĭgəttə</ta>
            <ta e="T643" id="Seg_7564" s="T642">kunol-zittə</ta>
            <ta e="T644" id="Seg_7565" s="T643">iʔbə-ʔi</ta>
            <ta e="T645" id="Seg_7566" s="T644">a</ta>
            <ta e="T646" id="Seg_7567" s="T645">lʼisa</ta>
            <ta e="T647" id="Seg_7568" s="T646">uge</ta>
            <ta e="T648" id="Seg_7569" s="T647">kuzur-leʔbə</ta>
            <ta e="T649" id="Seg_7570" s="T648">kuzur-leʔbə</ta>
            <ta e="T650" id="Seg_7571" s="T649">a</ta>
            <ta e="T651" id="Seg_7572" s="T650">volk</ta>
            <ta e="T652" id="Seg_7573" s="T651">măn-də</ta>
            <ta e="T653" id="Seg_7574" s="T652">šindi=də</ta>
            <ta e="T654" id="Seg_7575" s="T653">kuzur-leʔbə</ta>
            <ta e="T655" id="Seg_7576" s="T654">măna</ta>
            <ta e="T656" id="Seg_7577" s="T655">kăštə-lia-ʔi</ta>
            <ta e="T657" id="Seg_7578" s="T656">ešši</ta>
            <ta e="T658" id="Seg_7579" s="T657">ešši</ta>
            <ta e="T659" id="Seg_7580" s="T658">pădə-sʼtə</ta>
            <ta e="T660" id="Seg_7581" s="T659">dĭgəttə</ta>
            <ta e="T661" id="Seg_7582" s="T660">no</ta>
            <ta e="T662" id="Seg_7583" s="T661">kan-a-ʔ</ta>
            <ta e="T663" id="Seg_7584" s="T662">dĭ</ta>
            <ta e="T664" id="Seg_7585" s="T663">kam-bi</ta>
            <ta e="T665" id="Seg_7586" s="T664">a</ta>
            <ta e="T666" id="Seg_7587" s="T665">dĭ-zeŋ</ta>
            <ta e="T667" id="Seg_7588" s="T666">nulaʔ-bi</ta>
            <ta e="T668" id="Seg_7589" s="T667">mʼod</ta>
            <ta e="T669" id="Seg_7590" s="T668">am-bi</ta>
            <ta e="T670" id="Seg_7591" s="T669">am-bi</ta>
            <ta e="T671" id="Seg_7592" s="T670">mʼod</ta>
            <ta e="T672" id="Seg_7593" s="T671">dĭ</ta>
            <ta e="T673" id="Seg_7594" s="T672">šo-bi</ta>
            <ta e="T674" id="Seg_7595" s="T673">šindi</ta>
            <ta e="T675" id="Seg_7596" s="T674">dĭn</ta>
            <ta e="T676" id="Seg_7597" s="T675">i-ge</ta>
            <ta e="T677" id="Seg_7598" s="T676">veršok</ta>
            <ta e="T678" id="Seg_7599" s="T677">i-ge</ta>
            <ta e="T679" id="Seg_7600" s="T678">dĭgəttə</ta>
            <ta e="T680" id="Seg_7601" s="T679">bazoʔ</ta>
            <ta e="T681" id="Seg_7602" s="T680">nüdʼi-n</ta>
            <ta e="T682" id="Seg_7603" s="T681">bazoʔ</ta>
            <ta e="T683" id="Seg_7604" s="T682">kuzur-leʔbə</ta>
            <ta e="T684" id="Seg_7605" s="T683">lʼisʼitsa</ta>
            <ta e="T685" id="Seg_7606" s="T684">šindi</ta>
            <ta e="T686" id="Seg_7607" s="T685">kuzur-leʔbə</ta>
            <ta e="T687" id="Seg_7608" s="T686">da</ta>
            <ta e="T688" id="Seg_7609" s="T687">măna</ta>
            <ta e="T689" id="Seg_7610" s="T688">kăštə-laʔbə-ʔjə</ta>
            <ta e="T690" id="Seg_7611" s="T689">ešši</ta>
            <ta e="T691" id="Seg_7612" s="T690">ködər-zittə</ta>
            <ta e="T692" id="Seg_7613" s="T691">no</ta>
            <ta e="T693" id="Seg_7614" s="T692">kan-a-ʔ</ta>
            <ta e="T694" id="Seg_7615" s="T693">dĭ</ta>
            <ta e="T695" id="Seg_7616" s="T694">kam-bi</ta>
            <ta e="T696" id="Seg_7617" s="T695">am-bi</ta>
            <ta e="T697" id="Seg_7618" s="T696">am-bi</ta>
            <ta e="T698" id="Seg_7619" s="T697">dĭgəttə</ta>
            <ta e="T699" id="Seg_7620" s="T698">šo-bi</ta>
            <ta e="T700" id="Seg_7621" s="T699">šindi</ta>
            <ta e="T701" id="Seg_7622" s="T700">dĭn</ta>
            <ta e="T702" id="Seg_7623" s="T701">šo-bi</ta>
            <ta e="T703" id="Seg_7624" s="T702">sʼerʼodušok</ta>
            <ta e="T704" id="Seg_7625" s="T703">šo-bi</ta>
            <ta e="T705" id="Seg_7626" s="T704">dĭgəttə</ta>
            <ta e="T706" id="Seg_7627" s="T705">nagur-git</ta>
            <ta e="T707" id="Seg_7628" s="T706">nüdʼi-n</ta>
            <ta e="T708" id="Seg_7629" s="T707">bazoʔ</ta>
            <ta e="T709" id="Seg_7630" s="T708">kuzur-leʔbə</ta>
            <ta e="T710" id="Seg_7631" s="T709">dĭgəttə</ta>
            <ta e="T711" id="Seg_7632" s="T710">šindi=də</ta>
            <ta e="T712" id="Seg_7633" s="T711">kuzur-leʔbə</ta>
            <ta e="T713" id="Seg_7634" s="T712">da</ta>
            <ta e="T714" id="Seg_7635" s="T713">măna</ta>
            <ta e="T715" id="Seg_7636" s="T714">kăštə-laʔbə-ʔjə</ta>
            <ta e="T716" id="Seg_7637" s="T715">no</ta>
            <ta e="T717" id="Seg_7638" s="T716">kan-a-ʔ</ta>
            <ta e="T718" id="Seg_7639" s="T717">dĭgəttə</ta>
            <ta e="T719" id="Seg_7640" s="T718">dĭ</ta>
            <ta e="T720" id="Seg_7641" s="T719">am-bi</ta>
            <ta e="T721" id="Seg_7642" s="T720">bar</ta>
            <ta e="T722" id="Seg_7643" s="T721">šo-bi</ta>
            <ta e="T723" id="Seg_7644" s="T722">šindi</ta>
            <ta e="T724" id="Seg_7645" s="T723">dĭn</ta>
            <ta e="T725" id="Seg_7646" s="T724">dĭ</ta>
            <ta e="T726" id="Seg_7647" s="T725">skrʼobušok</ta>
            <ta e="T727" id="Seg_7648" s="T726">šo-bi</ta>
            <ta e="T728" id="Seg_7649" s="T727">dĭgəttə</ta>
            <ta e="T729" id="Seg_7650" s="T728">ertə-n</ta>
            <ta e="T730" id="Seg_7651" s="T729">uʔbdə-bi-ʔi</ta>
            <ta e="T731" id="Seg_7652" s="T730">dĭ</ta>
            <ta e="T732" id="Seg_7653" s="T731">ĭzem-nuʔ-bi</ta>
            <ta e="T733" id="Seg_7654" s="T732">măn-də</ta>
            <ta e="T734" id="Seg_7655" s="T733">de-ʔ</ta>
            <ta e="T735" id="Seg_7656" s="T734">măna</ta>
            <ta e="T736" id="Seg_7657" s="T735">nʼamga-ʔi</ta>
            <ta e="T737" id="Seg_7658" s="T736">dĭ</ta>
            <ta e="T738" id="Seg_7659" s="T737">kam-bi</ta>
            <ta e="T739" id="Seg_7660" s="T738">da</ta>
            <ta e="T740" id="Seg_7661" s="T739">naga</ta>
            <ta e="T741" id="Seg_7662" s="T740">šindi=də</ta>
            <ta e="T742" id="Seg_7663" s="T741">am-nuʔ-pi</ta>
            <ta e="T743" id="Seg_7664" s="T742">da</ta>
            <ta e="T744" id="Seg_7665" s="T743">tăn</ta>
            <ta e="T745" id="Seg_7666" s="T744">am-bia-l</ta>
            <ta e="T746" id="Seg_7667" s="T745">dĭ</ta>
            <ta e="T747" id="Seg_7668" s="T746">bar</ta>
            <ta e="T748" id="Seg_7669" s="T747">ej</ta>
            <ta e="T749" id="Seg_7670" s="T748">măn</ta>
            <ta e="T750" id="Seg_7671" s="T749">ej</ta>
            <ta e="T751" id="Seg_7672" s="T750">am-bia-m</ta>
            <ta e="T752" id="Seg_7673" s="T751">dĭgəttə</ta>
            <ta e="T753" id="Seg_7674" s="T752">davaj</ta>
            <ta e="T754" id="Seg_7675" s="T753">kuja-nə</ta>
            <ta e="T755" id="Seg_7676" s="T754">iʔbə-beʔ</ta>
            <ta e="T756" id="Seg_7677" s="T755">šində</ta>
            <ta e="T757" id="Seg_7678" s="T756">am-bi</ta>
            <ta e="T758" id="Seg_7679" s="T757">dĭn</ta>
            <ta e="T759" id="Seg_7680" s="T758">nʼamga</ta>
            <ta e="T760" id="Seg_7681" s="T759">nana-t</ta>
            <ta e="T761" id="Seg_7682" s="T760">mo-lə-j</ta>
            <ta e="T762" id="Seg_7683" s="T761">dĭgəttə</ta>
            <ta e="T763" id="Seg_7684" s="T762">ku-lia-t</ta>
            <ta e="T764" id="Seg_7685" s="T763">bos-tə</ta>
            <ta e="T765" id="Seg_7686" s="T764">nanə-ndə</ta>
            <ta e="T766" id="Seg_7687" s="T765">nʼamga</ta>
            <ta e="T767" id="Seg_7688" s="T766">i-ge</ta>
            <ta e="T768" id="Seg_7689" s="T767">dĭ</ta>
            <ta e="T769" id="Seg_7690" s="T768">davaj</ta>
            <ta e="T770" id="Seg_7691" s="T769">dĭ</ta>
            <ta e="T771" id="Seg_7692" s="T770">volk-tə</ta>
            <ta e="T772" id="Seg_7693" s="T771">tʼuʔ-sittə</ta>
            <ta e="T773" id="Seg_7694" s="T772">uʔbdə-ʔ</ta>
            <ta e="T774" id="Seg_7695" s="T773">tăn</ta>
            <ta e="T776" id="Seg_7696" s="T775">am-bia-l</ta>
            <ta e="T777" id="Seg_7697" s="T776">dĭgəttə</ta>
            <ta e="T778" id="Seg_7698" s="T777">dĭ</ta>
            <ta e="T779" id="Seg_7699" s="T778">uʔbdə-bi</ta>
            <ta e="T780" id="Seg_7700" s="T779">no</ta>
            <ta e="T781" id="Seg_7701" s="T780">măn</ta>
            <ta e="T782" id="Seg_7702" s="T781">am-bia-m</ta>
            <ta e="T783" id="Seg_7703" s="T782">dĭgəttə</ta>
            <ta e="T784" id="Seg_7704" s="T783">bar</ta>
            <ta e="T785" id="Seg_7705" s="T784">süjö</ta>
            <ta e="T786" id="Seg_7706" s="T785">i</ta>
            <ta e="T787" id="Seg_7707" s="T786">tʼetʼer</ta>
            <ta e="T788" id="Seg_7708" s="T787">amno-ʔjə</ta>
            <ta e="T789" id="Seg_7709" s="T788">kăda</ta>
            <ta e="T790" id="Seg_7710" s="T789">tura</ta>
            <ta e="T791" id="Seg_7711" s="T790">jaʔ-sittə</ta>
            <ta e="T792" id="Seg_7712" s="T791">baltu</ta>
            <ta e="T793" id="Seg_7713" s="T792">naga</ta>
            <ta e="T794" id="Seg_7714" s="T793">šindi</ta>
            <ta e="T795" id="Seg_7715" s="T794">baltu</ta>
            <ta e="T796" id="Seg_7716" s="T795">a-lə-j</ta>
            <ta e="T797" id="Seg_7717" s="T796">da</ta>
            <ta e="T798" id="Seg_7718" s="T797">našto</ta>
            <ta e="T799" id="Seg_7719" s="T798">măna</ta>
            <ta e="T801" id="Seg_7720" s="T800">tura</ta>
            <ta e="T802" id="Seg_7721" s="T801">măn</ta>
            <ta e="T803" id="Seg_7722" s="T802">sĭre-gən</ta>
            <ta e="T804" id="Seg_7723" s="T803">ša-la-m</ta>
            <ta e="T805" id="Seg_7724" s="T804">i</ta>
            <ta e="T806" id="Seg_7725" s="T805">saʔmə-luʔ-pi</ta>
            <ta e="T808" id="Seg_7726" s="T807">sĭre-nə</ta>
            <ta e="T809" id="Seg_7727" s="T808">dĭn</ta>
            <ta e="T810" id="Seg_7728" s="T809">ša-bi</ta>
            <ta e="T811" id="Seg_7729" s="T810">ertə-n</ta>
            <ta e="T812" id="Seg_7730" s="T811">uʔbdə-bi</ta>
            <ta e="T813" id="Seg_7731" s="T812">i</ta>
            <ta e="T814" id="Seg_7732" s="T813">kam-bi</ta>
            <ta e="T815" id="Seg_7733" s="T814">ku-zittə</ta>
            <ta e="T816" id="Seg_7734" s="T815">ele-zeŋ-də</ta>
            <ta e="T817" id="Seg_7735" s="T816">ku-bi</ta>
            <ta e="T818" id="Seg_7736" s="T817">dĭn</ta>
            <ta e="T819" id="Seg_7737" s="T818">sʼar-bi-ʔi</ta>
            <ta e="T820" id="Seg_7738" s="T819">suʔmi-luʔ-pi-ʔi</ta>
            <ta e="T822" id="Seg_7739" s="T821">pa-ʔi-nə</ta>
            <ta e="T823" id="Seg_7740" s="T822">dĭgəttə</ta>
            <ta e="T824" id="Seg_7741" s="T823">bos-kəndə</ta>
            <ta e="T825" id="Seg_7742" s="T824">gnʼozda-ʔi</ta>
            <ta e="T826" id="Seg_7743" s="T825">a-bi-ʔi</ta>
            <ta e="T827" id="Seg_7744" s="T826">munu-ʔi</ta>
            <ta e="T828" id="Seg_7745" s="T827">deʔ-pi-ʔi</ta>
            <ta e="T829" id="Seg_7746" s="T828">es-seŋ-də</ta>
            <ta e="T830" id="Seg_7747" s="T829">mo-bi-ʔi</ta>
            <ta e="T831" id="Seg_7748" s="T830">dĭgəttə</ta>
            <ta e="T832" id="Seg_7749" s="T831">es-seŋ-də</ta>
            <ta e="T833" id="Seg_7750" s="T832">bădə-bi-ʔi</ta>
            <ta e="T834" id="Seg_7751" s="T833">maška-ziʔ</ta>
            <ta e="T835" id="Seg_7752" s="T834">červə-ʔi-ziʔ</ta>
            <ta e="T836" id="Seg_7753" s="T835">dĭgəttə</ta>
            <ta e="T837" id="Seg_7754" s="T836">bazoʔ</ta>
            <ta e="T838" id="Seg_7755" s="T837">našto</ta>
            <ta e="T839" id="Seg_7756" s="T838">miʔnʼibeʔ</ta>
            <ta e="T840" id="Seg_7757" s="T839">maʔ</ta>
            <ta e="T841" id="Seg_7758" s="T840">sĭre-gən</ta>
            <ta e="T842" id="Seg_7759" s="T841">kunol-bi-baʔ</ta>
            <ta e="T843" id="Seg_7760" s="T842">onʼiʔ</ta>
            <ta e="T844" id="Seg_7761" s="T843">nüdʼi</ta>
            <ta e="T845" id="Seg_7762" s="T844">ma-kən</ta>
            <ta e="T846" id="Seg_7763" s="T845">amnol-baʔ</ta>
            <ta e="T847" id="Seg_7764" s="T846">dĭ</ta>
            <ta e="T848" id="Seg_7765" s="T847">bar</ta>
            <ta e="T849" id="Seg_7766" s="T848">vʼezʼdʼe</ta>
            <ta e="T850" id="Seg_7767" s="T849">măndə-r-laʔbə-baʔ</ta>
            <ta e="T851" id="Seg_7768" s="T850">kabarləj</ta>
            <ta e="T852" id="Seg_7769" s="T851">amno-bi-ʔi</ta>
            <ta e="T853" id="Seg_7770" s="T852">nüke</ta>
            <ta e="T854" id="Seg_7771" s="T853">da</ta>
            <ta e="T855" id="Seg_7772" s="T854">büzʼe</ta>
            <ta e="T857" id="Seg_7773" s="T856">dĭ-zeŋ</ta>
            <ta e="T858" id="Seg_7774" s="T857">ĭmbi=də</ta>
            <ta e="T859" id="Seg_7775" s="T858">nago-bi</ta>
            <ta e="T860" id="Seg_7776" s="T859">kam-bi-ʔi</ta>
            <ta e="T861" id="Seg_7777" s="T860">dʼije-nə</ta>
            <ta e="T862" id="Seg_7778" s="T861">šiška-ʔi</ta>
            <ta e="T863" id="Seg_7779" s="T862">žoludʼi</ta>
            <ta e="T864" id="Seg_7780" s="T863">oʔbdo-bi-ʔi</ta>
            <ta e="T865" id="Seg_7781" s="T864">šo-bi-ʔi</ta>
            <ta e="T866" id="Seg_7782" s="T865">am-naʔbə</ta>
            <ta e="T867" id="Seg_7783" s="T866">bar</ta>
            <ta e="T868" id="Seg_7784" s="T867">onʼiʔ</ta>
            <ta e="T869" id="Seg_7785" s="T868">saʔmə-luʔ-pi</ta>
            <ta e="T870" id="Seg_7786" s="T869">patpolʼlʼa-gən</ta>
            <ta e="T871" id="Seg_7787" s="T870">dĭgəttə</ta>
            <ta e="T872" id="Seg_7788" s="T871">davaj</ta>
            <ta e="T873" id="Seg_7789" s="T872">özer-zittə</ta>
            <ta e="T875" id="Seg_7790" s="T874">nüke</ta>
            <ta e="T876" id="Seg_7791" s="T875">măn-də</ta>
            <ta e="T877" id="Seg_7792" s="T876">baltu-zʼiʔ</ta>
            <ta e="T878" id="Seg_7793" s="T877">jaʔ-də</ta>
            <ta e="T879" id="Seg_7794" s="T878">pol-də</ta>
            <ta e="T880" id="Seg_7795" s="T879">dĭ</ta>
            <ta e="T881" id="Seg_7796" s="T880">jaʔ-pi</ta>
            <ta e="T882" id="Seg_7797" s="T881">dĭ</ta>
            <ta e="T883" id="Seg_7798" s="T882">döbər</ta>
            <ta e="T884" id="Seg_7799" s="T883">özer-bi</ta>
            <ta e="T885" id="Seg_7800" s="T884">tuj</ta>
            <ta e="T886" id="Seg_7801" s="T885">dĭn</ta>
            <ta e="T887" id="Seg_7802" s="T886">nʼuʔdə</ta>
            <ta e="T888" id="Seg_7803" s="T887">bar</ta>
            <ta e="T889" id="Seg_7804" s="T888">oʔbdo</ta>
            <ta e="T890" id="Seg_7805" s="T889">dĭ</ta>
            <ta e="T891" id="Seg_7806" s="T890">dibər</ta>
            <ta e="T892" id="Seg_7807" s="T891">krɨša-bə</ta>
            <ta e="T894" id="Seg_7808" s="T893">dibər</ta>
            <ta e="T895" id="Seg_7809" s="T894">kuŋge-ŋ</ta>
            <ta e="T896" id="Seg_7810" s="T895">özer-bi</ta>
            <ta e="T897" id="Seg_7811" s="T896">daže</ta>
            <ta e="T898" id="Seg_7812" s="T897">nʼuʔdə</ta>
            <ta e="T899" id="Seg_7813" s="T898">dĭgəttə</ta>
            <ta e="T900" id="Seg_7814" s="T899">büzʼe</ta>
            <ta e="T901" id="Seg_7815" s="T900">nada</ta>
            <ta e="T902" id="Seg_7816" s="T901">kan-zittə</ta>
            <ta e="T903" id="Seg_7817" s="T902">oʔbdə-sʼtə</ta>
            <ta e="T904" id="Seg_7818" s="T903">žoludʼi</ta>
            <ta e="T905" id="Seg_7819" s="T904">sʼa-bi</ta>
            <ta e="T906" id="Seg_7820" s="T905">sʼa-bi</ta>
            <ta e="T908" id="Seg_7821" s="T907">dibər</ta>
            <ta e="T909" id="Seg_7822" s="T908">nʼuʔdə</ta>
            <ta e="T910" id="Seg_7823" s="T909">dĭn</ta>
            <ta e="T911" id="Seg_7824" s="T910">ku-laʔbə</ta>
            <ta e="T912" id="Seg_7825" s="T911">tura</ta>
            <ta e="T914" id="Seg_7826" s="T913">nu-ga</ta>
            <ta e="T915" id="Seg_7827" s="T914">dibər</ta>
            <ta e="T916" id="Seg_7828" s="T915">tura-nə</ta>
            <ta e="T917" id="Seg_7829" s="T916">šo-bi</ta>
            <ta e="T918" id="Seg_7830" s="T917">dĭn</ta>
            <ta e="T919" id="Seg_7831" s="T918">kurizə-n</ta>
            <ta e="T920" id="Seg_7832" s="T919">tibi</ta>
            <ta e="T921" id="Seg_7833" s="T920">amno-laʔbə</ta>
            <ta e="T922" id="Seg_7834" s="T921">ulu-t</ta>
            <ta e="T923" id="Seg_7835" s="T922">kuvas</ta>
            <ta e="T924" id="Seg_7836" s="T923">komu</ta>
            <ta e="T925" id="Seg_7837" s="T924">bar</ta>
            <ta e="T926" id="Seg_7838" s="T925">i</ta>
            <ta e="T927" id="Seg_7839" s="T926">dĭn</ta>
            <ta e="T928" id="Seg_7840" s="T927">žernovka-zaŋ-də</ta>
            <ta e="T929" id="Seg_7841" s="T928">i-ge</ta>
            <ta e="T930" id="Seg_7842" s="T929">dĭ</ta>
            <ta e="T931" id="Seg_7843" s="T930">i-bi</ta>
            <ta e="T932" id="Seg_7844" s="T931">dĭ-m</ta>
            <ta e="T933" id="Seg_7845" s="T932">i</ta>
            <ta e="T934" id="Seg_7846" s="T933">šo-bi</ta>
            <ta e="T935" id="Seg_7847" s="T934">na</ta>
            <ta e="T936" id="Seg_7848" s="T935">tănan</ta>
            <ta e="T937" id="Seg_7849" s="T936">kurizə-n</ta>
            <ta e="T938" id="Seg_7850" s="T937">tibi</ta>
            <ta e="T939" id="Seg_7851" s="T938">komu</ta>
            <ta e="T940" id="Seg_7852" s="T939">ulu-t</ta>
            <ta e="T941" id="Seg_7853" s="T940">dĭ</ta>
            <ta e="T942" id="Seg_7854" s="T941">i-bi</ta>
            <ta e="T943" id="Seg_7855" s="T942">da</ta>
            <ta e="T944" id="Seg_7856" s="T943">davaj</ta>
            <ta e="T945" id="Seg_7857" s="T944">žernovka</ta>
            <ta e="T946" id="Seg_7858" s="T945">dĭ</ta>
            <ta e="T947" id="Seg_7859" s="T946">tʼermən-də</ta>
            <ta e="T948" id="Seg_7860" s="T947">kur-zittə</ta>
            <ta e="T949" id="Seg_7861" s="T948">dĭn</ta>
            <ta e="T950" id="Seg_7862" s="T949">blin-ʔi</ta>
            <ta e="T951" id="Seg_7863" s="T950">pirog-əʔi</ta>
            <ta e="T952" id="Seg_7864" s="T951">blin-ʔi</ta>
            <ta e="T953" id="Seg_7865" s="T952">pirog-əʔi</ta>
            <ta e="T954" id="Seg_7866" s="T953">büzʼe</ta>
            <ta e="T955" id="Seg_7867" s="T954">am-bi</ta>
            <ta e="T956" id="Seg_7868" s="T955">i</ta>
            <ta e="T957" id="Seg_7869" s="T956">nüke</ta>
            <ta e="T958" id="Seg_7870" s="T957">am-bi</ta>
            <ta e="T959" id="Seg_7871" s="T958">uge</ta>
            <ta e="T960" id="Seg_7872" s="T959">dăra</ta>
            <ta e="T961" id="Seg_7873" s="T960">amno-laʔ</ta>
            <ta e="T962" id="Seg_7874" s="T961">dĭgəttə</ta>
            <ta e="T963" id="Seg_7875" s="T962">šo-bi</ta>
            <ta e="T964" id="Seg_7876" s="T963">dĭ-zeŋ-də</ta>
            <ta e="T965" id="Seg_7877" s="T964">barin</ta>
            <ta e="T966" id="Seg_7878" s="T965">deʔ-keʔ</ta>
            <ta e="T967" id="Seg_7879" s="T966">măna</ta>
            <ta e="T968" id="Seg_7880" s="T967">am-zittə</ta>
            <ta e="T969" id="Seg_7881" s="T968">dĭ</ta>
            <ta e="T970" id="Seg_7882" s="T969">nünə-bi</ta>
            <ta e="T971" id="Seg_7883" s="T970">pravda</ta>
            <ta e="T972" id="Seg_7884" s="T971">dĭ</ta>
            <ta e="T974" id="Seg_7885" s="T973">dĭ</ta>
            <ta e="T976" id="Seg_7886" s="T975">tʼermən</ta>
            <ta e="T977" id="Seg_7887" s="T976">dĭgəttə</ta>
            <ta e="T978" id="Seg_7888" s="T977">nüke-t</ta>
            <ta e="T979" id="Seg_7889" s="T978">bar</ta>
            <ta e="T980" id="Seg_7890" s="T979">davaj</ta>
            <ta e="T981" id="Seg_7891" s="T980">tʼermən-də</ta>
            <ta e="T982" id="Seg_7892" s="T981">kur-zittə</ta>
            <ta e="T983" id="Seg_7893" s="T982">blin-ʔi</ta>
            <ta e="T984" id="Seg_7894" s="T983">pirog-əʔi</ta>
            <ta e="T985" id="Seg_7895" s="T984">dĭ</ta>
            <ta e="T986" id="Seg_7896" s="T985">am-bi</ta>
            <ta e="T987" id="Seg_7897" s="T986">sadar-gaʔ</ta>
            <ta e="T988" id="Seg_7898" s="T987">măna</ta>
            <ta e="T989" id="Seg_7899" s="T988">dʼok</ta>
            <ta e="T990" id="Seg_7900" s="T989">miʔ</ta>
            <ta e="T991" id="Seg_7901" s="T990">ej</ta>
            <ta e="T992" id="Seg_7902" s="T991">sadar-la-m</ta>
            <ta e="T993" id="Seg_7903" s="T992">ato</ta>
            <ta e="T995" id="Seg_7904" s="T994">miʔnʼibeʔ</ta>
            <ta e="T996" id="Seg_7905" s="T995">am-zittə</ta>
            <ta e="T997" id="Seg_7906" s="T996">ĭmbi</ta>
            <ta e="T998" id="Seg_7907" s="T997">dĭgəttə</ta>
            <ta e="T999" id="Seg_7908" s="T998">dĭ</ta>
            <ta e="T1000" id="Seg_7909" s="T999">nüdʼi-n</ta>
            <ta e="T1001" id="Seg_7910" s="T1000">šo-bi</ta>
            <ta e="T1002" id="Seg_7911" s="T1001">i</ta>
            <ta e="T1003" id="Seg_7912" s="T1002">tojir-laʔ</ta>
            <ta e="T1004" id="Seg_7913" s="T1003">i-luʔ-pi</ta>
            <ta e="T1005" id="Seg_7914" s="T1004">i</ta>
            <ta e="T1006" id="Seg_7915" s="T1005">kon-naːm-bi</ta>
            <ta e="T1007" id="Seg_7916" s="T1006">ertə-n</ta>
            <ta e="T1008" id="Seg_7917" s="T1007">dĭ-zeŋ</ta>
            <ta e="T1009" id="Seg_7918" s="T1008">uʔbdə-bi-ʔi</ta>
            <ta e="T1010" id="Seg_7919" s="T1009">naga</ta>
            <ta e="T1011" id="Seg_7920" s="T1010">tʼermən-də</ta>
            <ta e="T1012" id="Seg_7921" s="T1011">dĭgəttə</ta>
            <ta e="T1013" id="Seg_7922" s="T1012">tibi</ta>
            <ta e="T1014" id="Seg_7923" s="T1013">kuriz</ta>
            <ta e="T1015" id="Seg_7924" s="T1014">i-ʔ</ta>
            <ta e="T1016" id="Seg_7925" s="T1015">dʼor-gaʔ</ta>
            <ta e="T1017" id="Seg_7926" s="T1016">măn</ta>
            <ta e="T1018" id="Seg_7927" s="T1017">kal-la-m</ta>
            <ta e="T1020" id="Seg_7928" s="T1019">det-li-m</ta>
            <ta e="T1021" id="Seg_7929" s="T1020">bazoʔ</ta>
            <ta e="T1022" id="Seg_7930" s="T1021">šiʔnʼileʔ</ta>
            <ta e="T1023" id="Seg_7931" s="T1022">kam-bi</ta>
            <ta e="T1024" id="Seg_7932" s="T1023">kandə-ga</ta>
            <ta e="T1025" id="Seg_7933" s="T1024">kandə-ga</ta>
            <ta e="T1026" id="Seg_7934" s="T1025">šonə-ga</ta>
            <ta e="T1027" id="Seg_7935" s="T1026">diʔ-nə</ta>
            <ta e="T1028" id="Seg_7936" s="T1027">lʼisʼitsa</ta>
            <ta e="T1029" id="Seg_7937" s="T1028">gibər</ta>
            <ta e="T1030" id="Seg_7938" s="T1029">kandə-ga-l</ta>
            <ta e="T1031" id="Seg_7939" s="T1030">i-ʔ</ta>
            <ta e="T1032" id="Seg_7940" s="T1031">măna</ta>
            <ta e="T1033" id="Seg_7941" s="T1032">băd-a-ʔ</ta>
            <ta e="T1034" id="Seg_7942" s="T1033">măna</ta>
            <ta e="T1035" id="Seg_7943" s="T1034">šüjö-ndə</ta>
            <ta e="T1036" id="Seg_7944" s="T1035">dĭ</ta>
            <ta e="T1037" id="Seg_7945" s="T1036">băʔ-pi</ta>
            <ta e="T1038" id="Seg_7946" s="T1037">dĭgəttə</ta>
            <ta e="T1039" id="Seg_7947" s="T1038">šonə-ga</ta>
            <ta e="T1040" id="Seg_7948" s="T1039">šonə-ga</ta>
            <ta e="T1041" id="Seg_7949" s="T1040">volk-də</ta>
            <ta e="T1042" id="Seg_7950" s="T1041">gibər</ta>
            <ta e="T1043" id="Seg_7951" s="T1042">kandə-ga-l</ta>
            <ta e="T1044" id="Seg_7952" s="T1043">tʼermən-də</ta>
            <ta e="T1045" id="Seg_7953" s="T1044">i-zittə</ta>
            <ta e="T1047" id="Seg_7954" s="T1046">i-ʔ</ta>
            <ta e="T1048" id="Seg_7955" s="T1047">măna</ta>
            <ta e="T1049" id="Seg_7956" s="T1048">băd-a-ʔ</ta>
            <ta e="T1050" id="Seg_7957" s="T1049">măna</ta>
            <ta e="T1051" id="Seg_7958" s="T1050">šüjö-ndə</ta>
            <ta e="T1052" id="Seg_7959" s="T1051">dĭ</ta>
            <ta e="T1053" id="Seg_7960" s="T1052">băʔ-pi</ta>
            <ta e="T1054" id="Seg_7961" s="T1053">dĭgəttə</ta>
            <ta e="T1055" id="Seg_7962" s="T1054">šonə-ga</ta>
            <ta e="T1056" id="Seg_7963" s="T1055">urgaːba</ta>
            <ta e="T1057" id="Seg_7964" s="T1056">šonə-ga</ta>
            <ta e="T1058" id="Seg_7965" s="T1057">băd-a-ʔ</ta>
            <ta e="T1059" id="Seg_7966" s="T1058">măna</ta>
            <ta e="T1060" id="Seg_7967" s="T1059">dĭgəttə</ta>
            <ta e="T1061" id="Seg_7968" s="T1060">šo-bi</ta>
            <ta e="T1062" id="Seg_7969" s="T1061">davaj</ta>
            <ta e="T1063" id="Seg_7970" s="T1062">kirgar-zittə</ta>
            <ta e="T1065" id="Seg_7971" s="T1064">det-tə</ta>
            <ta e="T1066" id="Seg_7972" s="T1065">măn</ta>
            <ta e="T1067" id="Seg_7973" s="T1066">tʼermən-də</ta>
            <ta e="T1068" id="Seg_7974" s="T1067">det-tə</ta>
            <ta e="T1069" id="Seg_7975" s="T1068">măn</ta>
            <ta e="T1070" id="Seg_7976" s="T1069">tʼermən-də</ta>
            <ta e="T1071" id="Seg_7977" s="T1070">dĭ</ta>
            <ta e="T1072" id="Seg_7978" s="T1071">măn-də</ta>
            <ta e="T1074" id="Seg_7979" s="T1073">baʔ-gaʔ</ta>
            <ta e="T1075" id="Seg_7980" s="T1074">dĭ-m</ta>
            <ta e="T1076" id="Seg_7981" s="T1075">a</ta>
            <ta e="T1077" id="Seg_7982" s="T1076">nabə-ʔi-nə</ta>
            <ta e="T1078" id="Seg_7983" s="T1077">pušaj</ta>
            <ta e="T1079" id="Seg_7984" s="T1078">nabə-ʔi</ta>
            <ta e="T1080" id="Seg_7985" s="T1079">am-nuʔ-bə</ta>
            <ta e="T1081" id="Seg_7986" s="T1080">lə-ʔi</ta>
            <ta e="T1082" id="Seg_7987" s="T1081">dĭ-m</ta>
            <ta e="T1083" id="Seg_7988" s="T1082">baʔ-luʔ-pi</ta>
            <ta e="T1084" id="Seg_7989" s="T1083">a</ta>
            <ta e="T1085" id="Seg_7990" s="T1084">dĭ</ta>
            <ta e="T1086" id="Seg_7991" s="T1085">măn-də</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PKZ">
            <ta e="T5" id="Seg_7992" s="T4">amno-bi</ta>
            <ta e="T6" id="Seg_7993" s="T5">büzʼe</ta>
            <ta e="T7" id="Seg_7994" s="T6">dĭ-n</ta>
            <ta e="T8" id="Seg_7995" s="T7">nagur</ta>
            <ta e="T9" id="Seg_7996" s="T8">nʼi</ta>
            <ta e="T10" id="Seg_7997" s="T9">i-bi</ta>
            <ta e="T11" id="Seg_7998" s="T10">onʼiʔ</ta>
            <ta e="T12" id="Seg_7999" s="T11">Gavrila</ta>
            <ta e="T13" id="Seg_8000" s="T12">onʼiʔ</ta>
            <ta e="T14" id="Seg_8001" s="T13">Danʼila</ta>
            <ta e="T15" id="Seg_8002" s="T14">onʼiʔ</ta>
            <ta e="T16" id="Seg_8003" s="T15">Vanʼuška</ta>
            <ta e="T17" id="Seg_8004" s="T16">duračok</ta>
            <ta e="T18" id="Seg_8005" s="T17">dĭgəttə</ta>
            <ta e="T20" id="Seg_8006" s="T19">dĭ</ta>
            <ta e="T21" id="Seg_8007" s="T20">büzʼe</ta>
            <ta e="T22" id="Seg_8008" s="T21">kuʔ-laʔpi</ta>
            <ta e="T23" id="Seg_8009" s="T22">budəj</ta>
            <ta e="T24" id="Seg_8010" s="T23">i</ta>
            <ta e="T25" id="Seg_8011" s="T24">dĭ</ta>
            <ta e="T26" id="Seg_8012" s="T25">šində=də</ta>
            <ta e="T29" id="Seg_8013" s="T28">nüdʼi-n</ta>
            <ta e="T30" id="Seg_8014" s="T29">šo-luʔbdə-jəʔ</ta>
            <ta e="T31" id="Seg_8015" s="T30">da</ta>
            <ta e="T32" id="Seg_8016" s="T31">am-laʔbə</ta>
            <ta e="T33" id="Seg_8017" s="T32">budəj</ta>
            <ta e="T34" id="Seg_8018" s="T33">nadə</ta>
            <ta e="T35" id="Seg_8019" s="T34">măn-ntə</ta>
            <ta e="T36" id="Seg_8020" s="T35">kan-zittə</ta>
            <ta e="T38" id="Seg_8021" s="T37">măndo-zittə</ta>
            <ta e="T39" id="Seg_8022" s="T38">šində</ta>
            <ta e="T40" id="Seg_8023" s="T39">am-liA-t</ta>
            <ta e="T41" id="Seg_8024" s="T40">budəj</ta>
            <ta e="T42" id="Seg_8025" s="T41">nadə</ta>
            <ta e="T43" id="Seg_8026" s="T42">dʼabə-zittə</ta>
            <ta e="T44" id="Seg_8027" s="T43">dĭgəttə</ta>
            <ta e="T45" id="Seg_8028" s="T44">kan-bi</ta>
            <ta e="T46" id="Seg_8029" s="T45">staršij</ta>
            <ta e="T47" id="Seg_8030" s="T46">nʼi-t</ta>
            <ta e="T48" id="Seg_8031" s="T47">Gavrila</ta>
            <ta e="T49" id="Seg_8032" s="T48">dĭbər</ta>
            <ta e="T50" id="Seg_8033" s="T49">kan-bi</ta>
            <ta e="T51" id="Seg_8034" s="T50">kunol-bi</ta>
            <ta e="T52" id="Seg_8035" s="T51">noʔ-Kən</ta>
            <ta e="T53" id="Seg_8036" s="T52">i</ta>
            <ta e="T54" id="Seg_8037" s="T53">šo-bi</ta>
            <ta e="T55" id="Seg_8038" s="T54">maʔ-Tə</ta>
            <ta e="T56" id="Seg_8039" s="T55">amno-bi-m</ta>
            <ta e="T57" id="Seg_8040" s="T56">ej</ta>
            <ta e="T58" id="Seg_8041" s="T57">kunol-bi-m</ta>
            <ta e="T59" id="Seg_8042" s="T58">a</ta>
            <ta e="T60" id="Seg_8043" s="T59">šində=də</ta>
            <ta e="T61" id="Seg_8044" s="T60">ej</ta>
            <ta e="T62" id="Seg_8045" s="T61">ku-bi-m</ta>
            <ta e="T63" id="Seg_8046" s="T62">dĭgəttə</ta>
            <ta e="T64" id="Seg_8047" s="T63">baška</ta>
            <ta e="T65" id="Seg_8048" s="T64">nʼi-t</ta>
            <ta e="T66" id="Seg_8049" s="T65">kandə-gA</ta>
            <ta e="T67" id="Seg_8050" s="T66">Danʼila-t</ta>
            <ta e="T68" id="Seg_8051" s="T67">kan-bi</ta>
            <ta e="T69" id="Seg_8052" s="T68">kunol-bi</ta>
            <ta e="T70" id="Seg_8053" s="T69">noʔ-Kən</ta>
            <ta e="T71" id="Seg_8054" s="T70">dĭgəttə</ta>
            <ta e="T72" id="Seg_8055" s="T71">ertə-n</ta>
            <ta e="T73" id="Seg_8056" s="T72">šo-bi</ta>
            <ta e="T74" id="Seg_8057" s="T73">bar</ta>
            <ta e="T75" id="Seg_8058" s="T74">nüdʼi</ta>
            <ta e="T76" id="Seg_8059" s="T75">ej</ta>
            <ta e="T77" id="Seg_8060" s="T76">kunol-bi-m</ta>
            <ta e="T78" id="Seg_8061" s="T77">a</ta>
            <ta e="T79" id="Seg_8062" s="T78">šində-m=də</ta>
            <ta e="T80" id="Seg_8063" s="T79">ej</ta>
            <ta e="T81" id="Seg_8064" s="T80">ku-bi-m</ta>
            <ta e="T82" id="Seg_8065" s="T81">dĭgəttə</ta>
            <ta e="T83" id="Seg_8066" s="T82">Vanʼuška</ta>
            <ta e="T84" id="Seg_8067" s="T83">kan-bi</ta>
            <ta e="T85" id="Seg_8068" s="T84">i-bi</ta>
            <ta e="T86" id="Seg_8069" s="T85">arkan-də</ta>
            <ta e="T87" id="Seg_8070" s="T86">dĭgəttə</ta>
            <ta e="T88" id="Seg_8071" s="T87">amno-bi</ta>
            <ta e="T89" id="Seg_8072" s="T88">pi-Tə</ta>
            <ta e="T90" id="Seg_8073" s="T89">ej</ta>
            <ta e="T91" id="Seg_8074" s="T90">kunol-liA</ta>
            <ta e="T92" id="Seg_8075" s="T91">amno-laʔbə</ta>
            <ta e="T93" id="Seg_8076" s="T92">ku-liA-t</ta>
            <ta e="T94" id="Seg_8077" s="T93">ine</ta>
            <ta e="T95" id="Seg_8078" s="T94">šo-bi</ta>
            <ta e="T96" id="Seg_8079" s="T95">vsʼaka</ta>
            <ta e="T97" id="Seg_8080" s="T96">i</ta>
            <ta e="T98" id="Seg_8081" s="T97">bar</ta>
            <ta e="T99" id="Seg_8082" s="T98">dĭn</ta>
            <ta e="T100" id="Seg_8083" s="T99">kuba-Kən</ta>
            <ta e="T101" id="Seg_8084" s="T100">tor-də</ta>
            <ta e="T102" id="Seg_8085" s="T101">kuvas</ta>
            <ta e="T103" id="Seg_8086" s="T102">dĭ</ta>
            <ta e="T104" id="Seg_8087" s="T103">bar</ta>
            <ta e="T105" id="Seg_8088" s="T104">baʔbdə-luʔbdə-bi</ta>
            <ta e="T106" id="Seg_8089" s="T105">arkan-də</ta>
            <ta e="T107" id="Seg_8090" s="T106">dĭ-Tə</ta>
            <ta e="T108" id="Seg_8091" s="T107">i</ta>
            <ta e="T109" id="Seg_8092" s="T108">dʼabə-luʔbdə-bi</ta>
            <ta e="T110" id="Seg_8093" s="T109">dĭ</ta>
            <ta e="T111" id="Seg_8094" s="T110">süʔmə-luʔbdə-bi</ta>
            <ta e="T112" id="Seg_8095" s="T111">ej</ta>
            <ta e="T113" id="Seg_8096" s="T112">mo-liA</ta>
            <ta e="T114" id="Seg_8097" s="T113">süʔmə-zittə</ta>
            <ta e="T115" id="Seg_8098" s="T114">dĭgəttə</ta>
            <ta e="T116" id="Seg_8099" s="T115">öʔ-ʔ</ta>
            <ta e="T117" id="Seg_8100" s="T116">măna</ta>
            <ta e="T118" id="Seg_8101" s="T117">măn</ta>
            <ta e="T119" id="Seg_8102" s="T118">tănan</ta>
            <ta e="T120" id="Seg_8103" s="T119">bar</ta>
            <ta e="T121" id="Seg_8104" s="T120">ĭmbi</ta>
            <ta e="T122" id="Seg_8105" s="T121">mĭ-lV-m</ta>
            <ta e="T123" id="Seg_8106" s="T122">no</ta>
            <ta e="T124" id="Seg_8107" s="T123">kădaʔ</ta>
            <ta e="T125" id="Seg_8108" s="T124">nörbə-ʔ</ta>
            <ta e="T126" id="Seg_8109" s="T125">daška</ta>
            <ta e="T127" id="Seg_8110" s="T126">ej</ta>
            <ta e="T128" id="Seg_8111" s="T127">am-liA-l</ta>
            <ta e="T129" id="Seg_8112" s="T128">budəj</ta>
            <ta e="T130" id="Seg_8113" s="T129">ej</ta>
            <ta e="T131" id="Seg_8114" s="T130">am-lV-m</ta>
            <ta e="T132" id="Seg_8115" s="T131">a</ta>
            <ta e="T133" id="Seg_8116" s="T132">kădaʔ</ta>
            <ta e="T134" id="Seg_8117" s="T133">šo-lV-l</ta>
            <ta e="T135" id="Seg_8118" s="T134">dărəʔ</ta>
            <ta e="T136" id="Seg_8119" s="T135">măna</ta>
            <ta e="T137" id="Seg_8120" s="T136">šo-ʔ</ta>
            <ta e="T138" id="Seg_8121" s="T137">döbər</ta>
            <ta e="T139" id="Seg_8122" s="T138">ənu-ʔ</ta>
            <ta e="T141" id="Seg_8123" s="T140">măna</ta>
            <ta e="T142" id="Seg_8124" s="T141">no</ta>
            <ta e="T143" id="Seg_8125" s="T142">dĭrgit</ta>
            <ta e="T144" id="Seg_8126" s="T143">i</ta>
            <ta e="T145" id="Seg_8127" s="T144">kan-lAʔ</ta>
            <ta e="T146" id="Seg_8128" s="T145">tʼür-bi</ta>
            <ta e="T147" id="Seg_8129" s="T146">ine-t</ta>
            <ta e="T148" id="Seg_8130" s="T147">a</ta>
            <ta e="T149" id="Seg_8131" s="T148">dĭ</ta>
            <ta e="T150" id="Seg_8132" s="T149">šo-bi</ta>
            <ta e="T151" id="Seg_8133" s="T150">maʔ-gəndə</ta>
            <ta e="T152" id="Seg_8134" s="T151">pʼeːš-Tə</ta>
            <ta e="T153" id="Seg_8135" s="T152">šer-luʔbdə-bi</ta>
            <ta e="T154" id="Seg_8136" s="T153">măn</ta>
            <ta e="T155" id="Seg_8137" s="T154">dʼabə-bi-m</ta>
            <ta e="T156" id="Seg_8138" s="T155">ine</ta>
            <ta e="T157" id="Seg_8139" s="T156">dĭn</ta>
            <ta e="T158" id="Seg_8140" s="T157">tüj</ta>
            <ta e="T159" id="Seg_8141" s="T158">kamən=də</ta>
            <ta e="T160" id="Seg_8142" s="T159">ej</ta>
            <ta e="T161" id="Seg_8143" s="T160">šo-lV-j</ta>
            <ta e="T162" id="Seg_8144" s="T161">am-zittə</ta>
            <ta e="T163" id="Seg_8145" s="T162">budəj</ta>
            <ta e="T164" id="Seg_8146" s="T163">dĭgəttə</ta>
            <ta e="T165" id="Seg_8147" s="T164">koŋ</ta>
            <ta e="T166" id="Seg_8148" s="T165">a-bi</ta>
            <ta e="T167" id="Seg_8149" s="T166">štobɨ</ta>
            <ta e="T168" id="Seg_8150" s="T167">il</ta>
            <ta e="T169" id="Seg_8151" s="T168">šo-bi-jəʔ</ta>
            <ta e="T171" id="Seg_8152" s="T170">dĭ-n</ta>
            <ta e="T172" id="Seg_8153" s="T171">koʔbdo</ta>
            <ta e="T173" id="Seg_8154" s="T172">amno-laʔbə</ta>
            <ta e="T174" id="Seg_8155" s="T173">tʼerem-Kən</ta>
            <ta e="T175" id="Seg_8156" s="T174">bar</ta>
            <ta e="T176" id="Seg_8157" s="T175">il</ta>
            <ta e="T177" id="Seg_8158" s="T176">kan-laʔbə-jəʔ</ta>
            <ta e="T178" id="Seg_8159" s="T177">i</ta>
            <ta e="T179" id="Seg_8160" s="T178">i</ta>
            <ta e="T180" id="Seg_8161" s="T179">dĭ-n</ta>
            <ta e="T181" id="Seg_8162" s="T180">kaga-jəʔ</ta>
            <ta e="T182" id="Seg_8163" s="T181">kan-ntə-gA-jəʔ</ta>
            <ta e="T183" id="Seg_8164" s="T182">a</ta>
            <ta e="T184" id="Seg_8165" s="T183">dĭ</ta>
            <ta e="T185" id="Seg_8166" s="T184">măn-ntə</ta>
            <ta e="T186" id="Seg_8167" s="T185">i-KAʔ</ta>
            <ta e="T187" id="Seg_8168" s="T186">măna</ta>
            <ta e="T188" id="Seg_8169" s="T187">gibər</ta>
            <ta e="T189" id="Seg_8170" s="T188">tăn</ta>
            <ta e="T190" id="Seg_8171" s="T189">kan-lV-l</ta>
            <ta e="T191" id="Seg_8172" s="T190">ĭmbi=də</ta>
            <ta e="T192" id="Seg_8173" s="T191">dĭn</ta>
            <ta e="T193" id="Seg_8174" s="T192">ej</ta>
            <ta e="T194" id="Seg_8175" s="T193">ku-bi-l</ta>
            <ta e="T195" id="Seg_8176" s="T194">štobɨ</ta>
            <ta e="T196" id="Seg_8177" s="T195">il</ta>
            <ta e="T197" id="Seg_8178" s="T196">püšte-r-bi-jəʔ</ta>
            <ta e="T199" id="Seg_8179" s="T198">tănan</ta>
            <ta e="T200" id="Seg_8180" s="T199">dĭ-zAŋ</ta>
            <ta e="T201" id="Seg_8181" s="T200">kan-lAʔ</ta>
            <ta e="T202" id="Seg_8182" s="T201">tʼür-bi-jəʔ</ta>
            <ta e="T203" id="Seg_8183" s="T202">dĭ</ta>
            <ta e="T204" id="Seg_8184" s="T203">uʔbdə-bi</ta>
            <ta e="T205" id="Seg_8185" s="T204">karzʼina</ta>
            <ta e="T206" id="Seg_8186" s="T205">i-bi</ta>
            <ta e="T207" id="Seg_8187" s="T206">kan-bi</ta>
            <ta e="T208" id="Seg_8188" s="T207">i</ta>
            <ta e="T209" id="Seg_8189" s="T208">davaj</ta>
            <ta e="T210" id="Seg_8190" s="T209">kirgaːr-zittə</ta>
            <ta e="T211" id="Seg_8191" s="T210">ine-t</ta>
            <ta e="T212" id="Seg_8192" s="T211">šo-bi</ta>
            <ta e="T213" id="Seg_8193" s="T212">bar</ta>
            <ta e="T214" id="Seg_8194" s="T213">döbər</ta>
            <ta e="T215" id="Seg_8195" s="T214">modria-Kən</ta>
            <ta e="T216" id="Seg_8196" s="T215">šonə-gA-jəʔ</ta>
            <ta e="T217" id="Seg_8197" s="T216">šö</ta>
            <ta e="T218" id="Seg_8198" s="T217">kuza-gəndə</ta>
            <ta e="T219" id="Seg_8199" s="T218">šonə-gA</ta>
            <ta e="T220" id="Seg_8200" s="T219">dĭgəttə</ta>
            <ta e="T221" id="Seg_8201" s="T220">dĭ</ta>
            <ta e="T222" id="Seg_8202" s="T221">ine</ta>
            <ta e="T224" id="Seg_8203" s="T222">măn-ntə</ta>
            <ta e="T225" id="Seg_8204" s="T224">nu-bi</ta>
            <ta e="T226" id="Seg_8205" s="T225">dĭ-Tə</ta>
            <ta e="T227" id="Seg_8206" s="T226">măn-ntə</ta>
            <ta e="T228" id="Seg_8207" s="T227">onʼiʔ</ta>
            <ta e="T229" id="Seg_8208" s="T228">ku-Tə</ta>
            <ta e="T230" id="Seg_8209" s="T229">paʔ-KAʔ</ta>
            <ta e="T231" id="Seg_8210" s="T230">onʼiʔ-gəʔ</ta>
            <ta e="T232" id="Seg_8211" s="T231">supso-ʔ</ta>
            <ta e="T233" id="Seg_8212" s="T232">dĭ</ta>
            <ta e="T234" id="Seg_8213" s="T233">paʔ-bi</ta>
            <ta e="T235" id="Seg_8214" s="T234">supso-bi</ta>
            <ta e="T236" id="Seg_8215" s="T235">kuvas</ta>
            <ta e="T237" id="Seg_8216" s="T236">mo-laːm-bi</ta>
            <ta e="T238" id="Seg_8217" s="T237">dĭgəttə</ta>
            <ta e="T239" id="Seg_8218" s="T238">dĭ-Tə</ta>
            <ta e="T240" id="Seg_8219" s="T239">amno-bi</ta>
            <ta e="T241" id="Seg_8220" s="T240">dĭ</ta>
            <ta e="T242" id="Seg_8221" s="T241">nuʔmə</ta>
            <ta e="T243" id="Seg_8222" s="T242">nuʔmə-luʔbdə-bi</ta>
            <ta e="T244" id="Seg_8223" s="T243">dĭbər</ta>
            <ta e="T245" id="Seg_8224" s="T244">il</ta>
            <ta e="T246" id="Seg_8225" s="T245">iʔgö</ta>
            <ta e="T247" id="Seg_8226" s="T246">nu-gA-jəʔ</ta>
            <ta e="T248" id="Seg_8227" s="T247">a</ta>
            <ta e="T249" id="Seg_8228" s="T248">šində=də</ta>
            <ta e="T250" id="Seg_8229" s="T249">ej</ta>
            <ta e="T251" id="Seg_8230" s="T250">dĭ</ta>
            <ta e="T252" id="Seg_8231" s="T251">koʔbdo-nə</ta>
            <ta e="T253" id="Seg_8232" s="T252">ej</ta>
            <ta e="T254" id="Seg_8233" s="T253">sʼa-liA-jəʔ</ta>
            <ta e="T255" id="Seg_8234" s="T254">dĭ</ta>
            <ta e="T256" id="Seg_8235" s="T255">bar</ta>
            <ta e="T257" id="Seg_8236" s="T256">süʔmə-luʔbdə-bi</ta>
            <ta e="T258" id="Seg_8237" s="T257">šaʔ-lV-jəʔ</ta>
            <ta e="T259" id="Seg_8238" s="T258">ej</ta>
            <ta e="T260" id="Seg_8239" s="T259">ej</ta>
            <ta e="T261" id="Seg_8240" s="T260">dʼabə-bi</ta>
            <ta e="T262" id="Seg_8241" s="T261">dĭgəttə</ta>
            <ta e="T263" id="Seg_8242" s="T262">il</ta>
            <ta e="T264" id="Seg_8243" s="T263">bar</ta>
            <ta e="T265" id="Seg_8244" s="T264">kirgaːr-laʔbə-jəʔ</ta>
            <ta e="T266" id="Seg_8245" s="T265">dʼabə-ʔ</ta>
            <ta e="T267" id="Seg_8246" s="T266">dʼabə-ʔ</ta>
            <ta e="T268" id="Seg_8247" s="T267">a</ta>
            <ta e="T269" id="Seg_8248" s="T268">dĭ</ta>
            <ta e="T270" id="Seg_8249" s="T269">nuʔmə-luʔbdə-bi</ta>
            <ta e="T271" id="Seg_8250" s="T270">ine-m</ta>
            <ta e="T272" id="Seg_8251" s="T271">öʔlu-bi</ta>
            <ta e="T273" id="Seg_8252" s="T272">dĭgəttə</ta>
            <ta e="T274" id="Seg_8253" s="T273">beške-jəʔ</ta>
            <ta e="T275" id="Seg_8254" s="T274">oʔbdə-bi</ta>
            <ta e="T276" id="Seg_8255" s="T275">i</ta>
            <ta e="T277" id="Seg_8256" s="T276">vsʼaka</ta>
            <ta e="T278" id="Seg_8257" s="T277">iʔgö</ta>
            <ta e="T279" id="Seg_8258" s="T278">beške</ta>
            <ta e="T280" id="Seg_8259" s="T279">det-bi</ta>
            <ta e="T281" id="Seg_8260" s="T280">a</ta>
            <ta e="T282" id="Seg_8261" s="T281">ne-zAŋ</ta>
            <ta e="T283" id="Seg_8262" s="T282">kudo-laʔbə-jəʔ</ta>
            <ta e="T284" id="Seg_8263" s="T283">ĭmbi</ta>
            <ta e="T285" id="Seg_8264" s="T284">dĭrgit</ta>
            <ta e="T287" id="Seg_8265" s="T286">det-bi-l</ta>
            <ta e="T288" id="Seg_8266" s="T287">tolʼko</ta>
            <ta e="T289" id="Seg_8267" s="T288">tănan</ta>
            <ta e="T290" id="Seg_8268" s="T289">am-zittə</ta>
            <ta e="T291" id="Seg_8269" s="T290">dĭ</ta>
            <ta e="T292" id="Seg_8270" s="T291">pʼeːš-Tə</ta>
            <ta e="T294" id="Seg_8271" s="T293">sʼa-bi</ta>
            <ta e="T295" id="Seg_8272" s="T294">iʔbö-laʔbə</ta>
            <ta e="T296" id="Seg_8273" s="T295">dĭgəttə</ta>
            <ta e="T298" id="Seg_8274" s="T297">kaga-zAŋ-də</ta>
            <ta e="T299" id="Seg_8275" s="T298">šo-bi-jəʔ</ta>
            <ta e="T300" id="Seg_8276" s="T299">bar</ta>
            <ta e="T301" id="Seg_8277" s="T300">nörbə-laʔbə-jəʔ</ta>
            <ta e="T303" id="Seg_8278" s="T302">kădaʔ</ta>
            <ta e="T304" id="Seg_8279" s="T303">i-bi</ta>
            <ta e="T305" id="Seg_8280" s="T304">a</ta>
            <ta e="T306" id="Seg_8281" s="T305">dĭ</ta>
            <ta e="T307" id="Seg_8282" s="T306">iʔbö-laʔbə</ta>
            <ta e="T308" id="Seg_8283" s="T307">dĭ</ta>
            <ta e="T309" id="Seg_8284" s="T308">măn</ta>
            <ta e="T310" id="Seg_8285" s="T309">ulu-m</ta>
            <ta e="T311" id="Seg_8286" s="T310">dĭn</ta>
            <ta e="T312" id="Seg_8287" s="T311">i-bi</ta>
            <ta e="T313" id="Seg_8288" s="T312">e-ʔ</ta>
            <ta e="T314" id="Seg_8289" s="T313">šʼaːm-ʔ</ta>
            <ta e="T315" id="Seg_8290" s="T314">tăn</ta>
            <ta e="T316" id="Seg_8291" s="T315">ulu-m</ta>
            <ta e="T317" id="Seg_8292" s="T316">amno-ʔ</ta>
            <ta e="T318" id="Seg_8293" s="T317">už</ta>
            <ta e="T319" id="Seg_8294" s="T318">ĭmbi=də</ta>
            <ta e="T320" id="Seg_8295" s="T319">e-ʔ</ta>
            <ta e="T321" id="Seg_8296" s="T320">tʼăbaktər-ə-ʔ</ta>
            <ta e="T322" id="Seg_8297" s="T321">dĭrgit</ta>
            <ta e="T323" id="Seg_8298" s="T322">kuza</ta>
            <ta e="T324" id="Seg_8299" s="T323">i-bi</ta>
            <ta e="T325" id="Seg_8300" s="T324">ine-t</ta>
            <ta e="T326" id="Seg_8301" s="T325">kuvas</ta>
            <ta e="T327" id="Seg_8302" s="T326">i-bi</ta>
            <ta e="T328" id="Seg_8303" s="T327">bos-də</ta>
            <ta e="T329" id="Seg_8304" s="T328">dĭgəttə</ta>
            <ta e="T330" id="Seg_8305" s="T329">karəldʼaːn</ta>
            <ta e="T331" id="Seg_8306" s="T330">bazoʔ</ta>
            <ta e="T332" id="Seg_8307" s="T331">kan-ntə-gA-jəʔ</ta>
            <ta e="T333" id="Seg_8308" s="T332">kan-lAʔ</ta>
            <ta e="T334" id="Seg_8309" s="T333">tʼür-bi-jəʔ</ta>
            <ta e="T335" id="Seg_8310" s="T334">dĭ</ta>
            <ta e="T336" id="Seg_8311" s="T335">pʼeːš-gəʔ</ta>
            <ta e="T338" id="Seg_8312" s="T337">süʔmə-luʔbdə-bi</ta>
            <ta e="T339" id="Seg_8313" s="T338">karzʼina</ta>
            <ta e="T340" id="Seg_8314" s="T339">i-bi</ta>
            <ta e="T341" id="Seg_8315" s="T340">kan-bi</ta>
            <ta e="T342" id="Seg_8316" s="T341">kan-bi</ta>
            <ta e="T344" id="Seg_8317" s="T342">barəʔ-bi</ta>
            <ta e="T345" id="Seg_8318" s="T344">davaj</ta>
            <ta e="T346" id="Seg_8319" s="T345">ine-m</ta>
            <ta e="T347" id="Seg_8320" s="T346">kirgaːr-zittə</ta>
            <ta e="T348" id="Seg_8321" s="T347">ine-t</ta>
            <ta e="T349" id="Seg_8322" s="T348">šo-bi</ta>
            <ta e="T350" id="Seg_8323" s="T349">bazoʔ</ta>
            <ta e="T351" id="Seg_8324" s="T350">onʼiʔ</ta>
            <ta e="T352" id="Seg_8325" s="T351">kuʔ-də</ta>
            <ta e="T353" id="Seg_8326" s="T352">pada-laːm-bi</ta>
            <ta e="T354" id="Seg_8327" s="T353">onʼiʔ</ta>
            <ta e="T355" id="Seg_8328" s="T354">supso-laːm-bi</ta>
            <ta e="T356" id="Seg_8329" s="T355">dĭgəttə</ta>
            <ta e="T357" id="Seg_8330" s="T356">amnə-bi</ta>
            <ta e="T359" id="Seg_8331" s="T358">šonə-gA</ta>
            <ta e="T360" id="Seg_8332" s="T359">il</ta>
            <ta e="T361" id="Seg_8333" s="T360">iʔgö</ta>
            <ta e="T362" id="Seg_8334" s="T361">nu-gA-jəʔ</ta>
            <ta e="T363" id="Seg_8335" s="T362">dĭgəttə</ta>
            <ta e="T364" id="Seg_8336" s="T363">šində=də</ta>
            <ta e="T365" id="Seg_8337" s="T364">ej</ta>
            <ta e="T366" id="Seg_8338" s="T365">nuʔmə-liA</ta>
            <ta e="T367" id="Seg_8339" s="T366">dĭbər</ta>
            <ta e="T368" id="Seg_8340" s="T367">dĭbər</ta>
            <ta e="T370" id="Seg_8341" s="T369">süʔmə-luʔbdə-bi</ta>
            <ta e="T371" id="Seg_8342" s="T370">i-bi</ta>
            <ta e="T376" id="Seg_8343" s="T375">ej</ta>
            <ta e="T377" id="Seg_8344" s="T376">mo-bi</ta>
            <ta e="T378" id="Seg_8345" s="T377">dĭgəttə</ta>
            <ta e="T379" id="Seg_8346" s="T378">par-luʔbdə-bi</ta>
            <ta e="T380" id="Seg_8347" s="T379">kaga-zAŋ-də</ta>
            <ta e="T381" id="Seg_8348" s="T380">bar</ta>
            <ta e="T382" id="Seg_8349" s="T381">münör-bi</ta>
            <ta e="T383" id="Seg_8350" s="T382">i</ta>
            <ta e="T384" id="Seg_8351" s="T383">kan-lAʔ</ta>
            <ta e="T385" id="Seg_8352" s="T384">tʼür-bi</ta>
            <ta e="T386" id="Seg_8353" s="T385">a</ta>
            <ta e="T387" id="Seg_8354" s="T386">il</ta>
            <ta e="T388" id="Seg_8355" s="T387">kirgaːr-laʔbə-jəʔ</ta>
            <ta e="T389" id="Seg_8356" s="T388">dʼabə-ʔ</ta>
            <ta e="T390" id="Seg_8357" s="T389">dʼabə-ʔ</ta>
            <ta e="T391" id="Seg_8358" s="T390">da</ta>
            <ta e="T392" id="Seg_8359" s="T391">dĭ</ta>
            <ta e="T393" id="Seg_8360" s="T392">e-ʔ</ta>
            <ta e="T394" id="Seg_8361" s="T393">məluʔ-bi</ta>
            <ta e="T395" id="Seg_8362" s="T394">ine-bə</ta>
            <ta e="T396" id="Seg_8363" s="T395">öʔlu-bi</ta>
            <ta e="T397" id="Seg_8364" s="T396">a</ta>
            <ta e="T398" id="Seg_8365" s="T397">bos-də</ta>
            <ta e="T399" id="Seg_8366" s="T398">bazoʔ</ta>
            <ta e="T400" id="Seg_8367" s="T399">šo-bi</ta>
            <ta e="T401" id="Seg_8368" s="T400">ne-zAŋ-də</ta>
            <ta e="T402" id="Seg_8369" s="T401">bazoʔ</ta>
            <ta e="T403" id="Seg_8370" s="T402">kudo-laʔbə-jəʔ</ta>
            <ta e="T404" id="Seg_8371" s="T403">a</ta>
            <ta e="T405" id="Seg_8372" s="T404">dĭ</ta>
            <ta e="T406" id="Seg_8373" s="T405">pʼeːš-Tə</ta>
            <ta e="T407" id="Seg_8374" s="T406">sʼa-bi</ta>
            <ta e="T408" id="Seg_8375" s="T407">da</ta>
            <ta e="T409" id="Seg_8376" s="T408">iʔbö-laʔbə</ta>
            <ta e="T411" id="Seg_8377" s="T410">kazAŋ-də</ta>
            <ta e="T412" id="Seg_8378" s="T411">šo-bi-jəʔ</ta>
            <ta e="T413" id="Seg_8379" s="T412">aba-gəndə</ta>
            <ta e="T414" id="Seg_8380" s="T413">nörbə-liA-jəʔ</ta>
            <ta e="T415" id="Seg_8381" s="T414">girgit</ta>
            <ta e="T416" id="Seg_8382" s="T415">kuza</ta>
            <ta e="T417" id="Seg_8383" s="T416">i-bi</ta>
            <ta e="T418" id="Seg_8384" s="T417">ine-t</ta>
            <ta e="T419" id="Seg_8385" s="T418">ugaːndə</ta>
            <ta e="T420" id="Seg_8386" s="T419">kuvas</ta>
            <ta e="T421" id="Seg_8387" s="T420">i</ta>
            <ta e="T423" id="Seg_8388" s="T422">bos-də</ta>
            <ta e="T424" id="Seg_8389" s="T423">kuvas</ta>
            <ta e="T425" id="Seg_8390" s="T424">a</ta>
            <ta e="T426" id="Seg_8391" s="T425">dĭ</ta>
            <ta e="T427" id="Seg_8392" s="T426">iʔbö-laʔbə</ta>
            <ta e="T428" id="Seg_8393" s="T427">ej</ta>
            <ta e="T429" id="Seg_8394" s="T428">măn</ta>
            <ta e="T430" id="Seg_8395" s="T429">li</ta>
            <ta e="T431" id="Seg_8396" s="T430">ulu-m</ta>
            <ta e="T432" id="Seg_8397" s="T431">dĭn</ta>
            <ta e="T433" id="Seg_8398" s="T432">i-bi</ta>
            <ta e="T434" id="Seg_8399" s="T433">e-ʔ</ta>
            <ta e="T435" id="Seg_8400" s="T434">šʼaːm-ʔ</ta>
            <ta e="T436" id="Seg_8401" s="T435">iʔbö-ʔ</ta>
            <ta e="T437" id="Seg_8402" s="T436">už</ta>
            <ta e="T438" id="Seg_8403" s="T437">sagəš-žət</ta>
            <ta e="T439" id="Seg_8404" s="T438">nʼi</ta>
            <ta e="T440" id="Seg_8405" s="T439">tăn</ta>
            <ta e="T441" id="Seg_8406" s="T440">dĭn</ta>
            <ta e="T442" id="Seg_8407" s="T441">i-bi-l</ta>
            <ta e="T443" id="Seg_8408" s="T442">dĭgəttə</ta>
            <ta e="T444" id="Seg_8409" s="T443">bazoʔ</ta>
            <ta e="T445" id="Seg_8410" s="T444">nagur</ta>
            <ta e="T446" id="Seg_8411" s="T445">tʼala</ta>
            <ta e="T447" id="Seg_8412" s="T446">bazoʔ</ta>
            <ta e="T448" id="Seg_8413" s="T447">kan-ntə-gA-jəʔ</ta>
            <ta e="T449" id="Seg_8414" s="T448">kan-lAʔ</ta>
            <ta e="T450" id="Seg_8415" s="T449">tʼür-bi-jəʔ</ta>
            <ta e="T451" id="Seg_8416" s="T450">dĭ</ta>
            <ta e="T453" id="Seg_8417" s="T452">uʔbdə-bi</ta>
            <ta e="T454" id="Seg_8418" s="T453">karzʼina-bə</ta>
            <ta e="T455" id="Seg_8419" s="T454">i-bi</ta>
            <ta e="T456" id="Seg_8420" s="T455">kan-bi</ta>
            <ta e="T457" id="Seg_8421" s="T456">beške-jəʔ</ta>
            <ta e="T458" id="Seg_8422" s="T457">oʔbdə-zittə</ta>
            <ta e="T459" id="Seg_8423" s="T458">kan-bi</ta>
            <ta e="T460" id="Seg_8424" s="T459">davaj</ta>
            <ta e="T461" id="Seg_8425" s="T460">ine</ta>
            <ta e="T462" id="Seg_8426" s="T461">kirgaːr-zittə</ta>
            <ta e="T463" id="Seg_8427" s="T462">ine</ta>
            <ta e="T464" id="Seg_8428" s="T463">šo-bi</ta>
            <ta e="T465" id="Seg_8429" s="T464">dĭ</ta>
            <ta e="T466" id="Seg_8430" s="T465">bazoʔ</ta>
            <ta e="T467" id="Seg_8431" s="T466">dăra</ta>
            <ta e="T469" id="Seg_8432" s="T468">onʼiʔ</ta>
            <ta e="T470" id="Seg_8433" s="T469">ku-gəndə</ta>
            <ta e="T471" id="Seg_8434" s="T470">pada-laːm-bi</ta>
            <ta e="T472" id="Seg_8435" s="T471">onʼiʔ</ta>
            <ta e="T473" id="Seg_8436" s="T472">supso-laːm-bi</ta>
            <ta e="T474" id="Seg_8437" s="T473">amno-bi</ta>
            <ta e="T475" id="Seg_8438" s="T474">kan-bi</ta>
            <ta e="T476" id="Seg_8439" s="T475">dĭgəttə</ta>
            <ta e="T477" id="Seg_8440" s="T476">kak</ta>
            <ta e="T478" id="Seg_8441" s="T477">nuʔmə-luʔbdə-bi</ta>
            <ta e="T479" id="Seg_8442" s="T478">dĭ-m</ta>
            <ta e="T480" id="Seg_8443" s="T479">panar-bi</ta>
            <ta e="T481" id="Seg_8444" s="T480">i</ta>
            <ta e="T482" id="Seg_8445" s="T481">kalʼečka-bə</ta>
            <ta e="T483" id="Seg_8446" s="T482">i-luʔbdə-bi</ta>
            <ta e="T484" id="Seg_8447" s="T483">bos-də</ta>
            <ta e="T485" id="Seg_8448" s="T484">uda-gəndə</ta>
            <ta e="T487" id="Seg_8449" s="T486">šer-bi</ta>
            <ta e="T488" id="Seg_8450" s="T487">kan-lAʔ</ta>
            <ta e="T489" id="Seg_8451" s="T488">tʼür-bi</ta>
            <ta e="T490" id="Seg_8452" s="T489">ine-bə</ta>
            <ta e="T491" id="Seg_8453" s="T490">öʔlu-bi</ta>
            <ta e="T492" id="Seg_8454" s="T491">bos-də</ta>
            <ta e="T493" id="Seg_8455" s="T492">šo-bi</ta>
            <ta e="T494" id="Seg_8456" s="T493">maʔ-gəndə</ta>
            <ta e="T495" id="Seg_8457" s="T494">i</ta>
            <ta e="T496" id="Seg_8458" s="T495">pʼeːš-Tə</ta>
            <ta e="T497" id="Seg_8459" s="T496">sʼa-bi</ta>
            <ta e="T498" id="Seg_8460" s="T497">iʔbö-laʔbə</ta>
            <ta e="T499" id="Seg_8461" s="T498">dĭgəttə</ta>
            <ta e="T500" id="Seg_8462" s="T499">kaga-zAŋ-də</ta>
            <ta e="T501" id="Seg_8463" s="T500">šo-bi-jəʔ</ta>
            <ta e="T502" id="Seg_8464" s="T501">nu</ta>
            <ta e="T503" id="Seg_8465" s="T502">girgit=də</ta>
            <ta e="T504" id="Seg_8466" s="T503">nʼi</ta>
            <ta e="T505" id="Seg_8467" s="T504">nʼi</ta>
            <ta e="T506" id="Seg_8468" s="T505">kuvas</ta>
            <ta e="T507" id="Seg_8469" s="T506">ine-t</ta>
            <ta e="T508" id="Seg_8470" s="T507">kuvas</ta>
            <ta e="T509" id="Seg_8471" s="T508">panar-bi</ta>
            <ta e="T510" id="Seg_8472" s="T509">tsarskɨj</ta>
            <ta e="T511" id="Seg_8473" s="T510">koʔbdo</ta>
            <ta e="T512" id="Seg_8474" s="T511">koŋ</ta>
            <ta e="T513" id="Seg_8475" s="T512">koŋ</ta>
            <ta e="T514" id="Seg_8476" s="T513">koʔbdo</ta>
            <ta e="T515" id="Seg_8477" s="T514">i</ta>
            <ta e="T516" id="Seg_8478" s="T515">kalʼečka-bə</ta>
            <ta e="T517" id="Seg_8479" s="T516">i-bi</ta>
            <ta e="T518" id="Seg_8480" s="T517">kan-lAʔ</ta>
            <ta e="T519" id="Seg_8481" s="T518">tʼür-bi</ta>
            <ta e="T520" id="Seg_8482" s="T519">dĭgəttə</ta>
            <ta e="T521" id="Seg_8483" s="T520">bazoʔ</ta>
            <ta e="T522" id="Seg_8484" s="T521">dĭ</ta>
            <ta e="T523" id="Seg_8485" s="T522">koŋ</ta>
            <ta e="T524" id="Seg_8486" s="T523">a-bi</ta>
            <ta e="T525" id="Seg_8487" s="T524">iʔgö</ta>
            <ta e="T526" id="Seg_8488" s="T525">uja</ta>
            <ta e="T527" id="Seg_8489" s="T526">munəj-jəʔ</ta>
            <ta e="T528" id="Seg_8490" s="T527">hen-bi</ta>
            <ta e="T529" id="Seg_8491" s="T528">bar</ta>
            <ta e="T530" id="Seg_8492" s="T529">ĭmbi</ta>
            <ta e="T531" id="Seg_8493" s="T530">dĭn</ta>
            <ta e="T532" id="Seg_8494" s="T531">i-gA</ta>
            <ta e="T533" id="Seg_8495" s="T532">hen-bi</ta>
            <ta e="T534" id="Seg_8496" s="T533">dĭgəttə</ta>
            <ta e="T535" id="Seg_8497" s="T534">šo-KAʔ</ta>
            <ta e="T536" id="Seg_8498" s="T535">bar-gəʔ</ta>
            <ta e="T537" id="Seg_8499" s="T536">šində</ta>
            <ta e="T538" id="Seg_8500" s="T537">ej</ta>
            <ta e="T539" id="Seg_8501" s="T538">šo-lV-j</ta>
            <ta e="T540" id="Seg_8502" s="T539">ulu-bə</ta>
            <ta e="T542" id="Seg_8503" s="T541">săj-hʼaʔ-lV-m</ta>
            <ta e="T543" id="Seg_8504" s="T542">dĭgəttə</ta>
            <ta e="T544" id="Seg_8505" s="T543">dĭ-zAŋ</ta>
            <ta e="T546" id="Seg_8506" s="T545">dĭn</ta>
            <ta e="T547" id="Seg_8507" s="T546">aba-t</ta>
            <ta e="T548" id="Seg_8508" s="T547">bar-gəʔ</ta>
            <ta e="T549" id="Seg_8509" s="T548">kan-lAʔ</ta>
            <ta e="T550" id="Seg_8510" s="T549">tʼür-bi-jəʔ</ta>
            <ta e="T551" id="Seg_8511" s="T550">šo-bi-jəʔ</ta>
            <ta e="T552" id="Seg_8512" s="T551">dĭ-zAŋ</ta>
            <ta e="T553" id="Seg_8513" s="T552">amnəl-bi-jəʔ</ta>
            <ta e="T555" id="Seg_8514" s="T554">bar</ta>
            <ta e="T556" id="Seg_8515" s="T555">bĭs-laʔbə-jəʔ</ta>
            <ta e="T558" id="Seg_8516" s="T557">ara</ta>
            <ta e="T559" id="Seg_8517" s="T558">mĭ-laʔbə-jəʔ</ta>
            <ta e="T560" id="Seg_8518" s="T559">dĭgəttə</ta>
            <ta e="T561" id="Seg_8519" s="T560">šo-bi</ta>
            <ta e="T562" id="Seg_8520" s="T561">dĭ</ta>
            <ta e="T563" id="Seg_8521" s="T562">poslʼednij</ta>
            <ta e="T564" id="Seg_8522" s="T563">nʼi-t</ta>
            <ta e="T565" id="Seg_8523" s="T564">dĭ</ta>
            <ta e="T566" id="Seg_8524" s="T565">dĭ</ta>
            <ta e="T567" id="Seg_8525" s="T566">bar</ta>
            <ta e="T568" id="Seg_8526" s="T567">sagər</ta>
            <ta e="T569" id="Seg_8527" s="T568">amno-laʔbə</ta>
            <ta e="T570" id="Seg_8528" s="T569">i</ta>
            <ta e="T571" id="Seg_8529" s="T570">uda-t</ta>
            <ta e="T572" id="Seg_8530" s="T571">sagər</ta>
            <ta e="T573" id="Seg_8531" s="T572">trʼapka-ziʔ</ta>
            <ta e="T574" id="Seg_8532" s="T573">sar-o-NTA</ta>
            <ta e="T575" id="Seg_8533" s="T574">măn-ntə</ta>
            <ta e="T576" id="Seg_8534" s="T575">ĭmbi</ta>
            <ta e="T577" id="Seg_8535" s="T576">tăn</ta>
            <ta e="T578" id="Seg_8536" s="T577">uda-gəndə</ta>
            <ta e="T580" id="Seg_8537" s="T579">ku-liA-t</ta>
            <ta e="T581" id="Seg_8538" s="T580">bar</ta>
            <ta e="T582" id="Seg_8539" s="T581">bar</ta>
            <ta e="T583" id="Seg_8540" s="T582">dĭn</ta>
            <ta e="T584" id="Seg_8541" s="T583">kalʼečka</ta>
            <ta e="T585" id="Seg_8542" s="T584">amno-laʔbə</ta>
            <ta e="T586" id="Seg_8543" s="T585">dĭgəttə</ta>
            <ta e="T587" id="Seg_8544" s="T586">i-lV-t</ta>
            <ta e="T588" id="Seg_8545" s="T587">aba-gəndə</ta>
            <ta e="T589" id="Seg_8546" s="T588">šo-liA-t</ta>
            <ta e="T590" id="Seg_8547" s="T589">dö</ta>
            <ta e="T592" id="Seg_8548" s="T591">dö</ta>
            <ta e="T593" id="Seg_8549" s="T592">măn</ta>
            <ta e="T594" id="Seg_8550" s="T593">ženʼix-m</ta>
            <ta e="T595" id="Seg_8551" s="T594">dĭ-m</ta>
            <ta e="T596" id="Seg_8552" s="T595">dĭ</ta>
            <ta e="T598" id="Seg_8553" s="T597">dĭ-zAŋ</ta>
            <ta e="T599" id="Seg_8554" s="T598">dĭn</ta>
            <ta e="T600" id="Seg_8555" s="T599">bazə-bi-jəʔ</ta>
            <ta e="T601" id="Seg_8556" s="T600">kuvas</ta>
            <ta e="T602" id="Seg_8557" s="T601">oldʼa</ta>
            <ta e="T604" id="Seg_8558" s="T603">šer-bi-jəʔ</ta>
            <ta e="T605" id="Seg_8559" s="T604">ugaːndə</ta>
            <ta e="T606" id="Seg_8560" s="T605">kuvas</ta>
            <ta e="T607" id="Seg_8561" s="T606">mo-laːm-bi</ta>
            <ta e="T608" id="Seg_8562" s="T607">dĭgəttə</ta>
            <ta e="T609" id="Seg_8563" s="T608">bazoʔ</ta>
            <ta e="T610" id="Seg_8564" s="T609">davaj</ta>
            <ta e="T611" id="Seg_8565" s="T610">ara</ta>
            <ta e="T612" id="Seg_8566" s="T611">bĭs-zittə</ta>
            <ta e="T613" id="Seg_8567" s="T612">sʼar-zittə</ta>
            <ta e="T614" id="Seg_8568" s="T613">kaga-zAŋ-də</ta>
            <ta e="T615" id="Seg_8569" s="T614">bar</ta>
            <ta e="T616" id="Seg_8570" s="T615">măndo-laʔbə-jəʔ</ta>
            <ta e="T617" id="Seg_8571" s="T616">girgit</ta>
            <ta e="T618" id="Seg_8572" s="T617">kuvas</ta>
            <ta e="T619" id="Seg_8573" s="T618">mo-laːm-bi</ta>
            <ta e="T620" id="Seg_8574" s="T619">i</ta>
            <ta e="T621" id="Seg_8575" s="T620">dĭn</ta>
            <ta e="T622" id="Seg_8576" s="T621">măn</ta>
            <ta e="T623" id="Seg_8577" s="T622">i-bi-m</ta>
            <ta e="T624" id="Seg_8578" s="T623">ara</ta>
            <ta e="T625" id="Seg_8579" s="T624">bar</ta>
            <ta e="T626" id="Seg_8580" s="T625">mʼaŋ-laʔbə</ta>
            <ta e="T627" id="Seg_8581" s="T626">mĭ-bi-jəʔ</ta>
            <ta e="T628" id="Seg_8582" s="T627">măna</ta>
            <ta e="T629" id="Seg_8583" s="T628">blin</ta>
            <ta e="T634" id="Seg_8584" s="T633">amnə-bi-jəʔ</ta>
            <ta e="T635" id="Seg_8585" s="T634">šide-göʔ</ta>
            <ta e="T636" id="Seg_8586" s="T635">kun</ta>
            <ta e="T637" id="Seg_8587" s="T636">da</ta>
            <ta e="T638" id="Seg_8588" s="T637">kuma</ta>
            <ta e="T639" id="Seg_8589" s="T638">volk</ta>
            <ta e="T640" id="Seg_8590" s="T639">da</ta>
            <ta e="T641" id="Seg_8591" s="T640">lʼisa</ta>
            <ta e="T642" id="Seg_8592" s="T641">dĭgəttə</ta>
            <ta e="T643" id="Seg_8593" s="T642">kunol-zittə</ta>
            <ta e="T644" id="Seg_8594" s="T643">iʔbö-jəʔ</ta>
            <ta e="T645" id="Seg_8595" s="T644">a</ta>
            <ta e="T646" id="Seg_8596" s="T645">lʼisa</ta>
            <ta e="T647" id="Seg_8597" s="T646">üge</ta>
            <ta e="T648" id="Seg_8598" s="T647">kuzur-laʔbə</ta>
            <ta e="T649" id="Seg_8599" s="T648">kuzur-laʔbə</ta>
            <ta e="T650" id="Seg_8600" s="T649">a</ta>
            <ta e="T651" id="Seg_8601" s="T650">volk</ta>
            <ta e="T652" id="Seg_8602" s="T651">măn-ntə</ta>
            <ta e="T653" id="Seg_8603" s="T652">šində=də</ta>
            <ta e="T654" id="Seg_8604" s="T653">kuzur-laʔbə</ta>
            <ta e="T655" id="Seg_8605" s="T654">măna</ta>
            <ta e="T656" id="Seg_8606" s="T655">kăštə-liA-jəʔ</ta>
            <ta e="T657" id="Seg_8607" s="T656">ešši</ta>
            <ta e="T658" id="Seg_8608" s="T657">ešši</ta>
            <ta e="T659" id="Seg_8609" s="T658">bădə-zittə</ta>
            <ta e="T660" id="Seg_8610" s="T659">dĭgəttə</ta>
            <ta e="T661" id="Seg_8611" s="T660">no</ta>
            <ta e="T662" id="Seg_8612" s="T661">kan-ə-ʔ</ta>
            <ta e="T663" id="Seg_8613" s="T662">dĭ</ta>
            <ta e="T664" id="Seg_8614" s="T663">kan-bi</ta>
            <ta e="T665" id="Seg_8615" s="T664">a</ta>
            <ta e="T666" id="Seg_8616" s="T665">dĭ-zAŋ</ta>
            <ta e="T667" id="Seg_8617" s="T666">nulaʔ-bi</ta>
            <ta e="T668" id="Seg_8618" s="T667">mʼod</ta>
            <ta e="T669" id="Seg_8619" s="T668">am-bi</ta>
            <ta e="T670" id="Seg_8620" s="T669">am-bi</ta>
            <ta e="T671" id="Seg_8621" s="T670">mʼod</ta>
            <ta e="T672" id="Seg_8622" s="T671">dĭ</ta>
            <ta e="T673" id="Seg_8623" s="T672">šo-bi</ta>
            <ta e="T674" id="Seg_8624" s="T673">šində</ta>
            <ta e="T675" id="Seg_8625" s="T674">dĭn</ta>
            <ta e="T676" id="Seg_8626" s="T675">i-gA</ta>
            <ta e="T677" id="Seg_8627" s="T676">veršok</ta>
            <ta e="T678" id="Seg_8628" s="T677">i-gA</ta>
            <ta e="T679" id="Seg_8629" s="T678">dĭgəttə</ta>
            <ta e="T680" id="Seg_8630" s="T679">bazoʔ</ta>
            <ta e="T681" id="Seg_8631" s="T680">nüdʼi-n</ta>
            <ta e="T682" id="Seg_8632" s="T681">bazoʔ</ta>
            <ta e="T683" id="Seg_8633" s="T682">kuzur-laʔbə</ta>
            <ta e="T684" id="Seg_8634" s="T683">lʼisʼitsa</ta>
            <ta e="T685" id="Seg_8635" s="T684">šində</ta>
            <ta e="T686" id="Seg_8636" s="T685">kuzur-laʔbə</ta>
            <ta e="T687" id="Seg_8637" s="T686">da</ta>
            <ta e="T688" id="Seg_8638" s="T687">măna</ta>
            <ta e="T689" id="Seg_8639" s="T688">kăštə-laʔbə-jəʔ</ta>
            <ta e="T690" id="Seg_8640" s="T689">ešši</ta>
            <ta e="T691" id="Seg_8641" s="T690">ködər-zittə</ta>
            <ta e="T692" id="Seg_8642" s="T691">no</ta>
            <ta e="T693" id="Seg_8643" s="T692">kan-ə-ʔ</ta>
            <ta e="T694" id="Seg_8644" s="T693">dĭ</ta>
            <ta e="T695" id="Seg_8645" s="T694">kan-bi</ta>
            <ta e="T696" id="Seg_8646" s="T695">am-bi</ta>
            <ta e="T697" id="Seg_8647" s="T696">am-bi</ta>
            <ta e="T698" id="Seg_8648" s="T697">dĭgəttə</ta>
            <ta e="T699" id="Seg_8649" s="T698">šo-bi</ta>
            <ta e="T700" id="Seg_8650" s="T699">šində</ta>
            <ta e="T701" id="Seg_8651" s="T700">dĭn</ta>
            <ta e="T702" id="Seg_8652" s="T701">šo-bi</ta>
            <ta e="T703" id="Seg_8653" s="T702">sʼerʼodušok</ta>
            <ta e="T704" id="Seg_8654" s="T703">šo-bi</ta>
            <ta e="T705" id="Seg_8655" s="T704">dĭgəttə</ta>
            <ta e="T706" id="Seg_8656" s="T705">nagur-git</ta>
            <ta e="T707" id="Seg_8657" s="T706">nüdʼi-n</ta>
            <ta e="T708" id="Seg_8658" s="T707">bazoʔ</ta>
            <ta e="T709" id="Seg_8659" s="T708">kuzur-laʔbə</ta>
            <ta e="T710" id="Seg_8660" s="T709">dĭgəttə</ta>
            <ta e="T711" id="Seg_8661" s="T710">šində=də</ta>
            <ta e="T712" id="Seg_8662" s="T711">kuzur-laʔbə</ta>
            <ta e="T713" id="Seg_8663" s="T712">da</ta>
            <ta e="T714" id="Seg_8664" s="T713">măna</ta>
            <ta e="T715" id="Seg_8665" s="T714">kăštə-laʔbə-jəʔ</ta>
            <ta e="T716" id="Seg_8666" s="T715">no</ta>
            <ta e="T717" id="Seg_8667" s="T716">kan-ə-ʔ</ta>
            <ta e="T718" id="Seg_8668" s="T717">dĭgəttə</ta>
            <ta e="T719" id="Seg_8669" s="T718">dĭ</ta>
            <ta e="T720" id="Seg_8670" s="T719">am-bi</ta>
            <ta e="T721" id="Seg_8671" s="T720">bar</ta>
            <ta e="T722" id="Seg_8672" s="T721">šo-bi</ta>
            <ta e="T723" id="Seg_8673" s="T722">šində</ta>
            <ta e="T724" id="Seg_8674" s="T723">dĭn</ta>
            <ta e="T725" id="Seg_8675" s="T724">dĭ</ta>
            <ta e="T726" id="Seg_8676" s="T725">skrʼobušok</ta>
            <ta e="T727" id="Seg_8677" s="T726">šo-bi</ta>
            <ta e="T728" id="Seg_8678" s="T727">dĭgəttə</ta>
            <ta e="T729" id="Seg_8679" s="T728">ertə-n</ta>
            <ta e="T730" id="Seg_8680" s="T729">uʔbdə-bi-jəʔ</ta>
            <ta e="T731" id="Seg_8681" s="T730">dĭ</ta>
            <ta e="T732" id="Seg_8682" s="T731">ĭzem-luʔbdə-bi</ta>
            <ta e="T733" id="Seg_8683" s="T732">măn-ntə</ta>
            <ta e="T734" id="Seg_8684" s="T733">det-ʔ</ta>
            <ta e="T735" id="Seg_8685" s="T734">măna</ta>
            <ta e="T736" id="Seg_8686" s="T735">nʼamga-jəʔ</ta>
            <ta e="T737" id="Seg_8687" s="T736">dĭ</ta>
            <ta e="T738" id="Seg_8688" s="T737">kan-bi</ta>
            <ta e="T739" id="Seg_8689" s="T738">da</ta>
            <ta e="T740" id="Seg_8690" s="T739">naga</ta>
            <ta e="T741" id="Seg_8691" s="T740">šində=də</ta>
            <ta e="T742" id="Seg_8692" s="T741">am-luʔbdə-bi</ta>
            <ta e="T743" id="Seg_8693" s="T742">da</ta>
            <ta e="T744" id="Seg_8694" s="T743">tăn</ta>
            <ta e="T745" id="Seg_8695" s="T744">am-bi-l</ta>
            <ta e="T746" id="Seg_8696" s="T745">dĭ</ta>
            <ta e="T747" id="Seg_8697" s="T746">bar</ta>
            <ta e="T748" id="Seg_8698" s="T747">ej</ta>
            <ta e="T749" id="Seg_8699" s="T748">măn</ta>
            <ta e="T750" id="Seg_8700" s="T749">ej</ta>
            <ta e="T751" id="Seg_8701" s="T750">am-bi-m</ta>
            <ta e="T752" id="Seg_8702" s="T751">dĭgəttə</ta>
            <ta e="T753" id="Seg_8703" s="T752">davaj</ta>
            <ta e="T754" id="Seg_8704" s="T753">kuja-Tə</ta>
            <ta e="T755" id="Seg_8705" s="T754">iʔbö-bAʔ</ta>
            <ta e="T756" id="Seg_8706" s="T755">šində</ta>
            <ta e="T757" id="Seg_8707" s="T756">am-bi</ta>
            <ta e="T758" id="Seg_8708" s="T757">dĭn</ta>
            <ta e="T759" id="Seg_8709" s="T758">nʼamga</ta>
            <ta e="T760" id="Seg_8710" s="T759">nanə-t</ta>
            <ta e="T761" id="Seg_8711" s="T760">mo-lV-j</ta>
            <ta e="T762" id="Seg_8712" s="T761">dĭgəttə</ta>
            <ta e="T763" id="Seg_8713" s="T762">ku-liA-t</ta>
            <ta e="T764" id="Seg_8714" s="T763">bos-də</ta>
            <ta e="T765" id="Seg_8715" s="T764">nanə-gəndə</ta>
            <ta e="T766" id="Seg_8716" s="T765">nʼamga</ta>
            <ta e="T767" id="Seg_8717" s="T766">i-gA</ta>
            <ta e="T768" id="Seg_8718" s="T767">dĭ</ta>
            <ta e="T769" id="Seg_8719" s="T768">davaj</ta>
            <ta e="T770" id="Seg_8720" s="T769">dĭ</ta>
            <ta e="T771" id="Seg_8721" s="T770">volk-də</ta>
            <ta e="T772" id="Seg_8722" s="T771">tʼuʔbdə-zittə</ta>
            <ta e="T773" id="Seg_8723" s="T772">uʔbdə-ʔ</ta>
            <ta e="T774" id="Seg_8724" s="T773">tăn</ta>
            <ta e="T776" id="Seg_8725" s="T775">am-bi-l</ta>
            <ta e="T777" id="Seg_8726" s="T776">dĭgəttə</ta>
            <ta e="T778" id="Seg_8727" s="T777">dĭ</ta>
            <ta e="T779" id="Seg_8728" s="T778">uʔbdə-bi</ta>
            <ta e="T780" id="Seg_8729" s="T779">no</ta>
            <ta e="T781" id="Seg_8730" s="T780">măn</ta>
            <ta e="T782" id="Seg_8731" s="T781">am-bi-m</ta>
            <ta e="T783" id="Seg_8732" s="T782">dĭgəttə</ta>
            <ta e="T784" id="Seg_8733" s="T783">bar</ta>
            <ta e="T785" id="Seg_8734" s="T784">süjö</ta>
            <ta e="T786" id="Seg_8735" s="T785">i</ta>
            <ta e="T787" id="Seg_8736" s="T786">tʼetʼer</ta>
            <ta e="T788" id="Seg_8737" s="T787">amno-jəʔ</ta>
            <ta e="T789" id="Seg_8738" s="T788">kădaʔ</ta>
            <ta e="T790" id="Seg_8739" s="T789">tura</ta>
            <ta e="T791" id="Seg_8740" s="T790">hʼaʔ-zittə</ta>
            <ta e="T792" id="Seg_8741" s="T791">baltu</ta>
            <ta e="T793" id="Seg_8742" s="T792">naga</ta>
            <ta e="T794" id="Seg_8743" s="T793">šində</ta>
            <ta e="T795" id="Seg_8744" s="T794">baltu</ta>
            <ta e="T796" id="Seg_8745" s="T795">a-lV-j</ta>
            <ta e="T797" id="Seg_8746" s="T796">da</ta>
            <ta e="T798" id="Seg_8747" s="T797">našto</ta>
            <ta e="T799" id="Seg_8748" s="T798">măna</ta>
            <ta e="T801" id="Seg_8749" s="T800">tura</ta>
            <ta e="T802" id="Seg_8750" s="T801">măn</ta>
            <ta e="T803" id="Seg_8751" s="T802">sĭri-Kən</ta>
            <ta e="T804" id="Seg_8752" s="T803">šaʔ-lV-m</ta>
            <ta e="T805" id="Seg_8753" s="T804">i</ta>
            <ta e="T806" id="Seg_8754" s="T805">saʔmə-luʔbdə-bi</ta>
            <ta e="T808" id="Seg_8755" s="T807">sĭri-Tə</ta>
            <ta e="T809" id="Seg_8756" s="T808">dĭn</ta>
            <ta e="T810" id="Seg_8757" s="T809">šaʔ-bi</ta>
            <ta e="T811" id="Seg_8758" s="T810">ertə-n</ta>
            <ta e="T812" id="Seg_8759" s="T811">uʔbdə-bi</ta>
            <ta e="T813" id="Seg_8760" s="T812">i</ta>
            <ta e="T814" id="Seg_8761" s="T813">kan-bi</ta>
            <ta e="T815" id="Seg_8762" s="T814">ku-zittə</ta>
            <ta e="T816" id="Seg_8763" s="T815">hele-zAŋ-də</ta>
            <ta e="T817" id="Seg_8764" s="T816">ku-bi</ta>
            <ta e="T818" id="Seg_8765" s="T817">dĭn</ta>
            <ta e="T819" id="Seg_8766" s="T818">sʼar-bi-jəʔ</ta>
            <ta e="T820" id="Seg_8767" s="T819">süʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T822" id="Seg_8768" s="T821">pa-jəʔ-Tə</ta>
            <ta e="T823" id="Seg_8769" s="T822">dĭgəttə</ta>
            <ta e="T824" id="Seg_8770" s="T823">bos-gəndə</ta>
            <ta e="T825" id="Seg_8771" s="T824">gnʼozda-jəʔ</ta>
            <ta e="T826" id="Seg_8772" s="T825">a-bi-jəʔ</ta>
            <ta e="T827" id="Seg_8773" s="T826">munəj-jəʔ</ta>
            <ta e="T828" id="Seg_8774" s="T827">det-bi-jəʔ</ta>
            <ta e="T829" id="Seg_8775" s="T828">ešši-zAŋ-Tə</ta>
            <ta e="T830" id="Seg_8776" s="T829">mo-bi-jəʔ</ta>
            <ta e="T831" id="Seg_8777" s="T830">dĭgəttə</ta>
            <ta e="T832" id="Seg_8778" s="T831">ešši-zAŋ-Tə</ta>
            <ta e="T833" id="Seg_8779" s="T832">bădə-bi-jəʔ</ta>
            <ta e="T834" id="Seg_8780" s="T833">maška-ziʔ</ta>
            <ta e="T835" id="Seg_8781" s="T834">červə-jəʔ-ziʔ</ta>
            <ta e="T836" id="Seg_8782" s="T835">dĭgəttə</ta>
            <ta e="T837" id="Seg_8783" s="T836">bazoʔ</ta>
            <ta e="T838" id="Seg_8784" s="T837">našto</ta>
            <ta e="T839" id="Seg_8785" s="T838">miʔnʼibeʔ</ta>
            <ta e="T840" id="Seg_8786" s="T839">maʔ</ta>
            <ta e="T841" id="Seg_8787" s="T840">sĭri-Kən</ta>
            <ta e="T842" id="Seg_8788" s="T841">kunol-bi-bAʔ</ta>
            <ta e="T843" id="Seg_8789" s="T842">onʼiʔ</ta>
            <ta e="T844" id="Seg_8790" s="T843">nüdʼi</ta>
            <ta e="T845" id="Seg_8791" s="T844">maʔ-Kən</ta>
            <ta e="T846" id="Seg_8792" s="T845">amnol-bAʔ</ta>
            <ta e="T847" id="Seg_8793" s="T846">dĭ</ta>
            <ta e="T848" id="Seg_8794" s="T847">bar</ta>
            <ta e="T849" id="Seg_8795" s="T848">vʼezʼdʼe</ta>
            <ta e="T850" id="Seg_8796" s="T849">măndo-r-laʔbə-bAʔ</ta>
            <ta e="T851" id="Seg_8797" s="T850">kabarləj</ta>
            <ta e="T852" id="Seg_8798" s="T851">amnə-bi-jəʔ</ta>
            <ta e="T853" id="Seg_8799" s="T852">nüke</ta>
            <ta e="T854" id="Seg_8800" s="T853">da</ta>
            <ta e="T855" id="Seg_8801" s="T854">büzʼe</ta>
            <ta e="T857" id="Seg_8802" s="T856">dĭ-zAŋ</ta>
            <ta e="T858" id="Seg_8803" s="T857">ĭmbi=də</ta>
            <ta e="T859" id="Seg_8804" s="T858">naga-bi</ta>
            <ta e="T860" id="Seg_8805" s="T859">kan-bi-jəʔ</ta>
            <ta e="T861" id="Seg_8806" s="T860">dʼije-Tə</ta>
            <ta e="T862" id="Seg_8807" s="T861">šiška-jəʔ</ta>
            <ta e="T863" id="Seg_8808" s="T862">žoludʼi</ta>
            <ta e="T864" id="Seg_8809" s="T863">oʔbdo-bi-jəʔ</ta>
            <ta e="T865" id="Seg_8810" s="T864">šo-bi-jəʔ</ta>
            <ta e="T866" id="Seg_8811" s="T865">am-laʔbə</ta>
            <ta e="T867" id="Seg_8812" s="T866">bar</ta>
            <ta e="T868" id="Seg_8813" s="T867">onʼiʔ</ta>
            <ta e="T869" id="Seg_8814" s="T868">saʔmə-luʔbdə-bi</ta>
            <ta e="T870" id="Seg_8815" s="T869">patpolʼlʼa-Kən</ta>
            <ta e="T871" id="Seg_8816" s="T870">dĭgəttə</ta>
            <ta e="T872" id="Seg_8817" s="T871">davaj</ta>
            <ta e="T873" id="Seg_8818" s="T872">özer-zittə</ta>
            <ta e="T875" id="Seg_8819" s="T874">nüke</ta>
            <ta e="T876" id="Seg_8820" s="T875">măn-ntə</ta>
            <ta e="T877" id="Seg_8821" s="T876">baltu-ziʔ</ta>
            <ta e="T878" id="Seg_8822" s="T877">hʼaʔ-t</ta>
            <ta e="T879" id="Seg_8823" s="T878">pol-də</ta>
            <ta e="T880" id="Seg_8824" s="T879">dĭ</ta>
            <ta e="T881" id="Seg_8825" s="T880">hʼaʔ-bi</ta>
            <ta e="T882" id="Seg_8826" s="T881">dĭ</ta>
            <ta e="T883" id="Seg_8827" s="T882">döbər</ta>
            <ta e="T884" id="Seg_8828" s="T883">özer-bi</ta>
            <ta e="T885" id="Seg_8829" s="T884">tüj</ta>
            <ta e="T886" id="Seg_8830" s="T885">dĭn</ta>
            <ta e="T887" id="Seg_8831" s="T886">nʼuʔdə</ta>
            <ta e="T888" id="Seg_8832" s="T887">bar</ta>
            <ta e="T889" id="Seg_8833" s="T888">oʔbdo</ta>
            <ta e="T890" id="Seg_8834" s="T889">dĭ</ta>
            <ta e="T891" id="Seg_8835" s="T890">dĭbər</ta>
            <ta e="T892" id="Seg_8836" s="T891">krɨša-bə</ta>
            <ta e="T894" id="Seg_8837" s="T893">dĭbər</ta>
            <ta e="T895" id="Seg_8838" s="T894">kuŋgə-ŋ</ta>
            <ta e="T896" id="Seg_8839" s="T895">özer-bi</ta>
            <ta e="T897" id="Seg_8840" s="T896">daže</ta>
            <ta e="T898" id="Seg_8841" s="T897">nʼuʔdə</ta>
            <ta e="T899" id="Seg_8842" s="T898">dĭgəttə</ta>
            <ta e="T900" id="Seg_8843" s="T899">büzʼe</ta>
            <ta e="T901" id="Seg_8844" s="T900">nadə</ta>
            <ta e="T902" id="Seg_8845" s="T901">kan-zittə</ta>
            <ta e="T903" id="Seg_8846" s="T902">oʔbdə-zittə</ta>
            <ta e="T904" id="Seg_8847" s="T903">žoludʼi</ta>
            <ta e="T905" id="Seg_8848" s="T904">sʼa-bi</ta>
            <ta e="T906" id="Seg_8849" s="T905">sʼa-bi</ta>
            <ta e="T908" id="Seg_8850" s="T907">dĭbər</ta>
            <ta e="T909" id="Seg_8851" s="T908">nʼuʔdə</ta>
            <ta e="T910" id="Seg_8852" s="T909">dĭn</ta>
            <ta e="T911" id="Seg_8853" s="T910">ku-laʔbə</ta>
            <ta e="T912" id="Seg_8854" s="T911">tura</ta>
            <ta e="T914" id="Seg_8855" s="T913">nu-gA</ta>
            <ta e="T915" id="Seg_8856" s="T914">dĭbər</ta>
            <ta e="T916" id="Seg_8857" s="T915">tura-Tə</ta>
            <ta e="T917" id="Seg_8858" s="T916">šo-bi</ta>
            <ta e="T918" id="Seg_8859" s="T917">dĭn</ta>
            <ta e="T919" id="Seg_8860" s="T918">kuriza-n</ta>
            <ta e="T920" id="Seg_8861" s="T919">tibi</ta>
            <ta e="T921" id="Seg_8862" s="T920">amno-laʔbə</ta>
            <ta e="T922" id="Seg_8863" s="T921">ulu-t</ta>
            <ta e="T923" id="Seg_8864" s="T922">kuvas</ta>
            <ta e="T924" id="Seg_8865" s="T923">kömə</ta>
            <ta e="T925" id="Seg_8866" s="T924">bar</ta>
            <ta e="T926" id="Seg_8867" s="T925">i</ta>
            <ta e="T927" id="Seg_8868" s="T926">dĭn</ta>
            <ta e="T928" id="Seg_8869" s="T927">žernov-zAŋ-də</ta>
            <ta e="T929" id="Seg_8870" s="T928">i-gA</ta>
            <ta e="T930" id="Seg_8871" s="T929">dĭ</ta>
            <ta e="T931" id="Seg_8872" s="T930">i-bi</ta>
            <ta e="T932" id="Seg_8873" s="T931">dĭ-m</ta>
            <ta e="T933" id="Seg_8874" s="T932">i</ta>
            <ta e="T934" id="Seg_8875" s="T933">šo-bi</ta>
            <ta e="T935" id="Seg_8876" s="T934">na</ta>
            <ta e="T936" id="Seg_8877" s="T935">tănan</ta>
            <ta e="T937" id="Seg_8878" s="T936">kuriza-n</ta>
            <ta e="T938" id="Seg_8879" s="T937">tibi</ta>
            <ta e="T939" id="Seg_8880" s="T938">kömə</ta>
            <ta e="T940" id="Seg_8881" s="T939">ulu-t</ta>
            <ta e="T941" id="Seg_8882" s="T940">dĭ</ta>
            <ta e="T942" id="Seg_8883" s="T941">i-bi</ta>
            <ta e="T943" id="Seg_8884" s="T942">da</ta>
            <ta e="T944" id="Seg_8885" s="T943">davaj</ta>
            <ta e="T945" id="Seg_8886" s="T944">žernov</ta>
            <ta e="T946" id="Seg_8887" s="T945">dĭ</ta>
            <ta e="T947" id="Seg_8888" s="T946">tʼermən-də</ta>
            <ta e="T948" id="Seg_8889" s="T947">kur-zittə</ta>
            <ta e="T949" id="Seg_8890" s="T948">dĭn</ta>
            <ta e="T950" id="Seg_8891" s="T949">blin-jəʔ</ta>
            <ta e="T951" id="Seg_8892" s="T950">pirog-jəʔ</ta>
            <ta e="T952" id="Seg_8893" s="T951">blin-jəʔ</ta>
            <ta e="T953" id="Seg_8894" s="T952">pirog-jəʔ</ta>
            <ta e="T954" id="Seg_8895" s="T953">büzʼe</ta>
            <ta e="T955" id="Seg_8896" s="T954">am-bi</ta>
            <ta e="T956" id="Seg_8897" s="T955">i</ta>
            <ta e="T957" id="Seg_8898" s="T956">nüke</ta>
            <ta e="T958" id="Seg_8899" s="T957">am-bi</ta>
            <ta e="T959" id="Seg_8900" s="T958">üge</ta>
            <ta e="T960" id="Seg_8901" s="T959">dăra</ta>
            <ta e="T961" id="Seg_8902" s="T960">amno-lAʔ</ta>
            <ta e="T962" id="Seg_8903" s="T961">dĭgəttə</ta>
            <ta e="T963" id="Seg_8904" s="T962">šo-bi</ta>
            <ta e="T964" id="Seg_8905" s="T963">dĭ-zAŋ-Tə</ta>
            <ta e="T965" id="Seg_8906" s="T964">barin</ta>
            <ta e="T966" id="Seg_8907" s="T965">det-KAʔ</ta>
            <ta e="T967" id="Seg_8908" s="T966">măna</ta>
            <ta e="T968" id="Seg_8909" s="T967">am-zittə</ta>
            <ta e="T969" id="Seg_8910" s="T968">dĭ</ta>
            <ta e="T970" id="Seg_8911" s="T969">nünə-bi</ta>
            <ta e="T971" id="Seg_8912" s="T970">pravda</ta>
            <ta e="T972" id="Seg_8913" s="T971">dĭ</ta>
            <ta e="T974" id="Seg_8914" s="T973">dĭ</ta>
            <ta e="T976" id="Seg_8915" s="T975">tʼermən</ta>
            <ta e="T977" id="Seg_8916" s="T976">dĭgəttə</ta>
            <ta e="T978" id="Seg_8917" s="T977">nüke-t</ta>
            <ta e="T979" id="Seg_8918" s="T978">bar</ta>
            <ta e="T980" id="Seg_8919" s="T979">davaj</ta>
            <ta e="T981" id="Seg_8920" s="T980">tʼermən-də</ta>
            <ta e="T982" id="Seg_8921" s="T981">kur-zittə</ta>
            <ta e="T983" id="Seg_8922" s="T982">blin-jəʔ</ta>
            <ta e="T984" id="Seg_8923" s="T983">pirog-jəʔ</ta>
            <ta e="T985" id="Seg_8924" s="T984">dĭ</ta>
            <ta e="T986" id="Seg_8925" s="T985">am-bi</ta>
            <ta e="T987" id="Seg_8926" s="T986">sădar-KAʔ</ta>
            <ta e="T988" id="Seg_8927" s="T987">măna</ta>
            <ta e="T989" id="Seg_8928" s="T988">dʼok</ta>
            <ta e="T990" id="Seg_8929" s="T989">miʔ</ta>
            <ta e="T991" id="Seg_8930" s="T990">ej</ta>
            <ta e="T992" id="Seg_8931" s="T991">sădar-lV-m</ta>
            <ta e="T993" id="Seg_8932" s="T992">ato</ta>
            <ta e="T995" id="Seg_8933" s="T994">miʔnʼibeʔ</ta>
            <ta e="T996" id="Seg_8934" s="T995">am-zittə</ta>
            <ta e="T997" id="Seg_8935" s="T996">ĭmbi</ta>
            <ta e="T998" id="Seg_8936" s="T997">dĭgəttə</ta>
            <ta e="T999" id="Seg_8937" s="T998">dĭ</ta>
            <ta e="T1000" id="Seg_8938" s="T999">nüdʼi-n</ta>
            <ta e="T1001" id="Seg_8939" s="T1000">šo-bi</ta>
            <ta e="T1002" id="Seg_8940" s="T1001">i</ta>
            <ta e="T1003" id="Seg_8941" s="T1002">tojar-lAʔ</ta>
            <ta e="T1004" id="Seg_8942" s="T1003">i-luʔbdə-bi</ta>
            <ta e="T1005" id="Seg_8943" s="T1004">i</ta>
            <ta e="T1006" id="Seg_8944" s="T1005">kon-laːm-bi</ta>
            <ta e="T1007" id="Seg_8945" s="T1006">ertə-n</ta>
            <ta e="T1008" id="Seg_8946" s="T1007">dĭ-zAŋ</ta>
            <ta e="T1009" id="Seg_8947" s="T1008">uʔbdə-bi-jəʔ</ta>
            <ta e="T1010" id="Seg_8948" s="T1009">naga</ta>
            <ta e="T1011" id="Seg_8949" s="T1010">tʼermən-də</ta>
            <ta e="T1012" id="Seg_8950" s="T1011">dĭgəttə</ta>
            <ta e="T1013" id="Seg_8951" s="T1012">tibi</ta>
            <ta e="T1014" id="Seg_8952" s="T1013">kuriza</ta>
            <ta e="T1015" id="Seg_8953" s="T1014">e-ʔ</ta>
            <ta e="T1016" id="Seg_8954" s="T1015">tʼor-KAʔ</ta>
            <ta e="T1017" id="Seg_8955" s="T1016">măn</ta>
            <ta e="T1018" id="Seg_8956" s="T1017">kan-lV-m</ta>
            <ta e="T1020" id="Seg_8957" s="T1019">det-lV-m</ta>
            <ta e="T1021" id="Seg_8958" s="T1020">bazoʔ</ta>
            <ta e="T1022" id="Seg_8959" s="T1021">šiʔnʼileʔ</ta>
            <ta e="T1023" id="Seg_8960" s="T1022">kan-bi</ta>
            <ta e="T1024" id="Seg_8961" s="T1023">kandə-gA</ta>
            <ta e="T1025" id="Seg_8962" s="T1024">kandə-gA</ta>
            <ta e="T1026" id="Seg_8963" s="T1025">šonə-gA</ta>
            <ta e="T1027" id="Seg_8964" s="T1026">dĭ-Tə</ta>
            <ta e="T1028" id="Seg_8965" s="T1027">lʼisʼitsa</ta>
            <ta e="T1029" id="Seg_8966" s="T1028">gibər</ta>
            <ta e="T1030" id="Seg_8967" s="T1029">kandə-gA-l</ta>
            <ta e="T1031" id="Seg_8968" s="T1030">i-ʔ</ta>
            <ta e="T1032" id="Seg_8969" s="T1031">măna</ta>
            <ta e="T1033" id="Seg_8970" s="T1032">băd-ə-ʔ</ta>
            <ta e="T1034" id="Seg_8971" s="T1033">măna</ta>
            <ta e="T1035" id="Seg_8972" s="T1034">šüjə-gəndə</ta>
            <ta e="T1036" id="Seg_8973" s="T1035">dĭ</ta>
            <ta e="T1037" id="Seg_8974" s="T1036">băd-bi</ta>
            <ta e="T1038" id="Seg_8975" s="T1037">dĭgəttə</ta>
            <ta e="T1039" id="Seg_8976" s="T1038">šonə-gA</ta>
            <ta e="T1040" id="Seg_8977" s="T1039">šonə-gA</ta>
            <ta e="T1041" id="Seg_8978" s="T1040">volk-Tə</ta>
            <ta e="T1042" id="Seg_8979" s="T1041">gibər</ta>
            <ta e="T1043" id="Seg_8980" s="T1042">kandə-gA-l</ta>
            <ta e="T1044" id="Seg_8981" s="T1043">tʼermən-də</ta>
            <ta e="T1045" id="Seg_8982" s="T1044">i-zittə</ta>
            <ta e="T1047" id="Seg_8983" s="T1046">i-ʔ</ta>
            <ta e="T1048" id="Seg_8984" s="T1047">măna</ta>
            <ta e="T1049" id="Seg_8985" s="T1048">băd-ə-ʔ</ta>
            <ta e="T1050" id="Seg_8986" s="T1049">măna</ta>
            <ta e="T1051" id="Seg_8987" s="T1050">šüjə-gəndə</ta>
            <ta e="T1052" id="Seg_8988" s="T1051">dĭ</ta>
            <ta e="T1053" id="Seg_8989" s="T1052">băt-bi</ta>
            <ta e="T1054" id="Seg_8990" s="T1053">dĭgəttə</ta>
            <ta e="T1055" id="Seg_8991" s="T1054">šonə-gA</ta>
            <ta e="T1056" id="Seg_8992" s="T1055">urgaːba</ta>
            <ta e="T1057" id="Seg_8993" s="T1056">šonə-gA</ta>
            <ta e="T1058" id="Seg_8994" s="T1057">băd-ə-ʔ</ta>
            <ta e="T1059" id="Seg_8995" s="T1058">măna</ta>
            <ta e="T1060" id="Seg_8996" s="T1059">dĭgəttə</ta>
            <ta e="T1061" id="Seg_8997" s="T1060">šo-bi</ta>
            <ta e="T1062" id="Seg_8998" s="T1061">davaj</ta>
            <ta e="T1063" id="Seg_8999" s="T1062">kirgaːr-zittə</ta>
            <ta e="T1065" id="Seg_9000" s="T1064">det-t</ta>
            <ta e="T1066" id="Seg_9001" s="T1065">măn</ta>
            <ta e="T1067" id="Seg_9002" s="T1066">tʼermən-də</ta>
            <ta e="T1068" id="Seg_9003" s="T1067">det-t</ta>
            <ta e="T1069" id="Seg_9004" s="T1068">măn</ta>
            <ta e="T1070" id="Seg_9005" s="T1069">tʼermən-də</ta>
            <ta e="T1071" id="Seg_9006" s="T1070">dĭ</ta>
            <ta e="T1072" id="Seg_9007" s="T1071">măn-ntə</ta>
            <ta e="T1074" id="Seg_9008" s="T1073">baʔbdə-KAʔ</ta>
            <ta e="T1075" id="Seg_9009" s="T1074">dĭ-m</ta>
            <ta e="T1076" id="Seg_9010" s="T1075">a</ta>
            <ta e="T1077" id="Seg_9011" s="T1076">nabə-jəʔ-Tə</ta>
            <ta e="T1078" id="Seg_9012" s="T1077">pušaj</ta>
            <ta e="T1079" id="Seg_9013" s="T1078">nabə-jəʔ</ta>
            <ta e="T1080" id="Seg_9014" s="T1079">am-luʔbdə-bə</ta>
            <ta e="T1081" id="Seg_9015" s="T1080">lə-jəʔ</ta>
            <ta e="T1082" id="Seg_9016" s="T1081">dĭ-m</ta>
            <ta e="T1083" id="Seg_9017" s="T1082">baʔbdə-luʔbdə-bi</ta>
            <ta e="T1084" id="Seg_9018" s="T1083">a</ta>
            <ta e="T1085" id="Seg_9019" s="T1084">dĭ</ta>
            <ta e="T1086" id="Seg_9020" s="T1085">măn-ntə</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PKZ">
            <ta e="T5" id="Seg_9021" s="T4">live-PST.[3SG]</ta>
            <ta e="T6" id="Seg_9022" s="T5">man.[NOM.SG]</ta>
            <ta e="T7" id="Seg_9023" s="T6">this-GEN</ta>
            <ta e="T8" id="Seg_9024" s="T7">three.[NOM.SG]</ta>
            <ta e="T9" id="Seg_9025" s="T8">boy.[NOM.SG]</ta>
            <ta e="T10" id="Seg_9026" s="T9">be-PST.[3SG]</ta>
            <ta e="T11" id="Seg_9027" s="T10">one.[NOM.SG]</ta>
            <ta e="T12" id="Seg_9028" s="T11">Gavrila.[NOM.SG]</ta>
            <ta e="T13" id="Seg_9029" s="T12">one.[NOM.SG]</ta>
            <ta e="T14" id="Seg_9030" s="T13">Danila.[NOM.SG]</ta>
            <ta e="T15" id="Seg_9031" s="T14">one.[NOM.SG]</ta>
            <ta e="T16" id="Seg_9032" s="T15">Vanyushka.[NOM.SG]</ta>
            <ta e="T17" id="Seg_9033" s="T16">fool.[NOM.SG]</ta>
            <ta e="T18" id="Seg_9034" s="T17">then</ta>
            <ta e="T20" id="Seg_9035" s="T19">this.[NOM.SG]</ta>
            <ta e="T21" id="Seg_9036" s="T20">man.[NOM.SG]</ta>
            <ta e="T22" id="Seg_9037" s="T21">sow-DUR.PST.[3SG]</ta>
            <ta e="T23" id="Seg_9038" s="T22">wheat.[NOM.SG]</ta>
            <ta e="T24" id="Seg_9039" s="T23">and</ta>
            <ta e="T25" id="Seg_9040" s="T24">this.[NOM.SG]</ta>
            <ta e="T26" id="Seg_9041" s="T25">who.[NOM.SG]=INDEF</ta>
            <ta e="T29" id="Seg_9042" s="T28">evening-LOC.ADV</ta>
            <ta e="T30" id="Seg_9043" s="T29">come-MOM-3PL</ta>
            <ta e="T31" id="Seg_9044" s="T30">and</ta>
            <ta e="T32" id="Seg_9045" s="T31">eat-DUR.[3SG]</ta>
            <ta e="T33" id="Seg_9046" s="T32">wheat</ta>
            <ta e="T34" id="Seg_9047" s="T33">one.should</ta>
            <ta e="T35" id="Seg_9048" s="T34">say-IPFVZ.[3SG]</ta>
            <ta e="T36" id="Seg_9049" s="T35">go-INF.LAT</ta>
            <ta e="T38" id="Seg_9050" s="T37">look-INF.LAT</ta>
            <ta e="T39" id="Seg_9051" s="T38">who.[NOM.SG]</ta>
            <ta e="T40" id="Seg_9052" s="T39">eat-PRS-3SG.O</ta>
            <ta e="T41" id="Seg_9053" s="T40">wheat.[NOM.SG]</ta>
            <ta e="T42" id="Seg_9054" s="T41">one.should</ta>
            <ta e="T43" id="Seg_9055" s="T42">capture-INF.LAT</ta>
            <ta e="T44" id="Seg_9056" s="T43">then</ta>
            <ta e="T45" id="Seg_9057" s="T44">go-PST.[3SG]</ta>
            <ta e="T46" id="Seg_9058" s="T45">elder.[NOM.SG]</ta>
            <ta e="T47" id="Seg_9059" s="T46">son-NOM/GEN.3SG</ta>
            <ta e="T48" id="Seg_9060" s="T47">Gavrila.[NOM.SG]</ta>
            <ta e="T49" id="Seg_9061" s="T48">there</ta>
            <ta e="T50" id="Seg_9062" s="T49">go-PST.[3SG]</ta>
            <ta e="T51" id="Seg_9063" s="T50">sleep-PST.[3SG]</ta>
            <ta e="T52" id="Seg_9064" s="T51">grass-LOC</ta>
            <ta e="T53" id="Seg_9065" s="T52">and</ta>
            <ta e="T54" id="Seg_9066" s="T53">come-PST.[3SG]</ta>
            <ta e="T55" id="Seg_9067" s="T54">house-LAT</ta>
            <ta e="T56" id="Seg_9068" s="T55">sit-PST-1SG</ta>
            <ta e="T57" id="Seg_9069" s="T56">NEG</ta>
            <ta e="T58" id="Seg_9070" s="T57">sleep-PST-1SG</ta>
            <ta e="T59" id="Seg_9071" s="T58">and</ta>
            <ta e="T60" id="Seg_9072" s="T59">who.[NOM.SG]=INDEF</ta>
            <ta e="T61" id="Seg_9073" s="T60">NEG</ta>
            <ta e="T62" id="Seg_9074" s="T61">see-PST-1SG</ta>
            <ta e="T63" id="Seg_9075" s="T62">then</ta>
            <ta e="T64" id="Seg_9076" s="T63">another.[NOM.SG]</ta>
            <ta e="T65" id="Seg_9077" s="T64">son-NOM/GEN.3SG</ta>
            <ta e="T66" id="Seg_9078" s="T65">walk-PRS.[3SG]</ta>
            <ta e="T67" id="Seg_9079" s="T66">Danila-NOM/GEN.3SG</ta>
            <ta e="T68" id="Seg_9080" s="T67">go-PST.[3SG]</ta>
            <ta e="T69" id="Seg_9081" s="T68">sleep-PST.[3SG]</ta>
            <ta e="T70" id="Seg_9082" s="T69">grass-LOC</ta>
            <ta e="T71" id="Seg_9083" s="T70">then</ta>
            <ta e="T72" id="Seg_9084" s="T71">morning-LOC.ADV</ta>
            <ta e="T73" id="Seg_9085" s="T72">come-PST.[3SG]</ta>
            <ta e="T74" id="Seg_9086" s="T73">all</ta>
            <ta e="T75" id="Seg_9087" s="T74">evening.[NOM.SG]</ta>
            <ta e="T76" id="Seg_9088" s="T75">NEG</ta>
            <ta e="T77" id="Seg_9089" s="T76">sleep-PST-1SG</ta>
            <ta e="T78" id="Seg_9090" s="T77">and</ta>
            <ta e="T79" id="Seg_9091" s="T78">who-ACC=INDEF</ta>
            <ta e="T80" id="Seg_9092" s="T79">NEG</ta>
            <ta e="T81" id="Seg_9093" s="T80">see-PST-1SG</ta>
            <ta e="T82" id="Seg_9094" s="T81">then</ta>
            <ta e="T83" id="Seg_9095" s="T82">Vanyushka.[NOM.SG]</ta>
            <ta e="T84" id="Seg_9096" s="T83">go-PST.[3SG]</ta>
            <ta e="T85" id="Seg_9097" s="T84">take-PST.[3SG]</ta>
            <ta e="T86" id="Seg_9098" s="T85">lasso-NOM/GEN/ACC.3SG</ta>
            <ta e="T87" id="Seg_9099" s="T86">then</ta>
            <ta e="T88" id="Seg_9100" s="T87">sit-PST.[3SG]</ta>
            <ta e="T89" id="Seg_9101" s="T88">stone-LAT</ta>
            <ta e="T90" id="Seg_9102" s="T89">NEG</ta>
            <ta e="T91" id="Seg_9103" s="T90">sleep-PRS.[3SG]</ta>
            <ta e="T92" id="Seg_9104" s="T91">sit-DUR.[3SG]</ta>
            <ta e="T93" id="Seg_9105" s="T92">see-PRS-3SG.O</ta>
            <ta e="T94" id="Seg_9106" s="T93">horse.[NOM.SG]</ta>
            <ta e="T95" id="Seg_9107" s="T94">come-PST.[3SG]</ta>
            <ta e="T96" id="Seg_9108" s="T95">sundry</ta>
            <ta e="T97" id="Seg_9109" s="T96">and</ta>
            <ta e="T98" id="Seg_9110" s="T97">PTCL</ta>
            <ta e="T99" id="Seg_9111" s="T98">there</ta>
            <ta e="T100" id="Seg_9112" s="T99">skin-LOC</ta>
            <ta e="T101" id="Seg_9113" s="T100">hair-NOM/GEN/ACC.3SG</ta>
            <ta e="T102" id="Seg_9114" s="T101">beautiful.[NOM.SG]</ta>
            <ta e="T103" id="Seg_9115" s="T102">this.[NOM.SG]</ta>
            <ta e="T104" id="Seg_9116" s="T103">PTCL</ta>
            <ta e="T105" id="Seg_9117" s="T104">throw-MOM-PST.[3SG]</ta>
            <ta e="T106" id="Seg_9118" s="T105">lasso-NOM/GEN/ACC.3SG</ta>
            <ta e="T107" id="Seg_9119" s="T106">this-LAT</ta>
            <ta e="T108" id="Seg_9120" s="T107">and</ta>
            <ta e="T109" id="Seg_9121" s="T108">capture-MOM-PST.[3SG]</ta>
            <ta e="T110" id="Seg_9122" s="T109">this.[NOM.SG]</ta>
            <ta e="T111" id="Seg_9123" s="T110">jump-MOM-PST.[3SG]</ta>
            <ta e="T112" id="Seg_9124" s="T111">NEG</ta>
            <ta e="T113" id="Seg_9125" s="T112">can-PRS.[3SG]</ta>
            <ta e="T114" id="Seg_9126" s="T113">jump-INF.LAT</ta>
            <ta e="T115" id="Seg_9127" s="T114">then</ta>
            <ta e="T116" id="Seg_9128" s="T115">let-IMP.2SG</ta>
            <ta e="T117" id="Seg_9129" s="T116">I.ACC</ta>
            <ta e="T118" id="Seg_9130" s="T117">I.NOM</ta>
            <ta e="T119" id="Seg_9131" s="T118">you.DAT</ta>
            <ta e="T120" id="Seg_9132" s="T119">PTCL</ta>
            <ta e="T121" id="Seg_9133" s="T120">what.[NOM.SG]</ta>
            <ta e="T122" id="Seg_9134" s="T121">give-FUT-1SG</ta>
            <ta e="T123" id="Seg_9135" s="T122">well</ta>
            <ta e="T124" id="Seg_9136" s="T123">how</ta>
            <ta e="T125" id="Seg_9137" s="T124">tell-IMP.2SG</ta>
            <ta e="T126" id="Seg_9138" s="T125">else</ta>
            <ta e="T127" id="Seg_9139" s="T126">NEG</ta>
            <ta e="T128" id="Seg_9140" s="T127">eat-PRS-2SG</ta>
            <ta e="T129" id="Seg_9141" s="T128">wheat.[NOM.SG]</ta>
            <ta e="T130" id="Seg_9142" s="T129">NEG</ta>
            <ta e="T131" id="Seg_9143" s="T130">eat-FUT-1SG</ta>
            <ta e="T132" id="Seg_9144" s="T131">and</ta>
            <ta e="T133" id="Seg_9145" s="T132">how</ta>
            <ta e="T134" id="Seg_9146" s="T133">come-FUT-2SG</ta>
            <ta e="T135" id="Seg_9147" s="T134">so</ta>
            <ta e="T136" id="Seg_9148" s="T135">I.LAT</ta>
            <ta e="T137" id="Seg_9149" s="T136">come-IMP.2SG</ta>
            <ta e="T138" id="Seg_9150" s="T137">here</ta>
            <ta e="T139" id="Seg_9151" s="T138">%%-IMP.2SG</ta>
            <ta e="T141" id="Seg_9152" s="T140">I.ACC</ta>
            <ta e="T142" id="Seg_9153" s="T141">well</ta>
            <ta e="T143" id="Seg_9154" s="T142">such.[NOM.SG]</ta>
            <ta e="T144" id="Seg_9155" s="T143">and</ta>
            <ta e="T145" id="Seg_9156" s="T144">go-CVB</ta>
            <ta e="T146" id="Seg_9157" s="T145">disappear-PST.[3SG]</ta>
            <ta e="T147" id="Seg_9158" s="T146">horse-NOM/GEN.3SG</ta>
            <ta e="T148" id="Seg_9159" s="T147">and</ta>
            <ta e="T149" id="Seg_9160" s="T148">this.[NOM.SG]</ta>
            <ta e="T150" id="Seg_9161" s="T149">come-PST.[3SG]</ta>
            <ta e="T151" id="Seg_9162" s="T150">house-LAT/LOC.3SG</ta>
            <ta e="T152" id="Seg_9163" s="T151">stove-LAT</ta>
            <ta e="T153" id="Seg_9164" s="T152">dress-MOM-PST.[3SG]</ta>
            <ta e="T154" id="Seg_9165" s="T153">I.NOM</ta>
            <ta e="T155" id="Seg_9166" s="T154">capture-PST-1SG</ta>
            <ta e="T156" id="Seg_9167" s="T155">horse.[NOM.SG]</ta>
            <ta e="T157" id="Seg_9168" s="T156">there</ta>
            <ta e="T158" id="Seg_9169" s="T157">now</ta>
            <ta e="T159" id="Seg_9170" s="T158">when=INDEF</ta>
            <ta e="T160" id="Seg_9171" s="T159">NEG</ta>
            <ta e="T161" id="Seg_9172" s="T160">come-FUT-3SG</ta>
            <ta e="T162" id="Seg_9173" s="T161">eat-INF.LAT</ta>
            <ta e="T163" id="Seg_9174" s="T162">wheat.[NOM.SG]</ta>
            <ta e="T164" id="Seg_9175" s="T163">then</ta>
            <ta e="T165" id="Seg_9176" s="T164">chief.[NOM.SG]</ta>
            <ta e="T166" id="Seg_9177" s="T165">make-PST.[3SG]</ta>
            <ta e="T167" id="Seg_9178" s="T166">so.that</ta>
            <ta e="T168" id="Seg_9179" s="T167">people.[NOM.SG]</ta>
            <ta e="T169" id="Seg_9180" s="T168">come-PST-3PL</ta>
            <ta e="T171" id="Seg_9181" s="T170">this-GEN</ta>
            <ta e="T172" id="Seg_9182" s="T171">girl.[NOM.SG]</ta>
            <ta e="T173" id="Seg_9183" s="T172">live-DUR.[3SG]</ta>
            <ta e="T174" id="Seg_9184" s="T173">tower-LOC</ta>
            <ta e="T175" id="Seg_9185" s="T174">all</ta>
            <ta e="T176" id="Seg_9186" s="T175">people.[NOM.SG]</ta>
            <ta e="T177" id="Seg_9187" s="T176">go-DUR-3PL</ta>
            <ta e="T178" id="Seg_9188" s="T177">and</ta>
            <ta e="T179" id="Seg_9189" s="T178">and</ta>
            <ta e="T180" id="Seg_9190" s="T179">this-GEN</ta>
            <ta e="T181" id="Seg_9191" s="T180">brother-PL</ta>
            <ta e="T182" id="Seg_9192" s="T181">go-IPFVZ-PRS-3PL</ta>
            <ta e="T183" id="Seg_9193" s="T182">and</ta>
            <ta e="T184" id="Seg_9194" s="T183">this.[NOM.SG]</ta>
            <ta e="T185" id="Seg_9195" s="T184">say-IPFVZ.[3SG]</ta>
            <ta e="T186" id="Seg_9196" s="T185">take-IMP.2PL</ta>
            <ta e="T187" id="Seg_9197" s="T186">I.ACC</ta>
            <ta e="T188" id="Seg_9198" s="T187">where.to</ta>
            <ta e="T189" id="Seg_9199" s="T188">you.GEN</ta>
            <ta e="T190" id="Seg_9200" s="T189">go-FUT-2SG</ta>
            <ta e="T191" id="Seg_9201" s="T190">what.[NOM.SG]=INDEF</ta>
            <ta e="T192" id="Seg_9202" s="T191">there</ta>
            <ta e="T193" id="Seg_9203" s="T192">NEG</ta>
            <ta e="T194" id="Seg_9204" s="T193">see-PST-2SG</ta>
            <ta e="T195" id="Seg_9205" s="T194">so.that</ta>
            <ta e="T196" id="Seg_9206" s="T195">people.[NOM.SG]</ta>
            <ta e="T197" id="Seg_9207" s="T196">laugh-FRQ-PST-3PL</ta>
            <ta e="T199" id="Seg_9208" s="T198">you.ACC</ta>
            <ta e="T200" id="Seg_9209" s="T199">this-PL</ta>
            <ta e="T201" id="Seg_9210" s="T200">go-CVB</ta>
            <ta e="T202" id="Seg_9211" s="T201">disappear-PST-3PL</ta>
            <ta e="T203" id="Seg_9212" s="T202">this.[NOM.SG]</ta>
            <ta e="T204" id="Seg_9213" s="T203">get.up-PST.[3SG]</ta>
            <ta e="T205" id="Seg_9214" s="T204">basket.[NOM.SG]</ta>
            <ta e="T206" id="Seg_9215" s="T205">take-PST.[3SG]</ta>
            <ta e="T207" id="Seg_9216" s="T206">go-PST.[3SG]</ta>
            <ta e="T208" id="Seg_9217" s="T207">and</ta>
            <ta e="T209" id="Seg_9218" s="T208">INCH</ta>
            <ta e="T210" id="Seg_9219" s="T209">shout-INF.LAT</ta>
            <ta e="T211" id="Seg_9220" s="T210">horse-NOM/GEN.3SG</ta>
            <ta e="T212" id="Seg_9221" s="T211">come-PST.[3SG]</ta>
            <ta e="T213" id="Seg_9222" s="T212">PTCL</ta>
            <ta e="T214" id="Seg_9223" s="T213">here</ta>
            <ta e="T215" id="Seg_9224" s="T214">%%-LOC</ta>
            <ta e="T216" id="Seg_9225" s="T215">come-PRS-3PL</ta>
            <ta e="T217" id="Seg_9226" s="T216">that.[NOM.SG]</ta>
            <ta e="T218" id="Seg_9227" s="T217">man-LAT/LOC.3SG</ta>
            <ta e="T219" id="Seg_9228" s="T218">come-PRS.[3SG]</ta>
            <ta e="T220" id="Seg_9229" s="T219">then</ta>
            <ta e="T221" id="Seg_9230" s="T220">this.[NOM.SG]</ta>
            <ta e="T222" id="Seg_9231" s="T221">horse.[NOM.SG]</ta>
            <ta e="T224" id="Seg_9232" s="T222">say-IPFVZ.[3SG]</ta>
            <ta e="T225" id="Seg_9233" s="T224">stand-PST.[3SG]</ta>
            <ta e="T226" id="Seg_9234" s="T225">this-LAT</ta>
            <ta e="T227" id="Seg_9235" s="T226">say-IPFVZ.[3SG]</ta>
            <ta e="T228" id="Seg_9236" s="T227">one.[NOM.SG]</ta>
            <ta e="T229" id="Seg_9237" s="T228">ear-LAT</ta>
            <ta e="T230" id="Seg_9238" s="T229">climb-IMP.2PL</ta>
            <ta e="T231" id="Seg_9239" s="T230">one-ABL</ta>
            <ta e="T232" id="Seg_9240" s="T231">depart-IMP.2SG</ta>
            <ta e="T233" id="Seg_9241" s="T232">this.[NOM.SG]</ta>
            <ta e="T234" id="Seg_9242" s="T233">climb-PST.[3SG]</ta>
            <ta e="T235" id="Seg_9243" s="T234">depart-PST.[3SG]</ta>
            <ta e="T236" id="Seg_9244" s="T235">beautiful.[NOM.SG]</ta>
            <ta e="T237" id="Seg_9245" s="T236">become-RES-PST.[3SG]</ta>
            <ta e="T238" id="Seg_9246" s="T237">then</ta>
            <ta e="T239" id="Seg_9247" s="T238">this-LAT</ta>
            <ta e="T240" id="Seg_9248" s="T239">sit-PST.[3SG]</ta>
            <ta e="T241" id="Seg_9249" s="T240">this.[NOM.SG]</ta>
            <ta e="T242" id="Seg_9250" s="T241">run</ta>
            <ta e="T243" id="Seg_9251" s="T242">run-MOM-PST.[3SG]</ta>
            <ta e="T244" id="Seg_9252" s="T243">there</ta>
            <ta e="T245" id="Seg_9253" s="T244">people.[NOM.SG]</ta>
            <ta e="T246" id="Seg_9254" s="T245">many</ta>
            <ta e="T247" id="Seg_9255" s="T246">stand-PRS-3PL</ta>
            <ta e="T248" id="Seg_9256" s="T247">and</ta>
            <ta e="T249" id="Seg_9257" s="T248">who.[NOM.SG]=INDEF</ta>
            <ta e="T250" id="Seg_9258" s="T249">NEG</ta>
            <ta e="T251" id="Seg_9259" s="T250">this.[NOM.SG]</ta>
            <ta e="T252" id="Seg_9260" s="T251">daughter-GEN.1SG</ta>
            <ta e="T253" id="Seg_9261" s="T252">NEG</ta>
            <ta e="T254" id="Seg_9262" s="T253">climb-PRS-3PL</ta>
            <ta e="T255" id="Seg_9263" s="T254">this.[NOM.SG]</ta>
            <ta e="T256" id="Seg_9264" s="T255">PTCL</ta>
            <ta e="T257" id="Seg_9265" s="T256">jump-MOM-PST.[3SG]</ta>
            <ta e="T258" id="Seg_9266" s="T257">hide-FUT-3PL</ta>
            <ta e="T259" id="Seg_9267" s="T258">NEG</ta>
            <ta e="T260" id="Seg_9268" s="T259">NEG</ta>
            <ta e="T261" id="Seg_9269" s="T260">capture-PST.[3SG]</ta>
            <ta e="T262" id="Seg_9270" s="T261">then</ta>
            <ta e="T263" id="Seg_9271" s="T262">people.[NOM.SG]</ta>
            <ta e="T264" id="Seg_9272" s="T263">all</ta>
            <ta e="T265" id="Seg_9273" s="T264">shout-DUR-3PL</ta>
            <ta e="T266" id="Seg_9274" s="T265">capture-IMP.2SG</ta>
            <ta e="T267" id="Seg_9275" s="T266">capture-IMP.2SG</ta>
            <ta e="T268" id="Seg_9276" s="T267">and</ta>
            <ta e="T269" id="Seg_9277" s="T268">this.[NOM.SG]</ta>
            <ta e="T270" id="Seg_9278" s="T269">run-MOM-PST.[3SG]</ta>
            <ta e="T271" id="Seg_9279" s="T270">horse-ACC</ta>
            <ta e="T272" id="Seg_9280" s="T271">send-PST.[3SG]</ta>
            <ta e="T273" id="Seg_9281" s="T272">then</ta>
            <ta e="T274" id="Seg_9282" s="T273">mushroom-NOM/GEN/ACC.3PL</ta>
            <ta e="T275" id="Seg_9283" s="T274">collect-PST.[3SG]</ta>
            <ta e="T276" id="Seg_9284" s="T275">and</ta>
            <ta e="T277" id="Seg_9285" s="T276">sundry</ta>
            <ta e="T278" id="Seg_9286" s="T277">many</ta>
            <ta e="T279" id="Seg_9287" s="T278">mushroom.[NOM.SG]</ta>
            <ta e="T280" id="Seg_9288" s="T279">bring-PST.[3SG]</ta>
            <ta e="T281" id="Seg_9289" s="T280">and</ta>
            <ta e="T282" id="Seg_9290" s="T281">woman-PL</ta>
            <ta e="T283" id="Seg_9291" s="T282">scold-DUR-3PL</ta>
            <ta e="T284" id="Seg_9292" s="T283">what.[NOM.SG]</ta>
            <ta e="T285" id="Seg_9293" s="T284">such.[NOM.SG]</ta>
            <ta e="T287" id="Seg_9294" s="T286">bring-PST-2SG</ta>
            <ta e="T288" id="Seg_9295" s="T287">only</ta>
            <ta e="T289" id="Seg_9296" s="T288">you.DAT</ta>
            <ta e="T290" id="Seg_9297" s="T289">eat-INF.LAT</ta>
            <ta e="T291" id="Seg_9298" s="T290">this.[NOM.SG]</ta>
            <ta e="T292" id="Seg_9299" s="T291">stove-LAT</ta>
            <ta e="T294" id="Seg_9300" s="T293">climb-PST.[3SG]</ta>
            <ta e="T295" id="Seg_9301" s="T294">lie-DUR.[3SG]</ta>
            <ta e="T296" id="Seg_9302" s="T295">then</ta>
            <ta e="T298" id="Seg_9303" s="T297">brother-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T299" id="Seg_9304" s="T298">come-PST-3PL</ta>
            <ta e="T300" id="Seg_9305" s="T299">PTCL</ta>
            <ta e="T301" id="Seg_9306" s="T300">tell-DUR-3PL</ta>
            <ta e="T303" id="Seg_9307" s="T302">how</ta>
            <ta e="T304" id="Seg_9308" s="T303">be-PST.[3SG]</ta>
            <ta e="T305" id="Seg_9309" s="T304">and</ta>
            <ta e="T306" id="Seg_9310" s="T305">this.[NOM.SG]</ta>
            <ta e="T307" id="Seg_9311" s="T306">lie-DUR.[3SG]</ta>
            <ta e="T308" id="Seg_9312" s="T307">this.[NOM.SG]</ta>
            <ta e="T309" id="Seg_9313" s="T308">I.GEN</ta>
            <ta e="T310" id="Seg_9314" s="T309">head-NOM/GEN/ACC.1SG</ta>
            <ta e="T311" id="Seg_9315" s="T310">there</ta>
            <ta e="T312" id="Seg_9316" s="T311">be-PST.[3SG]</ta>
            <ta e="T313" id="Seg_9317" s="T312">NEG.AUX-IMP.2SG</ta>
            <ta e="T314" id="Seg_9318" s="T313">lie-CNG</ta>
            <ta e="T315" id="Seg_9319" s="T314">you.GEN</ta>
            <ta e="T316" id="Seg_9320" s="T315">head-NOM/GEN/ACC.1SG</ta>
            <ta e="T317" id="Seg_9321" s="T316">sit-IMP.2SG</ta>
            <ta e="T318" id="Seg_9322" s="T317">PTCL</ta>
            <ta e="T319" id="Seg_9323" s="T318">what.[NOM.SG]=INDEF</ta>
            <ta e="T320" id="Seg_9324" s="T319">NEG.AUX-IMP.2SG</ta>
            <ta e="T321" id="Seg_9325" s="T320">speak-EP-CNG</ta>
            <ta e="T322" id="Seg_9326" s="T321">such.[NOM.SG]</ta>
            <ta e="T323" id="Seg_9327" s="T322">man.[NOM.SG]</ta>
            <ta e="T324" id="Seg_9328" s="T323">be-PST.[3SG]</ta>
            <ta e="T325" id="Seg_9329" s="T324">horse-NOM/GEN.3SG</ta>
            <ta e="T326" id="Seg_9330" s="T325">beautiful.[NOM.SG]</ta>
            <ta e="T327" id="Seg_9331" s="T326">be-PST.[3SG]</ta>
            <ta e="T328" id="Seg_9332" s="T327">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T329" id="Seg_9333" s="T328">then</ta>
            <ta e="T330" id="Seg_9334" s="T329">tomorrow</ta>
            <ta e="T331" id="Seg_9335" s="T330">again</ta>
            <ta e="T332" id="Seg_9336" s="T331">go-IPFVZ-PRS-3PL</ta>
            <ta e="T333" id="Seg_9337" s="T332">go-CVB</ta>
            <ta e="T334" id="Seg_9338" s="T333">disappear-PST-3PL</ta>
            <ta e="T335" id="Seg_9339" s="T334">this.[NOM.SG]</ta>
            <ta e="T336" id="Seg_9340" s="T335">stove-ABL</ta>
            <ta e="T338" id="Seg_9341" s="T337">jump-MOM-PST.[3SG]</ta>
            <ta e="T339" id="Seg_9342" s="T338">basket.[NOM.SG]</ta>
            <ta e="T340" id="Seg_9343" s="T339">take-PST.[3SG]</ta>
            <ta e="T341" id="Seg_9344" s="T340">go-PST.[3SG]</ta>
            <ta e="T342" id="Seg_9345" s="T341">go-PST.[3SG]</ta>
            <ta e="T344" id="Seg_9346" s="T342">throw.away-PST.[3SG]</ta>
            <ta e="T345" id="Seg_9347" s="T344">INCH</ta>
            <ta e="T346" id="Seg_9348" s="T345">horse-ACC</ta>
            <ta e="T347" id="Seg_9349" s="T346">shout-INF.LAT</ta>
            <ta e="T348" id="Seg_9350" s="T347">horse-NOM/GEN.3SG</ta>
            <ta e="T349" id="Seg_9351" s="T348">come-PST.[3SG]</ta>
            <ta e="T350" id="Seg_9352" s="T349">again</ta>
            <ta e="T351" id="Seg_9353" s="T350">one.[NOM.SG]</ta>
            <ta e="T352" id="Seg_9354" s="T351">snot-NOM/GEN/ACC.3SG</ta>
            <ta e="T353" id="Seg_9355" s="T352">cook-RES-PST.[3SG]</ta>
            <ta e="T354" id="Seg_9356" s="T353">one.[NOM.SG]</ta>
            <ta e="T355" id="Seg_9357" s="T354">depart-RES-PST.[3SG]</ta>
            <ta e="T356" id="Seg_9358" s="T355">then</ta>
            <ta e="T357" id="Seg_9359" s="T356">sit.down-PST.[3SG]</ta>
            <ta e="T359" id="Seg_9360" s="T358">come-PRS.[3SG]</ta>
            <ta e="T360" id="Seg_9361" s="T359">people.[NOM.SG]</ta>
            <ta e="T361" id="Seg_9362" s="T360">many</ta>
            <ta e="T362" id="Seg_9363" s="T361">stand-PRS-3PL</ta>
            <ta e="T363" id="Seg_9364" s="T362">then</ta>
            <ta e="T364" id="Seg_9365" s="T363">who.[NOM.SG]=INDEF</ta>
            <ta e="T365" id="Seg_9366" s="T364">NEG</ta>
            <ta e="T366" id="Seg_9367" s="T365">run-PRS.[3SG]</ta>
            <ta e="T367" id="Seg_9368" s="T366">there</ta>
            <ta e="T368" id="Seg_9369" s="T367">there</ta>
            <ta e="T370" id="Seg_9370" s="T369">jump-MOM-PST.[3SG]</ta>
            <ta e="T371" id="Seg_9371" s="T370">take-PST.[3SG]</ta>
            <ta e="T376" id="Seg_9372" s="T375">NEG</ta>
            <ta e="T377" id="Seg_9373" s="T376">can-PST.[3SG]</ta>
            <ta e="T378" id="Seg_9374" s="T377">then</ta>
            <ta e="T379" id="Seg_9375" s="T378">return-MOM-PST.[3SG]</ta>
            <ta e="T380" id="Seg_9376" s="T379">brother-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T381" id="Seg_9377" s="T380">all</ta>
            <ta e="T382" id="Seg_9378" s="T381">beat-PST.[3SG]</ta>
            <ta e="T383" id="Seg_9379" s="T382">and</ta>
            <ta e="T384" id="Seg_9380" s="T383">go-CVB</ta>
            <ta e="T385" id="Seg_9381" s="T384">disappear-PST.[3SG]</ta>
            <ta e="T386" id="Seg_9382" s="T385">and</ta>
            <ta e="T387" id="Seg_9383" s="T386">people.[NOM.SG]</ta>
            <ta e="T388" id="Seg_9384" s="T387">shout-DUR-3PL</ta>
            <ta e="T389" id="Seg_9385" s="T388">capture-IMP.2SG</ta>
            <ta e="T390" id="Seg_9386" s="T389">capture-IMP.2SG</ta>
            <ta e="T391" id="Seg_9387" s="T390">and</ta>
            <ta e="T392" id="Seg_9388" s="T391">this.[NOM.SG]</ta>
            <ta e="T393" id="Seg_9389" s="T392">NEG.AUX-IMP.2SG</ta>
            <ta e="T394" id="Seg_9390" s="T393">give.away-PST</ta>
            <ta e="T395" id="Seg_9391" s="T394">horse-ACC.3SG</ta>
            <ta e="T396" id="Seg_9392" s="T395">send-PST.[3SG]</ta>
            <ta e="T397" id="Seg_9393" s="T396">and</ta>
            <ta e="T398" id="Seg_9394" s="T397">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T399" id="Seg_9395" s="T398">again</ta>
            <ta e="T400" id="Seg_9396" s="T399">come-PST.[3SG]</ta>
            <ta e="T401" id="Seg_9397" s="T400">woman-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T402" id="Seg_9398" s="T401">again</ta>
            <ta e="T403" id="Seg_9399" s="T402">scold-DUR-3PL</ta>
            <ta e="T404" id="Seg_9400" s="T403">and</ta>
            <ta e="T405" id="Seg_9401" s="T404">this.[NOM.SG]</ta>
            <ta e="T406" id="Seg_9402" s="T405">stove-LAT</ta>
            <ta e="T407" id="Seg_9403" s="T406">climb-PST.[3SG]</ta>
            <ta e="T408" id="Seg_9404" s="T407">and</ta>
            <ta e="T409" id="Seg_9405" s="T408">lie-DUR.[3SG]</ta>
            <ta e="T411" id="Seg_9406" s="T410">brother.PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T412" id="Seg_9407" s="T411">come-PST-3PL</ta>
            <ta e="T413" id="Seg_9408" s="T412">father-LAT/LOC.3SG</ta>
            <ta e="T414" id="Seg_9409" s="T413">tell-PRS-3PL</ta>
            <ta e="T415" id="Seg_9410" s="T414">what.kind</ta>
            <ta e="T416" id="Seg_9411" s="T415">man.[NOM.SG]</ta>
            <ta e="T417" id="Seg_9412" s="T416">be-PST.[3SG]</ta>
            <ta e="T418" id="Seg_9413" s="T417">horse-NOM/GEN.3SG</ta>
            <ta e="T419" id="Seg_9414" s="T418">very</ta>
            <ta e="T420" id="Seg_9415" s="T419">beautiful.[NOM.SG]</ta>
            <ta e="T421" id="Seg_9416" s="T420">and</ta>
            <ta e="T423" id="Seg_9417" s="T422">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T424" id="Seg_9418" s="T423">beautiful.[NOM.SG]</ta>
            <ta e="T425" id="Seg_9419" s="T424">and</ta>
            <ta e="T426" id="Seg_9420" s="T425">this.[NOM.SG]</ta>
            <ta e="T427" id="Seg_9421" s="T426">lie-DUR.[3SG]</ta>
            <ta e="T428" id="Seg_9422" s="T427">NEG</ta>
            <ta e="T429" id="Seg_9423" s="T428">I.NOM</ta>
            <ta e="T430" id="Seg_9424" s="T429">whether</ta>
            <ta e="T431" id="Seg_9425" s="T430">head-NOM/GEN/ACC.1SG</ta>
            <ta e="T432" id="Seg_9426" s="T431">there</ta>
            <ta e="T433" id="Seg_9427" s="T432">be-PST.[3SG]</ta>
            <ta e="T434" id="Seg_9428" s="T433">NEG.AUX-IMP.2SG</ta>
            <ta e="T435" id="Seg_9429" s="T434">lie-CNG</ta>
            <ta e="T436" id="Seg_9430" s="T435">lie-IMP.2SG</ta>
            <ta e="T437" id="Seg_9431" s="T436">PTCL</ta>
            <ta e="T438" id="Seg_9432" s="T437">mind-CAR.ADJ</ta>
            <ta e="T439" id="Seg_9433" s="T438">boy.[NOM.SG]</ta>
            <ta e="T440" id="Seg_9434" s="T439">you.GEN</ta>
            <ta e="T441" id="Seg_9435" s="T440">there</ta>
            <ta e="T442" id="Seg_9436" s="T441">be-PST-2SG</ta>
            <ta e="T443" id="Seg_9437" s="T442">then</ta>
            <ta e="T444" id="Seg_9438" s="T443">again</ta>
            <ta e="T445" id="Seg_9439" s="T444">three.[NOM.SG]</ta>
            <ta e="T446" id="Seg_9440" s="T445">day.[NOM.SG]</ta>
            <ta e="T447" id="Seg_9441" s="T446">again</ta>
            <ta e="T448" id="Seg_9442" s="T447">go-IPFVZ-PRS-3PL</ta>
            <ta e="T449" id="Seg_9443" s="T448">go-CVB</ta>
            <ta e="T450" id="Seg_9444" s="T449">disappear-PST-3PL</ta>
            <ta e="T451" id="Seg_9445" s="T450">this.[NOM.SG]</ta>
            <ta e="T453" id="Seg_9446" s="T452">get.up-PST.[3SG]</ta>
            <ta e="T454" id="Seg_9447" s="T453">basket-ACC.3SG</ta>
            <ta e="T455" id="Seg_9448" s="T454">take-PST.[3SG]</ta>
            <ta e="T456" id="Seg_9449" s="T455">go-PST.[3SG]</ta>
            <ta e="T457" id="Seg_9450" s="T456">mushroom-NOM/GEN/ACC.3PL</ta>
            <ta e="T458" id="Seg_9451" s="T457">collect-INF.LAT</ta>
            <ta e="T459" id="Seg_9452" s="T458">go-PST.[3SG]</ta>
            <ta e="T460" id="Seg_9453" s="T459">INCH</ta>
            <ta e="T461" id="Seg_9454" s="T460">horse.[NOM.SG]</ta>
            <ta e="T462" id="Seg_9455" s="T461">shout-INF.LAT</ta>
            <ta e="T463" id="Seg_9456" s="T462">horse.[NOM.SG]</ta>
            <ta e="T464" id="Seg_9457" s="T463">come-PST.[3SG]</ta>
            <ta e="T465" id="Seg_9458" s="T464">this.[NOM.SG]</ta>
            <ta e="T466" id="Seg_9459" s="T465">again</ta>
            <ta e="T467" id="Seg_9460" s="T466">along</ta>
            <ta e="T469" id="Seg_9461" s="T468">single.[NOM.SG]</ta>
            <ta e="T470" id="Seg_9462" s="T469">ear-LAT/LOC.3SG</ta>
            <ta e="T471" id="Seg_9463" s="T470">cook-RES-PST.[3SG]</ta>
            <ta e="T472" id="Seg_9464" s="T471">one.[NOM.SG]</ta>
            <ta e="T473" id="Seg_9465" s="T472">depart-RES-PST.[3SG]</ta>
            <ta e="T474" id="Seg_9466" s="T473">sit-PST.[3SG]</ta>
            <ta e="T475" id="Seg_9467" s="T474">go-PST.[3SG]</ta>
            <ta e="T476" id="Seg_9468" s="T475">then</ta>
            <ta e="T477" id="Seg_9469" s="T476">like</ta>
            <ta e="T478" id="Seg_9470" s="T477">run-MOM-PST.[3SG]</ta>
            <ta e="T479" id="Seg_9471" s="T478">this-ACC</ta>
            <ta e="T480" id="Seg_9472" s="T479">kiss-PST.[3SG]</ta>
            <ta e="T481" id="Seg_9473" s="T480">and</ta>
            <ta e="T482" id="Seg_9474" s="T481">ring-ACC.3SG</ta>
            <ta e="T483" id="Seg_9475" s="T482">take-MOM-PST.[3SG]</ta>
            <ta e="T484" id="Seg_9476" s="T483">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T485" id="Seg_9477" s="T484">hand-LAT/LOC.3SG</ta>
            <ta e="T487" id="Seg_9478" s="T486">dress-PST.[3SG]</ta>
            <ta e="T488" id="Seg_9479" s="T487">go-CVB</ta>
            <ta e="T489" id="Seg_9480" s="T488">disappear-PST.[3SG]</ta>
            <ta e="T490" id="Seg_9481" s="T489">horse-ACC.3SG</ta>
            <ta e="T491" id="Seg_9482" s="T490">send-PST.[3SG]</ta>
            <ta e="T492" id="Seg_9483" s="T491">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T493" id="Seg_9484" s="T492">come-PST.[3SG]</ta>
            <ta e="T494" id="Seg_9485" s="T493">house-LAT/LOC.3SG</ta>
            <ta e="T495" id="Seg_9486" s="T494">and</ta>
            <ta e="T496" id="Seg_9487" s="T495">stove-LAT</ta>
            <ta e="T497" id="Seg_9488" s="T496">climb-PST.[3SG]</ta>
            <ta e="T498" id="Seg_9489" s="T497">lie-DUR.[3SG]</ta>
            <ta e="T499" id="Seg_9490" s="T498">then</ta>
            <ta e="T500" id="Seg_9491" s="T499">brother-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T501" id="Seg_9492" s="T500">come-PST-3PL</ta>
            <ta e="T502" id="Seg_9493" s="T501">well</ta>
            <ta e="T503" id="Seg_9494" s="T502">what.kind=INDEF</ta>
            <ta e="T504" id="Seg_9495" s="T503">boy.[NOM.SG]</ta>
            <ta e="T505" id="Seg_9496" s="T504">boy.[NOM.SG]</ta>
            <ta e="T506" id="Seg_9497" s="T505">beautiful.[NOM.SG]</ta>
            <ta e="T507" id="Seg_9498" s="T506">horse-NOM/GEN.3SG</ta>
            <ta e="T508" id="Seg_9499" s="T507">beautiful.[NOM.SG]</ta>
            <ta e="T509" id="Seg_9500" s="T508">kiss-PST.[3SG]</ta>
            <ta e="T510" id="Seg_9501" s="T509">tsar.ADJ</ta>
            <ta e="T511" id="Seg_9502" s="T510">girl.[NOM.SG]</ta>
            <ta e="T512" id="Seg_9503" s="T511">chief.[NOM.SG]</ta>
            <ta e="T513" id="Seg_9504" s="T512">chief.[NOM.SG]</ta>
            <ta e="T514" id="Seg_9505" s="T513">girl.[NOM.SG]</ta>
            <ta e="T515" id="Seg_9506" s="T514">and</ta>
            <ta e="T516" id="Seg_9507" s="T515">ring-ACC.3SG</ta>
            <ta e="T517" id="Seg_9508" s="T516">take-PST.[3SG]</ta>
            <ta e="T518" id="Seg_9509" s="T517">go-CVB</ta>
            <ta e="T519" id="Seg_9510" s="T518">disappear-PST.[3SG]</ta>
            <ta e="T520" id="Seg_9511" s="T519">then</ta>
            <ta e="T521" id="Seg_9512" s="T520">again</ta>
            <ta e="T522" id="Seg_9513" s="T521">this.[NOM.SG]</ta>
            <ta e="T523" id="Seg_9514" s="T522">chief.[NOM.SG]</ta>
            <ta e="T524" id="Seg_9515" s="T523">make-PST.[3SG]</ta>
            <ta e="T525" id="Seg_9516" s="T524">many</ta>
            <ta e="T526" id="Seg_9517" s="T525">meat.[NOM.SG]</ta>
            <ta e="T527" id="Seg_9518" s="T526">egg-PL</ta>
            <ta e="T528" id="Seg_9519" s="T527">put-PST.[3SG]</ta>
            <ta e="T529" id="Seg_9520" s="T528">all</ta>
            <ta e="T530" id="Seg_9521" s="T529">what.[NOM.SG]</ta>
            <ta e="T531" id="Seg_9522" s="T530">there</ta>
            <ta e="T532" id="Seg_9523" s="T531">be-PRS.[3SG]</ta>
            <ta e="T533" id="Seg_9524" s="T532">put-PST.[3SG]</ta>
            <ta e="T534" id="Seg_9525" s="T533">then</ta>
            <ta e="T535" id="Seg_9526" s="T534">come-IMP.2PL</ta>
            <ta e="T536" id="Seg_9527" s="T535">all-ABL</ta>
            <ta e="T537" id="Seg_9528" s="T536">who.[NOM.SG]</ta>
            <ta e="T538" id="Seg_9529" s="T537">NEG</ta>
            <ta e="T539" id="Seg_9530" s="T538">come-FUT-3SG</ta>
            <ta e="T540" id="Seg_9531" s="T539">head-ACC.3SG</ta>
            <ta e="T542" id="Seg_9532" s="T541">off-cut-FUT-1SG</ta>
            <ta e="T543" id="Seg_9533" s="T542">then</ta>
            <ta e="T544" id="Seg_9534" s="T543">this-PL</ta>
            <ta e="T546" id="Seg_9535" s="T545">there</ta>
            <ta e="T547" id="Seg_9536" s="T546">father-NOM/GEN.3SG</ta>
            <ta e="T548" id="Seg_9537" s="T547">all-ABL</ta>
            <ta e="T549" id="Seg_9538" s="T548">go-CVB</ta>
            <ta e="T550" id="Seg_9539" s="T549">disappear-PST-3PL</ta>
            <ta e="T551" id="Seg_9540" s="T550">come-PST-3PL</ta>
            <ta e="T552" id="Seg_9541" s="T551">this-PL</ta>
            <ta e="T553" id="Seg_9542" s="T552">sit.down-PST-3PL</ta>
            <ta e="T555" id="Seg_9543" s="T554">PTCL</ta>
            <ta e="T556" id="Seg_9544" s="T555">drink-DUR-3PL</ta>
            <ta e="T558" id="Seg_9545" s="T557">vodka.[NOM.SG]</ta>
            <ta e="T559" id="Seg_9546" s="T558">give-DUR-3PL</ta>
            <ta e="T560" id="Seg_9547" s="T559">then</ta>
            <ta e="T561" id="Seg_9548" s="T560">come-PST.[3SG]</ta>
            <ta e="T562" id="Seg_9549" s="T561">this.[NOM.SG]</ta>
            <ta e="T563" id="Seg_9550" s="T562">last</ta>
            <ta e="T564" id="Seg_9551" s="T563">son-NOM/GEN.3SG</ta>
            <ta e="T565" id="Seg_9552" s="T564">this.[NOM.SG]</ta>
            <ta e="T566" id="Seg_9553" s="T565">this.[NOM.SG]</ta>
            <ta e="T567" id="Seg_9554" s="T566">PTCL</ta>
            <ta e="T568" id="Seg_9555" s="T567">black.[NOM.SG]</ta>
            <ta e="T569" id="Seg_9556" s="T568">sit-DUR.[3SG]</ta>
            <ta e="T570" id="Seg_9557" s="T569">and</ta>
            <ta e="T571" id="Seg_9558" s="T570">hand-NOM/GEN.3SG</ta>
            <ta e="T572" id="Seg_9559" s="T571">black.[NOM.SG]</ta>
            <ta e="T573" id="Seg_9560" s="T572">rag-INS</ta>
            <ta e="T574" id="Seg_9561" s="T573">bind-DETR-PTCP.[NOM.SG]</ta>
            <ta e="T575" id="Seg_9562" s="T574">say-IPFVZ.[3SG]</ta>
            <ta e="T576" id="Seg_9563" s="T575">what.[NOM.SG]</ta>
            <ta e="T577" id="Seg_9564" s="T576">you.GEN</ta>
            <ta e="T578" id="Seg_9565" s="T577">hand-LAT/LOC.3SG</ta>
            <ta e="T580" id="Seg_9566" s="T579">see-PRS-3SG.O</ta>
            <ta e="T581" id="Seg_9567" s="T580">PTCL</ta>
            <ta e="T582" id="Seg_9568" s="T581">PTCL</ta>
            <ta e="T583" id="Seg_9569" s="T582">there</ta>
            <ta e="T584" id="Seg_9570" s="T583">ring.[NOM.SG]</ta>
            <ta e="T585" id="Seg_9571" s="T584">sit-DUR.[3SG]</ta>
            <ta e="T586" id="Seg_9572" s="T585">then</ta>
            <ta e="T587" id="Seg_9573" s="T586">take-FUT-3SG.O</ta>
            <ta e="T588" id="Seg_9574" s="T587">father-LAT/LOC.3SG</ta>
            <ta e="T589" id="Seg_9575" s="T588">come-PRS-3SG.O </ta>
            <ta e="T590" id="Seg_9576" s="T589">that.[NOM.SG]</ta>
            <ta e="T592" id="Seg_9577" s="T591">that.[NOM.SG]</ta>
            <ta e="T593" id="Seg_9578" s="T592">I.GEN</ta>
            <ta e="T594" id="Seg_9579" s="T593">groom-NOM/GEN/ACC.1SG</ta>
            <ta e="T595" id="Seg_9580" s="T594">this-ACC</ta>
            <ta e="T596" id="Seg_9581" s="T595">this.[NOM.SG]</ta>
            <ta e="T598" id="Seg_9582" s="T597">this-PL</ta>
            <ta e="T599" id="Seg_9583" s="T598">there</ta>
            <ta e="T600" id="Seg_9584" s="T599">wash-PST-3PL</ta>
            <ta e="T601" id="Seg_9585" s="T600">beautiful.[NOM.SG]</ta>
            <ta e="T602" id="Seg_9586" s="T601">clothing.[NOM.SG]</ta>
            <ta e="T604" id="Seg_9587" s="T603">dress-PST-3PL</ta>
            <ta e="T605" id="Seg_9588" s="T604">very</ta>
            <ta e="T606" id="Seg_9589" s="T605">beautiful.[NOM.SG]</ta>
            <ta e="T607" id="Seg_9590" s="T606">become-RES-PST.[3SG]</ta>
            <ta e="T608" id="Seg_9591" s="T607">then</ta>
            <ta e="T609" id="Seg_9592" s="T608">again</ta>
            <ta e="T610" id="Seg_9593" s="T609">INCH</ta>
            <ta e="T611" id="Seg_9594" s="T610">vodka.[NOM.SG]</ta>
            <ta e="T612" id="Seg_9595" s="T611">drink-INF.LAT</ta>
            <ta e="T613" id="Seg_9596" s="T612">play-INF.LAT</ta>
            <ta e="T614" id="Seg_9597" s="T613">brother-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T615" id="Seg_9598" s="T614">PTCL</ta>
            <ta e="T616" id="Seg_9599" s="T615">look-DUR-3PL</ta>
            <ta e="T617" id="Seg_9600" s="T616">what.kind</ta>
            <ta e="T618" id="Seg_9601" s="T617">beautiful.[NOM.SG]</ta>
            <ta e="T619" id="Seg_9602" s="T618">become-RES-PST.[3SG]</ta>
            <ta e="T620" id="Seg_9603" s="T619">and</ta>
            <ta e="T621" id="Seg_9604" s="T620">there</ta>
            <ta e="T622" id="Seg_9605" s="T621">I.NOM</ta>
            <ta e="T623" id="Seg_9606" s="T622">be-PST-1SG</ta>
            <ta e="T624" id="Seg_9607" s="T623">vodka.[NOM.SG]</ta>
            <ta e="T625" id="Seg_9608" s="T624">PTCL</ta>
            <ta e="T626" id="Seg_9609" s="T625">flow-DUR.[3SG]</ta>
            <ta e="T627" id="Seg_9610" s="T626">give-PST-3PL</ta>
            <ta e="T628" id="Seg_9611" s="T627">I.LAT</ta>
            <ta e="T629" id="Seg_9612" s="T628">pancake.[NOM.SG]</ta>
            <ta e="T634" id="Seg_9613" s="T633">sit-PST-3PL</ta>
            <ta e="T635" id="Seg_9614" s="T634">two-COLL</ta>
            <ta e="T636" id="Seg_9615" s="T635">godfather.[NOM.SG]</ta>
            <ta e="T637" id="Seg_9616" s="T636">and</ta>
            <ta e="T638" id="Seg_9617" s="T637">godmother.[NOM.SG]</ta>
            <ta e="T639" id="Seg_9618" s="T638">wolf.[NOM.SG]</ta>
            <ta e="T640" id="Seg_9619" s="T639">and</ta>
            <ta e="T641" id="Seg_9620" s="T640">fox.[NOM.SG]</ta>
            <ta e="T642" id="Seg_9621" s="T641">then</ta>
            <ta e="T643" id="Seg_9622" s="T642">sleep-INF.LAT</ta>
            <ta e="T644" id="Seg_9623" s="T643">lie.[PRS]-3PL</ta>
            <ta e="T645" id="Seg_9624" s="T644">and</ta>
            <ta e="T646" id="Seg_9625" s="T645">fox.[NOM.SG]</ta>
            <ta e="T647" id="Seg_9626" s="T646">always</ta>
            <ta e="T648" id="Seg_9627" s="T647">rumble-DUR.[3SG]</ta>
            <ta e="T649" id="Seg_9628" s="T648">rumble-DUR.[3SG]</ta>
            <ta e="T650" id="Seg_9629" s="T649">and</ta>
            <ta e="T651" id="Seg_9630" s="T650">wolf.[NOM.SG]</ta>
            <ta e="T652" id="Seg_9631" s="T651">say-IPFVZ.[3SG]</ta>
            <ta e="T653" id="Seg_9632" s="T652">who.[NOM.SG]=INDEF</ta>
            <ta e="T654" id="Seg_9633" s="T653">rumble-DUR.[3SG]</ta>
            <ta e="T655" id="Seg_9634" s="T654">I.ACC</ta>
            <ta e="T656" id="Seg_9635" s="T655">call-PRS-3PL</ta>
            <ta e="T657" id="Seg_9636" s="T656">child.[NOM.SG]</ta>
            <ta e="T658" id="Seg_9637" s="T657">child.[NOM.SG]</ta>
            <ta e="T659" id="Seg_9638" s="T658">feed-INF.LAT</ta>
            <ta e="T660" id="Seg_9639" s="T659">then</ta>
            <ta e="T661" id="Seg_9640" s="T660">well</ta>
            <ta e="T662" id="Seg_9641" s="T661">go-EP-IMP.2SG</ta>
            <ta e="T663" id="Seg_9642" s="T662">this.[NOM.SG]</ta>
            <ta e="T664" id="Seg_9643" s="T663">go-PST.[3SG]</ta>
            <ta e="T665" id="Seg_9644" s="T664">and</ta>
            <ta e="T666" id="Seg_9645" s="T665">this-PL</ta>
            <ta e="T667" id="Seg_9646" s="T666">stand-PST.[3SG]</ta>
            <ta e="T668" id="Seg_9647" s="T667">honey.[NOM.SG]</ta>
            <ta e="T669" id="Seg_9648" s="T668">eat-PST.[3SG]</ta>
            <ta e="T670" id="Seg_9649" s="T669">eat-PST.[3SG]</ta>
            <ta e="T671" id="Seg_9650" s="T670">honey.[NOM.SG]</ta>
            <ta e="T672" id="Seg_9651" s="T671">this.[NOM.SG]</ta>
            <ta e="T673" id="Seg_9652" s="T672">come-PST.[3SG]</ta>
            <ta e="T674" id="Seg_9653" s="T673">who.[NOM.SG]</ta>
            <ta e="T675" id="Seg_9654" s="T674">there</ta>
            <ta e="T676" id="Seg_9655" s="T675">be-PRS.[3SG]</ta>
            <ta e="T677" id="Seg_9656" s="T676">upper.part.[NOM.SG]</ta>
            <ta e="T678" id="Seg_9657" s="T677">be-PRS.[3SG]</ta>
            <ta e="T679" id="Seg_9658" s="T678">then</ta>
            <ta e="T680" id="Seg_9659" s="T679">again</ta>
            <ta e="T681" id="Seg_9660" s="T680">evening-LOC.ADV</ta>
            <ta e="T682" id="Seg_9661" s="T681">again</ta>
            <ta e="T683" id="Seg_9662" s="T682">rumble-DUR.[3SG]</ta>
            <ta e="T684" id="Seg_9663" s="T683">fox.[NOM.SG]</ta>
            <ta e="T685" id="Seg_9664" s="T684">who.[NOM.SG]</ta>
            <ta e="T686" id="Seg_9665" s="T685">rumble-DUR.[3SG]</ta>
            <ta e="T687" id="Seg_9666" s="T686">and</ta>
            <ta e="T688" id="Seg_9667" s="T687">I.ACC</ta>
            <ta e="T689" id="Seg_9668" s="T688">call-DUR-3PL</ta>
            <ta e="T690" id="Seg_9669" s="T689">child.[NOM.SG]</ta>
            <ta e="T691" id="Seg_9670" s="T690">untie-INF.LAT</ta>
            <ta e="T692" id="Seg_9671" s="T691">well</ta>
            <ta e="T693" id="Seg_9672" s="T692">go-EP-IMP.2SG</ta>
            <ta e="T694" id="Seg_9673" s="T693">this.[NOM.SG]</ta>
            <ta e="T695" id="Seg_9674" s="T694">go-PST.[3SG]</ta>
            <ta e="T696" id="Seg_9675" s="T695">eat-PST.[3SG]</ta>
            <ta e="T697" id="Seg_9676" s="T696">eat-PST.[3SG]</ta>
            <ta e="T698" id="Seg_9677" s="T697">then</ta>
            <ta e="T699" id="Seg_9678" s="T698">come-PST.[3SG]</ta>
            <ta e="T700" id="Seg_9679" s="T699">who.[NOM.SG]</ta>
            <ta e="T701" id="Seg_9680" s="T700">there</ta>
            <ta e="T702" id="Seg_9681" s="T701">come-PST.[3SG]</ta>
            <ta e="T703" id="Seg_9682" s="T702">middle.part.[NOM.SG]</ta>
            <ta e="T704" id="Seg_9683" s="T703">come-PST.[3SG]</ta>
            <ta e="T705" id="Seg_9684" s="T704">then</ta>
            <ta e="T706" id="Seg_9685" s="T705">three-ORD.[NOM.SG]</ta>
            <ta e="T707" id="Seg_9686" s="T706">evening-LOC.ADV</ta>
            <ta e="T708" id="Seg_9687" s="T707">again</ta>
            <ta e="T709" id="Seg_9688" s="T708">rumble-DUR.[3SG]</ta>
            <ta e="T710" id="Seg_9689" s="T709">then</ta>
            <ta e="T711" id="Seg_9690" s="T710">who.[NOM.SG]=INDEF</ta>
            <ta e="T712" id="Seg_9691" s="T711">rumble-DUR.[3SG]</ta>
            <ta e="T713" id="Seg_9692" s="T712">and</ta>
            <ta e="T714" id="Seg_9693" s="T713">I.ACC</ta>
            <ta e="T715" id="Seg_9694" s="T714">call-DUR-3PL</ta>
            <ta e="T716" id="Seg_9695" s="T715">well</ta>
            <ta e="T717" id="Seg_9696" s="T716">go-EP-IMP.2SG</ta>
            <ta e="T718" id="Seg_9697" s="T717">then</ta>
            <ta e="T719" id="Seg_9698" s="T718">this.[NOM.SG]</ta>
            <ta e="T720" id="Seg_9699" s="T719">eat-PST.[3SG]</ta>
            <ta e="T721" id="Seg_9700" s="T720">PTCL</ta>
            <ta e="T722" id="Seg_9701" s="T721">come-PST.[3SG]</ta>
            <ta e="T723" id="Seg_9702" s="T722">who.[NOM.SG]</ta>
            <ta e="T724" id="Seg_9703" s="T723">there</ta>
            <ta e="T725" id="Seg_9704" s="T724">this.[NOM.SG]</ta>
            <ta e="T726" id="Seg_9705" s="T725">last.part.[NOM.SG]</ta>
            <ta e="T727" id="Seg_9706" s="T726">come-PST.[3SG]</ta>
            <ta e="T728" id="Seg_9707" s="T727">then</ta>
            <ta e="T729" id="Seg_9708" s="T728">morning-LOC.ADV</ta>
            <ta e="T730" id="Seg_9709" s="T729">get.up-PST-3PL</ta>
            <ta e="T731" id="Seg_9710" s="T730">this.[NOM.SG]</ta>
            <ta e="T732" id="Seg_9711" s="T731">hurt-MOM-PST.[3SG]</ta>
            <ta e="T733" id="Seg_9712" s="T732">say-IPFVZ.[3SG]</ta>
            <ta e="T734" id="Seg_9713" s="T733">bring-IMP.2SG</ta>
            <ta e="T735" id="Seg_9714" s="T734">I.LAT</ta>
            <ta e="T736" id="Seg_9715" s="T735">sweet-PL</ta>
            <ta e="T737" id="Seg_9716" s="T736">this.[NOM.SG]</ta>
            <ta e="T738" id="Seg_9717" s="T737">go-PST.[3SG]</ta>
            <ta e="T739" id="Seg_9718" s="T738">and</ta>
            <ta e="T740" id="Seg_9719" s="T739">NEG.EX.[3SG]</ta>
            <ta e="T741" id="Seg_9720" s="T740">who.[NOM.SG]=INDEF</ta>
            <ta e="T742" id="Seg_9721" s="T741">eat-MOM-PST.[3SG]</ta>
            <ta e="T743" id="Seg_9722" s="T742">and</ta>
            <ta e="T744" id="Seg_9723" s="T743">you.NOM</ta>
            <ta e="T745" id="Seg_9724" s="T744">eat-PST-2SG</ta>
            <ta e="T746" id="Seg_9725" s="T745">this.[NOM.SG]</ta>
            <ta e="T747" id="Seg_9726" s="T746">PTCL</ta>
            <ta e="T748" id="Seg_9727" s="T747">NEG</ta>
            <ta e="T749" id="Seg_9728" s="T748">I.NOM</ta>
            <ta e="T750" id="Seg_9729" s="T749">NEG</ta>
            <ta e="T751" id="Seg_9730" s="T750">eat-PST-1SG</ta>
            <ta e="T752" id="Seg_9731" s="T751">then</ta>
            <ta e="T753" id="Seg_9732" s="T752">HORT</ta>
            <ta e="T754" id="Seg_9733" s="T753">sun-LAT</ta>
            <ta e="T755" id="Seg_9734" s="T754">lie-1PL</ta>
            <ta e="T756" id="Seg_9735" s="T755">who.[NOM.SG]</ta>
            <ta e="T757" id="Seg_9736" s="T756">eat-PST.[3SG]</ta>
            <ta e="T758" id="Seg_9737" s="T757">there</ta>
            <ta e="T759" id="Seg_9738" s="T758">sweet.[NOM.SG]</ta>
            <ta e="T760" id="Seg_9739" s="T759">belly-NOM/GEN.3SG</ta>
            <ta e="T761" id="Seg_9740" s="T760">become-FUT-3SG</ta>
            <ta e="T762" id="Seg_9741" s="T761">then</ta>
            <ta e="T763" id="Seg_9742" s="T762">see-PRS-3SG.O</ta>
            <ta e="T764" id="Seg_9743" s="T763">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T765" id="Seg_9744" s="T764">belly-LAT/LOC.3SG</ta>
            <ta e="T766" id="Seg_9745" s="T765">sweet.[NOM.SG]</ta>
            <ta e="T767" id="Seg_9746" s="T766">be-PRS.[3SG]</ta>
            <ta e="T768" id="Seg_9747" s="T767">this.[NOM.SG]</ta>
            <ta e="T769" id="Seg_9748" s="T768">INCH</ta>
            <ta e="T770" id="Seg_9749" s="T769">this.[NOM.SG]</ta>
            <ta e="T771" id="Seg_9750" s="T770">wolf-NOM/GEN/ACC.3SG</ta>
            <ta e="T772" id="Seg_9751" s="T771">smear-INF.LAT</ta>
            <ta e="T773" id="Seg_9752" s="T772">get.up-IMP.2SG</ta>
            <ta e="T774" id="Seg_9753" s="T773">you.NOM</ta>
            <ta e="T776" id="Seg_9754" s="T775">eat-PST-2SG</ta>
            <ta e="T777" id="Seg_9755" s="T776">then</ta>
            <ta e="T778" id="Seg_9756" s="T777">this.[NOM.SG]</ta>
            <ta e="T779" id="Seg_9757" s="T778">get.up-PST.[3SG]</ta>
            <ta e="T780" id="Seg_9758" s="T779">well</ta>
            <ta e="T781" id="Seg_9759" s="T780">I.NOM</ta>
            <ta e="T782" id="Seg_9760" s="T781">eat-PST-1SG</ta>
            <ta e="T783" id="Seg_9761" s="T782">then</ta>
            <ta e="T784" id="Seg_9762" s="T783">all</ta>
            <ta e="T785" id="Seg_9763" s="T784">bird.[NOM.SG]</ta>
            <ta e="T786" id="Seg_9764" s="T785">and</ta>
            <ta e="T787" id="Seg_9765" s="T786">grouse.[NOM.SG]</ta>
            <ta e="T788" id="Seg_9766" s="T787">live-3PL</ta>
            <ta e="T789" id="Seg_9767" s="T788">how</ta>
            <ta e="T790" id="Seg_9768" s="T789">house.[NOM.SG]</ta>
            <ta e="T791" id="Seg_9769" s="T790">cut-INF.LAT</ta>
            <ta e="T792" id="Seg_9770" s="T791">axe.[NOM.SG]</ta>
            <ta e="T793" id="Seg_9771" s="T792">NEG.EX.[3SG]</ta>
            <ta e="T794" id="Seg_9772" s="T793">who.[NOM.SG]</ta>
            <ta e="T795" id="Seg_9773" s="T794">axe.[NOM.SG]</ta>
            <ta e="T796" id="Seg_9774" s="T795">make-FUT-3SG</ta>
            <ta e="T797" id="Seg_9775" s="T796">and</ta>
            <ta e="T798" id="Seg_9776" s="T797">what.for</ta>
            <ta e="T799" id="Seg_9777" s="T798">I.LAT</ta>
            <ta e="T801" id="Seg_9778" s="T800">house.[NOM.SG]</ta>
            <ta e="T802" id="Seg_9779" s="T801">I.NOM</ta>
            <ta e="T803" id="Seg_9780" s="T802">snow-LOC</ta>
            <ta e="T804" id="Seg_9781" s="T803">spend.night-FUT-1SG</ta>
            <ta e="T805" id="Seg_9782" s="T804">and</ta>
            <ta e="T806" id="Seg_9783" s="T805">fall-MOM-PST.[3SG]</ta>
            <ta e="T808" id="Seg_9784" s="T807">snow-LAT</ta>
            <ta e="T809" id="Seg_9785" s="T808">there</ta>
            <ta e="T810" id="Seg_9786" s="T809">spend.night-PST.[3SG]</ta>
            <ta e="T811" id="Seg_9787" s="T810">morning-LOC.ADV</ta>
            <ta e="T812" id="Seg_9788" s="T811">get.up-PST.[3SG]</ta>
            <ta e="T813" id="Seg_9789" s="T812">and</ta>
            <ta e="T814" id="Seg_9790" s="T813">go-PST.[3SG]</ta>
            <ta e="T815" id="Seg_9791" s="T814">see-INF.LAT</ta>
            <ta e="T816" id="Seg_9792" s="T815">companion-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T817" id="Seg_9793" s="T816">see-PST.[3SG]</ta>
            <ta e="T818" id="Seg_9794" s="T817">there</ta>
            <ta e="T819" id="Seg_9795" s="T818">play-PST-3PL</ta>
            <ta e="T820" id="Seg_9796" s="T819">jump-MOM-PST-3PL</ta>
            <ta e="T822" id="Seg_9797" s="T821">tree-3PL-LAT</ta>
            <ta e="T823" id="Seg_9798" s="T822">then</ta>
            <ta e="T824" id="Seg_9799" s="T823">self-LAT/LOC.3SG</ta>
            <ta e="T825" id="Seg_9800" s="T824">nest-NOM/GEN/ACC.3PL</ta>
            <ta e="T826" id="Seg_9801" s="T825">make-PST-3PL</ta>
            <ta e="T827" id="Seg_9802" s="T826">egg-PL</ta>
            <ta e="T828" id="Seg_9803" s="T827">bring-PST-3PL</ta>
            <ta e="T829" id="Seg_9804" s="T828">child-PL-LAT</ta>
            <ta e="T830" id="Seg_9805" s="T829">become-PST-3PL</ta>
            <ta e="T831" id="Seg_9806" s="T830">then</ta>
            <ta e="T832" id="Seg_9807" s="T831">child-PL-LAT</ta>
            <ta e="T833" id="Seg_9808" s="T832">feed-PST-3PL</ta>
            <ta e="T834" id="Seg_9809" s="T833">gnat-INS</ta>
            <ta e="T835" id="Seg_9810" s="T834">worm-3PL-INS</ta>
            <ta e="T836" id="Seg_9811" s="T835">then</ta>
            <ta e="T837" id="Seg_9812" s="T836">again</ta>
            <ta e="T838" id="Seg_9813" s="T837">what.for</ta>
            <ta e="T839" id="Seg_9814" s="T838">we.LAT</ta>
            <ta e="T840" id="Seg_9815" s="T839">house.[NOM.SG]</ta>
            <ta e="T841" id="Seg_9816" s="T840">snow-LOC</ta>
            <ta e="T842" id="Seg_9817" s="T841">sleep-PST-1PL</ta>
            <ta e="T843" id="Seg_9818" s="T842">one.[NOM.SG]</ta>
            <ta e="T844" id="Seg_9819" s="T843">evening</ta>
            <ta e="T845" id="Seg_9820" s="T844">house-LOC</ta>
            <ta e="T846" id="Seg_9821" s="T845">seat-1PL</ta>
            <ta e="T847" id="Seg_9822" s="T846">this.[NOM.SG]</ta>
            <ta e="T848" id="Seg_9823" s="T847">all</ta>
            <ta e="T849" id="Seg_9824" s="T848">everywhere</ta>
            <ta e="T850" id="Seg_9825" s="T849">look-FRQ-DUR-1PL</ta>
            <ta e="T851" id="Seg_9826" s="T850">enough</ta>
            <ta e="T852" id="Seg_9827" s="T851">live-PST-3PL</ta>
            <ta e="T853" id="Seg_9828" s="T852">woman</ta>
            <ta e="T854" id="Seg_9829" s="T853">and</ta>
            <ta e="T855" id="Seg_9830" s="T854">man.[NOM.SG]</ta>
            <ta e="T857" id="Seg_9831" s="T856">this-PL</ta>
            <ta e="T858" id="Seg_9832" s="T857">what.[NOM.SG]=INDEF</ta>
            <ta e="T859" id="Seg_9833" s="T858">NEG.EX-PST.[3SG]</ta>
            <ta e="T860" id="Seg_9834" s="T859">go-PST-3PL</ta>
            <ta e="T861" id="Seg_9835" s="T860">forest-LAT</ta>
            <ta e="T862" id="Seg_9836" s="T861">cone-PL</ta>
            <ta e="T863" id="Seg_9837" s="T862">acorns.[NOM.SG]</ta>
            <ta e="T864" id="Seg_9838" s="T863">gather-PST-3PL</ta>
            <ta e="T865" id="Seg_9839" s="T864">come-PST-3PL</ta>
            <ta e="T866" id="Seg_9840" s="T865">eat-DUR.[3SG]</ta>
            <ta e="T867" id="Seg_9841" s="T866">PTCL</ta>
            <ta e="T868" id="Seg_9842" s="T867">one.[NOM.SG]</ta>
            <ta e="T869" id="Seg_9843" s="T868">fall-MOM-PST.[3SG]</ta>
            <ta e="T870" id="Seg_9844" s="T869">cellar-LOC</ta>
            <ta e="T871" id="Seg_9845" s="T870">then</ta>
            <ta e="T872" id="Seg_9846" s="T871">INCH</ta>
            <ta e="T873" id="Seg_9847" s="T872">grow-INF.LAT</ta>
            <ta e="T875" id="Seg_9848" s="T874">woman.[NOM.SG]</ta>
            <ta e="T876" id="Seg_9849" s="T875">say-IPFVZ.[3SG]</ta>
            <ta e="T877" id="Seg_9850" s="T876">axe-INS</ta>
            <ta e="T878" id="Seg_9851" s="T877">cut-IMP.2SG.O</ta>
            <ta e="T879" id="Seg_9852" s="T878">floor-NOM/GEN/ACC.3SG</ta>
            <ta e="T880" id="Seg_9853" s="T879">this.[NOM.SG]</ta>
            <ta e="T881" id="Seg_9854" s="T880">cut-PST.[3SG]</ta>
            <ta e="T882" id="Seg_9855" s="T881">this.[NOM.SG]</ta>
            <ta e="T883" id="Seg_9856" s="T882">here</ta>
            <ta e="T884" id="Seg_9857" s="T883">grow-PST.[3SG]</ta>
            <ta e="T885" id="Seg_9858" s="T884">now</ta>
            <ta e="T886" id="Seg_9859" s="T885">there</ta>
            <ta e="T887" id="Seg_9860" s="T886">up</ta>
            <ta e="T888" id="Seg_9861" s="T887">PTCL</ta>
            <ta e="T889" id="Seg_9862" s="T888">gather</ta>
            <ta e="T890" id="Seg_9863" s="T889">this.[NOM.SG]</ta>
            <ta e="T891" id="Seg_9864" s="T890">there</ta>
            <ta e="T892" id="Seg_9865" s="T891">roof-ACC.3SG</ta>
            <ta e="T894" id="Seg_9866" s="T893">there</ta>
            <ta e="T895" id="Seg_9867" s="T894">far-LAT.ADV</ta>
            <ta e="T896" id="Seg_9868" s="T895">grow-PST.[3SG]</ta>
            <ta e="T897" id="Seg_9869" s="T896">even</ta>
            <ta e="T898" id="Seg_9870" s="T897">up</ta>
            <ta e="T899" id="Seg_9871" s="T898">then</ta>
            <ta e="T900" id="Seg_9872" s="T899">man.[NOM.SG]</ta>
            <ta e="T901" id="Seg_9873" s="T900">one.should</ta>
            <ta e="T902" id="Seg_9874" s="T901">go-INF.LAT</ta>
            <ta e="T903" id="Seg_9875" s="T902">collect-INF.LAT</ta>
            <ta e="T904" id="Seg_9876" s="T903">acorns.[NOM.SG]</ta>
            <ta e="T905" id="Seg_9877" s="T904">climb-PST.[3SG]</ta>
            <ta e="T906" id="Seg_9878" s="T905">climb-PST.[3SG]</ta>
            <ta e="T908" id="Seg_9879" s="T907">there</ta>
            <ta e="T909" id="Seg_9880" s="T908">up</ta>
            <ta e="T910" id="Seg_9881" s="T909">there</ta>
            <ta e="T911" id="Seg_9882" s="T910">see-DUR.[3SG]</ta>
            <ta e="T912" id="Seg_9883" s="T911">house.[NOM.SG]</ta>
            <ta e="T914" id="Seg_9884" s="T913">stand-PRS.[3SG]</ta>
            <ta e="T915" id="Seg_9885" s="T914">there</ta>
            <ta e="T916" id="Seg_9886" s="T915">house-LAT</ta>
            <ta e="T917" id="Seg_9887" s="T916">come-PST.[3SG]</ta>
            <ta e="T918" id="Seg_9888" s="T917">there</ta>
            <ta e="T919" id="Seg_9889" s="T918">hen-GEN</ta>
            <ta e="T920" id="Seg_9890" s="T919">man.[NOM.SG]</ta>
            <ta e="T921" id="Seg_9891" s="T920">live-DUR.[3SG]</ta>
            <ta e="T922" id="Seg_9892" s="T921">head-NOM/GEN.3SG</ta>
            <ta e="T923" id="Seg_9893" s="T922">beautiful.[NOM.SG]</ta>
            <ta e="T924" id="Seg_9894" s="T923">red.[NOM.SG]</ta>
            <ta e="T925" id="Seg_9895" s="T924">all</ta>
            <ta e="T926" id="Seg_9896" s="T925">and</ta>
            <ta e="T927" id="Seg_9897" s="T926">there</ta>
            <ta e="T928" id="Seg_9898" s="T927">millstone-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T929" id="Seg_9899" s="T928">be-PRS.[3SG]</ta>
            <ta e="T930" id="Seg_9900" s="T929">this.[NOM.SG]</ta>
            <ta e="T931" id="Seg_9901" s="T930">take-PST.[3SG]</ta>
            <ta e="T932" id="Seg_9902" s="T931">this-ACC</ta>
            <ta e="T933" id="Seg_9903" s="T932">and</ta>
            <ta e="T934" id="Seg_9904" s="T933">come-PST.[3SG]</ta>
            <ta e="T935" id="Seg_9905" s="T934">take.it</ta>
            <ta e="T936" id="Seg_9906" s="T935">you.DAT</ta>
            <ta e="T937" id="Seg_9907" s="T936">hen-GEN</ta>
            <ta e="T938" id="Seg_9908" s="T937">man.[NOM.SG]</ta>
            <ta e="T939" id="Seg_9909" s="T938">red.[NOM.SG]</ta>
            <ta e="T940" id="Seg_9910" s="T939">head-NOM/GEN.3SG</ta>
            <ta e="T941" id="Seg_9911" s="T940">this.[NOM.SG]</ta>
            <ta e="T942" id="Seg_9912" s="T941">take-PST.[3SG]</ta>
            <ta e="T943" id="Seg_9913" s="T942">and</ta>
            <ta e="T944" id="Seg_9914" s="T943">INCH</ta>
            <ta e="T945" id="Seg_9915" s="T944">millstone</ta>
            <ta e="T946" id="Seg_9916" s="T945">this.[NOM.SG]</ta>
            <ta e="T947" id="Seg_9917" s="T946">mill-NOM/GEN/ACC.3SG</ta>
            <ta e="T948" id="Seg_9918" s="T947">knit-INF.LAT</ta>
            <ta e="T949" id="Seg_9919" s="T948">there</ta>
            <ta e="T950" id="Seg_9920" s="T949">pancake-PL</ta>
            <ta e="T951" id="Seg_9921" s="T950">pie-PL</ta>
            <ta e="T952" id="Seg_9922" s="T951">pancake-PL</ta>
            <ta e="T953" id="Seg_9923" s="T952">pie-PL</ta>
            <ta e="T954" id="Seg_9924" s="T953">man.[NOM.SG]</ta>
            <ta e="T955" id="Seg_9925" s="T954">eat-PST.[3SG]</ta>
            <ta e="T956" id="Seg_9926" s="T955">and</ta>
            <ta e="T957" id="Seg_9927" s="T956">woman.[NOM.SG]</ta>
            <ta e="T958" id="Seg_9928" s="T957">eat-PST.[3SG]</ta>
            <ta e="T959" id="Seg_9929" s="T958">always</ta>
            <ta e="T960" id="Seg_9930" s="T959">along</ta>
            <ta e="T961" id="Seg_9931" s="T960">live-CVB</ta>
            <ta e="T962" id="Seg_9932" s="T961">then</ta>
            <ta e="T963" id="Seg_9933" s="T962">come-PST.[3SG]</ta>
            <ta e="T964" id="Seg_9934" s="T963">this-PL-LAT</ta>
            <ta e="T965" id="Seg_9935" s="T964">nobleman.[NOM.SG]</ta>
            <ta e="T966" id="Seg_9936" s="T965">bring-IMP.2PL</ta>
            <ta e="T967" id="Seg_9937" s="T966">I.LAT</ta>
            <ta e="T968" id="Seg_9938" s="T967">eat-INF.LAT</ta>
            <ta e="T969" id="Seg_9939" s="T968">this.[NOM.SG]</ta>
            <ta e="T970" id="Seg_9940" s="T969">hear-PST.[3SG]</ta>
            <ta e="T971" id="Seg_9941" s="T970">really</ta>
            <ta e="T972" id="Seg_9942" s="T971">this.[NOM.SG]</ta>
            <ta e="T974" id="Seg_9943" s="T973">this.[NOM.SG]</ta>
            <ta e="T976" id="Seg_9944" s="T975">mill.[NOM.SG]</ta>
            <ta e="T977" id="Seg_9945" s="T976">then</ta>
            <ta e="T978" id="Seg_9946" s="T977">woman-NOM/GEN.3SG</ta>
            <ta e="T979" id="Seg_9947" s="T978">PTCL</ta>
            <ta e="T980" id="Seg_9948" s="T979">INCH</ta>
            <ta e="T981" id="Seg_9949" s="T980">mill-NOM/GEN/ACC.3SG</ta>
            <ta e="T982" id="Seg_9950" s="T981">knit-INF.LAT</ta>
            <ta e="T983" id="Seg_9951" s="T982">pancake-PL</ta>
            <ta e="T984" id="Seg_9952" s="T983">pie-PL</ta>
            <ta e="T985" id="Seg_9953" s="T984">this.[NOM.SG]</ta>
            <ta e="T986" id="Seg_9954" s="T985">eat-PST.[3SG]</ta>
            <ta e="T987" id="Seg_9955" s="T986">sell-IMP.2PL</ta>
            <ta e="T988" id="Seg_9956" s="T987">I.LAT</ta>
            <ta e="T989" id="Seg_9957" s="T988">no</ta>
            <ta e="T990" id="Seg_9958" s="T989">we.NOM</ta>
            <ta e="T991" id="Seg_9959" s="T990">NEG</ta>
            <ta e="T992" id="Seg_9960" s="T991">sell-FUT-1SG</ta>
            <ta e="T993" id="Seg_9961" s="T992">otherwise</ta>
            <ta e="T995" id="Seg_9962" s="T994">we.LAT</ta>
            <ta e="T996" id="Seg_9963" s="T995">eat-INF.LAT</ta>
            <ta e="T997" id="Seg_9964" s="T996">what.[NOM.SG]</ta>
            <ta e="T998" id="Seg_9965" s="T997">then</ta>
            <ta e="T999" id="Seg_9966" s="T998">this.[NOM.SG]</ta>
            <ta e="T1000" id="Seg_9967" s="T999">evening-LOC.ADV</ta>
            <ta e="T1001" id="Seg_9968" s="T1000">come-PST.[3SG]</ta>
            <ta e="T1002" id="Seg_9969" s="T1001">and</ta>
            <ta e="T1003" id="Seg_9970" s="T1002">steal-CVB</ta>
            <ta e="T1004" id="Seg_9971" s="T1003">take-MOM-PST.[3SG]</ta>
            <ta e="T1005" id="Seg_9972" s="T1004">and</ta>
            <ta e="T1006" id="Seg_9973" s="T1005">take.away-RES-PST.[3SG]</ta>
            <ta e="T1007" id="Seg_9974" s="T1006">morning-GEN</ta>
            <ta e="T1008" id="Seg_9975" s="T1007">this-PL</ta>
            <ta e="T1009" id="Seg_9976" s="T1008">get.up-PST-3PL</ta>
            <ta e="T1010" id="Seg_9977" s="T1009">NEG.EX.[3SG]</ta>
            <ta e="T1011" id="Seg_9978" s="T1010">mill-NOM/GEN/ACC.3SG</ta>
            <ta e="T1012" id="Seg_9979" s="T1011">then</ta>
            <ta e="T1013" id="Seg_9980" s="T1012">man.[NOM.SG]</ta>
            <ta e="T1014" id="Seg_9981" s="T1013">hen.[NOM.SG]</ta>
            <ta e="T1015" id="Seg_9982" s="T1014">NEG.AUX-IMP.2SG</ta>
            <ta e="T1016" id="Seg_9983" s="T1015">cry-CNG</ta>
            <ta e="T1017" id="Seg_9984" s="T1016">I.NOM</ta>
            <ta e="T1018" id="Seg_9985" s="T1017">go-FUT-1SG</ta>
            <ta e="T1020" id="Seg_9986" s="T1019">bring-FUT-1SG</ta>
            <ta e="T1021" id="Seg_9987" s="T1020">again</ta>
            <ta e="T1022" id="Seg_9988" s="T1021">you.PL.LAT</ta>
            <ta e="T1023" id="Seg_9989" s="T1022">go-PST.[3SG]</ta>
            <ta e="T1024" id="Seg_9990" s="T1023">walk-PRS.[3SG]</ta>
            <ta e="T1025" id="Seg_9991" s="T1024">walk-PRS.[3SG]</ta>
            <ta e="T1026" id="Seg_9992" s="T1025">come-PRS.[3SG]</ta>
            <ta e="T1027" id="Seg_9993" s="T1026">this-LAT</ta>
            <ta e="T1028" id="Seg_9994" s="T1027">fox.[NOM.SG]</ta>
            <ta e="T1029" id="Seg_9995" s="T1028">where.to</ta>
            <ta e="T1030" id="Seg_9996" s="T1029">walk-PRS-2SG</ta>
            <ta e="T1031" id="Seg_9997" s="T1030">take-IMP.2SG</ta>
            <ta e="T1032" id="Seg_9998" s="T1031">I.ACC</ta>
            <ta e="T1033" id="Seg_9999" s="T1032">%%-EP-CNG</ta>
            <ta e="T1034" id="Seg_10000" s="T1033">I.LAT</ta>
            <ta e="T1035" id="Seg_10001" s="T1034">inside-LAT/LOC.3SG</ta>
            <ta e="T1036" id="Seg_10002" s="T1035">this.[NOM.SG]</ta>
            <ta e="T1037" id="Seg_10003" s="T1036">%%-PST.[3SG]</ta>
            <ta e="T1038" id="Seg_10004" s="T1037">then</ta>
            <ta e="T1039" id="Seg_10005" s="T1038">come-PRS.[3SG]</ta>
            <ta e="T1040" id="Seg_10006" s="T1039">come-PRS.[3SG]</ta>
            <ta e="T1041" id="Seg_10007" s="T1040">wolf-LAT</ta>
            <ta e="T1042" id="Seg_10008" s="T1041">where.to</ta>
            <ta e="T1043" id="Seg_10009" s="T1042">walk-PRS-2SG</ta>
            <ta e="T1044" id="Seg_10010" s="T1043">mill-NOM/GEN/ACC.3SG</ta>
            <ta e="T1045" id="Seg_10011" s="T1044">take-INF.LAT</ta>
            <ta e="T1047" id="Seg_10012" s="T1046">take-IMP.2SG</ta>
            <ta e="T1048" id="Seg_10013" s="T1047">I.ACC</ta>
            <ta e="T1049" id="Seg_10014" s="T1048">%%-EP-IMP.2SG</ta>
            <ta e="T1050" id="Seg_10015" s="T1049">I.LAT</ta>
            <ta e="T1051" id="Seg_10016" s="T1050">inside-LAT/LOC.3SG</ta>
            <ta e="T1052" id="Seg_10017" s="T1051">this.[NOM.SG]</ta>
            <ta e="T1053" id="Seg_10018" s="T1052">%%-PST.[3SG]</ta>
            <ta e="T1054" id="Seg_10019" s="T1053">then</ta>
            <ta e="T1055" id="Seg_10020" s="T1054">come-PRS.[3SG]</ta>
            <ta e="T1056" id="Seg_10021" s="T1055">bear.[NOM.SG]</ta>
            <ta e="T1057" id="Seg_10022" s="T1056">come-PRS.[3SG]</ta>
            <ta e="T1058" id="Seg_10023" s="T1057">%%-EP-IMP.2SG</ta>
            <ta e="T1059" id="Seg_10024" s="T1058">I.LAT</ta>
            <ta e="T1060" id="Seg_10025" s="T1059">then</ta>
            <ta e="T1061" id="Seg_10026" s="T1060">come-PST.[3SG]</ta>
            <ta e="T1062" id="Seg_10027" s="T1061">INCH</ta>
            <ta e="T1063" id="Seg_10028" s="T1062">shout-INF.LAT</ta>
            <ta e="T1065" id="Seg_10029" s="T1064">bring-IMP.2SG.O</ta>
            <ta e="T1066" id="Seg_10030" s="T1065">I.GEN</ta>
            <ta e="T1067" id="Seg_10031" s="T1066">mill-NOM/GEN/ACC.3SG</ta>
            <ta e="T1068" id="Seg_10032" s="T1067">bring-IMP.2SG.O</ta>
            <ta e="T1069" id="Seg_10033" s="T1068">I.GEN</ta>
            <ta e="T1070" id="Seg_10034" s="T1069">mill-NOM/GEN/ACC.3SG</ta>
            <ta e="T1071" id="Seg_10035" s="T1070">this.[NOM.SG]</ta>
            <ta e="T1072" id="Seg_10036" s="T1071">say-IPFVZ.[3SG]</ta>
            <ta e="T1074" id="Seg_10037" s="T1073">throw-IMP.2PL</ta>
            <ta e="T1075" id="Seg_10038" s="T1074">this-ACC</ta>
            <ta e="T1076" id="Seg_10039" s="T1075">and</ta>
            <ta e="T1077" id="Seg_10040" s="T1076">duck-3PL-LAT</ta>
            <ta e="T1078" id="Seg_10041" s="T1077">JUSS</ta>
            <ta e="T1079" id="Seg_10042" s="T1078">duck-PL</ta>
            <ta e="T1080" id="Seg_10043" s="T1079">eat-MOM-IMP.3SG.O</ta>
            <ta e="T1081" id="Seg_10044" s="T1080">bone-NOM/GEN/ACC.3PL</ta>
            <ta e="T1082" id="Seg_10045" s="T1081">this-ACC</ta>
            <ta e="T1083" id="Seg_10046" s="T1082">throw-MOM-PST.[3SG]</ta>
            <ta e="T1084" id="Seg_10047" s="T1083">and</ta>
            <ta e="T1085" id="Seg_10048" s="T1084">this.[NOM.SG]</ta>
            <ta e="T1086" id="Seg_10049" s="T1085">say-IPFVZ.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PKZ">
            <ta e="T5" id="Seg_10050" s="T4">жить-PST.[3SG]</ta>
            <ta e="T6" id="Seg_10051" s="T5">мужчина.[NOM.SG]</ta>
            <ta e="T7" id="Seg_10052" s="T6">этот-GEN</ta>
            <ta e="T8" id="Seg_10053" s="T7">три.[NOM.SG]</ta>
            <ta e="T9" id="Seg_10054" s="T8">мальчик.[NOM.SG]</ta>
            <ta e="T10" id="Seg_10055" s="T9">быть-PST.[3SG]</ta>
            <ta e="T11" id="Seg_10056" s="T10">один.[NOM.SG]</ta>
            <ta e="T12" id="Seg_10057" s="T11">Гаврила.[NOM.SG]</ta>
            <ta e="T13" id="Seg_10058" s="T12">один.[NOM.SG]</ta>
            <ta e="T14" id="Seg_10059" s="T13">Данила.[NOM.SG]</ta>
            <ta e="T15" id="Seg_10060" s="T14">один.[NOM.SG]</ta>
            <ta e="T16" id="Seg_10061" s="T15">Ванюшка.[NOM.SG]</ta>
            <ta e="T17" id="Seg_10062" s="T16">дурачок.[NOM.SG]</ta>
            <ta e="T18" id="Seg_10063" s="T17">тогда</ta>
            <ta e="T20" id="Seg_10064" s="T19">этот.[NOM.SG]</ta>
            <ta e="T21" id="Seg_10065" s="T20">мужчина.[NOM.SG]</ta>
            <ta e="T22" id="Seg_10066" s="T21">сеять-DUR.PST.[3SG]</ta>
            <ta e="T23" id="Seg_10067" s="T22">мука.[NOM.SG]</ta>
            <ta e="T24" id="Seg_10068" s="T23">и</ta>
            <ta e="T25" id="Seg_10069" s="T24">этот.[NOM.SG]</ta>
            <ta e="T26" id="Seg_10070" s="T25">кто.[NOM.SG]=INDEF</ta>
            <ta e="T29" id="Seg_10071" s="T28">вечер-LOC.ADV</ta>
            <ta e="T30" id="Seg_10072" s="T29">прийти-MOM-3PL</ta>
            <ta e="T31" id="Seg_10073" s="T30">и</ta>
            <ta e="T32" id="Seg_10074" s="T31">съесть-DUR.[3SG]</ta>
            <ta e="T33" id="Seg_10075" s="T32">мука</ta>
            <ta e="T34" id="Seg_10076" s="T33">надо</ta>
            <ta e="T35" id="Seg_10077" s="T34">сказать-IPFVZ.[3SG]</ta>
            <ta e="T36" id="Seg_10078" s="T35">пойти-INF.LAT</ta>
            <ta e="T38" id="Seg_10079" s="T37">смотреть-INF.LAT</ta>
            <ta e="T39" id="Seg_10080" s="T38">кто.[NOM.SG]</ta>
            <ta e="T40" id="Seg_10081" s="T39">съесть-PRS-3SG.O</ta>
            <ta e="T41" id="Seg_10082" s="T40">мука.[NOM.SG]</ta>
            <ta e="T42" id="Seg_10083" s="T41">надо</ta>
            <ta e="T43" id="Seg_10084" s="T42">ловить-INF.LAT</ta>
            <ta e="T44" id="Seg_10085" s="T43">тогда</ta>
            <ta e="T45" id="Seg_10086" s="T44">пойти-PST.[3SG]</ta>
            <ta e="T46" id="Seg_10087" s="T45">старший.[NOM.SG]</ta>
            <ta e="T47" id="Seg_10088" s="T46">сын-NOM/GEN.3SG</ta>
            <ta e="T48" id="Seg_10089" s="T47">Гаврила.[NOM.SG]</ta>
            <ta e="T49" id="Seg_10090" s="T48">там</ta>
            <ta e="T50" id="Seg_10091" s="T49">пойти-PST.[3SG]</ta>
            <ta e="T51" id="Seg_10092" s="T50">спать-PST.[3SG]</ta>
            <ta e="T52" id="Seg_10093" s="T51">трава-LOC</ta>
            <ta e="T53" id="Seg_10094" s="T52">и</ta>
            <ta e="T54" id="Seg_10095" s="T53">прийти-PST.[3SG]</ta>
            <ta e="T55" id="Seg_10096" s="T54">дом-LAT</ta>
            <ta e="T56" id="Seg_10097" s="T55">сидеть-PST-1SG</ta>
            <ta e="T57" id="Seg_10098" s="T56">NEG</ta>
            <ta e="T58" id="Seg_10099" s="T57">спать-PST-1SG</ta>
            <ta e="T59" id="Seg_10100" s="T58">а</ta>
            <ta e="T60" id="Seg_10101" s="T59">кто.[NOM.SG]=INDEF</ta>
            <ta e="T61" id="Seg_10102" s="T60">NEG</ta>
            <ta e="T62" id="Seg_10103" s="T61">видеть-PST-1SG</ta>
            <ta e="T63" id="Seg_10104" s="T62">тогда</ta>
            <ta e="T64" id="Seg_10105" s="T63">другой.[NOM.SG]</ta>
            <ta e="T65" id="Seg_10106" s="T64">сын-NOM/GEN.3SG</ta>
            <ta e="T66" id="Seg_10107" s="T65">идти-PRS.[3SG]</ta>
            <ta e="T67" id="Seg_10108" s="T66">Данила-NOM/GEN.3SG</ta>
            <ta e="T68" id="Seg_10109" s="T67">пойти-PST.[3SG]</ta>
            <ta e="T69" id="Seg_10110" s="T68">спать-PST.[3SG]</ta>
            <ta e="T70" id="Seg_10111" s="T69">трава-LOC</ta>
            <ta e="T71" id="Seg_10112" s="T70">тогда</ta>
            <ta e="T72" id="Seg_10113" s="T71">утро-LOC.ADV</ta>
            <ta e="T73" id="Seg_10114" s="T72">прийти-PST.[3SG]</ta>
            <ta e="T74" id="Seg_10115" s="T73">весь</ta>
            <ta e="T75" id="Seg_10116" s="T74">вечер.[NOM.SG]</ta>
            <ta e="T76" id="Seg_10117" s="T75">NEG</ta>
            <ta e="T77" id="Seg_10118" s="T76">спать-PST-1SG</ta>
            <ta e="T78" id="Seg_10119" s="T77">а</ta>
            <ta e="T79" id="Seg_10120" s="T78">кто-ACC=INDEF</ta>
            <ta e="T80" id="Seg_10121" s="T79">NEG</ta>
            <ta e="T81" id="Seg_10122" s="T80">видеть-PST-1SG</ta>
            <ta e="T82" id="Seg_10123" s="T81">тогда</ta>
            <ta e="T83" id="Seg_10124" s="T82">Ванюшка.[NOM.SG]</ta>
            <ta e="T84" id="Seg_10125" s="T83">пойти-PST.[3SG]</ta>
            <ta e="T85" id="Seg_10126" s="T84">взять-PST.[3SG]</ta>
            <ta e="T86" id="Seg_10127" s="T85">аркан-NOM/GEN/ACC.3SG</ta>
            <ta e="T87" id="Seg_10128" s="T86">тогда</ta>
            <ta e="T88" id="Seg_10129" s="T87">сидеть-PST.[3SG]</ta>
            <ta e="T89" id="Seg_10130" s="T88">камень-LAT</ta>
            <ta e="T90" id="Seg_10131" s="T89">NEG</ta>
            <ta e="T91" id="Seg_10132" s="T90">спать-PRS.[3SG]</ta>
            <ta e="T92" id="Seg_10133" s="T91">сидеть-DUR.[3SG]</ta>
            <ta e="T93" id="Seg_10134" s="T92">видеть-PRS-3SG.O</ta>
            <ta e="T94" id="Seg_10135" s="T93">лошадь.[NOM.SG]</ta>
            <ta e="T95" id="Seg_10136" s="T94">прийти-PST.[3SG]</ta>
            <ta e="T96" id="Seg_10137" s="T95">всякий</ta>
            <ta e="T97" id="Seg_10138" s="T96">и</ta>
            <ta e="T98" id="Seg_10139" s="T97">PTCL</ta>
            <ta e="T99" id="Seg_10140" s="T98">там</ta>
            <ta e="T100" id="Seg_10141" s="T99">кожа-LOC</ta>
            <ta e="T101" id="Seg_10142" s="T100">шерсть-NOM/GEN/ACC.3SG</ta>
            <ta e="T102" id="Seg_10143" s="T101">красивый.[NOM.SG]</ta>
            <ta e="T103" id="Seg_10144" s="T102">этот.[NOM.SG]</ta>
            <ta e="T104" id="Seg_10145" s="T103">PTCL</ta>
            <ta e="T105" id="Seg_10146" s="T104">бросить-MOM-PST.[3SG]</ta>
            <ta e="T106" id="Seg_10147" s="T105">аркан-NOM/GEN/ACC.3SG</ta>
            <ta e="T107" id="Seg_10148" s="T106">этот-LAT</ta>
            <ta e="T108" id="Seg_10149" s="T107">и</ta>
            <ta e="T109" id="Seg_10150" s="T108">ловить-MOM-PST.[3SG]</ta>
            <ta e="T110" id="Seg_10151" s="T109">этот.[NOM.SG]</ta>
            <ta e="T111" id="Seg_10152" s="T110">прыгнуть-MOM-PST.[3SG]</ta>
            <ta e="T112" id="Seg_10153" s="T111">NEG</ta>
            <ta e="T113" id="Seg_10154" s="T112">мочь-PRS.[3SG]</ta>
            <ta e="T114" id="Seg_10155" s="T113">прыгнуть-INF.LAT</ta>
            <ta e="T115" id="Seg_10156" s="T114">тогда</ta>
            <ta e="T116" id="Seg_10157" s="T115">пускать-IMP.2SG</ta>
            <ta e="T117" id="Seg_10158" s="T116">я.ACC</ta>
            <ta e="T118" id="Seg_10159" s="T117">я.NOM</ta>
            <ta e="T119" id="Seg_10160" s="T118">ты.DAT</ta>
            <ta e="T120" id="Seg_10161" s="T119">PTCL</ta>
            <ta e="T121" id="Seg_10162" s="T120">что.[NOM.SG]</ta>
            <ta e="T122" id="Seg_10163" s="T121">дать-FUT-1SG</ta>
            <ta e="T123" id="Seg_10164" s="T122">ну</ta>
            <ta e="T124" id="Seg_10165" s="T123">как</ta>
            <ta e="T125" id="Seg_10166" s="T124">сказать-IMP.2SG</ta>
            <ta e="T126" id="Seg_10167" s="T125">еще</ta>
            <ta e="T127" id="Seg_10168" s="T126">NEG</ta>
            <ta e="T128" id="Seg_10169" s="T127">съесть-PRS-2SG</ta>
            <ta e="T129" id="Seg_10170" s="T128">мука.[NOM.SG]</ta>
            <ta e="T130" id="Seg_10171" s="T129">NEG</ta>
            <ta e="T131" id="Seg_10172" s="T130">съесть-FUT-1SG</ta>
            <ta e="T132" id="Seg_10173" s="T131">а</ta>
            <ta e="T133" id="Seg_10174" s="T132">как</ta>
            <ta e="T134" id="Seg_10175" s="T133">прийти-FUT-2SG</ta>
            <ta e="T135" id="Seg_10176" s="T134">так</ta>
            <ta e="T136" id="Seg_10177" s="T135">я.LAT</ta>
            <ta e="T137" id="Seg_10178" s="T136">прийти-IMP.2SG</ta>
            <ta e="T138" id="Seg_10179" s="T137">здесь</ta>
            <ta e="T139" id="Seg_10180" s="T138">%%-IMP.2SG</ta>
            <ta e="T141" id="Seg_10181" s="T140">я.ACC</ta>
            <ta e="T142" id="Seg_10182" s="T141">ну</ta>
            <ta e="T143" id="Seg_10183" s="T142">такой.[NOM.SG]</ta>
            <ta e="T144" id="Seg_10184" s="T143">и</ta>
            <ta e="T145" id="Seg_10185" s="T144">пойти-CVB</ta>
            <ta e="T146" id="Seg_10186" s="T145">исчезнуть-PST.[3SG]</ta>
            <ta e="T147" id="Seg_10187" s="T146">лошадь-NOM/GEN.3SG</ta>
            <ta e="T148" id="Seg_10188" s="T147">а</ta>
            <ta e="T149" id="Seg_10189" s="T148">этот.[NOM.SG]</ta>
            <ta e="T150" id="Seg_10190" s="T149">прийти-PST.[3SG]</ta>
            <ta e="T151" id="Seg_10191" s="T150">дом-LAT/LOC.3SG</ta>
            <ta e="T152" id="Seg_10192" s="T151">печь-LAT</ta>
            <ta e="T153" id="Seg_10193" s="T152">надеть-MOM-PST.[3SG]</ta>
            <ta e="T154" id="Seg_10194" s="T153">я.NOM</ta>
            <ta e="T155" id="Seg_10195" s="T154">ловить-PST-1SG</ta>
            <ta e="T156" id="Seg_10196" s="T155">лошадь.[NOM.SG]</ta>
            <ta e="T157" id="Seg_10197" s="T156">там</ta>
            <ta e="T158" id="Seg_10198" s="T157">сейчас</ta>
            <ta e="T159" id="Seg_10199" s="T158">когда=INDEF</ta>
            <ta e="T160" id="Seg_10200" s="T159">NEG</ta>
            <ta e="T161" id="Seg_10201" s="T160">прийти-FUT-3SG</ta>
            <ta e="T162" id="Seg_10202" s="T161">съесть-INF.LAT</ta>
            <ta e="T163" id="Seg_10203" s="T162">мука.[NOM.SG]</ta>
            <ta e="T164" id="Seg_10204" s="T163">тогда</ta>
            <ta e="T165" id="Seg_10205" s="T164">вождь.[NOM.SG]</ta>
            <ta e="T166" id="Seg_10206" s="T165">делать-PST.[3SG]</ta>
            <ta e="T167" id="Seg_10207" s="T166">чтобы</ta>
            <ta e="T168" id="Seg_10208" s="T167">люди.[NOM.SG]</ta>
            <ta e="T169" id="Seg_10209" s="T168">прийти-PST-3PL</ta>
            <ta e="T171" id="Seg_10210" s="T170">этот-GEN</ta>
            <ta e="T172" id="Seg_10211" s="T171">девушка.[NOM.SG]</ta>
            <ta e="T173" id="Seg_10212" s="T172">жить-DUR.[3SG]</ta>
            <ta e="T174" id="Seg_10213" s="T173">терем-LOC</ta>
            <ta e="T175" id="Seg_10214" s="T174">весь</ta>
            <ta e="T176" id="Seg_10215" s="T175">люди.[NOM.SG]</ta>
            <ta e="T177" id="Seg_10216" s="T176">пойти-DUR-3PL</ta>
            <ta e="T178" id="Seg_10217" s="T177">и</ta>
            <ta e="T179" id="Seg_10218" s="T178">и</ta>
            <ta e="T180" id="Seg_10219" s="T179">этот-GEN</ta>
            <ta e="T181" id="Seg_10220" s="T180">брат-PL</ta>
            <ta e="T182" id="Seg_10221" s="T181">пойти-IPFVZ-PRS-3PL</ta>
            <ta e="T183" id="Seg_10222" s="T182">а</ta>
            <ta e="T184" id="Seg_10223" s="T183">этот.[NOM.SG]</ta>
            <ta e="T185" id="Seg_10224" s="T184">сказать-IPFVZ.[3SG]</ta>
            <ta e="T186" id="Seg_10225" s="T185">взять-IMP.2PL</ta>
            <ta e="T187" id="Seg_10226" s="T186">я.ACC</ta>
            <ta e="T188" id="Seg_10227" s="T187">куда</ta>
            <ta e="T189" id="Seg_10228" s="T188">ты.GEN</ta>
            <ta e="T190" id="Seg_10229" s="T189">пойти-FUT-2SG</ta>
            <ta e="T191" id="Seg_10230" s="T190">что.[NOM.SG]=INDEF</ta>
            <ta e="T192" id="Seg_10231" s="T191">там</ta>
            <ta e="T193" id="Seg_10232" s="T192">NEG</ta>
            <ta e="T194" id="Seg_10233" s="T193">видеть-PST-2SG</ta>
            <ta e="T195" id="Seg_10234" s="T194">чтобы</ta>
            <ta e="T196" id="Seg_10235" s="T195">люди.[NOM.SG]</ta>
            <ta e="T197" id="Seg_10236" s="T196">смеяться-FRQ-PST-3PL</ta>
            <ta e="T199" id="Seg_10237" s="T198">ты.ACC</ta>
            <ta e="T200" id="Seg_10238" s="T199">этот-PL</ta>
            <ta e="T201" id="Seg_10239" s="T200">пойти-CVB</ta>
            <ta e="T202" id="Seg_10240" s="T201">исчезнуть-PST-3PL</ta>
            <ta e="T203" id="Seg_10241" s="T202">этот.[NOM.SG]</ta>
            <ta e="T204" id="Seg_10242" s="T203">встать-PST.[3SG]</ta>
            <ta e="T205" id="Seg_10243" s="T204">корзина.[NOM.SG]</ta>
            <ta e="T206" id="Seg_10244" s="T205">взять-PST.[3SG]</ta>
            <ta e="T207" id="Seg_10245" s="T206">пойти-PST.[3SG]</ta>
            <ta e="T208" id="Seg_10246" s="T207">и</ta>
            <ta e="T209" id="Seg_10247" s="T208">INCH</ta>
            <ta e="T210" id="Seg_10248" s="T209">кричать-INF.LAT</ta>
            <ta e="T211" id="Seg_10249" s="T210">лошадь-NOM/GEN.3SG</ta>
            <ta e="T212" id="Seg_10250" s="T211">прийти-PST.[3SG]</ta>
            <ta e="T213" id="Seg_10251" s="T212">PTCL</ta>
            <ta e="T214" id="Seg_10252" s="T213">здесь</ta>
            <ta e="T215" id="Seg_10253" s="T214">%%-LOC</ta>
            <ta e="T216" id="Seg_10254" s="T215">прийти-PRS-3PL</ta>
            <ta e="T217" id="Seg_10255" s="T216">тот.[NOM.SG]</ta>
            <ta e="T218" id="Seg_10256" s="T217">мужчина-LAT/LOC.3SG</ta>
            <ta e="T219" id="Seg_10257" s="T218">прийти-PRS.[3SG]</ta>
            <ta e="T220" id="Seg_10258" s="T219">тогда</ta>
            <ta e="T221" id="Seg_10259" s="T220">этот.[NOM.SG]</ta>
            <ta e="T222" id="Seg_10260" s="T221">лошадь.[NOM.SG]</ta>
            <ta e="T224" id="Seg_10261" s="T222">сказать-IPFVZ.[3SG]</ta>
            <ta e="T225" id="Seg_10262" s="T224">стоять-PST.[3SG]</ta>
            <ta e="T226" id="Seg_10263" s="T225">этот-LAT</ta>
            <ta e="T227" id="Seg_10264" s="T226">сказать-IPFVZ.[3SG]</ta>
            <ta e="T228" id="Seg_10265" s="T227">один.[NOM.SG]</ta>
            <ta e="T229" id="Seg_10266" s="T228">ухо-LAT</ta>
            <ta e="T230" id="Seg_10267" s="T229">лезть-IMP.2PL</ta>
            <ta e="T231" id="Seg_10268" s="T230">один-ABL</ta>
            <ta e="T232" id="Seg_10269" s="T231">уйти-IMP.2SG</ta>
            <ta e="T233" id="Seg_10270" s="T232">этот.[NOM.SG]</ta>
            <ta e="T234" id="Seg_10271" s="T233">лезть-PST.[3SG]</ta>
            <ta e="T235" id="Seg_10272" s="T234">уйти-PST.[3SG]</ta>
            <ta e="T236" id="Seg_10273" s="T235">красивый.[NOM.SG]</ta>
            <ta e="T237" id="Seg_10274" s="T236">стать-RES-PST.[3SG]</ta>
            <ta e="T238" id="Seg_10275" s="T237">тогда</ta>
            <ta e="T239" id="Seg_10276" s="T238">этот-LAT</ta>
            <ta e="T240" id="Seg_10277" s="T239">сидеть-PST.[3SG]</ta>
            <ta e="T241" id="Seg_10278" s="T240">этот.[NOM.SG]</ta>
            <ta e="T242" id="Seg_10279" s="T241">бежать</ta>
            <ta e="T243" id="Seg_10280" s="T242">бежать-MOM-PST.[3SG]</ta>
            <ta e="T244" id="Seg_10281" s="T243">там</ta>
            <ta e="T245" id="Seg_10282" s="T244">люди.[NOM.SG]</ta>
            <ta e="T246" id="Seg_10283" s="T245">много</ta>
            <ta e="T247" id="Seg_10284" s="T246">стоять-PRS-3PL</ta>
            <ta e="T248" id="Seg_10285" s="T247">а</ta>
            <ta e="T249" id="Seg_10286" s="T248">кто.[NOM.SG]=INDEF</ta>
            <ta e="T250" id="Seg_10287" s="T249">NEG</ta>
            <ta e="T251" id="Seg_10288" s="T250">этот.[NOM.SG]</ta>
            <ta e="T252" id="Seg_10289" s="T251">дочь-GEN.1SG</ta>
            <ta e="T253" id="Seg_10290" s="T252">NEG</ta>
            <ta e="T254" id="Seg_10291" s="T253">влезать-PRS-3PL</ta>
            <ta e="T255" id="Seg_10292" s="T254">этот.[NOM.SG]</ta>
            <ta e="T256" id="Seg_10293" s="T255">PTCL</ta>
            <ta e="T257" id="Seg_10294" s="T256">прыгнуть-MOM-PST.[3SG]</ta>
            <ta e="T258" id="Seg_10295" s="T257">спрятаться-FUT-3PL</ta>
            <ta e="T259" id="Seg_10296" s="T258">NEG</ta>
            <ta e="T260" id="Seg_10297" s="T259">NEG</ta>
            <ta e="T261" id="Seg_10298" s="T260">ловить-PST.[3SG]</ta>
            <ta e="T262" id="Seg_10299" s="T261">тогда</ta>
            <ta e="T263" id="Seg_10300" s="T262">люди.[NOM.SG]</ta>
            <ta e="T264" id="Seg_10301" s="T263">весь</ta>
            <ta e="T265" id="Seg_10302" s="T264">кричать-DUR-3PL</ta>
            <ta e="T266" id="Seg_10303" s="T265">ловить-IMP.2SG</ta>
            <ta e="T267" id="Seg_10304" s="T266">ловить-IMP.2SG</ta>
            <ta e="T268" id="Seg_10305" s="T267">а</ta>
            <ta e="T269" id="Seg_10306" s="T268">этот.[NOM.SG]</ta>
            <ta e="T270" id="Seg_10307" s="T269">бежать-MOM-PST.[3SG]</ta>
            <ta e="T271" id="Seg_10308" s="T270">лошадь-ACC</ta>
            <ta e="T272" id="Seg_10309" s="T271">посылать-PST.[3SG]</ta>
            <ta e="T273" id="Seg_10310" s="T272">тогда</ta>
            <ta e="T274" id="Seg_10311" s="T273">гриб-NOM/GEN/ACC.3PL</ta>
            <ta e="T275" id="Seg_10312" s="T274">собирать-PST.[3SG]</ta>
            <ta e="T276" id="Seg_10313" s="T275">и</ta>
            <ta e="T277" id="Seg_10314" s="T276">всякий</ta>
            <ta e="T278" id="Seg_10315" s="T277">много</ta>
            <ta e="T279" id="Seg_10316" s="T278">гриб.[NOM.SG]</ta>
            <ta e="T280" id="Seg_10317" s="T279">принести-PST.[3SG]</ta>
            <ta e="T281" id="Seg_10318" s="T280">а</ta>
            <ta e="T282" id="Seg_10319" s="T281">женщина-PL</ta>
            <ta e="T283" id="Seg_10320" s="T282">ругать-DUR-3PL</ta>
            <ta e="T284" id="Seg_10321" s="T283">что.[NOM.SG]</ta>
            <ta e="T285" id="Seg_10322" s="T284">такой.[NOM.SG]</ta>
            <ta e="T287" id="Seg_10323" s="T286">принести-PST-2SG</ta>
            <ta e="T288" id="Seg_10324" s="T287">только</ta>
            <ta e="T289" id="Seg_10325" s="T288">ты.DAT</ta>
            <ta e="T290" id="Seg_10326" s="T289">съесть-INF.LAT</ta>
            <ta e="T291" id="Seg_10327" s="T290">этот.[NOM.SG]</ta>
            <ta e="T292" id="Seg_10328" s="T291">печь-LAT</ta>
            <ta e="T294" id="Seg_10329" s="T293">влезать-PST.[3SG]</ta>
            <ta e="T295" id="Seg_10330" s="T294">лежать-DUR.[3SG]</ta>
            <ta e="T296" id="Seg_10331" s="T295">тогда</ta>
            <ta e="T298" id="Seg_10332" s="T297">брат-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T299" id="Seg_10333" s="T298">прийти-PST-3PL</ta>
            <ta e="T300" id="Seg_10334" s="T299">PTCL</ta>
            <ta e="T301" id="Seg_10335" s="T300">сказать-DUR-3PL</ta>
            <ta e="T303" id="Seg_10336" s="T302">как</ta>
            <ta e="T304" id="Seg_10337" s="T303">быть-PST.[3SG]</ta>
            <ta e="T305" id="Seg_10338" s="T304">а</ta>
            <ta e="T306" id="Seg_10339" s="T305">этот.[NOM.SG]</ta>
            <ta e="T307" id="Seg_10340" s="T306">лежать-DUR.[3SG]</ta>
            <ta e="T308" id="Seg_10341" s="T307">этот.[NOM.SG]</ta>
            <ta e="T309" id="Seg_10342" s="T308">я.GEN</ta>
            <ta e="T310" id="Seg_10343" s="T309">голова-NOM/GEN/ACC.1SG</ta>
            <ta e="T311" id="Seg_10344" s="T310">там</ta>
            <ta e="T312" id="Seg_10345" s="T311">быть-PST.[3SG]</ta>
            <ta e="T313" id="Seg_10346" s="T312">NEG.AUX-IMP.2SG</ta>
            <ta e="T314" id="Seg_10347" s="T313">лгать-CNG</ta>
            <ta e="T315" id="Seg_10348" s="T314">ты.GEN</ta>
            <ta e="T316" id="Seg_10349" s="T315">голова-NOM/GEN/ACC.1SG</ta>
            <ta e="T317" id="Seg_10350" s="T316">сидеть-IMP.2SG</ta>
            <ta e="T318" id="Seg_10351" s="T317">PTCL</ta>
            <ta e="T319" id="Seg_10352" s="T318">что.[NOM.SG]=INDEF</ta>
            <ta e="T320" id="Seg_10353" s="T319">NEG.AUX-IMP.2SG</ta>
            <ta e="T321" id="Seg_10354" s="T320">говорить-EP-CNG</ta>
            <ta e="T322" id="Seg_10355" s="T321">такой.[NOM.SG]</ta>
            <ta e="T323" id="Seg_10356" s="T322">мужчина.[NOM.SG]</ta>
            <ta e="T324" id="Seg_10357" s="T323">быть-PST.[3SG]</ta>
            <ta e="T325" id="Seg_10358" s="T324">лошадь-NOM/GEN.3SG</ta>
            <ta e="T326" id="Seg_10359" s="T325">красивый.[NOM.SG]</ta>
            <ta e="T327" id="Seg_10360" s="T326">быть-PST.[3SG]</ta>
            <ta e="T328" id="Seg_10361" s="T327">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T329" id="Seg_10362" s="T328">тогда</ta>
            <ta e="T330" id="Seg_10363" s="T329">завтра</ta>
            <ta e="T331" id="Seg_10364" s="T330">опять</ta>
            <ta e="T332" id="Seg_10365" s="T331">пойти-IPFVZ-PRS-3PL</ta>
            <ta e="T333" id="Seg_10366" s="T332">пойти-CVB</ta>
            <ta e="T334" id="Seg_10367" s="T333">исчезнуть-PST-3PL</ta>
            <ta e="T335" id="Seg_10368" s="T334">этот.[NOM.SG]</ta>
            <ta e="T336" id="Seg_10369" s="T335">печь-ABL</ta>
            <ta e="T338" id="Seg_10370" s="T337">прыгнуть-MOM-PST.[3SG]</ta>
            <ta e="T339" id="Seg_10371" s="T338">корзина.[NOM.SG]</ta>
            <ta e="T340" id="Seg_10372" s="T339">взять-PST.[3SG]</ta>
            <ta e="T341" id="Seg_10373" s="T340">пойти-PST.[3SG]</ta>
            <ta e="T342" id="Seg_10374" s="T341">пойти-PST.[3SG]</ta>
            <ta e="T344" id="Seg_10375" s="T342">выбросить-PST.[3SG]</ta>
            <ta e="T345" id="Seg_10376" s="T344">INCH</ta>
            <ta e="T346" id="Seg_10377" s="T345">лошадь-ACC</ta>
            <ta e="T347" id="Seg_10378" s="T346">кричать-INF.LAT</ta>
            <ta e="T348" id="Seg_10379" s="T347">лошадь-NOM/GEN.3SG</ta>
            <ta e="T349" id="Seg_10380" s="T348">прийти-PST.[3SG]</ta>
            <ta e="T350" id="Seg_10381" s="T349">опять</ta>
            <ta e="T351" id="Seg_10382" s="T350">один.[NOM.SG]</ta>
            <ta e="T352" id="Seg_10383" s="T351">сопли-NOM/GEN/ACC.3SG</ta>
            <ta e="T353" id="Seg_10384" s="T352">варить-RES-PST.[3SG]</ta>
            <ta e="T354" id="Seg_10385" s="T353">один.[NOM.SG]</ta>
            <ta e="T355" id="Seg_10386" s="T354">уйти-RES-PST.[3SG]</ta>
            <ta e="T356" id="Seg_10387" s="T355">тогда</ta>
            <ta e="T357" id="Seg_10388" s="T356">сесть-PST.[3SG]</ta>
            <ta e="T359" id="Seg_10389" s="T358">прийти-PRS.[3SG]</ta>
            <ta e="T360" id="Seg_10390" s="T359">люди.[NOM.SG]</ta>
            <ta e="T361" id="Seg_10391" s="T360">много</ta>
            <ta e="T362" id="Seg_10392" s="T361">стоять-PRS-3PL</ta>
            <ta e="T363" id="Seg_10393" s="T362">тогда</ta>
            <ta e="T364" id="Seg_10394" s="T363">кто.[NOM.SG]=INDEF</ta>
            <ta e="T365" id="Seg_10395" s="T364">NEG</ta>
            <ta e="T366" id="Seg_10396" s="T365">бежать-PRS.[3SG]</ta>
            <ta e="T367" id="Seg_10397" s="T366">там</ta>
            <ta e="T368" id="Seg_10398" s="T367">там</ta>
            <ta e="T370" id="Seg_10399" s="T369">прыгнуть-MOM-PST.[3SG]</ta>
            <ta e="T371" id="Seg_10400" s="T370">взять-PST.[3SG]</ta>
            <ta e="T376" id="Seg_10401" s="T375">NEG</ta>
            <ta e="T377" id="Seg_10402" s="T376">мочь-PST.[3SG]</ta>
            <ta e="T378" id="Seg_10403" s="T377">тогда</ta>
            <ta e="T379" id="Seg_10404" s="T378">вернуться-MOM-PST.[3SG]</ta>
            <ta e="T380" id="Seg_10405" s="T379">брат-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T381" id="Seg_10406" s="T380">весь</ta>
            <ta e="T382" id="Seg_10407" s="T381">бить-PST.[3SG]</ta>
            <ta e="T383" id="Seg_10408" s="T382">и</ta>
            <ta e="T384" id="Seg_10409" s="T383">пойти-CVB</ta>
            <ta e="T385" id="Seg_10410" s="T384">исчезнуть-PST.[3SG]</ta>
            <ta e="T386" id="Seg_10411" s="T385">а</ta>
            <ta e="T387" id="Seg_10412" s="T386">люди.[NOM.SG]</ta>
            <ta e="T388" id="Seg_10413" s="T387">кричать-DUR-3PL</ta>
            <ta e="T389" id="Seg_10414" s="T388">ловить-IMP.2SG</ta>
            <ta e="T390" id="Seg_10415" s="T389">ловить-IMP.2SG</ta>
            <ta e="T391" id="Seg_10416" s="T390">и</ta>
            <ta e="T392" id="Seg_10417" s="T391">этот.[NOM.SG]</ta>
            <ta e="T393" id="Seg_10418" s="T392">NEG.AUX-IMP.2SG</ta>
            <ta e="T394" id="Seg_10419" s="T393">отдать-PST</ta>
            <ta e="T395" id="Seg_10420" s="T394">лошадь-ACC.3SG</ta>
            <ta e="T396" id="Seg_10421" s="T395">посылать-PST.[3SG]</ta>
            <ta e="T397" id="Seg_10422" s="T396">а</ta>
            <ta e="T398" id="Seg_10423" s="T397">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T399" id="Seg_10424" s="T398">опять</ta>
            <ta e="T400" id="Seg_10425" s="T399">прийти-PST.[3SG]</ta>
            <ta e="T401" id="Seg_10426" s="T400">женщина-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T402" id="Seg_10427" s="T401">опять</ta>
            <ta e="T403" id="Seg_10428" s="T402">ругать-DUR-3PL</ta>
            <ta e="T404" id="Seg_10429" s="T403">а</ta>
            <ta e="T405" id="Seg_10430" s="T404">этот.[NOM.SG]</ta>
            <ta e="T406" id="Seg_10431" s="T405">печь-LAT</ta>
            <ta e="T407" id="Seg_10432" s="T406">влезать-PST.[3SG]</ta>
            <ta e="T408" id="Seg_10433" s="T407">и</ta>
            <ta e="T409" id="Seg_10434" s="T408">лежать-DUR.[3SG]</ta>
            <ta e="T411" id="Seg_10435" s="T410">брат.PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T412" id="Seg_10436" s="T411">прийти-PST-3PL</ta>
            <ta e="T413" id="Seg_10437" s="T412">отец-LAT/LOC.3SG</ta>
            <ta e="T414" id="Seg_10438" s="T413">сказать-PRS-3PL</ta>
            <ta e="T415" id="Seg_10439" s="T414">какой</ta>
            <ta e="T416" id="Seg_10440" s="T415">мужчина.[NOM.SG]</ta>
            <ta e="T417" id="Seg_10441" s="T416">быть-PST.[3SG]</ta>
            <ta e="T418" id="Seg_10442" s="T417">лошадь-NOM/GEN.3SG</ta>
            <ta e="T419" id="Seg_10443" s="T418">очень</ta>
            <ta e="T420" id="Seg_10444" s="T419">красивый.[NOM.SG]</ta>
            <ta e="T421" id="Seg_10445" s="T420">и</ta>
            <ta e="T423" id="Seg_10446" s="T422">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T424" id="Seg_10447" s="T423">красивый.[NOM.SG]</ta>
            <ta e="T425" id="Seg_10448" s="T424">а</ta>
            <ta e="T426" id="Seg_10449" s="T425">этот.[NOM.SG]</ta>
            <ta e="T427" id="Seg_10450" s="T426">лежать-DUR.[3SG]</ta>
            <ta e="T428" id="Seg_10451" s="T427">NEG</ta>
            <ta e="T429" id="Seg_10452" s="T428">я.NOM</ta>
            <ta e="T430" id="Seg_10453" s="T429">ли</ta>
            <ta e="T431" id="Seg_10454" s="T430">голова-NOM/GEN/ACC.1SG</ta>
            <ta e="T432" id="Seg_10455" s="T431">там</ta>
            <ta e="T433" id="Seg_10456" s="T432">быть-PST.[3SG]</ta>
            <ta e="T434" id="Seg_10457" s="T433">NEG.AUX-IMP.2SG</ta>
            <ta e="T435" id="Seg_10458" s="T434">лгать-CNG</ta>
            <ta e="T436" id="Seg_10459" s="T435">лежать-IMP.2SG</ta>
            <ta e="T437" id="Seg_10460" s="T436">PTCL</ta>
            <ta e="T438" id="Seg_10461" s="T437">ум-CAR.ADJ</ta>
            <ta e="T439" id="Seg_10462" s="T438">мальчик.[NOM.SG]</ta>
            <ta e="T440" id="Seg_10463" s="T439">ты.GEN</ta>
            <ta e="T441" id="Seg_10464" s="T440">там</ta>
            <ta e="T442" id="Seg_10465" s="T441">быть-PST-2SG</ta>
            <ta e="T443" id="Seg_10466" s="T442">тогда</ta>
            <ta e="T444" id="Seg_10467" s="T443">опять</ta>
            <ta e="T445" id="Seg_10468" s="T444">три.[NOM.SG]</ta>
            <ta e="T446" id="Seg_10469" s="T445">день.[NOM.SG]</ta>
            <ta e="T447" id="Seg_10470" s="T446">опять</ta>
            <ta e="T448" id="Seg_10471" s="T447">пойти-IPFVZ-PRS-3PL</ta>
            <ta e="T449" id="Seg_10472" s="T448">пойти-CVB</ta>
            <ta e="T450" id="Seg_10473" s="T449">исчезнуть-PST-3PL</ta>
            <ta e="T451" id="Seg_10474" s="T450">этот.[NOM.SG]</ta>
            <ta e="T453" id="Seg_10475" s="T452">встать-PST.[3SG]</ta>
            <ta e="T454" id="Seg_10476" s="T453">корзина-ACC.3SG</ta>
            <ta e="T455" id="Seg_10477" s="T454">взять-PST.[3SG]</ta>
            <ta e="T456" id="Seg_10478" s="T455">пойти-PST.[3SG]</ta>
            <ta e="T457" id="Seg_10479" s="T456">гриб-NOM/GEN/ACC.3PL</ta>
            <ta e="T458" id="Seg_10480" s="T457">собирать-INF.LAT</ta>
            <ta e="T459" id="Seg_10481" s="T458">пойти-PST.[3SG]</ta>
            <ta e="T460" id="Seg_10482" s="T459">INCH</ta>
            <ta e="T461" id="Seg_10483" s="T460">лошадь.[NOM.SG]</ta>
            <ta e="T462" id="Seg_10484" s="T461">кричать-INF.LAT</ta>
            <ta e="T463" id="Seg_10485" s="T462">лошадь.[NOM.SG]</ta>
            <ta e="T464" id="Seg_10486" s="T463">прийти-PST.[3SG]</ta>
            <ta e="T465" id="Seg_10487" s="T464">этот.[NOM.SG]</ta>
            <ta e="T466" id="Seg_10488" s="T465">опять</ta>
            <ta e="T467" id="Seg_10489" s="T466">вдоль</ta>
            <ta e="T469" id="Seg_10490" s="T468">один.[NOM.SG]</ta>
            <ta e="T470" id="Seg_10491" s="T469">ухо-LAT/LOC.3SG</ta>
            <ta e="T471" id="Seg_10492" s="T470">варить-RES-PST.[3SG]</ta>
            <ta e="T472" id="Seg_10493" s="T471">один.[NOM.SG]</ta>
            <ta e="T473" id="Seg_10494" s="T472">уйти-RES-PST.[3SG]</ta>
            <ta e="T474" id="Seg_10495" s="T473">сидеть-PST.[3SG]</ta>
            <ta e="T475" id="Seg_10496" s="T474">пойти-PST.[3SG]</ta>
            <ta e="T476" id="Seg_10497" s="T475">тогда</ta>
            <ta e="T477" id="Seg_10498" s="T476">как</ta>
            <ta e="T478" id="Seg_10499" s="T477">бежать-MOM-PST.[3SG]</ta>
            <ta e="T479" id="Seg_10500" s="T478">этот-ACC</ta>
            <ta e="T480" id="Seg_10501" s="T479">целовать-PST.[3SG]</ta>
            <ta e="T481" id="Seg_10502" s="T480">и</ta>
            <ta e="T482" id="Seg_10503" s="T481">колечко-ACC.3SG</ta>
            <ta e="T483" id="Seg_10504" s="T482">взять-MOM-PST.[3SG]</ta>
            <ta e="T484" id="Seg_10505" s="T483">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T485" id="Seg_10506" s="T484">рука-LAT/LOC.3SG</ta>
            <ta e="T487" id="Seg_10507" s="T486">надеть-PST.[3SG]</ta>
            <ta e="T488" id="Seg_10508" s="T487">пойти-CVB</ta>
            <ta e="T489" id="Seg_10509" s="T488">исчезнуть-PST.[3SG]</ta>
            <ta e="T490" id="Seg_10510" s="T489">лошадь-ACC.3SG</ta>
            <ta e="T491" id="Seg_10511" s="T490">посылать-PST.[3SG]</ta>
            <ta e="T492" id="Seg_10512" s="T491">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T493" id="Seg_10513" s="T492">прийти-PST.[3SG]</ta>
            <ta e="T494" id="Seg_10514" s="T493">дом-LAT/LOC.3SG</ta>
            <ta e="T495" id="Seg_10515" s="T494">и</ta>
            <ta e="T496" id="Seg_10516" s="T495">печь-LAT</ta>
            <ta e="T497" id="Seg_10517" s="T496">влезать-PST.[3SG]</ta>
            <ta e="T498" id="Seg_10518" s="T497">лежать-DUR.[3SG]</ta>
            <ta e="T499" id="Seg_10519" s="T498">тогда</ta>
            <ta e="T500" id="Seg_10520" s="T499">брат-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T501" id="Seg_10521" s="T500">прийти-PST-3PL</ta>
            <ta e="T502" id="Seg_10522" s="T501">ну</ta>
            <ta e="T503" id="Seg_10523" s="T502">какой=INDEF</ta>
            <ta e="T504" id="Seg_10524" s="T503">мальчик.[NOM.SG]</ta>
            <ta e="T505" id="Seg_10525" s="T504">мальчик.[NOM.SG]</ta>
            <ta e="T506" id="Seg_10526" s="T505">красивый.[NOM.SG]</ta>
            <ta e="T507" id="Seg_10527" s="T506">лошадь-NOM/GEN.3SG</ta>
            <ta e="T508" id="Seg_10528" s="T507">красивый.[NOM.SG]</ta>
            <ta e="T509" id="Seg_10529" s="T508">целовать-PST.[3SG]</ta>
            <ta e="T510" id="Seg_10530" s="T509">царский</ta>
            <ta e="T511" id="Seg_10531" s="T510">девушка.[NOM.SG]</ta>
            <ta e="T512" id="Seg_10532" s="T511">вождь.[NOM.SG]</ta>
            <ta e="T513" id="Seg_10533" s="T512">вождь.[NOM.SG]</ta>
            <ta e="T514" id="Seg_10534" s="T513">девушка.[NOM.SG]</ta>
            <ta e="T515" id="Seg_10535" s="T514">и</ta>
            <ta e="T516" id="Seg_10536" s="T515">колечко-ACC.3SG</ta>
            <ta e="T517" id="Seg_10537" s="T516">взять-PST.[3SG]</ta>
            <ta e="T518" id="Seg_10538" s="T517">пойти-CVB</ta>
            <ta e="T519" id="Seg_10539" s="T518">исчезнуть-PST.[3SG]</ta>
            <ta e="T520" id="Seg_10540" s="T519">тогда</ta>
            <ta e="T521" id="Seg_10541" s="T520">опять</ta>
            <ta e="T522" id="Seg_10542" s="T521">этот.[NOM.SG]</ta>
            <ta e="T523" id="Seg_10543" s="T522">вождь.[NOM.SG]</ta>
            <ta e="T524" id="Seg_10544" s="T523">делать-PST.[3SG]</ta>
            <ta e="T525" id="Seg_10545" s="T524">много</ta>
            <ta e="T526" id="Seg_10546" s="T525">мясо.[NOM.SG]</ta>
            <ta e="T527" id="Seg_10547" s="T526">яйцо-PL</ta>
            <ta e="T528" id="Seg_10548" s="T527">класть-PST.[3SG]</ta>
            <ta e="T529" id="Seg_10549" s="T528">весь</ta>
            <ta e="T530" id="Seg_10550" s="T529">что.[NOM.SG]</ta>
            <ta e="T531" id="Seg_10551" s="T530">там</ta>
            <ta e="T532" id="Seg_10552" s="T531">быть-PRS.[3SG]</ta>
            <ta e="T533" id="Seg_10553" s="T532">класть-PST.[3SG]</ta>
            <ta e="T534" id="Seg_10554" s="T533">тогда</ta>
            <ta e="T535" id="Seg_10555" s="T534">прийти-IMP.2PL</ta>
            <ta e="T536" id="Seg_10556" s="T535">весь-ABL</ta>
            <ta e="T537" id="Seg_10557" s="T536">кто.[NOM.SG]</ta>
            <ta e="T538" id="Seg_10558" s="T537">NEG</ta>
            <ta e="T539" id="Seg_10559" s="T538">прийти-FUT-3SG</ta>
            <ta e="T540" id="Seg_10560" s="T539">голова-ACC.3SG</ta>
            <ta e="T542" id="Seg_10561" s="T541">от-резать-FUT-1SG</ta>
            <ta e="T543" id="Seg_10562" s="T542">тогда</ta>
            <ta e="T544" id="Seg_10563" s="T543">этот-PL</ta>
            <ta e="T546" id="Seg_10564" s="T545">там</ta>
            <ta e="T547" id="Seg_10565" s="T546">отец-NOM/GEN.3SG</ta>
            <ta e="T548" id="Seg_10566" s="T547">весь-ABL</ta>
            <ta e="T549" id="Seg_10567" s="T548">пойти-CVB</ta>
            <ta e="T550" id="Seg_10568" s="T549">исчезнуть-PST-3PL</ta>
            <ta e="T551" id="Seg_10569" s="T550">прийти-PST-3PL</ta>
            <ta e="T552" id="Seg_10570" s="T551">этот-PL</ta>
            <ta e="T553" id="Seg_10571" s="T552">сесть-PST-3PL</ta>
            <ta e="T555" id="Seg_10572" s="T554">PTCL</ta>
            <ta e="T556" id="Seg_10573" s="T555">пить-DUR-3PL</ta>
            <ta e="T558" id="Seg_10574" s="T557">водка.[NOM.SG]</ta>
            <ta e="T559" id="Seg_10575" s="T558">дать-DUR-3PL</ta>
            <ta e="T560" id="Seg_10576" s="T559">тогда</ta>
            <ta e="T561" id="Seg_10577" s="T560">прийти-PST.[3SG]</ta>
            <ta e="T562" id="Seg_10578" s="T561">этот.[NOM.SG]</ta>
            <ta e="T563" id="Seg_10579" s="T562">последний</ta>
            <ta e="T564" id="Seg_10580" s="T563">сын-NOM/GEN.3SG</ta>
            <ta e="T565" id="Seg_10581" s="T564">этот.[NOM.SG]</ta>
            <ta e="T566" id="Seg_10582" s="T565">этот.[NOM.SG]</ta>
            <ta e="T567" id="Seg_10583" s="T566">PTCL</ta>
            <ta e="T568" id="Seg_10584" s="T567">черный.[NOM.SG]</ta>
            <ta e="T569" id="Seg_10585" s="T568">сидеть-DUR.[3SG]</ta>
            <ta e="T570" id="Seg_10586" s="T569">и</ta>
            <ta e="T571" id="Seg_10587" s="T570">рука-NOM/GEN.3SG</ta>
            <ta e="T572" id="Seg_10588" s="T571">черный.[NOM.SG]</ta>
            <ta e="T573" id="Seg_10589" s="T572">тряпка-INS</ta>
            <ta e="T574" id="Seg_10590" s="T573">завязать-DETR-PTCP.[NOM.SG]</ta>
            <ta e="T575" id="Seg_10591" s="T574">сказать-IPFVZ.[3SG]</ta>
            <ta e="T576" id="Seg_10592" s="T575">что.[NOM.SG]</ta>
            <ta e="T577" id="Seg_10593" s="T576">ты.GEN</ta>
            <ta e="T578" id="Seg_10594" s="T577">рука-LAT/LOC.3SG</ta>
            <ta e="T580" id="Seg_10595" s="T579">видеть-PRS-3SG.O</ta>
            <ta e="T581" id="Seg_10596" s="T580">PTCL</ta>
            <ta e="T582" id="Seg_10597" s="T581">PTCL</ta>
            <ta e="T583" id="Seg_10598" s="T582">там</ta>
            <ta e="T584" id="Seg_10599" s="T583">колечко.[NOM.SG]</ta>
            <ta e="T585" id="Seg_10600" s="T584">сидеть-DUR.[3SG]</ta>
            <ta e="T586" id="Seg_10601" s="T585">тогда</ta>
            <ta e="T587" id="Seg_10602" s="T586">взять-FUT-3SG.O</ta>
            <ta e="T588" id="Seg_10603" s="T587">отец-LAT/LOC.3SG</ta>
            <ta e="T589" id="Seg_10604" s="T588">прийти-PRS-3SG.O </ta>
            <ta e="T590" id="Seg_10605" s="T589">тот.[NOM.SG]</ta>
            <ta e="T592" id="Seg_10606" s="T591">тот.[NOM.SG]</ta>
            <ta e="T593" id="Seg_10607" s="T592">я.GEN</ta>
            <ta e="T594" id="Seg_10608" s="T593">жених-NOM/GEN/ACC.1SG</ta>
            <ta e="T595" id="Seg_10609" s="T594">этот-ACC</ta>
            <ta e="T596" id="Seg_10610" s="T595">этот.[NOM.SG]</ta>
            <ta e="T598" id="Seg_10611" s="T597">этот-PL</ta>
            <ta e="T599" id="Seg_10612" s="T598">там</ta>
            <ta e="T600" id="Seg_10613" s="T599">мыть-PST-3PL</ta>
            <ta e="T601" id="Seg_10614" s="T600">красивый.[NOM.SG]</ta>
            <ta e="T602" id="Seg_10615" s="T601">одежда.[NOM.SG]</ta>
            <ta e="T604" id="Seg_10616" s="T603">надеть-PST-3PL</ta>
            <ta e="T605" id="Seg_10617" s="T604">очень</ta>
            <ta e="T606" id="Seg_10618" s="T605">красивый.[NOM.SG]</ta>
            <ta e="T607" id="Seg_10619" s="T606">стать-RES-PST.[3SG]</ta>
            <ta e="T608" id="Seg_10620" s="T607">тогда</ta>
            <ta e="T609" id="Seg_10621" s="T608">опять</ta>
            <ta e="T610" id="Seg_10622" s="T609">INCH</ta>
            <ta e="T611" id="Seg_10623" s="T610">водка.[NOM.SG]</ta>
            <ta e="T612" id="Seg_10624" s="T611">пить-INF.LAT</ta>
            <ta e="T613" id="Seg_10625" s="T612">играть-INF.LAT</ta>
            <ta e="T614" id="Seg_10626" s="T613">брат-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T615" id="Seg_10627" s="T614">PTCL</ta>
            <ta e="T616" id="Seg_10628" s="T615">смотреть-DUR-3PL</ta>
            <ta e="T617" id="Seg_10629" s="T616">какой</ta>
            <ta e="T618" id="Seg_10630" s="T617">красивый.[NOM.SG]</ta>
            <ta e="T619" id="Seg_10631" s="T618">стать-RES-PST.[3SG]</ta>
            <ta e="T620" id="Seg_10632" s="T619">и</ta>
            <ta e="T621" id="Seg_10633" s="T620">там</ta>
            <ta e="T622" id="Seg_10634" s="T621">я.NOM</ta>
            <ta e="T623" id="Seg_10635" s="T622">быть-PST-1SG</ta>
            <ta e="T624" id="Seg_10636" s="T623">водка.[NOM.SG]</ta>
            <ta e="T625" id="Seg_10637" s="T624">PTCL</ta>
            <ta e="T626" id="Seg_10638" s="T625">течь-DUR.[3SG]</ta>
            <ta e="T627" id="Seg_10639" s="T626">дать-PST-3PL</ta>
            <ta e="T628" id="Seg_10640" s="T627">я.LAT</ta>
            <ta e="T629" id="Seg_10641" s="T628">блин.[NOM.SG]</ta>
            <ta e="T634" id="Seg_10642" s="T633">сидеть-PST-3PL</ta>
            <ta e="T635" id="Seg_10643" s="T634">два-COLL</ta>
            <ta e="T636" id="Seg_10644" s="T635">кум.[NOM.SG]</ta>
            <ta e="T637" id="Seg_10645" s="T636">и</ta>
            <ta e="T638" id="Seg_10646" s="T637">кума.[NOM.SG]</ta>
            <ta e="T639" id="Seg_10647" s="T638">волк.[NOM.SG]</ta>
            <ta e="T640" id="Seg_10648" s="T639">и</ta>
            <ta e="T641" id="Seg_10649" s="T640">лиса.[NOM.SG]</ta>
            <ta e="T642" id="Seg_10650" s="T641">тогда</ta>
            <ta e="T643" id="Seg_10651" s="T642">спать-INF.LAT</ta>
            <ta e="T644" id="Seg_10652" s="T643">лежать.[PRS]-3PL</ta>
            <ta e="T645" id="Seg_10653" s="T644">а</ta>
            <ta e="T646" id="Seg_10654" s="T645">лиса.[NOM.SG]</ta>
            <ta e="T647" id="Seg_10655" s="T646">всегда</ta>
            <ta e="T648" id="Seg_10656" s="T647">греметь-DUR.[3SG]</ta>
            <ta e="T649" id="Seg_10657" s="T648">греметь-DUR.[3SG]</ta>
            <ta e="T650" id="Seg_10658" s="T649">а</ta>
            <ta e="T651" id="Seg_10659" s="T650">волк.[NOM.SG]</ta>
            <ta e="T652" id="Seg_10660" s="T651">сказать-IPFVZ.[3SG]</ta>
            <ta e="T653" id="Seg_10661" s="T652">кто.[NOM.SG]=INDEF</ta>
            <ta e="T654" id="Seg_10662" s="T653">греметь-DUR.[3SG]</ta>
            <ta e="T655" id="Seg_10663" s="T654">я.ACC</ta>
            <ta e="T656" id="Seg_10664" s="T655">позвать-PRS-3PL</ta>
            <ta e="T657" id="Seg_10665" s="T656">ребенок.[NOM.SG]</ta>
            <ta e="T658" id="Seg_10666" s="T657">ребенок.[NOM.SG]</ta>
            <ta e="T659" id="Seg_10667" s="T658">кормить-INF.LAT</ta>
            <ta e="T660" id="Seg_10668" s="T659">тогда</ta>
            <ta e="T661" id="Seg_10669" s="T660">ну</ta>
            <ta e="T662" id="Seg_10670" s="T661">пойти-EP-IMP.2SG</ta>
            <ta e="T663" id="Seg_10671" s="T662">этот.[NOM.SG]</ta>
            <ta e="T664" id="Seg_10672" s="T663">пойти-PST.[3SG]</ta>
            <ta e="T665" id="Seg_10673" s="T664">а</ta>
            <ta e="T666" id="Seg_10674" s="T665">этот-PL</ta>
            <ta e="T667" id="Seg_10675" s="T666">стоять-PST.[3SG]</ta>
            <ta e="T668" id="Seg_10676" s="T667">мед.[NOM.SG]</ta>
            <ta e="T669" id="Seg_10677" s="T668">съесть-PST.[3SG]</ta>
            <ta e="T670" id="Seg_10678" s="T669">съесть-PST.[3SG]</ta>
            <ta e="T671" id="Seg_10679" s="T670">мед.[NOM.SG]</ta>
            <ta e="T672" id="Seg_10680" s="T671">этот.[NOM.SG]</ta>
            <ta e="T673" id="Seg_10681" s="T672">прийти-PST.[3SG]</ta>
            <ta e="T674" id="Seg_10682" s="T673">кто.[NOM.SG]</ta>
            <ta e="T675" id="Seg_10683" s="T674">там</ta>
            <ta e="T676" id="Seg_10684" s="T675">быть-PRS.[3SG]</ta>
            <ta e="T677" id="Seg_10685" s="T676">вершок.[NOM.SG]</ta>
            <ta e="T678" id="Seg_10686" s="T677">быть-PRS.[3SG]</ta>
            <ta e="T679" id="Seg_10687" s="T678">тогда</ta>
            <ta e="T680" id="Seg_10688" s="T679">опять</ta>
            <ta e="T681" id="Seg_10689" s="T680">вечер-LOC.ADV</ta>
            <ta e="T682" id="Seg_10690" s="T681">опять</ta>
            <ta e="T683" id="Seg_10691" s="T682">греметь-DUR.[3SG]</ta>
            <ta e="T684" id="Seg_10692" s="T683">лисица.[NOM.SG]</ta>
            <ta e="T685" id="Seg_10693" s="T684">кто.[NOM.SG]</ta>
            <ta e="T686" id="Seg_10694" s="T685">греметь-DUR.[3SG]</ta>
            <ta e="T687" id="Seg_10695" s="T686">и</ta>
            <ta e="T688" id="Seg_10696" s="T687">я.ACC</ta>
            <ta e="T689" id="Seg_10697" s="T688">позвать-DUR-3PL</ta>
            <ta e="T690" id="Seg_10698" s="T689">ребенок.[NOM.SG]</ta>
            <ta e="T691" id="Seg_10699" s="T690">развязать-INF.LAT</ta>
            <ta e="T692" id="Seg_10700" s="T691">ну</ta>
            <ta e="T693" id="Seg_10701" s="T692">пойти-EP-IMP.2SG</ta>
            <ta e="T694" id="Seg_10702" s="T693">этот.[NOM.SG]</ta>
            <ta e="T695" id="Seg_10703" s="T694">пойти-PST.[3SG]</ta>
            <ta e="T696" id="Seg_10704" s="T695">съесть-PST.[3SG]</ta>
            <ta e="T697" id="Seg_10705" s="T696">съесть-PST.[3SG]</ta>
            <ta e="T698" id="Seg_10706" s="T697">тогда</ta>
            <ta e="T699" id="Seg_10707" s="T698">прийти-PST.[3SG]</ta>
            <ta e="T700" id="Seg_10708" s="T699">кто.[NOM.SG]</ta>
            <ta e="T701" id="Seg_10709" s="T700">там</ta>
            <ta e="T702" id="Seg_10710" s="T701">прийти-PST.[3SG]</ta>
            <ta e="T703" id="Seg_10711" s="T702">середушек.[NOM.SG]</ta>
            <ta e="T704" id="Seg_10712" s="T703">прийти-PST.[3SG]</ta>
            <ta e="T705" id="Seg_10713" s="T704">тогда</ta>
            <ta e="T706" id="Seg_10714" s="T705">три-ORD.[NOM.SG]</ta>
            <ta e="T707" id="Seg_10715" s="T706">вечер-LOC.ADV</ta>
            <ta e="T708" id="Seg_10716" s="T707">опять</ta>
            <ta e="T709" id="Seg_10717" s="T708">греметь-DUR.[3SG]</ta>
            <ta e="T710" id="Seg_10718" s="T709">тогда</ta>
            <ta e="T711" id="Seg_10719" s="T710">кто.[NOM.SG]=INDEF</ta>
            <ta e="T712" id="Seg_10720" s="T711">греметь-DUR.[3SG]</ta>
            <ta e="T713" id="Seg_10721" s="T712">и</ta>
            <ta e="T714" id="Seg_10722" s="T713">я.ACC</ta>
            <ta e="T715" id="Seg_10723" s="T714">позвать-DUR-3PL</ta>
            <ta e="T716" id="Seg_10724" s="T715">ну</ta>
            <ta e="T717" id="Seg_10725" s="T716">пойти-EP-IMP.2SG</ta>
            <ta e="T718" id="Seg_10726" s="T717">тогда</ta>
            <ta e="T719" id="Seg_10727" s="T718">этот.[NOM.SG]</ta>
            <ta e="T720" id="Seg_10728" s="T719">съесть-PST.[3SG]</ta>
            <ta e="T721" id="Seg_10729" s="T720">PTCL</ta>
            <ta e="T722" id="Seg_10730" s="T721">прийти-PST.[3SG]</ta>
            <ta e="T723" id="Seg_10731" s="T722">кто.[NOM.SG]</ta>
            <ta e="T724" id="Seg_10732" s="T723">там</ta>
            <ta e="T725" id="Seg_10733" s="T724">этот.[NOM.SG]</ta>
            <ta e="T726" id="Seg_10734" s="T725">скребушек.[NOM.SG]</ta>
            <ta e="T727" id="Seg_10735" s="T726">прийти-PST.[3SG]</ta>
            <ta e="T728" id="Seg_10736" s="T727">тогда</ta>
            <ta e="T729" id="Seg_10737" s="T728">утро-LOC.ADV</ta>
            <ta e="T730" id="Seg_10738" s="T729">встать-PST-3PL</ta>
            <ta e="T731" id="Seg_10739" s="T730">этот.[NOM.SG]</ta>
            <ta e="T732" id="Seg_10740" s="T731">болеть-MOM-PST.[3SG]</ta>
            <ta e="T733" id="Seg_10741" s="T732">сказать-IPFVZ.[3SG]</ta>
            <ta e="T734" id="Seg_10742" s="T733">принести-IMP.2SG</ta>
            <ta e="T735" id="Seg_10743" s="T734">я.LAT</ta>
            <ta e="T736" id="Seg_10744" s="T735">сладкий-PL</ta>
            <ta e="T737" id="Seg_10745" s="T736">этот.[NOM.SG]</ta>
            <ta e="T738" id="Seg_10746" s="T737">пойти-PST.[3SG]</ta>
            <ta e="T739" id="Seg_10747" s="T738">и</ta>
            <ta e="T740" id="Seg_10748" s="T739">NEG.EX.[3SG]</ta>
            <ta e="T741" id="Seg_10749" s="T740">кто.[NOM.SG]=INDEF</ta>
            <ta e="T742" id="Seg_10750" s="T741">съесть-MOM-PST.[3SG]</ta>
            <ta e="T743" id="Seg_10751" s="T742">и</ta>
            <ta e="T744" id="Seg_10752" s="T743">ты.NOM</ta>
            <ta e="T745" id="Seg_10753" s="T744">съесть-PST-2SG</ta>
            <ta e="T746" id="Seg_10754" s="T745">этот.[NOM.SG]</ta>
            <ta e="T747" id="Seg_10755" s="T746">PTCL</ta>
            <ta e="T748" id="Seg_10756" s="T747">NEG</ta>
            <ta e="T749" id="Seg_10757" s="T748">я.NOM</ta>
            <ta e="T750" id="Seg_10758" s="T749">NEG</ta>
            <ta e="T751" id="Seg_10759" s="T750">съесть-PST-1SG</ta>
            <ta e="T752" id="Seg_10760" s="T751">тогда</ta>
            <ta e="T753" id="Seg_10761" s="T752">HORT</ta>
            <ta e="T754" id="Seg_10762" s="T753">солнце-LAT</ta>
            <ta e="T755" id="Seg_10763" s="T754">лежать-1PL</ta>
            <ta e="T756" id="Seg_10764" s="T755">кто.[NOM.SG]</ta>
            <ta e="T757" id="Seg_10765" s="T756">съесть-PST.[3SG]</ta>
            <ta e="T758" id="Seg_10766" s="T757">там</ta>
            <ta e="T759" id="Seg_10767" s="T758">сладкий.[NOM.SG]</ta>
            <ta e="T760" id="Seg_10768" s="T759">живот-NOM/GEN.3SG</ta>
            <ta e="T761" id="Seg_10769" s="T760">стать-FUT-3SG</ta>
            <ta e="T762" id="Seg_10770" s="T761">тогда</ta>
            <ta e="T763" id="Seg_10771" s="T762">видеть-PRS-3SG.O</ta>
            <ta e="T764" id="Seg_10772" s="T763">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T765" id="Seg_10773" s="T764">живот-LAT/LOC.3SG</ta>
            <ta e="T766" id="Seg_10774" s="T765">сладкий.[NOM.SG]</ta>
            <ta e="T767" id="Seg_10775" s="T766">быть-PRS.[3SG]</ta>
            <ta e="T768" id="Seg_10776" s="T767">этот.[NOM.SG]</ta>
            <ta e="T769" id="Seg_10777" s="T768">INCH</ta>
            <ta e="T770" id="Seg_10778" s="T769">этот.[NOM.SG]</ta>
            <ta e="T771" id="Seg_10779" s="T770">волк-NOM/GEN/ACC.3SG</ta>
            <ta e="T772" id="Seg_10780" s="T771">мазать-INF.LAT</ta>
            <ta e="T773" id="Seg_10781" s="T772">встать-IMP.2SG</ta>
            <ta e="T774" id="Seg_10782" s="T773">ты.NOM</ta>
            <ta e="T776" id="Seg_10783" s="T775">съесть-PST-2SG</ta>
            <ta e="T777" id="Seg_10784" s="T776">тогда</ta>
            <ta e="T778" id="Seg_10785" s="T777">этот.[NOM.SG]</ta>
            <ta e="T779" id="Seg_10786" s="T778">встать-PST.[3SG]</ta>
            <ta e="T780" id="Seg_10787" s="T779">ну</ta>
            <ta e="T781" id="Seg_10788" s="T780">я.NOM</ta>
            <ta e="T782" id="Seg_10789" s="T781">съесть-PST-1SG</ta>
            <ta e="T783" id="Seg_10790" s="T782">тогда</ta>
            <ta e="T784" id="Seg_10791" s="T783">весь</ta>
            <ta e="T785" id="Seg_10792" s="T784">птица.[NOM.SG]</ta>
            <ta e="T786" id="Seg_10793" s="T785">и</ta>
            <ta e="T787" id="Seg_10794" s="T786">тетерев.[NOM.SG]</ta>
            <ta e="T788" id="Seg_10795" s="T787">жить-3PL</ta>
            <ta e="T789" id="Seg_10796" s="T788">как</ta>
            <ta e="T790" id="Seg_10797" s="T789">дом.[NOM.SG]</ta>
            <ta e="T791" id="Seg_10798" s="T790">резать-INF.LAT</ta>
            <ta e="T792" id="Seg_10799" s="T791">топор.[NOM.SG]</ta>
            <ta e="T793" id="Seg_10800" s="T792">NEG.EX.[3SG]</ta>
            <ta e="T794" id="Seg_10801" s="T793">кто.[NOM.SG]</ta>
            <ta e="T795" id="Seg_10802" s="T794">топор.[NOM.SG]</ta>
            <ta e="T796" id="Seg_10803" s="T795">делать-FUT-3SG</ta>
            <ta e="T797" id="Seg_10804" s="T796">и</ta>
            <ta e="T798" id="Seg_10805" s="T797">зачем</ta>
            <ta e="T799" id="Seg_10806" s="T798">я.LAT</ta>
            <ta e="T801" id="Seg_10807" s="T800">дом.[NOM.SG]</ta>
            <ta e="T802" id="Seg_10808" s="T801">я.NOM</ta>
            <ta e="T803" id="Seg_10809" s="T802">снег-LOC</ta>
            <ta e="T804" id="Seg_10810" s="T803">ночевать-FUT-1SG</ta>
            <ta e="T805" id="Seg_10811" s="T804">и</ta>
            <ta e="T806" id="Seg_10812" s="T805">упасть-MOM-PST.[3SG]</ta>
            <ta e="T808" id="Seg_10813" s="T807">снег-LAT</ta>
            <ta e="T809" id="Seg_10814" s="T808">там</ta>
            <ta e="T810" id="Seg_10815" s="T809">ночевать-PST.[3SG]</ta>
            <ta e="T811" id="Seg_10816" s="T810">утро-LOC.ADV</ta>
            <ta e="T812" id="Seg_10817" s="T811">встать-PST.[3SG]</ta>
            <ta e="T813" id="Seg_10818" s="T812">и</ta>
            <ta e="T814" id="Seg_10819" s="T813">пойти-PST.[3SG]</ta>
            <ta e="T815" id="Seg_10820" s="T814">видеть-INF.LAT</ta>
            <ta e="T816" id="Seg_10821" s="T815">товарищ-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T817" id="Seg_10822" s="T816">видеть-PST.[3SG]</ta>
            <ta e="T818" id="Seg_10823" s="T817">там</ta>
            <ta e="T819" id="Seg_10824" s="T818">играть-PST-3PL</ta>
            <ta e="T820" id="Seg_10825" s="T819">прыгнуть-MOM-PST-3PL</ta>
            <ta e="T822" id="Seg_10826" s="T821">дерево-3PL-LAT</ta>
            <ta e="T823" id="Seg_10827" s="T822">тогда</ta>
            <ta e="T824" id="Seg_10828" s="T823">сам-LAT/LOC.3SG</ta>
            <ta e="T825" id="Seg_10829" s="T824">гнездо-NOM/GEN/ACC.3PL</ta>
            <ta e="T826" id="Seg_10830" s="T825">делать-PST-3PL</ta>
            <ta e="T827" id="Seg_10831" s="T826">яйцо-PL</ta>
            <ta e="T828" id="Seg_10832" s="T827">принести-PST-3PL</ta>
            <ta e="T829" id="Seg_10833" s="T828">ребенок-PL-LAT</ta>
            <ta e="T830" id="Seg_10834" s="T829">стать-PST-3PL</ta>
            <ta e="T831" id="Seg_10835" s="T830">тогда</ta>
            <ta e="T832" id="Seg_10836" s="T831">ребенок-PL-LAT</ta>
            <ta e="T833" id="Seg_10837" s="T832">кормить-PST-3PL</ta>
            <ta e="T834" id="Seg_10838" s="T833">мошка-INS</ta>
            <ta e="T835" id="Seg_10839" s="T834">червь-3PL-INS</ta>
            <ta e="T836" id="Seg_10840" s="T835">тогда</ta>
            <ta e="T837" id="Seg_10841" s="T836">опять</ta>
            <ta e="T838" id="Seg_10842" s="T837">зачем</ta>
            <ta e="T839" id="Seg_10843" s="T838">мы.LAT</ta>
            <ta e="T840" id="Seg_10844" s="T839">дом.[NOM.SG]</ta>
            <ta e="T841" id="Seg_10845" s="T840">снег-LOC</ta>
            <ta e="T842" id="Seg_10846" s="T841">спать-PST-1PL</ta>
            <ta e="T843" id="Seg_10847" s="T842">один.[NOM.SG]</ta>
            <ta e="T844" id="Seg_10848" s="T843">вечер</ta>
            <ta e="T845" id="Seg_10849" s="T844">дом-LOC</ta>
            <ta e="T846" id="Seg_10850" s="T845">сажать-1PL</ta>
            <ta e="T847" id="Seg_10851" s="T846">этот.[NOM.SG]</ta>
            <ta e="T848" id="Seg_10852" s="T847">весь</ta>
            <ta e="T849" id="Seg_10853" s="T848">везде</ta>
            <ta e="T850" id="Seg_10854" s="T849">смотреть-FRQ-DUR-1PL</ta>
            <ta e="T851" id="Seg_10855" s="T850">хватит</ta>
            <ta e="T852" id="Seg_10856" s="T851">жить-PST-3PL</ta>
            <ta e="T853" id="Seg_10857" s="T852">женщина</ta>
            <ta e="T854" id="Seg_10858" s="T853">и</ta>
            <ta e="T855" id="Seg_10859" s="T854">мужчина.[NOM.SG]</ta>
            <ta e="T857" id="Seg_10860" s="T856">этот-PL</ta>
            <ta e="T858" id="Seg_10861" s="T857">что.[NOM.SG]=INDEF</ta>
            <ta e="T859" id="Seg_10862" s="T858">NEG.EX-PST.[3SG]</ta>
            <ta e="T860" id="Seg_10863" s="T859">пойти-PST-3PL</ta>
            <ta e="T861" id="Seg_10864" s="T860">лес-LAT</ta>
            <ta e="T862" id="Seg_10865" s="T861">шишка-PL</ta>
            <ta e="T863" id="Seg_10866" s="T862">желуди.[NOM.SG]</ta>
            <ta e="T864" id="Seg_10867" s="T863">собрать-PST-3PL</ta>
            <ta e="T865" id="Seg_10868" s="T864">прийти-PST-3PL</ta>
            <ta e="T866" id="Seg_10869" s="T865">съесть-DUR.[3SG]</ta>
            <ta e="T867" id="Seg_10870" s="T866">PTCL</ta>
            <ta e="T868" id="Seg_10871" s="T867">один.[NOM.SG]</ta>
            <ta e="T869" id="Seg_10872" s="T868">упасть-MOM-PST.[3SG]</ta>
            <ta e="T870" id="Seg_10873" s="T869">подвал-LOC</ta>
            <ta e="T871" id="Seg_10874" s="T870">тогда</ta>
            <ta e="T872" id="Seg_10875" s="T871">INCH</ta>
            <ta e="T873" id="Seg_10876" s="T872">расти-INF.LAT</ta>
            <ta e="T875" id="Seg_10877" s="T874">женщина.[NOM.SG]</ta>
            <ta e="T876" id="Seg_10878" s="T875">сказать-IPFVZ.[3SG]</ta>
            <ta e="T877" id="Seg_10879" s="T876">топор-INS</ta>
            <ta e="T878" id="Seg_10880" s="T877">резать-IMP.2SG.O</ta>
            <ta e="T879" id="Seg_10881" s="T878">пол-NOM/GEN/ACC.3SG</ta>
            <ta e="T880" id="Seg_10882" s="T879">этот.[NOM.SG]</ta>
            <ta e="T881" id="Seg_10883" s="T880">резать-PST.[3SG]</ta>
            <ta e="T882" id="Seg_10884" s="T881">этот.[NOM.SG]</ta>
            <ta e="T883" id="Seg_10885" s="T882">здесь</ta>
            <ta e="T884" id="Seg_10886" s="T883">расти-PST.[3SG]</ta>
            <ta e="T885" id="Seg_10887" s="T884">сейчас</ta>
            <ta e="T886" id="Seg_10888" s="T885">там</ta>
            <ta e="T887" id="Seg_10889" s="T886">вверх</ta>
            <ta e="T888" id="Seg_10890" s="T887">PTCL</ta>
            <ta e="T889" id="Seg_10891" s="T888">собрать</ta>
            <ta e="T890" id="Seg_10892" s="T889">этот.[NOM.SG]</ta>
            <ta e="T891" id="Seg_10893" s="T890">там</ta>
            <ta e="T892" id="Seg_10894" s="T891">крыша-ACC.3SG</ta>
            <ta e="T894" id="Seg_10895" s="T893">там</ta>
            <ta e="T895" id="Seg_10896" s="T894">далеко-LAT.ADV</ta>
            <ta e="T896" id="Seg_10897" s="T895">расти-PST.[3SG]</ta>
            <ta e="T897" id="Seg_10898" s="T896">даже</ta>
            <ta e="T898" id="Seg_10899" s="T897">вверх</ta>
            <ta e="T899" id="Seg_10900" s="T898">тогда</ta>
            <ta e="T900" id="Seg_10901" s="T899">мужчина.[NOM.SG]</ta>
            <ta e="T901" id="Seg_10902" s="T900">надо</ta>
            <ta e="T902" id="Seg_10903" s="T901">пойти-INF.LAT</ta>
            <ta e="T903" id="Seg_10904" s="T902">собирать-INF.LAT</ta>
            <ta e="T904" id="Seg_10905" s="T903">желуди.[NOM.SG]</ta>
            <ta e="T905" id="Seg_10906" s="T904">влезать-PST.[3SG]</ta>
            <ta e="T906" id="Seg_10907" s="T905">влезать-PST.[3SG]</ta>
            <ta e="T908" id="Seg_10908" s="T907">там</ta>
            <ta e="T909" id="Seg_10909" s="T908">вверх</ta>
            <ta e="T910" id="Seg_10910" s="T909">там</ta>
            <ta e="T911" id="Seg_10911" s="T910">видеть-DUR.[3SG]</ta>
            <ta e="T912" id="Seg_10912" s="T911">дом.[NOM.SG]</ta>
            <ta e="T914" id="Seg_10913" s="T913">стоять-PRS.[3SG]</ta>
            <ta e="T915" id="Seg_10914" s="T914">там</ta>
            <ta e="T916" id="Seg_10915" s="T915">дом-LAT</ta>
            <ta e="T917" id="Seg_10916" s="T916">прийти-PST.[3SG]</ta>
            <ta e="T918" id="Seg_10917" s="T917">там</ta>
            <ta e="T919" id="Seg_10918" s="T918">курица-GEN</ta>
            <ta e="T920" id="Seg_10919" s="T919">человек.[NOM.SG]</ta>
            <ta e="T921" id="Seg_10920" s="T920">жить-DUR.[3SG]</ta>
            <ta e="T922" id="Seg_10921" s="T921">голова-NOM/GEN.3SG</ta>
            <ta e="T923" id="Seg_10922" s="T922">красивый.[NOM.SG]</ta>
            <ta e="T924" id="Seg_10923" s="T923">красный.[NOM.SG]</ta>
            <ta e="T925" id="Seg_10924" s="T924">весь</ta>
            <ta e="T926" id="Seg_10925" s="T925">и</ta>
            <ta e="T927" id="Seg_10926" s="T926">там</ta>
            <ta e="T928" id="Seg_10927" s="T927">жернов-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T929" id="Seg_10928" s="T928">быть-PRS.[3SG]</ta>
            <ta e="T930" id="Seg_10929" s="T929">этот.[NOM.SG]</ta>
            <ta e="T931" id="Seg_10930" s="T930">взять-PST.[3SG]</ta>
            <ta e="T932" id="Seg_10931" s="T931">этот-ACC</ta>
            <ta e="T933" id="Seg_10932" s="T932">и</ta>
            <ta e="T934" id="Seg_10933" s="T933">прийти-PST.[3SG]</ta>
            <ta e="T935" id="Seg_10934" s="T934">возьми</ta>
            <ta e="T936" id="Seg_10935" s="T935">ты.DAT</ta>
            <ta e="T937" id="Seg_10936" s="T936">курица-GEN</ta>
            <ta e="T938" id="Seg_10937" s="T937">человек.[NOM.SG]</ta>
            <ta e="T939" id="Seg_10938" s="T938">красный.[NOM.SG]</ta>
            <ta e="T940" id="Seg_10939" s="T939">голова-NOM/GEN.3SG</ta>
            <ta e="T941" id="Seg_10940" s="T940">этот.[NOM.SG]</ta>
            <ta e="T942" id="Seg_10941" s="T941">взять-PST.[3SG]</ta>
            <ta e="T943" id="Seg_10942" s="T942">и</ta>
            <ta e="T944" id="Seg_10943" s="T943">INCH</ta>
            <ta e="T945" id="Seg_10944" s="T944">жернов</ta>
            <ta e="T946" id="Seg_10945" s="T945">этот.[NOM.SG]</ta>
            <ta e="T947" id="Seg_10946" s="T946">мельница-NOM/GEN/ACC.3SG</ta>
            <ta e="T948" id="Seg_10947" s="T947">плести-INF.LAT</ta>
            <ta e="T949" id="Seg_10948" s="T948">там</ta>
            <ta e="T950" id="Seg_10949" s="T949">блин-PL</ta>
            <ta e="T951" id="Seg_10950" s="T950">пирог-PL</ta>
            <ta e="T952" id="Seg_10951" s="T951">блин-PL</ta>
            <ta e="T953" id="Seg_10952" s="T952">пирог-PL</ta>
            <ta e="T954" id="Seg_10953" s="T953">мужчина.[NOM.SG]</ta>
            <ta e="T955" id="Seg_10954" s="T954">съесть-PST.[3SG]</ta>
            <ta e="T956" id="Seg_10955" s="T955">и</ta>
            <ta e="T957" id="Seg_10956" s="T956">женщина.[NOM.SG]</ta>
            <ta e="T958" id="Seg_10957" s="T957">съесть-PST.[3SG]</ta>
            <ta e="T959" id="Seg_10958" s="T958">всегда</ta>
            <ta e="T960" id="Seg_10959" s="T959">вдоль</ta>
            <ta e="T961" id="Seg_10960" s="T960">жить-CVB</ta>
            <ta e="T962" id="Seg_10961" s="T961">тогда</ta>
            <ta e="T963" id="Seg_10962" s="T962">прийти-PST.[3SG]</ta>
            <ta e="T964" id="Seg_10963" s="T963">этот-PL-LAT</ta>
            <ta e="T965" id="Seg_10964" s="T964">барин.[NOM.SG]</ta>
            <ta e="T966" id="Seg_10965" s="T965">принести-IMP.2PL</ta>
            <ta e="T967" id="Seg_10966" s="T966">я.LAT</ta>
            <ta e="T968" id="Seg_10967" s="T967">съесть-INF.LAT</ta>
            <ta e="T969" id="Seg_10968" s="T968">этот.[NOM.SG]</ta>
            <ta e="T970" id="Seg_10969" s="T969">слышать-PST.[3SG]</ta>
            <ta e="T971" id="Seg_10970" s="T970">правда</ta>
            <ta e="T972" id="Seg_10971" s="T971">этот.[NOM.SG]</ta>
            <ta e="T974" id="Seg_10972" s="T973">этот.[NOM.SG]</ta>
            <ta e="T976" id="Seg_10973" s="T975">мельница.[NOM.SG]</ta>
            <ta e="T977" id="Seg_10974" s="T976">тогда</ta>
            <ta e="T978" id="Seg_10975" s="T977">женщина-NOM/GEN.3SG</ta>
            <ta e="T979" id="Seg_10976" s="T978">PTCL</ta>
            <ta e="T980" id="Seg_10977" s="T979">INCH</ta>
            <ta e="T981" id="Seg_10978" s="T980">мельница-NOM/GEN/ACC.3SG</ta>
            <ta e="T982" id="Seg_10979" s="T981">плести-INF.LAT</ta>
            <ta e="T983" id="Seg_10980" s="T982">блин-PL</ta>
            <ta e="T984" id="Seg_10981" s="T983">пирог-PL</ta>
            <ta e="T985" id="Seg_10982" s="T984">этот.[NOM.SG]</ta>
            <ta e="T986" id="Seg_10983" s="T985">съесть-PST.[3SG]</ta>
            <ta e="T987" id="Seg_10984" s="T986">продавать-IMP.2PL</ta>
            <ta e="T988" id="Seg_10985" s="T987">я.LAT</ta>
            <ta e="T989" id="Seg_10986" s="T988">нет</ta>
            <ta e="T990" id="Seg_10987" s="T989">мы.NOM</ta>
            <ta e="T991" id="Seg_10988" s="T990">NEG</ta>
            <ta e="T992" id="Seg_10989" s="T991">продавать-FUT-1SG</ta>
            <ta e="T993" id="Seg_10990" s="T992">а.то</ta>
            <ta e="T995" id="Seg_10991" s="T994">мы.LAT</ta>
            <ta e="T996" id="Seg_10992" s="T995">съесть-INF.LAT</ta>
            <ta e="T997" id="Seg_10993" s="T996">что.[NOM.SG]</ta>
            <ta e="T998" id="Seg_10994" s="T997">тогда</ta>
            <ta e="T999" id="Seg_10995" s="T998">этот.[NOM.SG]</ta>
            <ta e="T1000" id="Seg_10996" s="T999">вечер-LOC.ADV</ta>
            <ta e="T1001" id="Seg_10997" s="T1000">прийти-PST.[3SG]</ta>
            <ta e="T1002" id="Seg_10998" s="T1001">и</ta>
            <ta e="T1003" id="Seg_10999" s="T1002">украсть-CVB</ta>
            <ta e="T1004" id="Seg_11000" s="T1003">взять-MOM-PST.[3SG]</ta>
            <ta e="T1005" id="Seg_11001" s="T1004">и</ta>
            <ta e="T1006" id="Seg_11002" s="T1005">унести-RES-PST.[3SG]</ta>
            <ta e="T1007" id="Seg_11003" s="T1006">утро-GEN</ta>
            <ta e="T1008" id="Seg_11004" s="T1007">этот-PL</ta>
            <ta e="T1009" id="Seg_11005" s="T1008">встать-PST-3PL</ta>
            <ta e="T1010" id="Seg_11006" s="T1009">NEG.EX.[3SG]</ta>
            <ta e="T1011" id="Seg_11007" s="T1010">мельница-NOM/GEN/ACC.3SG</ta>
            <ta e="T1012" id="Seg_11008" s="T1011">тогда</ta>
            <ta e="T1013" id="Seg_11009" s="T1012">человек.[NOM.SG]</ta>
            <ta e="T1014" id="Seg_11010" s="T1013">курица.[NOM.SG]</ta>
            <ta e="T1015" id="Seg_11011" s="T1014">NEG.AUX-IMP.2SG</ta>
            <ta e="T1016" id="Seg_11012" s="T1015">плакать-CNG</ta>
            <ta e="T1017" id="Seg_11013" s="T1016">я.NOM</ta>
            <ta e="T1018" id="Seg_11014" s="T1017">пойти-FUT-1SG</ta>
            <ta e="T1020" id="Seg_11015" s="T1019">принести-FUT-1SG</ta>
            <ta e="T1021" id="Seg_11016" s="T1020">опять</ta>
            <ta e="T1022" id="Seg_11017" s="T1021">вы.LAT</ta>
            <ta e="T1023" id="Seg_11018" s="T1022">пойти-PST.[3SG]</ta>
            <ta e="T1024" id="Seg_11019" s="T1023">идти-PRS.[3SG]</ta>
            <ta e="T1025" id="Seg_11020" s="T1024">идти-PRS.[3SG]</ta>
            <ta e="T1026" id="Seg_11021" s="T1025">прийти-PRS.[3SG]</ta>
            <ta e="T1027" id="Seg_11022" s="T1026">этот-LAT</ta>
            <ta e="T1028" id="Seg_11023" s="T1027">лисица.[NOM.SG]</ta>
            <ta e="T1029" id="Seg_11024" s="T1028">куда</ta>
            <ta e="T1030" id="Seg_11025" s="T1029">идти-PRS-2SG</ta>
            <ta e="T1031" id="Seg_11026" s="T1030">взять-IMP.2SG</ta>
            <ta e="T1032" id="Seg_11027" s="T1031">я.ACC</ta>
            <ta e="T1033" id="Seg_11028" s="T1032">%%-EP-CNG</ta>
            <ta e="T1034" id="Seg_11029" s="T1033">я.LAT</ta>
            <ta e="T1035" id="Seg_11030" s="T1034">внутри-LAT/LOC.3SG</ta>
            <ta e="T1036" id="Seg_11031" s="T1035">этот.[NOM.SG]</ta>
            <ta e="T1037" id="Seg_11032" s="T1036">%%-PST.[3SG]</ta>
            <ta e="T1038" id="Seg_11033" s="T1037">тогда</ta>
            <ta e="T1039" id="Seg_11034" s="T1038">прийти-PRS.[3SG]</ta>
            <ta e="T1040" id="Seg_11035" s="T1039">прийти-PRS.[3SG]</ta>
            <ta e="T1041" id="Seg_11036" s="T1040">волк-LAT</ta>
            <ta e="T1042" id="Seg_11037" s="T1041">куда</ta>
            <ta e="T1043" id="Seg_11038" s="T1042">идти-PRS-2SG</ta>
            <ta e="T1044" id="Seg_11039" s="T1043">мельница-NOM/GEN/ACC.3SG</ta>
            <ta e="T1045" id="Seg_11040" s="T1044">взять-INF.LAT</ta>
            <ta e="T1047" id="Seg_11041" s="T1046">взять-IMP.2SG</ta>
            <ta e="T1048" id="Seg_11042" s="T1047">я.ACC</ta>
            <ta e="T1049" id="Seg_11043" s="T1048">%%-EP-IMP.2SG</ta>
            <ta e="T1050" id="Seg_11044" s="T1049">я.LAT</ta>
            <ta e="T1051" id="Seg_11045" s="T1050">внутри-LAT/LOC.3SG</ta>
            <ta e="T1052" id="Seg_11046" s="T1051">этот.[NOM.SG]</ta>
            <ta e="T1053" id="Seg_11047" s="T1052">%%-PST.[3SG]</ta>
            <ta e="T1054" id="Seg_11048" s="T1053">тогда</ta>
            <ta e="T1055" id="Seg_11049" s="T1054">прийти-PRS.[3SG]</ta>
            <ta e="T1056" id="Seg_11050" s="T1055">медведь.[NOM.SG]</ta>
            <ta e="T1057" id="Seg_11051" s="T1056">прийти-PRS.[3SG]</ta>
            <ta e="T1058" id="Seg_11052" s="T1057">%%-EP-IMP.2SG</ta>
            <ta e="T1059" id="Seg_11053" s="T1058">я.LAT</ta>
            <ta e="T1060" id="Seg_11054" s="T1059">тогда</ta>
            <ta e="T1061" id="Seg_11055" s="T1060">прийти-PST.[3SG]</ta>
            <ta e="T1062" id="Seg_11056" s="T1061">INCH</ta>
            <ta e="T1063" id="Seg_11057" s="T1062">кричать-INF.LAT</ta>
            <ta e="T1065" id="Seg_11058" s="T1064">принести-IMP.2SG.O</ta>
            <ta e="T1066" id="Seg_11059" s="T1065">я.GEN</ta>
            <ta e="T1067" id="Seg_11060" s="T1066">мельница-NOM/GEN/ACC.3SG</ta>
            <ta e="T1068" id="Seg_11061" s="T1067">принести-IMP.2SG.O</ta>
            <ta e="T1069" id="Seg_11062" s="T1068">я.GEN</ta>
            <ta e="T1070" id="Seg_11063" s="T1069">мельница-NOM/GEN/ACC.3SG</ta>
            <ta e="T1071" id="Seg_11064" s="T1070">этот.[NOM.SG]</ta>
            <ta e="T1072" id="Seg_11065" s="T1071">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1074" id="Seg_11066" s="T1073">бросить-IMP.2PL</ta>
            <ta e="T1075" id="Seg_11067" s="T1074">этот-ACC</ta>
            <ta e="T1076" id="Seg_11068" s="T1075">а</ta>
            <ta e="T1077" id="Seg_11069" s="T1076">утка-3PL-LAT</ta>
            <ta e="T1078" id="Seg_11070" s="T1077">JUSS</ta>
            <ta e="T1079" id="Seg_11071" s="T1078">утка-PL</ta>
            <ta e="T1080" id="Seg_11072" s="T1079">съесть-MOM-IMP.3SG.O</ta>
            <ta e="T1081" id="Seg_11073" s="T1080">кость-NOM/GEN/ACC.3PL</ta>
            <ta e="T1082" id="Seg_11074" s="T1081">этот-ACC</ta>
            <ta e="T1083" id="Seg_11075" s="T1082">бросить-MOM-PST.[3SG]</ta>
            <ta e="T1084" id="Seg_11076" s="T1083">а</ta>
            <ta e="T1085" id="Seg_11077" s="T1084">этот.[NOM.SG]</ta>
            <ta e="T1086" id="Seg_11078" s="T1085">сказать-IPFVZ.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PKZ">
            <ta e="T5" id="Seg_11079" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_11080" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_11081" s="T6">dempro-n:case</ta>
            <ta e="T8" id="Seg_11082" s="T7">num-n:case</ta>
            <ta e="T9" id="Seg_11083" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_11084" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_11085" s="T10">num-n:case</ta>
            <ta e="T12" id="Seg_11086" s="T11">propr-n:case</ta>
            <ta e="T13" id="Seg_11087" s="T12">num-n:case</ta>
            <ta e="T14" id="Seg_11088" s="T13">propr-n:case</ta>
            <ta e="T15" id="Seg_11089" s="T14">num-n:case</ta>
            <ta e="T16" id="Seg_11090" s="T15">propr-n:case</ta>
            <ta e="T17" id="Seg_11091" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_11092" s="T17">adv</ta>
            <ta e="T20" id="Seg_11093" s="T19">dempro-n:case</ta>
            <ta e="T21" id="Seg_11094" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_11095" s="T21">v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_11096" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_11097" s="T23">conj</ta>
            <ta e="T25" id="Seg_11098" s="T24">dempro-n:case</ta>
            <ta e="T26" id="Seg_11099" s="T25">que-n:case=ptcl</ta>
            <ta e="T29" id="Seg_11100" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_11101" s="T29">v-v&gt;v-v:pn</ta>
            <ta e="T31" id="Seg_11102" s="T30">conj</ta>
            <ta e="T32" id="Seg_11103" s="T31">v-v&gt;v-v:pn</ta>
            <ta e="T33" id="Seg_11104" s="T32">n</ta>
            <ta e="T34" id="Seg_11105" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_11106" s="T34">v-v&gt;v-v:pn</ta>
            <ta e="T36" id="Seg_11107" s="T35">v-v:n.fin</ta>
            <ta e="T38" id="Seg_11108" s="T37">v-v:n.fin</ta>
            <ta e="T39" id="Seg_11109" s="T38">que-n:case</ta>
            <ta e="T40" id="Seg_11110" s="T39">v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_11111" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_11112" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_11113" s="T42">v-v:n.fin</ta>
            <ta e="T44" id="Seg_11114" s="T43">adv</ta>
            <ta e="T45" id="Seg_11115" s="T44">v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_11116" s="T45">adj-n:case</ta>
            <ta e="T47" id="Seg_11117" s="T46">n-n:case.poss</ta>
            <ta e="T48" id="Seg_11118" s="T47">propr-n:case</ta>
            <ta e="T49" id="Seg_11119" s="T48">adv</ta>
            <ta e="T50" id="Seg_11120" s="T49">v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_11121" s="T50">v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_11122" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_11123" s="T52">conj</ta>
            <ta e="T54" id="Seg_11124" s="T53">v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_11125" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_11126" s="T55">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_11127" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_11128" s="T57">v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_11129" s="T58">conj</ta>
            <ta e="T60" id="Seg_11130" s="T59">que-n:case=ptcl</ta>
            <ta e="T61" id="Seg_11131" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_11132" s="T61">v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_11133" s="T62">adv</ta>
            <ta e="T64" id="Seg_11134" s="T63">adj-n:case</ta>
            <ta e="T65" id="Seg_11135" s="T64">n-n:case.poss</ta>
            <ta e="T66" id="Seg_11136" s="T65">v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_11137" s="T66">propr-n:case.poss</ta>
            <ta e="T68" id="Seg_11138" s="T67">v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_11139" s="T68">v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_11140" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_11141" s="T70">adv</ta>
            <ta e="T72" id="Seg_11142" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_11143" s="T72">v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_11144" s="T73">quant</ta>
            <ta e="T75" id="Seg_11145" s="T74">n-n:case</ta>
            <ta e="T76" id="Seg_11146" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_11147" s="T76">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_11148" s="T77">conj</ta>
            <ta e="T79" id="Seg_11149" s="T78">que-n:case=ptcl</ta>
            <ta e="T80" id="Seg_11150" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_11151" s="T80">v-v:tense-v:pn</ta>
            <ta e="T82" id="Seg_11152" s="T81">adv</ta>
            <ta e="T83" id="Seg_11153" s="T82">propr-n:case</ta>
            <ta e="T84" id="Seg_11154" s="T83">v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_11155" s="T84">v-v:tense-v:pn</ta>
            <ta e="T86" id="Seg_11156" s="T85">n-n:case.poss</ta>
            <ta e="T87" id="Seg_11157" s="T86">adv</ta>
            <ta e="T88" id="Seg_11158" s="T87">v-v:tense-v:pn</ta>
            <ta e="T89" id="Seg_11159" s="T88">n-n:case</ta>
            <ta e="T90" id="Seg_11160" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_11161" s="T90">v-v:tense-v:pn</ta>
            <ta e="T92" id="Seg_11162" s="T91">v-v&gt;v-v:pn</ta>
            <ta e="T93" id="Seg_11163" s="T92">v-v:tense-v:pn</ta>
            <ta e="T94" id="Seg_11164" s="T93">n-n:case</ta>
            <ta e="T95" id="Seg_11165" s="T94">v-v:tense-v:pn</ta>
            <ta e="T96" id="Seg_11166" s="T95">adj</ta>
            <ta e="T97" id="Seg_11167" s="T96">conj</ta>
            <ta e="T98" id="Seg_11168" s="T97">ptcl</ta>
            <ta e="T99" id="Seg_11169" s="T98">adv</ta>
            <ta e="T100" id="Seg_11170" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_11171" s="T100">n-n:case.poss</ta>
            <ta e="T102" id="Seg_11172" s="T101">adj-n:case</ta>
            <ta e="T103" id="Seg_11173" s="T102">dempro-n:case</ta>
            <ta e="T104" id="Seg_11174" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_11175" s="T104">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T106" id="Seg_11176" s="T105">n-n:case.poss</ta>
            <ta e="T107" id="Seg_11177" s="T106">dempro-n:case</ta>
            <ta e="T108" id="Seg_11178" s="T107">conj</ta>
            <ta e="T109" id="Seg_11179" s="T108">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T110" id="Seg_11180" s="T109">dempro-n:case</ta>
            <ta e="T111" id="Seg_11181" s="T110">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T112" id="Seg_11182" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_11183" s="T112">v-v:tense-v:pn</ta>
            <ta e="T114" id="Seg_11184" s="T113">v-v:n.fin</ta>
            <ta e="T115" id="Seg_11185" s="T114">adv</ta>
            <ta e="T116" id="Seg_11186" s="T115">v-v:mood.pn</ta>
            <ta e="T117" id="Seg_11187" s="T116">pers</ta>
            <ta e="T118" id="Seg_11188" s="T117">pers</ta>
            <ta e="T119" id="Seg_11189" s="T118">pers</ta>
            <ta e="T120" id="Seg_11190" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_11191" s="T120">que-n:case</ta>
            <ta e="T122" id="Seg_11192" s="T121">v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_11193" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_11194" s="T123">que</ta>
            <ta e="T125" id="Seg_11195" s="T124">v-v:mood.pn</ta>
            <ta e="T126" id="Seg_11196" s="T125">adv</ta>
            <ta e="T127" id="Seg_11197" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_11198" s="T127">v-v:tense-v:pn</ta>
            <ta e="T129" id="Seg_11199" s="T128">n-n:case</ta>
            <ta e="T130" id="Seg_11200" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_11201" s="T130">v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_11202" s="T131">conj</ta>
            <ta e="T133" id="Seg_11203" s="T132">que</ta>
            <ta e="T134" id="Seg_11204" s="T133">v-v:tense-v:pn</ta>
            <ta e="T135" id="Seg_11205" s="T134">ptcl</ta>
            <ta e="T136" id="Seg_11206" s="T135">pers</ta>
            <ta e="T137" id="Seg_11207" s="T136">v-v:mood.pn</ta>
            <ta e="T138" id="Seg_11208" s="T137">adv</ta>
            <ta e="T139" id="Seg_11209" s="T138">v-v:mood.pn</ta>
            <ta e="T141" id="Seg_11210" s="T140">pers</ta>
            <ta e="T142" id="Seg_11211" s="T141">ptcl</ta>
            <ta e="T143" id="Seg_11212" s="T142">adj-n:case</ta>
            <ta e="T144" id="Seg_11213" s="T143">conj</ta>
            <ta e="T145" id="Seg_11214" s="T144">v-v:n.fin</ta>
            <ta e="T146" id="Seg_11215" s="T145">v-v:tense-v:pn</ta>
            <ta e="T147" id="Seg_11216" s="T146">n-n:case.poss</ta>
            <ta e="T148" id="Seg_11217" s="T147">conj</ta>
            <ta e="T149" id="Seg_11218" s="T148">dempro-n:case</ta>
            <ta e="T150" id="Seg_11219" s="T149">v-v:tense-v:pn</ta>
            <ta e="T151" id="Seg_11220" s="T150">n-n:case.poss</ta>
            <ta e="T152" id="Seg_11221" s="T151">n-n:case</ta>
            <ta e="T153" id="Seg_11222" s="T152">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T154" id="Seg_11223" s="T153">pers</ta>
            <ta e="T155" id="Seg_11224" s="T154">v-v:tense-v:pn</ta>
            <ta e="T156" id="Seg_11225" s="T155">n-n:case</ta>
            <ta e="T157" id="Seg_11226" s="T156">adv</ta>
            <ta e="T158" id="Seg_11227" s="T157">adv</ta>
            <ta e="T159" id="Seg_11228" s="T158">que=ptcl</ta>
            <ta e="T160" id="Seg_11229" s="T159">ptcl</ta>
            <ta e="T161" id="Seg_11230" s="T160">v-v:tense-v:pn</ta>
            <ta e="T162" id="Seg_11231" s="T161">v-v:n.fin</ta>
            <ta e="T163" id="Seg_11232" s="T162">n-n:case</ta>
            <ta e="T164" id="Seg_11233" s="T163">adv</ta>
            <ta e="T165" id="Seg_11234" s="T164">n-n:case</ta>
            <ta e="T166" id="Seg_11235" s="T165">v-v:tense-v:pn</ta>
            <ta e="T167" id="Seg_11236" s="T166">conj</ta>
            <ta e="T168" id="Seg_11237" s="T167">n-n:case</ta>
            <ta e="T169" id="Seg_11238" s="T168">v-v:tense-v:pn</ta>
            <ta e="T171" id="Seg_11239" s="T170">dempro-n:case</ta>
            <ta e="T172" id="Seg_11240" s="T171">n-n:case</ta>
            <ta e="T173" id="Seg_11241" s="T172">v-v&gt;v-v:pn</ta>
            <ta e="T174" id="Seg_11242" s="T173">n-n:case</ta>
            <ta e="T175" id="Seg_11243" s="T174">quant</ta>
            <ta e="T176" id="Seg_11244" s="T175">n-n:case</ta>
            <ta e="T177" id="Seg_11245" s="T176">v-v&gt;v-v:pn</ta>
            <ta e="T178" id="Seg_11246" s="T177">conj</ta>
            <ta e="T179" id="Seg_11247" s="T178">conj</ta>
            <ta e="T180" id="Seg_11248" s="T179">dempro-n:case</ta>
            <ta e="T181" id="Seg_11249" s="T180">n-n:num</ta>
            <ta e="T182" id="Seg_11250" s="T181">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T183" id="Seg_11251" s="T182">conj</ta>
            <ta e="T184" id="Seg_11252" s="T183">dempro-n:case</ta>
            <ta e="T185" id="Seg_11253" s="T184">v-v&gt;v-v:pn</ta>
            <ta e="T186" id="Seg_11254" s="T185">v-v:mood.pn</ta>
            <ta e="T187" id="Seg_11255" s="T186">pers</ta>
            <ta e="T188" id="Seg_11256" s="T187">que</ta>
            <ta e="T189" id="Seg_11257" s="T188">pers</ta>
            <ta e="T190" id="Seg_11258" s="T189">v-v:tense-v:pn</ta>
            <ta e="T191" id="Seg_11259" s="T190">que-n:case=ptcl</ta>
            <ta e="T192" id="Seg_11260" s="T191">adv</ta>
            <ta e="T193" id="Seg_11261" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_11262" s="T193">v-v:tense-v:pn</ta>
            <ta e="T195" id="Seg_11263" s="T194">conj</ta>
            <ta e="T196" id="Seg_11264" s="T195">n-n:case</ta>
            <ta e="T197" id="Seg_11265" s="T196">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T199" id="Seg_11266" s="T198">pers</ta>
            <ta e="T200" id="Seg_11267" s="T199">dempro-n:num</ta>
            <ta e="T201" id="Seg_11268" s="T200">v-v:n.fin</ta>
            <ta e="T202" id="Seg_11269" s="T201">v-v:tense-v:pn</ta>
            <ta e="T203" id="Seg_11270" s="T202">dempro-n:case</ta>
            <ta e="T204" id="Seg_11271" s="T203">v-v:tense-v:pn</ta>
            <ta e="T205" id="Seg_11272" s="T204">n-n:case</ta>
            <ta e="T206" id="Seg_11273" s="T205">v-v:tense-v:pn</ta>
            <ta e="T207" id="Seg_11274" s="T206">v-v:tense-v:pn</ta>
            <ta e="T208" id="Seg_11275" s="T207">conj</ta>
            <ta e="T209" id="Seg_11276" s="T208">ptcl</ta>
            <ta e="T210" id="Seg_11277" s="T209">v-v:n.fin</ta>
            <ta e="T211" id="Seg_11278" s="T210">n-n:case.poss</ta>
            <ta e="T212" id="Seg_11279" s="T211">v-v:tense-v:pn</ta>
            <ta e="T213" id="Seg_11280" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_11281" s="T213">adv</ta>
            <ta e="T215" id="Seg_11282" s="T214">n-n:case</ta>
            <ta e="T216" id="Seg_11283" s="T215">v-v:tense-v:pn</ta>
            <ta e="T217" id="Seg_11284" s="T216">dempro-n:case</ta>
            <ta e="T218" id="Seg_11285" s="T217">n-n:case.poss</ta>
            <ta e="T219" id="Seg_11286" s="T218">v-v:tense-v:pn</ta>
            <ta e="T220" id="Seg_11287" s="T219">adv</ta>
            <ta e="T221" id="Seg_11288" s="T220">dempro-n:case</ta>
            <ta e="T222" id="Seg_11289" s="T221">n-n:case</ta>
            <ta e="T224" id="Seg_11290" s="T222">v-v&gt;v-v:pn</ta>
            <ta e="T225" id="Seg_11291" s="T224">v-v:tense-v:pn</ta>
            <ta e="T226" id="Seg_11292" s="T225">dempro-n:case</ta>
            <ta e="T227" id="Seg_11293" s="T226">v-v&gt;v-v:pn</ta>
            <ta e="T228" id="Seg_11294" s="T227">num-n:case</ta>
            <ta e="T229" id="Seg_11295" s="T228">n-n:case</ta>
            <ta e="T230" id="Seg_11296" s="T229">v-v:mood.pn</ta>
            <ta e="T231" id="Seg_11297" s="T230">num-n:case</ta>
            <ta e="T232" id="Seg_11298" s="T231">v-v:mood.pn</ta>
            <ta e="T233" id="Seg_11299" s="T232">dempro-n:case</ta>
            <ta e="T234" id="Seg_11300" s="T233">v-v:tense-v:pn</ta>
            <ta e="T235" id="Seg_11301" s="T234">v-v:tense-v:pn</ta>
            <ta e="T236" id="Seg_11302" s="T235">adj-n:case</ta>
            <ta e="T237" id="Seg_11303" s="T236">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T238" id="Seg_11304" s="T237">adv</ta>
            <ta e="T239" id="Seg_11305" s="T238">dempro-n:case</ta>
            <ta e="T240" id="Seg_11306" s="T239">v-v:tense-v:pn</ta>
            <ta e="T241" id="Seg_11307" s="T240">dempro-n:case</ta>
            <ta e="T242" id="Seg_11308" s="T241">v</ta>
            <ta e="T243" id="Seg_11309" s="T242">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T244" id="Seg_11310" s="T243">adv</ta>
            <ta e="T245" id="Seg_11311" s="T244">n-n:case</ta>
            <ta e="T246" id="Seg_11312" s="T245">quant</ta>
            <ta e="T247" id="Seg_11313" s="T246">v-v:tense-v:pn</ta>
            <ta e="T248" id="Seg_11314" s="T247">conj</ta>
            <ta e="T249" id="Seg_11315" s="T248">que-n:case=ptcl</ta>
            <ta e="T250" id="Seg_11316" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_11317" s="T250">dempro-n:case</ta>
            <ta e="T252" id="Seg_11318" s="T251">n-n:case.poss</ta>
            <ta e="T253" id="Seg_11319" s="T252">ptcl</ta>
            <ta e="T254" id="Seg_11320" s="T253">v-v:tense-v:pn</ta>
            <ta e="T255" id="Seg_11321" s="T254">dempro-n:case</ta>
            <ta e="T256" id="Seg_11322" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_11323" s="T256">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T258" id="Seg_11324" s="T257">v-v:tense-v:pn</ta>
            <ta e="T259" id="Seg_11325" s="T258">ptcl</ta>
            <ta e="T260" id="Seg_11326" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_11327" s="T260">v-v:tense-v:pn</ta>
            <ta e="T262" id="Seg_11328" s="T261">adv</ta>
            <ta e="T263" id="Seg_11329" s="T262">n-n:case</ta>
            <ta e="T264" id="Seg_11330" s="T263">quant</ta>
            <ta e="T265" id="Seg_11331" s="T264">v-v&gt;v-v:pn</ta>
            <ta e="T266" id="Seg_11332" s="T265">v-v:mood.pn</ta>
            <ta e="T267" id="Seg_11333" s="T266">v-v:mood.pn</ta>
            <ta e="T268" id="Seg_11334" s="T267">conj</ta>
            <ta e="T269" id="Seg_11335" s="T268">dempro-n:case</ta>
            <ta e="T270" id="Seg_11336" s="T269">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T271" id="Seg_11337" s="T270">n-n:case</ta>
            <ta e="T272" id="Seg_11338" s="T271">v-v:tense-v:pn</ta>
            <ta e="T273" id="Seg_11339" s="T272">adv</ta>
            <ta e="T274" id="Seg_11340" s="T273">n-n:case.poss</ta>
            <ta e="T275" id="Seg_11341" s="T274">v-v:tense-v:pn</ta>
            <ta e="T276" id="Seg_11342" s="T275">conj</ta>
            <ta e="T277" id="Seg_11343" s="T276">adj</ta>
            <ta e="T278" id="Seg_11344" s="T277">quant</ta>
            <ta e="T279" id="Seg_11345" s="T278">n-n:case</ta>
            <ta e="T280" id="Seg_11346" s="T279">v-v:tense-v:pn</ta>
            <ta e="T281" id="Seg_11347" s="T280">conj</ta>
            <ta e="T282" id="Seg_11348" s="T281">n-n:num</ta>
            <ta e="T283" id="Seg_11349" s="T282">v-v&gt;v-v:pn</ta>
            <ta e="T284" id="Seg_11350" s="T283">que-n:case</ta>
            <ta e="T285" id="Seg_11351" s="T284">adj-n:case</ta>
            <ta e="T287" id="Seg_11352" s="T286">v-v:tense-v:pn</ta>
            <ta e="T288" id="Seg_11353" s="T287">ptcl</ta>
            <ta e="T289" id="Seg_11354" s="T288">pers</ta>
            <ta e="T290" id="Seg_11355" s="T289">v-v:n.fin</ta>
            <ta e="T291" id="Seg_11356" s="T290">dempro-n:case</ta>
            <ta e="T292" id="Seg_11357" s="T291">n-n:case</ta>
            <ta e="T294" id="Seg_11358" s="T293">v-v:tense-v:pn</ta>
            <ta e="T295" id="Seg_11359" s="T294">v-v&gt;v-v:pn</ta>
            <ta e="T296" id="Seg_11360" s="T295">adv</ta>
            <ta e="T298" id="Seg_11361" s="T297">n-n:num-n:case.poss</ta>
            <ta e="T299" id="Seg_11362" s="T298">v-v:tense-v:pn</ta>
            <ta e="T300" id="Seg_11363" s="T299">ptcl</ta>
            <ta e="T301" id="Seg_11364" s="T300">v-v&gt;v-v:pn</ta>
            <ta e="T303" id="Seg_11365" s="T302">que</ta>
            <ta e="T304" id="Seg_11366" s="T303">v-v:tense-v:pn</ta>
            <ta e="T305" id="Seg_11367" s="T304">conj</ta>
            <ta e="T306" id="Seg_11368" s="T305">dempro-n:case</ta>
            <ta e="T307" id="Seg_11369" s="T306">v-v&gt;v-v:pn</ta>
            <ta e="T308" id="Seg_11370" s="T307">dempro-n:case</ta>
            <ta e="T309" id="Seg_11371" s="T308">pers</ta>
            <ta e="T310" id="Seg_11372" s="T309">n-n:case.poss</ta>
            <ta e="T311" id="Seg_11373" s="T310">adv</ta>
            <ta e="T312" id="Seg_11374" s="T311">v-v:tense-v:pn</ta>
            <ta e="T313" id="Seg_11375" s="T312">aux-v:mood.pn</ta>
            <ta e="T314" id="Seg_11376" s="T313">v-v:n.fin</ta>
            <ta e="T315" id="Seg_11377" s="T314">pers</ta>
            <ta e="T316" id="Seg_11378" s="T315">n-n:case.poss</ta>
            <ta e="T317" id="Seg_11379" s="T316">v-v:mood.pn</ta>
            <ta e="T318" id="Seg_11380" s="T317">ptcl</ta>
            <ta e="T319" id="Seg_11381" s="T318">que-n:case=ptcl</ta>
            <ta e="T320" id="Seg_11382" s="T319">aux-v:mood.pn</ta>
            <ta e="T321" id="Seg_11383" s="T320">v-v:ins-v:n.fin</ta>
            <ta e="T322" id="Seg_11384" s="T321">adj-n:case</ta>
            <ta e="T323" id="Seg_11385" s="T322">n-n:case</ta>
            <ta e="T324" id="Seg_11386" s="T323">v-v:tense-v:pn</ta>
            <ta e="T325" id="Seg_11387" s="T324">n-n:case.poss</ta>
            <ta e="T326" id="Seg_11388" s="T325">adj-n:case</ta>
            <ta e="T327" id="Seg_11389" s="T326">v-v:tense-v:pn</ta>
            <ta e="T328" id="Seg_11390" s="T327">refl-n:case.poss</ta>
            <ta e="T329" id="Seg_11391" s="T328">adv</ta>
            <ta e="T330" id="Seg_11392" s="T329">adv</ta>
            <ta e="T331" id="Seg_11393" s="T330">adv</ta>
            <ta e="T332" id="Seg_11394" s="T331">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T333" id="Seg_11395" s="T332">v-v:n.fin</ta>
            <ta e="T334" id="Seg_11396" s="T333">v-v:tense-v:pn</ta>
            <ta e="T335" id="Seg_11397" s="T334">dempro-n:case</ta>
            <ta e="T336" id="Seg_11398" s="T335">n-n:case</ta>
            <ta e="T338" id="Seg_11399" s="T337">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T339" id="Seg_11400" s="T338">n-n:case</ta>
            <ta e="T340" id="Seg_11401" s="T339">v-v:tense-v:pn</ta>
            <ta e="T341" id="Seg_11402" s="T340">v-v:tense-v:pn</ta>
            <ta e="T342" id="Seg_11403" s="T341">v-v:tense-v:pn</ta>
            <ta e="T344" id="Seg_11404" s="T342">v-v:tense-v:pn</ta>
            <ta e="T345" id="Seg_11405" s="T344">v</ta>
            <ta e="T346" id="Seg_11406" s="T345">n-n:case</ta>
            <ta e="T347" id="Seg_11407" s="T346">v-v:n.fin</ta>
            <ta e="T348" id="Seg_11408" s="T347">n-n:case.poss</ta>
            <ta e="T349" id="Seg_11409" s="T348">v-v:tense-v:pn</ta>
            <ta e="T350" id="Seg_11410" s="T349">adv</ta>
            <ta e="T351" id="Seg_11411" s="T350">num-n:case</ta>
            <ta e="T352" id="Seg_11412" s="T351">n-n:case.poss</ta>
            <ta e="T353" id="Seg_11413" s="T352">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T354" id="Seg_11414" s="T353">num-n:case</ta>
            <ta e="T355" id="Seg_11415" s="T354">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T356" id="Seg_11416" s="T355">adv</ta>
            <ta e="T357" id="Seg_11417" s="T356">v-v:tense-v:pn</ta>
            <ta e="T359" id="Seg_11418" s="T358">v-v:tense-v:pn</ta>
            <ta e="T360" id="Seg_11419" s="T359">n-n:case</ta>
            <ta e="T361" id="Seg_11420" s="T360">quant</ta>
            <ta e="T362" id="Seg_11421" s="T361">v-v:tense-v:pn</ta>
            <ta e="T363" id="Seg_11422" s="T362">adv</ta>
            <ta e="T364" id="Seg_11423" s="T363">que-n:case=ptcl</ta>
            <ta e="T365" id="Seg_11424" s="T364">ptcl</ta>
            <ta e="T366" id="Seg_11425" s="T365">v-v:tense-v:pn</ta>
            <ta e="T367" id="Seg_11426" s="T366">adv</ta>
            <ta e="T368" id="Seg_11427" s="T367">adv</ta>
            <ta e="T370" id="Seg_11428" s="T369">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T371" id="Seg_11429" s="T370">v-v:tense-v:pn</ta>
            <ta e="T376" id="Seg_11430" s="T375">ptcl</ta>
            <ta e="T377" id="Seg_11431" s="T376">v-v:tense-v:pn</ta>
            <ta e="T378" id="Seg_11432" s="T377">adv</ta>
            <ta e="T379" id="Seg_11433" s="T378">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T380" id="Seg_11434" s="T379">n-n:num-n:case.poss</ta>
            <ta e="T381" id="Seg_11435" s="T380">quant</ta>
            <ta e="T382" id="Seg_11436" s="T381">v-v:tense-v:pn</ta>
            <ta e="T383" id="Seg_11437" s="T382">conj</ta>
            <ta e="T384" id="Seg_11438" s="T383">v-v:n.fin</ta>
            <ta e="T385" id="Seg_11439" s="T384">v-v:tense-v:pn</ta>
            <ta e="T386" id="Seg_11440" s="T385">conj</ta>
            <ta e="T387" id="Seg_11441" s="T386">n-n:case</ta>
            <ta e="T388" id="Seg_11442" s="T387">v-v&gt;v-v:pn</ta>
            <ta e="T389" id="Seg_11443" s="T388">v-v:mood.pn</ta>
            <ta e="T390" id="Seg_11444" s="T389">v-v:mood.pn</ta>
            <ta e="T391" id="Seg_11445" s="T390">conj</ta>
            <ta e="T392" id="Seg_11446" s="T391">dempro-n:case</ta>
            <ta e="T393" id="Seg_11447" s="T392">aux-v:mood.pn</ta>
            <ta e="T394" id="Seg_11448" s="T393">v-v:tense</ta>
            <ta e="T395" id="Seg_11449" s="T394">n-n:case.poss</ta>
            <ta e="T396" id="Seg_11450" s="T395">v-v:tense-v:pn</ta>
            <ta e="T397" id="Seg_11451" s="T396">conj</ta>
            <ta e="T398" id="Seg_11452" s="T397">refl-n:case.poss</ta>
            <ta e="T399" id="Seg_11453" s="T398">adv</ta>
            <ta e="T400" id="Seg_11454" s="T399">v-v:tense-v:pn</ta>
            <ta e="T401" id="Seg_11455" s="T400">n-n:num-n:case.poss</ta>
            <ta e="T402" id="Seg_11456" s="T401">adv</ta>
            <ta e="T403" id="Seg_11457" s="T402">v-v&gt;v-v:pn</ta>
            <ta e="T404" id="Seg_11458" s="T403">conj</ta>
            <ta e="T405" id="Seg_11459" s="T404">dempro-n:case</ta>
            <ta e="T406" id="Seg_11460" s="T405">n-n:case</ta>
            <ta e="T407" id="Seg_11461" s="T406">v-v:tense-v:pn</ta>
            <ta e="T408" id="Seg_11462" s="T407">conj</ta>
            <ta e="T409" id="Seg_11463" s="T408">v-v&gt;v-v:pn</ta>
            <ta e="T411" id="Seg_11464" s="T410">n-n:case.poss</ta>
            <ta e="T412" id="Seg_11465" s="T411">v-v:tense-v:pn</ta>
            <ta e="T413" id="Seg_11466" s="T412">n-n:case.poss</ta>
            <ta e="T414" id="Seg_11467" s="T413">v-v:tense-v:pn</ta>
            <ta e="T415" id="Seg_11468" s="T414">que</ta>
            <ta e="T416" id="Seg_11469" s="T415">n-n:case</ta>
            <ta e="T417" id="Seg_11470" s="T416">v-v:tense-v:pn</ta>
            <ta e="T418" id="Seg_11471" s="T417">n-n:case.poss</ta>
            <ta e="T419" id="Seg_11472" s="T418">adv</ta>
            <ta e="T420" id="Seg_11473" s="T419">adj-n:case</ta>
            <ta e="T421" id="Seg_11474" s="T420">conj</ta>
            <ta e="T423" id="Seg_11475" s="T422">refl-n:case.poss</ta>
            <ta e="T424" id="Seg_11476" s="T423">adj-n:case</ta>
            <ta e="T425" id="Seg_11477" s="T424">conj</ta>
            <ta e="T426" id="Seg_11478" s="T425">dempro-n:case</ta>
            <ta e="T427" id="Seg_11479" s="T426">v-v&gt;v-v:pn</ta>
            <ta e="T428" id="Seg_11480" s="T427">ptcl</ta>
            <ta e="T429" id="Seg_11481" s="T428">pers</ta>
            <ta e="T430" id="Seg_11482" s="T429">ptcl</ta>
            <ta e="T431" id="Seg_11483" s="T430">n-n:case.poss</ta>
            <ta e="T432" id="Seg_11484" s="T431">adv</ta>
            <ta e="T433" id="Seg_11485" s="T432">v-v:tense-v:pn</ta>
            <ta e="T434" id="Seg_11486" s="T433">aux-v:mood.pn</ta>
            <ta e="T435" id="Seg_11487" s="T434">v-v:n.fin</ta>
            <ta e="T436" id="Seg_11488" s="T435">v-v:mood.pn</ta>
            <ta e="T437" id="Seg_11489" s="T436">ptcl</ta>
            <ta e="T438" id="Seg_11490" s="T437">n-n&gt;adj</ta>
            <ta e="T439" id="Seg_11491" s="T438">n-n:case</ta>
            <ta e="T440" id="Seg_11492" s="T439">pers</ta>
            <ta e="T441" id="Seg_11493" s="T440">adv</ta>
            <ta e="T442" id="Seg_11494" s="T441">v-v:tense-v:pn</ta>
            <ta e="T443" id="Seg_11495" s="T442">adv</ta>
            <ta e="T444" id="Seg_11496" s="T443">adv</ta>
            <ta e="T445" id="Seg_11497" s="T444">num-n:case</ta>
            <ta e="T446" id="Seg_11498" s="T445">n-n:case</ta>
            <ta e="T447" id="Seg_11499" s="T446">adv</ta>
            <ta e="T448" id="Seg_11500" s="T447">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T449" id="Seg_11501" s="T448">v-v:n.fin</ta>
            <ta e="T450" id="Seg_11502" s="T449">v-v:tense-v:pn</ta>
            <ta e="T451" id="Seg_11503" s="T450">dempro-n:case</ta>
            <ta e="T453" id="Seg_11504" s="T452">v-v:tense-v:pn</ta>
            <ta e="T454" id="Seg_11505" s="T453">n-n:case.poss</ta>
            <ta e="T455" id="Seg_11506" s="T454">v-v:tense-v:pn</ta>
            <ta e="T456" id="Seg_11507" s="T455">v-v:tense-v:pn</ta>
            <ta e="T457" id="Seg_11508" s="T456">n-n:case.poss</ta>
            <ta e="T458" id="Seg_11509" s="T457">v-v:n.fin</ta>
            <ta e="T459" id="Seg_11510" s="T458">v-v:tense-v:pn</ta>
            <ta e="T460" id="Seg_11511" s="T459">ptcl</ta>
            <ta e="T461" id="Seg_11512" s="T460">n-n:case</ta>
            <ta e="T462" id="Seg_11513" s="T461">v-v:n.fin</ta>
            <ta e="T463" id="Seg_11514" s="T462">n-n:case</ta>
            <ta e="T464" id="Seg_11515" s="T463">v-v:tense-v:pn</ta>
            <ta e="T465" id="Seg_11516" s="T464">dempro-n:case</ta>
            <ta e="T466" id="Seg_11517" s="T465">adv</ta>
            <ta e="T467" id="Seg_11518" s="T466">post</ta>
            <ta e="T469" id="Seg_11519" s="T468">adj-n:case</ta>
            <ta e="T470" id="Seg_11520" s="T469">n-n:case.poss</ta>
            <ta e="T471" id="Seg_11521" s="T470">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T472" id="Seg_11522" s="T471">num-n:case</ta>
            <ta e="T473" id="Seg_11523" s="T472">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T474" id="Seg_11524" s="T473">v-v:tense-v:pn</ta>
            <ta e="T475" id="Seg_11525" s="T474">v-v:tense-v:pn</ta>
            <ta e="T476" id="Seg_11526" s="T475">adv</ta>
            <ta e="T477" id="Seg_11527" s="T476">ptcl</ta>
            <ta e="T478" id="Seg_11528" s="T477">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T479" id="Seg_11529" s="T478">dempro-n:case</ta>
            <ta e="T480" id="Seg_11530" s="T479">v-v:tense-v:pn</ta>
            <ta e="T481" id="Seg_11531" s="T480">conj</ta>
            <ta e="T482" id="Seg_11532" s="T481">n-n:case.poss</ta>
            <ta e="T483" id="Seg_11533" s="T482">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T484" id="Seg_11534" s="T483">refl-n:case.poss</ta>
            <ta e="T485" id="Seg_11535" s="T484">n-n:case.poss</ta>
            <ta e="T487" id="Seg_11536" s="T486">v-v:tense-v:pn</ta>
            <ta e="T488" id="Seg_11537" s="T487">v-v:n.fin</ta>
            <ta e="T489" id="Seg_11538" s="T488">v-v:tense-v:pn</ta>
            <ta e="T490" id="Seg_11539" s="T489">n-n:case.poss</ta>
            <ta e="T491" id="Seg_11540" s="T490">v-v:tense-v:pn</ta>
            <ta e="T492" id="Seg_11541" s="T491">refl-n:case.poss</ta>
            <ta e="T493" id="Seg_11542" s="T492">v-v:tense-v:pn</ta>
            <ta e="T494" id="Seg_11543" s="T493">n-n:case.poss</ta>
            <ta e="T495" id="Seg_11544" s="T494">conj</ta>
            <ta e="T496" id="Seg_11545" s="T495">n-n:case</ta>
            <ta e="T497" id="Seg_11546" s="T496">v-v:tense-v:pn</ta>
            <ta e="T498" id="Seg_11547" s="T497">v-v&gt;v-v:pn</ta>
            <ta e="T499" id="Seg_11548" s="T498">adv</ta>
            <ta e="T500" id="Seg_11549" s="T499">n-n:num-n:case.poss</ta>
            <ta e="T501" id="Seg_11550" s="T500">v-v:tense-v:pn</ta>
            <ta e="T502" id="Seg_11551" s="T501">ptcl</ta>
            <ta e="T503" id="Seg_11552" s="T502">que=ptcl</ta>
            <ta e="T504" id="Seg_11553" s="T503">n-n:case</ta>
            <ta e="T505" id="Seg_11554" s="T504">n-n:case</ta>
            <ta e="T506" id="Seg_11555" s="T505">adj-n:case</ta>
            <ta e="T507" id="Seg_11556" s="T506">n-n:case.poss</ta>
            <ta e="T508" id="Seg_11557" s="T507">adj-n:case</ta>
            <ta e="T509" id="Seg_11558" s="T508">v-v:tense-v:pn</ta>
            <ta e="T510" id="Seg_11559" s="T509">adj</ta>
            <ta e="T511" id="Seg_11560" s="T510">n-n:case</ta>
            <ta e="T512" id="Seg_11561" s="T511">n-n:case</ta>
            <ta e="T513" id="Seg_11562" s="T512">n-n:case</ta>
            <ta e="T514" id="Seg_11563" s="T513">n-n:case</ta>
            <ta e="T515" id="Seg_11564" s="T514">conj</ta>
            <ta e="T516" id="Seg_11565" s="T515">n-n:case.poss</ta>
            <ta e="T517" id="Seg_11566" s="T516">v-v:tense-v:pn</ta>
            <ta e="T518" id="Seg_11567" s="T517">v-v:n.fin</ta>
            <ta e="T519" id="Seg_11568" s="T518">v-v:tense-v:pn</ta>
            <ta e="T520" id="Seg_11569" s="T519">adv</ta>
            <ta e="T521" id="Seg_11570" s="T520">adv</ta>
            <ta e="T522" id="Seg_11571" s="T521">dempro-n:case</ta>
            <ta e="T523" id="Seg_11572" s="T522">n-n:case</ta>
            <ta e="T524" id="Seg_11573" s="T523">v-v:tense-v:pn</ta>
            <ta e="T525" id="Seg_11574" s="T524">quant</ta>
            <ta e="T526" id="Seg_11575" s="T525">n-n:case</ta>
            <ta e="T527" id="Seg_11576" s="T526">n-n:num</ta>
            <ta e="T528" id="Seg_11577" s="T527">v-v:tense-v:pn</ta>
            <ta e="T529" id="Seg_11578" s="T528">quant</ta>
            <ta e="T530" id="Seg_11579" s="T529">que-n:case</ta>
            <ta e="T531" id="Seg_11580" s="T530">adv</ta>
            <ta e="T532" id="Seg_11581" s="T531">v-v:tense-v:pn</ta>
            <ta e="T533" id="Seg_11582" s="T532">v-v:tense-v:pn</ta>
            <ta e="T534" id="Seg_11583" s="T533">adv</ta>
            <ta e="T535" id="Seg_11584" s="T534">v-v:mood.pn</ta>
            <ta e="T536" id="Seg_11585" s="T535">quant-n:case</ta>
            <ta e="T537" id="Seg_11586" s="T536">que</ta>
            <ta e="T538" id="Seg_11587" s="T537">ptcl</ta>
            <ta e="T539" id="Seg_11588" s="T538">v-v:tense-v:pn</ta>
            <ta e="T540" id="Seg_11589" s="T539">n-n:case.poss</ta>
            <ta e="T542" id="Seg_11590" s="T541">v&gt;v-v-v:tense-v:pn</ta>
            <ta e="T543" id="Seg_11591" s="T542">adv</ta>
            <ta e="T544" id="Seg_11592" s="T543">dempro-n:num</ta>
            <ta e="T546" id="Seg_11593" s="T545">adv</ta>
            <ta e="T547" id="Seg_11594" s="T546">n-n:case.poss</ta>
            <ta e="T548" id="Seg_11595" s="T547">quant-n:case</ta>
            <ta e="T549" id="Seg_11596" s="T548">v-v:n.fin</ta>
            <ta e="T550" id="Seg_11597" s="T549">v-v:tense-v:pn</ta>
            <ta e="T551" id="Seg_11598" s="T550">v-v:tense-v:pn</ta>
            <ta e="T552" id="Seg_11599" s="T551">dempro-n:num</ta>
            <ta e="T553" id="Seg_11600" s="T552">v-v:tense-v:pn</ta>
            <ta e="T555" id="Seg_11601" s="T554">ptcl</ta>
            <ta e="T556" id="Seg_11602" s="T555">v-v&gt;v-v:pn</ta>
            <ta e="T558" id="Seg_11603" s="T557">n.[n:case]</ta>
            <ta e="T559" id="Seg_11604" s="T558">v-v&gt;v-v:pn</ta>
            <ta e="T560" id="Seg_11605" s="T559">adv</ta>
            <ta e="T561" id="Seg_11606" s="T560">v-v:tense-v:pn</ta>
            <ta e="T562" id="Seg_11607" s="T561">dempro-n:case</ta>
            <ta e="T563" id="Seg_11608" s="T562">adj</ta>
            <ta e="T564" id="Seg_11609" s="T563">n-n:case.poss</ta>
            <ta e="T565" id="Seg_11610" s="T564">dempro-n:case</ta>
            <ta e="T566" id="Seg_11611" s="T565">dempro-n:case</ta>
            <ta e="T567" id="Seg_11612" s="T566">ptcl</ta>
            <ta e="T568" id="Seg_11613" s="T567">adj-n:case</ta>
            <ta e="T569" id="Seg_11614" s="T568">v-v&gt;v-v:pn</ta>
            <ta e="T570" id="Seg_11615" s="T569">conj</ta>
            <ta e="T571" id="Seg_11616" s="T570">n-n:case.poss</ta>
            <ta e="T572" id="Seg_11617" s="T571">adj-n:case</ta>
            <ta e="T573" id="Seg_11618" s="T572">n-n:case</ta>
            <ta e="T574" id="Seg_11619" s="T573">v-v&gt;v-v:n.fin-n:case</ta>
            <ta e="T575" id="Seg_11620" s="T574">v-v&gt;v-v:pn</ta>
            <ta e="T576" id="Seg_11621" s="T575">que-n:case</ta>
            <ta e="T577" id="Seg_11622" s="T576">pers</ta>
            <ta e="T578" id="Seg_11623" s="T577">n-n:case.poss</ta>
            <ta e="T580" id="Seg_11624" s="T579">v-v:tense-v:pn</ta>
            <ta e="T581" id="Seg_11625" s="T580">ptcl</ta>
            <ta e="T582" id="Seg_11626" s="T581">ptcl</ta>
            <ta e="T583" id="Seg_11627" s="T582">adv</ta>
            <ta e="T584" id="Seg_11628" s="T583">n-n:case</ta>
            <ta e="T585" id="Seg_11629" s="T584">v-v&gt;v-v:pn</ta>
            <ta e="T586" id="Seg_11630" s="T585">adv</ta>
            <ta e="T587" id="Seg_11631" s="T586">v-v:tense-v:pn</ta>
            <ta e="T588" id="Seg_11632" s="T587">n-n:case.poss</ta>
            <ta e="T589" id="Seg_11633" s="T588">v-v:tense-v:pn</ta>
            <ta e="T590" id="Seg_11634" s="T589">dempro-n:case</ta>
            <ta e="T592" id="Seg_11635" s="T591">dempro-n:case</ta>
            <ta e="T593" id="Seg_11636" s="T592">pers</ta>
            <ta e="T594" id="Seg_11637" s="T593">n-n:case.poss</ta>
            <ta e="T595" id="Seg_11638" s="T594">dempro-n:case</ta>
            <ta e="T596" id="Seg_11639" s="T595">dempro-n:case</ta>
            <ta e="T598" id="Seg_11640" s="T597">dempro-n:num</ta>
            <ta e="T599" id="Seg_11641" s="T598">adv</ta>
            <ta e="T600" id="Seg_11642" s="T599">v-v:tense-v:pn</ta>
            <ta e="T601" id="Seg_11643" s="T600">adj-n:case</ta>
            <ta e="T602" id="Seg_11644" s="T601">n-n:case</ta>
            <ta e="T604" id="Seg_11645" s="T603">v-v:tense-v:pn</ta>
            <ta e="T605" id="Seg_11646" s="T604">adv</ta>
            <ta e="T606" id="Seg_11647" s="T605">adj-n:case</ta>
            <ta e="T607" id="Seg_11648" s="T606">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T608" id="Seg_11649" s="T607">adv</ta>
            <ta e="T609" id="Seg_11650" s="T608">adv</ta>
            <ta e="T610" id="Seg_11651" s="T609">ptcl</ta>
            <ta e="T611" id="Seg_11652" s="T610">n.[n:case]</ta>
            <ta e="T612" id="Seg_11653" s="T611">v-v:n.fin</ta>
            <ta e="T613" id="Seg_11654" s="T612">v-v:n.fin</ta>
            <ta e="T614" id="Seg_11655" s="T613">n-n:num-n:case.poss</ta>
            <ta e="T615" id="Seg_11656" s="T614">ptcl</ta>
            <ta e="T616" id="Seg_11657" s="T615">v-v&gt;v-v:pn</ta>
            <ta e="T617" id="Seg_11658" s="T616">que</ta>
            <ta e="T618" id="Seg_11659" s="T617">adj-n:case</ta>
            <ta e="T619" id="Seg_11660" s="T618">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T620" id="Seg_11661" s="T619">conj</ta>
            <ta e="T621" id="Seg_11662" s="T620">adv</ta>
            <ta e="T622" id="Seg_11663" s="T621">pers</ta>
            <ta e="T623" id="Seg_11664" s="T622">v-v:tense-v:pn</ta>
            <ta e="T624" id="Seg_11665" s="T623">n.[n:case]</ta>
            <ta e="T625" id="Seg_11666" s="T624">ptcl</ta>
            <ta e="T626" id="Seg_11667" s="T625">v-v&gt;v-v:pn</ta>
            <ta e="T627" id="Seg_11668" s="T626">v-v:tense-v:pn</ta>
            <ta e="T628" id="Seg_11669" s="T627">pers</ta>
            <ta e="T629" id="Seg_11670" s="T628">n-n:case</ta>
            <ta e="T634" id="Seg_11671" s="T633">v-v:tense-v:pn</ta>
            <ta e="T635" id="Seg_11672" s="T634">num-num&gt;num</ta>
            <ta e="T636" id="Seg_11673" s="T635">n-n:case</ta>
            <ta e="T637" id="Seg_11674" s="T636">conj</ta>
            <ta e="T638" id="Seg_11675" s="T637">n-n:case</ta>
            <ta e="T639" id="Seg_11676" s="T638">n-n:case</ta>
            <ta e="T640" id="Seg_11677" s="T639">conj</ta>
            <ta e="T641" id="Seg_11678" s="T640">n-n:case</ta>
            <ta e="T642" id="Seg_11679" s="T641">adv</ta>
            <ta e="T643" id="Seg_11680" s="T642">v-v:n.fin</ta>
            <ta e="T644" id="Seg_11681" s="T643">v-v:tense-v:pn</ta>
            <ta e="T645" id="Seg_11682" s="T644">conj</ta>
            <ta e="T646" id="Seg_11683" s="T645">n-n:case</ta>
            <ta e="T647" id="Seg_11684" s="T646">adv</ta>
            <ta e="T648" id="Seg_11685" s="T647">v-v&gt;v-v:pn</ta>
            <ta e="T649" id="Seg_11686" s="T648">v-v&gt;v-v:pn</ta>
            <ta e="T650" id="Seg_11687" s="T649">conj</ta>
            <ta e="T651" id="Seg_11688" s="T650">n-n:case</ta>
            <ta e="T652" id="Seg_11689" s="T651">v-v&gt;v-v:pn</ta>
            <ta e="T653" id="Seg_11690" s="T652">que-n:case=ptcl</ta>
            <ta e="T654" id="Seg_11691" s="T653">v-v&gt;v-v:pn</ta>
            <ta e="T655" id="Seg_11692" s="T654">pers</ta>
            <ta e="T656" id="Seg_11693" s="T655">v-v:tense-v:pn</ta>
            <ta e="T657" id="Seg_11694" s="T656">n-n:case</ta>
            <ta e="T658" id="Seg_11695" s="T657">n-n:case</ta>
            <ta e="T659" id="Seg_11696" s="T658">v-v:n.fin</ta>
            <ta e="T660" id="Seg_11697" s="T659">adv</ta>
            <ta e="T661" id="Seg_11698" s="T660">ptcl</ta>
            <ta e="T662" id="Seg_11699" s="T661">v-v:ins-v:mood.pn</ta>
            <ta e="T663" id="Seg_11700" s="T662">dempro-n:case</ta>
            <ta e="T664" id="Seg_11701" s="T663">v-v:tense-v:pn</ta>
            <ta e="T665" id="Seg_11702" s="T664">conj</ta>
            <ta e="T666" id="Seg_11703" s="T665">dempro-n:num</ta>
            <ta e="T667" id="Seg_11704" s="T666">v-v:tense-v:pn</ta>
            <ta e="T668" id="Seg_11705" s="T667">n-n:case</ta>
            <ta e="T669" id="Seg_11706" s="T668">v-v:tense-v:pn</ta>
            <ta e="T670" id="Seg_11707" s="T669">v-v:tense-v:pn</ta>
            <ta e="T671" id="Seg_11708" s="T670">n-n:case</ta>
            <ta e="T672" id="Seg_11709" s="T671">dempro-n:case</ta>
            <ta e="T673" id="Seg_11710" s="T672">v-v:tense-v:pn</ta>
            <ta e="T674" id="Seg_11711" s="T673">que-n:case</ta>
            <ta e="T675" id="Seg_11712" s="T674">adv</ta>
            <ta e="T676" id="Seg_11713" s="T675">v-v:tense-v:pn</ta>
            <ta e="T677" id="Seg_11714" s="T676">n-n:case</ta>
            <ta e="T678" id="Seg_11715" s="T677">v-v:tense-v:pn</ta>
            <ta e="T679" id="Seg_11716" s="T678">adv</ta>
            <ta e="T680" id="Seg_11717" s="T679">adv</ta>
            <ta e="T681" id="Seg_11718" s="T680">n-n:case</ta>
            <ta e="T682" id="Seg_11719" s="T681">adv</ta>
            <ta e="T683" id="Seg_11720" s="T682">v-v&gt;v-v:pn</ta>
            <ta e="T684" id="Seg_11721" s="T683">n-n:case</ta>
            <ta e="T685" id="Seg_11722" s="T684">que-n:case</ta>
            <ta e="T686" id="Seg_11723" s="T685">v-v&gt;v-v:pn</ta>
            <ta e="T687" id="Seg_11724" s="T686">conj</ta>
            <ta e="T688" id="Seg_11725" s="T687">pers</ta>
            <ta e="T689" id="Seg_11726" s="T688">v-v&gt;v-v:pn</ta>
            <ta e="T690" id="Seg_11727" s="T689">n-n:case</ta>
            <ta e="T691" id="Seg_11728" s="T690">v-v:n.fin</ta>
            <ta e="T692" id="Seg_11729" s="T691">ptcl</ta>
            <ta e="T693" id="Seg_11730" s="T692">v-v:ins-v:mood.pn</ta>
            <ta e="T694" id="Seg_11731" s="T693">dempro-n:case</ta>
            <ta e="T695" id="Seg_11732" s="T694">v-v:tense-v:pn</ta>
            <ta e="T696" id="Seg_11733" s="T695">v-v:tense-v:pn</ta>
            <ta e="T697" id="Seg_11734" s="T696">v-v:tense-v:pn</ta>
            <ta e="T698" id="Seg_11735" s="T697">adv</ta>
            <ta e="T699" id="Seg_11736" s="T698">v-v:tense-v:pn</ta>
            <ta e="T700" id="Seg_11737" s="T699">que-n:case</ta>
            <ta e="T701" id="Seg_11738" s="T700">adv</ta>
            <ta e="T702" id="Seg_11739" s="T701">v-v:tense-v:pn</ta>
            <ta e="T703" id="Seg_11740" s="T702">n-n:case</ta>
            <ta e="T704" id="Seg_11741" s="T703">v-v:tense-v:pn</ta>
            <ta e="T705" id="Seg_11742" s="T704">adv</ta>
            <ta e="T706" id="Seg_11743" s="T705">num-num&gt;num-n:case</ta>
            <ta e="T707" id="Seg_11744" s="T706">n-n:case</ta>
            <ta e="T708" id="Seg_11745" s="T707">adv</ta>
            <ta e="T709" id="Seg_11746" s="T708">v-v&gt;v-v:pn</ta>
            <ta e="T710" id="Seg_11747" s="T709">adv</ta>
            <ta e="T711" id="Seg_11748" s="T710">que-n:case=ptcl</ta>
            <ta e="T712" id="Seg_11749" s="T711">v-v&gt;v-v:pn</ta>
            <ta e="T713" id="Seg_11750" s="T712">conj</ta>
            <ta e="T714" id="Seg_11751" s="T713">pers</ta>
            <ta e="T715" id="Seg_11752" s="T714">v-v&gt;v-v:pn</ta>
            <ta e="T716" id="Seg_11753" s="T715">ptcl</ta>
            <ta e="T717" id="Seg_11754" s="T716">v-v:ins-v:mood.pn</ta>
            <ta e="T718" id="Seg_11755" s="T717">adv</ta>
            <ta e="T719" id="Seg_11756" s="T718">dempro-n:case</ta>
            <ta e="T720" id="Seg_11757" s="T719">v-v:tense-v:pn</ta>
            <ta e="T721" id="Seg_11758" s="T720">ptcl</ta>
            <ta e="T722" id="Seg_11759" s="T721">v-v:tense-v:pn</ta>
            <ta e="T723" id="Seg_11760" s="T722">que-n:case</ta>
            <ta e="T724" id="Seg_11761" s="T723">adv</ta>
            <ta e="T725" id="Seg_11762" s="T724">dempro-n:case</ta>
            <ta e="T726" id="Seg_11763" s="T725">n-n:case</ta>
            <ta e="T727" id="Seg_11764" s="T726">v-v:tense-v:pn</ta>
            <ta e="T728" id="Seg_11765" s="T727">adv</ta>
            <ta e="T729" id="Seg_11766" s="T728">n-adv:case</ta>
            <ta e="T730" id="Seg_11767" s="T729">v-v:tense-v:pn</ta>
            <ta e="T731" id="Seg_11768" s="T730">dempro-n:case</ta>
            <ta e="T732" id="Seg_11769" s="T731">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T733" id="Seg_11770" s="T732">v-v&gt;v-v:pn</ta>
            <ta e="T734" id="Seg_11771" s="T733">v-v:mood.pn</ta>
            <ta e="T735" id="Seg_11772" s="T734">pers</ta>
            <ta e="T736" id="Seg_11773" s="T735">adj-n:num</ta>
            <ta e="T737" id="Seg_11774" s="T736">dempro-n:case</ta>
            <ta e="T738" id="Seg_11775" s="T737">v-v:tense-v:pn</ta>
            <ta e="T739" id="Seg_11776" s="T738">conj</ta>
            <ta e="T740" id="Seg_11777" s="T739">v-v:pn</ta>
            <ta e="T741" id="Seg_11778" s="T740">que-n:case=ptcl</ta>
            <ta e="T742" id="Seg_11779" s="T741">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T743" id="Seg_11780" s="T742">conj</ta>
            <ta e="T744" id="Seg_11781" s="T743">pers</ta>
            <ta e="T745" id="Seg_11782" s="T744">v-v:tense-v:pn</ta>
            <ta e="T746" id="Seg_11783" s="T745">dempro-n:case</ta>
            <ta e="T747" id="Seg_11784" s="T746">ptcl</ta>
            <ta e="T748" id="Seg_11785" s="T747">ptcl</ta>
            <ta e="T749" id="Seg_11786" s="T748">pers</ta>
            <ta e="T750" id="Seg_11787" s="T749">ptcl</ta>
            <ta e="T751" id="Seg_11788" s="T750">v-v:tense-v:pn</ta>
            <ta e="T752" id="Seg_11789" s="T751">adv</ta>
            <ta e="T753" id="Seg_11790" s="T752">ptcl</ta>
            <ta e="T754" id="Seg_11791" s="T753">n-n:case</ta>
            <ta e="T755" id="Seg_11792" s="T754">v-v:pn</ta>
            <ta e="T756" id="Seg_11793" s="T755">que</ta>
            <ta e="T757" id="Seg_11794" s="T756">v-v:tense-v:pn</ta>
            <ta e="T758" id="Seg_11795" s="T757">adv</ta>
            <ta e="T759" id="Seg_11796" s="T758">adj-n:case</ta>
            <ta e="T760" id="Seg_11797" s="T759">n-n:case.poss</ta>
            <ta e="T761" id="Seg_11798" s="T760">v-v:tense-v:pn</ta>
            <ta e="T762" id="Seg_11799" s="T761">adv</ta>
            <ta e="T763" id="Seg_11800" s="T762">v-v:tense-v:pn</ta>
            <ta e="T764" id="Seg_11801" s="T763">refl-n:case.poss</ta>
            <ta e="T765" id="Seg_11802" s="T764">n-n:case.poss</ta>
            <ta e="T766" id="Seg_11803" s="T765">adj-n:case</ta>
            <ta e="T767" id="Seg_11804" s="T766">v-v:tense-v:pn</ta>
            <ta e="T768" id="Seg_11805" s="T767">dempro-n:case</ta>
            <ta e="T769" id="Seg_11806" s="T768">ptcl</ta>
            <ta e="T770" id="Seg_11807" s="T769">dempro-n:case</ta>
            <ta e="T771" id="Seg_11808" s="T770">n-n:case.poss</ta>
            <ta e="T772" id="Seg_11809" s="T771">v-v:n.fin</ta>
            <ta e="T773" id="Seg_11810" s="T772">v-v:mood.pn</ta>
            <ta e="T774" id="Seg_11811" s="T773">pers</ta>
            <ta e="T776" id="Seg_11812" s="T775">v-v:tense-v:pn</ta>
            <ta e="T777" id="Seg_11813" s="T776">adv</ta>
            <ta e="T778" id="Seg_11814" s="T777">dempro-n:case</ta>
            <ta e="T779" id="Seg_11815" s="T778">v-v:tense-v:pn</ta>
            <ta e="T780" id="Seg_11816" s="T779">ptcl</ta>
            <ta e="T781" id="Seg_11817" s="T780">pers</ta>
            <ta e="T782" id="Seg_11818" s="T781">v-v:tense-v:pn</ta>
            <ta e="T783" id="Seg_11819" s="T782">adv</ta>
            <ta e="T784" id="Seg_11820" s="T783">quant</ta>
            <ta e="T785" id="Seg_11821" s="T784">n-n:case</ta>
            <ta e="T786" id="Seg_11822" s="T785">conj</ta>
            <ta e="T787" id="Seg_11823" s="T786">n-n:case</ta>
            <ta e="T788" id="Seg_11824" s="T787">v-v:pn</ta>
            <ta e="T789" id="Seg_11825" s="T788">que</ta>
            <ta e="T790" id="Seg_11826" s="T789">n.[n:case]</ta>
            <ta e="T791" id="Seg_11827" s="T790">v-v:n.fin</ta>
            <ta e="T792" id="Seg_11828" s="T791">n-n:case</ta>
            <ta e="T793" id="Seg_11829" s="T792">v-v:pn</ta>
            <ta e="T794" id="Seg_11830" s="T793">que-n:case</ta>
            <ta e="T795" id="Seg_11831" s="T794">n-n:case</ta>
            <ta e="T796" id="Seg_11832" s="T795">v-v:tense-v:pn</ta>
            <ta e="T797" id="Seg_11833" s="T796">conj</ta>
            <ta e="T798" id="Seg_11834" s="T797">adv</ta>
            <ta e="T799" id="Seg_11835" s="T798">pers</ta>
            <ta e="T801" id="Seg_11836" s="T800">n.[n:case]</ta>
            <ta e="T802" id="Seg_11837" s="T801">pers</ta>
            <ta e="T803" id="Seg_11838" s="T802">n-n:case</ta>
            <ta e="T804" id="Seg_11839" s="T803">v-v:tense-v:pn</ta>
            <ta e="T805" id="Seg_11840" s="T804">conj</ta>
            <ta e="T806" id="Seg_11841" s="T805">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T808" id="Seg_11842" s="T807">n-n:case</ta>
            <ta e="T809" id="Seg_11843" s="T808">adv</ta>
            <ta e="T810" id="Seg_11844" s="T809">v-v:tense-v:pn</ta>
            <ta e="T811" id="Seg_11845" s="T810">n-adv:case</ta>
            <ta e="T812" id="Seg_11846" s="T811">v-v:tense-v:pn</ta>
            <ta e="T813" id="Seg_11847" s="T812">conj</ta>
            <ta e="T814" id="Seg_11848" s="T813">v-v:tense-v:pn</ta>
            <ta e="T815" id="Seg_11849" s="T814">v-v:n.fin</ta>
            <ta e="T816" id="Seg_11850" s="T815">n-n:num-n:case.poss</ta>
            <ta e="T817" id="Seg_11851" s="T816">v-v:tense-v:pn</ta>
            <ta e="T818" id="Seg_11852" s="T817">adv</ta>
            <ta e="T819" id="Seg_11853" s="T818">v-v:tense-v:pn</ta>
            <ta e="T820" id="Seg_11854" s="T819">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T822" id="Seg_11855" s="T821">n-n:case.poss-n:case</ta>
            <ta e="T823" id="Seg_11856" s="T822">adv</ta>
            <ta e="T824" id="Seg_11857" s="T823">refl-n:case.poss</ta>
            <ta e="T825" id="Seg_11858" s="T824">n-n:case.poss</ta>
            <ta e="T826" id="Seg_11859" s="T825">v-v:tense-v:pn</ta>
            <ta e="T827" id="Seg_11860" s="T826">n-n:num</ta>
            <ta e="T828" id="Seg_11861" s="T827">v-v:tense-v:pn</ta>
            <ta e="T829" id="Seg_11862" s="T828">n-n:num-n:case</ta>
            <ta e="T830" id="Seg_11863" s="T829">v-v:tense-v:pn</ta>
            <ta e="T831" id="Seg_11864" s="T830">adv</ta>
            <ta e="T832" id="Seg_11865" s="T831">n-n:num-n:case</ta>
            <ta e="T833" id="Seg_11866" s="T832">v-v:tense-v:pn</ta>
            <ta e="T834" id="Seg_11867" s="T833">n-n:case</ta>
            <ta e="T835" id="Seg_11868" s="T834">n-n:case.poss-n:case</ta>
            <ta e="T836" id="Seg_11869" s="T835">adv</ta>
            <ta e="T837" id="Seg_11870" s="T836">adv</ta>
            <ta e="T838" id="Seg_11871" s="T837">adv</ta>
            <ta e="T839" id="Seg_11872" s="T838">pers</ta>
            <ta e="T840" id="Seg_11873" s="T839">n-n:case</ta>
            <ta e="T841" id="Seg_11874" s="T840">n-n:case</ta>
            <ta e="T842" id="Seg_11875" s="T841">v-v:tense-v:pn</ta>
            <ta e="T843" id="Seg_11876" s="T842">num-n:case</ta>
            <ta e="T844" id="Seg_11877" s="T843">n</ta>
            <ta e="T845" id="Seg_11878" s="T844">n-n:case</ta>
            <ta e="T846" id="Seg_11879" s="T845">v-v:pn</ta>
            <ta e="T847" id="Seg_11880" s="T846">dempro-n:case</ta>
            <ta e="T848" id="Seg_11881" s="T847">quant</ta>
            <ta e="T849" id="Seg_11882" s="T848">adv</ta>
            <ta e="T850" id="Seg_11883" s="T849">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T851" id="Seg_11884" s="T850">ptcl</ta>
            <ta e="T852" id="Seg_11885" s="T851">v-v:tense-v:pn</ta>
            <ta e="T853" id="Seg_11886" s="T852">n</ta>
            <ta e="T854" id="Seg_11887" s="T853">conj</ta>
            <ta e="T855" id="Seg_11888" s="T854">n-n:case</ta>
            <ta e="T857" id="Seg_11889" s="T856">dempro-n:num</ta>
            <ta e="T858" id="Seg_11890" s="T857">que-n:case=ptcl</ta>
            <ta e="T859" id="Seg_11891" s="T858">v-v:tense-v:pn</ta>
            <ta e="T860" id="Seg_11892" s="T859">v-v:tense-v:pn</ta>
            <ta e="T861" id="Seg_11893" s="T860">n-n:case</ta>
            <ta e="T862" id="Seg_11894" s="T861">n-n:num</ta>
            <ta e="T863" id="Seg_11895" s="T862">n-n:case</ta>
            <ta e="T864" id="Seg_11896" s="T863">v-v:tense-v:pn</ta>
            <ta e="T865" id="Seg_11897" s="T864">v-v:tense-v:pn</ta>
            <ta e="T866" id="Seg_11898" s="T865">v-v&gt;v-v:pn</ta>
            <ta e="T867" id="Seg_11899" s="T866">ptcl</ta>
            <ta e="T868" id="Seg_11900" s="T867">num-n:case</ta>
            <ta e="T869" id="Seg_11901" s="T868">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T870" id="Seg_11902" s="T869">n-n:case</ta>
            <ta e="T871" id="Seg_11903" s="T870">adv</ta>
            <ta e="T872" id="Seg_11904" s="T871">ptcl</ta>
            <ta e="T873" id="Seg_11905" s="T872">v-v:n.fin</ta>
            <ta e="T875" id="Seg_11906" s="T874">n-n:case</ta>
            <ta e="T876" id="Seg_11907" s="T875">v-v&gt;v-v:pn</ta>
            <ta e="T877" id="Seg_11908" s="T876">n-n:case</ta>
            <ta e="T878" id="Seg_11909" s="T877">v-v:mood.pn</ta>
            <ta e="T879" id="Seg_11910" s="T878">n-n:case.poss</ta>
            <ta e="T880" id="Seg_11911" s="T879">dempro-n:case</ta>
            <ta e="T881" id="Seg_11912" s="T880">v-v:tense-v:pn</ta>
            <ta e="T882" id="Seg_11913" s="T881">dempro-n:case</ta>
            <ta e="T883" id="Seg_11914" s="T882">adv</ta>
            <ta e="T884" id="Seg_11915" s="T883">v-v:tense-v:pn</ta>
            <ta e="T885" id="Seg_11916" s="T884">adv</ta>
            <ta e="T886" id="Seg_11917" s="T885">adv</ta>
            <ta e="T887" id="Seg_11918" s="T886">adv</ta>
            <ta e="T888" id="Seg_11919" s="T887">ptcl</ta>
            <ta e="T889" id="Seg_11920" s="T888">v</ta>
            <ta e="T890" id="Seg_11921" s="T889">dempro-n:case</ta>
            <ta e="T891" id="Seg_11922" s="T890">adv</ta>
            <ta e="T892" id="Seg_11923" s="T891">n-n:case.poss</ta>
            <ta e="T894" id="Seg_11924" s="T893">adv</ta>
            <ta e="T895" id="Seg_11925" s="T894">adv-adv&gt;adv</ta>
            <ta e="T896" id="Seg_11926" s="T895">v-v:tense-v:pn</ta>
            <ta e="T897" id="Seg_11927" s="T896">ptcl</ta>
            <ta e="T898" id="Seg_11928" s="T897">adv</ta>
            <ta e="T899" id="Seg_11929" s="T898">adv</ta>
            <ta e="T900" id="Seg_11930" s="T899">n-n:case</ta>
            <ta e="T901" id="Seg_11931" s="T900">ptcl</ta>
            <ta e="T902" id="Seg_11932" s="T901">v-v:n.fin</ta>
            <ta e="T903" id="Seg_11933" s="T902">v-v:n.fin</ta>
            <ta e="T904" id="Seg_11934" s="T903">n-n:case</ta>
            <ta e="T905" id="Seg_11935" s="T904">v-v:tense-v:pn</ta>
            <ta e="T906" id="Seg_11936" s="T905">v-v:tense-v:pn</ta>
            <ta e="T908" id="Seg_11937" s="T907">adv</ta>
            <ta e="T909" id="Seg_11938" s="T908">adv</ta>
            <ta e="T910" id="Seg_11939" s="T909">adv</ta>
            <ta e="T911" id="Seg_11940" s="T910">v-v&gt;v-v:pn</ta>
            <ta e="T912" id="Seg_11941" s="T911">n-n:case</ta>
            <ta e="T914" id="Seg_11942" s="T913">v-v:tense-v:pn</ta>
            <ta e="T915" id="Seg_11943" s="T914">adv</ta>
            <ta e="T916" id="Seg_11944" s="T915">n-n:case</ta>
            <ta e="T917" id="Seg_11945" s="T916">v-v:tense-v:pn</ta>
            <ta e="T918" id="Seg_11946" s="T917">adv</ta>
            <ta e="T919" id="Seg_11947" s="T918">n-n:case</ta>
            <ta e="T920" id="Seg_11948" s="T919">n-n:case</ta>
            <ta e="T921" id="Seg_11949" s="T920">v-v&gt;v-v:pn</ta>
            <ta e="T922" id="Seg_11950" s="T921">n-n:case.poss</ta>
            <ta e="T923" id="Seg_11951" s="T922">adj-n:case</ta>
            <ta e="T924" id="Seg_11952" s="T923">adj-n:case</ta>
            <ta e="T925" id="Seg_11953" s="T924">quant</ta>
            <ta e="T926" id="Seg_11954" s="T925">conj</ta>
            <ta e="T927" id="Seg_11955" s="T926">adv</ta>
            <ta e="T928" id="Seg_11956" s="T927">n-n:num-n:case.poss</ta>
            <ta e="T929" id="Seg_11957" s="T928">v-v:tense-v:pn</ta>
            <ta e="T930" id="Seg_11958" s="T929">dempro-n:case</ta>
            <ta e="T931" id="Seg_11959" s="T930">v-v:tense-v:pn</ta>
            <ta e="T932" id="Seg_11960" s="T931">dempro-n:case</ta>
            <ta e="T933" id="Seg_11961" s="T932">conj</ta>
            <ta e="T934" id="Seg_11962" s="T933">v-v:tense-v:pn</ta>
            <ta e="T935" id="Seg_11963" s="T934">ptcl</ta>
            <ta e="T936" id="Seg_11964" s="T935">pers</ta>
            <ta e="T937" id="Seg_11965" s="T936">n-n:case</ta>
            <ta e="T938" id="Seg_11966" s="T937">n-n:case</ta>
            <ta e="T939" id="Seg_11967" s="T938">adj-n:case</ta>
            <ta e="T940" id="Seg_11968" s="T939">n-n:case.poss</ta>
            <ta e="T941" id="Seg_11969" s="T940">dempro-n:case</ta>
            <ta e="T942" id="Seg_11970" s="T941">v-v:tense-v:pn</ta>
            <ta e="T943" id="Seg_11971" s="T942">conj</ta>
            <ta e="T944" id="Seg_11972" s="T943">ptcl</ta>
            <ta e="T945" id="Seg_11973" s="T944">n</ta>
            <ta e="T946" id="Seg_11974" s="T945">dempro-n:case</ta>
            <ta e="T947" id="Seg_11975" s="T946">n-n:case.poss</ta>
            <ta e="T948" id="Seg_11976" s="T947">v-v:n.fin</ta>
            <ta e="T949" id="Seg_11977" s="T948">adv</ta>
            <ta e="T950" id="Seg_11978" s="T949">n-n:num</ta>
            <ta e="T951" id="Seg_11979" s="T950">n-n:num</ta>
            <ta e="T952" id="Seg_11980" s="T951">n-n:num</ta>
            <ta e="T953" id="Seg_11981" s="T952">n-n:num</ta>
            <ta e="T954" id="Seg_11982" s="T953">n-n:case</ta>
            <ta e="T955" id="Seg_11983" s="T954">v-v:tense-v:pn</ta>
            <ta e="T956" id="Seg_11984" s="T955">conj</ta>
            <ta e="T957" id="Seg_11985" s="T956">n-n:case</ta>
            <ta e="T958" id="Seg_11986" s="T957">v-v:tense-v:pn</ta>
            <ta e="T959" id="Seg_11987" s="T958">adv</ta>
            <ta e="T960" id="Seg_11988" s="T959">post</ta>
            <ta e="T961" id="Seg_11989" s="T960">v-v:n.fin</ta>
            <ta e="T962" id="Seg_11990" s="T961">adv</ta>
            <ta e="T963" id="Seg_11991" s="T962">v-v:tense-v:pn</ta>
            <ta e="T964" id="Seg_11992" s="T963">dempro-n:num-n:case</ta>
            <ta e="T965" id="Seg_11993" s="T964">n-n:case</ta>
            <ta e="T966" id="Seg_11994" s="T965">v-v:mood.pn</ta>
            <ta e="T967" id="Seg_11995" s="T966">pers</ta>
            <ta e="T968" id="Seg_11996" s="T967">v-v:n.fin</ta>
            <ta e="T969" id="Seg_11997" s="T968">dempro-n:case</ta>
            <ta e="T970" id="Seg_11998" s="T969">v-v:tense-v:pn</ta>
            <ta e="T971" id="Seg_11999" s="T970">adv</ta>
            <ta e="T972" id="Seg_12000" s="T971">dempro-n:case</ta>
            <ta e="T974" id="Seg_12001" s="T973">dempro-n:case</ta>
            <ta e="T976" id="Seg_12002" s="T975">n-n:case</ta>
            <ta e="T977" id="Seg_12003" s="T976">adv</ta>
            <ta e="T978" id="Seg_12004" s="T977">n-n:case.poss</ta>
            <ta e="T979" id="Seg_12005" s="T978">ptcl</ta>
            <ta e="T980" id="Seg_12006" s="T979">ptcl</ta>
            <ta e="T981" id="Seg_12007" s="T980">n-n:case.poss</ta>
            <ta e="T982" id="Seg_12008" s="T981">v-v:n.fin</ta>
            <ta e="T983" id="Seg_12009" s="T982">n-n:num</ta>
            <ta e="T984" id="Seg_12010" s="T983">n-n:num</ta>
            <ta e="T985" id="Seg_12011" s="T984">dempro-n:case</ta>
            <ta e="T986" id="Seg_12012" s="T985">v-v:tense-v:pn</ta>
            <ta e="T987" id="Seg_12013" s="T986">v-v:mood.pn</ta>
            <ta e="T988" id="Seg_12014" s="T987">pers</ta>
            <ta e="T989" id="Seg_12015" s="T988">ptcl</ta>
            <ta e="T990" id="Seg_12016" s="T989">pers</ta>
            <ta e="T991" id="Seg_12017" s="T990">ptcl</ta>
            <ta e="T992" id="Seg_12018" s="T991">v-v:tense-v:pn</ta>
            <ta e="T993" id="Seg_12019" s="T992">ptcl</ta>
            <ta e="T995" id="Seg_12020" s="T994">pers</ta>
            <ta e="T996" id="Seg_12021" s="T995">v-v:n.fin</ta>
            <ta e="T997" id="Seg_12022" s="T996">que-n:case</ta>
            <ta e="T998" id="Seg_12023" s="T997">adv</ta>
            <ta e="T999" id="Seg_12024" s="T998">dempro-n:case</ta>
            <ta e="T1000" id="Seg_12025" s="T999">n-n:case</ta>
            <ta e="T1001" id="Seg_12026" s="T1000">v-v:tense-v:pn</ta>
            <ta e="T1002" id="Seg_12027" s="T1001">conj</ta>
            <ta e="T1003" id="Seg_12028" s="T1002">v-v:n.fin</ta>
            <ta e="T1004" id="Seg_12029" s="T1003">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1005" id="Seg_12030" s="T1004">conj</ta>
            <ta e="T1006" id="Seg_12031" s="T1005">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1007" id="Seg_12032" s="T1006">n-n:case</ta>
            <ta e="T1008" id="Seg_12033" s="T1007">dempro-n:num</ta>
            <ta e="T1009" id="Seg_12034" s="T1008">v-v:tense-v:pn</ta>
            <ta e="T1010" id="Seg_12035" s="T1009">v-v:pn</ta>
            <ta e="T1011" id="Seg_12036" s="T1010">n-n:case.poss</ta>
            <ta e="T1012" id="Seg_12037" s="T1011">adv</ta>
            <ta e="T1013" id="Seg_12038" s="T1012">n-n:case</ta>
            <ta e="T1014" id="Seg_12039" s="T1013">n-n:case</ta>
            <ta e="T1015" id="Seg_12040" s="T1014">aux-v:mood.pn</ta>
            <ta e="T1016" id="Seg_12041" s="T1015">v-v:mood.pn</ta>
            <ta e="T1017" id="Seg_12042" s="T1016">pers</ta>
            <ta e="T1018" id="Seg_12043" s="T1017">v-v:tense-v:pn</ta>
            <ta e="T1020" id="Seg_12044" s="T1019">v-v:tense-v:pn</ta>
            <ta e="T1021" id="Seg_12045" s="T1020">adv</ta>
            <ta e="T1022" id="Seg_12046" s="T1021">pers</ta>
            <ta e="T1023" id="Seg_12047" s="T1022">v-v:tense-v:pn</ta>
            <ta e="T1024" id="Seg_12048" s="T1023">v-v:tense-v:pn</ta>
            <ta e="T1025" id="Seg_12049" s="T1024">v-v:tense-v:pn</ta>
            <ta e="T1026" id="Seg_12050" s="T1025">v-v:tense-v:pn</ta>
            <ta e="T1027" id="Seg_12051" s="T1026">dempro-n:case</ta>
            <ta e="T1028" id="Seg_12052" s="T1027">n-n:case</ta>
            <ta e="T1029" id="Seg_12053" s="T1028">que</ta>
            <ta e="T1030" id="Seg_12054" s="T1029">v-v:tense-v:pn</ta>
            <ta e="T1031" id="Seg_12055" s="T1030">v-v:mood.pn</ta>
            <ta e="T1032" id="Seg_12056" s="T1031">pers</ta>
            <ta e="T1033" id="Seg_12057" s="T1032">v-v:ins-v:n.fin</ta>
            <ta e="T1034" id="Seg_12058" s="T1033">pers</ta>
            <ta e="T1035" id="Seg_12059" s="T1034">n-n:case.poss</ta>
            <ta e="T1036" id="Seg_12060" s="T1035">dempro-n:case</ta>
            <ta e="T1037" id="Seg_12061" s="T1036">v-v:tense-v:pn</ta>
            <ta e="T1038" id="Seg_12062" s="T1037">adv</ta>
            <ta e="T1039" id="Seg_12063" s="T1038">v-v:tense-v:pn</ta>
            <ta e="T1040" id="Seg_12064" s="T1039">v-v:tense-v:pn</ta>
            <ta e="T1041" id="Seg_12065" s="T1040">n-n:case</ta>
            <ta e="T1042" id="Seg_12066" s="T1041">que</ta>
            <ta e="T1043" id="Seg_12067" s="T1042">v-v:tense-v:pn</ta>
            <ta e="T1044" id="Seg_12068" s="T1043">n-n:case.poss</ta>
            <ta e="T1045" id="Seg_12069" s="T1044">v-v:n.fin</ta>
            <ta e="T1047" id="Seg_12070" s="T1046">v-v:mood.pn</ta>
            <ta e="T1048" id="Seg_12071" s="T1047">pers</ta>
            <ta e="T1049" id="Seg_12072" s="T1048">v-v:ins-v:mood.pn</ta>
            <ta e="T1050" id="Seg_12073" s="T1049">pers</ta>
            <ta e="T1051" id="Seg_12074" s="T1050">n-n:case.poss</ta>
            <ta e="T1052" id="Seg_12075" s="T1051">dempro-n:case</ta>
            <ta e="T1053" id="Seg_12076" s="T1052">v-v:tense-v:pn</ta>
            <ta e="T1054" id="Seg_12077" s="T1053">adv</ta>
            <ta e="T1055" id="Seg_12078" s="T1054">v-v:tense-v:pn</ta>
            <ta e="T1056" id="Seg_12079" s="T1055">n-n:case</ta>
            <ta e="T1057" id="Seg_12080" s="T1056">v-v:tense-v:pn</ta>
            <ta e="T1058" id="Seg_12081" s="T1057">v-v:ins-v:mood.pn</ta>
            <ta e="T1059" id="Seg_12082" s="T1058">pers</ta>
            <ta e="T1060" id="Seg_12083" s="T1059">adv</ta>
            <ta e="T1061" id="Seg_12084" s="T1060">v-v:tense-v:pn</ta>
            <ta e="T1062" id="Seg_12085" s="T1061">v</ta>
            <ta e="T1063" id="Seg_12086" s="T1062">v-v:n.fin</ta>
            <ta e="T1065" id="Seg_12087" s="T1064">v-v:mood.pn</ta>
            <ta e="T1066" id="Seg_12088" s="T1065">pers</ta>
            <ta e="T1067" id="Seg_12089" s="T1066">n-n:case.poss</ta>
            <ta e="T1068" id="Seg_12090" s="T1067">v-v:mood.pn</ta>
            <ta e="T1069" id="Seg_12091" s="T1068">pers</ta>
            <ta e="T1070" id="Seg_12092" s="T1069">n-n:case.poss</ta>
            <ta e="T1071" id="Seg_12093" s="T1070">dempro-n:case</ta>
            <ta e="T1072" id="Seg_12094" s="T1071">v-v&gt;v-v:pn</ta>
            <ta e="T1074" id="Seg_12095" s="T1073">v-v:mood.pn</ta>
            <ta e="T1075" id="Seg_12096" s="T1074">dempro-n:case</ta>
            <ta e="T1076" id="Seg_12097" s="T1075">conj</ta>
            <ta e="T1077" id="Seg_12098" s="T1076">n-n:case.poss-n:case</ta>
            <ta e="T1078" id="Seg_12099" s="T1077">ptcl</ta>
            <ta e="T1079" id="Seg_12100" s="T1078">n-n:num</ta>
            <ta e="T1080" id="Seg_12101" s="T1079">v-v&gt;v-v:pn</ta>
            <ta e="T1081" id="Seg_12102" s="T1080">n-n:case.poss</ta>
            <ta e="T1082" id="Seg_12103" s="T1081">dempro-n:case</ta>
            <ta e="T1083" id="Seg_12104" s="T1082">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1084" id="Seg_12105" s="T1083">conj</ta>
            <ta e="T1085" id="Seg_12106" s="T1084">dempro-n:case</ta>
            <ta e="T1086" id="Seg_12107" s="T1085">v-v&gt;v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PKZ">
            <ta e="T5" id="Seg_12108" s="T4">v</ta>
            <ta e="T6" id="Seg_12109" s="T5">n</ta>
            <ta e="T7" id="Seg_12110" s="T6">dempro</ta>
            <ta e="T8" id="Seg_12111" s="T7">num</ta>
            <ta e="T9" id="Seg_12112" s="T8">n</ta>
            <ta e="T10" id="Seg_12113" s="T9">v</ta>
            <ta e="T11" id="Seg_12114" s="T10">num</ta>
            <ta e="T12" id="Seg_12115" s="T11">propr</ta>
            <ta e="T13" id="Seg_12116" s="T12">num</ta>
            <ta e="T14" id="Seg_12117" s="T13">propr</ta>
            <ta e="T15" id="Seg_12118" s="T14">num</ta>
            <ta e="T16" id="Seg_12119" s="T15">propr</ta>
            <ta e="T17" id="Seg_12120" s="T16">n</ta>
            <ta e="T18" id="Seg_12121" s="T17">adv</ta>
            <ta e="T20" id="Seg_12122" s="T19">dempro</ta>
            <ta e="T21" id="Seg_12123" s="T20">n</ta>
            <ta e="T22" id="Seg_12124" s="T21">v</ta>
            <ta e="T23" id="Seg_12125" s="T22">n</ta>
            <ta e="T24" id="Seg_12126" s="T23">conj</ta>
            <ta e="T25" id="Seg_12127" s="T24">dempro</ta>
            <ta e="T26" id="Seg_12128" s="T25">que</ta>
            <ta e="T29" id="Seg_12129" s="T28">n</ta>
            <ta e="T30" id="Seg_12130" s="T29">v</ta>
            <ta e="T31" id="Seg_12131" s="T30">conj</ta>
            <ta e="T32" id="Seg_12132" s="T31">v</ta>
            <ta e="T33" id="Seg_12133" s="T32">n</ta>
            <ta e="T34" id="Seg_12134" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_12135" s="T34">v</ta>
            <ta e="T36" id="Seg_12136" s="T35">v</ta>
            <ta e="T38" id="Seg_12137" s="T37">v</ta>
            <ta e="T39" id="Seg_12138" s="T38">que</ta>
            <ta e="T40" id="Seg_12139" s="T39">v</ta>
            <ta e="T41" id="Seg_12140" s="T40">n</ta>
            <ta e="T42" id="Seg_12141" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_12142" s="T42">v</ta>
            <ta e="T44" id="Seg_12143" s="T43">adv</ta>
            <ta e="T45" id="Seg_12144" s="T44">v</ta>
            <ta e="T46" id="Seg_12145" s="T45">adj</ta>
            <ta e="T47" id="Seg_12146" s="T46">n</ta>
            <ta e="T48" id="Seg_12147" s="T47">propr</ta>
            <ta e="T49" id="Seg_12148" s="T48">adv</ta>
            <ta e="T50" id="Seg_12149" s="T49">v</ta>
            <ta e="T51" id="Seg_12150" s="T50">v</ta>
            <ta e="T52" id="Seg_12151" s="T51">n</ta>
            <ta e="T53" id="Seg_12152" s="T52">conj</ta>
            <ta e="T54" id="Seg_12153" s="T53">v</ta>
            <ta e="T55" id="Seg_12154" s="T54">n</ta>
            <ta e="T56" id="Seg_12155" s="T55">v</ta>
            <ta e="T57" id="Seg_12156" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_12157" s="T57">v</ta>
            <ta e="T59" id="Seg_12158" s="T58">conj</ta>
            <ta e="T60" id="Seg_12159" s="T59">que</ta>
            <ta e="T61" id="Seg_12160" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_12161" s="T61">v</ta>
            <ta e="T63" id="Seg_12162" s="T62">adv</ta>
            <ta e="T64" id="Seg_12163" s="T63">adj</ta>
            <ta e="T65" id="Seg_12164" s="T64">n</ta>
            <ta e="T66" id="Seg_12165" s="T65">v</ta>
            <ta e="T67" id="Seg_12166" s="T66">propr</ta>
            <ta e="T68" id="Seg_12167" s="T67">v</ta>
            <ta e="T69" id="Seg_12168" s="T68">v</ta>
            <ta e="T70" id="Seg_12169" s="T69">n</ta>
            <ta e="T71" id="Seg_12170" s="T70">adv</ta>
            <ta e="T72" id="Seg_12171" s="T71">n</ta>
            <ta e="T73" id="Seg_12172" s="T72">v</ta>
            <ta e="T74" id="Seg_12173" s="T73">quant</ta>
            <ta e="T75" id="Seg_12174" s="T74">n</ta>
            <ta e="T76" id="Seg_12175" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_12176" s="T76">v</ta>
            <ta e="T78" id="Seg_12177" s="T77">conj</ta>
            <ta e="T79" id="Seg_12178" s="T78">que</ta>
            <ta e="T80" id="Seg_12179" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_12180" s="T80">v</ta>
            <ta e="T82" id="Seg_12181" s="T81">adv</ta>
            <ta e="T83" id="Seg_12182" s="T82">propr</ta>
            <ta e="T84" id="Seg_12183" s="T83">v</ta>
            <ta e="T85" id="Seg_12184" s="T84">v</ta>
            <ta e="T86" id="Seg_12185" s="T85">n</ta>
            <ta e="T87" id="Seg_12186" s="T86">adv</ta>
            <ta e="T88" id="Seg_12187" s="T87">v</ta>
            <ta e="T89" id="Seg_12188" s="T88">n</ta>
            <ta e="T90" id="Seg_12189" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_12190" s="T90">v</ta>
            <ta e="T92" id="Seg_12191" s="T91">v</ta>
            <ta e="T93" id="Seg_12192" s="T92">v</ta>
            <ta e="T94" id="Seg_12193" s="T93">n</ta>
            <ta e="T95" id="Seg_12194" s="T94">v</ta>
            <ta e="T96" id="Seg_12195" s="T95">adj</ta>
            <ta e="T97" id="Seg_12196" s="T96">conj</ta>
            <ta e="T98" id="Seg_12197" s="T97">ptcl</ta>
            <ta e="T99" id="Seg_12198" s="T98">adv</ta>
            <ta e="T100" id="Seg_12199" s="T99">n</ta>
            <ta e="T101" id="Seg_12200" s="T100">n</ta>
            <ta e="T102" id="Seg_12201" s="T101">adj</ta>
            <ta e="T103" id="Seg_12202" s="T102">dempro</ta>
            <ta e="T104" id="Seg_12203" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_12204" s="T104">v</ta>
            <ta e="T106" id="Seg_12205" s="T105">n</ta>
            <ta e="T107" id="Seg_12206" s="T106">dempro</ta>
            <ta e="T108" id="Seg_12207" s="T107">conj</ta>
            <ta e="T109" id="Seg_12208" s="T108">v</ta>
            <ta e="T110" id="Seg_12209" s="T109">dempro</ta>
            <ta e="T111" id="Seg_12210" s="T110">v</ta>
            <ta e="T112" id="Seg_12211" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_12212" s="T112">v</ta>
            <ta e="T114" id="Seg_12213" s="T113">v</ta>
            <ta e="T115" id="Seg_12214" s="T114">adv</ta>
            <ta e="T116" id="Seg_12215" s="T115">v</ta>
            <ta e="T117" id="Seg_12216" s="T116">pers</ta>
            <ta e="T118" id="Seg_12217" s="T117">pers</ta>
            <ta e="T119" id="Seg_12218" s="T118">pers</ta>
            <ta e="T120" id="Seg_12219" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_12220" s="T120">que</ta>
            <ta e="T122" id="Seg_12221" s="T121">v</ta>
            <ta e="T123" id="Seg_12222" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_12223" s="T123">que</ta>
            <ta e="T125" id="Seg_12224" s="T124">v</ta>
            <ta e="T126" id="Seg_12225" s="T125">adv</ta>
            <ta e="T127" id="Seg_12226" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_12227" s="T127">v</ta>
            <ta e="T129" id="Seg_12228" s="T128">n</ta>
            <ta e="T130" id="Seg_12229" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_12230" s="T130">v</ta>
            <ta e="T132" id="Seg_12231" s="T131">conj</ta>
            <ta e="T133" id="Seg_12232" s="T132">que</ta>
            <ta e="T134" id="Seg_12233" s="T133">v</ta>
            <ta e="T135" id="Seg_12234" s="T134">ptcl</ta>
            <ta e="T136" id="Seg_12235" s="T135">pers</ta>
            <ta e="T137" id="Seg_12236" s="T136">v</ta>
            <ta e="T138" id="Seg_12237" s="T137">adv</ta>
            <ta e="T139" id="Seg_12238" s="T138">v</ta>
            <ta e="T141" id="Seg_12239" s="T140">pers</ta>
            <ta e="T142" id="Seg_12240" s="T141">ptcl</ta>
            <ta e="T143" id="Seg_12241" s="T142">adj</ta>
            <ta e="T144" id="Seg_12242" s="T143">conj</ta>
            <ta e="T145" id="Seg_12243" s="T144">v</ta>
            <ta e="T146" id="Seg_12244" s="T145">v</ta>
            <ta e="T147" id="Seg_12245" s="T146">n</ta>
            <ta e="T148" id="Seg_12246" s="T147">conj</ta>
            <ta e="T149" id="Seg_12247" s="T148">dempro</ta>
            <ta e="T150" id="Seg_12248" s="T149">v</ta>
            <ta e="T151" id="Seg_12249" s="T150">n</ta>
            <ta e="T152" id="Seg_12250" s="T151">n</ta>
            <ta e="T153" id="Seg_12251" s="T152">v</ta>
            <ta e="T154" id="Seg_12252" s="T153">pers</ta>
            <ta e="T155" id="Seg_12253" s="T154">v</ta>
            <ta e="T156" id="Seg_12254" s="T155">n</ta>
            <ta e="T157" id="Seg_12255" s="T156">adv</ta>
            <ta e="T158" id="Seg_12256" s="T157">adv</ta>
            <ta e="T159" id="Seg_12257" s="T158">que</ta>
            <ta e="T160" id="Seg_12258" s="T159">ptcl</ta>
            <ta e="T161" id="Seg_12259" s="T160">v</ta>
            <ta e="T162" id="Seg_12260" s="T161">v</ta>
            <ta e="T163" id="Seg_12261" s="T162">n</ta>
            <ta e="T164" id="Seg_12262" s="T163">adv</ta>
            <ta e="T165" id="Seg_12263" s="T164">n</ta>
            <ta e="T166" id="Seg_12264" s="T165">v</ta>
            <ta e="T167" id="Seg_12265" s="T166">conj</ta>
            <ta e="T168" id="Seg_12266" s="T167">n</ta>
            <ta e="T169" id="Seg_12267" s="T168">v</ta>
            <ta e="T171" id="Seg_12268" s="T170">dempro</ta>
            <ta e="T172" id="Seg_12269" s="T171">n</ta>
            <ta e="T173" id="Seg_12270" s="T172">v</ta>
            <ta e="T174" id="Seg_12271" s="T173">n</ta>
            <ta e="T175" id="Seg_12272" s="T174">quant</ta>
            <ta e="T176" id="Seg_12273" s="T175">n</ta>
            <ta e="T177" id="Seg_12274" s="T176">v</ta>
            <ta e="T178" id="Seg_12275" s="T177">conj</ta>
            <ta e="T179" id="Seg_12276" s="T178">conj</ta>
            <ta e="T180" id="Seg_12277" s="T179">dempro</ta>
            <ta e="T181" id="Seg_12278" s="T180">n</ta>
            <ta e="T182" id="Seg_12279" s="T181">v</ta>
            <ta e="T183" id="Seg_12280" s="T182">conj</ta>
            <ta e="T184" id="Seg_12281" s="T183">dempro</ta>
            <ta e="T185" id="Seg_12282" s="T184">v</ta>
            <ta e="T186" id="Seg_12283" s="T185">v</ta>
            <ta e="T187" id="Seg_12284" s="T186">pers</ta>
            <ta e="T188" id="Seg_12285" s="T187">que</ta>
            <ta e="T189" id="Seg_12286" s="T188">pers</ta>
            <ta e="T190" id="Seg_12287" s="T189">v</ta>
            <ta e="T191" id="Seg_12288" s="T190">que</ta>
            <ta e="T192" id="Seg_12289" s="T191">adv</ta>
            <ta e="T193" id="Seg_12290" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_12291" s="T193">v</ta>
            <ta e="T195" id="Seg_12292" s="T194">conj</ta>
            <ta e="T196" id="Seg_12293" s="T195">n</ta>
            <ta e="T197" id="Seg_12294" s="T196">v</ta>
            <ta e="T199" id="Seg_12295" s="T198">pers</ta>
            <ta e="T200" id="Seg_12296" s="T199">dempro</ta>
            <ta e="T201" id="Seg_12297" s="T200">v</ta>
            <ta e="T202" id="Seg_12298" s="T201">v</ta>
            <ta e="T203" id="Seg_12299" s="T202">dempro</ta>
            <ta e="T204" id="Seg_12300" s="T203">v</ta>
            <ta e="T205" id="Seg_12301" s="T204">n</ta>
            <ta e="T206" id="Seg_12302" s="T205">v</ta>
            <ta e="T207" id="Seg_12303" s="T206">v</ta>
            <ta e="T208" id="Seg_12304" s="T207">conj</ta>
            <ta e="T209" id="Seg_12305" s="T208">ptcl</ta>
            <ta e="T210" id="Seg_12306" s="T209">v</ta>
            <ta e="T211" id="Seg_12307" s="T210">n</ta>
            <ta e="T212" id="Seg_12308" s="T211">v</ta>
            <ta e="T213" id="Seg_12309" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_12310" s="T213">adv</ta>
            <ta e="T215" id="Seg_12311" s="T214">n</ta>
            <ta e="T216" id="Seg_12312" s="T215">v</ta>
            <ta e="T217" id="Seg_12313" s="T216">dempro</ta>
            <ta e="T218" id="Seg_12314" s="T217">n</ta>
            <ta e="T219" id="Seg_12315" s="T218">v</ta>
            <ta e="T220" id="Seg_12316" s="T219">adv</ta>
            <ta e="T221" id="Seg_12317" s="T220">dempro</ta>
            <ta e="T222" id="Seg_12318" s="T221">n</ta>
            <ta e="T224" id="Seg_12319" s="T222">v</ta>
            <ta e="T225" id="Seg_12320" s="T224">v</ta>
            <ta e="T226" id="Seg_12321" s="T225">dempro</ta>
            <ta e="T227" id="Seg_12322" s="T226">v</ta>
            <ta e="T228" id="Seg_12323" s="T227">num</ta>
            <ta e="T229" id="Seg_12324" s="T228">n</ta>
            <ta e="T230" id="Seg_12325" s="T229">v</ta>
            <ta e="T231" id="Seg_12326" s="T230">num</ta>
            <ta e="T232" id="Seg_12327" s="T231">v</ta>
            <ta e="T233" id="Seg_12328" s="T232">dempro</ta>
            <ta e="T234" id="Seg_12329" s="T233">v</ta>
            <ta e="T235" id="Seg_12330" s="T234">v</ta>
            <ta e="T236" id="Seg_12331" s="T235">adj</ta>
            <ta e="T237" id="Seg_12332" s="T236">v</ta>
            <ta e="T238" id="Seg_12333" s="T237">adv</ta>
            <ta e="T239" id="Seg_12334" s="T238">dempro</ta>
            <ta e="T240" id="Seg_12335" s="T239">v</ta>
            <ta e="T241" id="Seg_12336" s="T240">dempro</ta>
            <ta e="T242" id="Seg_12337" s="T241">v</ta>
            <ta e="T243" id="Seg_12338" s="T242">v</ta>
            <ta e="T244" id="Seg_12339" s="T243">adv</ta>
            <ta e="T245" id="Seg_12340" s="T244">n</ta>
            <ta e="T246" id="Seg_12341" s="T245">quant</ta>
            <ta e="T247" id="Seg_12342" s="T246">v</ta>
            <ta e="T248" id="Seg_12343" s="T247">conj</ta>
            <ta e="T249" id="Seg_12344" s="T248">que</ta>
            <ta e="T250" id="Seg_12345" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_12346" s="T250">dempro</ta>
            <ta e="T252" id="Seg_12347" s="T251">n</ta>
            <ta e="T253" id="Seg_12348" s="T252">ptcl</ta>
            <ta e="T254" id="Seg_12349" s="T253">v</ta>
            <ta e="T255" id="Seg_12350" s="T254">dempro</ta>
            <ta e="T256" id="Seg_12351" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_12352" s="T256">v</ta>
            <ta e="T258" id="Seg_12353" s="T257">v</ta>
            <ta e="T259" id="Seg_12354" s="T258">ptcl</ta>
            <ta e="T260" id="Seg_12355" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_12356" s="T260">v</ta>
            <ta e="T262" id="Seg_12357" s="T261">adv</ta>
            <ta e="T263" id="Seg_12358" s="T262">n</ta>
            <ta e="T264" id="Seg_12359" s="T263">quant</ta>
            <ta e="T265" id="Seg_12360" s="T264">v</ta>
            <ta e="T266" id="Seg_12361" s="T265">v</ta>
            <ta e="T267" id="Seg_12362" s="T266">v</ta>
            <ta e="T268" id="Seg_12363" s="T267">conj</ta>
            <ta e="T269" id="Seg_12364" s="T268">dempro</ta>
            <ta e="T270" id="Seg_12365" s="T269">v</ta>
            <ta e="T271" id="Seg_12366" s="T270">n</ta>
            <ta e="T272" id="Seg_12367" s="T271">v</ta>
            <ta e="T273" id="Seg_12368" s="T272">adv</ta>
            <ta e="T274" id="Seg_12369" s="T273">n</ta>
            <ta e="T275" id="Seg_12370" s="T274">v</ta>
            <ta e="T276" id="Seg_12371" s="T275">conj</ta>
            <ta e="T277" id="Seg_12372" s="T276">adj</ta>
            <ta e="T278" id="Seg_12373" s="T277">quant</ta>
            <ta e="T279" id="Seg_12374" s="T278">n</ta>
            <ta e="T280" id="Seg_12375" s="T279">v</ta>
            <ta e="T281" id="Seg_12376" s="T280">conj</ta>
            <ta e="T282" id="Seg_12377" s="T281">n</ta>
            <ta e="T283" id="Seg_12378" s="T282">v</ta>
            <ta e="T284" id="Seg_12379" s="T283">que</ta>
            <ta e="T285" id="Seg_12380" s="T284">adj</ta>
            <ta e="T287" id="Seg_12381" s="T286">v</ta>
            <ta e="T288" id="Seg_12382" s="T287">ptcl</ta>
            <ta e="T289" id="Seg_12383" s="T288">pers</ta>
            <ta e="T290" id="Seg_12384" s="T289">v</ta>
            <ta e="T291" id="Seg_12385" s="T290">dempro</ta>
            <ta e="T292" id="Seg_12386" s="T291">n</ta>
            <ta e="T294" id="Seg_12387" s="T293">v</ta>
            <ta e="T295" id="Seg_12388" s="T294">v</ta>
            <ta e="T296" id="Seg_12389" s="T295">adv</ta>
            <ta e="T298" id="Seg_12390" s="T297">n</ta>
            <ta e="T299" id="Seg_12391" s="T298">v</ta>
            <ta e="T300" id="Seg_12392" s="T299">ptcl</ta>
            <ta e="T301" id="Seg_12393" s="T300">v</ta>
            <ta e="T303" id="Seg_12394" s="T302">que</ta>
            <ta e="T304" id="Seg_12395" s="T303">v</ta>
            <ta e="T305" id="Seg_12396" s="T304">conj</ta>
            <ta e="T306" id="Seg_12397" s="T305">dempro</ta>
            <ta e="T307" id="Seg_12398" s="T306">v</ta>
            <ta e="T308" id="Seg_12399" s="T307">dempro</ta>
            <ta e="T309" id="Seg_12400" s="T308">pers</ta>
            <ta e="T310" id="Seg_12401" s="T309">n</ta>
            <ta e="T311" id="Seg_12402" s="T310">adv</ta>
            <ta e="T312" id="Seg_12403" s="T311">v</ta>
            <ta e="T313" id="Seg_12404" s="T312">aux</ta>
            <ta e="T314" id="Seg_12405" s="T313">v</ta>
            <ta e="T315" id="Seg_12406" s="T314">pers</ta>
            <ta e="T316" id="Seg_12407" s="T315">n</ta>
            <ta e="T317" id="Seg_12408" s="T316">v</ta>
            <ta e="T318" id="Seg_12409" s="T317">ptcl</ta>
            <ta e="T319" id="Seg_12410" s="T318">que</ta>
            <ta e="T320" id="Seg_12411" s="T319">aux</ta>
            <ta e="T321" id="Seg_12412" s="T320">v</ta>
            <ta e="T322" id="Seg_12413" s="T321">adj</ta>
            <ta e="T323" id="Seg_12414" s="T322">n</ta>
            <ta e="T324" id="Seg_12415" s="T323">v</ta>
            <ta e="T325" id="Seg_12416" s="T324">n</ta>
            <ta e="T326" id="Seg_12417" s="T325">adj</ta>
            <ta e="T327" id="Seg_12418" s="T326">v</ta>
            <ta e="T328" id="Seg_12419" s="T327">refl</ta>
            <ta e="T329" id="Seg_12420" s="T328">adv</ta>
            <ta e="T330" id="Seg_12421" s="T329">adv</ta>
            <ta e="T331" id="Seg_12422" s="T330">adv</ta>
            <ta e="T332" id="Seg_12423" s="T331">v</ta>
            <ta e="T333" id="Seg_12424" s="T332">v</ta>
            <ta e="T334" id="Seg_12425" s="T333">v</ta>
            <ta e="T335" id="Seg_12426" s="T334">dempro</ta>
            <ta e="T336" id="Seg_12427" s="T335">n</ta>
            <ta e="T338" id="Seg_12428" s="T337">v</ta>
            <ta e="T339" id="Seg_12429" s="T338">n</ta>
            <ta e="T340" id="Seg_12430" s="T339">v</ta>
            <ta e="T341" id="Seg_12431" s="T340">v</ta>
            <ta e="T342" id="Seg_12432" s="T341">v</ta>
            <ta e="T344" id="Seg_12433" s="T342">v</ta>
            <ta e="T345" id="Seg_12434" s="T344">ptcl</ta>
            <ta e="T346" id="Seg_12435" s="T345">n</ta>
            <ta e="T347" id="Seg_12436" s="T346">v</ta>
            <ta e="T348" id="Seg_12437" s="T347">n</ta>
            <ta e="T349" id="Seg_12438" s="T348">v</ta>
            <ta e="T350" id="Seg_12439" s="T349">adv</ta>
            <ta e="T351" id="Seg_12440" s="T350">num</ta>
            <ta e="T352" id="Seg_12441" s="T351">n</ta>
            <ta e="T353" id="Seg_12442" s="T352">v</ta>
            <ta e="T354" id="Seg_12443" s="T353">num</ta>
            <ta e="T355" id="Seg_12444" s="T354">v</ta>
            <ta e="T356" id="Seg_12445" s="T355">adv</ta>
            <ta e="T357" id="Seg_12446" s="T356">v</ta>
            <ta e="T359" id="Seg_12447" s="T358">v</ta>
            <ta e="T360" id="Seg_12448" s="T359">n</ta>
            <ta e="T361" id="Seg_12449" s="T360">quant</ta>
            <ta e="T362" id="Seg_12450" s="T361">v</ta>
            <ta e="T363" id="Seg_12451" s="T362">adv</ta>
            <ta e="T364" id="Seg_12452" s="T363">que</ta>
            <ta e="T365" id="Seg_12453" s="T364">ptcl</ta>
            <ta e="T366" id="Seg_12454" s="T365">v</ta>
            <ta e="T367" id="Seg_12455" s="T366">adv</ta>
            <ta e="T368" id="Seg_12456" s="T367">adv</ta>
            <ta e="T370" id="Seg_12457" s="T369">v</ta>
            <ta e="T371" id="Seg_12458" s="T370">v</ta>
            <ta e="T376" id="Seg_12459" s="T375">ptcl</ta>
            <ta e="T377" id="Seg_12460" s="T376">v</ta>
            <ta e="T378" id="Seg_12461" s="T377">adv</ta>
            <ta e="T379" id="Seg_12462" s="T378">v</ta>
            <ta e="T380" id="Seg_12463" s="T379">n</ta>
            <ta e="T381" id="Seg_12464" s="T380">quant</ta>
            <ta e="T382" id="Seg_12465" s="T381">v</ta>
            <ta e="T383" id="Seg_12466" s="T382">conj</ta>
            <ta e="T384" id="Seg_12467" s="T383">v</ta>
            <ta e="T385" id="Seg_12468" s="T384">v</ta>
            <ta e="T386" id="Seg_12469" s="T385">conj</ta>
            <ta e="T387" id="Seg_12470" s="T386">n</ta>
            <ta e="T388" id="Seg_12471" s="T387">v</ta>
            <ta e="T389" id="Seg_12472" s="T388">v</ta>
            <ta e="T390" id="Seg_12473" s="T389">v</ta>
            <ta e="T391" id="Seg_12474" s="T390">conj</ta>
            <ta e="T392" id="Seg_12475" s="T391">dempro</ta>
            <ta e="T393" id="Seg_12476" s="T392">aux</ta>
            <ta e="T394" id="Seg_12477" s="T393">v</ta>
            <ta e="T395" id="Seg_12478" s="T394">n</ta>
            <ta e="T396" id="Seg_12479" s="T395">v</ta>
            <ta e="T397" id="Seg_12480" s="T396">conj</ta>
            <ta e="T398" id="Seg_12481" s="T397">refl</ta>
            <ta e="T399" id="Seg_12482" s="T398">adv</ta>
            <ta e="T400" id="Seg_12483" s="T399">v</ta>
            <ta e="T401" id="Seg_12484" s="T400">n</ta>
            <ta e="T402" id="Seg_12485" s="T401">adv</ta>
            <ta e="T403" id="Seg_12486" s="T402">v</ta>
            <ta e="T404" id="Seg_12487" s="T403">conj</ta>
            <ta e="T405" id="Seg_12488" s="T404">dempro</ta>
            <ta e="T406" id="Seg_12489" s="T405">n</ta>
            <ta e="T407" id="Seg_12490" s="T406">v</ta>
            <ta e="T408" id="Seg_12491" s="T407">conj</ta>
            <ta e="T409" id="Seg_12492" s="T408">v</ta>
            <ta e="T412" id="Seg_12493" s="T411">v</ta>
            <ta e="T413" id="Seg_12494" s="T412">n</ta>
            <ta e="T414" id="Seg_12495" s="T413">v</ta>
            <ta e="T415" id="Seg_12496" s="T414">que</ta>
            <ta e="T416" id="Seg_12497" s="T415">n</ta>
            <ta e="T417" id="Seg_12498" s="T416">v</ta>
            <ta e="T418" id="Seg_12499" s="T417">n</ta>
            <ta e="T419" id="Seg_12500" s="T418">adv</ta>
            <ta e="T420" id="Seg_12501" s="T419">adj</ta>
            <ta e="T421" id="Seg_12502" s="T420">conj</ta>
            <ta e="T423" id="Seg_12503" s="T422">refl</ta>
            <ta e="T424" id="Seg_12504" s="T423">adj</ta>
            <ta e="T425" id="Seg_12505" s="T424">conj</ta>
            <ta e="T426" id="Seg_12506" s="T425">dempro</ta>
            <ta e="T427" id="Seg_12507" s="T426">v</ta>
            <ta e="T428" id="Seg_12508" s="T427">ptcl</ta>
            <ta e="T429" id="Seg_12509" s="T428">pers</ta>
            <ta e="T430" id="Seg_12510" s="T429">ptcl</ta>
            <ta e="T431" id="Seg_12511" s="T430">n</ta>
            <ta e="T432" id="Seg_12512" s="T431">adv</ta>
            <ta e="T433" id="Seg_12513" s="T432">v</ta>
            <ta e="T434" id="Seg_12514" s="T433">aux</ta>
            <ta e="T435" id="Seg_12515" s="T434">v</ta>
            <ta e="T436" id="Seg_12516" s="T435">v</ta>
            <ta e="T437" id="Seg_12517" s="T436">ptcl</ta>
            <ta e="T438" id="Seg_12518" s="T437">n</ta>
            <ta e="T439" id="Seg_12519" s="T438">n</ta>
            <ta e="T440" id="Seg_12520" s="T439">pers</ta>
            <ta e="T441" id="Seg_12521" s="T440">adv</ta>
            <ta e="T442" id="Seg_12522" s="T441">v</ta>
            <ta e="T443" id="Seg_12523" s="T442">adv</ta>
            <ta e="T444" id="Seg_12524" s="T443">adv</ta>
            <ta e="T445" id="Seg_12525" s="T444">num</ta>
            <ta e="T446" id="Seg_12526" s="T445">n</ta>
            <ta e="T447" id="Seg_12527" s="T446">adv</ta>
            <ta e="T448" id="Seg_12528" s="T447">v</ta>
            <ta e="T449" id="Seg_12529" s="T448">v</ta>
            <ta e="T450" id="Seg_12530" s="T449">v</ta>
            <ta e="T451" id="Seg_12531" s="T450">dempro</ta>
            <ta e="T453" id="Seg_12532" s="T452">v</ta>
            <ta e="T454" id="Seg_12533" s="T453">n</ta>
            <ta e="T455" id="Seg_12534" s="T454">v</ta>
            <ta e="T456" id="Seg_12535" s="T455">v</ta>
            <ta e="T457" id="Seg_12536" s="T456">n</ta>
            <ta e="T458" id="Seg_12537" s="T457">v</ta>
            <ta e="T459" id="Seg_12538" s="T458">v</ta>
            <ta e="T460" id="Seg_12539" s="T459">ptcl</ta>
            <ta e="T461" id="Seg_12540" s="T460">n</ta>
            <ta e="T462" id="Seg_12541" s="T461">v</ta>
            <ta e="T463" id="Seg_12542" s="T462">n</ta>
            <ta e="T464" id="Seg_12543" s="T463">v</ta>
            <ta e="T465" id="Seg_12544" s="T464">dempro</ta>
            <ta e="T466" id="Seg_12545" s="T465">adv</ta>
            <ta e="T467" id="Seg_12546" s="T466">post</ta>
            <ta e="T469" id="Seg_12547" s="T468">adj</ta>
            <ta e="T470" id="Seg_12548" s="T469">n</ta>
            <ta e="T471" id="Seg_12549" s="T470">v</ta>
            <ta e="T472" id="Seg_12550" s="T471">num</ta>
            <ta e="T473" id="Seg_12551" s="T472">v</ta>
            <ta e="T474" id="Seg_12552" s="T473">v</ta>
            <ta e="T475" id="Seg_12553" s="T474">v</ta>
            <ta e="T476" id="Seg_12554" s="T475">adv</ta>
            <ta e="T477" id="Seg_12555" s="T476">ptcl</ta>
            <ta e="T478" id="Seg_12556" s="T477">v</ta>
            <ta e="T479" id="Seg_12557" s="T478">dempro</ta>
            <ta e="T480" id="Seg_12558" s="T479">v</ta>
            <ta e="T481" id="Seg_12559" s="T480">conj</ta>
            <ta e="T482" id="Seg_12560" s="T481">n</ta>
            <ta e="T483" id="Seg_12561" s="T482">v</ta>
            <ta e="T484" id="Seg_12562" s="T483">refl</ta>
            <ta e="T485" id="Seg_12563" s="T484">n</ta>
            <ta e="T487" id="Seg_12564" s="T486">v</ta>
            <ta e="T488" id="Seg_12565" s="T487">v</ta>
            <ta e="T489" id="Seg_12566" s="T488">v</ta>
            <ta e="T490" id="Seg_12567" s="T489">n</ta>
            <ta e="T491" id="Seg_12568" s="T490">v</ta>
            <ta e="T492" id="Seg_12569" s="T491">refl</ta>
            <ta e="T493" id="Seg_12570" s="T492">v</ta>
            <ta e="T494" id="Seg_12571" s="T493">n</ta>
            <ta e="T495" id="Seg_12572" s="T494">conj</ta>
            <ta e="T496" id="Seg_12573" s="T495">n</ta>
            <ta e="T497" id="Seg_12574" s="T496">v</ta>
            <ta e="T498" id="Seg_12575" s="T497">v</ta>
            <ta e="T499" id="Seg_12576" s="T498">adv</ta>
            <ta e="T500" id="Seg_12577" s="T499">n</ta>
            <ta e="T501" id="Seg_12578" s="T500">v</ta>
            <ta e="T502" id="Seg_12579" s="T501">ptcl</ta>
            <ta e="T503" id="Seg_12580" s="T502">que</ta>
            <ta e="T504" id="Seg_12581" s="T503">n</ta>
            <ta e="T505" id="Seg_12582" s="T504">n</ta>
            <ta e="T506" id="Seg_12583" s="T505">adj</ta>
            <ta e="T507" id="Seg_12584" s="T506">n</ta>
            <ta e="T508" id="Seg_12585" s="T507">adj</ta>
            <ta e="T509" id="Seg_12586" s="T508">v</ta>
            <ta e="T510" id="Seg_12587" s="T509">adj</ta>
            <ta e="T511" id="Seg_12588" s="T510">n</ta>
            <ta e="T512" id="Seg_12589" s="T511">n</ta>
            <ta e="T513" id="Seg_12590" s="T512">n</ta>
            <ta e="T514" id="Seg_12591" s="T513">n</ta>
            <ta e="T515" id="Seg_12592" s="T514">conj</ta>
            <ta e="T516" id="Seg_12593" s="T515">n</ta>
            <ta e="T517" id="Seg_12594" s="T516">v</ta>
            <ta e="T518" id="Seg_12595" s="T517">v</ta>
            <ta e="T519" id="Seg_12596" s="T518">v</ta>
            <ta e="T520" id="Seg_12597" s="T519">adv</ta>
            <ta e="T521" id="Seg_12598" s="T520">adv</ta>
            <ta e="T522" id="Seg_12599" s="T521">dempro</ta>
            <ta e="T523" id="Seg_12600" s="T522">n</ta>
            <ta e="T524" id="Seg_12601" s="T523">v</ta>
            <ta e="T525" id="Seg_12602" s="T524">quant</ta>
            <ta e="T526" id="Seg_12603" s="T525">n</ta>
            <ta e="T527" id="Seg_12604" s="T526">n</ta>
            <ta e="T528" id="Seg_12605" s="T527">v</ta>
            <ta e="T529" id="Seg_12606" s="T528">quant</ta>
            <ta e="T530" id="Seg_12607" s="T529">que</ta>
            <ta e="T531" id="Seg_12608" s="T530">adv</ta>
            <ta e="T532" id="Seg_12609" s="T531">v</ta>
            <ta e="T533" id="Seg_12610" s="T532">v</ta>
            <ta e="T534" id="Seg_12611" s="T533">adv</ta>
            <ta e="T535" id="Seg_12612" s="T534">v</ta>
            <ta e="T536" id="Seg_12613" s="T535">quant</ta>
            <ta e="T537" id="Seg_12614" s="T536">que</ta>
            <ta e="T538" id="Seg_12615" s="T537">ptcl</ta>
            <ta e="T539" id="Seg_12616" s="T538">v</ta>
            <ta e="T540" id="Seg_12617" s="T539">n</ta>
            <ta e="T542" id="Seg_12618" s="T541">v</ta>
            <ta e="T543" id="Seg_12619" s="T542">adv</ta>
            <ta e="T544" id="Seg_12620" s="T543">dempro</ta>
            <ta e="T546" id="Seg_12621" s="T545">adv</ta>
            <ta e="T547" id="Seg_12622" s="T546">n</ta>
            <ta e="T548" id="Seg_12623" s="T547">quant</ta>
            <ta e="T549" id="Seg_12624" s="T548">v</ta>
            <ta e="T550" id="Seg_12625" s="T549">v</ta>
            <ta e="T551" id="Seg_12626" s="T550">v</ta>
            <ta e="T552" id="Seg_12627" s="T551">dempro</ta>
            <ta e="T553" id="Seg_12628" s="T552">v</ta>
            <ta e="T555" id="Seg_12629" s="T554">ptcl</ta>
            <ta e="T556" id="Seg_12630" s="T555">v</ta>
            <ta e="T558" id="Seg_12631" s="T557">n</ta>
            <ta e="T559" id="Seg_12632" s="T558">v</ta>
            <ta e="T560" id="Seg_12633" s="T559">adv</ta>
            <ta e="T561" id="Seg_12634" s="T560">v</ta>
            <ta e="T562" id="Seg_12635" s="T561">dempro</ta>
            <ta e="T563" id="Seg_12636" s="T562">adj</ta>
            <ta e="T564" id="Seg_12637" s="T563">n</ta>
            <ta e="T565" id="Seg_12638" s="T564">dempro</ta>
            <ta e="T566" id="Seg_12639" s="T565">dempro</ta>
            <ta e="T567" id="Seg_12640" s="T566">ptcl</ta>
            <ta e="T568" id="Seg_12641" s="T567">adj</ta>
            <ta e="T569" id="Seg_12642" s="T568">v</ta>
            <ta e="T570" id="Seg_12643" s="T569">conj</ta>
            <ta e="T571" id="Seg_12644" s="T570">n</ta>
            <ta e="T572" id="Seg_12645" s="T571">adj</ta>
            <ta e="T573" id="Seg_12646" s="T572">n</ta>
            <ta e="T574" id="Seg_12647" s="T573">adj</ta>
            <ta e="T575" id="Seg_12648" s="T574">v</ta>
            <ta e="T576" id="Seg_12649" s="T575">que</ta>
            <ta e="T577" id="Seg_12650" s="T576">pers</ta>
            <ta e="T578" id="Seg_12651" s="T577">n</ta>
            <ta e="T580" id="Seg_12652" s="T579">v</ta>
            <ta e="T581" id="Seg_12653" s="T580">ptcl</ta>
            <ta e="T582" id="Seg_12654" s="T581">ptcl</ta>
            <ta e="T583" id="Seg_12655" s="T582">adv</ta>
            <ta e="T584" id="Seg_12656" s="T583">n</ta>
            <ta e="T585" id="Seg_12657" s="T584">v</ta>
            <ta e="T586" id="Seg_12658" s="T585">adv</ta>
            <ta e="T587" id="Seg_12659" s="T586">v</ta>
            <ta e="T588" id="Seg_12660" s="T587">n</ta>
            <ta e="T589" id="Seg_12661" s="T588">v</ta>
            <ta e="T590" id="Seg_12662" s="T589">dempro</ta>
            <ta e="T592" id="Seg_12663" s="T591">dempro</ta>
            <ta e="T593" id="Seg_12664" s="T592">pers</ta>
            <ta e="T594" id="Seg_12665" s="T593">n</ta>
            <ta e="T595" id="Seg_12666" s="T594">dempro</ta>
            <ta e="T596" id="Seg_12667" s="T595">dempro</ta>
            <ta e="T598" id="Seg_12668" s="T597">dempro</ta>
            <ta e="T599" id="Seg_12669" s="T598">adv</ta>
            <ta e="T600" id="Seg_12670" s="T599">v</ta>
            <ta e="T601" id="Seg_12671" s="T600">adj</ta>
            <ta e="T602" id="Seg_12672" s="T601">n</ta>
            <ta e="T604" id="Seg_12673" s="T603">v</ta>
            <ta e="T605" id="Seg_12674" s="T604">adv</ta>
            <ta e="T606" id="Seg_12675" s="T605">adj</ta>
            <ta e="T607" id="Seg_12676" s="T606">v</ta>
            <ta e="T608" id="Seg_12677" s="T607">adv</ta>
            <ta e="T609" id="Seg_12678" s="T608">adv</ta>
            <ta e="T610" id="Seg_12679" s="T609">ptcl</ta>
            <ta e="T611" id="Seg_12680" s="T610">n</ta>
            <ta e="T612" id="Seg_12681" s="T611">v</ta>
            <ta e="T613" id="Seg_12682" s="T612">v</ta>
            <ta e="T614" id="Seg_12683" s="T613">n</ta>
            <ta e="T615" id="Seg_12684" s="T614">ptcl</ta>
            <ta e="T616" id="Seg_12685" s="T615">v</ta>
            <ta e="T617" id="Seg_12686" s="T616">que</ta>
            <ta e="T618" id="Seg_12687" s="T617">adj</ta>
            <ta e="T619" id="Seg_12688" s="T618">v</ta>
            <ta e="T620" id="Seg_12689" s="T619">conj</ta>
            <ta e="T621" id="Seg_12690" s="T620">adv</ta>
            <ta e="T622" id="Seg_12691" s="T621">pers</ta>
            <ta e="T623" id="Seg_12692" s="T622">v</ta>
            <ta e="T624" id="Seg_12693" s="T623">n</ta>
            <ta e="T625" id="Seg_12694" s="T624">ptcl</ta>
            <ta e="T626" id="Seg_12695" s="T625">v</ta>
            <ta e="T627" id="Seg_12696" s="T626">v</ta>
            <ta e="T628" id="Seg_12697" s="T627">pers</ta>
            <ta e="T629" id="Seg_12698" s="T628">n</ta>
            <ta e="T634" id="Seg_12699" s="T633">v</ta>
            <ta e="T635" id="Seg_12700" s="T634">num</ta>
            <ta e="T636" id="Seg_12701" s="T635">n</ta>
            <ta e="T637" id="Seg_12702" s="T636">conj</ta>
            <ta e="T638" id="Seg_12703" s="T637">n</ta>
            <ta e="T639" id="Seg_12704" s="T638">n</ta>
            <ta e="T640" id="Seg_12705" s="T639">conj</ta>
            <ta e="T641" id="Seg_12706" s="T640">n</ta>
            <ta e="T642" id="Seg_12707" s="T641">adv</ta>
            <ta e="T643" id="Seg_12708" s="T642">v</ta>
            <ta e="T644" id="Seg_12709" s="T643">v</ta>
            <ta e="T645" id="Seg_12710" s="T644">conj</ta>
            <ta e="T646" id="Seg_12711" s="T645">n</ta>
            <ta e="T647" id="Seg_12712" s="T646">adv</ta>
            <ta e="T648" id="Seg_12713" s="T647">v</ta>
            <ta e="T649" id="Seg_12714" s="T648">v</ta>
            <ta e="T650" id="Seg_12715" s="T649">conj</ta>
            <ta e="T651" id="Seg_12716" s="T650">n</ta>
            <ta e="T652" id="Seg_12717" s="T651">v</ta>
            <ta e="T653" id="Seg_12718" s="T652">que</ta>
            <ta e="T654" id="Seg_12719" s="T653">v</ta>
            <ta e="T655" id="Seg_12720" s="T654">pers</ta>
            <ta e="T656" id="Seg_12721" s="T655">v</ta>
            <ta e="T657" id="Seg_12722" s="T656">n</ta>
            <ta e="T658" id="Seg_12723" s="T657">n</ta>
            <ta e="T659" id="Seg_12724" s="T658">v</ta>
            <ta e="T660" id="Seg_12725" s="T659">adv</ta>
            <ta e="T661" id="Seg_12726" s="T660">ptcl</ta>
            <ta e="T662" id="Seg_12727" s="T661">v</ta>
            <ta e="T663" id="Seg_12728" s="T662">dempro</ta>
            <ta e="T664" id="Seg_12729" s="T663">v</ta>
            <ta e="T665" id="Seg_12730" s="T664">conj</ta>
            <ta e="T666" id="Seg_12731" s="T665">dempro</ta>
            <ta e="T667" id="Seg_12732" s="T666">v</ta>
            <ta e="T668" id="Seg_12733" s="T667">n</ta>
            <ta e="T669" id="Seg_12734" s="T668">v</ta>
            <ta e="T670" id="Seg_12735" s="T669">v</ta>
            <ta e="T671" id="Seg_12736" s="T670">n</ta>
            <ta e="T672" id="Seg_12737" s="T671">dempro</ta>
            <ta e="T673" id="Seg_12738" s="T672">v</ta>
            <ta e="T674" id="Seg_12739" s="T673">que</ta>
            <ta e="T675" id="Seg_12740" s="T674">adv</ta>
            <ta e="T676" id="Seg_12741" s="T675">v</ta>
            <ta e="T677" id="Seg_12742" s="T676">n</ta>
            <ta e="T678" id="Seg_12743" s="T677">v</ta>
            <ta e="T679" id="Seg_12744" s="T678">adv</ta>
            <ta e="T680" id="Seg_12745" s="T679">adv</ta>
            <ta e="T681" id="Seg_12746" s="T680">n</ta>
            <ta e="T682" id="Seg_12747" s="T681">adv</ta>
            <ta e="T683" id="Seg_12748" s="T682">v</ta>
            <ta e="T684" id="Seg_12749" s="T683">n</ta>
            <ta e="T685" id="Seg_12750" s="T684">que</ta>
            <ta e="T686" id="Seg_12751" s="T685">v</ta>
            <ta e="T687" id="Seg_12752" s="T686">conj</ta>
            <ta e="T688" id="Seg_12753" s="T687">pers</ta>
            <ta e="T689" id="Seg_12754" s="T688">v</ta>
            <ta e="T690" id="Seg_12755" s="T689">n</ta>
            <ta e="T691" id="Seg_12756" s="T690">v</ta>
            <ta e="T692" id="Seg_12757" s="T691">ptcl</ta>
            <ta e="T693" id="Seg_12758" s="T692">v</ta>
            <ta e="T694" id="Seg_12759" s="T693">dempro</ta>
            <ta e="T695" id="Seg_12760" s="T694">v</ta>
            <ta e="T696" id="Seg_12761" s="T695">v</ta>
            <ta e="T697" id="Seg_12762" s="T696">v</ta>
            <ta e="T698" id="Seg_12763" s="T697">adv</ta>
            <ta e="T699" id="Seg_12764" s="T698">v</ta>
            <ta e="T700" id="Seg_12765" s="T699">que</ta>
            <ta e="T701" id="Seg_12766" s="T700">adv</ta>
            <ta e="T702" id="Seg_12767" s="T701">v</ta>
            <ta e="T703" id="Seg_12768" s="T702">n</ta>
            <ta e="T704" id="Seg_12769" s="T703">v</ta>
            <ta e="T705" id="Seg_12770" s="T704">adv</ta>
            <ta e="T706" id="Seg_12771" s="T705">num</ta>
            <ta e="T707" id="Seg_12772" s="T706">n</ta>
            <ta e="T708" id="Seg_12773" s="T707">adv</ta>
            <ta e="T709" id="Seg_12774" s="T708">v</ta>
            <ta e="T710" id="Seg_12775" s="T709">adv</ta>
            <ta e="T711" id="Seg_12776" s="T710">que</ta>
            <ta e="T712" id="Seg_12777" s="T711">v</ta>
            <ta e="T713" id="Seg_12778" s="T712">conj</ta>
            <ta e="T714" id="Seg_12779" s="T713">pers</ta>
            <ta e="T715" id="Seg_12780" s="T714">v</ta>
            <ta e="T716" id="Seg_12781" s="T715">ptcl</ta>
            <ta e="T717" id="Seg_12782" s="T716">v</ta>
            <ta e="T718" id="Seg_12783" s="T717">adv</ta>
            <ta e="T719" id="Seg_12784" s="T718">dempro</ta>
            <ta e="T720" id="Seg_12785" s="T719">v</ta>
            <ta e="T721" id="Seg_12786" s="T720">ptcl</ta>
            <ta e="T722" id="Seg_12787" s="T721">v</ta>
            <ta e="T723" id="Seg_12788" s="T722">que</ta>
            <ta e="T724" id="Seg_12789" s="T723">adv</ta>
            <ta e="T725" id="Seg_12790" s="T724">dempro</ta>
            <ta e="T726" id="Seg_12791" s="T725">n</ta>
            <ta e="T727" id="Seg_12792" s="T726">v</ta>
            <ta e="T728" id="Seg_12793" s="T727">adv</ta>
            <ta e="T729" id="Seg_12794" s="T728">adv</ta>
            <ta e="T730" id="Seg_12795" s="T729">v</ta>
            <ta e="T731" id="Seg_12796" s="T730">dempro</ta>
            <ta e="T732" id="Seg_12797" s="T731">v</ta>
            <ta e="T733" id="Seg_12798" s="T732">v</ta>
            <ta e="T734" id="Seg_12799" s="T733">v</ta>
            <ta e="T735" id="Seg_12800" s="T734">pers</ta>
            <ta e="T736" id="Seg_12801" s="T735">adj</ta>
            <ta e="T737" id="Seg_12802" s="T736">dempro</ta>
            <ta e="T738" id="Seg_12803" s="T737">v</ta>
            <ta e="T739" id="Seg_12804" s="T738">conj</ta>
            <ta e="T740" id="Seg_12805" s="T739">v</ta>
            <ta e="T741" id="Seg_12806" s="T740">que</ta>
            <ta e="T742" id="Seg_12807" s="T741">v</ta>
            <ta e="T743" id="Seg_12808" s="T742">conj</ta>
            <ta e="T744" id="Seg_12809" s="T743">pers</ta>
            <ta e="T745" id="Seg_12810" s="T744">v</ta>
            <ta e="T746" id="Seg_12811" s="T745">dempro</ta>
            <ta e="T747" id="Seg_12812" s="T746">ptcl</ta>
            <ta e="T748" id="Seg_12813" s="T747">ptcl</ta>
            <ta e="T749" id="Seg_12814" s="T748">pers</ta>
            <ta e="T750" id="Seg_12815" s="T749">ptcl</ta>
            <ta e="T751" id="Seg_12816" s="T750">v</ta>
            <ta e="T752" id="Seg_12817" s="T751">adv</ta>
            <ta e="T753" id="Seg_12818" s="T752">ptcl</ta>
            <ta e="T754" id="Seg_12819" s="T753">n</ta>
            <ta e="T755" id="Seg_12820" s="T754">v</ta>
            <ta e="T756" id="Seg_12821" s="T755">que</ta>
            <ta e="T757" id="Seg_12822" s="T756">v</ta>
            <ta e="T758" id="Seg_12823" s="T757">adv</ta>
            <ta e="T759" id="Seg_12824" s="T758">adj</ta>
            <ta e="T760" id="Seg_12825" s="T759">n</ta>
            <ta e="T761" id="Seg_12826" s="T760">v</ta>
            <ta e="T762" id="Seg_12827" s="T761">adv</ta>
            <ta e="T763" id="Seg_12828" s="T762">v</ta>
            <ta e="T764" id="Seg_12829" s="T763">refl</ta>
            <ta e="T765" id="Seg_12830" s="T764">n</ta>
            <ta e="T766" id="Seg_12831" s="T765">adj</ta>
            <ta e="T767" id="Seg_12832" s="T766">v</ta>
            <ta e="T768" id="Seg_12833" s="T767">dempro</ta>
            <ta e="T769" id="Seg_12834" s="T768">ptcl</ta>
            <ta e="T770" id="Seg_12835" s="T769">dempro</ta>
            <ta e="T771" id="Seg_12836" s="T770">n</ta>
            <ta e="T772" id="Seg_12837" s="T771">v</ta>
            <ta e="T773" id="Seg_12838" s="T772">v</ta>
            <ta e="T774" id="Seg_12839" s="T773">pers</ta>
            <ta e="T776" id="Seg_12840" s="T775">v</ta>
            <ta e="T777" id="Seg_12841" s="T776">adv</ta>
            <ta e="T778" id="Seg_12842" s="T777">dempro</ta>
            <ta e="T779" id="Seg_12843" s="T778">v</ta>
            <ta e="T780" id="Seg_12844" s="T779">ptcl</ta>
            <ta e="T781" id="Seg_12845" s="T780">pers</ta>
            <ta e="T782" id="Seg_12846" s="T781">v</ta>
            <ta e="T783" id="Seg_12847" s="T782">adv</ta>
            <ta e="T784" id="Seg_12848" s="T783">quant</ta>
            <ta e="T785" id="Seg_12849" s="T784">n</ta>
            <ta e="T786" id="Seg_12850" s="T785">conj</ta>
            <ta e="T787" id="Seg_12851" s="T786">n</ta>
            <ta e="T788" id="Seg_12852" s="T787">v</ta>
            <ta e="T789" id="Seg_12853" s="T788">que</ta>
            <ta e="T790" id="Seg_12854" s="T789">n</ta>
            <ta e="T791" id="Seg_12855" s="T790">v</ta>
            <ta e="T792" id="Seg_12856" s="T791">n</ta>
            <ta e="T793" id="Seg_12857" s="T792">v</ta>
            <ta e="T794" id="Seg_12858" s="T793">que</ta>
            <ta e="T795" id="Seg_12859" s="T794">n</ta>
            <ta e="T796" id="Seg_12860" s="T795">v</ta>
            <ta e="T797" id="Seg_12861" s="T796">conj</ta>
            <ta e="T798" id="Seg_12862" s="T797">adv</ta>
            <ta e="T799" id="Seg_12863" s="T798">pers</ta>
            <ta e="T801" id="Seg_12864" s="T800">n</ta>
            <ta e="T802" id="Seg_12865" s="T801">pers</ta>
            <ta e="T803" id="Seg_12866" s="T802">n</ta>
            <ta e="T804" id="Seg_12867" s="T803">v</ta>
            <ta e="T805" id="Seg_12868" s="T804">conj</ta>
            <ta e="T806" id="Seg_12869" s="T805">v</ta>
            <ta e="T808" id="Seg_12870" s="T807">n</ta>
            <ta e="T809" id="Seg_12871" s="T808">adv</ta>
            <ta e="T810" id="Seg_12872" s="T809">v</ta>
            <ta e="T811" id="Seg_12873" s="T810">adv</ta>
            <ta e="T812" id="Seg_12874" s="T811">v</ta>
            <ta e="T813" id="Seg_12875" s="T812">conj</ta>
            <ta e="T814" id="Seg_12876" s="T813">v</ta>
            <ta e="T815" id="Seg_12877" s="T814">v</ta>
            <ta e="T816" id="Seg_12878" s="T815">n</ta>
            <ta e="T817" id="Seg_12879" s="T816">v</ta>
            <ta e="T818" id="Seg_12880" s="T817">adv</ta>
            <ta e="T819" id="Seg_12881" s="T818">v</ta>
            <ta e="T820" id="Seg_12882" s="T819">v</ta>
            <ta e="T822" id="Seg_12883" s="T821">n</ta>
            <ta e="T823" id="Seg_12884" s="T822">adv</ta>
            <ta e="T824" id="Seg_12885" s="T823">refl</ta>
            <ta e="T825" id="Seg_12886" s="T824">n</ta>
            <ta e="T826" id="Seg_12887" s="T825">v</ta>
            <ta e="T827" id="Seg_12888" s="T826">n</ta>
            <ta e="T828" id="Seg_12889" s="T827">v</ta>
            <ta e="T829" id="Seg_12890" s="T828">n</ta>
            <ta e="T830" id="Seg_12891" s="T829">v</ta>
            <ta e="T831" id="Seg_12892" s="T830">adv</ta>
            <ta e="T832" id="Seg_12893" s="T831">n</ta>
            <ta e="T833" id="Seg_12894" s="T832">v</ta>
            <ta e="T834" id="Seg_12895" s="T833">n</ta>
            <ta e="T835" id="Seg_12896" s="T834">n</ta>
            <ta e="T836" id="Seg_12897" s="T835">adv</ta>
            <ta e="T837" id="Seg_12898" s="T836">adv</ta>
            <ta e="T838" id="Seg_12899" s="T837">adv</ta>
            <ta e="T839" id="Seg_12900" s="T838">pers</ta>
            <ta e="T840" id="Seg_12901" s="T839">n</ta>
            <ta e="T841" id="Seg_12902" s="T840">n</ta>
            <ta e="T842" id="Seg_12903" s="T841">v</ta>
            <ta e="T843" id="Seg_12904" s="T842">num</ta>
            <ta e="T844" id="Seg_12905" s="T843">n</ta>
            <ta e="T845" id="Seg_12906" s="T844">n</ta>
            <ta e="T846" id="Seg_12907" s="T845">v</ta>
            <ta e="T847" id="Seg_12908" s="T846">dempro</ta>
            <ta e="T848" id="Seg_12909" s="T847">quant</ta>
            <ta e="T849" id="Seg_12910" s="T848">adv</ta>
            <ta e="T850" id="Seg_12911" s="T849">v</ta>
            <ta e="T851" id="Seg_12912" s="T850">ptcl</ta>
            <ta e="T852" id="Seg_12913" s="T851">v</ta>
            <ta e="T853" id="Seg_12914" s="T852">n</ta>
            <ta e="T854" id="Seg_12915" s="T853">conj</ta>
            <ta e="T855" id="Seg_12916" s="T854">n</ta>
            <ta e="T857" id="Seg_12917" s="T856">dempro</ta>
            <ta e="T858" id="Seg_12918" s="T857">que</ta>
            <ta e="T859" id="Seg_12919" s="T858">v</ta>
            <ta e="T860" id="Seg_12920" s="T859">v</ta>
            <ta e="T861" id="Seg_12921" s="T860">n</ta>
            <ta e="T862" id="Seg_12922" s="T861">n</ta>
            <ta e="T863" id="Seg_12923" s="T862">n</ta>
            <ta e="T864" id="Seg_12924" s="T863">v</ta>
            <ta e="T865" id="Seg_12925" s="T864">v</ta>
            <ta e="T866" id="Seg_12926" s="T865">v</ta>
            <ta e="T867" id="Seg_12927" s="T866">ptcl</ta>
            <ta e="T868" id="Seg_12928" s="T867">num</ta>
            <ta e="T869" id="Seg_12929" s="T868">v</ta>
            <ta e="T870" id="Seg_12930" s="T869">n</ta>
            <ta e="T871" id="Seg_12931" s="T870">adv</ta>
            <ta e="T872" id="Seg_12932" s="T871">ptcl</ta>
            <ta e="T873" id="Seg_12933" s="T872">v</ta>
            <ta e="T875" id="Seg_12934" s="T874">n</ta>
            <ta e="T876" id="Seg_12935" s="T875">v</ta>
            <ta e="T877" id="Seg_12936" s="T876">n</ta>
            <ta e="T878" id="Seg_12937" s="T877">v</ta>
            <ta e="T879" id="Seg_12938" s="T878">n</ta>
            <ta e="T880" id="Seg_12939" s="T879">dempro</ta>
            <ta e="T881" id="Seg_12940" s="T880">v</ta>
            <ta e="T882" id="Seg_12941" s="T881">dempro</ta>
            <ta e="T883" id="Seg_12942" s="T882">adv</ta>
            <ta e="T884" id="Seg_12943" s="T883">v</ta>
            <ta e="T885" id="Seg_12944" s="T884">adv</ta>
            <ta e="T886" id="Seg_12945" s="T885">adv</ta>
            <ta e="T887" id="Seg_12946" s="T886">adv</ta>
            <ta e="T888" id="Seg_12947" s="T887">ptcl</ta>
            <ta e="T889" id="Seg_12948" s="T888">v</ta>
            <ta e="T890" id="Seg_12949" s="T889">dempro</ta>
            <ta e="T891" id="Seg_12950" s="T890">adv</ta>
            <ta e="T892" id="Seg_12951" s="T891">n</ta>
            <ta e="T894" id="Seg_12952" s="T893">adv</ta>
            <ta e="T895" id="Seg_12953" s="T894">adv</ta>
            <ta e="T896" id="Seg_12954" s="T895">v</ta>
            <ta e="T897" id="Seg_12955" s="T896">ptcl</ta>
            <ta e="T898" id="Seg_12956" s="T897">adv</ta>
            <ta e="T899" id="Seg_12957" s="T898">adv</ta>
            <ta e="T900" id="Seg_12958" s="T899">n</ta>
            <ta e="T901" id="Seg_12959" s="T900">ptcl</ta>
            <ta e="T902" id="Seg_12960" s="T901">v</ta>
            <ta e="T903" id="Seg_12961" s="T902">v</ta>
            <ta e="T904" id="Seg_12962" s="T903">n</ta>
            <ta e="T905" id="Seg_12963" s="T904">v</ta>
            <ta e="T906" id="Seg_12964" s="T905">v</ta>
            <ta e="T908" id="Seg_12965" s="T907">adv</ta>
            <ta e="T909" id="Seg_12966" s="T908">adv</ta>
            <ta e="T910" id="Seg_12967" s="T909">adv</ta>
            <ta e="T911" id="Seg_12968" s="T910">v</ta>
            <ta e="T912" id="Seg_12969" s="T911">n</ta>
            <ta e="T914" id="Seg_12970" s="T913">v</ta>
            <ta e="T915" id="Seg_12971" s="T914">adv</ta>
            <ta e="T916" id="Seg_12972" s="T915">n</ta>
            <ta e="T917" id="Seg_12973" s="T916">v</ta>
            <ta e="T918" id="Seg_12974" s="T917">adv</ta>
            <ta e="T919" id="Seg_12975" s="T918">n</ta>
            <ta e="T920" id="Seg_12976" s="T919">n</ta>
            <ta e="T921" id="Seg_12977" s="T920">v</ta>
            <ta e="T922" id="Seg_12978" s="T921">n</ta>
            <ta e="T923" id="Seg_12979" s="T922">adj</ta>
            <ta e="T924" id="Seg_12980" s="T923">adj</ta>
            <ta e="T925" id="Seg_12981" s="T924">quant</ta>
            <ta e="T926" id="Seg_12982" s="T925">conj</ta>
            <ta e="T927" id="Seg_12983" s="T926">adv</ta>
            <ta e="T928" id="Seg_12984" s="T927">n</ta>
            <ta e="T929" id="Seg_12985" s="T928">v</ta>
            <ta e="T930" id="Seg_12986" s="T929">dempro</ta>
            <ta e="T931" id="Seg_12987" s="T930">v</ta>
            <ta e="T932" id="Seg_12988" s="T931">dempro</ta>
            <ta e="T933" id="Seg_12989" s="T932">conj</ta>
            <ta e="T934" id="Seg_12990" s="T933">v</ta>
            <ta e="T935" id="Seg_12991" s="T934">ptcl</ta>
            <ta e="T936" id="Seg_12992" s="T935">pers</ta>
            <ta e="T937" id="Seg_12993" s="T936">n</ta>
            <ta e="T938" id="Seg_12994" s="T937">n</ta>
            <ta e="T939" id="Seg_12995" s="T938">adj</ta>
            <ta e="T940" id="Seg_12996" s="T939">n</ta>
            <ta e="T941" id="Seg_12997" s="T940">dempro</ta>
            <ta e="T942" id="Seg_12998" s="T941">v</ta>
            <ta e="T943" id="Seg_12999" s="T942">conj</ta>
            <ta e="T944" id="Seg_13000" s="T943">ptcl</ta>
            <ta e="T945" id="Seg_13001" s="T944">n</ta>
            <ta e="T946" id="Seg_13002" s="T945">dempro</ta>
            <ta e="T947" id="Seg_13003" s="T946">n</ta>
            <ta e="T948" id="Seg_13004" s="T947">v</ta>
            <ta e="T949" id="Seg_13005" s="T948">adv</ta>
            <ta e="T950" id="Seg_13006" s="T949">n</ta>
            <ta e="T951" id="Seg_13007" s="T950">n</ta>
            <ta e="T952" id="Seg_13008" s="T951">n</ta>
            <ta e="T953" id="Seg_13009" s="T952">n</ta>
            <ta e="T954" id="Seg_13010" s="T953">n</ta>
            <ta e="T955" id="Seg_13011" s="T954">v</ta>
            <ta e="T956" id="Seg_13012" s="T955">conj</ta>
            <ta e="T957" id="Seg_13013" s="T956">n</ta>
            <ta e="T958" id="Seg_13014" s="T957">v</ta>
            <ta e="T959" id="Seg_13015" s="T958">adv</ta>
            <ta e="T960" id="Seg_13016" s="T959">post</ta>
            <ta e="T961" id="Seg_13017" s="T960">v</ta>
            <ta e="T962" id="Seg_13018" s="T961">adv</ta>
            <ta e="T963" id="Seg_13019" s="T962">v</ta>
            <ta e="T964" id="Seg_13020" s="T963">dempro</ta>
            <ta e="T965" id="Seg_13021" s="T964">n</ta>
            <ta e="T966" id="Seg_13022" s="T965">v</ta>
            <ta e="T967" id="Seg_13023" s="T966">pers</ta>
            <ta e="T968" id="Seg_13024" s="T967">v</ta>
            <ta e="T969" id="Seg_13025" s="T968">dempro</ta>
            <ta e="T970" id="Seg_13026" s="T969">v</ta>
            <ta e="T971" id="Seg_13027" s="T970">adv</ta>
            <ta e="T972" id="Seg_13028" s="T971">dempro</ta>
            <ta e="T974" id="Seg_13029" s="T973">dempro</ta>
            <ta e="T976" id="Seg_13030" s="T975">n</ta>
            <ta e="T977" id="Seg_13031" s="T976">adv</ta>
            <ta e="T978" id="Seg_13032" s="T977">n</ta>
            <ta e="T979" id="Seg_13033" s="T978">ptcl</ta>
            <ta e="T980" id="Seg_13034" s="T979">ptcl</ta>
            <ta e="T981" id="Seg_13035" s="T980">n</ta>
            <ta e="T982" id="Seg_13036" s="T981">v</ta>
            <ta e="T983" id="Seg_13037" s="T982">n</ta>
            <ta e="T984" id="Seg_13038" s="T983">n</ta>
            <ta e="T985" id="Seg_13039" s="T984">dempro</ta>
            <ta e="T986" id="Seg_13040" s="T985">v</ta>
            <ta e="T987" id="Seg_13041" s="T986">v</ta>
            <ta e="T988" id="Seg_13042" s="T987">pers</ta>
            <ta e="T989" id="Seg_13043" s="T988">ptcl</ta>
            <ta e="T990" id="Seg_13044" s="T989">pers</ta>
            <ta e="T991" id="Seg_13045" s="T990">ptcl</ta>
            <ta e="T992" id="Seg_13046" s="T991">v</ta>
            <ta e="T993" id="Seg_13047" s="T992">ptcl</ta>
            <ta e="T995" id="Seg_13048" s="T994">pers</ta>
            <ta e="T996" id="Seg_13049" s="T995">v</ta>
            <ta e="T997" id="Seg_13050" s="T996">que</ta>
            <ta e="T998" id="Seg_13051" s="T997">adv</ta>
            <ta e="T999" id="Seg_13052" s="T998">dempro</ta>
            <ta e="T1000" id="Seg_13053" s="T999">n</ta>
            <ta e="T1001" id="Seg_13054" s="T1000">v</ta>
            <ta e="T1002" id="Seg_13055" s="T1001">conj</ta>
            <ta e="T1003" id="Seg_13056" s="T1002">v</ta>
            <ta e="T1004" id="Seg_13057" s="T1003">v</ta>
            <ta e="T1005" id="Seg_13058" s="T1004">conj</ta>
            <ta e="T1006" id="Seg_13059" s="T1005">v</ta>
            <ta e="T1007" id="Seg_13060" s="T1006">n</ta>
            <ta e="T1008" id="Seg_13061" s="T1007">dempro</ta>
            <ta e="T1009" id="Seg_13062" s="T1008">v</ta>
            <ta e="T1010" id="Seg_13063" s="T1009">v</ta>
            <ta e="T1011" id="Seg_13064" s="T1010">n</ta>
            <ta e="T1012" id="Seg_13065" s="T1011">adv</ta>
            <ta e="T1013" id="Seg_13066" s="T1012">n</ta>
            <ta e="T1014" id="Seg_13067" s="T1013">n</ta>
            <ta e="T1015" id="Seg_13068" s="T1014">aux</ta>
            <ta e="T1016" id="Seg_13069" s="T1015">v</ta>
            <ta e="T1017" id="Seg_13070" s="T1016">pers</ta>
            <ta e="T1018" id="Seg_13071" s="T1017">v</ta>
            <ta e="T1020" id="Seg_13072" s="T1019">v</ta>
            <ta e="T1021" id="Seg_13073" s="T1020">adv</ta>
            <ta e="T1022" id="Seg_13074" s="T1021">pers</ta>
            <ta e="T1023" id="Seg_13075" s="T1022">v</ta>
            <ta e="T1024" id="Seg_13076" s="T1023">v</ta>
            <ta e="T1025" id="Seg_13077" s="T1024">v</ta>
            <ta e="T1026" id="Seg_13078" s="T1025">v</ta>
            <ta e="T1027" id="Seg_13079" s="T1026">dempro</ta>
            <ta e="T1028" id="Seg_13080" s="T1027">n</ta>
            <ta e="T1029" id="Seg_13081" s="T1028">que</ta>
            <ta e="T1030" id="Seg_13082" s="T1029">v</ta>
            <ta e="T1031" id="Seg_13083" s="T1030">v</ta>
            <ta e="T1032" id="Seg_13084" s="T1031">pers</ta>
            <ta e="T1033" id="Seg_13085" s="T1032">v</ta>
            <ta e="T1034" id="Seg_13086" s="T1033">pers</ta>
            <ta e="T1035" id="Seg_13087" s="T1034">adv</ta>
            <ta e="T1036" id="Seg_13088" s="T1035">dempro</ta>
            <ta e="T1037" id="Seg_13089" s="T1036">v</ta>
            <ta e="T1038" id="Seg_13090" s="T1037">adv</ta>
            <ta e="T1039" id="Seg_13091" s="T1038">v</ta>
            <ta e="T1040" id="Seg_13092" s="T1039">v</ta>
            <ta e="T1041" id="Seg_13093" s="T1040">n</ta>
            <ta e="T1042" id="Seg_13094" s="T1041">que</ta>
            <ta e="T1043" id="Seg_13095" s="T1042">v</ta>
            <ta e="T1044" id="Seg_13096" s="T1043">n</ta>
            <ta e="T1045" id="Seg_13097" s="T1044">v</ta>
            <ta e="T1047" id="Seg_13098" s="T1046">aux</ta>
            <ta e="T1048" id="Seg_13099" s="T1047">pers</ta>
            <ta e="T1049" id="Seg_13100" s="T1048">v</ta>
            <ta e="T1050" id="Seg_13101" s="T1049">pers</ta>
            <ta e="T1051" id="Seg_13102" s="T1050">adv</ta>
            <ta e="T1052" id="Seg_13103" s="T1051">dempro</ta>
            <ta e="T1053" id="Seg_13104" s="T1052">v</ta>
            <ta e="T1054" id="Seg_13105" s="T1053">adv</ta>
            <ta e="T1055" id="Seg_13106" s="T1054">v</ta>
            <ta e="T1056" id="Seg_13107" s="T1055">n</ta>
            <ta e="T1057" id="Seg_13108" s="T1056">v</ta>
            <ta e="T1058" id="Seg_13109" s="T1057">v</ta>
            <ta e="T1059" id="Seg_13110" s="T1058">pers</ta>
            <ta e="T1060" id="Seg_13111" s="T1059">adv</ta>
            <ta e="T1061" id="Seg_13112" s="T1060">v</ta>
            <ta e="T1062" id="Seg_13113" s="T1061">ptcl</ta>
            <ta e="T1063" id="Seg_13114" s="T1062">v</ta>
            <ta e="T1065" id="Seg_13115" s="T1064">v</ta>
            <ta e="T1066" id="Seg_13116" s="T1065">pers</ta>
            <ta e="T1067" id="Seg_13117" s="T1066">n</ta>
            <ta e="T1068" id="Seg_13118" s="T1067">v</ta>
            <ta e="T1069" id="Seg_13119" s="T1068">pers</ta>
            <ta e="T1070" id="Seg_13120" s="T1069">n</ta>
            <ta e="T1071" id="Seg_13121" s="T1070">dempro</ta>
            <ta e="T1072" id="Seg_13122" s="T1071">v</ta>
            <ta e="T1074" id="Seg_13123" s="T1073">v</ta>
            <ta e="T1075" id="Seg_13124" s="T1074">dempro</ta>
            <ta e="T1076" id="Seg_13125" s="T1075">conj</ta>
            <ta e="T1077" id="Seg_13126" s="T1076">n</ta>
            <ta e="T1078" id="Seg_13127" s="T1077">ptcl</ta>
            <ta e="T1079" id="Seg_13128" s="T1078">n</ta>
            <ta e="T1080" id="Seg_13129" s="T1079">v</ta>
            <ta e="T1081" id="Seg_13130" s="T1080">n</ta>
            <ta e="T1082" id="Seg_13131" s="T1081">dempro</ta>
            <ta e="T1083" id="Seg_13132" s="T1082">v</ta>
            <ta e="T1084" id="Seg_13133" s="T1083">conj</ta>
            <ta e="T1085" id="Seg_13134" s="T1084">dempro</ta>
            <ta e="T1086" id="Seg_13135" s="T1085">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PKZ">
            <ta e="T6" id="Seg_13136" s="T5">np.h:E</ta>
            <ta e="T7" id="Seg_13137" s="T6">pro.h:Poss</ta>
            <ta e="T9" id="Seg_13138" s="T8">np.h:Th</ta>
            <ta e="T12" id="Seg_13139" s="T11">np.h:Th</ta>
            <ta e="T14" id="Seg_13140" s="T13">np.h:Th</ta>
            <ta e="T16" id="Seg_13141" s="T15">np.h:Th</ta>
            <ta e="T17" id="Seg_13142" s="T16">np.h:Th</ta>
            <ta e="T18" id="Seg_13143" s="T17">adv:Time</ta>
            <ta e="T21" id="Seg_13144" s="T20">np.h:A</ta>
            <ta e="T23" id="Seg_13145" s="T22">np:Th</ta>
            <ta e="T26" id="Seg_13146" s="T25">pro.h:A</ta>
            <ta e="T29" id="Seg_13147" s="T28">n:Time</ta>
            <ta e="T32" id="Seg_13148" s="T31">0.3.h:A</ta>
            <ta e="T33" id="Seg_13149" s="T32">np:P</ta>
            <ta e="T35" id="Seg_13150" s="T34">0.3.h:A</ta>
            <ta e="T39" id="Seg_13151" s="T38">pro.h:A</ta>
            <ta e="T41" id="Seg_13152" s="T40">np:P</ta>
            <ta e="T44" id="Seg_13153" s="T43">adv:Time</ta>
            <ta e="T48" id="Seg_13154" s="T47">np.h:A</ta>
            <ta e="T49" id="Seg_13155" s="T48">adv:L</ta>
            <ta e="T50" id="Seg_13156" s="T49">0.3.h:A</ta>
            <ta e="T51" id="Seg_13157" s="T50">0.3.h:E</ta>
            <ta e="T52" id="Seg_13158" s="T51">np:L</ta>
            <ta e="T54" id="Seg_13159" s="T53">0.3.h:A</ta>
            <ta e="T55" id="Seg_13160" s="T54">np:G</ta>
            <ta e="T56" id="Seg_13161" s="T55">0.1.h:E</ta>
            <ta e="T58" id="Seg_13162" s="T57">0.1.h:E</ta>
            <ta e="T60" id="Seg_13163" s="T59">pro.h:Th</ta>
            <ta e="T62" id="Seg_13164" s="T61">0.1.h:E</ta>
            <ta e="T63" id="Seg_13165" s="T62">adv:Time</ta>
            <ta e="T65" id="Seg_13166" s="T64">np.h:A</ta>
            <ta e="T68" id="Seg_13167" s="T67">0.3.h:A</ta>
            <ta e="T69" id="Seg_13168" s="T68">0.3.h:E</ta>
            <ta e="T70" id="Seg_13169" s="T69">np:L</ta>
            <ta e="T71" id="Seg_13170" s="T70">adv:Time</ta>
            <ta e="T72" id="Seg_13171" s="T71">n:Time</ta>
            <ta e="T73" id="Seg_13172" s="T72">0.3.h:A</ta>
            <ta e="T75" id="Seg_13173" s="T74">n:Time</ta>
            <ta e="T77" id="Seg_13174" s="T76">0.1.h:E</ta>
            <ta e="T79" id="Seg_13175" s="T78">pro.h:Th</ta>
            <ta e="T81" id="Seg_13176" s="T80">0.1.h:E</ta>
            <ta e="T82" id="Seg_13177" s="T81">adv:Time</ta>
            <ta e="T83" id="Seg_13178" s="T82">np.h:A</ta>
            <ta e="T85" id="Seg_13179" s="T84">0.3.h:A</ta>
            <ta e="T86" id="Seg_13180" s="T85">np:Th</ta>
            <ta e="T87" id="Seg_13181" s="T86">adv:Time</ta>
            <ta e="T88" id="Seg_13182" s="T87">0.3.h:Th</ta>
            <ta e="T89" id="Seg_13183" s="T88">np:L</ta>
            <ta e="T91" id="Seg_13184" s="T90">0.3.h:E</ta>
            <ta e="T92" id="Seg_13185" s="T91">0.3.h:Th</ta>
            <ta e="T93" id="Seg_13186" s="T92">0.3.h:E</ta>
            <ta e="T94" id="Seg_13187" s="T93">np.h:A</ta>
            <ta e="T99" id="Seg_13188" s="T98">adv:L</ta>
            <ta e="T100" id="Seg_13189" s="T99">np:L</ta>
            <ta e="T101" id="Seg_13190" s="T100">np:Th</ta>
            <ta e="T103" id="Seg_13191" s="T102">pro.h:A</ta>
            <ta e="T106" id="Seg_13192" s="T105">np:Th</ta>
            <ta e="T107" id="Seg_13193" s="T106">pro:G</ta>
            <ta e="T109" id="Seg_13194" s="T108">0.3.h:A</ta>
            <ta e="T110" id="Seg_13195" s="T109">pro.h:A</ta>
            <ta e="T113" id="Seg_13196" s="T112">0.3.h:A</ta>
            <ta e="T115" id="Seg_13197" s="T114">adv:Time</ta>
            <ta e="T116" id="Seg_13198" s="T115">0.2.h:A</ta>
            <ta e="T117" id="Seg_13199" s="T116">pro.h:Th</ta>
            <ta e="T118" id="Seg_13200" s="T117">pro.h:A</ta>
            <ta e="T119" id="Seg_13201" s="T118">pro.h:R</ta>
            <ta e="T121" id="Seg_13202" s="T120">pro:Th</ta>
            <ta e="T125" id="Seg_13203" s="T124">0.2.h:A</ta>
            <ta e="T128" id="Seg_13204" s="T127">0.2.h:A</ta>
            <ta e="T129" id="Seg_13205" s="T128">np:P</ta>
            <ta e="T131" id="Seg_13206" s="T130">0.1.h:A</ta>
            <ta e="T134" id="Seg_13207" s="T133">0.2.h:A</ta>
            <ta e="T136" id="Seg_13208" s="T135">pro:G</ta>
            <ta e="T137" id="Seg_13209" s="T136">0.2.h:A</ta>
            <ta e="T138" id="Seg_13210" s="T137">adv:L</ta>
            <ta e="T147" id="Seg_13211" s="T146">np.h:A</ta>
            <ta e="T149" id="Seg_13212" s="T148">pro.h:A</ta>
            <ta e="T151" id="Seg_13213" s="T150">np:G</ta>
            <ta e="T152" id="Seg_13214" s="T151">np:G</ta>
            <ta e="T153" id="Seg_13215" s="T152">0.3.h:A</ta>
            <ta e="T154" id="Seg_13216" s="T153">pro.h:A</ta>
            <ta e="T156" id="Seg_13217" s="T155">np:Th</ta>
            <ta e="T157" id="Seg_13218" s="T156">adv:L</ta>
            <ta e="T158" id="Seg_13219" s="T157">adv:Time</ta>
            <ta e="T159" id="Seg_13220" s="T158">n:Time</ta>
            <ta e="T161" id="Seg_13221" s="T160">0.3.h:A</ta>
            <ta e="T163" id="Seg_13222" s="T162">np:P</ta>
            <ta e="T164" id="Seg_13223" s="T163">adv:Time</ta>
            <ta e="T165" id="Seg_13224" s="T164">np.h:A</ta>
            <ta e="T168" id="Seg_13225" s="T167">np.h:A</ta>
            <ta e="T171" id="Seg_13226" s="T170">pro.h:Poss</ta>
            <ta e="T172" id="Seg_13227" s="T171">np.h:E</ta>
            <ta e="T174" id="Seg_13228" s="T173">np:L</ta>
            <ta e="T176" id="Seg_13229" s="T175">np.h:A</ta>
            <ta e="T180" id="Seg_13230" s="T179">pro.h:Poss</ta>
            <ta e="T181" id="Seg_13231" s="T180">np.h:A</ta>
            <ta e="T184" id="Seg_13232" s="T183">pro.h:A</ta>
            <ta e="T186" id="Seg_13233" s="T185">0.2.h:A</ta>
            <ta e="T187" id="Seg_13234" s="T186">pro.h:Th</ta>
            <ta e="T190" id="Seg_13235" s="T189">0.2.h:A</ta>
            <ta e="T191" id="Seg_13236" s="T190">pro:Th</ta>
            <ta e="T192" id="Seg_13237" s="T191">adv:L</ta>
            <ta e="T194" id="Seg_13238" s="T193">0.2.h:E</ta>
            <ta e="T196" id="Seg_13239" s="T195">np.h:A</ta>
            <ta e="T199" id="Seg_13240" s="T198">pro.h:Th</ta>
            <ta e="T200" id="Seg_13241" s="T199">pro.h:A</ta>
            <ta e="T203" id="Seg_13242" s="T202">pro.h:A</ta>
            <ta e="T205" id="Seg_13243" s="T204">np:Th</ta>
            <ta e="T206" id="Seg_13244" s="T205">0.3.h:A</ta>
            <ta e="T207" id="Seg_13245" s="T206">0.3.h:A</ta>
            <ta e="T211" id="Seg_13246" s="T210">np.h:A</ta>
            <ta e="T214" id="Seg_13247" s="T213">adv:L</ta>
            <ta e="T216" id="Seg_13248" s="T215">0.3.h:A</ta>
            <ta e="T217" id="Seg_13249" s="T216">pro.h:A</ta>
            <ta e="T218" id="Seg_13250" s="T217">np:G</ta>
            <ta e="T220" id="Seg_13251" s="T219">adv:Time</ta>
            <ta e="T222" id="Seg_13252" s="T221">np.h:A</ta>
            <ta e="T225" id="Seg_13253" s="T224">0.3.h:A</ta>
            <ta e="T226" id="Seg_13254" s="T225">pro:L</ta>
            <ta e="T227" id="Seg_13255" s="T226">0.3.h:A</ta>
            <ta e="T229" id="Seg_13256" s="T228">np:G</ta>
            <ta e="T230" id="Seg_13257" s="T229">0.2.h:A</ta>
            <ta e="T231" id="Seg_13258" s="T230">np:So</ta>
            <ta e="T232" id="Seg_13259" s="T231">0.2.h:A</ta>
            <ta e="T233" id="Seg_13260" s="T232">pro.h:A</ta>
            <ta e="T235" id="Seg_13261" s="T234">0.3.h:A</ta>
            <ta e="T237" id="Seg_13262" s="T236">0.3.h:P</ta>
            <ta e="T238" id="Seg_13263" s="T237">adv:Time</ta>
            <ta e="T239" id="Seg_13264" s="T238">pro:G</ta>
            <ta e="T240" id="Seg_13265" s="T239">0.3.h:A</ta>
            <ta e="T241" id="Seg_13266" s="T240">pro.h:A</ta>
            <ta e="T244" id="Seg_13267" s="T243">adv:L</ta>
            <ta e="T245" id="Seg_13268" s="T244">np.h:Th</ta>
            <ta e="T249" id="Seg_13269" s="T248">pro.h:Th</ta>
            <ta e="T252" id="Seg_13270" s="T251">np:G</ta>
            <ta e="T255" id="Seg_13271" s="T254">pro.h:A</ta>
            <ta e="T258" id="Seg_13272" s="T257">0.3.h:A</ta>
            <ta e="T261" id="Seg_13273" s="T260">0.3.h:A</ta>
            <ta e="T262" id="Seg_13274" s="T261">adv:Time</ta>
            <ta e="T263" id="Seg_13275" s="T262">np.h:A</ta>
            <ta e="T266" id="Seg_13276" s="T265">0.2.h:A</ta>
            <ta e="T267" id="Seg_13277" s="T266">0.2.h:A</ta>
            <ta e="T269" id="Seg_13278" s="T268">pro.h:A</ta>
            <ta e="T271" id="Seg_13279" s="T270">np.h:Th</ta>
            <ta e="T272" id="Seg_13280" s="T271">0.3.h:A</ta>
            <ta e="T273" id="Seg_13281" s="T272">adv:Time</ta>
            <ta e="T274" id="Seg_13282" s="T273">np:Th</ta>
            <ta e="T275" id="Seg_13283" s="T274">0.3.h:A</ta>
            <ta e="T279" id="Seg_13284" s="T278">np:Th</ta>
            <ta e="T280" id="Seg_13285" s="T279">0.3.h:A</ta>
            <ta e="T282" id="Seg_13286" s="T281">np.h:A</ta>
            <ta e="T287" id="Seg_13287" s="T286">0.2.h:A</ta>
            <ta e="T291" id="Seg_13288" s="T290">pro.h:A</ta>
            <ta e="T292" id="Seg_13289" s="T291">np:G</ta>
            <ta e="T295" id="Seg_13290" s="T294">0.3.h:E</ta>
            <ta e="T296" id="Seg_13291" s="T295">adv:Time</ta>
            <ta e="T298" id="Seg_13292" s="T297">np.h:A</ta>
            <ta e="T301" id="Seg_13293" s="T300">0.3.h:A</ta>
            <ta e="T306" id="Seg_13294" s="T305">pro.h:E</ta>
            <ta e="T308" id="Seg_13295" s="T307">pro.h:Th</ta>
            <ta e="T309" id="Seg_13296" s="T308">pro.h:Poss</ta>
            <ta e="T310" id="Seg_13297" s="T309">np:Th</ta>
            <ta e="T311" id="Seg_13298" s="T310">adv:L</ta>
            <ta e="T313" id="Seg_13299" s="T312">0.2.h:A</ta>
            <ta e="T315" id="Seg_13300" s="T314">pro.h:Poss</ta>
            <ta e="T317" id="Seg_13301" s="T316">0.2.h:Th</ta>
            <ta e="T319" id="Seg_13302" s="T318">pro:Th</ta>
            <ta e="T320" id="Seg_13303" s="T319">0.2.h:A</ta>
            <ta e="T323" id="Seg_13304" s="T322">np.h:Th</ta>
            <ta e="T324" id="Seg_13305" s="T323">0.3.h:Th</ta>
            <ta e="T325" id="Seg_13306" s="T324">np.h:Th</ta>
            <ta e="T329" id="Seg_13307" s="T328">adv:Time</ta>
            <ta e="T330" id="Seg_13308" s="T329">adv:Time</ta>
            <ta e="T332" id="Seg_13309" s="T331">0.3.h:A</ta>
            <ta e="T334" id="Seg_13310" s="T333">0.3.h:A</ta>
            <ta e="T335" id="Seg_13311" s="T334">pro.h:A</ta>
            <ta e="T336" id="Seg_13312" s="T335">np:So</ta>
            <ta e="T339" id="Seg_13313" s="T338">np:Th</ta>
            <ta e="T340" id="Seg_13314" s="T339">0.3.h:A</ta>
            <ta e="T341" id="Seg_13315" s="T340">0.3.h:A</ta>
            <ta e="T342" id="Seg_13316" s="T341">0.3.h:A</ta>
            <ta e="T344" id="Seg_13317" s="T342">0.3.h:A</ta>
            <ta e="T346" id="Seg_13318" s="T345">np.h:Th</ta>
            <ta e="T348" id="Seg_13319" s="T347">np.h:A</ta>
            <ta e="T352" id="Seg_13320" s="T351">np:G</ta>
            <ta e="T353" id="Seg_13321" s="T352">0.3.h:A</ta>
            <ta e="T354" id="Seg_13322" s="T353">np:So</ta>
            <ta e="T355" id="Seg_13323" s="T354">0.3.h:A</ta>
            <ta e="T356" id="Seg_13324" s="T355">adv:Time</ta>
            <ta e="T357" id="Seg_13325" s="T356">0.3.h:E</ta>
            <ta e="T360" id="Seg_13326" s="T359">np.h:A</ta>
            <ta e="T361" id="Seg_13327" s="T360">np.h:E</ta>
            <ta e="T363" id="Seg_13328" s="T362">adv:Time</ta>
            <ta e="T364" id="Seg_13329" s="T363">pro.h:A</ta>
            <ta e="T367" id="Seg_13330" s="T366">adv:L</ta>
            <ta e="T368" id="Seg_13331" s="T367">adv:L</ta>
            <ta e="T370" id="Seg_13332" s="T369">0.3.h:A</ta>
            <ta e="T371" id="Seg_13333" s="T370">0.3.h:A</ta>
            <ta e="T377" id="Seg_13334" s="T376">0.3.h:A</ta>
            <ta e="T378" id="Seg_13335" s="T377">adv:Time</ta>
            <ta e="T379" id="Seg_13336" s="T378">0.3.h:A</ta>
            <ta e="T380" id="Seg_13337" s="T379">np.h:E</ta>
            <ta e="T382" id="Seg_13338" s="T381">0.3.h:A</ta>
            <ta e="T385" id="Seg_13339" s="T384">0.3.h:A</ta>
            <ta e="T387" id="Seg_13340" s="T386">np.h:A</ta>
            <ta e="T389" id="Seg_13341" s="T388">0.2.h:A</ta>
            <ta e="T390" id="Seg_13342" s="T389">0.2.h:A</ta>
            <ta e="T392" id="Seg_13343" s="T391">pro.h:A</ta>
            <ta e="T395" id="Seg_13344" s="T394">np.h:Th</ta>
            <ta e="T396" id="Seg_13345" s="T395">0.3.h:A</ta>
            <ta e="T398" id="Seg_13346" s="T397">pro.h:A</ta>
            <ta e="T401" id="Seg_13347" s="T400">np.h:A</ta>
            <ta e="T405" id="Seg_13348" s="T404">pro.h:A</ta>
            <ta e="T406" id="Seg_13349" s="T405">np:G</ta>
            <ta e="T409" id="Seg_13350" s="T408">0.3.h:E</ta>
            <ta e="T411" id="Seg_13351" s="T410">np.h:A</ta>
            <ta e="T413" id="Seg_13352" s="T412">np.h:R</ta>
            <ta e="T414" id="Seg_13353" s="T413">0.3.h:A</ta>
            <ta e="T415" id="Seg_13354" s="T414">pro.h:Th</ta>
            <ta e="T416" id="Seg_13355" s="T415">np.h:Th</ta>
            <ta e="T418" id="Seg_13356" s="T417">np.h:Th</ta>
            <ta e="T423" id="Seg_13357" s="T422">pro.h:Th</ta>
            <ta e="T426" id="Seg_13358" s="T425">pro.h:E</ta>
            <ta e="T429" id="Seg_13359" s="T428">pro.h:Th</ta>
            <ta e="T431" id="Seg_13360" s="T430">np:Th 0.1.h:Poss</ta>
            <ta e="T432" id="Seg_13361" s="T431">adv:L</ta>
            <ta e="T434" id="Seg_13362" s="T433">0.2.h:A</ta>
            <ta e="T436" id="Seg_13363" s="T435">0.2.h:A</ta>
            <ta e="T441" id="Seg_13364" s="T440">adv:L</ta>
            <ta e="T442" id="Seg_13365" s="T441">0.2.h:Th</ta>
            <ta e="T443" id="Seg_13366" s="T442">adv:Time</ta>
            <ta e="T446" id="Seg_13367" s="T445">n:Time</ta>
            <ta e="T448" id="Seg_13368" s="T447">0.3.h:A</ta>
            <ta e="T450" id="Seg_13369" s="T449">0.3.h:A</ta>
            <ta e="T451" id="Seg_13370" s="T450">pro.h:A</ta>
            <ta e="T454" id="Seg_13371" s="T453">np:Th</ta>
            <ta e="T455" id="Seg_13372" s="T454">0.3.h:A</ta>
            <ta e="T456" id="Seg_13373" s="T455">0.3.h:A</ta>
            <ta e="T457" id="Seg_13374" s="T456">np:Th</ta>
            <ta e="T459" id="Seg_13375" s="T458">0.3.h:A</ta>
            <ta e="T461" id="Seg_13376" s="T460">np.h:Th</ta>
            <ta e="T463" id="Seg_13377" s="T462">np.h:A</ta>
            <ta e="T465" id="Seg_13378" s="T464">pro.h:A</ta>
            <ta e="T470" id="Seg_13379" s="T469">np:G</ta>
            <ta e="T472" id="Seg_13380" s="T471">np:So</ta>
            <ta e="T473" id="Seg_13381" s="T472">0.3.h:A</ta>
            <ta e="T474" id="Seg_13382" s="T473">0.3.h:A</ta>
            <ta e="T475" id="Seg_13383" s="T474">0.3.h:A</ta>
            <ta e="T476" id="Seg_13384" s="T475">adv:Time</ta>
            <ta e="T478" id="Seg_13385" s="T477">0.3.h:A</ta>
            <ta e="T479" id="Seg_13386" s="T478">pro.h:Th</ta>
            <ta e="T480" id="Seg_13387" s="T479">0.3.h:A</ta>
            <ta e="T482" id="Seg_13388" s="T481">np:Th</ta>
            <ta e="T483" id="Seg_13389" s="T482">0.3.h:A</ta>
            <ta e="T485" id="Seg_13390" s="T484">np:L</ta>
            <ta e="T487" id="Seg_13391" s="T486">0.3.h:A</ta>
            <ta e="T489" id="Seg_13392" s="T488">0.3.h:A</ta>
            <ta e="T490" id="Seg_13393" s="T489">np.h:Th</ta>
            <ta e="T491" id="Seg_13394" s="T490">0.3.h:A</ta>
            <ta e="T492" id="Seg_13395" s="T491">pro.h:A</ta>
            <ta e="T494" id="Seg_13396" s="T493">np:L</ta>
            <ta e="T496" id="Seg_13397" s="T495">np:G</ta>
            <ta e="T497" id="Seg_13398" s="T496">0.3.h:A</ta>
            <ta e="T498" id="Seg_13399" s="T497">0.3.h:E</ta>
            <ta e="T499" id="Seg_13400" s="T498">adv:Time</ta>
            <ta e="T500" id="Seg_13401" s="T499">np.h:A</ta>
            <ta e="T505" id="Seg_13402" s="T504">np.h:Th</ta>
            <ta e="T507" id="Seg_13403" s="T506">np.h:Th</ta>
            <ta e="T509" id="Seg_13404" s="T508">0.3.h:A</ta>
            <ta e="T510" id="Seg_13405" s="T509">np.h:Poss</ta>
            <ta e="T511" id="Seg_13406" s="T510">np.h:Th</ta>
            <ta e="T513" id="Seg_13407" s="T512">np.h:Poss</ta>
            <ta e="T516" id="Seg_13408" s="T515">np:Th</ta>
            <ta e="T517" id="Seg_13409" s="T516">0.3.h:A</ta>
            <ta e="T519" id="Seg_13410" s="T518">0.3.h:A</ta>
            <ta e="T520" id="Seg_13411" s="T519">adv:Time</ta>
            <ta e="T523" id="Seg_13412" s="T522">np.h:A</ta>
            <ta e="T526" id="Seg_13413" s="T525">np:Th</ta>
            <ta e="T527" id="Seg_13414" s="T526">np:Th</ta>
            <ta e="T528" id="Seg_13415" s="T527">0.3.h:A</ta>
            <ta e="T530" id="Seg_13416" s="T529">pro:Th</ta>
            <ta e="T531" id="Seg_13417" s="T530">adv:L</ta>
            <ta e="T533" id="Seg_13418" s="T532">0.3.h:A</ta>
            <ta e="T534" id="Seg_13419" s="T533">adv:Time</ta>
            <ta e="T535" id="Seg_13420" s="T534">0.2.h:A</ta>
            <ta e="T536" id="Seg_13421" s="T535">pro:So</ta>
            <ta e="T539" id="Seg_13422" s="T538">0.3.h:A</ta>
            <ta e="T540" id="Seg_13423" s="T539">np:P</ta>
            <ta e="T542" id="Seg_13424" s="T541">0.1.h:A</ta>
            <ta e="T543" id="Seg_13425" s="T542">adv:Time</ta>
            <ta e="T544" id="Seg_13426" s="T543">pro.h:A</ta>
            <ta e="T547" id="Seg_13427" s="T546">0.3.h:Poss</ta>
            <ta e="T548" id="Seg_13428" s="T547">pro:So</ta>
            <ta e="T552" id="Seg_13429" s="T551">pro.h:A</ta>
            <ta e="T553" id="Seg_13430" s="T552">0.3.h:A</ta>
            <ta e="T556" id="Seg_13431" s="T555">0.3.h:A</ta>
            <ta e="T558" id="Seg_13432" s="T557">np:P</ta>
            <ta e="T559" id="Seg_13433" s="T558">0.3.h:A</ta>
            <ta e="T560" id="Seg_13434" s="T559">adv:Time</ta>
            <ta e="T564" id="Seg_13435" s="T563">np.h:A</ta>
            <ta e="T566" id="Seg_13436" s="T565">pro.h:E</ta>
            <ta e="T571" id="Seg_13437" s="T570">np:Th</ta>
            <ta e="T573" id="Seg_13438" s="T572">np:Ins</ta>
            <ta e="T574" id="Seg_13439" s="T573">0.3.h:A</ta>
            <ta e="T575" id="Seg_13440" s="T574">0.3.h:A</ta>
            <ta e="T576" id="Seg_13441" s="T575">pro:Th</ta>
            <ta e="T577" id="Seg_13442" s="T576">pro.h:Poss</ta>
            <ta e="T578" id="Seg_13443" s="T577">np:L</ta>
            <ta e="T580" id="Seg_13444" s="T579">0.3.h:E</ta>
            <ta e="T583" id="Seg_13445" s="T582">adv:L</ta>
            <ta e="T584" id="Seg_13446" s="T583">np:Th</ta>
            <ta e="T586" id="Seg_13447" s="T585">adv:Time</ta>
            <ta e="T587" id="Seg_13448" s="T586">0.3.h:A</ta>
            <ta e="T588" id="Seg_13449" s="T587">np:G</ta>
            <ta e="T589" id="Seg_13450" s="T588">0.3.h:A</ta>
            <ta e="T592" id="Seg_13451" s="T591">pro.h:Th</ta>
            <ta e="T593" id="Seg_13452" s="T592">pro.h:Poss</ta>
            <ta e="T594" id="Seg_13453" s="T593">np.h:Th</ta>
            <ta e="T595" id="Seg_13454" s="T594">pro.h:P</ta>
            <ta e="T598" id="Seg_13455" s="T597">pro.h:A</ta>
            <ta e="T599" id="Seg_13456" s="T598">adv:L</ta>
            <ta e="T602" id="Seg_13457" s="T601">np:Th</ta>
            <ta e="T604" id="Seg_13458" s="T603">0.3.h:A</ta>
            <ta e="T607" id="Seg_13459" s="T606">0.3.h:P</ta>
            <ta e="T608" id="Seg_13460" s="T607">adv:Time</ta>
            <ta e="T611" id="Seg_13461" s="T610">np:P</ta>
            <ta e="T614" id="Seg_13462" s="T613">np.h:A</ta>
            <ta e="T619" id="Seg_13463" s="T618">0.3.h:P</ta>
            <ta e="T621" id="Seg_13464" s="T620">adv:L</ta>
            <ta e="T622" id="Seg_13465" s="T621">pro.h:Th</ta>
            <ta e="T624" id="Seg_13466" s="T623">np:Th</ta>
            <ta e="T627" id="Seg_13467" s="T626">0.3.h:A</ta>
            <ta e="T628" id="Seg_13468" s="T627">pro.h:R</ta>
            <ta e="T629" id="Seg_13469" s="T628">np:Th</ta>
            <ta e="T636" id="Seg_13470" s="T635">np.h:E</ta>
            <ta e="T638" id="Seg_13471" s="T637">np.h:E</ta>
            <ta e="T642" id="Seg_13472" s="T641">adv:Time</ta>
            <ta e="T644" id="Seg_13473" s="T643">0.3.h:A</ta>
            <ta e="T646" id="Seg_13474" s="T645">np.h:A</ta>
            <ta e="T649" id="Seg_13475" s="T648">0.3.h:A</ta>
            <ta e="T651" id="Seg_13476" s="T650">np.h:A</ta>
            <ta e="T653" id="Seg_13477" s="T652">pro.h:A</ta>
            <ta e="T655" id="Seg_13478" s="T654">pro.h:Th</ta>
            <ta e="T656" id="Seg_13479" s="T655">0.3.h:A</ta>
            <ta e="T658" id="Seg_13480" s="T657">np.h:P</ta>
            <ta e="T660" id="Seg_13481" s="T659">adv:Time</ta>
            <ta e="T662" id="Seg_13482" s="T661">0.2.h:A</ta>
            <ta e="T663" id="Seg_13483" s="T662">pro.h:A</ta>
            <ta e="T666" id="Seg_13484" s="T665">pro.h:A</ta>
            <ta e="T668" id="Seg_13485" s="T667">np:P</ta>
            <ta e="T670" id="Seg_13486" s="T669">0.3.h:A</ta>
            <ta e="T671" id="Seg_13487" s="T670">np:P</ta>
            <ta e="T672" id="Seg_13488" s="T671">pro.h:A</ta>
            <ta e="T674" id="Seg_13489" s="T673">pro.h:Th</ta>
            <ta e="T675" id="Seg_13490" s="T674">adv:L</ta>
            <ta e="T677" id="Seg_13491" s="T676">np.h:Th</ta>
            <ta e="T678" id="Seg_13492" s="T677">0.3.h:Th</ta>
            <ta e="T679" id="Seg_13493" s="T678">adv:Time</ta>
            <ta e="T681" id="Seg_13494" s="T680">n:Time</ta>
            <ta e="T684" id="Seg_13495" s="T683">np.h:A</ta>
            <ta e="T685" id="Seg_13496" s="T684">pro.h:A</ta>
            <ta e="T688" id="Seg_13497" s="T687">pro.h:Th</ta>
            <ta e="T689" id="Seg_13498" s="T688">0.3.h:A</ta>
            <ta e="T690" id="Seg_13499" s="T689">np.h:Th</ta>
            <ta e="T693" id="Seg_13500" s="T692">0.2.h:A</ta>
            <ta e="T694" id="Seg_13501" s="T693">pro.h:A</ta>
            <ta e="T696" id="Seg_13502" s="T695">0.3.h:A</ta>
            <ta e="T697" id="Seg_13503" s="T696">0.3.h:A</ta>
            <ta e="T698" id="Seg_13504" s="T697">adv:Time</ta>
            <ta e="T700" id="Seg_13505" s="T699">pro.h:A</ta>
            <ta e="T701" id="Seg_13506" s="T700">adv:L</ta>
            <ta e="T703" id="Seg_13507" s="T702">np.h:A</ta>
            <ta e="T705" id="Seg_13508" s="T704">adv:Time</ta>
            <ta e="T707" id="Seg_13509" s="T706">n:Time</ta>
            <ta e="T709" id="Seg_13510" s="T708">0.3.h:A</ta>
            <ta e="T710" id="Seg_13511" s="T709">adv:Time</ta>
            <ta e="T711" id="Seg_13512" s="T710">pro.h:A</ta>
            <ta e="T714" id="Seg_13513" s="T713">pro.h:Th</ta>
            <ta e="T715" id="Seg_13514" s="T714">0.3.h:A</ta>
            <ta e="T717" id="Seg_13515" s="T716">0.2.h:A</ta>
            <ta e="T718" id="Seg_13516" s="T717">adv:Time</ta>
            <ta e="T719" id="Seg_13517" s="T718">pro.h:A</ta>
            <ta e="T722" id="Seg_13518" s="T721">0.3.h:A</ta>
            <ta e="T723" id="Seg_13519" s="T722">pro.h:Th</ta>
            <ta e="T724" id="Seg_13520" s="T723">adv:L</ta>
            <ta e="T725" id="Seg_13521" s="T724">pro.h:A</ta>
            <ta e="T728" id="Seg_13522" s="T727">adv:Time</ta>
            <ta e="T729" id="Seg_13523" s="T728">n:Time</ta>
            <ta e="T730" id="Seg_13524" s="T729">0.3.h:A</ta>
            <ta e="T731" id="Seg_13525" s="T730">pro.h:E</ta>
            <ta e="T733" id="Seg_13526" s="T732">0.3.h:A</ta>
            <ta e="T734" id="Seg_13527" s="T733">0.2.h:A</ta>
            <ta e="T735" id="Seg_13528" s="T734">pro.h:R</ta>
            <ta e="T736" id="Seg_13529" s="T735">np:Th</ta>
            <ta e="T737" id="Seg_13530" s="T736">pro.h:A</ta>
            <ta e="T740" id="Seg_13531" s="T739">0.3:Th</ta>
            <ta e="T741" id="Seg_13532" s="T740">pro.h:A</ta>
            <ta e="T744" id="Seg_13533" s="T743">pro.h:A</ta>
            <ta e="T746" id="Seg_13534" s="T745">pro.h:A</ta>
            <ta e="T749" id="Seg_13535" s="T748">pro.h:A</ta>
            <ta e="T752" id="Seg_13536" s="T751">adv:Time</ta>
            <ta e="T754" id="Seg_13537" s="T753">np:L</ta>
            <ta e="T755" id="Seg_13538" s="T754">0.1.h:A</ta>
            <ta e="T756" id="Seg_13539" s="T755">pro.h:A</ta>
            <ta e="T758" id="Seg_13540" s="T757">adv:L</ta>
            <ta e="T760" id="Seg_13541" s="T759">np:P</ta>
            <ta e="T762" id="Seg_13542" s="T761">adv:Time</ta>
            <ta e="T763" id="Seg_13543" s="T762">0.3.h:E</ta>
            <ta e="T765" id="Seg_13544" s="T764">np:Th</ta>
            <ta e="T767" id="Seg_13545" s="T766">0.3:Th</ta>
            <ta e="T768" id="Seg_13546" s="T767">pro.h:A</ta>
            <ta e="T771" id="Seg_13547" s="T770">np:L</ta>
            <ta e="T773" id="Seg_13548" s="T772">0.2.h:A</ta>
            <ta e="T774" id="Seg_13549" s="T773">pro.h:A</ta>
            <ta e="T777" id="Seg_13550" s="T776">adv:Time</ta>
            <ta e="T778" id="Seg_13551" s="T777">pro.h:A</ta>
            <ta e="T781" id="Seg_13552" s="T780">pro.h:A</ta>
            <ta e="T783" id="Seg_13553" s="T782">adv:Time</ta>
            <ta e="T785" id="Seg_13554" s="T784">np.h:E</ta>
            <ta e="T787" id="Seg_13555" s="T786">np.h:E</ta>
            <ta e="T790" id="Seg_13556" s="T789">np:P</ta>
            <ta e="T792" id="Seg_13557" s="T791">np:Th</ta>
            <ta e="T794" id="Seg_13558" s="T793">pro.h:A</ta>
            <ta e="T795" id="Seg_13559" s="T794">np:P</ta>
            <ta e="T799" id="Seg_13560" s="T798">pro.h:B</ta>
            <ta e="T802" id="Seg_13561" s="T801">pro.h:Th</ta>
            <ta e="T803" id="Seg_13562" s="T802">np:L</ta>
            <ta e="T806" id="Seg_13563" s="T805">0.3.h:A</ta>
            <ta e="T808" id="Seg_13564" s="T807">np:G</ta>
            <ta e="T809" id="Seg_13565" s="T808">adv:L</ta>
            <ta e="T810" id="Seg_13566" s="T809">0.3.h:Th</ta>
            <ta e="T811" id="Seg_13567" s="T810">adv:Time</ta>
            <ta e="T812" id="Seg_13568" s="T811">0.3.h:A</ta>
            <ta e="T814" id="Seg_13569" s="T813">0.3.h:A</ta>
            <ta e="T816" id="Seg_13570" s="T815">np.h:Th</ta>
            <ta e="T817" id="Seg_13571" s="T816">0.3.h:E</ta>
            <ta e="T818" id="Seg_13572" s="T817">adv:L</ta>
            <ta e="T819" id="Seg_13573" s="T818">0.3.h:A</ta>
            <ta e="T820" id="Seg_13574" s="T819">0.3.h:A</ta>
            <ta e="T822" id="Seg_13575" s="T821">np:G</ta>
            <ta e="T823" id="Seg_13576" s="T822">adv:Time</ta>
            <ta e="T824" id="Seg_13577" s="T823">pro.h:B</ta>
            <ta e="T825" id="Seg_13578" s="T824">np:P</ta>
            <ta e="T826" id="Seg_13579" s="T825">0.3.h:A</ta>
            <ta e="T827" id="Seg_13580" s="T826">np:Th</ta>
            <ta e="T828" id="Seg_13581" s="T827">0.3.h:A</ta>
            <ta e="T830" id="Seg_13582" s="T829">0.3.h:P</ta>
            <ta e="T831" id="Seg_13583" s="T830">adv:Time</ta>
            <ta e="T832" id="Seg_13584" s="T831">np.h:Th</ta>
            <ta e="T833" id="Seg_13585" s="T832">0.3.h:A</ta>
            <ta e="T834" id="Seg_13586" s="T833">np:Ins</ta>
            <ta e="T835" id="Seg_13587" s="T834">np:Ins</ta>
            <ta e="T836" id="Seg_13588" s="T835">adv:Time</ta>
            <ta e="T839" id="Seg_13589" s="T838">pro.h:B</ta>
            <ta e="T840" id="Seg_13590" s="T839">np:Th</ta>
            <ta e="T841" id="Seg_13591" s="T840">np:L</ta>
            <ta e="T842" id="Seg_13592" s="T841">0.1.h:E</ta>
            <ta e="T844" id="Seg_13593" s="T843">n:Time</ta>
            <ta e="T845" id="Seg_13594" s="T844">np:L</ta>
            <ta e="T846" id="Seg_13595" s="T845">0.1.h:Th</ta>
            <ta e="T849" id="Seg_13596" s="T848">adv:L</ta>
            <ta e="T850" id="Seg_13597" s="T849">0.1.h:A</ta>
            <ta e="T853" id="Seg_13598" s="T852">np.h:E</ta>
            <ta e="T855" id="Seg_13599" s="T854">np.h:E</ta>
            <ta e="T857" id="Seg_13600" s="T856">pro.h:Poss</ta>
            <ta e="T858" id="Seg_13601" s="T857">pro:Th</ta>
            <ta e="T860" id="Seg_13602" s="T859">0.3.h:A</ta>
            <ta e="T861" id="Seg_13603" s="T860">np:G</ta>
            <ta e="T862" id="Seg_13604" s="T861">np:Th</ta>
            <ta e="T864" id="Seg_13605" s="T863">0.3.h:A</ta>
            <ta e="T865" id="Seg_13606" s="T864">0.3.h:A</ta>
            <ta e="T866" id="Seg_13607" s="T865">0.3.h:A</ta>
            <ta e="T868" id="Seg_13608" s="T867">np:Th</ta>
            <ta e="T870" id="Seg_13609" s="T869">np:G</ta>
            <ta e="T871" id="Seg_13610" s="T870">adv:Time</ta>
            <ta e="T875" id="Seg_13611" s="T874">np.h:A</ta>
            <ta e="T877" id="Seg_13612" s="T876">np:Ins</ta>
            <ta e="T878" id="Seg_13613" s="T877">0.2.h:A</ta>
            <ta e="T879" id="Seg_13614" s="T878">np:P</ta>
            <ta e="T880" id="Seg_13615" s="T879">pro.h:A</ta>
            <ta e="T882" id="Seg_13616" s="T881">pro:P</ta>
            <ta e="T883" id="Seg_13617" s="T882">adv:L</ta>
            <ta e="T885" id="Seg_13618" s="T884">adv:Time</ta>
            <ta e="T886" id="Seg_13619" s="T885">adv:L</ta>
            <ta e="T890" id="Seg_13620" s="T889">pro.h:A</ta>
            <ta e="T891" id="Seg_13621" s="T890">adv:L</ta>
            <ta e="T892" id="Seg_13622" s="T891">np:Th</ta>
            <ta e="T894" id="Seg_13623" s="T893">adv:L</ta>
            <ta e="T896" id="Seg_13624" s="T895">0.3:P</ta>
            <ta e="T899" id="Seg_13625" s="T898">adv:Time</ta>
            <ta e="T900" id="Seg_13626" s="T899">np.h:A</ta>
            <ta e="T904" id="Seg_13627" s="T903">np:Th</ta>
            <ta e="T905" id="Seg_13628" s="T904">0.3.h:A</ta>
            <ta e="T906" id="Seg_13629" s="T905">0.3.h:A</ta>
            <ta e="T908" id="Seg_13630" s="T907">adv:L</ta>
            <ta e="T910" id="Seg_13631" s="T909">adv:L</ta>
            <ta e="T911" id="Seg_13632" s="T910">0.3.h:E</ta>
            <ta e="T912" id="Seg_13633" s="T911">np:Th</ta>
            <ta e="T915" id="Seg_13634" s="T914">adv:L</ta>
            <ta e="T916" id="Seg_13635" s="T915">np:G</ta>
            <ta e="T917" id="Seg_13636" s="T916">0.3.h:A</ta>
            <ta e="T918" id="Seg_13637" s="T917">adv:L</ta>
            <ta e="T919" id="Seg_13638" s="T918">np.h:Poss</ta>
            <ta e="T920" id="Seg_13639" s="T919">np.h:E</ta>
            <ta e="T922" id="Seg_13640" s="T921">np:Th</ta>
            <ta e="T927" id="Seg_13641" s="T926">adv:L</ta>
            <ta e="T928" id="Seg_13642" s="T927">np:Th</ta>
            <ta e="T930" id="Seg_13643" s="T929">pro.h:A</ta>
            <ta e="T932" id="Seg_13644" s="T931">pro:Th</ta>
            <ta e="T934" id="Seg_13645" s="T933">0.3.h:A</ta>
            <ta e="T936" id="Seg_13646" s="T935">pro.h:B</ta>
            <ta e="T937" id="Seg_13647" s="T936">np.h:Poss</ta>
            <ta e="T941" id="Seg_13648" s="T940">pro.h:A</ta>
            <ta e="T947" id="Seg_13649" s="T946">np:Th</ta>
            <ta e="T949" id="Seg_13650" s="T948">adv:L</ta>
            <ta e="T950" id="Seg_13651" s="T949">np:Th</ta>
            <ta e="T951" id="Seg_13652" s="T950">np:Th</ta>
            <ta e="T952" id="Seg_13653" s="T951">np:Th</ta>
            <ta e="T953" id="Seg_13654" s="T952">np:Th</ta>
            <ta e="T954" id="Seg_13655" s="T953">np.h:A</ta>
            <ta e="T957" id="Seg_13656" s="T956">np.h:A</ta>
            <ta e="T962" id="Seg_13657" s="T961">adv:Time</ta>
            <ta e="T965" id="Seg_13658" s="T964">np.h:A</ta>
            <ta e="T966" id="Seg_13659" s="T965">0.2.h:A</ta>
            <ta e="T967" id="Seg_13660" s="T966">pro.h:R</ta>
            <ta e="T969" id="Seg_13661" s="T968">pro.h:E</ta>
            <ta e="T976" id="Seg_13662" s="T975">np:Th</ta>
            <ta e="T977" id="Seg_13663" s="T976">adv:Time</ta>
            <ta e="T978" id="Seg_13664" s="T977">np.h:A</ta>
            <ta e="T981" id="Seg_13665" s="T980">np:Th</ta>
            <ta e="T983" id="Seg_13666" s="T982">np:Th</ta>
            <ta e="T984" id="Seg_13667" s="T983">np:Th</ta>
            <ta e="T985" id="Seg_13668" s="T984">pro.h:A</ta>
            <ta e="T987" id="Seg_13669" s="T986">0.2.h:A</ta>
            <ta e="T988" id="Seg_13670" s="T987">pro.h:R</ta>
            <ta e="T990" id="Seg_13671" s="T989">pro.h:A</ta>
            <ta e="T995" id="Seg_13672" s="T994">pro.h:B</ta>
            <ta e="T997" id="Seg_13673" s="T996">pro:Th</ta>
            <ta e="T998" id="Seg_13674" s="T997">adv:Time</ta>
            <ta e="T1000" id="Seg_13675" s="T999">n:Time</ta>
            <ta e="T1001" id="Seg_13676" s="T1000">0.3.h:A</ta>
            <ta e="T1004" id="Seg_13677" s="T1003">0.3.h:A</ta>
            <ta e="T1006" id="Seg_13678" s="T1005">0.3.h:A</ta>
            <ta e="T1007" id="Seg_13679" s="T1006">n:Time</ta>
            <ta e="T1008" id="Seg_13680" s="T1007">pro.h:A</ta>
            <ta e="T1011" id="Seg_13681" s="T1010">np:Th</ta>
            <ta e="T1012" id="Seg_13682" s="T1011">adv:Time</ta>
            <ta e="T1013" id="Seg_13683" s="T1012">np.h:A</ta>
            <ta e="T1014" id="Seg_13684" s="T1013">np.h:Poss</ta>
            <ta e="T1015" id="Seg_13685" s="T1014">0.2.h:E</ta>
            <ta e="T1017" id="Seg_13686" s="T1016">pro.h:A</ta>
            <ta e="T1020" id="Seg_13687" s="T1019">0.1.h:A</ta>
            <ta e="T1022" id="Seg_13688" s="T1021">pro:G</ta>
            <ta e="T1023" id="Seg_13689" s="T1022">0.3.h:A</ta>
            <ta e="T1024" id="Seg_13690" s="T1023">0.3.h:A</ta>
            <ta e="T1025" id="Seg_13691" s="T1024">0.3.h:A</ta>
            <ta e="T1027" id="Seg_13692" s="T1026">pro:G</ta>
            <ta e="T1028" id="Seg_13693" s="T1027">np.h:A</ta>
            <ta e="T1030" id="Seg_13694" s="T1029">0.2.h:A</ta>
            <ta e="T1031" id="Seg_13695" s="T1030">0.2.h:A</ta>
            <ta e="T1032" id="Seg_13696" s="T1031">pro.h:Th</ta>
            <ta e="T1038" id="Seg_13697" s="T1037">adv:Time</ta>
            <ta e="T1039" id="Seg_13698" s="T1038">0.3.h:A</ta>
            <ta e="T1040" id="Seg_13699" s="T1039">0.3.h:A</ta>
            <ta e="T1041" id="Seg_13700" s="T1040">np:G</ta>
            <ta e="T1043" id="Seg_13701" s="T1042">0.2.h:A</ta>
            <ta e="T1044" id="Seg_13702" s="T1043">np:Th</ta>
            <ta e="T1047" id="Seg_13703" s="T1046">0.2.h:A</ta>
            <ta e="T1048" id="Seg_13704" s="T1047">pro.h:Th</ta>
            <ta e="T1049" id="Seg_13705" s="T1048">0.2.h:A</ta>
            <ta e="T1050" id="Seg_13706" s="T1049">pro:G</ta>
            <ta e="T1051" id="Seg_13707" s="T1050">np:L</ta>
            <ta e="T1052" id="Seg_13708" s="T1051">pro.h:A</ta>
            <ta e="T1054" id="Seg_13709" s="T1053">adv:Time</ta>
            <ta e="T1055" id="Seg_13710" s="T1054">0.3.h:A</ta>
            <ta e="T1056" id="Seg_13711" s="T1055">np.h:A</ta>
            <ta e="T1059" id="Seg_13712" s="T1058">pro:G</ta>
            <ta e="T1060" id="Seg_13713" s="T1059">adv:Time</ta>
            <ta e="T1061" id="Seg_13714" s="T1060">0.2.h:A</ta>
            <ta e="T1065" id="Seg_13715" s="T1064">0.2.h:A</ta>
            <ta e="T1066" id="Seg_13716" s="T1065">pro.h:Poss</ta>
            <ta e="T1067" id="Seg_13717" s="T1066">np:Th</ta>
            <ta e="T1068" id="Seg_13718" s="T1067">0.2.h:A</ta>
            <ta e="T1069" id="Seg_13719" s="T1068">pro.h:Poss</ta>
            <ta e="T1070" id="Seg_13720" s="T1069">np:Th</ta>
            <ta e="T1071" id="Seg_13721" s="T1070">pro.h:A</ta>
            <ta e="T1074" id="Seg_13722" s="T1073">0.2.h:A</ta>
            <ta e="T1075" id="Seg_13723" s="T1074">pro.h:Th</ta>
            <ta e="T1077" id="Seg_13724" s="T1076">np:G</ta>
            <ta e="T1079" id="Seg_13725" s="T1078">np.h:A</ta>
            <ta e="T1081" id="Seg_13726" s="T1080">np:P</ta>
            <ta e="T1082" id="Seg_13727" s="T1081">pro.h:Th</ta>
            <ta e="T1083" id="Seg_13728" s="T1082">0.3.h:A</ta>
            <ta e="T1085" id="Seg_13729" s="T1084">pro.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-PKZ">
            <ta e="T5" id="Seg_13730" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_13731" s="T5">np.h:S</ta>
            <ta e="T9" id="Seg_13732" s="T8">np.h:S</ta>
            <ta e="T10" id="Seg_13733" s="T9">v:pred</ta>
            <ta e="T11" id="Seg_13734" s="T10">np:S</ta>
            <ta e="T12" id="Seg_13735" s="T11">n:pred</ta>
            <ta e="T13" id="Seg_13736" s="T12">np:S</ta>
            <ta e="T14" id="Seg_13737" s="T13">n:pred</ta>
            <ta e="T15" id="Seg_13738" s="T14">np:S</ta>
            <ta e="T16" id="Seg_13739" s="T15">n:pred</ta>
            <ta e="T17" id="Seg_13740" s="T16">n:pred</ta>
            <ta e="T21" id="Seg_13741" s="T20">np.h:S</ta>
            <ta e="T22" id="Seg_13742" s="T21">v:pred</ta>
            <ta e="T23" id="Seg_13743" s="T22">np:O</ta>
            <ta e="T26" id="Seg_13744" s="T25">pro.h:S</ta>
            <ta e="T30" id="Seg_13745" s="T29">v:pred</ta>
            <ta e="T32" id="Seg_13746" s="T31">v:pred 0.3.h:S</ta>
            <ta e="T33" id="Seg_13747" s="T32">np:O</ta>
            <ta e="T34" id="Seg_13748" s="T33">ptcl:pred</ta>
            <ta e="T35" id="Seg_13749" s="T34">v:pred 0.3.h:S</ta>
            <ta e="T39" id="Seg_13750" s="T38">pro.h:S</ta>
            <ta e="T40" id="Seg_13751" s="T39">v:pred</ta>
            <ta e="T41" id="Seg_13752" s="T40">np:O</ta>
            <ta e="T42" id="Seg_13753" s="T41">ptcl:pred</ta>
            <ta e="T45" id="Seg_13754" s="T44">v:pred</ta>
            <ta e="T48" id="Seg_13755" s="T47">np.h:S</ta>
            <ta e="T50" id="Seg_13756" s="T49">v:pred 0.3.h:S</ta>
            <ta e="T51" id="Seg_13757" s="T50">v:pred 0.3.h:S</ta>
            <ta e="T54" id="Seg_13758" s="T53">v:pred 0.3.h:S</ta>
            <ta e="T56" id="Seg_13759" s="T55">v:pred 0.1.h:S</ta>
            <ta e="T57" id="Seg_13760" s="T56">ptcl.neg</ta>
            <ta e="T58" id="Seg_13761" s="T57">v:pred 0.1.h:S</ta>
            <ta e="T60" id="Seg_13762" s="T59">pro.h:O</ta>
            <ta e="T61" id="Seg_13763" s="T60">ptcl.neg</ta>
            <ta e="T62" id="Seg_13764" s="T61">v:pred 0.1.h:S</ta>
            <ta e="T65" id="Seg_13765" s="T64">np.h:S</ta>
            <ta e="T66" id="Seg_13766" s="T65">v:pred</ta>
            <ta e="T68" id="Seg_13767" s="T67">v:pred 0.3.h:S</ta>
            <ta e="T69" id="Seg_13768" s="T68">v:pred 0.3.h:S</ta>
            <ta e="T73" id="Seg_13769" s="T72">v:pred 0.3.h:S</ta>
            <ta e="T76" id="Seg_13770" s="T75">ptcl.neg</ta>
            <ta e="T77" id="Seg_13771" s="T76">v:pred 0.1.h:S</ta>
            <ta e="T79" id="Seg_13772" s="T78">pro.h:O</ta>
            <ta e="T80" id="Seg_13773" s="T79">ptcl.neg</ta>
            <ta e="T81" id="Seg_13774" s="T80">v:pred 0.1.h:S</ta>
            <ta e="T83" id="Seg_13775" s="T82">np.h:S</ta>
            <ta e="T84" id="Seg_13776" s="T83">v:pred</ta>
            <ta e="T85" id="Seg_13777" s="T84">v:pred 0.3.h:S</ta>
            <ta e="T86" id="Seg_13778" s="T85">np:O</ta>
            <ta e="T88" id="Seg_13779" s="T87">v:pred 0.3.h:S</ta>
            <ta e="T90" id="Seg_13780" s="T89">ptcl.neg</ta>
            <ta e="T91" id="Seg_13781" s="T90">v:pred 0.3.h:S</ta>
            <ta e="T92" id="Seg_13782" s="T91">v:pred 0.3.h:S</ta>
            <ta e="T93" id="Seg_13783" s="T92">v:pred 0.3.h:S</ta>
            <ta e="T94" id="Seg_13784" s="T93">np.h:S</ta>
            <ta e="T95" id="Seg_13785" s="T94">v:pred</ta>
            <ta e="T101" id="Seg_13786" s="T100">np:S</ta>
            <ta e="T102" id="Seg_13787" s="T101">adj:pred</ta>
            <ta e="T103" id="Seg_13788" s="T102">pro.h:S</ta>
            <ta e="T105" id="Seg_13789" s="T104">v:pred</ta>
            <ta e="T106" id="Seg_13790" s="T105">np:O</ta>
            <ta e="T109" id="Seg_13791" s="T108">v:pred 0.3.h:S</ta>
            <ta e="T110" id="Seg_13792" s="T109">pro.h:S</ta>
            <ta e="T111" id="Seg_13793" s="T110">v:pred</ta>
            <ta e="T112" id="Seg_13794" s="T111">ptcl.neg</ta>
            <ta e="T113" id="Seg_13795" s="T112">v:pred 0.3.h:S</ta>
            <ta e="T116" id="Seg_13796" s="T115">v:pred 0.2.h:S</ta>
            <ta e="T117" id="Seg_13797" s="T116">pro.h:O</ta>
            <ta e="T118" id="Seg_13798" s="T117">pro.h:S</ta>
            <ta e="T121" id="Seg_13799" s="T120">pro:O</ta>
            <ta e="T122" id="Seg_13800" s="T121">v:pred</ta>
            <ta e="T125" id="Seg_13801" s="T124">v:pred 0.2.h:S</ta>
            <ta e="T127" id="Seg_13802" s="T126">ptcl.neg</ta>
            <ta e="T128" id="Seg_13803" s="T127">v:pred 0.2.h:S</ta>
            <ta e="T129" id="Seg_13804" s="T128">np:O</ta>
            <ta e="T130" id="Seg_13805" s="T129">ptcl.neg</ta>
            <ta e="T131" id="Seg_13806" s="T130">v:pred 0.1.h:S</ta>
            <ta e="T134" id="Seg_13807" s="T133">v:pred 0.2.h:S</ta>
            <ta e="T137" id="Seg_13808" s="T136">v:pred 0.2.h:S</ta>
            <ta e="T146" id="Seg_13809" s="T145">v:pred</ta>
            <ta e="T147" id="Seg_13810" s="T146">np.h:S</ta>
            <ta e="T149" id="Seg_13811" s="T148">pro.h:S</ta>
            <ta e="T150" id="Seg_13812" s="T149">v:pred</ta>
            <ta e="T153" id="Seg_13813" s="T152">v:pred 0.3.h:S</ta>
            <ta e="T154" id="Seg_13814" s="T153">pro.h:S</ta>
            <ta e="T155" id="Seg_13815" s="T154">v:pred</ta>
            <ta e="T156" id="Seg_13816" s="T155">np:O</ta>
            <ta e="T160" id="Seg_13817" s="T159">ptcl.neg</ta>
            <ta e="T161" id="Seg_13818" s="T160">v:pred 0.3.h:S</ta>
            <ta e="T162" id="Seg_13819" s="T161">s:purp</ta>
            <ta e="T163" id="Seg_13820" s="T162">np:O</ta>
            <ta e="T165" id="Seg_13821" s="T164">np.h:S</ta>
            <ta e="T166" id="Seg_13822" s="T165">v:pred</ta>
            <ta e="T168" id="Seg_13823" s="T167">np.h:S</ta>
            <ta e="T169" id="Seg_13824" s="T168">v:pred</ta>
            <ta e="T172" id="Seg_13825" s="T171">np.h:S</ta>
            <ta e="T173" id="Seg_13826" s="T172">v:pred</ta>
            <ta e="T176" id="Seg_13827" s="T175">np.h:S</ta>
            <ta e="T177" id="Seg_13828" s="T176">v:pred</ta>
            <ta e="T181" id="Seg_13829" s="T180">np.h:S</ta>
            <ta e="T182" id="Seg_13830" s="T181">v:pred</ta>
            <ta e="T184" id="Seg_13831" s="T183">pro.h:S</ta>
            <ta e="T185" id="Seg_13832" s="T184">v:pred</ta>
            <ta e="T186" id="Seg_13833" s="T185">v:pred 0.2.h:S</ta>
            <ta e="T187" id="Seg_13834" s="T186">pro.h:O</ta>
            <ta e="T190" id="Seg_13835" s="T189">v:pred 0.2.h:S</ta>
            <ta e="T191" id="Seg_13836" s="T190">pro:O</ta>
            <ta e="T193" id="Seg_13837" s="T192">ptcl.neg</ta>
            <ta e="T194" id="Seg_13838" s="T193">v:pred 0.2.h:S</ta>
            <ta e="T196" id="Seg_13839" s="T195">np.h:S</ta>
            <ta e="T197" id="Seg_13840" s="T196">v:pred</ta>
            <ta e="T200" id="Seg_13841" s="T199">pro.h:S</ta>
            <ta e="T201" id="Seg_13842" s="T200">conv:pred</ta>
            <ta e="T202" id="Seg_13843" s="T201">v:pred</ta>
            <ta e="T203" id="Seg_13844" s="T202">pro.h:S</ta>
            <ta e="T204" id="Seg_13845" s="T203">v:pred</ta>
            <ta e="T205" id="Seg_13846" s="T204">np:O</ta>
            <ta e="T206" id="Seg_13847" s="T205">v:pred 0.3.h:S</ta>
            <ta e="T207" id="Seg_13848" s="T206">v:pred 0.3.h:S</ta>
            <ta e="T209" id="Seg_13849" s="T208">ptcl:pred</ta>
            <ta e="T211" id="Seg_13850" s="T210">np.h:S</ta>
            <ta e="T212" id="Seg_13851" s="T211">v:pred</ta>
            <ta e="T216" id="Seg_13852" s="T215">v:pred 0.3.h:S</ta>
            <ta e="T217" id="Seg_13853" s="T216">pro.h:S</ta>
            <ta e="T219" id="Seg_13854" s="T218">v:pred</ta>
            <ta e="T222" id="Seg_13855" s="T221">np.h:S</ta>
            <ta e="T224" id="Seg_13856" s="T222">v:pred</ta>
            <ta e="T225" id="Seg_13857" s="T224">v:pred 0.3.h:S</ta>
            <ta e="T227" id="Seg_13858" s="T226">v:pred 0.3.h:S</ta>
            <ta e="T230" id="Seg_13859" s="T229">v:pred 0.2.h:S</ta>
            <ta e="T232" id="Seg_13860" s="T231">v:pred 0.2.h:S</ta>
            <ta e="T233" id="Seg_13861" s="T232">pro.h:S</ta>
            <ta e="T234" id="Seg_13862" s="T233">v:pred</ta>
            <ta e="T235" id="Seg_13863" s="T234">v:pred 0.3.h:S</ta>
            <ta e="T236" id="Seg_13864" s="T235">adj:pred</ta>
            <ta e="T237" id="Seg_13865" s="T236">cop 0.3.h:S</ta>
            <ta e="T240" id="Seg_13866" s="T239">v:pred 0.3.h:S</ta>
            <ta e="T241" id="Seg_13867" s="T240">pro.h:S</ta>
            <ta e="T243" id="Seg_13868" s="T242">v:pred</ta>
            <ta e="T245" id="Seg_13869" s="T244">np.h:S</ta>
            <ta e="T247" id="Seg_13870" s="T246">v:pred</ta>
            <ta e="T249" id="Seg_13871" s="T248">pro.h:S</ta>
            <ta e="T250" id="Seg_13872" s="T249">ptcl.neg</ta>
            <ta e="T253" id="Seg_13873" s="T252">ptcl.neg</ta>
            <ta e="T254" id="Seg_13874" s="T253">v:pred</ta>
            <ta e="T255" id="Seg_13875" s="T254">pro.h:S</ta>
            <ta e="T257" id="Seg_13876" s="T256">v:pred</ta>
            <ta e="T258" id="Seg_13877" s="T257">v:pred 0.3.h:S</ta>
            <ta e="T259" id="Seg_13878" s="T258">ptcl.neg</ta>
            <ta e="T260" id="Seg_13879" s="T259">ptcl.neg</ta>
            <ta e="T261" id="Seg_13880" s="T260">v:pred 0.3.h:S</ta>
            <ta e="T263" id="Seg_13881" s="T262">np.h:S</ta>
            <ta e="T265" id="Seg_13882" s="T264">v:pred</ta>
            <ta e="T266" id="Seg_13883" s="T265">v:pred 0.2.h:S</ta>
            <ta e="T267" id="Seg_13884" s="T266">v:pred 0.2.h:S</ta>
            <ta e="T269" id="Seg_13885" s="T268">pro.h:S</ta>
            <ta e="T270" id="Seg_13886" s="T269">v:pred</ta>
            <ta e="T271" id="Seg_13887" s="T270">np.h:O</ta>
            <ta e="T272" id="Seg_13888" s="T271">v:pred 0.3.h:S</ta>
            <ta e="T274" id="Seg_13889" s="T273">np:O</ta>
            <ta e="T275" id="Seg_13890" s="T274">v:pred 0.3.h:S</ta>
            <ta e="T279" id="Seg_13891" s="T278">np:O</ta>
            <ta e="T280" id="Seg_13892" s="T279">v:pred 0.3.h:S</ta>
            <ta e="T282" id="Seg_13893" s="T281">np.h:S</ta>
            <ta e="T283" id="Seg_13894" s="T282">v:pred</ta>
            <ta e="T287" id="Seg_13895" s="T286">v:pred 0.2.h:S</ta>
            <ta e="T290" id="Seg_13896" s="T289">s:purp</ta>
            <ta e="T291" id="Seg_13897" s="T290">pro.h:S</ta>
            <ta e="T294" id="Seg_13898" s="T293">v:pred</ta>
            <ta e="T295" id="Seg_13899" s="T294">v:pred 0.3.h:S</ta>
            <ta e="T298" id="Seg_13900" s="T297">np.h:S</ta>
            <ta e="T299" id="Seg_13901" s="T298">v:pred</ta>
            <ta e="T301" id="Seg_13902" s="T300">v:pred 0.3.h:S</ta>
            <ta e="T304" id="Seg_13903" s="T303">v:pred 0.3:S</ta>
            <ta e="T306" id="Seg_13904" s="T305">pro.h:S</ta>
            <ta e="T307" id="Seg_13905" s="T306">v:pred</ta>
            <ta e="T308" id="Seg_13906" s="T307">pro.h:S</ta>
            <ta e="T310" id="Seg_13907" s="T309">n:pred</ta>
            <ta e="T312" id="Seg_13908" s="T311">cop</ta>
            <ta e="T313" id="Seg_13909" s="T312">v:pred 0.2.h:S</ta>
            <ta e="T317" id="Seg_13910" s="T316">v:pred 0.2.h:S</ta>
            <ta e="T319" id="Seg_13911" s="T318">pro:O</ta>
            <ta e="T320" id="Seg_13912" s="T319">v:pred 0.2.h:S</ta>
            <ta e="T323" id="Seg_13913" s="T322">n:pred</ta>
            <ta e="T324" id="Seg_13914" s="T323">cop 0.3.h:S</ta>
            <ta e="T325" id="Seg_13915" s="T324">np.h:S</ta>
            <ta e="T326" id="Seg_13916" s="T325">adj:pred</ta>
            <ta e="T327" id="Seg_13917" s="T326">cop</ta>
            <ta e="T332" id="Seg_13918" s="T331">v:pred 0.3.h:S</ta>
            <ta e="T333" id="Seg_13919" s="T332">conv:pred</ta>
            <ta e="T334" id="Seg_13920" s="T333">v:pred 0.3.h:S</ta>
            <ta e="T335" id="Seg_13921" s="T334">pro.h:S</ta>
            <ta e="T338" id="Seg_13922" s="T337">v:pred</ta>
            <ta e="T339" id="Seg_13923" s="T338">np:O</ta>
            <ta e="T340" id="Seg_13924" s="T339">v:pred 0.3.h:S</ta>
            <ta e="T341" id="Seg_13925" s="T340">v:pred 0.3.h:S</ta>
            <ta e="T342" id="Seg_13926" s="T341">v:pred 0.3.h:S</ta>
            <ta e="T344" id="Seg_13927" s="T342">v:pred 0.3.h:S</ta>
            <ta e="T345" id="Seg_13928" s="T344">ptcl:pred</ta>
            <ta e="T346" id="Seg_13929" s="T345">np.h:O</ta>
            <ta e="T348" id="Seg_13930" s="T347">np.h:S</ta>
            <ta e="T349" id="Seg_13931" s="T348">v:pred</ta>
            <ta e="T353" id="Seg_13932" s="T352">v:pred 0.3.h:S</ta>
            <ta e="T355" id="Seg_13933" s="T354">v:pred 0.3.h:S</ta>
            <ta e="T357" id="Seg_13934" s="T356">v:pred 0.3.h:S</ta>
            <ta e="T359" id="Seg_13935" s="T358">v:pred</ta>
            <ta e="T360" id="Seg_13936" s="T359">np.h:S</ta>
            <ta e="T361" id="Seg_13937" s="T360">np.h:S</ta>
            <ta e="T362" id="Seg_13938" s="T361">v:pred</ta>
            <ta e="T364" id="Seg_13939" s="T363">pro.h:S</ta>
            <ta e="T365" id="Seg_13940" s="T364">ptcl.neg</ta>
            <ta e="T366" id="Seg_13941" s="T365">v:pred</ta>
            <ta e="T370" id="Seg_13942" s="T369">v:pred 0.3.h:S</ta>
            <ta e="T371" id="Seg_13943" s="T370">v:pred 0.3.h:S</ta>
            <ta e="T376" id="Seg_13944" s="T375">ptcl.neg</ta>
            <ta e="T377" id="Seg_13945" s="T376">v:pred 0.3.h:S</ta>
            <ta e="T379" id="Seg_13946" s="T378">v:pred 0.3.h:S</ta>
            <ta e="T380" id="Seg_13947" s="T379">np.h:O</ta>
            <ta e="T382" id="Seg_13948" s="T381">v:pred 0.3.h:S</ta>
            <ta e="T384" id="Seg_13949" s="T383">conv:pred</ta>
            <ta e="T385" id="Seg_13950" s="T384">v:pred 0.3.h:S</ta>
            <ta e="T387" id="Seg_13951" s="T386">np.h:S</ta>
            <ta e="T388" id="Seg_13952" s="T387">v:pred</ta>
            <ta e="T389" id="Seg_13953" s="T388">v:pred 0.2.h:S</ta>
            <ta e="T390" id="Seg_13954" s="T389">v:pred 0.2.h:S</ta>
            <ta e="T392" id="Seg_13955" s="T391">pro.h:S</ta>
            <ta e="T394" id="Seg_13956" s="T393">v:pred</ta>
            <ta e="T395" id="Seg_13957" s="T394">np.h:O</ta>
            <ta e="T396" id="Seg_13958" s="T395">v:pred 0.3.h:S</ta>
            <ta e="T398" id="Seg_13959" s="T397">pro.h:S</ta>
            <ta e="T400" id="Seg_13960" s="T399">v:pred</ta>
            <ta e="T401" id="Seg_13961" s="T400">np.h:S</ta>
            <ta e="T403" id="Seg_13962" s="T402">v:pred</ta>
            <ta e="T405" id="Seg_13963" s="T404">pro.h:S</ta>
            <ta e="T407" id="Seg_13964" s="T406">v:pred</ta>
            <ta e="T409" id="Seg_13965" s="T408">v:pred 0.3.h:S</ta>
            <ta e="T411" id="Seg_13966" s="T410">np.h:S</ta>
            <ta e="T412" id="Seg_13967" s="T411">v:pred</ta>
            <ta e="T414" id="Seg_13968" s="T413">v:pred 0.3.h:S</ta>
            <ta e="T415" id="Seg_13969" s="T414">pro.h:S</ta>
            <ta e="T416" id="Seg_13970" s="T415">n:pred</ta>
            <ta e="T417" id="Seg_13971" s="T416">cop</ta>
            <ta e="T418" id="Seg_13972" s="T417">np.h:S</ta>
            <ta e="T420" id="Seg_13973" s="T419">adj:pred</ta>
            <ta e="T423" id="Seg_13974" s="T422">pro.h:S</ta>
            <ta e="T424" id="Seg_13975" s="T423">adj:pred</ta>
            <ta e="T426" id="Seg_13976" s="T425">pro.h:S</ta>
            <ta e="T427" id="Seg_13977" s="T426">v:pred</ta>
            <ta e="T428" id="Seg_13978" s="T427">ptcl.neg</ta>
            <ta e="T429" id="Seg_13979" s="T428">pro.h:S</ta>
            <ta e="T433" id="Seg_13980" s="T432">cop</ta>
            <ta e="T434" id="Seg_13981" s="T433">v:pred 0.2.h:S</ta>
            <ta e="T436" id="Seg_13982" s="T435">v:pred 0.2.h:S</ta>
            <ta e="T442" id="Seg_13983" s="T441">v:pred 0.2.h:S</ta>
            <ta e="T448" id="Seg_13984" s="T447">v:pred 0.3.h:S</ta>
            <ta e="T449" id="Seg_13985" s="T448">conv:pred</ta>
            <ta e="T450" id="Seg_13986" s="T449">v:pred 0.3.h:S</ta>
            <ta e="T451" id="Seg_13987" s="T450">pro.h:S</ta>
            <ta e="T453" id="Seg_13988" s="T452">v:pred</ta>
            <ta e="T454" id="Seg_13989" s="T453">np:O</ta>
            <ta e="T455" id="Seg_13990" s="T454">v:pred 0.3.h:S</ta>
            <ta e="T456" id="Seg_13991" s="T455">v:pred 0.3.h:S</ta>
            <ta e="T457" id="Seg_13992" s="T456">np:O</ta>
            <ta e="T458" id="Seg_13993" s="T457">s:purp</ta>
            <ta e="T459" id="Seg_13994" s="T458">v:pred 0.3.h:S</ta>
            <ta e="T460" id="Seg_13995" s="T459">ptcl:pred</ta>
            <ta e="T461" id="Seg_13996" s="T460">np.h:O</ta>
            <ta e="T463" id="Seg_13997" s="T462">np.h:S</ta>
            <ta e="T464" id="Seg_13998" s="T463">v:pred</ta>
            <ta e="T465" id="Seg_13999" s="T464">pro.h:S</ta>
            <ta e="T471" id="Seg_14000" s="T470">v:pred</ta>
            <ta e="T473" id="Seg_14001" s="T472">v:pred 0.3.h:S</ta>
            <ta e="T474" id="Seg_14002" s="T473">v:pred 0.3.h:S</ta>
            <ta e="T475" id="Seg_14003" s="T474">v:pred 0.3.h:S</ta>
            <ta e="T478" id="Seg_14004" s="T477">v:pred 0.3.h:S</ta>
            <ta e="T479" id="Seg_14005" s="T478">pro.h:O</ta>
            <ta e="T480" id="Seg_14006" s="T479">v:pred 0.3.h:S</ta>
            <ta e="T482" id="Seg_14007" s="T481">np:O</ta>
            <ta e="T483" id="Seg_14008" s="T482">v:pred 0.3.h:S</ta>
            <ta e="T487" id="Seg_14009" s="T486">v:pred 0.3.h:S</ta>
            <ta e="T488" id="Seg_14010" s="T487">conv:pred</ta>
            <ta e="T489" id="Seg_14011" s="T488">v:pred 0.3.h:S</ta>
            <ta e="T490" id="Seg_14012" s="T489">np.h:O</ta>
            <ta e="T491" id="Seg_14013" s="T490">v:pred 0.3.h:S</ta>
            <ta e="T492" id="Seg_14014" s="T491">pro.h:S</ta>
            <ta e="T493" id="Seg_14015" s="T492">v:pred</ta>
            <ta e="T497" id="Seg_14016" s="T496">v:pred 0.3.h:S</ta>
            <ta e="T498" id="Seg_14017" s="T497">v:pred 0.3.h:S</ta>
            <ta e="T500" id="Seg_14018" s="T499">np.h:S</ta>
            <ta e="T501" id="Seg_14019" s="T500">v:pred</ta>
            <ta e="T505" id="Seg_14020" s="T504">np.h:S</ta>
            <ta e="T506" id="Seg_14021" s="T505">adj:pred</ta>
            <ta e="T507" id="Seg_14022" s="T506">np.h:S</ta>
            <ta e="T508" id="Seg_14023" s="T507">adj:pred</ta>
            <ta e="T509" id="Seg_14024" s="T508">v:pred 0.3.h:S</ta>
            <ta e="T511" id="Seg_14025" s="T510">np.h:O</ta>
            <ta e="T516" id="Seg_14026" s="T515">np:O</ta>
            <ta e="T517" id="Seg_14027" s="T516">v:pred 0.3.h:S</ta>
            <ta e="T518" id="Seg_14028" s="T517">conv:pred</ta>
            <ta e="T519" id="Seg_14029" s="T518">v:pred 0.3.h:S</ta>
            <ta e="T523" id="Seg_14030" s="T522">np.h:S</ta>
            <ta e="T524" id="Seg_14031" s="T523">v:pred</ta>
            <ta e="T526" id="Seg_14032" s="T525">np:O</ta>
            <ta e="T527" id="Seg_14033" s="T526">np:O</ta>
            <ta e="T528" id="Seg_14034" s="T527">v:pred 0.3.h:S</ta>
            <ta e="T530" id="Seg_14035" s="T529">pro:S</ta>
            <ta e="T532" id="Seg_14036" s="T531">v:pred</ta>
            <ta e="T533" id="Seg_14037" s="T532">v:pred 0.3.h:S</ta>
            <ta e="T535" id="Seg_14038" s="T534">v:pred 0.2.h:S</ta>
            <ta e="T538" id="Seg_14039" s="T537">ptcl.neg</ta>
            <ta e="T539" id="Seg_14040" s="T538">v:pred 0.3.h:S</ta>
            <ta e="T540" id="Seg_14041" s="T539">np:O</ta>
            <ta e="T542" id="Seg_14042" s="T541">v:pred 0.1.h:S</ta>
            <ta e="T544" id="Seg_14043" s="T543">pro.h:S</ta>
            <ta e="T549" id="Seg_14044" s="T548">conv:pred</ta>
            <ta e="T550" id="Seg_14045" s="T549">v:pred</ta>
            <ta e="T551" id="Seg_14046" s="T550">v:pred</ta>
            <ta e="T552" id="Seg_14047" s="T551">pro.h:S</ta>
            <ta e="T553" id="Seg_14048" s="T552">v:pred 0.3.h:S</ta>
            <ta e="T556" id="Seg_14049" s="T555">v:pred 0.3.h:S</ta>
            <ta e="T558" id="Seg_14050" s="T557">np:O</ta>
            <ta e="T559" id="Seg_14051" s="T558">v:pred 0.3.h:S</ta>
            <ta e="T561" id="Seg_14052" s="T560">v:pred</ta>
            <ta e="T564" id="Seg_14053" s="T563">np.h:S</ta>
            <ta e="T566" id="Seg_14054" s="T565">pro.h:S</ta>
            <ta e="T569" id="Seg_14055" s="T568">v:pred</ta>
            <ta e="T571" id="Seg_14056" s="T570">np:S</ta>
            <ta e="T572" id="Seg_14057" s="T571">adj:pred</ta>
            <ta e="T574" id="Seg_14058" s="T573">adj:pred 0.3.h:S</ta>
            <ta e="T575" id="Seg_14059" s="T574">v:pred 0.3.h:S</ta>
            <ta e="T576" id="Seg_14060" s="T575">pro:S</ta>
            <ta e="T578" id="Seg_14061" s="T577">n:pred</ta>
            <ta e="T580" id="Seg_14062" s="T579">v:pred 0.3.h:S</ta>
            <ta e="T584" id="Seg_14063" s="T583">np:S</ta>
            <ta e="T585" id="Seg_14064" s="T584">v:pred</ta>
            <ta e="T587" id="Seg_14065" s="T586">v:pred 0.3.h:S</ta>
            <ta e="T589" id="Seg_14066" s="T588">v:pred 0.3.h:S</ta>
            <ta e="T592" id="Seg_14067" s="T591">pro.h:S</ta>
            <ta e="T594" id="Seg_14068" s="T593">n:pred</ta>
            <ta e="T595" id="Seg_14069" s="T594">pro.h:O</ta>
            <ta e="T598" id="Seg_14070" s="T597">pro.h:S</ta>
            <ta e="T600" id="Seg_14071" s="T599">v:pred</ta>
            <ta e="T602" id="Seg_14072" s="T601">np:O</ta>
            <ta e="T604" id="Seg_14073" s="T603">v:pred 0.3.h:S</ta>
            <ta e="T606" id="Seg_14074" s="T605">adj:pred</ta>
            <ta e="T607" id="Seg_14075" s="T606">cop 0.3.h:S</ta>
            <ta e="T610" id="Seg_14076" s="T609">ptcl:pred</ta>
            <ta e="T611" id="Seg_14077" s="T610">np:O</ta>
            <ta e="T614" id="Seg_14078" s="T613">np.h:S</ta>
            <ta e="T616" id="Seg_14079" s="T615">v:pred</ta>
            <ta e="T618" id="Seg_14080" s="T617">adj:pred</ta>
            <ta e="T619" id="Seg_14081" s="T618">cop 0.3.h:S</ta>
            <ta e="T622" id="Seg_14082" s="T621">pro.h:S</ta>
            <ta e="T623" id="Seg_14083" s="T622">v:pred</ta>
            <ta e="T624" id="Seg_14084" s="T623">np:S</ta>
            <ta e="T626" id="Seg_14085" s="T625">v:pred</ta>
            <ta e="T627" id="Seg_14086" s="T626">v:pred 0.3.h:S</ta>
            <ta e="T629" id="Seg_14087" s="T628">np:O</ta>
            <ta e="T634" id="Seg_14088" s="T633">v:pred</ta>
            <ta e="T636" id="Seg_14089" s="T635">np.h:S</ta>
            <ta e="T638" id="Seg_14090" s="T637">np.h:S</ta>
            <ta e="T644" id="Seg_14091" s="T643">v:pred 0.3.h:S</ta>
            <ta e="T646" id="Seg_14092" s="T645">np.h:S</ta>
            <ta e="T648" id="Seg_14093" s="T647">v:pred</ta>
            <ta e="T649" id="Seg_14094" s="T648">v:pred 0.3.h:S</ta>
            <ta e="T651" id="Seg_14095" s="T650">np.h:S</ta>
            <ta e="T652" id="Seg_14096" s="T651">v:pred</ta>
            <ta e="T653" id="Seg_14097" s="T652">pro.h:S</ta>
            <ta e="T654" id="Seg_14098" s="T653">v:pred</ta>
            <ta e="T655" id="Seg_14099" s="T654">pro.h:O</ta>
            <ta e="T656" id="Seg_14100" s="T655">v:pred 0.3.h:S</ta>
            <ta e="T658" id="Seg_14101" s="T657">np.h:O</ta>
            <ta e="T659" id="Seg_14102" s="T658">v:pred</ta>
            <ta e="T662" id="Seg_14103" s="T661">v:pred 0.2.h:S</ta>
            <ta e="T663" id="Seg_14104" s="T662">pro.h:S</ta>
            <ta e="T664" id="Seg_14105" s="T663">v:pred</ta>
            <ta e="T666" id="Seg_14106" s="T665">pro.h:S</ta>
            <ta e="T667" id="Seg_14107" s="T666">v:pred</ta>
            <ta e="T668" id="Seg_14108" s="T667">np:O</ta>
            <ta e="T669" id="Seg_14109" s="T668">v:pred</ta>
            <ta e="T670" id="Seg_14110" s="T669">v:pred 0.3.h:S</ta>
            <ta e="T671" id="Seg_14111" s="T670">np:O</ta>
            <ta e="T672" id="Seg_14112" s="T671">pro.h:S</ta>
            <ta e="T673" id="Seg_14113" s="T672">v:pred</ta>
            <ta e="T674" id="Seg_14114" s="T673">pro.h:S</ta>
            <ta e="T676" id="Seg_14115" s="T675">v:pred</ta>
            <ta e="T677" id="Seg_14116" s="T676">n:pred</ta>
            <ta e="T678" id="Seg_14117" s="T677">cop 0.3.h:S</ta>
            <ta e="T683" id="Seg_14118" s="T682">v:pred</ta>
            <ta e="T684" id="Seg_14119" s="T683">np.h:S</ta>
            <ta e="T685" id="Seg_14120" s="T684">pro.h:S</ta>
            <ta e="T686" id="Seg_14121" s="T685">v:pred</ta>
            <ta e="T688" id="Seg_14122" s="T687">pro.h:O</ta>
            <ta e="T689" id="Seg_14123" s="T688">v:pred 0.3.h:S</ta>
            <ta e="T690" id="Seg_14124" s="T689">np.h:O</ta>
            <ta e="T691" id="Seg_14125" s="T690">v:pred</ta>
            <ta e="T693" id="Seg_14126" s="T692">v:pred 0.2.h:S</ta>
            <ta e="T694" id="Seg_14127" s="T693">pro.h:S</ta>
            <ta e="T695" id="Seg_14128" s="T694">v:pred</ta>
            <ta e="T696" id="Seg_14129" s="T695">v:pred 0.3.h:S</ta>
            <ta e="T697" id="Seg_14130" s="T696">v:pred 0.3.h:S</ta>
            <ta e="T699" id="Seg_14131" s="T698">v:pred 0.3.h:S</ta>
            <ta e="T700" id="Seg_14132" s="T699">pro.h:S</ta>
            <ta e="T702" id="Seg_14133" s="T701">v:pred</ta>
            <ta e="T703" id="Seg_14134" s="T702">np.h:S</ta>
            <ta e="T704" id="Seg_14135" s="T703">v:pred</ta>
            <ta e="T709" id="Seg_14136" s="T708">v:pred 0.3.h:S</ta>
            <ta e="T711" id="Seg_14137" s="T710">pro.h:S</ta>
            <ta e="T712" id="Seg_14138" s="T711">v:pred</ta>
            <ta e="T714" id="Seg_14139" s="T713">pro.h:O</ta>
            <ta e="T715" id="Seg_14140" s="T714">v:pred 0.3.h:S</ta>
            <ta e="T717" id="Seg_14141" s="T716">v:pred 0.2.h:S</ta>
            <ta e="T719" id="Seg_14142" s="T718">pro.h:S</ta>
            <ta e="T720" id="Seg_14143" s="T719">v:pred</ta>
            <ta e="T722" id="Seg_14144" s="T721">v:pred 0.3.h:S</ta>
            <ta e="T723" id="Seg_14145" s="T722">pro.h:S</ta>
            <ta e="T724" id="Seg_14146" s="T723">adj:pred</ta>
            <ta e="T725" id="Seg_14147" s="T724">pro.h:S</ta>
            <ta e="T727" id="Seg_14148" s="T726">v:pred</ta>
            <ta e="T730" id="Seg_14149" s="T729">v:pred 0.3.h:S</ta>
            <ta e="T731" id="Seg_14150" s="T730">pro.h:S</ta>
            <ta e="T732" id="Seg_14151" s="T731">v:pred</ta>
            <ta e="T733" id="Seg_14152" s="T732">v:pred 0.3.h:S</ta>
            <ta e="T734" id="Seg_14153" s="T733">v:pred 0.2.h:S</ta>
            <ta e="T736" id="Seg_14154" s="T735">np:O</ta>
            <ta e="T737" id="Seg_14155" s="T736">pro.h:S</ta>
            <ta e="T738" id="Seg_14156" s="T737">v:pred</ta>
            <ta e="T740" id="Seg_14157" s="T739">v:pred 0.3:S</ta>
            <ta e="T741" id="Seg_14158" s="T740">pro.h:S</ta>
            <ta e="T742" id="Seg_14159" s="T741">v:pred</ta>
            <ta e="T744" id="Seg_14160" s="T743">pro.h:S</ta>
            <ta e="T745" id="Seg_14161" s="T744">v:pred</ta>
            <ta e="T746" id="Seg_14162" s="T745">pro.h:S</ta>
            <ta e="T748" id="Seg_14163" s="T747">ptcl.neg</ta>
            <ta e="T749" id="Seg_14164" s="T748">pro.h:S</ta>
            <ta e="T750" id="Seg_14165" s="T749">ptcl.neg</ta>
            <ta e="T751" id="Seg_14166" s="T750">v:pred</ta>
            <ta e="T753" id="Seg_14167" s="T752">ptcl:pred</ta>
            <ta e="T756" id="Seg_14168" s="T755">pro.h:S</ta>
            <ta e="T757" id="Seg_14169" s="T756">v:pred</ta>
            <ta e="T759" id="Seg_14170" s="T758">adj:pred</ta>
            <ta e="T760" id="Seg_14171" s="T759">np:S</ta>
            <ta e="T761" id="Seg_14172" s="T760">cop</ta>
            <ta e="T763" id="Seg_14173" s="T762">v:pred 0.3.h:S</ta>
            <ta e="T765" id="Seg_14174" s="T764">np:O</ta>
            <ta e="T766" id="Seg_14175" s="T765">adj:pred</ta>
            <ta e="T767" id="Seg_14176" s="T766">cop 0.3:S</ta>
            <ta e="T769" id="Seg_14177" s="T768">ptcl:pred</ta>
            <ta e="T773" id="Seg_14178" s="T772">v:pred 0.2.h:S</ta>
            <ta e="T774" id="Seg_14179" s="T773">pro.h:S</ta>
            <ta e="T776" id="Seg_14180" s="T775">v:pred</ta>
            <ta e="T778" id="Seg_14181" s="T777">pro.h:S</ta>
            <ta e="T779" id="Seg_14182" s="T778">v:pred</ta>
            <ta e="T781" id="Seg_14183" s="T780">pro.h:S</ta>
            <ta e="T782" id="Seg_14184" s="T781">v:pred</ta>
            <ta e="T785" id="Seg_14185" s="T784">np.h:S</ta>
            <ta e="T787" id="Seg_14186" s="T786">np.h:S</ta>
            <ta e="T788" id="Seg_14187" s="T787">v:pred</ta>
            <ta e="T790" id="Seg_14188" s="T789">np:O</ta>
            <ta e="T791" id="Seg_14189" s="T790">v:pred</ta>
            <ta e="T792" id="Seg_14190" s="T791">np:S</ta>
            <ta e="T793" id="Seg_14191" s="T792">v:pred</ta>
            <ta e="T794" id="Seg_14192" s="T793">pro.h:S</ta>
            <ta e="T795" id="Seg_14193" s="T794">np:O</ta>
            <ta e="T796" id="Seg_14194" s="T795">v:pred</ta>
            <ta e="T802" id="Seg_14195" s="T801">pro.h:S</ta>
            <ta e="T804" id="Seg_14196" s="T803">v:pred</ta>
            <ta e="T806" id="Seg_14197" s="T805">v:pred 0.3.h:S</ta>
            <ta e="T810" id="Seg_14198" s="T809">v:pred 0.3.h:S</ta>
            <ta e="T812" id="Seg_14199" s="T811">v:pred 0.3.h:S</ta>
            <ta e="T814" id="Seg_14200" s="T813">v:pred 0.3.h:S</ta>
            <ta e="T815" id="Seg_14201" s="T814">s:purp</ta>
            <ta e="T816" id="Seg_14202" s="T815">np.h:O</ta>
            <ta e="T817" id="Seg_14203" s="T816">v:pred 0.3.h:S</ta>
            <ta e="T819" id="Seg_14204" s="T818">v:pred 0.3.h:S</ta>
            <ta e="T820" id="Seg_14205" s="T819">v:pred 0.3.h:S</ta>
            <ta e="T825" id="Seg_14206" s="T824">np:O</ta>
            <ta e="T826" id="Seg_14207" s="T825">v:pred 0.3.h:S</ta>
            <ta e="T827" id="Seg_14208" s="T826">np:O</ta>
            <ta e="T828" id="Seg_14209" s="T827">v:pred 0.3.h:S</ta>
            <ta e="T830" id="Seg_14210" s="T829">cop 0.3.h:S</ta>
            <ta e="T832" id="Seg_14211" s="T831">np.h:O</ta>
            <ta e="T833" id="Seg_14212" s="T832">v:pred 0.3.h:S</ta>
            <ta e="T842" id="Seg_14213" s="T841">v:pred 0.1.h:S</ta>
            <ta e="T846" id="Seg_14214" s="T845">v:pred 0.1.h:S</ta>
            <ta e="T850" id="Seg_14215" s="T849">v:pred 0.1.h:S</ta>
            <ta e="T852" id="Seg_14216" s="T851">v:pred</ta>
            <ta e="T853" id="Seg_14217" s="T852">np.h:S</ta>
            <ta e="T855" id="Seg_14218" s="T854">np.h:S</ta>
            <ta e="T858" id="Seg_14219" s="T857">pro:S</ta>
            <ta e="T859" id="Seg_14220" s="T858">v:pred</ta>
            <ta e="T860" id="Seg_14221" s="T859">v:pred 0.3.h:S</ta>
            <ta e="T862" id="Seg_14222" s="T861">np:O</ta>
            <ta e="T864" id="Seg_14223" s="T863">v:pred 0.3.h:S</ta>
            <ta e="T865" id="Seg_14224" s="T864">v:pred 0.3.h:S</ta>
            <ta e="T866" id="Seg_14225" s="T865">v:pred 0.3.h:S</ta>
            <ta e="T868" id="Seg_14226" s="T867">np:S</ta>
            <ta e="T869" id="Seg_14227" s="T868">v:pred</ta>
            <ta e="T872" id="Seg_14228" s="T871">ptcl:pred</ta>
            <ta e="T875" id="Seg_14229" s="T874">np.h:S</ta>
            <ta e="T876" id="Seg_14230" s="T875">v:pred</ta>
            <ta e="T878" id="Seg_14231" s="T877">v:pred 0.2.h:S</ta>
            <ta e="T879" id="Seg_14232" s="T878">np:O</ta>
            <ta e="T880" id="Seg_14233" s="T879">pro.h:S</ta>
            <ta e="T881" id="Seg_14234" s="T880">v:pred</ta>
            <ta e="T882" id="Seg_14235" s="T881">pro:S</ta>
            <ta e="T884" id="Seg_14236" s="T883">v:pred</ta>
            <ta e="T889" id="Seg_14237" s="T888">v:pred</ta>
            <ta e="T890" id="Seg_14238" s="T889">pro.h:S</ta>
            <ta e="T892" id="Seg_14239" s="T891">np:O</ta>
            <ta e="T896" id="Seg_14240" s="T895">v:pred 0.3:S</ta>
            <ta e="T900" id="Seg_14241" s="T899">np.h:S</ta>
            <ta e="T901" id="Seg_14242" s="T900">ptcl:pred</ta>
            <ta e="T904" id="Seg_14243" s="T903">np:O</ta>
            <ta e="T905" id="Seg_14244" s="T904">v:pred 0.3.h:S</ta>
            <ta e="T906" id="Seg_14245" s="T905">v:pred 0.3.h:S</ta>
            <ta e="T911" id="Seg_14246" s="T910">v:pred 0.3.h:S</ta>
            <ta e="T912" id="Seg_14247" s="T911">np:S</ta>
            <ta e="T914" id="Seg_14248" s="T913">v:pred</ta>
            <ta e="T917" id="Seg_14249" s="T916">v:pred 0.3.h:S</ta>
            <ta e="T920" id="Seg_14250" s="T919">np.h:S</ta>
            <ta e="T921" id="Seg_14251" s="T920">v:pred</ta>
            <ta e="T922" id="Seg_14252" s="T921">np:S</ta>
            <ta e="T923" id="Seg_14253" s="T922">adj:pred</ta>
            <ta e="T928" id="Seg_14254" s="T927">np:S</ta>
            <ta e="T929" id="Seg_14255" s="T928">v:pred</ta>
            <ta e="T930" id="Seg_14256" s="T929">pro.h:S</ta>
            <ta e="T931" id="Seg_14257" s="T930">v:pred</ta>
            <ta e="T932" id="Seg_14258" s="T931">pro:O</ta>
            <ta e="T934" id="Seg_14259" s="T933">v:pred 0.3.h:S</ta>
            <ta e="T935" id="Seg_14260" s="T934">v:pred</ta>
            <ta e="T941" id="Seg_14261" s="T940">pro.h:S</ta>
            <ta e="T942" id="Seg_14262" s="T941">v:pred</ta>
            <ta e="T944" id="Seg_14263" s="T943">ptcl:pred</ta>
            <ta e="T947" id="Seg_14264" s="T946">np:O</ta>
            <ta e="T950" id="Seg_14265" s="T949">np:O</ta>
            <ta e="T951" id="Seg_14266" s="T950">np:O</ta>
            <ta e="T952" id="Seg_14267" s="T951">np:O</ta>
            <ta e="T953" id="Seg_14268" s="T952">np:O</ta>
            <ta e="T954" id="Seg_14269" s="T953">np.h:S</ta>
            <ta e="T955" id="Seg_14270" s="T954">v:pred</ta>
            <ta e="T957" id="Seg_14271" s="T956">np.h:S</ta>
            <ta e="T958" id="Seg_14272" s="T957">v:pred</ta>
            <ta e="T961" id="Seg_14273" s="T960">conv:pred</ta>
            <ta e="T963" id="Seg_14274" s="T962">v:pred</ta>
            <ta e="T965" id="Seg_14275" s="T964">np.h:S</ta>
            <ta e="T966" id="Seg_14276" s="T965">v:pred 0.2.h:S</ta>
            <ta e="T968" id="Seg_14277" s="T967">s:purp</ta>
            <ta e="T969" id="Seg_14278" s="T968">pro.h:S</ta>
            <ta e="T970" id="Seg_14279" s="T969">v:pred</ta>
            <ta e="T976" id="Seg_14280" s="T975">np:O</ta>
            <ta e="T980" id="Seg_14281" s="T979">ptcl:pred</ta>
            <ta e="T981" id="Seg_14282" s="T980">np:O</ta>
            <ta e="T982" id="Seg_14283" s="T981">v:pred</ta>
            <ta e="T983" id="Seg_14284" s="T982">np:O</ta>
            <ta e="T984" id="Seg_14285" s="T983">np:O</ta>
            <ta e="T985" id="Seg_14286" s="T984">pro.h:S</ta>
            <ta e="T986" id="Seg_14287" s="T985">v:pred</ta>
            <ta e="T987" id="Seg_14288" s="T986">v:pred 0.2.h:S</ta>
            <ta e="T989" id="Seg_14289" s="T988">ptcl.neg</ta>
            <ta e="T990" id="Seg_14290" s="T989">pro.h:S</ta>
            <ta e="T991" id="Seg_14291" s="T990">ptcl.neg</ta>
            <ta e="T992" id="Seg_14292" s="T991">v:pred</ta>
            <ta e="T996" id="Seg_14293" s="T995">s:purp</ta>
            <ta e="T997" id="Seg_14294" s="T996">pro:S</ta>
            <ta e="T1001" id="Seg_14295" s="T1000">v:pred 0.3.h:S</ta>
            <ta e="T1003" id="Seg_14296" s="T1002">conv:pred</ta>
            <ta e="T1004" id="Seg_14297" s="T1003">v:pred 0.3.h:S</ta>
            <ta e="T1006" id="Seg_14298" s="T1005">v:pred 0.3.h:S</ta>
            <ta e="T1008" id="Seg_14299" s="T1007">pro.h:S</ta>
            <ta e="T1009" id="Seg_14300" s="T1008">v:pred</ta>
            <ta e="T1010" id="Seg_14301" s="T1009">v:pred</ta>
            <ta e="T1011" id="Seg_14302" s="T1010">np:S</ta>
            <ta e="T1013" id="Seg_14303" s="T1012">np.h:S</ta>
            <ta e="T1015" id="Seg_14304" s="T1014">v:pred 0.2.h:S</ta>
            <ta e="T1017" id="Seg_14305" s="T1016">pro.h:S</ta>
            <ta e="T1018" id="Seg_14306" s="T1017">v:pred</ta>
            <ta e="T1020" id="Seg_14307" s="T1019">v:pred 0.1.h:S</ta>
            <ta e="T1023" id="Seg_14308" s="T1022">v:pred 0.3.h:S</ta>
            <ta e="T1024" id="Seg_14309" s="T1023">v:pred 0.3.h:S</ta>
            <ta e="T1025" id="Seg_14310" s="T1024">v:pred 0.3.h:S</ta>
            <ta e="T1026" id="Seg_14311" s="T1025">v:pred</ta>
            <ta e="T1028" id="Seg_14312" s="T1027">np.h:S</ta>
            <ta e="T1030" id="Seg_14313" s="T1029">v:pred 0.2.h:S</ta>
            <ta e="T1031" id="Seg_14314" s="T1030">v:pred 0.2.h:S</ta>
            <ta e="T1032" id="Seg_14315" s="T1031">pro.h:O</ta>
            <ta e="T1039" id="Seg_14316" s="T1038">v:pred 0.3.h:S</ta>
            <ta e="T1040" id="Seg_14317" s="T1039">v:pred 0.3.h:S</ta>
            <ta e="T1043" id="Seg_14318" s="T1042">v:pred 0.2.h:S</ta>
            <ta e="T1044" id="Seg_14319" s="T1043">np:O</ta>
            <ta e="T1045" id="Seg_14320" s="T1044">v:pred</ta>
            <ta e="T1047" id="Seg_14321" s="T1046">v:pred 0.2.h:S</ta>
            <ta e="T1048" id="Seg_14322" s="T1047">pro.h:O</ta>
            <ta e="T1049" id="Seg_14323" s="T1048">v:pred 0.2.h:S</ta>
            <ta e="T1052" id="Seg_14324" s="T1051">pro.h:S</ta>
            <ta e="T1053" id="Seg_14325" s="T1052">v:pred</ta>
            <ta e="T1055" id="Seg_14326" s="T1054">v:pred 0.3.h:S</ta>
            <ta e="T1056" id="Seg_14327" s="T1055">np.h:S</ta>
            <ta e="T1057" id="Seg_14328" s="T1056">v:pred</ta>
            <ta e="T1058" id="Seg_14329" s="T1057">v:pred 0.2.h:S</ta>
            <ta e="T1061" id="Seg_14330" s="T1060">v:pred 0.2.h:S</ta>
            <ta e="T1062" id="Seg_14331" s="T1061">ptcl:pred</ta>
            <ta e="T1065" id="Seg_14332" s="T1064">v:pred 0.2.h:S</ta>
            <ta e="T1067" id="Seg_14333" s="T1066">np:O</ta>
            <ta e="T1068" id="Seg_14334" s="T1067">v:pred 0.2.h:S</ta>
            <ta e="T1070" id="Seg_14335" s="T1069">np:O</ta>
            <ta e="T1071" id="Seg_14336" s="T1070">pro.h:S</ta>
            <ta e="T1072" id="Seg_14337" s="T1071">v:pred</ta>
            <ta e="T1074" id="Seg_14338" s="T1073">v:pred 0.2.h:S</ta>
            <ta e="T1075" id="Seg_14339" s="T1074">pro.h:O</ta>
            <ta e="T1078" id="Seg_14340" s="T1077">ptcl:pred</ta>
            <ta e="T1079" id="Seg_14341" s="T1078">np.h:S</ta>
            <ta e="T1080" id="Seg_14342" s="T1079">v:pred</ta>
            <ta e="T1081" id="Seg_14343" s="T1080">np:O</ta>
            <ta e="T1082" id="Seg_14344" s="T1081">pro.h:O</ta>
            <ta e="T1083" id="Seg_14345" s="T1082">v:pred 0.3.h:S</ta>
            <ta e="T1085" id="Seg_14346" s="T1084">pro.h:S</ta>
            <ta e="T1086" id="Seg_14347" s="T1085">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST-PKZ" />
         <annotation name="BOR" tierref="BOR-PKZ">
            <ta e="T17" id="Seg_14348" s="T16">RUS:cult</ta>
            <ta e="T24" id="Seg_14349" s="T23">RUS:gram</ta>
            <ta e="T26" id="Seg_14350" s="T25">TURK:gram(INDEF)</ta>
            <ta e="T31" id="Seg_14351" s="T30">RUS:gram</ta>
            <ta e="T34" id="Seg_14352" s="T33">RUS:mod</ta>
            <ta e="T42" id="Seg_14353" s="T41">RUS:mod</ta>
            <ta e="T46" id="Seg_14354" s="T45">RUS:core</ta>
            <ta e="T53" id="Seg_14355" s="T52">RUS:gram</ta>
            <ta e="T59" id="Seg_14356" s="T58">RUS:gram</ta>
            <ta e="T60" id="Seg_14357" s="T59">TURK:gram(INDEF)</ta>
            <ta e="T64" id="Seg_14358" s="T63">TURK:core</ta>
            <ta e="T74" id="Seg_14359" s="T73">TURK:disc</ta>
            <ta e="T78" id="Seg_14360" s="T77">RUS:gram</ta>
            <ta e="T79" id="Seg_14361" s="T78">TURK:gram(INDEF)</ta>
            <ta e="T86" id="Seg_14362" s="T85">RUS:cult</ta>
            <ta e="T96" id="Seg_14363" s="T95">RUS:core</ta>
            <ta e="T97" id="Seg_14364" s="T96">RUS:gram</ta>
            <ta e="T98" id="Seg_14365" s="T97">TURK:disc</ta>
            <ta e="T104" id="Seg_14366" s="T103">TURK:disc</ta>
            <ta e="T106" id="Seg_14367" s="T105">RUS:cult</ta>
            <ta e="T108" id="Seg_14368" s="T107">RUS:gram</ta>
            <ta e="T120" id="Seg_14369" s="T119">TURK:disc</ta>
            <ta e="T123" id="Seg_14370" s="T122">RUS:disc</ta>
            <ta e="T132" id="Seg_14371" s="T131">RUS:gram</ta>
            <ta e="T142" id="Seg_14372" s="T141">RUS:disc</ta>
            <ta e="T144" id="Seg_14373" s="T143">RUS:gram</ta>
            <ta e="T148" id="Seg_14374" s="T147">RUS:gram</ta>
            <ta e="T152" id="Seg_14375" s="T151">RUS:cult</ta>
            <ta e="T159" id="Seg_14376" s="T158">TURK:gram(INDEF)</ta>
            <ta e="T165" id="Seg_14377" s="T164">TURK:cult</ta>
            <ta e="T167" id="Seg_14378" s="T166">RUS:gram</ta>
            <ta e="T175" id="Seg_14379" s="T174">TURK:disc</ta>
            <ta e="T178" id="Seg_14380" s="T177">RUS:gram</ta>
            <ta e="T179" id="Seg_14381" s="T178">RUS:gram</ta>
            <ta e="T183" id="Seg_14382" s="T182">RUS:gram</ta>
            <ta e="T191" id="Seg_14383" s="T190">TURK:gram(INDEF)</ta>
            <ta e="T195" id="Seg_14384" s="T194">RUS:gram</ta>
            <ta e="T205" id="Seg_14385" s="T204">RUS:cult</ta>
            <ta e="T208" id="Seg_14386" s="T207">RUS:gram</ta>
            <ta e="T209" id="Seg_14387" s="T208">RUS:gram</ta>
            <ta e="T213" id="Seg_14388" s="T212">TURK:disc</ta>
            <ta e="T248" id="Seg_14389" s="T247">RUS:gram</ta>
            <ta e="T249" id="Seg_14390" s="T248">TURK:gram(INDEF)</ta>
            <ta e="T256" id="Seg_14391" s="T255">TURK:disc</ta>
            <ta e="T264" id="Seg_14392" s="T263">TURK:disc</ta>
            <ta e="T268" id="Seg_14393" s="T267">RUS:gram</ta>
            <ta e="T276" id="Seg_14394" s="T275">RUS:gram</ta>
            <ta e="T277" id="Seg_14395" s="T276">RUS:core</ta>
            <ta e="T281" id="Seg_14396" s="T280">RUS:gram</ta>
            <ta e="T288" id="Seg_14397" s="T287">RUS:mod</ta>
            <ta e="T292" id="Seg_14398" s="T291">RUS:cult</ta>
            <ta e="T300" id="Seg_14399" s="T299">TURK:disc</ta>
            <ta e="T305" id="Seg_14400" s="T304">RUS:gram</ta>
            <ta e="T318" id="Seg_14401" s="T317">RUS:disc</ta>
            <ta e="T319" id="Seg_14402" s="T318">TURK:gram(INDEF)</ta>
            <ta e="T321" id="Seg_14403" s="T320">%TURK:core</ta>
            <ta e="T336" id="Seg_14404" s="T335">RUS:cult</ta>
            <ta e="T339" id="Seg_14405" s="T338">RUS:cult</ta>
            <ta e="T345" id="Seg_14406" s="T344">RUS:gram</ta>
            <ta e="T364" id="Seg_14407" s="T363">TURK:gram(INDEF)</ta>
            <ta e="T381" id="Seg_14408" s="T380">TURK:disc</ta>
            <ta e="T383" id="Seg_14409" s="T382">RUS:gram</ta>
            <ta e="T386" id="Seg_14410" s="T385">RUS:gram</ta>
            <ta e="T391" id="Seg_14411" s="T390">RUS:gram</ta>
            <ta e="T397" id="Seg_14412" s="T396">RUS:gram</ta>
            <ta e="T404" id="Seg_14413" s="T403">RUS:gram</ta>
            <ta e="T406" id="Seg_14414" s="T405">RUS:cult</ta>
            <ta e="T408" id="Seg_14415" s="T407">RUS:gram</ta>
            <ta e="T421" id="Seg_14416" s="T420">RUS:gram</ta>
            <ta e="T425" id="Seg_14417" s="T424">RUS:gram</ta>
            <ta e="T430" id="Seg_14418" s="T429">RUS:gram</ta>
            <ta e="T437" id="Seg_14419" s="T436">RUS:disc</ta>
            <ta e="T454" id="Seg_14420" s="T453">RUS:cult</ta>
            <ta e="T460" id="Seg_14421" s="T459">RUS:gram</ta>
            <ta e="T477" id="Seg_14422" s="T476">RUS:gram</ta>
            <ta e="T481" id="Seg_14423" s="T480">RUS:gram</ta>
            <ta e="T482" id="Seg_14424" s="T481">RUS:cult</ta>
            <ta e="T495" id="Seg_14425" s="T494">RUS:gram</ta>
            <ta e="T496" id="Seg_14426" s="T495">RUS:cult</ta>
            <ta e="T503" id="Seg_14427" s="T502">TURK:gram(INDEF)</ta>
            <ta e="T510" id="Seg_14428" s="T509">RUS:cult</ta>
            <ta e="T512" id="Seg_14429" s="T511">TURK:cult</ta>
            <ta e="T513" id="Seg_14430" s="T512">TURK:cult</ta>
            <ta e="T515" id="Seg_14431" s="T514">RUS:gram</ta>
            <ta e="T516" id="Seg_14432" s="T515">RUS:cult</ta>
            <ta e="T523" id="Seg_14433" s="T522">TURK:cult</ta>
            <ta e="T529" id="Seg_14434" s="T528">TURK:disc</ta>
            <ta e="T555" id="Seg_14435" s="T554">TURK:disc</ta>
            <ta e="T558" id="Seg_14436" s="T557">TURK:cult</ta>
            <ta e="T563" id="Seg_14437" s="T562">RUS:core</ta>
            <ta e="T567" id="Seg_14438" s="T566">TURK:disc</ta>
            <ta e="T570" id="Seg_14439" s="T569">RUS:gram</ta>
            <ta e="T573" id="Seg_14440" s="T572">RUS:cult</ta>
            <ta e="T581" id="Seg_14441" s="T580">TURK:disc</ta>
            <ta e="T582" id="Seg_14442" s="T581">TURK:disc</ta>
            <ta e="T584" id="Seg_14443" s="T583">RUS:cult</ta>
            <ta e="T594" id="Seg_14444" s="T593">RUS:cult</ta>
            <ta e="T610" id="Seg_14445" s="T609">RUS:gram</ta>
            <ta e="T611" id="Seg_14446" s="T610">TURK:cult</ta>
            <ta e="T615" id="Seg_14447" s="T614">TURK:disc</ta>
            <ta e="T620" id="Seg_14448" s="T619">RUS:gram</ta>
            <ta e="T624" id="Seg_14449" s="T623">TURK:cult</ta>
            <ta e="T625" id="Seg_14450" s="T624">TURK:disc</ta>
            <ta e="T629" id="Seg_14451" s="T628">RUS:cult</ta>
            <ta e="T636" id="Seg_14452" s="T635">RUS:cult</ta>
            <ta e="T637" id="Seg_14453" s="T636">RUS:gram</ta>
            <ta e="T638" id="Seg_14454" s="T637">RUS:cult</ta>
            <ta e="T639" id="Seg_14455" s="T638">RUS:cult</ta>
            <ta e="T640" id="Seg_14456" s="T639">RUS:gram</ta>
            <ta e="T641" id="Seg_14457" s="T640">RUS:cult</ta>
            <ta e="T645" id="Seg_14458" s="T644">RUS:gram</ta>
            <ta e="T646" id="Seg_14459" s="T645">RUS:cult</ta>
            <ta e="T650" id="Seg_14460" s="T649">RUS:gram</ta>
            <ta e="T651" id="Seg_14461" s="T650">RUS:cult</ta>
            <ta e="T653" id="Seg_14462" s="T652">TURK:gram(INDEF)</ta>
            <ta e="T661" id="Seg_14463" s="T660">RUS:disc</ta>
            <ta e="T665" id="Seg_14464" s="T664">RUS:gram</ta>
            <ta e="T668" id="Seg_14465" s="T667">RUS:cult</ta>
            <ta e="T671" id="Seg_14466" s="T670">RUS:cult</ta>
            <ta e="T677" id="Seg_14467" s="T676">RUS:cult</ta>
            <ta e="T684" id="Seg_14468" s="T683">RUS:cult</ta>
            <ta e="T687" id="Seg_14469" s="T686">RUS:gram</ta>
            <ta e="T692" id="Seg_14470" s="T691">RUS:disc</ta>
            <ta e="T703" id="Seg_14471" s="T702">RUS:cult</ta>
            <ta e="T711" id="Seg_14472" s="T710">TURK:gram(INDEF)</ta>
            <ta e="T713" id="Seg_14473" s="T712">RUS:gram</ta>
            <ta e="T716" id="Seg_14474" s="T715">RUS:disc</ta>
            <ta e="T721" id="Seg_14475" s="T720">TURK:disc</ta>
            <ta e="T726" id="Seg_14476" s="T725">RUS:cult</ta>
            <ta e="T739" id="Seg_14477" s="T738">RUS:gram</ta>
            <ta e="T741" id="Seg_14478" s="T740">TURK:gram(INDEF)</ta>
            <ta e="T743" id="Seg_14479" s="T742">RUS:gram</ta>
            <ta e="T747" id="Seg_14480" s="T746">TURK:disc</ta>
            <ta e="T753" id="Seg_14481" s="T752">RUS:gram</ta>
            <ta e="T769" id="Seg_14482" s="T768">RUS:gram</ta>
            <ta e="T771" id="Seg_14483" s="T770">RUS:cult</ta>
            <ta e="T780" id="Seg_14484" s="T779">RUS:disc</ta>
            <ta e="T784" id="Seg_14485" s="T783">TURK:disc</ta>
            <ta e="T786" id="Seg_14486" s="T785">RUS:gram</ta>
            <ta e="T787" id="Seg_14487" s="T786">RUS:cult</ta>
            <ta e="T790" id="Seg_14488" s="T789">TAT:cult</ta>
            <ta e="T797" id="Seg_14489" s="T796">RUS:gram</ta>
            <ta e="T798" id="Seg_14490" s="T797">RUS:mod</ta>
            <ta e="T801" id="Seg_14491" s="T800">TAT:cult</ta>
            <ta e="T805" id="Seg_14492" s="T804">RUS:gram</ta>
            <ta e="T813" id="Seg_14493" s="T812">RUS:gram</ta>
            <ta e="T825" id="Seg_14494" s="T824">RUS:cult</ta>
            <ta e="T834" id="Seg_14495" s="T833">RUS:cult</ta>
            <ta e="T835" id="Seg_14496" s="T834">RUS:cult</ta>
            <ta e="T838" id="Seg_14497" s="T837">RUS:mod</ta>
            <ta e="T848" id="Seg_14498" s="T847">TURK:disc</ta>
            <ta e="T849" id="Seg_14499" s="T848">RUS:core</ta>
            <ta e="T854" id="Seg_14500" s="T853">RUS:gram</ta>
            <ta e="T858" id="Seg_14501" s="T857">TURK:gram(INDEF)</ta>
            <ta e="T862" id="Seg_14502" s="T861">RUS:cult</ta>
            <ta e="T863" id="Seg_14503" s="T862">RUS:cult</ta>
            <ta e="T867" id="Seg_14504" s="T866">TURK:disc</ta>
            <ta e="T870" id="Seg_14505" s="T869">RUS:cult</ta>
            <ta e="T872" id="Seg_14506" s="T871">RUS:gram</ta>
            <ta e="T879" id="Seg_14507" s="T878">RUS:cult</ta>
            <ta e="T888" id="Seg_14508" s="T887">TURK:disc</ta>
            <ta e="T892" id="Seg_14509" s="T891">RUS:cult</ta>
            <ta e="T897" id="Seg_14510" s="T896">RUS:mod</ta>
            <ta e="T901" id="Seg_14511" s="T900">RUS:mod</ta>
            <ta e="T904" id="Seg_14512" s="T903">RUS:cult</ta>
            <ta e="T912" id="Seg_14513" s="T911">TAT:cult</ta>
            <ta e="T916" id="Seg_14514" s="T915">TAT:cult</ta>
            <ta e="T919" id="Seg_14515" s="T918">RUS:cult</ta>
            <ta e="T925" id="Seg_14516" s="T924">TURK:disc</ta>
            <ta e="T926" id="Seg_14517" s="T925">RUS:gram</ta>
            <ta e="T928" id="Seg_14518" s="T927">RUS:cult</ta>
            <ta e="T933" id="Seg_14519" s="T932">RUS:gram</ta>
            <ta e="T935" id="Seg_14520" s="T934">RUS:disc</ta>
            <ta e="T937" id="Seg_14521" s="T936">RUS:cult</ta>
            <ta e="T943" id="Seg_14522" s="T942">RUS:gram</ta>
            <ta e="T944" id="Seg_14523" s="T943">RUS:gram</ta>
            <ta e="T945" id="Seg_14524" s="T944">RUS:cult</ta>
            <ta e="T947" id="Seg_14525" s="T946">TURK:cult</ta>
            <ta e="T950" id="Seg_14526" s="T949">RUS:cult</ta>
            <ta e="T951" id="Seg_14527" s="T950">RUS:cult</ta>
            <ta e="T952" id="Seg_14528" s="T951">RUS:cult</ta>
            <ta e="T953" id="Seg_14529" s="T952">RUS:cult</ta>
            <ta e="T956" id="Seg_14530" s="T955">RUS:gram</ta>
            <ta e="T965" id="Seg_14531" s="T964">RUS:cult</ta>
            <ta e="T971" id="Seg_14532" s="T970">RUS:mod</ta>
            <ta e="T976" id="Seg_14533" s="T975">TURK:cult</ta>
            <ta e="T979" id="Seg_14534" s="T978">TURK:disc</ta>
            <ta e="T980" id="Seg_14535" s="T979">RUS:gram</ta>
            <ta e="T981" id="Seg_14536" s="T980">TURK:cult</ta>
            <ta e="T983" id="Seg_14537" s="T982">RUS:cult</ta>
            <ta e="T984" id="Seg_14538" s="T983">RUS:cult</ta>
            <ta e="T987" id="Seg_14539" s="T986">TURK:cult</ta>
            <ta e="T989" id="Seg_14540" s="T988">TURK:disc</ta>
            <ta e="T992" id="Seg_14541" s="T991">TURK:cult</ta>
            <ta e="T993" id="Seg_14542" s="T992">RUS:gram</ta>
            <ta e="T1002" id="Seg_14543" s="T1001">RUS:gram</ta>
            <ta e="T1005" id="Seg_14544" s="T1004">RUS:gram</ta>
            <ta e="T1011" id="Seg_14545" s="T1010">TURK:cult</ta>
            <ta e="T1014" id="Seg_14546" s="T1013">RUS:cult</ta>
            <ta e="T1028" id="Seg_14547" s="T1027">RUS:cult</ta>
            <ta e="T1041" id="Seg_14548" s="T1040">RUS:cult</ta>
            <ta e="T1044" id="Seg_14549" s="T1043">TURK:cult</ta>
            <ta e="T1062" id="Seg_14550" s="T1061">RUS:gram</ta>
            <ta e="T1067" id="Seg_14551" s="T1066">TURK:cult</ta>
            <ta e="T1070" id="Seg_14552" s="T1069">TURK:cult</ta>
            <ta e="T1076" id="Seg_14553" s="T1075">RUS:gram</ta>
            <ta e="T1078" id="Seg_14554" s="T1077">RUS:gram</ta>
            <ta e="T1084" id="Seg_14555" s="T1083">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-PKZ">
            <ta e="T919" id="Seg_14556" s="T918">Vsub</ta>
            <ta e="T937" id="Seg_14557" s="T936">Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph-PKZ" />
         <annotation name="CS" tierref="CS-PKZ">
            <ta e="T3" id="Seg_14558" s="T0">RUS:ext</ta>
            <ta e="T38" id="Seg_14559" s="T33">RUS:calq</ta>
            <ta e="T43" id="Seg_14560" s="T41">RUS:calq</ta>
            <ta e="T210" id="Seg_14561" s="T207">RUS:calq</ta>
            <ta e="T318" id="Seg_14562" s="T316">RUS:calq</ta>
            <ta e="T347" id="Seg_14563" s="T344">RUS:calq</ta>
            <ta e="T437" id="Seg_14564" s="T435">RUS:calq</ta>
            <ta e="T462" id="Seg_14565" s="T459">RUS:calq</ta>
            <ta e="T478" id="Seg_14566" s="T476">RUS:calq</ta>
            <ta e="T612" id="Seg_14567" s="T609">RUS:calq</ta>
            <ta e="T632" id="Seg_14568" s="T629">RUS:ext</ta>
            <ta e="T893" id="Seg_14569" s="T892">RUS:int.ins</ta>
            <ta e="T1064" id="Seg_14570" s="T1063">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-PKZ">
            <ta e="T3" id="Seg_14571" s="T0">Ты уже запустил [магнитофон]?</ta>
            <ta e="T10" id="Seg_14572" s="T4">Жил человек, у него было три сына.</ta>
            <ta e="T17" id="Seg_14573" s="T10">Один Гаврила, один Данила, один Ванюшка дурачок.</ta>
            <ta e="T23" id="Seg_14574" s="T17">Тогда этот человек сеял пшеницу.</ta>
            <ta e="T33" id="Seg_14575" s="T23">И кто-то приходит по вечерам и ест пшеницу.</ta>
            <ta e="T41" id="Seg_14576" s="T33">"Надо, говорит, пойти посмотреть, кто ест пшеницу.</ta>
            <ta e="T43" id="Seg_14577" s="T41">Надо поймать [его]."</ta>
            <ta e="T49" id="Seg_14578" s="T43">Тогда пошёл туда старший сын, Гаврила.</ta>
            <ta e="T55" id="Seg_14579" s="T49">Пошёл, поспал в траве и вернулся домой.</ta>
            <ta e="T62" id="Seg_14580" s="T55">"Я сидел, не спал, но никого не видел."</ta>
            <ta e="T67" id="Seg_14581" s="T62">Тогда идёт другой сын, Данила его.</ta>
            <ta e="T70" id="Seg_14582" s="T67">Пошёл, поспал в траве.</ta>
            <ta e="T73" id="Seg_14583" s="T70">Потом утром пришёл.</ta>
            <ta e="T81" id="Seg_14584" s="T73">"Целую ночь я не спал, но никого не видел."</ta>
            <ta e="T84" id="Seg_14585" s="T81">Тогда Ванюшка пошёл.</ta>
            <ta e="T86" id="Seg_14586" s="T84">Взял свой аркан.</ta>
            <ta e="T89" id="Seg_14587" s="T86">Потом сел на камень.</ta>
            <ta e="T92" id="Seg_14588" s="T89">Не спит, сидит.</ta>
            <ta e="T95" id="Seg_14589" s="T92">Видит, пришёл конь.</ta>
            <ta e="T102" id="Seg_14590" s="T95">Шерсть у него на шкуре красивая.</ta>
            <ta e="T109" id="Seg_14591" s="T102">Он бросил на него аркан и поймал.</ta>
            <ta e="T114" id="Seg_14592" s="T109">Тот прыгнул, но не смог убежать.</ta>
            <ta e="T117" id="Seg_14593" s="T114">Потом [говорит]: "Отпусти меня!</ta>
            <ta e="T122" id="Seg_14594" s="T117">Я тебе что угодно дам."</ta>
            <ta e="T129" id="Seg_14595" s="T122">"Ну, что, скажи, больше не будешь пшеницу есть?"</ta>
            <ta e="T131" id="Seg_14596" s="T129">"Не буду есть!"</ta>
            <ta e="T136" id="Seg_14597" s="T131">"А как ты ко мне придёшь?"</ta>
            <ta e="T141" id="Seg_14598" s="T136">Иди сюда, (?) мне.</ta>
            <ta e="T143" id="Seg_14599" s="T141">Ну, вот так.</ta>
            <ta e="T151" id="Seg_14600" s="T143">И конь ускакал, а он вернулся домой.</ta>
            <ta e="T153" id="Seg_14601" s="T151">Влез на печь.</ta>
            <ta e="T157" id="Seg_14602" s="T153">"Я там коня поймал.</ta>
            <ta e="T163" id="Seg_14603" s="T157">Теперь никогда не будет приходить пшеницу есть."</ta>
            <ta e="T169" id="Seg_14604" s="T163">Потом царь сделал, чтобы люди пришли (/созвал людей).</ta>
            <ta e="T174" id="Seg_14605" s="T169">Его дочь живёт в тереме.</ta>
            <ta e="T177" id="Seg_14606" s="T174">Все люди идут.</ta>
            <ta e="T182" id="Seg_14607" s="T177">И его братья тоже идут.</ta>
            <ta e="T187" id="Seg_14608" s="T182">А он говорит: "Возьмите меня!"</ta>
            <ta e="T190" id="Seg_14609" s="T187">"Куда ты пойдёшь?</ta>
            <ta e="T199" id="Seg_14610" s="T190">Ты ничего там не увидишь, (разве пойдёшь затем,) чтобы люди над тобой смеялись?"</ta>
            <ta e="T204" id="Seg_14611" s="T199">Они ушли, он встал.</ta>
            <ta e="T207" id="Seg_14612" s="T204">Взял корзину, пошёл.</ta>
            <ta e="T212" id="Seg_14613" s="T207">И давай кричать, [тут] конь его прискакал.</ta>
            <ta e="T216" id="Seg_14614" s="T212">Вот они идут в (?).</ta>
            <ta e="T219" id="Seg_14615" s="T216">Приходит [Ванюшка?] к этому человеку [к царю?].</ta>
            <ta e="T224" id="Seg_14616" s="T219">Потом конь говорит…</ta>
            <ta e="T226" id="Seg_14617" s="T224">Стоит около (него?).</ta>
            <ta e="T232" id="Seg_14618" s="T226">Говорит: "В одно ухо влезь, в другое вылези".</ta>
            <ta e="T235" id="Seg_14619" s="T232">Он влез, вылез.</ta>
            <ta e="T237" id="Seg_14620" s="T235">Красивым стал.</ta>
            <ta e="T240" id="Seg_14621" s="T237">Потом сел на [коня].</ta>
            <ta e="T243" id="Seg_14622" s="T240">[Конь] поскакал.</ta>
            <ta e="T254" id="Seg_14623" s="T243">Много народу стоит, но никто к этой [царской] дочери залезть не может.</ta>
            <ta e="T257" id="Seg_14624" s="T254">Он прыгнул.</ta>
            <ta e="T261" id="Seg_14625" s="T257">Они прячутся, не поймали [его?].</ta>
            <ta e="T267" id="Seg_14626" s="T261">Тогда люди закричали: "Лови [его], лови!"</ta>
            <ta e="T270" id="Seg_14627" s="T267">А он убежал.</ta>
            <ta e="T272" id="Seg_14628" s="T270">Коня отпустил.</ta>
            <ta e="T280" id="Seg_14629" s="T272">Потом собрал грибов и всякого другого принёс.</ta>
            <ta e="T283" id="Seg_14630" s="T280">А женщины ругаются:</ta>
            <ta e="T287" id="Seg_14631" s="T283">"Зачем такие принёс?</ta>
            <ta e="T290" id="Seg_14632" s="T287">Только тебе есть [их]."</ta>
            <ta e="T295" id="Seg_14633" s="T290">Он на печь залез, лежит.</ta>
            <ta e="T304" id="Seg_14634" s="T295">Потом его братья пришли, рассказывают, как всё было.</ta>
            <ta e="T307" id="Seg_14635" s="T304">А он лежит.</ta>
            <ta e="T312" id="Seg_14636" s="T307">"Это я там был!"</ta>
            <ta e="T314" id="Seg_14637" s="T312">"Не ври!</ta>
            <ta e="T316" id="Seg_14638" s="T314">Ты там был.</ta>
            <ta e="T318" id="Seg_14639" s="T316">Сиди уж.</ta>
            <ta e="T321" id="Seg_14640" s="T318">Не болтай.</ta>
            <ta e="T328" id="Seg_14641" s="T321">Такой был парень, конь у него красивый был свой."</ta>
            <ta e="T332" id="Seg_14642" s="T328">Потом назавтра снова идут.</ta>
            <ta e="T334" id="Seg_14643" s="T332">Ушли.</ta>
            <ta e="T338" id="Seg_14644" s="T334">Он с печи спрыгнул.</ta>
            <ta e="T340" id="Seg_14645" s="T338">Взял корзину.</ta>
            <ta e="T344" id="Seg_14646" s="T340">Пошёл, пошёл, выбросил [корзину?].</ta>
            <ta e="T347" id="Seg_14647" s="T344">Давай коня звать.</ta>
            <ta e="T349" id="Seg_14648" s="T347">Конь его прискакал.</ta>
            <ta e="T355" id="Seg_14649" s="T349">Снова в одно ухо влез, в другое вылез.</ta>
            <ta e="T357" id="Seg_14650" s="T355">Потом сел.</ta>
            <ta e="T362" id="Seg_14651" s="T357">Люди приходят, многие стоят.</ta>
            <ta e="T367" id="Seg_14652" s="T362">И никто туда не бежит.</ta>
            <ta e="T370" id="Seg_14653" s="T367">Он туда прыгнул.</ta>
            <ta e="T377" id="Seg_14654" s="T370">Схватил… не смог. [?]</ta>
            <ta e="T379" id="Seg_14655" s="T377">Потом вернулся.</ta>
            <ta e="T385" id="Seg_14656" s="T379">Братьев ударил [хлыстом?] и исчез.</ta>
            <ta e="T388" id="Seg_14657" s="T385">А люди кричат.</ta>
            <ta e="T394" id="Seg_14658" s="T388">"Лови [его], лови [его]," а он убежал.</ta>
            <ta e="T400" id="Seg_14659" s="T394">Отпустил коня, а сам домой вернулся.</ta>
            <ta e="T403" id="Seg_14660" s="T400">Женщины снова ругаются.</ta>
            <ta e="T409" id="Seg_14661" s="T403">А он влез на печь и лежит.</ta>
            <ta e="T412" id="Seg_14662" s="T409">Пришли братья.</ta>
            <ta e="T414" id="Seg_14663" s="T412">Рассказывают отцу.</ta>
            <ta e="T427" id="Seg_14664" s="T414">"Что за парень был, лошадь у него очень красивая была, и сам красивый," а тот лежит.</ta>
            <ta e="T433" id="Seg_14665" s="T427">"Не я ли там был (/Не моя ли голова там была)?"</ta>
            <ta e="T437" id="Seg_14666" s="T433">"Не ври, лежи уж!</ta>
            <ta e="T439" id="Seg_14667" s="T437">Дурачок!</ta>
            <ta e="T442" id="Seg_14668" s="T439">[Как будто] ты там был!"</ta>
            <ta e="T448" id="Seg_14669" s="T442">Потом снова, на третий день, снова они уходят.</ta>
            <ta e="T453" id="Seg_14670" s="T448">Они ушли, он поднялся.</ta>
            <ta e="T456" id="Seg_14671" s="T453">Взял корзину и пошёл.</ta>
            <ta e="T458" id="Seg_14672" s="T456">Грибы собирать.</ta>
            <ta e="T462" id="Seg_14673" s="T458">Он пошёл, стал звать коня.</ta>
            <ta e="T464" id="Seg_14674" s="T462">Конь прискакал.</ta>
            <ta e="T475" id="Seg_14675" s="T464">Он опять в одно ухо влез, в другое вылез, сел [на коня], поехал.</ta>
            <ta e="T478" id="Seg_14676" s="T475">Потом как поскакал.</ta>
            <ta e="T487" id="Seg_14677" s="T478">Он её поцеловал и забрал её колечко, себе на руку надел.</ta>
            <ta e="T489" id="Seg_14678" s="T487">Уехал прочь.</ta>
            <ta e="T498" id="Seg_14679" s="T489">Коня отпустил, сам пришёл домой, на печь залез и лежит.</ta>
            <ta e="T501" id="Seg_14680" s="T498">Тут его братья пришли.</ta>
            <ta e="T508" id="Seg_14681" s="T501">"Ну, какой парень красивый, конь у него красивый.</ta>
            <ta e="T511" id="Seg_14682" s="T508">Поцеловал царскую дочь.</ta>
            <ta e="T514" id="Seg_14683" s="T511">Царя дочь.</ta>
            <ta e="T519" id="Seg_14684" s="T514">И колечко её забрал, и прочь уехал".</ta>
            <ta e="T524" id="Seg_14685" s="T519">Потом царь опять [так] сделал.</ta>
            <ta e="T533" id="Seg_14686" s="T524">Положил много мяса, яиц, всё, что было положил.</ta>
            <ta e="T542" id="Seg_14687" s="T533">Потом [говорит]: "Приходите отовсюду, кто не придёт, голову отрублю".</ta>
            <ta e="T552" id="Seg_14688" s="T542">Потом они, их отец, отовсюду [люди] отправились и пришли.</ta>
            <ta e="T559" id="Seg_14689" s="T552">Всех усадили, накормили (/напоили), водки дали.</ta>
            <ta e="T565" id="Seg_14690" s="T559">Потом пришёл этот последний сын, он [Ванюшка].</ta>
            <ta e="T574" id="Seg_14691" s="T565">Он сидит чёрный (/грязный), и руки грязные, тряпкой замотанные.</ta>
            <ta e="T578" id="Seg_14692" s="T574">[Царевна] говорит: "Что у тебя на руке?"</ta>
            <ta e="T585" id="Seg_14693" s="T578">Видит: там её колечко.</ta>
            <ta e="T589" id="Seg_14694" s="T585">Тогда она берёт его и ведёт к своему отцу.</ta>
            <ta e="T594" id="Seg_14695" s="T589">"Вот, вот мой жених".</ta>
            <ta e="T600" id="Seg_14696" s="T594">Они его там вымыли.</ta>
            <ta e="T604" id="Seg_14697" s="T600">Одели в красивую одежду.</ta>
            <ta e="T607" id="Seg_14698" s="T604">Он стал очень красивым.</ta>
            <ta e="T612" id="Seg_14699" s="T607">Потом снова стали водку пить.</ta>
            <ta e="T613" id="Seg_14700" s="T612">Играть.</ta>
            <ta e="T616" id="Seg_14701" s="T613">Его братья смотрят.</ta>
            <ta e="T619" id="Seg_14702" s="T616">Каким красивым стал!</ta>
            <ta e="T623" id="Seg_14703" s="T619">И я там была.</ta>
            <ta e="T626" id="Seg_14704" s="T623">Водка текла.</ta>
            <ta e="T629" id="Seg_14705" s="T626">Дали мне блин.</ta>
            <ta e="T633" id="Seg_14706" s="T629">Три года у лоханки. </ta>
            <ta e="T638" id="Seg_14707" s="T633">Жили вдвоём кум да кума.</ta>
            <ta e="T641" id="Seg_14708" s="T638">Волк да лиса.</ta>
            <ta e="T644" id="Seg_14709" s="T641">Вот ложатся они спать.</ta>
            <ta e="T649" id="Seg_14710" s="T644">А лиса всё стучит, стучит.</ta>
            <ta e="T659" id="Seg_14711" s="T649">А волк говорит: "Кто-то стучит". — "Меня зовут, ребёнка кормить".</ta>
            <ta e="T662" id="Seg_14712" s="T659">Тогда [волк говорит]: "Ну, иди!"</ta>
            <ta e="T664" id="Seg_14713" s="T662">Она пошла.</ta>
            <ta e="T671" id="Seg_14714" s="T664">А они (стояли?), мёд поела, поела мёд.</ta>
            <ta e="T673" id="Seg_14715" s="T671">Вернулась она.</ta>
            <ta e="T676" id="Seg_14716" s="T673">"Кто там?"</ta>
            <ta e="T678" id="Seg_14717" s="T676">"Вершок".</ta>
            <ta e="T684" id="Seg_14718" s="T678">Потом вечером опять лиса стучит.</ta>
            <ta e="T686" id="Seg_14719" s="T684">"Кто стучит?"</ta>
            <ta e="T691" id="Seg_14720" s="T686">"Меня зовут, ребёнка (распеленать?)".</ta>
            <ta e="T693" id="Seg_14721" s="T691">"Ну, иди!"</ta>
            <ta e="T695" id="Seg_14722" s="T693">Она пошла.</ta>
            <ta e="T702" id="Seg_14723" s="T695">Она ела, ела, потм вернулась: "Кто там пришёл?"</ta>
            <ta e="T704" id="Seg_14724" s="T702">"Серёдушок пришёл".</ta>
            <ta e="T709" id="Seg_14725" s="T704">Потом на третий вечер снова стучит.</ta>
            <ta e="T715" id="Seg_14726" s="T709">Потом: "Кто-то стучит, меня зовут!"</ta>
            <ta e="T717" id="Seg_14727" s="T715">«Ну, иди!»</ta>
            <ta e="T720" id="Seg_14728" s="T717">Потом она поела.</ta>
            <ta e="T722" id="Seg_14729" s="T720">Пришла.</ta>
            <ta e="T724" id="Seg_14730" s="T722">«Кто там [был]?»</ta>
            <ta e="T727" id="Seg_14731" s="T724">«Этот, Скрёбушок пришёл».</ta>
            <ta e="T732" id="Seg_14732" s="T727">Потом утром встали, [лиса] заболела.</ta>
            <ta e="T736" id="Seg_14733" s="T732">Говорит: «Принеси мне [чего-нибудь] сладкого».</ta>
            <ta e="T738" id="Seg_14734" s="T736">Он пошёл.</ta>
            <ta e="T742" id="Seg_14735" s="T738">А ничего нет, кто-то съел.</ta>
            <ta e="T745" id="Seg_14736" s="T742">"Ты всё съёла!"</ta>
            <ta e="T751" id="Seg_14737" s="T745">Она: "Нет, я не ела!"</ta>
            <ta e="T761" id="Seg_14738" s="T751">Потом: «Давай ляжем на солнце, кто съел, у того живот сладким станет».</ta>
            <ta e="T765" id="Seg_14739" s="T761">Потом на свой живот посмотрела.</ta>
            <ta e="T767" id="Seg_14740" s="T765">Он сладкий.</ta>
            <ta e="T772" id="Seg_14741" s="T767">Она давай волку [живот] мазать.</ta>
            <ta e="T776" id="Seg_14742" s="T772">"Вставай, ты всё съел!"</ta>
            <ta e="T782" id="Seg_14743" s="T776">Потом он встал: "Ну, [значит,] я съел".</ta>
            <ta e="T784" id="Seg_14744" s="T782">Потом всё.</ta>
            <ta e="T788" id="Seg_14745" s="T784">Жили певчая птичка и тетерев.</ta>
            <ta e="T791" id="Seg_14746" s="T788">Как дом построить?</ta>
            <ta e="T796" id="Seg_14747" s="T791">Топора нет, кто топор сделает?</ta>
            <ta e="T801" id="Seg_14748" s="T796">"Да на что мне дом.</ta>
            <ta e="T804" id="Seg_14749" s="T801">Я в снегу переночую."</ta>
            <ta e="T808" id="Seg_14750" s="T804">И прыгнул в снег.</ta>
            <ta e="T812" id="Seg_14751" s="T808">Там переночевал, утром встал.</ta>
            <ta e="T819" id="Seg_14752" s="T812">И пошёл посмотреть, видит товарищей своих, они там играли.</ta>
            <ta e="T822" id="Seg_14753" s="T819">Они на дерево запрыгнули. [?]</ta>
            <ta e="T826" id="Seg_14754" s="T822">Потом сделали себе ггнёзда.</ta>
            <ta e="T828" id="Seg_14755" s="T826">Снесли яйца.</ta>
            <ta e="T832" id="Seg_14756" s="T828">Дети, появились у них дети.</ta>
            <ta e="T835" id="Seg_14757" s="T832">Они их кормили мошками (/мошкой), червями.</ta>
            <ta e="T840" id="Seg_14758" s="T835">Потом опять: "На что нам дом?</ta>
            <ta e="T842" id="Seg_14759" s="T840">Мы в снегу спали.</ta>
            <ta e="T847" id="Seg_14760" s="T842">На одну ночь в доме останемся.</ta>
            <ta e="T850" id="Seg_14761" s="T847">Мы везде смотрим.</ta>
            <ta e="T851" id="Seg_14762" s="T850">Хватит.</ta>
            <ta e="T855" id="Seg_14763" s="T851">Жили жена с мужем.</ta>
            <ta e="T859" id="Seg_14764" s="T855">У них ничего не было.</ta>
            <ta e="T861" id="Seg_14765" s="T859">Пошли они в тайгу.</ta>
            <ta e="T864" id="Seg_14766" s="T861">Шишки, жёлуди собирали.</ta>
            <ta e="T867" id="Seg_14767" s="T864">Вернулись, едят.</ta>
            <ta e="T870" id="Seg_14768" s="T867">Один [жёлудь] в подпол упал.</ta>
            <ta e="T873" id="Seg_14769" s="T870">И давай расти. </ta>
            <ta e="T876" id="Seg_14770" s="T873">Женщина говорит:</ta>
            <ta e="T879" id="Seg_14771" s="T876">"Проруби пол топором."</ta>
            <ta e="T881" id="Seg_14772" s="T879">Он прорубил.</ta>
            <ta e="T884" id="Seg_14773" s="T881">[Жёлудь] там пророс.</ta>
            <ta e="T893" id="Seg_14774" s="T884">Теперь доверху [дорос], [муж] там крышу снял.</ta>
            <ta e="T898" id="Seg_14775" s="T893">Там далеко вырос, даже выше [крыши].</ta>
            <ta e="T900" id="Seg_14776" s="T898">Тогда муж [говорит]:</ta>
            <ta e="T904" id="Seg_14777" s="T900">"Надо пойти жёлуди собрать".</ta>
            <ta e="T911" id="Seg_14778" s="T904">Он лез, лез, там наверху смотрит.</ta>
            <ta e="T914" id="Seg_14779" s="T911">Стоит дом.</ta>
            <ta e="T917" id="Seg_14780" s="T914">Он вошёл в дом.</ta>
            <ta e="T921" id="Seg_14781" s="T917">Там петух живёт.</ta>
            <ta e="T923" id="Seg_14782" s="T921">Голова у него красивая.</ta>
            <ta e="T925" id="Seg_14783" s="T923">Вся красная.</ta>
            <ta e="T936" id="Seg_14784" s="T925">И были у него жернова, он их берёт и говорит: "На, [это] тебе".</ta>
            <ta e="T940" id="Seg_14785" s="T936">Петух, красная голова.</ta>
            <ta e="T948" id="Seg_14786" s="T940">Он взял и начал мельницу крутить.</ta>
            <ta e="T958" id="Seg_14787" s="T948">Оттуда блины, пироги, блины, пироги, муж поел, жена поела.</ta>
            <ta e="T965" id="Seg_14788" s="T958">Всегда (/долго) так жили, потом приходит к ним барин.</ta>
            <ta e="T968" id="Seg_14789" s="T965">"Принесите мне поесть".</ta>
            <ta e="T976" id="Seg_14790" s="T968">Он слышал, правда, об этой мельнице.</ta>
            <ta e="T984" id="Seg_14791" s="T976">Тогда жена стала мельницу крутить, [делать] блины, пироги.</ta>
            <ta e="T986" id="Seg_14792" s="T984">Он поел.</ta>
            <ta e="T988" id="Seg_14793" s="T986">"Продай мне [её]".</ta>
            <ta e="T992" id="Seg_14794" s="T988">"Нет, не продадим.</ta>
            <ta e="T997" id="Seg_14795" s="T992">А то что нам будет есть?"</ta>
            <ta e="T1001" id="Seg_14796" s="T997">Тогда он вечером пришёл.</ta>
            <ta e="T1004" id="Seg_14797" s="T1001">И украл её.</ta>
            <ta e="T1006" id="Seg_14798" s="T1004">И унёс.</ta>
            <ta e="T1011" id="Seg_14799" s="T1006">Утром они встают: мельницы нет.</ta>
            <ta e="T1014" id="Seg_14800" s="T1011">Тогда петух [сказал]:</ta>
            <ta e="T1016" id="Seg_14801" s="T1014">"Не плачьте!</ta>
            <ta e="T1018" id="Seg_14802" s="T1016">Я пойду.</ta>
            <ta e="T1022" id="Seg_14803" s="T1018">Принесу вам её назад."</ta>
            <ta e="T1023" id="Seg_14804" s="T1022">Пошёл.</ta>
            <ta e="T1028" id="Seg_14805" s="T1023">Идёт, идёт, навстречу лиса.</ta>
            <ta e="T1030" id="Seg_14806" s="T1028">"Куда ты идёшь?</ta>
            <ta e="T1032" id="Seg_14807" s="T1030">Возьми меня [с собой]!"</ta>
            <ta e="T1037" id="Seg_14808" s="T1032">"Полезай(?) ко мне за пазуху". Он (?).</ta>
            <ta e="T1040" id="Seg_14809" s="T1037">Потом идёт, идёт.</ta>
            <ta e="T1041" id="Seg_14810" s="T1040">Волка [встречает].</ta>
            <ta e="T1043" id="Seg_14811" s="T1041">"Куда ты идёшь?"</ta>
            <ta e="T1045" id="Seg_14812" s="T1043">"Мельницу [назад] забрать".</ta>
            <ta e="T1048" id="Seg_14813" s="T1045">"Возьми меня [с собой]!"</ta>
            <ta e="T1051" id="Seg_14814" s="T1048">"Полезай(?) ко мне за пазуху".</ta>
            <ta e="T1053" id="Seg_14815" s="T1051">Тот (залез?).</ta>
            <ta e="T1057" id="Seg_14816" s="T1053">Потом идёт, [навстречу] медведь идёт.</ta>
            <ta e="T1059" id="Seg_14817" s="T1057">"Полезай(?) ко мне!"</ta>
            <ta e="T1061" id="Seg_14818" s="T1059">Потом пришёл [к барину].</ta>
            <ta e="T1063" id="Seg_14819" s="T1061">Стал кричать.</ta>
            <ta e="T1064" id="Seg_14820" s="T1063">"Отдай!</ta>
            <ta e="T1067" id="Seg_14821" s="T1064">Неси мою мельницу!</ta>
            <ta e="T1070" id="Seg_14822" s="T1067">Неси мою мельницу!"</ta>
            <ta e="T1072" id="Seg_14823" s="T1070">[Барин] говорит: </ta>
            <ta e="T1075" id="Seg_14824" s="T1072">"Выбросьте его [прочь]!</ta>
            <ta e="T1078" id="Seg_14825" s="T1075">Пусть [отправляется] к уткам.</ta>
            <ta e="T1081" id="Seg_14826" s="T1078">Пусть утки съедят его кости. [?]</ta>
            <ta e="T1086" id="Seg_14827" s="T1081">Его прочь выкинули, а он (говорит?).</ta>
         </annotation>
         <annotation name="fe" tierref="fe-PKZ">
            <ta e="T3" id="Seg_14828" s="T0">Did you already start [the tape]?</ta>
            <ta e="T10" id="Seg_14829" s="T4">There lived a man, he had three sons.</ta>
            <ta e="T17" id="Seg_14830" s="T10">One was Gavrila, one Danila, one was Vanyushka the fool.</ta>
            <ta e="T23" id="Seg_14831" s="T17">Then this man sowed wheat.</ta>
            <ta e="T33" id="Seg_14832" s="T23">And someone comes in the evening and eats the wheat.</ta>
            <ta e="T41" id="Seg_14833" s="T33">[We] should, he says, go and see, who is eating the wheat.</ta>
            <ta e="T43" id="Seg_14834" s="T41">[We] should catch [them].</ta>
            <ta e="T49" id="Seg_14835" s="T43">Then the older son Gavrila went there.</ta>
            <ta e="T55" id="Seg_14836" s="T49">He went, he slept in the grass, and came home.</ta>
            <ta e="T62" id="Seg_14837" s="T55">“I sat, I didn't sleep, but I didn't see anyone."</ta>
            <ta e="T67" id="Seg_14838" s="T62">Then the other son goes, his Danila.</ta>
            <ta e="T70" id="Seg_14839" s="T67">He went, he slept in the grass.</ta>
            <ta e="T73" id="Seg_14840" s="T70">Then in the morning he came.</ta>
            <ta e="T81" id="Seg_14841" s="T73">"The whole night I didn’t sleep, but I didn’t see anyone."</ta>
            <ta e="T84" id="Seg_14842" s="T81">Then Vanyushka went.</ta>
            <ta e="T86" id="Seg_14843" s="T84">He took his lasso.</ta>
            <ta e="T89" id="Seg_14844" s="T86">Then he sat on a rock.</ta>
            <ta e="T92" id="Seg_14845" s="T89">He doesn't sleep, he is sitting.</ta>
            <ta e="T95" id="Seg_14846" s="T92">He sees: a horse came.</ta>
            <ta e="T102" id="Seg_14847" s="T95">On its skin, the fur is beautiful.</ta>
            <ta e="T109" id="Seg_14848" s="T102">He threw his lasso onto it and caught [it].</ta>
            <ta e="T114" id="Seg_14849" s="T109">It jumped, could not jump [away].</ta>
            <ta e="T117" id="Seg_14850" s="T114">Then [it says]: “Let me go!</ta>
            <ta e="T122" id="Seg_14851" s="T117">I will give you anything."</ta>
            <ta e="T129" id="Seg_14852" s="T122">"Well, how, tell [me], will you not eat any more wheat?"</ta>
            <ta e="T131" id="Seg_14853" s="T129">"I won't eat [it]!"</ta>
            <ta e="T136" id="Seg_14854" s="T131">But how will you come like this to me?</ta>
            <ta e="T141" id="Seg_14855" s="T136">Come here, (?) me.</ta>
            <ta e="T143" id="Seg_14856" s="T141">Well, like this.</ta>
            <ta e="T151" id="Seg_14857" s="T143">And the horse left, and he came home.</ta>
            <ta e="T153" id="Seg_14858" s="T151">He climbed on the stove.</ta>
            <ta e="T157" id="Seg_14859" s="T153">I caught a horse there.</ta>
            <ta e="T163" id="Seg_14860" s="T157">Now it will never come to eat the wheat.</ta>
            <ta e="T169" id="Seg_14861" s="T163">Then the tsar made (/called) so that people came.</ta>
            <ta e="T174" id="Seg_14862" s="T169">His daughter is living in a tower.</ta>
            <ta e="T177" id="Seg_14863" s="T174">All the people are going.</ta>
            <ta e="T182" id="Seg_14864" s="T177">And his brothers also go.</ta>
            <ta e="T187" id="Seg_14865" s="T182">But he says: “Take me!".</ta>
            <ta e="T190" id="Seg_14866" s="T187">“Where will you go?".</ta>
            <ta e="T199" id="Seg_14867" s="T190">You would not see anything there, so that people would laugh at you?</ta>
            <ta e="T204" id="Seg_14868" s="T199">They left, he got up.</ta>
            <ta e="T207" id="Seg_14869" s="T204">He took a basket, he went.</ta>
            <ta e="T212" id="Seg_14870" s="T207">He starts to call, his horse came.</ta>
            <ta e="T216" id="Seg_14871" s="T212">They go there in (?).</ta>
            <ta e="T219" id="Seg_14872" s="T216">He [Vanyushka?] comes to this man [to the tsar?].</ta>
            <ta e="T224" id="Seg_14873" s="T219">Then the horse says…</ta>
            <ta e="T226" id="Seg_14874" s="T224">It stood by his (side?).</ta>
            <ta e="T232" id="Seg_14875" s="T226">It says: enter into one ear, come out from one (the other).</ta>
            <ta e="T235" id="Seg_14876" s="T232">He entered, came out.</ta>
            <ta e="T237" id="Seg_14877" s="T235">He became beautiful.</ta>
            <ta e="T240" id="Seg_14878" s="T237">Then he sat on it.</ta>
            <ta e="T243" id="Seg_14879" s="T240">It ran.</ta>
            <ta e="T254" id="Seg_14880" s="T243">There are many people standing, but no one, does not climb to the daughter.</ta>
            <ta e="T257" id="Seg_14881" s="T254">He jumped.</ta>
            <ta e="T261" id="Seg_14882" s="T257">They are hiding, they did not, did not catch [him?].</ta>
            <ta e="T267" id="Seg_14883" s="T261">Then the people are shouting: "Catch [him], catch [him]".</ta>
            <ta e="T270" id="Seg_14884" s="T267">But he ran [away].</ta>
            <ta e="T272" id="Seg_14885" s="T270">He sent away his horse.</ta>
            <ta e="T280" id="Seg_14886" s="T272">Then he picked mushrooms and brought whatever else there was.</ta>
            <ta e="T283" id="Seg_14887" s="T280">But the women scold:</ta>
            <ta e="T287" id="Seg_14888" s="T283">"Why did you bring such [mushrooms]?</ta>
            <ta e="T290" id="Seg_14889" s="T287">Only for you to eat."</ta>
            <ta e="T295" id="Seg_14890" s="T290">He climbed on the stove, he is lying there.</ta>
            <ta e="T304" id="Seg_14891" s="T295">Then his brothers came, they are telling how it was [there].</ta>
            <ta e="T307" id="Seg_14892" s="T304">But he is lying.</ta>
            <ta e="T312" id="Seg_14893" s="T307">"It was myself (/my head) there".</ta>
            <ta e="T314" id="Seg_14894" s="T312">Don't lie!</ta>
            <ta e="T316" id="Seg_14895" s="T314">Your head.</ta>
            <ta e="T318" id="Seg_14896" s="T316">Keep seated.</ta>
            <ta e="T321" id="Seg_14897" s="T318">Talk no more.</ta>
            <ta e="T328" id="Seg_14898" s="T321">Such a man [he] was, his own horse was beautiful.</ta>
            <ta e="T332" id="Seg_14899" s="T328">Then the next day they go [there] again.</ta>
            <ta e="T334" id="Seg_14900" s="T332">They left.</ta>
            <ta e="T338" id="Seg_14901" s="T334">He jumped off from the stove.</ta>
            <ta e="T340" id="Seg_14902" s="T338">He took a basket.</ta>
            <ta e="T344" id="Seg_14903" s="T340">He went, he went, threw it [the basket?] away.</ta>
            <ta e="T347" id="Seg_14904" s="T344">He starts to call the horse.</ta>
            <ta e="T349" id="Seg_14905" s="T347">His horse came.</ta>
            <ta e="T355" id="Seg_14906" s="T349">Once again he entered on ear and came out (from) another.</ta>
            <ta e="T357" id="Seg_14907" s="T355">Then he sat.</ta>
            <ta e="T362" id="Seg_14908" s="T357">People come, many are standing.</ta>
            <ta e="T367" id="Seg_14909" s="T362">Then no one ran there.</ta>
            <ta e="T370" id="Seg_14910" s="T367">He jumped there.</ta>
            <ta e="T377" id="Seg_14911" s="T370">He took… he could not. [?]</ta>
            <ta e="T379" id="Seg_14912" s="T377">Then he returned.</ta>
            <ta e="T385" id="Seg_14913" s="T379">He beat his brothers [with a whip?] and left.</ta>
            <ta e="T388" id="Seg_14914" s="T385">And the people are shouting.</ta>
            <ta e="T394" id="Seg_14915" s="T388">“Catch [him], catch [him]", and he ran away.</ta>
            <ta e="T400" id="Seg_14916" s="T394">He sent away his horse, and he himself came [home] again.</ta>
            <ta e="T403" id="Seg_14917" s="T400">The women scold again.</ta>
            <ta e="T409" id="Seg_14918" s="T403">But he climbed on the stove and is lying there.</ta>
            <ta e="T412" id="Seg_14919" s="T409">His brothers came.</ta>
            <ta e="T414" id="Seg_14920" s="T412">They are telling his father.</ta>
            <ta e="T427" id="Seg_14921" s="T414">"What a man it was, his horse [was] very beautiful and himself [was] beautiful", and he is lying.</ta>
            <ta e="T433" id="Seg_14922" s="T427">"Wasn’t it my head there."</ta>
            <ta e="T437" id="Seg_14923" s="T433">"Don't tell lies, lie down!</ta>
            <ta e="T439" id="Seg_14924" s="T437">Crazy boy!</ta>
            <ta e="T442" id="Seg_14925" s="T439">[As if] you were there!"</ta>
            <ta e="T448" id="Seg_14926" s="T442">Then again, [on] the third day they go again.</ta>
            <ta e="T453" id="Seg_14927" s="T448">They left, he got up.</ta>
            <ta e="T456" id="Seg_14928" s="T453">He took his basket and went.</ta>
            <ta e="T458" id="Seg_14929" s="T456">To pick mushrooms.</ta>
            <ta e="T462" id="Seg_14930" s="T458">He went, started to call the horse.</ta>
            <ta e="T464" id="Seg_14931" s="T462">The horse came.</ta>
            <ta e="T475" id="Seg_14932" s="T464">He entered one ear again, came out from one [the other], sat [on the horse], went.</ta>
            <ta e="T478" id="Seg_14933" s="T475">Then he gallopped away.</ta>
            <ta e="T487" id="Seg_14934" s="T478">He kissed her and took her ring, he wore his it in his hand.</ta>
            <ta e="T489" id="Seg_14935" s="T487">He left.</ta>
            <ta e="T498" id="Seg_14936" s="T489">He sent away his horse, he himself went home, climbed on the stove, he lies there.</ta>
            <ta e="T501" id="Seg_14937" s="T498">Then his brothers came.</ta>
            <ta e="T508" id="Seg_14938" s="T501">"Well, what a handsome boy, his horse [was] beautiful.</ta>
            <ta e="T511" id="Seg_14939" s="T508">He kissed the tsar’s daughter.</ta>
            <ta e="T514" id="Seg_14940" s="T511">The daughter of the tsar.</ta>
            <ta e="T519" id="Seg_14941" s="T514">And he took her ring, he left."</ta>
            <ta e="T524" id="Seg_14942" s="T519">Then the tsar made [so] again.</ta>
            <ta e="T533" id="Seg_14943" s="T524">He set a lot of meat, eggs, he put out everything that there is.</ta>
            <ta e="T542" id="Seg_14944" s="T533">Then: “Come from everywhere, I will chop off the heads of those who do not come".</ta>
            <ta e="T552" id="Seg_14945" s="T542">Then they, his father, from everywhere, left and they came.</ta>
            <ta e="T559" id="Seg_14946" s="T552">Everybody was sat down, fed, given vodka.</ta>
            <ta e="T565" id="Seg_14947" s="T559">Then came the last son, he [Vanyushka].</ta>
            <ta e="T574" id="Seg_14948" s="T565">He sits black (/dirty), his hands dirty, wrapped up with a rag.</ta>
            <ta e="T578" id="Seg_14949" s="T574">[The tsar's daughter] says: "What's in your hand?"</ta>
            <ta e="T585" id="Seg_14950" s="T578">She sees: her ring is (sitting) [there].</ta>
            <ta e="T589" id="Seg_14951" s="T585">Then she takes him and goes to her father.</ta>
            <ta e="T594" id="Seg_14952" s="T589">"This, this is my groom."</ta>
            <ta e="T600" id="Seg_14953" s="T594">They washed him there.</ta>
            <ta e="T604" id="Seg_14954" s="T600">They dressed him in beautiful clothes.</ta>
            <ta e="T607" id="Seg_14955" s="T604">He became very beautiful.</ta>
            <ta e="T612" id="Seg_14956" s="T607">Then again they started to drink vodka.</ta>
            <ta e="T613" id="Seg_14957" s="T612">To play.</ta>
            <ta e="T616" id="Seg_14958" s="T613">His brothers are looking.</ta>
            <ta e="T619" id="Seg_14959" s="T616">How beautiful he became!</ta>
            <ta e="T623" id="Seg_14960" s="T619">And I was there too!</ta>
            <ta e="T626" id="Seg_14961" s="T623">Vodka is flowing.</ta>
            <ta e="T629" id="Seg_14962" s="T626">They gave me pancake(s).</ta>
            <ta e="T633" id="Seg_14963" s="T629">Three years by the bowl (= drinking).</ta>
            <ta e="T638" id="Seg_14964" s="T633">Together lived a fellow (m) and a fellow (f) (=a married couple).</ta>
            <ta e="T641" id="Seg_14965" s="T638">Wolf and fox.</ta>
            <ta e="T644" id="Seg_14966" s="T641">Then they lie down to sleep.</ta>
            <ta e="T649" id="Seg_14967" s="T644">But the fox is always tapping, tapping.</ta>
            <ta e="T659" id="Seg_14968" s="T649">And the wolf says: “Someone is knocking." — “They are calling me, to feed the child."</ta>
            <ta e="T662" id="Seg_14969" s="T659">Then [the wolf says]: “Well, go!"</ta>
            <ta e="T664" id="Seg_14970" s="T662">She [the fox] went.</ta>
            <ta e="T671" id="Seg_14971" s="T664">And they (stood?), [the fox] ate the honey, ate the honey.</ta>
            <ta e="T673" id="Seg_14972" s="T671">She came.</ta>
            <ta e="T676" id="Seg_14973" s="T673">Who is there?</ta>
            <ta e="T678" id="Seg_14974" s="T676">It is Vershok (/Upperside).</ta>
            <ta e="T684" id="Seg_14975" s="T678">Then again in the evening, again the fox is tapping.</ta>
            <ta e="T686" id="Seg_14976" s="T684">"Who is knocking?"</ta>
            <ta e="T691" id="Seg_14977" s="T686">"They are calling me, to (unwrap?) the child."</ta>
            <ta e="T693" id="Seg_14978" s="T691">“Well, go!"</ta>
            <ta e="T695" id="Seg_14979" s="T693">She went.</ta>
            <ta e="T702" id="Seg_14980" s="T695">She ate, ate, then she came: “Who came there?"</ta>
            <ta e="T704" id="Seg_14981" s="T702">“Serjodushok came."</ta>
            <ta e="T709" id="Seg_14982" s="T704">Then the third evening again he is knocking.</ta>
            <ta e="T715" id="Seg_14983" s="T709">Then: “Someone is knocking, they are calling me."</ta>
            <ta e="T717" id="Seg_14984" s="T715">“Well, go!"</ta>
            <ta e="T720" id="Seg_14985" s="T717">Then she ate.</ta>
            <ta e="T722" id="Seg_14986" s="T720">She came.</ta>
            <ta e="T724" id="Seg_14987" s="T722">“Who [was] here?"</ta>
            <ta e="T727" id="Seg_14988" s="T724">“He, Skryobushok came."</ta>
            <ta e="T732" id="Seg_14989" s="T727">Then in the morning they got up, [the fox] is ill.</ta>
            <ta e="T736" id="Seg_14990" s="T732">She says: "Bring me [something] sweet."</ta>
            <ta e="T738" id="Seg_14991" s="T736">He went.</ta>
            <ta e="T742" id="Seg_14992" s="T738">And it's not there, someone ate it up.</ta>
            <ta e="T745" id="Seg_14993" s="T742">"You ate [it]."</ta>
            <ta e="T751" id="Seg_14994" s="T745">She [says]: “No, I did not eat!"</ta>
            <ta e="T761" id="Seg_14995" s="T751">Then: “Let’s lie down in the sun, who ate [it] will get a sweet stomach."</ta>
            <ta e="T765" id="Seg_14996" s="T761">Then she looks at her own stomach.</ta>
            <ta e="T767" id="Seg_14997" s="T765">It is sweet.</ta>
            <ta e="T772" id="Seg_14998" s="T767">He starts to smear it on the wolf.</ta>
            <ta e="T776" id="Seg_14999" s="T772">"Get up, you ate [it]!"</ta>
            <ta e="T782" id="Seg_15000" s="T776">Then he got up: “Well, I ate".</ta>
            <ta e="T784" id="Seg_15001" s="T782">That's all.</ta>
            <ta e="T788" id="Seg_15002" s="T784">There lived a small songbird and a grouse live.</ta>
            <ta e="T791" id="Seg_15003" s="T788">How to build a house?</ta>
            <ta e="T796" id="Seg_15004" s="T791">There is no axe, who will make an axe?</ta>
            <ta e="T801" id="Seg_15005" s="T796">Well, what [for] a house for me?</ta>
            <ta e="T804" id="Seg_15006" s="T801">I spend the night in the snow.</ta>
            <ta e="T808" id="Seg_15007" s="T804">And he jumped into the snow.</ta>
            <ta e="T812" id="Seg_15008" s="T808">There it spent the night, in the morning it got up.</ta>
            <ta e="T819" id="Seg_15009" s="T812">And it went to see, saw his companions, they played there.</ta>
            <ta e="T822" id="Seg_15010" s="T819">They jumped onto trees. [?]</ta>
            <ta e="T826" id="Seg_15011" s="T822">Then they made nests for themselves.</ta>
            <ta e="T828" id="Seg_15012" s="T826">They laid eggs.</ta>
            <ta e="T832" id="Seg_15013" s="T828">Children, then they became children.</ta>
            <ta e="T835" id="Seg_15014" s="T832">They fed them with flies and worms.</ta>
            <ta e="T840" id="Seg_15015" s="T835">Then again: "Why a house for us?</ta>
            <ta e="T842" id="Seg_15016" s="T840">We slept in the snow.</ta>
            <ta e="T847" id="Seg_15017" s="T842">One night we'll stay in the house.</ta>
            <ta e="T850" id="Seg_15018" s="T847">We are looking everywhere.</ta>
            <ta e="T851" id="Seg_15019" s="T850">Stop [the tape].</ta>
            <ta e="T855" id="Seg_15020" s="T851">There lived a woman and a man.</ta>
            <ta e="T859" id="Seg_15021" s="T855">They did not have anything.</ta>
            <ta e="T861" id="Seg_15022" s="T859">They went to the taiga.</ta>
            <ta e="T864" id="Seg_15023" s="T861">They collected pine cones, acorns.</ta>
            <ta e="T867" id="Seg_15024" s="T864">They came, they are eating.</ta>
            <ta e="T870" id="Seg_15025" s="T867">One [acorn] fell in the cellar.</ta>
            <ta e="T873" id="Seg_15026" s="T870">Then [it] started to grow.</ta>
            <ta e="T876" id="Seg_15027" s="T873">The woman says:</ta>
            <ta e="T879" id="Seg_15028" s="T876">“Cut the floor with the axe."</ta>
            <ta e="T881" id="Seg_15029" s="T879">He cut [it].</ta>
            <ta e="T884" id="Seg_15030" s="T881">It grew here.</ta>
            <ta e="T893" id="Seg_15031" s="T884">Now it (appeared) on the top, [he] took off the roof there.</ta>
            <ta e="T898" id="Seg_15032" s="T893">There it grew far, even above the top.</ta>
            <ta e="T900" id="Seg_15033" s="T898">Then the husband:</ta>
            <ta e="T904" id="Seg_15034" s="T900">"[We] should go and collect acorns."</ta>
            <ta e="T911" id="Seg_15035" s="T904">He climbed, he climbed, then at the top there he looks.</ta>
            <ta e="T914" id="Seg_15036" s="T911">A house is standing.</ta>
            <ta e="T917" id="Seg_15037" s="T914">There into the house he came.</ta>
            <ta e="T921" id="Seg_15038" s="T917">A cock lives there.</ta>
            <ta e="T923" id="Seg_15039" s="T921">His head is beautiful.</ta>
            <ta e="T925" id="Seg_15040" s="T923">All red.</ta>
            <ta e="T936" id="Seg_15041" s="T925">And he has his millstones, he took it and came: "Here, [it is] for you".</ta>
            <ta e="T940" id="Seg_15042" s="T936">The cock, red head.</ta>
            <ta e="T948" id="Seg_15043" s="T940">He took and started to twist the mill.</ta>
            <ta e="T958" id="Seg_15044" s="T948">There [come] pancakes, pies, pancakes, pies, the man ate them and the woman ate.</ta>
            <ta e="T965" id="Seg_15045" s="T958">Living like this always (/long time), then a nobleman came to them.</ta>
            <ta e="T968" id="Seg_15046" s="T965">"Bring me [something] to eat!"</ta>
            <ta e="T976" id="Seg_15047" s="T968">He had, of course, heard (of?) the mill.</ta>
            <ta e="T984" id="Seg_15048" s="T976">Then the woman let the mill twist (out) pancakes and pies.</ta>
            <ta e="T986" id="Seg_15049" s="T984">He ate.</ta>
            <ta e="T988" id="Seg_15050" s="T986">"Sell [it] to me."</ta>
            <ta e="T992" id="Seg_15051" s="T988">"No, we will not sell [it].</ta>
            <ta e="T997" id="Seg_15052" s="T992">Otherwise what [is] there for us to eat?"</ta>
            <ta e="T1001" id="Seg_15053" s="T997">Then he came in the evening.</ta>
            <ta e="T1004" id="Seg_15054" s="T1001">And stole it.</ta>
            <ta e="T1006" id="Seg_15055" s="T1004">And took it away.</ta>
            <ta e="T1011" id="Seg_15056" s="T1006">In the morning they got up: the mill is gone.</ta>
            <ta e="T1014" id="Seg_15057" s="T1011">Then the cock [said]:.</ta>
            <ta e="T1016" id="Seg_15058" s="T1014">"Don't cry!</ta>
            <ta e="T1018" id="Seg_15059" s="T1016">I will go.</ta>
            <ta e="T1022" id="Seg_15060" s="T1018">I will bring it [back] to you again."</ta>
            <ta e="T1023" id="Seg_15061" s="T1022">He went.</ta>
            <ta e="T1028" id="Seg_15062" s="T1023">He is going, he is going, a fox comes to him.</ta>
            <ta e="T1030" id="Seg_15063" s="T1028">"Where are you going?</ta>
            <ta e="T1032" id="Seg_15064" s="T1030">Take me [with you]!</ta>
            <ta e="T1037" id="Seg_15065" s="T1032">Come(?) in my bosom". He (?).</ta>
            <ta e="T1040" id="Seg_15066" s="T1037">Then he is going, he is going.</ta>
            <ta e="T1041" id="Seg_15067" s="T1040">To the wolf.</ta>
            <ta e="T1043" id="Seg_15068" s="T1041">Where are you going?</ta>
            <ta e="T1045" id="Seg_15069" s="T1043">To take [back] the mill.</ta>
            <ta e="T1048" id="Seg_15070" s="T1045">Take me [with you]!</ta>
            <ta e="T1051" id="Seg_15071" s="T1048">Come(?) in my bosom!"</ta>
            <ta e="T1053" id="Seg_15072" s="T1051">He crawled (in?).</ta>
            <ta e="T1057" id="Seg_15073" s="T1053">Then he goes, a bear comes.</ta>
            <ta e="T1059" id="Seg_15074" s="T1057">"(?) to me!"</ta>
            <ta e="T1061" id="Seg_15075" s="T1059">Then he arrived [to the nobleman].</ta>
            <ta e="T1063" id="Seg_15076" s="T1061">He started to yell.</ta>
            <ta e="T1064" id="Seg_15077" s="T1063">"Give it back!</ta>
            <ta e="T1067" id="Seg_15078" s="T1064">Bring my mill!</ta>
            <ta e="T1070" id="Seg_15079" s="T1067">Bring my mill!"</ta>
            <ta e="T1072" id="Seg_15080" s="T1070">[The nobleman] says:</ta>
            <ta e="T1075" id="Seg_15081" s="T1072">"Throw him (out)!</ta>
            <ta e="T1078" id="Seg_15082" s="T1075">Let him [go] to the ducks.</ta>
            <ta e="T1081" id="Seg_15083" s="T1078">Let the ducks eat [his] bones. [?]</ta>
            <ta e="T1086" id="Seg_15084" s="T1081">They threw him, but he (says?).</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PKZ">
            <ta e="T3" id="Seg_15085" s="T0">Hast du schon [das Band] gestartet?</ta>
            <ta e="T10" id="Seg_15086" s="T4">Es lebte ein Mann, er hatte drei Söhne.</ta>
            <ta e="T17" id="Seg_15087" s="T10">Einer war Gavrila, einer Danila, einer war der dumme Vanyushka.</ta>
            <ta e="T23" id="Seg_15088" s="T17">Dann säte dieser Mann Weizen.</ta>
            <ta e="T33" id="Seg_15089" s="T23">Und es kommt jemand am Abend und isst den Weizen.</ta>
            <ta e="T41" id="Seg_15090" s="T33">[Wir] sollten, sagte er, gehen und schauen, wer den Weizen isst.</ta>
            <ta e="T43" id="Seg_15091" s="T41">[Wir] sollten [sie] fangen.</ta>
            <ta e="T49" id="Seg_15092" s="T43">Dann ging der ältere Sohn Gavrila dort hinaus.</ta>
            <ta e="T55" id="Seg_15093" s="T49">Er ging, er schlief im Gras, und kam nach Hause.</ta>
            <ta e="T62" id="Seg_15094" s="T55">„Ich saß, ich schlief nicht, aber ich habe niemanden gesehen.“</ta>
            <ta e="T67" id="Seg_15095" s="T62">Dann geht der andere Sohn, sein Danila.</ta>
            <ta e="T70" id="Seg_15096" s="T67">Er ging, er schlief im Gras.</ta>
            <ta e="T73" id="Seg_15097" s="T70">Dann am Morgen kam er.</ta>
            <ta e="T81" id="Seg_15098" s="T73">„Die ganze Nacht schlief ich nicht, aber ich habe niemanden gesehen.“</ta>
            <ta e="T84" id="Seg_15099" s="T81">Dann ging Vanyushka.</ta>
            <ta e="T86" id="Seg_15100" s="T84">Er nahm sein Lasso.</ta>
            <ta e="T89" id="Seg_15101" s="T86">Dann setzte er sich auf einen Felsen.</ta>
            <ta e="T92" id="Seg_15102" s="T89">Er schläft nicht, er sitzt.</ta>
            <ta e="T95" id="Seg_15103" s="T92">Er sieht: es kam ein Pferd.</ta>
            <ta e="T102" id="Seg_15104" s="T95">Auf seiner Haut, ist das Fell wunderschön.</ta>
            <ta e="T109" id="Seg_15105" s="T102">Er warf sein Lasso drauf und fing [es].</ta>
            <ta e="T114" id="Seg_15106" s="T109">Es sprang, konnte nicht [weg]springen.</ta>
            <ta e="T117" id="Seg_15107" s="T114">Dann [sagte es]: „Lass mich gehen!</ta>
            <ta e="T122" id="Seg_15108" s="T117">Ich werde dir alles geben.“</ta>
            <ta e="T129" id="Seg_15109" s="T122">„Also, wie, erzähl [mir], wirst du kein Weizen mehr essen?“</ta>
            <ta e="T131" id="Seg_15110" s="T129">Ich werde [es] nicht essen!“</ta>
            <ta e="T136" id="Seg_15111" s="T131">Aber wie wirst du so zu mir kommen?</ta>
            <ta e="T141" id="Seg_15112" s="T136">Her kommen, (?) ich.</ta>
            <ta e="T143" id="Seg_15113" s="T141">Also, so.</ta>
            <ta e="T151" id="Seg_15114" s="T143">Und das Pferd ging, und er kam nach Hause.</ta>
            <ta e="T153" id="Seg_15115" s="T151">Er stieg auf den Ofen.</ta>
            <ta e="T157" id="Seg_15116" s="T153">Ich fing dort ein Pferd.</ta>
            <ta e="T163" id="Seg_15117" s="T157">Nun wird es nie wiederkommen, um Weizen zu fressen.</ta>
            <ta e="T169" id="Seg_15118" s="T163">Dann machte (/rufte) der Zar damit Leute kamen.</ta>
            <ta e="T174" id="Seg_15119" s="T169">Seine Tochter lebt in einem Turm.</ta>
            <ta e="T177" id="Seg_15120" s="T174">Alle Leute gehen.</ta>
            <ta e="T182" id="Seg_15121" s="T177">Und seine Brüder gehen auch.</ta>
            <ta e="T187" id="Seg_15122" s="T182">Aber er sagt: „Nimm mich!“</ta>
            <ta e="T190" id="Seg_15123" s="T187">„Wohin wirst du gehen?“</ta>
            <ta e="T199" id="Seg_15124" s="T190">Du würdest dort nichts sehen, so dass Leute über dich lachen würden? </ta>
            <ta e="T204" id="Seg_15125" s="T199">Sie gingen, er stand auf.</ta>
            <ta e="T207" id="Seg_15126" s="T204">Er nahm einen Korb, er ging.</ta>
            <ta e="T212" id="Seg_15127" s="T207">Er fing an zu rufen, sein Pferd kam.</ta>
            <ta e="T216" id="Seg_15128" s="T212">Sie gingen dort hinein (?).</ta>
            <ta e="T219" id="Seg_15129" s="T216">Er [Vanyushka?] kommt zu diesem Mann [zu dem Zaren?].</ta>
            <ta e="T224" id="Seg_15130" s="T219">Dann sagt das Pferd …</ta>
            <ta e="T226" id="Seg_15131" s="T224">Es stand an seiner (Seite?).</ta>
            <ta e="T232" id="Seg_15132" s="T226">Es sagt: schlüpfe in ein Ohr hinein, komm aus einem (dem anderen) heraus.</ta>
            <ta e="T235" id="Seg_15133" s="T232">Er schlüpfte hinein, kam heraus.</ta>
            <ta e="T237" id="Seg_15134" s="T235">Er wurde wunderschön.</ta>
            <ta e="T240" id="Seg_15135" s="T237">Dann setzte er sich drauf.</ta>
            <ta e="T243" id="Seg_15136" s="T240">Es rannte.</ta>
            <ta e="T254" id="Seg_15137" s="T243">Es standen viele Leute, aber keiner, klettert nicht zur Tochter hinauf.</ta>
            <ta e="T257" id="Seg_15138" s="T254">Er sprang.</ta>
            <ta e="T261" id="Seg_15139" s="T257">Sie verstecken sich, sie fingen [ihn?] nicht, nicht. </ta>
            <ta e="T267" id="Seg_15140" s="T261">Dann rufen die Leute: „Fang [ihn], fang [ihn]“.</ta>
            <ta e="T270" id="Seg_15141" s="T267">Aber er lief [weg].</ta>
            <ta e="T272" id="Seg_15142" s="T270">Er schickte sein Pferd fort.</ta>
            <ta e="T280" id="Seg_15143" s="T272">Dann sammelte er Pilze und holte was es auch sonst gab.</ta>
            <ta e="T283" id="Seg_15144" s="T280">Aber die Frauen schimpfen:</ta>
            <ta e="T287" id="Seg_15145" s="T283">„Warum hast du solche [Pilze] gebracht?</ta>
            <ta e="T290" id="Seg_15146" s="T287">Nur für dich zu essen.“</ta>
            <ta e="T295" id="Seg_15147" s="T290">Er stieg auf den Ofen, liegt dort.</ta>
            <ta e="T304" id="Seg_15148" s="T295">Dann kamen seine Brüder, sie erzählen, wie es [dort] war.</ta>
            <ta e="T307" id="Seg_15149" s="T304">Aber er liegt.</ta>
            <ta e="T312" id="Seg_15150" s="T307">„Ich (/mein Kopf) war's dort“.</ta>
            <ta e="T314" id="Seg_15151" s="T312">Lüge nicht!</ta>
            <ta e="T316" id="Seg_15152" s="T314">Dein Kopf.</ta>
            <ta e="T318" id="Seg_15153" s="T316">Setz dich bitte.</ta>
            <ta e="T321" id="Seg_15154" s="T318">Erzähle nichts.</ta>
            <ta e="T328" id="Seg_15155" s="T321">Solch ein Mann war [er], sein eigenes Pferd war wunderschön.</ta>
            <ta e="T332" id="Seg_15156" s="T328">Dann am nächsten Tag gehen sie wieder [dorthin].</ta>
            <ta e="T334" id="Seg_15157" s="T332">Sie gingen.</ta>
            <ta e="T338" id="Seg_15158" s="T334">Er sprang vom Ofen.</ta>
            <ta e="T340" id="Seg_15159" s="T338">Er nahm einen Korb.</ta>
            <ta e="T344" id="Seg_15160" s="T340">Er ging, er ging, warf es [den Korb?] weg.</ta>
            <ta e="T347" id="Seg_15161" s="T344">Er fing an, das Pferd zu rufen.</ta>
            <ta e="T349" id="Seg_15162" s="T347">Sein Pferd kam.</ta>
            <ta e="T355" id="Seg_15163" s="T349">Wiederum schlüpfte er in ein Ohr hinein und kam aus dem anderen heraus.</ta>
            <ta e="T357" id="Seg_15164" s="T355">Dann setzte er sich.</ta>
            <ta e="T362" id="Seg_15165" s="T357">Leute kommen, viele stehen.</ta>
            <ta e="T367" id="Seg_15166" s="T362">Dann lief niemand dorthin.</ta>
            <ta e="T370" id="Seg_15167" s="T367">Er sprang dorthin.</ta>
            <ta e="T377" id="Seg_15168" s="T370">Er nahm … er konnte nicht. [?]</ta>
            <ta e="T379" id="Seg_15169" s="T377">Dann kam er zurück.</ta>
            <ta e="T385" id="Seg_15170" s="T379">Er schlug seine Brüder [mit einer Peitsche?] und ging.</ta>
            <ta e="T388" id="Seg_15171" s="T385">Aber die Leute rufen.</ta>
            <ta e="T394" id="Seg_15172" s="T388">„Fang [ihn], fang [ihn]“, und er lief weg.</ta>
            <ta e="T400" id="Seg_15173" s="T394">Er schickte sein Pferd weg, aber er selber kam wieder [nach Hause].</ta>
            <ta e="T403" id="Seg_15174" s="T400">Die Frauen schimpfen wieder.</ta>
            <ta e="T409" id="Seg_15175" s="T403">Aber er stieg auf den Ofen und liegt dort.</ta>
            <ta e="T412" id="Seg_15176" s="T409">Seine Brüder kamen.</ta>
            <ta e="T414" id="Seg_15177" s="T412">Sie erzählen seinem Vater.</ta>
            <ta e="T427" id="Seg_15178" s="T414">„Was für ein Mann war das, sein Pferd [war] sehr schön, und selbst [war] schön“, und er lag.</ta>
            <ta e="T433" id="Seg_15179" s="T427">„War es nicht meinen Kopf dort.“</ta>
            <ta e="T437" id="Seg_15180" s="T433">„Erzähle keine Lügen, leg dich hin!</ta>
            <ta e="T439" id="Seg_15181" s="T437">Verrückter Junge!</ta>
            <ta e="T442" id="Seg_15182" s="T439">[Als ob] du da warst!“</ta>
            <ta e="T448" id="Seg_15183" s="T442">Dann wiederum, [am] dritten Tag gehen sie wieder.</ta>
            <ta e="T453" id="Seg_15184" s="T448">Sie gingen, er stand auf.</ta>
            <ta e="T456" id="Seg_15185" s="T453">Er nahm seinen Korb und ging.</ta>
            <ta e="T458" id="Seg_15186" s="T456">Um Pilze zu sammeln.</ta>
            <ta e="T462" id="Seg_15187" s="T458">Er ging, fing an das Pferd zu rufen.</ta>
            <ta e="T464" id="Seg_15188" s="T462">Das Pferd kam.</ta>
            <ta e="T475" id="Seg_15189" s="T464">Er schlüpfte wieder in ein Ohr ein, kam aus einem [dem anderen] heraus, saß [auf dem Pferd], ging.</ta>
            <ta e="T478" id="Seg_15190" s="T475">Dann galoppierte er fort.</ta>
            <ta e="T487" id="Seg_15191" s="T478">Er küsste sie und nahm ihren Ring, er trug ihn auf seine Hand. </ta>
            <ta e="T489" id="Seg_15192" s="T487">Er ging.</ta>
            <ta e="T498" id="Seg_15193" s="T489">Er schickte sein Pferd weg, er selbst ging nach Hause, stieg auf den Ofen, er liegt dort.</ta>
            <ta e="T501" id="Seg_15194" s="T498">Dann kamen seine Brüder.</ta>
            <ta e="T508" id="Seg_15195" s="T501">"Also, welch schöner Junge, sein Pferd [war] schön.</ta>
            <ta e="T511" id="Seg_15196" s="T508">Er küsste die Zarentochter.</ta>
            <ta e="T514" id="Seg_15197" s="T511">Die Tochter des Zaren.</ta>
            <ta e="T519" id="Seg_15198" s="T514">Und er nahm ihren Ring, er ging."</ta>
            <ta e="T524" id="Seg_15199" s="T519">Dann machte der Zar wieder [so].</ta>
            <ta e="T533" id="Seg_15200" s="T524">Er stellte eine Menge Fleisch, Eier, er stellte alles heraus, dass es gibt.</ta>
            <ta e="T542" id="Seg_15201" s="T533">Dann: „Komm von überall, ich werde denen, die nicht kommen, die Köpfe abschlagen“.</ta>
            <ta e="T552" id="Seg_15202" s="T542">Dann sie, sein Vater, von überall, gingen und sie kamen.</ta>
            <ta e="T559" id="Seg_15203" s="T552">Alle wurden hingesetzt, gefüttert, Vodka gereicht.</ta>
            <ta e="T565" id="Seg_15204" s="T559">Dann kam der letzte Sohn, er [Vanyushka].</ta>
            <ta e="T574" id="Seg_15205" s="T565">Er sitzt schwarz (/schmutzig), seine Hände schmutzig, in einem Lappen verbunden.</ta>
            <ta e="T578" id="Seg_15206" s="T574">[Die Zartochter] sagt: "Was ist in deiner Hand?"</ta>
            <ta e="T585" id="Seg_15207" s="T578">Sie sieht: ihr Ring (sitzt) [dort].</ta>
            <ta e="T589" id="Seg_15208" s="T585">Dann nimmt sie ihn und geht zu ihrem Vater.</ta>
            <ta e="T594" id="Seg_15209" s="T589">"Das, das ist mein Bräutigam."</ta>
            <ta e="T600" id="Seg_15210" s="T594">Sie wuschen ihn dort.</ta>
            <ta e="T604" id="Seg_15211" s="T600">Sie kleideten ihn in schöne Kleider.</ta>
            <ta e="T607" id="Seg_15212" s="T604">Er wurde sehr schön.</ta>
            <ta e="T612" id="Seg_15213" s="T607">Dann fingen sie wieder an, Vodka zu trinken.</ta>
            <ta e="T613" id="Seg_15214" s="T612">Zu spielen.</ta>
            <ta e="T616" id="Seg_15215" s="T613">Seine Brüder schauen.</ta>
            <ta e="T619" id="Seg_15216" s="T616">Wie schön ist er geworden!</ta>
            <ta e="T623" id="Seg_15217" s="T619">Und ich war auch dabei!</ta>
            <ta e="T626" id="Seg_15218" s="T623">Der Vodka fließt.</ta>
            <ta e="T629" id="Seg_15219" s="T626">Sie gaben mir Pfannkuchen.</ta>
            <ta e="T633" id="Seg_15220" s="T629">Drei Jahre an der Schüssel (= trinken).</ta>
            <ta e="T638" id="Seg_15221" s="T633">Zusammen lebten ein Bursche und ein Mädel (= ein Ehepaar).</ta>
            <ta e="T641" id="Seg_15222" s="T638">Wolf und Fuchs.</ta>
            <ta e="T644" id="Seg_15223" s="T641">Dann legen sie sich zum Schlafen hin.</ta>
            <ta e="T649" id="Seg_15224" s="T644">Doch der Fuchs klopft ständig, klopft.</ta>
            <ta e="T659" id="Seg_15225" s="T649">Doch der Wolf sagt: „Es klopft jemand.“ — „Sie rufen mich, das Kind zu füttern.“</ta>
            <ta e="T662" id="Seg_15226" s="T659">Dann [der Wolf sagt]: „Also, geh!“</ta>
            <ta e="T664" id="Seg_15227" s="T662">Sie [die Fuchsin] ging.</ta>
            <ta e="T671" id="Seg_15228" s="T664">Und sie (standen?), [die Fuchsin] aßen den Honig, aßen den Honig.</ta>
            <ta e="T673" id="Seg_15229" s="T671">Sie kam.</ta>
            <ta e="T676" id="Seg_15230" s="T673">Wer ist da?</ta>
            <ta e="T678" id="Seg_15231" s="T676">Es ist Wershok (/Oberteil).</ta>
            <ta e="T684" id="Seg_15232" s="T678">Dann wieder am Abend, wieder klopft der Fuchs.</ta>
            <ta e="T686" id="Seg_15233" s="T684">„Wer klopft?“</ta>
            <ta e="T691" id="Seg_15234" s="T686">„Sie rufen mich, um das Kind (auszuwickeln?).“</ta>
            <ta e="T693" id="Seg_15235" s="T691">„Also, geh!“</ta>
            <ta e="T695" id="Seg_15236" s="T693">Sie ging.</ta>
            <ta e="T702" id="Seg_15237" s="T695">Sie aß, aß, dann kam sie: „Wer ist da gekommen?“</ta>
            <ta e="T704" id="Seg_15238" s="T702">„Serjodushok ist gekommen.“</ta>
            <ta e="T709" id="Seg_15239" s="T704">Dann am dritten Abend klopft er schon wieder.</ta>
            <ta e="T715" id="Seg_15240" s="T709">Dann: „Jemand klopft, sie rufen mich.“</ta>
            <ta e="T717" id="Seg_15241" s="T715">„Also, geh!“</ta>
            <ta e="T720" id="Seg_15242" s="T717">Dann aß sie.</ta>
            <ta e="T722" id="Seg_15243" s="T720">Sie kam.</ta>
            <ta e="T724" id="Seg_15244" s="T722">„Wer [war] da?“</ta>
            <ta e="T727" id="Seg_15245" s="T724">„Er, Skryobushok kam.“</ta>
            <ta e="T732" id="Seg_15246" s="T727">Dann am Morgen standen sie auf, [die Fuchsin] ist krank.</ta>
            <ta e="T736" id="Seg_15247" s="T732">Sie sagt: „Hol mir [etwas] Süßes.“</ta>
            <ta e="T738" id="Seg_15248" s="T736">Er ging.</ta>
            <ta e="T742" id="Seg_15249" s="T738">Und es ist nicht da, jemand hat es aufgegessen.</ta>
            <ta e="T745" id="Seg_15250" s="T742">„Du hast [es] gegessen!“</ta>
            <ta e="T751" id="Seg_15251" s="T745">Sie [sagt]: „Nein, ich habe nicht gegessen!“</ta>
            <ta e="T761" id="Seg_15252" s="T751">Dann: „Legen wir uns in die Sonne, wer [es] gegessen hat bekommt einen süßen Bauch.“</ta>
            <ta e="T765" id="Seg_15253" s="T761">Dann schaut sie sich ihren eigenen Bauch an.</ta>
            <ta e="T767" id="Seg_15254" s="T765">Er ist süß.</ta>
            <ta e="T772" id="Seg_15255" s="T767">Er fängt an, es an den Wolf zu schmieren.</ta>
            <ta e="T776" id="Seg_15256" s="T772">„Steh auf, du hast [es] gegessen!“</ta>
            <ta e="T782" id="Seg_15257" s="T776">Dann stand er auf: „Also, ich habe gegessen“.</ta>
            <ta e="T784" id="Seg_15258" s="T782">Das war’s.</ta>
            <ta e="T788" id="Seg_15259" s="T784">Es leben ein kleiner Singvogel und ein Rebhuhn.</ta>
            <ta e="T791" id="Seg_15260" s="T788">Wie ein Haus bauen?</ta>
            <ta e="T796" id="Seg_15261" s="T791">Es ist keine Axt, wer wird eine Axt bauen?</ta>
            <ta e="T801" id="Seg_15262" s="T796">Also, warum ein Haus für mich?</ta>
            <ta e="T804" id="Seg_15263" s="T801">Ich verbringe die Nacht im Schnee.</ta>
            <ta e="T808" id="Seg_15264" s="T804">Und er sprang in den Schnee.</ta>
            <ta e="T812" id="Seg_15265" s="T808">Da verbrachte es die Nacht, am Morgen stand es auf.</ta>
            <ta e="T819" id="Seg_15266" s="T812">Und es ging nachschauen, sah seine Grfährten, sie spielten dort.</ta>
            <ta e="T822" id="Seg_15267" s="T819">Sie sprangen in Bäumen. [?]</ta>
            <ta e="T826" id="Seg_15268" s="T822">Dann bauten sie sich Nester.</ta>
            <ta e="T828" id="Seg_15269" s="T826">Sie legten Eier.</ta>
            <ta e="T832" id="Seg_15270" s="T828">Kinder, dann bekamen sie Kinder.</ta>
            <ta e="T835" id="Seg_15271" s="T832">Sie fütterte ihnen Fliegen und Würmer.</ta>
            <ta e="T840" id="Seg_15272" s="T835">Dann wieder: "Warum ein Haus für uns?</ta>
            <ta e="T842" id="Seg_15273" s="T840">Wir schliefen im Schnee.</ta>
            <ta e="T847" id="Seg_15274" s="T842">Eine Nacht werden wir im Haus bleiben.</ta>
            <ta e="T850" id="Seg_15275" s="T847">Wir suchen überall.</ta>
            <ta e="T851" id="Seg_15276" s="T850">Stopp [das Band].</ta>
            <ta e="T855" id="Seg_15277" s="T851">Eine Frau und ein Mann lebten.</ta>
            <ta e="T859" id="Seg_15278" s="T855">Sie hatten nichts.</ta>
            <ta e="T861" id="Seg_15279" s="T859">Sie gingen in die Taiga.</ta>
            <ta e="T864" id="Seg_15280" s="T861">Sie sammelten Tannenzäpfchen, Eichel.</ta>
            <ta e="T867" id="Seg_15281" s="T864">Sie kamen, sie essen.</ta>
            <ta e="T870" id="Seg_15282" s="T867">Eine [Eichel] fiel in den Keller.</ta>
            <ta e="T873" id="Seg_15283" s="T870">Dann fing [es] an zu wachsen.</ta>
            <ta e="T876" id="Seg_15284" s="T873">Die Frau sagt:</ta>
            <ta e="T879" id="Seg_15285" s="T876">„Breche den Boden mit einer Axt.“</ta>
            <ta e="T881" id="Seg_15286" s="T879">Er brach [ihn].</ta>
            <ta e="T884" id="Seg_15287" s="T881">Er wuchs dort.</ta>
            <ta e="T893" id="Seg_15288" s="T884">Nun (erschien) er oben, [er] nahm das Dach ab.</ta>
            <ta e="T898" id="Seg_15289" s="T893">Dort wuchs er hoch, höher als die Spitze.</ta>
            <ta e="T900" id="Seg_15290" s="T898">Dann, der Ehemann:</ta>
            <ta e="T904" id="Seg_15291" s="T900">„[Wir] sollten Eichel sammeln gehen.“</ta>
            <ta e="T911" id="Seg_15292" s="T904">Er kletterte, er kletterte, dann dort oben schaut er.</ta>
            <ta e="T914" id="Seg_15293" s="T911">Ein Haus steht.</ta>
            <ta e="T917" id="Seg_15294" s="T914">Dort ins Haus kam er.</ta>
            <ta e="T921" id="Seg_15295" s="T917">Ein Hahn lebt dort.</ta>
            <ta e="T923" id="Seg_15296" s="T921">Sein Kopf ist wunderschön.</ta>
            <ta e="T925" id="Seg_15297" s="T923">Ganz rot.</ta>
            <ta e="T936" id="Seg_15298" s="T925">Und, er hat seine Mahlsteine, er nahm ihn und kam: „Hier, [das ist] für dich“.</ta>
            <ta e="T940" id="Seg_15299" s="T936">Der Hahn, Rotkopf.</ta>
            <ta e="T948" id="Seg_15300" s="T940">Er nahm, und fing an die Mühle zu drehen.</ta>
            <ta e="T958" id="Seg_15301" s="T948">Es [kommen] Pfannkuchen, Pasteten, Pfannkuchen, Pasteten, der Mann aß, die Frau aß.</ta>
            <ta e="T965" id="Seg_15302" s="T958">So leben sie immer (/langer Zeit), dann kam ein Adliger zu ihnen.</ta>
            <ta e="T968" id="Seg_15303" s="T965">„Bringt mir [etwas] zu essen!“</ta>
            <ta e="T976" id="Seg_15304" s="T968">Er hatte, natürlich, (von?) der Mühle gehört.</ta>
            <ta e="T984" id="Seg_15305" s="T976">Dann ließ die Frau die Mühle Pfannkuchen und Pasteten (heraus) drehen.</ta>
            <ta e="T986" id="Seg_15306" s="T984">Er aß.</ta>
            <ta e="T988" id="Seg_15307" s="T986">„Verkauf [sie] mir.“</ta>
            <ta e="T992" id="Seg_15308" s="T988">„Nein, wir verkaufen[sie] nicht.</ta>
            <ta e="T997" id="Seg_15309" s="T992">Was [ist] sonst für uns zu essen da?“</ta>
            <ta e="T1001" id="Seg_15310" s="T997">Dann kam er abends.</ta>
            <ta e="T1004" id="Seg_15311" s="T1001">Und stahl sie.</ta>
            <ta e="T1006" id="Seg_15312" s="T1004">Und nahm sie weg.</ta>
            <ta e="T1011" id="Seg_15313" s="T1006">Am Morgen standen sie auf: die Mühle ist weg.</ta>
            <ta e="T1014" id="Seg_15314" s="T1011">Dann [sagte] der Hahn:</ta>
            <ta e="T1016" id="Seg_15315" s="T1014">„Weint nicht!</ta>
            <ta e="T1018" id="Seg_15316" s="T1016">Ich werde gehen.</ta>
            <ta e="T1022" id="Seg_15317" s="T1018">Ich hohl sie euch wieder [zurück].“</ta>
            <ta e="T1023" id="Seg_15318" s="T1022">Er ging.</ta>
            <ta e="T1028" id="Seg_15319" s="T1023">Er geht, er geht, ein Fuchs kommt zu ihm.</ta>
            <ta e="T1030" id="Seg_15320" s="T1028">„Wohin gehst du?</ta>
            <ta e="T1032" id="Seg_15321" s="T1030">Nimm mich [mit dir]!</ta>
            <ta e="T1037" id="Seg_15322" s="T1032">Komm(?) in meinen Busen.“ Er (?).</ta>
            <ta e="T1040" id="Seg_15323" s="T1037">Dann geht er, er geht.</ta>
            <ta e="T1041" id="Seg_15324" s="T1040">Zum Wolf.</ta>
            <ta e="T1043" id="Seg_15325" s="T1041">Wohin gehst du?</ta>
            <ta e="T1045" id="Seg_15326" s="T1043">Die Mühle [zurück] holen.</ta>
            <ta e="T1048" id="Seg_15327" s="T1045">Nimm mich [mit dir]!</ta>
            <ta e="T1051" id="Seg_15328" s="T1048">Komm(?) in meinen Busen!“</ta>
            <ta e="T1053" id="Seg_15329" s="T1051">Er (kroch?).</ta>
            <ta e="T1057" id="Seg_15330" s="T1053">Dann geht er, ein Bär kommt.</ta>
            <ta e="T1059" id="Seg_15331" s="T1057">„(?) zu mir!“</ta>
            <ta e="T1061" id="Seg_15332" s="T1059">Dann kam er an [zu dem Adligen].</ta>
            <ta e="T1063" id="Seg_15333" s="T1061">Er fing an, zu rufen.</ta>
            <ta e="T1064" id="Seg_15334" s="T1063">„Gib sie zurück!</ta>
            <ta e="T1067" id="Seg_15335" s="T1064">Hole meine Mühle!</ta>
            <ta e="T1070" id="Seg_15336" s="T1067">Hole meine Mühle!“</ta>
            <ta e="T1072" id="Seg_15337" s="T1070">[Der Adliger] sagt:</ta>
            <ta e="T1075" id="Seg_15338" s="T1072">"Wirf ihn (hinaus)!</ta>
            <ta e="T1078" id="Seg_15339" s="T1075">Lass ihn zu den Enten [gehen].</ta>
            <ta e="T1081" id="Seg_15340" s="T1078">Lass die Enten [sein] Knochen fressen. [?]</ta>
            <ta e="T1086" id="Seg_15341" s="T1081">Sie warfen ihn, aber er (sagt?).</ta>
         </annotation>
         <annotation name="nt" tierref="nt-PKZ">
            <ta e="T81" id="Seg_15342" s="T73">[GVY:] šinimdə must be a negligent pronounciation of šindi-m-də [who-ACC-INDEF]</ta>
            <ta e="T102" id="Seg_15343" s="T95">[GVY:] kubangən = kubagən [skin-LOC]. "i bar"unclear. </ta>
            <ta e="T141" id="Seg_15344" s="T136">Unclear.</ta>
            <ta e="T143" id="Seg_15345" s="T141">[GVY:] maybe "noʔ".</ta>
            <ta e="T153" id="Seg_15346" s="T151">[KlT:] šerluʔpi = s'aluʔpi</ta>
            <ta e="T199" id="Seg_15347" s="T190">[KlT:] Russian subjunctive model</ta>
            <ta e="T216" id="Seg_15348" s="T212">[KlT:] Unclear; Ru. мудрый 'wise'??</ta>
            <ta e="T232" id="Seg_15349" s="T226">Păʔkaʔ possibly 2PL, wrong number.</ta>
            <ta e="T280" id="Seg_15350" s="T272">[GVY:] Another possibility is "vsjaka iʔgö dăška deppi" 'brought many various other [things]".</ta>
            <ta e="T312" id="Seg_15351" s="T307">[GVY:] Evidently, this sentence means 'It was me there', but the construction is unclear. Note that below, the form "ulum" is repeated as it is, instead of the expected 2nd person.</ta>
            <ta e="T316" id="Seg_15352" s="T314">[AAV:] Sarcastic remark.</ta>
            <ta e="T355" id="Seg_15353" s="T349">[GVY:] slip of the tongue: kuʔ-tə 'snot-LAT' (or maybe kot-tə 'rib-LAT') instead of ku-nə 'ear-LAT'.</ta>
            <ta e="T370" id="Seg_15354" s="T367">[GVY:] suʔməluʔpi pronounced as suʔməluʔpie, -p'e.</ta>
            <ta e="T377" id="Seg_15355" s="T370">[KlT:] Unclear.</ta>
            <ta e="T394" id="Seg_15356" s="T388">[GVY:] The sentence is unclear; maybe it should be nuʔməluʔpi 'ran away'. Dʼabəʔ is pronounced d'abi.</ta>
            <ta e="T412" id="Seg_15357" s="T409">[KlT:] Kazaŋ instead of kagazaŋ.</ta>
            <ta e="T433" id="Seg_15358" s="T427">[GVY:] This may be a calque of a Russian construction ("Не моя ли голова там была"), but the meaning of the sentence is unclear. "Ulum" may also mean 'my feet'?</ta>
            <ta e="T458" id="Seg_15359" s="T456">[KlT:] oʔbšizittə: the expected form is oʔbdəzittə [gather-INF.LAT].</ta>
            <ta e="T487" id="Seg_15360" s="T478">Колечко 'little ring’ Ru.</ta>
            <ta e="T542" id="Seg_15361" s="T533">[GVY:] sajjeʔlim - Donner-Joki sajnʼeʔlem 'schneiden, abschneiden'.</ta>
            <ta e="T574" id="Seg_15362" s="T565">[KlT:] Тряпка Ru. 'rag'.</ta>
            <ta e="T578" id="Seg_15363" s="T574">[GVY:] udandə 3rd person possessive instead of 2nd</ta>
            <ta e="T585" id="Seg_15364" s="T578">[GVY:] the first fragment is rather heard as "kulʼot bəl".</ta>
            <ta e="T633" id="Seg_15365" s="T629">RUS лоханка 'washing bowl'.</ta>
            <ta e="T638" id="Seg_15366" s="T633">[KlT]: Кум, кума Ru. tricky fellow (m. and f.).</ta>
            <ta e="T678" id="Seg_15367" s="T676">[KlT:] Ru. верх 'up' вершок 'upper part'. The fox makes up a child's name to fool the wolf.</ta>
            <ta e="T695" id="Seg_15368" s="T693">[GVY:] dĭ pronounced as do (cf. above).</ta>
            <ta e="T704" id="Seg_15369" s="T702">[KlT:] Середина, серёдка 'middle part' - Another name the fox makes up (meaning it ate half of the honey).</ta>
            <ta e="T727" id="Seg_15370" s="T724">Скрести: скребу: скрёб Ru. 'to scratch' - the fox has scratched the last honey from the bottom and eaten it.</ta>
            <ta e="T761" id="Seg_15371" s="T751">[AAV:] i.e. the honey will transpire on the belly.</ta>
            <ta e="T765" id="Seg_15372" s="T761">[GVY:] kuliat pronounced as [kulʼot], cf. above.</ta>
            <ta e="T788" id="Seg_15373" s="T784">Тетерев 'grouse’ Ru. - alternative reading ’seje tʼetʼer’ capercaillie-grouse-bird.</ta>
            <ta e="T826" id="Seg_15374" s="T822">[GVY:] gnʼozdaʔi - Russian plural stem гнёзда (sg. гнездо ’nest’).</ta>
            <ta e="T832" id="Seg_15375" s="T828">[AAV:] Note LAT on "children".</ta>
            <ta e="T835" id="Seg_15376" s="T832">[GVY:] maškaziʔ: rather Ru. мошка́ coll. 'gnat', than мо́шка sg. 'blackfly'.</ta>
            <ta e="T847" id="Seg_15377" s="T842">[GVY:] =amno-la-baʔ [sit-PRS-1PL]?</ta>
            <ta e="T864" id="Seg_15378" s="T861">[GVY:] Ru. жёлуди pl. ’acorns’ (sg. жёлудь).</ta>
            <ta e="T867" id="Seg_15379" s="T864">[KlT:] amnaʔbə SG instead of PL.</ta>
            <ta e="T870" id="Seg_15380" s="T867">[KlT:] LOC inst. of LAT?</ta>
            <ta e="T936" id="Seg_15381" s="T925">[GVY:] žernovkazaŋdə or -kaʔazaŋdə; Russian жернов 'millstone', but the fragment ka(kaʔa) is unclear. It might be the Russian suffix -ка, but there seems to be no word "жерновка". </ta>
            <ta e="T948" id="Seg_15382" s="T940">Theoretical additional meaning of the verb ’kür-’ ’to twist’.</ta>
            <ta e="T992" id="Seg_15383" s="T988">1SG ending instead of 1PL.</ta>
            <ta e="T1037" id="Seg_15384" s="T1032">[GVY:] the verb is unknown (but cf. baʔluʔ- 'fort-, weggehen, verlassen, aufgeben', DJ: 8a)</ta>
            <ta e="T1067" id="Seg_15385" s="T1064">[GVY:] tʼerməndə should be t'ermənbə [mill-ACC.1SG]?</ta>
            <ta e="T1086" id="Seg_15386" s="T1081">[KlT:] Part of the tale is missing, SU0192 continues from a different episode.</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-KA"
                      id="tx-KA"
                      speaker="KA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KA">
            <ts e="T4" id="Seg_15387" n="sc" s="T3">
               <ts e="T4" id="Seg_15389" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_15391" n="HIAT:w" s="T3">Mhmh</ts>
                  <nts id="Seg_15392" n="HIAT:ip">.</nts>
                  <nts id="Seg_15393" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KA">
            <ts e="T4" id="Seg_15394" n="sc" s="T3">
               <ts e="T4" id="Seg_15396" n="e" s="T3">Mhmh. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KA">
            <ta e="T4" id="Seg_15397" s="T3">PKZ_196X_SU0191.KA.001 (002)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KA">
            <ta e="T4" id="Seg_15398" s="T3">Mhmh. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-KA">
            <ta e="T4" id="Seg_15399" s="T3">mhmh</ta>
         </annotation>
         <annotation name="mp" tierref="mp-KA">
            <ta e="T4" id="Seg_15400" s="T3">mhmh</ta>
         </annotation>
         <annotation name="ge" tierref="ge-KA">
            <ta e="T4" id="Seg_15401" s="T3">INTJ</ta>
         </annotation>
         <annotation name="gr" tierref="gr-KA">
            <ta e="T4" id="Seg_15402" s="T3">INTJ</ta>
         </annotation>
         <annotation name="mc" tierref="mc-KA">
            <ta e="T4" id="Seg_15403" s="T3">interj</ta>
         </annotation>
         <annotation name="ps" tierref="ps-KA">
            <ta e="T4" id="Seg_15404" s="T3">interj</ta>
         </annotation>
         <annotation name="CS" tierref="CS-KA" />
         <annotation name="fr" tierref="fr-KA" />
         <annotation name="fe" tierref="fe-KA" />
         <annotation name="fg" tierref="fg-KA" />
         <annotation name="nt" tierref="nt-KA" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1087" />
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T979" />
            <conversion-tli id="T980" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T985" />
            <conversion-tli id="T986" />
            <conversion-tli id="T987" />
            <conversion-tli id="T988" />
            <conversion-tli id="T989" />
            <conversion-tli id="T990" />
            <conversion-tli id="T991" />
            <conversion-tli id="T992" />
            <conversion-tli id="T993" />
            <conversion-tli id="T994" />
            <conversion-tli id="T995" />
            <conversion-tli id="T996" />
            <conversion-tli id="T997" />
            <conversion-tli id="T998" />
            <conversion-tli id="T999" />
            <conversion-tli id="T1000" />
            <conversion-tli id="T1001" />
            <conversion-tli id="T1002" />
            <conversion-tli id="T1003" />
            <conversion-tli id="T1004" />
            <conversion-tli id="T1005" />
            <conversion-tli id="T1006" />
            <conversion-tli id="T1007" />
            <conversion-tli id="T1008" />
            <conversion-tli id="T1009" />
            <conversion-tli id="T1010" />
            <conversion-tli id="T1011" />
            <conversion-tli id="T1012" />
            <conversion-tli id="T1013" />
            <conversion-tli id="T1014" />
            <conversion-tli id="T1015" />
            <conversion-tli id="T1016" />
            <conversion-tli id="T1017" />
            <conversion-tli id="T1018" />
            <conversion-tli id="T1019" />
            <conversion-tli id="T1020" />
            <conversion-tli id="T1021" />
            <conversion-tli id="T1022" />
            <conversion-tli id="T1023" />
            <conversion-tli id="T1024" />
            <conversion-tli id="T1025" />
            <conversion-tli id="T1026" />
            <conversion-tli id="T1027" />
            <conversion-tli id="T1028" />
            <conversion-tli id="T1029" />
            <conversion-tli id="T1030" />
            <conversion-tli id="T1031" />
            <conversion-tli id="T1032" />
            <conversion-tli id="T1033" />
            <conversion-tli id="T1034" />
            <conversion-tli id="T1035" />
            <conversion-tli id="T1036" />
            <conversion-tli id="T1037" />
            <conversion-tli id="T1038" />
            <conversion-tli id="T1039" />
            <conversion-tli id="T1040" />
            <conversion-tli id="T1041" />
            <conversion-tli id="T1042" />
            <conversion-tli id="T1043" />
            <conversion-tli id="T1044" />
            <conversion-tli id="T1045" />
            <conversion-tli id="T1046" />
            <conversion-tli id="T1047" />
            <conversion-tli id="T1048" />
            <conversion-tli id="T1049" />
            <conversion-tli id="T1050" />
            <conversion-tli id="T1051" />
            <conversion-tli id="T1052" />
            <conversion-tli id="T1053" />
            <conversion-tli id="T1054" />
            <conversion-tli id="T1055" />
            <conversion-tli id="T1056" />
            <conversion-tli id="T1057" />
            <conversion-tli id="T1058" />
            <conversion-tli id="T1059" />
            <conversion-tli id="T1060" />
            <conversion-tli id="T1061" />
            <conversion-tli id="T1062" />
            <conversion-tli id="T1063" />
            <conversion-tli id="T1064" />
            <conversion-tli id="T1065" />
            <conversion-tli id="T1066" />
            <conversion-tli id="T1067" />
            <conversion-tli id="T1068" />
            <conversion-tli id="T1069" />
            <conversion-tli id="T1070" />
            <conversion-tli id="T1071" />
            <conversion-tli id="T1072" />
            <conversion-tli id="T1073" />
            <conversion-tli id="T1074" />
            <conversion-tli id="T1075" />
            <conversion-tli id="T1076" />
            <conversion-tli id="T1077" />
            <conversion-tli id="T1078" />
            <conversion-tli id="T1079" />
            <conversion-tli id="T1080" />
            <conversion-tli id="T1081" />
            <conversion-tli id="T1082" />
            <conversion-tli id="T1083" />
            <conversion-tli id="T1084" />
            <conversion-tli id="T1085" />
            <conversion-tli id="T1086" />
            <conversion-tli id="T1088" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-PKZ"
                          name="ref"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PKZ"
                          name="ts"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PKZ"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PKZ"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PKZ"
                          name="mb"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PKZ"
                          name="mp"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PKZ"
                          name="ge"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PKZ"
                          name="gr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PKZ"
                          name="mc"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PKZ"
                          name="ps"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PKZ"
                          name="SeR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PKZ"
                          name="SyF"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PKZ"
                          name="IST"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PKZ"
                          name="BOR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PKZ"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PKZ"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PKZ"
                          name="CS"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PKZ"
                          name="fr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PKZ"
                          name="fe"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PKZ"
                          name="fg"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PKZ"
                          name="nt"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-KA"
                          name="ref"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KA"
                          name="ts"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KA"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KA"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-KA"
                          name="mb"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-KA"
                          name="mp"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-KA"
                          name="ge"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-KA"
                          name="gr"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-KA"
                          name="mc"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-KA"
                          name="ps"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-KA"
                          name="CS"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KA"
                          name="fr"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KA"
                          name="fe"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KA"
                          name="fg"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KA"
                          name="nt"
                          segmented-tier-id="tx-KA"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
