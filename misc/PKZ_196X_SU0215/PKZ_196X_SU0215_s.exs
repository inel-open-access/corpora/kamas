<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID1EE54957-80B2-B997-74ED-512CA9ADE1A9">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_SU0215.wav" />
         <referenced-file url="PKZ_196X_SU0215.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_196X_SU0215\PKZ_196X_SU0215.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1631</ud-information>
            <ud-information attribute-name="# HIAT:w">1053</ud-information>
            <ud-information attribute-name="# e">1056</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">5</ud-information>
            <ud-information attribute-name="# HIAT:u">191</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.36" type="appl" />
         <tli id="T1" time="0.977" type="appl" />
         <tli id="T2" time="1.594" type="appl" />
         <tli id="T3" time="2.211" type="appl" />
         <tli id="T4" time="2.829" type="appl" />
         <tli id="T5" time="3.446" type="appl" />
         <tli id="T6" time="4.063" type="appl" />
         <tli id="T7" time="4.68" type="appl" />
         <tli id="T8" time="5.735" type="appl" />
         <tli id="T9" time="6.76" type="appl" />
         <tli id="T10" time="7.285" type="appl" />
         <tli id="T11" time="9.939946590834895" />
         <tli id="T12" time="11.77" type="appl" />
         <tli id="T13" time="14.99991940266835" />
         <tli id="T14" time="15.913" type="appl" />
         <tli id="T15" time="16.53" type="appl" />
         <tli id="T16" time="17.36" type="appl" />
         <tli id="T17" time="19.115" type="appl" />
         <tli id="T18" time="20.4" type="appl" />
         <tli id="T19" time="21.685" type="appl" />
         <tli id="T20" time="24.793200114899374" />
         <tli id="T21" time="26.154" type="appl" />
         <tli id="T22" time="27.507" type="appl" />
         <tli id="T23" time="28.861" type="appl" />
         <tli id="T24" time="29.732" type="appl" />
         <tli id="T25" time="30.5" type="appl" />
         <tli id="T26" time="31.268" type="appl" />
         <tli id="T27" time="32.035" type="appl" />
         <tli id="T28" time="32.693" type="appl" />
         <tli id="T29" time="33.292" type="appl" />
         <tli id="T30" time="33.77981849480912" />
         <tli id="T31" time="34.55" type="appl" />
         <tli id="T32" time="35.21" type="appl" />
         <tli id="T33" time="36.80646889872531" />
         <tli id="T34" time="37.707" type="appl" />
         <tli id="T35" time="38.6" type="appl" />
         <tli id="T36" time="39.492" type="appl" />
         <tli id="T37" time="41.499777014049094" />
         <tli id="T38" time="42.121" type="appl" />
         <tli id="T39" time="42.748" type="appl" />
         <tli id="T40" time="43.374" type="appl" />
         <tli id="T41" time="44.0" type="appl" />
         <tli id="T42" time="44.626" type="appl" />
         <tli id="T43" time="45.252" type="appl" />
         <tli id="T44" time="45.879" type="appl" />
         <tli id="T45" time="46.793081905479625" />
         <tli id="T46" time="47.969" type="appl" />
         <tli id="T47" time="49.113" type="appl" />
         <tli id="T48" time="50.257" type="appl" />
         <tli id="T49" time="51.401" type="appl" />
         <tli id="T50" time="52.545" type="appl" />
         <tli id="T51" time="53.083" type="appl" />
         <tli id="T52" time="53.621" type="appl" />
         <tli id="T53" time="54.159" type="appl" />
         <tli id="T54" time="54.697" type="appl" />
         <tli id="T55" time="55.235" type="appl" />
         <tli id="T56" time="55.94" type="appl" />
         <tli id="T57" time="56.605" type="appl" />
         <tli id="T58" time="57.27" type="appl" />
         <tli id="T59" time="58.199687282353196" />
         <tli id="T60" time="58.957" type="appl" />
         <tli id="T61" time="59.663" type="appl" />
         <tli id="T62" time="60.586341125088865" />
         <tli id="T63" time="61.248" type="appl" />
         <tli id="T64" time="61.95" type="appl" />
         <tli id="T65" time="62.652" type="appl" />
         <tli id="T66" time="63.61965815985069" />
         <tli id="T67" time="64.508" type="appl" />
         <tli id="T68" time="65.302" type="appl" />
         <tli id="T69" time="66.095" type="appl" />
         <tli id="T70" time="66.831" type="appl" />
         <tli id="T71" time="67.502" type="appl" />
         <tli id="T72" time="68.173" type="appl" />
         <tli id="T73" time="68.844" type="appl" />
         <tli id="T74" time="69.516" type="appl" />
         <tli id="T75" time="70.187" type="appl" />
         <tli id="T76" time="70.858" type="appl" />
         <tli id="T77" time="71.529" type="appl" />
         <tli id="T78" time="72.2" type="appl" />
         <tli id="T79" time="73.268" type="appl" />
         <tli id="T80" time="74.181" type="appl" />
         <tli id="T81" time="75.094" type="appl" />
         <tli id="T82" time="76.007" type="appl" />
         <tli id="T83" time="78.83957638042484" />
         <tli id="T84" time="79.97" type="appl" />
         <tli id="T85" time="80.96" type="appl" />
         <tli id="T86" time="81.95" type="appl" />
         <tli id="T87" time="82.94" type="appl" />
         <tli id="T88" time="83.93" type="appl" />
         <tli id="T89" time="85.12620926785429" />
         <tli id="T90" time="86.0" type="appl" />
         <tli id="T91" time="87.07286547477835" />
         <tli id="T92" time="87.538" type="appl" />
         <tli id="T93" time="87.932" type="appl" />
         <tli id="T94" time="88.325" type="appl" />
         <tli id="T95" time="88.718" type="appl" />
         <tli id="T96" time="89.112" type="appl" />
         <tli id="T97" time="89.61951845780918" />
         <tli id="T98" time="90.1" type="appl" />
         <tli id="T99" time="90.5" type="appl" />
         <tli id="T100" time="91.04617745877407" />
         <tli id="T101" time="91.728" type="appl" />
         <tli id="T102" time="92.362" type="appl" />
         <tli id="T103" time="93.51949750250294" />
         <tli id="T104" time="95.44615381689012" />
         <tli id="T105" time="95.631" type="appl" />
         <tli id="T106" time="96.759" type="appl" />
         <tli id="T107" time="97.887" type="appl" />
         <tli id="T108" time="100.3794606426566" />
         <tli id="T109" time="101.313" type="appl" />
         <tli id="T110" time="102.136" type="appl" />
         <tli id="T111" time="102.959" type="appl" />
         <tli id="T112" time="103.781" type="appl" />
         <tli id="T113" time="104.604" type="appl" />
         <tli id="T114" time="105.427" type="appl" />
         <tli id="T115" time="106.25" type="appl" />
         <tli id="T116" time="107.73" type="appl" />
         <tli id="T117" time="108.95" type="appl" />
         <tli id="T118" time="110.50607289716915" />
         <tli id="T119" time="111.541" type="appl" />
         <tli id="T120" time="112.477" type="appl" />
         <tli id="T121" time="113.412" type="appl" />
         <tli id="T122" time="114.348" type="appl" />
         <tli id="T123" time="115.284" type="appl" />
         <tli id="T124" time="116.66603979853161" />
         <tli id="T125" time="117.215" type="appl" />
         <tli id="T126" time="117.825" type="appl" />
         <tli id="T127" time="118.435" type="appl" />
         <tli id="T128" time="119.045" type="appl" />
         <tli id="T129" time="119.665" type="appl" />
         <tli id="T130" time="120.1" type="appl" />
         <tli id="T131" time="120.535" type="appl" />
         <tli id="T132" time="120.97" type="appl" />
         <tli id="T133" time="121.405" type="appl" />
         <tli id="T134" time="121.91332931221122" />
         <tli id="T135" time="122.441" type="appl" />
         <tli id="T136" time="122.902" type="appl" />
         <tli id="T137" time="123.363" type="appl" />
         <tli id="T138" time="123.824" type="appl" />
         <tli id="T139" time="124.285" type="appl" />
         <tli id="T140" time="124.746" type="appl" />
         <tli id="T141" time="125.207" type="appl" />
         <tli id="T142" time="125.668" type="appl" />
         <tli id="T143" time="126.129" type="appl" />
         <tli id="T144" time="126.64332993871274" />
         <tli id="T145" time="127.238" type="appl" />
         <tli id="T146" time="127.655" type="appl" />
         <tli id="T147" time="128.072" type="appl" />
         <tli id="T148" time="128.49" type="appl" />
         <tli id="T149" time="128.908" type="appl" />
         <tli id="T150" time="129.325" type="appl" />
         <tli id="T151" time="129.742" type="appl" />
         <tli id="T152" time="130.4200023542891" />
         <tli id="T153" time="130.983" type="appl" />
         <tli id="T154" time="131.641" type="appl" />
         <tli id="T155" time="132.299" type="appl" />
         <tli id="T156" time="132.957" type="appl" />
         <tli id="T157" time="133.6283314695121" />
         <tli id="T158" time="134.444" type="appl" />
         <tli id="T159" time="135.178" type="appl" />
         <tli id="T160" time="135.912" type="appl" />
         <tli id="T161" time="136.646" type="appl" />
         <tli id="T162" time="137.381" type="appl" />
         <tli id="T163" time="138.115" type="appl" />
         <tli id="T164" time="138.849" type="appl" />
         <tli id="T165" time="139.583" type="appl" />
         <tli id="T166" time="141.4859064368135" />
         <tli id="T167" time="142.828" type="appl" />
         <tli id="T168" time="144.055" type="appl" />
         <tli id="T169" time="145.283" type="appl" />
         <tli id="T170" time="146.51" type="appl" />
         <tli id="T171" time="147.95253835707499" />
         <tli id="T172" time="148.892" type="appl" />
         <tli id="T173" time="149.874" type="appl" />
         <tli id="T174" time="150.856" type="appl" />
         <tli id="T175" time="151.838" type="appl" />
         <tli id="T176" time="153.78584034700157" />
         <tli id="T177" time="154.7" type="appl" />
         <tli id="T178" time="155.54" type="appl" />
         <tli id="T179" time="156.57249204047508" />
         <tli id="T180" time="157.358" type="appl" />
         <tli id="T181" time="158.13" type="appl" />
         <tli id="T182" time="158.902" type="appl" />
         <tli id="T183" time="161.40579940355707" />
         <tli id="T184" time="162.154" type="appl" />
         <tli id="T185" time="162.958" type="appl" />
         <tli id="T186" time="163.762" type="appl" />
         <tli id="T187" time="164.566" type="appl" />
         <tli id="T188" time="166.7924371268264" />
         <tli id="T189" time="167.678" type="appl" />
         <tli id="T190" time="168.502" type="appl" />
         <tli id="T191" time="169.325" type="appl" />
         <tli id="T192" time="170.382" type="appl" />
         <tli id="T193" time="171.278" type="appl" />
         <tli id="T194" time="173.20573600032284" />
         <tli id="T195" time="174.172" type="appl" />
         <tli id="T196" time="174.87" type="appl" />
         <tli id="T197" time="175.568" type="appl" />
         <tli id="T198" time="176.9257160121846" />
         <tli id="T199" time="177.598" type="appl" />
         <tli id="T200" time="178.492" type="appl" />
         <tli id="T201" time="181.43235846383072" />
         <tli id="T202" time="182.024" type="appl" />
         <tli id="T203" time="182.929" type="appl" />
         <tli id="T204" time="183.833" type="appl" />
         <tli id="T205" time="184.737" type="appl" />
         <tli id="T206" time="185.641" type="appl" />
         <tli id="T207" time="186.546" type="appl" />
         <tli id="T208" time="188.53232031442707" />
         <tli id="T209" time="189.3" type="appl" />
         <tli id="T210" time="190.03" type="appl" />
         <tli id="T211" time="190.76" type="appl" />
         <tli id="T212" time="191.64999887506553" />
         <tli id="T213" time="192.383" type="appl" />
         <tli id="T214" time="193.276" type="appl" />
         <tli id="T215" time="194.17" type="appl" />
         <tli id="T216" time="195.063" type="appl" />
         <tli id="T217" time="195.956" type="appl" />
         <tli id="T218" time="198.218934933128" />
         <tli id="T219" time="199.211" type="appl" />
         <tli id="T220" time="200.152" type="appl" />
         <tli id="T221" time="201.094" type="appl" />
         <tli id="T222" time="202.035" type="appl" />
         <tli id="T223" time="202.976" type="appl" />
         <tli id="T224" time="206.9922211259776" />
         <tli id="T225" time="207.643" type="appl" />
         <tli id="T226" time="208.498" type="appl" />
         <tli id="T227" time="209.353" type="appl" />
         <tli id="T228" time="210.208" type="appl" />
         <tli id="T229" time="211.005" type="appl" />
         <tli id="T230" time="211.803" type="appl" />
         <tli id="T231" time="212.533" type="appl" />
         <tli id="T232" time="213.263" type="appl" />
         <tli id="T233" time="215.67884111783397" />
         <tli id="T234" time="216.572" type="appl" />
         <tli id="T235" time="217.525" type="appl" />
         <tli id="T236" time="218.478" type="appl" />
         <tli id="T237" time="220.05215095256753" />
         <tli id="T238" time="221.033" type="appl" />
         <tli id="T239" time="221.967" type="appl" />
         <tli id="T240" time="222.9" type="appl" />
         <tli id="T241" time="223.771" type="appl" />
         <tli id="T242" time="224.477" type="appl" />
         <tli id="T243" time="225.183" type="appl" />
         <tli id="T244" time="225.889" type="appl" />
         <tli id="T245" time="226.83878115341923" />
         <tli id="T246" time="227.973" type="appl" />
         <tli id="T247" time="229.021" type="appl" />
         <tli id="T248" time="230.069" type="appl" />
         <tli id="T249" time="231.116" type="appl" />
         <tli id="T250" time="232.164" type="appl" />
         <tli id="T251" time="233.212" type="appl" />
         <tli id="T252" time="237.59205670742102" />
         <tli id="T253" time="238.499" type="appl" />
         <tli id="T254" time="239.241" type="appl" />
         <tli id="T255" time="239.983" type="appl" />
         <tli id="T256" time="240.725" type="appl" />
         <tli id="T257" time="242.79869539785835" />
         <tli id="T258" time="243.541" type="appl" />
         <tli id="T259" time="244.182" type="appl" />
         <tli id="T260" time="244.823" type="appl" />
         <tli id="T261" time="245.464" type="appl" />
         <tli id="T262" time="246.104" type="appl" />
         <tli id="T263" time="246.745" type="appl" />
         <tli id="T264" time="247.386" type="appl" />
         <tli id="T265" time="248.027" type="appl" />
         <tli id="T266" time="249.0853282852878" />
         <tli id="T267" time="249.763" type="appl" />
         <tli id="T268" time="250.567" type="appl" />
         <tli id="T269" time="251.37" type="appl" />
         <tli id="T270" time="252.173" type="appl" />
         <tli id="T271" time="252.976" type="appl" />
         <tli id="T272" time="253.779" type="appl" />
         <tli id="T273" time="254.583" type="appl" />
         <tli id="T274" time="257.251951071185" />
         <tli id="T275" time="258.609" type="appl" />
         <tli id="T276" time="259.947" type="appl" />
         <tli id="T277" time="261.286" type="appl" />
         <tli id="T278" time="262.624" type="appl" />
         <tli id="T279" time="263.963" type="appl" />
         <tli id="T280" time="265.301" type="appl" />
         <tli id="T281" time="268.0985594570256" />
         <tli id="T282" time="269.08" type="appl" />
         <tli id="T283" time="269.86" type="appl" />
         <tli id="T284" time="270.64" type="appl" />
         <tli id="T285" time="271.275" type="appl" />
         <tli id="T286" time="271.895" type="appl" />
         <tli id="T287" time="272.515" type="appl" />
         <tli id="T288" time="273.42" type="appl" />
         <tli id="T289" time="274.27" type="appl" />
         <tli id="T290" time="275.9451839623326" />
         <tli id="T291" time="276.503" type="appl" />
         <tli id="T292" time="277.146" type="appl" />
         <tli id="T293" time="277.789" type="appl" />
         <tli id="T294" time="278.431" type="appl" />
         <tli id="T295" time="279.074" type="appl" />
         <tli id="T296" time="279.717" type="appl" />
         <tli id="T297" time="281.3718214706757" />
         <tli id="T298" time="281.991" type="appl" />
         <tli id="T299" time="282.642" type="appl" />
         <tli id="T300" time="283.294" type="appl" />
         <tli id="T301" time="283.945" type="appl" />
         <tli id="T302" time="284.596" type="appl" />
         <tli id="T303" time="285.248" type="appl" />
         <tli id="T304" time="285.899" type="appl" />
         <tli id="T305" time="286.55" type="appl" />
         <tli id="T306" time="287.281" type="appl" />
         <tli id="T307" time="288.012" type="appl" />
         <tli id="T308" time="288.742" type="appl" />
         <tli id="T309" time="289.473" type="appl" />
         <tli id="T310" time="290.204" type="appl" />
         <tli id="T311" time="290.935" type="appl" />
         <tli id="T312" time="291.666" type="appl" />
         <tli id="T313" time="292.397" type="appl" />
         <tli id="T314" time="293.128" type="appl" />
         <tli id="T315" time="293.858" type="appl" />
         <tli id="T316" time="294.589" type="appl" />
         <tli id="T317" time="298.0650651081342" />
         <tli id="T318" time="298.564" type="appl" />
         <tli id="T319" time="299.037" type="appl" />
         <tli id="T320" time="299.511" type="appl" />
         <tli id="T321" time="299.985" type="appl" />
         <tli id="T322" time="300.459" type="appl" />
         <tli id="T323" time="300.932" type="appl" />
         <tli id="T324" time="301.406" type="appl" />
         <tli id="T325" time="302.8783725786793" />
         <tli id="T326" time="304.295" type="appl" />
         <tli id="T327" time="305.7650237348373" />
         <tli id="T328" time="307.04" type="appl" />
         <tli id="T329" time="309.358337760632" />
         <tli id="T330" time="310.203" type="appl" />
         <tli id="T331" time="311.181" type="appl" />
         <tli id="T1057" time="311.7941372628427" type="intp" />
         <tli id="T332" time="312.61165361329967" />
         <tli id="T333" time="313.133" type="appl" />
         <tli id="T334" time="313.816" type="appl" />
         <tli id="T335" time="314.499" type="appl" />
         <tli id="T336" time="315.182" type="appl" />
         <tli id="T337" time="315.866" type="appl" />
         <tli id="T338" time="316.549" type="appl" />
         <tli id="T339" time="317.232" type="appl" />
         <tli id="T340" time="317.915" type="appl" />
         <tli id="T341" time="318.598" type="appl" />
         <tli id="T342" time="319.74494861812417" />
         <tli id="T343" time="320.765" type="appl" />
         <tli id="T344" time="322.0916026757861" />
         <tli id="T345" time="323.155" type="appl" />
         <tli id="T346" time="327.50490692243795" />
         <tli id="T347" time="328.32" type="appl" />
         <tli id="T348" time="328.96" type="appl" />
         <tli id="T349" time="329.6" type="appl" />
         <tli id="T350" time="332.391547332285" />
         <tli id="T351" time="333.256" type="appl" />
         <tli id="T352" time="333.881" type="appl" />
         <tli id="T353" time="334.507" type="appl" />
         <tli id="T354" time="335.133" type="appl" />
         <tli id="T355" time="335.759" type="appl" />
         <tli id="T356" time="336.384" type="appl" />
         <tli id="T357" time="342.10482847436845" />
         <tli id="T358" time="343.04" type="appl" />
         <tli id="T359" time="343.76" type="appl" />
         <tli id="T360" time="345.37814421957296" />
         <tli id="T361" time="346.426" type="appl" />
         <tli id="T362" time="347.293" type="appl" />
         <tli id="T363" time="348.159" type="appl" />
         <tli id="T364" time="349.025" type="appl" />
         <tli id="T365" time="349.892" type="appl" />
         <tli id="T366" time="350.758" type="appl" />
         <tli id="T367" time="351.625" type="appl" />
         <tli id="T368" time="352.491" type="appl" />
         <tli id="T369" time="353.357" type="appl" />
         <tli id="T370" time="354.224" type="appl" />
         <tli id="T371" time="355.31142417956227" />
         <tli id="T372" time="356.352" type="appl" />
         <tli id="T373" time="357.315" type="appl" />
         <tli id="T374" time="358.278" type="appl" />
         <tli id="T375" time="361.2647255247101" />
         <tli id="T376" time="362.241" type="appl" />
         <tli id="T377" time="362.932" type="appl" />
         <tli id="T378" time="363.624" type="appl" />
         <tli id="T379" time="364.315" type="appl" />
         <tli id="T380" time="365.006" type="appl" />
         <tli id="T381" time="365.697" type="appl" />
         <tli id="T382" time="366.388" type="appl" />
         <tli id="T383" time="367.08" type="appl" />
         <tli id="T384" time="367.771" type="appl" />
         <tli id="T385" time="370.08467813347914" />
         <tli id="T386" time="370.483" type="appl" />
         <tli id="T387" time="370.986" type="appl" />
         <tli id="T388" time="371.489" type="appl" />
         <tli id="T389" time="371.992" type="appl" />
         <tli id="T390" time="372.494" type="appl" />
         <tli id="T391" time="372.997" type="appl" />
         <tli id="T392" time="373.5" type="appl" />
         <tli id="T393" time="374.39132165975644" />
         <tli id="T394" time="374.95" type="appl" />
         <tli id="T395" time="375.464" type="appl" />
         <tli id="T396" time="375.978" type="appl" />
         <tli id="T397" time="376.492" type="appl" />
         <tli id="T398" time="377.006" type="appl" />
         <tli id="T399" time="377.646" type="appl" />
         <tli id="T400" time="378.215" type="appl" />
         <tli id="T401" time="378.785" type="appl" />
         <tli id="T402" time="379.355" type="appl" />
         <tli id="T403" time="379.924" type="appl" />
         <tli id="T404" time="380.494" type="appl" />
         <tli id="T405" time="381.063" type="appl" />
         <tli id="T406" time="383.13794132922345" />
         <tli id="T407" time="383.814" type="appl" />
         <tli id="T408" time="384.418" type="appl" />
         <tli id="T409" time="385.022" type="appl" />
         <tli id="T410" time="385.626" type="appl" />
         <tli id="T411" time="386.23" type="appl" />
         <tli id="T412" time="386.834" type="appl" />
         <tli id="T413" time="387.438" type="appl" />
         <tli id="T414" time="388.042" type="appl" />
         <tli id="T415" time="388.646" type="appl" />
         <tli id="T416" time="389.45790737088106" />
         <tli id="T417" time="390.072" type="appl" />
         <tli id="T418" time="390.703" type="appl" />
         <tli id="T419" time="391.335" type="appl" />
         <tli id="T420" time="391.967" type="appl" />
         <tli id="T421" time="392.598" type="appl" />
         <tli id="T422" time="393.23" type="appl" />
         <tli id="T423" time="394.019" type="appl" />
         <tli id="T424" time="394.687" type="appl" />
         <tli id="T425" time="395.356" type="appl" />
         <tli id="T426" time="396.024" type="appl" />
         <tli id="T427" time="396.693" type="appl" />
         <tli id="T428" time="397.361" type="appl" />
         <tli id="T429" time="399.0111893726694" />
         <tli id="T430" time="399.983" type="appl" />
         <tli id="T431" time="400.876" type="appl" />
         <tli id="T432" time="401.769" type="appl" />
         <tli id="T433" time="402.661" type="appl" />
         <tli id="T434" time="403.554" type="appl" />
         <tli id="T435" time="404.447" type="appl" />
         <tli id="T436" time="406.16448427003075" />
         <tli id="T437" time="406.776" type="appl" />
         <tli id="T438" time="407.243" type="appl" />
         <tli id="T439" time="407.709" type="appl" />
         <tli id="T440" time="408.175" type="appl" />
         <tli id="T441" time="408.642" type="appl" />
         <tli id="T442" time="409.108" type="appl" />
         <tli id="T443" time="409.575" type="appl" />
         <tli id="T444" time="410.041" type="appl" />
         <tli id="T445" time="410.507" type="appl" />
         <tli id="T446" time="410.974" type="appl" />
         <tli id="T447" time="411.6511214559846" />
         <tli id="T448" time="412.198" type="appl" />
         <tli id="T449" time="412.796" type="appl" />
         <tli id="T450" time="413.395" type="appl" />
         <tli id="T451" time="413.993" type="appl" />
         <tli id="T452" time="414.591" type="appl" />
         <tli id="T453" time="415.189" type="appl" />
         <tli id="T454" time="415.787" type="appl" />
         <tli id="T455" time="416.385" type="appl" />
         <tli id="T456" time="416.984" type="appl" />
         <tli id="T457" time="417.582" type="appl" />
         <tli id="T458" time="418.18" type="appl" />
         <tli id="T459" time="418.633" type="appl" />
         <tli id="T460" time="419.087" type="appl" />
         <tli id="T461" time="420.00440990555944" />
         <tli id="T462" time="420.494" type="appl" />
         <tli id="T463" time="421.207" type="appl" />
         <tli id="T464" time="421.921" type="appl" />
         <tli id="T465" time="422.635" type="appl" />
         <tli id="T466" time="423.348" type="appl" />
         <tli id="T467" time="424.062" type="appl" />
         <tli id="T468" time="424.776" type="appl" />
         <tli id="T469" time="425.489" type="appl" />
         <tli id="T470" time="426.53104150343154" />
         <tli id="T471" time="427.04" type="appl" />
         <tli id="T472" time="427.69" type="appl" />
         <tli id="T473" time="428.34" type="appl" />
         <tli id="T474" time="428.99" type="appl" />
         <tli id="T475" time="429.751024201871" />
         <tli id="T476" time="430.208" type="appl" />
         <tli id="T477" time="430.677" type="appl" />
         <tli id="T478" time="431.145" type="appl" />
         <tli id="T479" time="431.613" type="appl" />
         <tli id="T480" time="432.082" type="appl" />
         <tli id="T481" time="433.20433897990756" />
         <tli id="T482" time="433.738" type="appl" />
         <tli id="T483" time="434.397" type="appl" />
         <tli id="T484" time="435.055" type="appl" />
         <tli id="T485" time="435.713" type="appl" />
         <tli id="T486" time="436.372" type="appl" />
         <tli id="T487" time="439.05097423152546" />
         <tli id="T488" time="439.737" type="appl" />
         <tli id="T489" time="440.444" type="appl" />
         <tli id="T490" time="441.151" type="appl" />
         <tli id="T491" time="441.858" type="appl" />
         <tli id="T492" time="442.565" type="appl" />
         <tli id="T493" time="443.272" type="appl" />
         <tli id="T494" time="443.979" type="appl" />
         <tli id="T495" time="444.686" type="appl" />
         <tli id="T496" time="445.393" type="appl" />
         <tli id="T497" time="446.24666473442073" />
         <tli id="T498" time="447.066" type="appl" />
         <tli id="T499" time="447.862" type="appl" />
         <tli id="T500" time="448.658" type="appl" />
         <tli id="T501" time="450.03091523427867" />
         <tli id="T502" time="450.979" type="appl" />
         <tli id="T503" time="451.647" type="appl" />
         <tli id="T504" time="452.316" type="appl" />
         <tli id="T505" time="452.984" type="appl" />
         <tli id="T506" time="453.653" type="appl" />
         <tli id="T507" time="454.321" type="appl" />
         <tli id="T508" time="455.143335680968" />
         <tli id="T509" time="455.76" type="appl" />
         <tli id="T510" time="456.48" type="appl" />
         <tli id="T511" time="457.2" type="appl" />
         <tli id="T512" time="457.92" type="appl" />
         <tli id="T513" time="458.64" type="appl" />
         <tli id="T514" time="460.8441904658911" />
         <tli id="T515" time="461.642" type="appl" />
         <tli id="T516" time="462.514" type="appl" />
         <tli id="T517" time="463.386" type="appl" />
         <tli id="T518" time="464.258" type="appl" />
         <tli id="T519" time="465.13" type="appl" />
         <tli id="T520" time="466.115" type="appl" />
         <tli id="T521" time="467.07" type="appl" />
         <tli id="T522" time="468.025" type="appl" />
         <tli id="T523" time="468.98" type="appl" />
         <tli id="T524" time="469.935" type="appl" />
         <tli id="T525" time="471.092" type="appl" />
         <tli id="T526" time="472.232" type="appl" />
         <tli id="T527" time="473.372" type="appl" />
         <tli id="T528" time="474.797" type="appl" />
         <tli id="T529" time="476.107" type="appl" />
         <tli id="T530" time="477.192" type="appl" />
         <tli id="T531" time="478.4953456229811" />
         <tli id="T532" time="479.339" type="appl" />
         <tli id="T533" time="480.161" type="appl" />
         <tli id="T534" time="480.983" type="appl" />
         <tli id="T535" time="481.805" type="appl" />
         <tli id="T536" time="482.8469889074663" />
         <tli id="T537" time="484.112" type="appl" />
         <tli id="T538" time="485.178" type="appl" />
         <tli id="T539" time="486.89071717976896" />
         <tli id="T540" time="487.662" type="appl" />
         <tli id="T541" time="488.465" type="appl" />
         <tli id="T542" time="489.268" type="appl" />
         <tli id="T543" time="490.07" type="appl" />
         <tli id="T544" time="490.703" type="appl" />
         <tli id="T545" time="491.206" type="appl" />
         <tli id="T546" time="491.709" type="appl" />
         <tli id="T547" time="492.212" type="appl" />
         <tli id="T548" time="492.715" type="appl" />
         <tli id="T549" time="493.218" type="appl" />
         <tli id="T550" time="493.721" type="appl" />
         <tli id="T551" time="495.10400638158563" />
         <tli id="T552" time="495.68" type="appl" />
         <tli id="T553" time="496.06" type="appl" />
         <tli id="T554" time="496.44" type="appl" />
         <tli id="T555" time="496.82" type="appl" />
         <tli id="T556" time="497.2" type="appl" />
         <tli id="T557" time="497.58" type="appl" />
         <tli id="T558" time="497.96" type="appl" />
         <tli id="T559" time="498.34" type="appl" />
         <tli id="T560" time="499.26" type="appl" />
         <tli id="T561" time="499.748" type="appl" />
         <tli id="T562" time="500.211" type="appl" />
         <tli id="T563" time="500.674" type="appl" />
         <tli id="T564" time="501.136" type="appl" />
         <tli id="T565" time="501.599" type="appl" />
         <tli id="T566" time="502.062" type="appl" />
         <tli id="T567" time="502.870631316745" />
         <tli id="T568" time="503.65" type="appl" />
         <tli id="T569" time="504.21" type="appl" />
         <tli id="T570" time="504.77" type="appl" />
         <tli id="T571" time="505.33" type="appl" />
         <tli id="T572" time="505.932" type="appl" />
         <tli id="T573" time="506.528" type="appl" />
         <tli id="T574" time="507.45060670769305" />
         <tli id="T575" time="508.3" type="appl" />
         <tli id="T576" time="509.035" type="appl" />
         <tli id="T577" time="509.77" type="appl" />
         <tli id="T578" time="510.505" type="appl" />
         <tli id="T579" time="510.898" type="appl" />
         <tli id="T580" time="511.265" type="appl" />
         <tli id="T581" time="511.632" type="appl" />
         <tli id="T582" time="512.0" type="appl" />
         <tli id="T583" time="512.524" type="appl" />
         <tli id="T584" time="513.042" type="appl" />
         <tli id="T585" time="513.56" type="appl" />
         <tli id="T586" time="514.078" type="appl" />
         <tli id="T587" time="516.5172246577504" />
         <tli id="T588" time="517.66" type="appl" />
         <tli id="T589" time="518.46" type="appl" />
         <tli id="T590" time="519.26" type="appl" />
         <tli id="T591" time="520.2838710855316" />
         <tli id="T592" time="520.955" type="appl" />
         <tli id="T593" time="521.465" type="appl" />
         <tli id="T594" time="521.975" type="appl" />
         <tli id="T595" time="522.485" type="appl" />
         <tli id="T596" time="523.415" type="appl" />
         <tli id="T597" time="524.8438465839428" />
         <tli id="T598" time="525.798" type="appl" />
         <tli id="T599" time="526.495" type="appl" />
         <tli id="T600" time="527.193" type="appl" />
         <tli id="T601" time="527.891" type="appl" />
         <tli id="T602" time="528.588" type="appl" />
         <tli id="T603" time="529.286" type="appl" />
         <tli id="T604" time="529.984" type="appl" />
         <tli id="T605" time="530.682" type="appl" />
         <tli id="T606" time="531.379" type="appl" />
         <tli id="T607" time="532.077" type="appl" />
         <tli id="T608" time="532.775" type="appl" />
         <tli id="T609" time="533.472" type="appl" />
         <tli id="T610" time="534.6904603429388" />
         <tli id="T611" time="535.396" type="appl" />
         <tli id="T612" time="536.212" type="appl" />
         <tli id="T613" time="537.027" type="appl" />
         <tli id="T614" time="537.843" type="appl" />
         <tli id="T615" time="538.659" type="appl" />
         <tli id="T616" time="539.475" type="appl" />
         <tli id="T617" time="540.29" type="appl" />
         <tli id="T618" time="541.106" type="appl" />
         <tli id="T619" time="541.922" type="appl" />
         <tli id="T620" time="542.356" type="appl" />
         <tli id="T621" time="542.789" type="appl" />
         <tli id="T622" time="543.223" type="appl" />
         <tli id="T623" time="543.657" type="appl" />
         <tli id="T624" time="544.09" type="appl" />
         <tli id="T625" time="544.524" type="appl" />
         <tli id="T626" time="544.958" type="appl" />
         <tli id="T627" time="545.392" type="appl" />
         <tli id="T628" time="545.825" type="appl" />
         <tli id="T629" time="546.259" type="appl" />
         <tli id="T630" time="546.693" type="appl" />
         <tli id="T631" time="547.126" type="appl" />
         <tli id="T632" time="547.56" type="appl" />
         <tli id="T633" time="548.212" type="appl" />
         <tli id="T634" time="548.785" type="appl" />
         <tli id="T635" time="549.358" type="appl" />
         <tli id="T636" time="549.93" type="appl" />
         <tli id="T637" time="550.448" type="appl" />
         <tli id="T638" time="550.965" type="appl" />
         <tli id="T639" time="551.483" type="appl" />
         <tli id="T640" time="552.001" type="appl" />
         <tli id="T641" time="552.518" type="appl" />
         <tli id="T642" time="553.036" type="appl" />
         <tli id="T643" time="553.554" type="appl" />
         <tli id="T644" time="554.071" type="appl" />
         <tli id="T645" time="554.589" type="appl" />
         <tli id="T646" time="555.106" type="appl" />
         <tli id="T647" time="555.624" type="appl" />
         <tli id="T648" time="556.142" type="appl" />
         <tli id="T649" time="556.659" type="appl" />
         <tli id="T650" time="557.177" type="appl" />
         <tli id="T651" time="557.695" type="appl" />
         <tli id="T652" time="558.212" type="appl" />
         <tli id="T653" time="560.1569901732469" />
         <tli id="T654" time="561.21" type="appl" />
         <tli id="T655" time="562.065" type="appl" />
         <tli id="T656" time="562.92" type="appl" />
         <tli id="T657" time="563.775" type="appl" />
         <tli id="T658" time="564.703" type="appl" />
         <tli id="T659" time="565.497" type="appl" />
         <tli id="T660" time="566.29" type="appl" />
         <tli id="T661" time="567.052" type="appl" />
         <tli id="T662" time="567.78" type="appl" />
         <tli id="T663" time="568.508" type="appl" />
         <tli id="T664" time="571.2702637929127" />
         <tli id="T665" time="571.961" type="appl" />
         <tli id="T666" time="572.683" type="appl" />
         <tli id="T667" time="573.404" type="appl" />
         <tli id="T668" time="574.126" type="appl" />
         <tli id="T669" time="574.847" type="appl" />
         <tli id="T670" time="575.569" type="appl" />
         <tli id="T671" time="576.29" type="appl" />
         <tli id="T672" time="577.157" type="appl" />
         <tli id="T673" time="577.684" type="appl" />
         <tli id="T674" time="578.211" type="appl" />
         <tli id="T675" time="578.739" type="appl" />
         <tli id="T676" time="579.266" type="appl" />
         <tli id="T677" time="579.793" type="appl" />
         <tli id="T678" time="580.32" type="appl" />
         <tli id="T679" time="580.964" type="appl" />
         <tli id="T680" time="581.508" type="appl" />
         <tli id="T681" time="582.052" type="appl" />
         <tli id="T682" time="582.596" type="appl" />
         <tli id="T683" time="583.14" type="appl" />
         <tli id="T684" time="583.749" type="appl" />
         <tli id="T685" time="584.258" type="appl" />
         <tli id="T686" time="584.767" type="appl" />
         <tli id="T687" time="585.276" type="appl" />
         <tli id="T688" time="585.784" type="appl" />
         <tli id="T689" time="586.293" type="appl" />
         <tli id="T690" time="586.802" type="appl" />
         <tli id="T691" time="587.311" type="appl" />
         <tli id="T692" time="587.9666844966334" />
         <tli id="T693" time="588.597" type="appl" />
         <tli id="T694" time="589.109" type="appl" />
         <tli id="T695" time="589.621" type="appl" />
         <tli id="T696" time="590.133" type="appl" />
         <tli id="T697" time="591.2168232830388" />
         <tli id="T698" time="592.465" type="appl" />
         <tli id="T699" time="593.48" type="appl" />
         <tli id="T700" time="594.212" type="appl" />
         <tli id="T701" time="594.905" type="appl" />
         <tli id="T702" time="595.598" type="appl" />
         <tli id="T703" time="596.29" type="appl" />
         <tli id="T704" time="597.238" type="appl" />
         <tli id="T705" time="598.055" type="appl" />
         <tli id="T706" time="598.873" type="appl" />
         <tli id="T707" time="599.8833392162514" />
         <tli id="T708" time="600.57" type="appl" />
         <tli id="T709" time="601.14" type="appl" />
         <tli id="T710" time="601.71" type="appl" />
         <tli id="T711" time="602.28" type="appl" />
         <tli id="T712" time="602.85" type="appl" />
         <tli id="T713" time="603.42" type="appl" />
         <tli id="T714" time="605.6700789563655" />
         <tli id="T715" time="606.683" type="appl" />
         <tli id="T716" time="607.266" type="appl" />
         <tli id="T717" time="607.849" type="appl" />
         <tli id="T718" time="608.432" type="appl" />
         <tli id="T719" time="609.015" type="appl" />
         <tli id="T720" time="609.598" type="appl" />
         <tli id="T721" time="610.181" type="appl" />
         <tli id="T722" time="610.764" type="appl" />
         <tli id="T723" time="611.347" type="appl" />
         <tli id="T724" time="611.93" type="appl" />
         <tli id="T725" time="612.592" type="appl" />
         <tli id="T726" time="613.184" type="appl" />
         <tli id="T727" time="613.776" type="appl" />
         <tli id="T728" time="614.368" type="appl" />
         <tli id="T729" time="615.5966922855091" />
         <tli id="T730" time="616.905" type="appl" />
         <tli id="T731" time="617.69" type="appl" />
         <tli id="T732" time="618.475" type="appl" />
         <tli id="T733" time="619.26" type="appl" />
         <tli id="T734" time="620.322" type="appl" />
         <tli id="T735" time="621.293" type="appl" />
         <tli id="T736" time="622.265" type="appl" />
         <tli id="T737" time="623.236" type="appl" />
         <tli id="T738" time="624.208" type="appl" />
         <tli id="T739" time="625.18" type="appl" />
         <tli id="T740" time="626.151" type="appl" />
         <tli id="T741" time="628.9499538693067" />
         <tli id="T742" time="629.575" type="appl" />
         <tli id="T743" time="630.23" type="appl" />
         <tli id="T744" time="630.885" type="appl" />
         <tli id="T745" time="631.7232723010889" />
         <tli id="T746" time="632.529" type="appl" />
         <tli id="T747" time="633.323" type="appl" />
         <tli id="T748" time="634.118" type="appl" />
         <tli id="T749" time="634.912" type="appl" />
         <tli id="T750" time="635.706" type="appl" />
         <tli id="T751" time="640.8165567745287" />
         <tli id="T752" time="641.557" type="appl" />
         <tli id="T753" time="642.172" type="appl" />
         <tli id="T754" time="642.787" type="appl" />
         <tli id="T755" time="643.402" type="appl" />
         <tli id="T756" time="644.017" type="appl" />
         <tli id="T757" time="647.4431878350854" />
         <tli id="T758" time="648.138" type="appl" />
         <tli id="T759" time="648.866" type="appl" />
         <tli id="T760" time="649.594" type="appl" />
         <tli id="T761" time="650.323" type="appl" />
         <tli id="T762" time="651.051" type="appl" />
         <tli id="T763" time="651.9431636558859" />
         <tli id="T764" time="652.87" type="appl" />
         <tli id="T765" time="653.52" type="appl" />
         <tli id="T766" time="654.17" type="appl" />
         <tli id="T767" time="654.82" type="appl" />
         <tli id="T768" time="655.47" type="appl" />
         <tli id="T769" time="656.12" type="appl" />
         <tli id="T770" time="657.3631345333835" />
         <tli id="T771" time="658.237" type="appl" />
         <tli id="T772" time="658.903" type="appl" />
         <tli id="T773" time="659.57" type="appl" />
         <tli id="T774" time="660.237" type="appl" />
         <tli id="T775" time="660.903" type="appl" />
         <tli id="T776" time="661.57" type="appl" />
         <tli id="T777" time="662.237" type="appl" />
         <tli id="T778" time="662.903" type="appl" />
         <tli id="T779" time="663.57" type="appl" />
         <tli id="T780" time="664.132" type="appl" />
         <tli id="T781" time="664.694" type="appl" />
         <tli id="T782" time="665.256" type="appl" />
         <tli id="T783" time="665.818" type="appl" />
         <tli id="T784" time="666.38" type="appl" />
         <tli id="T785" time="667.349" type="appl" />
         <tli id="T786" time="668.208" type="appl" />
         <tli id="T787" time="669.066" type="appl" />
         <tli id="T788" time="669.925" type="appl" />
         <tli id="T789" time="670.784" type="appl" />
         <tli id="T790" time="671.642" type="appl" />
         <tli id="T791" time="672.501" type="appl" />
         <tli id="T792" time="675.3563711857397" />
         <tli id="T793" time="676.196" type="appl" />
         <tli id="T794" time="676.961" type="appl" />
         <tli id="T795" time="677.727" type="appl" />
         <tli id="T796" time="678.492" type="appl" />
         <tli id="T797" time="679.258" type="appl" />
         <tli id="T798" time="680.023" type="appl" />
         <tli id="T799" time="680.789" type="appl" />
         <tli id="T800" time="681.554" type="appl" />
         <tli id="T801" time="683.6163268034759" />
         <tli id="T802" time="684.635" type="appl" />
         <tli id="T803" time="685.44" type="appl" />
         <tli id="T804" time="686.245" type="appl" />
         <tli id="T805" time="687.05" type="appl" />
         <tli id="T806" time="687.855" type="appl" />
         <tli id="T807" time="688.66" type="appl" />
         <tli id="T808" time="689.363" type="appl" />
         <tli id="T809" time="690.047" type="appl" />
         <tli id="T810" time="691.3496185844072" />
         <tli id="T811" time="692.273" type="appl" />
         <tli id="T812" time="693.226" type="appl" />
         <tli id="T813" time="694.179" type="appl" />
         <tli id="T814" time="695.132" type="appl" />
         <tli id="T815" time="696.085" type="appl" />
         <tli id="T816" time="697.038" type="appl" />
         <tli id="T817" time="697.991" type="appl" />
         <tli id="T818" time="698.944" type="appl" />
         <tli id="T819" time="699.897" type="appl" />
         <tli id="T820" time="700.85" type="appl" />
         <tli id="T821" time="701.587" type="appl" />
         <tli id="T822" time="702.283" type="appl" />
         <tli id="T823" time="703.2133569216876" />
         <tli id="T824" time="704.096" type="appl" />
         <tli id="T825" time="705.051" type="appl" />
         <tli id="T826" time="706.007" type="appl" />
         <tli id="T827" time="706.963" type="appl" />
         <tli id="T828" time="707.919" type="appl" />
         <tli id="T829" time="708.874" type="appl" />
         <tli id="T830" time="710.8561804387216" />
         <tli id="T831" time="711.576" type="appl" />
         <tli id="T832" time="712.422" type="appl" />
         <tli id="T833" time="713.267" type="appl" />
         <tli id="T834" time="714.113" type="appl" />
         <tli id="T835" time="714.959" type="appl" />
         <tli id="T836" time="715.804" type="appl" />
         <tli id="T837" time="716.65" type="appl" />
         <tli id="T838" time="717.496" type="appl" />
         <tli id="T839" time="718.563" type="appl" />
         <tli id="T840" time="719.397" type="appl" />
         <tli id="T841" time="720.23" type="appl" />
         <tli id="T842" time="721.063" type="appl" />
         <tli id="T843" time="721.897" type="appl" />
         <tli id="T844" time="724.3494412702775" />
         <tli id="T845" time="725.192" type="appl" />
         <tli id="T846" time="725.864" type="appl" />
         <tli id="T847" time="726.536" type="appl" />
         <tli id="T848" time="727.208" type="appl" />
         <tli id="T849" time="727.88" type="appl" />
         <tli id="T850" time="728.602" type="appl" />
         <tli id="T851" time="729.13" type="appl" />
         <tli id="T852" time="729.658" type="appl" />
         <tli id="T853" time="730.185" type="appl" />
         <tli id="T854" time="731.253" type="appl" />
         <tli id="T855" time="731.895" type="appl" />
         <tli id="T856" time="732.538" type="appl" />
         <tli id="T857" time="733.181" type="appl" />
         <tli id="T858" time="733.824" type="appl" />
         <tli id="T859" time="734.466" type="appl" />
         <tli id="T860" time="735.109" type="appl" />
         <tli id="T861" time="735.752" type="appl" />
         <tli id="T862" time="736.395" type="appl" />
         <tli id="T863" time="737.037" type="appl" />
         <tli id="T864" time="739.4293602430934" />
         <tli id="T865" time="740.325" type="appl" />
         <tli id="T866" time="741.01" type="appl" />
         <tli id="T867" time="741.695" type="appl" />
         <tli id="T868" time="742.38" type="appl" />
         <tli id="T869" time="743.065" type="appl" />
         <tli id="T870" time="743.7966805266766" />
         <tli id="T871" time="744.215" type="appl" />
         <tli id="T872" time="744.67" type="appl" />
         <tli id="T873" time="745.125" type="appl" />
         <tli id="T874" time="746.5426553553809" />
         <tli id="T875" time="747.63" type="appl" />
         <tli id="T876" time="748.71" type="appl" />
         <tli id="T877" time="749.795" type="appl" />
         <tli id="T878" time="750.845" type="appl" />
         <tli id="T879" time="752.962620859723" />
         <tli id="T880" time="753.767" type="appl" />
         <tli id="T881" time="754.39" type="appl" />
         <tli id="T882" time="754.984" type="appl" />
         <tli id="T883" time="755.578" type="appl" />
         <tli id="T884" time="756.172" type="appl" />
         <tli id="T885" time="756.766" type="appl" />
         <tli id="T886" time="757.3559305869934" />
         <tli id="T1055" time="759.0292549292466" />
         <tli id="T887" time="759.652" type="appl" />
         <tli id="T888" time="760.455" type="appl" />
         <tli id="T889" time="761.257" type="appl" />
         <tli id="T890" time="762.06" type="appl" />
         <tli id="T891" time="763.032" type="appl" />
         <tli id="T892" time="763.775" type="appl" />
         <tli id="T893" time="764.517" type="appl" />
         <tli id="T894" time="765.26" type="appl" />
         <tli id="T895" time="766.002" type="appl" />
         <tli id="T896" time="766.745" type="appl" />
         <tli id="T897" time="767.487" type="appl" />
         <tli id="T898" time="768.23" type="appl" />
         <tli id="T899" time="769.3520119631705" />
         <tli id="T900" time="770.192" type="appl" />
         <tli id="T901" time="771.103" type="appl" />
         <tli id="T902" time="772.015" type="appl" />
         <tli id="T903" time="772.927" type="appl" />
         <tli id="T904" time="773.838" type="appl" />
         <tli id="T905" time="774.8766697781657" />
         <tli id="T906" time="775.544" type="appl" />
         <tli id="T907" time="776.199" type="appl" />
         <tli id="T908" time="776.853" type="appl" />
         <tli id="T909" time="777.508" type="appl" />
         <tli id="T910" time="778.162" type="appl" />
         <tli id="T911" time="778.817" type="appl" />
         <tli id="T912" time="779.471" type="appl" />
         <tli id="T913" time="780.126" type="appl" />
         <tli id="T914" time="780.78" type="appl" />
         <tli id="T915" time="781.418" type="appl" />
         <tli id="T916" time="781.92" type="appl" />
         <tli id="T917" time="782.423" type="appl" />
         <tli id="T918" time="782.926" type="appl" />
         <tli id="T919" time="783.429" type="appl" />
         <tli id="T920" time="783.931" type="appl" />
         <tli id="T921" time="784.434" type="appl" />
         <tli id="T1058" time="784.6495714285714" type="intp" />
         <tli id="T922" time="784.937" type="appl" />
         <tli id="T923" time="785.44" type="appl" />
         <tli id="T924" time="785.942" type="appl" />
         <tli id="T925" time="788.495763266933" />
         <tli id="T926" time="789.665" type="appl" />
         <tli id="T927" time="790.511" type="appl" />
         <tli id="T928" time="791.356" type="appl" />
         <tli id="T929" time="792.202" type="appl" />
         <tli id="T930" time="793.047" type="appl" />
         <tli id="T931" time="793.893" type="appl" />
         <tli id="T932" time="794.738" type="appl" />
         <tli id="T933" time="795.584" type="appl" />
         <tli id="T934" time="796.429" type="appl" />
         <tli id="T935" time="797.275" type="appl" />
         <tli id="T936" time="798.266648266167" />
         <tli id="T937" time="799.073" type="appl" />
         <tli id="T938" time="799.777" type="appl" />
         <tli id="T939" time="802.129023346247" />
         <tli id="T940" time="802.999" type="appl" />
         <tli id="T941" time="803.827" type="appl" />
         <tli id="T942" time="804.656" type="appl" />
         <tli id="T943" time="805.485" type="appl" />
         <tli id="T944" time="806.313" type="appl" />
         <tli id="T945" time="807.142" type="appl" />
         <tli id="T946" time="807.97" type="appl" />
         <tli id="T947" time="808.799" type="appl" />
         <tli id="T948" time="809.628" type="appl" />
         <tli id="T949" time="810.456" type="appl" />
         <tli id="T950" time="811.4516815872194" />
         <tli id="T951" time="812.315" type="appl" />
         <tli id="T952" time="813.17" type="appl" />
         <tli id="T953" time="814.025" type="appl" />
         <tli id="T954" time="815.2399945652488" />
         <tli id="T955" time="816.925" type="appl" />
         <tli id="T956" time="818.62" type="appl" />
         <tli id="T957" time="820.5222578493413" />
         <tli id="T958" time="821.436" type="appl" />
         <tli id="T959" time="822.183" type="appl" />
         <tli id="T960" time="822.93" type="appl" />
         <tli id="T961" time="823.676" type="appl" />
         <tli id="T962" time="824.422" type="appl" />
         <tli id="T963" time="825.169" type="appl" />
         <tli id="T964" time="825.916" type="appl" />
         <tli id="T965" time="828.4488819247958" />
         <tli id="T966" time="829.219" type="appl" />
         <tli id="T967" time="829.948" type="appl" />
         <tli id="T968" time="830.676" type="appl" />
         <tli id="T969" time="831.405" type="appl" />
         <tli id="T970" time="832.134" type="appl" />
         <tli id="T971" time="832.863" type="appl" />
         <tli id="T972" time="833.591" type="appl" />
         <tli id="T973" time="834.32" type="appl" />
         <tli id="T974" time="835.3688447425602" />
         <tli id="T975" time="836.202" type="appl" />
         <tli id="T976" time="836.863" type="appl" />
         <tli id="T977" time="837.525" type="appl" />
         <tli id="T978" time="838.187" type="appl" />
         <tli id="T979" time="838.848" type="appl" />
         <tli id="T980" time="839.9421535026626" />
         <tli id="T981" time="841.17" type="appl" />
         <tli id="T982" time="842.19" type="appl" />
         <tli id="T983" time="844.6221283562951" />
         <tli id="T984" time="845.458" type="appl" />
         <tli id="T985" time="846.176" type="appl" />
         <tli id="T986" time="846.894" type="appl" />
         <tli id="T987" time="847.612" type="appl" />
         <tli id="T988" time="848.33" type="appl" />
         <tli id="T989" time="848.898" type="appl" />
         <tli id="T990" time="849.455" type="appl" />
         <tli id="T991" time="850.013" type="appl" />
         <tli id="T992" time="850.57" type="appl" />
         <tli id="T993" time="851.128" type="appl" />
         <tli id="T994" time="851.686" type="appl" />
         <tli id="T995" time="852.243" type="appl" />
         <tli id="T996" time="854.4287423302173" />
         <tli id="T997" time="855.442" type="appl" />
         <tli id="T998" time="856.434" type="appl" />
         <tli id="T999" time="857.426" type="appl" />
         <tli id="T1000" time="858.419" type="appl" />
         <tli id="T1001" time="859.411" type="appl" />
         <tli id="T1002" time="861.4687045032031" />
         <tli id="T1003" time="862.167" type="appl" />
         <tli id="T1004" time="862.814" type="appl" />
         <tli id="T1005" time="863.5533287187995" />
         <tli id="T1006" time="864.52" type="appl" />
         <tli id="T1007" time="865.7449836093004" />
         <tli id="T1008" time="866.765" type="appl" />
         <tli id="T1009" time="867.625" type="appl" />
         <tli id="T1010" time="868.4916876007537" />
         <tli id="T1011" time="870.06" type="appl" />
         <tli id="T1012" time="876.421957489952" />
         <tli id="T1013" time="877.67" type="appl" />
         <tli id="T1014" time="878.87" type="appl" />
         <tli id="T1015" time="880.108" type="appl" />
         <tli id="T1016" time="881.065" type="appl" />
         <tli id="T1017" time="882.022" type="appl" />
         <tli id="T1056" time="882.5352579753951" />
         <tli id="T1018" time="884.3152484111783" />
         <tli id="T1019" time="885.91" type="appl" />
         <tli id="T1020" time="886.792" type="appl" />
         <tli id="T1021" time="887.43" type="appl" />
         <tli id="T1022" time="888.068" type="appl" />
         <tli id="T1023" time="888.705" type="appl" />
         <tli id="T1024" time="889.342" type="appl" />
         <tli id="T1025" time="889.98" type="appl" />
         <tli id="T1026" time="890.618" type="appl" />
         <tli id="T1027" time="893.0552014497997" />
         <tli id="T1028" time="893.92" type="appl" />
         <tli id="T1029" time="894.69" type="appl" />
         <tli id="T1030" time="895.46" type="appl" />
         <tli id="T1031" time="896.23" type="appl" />
         <tli id="T1032" time="897.0" type="appl" />
         <tli id="T1033" time="897.77" type="appl" />
         <tli id="T1034" time="899.38" type="appl" />
         <tli id="T1035" time="900.965" type="appl" />
         <tli id="T1036" time="901.645" type="appl" />
         <tli id="T1037" time="902.275" type="appl" />
         <tli id="T1038" time="902.905" type="appl" />
         <tli id="T1039" time="903.535" type="appl" />
         <tli id="T1040" time="904.165" type="appl" />
         <tli id="T1041" time="905.0884701261625" />
         <tli id="T1042" time="906.035" type="appl" />
         <tli id="T1043" time="906.96" type="appl" />
         <tli id="T1044" time="907.885" type="appl" />
         <tli id="T1045" time="908.81" type="appl" />
         <tli id="T1046" time="909.735" type="appl" />
         <tli id="T1047" time="910.68" type="appl" />
         <tli id="T1048" time="914.7150850672529" />
         <tli id="T1049" time="915.527" type="appl" />
         <tli id="T1050" time="916.245" type="appl" />
         <tli id="T1051" time="916.962" type="appl" />
         <tli id="T1052" time="917.68" type="appl" />
         <tli id="T1053" time="918.338" type="appl" />
         <tli id="T1054" time="918.915" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T317" start="T315">
            <tli id="T315.tx.1" />
         </timeline-fork>
         <timeline-fork end="T581" start="T580">
            <tli id="T580.tx.1" />
         </timeline-fork>
         <timeline-fork end="T707" start="T706">
            <tli id="T706.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T1054" id="Seg_0" n="sc" s="T0">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Tăn</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6" n="HIAT:ip">(</nts>
                  <ts e="T2" id="Seg_8" n="HIAT:w" s="T1">taldʼen</ts>
                  <nts id="Seg_9" n="HIAT:ip">)</nts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_11" n="HIAT:ip">(</nts>
                  <ts e="T3" id="Seg_13" n="HIAT:w" s="T2">ka-</ts>
                  <nts id="Seg_14" n="HIAT:ip">)</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_17" n="HIAT:w" s="T3">kajaʔ</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_20" n="HIAT:w" s="T4">ibiel</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_23" n="HIAT:w" s="T5">dăk</ts>
                  <nts id="Seg_24" n="HIAT:ip">,</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_27" n="HIAT:w" s="T6">edəbiel</ts>
                  <nts id="Seg_28" n="HIAT:ip">?</nts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_31" n="HIAT:u" s="T7">
                  <nts id="Seg_32" n="HIAT:ip">(</nts>
                  <ts e="T8" id="Seg_34" n="HIAT:w" s="T7">Edəmiel-</ts>
                  <nts id="Seg_35" n="HIAT:ip">)</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_38" n="HIAT:w" s="T8">Edəbiem</ts>
                  <nts id="Seg_39" n="HIAT:ip">.</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_42" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_44" n="HIAT:w" s="T9">A</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_47" n="HIAT:w" s="T10">kumən</ts>
                  <nts id="Seg_48" n="HIAT:ip">?</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_51" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_53" n="HIAT:w" s="T11">Sumna</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_56" n="HIAT:w" s="T12">gram</ts>
                  <nts id="Seg_57" n="HIAT:ip">.</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_60" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_62" n="HIAT:w" s="T13">Aktʼa</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_65" n="HIAT:w" s="T14">deʔpiel</ts>
                  <nts id="Seg_66" n="HIAT:ip">?</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_69" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_71" n="HIAT:w" s="T15">Deʔpiem</ts>
                  <nts id="Seg_72" n="HIAT:ip">.</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_75" n="HIAT:u" s="T16">
                  <nts id="Seg_76" n="HIAT:ip">(</nts>
                  <ts e="T17" id="Seg_78" n="HIAT:w" s="T16">Šide</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_81" n="HIAT:w" s="T17">šăl-</ts>
                  <nts id="Seg_82" n="HIAT:ip">)</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_85" n="HIAT:w" s="T18">Šide</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_88" n="HIAT:w" s="T19">šălkovej</ts>
                  <nts id="Seg_89" n="HIAT:ip">.</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_92" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_94" n="HIAT:w" s="T20">Dʼăbaktərlaʔbəbaʔ</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_97" n="HIAT:w" s="T21">i</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_100" n="HIAT:w" s="T22">kaknarlaʔbəbaʔ</ts>
                  <nts id="Seg_101" n="HIAT:ip">.</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_104" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_106" n="HIAT:w" s="T23">I</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_108" n="HIAT:ip">(</nts>
                  <ts e="T25" id="Seg_110" n="HIAT:w" s="T24">arla-</ts>
                  <nts id="Seg_111" n="HIAT:ip">)</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_114" n="HIAT:w" s="T25">ara</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_117" n="HIAT:w" s="T26">bĭtleʔbəbeʔ</ts>
                  <nts id="Seg_118" n="HIAT:ip">.</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_121" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_123" n="HIAT:w" s="T27">I</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_126" n="HIAT:w" s="T28">ipek</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_129" n="HIAT:w" s="T29">amnia</ts>
                  <nts id="Seg_130" n="HIAT:ip">.</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_133" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_135" n="HIAT:w" s="T30">I</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_138" n="HIAT:w" s="T31">uja</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_141" n="HIAT:w" s="T32">amnaʔbəbaʔ</ts>
                  <nts id="Seg_142" n="HIAT:ip">.</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_145" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_147" n="HIAT:w" s="T33">I</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_150" n="HIAT:w" s="T34">taŋgu</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_153" n="HIAT:w" s="T35">nʼeʔleʔbəbeʔ</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_156" n="HIAT:w" s="T36">bar</ts>
                  <nts id="Seg_157" n="HIAT:ip">.</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_160" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_162" n="HIAT:w" s="T37">Miʔ</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_165" n="HIAT:w" s="T38">tondə</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_168" n="HIAT:w" s="T39">kuza</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_171" n="HIAT:w" s="T40">amnolaʔbə</ts>
                  <nts id="Seg_172" n="HIAT:ip">,</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_175" n="HIAT:w" s="T41">dĭn</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_178" n="HIAT:w" s="T42">nʼi</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_181" n="HIAT:w" s="T43">miʔnʼibeʔ</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_184" n="HIAT:w" s="T44">šobi</ts>
                  <nts id="Seg_185" n="HIAT:ip">.</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_188" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_190" n="HIAT:w" s="T45">Jašɨktə</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_192" n="HIAT:ip">(</nts>
                  <ts e="T47" id="Seg_194" n="HIAT:w" s="T46">păʔpi</ts>
                  <nts id="Seg_195" n="HIAT:ip">)</nts>
                  <nts id="Seg_196" n="HIAT:ip">,</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_199" n="HIAT:w" s="T47">i</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_202" n="HIAT:w" s="T48">škaptə</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_204" n="HIAT:ip">(</nts>
                  <ts e="T50" id="Seg_206" n="HIAT:w" s="T49">păʔpi</ts>
                  <nts id="Seg_207" n="HIAT:ip">)</nts>
                  <nts id="Seg_208" n="HIAT:ip">.</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_211" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_213" n="HIAT:w" s="T50">Măn</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_215" n="HIAT:ip">(</nts>
                  <ts e="T52" id="Seg_217" n="HIAT:w" s="T51">mămbiam=</ts>
                  <nts id="Seg_218" n="HIAT:ip">)</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_221" n="HIAT:w" s="T52">mămbiam:</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_224" n="HIAT:w" s="T53">Ĭmbi</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_226" n="HIAT:ip">(</nts>
                  <nts id="Seg_227" n="HIAT:ip">(</nts>
                  <ats e="T55" id="Seg_228" n="HIAT:non-pho" s="T54">…</ats>
                  <nts id="Seg_229" n="HIAT:ip">)</nts>
                  <nts id="Seg_230" n="HIAT:ip">)</nts>
                  <nts id="Seg_231" n="HIAT:ip">?</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_234" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_236" n="HIAT:w" s="T55">Măna</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_239" n="HIAT:w" s="T56">Vanʼka</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_242" n="HIAT:w" s="T57">велел</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_245" n="HIAT:w" s="T58">măndərzittə</ts>
                  <nts id="Seg_246" n="HIAT:ip">.</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_249" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_251" n="HIAT:w" s="T59">Măn</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_254" n="HIAT:w" s="T60">dĭm</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_257" n="HIAT:w" s="T61">sürerluʔpiem:</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_259" n="HIAT:ip">(</nts>
                  <ts e="T63" id="Seg_261" n="HIAT:w" s="T62">Măllia</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_264" n="HIAT:w" s="T63">Mišanə</ts>
                  <nts id="Seg_265" n="HIAT:ip">)</nts>
                  <nts id="Seg_266" n="HIAT:ip">,</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_269" n="HIAT:w" s="T64">kanaʔ</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_272" n="HIAT:w" s="T65">döʔə</ts>
                  <nts id="Seg_273" n="HIAT:ip">!</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_276" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_278" n="HIAT:w" s="T66">Dĭgəttə</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_281" n="HIAT:w" s="T67">teinen</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_284" n="HIAT:w" s="T68">šobi</ts>
                  <nts id="Seg_285" n="HIAT:ip">.</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_288" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_290" n="HIAT:w" s="T69">Bura</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_293" n="HIAT:w" s="T70">sanəzi</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_296" n="HIAT:w" s="T71">nuga</ts>
                  <nts id="Seg_297" n="HIAT:ip">,</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_300" n="HIAT:w" s="T72">dĭ</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_303" n="HIAT:w" s="T73">dagaj</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_306" n="HIAT:w" s="T74">ibi</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_309" n="HIAT:w" s="T75">i</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_312" n="HIAT:w" s="T76">bura</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_315" n="HIAT:w" s="T77">băʔpi</ts>
                  <nts id="Seg_316" n="HIAT:ip">.</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_319" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_321" n="HIAT:w" s="T78">Sanə</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_324" n="HIAT:w" s="T79">ibi</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_327" n="HIAT:w" s="T80">bostə</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_329" n="HIAT:ip">(</nts>
                  <ts e="T82" id="Seg_331" n="HIAT:w" s="T81">barəʔluʔpi</ts>
                  <nts id="Seg_332" n="HIAT:ip">)</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_335" n="HIAT:w" s="T82">sĭreʔpne</ts>
                  <nts id="Seg_336" n="HIAT:ip">.</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_339" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_341" n="HIAT:w" s="T83">Miʔ</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_344" n="HIAT:w" s="T84">turagən</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_346" n="HIAT:ip">(</nts>
                  <ts e="T86" id="Seg_348" n="HIAT:w" s="T85">ne</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_351" n="HIAT:w" s="T86">nuʔməluʔpi</ts>
                  <nts id="Seg_352" n="HIAT:ip">)</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_354" n="HIAT:ip">(</nts>
                  <ts e="T88" id="Seg_356" n="HIAT:w" s="T87">deš-</ts>
                  <nts id="Seg_357" n="HIAT:ip">)</nts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_359" n="HIAT:ip">(</nts>
                  <ts e="T89" id="Seg_361" n="HIAT:w" s="T88">dĭŋinaʔtə</ts>
                  <nts id="Seg_362" n="HIAT:ip">)</nts>
                  <nts id="Seg_363" n="HIAT:ip">.</nts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_366" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_368" n="HIAT:w" s="T89">Tojirbi</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_371" n="HIAT:w" s="T90">sanə</ts>
                  <nts id="Seg_372" n="HIAT:ip">.</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_375" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_377" n="HIAT:w" s="T91">A</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_380" n="HIAT:w" s="T92">dĭ</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_383" n="HIAT:w" s="T93">bar</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_386" n="HIAT:w" s="T94">pʼerluʔbə:</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_389" n="HIAT:w" s="T95">Dö</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_392" n="HIAT:w" s="T96">sanə</ts>
                  <nts id="Seg_393" n="HIAT:ip">.</nts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_396" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_398" n="HIAT:w" s="T97">A</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_401" n="HIAT:w" s="T98">dĭ</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_404" n="HIAT:w" s="T99">mămbi:</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_407" n="HIAT:w" s="T100">Măn</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_410" n="HIAT:w" s="T101">dĭm</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_413" n="HIAT:w" s="T102">kutlim</ts>
                  <nts id="Seg_414" n="HIAT:ip">!</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_417" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_419" n="HIAT:w" s="T103">A</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_421" n="HIAT:ip">(</nts>
                  <ts e="T105" id="Seg_423" n="HIAT:w" s="T104">ĭmbi</ts>
                  <nts id="Seg_424" n="HIAT:ip">)</nts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_427" n="HIAT:w" s="T105">ej</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_429" n="HIAT:ip">(</nts>
                  <ts e="T107" id="Seg_431" n="HIAT:w" s="T106">mămbileʔ</ts>
                  <nts id="Seg_432" n="HIAT:ip">)</nts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_435" n="HIAT:w" s="T107">раньше</ts>
                  <nts id="Seg_436" n="HIAT:ip">?</nts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_439" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_441" n="HIAT:w" s="T108">Taldʼen</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_444" n="HIAT:w" s="T109">măna</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_447" n="HIAT:w" s="T110">kăštəbiʔi</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_449" n="HIAT:ip">(</nts>
                  <ts e="T112" id="Seg_451" n="HIAT:w" s="T111">m-</ts>
                  <nts id="Seg_452" n="HIAT:ip">)</nts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_455" n="HIAT:w" s="T112">măn</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_457" n="HIAT:ip">(</nts>
                  <ts e="T114" id="Seg_459" n="HIAT:w" s="T113">tugan-</ts>
                  <nts id="Seg_460" n="HIAT:ip">)</nts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_463" n="HIAT:w" s="T114">tugandə:</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_466" n="HIAT:w" s="T115">Šoʔ</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_469" n="HIAT:w" s="T116">miʔnʼibeʔ</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_472" n="HIAT:w" s="T117">amorzittə</ts>
                  <nts id="Seg_473" n="HIAT:ip">.</nts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_476" n="HIAT:u" s="T118">
                  <nts id="Seg_477" n="HIAT:ip">(</nts>
                  <ts e="T119" id="Seg_479" n="HIAT:w" s="T118">Eššinʼ</ts>
                  <nts id="Seg_480" n="HIAT:ip">)</nts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_483" n="HIAT:w" s="T119">Ešši</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_485" n="HIAT:ip">(</nts>
                  <ts e="T121" id="Seg_487" n="HIAT:w" s="T120">ti-</ts>
                  <nts id="Seg_488" n="HIAT:ip">)</nts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_491" n="HIAT:w" s="T121">külambi</ts>
                  <nts id="Seg_492" n="HIAT:ip">,</nts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_494" n="HIAT:ip">(</nts>
                  <ts e="T123" id="Seg_496" n="HIAT:w" s="T122">ušonʼiʔkə</ts>
                  <nts id="Seg_497" n="HIAT:ip">)</nts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_500" n="HIAT:w" s="T123">kambi</ts>
                  <nts id="Seg_501" n="HIAT:ip">.</nts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_504" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_506" n="HIAT:w" s="T124">A</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_509" n="HIAT:w" s="T125">măn</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_512" n="HIAT:w" s="T126">mămbiam:</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_515" n="HIAT:w" s="T127">Šolam</ts>
                  <nts id="Seg_516" n="HIAT:ip">.</nts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_519" n="HIAT:u" s="T128">
                  <nts id="Seg_520" n="HIAT:ip">(</nts>
                  <ts e="T129" id="Seg_522" n="HIAT:w" s="T128">A</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_525" n="HIAT:w" s="T129">bos-</ts>
                  <nts id="Seg_526" n="HIAT:ip">)</nts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_529" n="HIAT:w" s="T130">A</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_532" n="HIAT:w" s="T131">bospə</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_535" n="HIAT:w" s="T132">ej</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_538" n="HIAT:w" s="T133">kambiam</ts>
                  <nts id="Seg_539" n="HIAT:ip">.</nts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_542" n="HIAT:u" s="T134">
                  <nts id="Seg_543" n="HIAT:ip">(</nts>
                  <ts e="T135" id="Seg_545" n="HIAT:w" s="T134">Meim</ts>
                  <nts id="Seg_546" n="HIAT:ip">)</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_549" n="HIAT:w" s="T135">kăštəbiam</ts>
                  <nts id="Seg_550" n="HIAT:ip">,</nts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_553" n="HIAT:w" s="T136">kăštəbiam</ts>
                  <nts id="Seg_554" n="HIAT:ip">,</nts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_557" n="HIAT:w" s="T137">dĭ</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_560" n="HIAT:w" s="T138">ej</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_563" n="HIAT:w" s="T139">kambi</ts>
                  <nts id="Seg_564" n="HIAT:ip">,</nts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_567" n="HIAT:w" s="T140">i</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_569" n="HIAT:ip">(</nts>
                  <ts e="T142" id="Seg_571" n="HIAT:w" s="T141">măn</ts>
                  <nts id="Seg_572" n="HIAT:ip">)</nts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_575" n="HIAT:w" s="T142">ej</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_578" n="HIAT:w" s="T143">kambiam</ts>
                  <nts id="Seg_579" n="HIAT:ip">.</nts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_582" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_584" n="HIAT:w" s="T144">Dĭn</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_587" n="HIAT:w" s="T145">ugandə</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_590" n="HIAT:w" s="T146">araj</ts>
                  <nts id="Seg_591" n="HIAT:ip">,</nts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_594" n="HIAT:w" s="T147">a</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_597" n="HIAT:w" s="T148">măna</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_600" n="HIAT:w" s="T149">ara</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_603" n="HIAT:w" s="T150">ej</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_606" n="HIAT:w" s="T151">kereʔ</ts>
                  <nts id="Seg_607" n="HIAT:ip">.</nts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_610" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_612" n="HIAT:w" s="T152">Măn</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_615" n="HIAT:w" s="T153">не</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_618" n="HIAT:w" s="T154">хочу</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_621" n="HIAT:w" s="T155">dĭm</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_624" n="HIAT:w" s="T156">bĭʔsittə</ts>
                  <nts id="Seg_625" n="HIAT:ip">.</nts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T166" id="Seg_628" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_630" n="HIAT:w" s="T157">A</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_633" n="HIAT:w" s="T158">dĭ</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_636" n="HIAT:w" s="T159">kuroluʔpi</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_639" n="HIAT:w" s="T160">bar</ts>
                  <nts id="Seg_640" n="HIAT:ip">,</nts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_643" n="HIAT:w" s="T161">măndə:</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_646" n="HIAT:w" s="T162">urgajam</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_649" n="HIAT:w" s="T163">ažnə</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_651" n="HIAT:ip">(</nts>
                  <ts e="T165" id="Seg_653" n="HIAT:w" s="T164">dʼoːdunʼi</ts>
                  <nts id="Seg_654" n="HIAT:ip">)</nts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_657" n="HIAT:w" s="T165">kubiam</ts>
                  <nts id="Seg_658" n="HIAT:ip">.</nts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_661" n="HIAT:u" s="T166">
                  <ts e="T167" id="Seg_663" n="HIAT:w" s="T166">Tüj</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_666" n="HIAT:w" s="T167">büzəjleʔ</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_669" n="HIAT:w" s="T168">bar</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_672" n="HIAT:w" s="T169">tüšəlbi</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_675" n="HIAT:w" s="T170">šiʔnʼileʔ</ts>
                  <nts id="Seg_676" n="HIAT:ip">.</nts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T176" id="Seg_679" n="HIAT:u" s="T171">
                  <ts e="T172" id="Seg_681" n="HIAT:w" s="T171">Iʔbəleʔbə</ts>
                  <nts id="Seg_682" n="HIAT:ip">,</nts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_685" n="HIAT:w" s="T172">amnoʔ</ts>
                  <nts id="Seg_686" n="HIAT:ip">,</nts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_689" n="HIAT:w" s="T173">iʔbəleʔbə</ts>
                  <nts id="Seg_690" n="HIAT:ip">,</nts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_693" n="HIAT:w" s="T174">ej</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_696" n="HIAT:w" s="T175">kirgarlaʔbə</ts>
                  <nts id="Seg_697" n="HIAT:ip">.</nts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_700" n="HIAT:u" s="T176">
                  <ts e="T177" id="Seg_702" n="HIAT:w" s="T176">Teinen</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_705" n="HIAT:w" s="T177">ejü</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_708" n="HIAT:w" s="T178">molaːmbi</ts>
                  <nts id="Seg_709" n="HIAT:ip">.</nts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T183" id="Seg_712" n="HIAT:u" s="T179">
                  <ts e="T180" id="Seg_714" n="HIAT:w" s="T179">Sĭre</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_717" n="HIAT:w" s="T180">bar</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_719" n="HIAT:ip">(</nts>
                  <ts e="T182" id="Seg_721" n="HIAT:w" s="T181">nömrunʼiʔ</ts>
                  <nts id="Seg_722" n="HIAT:ip">)</nts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_725" n="HIAT:w" s="T182">ibi</ts>
                  <nts id="Seg_726" n="HIAT:ip">.</nts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_729" n="HIAT:u" s="T183">
                  <ts e="T184" id="Seg_731" n="HIAT:w" s="T183">Sĭre</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_733" n="HIAT:ip">(</nts>
                  <ts e="T185" id="Seg_735" n="HIAT:w" s="T184">buž-</ts>
                  <nts id="Seg_736" n="HIAT:ip">)</nts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_739" n="HIAT:w" s="T185">büžü</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_742" n="HIAT:w" s="T186">bü</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_745" n="HIAT:w" s="T187">molalləj</ts>
                  <nts id="Seg_746" n="HIAT:ip">.</nts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T191" id="Seg_749" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_751" n="HIAT:w" s="T188">Dĭgəttə</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_754" n="HIAT:w" s="T189">noʔ</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_757" n="HIAT:w" s="T190">özerləj</ts>
                  <nts id="Seg_758" n="HIAT:ip">.</nts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_761" n="HIAT:u" s="T191">
                  <ts e="T192" id="Seg_763" n="HIAT:w" s="T191">Всякий</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_766" n="HIAT:w" s="T192">svʼetogəʔi</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_769" n="HIAT:w" s="T193">özerləʔi</ts>
                  <nts id="Seg_770" n="HIAT:ip">.</nts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_773" n="HIAT:u" s="T194">
                  <ts e="T195" id="Seg_775" n="HIAT:w" s="T194">Paʔi</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_778" n="HIAT:w" s="T195">bar</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_781" n="HIAT:w" s="T196">kuvas</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_784" n="HIAT:w" s="T197">moləʔi</ts>
                  <nts id="Seg_785" n="HIAT:ip">.</nts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T201" id="Seg_788" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_790" n="HIAT:w" s="T198">Zʼelʼonaʔi</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_793" n="HIAT:w" s="T199">moləʔi</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_796" n="HIAT:w" s="T200">bar</ts>
                  <nts id="Seg_797" n="HIAT:ip">.</nts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T208" id="Seg_800" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_802" n="HIAT:w" s="T201">Dĭgəttə</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_805" n="HIAT:w" s="T202">ineʔi</ts>
                  <nts id="Seg_806" n="HIAT:ip">,</nts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_809" n="HIAT:w" s="T203">tüžöjəʔi</ts>
                  <nts id="Seg_810" n="HIAT:ip">,</nts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_813" n="HIAT:w" s="T204">ulardə</ts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_816" n="HIAT:w" s="T205">kalaʔi</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_819" n="HIAT:w" s="T206">noʔ</ts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_821" n="HIAT:ip">(</nts>
                  <ts e="T208" id="Seg_823" n="HIAT:w" s="T207">amzittə</ts>
                  <nts id="Seg_824" n="HIAT:ip">)</nts>
                  <nts id="Seg_825" n="HIAT:ip">.</nts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_828" n="HIAT:u" s="T208">
                  <ts e="T209" id="Seg_830" n="HIAT:w" s="T208">Dĭgəttə</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_833" n="HIAT:w" s="T209">noʔ</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_836" n="HIAT:w" s="T210">urgo</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_839" n="HIAT:w" s="T211">özerləj</ts>
                  <nts id="Seg_840" n="HIAT:ip">.</nts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_843" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_845" n="HIAT:w" s="T212">Dĭgəttə</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_848" n="HIAT:w" s="T213">dĭm</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_850" n="HIAT:ip">(</nts>
                  <ts e="T215" id="Seg_852" n="HIAT:w" s="T214">šapkosi-</ts>
                  <nts id="Seg_853" n="HIAT:ip">)</nts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_856" n="HIAT:w" s="T215">šapkozi</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_859" n="HIAT:w" s="T216">jaʔsittə</ts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_862" n="HIAT:w" s="T217">nada</ts>
                  <nts id="Seg_863" n="HIAT:ip">.</nts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_866" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_868" n="HIAT:w" s="T218">Dĭgəttə</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_871" n="HIAT:w" s="T219">kujagən</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_874" n="HIAT:w" s="T220">koləj</ts>
                  <nts id="Seg_875" n="HIAT:ip">,</nts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_878" n="HIAT:w" s="T221">da</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_881" n="HIAT:w" s="T222">oʔbdəsʼtə</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_884" n="HIAT:w" s="T223">nada</ts>
                  <nts id="Seg_885" n="HIAT:ip">.</nts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T228" id="Seg_888" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_890" n="HIAT:w" s="T224">Il</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_892" n="HIAT:ip">(</nts>
                  <ts e="T226" id="Seg_894" n="HIAT:w" s="T225">mĭnliaʔi</ts>
                  <nts id="Seg_895" n="HIAT:ip">)</nts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_898" n="HIAT:w" s="T226">ugandə</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_901" n="HIAT:w" s="T227">jakše</ts>
                  <nts id="Seg_902" n="HIAT:ip">.</nts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T230" id="Seg_905" n="HIAT:u" s="T228">
                  <ts e="T229" id="Seg_907" n="HIAT:w" s="T228">Pʼe</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_910" n="HIAT:w" s="T229">šobi</ts>
                  <nts id="Seg_911" n="HIAT:ip">.</nts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T233" id="Seg_914" n="HIAT:u" s="T230">
                  <ts e="T231" id="Seg_916" n="HIAT:w" s="T230">Ugandə</ts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_918" n="HIAT:ip">(</nts>
                  <ts e="T232" id="Seg_920" n="HIAT:w" s="T231">ej-</ts>
                  <nts id="Seg_921" n="HIAT:ip">)</nts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_924" n="HIAT:w" s="T232">ejü</ts>
                  <nts id="Seg_925" n="HIAT:ip">.</nts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T237" id="Seg_928" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_930" n="HIAT:w" s="T233">Măn</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_932" n="HIAT:ip">(</nts>
                  <ts e="T235" id="Seg_934" n="HIAT:w" s="T234">dönüli</ts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_937" n="HIAT:w" s="T235">mobiam</ts>
                  <nts id="Seg_938" n="HIAT:ip">)</nts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_941" n="HIAT:w" s="T236">kunolzittə</ts>
                  <nts id="Seg_942" n="HIAT:ip">.</nts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T240" id="Seg_945" n="HIAT:u" s="T237">
                  <ts e="T238" id="Seg_947" n="HIAT:w" s="T237">Dĭgəttə</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_950" n="HIAT:w" s="T238">ertə</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_953" n="HIAT:w" s="T239">uʔbdəbiam</ts>
                  <nts id="Seg_954" n="HIAT:ip">.</nts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T245" id="Seg_957" n="HIAT:u" s="T240">
                  <ts e="T241" id="Seg_959" n="HIAT:w" s="T240">A</ts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_961" n="HIAT:ip">(</nts>
                  <ts e="T242" id="Seg_963" n="HIAT:w" s="T241">dʼala=</ts>
                  <nts id="Seg_964" n="HIAT:ip">)</nts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_967" n="HIAT:w" s="T242">dʼalam</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_970" n="HIAT:w" s="T243">bar</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_973" n="HIAT:w" s="T244">togonorbiam</ts>
                  <nts id="Seg_974" n="HIAT:ip">.</nts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T252" id="Seg_977" n="HIAT:u" s="T245">
                  <ts e="T246" id="Seg_979" n="HIAT:w" s="T245">Tüžöj</ts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_982" n="HIAT:w" s="T246">surdobiam</ts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_985" n="HIAT:w" s="T247">da</ts>
                  <nts id="Seg_986" n="HIAT:ip">,</nts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_989" n="HIAT:w" s="T248">mĭnzərbiem</ts>
                  <nts id="Seg_990" n="HIAT:ip">,</nts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_993" n="HIAT:w" s="T249">abiam</ts>
                  <nts id="Seg_994" n="HIAT:ip">,</nts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_997" n="HIAT:w" s="T250">dĭgəttə</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_1000" n="HIAT:w" s="T251">dʼăbaktərbiam</ts>
                  <nts id="Seg_1001" n="HIAT:ip">.</nts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T257" id="Seg_1004" n="HIAT:u" s="T252">
                  <ts e="T253" id="Seg_1006" n="HIAT:w" s="T252">Ertə</ts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_1009" n="HIAT:w" s="T253">oʔbdəbiam</ts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_1012" n="HIAT:w" s="T254">da</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_1015" n="HIAT:w" s="T255">šü</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_1018" n="HIAT:w" s="T256">nendəbiem</ts>
                  <nts id="Seg_1019" n="HIAT:ip">.</nts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T266" id="Seg_1022" n="HIAT:u" s="T257">
                  <ts e="T258" id="Seg_1024" n="HIAT:w" s="T257">Măna</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_1027" n="HIAT:w" s="T258">Kazan</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_1030" n="HIAT:w" s="T259">turanə</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_1033" n="HIAT:w" s="T260">nada</ts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1036" n="HIAT:w" s="T261">kanzittə</ts>
                  <nts id="Seg_1037" n="HIAT:ip">,</nts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1039" n="HIAT:ip">(</nts>
                  <ts e="T263" id="Seg_1041" n="HIAT:w" s="T262">măna</ts>
                  <nts id="Seg_1042" n="HIAT:ip">)</nts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1045" n="HIAT:w" s="T263">aktʼa</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1048" n="HIAT:w" s="T264">iʔbolaʔbə</ts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1051" n="HIAT:w" s="T265">bankan</ts>
                  <nts id="Seg_1052" n="HIAT:ip">.</nts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T274" id="Seg_1055" n="HIAT:u" s="T266">
                  <ts e="T267" id="Seg_1057" n="HIAT:w" s="T266">Izittə</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1060" n="HIAT:w" s="T267">nada</ts>
                  <nts id="Seg_1061" n="HIAT:ip">,</nts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1064" n="HIAT:w" s="T268">da</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1067" n="HIAT:w" s="T269">ildə</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1070" n="HIAT:w" s="T270">mĭzittə</ts>
                  <nts id="Seg_1071" n="HIAT:ip">,</nts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1074" n="HIAT:w" s="T271">măn</ts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1076" n="HIAT:ip">(</nts>
                  <ts e="T273" id="Seg_1078" n="HIAT:w" s="T272">dĭzeŋgən</ts>
                  <nts id="Seg_1079" n="HIAT:ip">)</nts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1082" n="HIAT:w" s="T273">ibiem</ts>
                  <nts id="Seg_1083" n="HIAT:ip">.</nts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T281" id="Seg_1086" n="HIAT:u" s="T274">
                  <ts e="T275" id="Seg_1088" n="HIAT:w" s="T274">Măn</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1091" n="HIAT:w" s="T275">iʔgö</ts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1094" n="HIAT:w" s="T276">dʼăbaktəriam</ts>
                  <nts id="Seg_1095" n="HIAT:ip">,</nts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1097" n="HIAT:ip">(</nts>
                  <ts e="T278" id="Seg_1099" n="HIAT:w" s="T277">tu-</ts>
                  <nts id="Seg_1100" n="HIAT:ip">)</nts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1103" n="HIAT:w" s="T278">tüj</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1105" n="HIAT:ip">(</nts>
                  <ts e="T280" id="Seg_1107" n="HIAT:w" s="T279">dʼăbaktər-</ts>
                  <nts id="Seg_1108" n="HIAT:ip">)</nts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1111" n="HIAT:w" s="T280">dʼăbaktərlaʔbəʔjə</ts>
                  <nts id="Seg_1112" n="HIAT:ip">.</nts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T284" id="Seg_1115" n="HIAT:u" s="T281">
                  <ts e="T282" id="Seg_1117" n="HIAT:w" s="T281">Ugandə</ts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1120" n="HIAT:w" s="T282">tăn</ts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1123" n="HIAT:w" s="T283">koloʔsəbi</ts>
                  <nts id="Seg_1124" n="HIAT:ip">.</nts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T287" id="Seg_1127" n="HIAT:u" s="T284">
                  <nts id="Seg_1128" n="HIAT:ip">(</nts>
                  <ts e="T285" id="Seg_1130" n="HIAT:w" s="T284">Koʔbdo</ts>
                  <nts id="Seg_1131" n="HIAT:ip">)</nts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1134" n="HIAT:w" s="T285">üge</ts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1137" n="HIAT:w" s="T286">măna</ts>
                  <nts id="Seg_1138" n="HIAT:ip">.</nts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T290" id="Seg_1141" n="HIAT:u" s="T287">
                  <ts e="T288" id="Seg_1143" n="HIAT:w" s="T287">Măna</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1146" n="HIAT:w" s="T288">nada</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1149" n="HIAT:w" s="T289">dʼăbaktərzittə</ts>
                  <nts id="Seg_1150" n="HIAT:ip">.</nts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1153" n="HIAT:u" s="T290">
                  <ts e="T291" id="Seg_1155" n="HIAT:w" s="T290">Tüj</ts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1158" n="HIAT:w" s="T291">tăn</ts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1161" n="HIAT:w" s="T292">dʼăbaktəraʔ</ts>
                  <nts id="Seg_1162" n="HIAT:ip">,</nts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1165" n="HIAT:w" s="T293">a</ts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1168" n="HIAT:w" s="T294">măn</ts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1171" n="HIAT:w" s="T295">ej</ts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1174" n="HIAT:w" s="T296">dʼăbaktərlam</ts>
                  <nts id="Seg_1175" n="HIAT:ip">.</nts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T305" id="Seg_1178" n="HIAT:u" s="T297">
                  <nts id="Seg_1179" n="HIAT:ip">(</nts>
                  <ts e="T298" id="Seg_1181" n="HIAT:w" s="T297">Măj-</ts>
                  <nts id="Seg_1182" n="HIAT:ip">)</nts>
                  <nts id="Seg_1183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1185" n="HIAT:w" s="T298">Măn</ts>
                  <nts id="Seg_1186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1188" n="HIAT:w" s="T299">ej</ts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1191" n="HIAT:w" s="T300">tĭmniem</ts>
                  <nts id="Seg_1192" n="HIAT:ip">,</nts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1195" n="HIAT:w" s="T301">ĭmbi</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1198" n="HIAT:w" s="T302">dʼăbaktərzittə</ts>
                  <nts id="Seg_1199" n="HIAT:ip">,</nts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1202" n="HIAT:w" s="T303">tăn</ts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1205" n="HIAT:w" s="T304">dʼăbaktəraʔ</ts>
                  <nts id="Seg_1206" n="HIAT:ip">.</nts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T317" id="Seg_1209" n="HIAT:u" s="T305">
                  <nts id="Seg_1210" n="HIAT:ip">(</nts>
                  <ts e="T306" id="Seg_1212" n="HIAT:w" s="T305">A</ts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1215" n="HIAT:w" s="T306">m-</ts>
                  <nts id="Seg_1216" n="HIAT:ip">)</nts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1219" n="HIAT:w" s="T307">A</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1222" n="HIAT:w" s="T308">măn</ts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1225" n="HIAT:w" s="T309">ĭmbi</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1228" n="HIAT:w" s="T310">üge</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1231" n="HIAT:w" s="T311">буду</ts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1233" n="HIAT:ip">(</nts>
                  <ts e="T313" id="Seg_1235" n="HIAT:w" s="T312">dʼăbaktərzittə</ts>
                  <nts id="Seg_1236" n="HIAT:ip">)</nts>
                  <nts id="Seg_1237" n="HIAT:ip">,</nts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1239" n="HIAT:ip">(</nts>
                  <ts e="T314" id="Seg_1241" n="HIAT:w" s="T313">iʔbəle-</ts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1244" n="HIAT:w" s="T314">iʔbola-</ts>
                  <nts id="Seg_1245" n="HIAT:ip">)</nts>
                  <nts id="Seg_1246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315.tx.1" id="Seg_1248" n="HIAT:w" s="T315">iʔbəlem</ts>
                  <nts id="Seg_1249" n="HIAT:ip">_</nts>
                  <ts e="T317" id="Seg_1251" n="HIAT:w" s="T315.tx.1">da</ts>
                  <nts id="Seg_1252" n="HIAT:ip">.</nts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T325" id="Seg_1255" n="HIAT:u" s="T317">
                  <ts e="T318" id="Seg_1257" n="HIAT:w" s="T317">Măn</ts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1260" n="HIAT:w" s="T318">turam</ts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1263" n="HIAT:w" s="T319">с</ts>
                  <nts id="Seg_1264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1266" n="HIAT:w" s="T320">краю</ts>
                  <nts id="Seg_1267" n="HIAT:ip">,</nts>
                  <nts id="Seg_1268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1270" n="HIAT:w" s="T321">măn</ts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1273" n="HIAT:w" s="T322">ĭmbidə</ts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1276" n="HIAT:w" s="T323">ej</ts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1279" n="HIAT:w" s="T324">tĭmniem</ts>
                  <nts id="Seg_1280" n="HIAT:ip">.</nts>
                  <nts id="Seg_1281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_1283" n="HIAT:u" s="T325">
                  <ts e="T326" id="Seg_1285" n="HIAT:w" s="T325">Tenöʔ</ts>
                  <nts id="Seg_1286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1288" n="HIAT:w" s="T326">ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_1289" n="HIAT:ip">.</nts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T329" id="Seg_1292" n="HIAT:u" s="T327">
                  <ts e="T328" id="Seg_1294" n="HIAT:w" s="T327">Šamaʔ</ts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1297" n="HIAT:w" s="T328">ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_1298" n="HIAT:ip">.</nts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T332" id="Seg_1301" n="HIAT:u" s="T329">
                  <ts e="T330" id="Seg_1303" n="HIAT:w" s="T329">Abam</ts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1305" n="HIAT:ip">(</nts>
                  <ts e="T331" id="Seg_1307" n="HIAT:w" s="T330">kuŋgəm</ts>
                  <nts id="Seg_1308" n="HIAT:ip">)</nts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1057" id="Seg_1311" n="HIAT:w" s="T331">kalla</ts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1314" n="HIAT:w" s="T1057">dʼürbi</ts>
                  <nts id="Seg_1315" n="HIAT:ip">.</nts>
                  <nts id="Seg_1316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T342" id="Seg_1318" n="HIAT:u" s="T332">
                  <ts e="T333" id="Seg_1320" n="HIAT:w" s="T332">A</ts>
                  <nts id="Seg_1321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1323" n="HIAT:w" s="T333">măna</ts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1326" n="HIAT:w" s="T334">ugandə</ts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1329" n="HIAT:w" s="T335">jakše</ts>
                  <nts id="Seg_1330" n="HIAT:ip">,</nts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1333" n="HIAT:w" s="T336">šindidə</ts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1336" n="HIAT:w" s="T337">ej</ts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1339" n="HIAT:w" s="T338">münöria</ts>
                  <nts id="Seg_1340" n="HIAT:ip">,</nts>
                  <nts id="Seg_1341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1343" n="HIAT:w" s="T339">măn</ts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1345" n="HIAT:ip">(</nts>
                  <ts e="T341" id="Seg_1347" n="HIAT:w" s="T340">balʔraʔ</ts>
                  <nts id="Seg_1348" n="HIAT:ip">)</nts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1351" n="HIAT:w" s="T341">alomniam</ts>
                  <nts id="Seg_1352" n="HIAT:ip">.</nts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T344" id="Seg_1355" n="HIAT:u" s="T342">
                  <nts id="Seg_1356" n="HIAT:ip">(</nts>
                  <ts e="T343" id="Seg_1358" n="HIAT:w" s="T342">Ajnu</ts>
                  <nts id="Seg_1359" n="HIAT:ip">)</nts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1362" n="HIAT:w" s="T343">nüjleʔbəm</ts>
                  <nts id="Seg_1363" n="HIAT:ip">.</nts>
                  <nts id="Seg_1364" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_1366" n="HIAT:u" s="T344">
                  <ts e="T345" id="Seg_1368" n="HIAT:w" s="T344">Suʔmileʔbəm</ts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1371" n="HIAT:w" s="T345">bar</ts>
                  <nts id="Seg_1372" n="HIAT:ip">.</nts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T350" id="Seg_1375" n="HIAT:u" s="T346">
                  <ts e="T347" id="Seg_1377" n="HIAT:w" s="T346">A</ts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1380" n="HIAT:w" s="T347">iam</ts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1383" n="HIAT:w" s="T348">ej</ts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1386" n="HIAT:w" s="T349">nʼilgöliem</ts>
                  <nts id="Seg_1387" n="HIAT:ip">.</nts>
                  <nts id="Seg_1388" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T357" id="Seg_1390" n="HIAT:u" s="T350">
                  <ts e="T351" id="Seg_1392" n="HIAT:w" s="T350">Iam</ts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1395" n="HIAT:w" s="T351">abam</ts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1398" n="HIAT:w" s="T352">dĭ</ts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1401" n="HIAT:w" s="T353">saʔpiaŋbi</ts>
                  <nts id="Seg_1402" n="HIAT:ip">,</nts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1405" n="HIAT:w" s="T354">što</ts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1408" n="HIAT:w" s="T355">măn</ts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1410" n="HIAT:ip">(</nts>
                  <nts id="Seg_1411" n="HIAT:ip">(</nts>
                  <ats e="T357" id="Seg_1412" n="HIAT:non-pho" s="T356">PAUSE</ats>
                  <nts id="Seg_1413" n="HIAT:ip">)</nts>
                  <nts id="Seg_1414" n="HIAT:ip">)</nts>
                  <nts id="Seg_1415" n="HIAT:ip">.</nts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T360" id="Seg_1418" n="HIAT:u" s="T357">
                  <ts e="T358" id="Seg_1420" n="HIAT:w" s="T357">Što</ts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1423" n="HIAT:w" s="T358">măn</ts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1426" n="HIAT:w" s="T359">alomniam</ts>
                  <nts id="Seg_1427" n="HIAT:ip">.</nts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T371" id="Seg_1430" n="HIAT:u" s="T360">
                  <ts e="T361" id="Seg_1432" n="HIAT:w" s="T360">Măn</ts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1435" n="HIAT:w" s="T361">elem</ts>
                  <nts id="Seg_1436" n="HIAT:ip">,</nts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1439" n="HIAT:w" s="T362">teinen</ts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1442" n="HIAT:w" s="T363">nüdʼin</ts>
                  <nts id="Seg_1443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1445" n="HIAT:w" s="T364">bar</ts>
                  <nts id="Seg_1446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1447" n="HIAT:ip">(</nts>
                  <ts e="T366" id="Seg_1449" n="HIAT:w" s="T365">ara</ts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1452" n="HIAT:w" s="T366">bĭʔ-</ts>
                  <nts id="Seg_1453" n="HIAT:ip">)</nts>
                  <nts id="Seg_1454" n="HIAT:ip">,</nts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1456" n="HIAT:ip">(</nts>
                  <ts e="T368" id="Seg_1458" n="HIAT:w" s="T367">dʼabr-</ts>
                  <nts id="Seg_1459" n="HIAT:ip">)</nts>
                  <nts id="Seg_1460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1462" n="HIAT:w" s="T368">nüjnə</ts>
                  <nts id="Seg_1463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1464" n="HIAT:ip">(</nts>
                  <ts e="T370" id="Seg_1466" n="HIAT:w" s="T369">nüjnə-</ts>
                  <nts id="Seg_1467" n="HIAT:ip">)</nts>
                  <nts id="Seg_1468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1470" n="HIAT:w" s="T370">nüjnəbi</ts>
                  <nts id="Seg_1471" n="HIAT:ip">.</nts>
                  <nts id="Seg_1472" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T375" id="Seg_1474" n="HIAT:u" s="T371">
                  <ts e="T372" id="Seg_1476" n="HIAT:w" s="T371">Dʼabərobi</ts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1479" n="HIAT:w" s="T372">bar</ts>
                  <nts id="Seg_1480" n="HIAT:ip">,</nts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1482" n="HIAT:ip">(</nts>
                  <ts e="T374" id="Seg_1484" n="HIAT:w" s="T373">kudonzəbi</ts>
                  <nts id="Seg_1485" n="HIAT:ip">)</nts>
                  <nts id="Seg_1486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1488" n="HIAT:w" s="T374">bar</ts>
                  <nts id="Seg_1489" n="HIAT:ip">.</nts>
                  <nts id="Seg_1490" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T385" id="Seg_1492" n="HIAT:u" s="T375">
                  <ts e="T376" id="Seg_1494" n="HIAT:w" s="T375">Jelʼa</ts>
                  <nts id="Seg_1495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1497" n="HIAT:w" s="T376">unnʼa</ts>
                  <nts id="Seg_1498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1500" n="HIAT:w" s="T377">amnolaʔbə</ts>
                  <nts id="Seg_1501" n="HIAT:ip">,</nts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1504" n="HIAT:w" s="T378">üge</ts>
                  <nts id="Seg_1505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1507" n="HIAT:w" s="T379">monoʔkolaʔbəʔjə</ts>
                  <nts id="Seg_1508" n="HIAT:ip">,</nts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1511" n="HIAT:w" s="T380">a</ts>
                  <nts id="Seg_1512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1514" n="HIAT:w" s="T381">dĭ</ts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1517" n="HIAT:w" s="T382">üge</ts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1520" n="HIAT:w" s="T383">ej</ts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1523" n="HIAT:w" s="T384">kalia</ts>
                  <nts id="Seg_1524" n="HIAT:ip">.</nts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T393" id="Seg_1527" n="HIAT:u" s="T385">
                  <ts e="T386" id="Seg_1529" n="HIAT:w" s="T385">Šindinədə</ts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1532" n="HIAT:w" s="T386">ej</ts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1535" n="HIAT:w" s="T387">kalia</ts>
                  <nts id="Seg_1536" n="HIAT:ip">,</nts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1539" n="HIAT:w" s="T388">ни</ts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1542" n="HIAT:w" s="T389">за</ts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1545" n="HIAT:w" s="T390">кого</ts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1548" n="HIAT:w" s="T391">не</ts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1551" n="HIAT:w" s="T392">идет</ts>
                  <nts id="Seg_1552" n="HIAT:ip">.</nts>
                  <nts id="Seg_1553" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T398" id="Seg_1555" n="HIAT:u" s="T393">
                  <ts e="T394" id="Seg_1557" n="HIAT:w" s="T393">Dĭn</ts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1560" n="HIAT:w" s="T394">bar</ts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1563" n="HIAT:w" s="T395">iat</ts>
                  <nts id="Seg_1564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1566" n="HIAT:w" s="T396">abat</ts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1569" n="HIAT:w" s="T397">ige</ts>
                  <nts id="Seg_1570" n="HIAT:ip">.</nts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T406" id="Seg_1573" n="HIAT:u" s="T398">
                  <ts e="T399" id="Seg_1575" n="HIAT:w" s="T398">Măliaʔi:</ts>
                  <nts id="Seg_1576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1578" n="HIAT:w" s="T399">Kanaʔ</ts>
                  <nts id="Seg_1579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1580" n="HIAT:ip">(</nts>
                  <ts e="T401" id="Seg_1582" n="HIAT:w" s="T400">tibinə</ts>
                  <nts id="Seg_1583" n="HIAT:ip">)</nts>
                  <nts id="Seg_1584" n="HIAT:ip">,</nts>
                  <nts id="Seg_1585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1587" n="HIAT:w" s="T401">a</ts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1590" n="HIAT:w" s="T402">dĭ</ts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1593" n="HIAT:w" s="T403">kădedə</ts>
                  <nts id="Seg_1594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1596" n="HIAT:w" s="T404">ej</ts>
                  <nts id="Seg_1597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1599" n="HIAT:w" s="T405">kalia</ts>
                  <nts id="Seg_1600" n="HIAT:ip">.</nts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T416" id="Seg_1603" n="HIAT:u" s="T406">
                  <ts e="T407" id="Seg_1605" n="HIAT:w" s="T406">Šobi</ts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1608" n="HIAT:w" s="T407">măna</ts>
                  <nts id="Seg_1609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1611" n="HIAT:w" s="T408">monoʔkosʼtə</ts>
                  <nts id="Seg_1612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1614" n="HIAT:w" s="T409">ej</ts>
                  <nts id="Seg_1615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1617" n="HIAT:w" s="T410">kuvas</ts>
                  <nts id="Seg_1618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1620" n="HIAT:w" s="T411">kuza</ts>
                  <nts id="Seg_1621" n="HIAT:ip">,</nts>
                  <nts id="Seg_1622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1624" n="HIAT:w" s="T412">măna</ts>
                  <nts id="Seg_1625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1627" n="HIAT:w" s="T413">nʼe</ts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1630" n="HIAT:w" s="T414">axota</ts>
                  <nts id="Seg_1631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1633" n="HIAT:w" s="T415">kanzittə</ts>
                  <nts id="Seg_1634" n="HIAT:ip">.</nts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T422" id="Seg_1637" n="HIAT:u" s="T416">
                  <ts e="T417" id="Seg_1639" n="HIAT:w" s="T416">Abam</ts>
                  <nts id="Seg_1640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1642" n="HIAT:w" s="T417">măndə:</ts>
                  <nts id="Seg_1643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1645" n="HIAT:w" s="T418">Ĭmbi</ts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1648" n="HIAT:w" s="T419">tăn</ts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1651" n="HIAT:w" s="T420">ej</ts>
                  <nts id="Seg_1652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1654" n="HIAT:w" s="T421">kalal</ts>
                  <nts id="Seg_1655" n="HIAT:ip">?</nts>
                  <nts id="Seg_1656" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T429" id="Seg_1658" n="HIAT:u" s="T422">
                  <nts id="Seg_1659" n="HIAT:ip">(</nts>
                  <ts e="T423" id="Seg_1661" n="HIAT:w" s="T422">Ej</ts>
                  <nts id="Seg_1662" n="HIAT:ip">)</nts>
                  <nts id="Seg_1663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1665" n="HIAT:w" s="T423">Nu</ts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1668" n="HIAT:w" s="T424">iʔ</ts>
                  <nts id="Seg_1669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1670" n="HIAT:ip">(</nts>
                  <ts e="T426" id="Seg_1672" n="HIAT:w" s="T425">kanaʔ</ts>
                  <nts id="Seg_1673" n="HIAT:ip">)</nts>
                  <nts id="Seg_1674" n="HIAT:ip">,</nts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1676" n="HIAT:ip">(</nts>
                  <ts e="T427" id="Seg_1678" n="HIAT:w" s="T426">măn</ts>
                  <nts id="Seg_1679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1681" n="HIAT:w" s="T427">nuʔməluʔpiam</ts>
                  <nts id="Seg_1682" n="HIAT:ip">)</nts>
                  <nts id="Seg_1683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1685" n="HIAT:w" s="T428">dĭgəttə</ts>
                  <nts id="Seg_1686" n="HIAT:ip">.</nts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T436" id="Seg_1689" n="HIAT:u" s="T429">
                  <ts e="T430" id="Seg_1691" n="HIAT:w" s="T429">Măn</ts>
                  <nts id="Seg_1692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1693" n="HIAT:ip">(</nts>
                  <ts e="T431" id="Seg_1695" n="HIAT:w" s="T430">pervam</ts>
                  <nts id="Seg_1696" n="HIAT:ip">)</nts>
                  <nts id="Seg_1697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1699" n="HIAT:w" s="T431">первый</ts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1702" n="HIAT:w" s="T432">tibim</ts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1705" n="HIAT:w" s="T433">šobi</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1708" n="HIAT:w" s="T434">măna</ts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1711" n="HIAT:w" s="T435">monoʔkosʼtə</ts>
                  <nts id="Seg_1712" n="HIAT:ip">.</nts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T447" id="Seg_1715" n="HIAT:u" s="T436">
                  <ts e="T437" id="Seg_1717" n="HIAT:w" s="T436">Dĭʔnə</ts>
                  <nts id="Seg_1718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1719" n="HIAT:ip">(</nts>
                  <ts e="T438" id="Seg_1721" n="HIAT:w" s="T437">мало</ts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1724" n="HIAT:w" s="T438">pʼe</ts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1727" n="HIAT:w" s="T439">i</ts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1730" n="HIAT:w" s="T440">măna=</ts>
                  <nts id="Seg_1731" n="HIAT:ip">)</nts>
                  <nts id="Seg_1732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1734" n="HIAT:w" s="T441">amga</ts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1737" n="HIAT:w" s="T442">pʼe</ts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1740" n="HIAT:w" s="T443">i</ts>
                  <nts id="Seg_1741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1743" n="HIAT:w" s="T444">măna</ts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1746" n="HIAT:w" s="T445">amga</ts>
                  <nts id="Seg_1747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1749" n="HIAT:w" s="T446">pʼe</ts>
                  <nts id="Seg_1750" n="HIAT:ip">.</nts>
                  <nts id="Seg_1751" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T458" id="Seg_1753" n="HIAT:u" s="T447">
                  <nts id="Seg_1754" n="HIAT:ip">(</nts>
                  <ts e="T448" id="Seg_1756" n="HIAT:w" s="T447">Măn</ts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1759" n="HIAT:w" s="T448">nuʔ</ts>
                  <nts id="Seg_1760" n="HIAT:ip">)</nts>
                  <nts id="Seg_1761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1763" n="HIAT:w" s="T449">Măn</ts>
                  <nts id="Seg_1764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1766" n="HIAT:w" s="T450">măndəm:</ts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1769" n="HIAT:w" s="T451">Kanaʔ</ts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1772" n="HIAT:w" s="T452">abanə</ts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1775" n="HIAT:w" s="T453">da</ts>
                  <nts id="Seg_1776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1778" n="HIAT:w" s="T454">ianə</ts>
                  <nts id="Seg_1779" n="HIAT:ip">,</nts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1782" n="HIAT:w" s="T455">monoʔkoʔ</ts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1785" n="HIAT:w" s="T456">dĭzeŋ</ts>
                  <nts id="Seg_1786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1788" n="HIAT:w" s="T457">mĭləʔi</ts>
                  <nts id="Seg_1789" n="HIAT:ip">?</nts>
                  <nts id="Seg_1790" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T461" id="Seg_1792" n="HIAT:u" s="T458">
                  <ts e="T459" id="Seg_1794" n="HIAT:w" s="T458">Dʼok</ts>
                  <nts id="Seg_1795" n="HIAT:ip">,</nts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1798" n="HIAT:w" s="T459">ej</ts>
                  <nts id="Seg_1799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1801" n="HIAT:w" s="T460">mĭləʔi</ts>
                  <nts id="Seg_1802" n="HIAT:ip">.</nts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T470" id="Seg_1805" n="HIAT:u" s="T461">
                  <ts e="T462" id="Seg_1807" n="HIAT:w" s="T461">Dĭgəttə</ts>
                  <nts id="Seg_1808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1810" n="HIAT:w" s="T462">măna</ts>
                  <nts id="Seg_1811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1813" n="HIAT:w" s="T463">dʼăbaktərluʔpiʔi</ts>
                  <nts id="Seg_1814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1816" n="HIAT:w" s="T464">i</ts>
                  <nts id="Seg_1817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1818" n="HIAT:ip">(</nts>
                  <ts e="T466" id="Seg_1820" n="HIAT:w" s="T465">kam-</ts>
                  <nts id="Seg_1821" n="HIAT:ip">)</nts>
                  <nts id="Seg_1822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1824" n="HIAT:w" s="T466">kambibaʔ</ts>
                  <nts id="Seg_1825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1826" n="HIAT:ip">(</nts>
                  <ts e="T468" id="Seg_1828" n="HIAT:w" s="T467">ine-</ts>
                  <nts id="Seg_1829" n="HIAT:ip">)</nts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1832" n="HIAT:w" s="T468">inetsi</ts>
                  <nts id="Seg_1833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1835" n="HIAT:w" s="T469">dĭʔnə</ts>
                  <nts id="Seg_1836" n="HIAT:ip">.</nts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T475" id="Seg_1839" n="HIAT:u" s="T470">
                  <ts e="T471" id="Seg_1841" n="HIAT:w" s="T470">Dĭgəttə</ts>
                  <nts id="Seg_1842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1844" n="HIAT:w" s="T471">iam</ts>
                  <nts id="Seg_1845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1847" n="HIAT:w" s="T472">šobi</ts>
                  <nts id="Seg_1848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1850" n="HIAT:w" s="T473">dibər</ts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1853" n="HIAT:w" s="T474">miʔnʼibeʔ</ts>
                  <nts id="Seg_1854" n="HIAT:ip">.</nts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T481" id="Seg_1857" n="HIAT:u" s="T475">
                  <ts e="T476" id="Seg_1859" n="HIAT:w" s="T475">Dĭ</ts>
                  <nts id="Seg_1860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1861" n="HIAT:ip">(</nts>
                  <ts e="T477" id="Seg_1863" n="HIAT:w" s="T476">š-</ts>
                  <nts id="Seg_1864" n="HIAT:ip">)</nts>
                  <nts id="Seg_1865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1867" n="HIAT:w" s="T477">döbər</ts>
                  <nts id="Seg_1868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1870" n="HIAT:w" s="T478">šobi</ts>
                  <nts id="Seg_1871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1873" n="HIAT:w" s="T479">noʔ</ts>
                  <nts id="Seg_1874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1876" n="HIAT:w" s="T480">jaʔsittə</ts>
                  <nts id="Seg_1877" n="HIAT:ip">.</nts>
                  <nts id="Seg_1878" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T487" id="Seg_1880" n="HIAT:u" s="T481">
                  <ts e="T482" id="Seg_1882" n="HIAT:w" s="T481">Dĭgəttə</ts>
                  <nts id="Seg_1883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1885" n="HIAT:w" s="T482">kambi</ts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1888" n="HIAT:w" s="T483">bünə</ts>
                  <nts id="Seg_1889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1891" n="HIAT:w" s="T484">da</ts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1894" n="HIAT:w" s="T485">dĭn</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1897" n="HIAT:w" s="T486">külambi</ts>
                  <nts id="Seg_1898" n="HIAT:ip">.</nts>
                  <nts id="Seg_1899" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T497" id="Seg_1901" n="HIAT:u" s="T487">
                  <ts e="T488" id="Seg_1903" n="HIAT:w" s="T487">Dĭgəttə</ts>
                  <nts id="Seg_1904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1906" n="HIAT:w" s="T488">măn</ts>
                  <nts id="Seg_1907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1909" n="HIAT:w" s="T489">maːndə</ts>
                  <nts id="Seg_1910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1912" n="HIAT:w" s="T490">šobiam</ts>
                  <nts id="Seg_1913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1915" n="HIAT:w" s="T491">i</ts>
                  <nts id="Seg_1916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1918" n="HIAT:w" s="T492">ugandə</ts>
                  <nts id="Seg_1919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1921" n="HIAT:w" s="T493">tăŋ</ts>
                  <nts id="Seg_1922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1924" n="HIAT:w" s="T494">tenöbiam</ts>
                  <nts id="Seg_1925" n="HIAT:ip">,</nts>
                  <nts id="Seg_1926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1928" n="HIAT:w" s="T495">sĭjbə</ts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1931" n="HIAT:w" s="T496">ĭzembie</ts>
                  <nts id="Seg_1932" n="HIAT:ip">.</nts>
                  <nts id="Seg_1933" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T501" id="Seg_1935" n="HIAT:u" s="T497">
                  <ts e="T498" id="Seg_1937" n="HIAT:w" s="T497">Dʼorbiam</ts>
                  <nts id="Seg_1938" n="HIAT:ip">,</nts>
                  <nts id="Seg_1939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1941" n="HIAT:w" s="T498">ajirbiom</ts>
                  <nts id="Seg_1942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1944" n="HIAT:w" s="T499">dĭm</ts>
                  <nts id="Seg_1945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1947" n="HIAT:w" s="T500">bar</ts>
                  <nts id="Seg_1948" n="HIAT:ip">.</nts>
                  <nts id="Seg_1949" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T508" id="Seg_1951" n="HIAT:u" s="T501">
                  <ts e="T502" id="Seg_1953" n="HIAT:w" s="T501">Dĭgəttə</ts>
                  <nts id="Seg_1954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1956" n="HIAT:w" s="T502">iam</ts>
                  <nts id="Seg_1957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1959" n="HIAT:w" s="T503">măna</ts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1962" n="HIAT:w" s="T504">bü</ts>
                  <nts id="Seg_1963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1965" n="HIAT:w" s="T505">dʼazirbi</ts>
                  <nts id="Seg_1966" n="HIAT:ip">,</nts>
                  <nts id="Seg_1967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1968" n="HIAT:ip">(</nts>
                  <ts e="T507" id="Seg_1970" n="HIAT:w" s="T506">pĭj</ts>
                  <nts id="Seg_1971" n="HIAT:ip">)</nts>
                  <nts id="Seg_1972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1974" n="HIAT:w" s="T507">măna</ts>
                  <nts id="Seg_1975" n="HIAT:ip">.</nts>
                  <nts id="Seg_1976" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T514" id="Seg_1978" n="HIAT:u" s="T508">
                  <ts e="T509" id="Seg_1980" n="HIAT:w" s="T508">Dĭgəttə</ts>
                  <nts id="Seg_1981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1983" n="HIAT:w" s="T509">măn</ts>
                  <nts id="Seg_1984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1986" n="HIAT:w" s="T510">davaj</ts>
                  <nts id="Seg_1987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1989" n="HIAT:w" s="T511">nüjnə</ts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1991" n="HIAT:ip">(</nts>
                  <ts e="T513" id="Seg_1993" n="HIAT:w" s="T512">nüjzit-</ts>
                  <nts id="Seg_1994" n="HIAT:ip">)</nts>
                  <nts id="Seg_1995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1997" n="HIAT:w" s="T513">nüjzittə</ts>
                  <nts id="Seg_1998" n="HIAT:ip">.</nts>
                  <nts id="Seg_1999" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T519" id="Seg_2001" n="HIAT:u" s="T514">
                  <ts e="T515" id="Seg_2003" n="HIAT:w" s="T514">Dĭgəttə</ts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_2006" n="HIAT:w" s="T515">măn</ts>
                  <nts id="Seg_2007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_2009" n="HIAT:w" s="T516">šobiam</ts>
                  <nts id="Seg_2010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2011" n="HIAT:ip">(</nts>
                  <ts e="T518" id="Seg_2013" n="HIAT:w" s="T517">maʔnʼi</ts>
                  <nts id="Seg_2014" n="HIAT:ip">)</nts>
                  <nts id="Seg_2015" n="HIAT:ip">,</nts>
                  <nts id="Seg_2016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_2018" n="HIAT:w" s="T518">amnobiam</ts>
                  <nts id="Seg_2019" n="HIAT:ip">.</nts>
                  <nts id="Seg_2020" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T524" id="Seg_2022" n="HIAT:u" s="T519">
                  <ts e="T520" id="Seg_2024" n="HIAT:w" s="T519">Dĭgəttə</ts>
                  <nts id="Seg_2025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_2027" n="HIAT:w" s="T520">kambiam</ts>
                  <nts id="Seg_2028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2029" n="HIAT:ip">(</nts>
                  <ts e="T522" id="Seg_2031" n="HIAT:w" s="T521">Permʼako-</ts>
                  <nts id="Seg_2032" n="HIAT:ip">)</nts>
                  <nts id="Seg_2033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_2035" n="HIAT:w" s="T522">Permʼakovtə</ts>
                  <nts id="Seg_2036" n="HIAT:ip">,</nts>
                  <nts id="Seg_2037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_2039" n="HIAT:w" s="T523">večorkanə</ts>
                  <nts id="Seg_2040" n="HIAT:ip">.</nts>
                  <nts id="Seg_2041" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T527" id="Seg_2043" n="HIAT:u" s="T524">
                  <nts id="Seg_2044" n="HIAT:ip">(</nts>
                  <ts e="T525" id="Seg_2046" n="HIAT:w" s="T524">Ten</ts>
                  <nts id="Seg_2047" n="HIAT:ip">)</nts>
                  <nts id="Seg_2048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_2050" n="HIAT:w" s="T525">sʼarbibaʔ</ts>
                  <nts id="Seg_2051" n="HIAT:ip">,</nts>
                  <nts id="Seg_2052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_2054" n="HIAT:w" s="T526">suʔmibeʔ</ts>
                  <nts id="Seg_2055" n="HIAT:ip">.</nts>
                  <nts id="Seg_2056" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T529" id="Seg_2058" n="HIAT:u" s="T527">
                  <ts e="T528" id="Seg_2060" n="HIAT:w" s="T527">Garmonʼnʼə</ts>
                  <nts id="Seg_2061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_2063" n="HIAT:w" s="T528">sʼarbibaʔ</ts>
                  <nts id="Seg_2064" n="HIAT:ip">.</nts>
                  <nts id="Seg_2065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T531" id="Seg_2067" n="HIAT:u" s="T529">
                  <ts e="T530" id="Seg_2069" n="HIAT:w" s="T529">Nüjnə</ts>
                  <nts id="Seg_2070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_2072" n="HIAT:w" s="T530">nüjnəbibeʔ</ts>
                  <nts id="Seg_2073" n="HIAT:ip">.</nts>
                  <nts id="Seg_2074" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T536" id="Seg_2076" n="HIAT:u" s="T531">
                  <ts e="T532" id="Seg_2078" n="HIAT:w" s="T531">Dĭgəttə</ts>
                  <nts id="Seg_2079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_2081" n="HIAT:w" s="T532">dĭn</ts>
                  <nts id="Seg_2082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_2084" n="HIAT:w" s="T533">tibi</ts>
                  <nts id="Seg_2085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_2087" n="HIAT:w" s="T534">šobi</ts>
                  <nts id="Seg_2088" n="HIAT:ip">,</nts>
                  <nts id="Seg_2089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2090" n="HIAT:ip">(</nts>
                  <ts e="T536" id="Seg_2092" n="HIAT:w" s="T535">Nagornaj</ts>
                  <nts id="Seg_2093" n="HIAT:ip">)</nts>
                  <nts id="Seg_2094" n="HIAT:ip">.</nts>
                  <nts id="Seg_2095" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T539" id="Seg_2097" n="HIAT:u" s="T536">
                  <ts e="T537" id="Seg_2099" n="HIAT:w" s="T536">Muʔzendə</ts>
                  <nts id="Seg_2100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2102" n="HIAT:w" s="T537">urgo</ts>
                  <nts id="Seg_2103" n="HIAT:ip">,</nts>
                  <nts id="Seg_2104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2106" n="HIAT:w" s="T538">numəʔi</ts>
                  <nts id="Seg_2107" n="HIAT:ip">.</nts>
                  <nts id="Seg_2108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T543" id="Seg_2110" n="HIAT:u" s="T539">
                  <ts e="T540" id="Seg_2112" n="HIAT:w" s="T539">Da</ts>
                  <nts id="Seg_2113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_2115" n="HIAT:w" s="T540">köməʔi</ts>
                  <nts id="Seg_2116" n="HIAT:ip">,</nts>
                  <nts id="Seg_2117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2119" n="HIAT:w" s="T541">bostə</ts>
                  <nts id="Seg_2120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_2122" n="HIAT:w" s="T542">kömə</ts>
                  <nts id="Seg_2123" n="HIAT:ip">.</nts>
                  <nts id="Seg_2124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T551" id="Seg_2126" n="HIAT:u" s="T543">
                  <ts e="T544" id="Seg_2128" n="HIAT:w" s="T543">Dĭgəttə</ts>
                  <nts id="Seg_2129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2131" n="HIAT:w" s="T544">măn</ts>
                  <nts id="Seg_2132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2134" n="HIAT:w" s="T545">maʔnʼi</ts>
                  <nts id="Seg_2135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2137" n="HIAT:w" s="T546">šobiam</ts>
                  <nts id="Seg_2138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2140" n="HIAT:w" s="T547">da</ts>
                  <nts id="Seg_2141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2143" n="HIAT:w" s="T548">šobi</ts>
                  <nts id="Seg_2144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2146" n="HIAT:w" s="T549">măna</ts>
                  <nts id="Seg_2147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2149" n="HIAT:w" s="T550">monoʔkosʼtə</ts>
                  <nts id="Seg_2150" n="HIAT:ip">.</nts>
                  <nts id="Seg_2151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T559" id="Seg_2153" n="HIAT:u" s="T551">
                  <ts e="T552" id="Seg_2155" n="HIAT:w" s="T551">Măn</ts>
                  <nts id="Seg_2156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2158" n="HIAT:w" s="T552">măndəm:</ts>
                  <nts id="Seg_2159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2161" n="HIAT:w" s="T553">Tăn</ts>
                  <nts id="Seg_2162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2163" n="HIAT:ip">(</nts>
                  <ts e="T555" id="Seg_2165" n="HIAT:w" s="T554">nʼi</ts>
                  <nts id="Seg_2166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2168" n="HIAT:w" s="T555">git</ts>
                  <nts id="Seg_2169" n="HIAT:ip">)</nts>
                  <nts id="Seg_2170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2171" n="HIAT:ip">(</nts>
                  <ts e="T557" id="Seg_2173" n="HIAT:w" s="T556">kək-</ts>
                  <nts id="Seg_2174" n="HIAT:ip">)</nts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2176" n="HIAT:ip">(</nts>
                  <ts e="T558" id="Seg_2178" n="HIAT:w" s="T557">kak</ts>
                  <nts id="Seg_2179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2181" n="HIAT:w" s="T558">măn</ts>
                  <nts id="Seg_2182" n="HIAT:ip">)</nts>
                  <nts id="Seg_2183" n="HIAT:ip">.</nts>
                  <nts id="Seg_2184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T560" id="Seg_2186" n="HIAT:u" s="T559">
                  <ts e="T560" id="Seg_2188" n="HIAT:w" s="T559">Dĭrgit</ts>
                  <nts id="Seg_2189" n="HIAT:ip">.</nts>
                  <nts id="Seg_2190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T567" id="Seg_2192" n="HIAT:u" s="T560">
                  <ts e="T561" id="Seg_2194" n="HIAT:w" s="T560">Nu</ts>
                  <nts id="Seg_2195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2197" n="HIAT:w" s="T561">măn</ts>
                  <nts id="Seg_2198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2200" n="HIAT:w" s="T562">tănzi</ts>
                  <nts id="Seg_2201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2203" n="HIAT:w" s="T563">ej</ts>
                  <nts id="Seg_2204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2206" n="HIAT:w" s="T564">amnolam</ts>
                  <nts id="Seg_2207" n="HIAT:ip">,</nts>
                  <nts id="Seg_2208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2210" n="HIAT:w" s="T565">a</ts>
                  <nts id="Seg_2211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2213" n="HIAT:w" s="T566">nʼizi</ts>
                  <nts id="Seg_2214" n="HIAT:ip">.</nts>
                  <nts id="Seg_2215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T571" id="Seg_2217" n="HIAT:u" s="T567">
                  <ts e="T568" id="Seg_2219" n="HIAT:w" s="T567">Dĭgəttə</ts>
                  <nts id="Seg_2220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2222" n="HIAT:w" s="T568">iam</ts>
                  <nts id="Seg_2223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2225" n="HIAT:w" s="T569">măndə:</ts>
                  <nts id="Seg_2226" n="HIAT:ip">"</nts>
                  <nts id="Seg_2227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2229" n="HIAT:w" s="T570">Kanaʔ</ts>
                  <nts id="Seg_2230" n="HIAT:ip">.</nts>
                  <nts id="Seg_2231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T574" id="Seg_2233" n="HIAT:u" s="T571">
                  <ts e="T572" id="Seg_2235" n="HIAT:w" s="T571">Dĭn</ts>
                  <nts id="Seg_2236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2238" n="HIAT:w" s="T572">aktʼa</ts>
                  <nts id="Seg_2239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2241" n="HIAT:w" s="T573">iʔgö</ts>
                  <nts id="Seg_2242" n="HIAT:ip">.</nts>
                  <nts id="Seg_2243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T578" id="Seg_2245" n="HIAT:u" s="T574">
                  <ts e="T575" id="Seg_2247" n="HIAT:w" s="T574">Ugandə</ts>
                  <nts id="Seg_2248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2249" n="HIAT:ip">(</nts>
                  <ts e="T576" id="Seg_2251" n="HIAT:w" s="T575">k-</ts>
                  <nts id="Seg_2252" n="HIAT:ip">)</nts>
                  <nts id="Seg_2253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2255" n="HIAT:w" s="T576">jakše</ts>
                  <nts id="Seg_2256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2258" n="HIAT:w" s="T577">kuza</ts>
                  <nts id="Seg_2259" n="HIAT:ip">.</nts>
                  <nts id="Seg_2260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T582" id="Seg_2262" n="HIAT:u" s="T578">
                  <ts e="T579" id="Seg_2264" n="HIAT:w" s="T578">A</ts>
                  <nts id="Seg_2265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2267" n="HIAT:w" s="T579">măn</ts>
                  <nts id="Seg_2268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2269" n="HIAT:ip">(</nts>
                  <ts e="T580.tx.1" id="Seg_2271" n="HIAT:w" s="T580">nuliam</ts>
                  <nts id="Seg_2272" n="HIAT:ip">)</nts>
                  <ts e="T581" id="Seg_2274" n="HIAT:w" s="T580.tx.1">:</ts>
                  <nts id="Seg_2275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2277" n="HIAT:w" s="T581">Ej</ts>
                  <nts id="Seg_2278" n="HIAT:ip">.</nts>
                  <nts id="Seg_2279" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T587" id="Seg_2281" n="HIAT:u" s="T582">
                  <ts e="T583" id="Seg_2283" n="HIAT:w" s="T582">Da</ts>
                  <nts id="Seg_2284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2286" n="HIAT:w" s="T583">kanaʔ</ts>
                  <nts id="Seg_2287" n="HIAT:ip">,</nts>
                  <nts id="Seg_2288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2290" n="HIAT:w" s="T584">da</ts>
                  <nts id="Seg_2291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2293" n="HIAT:w" s="T585">amnoʔ</ts>
                  <nts id="Seg_2294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2296" n="HIAT:w" s="T586">dĭzi</ts>
                  <nts id="Seg_2297" n="HIAT:ip">.</nts>
                  <nts id="Seg_2298" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T591" id="Seg_2300" n="HIAT:u" s="T587">
                  <ts e="T588" id="Seg_2302" n="HIAT:w" s="T587">Onʼiʔ</ts>
                  <nts id="Seg_2303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2305" n="HIAT:w" s="T588">kuza</ts>
                  <nts id="Seg_2306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2308" n="HIAT:w" s="T589">šobi</ts>
                  <nts id="Seg_2309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2311" n="HIAT:w" s="T590">monoʔkosʼtə</ts>
                  <nts id="Seg_2312" n="HIAT:ip">.</nts>
                  <nts id="Seg_2313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T595" id="Seg_2315" n="HIAT:u" s="T591">
                  <ts e="T592" id="Seg_2317" n="HIAT:w" s="T591">A</ts>
                  <nts id="Seg_2318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2320" n="HIAT:w" s="T592">măn</ts>
                  <nts id="Seg_2321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2323" n="HIAT:w" s="T593">ej</ts>
                  <nts id="Seg_2324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2326" n="HIAT:w" s="T594">kaliam</ts>
                  <nts id="Seg_2327" n="HIAT:ip">.</nts>
                  <nts id="Seg_2328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T597" id="Seg_2330" n="HIAT:u" s="T595">
                  <nts id="Seg_2331" n="HIAT:ip">(</nts>
                  <ts e="T596" id="Seg_2333" n="HIAT:w" s="T595">A</ts>
                  <nts id="Seg_2334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2336" n="HIAT:w" s="T596">ia=</ts>
                  <nts id="Seg_2337" n="HIAT:ip">)</nts>
                  <nts id="Seg_2338" n="HIAT:ip">.</nts>
                  <nts id="Seg_2339" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T610" id="Seg_2341" n="HIAT:u" s="T597">
                  <ts e="T598" id="Seg_2343" n="HIAT:w" s="T597">Ej</ts>
                  <nts id="Seg_2344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2346" n="HIAT:w" s="T598">kaliam</ts>
                  <nts id="Seg_2347" n="HIAT:ip">,</nts>
                  <nts id="Seg_2348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2350" n="HIAT:w" s="T599">dĭ</ts>
                  <nts id="Seg_2351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2352" n="HIAT:ip">(</nts>
                  <ts e="T601" id="Seg_2354" n="HIAT:w" s="T600">ej</ts>
                  <nts id="Seg_2355" n="HIAT:ip">)</nts>
                  <nts id="Seg_2356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2358" n="HIAT:w" s="T601">kuvas</ts>
                  <nts id="Seg_2359" n="HIAT:ip">,</nts>
                  <nts id="Seg_2360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2361" n="HIAT:ip">(</nts>
                  <ts e="T603" id="Seg_2363" n="HIAT:w" s="T602">üjü=</ts>
                  <nts id="Seg_2364" n="HIAT:ip">)</nts>
                  <nts id="Seg_2365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2367" n="HIAT:w" s="T603">üjüt</ts>
                  <nts id="Seg_2368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2370" n="HIAT:w" s="T604">bar</ts>
                  <nts id="Seg_2371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2372" n="HIAT:ip">(</nts>
                  <ts e="T606" id="Seg_2374" n="HIAT:w" s="T605">ponʼ-</ts>
                  <nts id="Seg_2375" n="HIAT:ip">)</nts>
                  <nts id="Seg_2376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2378" n="HIAT:w" s="T606">păjdʼaŋ</ts>
                  <nts id="Seg_2379" n="HIAT:ip">,</nts>
                  <nts id="Seg_2380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2381" n="HIAT:ip">(</nts>
                  <ts e="T608" id="Seg_2383" n="HIAT:w" s="T607">bost-</ts>
                  <nts id="Seg_2384" n="HIAT:ip">)</nts>
                  <nts id="Seg_2385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2387" n="HIAT:w" s="T608">muʔzendə</ts>
                  <nts id="Seg_2388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2390" n="HIAT:w" s="T609">kömə</ts>
                  <nts id="Seg_2391" n="HIAT:ip">.</nts>
                  <nts id="Seg_2392" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T619" id="Seg_2394" n="HIAT:u" s="T610">
                  <ts e="T611" id="Seg_2396" n="HIAT:w" s="T610">Šĭket</ts>
                  <nts id="Seg_2397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2399" n="HIAT:w" s="T611">ej</ts>
                  <nts id="Seg_2400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2402" n="HIAT:w" s="T612">dʼăbaktəria</ts>
                  <nts id="Seg_2403" n="HIAT:ip">,</nts>
                  <nts id="Seg_2404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2406" n="HIAT:w" s="T613">ej</ts>
                  <nts id="Seg_2407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2409" n="HIAT:w" s="T614">jakše</ts>
                  <nts id="Seg_2410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2412" n="HIAT:w" s="T615">dʼăbaktəria</ts>
                  <nts id="Seg_2413" n="HIAT:ip">,</nts>
                  <nts id="Seg_2414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2416" n="HIAT:w" s="T616">kuzaŋdə</ts>
                  <nts id="Seg_2417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2419" n="HIAT:w" s="T617">ej</ts>
                  <nts id="Seg_2420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2421" n="HIAT:ip">(</nts>
                  <ts e="T619" id="Seg_2423" n="HIAT:w" s="T618">nʼilgöliaʔjə</ts>
                  <nts id="Seg_2424" n="HIAT:ip">)</nts>
                  <nts id="Seg_2425" n="HIAT:ip">.</nts>
                  <nts id="Seg_2426" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T632" id="Seg_2428" n="HIAT:u" s="T619">
                  <ts e="T620" id="Seg_2430" n="HIAT:w" s="T619">A</ts>
                  <nts id="Seg_2431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2433" n="HIAT:w" s="T620">iam</ts>
                  <nts id="Seg_2434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2436" n="HIAT:w" s="T621">măndə:</ts>
                  <nts id="Seg_2437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2439" n="HIAT:w" s="T622">Kanaʔ</ts>
                  <nts id="Seg_2440" n="HIAT:ip">,</nts>
                  <nts id="Seg_2441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2443" n="HIAT:w" s="T623">dĭn</ts>
                  <nts id="Seg_2444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2446" n="HIAT:w" s="T624">tüžöj</ts>
                  <nts id="Seg_2447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2449" n="HIAT:w" s="T625">ige</ts>
                  <nts id="Seg_2450" n="HIAT:ip">,</nts>
                  <nts id="Seg_2451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2453" n="HIAT:w" s="T626">i</ts>
                  <nts id="Seg_2454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2456" n="HIAT:w" s="T627">ine</ts>
                  <nts id="Seg_2457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2459" n="HIAT:w" s="T628">ige</ts>
                  <nts id="Seg_2460" n="HIAT:ip">,</nts>
                  <nts id="Seg_2461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2463" n="HIAT:w" s="T629">i</ts>
                  <nts id="Seg_2464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2466" n="HIAT:w" s="T630">ipek</ts>
                  <nts id="Seg_2467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2469" n="HIAT:w" s="T631">iʔgö</ts>
                  <nts id="Seg_2470" n="HIAT:ip">.</nts>
                  <nts id="Seg_2471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T653" id="Seg_2473" n="HIAT:u" s="T632">
                  <ts e="T633" id="Seg_2475" n="HIAT:w" s="T632">A</ts>
                  <nts id="Seg_2476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2477" n="HIAT:ip">(</nts>
                  <ts e="T634" id="Seg_2479" n="HIAT:w" s="T633">măn=</ts>
                  <nts id="Seg_2480" n="HIAT:ip">)</nts>
                  <nts id="Seg_2481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2483" n="HIAT:w" s="T634">măn</ts>
                  <nts id="Seg_2484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2486" n="HIAT:w" s="T635">mămbiam:</ts>
                  <nts id="Seg_2487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2489" n="HIAT:w" s="T636">Măna</ts>
                  <nts id="Seg_2490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2492" n="HIAT:w" s="T637">inezi</ts>
                  <nts id="Seg_2493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2495" n="HIAT:w" s="T638">ej</ts>
                  <nts id="Seg_2496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2498" n="HIAT:w" s="T639">amnosʼtə</ts>
                  <nts id="Seg_2499" n="HIAT:ip">,</nts>
                  <nts id="Seg_2500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2502" n="HIAT:w" s="T640">i</ts>
                  <nts id="Seg_2503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2505" n="HIAT:w" s="T641">tüžöjdə</ts>
                  <nts id="Seg_2506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2508" n="HIAT:w" s="T642">ej</ts>
                  <nts id="Seg_2509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2511" n="HIAT:w" s="T643">amnosʼtə</ts>
                  <nts id="Seg_2512" n="HIAT:ip">,</nts>
                  <nts id="Seg_2513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2514" n="HIAT:ip">(</nts>
                  <ts e="T645" id="Seg_2516" n="HIAT:w" s="T644">nada</ts>
                  <nts id="Seg_2517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2519" n="HIAT:w" s="T645">tănan</ts>
                  <nts id="Seg_2520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2522" n="HIAT:w" s="T646">dăk=</ts>
                  <nts id="Seg_2523" n="HIAT:ip">)</nts>
                  <nts id="Seg_2524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2525" n="HIAT:ip">(</nts>
                  <ts e="T648" id="Seg_2527" n="HIAT:w" s="T647">tăn</ts>
                  <nts id="Seg_2528" n="HIAT:ip">)</nts>
                  <nts id="Seg_2529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2531" n="HIAT:w" s="T648">tănan</ts>
                  <nts id="Seg_2532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2534" n="HIAT:w" s="T649">kereʔ</ts>
                  <nts id="Seg_2535" n="HIAT:ip">,</nts>
                  <nts id="Seg_2536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2538" n="HIAT:w" s="T650">dak</ts>
                  <nts id="Seg_2539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2541" n="HIAT:w" s="T651">tăn</ts>
                  <nts id="Seg_2542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2544" n="HIAT:w" s="T652">kanaʔ</ts>
                  <nts id="Seg_2545" n="HIAT:ip">.</nts>
                  <nts id="Seg_2546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T657" id="Seg_2548" n="HIAT:u" s="T653">
                  <ts e="T654" id="Seg_2550" n="HIAT:w" s="T653">Tanitsagən</ts>
                  <nts id="Seg_2551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2553" n="HIAT:w" s="T654">šobi</ts>
                  <nts id="Seg_2554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2556" n="HIAT:w" s="T655">onʼiʔ</ts>
                  <nts id="Seg_2557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2559" n="HIAT:w" s="T656">tibi</ts>
                  <nts id="Seg_2560" n="HIAT:ip">.</nts>
                  <nts id="Seg_2561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T660" id="Seg_2563" n="HIAT:u" s="T657">
                  <ts e="T658" id="Seg_2565" n="HIAT:w" s="T657">Dĭm</ts>
                  <nts id="Seg_2566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2568" n="HIAT:w" s="T658">ne</ts>
                  <nts id="Seg_2569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2571" n="HIAT:w" s="T659">barəʔluʔpi</ts>
                  <nts id="Seg_2572" n="HIAT:ip">.</nts>
                  <nts id="Seg_2573" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T664" id="Seg_2575" n="HIAT:u" s="T660">
                  <ts e="T661" id="Seg_2577" n="HIAT:w" s="T660">Dĭ</ts>
                  <nts id="Seg_2578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2580" n="HIAT:w" s="T661">davaj</ts>
                  <nts id="Seg_2581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2583" n="HIAT:w" s="T662">măna</ts>
                  <nts id="Seg_2584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2586" n="HIAT:w" s="T663">monoʔkosʼtə</ts>
                  <nts id="Seg_2587" n="HIAT:ip">.</nts>
                  <nts id="Seg_2588" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T671" id="Seg_2590" n="HIAT:u" s="T664">
                  <ts e="T665" id="Seg_2592" n="HIAT:w" s="T664">Iam</ts>
                  <nts id="Seg_2593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2595" n="HIAT:w" s="T665">stal</ts>
                  <nts id="Seg_2596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2597" n="HIAT:ip">(</nts>
                  <ts e="T667" id="Seg_2599" n="HIAT:w" s="T666">mĭnzerzittə</ts>
                  <nts id="Seg_2600" n="HIAT:ip">)</nts>
                  <nts id="Seg_2601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2603" n="HIAT:w" s="T667">uja</ts>
                  <nts id="Seg_2604" n="HIAT:ip">,</nts>
                  <nts id="Seg_2605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2607" n="HIAT:w" s="T668">dĭzem</ts>
                  <nts id="Seg_2608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2609" n="HIAT:ip">(</nts>
                  <ts e="T670" id="Seg_2611" n="HIAT:w" s="T669">bĭd-</ts>
                  <nts id="Seg_2612" n="HIAT:ip">)</nts>
                  <nts id="Seg_2613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2615" n="HIAT:w" s="T670">bădəsʼtə</ts>
                  <nts id="Seg_2616" n="HIAT:ip">.</nts>
                  <nts id="Seg_2617" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T678" id="Seg_2619" n="HIAT:u" s="T671">
                  <ts e="T672" id="Seg_2621" n="HIAT:w" s="T671">A</ts>
                  <nts id="Seg_2622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2624" n="HIAT:w" s="T672">măn</ts>
                  <nts id="Seg_2625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2627" n="HIAT:w" s="T673">mălliam:</ts>
                  <nts id="Seg_2628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2630" n="HIAT:w" s="T674">kanzittə</ts>
                  <nts id="Seg_2631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2632" n="HIAT:ip">(</nts>
                  <ts e="T676" id="Seg_2634" n="HIAT:w" s="T675">ilʼi</ts>
                  <nts id="Seg_2635" n="HIAT:ip">)</nts>
                  <nts id="Seg_2636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2638" n="HIAT:w" s="T676">ej</ts>
                  <nts id="Seg_2639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2641" n="HIAT:w" s="T677">kanzittə</ts>
                  <nts id="Seg_2642" n="HIAT:ip">?</nts>
                  <nts id="Seg_2643" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T683" id="Seg_2645" n="HIAT:u" s="T678">
                  <ts e="T679" id="Seg_2647" n="HIAT:w" s="T678">Хошь</ts>
                  <nts id="Seg_2648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2650" n="HIAT:w" s="T679">kanaʔ</ts>
                  <nts id="Seg_2651" n="HIAT:ip">,</nts>
                  <nts id="Seg_2652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2654" n="HIAT:w" s="T680">хошь</ts>
                  <nts id="Seg_2655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2657" n="HIAT:w" s="T681">iʔ</ts>
                  <nts id="Seg_2658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2660" n="HIAT:w" s="T682">kanaʔ</ts>
                  <nts id="Seg_2661" n="HIAT:ip">.</nts>
                  <nts id="Seg_2662" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T692" id="Seg_2664" n="HIAT:u" s="T683">
                  <ts e="T684" id="Seg_2666" n="HIAT:w" s="T683">Tăn</ts>
                  <nts id="Seg_2667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2669" n="HIAT:w" s="T684">tüjö</ts>
                  <nts id="Seg_2670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2671" n="HIAT:ip">(</nts>
                  <ts e="T686" id="Seg_2673" n="HIAT:w" s="T685">s-</ts>
                  <nts id="Seg_2674" n="HIAT:ip">)</nts>
                  <nts id="Seg_2675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2676" n="HIAT:ip">(</nts>
                  <ts e="T687" id="Seg_2678" n="HIAT:w" s="T686">bostə</ts>
                  <nts id="Seg_2679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2681" n="HIAT:w" s="T687">urgo=</ts>
                  <nts id="Seg_2682" n="HIAT:ip">)</nts>
                  <nts id="Seg_2683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2685" n="HIAT:w" s="T688">bostə</ts>
                  <nts id="Seg_2686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2688" n="HIAT:w" s="T689">tĭmnel</ts>
                  <nts id="Seg_2689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2691" n="HIAT:w" s="T690">ĭmbi</ts>
                  <nts id="Seg_2692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2693" n="HIAT:ip">(</nts>
                  <ts e="T692" id="Seg_2695" n="HIAT:w" s="T691">azittə</ts>
                  <nts id="Seg_2696" n="HIAT:ip">)</nts>
                  <nts id="Seg_2697" n="HIAT:ip">.</nts>
                  <nts id="Seg_2698" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T697" id="Seg_2700" n="HIAT:u" s="T692">
                  <ts e="T693" id="Seg_2702" n="HIAT:w" s="T692">Dĭgəttə</ts>
                  <nts id="Seg_2703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2705" n="HIAT:w" s="T693">dĭ</ts>
                  <nts id="Seg_2706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2708" n="HIAT:w" s="T694">tibi</ts>
                  <nts id="Seg_2709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2710" n="HIAT:ip">(</nts>
                  <ts e="T696" id="Seg_2712" n="HIAT:w" s="T695">sʼ-</ts>
                  <nts id="Seg_2713" n="HIAT:ip">)</nts>
                  <nts id="Seg_2714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2716" n="HIAT:w" s="T696">šobi</ts>
                  <nts id="Seg_2717" n="HIAT:ip">.</nts>
                  <nts id="Seg_2718" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T699" id="Seg_2720" n="HIAT:u" s="T697">
                  <ts e="T698" id="Seg_2722" n="HIAT:w" s="T697">Măna</ts>
                  <nts id="Seg_2723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2725" n="HIAT:w" s="T698">amnobi</ts>
                  <nts id="Seg_2726" n="HIAT:ip">.</nts>
                  <nts id="Seg_2727" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T703" id="Seg_2729" n="HIAT:u" s="T699">
                  <ts e="T700" id="Seg_2731" n="HIAT:w" s="T699">I</ts>
                  <nts id="Seg_2732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2734" n="HIAT:w" s="T700">davaj</ts>
                  <nts id="Seg_2735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2737" n="HIAT:w" s="T701">monoʔkosʼtə</ts>
                  <nts id="Seg_2738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2740" n="HIAT:w" s="T702">bostə</ts>
                  <nts id="Seg_2741" n="HIAT:ip">.</nts>
                  <nts id="Seg_2742" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T707" id="Seg_2744" n="HIAT:u" s="T703">
                  <ts e="T704" id="Seg_2746" n="HIAT:w" s="T703">I</ts>
                  <nts id="Seg_2747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2749" n="HIAT:w" s="T704">davaj</ts>
                  <nts id="Seg_2750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2752" n="HIAT:w" s="T705">kudajdə</ts>
                  <nts id="Seg_2753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706.tx.1" id="Seg_2755" n="HIAT:w" s="T706">numan</ts>
                  <nts id="Seg_2756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2758" n="HIAT:w" s="T706.tx.1">üzəsʼtə</ts>
                  <nts id="Seg_2759" n="HIAT:ip">.</nts>
                  <nts id="Seg_2760" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T714" id="Seg_2762" n="HIAT:u" s="T707">
                  <ts e="T708" id="Seg_2764" n="HIAT:w" s="T707">Ej</ts>
                  <nts id="Seg_2765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2767" n="HIAT:w" s="T708">baʔlim</ts>
                  <nts id="Seg_2768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2770" n="HIAT:w" s="T709">tănan</ts>
                  <nts id="Seg_2771" n="HIAT:ip">,</nts>
                  <nts id="Seg_2772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2774" n="HIAT:w" s="T710">a</ts>
                  <nts id="Seg_2775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2777" n="HIAT:w" s="T711">munolam</ts>
                  <nts id="Seg_2778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2780" n="HIAT:w" s="T712">jakše</ts>
                  <nts id="Seg_2781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2783" n="HIAT:w" s="T713">amnolubaʔ</ts>
                  <nts id="Seg_2784" n="HIAT:ip">.</nts>
                  <nts id="Seg_2785" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T724" id="Seg_2787" n="HIAT:u" s="T714">
                  <ts e="T715" id="Seg_2789" n="HIAT:w" s="T714">Dĭgəttə</ts>
                  <nts id="Seg_2790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2792" n="HIAT:w" s="T715">dĭ</ts>
                  <nts id="Seg_2793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2795" n="HIAT:w" s="T716">ine</ts>
                  <nts id="Seg_2796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2797" n="HIAT:ip">(</nts>
                  <ts e="T718" id="Seg_2799" n="HIAT:w" s="T717">körerbi</ts>
                  <nts id="Seg_2800" n="HIAT:ip">)</nts>
                  <nts id="Seg_2801" n="HIAT:ip">,</nts>
                  <nts id="Seg_2802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2803" n="HIAT:ip">(</nts>
                  <ts e="T719" id="Seg_2805" n="HIAT:w" s="T718">m-</ts>
                  <nts id="Seg_2806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2808" n="HIAT:w" s="T719">miʔ=</ts>
                  <nts id="Seg_2809" n="HIAT:ip">)</nts>
                  <nts id="Seg_2810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2812" n="HIAT:w" s="T720">măn</ts>
                  <nts id="Seg_2813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2815" n="HIAT:w" s="T721">dĭzi</ts>
                  <nts id="Seg_2816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2818" n="HIAT:w" s="T722">kambiam</ts>
                  <nts id="Seg_2819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2821" n="HIAT:w" s="T723">Permʼakovtə</ts>
                  <nts id="Seg_2822" n="HIAT:ip">.</nts>
                  <nts id="Seg_2823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T729" id="Seg_2825" n="HIAT:u" s="T724">
                  <ts e="T725" id="Seg_2827" n="HIAT:w" s="T724">Ara</ts>
                  <nts id="Seg_2828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2830" n="HIAT:w" s="T725">ibiem</ts>
                  <nts id="Seg_2831" n="HIAT:ip">,</nts>
                  <nts id="Seg_2832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2834" n="HIAT:w" s="T726">deʔpibeʔ</ts>
                  <nts id="Seg_2835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2836" n="HIAT:ip">(</nts>
                  <ts e="T729" id="Seg_2838" n="HIAT:w" s="T727">s-</ts>
                  <nts id="Seg_2839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2840" n="HIAT:ip">)</nts>
                  <nts id="Seg_2841" n="HIAT:ip">…</nts>
                  <nts id="Seg_2842" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T733" id="Seg_2844" n="HIAT:u" s="T729">
                  <ts e="T730" id="Seg_2846" n="HIAT:w" s="T729">Stoldə</ts>
                  <nts id="Seg_2847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2849" n="HIAT:w" s="T730">oʔbdəbibeʔ</ts>
                  <nts id="Seg_2850" n="HIAT:ip">,</nts>
                  <nts id="Seg_2851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2853" n="HIAT:w" s="T731">il</ts>
                  <nts id="Seg_2854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2856" n="HIAT:w" s="T732">oʔbdəbibeʔ</ts>
                  <nts id="Seg_2857" n="HIAT:ip">.</nts>
                  <nts id="Seg_2858" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T741" id="Seg_2860" n="HIAT:u" s="T733">
                  <ts e="T734" id="Seg_2862" n="HIAT:w" s="T733">Ara</ts>
                  <nts id="Seg_2863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2865" n="HIAT:w" s="T734">bĭʔpiʔi</ts>
                  <nts id="Seg_2866" n="HIAT:ip">,</nts>
                  <nts id="Seg_2867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2869" n="HIAT:w" s="T735">ambiʔi</ts>
                  <nts id="Seg_2870" n="HIAT:ip">,</nts>
                  <nts id="Seg_2871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2873" n="HIAT:w" s="T736">dĭgəttə</ts>
                  <nts id="Seg_2874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2876" n="HIAT:w" s="T737">miʔ</ts>
                  <nts id="Seg_2877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2878" n="HIAT:ip">(</nts>
                  <ts e="T739" id="Seg_2880" n="HIAT:w" s="T738">ka-</ts>
                  <nts id="Seg_2881" n="HIAT:ip">)</nts>
                  <nts id="Seg_2882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2884" n="HIAT:w" s="T739">kambibaʔ</ts>
                  <nts id="Seg_2885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2887" n="HIAT:w" s="T740">stanitsanə</ts>
                  <nts id="Seg_2888" n="HIAT:ip">.</nts>
                  <nts id="Seg_2889" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T745" id="Seg_2891" n="HIAT:u" s="T741">
                  <ts e="T742" id="Seg_2893" n="HIAT:w" s="T741">Dĭgəttə</ts>
                  <nts id="Seg_2894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2895" n="HIAT:ip">(</nts>
                  <ts e="T743" id="Seg_2897" n="HIAT:w" s="T742">manobiam</ts>
                  <nts id="Seg_2898" n="HIAT:ip">)</nts>
                  <nts id="Seg_2899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2901" n="HIAT:w" s="T743">nagur</ts>
                  <nts id="Seg_2902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2904" n="HIAT:w" s="T744">kö</ts>
                  <nts id="Seg_2905" n="HIAT:ip">.</nts>
                  <nts id="Seg_2906" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T751" id="Seg_2908" n="HIAT:u" s="T745">
                  <ts e="T746" id="Seg_2910" n="HIAT:w" s="T745">Dĭgəttə</ts>
                  <nts id="Seg_2911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2913" n="HIAT:w" s="T746">văjna</ts>
                  <nts id="Seg_2914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2916" n="HIAT:w" s="T747">ibi</ts>
                  <nts id="Seg_2917" n="HIAT:ip">,</nts>
                  <nts id="Seg_2918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2920" n="HIAT:w" s="T748">dĭm</ts>
                  <nts id="Seg_2921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_2923" n="HIAT:w" s="T749">ibiʔi</ts>
                  <nts id="Seg_2924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_2926" n="HIAT:w" s="T750">văjnanə</ts>
                  <nts id="Seg_2927" n="HIAT:ip">.</nts>
                  <nts id="Seg_2928" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T757" id="Seg_2930" n="HIAT:u" s="T751">
                  <ts e="T752" id="Seg_2932" n="HIAT:w" s="T751">Dĭgəttə</ts>
                  <nts id="Seg_2933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2935" n="HIAT:w" s="T752">măn</ts>
                  <nts id="Seg_2936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_2938" n="HIAT:w" s="T753">nanəʔzəbi</ts>
                  <nts id="Seg_2939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_2941" n="HIAT:w" s="T754">ibiem</ts>
                  <nts id="Seg_2942" n="HIAT:ip">,</nts>
                  <nts id="Seg_2943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2944" n="HIAT:ip">(</nts>
                  <ts e="T756" id="Seg_2946" n="HIAT:w" s="T755">koʔbdo</ts>
                  <nts id="Seg_2947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_2949" n="HIAT:w" s="T756">dĭ</ts>
                  <nts id="Seg_2950" n="HIAT:ip">)</nts>
                  <nts id="Seg_2951" n="HIAT:ip">.</nts>
                  <nts id="Seg_2952" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T763" id="Seg_2954" n="HIAT:u" s="T757">
                  <ts e="T758" id="Seg_2956" n="HIAT:w" s="T757">Dĭgəttə</ts>
                  <nts id="Seg_2957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_2959" n="HIAT:w" s="T758">dĭ</ts>
                  <nts id="Seg_2960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_2962" n="HIAT:w" s="T759">šobi</ts>
                  <nts id="Seg_2963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_2965" n="HIAT:w" s="T760">văjnagə</ts>
                  <nts id="Seg_2966" n="HIAT:ip">,</nts>
                  <nts id="Seg_2967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_2969" n="HIAT:w" s="T761">раненый</ts>
                  <nts id="Seg_2970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_2972" n="HIAT:w" s="T762">ibi</ts>
                  <nts id="Seg_2973" n="HIAT:ip">.</nts>
                  <nts id="Seg_2974" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T770" id="Seg_2976" n="HIAT:u" s="T763">
                  <ts e="T764" id="Seg_2978" n="HIAT:w" s="T763">Dĭgəttə</ts>
                  <nts id="Seg_2979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2981" n="HIAT:w" s="T764">iššo</ts>
                  <nts id="Seg_2982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_2984" n="HIAT:w" s="T765">amnobibeʔ</ts>
                  <nts id="Seg_2985" n="HIAT:ip">,</nts>
                  <nts id="Seg_2986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_2988" n="HIAT:w" s="T766">măn</ts>
                  <nts id="Seg_2989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_2991" n="HIAT:w" s="T767">bazo</ts>
                  <nts id="Seg_2992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_2994" n="HIAT:w" s="T768">nanəʔzəbi</ts>
                  <nts id="Seg_2995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_2997" n="HIAT:w" s="T769">ibi</ts>
                  <nts id="Seg_2998" n="HIAT:ip">.</nts>
                  <nts id="Seg_2999" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T779" id="Seg_3001" n="HIAT:u" s="T770">
                  <ts e="T771" id="Seg_3003" n="HIAT:w" s="T770">Dĭgəttə</ts>
                  <nts id="Seg_3004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_3006" n="HIAT:w" s="T771">dĭ</ts>
                  <nts id="Seg_3007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3008" n="HIAT:ip">(</nts>
                  <ts e="T773" id="Seg_3010" n="HIAT:w" s="T772">n-DMG-nobi</ts>
                  <nts id="Seg_3011" n="HIAT:ip">)</nts>
                  <nts id="Seg_3012" n="HIAT:ip">,</nts>
                  <nts id="Seg_3013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_3015" n="HIAT:w" s="T773">dĭ</ts>
                  <nts id="Seg_3016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_3018" n="HIAT:w" s="T774">măna</ts>
                  <nts id="Seg_3019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_3021" n="HIAT:w" s="T775">sürerbi</ts>
                  <nts id="Seg_3022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_3024" n="HIAT:w" s="T776">i</ts>
                  <nts id="Seg_3025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_3027" n="HIAT:w" s="T777">dĭm</ts>
                  <nts id="Seg_3028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_3030" n="HIAT:w" s="T778">ibi</ts>
                  <nts id="Seg_3031" n="HIAT:ip">.</nts>
                  <nts id="Seg_3032" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T784" id="Seg_3034" n="HIAT:u" s="T779">
                  <ts e="T780" id="Seg_3036" n="HIAT:w" s="T779">Iššo</ts>
                  <nts id="Seg_3037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_3039" n="HIAT:w" s="T780">onʼiʔ</ts>
                  <nts id="Seg_3040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3041" n="HIAT:ip">(</nts>
                  <ts e="T782" id="Seg_3043" n="HIAT:w" s="T781">ni-</ts>
                  <nts id="Seg_3044" n="HIAT:ip">)</nts>
                  <nts id="Seg_3045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_3047" n="HIAT:w" s="T782">nʼi</ts>
                  <nts id="Seg_3048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_3050" n="HIAT:w" s="T783">deʔpiem</ts>
                  <nts id="Seg_3051" n="HIAT:ip">.</nts>
                  <nts id="Seg_3052" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T792" id="Seg_3054" n="HIAT:u" s="T784">
                  <ts e="T785" id="Seg_3056" n="HIAT:w" s="T784">Dĭgəttə</ts>
                  <nts id="Seg_3057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_3059" n="HIAT:w" s="T785">maʔnʼi</ts>
                  <nts id="Seg_3060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_3062" n="HIAT:w" s="T786">šobiam</ts>
                  <nts id="Seg_3063" n="HIAT:ip">,</nts>
                  <nts id="Seg_3064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3065" n="HIAT:ip">(</nts>
                  <ts e="T788" id="Seg_3067" n="HIAT:w" s="T787">aba-</ts>
                  <nts id="Seg_3068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_3070" n="HIAT:w" s="T788">aban-</ts>
                  <nts id="Seg_3071" n="HIAT:ip">)</nts>
                  <nts id="Seg_3072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_3074" n="HIAT:w" s="T789">abanə</ts>
                  <nts id="Seg_3075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_3077" n="HIAT:w" s="T790">i</ts>
                  <nts id="Seg_3078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_3080" n="HIAT:w" s="T791">ianə</ts>
                  <nts id="Seg_3081" n="HIAT:ip">.</nts>
                  <nts id="Seg_3082" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T801" id="Seg_3084" n="HIAT:u" s="T792">
                  <ts e="T793" id="Seg_3086" n="HIAT:w" s="T792">Dĭgəttə</ts>
                  <nts id="Seg_3087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_3089" n="HIAT:w" s="T793">külaːmbiʔi</ts>
                  <nts id="Seg_3090" n="HIAT:ip">,</nts>
                  <nts id="Seg_3091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_3093" n="HIAT:w" s="T794">koʔbdo</ts>
                  <nts id="Seg_3094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_3096" n="HIAT:w" s="T795">i</ts>
                  <nts id="Seg_3097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_3099" n="HIAT:w" s="T796">nʼi</ts>
                  <nts id="Seg_3100" n="HIAT:ip">,</nts>
                  <nts id="Seg_3101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3102" n="HIAT:ip">(</nts>
                  <ts e="T798" id="Seg_3104" n="HIAT:w" s="T797">măn</ts>
                  <nts id="Seg_3105" n="HIAT:ip">)</nts>
                  <nts id="Seg_3106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_3108" n="HIAT:w" s="T798">unnʼa</ts>
                  <nts id="Seg_3109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3110" n="HIAT:ip">(</nts>
                  <ts e="T800" id="Seg_3112" n="HIAT:w" s="T799">mola-</ts>
                  <nts id="Seg_3113" n="HIAT:ip">)</nts>
                  <nts id="Seg_3114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_3116" n="HIAT:w" s="T800">molambiam</ts>
                  <nts id="Seg_3117" n="HIAT:ip">.</nts>
                  <nts id="Seg_3118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T807" id="Seg_3120" n="HIAT:u" s="T801">
                  <ts e="T802" id="Seg_3122" n="HIAT:w" s="T801">Oʔkʼe</ts>
                  <nts id="Seg_3123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_3125" n="HIAT:w" s="T802">amnobiam</ts>
                  <nts id="Seg_3126" n="HIAT:ip">,</nts>
                  <nts id="Seg_3127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_3129" n="HIAT:w" s="T803">bazo</ts>
                  <nts id="Seg_3130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_3132" n="HIAT:w" s="T804">kuza</ts>
                  <nts id="Seg_3133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_3135" n="HIAT:w" s="T805">măna</ts>
                  <nts id="Seg_3136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_3138" n="HIAT:w" s="T806">monoʔkobi</ts>
                  <nts id="Seg_3139" n="HIAT:ip">.</nts>
                  <nts id="Seg_3140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T810" id="Seg_3142" n="HIAT:u" s="T807">
                  <ts e="T808" id="Seg_3144" n="HIAT:w" s="T807">Măn</ts>
                  <nts id="Seg_3145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_3147" n="HIAT:w" s="T808">kambiam</ts>
                  <nts id="Seg_3148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_3150" n="HIAT:w" s="T809">dĭzi</ts>
                  <nts id="Seg_3151" n="HIAT:ip">.</nts>
                  <nts id="Seg_3152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T820" id="Seg_3154" n="HIAT:u" s="T810">
                  <ts e="T811" id="Seg_3156" n="HIAT:w" s="T810">Amnobiam</ts>
                  <nts id="Seg_3157" n="HIAT:ip">,</nts>
                  <nts id="Seg_3158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_3160" n="HIAT:w" s="T811">oʔb</ts>
                  <nts id="Seg_3161" n="HIAT:ip">,</nts>
                  <nts id="Seg_3162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_3164" n="HIAT:w" s="T812">šide</ts>
                  <nts id="Seg_3165" n="HIAT:ip">,</nts>
                  <nts id="Seg_3166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_3168" n="HIAT:w" s="T813">nagur</ts>
                  <nts id="Seg_3169" n="HIAT:ip">,</nts>
                  <nts id="Seg_3170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_3172" n="HIAT:w" s="T814">teʔtə</ts>
                  <nts id="Seg_3173" n="HIAT:ip">,</nts>
                  <nts id="Seg_3174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_3176" n="HIAT:w" s="T815">sumna</ts>
                  <nts id="Seg_3177" n="HIAT:ip">,</nts>
                  <nts id="Seg_3178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_3180" n="HIAT:w" s="T816">muktuʔ</ts>
                  <nts id="Seg_3181" n="HIAT:ip">,</nts>
                  <nts id="Seg_3182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_3184" n="HIAT:w" s="T817">sejʔpü</ts>
                  <nts id="Seg_3185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_3187" n="HIAT:w" s="T818">kö</ts>
                  <nts id="Seg_3188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_3190" n="HIAT:w" s="T819">amnobiam</ts>
                  <nts id="Seg_3191" n="HIAT:ip">.</nts>
                  <nts id="Seg_3192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T823" id="Seg_3194" n="HIAT:u" s="T820">
                  <ts e="T821" id="Seg_3196" n="HIAT:w" s="T820">Šide</ts>
                  <nts id="Seg_3197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_3199" n="HIAT:w" s="T821">ešši</ts>
                  <nts id="Seg_3200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_3202" n="HIAT:w" s="T822">ibiʔi</ts>
                  <nts id="Seg_3203" n="HIAT:ip">.</nts>
                  <nts id="Seg_3204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T830" id="Seg_3206" n="HIAT:u" s="T823">
                  <nts id="Seg_3207" n="HIAT:ip">(</nts>
                  <ts e="T824" id="Seg_3209" n="HIAT:w" s="T823">Ugandə</ts>
                  <nts id="Seg_3210" n="HIAT:ip">)</nts>
                  <nts id="Seg_3211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3213" n="HIAT:w" s="T824">ej</ts>
                  <nts id="Seg_3214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_3216" n="HIAT:w" s="T825">jakše</ts>
                  <nts id="Seg_3217" n="HIAT:ip">,</nts>
                  <nts id="Seg_3218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_3220" n="HIAT:w" s="T826">ara</ts>
                  <nts id="Seg_3221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_3223" n="HIAT:w" s="T827">bĭʔpi</ts>
                  <nts id="Seg_3224" n="HIAT:ip">,</nts>
                  <nts id="Seg_3225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3226" n="HIAT:ip">(</nts>
                  <ts e="T829" id="Seg_3228" n="HIAT:w" s="T828">dʼabrolaʔpi</ts>
                  <nts id="Seg_3229" n="HIAT:ip">)</nts>
                  <nts id="Seg_3230" n="HIAT:ip">,</nts>
                  <nts id="Seg_3231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_3233" n="HIAT:w" s="T829">kudonzlaʔpi</ts>
                  <nts id="Seg_3234" n="HIAT:ip">.</nts>
                  <nts id="Seg_3235" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T838" id="Seg_3237" n="HIAT:u" s="T830">
                  <nts id="Seg_3238" n="HIAT:ip">(</nts>
                  <ts e="T831" id="Seg_3240" n="HIAT:w" s="T830">Kamen</ts>
                  <nts id="Seg_3241" n="HIAT:ip">)</nts>
                  <nts id="Seg_3242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3244" n="HIAT:w" s="T831">Kamen</ts>
                  <nts id="Seg_3245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_3247" n="HIAT:w" s="T832">baʔluʔpiam</ts>
                  <nts id="Seg_3248" n="HIAT:ip">,</nts>
                  <nts id="Seg_3249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3250" n="HIAT:ip">(</nts>
                  <ts e="T834" id="Seg_3252" n="HIAT:w" s="T833">bar</ts>
                  <nts id="Seg_3253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_3255" n="HIAT:w" s="T834">ulum=</ts>
                  <nts id="Seg_3256" n="HIAT:ip">)</nts>
                  <nts id="Seg_3257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_3259" n="HIAT:w" s="T835">bar</ts>
                  <nts id="Seg_3260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_3262" n="HIAT:w" s="T836">eʔbdəm</ts>
                  <nts id="Seg_3263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_3265" n="HIAT:w" s="T837">sajnʼeʔpi</ts>
                  <nts id="Seg_3266" n="HIAT:ip">.</nts>
                  <nts id="Seg_3267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T844" id="Seg_3269" n="HIAT:u" s="T838">
                  <nts id="Seg_3270" n="HIAT:ip">(</nts>
                  <ts e="T839" id="Seg_3272" n="HIAT:w" s="T838">Vekžen</ts>
                  <nts id="Seg_3273" n="HIAT:ip">)</nts>
                  <nts id="Seg_3274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_3276" n="HIAT:w" s="T839">tăŋ</ts>
                  <nts id="Seg_3277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_3279" n="HIAT:w" s="T840">münörbi</ts>
                  <nts id="Seg_3280" n="HIAT:ip">,</nts>
                  <nts id="Seg_3281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_3283" n="HIAT:w" s="T841">ažnə</ts>
                  <nts id="Seg_3284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_3286" n="HIAT:w" s="T842">kem</ts>
                  <nts id="Seg_3287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_3289" n="HIAT:w" s="T843">ibi</ts>
                  <nts id="Seg_3290" n="HIAT:ip">.</nts>
                  <nts id="Seg_3291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T849" id="Seg_3293" n="HIAT:u" s="T844">
                  <ts e="T845" id="Seg_3295" n="HIAT:w" s="T844">Dĭgəttə</ts>
                  <nts id="Seg_3296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_3298" n="HIAT:w" s="T845">măn</ts>
                  <nts id="Seg_3299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3300" n="HIAT:ip">(</nts>
                  <ts e="T847" id="Seg_3302" n="HIAT:w" s="T846">nʼim=</ts>
                  <nts id="Seg_3303" n="HIAT:ip">)</nts>
                  <nts id="Seg_3304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_3306" n="HIAT:w" s="T847">dĭm</ts>
                  <nts id="Seg_3307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_3309" n="HIAT:w" s="T848">barəʔluʔpiam</ts>
                  <nts id="Seg_3310" n="HIAT:ip">.</nts>
                  <nts id="Seg_3311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T853" id="Seg_3313" n="HIAT:u" s="T849">
                  <ts e="T850" id="Seg_3315" n="HIAT:w" s="T849">Măn</ts>
                  <nts id="Seg_3316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_3318" n="HIAT:w" s="T850">onʼiʔ</ts>
                  <nts id="Seg_3319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_3321" n="HIAT:w" s="T851">nʼim</ts>
                  <nts id="Seg_3322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_3324" n="HIAT:w" s="T852">ibi</ts>
                  <nts id="Seg_3325" n="HIAT:ip">.</nts>
                  <nts id="Seg_3326" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T864" id="Seg_3328" n="HIAT:u" s="T853">
                  <ts e="T854" id="Seg_3330" n="HIAT:w" s="T853">Tüj</ts>
                  <nts id="Seg_3331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_3333" n="HIAT:w" s="T854">măndəm</ts>
                  <nts id="Seg_3334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_3336" n="HIAT:w" s="T855">šindinədə:</ts>
                  <nts id="Seg_3337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_3339" n="HIAT:w" s="T856">ej</ts>
                  <nts id="Seg_3340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T859" id="Seg_3342" n="HIAT:w" s="T858">tibinə</ts>
                  <nts id="Seg_3343" n="HIAT:ip">,</nts>
                  <nts id="Seg_3344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_3346" n="HIAT:w" s="T859">unnʼa</ts>
                  <nts id="Seg_3347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_3349" n="HIAT:w" s="T860">amnolam</ts>
                  <nts id="Seg_3350" n="HIAT:ip">,</nts>
                  <nts id="Seg_3351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_3353" n="HIAT:w" s="T861">nʼizi</ts>
                  <nts id="Seg_3354" n="HIAT:ip">,</nts>
                  <nts id="Seg_3355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_3357" n="HIAT:w" s="T862">bostə</ts>
                  <nts id="Seg_3358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_3360" n="HIAT:w" s="T863">nʼizi</ts>
                  <nts id="Seg_3361" n="HIAT:ip">.</nts>
                  <nts id="Seg_3362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T870" id="Seg_3364" n="HIAT:u" s="T864">
                  <ts e="T865" id="Seg_3366" n="HIAT:w" s="T864">Dĭgəttə</ts>
                  <nts id="Seg_3367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_3369" n="HIAT:w" s="T865">onʼiʔ</ts>
                  <nts id="Seg_3370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_3372" n="HIAT:w" s="T866">nʼi</ts>
                  <nts id="Seg_3373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_3375" n="HIAT:w" s="T867">davaj</ts>
                  <nts id="Seg_3376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_3378" n="HIAT:w" s="T868">măna</ts>
                  <nts id="Seg_3379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_3381" n="HIAT:w" s="T869">monoʔkosʼtə</ts>
                  <nts id="Seg_3382" n="HIAT:ip">.</nts>
                  <nts id="Seg_3383" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T874" id="Seg_3385" n="HIAT:u" s="T870">
                  <ts e="T871" id="Seg_3387" n="HIAT:w" s="T870">Măn</ts>
                  <nts id="Seg_3388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_3390" n="HIAT:w" s="T871">не</ts>
                  <nts id="Seg_3391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_3393" n="HIAT:w" s="T872">хотел</ts>
                  <nts id="Seg_3394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_3396" n="HIAT:w" s="T873">идти</ts>
                  <nts id="Seg_3397" n="HIAT:ip">.</nts>
                  <nts id="Seg_3398" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T876" id="Seg_3400" n="HIAT:u" s="T874">
                  <ts e="T875" id="Seg_3402" n="HIAT:w" s="T874">Ej</ts>
                  <nts id="Seg_3403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_3405" n="HIAT:w" s="T875">kuvandoliam</ts>
                  <nts id="Seg_3406" n="HIAT:ip">.</nts>
                  <nts id="Seg_3407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T879" id="Seg_3409" n="HIAT:u" s="T876">
                  <nts id="Seg_3410" n="HIAT:ip">(</nts>
                  <ts e="T877" id="Seg_3412" n="HIAT:w" s="T876">Kadəlbə</ts>
                  <nts id="Seg_3413" n="HIAT:ip">)</nts>
                  <nts id="Seg_3414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_3416" n="HIAT:w" s="T877">Kadəldə</ts>
                  <nts id="Seg_3417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_3419" n="HIAT:w" s="T878">nʼišpək</ts>
                  <nts id="Seg_3420" n="HIAT:ip">.</nts>
                  <nts id="Seg_3421" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T881" id="Seg_3423" n="HIAT:u" s="T879">
                  <ts e="T880" id="Seg_3425" n="HIAT:w" s="T879">Ej</ts>
                  <nts id="Seg_3426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T881" id="Seg_3428" n="HIAT:w" s="T880">kalam</ts>
                  <nts id="Seg_3429" n="HIAT:ip">.</nts>
                  <nts id="Seg_3430" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T886" id="Seg_3432" n="HIAT:u" s="T881">
                  <nts id="Seg_3433" n="HIAT:ip">(</nts>
                  <ts e="T882" id="Seg_3435" n="HIAT:w" s="T881">Dĭ</ts>
                  <nts id="Seg_3436" n="HIAT:ip">)</nts>
                  <nts id="Seg_3437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_3439" n="HIAT:w" s="T882">măndə:</ts>
                  <nts id="Seg_3440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_3442" n="HIAT:w" s="T883">Davaj</ts>
                  <nts id="Seg_3443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_3445" n="HIAT:w" s="T884">amnosʼtə</ts>
                  <nts id="Seg_3446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_3448" n="HIAT:w" s="T885">tănzi</ts>
                  <nts id="Seg_3449" n="HIAT:ip">.</nts>
                  <nts id="Seg_3450" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T890" id="Seg_3452" n="HIAT:u" s="T886">
                  <ts e="T1055" id="Seg_3454" n="HIAT:w" s="T886">Nu</ts>
                  <nts id="Seg_3455" n="HIAT:ip">,</nts>
                  <nts id="Seg_3456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887" id="Seg_3458" n="HIAT:w" s="T1055">tenöbiam</ts>
                  <nts id="Seg_3459" n="HIAT:ip">,</nts>
                  <nts id="Seg_3460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T888" id="Seg_3462" n="HIAT:w" s="T887">tenöbiam</ts>
                  <nts id="Seg_3463" n="HIAT:ip">,</nts>
                  <nts id="Seg_3464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T889" id="Seg_3466" n="HIAT:w" s="T888">dĭgəttə</ts>
                  <nts id="Seg_3467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_3469" n="HIAT:w" s="T889">kambiam</ts>
                  <nts id="Seg_3470" n="HIAT:ip">.</nts>
                  <nts id="Seg_3471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T899" id="Seg_3473" n="HIAT:u" s="T890">
                  <ts e="T891" id="Seg_3475" n="HIAT:w" s="T890">Dĭgəttə</ts>
                  <nts id="Seg_3476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T892" id="Seg_3478" n="HIAT:w" s="T891">bazo:</ts>
                  <nts id="Seg_3479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3480" n="HIAT:ip">(</nts>
                  <ts e="T893" id="Seg_3482" n="HIAT:w" s="T892">Ba-</ts>
                  <nts id="Seg_3483" n="HIAT:ip">)</nts>
                  <nts id="Seg_3484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_3486" n="HIAT:w" s="T893">barəʔlim</ts>
                  <nts id="Seg_3487" n="HIAT:ip">,</nts>
                  <nts id="Seg_3488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_3490" n="HIAT:w" s="T894">măndəm</ts>
                  <nts id="Seg_3491" n="HIAT:ip">,</nts>
                  <nts id="Seg_3492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_3494" n="HIAT:w" s="T895">tănan</ts>
                  <nts id="Seg_3495" n="HIAT:ip">,</nts>
                  <nts id="Seg_3496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_3498" n="HIAT:w" s="T896">ej</ts>
                  <nts id="Seg_3499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_3501" n="HIAT:w" s="T897">amnolam</ts>
                  <nts id="Seg_3502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_3504" n="HIAT:w" s="T898">tănzi</ts>
                  <nts id="Seg_3505" n="HIAT:ip">.</nts>
                  <nts id="Seg_3506" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T905" id="Seg_3508" n="HIAT:u" s="T899">
                  <ts e="T900" id="Seg_3510" n="HIAT:w" s="T899">Dibər</ts>
                  <nts id="Seg_3511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_3513" n="HIAT:w" s="T900">davaj</ts>
                  <nts id="Seg_3514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_3516" n="HIAT:w" s="T901">dʼorzittə</ts>
                  <nts id="Seg_3517" n="HIAT:ip">,</nts>
                  <nts id="Seg_3518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_3520" n="HIAT:w" s="T902">tăŋ</ts>
                  <nts id="Seg_3521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T904" id="Seg_3523" n="HIAT:w" s="T903">dʼorbi</ts>
                  <nts id="Seg_3524" n="HIAT:ip">,</nts>
                  <nts id="Seg_3525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T905" id="Seg_3527" n="HIAT:w" s="T904">ugandə</ts>
                  <nts id="Seg_3528" n="HIAT:ip">.</nts>
                  <nts id="Seg_3529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T914" id="Seg_3531" n="HIAT:u" s="T905">
                  <ts e="T906" id="Seg_3533" n="HIAT:w" s="T905">Dĭgəttə</ts>
                  <nts id="Seg_3534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T907" id="Seg_3536" n="HIAT:w" s="T906">măn</ts>
                  <nts id="Seg_3537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_3539" n="HIAT:w" s="T907">mălliam:</ts>
                  <nts id="Seg_3540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3541" n="HIAT:ip">(</nts>
                  <nts id="Seg_3542" n="HIAT:ip">(</nts>
                  <ats e="T909" id="Seg_3543" n="HIAT:non-pho" s="T908">…</ats>
                  <nts id="Seg_3544" n="HIAT:ip">)</nts>
                  <nts id="Seg_3545" n="HIAT:ip">)</nts>
                  <nts id="Seg_3546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_3548" n="HIAT:w" s="T909">tura</ts>
                  <nts id="Seg_3549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_3551" n="HIAT:w" s="T910">nuldəbəj</ts>
                  <nts id="Seg_3552" n="HIAT:ip">,</nts>
                  <nts id="Seg_3553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T912" id="Seg_3555" n="HIAT:w" s="T911">dĭgəttə</ts>
                  <nts id="Seg_3556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T913" id="Seg_3558" n="HIAT:w" s="T912">pušaj</ts>
                  <nts id="Seg_3559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T914" id="Seg_3561" n="HIAT:w" s="T913">kaləj</ts>
                  <nts id="Seg_3562" n="HIAT:ip">.</nts>
                  <nts id="Seg_3563" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T925" id="Seg_3565" n="HIAT:u" s="T914">
                  <ts e="T915" id="Seg_3567" n="HIAT:w" s="T914">I</ts>
                  <nts id="Seg_3568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_3570" n="HIAT:w" s="T915">dăre</ts>
                  <nts id="Seg_3571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T917" id="Seg_3573" n="HIAT:w" s="T916">ibi:</ts>
                  <nts id="Seg_3574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T918" id="Seg_3576" n="HIAT:w" s="T917">tura</ts>
                  <nts id="Seg_3577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3578" n="HIAT:ip">(</nts>
                  <ts e="T919" id="Seg_3580" n="HIAT:w" s="T918">tol-</ts>
                  <nts id="Seg_3581" n="HIAT:ip">)</nts>
                  <nts id="Seg_3582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T920" id="Seg_3584" n="HIAT:w" s="T919">nuldəbi</ts>
                  <nts id="Seg_3585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T921" id="Seg_3587" n="HIAT:w" s="T920">i</ts>
                  <nts id="Seg_3588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1058" id="Seg_3590" n="HIAT:w" s="T921">kalla</ts>
                  <nts id="Seg_3591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_3593" n="HIAT:w" s="T1058">dʼürbi</ts>
                  <nts id="Seg_3594" n="HIAT:ip">,</nts>
                  <nts id="Seg_3595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_3597" n="HIAT:w" s="T922">măn</ts>
                  <nts id="Seg_3598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T924" id="Seg_3600" n="HIAT:w" s="T923">unnʼa</ts>
                  <nts id="Seg_3601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T925" id="Seg_3603" n="HIAT:w" s="T924">maːluʔpiam</ts>
                  <nts id="Seg_3604" n="HIAT:ip">.</nts>
                  <nts id="Seg_3605" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T936" id="Seg_3607" n="HIAT:u" s="T925">
                  <ts e="T926" id="Seg_3609" n="HIAT:w" s="T925">Tüj</ts>
                  <nts id="Seg_3610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T927" id="Seg_3612" n="HIAT:w" s="T926">măn</ts>
                  <nts id="Seg_3613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T928" id="Seg_3615" n="HIAT:w" s="T927">üge</ts>
                  <nts id="Seg_3616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T929" id="Seg_3618" n="HIAT:w" s="T928">unnʼa</ts>
                  <nts id="Seg_3619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_3621" n="HIAT:w" s="T929">amnolaʔbəm</ts>
                  <nts id="Seg_3622" n="HIAT:ip">,</nts>
                  <nts id="Seg_3623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3624" n="HIAT:ip">(</nts>
                  <ts e="T931" id="Seg_3626" n="HIAT:w" s="T930">amnolaʔbəm</ts>
                  <nts id="Seg_3627" n="HIAT:ip">)</nts>
                  <nts id="Seg_3628" n="HIAT:ip">,</nts>
                  <nts id="Seg_3629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T932" id="Seg_3631" n="HIAT:w" s="T931">už</ts>
                  <nts id="Seg_3632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T933" id="Seg_3634" n="HIAT:w" s="T932">šide</ts>
                  <nts id="Seg_3635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T934" id="Seg_3637" n="HIAT:w" s="T933">bʼeʔ</ts>
                  <nts id="Seg_3638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_3640" n="HIAT:w" s="T934">ki</ts>
                  <nts id="Seg_3641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T936" id="Seg_3643" n="HIAT:w" s="T935">amnolaʔbəm</ts>
                  <nts id="Seg_3644" n="HIAT:ip">.</nts>
                  <nts id="Seg_3645" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T939" id="Seg_3647" n="HIAT:u" s="T936">
                  <ts e="T937" id="Seg_3649" n="HIAT:w" s="T936">Kamən</ts>
                  <nts id="Seg_3650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T938" id="Seg_3652" n="HIAT:w" s="T937">dĭ</ts>
                  <nts id="Seg_3653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T939" id="Seg_3655" n="HIAT:w" s="T938">barəʔluʔpi</ts>
                  <nts id="Seg_3656" n="HIAT:ip">.</nts>
                  <nts id="Seg_3657" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T950" id="Seg_3659" n="HIAT:u" s="T939">
                  <ts e="T940" id="Seg_3661" n="HIAT:w" s="T939">Dĭgəttə</ts>
                  <nts id="Seg_3662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_3664" n="HIAT:w" s="T940">miʔ</ts>
                  <nts id="Seg_3665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T942" id="Seg_3667" n="HIAT:w" s="T941">dĭ</ts>
                  <nts id="Seg_3668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_3670" n="HIAT:w" s="T942">nʼizi</ts>
                  <nts id="Seg_3671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T944" id="Seg_3673" n="HIAT:w" s="T943">bar</ts>
                  <nts id="Seg_3674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T945" id="Seg_3676" n="HIAT:w" s="T944">pa</ts>
                  <nts id="Seg_3677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_3679" n="HIAT:w" s="T945">baltuzi</ts>
                  <nts id="Seg_3680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947" id="Seg_3682" n="HIAT:w" s="T946">jaʔpibaʔ</ts>
                  <nts id="Seg_3683" n="HIAT:ip">,</nts>
                  <nts id="Seg_3684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_3686" n="HIAT:w" s="T947">tura</ts>
                  <nts id="Seg_3687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3688" n="HIAT:ip">(</nts>
                  <ts e="T949" id="Seg_3690" n="HIAT:w" s="T948">nulzittə-</ts>
                  <nts id="Seg_3691" n="HIAT:ip">)</nts>
                  <nts id="Seg_3692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T950" id="Seg_3694" n="HIAT:w" s="T949">nuldəzittə</ts>
                  <nts id="Seg_3695" n="HIAT:ip">.</nts>
                  <nts id="Seg_3696" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T954" id="Seg_3698" n="HIAT:u" s="T950">
                  <nts id="Seg_3699" n="HIAT:ip">(</nts>
                  <ts e="T951" id="Seg_3701" n="HIAT:w" s="T950">Tazi-</ts>
                  <nts id="Seg_3702" n="HIAT:ip">)</nts>
                  <nts id="Seg_3703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T952" id="Seg_3705" n="HIAT:w" s="T951">Tažerbibaʔ</ts>
                  <nts id="Seg_3706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3707" n="HIAT:ip">(</nts>
                  <ts e="T953" id="Seg_3709" n="HIAT:w" s="T952">ine-</ts>
                  <nts id="Seg_3710" n="HIAT:ip">)</nts>
                  <nts id="Seg_3711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T954" id="Seg_3713" n="HIAT:w" s="T953">inezi</ts>
                  <nts id="Seg_3714" n="HIAT:ip">.</nts>
                  <nts id="Seg_3715" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T957" id="Seg_3717" n="HIAT:u" s="T954">
                  <ts e="T955" id="Seg_3719" n="HIAT:w" s="T954">I</ts>
                  <nts id="Seg_3720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T956" id="Seg_3722" n="HIAT:w" s="T955">tura</ts>
                  <nts id="Seg_3723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3724" n="HIAT:ip">(</nts>
                  <ts e="T957" id="Seg_3726" n="HIAT:w" s="T956">nuldəbibaʔ</ts>
                  <nts id="Seg_3727" n="HIAT:ip">)</nts>
                  <nts id="Seg_3728" n="HIAT:ip">.</nts>
                  <nts id="Seg_3729" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T965" id="Seg_3731" n="HIAT:u" s="T957">
                  <ts e="T958" id="Seg_3733" n="HIAT:w" s="T957">Tibizeŋ</ts>
                  <nts id="Seg_3734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T959" id="Seg_3736" n="HIAT:w" s="T958">jaʔpiʔi</ts>
                  <nts id="Seg_3737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T960" id="Seg_3739" n="HIAT:w" s="T959">baltuzi</ts>
                  <nts id="Seg_3740" n="HIAT:ip">,</nts>
                  <nts id="Seg_3741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T961" id="Seg_3743" n="HIAT:w" s="T960">a</ts>
                  <nts id="Seg_3744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T962" id="Seg_3746" n="HIAT:w" s="T961">măn</ts>
                  <nts id="Seg_3747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T963" id="Seg_3749" n="HIAT:w" s="T962">kubaʔi</ts>
                  <nts id="Seg_3750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964" id="Seg_3752" n="HIAT:w" s="T963">dĭʔnə</ts>
                  <nts id="Seg_3753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_3755" n="HIAT:w" s="T964">abiam</ts>
                  <nts id="Seg_3756" n="HIAT:ip">.</nts>
                  <nts id="Seg_3757" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T974" id="Seg_3759" n="HIAT:u" s="T965">
                  <ts e="T966" id="Seg_3761" n="HIAT:w" s="T965">Dĭgəttə</ts>
                  <nts id="Seg_3762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T967" id="Seg_3764" n="HIAT:w" s="T966">tsutonkaʔi</ts>
                  <nts id="Seg_3765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3766" n="HIAT:ip">(</nts>
                  <ts e="T968" id="Seg_3768" n="HIAT:w" s="T967">taz-</ts>
                  <nts id="Seg_3769" n="HIAT:ip">)</nts>
                  <nts id="Seg_3770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T969" id="Seg_3772" n="HIAT:w" s="T968">tažerbiʔi</ts>
                  <nts id="Seg_3773" n="HIAT:ip">,</nts>
                  <nts id="Seg_3774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T970" id="Seg_3776" n="HIAT:w" s="T969">пол</ts>
                  <nts id="Seg_3777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T971" id="Seg_3779" n="HIAT:w" s="T970">azittə</ts>
                  <nts id="Seg_3780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3781" n="HIAT:ip">(</nts>
                  <ts e="T972" id="Seg_3783" n="HIAT:w" s="T971">i</ts>
                  <nts id="Seg_3784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T973" id="Seg_3786" n="HIAT:w" s="T972">потолок</ts>
                  <nts id="Seg_3787" n="HIAT:ip">)</nts>
                  <nts id="Seg_3788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T974" id="Seg_3790" n="HIAT:w" s="T973">azittə</ts>
                  <nts id="Seg_3791" n="HIAT:ip">.</nts>
                  <nts id="Seg_3792" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T980" id="Seg_3794" n="HIAT:u" s="T974">
                  <ts e="T975" id="Seg_3796" n="HIAT:w" s="T974">A</ts>
                  <nts id="Seg_3797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T976" id="Seg_3799" n="HIAT:w" s="T975">măn</ts>
                  <nts id="Seg_3800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3801" n="HIAT:ip">(</nts>
                  <ts e="T977" id="Seg_3803" n="HIAT:w" s="T976">kuda</ts>
                  <nts id="Seg_3804" n="HIAT:ip">)</nts>
                  <nts id="Seg_3805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T978" id="Seg_3807" n="HIAT:w" s="T977">urgo</ts>
                  <nts id="Seg_3808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T979" id="Seg_3810" n="HIAT:w" s="T978">kubaʔi</ts>
                  <nts id="Seg_3811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T980" id="Seg_3813" n="HIAT:w" s="T979">abiam</ts>
                  <nts id="Seg_3814" n="HIAT:ip">.</nts>
                  <nts id="Seg_3815" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T983" id="Seg_3817" n="HIAT:u" s="T980">
                  <ts e="T981" id="Seg_3819" n="HIAT:w" s="T980">I</ts>
                  <nts id="Seg_3820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T982" id="Seg_3822" n="HIAT:w" s="T981">jamaʔi</ts>
                  <nts id="Seg_3823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T983" id="Seg_3825" n="HIAT:w" s="T982">šöʔpiem</ts>
                  <nts id="Seg_3826" n="HIAT:ip">.</nts>
                  <nts id="Seg_3827" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T988" id="Seg_3829" n="HIAT:u" s="T983">
                  <ts e="T984" id="Seg_3831" n="HIAT:w" s="T983">A</ts>
                  <nts id="Seg_3832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T985" id="Seg_3834" n="HIAT:w" s="T984">dĭ</ts>
                  <nts id="Seg_3835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T986" id="Seg_3837" n="HIAT:w" s="T985">tože</ts>
                  <nts id="Seg_3838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T987" id="Seg_3840" n="HIAT:w" s="T986">măna</ts>
                  <nts id="Seg_3841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3842" n="HIAT:ip">(</nts>
                  <ts e="T988" id="Seg_3844" n="HIAT:w" s="T987">kabaržəbi</ts>
                  <nts id="Seg_3845" n="HIAT:ip">)</nts>
                  <nts id="Seg_3846" n="HIAT:ip">.</nts>
                  <nts id="Seg_3847" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T996" id="Seg_3849" n="HIAT:u" s="T988">
                  <ts e="T989" id="Seg_3851" n="HIAT:w" s="T988">Kubat</ts>
                  <nts id="Seg_3852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T990" id="Seg_3854" n="HIAT:w" s="T989">edəmbi</ts>
                  <nts id="Seg_3855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T991" id="Seg_3857" n="HIAT:w" s="T990">da</ts>
                  <nts id="Seg_3858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T992" id="Seg_3860" n="HIAT:w" s="T991">ej</ts>
                  <nts id="Seg_3861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T993" id="Seg_3863" n="HIAT:w" s="T992">tĭmnebi</ts>
                  <nts id="Seg_3864" n="HIAT:ip">,</nts>
                  <nts id="Seg_3865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T994" id="Seg_3867" n="HIAT:w" s="T993">măn</ts>
                  <nts id="Seg_3868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T995" id="Seg_3870" n="HIAT:w" s="T994">dĭm</ts>
                  <nts id="Seg_3871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T996" id="Seg_3873" n="HIAT:w" s="T995">tüšəlbiem</ts>
                  <nts id="Seg_3874" n="HIAT:ip">.</nts>
                  <nts id="Seg_3875" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1002" id="Seg_3877" n="HIAT:u" s="T996">
                  <ts e="T997" id="Seg_3879" n="HIAT:w" s="T996">Măn</ts>
                  <nts id="Seg_3880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T998" id="Seg_3882" n="HIAT:w" s="T997">bostə</ts>
                  <nts id="Seg_3883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3884" n="HIAT:ip">(</nts>
                  <ts e="T999" id="Seg_3886" n="HIAT:w" s="T998">tibin</ts>
                  <nts id="Seg_3887" n="HIAT:ip">)</nts>
                  <nts id="Seg_3888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1000" id="Seg_3890" n="HIAT:w" s="T999">tibinə</ts>
                  <nts id="Seg_3891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1001" id="Seg_3893" n="HIAT:w" s="T1000">jamaʔi</ts>
                  <nts id="Seg_3894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1002" id="Seg_3896" n="HIAT:w" s="T1001">šöʔpiem</ts>
                  <nts id="Seg_3897" n="HIAT:ip">.</nts>
                  <nts id="Seg_3898" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1005" id="Seg_3900" n="HIAT:u" s="T1002">
                  <ts e="T1003" id="Seg_3902" n="HIAT:w" s="T1002">I</ts>
                  <nts id="Seg_3903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1004" id="Seg_3905" n="HIAT:w" s="T1003">parga</ts>
                  <nts id="Seg_3906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1005" id="Seg_3908" n="HIAT:w" s="T1004">šöʔpiem</ts>
                  <nts id="Seg_3909" n="HIAT:ip">.</nts>
                  <nts id="Seg_3910" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1007" id="Seg_3912" n="HIAT:u" s="T1005">
                  <ts e="T1006" id="Seg_3914" n="HIAT:w" s="T1005">Ularən</ts>
                  <nts id="Seg_3915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1007" id="Seg_3917" n="HIAT:w" s="T1006">kubaʔi</ts>
                  <nts id="Seg_3918" n="HIAT:ip">.</nts>
                  <nts id="Seg_3919" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1010" id="Seg_3921" n="HIAT:u" s="T1007">
                  <ts e="T1008" id="Seg_3923" n="HIAT:w" s="T1007">I</ts>
                  <nts id="Seg_3924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1009" id="Seg_3926" n="HIAT:w" s="T1008">üžü</ts>
                  <nts id="Seg_3927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1010" id="Seg_3929" n="HIAT:w" s="T1009">šöʔpiem</ts>
                  <nts id="Seg_3930" n="HIAT:ip">.</nts>
                  <nts id="Seg_3931" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1012" id="Seg_3933" n="HIAT:u" s="T1010">
                  <ts e="T1011" id="Seg_3935" n="HIAT:w" s="T1010">Kujnektə</ts>
                  <nts id="Seg_3936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3937" n="HIAT:ip">(</nts>
                  <ts e="T1012" id="Seg_3939" n="HIAT:w" s="T1011">šöʔpiem</ts>
                  <nts id="Seg_3940" n="HIAT:ip">)</nts>
                  <nts id="Seg_3941" n="HIAT:ip">.</nts>
                  <nts id="Seg_3942" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1014" id="Seg_3944" n="HIAT:u" s="T1012">
                  <ts e="T1013" id="Seg_3946" n="HIAT:w" s="T1012">Piʔmeʔi</ts>
                  <nts id="Seg_3947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1014" id="Seg_3949" n="HIAT:w" s="T1013">šöʔpiem</ts>
                  <nts id="Seg_3950" n="HIAT:ip">.</nts>
                  <nts id="Seg_3951" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1018" id="Seg_3953" n="HIAT:u" s="T1014">
                  <ts e="T1015" id="Seg_3955" n="HIAT:w" s="T1014">Dĭʔnə</ts>
                  <nts id="Seg_3956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1016" id="Seg_3958" n="HIAT:w" s="T1015">šerbiam</ts>
                  <nts id="Seg_3959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1017" id="Seg_3961" n="HIAT:w" s="T1016">ugandə</ts>
                  <nts id="Seg_3962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3963" n="HIAT:ip">(</nts>
                  <ts e="T1056" id="Seg_3965" n="HIAT:w" s="T1017">jakše</ts>
                  <nts id="Seg_3966" n="HIAT:ip">)</nts>
                  <nts id="Seg_3967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3968" n="HIAT:ip">(</nts>
                  <nts id="Seg_3969" n="HIAT:ip">(</nts>
                  <ats e="T1018" id="Seg_3970" n="HIAT:non-pho" s="T1056">BRK</ats>
                  <nts id="Seg_3971" n="HIAT:ip">)</nts>
                  <nts id="Seg_3972" n="HIAT:ip">)</nts>
                  <nts id="Seg_3973" n="HIAT:ip">.</nts>
                  <nts id="Seg_3974" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1019" id="Seg_3976" n="HIAT:u" s="T1018">
                  <ts e="T1019" id="Seg_3978" n="HIAT:w" s="T1018">Šerbiem</ts>
                  <nts id="Seg_3979" n="HIAT:ip">.</nts>
                  <nts id="Seg_3980" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1027" id="Seg_3982" n="HIAT:u" s="T1019">
                  <nts id="Seg_3983" n="HIAT:ip">(</nts>
                  <ts e="T1020" id="Seg_3985" n="HIAT:w" s="T1019">I-</ts>
                  <nts id="Seg_3986" n="HIAT:ip">)</nts>
                  <nts id="Seg_3987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1021" id="Seg_3989" n="HIAT:w" s="T1020">Il</ts>
                  <nts id="Seg_3990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1022" id="Seg_3992" n="HIAT:w" s="T1021">bar</ts>
                  <nts id="Seg_3993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1023" id="Seg_3995" n="HIAT:w" s="T1022">măndərbiʔi:</ts>
                  <nts id="Seg_3996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1024" id="Seg_3998" n="HIAT:w" s="T1023">ugandə</ts>
                  <nts id="Seg_3999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1025" id="Seg_4001" n="HIAT:w" s="T1024">kuvas</ts>
                  <nts id="Seg_4002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1026" id="Seg_4004" n="HIAT:w" s="T1025">kuza</ts>
                  <nts id="Seg_4005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1027" id="Seg_4007" n="HIAT:w" s="T1026">molambi</ts>
                  <nts id="Seg_4008" n="HIAT:ip">.</nts>
                  <nts id="Seg_4009" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1033" id="Seg_4011" n="HIAT:u" s="T1027">
                  <ts e="T1028" id="Seg_4013" n="HIAT:w" s="T1027">Dĭgəttə</ts>
                  <nts id="Seg_4014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1029" id="Seg_4016" n="HIAT:w" s="T1028">miʔ</ts>
                  <nts id="Seg_4017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1030" id="Seg_4019" n="HIAT:w" s="T1029">dʼü</ts>
                  <nts id="Seg_4020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4021" n="HIAT:ip">(</nts>
                  <ts e="T1031" id="Seg_4023" n="HIAT:w" s="T1030">tarir-</ts>
                  <nts id="Seg_4024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1032" id="Seg_4026" n="HIAT:w" s="T1031">tajir-</ts>
                  <nts id="Seg_4027" n="HIAT:ip">)</nts>
                  <nts id="Seg_4028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4029" n="HIAT:ip">(</nts>
                  <ts e="T1033" id="Seg_4031" n="HIAT:w" s="T1032">tarirlaʔpibaʔ</ts>
                  <nts id="Seg_4032" n="HIAT:ip">)</nts>
                  <nts id="Seg_4033" n="HIAT:ip">.</nts>
                  <nts id="Seg_4034" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1035" id="Seg_4036" n="HIAT:u" s="T1033">
                  <ts e="T1034" id="Seg_4038" n="HIAT:w" s="T1033">Aš</ts>
                  <nts id="Seg_4039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1035" id="Seg_4041" n="HIAT:w" s="T1034">kuʔpibaʔ</ts>
                  <nts id="Seg_4042" n="HIAT:ip">.</nts>
                  <nts id="Seg_4043" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1041" id="Seg_4045" n="HIAT:u" s="T1035">
                  <nts id="Seg_4046" n="HIAT:ip">(</nts>
                  <ts e="T1036" id="Seg_4048" n="HIAT:w" s="T1035">Ez-</ts>
                  <nts id="Seg_4049" n="HIAT:ip">)</nts>
                  <nts id="Seg_4050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1037" id="Seg_4052" n="HIAT:w" s="T1036">Özerbi</ts>
                  <nts id="Seg_4053" n="HIAT:ip">,</nts>
                  <nts id="Seg_4054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1038" id="Seg_4056" n="HIAT:w" s="T1037">ugandə</ts>
                  <nts id="Seg_4057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1039" id="Seg_4059" n="HIAT:w" s="T1038">jakše</ts>
                  <nts id="Seg_4060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1040" id="Seg_4062" n="HIAT:w" s="T1039">ibi</ts>
                  <nts id="Seg_4063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1041" id="Seg_4065" n="HIAT:w" s="T1040">aš</ts>
                  <nts id="Seg_4066" n="HIAT:ip">.</nts>
                  <nts id="Seg_4067" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1046" id="Seg_4069" n="HIAT:u" s="T1041">
                  <ts e="T1042" id="Seg_4071" n="HIAT:w" s="T1041">Dĭgəttə</ts>
                  <nts id="Seg_4072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1043" id="Seg_4074" n="HIAT:w" s="T1042">un</ts>
                  <nts id="Seg_4075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1044" id="Seg_4077" n="HIAT:w" s="T1043">abibaʔ</ts>
                  <nts id="Seg_4078" n="HIAT:ip">,</nts>
                  <nts id="Seg_4079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1045" id="Seg_4081" n="HIAT:w" s="T1044">tʼerməndə</ts>
                  <nts id="Seg_4082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1046" id="Seg_4084" n="HIAT:w" s="T1045">mĭmbibeʔ</ts>
                  <nts id="Seg_4085" n="HIAT:ip">.</nts>
                  <nts id="Seg_4086" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1048" id="Seg_4088" n="HIAT:u" s="T1046">
                  <ts e="T1047" id="Seg_4090" n="HIAT:w" s="T1046">Ipek</ts>
                  <nts id="Seg_4091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1048" id="Seg_4093" n="HIAT:w" s="T1047">pürbibeʔ</ts>
                  <nts id="Seg_4094" n="HIAT:ip">.</nts>
                  <nts id="Seg_4095" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1052" id="Seg_4097" n="HIAT:u" s="T1048">
                  <ts e="T1049" id="Seg_4099" n="HIAT:w" s="T1048">I</ts>
                  <nts id="Seg_4100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1050" id="Seg_4102" n="HIAT:w" s="T1049">budəj</ts>
                  <nts id="Seg_4103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1051" id="Seg_4105" n="HIAT:w" s="T1050">aš</ts>
                  <nts id="Seg_4106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1052" id="Seg_4108" n="HIAT:w" s="T1051">kuʔpibaʔ</ts>
                  <nts id="Seg_4109" n="HIAT:ip">.</nts>
                  <nts id="Seg_4110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1054" id="Seg_4112" n="HIAT:u" s="T1052">
                  <nts id="Seg_4113" n="HIAT:ip">(</nts>
                  <ts e="T1053" id="Seg_4115" n="HIAT:w" s="T1052">Jakšə</ts>
                  <nts id="Seg_4116" n="HIAT:ip">)</nts>
                  <nts id="Seg_4117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4118" n="HIAT:ip">(</nts>
                  <nts id="Seg_4119" n="HIAT:ip">(</nts>
                  <ats e="T1054" id="Seg_4120" n="HIAT:non-pho" s="T1053">DMG</ats>
                  <nts id="Seg_4121" n="HIAT:ip">)</nts>
                  <nts id="Seg_4122" n="HIAT:ip">)</nts>
                  <nts id="Seg_4123" n="HIAT:ip">.</nts>
                  <nts id="Seg_4124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T1054" id="Seg_4125" n="sc" s="T0">
               <ts e="T1" id="Seg_4127" n="e" s="T0">Tăn </ts>
               <ts e="T2" id="Seg_4129" n="e" s="T1">(taldʼen) </ts>
               <ts e="T3" id="Seg_4131" n="e" s="T2">(ka-) </ts>
               <ts e="T4" id="Seg_4133" n="e" s="T3">kajaʔ </ts>
               <ts e="T5" id="Seg_4135" n="e" s="T4">ibiel </ts>
               <ts e="T6" id="Seg_4137" n="e" s="T5">dăk, </ts>
               <ts e="T7" id="Seg_4139" n="e" s="T6">edəbiel? </ts>
               <ts e="T8" id="Seg_4141" n="e" s="T7">(Edəmiel-) </ts>
               <ts e="T9" id="Seg_4143" n="e" s="T8">Edəbiem. </ts>
               <ts e="T10" id="Seg_4145" n="e" s="T9">A </ts>
               <ts e="T11" id="Seg_4147" n="e" s="T10">kumən? </ts>
               <ts e="T12" id="Seg_4149" n="e" s="T11">Sumna </ts>
               <ts e="T13" id="Seg_4151" n="e" s="T12">gram. </ts>
               <ts e="T14" id="Seg_4153" n="e" s="T13">Aktʼa </ts>
               <ts e="T15" id="Seg_4155" n="e" s="T14">deʔpiel? </ts>
               <ts e="T16" id="Seg_4157" n="e" s="T15">Deʔpiem. </ts>
               <ts e="T17" id="Seg_4159" n="e" s="T16">(Šide </ts>
               <ts e="T18" id="Seg_4161" n="e" s="T17">šăl-) </ts>
               <ts e="T19" id="Seg_4163" n="e" s="T18">Šide </ts>
               <ts e="T20" id="Seg_4165" n="e" s="T19">šălkovej. </ts>
               <ts e="T21" id="Seg_4167" n="e" s="T20">Dʼăbaktərlaʔbəbaʔ </ts>
               <ts e="T22" id="Seg_4169" n="e" s="T21">i </ts>
               <ts e="T23" id="Seg_4171" n="e" s="T22">kaknarlaʔbəbaʔ. </ts>
               <ts e="T24" id="Seg_4173" n="e" s="T23">I </ts>
               <ts e="T25" id="Seg_4175" n="e" s="T24">(arla-) </ts>
               <ts e="T26" id="Seg_4177" n="e" s="T25">ara </ts>
               <ts e="T27" id="Seg_4179" n="e" s="T26">bĭtleʔbəbeʔ. </ts>
               <ts e="T28" id="Seg_4181" n="e" s="T27">I </ts>
               <ts e="T29" id="Seg_4183" n="e" s="T28">ipek </ts>
               <ts e="T30" id="Seg_4185" n="e" s="T29">amnia. </ts>
               <ts e="T31" id="Seg_4187" n="e" s="T30">I </ts>
               <ts e="T32" id="Seg_4189" n="e" s="T31">uja </ts>
               <ts e="T33" id="Seg_4191" n="e" s="T32">amnaʔbəbaʔ. </ts>
               <ts e="T34" id="Seg_4193" n="e" s="T33">I </ts>
               <ts e="T35" id="Seg_4195" n="e" s="T34">taŋgu </ts>
               <ts e="T36" id="Seg_4197" n="e" s="T35">nʼeʔleʔbəbeʔ </ts>
               <ts e="T37" id="Seg_4199" n="e" s="T36">bar. </ts>
               <ts e="T38" id="Seg_4201" n="e" s="T37">Miʔ </ts>
               <ts e="T39" id="Seg_4203" n="e" s="T38">tondə </ts>
               <ts e="T40" id="Seg_4205" n="e" s="T39">kuza </ts>
               <ts e="T41" id="Seg_4207" n="e" s="T40">amnolaʔbə, </ts>
               <ts e="T42" id="Seg_4209" n="e" s="T41">dĭn </ts>
               <ts e="T43" id="Seg_4211" n="e" s="T42">nʼi </ts>
               <ts e="T44" id="Seg_4213" n="e" s="T43">miʔnʼibeʔ </ts>
               <ts e="T45" id="Seg_4215" n="e" s="T44">šobi. </ts>
               <ts e="T46" id="Seg_4217" n="e" s="T45">Jašɨktə </ts>
               <ts e="T47" id="Seg_4219" n="e" s="T46">(păʔpi), </ts>
               <ts e="T48" id="Seg_4221" n="e" s="T47">i </ts>
               <ts e="T49" id="Seg_4223" n="e" s="T48">škaptə </ts>
               <ts e="T50" id="Seg_4225" n="e" s="T49">(păʔpi). </ts>
               <ts e="T51" id="Seg_4227" n="e" s="T50">Măn </ts>
               <ts e="T52" id="Seg_4229" n="e" s="T51">(mămbiam=) </ts>
               <ts e="T53" id="Seg_4231" n="e" s="T52">mămbiam: </ts>
               <ts e="T54" id="Seg_4233" n="e" s="T53">Ĭmbi </ts>
               <ts e="T55" id="Seg_4235" n="e" s="T54">((…))? </ts>
               <ts e="T56" id="Seg_4237" n="e" s="T55">Măna </ts>
               <ts e="T57" id="Seg_4239" n="e" s="T56">Vanʼka </ts>
               <ts e="T58" id="Seg_4241" n="e" s="T57">велел </ts>
               <ts e="T59" id="Seg_4243" n="e" s="T58">măndərzittə. </ts>
               <ts e="T60" id="Seg_4245" n="e" s="T59">Măn </ts>
               <ts e="T61" id="Seg_4247" n="e" s="T60">dĭm </ts>
               <ts e="T62" id="Seg_4249" n="e" s="T61">sürerluʔpiem: </ts>
               <ts e="T63" id="Seg_4251" n="e" s="T62">(Măllia </ts>
               <ts e="T64" id="Seg_4253" n="e" s="T63">Mišanə), </ts>
               <ts e="T65" id="Seg_4255" n="e" s="T64">kanaʔ </ts>
               <ts e="T66" id="Seg_4257" n="e" s="T65">döʔə! </ts>
               <ts e="T67" id="Seg_4259" n="e" s="T66">Dĭgəttə </ts>
               <ts e="T68" id="Seg_4261" n="e" s="T67">teinen </ts>
               <ts e="T69" id="Seg_4263" n="e" s="T68">šobi. </ts>
               <ts e="T70" id="Seg_4265" n="e" s="T69">Bura </ts>
               <ts e="T71" id="Seg_4267" n="e" s="T70">sanəzi </ts>
               <ts e="T72" id="Seg_4269" n="e" s="T71">nuga, </ts>
               <ts e="T73" id="Seg_4271" n="e" s="T72">dĭ </ts>
               <ts e="T74" id="Seg_4273" n="e" s="T73">dagaj </ts>
               <ts e="T75" id="Seg_4275" n="e" s="T74">ibi </ts>
               <ts e="T76" id="Seg_4277" n="e" s="T75">i </ts>
               <ts e="T77" id="Seg_4279" n="e" s="T76">bura </ts>
               <ts e="T78" id="Seg_4281" n="e" s="T77">băʔpi. </ts>
               <ts e="T79" id="Seg_4283" n="e" s="T78">Sanə </ts>
               <ts e="T80" id="Seg_4285" n="e" s="T79">ibi </ts>
               <ts e="T81" id="Seg_4287" n="e" s="T80">bostə </ts>
               <ts e="T82" id="Seg_4289" n="e" s="T81">(barəʔluʔpi) </ts>
               <ts e="T83" id="Seg_4291" n="e" s="T82">sĭreʔpne. </ts>
               <ts e="T84" id="Seg_4293" n="e" s="T83">Miʔ </ts>
               <ts e="T85" id="Seg_4295" n="e" s="T84">turagən </ts>
               <ts e="T86" id="Seg_4297" n="e" s="T85">(ne </ts>
               <ts e="T87" id="Seg_4299" n="e" s="T86">nuʔməluʔpi) </ts>
               <ts e="T88" id="Seg_4301" n="e" s="T87">(deš-) </ts>
               <ts e="T89" id="Seg_4303" n="e" s="T88">(dĭŋinaʔtə). </ts>
               <ts e="T90" id="Seg_4305" n="e" s="T89">Tojirbi </ts>
               <ts e="T91" id="Seg_4307" n="e" s="T90">sanə. </ts>
               <ts e="T92" id="Seg_4309" n="e" s="T91">A </ts>
               <ts e="T93" id="Seg_4311" n="e" s="T92">dĭ </ts>
               <ts e="T94" id="Seg_4313" n="e" s="T93">bar </ts>
               <ts e="T95" id="Seg_4315" n="e" s="T94">pʼerluʔbə: </ts>
               <ts e="T96" id="Seg_4317" n="e" s="T95">Dö </ts>
               <ts e="T97" id="Seg_4319" n="e" s="T96">sanə. </ts>
               <ts e="T98" id="Seg_4321" n="e" s="T97">A </ts>
               <ts e="T99" id="Seg_4323" n="e" s="T98">dĭ </ts>
               <ts e="T100" id="Seg_4325" n="e" s="T99">mămbi: </ts>
               <ts e="T101" id="Seg_4327" n="e" s="T100">Măn </ts>
               <ts e="T102" id="Seg_4329" n="e" s="T101">dĭm </ts>
               <ts e="T103" id="Seg_4331" n="e" s="T102">kutlim! </ts>
               <ts e="T104" id="Seg_4333" n="e" s="T103">A </ts>
               <ts e="T105" id="Seg_4335" n="e" s="T104">(ĭmbi) </ts>
               <ts e="T106" id="Seg_4337" n="e" s="T105">ej </ts>
               <ts e="T107" id="Seg_4339" n="e" s="T106">(mămbileʔ) </ts>
               <ts e="T108" id="Seg_4341" n="e" s="T107">раньше? </ts>
               <ts e="T109" id="Seg_4343" n="e" s="T108">Taldʼen </ts>
               <ts e="T110" id="Seg_4345" n="e" s="T109">măna </ts>
               <ts e="T111" id="Seg_4347" n="e" s="T110">kăštəbiʔi </ts>
               <ts e="T112" id="Seg_4349" n="e" s="T111">(m-) </ts>
               <ts e="T113" id="Seg_4351" n="e" s="T112">măn </ts>
               <ts e="T114" id="Seg_4353" n="e" s="T113">(tugan-) </ts>
               <ts e="T115" id="Seg_4355" n="e" s="T114">tugandə: </ts>
               <ts e="T116" id="Seg_4357" n="e" s="T115">Šoʔ </ts>
               <ts e="T117" id="Seg_4359" n="e" s="T116">miʔnʼibeʔ </ts>
               <ts e="T118" id="Seg_4361" n="e" s="T117">amorzittə. </ts>
               <ts e="T119" id="Seg_4363" n="e" s="T118">(Eššinʼ) </ts>
               <ts e="T120" id="Seg_4365" n="e" s="T119">Ešši </ts>
               <ts e="T121" id="Seg_4367" n="e" s="T120">(ti-) </ts>
               <ts e="T122" id="Seg_4369" n="e" s="T121">külambi, </ts>
               <ts e="T123" id="Seg_4371" n="e" s="T122">(ušonʼiʔkə) </ts>
               <ts e="T124" id="Seg_4373" n="e" s="T123">kambi. </ts>
               <ts e="T125" id="Seg_4375" n="e" s="T124">A </ts>
               <ts e="T126" id="Seg_4377" n="e" s="T125">măn </ts>
               <ts e="T127" id="Seg_4379" n="e" s="T126">mămbiam: </ts>
               <ts e="T128" id="Seg_4381" n="e" s="T127">Šolam. </ts>
               <ts e="T129" id="Seg_4383" n="e" s="T128">(A </ts>
               <ts e="T130" id="Seg_4385" n="e" s="T129">bos-) </ts>
               <ts e="T131" id="Seg_4387" n="e" s="T130">A </ts>
               <ts e="T132" id="Seg_4389" n="e" s="T131">bospə </ts>
               <ts e="T133" id="Seg_4391" n="e" s="T132">ej </ts>
               <ts e="T134" id="Seg_4393" n="e" s="T133">kambiam. </ts>
               <ts e="T135" id="Seg_4395" n="e" s="T134">(Meim) </ts>
               <ts e="T136" id="Seg_4397" n="e" s="T135">kăštəbiam, </ts>
               <ts e="T137" id="Seg_4399" n="e" s="T136">kăštəbiam, </ts>
               <ts e="T138" id="Seg_4401" n="e" s="T137">dĭ </ts>
               <ts e="T139" id="Seg_4403" n="e" s="T138">ej </ts>
               <ts e="T140" id="Seg_4405" n="e" s="T139">kambi, </ts>
               <ts e="T141" id="Seg_4407" n="e" s="T140">i </ts>
               <ts e="T142" id="Seg_4409" n="e" s="T141">(măn) </ts>
               <ts e="T143" id="Seg_4411" n="e" s="T142">ej </ts>
               <ts e="T144" id="Seg_4413" n="e" s="T143">kambiam. </ts>
               <ts e="T145" id="Seg_4415" n="e" s="T144">Dĭn </ts>
               <ts e="T146" id="Seg_4417" n="e" s="T145">ugandə </ts>
               <ts e="T147" id="Seg_4419" n="e" s="T146">araj, </ts>
               <ts e="T148" id="Seg_4421" n="e" s="T147">a </ts>
               <ts e="T149" id="Seg_4423" n="e" s="T148">măna </ts>
               <ts e="T150" id="Seg_4425" n="e" s="T149">ara </ts>
               <ts e="T151" id="Seg_4427" n="e" s="T150">ej </ts>
               <ts e="T152" id="Seg_4429" n="e" s="T151">kereʔ. </ts>
               <ts e="T153" id="Seg_4431" n="e" s="T152">Măn </ts>
               <ts e="T154" id="Seg_4433" n="e" s="T153">не </ts>
               <ts e="T155" id="Seg_4435" n="e" s="T154">хочу </ts>
               <ts e="T156" id="Seg_4437" n="e" s="T155">dĭm </ts>
               <ts e="T157" id="Seg_4439" n="e" s="T156">bĭʔsittə. </ts>
               <ts e="T158" id="Seg_4441" n="e" s="T157">A </ts>
               <ts e="T159" id="Seg_4443" n="e" s="T158">dĭ </ts>
               <ts e="T160" id="Seg_4445" n="e" s="T159">kuroluʔpi </ts>
               <ts e="T161" id="Seg_4447" n="e" s="T160">bar, </ts>
               <ts e="T162" id="Seg_4449" n="e" s="T161">măndə: </ts>
               <ts e="T163" id="Seg_4451" n="e" s="T162">urgajam </ts>
               <ts e="T164" id="Seg_4453" n="e" s="T163">ažnə </ts>
               <ts e="T165" id="Seg_4455" n="e" s="T164">(dʼoːdunʼi) </ts>
               <ts e="T166" id="Seg_4457" n="e" s="T165">kubiam. </ts>
               <ts e="T167" id="Seg_4459" n="e" s="T166">Tüj </ts>
               <ts e="T168" id="Seg_4461" n="e" s="T167">büzəjleʔ </ts>
               <ts e="T169" id="Seg_4463" n="e" s="T168">bar </ts>
               <ts e="T170" id="Seg_4465" n="e" s="T169">tüšəlbi </ts>
               <ts e="T171" id="Seg_4467" n="e" s="T170">šiʔnʼileʔ. </ts>
               <ts e="T172" id="Seg_4469" n="e" s="T171">Iʔbəleʔbə, </ts>
               <ts e="T173" id="Seg_4471" n="e" s="T172">amnoʔ, </ts>
               <ts e="T174" id="Seg_4473" n="e" s="T173">iʔbəleʔbə, </ts>
               <ts e="T175" id="Seg_4475" n="e" s="T174">ej </ts>
               <ts e="T176" id="Seg_4477" n="e" s="T175">kirgarlaʔbə. </ts>
               <ts e="T177" id="Seg_4479" n="e" s="T176">Teinen </ts>
               <ts e="T178" id="Seg_4481" n="e" s="T177">ejü </ts>
               <ts e="T179" id="Seg_4483" n="e" s="T178">molaːmbi. </ts>
               <ts e="T180" id="Seg_4485" n="e" s="T179">Sĭre </ts>
               <ts e="T181" id="Seg_4487" n="e" s="T180">bar </ts>
               <ts e="T182" id="Seg_4489" n="e" s="T181">(nömrunʼiʔ) </ts>
               <ts e="T183" id="Seg_4491" n="e" s="T182">ibi. </ts>
               <ts e="T184" id="Seg_4493" n="e" s="T183">Sĭre </ts>
               <ts e="T185" id="Seg_4495" n="e" s="T184">(buž-) </ts>
               <ts e="T186" id="Seg_4497" n="e" s="T185">büžü </ts>
               <ts e="T187" id="Seg_4499" n="e" s="T186">bü </ts>
               <ts e="T188" id="Seg_4501" n="e" s="T187">molalləj. </ts>
               <ts e="T189" id="Seg_4503" n="e" s="T188">Dĭgəttə </ts>
               <ts e="T190" id="Seg_4505" n="e" s="T189">noʔ </ts>
               <ts e="T191" id="Seg_4507" n="e" s="T190">özerləj. </ts>
               <ts e="T192" id="Seg_4509" n="e" s="T191">Всякий </ts>
               <ts e="T193" id="Seg_4511" n="e" s="T192">svʼetogəʔi </ts>
               <ts e="T194" id="Seg_4513" n="e" s="T193">özerləʔi. </ts>
               <ts e="T195" id="Seg_4515" n="e" s="T194">Paʔi </ts>
               <ts e="T196" id="Seg_4517" n="e" s="T195">bar </ts>
               <ts e="T197" id="Seg_4519" n="e" s="T196">kuvas </ts>
               <ts e="T198" id="Seg_4521" n="e" s="T197">moləʔi. </ts>
               <ts e="T199" id="Seg_4523" n="e" s="T198">Zʼelʼonaʔi </ts>
               <ts e="T200" id="Seg_4525" n="e" s="T199">moləʔi </ts>
               <ts e="T201" id="Seg_4527" n="e" s="T200">bar. </ts>
               <ts e="T202" id="Seg_4529" n="e" s="T201">Dĭgəttə </ts>
               <ts e="T203" id="Seg_4531" n="e" s="T202">ineʔi, </ts>
               <ts e="T204" id="Seg_4533" n="e" s="T203">tüžöjəʔi, </ts>
               <ts e="T205" id="Seg_4535" n="e" s="T204">ulardə </ts>
               <ts e="T206" id="Seg_4537" n="e" s="T205">kalaʔi </ts>
               <ts e="T207" id="Seg_4539" n="e" s="T206">noʔ </ts>
               <ts e="T208" id="Seg_4541" n="e" s="T207">(amzittə). </ts>
               <ts e="T209" id="Seg_4543" n="e" s="T208">Dĭgəttə </ts>
               <ts e="T210" id="Seg_4545" n="e" s="T209">noʔ </ts>
               <ts e="T211" id="Seg_4547" n="e" s="T210">urgo </ts>
               <ts e="T212" id="Seg_4549" n="e" s="T211">özerləj. </ts>
               <ts e="T213" id="Seg_4551" n="e" s="T212">Dĭgəttə </ts>
               <ts e="T214" id="Seg_4553" n="e" s="T213">dĭm </ts>
               <ts e="T215" id="Seg_4555" n="e" s="T214">(šapkosi-) </ts>
               <ts e="T216" id="Seg_4557" n="e" s="T215">šapkozi </ts>
               <ts e="T217" id="Seg_4559" n="e" s="T216">jaʔsittə </ts>
               <ts e="T218" id="Seg_4561" n="e" s="T217">nada. </ts>
               <ts e="T219" id="Seg_4563" n="e" s="T218">Dĭgəttə </ts>
               <ts e="T220" id="Seg_4565" n="e" s="T219">kujagən </ts>
               <ts e="T221" id="Seg_4567" n="e" s="T220">koləj, </ts>
               <ts e="T222" id="Seg_4569" n="e" s="T221">da </ts>
               <ts e="T223" id="Seg_4571" n="e" s="T222">oʔbdəsʼtə </ts>
               <ts e="T224" id="Seg_4573" n="e" s="T223">nada. </ts>
               <ts e="T225" id="Seg_4575" n="e" s="T224">Il </ts>
               <ts e="T226" id="Seg_4577" n="e" s="T225">(mĭnliaʔi) </ts>
               <ts e="T227" id="Seg_4579" n="e" s="T226">ugandə </ts>
               <ts e="T228" id="Seg_4581" n="e" s="T227">jakše. </ts>
               <ts e="T229" id="Seg_4583" n="e" s="T228">Pʼe </ts>
               <ts e="T230" id="Seg_4585" n="e" s="T229">šobi. </ts>
               <ts e="T231" id="Seg_4587" n="e" s="T230">Ugandə </ts>
               <ts e="T232" id="Seg_4589" n="e" s="T231">(ej-) </ts>
               <ts e="T233" id="Seg_4591" n="e" s="T232">ejü. </ts>
               <ts e="T234" id="Seg_4593" n="e" s="T233">Măn </ts>
               <ts e="T235" id="Seg_4595" n="e" s="T234">(dönüli </ts>
               <ts e="T236" id="Seg_4597" n="e" s="T235">mobiam) </ts>
               <ts e="T237" id="Seg_4599" n="e" s="T236">kunolzittə. </ts>
               <ts e="T238" id="Seg_4601" n="e" s="T237">Dĭgəttə </ts>
               <ts e="T239" id="Seg_4603" n="e" s="T238">ertə </ts>
               <ts e="T240" id="Seg_4605" n="e" s="T239">uʔbdəbiam. </ts>
               <ts e="T241" id="Seg_4607" n="e" s="T240">A </ts>
               <ts e="T242" id="Seg_4609" n="e" s="T241">(dʼala=) </ts>
               <ts e="T243" id="Seg_4611" n="e" s="T242">dʼalam </ts>
               <ts e="T244" id="Seg_4613" n="e" s="T243">bar </ts>
               <ts e="T245" id="Seg_4615" n="e" s="T244">togonorbiam. </ts>
               <ts e="T246" id="Seg_4617" n="e" s="T245">Tüžöj </ts>
               <ts e="T247" id="Seg_4619" n="e" s="T246">surdobiam </ts>
               <ts e="T248" id="Seg_4621" n="e" s="T247">da, </ts>
               <ts e="T249" id="Seg_4623" n="e" s="T248">mĭnzərbiem, </ts>
               <ts e="T250" id="Seg_4625" n="e" s="T249">abiam, </ts>
               <ts e="T251" id="Seg_4627" n="e" s="T250">dĭgəttə </ts>
               <ts e="T252" id="Seg_4629" n="e" s="T251">dʼăbaktərbiam. </ts>
               <ts e="T253" id="Seg_4631" n="e" s="T252">Ertə </ts>
               <ts e="T254" id="Seg_4633" n="e" s="T253">oʔbdəbiam </ts>
               <ts e="T255" id="Seg_4635" n="e" s="T254">da </ts>
               <ts e="T256" id="Seg_4637" n="e" s="T255">šü </ts>
               <ts e="T257" id="Seg_4639" n="e" s="T256">nendəbiem. </ts>
               <ts e="T258" id="Seg_4641" n="e" s="T257">Măna </ts>
               <ts e="T259" id="Seg_4643" n="e" s="T258">Kazan </ts>
               <ts e="T260" id="Seg_4645" n="e" s="T259">turanə </ts>
               <ts e="T261" id="Seg_4647" n="e" s="T260">nada </ts>
               <ts e="T262" id="Seg_4649" n="e" s="T261">kanzittə, </ts>
               <ts e="T263" id="Seg_4651" n="e" s="T262">(măna) </ts>
               <ts e="T264" id="Seg_4653" n="e" s="T263">aktʼa </ts>
               <ts e="T265" id="Seg_4655" n="e" s="T264">iʔbolaʔbə </ts>
               <ts e="T266" id="Seg_4657" n="e" s="T265">bankan. </ts>
               <ts e="T267" id="Seg_4659" n="e" s="T266">Izittə </ts>
               <ts e="T268" id="Seg_4661" n="e" s="T267">nada, </ts>
               <ts e="T269" id="Seg_4663" n="e" s="T268">da </ts>
               <ts e="T270" id="Seg_4665" n="e" s="T269">ildə </ts>
               <ts e="T271" id="Seg_4667" n="e" s="T270">mĭzittə, </ts>
               <ts e="T272" id="Seg_4669" n="e" s="T271">măn </ts>
               <ts e="T273" id="Seg_4671" n="e" s="T272">(dĭzeŋgən) </ts>
               <ts e="T274" id="Seg_4673" n="e" s="T273">ibiem. </ts>
               <ts e="T275" id="Seg_4675" n="e" s="T274">Măn </ts>
               <ts e="T276" id="Seg_4677" n="e" s="T275">iʔgö </ts>
               <ts e="T277" id="Seg_4679" n="e" s="T276">dʼăbaktəriam, </ts>
               <ts e="T278" id="Seg_4681" n="e" s="T277">(tu-) </ts>
               <ts e="T279" id="Seg_4683" n="e" s="T278">tüj </ts>
               <ts e="T280" id="Seg_4685" n="e" s="T279">(dʼăbaktər-) </ts>
               <ts e="T281" id="Seg_4687" n="e" s="T280">dʼăbaktərlaʔbəʔjə. </ts>
               <ts e="T282" id="Seg_4689" n="e" s="T281">Ugandə </ts>
               <ts e="T283" id="Seg_4691" n="e" s="T282">tăn </ts>
               <ts e="T284" id="Seg_4693" n="e" s="T283">koloʔsəbi. </ts>
               <ts e="T285" id="Seg_4695" n="e" s="T284">(Koʔbdo) </ts>
               <ts e="T286" id="Seg_4697" n="e" s="T285">üge </ts>
               <ts e="T287" id="Seg_4699" n="e" s="T286">măna. </ts>
               <ts e="T288" id="Seg_4701" n="e" s="T287">Măna </ts>
               <ts e="T289" id="Seg_4703" n="e" s="T288">nada </ts>
               <ts e="T290" id="Seg_4705" n="e" s="T289">dʼăbaktərzittə. </ts>
               <ts e="T291" id="Seg_4707" n="e" s="T290">Tüj </ts>
               <ts e="T292" id="Seg_4709" n="e" s="T291">tăn </ts>
               <ts e="T293" id="Seg_4711" n="e" s="T292">dʼăbaktəraʔ, </ts>
               <ts e="T294" id="Seg_4713" n="e" s="T293">a </ts>
               <ts e="T295" id="Seg_4715" n="e" s="T294">măn </ts>
               <ts e="T296" id="Seg_4717" n="e" s="T295">ej </ts>
               <ts e="T297" id="Seg_4719" n="e" s="T296">dʼăbaktərlam. </ts>
               <ts e="T298" id="Seg_4721" n="e" s="T297">(Măj-) </ts>
               <ts e="T299" id="Seg_4723" n="e" s="T298">Măn </ts>
               <ts e="T300" id="Seg_4725" n="e" s="T299">ej </ts>
               <ts e="T301" id="Seg_4727" n="e" s="T300">tĭmniem, </ts>
               <ts e="T302" id="Seg_4729" n="e" s="T301">ĭmbi </ts>
               <ts e="T303" id="Seg_4731" n="e" s="T302">dʼăbaktərzittə, </ts>
               <ts e="T304" id="Seg_4733" n="e" s="T303">tăn </ts>
               <ts e="T305" id="Seg_4735" n="e" s="T304">dʼăbaktəraʔ. </ts>
               <ts e="T306" id="Seg_4737" n="e" s="T305">(A </ts>
               <ts e="T307" id="Seg_4739" n="e" s="T306">m-) </ts>
               <ts e="T308" id="Seg_4741" n="e" s="T307">A </ts>
               <ts e="T309" id="Seg_4743" n="e" s="T308">măn </ts>
               <ts e="T310" id="Seg_4745" n="e" s="T309">ĭmbi </ts>
               <ts e="T311" id="Seg_4747" n="e" s="T310">üge </ts>
               <ts e="T312" id="Seg_4749" n="e" s="T311">буду </ts>
               <ts e="T313" id="Seg_4751" n="e" s="T312">(dʼăbaktərzittə), </ts>
               <ts e="T314" id="Seg_4753" n="e" s="T313">(iʔbəle- </ts>
               <ts e="T315" id="Seg_4755" n="e" s="T314">iʔbola-) </ts>
               <ts e="T317" id="Seg_4757" n="e" s="T315">iʔbəlem_da. </ts>
               <ts e="T318" id="Seg_4759" n="e" s="T317">Măn </ts>
               <ts e="T319" id="Seg_4761" n="e" s="T318">turam </ts>
               <ts e="T320" id="Seg_4763" n="e" s="T319">с </ts>
               <ts e="T321" id="Seg_4765" n="e" s="T320">краю, </ts>
               <ts e="T322" id="Seg_4767" n="e" s="T321">măn </ts>
               <ts e="T323" id="Seg_4769" n="e" s="T322">ĭmbidə </ts>
               <ts e="T324" id="Seg_4771" n="e" s="T323">ej </ts>
               <ts e="T325" id="Seg_4773" n="e" s="T324">tĭmniem. </ts>
               <ts e="T326" id="Seg_4775" n="e" s="T325">Tenöʔ </ts>
               <ts e="T327" id="Seg_4777" n="e" s="T326">ĭmbi-nʼibudʼ. </ts>
               <ts e="T328" id="Seg_4779" n="e" s="T327">Šamaʔ </ts>
               <ts e="T329" id="Seg_4781" n="e" s="T328">ĭmbi-nʼibudʼ. </ts>
               <ts e="T330" id="Seg_4783" n="e" s="T329">Abam </ts>
               <ts e="T331" id="Seg_4785" n="e" s="T330">(kuŋgəm) </ts>
               <ts e="T1057" id="Seg_4787" n="e" s="T331">kalla </ts>
               <ts e="T332" id="Seg_4789" n="e" s="T1057">dʼürbi. </ts>
               <ts e="T333" id="Seg_4791" n="e" s="T332">A </ts>
               <ts e="T334" id="Seg_4793" n="e" s="T333">măna </ts>
               <ts e="T335" id="Seg_4795" n="e" s="T334">ugandə </ts>
               <ts e="T336" id="Seg_4797" n="e" s="T335">jakše, </ts>
               <ts e="T337" id="Seg_4799" n="e" s="T336">šindidə </ts>
               <ts e="T338" id="Seg_4801" n="e" s="T337">ej </ts>
               <ts e="T339" id="Seg_4803" n="e" s="T338">münöria, </ts>
               <ts e="T340" id="Seg_4805" n="e" s="T339">măn </ts>
               <ts e="T341" id="Seg_4807" n="e" s="T340">(balʔraʔ) </ts>
               <ts e="T342" id="Seg_4809" n="e" s="T341">alomniam. </ts>
               <ts e="T343" id="Seg_4811" n="e" s="T342">(Ajnu) </ts>
               <ts e="T344" id="Seg_4813" n="e" s="T343">nüjleʔbəm. </ts>
               <ts e="T345" id="Seg_4815" n="e" s="T344">Suʔmileʔbəm </ts>
               <ts e="T346" id="Seg_4817" n="e" s="T345">bar. </ts>
               <ts e="T347" id="Seg_4819" n="e" s="T346">A </ts>
               <ts e="T348" id="Seg_4821" n="e" s="T347">iam </ts>
               <ts e="T349" id="Seg_4823" n="e" s="T348">ej </ts>
               <ts e="T350" id="Seg_4825" n="e" s="T349">nʼilgöliem. </ts>
               <ts e="T351" id="Seg_4827" n="e" s="T350">Iam </ts>
               <ts e="T352" id="Seg_4829" n="e" s="T351">abam </ts>
               <ts e="T353" id="Seg_4831" n="e" s="T352">dĭ </ts>
               <ts e="T354" id="Seg_4833" n="e" s="T353">saʔpiaŋbi, </ts>
               <ts e="T355" id="Seg_4835" n="e" s="T354">što </ts>
               <ts e="T356" id="Seg_4837" n="e" s="T355">măn </ts>
               <ts e="T357" id="Seg_4839" n="e" s="T356">((PAUSE)). </ts>
               <ts e="T358" id="Seg_4841" n="e" s="T357">Što </ts>
               <ts e="T359" id="Seg_4843" n="e" s="T358">măn </ts>
               <ts e="T360" id="Seg_4845" n="e" s="T359">alomniam. </ts>
               <ts e="T361" id="Seg_4847" n="e" s="T360">Măn </ts>
               <ts e="T362" id="Seg_4849" n="e" s="T361">elem, </ts>
               <ts e="T363" id="Seg_4851" n="e" s="T362">teinen </ts>
               <ts e="T364" id="Seg_4853" n="e" s="T363">nüdʼin </ts>
               <ts e="T365" id="Seg_4855" n="e" s="T364">bar </ts>
               <ts e="T366" id="Seg_4857" n="e" s="T365">(ara </ts>
               <ts e="T367" id="Seg_4859" n="e" s="T366">bĭʔ-), </ts>
               <ts e="T368" id="Seg_4861" n="e" s="T367">(dʼabr-) </ts>
               <ts e="T369" id="Seg_4863" n="e" s="T368">nüjnə </ts>
               <ts e="T370" id="Seg_4865" n="e" s="T369">(nüjnə-) </ts>
               <ts e="T371" id="Seg_4867" n="e" s="T370">nüjnəbi. </ts>
               <ts e="T372" id="Seg_4869" n="e" s="T371">Dʼabərobi </ts>
               <ts e="T373" id="Seg_4871" n="e" s="T372">bar, </ts>
               <ts e="T374" id="Seg_4873" n="e" s="T373">(kudonzəbi) </ts>
               <ts e="T375" id="Seg_4875" n="e" s="T374">bar. </ts>
               <ts e="T376" id="Seg_4877" n="e" s="T375">Jelʼa </ts>
               <ts e="T377" id="Seg_4879" n="e" s="T376">unnʼa </ts>
               <ts e="T378" id="Seg_4881" n="e" s="T377">amnolaʔbə, </ts>
               <ts e="T379" id="Seg_4883" n="e" s="T378">üge </ts>
               <ts e="T380" id="Seg_4885" n="e" s="T379">monoʔkolaʔbəʔjə, </ts>
               <ts e="T381" id="Seg_4887" n="e" s="T380">a </ts>
               <ts e="T382" id="Seg_4889" n="e" s="T381">dĭ </ts>
               <ts e="T383" id="Seg_4891" n="e" s="T382">üge </ts>
               <ts e="T384" id="Seg_4893" n="e" s="T383">ej </ts>
               <ts e="T385" id="Seg_4895" n="e" s="T384">kalia. </ts>
               <ts e="T386" id="Seg_4897" n="e" s="T385">Šindinədə </ts>
               <ts e="T387" id="Seg_4899" n="e" s="T386">ej </ts>
               <ts e="T388" id="Seg_4901" n="e" s="T387">kalia, </ts>
               <ts e="T389" id="Seg_4903" n="e" s="T388">ни </ts>
               <ts e="T390" id="Seg_4905" n="e" s="T389">за </ts>
               <ts e="T391" id="Seg_4907" n="e" s="T390">кого </ts>
               <ts e="T392" id="Seg_4909" n="e" s="T391">не </ts>
               <ts e="T393" id="Seg_4911" n="e" s="T392">идет. </ts>
               <ts e="T394" id="Seg_4913" n="e" s="T393">Dĭn </ts>
               <ts e="T395" id="Seg_4915" n="e" s="T394">bar </ts>
               <ts e="T396" id="Seg_4917" n="e" s="T395">iat </ts>
               <ts e="T397" id="Seg_4919" n="e" s="T396">abat </ts>
               <ts e="T398" id="Seg_4921" n="e" s="T397">ige. </ts>
               <ts e="T399" id="Seg_4923" n="e" s="T398">Măliaʔi: </ts>
               <ts e="T400" id="Seg_4925" n="e" s="T399">Kanaʔ </ts>
               <ts e="T401" id="Seg_4927" n="e" s="T400">(tibinə), </ts>
               <ts e="T402" id="Seg_4929" n="e" s="T401">a </ts>
               <ts e="T403" id="Seg_4931" n="e" s="T402">dĭ </ts>
               <ts e="T404" id="Seg_4933" n="e" s="T403">kădedə </ts>
               <ts e="T405" id="Seg_4935" n="e" s="T404">ej </ts>
               <ts e="T406" id="Seg_4937" n="e" s="T405">kalia. </ts>
               <ts e="T407" id="Seg_4939" n="e" s="T406">Šobi </ts>
               <ts e="T408" id="Seg_4941" n="e" s="T407">măna </ts>
               <ts e="T409" id="Seg_4943" n="e" s="T408">monoʔkosʼtə </ts>
               <ts e="T410" id="Seg_4945" n="e" s="T409">ej </ts>
               <ts e="T411" id="Seg_4947" n="e" s="T410">kuvas </ts>
               <ts e="T412" id="Seg_4949" n="e" s="T411">kuza, </ts>
               <ts e="T413" id="Seg_4951" n="e" s="T412">măna </ts>
               <ts e="T414" id="Seg_4953" n="e" s="T413">nʼe </ts>
               <ts e="T415" id="Seg_4955" n="e" s="T414">axota </ts>
               <ts e="T416" id="Seg_4957" n="e" s="T415">kanzittə. </ts>
               <ts e="T417" id="Seg_4959" n="e" s="T416">Abam </ts>
               <ts e="T418" id="Seg_4961" n="e" s="T417">măndə: </ts>
               <ts e="T419" id="Seg_4963" n="e" s="T418">Ĭmbi </ts>
               <ts e="T420" id="Seg_4965" n="e" s="T419">tăn </ts>
               <ts e="T421" id="Seg_4967" n="e" s="T420">ej </ts>
               <ts e="T422" id="Seg_4969" n="e" s="T421">kalal? </ts>
               <ts e="T423" id="Seg_4971" n="e" s="T422">(Ej) </ts>
               <ts e="T424" id="Seg_4973" n="e" s="T423">Nu </ts>
               <ts e="T425" id="Seg_4975" n="e" s="T424">iʔ </ts>
               <ts e="T426" id="Seg_4977" n="e" s="T425">(kanaʔ), </ts>
               <ts e="T427" id="Seg_4979" n="e" s="T426">(măn </ts>
               <ts e="T428" id="Seg_4981" n="e" s="T427">nuʔməluʔpiam) </ts>
               <ts e="T429" id="Seg_4983" n="e" s="T428">dĭgəttə. </ts>
               <ts e="T430" id="Seg_4985" n="e" s="T429">Măn </ts>
               <ts e="T431" id="Seg_4987" n="e" s="T430">(pervam) </ts>
               <ts e="T432" id="Seg_4989" n="e" s="T431">первый </ts>
               <ts e="T433" id="Seg_4991" n="e" s="T432">tibim </ts>
               <ts e="T434" id="Seg_4993" n="e" s="T433">šobi </ts>
               <ts e="T435" id="Seg_4995" n="e" s="T434">măna </ts>
               <ts e="T436" id="Seg_4997" n="e" s="T435">monoʔkosʼtə. </ts>
               <ts e="T437" id="Seg_4999" n="e" s="T436">Dĭʔnə </ts>
               <ts e="T438" id="Seg_5001" n="e" s="T437">(мало </ts>
               <ts e="T439" id="Seg_5003" n="e" s="T438">pʼe </ts>
               <ts e="T440" id="Seg_5005" n="e" s="T439">i </ts>
               <ts e="T441" id="Seg_5007" n="e" s="T440">măna=) </ts>
               <ts e="T442" id="Seg_5009" n="e" s="T441">amga </ts>
               <ts e="T443" id="Seg_5011" n="e" s="T442">pʼe </ts>
               <ts e="T444" id="Seg_5013" n="e" s="T443">i </ts>
               <ts e="T445" id="Seg_5015" n="e" s="T444">măna </ts>
               <ts e="T446" id="Seg_5017" n="e" s="T445">amga </ts>
               <ts e="T447" id="Seg_5019" n="e" s="T446">pʼe. </ts>
               <ts e="T448" id="Seg_5021" n="e" s="T447">(Măn </ts>
               <ts e="T449" id="Seg_5023" n="e" s="T448">nuʔ) </ts>
               <ts e="T450" id="Seg_5025" n="e" s="T449">Măn </ts>
               <ts e="T451" id="Seg_5027" n="e" s="T450">măndəm: </ts>
               <ts e="T452" id="Seg_5029" n="e" s="T451">Kanaʔ </ts>
               <ts e="T453" id="Seg_5031" n="e" s="T452">abanə </ts>
               <ts e="T454" id="Seg_5033" n="e" s="T453">da </ts>
               <ts e="T455" id="Seg_5035" n="e" s="T454">ianə, </ts>
               <ts e="T456" id="Seg_5037" n="e" s="T455">monoʔkoʔ </ts>
               <ts e="T457" id="Seg_5039" n="e" s="T456">dĭzeŋ </ts>
               <ts e="T458" id="Seg_5041" n="e" s="T457">mĭləʔi? </ts>
               <ts e="T459" id="Seg_5043" n="e" s="T458">Dʼok, </ts>
               <ts e="T460" id="Seg_5045" n="e" s="T459">ej </ts>
               <ts e="T461" id="Seg_5047" n="e" s="T460">mĭləʔi. </ts>
               <ts e="T462" id="Seg_5049" n="e" s="T461">Dĭgəttə </ts>
               <ts e="T463" id="Seg_5051" n="e" s="T462">măna </ts>
               <ts e="T464" id="Seg_5053" n="e" s="T463">dʼăbaktərluʔpiʔi </ts>
               <ts e="T465" id="Seg_5055" n="e" s="T464">i </ts>
               <ts e="T466" id="Seg_5057" n="e" s="T465">(kam-) </ts>
               <ts e="T467" id="Seg_5059" n="e" s="T466">kambibaʔ </ts>
               <ts e="T468" id="Seg_5061" n="e" s="T467">(ine-) </ts>
               <ts e="T469" id="Seg_5063" n="e" s="T468">inetsi </ts>
               <ts e="T470" id="Seg_5065" n="e" s="T469">dĭʔnə. </ts>
               <ts e="T471" id="Seg_5067" n="e" s="T470">Dĭgəttə </ts>
               <ts e="T472" id="Seg_5069" n="e" s="T471">iam </ts>
               <ts e="T473" id="Seg_5071" n="e" s="T472">šobi </ts>
               <ts e="T474" id="Seg_5073" n="e" s="T473">dibər </ts>
               <ts e="T475" id="Seg_5075" n="e" s="T474">miʔnʼibeʔ. </ts>
               <ts e="T476" id="Seg_5077" n="e" s="T475">Dĭ </ts>
               <ts e="T477" id="Seg_5079" n="e" s="T476">(š-) </ts>
               <ts e="T478" id="Seg_5081" n="e" s="T477">döbər </ts>
               <ts e="T479" id="Seg_5083" n="e" s="T478">šobi </ts>
               <ts e="T480" id="Seg_5085" n="e" s="T479">noʔ </ts>
               <ts e="T481" id="Seg_5087" n="e" s="T480">jaʔsittə. </ts>
               <ts e="T482" id="Seg_5089" n="e" s="T481">Dĭgəttə </ts>
               <ts e="T483" id="Seg_5091" n="e" s="T482">kambi </ts>
               <ts e="T484" id="Seg_5093" n="e" s="T483">bünə </ts>
               <ts e="T485" id="Seg_5095" n="e" s="T484">da </ts>
               <ts e="T486" id="Seg_5097" n="e" s="T485">dĭn </ts>
               <ts e="T487" id="Seg_5099" n="e" s="T486">külambi. </ts>
               <ts e="T488" id="Seg_5101" n="e" s="T487">Dĭgəttə </ts>
               <ts e="T489" id="Seg_5103" n="e" s="T488">măn </ts>
               <ts e="T490" id="Seg_5105" n="e" s="T489">maːndə </ts>
               <ts e="T491" id="Seg_5107" n="e" s="T490">šobiam </ts>
               <ts e="T492" id="Seg_5109" n="e" s="T491">i </ts>
               <ts e="T493" id="Seg_5111" n="e" s="T492">ugandə </ts>
               <ts e="T494" id="Seg_5113" n="e" s="T493">tăŋ </ts>
               <ts e="T495" id="Seg_5115" n="e" s="T494">tenöbiam, </ts>
               <ts e="T496" id="Seg_5117" n="e" s="T495">sĭjbə </ts>
               <ts e="T497" id="Seg_5119" n="e" s="T496">ĭzembie. </ts>
               <ts e="T498" id="Seg_5121" n="e" s="T497">Dʼorbiam, </ts>
               <ts e="T499" id="Seg_5123" n="e" s="T498">ajirbiom </ts>
               <ts e="T500" id="Seg_5125" n="e" s="T499">dĭm </ts>
               <ts e="T501" id="Seg_5127" n="e" s="T500">bar. </ts>
               <ts e="T502" id="Seg_5129" n="e" s="T501">Dĭgəttə </ts>
               <ts e="T503" id="Seg_5131" n="e" s="T502">iam </ts>
               <ts e="T504" id="Seg_5133" n="e" s="T503">măna </ts>
               <ts e="T505" id="Seg_5135" n="e" s="T504">bü </ts>
               <ts e="T506" id="Seg_5137" n="e" s="T505">dʼazirbi, </ts>
               <ts e="T507" id="Seg_5139" n="e" s="T506">(pĭj) </ts>
               <ts e="T508" id="Seg_5141" n="e" s="T507">măna. </ts>
               <ts e="T509" id="Seg_5143" n="e" s="T508">Dĭgəttə </ts>
               <ts e="T510" id="Seg_5145" n="e" s="T509">măn </ts>
               <ts e="T511" id="Seg_5147" n="e" s="T510">davaj </ts>
               <ts e="T512" id="Seg_5149" n="e" s="T511">nüjnə </ts>
               <ts e="T513" id="Seg_5151" n="e" s="T512">(nüjzit-) </ts>
               <ts e="T514" id="Seg_5153" n="e" s="T513">nüjzittə. </ts>
               <ts e="T515" id="Seg_5155" n="e" s="T514">Dĭgəttə </ts>
               <ts e="T516" id="Seg_5157" n="e" s="T515">măn </ts>
               <ts e="T517" id="Seg_5159" n="e" s="T516">šobiam </ts>
               <ts e="T518" id="Seg_5161" n="e" s="T517">(maʔnʼi), </ts>
               <ts e="T519" id="Seg_5163" n="e" s="T518">amnobiam. </ts>
               <ts e="T520" id="Seg_5165" n="e" s="T519">Dĭgəttə </ts>
               <ts e="T521" id="Seg_5167" n="e" s="T520">kambiam </ts>
               <ts e="T522" id="Seg_5169" n="e" s="T521">(Permʼako-) </ts>
               <ts e="T523" id="Seg_5171" n="e" s="T522">Permʼakovtə, </ts>
               <ts e="T524" id="Seg_5173" n="e" s="T523">večorkanə. </ts>
               <ts e="T525" id="Seg_5175" n="e" s="T524">(Ten) </ts>
               <ts e="T526" id="Seg_5177" n="e" s="T525">sʼarbibaʔ, </ts>
               <ts e="T527" id="Seg_5179" n="e" s="T526">suʔmibeʔ. </ts>
               <ts e="T528" id="Seg_5181" n="e" s="T527">Garmonʼnʼə </ts>
               <ts e="T529" id="Seg_5183" n="e" s="T528">sʼarbibaʔ. </ts>
               <ts e="T530" id="Seg_5185" n="e" s="T529">Nüjnə </ts>
               <ts e="T531" id="Seg_5187" n="e" s="T530">nüjnəbibeʔ. </ts>
               <ts e="T532" id="Seg_5189" n="e" s="T531">Dĭgəttə </ts>
               <ts e="T533" id="Seg_5191" n="e" s="T532">dĭn </ts>
               <ts e="T534" id="Seg_5193" n="e" s="T533">tibi </ts>
               <ts e="T535" id="Seg_5195" n="e" s="T534">šobi, </ts>
               <ts e="T536" id="Seg_5197" n="e" s="T535">(Nagornaj). </ts>
               <ts e="T537" id="Seg_5199" n="e" s="T536">Muʔzendə </ts>
               <ts e="T538" id="Seg_5201" n="e" s="T537">urgo, </ts>
               <ts e="T539" id="Seg_5203" n="e" s="T538">numəʔi. </ts>
               <ts e="T540" id="Seg_5205" n="e" s="T539">Da </ts>
               <ts e="T541" id="Seg_5207" n="e" s="T540">köməʔi, </ts>
               <ts e="T542" id="Seg_5209" n="e" s="T541">bostə </ts>
               <ts e="T543" id="Seg_5211" n="e" s="T542">kömə. </ts>
               <ts e="T544" id="Seg_5213" n="e" s="T543">Dĭgəttə </ts>
               <ts e="T545" id="Seg_5215" n="e" s="T544">măn </ts>
               <ts e="T546" id="Seg_5217" n="e" s="T545">maʔnʼi </ts>
               <ts e="T547" id="Seg_5219" n="e" s="T546">šobiam </ts>
               <ts e="T548" id="Seg_5221" n="e" s="T547">da </ts>
               <ts e="T549" id="Seg_5223" n="e" s="T548">šobi </ts>
               <ts e="T550" id="Seg_5225" n="e" s="T549">măna </ts>
               <ts e="T551" id="Seg_5227" n="e" s="T550">monoʔkosʼtə. </ts>
               <ts e="T552" id="Seg_5229" n="e" s="T551">Măn </ts>
               <ts e="T553" id="Seg_5231" n="e" s="T552">măndəm: </ts>
               <ts e="T554" id="Seg_5233" n="e" s="T553">Tăn </ts>
               <ts e="T555" id="Seg_5235" n="e" s="T554">(nʼi </ts>
               <ts e="T556" id="Seg_5237" n="e" s="T555">git) </ts>
               <ts e="T557" id="Seg_5239" n="e" s="T556">(kək-) </ts>
               <ts e="T558" id="Seg_5241" n="e" s="T557">(kak </ts>
               <ts e="T559" id="Seg_5243" n="e" s="T558">măn). </ts>
               <ts e="T560" id="Seg_5245" n="e" s="T559">Dĭrgit. </ts>
               <ts e="T561" id="Seg_5247" n="e" s="T560">Nu </ts>
               <ts e="T562" id="Seg_5249" n="e" s="T561">măn </ts>
               <ts e="T563" id="Seg_5251" n="e" s="T562">tănzi </ts>
               <ts e="T564" id="Seg_5253" n="e" s="T563">ej </ts>
               <ts e="T565" id="Seg_5255" n="e" s="T564">amnolam, </ts>
               <ts e="T566" id="Seg_5257" n="e" s="T565">a </ts>
               <ts e="T567" id="Seg_5259" n="e" s="T566">nʼizi. </ts>
               <ts e="T568" id="Seg_5261" n="e" s="T567">Dĭgəttə </ts>
               <ts e="T569" id="Seg_5263" n="e" s="T568">iam </ts>
               <ts e="T570" id="Seg_5265" n="e" s="T569">măndə:" </ts>
               <ts e="T571" id="Seg_5267" n="e" s="T570">Kanaʔ. </ts>
               <ts e="T572" id="Seg_5269" n="e" s="T571">Dĭn </ts>
               <ts e="T573" id="Seg_5271" n="e" s="T572">aktʼa </ts>
               <ts e="T574" id="Seg_5273" n="e" s="T573">iʔgö. </ts>
               <ts e="T575" id="Seg_5275" n="e" s="T574">Ugandə </ts>
               <ts e="T576" id="Seg_5277" n="e" s="T575">(k-) </ts>
               <ts e="T577" id="Seg_5279" n="e" s="T576">jakše </ts>
               <ts e="T578" id="Seg_5281" n="e" s="T577">kuza. </ts>
               <ts e="T579" id="Seg_5283" n="e" s="T578">A </ts>
               <ts e="T580" id="Seg_5285" n="e" s="T579">măn </ts>
               <ts e="T581" id="Seg_5287" n="e" s="T580">(nuliam): </ts>
               <ts e="T582" id="Seg_5289" n="e" s="T581">Ej. </ts>
               <ts e="T583" id="Seg_5291" n="e" s="T582">Da </ts>
               <ts e="T584" id="Seg_5293" n="e" s="T583">kanaʔ, </ts>
               <ts e="T585" id="Seg_5295" n="e" s="T584">da </ts>
               <ts e="T586" id="Seg_5297" n="e" s="T585">amnoʔ </ts>
               <ts e="T587" id="Seg_5299" n="e" s="T586">dĭzi. </ts>
               <ts e="T588" id="Seg_5301" n="e" s="T587">Onʼiʔ </ts>
               <ts e="T589" id="Seg_5303" n="e" s="T588">kuza </ts>
               <ts e="T590" id="Seg_5305" n="e" s="T589">šobi </ts>
               <ts e="T591" id="Seg_5307" n="e" s="T590">monoʔkosʼtə. </ts>
               <ts e="T592" id="Seg_5309" n="e" s="T591">A </ts>
               <ts e="T593" id="Seg_5311" n="e" s="T592">măn </ts>
               <ts e="T594" id="Seg_5313" n="e" s="T593">ej </ts>
               <ts e="T595" id="Seg_5315" n="e" s="T594">kaliam. </ts>
               <ts e="T596" id="Seg_5317" n="e" s="T595">(A </ts>
               <ts e="T597" id="Seg_5319" n="e" s="T596">ia=). </ts>
               <ts e="T598" id="Seg_5321" n="e" s="T597">Ej </ts>
               <ts e="T599" id="Seg_5323" n="e" s="T598">kaliam, </ts>
               <ts e="T600" id="Seg_5325" n="e" s="T599">dĭ </ts>
               <ts e="T601" id="Seg_5327" n="e" s="T600">(ej) </ts>
               <ts e="T602" id="Seg_5329" n="e" s="T601">kuvas, </ts>
               <ts e="T603" id="Seg_5331" n="e" s="T602">(üjü=) </ts>
               <ts e="T604" id="Seg_5333" n="e" s="T603">üjüt </ts>
               <ts e="T605" id="Seg_5335" n="e" s="T604">bar </ts>
               <ts e="T606" id="Seg_5337" n="e" s="T605">(ponʼ-) </ts>
               <ts e="T607" id="Seg_5339" n="e" s="T606">păjdʼaŋ, </ts>
               <ts e="T608" id="Seg_5341" n="e" s="T607">(bost-) </ts>
               <ts e="T609" id="Seg_5343" n="e" s="T608">muʔzendə </ts>
               <ts e="T610" id="Seg_5345" n="e" s="T609">kömə. </ts>
               <ts e="T611" id="Seg_5347" n="e" s="T610">Šĭket </ts>
               <ts e="T612" id="Seg_5349" n="e" s="T611">ej </ts>
               <ts e="T613" id="Seg_5351" n="e" s="T612">dʼăbaktəria, </ts>
               <ts e="T614" id="Seg_5353" n="e" s="T613">ej </ts>
               <ts e="T615" id="Seg_5355" n="e" s="T614">jakše </ts>
               <ts e="T616" id="Seg_5357" n="e" s="T615">dʼăbaktəria, </ts>
               <ts e="T617" id="Seg_5359" n="e" s="T616">kuzaŋdə </ts>
               <ts e="T618" id="Seg_5361" n="e" s="T617">ej </ts>
               <ts e="T619" id="Seg_5363" n="e" s="T618">(nʼilgöliaʔjə). </ts>
               <ts e="T620" id="Seg_5365" n="e" s="T619">A </ts>
               <ts e="T621" id="Seg_5367" n="e" s="T620">iam </ts>
               <ts e="T622" id="Seg_5369" n="e" s="T621">măndə: </ts>
               <ts e="T623" id="Seg_5371" n="e" s="T622">Kanaʔ, </ts>
               <ts e="T624" id="Seg_5373" n="e" s="T623">dĭn </ts>
               <ts e="T625" id="Seg_5375" n="e" s="T624">tüžöj </ts>
               <ts e="T626" id="Seg_5377" n="e" s="T625">ige, </ts>
               <ts e="T627" id="Seg_5379" n="e" s="T626">i </ts>
               <ts e="T628" id="Seg_5381" n="e" s="T627">ine </ts>
               <ts e="T629" id="Seg_5383" n="e" s="T628">ige, </ts>
               <ts e="T630" id="Seg_5385" n="e" s="T629">i </ts>
               <ts e="T631" id="Seg_5387" n="e" s="T630">ipek </ts>
               <ts e="T632" id="Seg_5389" n="e" s="T631">iʔgö. </ts>
               <ts e="T633" id="Seg_5391" n="e" s="T632">A </ts>
               <ts e="T634" id="Seg_5393" n="e" s="T633">(măn=) </ts>
               <ts e="T635" id="Seg_5395" n="e" s="T634">măn </ts>
               <ts e="T636" id="Seg_5397" n="e" s="T635">mămbiam: </ts>
               <ts e="T637" id="Seg_5399" n="e" s="T636">Măna </ts>
               <ts e="T638" id="Seg_5401" n="e" s="T637">inezi </ts>
               <ts e="T639" id="Seg_5403" n="e" s="T638">ej </ts>
               <ts e="T640" id="Seg_5405" n="e" s="T639">amnosʼtə, </ts>
               <ts e="T641" id="Seg_5407" n="e" s="T640">i </ts>
               <ts e="T642" id="Seg_5409" n="e" s="T641">tüžöjdə </ts>
               <ts e="T643" id="Seg_5411" n="e" s="T642">ej </ts>
               <ts e="T644" id="Seg_5413" n="e" s="T643">amnosʼtə, </ts>
               <ts e="T645" id="Seg_5415" n="e" s="T644">(nada </ts>
               <ts e="T646" id="Seg_5417" n="e" s="T645">tănan </ts>
               <ts e="T647" id="Seg_5419" n="e" s="T646">dăk=) </ts>
               <ts e="T648" id="Seg_5421" n="e" s="T647">(tăn) </ts>
               <ts e="T649" id="Seg_5423" n="e" s="T648">tănan </ts>
               <ts e="T650" id="Seg_5425" n="e" s="T649">kereʔ, </ts>
               <ts e="T651" id="Seg_5427" n="e" s="T650">dak </ts>
               <ts e="T652" id="Seg_5429" n="e" s="T651">tăn </ts>
               <ts e="T653" id="Seg_5431" n="e" s="T652">kanaʔ. </ts>
               <ts e="T654" id="Seg_5433" n="e" s="T653">Tanitsagən </ts>
               <ts e="T655" id="Seg_5435" n="e" s="T654">šobi </ts>
               <ts e="T656" id="Seg_5437" n="e" s="T655">onʼiʔ </ts>
               <ts e="T657" id="Seg_5439" n="e" s="T656">tibi. </ts>
               <ts e="T658" id="Seg_5441" n="e" s="T657">Dĭm </ts>
               <ts e="T659" id="Seg_5443" n="e" s="T658">ne </ts>
               <ts e="T660" id="Seg_5445" n="e" s="T659">barəʔluʔpi. </ts>
               <ts e="T661" id="Seg_5447" n="e" s="T660">Dĭ </ts>
               <ts e="T662" id="Seg_5449" n="e" s="T661">davaj </ts>
               <ts e="T663" id="Seg_5451" n="e" s="T662">măna </ts>
               <ts e="T664" id="Seg_5453" n="e" s="T663">monoʔkosʼtə. </ts>
               <ts e="T665" id="Seg_5455" n="e" s="T664">Iam </ts>
               <ts e="T666" id="Seg_5457" n="e" s="T665">stal </ts>
               <ts e="T667" id="Seg_5459" n="e" s="T666">(mĭnzerzittə) </ts>
               <ts e="T668" id="Seg_5461" n="e" s="T667">uja, </ts>
               <ts e="T669" id="Seg_5463" n="e" s="T668">dĭzem </ts>
               <ts e="T670" id="Seg_5465" n="e" s="T669">(bĭd-) </ts>
               <ts e="T671" id="Seg_5467" n="e" s="T670">bădəsʼtə. </ts>
               <ts e="T672" id="Seg_5469" n="e" s="T671">A </ts>
               <ts e="T673" id="Seg_5471" n="e" s="T672">măn </ts>
               <ts e="T674" id="Seg_5473" n="e" s="T673">mălliam: </ts>
               <ts e="T675" id="Seg_5475" n="e" s="T674">kanzittə </ts>
               <ts e="T676" id="Seg_5477" n="e" s="T675">(ilʼi) </ts>
               <ts e="T677" id="Seg_5479" n="e" s="T676">ej </ts>
               <ts e="T678" id="Seg_5481" n="e" s="T677">kanzittə? </ts>
               <ts e="T679" id="Seg_5483" n="e" s="T678">Хошь </ts>
               <ts e="T680" id="Seg_5485" n="e" s="T679">kanaʔ, </ts>
               <ts e="T681" id="Seg_5487" n="e" s="T680">хошь </ts>
               <ts e="T682" id="Seg_5489" n="e" s="T681">iʔ </ts>
               <ts e="T683" id="Seg_5491" n="e" s="T682">kanaʔ. </ts>
               <ts e="T684" id="Seg_5493" n="e" s="T683">Tăn </ts>
               <ts e="T685" id="Seg_5495" n="e" s="T684">tüjö </ts>
               <ts e="T686" id="Seg_5497" n="e" s="T685">(s-) </ts>
               <ts e="T687" id="Seg_5499" n="e" s="T686">(bostə </ts>
               <ts e="T688" id="Seg_5501" n="e" s="T687">urgo=) </ts>
               <ts e="T689" id="Seg_5503" n="e" s="T688">bostə </ts>
               <ts e="T690" id="Seg_5505" n="e" s="T689">tĭmnel </ts>
               <ts e="T691" id="Seg_5507" n="e" s="T690">ĭmbi </ts>
               <ts e="T692" id="Seg_5509" n="e" s="T691">(azittə). </ts>
               <ts e="T693" id="Seg_5511" n="e" s="T692">Dĭgəttə </ts>
               <ts e="T694" id="Seg_5513" n="e" s="T693">dĭ </ts>
               <ts e="T695" id="Seg_5515" n="e" s="T694">tibi </ts>
               <ts e="T696" id="Seg_5517" n="e" s="T695">(sʼ-) </ts>
               <ts e="T697" id="Seg_5519" n="e" s="T696">šobi. </ts>
               <ts e="T698" id="Seg_5521" n="e" s="T697">Măna </ts>
               <ts e="T699" id="Seg_5523" n="e" s="T698">amnobi. </ts>
               <ts e="T700" id="Seg_5525" n="e" s="T699">I </ts>
               <ts e="T701" id="Seg_5527" n="e" s="T700">davaj </ts>
               <ts e="T702" id="Seg_5529" n="e" s="T701">monoʔkosʼtə </ts>
               <ts e="T703" id="Seg_5531" n="e" s="T702">bostə. </ts>
               <ts e="T704" id="Seg_5533" n="e" s="T703">I </ts>
               <ts e="T705" id="Seg_5535" n="e" s="T704">davaj </ts>
               <ts e="T706" id="Seg_5537" n="e" s="T705">kudajdə </ts>
               <ts e="T707" id="Seg_5539" n="e" s="T706">numan üzəsʼtə. </ts>
               <ts e="T708" id="Seg_5541" n="e" s="T707">Ej </ts>
               <ts e="T709" id="Seg_5543" n="e" s="T708">baʔlim </ts>
               <ts e="T710" id="Seg_5545" n="e" s="T709">tănan, </ts>
               <ts e="T711" id="Seg_5547" n="e" s="T710">a </ts>
               <ts e="T712" id="Seg_5549" n="e" s="T711">munolam </ts>
               <ts e="T713" id="Seg_5551" n="e" s="T712">jakše </ts>
               <ts e="T714" id="Seg_5553" n="e" s="T713">amnolubaʔ. </ts>
               <ts e="T715" id="Seg_5555" n="e" s="T714">Dĭgəttə </ts>
               <ts e="T716" id="Seg_5557" n="e" s="T715">dĭ </ts>
               <ts e="T717" id="Seg_5559" n="e" s="T716">ine </ts>
               <ts e="T718" id="Seg_5561" n="e" s="T717">(körerbi), </ts>
               <ts e="T719" id="Seg_5563" n="e" s="T718">(m- </ts>
               <ts e="T720" id="Seg_5565" n="e" s="T719">miʔ=) </ts>
               <ts e="T721" id="Seg_5567" n="e" s="T720">măn </ts>
               <ts e="T722" id="Seg_5569" n="e" s="T721">dĭzi </ts>
               <ts e="T723" id="Seg_5571" n="e" s="T722">kambiam </ts>
               <ts e="T724" id="Seg_5573" n="e" s="T723">Permʼakovtə. </ts>
               <ts e="T725" id="Seg_5575" n="e" s="T724">Ara </ts>
               <ts e="T726" id="Seg_5577" n="e" s="T725">ibiem, </ts>
               <ts e="T727" id="Seg_5579" n="e" s="T726">deʔpibeʔ </ts>
               <ts e="T728" id="Seg_5581" n="e" s="T727">(s- </ts>
               <ts e="T729" id="Seg_5583" n="e" s="T728">)… </ts>
               <ts e="T730" id="Seg_5585" n="e" s="T729">Stoldə </ts>
               <ts e="T731" id="Seg_5587" n="e" s="T730">oʔbdəbibeʔ, </ts>
               <ts e="T732" id="Seg_5589" n="e" s="T731">il </ts>
               <ts e="T733" id="Seg_5591" n="e" s="T732">oʔbdəbibeʔ. </ts>
               <ts e="T734" id="Seg_5593" n="e" s="T733">Ara </ts>
               <ts e="T735" id="Seg_5595" n="e" s="T734">bĭʔpiʔi, </ts>
               <ts e="T736" id="Seg_5597" n="e" s="T735">ambiʔi, </ts>
               <ts e="T737" id="Seg_5599" n="e" s="T736">dĭgəttə </ts>
               <ts e="T738" id="Seg_5601" n="e" s="T737">miʔ </ts>
               <ts e="T739" id="Seg_5603" n="e" s="T738">(ka-) </ts>
               <ts e="T740" id="Seg_5605" n="e" s="T739">kambibaʔ </ts>
               <ts e="T741" id="Seg_5607" n="e" s="T740">stanitsanə. </ts>
               <ts e="T742" id="Seg_5609" n="e" s="T741">Dĭgəttə </ts>
               <ts e="T743" id="Seg_5611" n="e" s="T742">(manobiam) </ts>
               <ts e="T744" id="Seg_5613" n="e" s="T743">nagur </ts>
               <ts e="T745" id="Seg_5615" n="e" s="T744">kö. </ts>
               <ts e="T746" id="Seg_5617" n="e" s="T745">Dĭgəttə </ts>
               <ts e="T747" id="Seg_5619" n="e" s="T746">văjna </ts>
               <ts e="T748" id="Seg_5621" n="e" s="T747">ibi, </ts>
               <ts e="T749" id="Seg_5623" n="e" s="T748">dĭm </ts>
               <ts e="T750" id="Seg_5625" n="e" s="T749">ibiʔi </ts>
               <ts e="T751" id="Seg_5627" n="e" s="T750">văjnanə. </ts>
               <ts e="T752" id="Seg_5629" n="e" s="T751">Dĭgəttə </ts>
               <ts e="T753" id="Seg_5631" n="e" s="T752">măn </ts>
               <ts e="T754" id="Seg_5633" n="e" s="T753">nanəʔzəbi </ts>
               <ts e="T755" id="Seg_5635" n="e" s="T754">ibiem, </ts>
               <ts e="T756" id="Seg_5637" n="e" s="T755">(koʔbdo </ts>
               <ts e="T757" id="Seg_5639" n="e" s="T756">dĭ). </ts>
               <ts e="T758" id="Seg_5641" n="e" s="T757">Dĭgəttə </ts>
               <ts e="T759" id="Seg_5643" n="e" s="T758">dĭ </ts>
               <ts e="T760" id="Seg_5645" n="e" s="T759">šobi </ts>
               <ts e="T761" id="Seg_5647" n="e" s="T760">văjnagə, </ts>
               <ts e="T762" id="Seg_5649" n="e" s="T761">раненый </ts>
               <ts e="T763" id="Seg_5651" n="e" s="T762">ibi. </ts>
               <ts e="T764" id="Seg_5653" n="e" s="T763">Dĭgəttə </ts>
               <ts e="T765" id="Seg_5655" n="e" s="T764">iššo </ts>
               <ts e="T766" id="Seg_5657" n="e" s="T765">amnobibeʔ, </ts>
               <ts e="T767" id="Seg_5659" n="e" s="T766">măn </ts>
               <ts e="T768" id="Seg_5661" n="e" s="T767">bazo </ts>
               <ts e="T769" id="Seg_5663" n="e" s="T768">nanəʔzəbi </ts>
               <ts e="T770" id="Seg_5665" n="e" s="T769">ibi. </ts>
               <ts e="T771" id="Seg_5667" n="e" s="T770">Dĭgəttə </ts>
               <ts e="T772" id="Seg_5669" n="e" s="T771">dĭ </ts>
               <ts e="T773" id="Seg_5671" n="e" s="T772">(n-DMG-nobi), </ts>
               <ts e="T774" id="Seg_5673" n="e" s="T773">dĭ </ts>
               <ts e="T775" id="Seg_5675" n="e" s="T774">măna </ts>
               <ts e="T776" id="Seg_5677" n="e" s="T775">sürerbi </ts>
               <ts e="T777" id="Seg_5679" n="e" s="T776">i </ts>
               <ts e="T778" id="Seg_5681" n="e" s="T777">dĭm </ts>
               <ts e="T779" id="Seg_5683" n="e" s="T778">ibi. </ts>
               <ts e="T780" id="Seg_5685" n="e" s="T779">Iššo </ts>
               <ts e="T781" id="Seg_5687" n="e" s="T780">onʼiʔ </ts>
               <ts e="T782" id="Seg_5689" n="e" s="T781">(ni-) </ts>
               <ts e="T783" id="Seg_5691" n="e" s="T782">nʼi </ts>
               <ts e="T784" id="Seg_5693" n="e" s="T783">deʔpiem. </ts>
               <ts e="T785" id="Seg_5695" n="e" s="T784">Dĭgəttə </ts>
               <ts e="T786" id="Seg_5697" n="e" s="T785">maʔnʼi </ts>
               <ts e="T787" id="Seg_5699" n="e" s="T786">šobiam, </ts>
               <ts e="T788" id="Seg_5701" n="e" s="T787">(aba- </ts>
               <ts e="T789" id="Seg_5703" n="e" s="T788">aban-) </ts>
               <ts e="T790" id="Seg_5705" n="e" s="T789">abanə </ts>
               <ts e="T791" id="Seg_5707" n="e" s="T790">i </ts>
               <ts e="T792" id="Seg_5709" n="e" s="T791">ianə. </ts>
               <ts e="T793" id="Seg_5711" n="e" s="T792">Dĭgəttə </ts>
               <ts e="T794" id="Seg_5713" n="e" s="T793">külaːmbiʔi, </ts>
               <ts e="T795" id="Seg_5715" n="e" s="T794">koʔbdo </ts>
               <ts e="T796" id="Seg_5717" n="e" s="T795">i </ts>
               <ts e="T797" id="Seg_5719" n="e" s="T796">nʼi, </ts>
               <ts e="T798" id="Seg_5721" n="e" s="T797">(măn) </ts>
               <ts e="T799" id="Seg_5723" n="e" s="T798">unnʼa </ts>
               <ts e="T800" id="Seg_5725" n="e" s="T799">(mola-) </ts>
               <ts e="T801" id="Seg_5727" n="e" s="T800">molambiam. </ts>
               <ts e="T802" id="Seg_5729" n="e" s="T801">Oʔkʼe </ts>
               <ts e="T803" id="Seg_5731" n="e" s="T802">amnobiam, </ts>
               <ts e="T804" id="Seg_5733" n="e" s="T803">bazo </ts>
               <ts e="T805" id="Seg_5735" n="e" s="T804">kuza </ts>
               <ts e="T806" id="Seg_5737" n="e" s="T805">măna </ts>
               <ts e="T807" id="Seg_5739" n="e" s="T806">monoʔkobi. </ts>
               <ts e="T808" id="Seg_5741" n="e" s="T807">Măn </ts>
               <ts e="T809" id="Seg_5743" n="e" s="T808">kambiam </ts>
               <ts e="T810" id="Seg_5745" n="e" s="T809">dĭzi. </ts>
               <ts e="T811" id="Seg_5747" n="e" s="T810">Amnobiam, </ts>
               <ts e="T812" id="Seg_5749" n="e" s="T811">oʔb, </ts>
               <ts e="T813" id="Seg_5751" n="e" s="T812">šide, </ts>
               <ts e="T814" id="Seg_5753" n="e" s="T813">nagur, </ts>
               <ts e="T815" id="Seg_5755" n="e" s="T814">teʔtə, </ts>
               <ts e="T816" id="Seg_5757" n="e" s="T815">sumna, </ts>
               <ts e="T817" id="Seg_5759" n="e" s="T816">muktuʔ, </ts>
               <ts e="T818" id="Seg_5761" n="e" s="T817">sejʔpü </ts>
               <ts e="T819" id="Seg_5763" n="e" s="T818">kö </ts>
               <ts e="T820" id="Seg_5765" n="e" s="T819">amnobiam. </ts>
               <ts e="T821" id="Seg_5767" n="e" s="T820">Šide </ts>
               <ts e="T822" id="Seg_5769" n="e" s="T821">ešši </ts>
               <ts e="T823" id="Seg_5771" n="e" s="T822">ibiʔi. </ts>
               <ts e="T824" id="Seg_5773" n="e" s="T823">(Ugandə) </ts>
               <ts e="T825" id="Seg_5775" n="e" s="T824">ej </ts>
               <ts e="T826" id="Seg_5777" n="e" s="T825">jakše, </ts>
               <ts e="T827" id="Seg_5779" n="e" s="T826">ara </ts>
               <ts e="T828" id="Seg_5781" n="e" s="T827">bĭʔpi, </ts>
               <ts e="T829" id="Seg_5783" n="e" s="T828">(dʼabrolaʔpi), </ts>
               <ts e="T830" id="Seg_5785" n="e" s="T829">kudonzlaʔpi. </ts>
               <ts e="T831" id="Seg_5787" n="e" s="T830">(Kamen) </ts>
               <ts e="T832" id="Seg_5789" n="e" s="T831">Kamen </ts>
               <ts e="T833" id="Seg_5791" n="e" s="T832">baʔluʔpiam, </ts>
               <ts e="T834" id="Seg_5793" n="e" s="T833">(bar </ts>
               <ts e="T835" id="Seg_5795" n="e" s="T834">ulum=) </ts>
               <ts e="T836" id="Seg_5797" n="e" s="T835">bar </ts>
               <ts e="T837" id="Seg_5799" n="e" s="T836">eʔbdəm </ts>
               <ts e="T838" id="Seg_5801" n="e" s="T837">sajnʼeʔpi. </ts>
               <ts e="T839" id="Seg_5803" n="e" s="T838">(Vekžen) </ts>
               <ts e="T840" id="Seg_5805" n="e" s="T839">tăŋ </ts>
               <ts e="T841" id="Seg_5807" n="e" s="T840">münörbi, </ts>
               <ts e="T842" id="Seg_5809" n="e" s="T841">ažnə </ts>
               <ts e="T843" id="Seg_5811" n="e" s="T842">kem </ts>
               <ts e="T844" id="Seg_5813" n="e" s="T843">ibi. </ts>
               <ts e="T845" id="Seg_5815" n="e" s="T844">Dĭgəttə </ts>
               <ts e="T846" id="Seg_5817" n="e" s="T845">măn </ts>
               <ts e="T847" id="Seg_5819" n="e" s="T846">(nʼim=) </ts>
               <ts e="T848" id="Seg_5821" n="e" s="T847">dĭm </ts>
               <ts e="T849" id="Seg_5823" n="e" s="T848">barəʔluʔpiam. </ts>
               <ts e="T850" id="Seg_5825" n="e" s="T849">Măn </ts>
               <ts e="T851" id="Seg_5827" n="e" s="T850">onʼiʔ </ts>
               <ts e="T852" id="Seg_5829" n="e" s="T851">nʼim </ts>
               <ts e="T853" id="Seg_5831" n="e" s="T852">ibi. </ts>
               <ts e="T854" id="Seg_5833" n="e" s="T853">Tüj </ts>
               <ts e="T855" id="Seg_5835" n="e" s="T854">măndəm </ts>
               <ts e="T856" id="Seg_5837" n="e" s="T855">šindinədə: </ts>
               <ts e="T858" id="Seg_5839" n="e" s="T856">ej </ts>
               <ts e="T859" id="Seg_5841" n="e" s="T858">tibinə, </ts>
               <ts e="T860" id="Seg_5843" n="e" s="T859">unnʼa </ts>
               <ts e="T861" id="Seg_5845" n="e" s="T860">amnolam, </ts>
               <ts e="T862" id="Seg_5847" n="e" s="T861">nʼizi, </ts>
               <ts e="T863" id="Seg_5849" n="e" s="T862">bostə </ts>
               <ts e="T864" id="Seg_5851" n="e" s="T863">nʼizi. </ts>
               <ts e="T865" id="Seg_5853" n="e" s="T864">Dĭgəttə </ts>
               <ts e="T866" id="Seg_5855" n="e" s="T865">onʼiʔ </ts>
               <ts e="T867" id="Seg_5857" n="e" s="T866">nʼi </ts>
               <ts e="T868" id="Seg_5859" n="e" s="T867">davaj </ts>
               <ts e="T869" id="Seg_5861" n="e" s="T868">măna </ts>
               <ts e="T870" id="Seg_5863" n="e" s="T869">monoʔkosʼtə. </ts>
               <ts e="T871" id="Seg_5865" n="e" s="T870">Măn </ts>
               <ts e="T872" id="Seg_5867" n="e" s="T871">не </ts>
               <ts e="T873" id="Seg_5869" n="e" s="T872">хотел </ts>
               <ts e="T874" id="Seg_5871" n="e" s="T873">идти. </ts>
               <ts e="T875" id="Seg_5873" n="e" s="T874">Ej </ts>
               <ts e="T876" id="Seg_5875" n="e" s="T875">kuvandoliam. </ts>
               <ts e="T877" id="Seg_5877" n="e" s="T876">(Kadəlbə) </ts>
               <ts e="T878" id="Seg_5879" n="e" s="T877">Kadəldə </ts>
               <ts e="T879" id="Seg_5881" n="e" s="T878">nʼišpək. </ts>
               <ts e="T880" id="Seg_5883" n="e" s="T879">Ej </ts>
               <ts e="T881" id="Seg_5885" n="e" s="T880">kalam. </ts>
               <ts e="T882" id="Seg_5887" n="e" s="T881">(Dĭ) </ts>
               <ts e="T883" id="Seg_5889" n="e" s="T882">măndə: </ts>
               <ts e="T884" id="Seg_5891" n="e" s="T883">Davaj </ts>
               <ts e="T885" id="Seg_5893" n="e" s="T884">amnosʼtə </ts>
               <ts e="T886" id="Seg_5895" n="e" s="T885">tănzi. </ts>
               <ts e="T1055" id="Seg_5897" n="e" s="T886">Nu, </ts>
               <ts e="T887" id="Seg_5899" n="e" s="T1055">tenöbiam, </ts>
               <ts e="T888" id="Seg_5901" n="e" s="T887">tenöbiam, </ts>
               <ts e="T889" id="Seg_5903" n="e" s="T888">dĭgəttə </ts>
               <ts e="T890" id="Seg_5905" n="e" s="T889">kambiam. </ts>
               <ts e="T891" id="Seg_5907" n="e" s="T890">Dĭgəttə </ts>
               <ts e="T892" id="Seg_5909" n="e" s="T891">bazo: </ts>
               <ts e="T893" id="Seg_5911" n="e" s="T892">(Ba-) </ts>
               <ts e="T894" id="Seg_5913" n="e" s="T893">barəʔlim, </ts>
               <ts e="T895" id="Seg_5915" n="e" s="T894">măndəm, </ts>
               <ts e="T896" id="Seg_5917" n="e" s="T895">tănan, </ts>
               <ts e="T897" id="Seg_5919" n="e" s="T896">ej </ts>
               <ts e="T898" id="Seg_5921" n="e" s="T897">amnolam </ts>
               <ts e="T899" id="Seg_5923" n="e" s="T898">tănzi. </ts>
               <ts e="T900" id="Seg_5925" n="e" s="T899">Dibər </ts>
               <ts e="T901" id="Seg_5927" n="e" s="T900">davaj </ts>
               <ts e="T902" id="Seg_5929" n="e" s="T901">dʼorzittə, </ts>
               <ts e="T903" id="Seg_5931" n="e" s="T902">tăŋ </ts>
               <ts e="T904" id="Seg_5933" n="e" s="T903">dʼorbi, </ts>
               <ts e="T905" id="Seg_5935" n="e" s="T904">ugandə. </ts>
               <ts e="T906" id="Seg_5937" n="e" s="T905">Dĭgəttə </ts>
               <ts e="T907" id="Seg_5939" n="e" s="T906">măn </ts>
               <ts e="T908" id="Seg_5941" n="e" s="T907">mălliam: </ts>
               <ts e="T909" id="Seg_5943" n="e" s="T908">((…)) </ts>
               <ts e="T910" id="Seg_5945" n="e" s="T909">tura </ts>
               <ts e="T911" id="Seg_5947" n="e" s="T910">nuldəbəj, </ts>
               <ts e="T912" id="Seg_5949" n="e" s="T911">dĭgəttə </ts>
               <ts e="T913" id="Seg_5951" n="e" s="T912">pušaj </ts>
               <ts e="T914" id="Seg_5953" n="e" s="T913">kaləj. </ts>
               <ts e="T915" id="Seg_5955" n="e" s="T914">I </ts>
               <ts e="T916" id="Seg_5957" n="e" s="T915">dăre </ts>
               <ts e="T917" id="Seg_5959" n="e" s="T916">ibi: </ts>
               <ts e="T918" id="Seg_5961" n="e" s="T917">tura </ts>
               <ts e="T919" id="Seg_5963" n="e" s="T918">(tol-) </ts>
               <ts e="T920" id="Seg_5965" n="e" s="T919">nuldəbi </ts>
               <ts e="T921" id="Seg_5967" n="e" s="T920">i </ts>
               <ts e="T1058" id="Seg_5969" n="e" s="T921">kalla </ts>
               <ts e="T922" id="Seg_5971" n="e" s="T1058">dʼürbi, </ts>
               <ts e="T923" id="Seg_5973" n="e" s="T922">măn </ts>
               <ts e="T924" id="Seg_5975" n="e" s="T923">unnʼa </ts>
               <ts e="T925" id="Seg_5977" n="e" s="T924">maːluʔpiam. </ts>
               <ts e="T926" id="Seg_5979" n="e" s="T925">Tüj </ts>
               <ts e="T927" id="Seg_5981" n="e" s="T926">măn </ts>
               <ts e="T928" id="Seg_5983" n="e" s="T927">üge </ts>
               <ts e="T929" id="Seg_5985" n="e" s="T928">unnʼa </ts>
               <ts e="T930" id="Seg_5987" n="e" s="T929">amnolaʔbəm, </ts>
               <ts e="T931" id="Seg_5989" n="e" s="T930">(amnolaʔbəm), </ts>
               <ts e="T932" id="Seg_5991" n="e" s="T931">už </ts>
               <ts e="T933" id="Seg_5993" n="e" s="T932">šide </ts>
               <ts e="T934" id="Seg_5995" n="e" s="T933">bʼeʔ </ts>
               <ts e="T935" id="Seg_5997" n="e" s="T934">ki </ts>
               <ts e="T936" id="Seg_5999" n="e" s="T935">amnolaʔbəm. </ts>
               <ts e="T937" id="Seg_6001" n="e" s="T936">Kamən </ts>
               <ts e="T938" id="Seg_6003" n="e" s="T937">dĭ </ts>
               <ts e="T939" id="Seg_6005" n="e" s="T938">barəʔluʔpi. </ts>
               <ts e="T940" id="Seg_6007" n="e" s="T939">Dĭgəttə </ts>
               <ts e="T941" id="Seg_6009" n="e" s="T940">miʔ </ts>
               <ts e="T942" id="Seg_6011" n="e" s="T941">dĭ </ts>
               <ts e="T943" id="Seg_6013" n="e" s="T942">nʼizi </ts>
               <ts e="T944" id="Seg_6015" n="e" s="T943">bar </ts>
               <ts e="T945" id="Seg_6017" n="e" s="T944">pa </ts>
               <ts e="T946" id="Seg_6019" n="e" s="T945">baltuzi </ts>
               <ts e="T947" id="Seg_6021" n="e" s="T946">jaʔpibaʔ, </ts>
               <ts e="T948" id="Seg_6023" n="e" s="T947">tura </ts>
               <ts e="T949" id="Seg_6025" n="e" s="T948">(nulzittə-) </ts>
               <ts e="T950" id="Seg_6027" n="e" s="T949">nuldəzittə. </ts>
               <ts e="T951" id="Seg_6029" n="e" s="T950">(Tazi-) </ts>
               <ts e="T952" id="Seg_6031" n="e" s="T951">Tažerbibaʔ </ts>
               <ts e="T953" id="Seg_6033" n="e" s="T952">(ine-) </ts>
               <ts e="T954" id="Seg_6035" n="e" s="T953">inezi. </ts>
               <ts e="T955" id="Seg_6037" n="e" s="T954">I </ts>
               <ts e="T956" id="Seg_6039" n="e" s="T955">tura </ts>
               <ts e="T957" id="Seg_6041" n="e" s="T956">(nuldəbibaʔ). </ts>
               <ts e="T958" id="Seg_6043" n="e" s="T957">Tibizeŋ </ts>
               <ts e="T959" id="Seg_6045" n="e" s="T958">jaʔpiʔi </ts>
               <ts e="T960" id="Seg_6047" n="e" s="T959">baltuzi, </ts>
               <ts e="T961" id="Seg_6049" n="e" s="T960">a </ts>
               <ts e="T962" id="Seg_6051" n="e" s="T961">măn </ts>
               <ts e="T963" id="Seg_6053" n="e" s="T962">kubaʔi </ts>
               <ts e="T964" id="Seg_6055" n="e" s="T963">dĭʔnə </ts>
               <ts e="T965" id="Seg_6057" n="e" s="T964">abiam. </ts>
               <ts e="T966" id="Seg_6059" n="e" s="T965">Dĭgəttə </ts>
               <ts e="T967" id="Seg_6061" n="e" s="T966">tsutonkaʔi </ts>
               <ts e="T968" id="Seg_6063" n="e" s="T967">(taz-) </ts>
               <ts e="T969" id="Seg_6065" n="e" s="T968">tažerbiʔi, </ts>
               <ts e="T970" id="Seg_6067" n="e" s="T969">пол </ts>
               <ts e="T971" id="Seg_6069" n="e" s="T970">azittə </ts>
               <ts e="T972" id="Seg_6071" n="e" s="T971">(i </ts>
               <ts e="T973" id="Seg_6073" n="e" s="T972">потолок) </ts>
               <ts e="T974" id="Seg_6075" n="e" s="T973">azittə. </ts>
               <ts e="T975" id="Seg_6077" n="e" s="T974">A </ts>
               <ts e="T976" id="Seg_6079" n="e" s="T975">măn </ts>
               <ts e="T977" id="Seg_6081" n="e" s="T976">(kuda) </ts>
               <ts e="T978" id="Seg_6083" n="e" s="T977">urgo </ts>
               <ts e="T979" id="Seg_6085" n="e" s="T978">kubaʔi </ts>
               <ts e="T980" id="Seg_6087" n="e" s="T979">abiam. </ts>
               <ts e="T981" id="Seg_6089" n="e" s="T980">I </ts>
               <ts e="T982" id="Seg_6091" n="e" s="T981">jamaʔi </ts>
               <ts e="T983" id="Seg_6093" n="e" s="T982">šöʔpiem. </ts>
               <ts e="T984" id="Seg_6095" n="e" s="T983">A </ts>
               <ts e="T985" id="Seg_6097" n="e" s="T984">dĭ </ts>
               <ts e="T986" id="Seg_6099" n="e" s="T985">tože </ts>
               <ts e="T987" id="Seg_6101" n="e" s="T986">măna </ts>
               <ts e="T988" id="Seg_6103" n="e" s="T987">(kabaržəbi). </ts>
               <ts e="T989" id="Seg_6105" n="e" s="T988">Kubat </ts>
               <ts e="T990" id="Seg_6107" n="e" s="T989">edəmbi </ts>
               <ts e="T991" id="Seg_6109" n="e" s="T990">da </ts>
               <ts e="T992" id="Seg_6111" n="e" s="T991">ej </ts>
               <ts e="T993" id="Seg_6113" n="e" s="T992">tĭmnebi, </ts>
               <ts e="T994" id="Seg_6115" n="e" s="T993">măn </ts>
               <ts e="T995" id="Seg_6117" n="e" s="T994">dĭm </ts>
               <ts e="T996" id="Seg_6119" n="e" s="T995">tüšəlbiem. </ts>
               <ts e="T997" id="Seg_6121" n="e" s="T996">Măn </ts>
               <ts e="T998" id="Seg_6123" n="e" s="T997">bostə </ts>
               <ts e="T999" id="Seg_6125" n="e" s="T998">(tibin) </ts>
               <ts e="T1000" id="Seg_6127" n="e" s="T999">tibinə </ts>
               <ts e="T1001" id="Seg_6129" n="e" s="T1000">jamaʔi </ts>
               <ts e="T1002" id="Seg_6131" n="e" s="T1001">šöʔpiem. </ts>
               <ts e="T1003" id="Seg_6133" n="e" s="T1002">I </ts>
               <ts e="T1004" id="Seg_6135" n="e" s="T1003">parga </ts>
               <ts e="T1005" id="Seg_6137" n="e" s="T1004">šöʔpiem. </ts>
               <ts e="T1006" id="Seg_6139" n="e" s="T1005">Ularən </ts>
               <ts e="T1007" id="Seg_6141" n="e" s="T1006">kubaʔi. </ts>
               <ts e="T1008" id="Seg_6143" n="e" s="T1007">I </ts>
               <ts e="T1009" id="Seg_6145" n="e" s="T1008">üžü </ts>
               <ts e="T1010" id="Seg_6147" n="e" s="T1009">šöʔpiem. </ts>
               <ts e="T1011" id="Seg_6149" n="e" s="T1010">Kujnektə </ts>
               <ts e="T1012" id="Seg_6151" n="e" s="T1011">(šöʔpiem). </ts>
               <ts e="T1013" id="Seg_6153" n="e" s="T1012">Piʔmeʔi </ts>
               <ts e="T1014" id="Seg_6155" n="e" s="T1013">šöʔpiem. </ts>
               <ts e="T1015" id="Seg_6157" n="e" s="T1014">Dĭʔnə </ts>
               <ts e="T1016" id="Seg_6159" n="e" s="T1015">šerbiam </ts>
               <ts e="T1017" id="Seg_6161" n="e" s="T1016">ugandə </ts>
               <ts e="T1056" id="Seg_6163" n="e" s="T1017">(jakše) </ts>
               <ts e="T1018" id="Seg_6165" n="e" s="T1056">((BRK)). </ts>
               <ts e="T1019" id="Seg_6167" n="e" s="T1018">Šerbiem. </ts>
               <ts e="T1020" id="Seg_6169" n="e" s="T1019">(I-) </ts>
               <ts e="T1021" id="Seg_6171" n="e" s="T1020">Il </ts>
               <ts e="T1022" id="Seg_6173" n="e" s="T1021">bar </ts>
               <ts e="T1023" id="Seg_6175" n="e" s="T1022">măndərbiʔi: </ts>
               <ts e="T1024" id="Seg_6177" n="e" s="T1023">ugandə </ts>
               <ts e="T1025" id="Seg_6179" n="e" s="T1024">kuvas </ts>
               <ts e="T1026" id="Seg_6181" n="e" s="T1025">kuza </ts>
               <ts e="T1027" id="Seg_6183" n="e" s="T1026">molambi. </ts>
               <ts e="T1028" id="Seg_6185" n="e" s="T1027">Dĭgəttə </ts>
               <ts e="T1029" id="Seg_6187" n="e" s="T1028">miʔ </ts>
               <ts e="T1030" id="Seg_6189" n="e" s="T1029">dʼü </ts>
               <ts e="T1031" id="Seg_6191" n="e" s="T1030">(tarir- </ts>
               <ts e="T1032" id="Seg_6193" n="e" s="T1031">tajir-) </ts>
               <ts e="T1033" id="Seg_6195" n="e" s="T1032">(tarirlaʔpibaʔ). </ts>
               <ts e="T1034" id="Seg_6197" n="e" s="T1033">Aš </ts>
               <ts e="T1035" id="Seg_6199" n="e" s="T1034">kuʔpibaʔ. </ts>
               <ts e="T1036" id="Seg_6201" n="e" s="T1035">(Ez-) </ts>
               <ts e="T1037" id="Seg_6203" n="e" s="T1036">Özerbi, </ts>
               <ts e="T1038" id="Seg_6205" n="e" s="T1037">ugandə </ts>
               <ts e="T1039" id="Seg_6207" n="e" s="T1038">jakše </ts>
               <ts e="T1040" id="Seg_6209" n="e" s="T1039">ibi </ts>
               <ts e="T1041" id="Seg_6211" n="e" s="T1040">aš. </ts>
               <ts e="T1042" id="Seg_6213" n="e" s="T1041">Dĭgəttə </ts>
               <ts e="T1043" id="Seg_6215" n="e" s="T1042">un </ts>
               <ts e="T1044" id="Seg_6217" n="e" s="T1043">abibaʔ, </ts>
               <ts e="T1045" id="Seg_6219" n="e" s="T1044">tʼerməndə </ts>
               <ts e="T1046" id="Seg_6221" n="e" s="T1045">mĭmbibeʔ. </ts>
               <ts e="T1047" id="Seg_6223" n="e" s="T1046">Ipek </ts>
               <ts e="T1048" id="Seg_6225" n="e" s="T1047">pürbibeʔ. </ts>
               <ts e="T1049" id="Seg_6227" n="e" s="T1048">I </ts>
               <ts e="T1050" id="Seg_6229" n="e" s="T1049">budəj </ts>
               <ts e="T1051" id="Seg_6231" n="e" s="T1050">aš </ts>
               <ts e="T1052" id="Seg_6233" n="e" s="T1051">kuʔpibaʔ. </ts>
               <ts e="T1053" id="Seg_6235" n="e" s="T1052">(Jakšə) </ts>
               <ts e="T1054" id="Seg_6237" n="e" s="T1053">((DMG)). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_6238" s="T0">PKZ_196X_SU0215.001 (001)</ta>
            <ta e="T9" id="Seg_6239" s="T7">PKZ_196X_SU0215.002 (002)</ta>
            <ta e="T11" id="Seg_6240" s="T9">PKZ_196X_SU0215.003 (003)</ta>
            <ta e="T13" id="Seg_6241" s="T11">PKZ_196X_SU0215.004 (004)</ta>
            <ta e="T15" id="Seg_6242" s="T13">PKZ_196X_SU0215.005 (005)</ta>
            <ta e="T16" id="Seg_6243" s="T15">PKZ_196X_SU0215.006 (006)</ta>
            <ta e="T20" id="Seg_6244" s="T16">PKZ_196X_SU0215.007 (007)</ta>
            <ta e="T23" id="Seg_6245" s="T20">PKZ_196X_SU0215.008 (008)</ta>
            <ta e="T27" id="Seg_6246" s="T23">PKZ_196X_SU0215.009 (009)</ta>
            <ta e="T30" id="Seg_6247" s="T27">PKZ_196X_SU0215.010 (010)</ta>
            <ta e="T33" id="Seg_6248" s="T30">PKZ_196X_SU0215.011 (011)</ta>
            <ta e="T37" id="Seg_6249" s="T33">PKZ_196X_SU0215.012 (012)</ta>
            <ta e="T45" id="Seg_6250" s="T37">PKZ_196X_SU0215.013 (013)</ta>
            <ta e="T50" id="Seg_6251" s="T45">PKZ_196X_SU0215.014 (014)</ta>
            <ta e="T55" id="Seg_6252" s="T50">PKZ_196X_SU0215.015 (015)</ta>
            <ta e="T59" id="Seg_6253" s="T55">PKZ_196X_SU0215.016 (016)</ta>
            <ta e="T66" id="Seg_6254" s="T59">PKZ_196X_SU0215.017 (017) </ta>
            <ta e="T69" id="Seg_6255" s="T66">PKZ_196X_SU0215.018 (019)</ta>
            <ta e="T78" id="Seg_6256" s="T69">PKZ_196X_SU0215.019 (020)</ta>
            <ta e="T83" id="Seg_6257" s="T78">PKZ_196X_SU0215.020 (021)</ta>
            <ta e="T89" id="Seg_6258" s="T83">PKZ_196X_SU0215.021 (022)</ta>
            <ta e="T91" id="Seg_6259" s="T89">PKZ_196X_SU0215.022 (023)</ta>
            <ta e="T97" id="Seg_6260" s="T91">PKZ_196X_SU0215.023 (024)</ta>
            <ta e="T103" id="Seg_6261" s="T97">PKZ_196X_SU0215.024 (025) </ta>
            <ta e="T108" id="Seg_6262" s="T103">PKZ_196X_SU0215.025 (027)</ta>
            <ta e="T118" id="Seg_6263" s="T108">PKZ_196X_SU0215.026 (028) </ta>
            <ta e="T124" id="Seg_6264" s="T118">PKZ_196X_SU0215.027 (030)</ta>
            <ta e="T128" id="Seg_6265" s="T124">PKZ_196X_SU0215.028 (031)</ta>
            <ta e="T134" id="Seg_6266" s="T128">PKZ_196X_SU0215.029 (032)</ta>
            <ta e="T144" id="Seg_6267" s="T134">PKZ_196X_SU0215.030 (033)</ta>
            <ta e="T152" id="Seg_6268" s="T144">PKZ_196X_SU0215.031 (034)</ta>
            <ta e="T157" id="Seg_6269" s="T152">PKZ_196X_SU0215.032 (035)</ta>
            <ta e="T166" id="Seg_6270" s="T157">PKZ_196X_SU0215.033 (036)</ta>
            <ta e="T171" id="Seg_6271" s="T166">PKZ_196X_SU0215.034 (037)</ta>
            <ta e="T176" id="Seg_6272" s="T171">PKZ_196X_SU0215.035 (038)</ta>
            <ta e="T179" id="Seg_6273" s="T176">PKZ_196X_SU0215.036 (039)</ta>
            <ta e="T183" id="Seg_6274" s="T179">PKZ_196X_SU0215.037 (040)</ta>
            <ta e="T188" id="Seg_6275" s="T183">PKZ_196X_SU0215.038 (041)</ta>
            <ta e="T191" id="Seg_6276" s="T188">PKZ_196X_SU0215.039 (042)</ta>
            <ta e="T194" id="Seg_6277" s="T191">PKZ_196X_SU0215.040 (043)</ta>
            <ta e="T198" id="Seg_6278" s="T194">PKZ_196X_SU0215.041 (044)</ta>
            <ta e="T201" id="Seg_6279" s="T198">PKZ_196X_SU0215.042 (045)</ta>
            <ta e="T208" id="Seg_6280" s="T201">PKZ_196X_SU0215.043 (046)</ta>
            <ta e="T212" id="Seg_6281" s="T208">PKZ_196X_SU0215.044 (047)</ta>
            <ta e="T218" id="Seg_6282" s="T212">PKZ_196X_SU0215.045 (048)</ta>
            <ta e="T224" id="Seg_6283" s="T218">PKZ_196X_SU0215.046 (049)</ta>
            <ta e="T228" id="Seg_6284" s="T224">PKZ_196X_SU0215.047 (050)</ta>
            <ta e="T230" id="Seg_6285" s="T228">PKZ_196X_SU0215.048 (051)</ta>
            <ta e="T233" id="Seg_6286" s="T230">PKZ_196X_SU0215.049 (052)</ta>
            <ta e="T237" id="Seg_6287" s="T233">PKZ_196X_SU0215.050 (053)</ta>
            <ta e="T240" id="Seg_6288" s="T237">PKZ_196X_SU0215.051 (054)</ta>
            <ta e="T245" id="Seg_6289" s="T240">PKZ_196X_SU0215.052 (055)</ta>
            <ta e="T252" id="Seg_6290" s="T245">PKZ_196X_SU0215.053 (056)</ta>
            <ta e="T257" id="Seg_6291" s="T252">PKZ_196X_SU0215.054 (057)</ta>
            <ta e="T266" id="Seg_6292" s="T257">PKZ_196X_SU0215.055 (058)</ta>
            <ta e="T274" id="Seg_6293" s="T266">PKZ_196X_SU0215.056 (059)</ta>
            <ta e="T281" id="Seg_6294" s="T274">PKZ_196X_SU0215.057 (060)</ta>
            <ta e="T284" id="Seg_6295" s="T281">PKZ_196X_SU0215.058 (061)</ta>
            <ta e="T287" id="Seg_6296" s="T284">PKZ_196X_SU0215.059 (062)</ta>
            <ta e="T290" id="Seg_6297" s="T287">PKZ_196X_SU0215.060 (063)</ta>
            <ta e="T297" id="Seg_6298" s="T290">PKZ_196X_SU0215.061 (064)</ta>
            <ta e="T305" id="Seg_6299" s="T297">PKZ_196X_SU0215.062 (065)</ta>
            <ta e="T317" id="Seg_6300" s="T305">PKZ_196X_SU0215.063 (066)</ta>
            <ta e="T325" id="Seg_6301" s="T317">PKZ_196X_SU0215.064 (067)</ta>
            <ta e="T327" id="Seg_6302" s="T325">PKZ_196X_SU0215.065 (068)</ta>
            <ta e="T329" id="Seg_6303" s="T327">PKZ_196X_SU0215.066 (069)</ta>
            <ta e="T332" id="Seg_6304" s="T329">PKZ_196X_SU0215.067 (070)</ta>
            <ta e="T342" id="Seg_6305" s="T332">PKZ_196X_SU0215.068 (071)</ta>
            <ta e="T344" id="Seg_6306" s="T342">PKZ_196X_SU0215.069 (072)</ta>
            <ta e="T346" id="Seg_6307" s="T344">PKZ_196X_SU0215.070 (073)</ta>
            <ta e="T350" id="Seg_6308" s="T346">PKZ_196X_SU0215.071 (074)</ta>
            <ta e="T357" id="Seg_6309" s="T350">PKZ_196X_SU0215.072 (075)</ta>
            <ta e="T360" id="Seg_6310" s="T357">PKZ_196X_SU0215.073 (076)</ta>
            <ta e="T371" id="Seg_6311" s="T360">PKZ_196X_SU0215.074 (077)</ta>
            <ta e="T375" id="Seg_6312" s="T371">PKZ_196X_SU0215.075 (078)</ta>
            <ta e="T385" id="Seg_6313" s="T375">PKZ_196X_SU0215.076 (079)</ta>
            <ta e="T393" id="Seg_6314" s="T385">PKZ_196X_SU0215.077 (080)</ta>
            <ta e="T398" id="Seg_6315" s="T393">PKZ_196X_SU0215.078 (081)</ta>
            <ta e="T406" id="Seg_6316" s="T398">PKZ_196X_SU0215.079 (082)</ta>
            <ta e="T416" id="Seg_6317" s="T406">PKZ_196X_SU0215.080 (083)</ta>
            <ta e="T422" id="Seg_6318" s="T416">PKZ_196X_SU0215.081 (084)</ta>
            <ta e="T429" id="Seg_6319" s="T422">PKZ_196X_SU0215.082 (085)</ta>
            <ta e="T436" id="Seg_6320" s="T429">PKZ_196X_SU0215.083 (086)</ta>
            <ta e="T447" id="Seg_6321" s="T436">PKZ_196X_SU0215.084 (087)</ta>
            <ta e="T458" id="Seg_6322" s="T447">PKZ_196X_SU0215.085 (088)</ta>
            <ta e="T461" id="Seg_6323" s="T458">PKZ_196X_SU0215.086 (089)</ta>
            <ta e="T470" id="Seg_6324" s="T461">PKZ_196X_SU0215.087 (090)</ta>
            <ta e="T475" id="Seg_6325" s="T470">PKZ_196X_SU0215.088 (091)</ta>
            <ta e="T481" id="Seg_6326" s="T475">PKZ_196X_SU0215.089 (092)</ta>
            <ta e="T487" id="Seg_6327" s="T481">PKZ_196X_SU0215.090 (093)</ta>
            <ta e="T497" id="Seg_6328" s="T487">PKZ_196X_SU0215.091 (094)</ta>
            <ta e="T501" id="Seg_6329" s="T497">PKZ_196X_SU0215.092 (095)</ta>
            <ta e="T508" id="Seg_6330" s="T501">PKZ_196X_SU0215.093 (096)</ta>
            <ta e="T514" id="Seg_6331" s="T508">PKZ_196X_SU0215.094 (097)</ta>
            <ta e="T519" id="Seg_6332" s="T514">PKZ_196X_SU0215.095 (098)</ta>
            <ta e="T524" id="Seg_6333" s="T519">PKZ_196X_SU0215.096 (099)</ta>
            <ta e="T527" id="Seg_6334" s="T524">PKZ_196X_SU0215.097 (100)</ta>
            <ta e="T529" id="Seg_6335" s="T527">PKZ_196X_SU0215.098 (101)</ta>
            <ta e="T531" id="Seg_6336" s="T529">PKZ_196X_SU0215.099 (102)</ta>
            <ta e="T536" id="Seg_6337" s="T531">PKZ_196X_SU0215.100 (103)</ta>
            <ta e="T539" id="Seg_6338" s="T536">PKZ_196X_SU0215.101 (104)</ta>
            <ta e="T543" id="Seg_6339" s="T539">PKZ_196X_SU0215.102 (105)</ta>
            <ta e="T551" id="Seg_6340" s="T543">PKZ_196X_SU0215.103 (106)</ta>
            <ta e="T559" id="Seg_6341" s="T551">PKZ_196X_SU0215.104 (107)</ta>
            <ta e="T560" id="Seg_6342" s="T559">PKZ_196X_SU0215.105 (108)</ta>
            <ta e="T567" id="Seg_6343" s="T560">PKZ_196X_SU0215.106 (109)</ta>
            <ta e="T571" id="Seg_6344" s="T567">PKZ_196X_SU0215.107 (110)</ta>
            <ta e="T574" id="Seg_6345" s="T571">PKZ_196X_SU0215.108 (111)</ta>
            <ta e="T578" id="Seg_6346" s="T574">PKZ_196X_SU0215.109 (112)</ta>
            <ta e="T582" id="Seg_6347" s="T578">PKZ_196X_SU0215.110 (113)</ta>
            <ta e="T587" id="Seg_6348" s="T582">PKZ_196X_SU0215.111 (114)</ta>
            <ta e="T591" id="Seg_6349" s="T587">PKZ_196X_SU0215.112 (115)</ta>
            <ta e="T595" id="Seg_6350" s="T591">PKZ_196X_SU0215.113 (116)</ta>
            <ta e="T597" id="Seg_6351" s="T595">PKZ_196X_SU0215.114 (117)</ta>
            <ta e="T610" id="Seg_6352" s="T597">PKZ_196X_SU0215.115 (118)</ta>
            <ta e="T619" id="Seg_6353" s="T610">PKZ_196X_SU0215.116 (119)</ta>
            <ta e="T632" id="Seg_6354" s="T619">PKZ_196X_SU0215.117 (120)</ta>
            <ta e="T653" id="Seg_6355" s="T632">PKZ_196X_SU0215.118 (121) </ta>
            <ta e="T657" id="Seg_6356" s="T653">PKZ_196X_SU0215.119 (123)</ta>
            <ta e="T660" id="Seg_6357" s="T657">PKZ_196X_SU0215.120 (124)</ta>
            <ta e="T664" id="Seg_6358" s="T660">PKZ_196X_SU0215.121 (125)</ta>
            <ta e="T671" id="Seg_6359" s="T664">PKZ_196X_SU0215.122 (126)</ta>
            <ta e="T678" id="Seg_6360" s="T671">PKZ_196X_SU0215.123 (127)</ta>
            <ta e="T683" id="Seg_6361" s="T678">PKZ_196X_SU0215.124 (128)</ta>
            <ta e="T692" id="Seg_6362" s="T683">PKZ_196X_SU0215.125 (129)</ta>
            <ta e="T697" id="Seg_6363" s="T692">PKZ_196X_SU0215.126 (130)</ta>
            <ta e="T699" id="Seg_6364" s="T697">PKZ_196X_SU0215.127 (131)</ta>
            <ta e="T703" id="Seg_6365" s="T699">PKZ_196X_SU0215.128 (132)</ta>
            <ta e="T707" id="Seg_6366" s="T703">PKZ_196X_SU0215.129 (133)</ta>
            <ta e="T714" id="Seg_6367" s="T707">PKZ_196X_SU0215.130 (134)</ta>
            <ta e="T724" id="Seg_6368" s="T714">PKZ_196X_SU0215.131 (135)</ta>
            <ta e="T729" id="Seg_6369" s="T724">PKZ_196X_SU0215.132 (136)</ta>
            <ta e="T733" id="Seg_6370" s="T729">PKZ_196X_SU0215.133 (137)</ta>
            <ta e="T741" id="Seg_6371" s="T733">PKZ_196X_SU0215.134 (138)</ta>
            <ta e="T745" id="Seg_6372" s="T741">PKZ_196X_SU0215.135 (139)</ta>
            <ta e="T751" id="Seg_6373" s="T745">PKZ_196X_SU0215.136 (140)</ta>
            <ta e="T757" id="Seg_6374" s="T751">PKZ_196X_SU0215.137 (141)</ta>
            <ta e="T763" id="Seg_6375" s="T757">PKZ_196X_SU0215.138 (142)</ta>
            <ta e="T770" id="Seg_6376" s="T763">PKZ_196X_SU0215.139 (143)</ta>
            <ta e="T779" id="Seg_6377" s="T770">PKZ_196X_SU0215.140 (144)</ta>
            <ta e="T784" id="Seg_6378" s="T779">PKZ_196X_SU0215.141 (145)</ta>
            <ta e="T792" id="Seg_6379" s="T784">PKZ_196X_SU0215.142 (146)</ta>
            <ta e="T801" id="Seg_6380" s="T792">PKZ_196X_SU0215.143 (147)</ta>
            <ta e="T807" id="Seg_6381" s="T801">PKZ_196X_SU0215.144 (148)</ta>
            <ta e="T810" id="Seg_6382" s="T807">PKZ_196X_SU0215.145 (149)</ta>
            <ta e="T820" id="Seg_6383" s="T810">PKZ_196X_SU0215.146 (150)</ta>
            <ta e="T823" id="Seg_6384" s="T820">PKZ_196X_SU0215.147 (151)</ta>
            <ta e="T830" id="Seg_6385" s="T823">PKZ_196X_SU0215.148 (152)</ta>
            <ta e="T838" id="Seg_6386" s="T830">PKZ_196X_SU0215.149 (153)</ta>
            <ta e="T844" id="Seg_6387" s="T838">PKZ_196X_SU0215.150 (154)</ta>
            <ta e="T849" id="Seg_6388" s="T844">PKZ_196X_SU0215.151 (155)</ta>
            <ta e="T853" id="Seg_6389" s="T849">PKZ_196X_SU0215.152 (156)</ta>
            <ta e="T864" id="Seg_6390" s="T853">PKZ_196X_SU0215.153 (157)</ta>
            <ta e="T870" id="Seg_6391" s="T864">PKZ_196X_SU0215.154 (158)</ta>
            <ta e="T874" id="Seg_6392" s="T870">PKZ_196X_SU0215.155 (159)</ta>
            <ta e="T876" id="Seg_6393" s="T874">PKZ_196X_SU0215.156 (160)</ta>
            <ta e="T879" id="Seg_6394" s="T876">PKZ_196X_SU0215.157 (161)</ta>
            <ta e="T881" id="Seg_6395" s="T879">PKZ_196X_SU0215.158 (162)</ta>
            <ta e="T886" id="Seg_6396" s="T881">PKZ_196X_SU0215.159 (163)</ta>
            <ta e="T890" id="Seg_6397" s="T886">PKZ_196X_SU0215.160 (164)</ta>
            <ta e="T899" id="Seg_6398" s="T890">PKZ_196X_SU0215.161 (165)</ta>
            <ta e="T905" id="Seg_6399" s="T899">PKZ_196X_SU0215.162 (166)</ta>
            <ta e="T914" id="Seg_6400" s="T905">PKZ_196X_SU0215.163 (167)</ta>
            <ta e="T925" id="Seg_6401" s="T914">PKZ_196X_SU0215.164 (168)</ta>
            <ta e="T936" id="Seg_6402" s="T925">PKZ_196X_SU0215.165 (169)</ta>
            <ta e="T939" id="Seg_6403" s="T936">PKZ_196X_SU0215.166 (170)</ta>
            <ta e="T950" id="Seg_6404" s="T939">PKZ_196X_SU0215.167 (171)</ta>
            <ta e="T954" id="Seg_6405" s="T950">PKZ_196X_SU0215.168 (172)</ta>
            <ta e="T957" id="Seg_6406" s="T954">PKZ_196X_SU0215.169 (173)</ta>
            <ta e="T965" id="Seg_6407" s="T957">PKZ_196X_SU0215.170 (174)</ta>
            <ta e="T974" id="Seg_6408" s="T965">PKZ_196X_SU0215.171 (175)</ta>
            <ta e="T980" id="Seg_6409" s="T974">PKZ_196X_SU0215.172 (176)</ta>
            <ta e="T983" id="Seg_6410" s="T980">PKZ_196X_SU0215.173 (177)</ta>
            <ta e="T988" id="Seg_6411" s="T983">PKZ_196X_SU0215.174 (178)</ta>
            <ta e="T996" id="Seg_6412" s="T988">PKZ_196X_SU0215.175 (179)</ta>
            <ta e="T1002" id="Seg_6413" s="T996">PKZ_196X_SU0215.176 (180)</ta>
            <ta e="T1005" id="Seg_6414" s="T1002">PKZ_196X_SU0215.177 (181)</ta>
            <ta e="T1007" id="Seg_6415" s="T1005">PKZ_196X_SU0215.178 (182)</ta>
            <ta e="T1010" id="Seg_6416" s="T1007">PKZ_196X_SU0215.179 (183)</ta>
            <ta e="T1012" id="Seg_6417" s="T1010">PKZ_196X_SU0215.180 (184)</ta>
            <ta e="T1014" id="Seg_6418" s="T1012">PKZ_196X_SU0215.181 (185)</ta>
            <ta e="T1018" id="Seg_6419" s="T1014">PKZ_196X_SU0215.182 (186)</ta>
            <ta e="T1019" id="Seg_6420" s="T1018">PKZ_196X_SU0215.183 (187)</ta>
            <ta e="T1027" id="Seg_6421" s="T1019">PKZ_196X_SU0215.184 (188)</ta>
            <ta e="T1033" id="Seg_6422" s="T1027">PKZ_196X_SU0215.185 (189)</ta>
            <ta e="T1035" id="Seg_6423" s="T1033">PKZ_196X_SU0215.186 (190)</ta>
            <ta e="T1041" id="Seg_6424" s="T1035">PKZ_196X_SU0215.187 (191)</ta>
            <ta e="T1046" id="Seg_6425" s="T1041">PKZ_196X_SU0215.188 (192)</ta>
            <ta e="T1048" id="Seg_6426" s="T1046">PKZ_196X_SU0215.189 (193)</ta>
            <ta e="T1052" id="Seg_6427" s="T1048">PKZ_196X_SU0215.190 (194)</ta>
            <ta e="T1054" id="Seg_6428" s="T1052">PKZ_196X_SU0215.191 (195)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_6429" s="T0">"Tăn (taldʼen) (ka-) kajaʔ ibiel dăk, edəbiel?" </ta>
            <ta e="T9" id="Seg_6430" s="T7">(Edəmiel-) Edəbiem. </ta>
            <ta e="T11" id="Seg_6431" s="T9">A kumən? </ta>
            <ta e="T13" id="Seg_6432" s="T11">Sumna gram. </ta>
            <ta e="T15" id="Seg_6433" s="T13">Aktʼa deʔpiel? </ta>
            <ta e="T16" id="Seg_6434" s="T15">"Deʔpiem. </ta>
            <ta e="T20" id="Seg_6435" s="T16">(Šide šăl-) Šide šălkovej". </ta>
            <ta e="T23" id="Seg_6436" s="T20">Dʼăbaktərlaʔbəbaʔ i kaknarlaʔbəbaʔ. </ta>
            <ta e="T27" id="Seg_6437" s="T23">I (arla-) ara bĭtleʔbəbeʔ. </ta>
            <ta e="T30" id="Seg_6438" s="T27">I ipek amnia. </ta>
            <ta e="T33" id="Seg_6439" s="T30">I uja amnaʔbəbaʔ. </ta>
            <ta e="T37" id="Seg_6440" s="T33">I taŋgu nʼeʔleʔbəbeʔ bar. </ta>
            <ta e="T45" id="Seg_6441" s="T37">Miʔ tondə kuza amnolaʔbə, dĭn nʼi miʔnʼibeʔ šobi. </ta>
            <ta e="T50" id="Seg_6442" s="T45">Jašɨktə (păʔpi), i škaptə (păʔpi). </ta>
            <ta e="T55" id="Seg_6443" s="T50">Măn (mămbiam=) mămbiam:" Ĭmbi ((…))?" </ta>
            <ta e="T59" id="Seg_6444" s="T55">"Măna Vanʼka велел măndərzittə". </ta>
            <ta e="T66" id="Seg_6445" s="T59">Măn dĭm sürerluʔpiem: "(Măllia Mišanə), kanaʔ döʔə!" </ta>
            <ta e="T69" id="Seg_6446" s="T66">Dĭgəttə teinen šobi. </ta>
            <ta e="T78" id="Seg_6447" s="T69">Bura sanəzi nuga, dĭ dagaj ibi i bura băʔpi. </ta>
            <ta e="T83" id="Seg_6448" s="T78">Sanə ibi bostə (barəʔluʔpi) sĭreʔpne. </ta>
            <ta e="T89" id="Seg_6449" s="T83">Miʔ turagən (ne nuʔməluʔpi) (deš-) (dĭŋinaʔtə). </ta>
            <ta e="T91" id="Seg_6450" s="T89">Tojirbi sanə. </ta>
            <ta e="T97" id="Seg_6451" s="T91">A dĭ bar pʼerluʔbə: Dö sanə. </ta>
            <ta e="T103" id="Seg_6452" s="T97">A dĭ mămbi: "Măn dĭm kutlim!" </ta>
            <ta e="T108" id="Seg_6453" s="T103">"A (ĭmbi) ej (mămbileʔ) раньше?" </ta>
            <ta e="T118" id="Seg_6454" s="T108">Taldʼen măna kăštəbiʔi (m-) măn (tugan-) tugandə: "Šoʔ miʔnʼibeʔ amorzittə. </ta>
            <ta e="T124" id="Seg_6455" s="T118">(Eššinʼ) Ešši (ti-) külambi, (ušonʼiʔkə) kambi". </ta>
            <ta e="T128" id="Seg_6456" s="T124">A măn mămbiam:" Šolam". </ta>
            <ta e="T134" id="Seg_6457" s="T128">(A bos-) A bospə ej kambiam. </ta>
            <ta e="T144" id="Seg_6458" s="T134">(Meim) kăštəbiam, kăštəbiam, dĭ ej kambi, i (măn) ej kambiam. </ta>
            <ta e="T152" id="Seg_6459" s="T144">Dĭn ugandə araj, a măna ara ej kereʔ. </ta>
            <ta e="T157" id="Seg_6460" s="T152">Măn не хочу dĭm bĭʔsittə. </ta>
            <ta e="T166" id="Seg_6461" s="T157">A dĭ kuroluʔpi bar, măndə: urgajam ažnə (dʼoːdunʼi) kubiam. </ta>
            <ta e="T171" id="Seg_6462" s="T166">Tüj büzəjleʔ bar tüšəlbi šiʔnʼileʔ. </ta>
            <ta e="T176" id="Seg_6463" s="T171">Iʔbəleʔbə, amnoʔ, iʔbəleʔbə, ej kirgarlaʔbə. </ta>
            <ta e="T179" id="Seg_6464" s="T176">Teinen ejü molaːmbi. </ta>
            <ta e="T183" id="Seg_6465" s="T179">Sĭre bar (nömrunʼiʔ) ibi. </ta>
            <ta e="T188" id="Seg_6466" s="T183">Sĭre (buž-) büžü bü molalləj. </ta>
            <ta e="T191" id="Seg_6467" s="T188">Dĭgəttə noʔ özerləj. </ta>
            <ta e="T194" id="Seg_6468" s="T191">Всякий svʼetogəʔi özerləʔi. </ta>
            <ta e="T198" id="Seg_6469" s="T194">Paʔi bar kuvas moləʔi. </ta>
            <ta e="T201" id="Seg_6470" s="T198">Zʼelʼonaʔi moləʔi bar. </ta>
            <ta e="T208" id="Seg_6471" s="T201">Dĭgəttə ineʔi, tüžöjəʔi, ulardə kalaʔi noʔ (amzittə). </ta>
            <ta e="T212" id="Seg_6472" s="T208">Dĭgəttə noʔ urgo özerləj. </ta>
            <ta e="T218" id="Seg_6473" s="T212">Dĭgəttə dĭm (šapkosi-) šapkozi jaʔsittə nada. </ta>
            <ta e="T224" id="Seg_6474" s="T218">Dĭgəttə kujagən koləj, da oʔbdəsʼtə nada. </ta>
            <ta e="T228" id="Seg_6475" s="T224">Il (mĭnliaʔi) ugandə jakše. </ta>
            <ta e="T230" id="Seg_6476" s="T228">Pʼe šobi. </ta>
            <ta e="T233" id="Seg_6477" s="T230">Ugandə (ej-) ejü. </ta>
            <ta e="T237" id="Seg_6478" s="T233">Măn (dönüli mobiam) kunolzittə. </ta>
            <ta e="T240" id="Seg_6479" s="T237">Dĭgəttə ertə uʔbdəbiam. </ta>
            <ta e="T245" id="Seg_6480" s="T240">A (dʼala=) dʼalam bar togonorbiam. </ta>
            <ta e="T252" id="Seg_6481" s="T245">Tüžöj surdobiam da, mĭnzərbiem, abiam, dĭgəttə dʼăbaktərbiam. </ta>
            <ta e="T257" id="Seg_6482" s="T252">Ertə oʔbdəbiam da šü nendəbiem. </ta>
            <ta e="T266" id="Seg_6483" s="T257">Măna Kazan turanə nada kanzittə, (măna) aktʼa iʔbolaʔbə bankan. </ta>
            <ta e="T274" id="Seg_6484" s="T266">Izittə nada, da ildə mĭzittə, măn (dĭzeŋgən) ibiem. </ta>
            <ta e="T281" id="Seg_6485" s="T274">Măn iʔgö dʼăbaktəriam, (tu-) tüj (dʼăbaktər-) dʼăbaktərlaʔbəʔjə. </ta>
            <ta e="T284" id="Seg_6486" s="T281">Ugandə tăn koloʔsəbi. </ta>
            <ta e="T287" id="Seg_6487" s="T284">(Koʔbdo) üge măna. </ta>
            <ta e="T290" id="Seg_6488" s="T287">Măna nada dʼăbaktərzittə. </ta>
            <ta e="T297" id="Seg_6489" s="T290">Tüj tăn dʼăbaktəraʔ, a măn ej dʼăbaktərlam. </ta>
            <ta e="T305" id="Seg_6490" s="T297">(Măj-) Măn ej tĭmniem, ĭmbi dʼăbaktərzittə, tăn dʼăbaktəraʔ. </ta>
            <ta e="T317" id="Seg_6491" s="T305">(A m-) A măn ĭmbi üge буду (dʼăbaktərzittə), (iʔbəle- iʔbola-) iʔbəlem da. </ta>
            <ta e="T325" id="Seg_6492" s="T317">Măn turam с краю, măn ĭmbidə ej tĭmniem. </ta>
            <ta e="T327" id="Seg_6493" s="T325">Tenöʔ ĭmbi-nʼibudʼ. </ta>
            <ta e="T329" id="Seg_6494" s="T327">Šamaʔ ĭmbi-nʼibudʼ. </ta>
            <ta e="T332" id="Seg_6495" s="T329">Abam (kuŋgəm) kalla dʼürbi. </ta>
            <ta e="T342" id="Seg_6496" s="T332">A măna ugandə jakše, šindidə ej münöria, măn (balʔraʔ) alomniam. </ta>
            <ta e="T344" id="Seg_6497" s="T342">(Ajnu) nüjleʔbəm. </ta>
            <ta e="T346" id="Seg_6498" s="T344">Suʔmileʔbəm bar. </ta>
            <ta e="T350" id="Seg_6499" s="T346">A iam ej nʼilgöliem. </ta>
            <ta e="T357" id="Seg_6500" s="T350">Iam abam dĭ saʔpiaŋbi, što măn ((PAUSE)). </ta>
            <ta e="T360" id="Seg_6501" s="T357">Što măn alomniam. </ta>
            <ta e="T371" id="Seg_6502" s="T360">Măn elem, teinen nüdʼin bar (ara bĭʔ-), (dʼabr-) nüjnə (nüjnə-) nüjnəbi. </ta>
            <ta e="T375" id="Seg_6503" s="T371">Dʼabərobi bar, (kudonzəbi) bar. </ta>
            <ta e="T385" id="Seg_6504" s="T375">Jelʼa unnʼa amnolaʔbə, üge monoʔkolaʔbəʔjə, a dĭ üge ej kalia. </ta>
            <ta e="T393" id="Seg_6505" s="T385">Šindinədə ej kalia, ни за кого не идет. </ta>
            <ta e="T398" id="Seg_6506" s="T393">Dĭn bar iat abat ige. </ta>
            <ta e="T406" id="Seg_6507" s="T398">Măliaʔi:" Kanaʔ (tibinə)", a dĭ kădedə ej kalia. </ta>
            <ta e="T416" id="Seg_6508" s="T406">Šobi măna monoʔkosʼtə ej kuvas kuza, măna nʼe axota kanzittə. </ta>
            <ta e="T422" id="Seg_6509" s="T416">Abam măndə:" Ĭmbi tăn ej kalal? </ta>
            <ta e="T429" id="Seg_6510" s="T422">(Ej) Nu iʔ (kanaʔ)", (măn nuʔməluʔpiam) dĭgəttə. </ta>
            <ta e="T436" id="Seg_6511" s="T429">Măn (pervam) первый tibim šobi măna monoʔkosʼtə. </ta>
            <ta e="T447" id="Seg_6512" s="T436">Dĭʔnə (мало pʼe i măna=) amga pʼe i măna amga pʼe. </ta>
            <ta e="T458" id="Seg_6513" s="T447">(Măn nuʔ) Măn măndəm:" Kanaʔ abanə da ianə, monoʔkoʔ dĭzeŋ mĭləʔi?" </ta>
            <ta e="T461" id="Seg_6514" s="T458">Dʼok, ej mĭləʔi. </ta>
            <ta e="T470" id="Seg_6515" s="T461">Dĭgəttə măna dʼăbaktərluʔpiʔi i (kam-) kambibaʔ (ine-) inetsi dĭʔnə. </ta>
            <ta e="T475" id="Seg_6516" s="T470">Dĭgəttə iam šobi dibər miʔnʼibeʔ. </ta>
            <ta e="T481" id="Seg_6517" s="T475">Dĭ (š-) döbər šobi noʔ jaʔsittə. </ta>
            <ta e="T487" id="Seg_6518" s="T481">Dĭgəttə kambi bünə da dĭn külambi. </ta>
            <ta e="T497" id="Seg_6519" s="T487">Dĭgəttə măn maːndə šobiam i ugandə tăŋ tenöbiam, sĭjbə ĭzembie. </ta>
            <ta e="T501" id="Seg_6520" s="T497">Dʼorbiam, ajirbiom dĭm bar. </ta>
            <ta e="T508" id="Seg_6521" s="T501">Dĭgəttə iam măna bü dʼazirbi, (pĭj) măna. </ta>
            <ta e="T514" id="Seg_6522" s="T508">Dĭgəttə măn davaj nüjnə (nüjzit-) nüjzittə. </ta>
            <ta e="T519" id="Seg_6523" s="T514">Dĭgəttə măn šobiam (maʔnʼi), amnobiam. </ta>
            <ta e="T524" id="Seg_6524" s="T519">Dĭgəttə kambiam (Permʼako-) Permʼakovtə, večorkanə. </ta>
            <ta e="T527" id="Seg_6525" s="T524">(Ten) sʼarbibaʔ, suʔmibeʔ. </ta>
            <ta e="T529" id="Seg_6526" s="T527">Garmonʼnʼə sʼarbibaʔ. </ta>
            <ta e="T531" id="Seg_6527" s="T529">Nüjnə nüjnəbibeʔ. </ta>
            <ta e="T536" id="Seg_6528" s="T531">Dĭgəttə dĭn tibi šobi, (Nagornaj). </ta>
            <ta e="T539" id="Seg_6529" s="T536">Muʔzendə urgo, numəʔi. </ta>
            <ta e="T543" id="Seg_6530" s="T539">Da köməʔi, bostə kömə. </ta>
            <ta e="T551" id="Seg_6531" s="T543">Dĭgəttə măn maʔnʼi šobiam da šobi măna monoʔkosʼtə. </ta>
            <ta e="T559" id="Seg_6532" s="T551">Măn măndəm:" Tăn (nʼi git) (kək-) (kak măn). </ta>
            <ta e="T560" id="Seg_6533" s="T559">Dĭrgit. </ta>
            <ta e="T567" id="Seg_6534" s="T560">Nu măn tănzi ej amnolam, a nʼizi. </ta>
            <ta e="T571" id="Seg_6535" s="T567">Dĭgəttə iam măndə:" Kanaʔ. </ta>
            <ta e="T574" id="Seg_6536" s="T571">Dĭn aktʼa iʔgö. </ta>
            <ta e="T578" id="Seg_6537" s="T574">Ugandə (k-) jakše kuza". </ta>
            <ta e="T582" id="Seg_6538" s="T578">A măn (nuliam):" Ej". </ta>
            <ta e="T587" id="Seg_6539" s="T582">"Da kanaʔ, da amnoʔ dĭzi". </ta>
            <ta e="T591" id="Seg_6540" s="T587">Onʼiʔ kuza šobi monoʔkosʼtə. </ta>
            <ta e="T595" id="Seg_6541" s="T591">A măn ej kaliam. </ta>
            <ta e="T597" id="Seg_6542" s="T595">(A ia=.) </ta>
            <ta e="T610" id="Seg_6543" s="T597">Ej kaliam, dĭ (ej) kuvas, (üjü=) üjüt bar (ponʼ-) păjdʼaŋ, (bost-) muʔzendə kömə. </ta>
            <ta e="T619" id="Seg_6544" s="T610">Šĭket ej dʼăbaktəria, ej jakše dʼăbaktəria, kuzaŋdə ej (nʼilgöliaʔjə). </ta>
            <ta e="T632" id="Seg_6545" s="T619">A iam măndə:" Kanaʔ, dĭn tüžöj ige, i ine ige, i ipek iʔgö". </ta>
            <ta e="T653" id="Seg_6546" s="T632">A (măn=) măn mămbiam: "Măna inezi ej amnosʼtə, i tüžöjdə ej amnosʼtə, (nada tănan dăk=) (tăn) tănan kereʔ, dak tăn kanaʔ". </ta>
            <ta e="T657" id="Seg_6547" s="T653">Tanitsagən šobi onʼiʔ tibi. </ta>
            <ta e="T660" id="Seg_6548" s="T657">Dĭm ne barəʔluʔpi. </ta>
            <ta e="T664" id="Seg_6549" s="T660">Dĭ davaj măna monoʔkosʼtə. </ta>
            <ta e="T671" id="Seg_6550" s="T664">Iam stal (mĭnzerzittə) uja, dĭzem (bĭd-) bădəsʼtə. </ta>
            <ta e="T678" id="Seg_6551" s="T671">A măn mălliam: kanzittə (ilʼi) ej kanzittə? </ta>
            <ta e="T683" id="Seg_6552" s="T678">"Хошь kanaʔ, хошь iʔ kanaʔ. </ta>
            <ta e="T692" id="Seg_6553" s="T683">Tăn tüjö (s-) (bostə urgo=) bostə tĭmnel ĭmbi (azittə)". </ta>
            <ta e="T697" id="Seg_6554" s="T692">Dĭgəttə dĭ tibi (sʼ-) šobi. </ta>
            <ta e="T699" id="Seg_6555" s="T697">Măna amnobi. </ta>
            <ta e="T703" id="Seg_6556" s="T699">I davaj monoʔkosʼtə bostə. </ta>
            <ta e="T707" id="Seg_6557" s="T703">I davaj kudajdə numan üzəsʼtə. </ta>
            <ta e="T714" id="Seg_6558" s="T707">"Ej baʔlim tănan, a munolam jakše amnolubaʔ". </ta>
            <ta e="T724" id="Seg_6559" s="T714">Dĭgəttə dĭ ine (körerbi), (m- miʔ=) măn dĭzi kambiam Permʼakovtə. </ta>
            <ta e="T729" id="Seg_6560" s="T724">Ara ibiem, deʔpibeʔ (s- )… </ta>
            <ta e="T733" id="Seg_6561" s="T729">Stoldə oʔbdəbibeʔ, il oʔbdəbibeʔ. </ta>
            <ta e="T741" id="Seg_6562" s="T733">Ara bĭʔpiʔi, ambiʔi, dĭgəttə miʔ (ka-) kambibaʔ stanitsanə. </ta>
            <ta e="T745" id="Seg_6563" s="T741">Dĭgəttə (manobiam) nagur kö. </ta>
            <ta e="T751" id="Seg_6564" s="T745">Dĭgəttə văjna ibi, dĭm ibiʔi văjnanə. </ta>
            <ta e="T757" id="Seg_6565" s="T751">Dĭgəttə măn nanəʔzəbi ibiem, (koʔbdo dĭ). </ta>
            <ta e="T763" id="Seg_6566" s="T757">Dĭgəttə dĭ šobi văjnagə, раненый ibi. </ta>
            <ta e="T770" id="Seg_6567" s="T763">Dĭgəttə iššo amnobibeʔ, măn bazo nanəʔzəbi ibi. </ta>
            <ta e="T779" id="Seg_6568" s="T770">Dĭgəttə dĭ (n-DMG-nobi), dĭ măna sürerbi i dĭm ibi. </ta>
            <ta e="T784" id="Seg_6569" s="T779">Iššo onʼiʔ (ni-) nʼi deʔpiem. </ta>
            <ta e="T792" id="Seg_6570" s="T784">Dĭgəttə maʔnʼi šobiam, (aba- aban-) abanə i ianə. </ta>
            <ta e="T801" id="Seg_6571" s="T792">Dĭgəttə külaːmbiʔi, koʔbdo i nʼi, (măn) unnʼa (mola-) molambiam. </ta>
            <ta e="T807" id="Seg_6572" s="T801">Oʔkʼe amnobiam, bazo kuza măna monoʔkobi. </ta>
            <ta e="T810" id="Seg_6573" s="T807">Măn kambiam dĭzi. </ta>
            <ta e="T820" id="Seg_6574" s="T810">Amnobiam, oʔb, šide, nagur, teʔtə, sumna, muktuʔ, sejʔpü kö amnobiam. </ta>
            <ta e="T823" id="Seg_6575" s="T820">Šide ešši ibiʔi. </ta>
            <ta e="T830" id="Seg_6576" s="T823">(Ugandə) ej jakše, ara bĭʔpi, (dʼabrolaʔpi), kudonzlaʔpi. </ta>
            <ta e="T838" id="Seg_6577" s="T830">(Kamen) Kamen baʔluʔpiam, (bar ulum=) bar eʔbdəm sajnʼeʔpi. </ta>
            <ta e="T844" id="Seg_6578" s="T838">(Vekžen) tăŋ münörbi, ažnə kem ibi. </ta>
            <ta e="T849" id="Seg_6579" s="T844">Dĭgəttə măn (nʼim=) dĭm barəʔluʔpiam. </ta>
            <ta e="T853" id="Seg_6580" s="T849">Măn onʼiʔ nʼim ibi. </ta>
            <ta e="T864" id="Seg_6581" s="T853">Tüj măndəm šindinədə: ej ((DMG)) tibinə, unnʼa amnolam, nʼizi, bostə nʼizi. </ta>
            <ta e="T870" id="Seg_6582" s="T864">Dĭgəttə onʼiʔ nʼi davaj măna monoʔkosʼtə. </ta>
            <ta e="T874" id="Seg_6583" s="T870">Măn не хотел идти. </ta>
            <ta e="T876" id="Seg_6584" s="T874">Ej kuvandoliam. </ta>
            <ta e="T879" id="Seg_6585" s="T876">(Kadəlbə) Kadəldə nʼišpək. </ta>
            <ta e="T881" id="Seg_6586" s="T879">"Ej kalam". </ta>
            <ta e="T886" id="Seg_6587" s="T881">(Dĭ) măndə:" Davaj amnosʼtə tănzi." </ta>
            <ta e="T890" id="Seg_6588" s="T886">Nu, tenöbiam, tenöbiam, dĭgəttə kambiam. </ta>
            <ta e="T899" id="Seg_6589" s="T890">(Dĭgəttə) bazo: "(Ba-) barəʔlim, măndəm, tănan, ej amnolam tănzi". </ta>
            <ta e="T905" id="Seg_6590" s="T899">Dibər davaj dʼorzittə, tăŋ dʼorbi, ugandə. </ta>
            <ta e="T914" id="Seg_6591" s="T905">Dĭgəttə măn mălliam: "((…))tura nuldəbəj, dĭgəttə pušaj kaləj". </ta>
            <ta e="T925" id="Seg_6592" s="T914">I dăre ibi: tura (tol-) nuldəbi i kalla dʼürbi, măn unnʼa maːluʔpiam. </ta>
            <ta e="T936" id="Seg_6593" s="T925">Tüj măn üge unnʼa amnolaʔbəm, (amnolaʔbəm), už šide bʼeʔ ki amnolaʔbəm. </ta>
            <ta e="T939" id="Seg_6594" s="T936">Kamən dĭ barəʔluʔpi. </ta>
            <ta e="T950" id="Seg_6595" s="T939">Dĭgəttə miʔ dĭ nʼizi bar pa baltuzi jaʔpibaʔ, tura (nulzittə-) nuldəzittə. </ta>
            <ta e="T954" id="Seg_6596" s="T950">(Tazi-) Tažerbibaʔ (ine-) inezi. </ta>
            <ta e="T957" id="Seg_6597" s="T954">I tura (nuldəbibaʔ). </ta>
            <ta e="T965" id="Seg_6598" s="T957">Tibizeŋ jaʔpiʔi baltuzi, a măn kubaʔi dĭʔnə abiam. </ta>
            <ta e="T974" id="Seg_6599" s="T965">Dĭgəttə tsutonkaʔi (taz-) tažerbiʔi, пол azittə (i потолок) azittə. </ta>
            <ta e="T980" id="Seg_6600" s="T974">A măn (kuda) urgo kubaʔi abiam. </ta>
            <ta e="T983" id="Seg_6601" s="T980">I jamaʔi šöʔpiem. </ta>
            <ta e="T988" id="Seg_6602" s="T983">A dĭ tože măna (kabaržəbi). </ta>
            <ta e="T996" id="Seg_6603" s="T988">Kubat edəmbi da ej tĭmnebi, măn dĭm tüšəlbiem. </ta>
            <ta e="T1002" id="Seg_6604" s="T996">Măn bostə (tibin) tibinə jamaʔi šöʔpiem. </ta>
            <ta e="T1005" id="Seg_6605" s="T1002">I parga šöʔpiem. </ta>
            <ta e="T1007" id="Seg_6606" s="T1005">Ularən kubaʔi. </ta>
            <ta e="T1010" id="Seg_6607" s="T1007">I üžü šöʔpiem. </ta>
            <ta e="T1012" id="Seg_6608" s="T1010">Kujnektə (šöʔpiem). </ta>
            <ta e="T1014" id="Seg_6609" s="T1012">Piʔmeʔi šöʔpiem. </ta>
            <ta e="T1018" id="Seg_6610" s="T1014">Dĭʔnə šerbiam ugandə (jakše) ((BRK)). </ta>
            <ta e="T1019" id="Seg_6611" s="T1018">Šerbiem. </ta>
            <ta e="T1027" id="Seg_6612" s="T1019">(I-) Il bar măndərbiʔi: ugandə kuvas kuza molambi. </ta>
            <ta e="T1033" id="Seg_6613" s="T1027">Dĭgəttə miʔ dʼü (tarir- tajir-) (tarirlaʔpibaʔ). </ta>
            <ta e="T1035" id="Seg_6614" s="T1033">Aš kuʔpibaʔ. </ta>
            <ta e="T1041" id="Seg_6615" s="T1035">(Ez-) Özerbi, ugandə jakše ibi aš. </ta>
            <ta e="T1046" id="Seg_6616" s="T1041">Dĭgəttə un abibaʔ, tʼerməndə mĭmbibeʔ. </ta>
            <ta e="T1048" id="Seg_6617" s="T1046">Ipek pürbibeʔ. </ta>
            <ta e="T1052" id="Seg_6618" s="T1048">I budəj aš kuʔpibaʔ. </ta>
            <ta e="T1054" id="Seg_6619" s="T1052">(Jakšə) ((DMG)). </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_6620" s="T0">tăn</ta>
            <ta e="T2" id="Seg_6621" s="T1">taldʼen</ta>
            <ta e="T4" id="Seg_6622" s="T3">kajaʔ</ta>
            <ta e="T5" id="Seg_6623" s="T4">i-bie-l</ta>
            <ta e="T6" id="Seg_6624" s="T5">dăk</ta>
            <ta e="T7" id="Seg_6625" s="T6">edə-bie-l</ta>
            <ta e="T9" id="Seg_6626" s="T8">edə-bie-m</ta>
            <ta e="T10" id="Seg_6627" s="T9">a</ta>
            <ta e="T11" id="Seg_6628" s="T10">kumən</ta>
            <ta e="T12" id="Seg_6629" s="T11">sumna</ta>
            <ta e="T13" id="Seg_6630" s="T12">gram</ta>
            <ta e="T14" id="Seg_6631" s="T13">aktʼa</ta>
            <ta e="T15" id="Seg_6632" s="T14">deʔ-pie-l</ta>
            <ta e="T16" id="Seg_6633" s="T15">deʔ-pie-m</ta>
            <ta e="T17" id="Seg_6634" s="T16">šide</ta>
            <ta e="T19" id="Seg_6635" s="T18">šide</ta>
            <ta e="T20" id="Seg_6636" s="T19">šălkovej</ta>
            <ta e="T21" id="Seg_6637" s="T20">dʼăbaktər-laʔbə-baʔ</ta>
            <ta e="T22" id="Seg_6638" s="T21">i</ta>
            <ta e="T23" id="Seg_6639" s="T22">kaknar-laʔbə-baʔ</ta>
            <ta e="T24" id="Seg_6640" s="T23">i</ta>
            <ta e="T26" id="Seg_6641" s="T25">ara</ta>
            <ta e="T27" id="Seg_6642" s="T26">bĭt-leʔbə-beʔ</ta>
            <ta e="T28" id="Seg_6643" s="T27">i</ta>
            <ta e="T29" id="Seg_6644" s="T28">ipek</ta>
            <ta e="T30" id="Seg_6645" s="T29">am-nia</ta>
            <ta e="T31" id="Seg_6646" s="T30">i</ta>
            <ta e="T32" id="Seg_6647" s="T31">uja</ta>
            <ta e="T33" id="Seg_6648" s="T32">am-naʔbə-baʔ</ta>
            <ta e="T34" id="Seg_6649" s="T33">i</ta>
            <ta e="T35" id="Seg_6650" s="T34">taŋgu</ta>
            <ta e="T36" id="Seg_6651" s="T35">nʼeʔ-leʔbə-beʔ</ta>
            <ta e="T37" id="Seg_6652" s="T36">bar</ta>
            <ta e="T38" id="Seg_6653" s="T37">miʔ</ta>
            <ta e="T39" id="Seg_6654" s="T38">to-ndə</ta>
            <ta e="T40" id="Seg_6655" s="T39">kuza</ta>
            <ta e="T41" id="Seg_6656" s="T40">amno-laʔbə</ta>
            <ta e="T42" id="Seg_6657" s="T41">dĭ-n</ta>
            <ta e="T43" id="Seg_6658" s="T42">nʼi</ta>
            <ta e="T44" id="Seg_6659" s="T43">miʔnʼibeʔ</ta>
            <ta e="T45" id="Seg_6660" s="T44">šo-bi</ta>
            <ta e="T46" id="Seg_6661" s="T45">jašɨk-tə</ta>
            <ta e="T47" id="Seg_6662" s="T46">păʔ-pi</ta>
            <ta e="T48" id="Seg_6663" s="T47">i</ta>
            <ta e="T49" id="Seg_6664" s="T48">škap-tə</ta>
            <ta e="T50" id="Seg_6665" s="T49">păʔ-pi</ta>
            <ta e="T51" id="Seg_6666" s="T50">măn</ta>
            <ta e="T52" id="Seg_6667" s="T51">măm-bia-m</ta>
            <ta e="T53" id="Seg_6668" s="T52">măm-bia-m</ta>
            <ta e="T54" id="Seg_6669" s="T53">ĭmbi</ta>
            <ta e="T56" id="Seg_6670" s="T55">măna</ta>
            <ta e="T57" id="Seg_6671" s="T56">Vanʼka</ta>
            <ta e="T59" id="Seg_6672" s="T58">măndə-r-zittə</ta>
            <ta e="T60" id="Seg_6673" s="T59">măn</ta>
            <ta e="T61" id="Seg_6674" s="T60">dĭ-m</ta>
            <ta e="T62" id="Seg_6675" s="T61">sürer-luʔ-pie-m</ta>
            <ta e="T63" id="Seg_6676" s="T62">măl-lia</ta>
            <ta e="T64" id="Seg_6677" s="T63">Miša-nə</ta>
            <ta e="T65" id="Seg_6678" s="T64">kan-a-ʔ</ta>
            <ta e="T66" id="Seg_6679" s="T65">döʔə</ta>
            <ta e="T67" id="Seg_6680" s="T66">dĭgəttə</ta>
            <ta e="T68" id="Seg_6681" s="T67">teinen</ta>
            <ta e="T69" id="Seg_6682" s="T68">šo-bi</ta>
            <ta e="T70" id="Seg_6683" s="T69">bura</ta>
            <ta e="T71" id="Seg_6684" s="T70">sanə-zi</ta>
            <ta e="T72" id="Seg_6685" s="T71">nu-ga</ta>
            <ta e="T73" id="Seg_6686" s="T72">dĭ</ta>
            <ta e="T74" id="Seg_6687" s="T73">dagaj</ta>
            <ta e="T75" id="Seg_6688" s="T74">i-bi</ta>
            <ta e="T76" id="Seg_6689" s="T75">i</ta>
            <ta e="T77" id="Seg_6690" s="T76">bura</ta>
            <ta e="T78" id="Seg_6691" s="T77">băʔ-pi</ta>
            <ta e="T79" id="Seg_6692" s="T78">sanə</ta>
            <ta e="T80" id="Seg_6693" s="T79">i-bi</ta>
            <ta e="T81" id="Seg_6694" s="T80">bos-tə</ta>
            <ta e="T82" id="Seg_6695" s="T81">barəʔ-luʔ-pi</ta>
            <ta e="T83" id="Seg_6696" s="T82">sĭreʔpne</ta>
            <ta e="T84" id="Seg_6697" s="T83">miʔ</ta>
            <ta e="T85" id="Seg_6698" s="T84">tura-gən</ta>
            <ta e="T86" id="Seg_6699" s="T85">ne</ta>
            <ta e="T87" id="Seg_6700" s="T86">nuʔmə-luʔ-pi</ta>
            <ta e="T89" id="Seg_6701" s="T88">dĭŋinaʔtə</ta>
            <ta e="T90" id="Seg_6702" s="T89">tojir-bi</ta>
            <ta e="T91" id="Seg_6703" s="T90">sanə</ta>
            <ta e="T92" id="Seg_6704" s="T91">a</ta>
            <ta e="T93" id="Seg_6705" s="T92">dĭ</ta>
            <ta e="T94" id="Seg_6706" s="T93">bar</ta>
            <ta e="T95" id="Seg_6707" s="T94">pʼer-luʔbə</ta>
            <ta e="T96" id="Seg_6708" s="T95">dö</ta>
            <ta e="T97" id="Seg_6709" s="T96">sanə</ta>
            <ta e="T98" id="Seg_6710" s="T97">a</ta>
            <ta e="T99" id="Seg_6711" s="T98">dĭ</ta>
            <ta e="T100" id="Seg_6712" s="T99">măm-bi</ta>
            <ta e="T101" id="Seg_6713" s="T100">măn</ta>
            <ta e="T102" id="Seg_6714" s="T101">dĭ-m</ta>
            <ta e="T103" id="Seg_6715" s="T102">kut-li-m</ta>
            <ta e="T104" id="Seg_6716" s="T103">a</ta>
            <ta e="T105" id="Seg_6717" s="T104">ĭmbi</ta>
            <ta e="T106" id="Seg_6718" s="T105">ej</ta>
            <ta e="T107" id="Seg_6719" s="T106">măm-bi-leʔ</ta>
            <ta e="T109" id="Seg_6720" s="T108">taldʼen</ta>
            <ta e="T110" id="Seg_6721" s="T109">măna</ta>
            <ta e="T111" id="Seg_6722" s="T110">kăštə-bi-ʔi</ta>
            <ta e="T113" id="Seg_6723" s="T112">măn</ta>
            <ta e="T115" id="Seg_6724" s="T114">tugan-də</ta>
            <ta e="T116" id="Seg_6725" s="T115">šo-ʔ</ta>
            <ta e="T117" id="Seg_6726" s="T116">miʔnʼibeʔ</ta>
            <ta e="T118" id="Seg_6727" s="T117">amor-zittə</ta>
            <ta e="T120" id="Seg_6728" s="T119">ešši</ta>
            <ta e="T122" id="Seg_6729" s="T121">kü-lam-bi</ta>
            <ta e="T123" id="Seg_6730" s="T122">ušonʼiʔkə</ta>
            <ta e="T124" id="Seg_6731" s="T123">kam-bi</ta>
            <ta e="T125" id="Seg_6732" s="T124">a</ta>
            <ta e="T126" id="Seg_6733" s="T125">măn</ta>
            <ta e="T127" id="Seg_6734" s="T126">măm-bia-m</ta>
            <ta e="T128" id="Seg_6735" s="T127">šo-la-m</ta>
            <ta e="T129" id="Seg_6736" s="T128">a</ta>
            <ta e="T131" id="Seg_6737" s="T130">a</ta>
            <ta e="T132" id="Seg_6738" s="T131">bos-pə</ta>
            <ta e="T133" id="Seg_6739" s="T132">ej</ta>
            <ta e="T134" id="Seg_6740" s="T133">kam-bia-m</ta>
            <ta e="T135" id="Seg_6741" s="T134">mei-m</ta>
            <ta e="T136" id="Seg_6742" s="T135">kăštə-bia-m</ta>
            <ta e="T137" id="Seg_6743" s="T136">kăštə-bia-m</ta>
            <ta e="T138" id="Seg_6744" s="T137">dĭ</ta>
            <ta e="T139" id="Seg_6745" s="T138">ej</ta>
            <ta e="T140" id="Seg_6746" s="T139">kam-bi</ta>
            <ta e="T141" id="Seg_6747" s="T140">i</ta>
            <ta e="T142" id="Seg_6748" s="T141">măn</ta>
            <ta e="T143" id="Seg_6749" s="T142">ej</ta>
            <ta e="T144" id="Seg_6750" s="T143">kam-bia-m</ta>
            <ta e="T145" id="Seg_6751" s="T144">dĭn</ta>
            <ta e="T146" id="Seg_6752" s="T145">ugandə</ta>
            <ta e="T147" id="Seg_6753" s="T146">ara-j</ta>
            <ta e="T148" id="Seg_6754" s="T147">a</ta>
            <ta e="T149" id="Seg_6755" s="T148">măna</ta>
            <ta e="T150" id="Seg_6756" s="T149">ara</ta>
            <ta e="T151" id="Seg_6757" s="T150">ej</ta>
            <ta e="T152" id="Seg_6758" s="T151">kereʔ</ta>
            <ta e="T153" id="Seg_6759" s="T152">măn</ta>
            <ta e="T156" id="Seg_6760" s="T155">dĭ-m</ta>
            <ta e="T157" id="Seg_6761" s="T156">bĭʔ-sittə</ta>
            <ta e="T158" id="Seg_6762" s="T157">a</ta>
            <ta e="T159" id="Seg_6763" s="T158">dĭ</ta>
            <ta e="T160" id="Seg_6764" s="T159">kuro-luʔ-pi</ta>
            <ta e="T161" id="Seg_6765" s="T160">bar</ta>
            <ta e="T162" id="Seg_6766" s="T161">măn-də</ta>
            <ta e="T163" id="Seg_6767" s="T162">urgaja-m</ta>
            <ta e="T164" id="Seg_6768" s="T163">ažnə</ta>
            <ta e="T165" id="Seg_6769" s="T164">dʼoːdunʼi</ta>
            <ta e="T166" id="Seg_6770" s="T165">ku-bia-m</ta>
            <ta e="T167" id="Seg_6771" s="T166">tüj</ta>
            <ta e="T168" id="Seg_6772" s="T167">büzəj-leʔ</ta>
            <ta e="T169" id="Seg_6773" s="T168">bar</ta>
            <ta e="T170" id="Seg_6774" s="T169">tüšəl-bi</ta>
            <ta e="T171" id="Seg_6775" s="T170">šiʔnʼileʔ</ta>
            <ta e="T172" id="Seg_6776" s="T171">iʔbə-leʔbə</ta>
            <ta e="T173" id="Seg_6777" s="T172">amno-ʔ</ta>
            <ta e="T174" id="Seg_6778" s="T173">iʔbə-leʔbə</ta>
            <ta e="T175" id="Seg_6779" s="T174">ej</ta>
            <ta e="T176" id="Seg_6780" s="T175">kirgar-laʔbə</ta>
            <ta e="T177" id="Seg_6781" s="T176">teinen</ta>
            <ta e="T178" id="Seg_6782" s="T177">ejü</ta>
            <ta e="T179" id="Seg_6783" s="T178">mo-laːm-bi</ta>
            <ta e="T180" id="Seg_6784" s="T179">sĭre</ta>
            <ta e="T181" id="Seg_6785" s="T180">bar</ta>
            <ta e="T182" id="Seg_6786" s="T181">nömrunʼiʔ</ta>
            <ta e="T183" id="Seg_6787" s="T182">i-bi</ta>
            <ta e="T184" id="Seg_6788" s="T183">sĭre</ta>
            <ta e="T186" id="Seg_6789" s="T185">büžü</ta>
            <ta e="T187" id="Seg_6790" s="T186">bü</ta>
            <ta e="T188" id="Seg_6791" s="T187">mo-lal-ləj</ta>
            <ta e="T189" id="Seg_6792" s="T188">dĭgəttə</ta>
            <ta e="T190" id="Seg_6793" s="T189">noʔ</ta>
            <ta e="T191" id="Seg_6794" s="T190">özer-lə-j</ta>
            <ta e="T193" id="Seg_6795" s="T192">svʼetog-əʔi</ta>
            <ta e="T194" id="Seg_6796" s="T193">özer-lə-ʔi</ta>
            <ta e="T195" id="Seg_6797" s="T194">pa-ʔi</ta>
            <ta e="T196" id="Seg_6798" s="T195">bar</ta>
            <ta e="T197" id="Seg_6799" s="T196">kuvas</ta>
            <ta e="T198" id="Seg_6800" s="T197">mo-lə-ʔi</ta>
            <ta e="T199" id="Seg_6801" s="T198">zʼelʼona-ʔi</ta>
            <ta e="T200" id="Seg_6802" s="T199">mo-lə-ʔi</ta>
            <ta e="T201" id="Seg_6803" s="T200">bar</ta>
            <ta e="T202" id="Seg_6804" s="T201">dĭgəttə</ta>
            <ta e="T203" id="Seg_6805" s="T202">ine-ʔi</ta>
            <ta e="T204" id="Seg_6806" s="T203">tüžöj-əʔi</ta>
            <ta e="T205" id="Seg_6807" s="T204">ular-də</ta>
            <ta e="T206" id="Seg_6808" s="T205">ka-la-ʔi</ta>
            <ta e="T207" id="Seg_6809" s="T206">noʔ</ta>
            <ta e="T208" id="Seg_6810" s="T207">am-zittə</ta>
            <ta e="T209" id="Seg_6811" s="T208">dĭgəttə</ta>
            <ta e="T210" id="Seg_6812" s="T209">noʔ</ta>
            <ta e="T211" id="Seg_6813" s="T210">urgo</ta>
            <ta e="T212" id="Seg_6814" s="T211">özer-lə-j</ta>
            <ta e="T213" id="Seg_6815" s="T212">dĭgəttə</ta>
            <ta e="T214" id="Seg_6816" s="T213">dĭ-m</ta>
            <ta e="T216" id="Seg_6817" s="T215">šapko-zi</ta>
            <ta e="T217" id="Seg_6818" s="T216">jaʔ-sittə</ta>
            <ta e="T218" id="Seg_6819" s="T217">nada</ta>
            <ta e="T219" id="Seg_6820" s="T218">dĭgəttə</ta>
            <ta e="T220" id="Seg_6821" s="T219">kuja-gən</ta>
            <ta e="T221" id="Seg_6822" s="T220">ko-lə-j</ta>
            <ta e="T222" id="Seg_6823" s="T221">da</ta>
            <ta e="T223" id="Seg_6824" s="T222">oʔbdə-sʼtə</ta>
            <ta e="T224" id="Seg_6825" s="T223">nada</ta>
            <ta e="T225" id="Seg_6826" s="T224">il</ta>
            <ta e="T226" id="Seg_6827" s="T225">mĭn-lia-ʔi</ta>
            <ta e="T227" id="Seg_6828" s="T226">ugandə</ta>
            <ta e="T228" id="Seg_6829" s="T227">jakše</ta>
            <ta e="T229" id="Seg_6830" s="T228">pʼe</ta>
            <ta e="T230" id="Seg_6831" s="T229">šo-bi</ta>
            <ta e="T231" id="Seg_6832" s="T230">ugandə</ta>
            <ta e="T233" id="Seg_6833" s="T232">ejü</ta>
            <ta e="T234" id="Seg_6834" s="T233">măn</ta>
            <ta e="T235" id="Seg_6835" s="T234">dönüli</ta>
            <ta e="T236" id="Seg_6836" s="T235">mo-bia-m</ta>
            <ta e="T237" id="Seg_6837" s="T236">kunol-zittə</ta>
            <ta e="T238" id="Seg_6838" s="T237">dĭgəttə</ta>
            <ta e="T239" id="Seg_6839" s="T238">ertə</ta>
            <ta e="T240" id="Seg_6840" s="T239">uʔbdə-bia-m</ta>
            <ta e="T241" id="Seg_6841" s="T240">a</ta>
            <ta e="T242" id="Seg_6842" s="T241">dʼala</ta>
            <ta e="T243" id="Seg_6843" s="T242">dʼala-m</ta>
            <ta e="T244" id="Seg_6844" s="T243">bar</ta>
            <ta e="T245" id="Seg_6845" s="T244">togonor-bia-m</ta>
            <ta e="T246" id="Seg_6846" s="T245">tüžöj</ta>
            <ta e="T247" id="Seg_6847" s="T246">surdo-bia-m</ta>
            <ta e="T248" id="Seg_6848" s="T247">da</ta>
            <ta e="T249" id="Seg_6849" s="T248">mĭnzər-bie-m</ta>
            <ta e="T250" id="Seg_6850" s="T249">a-bia-m</ta>
            <ta e="T251" id="Seg_6851" s="T250">dĭgəttə</ta>
            <ta e="T252" id="Seg_6852" s="T251">dʼăbaktər-bia-m</ta>
            <ta e="T253" id="Seg_6853" s="T252">ertə</ta>
            <ta e="T254" id="Seg_6854" s="T253">oʔbdə-bia-m</ta>
            <ta e="T255" id="Seg_6855" s="T254">da</ta>
            <ta e="T256" id="Seg_6856" s="T255">šü</ta>
            <ta e="T257" id="Seg_6857" s="T256">nendə-bie-m</ta>
            <ta e="T258" id="Seg_6858" s="T257">măna</ta>
            <ta e="T259" id="Seg_6859" s="T258">Kazan</ta>
            <ta e="T260" id="Seg_6860" s="T259">tura-nə</ta>
            <ta e="T261" id="Seg_6861" s="T260">nada</ta>
            <ta e="T262" id="Seg_6862" s="T261">kan-zittə</ta>
            <ta e="T263" id="Seg_6863" s="T262">măna</ta>
            <ta e="T264" id="Seg_6864" s="T263">aktʼa</ta>
            <ta e="T265" id="Seg_6865" s="T264">iʔbo-laʔbə</ta>
            <ta e="T266" id="Seg_6866" s="T265">bank-a-n</ta>
            <ta e="T267" id="Seg_6867" s="T266">i-zittə</ta>
            <ta e="T268" id="Seg_6868" s="T267">nada</ta>
            <ta e="T269" id="Seg_6869" s="T268">da</ta>
            <ta e="T270" id="Seg_6870" s="T269">il-də</ta>
            <ta e="T271" id="Seg_6871" s="T270">mĭ-zittə</ta>
            <ta e="T272" id="Seg_6872" s="T271">măn</ta>
            <ta e="T273" id="Seg_6873" s="T272">dĭ-zeŋ-gən</ta>
            <ta e="T274" id="Seg_6874" s="T273">i-bie-m</ta>
            <ta e="T275" id="Seg_6875" s="T274">măn</ta>
            <ta e="T276" id="Seg_6876" s="T275">iʔgö</ta>
            <ta e="T277" id="Seg_6877" s="T276">dʼăbaktər-ia-m</ta>
            <ta e="T279" id="Seg_6878" s="T278">tüj</ta>
            <ta e="T281" id="Seg_6879" s="T280">dʼăbaktər-laʔbə-ʔjə</ta>
            <ta e="T282" id="Seg_6880" s="T281">ugandə</ta>
            <ta e="T283" id="Seg_6881" s="T282">tăn</ta>
            <ta e="T284" id="Seg_6882" s="T283">koloʔ-səbi</ta>
            <ta e="T285" id="Seg_6883" s="T284">koʔbdo</ta>
            <ta e="T286" id="Seg_6884" s="T285">üge</ta>
            <ta e="T287" id="Seg_6885" s="T286">măna</ta>
            <ta e="T288" id="Seg_6886" s="T287">măna</ta>
            <ta e="T289" id="Seg_6887" s="T288">nada</ta>
            <ta e="T290" id="Seg_6888" s="T289">dʼăbaktər-zittə</ta>
            <ta e="T291" id="Seg_6889" s="T290">tüj</ta>
            <ta e="T292" id="Seg_6890" s="T291">tăn</ta>
            <ta e="T293" id="Seg_6891" s="T292">dʼăbaktər-a-ʔ</ta>
            <ta e="T294" id="Seg_6892" s="T293">a</ta>
            <ta e="T295" id="Seg_6893" s="T294">măn</ta>
            <ta e="T296" id="Seg_6894" s="T295">ej</ta>
            <ta e="T297" id="Seg_6895" s="T296">dʼăbaktər-la-m</ta>
            <ta e="T299" id="Seg_6896" s="T298">măn</ta>
            <ta e="T300" id="Seg_6897" s="T299">ej</ta>
            <ta e="T301" id="Seg_6898" s="T300">tĭm-nie-m</ta>
            <ta e="T302" id="Seg_6899" s="T301">ĭmbi</ta>
            <ta e="T303" id="Seg_6900" s="T302">dʼăbaktər-zittə</ta>
            <ta e="T304" id="Seg_6901" s="T303">tăn</ta>
            <ta e="T305" id="Seg_6902" s="T304">dʼăbaktər-a-ʔ</ta>
            <ta e="T306" id="Seg_6903" s="T305">a</ta>
            <ta e="T308" id="Seg_6904" s="T307">a</ta>
            <ta e="T309" id="Seg_6905" s="T308">măn</ta>
            <ta e="T310" id="Seg_6906" s="T309">ĭmbi</ta>
            <ta e="T311" id="Seg_6907" s="T310">üge</ta>
            <ta e="T313" id="Seg_6908" s="T312">dʼăbaktər-zittə</ta>
            <ta e="T317" id="Seg_6909" s="T315">iʔbə-le-m=da</ta>
            <ta e="T318" id="Seg_6910" s="T317">măn</ta>
            <ta e="T319" id="Seg_6911" s="T318">tura-m</ta>
            <ta e="T322" id="Seg_6912" s="T321">măn</ta>
            <ta e="T323" id="Seg_6913" s="T322">ĭmbi=də</ta>
            <ta e="T324" id="Seg_6914" s="T323">ej</ta>
            <ta e="T325" id="Seg_6915" s="T324">tĭm-nie-m</ta>
            <ta e="T326" id="Seg_6916" s="T325">tenö-ʔ</ta>
            <ta e="T327" id="Seg_6917" s="T326">ĭmbi=nʼibudʼ</ta>
            <ta e="T328" id="Seg_6918" s="T327">šama-ʔ</ta>
            <ta e="T329" id="Seg_6919" s="T328">ĭmbi=nʼibudʼ</ta>
            <ta e="T330" id="Seg_6920" s="T329">aba-m</ta>
            <ta e="T331" id="Seg_6921" s="T330">kuŋgə-m</ta>
            <ta e="T1057" id="Seg_6922" s="T331">kal-la</ta>
            <ta e="T332" id="Seg_6923" s="T1057">dʼür-bi</ta>
            <ta e="T333" id="Seg_6924" s="T332">a</ta>
            <ta e="T334" id="Seg_6925" s="T333">măna</ta>
            <ta e="T335" id="Seg_6926" s="T334">ugandə</ta>
            <ta e="T336" id="Seg_6927" s="T335">jakše</ta>
            <ta e="T337" id="Seg_6928" s="T336">šindi=də</ta>
            <ta e="T338" id="Seg_6929" s="T337">ej</ta>
            <ta e="T339" id="Seg_6930" s="T338">münör-ia</ta>
            <ta e="T340" id="Seg_6931" s="T339">măn</ta>
            <ta e="T341" id="Seg_6932" s="T340">balʔraʔ</ta>
            <ta e="T342" id="Seg_6933" s="T341">alom-nia-m</ta>
            <ta e="T343" id="Seg_6934" s="T342">ajnu</ta>
            <ta e="T344" id="Seg_6935" s="T343">nüj-leʔbə-m</ta>
            <ta e="T345" id="Seg_6936" s="T344">suʔmi-leʔbə-m</ta>
            <ta e="T346" id="Seg_6937" s="T345">bar</ta>
            <ta e="T347" id="Seg_6938" s="T346">a</ta>
            <ta e="T348" id="Seg_6939" s="T347">ia-m</ta>
            <ta e="T349" id="Seg_6940" s="T348">ej</ta>
            <ta e="T350" id="Seg_6941" s="T349">nʼilgö-lie-m</ta>
            <ta e="T351" id="Seg_6942" s="T350">ia-m</ta>
            <ta e="T352" id="Seg_6943" s="T351">aba-m</ta>
            <ta e="T353" id="Seg_6944" s="T352">dĭ</ta>
            <ta e="T354" id="Seg_6945" s="T353">saʔpiaŋ-bi</ta>
            <ta e="T355" id="Seg_6946" s="T354">što</ta>
            <ta e="T356" id="Seg_6947" s="T355">măn</ta>
            <ta e="T358" id="Seg_6948" s="T357">što</ta>
            <ta e="T359" id="Seg_6949" s="T358">măn</ta>
            <ta e="T360" id="Seg_6950" s="T359">alom-nia-m</ta>
            <ta e="T361" id="Seg_6951" s="T360">măn</ta>
            <ta e="T362" id="Seg_6952" s="T361">elem</ta>
            <ta e="T363" id="Seg_6953" s="T362">teinen</ta>
            <ta e="T364" id="Seg_6954" s="T363">nüdʼi-n</ta>
            <ta e="T365" id="Seg_6955" s="T364">bar</ta>
            <ta e="T366" id="Seg_6956" s="T365">ara</ta>
            <ta e="T369" id="Seg_6957" s="T368">nüjnə</ta>
            <ta e="T371" id="Seg_6958" s="T370">nüjnə-bi</ta>
            <ta e="T372" id="Seg_6959" s="T371">dʼabəro-bi</ta>
            <ta e="T373" id="Seg_6960" s="T372">bar</ta>
            <ta e="T374" id="Seg_6961" s="T373">kudo-nzə-bi</ta>
            <ta e="T375" id="Seg_6962" s="T374">bar</ta>
            <ta e="T376" id="Seg_6963" s="T375">Jelʼa</ta>
            <ta e="T377" id="Seg_6964" s="T376">unnʼa</ta>
            <ta e="T378" id="Seg_6965" s="T377">amno-laʔbə</ta>
            <ta e="T379" id="Seg_6966" s="T378">üge</ta>
            <ta e="T380" id="Seg_6967" s="T379">monoʔko-laʔbə-ʔjə</ta>
            <ta e="T381" id="Seg_6968" s="T380">a</ta>
            <ta e="T382" id="Seg_6969" s="T381">dĭ</ta>
            <ta e="T383" id="Seg_6970" s="T382">üge</ta>
            <ta e="T384" id="Seg_6971" s="T383">ej</ta>
            <ta e="T385" id="Seg_6972" s="T384">ka-lia</ta>
            <ta e="T386" id="Seg_6973" s="T385">šindi-nə=də</ta>
            <ta e="T387" id="Seg_6974" s="T386">ej</ta>
            <ta e="T388" id="Seg_6975" s="T387">ka-lia</ta>
            <ta e="T394" id="Seg_6976" s="T393">dĭ-n</ta>
            <ta e="T395" id="Seg_6977" s="T394">bar</ta>
            <ta e="T396" id="Seg_6978" s="T395">ia-t</ta>
            <ta e="T397" id="Seg_6979" s="T396">aba-t</ta>
            <ta e="T398" id="Seg_6980" s="T397">i-ge</ta>
            <ta e="T399" id="Seg_6981" s="T398">mă-lia-ʔi</ta>
            <ta e="T400" id="Seg_6982" s="T399">kan-a-ʔ</ta>
            <ta e="T401" id="Seg_6983" s="T400">tibi-nə</ta>
            <ta e="T402" id="Seg_6984" s="T401">a</ta>
            <ta e="T403" id="Seg_6985" s="T402">dĭ</ta>
            <ta e="T404" id="Seg_6986" s="T403">kăde=də</ta>
            <ta e="T405" id="Seg_6987" s="T404">ej</ta>
            <ta e="T406" id="Seg_6988" s="T405">ka-lia</ta>
            <ta e="T407" id="Seg_6989" s="T406">šo-bi</ta>
            <ta e="T408" id="Seg_6990" s="T407">măna</ta>
            <ta e="T409" id="Seg_6991" s="T408">monoʔko-sʼtə</ta>
            <ta e="T410" id="Seg_6992" s="T409">ej</ta>
            <ta e="T411" id="Seg_6993" s="T410">kuvas</ta>
            <ta e="T412" id="Seg_6994" s="T411">kuza</ta>
            <ta e="T413" id="Seg_6995" s="T412">măna</ta>
            <ta e="T414" id="Seg_6996" s="T413">nʼe</ta>
            <ta e="T415" id="Seg_6997" s="T414">axota</ta>
            <ta e="T416" id="Seg_6998" s="T415">kan-zittə</ta>
            <ta e="T417" id="Seg_6999" s="T416">aba-m</ta>
            <ta e="T418" id="Seg_7000" s="T417">măn-də</ta>
            <ta e="T419" id="Seg_7001" s="T418">ĭmbi</ta>
            <ta e="T420" id="Seg_7002" s="T419">tăn</ta>
            <ta e="T421" id="Seg_7003" s="T420">ej</ta>
            <ta e="T422" id="Seg_7004" s="T421">ka-la-l</ta>
            <ta e="T423" id="Seg_7005" s="T422">ej</ta>
            <ta e="T424" id="Seg_7006" s="T423">nu</ta>
            <ta e="T425" id="Seg_7007" s="T424">i-ʔ</ta>
            <ta e="T426" id="Seg_7008" s="T425">kan-a-ʔ</ta>
            <ta e="T427" id="Seg_7009" s="T426">măn</ta>
            <ta e="T428" id="Seg_7010" s="T427">nuʔmə-luʔ-pia-m</ta>
            <ta e="T429" id="Seg_7011" s="T428">dĭgəttə</ta>
            <ta e="T430" id="Seg_7012" s="T429">măn</ta>
            <ta e="T433" id="Seg_7013" s="T432">tibi-m</ta>
            <ta e="T434" id="Seg_7014" s="T433">šo-bi</ta>
            <ta e="T435" id="Seg_7015" s="T434">măna</ta>
            <ta e="T436" id="Seg_7016" s="T435">monoʔko-sʼtə</ta>
            <ta e="T437" id="Seg_7017" s="T436">dĭʔ-nə</ta>
            <ta e="T439" id="Seg_7018" s="T438">pʼe</ta>
            <ta e="T440" id="Seg_7019" s="T439">i</ta>
            <ta e="T441" id="Seg_7020" s="T440">măna</ta>
            <ta e="T442" id="Seg_7021" s="T441">amga</ta>
            <ta e="T443" id="Seg_7022" s="T442">pʼe</ta>
            <ta e="T444" id="Seg_7023" s="T443">i</ta>
            <ta e="T445" id="Seg_7024" s="T444">măna</ta>
            <ta e="T446" id="Seg_7025" s="T445">amga</ta>
            <ta e="T447" id="Seg_7026" s="T446">pʼe</ta>
            <ta e="T448" id="Seg_7027" s="T447">măn</ta>
            <ta e="T449" id="Seg_7028" s="T448">nuʔ</ta>
            <ta e="T450" id="Seg_7029" s="T449">măn</ta>
            <ta e="T451" id="Seg_7030" s="T450">măn-də-m</ta>
            <ta e="T452" id="Seg_7031" s="T451">kan-a-ʔ</ta>
            <ta e="T453" id="Seg_7032" s="T452">aba-nə</ta>
            <ta e="T454" id="Seg_7033" s="T453">da</ta>
            <ta e="T455" id="Seg_7034" s="T454">ia-nə</ta>
            <ta e="T456" id="Seg_7035" s="T455">monoʔko-ʔ</ta>
            <ta e="T457" id="Seg_7036" s="T456">dĭ-zeŋ</ta>
            <ta e="T458" id="Seg_7037" s="T457">mĭ-lə-ʔi</ta>
            <ta e="T459" id="Seg_7038" s="T458">dʼok</ta>
            <ta e="T460" id="Seg_7039" s="T459">ej</ta>
            <ta e="T461" id="Seg_7040" s="T460">mĭ-lə-ʔi</ta>
            <ta e="T462" id="Seg_7041" s="T461">dĭgəttə</ta>
            <ta e="T463" id="Seg_7042" s="T462">măna</ta>
            <ta e="T464" id="Seg_7043" s="T463">dʼăbaktər-luʔ-pi-ʔi</ta>
            <ta e="T465" id="Seg_7044" s="T464">i</ta>
            <ta e="T467" id="Seg_7045" s="T466">kam-bi-baʔ</ta>
            <ta e="T469" id="Seg_7046" s="T468">ine-t-si</ta>
            <ta e="T470" id="Seg_7047" s="T469">dĭʔ-nə</ta>
            <ta e="T471" id="Seg_7048" s="T470">dĭgəttə</ta>
            <ta e="T472" id="Seg_7049" s="T471">ia-m</ta>
            <ta e="T473" id="Seg_7050" s="T472">šo-bi</ta>
            <ta e="T474" id="Seg_7051" s="T473">dibər</ta>
            <ta e="T475" id="Seg_7052" s="T474">miʔnʼibeʔ</ta>
            <ta e="T476" id="Seg_7053" s="T475">dĭ</ta>
            <ta e="T478" id="Seg_7054" s="T477">döbər</ta>
            <ta e="T479" id="Seg_7055" s="T478">šo-bi</ta>
            <ta e="T480" id="Seg_7056" s="T479">noʔ</ta>
            <ta e="T481" id="Seg_7057" s="T480">jaʔ-sittə</ta>
            <ta e="T482" id="Seg_7058" s="T481">dĭgəttə</ta>
            <ta e="T483" id="Seg_7059" s="T482">kam-bi</ta>
            <ta e="T484" id="Seg_7060" s="T483">bü-nə</ta>
            <ta e="T485" id="Seg_7061" s="T484">da</ta>
            <ta e="T486" id="Seg_7062" s="T485">dĭn</ta>
            <ta e="T487" id="Seg_7063" s="T486">kü-lam-bi</ta>
            <ta e="T488" id="Seg_7064" s="T487">dĭgəttə</ta>
            <ta e="T489" id="Seg_7065" s="T488">măn</ta>
            <ta e="T490" id="Seg_7066" s="T489">ma-ndə</ta>
            <ta e="T491" id="Seg_7067" s="T490">šo-bia-m</ta>
            <ta e="T492" id="Seg_7068" s="T491">i</ta>
            <ta e="T493" id="Seg_7069" s="T492">ugandə</ta>
            <ta e="T494" id="Seg_7070" s="T493">tăŋ</ta>
            <ta e="T495" id="Seg_7071" s="T494">tenö-bia-m</ta>
            <ta e="T496" id="Seg_7072" s="T495">sĭj-bə</ta>
            <ta e="T497" id="Seg_7073" s="T496">ĭzem-bie</ta>
            <ta e="T498" id="Seg_7074" s="T497">dʼor-bia-m</ta>
            <ta e="T499" id="Seg_7075" s="T498">ajir-bio-m</ta>
            <ta e="T500" id="Seg_7076" s="T499">dĭ-m</ta>
            <ta e="T501" id="Seg_7077" s="T500">bar</ta>
            <ta e="T502" id="Seg_7078" s="T501">dĭgəttə</ta>
            <ta e="T503" id="Seg_7079" s="T502">ia-m</ta>
            <ta e="T504" id="Seg_7080" s="T503">măna</ta>
            <ta e="T505" id="Seg_7081" s="T504">bü</ta>
            <ta e="T506" id="Seg_7082" s="T505">dʼazir-bi</ta>
            <ta e="T507" id="Seg_7083" s="T506">pĭj</ta>
            <ta e="T508" id="Seg_7084" s="T507">măna</ta>
            <ta e="T509" id="Seg_7085" s="T508">dĭgəttə</ta>
            <ta e="T510" id="Seg_7086" s="T509">măn</ta>
            <ta e="T511" id="Seg_7087" s="T510">davaj</ta>
            <ta e="T512" id="Seg_7088" s="T511">nüjnə</ta>
            <ta e="T514" id="Seg_7089" s="T513">nüj-zittə</ta>
            <ta e="T515" id="Seg_7090" s="T514">dĭgəttə</ta>
            <ta e="T516" id="Seg_7091" s="T515">măn</ta>
            <ta e="T517" id="Seg_7092" s="T516">šo-bia-m</ta>
            <ta e="T518" id="Seg_7093" s="T517">maʔ-nʼi</ta>
            <ta e="T519" id="Seg_7094" s="T518">amno-bia-m</ta>
            <ta e="T520" id="Seg_7095" s="T519">dĭgəttə</ta>
            <ta e="T521" id="Seg_7096" s="T520">kam-bia-m</ta>
            <ta e="T523" id="Seg_7097" s="T522">Permʼakov-tə</ta>
            <ta e="T525" id="Seg_7098" s="T524">ten</ta>
            <ta e="T526" id="Seg_7099" s="T525">sʼar-bi-baʔ</ta>
            <ta e="T527" id="Seg_7100" s="T526">suʔmi-beʔ</ta>
            <ta e="T528" id="Seg_7101" s="T527">garmonʼ-nʼə</ta>
            <ta e="T529" id="Seg_7102" s="T528">sʼar-bi-baʔ</ta>
            <ta e="T530" id="Seg_7103" s="T529">nüjnə</ta>
            <ta e="T531" id="Seg_7104" s="T530">nüjnə-bi-beʔ</ta>
            <ta e="T532" id="Seg_7105" s="T531">dĭgəttə</ta>
            <ta e="T533" id="Seg_7106" s="T532">dĭn</ta>
            <ta e="T534" id="Seg_7107" s="T533">tibi</ta>
            <ta e="T535" id="Seg_7108" s="T534">šo-bi</ta>
            <ta e="T536" id="Seg_7109" s="T535">Nagornaj</ta>
            <ta e="T537" id="Seg_7110" s="T536">muʔzen-də</ta>
            <ta e="T538" id="Seg_7111" s="T537">urgo</ta>
            <ta e="T540" id="Seg_7112" s="T539">da</ta>
            <ta e="T541" id="Seg_7113" s="T540">kömə-ʔi</ta>
            <ta e="T542" id="Seg_7114" s="T541">bos-tə</ta>
            <ta e="T543" id="Seg_7115" s="T542">kömə</ta>
            <ta e="T544" id="Seg_7116" s="T543">dĭgəttə</ta>
            <ta e="T545" id="Seg_7117" s="T544">măn</ta>
            <ta e="T546" id="Seg_7118" s="T545">maʔ-nʼi</ta>
            <ta e="T547" id="Seg_7119" s="T546">šo-bia-m</ta>
            <ta e="T548" id="Seg_7120" s="T547">da</ta>
            <ta e="T549" id="Seg_7121" s="T548">šo-bi</ta>
            <ta e="T550" id="Seg_7122" s="T549">măna</ta>
            <ta e="T551" id="Seg_7123" s="T550">monoʔko-sʼtə</ta>
            <ta e="T552" id="Seg_7124" s="T551">măn</ta>
            <ta e="T553" id="Seg_7125" s="T552">măn-də-m</ta>
            <ta e="T554" id="Seg_7126" s="T553">tăn</ta>
            <ta e="T555" id="Seg_7127" s="T554">nʼi</ta>
            <ta e="T556" id="Seg_7128" s="T555">git</ta>
            <ta e="T558" id="Seg_7129" s="T557">kak</ta>
            <ta e="T559" id="Seg_7130" s="T558">măn</ta>
            <ta e="T560" id="Seg_7131" s="T559">dĭrgit</ta>
            <ta e="T561" id="Seg_7132" s="T560">nu</ta>
            <ta e="T562" id="Seg_7133" s="T561">măn</ta>
            <ta e="T563" id="Seg_7134" s="T562">tăn-zi</ta>
            <ta e="T564" id="Seg_7135" s="T563">ej</ta>
            <ta e="T565" id="Seg_7136" s="T564">amno-la-m</ta>
            <ta e="T566" id="Seg_7137" s="T565">a</ta>
            <ta e="T567" id="Seg_7138" s="T566">nʼi-zi</ta>
            <ta e="T568" id="Seg_7139" s="T567">dĭgəttə</ta>
            <ta e="T569" id="Seg_7140" s="T568">ia-m</ta>
            <ta e="T570" id="Seg_7141" s="T569">măn-də</ta>
            <ta e="T571" id="Seg_7142" s="T570">kan-a-ʔ</ta>
            <ta e="T572" id="Seg_7143" s="T571">dĭ-n</ta>
            <ta e="T573" id="Seg_7144" s="T572">aktʼa</ta>
            <ta e="T574" id="Seg_7145" s="T573">iʔgö</ta>
            <ta e="T575" id="Seg_7146" s="T574">ugandə</ta>
            <ta e="T577" id="Seg_7147" s="T576">jakše</ta>
            <ta e="T578" id="Seg_7148" s="T577">kuza</ta>
            <ta e="T579" id="Seg_7149" s="T578">a</ta>
            <ta e="T580" id="Seg_7150" s="T579">măn</ta>
            <ta e="T581" id="Seg_7151" s="T580">nu-lia-m</ta>
            <ta e="T582" id="Seg_7152" s="T581">ej</ta>
            <ta e="T583" id="Seg_7153" s="T582">da</ta>
            <ta e="T584" id="Seg_7154" s="T583">kan-a-ʔ</ta>
            <ta e="T585" id="Seg_7155" s="T584">da</ta>
            <ta e="T586" id="Seg_7156" s="T585">amno-ʔ</ta>
            <ta e="T587" id="Seg_7157" s="T586">dĭ-zi</ta>
            <ta e="T588" id="Seg_7158" s="T587">onʼiʔ</ta>
            <ta e="T589" id="Seg_7159" s="T588">kuza</ta>
            <ta e="T590" id="Seg_7160" s="T589">šo-bi</ta>
            <ta e="T591" id="Seg_7161" s="T590">monoʔko-sʼtə</ta>
            <ta e="T592" id="Seg_7162" s="T591">a</ta>
            <ta e="T593" id="Seg_7163" s="T592">măn</ta>
            <ta e="T594" id="Seg_7164" s="T593">ej</ta>
            <ta e="T595" id="Seg_7165" s="T594">ka-lia-m</ta>
            <ta e="T596" id="Seg_7166" s="T595">a</ta>
            <ta e="T597" id="Seg_7167" s="T596">ia</ta>
            <ta e="T598" id="Seg_7168" s="T597">ej</ta>
            <ta e="T599" id="Seg_7169" s="T598">ka-lia-m</ta>
            <ta e="T600" id="Seg_7170" s="T599">dĭ</ta>
            <ta e="T601" id="Seg_7171" s="T600">ej</ta>
            <ta e="T602" id="Seg_7172" s="T601">kuvas</ta>
            <ta e="T603" id="Seg_7173" s="T602">üjü</ta>
            <ta e="T604" id="Seg_7174" s="T603">üjü-t</ta>
            <ta e="T605" id="Seg_7175" s="T604">bar</ta>
            <ta e="T607" id="Seg_7176" s="T606">păjdʼaŋ</ta>
            <ta e="T609" id="Seg_7177" s="T608">muʔzen-də</ta>
            <ta e="T610" id="Seg_7178" s="T609">kömə</ta>
            <ta e="T611" id="Seg_7179" s="T610">šĭke-t</ta>
            <ta e="T612" id="Seg_7180" s="T611">ej</ta>
            <ta e="T613" id="Seg_7181" s="T612">dʼăbaktər-ia</ta>
            <ta e="T614" id="Seg_7182" s="T613">ej</ta>
            <ta e="T615" id="Seg_7183" s="T614">jakše</ta>
            <ta e="T616" id="Seg_7184" s="T615">dʼăbaktər-ia</ta>
            <ta e="T617" id="Seg_7185" s="T616">ku-zaŋ-də</ta>
            <ta e="T618" id="Seg_7186" s="T617">ej</ta>
            <ta e="T619" id="Seg_7187" s="T618">nʼilgö-lia-ʔjə</ta>
            <ta e="T620" id="Seg_7188" s="T619">a</ta>
            <ta e="T621" id="Seg_7189" s="T620">ia-m</ta>
            <ta e="T622" id="Seg_7190" s="T621">măn-də</ta>
            <ta e="T623" id="Seg_7191" s="T622">kan-a-ʔ</ta>
            <ta e="T624" id="Seg_7192" s="T623">dĭ-n</ta>
            <ta e="T625" id="Seg_7193" s="T624">tüžöj</ta>
            <ta e="T626" id="Seg_7194" s="T625">i-ge</ta>
            <ta e="T627" id="Seg_7195" s="T626">i</ta>
            <ta e="T628" id="Seg_7196" s="T627">ine</ta>
            <ta e="T629" id="Seg_7197" s="T628">i-ge</ta>
            <ta e="T630" id="Seg_7198" s="T629">i</ta>
            <ta e="T631" id="Seg_7199" s="T630">ipek</ta>
            <ta e="T632" id="Seg_7200" s="T631">iʔgö</ta>
            <ta e="T633" id="Seg_7201" s="T632">a</ta>
            <ta e="T634" id="Seg_7202" s="T633">măn</ta>
            <ta e="T635" id="Seg_7203" s="T634">măn</ta>
            <ta e="T636" id="Seg_7204" s="T635">măm-bia-m</ta>
            <ta e="T637" id="Seg_7205" s="T636">măna</ta>
            <ta e="T638" id="Seg_7206" s="T637">ine-zi</ta>
            <ta e="T639" id="Seg_7207" s="T638">ej</ta>
            <ta e="T640" id="Seg_7208" s="T639">amno-sʼtə</ta>
            <ta e="T641" id="Seg_7209" s="T640">i</ta>
            <ta e="T642" id="Seg_7210" s="T641">tüžöj-də</ta>
            <ta e="T643" id="Seg_7211" s="T642">ej</ta>
            <ta e="T644" id="Seg_7212" s="T643">amno-sʼtə</ta>
            <ta e="T645" id="Seg_7213" s="T644">nada</ta>
            <ta e="T646" id="Seg_7214" s="T645">tănan</ta>
            <ta e="T647" id="Seg_7215" s="T646">dăk</ta>
            <ta e="T648" id="Seg_7216" s="T647">tăn</ta>
            <ta e="T649" id="Seg_7217" s="T648">tănan</ta>
            <ta e="T650" id="Seg_7218" s="T649">kereʔ</ta>
            <ta e="T651" id="Seg_7219" s="T650">dak</ta>
            <ta e="T652" id="Seg_7220" s="T651">tăn</ta>
            <ta e="T653" id="Seg_7221" s="T652">kan-a-ʔ</ta>
            <ta e="T654" id="Seg_7222" s="T653">tanitsa-gən</ta>
            <ta e="T655" id="Seg_7223" s="T654">šo-bi</ta>
            <ta e="T656" id="Seg_7224" s="T655">onʼiʔ</ta>
            <ta e="T657" id="Seg_7225" s="T656">tibi</ta>
            <ta e="T658" id="Seg_7226" s="T657">dĭ-m</ta>
            <ta e="T659" id="Seg_7227" s="T658">ne</ta>
            <ta e="T660" id="Seg_7228" s="T659">barəʔ-luʔ-pi</ta>
            <ta e="T661" id="Seg_7229" s="T660">dĭ</ta>
            <ta e="T662" id="Seg_7230" s="T661">davaj</ta>
            <ta e="T663" id="Seg_7231" s="T662">măna</ta>
            <ta e="T664" id="Seg_7232" s="T663">monoʔko-sʼtə</ta>
            <ta e="T665" id="Seg_7233" s="T664">ia-m</ta>
            <ta e="T666" id="Seg_7234" s="T665">stal</ta>
            <ta e="T667" id="Seg_7235" s="T666">mĭnzer-zittə</ta>
            <ta e="T668" id="Seg_7236" s="T667">uja</ta>
            <ta e="T669" id="Seg_7237" s="T668">dĭ-zem</ta>
            <ta e="T671" id="Seg_7238" s="T670">bădə-sʼtə</ta>
            <ta e="T672" id="Seg_7239" s="T671">a</ta>
            <ta e="T673" id="Seg_7240" s="T672">măn</ta>
            <ta e="T674" id="Seg_7241" s="T673">măl-lia-m</ta>
            <ta e="T675" id="Seg_7242" s="T674">kan-zittə</ta>
            <ta e="T676" id="Seg_7243" s="T675">ilʼi</ta>
            <ta e="T677" id="Seg_7244" s="T676">ej</ta>
            <ta e="T678" id="Seg_7245" s="T677">kan-zittə</ta>
            <ta e="T680" id="Seg_7246" s="T679">kan-a-ʔ</ta>
            <ta e="T682" id="Seg_7247" s="T681">i-ʔ</ta>
            <ta e="T683" id="Seg_7248" s="T682">kan-a-ʔ</ta>
            <ta e="T684" id="Seg_7249" s="T683">tăn</ta>
            <ta e="T685" id="Seg_7250" s="T684">tüjö</ta>
            <ta e="T687" id="Seg_7251" s="T686">bos-tə</ta>
            <ta e="T688" id="Seg_7252" s="T687">urgo</ta>
            <ta e="T689" id="Seg_7253" s="T688">bos-tə</ta>
            <ta e="T690" id="Seg_7254" s="T689">tĭmne-l</ta>
            <ta e="T691" id="Seg_7255" s="T690">ĭmbi</ta>
            <ta e="T692" id="Seg_7256" s="T691">a-zittə</ta>
            <ta e="T693" id="Seg_7257" s="T692">dĭgəttə</ta>
            <ta e="T694" id="Seg_7258" s="T693">dĭ</ta>
            <ta e="T695" id="Seg_7259" s="T694">tibi</ta>
            <ta e="T697" id="Seg_7260" s="T696">šo-bi</ta>
            <ta e="T698" id="Seg_7261" s="T697">măna</ta>
            <ta e="T699" id="Seg_7262" s="T698">amno-bi</ta>
            <ta e="T700" id="Seg_7263" s="T699">i</ta>
            <ta e="T701" id="Seg_7264" s="T700">davaj</ta>
            <ta e="T702" id="Seg_7265" s="T701">monoʔko-sʼtə</ta>
            <ta e="T703" id="Seg_7266" s="T702">bos-tə</ta>
            <ta e="T704" id="Seg_7267" s="T703">i</ta>
            <ta e="T705" id="Seg_7268" s="T704">davaj</ta>
            <ta e="T706" id="Seg_7269" s="T705">kudaj-də</ta>
            <ta e="T707" id="Seg_7270" s="T706">numan üzə-sʼtə</ta>
            <ta e="T708" id="Seg_7271" s="T707">ej</ta>
            <ta e="T709" id="Seg_7272" s="T708">baʔ-li-m</ta>
            <ta e="T710" id="Seg_7273" s="T709">tănan</ta>
            <ta e="T711" id="Seg_7274" s="T710">a</ta>
            <ta e="T712" id="Seg_7275" s="T711">muno-la-m</ta>
            <ta e="T713" id="Seg_7276" s="T712">jakše</ta>
            <ta e="T714" id="Seg_7277" s="T713">amno-lu-baʔ</ta>
            <ta e="T715" id="Seg_7278" s="T714">dĭgəttə</ta>
            <ta e="T716" id="Seg_7279" s="T715">dĭ</ta>
            <ta e="T717" id="Seg_7280" s="T716">ine</ta>
            <ta e="T718" id="Seg_7281" s="T717">körer-bi</ta>
            <ta e="T720" id="Seg_7282" s="T719">miʔ</ta>
            <ta e="T721" id="Seg_7283" s="T720">măn</ta>
            <ta e="T722" id="Seg_7284" s="T721">dĭ-zi</ta>
            <ta e="T723" id="Seg_7285" s="T722">kam-bia-m</ta>
            <ta e="T724" id="Seg_7286" s="T723">Permʼakov-tə</ta>
            <ta e="T725" id="Seg_7287" s="T724">ara</ta>
            <ta e="T726" id="Seg_7288" s="T725">i-bie-m</ta>
            <ta e="T727" id="Seg_7289" s="T726">deʔ-pi-beʔ</ta>
            <ta e="T730" id="Seg_7290" s="T729">stol-də</ta>
            <ta e="T731" id="Seg_7291" s="T730">oʔbdə-bi-beʔ</ta>
            <ta e="T732" id="Seg_7292" s="T731">il</ta>
            <ta e="T733" id="Seg_7293" s="T732">oʔbdə-bi-beʔ</ta>
            <ta e="T734" id="Seg_7294" s="T733">ara</ta>
            <ta e="T735" id="Seg_7295" s="T734">bĭʔ-pi-ʔi</ta>
            <ta e="T736" id="Seg_7296" s="T735">am-bi-ʔi</ta>
            <ta e="T737" id="Seg_7297" s="T736">dĭgəttə</ta>
            <ta e="T738" id="Seg_7298" s="T737">miʔ</ta>
            <ta e="T740" id="Seg_7299" s="T739">kam-bi-baʔ</ta>
            <ta e="T741" id="Seg_7300" s="T740">stanitsa-nə</ta>
            <ta e="T742" id="Seg_7301" s="T741">dĭgəttə</ta>
            <ta e="T743" id="Seg_7302" s="T742">mano-bia-m</ta>
            <ta e="T744" id="Seg_7303" s="T743">nagur</ta>
            <ta e="T745" id="Seg_7304" s="T744">kö</ta>
            <ta e="T746" id="Seg_7305" s="T745">dĭgəttə</ta>
            <ta e="T747" id="Seg_7306" s="T746">văjna</ta>
            <ta e="T748" id="Seg_7307" s="T747">i-bi</ta>
            <ta e="T749" id="Seg_7308" s="T748">dĭ-m</ta>
            <ta e="T750" id="Seg_7309" s="T749">i-bi-ʔi</ta>
            <ta e="T751" id="Seg_7310" s="T750">văjna-nə</ta>
            <ta e="T752" id="Seg_7311" s="T751">dĭgəttə</ta>
            <ta e="T753" id="Seg_7312" s="T752">măn</ta>
            <ta e="T754" id="Seg_7313" s="T753">nanəʔzəbi</ta>
            <ta e="T755" id="Seg_7314" s="T754">i-bie-m</ta>
            <ta e="T756" id="Seg_7315" s="T755">koʔbdo</ta>
            <ta e="T757" id="Seg_7316" s="T756">dĭ</ta>
            <ta e="T758" id="Seg_7317" s="T757">dĭgəttə</ta>
            <ta e="T759" id="Seg_7318" s="T758">dĭ</ta>
            <ta e="T760" id="Seg_7319" s="T759">šo-bi</ta>
            <ta e="T761" id="Seg_7320" s="T760">văjna-gə</ta>
            <ta e="T763" id="Seg_7321" s="T762">i-bi</ta>
            <ta e="T764" id="Seg_7322" s="T763">dĭgəttə</ta>
            <ta e="T765" id="Seg_7323" s="T764">iššo</ta>
            <ta e="T766" id="Seg_7324" s="T765">amno-bi-beʔ</ta>
            <ta e="T767" id="Seg_7325" s="T766">măn</ta>
            <ta e="T768" id="Seg_7326" s="T767">bazo</ta>
            <ta e="T769" id="Seg_7327" s="T768">nanəʔzəbi</ta>
            <ta e="T770" id="Seg_7328" s="T769">i-bi</ta>
            <ta e="T771" id="Seg_7329" s="T770">dĭgəttə</ta>
            <ta e="T772" id="Seg_7330" s="T771">dĭ</ta>
            <ta e="T774" id="Seg_7331" s="T773">dĭ</ta>
            <ta e="T775" id="Seg_7332" s="T774">măna</ta>
            <ta e="T776" id="Seg_7333" s="T775">sürer-bi</ta>
            <ta e="T777" id="Seg_7334" s="T776">i</ta>
            <ta e="T778" id="Seg_7335" s="T777">dĭ-m</ta>
            <ta e="T779" id="Seg_7336" s="T778">i-bi</ta>
            <ta e="T780" id="Seg_7337" s="T779">iššo</ta>
            <ta e="T781" id="Seg_7338" s="T780">onʼiʔ</ta>
            <ta e="T783" id="Seg_7339" s="T782">nʼi</ta>
            <ta e="T784" id="Seg_7340" s="T783">deʔ-pie-m</ta>
            <ta e="T785" id="Seg_7341" s="T784">dĭgəttə</ta>
            <ta e="T786" id="Seg_7342" s="T785">maʔ-nʼi</ta>
            <ta e="T787" id="Seg_7343" s="T786">šo-bia-m</ta>
            <ta e="T790" id="Seg_7344" s="T789">aba-nə</ta>
            <ta e="T791" id="Seg_7345" s="T790">i</ta>
            <ta e="T792" id="Seg_7346" s="T791">ia-nə</ta>
            <ta e="T793" id="Seg_7347" s="T792">dĭgəttə</ta>
            <ta e="T794" id="Seg_7348" s="T793">kü-laːm-bi-ʔi</ta>
            <ta e="T795" id="Seg_7349" s="T794">koʔbdo</ta>
            <ta e="T796" id="Seg_7350" s="T795">i</ta>
            <ta e="T797" id="Seg_7351" s="T796">nʼi</ta>
            <ta e="T798" id="Seg_7352" s="T797">măn</ta>
            <ta e="T799" id="Seg_7353" s="T798">unnʼa</ta>
            <ta e="T801" id="Seg_7354" s="T800">mo-lam-bia-m</ta>
            <ta e="T802" id="Seg_7355" s="T801">oʔkʼe</ta>
            <ta e="T803" id="Seg_7356" s="T802">amno-bia-m</ta>
            <ta e="T804" id="Seg_7357" s="T803">bazo</ta>
            <ta e="T805" id="Seg_7358" s="T804">kuza</ta>
            <ta e="T806" id="Seg_7359" s="T805">măna</ta>
            <ta e="T807" id="Seg_7360" s="T806">monoʔko-bi</ta>
            <ta e="T808" id="Seg_7361" s="T807">măn</ta>
            <ta e="T809" id="Seg_7362" s="T808">kam-bia-m</ta>
            <ta e="T810" id="Seg_7363" s="T809">dĭ-zi</ta>
            <ta e="T811" id="Seg_7364" s="T810">amno-bia-m</ta>
            <ta e="T812" id="Seg_7365" s="T811">oʔb</ta>
            <ta e="T813" id="Seg_7366" s="T812">šide</ta>
            <ta e="T814" id="Seg_7367" s="T813">nagur</ta>
            <ta e="T815" id="Seg_7368" s="T814">teʔtə</ta>
            <ta e="T816" id="Seg_7369" s="T815">sumna</ta>
            <ta e="T817" id="Seg_7370" s="T816">muktuʔ</ta>
            <ta e="T818" id="Seg_7371" s="T817">sejʔpü</ta>
            <ta e="T819" id="Seg_7372" s="T818">kö</ta>
            <ta e="T820" id="Seg_7373" s="T819">amno-bia-m</ta>
            <ta e="T821" id="Seg_7374" s="T820">šide</ta>
            <ta e="T822" id="Seg_7375" s="T821">ešši</ta>
            <ta e="T823" id="Seg_7376" s="T822">i-bi-ʔi</ta>
            <ta e="T824" id="Seg_7377" s="T823">ugandə</ta>
            <ta e="T825" id="Seg_7378" s="T824">ej</ta>
            <ta e="T826" id="Seg_7379" s="T825">jakše</ta>
            <ta e="T827" id="Seg_7380" s="T826">ara</ta>
            <ta e="T828" id="Seg_7381" s="T827">bĭʔ-pi</ta>
            <ta e="T829" id="Seg_7382" s="T828">dʼabro-laʔ-pi</ta>
            <ta e="T830" id="Seg_7383" s="T829">kudo-nz-laʔ-pi</ta>
            <ta e="T831" id="Seg_7384" s="T830">kamen</ta>
            <ta e="T832" id="Seg_7385" s="T831">kamen</ta>
            <ta e="T833" id="Seg_7386" s="T832">baʔ-luʔ-pia-m</ta>
            <ta e="T834" id="Seg_7387" s="T833">bar</ta>
            <ta e="T835" id="Seg_7388" s="T834">ulu-m</ta>
            <ta e="T836" id="Seg_7389" s="T835">bar</ta>
            <ta e="T837" id="Seg_7390" s="T836">eʔbdə-m</ta>
            <ta e="T838" id="Seg_7391" s="T837">saj-nʼeʔ-pi</ta>
            <ta e="T839" id="Seg_7392" s="T838">vekžen</ta>
            <ta e="T840" id="Seg_7393" s="T839">tăŋ</ta>
            <ta e="T841" id="Seg_7394" s="T840">münör-bi</ta>
            <ta e="T842" id="Seg_7395" s="T841">ažnə</ta>
            <ta e="T843" id="Seg_7396" s="T842">kem</ta>
            <ta e="T844" id="Seg_7397" s="T843">i-bi</ta>
            <ta e="T845" id="Seg_7398" s="T844">dĭgəttə</ta>
            <ta e="T846" id="Seg_7399" s="T845">măn</ta>
            <ta e="T847" id="Seg_7400" s="T846">nʼi-m</ta>
            <ta e="T848" id="Seg_7401" s="T847">dĭ-m</ta>
            <ta e="T849" id="Seg_7402" s="T848">barəʔ-luʔ-pia-m</ta>
            <ta e="T850" id="Seg_7403" s="T849">măn</ta>
            <ta e="T851" id="Seg_7404" s="T850">onʼiʔ</ta>
            <ta e="T852" id="Seg_7405" s="T851">nʼi-m</ta>
            <ta e="T853" id="Seg_7406" s="T852">i-bi</ta>
            <ta e="T854" id="Seg_7407" s="T853">tüj</ta>
            <ta e="T855" id="Seg_7408" s="T854">măn-də-m</ta>
            <ta e="T856" id="Seg_7409" s="T855">šindi-nə=də</ta>
            <ta e="T858" id="Seg_7410" s="T856">ej</ta>
            <ta e="T859" id="Seg_7411" s="T858">tibi-nə</ta>
            <ta e="T860" id="Seg_7412" s="T859">unnʼa</ta>
            <ta e="T861" id="Seg_7413" s="T860">amno-la-m</ta>
            <ta e="T862" id="Seg_7414" s="T861">nʼi-zi</ta>
            <ta e="T863" id="Seg_7415" s="T862">bos-tə</ta>
            <ta e="T864" id="Seg_7416" s="T863">nʼi-zi</ta>
            <ta e="T865" id="Seg_7417" s="T864">dĭgəttə</ta>
            <ta e="T866" id="Seg_7418" s="T865">onʼiʔ</ta>
            <ta e="T867" id="Seg_7419" s="T866">nʼi</ta>
            <ta e="T868" id="Seg_7420" s="T867">davaj</ta>
            <ta e="T869" id="Seg_7421" s="T868">măna</ta>
            <ta e="T870" id="Seg_7422" s="T869">monoʔko-sʼtə</ta>
            <ta e="T871" id="Seg_7423" s="T870">măn</ta>
            <ta e="T875" id="Seg_7424" s="T874">ej</ta>
            <ta e="T876" id="Seg_7425" s="T875">kuvando-lia-m</ta>
            <ta e="T877" id="Seg_7426" s="T876">kadəl-bə</ta>
            <ta e="T878" id="Seg_7427" s="T877">kadəl-də</ta>
            <ta e="T879" id="Seg_7428" s="T878">nʼišpək</ta>
            <ta e="T880" id="Seg_7429" s="T879">ej</ta>
            <ta e="T881" id="Seg_7430" s="T880">ka-la-m</ta>
            <ta e="T882" id="Seg_7431" s="T881">dĭ</ta>
            <ta e="T883" id="Seg_7432" s="T882">măn-də</ta>
            <ta e="T884" id="Seg_7433" s="T883">davaj</ta>
            <ta e="T885" id="Seg_7434" s="T884">amno-sʼtə</ta>
            <ta e="T886" id="Seg_7435" s="T885">tăn-zi</ta>
            <ta e="T1055" id="Seg_7436" s="T886">nu</ta>
            <ta e="T887" id="Seg_7437" s="T1055">tenö-bia-m</ta>
            <ta e="T888" id="Seg_7438" s="T887">tenö-bia-m</ta>
            <ta e="T889" id="Seg_7439" s="T888">dĭgəttə</ta>
            <ta e="T890" id="Seg_7440" s="T889">kam-bia-m</ta>
            <ta e="T891" id="Seg_7441" s="T890">dĭgəttə</ta>
            <ta e="T892" id="Seg_7442" s="T891">bazo</ta>
            <ta e="T894" id="Seg_7443" s="T893">barəʔ-li-m</ta>
            <ta e="T895" id="Seg_7444" s="T894">măn-də-m</ta>
            <ta e="T896" id="Seg_7445" s="T895">tănan</ta>
            <ta e="T897" id="Seg_7446" s="T896">ej</ta>
            <ta e="T898" id="Seg_7447" s="T897">amno-la-m</ta>
            <ta e="T899" id="Seg_7448" s="T898">tăn-zi</ta>
            <ta e="T900" id="Seg_7449" s="T899">dibər</ta>
            <ta e="T901" id="Seg_7450" s="T900">davaj</ta>
            <ta e="T902" id="Seg_7451" s="T901">dʼor-zittə</ta>
            <ta e="T903" id="Seg_7452" s="T902">tăŋ</ta>
            <ta e="T904" id="Seg_7453" s="T903">dʼor-bi</ta>
            <ta e="T905" id="Seg_7454" s="T904">ugandə</ta>
            <ta e="T906" id="Seg_7455" s="T905">dĭgəttə</ta>
            <ta e="T907" id="Seg_7456" s="T906">măn</ta>
            <ta e="T908" id="Seg_7457" s="T907">măl-lia-m</ta>
            <ta e="T910" id="Seg_7458" s="T909">tura</ta>
            <ta e="T911" id="Seg_7459" s="T910">nuldə-bə-j</ta>
            <ta e="T912" id="Seg_7460" s="T911">dĭgəttə</ta>
            <ta e="T913" id="Seg_7461" s="T912">pušaj</ta>
            <ta e="T914" id="Seg_7462" s="T913">ka-lə-j</ta>
            <ta e="T915" id="Seg_7463" s="T914">i</ta>
            <ta e="T916" id="Seg_7464" s="T915">dăre</ta>
            <ta e="T917" id="Seg_7465" s="T916">i-bi</ta>
            <ta e="T918" id="Seg_7466" s="T917">tura</ta>
            <ta e="T920" id="Seg_7467" s="T919">nuldə-bi</ta>
            <ta e="T921" id="Seg_7468" s="T920">i</ta>
            <ta e="T1058" id="Seg_7469" s="T921">kal-la</ta>
            <ta e="T922" id="Seg_7470" s="T1058">dʼür-bi</ta>
            <ta e="T923" id="Seg_7471" s="T922">măn</ta>
            <ta e="T924" id="Seg_7472" s="T923">unnʼa</ta>
            <ta e="T925" id="Seg_7473" s="T924">maːluʔpiam</ta>
            <ta e="T926" id="Seg_7474" s="T925">tüj</ta>
            <ta e="T927" id="Seg_7475" s="T926">măn</ta>
            <ta e="T928" id="Seg_7476" s="T927">üge</ta>
            <ta e="T929" id="Seg_7477" s="T928">unnʼa</ta>
            <ta e="T930" id="Seg_7478" s="T929">amno-laʔbə-m</ta>
            <ta e="T931" id="Seg_7479" s="T930">amno-laʔbə-m</ta>
            <ta e="T932" id="Seg_7480" s="T931">už</ta>
            <ta e="T933" id="Seg_7481" s="T932">šide</ta>
            <ta e="T934" id="Seg_7482" s="T933">bʼeʔ</ta>
            <ta e="T935" id="Seg_7483" s="T934">ki</ta>
            <ta e="T936" id="Seg_7484" s="T935">amno-laʔbə-m</ta>
            <ta e="T937" id="Seg_7485" s="T936">kamən</ta>
            <ta e="T938" id="Seg_7486" s="T937">dĭ</ta>
            <ta e="T939" id="Seg_7487" s="T938">barəʔ-luʔ-pi</ta>
            <ta e="T940" id="Seg_7488" s="T939">dĭgəttə</ta>
            <ta e="T941" id="Seg_7489" s="T940">miʔ</ta>
            <ta e="T942" id="Seg_7490" s="T941">dĭ</ta>
            <ta e="T943" id="Seg_7491" s="T942">nʼi-zi</ta>
            <ta e="T944" id="Seg_7492" s="T943">bar</ta>
            <ta e="T945" id="Seg_7493" s="T944">pa</ta>
            <ta e="T946" id="Seg_7494" s="T945">baltu-zi</ta>
            <ta e="T947" id="Seg_7495" s="T946">jaʔ-pi-baʔ</ta>
            <ta e="T948" id="Seg_7496" s="T947">tura</ta>
            <ta e="T950" id="Seg_7497" s="T949">nuldə-zittə</ta>
            <ta e="T952" id="Seg_7498" s="T951">tažer-bi-baʔ</ta>
            <ta e="T954" id="Seg_7499" s="T953">ine-zi</ta>
            <ta e="T955" id="Seg_7500" s="T954">i</ta>
            <ta e="T956" id="Seg_7501" s="T955">tura</ta>
            <ta e="T957" id="Seg_7502" s="T956">nuldə-bi-baʔ</ta>
            <ta e="T958" id="Seg_7503" s="T957">tibi-zeŋ</ta>
            <ta e="T959" id="Seg_7504" s="T958">jaʔ-pi-ʔi</ta>
            <ta e="T960" id="Seg_7505" s="T959">baltu-zi</ta>
            <ta e="T961" id="Seg_7506" s="T960">a</ta>
            <ta e="T962" id="Seg_7507" s="T961">măn</ta>
            <ta e="T963" id="Seg_7508" s="T962">kuba-ʔi</ta>
            <ta e="T964" id="Seg_7509" s="T963">dĭʔ-nə</ta>
            <ta e="T965" id="Seg_7510" s="T964">a-bia-m</ta>
            <ta e="T966" id="Seg_7511" s="T965">dĭgəttə</ta>
            <ta e="T967" id="Seg_7512" s="T966">tsutonka-ʔi</ta>
            <ta e="T969" id="Seg_7513" s="T968">tažer-bi-ʔi</ta>
            <ta e="T971" id="Seg_7514" s="T970">a-zittə</ta>
            <ta e="T972" id="Seg_7515" s="T971">i</ta>
            <ta e="T974" id="Seg_7516" s="T973">a-zittə</ta>
            <ta e="T975" id="Seg_7517" s="T974">a</ta>
            <ta e="T976" id="Seg_7518" s="T975">măn</ta>
            <ta e="T977" id="Seg_7519" s="T976">kuda</ta>
            <ta e="T978" id="Seg_7520" s="T977">urgo</ta>
            <ta e="T979" id="Seg_7521" s="T978">kuba-ʔi</ta>
            <ta e="T980" id="Seg_7522" s="T979">a-bia-m</ta>
            <ta e="T981" id="Seg_7523" s="T980">i</ta>
            <ta e="T982" id="Seg_7524" s="T981">jama-ʔi</ta>
            <ta e="T983" id="Seg_7525" s="T982">šöʔ-pie-m</ta>
            <ta e="T984" id="Seg_7526" s="T983">a</ta>
            <ta e="T985" id="Seg_7527" s="T984">dĭ</ta>
            <ta e="T986" id="Seg_7528" s="T985">tože</ta>
            <ta e="T987" id="Seg_7529" s="T986">măna</ta>
            <ta e="T988" id="Seg_7530" s="T987">kabaržə-bi</ta>
            <ta e="T989" id="Seg_7531" s="T988">kuba-t</ta>
            <ta e="T990" id="Seg_7532" s="T989">edə-m-bi</ta>
            <ta e="T991" id="Seg_7533" s="T990">da</ta>
            <ta e="T992" id="Seg_7534" s="T991">ej</ta>
            <ta e="T993" id="Seg_7535" s="T992">tĭmne-bi</ta>
            <ta e="T994" id="Seg_7536" s="T993">măn</ta>
            <ta e="T995" id="Seg_7537" s="T994">dĭ-m</ta>
            <ta e="T996" id="Seg_7538" s="T995">tüšə-l-bie-m</ta>
            <ta e="T997" id="Seg_7539" s="T996">măn</ta>
            <ta e="T998" id="Seg_7540" s="T997">bos-tə</ta>
            <ta e="T1000" id="Seg_7541" s="T999">tibi-nə</ta>
            <ta e="T1001" id="Seg_7542" s="T1000">jama-ʔi</ta>
            <ta e="T1002" id="Seg_7543" s="T1001">šöʔ-pie-m</ta>
            <ta e="T1003" id="Seg_7544" s="T1002">i</ta>
            <ta e="T1004" id="Seg_7545" s="T1003">parga</ta>
            <ta e="T1005" id="Seg_7546" s="T1004">šöʔ-pie-m</ta>
            <ta e="T1006" id="Seg_7547" s="T1005">ular-ən</ta>
            <ta e="T1007" id="Seg_7548" s="T1006">kuba-ʔi</ta>
            <ta e="T1008" id="Seg_7549" s="T1007">i</ta>
            <ta e="T1009" id="Seg_7550" s="T1008">üžü</ta>
            <ta e="T1010" id="Seg_7551" s="T1009">šöʔ-pie-m</ta>
            <ta e="T1011" id="Seg_7552" s="T1010">kujnek-tə</ta>
            <ta e="T1012" id="Seg_7553" s="T1011">šöʔ-pie-m</ta>
            <ta e="T1013" id="Seg_7554" s="T1012">piʔme-ʔi</ta>
            <ta e="T1014" id="Seg_7555" s="T1013">šöʔ-pie-m</ta>
            <ta e="T1015" id="Seg_7556" s="T1014">dĭʔ-nə</ta>
            <ta e="T1016" id="Seg_7557" s="T1015">šer-bia-m</ta>
            <ta e="T1017" id="Seg_7558" s="T1016">ugandə</ta>
            <ta e="T1056" id="Seg_7559" s="T1017">jakše</ta>
            <ta e="T1019" id="Seg_7560" s="T1018">šer-bie-m</ta>
            <ta e="T1021" id="Seg_7561" s="T1020">il</ta>
            <ta e="T1022" id="Seg_7562" s="T1021">bar</ta>
            <ta e="T1023" id="Seg_7563" s="T1022">măndə-r-bi-ʔi</ta>
            <ta e="T1024" id="Seg_7564" s="T1023">ugandə</ta>
            <ta e="T1025" id="Seg_7565" s="T1024">kuvas</ta>
            <ta e="T1026" id="Seg_7566" s="T1025">kuza</ta>
            <ta e="T1027" id="Seg_7567" s="T1026">mo-lam-bi</ta>
            <ta e="T1028" id="Seg_7568" s="T1027">dĭgəttə</ta>
            <ta e="T1029" id="Seg_7569" s="T1028">miʔ</ta>
            <ta e="T1030" id="Seg_7570" s="T1029">dʼü</ta>
            <ta e="T1033" id="Seg_7571" s="T1032">tarir-laʔ-pi-baʔ</ta>
            <ta e="T1034" id="Seg_7572" s="T1033">aš</ta>
            <ta e="T1035" id="Seg_7573" s="T1034">kuʔ-pi-baʔ</ta>
            <ta e="T1037" id="Seg_7574" s="T1036">özer-bi</ta>
            <ta e="T1038" id="Seg_7575" s="T1037">ugandə</ta>
            <ta e="T1039" id="Seg_7576" s="T1038">jakše</ta>
            <ta e="T1040" id="Seg_7577" s="T1039">i-bi</ta>
            <ta e="T1041" id="Seg_7578" s="T1040">aš</ta>
            <ta e="T1042" id="Seg_7579" s="T1041">dĭgəttə</ta>
            <ta e="T1043" id="Seg_7580" s="T1042">un</ta>
            <ta e="T1044" id="Seg_7581" s="T1043">a-bi-baʔ</ta>
            <ta e="T1045" id="Seg_7582" s="T1044">tʼermən-də</ta>
            <ta e="T1046" id="Seg_7583" s="T1045">mĭm-bi-beʔ</ta>
            <ta e="T1047" id="Seg_7584" s="T1046">ipek</ta>
            <ta e="T1048" id="Seg_7585" s="T1047">pür-bi-beʔ</ta>
            <ta e="T1049" id="Seg_7586" s="T1048">i</ta>
            <ta e="T1050" id="Seg_7587" s="T1049">budəj</ta>
            <ta e="T1051" id="Seg_7588" s="T1050">aš</ta>
            <ta e="T1052" id="Seg_7589" s="T1051">kuʔ-pi-baʔ</ta>
            <ta e="T1053" id="Seg_7590" s="T1052">jakšə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_7591" s="T0">tăn</ta>
            <ta e="T2" id="Seg_7592" s="T1">taldʼen</ta>
            <ta e="T4" id="Seg_7593" s="T3">kajaʔ</ta>
            <ta e="T5" id="Seg_7594" s="T4">i-bi-l</ta>
            <ta e="T6" id="Seg_7595" s="T5">tak</ta>
            <ta e="T7" id="Seg_7596" s="T6">edə-bi-l</ta>
            <ta e="T9" id="Seg_7597" s="T8">edə-bi-m</ta>
            <ta e="T10" id="Seg_7598" s="T9">a</ta>
            <ta e="T11" id="Seg_7599" s="T10">kumən</ta>
            <ta e="T12" id="Seg_7600" s="T11">sumna</ta>
            <ta e="T13" id="Seg_7601" s="T12">gram</ta>
            <ta e="T14" id="Seg_7602" s="T13">aktʼa</ta>
            <ta e="T15" id="Seg_7603" s="T14">det-bi-l</ta>
            <ta e="T16" id="Seg_7604" s="T15">det-bi-m</ta>
            <ta e="T17" id="Seg_7605" s="T16">šide</ta>
            <ta e="T19" id="Seg_7606" s="T18">šide</ta>
            <ta e="T20" id="Seg_7607" s="T19">šălkovej</ta>
            <ta e="T21" id="Seg_7608" s="T20">tʼăbaktər-laʔbə-bAʔ</ta>
            <ta e="T22" id="Seg_7609" s="T21">i</ta>
            <ta e="T23" id="Seg_7610" s="T22">kakənar-laʔbə-bAʔ</ta>
            <ta e="T24" id="Seg_7611" s="T23">i</ta>
            <ta e="T26" id="Seg_7612" s="T25">ara</ta>
            <ta e="T27" id="Seg_7613" s="T26">bĭs-laʔbə-bAʔ</ta>
            <ta e="T28" id="Seg_7614" s="T27">i</ta>
            <ta e="T29" id="Seg_7615" s="T28">ipek</ta>
            <ta e="T30" id="Seg_7616" s="T29">am-liA</ta>
            <ta e="T31" id="Seg_7617" s="T30">i</ta>
            <ta e="T32" id="Seg_7618" s="T31">uja</ta>
            <ta e="T33" id="Seg_7619" s="T32">am-laʔbə-bAʔ</ta>
            <ta e="T34" id="Seg_7620" s="T33">i</ta>
            <ta e="T35" id="Seg_7621" s="T34">taŋgu</ta>
            <ta e="T36" id="Seg_7622" s="T35">nʼeʔbdə-laʔbə-bAʔ</ta>
            <ta e="T37" id="Seg_7623" s="T36">bar</ta>
            <ta e="T38" id="Seg_7624" s="T37">miʔ</ta>
            <ta e="T39" id="Seg_7625" s="T38">toʔ-gəndə</ta>
            <ta e="T40" id="Seg_7626" s="T39">kuza</ta>
            <ta e="T41" id="Seg_7627" s="T40">amno-laʔbə</ta>
            <ta e="T42" id="Seg_7628" s="T41">dĭ-n</ta>
            <ta e="T43" id="Seg_7629" s="T42">nʼi</ta>
            <ta e="T44" id="Seg_7630" s="T43">miʔnʼibeʔ</ta>
            <ta e="T45" id="Seg_7631" s="T44">šo-bi</ta>
            <ta e="T46" id="Seg_7632" s="T45">jašɨk-Tə</ta>
            <ta e="T47" id="Seg_7633" s="T46">păda-bi</ta>
            <ta e="T48" id="Seg_7634" s="T47">i</ta>
            <ta e="T49" id="Seg_7635" s="T48">škap-Tə</ta>
            <ta e="T50" id="Seg_7636" s="T49">păda-bi</ta>
            <ta e="T51" id="Seg_7637" s="T50">măn</ta>
            <ta e="T52" id="Seg_7638" s="T51">măn-bi-m</ta>
            <ta e="T53" id="Seg_7639" s="T52">măn-bi-m</ta>
            <ta e="T54" id="Seg_7640" s="T53">ĭmbi</ta>
            <ta e="T56" id="Seg_7641" s="T55">măna</ta>
            <ta e="T57" id="Seg_7642" s="T56">Vanʼka</ta>
            <ta e="T59" id="Seg_7643" s="T58">măndo-r-zittə</ta>
            <ta e="T60" id="Seg_7644" s="T59">măn</ta>
            <ta e="T61" id="Seg_7645" s="T60">dĭ-m</ta>
            <ta e="T62" id="Seg_7646" s="T61">sürer-luʔbdə-bi-m</ta>
            <ta e="T63" id="Seg_7647" s="T62">măn-liA</ta>
            <ta e="T64" id="Seg_7648" s="T63">Miša-Tə</ta>
            <ta e="T65" id="Seg_7649" s="T64">kan-ə-ʔ</ta>
            <ta e="T66" id="Seg_7650" s="T65">döʔə</ta>
            <ta e="T67" id="Seg_7651" s="T66">dĭgəttə</ta>
            <ta e="T68" id="Seg_7652" s="T67">teinen</ta>
            <ta e="T69" id="Seg_7653" s="T68">šo-bi</ta>
            <ta e="T70" id="Seg_7654" s="T69">băra</ta>
            <ta e="T71" id="Seg_7655" s="T70">sanə-ziʔ</ta>
            <ta e="T72" id="Seg_7656" s="T71">nu-gA</ta>
            <ta e="T73" id="Seg_7657" s="T72">dĭ</ta>
            <ta e="T74" id="Seg_7658" s="T73">tagaj</ta>
            <ta e="T75" id="Seg_7659" s="T74">i-bi</ta>
            <ta e="T76" id="Seg_7660" s="T75">i</ta>
            <ta e="T77" id="Seg_7661" s="T76">băra</ta>
            <ta e="T78" id="Seg_7662" s="T77">băt-bi</ta>
            <ta e="T79" id="Seg_7663" s="T78">sanə</ta>
            <ta e="T80" id="Seg_7664" s="T79">i-bi</ta>
            <ta e="T81" id="Seg_7665" s="T80">bos-də</ta>
            <ta e="T82" id="Seg_7666" s="T81">barəʔ-luʔbdə-bi</ta>
            <ta e="T83" id="Seg_7667" s="T82">sĭreʔp</ta>
            <ta e="T84" id="Seg_7668" s="T83">miʔ</ta>
            <ta e="T85" id="Seg_7669" s="T84">tura-Kən</ta>
            <ta e="T86" id="Seg_7670" s="T85">ne</ta>
            <ta e="T87" id="Seg_7671" s="T86">nuʔmə-luʔbdə-bi</ta>
            <ta e="T89" id="Seg_7672" s="T88">dĭŋinaʔtə</ta>
            <ta e="T90" id="Seg_7673" s="T89">tojar-bi</ta>
            <ta e="T91" id="Seg_7674" s="T90">sanə</ta>
            <ta e="T92" id="Seg_7675" s="T91">a</ta>
            <ta e="T93" id="Seg_7676" s="T92">dĭ</ta>
            <ta e="T94" id="Seg_7677" s="T93">bar</ta>
            <ta e="T95" id="Seg_7678" s="T94">pʼer-luʔbə</ta>
            <ta e="T96" id="Seg_7679" s="T95">dö</ta>
            <ta e="T97" id="Seg_7680" s="T96">sanə</ta>
            <ta e="T98" id="Seg_7681" s="T97">a</ta>
            <ta e="T99" id="Seg_7682" s="T98">dĭ</ta>
            <ta e="T100" id="Seg_7683" s="T99">măn-bi</ta>
            <ta e="T101" id="Seg_7684" s="T100">măn</ta>
            <ta e="T102" id="Seg_7685" s="T101">dĭ-m</ta>
            <ta e="T103" id="Seg_7686" s="T102">kut-lV-m</ta>
            <ta e="T104" id="Seg_7687" s="T103">a</ta>
            <ta e="T105" id="Seg_7688" s="T104">ĭmbi</ta>
            <ta e="T106" id="Seg_7689" s="T105">ej</ta>
            <ta e="T107" id="Seg_7690" s="T106">măn-bi-lAʔ</ta>
            <ta e="T109" id="Seg_7691" s="T108">taldʼen</ta>
            <ta e="T110" id="Seg_7692" s="T109">măna</ta>
            <ta e="T111" id="Seg_7693" s="T110">kăštə-bi-jəʔ</ta>
            <ta e="T113" id="Seg_7694" s="T112">măn</ta>
            <ta e="T115" id="Seg_7695" s="T114">tugan-də</ta>
            <ta e="T116" id="Seg_7696" s="T115">šo-ʔ</ta>
            <ta e="T117" id="Seg_7697" s="T116">miʔnʼibeʔ</ta>
            <ta e="T118" id="Seg_7698" s="T117">amor-zittə</ta>
            <ta e="T120" id="Seg_7699" s="T119">ešši</ta>
            <ta e="T122" id="Seg_7700" s="T121">kü-laːm-bi</ta>
            <ta e="T123" id="Seg_7701" s="T122">ušonʼiʔkə</ta>
            <ta e="T124" id="Seg_7702" s="T123">kan-bi</ta>
            <ta e="T125" id="Seg_7703" s="T124">a</ta>
            <ta e="T126" id="Seg_7704" s="T125">măn</ta>
            <ta e="T127" id="Seg_7705" s="T126">măn-bi-m</ta>
            <ta e="T128" id="Seg_7706" s="T127">šo-lV-m</ta>
            <ta e="T129" id="Seg_7707" s="T128">a</ta>
            <ta e="T131" id="Seg_7708" s="T130">a</ta>
            <ta e="T132" id="Seg_7709" s="T131">bos-m</ta>
            <ta e="T133" id="Seg_7710" s="T132">ej</ta>
            <ta e="T134" id="Seg_7711" s="T133">kan-bi-m</ta>
            <ta e="T135" id="Seg_7712" s="T134">mej-m</ta>
            <ta e="T136" id="Seg_7713" s="T135">kăštə-bi-m</ta>
            <ta e="T137" id="Seg_7714" s="T136">kăštə-bi-m</ta>
            <ta e="T138" id="Seg_7715" s="T137">dĭ</ta>
            <ta e="T139" id="Seg_7716" s="T138">ej</ta>
            <ta e="T140" id="Seg_7717" s="T139">kan-bi</ta>
            <ta e="T141" id="Seg_7718" s="T140">i</ta>
            <ta e="T142" id="Seg_7719" s="T141">măn</ta>
            <ta e="T143" id="Seg_7720" s="T142">ej</ta>
            <ta e="T144" id="Seg_7721" s="T143">kan-bi-m</ta>
            <ta e="T145" id="Seg_7722" s="T144">dĭn</ta>
            <ta e="T146" id="Seg_7723" s="T145">ugaːndə</ta>
            <ta e="T147" id="Seg_7724" s="T146">ara-j</ta>
            <ta e="T148" id="Seg_7725" s="T147">a</ta>
            <ta e="T149" id="Seg_7726" s="T148">măna</ta>
            <ta e="T150" id="Seg_7727" s="T149">ara</ta>
            <ta e="T151" id="Seg_7728" s="T150">ej</ta>
            <ta e="T152" id="Seg_7729" s="T151">kereʔ</ta>
            <ta e="T153" id="Seg_7730" s="T152">măn</ta>
            <ta e="T156" id="Seg_7731" s="T155">dĭ-m</ta>
            <ta e="T157" id="Seg_7732" s="T156">bĭs-zittə</ta>
            <ta e="T158" id="Seg_7733" s="T157">a</ta>
            <ta e="T159" id="Seg_7734" s="T158">dĭ</ta>
            <ta e="T160" id="Seg_7735" s="T159">kuroː-luʔbdə-bi</ta>
            <ta e="T161" id="Seg_7736" s="T160">bar</ta>
            <ta e="T162" id="Seg_7737" s="T161">măn-ntə</ta>
            <ta e="T163" id="Seg_7738" s="T162">urgaja-m</ta>
            <ta e="T164" id="Seg_7739" s="T163">ažnə</ta>
            <ta e="T165" id="Seg_7740" s="T164">dʼoduni</ta>
            <ta e="T166" id="Seg_7741" s="T165">ku-bi-m</ta>
            <ta e="T167" id="Seg_7742" s="T166">tüj</ta>
            <ta e="T168" id="Seg_7743" s="T167">büzəj-lAʔ</ta>
            <ta e="T169" id="Seg_7744" s="T168">bar</ta>
            <ta e="T170" id="Seg_7745" s="T169">tüšəl-bi</ta>
            <ta e="T171" id="Seg_7746" s="T170">šiʔnʼileʔ</ta>
            <ta e="T172" id="Seg_7747" s="T171">iʔbə-laʔbə</ta>
            <ta e="T173" id="Seg_7748" s="T172">amno-ʔ</ta>
            <ta e="T174" id="Seg_7749" s="T173">iʔbə-laʔbə</ta>
            <ta e="T175" id="Seg_7750" s="T174">ej</ta>
            <ta e="T176" id="Seg_7751" s="T175">kirgaːr-laʔbə</ta>
            <ta e="T177" id="Seg_7752" s="T176">teinen</ta>
            <ta e="T178" id="Seg_7753" s="T177">ejü</ta>
            <ta e="T179" id="Seg_7754" s="T178">mo-laːm-bi</ta>
            <ta e="T180" id="Seg_7755" s="T179">sĭri</ta>
            <ta e="T181" id="Seg_7756" s="T180">bar</ta>
            <ta e="T182" id="Seg_7757" s="T181">nömrunʼiʔ</ta>
            <ta e="T183" id="Seg_7758" s="T182">i-bi</ta>
            <ta e="T184" id="Seg_7759" s="T183">sĭri</ta>
            <ta e="T186" id="Seg_7760" s="T185">büžü</ta>
            <ta e="T187" id="Seg_7761" s="T186">bü</ta>
            <ta e="T188" id="Seg_7762" s="T187">mo-laːm-ləj</ta>
            <ta e="T189" id="Seg_7763" s="T188">dĭgəttə</ta>
            <ta e="T190" id="Seg_7764" s="T189">noʔ</ta>
            <ta e="T191" id="Seg_7765" s="T190">özer-lV-j</ta>
            <ta e="T193" id="Seg_7766" s="T192">svetok-jəʔ</ta>
            <ta e="T194" id="Seg_7767" s="T193">özer-lV-jəʔ</ta>
            <ta e="T195" id="Seg_7768" s="T194">pa-jəʔ</ta>
            <ta e="T196" id="Seg_7769" s="T195">bar</ta>
            <ta e="T197" id="Seg_7770" s="T196">kuvas</ta>
            <ta e="T198" id="Seg_7771" s="T197">mo-lV-jəʔ</ta>
            <ta e="T199" id="Seg_7772" s="T198">zʼelʼona-jəʔ</ta>
            <ta e="T200" id="Seg_7773" s="T199">mo-lV-jəʔ</ta>
            <ta e="T201" id="Seg_7774" s="T200">bar</ta>
            <ta e="T202" id="Seg_7775" s="T201">dĭgəttə</ta>
            <ta e="T203" id="Seg_7776" s="T202">ine-jəʔ</ta>
            <ta e="T204" id="Seg_7777" s="T203">tüžöj-jəʔ</ta>
            <ta e="T205" id="Seg_7778" s="T204">ular-də</ta>
            <ta e="T206" id="Seg_7779" s="T205">kan-lV-jəʔ</ta>
            <ta e="T207" id="Seg_7780" s="T206">noʔ</ta>
            <ta e="T208" id="Seg_7781" s="T207">am-zittə</ta>
            <ta e="T209" id="Seg_7782" s="T208">dĭgəttə</ta>
            <ta e="T210" id="Seg_7783" s="T209">noʔ</ta>
            <ta e="T211" id="Seg_7784" s="T210">urgo</ta>
            <ta e="T212" id="Seg_7785" s="T211">özer-lV-j</ta>
            <ta e="T213" id="Seg_7786" s="T212">dĭgəttə</ta>
            <ta e="T214" id="Seg_7787" s="T213">dĭ-m</ta>
            <ta e="T216" id="Seg_7788" s="T215">šapku-ziʔ</ta>
            <ta e="T217" id="Seg_7789" s="T216">hʼaʔ-zittə</ta>
            <ta e="T218" id="Seg_7790" s="T217">nadə</ta>
            <ta e="T219" id="Seg_7791" s="T218">dĭgəttə</ta>
            <ta e="T220" id="Seg_7792" s="T219">kuja-Kən</ta>
            <ta e="T221" id="Seg_7793" s="T220">koː-lV-j</ta>
            <ta e="T222" id="Seg_7794" s="T221">da</ta>
            <ta e="T223" id="Seg_7795" s="T222">oʔbdə-zittə</ta>
            <ta e="T224" id="Seg_7796" s="T223">nadə</ta>
            <ta e="T225" id="Seg_7797" s="T224">il</ta>
            <ta e="T226" id="Seg_7798" s="T225">mĭn-liA-jəʔ</ta>
            <ta e="T227" id="Seg_7799" s="T226">ugaːndə</ta>
            <ta e="T228" id="Seg_7800" s="T227">jakšə</ta>
            <ta e="T229" id="Seg_7801" s="T228">pʼe</ta>
            <ta e="T230" id="Seg_7802" s="T229">šo-bi</ta>
            <ta e="T231" id="Seg_7803" s="T230">ugaːndə</ta>
            <ta e="T233" id="Seg_7804" s="T232">ejü</ta>
            <ta e="T234" id="Seg_7805" s="T233">măn</ta>
            <ta e="T235" id="Seg_7806" s="T234">dönüli</ta>
            <ta e="T236" id="Seg_7807" s="T235">mo-bi-m</ta>
            <ta e="T237" id="Seg_7808" s="T236">kunol-zittə</ta>
            <ta e="T238" id="Seg_7809" s="T237">dĭgəttə</ta>
            <ta e="T239" id="Seg_7810" s="T238">ertə</ta>
            <ta e="T240" id="Seg_7811" s="T239">uʔbdə-bi-m</ta>
            <ta e="T241" id="Seg_7812" s="T240">a</ta>
            <ta e="T242" id="Seg_7813" s="T241">tʼala</ta>
            <ta e="T243" id="Seg_7814" s="T242">tʼala-m</ta>
            <ta e="T244" id="Seg_7815" s="T243">bar</ta>
            <ta e="T245" id="Seg_7816" s="T244">togonər-bi-m</ta>
            <ta e="T246" id="Seg_7817" s="T245">tüžöj</ta>
            <ta e="T247" id="Seg_7818" s="T246">surdo-bi-m</ta>
            <ta e="T248" id="Seg_7819" s="T247">da</ta>
            <ta e="T249" id="Seg_7820" s="T248">mĭnzər-bi-m</ta>
            <ta e="T250" id="Seg_7821" s="T249">a-bi-m</ta>
            <ta e="T251" id="Seg_7822" s="T250">dĭgəttə</ta>
            <ta e="T252" id="Seg_7823" s="T251">tʼăbaktər-bi-m</ta>
            <ta e="T253" id="Seg_7824" s="T252">ertə</ta>
            <ta e="T254" id="Seg_7825" s="T253">oʔbdə-bi-m</ta>
            <ta e="T255" id="Seg_7826" s="T254">da</ta>
            <ta e="T256" id="Seg_7827" s="T255">šü</ta>
            <ta e="T257" id="Seg_7828" s="T256">nendə-bi-m</ta>
            <ta e="T258" id="Seg_7829" s="T257">măna</ta>
            <ta e="T259" id="Seg_7830" s="T258">Kazan</ta>
            <ta e="T260" id="Seg_7831" s="T259">tura-Tə</ta>
            <ta e="T261" id="Seg_7832" s="T260">nadə</ta>
            <ta e="T262" id="Seg_7833" s="T261">kan-zittə</ta>
            <ta e="T263" id="Seg_7834" s="T262">măna</ta>
            <ta e="T264" id="Seg_7835" s="T263">aktʼa</ta>
            <ta e="T265" id="Seg_7836" s="T264">iʔbö-laʔbə</ta>
            <ta e="T266" id="Seg_7837" s="T265">bank-ə-n</ta>
            <ta e="T267" id="Seg_7838" s="T266">i-zittə</ta>
            <ta e="T268" id="Seg_7839" s="T267">nadə</ta>
            <ta e="T269" id="Seg_7840" s="T268">da</ta>
            <ta e="T270" id="Seg_7841" s="T269">il-də</ta>
            <ta e="T271" id="Seg_7842" s="T270">mĭ-zittə</ta>
            <ta e="T272" id="Seg_7843" s="T271">măn</ta>
            <ta e="T273" id="Seg_7844" s="T272">dĭ-zAŋ-Kən</ta>
            <ta e="T274" id="Seg_7845" s="T273">i-bi-m</ta>
            <ta e="T275" id="Seg_7846" s="T274">măn</ta>
            <ta e="T276" id="Seg_7847" s="T275">iʔgö</ta>
            <ta e="T277" id="Seg_7848" s="T276">tʼăbaktər-liA-m</ta>
            <ta e="T279" id="Seg_7849" s="T278">tüj</ta>
            <ta e="T281" id="Seg_7850" s="T280">tʼăbaktər-laʔbə-jəʔ</ta>
            <ta e="T282" id="Seg_7851" s="T281">ugaːndə</ta>
            <ta e="T283" id="Seg_7852" s="T282">tăn</ta>
            <ta e="T284" id="Seg_7853" s="T283">koloʔ-zəbi</ta>
            <ta e="T285" id="Seg_7854" s="T284">koʔbdo</ta>
            <ta e="T286" id="Seg_7855" s="T285">üge</ta>
            <ta e="T287" id="Seg_7856" s="T286">măna</ta>
            <ta e="T288" id="Seg_7857" s="T287">măna</ta>
            <ta e="T289" id="Seg_7858" s="T288">nadə</ta>
            <ta e="T290" id="Seg_7859" s="T289">tʼăbaktər-zittə</ta>
            <ta e="T291" id="Seg_7860" s="T290">tüj</ta>
            <ta e="T292" id="Seg_7861" s="T291">tăn</ta>
            <ta e="T293" id="Seg_7862" s="T292">tʼăbaktər-ə-ʔ</ta>
            <ta e="T294" id="Seg_7863" s="T293">a</ta>
            <ta e="T295" id="Seg_7864" s="T294">măn</ta>
            <ta e="T296" id="Seg_7865" s="T295">ej</ta>
            <ta e="T297" id="Seg_7866" s="T296">tʼăbaktər-lV-m</ta>
            <ta e="T299" id="Seg_7867" s="T298">măn</ta>
            <ta e="T300" id="Seg_7868" s="T299">ej</ta>
            <ta e="T301" id="Seg_7869" s="T300">tĭm-liA-m</ta>
            <ta e="T302" id="Seg_7870" s="T301">ĭmbi</ta>
            <ta e="T303" id="Seg_7871" s="T302">tʼăbaktər-zittə</ta>
            <ta e="T304" id="Seg_7872" s="T303">tăn</ta>
            <ta e="T305" id="Seg_7873" s="T304">tʼăbaktər-ə-ʔ</ta>
            <ta e="T306" id="Seg_7874" s="T305">a</ta>
            <ta e="T308" id="Seg_7875" s="T307">a</ta>
            <ta e="T309" id="Seg_7876" s="T308">măn</ta>
            <ta e="T310" id="Seg_7877" s="T309">ĭmbi</ta>
            <ta e="T311" id="Seg_7878" s="T310">üge</ta>
            <ta e="T313" id="Seg_7879" s="T312">tʼăbaktər-zittə</ta>
            <ta e="T317" id="Seg_7880" s="T315">iʔbə-lV-m=da</ta>
            <ta e="T318" id="Seg_7881" s="T317">măn</ta>
            <ta e="T319" id="Seg_7882" s="T318">tura-m</ta>
            <ta e="T322" id="Seg_7883" s="T321">măn</ta>
            <ta e="T323" id="Seg_7884" s="T322">ĭmbi=də</ta>
            <ta e="T324" id="Seg_7885" s="T323">ej</ta>
            <ta e="T325" id="Seg_7886" s="T324">tĭm-liA-m</ta>
            <ta e="T326" id="Seg_7887" s="T325">tenö-ʔ</ta>
            <ta e="T327" id="Seg_7888" s="T326">ĭmbi=nʼibudʼ</ta>
            <ta e="T328" id="Seg_7889" s="T327">šʼaːm-ʔ</ta>
            <ta e="T329" id="Seg_7890" s="T328">ĭmbi=nʼibudʼ</ta>
            <ta e="T330" id="Seg_7891" s="T329">aba-m</ta>
            <ta e="T331" id="Seg_7892" s="T330">kuŋgə-m</ta>
            <ta e="T1057" id="Seg_7893" s="T331">kan-lAʔ</ta>
            <ta e="T332" id="Seg_7894" s="T1057">tʼür-bi</ta>
            <ta e="T333" id="Seg_7895" s="T332">a</ta>
            <ta e="T334" id="Seg_7896" s="T333">măna</ta>
            <ta e="T335" id="Seg_7897" s="T334">ugaːndə</ta>
            <ta e="T336" id="Seg_7898" s="T335">jakšə</ta>
            <ta e="T337" id="Seg_7899" s="T336">šində=də</ta>
            <ta e="T338" id="Seg_7900" s="T337">ej</ta>
            <ta e="T339" id="Seg_7901" s="T338">münör-liA</ta>
            <ta e="T340" id="Seg_7902" s="T339">măn</ta>
            <ta e="T341" id="Seg_7903" s="T340">balʔraʔ</ta>
            <ta e="T342" id="Seg_7904" s="T341">alom-liA-m</ta>
            <ta e="T343" id="Seg_7905" s="T342">ajnu</ta>
            <ta e="T344" id="Seg_7906" s="T343">nüj-laʔbə-m</ta>
            <ta e="T345" id="Seg_7907" s="T344">süʔmə-laʔbə-m</ta>
            <ta e="T346" id="Seg_7908" s="T345">bar</ta>
            <ta e="T347" id="Seg_7909" s="T346">a</ta>
            <ta e="T348" id="Seg_7910" s="T347">ija-m</ta>
            <ta e="T349" id="Seg_7911" s="T348">ej</ta>
            <ta e="T350" id="Seg_7912" s="T349">nʼilgö-liA-m</ta>
            <ta e="T351" id="Seg_7913" s="T350">ija-m</ta>
            <ta e="T352" id="Seg_7914" s="T351">aba-m</ta>
            <ta e="T353" id="Seg_7915" s="T352">dĭ</ta>
            <ta e="T354" id="Seg_7916" s="T353">saʔpiaŋ-bi</ta>
            <ta e="T355" id="Seg_7917" s="T354">što</ta>
            <ta e="T356" id="Seg_7918" s="T355">măn</ta>
            <ta e="T358" id="Seg_7919" s="T357">što</ta>
            <ta e="T359" id="Seg_7920" s="T358">măn</ta>
            <ta e="T360" id="Seg_7921" s="T359">alom-liA-m</ta>
            <ta e="T361" id="Seg_7922" s="T360">măn</ta>
            <ta e="T362" id="Seg_7923" s="T361">hele</ta>
            <ta e="T363" id="Seg_7924" s="T362">teinen</ta>
            <ta e="T364" id="Seg_7925" s="T363">nüdʼi-n</ta>
            <ta e="T365" id="Seg_7926" s="T364">bar</ta>
            <ta e="T366" id="Seg_7927" s="T365">ara</ta>
            <ta e="T369" id="Seg_7928" s="T368">nüjnə</ta>
            <ta e="T371" id="Seg_7929" s="T370">nüj-bi</ta>
            <ta e="T372" id="Seg_7930" s="T371">tʼabəro-bi</ta>
            <ta e="T373" id="Seg_7931" s="T372">bar</ta>
            <ta e="T374" id="Seg_7932" s="T373">kudo-nzə-bi</ta>
            <ta e="T375" id="Seg_7933" s="T374">bar</ta>
            <ta e="T376" id="Seg_7934" s="T375">Jelʼa</ta>
            <ta e="T377" id="Seg_7935" s="T376">unʼə</ta>
            <ta e="T378" id="Seg_7936" s="T377">amno-laʔbə</ta>
            <ta e="T379" id="Seg_7937" s="T378">üge</ta>
            <ta e="T380" id="Seg_7938" s="T379">monoʔko-laʔbə-jəʔ</ta>
            <ta e="T381" id="Seg_7939" s="T380">a</ta>
            <ta e="T382" id="Seg_7940" s="T381">dĭ</ta>
            <ta e="T383" id="Seg_7941" s="T382">üge</ta>
            <ta e="T384" id="Seg_7942" s="T383">ej</ta>
            <ta e="T385" id="Seg_7943" s="T384">kan-liA</ta>
            <ta e="T386" id="Seg_7944" s="T385">šində-Tə=də</ta>
            <ta e="T387" id="Seg_7945" s="T386">ej</ta>
            <ta e="T388" id="Seg_7946" s="T387">kan-liA</ta>
            <ta e="T394" id="Seg_7947" s="T393">dĭ-n</ta>
            <ta e="T395" id="Seg_7948" s="T394">bar</ta>
            <ta e="T396" id="Seg_7949" s="T395">ija-t</ta>
            <ta e="T397" id="Seg_7950" s="T396">aba-t</ta>
            <ta e="T398" id="Seg_7951" s="T397">i-gA</ta>
            <ta e="T399" id="Seg_7952" s="T398">măn-liA-jəʔ</ta>
            <ta e="T400" id="Seg_7953" s="T399">kan-ə-ʔ</ta>
            <ta e="T401" id="Seg_7954" s="T400">tibi-Tə</ta>
            <ta e="T402" id="Seg_7955" s="T401">a</ta>
            <ta e="T403" id="Seg_7956" s="T402">dĭ</ta>
            <ta e="T404" id="Seg_7957" s="T403">kădaʔ=də</ta>
            <ta e="T405" id="Seg_7958" s="T404">ej</ta>
            <ta e="T406" id="Seg_7959" s="T405">kan-liA</ta>
            <ta e="T407" id="Seg_7960" s="T406">šo-bi</ta>
            <ta e="T408" id="Seg_7961" s="T407">măna</ta>
            <ta e="T409" id="Seg_7962" s="T408">monoʔko-zittə</ta>
            <ta e="T410" id="Seg_7963" s="T409">ej</ta>
            <ta e="T411" id="Seg_7964" s="T410">kuvas</ta>
            <ta e="T412" id="Seg_7965" s="T411">kuza</ta>
            <ta e="T413" id="Seg_7966" s="T412">măna</ta>
            <ta e="T414" id="Seg_7967" s="T413">nʼe</ta>
            <ta e="T415" id="Seg_7968" s="T414">axota</ta>
            <ta e="T416" id="Seg_7969" s="T415">kan-zittə</ta>
            <ta e="T417" id="Seg_7970" s="T416">aba-m</ta>
            <ta e="T418" id="Seg_7971" s="T417">măn-ntə</ta>
            <ta e="T419" id="Seg_7972" s="T418">ĭmbi</ta>
            <ta e="T420" id="Seg_7973" s="T419">tăn</ta>
            <ta e="T421" id="Seg_7974" s="T420">ej</ta>
            <ta e="T422" id="Seg_7975" s="T421">kan-lV-l</ta>
            <ta e="T423" id="Seg_7976" s="T422">ej</ta>
            <ta e="T424" id="Seg_7977" s="T423">nu</ta>
            <ta e="T425" id="Seg_7978" s="T424">e-ʔ</ta>
            <ta e="T426" id="Seg_7979" s="T425">kan-ə-ʔ</ta>
            <ta e="T427" id="Seg_7980" s="T426">măn</ta>
            <ta e="T428" id="Seg_7981" s="T427">nuʔmə-luʔbdə-bi-m</ta>
            <ta e="T429" id="Seg_7982" s="T428">dĭgəttə</ta>
            <ta e="T430" id="Seg_7983" s="T429">măn</ta>
            <ta e="T433" id="Seg_7984" s="T432">tibi-m</ta>
            <ta e="T434" id="Seg_7985" s="T433">šo-bi</ta>
            <ta e="T435" id="Seg_7986" s="T434">măna</ta>
            <ta e="T436" id="Seg_7987" s="T435">monoʔko-zittə</ta>
            <ta e="T437" id="Seg_7988" s="T436">dĭ-Tə</ta>
            <ta e="T439" id="Seg_7989" s="T438">pʼe</ta>
            <ta e="T440" id="Seg_7990" s="T439">i</ta>
            <ta e="T441" id="Seg_7991" s="T440">măna</ta>
            <ta e="T442" id="Seg_7992" s="T441">amka</ta>
            <ta e="T443" id="Seg_7993" s="T442">pʼe</ta>
            <ta e="T444" id="Seg_7994" s="T443">i</ta>
            <ta e="T445" id="Seg_7995" s="T444">măna</ta>
            <ta e="T446" id="Seg_7996" s="T445">amka</ta>
            <ta e="T447" id="Seg_7997" s="T446">pʼe</ta>
            <ta e="T448" id="Seg_7998" s="T447">măn</ta>
            <ta e="T450" id="Seg_7999" s="T449">măn</ta>
            <ta e="T451" id="Seg_8000" s="T450">măn-ntə-m</ta>
            <ta e="T452" id="Seg_8001" s="T451">kan-ə-ʔ</ta>
            <ta e="T453" id="Seg_8002" s="T452">aba-Tə</ta>
            <ta e="T454" id="Seg_8003" s="T453">da</ta>
            <ta e="T455" id="Seg_8004" s="T454">ija-Tə</ta>
            <ta e="T456" id="Seg_8005" s="T455">monoʔko-ʔ</ta>
            <ta e="T457" id="Seg_8006" s="T456">dĭ-zAŋ</ta>
            <ta e="T458" id="Seg_8007" s="T457">mĭ-lV-jəʔ</ta>
            <ta e="T459" id="Seg_8008" s="T458">dʼok</ta>
            <ta e="T460" id="Seg_8009" s="T459">ej</ta>
            <ta e="T461" id="Seg_8010" s="T460">mĭ-lV-jəʔ</ta>
            <ta e="T462" id="Seg_8011" s="T461">dĭgəttə</ta>
            <ta e="T463" id="Seg_8012" s="T462">măna</ta>
            <ta e="T464" id="Seg_8013" s="T463">tʼăbaktər-luʔbdə-bi-jəʔ</ta>
            <ta e="T465" id="Seg_8014" s="T464">i</ta>
            <ta e="T467" id="Seg_8015" s="T466">kan-bi-bAʔ</ta>
            <ta e="T469" id="Seg_8016" s="T468">ine-t-ziʔ</ta>
            <ta e="T470" id="Seg_8017" s="T469">dĭ-Tə</ta>
            <ta e="T471" id="Seg_8018" s="T470">dĭgəttə</ta>
            <ta e="T472" id="Seg_8019" s="T471">ija-m</ta>
            <ta e="T473" id="Seg_8020" s="T472">šo-bi</ta>
            <ta e="T474" id="Seg_8021" s="T473">dĭbər</ta>
            <ta e="T475" id="Seg_8022" s="T474">miʔnʼibeʔ</ta>
            <ta e="T476" id="Seg_8023" s="T475">dĭ</ta>
            <ta e="T478" id="Seg_8024" s="T477">döbər</ta>
            <ta e="T479" id="Seg_8025" s="T478">šo-bi</ta>
            <ta e="T480" id="Seg_8026" s="T479">noʔ</ta>
            <ta e="T481" id="Seg_8027" s="T480">hʼaʔ-zittə</ta>
            <ta e="T482" id="Seg_8028" s="T481">dĭgəttə</ta>
            <ta e="T483" id="Seg_8029" s="T482">kan-bi</ta>
            <ta e="T484" id="Seg_8030" s="T483">bü-Tə</ta>
            <ta e="T485" id="Seg_8031" s="T484">da</ta>
            <ta e="T486" id="Seg_8032" s="T485">dĭn</ta>
            <ta e="T487" id="Seg_8033" s="T486">kü-laːm-bi</ta>
            <ta e="T488" id="Seg_8034" s="T487">dĭgəttə</ta>
            <ta e="T489" id="Seg_8035" s="T488">măn</ta>
            <ta e="T490" id="Seg_8036" s="T489">maʔ-gəndə</ta>
            <ta e="T491" id="Seg_8037" s="T490">šo-bi-m</ta>
            <ta e="T492" id="Seg_8038" s="T491">i</ta>
            <ta e="T493" id="Seg_8039" s="T492">ugaːndə</ta>
            <ta e="T494" id="Seg_8040" s="T493">tăŋ</ta>
            <ta e="T495" id="Seg_8041" s="T494">tenö-bi-m</ta>
            <ta e="T496" id="Seg_8042" s="T495">sĭj-m</ta>
            <ta e="T497" id="Seg_8043" s="T496">ĭzem-bi</ta>
            <ta e="T498" id="Seg_8044" s="T497">tʼor-bi-m</ta>
            <ta e="T499" id="Seg_8045" s="T498">ajir-bi-m</ta>
            <ta e="T500" id="Seg_8046" s="T499">dĭ-m</ta>
            <ta e="T501" id="Seg_8047" s="T500">bar</ta>
            <ta e="T502" id="Seg_8048" s="T501">dĭgəttə</ta>
            <ta e="T503" id="Seg_8049" s="T502">ija-m</ta>
            <ta e="T504" id="Seg_8050" s="T503">măna</ta>
            <ta e="T505" id="Seg_8051" s="T504">bü</ta>
            <ta e="T506" id="Seg_8052" s="T505">tʼezer-bi</ta>
            <ta e="T507" id="Seg_8053" s="T506">pĭj</ta>
            <ta e="T508" id="Seg_8054" s="T507">măna</ta>
            <ta e="T509" id="Seg_8055" s="T508">dĭgəttə</ta>
            <ta e="T510" id="Seg_8056" s="T509">măn</ta>
            <ta e="T511" id="Seg_8057" s="T510">davaj</ta>
            <ta e="T512" id="Seg_8058" s="T511">nüjnə</ta>
            <ta e="T514" id="Seg_8059" s="T513">nüj-zittə</ta>
            <ta e="T515" id="Seg_8060" s="T514">dĭgəttə</ta>
            <ta e="T516" id="Seg_8061" s="T515">măn</ta>
            <ta e="T517" id="Seg_8062" s="T516">šo-bi-m</ta>
            <ta e="T518" id="Seg_8063" s="T517">maʔ-gənʼi</ta>
            <ta e="T519" id="Seg_8064" s="T518">amno-bi-m</ta>
            <ta e="T520" id="Seg_8065" s="T519">dĭgəttə</ta>
            <ta e="T521" id="Seg_8066" s="T520">kan-bi-m</ta>
            <ta e="T523" id="Seg_8067" s="T522">Permʼakovo-Tə</ta>
            <ta e="T525" id="Seg_8068" s="T524">ten</ta>
            <ta e="T526" id="Seg_8069" s="T525">sʼar-bi-bAʔ</ta>
            <ta e="T527" id="Seg_8070" s="T526">süʔmə-bAʔ</ta>
            <ta e="T528" id="Seg_8071" s="T527">garmonʼ-gənʼi</ta>
            <ta e="T529" id="Seg_8072" s="T528">sʼar-bi-bAʔ</ta>
            <ta e="T530" id="Seg_8073" s="T529">nüjnə</ta>
            <ta e="T531" id="Seg_8074" s="T530">nüj-bi-bAʔ</ta>
            <ta e="T532" id="Seg_8075" s="T531">dĭgəttə</ta>
            <ta e="T533" id="Seg_8076" s="T532">dĭn</ta>
            <ta e="T534" id="Seg_8077" s="T533">tibi</ta>
            <ta e="T535" id="Seg_8078" s="T534">šo-bi</ta>
            <ta e="T536" id="Seg_8079" s="T535">Nagornaj</ta>
            <ta e="T537" id="Seg_8080" s="T536">muʔzen-də</ta>
            <ta e="T538" id="Seg_8081" s="T537">urgo</ta>
            <ta e="T540" id="Seg_8082" s="T539">da</ta>
            <ta e="T541" id="Seg_8083" s="T540">kömə-jəʔ</ta>
            <ta e="T542" id="Seg_8084" s="T541">bos-də</ta>
            <ta e="T543" id="Seg_8085" s="T542">kömə</ta>
            <ta e="T544" id="Seg_8086" s="T543">dĭgəttə</ta>
            <ta e="T545" id="Seg_8087" s="T544">măn</ta>
            <ta e="T546" id="Seg_8088" s="T545">maʔ-gənʼi</ta>
            <ta e="T547" id="Seg_8089" s="T546">šo-bi-m</ta>
            <ta e="T548" id="Seg_8090" s="T547">da</ta>
            <ta e="T549" id="Seg_8091" s="T548">šo-bi</ta>
            <ta e="T550" id="Seg_8092" s="T549">măna</ta>
            <ta e="T551" id="Seg_8093" s="T550">monoʔko-zittə</ta>
            <ta e="T552" id="Seg_8094" s="T551">măn</ta>
            <ta e="T553" id="Seg_8095" s="T552">măn-ntə-m</ta>
            <ta e="T554" id="Seg_8096" s="T553">tăn</ta>
            <ta e="T555" id="Seg_8097" s="T554">nʼi</ta>
            <ta e="T556" id="Seg_8098" s="T555">git</ta>
            <ta e="T558" id="Seg_8099" s="T557">kak</ta>
            <ta e="T559" id="Seg_8100" s="T558">măn</ta>
            <ta e="T560" id="Seg_8101" s="T559">dĭrgit</ta>
            <ta e="T561" id="Seg_8102" s="T560">nu</ta>
            <ta e="T562" id="Seg_8103" s="T561">măn</ta>
            <ta e="T563" id="Seg_8104" s="T562">tăn-ziʔ</ta>
            <ta e="T564" id="Seg_8105" s="T563">ej</ta>
            <ta e="T565" id="Seg_8106" s="T564">amno-lV-m</ta>
            <ta e="T566" id="Seg_8107" s="T565">a</ta>
            <ta e="T567" id="Seg_8108" s="T566">nʼi-ziʔ</ta>
            <ta e="T568" id="Seg_8109" s="T567">dĭgəttə</ta>
            <ta e="T569" id="Seg_8110" s="T568">ija-m</ta>
            <ta e="T570" id="Seg_8111" s="T569">măn-ntə</ta>
            <ta e="T571" id="Seg_8112" s="T570">kan-ə-ʔ</ta>
            <ta e="T572" id="Seg_8113" s="T571">dĭ-n</ta>
            <ta e="T573" id="Seg_8114" s="T572">aktʼa</ta>
            <ta e="T574" id="Seg_8115" s="T573">iʔgö</ta>
            <ta e="T575" id="Seg_8116" s="T574">ugaːndə</ta>
            <ta e="T577" id="Seg_8117" s="T576">jakšə</ta>
            <ta e="T578" id="Seg_8118" s="T577">kuza</ta>
            <ta e="T579" id="Seg_8119" s="T578">a</ta>
            <ta e="T580" id="Seg_8120" s="T579">măn</ta>
            <ta e="T581" id="Seg_8121" s="T580">nu-liA-m</ta>
            <ta e="T582" id="Seg_8122" s="T581">ej</ta>
            <ta e="T583" id="Seg_8123" s="T582">da</ta>
            <ta e="T584" id="Seg_8124" s="T583">kan-ə-ʔ</ta>
            <ta e="T585" id="Seg_8125" s="T584">da</ta>
            <ta e="T586" id="Seg_8126" s="T585">amno-ʔ</ta>
            <ta e="T587" id="Seg_8127" s="T586">dĭ-ziʔ</ta>
            <ta e="T588" id="Seg_8128" s="T587">onʼiʔ</ta>
            <ta e="T589" id="Seg_8129" s="T588">kuza</ta>
            <ta e="T590" id="Seg_8130" s="T589">šo-bi</ta>
            <ta e="T591" id="Seg_8131" s="T590">monoʔko-zittə</ta>
            <ta e="T592" id="Seg_8132" s="T591">a</ta>
            <ta e="T593" id="Seg_8133" s="T592">măn</ta>
            <ta e="T594" id="Seg_8134" s="T593">ej</ta>
            <ta e="T595" id="Seg_8135" s="T594">kan-liA-m</ta>
            <ta e="T596" id="Seg_8136" s="T595">a</ta>
            <ta e="T597" id="Seg_8137" s="T596">ija</ta>
            <ta e="T598" id="Seg_8138" s="T597">ej</ta>
            <ta e="T599" id="Seg_8139" s="T598">kan-liA-m</ta>
            <ta e="T600" id="Seg_8140" s="T599">dĭ</ta>
            <ta e="T601" id="Seg_8141" s="T600">ej</ta>
            <ta e="T602" id="Seg_8142" s="T601">kuvas</ta>
            <ta e="T603" id="Seg_8143" s="T602">üjü</ta>
            <ta e="T604" id="Seg_8144" s="T603">üjü-t</ta>
            <ta e="T605" id="Seg_8145" s="T604">bar</ta>
            <ta e="T607" id="Seg_8146" s="T606">păjdʼaŋ</ta>
            <ta e="T609" id="Seg_8147" s="T608">muʔzen-də</ta>
            <ta e="T610" id="Seg_8148" s="T609">kömə</ta>
            <ta e="T611" id="Seg_8149" s="T610">šĭkə-t</ta>
            <ta e="T612" id="Seg_8150" s="T611">ej</ta>
            <ta e="T613" id="Seg_8151" s="T612">tʼăbaktər-liA</ta>
            <ta e="T614" id="Seg_8152" s="T613">ej</ta>
            <ta e="T615" id="Seg_8153" s="T614">jakšə</ta>
            <ta e="T616" id="Seg_8154" s="T615">tʼăbaktər-liA</ta>
            <ta e="T617" id="Seg_8155" s="T616">ku-zAŋ-də</ta>
            <ta e="T618" id="Seg_8156" s="T617">ej</ta>
            <ta e="T619" id="Seg_8157" s="T618">nʼilgö-liA-jəʔ</ta>
            <ta e="T620" id="Seg_8158" s="T619">a</ta>
            <ta e="T621" id="Seg_8159" s="T620">ija-m</ta>
            <ta e="T622" id="Seg_8160" s="T621">măn-ntə</ta>
            <ta e="T623" id="Seg_8161" s="T622">kan-ə-ʔ</ta>
            <ta e="T624" id="Seg_8162" s="T623">dĭ-n</ta>
            <ta e="T625" id="Seg_8163" s="T624">tüžöj</ta>
            <ta e="T626" id="Seg_8164" s="T625">i-gA</ta>
            <ta e="T627" id="Seg_8165" s="T626">i</ta>
            <ta e="T628" id="Seg_8166" s="T627">ine</ta>
            <ta e="T629" id="Seg_8167" s="T628">i-gA</ta>
            <ta e="T630" id="Seg_8168" s="T629">i</ta>
            <ta e="T631" id="Seg_8169" s="T630">ipek</ta>
            <ta e="T632" id="Seg_8170" s="T631">iʔgö</ta>
            <ta e="T633" id="Seg_8171" s="T632">a</ta>
            <ta e="T634" id="Seg_8172" s="T633">măn</ta>
            <ta e="T635" id="Seg_8173" s="T634">măn</ta>
            <ta e="T636" id="Seg_8174" s="T635">măn-bi-m</ta>
            <ta e="T637" id="Seg_8175" s="T636">măna</ta>
            <ta e="T638" id="Seg_8176" s="T637">ine-ziʔ</ta>
            <ta e="T639" id="Seg_8177" s="T638">ej</ta>
            <ta e="T640" id="Seg_8178" s="T639">amno-zittə</ta>
            <ta e="T641" id="Seg_8179" s="T640">i</ta>
            <ta e="T642" id="Seg_8180" s="T641">tüžöj-Tə</ta>
            <ta e="T643" id="Seg_8181" s="T642">ej</ta>
            <ta e="T644" id="Seg_8182" s="T643">amno-zittə</ta>
            <ta e="T645" id="Seg_8183" s="T644">nadə</ta>
            <ta e="T646" id="Seg_8184" s="T645">tănan</ta>
            <ta e="T647" id="Seg_8185" s="T646">tak</ta>
            <ta e="T648" id="Seg_8186" s="T647">tăn</ta>
            <ta e="T649" id="Seg_8187" s="T648">tănan</ta>
            <ta e="T650" id="Seg_8188" s="T649">kereʔ</ta>
            <ta e="T651" id="Seg_8189" s="T650">tak</ta>
            <ta e="T652" id="Seg_8190" s="T651">tăn</ta>
            <ta e="T653" id="Seg_8191" s="T652">kan-ə-ʔ</ta>
            <ta e="T654" id="Seg_8192" s="T653">stanitsa-Kən</ta>
            <ta e="T655" id="Seg_8193" s="T654">šo-bi</ta>
            <ta e="T656" id="Seg_8194" s="T655">onʼiʔ</ta>
            <ta e="T657" id="Seg_8195" s="T656">tibi</ta>
            <ta e="T658" id="Seg_8196" s="T657">dĭ-m</ta>
            <ta e="T659" id="Seg_8197" s="T658">ne</ta>
            <ta e="T660" id="Seg_8198" s="T659">barəʔ-luʔbdə-bi</ta>
            <ta e="T661" id="Seg_8199" s="T660">dĭ</ta>
            <ta e="T662" id="Seg_8200" s="T661">davaj</ta>
            <ta e="T663" id="Seg_8201" s="T662">măna</ta>
            <ta e="T664" id="Seg_8202" s="T663">monoʔko-zittə</ta>
            <ta e="T665" id="Seg_8203" s="T664">ija-m</ta>
            <ta e="T666" id="Seg_8204" s="T665">stal</ta>
            <ta e="T667" id="Seg_8205" s="T666">mĭnzər-zittə</ta>
            <ta e="T668" id="Seg_8206" s="T667">uja</ta>
            <ta e="T669" id="Seg_8207" s="T668">dĭ-zem</ta>
            <ta e="T671" id="Seg_8208" s="T670">bădə-zittə</ta>
            <ta e="T672" id="Seg_8209" s="T671">a</ta>
            <ta e="T673" id="Seg_8210" s="T672">măn</ta>
            <ta e="T674" id="Seg_8211" s="T673">măn-liA-m</ta>
            <ta e="T675" id="Seg_8212" s="T674">kan-zittə</ta>
            <ta e="T676" id="Seg_8213" s="T675">ilʼi</ta>
            <ta e="T677" id="Seg_8214" s="T676">ej</ta>
            <ta e="T678" id="Seg_8215" s="T677">kan-zittə</ta>
            <ta e="T680" id="Seg_8216" s="T679">kan-ə-ʔ</ta>
            <ta e="T682" id="Seg_8217" s="T681">e-ʔ</ta>
            <ta e="T683" id="Seg_8218" s="T682">kan-ə-ʔ</ta>
            <ta e="T684" id="Seg_8219" s="T683">tăn</ta>
            <ta e="T685" id="Seg_8220" s="T684">tüjö</ta>
            <ta e="T687" id="Seg_8221" s="T686">bos-də</ta>
            <ta e="T688" id="Seg_8222" s="T687">urgo</ta>
            <ta e="T689" id="Seg_8223" s="T688">bos-də</ta>
            <ta e="T690" id="Seg_8224" s="T689">tĭmne-l</ta>
            <ta e="T691" id="Seg_8225" s="T690">ĭmbi</ta>
            <ta e="T692" id="Seg_8226" s="T691">a-zittə</ta>
            <ta e="T693" id="Seg_8227" s="T692">dĭgəttə</ta>
            <ta e="T694" id="Seg_8228" s="T693">dĭ</ta>
            <ta e="T695" id="Seg_8229" s="T694">tibi</ta>
            <ta e="T697" id="Seg_8230" s="T696">šo-bi</ta>
            <ta e="T698" id="Seg_8231" s="T697">măna</ta>
            <ta e="T699" id="Seg_8232" s="T698">amnə-bi</ta>
            <ta e="T700" id="Seg_8233" s="T699">i</ta>
            <ta e="T701" id="Seg_8234" s="T700">davaj</ta>
            <ta e="T702" id="Seg_8235" s="T701">monoʔko-zittə</ta>
            <ta e="T703" id="Seg_8236" s="T702">bos-də</ta>
            <ta e="T704" id="Seg_8237" s="T703">i</ta>
            <ta e="T705" id="Seg_8238" s="T704">davaj</ta>
            <ta e="T706" id="Seg_8239" s="T705">kudaj-Tə</ta>
            <ta e="T707" id="Seg_8240" s="T706">numan üzə-zittə</ta>
            <ta e="T708" id="Seg_8241" s="T707">ej</ta>
            <ta e="T709" id="Seg_8242" s="T708">baʔbdə-lV-m</ta>
            <ta e="T710" id="Seg_8243" s="T709">tănan</ta>
            <ta e="T711" id="Seg_8244" s="T710">a</ta>
            <ta e="T712" id="Seg_8245" s="T711">muno-lV-m</ta>
            <ta e="T713" id="Seg_8246" s="T712">jakšə</ta>
            <ta e="T714" id="Seg_8247" s="T713">amno-lV-bAʔ</ta>
            <ta e="T715" id="Seg_8248" s="T714">dĭgəttə</ta>
            <ta e="T716" id="Seg_8249" s="T715">dĭ</ta>
            <ta e="T717" id="Seg_8250" s="T716">ine</ta>
            <ta e="T718" id="Seg_8251" s="T717">körer-bi</ta>
            <ta e="T720" id="Seg_8252" s="T719">miʔ</ta>
            <ta e="T721" id="Seg_8253" s="T720">măn</ta>
            <ta e="T722" id="Seg_8254" s="T721">dĭ-ziʔ</ta>
            <ta e="T723" id="Seg_8255" s="T722">kan-bi-m</ta>
            <ta e="T724" id="Seg_8256" s="T723">Permʼakovo-Tə</ta>
            <ta e="T725" id="Seg_8257" s="T724">ara</ta>
            <ta e="T726" id="Seg_8258" s="T725">i-bi-m</ta>
            <ta e="T727" id="Seg_8259" s="T726">det-bi-bAʔ</ta>
            <ta e="T730" id="Seg_8260" s="T729">stol-də</ta>
            <ta e="T731" id="Seg_8261" s="T730">oʔbdə-bi-bAʔ</ta>
            <ta e="T732" id="Seg_8262" s="T731">il</ta>
            <ta e="T733" id="Seg_8263" s="T732">oʔbdə-bi-bAʔ</ta>
            <ta e="T734" id="Seg_8264" s="T733">ara</ta>
            <ta e="T735" id="Seg_8265" s="T734">bĭs-bi-jəʔ</ta>
            <ta e="T736" id="Seg_8266" s="T735">am-bi-jəʔ</ta>
            <ta e="T737" id="Seg_8267" s="T736">dĭgəttə</ta>
            <ta e="T738" id="Seg_8268" s="T737">miʔ</ta>
            <ta e="T740" id="Seg_8269" s="T739">kan-bi-bAʔ</ta>
            <ta e="T741" id="Seg_8270" s="T740">stanitsa-Tə</ta>
            <ta e="T742" id="Seg_8271" s="T741">dĭgəttə</ta>
            <ta e="T743" id="Seg_8272" s="T742">mano-bi-m</ta>
            <ta e="T744" id="Seg_8273" s="T743">nagur</ta>
            <ta e="T745" id="Seg_8274" s="T744">kö</ta>
            <ta e="T746" id="Seg_8275" s="T745">dĭgəttə</ta>
            <ta e="T747" id="Seg_8276" s="T746">văjna</ta>
            <ta e="T748" id="Seg_8277" s="T747">i-bi</ta>
            <ta e="T749" id="Seg_8278" s="T748">dĭ-m</ta>
            <ta e="T750" id="Seg_8279" s="T749">i-bi-jəʔ</ta>
            <ta e="T751" id="Seg_8280" s="T750">văjna-Tə</ta>
            <ta e="T752" id="Seg_8281" s="T751">dĭgəttə</ta>
            <ta e="T753" id="Seg_8282" s="T752">măn</ta>
            <ta e="T754" id="Seg_8283" s="T753">nanəʔzəbi</ta>
            <ta e="T755" id="Seg_8284" s="T754">i-bi-m</ta>
            <ta e="T756" id="Seg_8285" s="T755">koʔbdo</ta>
            <ta e="T757" id="Seg_8286" s="T756">dĭ</ta>
            <ta e="T758" id="Seg_8287" s="T757">dĭgəttə</ta>
            <ta e="T759" id="Seg_8288" s="T758">dĭ</ta>
            <ta e="T760" id="Seg_8289" s="T759">šo-bi</ta>
            <ta e="T761" id="Seg_8290" s="T760">văjna-gəʔ</ta>
            <ta e="T763" id="Seg_8291" s="T762">i-bi</ta>
            <ta e="T764" id="Seg_8292" s="T763">dĭgəttə</ta>
            <ta e="T765" id="Seg_8293" s="T764">ĭššo</ta>
            <ta e="T766" id="Seg_8294" s="T765">amno-bi-bAʔ</ta>
            <ta e="T767" id="Seg_8295" s="T766">măn</ta>
            <ta e="T768" id="Seg_8296" s="T767">bazoʔ</ta>
            <ta e="T769" id="Seg_8297" s="T768">nanəʔzəbi</ta>
            <ta e="T770" id="Seg_8298" s="T769">i-bi</ta>
            <ta e="T771" id="Seg_8299" s="T770">dĭgəttə</ta>
            <ta e="T772" id="Seg_8300" s="T771">dĭ</ta>
            <ta e="T774" id="Seg_8301" s="T773">dĭ</ta>
            <ta e="T775" id="Seg_8302" s="T774">măna</ta>
            <ta e="T776" id="Seg_8303" s="T775">sürer-bi</ta>
            <ta e="T777" id="Seg_8304" s="T776">i</ta>
            <ta e="T778" id="Seg_8305" s="T777">dĭ-m</ta>
            <ta e="T779" id="Seg_8306" s="T778">i-bi</ta>
            <ta e="T780" id="Seg_8307" s="T779">ĭššo</ta>
            <ta e="T781" id="Seg_8308" s="T780">onʼiʔ</ta>
            <ta e="T783" id="Seg_8309" s="T782">nʼi</ta>
            <ta e="T784" id="Seg_8310" s="T783">det-bi-m</ta>
            <ta e="T785" id="Seg_8311" s="T784">dĭgəttə</ta>
            <ta e="T786" id="Seg_8312" s="T785">maʔ-gənʼi</ta>
            <ta e="T787" id="Seg_8313" s="T786">šo-bi-m</ta>
            <ta e="T790" id="Seg_8314" s="T789">aba-Tə</ta>
            <ta e="T791" id="Seg_8315" s="T790">i</ta>
            <ta e="T792" id="Seg_8316" s="T791">ija-Tə</ta>
            <ta e="T793" id="Seg_8317" s="T792">dĭgəttə</ta>
            <ta e="T794" id="Seg_8318" s="T793">kü-laːm-bi-jəʔ</ta>
            <ta e="T795" id="Seg_8319" s="T794">koʔbdo</ta>
            <ta e="T796" id="Seg_8320" s="T795">i</ta>
            <ta e="T797" id="Seg_8321" s="T796">nʼi</ta>
            <ta e="T798" id="Seg_8322" s="T797">măn</ta>
            <ta e="T799" id="Seg_8323" s="T798">unʼə</ta>
            <ta e="T801" id="Seg_8324" s="T800">mo-laːm-bi-m</ta>
            <ta e="T802" id="Seg_8325" s="T801">oʔkʼe</ta>
            <ta e="T803" id="Seg_8326" s="T802">amno-bi-m</ta>
            <ta e="T804" id="Seg_8327" s="T803">bazoʔ</ta>
            <ta e="T805" id="Seg_8328" s="T804">kuza</ta>
            <ta e="T806" id="Seg_8329" s="T805">măna</ta>
            <ta e="T807" id="Seg_8330" s="T806">monoʔko-bi</ta>
            <ta e="T808" id="Seg_8331" s="T807">măn</ta>
            <ta e="T809" id="Seg_8332" s="T808">kan-bi-m</ta>
            <ta e="T810" id="Seg_8333" s="T809">dĭ-ziʔ</ta>
            <ta e="T811" id="Seg_8334" s="T810">amnə-bi-m</ta>
            <ta e="T812" id="Seg_8335" s="T811">oʔb</ta>
            <ta e="T813" id="Seg_8336" s="T812">šide</ta>
            <ta e="T814" id="Seg_8337" s="T813">nagur</ta>
            <ta e="T815" id="Seg_8338" s="T814">teʔdə</ta>
            <ta e="T816" id="Seg_8339" s="T815">sumna</ta>
            <ta e="T817" id="Seg_8340" s="T816">muktuʔ</ta>
            <ta e="T818" id="Seg_8341" s="T817">sejʔpü</ta>
            <ta e="T819" id="Seg_8342" s="T818">kö</ta>
            <ta e="T820" id="Seg_8343" s="T819">amno-bi-m</ta>
            <ta e="T821" id="Seg_8344" s="T820">šide</ta>
            <ta e="T822" id="Seg_8345" s="T821">ešši</ta>
            <ta e="T823" id="Seg_8346" s="T822">i-bi-jəʔ</ta>
            <ta e="T824" id="Seg_8347" s="T823">ugaːndə</ta>
            <ta e="T825" id="Seg_8348" s="T824">ej</ta>
            <ta e="T826" id="Seg_8349" s="T825">jakšə</ta>
            <ta e="T827" id="Seg_8350" s="T826">ara</ta>
            <ta e="T828" id="Seg_8351" s="T827">bĭs-bi</ta>
            <ta e="T829" id="Seg_8352" s="T828">tʼabəro-laʔbə-bi</ta>
            <ta e="T830" id="Seg_8353" s="T829">kudo-nzə-laʔbə-bi</ta>
            <ta e="T831" id="Seg_8354" s="T830">kamən</ta>
            <ta e="T832" id="Seg_8355" s="T831">kamən</ta>
            <ta e="T833" id="Seg_8356" s="T832">baʔbdə-luʔbdə-bi-m</ta>
            <ta e="T834" id="Seg_8357" s="T833">bar</ta>
            <ta e="T835" id="Seg_8358" s="T834">ulu-m</ta>
            <ta e="T836" id="Seg_8359" s="T835">bar</ta>
            <ta e="T837" id="Seg_8360" s="T836">eʔbdə-m</ta>
            <ta e="T838" id="Seg_8361" s="T837">săj-nʼeʔbdə-bi</ta>
            <ta e="T839" id="Seg_8362" s="T838">vekžen</ta>
            <ta e="T840" id="Seg_8363" s="T839">tăŋ</ta>
            <ta e="T841" id="Seg_8364" s="T840">münör-bi</ta>
            <ta e="T842" id="Seg_8365" s="T841">ažnə</ta>
            <ta e="T843" id="Seg_8366" s="T842">kem</ta>
            <ta e="T844" id="Seg_8367" s="T843">i-bi</ta>
            <ta e="T845" id="Seg_8368" s="T844">dĭgəttə</ta>
            <ta e="T846" id="Seg_8369" s="T845">măn</ta>
            <ta e="T847" id="Seg_8370" s="T846">nʼi-m</ta>
            <ta e="T848" id="Seg_8371" s="T847">dĭ-m</ta>
            <ta e="T849" id="Seg_8372" s="T848">barəʔ-luʔbdə-bi-m</ta>
            <ta e="T850" id="Seg_8373" s="T849">măn</ta>
            <ta e="T851" id="Seg_8374" s="T850">onʼiʔ</ta>
            <ta e="T852" id="Seg_8375" s="T851">nʼi-m</ta>
            <ta e="T853" id="Seg_8376" s="T852">i-bi</ta>
            <ta e="T854" id="Seg_8377" s="T853">tüj</ta>
            <ta e="T855" id="Seg_8378" s="T854">măn-ntə-m</ta>
            <ta e="T856" id="Seg_8379" s="T855">šində-Tə=də</ta>
            <ta e="T858" id="Seg_8380" s="T856">ej</ta>
            <ta e="T859" id="Seg_8381" s="T858">tibi-Tə</ta>
            <ta e="T860" id="Seg_8382" s="T859">unʼə</ta>
            <ta e="T861" id="Seg_8383" s="T860">amno-lV-m</ta>
            <ta e="T862" id="Seg_8384" s="T861">nʼi-ziʔ</ta>
            <ta e="T863" id="Seg_8385" s="T862">bos-də</ta>
            <ta e="T864" id="Seg_8386" s="T863">nʼi-ziʔ</ta>
            <ta e="T865" id="Seg_8387" s="T864">dĭgəttə</ta>
            <ta e="T866" id="Seg_8388" s="T865">onʼiʔ</ta>
            <ta e="T867" id="Seg_8389" s="T866">nʼi</ta>
            <ta e="T868" id="Seg_8390" s="T867">davaj</ta>
            <ta e="T869" id="Seg_8391" s="T868">măna</ta>
            <ta e="T870" id="Seg_8392" s="T869">monoʔko-zittə</ta>
            <ta e="T871" id="Seg_8393" s="T870">măn</ta>
            <ta e="T875" id="Seg_8394" s="T874">ej</ta>
            <ta e="T876" id="Seg_8395" s="T875">kuvando-liA-m</ta>
            <ta e="T877" id="Seg_8396" s="T876">kadul-m</ta>
            <ta e="T878" id="Seg_8397" s="T877">kadul-də</ta>
            <ta e="T879" id="Seg_8398" s="T878">nʼešpək</ta>
            <ta e="T880" id="Seg_8399" s="T879">ej</ta>
            <ta e="T881" id="Seg_8400" s="T880">kan-lV-m</ta>
            <ta e="T882" id="Seg_8401" s="T881">dĭ</ta>
            <ta e="T883" id="Seg_8402" s="T882">măn-ntə</ta>
            <ta e="T884" id="Seg_8403" s="T883">davaj</ta>
            <ta e="T885" id="Seg_8404" s="T884">amno-zittə</ta>
            <ta e="T886" id="Seg_8405" s="T885">tăn-ziʔ</ta>
            <ta e="T1055" id="Seg_8406" s="T886">nu</ta>
            <ta e="T887" id="Seg_8407" s="T1055">tenö-bi-m</ta>
            <ta e="T888" id="Seg_8408" s="T887">tenö-bi-m</ta>
            <ta e="T889" id="Seg_8409" s="T888">dĭgəttə</ta>
            <ta e="T890" id="Seg_8410" s="T889">kan-bi-m</ta>
            <ta e="T891" id="Seg_8411" s="T890">dĭgəttə</ta>
            <ta e="T892" id="Seg_8412" s="T891">bazoʔ</ta>
            <ta e="T894" id="Seg_8413" s="T893">barəʔ-lV-m</ta>
            <ta e="T895" id="Seg_8414" s="T894">măn-ntə-m</ta>
            <ta e="T896" id="Seg_8415" s="T895">tănan</ta>
            <ta e="T897" id="Seg_8416" s="T896">ej</ta>
            <ta e="T898" id="Seg_8417" s="T897">amno-lV-m</ta>
            <ta e="T899" id="Seg_8418" s="T898">tăn-ziʔ</ta>
            <ta e="T900" id="Seg_8419" s="T899">dĭbər</ta>
            <ta e="T901" id="Seg_8420" s="T900">davaj</ta>
            <ta e="T902" id="Seg_8421" s="T901">tʼor-zittə</ta>
            <ta e="T903" id="Seg_8422" s="T902">tăŋ</ta>
            <ta e="T904" id="Seg_8423" s="T903">tʼor-bi</ta>
            <ta e="T905" id="Seg_8424" s="T904">ugaːndə</ta>
            <ta e="T906" id="Seg_8425" s="T905">dĭgəttə</ta>
            <ta e="T907" id="Seg_8426" s="T906">măn</ta>
            <ta e="T908" id="Seg_8427" s="T907">măn-liA-m</ta>
            <ta e="T910" id="Seg_8428" s="T909">tura</ta>
            <ta e="T911" id="Seg_8429" s="T910">nuldə-bə-j</ta>
            <ta e="T912" id="Seg_8430" s="T911">dĭgəttə</ta>
            <ta e="T913" id="Seg_8431" s="T912">pušaj</ta>
            <ta e="T914" id="Seg_8432" s="T913">kan-lV-j</ta>
            <ta e="T915" id="Seg_8433" s="T914">i</ta>
            <ta e="T916" id="Seg_8434" s="T915">dărəʔ</ta>
            <ta e="T917" id="Seg_8435" s="T916">i-bi</ta>
            <ta e="T918" id="Seg_8436" s="T917">tura</ta>
            <ta e="T920" id="Seg_8437" s="T919">nuldə-bi</ta>
            <ta e="T921" id="Seg_8438" s="T920">i</ta>
            <ta e="T1058" id="Seg_8439" s="T921">kan-lAʔ</ta>
            <ta e="T922" id="Seg_8440" s="T1058">tʼür-bi</ta>
            <ta e="T923" id="Seg_8441" s="T922">măn</ta>
            <ta e="T924" id="Seg_8442" s="T923">unʼə</ta>
            <ta e="T926" id="Seg_8443" s="T925">tüj</ta>
            <ta e="T927" id="Seg_8444" s="T926">măn</ta>
            <ta e="T928" id="Seg_8445" s="T927">üge</ta>
            <ta e="T929" id="Seg_8446" s="T928">unʼə</ta>
            <ta e="T930" id="Seg_8447" s="T929">amno-laʔbə-m</ta>
            <ta e="T931" id="Seg_8448" s="T930">amno-laʔbə-m</ta>
            <ta e="T932" id="Seg_8449" s="T931">už</ta>
            <ta e="T933" id="Seg_8450" s="T932">šide</ta>
            <ta e="T934" id="Seg_8451" s="T933">biəʔ</ta>
            <ta e="T935" id="Seg_8452" s="T934">ki</ta>
            <ta e="T936" id="Seg_8453" s="T935">amno-laʔbə-m</ta>
            <ta e="T937" id="Seg_8454" s="T936">kamən</ta>
            <ta e="T938" id="Seg_8455" s="T937">dĭ</ta>
            <ta e="T939" id="Seg_8456" s="T938">barəʔ-luʔbdə-bi</ta>
            <ta e="T940" id="Seg_8457" s="T939">dĭgəttə</ta>
            <ta e="T941" id="Seg_8458" s="T940">miʔ</ta>
            <ta e="T942" id="Seg_8459" s="T941">dĭ</ta>
            <ta e="T943" id="Seg_8460" s="T942">nʼi-ziʔ</ta>
            <ta e="T944" id="Seg_8461" s="T943">bar</ta>
            <ta e="T945" id="Seg_8462" s="T944">pa</ta>
            <ta e="T946" id="Seg_8463" s="T945">baltu-ziʔ</ta>
            <ta e="T947" id="Seg_8464" s="T946">hʼaʔ-bi-bAʔ</ta>
            <ta e="T948" id="Seg_8465" s="T947">tura</ta>
            <ta e="T950" id="Seg_8466" s="T949">nuldə-zittə</ta>
            <ta e="T952" id="Seg_8467" s="T951">tažor-bi-bAʔ</ta>
            <ta e="T954" id="Seg_8468" s="T953">ine-ziʔ</ta>
            <ta e="T955" id="Seg_8469" s="T954">i</ta>
            <ta e="T956" id="Seg_8470" s="T955">tura</ta>
            <ta e="T957" id="Seg_8471" s="T956">nuldə-bi-bAʔ</ta>
            <ta e="T958" id="Seg_8472" s="T957">tibi-zAŋ</ta>
            <ta e="T959" id="Seg_8473" s="T958">hʼaʔ-bi-jəʔ</ta>
            <ta e="T960" id="Seg_8474" s="T959">baltu-ziʔ</ta>
            <ta e="T961" id="Seg_8475" s="T960">a</ta>
            <ta e="T962" id="Seg_8476" s="T961">măn</ta>
            <ta e="T963" id="Seg_8477" s="T962">kuba-jəʔ</ta>
            <ta e="T964" id="Seg_8478" s="T963">dĭ-Tə</ta>
            <ta e="T965" id="Seg_8479" s="T964">a-bi-m</ta>
            <ta e="T966" id="Seg_8480" s="T965">dĭgəttə</ta>
            <ta e="T967" id="Seg_8481" s="T966">tsutonka-jəʔ</ta>
            <ta e="T969" id="Seg_8482" s="T968">tažor-bi-jəʔ</ta>
            <ta e="T971" id="Seg_8483" s="T970">a-zittə</ta>
            <ta e="T972" id="Seg_8484" s="T971">i</ta>
            <ta e="T974" id="Seg_8485" s="T973">a-zittə</ta>
            <ta e="T975" id="Seg_8486" s="T974">a</ta>
            <ta e="T976" id="Seg_8487" s="T975">măn</ta>
            <ta e="T977" id="Seg_8488" s="T976">kuda</ta>
            <ta e="T978" id="Seg_8489" s="T977">urgo</ta>
            <ta e="T979" id="Seg_8490" s="T978">kuba-jəʔ</ta>
            <ta e="T980" id="Seg_8491" s="T979">a-bi-m</ta>
            <ta e="T981" id="Seg_8492" s="T980">i</ta>
            <ta e="T982" id="Seg_8493" s="T981">jama-jəʔ</ta>
            <ta e="T983" id="Seg_8494" s="T982">šöʔ-bi-m</ta>
            <ta e="T984" id="Seg_8495" s="T983">a</ta>
            <ta e="T985" id="Seg_8496" s="T984">dĭ</ta>
            <ta e="T986" id="Seg_8497" s="T985">tože</ta>
            <ta e="T987" id="Seg_8498" s="T986">măna</ta>
            <ta e="T988" id="Seg_8499" s="T987">kabažar-bi</ta>
            <ta e="T989" id="Seg_8500" s="T988">kuba-t</ta>
            <ta e="T990" id="Seg_8501" s="T989">edə-m-bi</ta>
            <ta e="T991" id="Seg_8502" s="T990">da</ta>
            <ta e="T992" id="Seg_8503" s="T991">ej</ta>
            <ta e="T993" id="Seg_8504" s="T992">tĭmne-bi</ta>
            <ta e="T994" id="Seg_8505" s="T993">măn</ta>
            <ta e="T995" id="Seg_8506" s="T994">dĭ-m</ta>
            <ta e="T996" id="Seg_8507" s="T995">tüšə-lə-bi-m</ta>
            <ta e="T997" id="Seg_8508" s="T996">măn</ta>
            <ta e="T998" id="Seg_8509" s="T997">bos-də</ta>
            <ta e="T1000" id="Seg_8510" s="T999">tibi-Tə</ta>
            <ta e="T1001" id="Seg_8511" s="T1000">jama-jəʔ</ta>
            <ta e="T1002" id="Seg_8512" s="T1001">šöʔ-bi-m</ta>
            <ta e="T1003" id="Seg_8513" s="T1002">i</ta>
            <ta e="T1004" id="Seg_8514" s="T1003">parga</ta>
            <ta e="T1005" id="Seg_8515" s="T1004">šöʔ-bi-m</ta>
            <ta e="T1006" id="Seg_8516" s="T1005">ular-n</ta>
            <ta e="T1007" id="Seg_8517" s="T1006">kuba-jəʔ</ta>
            <ta e="T1008" id="Seg_8518" s="T1007">i</ta>
            <ta e="T1009" id="Seg_8519" s="T1008">üžü</ta>
            <ta e="T1010" id="Seg_8520" s="T1009">šöʔ-bi-m</ta>
            <ta e="T1011" id="Seg_8521" s="T1010">kujnek-də</ta>
            <ta e="T1012" id="Seg_8522" s="T1011">šöʔ-bi-m</ta>
            <ta e="T1013" id="Seg_8523" s="T1012">piʔme-jəʔ</ta>
            <ta e="T1014" id="Seg_8524" s="T1013">šöʔ-bi-m</ta>
            <ta e="T1015" id="Seg_8525" s="T1014">dĭ-Tə</ta>
            <ta e="T1016" id="Seg_8526" s="T1015">šer-bi-m</ta>
            <ta e="T1017" id="Seg_8527" s="T1016">ugaːndə</ta>
            <ta e="T1056" id="Seg_8528" s="T1017">jakšə</ta>
            <ta e="T1019" id="Seg_8529" s="T1018">šer-bi-m</ta>
            <ta e="T1021" id="Seg_8530" s="T1020">il</ta>
            <ta e="T1022" id="Seg_8531" s="T1021">bar</ta>
            <ta e="T1023" id="Seg_8532" s="T1022">măndo-r-bi-jəʔ</ta>
            <ta e="T1024" id="Seg_8533" s="T1023">ugaːndə</ta>
            <ta e="T1025" id="Seg_8534" s="T1024">kuvas</ta>
            <ta e="T1026" id="Seg_8535" s="T1025">kuza</ta>
            <ta e="T1027" id="Seg_8536" s="T1026">mo-laːm-bi</ta>
            <ta e="T1028" id="Seg_8537" s="T1027">dĭgəttə</ta>
            <ta e="T1029" id="Seg_8538" s="T1028">miʔ</ta>
            <ta e="T1030" id="Seg_8539" s="T1029">tʼo</ta>
            <ta e="T1033" id="Seg_8540" s="T1032">tajər-laʔbə-bi-bAʔ</ta>
            <ta e="T1034" id="Seg_8541" s="T1033">aš</ta>
            <ta e="T1035" id="Seg_8542" s="T1034">kuʔ-bi-bAʔ</ta>
            <ta e="T1037" id="Seg_8543" s="T1036">özer-bi</ta>
            <ta e="T1038" id="Seg_8544" s="T1037">ugaːndə</ta>
            <ta e="T1039" id="Seg_8545" s="T1038">jakšə</ta>
            <ta e="T1040" id="Seg_8546" s="T1039">i-bi</ta>
            <ta e="T1041" id="Seg_8547" s="T1040">aš</ta>
            <ta e="T1042" id="Seg_8548" s="T1041">dĭgəttə</ta>
            <ta e="T1043" id="Seg_8549" s="T1042">un</ta>
            <ta e="T1044" id="Seg_8550" s="T1043">a-bi-bAʔ</ta>
            <ta e="T1045" id="Seg_8551" s="T1044">tʼermən-də</ta>
            <ta e="T1046" id="Seg_8552" s="T1045">mĭn-bi-bAʔ</ta>
            <ta e="T1047" id="Seg_8553" s="T1046">ipek</ta>
            <ta e="T1048" id="Seg_8554" s="T1047">pür-bi-bAʔ</ta>
            <ta e="T1049" id="Seg_8555" s="T1048">i</ta>
            <ta e="T1050" id="Seg_8556" s="T1049">budəj</ta>
            <ta e="T1051" id="Seg_8557" s="T1050">aš</ta>
            <ta e="T1052" id="Seg_8558" s="T1051">kuʔ-bi-bAʔ</ta>
            <ta e="T1053" id="Seg_8559" s="T1052">jakšə</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_8560" s="T0">you.NOM</ta>
            <ta e="T2" id="Seg_8561" s="T1">yesterday</ta>
            <ta e="T4" id="Seg_8562" s="T3">butter.[NOM.SG]</ta>
            <ta e="T5" id="Seg_8563" s="T4">take-PST-2SG</ta>
            <ta e="T6" id="Seg_8564" s="T5">so</ta>
            <ta e="T7" id="Seg_8565" s="T6">hang.up-PST-2SG</ta>
            <ta e="T9" id="Seg_8566" s="T8">hang.up-PST-1SG</ta>
            <ta e="T10" id="Seg_8567" s="T9">and</ta>
            <ta e="T11" id="Seg_8568" s="T10">how.much</ta>
            <ta e="T12" id="Seg_8569" s="T11">five.[NOM.SG]</ta>
            <ta e="T13" id="Seg_8570" s="T12">gramm.[NOM.SG]</ta>
            <ta e="T14" id="Seg_8571" s="T13">money.[NOM.SG]</ta>
            <ta e="T15" id="Seg_8572" s="T14">bring-PST-2SG</ta>
            <ta e="T16" id="Seg_8573" s="T15">bring-PST-1SG</ta>
            <ta e="T17" id="Seg_8574" s="T16">two.[NOM.SG]</ta>
            <ta e="T19" id="Seg_8575" s="T18">two.[NOM.SG]</ta>
            <ta e="T20" id="Seg_8576" s="T19">rouble.[NOM.SG]</ta>
            <ta e="T21" id="Seg_8577" s="T20">speak-DUR-1PL</ta>
            <ta e="T22" id="Seg_8578" s="T21">and</ta>
            <ta e="T23" id="Seg_8579" s="T22">laugh-DUR-1PL</ta>
            <ta e="T24" id="Seg_8580" s="T23">and</ta>
            <ta e="T26" id="Seg_8581" s="T25">vodka.[NOM.SG]</ta>
            <ta e="T27" id="Seg_8582" s="T26">drink-DUR-1PL</ta>
            <ta e="T28" id="Seg_8583" s="T27">and</ta>
            <ta e="T29" id="Seg_8584" s="T28">bread.[NOM.SG]</ta>
            <ta e="T30" id="Seg_8585" s="T29">eat-PRS.[3SG]</ta>
            <ta e="T31" id="Seg_8586" s="T30">and</ta>
            <ta e="T32" id="Seg_8587" s="T31">meat.[NOM.SG]</ta>
            <ta e="T33" id="Seg_8588" s="T32">eat-DUR-1PL</ta>
            <ta e="T34" id="Seg_8589" s="T33">and</ta>
            <ta e="T35" id="Seg_8590" s="T34">tobacco</ta>
            <ta e="T36" id="Seg_8591" s="T35">smoke-DUR-1PL</ta>
            <ta e="T37" id="Seg_8592" s="T36">PTCL</ta>
            <ta e="T38" id="Seg_8593" s="T37">we.NOM</ta>
            <ta e="T39" id="Seg_8594" s="T38">edge-LAT/LOC.3SG</ta>
            <ta e="T40" id="Seg_8595" s="T39">man.[NOM.SG]</ta>
            <ta e="T41" id="Seg_8596" s="T40">live-DUR.[3SG]</ta>
            <ta e="T42" id="Seg_8597" s="T41">this-GEN</ta>
            <ta e="T43" id="Seg_8598" s="T42">boy.[NOM.SG]</ta>
            <ta e="T44" id="Seg_8599" s="T43">we.LAT</ta>
            <ta e="T45" id="Seg_8600" s="T44">come-PST.[3SG]</ta>
            <ta e="T46" id="Seg_8601" s="T45">drawer-LAT</ta>
            <ta e="T47" id="Seg_8602" s="T46">creep.into-PST.[3SG]</ta>
            <ta e="T48" id="Seg_8603" s="T47">and</ta>
            <ta e="T49" id="Seg_8604" s="T48">wardrobe-LAT</ta>
            <ta e="T50" id="Seg_8605" s="T49">creep.into-PST.[3SG]</ta>
            <ta e="T51" id="Seg_8606" s="T50">I.NOM</ta>
            <ta e="T52" id="Seg_8607" s="T51">say-PST-1SG</ta>
            <ta e="T53" id="Seg_8608" s="T52">say-PST-1SG</ta>
            <ta e="T54" id="Seg_8609" s="T53">what.[NOM.SG]</ta>
            <ta e="T56" id="Seg_8610" s="T55">I.LAT</ta>
            <ta e="T57" id="Seg_8611" s="T56">Vanka.[NOM.SG]</ta>
            <ta e="T59" id="Seg_8612" s="T58">look-FRQ-INF.LAT</ta>
            <ta e="T60" id="Seg_8613" s="T59">I.NOM</ta>
            <ta e="T61" id="Seg_8614" s="T60">this-ACC</ta>
            <ta e="T62" id="Seg_8615" s="T61">drive-MOM-PST-1SG</ta>
            <ta e="T63" id="Seg_8616" s="T62">say-PRS.[3SG]</ta>
            <ta e="T64" id="Seg_8617" s="T63">Misha-LAT</ta>
            <ta e="T65" id="Seg_8618" s="T64">go-EP-IMP.2SG</ta>
            <ta e="T66" id="Seg_8619" s="T65">hence</ta>
            <ta e="T67" id="Seg_8620" s="T66">then</ta>
            <ta e="T68" id="Seg_8621" s="T67">today</ta>
            <ta e="T69" id="Seg_8622" s="T68">come-PST.[3SG]</ta>
            <ta e="T70" id="Seg_8623" s="T69">sack.[NOM.SG]</ta>
            <ta e="T71" id="Seg_8624" s="T70">pine.nut-INS</ta>
            <ta e="T72" id="Seg_8625" s="T71">stand-PRS.[3SG]</ta>
            <ta e="T73" id="Seg_8626" s="T72">this.[NOM.SG]</ta>
            <ta e="T74" id="Seg_8627" s="T73">knife.[NOM.SG]</ta>
            <ta e="T75" id="Seg_8628" s="T74">take-PST.[3SG]</ta>
            <ta e="T76" id="Seg_8629" s="T75">and</ta>
            <ta e="T77" id="Seg_8630" s="T76">sack.[NOM.SG]</ta>
            <ta e="T78" id="Seg_8631" s="T77">cut-PST.[3SG]</ta>
            <ta e="T79" id="Seg_8632" s="T78">pine.nut.[NOM.SG]</ta>
            <ta e="T80" id="Seg_8633" s="T79">take-PST.[3SG]</ta>
            <ta e="T81" id="Seg_8634" s="T80">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T82" id="Seg_8635" s="T81">throw.away-MOM-PST.[3SG]</ta>
            <ta e="T83" id="Seg_8636" s="T82">sugar.[NOM.SG]</ta>
            <ta e="T84" id="Seg_8637" s="T83">we.NOM</ta>
            <ta e="T85" id="Seg_8638" s="T84">house-LOC</ta>
            <ta e="T86" id="Seg_8639" s="T85">woman.[NOM.SG]</ta>
            <ta e="T87" id="Seg_8640" s="T86">run-MOM-PST.[3SG]</ta>
            <ta e="T89" id="Seg_8641" s="T88">%%</ta>
            <ta e="T90" id="Seg_8642" s="T89">steal-PST.[3SG]</ta>
            <ta e="T91" id="Seg_8643" s="T90">pine.nut.[NOM.SG]</ta>
            <ta e="T92" id="Seg_8644" s="T91">and</ta>
            <ta e="T93" id="Seg_8645" s="T92">this.[NOM.SG]</ta>
            <ta e="T94" id="Seg_8646" s="T93">PTCL</ta>
            <ta e="T95" id="Seg_8647" s="T94">show-MOM.PRS.[3SG]</ta>
            <ta e="T96" id="Seg_8648" s="T95">that.[NOM.SG]</ta>
            <ta e="T97" id="Seg_8649" s="T96">pine.nut.[NOM.SG]</ta>
            <ta e="T98" id="Seg_8650" s="T97">and</ta>
            <ta e="T99" id="Seg_8651" s="T98">this.[NOM.SG]</ta>
            <ta e="T100" id="Seg_8652" s="T99">say-PST.[3SG]</ta>
            <ta e="T101" id="Seg_8653" s="T100">I.NOM</ta>
            <ta e="T102" id="Seg_8654" s="T101">this-ACC</ta>
            <ta e="T103" id="Seg_8655" s="T102">kill-FUT-1SG</ta>
            <ta e="T104" id="Seg_8656" s="T103">and</ta>
            <ta e="T105" id="Seg_8657" s="T104">what.[NOM.SG]</ta>
            <ta e="T106" id="Seg_8658" s="T105">NEG</ta>
            <ta e="T107" id="Seg_8659" s="T106">say-PST-2PL</ta>
            <ta e="T109" id="Seg_8660" s="T108">yesterday</ta>
            <ta e="T110" id="Seg_8661" s="T109">I.LAT</ta>
            <ta e="T111" id="Seg_8662" s="T110">call-PST-3PL</ta>
            <ta e="T113" id="Seg_8663" s="T112">I.NOM</ta>
            <ta e="T115" id="Seg_8664" s="T114">relative-NOM/GEN/ACC.3SG</ta>
            <ta e="T116" id="Seg_8665" s="T115">come-IMP.2SG</ta>
            <ta e="T117" id="Seg_8666" s="T116">we.LAT</ta>
            <ta e="T118" id="Seg_8667" s="T117">eat-INF.LAT</ta>
            <ta e="T120" id="Seg_8668" s="T119">child.[NOM.SG]</ta>
            <ta e="T122" id="Seg_8669" s="T121">die-RES-PST.[3SG]</ta>
            <ta e="T123" id="Seg_8670" s="T122">%%</ta>
            <ta e="T124" id="Seg_8671" s="T123">go-PST.[3SG]</ta>
            <ta e="T125" id="Seg_8672" s="T124">and</ta>
            <ta e="T126" id="Seg_8673" s="T125">I.NOM</ta>
            <ta e="T127" id="Seg_8674" s="T126">say-PST-1SG</ta>
            <ta e="T128" id="Seg_8675" s="T127">come-FUT-1SG</ta>
            <ta e="T129" id="Seg_8676" s="T128">and</ta>
            <ta e="T131" id="Seg_8677" s="T130">and</ta>
            <ta e="T132" id="Seg_8678" s="T131">self-NOM/GEN/ACC.1SG</ta>
            <ta e="T133" id="Seg_8679" s="T132">NEG</ta>
            <ta e="T134" id="Seg_8680" s="T133">go-PST-1SG</ta>
            <ta e="T135" id="Seg_8681" s="T134">daughter_in_law-NOM/GEN/ACC.1SG</ta>
            <ta e="T136" id="Seg_8682" s="T135">call-PST-1SG</ta>
            <ta e="T137" id="Seg_8683" s="T136">call-PST-1SG</ta>
            <ta e="T138" id="Seg_8684" s="T137">this.[NOM.SG]</ta>
            <ta e="T139" id="Seg_8685" s="T138">NEG</ta>
            <ta e="T140" id="Seg_8686" s="T139">go-PST.[3SG]</ta>
            <ta e="T141" id="Seg_8687" s="T140">and</ta>
            <ta e="T142" id="Seg_8688" s="T141">I.NOM</ta>
            <ta e="T143" id="Seg_8689" s="T142">NEG</ta>
            <ta e="T144" id="Seg_8690" s="T143">go-PST-1SG</ta>
            <ta e="T145" id="Seg_8691" s="T144">there</ta>
            <ta e="T146" id="Seg_8692" s="T145">very</ta>
            <ta e="T147" id="Seg_8693" s="T146">vodka-ADJZ.[NOM.SG]</ta>
            <ta e="T148" id="Seg_8694" s="T147">and</ta>
            <ta e="T149" id="Seg_8695" s="T148">I.LAT</ta>
            <ta e="T150" id="Seg_8696" s="T149">vodka.[NOM.SG]</ta>
            <ta e="T151" id="Seg_8697" s="T150">NEG</ta>
            <ta e="T152" id="Seg_8698" s="T151">one.needs</ta>
            <ta e="T153" id="Seg_8699" s="T152">I.NOM</ta>
            <ta e="T156" id="Seg_8700" s="T155">this-ACC</ta>
            <ta e="T157" id="Seg_8701" s="T156">drink-INF.LAT</ta>
            <ta e="T158" id="Seg_8702" s="T157">and</ta>
            <ta e="T159" id="Seg_8703" s="T158">this.[NOM.SG]</ta>
            <ta e="T160" id="Seg_8704" s="T159">be.angry-MOM-PST.[3SG]</ta>
            <ta e="T161" id="Seg_8705" s="T160">PTCL</ta>
            <ta e="T162" id="Seg_8706" s="T161">say-IPFVZ.[3SG]</ta>
            <ta e="T163" id="Seg_8707" s="T162">grandmother-NOM/GEN/ACC.1SG</ta>
            <ta e="T164" id="Seg_8708" s="T163">even</ta>
            <ta e="T165" id="Seg_8709" s="T164">%%</ta>
            <ta e="T166" id="Seg_8710" s="T165">see-PST-1SG</ta>
            <ta e="T167" id="Seg_8711" s="T166">now</ta>
            <ta e="T168" id="Seg_8712" s="T167">calf-NOM/GEN/ACC.2PL</ta>
            <ta e="T169" id="Seg_8713" s="T168">PTCL</ta>
            <ta e="T170" id="Seg_8714" s="T169">study-PST</ta>
            <ta e="T171" id="Seg_8715" s="T170">you.PL.ACC</ta>
            <ta e="T172" id="Seg_8716" s="T171">lie.down-DUR.[3SG]</ta>
            <ta e="T173" id="Seg_8717" s="T172">sit-IMP.2SG</ta>
            <ta e="T174" id="Seg_8718" s="T173">lie.down-DUR.[3SG]</ta>
            <ta e="T175" id="Seg_8719" s="T174">NEG</ta>
            <ta e="T176" id="Seg_8720" s="T175">shout-DUR.[3SG]</ta>
            <ta e="T177" id="Seg_8721" s="T176">today</ta>
            <ta e="T178" id="Seg_8722" s="T177">warm.[NOM.SG]</ta>
            <ta e="T179" id="Seg_8723" s="T178">become-RES-PST.[3SG]</ta>
            <ta e="T180" id="Seg_8724" s="T179">snow.[NOM.SG]</ta>
            <ta e="T181" id="Seg_8725" s="T180">PTCL</ta>
            <ta e="T182" id="Seg_8726" s="T181">%soft</ta>
            <ta e="T183" id="Seg_8727" s="T182">be-PST.[3SG]</ta>
            <ta e="T184" id="Seg_8728" s="T183">snow.[NOM.SG]</ta>
            <ta e="T186" id="Seg_8729" s="T185">soon</ta>
            <ta e="T187" id="Seg_8730" s="T186">water.[NOM.SG]</ta>
            <ta e="T188" id="Seg_8731" s="T187">become-RES-2DU</ta>
            <ta e="T189" id="Seg_8732" s="T188">then</ta>
            <ta e="T190" id="Seg_8733" s="T189">grass.[NOM.SG]</ta>
            <ta e="T191" id="Seg_8734" s="T190">grow-FUT-3SG</ta>
            <ta e="T193" id="Seg_8735" s="T192">flower-PL</ta>
            <ta e="T194" id="Seg_8736" s="T193">grow-FUT-3PL</ta>
            <ta e="T195" id="Seg_8737" s="T194">tree-PL</ta>
            <ta e="T196" id="Seg_8738" s="T195">PTCL</ta>
            <ta e="T197" id="Seg_8739" s="T196">beautiful.[NOM.SG]</ta>
            <ta e="T198" id="Seg_8740" s="T197">become-FUT-3PL</ta>
            <ta e="T199" id="Seg_8741" s="T198">green-PL</ta>
            <ta e="T200" id="Seg_8742" s="T199">become-FUT-3PL</ta>
            <ta e="T201" id="Seg_8743" s="T200">PTCL</ta>
            <ta e="T202" id="Seg_8744" s="T201">then</ta>
            <ta e="T203" id="Seg_8745" s="T202">horse-PL</ta>
            <ta e="T204" id="Seg_8746" s="T203">cow-PL</ta>
            <ta e="T205" id="Seg_8747" s="T204">sheep-NOM/GEN/ACC.3SG</ta>
            <ta e="T206" id="Seg_8748" s="T205">go-FUT-3PL</ta>
            <ta e="T207" id="Seg_8749" s="T206">grass.[NOM.SG]</ta>
            <ta e="T208" id="Seg_8750" s="T207">eat-INF.LAT</ta>
            <ta e="T209" id="Seg_8751" s="T208">then</ta>
            <ta e="T210" id="Seg_8752" s="T209">grass.[NOM.SG]</ta>
            <ta e="T211" id="Seg_8753" s="T210">big.[NOM.SG]</ta>
            <ta e="T212" id="Seg_8754" s="T211">grow-FUT-3SG</ta>
            <ta e="T213" id="Seg_8755" s="T212">then</ta>
            <ta e="T214" id="Seg_8756" s="T213">this-ACC</ta>
            <ta e="T216" id="Seg_8757" s="T215">scythe-INS</ta>
            <ta e="T217" id="Seg_8758" s="T216">cut-INF.LAT</ta>
            <ta e="T218" id="Seg_8759" s="T217">one.should</ta>
            <ta e="T219" id="Seg_8760" s="T218">then</ta>
            <ta e="T220" id="Seg_8761" s="T219">sun-LOC</ta>
            <ta e="T221" id="Seg_8762" s="T220">become.dry-FUT-3SG</ta>
            <ta e="T222" id="Seg_8763" s="T221">and</ta>
            <ta e="T223" id="Seg_8764" s="T222">collect-INF.LAT</ta>
            <ta e="T224" id="Seg_8765" s="T223">one.should</ta>
            <ta e="T225" id="Seg_8766" s="T224">people.[NOM.SG]</ta>
            <ta e="T226" id="Seg_8767" s="T225">go-PRS-3PL</ta>
            <ta e="T227" id="Seg_8768" s="T226">very</ta>
            <ta e="T228" id="Seg_8769" s="T227">good</ta>
            <ta e="T229" id="Seg_8770" s="T228">year.[NOM.SG]</ta>
            <ta e="T230" id="Seg_8771" s="T229">come-PST.[3SG]</ta>
            <ta e="T231" id="Seg_8772" s="T230">very</ta>
            <ta e="T233" id="Seg_8773" s="T232">warm.[NOM.SG]</ta>
            <ta e="T234" id="Seg_8774" s="T233">I.NOM</ta>
            <ta e="T235" id="Seg_8775" s="T234">%%</ta>
            <ta e="T236" id="Seg_8776" s="T235">can-PST-1SG</ta>
            <ta e="T237" id="Seg_8777" s="T236">sleep-INF.LAT</ta>
            <ta e="T238" id="Seg_8778" s="T237">then</ta>
            <ta e="T239" id="Seg_8779" s="T238">morning</ta>
            <ta e="T240" id="Seg_8780" s="T239">get.up-PST-1SG</ta>
            <ta e="T241" id="Seg_8781" s="T240">and</ta>
            <ta e="T242" id="Seg_8782" s="T241">day.[NOM.SG]</ta>
            <ta e="T243" id="Seg_8783" s="T242">day-ACC</ta>
            <ta e="T244" id="Seg_8784" s="T243">PTCL</ta>
            <ta e="T245" id="Seg_8785" s="T244">work-PST-1SG</ta>
            <ta e="T246" id="Seg_8786" s="T245">cow.[NOM.SG]</ta>
            <ta e="T247" id="Seg_8787" s="T246">milk-PST-1SG</ta>
            <ta e="T248" id="Seg_8788" s="T247">and</ta>
            <ta e="T249" id="Seg_8789" s="T248">boil-PST-1SG</ta>
            <ta e="T250" id="Seg_8790" s="T249">make-PST-1SG</ta>
            <ta e="T251" id="Seg_8791" s="T250">then</ta>
            <ta e="T252" id="Seg_8792" s="T251">speak-PST-1SG</ta>
            <ta e="T253" id="Seg_8793" s="T252">early</ta>
            <ta e="T254" id="Seg_8794" s="T253">collect-PST-1SG</ta>
            <ta e="T255" id="Seg_8795" s="T254">and</ta>
            <ta e="T256" id="Seg_8796" s="T255">fire.[NOM.SG]</ta>
            <ta e="T257" id="Seg_8797" s="T256">light-PST-1SG</ta>
            <ta e="T258" id="Seg_8798" s="T257">I.LAT</ta>
            <ta e="T259" id="Seg_8799" s="T258">Aginskoe</ta>
            <ta e="T260" id="Seg_8800" s="T259">settlement-LAT</ta>
            <ta e="T261" id="Seg_8801" s="T260">one.should</ta>
            <ta e="T262" id="Seg_8802" s="T261">go-INF.LAT</ta>
            <ta e="T263" id="Seg_8803" s="T262">I.LAT</ta>
            <ta e="T264" id="Seg_8804" s="T263">money.[NOM.SG]</ta>
            <ta e="T265" id="Seg_8805" s="T264">lie-DUR.[3SG]</ta>
            <ta e="T266" id="Seg_8806" s="T265">bank-EP-GEN</ta>
            <ta e="T267" id="Seg_8807" s="T266">take-INF.LAT</ta>
            <ta e="T268" id="Seg_8808" s="T267">one.should</ta>
            <ta e="T269" id="Seg_8809" s="T268">and</ta>
            <ta e="T270" id="Seg_8810" s="T269">people-NOM/GEN/ACC.3SG</ta>
            <ta e="T271" id="Seg_8811" s="T270">give-INF.LAT</ta>
            <ta e="T272" id="Seg_8812" s="T271">I.NOM</ta>
            <ta e="T273" id="Seg_8813" s="T272">this-PL-LOC</ta>
            <ta e="T274" id="Seg_8814" s="T273">take-PST-1SG</ta>
            <ta e="T275" id="Seg_8815" s="T274">I.NOM</ta>
            <ta e="T276" id="Seg_8816" s="T275">many</ta>
            <ta e="T277" id="Seg_8817" s="T276">speak-PRS-1SG</ta>
            <ta e="T279" id="Seg_8818" s="T278">now</ta>
            <ta e="T281" id="Seg_8819" s="T280">speak-DUR-3PL</ta>
            <ta e="T282" id="Seg_8820" s="T281">very</ta>
            <ta e="T283" id="Seg_8821" s="T282">you.NOM</ta>
            <ta e="T284" id="Seg_8822" s="T283">wit-ADJZ.[NOM.SG]</ta>
            <ta e="T285" id="Seg_8823" s="T284">girl.[NOM.SG]</ta>
            <ta e="T286" id="Seg_8824" s="T285">always</ta>
            <ta e="T287" id="Seg_8825" s="T286">I.LAT</ta>
            <ta e="T288" id="Seg_8826" s="T287">I.LAT</ta>
            <ta e="T289" id="Seg_8827" s="T288">one.should</ta>
            <ta e="T290" id="Seg_8828" s="T289">speak-INF.LAT</ta>
            <ta e="T291" id="Seg_8829" s="T290">now</ta>
            <ta e="T292" id="Seg_8830" s="T291">you.NOM</ta>
            <ta e="T293" id="Seg_8831" s="T292">speak-EP-IMP.2SG</ta>
            <ta e="T294" id="Seg_8832" s="T293">and</ta>
            <ta e="T295" id="Seg_8833" s="T294">I.NOM</ta>
            <ta e="T296" id="Seg_8834" s="T295">NEG</ta>
            <ta e="T297" id="Seg_8835" s="T296">speak-FUT-1SG</ta>
            <ta e="T299" id="Seg_8836" s="T298">I.NOM</ta>
            <ta e="T300" id="Seg_8837" s="T299">NEG</ta>
            <ta e="T301" id="Seg_8838" s="T300">know-PRS-1SG</ta>
            <ta e="T302" id="Seg_8839" s="T301">what.[NOM.SG]</ta>
            <ta e="T303" id="Seg_8840" s="T302">speak-INF.LAT</ta>
            <ta e="T304" id="Seg_8841" s="T303">you.NOM</ta>
            <ta e="T305" id="Seg_8842" s="T304">speak-EP-IMP.2SG</ta>
            <ta e="T306" id="Seg_8843" s="T305">and</ta>
            <ta e="T308" id="Seg_8844" s="T307">and</ta>
            <ta e="T309" id="Seg_8845" s="T308">I.NOM</ta>
            <ta e="T310" id="Seg_8846" s="T309">what.[NOM.SG]</ta>
            <ta e="T311" id="Seg_8847" s="T310">always</ta>
            <ta e="T313" id="Seg_8848" s="T312">speak-INF.LAT</ta>
            <ta e="T317" id="Seg_8849" s="T315">lie.down-FUT-1SG=PTCL</ta>
            <ta e="T318" id="Seg_8850" s="T317">I.NOM</ta>
            <ta e="T319" id="Seg_8851" s="T318">house-NOM/GEN/ACC.1SG</ta>
            <ta e="T322" id="Seg_8852" s="T321">I.NOM</ta>
            <ta e="T323" id="Seg_8853" s="T322">what.[NOM.SG]=INDEF</ta>
            <ta e="T324" id="Seg_8854" s="T323">NEG</ta>
            <ta e="T325" id="Seg_8855" s="T324">know-PRS-1SG</ta>
            <ta e="T326" id="Seg_8856" s="T325">think-IMP.2SG</ta>
            <ta e="T327" id="Seg_8857" s="T326">what=INDEF</ta>
            <ta e="T328" id="Seg_8858" s="T327">lie-IMP.2SG</ta>
            <ta e="T329" id="Seg_8859" s="T328">what=INDEF</ta>
            <ta e="T330" id="Seg_8860" s="T329">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T331" id="Seg_8861" s="T330">far-ACC</ta>
            <ta e="T1057" id="Seg_8862" s="T331">go-CVB</ta>
            <ta e="T332" id="Seg_8863" s="T1057">disappear-PST.[3SG]</ta>
            <ta e="T333" id="Seg_8864" s="T332">and</ta>
            <ta e="T334" id="Seg_8865" s="T333">I.LAT</ta>
            <ta e="T335" id="Seg_8866" s="T334">very</ta>
            <ta e="T336" id="Seg_8867" s="T335">good</ta>
            <ta e="T337" id="Seg_8868" s="T336">who.[NOM.SG]=INDEF</ta>
            <ta e="T338" id="Seg_8869" s="T337">NEG</ta>
            <ta e="T339" id="Seg_8870" s="T338">beat-PRS.[3SG]</ta>
            <ta e="T340" id="Seg_8871" s="T339">I.NOM</ta>
            <ta e="T341" id="Seg_8872" s="T340">%%</ta>
            <ta e="T342" id="Seg_8873" s="T341">make.noise-PRS-1SG</ta>
            <ta e="T343" id="Seg_8874" s="T342">%%</ta>
            <ta e="T344" id="Seg_8875" s="T343">sing-DUR-1SG</ta>
            <ta e="T345" id="Seg_8876" s="T344">jump-DUR-1SG</ta>
            <ta e="T346" id="Seg_8877" s="T345">PTCL</ta>
            <ta e="T347" id="Seg_8878" s="T346">and</ta>
            <ta e="T348" id="Seg_8879" s="T347">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T349" id="Seg_8880" s="T348">NEG</ta>
            <ta e="T350" id="Seg_8881" s="T349">listen-PRS-1SG</ta>
            <ta e="T351" id="Seg_8882" s="T350">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T352" id="Seg_8883" s="T351">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T353" id="Seg_8884" s="T352">this.[NOM.SG]</ta>
            <ta e="T354" id="Seg_8885" s="T353">%%-PST.[3SG]</ta>
            <ta e="T355" id="Seg_8886" s="T354">that</ta>
            <ta e="T356" id="Seg_8887" s="T355">I.NOM</ta>
            <ta e="T358" id="Seg_8888" s="T357">that</ta>
            <ta e="T359" id="Seg_8889" s="T358">I.NOM</ta>
            <ta e="T360" id="Seg_8890" s="T359">make.noise-PRS-1SG</ta>
            <ta e="T361" id="Seg_8891" s="T360">I.NOM</ta>
            <ta e="T362" id="Seg_8892" s="T361">companion.[NOM.SG]</ta>
            <ta e="T363" id="Seg_8893" s="T362">today</ta>
            <ta e="T364" id="Seg_8894" s="T363">evening-LOC.ADV</ta>
            <ta e="T365" id="Seg_8895" s="T364">PTCL</ta>
            <ta e="T366" id="Seg_8896" s="T365">vodka.[NOM.SG]</ta>
            <ta e="T369" id="Seg_8897" s="T368">song.[NOM.SG]</ta>
            <ta e="T371" id="Seg_8898" s="T370">sing-PST.[3SG]</ta>
            <ta e="T372" id="Seg_8899" s="T371">fight-PST.[3SG]</ta>
            <ta e="T373" id="Seg_8900" s="T372">PTCL</ta>
            <ta e="T374" id="Seg_8901" s="T373">scold-DES-PST.[3SG]</ta>
            <ta e="T375" id="Seg_8902" s="T374">PTCL</ta>
            <ta e="T376" id="Seg_8903" s="T375">Elya.[NOM.SG]</ta>
            <ta e="T377" id="Seg_8904" s="T376">alone</ta>
            <ta e="T378" id="Seg_8905" s="T377">live-DUR.[3SG]</ta>
            <ta e="T379" id="Seg_8906" s="T378">always</ta>
            <ta e="T380" id="Seg_8907" s="T379">marry-DUR-3PL</ta>
            <ta e="T381" id="Seg_8908" s="T380">and</ta>
            <ta e="T382" id="Seg_8909" s="T381">this.[NOM.SG]</ta>
            <ta e="T383" id="Seg_8910" s="T382">always</ta>
            <ta e="T384" id="Seg_8911" s="T383">NEG</ta>
            <ta e="T385" id="Seg_8912" s="T384">go-PRS.[3SG]</ta>
            <ta e="T386" id="Seg_8913" s="T385">who-LAT=INDEF</ta>
            <ta e="T387" id="Seg_8914" s="T386">NEG</ta>
            <ta e="T388" id="Seg_8915" s="T387">go-PRS.[3SG]</ta>
            <ta e="T394" id="Seg_8916" s="T393">this-GEN</ta>
            <ta e="T395" id="Seg_8917" s="T394">PTCL</ta>
            <ta e="T396" id="Seg_8918" s="T395">mother-NOM/GEN.3SG</ta>
            <ta e="T397" id="Seg_8919" s="T396">father-NOM/GEN.3SG</ta>
            <ta e="T398" id="Seg_8920" s="T397">be-PRS.[3SG]</ta>
            <ta e="T399" id="Seg_8921" s="T398">say-PRS-3PL</ta>
            <ta e="T400" id="Seg_8922" s="T399">go-EP-IMP.2SG</ta>
            <ta e="T401" id="Seg_8923" s="T400">man-LAT</ta>
            <ta e="T402" id="Seg_8924" s="T401">and</ta>
            <ta e="T403" id="Seg_8925" s="T402">this.[NOM.SG]</ta>
            <ta e="T404" id="Seg_8926" s="T403">how=INDEF</ta>
            <ta e="T405" id="Seg_8927" s="T404">NEG</ta>
            <ta e="T406" id="Seg_8928" s="T405">go-PRS.[3SG]</ta>
            <ta e="T407" id="Seg_8929" s="T406">come-PST.[3SG]</ta>
            <ta e="T408" id="Seg_8930" s="T407">I.LAT</ta>
            <ta e="T409" id="Seg_8931" s="T408">marry-INF.LAT</ta>
            <ta e="T410" id="Seg_8932" s="T409">NEG</ta>
            <ta e="T411" id="Seg_8933" s="T410">beautiful.[NOM.SG]</ta>
            <ta e="T412" id="Seg_8934" s="T411">man.[NOM.SG]</ta>
            <ta e="T413" id="Seg_8935" s="T412">I.LAT</ta>
            <ta e="T414" id="Seg_8936" s="T413">not</ta>
            <ta e="T415" id="Seg_8937" s="T414">one.wants</ta>
            <ta e="T416" id="Seg_8938" s="T415">go-INF.LAT</ta>
            <ta e="T417" id="Seg_8939" s="T416">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T418" id="Seg_8940" s="T417">say-IPFVZ.[3SG]</ta>
            <ta e="T419" id="Seg_8941" s="T418">what.[NOM.SG]</ta>
            <ta e="T420" id="Seg_8942" s="T419">you.NOM</ta>
            <ta e="T421" id="Seg_8943" s="T420">NEG</ta>
            <ta e="T422" id="Seg_8944" s="T421">go-FUT-2SG</ta>
            <ta e="T423" id="Seg_8945" s="T422">NEG</ta>
            <ta e="T424" id="Seg_8946" s="T423">well</ta>
            <ta e="T425" id="Seg_8947" s="T424">NEG.AUX-IMP.2SG</ta>
            <ta e="T426" id="Seg_8948" s="T425">go-EP-CNG</ta>
            <ta e="T427" id="Seg_8949" s="T426">I.NOM</ta>
            <ta e="T428" id="Seg_8950" s="T427">run-MOM-PST-1SG</ta>
            <ta e="T429" id="Seg_8951" s="T428">then</ta>
            <ta e="T430" id="Seg_8952" s="T429">I.NOM</ta>
            <ta e="T433" id="Seg_8953" s="T432">man-NOM/GEN/ACC.1SG</ta>
            <ta e="T434" id="Seg_8954" s="T433">come-PST.[3SG]</ta>
            <ta e="T435" id="Seg_8955" s="T434">I.ACC</ta>
            <ta e="T436" id="Seg_8956" s="T435">marry-INF.LAT</ta>
            <ta e="T437" id="Seg_8957" s="T436">this-LAT</ta>
            <ta e="T439" id="Seg_8958" s="T438">year.[NOM.SG]</ta>
            <ta e="T440" id="Seg_8959" s="T439">and</ta>
            <ta e="T441" id="Seg_8960" s="T440">I.LAT</ta>
            <ta e="T442" id="Seg_8961" s="T441">few</ta>
            <ta e="T443" id="Seg_8962" s="T442">year.[NOM.SG]</ta>
            <ta e="T444" id="Seg_8963" s="T443">and</ta>
            <ta e="T445" id="Seg_8964" s="T444">I.LAT</ta>
            <ta e="T446" id="Seg_8965" s="T445">few</ta>
            <ta e="T447" id="Seg_8966" s="T446">year.[NOM.SG]</ta>
            <ta e="T448" id="Seg_8967" s="T447">I.NOM</ta>
            <ta e="T450" id="Seg_8968" s="T449">I.NOM</ta>
            <ta e="T451" id="Seg_8969" s="T450">say-IPFVZ-1SG</ta>
            <ta e="T452" id="Seg_8970" s="T451">go-EP-IMP.2SG</ta>
            <ta e="T453" id="Seg_8971" s="T452">father-LAT</ta>
            <ta e="T454" id="Seg_8972" s="T453">and</ta>
            <ta e="T455" id="Seg_8973" s="T454">mother-LAT</ta>
            <ta e="T456" id="Seg_8974" s="T455">marry-IMP.2SG</ta>
            <ta e="T457" id="Seg_8975" s="T456">this-PL</ta>
            <ta e="T458" id="Seg_8976" s="T457">give-FUT-3PL</ta>
            <ta e="T459" id="Seg_8977" s="T458">no</ta>
            <ta e="T460" id="Seg_8978" s="T459">NEG</ta>
            <ta e="T461" id="Seg_8979" s="T460">give-FUT-3PL</ta>
            <ta e="T462" id="Seg_8980" s="T461">then</ta>
            <ta e="T463" id="Seg_8981" s="T462">I.ACC</ta>
            <ta e="T464" id="Seg_8982" s="T463">speak-MOM-PST-3PL</ta>
            <ta e="T465" id="Seg_8983" s="T464">and</ta>
            <ta e="T467" id="Seg_8984" s="T466">go-PST-1PL</ta>
            <ta e="T469" id="Seg_8985" s="T468">horse-3SG-INS</ta>
            <ta e="T470" id="Seg_8986" s="T469">this-LAT</ta>
            <ta e="T471" id="Seg_8987" s="T470">then</ta>
            <ta e="T472" id="Seg_8988" s="T471">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T473" id="Seg_8989" s="T472">come-PST.[3SG]</ta>
            <ta e="T474" id="Seg_8990" s="T473">there</ta>
            <ta e="T475" id="Seg_8991" s="T474">we.LAT</ta>
            <ta e="T476" id="Seg_8992" s="T475">this.[NOM.SG]</ta>
            <ta e="T478" id="Seg_8993" s="T477">here</ta>
            <ta e="T479" id="Seg_8994" s="T478">come-PST.[3SG]</ta>
            <ta e="T480" id="Seg_8995" s="T479">grass.[NOM.SG]</ta>
            <ta e="T481" id="Seg_8996" s="T480">cut-INF.LAT</ta>
            <ta e="T482" id="Seg_8997" s="T481">then</ta>
            <ta e="T483" id="Seg_8998" s="T482">go-PST.[3SG]</ta>
            <ta e="T484" id="Seg_8999" s="T483">river-LAT</ta>
            <ta e="T485" id="Seg_9000" s="T484">and</ta>
            <ta e="T486" id="Seg_9001" s="T485">there</ta>
            <ta e="T487" id="Seg_9002" s="T486">die-RES-PST.[3SG]</ta>
            <ta e="T488" id="Seg_9003" s="T487">then</ta>
            <ta e="T489" id="Seg_9004" s="T488">I.NOM</ta>
            <ta e="T490" id="Seg_9005" s="T489">house-LAT/LOC.3SG</ta>
            <ta e="T491" id="Seg_9006" s="T490">come-PST-1SG</ta>
            <ta e="T492" id="Seg_9007" s="T491">and</ta>
            <ta e="T493" id="Seg_9008" s="T492">very</ta>
            <ta e="T494" id="Seg_9009" s="T493">strongly</ta>
            <ta e="T495" id="Seg_9010" s="T494">think-PST-1SG</ta>
            <ta e="T496" id="Seg_9011" s="T495">heart-NOM/GEN/ACC.1SG</ta>
            <ta e="T497" id="Seg_9012" s="T496">hurt-PST</ta>
            <ta e="T498" id="Seg_9013" s="T497">cry-PST-1SG</ta>
            <ta e="T499" id="Seg_9014" s="T498">pity-PST-1SG</ta>
            <ta e="T500" id="Seg_9015" s="T499">this-ACC</ta>
            <ta e="T501" id="Seg_9016" s="T500">PTCL</ta>
            <ta e="T502" id="Seg_9017" s="T501">then</ta>
            <ta e="T503" id="Seg_9018" s="T502">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T504" id="Seg_9019" s="T503">I.LAT</ta>
            <ta e="T505" id="Seg_9020" s="T504">water.[NOM.SG]</ta>
            <ta e="T506" id="Seg_9021" s="T505">heal-PST.[3SG]</ta>
            <ta e="T507" id="Seg_9022" s="T506">%%</ta>
            <ta e="T508" id="Seg_9023" s="T507">I.ACC</ta>
            <ta e="T509" id="Seg_9024" s="T508">then</ta>
            <ta e="T510" id="Seg_9025" s="T509">I.NOM</ta>
            <ta e="T511" id="Seg_9026" s="T510">INCH</ta>
            <ta e="T512" id="Seg_9027" s="T511">song.[NOM.SG]</ta>
            <ta e="T514" id="Seg_9028" s="T513">sing-INF.LAT</ta>
            <ta e="T515" id="Seg_9029" s="T514">then</ta>
            <ta e="T516" id="Seg_9030" s="T515">I.NOM</ta>
            <ta e="T517" id="Seg_9031" s="T516">come-PST-1SG</ta>
            <ta e="T518" id="Seg_9032" s="T517">house-LAT/LOC.1SG</ta>
            <ta e="T519" id="Seg_9033" s="T518">live-PST-1SG</ta>
            <ta e="T520" id="Seg_9034" s="T519">then</ta>
            <ta e="T521" id="Seg_9035" s="T520">go-PST-1SG</ta>
            <ta e="T523" id="Seg_9036" s="T522">Permyakovo-LAT</ta>
            <ta e="T525" id="Seg_9037" s="T524">sinew.[NOM.SG]</ta>
            <ta e="T526" id="Seg_9038" s="T525">play-PST-1PL</ta>
            <ta e="T527" id="Seg_9039" s="T526">jump-1PL</ta>
            <ta e="T528" id="Seg_9040" s="T527">accordeon-LAT/LOC.1SG</ta>
            <ta e="T529" id="Seg_9041" s="T528">play-PST-1PL</ta>
            <ta e="T530" id="Seg_9042" s="T529">song.[NOM.SG]</ta>
            <ta e="T531" id="Seg_9043" s="T530">sing-PST-1PL</ta>
            <ta e="T532" id="Seg_9044" s="T531">then</ta>
            <ta e="T533" id="Seg_9045" s="T532">there</ta>
            <ta e="T534" id="Seg_9046" s="T533">man.[NOM.SG]</ta>
            <ta e="T535" id="Seg_9047" s="T534">come-PST.[3SG]</ta>
            <ta e="T536" id="Seg_9048" s="T535">%Nagornyj.[NOM.SG]</ta>
            <ta e="T537" id="Seg_9049" s="T536">beard-NOM/GEN/ACC.3SG</ta>
            <ta e="T538" id="Seg_9050" s="T537">big.[NOM.SG]</ta>
            <ta e="T540" id="Seg_9051" s="T539">and</ta>
            <ta e="T541" id="Seg_9052" s="T540">red-PL</ta>
            <ta e="T542" id="Seg_9053" s="T541">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T543" id="Seg_9054" s="T542">red.[NOM.SG]</ta>
            <ta e="T544" id="Seg_9055" s="T543">then</ta>
            <ta e="T545" id="Seg_9056" s="T544">I.NOM</ta>
            <ta e="T546" id="Seg_9057" s="T545">house-LAT/LOC.1SG</ta>
            <ta e="T547" id="Seg_9058" s="T546">come-PST-1SG</ta>
            <ta e="T548" id="Seg_9059" s="T547">and</ta>
            <ta e="T549" id="Seg_9060" s="T548">come-PST.[3SG]</ta>
            <ta e="T550" id="Seg_9061" s="T549">I.ACC</ta>
            <ta e="T551" id="Seg_9062" s="T550">marry-INF.LAT</ta>
            <ta e="T552" id="Seg_9063" s="T551">I.NOM</ta>
            <ta e="T553" id="Seg_9064" s="T552">say-IPFVZ-1SG</ta>
            <ta e="T554" id="Seg_9065" s="T553">you.NOM</ta>
            <ta e="T555" id="Seg_9066" s="T554">boy.[NOM.SG]</ta>
            <ta e="T556" id="Seg_9067" s="T555">%%</ta>
            <ta e="T558" id="Seg_9068" s="T557">like</ta>
            <ta e="T559" id="Seg_9069" s="T558">I.NOM</ta>
            <ta e="T560" id="Seg_9070" s="T559">such.[NOM.SG]</ta>
            <ta e="T561" id="Seg_9071" s="T560">well</ta>
            <ta e="T562" id="Seg_9072" s="T561">I.NOM</ta>
            <ta e="T563" id="Seg_9073" s="T562">you.NOM-COM</ta>
            <ta e="T564" id="Seg_9074" s="T563">NEG</ta>
            <ta e="T565" id="Seg_9075" s="T564">live-FUT-1SG</ta>
            <ta e="T566" id="Seg_9076" s="T565">and</ta>
            <ta e="T567" id="Seg_9077" s="T566">son-COM</ta>
            <ta e="T568" id="Seg_9078" s="T567">then</ta>
            <ta e="T569" id="Seg_9079" s="T568">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T570" id="Seg_9080" s="T569">say-IPFVZ.[3SG]</ta>
            <ta e="T571" id="Seg_9081" s="T570">go-EP-IMP.2SG</ta>
            <ta e="T572" id="Seg_9082" s="T571">this-GEN</ta>
            <ta e="T573" id="Seg_9083" s="T572">money.[NOM.SG]</ta>
            <ta e="T574" id="Seg_9084" s="T573">many</ta>
            <ta e="T575" id="Seg_9085" s="T574">very</ta>
            <ta e="T577" id="Seg_9086" s="T576">good</ta>
            <ta e="T578" id="Seg_9087" s="T577">man.[NOM.SG]</ta>
            <ta e="T579" id="Seg_9088" s="T578">and</ta>
            <ta e="T580" id="Seg_9089" s="T579">I.NOM</ta>
            <ta e="T581" id="Seg_9090" s="T580">stand-PRS-1SG</ta>
            <ta e="T582" id="Seg_9091" s="T581">NEG</ta>
            <ta e="T583" id="Seg_9092" s="T582">and</ta>
            <ta e="T584" id="Seg_9093" s="T583">go-EP-IMP.2SG</ta>
            <ta e="T585" id="Seg_9094" s="T584">and</ta>
            <ta e="T586" id="Seg_9095" s="T585">live-IMP.2SG</ta>
            <ta e="T587" id="Seg_9096" s="T586">this-COM</ta>
            <ta e="T588" id="Seg_9097" s="T587">one.[NOM.SG]</ta>
            <ta e="T589" id="Seg_9098" s="T588">man.[NOM.SG]</ta>
            <ta e="T590" id="Seg_9099" s="T589">come-PST.[3SG]</ta>
            <ta e="T591" id="Seg_9100" s="T590">marry-INF.LAT</ta>
            <ta e="T592" id="Seg_9101" s="T591">and</ta>
            <ta e="T593" id="Seg_9102" s="T592">I.NOM</ta>
            <ta e="T594" id="Seg_9103" s="T593">NEG</ta>
            <ta e="T595" id="Seg_9104" s="T594">go-PRS-1SG</ta>
            <ta e="T596" id="Seg_9105" s="T595">and</ta>
            <ta e="T597" id="Seg_9106" s="T596">mother</ta>
            <ta e="T598" id="Seg_9107" s="T597">NEG</ta>
            <ta e="T599" id="Seg_9108" s="T598">go-PRS-1SG</ta>
            <ta e="T600" id="Seg_9109" s="T599">this.[NOM.SG]</ta>
            <ta e="T601" id="Seg_9110" s="T600">NEG</ta>
            <ta e="T602" id="Seg_9111" s="T601">beautiful.[NOM.SG]</ta>
            <ta e="T603" id="Seg_9112" s="T602">foot.[NOM.SG]</ta>
            <ta e="T604" id="Seg_9113" s="T603">foot-NOM/GEN.3SG</ta>
            <ta e="T605" id="Seg_9114" s="T604">PTCL</ta>
            <ta e="T607" id="Seg_9115" s="T606">wry.[NOM.SG]</ta>
            <ta e="T609" id="Seg_9116" s="T608">beard-NOM/GEN/ACC.3SG</ta>
            <ta e="T610" id="Seg_9117" s="T609">red.[NOM.SG]</ta>
            <ta e="T611" id="Seg_9118" s="T610">tongue-NOM/GEN.3SG</ta>
            <ta e="T612" id="Seg_9119" s="T611">NEG</ta>
            <ta e="T613" id="Seg_9120" s="T612">speak-PRS.[3SG]</ta>
            <ta e="T614" id="Seg_9121" s="T613">NEG</ta>
            <ta e="T615" id="Seg_9122" s="T614">good</ta>
            <ta e="T616" id="Seg_9123" s="T615">speak-PRS.[3SG]</ta>
            <ta e="T617" id="Seg_9124" s="T616">ear-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T618" id="Seg_9125" s="T617">NEG</ta>
            <ta e="T619" id="Seg_9126" s="T618">listen-PRS-3PL</ta>
            <ta e="T620" id="Seg_9127" s="T619">and</ta>
            <ta e="T621" id="Seg_9128" s="T620">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T622" id="Seg_9129" s="T621">say-IPFVZ.[3SG]</ta>
            <ta e="T623" id="Seg_9130" s="T622">go-EP-IMP.2SG</ta>
            <ta e="T624" id="Seg_9131" s="T623">this-GEN</ta>
            <ta e="T625" id="Seg_9132" s="T624">cow.[NOM.SG]</ta>
            <ta e="T626" id="Seg_9133" s="T625">be-PRS.[3SG]</ta>
            <ta e="T627" id="Seg_9134" s="T626">and</ta>
            <ta e="T628" id="Seg_9135" s="T627">horse.[NOM.SG]</ta>
            <ta e="T629" id="Seg_9136" s="T628">be-PRS.[3SG]</ta>
            <ta e="T630" id="Seg_9137" s="T629">and</ta>
            <ta e="T631" id="Seg_9138" s="T630">bread.[NOM.SG]</ta>
            <ta e="T632" id="Seg_9139" s="T631">many</ta>
            <ta e="T633" id="Seg_9140" s="T632">and</ta>
            <ta e="T634" id="Seg_9141" s="T633">I.NOM</ta>
            <ta e="T635" id="Seg_9142" s="T634">I.NOM</ta>
            <ta e="T636" id="Seg_9143" s="T635">say-PST-1SG</ta>
            <ta e="T637" id="Seg_9144" s="T636">I.LAT</ta>
            <ta e="T638" id="Seg_9145" s="T637">horse-COM</ta>
            <ta e="T639" id="Seg_9146" s="T638">NEG</ta>
            <ta e="T640" id="Seg_9147" s="T639">live-INF.LAT</ta>
            <ta e="T641" id="Seg_9148" s="T640">and</ta>
            <ta e="T642" id="Seg_9149" s="T641">cow-LAT</ta>
            <ta e="T643" id="Seg_9150" s="T642">NEG</ta>
            <ta e="T644" id="Seg_9151" s="T643">live-INF.LAT</ta>
            <ta e="T645" id="Seg_9152" s="T644">one.should</ta>
            <ta e="T646" id="Seg_9153" s="T645">you.DAT</ta>
            <ta e="T647" id="Seg_9154" s="T646">so</ta>
            <ta e="T648" id="Seg_9155" s="T647">you.NOM</ta>
            <ta e="T649" id="Seg_9156" s="T648">you.DAT</ta>
            <ta e="T650" id="Seg_9157" s="T649">one.needs</ta>
            <ta e="T651" id="Seg_9158" s="T650">so</ta>
            <ta e="T652" id="Seg_9159" s="T651">you.NOM</ta>
            <ta e="T653" id="Seg_9160" s="T652">go-EP-IMP.2SG</ta>
            <ta e="T654" id="Seg_9161" s="T653">Cossack.village-LOC</ta>
            <ta e="T655" id="Seg_9162" s="T654">come-PST.[3SG]</ta>
            <ta e="T656" id="Seg_9163" s="T655">one.[NOM.SG]</ta>
            <ta e="T657" id="Seg_9164" s="T656">man.[NOM.SG]</ta>
            <ta e="T658" id="Seg_9165" s="T657">this-ACC</ta>
            <ta e="T659" id="Seg_9166" s="T658">woman.[NOM.SG]</ta>
            <ta e="T660" id="Seg_9167" s="T659">throw.away-MOM-PST.[3SG]</ta>
            <ta e="T661" id="Seg_9168" s="T660">this.[NOM.SG]</ta>
            <ta e="T662" id="Seg_9169" s="T661">INCH</ta>
            <ta e="T663" id="Seg_9170" s="T662">I.ACC</ta>
            <ta e="T664" id="Seg_9171" s="T663">marry-INF.LAT</ta>
            <ta e="T665" id="Seg_9172" s="T664">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T666" id="Seg_9173" s="T665">began</ta>
            <ta e="T667" id="Seg_9174" s="T666">boil-INF.LAT</ta>
            <ta e="T668" id="Seg_9175" s="T667">meat.[NOM.SG]</ta>
            <ta e="T669" id="Seg_9176" s="T668">this-ACC.PL</ta>
            <ta e="T671" id="Seg_9177" s="T670">feed-INF.LAT</ta>
            <ta e="T672" id="Seg_9178" s="T671">and</ta>
            <ta e="T673" id="Seg_9179" s="T672">I.NOM</ta>
            <ta e="T674" id="Seg_9180" s="T673">say-PRS-1SG</ta>
            <ta e="T675" id="Seg_9181" s="T674">go-INF.LAT</ta>
            <ta e="T676" id="Seg_9182" s="T675">or</ta>
            <ta e="T677" id="Seg_9183" s="T676">NEG</ta>
            <ta e="T678" id="Seg_9184" s="T677">go-INF.LAT</ta>
            <ta e="T680" id="Seg_9185" s="T679">go-EP-IMP.2SG</ta>
            <ta e="T682" id="Seg_9186" s="T681">NEG.AUX-IMP.2SG</ta>
            <ta e="T683" id="Seg_9187" s="T682">go-EP-CNG</ta>
            <ta e="T684" id="Seg_9188" s="T683">you.NOM</ta>
            <ta e="T685" id="Seg_9189" s="T684">soon</ta>
            <ta e="T687" id="Seg_9190" s="T686">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T688" id="Seg_9191" s="T687">big.[NOM.SG]</ta>
            <ta e="T689" id="Seg_9192" s="T688">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T690" id="Seg_9193" s="T689">know-2SG</ta>
            <ta e="T691" id="Seg_9194" s="T690">what.[NOM.SG]</ta>
            <ta e="T692" id="Seg_9195" s="T691">make-INF.LAT</ta>
            <ta e="T693" id="Seg_9196" s="T692">then</ta>
            <ta e="T694" id="Seg_9197" s="T693">this.[NOM.SG]</ta>
            <ta e="T695" id="Seg_9198" s="T694">man.[NOM.SG]</ta>
            <ta e="T697" id="Seg_9199" s="T696">come-PST.[3SG]</ta>
            <ta e="T698" id="Seg_9200" s="T697">I.LAT</ta>
            <ta e="T699" id="Seg_9201" s="T698">sit.down-PST.[3SG]</ta>
            <ta e="T700" id="Seg_9202" s="T699">and</ta>
            <ta e="T701" id="Seg_9203" s="T700">INCH</ta>
            <ta e="T702" id="Seg_9204" s="T701">marry-INF.LAT</ta>
            <ta e="T703" id="Seg_9205" s="T702">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T704" id="Seg_9206" s="T703">and</ta>
            <ta e="T705" id="Seg_9207" s="T704">INCH</ta>
            <ta e="T706" id="Seg_9208" s="T705">God-LAT</ta>
            <ta e="T707" id="Seg_9209" s="T706">pray-INF.LAT</ta>
            <ta e="T708" id="Seg_9210" s="T707">NEG</ta>
            <ta e="T709" id="Seg_9211" s="T708">throw-FUT-1SG</ta>
            <ta e="T710" id="Seg_9212" s="T709">you.ACC</ta>
            <ta e="T711" id="Seg_9213" s="T710">and</ta>
            <ta e="T712" id="Seg_9214" s="T711">%%-FUT-1SG</ta>
            <ta e="T713" id="Seg_9215" s="T712">good</ta>
            <ta e="T714" id="Seg_9216" s="T713">live-FUT-1PL</ta>
            <ta e="T715" id="Seg_9217" s="T714">then</ta>
            <ta e="T716" id="Seg_9218" s="T715">this.[NOM.SG]</ta>
            <ta e="T717" id="Seg_9219" s="T716">horse.[NOM.SG]</ta>
            <ta e="T718" id="Seg_9220" s="T717">harness-PST.[3SG]</ta>
            <ta e="T720" id="Seg_9221" s="T719">we.NOM</ta>
            <ta e="T721" id="Seg_9222" s="T720">I.NOM</ta>
            <ta e="T722" id="Seg_9223" s="T721">this-COM</ta>
            <ta e="T723" id="Seg_9224" s="T722">go-PST-1SG</ta>
            <ta e="T724" id="Seg_9225" s="T723">Permyakovo-LAT</ta>
            <ta e="T725" id="Seg_9226" s="T724">vodka.[NOM.SG]</ta>
            <ta e="T726" id="Seg_9227" s="T725">take-PST-1SG</ta>
            <ta e="T727" id="Seg_9228" s="T726">bring-PST-1PL</ta>
            <ta e="T730" id="Seg_9229" s="T729">table-NOM/GEN/ACC.3SG</ta>
            <ta e="T731" id="Seg_9230" s="T730">collect-PST-1PL</ta>
            <ta e="T732" id="Seg_9231" s="T731">people.[NOM.SG]</ta>
            <ta e="T733" id="Seg_9232" s="T732">collect-PST-1PL</ta>
            <ta e="T734" id="Seg_9233" s="T733">vodka.[NOM.SG]</ta>
            <ta e="T735" id="Seg_9234" s="T734">drink-PST-3PL</ta>
            <ta e="T736" id="Seg_9235" s="T735">eat-PST-3PL</ta>
            <ta e="T737" id="Seg_9236" s="T736">then</ta>
            <ta e="T738" id="Seg_9237" s="T737">we.NOM</ta>
            <ta e="T740" id="Seg_9238" s="T739">go-PST-1PL</ta>
            <ta e="T741" id="Seg_9239" s="T740">Cossack.village-LAT</ta>
            <ta e="T742" id="Seg_9240" s="T741">then</ta>
            <ta e="T743" id="Seg_9241" s="T742">%%-PST-1SG</ta>
            <ta e="T744" id="Seg_9242" s="T743">three.[NOM.SG]</ta>
            <ta e="T745" id="Seg_9243" s="T744">winter.[NOM.SG]</ta>
            <ta e="T746" id="Seg_9244" s="T745">then</ta>
            <ta e="T747" id="Seg_9245" s="T746">war.[NOM.SG]</ta>
            <ta e="T748" id="Seg_9246" s="T747">be-PST.[3SG]</ta>
            <ta e="T749" id="Seg_9247" s="T748">this-ACC</ta>
            <ta e="T750" id="Seg_9248" s="T749">take-PST-3PL</ta>
            <ta e="T751" id="Seg_9249" s="T750">war-LAT</ta>
            <ta e="T752" id="Seg_9250" s="T751">then</ta>
            <ta e="T753" id="Seg_9251" s="T752">I.NOM</ta>
            <ta e="T754" id="Seg_9252" s="T753">pregnant.[NOM.SG]</ta>
            <ta e="T755" id="Seg_9253" s="T754">be-PST-1SG</ta>
            <ta e="T756" id="Seg_9254" s="T755">girl.[NOM.SG]</ta>
            <ta e="T757" id="Seg_9255" s="T756">this.[NOM.SG]</ta>
            <ta e="T758" id="Seg_9256" s="T757">then</ta>
            <ta e="T759" id="Seg_9257" s="T758">this.[NOM.SG]</ta>
            <ta e="T760" id="Seg_9258" s="T759">come-PST.[3SG]</ta>
            <ta e="T761" id="Seg_9259" s="T760">war-ABL</ta>
            <ta e="T763" id="Seg_9260" s="T762">be-PST.[3SG]</ta>
            <ta e="T764" id="Seg_9261" s="T763">then</ta>
            <ta e="T765" id="Seg_9262" s="T764">more</ta>
            <ta e="T766" id="Seg_9263" s="T765">live-PST-1PL</ta>
            <ta e="T767" id="Seg_9264" s="T766">I.NOM</ta>
            <ta e="T768" id="Seg_9265" s="T767">again</ta>
            <ta e="T769" id="Seg_9266" s="T768">pregnant.[NOM.SG]</ta>
            <ta e="T770" id="Seg_9267" s="T769">be-PST.[3SG]</ta>
            <ta e="T771" id="Seg_9268" s="T770">then</ta>
            <ta e="T772" id="Seg_9269" s="T771">this.[NOM.SG]</ta>
            <ta e="T774" id="Seg_9270" s="T773">this.[NOM.SG]</ta>
            <ta e="T775" id="Seg_9271" s="T774">I.ACC</ta>
            <ta e="T776" id="Seg_9272" s="T775">drive-PST.[3SG]</ta>
            <ta e="T777" id="Seg_9273" s="T776">and</ta>
            <ta e="T778" id="Seg_9274" s="T777">this-ACC</ta>
            <ta e="T779" id="Seg_9275" s="T778">take-PST.[3SG]</ta>
            <ta e="T780" id="Seg_9276" s="T779">more</ta>
            <ta e="T781" id="Seg_9277" s="T780">one.[NOM.SG]</ta>
            <ta e="T783" id="Seg_9278" s="T782">boy.[NOM.SG]</ta>
            <ta e="T784" id="Seg_9279" s="T783">bring-PST-1SG</ta>
            <ta e="T785" id="Seg_9280" s="T784">then</ta>
            <ta e="T786" id="Seg_9281" s="T785">house-LAT/LOC.1SG</ta>
            <ta e="T787" id="Seg_9282" s="T786">come-PST-1SG</ta>
            <ta e="T790" id="Seg_9283" s="T789">father-LAT</ta>
            <ta e="T791" id="Seg_9284" s="T790">and</ta>
            <ta e="T792" id="Seg_9285" s="T791">mother-LAT</ta>
            <ta e="T793" id="Seg_9286" s="T792">then</ta>
            <ta e="T794" id="Seg_9287" s="T793">die-RES-PST-3PL</ta>
            <ta e="T795" id="Seg_9288" s="T794">girl.[NOM.SG]</ta>
            <ta e="T796" id="Seg_9289" s="T795">and</ta>
            <ta e="T797" id="Seg_9290" s="T796">boy.[NOM.SG]</ta>
            <ta e="T798" id="Seg_9291" s="T797">I.NOM</ta>
            <ta e="T799" id="Seg_9292" s="T798">alone</ta>
            <ta e="T801" id="Seg_9293" s="T800">become-RES-PST-1SG</ta>
            <ta e="T802" id="Seg_9294" s="T801">%%</ta>
            <ta e="T803" id="Seg_9295" s="T802">live-PST-1SG</ta>
            <ta e="T804" id="Seg_9296" s="T803">again</ta>
            <ta e="T805" id="Seg_9297" s="T804">man.[NOM.SG]</ta>
            <ta e="T806" id="Seg_9298" s="T805">I.ACC</ta>
            <ta e="T807" id="Seg_9299" s="T806">marry-PST.[3SG]</ta>
            <ta e="T808" id="Seg_9300" s="T807">I.NOM</ta>
            <ta e="T809" id="Seg_9301" s="T808">go-PST-1SG</ta>
            <ta e="T810" id="Seg_9302" s="T809">this-COM</ta>
            <ta e="T811" id="Seg_9303" s="T810">sit-PST-1SG</ta>
            <ta e="T812" id="Seg_9304" s="T811">one.[NOM.SG]</ta>
            <ta e="T813" id="Seg_9305" s="T812">two.[NOM.SG]</ta>
            <ta e="T814" id="Seg_9306" s="T813">three.[NOM.SG]</ta>
            <ta e="T815" id="Seg_9307" s="T814">four.[NOM.SG]</ta>
            <ta e="T816" id="Seg_9308" s="T815">five.[NOM.SG]</ta>
            <ta e="T817" id="Seg_9309" s="T816">six.[NOM.SG]</ta>
            <ta e="T818" id="Seg_9310" s="T817">seven.[NOM.SG]</ta>
            <ta e="T819" id="Seg_9311" s="T818">winter.[NOM.SG]</ta>
            <ta e="T820" id="Seg_9312" s="T819">live-PST-1SG</ta>
            <ta e="T821" id="Seg_9313" s="T820">two.[NOM.SG]</ta>
            <ta e="T822" id="Seg_9314" s="T821">child.[NOM.SG]</ta>
            <ta e="T823" id="Seg_9315" s="T822">be-PST-3PL</ta>
            <ta e="T824" id="Seg_9316" s="T823">very</ta>
            <ta e="T825" id="Seg_9317" s="T824">NEG</ta>
            <ta e="T826" id="Seg_9318" s="T825">good</ta>
            <ta e="T827" id="Seg_9319" s="T826">vodka.[NOM.SG]</ta>
            <ta e="T828" id="Seg_9320" s="T827">drink-PST.[3SG]</ta>
            <ta e="T829" id="Seg_9321" s="T828">fight-DUR-PST.[3SG]</ta>
            <ta e="T830" id="Seg_9322" s="T829">scold-DES-DUR-PST.[3SG]</ta>
            <ta e="T831" id="Seg_9323" s="T830">when</ta>
            <ta e="T832" id="Seg_9324" s="T831">when</ta>
            <ta e="T833" id="Seg_9325" s="T832">throw-MOM-PST-1SG</ta>
            <ta e="T834" id="Seg_9326" s="T833">PTCL</ta>
            <ta e="T835" id="Seg_9327" s="T834">head-NOM/GEN/ACC.1SG</ta>
            <ta e="T836" id="Seg_9328" s="T835">PTCL</ta>
            <ta e="T837" id="Seg_9329" s="T836">hair-NOM/GEN/ACC.1SG</ta>
            <ta e="T838" id="Seg_9330" s="T837">off-pull-PST.[3SG]</ta>
            <ta e="T839" id="Seg_9331" s="T838">%%</ta>
            <ta e="T840" id="Seg_9332" s="T839">strongly</ta>
            <ta e="T841" id="Seg_9333" s="T840">beat-PST.[3SG]</ta>
            <ta e="T842" id="Seg_9334" s="T841">even</ta>
            <ta e="T843" id="Seg_9335" s="T842">blood.[NOM.SG]</ta>
            <ta e="T844" id="Seg_9336" s="T843">be-PST.[3SG]</ta>
            <ta e="T845" id="Seg_9337" s="T844">then</ta>
            <ta e="T846" id="Seg_9338" s="T845">I.NOM</ta>
            <ta e="T847" id="Seg_9339" s="T846">boy-NOM/GEN/ACC.1SG</ta>
            <ta e="T848" id="Seg_9340" s="T847">this-ACC</ta>
            <ta e="T849" id="Seg_9341" s="T848">throw.away-MOM-PST-1SG</ta>
            <ta e="T850" id="Seg_9342" s="T849">I.NOM</ta>
            <ta e="T851" id="Seg_9343" s="T850">one.[NOM.SG]</ta>
            <ta e="T852" id="Seg_9344" s="T851">boy-NOM/GEN/ACC.1SG</ta>
            <ta e="T853" id="Seg_9345" s="T852">be-PST.[3SG]</ta>
            <ta e="T854" id="Seg_9346" s="T853">now</ta>
            <ta e="T855" id="Seg_9347" s="T854">say-IPFVZ-1SG</ta>
            <ta e="T856" id="Seg_9348" s="T855">who-LAT=INDEF</ta>
            <ta e="T858" id="Seg_9349" s="T856">NEG</ta>
            <ta e="T859" id="Seg_9350" s="T858">man-LAT</ta>
            <ta e="T860" id="Seg_9351" s="T859">alone</ta>
            <ta e="T861" id="Seg_9352" s="T860">live-FUT-1SG</ta>
            <ta e="T862" id="Seg_9353" s="T861">son-COM</ta>
            <ta e="T863" id="Seg_9354" s="T862">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T864" id="Seg_9355" s="T863">son-COM</ta>
            <ta e="T865" id="Seg_9356" s="T864">then</ta>
            <ta e="T866" id="Seg_9357" s="T865">one.[NOM.SG]</ta>
            <ta e="T867" id="Seg_9358" s="T866">boy.[NOM.SG]</ta>
            <ta e="T868" id="Seg_9359" s="T867">INCH</ta>
            <ta e="T869" id="Seg_9360" s="T868">I.ACC</ta>
            <ta e="T870" id="Seg_9361" s="T869">marry-INF.LAT</ta>
            <ta e="T871" id="Seg_9362" s="T870">I.NOM</ta>
            <ta e="T875" id="Seg_9363" s="T874">NEG</ta>
            <ta e="T876" id="Seg_9364" s="T875">%%-PRS-1SG</ta>
            <ta e="T877" id="Seg_9365" s="T876">face-NOM/GEN/ACC.1SG</ta>
            <ta e="T878" id="Seg_9366" s="T877">face-NOM/GEN/ACC.3SG</ta>
            <ta e="T879" id="Seg_9367" s="T878">thick.[NOM.SG]</ta>
            <ta e="T880" id="Seg_9368" s="T879">NEG</ta>
            <ta e="T881" id="Seg_9369" s="T880">go-FUT-1SG</ta>
            <ta e="T882" id="Seg_9370" s="T881">this</ta>
            <ta e="T883" id="Seg_9371" s="T882">say-IPFVZ.[3SG]</ta>
            <ta e="T884" id="Seg_9372" s="T883">HORT</ta>
            <ta e="T885" id="Seg_9373" s="T884">live-INF.LAT</ta>
            <ta e="T886" id="Seg_9374" s="T885">you-COM</ta>
            <ta e="T1055" id="Seg_9375" s="T886">well</ta>
            <ta e="T887" id="Seg_9376" s="T1055">think-PST-1SG</ta>
            <ta e="T888" id="Seg_9377" s="T887">think-PST-1SG</ta>
            <ta e="T889" id="Seg_9378" s="T888">then</ta>
            <ta e="T890" id="Seg_9379" s="T889">go-PST-1SG</ta>
            <ta e="T891" id="Seg_9380" s="T890">then</ta>
            <ta e="T892" id="Seg_9381" s="T891">again</ta>
            <ta e="T894" id="Seg_9382" s="T893">throw.away-FUT-1SG</ta>
            <ta e="T895" id="Seg_9383" s="T894">say-IPFVZ-1SG</ta>
            <ta e="T896" id="Seg_9384" s="T895">you.DAT</ta>
            <ta e="T897" id="Seg_9385" s="T896">NEG</ta>
            <ta e="T898" id="Seg_9386" s="T897">live-FUT-1SG</ta>
            <ta e="T899" id="Seg_9387" s="T898">you.NOM-COM</ta>
            <ta e="T900" id="Seg_9388" s="T899">there</ta>
            <ta e="T901" id="Seg_9389" s="T900">INCH</ta>
            <ta e="T902" id="Seg_9390" s="T901">cry-INF.LAT</ta>
            <ta e="T903" id="Seg_9391" s="T902">strongly</ta>
            <ta e="T904" id="Seg_9392" s="T903">cry-PST.[3SG]</ta>
            <ta e="T905" id="Seg_9393" s="T904">very</ta>
            <ta e="T906" id="Seg_9394" s="T905">then</ta>
            <ta e="T907" id="Seg_9395" s="T906">I.NOM</ta>
            <ta e="T908" id="Seg_9396" s="T907">say-PRS-1SG</ta>
            <ta e="T910" id="Seg_9397" s="T909">house.[NOM.SG]</ta>
            <ta e="T911" id="Seg_9398" s="T910">place-IMP.3SG.O-3SG</ta>
            <ta e="T912" id="Seg_9399" s="T911">then</ta>
            <ta e="T913" id="Seg_9400" s="T912">JUSS</ta>
            <ta e="T914" id="Seg_9401" s="T913">go-FUT-3SG</ta>
            <ta e="T915" id="Seg_9402" s="T914">and</ta>
            <ta e="T916" id="Seg_9403" s="T915">so</ta>
            <ta e="T917" id="Seg_9404" s="T916">be-PST.[3SG]</ta>
            <ta e="T918" id="Seg_9405" s="T917">house.[NOM.SG]</ta>
            <ta e="T920" id="Seg_9406" s="T919">place-PST.[3SG]</ta>
            <ta e="T921" id="Seg_9407" s="T920">and</ta>
            <ta e="T1058" id="Seg_9408" s="T921">go-CVB</ta>
            <ta e="T922" id="Seg_9409" s="T1058">disappear-PST.[3SG]</ta>
            <ta e="T923" id="Seg_9410" s="T922">I.NOM</ta>
            <ta e="T924" id="Seg_9411" s="T923">alone</ta>
            <ta e="T926" id="Seg_9412" s="T925">now</ta>
            <ta e="T927" id="Seg_9413" s="T926">I.NOM</ta>
            <ta e="T928" id="Seg_9414" s="T927">always</ta>
            <ta e="T929" id="Seg_9415" s="T928">alone</ta>
            <ta e="T930" id="Seg_9416" s="T929">live-DUR-1SG</ta>
            <ta e="T931" id="Seg_9417" s="T930">live-DUR-1SG</ta>
            <ta e="T932" id="Seg_9418" s="T931">PTCL</ta>
            <ta e="T933" id="Seg_9419" s="T932">two.[NOM.SG]</ta>
            <ta e="T934" id="Seg_9420" s="T933">ten.[NOM.SG]</ta>
            <ta e="T935" id="Seg_9421" s="T934">moon.[NOM.SG]</ta>
            <ta e="T936" id="Seg_9422" s="T935">live-DUR-1SG</ta>
            <ta e="T937" id="Seg_9423" s="T936">when</ta>
            <ta e="T938" id="Seg_9424" s="T937">this.[NOM.SG]</ta>
            <ta e="T939" id="Seg_9425" s="T938">throw.away-MOM-PST.[3SG]</ta>
            <ta e="T940" id="Seg_9426" s="T939">then</ta>
            <ta e="T941" id="Seg_9427" s="T940">we.NOM</ta>
            <ta e="T942" id="Seg_9428" s="T941">this.[NOM.SG]</ta>
            <ta e="T943" id="Seg_9429" s="T942">son-COM</ta>
            <ta e="T944" id="Seg_9430" s="T943">PTCL</ta>
            <ta e="T945" id="Seg_9431" s="T944">tree.[NOM.SG]</ta>
            <ta e="T946" id="Seg_9432" s="T945">axe-INS</ta>
            <ta e="T947" id="Seg_9433" s="T946">cut-PST-1PL</ta>
            <ta e="T948" id="Seg_9434" s="T947">house.[NOM.SG]</ta>
            <ta e="T950" id="Seg_9435" s="T949">place-INF.LAT</ta>
            <ta e="T952" id="Seg_9436" s="T951">carry-PST-1PL</ta>
            <ta e="T954" id="Seg_9437" s="T953">horse-INS</ta>
            <ta e="T955" id="Seg_9438" s="T954">and</ta>
            <ta e="T956" id="Seg_9439" s="T955">house.[NOM.SG]</ta>
            <ta e="T957" id="Seg_9440" s="T956">place-PST-1PL</ta>
            <ta e="T958" id="Seg_9441" s="T957">man-PL</ta>
            <ta e="T959" id="Seg_9442" s="T958">cut-PST-3PL</ta>
            <ta e="T960" id="Seg_9443" s="T959">axe-INS</ta>
            <ta e="T961" id="Seg_9444" s="T960">and</ta>
            <ta e="T962" id="Seg_9445" s="T961">I.NOM</ta>
            <ta e="T963" id="Seg_9446" s="T962">skin-PL</ta>
            <ta e="T964" id="Seg_9447" s="T963">this-LAT</ta>
            <ta e="T965" id="Seg_9448" s="T964">make-PST-1SG</ta>
            <ta e="T966" id="Seg_9449" s="T965">then</ta>
            <ta e="T967" id="Seg_9450" s="T966">%%-PL</ta>
            <ta e="T969" id="Seg_9451" s="T968">carry-PST-3PL</ta>
            <ta e="T971" id="Seg_9452" s="T970">make-INF.LAT</ta>
            <ta e="T972" id="Seg_9453" s="T971">and</ta>
            <ta e="T974" id="Seg_9454" s="T973">make-INF.LAT</ta>
            <ta e="T975" id="Seg_9455" s="T974">and</ta>
            <ta e="T976" id="Seg_9456" s="T975">I.NOM</ta>
            <ta e="T977" id="Seg_9457" s="T976">%%</ta>
            <ta e="T978" id="Seg_9458" s="T977">big.[NOM.SG]</ta>
            <ta e="T979" id="Seg_9459" s="T978">skin-PL</ta>
            <ta e="T980" id="Seg_9460" s="T979">make-PST-1SG</ta>
            <ta e="T981" id="Seg_9461" s="T980">and</ta>
            <ta e="T982" id="Seg_9462" s="T981">boot-PL</ta>
            <ta e="T983" id="Seg_9463" s="T982">sew-PST-1SG</ta>
            <ta e="T984" id="Seg_9464" s="T983">and</ta>
            <ta e="T985" id="Seg_9465" s="T984">this.[NOM.SG]</ta>
            <ta e="T986" id="Seg_9466" s="T985">also</ta>
            <ta e="T987" id="Seg_9467" s="T986">I.LAT</ta>
            <ta e="T988" id="Seg_9468" s="T987">help-PST.[3SG]</ta>
            <ta e="T989" id="Seg_9469" s="T988">skin-NOM/GEN.3SG</ta>
            <ta e="T990" id="Seg_9470" s="T989">hang.up-%%-PST.[3SG]</ta>
            <ta e="T991" id="Seg_9471" s="T990">and</ta>
            <ta e="T992" id="Seg_9472" s="T991">NEG</ta>
            <ta e="T993" id="Seg_9473" s="T992">know-PST.[3SG]</ta>
            <ta e="T994" id="Seg_9474" s="T993">I.NOM</ta>
            <ta e="T995" id="Seg_9475" s="T994">this-ACC</ta>
            <ta e="T996" id="Seg_9476" s="T995">learn-TR-PST-1SG</ta>
            <ta e="T997" id="Seg_9477" s="T996">I.NOM</ta>
            <ta e="T998" id="Seg_9478" s="T997">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T1000" id="Seg_9479" s="T999">man-LAT</ta>
            <ta e="T1001" id="Seg_9480" s="T1000">boot-PL</ta>
            <ta e="T1002" id="Seg_9481" s="T1001">sew-PST-1SG</ta>
            <ta e="T1003" id="Seg_9482" s="T1002">and</ta>
            <ta e="T1004" id="Seg_9483" s="T1003">fur.coat.[NOM.SG]</ta>
            <ta e="T1005" id="Seg_9484" s="T1004">sew-PST-1SG</ta>
            <ta e="T1006" id="Seg_9485" s="T1005">sheep-GEN</ta>
            <ta e="T1007" id="Seg_9486" s="T1006">skin-PL</ta>
            <ta e="T1008" id="Seg_9487" s="T1007">and</ta>
            <ta e="T1009" id="Seg_9488" s="T1008">cap.[NOM.SG]</ta>
            <ta e="T1010" id="Seg_9489" s="T1009">sew-PST-1SG</ta>
            <ta e="T1011" id="Seg_9490" s="T1010">shirt-NOM/GEN/ACC.3SG</ta>
            <ta e="T1012" id="Seg_9491" s="T1011">sew-PST-1SG</ta>
            <ta e="T1013" id="Seg_9492" s="T1012">pants-PL</ta>
            <ta e="T1014" id="Seg_9493" s="T1013">sew-PST-1SG</ta>
            <ta e="T1015" id="Seg_9494" s="T1014">this-LAT</ta>
            <ta e="T1016" id="Seg_9495" s="T1015">dress-PST-1SG</ta>
            <ta e="T1017" id="Seg_9496" s="T1016">very</ta>
            <ta e="T1056" id="Seg_9497" s="T1017">good</ta>
            <ta e="T1019" id="Seg_9498" s="T1018">dress-PST-1SG</ta>
            <ta e="T1021" id="Seg_9499" s="T1020">people.[NOM.SG]</ta>
            <ta e="T1022" id="Seg_9500" s="T1021">PTCL</ta>
            <ta e="T1023" id="Seg_9501" s="T1022">look-FRQ-PST-3PL</ta>
            <ta e="T1024" id="Seg_9502" s="T1023">very</ta>
            <ta e="T1025" id="Seg_9503" s="T1024">beautiful.[NOM.SG]</ta>
            <ta e="T1026" id="Seg_9504" s="T1025">man.[NOM.SG]</ta>
            <ta e="T1027" id="Seg_9505" s="T1026">become-RES-PST.[3SG]</ta>
            <ta e="T1028" id="Seg_9506" s="T1027">then</ta>
            <ta e="T1029" id="Seg_9507" s="T1028">we.NOM</ta>
            <ta e="T1030" id="Seg_9508" s="T1029">earth.[NOM.SG]</ta>
            <ta e="T1033" id="Seg_9509" s="T1032">plough-DUR-PST-1PL</ta>
            <ta e="T1034" id="Seg_9510" s="T1033">rye.[NOM.SG]</ta>
            <ta e="T1035" id="Seg_9511" s="T1034">sow-PST-1PL</ta>
            <ta e="T1037" id="Seg_9512" s="T1036">grow-PST.[3SG]</ta>
            <ta e="T1038" id="Seg_9513" s="T1037">very</ta>
            <ta e="T1039" id="Seg_9514" s="T1038">good</ta>
            <ta e="T1040" id="Seg_9515" s="T1039">be-PST.[3SG]</ta>
            <ta e="T1041" id="Seg_9516" s="T1040">rye.[NOM.SG]</ta>
            <ta e="T1042" id="Seg_9517" s="T1041">then</ta>
            <ta e="T1043" id="Seg_9518" s="T1042">flour.[NOM.SG]</ta>
            <ta e="T1044" id="Seg_9519" s="T1043">make-PST-1PL</ta>
            <ta e="T1045" id="Seg_9520" s="T1044">mill-NOM/GEN/ACC.3SG</ta>
            <ta e="T1046" id="Seg_9521" s="T1045">go-PST-1PL</ta>
            <ta e="T1047" id="Seg_9522" s="T1046">bread.[NOM.SG]</ta>
            <ta e="T1048" id="Seg_9523" s="T1047">bake-PST-1PL</ta>
            <ta e="T1049" id="Seg_9524" s="T1048">and</ta>
            <ta e="T1050" id="Seg_9525" s="T1049">wheat.[NOM.SG]</ta>
            <ta e="T1051" id="Seg_9526" s="T1050">rye.[NOM.SG]</ta>
            <ta e="T1052" id="Seg_9527" s="T1051">sow-PST-1PL</ta>
            <ta e="T1053" id="Seg_9528" s="T1052">good.[NOM.SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_9529" s="T0">ты.NOM</ta>
            <ta e="T2" id="Seg_9530" s="T1">вчера</ta>
            <ta e="T4" id="Seg_9531" s="T3">масло.[NOM.SG]</ta>
            <ta e="T5" id="Seg_9532" s="T4">взять-PST-2SG</ta>
            <ta e="T6" id="Seg_9533" s="T5">так</ta>
            <ta e="T7" id="Seg_9534" s="T6">вешать-PST-2SG</ta>
            <ta e="T9" id="Seg_9535" s="T8">вешать-PST-1SG</ta>
            <ta e="T10" id="Seg_9536" s="T9">а</ta>
            <ta e="T11" id="Seg_9537" s="T10">сколько</ta>
            <ta e="T12" id="Seg_9538" s="T11">пять.[NOM.SG]</ta>
            <ta e="T13" id="Seg_9539" s="T12">грамм.[NOM.SG]</ta>
            <ta e="T14" id="Seg_9540" s="T13">деньги.[NOM.SG]</ta>
            <ta e="T15" id="Seg_9541" s="T14">принести-PST-2SG</ta>
            <ta e="T16" id="Seg_9542" s="T15">принести-PST-1SG</ta>
            <ta e="T17" id="Seg_9543" s="T16">два.[NOM.SG]</ta>
            <ta e="T19" id="Seg_9544" s="T18">два.[NOM.SG]</ta>
            <ta e="T20" id="Seg_9545" s="T19">рубль.[NOM.SG]</ta>
            <ta e="T21" id="Seg_9546" s="T20">говорить-DUR-1PL</ta>
            <ta e="T22" id="Seg_9547" s="T21">и</ta>
            <ta e="T23" id="Seg_9548" s="T22">смеяться-DUR-1PL</ta>
            <ta e="T24" id="Seg_9549" s="T23">и</ta>
            <ta e="T26" id="Seg_9550" s="T25">водка.[NOM.SG]</ta>
            <ta e="T27" id="Seg_9551" s="T26">пить-DUR-1PL</ta>
            <ta e="T28" id="Seg_9552" s="T27">и</ta>
            <ta e="T29" id="Seg_9553" s="T28">хлеб.[NOM.SG]</ta>
            <ta e="T30" id="Seg_9554" s="T29">съесть-PRS.[3SG]</ta>
            <ta e="T31" id="Seg_9555" s="T30">и</ta>
            <ta e="T32" id="Seg_9556" s="T31">мясо.[NOM.SG]</ta>
            <ta e="T33" id="Seg_9557" s="T32">съесть-DUR-1PL</ta>
            <ta e="T34" id="Seg_9558" s="T33">и</ta>
            <ta e="T35" id="Seg_9559" s="T34">табак</ta>
            <ta e="T36" id="Seg_9560" s="T35">курить-DUR-1PL</ta>
            <ta e="T37" id="Seg_9561" s="T36">PTCL</ta>
            <ta e="T38" id="Seg_9562" s="T37">мы.NOM</ta>
            <ta e="T39" id="Seg_9563" s="T38">край-LAT/LOC.3SG</ta>
            <ta e="T40" id="Seg_9564" s="T39">мужчина.[NOM.SG]</ta>
            <ta e="T41" id="Seg_9565" s="T40">жить-DUR.[3SG]</ta>
            <ta e="T42" id="Seg_9566" s="T41">этот-GEN</ta>
            <ta e="T43" id="Seg_9567" s="T42">мальчик.[NOM.SG]</ta>
            <ta e="T44" id="Seg_9568" s="T43">мы.LAT</ta>
            <ta e="T45" id="Seg_9569" s="T44">прийти-PST.[3SG]</ta>
            <ta e="T46" id="Seg_9570" s="T45">ящик-LAT</ta>
            <ta e="T47" id="Seg_9571" s="T46">ползти-PST.[3SG]</ta>
            <ta e="T48" id="Seg_9572" s="T47">и</ta>
            <ta e="T49" id="Seg_9573" s="T48">шкаф-LAT</ta>
            <ta e="T50" id="Seg_9574" s="T49">ползти-PST.[3SG]</ta>
            <ta e="T51" id="Seg_9575" s="T50">я.NOM</ta>
            <ta e="T52" id="Seg_9576" s="T51">сказать-PST-1SG</ta>
            <ta e="T53" id="Seg_9577" s="T52">сказать-PST-1SG</ta>
            <ta e="T54" id="Seg_9578" s="T53">что.[NOM.SG]</ta>
            <ta e="T56" id="Seg_9579" s="T55">я.LAT</ta>
            <ta e="T57" id="Seg_9580" s="T56">Ванька.[NOM.SG]</ta>
            <ta e="T59" id="Seg_9581" s="T58">смотреть-FRQ-INF.LAT</ta>
            <ta e="T60" id="Seg_9582" s="T59">я.NOM</ta>
            <ta e="T61" id="Seg_9583" s="T60">этот-ACC</ta>
            <ta e="T62" id="Seg_9584" s="T61">гнать-MOM-PST-1SG</ta>
            <ta e="T63" id="Seg_9585" s="T62">сказать-PRS.[3SG]</ta>
            <ta e="T64" id="Seg_9586" s="T63">Миша-LAT</ta>
            <ta e="T65" id="Seg_9587" s="T64">пойти-EP-IMP.2SG</ta>
            <ta e="T66" id="Seg_9588" s="T65">отсюда</ta>
            <ta e="T67" id="Seg_9589" s="T66">тогда</ta>
            <ta e="T68" id="Seg_9590" s="T67">сегодня</ta>
            <ta e="T69" id="Seg_9591" s="T68">прийти-PST.[3SG]</ta>
            <ta e="T70" id="Seg_9592" s="T69">мешок.[NOM.SG]</ta>
            <ta e="T71" id="Seg_9593" s="T70">кедровый.орех-INS</ta>
            <ta e="T72" id="Seg_9594" s="T71">стоять-PRS.[3SG]</ta>
            <ta e="T73" id="Seg_9595" s="T72">этот.[NOM.SG]</ta>
            <ta e="T74" id="Seg_9596" s="T73">нож.[NOM.SG]</ta>
            <ta e="T75" id="Seg_9597" s="T74">взять-PST.[3SG]</ta>
            <ta e="T76" id="Seg_9598" s="T75">и</ta>
            <ta e="T77" id="Seg_9599" s="T76">мешок.[NOM.SG]</ta>
            <ta e="T78" id="Seg_9600" s="T77">резать-PST.[3SG]</ta>
            <ta e="T79" id="Seg_9601" s="T78">кедровый.орех.[NOM.SG]</ta>
            <ta e="T80" id="Seg_9602" s="T79">взять-PST.[3SG]</ta>
            <ta e="T81" id="Seg_9603" s="T80">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T82" id="Seg_9604" s="T81">выбросить-MOM-PST.[3SG]</ta>
            <ta e="T83" id="Seg_9605" s="T82">сахар.[NOM.SG]</ta>
            <ta e="T84" id="Seg_9606" s="T83">мы.NOM</ta>
            <ta e="T85" id="Seg_9607" s="T84">дом-LOC</ta>
            <ta e="T86" id="Seg_9608" s="T85">женщина.[NOM.SG]</ta>
            <ta e="T87" id="Seg_9609" s="T86">бежать-MOM-PST.[3SG]</ta>
            <ta e="T89" id="Seg_9610" s="T88">%%</ta>
            <ta e="T90" id="Seg_9611" s="T89">украсть-PST.[3SG]</ta>
            <ta e="T91" id="Seg_9612" s="T90">кедровый.орех.[NOM.SG]</ta>
            <ta e="T92" id="Seg_9613" s="T91">а</ta>
            <ta e="T93" id="Seg_9614" s="T92">этот.[NOM.SG]</ta>
            <ta e="T94" id="Seg_9615" s="T93">PTCL</ta>
            <ta e="T95" id="Seg_9616" s="T94">показать-MOM.PRS.[3SG]</ta>
            <ta e="T96" id="Seg_9617" s="T95">тот.[NOM.SG]</ta>
            <ta e="T97" id="Seg_9618" s="T96">кедровый.орех.[NOM.SG]</ta>
            <ta e="T98" id="Seg_9619" s="T97">а</ta>
            <ta e="T99" id="Seg_9620" s="T98">этот.[NOM.SG]</ta>
            <ta e="T100" id="Seg_9621" s="T99">сказать-PST.[3SG]</ta>
            <ta e="T101" id="Seg_9622" s="T100">я.NOM</ta>
            <ta e="T102" id="Seg_9623" s="T101">этот-ACC</ta>
            <ta e="T103" id="Seg_9624" s="T102">убить-FUT-1SG</ta>
            <ta e="T104" id="Seg_9625" s="T103">а</ta>
            <ta e="T105" id="Seg_9626" s="T104">что.[NOM.SG]</ta>
            <ta e="T106" id="Seg_9627" s="T105">NEG</ta>
            <ta e="T107" id="Seg_9628" s="T106">сказать-PST-2PL</ta>
            <ta e="T109" id="Seg_9629" s="T108">вчера</ta>
            <ta e="T110" id="Seg_9630" s="T109">я.LAT</ta>
            <ta e="T111" id="Seg_9631" s="T110">позвать-PST-3PL</ta>
            <ta e="T113" id="Seg_9632" s="T112">я.NOM</ta>
            <ta e="T115" id="Seg_9633" s="T114">родственник-NOM/GEN/ACC.3SG</ta>
            <ta e="T116" id="Seg_9634" s="T115">прийти-IMP.2SG</ta>
            <ta e="T117" id="Seg_9635" s="T116">мы.LAT</ta>
            <ta e="T118" id="Seg_9636" s="T117">есть-INF.LAT</ta>
            <ta e="T120" id="Seg_9637" s="T119">ребенок.[NOM.SG]</ta>
            <ta e="T122" id="Seg_9638" s="T121">умереть-RES-PST.[3SG]</ta>
            <ta e="T123" id="Seg_9639" s="T122">%%</ta>
            <ta e="T124" id="Seg_9640" s="T123">пойти-PST.[3SG]</ta>
            <ta e="T125" id="Seg_9641" s="T124">а</ta>
            <ta e="T126" id="Seg_9642" s="T125">я.NOM</ta>
            <ta e="T127" id="Seg_9643" s="T126">сказать-PST-1SG</ta>
            <ta e="T128" id="Seg_9644" s="T127">прийти-FUT-1SG</ta>
            <ta e="T129" id="Seg_9645" s="T128">а</ta>
            <ta e="T131" id="Seg_9646" s="T130">а</ta>
            <ta e="T132" id="Seg_9647" s="T131">сам-NOM/GEN/ACC.1SG</ta>
            <ta e="T133" id="Seg_9648" s="T132">NEG</ta>
            <ta e="T134" id="Seg_9649" s="T133">пойти-PST-1SG</ta>
            <ta e="T135" id="Seg_9650" s="T134">невестка-NOM/GEN/ACC.1SG</ta>
            <ta e="T136" id="Seg_9651" s="T135">позвать-PST-1SG</ta>
            <ta e="T137" id="Seg_9652" s="T136">позвать-PST-1SG</ta>
            <ta e="T138" id="Seg_9653" s="T137">этот.[NOM.SG]</ta>
            <ta e="T139" id="Seg_9654" s="T138">NEG</ta>
            <ta e="T140" id="Seg_9655" s="T139">пойти-PST.[3SG]</ta>
            <ta e="T141" id="Seg_9656" s="T140">и</ta>
            <ta e="T142" id="Seg_9657" s="T141">я.NOM</ta>
            <ta e="T143" id="Seg_9658" s="T142">NEG</ta>
            <ta e="T144" id="Seg_9659" s="T143">пойти-PST-1SG</ta>
            <ta e="T145" id="Seg_9660" s="T144">там</ta>
            <ta e="T146" id="Seg_9661" s="T145">очень</ta>
            <ta e="T147" id="Seg_9662" s="T146">водка-ADJZ.[NOM.SG]</ta>
            <ta e="T148" id="Seg_9663" s="T147">а</ta>
            <ta e="T149" id="Seg_9664" s="T148">я.LAT</ta>
            <ta e="T150" id="Seg_9665" s="T149">водка.[NOM.SG]</ta>
            <ta e="T151" id="Seg_9666" s="T150">NEG</ta>
            <ta e="T152" id="Seg_9667" s="T151">нужно</ta>
            <ta e="T153" id="Seg_9668" s="T152">я.NOM</ta>
            <ta e="T156" id="Seg_9669" s="T155">этот-ACC</ta>
            <ta e="T157" id="Seg_9670" s="T156">пить-INF.LAT</ta>
            <ta e="T158" id="Seg_9671" s="T157">а</ta>
            <ta e="T159" id="Seg_9672" s="T158">этот.[NOM.SG]</ta>
            <ta e="T160" id="Seg_9673" s="T159">сердиться-MOM-PST.[3SG]</ta>
            <ta e="T161" id="Seg_9674" s="T160">PTCL</ta>
            <ta e="T162" id="Seg_9675" s="T161">сказать-IPFVZ.[3SG]</ta>
            <ta e="T163" id="Seg_9676" s="T162">бабушка-NOM/GEN/ACC.1SG</ta>
            <ta e="T164" id="Seg_9677" s="T163">даже</ta>
            <ta e="T165" id="Seg_9678" s="T164">%%</ta>
            <ta e="T166" id="Seg_9679" s="T165">видеть-PST-1SG</ta>
            <ta e="T167" id="Seg_9680" s="T166">сейчас</ta>
            <ta e="T168" id="Seg_9681" s="T167">теленок-NOM/GEN/ACC.2PL</ta>
            <ta e="T169" id="Seg_9682" s="T168">PTCL</ta>
            <ta e="T170" id="Seg_9683" s="T169">учиться-PST</ta>
            <ta e="T171" id="Seg_9684" s="T170">вы.ACC</ta>
            <ta e="T172" id="Seg_9685" s="T171">ложиться-DUR.[3SG]</ta>
            <ta e="T173" id="Seg_9686" s="T172">сидеть-IMP.2SG</ta>
            <ta e="T174" id="Seg_9687" s="T173">ложиться-DUR.[3SG]</ta>
            <ta e="T175" id="Seg_9688" s="T174">NEG</ta>
            <ta e="T176" id="Seg_9689" s="T175">кричать-DUR.[3SG]</ta>
            <ta e="T177" id="Seg_9690" s="T176">сегодня</ta>
            <ta e="T178" id="Seg_9691" s="T177">теплый.[NOM.SG]</ta>
            <ta e="T179" id="Seg_9692" s="T178">стать-RES-PST.[3SG]</ta>
            <ta e="T180" id="Seg_9693" s="T179">снег.[NOM.SG]</ta>
            <ta e="T181" id="Seg_9694" s="T180">PTCL</ta>
            <ta e="T182" id="Seg_9695" s="T181">%мягкий</ta>
            <ta e="T183" id="Seg_9696" s="T182">быть-PST.[3SG]</ta>
            <ta e="T184" id="Seg_9697" s="T183">снег.[NOM.SG]</ta>
            <ta e="T186" id="Seg_9698" s="T185">скоро</ta>
            <ta e="T187" id="Seg_9699" s="T186">вода.[NOM.SG]</ta>
            <ta e="T188" id="Seg_9700" s="T187">стать-RES-2DU</ta>
            <ta e="T189" id="Seg_9701" s="T188">тогда</ta>
            <ta e="T190" id="Seg_9702" s="T189">трава.[NOM.SG]</ta>
            <ta e="T191" id="Seg_9703" s="T190">расти-FUT-3SG</ta>
            <ta e="T193" id="Seg_9704" s="T192">цветок-PL</ta>
            <ta e="T194" id="Seg_9705" s="T193">расти-FUT-3PL</ta>
            <ta e="T195" id="Seg_9706" s="T194">дерево-PL</ta>
            <ta e="T196" id="Seg_9707" s="T195">PTCL</ta>
            <ta e="T197" id="Seg_9708" s="T196">красивый.[NOM.SG]</ta>
            <ta e="T198" id="Seg_9709" s="T197">стать-FUT-3PL</ta>
            <ta e="T199" id="Seg_9710" s="T198">зеленый-PL</ta>
            <ta e="T200" id="Seg_9711" s="T199">стать-FUT-3PL</ta>
            <ta e="T201" id="Seg_9712" s="T200">PTCL</ta>
            <ta e="T202" id="Seg_9713" s="T201">тогда</ta>
            <ta e="T203" id="Seg_9714" s="T202">лошадь-PL</ta>
            <ta e="T204" id="Seg_9715" s="T203">корова-PL</ta>
            <ta e="T205" id="Seg_9716" s="T204">овца-NOM/GEN/ACC.3SG</ta>
            <ta e="T206" id="Seg_9717" s="T205">пойти-FUT-3PL</ta>
            <ta e="T207" id="Seg_9718" s="T206">трава.[NOM.SG]</ta>
            <ta e="T208" id="Seg_9719" s="T207">съесть-INF.LAT</ta>
            <ta e="T209" id="Seg_9720" s="T208">тогда</ta>
            <ta e="T210" id="Seg_9721" s="T209">трава.[NOM.SG]</ta>
            <ta e="T211" id="Seg_9722" s="T210">большой.[NOM.SG]</ta>
            <ta e="T212" id="Seg_9723" s="T211">расти-FUT-3SG</ta>
            <ta e="T213" id="Seg_9724" s="T212">тогда</ta>
            <ta e="T214" id="Seg_9725" s="T213">этот-ACC</ta>
            <ta e="T216" id="Seg_9726" s="T215">коса-INS</ta>
            <ta e="T217" id="Seg_9727" s="T216">резать-INF.LAT</ta>
            <ta e="T218" id="Seg_9728" s="T217">надо</ta>
            <ta e="T219" id="Seg_9729" s="T218">тогда</ta>
            <ta e="T220" id="Seg_9730" s="T219">солнце-LOC</ta>
            <ta e="T221" id="Seg_9731" s="T220">высохнуть-FUT-3SG</ta>
            <ta e="T222" id="Seg_9732" s="T221">и</ta>
            <ta e="T223" id="Seg_9733" s="T222">собирать-INF.LAT</ta>
            <ta e="T224" id="Seg_9734" s="T223">надо</ta>
            <ta e="T225" id="Seg_9735" s="T224">люди.[NOM.SG]</ta>
            <ta e="T226" id="Seg_9736" s="T225">идти-PRS-3PL</ta>
            <ta e="T227" id="Seg_9737" s="T226">очень</ta>
            <ta e="T228" id="Seg_9738" s="T227">хороший</ta>
            <ta e="T229" id="Seg_9739" s="T228">год.[NOM.SG]</ta>
            <ta e="T230" id="Seg_9740" s="T229">прийти-PST.[3SG]</ta>
            <ta e="T231" id="Seg_9741" s="T230">очень</ta>
            <ta e="T233" id="Seg_9742" s="T232">теплый.[NOM.SG]</ta>
            <ta e="T234" id="Seg_9743" s="T233">я.NOM</ta>
            <ta e="T235" id="Seg_9744" s="T234">%%</ta>
            <ta e="T236" id="Seg_9745" s="T235">мочь-PST-1SG</ta>
            <ta e="T237" id="Seg_9746" s="T236">спать-INF.LAT</ta>
            <ta e="T238" id="Seg_9747" s="T237">тогда</ta>
            <ta e="T239" id="Seg_9748" s="T238">утро</ta>
            <ta e="T240" id="Seg_9749" s="T239">встать-PST-1SG</ta>
            <ta e="T241" id="Seg_9750" s="T240">а</ta>
            <ta e="T242" id="Seg_9751" s="T241">день.[NOM.SG]</ta>
            <ta e="T243" id="Seg_9752" s="T242">день-ACC</ta>
            <ta e="T244" id="Seg_9753" s="T243">PTCL</ta>
            <ta e="T245" id="Seg_9754" s="T244">работать-PST-1SG</ta>
            <ta e="T246" id="Seg_9755" s="T245">корова.[NOM.SG]</ta>
            <ta e="T247" id="Seg_9756" s="T246">доить-PST-1SG</ta>
            <ta e="T248" id="Seg_9757" s="T247">и</ta>
            <ta e="T249" id="Seg_9758" s="T248">кипятить-PST-1SG</ta>
            <ta e="T250" id="Seg_9759" s="T249">делать-PST-1SG</ta>
            <ta e="T251" id="Seg_9760" s="T250">тогда</ta>
            <ta e="T252" id="Seg_9761" s="T251">говорить-PST-1SG</ta>
            <ta e="T253" id="Seg_9762" s="T252">рано</ta>
            <ta e="T254" id="Seg_9763" s="T253">собирать-PST-1SG</ta>
            <ta e="T255" id="Seg_9764" s="T254">и</ta>
            <ta e="T256" id="Seg_9765" s="T255">огонь.[NOM.SG]</ta>
            <ta e="T257" id="Seg_9766" s="T256">светить-PST-1SG</ta>
            <ta e="T258" id="Seg_9767" s="T257">я.LAT</ta>
            <ta e="T259" id="Seg_9768" s="T258">Агинское</ta>
            <ta e="T260" id="Seg_9769" s="T259">поселение-LAT</ta>
            <ta e="T261" id="Seg_9770" s="T260">надо</ta>
            <ta e="T262" id="Seg_9771" s="T261">пойти-INF.LAT</ta>
            <ta e="T263" id="Seg_9772" s="T262">я.LAT</ta>
            <ta e="T264" id="Seg_9773" s="T263">деньги.[NOM.SG]</ta>
            <ta e="T265" id="Seg_9774" s="T264">лежать-DUR.[3SG]</ta>
            <ta e="T266" id="Seg_9775" s="T265">банк-EP-GEN</ta>
            <ta e="T267" id="Seg_9776" s="T266">взять-INF.LAT</ta>
            <ta e="T268" id="Seg_9777" s="T267">надо</ta>
            <ta e="T269" id="Seg_9778" s="T268">и</ta>
            <ta e="T270" id="Seg_9779" s="T269">люди-NOM/GEN/ACC.3SG</ta>
            <ta e="T271" id="Seg_9780" s="T270">дать-INF.LAT</ta>
            <ta e="T272" id="Seg_9781" s="T271">я.NOM</ta>
            <ta e="T273" id="Seg_9782" s="T272">этот-PL-LOC</ta>
            <ta e="T274" id="Seg_9783" s="T273">взять-PST-1SG</ta>
            <ta e="T275" id="Seg_9784" s="T274">я.NOM</ta>
            <ta e="T276" id="Seg_9785" s="T275">много</ta>
            <ta e="T277" id="Seg_9786" s="T276">говорить-PRS-1SG</ta>
            <ta e="T279" id="Seg_9787" s="T278">сейчас</ta>
            <ta e="T281" id="Seg_9788" s="T280">говорить-DUR-3PL</ta>
            <ta e="T282" id="Seg_9789" s="T281">очень</ta>
            <ta e="T283" id="Seg_9790" s="T282">ты.NOM</ta>
            <ta e="T284" id="Seg_9791" s="T283">ум-ADJZ.[NOM.SG]</ta>
            <ta e="T285" id="Seg_9792" s="T284">девушка.[NOM.SG]</ta>
            <ta e="T286" id="Seg_9793" s="T285">всегда</ta>
            <ta e="T287" id="Seg_9794" s="T286">я.LAT</ta>
            <ta e="T288" id="Seg_9795" s="T287">я.LAT</ta>
            <ta e="T289" id="Seg_9796" s="T288">надо</ta>
            <ta e="T290" id="Seg_9797" s="T289">говорить-INF.LAT</ta>
            <ta e="T291" id="Seg_9798" s="T290">сейчас</ta>
            <ta e="T292" id="Seg_9799" s="T291">ты.NOM</ta>
            <ta e="T293" id="Seg_9800" s="T292">говорить-EP-IMP.2SG</ta>
            <ta e="T294" id="Seg_9801" s="T293">а</ta>
            <ta e="T295" id="Seg_9802" s="T294">я.NOM</ta>
            <ta e="T296" id="Seg_9803" s="T295">NEG</ta>
            <ta e="T297" id="Seg_9804" s="T296">говорить-FUT-1SG</ta>
            <ta e="T299" id="Seg_9805" s="T298">я.NOM</ta>
            <ta e="T300" id="Seg_9806" s="T299">NEG</ta>
            <ta e="T301" id="Seg_9807" s="T300">знать-PRS-1SG</ta>
            <ta e="T302" id="Seg_9808" s="T301">что.[NOM.SG]</ta>
            <ta e="T303" id="Seg_9809" s="T302">говорить-INF.LAT</ta>
            <ta e="T304" id="Seg_9810" s="T303">ты.NOM</ta>
            <ta e="T305" id="Seg_9811" s="T304">говорить-EP-IMP.2SG</ta>
            <ta e="T306" id="Seg_9812" s="T305">а</ta>
            <ta e="T308" id="Seg_9813" s="T307">а</ta>
            <ta e="T309" id="Seg_9814" s="T308">я.NOM</ta>
            <ta e="T310" id="Seg_9815" s="T309">что.[NOM.SG]</ta>
            <ta e="T311" id="Seg_9816" s="T310">всегда</ta>
            <ta e="T313" id="Seg_9817" s="T312">говорить-INF.LAT</ta>
            <ta e="T317" id="Seg_9818" s="T315">ложиться-FUT-1SG=PTCL</ta>
            <ta e="T318" id="Seg_9819" s="T317">я.NOM</ta>
            <ta e="T319" id="Seg_9820" s="T318">дом-NOM/GEN/ACC.1SG</ta>
            <ta e="T322" id="Seg_9821" s="T321">я.NOM</ta>
            <ta e="T323" id="Seg_9822" s="T322">что.[NOM.SG]=INDEF</ta>
            <ta e="T324" id="Seg_9823" s="T323">NEG</ta>
            <ta e="T325" id="Seg_9824" s="T324">знать-PRS-1SG</ta>
            <ta e="T326" id="Seg_9825" s="T325">думать-IMP.2SG</ta>
            <ta e="T327" id="Seg_9826" s="T326">что=INDEF</ta>
            <ta e="T328" id="Seg_9827" s="T327">лгать-IMP.2SG</ta>
            <ta e="T329" id="Seg_9828" s="T328">что=INDEF</ta>
            <ta e="T330" id="Seg_9829" s="T329">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T331" id="Seg_9830" s="T330">далеко-ACC</ta>
            <ta e="T1057" id="Seg_9831" s="T331">пойти-CVB</ta>
            <ta e="T332" id="Seg_9832" s="T1057">исчезнуть-PST.[3SG]</ta>
            <ta e="T333" id="Seg_9833" s="T332">а</ta>
            <ta e="T334" id="Seg_9834" s="T333">я.LAT</ta>
            <ta e="T335" id="Seg_9835" s="T334">очень</ta>
            <ta e="T336" id="Seg_9836" s="T335">хороший</ta>
            <ta e="T337" id="Seg_9837" s="T336">кто.[NOM.SG]=INDEF</ta>
            <ta e="T338" id="Seg_9838" s="T337">NEG</ta>
            <ta e="T339" id="Seg_9839" s="T338">бить-PRS.[3SG]</ta>
            <ta e="T340" id="Seg_9840" s="T339">я.NOM</ta>
            <ta e="T341" id="Seg_9841" s="T340">%%</ta>
            <ta e="T342" id="Seg_9842" s="T341">шуметь-PRS-1SG</ta>
            <ta e="T343" id="Seg_9843" s="T342">%%</ta>
            <ta e="T344" id="Seg_9844" s="T343">петь-DUR-1SG</ta>
            <ta e="T345" id="Seg_9845" s="T344">прыгнуть-DUR-1SG</ta>
            <ta e="T346" id="Seg_9846" s="T345">PTCL</ta>
            <ta e="T347" id="Seg_9847" s="T346">а</ta>
            <ta e="T348" id="Seg_9848" s="T347">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T349" id="Seg_9849" s="T348">NEG</ta>
            <ta e="T350" id="Seg_9850" s="T349">слушать-PRS-1SG</ta>
            <ta e="T351" id="Seg_9851" s="T350">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T352" id="Seg_9852" s="T351">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T353" id="Seg_9853" s="T352">этот.[NOM.SG]</ta>
            <ta e="T354" id="Seg_9854" s="T353">%%-PST.[3SG]</ta>
            <ta e="T355" id="Seg_9855" s="T354">что</ta>
            <ta e="T356" id="Seg_9856" s="T355">я.NOM</ta>
            <ta e="T358" id="Seg_9857" s="T357">что</ta>
            <ta e="T359" id="Seg_9858" s="T358">я.NOM</ta>
            <ta e="T360" id="Seg_9859" s="T359">шуметь-PRS-1SG</ta>
            <ta e="T361" id="Seg_9860" s="T360">я.NOM</ta>
            <ta e="T362" id="Seg_9861" s="T361">товарищ.[NOM.SG]</ta>
            <ta e="T363" id="Seg_9862" s="T362">сегодня</ta>
            <ta e="T364" id="Seg_9863" s="T363">вечер-LOC.ADV</ta>
            <ta e="T365" id="Seg_9864" s="T364">PTCL</ta>
            <ta e="T366" id="Seg_9865" s="T365">водка.[NOM.SG]</ta>
            <ta e="T369" id="Seg_9866" s="T368">песня.[NOM.SG]</ta>
            <ta e="T371" id="Seg_9867" s="T370">петь-PST.[3SG]</ta>
            <ta e="T372" id="Seg_9868" s="T371">бороться-PST.[3SG]</ta>
            <ta e="T373" id="Seg_9869" s="T372">PTCL</ta>
            <ta e="T374" id="Seg_9870" s="T373">ругать-DES-PST.[3SG]</ta>
            <ta e="T375" id="Seg_9871" s="T374">PTCL</ta>
            <ta e="T376" id="Seg_9872" s="T375">Эля.[NOM.SG]</ta>
            <ta e="T377" id="Seg_9873" s="T376">один</ta>
            <ta e="T378" id="Seg_9874" s="T377">жить-DUR.[3SG]</ta>
            <ta e="T379" id="Seg_9875" s="T378">всегда</ta>
            <ta e="T380" id="Seg_9876" s="T379">жениться-DUR-3PL</ta>
            <ta e="T381" id="Seg_9877" s="T380">а</ta>
            <ta e="T382" id="Seg_9878" s="T381">этот.[NOM.SG]</ta>
            <ta e="T383" id="Seg_9879" s="T382">всегда</ta>
            <ta e="T384" id="Seg_9880" s="T383">NEG</ta>
            <ta e="T385" id="Seg_9881" s="T384">пойти-PRS.[3SG]</ta>
            <ta e="T386" id="Seg_9882" s="T385">кто-LAT=INDEF</ta>
            <ta e="T387" id="Seg_9883" s="T386">NEG</ta>
            <ta e="T388" id="Seg_9884" s="T387">пойти-PRS.[3SG]</ta>
            <ta e="T394" id="Seg_9885" s="T393">этот-GEN</ta>
            <ta e="T395" id="Seg_9886" s="T394">PTCL</ta>
            <ta e="T396" id="Seg_9887" s="T395">мать-NOM/GEN.3SG</ta>
            <ta e="T397" id="Seg_9888" s="T396">отец-NOM/GEN.3SG</ta>
            <ta e="T398" id="Seg_9889" s="T397">быть-PRS.[3SG]</ta>
            <ta e="T399" id="Seg_9890" s="T398">сказать-PRS-3PL</ta>
            <ta e="T400" id="Seg_9891" s="T399">пойти-EP-IMP.2SG</ta>
            <ta e="T401" id="Seg_9892" s="T400">мужчина-LAT</ta>
            <ta e="T402" id="Seg_9893" s="T401">а</ta>
            <ta e="T403" id="Seg_9894" s="T402">этот.[NOM.SG]</ta>
            <ta e="T404" id="Seg_9895" s="T403">как=INDEF</ta>
            <ta e="T405" id="Seg_9896" s="T404">NEG</ta>
            <ta e="T406" id="Seg_9897" s="T405">пойти-PRS.[3SG]</ta>
            <ta e="T407" id="Seg_9898" s="T406">прийти-PST.[3SG]</ta>
            <ta e="T408" id="Seg_9899" s="T407">я.LAT</ta>
            <ta e="T409" id="Seg_9900" s="T408">жениться-INF.LAT</ta>
            <ta e="T410" id="Seg_9901" s="T409">NEG</ta>
            <ta e="T411" id="Seg_9902" s="T410">красивый.[NOM.SG]</ta>
            <ta e="T412" id="Seg_9903" s="T411">мужчина.[NOM.SG]</ta>
            <ta e="T413" id="Seg_9904" s="T412">я.LAT</ta>
            <ta e="T414" id="Seg_9905" s="T413">не</ta>
            <ta e="T415" id="Seg_9906" s="T414">хочется</ta>
            <ta e="T416" id="Seg_9907" s="T415">пойти-INF.LAT</ta>
            <ta e="T417" id="Seg_9908" s="T416">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T418" id="Seg_9909" s="T417">сказать-IPFVZ.[3SG]</ta>
            <ta e="T419" id="Seg_9910" s="T418">что.[NOM.SG]</ta>
            <ta e="T420" id="Seg_9911" s="T419">ты.NOM</ta>
            <ta e="T421" id="Seg_9912" s="T420">NEG</ta>
            <ta e="T422" id="Seg_9913" s="T421">пойти-FUT-2SG</ta>
            <ta e="T423" id="Seg_9914" s="T422">NEG</ta>
            <ta e="T424" id="Seg_9915" s="T423">ну</ta>
            <ta e="T425" id="Seg_9916" s="T424">NEG.AUX-IMP.2SG</ta>
            <ta e="T426" id="Seg_9917" s="T425">пойти-EP-CNG</ta>
            <ta e="T427" id="Seg_9918" s="T426">я.NOM</ta>
            <ta e="T428" id="Seg_9919" s="T427">бежать-MOM-PST-1SG</ta>
            <ta e="T429" id="Seg_9920" s="T428">тогда</ta>
            <ta e="T430" id="Seg_9921" s="T429">я.NOM</ta>
            <ta e="T433" id="Seg_9922" s="T432">мужчина-NOM/GEN/ACC.1SG</ta>
            <ta e="T434" id="Seg_9923" s="T433">прийти-PST.[3SG]</ta>
            <ta e="T435" id="Seg_9924" s="T434">я.ACC</ta>
            <ta e="T436" id="Seg_9925" s="T435">жениться-INF.LAT</ta>
            <ta e="T437" id="Seg_9926" s="T436">этот-LAT</ta>
            <ta e="T439" id="Seg_9927" s="T438">год.[NOM.SG]</ta>
            <ta e="T440" id="Seg_9928" s="T439">и</ta>
            <ta e="T441" id="Seg_9929" s="T440">я.LAT</ta>
            <ta e="T442" id="Seg_9930" s="T441">мало</ta>
            <ta e="T443" id="Seg_9931" s="T442">год.[NOM.SG]</ta>
            <ta e="T444" id="Seg_9932" s="T443">и</ta>
            <ta e="T445" id="Seg_9933" s="T444">я.LAT</ta>
            <ta e="T446" id="Seg_9934" s="T445">мало</ta>
            <ta e="T447" id="Seg_9935" s="T446">год.[NOM.SG]</ta>
            <ta e="T448" id="Seg_9936" s="T447">я.NOM</ta>
            <ta e="T450" id="Seg_9937" s="T449">я.NOM</ta>
            <ta e="T451" id="Seg_9938" s="T450">сказать-IPFVZ-1SG</ta>
            <ta e="T452" id="Seg_9939" s="T451">пойти-EP-IMP.2SG</ta>
            <ta e="T453" id="Seg_9940" s="T452">отец-LAT</ta>
            <ta e="T454" id="Seg_9941" s="T453">и</ta>
            <ta e="T455" id="Seg_9942" s="T454">мать-LAT</ta>
            <ta e="T456" id="Seg_9943" s="T455">жениться-IMP.2SG</ta>
            <ta e="T457" id="Seg_9944" s="T456">этот-PL</ta>
            <ta e="T458" id="Seg_9945" s="T457">дать-FUT-3PL</ta>
            <ta e="T459" id="Seg_9946" s="T458">нет</ta>
            <ta e="T460" id="Seg_9947" s="T459">NEG</ta>
            <ta e="T461" id="Seg_9948" s="T460">дать-FUT-3PL</ta>
            <ta e="T462" id="Seg_9949" s="T461">тогда</ta>
            <ta e="T463" id="Seg_9950" s="T462">я.ACC</ta>
            <ta e="T464" id="Seg_9951" s="T463">говорить-MOM-PST-3PL</ta>
            <ta e="T465" id="Seg_9952" s="T464">и</ta>
            <ta e="T467" id="Seg_9953" s="T466">пойти-PST-1PL</ta>
            <ta e="T469" id="Seg_9954" s="T468">лошадь-3SG-INS</ta>
            <ta e="T470" id="Seg_9955" s="T469">этот-LAT</ta>
            <ta e="T471" id="Seg_9956" s="T470">тогда</ta>
            <ta e="T472" id="Seg_9957" s="T471">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T473" id="Seg_9958" s="T472">прийти-PST.[3SG]</ta>
            <ta e="T474" id="Seg_9959" s="T473">там</ta>
            <ta e="T475" id="Seg_9960" s="T474">мы.LAT</ta>
            <ta e="T476" id="Seg_9961" s="T475">этот.[NOM.SG]</ta>
            <ta e="T478" id="Seg_9962" s="T477">здесь</ta>
            <ta e="T479" id="Seg_9963" s="T478">прийти-PST.[3SG]</ta>
            <ta e="T480" id="Seg_9964" s="T479">трава.[NOM.SG]</ta>
            <ta e="T481" id="Seg_9965" s="T480">резать-INF.LAT</ta>
            <ta e="T482" id="Seg_9966" s="T481">тогда</ta>
            <ta e="T483" id="Seg_9967" s="T482">пойти-PST.[3SG]</ta>
            <ta e="T484" id="Seg_9968" s="T483">река-LAT</ta>
            <ta e="T485" id="Seg_9969" s="T484">и</ta>
            <ta e="T486" id="Seg_9970" s="T485">там</ta>
            <ta e="T487" id="Seg_9971" s="T486">умереть-RES-PST.[3SG]</ta>
            <ta e="T488" id="Seg_9972" s="T487">тогда</ta>
            <ta e="T489" id="Seg_9973" s="T488">я.NOM</ta>
            <ta e="T490" id="Seg_9974" s="T489">дом-LAT/LOC.3SG</ta>
            <ta e="T491" id="Seg_9975" s="T490">прийти-PST-1SG</ta>
            <ta e="T492" id="Seg_9976" s="T491">и</ta>
            <ta e="T493" id="Seg_9977" s="T492">очень</ta>
            <ta e="T494" id="Seg_9978" s="T493">сильно</ta>
            <ta e="T495" id="Seg_9979" s="T494">думать-PST-1SG</ta>
            <ta e="T496" id="Seg_9980" s="T495">сердце-NOM/GEN/ACC.1SG</ta>
            <ta e="T497" id="Seg_9981" s="T496">болеть-PST</ta>
            <ta e="T498" id="Seg_9982" s="T497">плакать-PST-1SG</ta>
            <ta e="T499" id="Seg_9983" s="T498">жалеть-PST-1SG</ta>
            <ta e="T500" id="Seg_9984" s="T499">этот-ACC</ta>
            <ta e="T501" id="Seg_9985" s="T500">PTCL</ta>
            <ta e="T502" id="Seg_9986" s="T501">тогда</ta>
            <ta e="T503" id="Seg_9987" s="T502">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T504" id="Seg_9988" s="T503">я.LAT</ta>
            <ta e="T505" id="Seg_9989" s="T504">вода.[NOM.SG]</ta>
            <ta e="T506" id="Seg_9990" s="T505">вылечить-PST.[3SG]</ta>
            <ta e="T507" id="Seg_9991" s="T506">%%</ta>
            <ta e="T508" id="Seg_9992" s="T507">я.ACC</ta>
            <ta e="T509" id="Seg_9993" s="T508">тогда</ta>
            <ta e="T510" id="Seg_9994" s="T509">я.NOM</ta>
            <ta e="T511" id="Seg_9995" s="T510">INCH</ta>
            <ta e="T512" id="Seg_9996" s="T511">песня.[NOM.SG]</ta>
            <ta e="T514" id="Seg_9997" s="T513">петь-INF.LAT</ta>
            <ta e="T515" id="Seg_9998" s="T514">тогда</ta>
            <ta e="T516" id="Seg_9999" s="T515">я.NOM</ta>
            <ta e="T517" id="Seg_10000" s="T516">прийти-PST-1SG</ta>
            <ta e="T518" id="Seg_10001" s="T517">дом-LAT/LOC.1SG</ta>
            <ta e="T519" id="Seg_10002" s="T518">жить-PST-1SG</ta>
            <ta e="T520" id="Seg_10003" s="T519">тогда</ta>
            <ta e="T521" id="Seg_10004" s="T520">пойти-PST-1SG</ta>
            <ta e="T523" id="Seg_10005" s="T522">Пермяково-LAT</ta>
            <ta e="T525" id="Seg_10006" s="T524">жила.[NOM.SG]</ta>
            <ta e="T526" id="Seg_10007" s="T525">играть-PST-1PL</ta>
            <ta e="T527" id="Seg_10008" s="T526">прыгнуть-1PL</ta>
            <ta e="T528" id="Seg_10009" s="T527">гармонь-LAT/LOC.1SG</ta>
            <ta e="T529" id="Seg_10010" s="T528">играть-PST-1PL</ta>
            <ta e="T530" id="Seg_10011" s="T529">песня.[NOM.SG]</ta>
            <ta e="T531" id="Seg_10012" s="T530">петь-PST-1PL</ta>
            <ta e="T532" id="Seg_10013" s="T531">тогда</ta>
            <ta e="T533" id="Seg_10014" s="T532">там</ta>
            <ta e="T534" id="Seg_10015" s="T533">мужчина.[NOM.SG]</ta>
            <ta e="T535" id="Seg_10016" s="T534">прийти-PST.[3SG]</ta>
            <ta e="T536" id="Seg_10017" s="T535">%Нагорный.[NOM.SG]</ta>
            <ta e="T537" id="Seg_10018" s="T536">борода-NOM/GEN/ACC.3SG</ta>
            <ta e="T538" id="Seg_10019" s="T537">большой.[NOM.SG]</ta>
            <ta e="T540" id="Seg_10020" s="T539">и</ta>
            <ta e="T541" id="Seg_10021" s="T540">красный-PL</ta>
            <ta e="T542" id="Seg_10022" s="T541">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T543" id="Seg_10023" s="T542">красный.[NOM.SG]</ta>
            <ta e="T544" id="Seg_10024" s="T543">тогда</ta>
            <ta e="T545" id="Seg_10025" s="T544">я.NOM</ta>
            <ta e="T546" id="Seg_10026" s="T545">дом-LAT/LOC.1SG</ta>
            <ta e="T547" id="Seg_10027" s="T546">прийти-PST-1SG</ta>
            <ta e="T548" id="Seg_10028" s="T547">и</ta>
            <ta e="T549" id="Seg_10029" s="T548">прийти-PST.[3SG]</ta>
            <ta e="T550" id="Seg_10030" s="T549">я.ACC</ta>
            <ta e="T551" id="Seg_10031" s="T550">жениться-INF.LAT</ta>
            <ta e="T552" id="Seg_10032" s="T551">я.NOM</ta>
            <ta e="T553" id="Seg_10033" s="T552">сказать-IPFVZ-1SG</ta>
            <ta e="T554" id="Seg_10034" s="T553">ты.NOM</ta>
            <ta e="T555" id="Seg_10035" s="T554">мальчик.[NOM.SG]</ta>
            <ta e="T556" id="Seg_10036" s="T555">%%</ta>
            <ta e="T558" id="Seg_10037" s="T557">как</ta>
            <ta e="T559" id="Seg_10038" s="T558">я.NOM</ta>
            <ta e="T560" id="Seg_10039" s="T559">такой.[NOM.SG]</ta>
            <ta e="T561" id="Seg_10040" s="T560">ну</ta>
            <ta e="T562" id="Seg_10041" s="T561">я.NOM</ta>
            <ta e="T563" id="Seg_10042" s="T562">ты.NOM-COM</ta>
            <ta e="T564" id="Seg_10043" s="T563">NEG</ta>
            <ta e="T565" id="Seg_10044" s="T564">жить-FUT-1SG</ta>
            <ta e="T566" id="Seg_10045" s="T565">а</ta>
            <ta e="T567" id="Seg_10046" s="T566">сын-COM</ta>
            <ta e="T568" id="Seg_10047" s="T567">тогда</ta>
            <ta e="T569" id="Seg_10048" s="T568">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T570" id="Seg_10049" s="T569">сказать-IPFVZ.[3SG]</ta>
            <ta e="T571" id="Seg_10050" s="T570">пойти-EP-IMP.2SG</ta>
            <ta e="T572" id="Seg_10051" s="T571">этот-GEN</ta>
            <ta e="T573" id="Seg_10052" s="T572">деньги.[NOM.SG]</ta>
            <ta e="T574" id="Seg_10053" s="T573">много</ta>
            <ta e="T575" id="Seg_10054" s="T574">очень</ta>
            <ta e="T577" id="Seg_10055" s="T576">хороший</ta>
            <ta e="T578" id="Seg_10056" s="T577">мужчина.[NOM.SG]</ta>
            <ta e="T579" id="Seg_10057" s="T578">а</ta>
            <ta e="T580" id="Seg_10058" s="T579">я.NOM</ta>
            <ta e="T581" id="Seg_10059" s="T580">стоять-PRS-1SG</ta>
            <ta e="T582" id="Seg_10060" s="T581">NEG</ta>
            <ta e="T583" id="Seg_10061" s="T582">и</ta>
            <ta e="T584" id="Seg_10062" s="T583">пойти-EP-IMP.2SG</ta>
            <ta e="T585" id="Seg_10063" s="T584">и</ta>
            <ta e="T586" id="Seg_10064" s="T585">жить-IMP.2SG</ta>
            <ta e="T587" id="Seg_10065" s="T586">этот-COM</ta>
            <ta e="T588" id="Seg_10066" s="T587">один.[NOM.SG]</ta>
            <ta e="T589" id="Seg_10067" s="T588">мужчина.[NOM.SG]</ta>
            <ta e="T590" id="Seg_10068" s="T589">прийти-PST.[3SG]</ta>
            <ta e="T591" id="Seg_10069" s="T590">жениться-INF.LAT</ta>
            <ta e="T592" id="Seg_10070" s="T591">а</ta>
            <ta e="T593" id="Seg_10071" s="T592">я.NOM</ta>
            <ta e="T594" id="Seg_10072" s="T593">NEG</ta>
            <ta e="T595" id="Seg_10073" s="T594">пойти-PRS-1SG</ta>
            <ta e="T596" id="Seg_10074" s="T595">а</ta>
            <ta e="T597" id="Seg_10075" s="T596">мать</ta>
            <ta e="T598" id="Seg_10076" s="T597">NEG</ta>
            <ta e="T599" id="Seg_10077" s="T598">пойти-PRS-1SG</ta>
            <ta e="T600" id="Seg_10078" s="T599">этот.[NOM.SG]</ta>
            <ta e="T601" id="Seg_10079" s="T600">NEG</ta>
            <ta e="T602" id="Seg_10080" s="T601">красивый.[NOM.SG]</ta>
            <ta e="T603" id="Seg_10081" s="T602">нога.[NOM.SG]</ta>
            <ta e="T604" id="Seg_10082" s="T603">нога-NOM/GEN.3SG</ta>
            <ta e="T605" id="Seg_10083" s="T604">PTCL</ta>
            <ta e="T607" id="Seg_10084" s="T606">кривой.[NOM.SG]</ta>
            <ta e="T609" id="Seg_10085" s="T608">борода-NOM/GEN/ACC.3SG</ta>
            <ta e="T610" id="Seg_10086" s="T609">красный.[NOM.SG]</ta>
            <ta e="T611" id="Seg_10087" s="T610">язык-NOM/GEN.3SG</ta>
            <ta e="T612" id="Seg_10088" s="T611">NEG</ta>
            <ta e="T613" id="Seg_10089" s="T612">говорить-PRS.[3SG]</ta>
            <ta e="T614" id="Seg_10090" s="T613">NEG</ta>
            <ta e="T615" id="Seg_10091" s="T614">хороший</ta>
            <ta e="T616" id="Seg_10092" s="T615">говорить-PRS.[3SG]</ta>
            <ta e="T617" id="Seg_10093" s="T616">ухо-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T618" id="Seg_10094" s="T617">NEG</ta>
            <ta e="T619" id="Seg_10095" s="T618">слушать-PRS-3PL</ta>
            <ta e="T620" id="Seg_10096" s="T619">а</ta>
            <ta e="T621" id="Seg_10097" s="T620">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T622" id="Seg_10098" s="T621">сказать-IPFVZ.[3SG]</ta>
            <ta e="T623" id="Seg_10099" s="T622">пойти-EP-IMP.2SG</ta>
            <ta e="T624" id="Seg_10100" s="T623">этот-GEN</ta>
            <ta e="T625" id="Seg_10101" s="T624">корова.[NOM.SG]</ta>
            <ta e="T626" id="Seg_10102" s="T625">быть-PRS.[3SG]</ta>
            <ta e="T627" id="Seg_10103" s="T626">и</ta>
            <ta e="T628" id="Seg_10104" s="T627">лошадь.[NOM.SG]</ta>
            <ta e="T629" id="Seg_10105" s="T628">быть-PRS.[3SG]</ta>
            <ta e="T630" id="Seg_10106" s="T629">и</ta>
            <ta e="T631" id="Seg_10107" s="T630">хлеб.[NOM.SG]</ta>
            <ta e="T632" id="Seg_10108" s="T631">много</ta>
            <ta e="T633" id="Seg_10109" s="T632">а</ta>
            <ta e="T634" id="Seg_10110" s="T633">я.NOM</ta>
            <ta e="T635" id="Seg_10111" s="T634">я.NOM</ta>
            <ta e="T636" id="Seg_10112" s="T635">сказать-PST-1SG</ta>
            <ta e="T637" id="Seg_10113" s="T636">я.LAT</ta>
            <ta e="T638" id="Seg_10114" s="T637">лошадь-COM</ta>
            <ta e="T639" id="Seg_10115" s="T638">NEG</ta>
            <ta e="T640" id="Seg_10116" s="T639">жить-INF.LAT</ta>
            <ta e="T641" id="Seg_10117" s="T640">и</ta>
            <ta e="T642" id="Seg_10118" s="T641">корова-LAT</ta>
            <ta e="T643" id="Seg_10119" s="T642">NEG</ta>
            <ta e="T644" id="Seg_10120" s="T643">жить-INF.LAT</ta>
            <ta e="T645" id="Seg_10121" s="T644">надо</ta>
            <ta e="T646" id="Seg_10122" s="T645">ты.DAT</ta>
            <ta e="T647" id="Seg_10123" s="T646">так</ta>
            <ta e="T648" id="Seg_10124" s="T647">ты.NOM</ta>
            <ta e="T649" id="Seg_10125" s="T648">ты.DAT</ta>
            <ta e="T650" id="Seg_10126" s="T649">нужно</ta>
            <ta e="T651" id="Seg_10127" s="T650">так</ta>
            <ta e="T652" id="Seg_10128" s="T651">ты.NOM</ta>
            <ta e="T653" id="Seg_10129" s="T652">пойти-EP-IMP.2SG</ta>
            <ta e="T654" id="Seg_10130" s="T653">станица-LOC</ta>
            <ta e="T655" id="Seg_10131" s="T654">прийти-PST.[3SG]</ta>
            <ta e="T656" id="Seg_10132" s="T655">один.[NOM.SG]</ta>
            <ta e="T657" id="Seg_10133" s="T656">мужчина.[NOM.SG]</ta>
            <ta e="T658" id="Seg_10134" s="T657">этот-ACC</ta>
            <ta e="T659" id="Seg_10135" s="T658">женщина.[NOM.SG]</ta>
            <ta e="T660" id="Seg_10136" s="T659">выбросить-MOM-PST.[3SG]</ta>
            <ta e="T661" id="Seg_10137" s="T660">этот.[NOM.SG]</ta>
            <ta e="T662" id="Seg_10138" s="T661">INCH</ta>
            <ta e="T663" id="Seg_10139" s="T662">я.ACC</ta>
            <ta e="T664" id="Seg_10140" s="T663">жениться-INF.LAT</ta>
            <ta e="T665" id="Seg_10141" s="T664">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T666" id="Seg_10142" s="T665">стал</ta>
            <ta e="T667" id="Seg_10143" s="T666">кипятить-INF.LAT</ta>
            <ta e="T668" id="Seg_10144" s="T667">мясо.[NOM.SG]</ta>
            <ta e="T669" id="Seg_10145" s="T668">этот-ACC.PL</ta>
            <ta e="T671" id="Seg_10146" s="T670">кормить-INF.LAT</ta>
            <ta e="T672" id="Seg_10147" s="T671">а</ta>
            <ta e="T673" id="Seg_10148" s="T672">я.NOM</ta>
            <ta e="T674" id="Seg_10149" s="T673">сказать-PRS-1SG</ta>
            <ta e="T675" id="Seg_10150" s="T674">пойти-INF.LAT</ta>
            <ta e="T676" id="Seg_10151" s="T675">или</ta>
            <ta e="T677" id="Seg_10152" s="T676">NEG</ta>
            <ta e="T678" id="Seg_10153" s="T677">пойти-INF.LAT</ta>
            <ta e="T680" id="Seg_10154" s="T679">пойти-EP-IMP.2SG</ta>
            <ta e="T682" id="Seg_10155" s="T681">NEG.AUX-IMP.2SG</ta>
            <ta e="T683" id="Seg_10156" s="T682">пойти-EP-CNG</ta>
            <ta e="T684" id="Seg_10157" s="T683">ты.NOM</ta>
            <ta e="T685" id="Seg_10158" s="T684">скоро</ta>
            <ta e="T687" id="Seg_10159" s="T686">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T688" id="Seg_10160" s="T687">большой.[NOM.SG]</ta>
            <ta e="T689" id="Seg_10161" s="T688">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T690" id="Seg_10162" s="T689">знать-2SG</ta>
            <ta e="T691" id="Seg_10163" s="T690">что.[NOM.SG]</ta>
            <ta e="T692" id="Seg_10164" s="T691">делать-INF.LAT</ta>
            <ta e="T693" id="Seg_10165" s="T692">тогда</ta>
            <ta e="T694" id="Seg_10166" s="T693">этот.[NOM.SG]</ta>
            <ta e="T695" id="Seg_10167" s="T694">мужчина.[NOM.SG]</ta>
            <ta e="T697" id="Seg_10168" s="T696">прийти-PST.[3SG]</ta>
            <ta e="T698" id="Seg_10169" s="T697">я.LAT</ta>
            <ta e="T699" id="Seg_10170" s="T698">сесть-PST.[3SG]</ta>
            <ta e="T700" id="Seg_10171" s="T699">и</ta>
            <ta e="T701" id="Seg_10172" s="T700">INCH</ta>
            <ta e="T702" id="Seg_10173" s="T701">жениться-INF.LAT</ta>
            <ta e="T703" id="Seg_10174" s="T702">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T704" id="Seg_10175" s="T703">и</ta>
            <ta e="T705" id="Seg_10176" s="T704">INCH</ta>
            <ta e="T706" id="Seg_10177" s="T705">Бог-LAT</ta>
            <ta e="T707" id="Seg_10178" s="T706">молиться-INF.LAT</ta>
            <ta e="T708" id="Seg_10179" s="T707">NEG</ta>
            <ta e="T709" id="Seg_10180" s="T708">бросить-FUT-1SG</ta>
            <ta e="T710" id="Seg_10181" s="T709">ты.ACC</ta>
            <ta e="T711" id="Seg_10182" s="T710">а</ta>
            <ta e="T712" id="Seg_10183" s="T711">%%-FUT-1SG</ta>
            <ta e="T713" id="Seg_10184" s="T712">хороший</ta>
            <ta e="T714" id="Seg_10185" s="T713">жить-FUT-1PL</ta>
            <ta e="T715" id="Seg_10186" s="T714">тогда</ta>
            <ta e="T716" id="Seg_10187" s="T715">этот.[NOM.SG]</ta>
            <ta e="T717" id="Seg_10188" s="T716">лошадь.[NOM.SG]</ta>
            <ta e="T718" id="Seg_10189" s="T717">запрячь-PST.[3SG]</ta>
            <ta e="T720" id="Seg_10190" s="T719">мы.NOM</ta>
            <ta e="T721" id="Seg_10191" s="T720">я.NOM</ta>
            <ta e="T722" id="Seg_10192" s="T721">этот-COM</ta>
            <ta e="T723" id="Seg_10193" s="T722">пойти-PST-1SG</ta>
            <ta e="T724" id="Seg_10194" s="T723">Пермяково-LAT</ta>
            <ta e="T725" id="Seg_10195" s="T724">водка.[NOM.SG]</ta>
            <ta e="T726" id="Seg_10196" s="T725">взять-PST-1SG</ta>
            <ta e="T727" id="Seg_10197" s="T726">принести-PST-1PL</ta>
            <ta e="T730" id="Seg_10198" s="T729">стол-NOM/GEN/ACC.3SG</ta>
            <ta e="T731" id="Seg_10199" s="T730">собирать-PST-1PL</ta>
            <ta e="T732" id="Seg_10200" s="T731">люди.[NOM.SG]</ta>
            <ta e="T733" id="Seg_10201" s="T732">собирать-PST-1PL</ta>
            <ta e="T734" id="Seg_10202" s="T733">водка.[NOM.SG]</ta>
            <ta e="T735" id="Seg_10203" s="T734">пить-PST-3PL</ta>
            <ta e="T736" id="Seg_10204" s="T735">съесть-PST-3PL</ta>
            <ta e="T737" id="Seg_10205" s="T736">тогда</ta>
            <ta e="T738" id="Seg_10206" s="T737">мы.NOM</ta>
            <ta e="T740" id="Seg_10207" s="T739">пойти-PST-1PL</ta>
            <ta e="T741" id="Seg_10208" s="T740">станица-LAT</ta>
            <ta e="T742" id="Seg_10209" s="T741">тогда</ta>
            <ta e="T743" id="Seg_10210" s="T742">%%-PST-1SG</ta>
            <ta e="T744" id="Seg_10211" s="T743">три.[NOM.SG]</ta>
            <ta e="T745" id="Seg_10212" s="T744">зима.[NOM.SG]</ta>
            <ta e="T746" id="Seg_10213" s="T745">тогда</ta>
            <ta e="T747" id="Seg_10214" s="T746">война.[NOM.SG]</ta>
            <ta e="T748" id="Seg_10215" s="T747">быть-PST.[3SG]</ta>
            <ta e="T749" id="Seg_10216" s="T748">этот-ACC</ta>
            <ta e="T750" id="Seg_10217" s="T749">взять-PST-3PL</ta>
            <ta e="T751" id="Seg_10218" s="T750">война-LAT</ta>
            <ta e="T752" id="Seg_10219" s="T751">тогда</ta>
            <ta e="T753" id="Seg_10220" s="T752">я.NOM</ta>
            <ta e="T754" id="Seg_10221" s="T753">беременная.[NOM.SG]</ta>
            <ta e="T755" id="Seg_10222" s="T754">быть-PST-1SG</ta>
            <ta e="T756" id="Seg_10223" s="T755">девушка.[NOM.SG]</ta>
            <ta e="T757" id="Seg_10224" s="T756">этот.[NOM.SG]</ta>
            <ta e="T758" id="Seg_10225" s="T757">тогда</ta>
            <ta e="T759" id="Seg_10226" s="T758">этот.[NOM.SG]</ta>
            <ta e="T760" id="Seg_10227" s="T759">прийти-PST.[3SG]</ta>
            <ta e="T761" id="Seg_10228" s="T760">война-ABL</ta>
            <ta e="T763" id="Seg_10229" s="T762">быть-PST.[3SG]</ta>
            <ta e="T764" id="Seg_10230" s="T763">тогда</ta>
            <ta e="T765" id="Seg_10231" s="T764">еще</ta>
            <ta e="T766" id="Seg_10232" s="T765">жить-PST-1PL</ta>
            <ta e="T767" id="Seg_10233" s="T766">я.NOM</ta>
            <ta e="T768" id="Seg_10234" s="T767">опять</ta>
            <ta e="T769" id="Seg_10235" s="T768">беременная.[NOM.SG]</ta>
            <ta e="T770" id="Seg_10236" s="T769">быть-PST.[3SG]</ta>
            <ta e="T771" id="Seg_10237" s="T770">тогда</ta>
            <ta e="T772" id="Seg_10238" s="T771">этот.[NOM.SG]</ta>
            <ta e="T774" id="Seg_10239" s="T773">этот.[NOM.SG]</ta>
            <ta e="T775" id="Seg_10240" s="T774">я.ACC</ta>
            <ta e="T776" id="Seg_10241" s="T775">гнать-PST.[3SG]</ta>
            <ta e="T777" id="Seg_10242" s="T776">и</ta>
            <ta e="T778" id="Seg_10243" s="T777">этот-ACC</ta>
            <ta e="T779" id="Seg_10244" s="T778">взять-PST.[3SG]</ta>
            <ta e="T780" id="Seg_10245" s="T779">еще</ta>
            <ta e="T781" id="Seg_10246" s="T780">один.[NOM.SG]</ta>
            <ta e="T783" id="Seg_10247" s="T782">мальчик.[NOM.SG]</ta>
            <ta e="T784" id="Seg_10248" s="T783">принести-PST-1SG</ta>
            <ta e="T785" id="Seg_10249" s="T784">тогда</ta>
            <ta e="T786" id="Seg_10250" s="T785">дом-LAT/LOC.1SG</ta>
            <ta e="T787" id="Seg_10251" s="T786">прийти-PST-1SG</ta>
            <ta e="T790" id="Seg_10252" s="T789">отец-LAT</ta>
            <ta e="T791" id="Seg_10253" s="T790">и</ta>
            <ta e="T792" id="Seg_10254" s="T791">мать-LAT</ta>
            <ta e="T793" id="Seg_10255" s="T792">тогда</ta>
            <ta e="T794" id="Seg_10256" s="T793">умереть-RES-PST-3PL</ta>
            <ta e="T795" id="Seg_10257" s="T794">девушка.[NOM.SG]</ta>
            <ta e="T796" id="Seg_10258" s="T795">и</ta>
            <ta e="T797" id="Seg_10259" s="T796">мальчик.[NOM.SG]</ta>
            <ta e="T798" id="Seg_10260" s="T797">я.NOM</ta>
            <ta e="T799" id="Seg_10261" s="T798">один</ta>
            <ta e="T801" id="Seg_10262" s="T800">стать-RES-PST-1SG</ta>
            <ta e="T802" id="Seg_10263" s="T801">%%</ta>
            <ta e="T803" id="Seg_10264" s="T802">жить-PST-1SG</ta>
            <ta e="T804" id="Seg_10265" s="T803">опять</ta>
            <ta e="T805" id="Seg_10266" s="T804">мужчина.[NOM.SG]</ta>
            <ta e="T806" id="Seg_10267" s="T805">я.ACC</ta>
            <ta e="T807" id="Seg_10268" s="T806">жениться-PST.[3SG]</ta>
            <ta e="T808" id="Seg_10269" s="T807">я.NOM</ta>
            <ta e="T809" id="Seg_10270" s="T808">пойти-PST-1SG</ta>
            <ta e="T810" id="Seg_10271" s="T809">этот-COM</ta>
            <ta e="T811" id="Seg_10272" s="T810">сидеть-PST-1SG</ta>
            <ta e="T812" id="Seg_10273" s="T811">один.[NOM.SG]</ta>
            <ta e="T813" id="Seg_10274" s="T812">два.[NOM.SG]</ta>
            <ta e="T814" id="Seg_10275" s="T813">три.[NOM.SG]</ta>
            <ta e="T815" id="Seg_10276" s="T814">четыре.[NOM.SG]</ta>
            <ta e="T816" id="Seg_10277" s="T815">пять.[NOM.SG]</ta>
            <ta e="T817" id="Seg_10278" s="T816">шесть.[NOM.SG]</ta>
            <ta e="T818" id="Seg_10279" s="T817">семь.[NOM.SG]</ta>
            <ta e="T819" id="Seg_10280" s="T818">зима.[NOM.SG]</ta>
            <ta e="T820" id="Seg_10281" s="T819">жить-PST-1SG</ta>
            <ta e="T821" id="Seg_10282" s="T820">два.[NOM.SG]</ta>
            <ta e="T822" id="Seg_10283" s="T821">ребенок.[NOM.SG]</ta>
            <ta e="T823" id="Seg_10284" s="T822">быть-PST-3PL</ta>
            <ta e="T824" id="Seg_10285" s="T823">очень</ta>
            <ta e="T825" id="Seg_10286" s="T824">NEG</ta>
            <ta e="T826" id="Seg_10287" s="T825">хороший</ta>
            <ta e="T827" id="Seg_10288" s="T826">водка.[NOM.SG]</ta>
            <ta e="T828" id="Seg_10289" s="T827">пить-PST.[3SG]</ta>
            <ta e="T829" id="Seg_10290" s="T828">бороться-DUR-PST.[3SG]</ta>
            <ta e="T830" id="Seg_10291" s="T829">ругать-DES-DUR-PST.[3SG]</ta>
            <ta e="T831" id="Seg_10292" s="T830">когда</ta>
            <ta e="T832" id="Seg_10293" s="T831">когда</ta>
            <ta e="T833" id="Seg_10294" s="T832">бросить-MOM-PST-1SG</ta>
            <ta e="T834" id="Seg_10295" s="T833">PTCL</ta>
            <ta e="T835" id="Seg_10296" s="T834">голова-NOM/GEN/ACC.1SG</ta>
            <ta e="T836" id="Seg_10297" s="T835">PTCL</ta>
            <ta e="T837" id="Seg_10298" s="T836">волосы-NOM/GEN/ACC.1SG</ta>
            <ta e="T838" id="Seg_10299" s="T837">от-тянуть-PST.[3SG]</ta>
            <ta e="T839" id="Seg_10300" s="T838">%%</ta>
            <ta e="T840" id="Seg_10301" s="T839">сильно</ta>
            <ta e="T841" id="Seg_10302" s="T840">бить-PST.[3SG]</ta>
            <ta e="T842" id="Seg_10303" s="T841">даже</ta>
            <ta e="T843" id="Seg_10304" s="T842">кровь.[NOM.SG]</ta>
            <ta e="T844" id="Seg_10305" s="T843">быть-PST.[3SG]</ta>
            <ta e="T845" id="Seg_10306" s="T844">тогда</ta>
            <ta e="T846" id="Seg_10307" s="T845">я.NOM</ta>
            <ta e="T847" id="Seg_10308" s="T846">мальчик-NOM/GEN/ACC.1SG</ta>
            <ta e="T848" id="Seg_10309" s="T847">этот-ACC</ta>
            <ta e="T849" id="Seg_10310" s="T848">выбросить-MOM-PST-1SG</ta>
            <ta e="T850" id="Seg_10311" s="T849">я.NOM</ta>
            <ta e="T851" id="Seg_10312" s="T850">один.[NOM.SG]</ta>
            <ta e="T852" id="Seg_10313" s="T851">мальчик-NOM/GEN/ACC.1SG</ta>
            <ta e="T853" id="Seg_10314" s="T852">быть-PST.[3SG]</ta>
            <ta e="T854" id="Seg_10315" s="T853">сейчас</ta>
            <ta e="T855" id="Seg_10316" s="T854">сказать-IPFVZ-1SG</ta>
            <ta e="T856" id="Seg_10317" s="T855">кто-LAT=INDEF</ta>
            <ta e="T858" id="Seg_10318" s="T856">NEG</ta>
            <ta e="T859" id="Seg_10319" s="T858">мужчина-LAT</ta>
            <ta e="T860" id="Seg_10320" s="T859">один</ta>
            <ta e="T861" id="Seg_10321" s="T860">жить-FUT-1SG</ta>
            <ta e="T862" id="Seg_10322" s="T861">сын-COM</ta>
            <ta e="T863" id="Seg_10323" s="T862">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T864" id="Seg_10324" s="T863">сын-COM</ta>
            <ta e="T865" id="Seg_10325" s="T864">тогда</ta>
            <ta e="T866" id="Seg_10326" s="T865">один.[NOM.SG]</ta>
            <ta e="T867" id="Seg_10327" s="T866">мальчик.[NOM.SG]</ta>
            <ta e="T868" id="Seg_10328" s="T867">INCH</ta>
            <ta e="T869" id="Seg_10329" s="T868">я.ACC</ta>
            <ta e="T870" id="Seg_10330" s="T869">жениться-INF.LAT</ta>
            <ta e="T871" id="Seg_10331" s="T870">я.NOM</ta>
            <ta e="T875" id="Seg_10332" s="T874">NEG</ta>
            <ta e="T876" id="Seg_10333" s="T875">%%-PRS-1SG</ta>
            <ta e="T877" id="Seg_10334" s="T876">лицо-NOM/GEN/ACC.1SG</ta>
            <ta e="T878" id="Seg_10335" s="T877">лицо-NOM/GEN/ACC.3SG</ta>
            <ta e="T879" id="Seg_10336" s="T878">толстый.[NOM.SG]</ta>
            <ta e="T880" id="Seg_10337" s="T879">NEG</ta>
            <ta e="T881" id="Seg_10338" s="T880">пойти-FUT-1SG</ta>
            <ta e="T882" id="Seg_10339" s="T881">этот</ta>
            <ta e="T883" id="Seg_10340" s="T882">сказать-IPFVZ.[3SG]</ta>
            <ta e="T884" id="Seg_10341" s="T883">HORT</ta>
            <ta e="T885" id="Seg_10342" s="T884">жить-INF.LAT</ta>
            <ta e="T886" id="Seg_10343" s="T885">ты-COM</ta>
            <ta e="T1055" id="Seg_10344" s="T886">ну</ta>
            <ta e="T887" id="Seg_10345" s="T1055">думать-PST-1SG</ta>
            <ta e="T888" id="Seg_10346" s="T887">думать-PST-1SG</ta>
            <ta e="T889" id="Seg_10347" s="T888">тогда</ta>
            <ta e="T890" id="Seg_10348" s="T889">пойти-PST-1SG</ta>
            <ta e="T891" id="Seg_10349" s="T890">тогда</ta>
            <ta e="T892" id="Seg_10350" s="T891">опять</ta>
            <ta e="T894" id="Seg_10351" s="T893">выбросить-FUT-1SG</ta>
            <ta e="T895" id="Seg_10352" s="T894">сказать-IPFVZ-1SG</ta>
            <ta e="T896" id="Seg_10353" s="T895">ты.DAT</ta>
            <ta e="T897" id="Seg_10354" s="T896">NEG</ta>
            <ta e="T898" id="Seg_10355" s="T897">жить-FUT-1SG</ta>
            <ta e="T899" id="Seg_10356" s="T898">ты.NOM-COM</ta>
            <ta e="T900" id="Seg_10357" s="T899">там</ta>
            <ta e="T901" id="Seg_10358" s="T900">INCH</ta>
            <ta e="T902" id="Seg_10359" s="T901">плакать-INF.LAT</ta>
            <ta e="T903" id="Seg_10360" s="T902">сильно</ta>
            <ta e="T904" id="Seg_10361" s="T903">плакать-PST.[3SG]</ta>
            <ta e="T905" id="Seg_10362" s="T904">очень</ta>
            <ta e="T906" id="Seg_10363" s="T905">тогда</ta>
            <ta e="T907" id="Seg_10364" s="T906">я.NOM</ta>
            <ta e="T908" id="Seg_10365" s="T907">сказать-PRS-1SG</ta>
            <ta e="T910" id="Seg_10366" s="T909">дом.[NOM.SG]</ta>
            <ta e="T911" id="Seg_10367" s="T910">поставить-IMP.3SG.O-3SG</ta>
            <ta e="T912" id="Seg_10368" s="T911">тогда</ta>
            <ta e="T913" id="Seg_10369" s="T912">JUSS</ta>
            <ta e="T914" id="Seg_10370" s="T913">пойти-FUT-3SG</ta>
            <ta e="T915" id="Seg_10371" s="T914">и</ta>
            <ta e="T916" id="Seg_10372" s="T915">так</ta>
            <ta e="T917" id="Seg_10373" s="T916">быть-PST.[3SG]</ta>
            <ta e="T918" id="Seg_10374" s="T917">дом.[NOM.SG]</ta>
            <ta e="T920" id="Seg_10375" s="T919">поставить-PST.[3SG]</ta>
            <ta e="T921" id="Seg_10376" s="T920">и</ta>
            <ta e="T1058" id="Seg_10377" s="T921">пойти-CVB</ta>
            <ta e="T922" id="Seg_10378" s="T1058">исчезнуть-PST.[3SG]</ta>
            <ta e="T923" id="Seg_10379" s="T922">я.NOM</ta>
            <ta e="T924" id="Seg_10380" s="T923">один</ta>
            <ta e="T926" id="Seg_10381" s="T925">сейчас</ta>
            <ta e="T927" id="Seg_10382" s="T926">я.NOM</ta>
            <ta e="T928" id="Seg_10383" s="T927">всегда</ta>
            <ta e="T929" id="Seg_10384" s="T928">один</ta>
            <ta e="T930" id="Seg_10385" s="T929">жить-DUR-1SG</ta>
            <ta e="T931" id="Seg_10386" s="T930">жить-DUR-1SG</ta>
            <ta e="T932" id="Seg_10387" s="T931">PTCL</ta>
            <ta e="T933" id="Seg_10388" s="T932">два.[NOM.SG]</ta>
            <ta e="T934" id="Seg_10389" s="T933">десять.[NOM.SG]</ta>
            <ta e="T935" id="Seg_10390" s="T934">луна.[NOM.SG]</ta>
            <ta e="T936" id="Seg_10391" s="T935">жить-DUR-1SG</ta>
            <ta e="T937" id="Seg_10392" s="T936">когда</ta>
            <ta e="T938" id="Seg_10393" s="T937">этот.[NOM.SG]</ta>
            <ta e="T939" id="Seg_10394" s="T938">выбросить-MOM-PST.[3SG]</ta>
            <ta e="T940" id="Seg_10395" s="T939">тогда</ta>
            <ta e="T941" id="Seg_10396" s="T940">мы.NOM</ta>
            <ta e="T942" id="Seg_10397" s="T941">этот.[NOM.SG]</ta>
            <ta e="T943" id="Seg_10398" s="T942">сын-COM</ta>
            <ta e="T944" id="Seg_10399" s="T943">PTCL</ta>
            <ta e="T945" id="Seg_10400" s="T944">дерево.[NOM.SG]</ta>
            <ta e="T946" id="Seg_10401" s="T945">топор-INS</ta>
            <ta e="T947" id="Seg_10402" s="T946">резать-PST-1PL</ta>
            <ta e="T948" id="Seg_10403" s="T947">дом.[NOM.SG]</ta>
            <ta e="T950" id="Seg_10404" s="T949">поставить-INF.LAT</ta>
            <ta e="T952" id="Seg_10405" s="T951">носить-PST-1PL</ta>
            <ta e="T954" id="Seg_10406" s="T953">лошадь-INS</ta>
            <ta e="T955" id="Seg_10407" s="T954">и</ta>
            <ta e="T956" id="Seg_10408" s="T955">дом.[NOM.SG]</ta>
            <ta e="T957" id="Seg_10409" s="T956">поставить-PST-1PL</ta>
            <ta e="T958" id="Seg_10410" s="T957">мужчина-PL</ta>
            <ta e="T959" id="Seg_10411" s="T958">резать-PST-3PL</ta>
            <ta e="T960" id="Seg_10412" s="T959">топор-INS</ta>
            <ta e="T961" id="Seg_10413" s="T960">а</ta>
            <ta e="T962" id="Seg_10414" s="T961">я.NOM</ta>
            <ta e="T963" id="Seg_10415" s="T962">кожа-PL</ta>
            <ta e="T964" id="Seg_10416" s="T963">этот-LAT</ta>
            <ta e="T965" id="Seg_10417" s="T964">делать-PST-1SG</ta>
            <ta e="T966" id="Seg_10418" s="T965">тогда</ta>
            <ta e="T967" id="Seg_10419" s="T966">%%-PL</ta>
            <ta e="T969" id="Seg_10420" s="T968">носить-PST-3PL</ta>
            <ta e="T971" id="Seg_10421" s="T970">делать-INF.LAT</ta>
            <ta e="T972" id="Seg_10422" s="T971">и</ta>
            <ta e="T974" id="Seg_10423" s="T973">делать-INF.LAT</ta>
            <ta e="T975" id="Seg_10424" s="T974">а</ta>
            <ta e="T976" id="Seg_10425" s="T975">я.NOM</ta>
            <ta e="T977" id="Seg_10426" s="T976">%%</ta>
            <ta e="T978" id="Seg_10427" s="T977">большой.[NOM.SG]</ta>
            <ta e="T979" id="Seg_10428" s="T978">кожа-PL</ta>
            <ta e="T980" id="Seg_10429" s="T979">делать-PST-1SG</ta>
            <ta e="T981" id="Seg_10430" s="T980">и</ta>
            <ta e="T982" id="Seg_10431" s="T981">сапог-PL</ta>
            <ta e="T983" id="Seg_10432" s="T982">шить-PST-1SG</ta>
            <ta e="T984" id="Seg_10433" s="T983">а</ta>
            <ta e="T985" id="Seg_10434" s="T984">этот.[NOM.SG]</ta>
            <ta e="T986" id="Seg_10435" s="T985">тоже</ta>
            <ta e="T987" id="Seg_10436" s="T986">я.LAT</ta>
            <ta e="T988" id="Seg_10437" s="T987">помогать-PST.[3SG]</ta>
            <ta e="T989" id="Seg_10438" s="T988">кожа-NOM/GEN.3SG</ta>
            <ta e="T990" id="Seg_10439" s="T989">вешать-%%-PST.[3SG]</ta>
            <ta e="T991" id="Seg_10440" s="T990">и</ta>
            <ta e="T992" id="Seg_10441" s="T991">NEG</ta>
            <ta e="T993" id="Seg_10442" s="T992">знать-PST.[3SG]</ta>
            <ta e="T994" id="Seg_10443" s="T993">я.NOM</ta>
            <ta e="T995" id="Seg_10444" s="T994">этот-ACC</ta>
            <ta e="T996" id="Seg_10445" s="T995">научиться-TR-PST-1SG</ta>
            <ta e="T997" id="Seg_10446" s="T996">я.NOM</ta>
            <ta e="T998" id="Seg_10447" s="T997">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T1000" id="Seg_10448" s="T999">мужчина-LAT</ta>
            <ta e="T1001" id="Seg_10449" s="T1000">сапог-PL</ta>
            <ta e="T1002" id="Seg_10450" s="T1001">шить-PST-1SG</ta>
            <ta e="T1003" id="Seg_10451" s="T1002">и</ta>
            <ta e="T1004" id="Seg_10452" s="T1003">парка.[NOM.SG]</ta>
            <ta e="T1005" id="Seg_10453" s="T1004">шить-PST-1SG</ta>
            <ta e="T1006" id="Seg_10454" s="T1005">овца-GEN</ta>
            <ta e="T1007" id="Seg_10455" s="T1006">кожа-PL</ta>
            <ta e="T1008" id="Seg_10456" s="T1007">и</ta>
            <ta e="T1009" id="Seg_10457" s="T1008">шапка.[NOM.SG]</ta>
            <ta e="T1010" id="Seg_10458" s="T1009">шить-PST-1SG</ta>
            <ta e="T1011" id="Seg_10459" s="T1010">рубашка-NOM/GEN/ACC.3SG</ta>
            <ta e="T1012" id="Seg_10460" s="T1011">шить-PST-1SG</ta>
            <ta e="T1013" id="Seg_10461" s="T1012">штаны-PL</ta>
            <ta e="T1014" id="Seg_10462" s="T1013">шить-PST-1SG</ta>
            <ta e="T1015" id="Seg_10463" s="T1014">этот-LAT</ta>
            <ta e="T1016" id="Seg_10464" s="T1015">надеть-PST-1SG</ta>
            <ta e="T1017" id="Seg_10465" s="T1016">очень</ta>
            <ta e="T1056" id="Seg_10466" s="T1017">хороший</ta>
            <ta e="T1019" id="Seg_10467" s="T1018">надеть-PST-1SG</ta>
            <ta e="T1021" id="Seg_10468" s="T1020">люди.[NOM.SG]</ta>
            <ta e="T1022" id="Seg_10469" s="T1021">PTCL</ta>
            <ta e="T1023" id="Seg_10470" s="T1022">смотреть-FRQ-PST-3PL</ta>
            <ta e="T1024" id="Seg_10471" s="T1023">очень</ta>
            <ta e="T1025" id="Seg_10472" s="T1024">красивый.[NOM.SG]</ta>
            <ta e="T1026" id="Seg_10473" s="T1025">мужчина.[NOM.SG]</ta>
            <ta e="T1027" id="Seg_10474" s="T1026">стать-RES-PST.[3SG]</ta>
            <ta e="T1028" id="Seg_10475" s="T1027">тогда</ta>
            <ta e="T1029" id="Seg_10476" s="T1028">мы.NOM</ta>
            <ta e="T1030" id="Seg_10477" s="T1029">земля.[NOM.SG]</ta>
            <ta e="T1033" id="Seg_10478" s="T1032">пахать-DUR-PST-1PL</ta>
            <ta e="T1034" id="Seg_10479" s="T1033">рожь.[NOM.SG]</ta>
            <ta e="T1035" id="Seg_10480" s="T1034">сеять-PST-1PL</ta>
            <ta e="T1037" id="Seg_10481" s="T1036">расти-PST.[3SG]</ta>
            <ta e="T1038" id="Seg_10482" s="T1037">очень</ta>
            <ta e="T1039" id="Seg_10483" s="T1038">хороший</ta>
            <ta e="T1040" id="Seg_10484" s="T1039">быть-PST.[3SG]</ta>
            <ta e="T1041" id="Seg_10485" s="T1040">рожь.[NOM.SG]</ta>
            <ta e="T1042" id="Seg_10486" s="T1041">тогда</ta>
            <ta e="T1043" id="Seg_10487" s="T1042">мука.[NOM.SG]</ta>
            <ta e="T1044" id="Seg_10488" s="T1043">делать-PST-1PL</ta>
            <ta e="T1045" id="Seg_10489" s="T1044">мельница-NOM/GEN/ACC.3SG</ta>
            <ta e="T1046" id="Seg_10490" s="T1045">идти-PST-1PL</ta>
            <ta e="T1047" id="Seg_10491" s="T1046">хлеб.[NOM.SG]</ta>
            <ta e="T1048" id="Seg_10492" s="T1047">печь-PST-1PL</ta>
            <ta e="T1049" id="Seg_10493" s="T1048">и</ta>
            <ta e="T1050" id="Seg_10494" s="T1049">мука.[NOM.SG]</ta>
            <ta e="T1051" id="Seg_10495" s="T1050">рожь.[NOM.SG]</ta>
            <ta e="T1052" id="Seg_10496" s="T1051">сеять-PST-1PL</ta>
            <ta e="T1053" id="Seg_10497" s="T1052">хороший.[NOM.SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_10498" s="T0">pers</ta>
            <ta e="T2" id="Seg_10499" s="T1">adv</ta>
            <ta e="T4" id="Seg_10500" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_10501" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_10502" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_10503" s="T6">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_10504" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_10505" s="T9">conj</ta>
            <ta e="T11" id="Seg_10506" s="T10">adv</ta>
            <ta e="T12" id="Seg_10507" s="T11">num-n:case</ta>
            <ta e="T13" id="Seg_10508" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_10509" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_10510" s="T14">v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_10511" s="T15">v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_10512" s="T16">num-n:case</ta>
            <ta e="T19" id="Seg_10513" s="T18">num-n:case</ta>
            <ta e="T20" id="Seg_10514" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_10515" s="T20">v-v&gt;v-v:pn</ta>
            <ta e="T22" id="Seg_10516" s="T21">conj</ta>
            <ta e="T23" id="Seg_10517" s="T22">v-v&gt;v-v:pn</ta>
            <ta e="T24" id="Seg_10518" s="T23">conj</ta>
            <ta e="T26" id="Seg_10519" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_10520" s="T26">v-v&gt;v-v:pn</ta>
            <ta e="T28" id="Seg_10521" s="T27">conj</ta>
            <ta e="T29" id="Seg_10522" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_10523" s="T29">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_10524" s="T30">conj</ta>
            <ta e="T32" id="Seg_10525" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_10526" s="T32">v-v&gt;v-v:pn</ta>
            <ta e="T34" id="Seg_10527" s="T33">conj</ta>
            <ta e="T35" id="Seg_10528" s="T34">n</ta>
            <ta e="T36" id="Seg_10529" s="T35">v-v&gt;v-v:pn</ta>
            <ta e="T37" id="Seg_10530" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_10531" s="T37">pers</ta>
            <ta e="T39" id="Seg_10532" s="T38">n-n:case.poss</ta>
            <ta e="T40" id="Seg_10533" s="T39">n-n:case</ta>
            <ta e="T41" id="Seg_10534" s="T40">v-v&gt;v-v:pn</ta>
            <ta e="T42" id="Seg_10535" s="T41">dempro-n:case</ta>
            <ta e="T43" id="Seg_10536" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_10537" s="T43">pers</ta>
            <ta e="T45" id="Seg_10538" s="T44">v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_10539" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_10540" s="T46">v-v:tense-v:pn</ta>
            <ta e="T48" id="Seg_10541" s="T47">conj</ta>
            <ta e="T49" id="Seg_10542" s="T48">n-n:case</ta>
            <ta e="T50" id="Seg_10543" s="T49">v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_10544" s="T50">pers</ta>
            <ta e="T52" id="Seg_10545" s="T51">v-v:tense-v:pn</ta>
            <ta e="T53" id="Seg_10546" s="T52">v-v:tense-v:pn</ta>
            <ta e="T54" id="Seg_10547" s="T53">que-n:case</ta>
            <ta e="T56" id="Seg_10548" s="T55">pers</ta>
            <ta e="T57" id="Seg_10549" s="T56">propr-n:case</ta>
            <ta e="T59" id="Seg_10550" s="T58">v-v&gt;v-v:n.fin</ta>
            <ta e="T60" id="Seg_10551" s="T59">pers</ta>
            <ta e="T61" id="Seg_10552" s="T60">dempro-n:case</ta>
            <ta e="T62" id="Seg_10553" s="T61">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_10554" s="T62">v-v:tense-v:pn</ta>
            <ta e="T64" id="Seg_10555" s="T63">propr-n:case</ta>
            <ta e="T65" id="Seg_10556" s="T64">v-v:ins-v:mood.pn</ta>
            <ta e="T66" id="Seg_10557" s="T65">adv</ta>
            <ta e="T67" id="Seg_10558" s="T66">adv</ta>
            <ta e="T68" id="Seg_10559" s="T67">adv</ta>
            <ta e="T69" id="Seg_10560" s="T68">v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_10561" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_10562" s="T70">n-n:case</ta>
            <ta e="T72" id="Seg_10563" s="T71">v-v:tense-v:pn</ta>
            <ta e="T73" id="Seg_10564" s="T72">dempro-n:case</ta>
            <ta e="T74" id="Seg_10565" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_10566" s="T74">v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_10567" s="T75">conj</ta>
            <ta e="T77" id="Seg_10568" s="T76">n-n:case</ta>
            <ta e="T78" id="Seg_10569" s="T77">v-v:tense-v:pn</ta>
            <ta e="T79" id="Seg_10570" s="T78">n-n:case</ta>
            <ta e="T80" id="Seg_10571" s="T79">v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_10572" s="T80">refl-n:case.poss</ta>
            <ta e="T82" id="Seg_10573" s="T81">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_10574" s="T82">n-n:case</ta>
            <ta e="T84" id="Seg_10575" s="T83">pers</ta>
            <ta e="T85" id="Seg_10576" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_10577" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_10578" s="T86">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T89" id="Seg_10579" s="T88">%%</ta>
            <ta e="T90" id="Seg_10580" s="T89">v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_10581" s="T90">n-n:case</ta>
            <ta e="T92" id="Seg_10582" s="T91">conj</ta>
            <ta e="T93" id="Seg_10583" s="T92">dempro-n:case</ta>
            <ta e="T94" id="Seg_10584" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_10585" s="T94">v-v:tense-v:pn</ta>
            <ta e="T96" id="Seg_10586" s="T95">dempro-n:case</ta>
            <ta e="T97" id="Seg_10587" s="T96">n-n:case</ta>
            <ta e="T98" id="Seg_10588" s="T97">conj</ta>
            <ta e="T99" id="Seg_10589" s="T98">dempro-n:case</ta>
            <ta e="T100" id="Seg_10590" s="T99">v-v:tense-v:pn</ta>
            <ta e="T101" id="Seg_10591" s="T100">pers</ta>
            <ta e="T102" id="Seg_10592" s="T101">dempro-n:case</ta>
            <ta e="T103" id="Seg_10593" s="T102">v-v:tense-v:pn</ta>
            <ta e="T104" id="Seg_10594" s="T103">conj</ta>
            <ta e="T105" id="Seg_10595" s="T104">que-n:case</ta>
            <ta e="T106" id="Seg_10596" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_10597" s="T106">v-v:tense-v:pn</ta>
            <ta e="T109" id="Seg_10598" s="T108">adv</ta>
            <ta e="T110" id="Seg_10599" s="T109">pers</ta>
            <ta e="T111" id="Seg_10600" s="T110">v-v:tense-v:pn</ta>
            <ta e="T113" id="Seg_10601" s="T112">pers</ta>
            <ta e="T115" id="Seg_10602" s="T114">n-n:case.poss</ta>
            <ta e="T116" id="Seg_10603" s="T115">v-v:mood.pn</ta>
            <ta e="T117" id="Seg_10604" s="T116">pers</ta>
            <ta e="T118" id="Seg_10605" s="T117">v-v:n.fin</ta>
            <ta e="T120" id="Seg_10606" s="T119">n-n:case</ta>
            <ta e="T122" id="Seg_10607" s="T121">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_10608" s="T122">%%</ta>
            <ta e="T124" id="Seg_10609" s="T123">v-v:tense-v:pn</ta>
            <ta e="T125" id="Seg_10610" s="T124">conj</ta>
            <ta e="T126" id="Seg_10611" s="T125">pers</ta>
            <ta e="T127" id="Seg_10612" s="T126">v-v:tense-v:pn</ta>
            <ta e="T128" id="Seg_10613" s="T127">v-v:tense-v:pn</ta>
            <ta e="T129" id="Seg_10614" s="T128">conj</ta>
            <ta e="T131" id="Seg_10615" s="T130">conj</ta>
            <ta e="T132" id="Seg_10616" s="T131">refl-n:case.poss</ta>
            <ta e="T133" id="Seg_10617" s="T132">ptcl</ta>
            <ta e="T134" id="Seg_10618" s="T133">v-v:tense-v:pn</ta>
            <ta e="T135" id="Seg_10619" s="T134">n-n:case.poss</ta>
            <ta e="T136" id="Seg_10620" s="T135">v-v:tense-v:pn</ta>
            <ta e="T137" id="Seg_10621" s="T136">v-v:tense-v:pn</ta>
            <ta e="T138" id="Seg_10622" s="T137">dempro-n:case</ta>
            <ta e="T139" id="Seg_10623" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_10624" s="T139">v-v:tense-v:pn</ta>
            <ta e="T141" id="Seg_10625" s="T140">conj</ta>
            <ta e="T142" id="Seg_10626" s="T141">pers</ta>
            <ta e="T143" id="Seg_10627" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_10628" s="T143">v-v:tense-v:pn</ta>
            <ta e="T145" id="Seg_10629" s="T144">adv</ta>
            <ta e="T146" id="Seg_10630" s="T145">adv</ta>
            <ta e="T147" id="Seg_10631" s="T146">n-n&gt;adj-n:case</ta>
            <ta e="T148" id="Seg_10632" s="T147">conj</ta>
            <ta e="T149" id="Seg_10633" s="T148">pers</ta>
            <ta e="T150" id="Seg_10634" s="T149">n-n:case</ta>
            <ta e="T151" id="Seg_10635" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_10636" s="T151">adv</ta>
            <ta e="T153" id="Seg_10637" s="T152">pers</ta>
            <ta e="T156" id="Seg_10638" s="T155">dempro-n:case</ta>
            <ta e="T157" id="Seg_10639" s="T156">v-v:n.fin</ta>
            <ta e="T158" id="Seg_10640" s="T157">conj</ta>
            <ta e="T159" id="Seg_10641" s="T158">dempro-n:case</ta>
            <ta e="T160" id="Seg_10642" s="T159">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T161" id="Seg_10643" s="T160">ptcl</ta>
            <ta e="T162" id="Seg_10644" s="T161">v-v&gt;v-v:pn</ta>
            <ta e="T163" id="Seg_10645" s="T162">n-n:case.poss</ta>
            <ta e="T164" id="Seg_10646" s="T163">adv</ta>
            <ta e="T165" id="Seg_10647" s="T164">%%</ta>
            <ta e="T166" id="Seg_10648" s="T165">v-v:tense-v:pn</ta>
            <ta e="T167" id="Seg_10649" s="T166">adv</ta>
            <ta e="T168" id="Seg_10650" s="T167">n-n:case.poss</ta>
            <ta e="T169" id="Seg_10651" s="T168">ptcl</ta>
            <ta e="T170" id="Seg_10652" s="T169">v-v:tense</ta>
            <ta e="T171" id="Seg_10653" s="T170">pers</ta>
            <ta e="T172" id="Seg_10654" s="T171">v-v&gt;v-v:pn</ta>
            <ta e="T173" id="Seg_10655" s="T172">v-v:mood.pn</ta>
            <ta e="T174" id="Seg_10656" s="T173">v-v&gt;v-v:pn</ta>
            <ta e="T175" id="Seg_10657" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_10658" s="T175">v-v&gt;v-v:pn</ta>
            <ta e="T177" id="Seg_10659" s="T176">adv</ta>
            <ta e="T178" id="Seg_10660" s="T177">adj-n:case</ta>
            <ta e="T179" id="Seg_10661" s="T178">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T180" id="Seg_10662" s="T179">n-n:case</ta>
            <ta e="T181" id="Seg_10663" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_10664" s="T181">adj</ta>
            <ta e="T183" id="Seg_10665" s="T182">v-v:tense-v:pn</ta>
            <ta e="T184" id="Seg_10666" s="T183">n-n:case</ta>
            <ta e="T186" id="Seg_10667" s="T185">adv</ta>
            <ta e="T187" id="Seg_10668" s="T186">n-n:case</ta>
            <ta e="T188" id="Seg_10669" s="T187">v-v&gt;v-v:pn</ta>
            <ta e="T189" id="Seg_10670" s="T188">adv</ta>
            <ta e="T190" id="Seg_10671" s="T189">n-n:case</ta>
            <ta e="T191" id="Seg_10672" s="T190">v-v:tense-v:pn</ta>
            <ta e="T193" id="Seg_10673" s="T192">n-n:num</ta>
            <ta e="T194" id="Seg_10674" s="T193">v-v:tense-v:pn</ta>
            <ta e="T195" id="Seg_10675" s="T194">n-n:num</ta>
            <ta e="T196" id="Seg_10676" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_10677" s="T196">adj-n:case</ta>
            <ta e="T198" id="Seg_10678" s="T197">v-v:tense-v:pn</ta>
            <ta e="T199" id="Seg_10679" s="T198">adj-n:num</ta>
            <ta e="T200" id="Seg_10680" s="T199">v-v:tense-v:pn</ta>
            <ta e="T201" id="Seg_10681" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_10682" s="T201">adv</ta>
            <ta e="T203" id="Seg_10683" s="T202">n-n:num</ta>
            <ta e="T204" id="Seg_10684" s="T203">n-n:num</ta>
            <ta e="T205" id="Seg_10685" s="T204">n-n:case.poss</ta>
            <ta e="T206" id="Seg_10686" s="T205">v-v:tense-v:pn</ta>
            <ta e="T207" id="Seg_10687" s="T206">n-n:case</ta>
            <ta e="T208" id="Seg_10688" s="T207">v-v:n.fin</ta>
            <ta e="T209" id="Seg_10689" s="T208">adv</ta>
            <ta e="T210" id="Seg_10690" s="T209">n-n:case</ta>
            <ta e="T211" id="Seg_10691" s="T210">adj-n:case</ta>
            <ta e="T212" id="Seg_10692" s="T211">v-v:tense-v:pn</ta>
            <ta e="T213" id="Seg_10693" s="T212">adv</ta>
            <ta e="T214" id="Seg_10694" s="T213">dempro-n:case</ta>
            <ta e="T216" id="Seg_10695" s="T215">n-n:case</ta>
            <ta e="T217" id="Seg_10696" s="T216">v-v:n.fin</ta>
            <ta e="T218" id="Seg_10697" s="T217">ptcl</ta>
            <ta e="T219" id="Seg_10698" s="T218">adv</ta>
            <ta e="T220" id="Seg_10699" s="T219">n-n:case</ta>
            <ta e="T221" id="Seg_10700" s="T220">v-v:tense-v:pn</ta>
            <ta e="T222" id="Seg_10701" s="T221">conj</ta>
            <ta e="T223" id="Seg_10702" s="T222">v-v:n.fin</ta>
            <ta e="T224" id="Seg_10703" s="T223">ptcl</ta>
            <ta e="T225" id="Seg_10704" s="T224">n-n:case</ta>
            <ta e="T226" id="Seg_10705" s="T225">v-v:tense-v:pn</ta>
            <ta e="T227" id="Seg_10706" s="T226">adv</ta>
            <ta e="T228" id="Seg_10707" s="T227">adj</ta>
            <ta e="T229" id="Seg_10708" s="T228">n-n:case</ta>
            <ta e="T230" id="Seg_10709" s="T229">v-v:tense-v:pn</ta>
            <ta e="T231" id="Seg_10710" s="T230">adv</ta>
            <ta e="T233" id="Seg_10711" s="T232">adj-n:case</ta>
            <ta e="T234" id="Seg_10712" s="T233">pers</ta>
            <ta e="T235" id="Seg_10713" s="T234">%%</ta>
            <ta e="T236" id="Seg_10714" s="T235">v-v:tense-v:pn</ta>
            <ta e="T237" id="Seg_10715" s="T236">v-v:n.fin</ta>
            <ta e="T238" id="Seg_10716" s="T237">adv</ta>
            <ta e="T239" id="Seg_10717" s="T238">n</ta>
            <ta e="T240" id="Seg_10718" s="T239">v-v:tense-v:pn</ta>
            <ta e="T241" id="Seg_10719" s="T240">conj</ta>
            <ta e="T242" id="Seg_10720" s="T241">n-n:case</ta>
            <ta e="T243" id="Seg_10721" s="T242">n-n:case</ta>
            <ta e="T244" id="Seg_10722" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_10723" s="T244">v-v:tense-v:pn</ta>
            <ta e="T246" id="Seg_10724" s="T245">n-n:case</ta>
            <ta e="T247" id="Seg_10725" s="T246">v-v:tense-v:pn</ta>
            <ta e="T248" id="Seg_10726" s="T247">conj</ta>
            <ta e="T249" id="Seg_10727" s="T248">v-v:tense-v:pn</ta>
            <ta e="T250" id="Seg_10728" s="T249">v-v:tense-v:pn</ta>
            <ta e="T251" id="Seg_10729" s="T250">adv</ta>
            <ta e="T252" id="Seg_10730" s="T251">v-v:tense-v:pn</ta>
            <ta e="T253" id="Seg_10731" s="T252">adv</ta>
            <ta e="T254" id="Seg_10732" s="T253">v-v:tense-v:pn</ta>
            <ta e="T255" id="Seg_10733" s="T254">conj</ta>
            <ta e="T256" id="Seg_10734" s="T255">n-n:case</ta>
            <ta e="T257" id="Seg_10735" s="T256">v-v:tense-v:pn</ta>
            <ta e="T258" id="Seg_10736" s="T257">pers</ta>
            <ta e="T259" id="Seg_10737" s="T258">propr</ta>
            <ta e="T260" id="Seg_10738" s="T259">n-n:case</ta>
            <ta e="T261" id="Seg_10739" s="T260">ptcl</ta>
            <ta e="T262" id="Seg_10740" s="T261">v-v:n.fin</ta>
            <ta e="T263" id="Seg_10741" s="T262">pers</ta>
            <ta e="T264" id="Seg_10742" s="T263">n-n:case</ta>
            <ta e="T265" id="Seg_10743" s="T264">v-v&gt;v-v:pn</ta>
            <ta e="T266" id="Seg_10744" s="T265">n-n:ins-n:case</ta>
            <ta e="T267" id="Seg_10745" s="T266">v-v:n.fin</ta>
            <ta e="T268" id="Seg_10746" s="T267">ptcl</ta>
            <ta e="T269" id="Seg_10747" s="T268">conj</ta>
            <ta e="T270" id="Seg_10748" s="T269">n-n:case.poss</ta>
            <ta e="T271" id="Seg_10749" s="T270">v-v:n.fin</ta>
            <ta e="T272" id="Seg_10750" s="T271">pers</ta>
            <ta e="T273" id="Seg_10751" s="T272">dempro-n:num-n:case</ta>
            <ta e="T274" id="Seg_10752" s="T273">v-v:tense-v:pn</ta>
            <ta e="T275" id="Seg_10753" s="T274">pers</ta>
            <ta e="T276" id="Seg_10754" s="T275">quant</ta>
            <ta e="T277" id="Seg_10755" s="T276">v-v:tense-v:pn</ta>
            <ta e="T279" id="Seg_10756" s="T278">adv</ta>
            <ta e="T281" id="Seg_10757" s="T280">v-v&gt;v-v:pn</ta>
            <ta e="T282" id="Seg_10758" s="T281">adv</ta>
            <ta e="T283" id="Seg_10759" s="T282">pers</ta>
            <ta e="T284" id="Seg_10760" s="T283">n-n&gt;adj-n:case</ta>
            <ta e="T285" id="Seg_10761" s="T284">n-n:case</ta>
            <ta e="T286" id="Seg_10762" s="T285">adv</ta>
            <ta e="T287" id="Seg_10763" s="T286">pers</ta>
            <ta e="T288" id="Seg_10764" s="T287">pers</ta>
            <ta e="T289" id="Seg_10765" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_10766" s="T289">v-v:n.fin</ta>
            <ta e="T291" id="Seg_10767" s="T290">adv</ta>
            <ta e="T292" id="Seg_10768" s="T291">pers</ta>
            <ta e="T293" id="Seg_10769" s="T292">v-v:ins-v:mood.pn</ta>
            <ta e="T294" id="Seg_10770" s="T293">conj</ta>
            <ta e="T295" id="Seg_10771" s="T294">pers</ta>
            <ta e="T296" id="Seg_10772" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_10773" s="T296">v-v:tense-v:pn</ta>
            <ta e="T299" id="Seg_10774" s="T298">pers</ta>
            <ta e="T300" id="Seg_10775" s="T299">ptcl</ta>
            <ta e="T301" id="Seg_10776" s="T300">v-v:tense-v:pn</ta>
            <ta e="T302" id="Seg_10777" s="T301">que-n:case</ta>
            <ta e="T303" id="Seg_10778" s="T302">v-v:n.fin</ta>
            <ta e="T304" id="Seg_10779" s="T303">pers</ta>
            <ta e="T305" id="Seg_10780" s="T304">v-v:ins-v:mood.pn</ta>
            <ta e="T306" id="Seg_10781" s="T305">conj</ta>
            <ta e="T308" id="Seg_10782" s="T307">conj</ta>
            <ta e="T309" id="Seg_10783" s="T308">pers</ta>
            <ta e="T310" id="Seg_10784" s="T309">que-n:case</ta>
            <ta e="T311" id="Seg_10785" s="T310">adv</ta>
            <ta e="T313" id="Seg_10786" s="T312">v-v:n.fin</ta>
            <ta e="T317" id="Seg_10787" s="T315">v-v:tense-v:pn=ptcl</ta>
            <ta e="T318" id="Seg_10788" s="T317">pers</ta>
            <ta e="T319" id="Seg_10789" s="T318">n-n:case.poss</ta>
            <ta e="T322" id="Seg_10790" s="T321">pers</ta>
            <ta e="T323" id="Seg_10791" s="T322">que-n:case=ptcl</ta>
            <ta e="T324" id="Seg_10792" s="T323">ptcl</ta>
            <ta e="T325" id="Seg_10793" s="T324">v-v:tense-v:pn</ta>
            <ta e="T326" id="Seg_10794" s="T325">v-v:mood.pn</ta>
            <ta e="T327" id="Seg_10795" s="T326">que=ptcl</ta>
            <ta e="T328" id="Seg_10796" s="T327">v-v:mood.pn</ta>
            <ta e="T329" id="Seg_10797" s="T328">que=ptcl</ta>
            <ta e="T330" id="Seg_10798" s="T329">n-n:case.poss</ta>
            <ta e="T331" id="Seg_10799" s="T330">adv-n:case</ta>
            <ta e="T1057" id="Seg_10800" s="T331">v-v:n-fin</ta>
            <ta e="T332" id="Seg_10801" s="T1057">v-v:tense-v:pn</ta>
            <ta e="T333" id="Seg_10802" s="T332">conj</ta>
            <ta e="T334" id="Seg_10803" s="T333">pers</ta>
            <ta e="T335" id="Seg_10804" s="T334">adv</ta>
            <ta e="T336" id="Seg_10805" s="T335">adj</ta>
            <ta e="T337" id="Seg_10806" s="T336">que-n:case=ptcl</ta>
            <ta e="T338" id="Seg_10807" s="T337">ptcl</ta>
            <ta e="T339" id="Seg_10808" s="T338">v-v:tense-v:pn</ta>
            <ta e="T340" id="Seg_10809" s="T339">pers</ta>
            <ta e="T341" id="Seg_10810" s="T340">%%</ta>
            <ta e="T342" id="Seg_10811" s="T341">v-v:tense-v:pn</ta>
            <ta e="T343" id="Seg_10812" s="T342">%%</ta>
            <ta e="T344" id="Seg_10813" s="T343">v-v&gt;v-v:pn</ta>
            <ta e="T345" id="Seg_10814" s="T344">v-v&gt;v-v:pn</ta>
            <ta e="T346" id="Seg_10815" s="T345">ptcl</ta>
            <ta e="T347" id="Seg_10816" s="T346">conj</ta>
            <ta e="T348" id="Seg_10817" s="T347">n-n:case.poss</ta>
            <ta e="T349" id="Seg_10818" s="T348">ptcl</ta>
            <ta e="T350" id="Seg_10819" s="T349">v-v:tense-v:pn</ta>
            <ta e="T351" id="Seg_10820" s="T350">n-n:case.poss</ta>
            <ta e="T352" id="Seg_10821" s="T351">n-n:case.poss</ta>
            <ta e="T353" id="Seg_10822" s="T352">dempro-n:case</ta>
            <ta e="T354" id="Seg_10823" s="T353">v-v:tense-v:pn</ta>
            <ta e="T355" id="Seg_10824" s="T354">conj</ta>
            <ta e="T356" id="Seg_10825" s="T355">pers</ta>
            <ta e="T358" id="Seg_10826" s="T357">conj</ta>
            <ta e="T359" id="Seg_10827" s="T358">pers</ta>
            <ta e="T360" id="Seg_10828" s="T359">v-v:tense-v:pn</ta>
            <ta e="T361" id="Seg_10829" s="T360">pers</ta>
            <ta e="T362" id="Seg_10830" s="T361">n-n:case</ta>
            <ta e="T363" id="Seg_10831" s="T362">adv</ta>
            <ta e="T364" id="Seg_10832" s="T363">n-n:case</ta>
            <ta e="T365" id="Seg_10833" s="T364">ptcl</ta>
            <ta e="T366" id="Seg_10834" s="T365">n-n:case</ta>
            <ta e="T369" id="Seg_10835" s="T368">n-n:case</ta>
            <ta e="T371" id="Seg_10836" s="T370">v-v:tense-v:pn</ta>
            <ta e="T372" id="Seg_10837" s="T371">v-v:tense-v:pn</ta>
            <ta e="T373" id="Seg_10838" s="T372">ptcl</ta>
            <ta e="T374" id="Seg_10839" s="T373">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T375" id="Seg_10840" s="T374">ptcl</ta>
            <ta e="T376" id="Seg_10841" s="T375">propr-n:case</ta>
            <ta e="T377" id="Seg_10842" s="T376">adv</ta>
            <ta e="T378" id="Seg_10843" s="T377">v-v&gt;v-v:pn</ta>
            <ta e="T379" id="Seg_10844" s="T378">adv</ta>
            <ta e="T380" id="Seg_10845" s="T379">v-v&gt;v-v:pn</ta>
            <ta e="T381" id="Seg_10846" s="T380">conj</ta>
            <ta e="T382" id="Seg_10847" s="T381">dempro-n:case</ta>
            <ta e="T383" id="Seg_10848" s="T382">adv</ta>
            <ta e="T384" id="Seg_10849" s="T383">ptcl</ta>
            <ta e="T385" id="Seg_10850" s="T384">v-v:tense-v:pn</ta>
            <ta e="T386" id="Seg_10851" s="T385">que-n:case=ptcl</ta>
            <ta e="T387" id="Seg_10852" s="T386">ptcl</ta>
            <ta e="T388" id="Seg_10853" s="T387">v-v:tense-v:pn</ta>
            <ta e="T394" id="Seg_10854" s="T393">dempro-n:case</ta>
            <ta e="T395" id="Seg_10855" s="T394">ptcl</ta>
            <ta e="T396" id="Seg_10856" s="T395">n-n:case.poss</ta>
            <ta e="T397" id="Seg_10857" s="T396">n-n:case.poss</ta>
            <ta e="T398" id="Seg_10858" s="T397">v-v:tense-v:pn</ta>
            <ta e="T399" id="Seg_10859" s="T398">v-v:tense-v:pn</ta>
            <ta e="T400" id="Seg_10860" s="T399">v-v:ins-v:mood.pn</ta>
            <ta e="T401" id="Seg_10861" s="T400">n-n:case</ta>
            <ta e="T402" id="Seg_10862" s="T401">conj</ta>
            <ta e="T403" id="Seg_10863" s="T402">dempro-n:case</ta>
            <ta e="T404" id="Seg_10864" s="T403">que=ptcl</ta>
            <ta e="T405" id="Seg_10865" s="T404">ptcl</ta>
            <ta e="T406" id="Seg_10866" s="T405">v-v:tense-v:pn</ta>
            <ta e="T407" id="Seg_10867" s="T406">v-v:tense-v:pn</ta>
            <ta e="T408" id="Seg_10868" s="T407">pers</ta>
            <ta e="T409" id="Seg_10869" s="T408">v-v:n.fin</ta>
            <ta e="T410" id="Seg_10870" s="T409">ptcl</ta>
            <ta e="T411" id="Seg_10871" s="T410">adj-n:case</ta>
            <ta e="T412" id="Seg_10872" s="T411">n-n:case</ta>
            <ta e="T413" id="Seg_10873" s="T412">pers</ta>
            <ta e="T414" id="Seg_10874" s="T413">ptcl</ta>
            <ta e="T415" id="Seg_10875" s="T414">n</ta>
            <ta e="T416" id="Seg_10876" s="T415">v-v:n.fin</ta>
            <ta e="T417" id="Seg_10877" s="T416">n-n:case.poss</ta>
            <ta e="T418" id="Seg_10878" s="T417">v-v&gt;v-v:pn</ta>
            <ta e="T419" id="Seg_10879" s="T418">que-n:case</ta>
            <ta e="T420" id="Seg_10880" s="T419">pers</ta>
            <ta e="T421" id="Seg_10881" s="T420">ptcl</ta>
            <ta e="T422" id="Seg_10882" s="T421">v-v:tense-v:pn</ta>
            <ta e="T423" id="Seg_10883" s="T422">ptcl</ta>
            <ta e="T424" id="Seg_10884" s="T423">ptcl</ta>
            <ta e="T425" id="Seg_10885" s="T424">aux-v:mood.pn</ta>
            <ta e="T426" id="Seg_10886" s="T425">v-v:ins-v:mood.pn</ta>
            <ta e="T427" id="Seg_10887" s="T426">pers</ta>
            <ta e="T428" id="Seg_10888" s="T427">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T429" id="Seg_10889" s="T428">adv</ta>
            <ta e="T430" id="Seg_10890" s="T429">pers</ta>
            <ta e="T433" id="Seg_10891" s="T432">n-n:case.poss</ta>
            <ta e="T434" id="Seg_10892" s="T433">v-v:tense-v:pn</ta>
            <ta e="T435" id="Seg_10893" s="T434">pers</ta>
            <ta e="T436" id="Seg_10894" s="T435">v-v:n.fin</ta>
            <ta e="T437" id="Seg_10895" s="T436">dempro-n:case</ta>
            <ta e="T439" id="Seg_10896" s="T438">n-n:case</ta>
            <ta e="T440" id="Seg_10897" s="T439">conj</ta>
            <ta e="T441" id="Seg_10898" s="T440">pers</ta>
            <ta e="T442" id="Seg_10899" s="T441">adv</ta>
            <ta e="T443" id="Seg_10900" s="T442">n-n:case</ta>
            <ta e="T444" id="Seg_10901" s="T443">conj</ta>
            <ta e="T445" id="Seg_10902" s="T444">pers</ta>
            <ta e="T446" id="Seg_10903" s="T445">adv</ta>
            <ta e="T447" id="Seg_10904" s="T446">n-n:case</ta>
            <ta e="T448" id="Seg_10905" s="T447">pers</ta>
            <ta e="T450" id="Seg_10906" s="T449">pers</ta>
            <ta e="T451" id="Seg_10907" s="T450">v-v&gt;v-v:pn</ta>
            <ta e="T452" id="Seg_10908" s="T451">v-v:ins-v:mood.pn</ta>
            <ta e="T453" id="Seg_10909" s="T452">n-n:case</ta>
            <ta e="T454" id="Seg_10910" s="T453">conj</ta>
            <ta e="T455" id="Seg_10911" s="T454">n-n:case</ta>
            <ta e="T456" id="Seg_10912" s="T455">v-v:mood.pn</ta>
            <ta e="T457" id="Seg_10913" s="T456">dempro-n:num</ta>
            <ta e="T458" id="Seg_10914" s="T457">v-v:tense-v:pn</ta>
            <ta e="T459" id="Seg_10915" s="T458">ptcl</ta>
            <ta e="T460" id="Seg_10916" s="T459">ptcl</ta>
            <ta e="T461" id="Seg_10917" s="T460">v-v:tense-v:pn</ta>
            <ta e="T462" id="Seg_10918" s="T461">adv</ta>
            <ta e="T463" id="Seg_10919" s="T462">pers</ta>
            <ta e="T464" id="Seg_10920" s="T463">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T465" id="Seg_10921" s="T464">conj</ta>
            <ta e="T467" id="Seg_10922" s="T466">v-v:tense-v:pn</ta>
            <ta e="T469" id="Seg_10923" s="T468">n-n:case.poss-n:case</ta>
            <ta e="T470" id="Seg_10924" s="T469">dempro-n:case</ta>
            <ta e="T471" id="Seg_10925" s="T470">adv</ta>
            <ta e="T472" id="Seg_10926" s="T471">n-n:case.poss</ta>
            <ta e="T473" id="Seg_10927" s="T472">v-v:tense-v:pn</ta>
            <ta e="T474" id="Seg_10928" s="T473">adv</ta>
            <ta e="T475" id="Seg_10929" s="T474">pers</ta>
            <ta e="T476" id="Seg_10930" s="T475">dempro-n:case</ta>
            <ta e="T478" id="Seg_10931" s="T477">adv</ta>
            <ta e="T479" id="Seg_10932" s="T478">v-v:tense-v:pn</ta>
            <ta e="T480" id="Seg_10933" s="T479">n-n:case</ta>
            <ta e="T481" id="Seg_10934" s="T480">v-v:n.fin</ta>
            <ta e="T482" id="Seg_10935" s="T481">adv</ta>
            <ta e="T483" id="Seg_10936" s="T482">v-v:tense-v:pn</ta>
            <ta e="T484" id="Seg_10937" s="T483">n-n:case</ta>
            <ta e="T485" id="Seg_10938" s="T484">conj</ta>
            <ta e="T486" id="Seg_10939" s="T485">adv</ta>
            <ta e="T487" id="Seg_10940" s="T486">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T488" id="Seg_10941" s="T487">adv</ta>
            <ta e="T489" id="Seg_10942" s="T488">pers</ta>
            <ta e="T490" id="Seg_10943" s="T489">n-n:case.poss</ta>
            <ta e="T491" id="Seg_10944" s="T490">v-v:tense-v:pn</ta>
            <ta e="T492" id="Seg_10945" s="T491">conj</ta>
            <ta e="T493" id="Seg_10946" s="T492">adv</ta>
            <ta e="T494" id="Seg_10947" s="T493">adv</ta>
            <ta e="T495" id="Seg_10948" s="T494">v-v:tense-v:pn</ta>
            <ta e="T496" id="Seg_10949" s="T495">n-n:case.poss</ta>
            <ta e="T497" id="Seg_10950" s="T496">v-v:tense</ta>
            <ta e="T498" id="Seg_10951" s="T497">v-v:tense-v:pn</ta>
            <ta e="T499" id="Seg_10952" s="T498">v-v:tense-v:pn</ta>
            <ta e="T500" id="Seg_10953" s="T499">dempro-n:case</ta>
            <ta e="T501" id="Seg_10954" s="T500">ptcl</ta>
            <ta e="T502" id="Seg_10955" s="T501">adv</ta>
            <ta e="T503" id="Seg_10956" s="T502">n-n:case.poss</ta>
            <ta e="T504" id="Seg_10957" s="T503">pers</ta>
            <ta e="T505" id="Seg_10958" s="T504">n-n:case</ta>
            <ta e="T506" id="Seg_10959" s="T505">v-v:tense-v:pn</ta>
            <ta e="T507" id="Seg_10960" s="T506">%%</ta>
            <ta e="T508" id="Seg_10961" s="T507">pers</ta>
            <ta e="T509" id="Seg_10962" s="T508">adv</ta>
            <ta e="T510" id="Seg_10963" s="T509">pers</ta>
            <ta e="T511" id="Seg_10964" s="T510">ptcl</ta>
            <ta e="T512" id="Seg_10965" s="T511">n-n:case</ta>
            <ta e="T514" id="Seg_10966" s="T513">v-v:n.fin</ta>
            <ta e="T515" id="Seg_10967" s="T514">adv</ta>
            <ta e="T516" id="Seg_10968" s="T515">pers</ta>
            <ta e="T517" id="Seg_10969" s="T516">v-v:tense-v:pn</ta>
            <ta e="T518" id="Seg_10970" s="T517">n-n:case.poss</ta>
            <ta e="T519" id="Seg_10971" s="T518">v-v:tense-v:pn</ta>
            <ta e="T520" id="Seg_10972" s="T519">adv</ta>
            <ta e="T521" id="Seg_10973" s="T520">v-v:tense-v:pn</ta>
            <ta e="T523" id="Seg_10974" s="T522">propr-n:case</ta>
            <ta e="T525" id="Seg_10975" s="T524">n-n:case</ta>
            <ta e="T526" id="Seg_10976" s="T525">v-v:tense-v:pn</ta>
            <ta e="T527" id="Seg_10977" s="T526">v-v:pn</ta>
            <ta e="T528" id="Seg_10978" s="T527">n-n:case.poss</ta>
            <ta e="T529" id="Seg_10979" s="T528">v-v:tense-v:pn</ta>
            <ta e="T530" id="Seg_10980" s="T529">n-n:case</ta>
            <ta e="T531" id="Seg_10981" s="T530">v-v:tense-v:pn</ta>
            <ta e="T532" id="Seg_10982" s="T531">adv</ta>
            <ta e="T533" id="Seg_10983" s="T532">adv</ta>
            <ta e="T534" id="Seg_10984" s="T533">n-n:case</ta>
            <ta e="T535" id="Seg_10985" s="T534">v-v:tense-v:pn</ta>
            <ta e="T536" id="Seg_10986" s="T535">propr-n:case</ta>
            <ta e="T537" id="Seg_10987" s="T536">n-n:case.poss</ta>
            <ta e="T538" id="Seg_10988" s="T537">adj-n:case</ta>
            <ta e="T540" id="Seg_10989" s="T539">conj</ta>
            <ta e="T541" id="Seg_10990" s="T540">adj-n:num</ta>
            <ta e="T542" id="Seg_10991" s="T541">refl-n:case.poss</ta>
            <ta e="T543" id="Seg_10992" s="T542">adj-n:case</ta>
            <ta e="T544" id="Seg_10993" s="T543">adv</ta>
            <ta e="T545" id="Seg_10994" s="T544">pers</ta>
            <ta e="T546" id="Seg_10995" s="T545">n-n:case.poss</ta>
            <ta e="T547" id="Seg_10996" s="T546">v-v:tense-v:pn</ta>
            <ta e="T548" id="Seg_10997" s="T547">conj</ta>
            <ta e="T549" id="Seg_10998" s="T548">v-v:tense-v:pn</ta>
            <ta e="T550" id="Seg_10999" s="T549">pers</ta>
            <ta e="T551" id="Seg_11000" s="T550">v-v:n.fin</ta>
            <ta e="T552" id="Seg_11001" s="T551">pers</ta>
            <ta e="T553" id="Seg_11002" s="T552">v-v&gt;v-v:pn</ta>
            <ta e="T554" id="Seg_11003" s="T553">pers</ta>
            <ta e="T555" id="Seg_11004" s="T554">n-n:case</ta>
            <ta e="T556" id="Seg_11005" s="T555">%%</ta>
            <ta e="T558" id="Seg_11006" s="T557">ptcl</ta>
            <ta e="T559" id="Seg_11007" s="T558">pers</ta>
            <ta e="T560" id="Seg_11008" s="T559">adj-n:case</ta>
            <ta e="T561" id="Seg_11009" s="T560">ptcl</ta>
            <ta e="T562" id="Seg_11010" s="T561">pers</ta>
            <ta e="T563" id="Seg_11011" s="T562">pers-n:case</ta>
            <ta e="T564" id="Seg_11012" s="T563">ptcl</ta>
            <ta e="T565" id="Seg_11013" s="T564">v-v:tense-v:pn</ta>
            <ta e="T566" id="Seg_11014" s="T565">conj</ta>
            <ta e="T567" id="Seg_11015" s="T566">n-n:case</ta>
            <ta e="T568" id="Seg_11016" s="T567">adv</ta>
            <ta e="T569" id="Seg_11017" s="T568">n-n:case.poss</ta>
            <ta e="T570" id="Seg_11018" s="T569">v-v&gt;v-v:pn</ta>
            <ta e="T571" id="Seg_11019" s="T570">v-v:ins-v:mood.pn</ta>
            <ta e="T572" id="Seg_11020" s="T571">dempro-n:case</ta>
            <ta e="T573" id="Seg_11021" s="T572">n-n:case</ta>
            <ta e="T574" id="Seg_11022" s="T573">quant</ta>
            <ta e="T575" id="Seg_11023" s="T574">adv</ta>
            <ta e="T577" id="Seg_11024" s="T576">adj</ta>
            <ta e="T578" id="Seg_11025" s="T577">n-n:case</ta>
            <ta e="T579" id="Seg_11026" s="T578">conj</ta>
            <ta e="T580" id="Seg_11027" s="T579">pers</ta>
            <ta e="T581" id="Seg_11028" s="T580">v-v:tense-v:pn</ta>
            <ta e="T582" id="Seg_11029" s="T581">ptcl</ta>
            <ta e="T583" id="Seg_11030" s="T582">conj</ta>
            <ta e="T584" id="Seg_11031" s="T583">v-v:ins-v:mood.pn</ta>
            <ta e="T585" id="Seg_11032" s="T584">conj</ta>
            <ta e="T586" id="Seg_11033" s="T585">v-v:mood.pn</ta>
            <ta e="T587" id="Seg_11034" s="T586">dempro-n:case</ta>
            <ta e="T588" id="Seg_11035" s="T587">num-n:case</ta>
            <ta e="T589" id="Seg_11036" s="T588">n-n:case</ta>
            <ta e="T590" id="Seg_11037" s="T589">v-v:tense-v:pn</ta>
            <ta e="T591" id="Seg_11038" s="T590">v-v:n.fin</ta>
            <ta e="T592" id="Seg_11039" s="T591">conj</ta>
            <ta e="T593" id="Seg_11040" s="T592">pers</ta>
            <ta e="T594" id="Seg_11041" s="T593">ptcl</ta>
            <ta e="T595" id="Seg_11042" s="T594">v-v:tense-v:pn</ta>
            <ta e="T596" id="Seg_11043" s="T595">conj</ta>
            <ta e="T597" id="Seg_11044" s="T596">n</ta>
            <ta e="T598" id="Seg_11045" s="T597">ptcl</ta>
            <ta e="T599" id="Seg_11046" s="T598">v-v:tense-v:pn</ta>
            <ta e="T600" id="Seg_11047" s="T599">dempro-n:case</ta>
            <ta e="T601" id="Seg_11048" s="T600">ptcl</ta>
            <ta e="T602" id="Seg_11049" s="T601">adj-n:case</ta>
            <ta e="T603" id="Seg_11050" s="T602">n-n:case</ta>
            <ta e="T604" id="Seg_11051" s="T603">n-n:case.poss</ta>
            <ta e="T605" id="Seg_11052" s="T604">ptcl</ta>
            <ta e="T607" id="Seg_11053" s="T606">adj-n:case</ta>
            <ta e="T609" id="Seg_11054" s="T608">n-n:case.poss</ta>
            <ta e="T610" id="Seg_11055" s="T609">adj-n:case</ta>
            <ta e="T611" id="Seg_11056" s="T610">n-n:case.poss</ta>
            <ta e="T612" id="Seg_11057" s="T611">ptcl</ta>
            <ta e="T613" id="Seg_11058" s="T612">v-v:tense-v:pn</ta>
            <ta e="T614" id="Seg_11059" s="T613">ptcl</ta>
            <ta e="T615" id="Seg_11060" s="T614">adj</ta>
            <ta e="T616" id="Seg_11061" s="T615">v-v:tense-v:pn</ta>
            <ta e="T617" id="Seg_11062" s="T616">n-n:num-n:case.poss</ta>
            <ta e="T618" id="Seg_11063" s="T617">ptcl</ta>
            <ta e="T619" id="Seg_11064" s="T618">v-v:tense-v:pn</ta>
            <ta e="T620" id="Seg_11065" s="T619">conj</ta>
            <ta e="T621" id="Seg_11066" s="T620">n-n:case.poss</ta>
            <ta e="T622" id="Seg_11067" s="T621">v-v&gt;v-v:pn</ta>
            <ta e="T623" id="Seg_11068" s="T622">v-v:ins-v:mood.pn</ta>
            <ta e="T624" id="Seg_11069" s="T623">dempro-n:case</ta>
            <ta e="T625" id="Seg_11070" s="T624">n-n:case</ta>
            <ta e="T626" id="Seg_11071" s="T625">v-v:tense-v:pn</ta>
            <ta e="T627" id="Seg_11072" s="T626">conj</ta>
            <ta e="T628" id="Seg_11073" s="T627">n-n:case</ta>
            <ta e="T629" id="Seg_11074" s="T628">v-v:tense-v:pn</ta>
            <ta e="T630" id="Seg_11075" s="T629">conj</ta>
            <ta e="T631" id="Seg_11076" s="T630">n-n:case</ta>
            <ta e="T632" id="Seg_11077" s="T631">quant</ta>
            <ta e="T633" id="Seg_11078" s="T632">conj</ta>
            <ta e="T634" id="Seg_11079" s="T633">pers</ta>
            <ta e="T635" id="Seg_11080" s="T634">pers</ta>
            <ta e="T636" id="Seg_11081" s="T635">v-v:tense-v:pn</ta>
            <ta e="T637" id="Seg_11082" s="T636">pers</ta>
            <ta e="T638" id="Seg_11083" s="T637">n-n:case</ta>
            <ta e="T639" id="Seg_11084" s="T638">ptcl</ta>
            <ta e="T640" id="Seg_11085" s="T639">v-v:n.fin</ta>
            <ta e="T641" id="Seg_11086" s="T640">conj</ta>
            <ta e="T642" id="Seg_11087" s="T641">n-n:case</ta>
            <ta e="T643" id="Seg_11088" s="T642">ptcl</ta>
            <ta e="T644" id="Seg_11089" s="T643">v-v:n.fin</ta>
            <ta e="T645" id="Seg_11090" s="T644">ptcl</ta>
            <ta e="T646" id="Seg_11091" s="T645">pers</ta>
            <ta e="T647" id="Seg_11092" s="T646">ptcl</ta>
            <ta e="T648" id="Seg_11093" s="T647">pers</ta>
            <ta e="T649" id="Seg_11094" s="T648">pers</ta>
            <ta e="T650" id="Seg_11095" s="T649">adv</ta>
            <ta e="T651" id="Seg_11096" s="T650">ptcl</ta>
            <ta e="T652" id="Seg_11097" s="T651">pers</ta>
            <ta e="T653" id="Seg_11098" s="T652">v-v:ins-v:mood.pn</ta>
            <ta e="T654" id="Seg_11099" s="T653">n-n:case</ta>
            <ta e="T655" id="Seg_11100" s="T654">v-v:tense-v:pn</ta>
            <ta e="T656" id="Seg_11101" s="T655">num-n:case</ta>
            <ta e="T657" id="Seg_11102" s="T656">n-n:case</ta>
            <ta e="T658" id="Seg_11103" s="T657">dempro-n:case</ta>
            <ta e="T659" id="Seg_11104" s="T658">n-n:case</ta>
            <ta e="T660" id="Seg_11105" s="T659">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T661" id="Seg_11106" s="T660">dempro-n:case</ta>
            <ta e="T662" id="Seg_11107" s="T661">ptcl</ta>
            <ta e="T663" id="Seg_11108" s="T662">pers</ta>
            <ta e="T664" id="Seg_11109" s="T663">v-v:n.fin</ta>
            <ta e="T665" id="Seg_11110" s="T664">n-n:case.poss</ta>
            <ta e="T666" id="Seg_11111" s="T665">v</ta>
            <ta e="T667" id="Seg_11112" s="T666">v-v:n.fin</ta>
            <ta e="T668" id="Seg_11113" s="T667">n-n:case</ta>
            <ta e="T669" id="Seg_11114" s="T668">dempro-n:case</ta>
            <ta e="T671" id="Seg_11115" s="T670">v-v:n.fin</ta>
            <ta e="T672" id="Seg_11116" s="T671">conj</ta>
            <ta e="T673" id="Seg_11117" s="T672">pers</ta>
            <ta e="T674" id="Seg_11118" s="T673">v-v:tense-v:pn</ta>
            <ta e="T675" id="Seg_11119" s="T674">v-v:n.fin</ta>
            <ta e="T676" id="Seg_11120" s="T675">conj</ta>
            <ta e="T677" id="Seg_11121" s="T676">ptcl</ta>
            <ta e="T678" id="Seg_11122" s="T677">v-v:n.fin</ta>
            <ta e="T680" id="Seg_11123" s="T679">v-v:ins-v:mood.pn</ta>
            <ta e="T682" id="Seg_11124" s="T681">aux-v:mood.pn</ta>
            <ta e="T683" id="Seg_11125" s="T682">v-v:ins-v:mood.pn</ta>
            <ta e="T684" id="Seg_11126" s="T683">pers</ta>
            <ta e="T685" id="Seg_11127" s="T684">adv</ta>
            <ta e="T687" id="Seg_11128" s="T686">refl-n:case.poss</ta>
            <ta e="T688" id="Seg_11129" s="T687">adj-n:case</ta>
            <ta e="T689" id="Seg_11130" s="T688">refl-n:case.poss</ta>
            <ta e="T690" id="Seg_11131" s="T689">v-v:pn</ta>
            <ta e="T691" id="Seg_11132" s="T690">que-n:case</ta>
            <ta e="T692" id="Seg_11133" s="T691">v-v:n.fin</ta>
            <ta e="T693" id="Seg_11134" s="T692">adv</ta>
            <ta e="T694" id="Seg_11135" s="T693">dempro-n:case</ta>
            <ta e="T695" id="Seg_11136" s="T694">n-n:case</ta>
            <ta e="T697" id="Seg_11137" s="T696">v-v:tense-v:pn</ta>
            <ta e="T698" id="Seg_11138" s="T697">pers</ta>
            <ta e="T699" id="Seg_11139" s="T698">v-v:tense-v:pn</ta>
            <ta e="T700" id="Seg_11140" s="T699">conj</ta>
            <ta e="T701" id="Seg_11141" s="T700">ptcl</ta>
            <ta e="T702" id="Seg_11142" s="T701">v-v:n.fin</ta>
            <ta e="T703" id="Seg_11143" s="T702">refl-n:case.poss</ta>
            <ta e="T704" id="Seg_11144" s="T703">conj</ta>
            <ta e="T705" id="Seg_11145" s="T704">ptcl</ta>
            <ta e="T706" id="Seg_11146" s="T705">n-n:case</ta>
            <ta e="T707" id="Seg_11147" s="T706">v-v:n.fin</ta>
            <ta e="T708" id="Seg_11148" s="T707">ptcl</ta>
            <ta e="T709" id="Seg_11149" s="T708">v-v:tense-v:pn</ta>
            <ta e="T710" id="Seg_11150" s="T709">pers</ta>
            <ta e="T711" id="Seg_11151" s="T710">conj</ta>
            <ta e="T712" id="Seg_11152" s="T711">v-v:tense-v:pn</ta>
            <ta e="T713" id="Seg_11153" s="T712">adj</ta>
            <ta e="T714" id="Seg_11154" s="T713">v-v:tense-v:pn</ta>
            <ta e="T715" id="Seg_11155" s="T714">adv</ta>
            <ta e="T716" id="Seg_11156" s="T715">dempro-n:case</ta>
            <ta e="T717" id="Seg_11157" s="T716">n-n:case</ta>
            <ta e="T718" id="Seg_11158" s="T717">v-v:tense-v:pn</ta>
            <ta e="T720" id="Seg_11159" s="T719">pers</ta>
            <ta e="T721" id="Seg_11160" s="T720">pers</ta>
            <ta e="T722" id="Seg_11161" s="T721">dempro-n:case</ta>
            <ta e="T723" id="Seg_11162" s="T722">v-v:tense-v:pn</ta>
            <ta e="T724" id="Seg_11163" s="T723">propr-n:case</ta>
            <ta e="T725" id="Seg_11164" s="T724">n-n:case</ta>
            <ta e="T726" id="Seg_11165" s="T725">v-v:tense-v:pn</ta>
            <ta e="T727" id="Seg_11166" s="T726">v-v:tense-v:pn</ta>
            <ta e="T730" id="Seg_11167" s="T729">n-n:case.poss</ta>
            <ta e="T731" id="Seg_11168" s="T730">v-v:tense-v:pn</ta>
            <ta e="T732" id="Seg_11169" s="T731">n-n:case</ta>
            <ta e="T733" id="Seg_11170" s="T732">v-v:tense-v:pn</ta>
            <ta e="T734" id="Seg_11171" s="T733">n-n:case</ta>
            <ta e="T735" id="Seg_11172" s="T734">v-v:tense-v:pn</ta>
            <ta e="T736" id="Seg_11173" s="T735">v-v:tense-v:pn</ta>
            <ta e="T737" id="Seg_11174" s="T736">adv</ta>
            <ta e="T738" id="Seg_11175" s="T737">pers</ta>
            <ta e="T740" id="Seg_11176" s="T739">v-v:tense-v:pn</ta>
            <ta e="T741" id="Seg_11177" s="T740">n-n:case</ta>
            <ta e="T742" id="Seg_11178" s="T741">adv</ta>
            <ta e="T743" id="Seg_11179" s="T742">v-v:tense-v:pn</ta>
            <ta e="T744" id="Seg_11180" s="T743">num-n:case</ta>
            <ta e="T745" id="Seg_11181" s="T744">n-n:case</ta>
            <ta e="T746" id="Seg_11182" s="T745">adv</ta>
            <ta e="T747" id="Seg_11183" s="T746">n-n:case</ta>
            <ta e="T748" id="Seg_11184" s="T747">v-v:tense-v:pn</ta>
            <ta e="T749" id="Seg_11185" s="T748">dempro-n:case</ta>
            <ta e="T750" id="Seg_11186" s="T749">v-v:tense-v:pn</ta>
            <ta e="T751" id="Seg_11187" s="T750">n-n:case</ta>
            <ta e="T752" id="Seg_11188" s="T751">adv</ta>
            <ta e="T753" id="Seg_11189" s="T752">pers</ta>
            <ta e="T754" id="Seg_11190" s="T753">adj-n:case</ta>
            <ta e="T755" id="Seg_11191" s="T754">v-v:tense-v:pn</ta>
            <ta e="T756" id="Seg_11192" s="T755">n-n:case</ta>
            <ta e="T757" id="Seg_11193" s="T756">dempro-n:case</ta>
            <ta e="T758" id="Seg_11194" s="T757">adv</ta>
            <ta e="T759" id="Seg_11195" s="T758">dempro-n:case</ta>
            <ta e="T760" id="Seg_11196" s="T759">v-v:tense-v:pn</ta>
            <ta e="T761" id="Seg_11197" s="T760">n-n:case</ta>
            <ta e="T763" id="Seg_11198" s="T762">v-v:tense-v:pn</ta>
            <ta e="T764" id="Seg_11199" s="T763">adv</ta>
            <ta e="T765" id="Seg_11200" s="T764">adv</ta>
            <ta e="T766" id="Seg_11201" s="T765">v-v:tense-v:pn</ta>
            <ta e="T767" id="Seg_11202" s="T766">pers</ta>
            <ta e="T768" id="Seg_11203" s="T767">adv</ta>
            <ta e="T769" id="Seg_11204" s="T768">adj-n:case</ta>
            <ta e="T770" id="Seg_11205" s="T769">v-v:tense-v:pn</ta>
            <ta e="T771" id="Seg_11206" s="T770">adv</ta>
            <ta e="T772" id="Seg_11207" s="T771">dempro-n:case</ta>
            <ta e="T774" id="Seg_11208" s="T773">dempro-n:case</ta>
            <ta e="T775" id="Seg_11209" s="T774">pers</ta>
            <ta e="T776" id="Seg_11210" s="T775">v-v:tense-v:pn</ta>
            <ta e="T777" id="Seg_11211" s="T776">conj</ta>
            <ta e="T778" id="Seg_11212" s="T777">dempro-n:case</ta>
            <ta e="T779" id="Seg_11213" s="T778">v-v:tense-v:pn</ta>
            <ta e="T780" id="Seg_11214" s="T779">adv</ta>
            <ta e="T781" id="Seg_11215" s="T780">num-n:case</ta>
            <ta e="T783" id="Seg_11216" s="T782">n-n:case</ta>
            <ta e="T784" id="Seg_11217" s="T783">v-v:tense-v:pn</ta>
            <ta e="T785" id="Seg_11218" s="T784">adv</ta>
            <ta e="T786" id="Seg_11219" s="T785">n-n:case.poss</ta>
            <ta e="T787" id="Seg_11220" s="T786">v-v:tense-v:pn</ta>
            <ta e="T790" id="Seg_11221" s="T789">n-n:case</ta>
            <ta e="T791" id="Seg_11222" s="T790">conj</ta>
            <ta e="T792" id="Seg_11223" s="T791">n-n:case</ta>
            <ta e="T793" id="Seg_11224" s="T792">adv</ta>
            <ta e="T794" id="Seg_11225" s="T793">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T795" id="Seg_11226" s="T794">n-n:case</ta>
            <ta e="T796" id="Seg_11227" s="T795">conj</ta>
            <ta e="T797" id="Seg_11228" s="T796">n-n:case</ta>
            <ta e="T798" id="Seg_11229" s="T797">pers</ta>
            <ta e="T799" id="Seg_11230" s="T798">adv</ta>
            <ta e="T801" id="Seg_11231" s="T800">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T802" id="Seg_11232" s="T801">%%</ta>
            <ta e="T803" id="Seg_11233" s="T802">v-v:tense-v:pn</ta>
            <ta e="T804" id="Seg_11234" s="T803">adv</ta>
            <ta e="T805" id="Seg_11235" s="T804">n-n:case</ta>
            <ta e="T806" id="Seg_11236" s="T805">pers</ta>
            <ta e="T807" id="Seg_11237" s="T806">v-v:tense-v:pn</ta>
            <ta e="T808" id="Seg_11238" s="T807">pers</ta>
            <ta e="T809" id="Seg_11239" s="T808">v-v:tense-v:pn</ta>
            <ta e="T810" id="Seg_11240" s="T809">dempro-n:case</ta>
            <ta e="T811" id="Seg_11241" s="T810">v-v:tense-v:pn</ta>
            <ta e="T812" id="Seg_11242" s="T811">num-n:case</ta>
            <ta e="T813" id="Seg_11243" s="T812">num-n:case</ta>
            <ta e="T814" id="Seg_11244" s="T813">num-n:case</ta>
            <ta e="T815" id="Seg_11245" s="T814">num-n:case</ta>
            <ta e="T816" id="Seg_11246" s="T815">num-n:case</ta>
            <ta e="T817" id="Seg_11247" s="T816">num-n:case</ta>
            <ta e="T818" id="Seg_11248" s="T817">num-n:case</ta>
            <ta e="T819" id="Seg_11249" s="T818">n-n:case</ta>
            <ta e="T820" id="Seg_11250" s="T819">v-v:tense-v:pn</ta>
            <ta e="T821" id="Seg_11251" s="T820">num-n:case</ta>
            <ta e="T822" id="Seg_11252" s="T821">n-n:case</ta>
            <ta e="T823" id="Seg_11253" s="T822">v-v:tense-v:pn</ta>
            <ta e="T824" id="Seg_11254" s="T823">adv</ta>
            <ta e="T825" id="Seg_11255" s="T824">ptcl</ta>
            <ta e="T826" id="Seg_11256" s="T825">adj</ta>
            <ta e="T827" id="Seg_11257" s="T826">n-n:case</ta>
            <ta e="T828" id="Seg_11258" s="T827">v-v:tense-v:pn</ta>
            <ta e="T829" id="Seg_11259" s="T828">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T830" id="Seg_11260" s="T829">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T831" id="Seg_11261" s="T830">que</ta>
            <ta e="T832" id="Seg_11262" s="T831">que</ta>
            <ta e="T833" id="Seg_11263" s="T832">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T834" id="Seg_11264" s="T833">ptcl</ta>
            <ta e="T835" id="Seg_11265" s="T834">n-n:case.poss</ta>
            <ta e="T836" id="Seg_11266" s="T835">ptcl</ta>
            <ta e="T837" id="Seg_11267" s="T836">n-n:case.poss</ta>
            <ta e="T838" id="Seg_11268" s="T837">v&gt;v-v-v:tense-v:pn</ta>
            <ta e="T839" id="Seg_11269" s="T838">%%</ta>
            <ta e="T840" id="Seg_11270" s="T839">adv</ta>
            <ta e="T841" id="Seg_11271" s="T840">v-v:tense-v:pn</ta>
            <ta e="T842" id="Seg_11272" s="T841">adv</ta>
            <ta e="T843" id="Seg_11273" s="T842">n-n:case</ta>
            <ta e="T844" id="Seg_11274" s="T843">v-v:tense-v:pn</ta>
            <ta e="T845" id="Seg_11275" s="T844">adv</ta>
            <ta e="T846" id="Seg_11276" s="T845">pers</ta>
            <ta e="T847" id="Seg_11277" s="T846">n-n:case.poss</ta>
            <ta e="T848" id="Seg_11278" s="T847">dempro-n:case</ta>
            <ta e="T849" id="Seg_11279" s="T848">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T850" id="Seg_11280" s="T849">pers</ta>
            <ta e="T851" id="Seg_11281" s="T850">num-n:case</ta>
            <ta e="T852" id="Seg_11282" s="T851">n-n:case.poss</ta>
            <ta e="T853" id="Seg_11283" s="T852">v-v:tense-v:pn</ta>
            <ta e="T854" id="Seg_11284" s="T853">adv</ta>
            <ta e="T855" id="Seg_11285" s="T854">v-v&gt;v-v:pn</ta>
            <ta e="T856" id="Seg_11286" s="T855">que-n:case=ptcl</ta>
            <ta e="T858" id="Seg_11287" s="T856">ptcl</ta>
            <ta e="T859" id="Seg_11288" s="T858">n-n:case</ta>
            <ta e="T860" id="Seg_11289" s="T859">adv</ta>
            <ta e="T861" id="Seg_11290" s="T860">v-v:tense-v:pn</ta>
            <ta e="T862" id="Seg_11291" s="T861">n-n:case</ta>
            <ta e="T863" id="Seg_11292" s="T862">refl-n:case.poss</ta>
            <ta e="T864" id="Seg_11293" s="T863">n-n:case</ta>
            <ta e="T865" id="Seg_11294" s="T864">adv</ta>
            <ta e="T866" id="Seg_11295" s="T865">num-n:case</ta>
            <ta e="T867" id="Seg_11296" s="T866">n-n:case</ta>
            <ta e="T868" id="Seg_11297" s="T867">ptcl</ta>
            <ta e="T869" id="Seg_11298" s="T868">pers</ta>
            <ta e="T870" id="Seg_11299" s="T869">v-v:n.fin</ta>
            <ta e="T871" id="Seg_11300" s="T870">pers</ta>
            <ta e="T875" id="Seg_11301" s="T874">ptcl</ta>
            <ta e="T876" id="Seg_11302" s="T875">v-v:tense-v:pn</ta>
            <ta e="T877" id="Seg_11303" s="T876">n-n:case.poss</ta>
            <ta e="T878" id="Seg_11304" s="T877">n-n:case.poss</ta>
            <ta e="T879" id="Seg_11305" s="T878">adj-n:case</ta>
            <ta e="T880" id="Seg_11306" s="T879">ptcl</ta>
            <ta e="T881" id="Seg_11307" s="T880">v-v:tense-v:pn</ta>
            <ta e="T882" id="Seg_11308" s="T881">dempro</ta>
            <ta e="T883" id="Seg_11309" s="T882">v-v&gt;v-v:pn</ta>
            <ta e="T884" id="Seg_11310" s="T883">ptcl</ta>
            <ta e="T885" id="Seg_11311" s="T884">v-v:n.fin</ta>
            <ta e="T886" id="Seg_11312" s="T885">pers-n:case</ta>
            <ta e="T1055" id="Seg_11313" s="T886">ptcl</ta>
            <ta e="T887" id="Seg_11314" s="T1055">v-v:tense-v:pn</ta>
            <ta e="T888" id="Seg_11315" s="T887">v-v:tense-v:pn</ta>
            <ta e="T889" id="Seg_11316" s="T888">adv</ta>
            <ta e="T890" id="Seg_11317" s="T889">v-v:tense-v:pn</ta>
            <ta e="T891" id="Seg_11318" s="T890">adv</ta>
            <ta e="T892" id="Seg_11319" s="T891">adv</ta>
            <ta e="T894" id="Seg_11320" s="T893">v-v:tense-v:pn</ta>
            <ta e="T895" id="Seg_11321" s="T894">v-v&gt;v-v:pn</ta>
            <ta e="T896" id="Seg_11322" s="T895">pers</ta>
            <ta e="T897" id="Seg_11323" s="T896">ptcl</ta>
            <ta e="T898" id="Seg_11324" s="T897">v-v:tense-v:pn</ta>
            <ta e="T899" id="Seg_11325" s="T898">pers-n:case</ta>
            <ta e="T900" id="Seg_11326" s="T899">adv</ta>
            <ta e="T901" id="Seg_11327" s="T900">ptcl</ta>
            <ta e="T902" id="Seg_11328" s="T901">v-v:n.fin</ta>
            <ta e="T903" id="Seg_11329" s="T902">adv</ta>
            <ta e="T904" id="Seg_11330" s="T903">v-v:tense-v:pn</ta>
            <ta e="T905" id="Seg_11331" s="T904">adv</ta>
            <ta e="T906" id="Seg_11332" s="T905">adv</ta>
            <ta e="T907" id="Seg_11333" s="T906">pers</ta>
            <ta e="T908" id="Seg_11334" s="T907">v-v:tense-v:pn</ta>
            <ta e="T910" id="Seg_11335" s="T909">n-n:case</ta>
            <ta e="T911" id="Seg_11336" s="T910">v-v:pn-v:pn</ta>
            <ta e="T912" id="Seg_11337" s="T911">adv</ta>
            <ta e="T913" id="Seg_11338" s="T912">ptcl</ta>
            <ta e="T914" id="Seg_11339" s="T913">v-v:tense-v:pn</ta>
            <ta e="T915" id="Seg_11340" s="T914">conj</ta>
            <ta e="T916" id="Seg_11341" s="T915">ptcl</ta>
            <ta e="T917" id="Seg_11342" s="T916">v-v:tense-v:pn</ta>
            <ta e="T918" id="Seg_11343" s="T917">n-n:case</ta>
            <ta e="T920" id="Seg_11344" s="T919">v-v:tense-v:pn</ta>
            <ta e="T921" id="Seg_11345" s="T920">conj</ta>
            <ta e="T1058" id="Seg_11346" s="T921">v-v:n-fin</ta>
            <ta e="T922" id="Seg_11347" s="T1058">v-v:tense-v:pn</ta>
            <ta e="T923" id="Seg_11348" s="T922">pers</ta>
            <ta e="T924" id="Seg_11349" s="T923">adv</ta>
            <ta e="T926" id="Seg_11350" s="T925">adv</ta>
            <ta e="T927" id="Seg_11351" s="T926">pers</ta>
            <ta e="T928" id="Seg_11352" s="T927">adv</ta>
            <ta e="T929" id="Seg_11353" s="T928">adv</ta>
            <ta e="T930" id="Seg_11354" s="T929">v-v&gt;v-v:pn</ta>
            <ta e="T931" id="Seg_11355" s="T930">v-v&gt;v-v:pn</ta>
            <ta e="T932" id="Seg_11356" s="T931">ptcl</ta>
            <ta e="T933" id="Seg_11357" s="T932">num-n:case</ta>
            <ta e="T934" id="Seg_11358" s="T933">num-n:case</ta>
            <ta e="T935" id="Seg_11359" s="T934">n-n:case</ta>
            <ta e="T936" id="Seg_11360" s="T935">v-v&gt;v-v:pn</ta>
            <ta e="T937" id="Seg_11361" s="T936">que</ta>
            <ta e="T938" id="Seg_11362" s="T937">dempro-n:case</ta>
            <ta e="T939" id="Seg_11363" s="T938">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T940" id="Seg_11364" s="T939">adv</ta>
            <ta e="T941" id="Seg_11365" s="T940">pers</ta>
            <ta e="T942" id="Seg_11366" s="T941">dempro-n:case</ta>
            <ta e="T943" id="Seg_11367" s="T942">n-n:case</ta>
            <ta e="T944" id="Seg_11368" s="T943">ptcl</ta>
            <ta e="T945" id="Seg_11369" s="T944">n-n:case</ta>
            <ta e="T946" id="Seg_11370" s="T945">n-n:case</ta>
            <ta e="T947" id="Seg_11371" s="T946">v-v:tense-v:pn</ta>
            <ta e="T948" id="Seg_11372" s="T947">n-n:case</ta>
            <ta e="T950" id="Seg_11373" s="T949">v-v:n.fin</ta>
            <ta e="T952" id="Seg_11374" s="T951">v-v:tense-v:pn</ta>
            <ta e="T954" id="Seg_11375" s="T953">n-n:case</ta>
            <ta e="T955" id="Seg_11376" s="T954">conj</ta>
            <ta e="T956" id="Seg_11377" s="T955">n-n:case</ta>
            <ta e="T957" id="Seg_11378" s="T956">v-v:tense-v:pn</ta>
            <ta e="T958" id="Seg_11379" s="T957">n-n:num</ta>
            <ta e="T959" id="Seg_11380" s="T958">v-v:tense-v:pn</ta>
            <ta e="T960" id="Seg_11381" s="T959">n-n:case</ta>
            <ta e="T961" id="Seg_11382" s="T960">conj</ta>
            <ta e="T962" id="Seg_11383" s="T961">pers</ta>
            <ta e="T963" id="Seg_11384" s="T962">n-n:num</ta>
            <ta e="T964" id="Seg_11385" s="T963">dempro-n:case</ta>
            <ta e="T965" id="Seg_11386" s="T964">v-v:tense-v:pn</ta>
            <ta e="T966" id="Seg_11387" s="T965">adv</ta>
            <ta e="T967" id="Seg_11388" s="T966">%%-n:num</ta>
            <ta e="T969" id="Seg_11389" s="T968">v-v:tense-v:pn</ta>
            <ta e="T971" id="Seg_11390" s="T970">v-v:n.fin</ta>
            <ta e="T972" id="Seg_11391" s="T971">conj</ta>
            <ta e="T974" id="Seg_11392" s="T973">v-v:n.fin</ta>
            <ta e="T975" id="Seg_11393" s="T974">conj</ta>
            <ta e="T976" id="Seg_11394" s="T975">pers</ta>
            <ta e="T977" id="Seg_11395" s="T976">%%</ta>
            <ta e="T978" id="Seg_11396" s="T977">adj-n:case</ta>
            <ta e="T979" id="Seg_11397" s="T978">n-n:num</ta>
            <ta e="T980" id="Seg_11398" s="T979">v-v:tense-v:pn</ta>
            <ta e="T981" id="Seg_11399" s="T980">conj</ta>
            <ta e="T982" id="Seg_11400" s="T981">n-n:num</ta>
            <ta e="T983" id="Seg_11401" s="T982">v-v:tense-v:pn</ta>
            <ta e="T984" id="Seg_11402" s="T983">conj</ta>
            <ta e="T985" id="Seg_11403" s="T984">dempro-n:case</ta>
            <ta e="T986" id="Seg_11404" s="T985">ptcl</ta>
            <ta e="T987" id="Seg_11405" s="T986">pers</ta>
            <ta e="T988" id="Seg_11406" s="T987">v-v:tense-v:pn</ta>
            <ta e="T989" id="Seg_11407" s="T988">n-n:case.poss</ta>
            <ta e="T990" id="Seg_11408" s="T989">v-%%-v:tense-v:pn</ta>
            <ta e="T991" id="Seg_11409" s="T990">conj</ta>
            <ta e="T992" id="Seg_11410" s="T991">ptcl</ta>
            <ta e="T993" id="Seg_11411" s="T992">v-v:tense-v:pn</ta>
            <ta e="T994" id="Seg_11412" s="T993">pers</ta>
            <ta e="T995" id="Seg_11413" s="T994">dempro-n:case</ta>
            <ta e="T996" id="Seg_11414" s="T995">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T997" id="Seg_11415" s="T996">pers</ta>
            <ta e="T998" id="Seg_11416" s="T997">refl-n:case.poss</ta>
            <ta e="T1000" id="Seg_11417" s="T999">n-n:case</ta>
            <ta e="T1001" id="Seg_11418" s="T1000">n-n:num</ta>
            <ta e="T1002" id="Seg_11419" s="T1001">v-v:tense-v:pn</ta>
            <ta e="T1003" id="Seg_11420" s="T1002">conj</ta>
            <ta e="T1004" id="Seg_11421" s="T1003">n-n:case</ta>
            <ta e="T1005" id="Seg_11422" s="T1004">v-v:tense-v:pn</ta>
            <ta e="T1006" id="Seg_11423" s="T1005">n-n:case</ta>
            <ta e="T1007" id="Seg_11424" s="T1006">n-n:num</ta>
            <ta e="T1008" id="Seg_11425" s="T1007">conj</ta>
            <ta e="T1009" id="Seg_11426" s="T1008">n-n:case</ta>
            <ta e="T1010" id="Seg_11427" s="T1009">v-v:tense-v:pn</ta>
            <ta e="T1011" id="Seg_11428" s="T1010">n-n:case.poss</ta>
            <ta e="T1012" id="Seg_11429" s="T1011">v-v:tense-v:pn</ta>
            <ta e="T1013" id="Seg_11430" s="T1012">n-n:num</ta>
            <ta e="T1014" id="Seg_11431" s="T1013">v-v:tense-v:pn</ta>
            <ta e="T1015" id="Seg_11432" s="T1014">dempro-n:case</ta>
            <ta e="T1016" id="Seg_11433" s="T1015">v-v:tense-v:pn</ta>
            <ta e="T1017" id="Seg_11434" s="T1016">adv</ta>
            <ta e="T1056" id="Seg_11435" s="T1017">adj</ta>
            <ta e="T1019" id="Seg_11436" s="T1018">v-v:tense-v:pn</ta>
            <ta e="T1021" id="Seg_11437" s="T1020">n-n:case</ta>
            <ta e="T1022" id="Seg_11438" s="T1021">ptcl</ta>
            <ta e="T1023" id="Seg_11439" s="T1022">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1024" id="Seg_11440" s="T1023">adv</ta>
            <ta e="T1025" id="Seg_11441" s="T1024">adj-n:case</ta>
            <ta e="T1026" id="Seg_11442" s="T1025">n-n:case</ta>
            <ta e="T1027" id="Seg_11443" s="T1026">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1028" id="Seg_11444" s="T1027">adv</ta>
            <ta e="T1029" id="Seg_11445" s="T1028">pers</ta>
            <ta e="T1030" id="Seg_11446" s="T1029">n-n:case</ta>
            <ta e="T1033" id="Seg_11447" s="T1032">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1034" id="Seg_11448" s="T1033">n-n:case</ta>
            <ta e="T1035" id="Seg_11449" s="T1034">v-v:tense-v:pn</ta>
            <ta e="T1037" id="Seg_11450" s="T1036">v-v:tense-v:pn</ta>
            <ta e="T1038" id="Seg_11451" s="T1037">adv</ta>
            <ta e="T1039" id="Seg_11452" s="T1038">adj</ta>
            <ta e="T1040" id="Seg_11453" s="T1039">v-v:tense-v:pn</ta>
            <ta e="T1041" id="Seg_11454" s="T1040">n-n:case</ta>
            <ta e="T1042" id="Seg_11455" s="T1041">adv</ta>
            <ta e="T1043" id="Seg_11456" s="T1042">n.[n:case]</ta>
            <ta e="T1044" id="Seg_11457" s="T1043">v-v:tense-v:pn</ta>
            <ta e="T1045" id="Seg_11458" s="T1044">n-n:case.poss</ta>
            <ta e="T1046" id="Seg_11459" s="T1045">v-v:tense-v:pn</ta>
            <ta e="T1047" id="Seg_11460" s="T1046">n-n:case</ta>
            <ta e="T1048" id="Seg_11461" s="T1047">v-v:tense-v:pn</ta>
            <ta e="T1049" id="Seg_11462" s="T1048">conj</ta>
            <ta e="T1050" id="Seg_11463" s="T1049">n-n:case</ta>
            <ta e="T1051" id="Seg_11464" s="T1050">n-n:case</ta>
            <ta e="T1052" id="Seg_11465" s="T1051">v-v:tense-v:pn</ta>
            <ta e="T1053" id="Seg_11466" s="T1052">adj-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_11467" s="T0">pers</ta>
            <ta e="T2" id="Seg_11468" s="T1">adv</ta>
            <ta e="T4" id="Seg_11469" s="T3">n</ta>
            <ta e="T5" id="Seg_11470" s="T4">v</ta>
            <ta e="T6" id="Seg_11471" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_11472" s="T6">v</ta>
            <ta e="T9" id="Seg_11473" s="T8">v</ta>
            <ta e="T10" id="Seg_11474" s="T9">conj</ta>
            <ta e="T11" id="Seg_11475" s="T10">adv</ta>
            <ta e="T12" id="Seg_11476" s="T11">num</ta>
            <ta e="T13" id="Seg_11477" s="T12">n</ta>
            <ta e="T14" id="Seg_11478" s="T13">n</ta>
            <ta e="T15" id="Seg_11479" s="T14">v</ta>
            <ta e="T16" id="Seg_11480" s="T15">v</ta>
            <ta e="T17" id="Seg_11481" s="T16">num</ta>
            <ta e="T19" id="Seg_11482" s="T18">num</ta>
            <ta e="T20" id="Seg_11483" s="T19">n</ta>
            <ta e="T21" id="Seg_11484" s="T20">v</ta>
            <ta e="T22" id="Seg_11485" s="T21">conj</ta>
            <ta e="T23" id="Seg_11486" s="T22">v</ta>
            <ta e="T24" id="Seg_11487" s="T23">conj</ta>
            <ta e="T26" id="Seg_11488" s="T25">n</ta>
            <ta e="T27" id="Seg_11489" s="T26">v</ta>
            <ta e="T28" id="Seg_11490" s="T27">conj</ta>
            <ta e="T29" id="Seg_11491" s="T28">n</ta>
            <ta e="T30" id="Seg_11492" s="T29">v</ta>
            <ta e="T31" id="Seg_11493" s="T30">conj</ta>
            <ta e="T32" id="Seg_11494" s="T31">n</ta>
            <ta e="T33" id="Seg_11495" s="T32">v</ta>
            <ta e="T34" id="Seg_11496" s="T33">conj</ta>
            <ta e="T35" id="Seg_11497" s="T34">n</ta>
            <ta e="T36" id="Seg_11498" s="T35">v</ta>
            <ta e="T37" id="Seg_11499" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_11500" s="T37">pers</ta>
            <ta e="T39" id="Seg_11501" s="T38">n</ta>
            <ta e="T40" id="Seg_11502" s="T39">n</ta>
            <ta e="T41" id="Seg_11503" s="T40">v</ta>
            <ta e="T42" id="Seg_11504" s="T41">dempro</ta>
            <ta e="T43" id="Seg_11505" s="T42">n</ta>
            <ta e="T44" id="Seg_11506" s="T43">pers</ta>
            <ta e="T45" id="Seg_11507" s="T44">v</ta>
            <ta e="T46" id="Seg_11508" s="T45">n</ta>
            <ta e="T47" id="Seg_11509" s="T46">v</ta>
            <ta e="T48" id="Seg_11510" s="T47">conj</ta>
            <ta e="T49" id="Seg_11511" s="T48">n</ta>
            <ta e="T50" id="Seg_11512" s="T49">v</ta>
            <ta e="T51" id="Seg_11513" s="T50">pers</ta>
            <ta e="T52" id="Seg_11514" s="T51">v</ta>
            <ta e="T53" id="Seg_11515" s="T52">v</ta>
            <ta e="T54" id="Seg_11516" s="T53">que</ta>
            <ta e="T56" id="Seg_11517" s="T55">pers</ta>
            <ta e="T57" id="Seg_11518" s="T56">propr</ta>
            <ta e="T59" id="Seg_11519" s="T58">v</ta>
            <ta e="T60" id="Seg_11520" s="T59">pers</ta>
            <ta e="T61" id="Seg_11521" s="T60">dempro</ta>
            <ta e="T62" id="Seg_11522" s="T61">v</ta>
            <ta e="T63" id="Seg_11523" s="T62">v</ta>
            <ta e="T64" id="Seg_11524" s="T63">propr</ta>
            <ta e="T65" id="Seg_11525" s="T64">v</ta>
            <ta e="T66" id="Seg_11526" s="T65">adv</ta>
            <ta e="T67" id="Seg_11527" s="T66">adv</ta>
            <ta e="T68" id="Seg_11528" s="T67">adv</ta>
            <ta e="T69" id="Seg_11529" s="T68">v</ta>
            <ta e="T70" id="Seg_11530" s="T69">n</ta>
            <ta e="T71" id="Seg_11531" s="T70">n</ta>
            <ta e="T72" id="Seg_11532" s="T71">v</ta>
            <ta e="T73" id="Seg_11533" s="T72">dempro</ta>
            <ta e="T74" id="Seg_11534" s="T73">n</ta>
            <ta e="T75" id="Seg_11535" s="T74">v</ta>
            <ta e="T76" id="Seg_11536" s="T75">conj</ta>
            <ta e="T77" id="Seg_11537" s="T76">n</ta>
            <ta e="T78" id="Seg_11538" s="T77">v</ta>
            <ta e="T79" id="Seg_11539" s="T78">n</ta>
            <ta e="T80" id="Seg_11540" s="T79">v</ta>
            <ta e="T81" id="Seg_11541" s="T80">refl</ta>
            <ta e="T82" id="Seg_11542" s="T81">v</ta>
            <ta e="T83" id="Seg_11543" s="T82">n</ta>
            <ta e="T84" id="Seg_11544" s="T83">pers</ta>
            <ta e="T85" id="Seg_11545" s="T84">n</ta>
            <ta e="T86" id="Seg_11546" s="T85">n</ta>
            <ta e="T87" id="Seg_11547" s="T86">v</ta>
            <ta e="T90" id="Seg_11548" s="T89">v</ta>
            <ta e="T91" id="Seg_11549" s="T90">n</ta>
            <ta e="T92" id="Seg_11550" s="T91">conj</ta>
            <ta e="T93" id="Seg_11551" s="T92">dempro</ta>
            <ta e="T94" id="Seg_11552" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_11553" s="T94">v</ta>
            <ta e="T96" id="Seg_11554" s="T95">dempro</ta>
            <ta e="T97" id="Seg_11555" s="T96">n</ta>
            <ta e="T98" id="Seg_11556" s="T97">conj</ta>
            <ta e="T99" id="Seg_11557" s="T98">dempro</ta>
            <ta e="T100" id="Seg_11558" s="T99">v</ta>
            <ta e="T101" id="Seg_11559" s="T100">pers</ta>
            <ta e="T102" id="Seg_11560" s="T101">dempro</ta>
            <ta e="T103" id="Seg_11561" s="T102">v</ta>
            <ta e="T104" id="Seg_11562" s="T103">conj</ta>
            <ta e="T105" id="Seg_11563" s="T104">que</ta>
            <ta e="T106" id="Seg_11564" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_11565" s="T106">v</ta>
            <ta e="T109" id="Seg_11566" s="T108">adv</ta>
            <ta e="T110" id="Seg_11567" s="T109">pers</ta>
            <ta e="T111" id="Seg_11568" s="T110">v</ta>
            <ta e="T113" id="Seg_11569" s="T112">pers</ta>
            <ta e="T115" id="Seg_11570" s="T114">n</ta>
            <ta e="T116" id="Seg_11571" s="T115">v</ta>
            <ta e="T117" id="Seg_11572" s="T116">pers</ta>
            <ta e="T118" id="Seg_11573" s="T117">v</ta>
            <ta e="T120" id="Seg_11574" s="T119">n</ta>
            <ta e="T122" id="Seg_11575" s="T121">v</ta>
            <ta e="T124" id="Seg_11576" s="T123">v</ta>
            <ta e="T125" id="Seg_11577" s="T124">conj</ta>
            <ta e="T126" id="Seg_11578" s="T125">pers</ta>
            <ta e="T127" id="Seg_11579" s="T126">v</ta>
            <ta e="T128" id="Seg_11580" s="T127">v</ta>
            <ta e="T129" id="Seg_11581" s="T128">conj</ta>
            <ta e="T131" id="Seg_11582" s="T130">conj</ta>
            <ta e="T132" id="Seg_11583" s="T131">refl</ta>
            <ta e="T133" id="Seg_11584" s="T132">ptcl</ta>
            <ta e="T134" id="Seg_11585" s="T133">v</ta>
            <ta e="T135" id="Seg_11586" s="T134">n</ta>
            <ta e="T136" id="Seg_11587" s="T135">v</ta>
            <ta e="T137" id="Seg_11588" s="T136">v</ta>
            <ta e="T138" id="Seg_11589" s="T137">dempro</ta>
            <ta e="T139" id="Seg_11590" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_11591" s="T139">v</ta>
            <ta e="T141" id="Seg_11592" s="T140">conj</ta>
            <ta e="T142" id="Seg_11593" s="T141">pers</ta>
            <ta e="T143" id="Seg_11594" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_11595" s="T143">v</ta>
            <ta e="T145" id="Seg_11596" s="T144">adv</ta>
            <ta e="T146" id="Seg_11597" s="T145">adv</ta>
            <ta e="T147" id="Seg_11598" s="T146">adj</ta>
            <ta e="T148" id="Seg_11599" s="T147">conj</ta>
            <ta e="T149" id="Seg_11600" s="T148">pers</ta>
            <ta e="T150" id="Seg_11601" s="T149">n</ta>
            <ta e="T151" id="Seg_11602" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_11603" s="T151">adv</ta>
            <ta e="T153" id="Seg_11604" s="T152">pers</ta>
            <ta e="T156" id="Seg_11605" s="T155">dempro</ta>
            <ta e="T157" id="Seg_11606" s="T156">v</ta>
            <ta e="T158" id="Seg_11607" s="T157">conj</ta>
            <ta e="T159" id="Seg_11608" s="T158">dempro</ta>
            <ta e="T160" id="Seg_11609" s="T159">v</ta>
            <ta e="T161" id="Seg_11610" s="T160">ptcl</ta>
            <ta e="T162" id="Seg_11611" s="T161">v</ta>
            <ta e="T163" id="Seg_11612" s="T162">n</ta>
            <ta e="T164" id="Seg_11613" s="T163">adv</ta>
            <ta e="T166" id="Seg_11614" s="T165">v</ta>
            <ta e="T167" id="Seg_11615" s="T166">adv</ta>
            <ta e="T168" id="Seg_11616" s="T167">n</ta>
            <ta e="T169" id="Seg_11617" s="T168">ptcl</ta>
            <ta e="T170" id="Seg_11618" s="T169">v</ta>
            <ta e="T171" id="Seg_11619" s="T170">pers</ta>
            <ta e="T172" id="Seg_11620" s="T171">v</ta>
            <ta e="T173" id="Seg_11621" s="T172">v</ta>
            <ta e="T174" id="Seg_11622" s="T173">v</ta>
            <ta e="T175" id="Seg_11623" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_11624" s="T175">v</ta>
            <ta e="T177" id="Seg_11625" s="T176">adv</ta>
            <ta e="T178" id="Seg_11626" s="T177">adj</ta>
            <ta e="T179" id="Seg_11627" s="T178">v</ta>
            <ta e="T180" id="Seg_11628" s="T179">n</ta>
            <ta e="T181" id="Seg_11629" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_11630" s="T181">adj</ta>
            <ta e="T183" id="Seg_11631" s="T182">v</ta>
            <ta e="T184" id="Seg_11632" s="T183">n</ta>
            <ta e="T186" id="Seg_11633" s="T185">adv</ta>
            <ta e="T187" id="Seg_11634" s="T186">n</ta>
            <ta e="T188" id="Seg_11635" s="T187">v</ta>
            <ta e="T189" id="Seg_11636" s="T188">adv</ta>
            <ta e="T190" id="Seg_11637" s="T189">n</ta>
            <ta e="T191" id="Seg_11638" s="T190">v</ta>
            <ta e="T193" id="Seg_11639" s="T192">n</ta>
            <ta e="T194" id="Seg_11640" s="T193">v</ta>
            <ta e="T195" id="Seg_11641" s="T194">n</ta>
            <ta e="T196" id="Seg_11642" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_11643" s="T196">adj</ta>
            <ta e="T198" id="Seg_11644" s="T197">v</ta>
            <ta e="T199" id="Seg_11645" s="T198">adj</ta>
            <ta e="T200" id="Seg_11646" s="T199">v</ta>
            <ta e="T201" id="Seg_11647" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_11648" s="T201">adv</ta>
            <ta e="T203" id="Seg_11649" s="T202">n</ta>
            <ta e="T204" id="Seg_11650" s="T203">n</ta>
            <ta e="T205" id="Seg_11651" s="T204">n</ta>
            <ta e="T206" id="Seg_11652" s="T205">v</ta>
            <ta e="T207" id="Seg_11653" s="T206">n</ta>
            <ta e="T208" id="Seg_11654" s="T207">v</ta>
            <ta e="T209" id="Seg_11655" s="T208">adv</ta>
            <ta e="T210" id="Seg_11656" s="T209">n</ta>
            <ta e="T211" id="Seg_11657" s="T210">adj</ta>
            <ta e="T212" id="Seg_11658" s="T211">v</ta>
            <ta e="T213" id="Seg_11659" s="T212">adv</ta>
            <ta e="T214" id="Seg_11660" s="T213">dempro</ta>
            <ta e="T216" id="Seg_11661" s="T215">n</ta>
            <ta e="T217" id="Seg_11662" s="T216">v</ta>
            <ta e="T218" id="Seg_11663" s="T217">ptcl</ta>
            <ta e="T219" id="Seg_11664" s="T218">adv</ta>
            <ta e="T220" id="Seg_11665" s="T219">n</ta>
            <ta e="T221" id="Seg_11666" s="T220">v</ta>
            <ta e="T222" id="Seg_11667" s="T221">conj</ta>
            <ta e="T223" id="Seg_11668" s="T222">v</ta>
            <ta e="T224" id="Seg_11669" s="T223">ptcl</ta>
            <ta e="T225" id="Seg_11670" s="T224">n</ta>
            <ta e="T226" id="Seg_11671" s="T225">v</ta>
            <ta e="T227" id="Seg_11672" s="T226">adv</ta>
            <ta e="T228" id="Seg_11673" s="T227">adj</ta>
            <ta e="T229" id="Seg_11674" s="T228">n</ta>
            <ta e="T230" id="Seg_11675" s="T229">v</ta>
            <ta e="T231" id="Seg_11676" s="T230">adv</ta>
            <ta e="T233" id="Seg_11677" s="T232">adj</ta>
            <ta e="T234" id="Seg_11678" s="T233">pers</ta>
            <ta e="T236" id="Seg_11679" s="T235">v</ta>
            <ta e="T237" id="Seg_11680" s="T236">v</ta>
            <ta e="T238" id="Seg_11681" s="T237">adv</ta>
            <ta e="T239" id="Seg_11682" s="T238">n</ta>
            <ta e="T240" id="Seg_11683" s="T239">v</ta>
            <ta e="T241" id="Seg_11684" s="T240">conj</ta>
            <ta e="T242" id="Seg_11685" s="T241">n</ta>
            <ta e="T243" id="Seg_11686" s="T242">n</ta>
            <ta e="T244" id="Seg_11687" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_11688" s="T244">v</ta>
            <ta e="T246" id="Seg_11689" s="T245">n</ta>
            <ta e="T247" id="Seg_11690" s="T246">v</ta>
            <ta e="T248" id="Seg_11691" s="T247">conj</ta>
            <ta e="T249" id="Seg_11692" s="T248">v</ta>
            <ta e="T250" id="Seg_11693" s="T249">v</ta>
            <ta e="T251" id="Seg_11694" s="T250">adv</ta>
            <ta e="T252" id="Seg_11695" s="T251">v</ta>
            <ta e="T253" id="Seg_11696" s="T252">adv</ta>
            <ta e="T254" id="Seg_11697" s="T253">v</ta>
            <ta e="T255" id="Seg_11698" s="T254">conj</ta>
            <ta e="T256" id="Seg_11699" s="T255">n</ta>
            <ta e="T257" id="Seg_11700" s="T256">v</ta>
            <ta e="T258" id="Seg_11701" s="T257">pers</ta>
            <ta e="T259" id="Seg_11702" s="T258">propr</ta>
            <ta e="T260" id="Seg_11703" s="T259">n</ta>
            <ta e="T261" id="Seg_11704" s="T260">ptcl</ta>
            <ta e="T262" id="Seg_11705" s="T261">v</ta>
            <ta e="T263" id="Seg_11706" s="T262">pers</ta>
            <ta e="T264" id="Seg_11707" s="T263">n</ta>
            <ta e="T265" id="Seg_11708" s="T264">v</ta>
            <ta e="T266" id="Seg_11709" s="T265">n</ta>
            <ta e="T267" id="Seg_11710" s="T266">v</ta>
            <ta e="T268" id="Seg_11711" s="T267">ptcl</ta>
            <ta e="T269" id="Seg_11712" s="T268">conj</ta>
            <ta e="T270" id="Seg_11713" s="T269">n</ta>
            <ta e="T271" id="Seg_11714" s="T270">v</ta>
            <ta e="T272" id="Seg_11715" s="T271">pers</ta>
            <ta e="T273" id="Seg_11716" s="T272">dempro</ta>
            <ta e="T274" id="Seg_11717" s="T273">v</ta>
            <ta e="T275" id="Seg_11718" s="T274">pers</ta>
            <ta e="T276" id="Seg_11719" s="T275">quant</ta>
            <ta e="T277" id="Seg_11720" s="T276">v</ta>
            <ta e="T279" id="Seg_11721" s="T278">adv</ta>
            <ta e="T281" id="Seg_11722" s="T280">v</ta>
            <ta e="T282" id="Seg_11723" s="T281">adv</ta>
            <ta e="T283" id="Seg_11724" s="T282">pers</ta>
            <ta e="T284" id="Seg_11725" s="T283">adj</ta>
            <ta e="T285" id="Seg_11726" s="T284">n</ta>
            <ta e="T286" id="Seg_11727" s="T285">adv</ta>
            <ta e="T287" id="Seg_11728" s="T286">pers</ta>
            <ta e="T288" id="Seg_11729" s="T287">pers</ta>
            <ta e="T289" id="Seg_11730" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_11731" s="T289">v</ta>
            <ta e="T291" id="Seg_11732" s="T290">adv</ta>
            <ta e="T292" id="Seg_11733" s="T291">pers</ta>
            <ta e="T293" id="Seg_11734" s="T292">v</ta>
            <ta e="T294" id="Seg_11735" s="T293">conj</ta>
            <ta e="T295" id="Seg_11736" s="T294">pers</ta>
            <ta e="T296" id="Seg_11737" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_11738" s="T296">v</ta>
            <ta e="T299" id="Seg_11739" s="T298">pers</ta>
            <ta e="T300" id="Seg_11740" s="T299">ptcl</ta>
            <ta e="T301" id="Seg_11741" s="T300">v</ta>
            <ta e="T302" id="Seg_11742" s="T301">que</ta>
            <ta e="T303" id="Seg_11743" s="T302">v</ta>
            <ta e="T304" id="Seg_11744" s="T303">pers</ta>
            <ta e="T305" id="Seg_11745" s="T304">v</ta>
            <ta e="T306" id="Seg_11746" s="T305">conj</ta>
            <ta e="T308" id="Seg_11747" s="T307">conj</ta>
            <ta e="T309" id="Seg_11748" s="T308">pers</ta>
            <ta e="T310" id="Seg_11749" s="T309">que</ta>
            <ta e="T311" id="Seg_11750" s="T310">adv</ta>
            <ta e="T313" id="Seg_11751" s="T312">v</ta>
            <ta e="T317" id="Seg_11752" s="T315">v</ta>
            <ta e="T318" id="Seg_11753" s="T317">pers</ta>
            <ta e="T319" id="Seg_11754" s="T318">n</ta>
            <ta e="T322" id="Seg_11755" s="T321">pers</ta>
            <ta e="T323" id="Seg_11756" s="T322">que</ta>
            <ta e="T324" id="Seg_11757" s="T323">ptcl</ta>
            <ta e="T325" id="Seg_11758" s="T324">v</ta>
            <ta e="T326" id="Seg_11759" s="T325">v</ta>
            <ta e="T327" id="Seg_11760" s="T326">que</ta>
            <ta e="T328" id="Seg_11761" s="T327">v</ta>
            <ta e="T329" id="Seg_11762" s="T328">que</ta>
            <ta e="T330" id="Seg_11763" s="T329">n</ta>
            <ta e="T331" id="Seg_11764" s="T330">adv</ta>
            <ta e="T1057" id="Seg_11765" s="T331">v</ta>
            <ta e="T332" id="Seg_11766" s="T1057">v</ta>
            <ta e="T333" id="Seg_11767" s="T332">conj</ta>
            <ta e="T334" id="Seg_11768" s="T333">pers</ta>
            <ta e="T335" id="Seg_11769" s="T334">adv</ta>
            <ta e="T336" id="Seg_11770" s="T335">adj</ta>
            <ta e="T337" id="Seg_11771" s="T336">que</ta>
            <ta e="T338" id="Seg_11772" s="T337">ptcl</ta>
            <ta e="T339" id="Seg_11773" s="T338">v</ta>
            <ta e="T340" id="Seg_11774" s="T339">pers</ta>
            <ta e="T342" id="Seg_11775" s="T341">v</ta>
            <ta e="T344" id="Seg_11776" s="T343">v</ta>
            <ta e="T345" id="Seg_11777" s="T344">v</ta>
            <ta e="T346" id="Seg_11778" s="T345">ptcl</ta>
            <ta e="T347" id="Seg_11779" s="T346">conj</ta>
            <ta e="T348" id="Seg_11780" s="T347">n</ta>
            <ta e="T349" id="Seg_11781" s="T348">ptcl</ta>
            <ta e="T350" id="Seg_11782" s="T349">v</ta>
            <ta e="T351" id="Seg_11783" s="T350">n</ta>
            <ta e="T352" id="Seg_11784" s="T351">n</ta>
            <ta e="T353" id="Seg_11785" s="T352">dempro</ta>
            <ta e="T354" id="Seg_11786" s="T353">v</ta>
            <ta e="T355" id="Seg_11787" s="T354">conj</ta>
            <ta e="T356" id="Seg_11788" s="T355">pers</ta>
            <ta e="T358" id="Seg_11789" s="T357">conj</ta>
            <ta e="T359" id="Seg_11790" s="T358">pers</ta>
            <ta e="T360" id="Seg_11791" s="T359">v</ta>
            <ta e="T361" id="Seg_11792" s="T360">pers</ta>
            <ta e="T362" id="Seg_11793" s="T361">n</ta>
            <ta e="T363" id="Seg_11794" s="T362">adv</ta>
            <ta e="T364" id="Seg_11795" s="T363">n</ta>
            <ta e="T365" id="Seg_11796" s="T364">ptcl</ta>
            <ta e="T366" id="Seg_11797" s="T365">n</ta>
            <ta e="T369" id="Seg_11798" s="T368">n</ta>
            <ta e="T371" id="Seg_11799" s="T370">v</ta>
            <ta e="T372" id="Seg_11800" s="T371">v</ta>
            <ta e="T373" id="Seg_11801" s="T372">ptcl</ta>
            <ta e="T374" id="Seg_11802" s="T373">v</ta>
            <ta e="T375" id="Seg_11803" s="T374">ptcl</ta>
            <ta e="T376" id="Seg_11804" s="T375">propr</ta>
            <ta e="T377" id="Seg_11805" s="T376">adv</ta>
            <ta e="T378" id="Seg_11806" s="T377">v</ta>
            <ta e="T379" id="Seg_11807" s="T378">adv</ta>
            <ta e="T380" id="Seg_11808" s="T379">v</ta>
            <ta e="T381" id="Seg_11809" s="T380">conj</ta>
            <ta e="T382" id="Seg_11810" s="T381">dempro</ta>
            <ta e="T383" id="Seg_11811" s="T382">adv</ta>
            <ta e="T384" id="Seg_11812" s="T383">ptcl</ta>
            <ta e="T385" id="Seg_11813" s="T384">v</ta>
            <ta e="T386" id="Seg_11814" s="T385">que</ta>
            <ta e="T387" id="Seg_11815" s="T386">ptcl</ta>
            <ta e="T388" id="Seg_11816" s="T387">v</ta>
            <ta e="T394" id="Seg_11817" s="T393">dempro</ta>
            <ta e="T395" id="Seg_11818" s="T394">ptcl</ta>
            <ta e="T396" id="Seg_11819" s="T395">n</ta>
            <ta e="T397" id="Seg_11820" s="T396">n</ta>
            <ta e="T398" id="Seg_11821" s="T397">v</ta>
            <ta e="T399" id="Seg_11822" s="T398">v</ta>
            <ta e="T400" id="Seg_11823" s="T399">v</ta>
            <ta e="T401" id="Seg_11824" s="T400">n</ta>
            <ta e="T402" id="Seg_11825" s="T401">conj</ta>
            <ta e="T403" id="Seg_11826" s="T402">dempro</ta>
            <ta e="T404" id="Seg_11827" s="T403">que</ta>
            <ta e="T405" id="Seg_11828" s="T404">ptcl</ta>
            <ta e="T406" id="Seg_11829" s="T405">v</ta>
            <ta e="T407" id="Seg_11830" s="T406">v</ta>
            <ta e="T408" id="Seg_11831" s="T407">pers</ta>
            <ta e="T409" id="Seg_11832" s="T408">v</ta>
            <ta e="T410" id="Seg_11833" s="T409">ptcl</ta>
            <ta e="T411" id="Seg_11834" s="T410">adj</ta>
            <ta e="T412" id="Seg_11835" s="T411">n</ta>
            <ta e="T413" id="Seg_11836" s="T412">pers</ta>
            <ta e="T414" id="Seg_11837" s="T413">ptcl</ta>
            <ta e="T415" id="Seg_11838" s="T414">ptcl</ta>
            <ta e="T416" id="Seg_11839" s="T415">v</ta>
            <ta e="T417" id="Seg_11840" s="T416">n</ta>
            <ta e="T418" id="Seg_11841" s="T417">v</ta>
            <ta e="T419" id="Seg_11842" s="T418">que</ta>
            <ta e="T420" id="Seg_11843" s="T419">pers</ta>
            <ta e="T421" id="Seg_11844" s="T420">ptcl</ta>
            <ta e="T422" id="Seg_11845" s="T421">v</ta>
            <ta e="T423" id="Seg_11846" s="T422">ptcl</ta>
            <ta e="T424" id="Seg_11847" s="T423">ptcl</ta>
            <ta e="T425" id="Seg_11848" s="T424">aux</ta>
            <ta e="T426" id="Seg_11849" s="T425">v</ta>
            <ta e="T427" id="Seg_11850" s="T426">pers</ta>
            <ta e="T428" id="Seg_11851" s="T427">v</ta>
            <ta e="T429" id="Seg_11852" s="T428">adv</ta>
            <ta e="T430" id="Seg_11853" s="T429">pers</ta>
            <ta e="T433" id="Seg_11854" s="T432">n</ta>
            <ta e="T434" id="Seg_11855" s="T433">v</ta>
            <ta e="T435" id="Seg_11856" s="T434">pers</ta>
            <ta e="T436" id="Seg_11857" s="T435">v</ta>
            <ta e="T437" id="Seg_11858" s="T436">dempro</ta>
            <ta e="T439" id="Seg_11859" s="T438">n</ta>
            <ta e="T440" id="Seg_11860" s="T439">conj</ta>
            <ta e="T441" id="Seg_11861" s="T440">pers</ta>
            <ta e="T442" id="Seg_11862" s="T441">adv</ta>
            <ta e="T443" id="Seg_11863" s="T442">n</ta>
            <ta e="T444" id="Seg_11864" s="T443">conj</ta>
            <ta e="T445" id="Seg_11865" s="T444">pers</ta>
            <ta e="T446" id="Seg_11866" s="T445">adv</ta>
            <ta e="T447" id="Seg_11867" s="T446">n</ta>
            <ta e="T448" id="Seg_11868" s="T447">pers</ta>
            <ta e="T450" id="Seg_11869" s="T449">pers</ta>
            <ta e="T451" id="Seg_11870" s="T450">v</ta>
            <ta e="T452" id="Seg_11871" s="T451">v</ta>
            <ta e="T453" id="Seg_11872" s="T452">n</ta>
            <ta e="T454" id="Seg_11873" s="T453">conj</ta>
            <ta e="T455" id="Seg_11874" s="T454">n</ta>
            <ta e="T456" id="Seg_11875" s="T455">v</ta>
            <ta e="T457" id="Seg_11876" s="T456">dempro</ta>
            <ta e="T458" id="Seg_11877" s="T457">v</ta>
            <ta e="T459" id="Seg_11878" s="T458">ptcl</ta>
            <ta e="T460" id="Seg_11879" s="T459">ptcl</ta>
            <ta e="T461" id="Seg_11880" s="T460">v</ta>
            <ta e="T462" id="Seg_11881" s="T461">adv</ta>
            <ta e="T463" id="Seg_11882" s="T462">pers</ta>
            <ta e="T464" id="Seg_11883" s="T463">v</ta>
            <ta e="T465" id="Seg_11884" s="T464">conj</ta>
            <ta e="T467" id="Seg_11885" s="T466">v</ta>
            <ta e="T469" id="Seg_11886" s="T468">n</ta>
            <ta e="T470" id="Seg_11887" s="T469">dempro</ta>
            <ta e="T471" id="Seg_11888" s="T470">adv</ta>
            <ta e="T472" id="Seg_11889" s="T471">n</ta>
            <ta e="T473" id="Seg_11890" s="T472">v</ta>
            <ta e="T474" id="Seg_11891" s="T473">adv</ta>
            <ta e="T475" id="Seg_11892" s="T474">pers</ta>
            <ta e="T476" id="Seg_11893" s="T475">dempro</ta>
            <ta e="T478" id="Seg_11894" s="T477">adv</ta>
            <ta e="T479" id="Seg_11895" s="T478">v</ta>
            <ta e="T480" id="Seg_11896" s="T479">n</ta>
            <ta e="T481" id="Seg_11897" s="T480">v</ta>
            <ta e="T482" id="Seg_11898" s="T481">adv</ta>
            <ta e="T483" id="Seg_11899" s="T482">v</ta>
            <ta e="T484" id="Seg_11900" s="T483">n</ta>
            <ta e="T485" id="Seg_11901" s="T484">conj</ta>
            <ta e="T486" id="Seg_11902" s="T485">adv</ta>
            <ta e="T487" id="Seg_11903" s="T486">v</ta>
            <ta e="T488" id="Seg_11904" s="T487">adv</ta>
            <ta e="T489" id="Seg_11905" s="T488">pers</ta>
            <ta e="T490" id="Seg_11906" s="T489">n</ta>
            <ta e="T491" id="Seg_11907" s="T490">v</ta>
            <ta e="T492" id="Seg_11908" s="T491">conj</ta>
            <ta e="T493" id="Seg_11909" s="T492">adv</ta>
            <ta e="T494" id="Seg_11910" s="T493">adv</ta>
            <ta e="T495" id="Seg_11911" s="T494">v</ta>
            <ta e="T496" id="Seg_11912" s="T495">n</ta>
            <ta e="T498" id="Seg_11913" s="T497">v</ta>
            <ta e="T499" id="Seg_11914" s="T498">v</ta>
            <ta e="T500" id="Seg_11915" s="T499">dempro</ta>
            <ta e="T501" id="Seg_11916" s="T500">ptcl</ta>
            <ta e="T502" id="Seg_11917" s="T501">adv</ta>
            <ta e="T503" id="Seg_11918" s="T502">n</ta>
            <ta e="T504" id="Seg_11919" s="T503">pers</ta>
            <ta e="T505" id="Seg_11920" s="T504">n</ta>
            <ta e="T506" id="Seg_11921" s="T505">v</ta>
            <ta e="T508" id="Seg_11922" s="T507">pers</ta>
            <ta e="T509" id="Seg_11923" s="T508">adv</ta>
            <ta e="T510" id="Seg_11924" s="T509">pers</ta>
            <ta e="T511" id="Seg_11925" s="T510">ptcl</ta>
            <ta e="T512" id="Seg_11926" s="T511">n</ta>
            <ta e="T514" id="Seg_11927" s="T513">v</ta>
            <ta e="T515" id="Seg_11928" s="T514">adv</ta>
            <ta e="T516" id="Seg_11929" s="T515">pers</ta>
            <ta e="T517" id="Seg_11930" s="T516">v</ta>
            <ta e="T518" id="Seg_11931" s="T517">n</ta>
            <ta e="T519" id="Seg_11932" s="T518">v</ta>
            <ta e="T520" id="Seg_11933" s="T519">adv</ta>
            <ta e="T521" id="Seg_11934" s="T520">v</ta>
            <ta e="T523" id="Seg_11935" s="T522">propr</ta>
            <ta e="T525" id="Seg_11936" s="T524">n</ta>
            <ta e="T526" id="Seg_11937" s="T525">v</ta>
            <ta e="T527" id="Seg_11938" s="T526">v</ta>
            <ta e="T528" id="Seg_11939" s="T527">n</ta>
            <ta e="T529" id="Seg_11940" s="T528">v</ta>
            <ta e="T530" id="Seg_11941" s="T529">n</ta>
            <ta e="T531" id="Seg_11942" s="T530">v</ta>
            <ta e="T532" id="Seg_11943" s="T531">adv</ta>
            <ta e="T533" id="Seg_11944" s="T532">adv</ta>
            <ta e="T534" id="Seg_11945" s="T533">n</ta>
            <ta e="T535" id="Seg_11946" s="T534">v</ta>
            <ta e="T536" id="Seg_11947" s="T535">propr</ta>
            <ta e="T537" id="Seg_11948" s="T536">n</ta>
            <ta e="T538" id="Seg_11949" s="T537">adj</ta>
            <ta e="T540" id="Seg_11950" s="T539">conj</ta>
            <ta e="T541" id="Seg_11951" s="T540">adj</ta>
            <ta e="T542" id="Seg_11952" s="T541">refl</ta>
            <ta e="T543" id="Seg_11953" s="T542">adj</ta>
            <ta e="T544" id="Seg_11954" s="T543">adv</ta>
            <ta e="T545" id="Seg_11955" s="T544">pers</ta>
            <ta e="T546" id="Seg_11956" s="T545">n</ta>
            <ta e="T547" id="Seg_11957" s="T546">v</ta>
            <ta e="T548" id="Seg_11958" s="T547">conj</ta>
            <ta e="T549" id="Seg_11959" s="T548">v</ta>
            <ta e="T550" id="Seg_11960" s="T549">pers</ta>
            <ta e="T551" id="Seg_11961" s="T550">v</ta>
            <ta e="T552" id="Seg_11962" s="T551">pers</ta>
            <ta e="T553" id="Seg_11963" s="T552">v</ta>
            <ta e="T554" id="Seg_11964" s="T553">pers</ta>
            <ta e="T555" id="Seg_11965" s="T554">n</ta>
            <ta e="T558" id="Seg_11966" s="T557">ptcl</ta>
            <ta e="T559" id="Seg_11967" s="T558">pers</ta>
            <ta e="T560" id="Seg_11968" s="T559">adj</ta>
            <ta e="T561" id="Seg_11969" s="T560">ptcl</ta>
            <ta e="T562" id="Seg_11970" s="T561">pers</ta>
            <ta e="T563" id="Seg_11971" s="T562">pers</ta>
            <ta e="T564" id="Seg_11972" s="T563">ptcl</ta>
            <ta e="T565" id="Seg_11973" s="T564">v</ta>
            <ta e="T566" id="Seg_11974" s="T565">conj</ta>
            <ta e="T567" id="Seg_11975" s="T566">n</ta>
            <ta e="T568" id="Seg_11976" s="T567">adv</ta>
            <ta e="T569" id="Seg_11977" s="T568">n</ta>
            <ta e="T570" id="Seg_11978" s="T569">v</ta>
            <ta e="T571" id="Seg_11979" s="T570">v</ta>
            <ta e="T572" id="Seg_11980" s="T571">dempro</ta>
            <ta e="T573" id="Seg_11981" s="T572">n</ta>
            <ta e="T574" id="Seg_11982" s="T573">quant</ta>
            <ta e="T575" id="Seg_11983" s="T574">adv</ta>
            <ta e="T577" id="Seg_11984" s="T576">adj</ta>
            <ta e="T578" id="Seg_11985" s="T577">n</ta>
            <ta e="T579" id="Seg_11986" s="T578">conj</ta>
            <ta e="T580" id="Seg_11987" s="T579">pers</ta>
            <ta e="T581" id="Seg_11988" s="T580">v</ta>
            <ta e="T582" id="Seg_11989" s="T581">ptcl</ta>
            <ta e="T583" id="Seg_11990" s="T582">conj</ta>
            <ta e="T584" id="Seg_11991" s="T583">v</ta>
            <ta e="T585" id="Seg_11992" s="T584">conj</ta>
            <ta e="T586" id="Seg_11993" s="T585">v</ta>
            <ta e="T587" id="Seg_11994" s="T586">dempro</ta>
            <ta e="T588" id="Seg_11995" s="T587">num</ta>
            <ta e="T589" id="Seg_11996" s="T588">n</ta>
            <ta e="T590" id="Seg_11997" s="T589">v</ta>
            <ta e="T591" id="Seg_11998" s="T590">v</ta>
            <ta e="T592" id="Seg_11999" s="T591">conj</ta>
            <ta e="T593" id="Seg_12000" s="T592">pers</ta>
            <ta e="T594" id="Seg_12001" s="T593">ptcl</ta>
            <ta e="T595" id="Seg_12002" s="T594">v</ta>
            <ta e="T596" id="Seg_12003" s="T595">conj</ta>
            <ta e="T597" id="Seg_12004" s="T596">n</ta>
            <ta e="T598" id="Seg_12005" s="T597">ptcl</ta>
            <ta e="T599" id="Seg_12006" s="T598">v</ta>
            <ta e="T600" id="Seg_12007" s="T599">dempro</ta>
            <ta e="T601" id="Seg_12008" s="T600">ptcl</ta>
            <ta e="T602" id="Seg_12009" s="T601">adj</ta>
            <ta e="T603" id="Seg_12010" s="T602">n</ta>
            <ta e="T604" id="Seg_12011" s="T603">n</ta>
            <ta e="T605" id="Seg_12012" s="T604">ptcl</ta>
            <ta e="T607" id="Seg_12013" s="T606">adj</ta>
            <ta e="T609" id="Seg_12014" s="T608">n</ta>
            <ta e="T610" id="Seg_12015" s="T609">adj</ta>
            <ta e="T611" id="Seg_12016" s="T610">n</ta>
            <ta e="T612" id="Seg_12017" s="T611">ptcl</ta>
            <ta e="T613" id="Seg_12018" s="T612">v</ta>
            <ta e="T614" id="Seg_12019" s="T613">ptcl</ta>
            <ta e="T615" id="Seg_12020" s="T614">adj</ta>
            <ta e="T616" id="Seg_12021" s="T615">v</ta>
            <ta e="T617" id="Seg_12022" s="T616">n</ta>
            <ta e="T618" id="Seg_12023" s="T617">ptcl</ta>
            <ta e="T619" id="Seg_12024" s="T618">v</ta>
            <ta e="T620" id="Seg_12025" s="T619">conj</ta>
            <ta e="T621" id="Seg_12026" s="T620">n</ta>
            <ta e="T622" id="Seg_12027" s="T621">v</ta>
            <ta e="T623" id="Seg_12028" s="T622">v</ta>
            <ta e="T624" id="Seg_12029" s="T623">dempro</ta>
            <ta e="T625" id="Seg_12030" s="T624">n</ta>
            <ta e="T626" id="Seg_12031" s="T625">v</ta>
            <ta e="T627" id="Seg_12032" s="T626">conj</ta>
            <ta e="T628" id="Seg_12033" s="T627">n</ta>
            <ta e="T629" id="Seg_12034" s="T628">v</ta>
            <ta e="T630" id="Seg_12035" s="T629">conj</ta>
            <ta e="T631" id="Seg_12036" s="T630">n</ta>
            <ta e="T632" id="Seg_12037" s="T631">quant</ta>
            <ta e="T633" id="Seg_12038" s="T632">conj</ta>
            <ta e="T634" id="Seg_12039" s="T633">pers</ta>
            <ta e="T635" id="Seg_12040" s="T634">pers</ta>
            <ta e="T636" id="Seg_12041" s="T635">v</ta>
            <ta e="T637" id="Seg_12042" s="T636">pers</ta>
            <ta e="T638" id="Seg_12043" s="T637">n</ta>
            <ta e="T639" id="Seg_12044" s="T638">ptcl</ta>
            <ta e="T640" id="Seg_12045" s="T639">v</ta>
            <ta e="T641" id="Seg_12046" s="T640">conj</ta>
            <ta e="T642" id="Seg_12047" s="T641">n</ta>
            <ta e="T643" id="Seg_12048" s="T642">ptcl</ta>
            <ta e="T644" id="Seg_12049" s="T643">v</ta>
            <ta e="T645" id="Seg_12050" s="T644">ptcl</ta>
            <ta e="T646" id="Seg_12051" s="T645">pers</ta>
            <ta e="T647" id="Seg_12052" s="T646">ptcl</ta>
            <ta e="T648" id="Seg_12053" s="T647">pers</ta>
            <ta e="T649" id="Seg_12054" s="T648">pers</ta>
            <ta e="T650" id="Seg_12055" s="T649">adv</ta>
            <ta e="T651" id="Seg_12056" s="T650">ptcl</ta>
            <ta e="T652" id="Seg_12057" s="T651">pers</ta>
            <ta e="T653" id="Seg_12058" s="T652">v</ta>
            <ta e="T654" id="Seg_12059" s="T653">n</ta>
            <ta e="T655" id="Seg_12060" s="T654">v</ta>
            <ta e="T656" id="Seg_12061" s="T655">num</ta>
            <ta e="T657" id="Seg_12062" s="T656">n</ta>
            <ta e="T658" id="Seg_12063" s="T657">dempro</ta>
            <ta e="T659" id="Seg_12064" s="T658">n</ta>
            <ta e="T660" id="Seg_12065" s="T659">v</ta>
            <ta e="T661" id="Seg_12066" s="T660">dempro</ta>
            <ta e="T662" id="Seg_12067" s="T661">ptcl</ta>
            <ta e="T663" id="Seg_12068" s="T662">pers</ta>
            <ta e="T664" id="Seg_12069" s="T663">v</ta>
            <ta e="T665" id="Seg_12070" s="T664">n</ta>
            <ta e="T666" id="Seg_12071" s="T665">v</ta>
            <ta e="T667" id="Seg_12072" s="T666">v</ta>
            <ta e="T668" id="Seg_12073" s="T667">n</ta>
            <ta e="T669" id="Seg_12074" s="T668">dempro</ta>
            <ta e="T671" id="Seg_12075" s="T670">v</ta>
            <ta e="T672" id="Seg_12076" s="T671">conj</ta>
            <ta e="T673" id="Seg_12077" s="T672">pers</ta>
            <ta e="T674" id="Seg_12078" s="T673">v</ta>
            <ta e="T675" id="Seg_12079" s="T674">v</ta>
            <ta e="T676" id="Seg_12080" s="T675">conj</ta>
            <ta e="T677" id="Seg_12081" s="T676">ptcl</ta>
            <ta e="T678" id="Seg_12082" s="T677">v</ta>
            <ta e="T680" id="Seg_12083" s="T679">v</ta>
            <ta e="T682" id="Seg_12084" s="T681">aux</ta>
            <ta e="T683" id="Seg_12085" s="T682">v</ta>
            <ta e="T684" id="Seg_12086" s="T683">pers</ta>
            <ta e="T685" id="Seg_12087" s="T684">adv</ta>
            <ta e="T687" id="Seg_12088" s="T686">refl</ta>
            <ta e="T688" id="Seg_12089" s="T687">adj</ta>
            <ta e="T689" id="Seg_12090" s="T688">refl</ta>
            <ta e="T690" id="Seg_12091" s="T689">v</ta>
            <ta e="T691" id="Seg_12092" s="T690">que</ta>
            <ta e="T692" id="Seg_12093" s="T691">v</ta>
            <ta e="T693" id="Seg_12094" s="T692">adv</ta>
            <ta e="T694" id="Seg_12095" s="T693">dempro</ta>
            <ta e="T695" id="Seg_12096" s="T694">n</ta>
            <ta e="T697" id="Seg_12097" s="T696">v</ta>
            <ta e="T698" id="Seg_12098" s="T697">pers</ta>
            <ta e="T699" id="Seg_12099" s="T698">v</ta>
            <ta e="T700" id="Seg_12100" s="T699">conj</ta>
            <ta e="T701" id="Seg_12101" s="T700">ptcl</ta>
            <ta e="T702" id="Seg_12102" s="T701">v</ta>
            <ta e="T703" id="Seg_12103" s="T702">refl</ta>
            <ta e="T704" id="Seg_12104" s="T703">conj</ta>
            <ta e="T705" id="Seg_12105" s="T704">ptcl</ta>
            <ta e="T706" id="Seg_12106" s="T705">n</ta>
            <ta e="T707" id="Seg_12107" s="T706">v</ta>
            <ta e="T708" id="Seg_12108" s="T707">ptcl</ta>
            <ta e="T710" id="Seg_12109" s="T709">pers</ta>
            <ta e="T711" id="Seg_12110" s="T710">conj</ta>
            <ta e="T712" id="Seg_12111" s="T711">v</ta>
            <ta e="T713" id="Seg_12112" s="T712">adj</ta>
            <ta e="T714" id="Seg_12113" s="T713">v</ta>
            <ta e="T715" id="Seg_12114" s="T714">adv</ta>
            <ta e="T716" id="Seg_12115" s="T715">dempro</ta>
            <ta e="T717" id="Seg_12116" s="T716">n</ta>
            <ta e="T718" id="Seg_12117" s="T717">v</ta>
            <ta e="T720" id="Seg_12118" s="T719">pers</ta>
            <ta e="T721" id="Seg_12119" s="T720">pers</ta>
            <ta e="T722" id="Seg_12120" s="T721">dempro</ta>
            <ta e="T723" id="Seg_12121" s="T722">v</ta>
            <ta e="T724" id="Seg_12122" s="T723">propr</ta>
            <ta e="T725" id="Seg_12123" s="T724">n</ta>
            <ta e="T726" id="Seg_12124" s="T725">v</ta>
            <ta e="T727" id="Seg_12125" s="T726">v</ta>
            <ta e="T730" id="Seg_12126" s="T729">n</ta>
            <ta e="T731" id="Seg_12127" s="T730">v</ta>
            <ta e="T732" id="Seg_12128" s="T731">n</ta>
            <ta e="T733" id="Seg_12129" s="T732">v</ta>
            <ta e="T734" id="Seg_12130" s="T733">n</ta>
            <ta e="T735" id="Seg_12131" s="T734">v</ta>
            <ta e="T736" id="Seg_12132" s="T735">v</ta>
            <ta e="T737" id="Seg_12133" s="T736">adv</ta>
            <ta e="T738" id="Seg_12134" s="T737">pers</ta>
            <ta e="T740" id="Seg_12135" s="T739">v</ta>
            <ta e="T741" id="Seg_12136" s="T740">n</ta>
            <ta e="T742" id="Seg_12137" s="T741">adv</ta>
            <ta e="T743" id="Seg_12138" s="T742">v</ta>
            <ta e="T744" id="Seg_12139" s="T743">num</ta>
            <ta e="T745" id="Seg_12140" s="T744">n</ta>
            <ta e="T746" id="Seg_12141" s="T745">adv</ta>
            <ta e="T747" id="Seg_12142" s="T746">n</ta>
            <ta e="T748" id="Seg_12143" s="T747">v</ta>
            <ta e="T749" id="Seg_12144" s="T748">dempro</ta>
            <ta e="T750" id="Seg_12145" s="T749">v</ta>
            <ta e="T751" id="Seg_12146" s="T750">n</ta>
            <ta e="T752" id="Seg_12147" s="T751">adv</ta>
            <ta e="T753" id="Seg_12148" s="T752">pers</ta>
            <ta e="T754" id="Seg_12149" s="T753">adj</ta>
            <ta e="T755" id="Seg_12150" s="T754">v</ta>
            <ta e="T756" id="Seg_12151" s="T755">n</ta>
            <ta e="T757" id="Seg_12152" s="T756">dempro</ta>
            <ta e="T758" id="Seg_12153" s="T757">adv</ta>
            <ta e="T759" id="Seg_12154" s="T758">dempro</ta>
            <ta e="T760" id="Seg_12155" s="T759">v</ta>
            <ta e="T761" id="Seg_12156" s="T760">n</ta>
            <ta e="T763" id="Seg_12157" s="T762">v</ta>
            <ta e="T764" id="Seg_12158" s="T763">adv</ta>
            <ta e="T765" id="Seg_12159" s="T764">adv</ta>
            <ta e="T767" id="Seg_12160" s="T766">pers</ta>
            <ta e="T768" id="Seg_12161" s="T767">adv</ta>
            <ta e="T769" id="Seg_12162" s="T768">adj</ta>
            <ta e="T770" id="Seg_12163" s="T769">v</ta>
            <ta e="T771" id="Seg_12164" s="T770">adv</ta>
            <ta e="T772" id="Seg_12165" s="T771">dempro</ta>
            <ta e="T774" id="Seg_12166" s="T773">dempro</ta>
            <ta e="T775" id="Seg_12167" s="T774">pers</ta>
            <ta e="T776" id="Seg_12168" s="T775">v</ta>
            <ta e="T777" id="Seg_12169" s="T776">conj</ta>
            <ta e="T778" id="Seg_12170" s="T777">dempro</ta>
            <ta e="T779" id="Seg_12171" s="T778">v</ta>
            <ta e="T780" id="Seg_12172" s="T779">adv</ta>
            <ta e="T781" id="Seg_12173" s="T780">num</ta>
            <ta e="T783" id="Seg_12174" s="T782">n</ta>
            <ta e="T784" id="Seg_12175" s="T783">v</ta>
            <ta e="T785" id="Seg_12176" s="T784">adv</ta>
            <ta e="T786" id="Seg_12177" s="T785">n</ta>
            <ta e="T787" id="Seg_12178" s="T786">v</ta>
            <ta e="T790" id="Seg_12179" s="T789">n</ta>
            <ta e="T791" id="Seg_12180" s="T790">conj</ta>
            <ta e="T792" id="Seg_12181" s="T791">n</ta>
            <ta e="T793" id="Seg_12182" s="T792">adv</ta>
            <ta e="T794" id="Seg_12183" s="T793">v</ta>
            <ta e="T795" id="Seg_12184" s="T794">n</ta>
            <ta e="T796" id="Seg_12185" s="T795">conj</ta>
            <ta e="T797" id="Seg_12186" s="T796">n</ta>
            <ta e="T798" id="Seg_12187" s="T797">pers</ta>
            <ta e="T799" id="Seg_12188" s="T798">adv</ta>
            <ta e="T801" id="Seg_12189" s="T800">v</ta>
            <ta e="T803" id="Seg_12190" s="T802">v</ta>
            <ta e="T804" id="Seg_12191" s="T803">adv</ta>
            <ta e="T805" id="Seg_12192" s="T804">n</ta>
            <ta e="T806" id="Seg_12193" s="T805">pers</ta>
            <ta e="T807" id="Seg_12194" s="T806">v</ta>
            <ta e="T808" id="Seg_12195" s="T807">pers</ta>
            <ta e="T809" id="Seg_12196" s="T808">v</ta>
            <ta e="T810" id="Seg_12197" s="T809">dempro</ta>
            <ta e="T811" id="Seg_12198" s="T810">v</ta>
            <ta e="T812" id="Seg_12199" s="T811">num</ta>
            <ta e="T813" id="Seg_12200" s="T812">num</ta>
            <ta e="T814" id="Seg_12201" s="T813">num</ta>
            <ta e="T815" id="Seg_12202" s="T814">num</ta>
            <ta e="T816" id="Seg_12203" s="T815">num</ta>
            <ta e="T817" id="Seg_12204" s="T816">num</ta>
            <ta e="T818" id="Seg_12205" s="T817">num</ta>
            <ta e="T819" id="Seg_12206" s="T818">n</ta>
            <ta e="T820" id="Seg_12207" s="T819">v</ta>
            <ta e="T821" id="Seg_12208" s="T820">num</ta>
            <ta e="T822" id="Seg_12209" s="T821">n</ta>
            <ta e="T823" id="Seg_12210" s="T822">v</ta>
            <ta e="T824" id="Seg_12211" s="T823">adv</ta>
            <ta e="T825" id="Seg_12212" s="T824">ptcl</ta>
            <ta e="T826" id="Seg_12213" s="T825">adj</ta>
            <ta e="T827" id="Seg_12214" s="T826">n</ta>
            <ta e="T828" id="Seg_12215" s="T827">v</ta>
            <ta e="T829" id="Seg_12216" s="T828">v</ta>
            <ta e="T830" id="Seg_12217" s="T829">v</ta>
            <ta e="T831" id="Seg_12218" s="T830">que</ta>
            <ta e="T832" id="Seg_12219" s="T831">que</ta>
            <ta e="T833" id="Seg_12220" s="T832">v</ta>
            <ta e="T834" id="Seg_12221" s="T833">ptcl</ta>
            <ta e="T835" id="Seg_12222" s="T834">n</ta>
            <ta e="T836" id="Seg_12223" s="T835">ptcl</ta>
            <ta e="T837" id="Seg_12224" s="T836">n</ta>
            <ta e="T838" id="Seg_12225" s="T837">v</ta>
            <ta e="T840" id="Seg_12226" s="T839">adv</ta>
            <ta e="T841" id="Seg_12227" s="T840">v</ta>
            <ta e="T842" id="Seg_12228" s="T841">adv</ta>
            <ta e="T843" id="Seg_12229" s="T842">n</ta>
            <ta e="T844" id="Seg_12230" s="T843">v</ta>
            <ta e="T845" id="Seg_12231" s="T844">adv</ta>
            <ta e="T846" id="Seg_12232" s="T845">pers</ta>
            <ta e="T847" id="Seg_12233" s="T846">n</ta>
            <ta e="T848" id="Seg_12234" s="T847">dempro</ta>
            <ta e="T849" id="Seg_12235" s="T848">v</ta>
            <ta e="T850" id="Seg_12236" s="T849">pers</ta>
            <ta e="T851" id="Seg_12237" s="T850">num</ta>
            <ta e="T852" id="Seg_12238" s="T851">n</ta>
            <ta e="T853" id="Seg_12239" s="T852">v</ta>
            <ta e="T854" id="Seg_12240" s="T853">adv</ta>
            <ta e="T855" id="Seg_12241" s="T854">v</ta>
            <ta e="T856" id="Seg_12242" s="T855">que</ta>
            <ta e="T858" id="Seg_12243" s="T856">ptcl</ta>
            <ta e="T859" id="Seg_12244" s="T858">n</ta>
            <ta e="T860" id="Seg_12245" s="T859">adv</ta>
            <ta e="T861" id="Seg_12246" s="T860">v</ta>
            <ta e="T862" id="Seg_12247" s="T861">n</ta>
            <ta e="T863" id="Seg_12248" s="T862">refl</ta>
            <ta e="T864" id="Seg_12249" s="T863">n</ta>
            <ta e="T865" id="Seg_12250" s="T864">adv</ta>
            <ta e="T866" id="Seg_12251" s="T865">num</ta>
            <ta e="T867" id="Seg_12252" s="T866">n</ta>
            <ta e="T868" id="Seg_12253" s="T867">ptcl</ta>
            <ta e="T869" id="Seg_12254" s="T868">pers</ta>
            <ta e="T870" id="Seg_12255" s="T869">v</ta>
            <ta e="T871" id="Seg_12256" s="T870">pers</ta>
            <ta e="T875" id="Seg_12257" s="T874">ptcl</ta>
            <ta e="T876" id="Seg_12258" s="T875">v</ta>
            <ta e="T877" id="Seg_12259" s="T876">n</ta>
            <ta e="T878" id="Seg_12260" s="T877">n</ta>
            <ta e="T879" id="Seg_12261" s="T878">adj</ta>
            <ta e="T880" id="Seg_12262" s="T879">ptcl</ta>
            <ta e="T881" id="Seg_12263" s="T880">v</ta>
            <ta e="T882" id="Seg_12264" s="T881">dempro</ta>
            <ta e="T883" id="Seg_12265" s="T882">v</ta>
            <ta e="T884" id="Seg_12266" s="T883">ptcl</ta>
            <ta e="T885" id="Seg_12267" s="T884">v</ta>
            <ta e="T886" id="Seg_12268" s="T885">pers</ta>
            <ta e="T1055" id="Seg_12269" s="T886">ptcl</ta>
            <ta e="T887" id="Seg_12270" s="T1055">v</ta>
            <ta e="T888" id="Seg_12271" s="T887">v</ta>
            <ta e="T889" id="Seg_12272" s="T888">adv</ta>
            <ta e="T890" id="Seg_12273" s="T889">v</ta>
            <ta e="T891" id="Seg_12274" s="T890">adv</ta>
            <ta e="T892" id="Seg_12275" s="T891">adv</ta>
            <ta e="T894" id="Seg_12276" s="T893">v</ta>
            <ta e="T895" id="Seg_12277" s="T894">v</ta>
            <ta e="T896" id="Seg_12278" s="T895">pers</ta>
            <ta e="T897" id="Seg_12279" s="T896">ptcl</ta>
            <ta e="T898" id="Seg_12280" s="T897">v</ta>
            <ta e="T899" id="Seg_12281" s="T898">pers</ta>
            <ta e="T900" id="Seg_12282" s="T899">adv</ta>
            <ta e="T901" id="Seg_12283" s="T900">ptcl</ta>
            <ta e="T902" id="Seg_12284" s="T901">v</ta>
            <ta e="T903" id="Seg_12285" s="T902">adv</ta>
            <ta e="T904" id="Seg_12286" s="T903">v</ta>
            <ta e="T905" id="Seg_12287" s="T904">adv</ta>
            <ta e="T906" id="Seg_12288" s="T905">adv</ta>
            <ta e="T907" id="Seg_12289" s="T906">pers</ta>
            <ta e="T908" id="Seg_12290" s="T907">v</ta>
            <ta e="T910" id="Seg_12291" s="T909">n</ta>
            <ta e="T911" id="Seg_12292" s="T910">v</ta>
            <ta e="T912" id="Seg_12293" s="T911">adv</ta>
            <ta e="T913" id="Seg_12294" s="T912">ptcl</ta>
            <ta e="T914" id="Seg_12295" s="T913">v</ta>
            <ta e="T915" id="Seg_12296" s="T914">conj</ta>
            <ta e="T916" id="Seg_12297" s="T915">ptcl</ta>
            <ta e="T917" id="Seg_12298" s="T916">v</ta>
            <ta e="T918" id="Seg_12299" s="T917">n</ta>
            <ta e="T920" id="Seg_12300" s="T919">n</ta>
            <ta e="T921" id="Seg_12301" s="T920">conj</ta>
            <ta e="T1058" id="Seg_12302" s="T921">v</ta>
            <ta e="T922" id="Seg_12303" s="T1058">v</ta>
            <ta e="T923" id="Seg_12304" s="T922">pers</ta>
            <ta e="T924" id="Seg_12305" s="T923">adv</ta>
            <ta e="T926" id="Seg_12306" s="T925">adv</ta>
            <ta e="T927" id="Seg_12307" s="T926">pers</ta>
            <ta e="T928" id="Seg_12308" s="T927">adv</ta>
            <ta e="T929" id="Seg_12309" s="T928">adv</ta>
            <ta e="T930" id="Seg_12310" s="T929">v</ta>
            <ta e="T931" id="Seg_12311" s="T930">v</ta>
            <ta e="T932" id="Seg_12312" s="T931">ptcl</ta>
            <ta e="T933" id="Seg_12313" s="T932">num</ta>
            <ta e="T934" id="Seg_12314" s="T933">num</ta>
            <ta e="T935" id="Seg_12315" s="T934">n</ta>
            <ta e="T936" id="Seg_12316" s="T935">v</ta>
            <ta e="T937" id="Seg_12317" s="T936">conj</ta>
            <ta e="T938" id="Seg_12318" s="T937">dempro</ta>
            <ta e="T939" id="Seg_12319" s="T938">v</ta>
            <ta e="T940" id="Seg_12320" s="T939">adv</ta>
            <ta e="T941" id="Seg_12321" s="T940">pers</ta>
            <ta e="T942" id="Seg_12322" s="T941">dempro</ta>
            <ta e="T943" id="Seg_12323" s="T942">n</ta>
            <ta e="T944" id="Seg_12324" s="T943">ptcl</ta>
            <ta e="T945" id="Seg_12325" s="T944">n</ta>
            <ta e="T946" id="Seg_12326" s="T945">n</ta>
            <ta e="T947" id="Seg_12327" s="T946">v</ta>
            <ta e="T948" id="Seg_12328" s="T947">n</ta>
            <ta e="T950" id="Seg_12329" s="T949">v</ta>
            <ta e="T952" id="Seg_12330" s="T951">v</ta>
            <ta e="T954" id="Seg_12331" s="T953">n</ta>
            <ta e="T955" id="Seg_12332" s="T954">conj</ta>
            <ta e="T956" id="Seg_12333" s="T955">n</ta>
            <ta e="T957" id="Seg_12334" s="T956">v</ta>
            <ta e="T958" id="Seg_12335" s="T957">n</ta>
            <ta e="T959" id="Seg_12336" s="T958">v</ta>
            <ta e="T960" id="Seg_12337" s="T959">n</ta>
            <ta e="T961" id="Seg_12338" s="T960">conj</ta>
            <ta e="T962" id="Seg_12339" s="T961">pers</ta>
            <ta e="T963" id="Seg_12340" s="T962">n</ta>
            <ta e="T964" id="Seg_12341" s="T963">dempro</ta>
            <ta e="T965" id="Seg_12342" s="T964">v</ta>
            <ta e="T966" id="Seg_12343" s="T965">adv</ta>
            <ta e="T967" id="Seg_12344" s="T966">n</ta>
            <ta e="T969" id="Seg_12345" s="T968">v</ta>
            <ta e="T971" id="Seg_12346" s="T970">v</ta>
            <ta e="T972" id="Seg_12347" s="T971">conj</ta>
            <ta e="T974" id="Seg_12348" s="T973">v</ta>
            <ta e="T975" id="Seg_12349" s="T974">conj</ta>
            <ta e="T976" id="Seg_12350" s="T975">pers</ta>
            <ta e="T978" id="Seg_12351" s="T977">adj</ta>
            <ta e="T979" id="Seg_12352" s="T978">n</ta>
            <ta e="T980" id="Seg_12353" s="T979">v</ta>
            <ta e="T981" id="Seg_12354" s="T980">conj</ta>
            <ta e="T982" id="Seg_12355" s="T981">n</ta>
            <ta e="T983" id="Seg_12356" s="T982">v</ta>
            <ta e="T984" id="Seg_12357" s="T983">conj</ta>
            <ta e="T985" id="Seg_12358" s="T984">dempro</ta>
            <ta e="T986" id="Seg_12359" s="T985">ptcl</ta>
            <ta e="T987" id="Seg_12360" s="T986">pers</ta>
            <ta e="T988" id="Seg_12361" s="T987">v</ta>
            <ta e="T989" id="Seg_12362" s="T988">n</ta>
            <ta e="T990" id="Seg_12363" s="T989">v</ta>
            <ta e="T991" id="Seg_12364" s="T990">conj</ta>
            <ta e="T992" id="Seg_12365" s="T991">ptcl</ta>
            <ta e="T993" id="Seg_12366" s="T992">v</ta>
            <ta e="T994" id="Seg_12367" s="T993">pers</ta>
            <ta e="T995" id="Seg_12368" s="T994">dempro</ta>
            <ta e="T996" id="Seg_12369" s="T995">v</ta>
            <ta e="T997" id="Seg_12370" s="T996">pers</ta>
            <ta e="T998" id="Seg_12371" s="T997">refl</ta>
            <ta e="T1000" id="Seg_12372" s="T999">n</ta>
            <ta e="T1001" id="Seg_12373" s="T1000">n</ta>
            <ta e="T1002" id="Seg_12374" s="T1001">v</ta>
            <ta e="T1003" id="Seg_12375" s="T1002">conj</ta>
            <ta e="T1004" id="Seg_12376" s="T1003">n</ta>
            <ta e="T1005" id="Seg_12377" s="T1004">v</ta>
            <ta e="T1006" id="Seg_12378" s="T1005">n</ta>
            <ta e="T1007" id="Seg_12379" s="T1006">n</ta>
            <ta e="T1008" id="Seg_12380" s="T1007">conj</ta>
            <ta e="T1009" id="Seg_12381" s="T1008">n</ta>
            <ta e="T1010" id="Seg_12382" s="T1009">v</ta>
            <ta e="T1011" id="Seg_12383" s="T1010">n</ta>
            <ta e="T1012" id="Seg_12384" s="T1011">v</ta>
            <ta e="T1013" id="Seg_12385" s="T1012">n</ta>
            <ta e="T1014" id="Seg_12386" s="T1013">v</ta>
            <ta e="T1015" id="Seg_12387" s="T1014">dempro</ta>
            <ta e="T1016" id="Seg_12388" s="T1015">v</ta>
            <ta e="T1017" id="Seg_12389" s="T1016">adv</ta>
            <ta e="T1056" id="Seg_12390" s="T1017">adj</ta>
            <ta e="T1019" id="Seg_12391" s="T1018">v</ta>
            <ta e="T1021" id="Seg_12392" s="T1020">n</ta>
            <ta e="T1022" id="Seg_12393" s="T1021">ptcl</ta>
            <ta e="T1023" id="Seg_12394" s="T1022">v</ta>
            <ta e="T1024" id="Seg_12395" s="T1023">adv</ta>
            <ta e="T1025" id="Seg_12396" s="T1024">adj</ta>
            <ta e="T1026" id="Seg_12397" s="T1025">n</ta>
            <ta e="T1027" id="Seg_12398" s="T1026">v</ta>
            <ta e="T1028" id="Seg_12399" s="T1027">adv</ta>
            <ta e="T1029" id="Seg_12400" s="T1028">pers</ta>
            <ta e="T1030" id="Seg_12401" s="T1029">n</ta>
            <ta e="T1033" id="Seg_12402" s="T1032">v</ta>
            <ta e="T1034" id="Seg_12403" s="T1033">n</ta>
            <ta e="T1035" id="Seg_12404" s="T1034">v</ta>
            <ta e="T1037" id="Seg_12405" s="T1036">v</ta>
            <ta e="T1038" id="Seg_12406" s="T1037">adv</ta>
            <ta e="T1039" id="Seg_12407" s="T1038">adj</ta>
            <ta e="T1040" id="Seg_12408" s="T1039">v</ta>
            <ta e="T1041" id="Seg_12409" s="T1040">n</ta>
            <ta e="T1042" id="Seg_12410" s="T1041">adv</ta>
            <ta e="T1043" id="Seg_12411" s="T1042">n</ta>
            <ta e="T1044" id="Seg_12412" s="T1043">v</ta>
            <ta e="T1045" id="Seg_12413" s="T1044">n</ta>
            <ta e="T1046" id="Seg_12414" s="T1045">v</ta>
            <ta e="T1047" id="Seg_12415" s="T1046">n</ta>
            <ta e="T1048" id="Seg_12416" s="T1047">v</ta>
            <ta e="T1049" id="Seg_12417" s="T1048">conj</ta>
            <ta e="T1050" id="Seg_12418" s="T1049">n</ta>
            <ta e="T1051" id="Seg_12419" s="T1050">n</ta>
            <ta e="T1052" id="Seg_12420" s="T1051">v</ta>
            <ta e="T1053" id="Seg_12421" s="T1052">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_12422" s="T0">pro.h:A</ta>
            <ta e="T2" id="Seg_12423" s="T1">adv:Time</ta>
            <ta e="T4" id="Seg_12424" s="T3">np:Th</ta>
            <ta e="T7" id="Seg_12425" s="T6">0.2.h:A</ta>
            <ta e="T9" id="Seg_12426" s="T8">0.1.h:A</ta>
            <ta e="T14" id="Seg_12427" s="T13">np:Th</ta>
            <ta e="T15" id="Seg_12428" s="T14">0.2.h:A</ta>
            <ta e="T16" id="Seg_12429" s="T15">0.2.h:A</ta>
            <ta e="T21" id="Seg_12430" s="T20">0.1.h:A</ta>
            <ta e="T23" id="Seg_12431" s="T22">0.1.h:A</ta>
            <ta e="T26" id="Seg_12432" s="T25">np:P</ta>
            <ta e="T27" id="Seg_12433" s="T26">0.1.h:A</ta>
            <ta e="T29" id="Seg_12434" s="T28">np:P</ta>
            <ta e="T30" id="Seg_12435" s="T29">0.3.h:A</ta>
            <ta e="T32" id="Seg_12436" s="T31">np:P</ta>
            <ta e="T33" id="Seg_12437" s="T32">0.1.h:A</ta>
            <ta e="T35" id="Seg_12438" s="T34">np:P</ta>
            <ta e="T36" id="Seg_12439" s="T35">0.1.h:A</ta>
            <ta e="T38" id="Seg_12440" s="T37">pro.h:Poss</ta>
            <ta e="T39" id="Seg_12441" s="T38">np:L</ta>
            <ta e="T40" id="Seg_12442" s="T39">np.h:E</ta>
            <ta e="T42" id="Seg_12443" s="T41">pro.h:Poss</ta>
            <ta e="T43" id="Seg_12444" s="T42">np.h:A</ta>
            <ta e="T44" id="Seg_12445" s="T43">pro:G</ta>
            <ta e="T46" id="Seg_12446" s="T45">np:G</ta>
            <ta e="T47" id="Seg_12447" s="T46">0.3.h:A</ta>
            <ta e="T49" id="Seg_12448" s="T48">np:G</ta>
            <ta e="T50" id="Seg_12449" s="T49">0.3.h:A</ta>
            <ta e="T51" id="Seg_12450" s="T50">pro.h:A</ta>
            <ta e="T54" id="Seg_12451" s="T53">pro:Th</ta>
            <ta e="T56" id="Seg_12452" s="T55">pro.h:R</ta>
            <ta e="T57" id="Seg_12453" s="T56">np.h:A</ta>
            <ta e="T60" id="Seg_12454" s="T59">pro.h:A</ta>
            <ta e="T61" id="Seg_12455" s="T60">pro.h:Th</ta>
            <ta e="T65" id="Seg_12456" s="T64">0.2.h:A</ta>
            <ta e="T66" id="Seg_12457" s="T65">adv:Pth</ta>
            <ta e="T67" id="Seg_12458" s="T66">adv:Time</ta>
            <ta e="T68" id="Seg_12459" s="T67">adv:Time</ta>
            <ta e="T69" id="Seg_12460" s="T68">0.3.h:A</ta>
            <ta e="T70" id="Seg_12461" s="T69">np:Th</ta>
            <ta e="T71" id="Seg_12462" s="T70">np:Ins</ta>
            <ta e="T73" id="Seg_12463" s="T72">pro.h:A</ta>
            <ta e="T74" id="Seg_12464" s="T73">np:Th</ta>
            <ta e="T77" id="Seg_12465" s="T76">np:P</ta>
            <ta e="T78" id="Seg_12466" s="T77">0.3.h:A</ta>
            <ta e="T79" id="Seg_12467" s="T78">np:Th</ta>
            <ta e="T80" id="Seg_12468" s="T79">0.3.h:A</ta>
            <ta e="T81" id="Seg_12469" s="T80">pro.h:A</ta>
            <ta e="T83" id="Seg_12470" s="T82">np:Th</ta>
            <ta e="T84" id="Seg_12471" s="T83">pro.h:Poss</ta>
            <ta e="T85" id="Seg_12472" s="T84">np:L</ta>
            <ta e="T90" id="Seg_12473" s="T89">0.3.h:A</ta>
            <ta e="T91" id="Seg_12474" s="T90">np:Th</ta>
            <ta e="T93" id="Seg_12475" s="T92">pro.h:A</ta>
            <ta e="T96" id="Seg_12476" s="T95">pro:Th</ta>
            <ta e="T97" id="Seg_12477" s="T96">np:Th</ta>
            <ta e="T99" id="Seg_12478" s="T98">pro.h:A</ta>
            <ta e="T101" id="Seg_12479" s="T100">pro.h:A</ta>
            <ta e="T102" id="Seg_12480" s="T101">pro.h:P</ta>
            <ta e="T107" id="Seg_12481" s="T106">0.2.h:A</ta>
            <ta e="T109" id="Seg_12482" s="T108">adv:Time</ta>
            <ta e="T110" id="Seg_12483" s="T109">pro.h:R</ta>
            <ta e="T113" id="Seg_12484" s="T112">pro.h:Poss</ta>
            <ta e="T115" id="Seg_12485" s="T114">np.h:A</ta>
            <ta e="T116" id="Seg_12486" s="T115">0.2.h:A</ta>
            <ta e="T117" id="Seg_12487" s="T116">pro:G</ta>
            <ta e="T120" id="Seg_12488" s="T119">np.h:P</ta>
            <ta e="T124" id="Seg_12489" s="T123">0.3.h:A</ta>
            <ta e="T126" id="Seg_12490" s="T125">pro.h:A</ta>
            <ta e="T128" id="Seg_12491" s="T127">0.1.h:A</ta>
            <ta e="T132" id="Seg_12492" s="T131">pro.h:A</ta>
            <ta e="T135" id="Seg_12493" s="T134">np.h:R</ta>
            <ta e="T136" id="Seg_12494" s="T135">0.1.h:A</ta>
            <ta e="T137" id="Seg_12495" s="T136">0.1.h:A</ta>
            <ta e="T138" id="Seg_12496" s="T137">pro.h:A</ta>
            <ta e="T142" id="Seg_12497" s="T141">pro.h:A</ta>
            <ta e="T145" id="Seg_12498" s="T144">adv:L</ta>
            <ta e="T149" id="Seg_12499" s="T148">pro.h:B</ta>
            <ta e="T150" id="Seg_12500" s="T149">np:Th</ta>
            <ta e="T153" id="Seg_12501" s="T152">pro.h:A</ta>
            <ta e="T156" id="Seg_12502" s="T155">pro:P</ta>
            <ta e="T159" id="Seg_12503" s="T158">pro.h:E</ta>
            <ta e="T162" id="Seg_12504" s="T161">0.3.h:A</ta>
            <ta e="T163" id="Seg_12505" s="T162">np.h:Th</ta>
            <ta e="T166" id="Seg_12506" s="T165">0.1.h:E</ta>
            <ta e="T167" id="Seg_12507" s="T166">adv:Time</ta>
            <ta e="T168" id="Seg_12508" s="T167">np:E</ta>
            <ta e="T172" id="Seg_12509" s="T171">np:A</ta>
            <ta e="T173" id="Seg_12510" s="T172">0.2:A</ta>
            <ta e="T174" id="Seg_12511" s="T173">0.3:A</ta>
            <ta e="T176" id="Seg_12512" s="T175">0.3:A</ta>
            <ta e="T177" id="Seg_12513" s="T176">adv:Time</ta>
            <ta e="T179" id="Seg_12514" s="T178">0.3:P</ta>
            <ta e="T180" id="Seg_12515" s="T179">np:Th</ta>
            <ta e="T184" id="Seg_12516" s="T183">np:P</ta>
            <ta e="T186" id="Seg_12517" s="T185">adv:Time</ta>
            <ta e="T187" id="Seg_12518" s="T186">np:Th</ta>
            <ta e="T189" id="Seg_12519" s="T188">adv:Time</ta>
            <ta e="T190" id="Seg_12520" s="T189">np:P</ta>
            <ta e="T193" id="Seg_12521" s="T192">np:P</ta>
            <ta e="T195" id="Seg_12522" s="T194">np:P</ta>
            <ta e="T200" id="Seg_12523" s="T199">0.3:P</ta>
            <ta e="T202" id="Seg_12524" s="T201">adv:Time</ta>
            <ta e="T203" id="Seg_12525" s="T202">np:A</ta>
            <ta e="T204" id="Seg_12526" s="T203">np:A</ta>
            <ta e="T205" id="Seg_12527" s="T204">np:A</ta>
            <ta e="T207" id="Seg_12528" s="T206">np:P</ta>
            <ta e="T209" id="Seg_12529" s="T208">adv:Time</ta>
            <ta e="T210" id="Seg_12530" s="T209">np:P</ta>
            <ta e="T213" id="Seg_12531" s="T212">adv:Time</ta>
            <ta e="T214" id="Seg_12532" s="T213">pro:P</ta>
            <ta e="T216" id="Seg_12533" s="T215">np:Ins</ta>
            <ta e="T219" id="Seg_12534" s="T218">adv:Time</ta>
            <ta e="T220" id="Seg_12535" s="T219">np:L</ta>
            <ta e="T221" id="Seg_12536" s="T220">0.3:P</ta>
            <ta e="T225" id="Seg_12537" s="T224">np.h:A</ta>
            <ta e="T229" id="Seg_12538" s="T228">np:Th</ta>
            <ta e="T234" id="Seg_12539" s="T233">pro.h:A</ta>
            <ta e="T238" id="Seg_12540" s="T237">adv:Time</ta>
            <ta e="T239" id="Seg_12541" s="T238">n:Time</ta>
            <ta e="T240" id="Seg_12542" s="T239">0.1.h:A</ta>
            <ta e="T243" id="Seg_12543" s="T242">n:Time</ta>
            <ta e="T245" id="Seg_12544" s="T244">0.1.h:A</ta>
            <ta e="T246" id="Seg_12545" s="T245">np:Th</ta>
            <ta e="T247" id="Seg_12546" s="T246">0.1.h:A</ta>
            <ta e="T249" id="Seg_12547" s="T248">0.1.h:A</ta>
            <ta e="T250" id="Seg_12548" s="T249">0.1.h:A</ta>
            <ta e="T251" id="Seg_12549" s="T250">adv:Time</ta>
            <ta e="T252" id="Seg_12550" s="T251">0.1.h:A</ta>
            <ta e="T254" id="Seg_12551" s="T253">0.1.h:A</ta>
            <ta e="T256" id="Seg_12552" s="T255">np:P</ta>
            <ta e="T257" id="Seg_12553" s="T256">0.1.h:A</ta>
            <ta e="T258" id="Seg_12554" s="T257">pro.h:A</ta>
            <ta e="T260" id="Seg_12555" s="T259">np:G</ta>
            <ta e="T263" id="Seg_12556" s="T262">pro.h:Poss</ta>
            <ta e="T264" id="Seg_12557" s="T263">np:Th</ta>
            <ta e="T266" id="Seg_12558" s="T265">np:L</ta>
            <ta e="T270" id="Seg_12559" s="T269">np.h:R</ta>
            <ta e="T272" id="Seg_12560" s="T271">pro.h:A</ta>
            <ta e="T275" id="Seg_12561" s="T274">pro.h:A</ta>
            <ta e="T279" id="Seg_12562" s="T278">adv:Time</ta>
            <ta e="T281" id="Seg_12563" s="T280">0.3.h:A</ta>
            <ta e="T283" id="Seg_12564" s="T282">pro.h:Th</ta>
            <ta e="T288" id="Seg_12565" s="T287">pro.h:A</ta>
            <ta e="T291" id="Seg_12566" s="T290">adv:Time</ta>
            <ta e="T292" id="Seg_12567" s="T291">pro.h:A</ta>
            <ta e="T295" id="Seg_12568" s="T294">pro.h:A</ta>
            <ta e="T299" id="Seg_12569" s="T298">pro.h:E</ta>
            <ta e="T302" id="Seg_12570" s="T301">pro:Th</ta>
            <ta e="T304" id="Seg_12571" s="T303">pro.h:A</ta>
            <ta e="T309" id="Seg_12572" s="T308">pro.h:A</ta>
            <ta e="T310" id="Seg_12573" s="T309">pro:Th</ta>
            <ta e="T317" id="Seg_12574" s="T315">0.1.h:A</ta>
            <ta e="T318" id="Seg_12575" s="T317">pro.h:Poss</ta>
            <ta e="T322" id="Seg_12576" s="T321">pro.h:E</ta>
            <ta e="T323" id="Seg_12577" s="T322">pro:Th</ta>
            <ta e="T326" id="Seg_12578" s="T325">0.2.h:E</ta>
            <ta e="T327" id="Seg_12579" s="T326">pro:Th</ta>
            <ta e="T328" id="Seg_12580" s="T327">0.2.h:A</ta>
            <ta e="T330" id="Seg_12581" s="T329">np.h:A 0.1.h:Poss</ta>
            <ta e="T334" id="Seg_12582" s="T333">pro.h:Th</ta>
            <ta e="T337" id="Seg_12583" s="T336">pro.h:A</ta>
            <ta e="T340" id="Seg_12584" s="T339">pro.h:A</ta>
            <ta e="T344" id="Seg_12585" s="T343">0.1.h:A</ta>
            <ta e="T345" id="Seg_12586" s="T344">0.1.h:A</ta>
            <ta e="T348" id="Seg_12587" s="T347">np.h:Th 0.1.h:Poss</ta>
            <ta e="T350" id="Seg_12588" s="T349">0.1.h:E</ta>
            <ta e="T351" id="Seg_12589" s="T350">0.1.h:Poss</ta>
            <ta e="T352" id="Seg_12590" s="T351">0.1.h:Poss</ta>
            <ta e="T359" id="Seg_12591" s="T358">pro.h:A</ta>
            <ta e="T361" id="Seg_12592" s="T360">pro.h:Poss</ta>
            <ta e="T362" id="Seg_12593" s="T361">np.h:A</ta>
            <ta e="T363" id="Seg_12594" s="T362">adv:Time</ta>
            <ta e="T364" id="Seg_12595" s="T363">n:Time</ta>
            <ta e="T366" id="Seg_12596" s="T365">np:P</ta>
            <ta e="T369" id="Seg_12597" s="T368">np:Th</ta>
            <ta e="T371" id="Seg_12598" s="T370">0.3.h:A</ta>
            <ta e="T372" id="Seg_12599" s="T371">0.3.h:A</ta>
            <ta e="T374" id="Seg_12600" s="T373">0.3.h:A</ta>
            <ta e="T376" id="Seg_12601" s="T375">np.h:E</ta>
            <ta e="T380" id="Seg_12602" s="T379">0.3.h:A</ta>
            <ta e="T382" id="Seg_12603" s="T381">pro.h:A</ta>
            <ta e="T386" id="Seg_12604" s="T385">pro.h:Th</ta>
            <ta e="T388" id="Seg_12605" s="T387">0.3.h:A</ta>
            <ta e="T394" id="Seg_12606" s="T393">pro.h:Poss</ta>
            <ta e="T396" id="Seg_12607" s="T395">np.h:Th</ta>
            <ta e="T397" id="Seg_12608" s="T396">np.h:Th</ta>
            <ta e="T399" id="Seg_12609" s="T398">0.3.h:A</ta>
            <ta e="T400" id="Seg_12610" s="T399">0.2.h:A</ta>
            <ta e="T403" id="Seg_12611" s="T402">pro.h:A</ta>
            <ta e="T408" id="Seg_12612" s="T407">pro:G</ta>
            <ta e="T412" id="Seg_12613" s="T411">np.h:A</ta>
            <ta e="T413" id="Seg_12614" s="T412">pro:G</ta>
            <ta e="T417" id="Seg_12615" s="T416">np.h:A</ta>
            <ta e="T420" id="Seg_12616" s="T419">pro.h:A</ta>
            <ta e="T425" id="Seg_12617" s="T424">0.2.h:A</ta>
            <ta e="T427" id="Seg_12618" s="T426">pro.h:A</ta>
            <ta e="T429" id="Seg_12619" s="T428">adv:Time</ta>
            <ta e="T430" id="Seg_12620" s="T429">pro.h:Poss</ta>
            <ta e="T433" id="Seg_12621" s="T432">np.h:A</ta>
            <ta e="T435" id="Seg_12622" s="T434">pro.h:Th</ta>
            <ta e="T437" id="Seg_12623" s="T436">pro.h:B</ta>
            <ta e="T445" id="Seg_12624" s="T444">pro.h:B</ta>
            <ta e="T450" id="Seg_12625" s="T449">pro.h:A</ta>
            <ta e="T452" id="Seg_12626" s="T451">0.2.h:A</ta>
            <ta e="T453" id="Seg_12627" s="T452">np:G</ta>
            <ta e="T455" id="Seg_12628" s="T454">np:G</ta>
            <ta e="T456" id="Seg_12629" s="T455">0.2.h:A</ta>
            <ta e="T457" id="Seg_12630" s="T456">pro.h:A</ta>
            <ta e="T461" id="Seg_12631" s="T460">0.3.h:A</ta>
            <ta e="T462" id="Seg_12632" s="T461">adv:Time</ta>
            <ta e="T463" id="Seg_12633" s="T462">pro.h:R</ta>
            <ta e="T464" id="Seg_12634" s="T463">0.3.h:A</ta>
            <ta e="T467" id="Seg_12635" s="T466">0.1.h:A</ta>
            <ta e="T469" id="Seg_12636" s="T468">np:Ins</ta>
            <ta e="T470" id="Seg_12637" s="T469">pro:G</ta>
            <ta e="T471" id="Seg_12638" s="T470">adv:Time</ta>
            <ta e="T472" id="Seg_12639" s="T471">np.h:A 0.1.h:Poss</ta>
            <ta e="T474" id="Seg_12640" s="T473">adv:L</ta>
            <ta e="T475" id="Seg_12641" s="T474">pro:G</ta>
            <ta e="T476" id="Seg_12642" s="T475">pro.h:A</ta>
            <ta e="T478" id="Seg_12643" s="T477">adv:L</ta>
            <ta e="T480" id="Seg_12644" s="T479">np:P</ta>
            <ta e="T482" id="Seg_12645" s="T481">adv:Time</ta>
            <ta e="T483" id="Seg_12646" s="T482">0.3.h:A</ta>
            <ta e="T484" id="Seg_12647" s="T483">np:G</ta>
            <ta e="T486" id="Seg_12648" s="T485">adv:L</ta>
            <ta e="T487" id="Seg_12649" s="T486">0.3.h:P</ta>
            <ta e="T488" id="Seg_12650" s="T487">adv:Time</ta>
            <ta e="T489" id="Seg_12651" s="T488">pro.h:A</ta>
            <ta e="T490" id="Seg_12652" s="T489">np:G</ta>
            <ta e="T495" id="Seg_12653" s="T494">0.1.h:E</ta>
            <ta e="T496" id="Seg_12654" s="T495">np:E</ta>
            <ta e="T498" id="Seg_12655" s="T497">0.1.h:E</ta>
            <ta e="T499" id="Seg_12656" s="T498">0.1.h:E</ta>
            <ta e="T500" id="Seg_12657" s="T499">pro.h:B</ta>
            <ta e="T502" id="Seg_12658" s="T501">adv:Time</ta>
            <ta e="T503" id="Seg_12659" s="T502">0.1.h:Poss</ta>
            <ta e="T509" id="Seg_12660" s="T508">adv:Time</ta>
            <ta e="T510" id="Seg_12661" s="T509">pro.h:A</ta>
            <ta e="T512" id="Seg_12662" s="T511">np:Th</ta>
            <ta e="T515" id="Seg_12663" s="T514">adv:Time</ta>
            <ta e="T516" id="Seg_12664" s="T515">pro.h:A</ta>
            <ta e="T518" id="Seg_12665" s="T517">np:G</ta>
            <ta e="T519" id="Seg_12666" s="T518">0.1.h:E</ta>
            <ta e="T520" id="Seg_12667" s="T519">adv:Time</ta>
            <ta e="T521" id="Seg_12668" s="T520">0.1.h:A</ta>
            <ta e="T523" id="Seg_12669" s="T522">np:G</ta>
            <ta e="T526" id="Seg_12670" s="T525">0.1.h:A</ta>
            <ta e="T527" id="Seg_12671" s="T526">0.1.h:A</ta>
            <ta e="T528" id="Seg_12672" s="T527">np:Th</ta>
            <ta e="T529" id="Seg_12673" s="T528">0.1.h:A</ta>
            <ta e="T530" id="Seg_12674" s="T529">np:Th</ta>
            <ta e="T531" id="Seg_12675" s="T530">0.1.h:A</ta>
            <ta e="T532" id="Seg_12676" s="T531">adv:Time</ta>
            <ta e="T533" id="Seg_12677" s="T532">adv:L</ta>
            <ta e="T534" id="Seg_12678" s="T533">np.h:A</ta>
            <ta e="T537" id="Seg_12679" s="T536">np:Th</ta>
            <ta e="T542" id="Seg_12680" s="T541">pro.h:Th</ta>
            <ta e="T544" id="Seg_12681" s="T543">adv:Time</ta>
            <ta e="T545" id="Seg_12682" s="T544">pro.h:A</ta>
            <ta e="T546" id="Seg_12683" s="T545">np:G</ta>
            <ta e="T549" id="Seg_12684" s="T548">0.3.h:A</ta>
            <ta e="T550" id="Seg_12685" s="T549">pro:Th</ta>
            <ta e="T552" id="Seg_12686" s="T551">pro.h:A</ta>
            <ta e="T554" id="Seg_12687" s="T553">pro.h:Poss</ta>
            <ta e="T555" id="Seg_12688" s="T554">np.h:Th</ta>
            <ta e="T562" id="Seg_12689" s="T561">pro.h:E</ta>
            <ta e="T563" id="Seg_12690" s="T562">pro.h:Com</ta>
            <ta e="T567" id="Seg_12691" s="T566">np.h:Com</ta>
            <ta e="T568" id="Seg_12692" s="T567">adv:Time</ta>
            <ta e="T569" id="Seg_12693" s="T568">np.h:A</ta>
            <ta e="T571" id="Seg_12694" s="T570">0.2.h:A</ta>
            <ta e="T572" id="Seg_12695" s="T571">pro.h:Poss</ta>
            <ta e="T573" id="Seg_12696" s="T572">np:Th</ta>
            <ta e="T578" id="Seg_12697" s="T577">np.h:Th</ta>
            <ta e="T580" id="Seg_12698" s="T579">pro.h:Th</ta>
            <ta e="T584" id="Seg_12699" s="T583">0.2.h:A</ta>
            <ta e="T586" id="Seg_12700" s="T585">0.2.h:E</ta>
            <ta e="T587" id="Seg_12701" s="T586">pro.h:Com</ta>
            <ta e="T589" id="Seg_12702" s="T588">np.h:A</ta>
            <ta e="T593" id="Seg_12703" s="T592">pro.h:A</ta>
            <ta e="T599" id="Seg_12704" s="T598">0.1.h:A</ta>
            <ta e="T600" id="Seg_12705" s="T599">pro.h:Th</ta>
            <ta e="T604" id="Seg_12706" s="T603">np:Th</ta>
            <ta e="T609" id="Seg_12707" s="T608">np:Th</ta>
            <ta e="T611" id="Seg_12708" s="T610">np:A</ta>
            <ta e="T616" id="Seg_12709" s="T615">0.3.h:A</ta>
            <ta e="T617" id="Seg_12710" s="T616">np:E</ta>
            <ta e="T621" id="Seg_12711" s="T620">np.h:A 0.1.h:Poss</ta>
            <ta e="T623" id="Seg_12712" s="T622">0.2.h:A</ta>
            <ta e="T624" id="Seg_12713" s="T623">pro.h:Poss</ta>
            <ta e="T625" id="Seg_12714" s="T624">np:Th</ta>
            <ta e="T628" id="Seg_12715" s="T627">np:Th</ta>
            <ta e="T631" id="Seg_12716" s="T630">np:Th</ta>
            <ta e="T635" id="Seg_12717" s="T634">pro.h:A</ta>
            <ta e="T637" id="Seg_12718" s="T636">pro.h:E</ta>
            <ta e="T638" id="Seg_12719" s="T637">np:Com</ta>
            <ta e="T642" id="Seg_12720" s="T641">np:Com</ta>
            <ta e="T649" id="Seg_12721" s="T648">pro.h:B</ta>
            <ta e="T652" id="Seg_12722" s="T651">pro.h:A</ta>
            <ta e="T654" id="Seg_12723" s="T653">np:L</ta>
            <ta e="T657" id="Seg_12724" s="T656">np.h:A</ta>
            <ta e="T658" id="Seg_12725" s="T657">pro.h:Th</ta>
            <ta e="T659" id="Seg_12726" s="T658">np.h:A</ta>
            <ta e="T661" id="Seg_12727" s="T660">pro.h:A</ta>
            <ta e="T663" id="Seg_12728" s="T662">pro.h:Th</ta>
            <ta e="T665" id="Seg_12729" s="T664">np.h:A 0.1.h:Poss</ta>
            <ta e="T668" id="Seg_12730" s="T667">np:P</ta>
            <ta e="T669" id="Seg_12731" s="T668">pro.h:R</ta>
            <ta e="T673" id="Seg_12732" s="T672">pro.h:A</ta>
            <ta e="T680" id="Seg_12733" s="T679">0.2.h:A</ta>
            <ta e="T682" id="Seg_12734" s="T681">0.2.h:A</ta>
            <ta e="T684" id="Seg_12735" s="T683">pro.h:Th</ta>
            <ta e="T685" id="Seg_12736" s="T684">adv:Time</ta>
            <ta e="T689" id="Seg_12737" s="T688">pro.h:Th</ta>
            <ta e="T690" id="Seg_12738" s="T689">0.2.h:E</ta>
            <ta e="T691" id="Seg_12739" s="T690">pro:Th</ta>
            <ta e="T693" id="Seg_12740" s="T692">adv:Time</ta>
            <ta e="T695" id="Seg_12741" s="T694">np.h:A</ta>
            <ta e="T698" id="Seg_12742" s="T697">pro:L</ta>
            <ta e="T699" id="Seg_12743" s="T698">0.3.h:A</ta>
            <ta e="T703" id="Seg_12744" s="T702">pro.h:Th</ta>
            <ta e="T706" id="Seg_12745" s="T705">np:R</ta>
            <ta e="T709" id="Seg_12746" s="T708">0.1.h:A</ta>
            <ta e="T710" id="Seg_12747" s="T709">pro.h:Th</ta>
            <ta e="T714" id="Seg_12748" s="T713">0.1.h:E</ta>
            <ta e="T715" id="Seg_12749" s="T714">adv:Time</ta>
            <ta e="T716" id="Seg_12750" s="T715">pro.h:A</ta>
            <ta e="T717" id="Seg_12751" s="T716">np:Th</ta>
            <ta e="T721" id="Seg_12752" s="T720">pro.h:A</ta>
            <ta e="T722" id="Seg_12753" s="T721">pro.h:Com</ta>
            <ta e="T724" id="Seg_12754" s="T723">np:L</ta>
            <ta e="T725" id="Seg_12755" s="T724">np:Th</ta>
            <ta e="T726" id="Seg_12756" s="T725">0.1.h:A</ta>
            <ta e="T727" id="Seg_12757" s="T726">0.1.h:A</ta>
            <ta e="T730" id="Seg_12758" s="T729">np:Th</ta>
            <ta e="T731" id="Seg_12759" s="T730">0.1.h:A</ta>
            <ta e="T732" id="Seg_12760" s="T731">np.h:Th</ta>
            <ta e="T733" id="Seg_12761" s="T732">0.1.h:A</ta>
            <ta e="T734" id="Seg_12762" s="T733">np:P</ta>
            <ta e="T735" id="Seg_12763" s="T734">0.3.h:A</ta>
            <ta e="T736" id="Seg_12764" s="T735">0.3.h:A</ta>
            <ta e="T737" id="Seg_12765" s="T736">adv:Time</ta>
            <ta e="T738" id="Seg_12766" s="T737">pro.h:A</ta>
            <ta e="T741" id="Seg_12767" s="T740">np:G</ta>
            <ta e="T742" id="Seg_12768" s="T741">adv:Time</ta>
            <ta e="T746" id="Seg_12769" s="T745">adv:Time</ta>
            <ta e="T747" id="Seg_12770" s="T746">np:Th</ta>
            <ta e="T749" id="Seg_12771" s="T748">pro.h:Th</ta>
            <ta e="T750" id="Seg_12772" s="T749">0.3.h:A</ta>
            <ta e="T751" id="Seg_12773" s="T750">np:G</ta>
            <ta e="T752" id="Seg_12774" s="T751">adv:Time</ta>
            <ta e="T753" id="Seg_12775" s="T752">pro.h:Th</ta>
            <ta e="T756" id="Seg_12776" s="T755">np.h:Th</ta>
            <ta e="T757" id="Seg_12777" s="T756">pro.h:Th</ta>
            <ta e="T758" id="Seg_12778" s="T757">adv:Time</ta>
            <ta e="T759" id="Seg_12779" s="T758">pro.h:A</ta>
            <ta e="T761" id="Seg_12780" s="T760">np:So</ta>
            <ta e="T764" id="Seg_12781" s="T763">adv:Time</ta>
            <ta e="T766" id="Seg_12782" s="T765">0.1.h:E</ta>
            <ta e="T767" id="Seg_12783" s="T766">pro.h:Th</ta>
            <ta e="T771" id="Seg_12784" s="T770">adv:Time</ta>
            <ta e="T774" id="Seg_12785" s="T773">pro.h:A</ta>
            <ta e="T775" id="Seg_12786" s="T774">pro.h:Th</ta>
            <ta e="T778" id="Seg_12787" s="T777">pro.h:Th</ta>
            <ta e="T779" id="Seg_12788" s="T778">0.3.h:A</ta>
            <ta e="T783" id="Seg_12789" s="T782">np.h:P</ta>
            <ta e="T784" id="Seg_12790" s="T783">0.1.h:A</ta>
            <ta e="T785" id="Seg_12791" s="T784">adv:Time</ta>
            <ta e="T786" id="Seg_12792" s="T785">np:G</ta>
            <ta e="T787" id="Seg_12793" s="T786">0.1.h:A</ta>
            <ta e="T790" id="Seg_12794" s="T789">np:G</ta>
            <ta e="T792" id="Seg_12795" s="T791">np:G</ta>
            <ta e="T793" id="Seg_12796" s="T792">adv:Time</ta>
            <ta e="T794" id="Seg_12797" s="T793">0.3.h:P</ta>
            <ta e="T798" id="Seg_12798" s="T797">pro.h:Th</ta>
            <ta e="T803" id="Seg_12799" s="T802">0.1.h:E</ta>
            <ta e="T805" id="Seg_12800" s="T804">np.h:A</ta>
            <ta e="T806" id="Seg_12801" s="T805">pro.h:Th</ta>
            <ta e="T808" id="Seg_12802" s="T807">pro.h:A</ta>
            <ta e="T810" id="Seg_12803" s="T809">pro.h:Com</ta>
            <ta e="T811" id="Seg_12804" s="T810">0.1.h:Th</ta>
            <ta e="T820" id="Seg_12805" s="T819">0.1.h:E</ta>
            <ta e="T822" id="Seg_12806" s="T821">np.h:Th</ta>
            <ta e="T827" id="Seg_12807" s="T826">np:P</ta>
            <ta e="T828" id="Seg_12808" s="T827">0.3.h:A</ta>
            <ta e="T829" id="Seg_12809" s="T828">0.3.h:A</ta>
            <ta e="T830" id="Seg_12810" s="T829">0.3.h:A</ta>
            <ta e="T833" id="Seg_12811" s="T832">0.3.h:A</ta>
            <ta e="T837" id="Seg_12812" s="T836">np:P</ta>
            <ta e="T838" id="Seg_12813" s="T837">0.3.h:A</ta>
            <ta e="T841" id="Seg_12814" s="T840">0.3.h:A</ta>
            <ta e="T843" id="Seg_12815" s="T842">np:Th</ta>
            <ta e="T845" id="Seg_12816" s="T844">adv:Time</ta>
            <ta e="T846" id="Seg_12817" s="T845">pro.h:A</ta>
            <ta e="T848" id="Seg_12818" s="T847">pro.h:Th</ta>
            <ta e="T850" id="Seg_12819" s="T849">pro.h:Poss</ta>
            <ta e="T852" id="Seg_12820" s="T851">np.h:Th</ta>
            <ta e="T854" id="Seg_12821" s="T853">adv:Time</ta>
            <ta e="T855" id="Seg_12822" s="T854">0.1.h:A</ta>
            <ta e="T856" id="Seg_12823" s="T855">pro.h:R</ta>
            <ta e="T861" id="Seg_12824" s="T860">0.1.h:E</ta>
            <ta e="T862" id="Seg_12825" s="T861">np.h:Com</ta>
            <ta e="T863" id="Seg_12826" s="T862">pro.h:Poss</ta>
            <ta e="T864" id="Seg_12827" s="T863">np.h:Com</ta>
            <ta e="T865" id="Seg_12828" s="T864">adv:Time</ta>
            <ta e="T867" id="Seg_12829" s="T866">np.h:A</ta>
            <ta e="T869" id="Seg_12830" s="T868">pro.h:Th</ta>
            <ta e="T878" id="Seg_12831" s="T877">np:Th 0.3.h:Poss</ta>
            <ta e="T881" id="Seg_12832" s="T880">0.1.h:A</ta>
            <ta e="T883" id="Seg_12833" s="T882">0.3.h:A</ta>
            <ta e="T886" id="Seg_12834" s="T885">pro.h:Com</ta>
            <ta e="T887" id="Seg_12835" s="T1055">0.1.h:E</ta>
            <ta e="T888" id="Seg_12836" s="T887">0.1.h:E</ta>
            <ta e="T889" id="Seg_12837" s="T888">adv:Time</ta>
            <ta e="T890" id="Seg_12838" s="T889">0.1.h:A</ta>
            <ta e="T891" id="Seg_12839" s="T890">adv:Time</ta>
            <ta e="T894" id="Seg_12840" s="T893">0.1.h:A</ta>
            <ta e="T895" id="Seg_12841" s="T894">0.1.h:A</ta>
            <ta e="T896" id="Seg_12842" s="T895">pro.h:R</ta>
            <ta e="T898" id="Seg_12843" s="T897">0.1.h:E</ta>
            <ta e="T899" id="Seg_12844" s="T898">pro.h:Com</ta>
            <ta e="T900" id="Seg_12845" s="T899">adv:L</ta>
            <ta e="T904" id="Seg_12846" s="T903">0.3.h:E</ta>
            <ta e="T906" id="Seg_12847" s="T905">adv:Time</ta>
            <ta e="T907" id="Seg_12848" s="T906">pro.h:A</ta>
            <ta e="T910" id="Seg_12849" s="T909">np:P</ta>
            <ta e="T911" id="Seg_12850" s="T910">0.2.h:A</ta>
            <ta e="T912" id="Seg_12851" s="T911">adv:Time</ta>
            <ta e="T914" id="Seg_12852" s="T913">0.3.h:A</ta>
            <ta e="T918" id="Seg_12853" s="T917">np:P</ta>
            <ta e="T920" id="Seg_12854" s="T919">0.3.h:A</ta>
            <ta e="T922" id="Seg_12855" s="T1058">0.3.h:A</ta>
            <ta e="T923" id="Seg_12856" s="T922">pro.h:A</ta>
            <ta e="T926" id="Seg_12857" s="T925">adv:Time</ta>
            <ta e="T927" id="Seg_12858" s="T926">pro.h:E</ta>
            <ta e="T936" id="Seg_12859" s="T935">0.1.h:E</ta>
            <ta e="T938" id="Seg_12860" s="T937">pro.h:A</ta>
            <ta e="T940" id="Seg_12861" s="T939">adv:Time</ta>
            <ta e="T941" id="Seg_12862" s="T940">pro.h:A</ta>
            <ta e="T945" id="Seg_12863" s="T944">np:P</ta>
            <ta e="T946" id="Seg_12864" s="T945">np:Ins</ta>
            <ta e="T948" id="Seg_12865" s="T947">np:P</ta>
            <ta e="T952" id="Seg_12866" s="T951">0.1.h:A</ta>
            <ta e="T954" id="Seg_12867" s="T953">np:Ins</ta>
            <ta e="T956" id="Seg_12868" s="T955">np:P</ta>
            <ta e="T957" id="Seg_12869" s="T956">0.1.h:A</ta>
            <ta e="T958" id="Seg_12870" s="T957">np.h:A</ta>
            <ta e="T960" id="Seg_12871" s="T959">np:Ins</ta>
            <ta e="T962" id="Seg_12872" s="T961">pro.h:A</ta>
            <ta e="T963" id="Seg_12873" s="T962">np:P</ta>
            <ta e="T964" id="Seg_12874" s="T963">pro.h:B</ta>
            <ta e="T966" id="Seg_12875" s="T965">adv:Time</ta>
            <ta e="T967" id="Seg_12876" s="T966">np:Th</ta>
            <ta e="T969" id="Seg_12877" s="T968">0.3.h:A</ta>
            <ta e="T976" id="Seg_12878" s="T975">pro.h:A</ta>
            <ta e="T979" id="Seg_12879" s="T978">np:P</ta>
            <ta e="T982" id="Seg_12880" s="T981">np:P</ta>
            <ta e="T983" id="Seg_12881" s="T982">0.1.h:A</ta>
            <ta e="T985" id="Seg_12882" s="T984">pro.h:A</ta>
            <ta e="T987" id="Seg_12883" s="T986">pro.h:B</ta>
            <ta e="T989" id="Seg_12884" s="T988">np:Th</ta>
            <ta e="T990" id="Seg_12885" s="T989">0.3.h:A</ta>
            <ta e="T993" id="Seg_12886" s="T992">0.3.h:E</ta>
            <ta e="T994" id="Seg_12887" s="T993">pro.h:A</ta>
            <ta e="T995" id="Seg_12888" s="T994">pro.h:R</ta>
            <ta e="T997" id="Seg_12889" s="T996">pro.h:A</ta>
            <ta e="T1000" id="Seg_12890" s="T999">np.h:B</ta>
            <ta e="T1001" id="Seg_12891" s="T1000">np:P</ta>
            <ta e="T1006" id="Seg_12892" s="T1005">np:Poss</ta>
            <ta e="T1009" id="Seg_12893" s="T1008">np:P</ta>
            <ta e="T1010" id="Seg_12894" s="T1009">0.1.h:A</ta>
            <ta e="T1011" id="Seg_12895" s="T1010">np:P</ta>
            <ta e="T1012" id="Seg_12896" s="T1011">0.1.h:A</ta>
            <ta e="T1013" id="Seg_12897" s="T1012">np:P</ta>
            <ta e="T1014" id="Seg_12898" s="T1013">0.1.h:A</ta>
            <ta e="T1015" id="Seg_12899" s="T1014">pro.h:R</ta>
            <ta e="T1016" id="Seg_12900" s="T1015">0.1.h:A</ta>
            <ta e="T1019" id="Seg_12901" s="T1018">0.1.h:A</ta>
            <ta e="T1021" id="Seg_12902" s="T1020">np.h:A</ta>
            <ta e="T1026" id="Seg_12903" s="T1025">np.h:Th</ta>
            <ta e="T1027" id="Seg_12904" s="T1026">0.3.h:P</ta>
            <ta e="T1028" id="Seg_12905" s="T1027">adv:Time</ta>
            <ta e="T1029" id="Seg_12906" s="T1028">pro.h:A</ta>
            <ta e="T1030" id="Seg_12907" s="T1029">np:P</ta>
            <ta e="T1034" id="Seg_12908" s="T1033">np:Th</ta>
            <ta e="T1035" id="Seg_12909" s="T1034">0.1.h:A</ta>
            <ta e="T1037" id="Seg_12910" s="T1036">0.3:P</ta>
            <ta e="T1040" id="Seg_12911" s="T1039">0.3:Th</ta>
            <ta e="T1041" id="Seg_12912" s="T1040">np:Th</ta>
            <ta e="T1042" id="Seg_12913" s="T1041">adv:Time</ta>
            <ta e="T1043" id="Seg_12914" s="T1042">np:P</ta>
            <ta e="T1044" id="Seg_12915" s="T1043">0.1.h:A</ta>
            <ta e="T1045" id="Seg_12916" s="T1044">np:G</ta>
            <ta e="T1046" id="Seg_12917" s="T1045">0.1.h:A</ta>
            <ta e="T1047" id="Seg_12918" s="T1046">np:P</ta>
            <ta e="T1048" id="Seg_12919" s="T1047">0.1.h:A</ta>
            <ta e="T1050" id="Seg_12920" s="T1049">np:Th</ta>
            <ta e="T1052" id="Seg_12921" s="T1051">0.1.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_12922" s="T0">pro.h:S</ta>
            <ta e="T4" id="Seg_12923" s="T3">np:O</ta>
            <ta e="T5" id="Seg_12924" s="T4">v:pred</ta>
            <ta e="T7" id="Seg_12925" s="T6">v:pred 0.2.h:S</ta>
            <ta e="T9" id="Seg_12926" s="T8">v:pred 0.1.h:S</ta>
            <ta e="T14" id="Seg_12927" s="T13">np:O</ta>
            <ta e="T15" id="Seg_12928" s="T14">v:pred 0.2.h:S</ta>
            <ta e="T16" id="Seg_12929" s="T15">v:pred 0.1.h:S</ta>
            <ta e="T21" id="Seg_12930" s="T20">v:pred 0.1.h:S</ta>
            <ta e="T23" id="Seg_12931" s="T22">v:pred 0.1.h:S</ta>
            <ta e="T26" id="Seg_12932" s="T25">np:O</ta>
            <ta e="T27" id="Seg_12933" s="T26">v:pred 0.1.h:S</ta>
            <ta e="T29" id="Seg_12934" s="T28">np:O</ta>
            <ta e="T30" id="Seg_12935" s="T29">v:pred 0.3.h:S</ta>
            <ta e="T32" id="Seg_12936" s="T31">np:O</ta>
            <ta e="T33" id="Seg_12937" s="T32">v:pred 0.1.h:S</ta>
            <ta e="T35" id="Seg_12938" s="T34">np:O</ta>
            <ta e="T36" id="Seg_12939" s="T35">v:pred 0.1.h:S</ta>
            <ta e="T40" id="Seg_12940" s="T39">np.h:S</ta>
            <ta e="T41" id="Seg_12941" s="T40">v:pred</ta>
            <ta e="T43" id="Seg_12942" s="T42">np.h:S</ta>
            <ta e="T45" id="Seg_12943" s="T44">v:pred</ta>
            <ta e="T47" id="Seg_12944" s="T46">v:pred 0.3.h:S</ta>
            <ta e="T50" id="Seg_12945" s="T49">v:pred 0.3.h:S</ta>
            <ta e="T51" id="Seg_12946" s="T50">pro.h:S</ta>
            <ta e="T53" id="Seg_12947" s="T52">v:pred</ta>
            <ta e="T54" id="Seg_12948" s="T53">pro:O</ta>
            <ta e="T57" id="Seg_12949" s="T56">np.h:S</ta>
            <ta e="T60" id="Seg_12950" s="T59">pro.h:S</ta>
            <ta e="T61" id="Seg_12951" s="T60">pro.h:O</ta>
            <ta e="T62" id="Seg_12952" s="T61">v:pred</ta>
            <ta e="T65" id="Seg_12953" s="T64">v:pred 0.2.h:S</ta>
            <ta e="T69" id="Seg_12954" s="T68">v:pred 0.3.h:S</ta>
            <ta e="T70" id="Seg_12955" s="T69">np:S</ta>
            <ta e="T72" id="Seg_12956" s="T71">v:pred</ta>
            <ta e="T73" id="Seg_12957" s="T72">pro.h:S</ta>
            <ta e="T74" id="Seg_12958" s="T73">np:O</ta>
            <ta e="T75" id="Seg_12959" s="T74">v:pred</ta>
            <ta e="T77" id="Seg_12960" s="T76">np:O</ta>
            <ta e="T78" id="Seg_12961" s="T77">v:pred 0.3.h:S</ta>
            <ta e="T79" id="Seg_12962" s="T78">np:O</ta>
            <ta e="T80" id="Seg_12963" s="T79">v:pred 0.3.h:S</ta>
            <ta e="T81" id="Seg_12964" s="T80">pro.h:S</ta>
            <ta e="T82" id="Seg_12965" s="T81">v:pred</ta>
            <ta e="T83" id="Seg_12966" s="T82">np:O</ta>
            <ta e="T90" id="Seg_12967" s="T89">v:pred 0.3.h:S</ta>
            <ta e="T91" id="Seg_12968" s="T90">np:O</ta>
            <ta e="T93" id="Seg_12969" s="T92">pro.h:S</ta>
            <ta e="T95" id="Seg_12970" s="T94">v:pred</ta>
            <ta e="T96" id="Seg_12971" s="T95">pro:S</ta>
            <ta e="T97" id="Seg_12972" s="T96">n:pred</ta>
            <ta e="T99" id="Seg_12973" s="T98">pro.h:S</ta>
            <ta e="T100" id="Seg_12974" s="T99">v:pred</ta>
            <ta e="T101" id="Seg_12975" s="T100">pro.h:S</ta>
            <ta e="T102" id="Seg_12976" s="T101">pro.h:O</ta>
            <ta e="T103" id="Seg_12977" s="T102">v:pred</ta>
            <ta e="T106" id="Seg_12978" s="T105">ptcl.neg</ta>
            <ta e="T107" id="Seg_12979" s="T106">v:pred 0.2.h:S</ta>
            <ta e="T111" id="Seg_12980" s="T110">v:pred</ta>
            <ta e="T115" id="Seg_12981" s="T114">np.h:S</ta>
            <ta e="T116" id="Seg_12982" s="T115">v:pred 0.2.h:S</ta>
            <ta e="T118" id="Seg_12983" s="T117">s:purp</ta>
            <ta e="T120" id="Seg_12984" s="T119">np.h:S</ta>
            <ta e="T122" id="Seg_12985" s="T121">v:pred</ta>
            <ta e="T124" id="Seg_12986" s="T123">v:pred 0.3.h:S</ta>
            <ta e="T126" id="Seg_12987" s="T125">pro.h:S</ta>
            <ta e="T127" id="Seg_12988" s="T126">v:pred</ta>
            <ta e="T128" id="Seg_12989" s="T127">v:pred 0.1.h:S</ta>
            <ta e="T132" id="Seg_12990" s="T131">pro.h:S</ta>
            <ta e="T133" id="Seg_12991" s="T132">ptcl.neg</ta>
            <ta e="T134" id="Seg_12992" s="T133">v:pred</ta>
            <ta e="T136" id="Seg_12993" s="T135">v:pred 0.1.h:S</ta>
            <ta e="T137" id="Seg_12994" s="T136">v:pred 0.1.h:S</ta>
            <ta e="T138" id="Seg_12995" s="T137">pro.h:S</ta>
            <ta e="T139" id="Seg_12996" s="T138">ptcl.neg</ta>
            <ta e="T140" id="Seg_12997" s="T139">v:pred</ta>
            <ta e="T142" id="Seg_12998" s="T141">pro.h:S</ta>
            <ta e="T143" id="Seg_12999" s="T142">ptcl.neg</ta>
            <ta e="T144" id="Seg_13000" s="T143">v:pred</ta>
            <ta e="T150" id="Seg_13001" s="T149">np:S</ta>
            <ta e="T151" id="Seg_13002" s="T150">ptcl.neg</ta>
            <ta e="T152" id="Seg_13003" s="T151">adj:pred</ta>
            <ta e="T153" id="Seg_13004" s="T152">pro.h:S</ta>
            <ta e="T156" id="Seg_13005" s="T155">pro:O</ta>
            <ta e="T159" id="Seg_13006" s="T158">pro.h:S</ta>
            <ta e="T160" id="Seg_13007" s="T159">v:pred</ta>
            <ta e="T162" id="Seg_13008" s="T161">v:pred 0.3.h:S</ta>
            <ta e="T163" id="Seg_13009" s="T162">np.h:O</ta>
            <ta e="T166" id="Seg_13010" s="T165">v:pred 0.1.h:S</ta>
            <ta e="T168" id="Seg_13011" s="T167">np:S</ta>
            <ta e="T170" id="Seg_13012" s="T169">v:pred</ta>
            <ta e="T172" id="Seg_13013" s="T171">v:pred np:S</ta>
            <ta e="T173" id="Seg_13014" s="T172">v:pred 0.2:S</ta>
            <ta e="T174" id="Seg_13015" s="T173">v:pred 0.3:S</ta>
            <ta e="T175" id="Seg_13016" s="T174">ptcl.neg</ta>
            <ta e="T176" id="Seg_13017" s="T175">v:pred 0.3:S</ta>
            <ta e="T178" id="Seg_13018" s="T177">adj:pred</ta>
            <ta e="T179" id="Seg_13019" s="T178">cop 0.3:S</ta>
            <ta e="T180" id="Seg_13020" s="T179">np:S</ta>
            <ta e="T182" id="Seg_13021" s="T181">adj:pred</ta>
            <ta e="T183" id="Seg_13022" s="T182">cop</ta>
            <ta e="T184" id="Seg_13023" s="T183">np:S</ta>
            <ta e="T187" id="Seg_13024" s="T186">n:pred</ta>
            <ta e="T188" id="Seg_13025" s="T187">cop</ta>
            <ta e="T190" id="Seg_13026" s="T189">np:S</ta>
            <ta e="T191" id="Seg_13027" s="T190">v:pred</ta>
            <ta e="T193" id="Seg_13028" s="T192">np:S</ta>
            <ta e="T194" id="Seg_13029" s="T193">v:pred</ta>
            <ta e="T195" id="Seg_13030" s="T194">np:S</ta>
            <ta e="T197" id="Seg_13031" s="T196">adj:pred</ta>
            <ta e="T198" id="Seg_13032" s="T197">cop</ta>
            <ta e="T199" id="Seg_13033" s="T198">adj:pred</ta>
            <ta e="T200" id="Seg_13034" s="T199">cop 0.3:S</ta>
            <ta e="T203" id="Seg_13035" s="T202">np:S</ta>
            <ta e="T204" id="Seg_13036" s="T203">np:S</ta>
            <ta e="T205" id="Seg_13037" s="T204">np:S</ta>
            <ta e="T206" id="Seg_13038" s="T205">v:pred</ta>
            <ta e="T207" id="Seg_13039" s="T206">np:O</ta>
            <ta e="T208" id="Seg_13040" s="T207">s:purp</ta>
            <ta e="T210" id="Seg_13041" s="T209">np:S</ta>
            <ta e="T212" id="Seg_13042" s="T211">v:pred</ta>
            <ta e="T214" id="Seg_13043" s="T213">pro:O</ta>
            <ta e="T218" id="Seg_13044" s="T217">ptcl:pred</ta>
            <ta e="T221" id="Seg_13045" s="T220">v:pred 0.3:S</ta>
            <ta e="T224" id="Seg_13046" s="T223">ptcl:pred</ta>
            <ta e="T225" id="Seg_13047" s="T224">np.h:S</ta>
            <ta e="T226" id="Seg_13048" s="T225">v:pred</ta>
            <ta e="T229" id="Seg_13049" s="T228">np:S</ta>
            <ta e="T230" id="Seg_13050" s="T229">v:pred</ta>
            <ta e="T233" id="Seg_13051" s="T232">adj:pred</ta>
            <ta e="T234" id="Seg_13052" s="T233">pro.h:S</ta>
            <ta e="T236" id="Seg_13053" s="T235">v:pred</ta>
            <ta e="T240" id="Seg_13054" s="T239">v:pred 0.1.h:S</ta>
            <ta e="T245" id="Seg_13055" s="T244">v:pred 0.1.h:S</ta>
            <ta e="T246" id="Seg_13056" s="T245">np:O</ta>
            <ta e="T247" id="Seg_13057" s="T246">v:pred 0.1.h:S</ta>
            <ta e="T249" id="Seg_13058" s="T248">v:pred 0.1.h:S</ta>
            <ta e="T250" id="Seg_13059" s="T249">v:pred 0.1.h:S</ta>
            <ta e="T252" id="Seg_13060" s="T251">v:pred 0.1.h:S</ta>
            <ta e="T254" id="Seg_13061" s="T253">v:pred 0.1.h:S</ta>
            <ta e="T256" id="Seg_13062" s="T255">np:O</ta>
            <ta e="T257" id="Seg_13063" s="T256">v:pred 0.1.h:S</ta>
            <ta e="T261" id="Seg_13064" s="T260">ptcl:pred</ta>
            <ta e="T264" id="Seg_13065" s="T263">np:S</ta>
            <ta e="T265" id="Seg_13066" s="T264">v:pred</ta>
            <ta e="T268" id="Seg_13067" s="T267">ptcl:pred</ta>
            <ta e="T272" id="Seg_13068" s="T271">pro.h:S</ta>
            <ta e="T274" id="Seg_13069" s="T273">v:pred</ta>
            <ta e="T275" id="Seg_13070" s="T274">pro.h:S</ta>
            <ta e="T277" id="Seg_13071" s="T276">v:pred</ta>
            <ta e="T281" id="Seg_13072" s="T280">v:pred 0.3.h:S</ta>
            <ta e="T283" id="Seg_13073" s="T282">pro.h:S</ta>
            <ta e="T284" id="Seg_13074" s="T283">adj:pred</ta>
            <ta e="T289" id="Seg_13075" s="T288">ptcl:pred</ta>
            <ta e="T292" id="Seg_13076" s="T291">pro.h:S</ta>
            <ta e="T293" id="Seg_13077" s="T292">v:pred</ta>
            <ta e="T295" id="Seg_13078" s="T294">pro.h:S</ta>
            <ta e="T296" id="Seg_13079" s="T295">ptcl.neg</ta>
            <ta e="T297" id="Seg_13080" s="T296">v:pred</ta>
            <ta e="T299" id="Seg_13081" s="T298">pro.h:S</ta>
            <ta e="T300" id="Seg_13082" s="T299">ptcl.neg</ta>
            <ta e="T301" id="Seg_13083" s="T300">v:pred</ta>
            <ta e="T302" id="Seg_13084" s="T301">pro:O</ta>
            <ta e="T304" id="Seg_13085" s="T303">pro.h:S</ta>
            <ta e="T305" id="Seg_13086" s="T304">v:pred</ta>
            <ta e="T309" id="Seg_13087" s="T308">pro.h:S</ta>
            <ta e="T310" id="Seg_13088" s="T309">pro:O</ta>
            <ta e="T317" id="Seg_13089" s="T315">v:pred 0.1.h:S</ta>
            <ta e="T322" id="Seg_13090" s="T321">pro.h:S</ta>
            <ta e="T323" id="Seg_13091" s="T322">pro:O</ta>
            <ta e="T324" id="Seg_13092" s="T323">ptcl.neg</ta>
            <ta e="T325" id="Seg_13093" s="T324">v:pred</ta>
            <ta e="T326" id="Seg_13094" s="T325">v:pred 0.2.h:S</ta>
            <ta e="T327" id="Seg_13095" s="T326">pro:O</ta>
            <ta e="T328" id="Seg_13096" s="T327">v:pred 0.2.h:S</ta>
            <ta e="T330" id="Seg_13097" s="T329">np.h:S</ta>
            <ta e="T1057" id="Seg_13098" s="T331">conv:pred</ta>
            <ta e="T332" id="Seg_13099" s="T1057">v:pred</ta>
            <ta e="T334" id="Seg_13100" s="T333">pro.h:S</ta>
            <ta e="T336" id="Seg_13101" s="T335">adj:pred</ta>
            <ta e="T337" id="Seg_13102" s="T336">pro.h:S</ta>
            <ta e="T338" id="Seg_13103" s="T337">ptcl.neg</ta>
            <ta e="T339" id="Seg_13104" s="T338">v:pred</ta>
            <ta e="T340" id="Seg_13105" s="T339">pro.h:S</ta>
            <ta e="T342" id="Seg_13106" s="T341">v:pred</ta>
            <ta e="T344" id="Seg_13107" s="T343">v:pred 0.1.h:S</ta>
            <ta e="T345" id="Seg_13108" s="T344">v:pred 0.1.h:S</ta>
            <ta e="T349" id="Seg_13109" s="T348">ptcl.neg</ta>
            <ta e="T350" id="Seg_13110" s="T349">v:pred 0.1.h:S</ta>
            <ta e="T351" id="Seg_13111" s="T350">np.h:S</ta>
            <ta e="T352" id="Seg_13112" s="T351">np.h:S</ta>
            <ta e="T354" id="Seg_13113" s="T353">v:pred</ta>
            <ta e="T359" id="Seg_13114" s="T358">pro.h:S</ta>
            <ta e="T360" id="Seg_13115" s="T359">v:pred</ta>
            <ta e="T362" id="Seg_13116" s="T361">np.h:S</ta>
            <ta e="T366" id="Seg_13117" s="T365">np:O</ta>
            <ta e="T369" id="Seg_13118" s="T368">np:O</ta>
            <ta e="T371" id="Seg_13119" s="T370">v:pred 0.3.h:S</ta>
            <ta e="T372" id="Seg_13120" s="T371">v:pred 0.3.h:S</ta>
            <ta e="T374" id="Seg_13121" s="T373">v:pred 0.3.h:S</ta>
            <ta e="T376" id="Seg_13122" s="T375">np.h:S</ta>
            <ta e="T378" id="Seg_13123" s="T377">v:pred</ta>
            <ta e="T380" id="Seg_13124" s="T379">v:pred 0.3.h:S</ta>
            <ta e="T382" id="Seg_13125" s="T381">pro.h:S</ta>
            <ta e="T384" id="Seg_13126" s="T383">ptcl.neg</ta>
            <ta e="T385" id="Seg_13127" s="T384">v:pred</ta>
            <ta e="T386" id="Seg_13128" s="T385">pro.h:O</ta>
            <ta e="T387" id="Seg_13129" s="T386">ptcl.neg</ta>
            <ta e="T388" id="Seg_13130" s="T387">v:pred 0.3.h:S</ta>
            <ta e="T396" id="Seg_13131" s="T395">np.h:S</ta>
            <ta e="T397" id="Seg_13132" s="T396">np.h:S</ta>
            <ta e="T398" id="Seg_13133" s="T397">v:pred</ta>
            <ta e="T399" id="Seg_13134" s="T398">v:pred 0.3.h:S</ta>
            <ta e="T400" id="Seg_13135" s="T399">v:pred 0.2.h:S</ta>
            <ta e="T403" id="Seg_13136" s="T402">pro.h:S</ta>
            <ta e="T405" id="Seg_13137" s="T404">ptcl.neg</ta>
            <ta e="T406" id="Seg_13138" s="T405">v:pred</ta>
            <ta e="T407" id="Seg_13139" s="T406">v:pred</ta>
            <ta e="T409" id="Seg_13140" s="T408">s:purp</ta>
            <ta e="T410" id="Seg_13141" s="T409">ptcl.neg</ta>
            <ta e="T412" id="Seg_13142" s="T411">np.h:S</ta>
            <ta e="T414" id="Seg_13143" s="T413">ptcl.neg</ta>
            <ta e="T415" id="Seg_13144" s="T414">ptcl:pred</ta>
            <ta e="T417" id="Seg_13145" s="T416">np.h:S</ta>
            <ta e="T418" id="Seg_13146" s="T417">v:pred</ta>
            <ta e="T420" id="Seg_13147" s="T419">pro.h:S</ta>
            <ta e="T421" id="Seg_13148" s="T420">ptcl.neg</ta>
            <ta e="T422" id="Seg_13149" s="T421">v:pred</ta>
            <ta e="T425" id="Seg_13150" s="T424">v:pred 0.2.h:S</ta>
            <ta e="T427" id="Seg_13151" s="T426">pro.h:S</ta>
            <ta e="T428" id="Seg_13152" s="T427">v:pred</ta>
            <ta e="T433" id="Seg_13153" s="T432">np.h:S</ta>
            <ta e="T434" id="Seg_13154" s="T433">v:pred</ta>
            <ta e="T435" id="Seg_13155" s="T434">pro.h:O</ta>
            <ta e="T436" id="Seg_13156" s="T435">s:purp</ta>
            <ta e="T443" id="Seg_13157" s="T442">n:pred</ta>
            <ta e="T447" id="Seg_13158" s="T446">n:pred</ta>
            <ta e="T450" id="Seg_13159" s="T449">pro.h:S</ta>
            <ta e="T451" id="Seg_13160" s="T450">v:pred</ta>
            <ta e="T452" id="Seg_13161" s="T451">v:pred 0.2.h:S</ta>
            <ta e="T456" id="Seg_13162" s="T455">v:pred 0.2.h:S</ta>
            <ta e="T457" id="Seg_13163" s="T456">pro.h:S</ta>
            <ta e="T458" id="Seg_13164" s="T457">v:pred</ta>
            <ta e="T459" id="Seg_13165" s="T458">ptcl.neg</ta>
            <ta e="T460" id="Seg_13166" s="T459">ptcl.neg</ta>
            <ta e="T461" id="Seg_13167" s="T460">v:pred 0.3.h:S</ta>
            <ta e="T464" id="Seg_13168" s="T463">v:pred 0.3.h:S</ta>
            <ta e="T467" id="Seg_13169" s="T466">v:pred 0.1.h:S</ta>
            <ta e="T472" id="Seg_13170" s="T471">np.h:S</ta>
            <ta e="T473" id="Seg_13171" s="T472">v:pred</ta>
            <ta e="T476" id="Seg_13172" s="T475">pro.h:S</ta>
            <ta e="T479" id="Seg_13173" s="T478">v:pred</ta>
            <ta e="T480" id="Seg_13174" s="T479">np:O</ta>
            <ta e="T481" id="Seg_13175" s="T480">s:purp</ta>
            <ta e="T483" id="Seg_13176" s="T482">v:pred 0.3.h:S</ta>
            <ta e="T487" id="Seg_13177" s="T486">v:pred 0.3.h:S</ta>
            <ta e="T489" id="Seg_13178" s="T488">pro.h:S</ta>
            <ta e="T491" id="Seg_13179" s="T490">v:pred</ta>
            <ta e="T495" id="Seg_13180" s="T494">v:pred 0.1.h:S</ta>
            <ta e="T496" id="Seg_13181" s="T495">np:S</ta>
            <ta e="T497" id="Seg_13182" s="T496">v:pred</ta>
            <ta e="T498" id="Seg_13183" s="T497">v:pred 0.1.h:S</ta>
            <ta e="T499" id="Seg_13184" s="T498">v:pred 0.1.h:S</ta>
            <ta e="T503" id="Seg_13185" s="T502">np.h:S</ta>
            <ta e="T506" id="Seg_13186" s="T505">v:pred</ta>
            <ta e="T511" id="Seg_13187" s="T510">ptcl:pred</ta>
            <ta e="T512" id="Seg_13188" s="T511">np:O</ta>
            <ta e="T516" id="Seg_13189" s="T515">pro.h:S</ta>
            <ta e="T517" id="Seg_13190" s="T516">v:pred</ta>
            <ta e="T519" id="Seg_13191" s="T518">v:pred 0.1.h:S</ta>
            <ta e="T521" id="Seg_13192" s="T520">v:pred 0.1.h:S</ta>
            <ta e="T526" id="Seg_13193" s="T525">v:pred 0.1.h:S</ta>
            <ta e="T527" id="Seg_13194" s="T526">v:pred 0.1.h:S</ta>
            <ta e="T528" id="Seg_13195" s="T527">np:O</ta>
            <ta e="T529" id="Seg_13196" s="T528">v:pred 0.1.h:S</ta>
            <ta e="T530" id="Seg_13197" s="T529">np:O</ta>
            <ta e="T531" id="Seg_13198" s="T530">v:pred 0.1.h:S</ta>
            <ta e="T534" id="Seg_13199" s="T533">np.h:S</ta>
            <ta e="T535" id="Seg_13200" s="T534">v:pred</ta>
            <ta e="T537" id="Seg_13201" s="T536">np:S</ta>
            <ta e="T538" id="Seg_13202" s="T537">adj:pred</ta>
            <ta e="T542" id="Seg_13203" s="T541">pro.h:S</ta>
            <ta e="T543" id="Seg_13204" s="T542">adj:pred</ta>
            <ta e="T545" id="Seg_13205" s="T544">pro.h:S</ta>
            <ta e="T547" id="Seg_13206" s="T546">v:pred</ta>
            <ta e="T549" id="Seg_13207" s="T548">v:pred 0.3.h:S</ta>
            <ta e="T550" id="Seg_13208" s="T549">pro:O</ta>
            <ta e="T552" id="Seg_13209" s="T551">pro.h:S</ta>
            <ta e="T553" id="Seg_13210" s="T552">v:pred</ta>
            <ta e="T555" id="Seg_13211" s="T554">np.h:S</ta>
            <ta e="T558" id="Seg_13212" s="T557">ptcl:pred</ta>
            <ta e="T562" id="Seg_13213" s="T561">pro.h:S</ta>
            <ta e="T564" id="Seg_13214" s="T563">ptcl.neg</ta>
            <ta e="T565" id="Seg_13215" s="T564">v:pred</ta>
            <ta e="T569" id="Seg_13216" s="T568">np.h:S</ta>
            <ta e="T570" id="Seg_13217" s="T569">v:pred</ta>
            <ta e="T571" id="Seg_13218" s="T570">v:pred 0.2.h:S</ta>
            <ta e="T573" id="Seg_13219" s="T572">np:S</ta>
            <ta e="T574" id="Seg_13220" s="T573">adj:pred</ta>
            <ta e="T577" id="Seg_13221" s="T576">adj:pred</ta>
            <ta e="T578" id="Seg_13222" s="T577">np.h:S</ta>
            <ta e="T580" id="Seg_13223" s="T579">pro.h:S</ta>
            <ta e="T581" id="Seg_13224" s="T580">v:pred</ta>
            <ta e="T582" id="Seg_13225" s="T581">ptcl.neg</ta>
            <ta e="T584" id="Seg_13226" s="T583">v:pred 0.2.h:S</ta>
            <ta e="T586" id="Seg_13227" s="T585">v:pred 0.2.h:S</ta>
            <ta e="T589" id="Seg_13228" s="T588">np.h:S</ta>
            <ta e="T590" id="Seg_13229" s="T589">v:pred</ta>
            <ta e="T591" id="Seg_13230" s="T590">s:purp</ta>
            <ta e="T593" id="Seg_13231" s="T592">pro.h:S</ta>
            <ta e="T594" id="Seg_13232" s="T593">ptcl.neg</ta>
            <ta e="T595" id="Seg_13233" s="T594">v:pred</ta>
            <ta e="T598" id="Seg_13234" s="T597">ptcl.neg</ta>
            <ta e="T599" id="Seg_13235" s="T598">v:pred 0.1.h:S</ta>
            <ta e="T600" id="Seg_13236" s="T599">pro.h:S</ta>
            <ta e="T601" id="Seg_13237" s="T600">ptcl.neg</ta>
            <ta e="T602" id="Seg_13238" s="T601">adj:pred</ta>
            <ta e="T604" id="Seg_13239" s="T603">np:S</ta>
            <ta e="T607" id="Seg_13240" s="T606">adj:pred</ta>
            <ta e="T609" id="Seg_13241" s="T608">np:S</ta>
            <ta e="T610" id="Seg_13242" s="T609">adj:pred</ta>
            <ta e="T611" id="Seg_13243" s="T610">np:S</ta>
            <ta e="T612" id="Seg_13244" s="T611">ptcl.neg</ta>
            <ta e="T613" id="Seg_13245" s="T612">v:pred</ta>
            <ta e="T614" id="Seg_13246" s="T613">ptcl.neg</ta>
            <ta e="T616" id="Seg_13247" s="T615">v:pred 0.3.h:S</ta>
            <ta e="T617" id="Seg_13248" s="T616">np:S</ta>
            <ta e="T618" id="Seg_13249" s="T617">ptcl.neg</ta>
            <ta e="T619" id="Seg_13250" s="T618">v:pred</ta>
            <ta e="T621" id="Seg_13251" s="T620">np.h:S</ta>
            <ta e="T622" id="Seg_13252" s="T621">v:pred</ta>
            <ta e="T623" id="Seg_13253" s="T622">v:pred 0.2.h:S</ta>
            <ta e="T625" id="Seg_13254" s="T624">np:S</ta>
            <ta e="T626" id="Seg_13255" s="T625">v:pred</ta>
            <ta e="T628" id="Seg_13256" s="T627">np:S</ta>
            <ta e="T629" id="Seg_13257" s="T628">v:pred</ta>
            <ta e="T631" id="Seg_13258" s="T630">np:S</ta>
            <ta e="T632" id="Seg_13259" s="T631">adj:pred</ta>
            <ta e="T635" id="Seg_13260" s="T634">pro.h:S</ta>
            <ta e="T636" id="Seg_13261" s="T635">v:pred</ta>
            <ta e="T637" id="Seg_13262" s="T636">pro.h:S</ta>
            <ta e="T639" id="Seg_13263" s="T638">ptcl.neg</ta>
            <ta e="T640" id="Seg_13264" s="T639">v:pred</ta>
            <ta e="T643" id="Seg_13265" s="T642">ptcl.neg</ta>
            <ta e="T644" id="Seg_13266" s="T643">v:pred</ta>
            <ta e="T645" id="Seg_13267" s="T644">ptcl:pred</ta>
            <ta e="T650" id="Seg_13268" s="T649">adj:pred</ta>
            <ta e="T652" id="Seg_13269" s="T651">pro.h:S</ta>
            <ta e="T653" id="Seg_13270" s="T652">v:pred</ta>
            <ta e="T655" id="Seg_13271" s="T654">v:pred</ta>
            <ta e="T657" id="Seg_13272" s="T656">np.h:S</ta>
            <ta e="T658" id="Seg_13273" s="T657">pro.h:O</ta>
            <ta e="T659" id="Seg_13274" s="T658">np.h:S</ta>
            <ta e="T660" id="Seg_13275" s="T659">v:pred</ta>
            <ta e="T662" id="Seg_13276" s="T661">ptcl:pred</ta>
            <ta e="T663" id="Seg_13277" s="T662">pro.h:O</ta>
            <ta e="T664" id="Seg_13278" s="T663">s:purp</ta>
            <ta e="T665" id="Seg_13279" s="T664">np.h:S</ta>
            <ta e="T666" id="Seg_13280" s="T665">v:pred</ta>
            <ta e="T667" id="Seg_13281" s="T666">v:pred</ta>
            <ta e="T668" id="Seg_13282" s="T667">np:O</ta>
            <ta e="T669" id="Seg_13283" s="T668">pro.h:O</ta>
            <ta e="T671" id="Seg_13284" s="T670">v:pred</ta>
            <ta e="T673" id="Seg_13285" s="T672">pro.h:S</ta>
            <ta e="T674" id="Seg_13286" s="T673">v:pred</ta>
            <ta e="T677" id="Seg_13287" s="T676">ptcl.neg</ta>
            <ta e="T680" id="Seg_13288" s="T679">v:pred 0.2.h:S</ta>
            <ta e="T682" id="Seg_13289" s="T681">v:pred 0.2.h:S</ta>
            <ta e="T684" id="Seg_13290" s="T683">pro.h:S</ta>
            <ta e="T688" id="Seg_13291" s="T687">adj:pred</ta>
            <ta e="T689" id="Seg_13292" s="T688">pro.h:O</ta>
            <ta e="T690" id="Seg_13293" s="T689">v:pred 0.2.h:S</ta>
            <ta e="T691" id="Seg_13294" s="T690">pro:O</ta>
            <ta e="T695" id="Seg_13295" s="T694">np.h:S</ta>
            <ta e="T697" id="Seg_13296" s="T696">v:pred</ta>
            <ta e="T699" id="Seg_13297" s="T698">v:pred 0.3.h:S</ta>
            <ta e="T701" id="Seg_13298" s="T700">ptcl:pred</ta>
            <ta e="T703" id="Seg_13299" s="T702">pro.h:O</ta>
            <ta e="T705" id="Seg_13300" s="T704">ptcl:pred</ta>
            <ta e="T708" id="Seg_13301" s="T707">ptcl.neg</ta>
            <ta e="T709" id="Seg_13302" s="T708">v:pred 0.1.h:S</ta>
            <ta e="T710" id="Seg_13303" s="T709">pro.h:O</ta>
            <ta e="T714" id="Seg_13304" s="T713">v:pred 0.1.h:S</ta>
            <ta e="T716" id="Seg_13305" s="T715">pro.h:S</ta>
            <ta e="T717" id="Seg_13306" s="T716">np:O</ta>
            <ta e="T718" id="Seg_13307" s="T717">v:pred</ta>
            <ta e="T721" id="Seg_13308" s="T720">pro.h:S</ta>
            <ta e="T723" id="Seg_13309" s="T722">v:pred</ta>
            <ta e="T725" id="Seg_13310" s="T724">np:O</ta>
            <ta e="T726" id="Seg_13311" s="T725">v:pred 0.1.h:S</ta>
            <ta e="T727" id="Seg_13312" s="T726">v:pred 0.1.h:S</ta>
            <ta e="T730" id="Seg_13313" s="T729">np:O</ta>
            <ta e="T731" id="Seg_13314" s="T730">v:pred 0.1.h:S</ta>
            <ta e="T732" id="Seg_13315" s="T731">np.h:O</ta>
            <ta e="T733" id="Seg_13316" s="T732">v:pred 0.1.h:S</ta>
            <ta e="T734" id="Seg_13317" s="T733">np:O</ta>
            <ta e="T735" id="Seg_13318" s="T734">v:pred 0.3.h:S</ta>
            <ta e="T736" id="Seg_13319" s="T735">v:pred 0.3.h:S</ta>
            <ta e="T738" id="Seg_13320" s="T737">pro.h:S</ta>
            <ta e="T740" id="Seg_13321" s="T739">v:pred</ta>
            <ta e="T747" id="Seg_13322" s="T746">np:S</ta>
            <ta e="T748" id="Seg_13323" s="T747">v:pred</ta>
            <ta e="T749" id="Seg_13324" s="T748">pro.h:O</ta>
            <ta e="T750" id="Seg_13325" s="T749">v:pred 0.3.h:S</ta>
            <ta e="T753" id="Seg_13326" s="T752">pro.h:S</ta>
            <ta e="T754" id="Seg_13327" s="T753">adj:pred</ta>
            <ta e="T755" id="Seg_13328" s="T754">cop</ta>
            <ta e="T756" id="Seg_13329" s="T755">n:pred</ta>
            <ta e="T757" id="Seg_13330" s="T756">pro.h:S</ta>
            <ta e="T759" id="Seg_13331" s="T758">pro.h:S</ta>
            <ta e="T760" id="Seg_13332" s="T759">v:pred</ta>
            <ta e="T763" id="Seg_13333" s="T762">v:pred 0.3.h:S</ta>
            <ta e="T766" id="Seg_13334" s="T765">v:pred 0.1.h:S</ta>
            <ta e="T767" id="Seg_13335" s="T766">pro.h:S</ta>
            <ta e="T769" id="Seg_13336" s="T768">adj:pred</ta>
            <ta e="T770" id="Seg_13337" s="T769">cop</ta>
            <ta e="T774" id="Seg_13338" s="T773">pro.h:S</ta>
            <ta e="T775" id="Seg_13339" s="T774">pro.h:O</ta>
            <ta e="T776" id="Seg_13340" s="T775">v:pred</ta>
            <ta e="T778" id="Seg_13341" s="T777">pro.h:O</ta>
            <ta e="T779" id="Seg_13342" s="T778">v:pred 0.3.h:S</ta>
            <ta e="T783" id="Seg_13343" s="T782">np.h:O</ta>
            <ta e="T784" id="Seg_13344" s="T783">v:pred 0.1.h:S</ta>
            <ta e="T787" id="Seg_13345" s="T786">v:pred 0.1.h:S</ta>
            <ta e="T794" id="Seg_13346" s="T793">v:pred 0.3.h:S</ta>
            <ta e="T798" id="Seg_13347" s="T797">pro.h:S</ta>
            <ta e="T799" id="Seg_13348" s="T798">adj:pred</ta>
            <ta e="T801" id="Seg_13349" s="T800">cop</ta>
            <ta e="T803" id="Seg_13350" s="T802">v:pred 0.1.h:S</ta>
            <ta e="T805" id="Seg_13351" s="T804">np.h:S</ta>
            <ta e="T806" id="Seg_13352" s="T805">pro.h:O</ta>
            <ta e="T807" id="Seg_13353" s="T806">v:pred</ta>
            <ta e="T808" id="Seg_13354" s="T807">pro.h:S</ta>
            <ta e="T809" id="Seg_13355" s="T808">v:pred</ta>
            <ta e="T811" id="Seg_13356" s="T810">v:pred 0.1.h:S</ta>
            <ta e="T820" id="Seg_13357" s="T819">v:pred 0.1.h:S</ta>
            <ta e="T822" id="Seg_13358" s="T821">np.h:S</ta>
            <ta e="T823" id="Seg_13359" s="T822">v:pred</ta>
            <ta e="T825" id="Seg_13360" s="T824">ptcl.neg</ta>
            <ta e="T826" id="Seg_13361" s="T825">adj:pred</ta>
            <ta e="T827" id="Seg_13362" s="T826">np:O</ta>
            <ta e="T828" id="Seg_13363" s="T827">v:pred 0.3.h:S</ta>
            <ta e="T829" id="Seg_13364" s="T828">v:pred 0.3.h:S</ta>
            <ta e="T830" id="Seg_13365" s="T829">v:pred 0.3.h:S</ta>
            <ta e="T833" id="Seg_13366" s="T832">v:pred 0.1.h:S</ta>
            <ta e="T837" id="Seg_13367" s="T836">np:O</ta>
            <ta e="T838" id="Seg_13368" s="T837">v:pred 0.3.h:S</ta>
            <ta e="T841" id="Seg_13369" s="T840">v:pred 0.3.h:S</ta>
            <ta e="T843" id="Seg_13370" s="T842">np:S</ta>
            <ta e="T844" id="Seg_13371" s="T843">v:pred</ta>
            <ta e="T846" id="Seg_13372" s="T845">pro.h:S</ta>
            <ta e="T848" id="Seg_13373" s="T847">pro.h:O</ta>
            <ta e="T849" id="Seg_13374" s="T848">v:pred</ta>
            <ta e="T852" id="Seg_13375" s="T851">np.h:S</ta>
            <ta e="T853" id="Seg_13376" s="T852">v:pred</ta>
            <ta e="T855" id="Seg_13377" s="T854">v:pred 0.1.h:S</ta>
            <ta e="T858" id="Seg_13378" s="T856">ptcl.neg</ta>
            <ta e="T861" id="Seg_13379" s="T860">v:pred 0.1.h:S</ta>
            <ta e="T868" id="Seg_13380" s="T867">ptcl:pred</ta>
            <ta e="T869" id="Seg_13381" s="T868">pro.h:O</ta>
            <ta e="T871" id="Seg_13382" s="T870">pro.h:S</ta>
            <ta e="T875" id="Seg_13383" s="T874">ptcl.neg</ta>
            <ta e="T876" id="Seg_13384" s="T875">v:pred 0.1.h:S</ta>
            <ta e="T878" id="Seg_13385" s="T877">np:S</ta>
            <ta e="T879" id="Seg_13386" s="T878">adj:pred</ta>
            <ta e="T880" id="Seg_13387" s="T879">ptcl.neg</ta>
            <ta e="T881" id="Seg_13388" s="T880">v:pred 0.1.h:S</ta>
            <ta e="T883" id="Seg_13389" s="T882">v:pred 0.3.h:S</ta>
            <ta e="T884" id="Seg_13390" s="T883">ptcl:pred</ta>
            <ta e="T887" id="Seg_13391" s="T1055">v:pred 0.1.h:S</ta>
            <ta e="T888" id="Seg_13392" s="T887">v:pred 0.1.h:S</ta>
            <ta e="T890" id="Seg_13393" s="T889">v:pred 0.1.h:S</ta>
            <ta e="T894" id="Seg_13394" s="T893">v:pred 0.1.h:S</ta>
            <ta e="T895" id="Seg_13395" s="T894">v:pred 0.1.h:S</ta>
            <ta e="T897" id="Seg_13396" s="T896">ptcl.neg</ta>
            <ta e="T898" id="Seg_13397" s="T897">v:pred 0.1.h:S</ta>
            <ta e="T901" id="Seg_13398" s="T900">ptcl:pred</ta>
            <ta e="T904" id="Seg_13399" s="T903">v:pred 0.3.h:S</ta>
            <ta e="T907" id="Seg_13400" s="T906">pro.h:S</ta>
            <ta e="T908" id="Seg_13401" s="T907">v:pred</ta>
            <ta e="T910" id="Seg_13402" s="T909">np:O</ta>
            <ta e="T911" id="Seg_13403" s="T910">v:pred 0.2.h:S</ta>
            <ta e="T913" id="Seg_13404" s="T912">ptcl:pred</ta>
            <ta e="T914" id="Seg_13405" s="T913">v:pred 0.3.h:S</ta>
            <ta e="T917" id="Seg_13406" s="T916">v:pred 0.3:S</ta>
            <ta e="T918" id="Seg_13407" s="T917">np:O</ta>
            <ta e="T920" id="Seg_13408" s="T919">v:pred 0.3.h:S</ta>
            <ta e="T1058" id="Seg_13409" s="T921">conv:pred</ta>
            <ta e="T922" id="Seg_13410" s="T1058">v:pred 0.3.h:S</ta>
            <ta e="T923" id="Seg_13411" s="T922">pro.h:S</ta>
            <ta e="T924" id="Seg_13412" s="T923">adj:pred</ta>
            <ta e="T927" id="Seg_13413" s="T926">pro.h:S</ta>
            <ta e="T930" id="Seg_13414" s="T929">v:pred</ta>
            <ta e="T936" id="Seg_13415" s="T935">v:pred 0.1.h:S</ta>
            <ta e="T938" id="Seg_13416" s="T937">pro.h:S</ta>
            <ta e="T939" id="Seg_13417" s="T938">v:pred</ta>
            <ta e="T941" id="Seg_13418" s="T940">pro.h:S</ta>
            <ta e="T945" id="Seg_13419" s="T944">np:O</ta>
            <ta e="T947" id="Seg_13420" s="T946">v:pred</ta>
            <ta e="T948" id="Seg_13421" s="T947">np:O</ta>
            <ta e="T950" id="Seg_13422" s="T949">v:pred</ta>
            <ta e="T952" id="Seg_13423" s="T951">v:pred 0.1.h:S</ta>
            <ta e="T956" id="Seg_13424" s="T955">np:O</ta>
            <ta e="T957" id="Seg_13425" s="T956">v:pred 0.1.h:S</ta>
            <ta e="T958" id="Seg_13426" s="T957">np.h:S</ta>
            <ta e="T959" id="Seg_13427" s="T958">v:pred</ta>
            <ta e="T962" id="Seg_13428" s="T961">pro.h:S</ta>
            <ta e="T963" id="Seg_13429" s="T962">np:O</ta>
            <ta e="T965" id="Seg_13430" s="T964">v:pred</ta>
            <ta e="T967" id="Seg_13431" s="T966">np:O</ta>
            <ta e="T969" id="Seg_13432" s="T968">v:pred 0.3.h:S</ta>
            <ta e="T971" id="Seg_13433" s="T969">s:purp</ta>
            <ta e="T974" id="Seg_13434" s="T972">s:purp</ta>
            <ta e="T976" id="Seg_13435" s="T975">pro.h:S</ta>
            <ta e="T979" id="Seg_13436" s="T978">np:O</ta>
            <ta e="T980" id="Seg_13437" s="T979">v:pred</ta>
            <ta e="T982" id="Seg_13438" s="T981">np:O</ta>
            <ta e="T983" id="Seg_13439" s="T982">v:pred 0.1.h:S</ta>
            <ta e="T985" id="Seg_13440" s="T984">pro.h:S</ta>
            <ta e="T988" id="Seg_13441" s="T987">v:pred</ta>
            <ta e="T989" id="Seg_13442" s="T988">np:O</ta>
            <ta e="T990" id="Seg_13443" s="T989">v:pred 0.3.h:S</ta>
            <ta e="T992" id="Seg_13444" s="T991">ptcl.neg</ta>
            <ta e="T993" id="Seg_13445" s="T992">v:pred 0.3.h:S</ta>
            <ta e="T994" id="Seg_13446" s="T993">pro.h:S</ta>
            <ta e="T995" id="Seg_13447" s="T994">pro.h:O</ta>
            <ta e="T996" id="Seg_13448" s="T995">v:pred</ta>
            <ta e="T997" id="Seg_13449" s="T996">pro.h:S</ta>
            <ta e="T1001" id="Seg_13450" s="T1000">np:O</ta>
            <ta e="T1002" id="Seg_13451" s="T1001">v:pred</ta>
            <ta e="T1004" id="Seg_13452" s="T1003">np:O</ta>
            <ta e="T1005" id="Seg_13453" s="T1004">v:pred 0.1.h:S</ta>
            <ta e="T1009" id="Seg_13454" s="T1008">np:O</ta>
            <ta e="T1010" id="Seg_13455" s="T1009">v:pred 0.1.h:S</ta>
            <ta e="T1011" id="Seg_13456" s="T1010">np:O</ta>
            <ta e="T1012" id="Seg_13457" s="T1011">v:pred 0.1.h:S</ta>
            <ta e="T1013" id="Seg_13458" s="T1012">np:O</ta>
            <ta e="T1014" id="Seg_13459" s="T1013">v:pred 0.1.h:S</ta>
            <ta e="T1016" id="Seg_13460" s="T1015">v:pred 0.1.h:S</ta>
            <ta e="T1019" id="Seg_13461" s="T1018">v:pred 0.1.h:S</ta>
            <ta e="T1021" id="Seg_13462" s="T1020">np.h:S</ta>
            <ta e="T1023" id="Seg_13463" s="T1022">v:pred</ta>
            <ta e="T1026" id="Seg_13464" s="T1025">n:pred</ta>
            <ta e="T1027" id="Seg_13465" s="T1026">cop 0.3.h:S</ta>
            <ta e="T1029" id="Seg_13466" s="T1028">pro.h:S</ta>
            <ta e="T1030" id="Seg_13467" s="T1029">np:O</ta>
            <ta e="T1033" id="Seg_13468" s="T1032">v:pred</ta>
            <ta e="T1034" id="Seg_13469" s="T1033">np:O</ta>
            <ta e="T1035" id="Seg_13470" s="T1034">v:pred 0.1.h:S</ta>
            <ta e="T1037" id="Seg_13471" s="T1036">v:pred 0.3:S</ta>
            <ta e="T1040" id="Seg_13472" s="T1039">cop 0.3:S</ta>
            <ta e="T1041" id="Seg_13473" s="T1040">n:pred</ta>
            <ta e="T1043" id="Seg_13474" s="T1042">np:O</ta>
            <ta e="T1044" id="Seg_13475" s="T1043">v:pred 0.1.h:S</ta>
            <ta e="T1046" id="Seg_13476" s="T1045">v:pred 0.1.h:S</ta>
            <ta e="T1047" id="Seg_13477" s="T1046">np:O</ta>
            <ta e="T1048" id="Seg_13478" s="T1047">v:pred 0.1.h:S</ta>
            <ta e="T1050" id="Seg_13479" s="T1049">np:O</ta>
            <ta e="T1052" id="Seg_13480" s="T1051">v:pred 0.1.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T6" id="Seg_13481" s="T5">RUS:gram</ta>
            <ta e="T10" id="Seg_13482" s="T9">RUS:gram</ta>
            <ta e="T13" id="Seg_13483" s="T12">RUS:cult</ta>
            <ta e="T20" id="Seg_13484" s="T19">RUS:cult</ta>
            <ta e="T21" id="Seg_13485" s="T20">%TURK:core</ta>
            <ta e="T22" id="Seg_13486" s="T21">RUS:gram</ta>
            <ta e="T24" id="Seg_13487" s="T23">RUS:gram</ta>
            <ta e="T26" id="Seg_13488" s="T25">TURK:cult</ta>
            <ta e="T28" id="Seg_13489" s="T27">RUS:gram</ta>
            <ta e="T31" id="Seg_13490" s="T30">RUS:gram</ta>
            <ta e="T34" id="Seg_13491" s="T33">RUS:gram</ta>
            <ta e="T35" id="Seg_13492" s="T34">TURK:cult</ta>
            <ta e="T37" id="Seg_13493" s="T36">TURK:disc</ta>
            <ta e="T46" id="Seg_13494" s="T45">RUS:cult</ta>
            <ta e="T48" id="Seg_13495" s="T47">RUS:gram</ta>
            <ta e="T49" id="Seg_13496" s="T48">RUS:cult</ta>
            <ta e="T76" id="Seg_13497" s="T75">RUS:gram</ta>
            <ta e="T85" id="Seg_13498" s="T84">TAT:cult</ta>
            <ta e="T92" id="Seg_13499" s="T91">RUS:gram</ta>
            <ta e="T94" id="Seg_13500" s="T93">TURK:disc</ta>
            <ta e="T98" id="Seg_13501" s="T97">RUS:gram</ta>
            <ta e="T104" id="Seg_13502" s="T103">RUS:gram</ta>
            <ta e="T115" id="Seg_13503" s="T114">TURK:core</ta>
            <ta e="T125" id="Seg_13504" s="T124">RUS:gram</ta>
            <ta e="T129" id="Seg_13505" s="T128">RUS:gram</ta>
            <ta e="T131" id="Seg_13506" s="T130">RUS:gram</ta>
            <ta e="T141" id="Seg_13507" s="T140">RUS:gram</ta>
            <ta e="T147" id="Seg_13508" s="T146">TURK:cult</ta>
            <ta e="T148" id="Seg_13509" s="T147">RUS:gram</ta>
            <ta e="T150" id="Seg_13510" s="T149">TURK:cult</ta>
            <ta e="T158" id="Seg_13511" s="T157">RUS:gram</ta>
            <ta e="T161" id="Seg_13512" s="T160">TURK:disc</ta>
            <ta e="T164" id="Seg_13513" s="T163">RUS:mod</ta>
            <ta e="T169" id="Seg_13514" s="T168">TURK:disc</ta>
            <ta e="T181" id="Seg_13515" s="T180">TURK:disc</ta>
            <ta e="T192" id="Seg_13516" s="T191">RUS:core</ta>
            <ta e="T196" id="Seg_13517" s="T195">TURK:disc</ta>
            <ta e="T199" id="Seg_13518" s="T198">RUS:core</ta>
            <ta e="T201" id="Seg_13519" s="T200">TURK:disc</ta>
            <ta e="T204" id="Seg_13520" s="T203">TURK:cult</ta>
            <ta e="T218" id="Seg_13521" s="T217">RUS:mod</ta>
            <ta e="T222" id="Seg_13522" s="T221">RUS:gram</ta>
            <ta e="T224" id="Seg_13523" s="T223">RUS:mod</ta>
            <ta e="T228" id="Seg_13524" s="T227">TURK:core</ta>
            <ta e="T241" id="Seg_13525" s="T240">RUS:gram</ta>
            <ta e="T244" id="Seg_13526" s="T243">TURK:disc</ta>
            <ta e="T246" id="Seg_13527" s="T245">TURK:cult</ta>
            <ta e="T248" id="Seg_13528" s="T247">RUS:gram</ta>
            <ta e="T252" id="Seg_13529" s="T251">%TURK:core</ta>
            <ta e="T255" id="Seg_13530" s="T254">RUS:gram</ta>
            <ta e="T260" id="Seg_13531" s="T259">TAT:cult</ta>
            <ta e="T261" id="Seg_13532" s="T260">RUS:mod</ta>
            <ta e="T264" id="Seg_13533" s="T263">RUS:cult</ta>
            <ta e="T266" id="Seg_13534" s="T265">RUS:cult</ta>
            <ta e="T268" id="Seg_13535" s="T267">RUS:mod</ta>
            <ta e="T269" id="Seg_13536" s="T268">RUS:gram</ta>
            <ta e="T277" id="Seg_13537" s="T276">%TURK:core</ta>
            <ta e="T281" id="Seg_13538" s="T280">%TURK:core</ta>
            <ta e="T289" id="Seg_13539" s="T288">RUS:mod</ta>
            <ta e="T290" id="Seg_13540" s="T289">%TURK:core</ta>
            <ta e="T293" id="Seg_13541" s="T292">%TURK:core</ta>
            <ta e="T294" id="Seg_13542" s="T293">RUS:gram</ta>
            <ta e="T297" id="Seg_13543" s="T296">%TURK:core</ta>
            <ta e="T303" id="Seg_13544" s="T302">%TURK:core</ta>
            <ta e="T305" id="Seg_13545" s="T304">%TURK:core</ta>
            <ta e="T306" id="Seg_13546" s="T305">RUS:gram</ta>
            <ta e="T308" id="Seg_13547" s="T307">RUS:gram</ta>
            <ta e="T313" id="Seg_13548" s="T312">%TURK:core</ta>
            <ta e="T317" id="Seg_13549" s="T315">TURK:mod(PTCL)</ta>
            <ta e="T319" id="Seg_13550" s="T318">TAT:cult</ta>
            <ta e="T323" id="Seg_13551" s="T322">TURK:gram(INDEF)</ta>
            <ta e="T327" id="Seg_13552" s="T326">RUS:gram(INDEF)</ta>
            <ta e="T329" id="Seg_13553" s="T328">RUS:gram(INDEF)</ta>
            <ta e="T333" id="Seg_13554" s="T332">RUS:gram</ta>
            <ta e="T336" id="Seg_13555" s="T335">TURK:core</ta>
            <ta e="T337" id="Seg_13556" s="T336">TURK:gram(INDEF)</ta>
            <ta e="T346" id="Seg_13557" s="T345">TURK:disc</ta>
            <ta e="T347" id="Seg_13558" s="T346">RUS:gram</ta>
            <ta e="T355" id="Seg_13559" s="T354">RUS:gram</ta>
            <ta e="T358" id="Seg_13560" s="T357">RUS:gram</ta>
            <ta e="T365" id="Seg_13561" s="T364">TURK:disc</ta>
            <ta e="T366" id="Seg_13562" s="T365">TURK:cult</ta>
            <ta e="T373" id="Seg_13563" s="T372">TURK:disc</ta>
            <ta e="T375" id="Seg_13564" s="T374">TURK:disc</ta>
            <ta e="T381" id="Seg_13565" s="T380">RUS:gram</ta>
            <ta e="T386" id="Seg_13566" s="T385">TURK:gram(INDEF)</ta>
            <ta e="T395" id="Seg_13567" s="T394">TURK:disc</ta>
            <ta e="T402" id="Seg_13568" s="T401">RUS:gram</ta>
            <ta e="T404" id="Seg_13569" s="T403">TURK:gram(INDEF)</ta>
            <ta e="T414" id="Seg_13570" s="T413">RUS:gram</ta>
            <ta e="T415" id="Seg_13571" s="T414">RUS:mod</ta>
            <ta e="T440" id="Seg_13572" s="T439">RUS:gram</ta>
            <ta e="T444" id="Seg_13573" s="T443">RUS:gram</ta>
            <ta e="T454" id="Seg_13574" s="T453">RUS:gram</ta>
            <ta e="T459" id="Seg_13575" s="T458">TURK:disc</ta>
            <ta e="T464" id="Seg_13576" s="T463">%TURK:core</ta>
            <ta e="T465" id="Seg_13577" s="T464">RUS:gram</ta>
            <ta e="T485" id="Seg_13578" s="T484">RUS:gram</ta>
            <ta e="T492" id="Seg_13579" s="T491">RUS:gram</ta>
            <ta e="T501" id="Seg_13580" s="T500">TURK:disc</ta>
            <ta e="T511" id="Seg_13581" s="T510">RUS:gram</ta>
            <ta e="T524" id="Seg_13582" s="T523">RUS:cult</ta>
            <ta e="T528" id="Seg_13583" s="T527">RUS:cult</ta>
            <ta e="T540" id="Seg_13584" s="T539">RUS:gram</ta>
            <ta e="T548" id="Seg_13585" s="T547">RUS:gram</ta>
            <ta e="T558" id="Seg_13586" s="T557">RUS:gram</ta>
            <ta e="T566" id="Seg_13587" s="T565">RUS:gram</ta>
            <ta e="T577" id="Seg_13588" s="T576">TURK:core</ta>
            <ta e="T579" id="Seg_13589" s="T578">RUS:gram</ta>
            <ta e="T583" id="Seg_13590" s="T582">RUS:gram</ta>
            <ta e="T585" id="Seg_13591" s="T584">RUS:gram</ta>
            <ta e="T592" id="Seg_13592" s="T591">RUS:gram</ta>
            <ta e="T596" id="Seg_13593" s="T595">RUS:gram</ta>
            <ta e="T605" id="Seg_13594" s="T604">TURK:disc</ta>
            <ta e="T613" id="Seg_13595" s="T612">%TURK:core</ta>
            <ta e="T615" id="Seg_13596" s="T614">TURK:core</ta>
            <ta e="T616" id="Seg_13597" s="T615">%TURK:core</ta>
            <ta e="T620" id="Seg_13598" s="T619">RUS:gram</ta>
            <ta e="T625" id="Seg_13599" s="T624">TURK:cult</ta>
            <ta e="T627" id="Seg_13600" s="T626">RUS:gram</ta>
            <ta e="T630" id="Seg_13601" s="T629">RUS:gram</ta>
            <ta e="T633" id="Seg_13602" s="T632">RUS:gram</ta>
            <ta e="T641" id="Seg_13603" s="T640">RUS:gram</ta>
            <ta e="T642" id="Seg_13604" s="T641">TURK:cult</ta>
            <ta e="T645" id="Seg_13605" s="T644">RUS:mod</ta>
            <ta e="T647" id="Seg_13606" s="T646">RUS:gram</ta>
            <ta e="T651" id="Seg_13607" s="T650">RUS:gram</ta>
            <ta e="T654" id="Seg_13608" s="T653">RUS:cult</ta>
            <ta e="T662" id="Seg_13609" s="T661">RUS:gram</ta>
            <ta e="T666" id="Seg_13610" s="T665">RUS:gram</ta>
            <ta e="T672" id="Seg_13611" s="T671">RUS:gram</ta>
            <ta e="T676" id="Seg_13612" s="T675">RUS:gram</ta>
            <ta e="T700" id="Seg_13613" s="T699">RUS:gram</ta>
            <ta e="T701" id="Seg_13614" s="T700">RUS:gram</ta>
            <ta e="T704" id="Seg_13615" s="T703">RUS:gram</ta>
            <ta e="T705" id="Seg_13616" s="T704">RUS:gram</ta>
            <ta e="T706" id="Seg_13617" s="T705">TURK:cult</ta>
            <ta e="T711" id="Seg_13618" s="T710">RUS:gram</ta>
            <ta e="T713" id="Seg_13619" s="T712">TURK:core</ta>
            <ta e="T725" id="Seg_13620" s="T724">TURK:cult</ta>
            <ta e="T730" id="Seg_13621" s="T729">RUS:cult</ta>
            <ta e="T734" id="Seg_13622" s="T733">TURK:cult</ta>
            <ta e="T741" id="Seg_13623" s="T740">RUS:cult</ta>
            <ta e="T747" id="Seg_13624" s="T746">RUS:cult</ta>
            <ta e="T751" id="Seg_13625" s="T750">RUS:cult</ta>
            <ta e="T761" id="Seg_13626" s="T760">RUS:cult</ta>
            <ta e="T765" id="Seg_13627" s="T764">RUS:mod</ta>
            <ta e="T777" id="Seg_13628" s="T776">RUS:gram</ta>
            <ta e="T780" id="Seg_13629" s="T779">RUS:mod</ta>
            <ta e="T791" id="Seg_13630" s="T790">RUS:gram</ta>
            <ta e="T796" id="Seg_13631" s="T795">RUS:gram</ta>
            <ta e="T826" id="Seg_13632" s="T825">TURK:core</ta>
            <ta e="T827" id="Seg_13633" s="T826">TURK:cult</ta>
            <ta e="T834" id="Seg_13634" s="T833">TURK:disc</ta>
            <ta e="T836" id="Seg_13635" s="T835">TURK:disc</ta>
            <ta e="T842" id="Seg_13636" s="T841">RUS:mod</ta>
            <ta e="T856" id="Seg_13637" s="T855">TURK:gram(INDEF)</ta>
            <ta e="T868" id="Seg_13638" s="T867">RUS:gram</ta>
            <ta e="T884" id="Seg_13639" s="T883">RUS:gram</ta>
            <ta e="T901" id="Seg_13640" s="T900">RUS:gram</ta>
            <ta e="T910" id="Seg_13641" s="T909">TAT:cult</ta>
            <ta e="T913" id="Seg_13642" s="T912">RUS:gram</ta>
            <ta e="T915" id="Seg_13643" s="T914">RUS:gram</ta>
            <ta e="T918" id="Seg_13644" s="T917">TAT:cult</ta>
            <ta e="T921" id="Seg_13645" s="T920">RUS:gram</ta>
            <ta e="T932" id="Seg_13646" s="T931">RUS:disc</ta>
            <ta e="T944" id="Seg_13647" s="T943">TURK:disc</ta>
            <ta e="T948" id="Seg_13648" s="T947">TAT:cult</ta>
            <ta e="T955" id="Seg_13649" s="T954">RUS:gram</ta>
            <ta e="T956" id="Seg_13650" s="T955">TAT:cult</ta>
            <ta e="T961" id="Seg_13651" s="T960">RUS:gram</ta>
            <ta e="T972" id="Seg_13652" s="T971">RUS:gram</ta>
            <ta e="T975" id="Seg_13653" s="T974">RUS:gram</ta>
            <ta e="T981" id="Seg_13654" s="T980">RUS:gram</ta>
            <ta e="T984" id="Seg_13655" s="T983">RUS:gram</ta>
            <ta e="T986" id="Seg_13656" s="T985">RUS:mod</ta>
            <ta e="T991" id="Seg_13657" s="T990">RUS:gram</ta>
            <ta e="T1003" id="Seg_13658" s="T1002">RUS:gram</ta>
            <ta e="T1008" id="Seg_13659" s="T1007">RUS:gram</ta>
            <ta e="T1056" id="Seg_13660" s="T1017">TURK:core</ta>
            <ta e="T1022" id="Seg_13661" s="T1021">TURK:disc</ta>
            <ta e="T1034" id="Seg_13662" s="T1033">TURK:cult</ta>
            <ta e="T1039" id="Seg_13663" s="T1038">TURK:core</ta>
            <ta e="T1041" id="Seg_13664" s="T1040">TURK:cult</ta>
            <ta e="T1045" id="Seg_13665" s="T1044">TURK:cult</ta>
            <ta e="T1049" id="Seg_13666" s="T1048">RUS:gram</ta>
            <ta e="T1051" id="Seg_13667" s="T1050">TURK:cult</ta>
            <ta e="T1053" id="Seg_13668" s="T1052">TURK:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T58" id="Seg_13669" s="T57">RUS:int</ta>
            <ta e="T108" id="Seg_13670" s="T107">RUS:int</ta>
            <ta e="T155" id="Seg_13671" s="T153">RUS:int</ta>
            <ta e="T312" id="Seg_13672" s="T311">RUS:int</ta>
            <ta e="T321" id="Seg_13673" s="T319">RUS:int</ta>
            <ta e="T393" id="Seg_13674" s="T388">RUS:int</ta>
            <ta e="T432" id="Seg_13675" s="T431">RUS:int</ta>
            <ta e="T438" id="Seg_13676" s="T437">RUS:int</ta>
            <ta e="T679" id="Seg_13677" s="T678">RUS:int</ta>
            <ta e="T681" id="Seg_13678" s="T680">RUS:int</ta>
            <ta e="T762" id="Seg_13679" s="T761">RUS:int</ta>
            <ta e="T874" id="Seg_13680" s="T871">RUS:int</ta>
            <ta e="T970" id="Seg_13681" s="T969">RUS:int</ta>
            <ta e="T973" id="Seg_13682" s="T972">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T7" id="Seg_13683" s="T0">"Вчера, когда ты покупала масло, ты его взвесила?"</ta>
            <ta e="T9" id="Seg_13684" s="T7">Взвесила.</ta>
            <ta e="T11" id="Seg_13685" s="T9">И сколько [его было]?</ta>
            <ta e="T13" id="Seg_13686" s="T11">Пять[сот] граммов.</ta>
            <ta e="T15" id="Seg_13687" s="T13">Ты деньги принесла?</ta>
            <ta e="T16" id="Seg_13688" s="T15">"Принесла.</ta>
            <ta e="T20" id="Seg_13689" s="T16">Два рубля".</ta>
            <ta e="T23" id="Seg_13690" s="T20">Мы разговариваем и смеемся.</ta>
            <ta e="T27" id="Seg_13691" s="T23">И водку пьем.</ta>
            <ta e="T30" id="Seg_13692" s="T27">И едим хлеб.</ta>
            <ta e="T33" id="Seg_13693" s="T30">И едим мясо.</ta>
            <ta e="T37" id="Seg_13694" s="T33">И курим табак.</ta>
            <ta e="T45" id="Seg_13695" s="T37">Рядом с нами человек живет, его сын пришел к нам.</ta>
            <ta e="T50" id="Seg_13696" s="T45">В ящик залез и в шкаф залез. [?]</ta>
            <ta e="T55" id="Seg_13697" s="T50">Я сказала: "Что ты ищешь? (?)"</ta>
            <ta e="T59" id="Seg_13698" s="T55">"Мне Ванька сказал посмотреть".</ta>
            <ta e="T62" id="Seg_13699" s="T59">Я его прогнала.</ta>
            <ta e="T66" id="Seg_13700" s="T62">"(?) Мише, иди отсюда!"</ta>
            <ta e="T69" id="Seg_13701" s="T66">Потом он сегодня пришел.</ta>
            <ta e="T78" id="Seg_13702" s="T69">Мешок с орехами стоит, он взял нож и разрезал мешок.</ta>
            <ta e="T83" id="Seg_13703" s="T78">Орехи взял, а сам бросил [внутрь] сахар. [?]</ta>
            <ta e="T89" id="Seg_13704" s="T83">В нашем доме (?).</ta>
            <ta e="T91" id="Seg_13705" s="T89">Он украл (орехи?).</ta>
            <ta e="T97" id="Seg_13706" s="T91">А он показывает: вот (орехи?).</ta>
            <ta e="T100" id="Seg_13707" s="T97">А она говорит:</ta>
            <ta e="T103" id="Seg_13708" s="T100">"Я его убью!"</ta>
            <ta e="T108" id="Seg_13709" s="T103">"А что вы раньше не сказали?"</ta>
            <ta e="T115" id="Seg_13710" s="T108">Вчера меня позвали мои родственники:</ta>
            <ta e="T118" id="Seg_13711" s="T115">"Приходи к нам есть.</ta>
            <ta e="T124" id="Seg_13712" s="T118">Ребенок умер, (?) пошел".</ta>
            <ta e="T128" id="Seg_13713" s="T124">Я говорю: "Приду".</ta>
            <ta e="T134" id="Seg_13714" s="T128">А сама не пошла.</ta>
            <ta e="T144" id="Seg_13715" s="T134">Я невестку звала-звала, она не пошла, и я не пошла.</ta>
            <ta e="T152" id="Seg_13716" s="T144">Там очень много [пьют] водки, а я не хочу водки.</ta>
            <ta e="T157" id="Seg_13717" s="T152">Я не хочу ее пить.</ta>
            <ta e="T166" id="Seg_13718" s="T157">А он рассердился, говорит: "Я даже бабушку (?) видел". [?]</ta>
            <ta e="T171" id="Seg_13719" s="T166">Ваш теленок научился. [?]</ta>
            <ta e="T176" id="Seg_13720" s="T171">Он ложится, сиди, он ложится, не мычит. [?]</ta>
            <ta e="T179" id="Seg_13721" s="T176">Сегодня тепло стало.</ta>
            <ta e="T183" id="Seg_13722" s="T179">Снег (мягкий?) был.</ta>
            <ta e="T188" id="Seg_13723" s="T183">Снег скоро станет водой [=растает]</ta>
            <ta e="T191" id="Seg_13724" s="T188">Тогда трава будет расти.</ta>
            <ta e="T194" id="Seg_13725" s="T191">Всякие цветы вырастут.</ta>
            <ta e="T198" id="Seg_13726" s="T194">Деревья красивые станут.</ta>
            <ta e="T201" id="Seg_13727" s="T198">Зелеными станут.</ta>
            <ta e="T208" id="Seg_13728" s="T201">Тогда лошади, коровы, овцы пойдут есть траву.</ta>
            <ta e="T212" id="Seg_13729" s="T208">Потом трава большая вырастет.</ta>
            <ta e="T218" id="Seg_13730" s="T212">Ее надо травой косить.</ta>
            <ta e="T224" id="Seg_13731" s="T218">Потом она на солнце высохнет, и надо будет ее собрать.</ta>
            <ta e="T228" id="Seg_13732" s="T224">Люди (гуляют?) очень хорошо.</ta>
            <ta e="T230" id="Seg_13733" s="T228">Лето пришло.</ta>
            <ta e="T233" id="Seg_13734" s="T230">Очень жарко.</ta>
            <ta e="T237" id="Seg_13735" s="T233">Я (могла?) (?) спать.</ta>
            <ta e="T240" id="Seg_13736" s="T237">Потом рано встала.</ta>
            <ta e="T245" id="Seg_13737" s="T240">А [весь] день я работала.</ta>
            <ta e="T252" id="Seg_13738" s="T245">Корову доила, варила, (делала?), потом разговаривала.</ta>
            <ta e="T257" id="Seg_13739" s="T252">Я рано встала и разожгла огонь.</ta>
            <ta e="T266" id="Seg_13740" s="T257">Мне надо ехать в Агинское, у меня деньги лежат в банке.</ta>
            <ta e="T274" id="Seg_13741" s="T266">Взять надо и людям отдать, у которых я [их] брала.</ta>
            <ta e="T281" id="Seg_13742" s="T274">Я много говорю, теперь много говорят.</ta>
            <ta e="T284" id="Seg_13743" s="T281">Ты очень умный.</ta>
            <ta e="T287" id="Seg_13744" s="T284">(…) всегда мне.</ta>
            <ta e="T290" id="Seg_13745" s="T287">Мне надо говорить.</ta>
            <ta e="T297" id="Seg_13746" s="T290">Теперь ты говори, а я не буду говорить.</ta>
            <ta e="T305" id="Seg_13747" s="T297">Я не знаю, что говорить, ты говори!</ta>
            <ta e="T317" id="Seg_13748" s="T305">Что я буду все время говорить, я лягу. [?]</ta>
            <ta e="T325" id="Seg_13749" s="T317">Моя хата с краю, ничего не знаю.</ta>
            <ta e="T327" id="Seg_13750" s="T325">Выдумай(?) что-нибудь.</ta>
            <ta e="T329" id="Seg_13751" s="T327">Соври что-нибудь.</ta>
            <ta e="T332" id="Seg_13752" s="T329">Мой отец далеко ушел.</ta>
            <ta e="T342" id="Seg_13753" s="T332">А мне очень хорошо, никто не бьет, я (?) шумлю.</ta>
            <ta e="T344" id="Seg_13754" s="T342">Пою (?).</ta>
            <ta e="T346" id="Seg_13755" s="T344">Танцую.</ta>
            <ta e="T350" id="Seg_13756" s="T346">И не слушаюсь матери.</ta>
            <ta e="T357" id="Seg_13757" s="T350">Мои родители (?), что я…</ta>
            <ta e="T360" id="Seg_13758" s="T357">Что я шумлю.</ta>
            <ta e="T371" id="Seg_13759" s="T360">Мой приятель сегодня вечером водку (пил?), песни пел.</ta>
            <ta e="T375" id="Seg_13760" s="T371">Он дрался, (ругался?).</ta>
            <ta e="T385" id="Seg_13761" s="T375">Эля одна живет, [к ней] все время сватаются, а она все не идет.</ta>
            <ta e="T393" id="Seg_13762" s="T385">Ни за кого не идет.</ta>
            <ta e="T398" id="Seg_13763" s="T393">У нее есть родители.</ta>
            <ta e="T406" id="Seg_13764" s="T398">Говорят: "Иди замуж", а она никак не идет.</ta>
            <ta e="T416" id="Seg_13765" s="T406">Пришел ко мне свататься некрасивый мужчина, я не хочу [за него] идти.</ta>
            <ta e="T422" id="Seg_13766" s="T416">Отец говорит: "Почему ты не идешь?</ta>
            <ta e="T429" id="Seg_13767" s="T422">Ну не (ходи?)", потом я (убежала?). </ta>
            <ta e="T436" id="Seg_13768" s="T429">Мой первый муж пришел ко мне свататься.</ta>
            <ta e="T447" id="Seg_13769" s="T436">Ему мало лет, и мне мало лет.</ta>
            <ta e="T458" id="Seg_13770" s="T447">Я говорю: "Иди к отцу и матери, посватайся, они [меня] отдадут".</ta>
            <ta e="T461" id="Seg_13771" s="T458">Нет, не дадут.</ta>
            <ta e="T470" id="Seg_13772" s="T461">Потом меня уговорили, и мы поехали к нему на лошади.</ta>
            <ta e="T475" id="Seg_13773" s="T470">Потом мать приехала туда к нам.</ta>
            <ta e="T481" id="Seg_13774" s="T475">Он туда пошел траву косить.</ta>
            <ta e="T487" id="Seg_13775" s="T481">Потом пошел на реку и там умер.</ta>
            <ta e="T497" id="Seg_13776" s="T487">Потом я поехала домой и очень долго думала [о нем], сердце болело.</ta>
            <ta e="T501" id="Seg_13777" s="T497">Плакала, жалела о нем.</ta>
            <ta e="T508" id="Seg_13778" s="T501">Потом мать (?).</ta>
            <ta e="T514" id="Seg_13779" s="T508">Потом я стала песню/песни петь.</ta>
            <ta e="T519" id="Seg_13780" s="T514">Потом я пришла домой, жила.</ta>
            <ta e="T524" id="Seg_13781" s="T519">Потом я пошла в Пермяково на праздник.</ta>
            <ta e="T527" id="Seg_13782" s="T524">Мы играли (?), плясали.</ta>
            <ta e="T529" id="Seg_13783" s="T527">Играли на гармошке.</ta>
            <ta e="T531" id="Seg_13784" s="T529">Пели песни.</ta>
            <ta e="T536" id="Seg_13785" s="T531">Потом туда пришел мужчина, (Нагорный?)</ta>
            <ta e="T539" id="Seg_13786" s="T536">У него усы большие, длинные.</ta>
            <ta e="T543" id="Seg_13787" s="T539">И рыжие, сам он рыжий.</ta>
            <ta e="T551" id="Seg_13788" s="T543">Потом я домой пришла, и он пришел меня сватать.</ta>
            <ta e="T559" id="Seg_13789" s="T551">Я говорю: "Твой сын (?) как мой". [?]</ta>
            <ta e="T560" id="Seg_13790" s="T559">Такой.</ta>
            <ta e="T567" id="Seg_13791" s="T560">Ну я с тобой не буду жить, а с сыном.</ta>
            <ta e="T571" id="Seg_13792" s="T567">Потом моя мать говорит: "Иди.</ta>
            <ta e="T574" id="Seg_13793" s="T571">У него денег много.</ta>
            <ta e="T578" id="Seg_13794" s="T574">Очень хороший человек".</ta>
            <ta e="T582" id="Seg_13795" s="T578">А я стою на своем: "Нет!"</ta>
            <ta e="T587" id="Seg_13796" s="T582">"Да иди, живи с ним".</ta>
            <ta e="T591" id="Seg_13797" s="T587">Один человек пришел меня сватать.</ta>
            <ta e="T595" id="Seg_13798" s="T591">А я не иду.</ta>
            <ta e="T597" id="Seg_13799" s="T595">А мать…</ta>
            <ta e="T610" id="Seg_13800" s="T597">"Не пойду, он некрасивый, у него ноги кривые, усы рыжие.</ta>
            <ta e="T619" id="Seg_13801" s="T610">Его язык не говорит, он плохо говорит, его уши не слышат.</ta>
            <ta e="T632" id="Seg_13802" s="T619">А моя мать говорит: "Иди, у него есть корова, и лошадь, и много хлеба".</ta>
            <ta e="T636" id="Seg_13803" s="T632">А я говорю:</ta>
            <ta e="T653" id="Seg_13804" s="T636">"Мне с лошадью не жить и [с] коровой не жить, тебе надо, так ты и иди!"</ta>
            <ta e="T657" id="Seg_13805" s="T653">В станицу пришел один человек.</ta>
            <ta e="T660" id="Seg_13806" s="T657">Его жена бросила.</ta>
            <ta e="T664" id="Seg_13807" s="T660">Он стал ко мне свататься.</ta>
            <ta e="T671" id="Seg_13808" s="T664">Мать стала варить мясо, их кормить.</ta>
            <ta e="T678" id="Seg_13809" s="T671">А я говорю: идти или не идти?</ta>
            <ta e="T683" id="Seg_13810" s="T678">"Хочешь — иди, хочешь — не иди".</ta>
            <ta e="T692" id="Seg_13811" s="T683">Ты теперь сама большая, сама знаешь, что делать".</ta>
            <ta e="T697" id="Seg_13812" s="T692">Потом этот человек пришел.</ta>
            <ta e="T699" id="Seg_13813" s="T697">Сел рядом со мной.</ta>
            <ta e="T703" id="Seg_13814" s="T699">И давай ко мне свататься.</ta>
            <ta e="T707" id="Seg_13815" s="T703">И стал богу молиться.</ta>
            <ta e="T714" id="Seg_13816" s="T707">"Я тебя не брошу, и (?) мы будем жить хорошо".</ta>
            <ta e="T724" id="Seg_13817" s="T714">Потом он запряг лошадь, и я с ним поехала в Пермяково.</ta>
            <ta e="T729" id="Seg_13818" s="T724">Я купила водки, мы принесли…</ta>
            <ta e="T733" id="Seg_13819" s="T729">Мы на стол собрали, людей позвали.</ta>
            <ta e="T741" id="Seg_13820" s="T733">[Они] выпили водки, поели, потом мы поехали в станицу.</ta>
            <ta e="T745" id="Seg_13821" s="T741">Потом я (?) три года.</ta>
            <ta e="T751" id="Seg_13822" s="T745">Потом война была, его забрали на войну.</ta>
            <ta e="T757" id="Seg_13823" s="T751">Я была беременная, девочкой.</ta>
            <ta e="T763" id="Seg_13824" s="T757">Потом он пришел с войны, он был раненый.</ta>
            <ta e="T770" id="Seg_13825" s="T763">Потом мы еще жили, я опять забеременела.</ta>
            <ta e="T779" id="Seg_13826" s="T770">Потом он (?) он меня выгнал и ее взял.</ta>
            <ta e="T784" id="Seg_13827" s="T779">У меня еще один сын был.</ta>
            <ta e="T792" id="Seg_13828" s="T784">Потом я домой приехала, к родителям.</ta>
            <ta e="T801" id="Seg_13829" s="T792">Потом умерли дочь и сын, я одна осталась.</ta>
            <ta e="T807" id="Seg_13830" s="T801">Я жила (?), опять один мужчина ко мне посватался.</ta>
            <ta e="T810" id="Seg_13831" s="T807">Я пошла к нему.</ta>
            <ta e="T820" id="Seg_13832" s="T810">Я жила… один, два, три, четыре, пять, шесть, семь лет жила.</ta>
            <ta e="T823" id="Seg_13833" s="T820">Двое детей было.</ta>
            <ta e="T830" id="Seg_13834" s="T823">(…) был плохой, водку пил, дрался, ругался.</ta>
            <ta e="T838" id="Seg_13835" s="T830">Когда я его бросила, он вырвал мне волосы.</ta>
            <ta e="T844" id="Seg_13836" s="T838">(…) сильно бил, аж кровь текла.</ta>
            <ta e="T849" id="Seg_13837" s="T844">Потом я его бросила.</ta>
            <ta e="T853" id="Seg_13838" s="T849">У меня один сын был.</ta>
            <ta e="T864" id="Seg_13839" s="T853">Теперь я всем говорю: Я не [пойду] замуж, буду жить одна, с сыном, с собственным сыном.</ta>
            <ta e="T870" id="Seg_13840" s="T864">Потом один парень стал меня сватать.</ta>
            <ta e="T874" id="Seg_13841" s="T870">Я не хотела идти [за него].</ta>
            <ta e="T876" id="Seg_13842" s="T874">Я не (?).</ta>
            <ta e="T879" id="Seg_13843" s="T876">У него лицо толстое.</ta>
            <ta e="T881" id="Seg_13844" s="T879">"Не пойду [за него]".</ta>
            <ta e="T886" id="Seg_13845" s="T881">Он говорит: "Давай жить с тобой".</ta>
            <ta e="T890" id="Seg_13846" s="T886">Ну, я думала, думала, потом пошла [за него].</ta>
            <ta e="T899" id="Seg_13847" s="T890">Потом опять: "Брошу, говорю, тебя, не буду с тобой жить".</ta>
            <ta e="T905" id="Seg_13848" s="T899">Он стал плакать, очень сильно плакал.</ta>
            <ta e="T914" id="Seg_13849" s="T905">Потом я говорю: "(?) дом пусть поставит, потом пусть идет".</ta>
            <ta e="T925" id="Seg_13850" s="T914">И так и было: он дом поставил и ушел, я одна осталась.</ta>
            <ta e="T936" id="Seg_13851" s="T925">Теперь я все одна живу и живу, уже двенадцать (лет?) живу.</ta>
            <ta e="T939" id="Seg_13852" s="T936">Когда он от меня ушел.</ta>
            <ta e="T950" id="Seg_13853" s="T939">Потом мы с сыном деревья топором рубили, чтобы дом поставить.</ta>
            <ta e="T954" id="Seg_13854" s="T950">Мы возили на лошади.</ta>
            <ta e="T957" id="Seg_13855" s="T954">И построили дом.</ta>
            <ta e="T965" id="Seg_13856" s="T957">Мужчины рубили [бревна] топором, а я для них шкуры (выделывала?).</ta>
            <ta e="T974" id="Seg_13857" s="T965">Потом возили (?), чтобы сделать пол и потолок.</ta>
            <ta e="T980" id="Seg_13858" s="T974">А я большие шкуры выделывала.</ta>
            <ta e="T983" id="Seg_13859" s="T980">И сапоги шила.</ta>
            <ta e="T988" id="Seg_13860" s="T983">Он тоже мне помогал.</ta>
            <ta e="T996" id="Seg_13861" s="T988">Он вешал шкуры и не знал, я учила его.</ta>
            <ta e="T1002" id="Seg_13862" s="T996">Я сама мужчинам сапоги шила.</ta>
            <ta e="T1005" id="Seg_13863" s="T1002">И парки шила.</ta>
            <ta e="T1007" id="Seg_13864" s="T1005">[Их] овечьих шкур.</ta>
            <ta e="T1010" id="Seg_13865" s="T1007">И шапки шила.</ta>
            <ta e="T1012" id="Seg_13866" s="T1010">Рубашки шила.</ta>
            <ta e="T1014" id="Seg_13867" s="T1012">Штаны шила.</ta>
            <ta e="T1018" id="Seg_13868" s="T1014">Я его очень хорошо одевала. [?]</ta>
            <ta e="T1019" id="Seg_13869" s="T1018">Я одевала.</ta>
            <ta e="T1027" id="Seg_13870" s="T1019">Люди смотрят: очень красивый человек стал.</ta>
            <ta e="T1033" id="Seg_13871" s="T1027">Потом мы землю пахали.</ta>
            <ta e="T1035" id="Seg_13872" s="T1033">Рожь сеяли.</ta>
            <ta e="T1041" id="Seg_13873" s="T1035">Она выросла, очень хорошая была рожь.</ta>
            <ta e="T1046" id="Seg_13874" s="T1041">Потом мы муку делали, на мельницу ходили.</ta>
            <ta e="T1048" id="Seg_13875" s="T1046">Хлеб пекли.</ta>
            <ta e="T1052" id="Seg_13876" s="T1048">И (пшеницу?) сеяли.</ta>
            <ta e="T1054" id="Seg_13877" s="T1052">Хорошо.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_13878" s="T0">"Yesterday, when you took butter, did you weigh [it]?"</ta>
            <ta e="T9" id="Seg_13879" s="T7">I weighed [it].</ta>
            <ta e="T11" id="Seg_13880" s="T9">How much [it was]?</ta>
            <ta e="T13" id="Seg_13881" s="T11">Five [hundred] grams.</ta>
            <ta e="T15" id="Seg_13882" s="T13">Did you bring the money?</ta>
            <ta e="T16" id="Seg_13883" s="T15">"I brought [it].</ta>
            <ta e="T20" id="Seg_13884" s="T16">Two roubles."</ta>
            <ta e="T23" id="Seg_13885" s="T20">We are speaking and laughing.</ta>
            <ta e="T27" id="Seg_13886" s="T23">And we are drinking vodka.</ta>
            <ta e="T30" id="Seg_13887" s="T27">And eat bread.</ta>
            <ta e="T33" id="Seg_13888" s="T30">And we are eating meat.</ta>
            <ta e="T37" id="Seg_13889" s="T33">And we are smoking tobacco.</ta>
            <ta e="T45" id="Seg_13890" s="T37">A man lives next to me, his son came to us.</ta>
            <ta e="T50" id="Seg_13891" s="T45">He looked in the drawer, and he looked in the cupboard. [?]</ta>
            <ta e="T55" id="Seg_13892" s="T50">I said: "What to you look for? (?)"</ta>
            <ta e="T59" id="Seg_13893" s="T55">"Van'ka told me to look."</ta>
            <ta e="T62" id="Seg_13894" s="T59">I chased him away.</ta>
            <ta e="T66" id="Seg_13895" s="T62">"(?) to Misha, get out of here!"</ta>
            <ta e="T69" id="Seg_13896" s="T66">Then he came today.</ta>
            <ta e="T78" id="Seg_13897" s="T69">There was a bag with nuts, he took a knife and cut the bag.</ta>
            <ta e="T83" id="Seg_13898" s="T78">He took the nuts and himself threw [in] sugar. [?]</ta>
            <ta e="T89" id="Seg_13899" s="T83">In our house (?).</ta>
            <ta e="T91" id="Seg_13900" s="T89">He stole (nuts?).</ta>
            <ta e="T97" id="Seg_13901" s="T91">And he pointed: here are the (nuts?).</ta>
            <ta e="T100" id="Seg_13902" s="T97">And she says:</ta>
            <ta e="T103" id="Seg_13903" s="T100">"I'll kill him!"</ta>
            <ta e="T108" id="Seg_13904" s="T103">"Why didn't you say earlier?"</ta>
            <ta e="T115" id="Seg_13905" s="T108">Yesterday [my] relatives invited me:</ta>
            <ta e="T118" id="Seg_13906" s="T115">"Come to us to eat.</ta>
            <ta e="T124" id="Seg_13907" s="T118">[A] child died, and (?) went (?)."</ta>
            <ta e="T128" id="Seg_13908" s="T124">And I said: "I'll come."</ta>
            <ta e="T134" id="Seg_13909" s="T128">But I didn't go.</ta>
            <ta e="T144" id="Seg_13910" s="T134">I called [my] sister/daughter-in-law with me, she didn't go, and I didn't go.</ta>
            <ta e="T152" id="Seg_13911" s="T144">[People drink] much vodka there, and I don't want vodka.</ta>
            <ta e="T157" id="Seg_13912" s="T152">I don't want to drink it.</ta>
            <ta e="T166" id="Seg_13913" s="T157">He was angry, he said: "I even saw my grandmother (?)." [?]</ta>
            <ta e="T171" id="Seg_13914" s="T166">Your calf has learnt [something]. [?]</ta>
            <ta e="T176" id="Seg_13915" s="T171">It lies down, sit, it lies down, doesn't mow. [?]</ta>
            <ta e="T179" id="Seg_13916" s="T176">Today it became warm.</ta>
            <ta e="T183" id="Seg_13917" s="T179">The snow was (soft?).</ta>
            <ta e="T188" id="Seg_13918" s="T183">The snow will soon become water [= will melt].</ta>
            <ta e="T191" id="Seg_13919" s="T188">Then the grass will grow.</ta>
            <ta e="T194" id="Seg_13920" s="T191">All kinds of flowers will grow.</ta>
            <ta e="T198" id="Seg_13921" s="T194">The trees will become beautiful.</ta>
            <ta e="T201" id="Seg_13922" s="T198">They'll become green.</ta>
            <ta e="T208" id="Seg_13923" s="T201">Then the horses, the cows, the sheep will go to eat grass.</ta>
            <ta e="T212" id="Seg_13924" s="T208">Then the grass will grow big.</ta>
            <ta e="T218" id="Seg_13925" s="T212">Then [people] must cut it with scythes.</ta>
            <ta e="T224" id="Seg_13926" s="T218">Then it will dry under the sun, and one must gather it.</ta>
            <ta e="T228" id="Seg_13927" s="T224">People walk very well. [?]</ta>
            <ta e="T230" id="Seg_13928" s="T228">Summer came.</ta>
            <ta e="T233" id="Seg_13929" s="T230">It is very hot.</ta>
            <ta e="T237" id="Seg_13930" s="T233">I (could?) (?) sleep.</ta>
            <ta e="T240" id="Seg_13931" s="T237">Then I stood up early.</ta>
            <ta e="T245" id="Seg_13932" s="T240">During the day I worked.</ta>
            <ta e="T252" id="Seg_13933" s="T245">I milked the cow, I cooked, I (did?), then I spoke.</ta>
            <ta e="T257" id="Seg_13934" s="T252">I got up early and lit fire.</ta>
            <ta e="T266" id="Seg_13935" s="T257">I have to go to Aginskoye, I have money there [=lying] in a bank.</ta>
            <ta e="T274" id="Seg_13936" s="T266">I have to take [it] and give to the people, from whom I had borrowed it.</ta>
            <ta e="T281" id="Seg_13937" s="T274">I speak much, now [people?] speak much.</ta>
            <ta e="T284" id="Seg_13938" s="T281">You are very smart.</ta>
            <ta e="T287" id="Seg_13939" s="T284">(…) always me.</ta>
            <ta e="T290" id="Seg_13940" s="T287">I have to speak.</ta>
            <ta e="T297" id="Seg_13941" s="T290">Now you speak, and I won't speak.</ta>
            <ta e="T305" id="Seg_13942" s="T297">I don't know what to say, you speak!</ta>
            <ta e="T317" id="Seg_13943" s="T305">Why would I always (speak?), I'm lying down. [?]</ta>
            <ta e="T325" id="Seg_13944" s="T317">This is no concern of mine</ta>
            <ta e="T327" id="Seg_13945" s="T325">Find (out?) something.</ta>
            <ta e="T329" id="Seg_13946" s="T327">Lie [me about] something.</ta>
            <ta e="T332" id="Seg_13947" s="T329">My father went far away.</ta>
            <ta e="T342" id="Seg_13948" s="T332">I am very good, nobody beats [me], I (?) make noise.</ta>
            <ta e="T344" id="Seg_13949" s="T342">I sing (?).</ta>
            <ta e="T346" id="Seg_13950" s="T344">I dance.</ta>
            <ta e="T350" id="Seg_13951" s="T346">And I don't listen to my mother.</ta>
            <ta e="T357" id="Seg_13952" s="T350">My parents (?), that I…</ta>
            <ta e="T360" id="Seg_13953" s="T357">That I make noise.</ta>
            <ta e="T371" id="Seg_13954" s="T360">My friend (drank?) vodka yesterday in the evening, he sang songs.</ta>
            <ta e="T375" id="Seg_13955" s="T371">He (fought?), he (scolded?).</ta>
            <ta e="T385" id="Seg_13956" s="T375">Elya lives alone, [people] often want to marry her, but she never agrees.</ta>
            <ta e="T393" id="Seg_13957" s="T385">She does not marry anyone.</ta>
            <ta e="T398" id="Seg_13958" s="T393">She has parents.</ta>
            <ta e="T406" id="Seg_13959" s="T398">They say: "Marry", and she doesn't marry anyone.</ta>
            <ta e="T416" id="Seg_13960" s="T406">A man, which is not handsome, came to marry me, I don't want to marry [him].</ta>
            <ta e="T422" id="Seg_13961" s="T416">My father says: "Why don't you go?</ta>
            <ta e="T429" id="Seg_13962" s="T422">Well, don't (go?)", then I ran (away?).</ta>
            <ta e="T436" id="Seg_13963" s="T429">My first husband came to marry me.</ta>
            <ta e="T447" id="Seg_13964" s="T436">He is young and I am young, [too].</ta>
            <ta e="T458" id="Seg_13965" s="T447">I say: "Go to [my] father and [my] mother, they [will] give me?"</ta>
            <ta e="T461" id="Seg_13966" s="T458">No, they won't give [me].</ta>
            <ta e="T470" id="Seg_13967" s="T461">Then they persuaded me, and we went to him on a horse.</ta>
            <ta e="T475" id="Seg_13968" s="T470">Then my mother came there to us.</ta>
            <ta e="T481" id="Seg_13969" s="T475">He came here to cut grass.</ta>
            <ta e="T487" id="Seg_13970" s="T481">Then he went to the river and died there.</ta>
            <ta e="T497" id="Seg_13971" s="T487">Then I came home and very long remembered [him], my heart ached.</ta>
            <ta e="T501" id="Seg_13972" s="T497">I cried, I felt pity for him.</ta>
            <ta e="T508" id="Seg_13973" s="T501">Then my mother (?).</ta>
            <ta e="T514" id="Seg_13974" s="T508">They I started singing song(s).</ta>
            <ta e="T519" id="Seg_13975" s="T514">Then I came home, I lived [there].</ta>
            <ta e="T524" id="Seg_13976" s="T519">Then I went to Permyakovo, to a party.</ta>
            <ta e="T527" id="Seg_13977" s="T524">We played (?), danced.</ta>
            <ta e="T529" id="Seg_13978" s="T527">We played accordeon.</ta>
            <ta e="T531" id="Seg_13979" s="T529">We sang songs.</ta>
            <ta e="T536" id="Seg_13980" s="T531">Then that man came, Nagorny (?)</ta>
            <ta e="T539" id="Seg_13981" s="T536">His moustache is big, long.</ta>
            <ta e="T543" id="Seg_13982" s="T539">And red, himself is red.</ta>
            <ta e="T551" id="Seg_13983" s="T543">Then I came home, and he came to ask me in marriage.</ta>
            <ta e="T559" id="Seg_13984" s="T551">I say: "Your son (?) like mine." [?]</ta>
            <ta e="T560" id="Seg_13985" s="T559">So</ta>
            <ta e="T567" id="Seg_13986" s="T560">Then I'll live not with you, but with [my] son.</ta>
            <ta e="T571" id="Seg_13987" s="T567">Then my mother said: "Go [to him].</ta>
            <ta e="T574" id="Seg_13988" s="T571">He has much money.</ta>
            <ta e="T578" id="Seg_13989" s="T574">[He's] a very good person."</ta>
            <ta e="T582" id="Seg_13990" s="T578">But I'm insisting: "No!"</ta>
            <ta e="T587" id="Seg_13991" s="T582">"Go, live with him!"</ta>
            <ta e="T591" id="Seg_13992" s="T587">One man came to ask [me] in marriage.</ta>
            <ta e="T595" id="Seg_13993" s="T591">And I don't go.</ta>
            <ta e="T597" id="Seg_13994" s="T595">And mother…</ta>
            <ta e="T610" id="Seg_13995" s="T597">"I won't go, he's not handsome, his legs are bandy, his moustache is red.</ta>
            <ta e="T619" id="Seg_13996" s="T610">His tongue doesn't speak, he doesn't speak well, his ears don't hear.</ta>
            <ta e="T632" id="Seg_13997" s="T619">And my mother says: "Go, he has a cow, and a horse, and much grain."</ta>
            <ta e="T636" id="Seg_13998" s="T632">And I say:</ta>
            <ta e="T653" id="Seg_13999" s="T636">"I don't have to live with a horse, I don't have to live [with] a cow, if you need [it], you go!".</ta>
            <ta e="T657" id="Seg_14000" s="T653">One man came to the stanitsa.</ta>
            <ta e="T660" id="Seg_14001" s="T657">[His] wife left him.</ta>
            <ta e="T664" id="Seg_14002" s="T660">He began to ask me in marriage.</ta>
            <ta e="T671" id="Seg_14003" s="T664">My mother started cooking meat, feed them.</ta>
            <ta e="T678" id="Seg_14004" s="T671">And I say [= ask her]: to go or not to go?</ta>
            <ta e="T683" id="Seg_14005" s="T678">"If you want, go, if you don't want, don't go.</ta>
            <ta e="T692" id="Seg_14006" s="T683">You are now grown-up, you know yourself, what to do."</ta>
            <ta e="T697" id="Seg_14007" s="T692">Then this man came.</ta>
            <ta e="T699" id="Seg_14008" s="T697">Sat down next to me.</ta>
            <ta e="T703" id="Seg_14009" s="T699">And began himself to ask me to marry him.</ta>
            <ta e="T707" id="Seg_14010" s="T703">And began to pray to God. </ta>
            <ta e="T714" id="Seg_14011" s="T707">"I won't leave you, and (?) we'll live well."</ta>
            <ta e="T724" id="Seg_14012" s="T714">Then he harnessed the horse, I went with him to Permyakovo.</ta>
            <ta e="T729" id="Seg_14013" s="T724">I bought vodka, we brought…</ta>
            <ta e="T733" id="Seg_14014" s="T729">We laid the table, invited people.</ta>
            <ta e="T741" id="Seg_14015" s="T733">They drank vodka and ate, then we went to the stanitsa.</ta>
            <ta e="T745" id="Seg_14016" s="T741">Then I l(?) three years.</ta>
            <ta e="T751" id="Seg_14017" s="T745">Then there was the war, he was taken away to the war.</ta>
            <ta e="T757" id="Seg_14018" s="T751">I was/became pregnant, [I had] a girl.</ta>
            <ta e="T763" id="Seg_14019" s="T757">Then he came from the war, he was wounded.</ta>
            <ta e="T770" id="Seg_14020" s="T763">Then we lived some more, I became pregnant again.</ta>
            <ta e="T779" id="Seg_14021" s="T770">Then he (?), chased me away and took her.</ta>
            <ta e="T784" id="Seg_14022" s="T779">I had one more son.</ta>
            <ta e="T792" id="Seg_14023" s="T784">Then I came home, to my parents.</ta>
            <ta e="T801" id="Seg_14024" s="T792">Then they died, [my] daughter and [my] son, I remained alone.</ta>
            <ta e="T807" id="Seg_14025" s="T801">I lived (?), a man again came to ask me in marriage.</ta>
            <ta e="T810" id="Seg_14026" s="T807">I went with him.</ta>
            <ta e="T820" id="Seg_14027" s="T810">I lived… one, two, three, four, five, six, seven years.</ta>
            <ta e="T823" id="Seg_14028" s="T820">[We had] two children.</ta>
            <ta e="T830" id="Seg_14029" s="T823">(…) wasn't good, he drank vodka, fought, scolded.</ta>
            <ta e="T838" id="Seg_14030" s="T830">When I left, he tore my hairs.</ta>
            <ta e="T844" id="Seg_14031" s="T838">(…) he beat [me] hard, I was even bleeding.</ta>
            <ta e="T849" id="Seg_14032" s="T844">Then I left him.</ta>
            <ta e="T853" id="Seg_14033" s="T849">I had one son.</ta>
            <ta e="T864" id="Seg_14034" s="T853">Now I tell everyone: I [won't marry] anyone, I'll live alone, with my son, my own son.</ta>
            <ta e="T870" id="Seg_14035" s="T864">Then one boy wanted to marry me.</ta>
            <ta e="T874" id="Seg_14036" s="T870">I didn't want to marry [him].</ta>
            <ta e="T876" id="Seg_14037" s="T874">I don't (?).</ta>
            <ta e="T879" id="Seg_14038" s="T876">His face is thick.</ta>
            <ta e="T881" id="Seg_14039" s="T879">"I won't marry [him]."</ta>
            <ta e="T886" id="Seg_14040" s="T881">He says: "Let's live together with you."</ta>
            <ta e="T890" id="Seg_14041" s="T886">Well, I thought for a long time, then I married [him].</ta>
            <ta e="T899" id="Seg_14042" s="T890">Then again: "I'll leave you, I say, I won't live with you."</ta>
            <ta e="T905" id="Seg_14043" s="T899">He began to cry, he cried very hard.</ta>
            <ta e="T914" id="Seg_14044" s="T905">Then I say: "(?) let him build a house, then let him go."</ta>
            <ta e="T925" id="Seg_14045" s="T914">And so was: he built a house and went away, I left alone.</ta>
            <ta e="T936" id="Seg_14046" s="T925">Now I live always alone, already twelve (years?) I live [so].</ta>
            <ta e="T939" id="Seg_14047" s="T936">[Since] when he left [me].</ta>
            <ta e="T950" id="Seg_14048" s="T939">Then my son and I, we cut wood to build the house.</ta>
            <ta e="T954" id="Seg_14049" s="T950">We carried on a horse.</ta>
            <ta e="T957" id="Seg_14050" s="T954">And we (built?) a house.</ta>
            <ta e="T965" id="Seg_14051" s="T957">The men cut [the logs] with axes, and I (dressed?) skins for them.</ta>
            <ta e="T974" id="Seg_14052" s="T965">Then they carried (?), to make the floor and the ceiling.</ta>
            <ta e="T980" id="Seg_14053" s="T974">And I dressed large skins.</ta>
            <ta e="T983" id="Seg_14054" s="T980">And sew boots.</ta>
            <ta e="T988" id="Seg_14055" s="T983">He helped me, too.</ta>
            <ta e="T996" id="Seg_14056" s="T988">He hung the skin and didn't know, I teached him.</ta>
            <ta e="T1002" id="Seg_14057" s="T996">Myself made boots for men.</ta>
            <ta e="T1005" id="Seg_14058" s="T1002">And sewed coat(s).</ta>
            <ta e="T1007" id="Seg_14059" s="T1005">[From] sheep's skins.</ta>
            <ta e="T1010" id="Seg_14060" s="T1007">And I sew hat(s), too.</ta>
            <ta e="T1012" id="Seg_14061" s="T1010">I sew shirts.</ta>
            <ta e="T1014" id="Seg_14062" s="T1012">I sew pants.</ta>
            <ta e="T1018" id="Seg_14063" s="T1014">I dressed him very well. [?]</ta>
            <ta e="T1019" id="Seg_14064" s="T1018">I dressed.</ta>
            <ta e="T1027" id="Seg_14065" s="T1019">People said: he became a very handsome person.</ta>
            <ta e="T1033" id="Seg_14066" s="T1027">Then we ploughed.</ta>
            <ta e="T1035" id="Seg_14067" s="T1033">We sowed rye.</ta>
            <ta e="T1041" id="Seg_14068" s="T1035">It grew, it was very good rye.</ta>
            <ta e="T1046" id="Seg_14069" s="T1041">Then we made flour, we went to the mill.</ta>
            <ta e="T1048" id="Seg_14070" s="T1046">We baked bread.</ta>
            <ta e="T1052" id="Seg_14071" s="T1048">We sowed (wheat?), too.</ta>
            <ta e="T1054" id="Seg_14072" s="T1052">Good.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_14073" s="T0">"Gestern, als du Butter genommen hast, hast du [sie] abgewogen?"</ta>
            <ta e="T9" id="Seg_14074" s="T7">Ich habe [sie] abgewogen.</ta>
            <ta e="T11" id="Seg_14075" s="T9">Wie viel [war es]?</ta>
            <ta e="T13" id="Seg_14076" s="T11">Fünf[hundert] Gramm.</ta>
            <ta e="T15" id="Seg_14077" s="T13">Hast du das Geld gebracht?</ta>
            <ta e="T16" id="Seg_14078" s="T15">"Ich habe [es] gebracht.</ta>
            <ta e="T20" id="Seg_14079" s="T16">Zwei Rubel."</ta>
            <ta e="T23" id="Seg_14080" s="T20">Wir reden und lachen.</ta>
            <ta e="T27" id="Seg_14081" s="T23">Und wir trinken Wodka.</ta>
            <ta e="T30" id="Seg_14082" s="T27">Und essen Brot.</ta>
            <ta e="T33" id="Seg_14083" s="T30">Und wir essen Fleisch.</ta>
            <ta e="T37" id="Seg_14084" s="T33">Und wir rauchen Tabak.</ta>
            <ta e="T45" id="Seg_14085" s="T37">Ein Mann wohnt neben mir, sein Sohn ist zu uns gekommen.</ta>
            <ta e="T50" id="Seg_14086" s="T45">Er schaute in die Schublade und er schaute in den Schrank. [?]</ta>
            <ta e="T55" id="Seg_14087" s="T50">Ich sagte: "Was suchst du? (?)"</ta>
            <ta e="T59" id="Seg_14088" s="T55">"Van'ka sagte mir, ich solle gucken."</ta>
            <ta e="T62" id="Seg_14089" s="T59">Ich verjagte ihn.</ta>
            <ta e="T66" id="Seg_14090" s="T62">"(?) zu Mischa, geh raus hier!"</ta>
            <ta e="T69" id="Seg_14091" s="T66">Dann kam er heute.</ta>
            <ta e="T78" id="Seg_14092" s="T69">Dort stand ein Sack mit Nüsse, er nahm ein Messer und schnitt dem Sack.</ta>
            <ta e="T83" id="Seg_14093" s="T78">Er nahm die Nüsse und selbst warf Zucker [hinein]. [?]</ta>
            <ta e="T89" id="Seg_14094" s="T83">Im unserem Haus (?).</ta>
            <ta e="T91" id="Seg_14095" s="T89">Er stahl (Nüsse?).</ta>
            <ta e="T97" id="Seg_14096" s="T91">Und er zeigte: hier sind die (Nüsse?).</ta>
            <ta e="T100" id="Seg_14097" s="T97">Und sie sagt:</ta>
            <ta e="T103" id="Seg_14098" s="T100">"Ich werde ihn töten!"</ta>
            <ta e="T108" id="Seg_14099" s="T103">"Warum habt ihr es nicht früher gesagt?"</ta>
            <ta e="T115" id="Seg_14100" s="T108">Gestern luden mich [meine] Verwandten ein:</ta>
            <ta e="T118" id="Seg_14101" s="T115">"Komm zu uns zum Essen.</ta>
            <ta e="T124" id="Seg_14102" s="T118">[Ein] Kind ist gekommen und (?) ging (?)."</ta>
            <ta e="T128" id="Seg_14103" s="T124">Und ich sagte: "Ich komme."</ta>
            <ta e="T134" id="Seg_14104" s="T128">Aber ich bin nicht gegangen.</ta>
            <ta e="T144" id="Seg_14105" s="T134">Ich rief meine Schwägerin/Schwiegertochter mit mir, sie ist nicht gegangen und ich bin nicht gegangen.</ta>
            <ta e="T152" id="Seg_14106" s="T144">[Die Leute trinken] viel Wodka da und ich will keinen Wodka.</ta>
            <ta e="T157" id="Seg_14107" s="T152">Ich will ihn nicht trinken.</ta>
            <ta e="T166" id="Seg_14108" s="T157">Er war böse, er sagte: "Ich sah sogar meine Großmutter sogar (?)." [?]</ta>
            <ta e="T171" id="Seg_14109" s="T166">Euer Kalb hat [etwas] gelernt. [?]</ta>
            <ta e="T176" id="Seg_14110" s="T171">Es legt sich, sitz, es legt sich, es muht nicht. [?]</ta>
            <ta e="T179" id="Seg_14111" s="T176">Heute ist es warm geworden.</ta>
            <ta e="T183" id="Seg_14112" s="T179">Der Schnee war (weich?).</ta>
            <ta e="T188" id="Seg_14113" s="T183">Der Schnee wird bald zu Wasser werden [=wird schmelzen].</ta>
            <ta e="T191" id="Seg_14114" s="T188">Dann wird das Gras wachsen.</ta>
            <ta e="T194" id="Seg_14115" s="T191">All möglichen Blumen werden wachsen.</ta>
            <ta e="T198" id="Seg_14116" s="T194">Die Bäume werden schön werden.</ta>
            <ta e="T201" id="Seg_14117" s="T198">Sie werden grün werden.</ta>
            <ta e="T208" id="Seg_14118" s="T201">Dann werden die Pferde, die Kühe, die Schafe gehen, um Gras zu fressen.</ta>
            <ta e="T212" id="Seg_14119" s="T208">Dann wird das Gras hoch wachsen.</ta>
            <ta e="T218" id="Seg_14120" s="T212">Dann must man es mit Sensen schneiden.</ta>
            <ta e="T224" id="Seg_14121" s="T218">Dann trocknet es in der Sonne und man muss es einsammeln.</ta>
            <ta e="T228" id="Seg_14122" s="T224">Die Leute gehen [=spazieren?] sehr gut.</ta>
            <ta e="T230" id="Seg_14123" s="T228">Es ist Sommer geworden.</ta>
            <ta e="T233" id="Seg_14124" s="T230">Es ist sehr heiß.</ta>
            <ta e="T237" id="Seg_14125" s="T233">Ich (kannte?) (?) schlafen.</ta>
            <ta e="T240" id="Seg_14126" s="T237">Dann stand ich früh auf.</ta>
            <ta e="T245" id="Seg_14127" s="T240">Am Tag arbeitete ich.</ta>
            <ta e="T252" id="Seg_14128" s="T245">Ich molk die Kuh, ich kochte, ich (machte?), dann sprach ich.</ta>
            <ta e="T257" id="Seg_14129" s="T252">Ich stand auf früh und machte Feuer.</ta>
            <ta e="T266" id="Seg_14130" s="T257">Ich muss nach Aginskoje gehen, ich habe Geld in einem Bank.</ta>
            <ta e="T274" id="Seg_14131" s="T266">Ich muss [es] nehmen und den Leuten geben, von denen ich habe [es] genommen.</ta>
            <ta e="T281" id="Seg_14132" s="T274">Ich rede viel, jetzt sprechen [die Leute?] viel.</ta>
            <ta e="T284" id="Seg_14133" s="T281">Du bist sehr schlau.</ta>
            <ta e="T287" id="Seg_14134" s="T284">(…) mich immer.</ta>
            <ta e="T290" id="Seg_14135" s="T287">Ich muss sprechen.</ta>
            <ta e="T297" id="Seg_14136" s="T290">Jetzt sprichst du und ich spreche nicht.</ta>
            <ta e="T305" id="Seg_14137" s="T297">Ich weiß nicht, was ich sagen soll, sprich du!</ta>
            <ta e="T317" id="Seg_14138" s="T305">Warum sollte ich immer (sprechen?), ich liege mich. [?]</ta>
            <ta e="T325" id="Seg_14139" s="T317">Meine Hütte steht am Rande, ich weiß von nichts.</ta>
            <ta e="T327" id="Seg_14140" s="T325">Denke dir etwas (aus?).</ta>
            <ta e="T329" id="Seg_14141" s="T327">Lüge [mir] etwas.</ta>
            <ta e="T332" id="Seg_14142" s="T329">Mein Vater ging weit weg.</ta>
            <ta e="T342" id="Seg_14143" s="T332">Ich bin sehr gut, niemand schlägt [mich], ich (?) mache Lärm.</ta>
            <ta e="T344" id="Seg_14144" s="T342">Ich singe (?).</ta>
            <ta e="T346" id="Seg_14145" s="T344">Ich tanze.</ta>
            <ta e="T350" id="Seg_14146" s="T346">Und ich höre nicht auf meine Mutter.</ta>
            <ta e="T357" id="Seg_14147" s="T350">Meine Eltern (?), dass ich…</ta>
            <ta e="T360" id="Seg_14148" s="T357">Dass ich Lärm mache.</ta>
            <ta e="T371" id="Seg_14149" s="T360">Mein Freund (trank?) gestern Abend Wodka, er sang Lieder.</ta>
            <ta e="T375" id="Seg_14150" s="T371">Er (kämpfte?), er (schimpfte?).</ta>
            <ta e="T385" id="Seg_14151" s="T375">Elja lebt alleine, [Leute] wollen sie oft heiraten, aber sie willigt nie ein.</ta>
            <ta e="T393" id="Seg_14152" s="T385">Sie heiratet niemanden.</ta>
            <ta e="T398" id="Seg_14153" s="T393">Sie hat Eltern.</ta>
            <ta e="T406" id="Seg_14154" s="T398">Sie sagen: "Heirate", aber sie heiratet niemanden.</ta>
            <ta e="T416" id="Seg_14155" s="T406">Ein Mann, der unattraktiv ist, kam, um mich zu heiraten, ich will [ihn] nicht heiraten.</ta>
            <ta e="T422" id="Seg_14156" s="T416">Der Vater sagt: "Warum gehst du nicht?</ta>
            <ta e="T429" id="Seg_14157" s="T422">Gut, dann geh (nicht?)", ich (rannte?) dann (weg?).</ta>
            <ta e="T436" id="Seg_14158" s="T429">Mein erster Mann kam, um mich zu heiraten.</ta>
            <ta e="T447" id="Seg_14159" s="T436">Er ist jung und ich bin [auch] jung.</ta>
            <ta e="T458" id="Seg_14160" s="T447">Ich sage: "Gehe zu [meinem] Vater und [meiner] Mutter, sie werden [mich] geben?"</ta>
            <ta e="T461" id="Seg_14161" s="T458">Nein, sie werden [mich] nicht geben.</ta>
            <ta e="T470" id="Seg_14162" s="T461">Dann überredeten sie mich und wir ritten auf einem Pferd zu ihm.</ta>
            <ta e="T475" id="Seg_14163" s="T470">Dann kam meine Mutter dorthin zu uns.</ta>
            <ta e="T481" id="Seg_14164" s="T475">Er kam dorthin, um Gras zu schneiden.</ta>
            <ta e="T487" id="Seg_14165" s="T481">Dann ging er zum Fluss und starb dort.</ta>
            <ta e="T497" id="Seg_14166" s="T487">Dann kam ich nach Hause und erinnerte [ihn] sehr lange, mein Herz tat weh.</ta>
            <ta e="T501" id="Seg_14167" s="T497">Ich weinte, ich trauerte um ihn.</ta>
            <ta e="T508" id="Seg_14168" s="T501">Dann (?) meine Mutter.</ta>
            <ta e="T514" id="Seg_14169" s="T508">Dann fing ich an, Lied(er) zu singen.</ta>
            <ta e="T519" id="Seg_14170" s="T514">Dann kam ich zu Hause, ich lebte [dort].</ta>
            <ta e="T524" id="Seg_14171" s="T519">Dann ging ich nach Permjakovo, auf ein Fest.</ta>
            <ta e="T527" id="Seg_14172" s="T524">Wir spielten (?), tanzten.</ta>
            <ta e="T529" id="Seg_14173" s="T527">Wir spielten Akkordeon.</ta>
            <ta e="T531" id="Seg_14174" s="T529">Wir sangen Lieder.</ta>
            <ta e="T536" id="Seg_14175" s="T531">Dann kam dieser Mann, Nagorny. [?]</ta>
            <ta e="T539" id="Seg_14176" s="T536">Sein Schnurbart is sind groß, lang.</ta>
            <ta e="T543" id="Seg_14177" s="T539">Und rot, er selbst ist rot.</ta>
            <ta e="T551" id="Seg_14178" s="T543">Dann kam ich nach Hause und er kam, um um meine Hand anzuhalten.</ta>
            <ta e="T559" id="Seg_14179" s="T551">Ich sage: "Dein Sohn (?) wie mein." [?]</ta>
            <ta e="T560" id="Seg_14180" s="T559">So.</ta>
            <ta e="T567" id="Seg_14181" s="T560">Dann werde ich nicht mit dir leben, sondern mit [meinem] Sohn.</ta>
            <ta e="T571" id="Seg_14182" s="T567">Dann sagte meine Mutter: "Geh [zu ihm].</ta>
            <ta e="T574" id="Seg_14183" s="T571">Er hat viel Geld.</ta>
            <ta e="T578" id="Seg_14184" s="T574">[Er ist] ein sehr guter Mensch."</ta>
            <ta e="T582" id="Seg_14185" s="T578">Aber ich beharre: "Nein!"</ta>
            <ta e="T587" id="Seg_14186" s="T582">"Geh, leb mit ihm!"</ta>
            <ta e="T591" id="Seg_14187" s="T587">Ein Mann kam, um um [meine] Hand anzuhalten.</ta>
            <ta e="T595" id="Seg_14188" s="T591">Und ich gehe nicht.</ta>
            <ta e="T597" id="Seg_14189" s="T595">Und Mutter…</ta>
            <ta e="T610" id="Seg_14190" s="T597">"Ich gehe nicht, er ist unattraktiv, seine Beine sind krumm, sein Schnurrbart ist rot.</ta>
            <ta e="T619" id="Seg_14191" s="T610">Seine Zunge spricht nicht, er spricht nicht gut, seine Ohren hören nicht.</ta>
            <ta e="T632" id="Seg_14192" s="T619">Und meine Mutter sagt: "Geh, er hat eine Kuh und ein Pferd und viel Korn."</ta>
            <ta e="T636" id="Seg_14193" s="T632">Und ich sage:</ta>
            <ta e="T653" id="Seg_14194" s="T636">"Ich muss nicht mit einem Pferd leben, ich muss nicht [mit] einer Kuh leben (?), wenn du [es] brauchst, geh du!"</ta>
            <ta e="T657" id="Seg_14195" s="T653">Ein Mann kam in die Staniza.</ta>
            <ta e="T660" id="Seg_14196" s="T657">[Seine] Frau hat ihn verlassen.</ta>
            <ta e="T664" id="Seg_14197" s="T660">Er fing an um meine Hand anzuhalten.</ta>
            <ta e="T671" id="Seg_14198" s="T664">Meine Mutter fing an, Fleisch zu kochen, ihnen essen zu geben.</ta>
            <ta e="T678" id="Seg_14199" s="T671">Und ich sage [= frage sie]: Gehen oder nicht gehen?</ta>
            <ta e="T683" id="Seg_14200" s="T678">"Wenn du willst, geh, wenn du nicht willst, geh nicht.</ta>
            <ta e="T692" id="Seg_14201" s="T683">Jetzt bist du groß, weißt du selbst, was zu tun ist."</ta>
            <ta e="T697" id="Seg_14202" s="T692">Dann kam dieser Mann.</ta>
            <ta e="T699" id="Seg_14203" s="T697">Setzte sich neben mich.</ta>
            <ta e="T703" id="Seg_14204" s="T699">Und fing an um meine Hand anzuhalten.</ta>
            <ta e="T707" id="Seg_14205" s="T703">Und begann zu Gott zu beten.</ta>
            <ta e="T714" id="Seg_14206" s="T707">"Ich werde ich nicht verlassen und (?) wir werden gut leben."</ta>
            <ta e="T724" id="Seg_14207" s="T714">Dann spannte er das Pferd an, ich ging mit ihm nach Permjakovo.</ta>
            <ta e="T729" id="Seg_14208" s="T724">Ich kaufte Wodka, wir brachten…</ta>
            <ta e="T733" id="Seg_14209" s="T729">Wir deckten den Tisch, luden Leute ein.</ta>
            <ta e="T741" id="Seg_14210" s="T733">Sie tranken Wodka und aßen, dann gingen wir in die Staniza.</ta>
            <ta e="T745" id="Seg_14211" s="T741">Dann (?) ich drei Jahre.</ta>
            <ta e="T751" id="Seg_14212" s="T745">Dann kam der Krieg, er wurde in den Krieg eingezogen.</ta>
            <ta e="T757" id="Seg_14213" s="T751">Ich war schwanger, [ich bekam] ein Mädchen.</ta>
            <ta e="T763" id="Seg_14214" s="T757">Dann kam er aus dem Krieg, er war verwundet.</ta>
            <ta e="T770" id="Seg_14215" s="T763">Dann lebten wir noch etwas länger, ich wurde wieder schwanger.</ta>
            <ta e="T779" id="Seg_14216" s="T770">Dann er (?), jagte mich weg und nahm sie.</ta>
            <ta e="T784" id="Seg_14217" s="T779">Ich bekam noch einen Sohn.</ta>
            <ta e="T792" id="Seg_14218" s="T784">Dann kam ich nach Hause, zu meinen Eltern.</ta>
            <ta e="T801" id="Seg_14219" s="T792">Dann starben sie, [meine] Tochter und [mein] Sohn, ich blieb alleine.</ta>
            <ta e="T807" id="Seg_14220" s="T801">Ich lebte (?), wieder kam ein Mann, um um meine Hand anzuhalten.</ta>
            <ta e="T810" id="Seg_14221" s="T807">Ich ging mit ihm.</ta>
            <ta e="T820" id="Seg_14222" s="T810">Ich lebte… eins, zwei, drei, vier, fünf, sechs, sieben Jahre.</ta>
            <ta e="T823" id="Seg_14223" s="T820">[Wir hatten] zwei Kinder.</ta>
            <ta e="T830" id="Seg_14224" s="T823">(…) war nicht gut, er trank Wodka, kämpfte, schimpfte.</ta>
            <ta e="T838" id="Seg_14225" s="T830">Als ich ging, zog er mir an den Haaren.</ta>
            <ta e="T844" id="Seg_14226" s="T838">(…) er schlug [mich] doll, ich blutete sogar.</ta>
            <ta e="T849" id="Seg_14227" s="T844">Dann verließ ich ihn.</ta>
            <ta e="T853" id="Seg_14228" s="T849">Ich hatte einen Sohn.</ta>
            <ta e="T864" id="Seg_14229" s="T853">Jetzt sage ich jedem: Ich [heiratete] niemanden, ich lebe alleine, mit meinem Sohn, meinem eigenen Sohn.</ta>
            <ta e="T870" id="Seg_14230" s="T864">Dann wollte mich ein Junge heiraten.</ta>
            <ta e="T874" id="Seg_14231" s="T870">Ich wollte [ihn] nicht heiraten.</ta>
            <ta e="T876" id="Seg_14232" s="T874">Ich (?) nicht.</ta>
            <ta e="T879" id="Seg_14233" s="T876">Sein Gesicht ist dick.</ta>
            <ta e="T881" id="Seg_14234" s="T879">"Ich heirate [ihn] nicht."</ta>
            <ta e="T886" id="Seg_14235" s="T881">Er sagt: "Lass uns zusammen leben."</ta>
            <ta e="T890" id="Seg_14236" s="T886">Nun, ich dachte lange Zeit nach, dann heiratete ich [ihn].</ta>
            <ta e="T899" id="Seg_14237" s="T890">Dann wieder: "Ich verlasse dich, sage ich, ich werde nicht mit dir leben."</ta>
            <ta e="T905" id="Seg_14238" s="T899">Er fing an zu weinen, er weinte sehr doll.</ta>
            <ta e="T914" id="Seg_14239" s="T905">Dann sage ich: "(?) lass ihn ein Haus bauen, dann lass ihn gehen."</ta>
            <ta e="T925" id="Seg_14240" s="T914">Und so war es: Er baute ein Haus und ging weg, ich blieb alleine.</ta>
            <ta e="T936" id="Seg_14241" s="T925">Jetzt lebe ich immer alleine, schon zwölf (Jahre?) lebe ich [so].</ta>
            <ta e="T939" id="Seg_14242" s="T936">[Seit] er [mich] verlassen hat.</ta>
            <ta e="T950" id="Seg_14243" s="T939">Dann mein Sohn und ich, wir fällten Bäume, um das Haus zu bauen.</ta>
            <ta e="T954" id="Seg_14244" s="T950">Wir fahrten mit einem Pferd.</ta>
            <ta e="T957" id="Seg_14245" s="T954">Und wir (bauten?) ein Haus.</ta>
            <ta e="T965" id="Seg_14246" s="T957">Die Männer spalten [die Stämme] mit Äxten und ich (beizte?) Felle für sie.</ta>
            <ta e="T974" id="Seg_14247" s="T965">Dann farhten sie (?), um den Boden und die Decke zu machen.</ta>
            <ta e="T980" id="Seg_14248" s="T974">Und ich beizte große Felle.</ta>
            <ta e="T983" id="Seg_14249" s="T980">Und nähte Stiefel.</ta>
            <ta e="T988" id="Seg_14250" s="T983">Er half mir auch.</ta>
            <ta e="T996" id="Seg_14251" s="T988">Er hängt das Fell auf und konnte [es] nicht, ich brachte es ihm bei.</ta>
            <ta e="T1002" id="Seg_14252" s="T996">Ich selbst machte Stiefel für die Männer.</ta>
            <ta e="T1005" id="Seg_14253" s="T1002">Und nähte Mäntel.</ta>
            <ta e="T1007" id="Seg_14254" s="T1005">[Aus] Schaffell.</ta>
            <ta e="T1010" id="Seg_14255" s="T1007">Und ich nähte auch Hüte.</ta>
            <ta e="T1012" id="Seg_14256" s="T1010">Ich nähe Hemden.</ta>
            <ta e="T1014" id="Seg_14257" s="T1012">Ich nähe Hosen.</ta>
            <ta e="T1018" id="Seg_14258" s="T1014">Ich zog ihn sehr gut an. [?]</ta>
            <ta e="T1019" id="Seg_14259" s="T1018">Ich zog an.</ta>
            <ta e="T1027" id="Seg_14260" s="T1019">Die Leute sagten: Er wurde zu einem sehr attraktiven Menschen.</ta>
            <ta e="T1033" id="Seg_14261" s="T1027">Dann pflügten wird.</ta>
            <ta e="T1035" id="Seg_14262" s="T1033">Dann säten wir Roggen.</ta>
            <ta e="T1041" id="Seg_14263" s="T1035">Er wuchs, es war sehr guter Roggen.</ta>
            <ta e="T1046" id="Seg_14264" s="T1041">Dann machten wir Mehl, wir gingen zur Mühle.</ta>
            <ta e="T1048" id="Seg_14265" s="T1046">Wir buken Brot.</ta>
            <ta e="T1052" id="Seg_14266" s="T1048">Wir säten auch (Weizen?).</ta>
            <ta e="T1054" id="Seg_14267" s="T1052">Gut.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T30" id="Seg_14268" s="T27">[GVY:] amnia = amniabaʔ</ta>
            <ta e="T37" id="Seg_14269" s="T33">[GVY:] nʼiʔleʔbəbeʔ</ta>
            <ta e="T50" id="Seg_14270" s="T45">[GVY:] pĭʔpi, poʔpi? It is also not clear, which kind of ящик and шкаф are meant here.</ta>
            <ta e="T66" id="Seg_14271" s="T62">[GVY:] Măllia 'says'? Unclear.</ta>
            <ta e="T83" id="Seg_14272" s="T78">[GVY:] The most part of the following fragment remains obscure.</ta>
            <ta e="T91" id="Seg_14273" s="T89">[GVY:] sanu?</ta>
            <ta e="T115" id="Seg_14274" s="T108">[GVY:] Or 'I was invited to my relatives'.</ta>
            <ta e="T152" id="Seg_14275" s="T144">[GVY:] araj - an adjective from ara, see D 5b-6a; here means something like "it is very drunk there"?</ta>
            <ta e="T166" id="Seg_14276" s="T157">[GVY:] Unclear.</ta>
            <ta e="T171" id="Seg_14277" s="T166">[GVY:] šiʔnʼile 'you.PL-ACC/DAT/LOC' = Russian "у вас"?</ta>
            <ta e="T224" id="Seg_14278" s="T218">[GVY:] here da is prosodically linked to the second clause.</ta>
            <ta e="T228" id="Seg_14279" s="T224">[GVY:] Unclear. Maybe mănliaʔi '(people) say'?</ta>
            <ta e="T230" id="Seg_14280" s="T228">[GVY:] PKZ uses kö 'winter' as 'year' and pʼe 'year' as 'summer'.</ta>
            <ta e="T287" id="Seg_14281" s="T284">[GVY:] Unclear</ta>
            <ta e="T317" id="Seg_14282" s="T305">[GVY:] Unclear.</ta>
            <ta e="T327" id="Seg_14283" s="T325">выдумай</ta>
            <ta e="T342" id="Seg_14284" s="T332">[GVY:] balʔ- may be an attempt to say баловаться.</ta>
            <ta e="T344" id="Seg_14285" s="T342">[GVY:] nüjnə 'song'?</ta>
            <ta e="T357" id="Seg_14286" s="T350">[GVY:] were angry?</ta>
            <ta e="T393" id="Seg_14287" s="T385">[GVY:] šindinidə</ta>
            <ta e="T487" id="Seg_14288" s="T481">[GVY:] here dĭn [tĭn]</ta>
            <ta e="T527" id="Seg_14289" s="T524">[AAV] no PST marker in suʔmibeʔ ?</ta>
            <ta e="T536" id="Seg_14290" s="T531">[GVY:] Is Nagorny a name?</ta>
            <ta e="T559" id="Seg_14291" s="T551">[GVY:] Maybe the tape is damaged.</ta>
            <ta e="T582" id="Seg_14292" s="T578">[GVY:] The interpretation (and transcription) of this and the following sentence is hypothetical.</ta>
            <ta e="T597" id="Seg_14293" s="T595">[GVY:] Unfinished sentence.</ta>
            <ta e="T653" id="Seg_14294" s="T636">[GVY:] măna … ej amnosʼtə - a calque or Russian "мне … не жить"</ta>
            <ta e="T664" id="Seg_14295" s="T660">[GVY:] it is not clear whether monoʔko- takes an accusative or a dative object - in the latter case măna should be glossed as I.LAT.</ta>
            <ta e="T671" id="Seg_14296" s="T664">[GVY:] The sentence is not clear. </ta>
            <ta e="T692" id="Seg_14297" s="T683">[GVY:] boštə</ta>
            <ta e="T745" id="Seg_14298" s="T741">[GVY:] manobiam =măn obiam 'I ((unknown))-PST-1SG'?</ta>
            <ta e="T757" id="Seg_14299" s="T751">[GVY:] dĭʔ?</ta>
            <ta e="T770" id="Seg_14300" s="T763">[GVY:] nanəksəbi</ta>
            <ta e="T801" id="Seg_14301" s="T792">[GVY:] nʼim? măn-nu 'and I'?</ta>
            <ta e="T807" id="Seg_14302" s="T801">[GVY:] oʔkʼe = oʔb kö 'one year'?</ta>
            <ta e="T864" id="Seg_14303" s="T853">[GVY:] a break in the recording.</ta>
            <ta e="T936" id="Seg_14304" s="T925">[GVY:] ki 'month' = kö 'winter/year'?</ta>
            <ta e="T988" id="Seg_14305" s="T983">[GVY:] kabaržəbi = kabažərbi?</ta>
            <ta e="T996" id="Seg_14306" s="T988">[GVY:] The first part is not clear. edəmbi = edəbi or kuba tedəmbi?</ta>
            <ta e="T1007" id="Seg_14307" s="T1005">[AAV] Tape begins toslow down.</ta>
            <ta e="T1012" id="Seg_14308" s="T1010">[GVY:] kujnektə = kujnekʔi [shirt=PL]?</ta>
            <ta e="T1018" id="Seg_14309" s="T1014">[AAV] Tape very slow.</ta>
            <ta e="T1035" id="Seg_14310" s="T1033">[AAV] Irregular speed.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T1057" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T1055" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T1058" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T979" />
            <conversion-tli id="T980" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T985" />
            <conversion-tli id="T986" />
            <conversion-tli id="T987" />
            <conversion-tli id="T988" />
            <conversion-tli id="T989" />
            <conversion-tli id="T990" />
            <conversion-tli id="T991" />
            <conversion-tli id="T992" />
            <conversion-tli id="T993" />
            <conversion-tli id="T994" />
            <conversion-tli id="T995" />
            <conversion-tli id="T996" />
            <conversion-tli id="T997" />
            <conversion-tli id="T998" />
            <conversion-tli id="T999" />
            <conversion-tli id="T1000" />
            <conversion-tli id="T1001" />
            <conversion-tli id="T1002" />
            <conversion-tli id="T1003" />
            <conversion-tli id="T1004" />
            <conversion-tli id="T1005" />
            <conversion-tli id="T1006" />
            <conversion-tli id="T1007" />
            <conversion-tli id="T1008" />
            <conversion-tli id="T1009" />
            <conversion-tli id="T1010" />
            <conversion-tli id="T1011" />
            <conversion-tli id="T1012" />
            <conversion-tli id="T1013" />
            <conversion-tli id="T1014" />
            <conversion-tli id="T1015" />
            <conversion-tli id="T1016" />
            <conversion-tli id="T1017" />
            <conversion-tli id="T1056" />
            <conversion-tli id="T1018" />
            <conversion-tli id="T1019" />
            <conversion-tli id="T1020" />
            <conversion-tli id="T1021" />
            <conversion-tli id="T1022" />
            <conversion-tli id="T1023" />
            <conversion-tli id="T1024" />
            <conversion-tli id="T1025" />
            <conversion-tli id="T1026" />
            <conversion-tli id="T1027" />
            <conversion-tli id="T1028" />
            <conversion-tli id="T1029" />
            <conversion-tli id="T1030" />
            <conversion-tli id="T1031" />
            <conversion-tli id="T1032" />
            <conversion-tli id="T1033" />
            <conversion-tli id="T1034" />
            <conversion-tli id="T1035" />
            <conversion-tli id="T1036" />
            <conversion-tli id="T1037" />
            <conversion-tli id="T1038" />
            <conversion-tli id="T1039" />
            <conversion-tli id="T1040" />
            <conversion-tli id="T1041" />
            <conversion-tli id="T1042" />
            <conversion-tli id="T1043" />
            <conversion-tli id="T1044" />
            <conversion-tli id="T1045" />
            <conversion-tli id="T1046" />
            <conversion-tli id="T1047" />
            <conversion-tli id="T1048" />
            <conversion-tli id="T1049" />
            <conversion-tli id="T1050" />
            <conversion-tli id="T1051" />
            <conversion-tli id="T1052" />
            <conversion-tli id="T1053" />
            <conversion-tli id="T1054" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
