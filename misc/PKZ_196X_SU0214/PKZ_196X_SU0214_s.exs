<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID155C85A8-F387-FC5F-9D8C-F7E0459EE1EB">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_SU0214.wav" />
         <referenced-file url="PKZ_196X_SU0214.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_196X_SU0214\PKZ_196X_SU0214.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1345</ud-information>
            <ud-information attribute-name="# HIAT:w">868</ud-information>
            <ud-information attribute-name="# e">862</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">4</ud-information>
            <ud-information attribute-name="# HIAT:u">183</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="2.09" type="appl" />
         <tli id="T1" time="3.11" type="appl" />
         <tli id="T2" time="4.13" type="appl" />
         <tli id="T3" time="5.15" type="appl" />
         <tli id="T4" time="6.17" type="appl" />
         <tli id="T5" time="7.165" type="appl" />
         <tli id="T6" time="7.97" type="appl" />
         <tli id="T7" time="8.775" type="appl" />
         <tli id="T8" time="9.58" type="appl" />
         <tli id="T9" time="10.385" type="appl" />
         <tli id="T10" time="11.19" type="appl" />
         <tli id="T11" time="12.162" type="appl" />
         <tli id="T12" time="14.31991978489805" />
         <tli id="T13" time="15.318" type="appl" />
         <tli id="T14" time="16.226" type="appl" />
         <tli id="T15" time="17.134" type="appl" />
         <tli id="T16" time="18.042" type="appl" />
         <tli id="T17" time="18.95" type="appl" />
         <tli id="T18" time="19.975" type="appl" />
         <tli id="T19" time="20.785" type="appl" />
         <tli id="T20" time="21.527" type="appl" />
         <tli id="T21" time="22.133" type="appl" />
         <tli id="T22" time="23.34653588766898" />
         <tli id="T23" time="24.545" type="appl" />
         <tli id="T24" time="25.465" type="appl" />
         <tli id="T25" time="26.385" type="appl" />
         <tli id="T26" time="27.02" type="appl" />
         <tli id="T27" time="28.046509560086637" />
         <tli id="T28" time="28.92" type="appl" />
         <tli id="T29" time="32.87981581895586" />
         <tli id="T30" time="33.88" type="appl" />
         <tli id="T31" time="34.53" type="appl" />
         <tli id="T32" time="35.18" type="appl" />
         <tli id="T33" time="36.345" type="appl" />
         <tli id="T34" time="37.45" type="appl" />
         <tli id="T35" time="38.555" type="appl" />
         <tli id="T36" time="39.79977705579206" />
         <tli id="T37" time="40.74" type="appl" />
         <tli id="T38" time="41.65" type="appl" />
         <tli id="T39" time="42.745" type="appl" />
         <tli id="T40" time="43.695" type="appl" />
         <tli id="T41" time="45.15974703114497" />
         <tli id="T42" time="46.385" type="appl" />
         <tli id="T43" time="47.525" type="appl" />
         <tli id="T44" time="48.55306135633729" />
         <tli id="T45" time="49.785" type="appl" />
         <tli id="T46" time="50.9" type="appl" />
         <tli id="T47" time="51.678" type="appl" />
         <tli id="T48" time="52.377" type="appl" />
         <tli id="T49" time="53.075" type="appl" />
         <tli id="T50" time="53.773" type="appl" />
         <tli id="T51" time="54.472" type="appl" />
         <tli id="T52" time="55.75968765404436" />
         <tli id="T53" time="56.869" type="appl" />
         <tli id="T54" time="57.858" type="appl" />
         <tli id="T55" time="58.847" type="appl" />
         <tli id="T56" time="59.836" type="appl" />
         <tli id="T57" time="60.825" type="appl" />
         <tli id="T58" time="63.799642617073715" />
         <tli id="T59" time="64.713" type="appl" />
         <tli id="T60" time="65.366" type="appl" />
         <tli id="T61" time="66.019" type="appl" />
         <tli id="T62" time="66.672" type="appl" />
         <tli id="T63" time="67.324" type="appl" />
         <tli id="T64" time="67.977" type="appl" />
         <tli id="T65" time="68.63" type="appl" />
         <tli id="T66" time="69.283" type="appl" />
         <tli id="T67" time="69.936" type="appl" />
         <tli id="T68" time="70.804" type="appl" />
         <tli id="T69" time="71.526" type="appl" />
         <tli id="T70" time="72.247" type="appl" />
         <tli id="T71" time="72.969" type="appl" />
         <tli id="T72" time="73.69" type="appl" />
         <tli id="T73" time="74.575" type="appl" />
         <tli id="T74" time="75.369" type="appl" />
         <tli id="T75" time="76.162" type="appl" />
         <tli id="T76" time="76.956" type="appl" />
         <tli id="T77" time="77.75" type="appl" />
         <tli id="T78" time="79.22" type="appl" />
         <tli id="T79" time="82.51287112555083" />
         <tli id="T80" time="83.585" type="appl" />
         <tli id="T81" time="84.609" type="appl" />
         <tli id="T82" time="85.634" type="appl" />
         <tli id="T83" time="86.658" type="appl" />
         <tli id="T84" time="87.683" type="appl" />
         <tli id="T85" time="88.708" type="appl" />
         <tli id="T86" time="89.732" type="appl" />
         <tli id="T87" time="90.757" type="appl" />
         <tli id="T88" time="92.088" type="appl" />
         <tli id="T89" time="93.237" type="appl" />
         <tli id="T90" time="94.385" type="appl" />
         <tli id="T91" time="95.533" type="appl" />
         <tli id="T92" time="96.682" type="appl" />
         <tli id="T93" time="97.83" type="appl" />
         <tli id="T94" time="98.758" type="appl" />
         <tli id="T95" time="99.646" type="appl" />
         <tli id="T96" time="100.535" type="appl" />
         <tli id="T97" time="101.423" type="appl" />
         <tli id="T98" time="102.61275853312421" />
         <tli id="T99" time="103.983" type="appl" />
         <tli id="T100" time="105.317" type="appl" />
         <tli id="T101" time="106.65" type="appl" />
         <tli id="T102" time="107.517" type="appl" />
         <tli id="T103" time="108.183" type="appl" />
         <tli id="T104" time="108.85" type="appl" />
         <tli id="T105" time="109.517" type="appl" />
         <tli id="T106" time="110.183" type="appl" />
         <tli id="T107" time="110.85" type="appl" />
         <tli id="T108" time="111.848" type="appl" />
         <tli id="T109" time="112.781" type="appl" />
         <tli id="T110" time="113.714" type="appl" />
         <tli id="T111" time="114.647" type="appl" />
         <tli id="T112" time="115.7526849279259" />
         <tli id="T113" time="116.929" type="appl" />
         <tli id="T114" time="117.995" type="appl" />
         <tli id="T115" time="120.01932769437599" />
         <tli id="T116" time="120.613" type="appl" />
         <tli id="T117" time="121.356" type="appl" />
         <tli id="T118" time="122.1" type="appl" />
         <tli id="T119" time="122.843" type="appl" />
         <tli id="T120" time="123.586" type="appl" />
         <tli id="T868" time="124.01455804871803" type="intp" />
         <tli id="T121" time="124.58596878034207" />
         <tli id="T122" time="125.387" type="appl" />
         <tli id="T123" time="126.009" type="appl" />
         <tli id="T124" time="126.632" type="appl" />
         <tli id="T125" time="127.255" type="appl" />
         <tli id="T126" time="127.877" type="appl" />
         <tli id="T127" time="129.14594323698557" />
         <tli id="T128" time="130.03" type="appl" />
         <tli id="T129" time="130.674" type="appl" />
         <tli id="T130" time="131.317" type="appl" />
         <tli id="T131" time="132.057" type="appl" />
         <tli id="T132" time="132.687" type="appl" />
         <tli id="T133" time="133.9459163492419" />
         <tli id="T134" time="135.642" type="appl" />
         <tli id="T135" time="137.317" type="appl" />
         <tli id="T136" time="138.797" type="appl" />
         <tli id="T137" time="140.217" type="appl" />
         <tli id="T138" time="141.637" type="appl" />
         <tli id="T139" time="143.85919415191574" />
         <tli id="T140" time="144.622" type="appl" />
         <tli id="T141" time="145.233" type="appl" />
         <tli id="T142" time="145.845" type="appl" />
         <tli id="T143" time="146.457" type="appl" />
         <tli id="T144" time="147.068" type="appl" />
         <tli id="T145" time="147.68" type="appl" />
         <tli id="T146" time="148.392" type="appl" />
         <tli id="T147" time="149.103" type="appl" />
         <tli id="T148" time="149.815" type="appl" />
         <tli id="T149" time="150.527" type="appl" />
         <tli id="T150" time="151.238" type="appl" />
         <tli id="T151" time="152.22581395175143" />
         <tli id="T152" time="153.282" type="appl" />
         <tli id="T153" time="154.373" type="appl" />
         <tli id="T154" time="155.59912838897603" />
         <tli id="T155" time="156.083" type="appl" />
         <tli id="T156" time="156.577" type="appl" />
         <tli id="T157" time="157.07" type="appl" />
         <tli id="T158" time="157.85" type="appl" />
         <tli id="T159" time="158.772" type="appl" />
         <tli id="T160" time="159.674" type="appl" />
         <tli id="T161" time="160.576" type="appl" />
         <tli id="T162" time="161.478" type="appl" />
         <tli id="T163" time="163.42575121368287" />
         <tli id="T164" time="167.497" type="appl" />
         <tli id="T165" time="168.303" type="appl" />
         <tli id="T166" time="169.11" type="appl" />
         <tli id="T167" time="170.683" type="appl" />
         <tli id="T168" time="172.217" type="appl" />
         <tli id="T169" time="174.73902117409813" />
         <tli id="T170" time="176.365" type="appl" />
         <tli id="T171" time="177.981" type="appl" />
         <tli id="T172" time="179.596" type="appl" />
         <tli id="T173" time="181.212" type="appl" />
         <tli id="T174" time="182.827" type="appl" />
         <tli id="T175" time="184.442" type="appl" />
         <tli id="T176" time="186.058" type="appl" />
         <tli id="T177" time="187.673" type="appl" />
         <tli id="T178" time="188.369" type="appl" />
         <tli id="T179" time="188.887" type="appl" />
         <tli id="T180" time="189.406" type="appl" />
         <tli id="T181" time="189.924" type="appl" />
         <tli id="T182" time="190.443" type="appl" />
         <tli id="T183" time="190.962" type="appl" />
         <tli id="T184" time="191.48" type="appl" />
         <tli id="T185" time="191.999" type="appl" />
         <tli id="T186" time="192.834" type="appl" />
         <tli id="T187" time="194.23891194263948" />
         <tli id="T188" time="194.926" type="appl" />
         <tli id="T189" time="195.702" type="appl" />
         <tli id="T190" time="196.479" type="appl" />
         <tli id="T191" time="197.359" type="appl" />
         <tli id="T192" time="198.189" type="appl" />
         <tli id="T193" time="199.014" type="appl" />
         <tli id="T194" time="199.614" type="appl" />
         <tli id="T195" time="200.214" type="appl" />
         <tli id="T196" time="201.0322072223467" />
         <tli id="T197" time="201.742" type="appl" />
         <tli id="T198" time="202.264" type="appl" />
         <tli id="T199" time="202.786" type="appl" />
         <tli id="T200" time="203.309" type="appl" />
         <tli id="T201" time="203.756" type="appl" />
         <tli id="T202" time="204.204" type="appl" />
         <tli id="T203" time="204.652" type="appl" />
         <tli id="T204" time="205.4655157218612" />
         <tli id="T205" time="206.267" type="appl" />
         <tli id="T206" time="207.193" type="appl" />
         <tli id="T207" time="208.12" type="appl" />
         <tli id="T208" time="208.667" type="appl" />
         <tli id="T209" time="209.214" type="appl" />
         <tli id="T210" time="209.761" type="appl" />
         <tli id="T211" time="210.309" type="appl" />
         <tli id="T212" time="210.856" type="appl" />
         <tli id="T213" time="211.403" type="appl" />
         <tli id="T214" time="211.95" type="appl" />
         <tli id="T215" time="212.497" type="appl" />
         <tli id="T216" time="213.044" type="appl" />
         <tli id="T217" time="213.591" type="appl" />
         <tli id="T218" time="214.139" type="appl" />
         <tli id="T219" time="214.686" type="appl" />
         <tli id="T220" time="215.233" type="appl" />
         <tli id="T221" time="215.78" type="appl" />
         <tli id="T222" time="216.768" type="appl" />
         <tli id="T223" time="217.547" type="appl" />
         <tli id="T224" time="218.325" type="appl" />
         <tli id="T225" time="219.103" type="appl" />
         <tli id="T226" time="219.882" type="appl" />
         <tli id="T227" time="222.1320890283068" />
         <tli id="T228" time="223.135" type="appl" />
         <tli id="T229" time="224.38540973933826" />
         <tli id="T230" time="225.22" type="appl" />
         <tli id="T231" time="226.13" type="appl" />
         <tli id="T232" time="227.04" type="appl" />
         <tli id="T233" time="227.95" type="appl" />
         <tli id="T234" time="228.59" type="appl" />
         <tli id="T235" time="229.23" type="appl" />
         <tli id="T236" time="229.87" type="appl" />
         <tli id="T237" time="230.51" type="appl" />
         <tli id="T238" time="231.415" type="appl" />
         <tli id="T239" time="232.29" type="appl" />
         <tli id="T240" time="233.152" type="appl" />
         <tli id="T241" time="233.933" type="appl" />
         <tli id="T242" time="234.715" type="appl" />
         <tli id="T243" time="235.497" type="appl" />
         <tli id="T244" time="236.278" type="appl" />
         <tli id="T245" time="237.87200085891405" />
         <tli id="T246" time="238.697" type="appl" />
         <tli id="T247" time="239.893" type="appl" />
         <tli id="T248" time="241.09" type="appl" />
         <tli id="T249" time="242.287" type="appl" />
         <tli id="T250" time="243.483" type="appl" />
         <tli id="T251" time="245.73862345955635" />
         <tli id="T252" time="246.743" type="appl" />
         <tli id="T253" time="247.617" type="appl" />
         <tli id="T254" time="248.5833210662379" />
         <tli id="T255" time="249.323" type="appl" />
         <tli id="T256" time="250.157" type="appl" />
         <tli id="T257" time="250.99" type="appl" />
         <tli id="T258" time="251.823" type="appl" />
         <tli id="T259" time="252.657" type="appl" />
         <tli id="T260" time="253.83857808648892" />
         <tli id="T261" time="254.68" type="appl" />
         <tli id="T262" time="255.565" type="appl" />
         <tli id="T263" time="256.45" type="appl" />
         <tli id="T264" time="257.335" type="appl" />
         <tli id="T265" time="258.22" type="appl" />
         <tli id="T266" time="258.905" type="appl" />
         <tli id="T267" time="259.559" type="appl" />
         <tli id="T268" time="260.29867210767117" />
         <tli id="T269" time="260.752" type="appl" />
         <tli id="T270" time="261.287" type="appl" />
         <tli id="T271" time="261.864" type="appl" />
         <tli id="T272" time="262.4" type="appl" />
         <tli id="T273" time="262.937" type="appl" />
         <tli id="T274" time="263.727" type="appl" />
         <tli id="T275" time="265.1185149002913" />
         <tli id="T276" time="266.622" type="appl" />
         <tli id="T277" time="267.244" type="appl" />
         <tli id="T278" time="267.747" type="appl" />
         <tli id="T279" time="268.668" type="appl" />
         <tli id="T280" time="269.589" type="appl" />
         <tli id="T281" time="270.51" type="appl" />
         <tli id="T282" time="271.525" type="appl" />
         <tli id="T283" time="272.456" type="appl" />
         <tli id="T284" time="273.388" type="appl" />
         <tli id="T285" time="274.28000003924046" />
         <tli id="T286" time="275.038" type="appl" />
         <tli id="T287" time="275.755" type="appl" />
         <tli id="T288" time="276.472" type="appl" />
         <tli id="T289" time="277.33177981925456" />
         <tli id="T290" time="278.428" type="appl" />
         <tli id="T291" time="279.495" type="appl" />
         <tli id="T292" time="280.562" type="appl" />
         <tli id="T293" time="281.63" type="appl" />
         <tli id="T294" time="282.207" type="appl" />
         <tli id="T295" time="282.784" type="appl" />
         <tli id="T296" time="283.361" type="appl" />
         <tli id="T297" time="283.939" type="appl" />
         <tli id="T298" time="284.516" type="appl" />
         <tli id="T299" time="285.093" type="appl" />
         <tli id="T300" time="288.46505078796025" />
         <tli id="T301" time="289.854" type="appl" />
         <tli id="T302" time="291.349" type="appl" />
         <tli id="T303" time="292.843" type="appl" />
         <tli id="T304" time="294.337" type="appl" />
         <tli id="T305" time="295.831" type="appl" />
         <tli id="T306" time="297.326" type="appl" />
         <tli id="T307" time="298.82" type="appl" />
         <tli id="T308" time="299.49" type="appl" />
         <tli id="T309" time="300.159" type="appl" />
         <tli id="T310" time="300.829" type="appl" />
         <tli id="T311" time="301.498" type="appl" />
         <tli id="T312" time="302.168" type="appl" />
         <tli id="T313" time="302.837" type="appl" />
         <tli id="T314" time="303.68496553140636" />
         <tli id="T315" time="303.937" type="appl" />
         <tli id="T316" time="304.366" type="appl" />
         <tli id="T317" time="304.794" type="appl" />
         <tli id="T318" time="305.223" type="appl" />
         <tli id="T319" time="305.652" type="appl" />
         <tli id="T320" time="306.08" type="appl" />
         <tli id="T321" time="306.509" type="appl" />
         <tli id="T322" time="308.7916035925013" />
         <tli id="T323" time="309.7" type="appl" />
         <tli id="T324" time="310.5" type="appl" />
         <tli id="T325" time="311.3" type="appl" />
         <tli id="T326" time="312.1" type="appl" />
         <tli id="T327" time="312.9649135484353" />
         <tli id="T328" time="314.716" type="appl" />
         <tli id="T329" time="315.748" type="appl" />
         <tli id="T330" time="316.779" type="appl" />
         <tli id="T331" time="318.05821835088506" />
         <tli id="T332" time="318.847" type="appl" />
         <tli id="T333" time="319.593" type="appl" />
         <tli id="T334" time="320.34" type="appl" />
         <tli id="T335" time="320.921" type="appl" />
         <tli id="T336" time="321.486" type="appl" />
         <tli id="T337" time="322.052" type="appl" />
         <tli id="T338" time="322.618" type="appl" />
         <tli id="T339" time="323.184" type="appl" />
         <tli id="T340" time="323.749" type="appl" />
         <tli id="T341" time="324.444849241915" />
         <tli id="T342" time="325.393" type="appl" />
         <tli id="T343" time="326.407" type="appl" />
         <tli id="T344" time="327.811497049817" />
         <tli id="T345" time="328.855" type="appl" />
         <tli id="T346" time="330.105" type="appl" />
         <tli id="T347" time="330.694" type="appl" />
         <tli id="T348" time="331.269" type="appl" />
         <tli id="T349" time="331.843" type="appl" />
         <tli id="T350" time="332.417" type="appl" />
         <tli id="T351" time="332.991" type="appl" />
         <tli id="T352" time="333.566" type="appl" />
         <tli id="T353" time="334.28479412204047" />
         <tli id="T354" time="334.869" type="appl" />
         <tli id="T355" time="335.483" type="appl" />
         <tli id="T356" time="336.097" type="appl" />
         <tli id="T357" time="336.711" type="appl" />
         <tli id="T358" time="337.325" type="appl" />
         <tli id="T359" time="338.435" type="appl" />
         <tli id="T360" time="339.64476409739336" />
         <tli id="T361" time="341.54" type="appl" />
         <tli id="T362" time="342.123" type="appl" />
         <tli id="T363" time="342.656" type="appl" />
         <tli id="T364" time="343.189" type="appl" />
         <tli id="T365" time="343.721" type="appl" />
         <tli id="T366" time="344.254" type="appl" />
         <tli id="T367" time="344.787" type="appl" />
         <tli id="T368" time="346.61139173948766" />
         <tli id="T369" time="347.837" type="appl" />
         <tli id="T370" time="348.965" type="appl" />
         <tli id="T371" time="350.092" type="appl" />
         <tli id="T372" time="351.219" type="appl" />
         <tli id="T373" time="352.347" type="appl" />
         <tli id="T374" time="353.474" type="appl" />
         <tli id="T375" time="354.326" type="appl" />
         <tli id="T376" time="355.124" type="appl" />
         <tli id="T377" time="356.40467021435506" />
         <tli id="T378" time="357.096" type="appl" />
         <tli id="T379" time="357.842" type="appl" />
         <tli id="T380" time="358.588" type="appl" />
         <tli id="T381" time="359.158" type="appl" />
         <tli id="T382" time="359.728" type="appl" />
         <tli id="T383" time="360.298" type="appl" />
         <tli id="T384" time="360.868" type="appl" />
         <tli id="T385" time="361.438" type="appl" />
         <tli id="T386" time="362.008" type="appl" />
         <tli id="T387" time="362.578" type="appl" />
         <tli id="T388" time="363.148" type="appl" />
         <tli id="T389" time="364.73129023825527" />
         <tli id="T390" time="365.882" type="appl" />
         <tli id="T391" time="366.796" type="appl" />
         <tli id="T392" time="368.6779347972216" />
         <tli id="T393" time="369.3" type="appl" />
         <tli id="T394" time="370.0" type="appl" />
         <tli id="T395" time="370.7" type="appl" />
         <tli id="T396" time="371.399" type="appl" />
         <tli id="T397" time="372.099" type="appl" />
         <tli id="T398" time="372.799" type="appl" />
         <tli id="T399" time="373.499" type="appl" />
         <tli id="T400" time="374.14" type="appl" />
         <tli id="T401" time="374.657" type="appl" />
         <tli id="T402" time="375.173" type="appl" />
         <tli id="T403" time="375.689" type="appl" />
         <tli id="T404" time="376.206" type="appl" />
         <tli id="T405" time="376.722" type="appl" />
         <tli id="T406" time="377.352" type="appl" />
         <tli id="T407" time="377.981" type="appl" />
         <tli id="T408" time="378.611" type="appl" />
         <tli id="T409" time="379.24" type="appl" />
         <tli id="T869" time="379.51" type="intp" />
         <tli id="T410" time="379.87" type="appl" />
         <tli id="T411" time="380.499" type="appl" />
         <tli id="T412" time="383.117853909926" />
         <tli id="T413" time="384.548" type="appl" />
         <tli id="T414" time="385.487" type="appl" />
         <tli id="T415" time="386.425" type="appl" />
         <tli id="T416" time="387.145" type="appl" />
         <tli id="T417" time="387.865" type="appl" />
         <tli id="T418" time="388.585" type="appl" />
         <tli id="T419" time="389.7311501979236" />
         <tli id="T420" time="390.527" type="appl" />
         <tli id="T421" time="391.384" type="appl" />
         <tli id="T422" time="392.24" type="appl" />
         <tli id="T423" time="393.097" type="appl" />
         <tli id="T424" time="393.954" type="appl" />
         <tli id="T425" time="394.811" type="appl" />
         <tli id="T426" time="395.368" type="appl" />
         <tli id="T427" time="395.88" type="appl" />
         <tli id="T428" time="396.392" type="appl" />
         <tli id="T429" time="396.905" type="appl" />
         <tli id="T430" time="397.828" type="appl" />
         <tli id="T431" time="398.701" type="appl" />
         <tli id="T432" time="399.573" type="appl" />
         <tli id="T433" time="400.446" type="appl" />
         <tli id="T434" time="401.319" type="appl" />
         <tli id="T435" time="402.387" type="appl" />
         <tli id="T436" time="403.393" type="appl" />
         <tli id="T437" time="404.63106673388603" />
         <tli id="T438" time="405.415" type="appl" />
         <tli id="T439" time="414.25767947568903" />
         <tli id="T440" time="415.01" type="appl" />
         <tli id="T441" time="415.86" type="appl" />
         <tli id="T442" time="416.71" type="appl" />
         <tli id="T443" time="417.645" type="appl" />
         <tli id="T444" time="418.41" type="appl" />
         <tli id="T445" time="419.175" type="appl" />
         <tli id="T446" time="419.94" type="appl" />
         <tli id="T447" time="420.705" type="appl" />
         <tli id="T448" time="424.4242891926208" />
         <tli id="T449" time="425.408" type="appl" />
         <tli id="T450" time="426.266" type="appl" />
         <tli id="T451" time="427.124" type="appl" />
         <tli id="T452" time="427.982" type="appl" />
         <tli id="T453" time="429.23759556352235" />
         <tli id="T454" time="430.081" type="appl" />
         <tli id="T455" time="430.652" type="appl" />
         <tli id="T456" time="431.223" type="appl" />
         <tli id="T457" time="431.794" type="appl" />
         <tli id="T458" time="432.365" type="appl" />
         <tli id="T459" time="432.936" type="appl" />
         <tli id="T460" time="433.507" type="appl" />
         <tli id="T461" time="434.7508980132945" />
         <tli id="T462" time="436.06" type="appl" />
         <tli id="T463" time="437.089" type="appl" />
         <tli id="T464" time="438.119" type="appl" />
         <tli id="T465" time="439.148" type="appl" />
         <tli id="T466" time="440.178" type="appl" />
         <tli id="T467" time="441.207" type="appl" />
         <tli id="T468" time="442.7641864590335" />
         <tli id="T469" time="443.64" type="appl" />
         <tli id="T470" time="444.43" type="appl" />
         <tli id="T471" time="445.219" type="appl" />
         <tli id="T472" time="446.009" type="appl" />
         <tli id="T473" time="448.2974887967735" />
         <tli id="T474" time="449.122" type="appl" />
         <tli id="T475" time="449.854" type="appl" />
         <tli id="T476" time="450.586" type="appl" />
         <tli id="T477" time="451.318" type="appl" />
         <tli id="T478" time="453.5641259616103" />
         <tli id="T479" time="454.475" type="appl" />
         <tli id="T480" time="455.06" type="appl" />
         <tli id="T481" time="455.645" type="appl" />
         <tli id="T482" time="456.231" type="appl" />
         <tli id="T483" time="456.816" type="appl" />
         <tli id="T484" time="458.28409952199564" />
         <tli id="T485" time="459.356" type="appl" />
         <tli id="T486" time="460.053" type="appl" />
         <tli id="T487" time="460.749" type="appl" />
         <tli id="T488" time="461.445" type="appl" />
         <tli id="T489" time="462.141" type="appl" />
         <tli id="T490" time="462.838" type="appl" />
         <tli id="T491" time="465.71072458734784" />
         <tli id="T492" time="466.448" type="appl" />
         <tli id="T493" time="467.276" type="appl" />
         <tli id="T494" time="468.105" type="appl" />
         <tli id="T495" time="468.933" type="appl" />
         <tli id="T496" time="469.761" type="appl" />
         <tli id="T497" time="470.589" type="appl" />
         <tli id="T498" time="471.417" type="appl" />
         <tli id="T499" time="472.245" type="appl" />
         <tli id="T500" time="473.074" type="appl" />
         <tli id="T501" time="473.902" type="appl" />
         <tli id="T502" time="475.950667226828" />
         <tli id="T503" time="476.847" type="appl" />
         <tli id="T504" time="477.625" type="appl" />
         <tli id="T505" time="478.403" type="appl" />
         <tli id="T506" time="479.18" type="appl" />
         <tli id="T507" time="479.991" type="appl" />
         <tli id="T508" time="480.542" type="appl" />
         <tli id="T509" time="481.093" type="appl" />
         <tli id="T510" time="481.644" type="appl" />
         <tli id="T511" time="482.195" type="appl" />
         <tli id="T512" time="482.745" type="appl" />
         <tli id="T513" time="483.296" type="appl" />
         <tli id="T514" time="483.847" type="appl" />
         <tli id="T515" time="484.398" type="appl" />
         <tli id="T516" time="484.949" type="appl" />
         <tli id="T517" time="485.5" type="appl" />
         <tli id="T518" time="486.18" type="appl" />
         <tli id="T519" time="486.86" type="appl" />
         <tli id="T520" time="487.54" type="appl" />
         <tli id="T521" time="488.552" type="appl" />
         <tli id="T522" time="489.448" type="appl" />
         <tli id="T523" time="490.345" type="appl" />
         <tli id="T524" time="491.32" type="appl" />
         <tli id="T525" time="492.29" type="appl" />
         <tli id="T526" time="493.26" type="appl" />
         <tli id="T527" time="494.18999213994465" />
         <tli id="T528" time="494.94" type="appl" />
         <tli id="T529" time="495.645" type="appl" />
         <tli id="T530" time="496.35" type="appl" />
         <tli id="T531" time="497.19721487788485" />
         <tli id="T532" time="497.726" type="appl" />
         <tli id="T533" time="498.232" type="appl" />
         <tli id="T534" time="498.739" type="appl" />
         <tli id="T535" time="499.245" type="appl" />
         <tli id="T536" time="499.751" type="appl" />
         <tli id="T537" time="500.258" type="appl" />
         <tli id="T538" time="500.764" type="appl" />
         <tli id="T539" time="504.497173986108" />
         <tli id="T540" time="505.41" type="appl" />
         <tli id="T541" time="505.99" type="appl" />
         <tli id="T542" time="506.57" type="appl" />
         <tli id="T543" time="507.975" type="appl" />
         <tli id="T544" time="509.285" type="appl" />
         <tli id="T545" time="510.167" type="appl" />
         <tli id="T546" time="510.963" type="appl" />
         <tli id="T547" time="513.4637904249757" />
         <tli id="T548" time="515.437" type="appl" />
         <tli id="T549" time="517.163" type="appl" />
         <tli id="T550" time="518.89" type="appl" />
         <tli id="T551" time="519.566" type="appl" />
         <tli id="T552" time="520.192" type="appl" />
         <tli id="T553" time="520.819" type="appl" />
         <tli id="T554" time="521.445" type="appl" />
         <tli id="T555" time="522.071" type="appl" />
         <tli id="T556" time="522.9504039510047" />
         <tli id="T557" time="523.946" type="appl" />
         <tli id="T558" time="524.96" type="appl" />
         <tli id="T559" time="525.973" type="appl" />
         <tli id="T560" time="526.537" type="appl" />
         <tli id="T561" time="527.025" type="appl" />
         <tli id="T562" time="527.514" type="appl" />
         <tli id="T563" time="528.002" type="appl" />
         <tli id="T564" time="528.491" type="appl" />
         <tli id="T565" time="528.979" type="appl" />
         <tli id="T566" time="530.2036966539697" />
         <tli id="T567" time="530.976" type="appl" />
         <tli id="T568" time="531.822" type="appl" />
         <tli id="T569" time="532.668" type="appl" />
         <tli id="T570" time="533.514" type="appl" />
         <tli id="T571" time="534.359" type="appl" />
         <tli id="T572" time="535.205" type="appl" />
         <tli id="T573" time="536.051" type="appl" />
         <tli id="T574" time="539.0303138770632" />
         <tli id="T575" time="539.788" type="appl" />
         <tli id="T576" time="540.386" type="appl" />
         <tli id="T577" time="540.984" type="appl" />
         <tli id="T578" time="541.582" type="appl" />
         <tli id="T579" time="542.18" type="appl" />
         <tli id="T580" time="543.227" type="appl" />
         <tli id="T581" time="544.093" type="appl" />
         <tli id="T582" time="544.96" type="appl" />
         <tli id="T583" time="546.102" type="appl" />
         <tli id="T584" time="547.17" type="appl" />
         <tli id="T585" time="548.175" type="appl" />
         <tli id="T586" time="549.18" type="appl" />
         <tli id="T587" time="550.022" type="appl" />
         <tli id="T588" time="550.785" type="appl" />
         <tli id="T589" time="551.547" type="appl" />
         <tli id="T590" time="553.2435675928" />
         <tli id="T591" time="553.95" type="appl" />
         <tli id="T592" time="554.74" type="appl" />
         <tli id="T593" time="555.53" type="appl" />
         <tli id="T594" time="559.5235324146687" />
         <tli id="T595" time="560.652" type="appl" />
         <tli id="T596" time="561.615" type="appl" />
         <tli id="T597" time="562.577" type="appl" />
         <tli id="T598" time="563.279" type="appl" />
         <tli id="T599" time="563.982" type="appl" />
         <tli id="T600" time="564.684" type="appl" />
         <tli id="T601" time="565.386" type="appl" />
         <tli id="T602" time="566.089" type="appl" />
         <tli id="T603" time="566.791" type="appl" />
         <tli id="T604" time="567.493" type="appl" />
         <tli id="T605" time="568.195" type="appl" />
         <tli id="T606" time="568.898" type="appl" />
         <tli id="T607" time="569.7768083127941" />
         <tli id="T608" time="570.615" type="appl" />
         <tli id="T609" time="571.45" type="appl" />
         <tli id="T610" time="572.285" type="appl" />
         <tli id="T611" time="573.12" type="appl" />
         <tli id="T612" time="574.145" type="appl" />
         <tli id="T613" time="574.945" type="appl" />
         <tli id="T867" time="575.7434415565016" />
         <tli id="T614" time="576.9634347225333" />
         <tli id="T615" time="577.41" type="appl" />
         <tli id="T616" time="578.14" type="appl" />
         <tli id="T617" time="578.87" type="appl" />
         <tli id="T618" time="579.6" type="appl" />
         <tli id="T619" time="580.3967488236611" />
         <tli id="T620" time="581.133" type="appl" />
         <tli id="T621" time="581.866" type="appl" />
         <tli id="T622" time="582.599" type="appl" />
         <tli id="T623" time="583.331" type="appl" />
         <tli id="T624" time="584.064" type="appl" />
         <tli id="T625" time="584.797" type="appl" />
         <tli id="T626" time="585.53" type="appl" />
         <tli id="T627" time="586.38" type="appl" />
         <tli id="T628" time="587.088" type="appl" />
         <tli id="T629" time="587.423" type="appl" />
         <tli id="T630" time="587.758" type="appl" />
         <tli id="T631" time="588.093" type="appl" />
         <tli id="T632" time="588.810035028755" />
         <tli id="T633" time="589.636" type="appl" />
         <tli id="T634" time="590.308" type="appl" />
         <tli id="T635" time="590.98" type="appl" />
         <tli id="T866" time="591.2043333333334" type="intp" />
         <tli id="T636" time="591.653" type="appl" />
         <tli id="T637" time="592.325" type="appl" />
         <tli id="T638" time="592.998" type="appl" />
         <tli id="T639" time="593.67" type="appl" />
         <tli id="T640" time="594.343" type="appl" />
         <tli id="T641" time="595.016" type="appl" />
         <tli id="T642" time="595.9366617745911" />
         <tli id="T643" time="596.842" type="appl" />
         <tli id="T644" time="597.532" type="appl" />
         <tli id="T645" time="598.221" type="appl" />
         <tli id="T646" time="598.91" type="appl" />
         <tli id="T647" time="600.549" type="appl" />
         <tli id="T648" time="602.3432925535888" />
         <tli id="T649" time="603.301" type="appl" />
         <tli id="T650" time="604.255" type="appl" />
         <tli id="T651" time="607.4965970199418" />
         <tli id="T652" time="608.24" type="appl" />
         <tli id="T653" time="608.93" type="appl" />
         <tli id="T654" time="609.62" type="appl" />
         <tli id="T655" time="611.001" type="appl" />
         <tli id="T656" time="612.362" type="appl" />
         <tli id="T657" time="613.723" type="appl" />
         <tli id="T658" time="614.612" type="appl" />
         <tli id="T659" time="615.433" type="appl" />
         <tli id="T660" time="616.255" type="appl" />
         <tli id="T661" time="617.077" type="appl" />
         <tli id="T662" time="617.898" type="appl" />
         <tli id="T663" time="618.72" type="appl" />
         <tli id="T664" time="619.998" type="appl" />
         <tli id="T665" time="621.207" type="appl" />
         <tli id="T666" time="622.415" type="appl" />
         <tli id="T667" time="623.623" type="appl" />
         <tli id="T668" time="624.832" type="appl" />
         <tli id="T669" time="626.04" type="appl" />
         <tli id="T670" time="626.73" type="appl" />
         <tli id="T671" time="627.34" type="appl" />
         <tli id="T672" time="627.95" type="appl" />
         <tli id="T673" time="628.807" type="appl" />
         <tli id="T674" time="629.453" type="appl" />
         <tli id="T675" time="630.1" type="appl" />
         <tli id="T676" time="630.747" type="appl" />
         <tli id="T677" time="631.393" type="appl" />
         <tli id="T678" time="632.04" type="appl" />
         <tli id="T679" time="632.76" type="appl" />
         <tli id="T680" time="633.45" type="appl" />
         <tli id="T681" time="634.14" type="appl" />
         <tli id="T682" time="634.83" type="appl" />
         <tli id="T683" time="635.52" type="appl" />
         <tli id="T684" time="636.3964351333184" />
         <tli id="T685" time="637.026" type="appl" />
         <tli id="T686" time="637.772" type="appl" />
         <tli id="T687" time="638.518" type="appl" />
         <tli id="T688" time="639.264" type="appl" />
         <tli id="T689" time="640.01" type="appl" />
         <tli id="T690" time="640.756" type="appl" />
         <tli id="T691" time="643.4563955859287" />
         <tli id="T692" time="644.052" type="appl" />
         <tli id="T693" time="644.663" type="appl" />
         <tli id="T694" time="645.275" type="appl" />
         <tli id="T695" time="645.887" type="appl" />
         <tli id="T696" time="646.499" type="appl" />
         <tli id="T697" time="647.11" type="appl" />
         <tli id="T698" time="649.5630280454104" />
         <tli id="T699" time="650.202" type="appl" />
         <tli id="T700" time="650.824" type="appl" />
         <tli id="T701" time="651.446" type="appl" />
         <tli id="T702" time="652.068" type="appl" />
         <tli id="T703" time="652.8030098961835" />
         <tli id="T704" time="653.44" type="appl" />
         <tli id="T705" time="654.1" type="appl" />
         <tli id="T706" time="654.76" type="appl" />
         <tli id="T707" time="657.1363189558593" />
         <tli id="T708" time="657.917" type="appl" />
         <tli id="T709" time="658.673" type="appl" />
         <tli id="T710" time="659.43" type="appl" />
         <tli id="T711" time="660.358" type="appl" />
         <tli id="T712" time="660.975" type="appl" />
         <tli id="T713" time="661.592" type="appl" />
         <tli id="T714" time="662.356289715438" />
         <tli id="T715" time="663.7" type="appl" />
         <tli id="T716" time="664.88" type="appl" />
         <tli id="T717" time="665.617" type="appl" />
         <tli id="T718" time="666.353" type="appl" />
         <tli id="T719" time="668.789587011726" />
         <tli id="T720" time="669.254" type="appl" />
         <tli id="T721" time="669.898" type="appl" />
         <tli id="T722" time="670.542" type="appl" />
         <tli id="T723" time="671.186" type="appl" />
         <tli id="T724" time="672.0829018970796" />
         <tli id="T725" time="672.657" type="appl" />
         <tli id="T726" time="673.273" type="appl" />
         <tli id="T727" time="676.156212413175" />
         <tli id="T728" time="676.88" type="appl" />
         <tli id="T729" time="677.63" type="appl" />
         <tli id="T730" time="678.38" type="appl" />
         <tli id="T731" time="680.6361873179476" />
         <tli id="T732" time="681.278" type="appl" />
         <tli id="T733" time="681.986" type="appl" />
         <tli id="T734" time="682.694" type="appl" />
         <tli id="T735" time="683.402" type="appl" />
         <tli id="T736" time="684.11" type="appl" />
         <tli id="T737" time="684.818" type="appl" />
         <tli id="T738" time="686.509487751139" />
         <tli id="T739" time="687.28" type="appl" />
         <tli id="T740" time="687.96" type="appl" />
         <tli id="T741" time="688.64" type="appl" />
         <tli id="T742" time="689.32" type="appl" />
         <tli id="T743" time="690.0" type="appl" />
         <tli id="T744" time="690.68" type="appl" />
         <tli id="T745" time="691.419" type="appl" />
         <tli id="T746" time="692.158" type="appl" />
         <tli id="T747" time="692.896" type="appl" />
         <tli id="T748" time="693.635" type="appl" />
         <tli id="T749" time="694.374" type="appl" />
         <tli id="T750" time="695.112" type="appl" />
         <tli id="T751" time="695.851" type="appl" />
         <tli id="T752" time="697.88942400478" />
         <tli id="T753" time="698.621" type="appl" />
         <tli id="T754" time="699.512" type="appl" />
         <tli id="T755" time="700.404" type="appl" />
         <tli id="T756" time="701.295" type="appl" />
         <tli id="T757" time="704.0760560161326" />
         <tli id="T758" time="704.99" type="appl" />
         <tli id="T759" time="705.86" type="appl" />
         <tli id="T760" time="706.73" type="appl" />
         <tli id="T761" time="708.8693624990664" />
         <tli id="T762" time="709.713" type="appl" />
         <tli id="T763" time="710.597" type="appl" />
         <tli id="T764" time="712.3826761520651" />
         <tli id="T765" time="712.964" type="appl" />
         <tli id="T766" time="713.459" type="appl" />
         <tli id="T767" time="713.953" type="appl" />
         <tli id="T768" time="714.447" type="appl" />
         <tli id="T769" time="714.941" type="appl" />
         <tli id="T770" time="715.436" type="appl" />
         <tli id="T771" time="718.722640637837" />
         <tli id="T772" time="719.87" type="appl" />
         <tli id="T773" time="721.3826257375458" />
         <tli id="T774" time="722.616" type="appl" />
         <tli id="T775" time="723.937" type="appl" />
         <tli id="T776" time="727.3559256105758" />
         <tli id="T777" time="728.479" type="appl" />
         <tli id="T778" time="729.258" type="appl" />
         <tli id="T779" time="730.037" type="appl" />
         <tli id="T780" time="730.816" type="appl" />
         <tli id="T781" time="731.594" type="appl" />
         <tli id="T782" time="732.373" type="appl" />
         <tli id="T783" time="733.152" type="appl" />
         <tli id="T784" time="733.931" type="appl" />
         <tli id="T785" time="734.71" type="appl" />
         <tli id="T786" time="735.59" type="appl" />
         <tli id="T787" time="736.24" type="appl" />
         <tli id="T788" time="736.89" type="appl" />
         <tli id="T789" time="737.62" type="appl" />
         <tli id="T790" time="738.35" type="appl" />
         <tli id="T791" time="739.08" type="appl" />
         <tli id="T792" time="740.442518970797" />
         <tli id="T793" time="741.65" type="appl" />
         <tli id="T794" time="743.0158378893121" />
         <tli id="T795" time="744.195" type="appl" />
         <tli id="T796" time="745.085" type="appl" />
         <tli id="T797" time="745.975" type="appl" />
         <tli id="T798" time="746.865" type="appl" />
         <tli id="T799" time="747.755" type="appl" />
         <tli id="T800" time="748.619" type="appl" />
         <tli id="T801" time="749.323" type="appl" />
         <tli id="T802" time="750.027" type="appl" />
         <tli id="T803" time="750.731" type="appl" />
         <tli id="T804" time="751.435" type="appl" />
         <tli id="T805" time="752.176" type="appl" />
         <tli id="T806" time="752.872" type="appl" />
         <tli id="T807" time="753.569" type="appl" />
         <tli id="T808" time="754.265" type="appl" />
         <tli id="T809" time="754.961" type="appl" />
         <tli id="T810" time="755.658" type="appl" />
         <tli id="T811" time="756.354" type="appl" />
         <tli id="T812" time="757.2233103874332" />
         <tli id="T813" time="757.785" type="appl" />
         <tli id="T814" time="758.52" type="appl" />
         <tli id="T815" time="759.255" type="appl" />
         <tli id="T816" time="759.99" type="appl" />
         <tli id="T817" time="760.725" type="appl" />
         <tli id="T818" time="761.46" type="appl" />
         <tli id="T819" time="762.195" type="appl" />
         <tli id="T820" time="762.93" type="appl" />
         <tli id="T821" time="763.665" type="appl" />
         <tli id="T822" time="766.2823742251102" />
         <tli id="T823" time="767.051" type="appl" />
         <tli id="T824" time="767.652" type="appl" />
         <tli id="T825" time="768.254" type="appl" />
         <tli id="T826" time="768.855" type="appl" />
         <tli id="T827" time="769.456" type="appl" />
         <tli id="T828" time="770.058" type="appl" />
         <tli id="T829" time="770.659" type="appl" />
         <tli id="T830" time="771.26" type="appl" />
         <tli id="T831" time="771.931" type="appl" />
         <tli id="T832" time="772.601" type="appl" />
         <tli id="T833" time="773.272" type="appl" />
         <tli id="T834" time="773.942" type="appl" />
         <tli id="T835" time="774.613" type="appl" />
         <tli id="T836" time="775.283" type="appl" />
         <tli id="T837" time="775.954" type="appl" />
         <tli id="T838" time="776.624" type="appl" />
         <tli id="T839" time="778.8156373515573" />
         <tli id="T840" time="779.701" type="appl" />
         <tli id="T841" time="780.431" type="appl" />
         <tli id="T842" time="781.162" type="appl" />
         <tli id="T843" time="781.893" type="appl" />
         <tli id="T844" time="782.624" type="appl" />
         <tli id="T845" time="783.354" type="appl" />
         <tli id="T846" time="786.2022626409739" />
         <tli id="T847" time="786.467" type="appl" />
         <tli id="T848" time="787.243" type="appl" />
         <tli id="T849" time="788.02" type="appl" />
         <tli id="T850" time="788.797" type="appl" />
         <tli id="T851" time="789.573" type="appl" />
         <tli id="T852" time="790.35" type="appl" />
         <tli id="T853" time="791.127" type="appl" />
         <tli id="T854" time="791.903" type="appl" />
         <tli id="T855" time="792.68" type="appl" />
         <tli id="T856" time="793.457" type="appl" />
         <tli id="T857" time="794.233" type="appl" />
         <tli id="T858" time="795.01" type="appl" />
         <tli id="T859" time="796.833" type="appl" />
         <tli id="T860" time="797.857" type="appl" />
         <tli id="T861" time="798.88" type="appl" />
         <tli id="T862" time="800.151" type="appl" />
         <tli id="T863" time="801.213" type="appl" />
         <tli id="T864" time="802.274" type="appl" />
         <tli id="T865" time="803.335" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T67" start="T66">
            <tli id="T66.tx.1" />
         </timeline-fork>
         <timeline-fork end="T72" start="T71">
            <tli id="T71.tx.1" />
         </timeline-fork>
         <timeline-fork end="T98" start="T97">
            <tli id="T97.tx.1" />
         </timeline-fork>
         <timeline-fork end="T101" start="T100">
            <tli id="T100.tx.1" />
         </timeline-fork>
         <timeline-fork end="T362" start="T361">
            <tli id="T361.tx.1" />
         </timeline-fork>
         <timeline-fork end="T383" start="T381">
            <tli id="T381.tx.1" />
         </timeline-fork>
         <timeline-fork end="T387" start="T385">
            <tli id="T385.tx.1" />
         </timeline-fork>
         <timeline-fork end="T527" start="T526">
            <tli id="T526.tx.1" />
         </timeline-fork>
         <timeline-fork end="T672" start="T669">
            <tli id="T669.tx.1" />
            <tli id="T669.tx.2" />
         </timeline-fork>
         <timeline-fork end="T725" start="T724">
            <tli id="T724.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T865" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Miʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">bar</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_9" n="HIAT:ip">(</nts>
                  <ts e="T3" id="Seg_11" n="HIAT:w" s="T2">Sverdlovskagən</ts>
                  <nts id="Seg_12" n="HIAT:ip">)</nts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_15" n="HIAT:w" s="T3">amnobiaʔ</ts>
                  <nts id="Seg_16" n="HIAT:ip">.</nts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_19" n="HIAT:u" s="T4">
                  <nts id="Seg_20" n="HIAT:ip">(</nts>
                  <ts e="T5" id="Seg_22" n="HIAT:w" s="T4">Sujo=</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_25" n="HIAT:w" s="T5">sujo=</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">bazaj=</ts>
                  <nts id="Seg_29" n="HIAT:ip">)</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_32" n="HIAT:w" s="T7">Bazaj</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_35" n="HIAT:w" s="T8">süjönə</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">amnobibaʔ</ts>
                  <nts id="Seg_39" n="HIAT:ip">.</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_42" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_44" n="HIAT:w" s="T10">Dĭgəttə</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_47" n="HIAT:w" s="T11">nʼergöbibaʔ</ts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_51" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_53" n="HIAT:w" s="T12">Aktʼigən</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_56" n="HIAT:w" s="T13">ej</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_59" n="HIAT:w" s="T14">molia</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_61" n="HIAT:ip">(</nts>
                  <ts e="T16" id="Seg_63" n="HIAT:w" s="T15">nʼ-</ts>
                  <nts id="Seg_64" n="HIAT:ip">)</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_67" n="HIAT:w" s="T16">nʼergözittə</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_71" n="HIAT:u" s="T17">
                  <nts id="Seg_72" n="HIAT:ip">(</nts>
                  <ts e="T18" id="Seg_74" n="HIAT:w" s="T17">Berzəbi</ts>
                  <nts id="Seg_75" n="HIAT:ip">)</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_78" n="HIAT:w" s="T18">bar</ts>
                  <nts id="Seg_79" n="HIAT:ip">.</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_82" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_84" n="HIAT:w" s="T19">Туман</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_86" n="HIAT:ip">(</nts>
                  <ts e="T21" id="Seg_88" n="HIAT:w" s="T20">nuʔ-</ts>
                  <nts id="Seg_89" n="HIAT:ip">)</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_92" n="HIAT:w" s="T21">nulaʔbə</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_96" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_98" n="HIAT:w" s="T22">Dĭgəttə</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_101" n="HIAT:w" s="T23">šobibaʔ</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_104" n="HIAT:w" s="T24">Krasnojarskənə</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_108" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_110" n="HIAT:w" s="T25">Dĭn</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_113" n="HIAT:w" s="T26">amnəbibaʔ</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_117" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_119" n="HIAT:w" s="T27">Dĭn</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_122" n="HIAT:w" s="T28">amnobiam</ts>
                  <nts id="Seg_123" n="HIAT:ip">.</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_126" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_128" n="HIAT:w" s="T29">Sejʔpü</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_131" n="HIAT:w" s="T30">dʼala</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_134" n="HIAT:w" s="T31">amnobiam</ts>
                  <nts id="Seg_135" n="HIAT:ip">.</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_138" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_140" n="HIAT:w" s="T32">Dĭgəttə</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_143" n="HIAT:w" s="T33">amnobiam</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_146" n="HIAT:w" s="T34">bazaj</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_148" n="HIAT:ip">(</nts>
                  <ts e="T36" id="Seg_150" n="HIAT:w" s="T35">aʔtʼinə</ts>
                  <nts id="Seg_151" n="HIAT:ip">)</nts>
                  <nts id="Seg_152" n="HIAT:ip">.</nts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_155" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_157" n="HIAT:w" s="T36">Šobiam</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_160" n="HIAT:w" s="T37">Ujardə</ts>
                  <nts id="Seg_161" n="HIAT:ip">.</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_164" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_166" n="HIAT:w" s="T38">Dĭgəttə</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_169" n="HIAT:w" s="T39">Ujargəʔ</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_172" n="HIAT:w" s="T40">šobiam</ts>
                  <nts id="Seg_173" n="HIAT:ip">.</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_176" n="HIAT:u" s="T41">
                  <nts id="Seg_177" n="HIAT:ip">(</nts>
                  <ts e="T42" id="Seg_179" n="HIAT:w" s="T41">Amnobi-</ts>
                  <nts id="Seg_180" n="HIAT:ip">)</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_183" n="HIAT:w" s="T42">Amnobiam</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_186" n="HIAT:w" s="T43">avtobustə</ts>
                  <nts id="Seg_187" n="HIAT:ip">.</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_190" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_192" n="HIAT:w" s="T44">Šobiam</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_195" n="HIAT:w" s="T45">Săjanskən</ts>
                  <nts id="Seg_196" n="HIAT:ip">.</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_199" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_201" n="HIAT:w" s="T46">Dön</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_204" n="HIAT:w" s="T47">amnobiam</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_206" n="HIAT:ip">(</nts>
                  <ts e="T49" id="Seg_208" n="HIAT:w" s="T48">четыре</ts>
                  <nts id="Seg_209" n="HIAT:ip">)</nts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_211" n="HIAT:ip">(</nts>
                  <ts e="T50" id="Seg_213" n="HIAT:w" s="T49">nagu-</ts>
                  <nts id="Seg_214" n="HIAT:ip">)</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_217" n="HIAT:w" s="T50">šide</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_220" n="HIAT:w" s="T51">dʼala</ts>
                  <nts id="Seg_221" n="HIAT:ip">.</nts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_224" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_226" n="HIAT:w" s="T52">Dĭgəttə</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_229" n="HIAT:w" s="T53">Abalakoftə</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_232" n="HIAT:w" s="T54">šobiam</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_235" n="HIAT:w" s="T55">i</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_238" n="HIAT:w" s="T56">tănan</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_241" n="HIAT:w" s="T57">šobiam</ts>
                  <nts id="Seg_242" n="HIAT:ip">.</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_245" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_247" n="HIAT:w" s="T58">Tănan</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_250" n="HIAT:w" s="T59">šobiam</ts>
                  <nts id="Seg_251" n="HIAT:ip">,</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_254" n="HIAT:w" s="T60">dʼăbaktərbibaʔ</ts>
                  <nts id="Seg_255" n="HIAT:ip">,</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_258" n="HIAT:w" s="T61">dĭgəttə</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_261" n="HIAT:w" s="T62">tăn</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_263" n="HIAT:ip">(</nts>
                  <ts e="T64" id="Seg_265" n="HIAT:w" s="T63">măn=</ts>
                  <nts id="Seg_266" n="HIAT:ip">)</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_269" n="HIAT:w" s="T64">măna</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_271" n="HIAT:ip">(</nts>
                  <ts e="T66" id="Seg_273" n="HIAT:w" s="T65">măm-</ts>
                  <nts id="Seg_274" n="HIAT:ip">)</nts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_276" n="HIAT:ip">(</nts>
                  <ts e="T66.tx.1" id="Seg_278" n="HIAT:w" s="T66">măllal</ts>
                  <nts id="Seg_279" n="HIAT:ip">)</nts>
                  <ts e="T67" id="Seg_281" n="HIAT:w" s="T66.tx.1">:</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_284" n="HIAT:w" s="T67">Amnaʔ</ts>
                  <nts id="Seg_285" n="HIAT:ip">,</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_288" n="HIAT:w" s="T68">amorgaʔ</ts>
                  <nts id="Seg_289" n="HIAT:ip">,</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_291" n="HIAT:ip">(</nts>
                  <ts e="T70" id="Seg_293" n="HIAT:w" s="T69">tu-</ts>
                  <nts id="Seg_294" n="HIAT:ip">)</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_297" n="HIAT:w" s="T70">püjölial</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71.tx.1" id="Seg_300" n="HIAT:w" s="T71">padʼi</ts>
                  <nts id="Seg_301" n="HIAT:ip">_</nts>
                  <ts e="T72" id="Seg_303" n="HIAT:w" s="T71.tx.1">ka</ts>
                  <nts id="Seg_304" n="HIAT:ip">.</nts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_307" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_309" n="HIAT:w" s="T72">Dĭgəttə</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_312" n="HIAT:w" s="T73">măn</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_315" n="HIAT:w" s="T74">amnobiam</ts>
                  <nts id="Seg_316" n="HIAT:ip">,</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_319" n="HIAT:w" s="T75">amorbiam</ts>
                  <nts id="Seg_320" n="HIAT:ip">,</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_323" n="HIAT:w" s="T76">kambiam</ts>
                  <nts id="Seg_324" n="HIAT:ip">.</nts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_327" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_329" n="HIAT:w" s="T77">Bostə</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_331" n="HIAT:ip">(</nts>
                  <ts e="T79" id="Seg_333" n="HIAT:w" s="T78">fatʼeratsi</ts>
                  <nts id="Seg_334" n="HIAT:ip">)</nts>
                  <nts id="Seg_335" n="HIAT:ip">.</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_338" n="HIAT:u" s="T79">
                  <nts id="Seg_339" n="HIAT:ip">(</nts>
                  <ts e="T80" id="Seg_341" n="HIAT:w" s="T79">Onʼiʔ=</ts>
                  <nts id="Seg_342" n="HIAT:ip">)</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_345" n="HIAT:w" s="T80">Onʼiʔ</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_348" n="HIAT:w" s="T81">raz</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_351" n="HIAT:w" s="T82">miʔ</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_354" n="HIAT:w" s="T83">kambibaʔ</ts>
                  <nts id="Seg_355" n="HIAT:ip">,</nts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_358" n="HIAT:w" s="T84">nagur</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_361" n="HIAT:w" s="T85">ne</ts>
                  <nts id="Seg_362" n="HIAT:ip">,</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_365" n="HIAT:w" s="T86">Zaozʼorkanə</ts>
                  <nts id="Seg_366" n="HIAT:ip">.</nts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_369" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_371" n="HIAT:w" s="T87">Dĭgəttə</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_374" n="HIAT:w" s="T88">dĭʔə</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_377" n="HIAT:w" s="T89">bazaj</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_380" n="HIAT:w" s="T90">aʔtʼinə</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_382" n="HIAT:ip">(</nts>
                  <ts e="T92" id="Seg_384" n="HIAT:w" s="T91">am-</ts>
                  <nts id="Seg_385" n="HIAT:ip">)</nts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_388" n="HIAT:w" s="T92">amnobibaʔ</ts>
                  <nts id="Seg_389" n="HIAT:ip">.</nts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_392" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_394" n="HIAT:w" s="T93">Kambibaʔ</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_397" n="HIAT:w" s="T94">Ujardə</ts>
                  <nts id="Seg_398" n="HIAT:ip">,</nts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_401" n="HIAT:w" s="T95">dĭn</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_404" n="HIAT:w" s="T96">kudajdə</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97.tx.1" id="Seg_407" n="HIAT:w" s="T97">numan</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_410" n="HIAT:w" s="T97.tx.1">üzəsʼtə</ts>
                  <nts id="Seg_411" n="HIAT:ip">.</nts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_414" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_416" n="HIAT:w" s="T98">Dĭn</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_419" n="HIAT:w" s="T99">kudajdə</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100.tx.1" id="Seg_422" n="HIAT:w" s="T100">numan</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_425" n="HIAT:w" s="T100.tx.1">üzəleʔpibeʔ</ts>
                  <nts id="Seg_426" n="HIAT:ip">.</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_429" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_431" n="HIAT:w" s="T101">Dĭgəttə</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_434" n="HIAT:w" s="T102">parluʔpibaʔ</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_436" n="HIAT:ip">(</nts>
                  <ts e="T104" id="Seg_438" n="HIAT:w" s="T103">bazoʔ</ts>
                  <nts id="Seg_439" n="HIAT:ip">)</nts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_441" n="HIAT:ip">(</nts>
                  <ts e="T105" id="Seg_443" n="HIAT:w" s="T104">s-</ts>
                  <nts id="Seg_444" n="HIAT:ip">)</nts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_447" n="HIAT:w" s="T105">döbər</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_450" n="HIAT:w" s="T106">Zaozʼorkanə</ts>
                  <nts id="Seg_451" n="HIAT:ip">.</nts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_454" n="HIAT:u" s="T107">
                  <nts id="Seg_455" n="HIAT:ip">(</nts>
                  <ts e="T108" id="Seg_457" n="HIAT:w" s="T107">Dĭn</ts>
                  <nts id="Seg_458" n="HIAT:ip">)</nts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_461" n="HIAT:w" s="T108">Dön</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_464" n="HIAT:w" s="T109">šabiaʔ</ts>
                  <nts id="Seg_465" n="HIAT:ip">,</nts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_468" n="HIAT:w" s="T110">iʔgö</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_471" n="HIAT:w" s="T111">dʼăbaktərlaʔpibeʔ</ts>
                  <nts id="Seg_472" n="HIAT:ip">.</nts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_475" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_477" n="HIAT:w" s="T112">Dĭgəttə</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_479" n="HIAT:ip">(</nts>
                  <ts e="T114" id="Seg_481" n="HIAT:w" s="T113">kozʼa</ts>
                  <nts id="Seg_482" n="HIAT:ip">)</nts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_485" n="HIAT:w" s="T114">kambi</ts>
                  <nts id="Seg_486" n="HIAT:ip">.</nts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T121" id="Seg_489" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_491" n="HIAT:w" s="T115">Avtobus</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_494" n="HIAT:w" s="T116">măndərzittə</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_497" n="HIAT:w" s="T117">šobi</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_500" n="HIAT:w" s="T118">da</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_502" n="HIAT:ip">(</nts>
                  <ts e="T120" id="Seg_504" n="HIAT:w" s="T119">măndede</ts>
                  <nts id="Seg_505" n="HIAT:ip">)</nts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_508" n="HIAT:w" s="T120">kalla</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_511" n="HIAT:w" s="T868">dʼürbi</ts>
                  <nts id="Seg_512" n="HIAT:ip">.</nts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_515" n="HIAT:u" s="T121">
                  <ts e="T122" id="Seg_517" n="HIAT:w" s="T121">A</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_520" n="HIAT:w" s="T122">miʔ</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_523" n="HIAT:w" s="T123">oʔbdəbibaʔ</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_526" n="HIAT:w" s="T124">da</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_529" n="HIAT:w" s="T125">kambibaʔ</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_532" n="HIAT:w" s="T126">nʼiʔdə</ts>
                  <nts id="Seg_533" n="HIAT:ip">.</nts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_536" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_538" n="HIAT:w" s="T127">Dön</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_541" n="HIAT:w" s="T128">avtobus</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_544" n="HIAT:w" s="T129">šonəga</ts>
                  <nts id="Seg_545" n="HIAT:ip">.</nts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_548" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_550" n="HIAT:w" s="T130">Amnəldəbi</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_553" n="HIAT:w" s="T131">miʔ</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_555" n="HIAT:ip">(</nts>
                  <ts e="T133" id="Seg_557" n="HIAT:w" s="T132">da</ts>
                  <nts id="Seg_558" n="HIAT:ip">)</nts>
                  <nts id="Seg_559" n="HIAT:ip">.</nts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_562" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_564" n="HIAT:w" s="T133">Perejaslăvkanə</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_567" n="HIAT:w" s="T134">šobibaʔ</ts>
                  <nts id="Seg_568" n="HIAT:ip">.</nts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_571" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_573" n="HIAT:w" s="T135">Dĭn</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_575" n="HIAT:ip">(</nts>
                  <ts e="T137" id="Seg_577" n="HIAT:w" s="T136">нас=</ts>
                  <nts id="Seg_578" n="HIAT:ip">)</nts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_581" n="HIAT:w" s="T137">miʔnʼibeʔ</ts>
                  <nts id="Seg_582" n="HIAT:ip">…</nts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_585" n="HIAT:u" s="T139">
                  <nts id="Seg_586" n="HIAT:ip">(</nts>
                  <nts id="Seg_587" n="HIAT:ip">(</nts>
                  <ats e="T141" id="Seg_588" n="HIAT:non-pho" s="T139">Dĭn miʔ</ats>
                  <nts id="Seg_589" n="HIAT:ip">)</nts>
                  <nts id="Seg_590" n="HIAT:ip">)</nts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_593" n="HIAT:w" s="T141">Dĭn</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_596" n="HIAT:w" s="T142">üzəbibeʔ</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_599" n="HIAT:w" s="T143">i</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_602" n="HIAT:w" s="T144">nulabibaʔ</ts>
                  <nts id="Seg_603" n="HIAT:ip">.</nts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_606" n="HIAT:u" s="T145">
                  <ts e="T146" id="Seg_608" n="HIAT:w" s="T145">Măn</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_611" n="HIAT:w" s="T146">măndəm:</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_614" n="HIAT:w" s="T147">Ĭmbi</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_617" n="HIAT:w" s="T148">nulaʔsittə</ts>
                  <nts id="Seg_618" n="HIAT:ip">,</nts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_621" n="HIAT:w" s="T149">kanžəbaʔ</ts>
                  <nts id="Seg_622" n="HIAT:ip">,</nts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_625" n="HIAT:w" s="T150">üdʼiʔ</ts>
                  <nts id="Seg_626" n="HIAT:ip">!</nts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_629" n="HIAT:u" s="T151">
                  <nts id="Seg_630" n="HIAT:ip">(</nts>
                  <ts e="T152" id="Seg_632" n="HIAT:w" s="T151">Dĭgəttə</ts>
                  <nts id="Seg_633" n="HIAT:ip">)</nts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_635" n="HIAT:ip">(</nts>
                  <ts e="T153" id="Seg_637" n="HIAT:w" s="T152">kanžəbaʔ=</ts>
                  <nts id="Seg_638" n="HIAT:ip">)</nts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_641" n="HIAT:w" s="T153">kandlaʔbəbaʔ</ts>
                  <nts id="Seg_642" n="HIAT:ip">.</nts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_645" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_647" n="HIAT:w" s="T154">Mašina</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_650" n="HIAT:w" s="T155">bar</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_653" n="HIAT:w" s="T156">šonəga</ts>
                  <nts id="Seg_654" n="HIAT:ip">.</nts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_657" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_659" n="HIAT:w" s="T157">Amnogaʔ</ts>
                  <nts id="Seg_660" n="HIAT:ip">!</nts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_663" n="HIAT:u" s="T158">
                  <nts id="Seg_664" n="HIAT:ip">(</nts>
                  <ts e="T159" id="Seg_666" n="HIAT:w" s="T158">Miʔ=</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_669" n="HIAT:w" s="T159">amne-</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_672" n="HIAT:w" s="T160">amno-</ts>
                  <nts id="Seg_673" n="HIAT:ip">)</nts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_676" n="HIAT:w" s="T161">Amnogaʔ</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_679" n="HIAT:w" s="T162">döber</ts>
                  <nts id="Seg_680" n="HIAT:ip">!</nts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T166" id="Seg_683" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_685" n="HIAT:w" s="T163">Dĭgəttə</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_688" n="HIAT:w" s="T164">miʔ</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_691" n="HIAT:w" s="T165">amnobibaʔ</ts>
                  <nts id="Seg_692" n="HIAT:ip">.</nts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T169" id="Seg_695" n="HIAT:u" s="T166">
                  <ts e="T167" id="Seg_697" n="HIAT:w" s="T166">I</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_700" n="HIAT:w" s="T167">šobibaʔ</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_703" n="HIAT:w" s="T168">Kandəganə</ts>
                  <nts id="Seg_704" n="HIAT:ip">.</nts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_707" n="HIAT:u" s="T169">
                  <ts e="T170" id="Seg_709" n="HIAT:w" s="T169">Dĭgəttə</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_712" n="HIAT:w" s="T170">dĭn</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_715" n="HIAT:w" s="T171">iššo</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_718" n="HIAT:w" s="T172">sejʔpü</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_721" n="HIAT:w" s="T173">верста</ts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_724" n="HIAT:w" s="T174">maluʔpi</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_727" n="HIAT:w" s="T175">Unʼerdə</ts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_730" n="HIAT:w" s="T176">kanzittə</ts>
                  <nts id="Seg_731" n="HIAT:ip">.</nts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T185" id="Seg_734" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_736" n="HIAT:w" s="T177">Šide</ts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_739" n="HIAT:w" s="T178">nezeŋ</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_742" n="HIAT:w" s="T179">kambiʔi</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_745" n="HIAT:w" s="T180">turanə</ts>
                  <nts id="Seg_746" n="HIAT:ip">,</nts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_749" n="HIAT:w" s="T181">a</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_752" n="HIAT:w" s="T182">măn</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_755" n="HIAT:w" s="T183">kambiam</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_758" n="HIAT:w" s="T184">üdʼiʔ</ts>
                  <nts id="Seg_759" n="HIAT:ip">.</nts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T187" id="Seg_762" n="HIAT:u" s="T185">
                  <ts e="T186" id="Seg_764" n="HIAT:w" s="T185">Unʼerdə</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_767" n="HIAT:w" s="T186">šobiam</ts>
                  <nts id="Seg_768" n="HIAT:ip">.</nts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_771" n="HIAT:u" s="T187">
                  <ts e="T188" id="Seg_773" n="HIAT:w" s="T187">Deʔkeʔ</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_776" n="HIAT:w" s="T188">măna</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_779" n="HIAT:w" s="T189">ipek</ts>
                  <nts id="Seg_780" n="HIAT:ip">.</nts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T192" id="Seg_783" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_785" n="HIAT:w" s="T190">Sadargaʔ</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_788" n="HIAT:w" s="T191">ipek</ts>
                  <nts id="Seg_789" n="HIAT:ip">.</nts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_792" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_794" n="HIAT:w" s="T192">Dĭzeŋ</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_797" n="HIAT:w" s="T193">măndə:</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_799" n="HIAT:ip">(</nts>
                  <ts e="T195" id="Seg_801" n="HIAT:w" s="T194">Ka-</ts>
                  <nts id="Seg_802" n="HIAT:ip">)</nts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_805" n="HIAT:w" s="T195">Kanaʔ</ts>
                  <nts id="Seg_806" n="HIAT:ip">.</nts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T200" id="Seg_809" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_811" n="HIAT:w" s="T196">Măgăzində</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_813" n="HIAT:ip">(</nts>
                  <ts e="T198" id="Seg_815" n="HIAT:w" s="T197">de-</ts>
                  <nts id="Seg_816" n="HIAT:ip">)</nts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_819" n="HIAT:w" s="T198">dĭn</ts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_822" n="HIAT:w" s="T199">iʔ</ts>
                  <nts id="Seg_823" n="HIAT:ip">.</nts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T204" id="Seg_826" n="HIAT:u" s="T200">
                  <ts e="T201" id="Seg_828" n="HIAT:w" s="T200">Măna</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_831" n="HIAT:w" s="T201">iʔgö</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_834" n="HIAT:w" s="T202">ej</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_837" n="HIAT:w" s="T203">kereʔ</ts>
                  <nts id="Seg_838" n="HIAT:ip">.</nts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_841" n="HIAT:u" s="T204">
                  <ts e="T205" id="Seg_843" n="HIAT:w" s="T204">Šide</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_846" n="HIAT:w" s="T205">gram</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_849" n="HIAT:w" s="T206">mĭbileʔ</ts>
                  <nts id="Seg_850" n="HIAT:ip">.</nts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T221" id="Seg_853" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_855" n="HIAT:w" s="T207">Dĭ</ts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_858" n="HIAT:w" s="T208">măna</ts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_861" n="HIAT:w" s="T209">šide</ts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_864" n="HIAT:w" s="T210">gram</ts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_867" n="HIAT:w" s="T211">edəbi</ts>
                  <nts id="Seg_868" n="HIAT:ip">,</nts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_871" n="HIAT:w" s="T212">măn</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_874" n="HIAT:w" s="T213">ibiem</ts>
                  <nts id="Seg_875" n="HIAT:ip">,</nts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_878" n="HIAT:w" s="T214">aktʼa</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_881" n="HIAT:w" s="T215">mĭbiem</ts>
                  <nts id="Seg_882" n="HIAT:ip">,</nts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_885" n="HIAT:w" s="T216">ambiam</ts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_888" n="HIAT:w" s="T217">i</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_891" n="HIAT:w" s="T218">kambiam</ts>
                  <nts id="Seg_892" n="HIAT:ip">,</nts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_895" n="HIAT:w" s="T219">bü</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_898" n="HIAT:w" s="T220">bĭʔpiem</ts>
                  <nts id="Seg_899" n="HIAT:ip">.</nts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T227" id="Seg_902" n="HIAT:u" s="T221">
                  <ts e="T222" id="Seg_904" n="HIAT:w" s="T221">Dĭgəttə</ts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_907" n="HIAT:w" s="T222">mašina</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_910" n="HIAT:w" s="T223">bĭdəbi</ts>
                  <nts id="Seg_911" n="HIAT:ip">,</nts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_914" n="HIAT:w" s="T224">măn</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_916" n="HIAT:ip">(</nts>
                  <ts e="T226" id="Seg_918" n="HIAT:w" s="T225">amno-</ts>
                  <nts id="Seg_919" n="HIAT:ip">)</nts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_922" n="HIAT:w" s="T226">amnobiam</ts>
                  <nts id="Seg_923" n="HIAT:ip">.</nts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_926" n="HIAT:u" s="T227">
                  <ts e="T228" id="Seg_928" n="HIAT:w" s="T227">Dĭgəttə</ts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_931" n="HIAT:w" s="T228">üzəbiem</ts>
                  <nts id="Seg_932" n="HIAT:ip">.</nts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T233" id="Seg_935" n="HIAT:u" s="T229">
                  <ts e="T230" id="Seg_937" n="HIAT:w" s="T229">Šide</ts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_940" n="HIAT:w" s="T230">kuza</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_943" n="HIAT:w" s="T231">nugaʔi</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_946" n="HIAT:w" s="T232">măjagən</ts>
                  <nts id="Seg_947" n="HIAT:ip">.</nts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T237" id="Seg_950" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_952" n="HIAT:w" s="T233">Măn</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_955" n="HIAT:w" s="T234">dĭbər</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_957" n="HIAT:ip">(</nts>
                  <ts e="T236" id="Seg_959" n="HIAT:w" s="T235">sa-</ts>
                  <nts id="Seg_960" n="HIAT:ip">)</nts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_963" n="HIAT:w" s="T236">sʼabiam</ts>
                  <nts id="Seg_964" n="HIAT:ip">.</nts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T239" id="Seg_967" n="HIAT:u" s="T237">
                  <ts e="T238" id="Seg_969" n="HIAT:w" s="T237">Kanžəbəj</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_971" n="HIAT:ip">(</nts>
                  <ts e="T239" id="Seg_973" n="HIAT:w" s="T238">obbere</ts>
                  <nts id="Seg_974" n="HIAT:ip">)</nts>
                  <nts id="Seg_975" n="HIAT:ip">.</nts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T245" id="Seg_978" n="HIAT:u" s="T239">
                  <nts id="Seg_979" n="HIAT:ip">(</nts>
                  <ts e="T240" id="Seg_981" n="HIAT:w" s="T239">Dʼok</ts>
                  <nts id="Seg_982" n="HIAT:ip">)</nts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_985" n="HIAT:w" s="T240">miʔ</ts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_988" n="HIAT:w" s="T241">edəʔleʔbə</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_991" n="HIAT:w" s="T242">mašina</ts>
                  <nts id="Seg_992" n="HIAT:ip">,</nts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_995" n="HIAT:w" s="T243">mašinaziʔ</ts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_998" n="HIAT:w" s="T244">kanžəbəj</ts>
                  <nts id="Seg_999" n="HIAT:ip">.</nts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_1002" n="HIAT:u" s="T245">
                  <ts e="T246" id="Seg_1004" n="HIAT:w" s="T245">Dĭgəttə</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_1007" n="HIAT:w" s="T246">mašina</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_1010" n="HIAT:w" s="T247">šobi</ts>
                  <nts id="Seg_1011" n="HIAT:ip">,</nts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1013" n="HIAT:ip">(</nts>
                  <ts e="T249" id="Seg_1015" n="HIAT:w" s="T248">amnobiaʔ-</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_1018" n="HIAT:w" s="T249">amnobiaʔ-</ts>
                  <nts id="Seg_1019" n="HIAT:ip">)</nts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_1022" n="HIAT:w" s="T250">amnobiam</ts>
                  <nts id="Seg_1023" n="HIAT:ip">.</nts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T254" id="Seg_1026" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_1028" n="HIAT:w" s="T251">I</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_1031" n="HIAT:w" s="T252">Săjanskəjnə</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_1034" n="HIAT:w" s="T253">šobiam</ts>
                  <nts id="Seg_1035" n="HIAT:ip">.</nts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_1038" n="HIAT:u" s="T254">
                  <ts e="T255" id="Seg_1040" n="HIAT:w" s="T254">Dĭgəttə</ts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1042" n="HIAT:ip">(</nts>
                  <ts e="T256" id="Seg_1044" n="HIAT:w" s="T255">nĭn</ts>
                  <nts id="Seg_1045" n="HIAT:ip">)</nts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1047" n="HIAT:ip">(</nts>
                  <ts e="T257" id="Seg_1049" n="HIAT:w" s="T256">ibem-</ts>
                  <nts id="Seg_1050" n="HIAT:ip">)</nts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_1053" n="HIAT:w" s="T257">ibiem</ts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_1056" n="HIAT:w" s="T258">ipek</ts>
                  <nts id="Seg_1057" n="HIAT:ip">,</nts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_1060" n="HIAT:w" s="T259">sĭreʔpne</ts>
                  <nts id="Seg_1061" n="HIAT:ip">.</nts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T265" id="Seg_1064" n="HIAT:u" s="T260">
                  <ts e="T261" id="Seg_1066" n="HIAT:w" s="T260">Vanʼka</ts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1069" n="HIAT:w" s="T261">bălʼnʼitsagən</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1072" n="HIAT:w" s="T262">iʔbolaʔpi</ts>
                  <nts id="Seg_1073" n="HIAT:ip">,</nts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1076" n="HIAT:w" s="T263">kumbiam</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1079" n="HIAT:w" s="T264">dĭʔnə</ts>
                  <nts id="Seg_1080" n="HIAT:ip">.</nts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T270" id="Seg_1083" n="HIAT:u" s="T265">
                  <ts e="T266" id="Seg_1085" n="HIAT:w" s="T265">Măna</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1088" n="HIAT:w" s="T266">dĭn</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1091" n="HIAT:w" s="T267">nörbəlaʔbəʔjə:</ts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1094" n="HIAT:w" s="T268">Dön</ts>
                  <nts id="Seg_1095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1097" n="HIAT:w" s="T269">igeʔ</ts>
                  <nts id="Seg_1098" n="HIAT:ip">.</nts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T273" id="Seg_1101" n="HIAT:u" s="T270">
                  <ts e="T271" id="Seg_1103" n="HIAT:w" s="T270">Miʔ</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1106" n="HIAT:w" s="T271">mašina</ts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1109" n="HIAT:w" s="T272">kanaʔ</ts>
                  <nts id="Seg_1110" n="HIAT:ip">.</nts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T275" id="Seg_1113" n="HIAT:u" s="T273">
                  <ts e="T274" id="Seg_1115" n="HIAT:w" s="T273">Măn</ts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1118" n="HIAT:w" s="T274">nuʔməbiem</ts>
                  <nts id="Seg_1119" n="HIAT:ip">.</nts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T276" id="Seg_1122" n="HIAT:u" s="T275">
                  <ts e="T276" id="Seg_1124" n="HIAT:w" s="T275">Gijendə</ts>
                  <nts id="Seg_1125" n="HIAT:ip">.</nts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T278" id="Seg_1128" n="HIAT:u" s="T276">
                  <ts e="T277" id="Seg_1130" n="HIAT:w" s="T276">Ej</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1133" n="HIAT:w" s="T277">kubiam</ts>
                  <nts id="Seg_1134" n="HIAT:ip">.</nts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T281" id="Seg_1137" n="HIAT:u" s="T278">
                  <ts e="T279" id="Seg_1139" n="HIAT:w" s="T278">Dĭgəttə</ts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1142" n="HIAT:w" s="T279">kambiam</ts>
                  <nts id="Seg_1143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1145" n="HIAT:w" s="T280">üdʼiʔ</ts>
                  <nts id="Seg_1146" n="HIAT:ip">.</nts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T285" id="Seg_1149" n="HIAT:u" s="T281">
                  <ts e="T282" id="Seg_1151" n="HIAT:w" s="T281">Dĭgəttə</ts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1154" n="HIAT:w" s="T282">bazo</ts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1157" n="HIAT:w" s="T283">mašina</ts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1160" n="HIAT:w" s="T284">šonəga</ts>
                  <nts id="Seg_1161" n="HIAT:ip">.</nts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T289" id="Seg_1164" n="HIAT:u" s="T285">
                  <ts e="T286" id="Seg_1166" n="HIAT:w" s="T285">Măn</ts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1169" n="HIAT:w" s="T286">amnobiam</ts>
                  <nts id="Seg_1170" n="HIAT:ip">,</nts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1173" n="HIAT:w" s="T287">Malinovkinə</ts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1176" n="HIAT:w" s="T288">šobiam</ts>
                  <nts id="Seg_1177" n="HIAT:ip">.</nts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T293" id="Seg_1180" n="HIAT:u" s="T289">
                  <ts e="T290" id="Seg_1182" n="HIAT:w" s="T289">Dĭgəttə</ts>
                  <nts id="Seg_1183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1185" n="HIAT:w" s="T290">bazo</ts>
                  <nts id="Seg_1186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1188" n="HIAT:w" s="T291">üjüzi</ts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1191" n="HIAT:w" s="T292">kambiam</ts>
                  <nts id="Seg_1192" n="HIAT:ip">.</nts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_1195" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_1197" n="HIAT:w" s="T293">Dĭgəttə</ts>
                  <nts id="Seg_1198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1200" n="HIAT:w" s="T294">bazo</ts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1203" n="HIAT:w" s="T295">mašina</ts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1206" n="HIAT:w" s="T296">bĭdəbi</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1209" n="HIAT:w" s="T297">i</ts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1212" n="HIAT:w" s="T298">maːndə</ts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1215" n="HIAT:w" s="T299">šobiam</ts>
                  <nts id="Seg_1216" n="HIAT:ip">.</nts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T307" id="Seg_1219" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_1221" n="HIAT:w" s="T300">Kamən</ts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1224" n="HIAT:w" s="T301">Matvejev</ts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1227" n="HIAT:w" s="T302">šobi</ts>
                  <nts id="Seg_1228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1230" n="HIAT:w" s="T303">măna</ts>
                  <nts id="Seg_1231" n="HIAT:ip">,</nts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1233" n="HIAT:ip">(</nts>
                  <ts e="T305" id="Seg_1235" n="HIAT:w" s="T304">dʼăbaktərla-</ts>
                  <nts id="Seg_1236" n="HIAT:ip">)</nts>
                  <nts id="Seg_1237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1239" n="HIAT:w" s="T305">dʼăbaktərlaʔbə</ts>
                  <nts id="Seg_1240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1242" n="HIAT:w" s="T306">nudla</ts>
                  <nts id="Seg_1243" n="HIAT:ip">.</nts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T314" id="Seg_1246" n="HIAT:u" s="T307">
                  <ts e="T308" id="Seg_1248" n="HIAT:w" s="T307">A</ts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1251" n="HIAT:w" s="T308">măn</ts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1253" n="HIAT:ip">(</nts>
                  <ts e="T310" id="Seg_1255" n="HIAT:w" s="T309">kazak=</ts>
                  <nts id="Seg_1256" n="HIAT:ip">)</nts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1259" n="HIAT:w" s="T310">kazak</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1262" n="HIAT:w" s="T311">dʼăbaktərlaʔbəm</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1265" n="HIAT:w" s="T312">i</ts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1267" n="HIAT:ip">(</nts>
                  <ts e="T314" id="Seg_1269" n="HIAT:w" s="T313">petʼerlaʔbəm</ts>
                  <nts id="Seg_1270" n="HIAT:ip">)</nts>
                  <nts id="Seg_1271" n="HIAT:ip">.</nts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T322" id="Seg_1274" n="HIAT:u" s="T314">
                  <ts e="T315" id="Seg_1276" n="HIAT:w" s="T314">Tăn</ts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1279" n="HIAT:w" s="T315">tüj</ts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1282" n="HIAT:w" s="T316">nu</ts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1285" n="HIAT:w" s="T317">igel</ts>
                  <nts id="Seg_1286" n="HIAT:ip">,</nts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1289" n="HIAT:w" s="T318">a</ts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1292" n="HIAT:w" s="T319">măn</ts>
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1295" n="HIAT:w" s="T320">kazak</ts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1298" n="HIAT:w" s="T321">igem</ts>
                  <nts id="Seg_1299" n="HIAT:ip">.</nts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_1302" n="HIAT:u" s="T322">
                  <ts e="T323" id="Seg_1304" n="HIAT:w" s="T322">Dĭgəttə</ts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1307" n="HIAT:w" s="T323">dĭzeŋ</ts>
                  <nts id="Seg_1308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1310" n="HIAT:w" s="T324">dʼijenə</ts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1313" n="HIAT:w" s="T325">kambiʔi</ts>
                  <nts id="Seg_1314" n="HIAT:ip">,</nts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1317" n="HIAT:w" s="T326">Dʼelamdə</ts>
                  <nts id="Seg_1318" n="HIAT:ip">.</nts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T331" id="Seg_1321" n="HIAT:u" s="T327">
                  <nts id="Seg_1322" n="HIAT:ip">(</nts>
                  <ts e="T328" id="Seg_1324" n="HIAT:w" s="T327">Bi-</ts>
                  <nts id="Seg_1325" n="HIAT:ip">)</nts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1328" n="HIAT:w" s="T328">Kola</ts>
                  <nts id="Seg_1329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1331" n="HIAT:w" s="T329">dʼaʔpiʔi</ts>
                  <nts id="Seg_1332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1334" n="HIAT:w" s="T330">dĭn</ts>
                  <nts id="Seg_1335" n="HIAT:ip">.</nts>
                  <nts id="Seg_1336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T334" id="Seg_1338" n="HIAT:u" s="T331">
                  <ts e="T332" id="Seg_1340" n="HIAT:w" s="T331">Bi</ts>
                  <nts id="Seg_1341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1343" n="HIAT:w" s="T332">idʼem</ts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1346" n="HIAT:w" s="T333">enbiʔi</ts>
                  <nts id="Seg_1347" n="HIAT:ip">.</nts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T341" id="Seg_1350" n="HIAT:u" s="T334">
                  <ts e="T335" id="Seg_1352" n="HIAT:w" s="T334">Dĭgəttə</ts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1355" n="HIAT:w" s="T335">onʼiʔ</ts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1358" n="HIAT:w" s="T336">koʔbdo</ts>
                  <nts id="Seg_1359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1361" n="HIAT:w" s="T337">urgo</ts>
                  <nts id="Seg_1362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1364" n="HIAT:w" s="T338">bar</ts>
                  <nts id="Seg_1365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1367" n="HIAT:w" s="T339">ĭzemnuʔpi</ts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1370" n="HIAT:w" s="T340">bar</ts>
                  <nts id="Seg_1371" n="HIAT:ip">.</nts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T344" id="Seg_1374" n="HIAT:u" s="T341">
                  <ts e="T342" id="Seg_1376" n="HIAT:w" s="T341">Dĭgəttə</ts>
                  <nts id="Seg_1377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1378" n="HIAT:ip">(</nts>
                  <ts e="T343" id="Seg_1380" n="HIAT:w" s="T342">dĭzeŋ</ts>
                  <nts id="Seg_1381" n="HIAT:ip">)</nts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1384" n="HIAT:w" s="T343">šobiʔi</ts>
                  <nts id="Seg_1385" n="HIAT:ip">.</nts>
                  <nts id="Seg_1386" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_1388" n="HIAT:u" s="T344">
                  <ts e="T345" id="Seg_1390" n="HIAT:w" s="T344">Turanə</ts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1393" n="HIAT:w" s="T345">Kanoklerdə</ts>
                  <nts id="Seg_1394" n="HIAT:ip">.</nts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T353" id="Seg_1397" n="HIAT:u" s="T346">
                  <ts e="T347" id="Seg_1399" n="HIAT:w" s="T346">I</ts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1402" n="HIAT:w" s="T347">ne</ts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1405" n="HIAT:w" s="T348">ibiʔi</ts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1408" n="HIAT:w" s="T349">i</ts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1411" n="HIAT:w" s="T350">dĭm</ts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1414" n="HIAT:w" s="T351">deʔpiʔi</ts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1417" n="HIAT:w" s="T352">bălʼnʼitsanə</ts>
                  <nts id="Seg_1418" n="HIAT:ip">.</nts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T358" id="Seg_1421" n="HIAT:u" s="T353">
                  <ts e="T354" id="Seg_1423" n="HIAT:w" s="T353">Tĭn</ts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1426" n="HIAT:w" s="T354">üjün</ts>
                  <nts id="Seg_1427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1429" n="HIAT:w" s="T355">bar</ts>
                  <nts id="Seg_1430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1431" n="HIAT:ip">(</nts>
                  <ts e="T357" id="Seg_1433" n="HIAT:w" s="T356">bü</ts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1436" n="HIAT:w" s="T357">bĭʔpiʔi</ts>
                  <nts id="Seg_1437" n="HIAT:ip">)</nts>
                  <nts id="Seg_1438" n="HIAT:ip">.</nts>
                  <nts id="Seg_1439" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T360" id="Seg_1441" n="HIAT:u" s="T358">
                  <ts e="T359" id="Seg_1443" n="HIAT:w" s="T358">I</ts>
                  <nts id="Seg_1444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1446" n="HIAT:w" s="T359">dʼubtəbiʔi</ts>
                  <nts id="Seg_1447" n="HIAT:ip">.</nts>
                  <nts id="Seg_1448" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T361" id="Seg_1450" n="HIAT:u" s="T360">
                  <ts e="T361" id="Seg_1452" n="HIAT:w" s="T360">Dʼazirbiʔi</ts>
                  <nts id="Seg_1453" n="HIAT:ip">.</nts>
                  <nts id="Seg_1454" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T368" id="Seg_1456" n="HIAT:u" s="T361">
                  <ts e="T361.tx.1" id="Seg_1458" n="HIAT:w" s="T361">A</ts>
                  <nts id="Seg_1459" n="HIAT:ip">_</nts>
                  <ts e="T362" id="Seg_1461" n="HIAT:w" s="T361.tx.1">to</ts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1464" n="HIAT:w" s="T362">dĭ</ts>
                  <nts id="Seg_1465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1467" n="HIAT:w" s="T363">mĭnzittə</ts>
                  <nts id="Seg_1468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1469" n="HIAT:ip">(</nts>
                  <ts e="T365" id="Seg_1471" n="HIAT:w" s="T364">ej</ts>
                  <nts id="Seg_1472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1474" n="HIAT:w" s="T365">m-</ts>
                  <nts id="Seg_1475" n="HIAT:ip">)</nts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1478" n="HIAT:w" s="T366">ej</ts>
                  <nts id="Seg_1479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1481" n="HIAT:w" s="T367">molia</ts>
                  <nts id="Seg_1482" n="HIAT:ip">.</nts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T374" id="Seg_1485" n="HIAT:u" s="T368">
                  <ts e="T369" id="Seg_1487" n="HIAT:w" s="T368">Măn</ts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1489" n="HIAT:ip">(</nts>
                  <ts e="T370" id="Seg_1491" n="HIAT:w" s="T369">măndər-</ts>
                  <nts id="Seg_1492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1494" n="HIAT:w" s="T370">măn-</ts>
                  <nts id="Seg_1495" n="HIAT:ip">)</nts>
                  <nts id="Seg_1496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1498" n="HIAT:w" s="T371">măndərbiam</ts>
                  <nts id="Seg_1499" n="HIAT:ip">,</nts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1502" n="HIAT:w" s="T372">măndərbiam</ts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1504" n="HIAT:ip">(</nts>
                  <ts e="T374" id="Seg_1506" n="HIAT:w" s="T373">правда</ts>
                  <nts id="Seg_1507" n="HIAT:ip">)</nts>
                  <nts id="Seg_1508" n="HIAT:ip">.</nts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T377" id="Seg_1511" n="HIAT:u" s="T374">
                  <ts e="T375" id="Seg_1513" n="HIAT:w" s="T374">Dĭbər</ts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1515" n="HIAT:ip">(</nts>
                  <ts e="T376" id="Seg_1517" n="HIAT:w" s="T375">pʼaŋdəbiam</ts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1520" n="HIAT:w" s="T376">sazən</ts>
                  <nts id="Seg_1521" n="HIAT:ip">)</nts>
                  <nts id="Seg_1522" n="HIAT:ip">.</nts>
                  <nts id="Seg_1523" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T380" id="Seg_1525" n="HIAT:u" s="T377">
                  <ts e="T378" id="Seg_1527" n="HIAT:w" s="T377">Dĭbər</ts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1529" n="HIAT:ip">(</nts>
                  <ts e="T379" id="Seg_1531" n="HIAT:w" s="T378">pəŋ-</ts>
                  <nts id="Seg_1532" n="HIAT:ip">)</nts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1535" n="HIAT:w" s="T379">pʼaŋdəbiam</ts>
                  <nts id="Seg_1536" n="HIAT:ip">.</nts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T389" id="Seg_1539" n="HIAT:u" s="T380">
                  <ts e="T381" id="Seg_1541" n="HIAT:w" s="T380">Šindi</ts>
                  <nts id="Seg_1542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381.tx.1" id="Seg_1544" n="HIAT:w" s="T381">măn</ts>
                  <nts id="Seg_1545" n="HIAT:ip">_</nts>
                  <ts e="T383" id="Seg_1547" n="HIAT:w" s="T381.tx.1">də</ts>
                  <nts id="Seg_1548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1550" n="HIAT:w" s="T383">kulambi</ts>
                  <nts id="Seg_1551" n="HIAT:ip">,</nts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1554" n="HIAT:w" s="T384">šindi</ts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385.tx.1" id="Seg_1557" n="HIAT:w" s="T385">măn</ts>
                  <nts id="Seg_1558" n="HIAT:ip">_</nts>
                  <ts e="T387" id="Seg_1560" n="HIAT:w" s="T385.tx.1">də</ts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1563" n="HIAT:w" s="T387">ej</ts>
                  <nts id="Seg_1564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1566" n="HIAT:w" s="T388">kulambi</ts>
                  <nts id="Seg_1567" n="HIAT:ip">.</nts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T392" id="Seg_1570" n="HIAT:u" s="T389">
                  <ts e="T390" id="Seg_1572" n="HIAT:w" s="T389">Gijendə</ts>
                  <nts id="Seg_1573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1575" n="HIAT:w" s="T390">naga</ts>
                  <nts id="Seg_1576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1578" n="HIAT:w" s="T391">pravdaʔi</ts>
                  <nts id="Seg_1579" n="HIAT:ip">.</nts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T399" id="Seg_1582" n="HIAT:u" s="T392">
                  <ts e="T393" id="Seg_1584" n="HIAT:w" s="T392">A</ts>
                  <nts id="Seg_1585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1587" n="HIAT:w" s="T393">măn</ts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1590" n="HIAT:w" s="T394">tenəbiem</ts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1592" n="HIAT:ip">(</nts>
                  <ts e="T396" id="Seg_1594" n="HIAT:w" s="T395">ige=</ts>
                  <nts id="Seg_1595" n="HIAT:ip">)</nts>
                  <nts id="Seg_1596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1598" n="HIAT:w" s="T396">vezʼdʼe</ts>
                  <nts id="Seg_1599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1601" n="HIAT:w" s="T397">pravda</ts>
                  <nts id="Seg_1602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1604" n="HIAT:w" s="T398">ige</ts>
                  <nts id="Seg_1605" n="HIAT:ip">.</nts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T405" id="Seg_1608" n="HIAT:u" s="T399">
                  <ts e="T400" id="Seg_1610" n="HIAT:w" s="T399">A</ts>
                  <nts id="Seg_1611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1613" n="HIAT:w" s="T400">dĭm</ts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1616" n="HIAT:w" s="T401">gijendə</ts>
                  <nts id="Seg_1617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1619" n="HIAT:w" s="T402">naga</ts>
                  <nts id="Seg_1620" n="HIAT:ip">,</nts>
                  <nts id="Seg_1621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1623" n="HIAT:w" s="T403">üge</ts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1625" n="HIAT:ip">(</nts>
                  <ts e="T405" id="Seg_1627" n="HIAT:w" s="T404">šanaklʼu</ts>
                  <nts id="Seg_1628" n="HIAT:ip">)</nts>
                  <nts id="Seg_1629" n="HIAT:ip">.</nts>
                  <nts id="Seg_1630" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T412" id="Seg_1632" n="HIAT:u" s="T405">
                  <ts e="T406" id="Seg_1634" n="HIAT:w" s="T405">Dĭgəttə</ts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1637" n="HIAT:w" s="T406">prokuror</ts>
                  <nts id="Seg_1638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1640" n="HIAT:w" s="T407">pʼaŋdəbi</ts>
                  <nts id="Seg_1641" n="HIAT:ip">,</nts>
                  <nts id="Seg_1642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1643" n="HIAT:ip">(</nts>
                  <ts e="T409" id="Seg_1645" n="HIAT:w" s="T408">gibərdə</ts>
                  <nts id="Seg_1646" n="HIAT:ip">)</nts>
                  <nts id="Seg_1647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_1649" n="HIAT:w" s="T409">kalla</ts>
                  <nts id="Seg_1650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1652" n="HIAT:w" s="T869">dʼürbi</ts>
                  <nts id="Seg_1653" n="HIAT:ip">,</nts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1656" n="HIAT:w" s="T410">ej</ts>
                  <nts id="Seg_1657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1659" n="HIAT:w" s="T411">tĭmnem</ts>
                  <nts id="Seg_1660" n="HIAT:ip">.</nts>
                  <nts id="Seg_1661" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T415" id="Seg_1663" n="HIAT:u" s="T412">
                  <ts e="T413" id="Seg_1665" n="HIAT:w" s="T412">Jelʼa</ts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1667" n="HIAT:ip">(</nts>
                  <ts e="T414" id="Seg_1669" n="HIAT:w" s="T413">kuʔkubi</ts>
                  <nts id="Seg_1670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1672" n="HIAT:w" s="T414">dʼodunʼim</ts>
                  <nts id="Seg_1673" n="HIAT:ip">)</nts>
                  <nts id="Seg_1674" n="HIAT:ip">.</nts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T419" id="Seg_1677" n="HIAT:u" s="T415">
                  <ts e="T416" id="Seg_1679" n="HIAT:w" s="T415">Bar</ts>
                  <nts id="Seg_1680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1682" n="HIAT:w" s="T416">lotkanə</ts>
                  <nts id="Seg_1683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1685" n="HIAT:w" s="T417">bünə</ts>
                  <nts id="Seg_1686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1688" n="HIAT:w" s="T418">kambiʔi</ts>
                  <nts id="Seg_1689" n="HIAT:ip">.</nts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T425" id="Seg_1692" n="HIAT:u" s="T419">
                  <nts id="Seg_1693" n="HIAT:ip">(</nts>
                  <ts e="T420" id="Seg_1695" n="HIAT:w" s="T419">Dĭgəttə</ts>
                  <nts id="Seg_1696" n="HIAT:ip">)</nts>
                  <nts id="Seg_1697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1699" n="HIAT:w" s="T420">girgitdə</ts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1702" n="HIAT:w" s="T421">koʔbdozi</ts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1705" n="HIAT:w" s="T422">xatʼeli</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1708" n="HIAT:w" s="T423">bügən</ts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1711" n="HIAT:w" s="T424">плавать</ts>
                  <nts id="Seg_1712" n="HIAT:ip">.</nts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T429" id="Seg_1715" n="HIAT:u" s="T425">
                  <ts e="T426" id="Seg_1717" n="HIAT:w" s="T425">A</ts>
                  <nts id="Seg_1718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1720" n="HIAT:w" s="T426">bü</ts>
                  <nts id="Seg_1721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1723" n="HIAT:w" s="T427">ugandə</ts>
                  <nts id="Seg_1724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1726" n="HIAT:w" s="T428">urgo</ts>
                  <nts id="Seg_1727" n="HIAT:ip">.</nts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T434" id="Seg_1730" n="HIAT:u" s="T429">
                  <ts e="T430" id="Seg_1732" n="HIAT:w" s="T429">Dĭzeŋ</ts>
                  <nts id="Seg_1733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1735" n="HIAT:w" s="T430">bar</ts>
                  <nts id="Seg_1736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1738" n="HIAT:w" s="T431">xatʼeli</ts>
                  <nts id="Seg_1739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1741" n="HIAT:w" s="T432">lotkanə</ts>
                  <nts id="Seg_1742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1744" n="HIAT:w" s="T433">sʼazittə</ts>
                  <nts id="Seg_1745" n="HIAT:ip">.</nts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T437" id="Seg_1748" n="HIAT:u" s="T434">
                  <ts e="T435" id="Seg_1750" n="HIAT:w" s="T434">Da</ts>
                  <nts id="Seg_1751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1753" n="HIAT:w" s="T435">bar</ts>
                  <nts id="Seg_1754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1756" n="HIAT:w" s="T436">nereʔluʔpiʔi</ts>
                  <nts id="Seg_1757" n="HIAT:ip">.</nts>
                  <nts id="Seg_1758" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T439" id="Seg_1760" n="HIAT:u" s="T437">
                  <ts e="T439" id="Seg_1762" n="HIAT:w" s="T437">Dĭgəttə</ts>
                  <nts id="Seg_1763" n="HIAT:ip">…</nts>
                  <nts id="Seg_1764" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T442" id="Seg_1766" n="HIAT:u" s="T439">
                  <ts e="T440" id="Seg_1768" n="HIAT:w" s="T439">Dĭgəttə</ts>
                  <nts id="Seg_1769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1771" n="HIAT:w" s="T440">dĭn</ts>
                  <nts id="Seg_1772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1773" n="HIAT:ip">(</nts>
                  <ts e="T442" id="Seg_1775" n="HIAT:w" s="T441">nereʔluʔpiʔi</ts>
                  <nts id="Seg_1776" n="HIAT:ip">)</nts>
                  <nts id="Seg_1777" n="HIAT:ip">.</nts>
                  <nts id="Seg_1778" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T448" id="Seg_1780" n="HIAT:u" s="T442">
                  <ts e="T443" id="Seg_1782" n="HIAT:w" s="T442">Oʔbdəbi</ts>
                  <nts id="Seg_1783" n="HIAT:ip">,</nts>
                  <nts id="Seg_1784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1785" n="HIAT:ip">(</nts>
                  <ts e="T444" id="Seg_1787" n="HIAT:w" s="T443">kuiol-</ts>
                  <nts id="Seg_1788" n="HIAT:ip">)</nts>
                  <nts id="Seg_1789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1791" n="HIAT:w" s="T444">kuliat</ts>
                  <nts id="Seg_1792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1794" n="HIAT:w" s="T445">što</ts>
                  <nts id="Seg_1795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1797" n="HIAT:w" s="T446">dʼoduni</ts>
                  <nts id="Seg_1798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1800" n="HIAT:w" s="T447">kübi</ts>
                  <nts id="Seg_1801" n="HIAT:ip">.</nts>
                  <nts id="Seg_1802" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T453" id="Seg_1804" n="HIAT:u" s="T448">
                  <nts id="Seg_1805" n="HIAT:ip">(</nts>
                  <ts e="T449" id="Seg_1807" n="HIAT:w" s="T448">Sălămatən</ts>
                  <nts id="Seg_1808" n="HIAT:ip">)</nts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1811" n="HIAT:w" s="T449">Mitrăfanən</ts>
                  <nts id="Seg_1812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1814" n="HIAT:w" s="T450">turat</ts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1817" n="HIAT:w" s="T451">iššo</ts>
                  <nts id="Seg_1818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1820" n="HIAT:w" s="T452">nuga</ts>
                  <nts id="Seg_1821" n="HIAT:ip">.</nts>
                  <nts id="Seg_1822" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T461" id="Seg_1824" n="HIAT:u" s="T453">
                  <nts id="Seg_1825" n="HIAT:ip">(</nts>
                  <ts e="T454" id="Seg_1827" n="HIAT:w" s="T453">I</ts>
                  <nts id="Seg_1828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1830" n="HIAT:w" s="T454">ku-</ts>
                  <nts id="Seg_1831" n="HIAT:ip">)</nts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1834" n="HIAT:w" s="T455">I</ts>
                  <nts id="Seg_1835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1837" n="HIAT:w" s="T456">gurun</ts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1840" n="HIAT:w" s="T457">Andʼigatovan</ts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1843" n="HIAT:w" s="T458">tura</ts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1846" n="HIAT:w" s="T459">iššo</ts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1849" n="HIAT:w" s="T460">nuga</ts>
                  <nts id="Seg_1850" n="HIAT:ip">.</nts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T468" id="Seg_1853" n="HIAT:u" s="T461">
                  <nts id="Seg_1854" n="HIAT:ip">(</nts>
                  <ts e="T462" id="Seg_1856" n="HIAT:w" s="T461">Dʼibiai-</ts>
                  <nts id="Seg_1857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1859" n="HIAT:w" s="T462">ən-</ts>
                  <nts id="Seg_1860" n="HIAT:ip">)</nts>
                  <nts id="Seg_1861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1863" n="HIAT:w" s="T463">Dʼibiev</ts>
                  <nts id="Seg_1864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1866" n="HIAT:w" s="T464">Vălodʼan</ts>
                  <nts id="Seg_1867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1869" n="HIAT:w" s="T465">tura</ts>
                  <nts id="Seg_1870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1872" n="HIAT:w" s="T466">iššo</ts>
                  <nts id="Seg_1873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1875" n="HIAT:w" s="T467">nuga</ts>
                  <nts id="Seg_1876" n="HIAT:ip">.</nts>
                  <nts id="Seg_1877" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T473" id="Seg_1879" n="HIAT:u" s="T468">
                  <ts e="T469" id="Seg_1881" n="HIAT:w" s="T468">Kotʼerov</ts>
                  <nts id="Seg_1882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1884" n="HIAT:w" s="T469">Mikitan</ts>
                  <nts id="Seg_1885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1887" n="HIAT:w" s="T470">tura</ts>
                  <nts id="Seg_1888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1890" n="HIAT:w" s="T471">iššo</ts>
                  <nts id="Seg_1891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1893" n="HIAT:w" s="T472">nuga</ts>
                  <nts id="Seg_1894" n="HIAT:ip">.</nts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T478" id="Seg_1897" n="HIAT:u" s="T473">
                  <nts id="Seg_1898" n="HIAT:ip">(</nts>
                  <ts e="T474" id="Seg_1900" n="HIAT:w" s="T473">Gateva</ts>
                  <nts id="Seg_1901" n="HIAT:ip">)</nts>
                  <nts id="Seg_1902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1904" n="HIAT:w" s="T474">Afanasin</ts>
                  <nts id="Seg_1905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1907" n="HIAT:w" s="T475">tura</ts>
                  <nts id="Seg_1908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1910" n="HIAT:w" s="T476">iššo</ts>
                  <nts id="Seg_1911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1913" n="HIAT:w" s="T477">nuga</ts>
                  <nts id="Seg_1914" n="HIAT:ip">.</nts>
                  <nts id="Seg_1915" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T484" id="Seg_1917" n="HIAT:u" s="T478">
                  <ts e="T479" id="Seg_1919" n="HIAT:w" s="T478">I</ts>
                  <nts id="Seg_1920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1922" n="HIAT:w" s="T479">Šajbin</ts>
                  <nts id="Seg_1923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1925" n="HIAT:w" s="T480">Vlastə</ts>
                  <nts id="Seg_1926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1928" n="HIAT:w" s="T481">iššo</ts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1931" n="HIAT:w" s="T482">tura</ts>
                  <nts id="Seg_1932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1934" n="HIAT:w" s="T483">nuga</ts>
                  <nts id="Seg_1935" n="HIAT:ip">.</nts>
                  <nts id="Seg_1936" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T491" id="Seg_1938" n="HIAT:u" s="T484">
                  <ts e="T485" id="Seg_1940" n="HIAT:w" s="T484">I</ts>
                  <nts id="Seg_1941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1942" n="HIAT:ip">(</nts>
                  <ts e="T486" id="Seg_1944" n="HIAT:w" s="T485">Solomatov=</ts>
                  <nts id="Seg_1945" n="HIAT:ip">)</nts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1947" n="HIAT:ip">(</nts>
                  <ts e="T487" id="Seg_1949" n="HIAT:w" s="T486">Sălămatov</ts>
                  <nts id="Seg_1950" n="HIAT:ip">)</nts>
                  <nts id="Seg_1951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1953" n="HIAT:w" s="T487">Vasilij</ts>
                  <nts id="Seg_1954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1956" n="HIAT:w" s="T488">iššo</ts>
                  <nts id="Seg_1957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1959" n="HIAT:w" s="T489">tura</ts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1961" n="HIAT:ip">(</nts>
                  <ts e="T491" id="Seg_1963" n="HIAT:w" s="T490">nuga</ts>
                  <nts id="Seg_1964" n="HIAT:ip">)</nts>
                  <nts id="Seg_1965" n="HIAT:ip">.</nts>
                  <nts id="Seg_1966" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T502" id="Seg_1968" n="HIAT:u" s="T491">
                  <ts e="T492" id="Seg_1970" n="HIAT:w" s="T491">Kamən</ts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1973" n="HIAT:w" s="T492">Tugarin</ts>
                  <nts id="Seg_1974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1976" n="HIAT:w" s="T493">šobi</ts>
                  <nts id="Seg_1977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1979" n="HIAT:w" s="T494">dʼăbaktərzittə</ts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1982" n="HIAT:w" s="T495">döbər</ts>
                  <nts id="Seg_1983" n="HIAT:ip">,</nts>
                  <nts id="Seg_1984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1986" n="HIAT:w" s="T496">dön</ts>
                  <nts id="Seg_1987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1988" n="HIAT:ip">(</nts>
                  <ts e="T498" id="Seg_1990" n="HIAT:w" s="T497">su-</ts>
                  <nts id="Seg_1991" n="HIAT:ip">)</nts>
                  <nts id="Seg_1992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1994" n="HIAT:w" s="T498">sumna</ts>
                  <nts id="Seg_1995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1997" n="HIAT:w" s="T499">bʼeʔ</ts>
                  <nts id="Seg_1998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_2000" n="HIAT:w" s="T500">tura</ts>
                  <nts id="Seg_2001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_2003" n="HIAT:w" s="T501">ibi</ts>
                  <nts id="Seg_2004" n="HIAT:ip">.</nts>
                  <nts id="Seg_2005" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T506" id="Seg_2007" n="HIAT:u" s="T502">
                  <nts id="Seg_2008" n="HIAT:ip">(</nts>
                  <ts e="T503" id="Seg_2010" n="HIAT:w" s="T502">Sargolov</ts>
                  <nts id="Seg_2011" n="HIAT:ip">)</nts>
                  <nts id="Seg_2012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_2014" n="HIAT:w" s="T503">Jakov</ts>
                  <nts id="Seg_2015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2016" n="HIAT:ip">(</nts>
                  <ts e="T505" id="Seg_2018" n="HIAT:w" s="T504">Filippovič</ts>
                  <nts id="Seg_2019" n="HIAT:ip">)</nts>
                  <nts id="Seg_2020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_2022" n="HIAT:w" s="T505">külaːmbi</ts>
                  <nts id="Seg_2023" n="HIAT:ip">.</nts>
                  <nts id="Seg_2024" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T517" id="Seg_2026" n="HIAT:u" s="T506">
                  <ts e="T507" id="Seg_2028" n="HIAT:w" s="T506">Dĭ</ts>
                  <nts id="Seg_2029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_2031" n="HIAT:w" s="T507">kambi</ts>
                  <nts id="Seg_2032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_2034" n="HIAT:w" s="T508">Permʼakovtə</ts>
                  <nts id="Seg_2035" n="HIAT:ip">,</nts>
                  <nts id="Seg_2036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_2038" n="HIAT:w" s="T509">dĭn</ts>
                  <nts id="Seg_2039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_2041" n="HIAT:w" s="T510">ara</ts>
                  <nts id="Seg_2042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_2044" n="HIAT:w" s="T511">bĭʔpi</ts>
                  <nts id="Seg_2045" n="HIAT:ip">,</nts>
                  <nts id="Seg_2046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_2048" n="HIAT:w" s="T512">dĭn</ts>
                  <nts id="Seg_2049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2050" n="HIAT:ip">(</nts>
                  <ts e="T514" id="Seg_2052" n="HIAT:w" s="T513">i-</ts>
                  <nts id="Seg_2053" n="HIAT:ip">)</nts>
                  <nts id="Seg_2054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_2056" n="HIAT:w" s="T514">ibi</ts>
                  <nts id="Seg_2057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_2059" n="HIAT:w" s="T515">onʼiʔ</ts>
                  <nts id="Seg_2060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_2062" n="HIAT:w" s="T516">бутылка</ts>
                  <nts id="Seg_2063" n="HIAT:ip">.</nts>
                  <nts id="Seg_2064" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T520" id="Seg_2066" n="HIAT:u" s="T517">
                  <ts e="T518" id="Seg_2068" n="HIAT:w" s="T517">I</ts>
                  <nts id="Seg_2069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_2071" n="HIAT:w" s="T518">netsi</ts>
                  <nts id="Seg_2072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_2074" n="HIAT:w" s="T519">šobi</ts>
                  <nts id="Seg_2075" n="HIAT:ip">.</nts>
                  <nts id="Seg_2076" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T523" id="Seg_2078" n="HIAT:u" s="T520">
                  <ts e="T521" id="Seg_2080" n="HIAT:w" s="T520">Dĭgəttə</ts>
                  <nts id="Seg_2081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_2083" n="HIAT:w" s="T521">kambi</ts>
                  <nts id="Seg_2084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_2086" n="HIAT:w" s="T522">krospaʔinə</ts>
                  <nts id="Seg_2087" n="HIAT:ip">.</nts>
                  <nts id="Seg_2088" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T531" id="Seg_2090" n="HIAT:u" s="T523">
                  <ts e="T524" id="Seg_2092" n="HIAT:w" s="T523">Dĭgəttə</ts>
                  <nts id="Seg_2093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_2095" n="HIAT:w" s="T524">kămlia</ts>
                  <nts id="Seg_2096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_2098" n="HIAT:w" s="T525">da</ts>
                  <nts id="Seg_2099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2100" n="HIAT:ip">(</nts>
                  <ts e="T526.tx.1" id="Seg_2102" n="HIAT:w" s="T526">kudonzlia</ts>
                  <nts id="Seg_2103" n="HIAT:ip">)</nts>
                  <ts e="T527" id="Seg_2105" n="HIAT:w" s="T526.tx.1">:</ts>
                  <nts id="Seg_2106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_2108" n="HIAT:w" s="T527">Bĭdeʔ</ts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_2111" n="HIAT:w" s="T528">nükem</ts>
                  <nts id="Seg_2112" n="HIAT:ip">,</nts>
                  <nts id="Seg_2113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_2115" n="HIAT:w" s="T529">bĭdeʔ</ts>
                  <nts id="Seg_2116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_2118" n="HIAT:w" s="T530">nükem</ts>
                  <nts id="Seg_2119" n="HIAT:ip">!</nts>
                  <nts id="Seg_2120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T539" id="Seg_2122" n="HIAT:u" s="T531">
                  <ts e="T532" id="Seg_2124" n="HIAT:w" s="T531">A</ts>
                  <nts id="Seg_2125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_2127" n="HIAT:w" s="T532">măn</ts>
                  <nts id="Seg_2128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_2130" n="HIAT:w" s="T533">abam</ts>
                  <nts id="Seg_2131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_2133" n="HIAT:w" s="T534">nuga</ts>
                  <nts id="Seg_2134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_2136" n="HIAT:w" s="T535">da</ts>
                  <nts id="Seg_2137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_2139" n="HIAT:w" s="T536">măndolaʔbə</ts>
                  <nts id="Seg_2140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2142" n="HIAT:w" s="T537">da</ts>
                  <nts id="Seg_2143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2145" n="HIAT:w" s="T538">kaknarlaʔbə</ts>
                  <nts id="Seg_2146" n="HIAT:ip">.</nts>
                  <nts id="Seg_2147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T542" id="Seg_2149" n="HIAT:u" s="T539">
                  <ts e="T540" id="Seg_2151" n="HIAT:w" s="T539">Măn</ts>
                  <nts id="Seg_2152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_2154" n="HIAT:w" s="T540">noʔ</ts>
                  <nts id="Seg_2155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2157" n="HIAT:w" s="T541">jaʔpiam</ts>
                  <nts id="Seg_2158" n="HIAT:ip">.</nts>
                  <nts id="Seg_2159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T544" id="Seg_2161" n="HIAT:u" s="T542">
                  <nts id="Seg_2162" n="HIAT:ip">(</nts>
                  <ts e="T543" id="Seg_2164" n="HIAT:w" s="T542">Dĭgəttə</ts>
                  <nts id="Seg_2165" n="HIAT:ip">)</nts>
                  <nts id="Seg_2166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2168" n="HIAT:w" s="T543">oʔbdəbiam</ts>
                  <nts id="Seg_2169" n="HIAT:ip">.</nts>
                  <nts id="Seg_2170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T547" id="Seg_2172" n="HIAT:u" s="T544">
                  <ts e="T545" id="Seg_2174" n="HIAT:w" s="T544">Dĭgəttə</ts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2177" n="HIAT:w" s="T545">stogdə</ts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2180" n="HIAT:w" s="T546">embiem</ts>
                  <nts id="Seg_2181" n="HIAT:ip">.</nts>
                  <nts id="Seg_2182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T550" id="Seg_2184" n="HIAT:u" s="T547">
                  <ts e="T548" id="Seg_2186" n="HIAT:w" s="T547">Dĭ</ts>
                  <nts id="Seg_2187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2189" n="HIAT:w" s="T548">dʼüʔpi</ts>
                  <nts id="Seg_2190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2192" n="HIAT:w" s="T549">ibi</ts>
                  <nts id="Seg_2193" n="HIAT:ip">.</nts>
                  <nts id="Seg_2194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T556" id="Seg_2196" n="HIAT:u" s="T550">
                  <ts e="T551" id="Seg_2198" n="HIAT:w" s="T550">Dĭgəttə</ts>
                  <nts id="Seg_2199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2201" n="HIAT:w" s="T551">nagur</ts>
                  <nts id="Seg_2202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2203" n="HIAT:ip">(</nts>
                  <ts e="T553" id="Seg_2205" n="HIAT:w" s="T552">dʼara-</ts>
                  <nts id="Seg_2206" n="HIAT:ip">)</nts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2209" n="HIAT:w" s="T553">dʼala</ts>
                  <nts id="Seg_2210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2212" n="HIAT:w" s="T554">ej</ts>
                  <nts id="Seg_2213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2215" n="HIAT:w" s="T555">kambiam</ts>
                  <nts id="Seg_2216" n="HIAT:ip">.</nts>
                  <nts id="Seg_2217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T559" id="Seg_2219" n="HIAT:u" s="T556">
                  <ts e="T557" id="Seg_2221" n="HIAT:w" s="T556">Ugandə</ts>
                  <nts id="Seg_2222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2224" n="HIAT:w" s="T557">dʼibige</ts>
                  <nts id="Seg_2225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2227" n="HIAT:w" s="T558">molambi</ts>
                  <nts id="Seg_2228" n="HIAT:ip">.</nts>
                  <nts id="Seg_2229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T566" id="Seg_2231" n="HIAT:u" s="T559">
                  <nts id="Seg_2232" n="HIAT:ip">(</nts>
                  <ts e="T560" id="Seg_2234" n="HIAT:w" s="T559">Mɨ-</ts>
                  <nts id="Seg_2235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2237" n="HIAT:w" s="T560">ši-</ts>
                  <nts id="Seg_2238" n="HIAT:ip">)</nts>
                  <nts id="Seg_2239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2241" n="HIAT:w" s="T561">Măn</ts>
                  <nts id="Seg_2242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2244" n="HIAT:w" s="T562">šide</ts>
                  <nts id="Seg_2245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2247" n="HIAT:w" s="T563">ne</ts>
                  <nts id="Seg_2248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2250" n="HIAT:w" s="T564">ibiem</ts>
                  <nts id="Seg_2251" n="HIAT:ip">,</nts>
                  <nts id="Seg_2252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2254" n="HIAT:w" s="T565">kambiam</ts>
                  <nts id="Seg_2255" n="HIAT:ip">.</nts>
                  <nts id="Seg_2256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T574" id="Seg_2258" n="HIAT:u" s="T566">
                  <ts e="T567" id="Seg_2260" n="HIAT:w" s="T566">Bar</ts>
                  <nts id="Seg_2261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2263" n="HIAT:w" s="T567">kobi</ts>
                  <nts id="Seg_2264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2266" n="HIAT:w" s="T568">da</ts>
                  <nts id="Seg_2267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2269" n="HIAT:w" s="T569">bazoʔ</ts>
                  <nts id="Seg_2270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2272" n="HIAT:w" s="T570">embibeʔ</ts>
                  <nts id="Seg_2273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2275" n="HIAT:w" s="T571">kuvas</ts>
                  <nts id="Seg_2276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2277" n="HIAT:ip">(</nts>
                  <ts e="T573" id="Seg_2279" n="HIAT:w" s="T572">zəʔ-</ts>
                  <nts id="Seg_2280" n="HIAT:ip">)</nts>
                  <nts id="Seg_2281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2282" n="HIAT:ip">(</nts>
                  <ts e="T574" id="Seg_2284" n="HIAT:w" s="T573">stog</ts>
                  <nts id="Seg_2285" n="HIAT:ip">)</nts>
                  <nts id="Seg_2286" n="HIAT:ip">.</nts>
                  <nts id="Seg_2287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T579" id="Seg_2289" n="HIAT:u" s="T574">
                  <ts e="T575" id="Seg_2291" n="HIAT:w" s="T574">Măn</ts>
                  <nts id="Seg_2292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2294" n="HIAT:w" s="T575">amnobiam</ts>
                  <nts id="Seg_2295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2297" n="HIAT:w" s="T576">šide</ts>
                  <nts id="Seg_2298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2300" n="HIAT:w" s="T577">bʼeʔ</ts>
                  <nts id="Seg_2301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2303" n="HIAT:w" s="T578">pʼe</ts>
                  <nts id="Seg_2304" n="HIAT:ip">.</nts>
                  <nts id="Seg_2305" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T582" id="Seg_2307" n="HIAT:u" s="T579">
                  <ts e="T580" id="Seg_2309" n="HIAT:w" s="T579">Bospə</ts>
                  <nts id="Seg_2310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2312" n="HIAT:w" s="T580">noʔ</ts>
                  <nts id="Seg_2313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2315" n="HIAT:w" s="T581">jaʔpiam</ts>
                  <nts id="Seg_2316" n="HIAT:ip">.</nts>
                  <nts id="Seg_2317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T584" id="Seg_2319" n="HIAT:u" s="T582">
                  <ts e="T583" id="Seg_2321" n="HIAT:w" s="T582">Bostə</ts>
                  <nts id="Seg_2322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2324" n="HIAT:w" s="T583">oʔbdəbiam</ts>
                  <nts id="Seg_2325" n="HIAT:ip">.</nts>
                  <nts id="Seg_2326" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T586" id="Seg_2328" n="HIAT:u" s="T584">
                  <ts e="T585" id="Seg_2330" n="HIAT:w" s="T584">Bostə</ts>
                  <nts id="Seg_2331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2333" n="HIAT:w" s="T585">embiem</ts>
                  <nts id="Seg_2334" n="HIAT:ip">.</nts>
                  <nts id="Seg_2335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T590" id="Seg_2337" n="HIAT:u" s="T586">
                  <ts e="T587" id="Seg_2339" n="HIAT:w" s="T586">Inezi</ts>
                  <nts id="Seg_2340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2342" n="HIAT:w" s="T587">maʔnʼi</ts>
                  <nts id="Seg_2343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2345" n="HIAT:w" s="T588">tažerbiam</ts>
                  <nts id="Seg_2346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2348" n="HIAT:w" s="T589">bar</ts>
                  <nts id="Seg_2349" n="HIAT:ip">.</nts>
                  <nts id="Seg_2350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T594" id="Seg_2352" n="HIAT:u" s="T590">
                  <nts id="Seg_2353" n="HIAT:ip">(</nts>
                  <ts e="T591" id="Seg_2355" n="HIAT:w" s="T590">Tüžöjʔim</ts>
                  <nts id="Seg_2356" n="HIAT:ip">)</nts>
                  <nts id="Seg_2357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2358" n="HIAT:ip">(</nts>
                  <ts e="T592" id="Seg_2360" n="HIAT:w" s="T591">im</ts>
                  <nts id="Seg_2361" n="HIAT:ip">)</nts>
                  <nts id="Seg_2362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2363" n="HIAT:ip">(</nts>
                  <ts e="T593" id="Seg_2365" n="HIAT:w" s="T592">bĭd-</ts>
                  <nts id="Seg_2366" n="HIAT:ip">)</nts>
                  <nts id="Seg_2367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2369" n="HIAT:w" s="T593">bădlaʔpiam</ts>
                  <nts id="Seg_2370" n="HIAT:ip">.</nts>
                  <nts id="Seg_2371" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T597" id="Seg_2373" n="HIAT:u" s="T594">
                  <ts e="T595" id="Seg_2375" n="HIAT:w" s="T594">Onʼiʔ</ts>
                  <nts id="Seg_2376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2378" n="HIAT:w" s="T595">ine</ts>
                  <nts id="Seg_2379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2381" n="HIAT:w" s="T596">ibiem</ts>
                  <nts id="Seg_2382" n="HIAT:ip">.</nts>
                  <nts id="Seg_2383" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T607" id="Seg_2385" n="HIAT:u" s="T597">
                  <ts e="T598" id="Seg_2387" n="HIAT:w" s="T597">Dĭgəttə</ts>
                  <nts id="Seg_2388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2390" n="HIAT:w" s="T598">plemʼannicat</ts>
                  <nts id="Seg_2391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2392" n="HIAT:ip">(</nts>
                  <ts e="T600" id="Seg_2394" n="HIAT:w" s="T599">nʼin</ts>
                  <nts id="Seg_2395" n="HIAT:ip">)</nts>
                  <nts id="Seg_2396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2398" n="HIAT:w" s="T600">ibiem</ts>
                  <nts id="Seg_2399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2401" n="HIAT:w" s="T601">da</ts>
                  <nts id="Seg_2402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2404" n="HIAT:w" s="T602">iššo</ts>
                  <nts id="Seg_2405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2406" n="HIAT:ip">(</nts>
                  <ts e="T604" id="Seg_2408" n="HIAT:w" s="T603">dʼügej</ts>
                  <nts id="Seg_2409" n="HIAT:ip">)</nts>
                  <nts id="Seg_2410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2412" n="HIAT:w" s="T604">kambibaʔ</ts>
                  <nts id="Seg_2413" n="HIAT:ip">,</nts>
                  <nts id="Seg_2414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2416" n="HIAT:w" s="T605">noʔ</ts>
                  <nts id="Seg_2417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2419" n="HIAT:w" s="T606">embibeʔ</ts>
                  <nts id="Seg_2420" n="HIAT:ip">.</nts>
                  <nts id="Seg_2421" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T611" id="Seg_2423" n="HIAT:u" s="T607">
                  <ts e="T608" id="Seg_2425" n="HIAT:w" s="T607">Dĭgəttə</ts>
                  <nts id="Seg_2426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2428" n="HIAT:w" s="T608">šobibaʔ</ts>
                  <nts id="Seg_2429" n="HIAT:ip">,</nts>
                  <nts id="Seg_2430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2432" n="HIAT:w" s="T609">šobibaʔ</ts>
                  <nts id="Seg_2433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2435" n="HIAT:w" s="T610">miʔ</ts>
                  <nts id="Seg_2436" n="HIAT:ip">.</nts>
                  <nts id="Seg_2437" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T614" id="Seg_2439" n="HIAT:u" s="T611">
                  <ts e="T612" id="Seg_2441" n="HIAT:w" s="T611">Noʔ</ts>
                  <nts id="Seg_2442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2443" n="HIAT:ip">(</nts>
                  <ts e="T613" id="Seg_2445" n="HIAT:w" s="T612">üzə-</ts>
                  <nts id="Seg_2446" n="HIAT:ip">)</nts>
                  <nts id="Seg_2447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_2449" n="HIAT:w" s="T613">üzəbi</ts>
                  <nts id="Seg_2450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2451" n="HIAT:ip">(</nts>
                  <ts e="T614" id="Seg_2453" n="HIAT:w" s="T867">dʼuno</ts>
                  <nts id="Seg_2454" n="HIAT:ip">)</nts>
                  <nts id="Seg_2455" n="HIAT:ip">.</nts>
                  <nts id="Seg_2456" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T619" id="Seg_2458" n="HIAT:u" s="T614">
                  <ts e="T615" id="Seg_2460" n="HIAT:w" s="T614">Dĭgəttə</ts>
                  <nts id="Seg_2461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2463" n="HIAT:w" s="T615">dʼildəbibeʔ</ts>
                  <nts id="Seg_2464" n="HIAT:ip">,</nts>
                  <nts id="Seg_2465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2467" n="HIAT:w" s="T616">dʼildəbibeʔ</ts>
                  <nts id="Seg_2468" n="HIAT:ip">,</nts>
                  <nts id="Seg_2469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2471" n="HIAT:w" s="T617">ej</ts>
                  <nts id="Seg_2472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2474" n="HIAT:w" s="T618">moliabaʔ</ts>
                  <nts id="Seg_2475" n="HIAT:ip">.</nts>
                  <nts id="Seg_2476" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T626" id="Seg_2478" n="HIAT:u" s="T619">
                  <nts id="Seg_2479" n="HIAT:ip">(</nts>
                  <ts e="T620" id="Seg_2481" n="HIAT:w" s="T619">Dĭ</ts>
                  <nts id="Seg_2482" n="HIAT:ip">)</nts>
                  <nts id="Seg_2483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2485" n="HIAT:w" s="T620">măndə:</ts>
                  <nts id="Seg_2486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2488" n="HIAT:w" s="T621">Tüjö</ts>
                  <nts id="Seg_2489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2491" n="HIAT:w" s="T622">ine</ts>
                  <nts id="Seg_2492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2493" n="HIAT:ip">(</nts>
                  <ts e="T624" id="Seg_2495" n="HIAT:w" s="T623">kor-</ts>
                  <nts id="Seg_2496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2498" n="HIAT:w" s="T624">kor-</ts>
                  <nts id="Seg_2499" n="HIAT:ip">)</nts>
                  <nts id="Seg_2500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2501" n="HIAT:ip">(</nts>
                  <ts e="T626" id="Seg_2503" n="HIAT:w" s="T625">körerbim</ts>
                  <nts id="Seg_2504" n="HIAT:ip">)</nts>
                  <nts id="Seg_2505" n="HIAT:ip">.</nts>
                  <nts id="Seg_2506" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T628" id="Seg_2508" n="HIAT:u" s="T626">
                  <ts e="T627" id="Seg_2510" n="HIAT:w" s="T626">Dĭgəttə</ts>
                  <nts id="Seg_2511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2513" n="HIAT:w" s="T627">dʼildlim</ts>
                  <nts id="Seg_2514" n="HIAT:ip">.</nts>
                  <nts id="Seg_2515" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T632" id="Seg_2517" n="HIAT:u" s="T628">
                  <ts e="T629" id="Seg_2519" n="HIAT:w" s="T628">A</ts>
                  <nts id="Seg_2520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2522" n="HIAT:w" s="T629">kăde</ts>
                  <nts id="Seg_2523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2524" n="HIAT:ip">(</nts>
                  <ts e="T631" id="Seg_2526" n="HIAT:w" s="T630">tăn</ts>
                  <nts id="Seg_2527" n="HIAT:ip">)</nts>
                  <nts id="Seg_2528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2530" n="HIAT:w" s="T631">dʼildlil</ts>
                  <nts id="Seg_2531" n="HIAT:ip">?</nts>
                  <nts id="Seg_2532" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T642" id="Seg_2534" n="HIAT:u" s="T632">
                  <ts e="T633" id="Seg_2536" n="HIAT:w" s="T632">A</ts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2538" n="HIAT:ip">(</nts>
                  <ts e="T634" id="Seg_2540" n="HIAT:w" s="T633">dăre</ts>
                  <nts id="Seg_2541" n="HIAT:ip">)</nts>
                  <nts id="Seg_2542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2544" n="HIAT:w" s="T634">bar</ts>
                  <nts id="Seg_2545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2546" n="HIAT:ip">(</nts>
                  <nts id="Seg_2547" n="HIAT:ip">(</nts>
                  <ats e="T866" id="Seg_2548" n="HIAT:non-pho" s="T635">…</ats>
                  <nts id="Seg_2549" n="HIAT:ip">)</nts>
                  <nts id="Seg_2550" n="HIAT:ip">)</nts>
                  <nts id="Seg_2551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2552" n="HIAT:ip">(</nts>
                  <ts e="T636" id="Seg_2554" n="HIAT:w" s="T866">barəʔpi</ts>
                  <nts id="Seg_2555" n="HIAT:ip">)</nts>
                  <nts id="Seg_2556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2557" n="HIAT:ip">(</nts>
                  <ts e="T637" id="Seg_2559" n="HIAT:w" s="T636">oršaʔi</ts>
                  <nts id="Seg_2560" n="HIAT:ip">)</nts>
                  <nts id="Seg_2561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2563" n="HIAT:w" s="T637">i</ts>
                  <nts id="Seg_2564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2566" n="HIAT:w" s="T638">dĭgəttə</ts>
                  <nts id="Seg_2567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2569" n="HIAT:w" s="T639">ine</ts>
                  <nts id="Seg_2570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2571" n="HIAT:ip">(</nts>
                  <ts e="T642" id="Seg_2573" n="HIAT:w" s="T640">dʼi</ts>
                  <nts id="Seg_2574" n="HIAT:ip">)</nts>
                  <nts id="Seg_2575" n="HIAT:ip">.</nts>
                  <nts id="Seg_2576" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T646" id="Seg_2578" n="HIAT:u" s="T642">
                  <ts e="T643" id="Seg_2580" n="HIAT:w" s="T642">Dĭgəttə</ts>
                  <nts id="Seg_2581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2582" n="HIAT:ip">(</nts>
                  <ts e="T644" id="Seg_2584" n="HIAT:w" s="T643">maʔ</ts>
                  <nts id="Seg_2585" n="HIAT:ip">)</nts>
                  <nts id="Seg_2586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2588" n="HIAT:w" s="T644">небось</ts>
                  <nts id="Seg_2589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2591" n="HIAT:w" s="T645">šobibaʔ</ts>
                  <nts id="Seg_2592" n="HIAT:ip">.</nts>
                  <nts id="Seg_2593" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T648" id="Seg_2595" n="HIAT:u" s="T646">
                  <ts e="T647" id="Seg_2597" n="HIAT:w" s="T646">Mĭmbibeʔ</ts>
                  <nts id="Seg_2598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2600" n="HIAT:w" s="T647">noʔ</ts>
                  <nts id="Seg_2601" n="HIAT:ip">.</nts>
                  <nts id="Seg_2602" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T651" id="Seg_2604" n="HIAT:u" s="T648">
                  <ts e="T649" id="Seg_2606" n="HIAT:w" s="T648">Dĭgəttə</ts>
                  <nts id="Seg_2607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2609" n="HIAT:w" s="T649">inem</ts>
                  <nts id="Seg_2610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2611" n="HIAT:ip">(</nts>
                  <ts e="T651" id="Seg_2613" n="HIAT:w" s="T650">kobmbibaʔ</ts>
                  <nts id="Seg_2614" n="HIAT:ip">)</nts>
                  <nts id="Seg_2615" n="HIAT:ip">.</nts>
                  <nts id="Seg_2616" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T654" id="Seg_2618" n="HIAT:u" s="T651">
                  <ts e="T652" id="Seg_2620" n="HIAT:w" s="T651">Măn</ts>
                  <nts id="Seg_2621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2623" n="HIAT:w" s="T652">ugandə</ts>
                  <nts id="Seg_2624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2626" n="HIAT:w" s="T653">kămbiam</ts>
                  <nts id="Seg_2627" n="HIAT:ip">.</nts>
                  <nts id="Seg_2628" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T657" id="Seg_2630" n="HIAT:u" s="T654">
                  <ts e="T655" id="Seg_2632" n="HIAT:w" s="T654">Dĭgəttə</ts>
                  <nts id="Seg_2633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2635" n="HIAT:w" s="T655">kašelʼ</ts>
                  <nts id="Seg_2636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2638" n="HIAT:w" s="T656">šobi</ts>
                  <nts id="Seg_2639" n="HIAT:ip">.</nts>
                  <nts id="Seg_2640" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T663" id="Seg_2642" n="HIAT:u" s="T657">
                  <ts e="T658" id="Seg_2644" n="HIAT:w" s="T657">Dĭgəttə</ts>
                  <nts id="Seg_2645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2647" n="HIAT:w" s="T658">măn</ts>
                  <nts id="Seg_2648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2650" n="HIAT:w" s="T659">sĭreʔpne</ts>
                  <nts id="Seg_2651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2653" n="HIAT:w" s="T660">bar</ts>
                  <nts id="Seg_2654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2656" n="HIAT:w" s="T661">nendəbiem</ts>
                  <nts id="Seg_2657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2659" n="HIAT:w" s="T662">aranə</ts>
                  <nts id="Seg_2660" n="HIAT:ip">.</nts>
                  <nts id="Seg_2661" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T669" id="Seg_2663" n="HIAT:u" s="T663">
                  <ts e="T664" id="Seg_2665" n="HIAT:w" s="T663">Dĭgəttə</ts>
                  <nts id="Seg_2666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2668" n="HIAT:w" s="T664">bĭʔpiem</ts>
                  <nts id="Seg_2669" n="HIAT:ip">,</nts>
                  <nts id="Seg_2670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2672" n="HIAT:w" s="T665">i</ts>
                  <nts id="Seg_2673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2675" n="HIAT:w" s="T666">gibərdə</ts>
                  <nts id="Seg_2676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2678" n="HIAT:w" s="T667">kalaj</ts>
                  <nts id="Seg_2679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2681" n="HIAT:w" s="T668">kašelʼbə</ts>
                  <nts id="Seg_2682" n="HIAT:ip">.</nts>
                  <nts id="Seg_2683" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T672" id="Seg_2685" n="HIAT:u" s="T669">
                  <ts e="T669.tx.1" id="Seg_2687" n="HIAT:w" s="T669">Не</ts>
                  <nts id="Seg_2688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669.tx.2" id="Seg_2690" n="HIAT:w" s="T669.tx.1">стало</ts>
                  <nts id="Seg_2691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2693" n="HIAT:w" s="T669.tx.2">кашля</ts>
                  <nts id="Seg_2694" n="HIAT:ip">.</nts>
                  <nts id="Seg_2695" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T678" id="Seg_2697" n="HIAT:u" s="T672">
                  <ts e="T673" id="Seg_2699" n="HIAT:w" s="T672">Girgit</ts>
                  <nts id="Seg_2700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2701" n="HIAT:ip">(</nts>
                  <ts e="T674" id="Seg_2703" n="HIAT:w" s="T673">noʔ</ts>
                  <nts id="Seg_2704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2706" n="HIAT:w" s="T674">i-</ts>
                  <nts id="Seg_2707" n="HIAT:ip">)</nts>
                  <nts id="Seg_2708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2710" n="HIAT:w" s="T675">noʔ</ts>
                  <nts id="Seg_2711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2713" n="HIAT:w" s="T676">ige</ts>
                  <nts id="Seg_2714" n="HIAT:ip">,</nts>
                  <nts id="Seg_2715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2717" n="HIAT:w" s="T677">крапива</ts>
                  <nts id="Seg_2718" n="HIAT:ip">.</nts>
                  <nts id="Seg_2719" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T684" id="Seg_2721" n="HIAT:u" s="T678">
                  <ts e="T679" id="Seg_2723" n="HIAT:w" s="T678">Dĭ</ts>
                  <nts id="Seg_2724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2726" n="HIAT:w" s="T679">dudkanə</ts>
                  <nts id="Seg_2727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2729" n="HIAT:w" s="T680">izittə</ts>
                  <nts id="Seg_2730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2732" n="HIAT:w" s="T681">nada</ts>
                  <nts id="Seg_2733" n="HIAT:ip">,</nts>
                  <nts id="Seg_2734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2736" n="HIAT:w" s="T682">nʼiʔsittə</ts>
                  <nts id="Seg_2737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2739" n="HIAT:w" s="T683">dĭm</ts>
                  <nts id="Seg_2740" n="HIAT:ip">.</nts>
                  <nts id="Seg_2741" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T691" id="Seg_2743" n="HIAT:u" s="T684">
                  <nts id="Seg_2744" n="HIAT:ip">(</nts>
                  <ts e="T685" id="Seg_2746" n="HIAT:w" s="T684">Əbərdə</ts>
                  <nts id="Seg_2747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2749" n="HIAT:w" s="T685">bər</ts>
                  <nts id="Seg_2750" n="HIAT:ip">)</nts>
                  <nts id="Seg_2751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2753" n="HIAT:w" s="T686">amnosʼtə</ts>
                  <nts id="Seg_2754" n="HIAT:ip">,</nts>
                  <nts id="Seg_2755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2756" n="HIAT:ip">(</nts>
                  <ts e="T688" id="Seg_2758" n="HIAT:w" s="T687">dĭgəttə</ts>
                  <nts id="Seg_2759" n="HIAT:ip">)</nts>
                  <nts id="Seg_2760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2762" n="HIAT:w" s="T688">ej</ts>
                  <nts id="Seg_2763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2765" n="HIAT:w" s="T689">moləj</ts>
                  <nts id="Seg_2766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2768" n="HIAT:w" s="T690">kašelʼ</ts>
                  <nts id="Seg_2769" n="HIAT:ip">.</nts>
                  <nts id="Seg_2770" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T698" id="Seg_2772" n="HIAT:u" s="T691">
                  <ts e="T692" id="Seg_2774" n="HIAT:w" s="T691">Nendluʔpi</ts>
                  <nts id="Seg_2775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2777" n="HIAT:w" s="T692">i</ts>
                  <nts id="Seg_2778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2779" n="HIAT:ip">(</nts>
                  <ts e="T694" id="Seg_2781" n="HIAT:w" s="T693">bər</ts>
                  <nts id="Seg_2782" n="HIAT:ip">)</nts>
                  <nts id="Seg_2783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2785" n="HIAT:w" s="T694">naga</ts>
                  <nts id="Seg_2786" n="HIAT:ip">,</nts>
                  <nts id="Seg_2787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2789" n="HIAT:w" s="T695">i</ts>
                  <nts id="Seg_2790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2792" n="HIAT:w" s="T696">ejü</ts>
                  <nts id="Seg_2793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2795" n="HIAT:w" s="T697">naga</ts>
                  <nts id="Seg_2796" n="HIAT:ip">.</nts>
                  <nts id="Seg_2797" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T703" id="Seg_2799" n="HIAT:u" s="T698">
                  <ts e="T699" id="Seg_2801" n="HIAT:w" s="T698">Paʔi</ts>
                  <nts id="Seg_2802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2804" n="HIAT:w" s="T699">măna</ts>
                  <nts id="Seg_2805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2807" n="HIAT:w" s="T700">teinen</ts>
                  <nts id="Seg_2808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2810" n="HIAT:w" s="T701">ej</ts>
                  <nts id="Seg_2811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2813" n="HIAT:w" s="T702">toʔnarzittə</ts>
                  <nts id="Seg_2814" n="HIAT:ip">.</nts>
                  <nts id="Seg_2815" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T707" id="Seg_2817" n="HIAT:u" s="T703">
                  <ts e="T704" id="Seg_2819" n="HIAT:w" s="T703">Kabarləj</ts>
                  <nts id="Seg_2820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2821" n="HIAT:ip">(</nts>
                  <ts e="T705" id="Seg_2823" n="HIAT:w" s="T704">mə-</ts>
                  <nts id="Seg_2824" n="HIAT:ip">)</nts>
                  <nts id="Seg_2825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2827" n="HIAT:w" s="T705">dö</ts>
                  <nts id="Seg_2828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2830" n="HIAT:w" s="T706">nüdʼinə</ts>
                  <nts id="Seg_2831" n="HIAT:ip">.</nts>
                  <nts id="Seg_2832" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T710" id="Seg_2834" n="HIAT:u" s="T707">
                  <ts e="T708" id="Seg_2836" n="HIAT:w" s="T707">Paʔi</ts>
                  <nts id="Seg_2837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2839" n="HIAT:w" s="T708">măn</ts>
                  <nts id="Seg_2840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2842" n="HIAT:w" s="T709">iʔgö</ts>
                  <nts id="Seg_2843" n="HIAT:ip">.</nts>
                  <nts id="Seg_2844" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T714" id="Seg_2846" n="HIAT:u" s="T710">
                  <nts id="Seg_2847" n="HIAT:ip">(</nts>
                  <ts e="T711" id="Seg_2849" n="HIAT:w" s="T710">Dö</ts>
                  <nts id="Seg_2850" n="HIAT:ip">)</nts>
                  <nts id="Seg_2851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2853" n="HIAT:w" s="T711">pʼenə</ts>
                  <nts id="Seg_2854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2856" n="HIAT:w" s="T712">iššo</ts>
                  <nts id="Seg_2857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2859" n="HIAT:w" s="T713">kabarləj</ts>
                  <nts id="Seg_2860" n="HIAT:ip">.</nts>
                  <nts id="Seg_2861" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T716" id="Seg_2863" n="HIAT:u" s="T714">
                  <ts e="T715" id="Seg_2865" n="HIAT:w" s="T714">Šĭšəge</ts>
                  <nts id="Seg_2866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2868" n="HIAT:w" s="T715">ugandə</ts>
                  <nts id="Seg_2869" n="HIAT:ip">.</nts>
                  <nts id="Seg_2870" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T719" id="Seg_2872" n="HIAT:u" s="T716">
                  <ts e="T717" id="Seg_2874" n="HIAT:w" s="T716">Iʔgö</ts>
                  <nts id="Seg_2875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2877" n="HIAT:w" s="T717">pa</ts>
                  <nts id="Seg_2878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2880" n="HIAT:w" s="T718">kandəgaʔi</ts>
                  <nts id="Seg_2881" n="HIAT:ip">.</nts>
                  <nts id="Seg_2882" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T724" id="Seg_2884" n="HIAT:u" s="T719">
                  <ts e="T720" id="Seg_2886" n="HIAT:w" s="T719">Paʔi</ts>
                  <nts id="Seg_2887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2889" n="HIAT:w" s="T720">bar</ts>
                  <nts id="Seg_2890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2892" n="HIAT:w" s="T721">surnogə</ts>
                  <nts id="Seg_2893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2895" n="HIAT:w" s="T722">sagər</ts>
                  <nts id="Seg_2896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2898" n="HIAT:w" s="T723">mobiʔi</ts>
                  <nts id="Seg_2899" n="HIAT:ip">.</nts>
                  <nts id="Seg_2900" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T727" id="Seg_2902" n="HIAT:u" s="T724">
                  <nts id="Seg_2903" n="HIAT:ip">(</nts>
                  <ts e="T724.tx.1" id="Seg_2905" n="HIAT:w" s="T724">A</ts>
                  <nts id="Seg_2906" n="HIAT:ip">_</nts>
                  <ts e="T725" id="Seg_2908" n="HIAT:w" s="T724.tx.1">to</ts>
                  <nts id="Seg_2909" n="HIAT:ip">)</nts>
                  <nts id="Seg_2910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2912" n="HIAT:w" s="T725">sĭre</ts>
                  <nts id="Seg_2913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2915" n="HIAT:w" s="T726">ibiʔi</ts>
                  <nts id="Seg_2916" n="HIAT:ip">.</nts>
                  <nts id="Seg_2917" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T731" id="Seg_2919" n="HIAT:u" s="T727">
                  <nts id="Seg_2920" n="HIAT:ip">(</nts>
                  <ts e="T728" id="Seg_2922" n="HIAT:w" s="T727">Măn</ts>
                  <nts id="Seg_2923" n="HIAT:ip">)</nts>
                  <nts id="Seg_2924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2925" n="HIAT:ip">(</nts>
                  <ts e="T729" id="Seg_2927" n="HIAT:w" s="T728">büttona</ts>
                  <nts id="Seg_2928" n="HIAT:ip">)</nts>
                  <nts id="Seg_2929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2931" n="HIAT:w" s="T729">šomi</ts>
                  <nts id="Seg_2932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2934" n="HIAT:w" s="T730">paʔi</ts>
                  <nts id="Seg_2935" n="HIAT:ip">.</nts>
                  <nts id="Seg_2936" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T738" id="Seg_2938" n="HIAT:u" s="T731">
                  <ts e="T732" id="Seg_2940" n="HIAT:w" s="T731">Sĭre</ts>
                  <nts id="Seg_2941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2943" n="HIAT:w" s="T732">paʔi</ts>
                  <nts id="Seg_2944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2946" n="HIAT:w" s="T733">bar</ts>
                  <nts id="Seg_2947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2948" n="HIAT:ip">(</nts>
                  <ts e="T735" id="Seg_2950" n="HIAT:w" s="T734">bătek</ts>
                  <nts id="Seg_2951" n="HIAT:ip">)</nts>
                  <nts id="Seg_2952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2954" n="HIAT:w" s="T735">dĭzeŋ</ts>
                  <nts id="Seg_2955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2957" n="HIAT:w" s="T736">ugandə</ts>
                  <nts id="Seg_2958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2960" n="HIAT:w" s="T737">jakše</ts>
                  <nts id="Seg_2961" n="HIAT:ip">.</nts>
                  <nts id="Seg_2962" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T744" id="Seg_2964" n="HIAT:u" s="T738">
                  <ts e="T739" id="Seg_2966" n="HIAT:w" s="T738">Tăŋ</ts>
                  <nts id="Seg_2967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2969" n="HIAT:w" s="T739">nendleʔbəʔjə</ts>
                  <nts id="Seg_2970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2972" n="HIAT:w" s="T740">i</ts>
                  <nts id="Seg_2973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2975" n="HIAT:w" s="T741">ejü</ts>
                  <nts id="Seg_2976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2978" n="HIAT:w" s="T742">dĭzeŋ</ts>
                  <nts id="Seg_2979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2980" n="HIAT:ip">(</nts>
                  <ts e="T744" id="Seg_2982" n="HIAT:w" s="T743">dĭmbi</ts>
                  <nts id="Seg_2983" n="HIAT:ip">)</nts>
                  <nts id="Seg_2984" n="HIAT:ip">.</nts>
                  <nts id="Seg_2985" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T752" id="Seg_2987" n="HIAT:u" s="T744">
                  <ts e="T745" id="Seg_2989" n="HIAT:w" s="T744">Nuldliel</ts>
                  <nts id="Seg_2990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2992" n="HIAT:w" s="T745">mĭndərzittə</ts>
                  <nts id="Seg_2993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2994" n="HIAT:ip">(</nts>
                  <ts e="T747" id="Seg_2996" n="HIAT:w" s="T746">m</ts>
                  <nts id="Seg_2997" n="HIAT:ip">)</nts>
                  <nts id="Seg_2998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_3000" n="HIAT:w" s="T747">kös</ts>
                  <nts id="Seg_3001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3002" n="HIAT:ip">(</nts>
                  <ts e="T749" id="Seg_3004" n="HIAT:w" s="T748">ej=</ts>
                  <nts id="Seg_3005" n="HIAT:ip">)</nts>
                  <nts id="Seg_3006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_3008" n="HIAT:w" s="T749">ej</ts>
                  <nts id="Seg_3009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_3011" n="HIAT:w" s="T750">păʔlia</ts>
                  <nts id="Seg_3012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_3014" n="HIAT:w" s="T751">dibər</ts>
                  <nts id="Seg_3015" n="HIAT:ip">.</nts>
                  <nts id="Seg_3016" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T757" id="Seg_3018" n="HIAT:u" s="T752">
                  <ts e="T753" id="Seg_3020" n="HIAT:w" s="T752">Ipadək</ts>
                  <nts id="Seg_3021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_3023" n="HIAT:w" s="T753">ugandə</ts>
                  <nts id="Seg_3024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_3026" n="HIAT:w" s="T754">kös</ts>
                  <nts id="Seg_3027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_3029" n="HIAT:w" s="T755">iʔgö</ts>
                  <nts id="Seg_3030" n="HIAT:ip">,</nts>
                  <nts id="Seg_3031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3032" n="HIAT:ip">(</nts>
                  <ts e="T757" id="Seg_3034" n="HIAT:w" s="T756">barəʔlia</ts>
                  <nts id="Seg_3035" n="HIAT:ip">)</nts>
                  <nts id="Seg_3036" n="HIAT:ip">.</nts>
                  <nts id="Seg_3037" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T761" id="Seg_3039" n="HIAT:u" s="T757">
                  <ts e="T758" id="Seg_3041" n="HIAT:w" s="T757">Bobti</ts>
                  <nts id="Seg_3042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_3044" n="HIAT:w" s="T758">măna</ts>
                  <nts id="Seg_3045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_3047" n="HIAT:w" s="T759">Vasʼka</ts>
                  <nts id="Seg_3048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_3050" n="HIAT:w" s="T760">Šajbin</ts>
                  <nts id="Seg_3051" n="HIAT:ip">.</nts>
                  <nts id="Seg_3052" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T764" id="Seg_3054" n="HIAT:u" s="T761">
                  <ts e="T762" id="Seg_3056" n="HIAT:w" s="T761">Bobti</ts>
                  <nts id="Seg_3057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_3059" n="HIAT:w" s="T762">tože</ts>
                  <nts id="Seg_3060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_3062" n="HIAT:w" s="T763">tɨ</ts>
                  <nts id="Seg_3063" n="HIAT:ip">.</nts>
                  <nts id="Seg_3064" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T771" id="Seg_3066" n="HIAT:u" s="T764">
                  <ts e="T765" id="Seg_3068" n="HIAT:w" s="T764">I</ts>
                  <nts id="Seg_3069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3070" n="HIAT:ip">(</nts>
                  <ts e="T766" id="Seg_3072" n="HIAT:w" s="T765">tănarbi</ts>
                  <nts id="Seg_3073" n="HIAT:ip">)</nts>
                  <nts id="Seg_3074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_3076" n="HIAT:w" s="T766">dĭ</ts>
                  <nts id="Seg_3077" n="HIAT:ip">,</nts>
                  <nts id="Seg_3078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_3080" n="HIAT:w" s="T767">a</ts>
                  <nts id="Seg_3081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_3083" n="HIAT:w" s="T768">măn</ts>
                  <nts id="Seg_3084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_3086" n="HIAT:w" s="T769">bospə</ts>
                  <nts id="Seg_3087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_3089" n="HIAT:w" s="T770">embiem</ts>
                  <nts id="Seg_3090" n="HIAT:ip">.</nts>
                  <nts id="Seg_3091" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T773" id="Seg_3093" n="HIAT:u" s="T771">
                  <ts e="T772" id="Seg_3095" n="HIAT:w" s="T771">Traktărzi</ts>
                  <nts id="Seg_3096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_3098" n="HIAT:w" s="T772">deʔpiʔi</ts>
                  <nts id="Seg_3099" n="HIAT:ip">.</nts>
                  <nts id="Seg_3100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T776" id="Seg_3102" n="HIAT:u" s="T773">
                  <ts e="T774" id="Seg_3104" n="HIAT:w" s="T773">Kajak</ts>
                  <nts id="Seg_3105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3106" n="HIAT:ip">(</nts>
                  <ts e="T775" id="Seg_3108" n="HIAT:w" s="T774">ibiem</ts>
                  <nts id="Seg_3109" n="HIAT:ip">)</nts>
                  <nts id="Seg_3110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_3112" n="HIAT:w" s="T775">traktărdə</ts>
                  <nts id="Seg_3113" n="HIAT:ip">.</nts>
                  <nts id="Seg_3114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T785" id="Seg_3116" n="HIAT:u" s="T776">
                  <ts e="T777" id="Seg_3118" n="HIAT:w" s="T776">Tăŋ</ts>
                  <nts id="Seg_3119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3120" n="HIAT:ip">(</nts>
                  <nts id="Seg_3121" n="HIAT:ip">(</nts>
                  <ats e="T778" id="Seg_3122" n="HIAT:non-pho" s="T777">…</ats>
                  <nts id="Seg_3123" n="HIAT:ip">)</nts>
                  <nts id="Seg_3124" n="HIAT:ip">)</nts>
                  <nts id="Seg_3125" n="HIAT:ip">,</nts>
                  <nts id="Seg_3126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_3128" n="HIAT:w" s="T778">a</ts>
                  <nts id="Seg_3129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_3131" n="HIAT:w" s="T779">măn</ts>
                  <nts id="Seg_3132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_3134" n="HIAT:w" s="T780">bar</ts>
                  <nts id="Seg_3135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_3137" n="HIAT:w" s="T781">pa</ts>
                  <nts id="Seg_3138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_3140" n="HIAT:w" s="T782">tažerbiam</ts>
                  <nts id="Seg_3141" n="HIAT:ip">,</nts>
                  <nts id="Seg_3142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_3144" n="HIAT:w" s="T783">büjle</ts>
                  <nts id="Seg_3145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_3147" n="HIAT:w" s="T784">mĭmbiem</ts>
                  <nts id="Seg_3148" n="HIAT:ip">.</nts>
                  <nts id="Seg_3149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T788" id="Seg_3151" n="HIAT:u" s="T785">
                  <ts e="T786" id="Seg_3153" n="HIAT:w" s="T785">Tüžöjdə</ts>
                  <nts id="Seg_3154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_3156" n="HIAT:w" s="T786">nuʔ</ts>
                  <nts id="Seg_3157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_3159" n="HIAT:w" s="T787">mĭbiem</ts>
                  <nts id="Seg_3160" n="HIAT:ip">.</nts>
                  <nts id="Seg_3161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T792" id="Seg_3163" n="HIAT:u" s="T788">
                  <ts e="T789" id="Seg_3165" n="HIAT:w" s="T788">Surdəbiam</ts>
                  <nts id="Seg_3166" n="HIAT:ip">,</nts>
                  <nts id="Seg_3167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_3169" n="HIAT:w" s="T789">dĭgəttə</ts>
                  <nts id="Seg_3170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_3172" n="HIAT:w" s="T790">amnobiam</ts>
                  <nts id="Seg_3173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_3175" n="HIAT:w" s="T791">amorzittə</ts>
                  <nts id="Seg_3176" n="HIAT:ip">.</nts>
                  <nts id="Seg_3177" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T794" id="Seg_3179" n="HIAT:u" s="T792">
                  <ts e="T793" id="Seg_3181" n="HIAT:w" s="T792">I</ts>
                  <nts id="Seg_3182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_3184" n="HIAT:w" s="T793">tenölaʔbəm</ts>
                  <nts id="Seg_3185" n="HIAT:ip">.</nts>
                  <nts id="Seg_3186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T799" id="Seg_3188" n="HIAT:u" s="T794">
                  <ts e="T795" id="Seg_3190" n="HIAT:w" s="T794">Dĭ</ts>
                  <nts id="Seg_3191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_3193" n="HIAT:w" s="T795">Jelʼa</ts>
                  <nts id="Seg_3194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_3196" n="HIAT:w" s="T796">măna</ts>
                  <nts id="Seg_3197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_3199" n="HIAT:w" s="T797">bar</ts>
                  <nts id="Seg_3200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_3202" n="HIAT:w" s="T798">šaːmbi</ts>
                  <nts id="Seg_3203" n="HIAT:ip">.</nts>
                  <nts id="Seg_3204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T804" id="Seg_3206" n="HIAT:u" s="T799">
                  <ts e="T800" id="Seg_3208" n="HIAT:w" s="T799">Aktʼat</ts>
                  <nts id="Seg_3209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_3211" n="HIAT:w" s="T800">nagur</ts>
                  <nts id="Seg_3212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_3214" n="HIAT:w" s="T801">šălkovej</ts>
                  <nts id="Seg_3215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3216" n="HIAT:ip">(</nts>
                  <ts e="T803" id="Seg_3218" n="HIAT:w" s="T802">b-</ts>
                  <nts id="Seg_3219" n="HIAT:ip">)</nts>
                  <nts id="Seg_3220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_3222" n="HIAT:w" s="T803">ibi</ts>
                  <nts id="Seg_3223" n="HIAT:ip">.</nts>
                  <nts id="Seg_3224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T812" id="Seg_3226" n="HIAT:u" s="T804">
                  <ts e="T805" id="Seg_3228" n="HIAT:w" s="T804">A</ts>
                  <nts id="Seg_3229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_3231" n="HIAT:w" s="T805">măna</ts>
                  <nts id="Seg_3232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_3234" n="HIAT:w" s="T806">ibi</ts>
                  <nts id="Seg_3235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_3237" n="HIAT:w" s="T807">onʼiʔ</ts>
                  <nts id="Seg_3238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_3240" n="HIAT:w" s="T808">šălkovej</ts>
                  <nts id="Seg_3241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_3243" n="HIAT:w" s="T809">šide</ts>
                  <nts id="Seg_3244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_3246" n="HIAT:w" s="T810">bʼeʔ</ts>
                  <nts id="Seg_3247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_3249" n="HIAT:w" s="T811">sumna</ts>
                  <nts id="Seg_3250" n="HIAT:ip">.</nts>
                  <nts id="Seg_3251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T822" id="Seg_3253" n="HIAT:u" s="T812">
                  <ts e="T813" id="Seg_3255" n="HIAT:w" s="T812">Dĭʔnə</ts>
                  <nts id="Seg_3256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3257" n="HIAT:ip">(</nts>
                  <ts e="T814" id="Seg_3259" n="HIAT:w" s="T813">bɨl</ts>
                  <nts id="Seg_3260" n="HIAT:ip">)</nts>
                  <nts id="Seg_3261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_3263" n="HIAT:w" s="T814">izittə</ts>
                  <nts id="Seg_3264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_3266" n="HIAT:w" s="T815">măna</ts>
                  <nts id="Seg_3267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_3269" n="HIAT:w" s="T816">onʼiʔ</ts>
                  <nts id="Seg_3270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_3272" n="HIAT:w" s="T817">šălkovej</ts>
                  <nts id="Seg_3273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_3275" n="HIAT:w" s="T818">i</ts>
                  <nts id="Seg_3276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_3278" n="HIAT:w" s="T819">bʼeʔ</ts>
                  <nts id="Seg_3279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_3281" n="HIAT:w" s="T820">sumna</ts>
                  <nts id="Seg_3282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_3284" n="HIAT:w" s="T821">kăpʼejkaʔi</ts>
                  <nts id="Seg_3285" n="HIAT:ip">.</nts>
                  <nts id="Seg_3286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T830" id="Seg_3288" n="HIAT:u" s="T822">
                  <ts e="T823" id="Seg_3290" n="HIAT:w" s="T822">Măn</ts>
                  <nts id="Seg_3291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_3293" n="HIAT:w" s="T823">teinen</ts>
                  <nts id="Seg_3294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3296" n="HIAT:w" s="T824">bünə</ts>
                  <nts id="Seg_3297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_3299" n="HIAT:w" s="T825">kambiam</ts>
                  <nts id="Seg_3300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_3302" n="HIAT:w" s="T826">da</ts>
                  <nts id="Seg_3303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_3305" n="HIAT:w" s="T827">ugandə</ts>
                  <nts id="Seg_3306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_3308" n="HIAT:w" s="T828">tăŋ</ts>
                  <nts id="Seg_3309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3310" n="HIAT:ip">(</nts>
                  <ts e="T830" id="Seg_3312" n="HIAT:w" s="T829">kănnambi</ts>
                  <nts id="Seg_3313" n="HIAT:ip">)</nts>
                  <nts id="Seg_3314" n="HIAT:ip">.</nts>
                  <nts id="Seg_3315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T839" id="Seg_3317" n="HIAT:u" s="T830">
                  <ts e="T831" id="Seg_3319" n="HIAT:w" s="T830">Măn</ts>
                  <nts id="Seg_3320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3322" n="HIAT:w" s="T831">parbiam</ts>
                  <nts id="Seg_3323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3324" n="HIAT:ip">(</nts>
                  <ts e="T833" id="Seg_3326" n="HIAT:w" s="T832">baltu</ts>
                  <nts id="Seg_3327" n="HIAT:ip">)</nts>
                  <nts id="Seg_3328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_3330" n="HIAT:w" s="T833">ibiem</ts>
                  <nts id="Seg_3331" n="HIAT:ip">,</nts>
                  <nts id="Seg_3332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_3334" n="HIAT:w" s="T834">baltuzi</ts>
                  <nts id="Seg_3335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_3337" n="HIAT:w" s="T835">jaʔpiam</ts>
                  <nts id="Seg_3338" n="HIAT:ip">,</nts>
                  <nts id="Seg_3339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_3341" n="HIAT:w" s="T836">jaʔpiam</ts>
                  <nts id="Seg_3342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_3344" n="HIAT:w" s="T837">bar</ts>
                  <nts id="Seg_3345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_3347" n="HIAT:w" s="T838">sĭre</ts>
                  <nts id="Seg_3348" n="HIAT:ip">.</nts>
                  <nts id="Seg_3349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T846" id="Seg_3351" n="HIAT:u" s="T839">
                  <ts e="T840" id="Seg_3353" n="HIAT:w" s="T839">Целый</ts>
                  <nts id="Seg_3354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_3356" n="HIAT:w" s="T840">вершок</ts>
                  <nts id="Seg_3357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_3359" n="HIAT:w" s="T841">kănnambi</ts>
                  <nts id="Seg_3360" n="HIAT:ip">,</nts>
                  <nts id="Seg_3361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_3363" n="HIAT:w" s="T842">ugandə</ts>
                  <nts id="Seg_3364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_3366" n="HIAT:w" s="T843">šĭšəge</ts>
                  <nts id="Seg_3367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_3369" n="HIAT:w" s="T844">erte</ts>
                  <nts id="Seg_3370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_3372" n="HIAT:w" s="T845">ibi</ts>
                  <nts id="Seg_3373" n="HIAT:ip">.</nts>
                  <nts id="Seg_3374" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T858" id="Seg_3376" n="HIAT:u" s="T846">
                  <ts e="T847" id="Seg_3378" n="HIAT:w" s="T846">Maʔnʼi</ts>
                  <nts id="Seg_3379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_3381" n="HIAT:w" s="T847">šobiam</ts>
                  <nts id="Seg_3382" n="HIAT:ip">,</nts>
                  <nts id="Seg_3383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3384" n="HIAT:ip">(</nts>
                  <ts e="T849" id="Seg_3386" n="HIAT:w" s="T848">šiʔnʼileʔ</ts>
                  <nts id="Seg_3387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_3389" n="HIAT:w" s="T849">edəʔpiem</ts>
                  <nts id="Seg_3390" n="HIAT:ip">)</nts>
                  <nts id="Seg_3391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_3393" n="HIAT:w" s="T850">i</ts>
                  <nts id="Seg_3394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_3396" n="HIAT:w" s="T851">bü</ts>
                  <nts id="Seg_3397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_3399" n="HIAT:w" s="T852">deʔpiem</ts>
                  <nts id="Seg_3400" n="HIAT:ip">,</nts>
                  <nts id="Seg_3401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_3403" n="HIAT:w" s="T853">pʼeštə</ts>
                  <nts id="Seg_3404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3405" n="HIAT:ip">(</nts>
                  <ts e="T855" id="Seg_3407" n="HIAT:w" s="T854">še-</ts>
                  <nts id="Seg_3408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_3410" n="HIAT:w" s="T855">s-</ts>
                  <nts id="Seg_3411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_3413" n="HIAT:w" s="T856">šo-</ts>
                  <nts id="Seg_3414" n="HIAT:ip">)</nts>
                  <nts id="Seg_3415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_3417" n="HIAT:w" s="T857">sʼabiam</ts>
                  <nts id="Seg_3418" n="HIAT:ip">.</nts>
                  <nts id="Seg_3419" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T861" id="Seg_3421" n="HIAT:u" s="T858">
                  <ts e="T859" id="Seg_3423" n="HIAT:w" s="T858">Amnobiam</ts>
                  <nts id="Seg_3424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_3426" n="HIAT:w" s="T859">i</ts>
                  <nts id="Seg_3427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3428" n="HIAT:ip">(</nts>
                  <ts e="T861" id="Seg_3430" n="HIAT:w" s="T860">emneʔpəm</ts>
                  <nts id="Seg_3431" n="HIAT:ip">)</nts>
                  <nts id="Seg_3432" n="HIAT:ip">.</nts>
                  <nts id="Seg_3433" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T865" id="Seg_3435" n="HIAT:u" s="T861">
                  <ts e="T862" id="Seg_3437" n="HIAT:w" s="T861">Dĭgəttə</ts>
                  <nts id="Seg_3438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_3440" n="HIAT:w" s="T862">šobiʔi</ts>
                  <nts id="Seg_3441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_3443" n="HIAT:w" s="T863">i</ts>
                  <nts id="Seg_3444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3445" n="HIAT:ip">(</nts>
                  <nts id="Seg_3446" n="HIAT:ip">(</nts>
                  <ats e="T865" id="Seg_3447" n="HIAT:non-pho" s="T864">DMG</ats>
                  <nts id="Seg_3448" n="HIAT:ip">)</nts>
                  <nts id="Seg_3449" n="HIAT:ip">)</nts>
                  <nts id="Seg_3450" n="HIAT:ip">.</nts>
                  <nts id="Seg_3451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T865" id="Seg_3452" n="sc" s="T0">
               <ts e="T1" id="Seg_3454" n="e" s="T0">Miʔ </ts>
               <ts e="T2" id="Seg_3456" n="e" s="T1">bar </ts>
               <ts e="T3" id="Seg_3458" n="e" s="T2">(Sverdlovskagən) </ts>
               <ts e="T4" id="Seg_3460" n="e" s="T3">amnobiaʔ. </ts>
               <ts e="T5" id="Seg_3462" n="e" s="T4">(Sujo= </ts>
               <ts e="T6" id="Seg_3464" n="e" s="T5">sujo= </ts>
               <ts e="T7" id="Seg_3466" n="e" s="T6">bazaj=) </ts>
               <ts e="T8" id="Seg_3468" n="e" s="T7">Bazaj </ts>
               <ts e="T9" id="Seg_3470" n="e" s="T8">süjönə </ts>
               <ts e="T10" id="Seg_3472" n="e" s="T9">amnobibaʔ. </ts>
               <ts e="T11" id="Seg_3474" n="e" s="T10">Dĭgəttə </ts>
               <ts e="T12" id="Seg_3476" n="e" s="T11">nʼergöbibaʔ. </ts>
               <ts e="T13" id="Seg_3478" n="e" s="T12">Aktʼigən </ts>
               <ts e="T14" id="Seg_3480" n="e" s="T13">ej </ts>
               <ts e="T15" id="Seg_3482" n="e" s="T14">molia </ts>
               <ts e="T16" id="Seg_3484" n="e" s="T15">(nʼ-) </ts>
               <ts e="T17" id="Seg_3486" n="e" s="T16">nʼergözittə. </ts>
               <ts e="T18" id="Seg_3488" n="e" s="T17">(Berzəbi) </ts>
               <ts e="T19" id="Seg_3490" n="e" s="T18">bar. </ts>
               <ts e="T20" id="Seg_3492" n="e" s="T19">Туман </ts>
               <ts e="T21" id="Seg_3494" n="e" s="T20">(nuʔ-) </ts>
               <ts e="T22" id="Seg_3496" n="e" s="T21">nulaʔbə. </ts>
               <ts e="T23" id="Seg_3498" n="e" s="T22">Dĭgəttə </ts>
               <ts e="T24" id="Seg_3500" n="e" s="T23">šobibaʔ </ts>
               <ts e="T25" id="Seg_3502" n="e" s="T24">Krasnojarskənə. </ts>
               <ts e="T26" id="Seg_3504" n="e" s="T25">Dĭn </ts>
               <ts e="T27" id="Seg_3506" n="e" s="T26">amnəbibaʔ. </ts>
               <ts e="T28" id="Seg_3508" n="e" s="T27">Dĭn </ts>
               <ts e="T29" id="Seg_3510" n="e" s="T28">amnobiam. </ts>
               <ts e="T30" id="Seg_3512" n="e" s="T29">Sejʔpü </ts>
               <ts e="T31" id="Seg_3514" n="e" s="T30">dʼala </ts>
               <ts e="T32" id="Seg_3516" n="e" s="T31">amnobiam. </ts>
               <ts e="T33" id="Seg_3518" n="e" s="T32">Dĭgəttə </ts>
               <ts e="T34" id="Seg_3520" n="e" s="T33">amnobiam </ts>
               <ts e="T35" id="Seg_3522" n="e" s="T34">bazaj </ts>
               <ts e="T36" id="Seg_3524" n="e" s="T35">(aʔtʼinə). </ts>
               <ts e="T37" id="Seg_3526" n="e" s="T36">Šobiam </ts>
               <ts e="T38" id="Seg_3528" n="e" s="T37">Ujardə. </ts>
               <ts e="T39" id="Seg_3530" n="e" s="T38">Dĭgəttə </ts>
               <ts e="T40" id="Seg_3532" n="e" s="T39">Ujargəʔ </ts>
               <ts e="T41" id="Seg_3534" n="e" s="T40">šobiam. </ts>
               <ts e="T42" id="Seg_3536" n="e" s="T41">(Amnobi-) </ts>
               <ts e="T43" id="Seg_3538" n="e" s="T42">Amnobiam </ts>
               <ts e="T44" id="Seg_3540" n="e" s="T43">avtobustə. </ts>
               <ts e="T45" id="Seg_3542" n="e" s="T44">Šobiam </ts>
               <ts e="T46" id="Seg_3544" n="e" s="T45">Săjanskən. </ts>
               <ts e="T47" id="Seg_3546" n="e" s="T46">Dön </ts>
               <ts e="T48" id="Seg_3548" n="e" s="T47">amnobiam </ts>
               <ts e="T49" id="Seg_3550" n="e" s="T48">(четыре) </ts>
               <ts e="T50" id="Seg_3552" n="e" s="T49">(nagu-) </ts>
               <ts e="T51" id="Seg_3554" n="e" s="T50">šide </ts>
               <ts e="T52" id="Seg_3556" n="e" s="T51">dʼala. </ts>
               <ts e="T53" id="Seg_3558" n="e" s="T52">Dĭgəttə </ts>
               <ts e="T54" id="Seg_3560" n="e" s="T53">Abalakoftə </ts>
               <ts e="T55" id="Seg_3562" n="e" s="T54">šobiam </ts>
               <ts e="T56" id="Seg_3564" n="e" s="T55">i </ts>
               <ts e="T57" id="Seg_3566" n="e" s="T56">tănan </ts>
               <ts e="T58" id="Seg_3568" n="e" s="T57">šobiam. </ts>
               <ts e="T59" id="Seg_3570" n="e" s="T58">Tănan </ts>
               <ts e="T60" id="Seg_3572" n="e" s="T59">šobiam, </ts>
               <ts e="T61" id="Seg_3574" n="e" s="T60">dʼăbaktərbibaʔ, </ts>
               <ts e="T62" id="Seg_3576" n="e" s="T61">dĭgəttə </ts>
               <ts e="T63" id="Seg_3578" n="e" s="T62">tăn </ts>
               <ts e="T64" id="Seg_3580" n="e" s="T63">(măn=) </ts>
               <ts e="T65" id="Seg_3582" n="e" s="T64">măna </ts>
               <ts e="T66" id="Seg_3584" n="e" s="T65">(măm-) </ts>
               <ts e="T67" id="Seg_3586" n="e" s="T66">(măllal): </ts>
               <ts e="T68" id="Seg_3588" n="e" s="T67">Amnaʔ, </ts>
               <ts e="T69" id="Seg_3590" n="e" s="T68">amorgaʔ, </ts>
               <ts e="T70" id="Seg_3592" n="e" s="T69">(tu-) </ts>
               <ts e="T71" id="Seg_3594" n="e" s="T70">püjölial </ts>
               <ts e="T72" id="Seg_3596" n="e" s="T71">padʼi_ka. </ts>
               <ts e="T73" id="Seg_3598" n="e" s="T72">Dĭgəttə </ts>
               <ts e="T74" id="Seg_3600" n="e" s="T73">măn </ts>
               <ts e="T75" id="Seg_3602" n="e" s="T74">amnobiam, </ts>
               <ts e="T76" id="Seg_3604" n="e" s="T75">amorbiam, </ts>
               <ts e="T77" id="Seg_3606" n="e" s="T76">kambiam. </ts>
               <ts e="T78" id="Seg_3608" n="e" s="T77">Bostə </ts>
               <ts e="T79" id="Seg_3610" n="e" s="T78">(fatʼeratsi). </ts>
               <ts e="T80" id="Seg_3612" n="e" s="T79">(Onʼiʔ=) </ts>
               <ts e="T81" id="Seg_3614" n="e" s="T80">Onʼiʔ </ts>
               <ts e="T82" id="Seg_3616" n="e" s="T81">raz </ts>
               <ts e="T83" id="Seg_3618" n="e" s="T82">miʔ </ts>
               <ts e="T84" id="Seg_3620" n="e" s="T83">kambibaʔ, </ts>
               <ts e="T85" id="Seg_3622" n="e" s="T84">nagur </ts>
               <ts e="T86" id="Seg_3624" n="e" s="T85">ne, </ts>
               <ts e="T87" id="Seg_3626" n="e" s="T86">Zaozʼorkanə. </ts>
               <ts e="T88" id="Seg_3628" n="e" s="T87">Dĭgəttə </ts>
               <ts e="T89" id="Seg_3630" n="e" s="T88">dĭʔə </ts>
               <ts e="T90" id="Seg_3632" n="e" s="T89">bazaj </ts>
               <ts e="T91" id="Seg_3634" n="e" s="T90">aʔtʼinə </ts>
               <ts e="T92" id="Seg_3636" n="e" s="T91">(am-) </ts>
               <ts e="T93" id="Seg_3638" n="e" s="T92">amnobibaʔ. </ts>
               <ts e="T94" id="Seg_3640" n="e" s="T93">Kambibaʔ </ts>
               <ts e="T95" id="Seg_3642" n="e" s="T94">Ujardə, </ts>
               <ts e="T96" id="Seg_3644" n="e" s="T95">dĭn </ts>
               <ts e="T97" id="Seg_3646" n="e" s="T96">kudajdə </ts>
               <ts e="T98" id="Seg_3648" n="e" s="T97">numan üzəsʼtə. </ts>
               <ts e="T99" id="Seg_3650" n="e" s="T98">Dĭn </ts>
               <ts e="T100" id="Seg_3652" n="e" s="T99">kudajdə </ts>
               <ts e="T101" id="Seg_3654" n="e" s="T100">numan üzəleʔpibeʔ. </ts>
               <ts e="T102" id="Seg_3656" n="e" s="T101">Dĭgəttə </ts>
               <ts e="T103" id="Seg_3658" n="e" s="T102">parluʔpibaʔ </ts>
               <ts e="T104" id="Seg_3660" n="e" s="T103">(bazoʔ) </ts>
               <ts e="T105" id="Seg_3662" n="e" s="T104">(s-) </ts>
               <ts e="T106" id="Seg_3664" n="e" s="T105">döbər </ts>
               <ts e="T107" id="Seg_3666" n="e" s="T106">Zaozʼorkanə. </ts>
               <ts e="T108" id="Seg_3668" n="e" s="T107">(Dĭn) </ts>
               <ts e="T109" id="Seg_3670" n="e" s="T108">Dön </ts>
               <ts e="T110" id="Seg_3672" n="e" s="T109">šabiaʔ, </ts>
               <ts e="T111" id="Seg_3674" n="e" s="T110">iʔgö </ts>
               <ts e="T112" id="Seg_3676" n="e" s="T111">dʼăbaktərlaʔpibeʔ. </ts>
               <ts e="T113" id="Seg_3678" n="e" s="T112">Dĭgəttə </ts>
               <ts e="T114" id="Seg_3680" n="e" s="T113">(kozʼa) </ts>
               <ts e="T115" id="Seg_3682" n="e" s="T114">kambi. </ts>
               <ts e="T116" id="Seg_3684" n="e" s="T115">Avtobus </ts>
               <ts e="T117" id="Seg_3686" n="e" s="T116">măndərzittə </ts>
               <ts e="T118" id="Seg_3688" n="e" s="T117">šobi </ts>
               <ts e="T119" id="Seg_3690" n="e" s="T118">da </ts>
               <ts e="T120" id="Seg_3692" n="e" s="T119">(măndede) </ts>
               <ts e="T868" id="Seg_3694" n="e" s="T120">kalla </ts>
               <ts e="T121" id="Seg_3696" n="e" s="T868">dʼürbi. </ts>
               <ts e="T122" id="Seg_3698" n="e" s="T121">A </ts>
               <ts e="T123" id="Seg_3700" n="e" s="T122">miʔ </ts>
               <ts e="T124" id="Seg_3702" n="e" s="T123">oʔbdəbibaʔ </ts>
               <ts e="T125" id="Seg_3704" n="e" s="T124">da </ts>
               <ts e="T126" id="Seg_3706" n="e" s="T125">kambibaʔ </ts>
               <ts e="T127" id="Seg_3708" n="e" s="T126">nʼiʔdə. </ts>
               <ts e="T128" id="Seg_3710" n="e" s="T127">Dön </ts>
               <ts e="T129" id="Seg_3712" n="e" s="T128">avtobus </ts>
               <ts e="T130" id="Seg_3714" n="e" s="T129">šonəga. </ts>
               <ts e="T131" id="Seg_3716" n="e" s="T130">Amnəldəbi </ts>
               <ts e="T132" id="Seg_3718" n="e" s="T131">miʔ </ts>
               <ts e="T133" id="Seg_3720" n="e" s="T132">(da). </ts>
               <ts e="T134" id="Seg_3722" n="e" s="T133">Perejaslăvkanə </ts>
               <ts e="T135" id="Seg_3724" n="e" s="T134">šobibaʔ. </ts>
               <ts e="T136" id="Seg_3726" n="e" s="T135">Dĭn </ts>
               <ts e="T137" id="Seg_3728" n="e" s="T136">(нас=) </ts>
               <ts e="T139" id="Seg_3730" n="e" s="T137">miʔnʼibeʔ… </ts>
               <ts e="T140" id="Seg_3732" n="e" s="T139">((Dĭn </ts>
               <ts e="T141" id="Seg_3734" n="e" s="T140">miʔ)) </ts>
               <ts e="T142" id="Seg_3736" n="e" s="T141">Dĭn </ts>
               <ts e="T143" id="Seg_3738" n="e" s="T142">üzəbibeʔ </ts>
               <ts e="T144" id="Seg_3740" n="e" s="T143">i </ts>
               <ts e="T145" id="Seg_3742" n="e" s="T144">nulabibaʔ. </ts>
               <ts e="T146" id="Seg_3744" n="e" s="T145">Măn </ts>
               <ts e="T147" id="Seg_3746" n="e" s="T146">măndəm: </ts>
               <ts e="T148" id="Seg_3748" n="e" s="T147">Ĭmbi </ts>
               <ts e="T149" id="Seg_3750" n="e" s="T148">nulaʔsittə, </ts>
               <ts e="T150" id="Seg_3752" n="e" s="T149">kanžəbaʔ, </ts>
               <ts e="T151" id="Seg_3754" n="e" s="T150">üdʼiʔ! </ts>
               <ts e="T152" id="Seg_3756" n="e" s="T151">(Dĭgəttə) </ts>
               <ts e="T153" id="Seg_3758" n="e" s="T152">(kanžəbaʔ=) </ts>
               <ts e="T154" id="Seg_3760" n="e" s="T153">kandlaʔbəbaʔ. </ts>
               <ts e="T155" id="Seg_3762" n="e" s="T154">Mašina </ts>
               <ts e="T156" id="Seg_3764" n="e" s="T155">bar </ts>
               <ts e="T157" id="Seg_3766" n="e" s="T156">šonəga. </ts>
               <ts e="T158" id="Seg_3768" n="e" s="T157">Amnogaʔ! </ts>
               <ts e="T159" id="Seg_3770" n="e" s="T158">(Miʔ= </ts>
               <ts e="T160" id="Seg_3772" n="e" s="T159">amne- </ts>
               <ts e="T161" id="Seg_3774" n="e" s="T160">amno-) </ts>
               <ts e="T162" id="Seg_3776" n="e" s="T161">Amnogaʔ </ts>
               <ts e="T163" id="Seg_3778" n="e" s="T162">döber! </ts>
               <ts e="T164" id="Seg_3780" n="e" s="T163">Dĭgəttə </ts>
               <ts e="T165" id="Seg_3782" n="e" s="T164">miʔ </ts>
               <ts e="T166" id="Seg_3784" n="e" s="T165">amnobibaʔ. </ts>
               <ts e="T167" id="Seg_3786" n="e" s="T166">I </ts>
               <ts e="T168" id="Seg_3788" n="e" s="T167">šobibaʔ </ts>
               <ts e="T169" id="Seg_3790" n="e" s="T168">Kandəganə. </ts>
               <ts e="T170" id="Seg_3792" n="e" s="T169">Dĭgəttə </ts>
               <ts e="T171" id="Seg_3794" n="e" s="T170">dĭn </ts>
               <ts e="T172" id="Seg_3796" n="e" s="T171">iššo </ts>
               <ts e="T173" id="Seg_3798" n="e" s="T172">sejʔpü </ts>
               <ts e="T174" id="Seg_3800" n="e" s="T173">верста </ts>
               <ts e="T175" id="Seg_3802" n="e" s="T174">maluʔpi </ts>
               <ts e="T176" id="Seg_3804" n="e" s="T175">Unʼerdə </ts>
               <ts e="T177" id="Seg_3806" n="e" s="T176">kanzittə. </ts>
               <ts e="T178" id="Seg_3808" n="e" s="T177">Šide </ts>
               <ts e="T179" id="Seg_3810" n="e" s="T178">nezeŋ </ts>
               <ts e="T180" id="Seg_3812" n="e" s="T179">kambiʔi </ts>
               <ts e="T181" id="Seg_3814" n="e" s="T180">turanə, </ts>
               <ts e="T182" id="Seg_3816" n="e" s="T181">a </ts>
               <ts e="T183" id="Seg_3818" n="e" s="T182">măn </ts>
               <ts e="T184" id="Seg_3820" n="e" s="T183">kambiam </ts>
               <ts e="T185" id="Seg_3822" n="e" s="T184">üdʼiʔ. </ts>
               <ts e="T186" id="Seg_3824" n="e" s="T185">Unʼerdə </ts>
               <ts e="T187" id="Seg_3826" n="e" s="T186">šobiam. </ts>
               <ts e="T188" id="Seg_3828" n="e" s="T187">Deʔkeʔ </ts>
               <ts e="T189" id="Seg_3830" n="e" s="T188">măna </ts>
               <ts e="T190" id="Seg_3832" n="e" s="T189">ipek. </ts>
               <ts e="T191" id="Seg_3834" n="e" s="T190">Sadargaʔ </ts>
               <ts e="T192" id="Seg_3836" n="e" s="T191">ipek. </ts>
               <ts e="T193" id="Seg_3838" n="e" s="T192">Dĭzeŋ </ts>
               <ts e="T194" id="Seg_3840" n="e" s="T193">măndə: </ts>
               <ts e="T195" id="Seg_3842" n="e" s="T194">(Ka-) </ts>
               <ts e="T196" id="Seg_3844" n="e" s="T195">Kanaʔ. </ts>
               <ts e="T197" id="Seg_3846" n="e" s="T196">Măgăzində </ts>
               <ts e="T198" id="Seg_3848" n="e" s="T197">(de-) </ts>
               <ts e="T199" id="Seg_3850" n="e" s="T198">dĭn </ts>
               <ts e="T200" id="Seg_3852" n="e" s="T199">iʔ. </ts>
               <ts e="T201" id="Seg_3854" n="e" s="T200">Măna </ts>
               <ts e="T202" id="Seg_3856" n="e" s="T201">iʔgö </ts>
               <ts e="T203" id="Seg_3858" n="e" s="T202">ej </ts>
               <ts e="T204" id="Seg_3860" n="e" s="T203">kereʔ. </ts>
               <ts e="T205" id="Seg_3862" n="e" s="T204">Šide </ts>
               <ts e="T206" id="Seg_3864" n="e" s="T205">gram </ts>
               <ts e="T207" id="Seg_3866" n="e" s="T206">mĭbileʔ. </ts>
               <ts e="T208" id="Seg_3868" n="e" s="T207">Dĭ </ts>
               <ts e="T209" id="Seg_3870" n="e" s="T208">măna </ts>
               <ts e="T210" id="Seg_3872" n="e" s="T209">šide </ts>
               <ts e="T211" id="Seg_3874" n="e" s="T210">gram </ts>
               <ts e="T212" id="Seg_3876" n="e" s="T211">edəbi, </ts>
               <ts e="T213" id="Seg_3878" n="e" s="T212">măn </ts>
               <ts e="T214" id="Seg_3880" n="e" s="T213">ibiem, </ts>
               <ts e="T215" id="Seg_3882" n="e" s="T214">aktʼa </ts>
               <ts e="T216" id="Seg_3884" n="e" s="T215">mĭbiem, </ts>
               <ts e="T217" id="Seg_3886" n="e" s="T216">ambiam </ts>
               <ts e="T218" id="Seg_3888" n="e" s="T217">i </ts>
               <ts e="T219" id="Seg_3890" n="e" s="T218">kambiam, </ts>
               <ts e="T220" id="Seg_3892" n="e" s="T219">bü </ts>
               <ts e="T221" id="Seg_3894" n="e" s="T220">bĭʔpiem. </ts>
               <ts e="T222" id="Seg_3896" n="e" s="T221">Dĭgəttə </ts>
               <ts e="T223" id="Seg_3898" n="e" s="T222">mašina </ts>
               <ts e="T224" id="Seg_3900" n="e" s="T223">bĭdəbi, </ts>
               <ts e="T225" id="Seg_3902" n="e" s="T224">măn </ts>
               <ts e="T226" id="Seg_3904" n="e" s="T225">(amno-) </ts>
               <ts e="T227" id="Seg_3906" n="e" s="T226">amnobiam. </ts>
               <ts e="T228" id="Seg_3908" n="e" s="T227">Dĭgəttə </ts>
               <ts e="T229" id="Seg_3910" n="e" s="T228">üzəbiem. </ts>
               <ts e="T230" id="Seg_3912" n="e" s="T229">Šide </ts>
               <ts e="T231" id="Seg_3914" n="e" s="T230">kuza </ts>
               <ts e="T232" id="Seg_3916" n="e" s="T231">nugaʔi </ts>
               <ts e="T233" id="Seg_3918" n="e" s="T232">măjagən. </ts>
               <ts e="T234" id="Seg_3920" n="e" s="T233">Măn </ts>
               <ts e="T235" id="Seg_3922" n="e" s="T234">dĭbər </ts>
               <ts e="T236" id="Seg_3924" n="e" s="T235">(sa-) </ts>
               <ts e="T237" id="Seg_3926" n="e" s="T236">sʼabiam. </ts>
               <ts e="T238" id="Seg_3928" n="e" s="T237">Kanžəbəj </ts>
               <ts e="T239" id="Seg_3930" n="e" s="T238">(obbere). </ts>
               <ts e="T240" id="Seg_3932" n="e" s="T239">(Dʼok) </ts>
               <ts e="T241" id="Seg_3934" n="e" s="T240">miʔ </ts>
               <ts e="T242" id="Seg_3936" n="e" s="T241">edəʔleʔbə </ts>
               <ts e="T243" id="Seg_3938" n="e" s="T242">mašina, </ts>
               <ts e="T244" id="Seg_3940" n="e" s="T243">mašinaziʔ </ts>
               <ts e="T245" id="Seg_3942" n="e" s="T244">kanžəbəj. </ts>
               <ts e="T246" id="Seg_3944" n="e" s="T245">Dĭgəttə </ts>
               <ts e="T247" id="Seg_3946" n="e" s="T246">mašina </ts>
               <ts e="T248" id="Seg_3948" n="e" s="T247">šobi, </ts>
               <ts e="T249" id="Seg_3950" n="e" s="T248">(amnobiaʔ- </ts>
               <ts e="T250" id="Seg_3952" n="e" s="T249">amnobiaʔ-) </ts>
               <ts e="T251" id="Seg_3954" n="e" s="T250">amnobiam. </ts>
               <ts e="T252" id="Seg_3956" n="e" s="T251">I </ts>
               <ts e="T253" id="Seg_3958" n="e" s="T252">Săjanskəjnə </ts>
               <ts e="T254" id="Seg_3960" n="e" s="T253">šobiam. </ts>
               <ts e="T255" id="Seg_3962" n="e" s="T254">Dĭgəttə </ts>
               <ts e="T256" id="Seg_3964" n="e" s="T255">(nĭn) </ts>
               <ts e="T257" id="Seg_3966" n="e" s="T256">(ibem-) </ts>
               <ts e="T258" id="Seg_3968" n="e" s="T257">ibiem </ts>
               <ts e="T259" id="Seg_3970" n="e" s="T258">ipek, </ts>
               <ts e="T260" id="Seg_3972" n="e" s="T259">sĭreʔpne. </ts>
               <ts e="T261" id="Seg_3974" n="e" s="T260">Vanʼka </ts>
               <ts e="T262" id="Seg_3976" n="e" s="T261">bălʼnʼitsagən </ts>
               <ts e="T263" id="Seg_3978" n="e" s="T262">iʔbolaʔpi, </ts>
               <ts e="T264" id="Seg_3980" n="e" s="T263">kumbiam </ts>
               <ts e="T265" id="Seg_3982" n="e" s="T264">dĭʔnə. </ts>
               <ts e="T266" id="Seg_3984" n="e" s="T265">Măna </ts>
               <ts e="T267" id="Seg_3986" n="e" s="T266">dĭn </ts>
               <ts e="T268" id="Seg_3988" n="e" s="T267">nörbəlaʔbəʔjə: </ts>
               <ts e="T269" id="Seg_3990" n="e" s="T268">Dön </ts>
               <ts e="T270" id="Seg_3992" n="e" s="T269">igeʔ. </ts>
               <ts e="T271" id="Seg_3994" n="e" s="T270">Miʔ </ts>
               <ts e="T272" id="Seg_3996" n="e" s="T271">mašina </ts>
               <ts e="T273" id="Seg_3998" n="e" s="T272">kanaʔ. </ts>
               <ts e="T274" id="Seg_4000" n="e" s="T273">Măn </ts>
               <ts e="T275" id="Seg_4002" n="e" s="T274">nuʔməbiem. </ts>
               <ts e="T276" id="Seg_4004" n="e" s="T275">Gijendə. </ts>
               <ts e="T277" id="Seg_4006" n="e" s="T276">Ej </ts>
               <ts e="T278" id="Seg_4008" n="e" s="T277">kubiam. </ts>
               <ts e="T279" id="Seg_4010" n="e" s="T278">Dĭgəttə </ts>
               <ts e="T280" id="Seg_4012" n="e" s="T279">kambiam </ts>
               <ts e="T281" id="Seg_4014" n="e" s="T280">üdʼiʔ. </ts>
               <ts e="T282" id="Seg_4016" n="e" s="T281">Dĭgəttə </ts>
               <ts e="T283" id="Seg_4018" n="e" s="T282">bazo </ts>
               <ts e="T284" id="Seg_4020" n="e" s="T283">mašina </ts>
               <ts e="T285" id="Seg_4022" n="e" s="T284">šonəga. </ts>
               <ts e="T286" id="Seg_4024" n="e" s="T285">Măn </ts>
               <ts e="T287" id="Seg_4026" n="e" s="T286">amnobiam, </ts>
               <ts e="T288" id="Seg_4028" n="e" s="T287">Malinovkinə </ts>
               <ts e="T289" id="Seg_4030" n="e" s="T288">šobiam. </ts>
               <ts e="T290" id="Seg_4032" n="e" s="T289">Dĭgəttə </ts>
               <ts e="T291" id="Seg_4034" n="e" s="T290">bazo </ts>
               <ts e="T292" id="Seg_4036" n="e" s="T291">üjüzi </ts>
               <ts e="T293" id="Seg_4038" n="e" s="T292">kambiam. </ts>
               <ts e="T294" id="Seg_4040" n="e" s="T293">Dĭgəttə </ts>
               <ts e="T295" id="Seg_4042" n="e" s="T294">bazo </ts>
               <ts e="T296" id="Seg_4044" n="e" s="T295">mašina </ts>
               <ts e="T297" id="Seg_4046" n="e" s="T296">bĭdəbi </ts>
               <ts e="T298" id="Seg_4048" n="e" s="T297">i </ts>
               <ts e="T299" id="Seg_4050" n="e" s="T298">maːndə </ts>
               <ts e="T300" id="Seg_4052" n="e" s="T299">šobiam. </ts>
               <ts e="T301" id="Seg_4054" n="e" s="T300">Kamən </ts>
               <ts e="T302" id="Seg_4056" n="e" s="T301">Matvejev </ts>
               <ts e="T303" id="Seg_4058" n="e" s="T302">šobi </ts>
               <ts e="T304" id="Seg_4060" n="e" s="T303">măna, </ts>
               <ts e="T305" id="Seg_4062" n="e" s="T304">(dʼăbaktərla-) </ts>
               <ts e="T306" id="Seg_4064" n="e" s="T305">dʼăbaktərlaʔbə </ts>
               <ts e="T307" id="Seg_4066" n="e" s="T306">nudla. </ts>
               <ts e="T308" id="Seg_4068" n="e" s="T307">A </ts>
               <ts e="T309" id="Seg_4070" n="e" s="T308">măn </ts>
               <ts e="T310" id="Seg_4072" n="e" s="T309">(kazak=) </ts>
               <ts e="T311" id="Seg_4074" n="e" s="T310">kazak </ts>
               <ts e="T312" id="Seg_4076" n="e" s="T311">dʼăbaktərlaʔbəm </ts>
               <ts e="T313" id="Seg_4078" n="e" s="T312">i </ts>
               <ts e="T314" id="Seg_4080" n="e" s="T313">(petʼerlaʔbəm). </ts>
               <ts e="T315" id="Seg_4082" n="e" s="T314">Tăn </ts>
               <ts e="T316" id="Seg_4084" n="e" s="T315">tüj </ts>
               <ts e="T317" id="Seg_4086" n="e" s="T316">nu </ts>
               <ts e="T318" id="Seg_4088" n="e" s="T317">igel, </ts>
               <ts e="T319" id="Seg_4090" n="e" s="T318">a </ts>
               <ts e="T320" id="Seg_4092" n="e" s="T319">măn </ts>
               <ts e="T321" id="Seg_4094" n="e" s="T320">kazak </ts>
               <ts e="T322" id="Seg_4096" n="e" s="T321">igem. </ts>
               <ts e="T323" id="Seg_4098" n="e" s="T322">Dĭgəttə </ts>
               <ts e="T324" id="Seg_4100" n="e" s="T323">dĭzeŋ </ts>
               <ts e="T325" id="Seg_4102" n="e" s="T324">dʼijenə </ts>
               <ts e="T326" id="Seg_4104" n="e" s="T325">kambiʔi, </ts>
               <ts e="T327" id="Seg_4106" n="e" s="T326">Dʼelamdə. </ts>
               <ts e="T328" id="Seg_4108" n="e" s="T327">(Bi-) </ts>
               <ts e="T329" id="Seg_4110" n="e" s="T328">Kola </ts>
               <ts e="T330" id="Seg_4112" n="e" s="T329">dʼaʔpiʔi </ts>
               <ts e="T331" id="Seg_4114" n="e" s="T330">dĭn. </ts>
               <ts e="T332" id="Seg_4116" n="e" s="T331">Bi </ts>
               <ts e="T333" id="Seg_4118" n="e" s="T332">idʼem </ts>
               <ts e="T334" id="Seg_4120" n="e" s="T333">enbiʔi. </ts>
               <ts e="T335" id="Seg_4122" n="e" s="T334">Dĭgəttə </ts>
               <ts e="T336" id="Seg_4124" n="e" s="T335">onʼiʔ </ts>
               <ts e="T337" id="Seg_4126" n="e" s="T336">koʔbdo </ts>
               <ts e="T338" id="Seg_4128" n="e" s="T337">urgo </ts>
               <ts e="T339" id="Seg_4130" n="e" s="T338">bar </ts>
               <ts e="T340" id="Seg_4132" n="e" s="T339">ĭzemnuʔpi </ts>
               <ts e="T341" id="Seg_4134" n="e" s="T340">bar. </ts>
               <ts e="T342" id="Seg_4136" n="e" s="T341">Dĭgəttə </ts>
               <ts e="T343" id="Seg_4138" n="e" s="T342">(dĭzeŋ) </ts>
               <ts e="T344" id="Seg_4140" n="e" s="T343">šobiʔi. </ts>
               <ts e="T345" id="Seg_4142" n="e" s="T344">Turanə </ts>
               <ts e="T346" id="Seg_4144" n="e" s="T345">Kanoklerdə. </ts>
               <ts e="T347" id="Seg_4146" n="e" s="T346">I </ts>
               <ts e="T348" id="Seg_4148" n="e" s="T347">ne </ts>
               <ts e="T349" id="Seg_4150" n="e" s="T348">ibiʔi </ts>
               <ts e="T350" id="Seg_4152" n="e" s="T349">i </ts>
               <ts e="T351" id="Seg_4154" n="e" s="T350">dĭm </ts>
               <ts e="T352" id="Seg_4156" n="e" s="T351">deʔpiʔi </ts>
               <ts e="T353" id="Seg_4158" n="e" s="T352">bălʼnʼitsanə. </ts>
               <ts e="T354" id="Seg_4160" n="e" s="T353">Tĭn </ts>
               <ts e="T355" id="Seg_4162" n="e" s="T354">üjün </ts>
               <ts e="T356" id="Seg_4164" n="e" s="T355">bar </ts>
               <ts e="T357" id="Seg_4166" n="e" s="T356">(bü </ts>
               <ts e="T358" id="Seg_4168" n="e" s="T357">bĭʔpiʔi). </ts>
               <ts e="T359" id="Seg_4170" n="e" s="T358">I </ts>
               <ts e="T360" id="Seg_4172" n="e" s="T359">dʼubtəbiʔi. </ts>
               <ts e="T361" id="Seg_4174" n="e" s="T360">Dʼazirbiʔi. </ts>
               <ts e="T362" id="Seg_4176" n="e" s="T361">A_to </ts>
               <ts e="T363" id="Seg_4178" n="e" s="T362">dĭ </ts>
               <ts e="T364" id="Seg_4180" n="e" s="T363">mĭnzittə </ts>
               <ts e="T365" id="Seg_4182" n="e" s="T364">(ej </ts>
               <ts e="T366" id="Seg_4184" n="e" s="T365">m-) </ts>
               <ts e="T367" id="Seg_4186" n="e" s="T366">ej </ts>
               <ts e="T368" id="Seg_4188" n="e" s="T367">molia. </ts>
               <ts e="T369" id="Seg_4190" n="e" s="T368">Măn </ts>
               <ts e="T370" id="Seg_4192" n="e" s="T369">(măndər- </ts>
               <ts e="T371" id="Seg_4194" n="e" s="T370">măn-) </ts>
               <ts e="T372" id="Seg_4196" n="e" s="T371">măndərbiam, </ts>
               <ts e="T373" id="Seg_4198" n="e" s="T372">măndərbiam </ts>
               <ts e="T374" id="Seg_4200" n="e" s="T373">(правда). </ts>
               <ts e="T375" id="Seg_4202" n="e" s="T374">Dĭbər </ts>
               <ts e="T376" id="Seg_4204" n="e" s="T375">(pʼaŋdəbiam </ts>
               <ts e="T377" id="Seg_4206" n="e" s="T376">sazən). </ts>
               <ts e="T378" id="Seg_4208" n="e" s="T377">Dĭbər </ts>
               <ts e="T379" id="Seg_4210" n="e" s="T378">(pəŋ-) </ts>
               <ts e="T380" id="Seg_4212" n="e" s="T379">pʼaŋdəbiam. </ts>
               <ts e="T381" id="Seg_4214" n="e" s="T380">Šindi </ts>
               <ts e="T383" id="Seg_4216" n="e" s="T381">măn_də </ts>
               <ts e="T384" id="Seg_4218" n="e" s="T383">kulambi, </ts>
               <ts e="T385" id="Seg_4220" n="e" s="T384">šindi </ts>
               <ts e="T387" id="Seg_4222" n="e" s="T385">măn_də </ts>
               <ts e="T388" id="Seg_4224" n="e" s="T387">ej </ts>
               <ts e="T389" id="Seg_4226" n="e" s="T388">kulambi. </ts>
               <ts e="T390" id="Seg_4228" n="e" s="T389">Gijendə </ts>
               <ts e="T391" id="Seg_4230" n="e" s="T390">naga </ts>
               <ts e="T392" id="Seg_4232" n="e" s="T391">pravdaʔi. </ts>
               <ts e="T393" id="Seg_4234" n="e" s="T392">A </ts>
               <ts e="T394" id="Seg_4236" n="e" s="T393">măn </ts>
               <ts e="T395" id="Seg_4238" n="e" s="T394">tenəbiem </ts>
               <ts e="T396" id="Seg_4240" n="e" s="T395">(ige=) </ts>
               <ts e="T397" id="Seg_4242" n="e" s="T396">vezʼdʼe </ts>
               <ts e="T398" id="Seg_4244" n="e" s="T397">pravda </ts>
               <ts e="T399" id="Seg_4246" n="e" s="T398">ige. </ts>
               <ts e="T400" id="Seg_4248" n="e" s="T399">A </ts>
               <ts e="T401" id="Seg_4250" n="e" s="T400">dĭm </ts>
               <ts e="T402" id="Seg_4252" n="e" s="T401">gijendə </ts>
               <ts e="T403" id="Seg_4254" n="e" s="T402">naga, </ts>
               <ts e="T404" id="Seg_4256" n="e" s="T403">üge </ts>
               <ts e="T405" id="Seg_4258" n="e" s="T404">(šanaklʼu). </ts>
               <ts e="T406" id="Seg_4260" n="e" s="T405">Dĭgəttə </ts>
               <ts e="T407" id="Seg_4262" n="e" s="T406">prokuror </ts>
               <ts e="T408" id="Seg_4264" n="e" s="T407">pʼaŋdəbi, </ts>
               <ts e="T409" id="Seg_4266" n="e" s="T408">(gibərdə) </ts>
               <ts e="T869" id="Seg_4268" n="e" s="T409">kalla </ts>
               <ts e="T410" id="Seg_4270" n="e" s="T869">dʼürbi, </ts>
               <ts e="T411" id="Seg_4272" n="e" s="T410">ej </ts>
               <ts e="T412" id="Seg_4274" n="e" s="T411">tĭmnem. </ts>
               <ts e="T413" id="Seg_4276" n="e" s="T412">Jelʼa </ts>
               <ts e="T414" id="Seg_4278" n="e" s="T413">(kuʔkubi </ts>
               <ts e="T415" id="Seg_4280" n="e" s="T414">dʼodunʼim). </ts>
               <ts e="T416" id="Seg_4282" n="e" s="T415">Bar </ts>
               <ts e="T417" id="Seg_4284" n="e" s="T416">lotkanə </ts>
               <ts e="T418" id="Seg_4286" n="e" s="T417">bünə </ts>
               <ts e="T419" id="Seg_4288" n="e" s="T418">kambiʔi. </ts>
               <ts e="T420" id="Seg_4290" n="e" s="T419">(Dĭgəttə) </ts>
               <ts e="T421" id="Seg_4292" n="e" s="T420">girgitdə </ts>
               <ts e="T422" id="Seg_4294" n="e" s="T421">koʔbdozi </ts>
               <ts e="T423" id="Seg_4296" n="e" s="T422">xatʼeli </ts>
               <ts e="T424" id="Seg_4298" n="e" s="T423">bügən </ts>
               <ts e="T425" id="Seg_4300" n="e" s="T424">плавать. </ts>
               <ts e="T426" id="Seg_4302" n="e" s="T425">A </ts>
               <ts e="T427" id="Seg_4304" n="e" s="T426">bü </ts>
               <ts e="T428" id="Seg_4306" n="e" s="T427">ugandə </ts>
               <ts e="T429" id="Seg_4308" n="e" s="T428">urgo. </ts>
               <ts e="T430" id="Seg_4310" n="e" s="T429">Dĭzeŋ </ts>
               <ts e="T431" id="Seg_4312" n="e" s="T430">bar </ts>
               <ts e="T432" id="Seg_4314" n="e" s="T431">xatʼeli </ts>
               <ts e="T433" id="Seg_4316" n="e" s="T432">lotkanə </ts>
               <ts e="T434" id="Seg_4318" n="e" s="T433">sʼazittə. </ts>
               <ts e="T435" id="Seg_4320" n="e" s="T434">Da </ts>
               <ts e="T436" id="Seg_4322" n="e" s="T435">bar </ts>
               <ts e="T437" id="Seg_4324" n="e" s="T436">nereʔluʔpiʔi. </ts>
               <ts e="T439" id="Seg_4326" n="e" s="T437">Dĭgəttə… </ts>
               <ts e="T440" id="Seg_4328" n="e" s="T439">Dĭgəttə </ts>
               <ts e="T441" id="Seg_4330" n="e" s="T440">dĭn </ts>
               <ts e="T442" id="Seg_4332" n="e" s="T441">(nereʔluʔpiʔi). </ts>
               <ts e="T443" id="Seg_4334" n="e" s="T442">Oʔbdəbi, </ts>
               <ts e="T444" id="Seg_4336" n="e" s="T443">(kuiol-) </ts>
               <ts e="T445" id="Seg_4338" n="e" s="T444">kuliat </ts>
               <ts e="T446" id="Seg_4340" n="e" s="T445">što </ts>
               <ts e="T447" id="Seg_4342" n="e" s="T446">dʼoduni </ts>
               <ts e="T448" id="Seg_4344" n="e" s="T447">kübi. </ts>
               <ts e="T449" id="Seg_4346" n="e" s="T448">(Sălămatən) </ts>
               <ts e="T450" id="Seg_4348" n="e" s="T449">Mitrăfanən </ts>
               <ts e="T451" id="Seg_4350" n="e" s="T450">turat </ts>
               <ts e="T452" id="Seg_4352" n="e" s="T451">iššo </ts>
               <ts e="T453" id="Seg_4354" n="e" s="T452">nuga. </ts>
               <ts e="T454" id="Seg_4356" n="e" s="T453">(I </ts>
               <ts e="T455" id="Seg_4358" n="e" s="T454">ku-) </ts>
               <ts e="T456" id="Seg_4360" n="e" s="T455">I </ts>
               <ts e="T457" id="Seg_4362" n="e" s="T456">gurun </ts>
               <ts e="T458" id="Seg_4364" n="e" s="T457">Andʼigatovan </ts>
               <ts e="T459" id="Seg_4366" n="e" s="T458">tura </ts>
               <ts e="T460" id="Seg_4368" n="e" s="T459">iššo </ts>
               <ts e="T461" id="Seg_4370" n="e" s="T460">nuga. </ts>
               <ts e="T462" id="Seg_4372" n="e" s="T461">(Dʼibiai- </ts>
               <ts e="T463" id="Seg_4374" n="e" s="T462">ən-) </ts>
               <ts e="T464" id="Seg_4376" n="e" s="T463">Dʼibiev </ts>
               <ts e="T465" id="Seg_4378" n="e" s="T464">Vălodʼan </ts>
               <ts e="T466" id="Seg_4380" n="e" s="T465">tura </ts>
               <ts e="T467" id="Seg_4382" n="e" s="T466">iššo </ts>
               <ts e="T468" id="Seg_4384" n="e" s="T467">nuga. </ts>
               <ts e="T469" id="Seg_4386" n="e" s="T468">Kotʼerov </ts>
               <ts e="T470" id="Seg_4388" n="e" s="T469">Mikitan </ts>
               <ts e="T471" id="Seg_4390" n="e" s="T470">tura </ts>
               <ts e="T472" id="Seg_4392" n="e" s="T471">iššo </ts>
               <ts e="T473" id="Seg_4394" n="e" s="T472">nuga. </ts>
               <ts e="T474" id="Seg_4396" n="e" s="T473">(Gateva) </ts>
               <ts e="T475" id="Seg_4398" n="e" s="T474">Afanasin </ts>
               <ts e="T476" id="Seg_4400" n="e" s="T475">tura </ts>
               <ts e="T477" id="Seg_4402" n="e" s="T476">iššo </ts>
               <ts e="T478" id="Seg_4404" n="e" s="T477">nuga. </ts>
               <ts e="T479" id="Seg_4406" n="e" s="T478">I </ts>
               <ts e="T480" id="Seg_4408" n="e" s="T479">Šajbin </ts>
               <ts e="T481" id="Seg_4410" n="e" s="T480">Vlastə </ts>
               <ts e="T482" id="Seg_4412" n="e" s="T481">iššo </ts>
               <ts e="T483" id="Seg_4414" n="e" s="T482">tura </ts>
               <ts e="T484" id="Seg_4416" n="e" s="T483">nuga. </ts>
               <ts e="T485" id="Seg_4418" n="e" s="T484">I </ts>
               <ts e="T486" id="Seg_4420" n="e" s="T485">(Solomatov=) </ts>
               <ts e="T487" id="Seg_4422" n="e" s="T486">(Sălămatov) </ts>
               <ts e="T488" id="Seg_4424" n="e" s="T487">Vasilij </ts>
               <ts e="T489" id="Seg_4426" n="e" s="T488">iššo </ts>
               <ts e="T490" id="Seg_4428" n="e" s="T489">tura </ts>
               <ts e="T491" id="Seg_4430" n="e" s="T490">(nuga). </ts>
               <ts e="T492" id="Seg_4432" n="e" s="T491">Kamən </ts>
               <ts e="T493" id="Seg_4434" n="e" s="T492">Tugarin </ts>
               <ts e="T494" id="Seg_4436" n="e" s="T493">šobi </ts>
               <ts e="T495" id="Seg_4438" n="e" s="T494">dʼăbaktərzittə </ts>
               <ts e="T496" id="Seg_4440" n="e" s="T495">döbər, </ts>
               <ts e="T497" id="Seg_4442" n="e" s="T496">dön </ts>
               <ts e="T498" id="Seg_4444" n="e" s="T497">(su-) </ts>
               <ts e="T499" id="Seg_4446" n="e" s="T498">sumna </ts>
               <ts e="T500" id="Seg_4448" n="e" s="T499">bʼeʔ </ts>
               <ts e="T501" id="Seg_4450" n="e" s="T500">tura </ts>
               <ts e="T502" id="Seg_4452" n="e" s="T501">ibi. </ts>
               <ts e="T503" id="Seg_4454" n="e" s="T502">(Sargolov) </ts>
               <ts e="T504" id="Seg_4456" n="e" s="T503">Jakov </ts>
               <ts e="T505" id="Seg_4458" n="e" s="T504">(Filippovič) </ts>
               <ts e="T506" id="Seg_4460" n="e" s="T505">külaːmbi. </ts>
               <ts e="T507" id="Seg_4462" n="e" s="T506">Dĭ </ts>
               <ts e="T508" id="Seg_4464" n="e" s="T507">kambi </ts>
               <ts e="T509" id="Seg_4466" n="e" s="T508">Permʼakovtə, </ts>
               <ts e="T510" id="Seg_4468" n="e" s="T509">dĭn </ts>
               <ts e="T511" id="Seg_4470" n="e" s="T510">ara </ts>
               <ts e="T512" id="Seg_4472" n="e" s="T511">bĭʔpi, </ts>
               <ts e="T513" id="Seg_4474" n="e" s="T512">dĭn </ts>
               <ts e="T514" id="Seg_4476" n="e" s="T513">(i-) </ts>
               <ts e="T515" id="Seg_4478" n="e" s="T514">ibi </ts>
               <ts e="T516" id="Seg_4480" n="e" s="T515">onʼiʔ </ts>
               <ts e="T517" id="Seg_4482" n="e" s="T516">бутылка. </ts>
               <ts e="T518" id="Seg_4484" n="e" s="T517">I </ts>
               <ts e="T519" id="Seg_4486" n="e" s="T518">netsi </ts>
               <ts e="T520" id="Seg_4488" n="e" s="T519">šobi. </ts>
               <ts e="T521" id="Seg_4490" n="e" s="T520">Dĭgəttə </ts>
               <ts e="T522" id="Seg_4492" n="e" s="T521">kambi </ts>
               <ts e="T523" id="Seg_4494" n="e" s="T522">krospaʔinə. </ts>
               <ts e="T524" id="Seg_4496" n="e" s="T523">Dĭgəttə </ts>
               <ts e="T525" id="Seg_4498" n="e" s="T524">kămlia </ts>
               <ts e="T526" id="Seg_4500" n="e" s="T525">da </ts>
               <ts e="T527" id="Seg_4502" n="e" s="T526">(kudonzlia): </ts>
               <ts e="T528" id="Seg_4504" n="e" s="T527">Bĭdeʔ </ts>
               <ts e="T529" id="Seg_4506" n="e" s="T528">nükem, </ts>
               <ts e="T530" id="Seg_4508" n="e" s="T529">bĭdeʔ </ts>
               <ts e="T531" id="Seg_4510" n="e" s="T530">nükem! </ts>
               <ts e="T532" id="Seg_4512" n="e" s="T531">A </ts>
               <ts e="T533" id="Seg_4514" n="e" s="T532">măn </ts>
               <ts e="T534" id="Seg_4516" n="e" s="T533">abam </ts>
               <ts e="T535" id="Seg_4518" n="e" s="T534">nuga </ts>
               <ts e="T536" id="Seg_4520" n="e" s="T535">da </ts>
               <ts e="T537" id="Seg_4522" n="e" s="T536">măndolaʔbə </ts>
               <ts e="T538" id="Seg_4524" n="e" s="T537">da </ts>
               <ts e="T539" id="Seg_4526" n="e" s="T538">kaknarlaʔbə. </ts>
               <ts e="T540" id="Seg_4528" n="e" s="T539">Măn </ts>
               <ts e="T541" id="Seg_4530" n="e" s="T540">noʔ </ts>
               <ts e="T542" id="Seg_4532" n="e" s="T541">jaʔpiam. </ts>
               <ts e="T543" id="Seg_4534" n="e" s="T542">(Dĭgəttə) </ts>
               <ts e="T544" id="Seg_4536" n="e" s="T543">oʔbdəbiam. </ts>
               <ts e="T545" id="Seg_4538" n="e" s="T544">Dĭgəttə </ts>
               <ts e="T546" id="Seg_4540" n="e" s="T545">stogdə </ts>
               <ts e="T547" id="Seg_4542" n="e" s="T546">embiem. </ts>
               <ts e="T548" id="Seg_4544" n="e" s="T547">Dĭ </ts>
               <ts e="T549" id="Seg_4546" n="e" s="T548">dʼüʔpi </ts>
               <ts e="T550" id="Seg_4548" n="e" s="T549">ibi. </ts>
               <ts e="T551" id="Seg_4550" n="e" s="T550">Dĭgəttə </ts>
               <ts e="T552" id="Seg_4552" n="e" s="T551">nagur </ts>
               <ts e="T553" id="Seg_4554" n="e" s="T552">(dʼara-) </ts>
               <ts e="T554" id="Seg_4556" n="e" s="T553">dʼala </ts>
               <ts e="T555" id="Seg_4558" n="e" s="T554">ej </ts>
               <ts e="T556" id="Seg_4560" n="e" s="T555">kambiam. </ts>
               <ts e="T557" id="Seg_4562" n="e" s="T556">Ugandə </ts>
               <ts e="T558" id="Seg_4564" n="e" s="T557">dʼibige </ts>
               <ts e="T559" id="Seg_4566" n="e" s="T558">molambi. </ts>
               <ts e="T560" id="Seg_4568" n="e" s="T559">(Mɨ- </ts>
               <ts e="T561" id="Seg_4570" n="e" s="T560">ši-) </ts>
               <ts e="T562" id="Seg_4572" n="e" s="T561">Măn </ts>
               <ts e="T563" id="Seg_4574" n="e" s="T562">šide </ts>
               <ts e="T564" id="Seg_4576" n="e" s="T563">ne </ts>
               <ts e="T565" id="Seg_4578" n="e" s="T564">ibiem, </ts>
               <ts e="T566" id="Seg_4580" n="e" s="T565">kambiam. </ts>
               <ts e="T567" id="Seg_4582" n="e" s="T566">Bar </ts>
               <ts e="T568" id="Seg_4584" n="e" s="T567">kobi </ts>
               <ts e="T569" id="Seg_4586" n="e" s="T568">da </ts>
               <ts e="T570" id="Seg_4588" n="e" s="T569">bazoʔ </ts>
               <ts e="T571" id="Seg_4590" n="e" s="T570">embibeʔ </ts>
               <ts e="T572" id="Seg_4592" n="e" s="T571">kuvas </ts>
               <ts e="T573" id="Seg_4594" n="e" s="T572">(zəʔ-) </ts>
               <ts e="T574" id="Seg_4596" n="e" s="T573">(stog). </ts>
               <ts e="T575" id="Seg_4598" n="e" s="T574">Măn </ts>
               <ts e="T576" id="Seg_4600" n="e" s="T575">amnobiam </ts>
               <ts e="T577" id="Seg_4602" n="e" s="T576">šide </ts>
               <ts e="T578" id="Seg_4604" n="e" s="T577">bʼeʔ </ts>
               <ts e="T579" id="Seg_4606" n="e" s="T578">pʼe. </ts>
               <ts e="T580" id="Seg_4608" n="e" s="T579">Bospə </ts>
               <ts e="T581" id="Seg_4610" n="e" s="T580">noʔ </ts>
               <ts e="T582" id="Seg_4612" n="e" s="T581">jaʔpiam. </ts>
               <ts e="T583" id="Seg_4614" n="e" s="T582">Bostə </ts>
               <ts e="T584" id="Seg_4616" n="e" s="T583">oʔbdəbiam. </ts>
               <ts e="T585" id="Seg_4618" n="e" s="T584">Bostə </ts>
               <ts e="T586" id="Seg_4620" n="e" s="T585">embiem. </ts>
               <ts e="T587" id="Seg_4622" n="e" s="T586">Inezi </ts>
               <ts e="T588" id="Seg_4624" n="e" s="T587">maʔnʼi </ts>
               <ts e="T589" id="Seg_4626" n="e" s="T588">tažerbiam </ts>
               <ts e="T590" id="Seg_4628" n="e" s="T589">bar. </ts>
               <ts e="T591" id="Seg_4630" n="e" s="T590">(Tüžöjʔim) </ts>
               <ts e="T592" id="Seg_4632" n="e" s="T591">(im) </ts>
               <ts e="T593" id="Seg_4634" n="e" s="T592">(bĭd-) </ts>
               <ts e="T594" id="Seg_4636" n="e" s="T593">bădlaʔpiam. </ts>
               <ts e="T595" id="Seg_4638" n="e" s="T594">Onʼiʔ </ts>
               <ts e="T596" id="Seg_4640" n="e" s="T595">ine </ts>
               <ts e="T597" id="Seg_4642" n="e" s="T596">ibiem. </ts>
               <ts e="T598" id="Seg_4644" n="e" s="T597">Dĭgəttə </ts>
               <ts e="T599" id="Seg_4646" n="e" s="T598">plemʼannicat </ts>
               <ts e="T600" id="Seg_4648" n="e" s="T599">(nʼin) </ts>
               <ts e="T601" id="Seg_4650" n="e" s="T600">ibiem </ts>
               <ts e="T602" id="Seg_4652" n="e" s="T601">da </ts>
               <ts e="T603" id="Seg_4654" n="e" s="T602">iššo </ts>
               <ts e="T604" id="Seg_4656" n="e" s="T603">(dʼügej) </ts>
               <ts e="T605" id="Seg_4658" n="e" s="T604">kambibaʔ, </ts>
               <ts e="T606" id="Seg_4660" n="e" s="T605">noʔ </ts>
               <ts e="T607" id="Seg_4662" n="e" s="T606">embibeʔ. </ts>
               <ts e="T608" id="Seg_4664" n="e" s="T607">Dĭgəttə </ts>
               <ts e="T609" id="Seg_4666" n="e" s="T608">šobibaʔ, </ts>
               <ts e="T610" id="Seg_4668" n="e" s="T609">šobibaʔ </ts>
               <ts e="T611" id="Seg_4670" n="e" s="T610">miʔ. </ts>
               <ts e="T612" id="Seg_4672" n="e" s="T611">Noʔ </ts>
               <ts e="T613" id="Seg_4674" n="e" s="T612">(üzə-) </ts>
               <ts e="T867" id="Seg_4676" n="e" s="T613">üzəbi </ts>
               <ts e="T614" id="Seg_4678" n="e" s="T867">(dʼuno). </ts>
               <ts e="T615" id="Seg_4680" n="e" s="T614">Dĭgəttə </ts>
               <ts e="T616" id="Seg_4682" n="e" s="T615">dʼildəbibeʔ, </ts>
               <ts e="T617" id="Seg_4684" n="e" s="T616">dʼildəbibeʔ, </ts>
               <ts e="T618" id="Seg_4686" n="e" s="T617">ej </ts>
               <ts e="T619" id="Seg_4688" n="e" s="T618">moliabaʔ. </ts>
               <ts e="T620" id="Seg_4690" n="e" s="T619">(Dĭ) </ts>
               <ts e="T621" id="Seg_4692" n="e" s="T620">măndə: </ts>
               <ts e="T622" id="Seg_4694" n="e" s="T621">Tüjö </ts>
               <ts e="T623" id="Seg_4696" n="e" s="T622">ine </ts>
               <ts e="T624" id="Seg_4698" n="e" s="T623">(kor- </ts>
               <ts e="T625" id="Seg_4700" n="e" s="T624">kor-) </ts>
               <ts e="T626" id="Seg_4702" n="e" s="T625">(körerbim). </ts>
               <ts e="T627" id="Seg_4704" n="e" s="T626">Dĭgəttə </ts>
               <ts e="T628" id="Seg_4706" n="e" s="T627">dʼildlim. </ts>
               <ts e="T629" id="Seg_4708" n="e" s="T628">A </ts>
               <ts e="T630" id="Seg_4710" n="e" s="T629">kăde </ts>
               <ts e="T631" id="Seg_4712" n="e" s="T630">(tăn) </ts>
               <ts e="T632" id="Seg_4714" n="e" s="T631">dʼildlil? </ts>
               <ts e="T633" id="Seg_4716" n="e" s="T632">A </ts>
               <ts e="T634" id="Seg_4718" n="e" s="T633">(dăre) </ts>
               <ts e="T635" id="Seg_4720" n="e" s="T634">bar </ts>
               <ts e="T866" id="Seg_4722" n="e" s="T635">((…)) </ts>
               <ts e="T636" id="Seg_4724" n="e" s="T866">(barəʔpi) </ts>
               <ts e="T637" id="Seg_4726" n="e" s="T636">(oršaʔi) </ts>
               <ts e="T638" id="Seg_4728" n="e" s="T637">i </ts>
               <ts e="T639" id="Seg_4730" n="e" s="T638">dĭgəttə </ts>
               <ts e="T640" id="Seg_4732" n="e" s="T639">ine </ts>
               <ts e="T642" id="Seg_4734" n="e" s="T640">(dʼi). </ts>
               <ts e="T643" id="Seg_4736" n="e" s="T642">Dĭgəttə </ts>
               <ts e="T644" id="Seg_4738" n="e" s="T643">(maʔ) </ts>
               <ts e="T645" id="Seg_4740" n="e" s="T644">небось </ts>
               <ts e="T646" id="Seg_4742" n="e" s="T645">šobibaʔ. </ts>
               <ts e="T647" id="Seg_4744" n="e" s="T646">Mĭmbibeʔ </ts>
               <ts e="T648" id="Seg_4746" n="e" s="T647">noʔ. </ts>
               <ts e="T649" id="Seg_4748" n="e" s="T648">Dĭgəttə </ts>
               <ts e="T650" id="Seg_4750" n="e" s="T649">inem </ts>
               <ts e="T651" id="Seg_4752" n="e" s="T650">(kobmbibaʔ). </ts>
               <ts e="T652" id="Seg_4754" n="e" s="T651">Măn </ts>
               <ts e="T653" id="Seg_4756" n="e" s="T652">ugandə </ts>
               <ts e="T654" id="Seg_4758" n="e" s="T653">kămbiam. </ts>
               <ts e="T655" id="Seg_4760" n="e" s="T654">Dĭgəttə </ts>
               <ts e="T656" id="Seg_4762" n="e" s="T655">kašelʼ </ts>
               <ts e="T657" id="Seg_4764" n="e" s="T656">šobi. </ts>
               <ts e="T658" id="Seg_4766" n="e" s="T657">Dĭgəttə </ts>
               <ts e="T659" id="Seg_4768" n="e" s="T658">măn </ts>
               <ts e="T660" id="Seg_4770" n="e" s="T659">sĭreʔpne </ts>
               <ts e="T661" id="Seg_4772" n="e" s="T660">bar </ts>
               <ts e="T662" id="Seg_4774" n="e" s="T661">nendəbiem </ts>
               <ts e="T663" id="Seg_4776" n="e" s="T662">aranə. </ts>
               <ts e="T664" id="Seg_4778" n="e" s="T663">Dĭgəttə </ts>
               <ts e="T665" id="Seg_4780" n="e" s="T664">bĭʔpiem, </ts>
               <ts e="T666" id="Seg_4782" n="e" s="T665">i </ts>
               <ts e="T667" id="Seg_4784" n="e" s="T666">gibərdə </ts>
               <ts e="T668" id="Seg_4786" n="e" s="T667">kalaj </ts>
               <ts e="T669" id="Seg_4788" n="e" s="T668">kašelʼbə. </ts>
               <ts e="T672" id="Seg_4790" n="e" s="T669">Не стало кашля. </ts>
               <ts e="T673" id="Seg_4792" n="e" s="T672">Girgit </ts>
               <ts e="T674" id="Seg_4794" n="e" s="T673">(noʔ </ts>
               <ts e="T675" id="Seg_4796" n="e" s="T674">i-) </ts>
               <ts e="T676" id="Seg_4798" n="e" s="T675">noʔ </ts>
               <ts e="T677" id="Seg_4800" n="e" s="T676">ige, </ts>
               <ts e="T678" id="Seg_4802" n="e" s="T677">крапива. </ts>
               <ts e="T679" id="Seg_4804" n="e" s="T678">Dĭ </ts>
               <ts e="T680" id="Seg_4806" n="e" s="T679">dudkanə </ts>
               <ts e="T681" id="Seg_4808" n="e" s="T680">izittə </ts>
               <ts e="T682" id="Seg_4810" n="e" s="T681">nada, </ts>
               <ts e="T683" id="Seg_4812" n="e" s="T682">nʼiʔsittə </ts>
               <ts e="T684" id="Seg_4814" n="e" s="T683">dĭm. </ts>
               <ts e="T685" id="Seg_4816" n="e" s="T684">(Əbərdə </ts>
               <ts e="T686" id="Seg_4818" n="e" s="T685">bər) </ts>
               <ts e="T687" id="Seg_4820" n="e" s="T686">amnosʼtə, </ts>
               <ts e="T688" id="Seg_4822" n="e" s="T687">(dĭgəttə) </ts>
               <ts e="T689" id="Seg_4824" n="e" s="T688">ej </ts>
               <ts e="T690" id="Seg_4826" n="e" s="T689">moləj </ts>
               <ts e="T691" id="Seg_4828" n="e" s="T690">kašelʼ. </ts>
               <ts e="T692" id="Seg_4830" n="e" s="T691">Nendluʔpi </ts>
               <ts e="T693" id="Seg_4832" n="e" s="T692">i </ts>
               <ts e="T694" id="Seg_4834" n="e" s="T693">(bər) </ts>
               <ts e="T695" id="Seg_4836" n="e" s="T694">naga, </ts>
               <ts e="T696" id="Seg_4838" n="e" s="T695">i </ts>
               <ts e="T697" id="Seg_4840" n="e" s="T696">ejü </ts>
               <ts e="T698" id="Seg_4842" n="e" s="T697">naga. </ts>
               <ts e="T699" id="Seg_4844" n="e" s="T698">Paʔi </ts>
               <ts e="T700" id="Seg_4846" n="e" s="T699">măna </ts>
               <ts e="T701" id="Seg_4848" n="e" s="T700">teinen </ts>
               <ts e="T702" id="Seg_4850" n="e" s="T701">ej </ts>
               <ts e="T703" id="Seg_4852" n="e" s="T702">toʔnarzittə. </ts>
               <ts e="T704" id="Seg_4854" n="e" s="T703">Kabarləj </ts>
               <ts e="T705" id="Seg_4856" n="e" s="T704">(mə-) </ts>
               <ts e="T706" id="Seg_4858" n="e" s="T705">dö </ts>
               <ts e="T707" id="Seg_4860" n="e" s="T706">nüdʼinə. </ts>
               <ts e="T708" id="Seg_4862" n="e" s="T707">Paʔi </ts>
               <ts e="T709" id="Seg_4864" n="e" s="T708">măn </ts>
               <ts e="T710" id="Seg_4866" n="e" s="T709">iʔgö. </ts>
               <ts e="T711" id="Seg_4868" n="e" s="T710">(Dö) </ts>
               <ts e="T712" id="Seg_4870" n="e" s="T711">pʼenə </ts>
               <ts e="T713" id="Seg_4872" n="e" s="T712">iššo </ts>
               <ts e="T714" id="Seg_4874" n="e" s="T713">kabarləj. </ts>
               <ts e="T715" id="Seg_4876" n="e" s="T714">Šĭšəge </ts>
               <ts e="T716" id="Seg_4878" n="e" s="T715">ugandə. </ts>
               <ts e="T717" id="Seg_4880" n="e" s="T716">Iʔgö </ts>
               <ts e="T718" id="Seg_4882" n="e" s="T717">pa </ts>
               <ts e="T719" id="Seg_4884" n="e" s="T718">kandəgaʔi. </ts>
               <ts e="T720" id="Seg_4886" n="e" s="T719">Paʔi </ts>
               <ts e="T721" id="Seg_4888" n="e" s="T720">bar </ts>
               <ts e="T722" id="Seg_4890" n="e" s="T721">surnogə </ts>
               <ts e="T723" id="Seg_4892" n="e" s="T722">sagər </ts>
               <ts e="T724" id="Seg_4894" n="e" s="T723">mobiʔi. </ts>
               <ts e="T725" id="Seg_4896" n="e" s="T724">(A_to) </ts>
               <ts e="T726" id="Seg_4898" n="e" s="T725">sĭre </ts>
               <ts e="T727" id="Seg_4900" n="e" s="T726">ibiʔi. </ts>
               <ts e="T728" id="Seg_4902" n="e" s="T727">(Măn) </ts>
               <ts e="T729" id="Seg_4904" n="e" s="T728">(büttona) </ts>
               <ts e="T730" id="Seg_4906" n="e" s="T729">šomi </ts>
               <ts e="T731" id="Seg_4908" n="e" s="T730">paʔi. </ts>
               <ts e="T732" id="Seg_4910" n="e" s="T731">Sĭre </ts>
               <ts e="T733" id="Seg_4912" n="e" s="T732">paʔi </ts>
               <ts e="T734" id="Seg_4914" n="e" s="T733">bar </ts>
               <ts e="T735" id="Seg_4916" n="e" s="T734">(bătek) </ts>
               <ts e="T736" id="Seg_4918" n="e" s="T735">dĭzeŋ </ts>
               <ts e="T737" id="Seg_4920" n="e" s="T736">ugandə </ts>
               <ts e="T738" id="Seg_4922" n="e" s="T737">jakše. </ts>
               <ts e="T739" id="Seg_4924" n="e" s="T738">Tăŋ </ts>
               <ts e="T740" id="Seg_4926" n="e" s="T739">nendleʔbəʔjə </ts>
               <ts e="T741" id="Seg_4928" n="e" s="T740">i </ts>
               <ts e="T742" id="Seg_4930" n="e" s="T741">ejü </ts>
               <ts e="T743" id="Seg_4932" n="e" s="T742">dĭzeŋ </ts>
               <ts e="T744" id="Seg_4934" n="e" s="T743">(dĭmbi). </ts>
               <ts e="T745" id="Seg_4936" n="e" s="T744">Nuldliel </ts>
               <ts e="T746" id="Seg_4938" n="e" s="T745">mĭndərzittə </ts>
               <ts e="T747" id="Seg_4940" n="e" s="T746">(m) </ts>
               <ts e="T748" id="Seg_4942" n="e" s="T747">kös </ts>
               <ts e="T749" id="Seg_4944" n="e" s="T748">(ej=) </ts>
               <ts e="T750" id="Seg_4946" n="e" s="T749">ej </ts>
               <ts e="T751" id="Seg_4948" n="e" s="T750">păʔlia </ts>
               <ts e="T752" id="Seg_4950" n="e" s="T751">dibər. </ts>
               <ts e="T753" id="Seg_4952" n="e" s="T752">Ipadək </ts>
               <ts e="T754" id="Seg_4954" n="e" s="T753">ugandə </ts>
               <ts e="T755" id="Seg_4956" n="e" s="T754">kös </ts>
               <ts e="T756" id="Seg_4958" n="e" s="T755">iʔgö, </ts>
               <ts e="T757" id="Seg_4960" n="e" s="T756">(barəʔlia). </ts>
               <ts e="T758" id="Seg_4962" n="e" s="T757">Bobti </ts>
               <ts e="T759" id="Seg_4964" n="e" s="T758">măna </ts>
               <ts e="T760" id="Seg_4966" n="e" s="T759">Vasʼka </ts>
               <ts e="T761" id="Seg_4968" n="e" s="T760">Šajbin. </ts>
               <ts e="T762" id="Seg_4970" n="e" s="T761">Bobti </ts>
               <ts e="T763" id="Seg_4972" n="e" s="T762">tože </ts>
               <ts e="T764" id="Seg_4974" n="e" s="T763">tɨ. </ts>
               <ts e="T765" id="Seg_4976" n="e" s="T764">I </ts>
               <ts e="T766" id="Seg_4978" n="e" s="T765">(tănarbi) </ts>
               <ts e="T767" id="Seg_4980" n="e" s="T766">dĭ, </ts>
               <ts e="T768" id="Seg_4982" n="e" s="T767">a </ts>
               <ts e="T769" id="Seg_4984" n="e" s="T768">măn </ts>
               <ts e="T770" id="Seg_4986" n="e" s="T769">bospə </ts>
               <ts e="T771" id="Seg_4988" n="e" s="T770">embiem. </ts>
               <ts e="T772" id="Seg_4990" n="e" s="T771">Traktărzi </ts>
               <ts e="T773" id="Seg_4992" n="e" s="T772">deʔpiʔi. </ts>
               <ts e="T774" id="Seg_4994" n="e" s="T773">Kajak </ts>
               <ts e="T775" id="Seg_4996" n="e" s="T774">(ibiem) </ts>
               <ts e="T776" id="Seg_4998" n="e" s="T775">traktărdə. </ts>
               <ts e="T777" id="Seg_5000" n="e" s="T776">Tăŋ </ts>
               <ts e="T778" id="Seg_5002" n="e" s="T777">((…)), </ts>
               <ts e="T779" id="Seg_5004" n="e" s="T778">a </ts>
               <ts e="T780" id="Seg_5006" n="e" s="T779">măn </ts>
               <ts e="T781" id="Seg_5008" n="e" s="T780">bar </ts>
               <ts e="T782" id="Seg_5010" n="e" s="T781">pa </ts>
               <ts e="T783" id="Seg_5012" n="e" s="T782">tažerbiam, </ts>
               <ts e="T784" id="Seg_5014" n="e" s="T783">büjle </ts>
               <ts e="T785" id="Seg_5016" n="e" s="T784">mĭmbiem. </ts>
               <ts e="T786" id="Seg_5018" n="e" s="T785">Tüžöjdə </ts>
               <ts e="T787" id="Seg_5020" n="e" s="T786">nuʔ </ts>
               <ts e="T788" id="Seg_5022" n="e" s="T787">mĭbiem. </ts>
               <ts e="T789" id="Seg_5024" n="e" s="T788">Surdəbiam, </ts>
               <ts e="T790" id="Seg_5026" n="e" s="T789">dĭgəttə </ts>
               <ts e="T791" id="Seg_5028" n="e" s="T790">amnobiam </ts>
               <ts e="T792" id="Seg_5030" n="e" s="T791">amorzittə. </ts>
               <ts e="T793" id="Seg_5032" n="e" s="T792">I </ts>
               <ts e="T794" id="Seg_5034" n="e" s="T793">tenölaʔbəm. </ts>
               <ts e="T795" id="Seg_5036" n="e" s="T794">Dĭ </ts>
               <ts e="T796" id="Seg_5038" n="e" s="T795">Jelʼa </ts>
               <ts e="T797" id="Seg_5040" n="e" s="T796">măna </ts>
               <ts e="T798" id="Seg_5042" n="e" s="T797">bar </ts>
               <ts e="T799" id="Seg_5044" n="e" s="T798">šaːmbi. </ts>
               <ts e="T800" id="Seg_5046" n="e" s="T799">Aktʼat </ts>
               <ts e="T801" id="Seg_5048" n="e" s="T800">nagur </ts>
               <ts e="T802" id="Seg_5050" n="e" s="T801">šălkovej </ts>
               <ts e="T803" id="Seg_5052" n="e" s="T802">(b-) </ts>
               <ts e="T804" id="Seg_5054" n="e" s="T803">ibi. </ts>
               <ts e="T805" id="Seg_5056" n="e" s="T804">A </ts>
               <ts e="T806" id="Seg_5058" n="e" s="T805">măna </ts>
               <ts e="T807" id="Seg_5060" n="e" s="T806">ibi </ts>
               <ts e="T808" id="Seg_5062" n="e" s="T807">onʼiʔ </ts>
               <ts e="T809" id="Seg_5064" n="e" s="T808">šălkovej </ts>
               <ts e="T810" id="Seg_5066" n="e" s="T809">šide </ts>
               <ts e="T811" id="Seg_5068" n="e" s="T810">bʼeʔ </ts>
               <ts e="T812" id="Seg_5070" n="e" s="T811">sumna. </ts>
               <ts e="T813" id="Seg_5072" n="e" s="T812">Dĭʔnə </ts>
               <ts e="T814" id="Seg_5074" n="e" s="T813">(bɨl) </ts>
               <ts e="T815" id="Seg_5076" n="e" s="T814">izittə </ts>
               <ts e="T816" id="Seg_5078" n="e" s="T815">măna </ts>
               <ts e="T817" id="Seg_5080" n="e" s="T816">onʼiʔ </ts>
               <ts e="T818" id="Seg_5082" n="e" s="T817">šălkovej </ts>
               <ts e="T819" id="Seg_5084" n="e" s="T818">i </ts>
               <ts e="T820" id="Seg_5086" n="e" s="T819">bʼeʔ </ts>
               <ts e="T821" id="Seg_5088" n="e" s="T820">sumna </ts>
               <ts e="T822" id="Seg_5090" n="e" s="T821">kăpʼejkaʔi. </ts>
               <ts e="T823" id="Seg_5092" n="e" s="T822">Măn </ts>
               <ts e="T824" id="Seg_5094" n="e" s="T823">teinen </ts>
               <ts e="T825" id="Seg_5096" n="e" s="T824">bünə </ts>
               <ts e="T826" id="Seg_5098" n="e" s="T825">kambiam </ts>
               <ts e="T827" id="Seg_5100" n="e" s="T826">da </ts>
               <ts e="T828" id="Seg_5102" n="e" s="T827">ugandə </ts>
               <ts e="T829" id="Seg_5104" n="e" s="T828">tăŋ </ts>
               <ts e="T830" id="Seg_5106" n="e" s="T829">(kănnambi). </ts>
               <ts e="T831" id="Seg_5108" n="e" s="T830">Măn </ts>
               <ts e="T832" id="Seg_5110" n="e" s="T831">parbiam </ts>
               <ts e="T833" id="Seg_5112" n="e" s="T832">(baltu) </ts>
               <ts e="T834" id="Seg_5114" n="e" s="T833">ibiem, </ts>
               <ts e="T835" id="Seg_5116" n="e" s="T834">baltuzi </ts>
               <ts e="T836" id="Seg_5118" n="e" s="T835">jaʔpiam, </ts>
               <ts e="T837" id="Seg_5120" n="e" s="T836">jaʔpiam </ts>
               <ts e="T838" id="Seg_5122" n="e" s="T837">bar </ts>
               <ts e="T839" id="Seg_5124" n="e" s="T838">sĭre. </ts>
               <ts e="T840" id="Seg_5126" n="e" s="T839">Целый </ts>
               <ts e="T841" id="Seg_5128" n="e" s="T840">вершок </ts>
               <ts e="T842" id="Seg_5130" n="e" s="T841">kănnambi, </ts>
               <ts e="T843" id="Seg_5132" n="e" s="T842">ugandə </ts>
               <ts e="T844" id="Seg_5134" n="e" s="T843">šĭšəge </ts>
               <ts e="T845" id="Seg_5136" n="e" s="T844">erte </ts>
               <ts e="T846" id="Seg_5138" n="e" s="T845">ibi. </ts>
               <ts e="T847" id="Seg_5140" n="e" s="T846">Maʔnʼi </ts>
               <ts e="T848" id="Seg_5142" n="e" s="T847">šobiam, </ts>
               <ts e="T849" id="Seg_5144" n="e" s="T848">(šiʔnʼileʔ </ts>
               <ts e="T850" id="Seg_5146" n="e" s="T849">edəʔpiem) </ts>
               <ts e="T851" id="Seg_5148" n="e" s="T850">i </ts>
               <ts e="T852" id="Seg_5150" n="e" s="T851">bü </ts>
               <ts e="T853" id="Seg_5152" n="e" s="T852">deʔpiem, </ts>
               <ts e="T854" id="Seg_5154" n="e" s="T853">pʼeštə </ts>
               <ts e="T855" id="Seg_5156" n="e" s="T854">(še- </ts>
               <ts e="T856" id="Seg_5158" n="e" s="T855">s- </ts>
               <ts e="T857" id="Seg_5160" n="e" s="T856">šo-) </ts>
               <ts e="T858" id="Seg_5162" n="e" s="T857">sʼabiam. </ts>
               <ts e="T859" id="Seg_5164" n="e" s="T858">Amnobiam </ts>
               <ts e="T860" id="Seg_5166" n="e" s="T859">i </ts>
               <ts e="T861" id="Seg_5168" n="e" s="T860">(emneʔpəm). </ts>
               <ts e="T862" id="Seg_5170" n="e" s="T861">Dĭgəttə </ts>
               <ts e="T863" id="Seg_5172" n="e" s="T862">šobiʔi </ts>
               <ts e="T864" id="Seg_5174" n="e" s="T863">i </ts>
               <ts e="T865" id="Seg_5176" n="e" s="T864">((DMG)). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_5177" s="T0">PKZ_196X_SU0214.001 (001)</ta>
            <ta e="T10" id="Seg_5178" s="T4">PKZ_196X_SU0214.002 (002)</ta>
            <ta e="T12" id="Seg_5179" s="T10">PKZ_196X_SU0214.003 (003)</ta>
            <ta e="T17" id="Seg_5180" s="T12">PKZ_196X_SU0214.004 (004)</ta>
            <ta e="T19" id="Seg_5181" s="T17">PKZ_196X_SU0214.005 (005)</ta>
            <ta e="T22" id="Seg_5182" s="T19">PKZ_196X_SU0214.006 (006)</ta>
            <ta e="T25" id="Seg_5183" s="T22">PKZ_196X_SU0214.007 (007)</ta>
            <ta e="T27" id="Seg_5184" s="T25">PKZ_196X_SU0214.008 (008)</ta>
            <ta e="T29" id="Seg_5185" s="T27">PKZ_196X_SU0214.009 (009)</ta>
            <ta e="T32" id="Seg_5186" s="T29">PKZ_196X_SU0214.010 (010)</ta>
            <ta e="T36" id="Seg_5187" s="T32">PKZ_196X_SU0214.011 (011)</ta>
            <ta e="T38" id="Seg_5188" s="T36">PKZ_196X_SU0214.012 (012)</ta>
            <ta e="T41" id="Seg_5189" s="T38">PKZ_196X_SU0214.013 (013)</ta>
            <ta e="T44" id="Seg_5190" s="T41">PKZ_196X_SU0214.014 (014)</ta>
            <ta e="T46" id="Seg_5191" s="T44">PKZ_196X_SU0214.015 (015)</ta>
            <ta e="T52" id="Seg_5192" s="T46">PKZ_196X_SU0214.016 (016)</ta>
            <ta e="T58" id="Seg_5193" s="T52">PKZ_196X_SU0214.017 (017)</ta>
            <ta e="T72" id="Seg_5194" s="T58">PKZ_196X_SU0214.018 (018) </ta>
            <ta e="T77" id="Seg_5195" s="T72">PKZ_196X_SU0214.019 (020)</ta>
            <ta e="T79" id="Seg_5196" s="T77">PKZ_196X_SU0214.020 (021)</ta>
            <ta e="T87" id="Seg_5197" s="T79">PKZ_196X_SU0214.021 (022)</ta>
            <ta e="T93" id="Seg_5198" s="T87">PKZ_196X_SU0214.022 (023)</ta>
            <ta e="T98" id="Seg_5199" s="T93">PKZ_196X_SU0214.023 (024)</ta>
            <ta e="T101" id="Seg_5200" s="T98">PKZ_196X_SU0214.024 (025)</ta>
            <ta e="T107" id="Seg_5201" s="T101">PKZ_196X_SU0214.025 (026)</ta>
            <ta e="T112" id="Seg_5202" s="T107">PKZ_196X_SU0214.026 (027)</ta>
            <ta e="T115" id="Seg_5203" s="T112">PKZ_196X_SU0214.027 (028)</ta>
            <ta e="T121" id="Seg_5204" s="T115">PKZ_196X_SU0214.028 (029)</ta>
            <ta e="T127" id="Seg_5205" s="T121">PKZ_196X_SU0214.029 (030)</ta>
            <ta e="T130" id="Seg_5206" s="T127">PKZ_196X_SU0214.030 (031)</ta>
            <ta e="T133" id="Seg_5207" s="T130">PKZ_196X_SU0214.031 (032)</ta>
            <ta e="T135" id="Seg_5208" s="T133">PKZ_196X_SU0214.032 (033)</ta>
            <ta e="T139" id="Seg_5209" s="T135">PKZ_196X_SU0214.033 (034)</ta>
            <ta e="T145" id="Seg_5210" s="T139">PKZ_196X_SU0214.034 (035)</ta>
            <ta e="T151" id="Seg_5211" s="T145">PKZ_196X_SU0214.035 (036)</ta>
            <ta e="T154" id="Seg_5212" s="T151">PKZ_196X_SU0214.036 (037)</ta>
            <ta e="T157" id="Seg_5213" s="T154">PKZ_196X_SU0214.037 (038)</ta>
            <ta e="T158" id="Seg_5214" s="T157">PKZ_196X_SU0214.038 (039)</ta>
            <ta e="T163" id="Seg_5215" s="T158">PKZ_196X_SU0214.039 (040)</ta>
            <ta e="T166" id="Seg_5216" s="T163">PKZ_196X_SU0214.040 (041)</ta>
            <ta e="T169" id="Seg_5217" s="T166">PKZ_196X_SU0214.041 (042)</ta>
            <ta e="T177" id="Seg_5218" s="T169">PKZ_196X_SU0214.042 (043)</ta>
            <ta e="T185" id="Seg_5219" s="T177">PKZ_196X_SU0214.043 (044)</ta>
            <ta e="T187" id="Seg_5220" s="T185">PKZ_196X_SU0214.044 (045)</ta>
            <ta e="T190" id="Seg_5221" s="T187">PKZ_196X_SU0214.045 (046)</ta>
            <ta e="T192" id="Seg_5222" s="T190">PKZ_196X_SU0214.046 (047)</ta>
            <ta e="T196" id="Seg_5223" s="T192">PKZ_196X_SU0214.047 (048)</ta>
            <ta e="T200" id="Seg_5224" s="T196">PKZ_196X_SU0214.048 (049)</ta>
            <ta e="T204" id="Seg_5225" s="T200">PKZ_196X_SU0214.049 (050)</ta>
            <ta e="T207" id="Seg_5226" s="T204">PKZ_196X_SU0214.050 (051)</ta>
            <ta e="T221" id="Seg_5227" s="T207">PKZ_196X_SU0214.051 (052)</ta>
            <ta e="T227" id="Seg_5228" s="T221">PKZ_196X_SU0214.052 (053)</ta>
            <ta e="T229" id="Seg_5229" s="T227">PKZ_196X_SU0214.053 (054)</ta>
            <ta e="T233" id="Seg_5230" s="T229">PKZ_196X_SU0214.054 (055)</ta>
            <ta e="T237" id="Seg_5231" s="T233">PKZ_196X_SU0214.055 (056)</ta>
            <ta e="T239" id="Seg_5232" s="T237">PKZ_196X_SU0214.056 (057)</ta>
            <ta e="T245" id="Seg_5233" s="T239">PKZ_196X_SU0214.057 (058)</ta>
            <ta e="T251" id="Seg_5234" s="T245">PKZ_196X_SU0214.058 (059)</ta>
            <ta e="T254" id="Seg_5235" s="T251">PKZ_196X_SU0214.059 (060)</ta>
            <ta e="T260" id="Seg_5236" s="T254">PKZ_196X_SU0214.060 (061)</ta>
            <ta e="T265" id="Seg_5237" s="T260">PKZ_196X_SU0214.061 (062)</ta>
            <ta e="T270" id="Seg_5238" s="T265">PKZ_196X_SU0214.062 (063) </ta>
            <ta e="T273" id="Seg_5239" s="T270">PKZ_196X_SU0214.063 (065)</ta>
            <ta e="T275" id="Seg_5240" s="T273">PKZ_196X_SU0214.064 (066)</ta>
            <ta e="T276" id="Seg_5241" s="T275">PKZ_196X_SU0214.065 (067)</ta>
            <ta e="T278" id="Seg_5242" s="T276">PKZ_196X_SU0214.066 (068)</ta>
            <ta e="T281" id="Seg_5243" s="T278">PKZ_196X_SU0214.067 (069)</ta>
            <ta e="T285" id="Seg_5244" s="T281">PKZ_196X_SU0214.068 (070)</ta>
            <ta e="T289" id="Seg_5245" s="T285">PKZ_196X_SU0214.069 (071)</ta>
            <ta e="T293" id="Seg_5246" s="T289">PKZ_196X_SU0214.070 (072)</ta>
            <ta e="T300" id="Seg_5247" s="T293">PKZ_196X_SU0214.071 (073)</ta>
            <ta e="T307" id="Seg_5248" s="T300">PKZ_196X_SU0214.072 (074)</ta>
            <ta e="T314" id="Seg_5249" s="T307">PKZ_196X_SU0214.073 (075)</ta>
            <ta e="T322" id="Seg_5250" s="T314">PKZ_196X_SU0214.074 (076)</ta>
            <ta e="T327" id="Seg_5251" s="T322">PKZ_196X_SU0214.075 (077)</ta>
            <ta e="T331" id="Seg_5252" s="T327">PKZ_196X_SU0214.076 (078)</ta>
            <ta e="T334" id="Seg_5253" s="T331">PKZ_196X_SU0214.077 (079)</ta>
            <ta e="T341" id="Seg_5254" s="T334">PKZ_196X_SU0214.078 (080)</ta>
            <ta e="T344" id="Seg_5255" s="T341">PKZ_196X_SU0214.079 (081)</ta>
            <ta e="T346" id="Seg_5256" s="T344">PKZ_196X_SU0214.080 (082)</ta>
            <ta e="T353" id="Seg_5257" s="T346">PKZ_196X_SU0214.081 (083)</ta>
            <ta e="T358" id="Seg_5258" s="T353">PKZ_196X_SU0214.082 (084)</ta>
            <ta e="T360" id="Seg_5259" s="T358">PKZ_196X_SU0214.083 (085)</ta>
            <ta e="T361" id="Seg_5260" s="T360">PKZ_196X_SU0214.084 (086)</ta>
            <ta e="T368" id="Seg_5261" s="T361">PKZ_196X_SU0214.085 (087)</ta>
            <ta e="T374" id="Seg_5262" s="T368">PKZ_196X_SU0214.086 (088)</ta>
            <ta e="T377" id="Seg_5263" s="T374">PKZ_196X_SU0214.087 (089)</ta>
            <ta e="T380" id="Seg_5264" s="T377">PKZ_196X_SU0214.088 (090)</ta>
            <ta e="T389" id="Seg_5265" s="T380">PKZ_196X_SU0214.089 (091)</ta>
            <ta e="T392" id="Seg_5266" s="T389">PKZ_196X_SU0214.090 (092)</ta>
            <ta e="T399" id="Seg_5267" s="T392">PKZ_196X_SU0214.091 (093)</ta>
            <ta e="T405" id="Seg_5268" s="T399">PKZ_196X_SU0214.092 (094)</ta>
            <ta e="T412" id="Seg_5269" s="T405">PKZ_196X_SU0214.093 (095)</ta>
            <ta e="T415" id="Seg_5270" s="T412">PKZ_196X_SU0214.094 (096)</ta>
            <ta e="T419" id="Seg_5271" s="T415">PKZ_196X_SU0214.095 (097)</ta>
            <ta e="T425" id="Seg_5272" s="T419">PKZ_196X_SU0214.096 (098)</ta>
            <ta e="T429" id="Seg_5273" s="T425">PKZ_196X_SU0214.097 (099)</ta>
            <ta e="T434" id="Seg_5274" s="T429">PKZ_196X_SU0214.098 (100)</ta>
            <ta e="T437" id="Seg_5275" s="T434">PKZ_196X_SU0214.099 (101)</ta>
            <ta e="T439" id="Seg_5276" s="T437">PKZ_196X_SU0214.100 (102)</ta>
            <ta e="T442" id="Seg_5277" s="T439">PKZ_196X_SU0214.101 (103)</ta>
            <ta e="T448" id="Seg_5278" s="T442">PKZ_196X_SU0214.102 (104)</ta>
            <ta e="T453" id="Seg_5279" s="T448">PKZ_196X_SU0214.103 (105)</ta>
            <ta e="T461" id="Seg_5280" s="T453">PKZ_196X_SU0214.104 (106)</ta>
            <ta e="T468" id="Seg_5281" s="T461">PKZ_196X_SU0214.105 (107)</ta>
            <ta e="T473" id="Seg_5282" s="T468">PKZ_196X_SU0214.106 (108)</ta>
            <ta e="T478" id="Seg_5283" s="T473">PKZ_196X_SU0214.107 (109)</ta>
            <ta e="T484" id="Seg_5284" s="T478">PKZ_196X_SU0214.108 (110)</ta>
            <ta e="T491" id="Seg_5285" s="T484">PKZ_196X_SU0214.109 (111)</ta>
            <ta e="T502" id="Seg_5286" s="T491">PKZ_196X_SU0214.110 (112)</ta>
            <ta e="T506" id="Seg_5287" s="T502">PKZ_196X_SU0214.111 (113)</ta>
            <ta e="T517" id="Seg_5288" s="T506">PKZ_196X_SU0214.112 (114)</ta>
            <ta e="T520" id="Seg_5289" s="T517">PKZ_196X_SU0214.113 (115)</ta>
            <ta e="T523" id="Seg_5290" s="T520">PKZ_196X_SU0214.114 (116)</ta>
            <ta e="T531" id="Seg_5291" s="T523">PKZ_196X_SU0214.115 (117) </ta>
            <ta e="T539" id="Seg_5292" s="T531">PKZ_196X_SU0214.116 (119)</ta>
            <ta e="T542" id="Seg_5293" s="T539">PKZ_196X_SU0214.117 (120)</ta>
            <ta e="T544" id="Seg_5294" s="T542">PKZ_196X_SU0214.118 (121)</ta>
            <ta e="T547" id="Seg_5295" s="T544">PKZ_196X_SU0214.119 (122)</ta>
            <ta e="T550" id="Seg_5296" s="T547">PKZ_196X_SU0214.120 (123)</ta>
            <ta e="T556" id="Seg_5297" s="T550">PKZ_196X_SU0214.121 (124)</ta>
            <ta e="T559" id="Seg_5298" s="T556">PKZ_196X_SU0214.122 (125)</ta>
            <ta e="T566" id="Seg_5299" s="T559">PKZ_196X_SU0214.123 (126)</ta>
            <ta e="T574" id="Seg_5300" s="T566">PKZ_196X_SU0214.124 (127)</ta>
            <ta e="T579" id="Seg_5301" s="T574">PKZ_196X_SU0214.125 (128)</ta>
            <ta e="T582" id="Seg_5302" s="T579">PKZ_196X_SU0214.126 (129)</ta>
            <ta e="T584" id="Seg_5303" s="T582">PKZ_196X_SU0214.127 (130)</ta>
            <ta e="T586" id="Seg_5304" s="T584">PKZ_196X_SU0214.128 (131)</ta>
            <ta e="T590" id="Seg_5305" s="T586">PKZ_196X_SU0214.129 (132)</ta>
            <ta e="T594" id="Seg_5306" s="T590">PKZ_196X_SU0214.130 (133)</ta>
            <ta e="T597" id="Seg_5307" s="T594">PKZ_196X_SU0214.131 (134)</ta>
            <ta e="T607" id="Seg_5308" s="T597">PKZ_196X_SU0214.132 (135)</ta>
            <ta e="T611" id="Seg_5309" s="T607">PKZ_196X_SU0214.133 (136)</ta>
            <ta e="T614" id="Seg_5310" s="T611">PKZ_196X_SU0214.134 (137)</ta>
            <ta e="T619" id="Seg_5311" s="T614">PKZ_196X_SU0214.135 (138)</ta>
            <ta e="T626" id="Seg_5312" s="T619">PKZ_196X_SU0214.136 (139)</ta>
            <ta e="T628" id="Seg_5313" s="T626">PKZ_196X_SU0214.137 (140)</ta>
            <ta e="T632" id="Seg_5314" s="T628">PKZ_196X_SU0214.138 (141)</ta>
            <ta e="T642" id="Seg_5315" s="T632">PKZ_196X_SU0214.139 (142)</ta>
            <ta e="T646" id="Seg_5316" s="T642">PKZ_196X_SU0214.140 (143)</ta>
            <ta e="T648" id="Seg_5317" s="T646">PKZ_196X_SU0214.141 (144)</ta>
            <ta e="T651" id="Seg_5318" s="T648">PKZ_196X_SU0214.142 (145)</ta>
            <ta e="T654" id="Seg_5319" s="T651">PKZ_196X_SU0214.143 (146)</ta>
            <ta e="T657" id="Seg_5320" s="T654">PKZ_196X_SU0214.144 (147)</ta>
            <ta e="T663" id="Seg_5321" s="T657">PKZ_196X_SU0214.145 (148)</ta>
            <ta e="T669" id="Seg_5322" s="T663">PKZ_196X_SU0214.146 (149)</ta>
            <ta e="T672" id="Seg_5323" s="T669">PKZ_196X_SU0214.147 (150)</ta>
            <ta e="T678" id="Seg_5324" s="T672">PKZ_196X_SU0214.148 (151)</ta>
            <ta e="T684" id="Seg_5325" s="T678">PKZ_196X_SU0214.149 (152)</ta>
            <ta e="T691" id="Seg_5326" s="T684">PKZ_196X_SU0214.150 (153)</ta>
            <ta e="T698" id="Seg_5327" s="T691">PKZ_196X_SU0214.151 (154)</ta>
            <ta e="T703" id="Seg_5328" s="T698">PKZ_196X_SU0214.152 (155)</ta>
            <ta e="T707" id="Seg_5329" s="T703">PKZ_196X_SU0214.153 (156)</ta>
            <ta e="T710" id="Seg_5330" s="T707">PKZ_196X_SU0214.154 (157)</ta>
            <ta e="T714" id="Seg_5331" s="T710">PKZ_196X_SU0214.155 (158)</ta>
            <ta e="T716" id="Seg_5332" s="T714">PKZ_196X_SU0214.156 (159)</ta>
            <ta e="T719" id="Seg_5333" s="T716">PKZ_196X_SU0214.157 (160)</ta>
            <ta e="T724" id="Seg_5334" s="T719">PKZ_196X_SU0214.158 (161)</ta>
            <ta e="T727" id="Seg_5335" s="T724">PKZ_196X_SU0214.159 (162)</ta>
            <ta e="T731" id="Seg_5336" s="T727">PKZ_196X_SU0214.160 (163)</ta>
            <ta e="T738" id="Seg_5337" s="T731">PKZ_196X_SU0214.161 (164)</ta>
            <ta e="T744" id="Seg_5338" s="T738">PKZ_196X_SU0214.162 (165)</ta>
            <ta e="T752" id="Seg_5339" s="T744">PKZ_196X_SU0214.163 (166)</ta>
            <ta e="T757" id="Seg_5340" s="T752">PKZ_196X_SU0214.164 (167)</ta>
            <ta e="T761" id="Seg_5341" s="T757">PKZ_196X_SU0214.165 (168)</ta>
            <ta e="T764" id="Seg_5342" s="T761">PKZ_196X_SU0214.166 (169)</ta>
            <ta e="T771" id="Seg_5343" s="T764">PKZ_196X_SU0214.167 (170)</ta>
            <ta e="T773" id="Seg_5344" s="T771">PKZ_196X_SU0214.168 (171)</ta>
            <ta e="T776" id="Seg_5345" s="T773">PKZ_196X_SU0214.169 (172)</ta>
            <ta e="T785" id="Seg_5346" s="T776">PKZ_196X_SU0214.170 (173)</ta>
            <ta e="T788" id="Seg_5347" s="T785">PKZ_196X_SU0214.171 (174)</ta>
            <ta e="T792" id="Seg_5348" s="T788">PKZ_196X_SU0214.172 (175)</ta>
            <ta e="T794" id="Seg_5349" s="T792">PKZ_196X_SU0214.173 (176)</ta>
            <ta e="T799" id="Seg_5350" s="T794">PKZ_196X_SU0214.174 (177)</ta>
            <ta e="T804" id="Seg_5351" s="T799">PKZ_196X_SU0214.175 (178)</ta>
            <ta e="T812" id="Seg_5352" s="T804">PKZ_196X_SU0214.176 (179)</ta>
            <ta e="T822" id="Seg_5353" s="T812">PKZ_196X_SU0214.177 (180)</ta>
            <ta e="T830" id="Seg_5354" s="T822">PKZ_196X_SU0214.178 (181)</ta>
            <ta e="T839" id="Seg_5355" s="T830">PKZ_196X_SU0214.179 (182)</ta>
            <ta e="T846" id="Seg_5356" s="T839">PKZ_196X_SU0214.180 (183)</ta>
            <ta e="T858" id="Seg_5357" s="T846">PKZ_196X_SU0214.181 (184)</ta>
            <ta e="T861" id="Seg_5358" s="T858">PKZ_196X_SU0214.182 (185)</ta>
            <ta e="T865" id="Seg_5359" s="T861">PKZ_196X_SU0214.183 (186)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_5360" s="T0">Miʔ bar (Sverdlovskagən) amnobiaʔ. </ta>
            <ta e="T10" id="Seg_5361" s="T4">(Sujo= sujo= bazaj=) Bazaj süjönə amnobibaʔ. </ta>
            <ta e="T12" id="Seg_5362" s="T10">Dĭgəttə nʼergöbibaʔ. </ta>
            <ta e="T17" id="Seg_5363" s="T12">Aktʼigən ej molia (nʼ-) nʼergözittə. </ta>
            <ta e="T19" id="Seg_5364" s="T17">(Berzəbi) bar. </ta>
            <ta e="T22" id="Seg_5365" s="T19">Туман (nuʔ-) nulaʔbə. </ta>
            <ta e="T25" id="Seg_5366" s="T22">Dĭgəttə šobibaʔ Krasnojarskənə. </ta>
            <ta e="T27" id="Seg_5367" s="T25">Dĭn amnəbibaʔ. </ta>
            <ta e="T29" id="Seg_5368" s="T27">Dĭn amnobiam. </ta>
            <ta e="T32" id="Seg_5369" s="T29">Sejʔpü dʼala amnobiam. </ta>
            <ta e="T36" id="Seg_5370" s="T32">Dĭgəttə amnobiam bazaj (aʔtʼinə). </ta>
            <ta e="T38" id="Seg_5371" s="T36">Šobiam Ujardə. </ta>
            <ta e="T41" id="Seg_5372" s="T38">Dĭgəttə Ujargəʔ šobiam. </ta>
            <ta e="T44" id="Seg_5373" s="T41">(Amnobi-) Amnobiam avtobustə. </ta>
            <ta e="T46" id="Seg_5374" s="T44">Šobiam Săjanskən. </ta>
            <ta e="T52" id="Seg_5375" s="T46">Dön amnobiam (четыре) (nagu-) šide dʼala. </ta>
            <ta e="T58" id="Seg_5376" s="T52">Dĭgəttə Abalakoftə šobiam i tănan šobiam. </ta>
            <ta e="T72" id="Seg_5377" s="T58">Tănan šobiam, dʼăbaktərbibaʔ, dĭgəttə tăn (măn=) măna (măm-) (măllal): "Amnaʔ, amorgaʔ, (tu-) püjölial padʼi-ka". </ta>
            <ta e="T77" id="Seg_5378" s="T72">Dĭgəttə măn amnobiam, amorbiam, kambiam. </ta>
            <ta e="T79" id="Seg_5379" s="T77">Bostə (fatʼeratsi). </ta>
            <ta e="T87" id="Seg_5380" s="T79">(Onʼiʔ=) Onʼiʔ raz miʔ kambibaʔ, nagur ne, Zaozʼorkanə. </ta>
            <ta e="T93" id="Seg_5381" s="T87">Dĭgəttə dĭʔə bazaj aʔtʼinə (am-) amnobibaʔ. </ta>
            <ta e="T98" id="Seg_5382" s="T93">Kambibaʔ Ujardə, dĭn kudajdə numan üzəsʼtə. </ta>
            <ta e="T101" id="Seg_5383" s="T98">Dĭn kudajdə numan üzəleʔpibeʔ. </ta>
            <ta e="T107" id="Seg_5384" s="T101">Dĭgəttə parluʔpibaʔ (bazoʔ) (s-) döbər Zaozʼorkanə. </ta>
            <ta e="T112" id="Seg_5385" s="T107">(Dĭn) Dön šabiaʔ, iʔgö dʼăbaktərlaʔpibeʔ. </ta>
            <ta e="T115" id="Seg_5386" s="T112">Dĭgəttə (kozʼa) kambi. </ta>
            <ta e="T121" id="Seg_5387" s="T115">Avtobus măndərzittə šobi da (măndede) kalla dʼürbi. </ta>
            <ta e="T127" id="Seg_5388" s="T121">A miʔ oʔbdəbibaʔ da kambibaʔ nʼiʔdə. </ta>
            <ta e="T130" id="Seg_5389" s="T127">Dön avtobus šonəga. </ta>
            <ta e="T133" id="Seg_5390" s="T130">Amnəldəbi miʔ (da). </ta>
            <ta e="T135" id="Seg_5391" s="T133">Perejaslăvkanə šobibaʔ. </ta>
            <ta e="T139" id="Seg_5392" s="T135">Dĭn (нас=) miʔnʼibeʔ … </ta>
            <ta e="T145" id="Seg_5393" s="T139">((DMG)) ((Dĭn miʔ)) Dĭn üzəbibeʔ i nulabibaʔ. </ta>
            <ta e="T151" id="Seg_5394" s="T145">Măn măndəm:" Ĭmbi nulaʔsittə, kanžəbaʔ, üdʼiʔ!" </ta>
            <ta e="T154" id="Seg_5395" s="T151">(Dĭgəttə) (kanžəbaʔ=) kandlaʔbəbaʔ. </ta>
            <ta e="T157" id="Seg_5396" s="T154">Mašina bar šonəga. </ta>
            <ta e="T158" id="Seg_5397" s="T157">"Amnogaʔ!" </ta>
            <ta e="T163" id="Seg_5398" s="T158">(Miʔ= amne- amno-)" Amnogaʔ döber!" </ta>
            <ta e="T166" id="Seg_5399" s="T163">Dĭgəttə miʔ amnobibaʔ. </ta>
            <ta e="T169" id="Seg_5400" s="T166">I šobibaʔ Kandəganə. </ta>
            <ta e="T177" id="Seg_5401" s="T169">Dĭgəttə dĭn iššo sejʔpü верста maluʔpi Unʼerdə kanzittə. </ta>
            <ta e="T185" id="Seg_5402" s="T177">Šide nezeŋ kambiʔi turanə, a măn kambiam üdʼiʔ. </ta>
            <ta e="T187" id="Seg_5403" s="T185">Unʼerdə šobiam. </ta>
            <ta e="T190" id="Seg_5404" s="T187">"Deʔkeʔ măna ipek. </ta>
            <ta e="T192" id="Seg_5405" s="T190">Sadargaʔ ipek". </ta>
            <ta e="T196" id="Seg_5406" s="T192">Dĭzeŋ măndə: "(Ka-) Kanaʔ. </ta>
            <ta e="T200" id="Seg_5407" s="T196">Măgăzində (de-) dĭn iʔ". </ta>
            <ta e="T204" id="Seg_5408" s="T200">"Măna iʔgö ej kereʔ. </ta>
            <ta e="T207" id="Seg_5409" s="T204">Šide gram mĭbileʔ". </ta>
            <ta e="T221" id="Seg_5410" s="T207">Dĭ măna šide gram edəbi, măn ibiem, aktʼa mĭbiem, ambiam i kambiam, bü bĭʔpiem. </ta>
            <ta e="T227" id="Seg_5411" s="T221">Dĭgəttə mašina bĭdəbi, măn (amno-) amnobiam. </ta>
            <ta e="T229" id="Seg_5412" s="T227">Dĭgəttə üzəbiem. </ta>
            <ta e="T233" id="Seg_5413" s="T229">Šide kuza nugaʔi măjagən. </ta>
            <ta e="T237" id="Seg_5414" s="T233">Măn dĭbər (sa-) sʼabiam. </ta>
            <ta e="T239" id="Seg_5415" s="T237">"Kanžəbəj (obbere)". </ta>
            <ta e="T245" id="Seg_5416" s="T239">(Dʼok) miʔ edəʔleʔbə mašina, mašinaziʔ kanžəbəj. </ta>
            <ta e="T251" id="Seg_5417" s="T245">Dĭgəttə mašina šobi, (amnobiaʔ- amnobiaʔ-) amnobiam. </ta>
            <ta e="T254" id="Seg_5418" s="T251">I Săjanskəjnə šobiam. </ta>
            <ta e="T260" id="Seg_5419" s="T254">Dĭgəttə (nĭn) (ibem-) ibiem ipek, sĭreʔpne. </ta>
            <ta e="T265" id="Seg_5420" s="T260">Vanʼka bălʼnʼitsagən iʔbolaʔpi, kumbiam dĭʔnə. </ta>
            <ta e="T270" id="Seg_5421" s="T265">Măna dĭn nörbəlaʔbəʔjə: "Dön igeʔ. </ta>
            <ta e="T273" id="Seg_5422" s="T270">Miʔ mašina kanaʔ. </ta>
            <ta e="T275" id="Seg_5423" s="T273">Măn nuʔməbiem. </ta>
            <ta e="T276" id="Seg_5424" s="T275">Gijendə. </ta>
            <ta e="T278" id="Seg_5425" s="T276">Ej kubiam. </ta>
            <ta e="T281" id="Seg_5426" s="T278">Dĭgəttə kambiam üdʼiʔ. </ta>
            <ta e="T285" id="Seg_5427" s="T281">Dĭgəttə bazo mašina šonəga. </ta>
            <ta e="T289" id="Seg_5428" s="T285">Măn amnobiam, Malinovkinə šobiam. </ta>
            <ta e="T293" id="Seg_5429" s="T289">Dĭgəttə bazo üjüzi kambiam. </ta>
            <ta e="T300" id="Seg_5430" s="T293">Dĭgəttə bazo mašina bĭdəbi i maːndə šobiam. </ta>
            <ta e="T307" id="Seg_5431" s="T300">Kamən Matvejev šobi măna, (dʼăbaktərla-) dʼăbaktərlaʔbə nudla. </ta>
            <ta e="T314" id="Seg_5432" s="T307">A măn (kazak=) kazak dʼăbaktərlaʔbəm i (petʼerlaʔbəm). </ta>
            <ta e="T322" id="Seg_5433" s="T314">Tăn tüj nu igel, a măn kazak igem. </ta>
            <ta e="T327" id="Seg_5434" s="T322">Dĭgəttə dĭzeŋ dʼijenə kambiʔi, Dʼelamdə. </ta>
            <ta e="T331" id="Seg_5435" s="T327">(Bi-) Kola dʼaʔpiʔi dĭn. </ta>
            <ta e="T334" id="Seg_5436" s="T331">Bi idʼem enbiʔi. </ta>
            <ta e="T341" id="Seg_5437" s="T334">Dĭgəttə onʼiʔ koʔbdo urgo bar ĭzemnuʔpi bar. </ta>
            <ta e="T344" id="Seg_5438" s="T341">Dĭgəttə (dĭzeŋ) šobiʔi. </ta>
            <ta e="T346" id="Seg_5439" s="T344">Turanə Kanoklerdə. </ta>
            <ta e="T353" id="Seg_5440" s="T346">I ne ibiʔi i dĭm deʔpiʔi bălʼnʼitsanə. </ta>
            <ta e="T358" id="Seg_5441" s="T353">Tĭn üjün bar (bü bĭʔpiʔi). </ta>
            <ta e="T360" id="Seg_5442" s="T358">I dʼubtəbiʔi. </ta>
            <ta e="T361" id="Seg_5443" s="T360">Dʼazirbiʔi. </ta>
            <ta e="T368" id="Seg_5444" s="T361">A to dĭ mĭnzittə (ej m-) ej molia. </ta>
            <ta e="T374" id="Seg_5445" s="T368">Măn (măndər- măn-) măndərbiam, măndərbiam (правда). </ta>
            <ta e="T377" id="Seg_5446" s="T374">Dĭbər (pʼaŋdəbiam sazən). </ta>
            <ta e="T380" id="Seg_5447" s="T377">Dĭbər (pəŋ-) pʼaŋdəbiam. </ta>
            <ta e="T389" id="Seg_5448" s="T380">Šindi măn də kulambi, šindi măn də ej kulambi. </ta>
            <ta e="T392" id="Seg_5449" s="T389">Gijendə naga pravdaʔi. </ta>
            <ta e="T399" id="Seg_5450" s="T392">A măn tenəbiem (ige=) vezʼdʼe pravda ige. </ta>
            <ta e="T405" id="Seg_5451" s="T399">A dĭm gijendə naga, üge (šanaklʼu). </ta>
            <ta e="T412" id="Seg_5452" s="T405">Dĭgəttə prokuror pʼaŋdəbi, (gibərdə) kalla dʼürbi, ej tĭmnem. </ta>
            <ta e="T415" id="Seg_5453" s="T412">Jelʼa (kuʔkubi dʼodunʼim). </ta>
            <ta e="T419" id="Seg_5454" s="T415">Bar lotkanə bünə kambiʔi. </ta>
            <ta e="T425" id="Seg_5455" s="T419">(Dĭgəttə) girgitdə koʔbdozi xatʼeli bügən плавать. </ta>
            <ta e="T429" id="Seg_5456" s="T425">A bü ugandə urgo. </ta>
            <ta e="T434" id="Seg_5457" s="T429">Dĭzeŋ bar xatʼeli lotkanə sʼazittə. </ta>
            <ta e="T437" id="Seg_5458" s="T434">Da bar nereʔluʔpiʔi. </ta>
            <ta e="T439" id="Seg_5459" s="T437">Dĭgəttə … </ta>
            <ta e="T442" id="Seg_5460" s="T439">Dĭgəttə dĭn (nereʔluʔpiʔi). </ta>
            <ta e="T448" id="Seg_5461" s="T442">Oʔbdəbi, (kuiol-) kuliat što dʼoduni kübi. </ta>
            <ta e="T453" id="Seg_5462" s="T448">(Sălămatən) Mitrăfanən turat iššo nuga. </ta>
            <ta e="T461" id="Seg_5463" s="T453">(I ku-) I gurun Andʼigatovan tura iššo nuga. </ta>
            <ta e="T468" id="Seg_5464" s="T461">(Dʼibiai- ən-) Dʼibiev Vălodʼan tura iššo nuga. </ta>
            <ta e="T473" id="Seg_5465" s="T468">Kotʼerov Mikitan tura iššo nuga. </ta>
            <ta e="T478" id="Seg_5466" s="T473">(Gateva) Afanasin tura iššo nuga. </ta>
            <ta e="T484" id="Seg_5467" s="T478">I Šajbin Vlastə iššo tura nuga. </ta>
            <ta e="T491" id="Seg_5468" s="T484">I (Solomatov=) (Sălămatov) Vasilij iššo tura (nuga). </ta>
            <ta e="T502" id="Seg_5469" s="T491">Kamən Tugarin šobi dʼăbaktərzittə döbər, dön (su-) sumna bʼeʔ tura ibi. </ta>
            <ta e="T506" id="Seg_5470" s="T502">(Sargolov) Jakov (Filippovič) külaːmbi. </ta>
            <ta e="T517" id="Seg_5471" s="T506">Dĭ kambi Permʼakovtə, dĭn ara bĭʔpi, dĭn (i-) ibi onʼiʔ бутылка. </ta>
            <ta e="T520" id="Seg_5472" s="T517">I netsi šobi. </ta>
            <ta e="T523" id="Seg_5473" s="T520">Dĭgəttə kambi krospaʔinə. </ta>
            <ta e="T531" id="Seg_5474" s="T523">Dĭgəttə kămlia da (kudonzlia): "Bĭdeʔ nükem, bĭdeʔ nükem!" </ta>
            <ta e="T539" id="Seg_5475" s="T531">A măn abam nuga da măndolaʔbə da kaknarlaʔbə. </ta>
            <ta e="T542" id="Seg_5476" s="T539">Măn noʔ jaʔpiam. </ta>
            <ta e="T544" id="Seg_5477" s="T542">(Dĭgəttə) oʔbdəbiam. </ta>
            <ta e="T547" id="Seg_5478" s="T544">Dĭgəttə stogdə embiem. </ta>
            <ta e="T550" id="Seg_5479" s="T547">Dĭ dʼüʔpi ibi. </ta>
            <ta e="T556" id="Seg_5480" s="T550">Dĭgəttə nagur (dʼara-) dʼala ej kambiam. </ta>
            <ta e="T559" id="Seg_5481" s="T556">Ugandə dʼibige molambi. </ta>
            <ta e="T566" id="Seg_5482" s="T559">(Mɨ- ši-) Măn šide ne ibiem, kambiam. </ta>
            <ta e="T574" id="Seg_5483" s="T566">Bar kobi da bazoʔ embibeʔ kuvas (zəʔ-) (stog). </ta>
            <ta e="T579" id="Seg_5484" s="T574">Măn amnobiam šide bʼeʔ pʼe. </ta>
            <ta e="T582" id="Seg_5485" s="T579">Bospə noʔ jaʔpiam. </ta>
            <ta e="T584" id="Seg_5486" s="T582">Bostə oʔbdəbiam. </ta>
            <ta e="T586" id="Seg_5487" s="T584">Bostə embiem. </ta>
            <ta e="T590" id="Seg_5488" s="T586">Inezi maʔnʼi tažerbiam bar. </ta>
            <ta e="T594" id="Seg_5489" s="T590">(Tüžöjʔim) (im) (bĭd-) bădlaʔpiam. </ta>
            <ta e="T597" id="Seg_5490" s="T594">Onʼiʔ ine ibiem. </ta>
            <ta e="T607" id="Seg_5491" s="T597">Dĭgəttə plemʼannicat (nʼin) ibiem da iššo (dʼügej) kambibaʔ, noʔ embibeʔ. </ta>
            <ta e="T611" id="Seg_5492" s="T607">Dĭgəttə šobibaʔ, šobibaʔ miʔ. </ta>
            <ta e="T614" id="Seg_5493" s="T611">Noʔ (üzə-) üzəbi (dʼuno). </ta>
            <ta e="T619" id="Seg_5494" s="T614">Dĭgəttə dʼildəbibeʔ, dʼildəbibeʔ, ej moliabaʔ. </ta>
            <ta e="T626" id="Seg_5495" s="T619">(Dĭ) măndə:" Tüjö ine (kor- kor-) (körerbim). </ta>
            <ta e="T628" id="Seg_5496" s="T626">Dĭgəttə dʼildlim". </ta>
            <ta e="T632" id="Seg_5497" s="T628">"A kăde (tăn) dʼildlil?" </ta>
            <ta e="T642" id="Seg_5498" s="T632">A (dăre) bar ((…)) (barəʔpi) (oršaʔi) i dĭgəttə ine (dʼi). </ta>
            <ta e="T646" id="Seg_5499" s="T642">Dĭgəttə (maʔ) небось šobibaʔ. </ta>
            <ta e="T648" id="Seg_5500" s="T646">Mĭmbibeʔ noʔ. </ta>
            <ta e="T651" id="Seg_5501" s="T648">Dĭgəttə inem (kobmbibaʔ). </ta>
            <ta e="T654" id="Seg_5502" s="T651">Măn ugandə kămbiam. </ta>
            <ta e="T657" id="Seg_5503" s="T654">Dĭgəttə kašelʼ šobi. </ta>
            <ta e="T663" id="Seg_5504" s="T657">Dĭgəttə măn sĭreʔpne bar nendəbiem aranə. </ta>
            <ta e="T669" id="Seg_5505" s="T663">Dĭgəttə bĭʔpiem, i gibərdə kalaj kašelʼbə. </ta>
            <ta e="T672" id="Seg_5506" s="T669">Не стало кашля. </ta>
            <ta e="T678" id="Seg_5507" s="T672">((DMG)) Girgit (noʔ i-) noʔ ige, крапива. </ta>
            <ta e="T684" id="Seg_5508" s="T678">Dĭ dudkanə izittə nada, nʼiʔsittə dĭm. </ta>
            <ta e="T691" id="Seg_5509" s="T684">(Əbərdə bər) amnosʼtə, (dĭgəttə) ej moləj kašelʼ. </ta>
            <ta e="T698" id="Seg_5510" s="T691">Nendluʔpi i (bər) naga, i ejü naga. </ta>
            <ta e="T703" id="Seg_5511" s="T698">Paʔi măna teinen ej toʔnarzittə. </ta>
            <ta e="T707" id="Seg_5512" s="T703">Kabarləj (mə-) dö nüdʼinə. </ta>
            <ta e="T710" id="Seg_5513" s="T707">Paʔi măn iʔgö. </ta>
            <ta e="T714" id="Seg_5514" s="T710">(Dö) pʼenə iššo kabarləj. </ta>
            <ta e="T716" id="Seg_5515" s="T714">Šĭšəge ugandə. </ta>
            <ta e="T719" id="Seg_5516" s="T716">Iʔgö pa kandəgaʔi. </ta>
            <ta e="T724" id="Seg_5517" s="T719">Paʔi bar surnogə sagər mobiʔi. </ta>
            <ta e="T727" id="Seg_5518" s="T724">(A to) sĭre ibiʔi. </ta>
            <ta e="T731" id="Seg_5519" s="T727">(Măn) (büttona) šomi paʔi. </ta>
            <ta e="T738" id="Seg_5520" s="T731">Sĭre paʔi bar (bătek) dĭzeŋ ugandə jakše. </ta>
            <ta e="T744" id="Seg_5521" s="T738">Tăŋ nendleʔbəʔjə i ejü dĭzeŋ (dĭmbi). </ta>
            <ta e="T752" id="Seg_5522" s="T744">Nuldliel mĭndərzittə (m) kös (ej=) ej păʔlia dibər. </ta>
            <ta e="T757" id="Seg_5523" s="T752">Ipadək ugandə kös iʔgö, (barəʔlia). </ta>
            <ta e="T761" id="Seg_5524" s="T757">Bobti măna Vasʼka Šajbin. </ta>
            <ta e="T764" id="Seg_5525" s="T761">Bobti tože tɨ. </ta>
            <ta e="T771" id="Seg_5526" s="T764">I (tănarbi) dĭ, a măn bospə embiem. </ta>
            <ta e="T773" id="Seg_5527" s="T771">Traktărzi deʔpiʔi. </ta>
            <ta e="T776" id="Seg_5528" s="T773">Kajak (ibiem) traktărdə. </ta>
            <ta e="T785" id="Seg_5529" s="T776">Tăŋ ((…)), a măn bar pa tažerbiam, büjle mĭmbiem. </ta>
            <ta e="T788" id="Seg_5530" s="T785">Tüžöjdə nuʔ mĭbiem. </ta>
            <ta e="T792" id="Seg_5531" s="T788">Surdəbiam, dĭgəttə amnobiam amorzittə. </ta>
            <ta e="T794" id="Seg_5532" s="T792">I tenölaʔbəm. </ta>
            <ta e="T799" id="Seg_5533" s="T794">Dĭ Jelʼa măna bar šaːmbi. </ta>
            <ta e="T804" id="Seg_5534" s="T799">Aktʼat nagur šălkovej (b-) ibi. </ta>
            <ta e="T812" id="Seg_5535" s="T804">A măna ibi onʼiʔ šălkovej šide bʼeʔ sumna. ((DMG))</ta>
            <ta e="T822" id="Seg_5536" s="T812">Dĭʔnə (bɨl) izittə măna onʼiʔ šălkovej i bʼeʔ sumna kăpʼejkaʔi. </ta>
            <ta e="T830" id="Seg_5537" s="T822">Măn teinen bünə kambiam da ugandə tăŋ (kănnambi). </ta>
            <ta e="T839" id="Seg_5538" s="T830">Măn parbiam (baltu) ibiem, baltuzi jaʔpiam, jaʔpiam bar sĭre. </ta>
            <ta e="T846" id="Seg_5539" s="T839">Целый вершок kănnambi, ugandə šĭšəge erte ibi. </ta>
            <ta e="T858" id="Seg_5540" s="T846">Maʔnʼi šobiam, (šiʔnʼileʔ edəʔpiem) i bü deʔpiem, pʼeštə (še- s- šo-) sʼabiam. </ta>
            <ta e="T861" id="Seg_5541" s="T858">Amnobiam i (emneʔpəm). </ta>
            <ta e="T865" id="Seg_5542" s="T861">Dĭgəttə šobiʔi i ((DMG)). </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_5543" s="T0">miʔ</ta>
            <ta e="T2" id="Seg_5544" s="T1">bar</ta>
            <ta e="T3" id="Seg_5545" s="T2">Sverdlovska-gən</ta>
            <ta e="T4" id="Seg_5546" s="T3">amno-biaʔ</ta>
            <ta e="T7" id="Seg_5547" s="T6">baza-j</ta>
            <ta e="T8" id="Seg_5548" s="T7">baza-j</ta>
            <ta e="T9" id="Seg_5549" s="T8">süjö-nə</ta>
            <ta e="T10" id="Seg_5550" s="T9">amno-bi-baʔ</ta>
            <ta e="T11" id="Seg_5551" s="T10">dĭgəttə</ta>
            <ta e="T12" id="Seg_5552" s="T11">nʼergö-bi-baʔ</ta>
            <ta e="T13" id="Seg_5553" s="T12">aktʼi-gən</ta>
            <ta e="T14" id="Seg_5554" s="T13">ej</ta>
            <ta e="T15" id="Seg_5555" s="T14">mo-lia</ta>
            <ta e="T17" id="Seg_5556" s="T16">nʼergö-zittə</ta>
            <ta e="T18" id="Seg_5557" s="T17">ber-zəbi</ta>
            <ta e="T19" id="Seg_5558" s="T18">bar</ta>
            <ta e="T22" id="Seg_5559" s="T21">nu-laʔbə</ta>
            <ta e="T23" id="Seg_5560" s="T22">dĭgəttə</ta>
            <ta e="T24" id="Seg_5561" s="T23">šo-bi-baʔ</ta>
            <ta e="T25" id="Seg_5562" s="T24">Krasnojarskə-nə</ta>
            <ta e="T26" id="Seg_5563" s="T25">dĭn</ta>
            <ta e="T27" id="Seg_5564" s="T26">amnə-bi-baʔ</ta>
            <ta e="T28" id="Seg_5565" s="T27">dĭn</ta>
            <ta e="T29" id="Seg_5566" s="T28">amno-bia-m</ta>
            <ta e="T30" id="Seg_5567" s="T29">sejʔpü</ta>
            <ta e="T31" id="Seg_5568" s="T30">dʼala</ta>
            <ta e="T32" id="Seg_5569" s="T31">amno-bia-m</ta>
            <ta e="T33" id="Seg_5570" s="T32">dĭgəttə</ta>
            <ta e="T34" id="Seg_5571" s="T33">amno-bia-m</ta>
            <ta e="T35" id="Seg_5572" s="T34">baza-j</ta>
            <ta e="T36" id="Seg_5573" s="T35">aʔtʼi-nə</ta>
            <ta e="T37" id="Seg_5574" s="T36">šo-bia-m</ta>
            <ta e="T38" id="Seg_5575" s="T37">Ujar-də</ta>
            <ta e="T39" id="Seg_5576" s="T38">dĭgəttə</ta>
            <ta e="T40" id="Seg_5577" s="T39">Ujar-gəʔ</ta>
            <ta e="T41" id="Seg_5578" s="T40">šo-bia-m</ta>
            <ta e="T43" id="Seg_5579" s="T42">amno-bia-m</ta>
            <ta e="T44" id="Seg_5580" s="T43">avtobus-tə</ta>
            <ta e="T45" id="Seg_5581" s="T44">šo-bia-m</ta>
            <ta e="T46" id="Seg_5582" s="T45">Săjanskə-n</ta>
            <ta e="T47" id="Seg_5583" s="T46">dön</ta>
            <ta e="T48" id="Seg_5584" s="T47">amno-bia-m</ta>
            <ta e="T51" id="Seg_5585" s="T50">šide</ta>
            <ta e="T52" id="Seg_5586" s="T51">dʼala</ta>
            <ta e="T53" id="Seg_5587" s="T52">dĭgəttə</ta>
            <ta e="T54" id="Seg_5588" s="T53">Abalakof-tə</ta>
            <ta e="T55" id="Seg_5589" s="T54">šo-bia-m</ta>
            <ta e="T56" id="Seg_5590" s="T55">i</ta>
            <ta e="T57" id="Seg_5591" s="T56">tănan</ta>
            <ta e="T58" id="Seg_5592" s="T57">šo-bia-m</ta>
            <ta e="T59" id="Seg_5593" s="T58">tănan</ta>
            <ta e="T60" id="Seg_5594" s="T59">šo-bia-m</ta>
            <ta e="T61" id="Seg_5595" s="T60">dʼăbaktər-bi-baʔ</ta>
            <ta e="T62" id="Seg_5596" s="T61">dĭgəttə</ta>
            <ta e="T63" id="Seg_5597" s="T62">tăn</ta>
            <ta e="T64" id="Seg_5598" s="T63">măn</ta>
            <ta e="T65" id="Seg_5599" s="T64">măna</ta>
            <ta e="T67" id="Seg_5600" s="T66">măl-la-l</ta>
            <ta e="T68" id="Seg_5601" s="T67">amna-ʔ</ta>
            <ta e="T69" id="Seg_5602" s="T68">amor-gaʔ</ta>
            <ta e="T71" id="Seg_5603" s="T70">püjö-lia-l</ta>
            <ta e="T72" id="Seg_5604" s="T71">padʼi=ka</ta>
            <ta e="T73" id="Seg_5605" s="T72">dĭgəttə</ta>
            <ta e="T74" id="Seg_5606" s="T73">măn</ta>
            <ta e="T75" id="Seg_5607" s="T74">amno-bia-m</ta>
            <ta e="T76" id="Seg_5608" s="T75">amor-bia-m</ta>
            <ta e="T77" id="Seg_5609" s="T76">kam-bia-m</ta>
            <ta e="T78" id="Seg_5610" s="T77">bos-tə</ta>
            <ta e="T79" id="Seg_5611" s="T78">fatʼera-t-si</ta>
            <ta e="T80" id="Seg_5612" s="T79">onʼiʔ</ta>
            <ta e="T81" id="Seg_5613" s="T80">onʼiʔ</ta>
            <ta e="T82" id="Seg_5614" s="T81">raz</ta>
            <ta e="T83" id="Seg_5615" s="T82">miʔ</ta>
            <ta e="T84" id="Seg_5616" s="T83">kam-bi-baʔ</ta>
            <ta e="T85" id="Seg_5617" s="T84">nagur</ta>
            <ta e="T86" id="Seg_5618" s="T85">ne</ta>
            <ta e="T87" id="Seg_5619" s="T86">Zaozʼorka-nə</ta>
            <ta e="T88" id="Seg_5620" s="T87">dĭgəttə</ta>
            <ta e="T89" id="Seg_5621" s="T88">dĭʔə</ta>
            <ta e="T90" id="Seg_5622" s="T89">baza-j</ta>
            <ta e="T91" id="Seg_5623" s="T90">aʔtʼi-nə</ta>
            <ta e="T93" id="Seg_5624" s="T92">amno-bi-baʔ</ta>
            <ta e="T94" id="Seg_5625" s="T93">kam-bi-baʔ</ta>
            <ta e="T95" id="Seg_5626" s="T94">Ujar-də</ta>
            <ta e="T96" id="Seg_5627" s="T95">dĭn</ta>
            <ta e="T97" id="Seg_5628" s="T96">kudaj-də</ta>
            <ta e="T98" id="Seg_5629" s="T97">numan üzə-sʼtə</ta>
            <ta e="T99" id="Seg_5630" s="T98">dĭn</ta>
            <ta e="T100" id="Seg_5631" s="T99">kudaj-də</ta>
            <ta e="T101" id="Seg_5632" s="T100">numan üzə-leʔ-pi-beʔ</ta>
            <ta e="T102" id="Seg_5633" s="T101">dĭgəttə</ta>
            <ta e="T103" id="Seg_5634" s="T102">par-luʔ-pi-baʔ</ta>
            <ta e="T104" id="Seg_5635" s="T103">bazoʔ</ta>
            <ta e="T106" id="Seg_5636" s="T105">döbər</ta>
            <ta e="T107" id="Seg_5637" s="T106">Zaozʼorka-nə</ta>
            <ta e="T108" id="Seg_5638" s="T107">dĭn</ta>
            <ta e="T109" id="Seg_5639" s="T108">dön</ta>
            <ta e="T110" id="Seg_5640" s="T109">ša-biaʔ</ta>
            <ta e="T111" id="Seg_5641" s="T110">iʔgö</ta>
            <ta e="T112" id="Seg_5642" s="T111">dʼăbaktər-laʔ-pi-beʔ</ta>
            <ta e="T113" id="Seg_5643" s="T112">dĭgəttə</ta>
            <ta e="T114" id="Seg_5644" s="T113">kozʼa</ta>
            <ta e="T115" id="Seg_5645" s="T114">kam-bi</ta>
            <ta e="T116" id="Seg_5646" s="T115">avtobus</ta>
            <ta e="T117" id="Seg_5647" s="T116">măndə-r-zittə</ta>
            <ta e="T118" id="Seg_5648" s="T117">šo-bi</ta>
            <ta e="T119" id="Seg_5649" s="T118">da</ta>
            <ta e="T120" id="Seg_5650" s="T119">măndede</ta>
            <ta e="T868" id="Seg_5651" s="T120">kal-la</ta>
            <ta e="T121" id="Seg_5652" s="T868">dʼür-bi</ta>
            <ta e="T122" id="Seg_5653" s="T121">a</ta>
            <ta e="T123" id="Seg_5654" s="T122">miʔ</ta>
            <ta e="T124" id="Seg_5655" s="T123">oʔbdə-bi-baʔ</ta>
            <ta e="T125" id="Seg_5656" s="T124">da</ta>
            <ta e="T126" id="Seg_5657" s="T125">kam-bi-baʔ</ta>
            <ta e="T127" id="Seg_5658" s="T126">nʼiʔdə</ta>
            <ta e="T128" id="Seg_5659" s="T127">dön</ta>
            <ta e="T129" id="Seg_5660" s="T128">avtobus</ta>
            <ta e="T130" id="Seg_5661" s="T129">šonə-ga</ta>
            <ta e="T131" id="Seg_5662" s="T130">amnəl-də-bi</ta>
            <ta e="T132" id="Seg_5663" s="T131">miʔ</ta>
            <ta e="T133" id="Seg_5664" s="T132">da</ta>
            <ta e="T134" id="Seg_5665" s="T133">perejaslăvka-nə</ta>
            <ta e="T135" id="Seg_5666" s="T134">šo-bi-baʔ</ta>
            <ta e="T136" id="Seg_5667" s="T135">dĭn</ta>
            <ta e="T139" id="Seg_5668" s="T137">miʔnʼibeʔ</ta>
            <ta e="T140" id="Seg_5669" s="T139">dĭn</ta>
            <ta e="T141" id="Seg_5670" s="T140">miʔ</ta>
            <ta e="T142" id="Seg_5671" s="T141">dĭn</ta>
            <ta e="T143" id="Seg_5672" s="T142">üzə-bi-beʔ</ta>
            <ta e="T144" id="Seg_5673" s="T143">i</ta>
            <ta e="T145" id="Seg_5674" s="T144">nula-bi-baʔ</ta>
            <ta e="T146" id="Seg_5675" s="T145">măn</ta>
            <ta e="T147" id="Seg_5676" s="T146">măn-də-m</ta>
            <ta e="T148" id="Seg_5677" s="T147">ĭmbi</ta>
            <ta e="T149" id="Seg_5678" s="T148">nu-laʔ-sittə</ta>
            <ta e="T150" id="Seg_5679" s="T149">kan-žə-baʔ</ta>
            <ta e="T151" id="Seg_5680" s="T150">üdʼiʔ</ta>
            <ta e="T152" id="Seg_5681" s="T151">dĭgəttə</ta>
            <ta e="T153" id="Seg_5682" s="T152">kan-žə-baʔ</ta>
            <ta e="T154" id="Seg_5683" s="T153">kand-laʔbə-baʔ</ta>
            <ta e="T155" id="Seg_5684" s="T154">mašina</ta>
            <ta e="T156" id="Seg_5685" s="T155">bar</ta>
            <ta e="T157" id="Seg_5686" s="T156">šonə-ga</ta>
            <ta e="T158" id="Seg_5687" s="T157">amno-gaʔ</ta>
            <ta e="T159" id="Seg_5688" s="T158">miʔ</ta>
            <ta e="T162" id="Seg_5689" s="T161">amno-gaʔ</ta>
            <ta e="T163" id="Seg_5690" s="T162">döber</ta>
            <ta e="T164" id="Seg_5691" s="T163">dĭgəttə</ta>
            <ta e="T165" id="Seg_5692" s="T164">miʔ</ta>
            <ta e="T166" id="Seg_5693" s="T165">amno-bi-baʔ</ta>
            <ta e="T167" id="Seg_5694" s="T166">i</ta>
            <ta e="T168" id="Seg_5695" s="T167">šo-bi-baʔ</ta>
            <ta e="T169" id="Seg_5696" s="T168">Kandəga-nə</ta>
            <ta e="T170" id="Seg_5697" s="T169">dĭgəttə</ta>
            <ta e="T171" id="Seg_5698" s="T170">dĭn</ta>
            <ta e="T172" id="Seg_5699" s="T171">iššo</ta>
            <ta e="T173" id="Seg_5700" s="T172">sejʔpü</ta>
            <ta e="T175" id="Seg_5701" s="T174">ma-luʔ-pi</ta>
            <ta e="T176" id="Seg_5702" s="T175">Unʼer-də</ta>
            <ta e="T177" id="Seg_5703" s="T176">kan-zittə</ta>
            <ta e="T178" id="Seg_5704" s="T177">šide</ta>
            <ta e="T179" id="Seg_5705" s="T178">ne-zeŋ</ta>
            <ta e="T180" id="Seg_5706" s="T179">kam-bi-ʔi</ta>
            <ta e="T181" id="Seg_5707" s="T180">tura-nə</ta>
            <ta e="T182" id="Seg_5708" s="T181">a</ta>
            <ta e="T183" id="Seg_5709" s="T182">măn</ta>
            <ta e="T184" id="Seg_5710" s="T183">kam-bia-m</ta>
            <ta e="T185" id="Seg_5711" s="T184">üdʼiʔ</ta>
            <ta e="T186" id="Seg_5712" s="T185">Unʼer-də</ta>
            <ta e="T187" id="Seg_5713" s="T186">šo-bia-m</ta>
            <ta e="T188" id="Seg_5714" s="T187">deʔ-keʔ</ta>
            <ta e="T189" id="Seg_5715" s="T188">măna</ta>
            <ta e="T190" id="Seg_5716" s="T189">ipek</ta>
            <ta e="T191" id="Seg_5717" s="T190">sadar-gaʔ</ta>
            <ta e="T192" id="Seg_5718" s="T191">ipek</ta>
            <ta e="T193" id="Seg_5719" s="T192">dĭ-zeŋ</ta>
            <ta e="T194" id="Seg_5720" s="T193">măn-də</ta>
            <ta e="T196" id="Seg_5721" s="T195">kan-a-ʔ</ta>
            <ta e="T197" id="Seg_5722" s="T196">măgăzin-də</ta>
            <ta e="T199" id="Seg_5723" s="T198">dĭn</ta>
            <ta e="T200" id="Seg_5724" s="T199">i-ʔ</ta>
            <ta e="T201" id="Seg_5725" s="T200">măna</ta>
            <ta e="T202" id="Seg_5726" s="T201">iʔgö</ta>
            <ta e="T203" id="Seg_5727" s="T202">ej</ta>
            <ta e="T204" id="Seg_5728" s="T203">kereʔ</ta>
            <ta e="T205" id="Seg_5729" s="T204">šide</ta>
            <ta e="T206" id="Seg_5730" s="T205">gram</ta>
            <ta e="T207" id="Seg_5731" s="T206">mĭ-bi-leʔ</ta>
            <ta e="T208" id="Seg_5732" s="T207">dĭ</ta>
            <ta e="T209" id="Seg_5733" s="T208">măna</ta>
            <ta e="T210" id="Seg_5734" s="T209">šide</ta>
            <ta e="T211" id="Seg_5735" s="T210">gram</ta>
            <ta e="T212" id="Seg_5736" s="T211">edə-bi</ta>
            <ta e="T213" id="Seg_5737" s="T212">măn</ta>
            <ta e="T214" id="Seg_5738" s="T213">i-bie-m</ta>
            <ta e="T215" id="Seg_5739" s="T214">aktʼa</ta>
            <ta e="T216" id="Seg_5740" s="T215">mĭ-bie-m</ta>
            <ta e="T217" id="Seg_5741" s="T216">am-bia-m</ta>
            <ta e="T218" id="Seg_5742" s="T217">i</ta>
            <ta e="T219" id="Seg_5743" s="T218">kam-bia-m</ta>
            <ta e="T220" id="Seg_5744" s="T219">bü</ta>
            <ta e="T221" id="Seg_5745" s="T220">bĭʔ-pie-m</ta>
            <ta e="T222" id="Seg_5746" s="T221">dĭgəttə</ta>
            <ta e="T223" id="Seg_5747" s="T222">mašina</ta>
            <ta e="T224" id="Seg_5748" s="T223">bĭdə-bi</ta>
            <ta e="T225" id="Seg_5749" s="T224">măn</ta>
            <ta e="T227" id="Seg_5750" s="T226">amno-bia-m</ta>
            <ta e="T228" id="Seg_5751" s="T227">dĭgəttə</ta>
            <ta e="T229" id="Seg_5752" s="T228">üzə-bie-m</ta>
            <ta e="T230" id="Seg_5753" s="T229">šide</ta>
            <ta e="T231" id="Seg_5754" s="T230">kuza</ta>
            <ta e="T232" id="Seg_5755" s="T231">nu-ga-ʔi</ta>
            <ta e="T233" id="Seg_5756" s="T232">măja-gən</ta>
            <ta e="T234" id="Seg_5757" s="T233">măn</ta>
            <ta e="T235" id="Seg_5758" s="T234">dĭbər</ta>
            <ta e="T237" id="Seg_5759" s="T236">sʼa-bia-m</ta>
            <ta e="T238" id="Seg_5760" s="T237">kan-žə-bəj</ta>
            <ta e="T239" id="Seg_5761" s="T238">obbere</ta>
            <ta e="T240" id="Seg_5762" s="T239">dʼok</ta>
            <ta e="T241" id="Seg_5763" s="T240">miʔ</ta>
            <ta e="T242" id="Seg_5764" s="T241">edəʔ-leʔbə</ta>
            <ta e="T243" id="Seg_5765" s="T242">mašina</ta>
            <ta e="T244" id="Seg_5766" s="T243">mašina-ziʔ</ta>
            <ta e="T245" id="Seg_5767" s="T244">kan-žə-bəj</ta>
            <ta e="T246" id="Seg_5768" s="T245">dĭgəttə</ta>
            <ta e="T247" id="Seg_5769" s="T246">mašina</ta>
            <ta e="T248" id="Seg_5770" s="T247">šo-bi</ta>
            <ta e="T251" id="Seg_5771" s="T250">amno-bia-m</ta>
            <ta e="T252" id="Seg_5772" s="T251">i</ta>
            <ta e="T253" id="Seg_5773" s="T252">Săjanskəj-nə</ta>
            <ta e="T254" id="Seg_5774" s="T253">šo-bia-m</ta>
            <ta e="T255" id="Seg_5775" s="T254">dĭgəttə</ta>
            <ta e="T256" id="Seg_5776" s="T255">nĭn</ta>
            <ta e="T258" id="Seg_5777" s="T257">i-bie-m</ta>
            <ta e="T259" id="Seg_5778" s="T258">ipek</ta>
            <ta e="T260" id="Seg_5779" s="T259">sĭreʔpne</ta>
            <ta e="T261" id="Seg_5780" s="T260">Vanʼka</ta>
            <ta e="T262" id="Seg_5781" s="T261">bălʼnʼitsa-gən</ta>
            <ta e="T263" id="Seg_5782" s="T262">iʔbo-laʔ-pi</ta>
            <ta e="T264" id="Seg_5783" s="T263">kum-bia-m</ta>
            <ta e="T265" id="Seg_5784" s="T264">dĭʔ-nə</ta>
            <ta e="T266" id="Seg_5785" s="T265">măna</ta>
            <ta e="T267" id="Seg_5786" s="T266">dĭn</ta>
            <ta e="T268" id="Seg_5787" s="T267">nörbə-laʔbə-ʔjə</ta>
            <ta e="T269" id="Seg_5788" s="T268">dön</ta>
            <ta e="T270" id="Seg_5789" s="T269">i-geʔ</ta>
            <ta e="T271" id="Seg_5790" s="T270">miʔ</ta>
            <ta e="T272" id="Seg_5791" s="T271">mašina</ta>
            <ta e="T273" id="Seg_5792" s="T272">kan-a-ʔ</ta>
            <ta e="T274" id="Seg_5793" s="T273">măn</ta>
            <ta e="T275" id="Seg_5794" s="T274">nuʔmə-bie-m</ta>
            <ta e="T276" id="Seg_5795" s="T275">gijen=də</ta>
            <ta e="T277" id="Seg_5796" s="T276">ej</ta>
            <ta e="T278" id="Seg_5797" s="T277">ku-bia-m</ta>
            <ta e="T279" id="Seg_5798" s="T278">dĭgəttə</ta>
            <ta e="T280" id="Seg_5799" s="T279">kam-bia-m</ta>
            <ta e="T281" id="Seg_5800" s="T280">üdʼiʔ</ta>
            <ta e="T282" id="Seg_5801" s="T281">dĭgəttə</ta>
            <ta e="T283" id="Seg_5802" s="T282">bazo</ta>
            <ta e="T284" id="Seg_5803" s="T283">mašina</ta>
            <ta e="T285" id="Seg_5804" s="T284">šonə-ga</ta>
            <ta e="T286" id="Seg_5805" s="T285">măn</ta>
            <ta e="T287" id="Seg_5806" s="T286">amno-bia-m</ta>
            <ta e="T288" id="Seg_5807" s="T287">Malinovki-nə</ta>
            <ta e="T289" id="Seg_5808" s="T288">šo-bia-m</ta>
            <ta e="T290" id="Seg_5809" s="T289">dĭgəttə</ta>
            <ta e="T291" id="Seg_5810" s="T290">bazo</ta>
            <ta e="T292" id="Seg_5811" s="T291">üjü-zi</ta>
            <ta e="T293" id="Seg_5812" s="T292">kam-bia-m</ta>
            <ta e="T294" id="Seg_5813" s="T293">dĭgəttə</ta>
            <ta e="T295" id="Seg_5814" s="T294">bazo</ta>
            <ta e="T296" id="Seg_5815" s="T295">mašina</ta>
            <ta e="T297" id="Seg_5816" s="T296">bĭdə-bi</ta>
            <ta e="T298" id="Seg_5817" s="T297">i</ta>
            <ta e="T299" id="Seg_5818" s="T298">ma-ndə</ta>
            <ta e="T300" id="Seg_5819" s="T299">šo-bia-m</ta>
            <ta e="T301" id="Seg_5820" s="T300">kamən</ta>
            <ta e="T302" id="Seg_5821" s="T301">Matvejev</ta>
            <ta e="T303" id="Seg_5822" s="T302">šo-bi</ta>
            <ta e="T304" id="Seg_5823" s="T303">măna</ta>
            <ta e="T306" id="Seg_5824" s="T305">dʼăbaktər-laʔbə</ta>
            <ta e="T307" id="Seg_5825" s="T306">nu-d-la</ta>
            <ta e="T308" id="Seg_5826" s="T307">a</ta>
            <ta e="T309" id="Seg_5827" s="T308">măn</ta>
            <ta e="T310" id="Seg_5828" s="T309">kazak</ta>
            <ta e="T311" id="Seg_5829" s="T310">kazak</ta>
            <ta e="T312" id="Seg_5830" s="T311">dʼăbaktər-laʔbə-m</ta>
            <ta e="T313" id="Seg_5831" s="T312">i</ta>
            <ta e="T314" id="Seg_5832" s="T313">petʼer-laʔbə-m</ta>
            <ta e="T315" id="Seg_5833" s="T314">tăn</ta>
            <ta e="T316" id="Seg_5834" s="T315">tüj</ta>
            <ta e="T317" id="Seg_5835" s="T316">nu</ta>
            <ta e="T318" id="Seg_5836" s="T317">i-ge-l</ta>
            <ta e="T319" id="Seg_5837" s="T318">a</ta>
            <ta e="T320" id="Seg_5838" s="T319">măn</ta>
            <ta e="T321" id="Seg_5839" s="T320">kazak</ta>
            <ta e="T322" id="Seg_5840" s="T321">i-ge-m</ta>
            <ta e="T323" id="Seg_5841" s="T322">dĭgəttə</ta>
            <ta e="T324" id="Seg_5842" s="T323">dĭ-zeŋ</ta>
            <ta e="T325" id="Seg_5843" s="T324">dʼije-nə</ta>
            <ta e="T326" id="Seg_5844" s="T325">kam-bi-ʔi</ta>
            <ta e="T327" id="Seg_5845" s="T326">Dʼelam-də</ta>
            <ta e="T329" id="Seg_5846" s="T328">kola</ta>
            <ta e="T330" id="Seg_5847" s="T329">dʼaʔ-pi-ʔi</ta>
            <ta e="T331" id="Seg_5848" s="T330">dĭn</ta>
            <ta e="T334" id="Seg_5849" s="T333">en-bi-ʔi</ta>
            <ta e="T335" id="Seg_5850" s="T334">dĭgəttə</ta>
            <ta e="T336" id="Seg_5851" s="T335">onʼiʔ</ta>
            <ta e="T337" id="Seg_5852" s="T336">koʔbdo</ta>
            <ta e="T338" id="Seg_5853" s="T337">urgo</ta>
            <ta e="T339" id="Seg_5854" s="T338">bar</ta>
            <ta e="T340" id="Seg_5855" s="T339">ĭzem-nuʔ-pi</ta>
            <ta e="T341" id="Seg_5856" s="T340">bar</ta>
            <ta e="T342" id="Seg_5857" s="T341">dĭgəttə</ta>
            <ta e="T343" id="Seg_5858" s="T342">dĭ-zeŋ</ta>
            <ta e="T344" id="Seg_5859" s="T343">šo-bi-ʔi</ta>
            <ta e="T345" id="Seg_5860" s="T344">tura-nə</ta>
            <ta e="T346" id="Seg_5861" s="T345">Kanokler-də</ta>
            <ta e="T347" id="Seg_5862" s="T346">i</ta>
            <ta e="T348" id="Seg_5863" s="T347">ne</ta>
            <ta e="T349" id="Seg_5864" s="T348">i-bi-ʔi</ta>
            <ta e="T350" id="Seg_5865" s="T349">i</ta>
            <ta e="T351" id="Seg_5866" s="T350">dĭ-m</ta>
            <ta e="T352" id="Seg_5867" s="T351">deʔ-pi-ʔi</ta>
            <ta e="T353" id="Seg_5868" s="T352">bălʼnʼitsa-nə</ta>
            <ta e="T354" id="Seg_5869" s="T353">tĭn</ta>
            <ta e="T355" id="Seg_5870" s="T354">üjü-n</ta>
            <ta e="T356" id="Seg_5871" s="T355">bar</ta>
            <ta e="T357" id="Seg_5872" s="T356">bü</ta>
            <ta e="T358" id="Seg_5873" s="T357">bĭʔ-pi-ʔi</ta>
            <ta e="T359" id="Seg_5874" s="T358">i</ta>
            <ta e="T360" id="Seg_5875" s="T359">dʼubtə-bi-ʔi</ta>
            <ta e="T361" id="Seg_5876" s="T360">dʼazir-bi-ʔi</ta>
            <ta e="T362" id="Seg_5877" s="T361">ato</ta>
            <ta e="T363" id="Seg_5878" s="T362">dĭ</ta>
            <ta e="T364" id="Seg_5879" s="T363">mĭn-zittə</ta>
            <ta e="T365" id="Seg_5880" s="T364">ej</ta>
            <ta e="T367" id="Seg_5881" s="T366">ej</ta>
            <ta e="T368" id="Seg_5882" s="T367">mo-lia</ta>
            <ta e="T369" id="Seg_5883" s="T368">măn</ta>
            <ta e="T370" id="Seg_5884" s="T369">măndər</ta>
            <ta e="T372" id="Seg_5885" s="T371">măndə-r-bia-m</ta>
            <ta e="T373" id="Seg_5886" s="T372">măndə-r-bia-m</ta>
            <ta e="T375" id="Seg_5887" s="T374">dĭbər</ta>
            <ta e="T376" id="Seg_5888" s="T375">pʼaŋdə-bia-m</ta>
            <ta e="T377" id="Seg_5889" s="T376">sazən</ta>
            <ta e="T378" id="Seg_5890" s="T377">dĭbər</ta>
            <ta e="T380" id="Seg_5891" s="T379">pʼaŋdə-bia-m</ta>
            <ta e="T381" id="Seg_5892" s="T380">šindi</ta>
            <ta e="T383" id="Seg_5893" s="T381">măn=də</ta>
            <ta e="T384" id="Seg_5894" s="T383">ku-lam-bi</ta>
            <ta e="T385" id="Seg_5895" s="T384">šindi</ta>
            <ta e="T387" id="Seg_5896" s="T385">măn=də</ta>
            <ta e="T388" id="Seg_5897" s="T387">ej</ta>
            <ta e="T389" id="Seg_5898" s="T388">ku-lam-bi</ta>
            <ta e="T390" id="Seg_5899" s="T389">gijen=də</ta>
            <ta e="T391" id="Seg_5900" s="T390">naga</ta>
            <ta e="T392" id="Seg_5901" s="T391">pravda-ʔi</ta>
            <ta e="T393" id="Seg_5902" s="T392">a</ta>
            <ta e="T394" id="Seg_5903" s="T393">măn</ta>
            <ta e="T395" id="Seg_5904" s="T394">tenə-bie-m</ta>
            <ta e="T396" id="Seg_5905" s="T395">i-ge</ta>
            <ta e="T397" id="Seg_5906" s="T396">vezʼdʼe</ta>
            <ta e="T398" id="Seg_5907" s="T397">pravda</ta>
            <ta e="T399" id="Seg_5908" s="T398">i-ge</ta>
            <ta e="T400" id="Seg_5909" s="T399">a</ta>
            <ta e="T401" id="Seg_5910" s="T400">dĭ-m</ta>
            <ta e="T402" id="Seg_5911" s="T401">gijen=də</ta>
            <ta e="T403" id="Seg_5912" s="T402">naga</ta>
            <ta e="T404" id="Seg_5913" s="T403">üge</ta>
            <ta e="T405" id="Seg_5914" s="T404">šanaklʼu</ta>
            <ta e="T406" id="Seg_5915" s="T405">dĭgəttə</ta>
            <ta e="T407" id="Seg_5916" s="T406">prokuror</ta>
            <ta e="T408" id="Seg_5917" s="T407">pʼaŋdə-bi</ta>
            <ta e="T409" id="Seg_5918" s="T408">gibər=də</ta>
            <ta e="T869" id="Seg_5919" s="T409">kal-la</ta>
            <ta e="T410" id="Seg_5920" s="T869">dʼür-bi</ta>
            <ta e="T411" id="Seg_5921" s="T410">ej</ta>
            <ta e="T412" id="Seg_5922" s="T411">tĭmne-m</ta>
            <ta e="T413" id="Seg_5923" s="T412">Jelʼa</ta>
            <ta e="T414" id="Seg_5924" s="T413">kuʔkubi</ta>
            <ta e="T415" id="Seg_5925" s="T414">dʼodunʼim</ta>
            <ta e="T416" id="Seg_5926" s="T415">bar</ta>
            <ta e="T417" id="Seg_5927" s="T416">lotka-nə</ta>
            <ta e="T418" id="Seg_5928" s="T417">bü-nə</ta>
            <ta e="T419" id="Seg_5929" s="T418">kam-bi-ʔi</ta>
            <ta e="T420" id="Seg_5930" s="T419">dĭgəttə</ta>
            <ta e="T421" id="Seg_5931" s="T420">girgit=də</ta>
            <ta e="T422" id="Seg_5932" s="T421">koʔbdo-zi</ta>
            <ta e="T423" id="Seg_5933" s="T422">xatʼeli</ta>
            <ta e="T424" id="Seg_5934" s="T423">bü-gən</ta>
            <ta e="T426" id="Seg_5935" s="T425">a</ta>
            <ta e="T427" id="Seg_5936" s="T426">bü</ta>
            <ta e="T428" id="Seg_5937" s="T427">ugandə</ta>
            <ta e="T429" id="Seg_5938" s="T428">urgo</ta>
            <ta e="T430" id="Seg_5939" s="T429">dĭ-zeŋ</ta>
            <ta e="T431" id="Seg_5940" s="T430">bar</ta>
            <ta e="T432" id="Seg_5941" s="T431">xatʼeli</ta>
            <ta e="T433" id="Seg_5942" s="T432">lotka-nə</ta>
            <ta e="T434" id="Seg_5943" s="T433">sʼa-zittə</ta>
            <ta e="T435" id="Seg_5944" s="T434">da</ta>
            <ta e="T436" id="Seg_5945" s="T435">bar</ta>
            <ta e="T437" id="Seg_5946" s="T436">nereʔ-luʔ-pi-ʔi</ta>
            <ta e="T439" id="Seg_5947" s="T437">dĭgəttə</ta>
            <ta e="T440" id="Seg_5948" s="T439">dĭgəttə</ta>
            <ta e="T441" id="Seg_5949" s="T440">dĭn</ta>
            <ta e="T442" id="Seg_5950" s="T441">nereʔ-luʔ-pi-ʔi</ta>
            <ta e="T443" id="Seg_5951" s="T442">oʔbdə-bi</ta>
            <ta e="T445" id="Seg_5952" s="T444">ku-lia-t</ta>
            <ta e="T446" id="Seg_5953" s="T445">što</ta>
            <ta e="T447" id="Seg_5954" s="T446">dʼoduni</ta>
            <ta e="T448" id="Seg_5955" s="T447">kü-bi</ta>
            <ta e="T449" id="Seg_5956" s="T448">Sălămatən</ta>
            <ta e="T450" id="Seg_5957" s="T449">Mitrăfan-ə-n</ta>
            <ta e="T451" id="Seg_5958" s="T450">tura-t</ta>
            <ta e="T452" id="Seg_5959" s="T451">iššo</ta>
            <ta e="T453" id="Seg_5960" s="T452">nu-ga</ta>
            <ta e="T454" id="Seg_5961" s="T453">i</ta>
            <ta e="T456" id="Seg_5962" s="T455">i</ta>
            <ta e="T457" id="Seg_5963" s="T456">gurun</ta>
            <ta e="T458" id="Seg_5964" s="T457">Andʼigatovan</ta>
            <ta e="T459" id="Seg_5965" s="T458">tura</ta>
            <ta e="T460" id="Seg_5966" s="T459">iššo</ta>
            <ta e="T461" id="Seg_5967" s="T460">nu-ga</ta>
            <ta e="T466" id="Seg_5968" s="T465">tura</ta>
            <ta e="T467" id="Seg_5969" s="T466">iššo</ta>
            <ta e="T468" id="Seg_5970" s="T467">nu-ga</ta>
            <ta e="T471" id="Seg_5971" s="T470">tura</ta>
            <ta e="T472" id="Seg_5972" s="T471">iššo</ta>
            <ta e="T473" id="Seg_5973" s="T472">nu-ga</ta>
            <ta e="T476" id="Seg_5974" s="T475">tura</ta>
            <ta e="T477" id="Seg_5975" s="T476">iššo</ta>
            <ta e="T478" id="Seg_5976" s="T477">nu-ga</ta>
            <ta e="T479" id="Seg_5977" s="T478">i</ta>
            <ta e="T481" id="Seg_5978" s="T480">Vlas-tə</ta>
            <ta e="T482" id="Seg_5979" s="T481">iššo</ta>
            <ta e="T483" id="Seg_5980" s="T482">tura</ta>
            <ta e="T484" id="Seg_5981" s="T483">nu-ga</ta>
            <ta e="T485" id="Seg_5982" s="T484">i</ta>
            <ta e="T489" id="Seg_5983" s="T488">iššo</ta>
            <ta e="T490" id="Seg_5984" s="T489">tura</ta>
            <ta e="T491" id="Seg_5985" s="T490">nu-ga</ta>
            <ta e="T492" id="Seg_5986" s="T491">kamən</ta>
            <ta e="T494" id="Seg_5987" s="T493">šo-bi</ta>
            <ta e="T495" id="Seg_5988" s="T494">dʼăbaktər-zittə</ta>
            <ta e="T496" id="Seg_5989" s="T495">döbər</ta>
            <ta e="T497" id="Seg_5990" s="T496">dön</ta>
            <ta e="T499" id="Seg_5991" s="T498">sumna</ta>
            <ta e="T500" id="Seg_5992" s="T499">bʼeʔ</ta>
            <ta e="T501" id="Seg_5993" s="T500">tura</ta>
            <ta e="T502" id="Seg_5994" s="T501">i-bi</ta>
            <ta e="T506" id="Seg_5995" s="T505">kü-laːm-bi</ta>
            <ta e="T507" id="Seg_5996" s="T506">dĭ</ta>
            <ta e="T508" id="Seg_5997" s="T507">kam-bi</ta>
            <ta e="T509" id="Seg_5998" s="T508">Permʼakov-tə</ta>
            <ta e="T510" id="Seg_5999" s="T509">dĭn</ta>
            <ta e="T511" id="Seg_6000" s="T510">ara</ta>
            <ta e="T512" id="Seg_6001" s="T511">bĭʔ-pi</ta>
            <ta e="T513" id="Seg_6002" s="T512">dĭn</ta>
            <ta e="T515" id="Seg_6003" s="T514">i-bi</ta>
            <ta e="T516" id="Seg_6004" s="T515">onʼiʔ</ta>
            <ta e="T518" id="Seg_6005" s="T517">i</ta>
            <ta e="T519" id="Seg_6006" s="T518">ne-t-si</ta>
            <ta e="T520" id="Seg_6007" s="T519">šo-bi</ta>
            <ta e="T521" id="Seg_6008" s="T520">dĭgəttə</ta>
            <ta e="T522" id="Seg_6009" s="T521">kam-bi</ta>
            <ta e="T523" id="Seg_6010" s="T522">krospa-ʔi-nə</ta>
            <ta e="T524" id="Seg_6011" s="T523">dĭgəttə</ta>
            <ta e="T525" id="Seg_6012" s="T524">kăm-lia</ta>
            <ta e="T526" id="Seg_6013" s="T525">da</ta>
            <ta e="T527" id="Seg_6014" s="T526">kudo-nz-lia</ta>
            <ta e="T528" id="Seg_6015" s="T527">bĭd-e-ʔ</ta>
            <ta e="T529" id="Seg_6016" s="T528">nüke-m</ta>
            <ta e="T530" id="Seg_6017" s="T529">bĭd-e-ʔ</ta>
            <ta e="T531" id="Seg_6018" s="T530">nüke-m</ta>
            <ta e="T532" id="Seg_6019" s="T531">a</ta>
            <ta e="T533" id="Seg_6020" s="T532">măn</ta>
            <ta e="T534" id="Seg_6021" s="T533">aba-m</ta>
            <ta e="T535" id="Seg_6022" s="T534">nu-ga</ta>
            <ta e="T536" id="Seg_6023" s="T535">da</ta>
            <ta e="T537" id="Seg_6024" s="T536">măndo-laʔbǝ</ta>
            <ta e="T538" id="Seg_6025" s="T537">da</ta>
            <ta e="T539" id="Seg_6026" s="T538">kaknar-laʔbə</ta>
            <ta e="T540" id="Seg_6027" s="T539">măn</ta>
            <ta e="T541" id="Seg_6028" s="T540">noʔ</ta>
            <ta e="T542" id="Seg_6029" s="T541">jaʔ-pia-m</ta>
            <ta e="T543" id="Seg_6030" s="T542">dĭgəttə</ta>
            <ta e="T544" id="Seg_6031" s="T543">oʔbdə-bia-m</ta>
            <ta e="T545" id="Seg_6032" s="T544">dĭgəttə</ta>
            <ta e="T546" id="Seg_6033" s="T545">stog-də</ta>
            <ta e="T547" id="Seg_6034" s="T546">em-bie-m</ta>
            <ta e="T548" id="Seg_6035" s="T547">dĭ</ta>
            <ta e="T549" id="Seg_6036" s="T548">dʼüʔpi</ta>
            <ta e="T550" id="Seg_6037" s="T549">i-bi</ta>
            <ta e="T551" id="Seg_6038" s="T550">dĭgəttə</ta>
            <ta e="T552" id="Seg_6039" s="T551">nagur</ta>
            <ta e="T554" id="Seg_6040" s="T553">dʼala</ta>
            <ta e="T555" id="Seg_6041" s="T554">ej</ta>
            <ta e="T556" id="Seg_6042" s="T555">kam-bia-m</ta>
            <ta e="T557" id="Seg_6043" s="T556">ugandə</ta>
            <ta e="T558" id="Seg_6044" s="T557">dʼibige</ta>
            <ta e="T559" id="Seg_6045" s="T558">mo-lam-bi</ta>
            <ta e="T562" id="Seg_6046" s="T561">măn</ta>
            <ta e="T563" id="Seg_6047" s="T562">šide</ta>
            <ta e="T564" id="Seg_6048" s="T563">ne</ta>
            <ta e="T565" id="Seg_6049" s="T564">i-bie-m</ta>
            <ta e="T566" id="Seg_6050" s="T565">kam-bia-m</ta>
            <ta e="T567" id="Seg_6051" s="T566">bar</ta>
            <ta e="T568" id="Seg_6052" s="T567">ko-bi</ta>
            <ta e="T569" id="Seg_6053" s="T568">da</ta>
            <ta e="T570" id="Seg_6054" s="T569">bazoʔ</ta>
            <ta e="T571" id="Seg_6055" s="T570">em-bi-beʔ</ta>
            <ta e="T572" id="Seg_6056" s="T571">kuvas</ta>
            <ta e="T574" id="Seg_6057" s="T573">stog</ta>
            <ta e="T575" id="Seg_6058" s="T574">măn</ta>
            <ta e="T576" id="Seg_6059" s="T575">amno-bia-m</ta>
            <ta e="T577" id="Seg_6060" s="T576">šide</ta>
            <ta e="T578" id="Seg_6061" s="T577">bʼeʔ</ta>
            <ta e="T579" id="Seg_6062" s="T578">pʼe</ta>
            <ta e="T580" id="Seg_6063" s="T579">bos-pə</ta>
            <ta e="T581" id="Seg_6064" s="T580">noʔ</ta>
            <ta e="T582" id="Seg_6065" s="T581">jaʔ-pia-m</ta>
            <ta e="T583" id="Seg_6066" s="T582">bos-tə</ta>
            <ta e="T584" id="Seg_6067" s="T583">oʔbdə-bia-m</ta>
            <ta e="T585" id="Seg_6068" s="T584">bos-tə</ta>
            <ta e="T586" id="Seg_6069" s="T585">em-bie-m</ta>
            <ta e="T587" id="Seg_6070" s="T586">ine-zi</ta>
            <ta e="T588" id="Seg_6071" s="T587">maʔ-nʼi</ta>
            <ta e="T589" id="Seg_6072" s="T588">tažer-bia-m</ta>
            <ta e="T590" id="Seg_6073" s="T589">bar</ta>
            <ta e="T591" id="Seg_6074" s="T590">tüžöj-ʔi-m</ta>
            <ta e="T594" id="Seg_6075" s="T593">băd-laʔ-pia-m</ta>
            <ta e="T595" id="Seg_6076" s="T594">onʼiʔ</ta>
            <ta e="T596" id="Seg_6077" s="T595">ine</ta>
            <ta e="T597" id="Seg_6078" s="T596">i-bie-m</ta>
            <ta e="T598" id="Seg_6079" s="T597">dĭgəttə</ta>
            <ta e="T599" id="Seg_6080" s="T598">plemʼannica-t</ta>
            <ta e="T600" id="Seg_6081" s="T599">nʼi-n</ta>
            <ta e="T601" id="Seg_6082" s="T600">i-bie-m</ta>
            <ta e="T602" id="Seg_6083" s="T601">da</ta>
            <ta e="T603" id="Seg_6084" s="T602">iššo</ta>
            <ta e="T605" id="Seg_6085" s="T604">kam-bi-baʔ</ta>
            <ta e="T606" id="Seg_6086" s="T605">noʔ</ta>
            <ta e="T607" id="Seg_6087" s="T606">em-bi-beʔ</ta>
            <ta e="T608" id="Seg_6088" s="T607">dĭgəttə</ta>
            <ta e="T609" id="Seg_6089" s="T608">šo-bi-baʔ</ta>
            <ta e="T610" id="Seg_6090" s="T609">šo-bi-baʔ</ta>
            <ta e="T611" id="Seg_6091" s="T610">miʔ</ta>
            <ta e="T612" id="Seg_6092" s="T611">noʔ</ta>
            <ta e="T867" id="Seg_6093" s="T613">üzə-bi </ta>
            <ta e="T615" id="Seg_6094" s="T614">dĭgəttə</ta>
            <ta e="T616" id="Seg_6095" s="T615">dʼildə-bi-beʔ</ta>
            <ta e="T617" id="Seg_6096" s="T616">dʼildə-bi-beʔ</ta>
            <ta e="T618" id="Seg_6097" s="T617">ej</ta>
            <ta e="T619" id="Seg_6098" s="T618">mo-lia-baʔ</ta>
            <ta e="T620" id="Seg_6099" s="T619">dĭ</ta>
            <ta e="T621" id="Seg_6100" s="T620">măn-də</ta>
            <ta e="T622" id="Seg_6101" s="T621">tüjö</ta>
            <ta e="T623" id="Seg_6102" s="T622">ine</ta>
            <ta e="T626" id="Seg_6103" s="T625">körer-bi-m</ta>
            <ta e="T627" id="Seg_6104" s="T626">dĭgəttə</ta>
            <ta e="T628" id="Seg_6105" s="T627">dʼild-li-m</ta>
            <ta e="T629" id="Seg_6106" s="T628">a</ta>
            <ta e="T630" id="Seg_6107" s="T629">kăde</ta>
            <ta e="T631" id="Seg_6108" s="T630">tăn</ta>
            <ta e="T632" id="Seg_6109" s="T631">dʼild-li-l</ta>
            <ta e="T633" id="Seg_6110" s="T632">a</ta>
            <ta e="T634" id="Seg_6111" s="T633">dăre</ta>
            <ta e="T635" id="Seg_6112" s="T634">bar</ta>
            <ta e="T636" id="Seg_6113" s="T866">barəʔ-pi</ta>
            <ta e="T637" id="Seg_6114" s="T636">orša-ʔi</ta>
            <ta e="T638" id="Seg_6115" s="T637">i</ta>
            <ta e="T639" id="Seg_6116" s="T638">dĭgəttə</ta>
            <ta e="T640" id="Seg_6117" s="T639">ine</ta>
            <ta e="T643" id="Seg_6118" s="T642">dĭgəttə</ta>
            <ta e="T644" id="Seg_6119" s="T643">maʔ</ta>
            <ta e="T646" id="Seg_6120" s="T645">šo-bi-baʔ</ta>
            <ta e="T647" id="Seg_6121" s="T646">mĭm-bi-beʔ</ta>
            <ta e="T648" id="Seg_6122" s="T647">noʔ</ta>
            <ta e="T649" id="Seg_6123" s="T648">dĭgəttə</ta>
            <ta e="T650" id="Seg_6124" s="T649">ine-m</ta>
            <ta e="T652" id="Seg_6125" s="T651">măn</ta>
            <ta e="T653" id="Seg_6126" s="T652">ugandə</ta>
            <ta e="T654" id="Seg_6127" s="T653">kăm-bia-m</ta>
            <ta e="T655" id="Seg_6128" s="T654">dĭgəttə</ta>
            <ta e="T656" id="Seg_6129" s="T655">kašelʼ</ta>
            <ta e="T657" id="Seg_6130" s="T656">šo-bi</ta>
            <ta e="T658" id="Seg_6131" s="T657">dĭgəttə</ta>
            <ta e="T659" id="Seg_6132" s="T658">măn</ta>
            <ta e="T660" id="Seg_6133" s="T659">sĭreʔpne</ta>
            <ta e="T661" id="Seg_6134" s="T660">bar</ta>
            <ta e="T662" id="Seg_6135" s="T661">nendə-bie-m</ta>
            <ta e="T663" id="Seg_6136" s="T662">ara-nə</ta>
            <ta e="T664" id="Seg_6137" s="T663">dĭgəttə</ta>
            <ta e="T665" id="Seg_6138" s="T664">bĭʔ-pie-m</ta>
            <ta e="T666" id="Seg_6139" s="T665">i</ta>
            <ta e="T667" id="Seg_6140" s="T666">gibər=də</ta>
            <ta e="T668" id="Seg_6141" s="T667">ka-la-j</ta>
            <ta e="T669" id="Seg_6142" s="T668">kašelʼ-bə</ta>
            <ta e="T673" id="Seg_6143" s="T672">girgit</ta>
            <ta e="T674" id="Seg_6144" s="T673">noʔ</ta>
            <ta e="T676" id="Seg_6145" s="T675">noʔ</ta>
            <ta e="T677" id="Seg_6146" s="T676">i-ge</ta>
            <ta e="T679" id="Seg_6147" s="T678">dĭ</ta>
            <ta e="T680" id="Seg_6148" s="T679">dudka-nə</ta>
            <ta e="T681" id="Seg_6149" s="T680">i-zittə</ta>
            <ta e="T682" id="Seg_6150" s="T681">nada</ta>
            <ta e="T683" id="Seg_6151" s="T682">nʼiʔ-sittə</ta>
            <ta e="T684" id="Seg_6152" s="T683">dĭ-m</ta>
            <ta e="T685" id="Seg_6153" s="T684">əbərdə</ta>
            <ta e="T686" id="Seg_6154" s="T685">bər</ta>
            <ta e="T687" id="Seg_6155" s="T686">amno-sʼtə</ta>
            <ta e="T688" id="Seg_6156" s="T687">dĭgəttə</ta>
            <ta e="T689" id="Seg_6157" s="T688">ej</ta>
            <ta e="T690" id="Seg_6158" s="T689">mo-lə-j</ta>
            <ta e="T691" id="Seg_6159" s="T690">kašelʼ</ta>
            <ta e="T692" id="Seg_6160" s="T691">nend-luʔ-pi</ta>
            <ta e="T693" id="Seg_6161" s="T692">i</ta>
            <ta e="T694" id="Seg_6162" s="T693">bər</ta>
            <ta e="T695" id="Seg_6163" s="T694">naga</ta>
            <ta e="T696" id="Seg_6164" s="T695">i</ta>
            <ta e="T697" id="Seg_6165" s="T696">ejü</ta>
            <ta e="T698" id="Seg_6166" s="T697">naga</ta>
            <ta e="T699" id="Seg_6167" s="T698">pa-ʔi</ta>
            <ta e="T700" id="Seg_6168" s="T699">măna</ta>
            <ta e="T701" id="Seg_6169" s="T700">teinen</ta>
            <ta e="T702" id="Seg_6170" s="T701">ej</ta>
            <ta e="T703" id="Seg_6171" s="T702">toʔ-nar-zittə</ta>
            <ta e="T704" id="Seg_6172" s="T703">kabarləj</ta>
            <ta e="T706" id="Seg_6173" s="T705">dö</ta>
            <ta e="T707" id="Seg_6174" s="T706">nüdʼi-nə</ta>
            <ta e="T708" id="Seg_6175" s="T707">pa-ʔi</ta>
            <ta e="T709" id="Seg_6176" s="T708">măn</ta>
            <ta e="T710" id="Seg_6177" s="T709">iʔgö</ta>
            <ta e="T711" id="Seg_6178" s="T710">dö</ta>
            <ta e="T712" id="Seg_6179" s="T711">pʼe-nə</ta>
            <ta e="T713" id="Seg_6180" s="T712">iššo</ta>
            <ta e="T714" id="Seg_6181" s="T713">kabar-lə-j</ta>
            <ta e="T715" id="Seg_6182" s="T714">šĭšəge</ta>
            <ta e="T716" id="Seg_6183" s="T715">ugandə</ta>
            <ta e="T717" id="Seg_6184" s="T716">iʔgö</ta>
            <ta e="T718" id="Seg_6185" s="T717">pa</ta>
            <ta e="T719" id="Seg_6186" s="T718">kan-də-ga-ʔi</ta>
            <ta e="T720" id="Seg_6187" s="T719">pa-ʔi</ta>
            <ta e="T721" id="Seg_6188" s="T720">bar</ta>
            <ta e="T722" id="Seg_6189" s="T721">surno-gə</ta>
            <ta e="T723" id="Seg_6190" s="T722">sagər</ta>
            <ta e="T724" id="Seg_6191" s="T723">mo-bi-ʔi</ta>
            <ta e="T725" id="Seg_6192" s="T724">ato</ta>
            <ta e="T726" id="Seg_6193" s="T725">sĭre</ta>
            <ta e="T727" id="Seg_6194" s="T726">i-bi-ʔi</ta>
            <ta e="T728" id="Seg_6195" s="T727">măn</ta>
            <ta e="T729" id="Seg_6196" s="T728">büttona</ta>
            <ta e="T730" id="Seg_6197" s="T729">šomi</ta>
            <ta e="T731" id="Seg_6198" s="T730">pa-ʔi</ta>
            <ta e="T732" id="Seg_6199" s="T731">sĭre</ta>
            <ta e="T733" id="Seg_6200" s="T732">pa-ʔi</ta>
            <ta e="T734" id="Seg_6201" s="T733">bar</ta>
            <ta e="T735" id="Seg_6202" s="T734">bătek</ta>
            <ta e="T736" id="Seg_6203" s="T735">dĭ-zeŋ</ta>
            <ta e="T737" id="Seg_6204" s="T736">ugandə</ta>
            <ta e="T738" id="Seg_6205" s="T737">jakše</ta>
            <ta e="T739" id="Seg_6206" s="T738">tăŋ</ta>
            <ta e="T740" id="Seg_6207" s="T739">nend-leʔbə-ʔjə</ta>
            <ta e="T741" id="Seg_6208" s="T740">i</ta>
            <ta e="T742" id="Seg_6209" s="T741">ejü</ta>
            <ta e="T743" id="Seg_6210" s="T742">dĭ-zeŋ</ta>
            <ta e="T744" id="Seg_6211" s="T743">dĭmbi</ta>
            <ta e="T745" id="Seg_6212" s="T744">nuld-lie-l</ta>
            <ta e="T746" id="Seg_6213" s="T745">mĭndər-zittə</ta>
            <ta e="T748" id="Seg_6214" s="T747">kös</ta>
            <ta e="T749" id="Seg_6215" s="T748">ej</ta>
            <ta e="T750" id="Seg_6216" s="T749">ej</ta>
            <ta e="T751" id="Seg_6217" s="T750">păʔ-lia</ta>
            <ta e="T752" id="Seg_6218" s="T751">dibər</ta>
            <ta e="T754" id="Seg_6219" s="T753">ugandə</ta>
            <ta e="T755" id="Seg_6220" s="T754">kös</ta>
            <ta e="T756" id="Seg_6221" s="T755">iʔgö</ta>
            <ta e="T757" id="Seg_6222" s="T756">barəʔ-lia</ta>
            <ta e="T758" id="Seg_6223" s="T757">bobti</ta>
            <ta e="T759" id="Seg_6224" s="T758">măna</ta>
            <ta e="T762" id="Seg_6225" s="T761">bobti</ta>
            <ta e="T763" id="Seg_6226" s="T762">tože</ta>
            <ta e="T765" id="Seg_6227" s="T764">i</ta>
            <ta e="T766" id="Seg_6228" s="T765">tănar-bi</ta>
            <ta e="T767" id="Seg_6229" s="T766">dĭ</ta>
            <ta e="T768" id="Seg_6230" s="T767">a</ta>
            <ta e="T769" id="Seg_6231" s="T768">măn</ta>
            <ta e="T770" id="Seg_6232" s="T769">bos-pə</ta>
            <ta e="T771" id="Seg_6233" s="T770">em-bie-m</ta>
            <ta e="T772" id="Seg_6234" s="T771">traktăr-zi</ta>
            <ta e="T773" id="Seg_6235" s="T772">deʔ-pi-ʔi</ta>
            <ta e="T774" id="Seg_6236" s="T773">kajak</ta>
            <ta e="T775" id="Seg_6237" s="T774">i-bie-m</ta>
            <ta e="T776" id="Seg_6238" s="T775">traktăr-də</ta>
            <ta e="T777" id="Seg_6239" s="T776">tăŋ</ta>
            <ta e="T779" id="Seg_6240" s="T778">a</ta>
            <ta e="T780" id="Seg_6241" s="T779">măn</ta>
            <ta e="T781" id="Seg_6242" s="T780">bar</ta>
            <ta e="T782" id="Seg_6243" s="T781">pa</ta>
            <ta e="T783" id="Seg_6244" s="T782">tažer-bia-m</ta>
            <ta e="T784" id="Seg_6245" s="T783">bü-j-le</ta>
            <ta e="T785" id="Seg_6246" s="T784">mĭm-bie-m</ta>
            <ta e="T786" id="Seg_6247" s="T785">tüžöj-də</ta>
            <ta e="T787" id="Seg_6248" s="T786">nu-ʔ</ta>
            <ta e="T788" id="Seg_6249" s="T787">mĭ-bie-m</ta>
            <ta e="T789" id="Seg_6250" s="T788">surdə-bia-m</ta>
            <ta e="T790" id="Seg_6251" s="T789">dĭgəttə</ta>
            <ta e="T791" id="Seg_6252" s="T790">amno-bia-m</ta>
            <ta e="T792" id="Seg_6253" s="T791">amor-zittə</ta>
            <ta e="T793" id="Seg_6254" s="T792">i</ta>
            <ta e="T794" id="Seg_6255" s="T793">tenö-laʔbə-m</ta>
            <ta e="T795" id="Seg_6256" s="T794">dĭ</ta>
            <ta e="T796" id="Seg_6257" s="T795">Jelʼa</ta>
            <ta e="T797" id="Seg_6258" s="T796">măna</ta>
            <ta e="T798" id="Seg_6259" s="T797">bar</ta>
            <ta e="T799" id="Seg_6260" s="T798">šaːm-bi</ta>
            <ta e="T800" id="Seg_6261" s="T799">aktʼa-t</ta>
            <ta e="T801" id="Seg_6262" s="T800">nagur</ta>
            <ta e="T802" id="Seg_6263" s="T801">šălkovej</ta>
            <ta e="T804" id="Seg_6264" s="T803">i-bi</ta>
            <ta e="T805" id="Seg_6265" s="T804">a</ta>
            <ta e="T806" id="Seg_6266" s="T805">măna</ta>
            <ta e="T807" id="Seg_6267" s="T806">i-bi</ta>
            <ta e="T808" id="Seg_6268" s="T807">onʼiʔ</ta>
            <ta e="T809" id="Seg_6269" s="T808">šălkovej</ta>
            <ta e="T810" id="Seg_6270" s="T809">šide</ta>
            <ta e="T811" id="Seg_6271" s="T810">bʼeʔ</ta>
            <ta e="T812" id="Seg_6272" s="T811">sumna</ta>
            <ta e="T813" id="Seg_6273" s="T812">dĭʔ-nə</ta>
            <ta e="T815" id="Seg_6274" s="T814">i-zittə</ta>
            <ta e="T816" id="Seg_6275" s="T815">măna</ta>
            <ta e="T817" id="Seg_6276" s="T816">onʼiʔ</ta>
            <ta e="T818" id="Seg_6277" s="T817">šălkovej</ta>
            <ta e="T819" id="Seg_6278" s="T818">i</ta>
            <ta e="T820" id="Seg_6279" s="T819">bʼeʔ</ta>
            <ta e="T821" id="Seg_6280" s="T820">sumna</ta>
            <ta e="T822" id="Seg_6281" s="T821">kăpʼejka-ʔi</ta>
            <ta e="T823" id="Seg_6282" s="T822">măn</ta>
            <ta e="T824" id="Seg_6283" s="T823">teinen</ta>
            <ta e="T825" id="Seg_6284" s="T824">bü-nə</ta>
            <ta e="T826" id="Seg_6285" s="T825">kam-bia-m</ta>
            <ta e="T827" id="Seg_6286" s="T826">da</ta>
            <ta e="T828" id="Seg_6287" s="T827">ugandə</ta>
            <ta e="T829" id="Seg_6288" s="T828">tăŋ</ta>
            <ta e="T830" id="Seg_6289" s="T829">kăn-nam-bi</ta>
            <ta e="T831" id="Seg_6290" s="T830">măn</ta>
            <ta e="T832" id="Seg_6291" s="T831">par-bia-m</ta>
            <ta e="T833" id="Seg_6292" s="T832">baltu</ta>
            <ta e="T834" id="Seg_6293" s="T833">i-bie-m</ta>
            <ta e="T835" id="Seg_6294" s="T834">baltu-zi</ta>
            <ta e="T836" id="Seg_6295" s="T835">jaʔ-pia-m</ta>
            <ta e="T837" id="Seg_6296" s="T836">jaʔ-pia-m</ta>
            <ta e="T838" id="Seg_6297" s="T837">bar</ta>
            <ta e="T839" id="Seg_6298" s="T838">sĭre</ta>
            <ta e="T842" id="Seg_6299" s="T841">kăn-nam-bi</ta>
            <ta e="T843" id="Seg_6300" s="T842">ugandə</ta>
            <ta e="T844" id="Seg_6301" s="T843">šĭšəge</ta>
            <ta e="T845" id="Seg_6302" s="T844">erte</ta>
            <ta e="T846" id="Seg_6303" s="T845">i-bi</ta>
            <ta e="T847" id="Seg_6304" s="T846">maʔ-nʼi</ta>
            <ta e="T848" id="Seg_6305" s="T847">šo-bia-m</ta>
            <ta e="T849" id="Seg_6306" s="T848">šiʔnʼileʔ</ta>
            <ta e="T850" id="Seg_6307" s="T849">edəʔ-pie-m</ta>
            <ta e="T851" id="Seg_6308" s="T850">i</ta>
            <ta e="T852" id="Seg_6309" s="T851">bü</ta>
            <ta e="T853" id="Seg_6310" s="T852">deʔ-pie-m</ta>
            <ta e="T854" id="Seg_6311" s="T853">pʼeš-tə</ta>
            <ta e="T858" id="Seg_6312" s="T857">sʼa-bia-m</ta>
            <ta e="T859" id="Seg_6313" s="T858">amno-bia-m</ta>
            <ta e="T860" id="Seg_6314" s="T859">i</ta>
            <ta e="T861" id="Seg_6315" s="T860">emneʔpəm</ta>
            <ta e="T862" id="Seg_6316" s="T861">dĭgəttə</ta>
            <ta e="T863" id="Seg_6317" s="T862">šo-bi-ʔi</ta>
            <ta e="T864" id="Seg_6318" s="T863">i</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_6319" s="T0">miʔ</ta>
            <ta e="T2" id="Seg_6320" s="T1">bar</ta>
            <ta e="T3" id="Seg_6321" s="T2">Sverdlovsk-Kən</ta>
            <ta e="T4" id="Seg_6322" s="T3">amno-bieʔ</ta>
            <ta e="T7" id="Seg_6323" s="T6">baza-j</ta>
            <ta e="T8" id="Seg_6324" s="T7">baza-j</ta>
            <ta e="T9" id="Seg_6325" s="T8">süjö-Tə</ta>
            <ta e="T10" id="Seg_6326" s="T9">amno-bi-bAʔ</ta>
            <ta e="T11" id="Seg_6327" s="T10">dĭgəttə</ta>
            <ta e="T12" id="Seg_6328" s="T11">nʼergö-bi-bAʔ</ta>
            <ta e="T13" id="Seg_6329" s="T12">aʔtʼi-Kən</ta>
            <ta e="T14" id="Seg_6330" s="T13">ej</ta>
            <ta e="T15" id="Seg_6331" s="T14">mo-liA</ta>
            <ta e="T17" id="Seg_6332" s="T16">nʼergö-zittə</ta>
            <ta e="T18" id="Seg_6333" s="T17">bor-zəbi</ta>
            <ta e="T19" id="Seg_6334" s="T18">bar</ta>
            <ta e="T22" id="Seg_6335" s="T21">nu-laʔbə</ta>
            <ta e="T23" id="Seg_6336" s="T22">dĭgəttə</ta>
            <ta e="T24" id="Seg_6337" s="T23">šo-bi-bAʔ</ta>
            <ta e="T25" id="Seg_6338" s="T24">Krasnojarskə-Tə</ta>
            <ta e="T26" id="Seg_6339" s="T25">dĭn</ta>
            <ta e="T27" id="Seg_6340" s="T26">amnə-bi-bAʔ</ta>
            <ta e="T28" id="Seg_6341" s="T27">dĭn</ta>
            <ta e="T29" id="Seg_6342" s="T28">amno-bi-m</ta>
            <ta e="T30" id="Seg_6343" s="T29">sejʔpü</ta>
            <ta e="T31" id="Seg_6344" s="T30">tʼala</ta>
            <ta e="T32" id="Seg_6345" s="T31">amno-bi-m</ta>
            <ta e="T33" id="Seg_6346" s="T32">dĭgəttə</ta>
            <ta e="T34" id="Seg_6347" s="T33">amnə-bi-m</ta>
            <ta e="T35" id="Seg_6348" s="T34">baza-j</ta>
            <ta e="T36" id="Seg_6349" s="T35">aʔtʼi-Tə</ta>
            <ta e="T37" id="Seg_6350" s="T36">šo-bi-m</ta>
            <ta e="T38" id="Seg_6351" s="T37">Ujar-Tə</ta>
            <ta e="T39" id="Seg_6352" s="T38">dĭgəttə</ta>
            <ta e="T40" id="Seg_6353" s="T39">Ujar-gəʔ</ta>
            <ta e="T41" id="Seg_6354" s="T40">šo-bi-m</ta>
            <ta e="T43" id="Seg_6355" s="T42">amnə-bi-m</ta>
            <ta e="T44" id="Seg_6356" s="T43">avtobus-Tə</ta>
            <ta e="T45" id="Seg_6357" s="T44">šo-bi-m</ta>
            <ta e="T46" id="Seg_6358" s="T45">Săjanskə-n</ta>
            <ta e="T47" id="Seg_6359" s="T46">dön</ta>
            <ta e="T48" id="Seg_6360" s="T47">amno-bi-m</ta>
            <ta e="T51" id="Seg_6361" s="T50">šide</ta>
            <ta e="T52" id="Seg_6362" s="T51">tʼala</ta>
            <ta e="T53" id="Seg_6363" s="T52">dĭgəttə</ta>
            <ta e="T54" id="Seg_6364" s="T53">Abalakovo-Tə</ta>
            <ta e="T55" id="Seg_6365" s="T54">šo-bi-m</ta>
            <ta e="T56" id="Seg_6366" s="T55">i</ta>
            <ta e="T57" id="Seg_6367" s="T56">tănan</ta>
            <ta e="T58" id="Seg_6368" s="T57">šo-bi-m</ta>
            <ta e="T59" id="Seg_6369" s="T58">tănan</ta>
            <ta e="T60" id="Seg_6370" s="T59">šo-bi-m</ta>
            <ta e="T61" id="Seg_6371" s="T60">tʼăbaktər-bi-bAʔ</ta>
            <ta e="T62" id="Seg_6372" s="T61">dĭgəttə</ta>
            <ta e="T63" id="Seg_6373" s="T62">tăn</ta>
            <ta e="T64" id="Seg_6374" s="T63">măn</ta>
            <ta e="T65" id="Seg_6375" s="T64">măna</ta>
            <ta e="T67" id="Seg_6376" s="T66">măn-lV-l</ta>
            <ta e="T68" id="Seg_6377" s="T67">amnə-ʔ</ta>
            <ta e="T69" id="Seg_6378" s="T68">amor-KAʔ</ta>
            <ta e="T71" id="Seg_6379" s="T70">püjö-liA-l</ta>
            <ta e="T72" id="Seg_6380" s="T71">padʼi=ka</ta>
            <ta e="T73" id="Seg_6381" s="T72">dĭgəttə</ta>
            <ta e="T74" id="Seg_6382" s="T73">măn</ta>
            <ta e="T75" id="Seg_6383" s="T74">amnə-bi-m</ta>
            <ta e="T76" id="Seg_6384" s="T75">amor-bi-m</ta>
            <ta e="T77" id="Seg_6385" s="T76">kan-bi-m</ta>
            <ta e="T78" id="Seg_6386" s="T77">bos-də</ta>
            <ta e="T79" id="Seg_6387" s="T78">fatʼera-t-ziʔ</ta>
            <ta e="T80" id="Seg_6388" s="T79">onʼiʔ</ta>
            <ta e="T81" id="Seg_6389" s="T80">onʼiʔ</ta>
            <ta e="T82" id="Seg_6390" s="T81">raz</ta>
            <ta e="T83" id="Seg_6391" s="T82">miʔ</ta>
            <ta e="T84" id="Seg_6392" s="T83">kan-bi-bAʔ</ta>
            <ta e="T85" id="Seg_6393" s="T84">nagur</ta>
            <ta e="T86" id="Seg_6394" s="T85">ne</ta>
            <ta e="T87" id="Seg_6395" s="T86">Zaozʼorka-Tə</ta>
            <ta e="T88" id="Seg_6396" s="T87">dĭgəttə</ta>
            <ta e="T89" id="Seg_6397" s="T88">dĭʔə</ta>
            <ta e="T90" id="Seg_6398" s="T89">baza-j</ta>
            <ta e="T91" id="Seg_6399" s="T90">aʔtʼi-Tə</ta>
            <ta e="T93" id="Seg_6400" s="T92">amnə-bi-bAʔ</ta>
            <ta e="T94" id="Seg_6401" s="T93">kan-bi-bAʔ</ta>
            <ta e="T95" id="Seg_6402" s="T94">Ujar-Tə</ta>
            <ta e="T96" id="Seg_6403" s="T95">dĭn</ta>
            <ta e="T97" id="Seg_6404" s="T96">kudaj-Tə</ta>
            <ta e="T98" id="Seg_6405" s="T97">numan üzə-zittə</ta>
            <ta e="T99" id="Seg_6406" s="T98">dĭn</ta>
            <ta e="T100" id="Seg_6407" s="T99">kudaj-Tə</ta>
            <ta e="T101" id="Seg_6408" s="T100">numan üzə-lAʔ-bi-bAʔ</ta>
            <ta e="T102" id="Seg_6409" s="T101">dĭgəttə</ta>
            <ta e="T103" id="Seg_6410" s="T102">par-luʔbdə-bi-bAʔ</ta>
            <ta e="T104" id="Seg_6411" s="T103">bazoʔ</ta>
            <ta e="T106" id="Seg_6412" s="T105">döbər</ta>
            <ta e="T107" id="Seg_6413" s="T106">Zaozʼorka-Tə</ta>
            <ta e="T108" id="Seg_6414" s="T107">dĭn</ta>
            <ta e="T109" id="Seg_6415" s="T108">dön</ta>
            <ta e="T110" id="Seg_6416" s="T109">šaʔ-bieʔ</ta>
            <ta e="T111" id="Seg_6417" s="T110">iʔgö</ta>
            <ta e="T112" id="Seg_6418" s="T111">tʼăbaktər-lAʔ-bi-bAʔ</ta>
            <ta e="T113" id="Seg_6419" s="T112">dĭgəttə</ta>
            <ta e="T114" id="Seg_6420" s="T113">kozʼa</ta>
            <ta e="T115" id="Seg_6421" s="T114">kan-bi</ta>
            <ta e="T116" id="Seg_6422" s="T115">avtobus</ta>
            <ta e="T117" id="Seg_6423" s="T116">măndo-r-zittə</ta>
            <ta e="T118" id="Seg_6424" s="T117">šo-bi</ta>
            <ta e="T119" id="Seg_6425" s="T118">da</ta>
            <ta e="T120" id="Seg_6426" s="T119">măndede</ta>
            <ta e="T868" id="Seg_6427" s="T120">kan-lAʔ</ta>
            <ta e="T121" id="Seg_6428" s="T868">tʼür-bi</ta>
            <ta e="T122" id="Seg_6429" s="T121">a</ta>
            <ta e="T123" id="Seg_6430" s="T122">miʔ</ta>
            <ta e="T124" id="Seg_6431" s="T123">oʔbdə-bi-bAʔ</ta>
            <ta e="T125" id="Seg_6432" s="T124">da</ta>
            <ta e="T126" id="Seg_6433" s="T125">kan-bi-bAʔ</ta>
            <ta e="T127" id="Seg_6434" s="T126">nʼiʔdə</ta>
            <ta e="T128" id="Seg_6435" s="T127">dön</ta>
            <ta e="T129" id="Seg_6436" s="T128">avtobus</ta>
            <ta e="T130" id="Seg_6437" s="T129">šonə-gA</ta>
            <ta e="T131" id="Seg_6438" s="T130">amnəl-də-bi</ta>
            <ta e="T132" id="Seg_6439" s="T131">miʔ</ta>
            <ta e="T133" id="Seg_6440" s="T132">da</ta>
            <ta e="T134" id="Seg_6441" s="T133">perejaslăvka-Tə</ta>
            <ta e="T135" id="Seg_6442" s="T134">šo-bi-bAʔ</ta>
            <ta e="T136" id="Seg_6443" s="T135">dĭn</ta>
            <ta e="T139" id="Seg_6444" s="T137">miʔnʼibeʔ</ta>
            <ta e="T140" id="Seg_6445" s="T139">dĭn</ta>
            <ta e="T141" id="Seg_6446" s="T140">miʔ</ta>
            <ta e="T142" id="Seg_6447" s="T141">dĭn</ta>
            <ta e="T143" id="Seg_6448" s="T142">üzə-bi-bAʔ</ta>
            <ta e="T144" id="Seg_6449" s="T143">i</ta>
            <ta e="T145" id="Seg_6450" s="T144">nu-bi-bAʔ</ta>
            <ta e="T146" id="Seg_6451" s="T145">măn</ta>
            <ta e="T147" id="Seg_6452" s="T146">măn-ntə-m</ta>
            <ta e="T148" id="Seg_6453" s="T147">ĭmbi</ta>
            <ta e="T149" id="Seg_6454" s="T148">nu-laʔbə-zittə</ta>
            <ta e="T150" id="Seg_6455" s="T149">kan-žə-bAʔ</ta>
            <ta e="T151" id="Seg_6456" s="T150">üdʼi</ta>
            <ta e="T152" id="Seg_6457" s="T151">dĭgəttə</ta>
            <ta e="T153" id="Seg_6458" s="T152">kan-žə-bAʔ</ta>
            <ta e="T154" id="Seg_6459" s="T153">kandə-laʔbə-bAʔ</ta>
            <ta e="T155" id="Seg_6460" s="T154">mašina</ta>
            <ta e="T156" id="Seg_6461" s="T155">bar</ta>
            <ta e="T157" id="Seg_6462" s="T156">šonə-gA</ta>
            <ta e="T158" id="Seg_6463" s="T157">amnə-KAʔ</ta>
            <ta e="T159" id="Seg_6464" s="T158">miʔ</ta>
            <ta e="T162" id="Seg_6465" s="T161">amnə-KAʔ</ta>
            <ta e="T163" id="Seg_6466" s="T162">döbər</ta>
            <ta e="T164" id="Seg_6467" s="T163">dĭgəttə</ta>
            <ta e="T165" id="Seg_6468" s="T164">miʔ</ta>
            <ta e="T166" id="Seg_6469" s="T165">amnə-bi-bAʔ</ta>
            <ta e="T167" id="Seg_6470" s="T166">i</ta>
            <ta e="T168" id="Seg_6471" s="T167">šo-bi-bAʔ</ta>
            <ta e="T169" id="Seg_6472" s="T168">Kandəga-Tə</ta>
            <ta e="T170" id="Seg_6473" s="T169">dĭgəttə</ta>
            <ta e="T171" id="Seg_6474" s="T170">dĭn</ta>
            <ta e="T172" id="Seg_6475" s="T171">ĭššo</ta>
            <ta e="T173" id="Seg_6476" s="T172">sejʔpü</ta>
            <ta e="T175" id="Seg_6477" s="T174">ma-luʔbdə-bi</ta>
            <ta e="T176" id="Seg_6478" s="T175">Unʼer-də</ta>
            <ta e="T177" id="Seg_6479" s="T176">kan-zittə</ta>
            <ta e="T178" id="Seg_6480" s="T177">šide</ta>
            <ta e="T179" id="Seg_6481" s="T178">ne-zAŋ</ta>
            <ta e="T180" id="Seg_6482" s="T179">kan-bi-jəʔ</ta>
            <ta e="T181" id="Seg_6483" s="T180">tura-Tə</ta>
            <ta e="T182" id="Seg_6484" s="T181">a</ta>
            <ta e="T183" id="Seg_6485" s="T182">măn</ta>
            <ta e="T184" id="Seg_6486" s="T183">kan-bi-m</ta>
            <ta e="T185" id="Seg_6487" s="T184">üdʼi</ta>
            <ta e="T186" id="Seg_6488" s="T185">Unʼer-də</ta>
            <ta e="T187" id="Seg_6489" s="T186">šo-bi-m</ta>
            <ta e="T188" id="Seg_6490" s="T187">det-KAʔ</ta>
            <ta e="T189" id="Seg_6491" s="T188">măna</ta>
            <ta e="T190" id="Seg_6492" s="T189">ipek</ta>
            <ta e="T191" id="Seg_6493" s="T190">sădar-KAʔ</ta>
            <ta e="T192" id="Seg_6494" s="T191">ipek</ta>
            <ta e="T193" id="Seg_6495" s="T192">dĭ-zAŋ</ta>
            <ta e="T194" id="Seg_6496" s="T193">măn-ntə</ta>
            <ta e="T196" id="Seg_6497" s="T195">kan-ə-ʔ</ta>
            <ta e="T197" id="Seg_6498" s="T196">măgăzin-Tə</ta>
            <ta e="T199" id="Seg_6499" s="T198">dĭn</ta>
            <ta e="T200" id="Seg_6500" s="T199">i-ʔ</ta>
            <ta e="T201" id="Seg_6501" s="T200">măna</ta>
            <ta e="T202" id="Seg_6502" s="T201">iʔgö</ta>
            <ta e="T203" id="Seg_6503" s="T202">ej</ta>
            <ta e="T204" id="Seg_6504" s="T203">kereʔ</ta>
            <ta e="T205" id="Seg_6505" s="T204">šide</ta>
            <ta e="T206" id="Seg_6506" s="T205">gram</ta>
            <ta e="T207" id="Seg_6507" s="T206">mĭ-bi-lAʔ</ta>
            <ta e="T208" id="Seg_6508" s="T207">dĭ</ta>
            <ta e="T209" id="Seg_6509" s="T208">măna</ta>
            <ta e="T210" id="Seg_6510" s="T209">šide</ta>
            <ta e="T211" id="Seg_6511" s="T210">gram</ta>
            <ta e="T212" id="Seg_6512" s="T211">edə-bi</ta>
            <ta e="T213" id="Seg_6513" s="T212">măn</ta>
            <ta e="T214" id="Seg_6514" s="T213">i-bi-m</ta>
            <ta e="T215" id="Seg_6515" s="T214">aktʼa</ta>
            <ta e="T216" id="Seg_6516" s="T215">mĭ-bi-m</ta>
            <ta e="T217" id="Seg_6517" s="T216">am-bi-m</ta>
            <ta e="T218" id="Seg_6518" s="T217">i</ta>
            <ta e="T219" id="Seg_6519" s="T218">kan-bi-m</ta>
            <ta e="T220" id="Seg_6520" s="T219">bü</ta>
            <ta e="T221" id="Seg_6521" s="T220">bĭs-bi-m</ta>
            <ta e="T222" id="Seg_6522" s="T221">dĭgəttə</ta>
            <ta e="T223" id="Seg_6523" s="T222">mašina</ta>
            <ta e="T224" id="Seg_6524" s="T223">bĭdə-bi</ta>
            <ta e="T225" id="Seg_6525" s="T224">măn</ta>
            <ta e="T227" id="Seg_6526" s="T226">amnə-bi-m</ta>
            <ta e="T228" id="Seg_6527" s="T227">dĭgəttə</ta>
            <ta e="T229" id="Seg_6528" s="T228">üzə-bi-m</ta>
            <ta e="T230" id="Seg_6529" s="T229">šide</ta>
            <ta e="T231" id="Seg_6530" s="T230">kuza</ta>
            <ta e="T232" id="Seg_6531" s="T231">nu-gA-jəʔ</ta>
            <ta e="T233" id="Seg_6532" s="T232">măja-Kən</ta>
            <ta e="T234" id="Seg_6533" s="T233">măn</ta>
            <ta e="T235" id="Seg_6534" s="T234">dĭbər</ta>
            <ta e="T237" id="Seg_6535" s="T236">sʼa-bi-m</ta>
            <ta e="T238" id="Seg_6536" s="T237">kan-žə-bəj</ta>
            <ta e="T239" id="Seg_6537" s="T238">obbere</ta>
            <ta e="T240" id="Seg_6538" s="T239">dʼok</ta>
            <ta e="T241" id="Seg_6539" s="T240">miʔ</ta>
            <ta e="T242" id="Seg_6540" s="T241">edəʔ-laʔbə</ta>
            <ta e="T243" id="Seg_6541" s="T242">mašina</ta>
            <ta e="T244" id="Seg_6542" s="T243">mašina-ziʔ</ta>
            <ta e="T245" id="Seg_6543" s="T244">kan-žə-bəj</ta>
            <ta e="T246" id="Seg_6544" s="T245">dĭgəttə</ta>
            <ta e="T247" id="Seg_6545" s="T246">mašina</ta>
            <ta e="T248" id="Seg_6546" s="T247">šo-bi</ta>
            <ta e="T251" id="Seg_6547" s="T250">amnə-bi-m</ta>
            <ta e="T252" id="Seg_6548" s="T251">i</ta>
            <ta e="T253" id="Seg_6549" s="T252">Săjanskəj-Tə</ta>
            <ta e="T254" id="Seg_6550" s="T253">šo-bi-m</ta>
            <ta e="T255" id="Seg_6551" s="T254">dĭgəttə</ta>
            <ta e="T256" id="Seg_6552" s="T255">nĭn</ta>
            <ta e="T258" id="Seg_6553" s="T257">i-bi-m</ta>
            <ta e="T259" id="Seg_6554" s="T258">ipek</ta>
            <ta e="T260" id="Seg_6555" s="T259">sĭreʔp</ta>
            <ta e="T261" id="Seg_6556" s="T260">Vanʼka</ta>
            <ta e="T262" id="Seg_6557" s="T261">bălʼnʼitsa-Kən</ta>
            <ta e="T263" id="Seg_6558" s="T262">iʔbö-laʔbə-bi</ta>
            <ta e="T264" id="Seg_6559" s="T263">kun-bi-m</ta>
            <ta e="T265" id="Seg_6560" s="T264">dĭ-Tə</ta>
            <ta e="T266" id="Seg_6561" s="T265">măna</ta>
            <ta e="T267" id="Seg_6562" s="T266">dĭn</ta>
            <ta e="T268" id="Seg_6563" s="T267">nörbə-laʔbə-jəʔ</ta>
            <ta e="T269" id="Seg_6564" s="T268">dön</ta>
            <ta e="T270" id="Seg_6565" s="T269">i-KAʔ</ta>
            <ta e="T271" id="Seg_6566" s="T270">miʔ</ta>
            <ta e="T272" id="Seg_6567" s="T271">mašina</ta>
            <ta e="T273" id="Seg_6568" s="T272">kan-ə-ʔ</ta>
            <ta e="T274" id="Seg_6569" s="T273">măn</ta>
            <ta e="T275" id="Seg_6570" s="T274">nuʔmə-bi-m</ta>
            <ta e="T276" id="Seg_6571" s="T275">gijen=də</ta>
            <ta e="T277" id="Seg_6572" s="T276">ej</ta>
            <ta e="T278" id="Seg_6573" s="T277">ku-bi-m</ta>
            <ta e="T279" id="Seg_6574" s="T278">dĭgəttə</ta>
            <ta e="T280" id="Seg_6575" s="T279">kan-bi-m</ta>
            <ta e="T281" id="Seg_6576" s="T280">üdʼi</ta>
            <ta e="T282" id="Seg_6577" s="T281">dĭgəttə</ta>
            <ta e="T283" id="Seg_6578" s="T282">bazoʔ</ta>
            <ta e="T284" id="Seg_6579" s="T283">mašina</ta>
            <ta e="T285" id="Seg_6580" s="T284">šonə-gA</ta>
            <ta e="T286" id="Seg_6581" s="T285">măn</ta>
            <ta e="T287" id="Seg_6582" s="T286">amnə-bi-m</ta>
            <ta e="T288" id="Seg_6583" s="T287">malinovka-Tə</ta>
            <ta e="T289" id="Seg_6584" s="T288">šo-bi-m</ta>
            <ta e="T290" id="Seg_6585" s="T289">dĭgəttə</ta>
            <ta e="T291" id="Seg_6586" s="T290">bazoʔ</ta>
            <ta e="T292" id="Seg_6587" s="T291">üjü-ziʔ</ta>
            <ta e="T293" id="Seg_6588" s="T292">kan-bi-m</ta>
            <ta e="T294" id="Seg_6589" s="T293">dĭgəttə</ta>
            <ta e="T295" id="Seg_6590" s="T294">bazoʔ</ta>
            <ta e="T296" id="Seg_6591" s="T295">mašina</ta>
            <ta e="T297" id="Seg_6592" s="T296">bĭdə-bi</ta>
            <ta e="T298" id="Seg_6593" s="T297">i</ta>
            <ta e="T299" id="Seg_6594" s="T298">maʔ-gəndə</ta>
            <ta e="T300" id="Seg_6595" s="T299">šo-bi-m</ta>
            <ta e="T301" id="Seg_6596" s="T300">kamən</ta>
            <ta e="T302" id="Seg_6597" s="T301">Matvejev</ta>
            <ta e="T303" id="Seg_6598" s="T302">šo-bi</ta>
            <ta e="T304" id="Seg_6599" s="T303">măna</ta>
            <ta e="T306" id="Seg_6600" s="T305">tʼăbaktər-laʔbə</ta>
            <ta e="T307" id="Seg_6601" s="T306">nu-də-lAʔ</ta>
            <ta e="T308" id="Seg_6602" s="T307">a</ta>
            <ta e="T309" id="Seg_6603" s="T308">măn</ta>
            <ta e="T310" id="Seg_6604" s="T309">kazak</ta>
            <ta e="T311" id="Seg_6605" s="T310">kazak</ta>
            <ta e="T312" id="Seg_6606" s="T311">tʼăbaktər-laʔbə-m</ta>
            <ta e="T313" id="Seg_6607" s="T312">i</ta>
            <ta e="T314" id="Seg_6608" s="T313">petʼer-laʔbə-m</ta>
            <ta e="T315" id="Seg_6609" s="T314">tăn</ta>
            <ta e="T316" id="Seg_6610" s="T315">tüj</ta>
            <ta e="T317" id="Seg_6611" s="T316">nu</ta>
            <ta e="T318" id="Seg_6612" s="T317">i-gA-l</ta>
            <ta e="T319" id="Seg_6613" s="T318">a</ta>
            <ta e="T320" id="Seg_6614" s="T319">măn</ta>
            <ta e="T321" id="Seg_6615" s="T320">kazak</ta>
            <ta e="T322" id="Seg_6616" s="T321">i-gA-m</ta>
            <ta e="T323" id="Seg_6617" s="T322">dĭgəttə</ta>
            <ta e="T324" id="Seg_6618" s="T323">dĭ-zAŋ</ta>
            <ta e="T325" id="Seg_6619" s="T324">dʼije-Tə</ta>
            <ta e="T326" id="Seg_6620" s="T325">kan-bi-jəʔ</ta>
            <ta e="T327" id="Seg_6621" s="T326">Dʼelam-Tə</ta>
            <ta e="T329" id="Seg_6622" s="T328">kola</ta>
            <ta e="T330" id="Seg_6623" s="T329">dʼabə-bi-jəʔ</ta>
            <ta e="T331" id="Seg_6624" s="T330">dĭn</ta>
            <ta e="T334" id="Seg_6625" s="T333">hen-bi-jəʔ</ta>
            <ta e="T335" id="Seg_6626" s="T334">dĭgəttə</ta>
            <ta e="T336" id="Seg_6627" s="T335">onʼiʔ</ta>
            <ta e="T337" id="Seg_6628" s="T336">koʔbdo</ta>
            <ta e="T338" id="Seg_6629" s="T337">urgo</ta>
            <ta e="T339" id="Seg_6630" s="T338">bar</ta>
            <ta e="T340" id="Seg_6631" s="T339">ĭzem-luʔbdə-bi</ta>
            <ta e="T341" id="Seg_6632" s="T340">bar</ta>
            <ta e="T342" id="Seg_6633" s="T341">dĭgəttə</ta>
            <ta e="T343" id="Seg_6634" s="T342">dĭ-zAŋ</ta>
            <ta e="T344" id="Seg_6635" s="T343">šo-bi-jəʔ</ta>
            <ta e="T345" id="Seg_6636" s="T344">tura-Tə</ta>
            <ta e="T346" id="Seg_6637" s="T345">Kanokler-Tə</ta>
            <ta e="T347" id="Seg_6638" s="T346">i</ta>
            <ta e="T348" id="Seg_6639" s="T347">ne</ta>
            <ta e="T349" id="Seg_6640" s="T348">i-bi-jəʔ</ta>
            <ta e="T350" id="Seg_6641" s="T349">i</ta>
            <ta e="T351" id="Seg_6642" s="T350">dĭ-m</ta>
            <ta e="T352" id="Seg_6643" s="T351">det-bi-jəʔ</ta>
            <ta e="T353" id="Seg_6644" s="T352">bălʼnʼitsa-Tə</ta>
            <ta e="T354" id="Seg_6645" s="T353">dĭn</ta>
            <ta e="T355" id="Seg_6646" s="T354">üjü-n</ta>
            <ta e="T356" id="Seg_6647" s="T355">bar</ta>
            <ta e="T357" id="Seg_6648" s="T356">bü</ta>
            <ta e="T358" id="Seg_6649" s="T357">bĭs-bi-jəʔ</ta>
            <ta e="T359" id="Seg_6650" s="T358">i</ta>
            <ta e="T360" id="Seg_6651" s="T359">dʼubtə-bi-jəʔ</ta>
            <ta e="T361" id="Seg_6652" s="T360">tʼezer-bi-jəʔ</ta>
            <ta e="T362" id="Seg_6653" s="T361">ato</ta>
            <ta e="T363" id="Seg_6654" s="T362">dĭ</ta>
            <ta e="T364" id="Seg_6655" s="T363">mĭn-zittə</ta>
            <ta e="T365" id="Seg_6656" s="T364">ej</ta>
            <ta e="T367" id="Seg_6657" s="T366">ej</ta>
            <ta e="T368" id="Seg_6658" s="T367">mo-liA</ta>
            <ta e="T369" id="Seg_6659" s="T368">măn</ta>
            <ta e="T372" id="Seg_6660" s="T371">măndo-r-bi-m</ta>
            <ta e="T373" id="Seg_6661" s="T372">măndo-r-bi-m</ta>
            <ta e="T375" id="Seg_6662" s="T374">dĭbər</ta>
            <ta e="T376" id="Seg_6663" s="T375">pʼaŋdə-bi-m</ta>
            <ta e="T377" id="Seg_6664" s="T376">sazən</ta>
            <ta e="T378" id="Seg_6665" s="T377">dĭbər</ta>
            <ta e="T380" id="Seg_6666" s="T379">pʼaŋdə-bi-m</ta>
            <ta e="T381" id="Seg_6667" s="T380">šində</ta>
            <ta e="T383" id="Seg_6668" s="T381">măn=da</ta>
            <ta e="T384" id="Seg_6669" s="T383">ku-laːm-bi</ta>
            <ta e="T385" id="Seg_6670" s="T384">šində</ta>
            <ta e="T387" id="Seg_6671" s="T385">măn=da</ta>
            <ta e="T388" id="Seg_6672" s="T387">ej</ta>
            <ta e="T389" id="Seg_6673" s="T388">ku-laːm-bi</ta>
            <ta e="T390" id="Seg_6674" s="T389">gijen=də</ta>
            <ta e="T391" id="Seg_6675" s="T390">naga</ta>
            <ta e="T392" id="Seg_6676" s="T391">pravda-jəʔ</ta>
            <ta e="T393" id="Seg_6677" s="T392">a</ta>
            <ta e="T394" id="Seg_6678" s="T393">măn</ta>
            <ta e="T395" id="Seg_6679" s="T394">tene-bi-m</ta>
            <ta e="T396" id="Seg_6680" s="T395">i-gA</ta>
            <ta e="T397" id="Seg_6681" s="T396">vezʼdʼe</ta>
            <ta e="T398" id="Seg_6682" s="T397">pravda</ta>
            <ta e="T399" id="Seg_6683" s="T398">i-gA</ta>
            <ta e="T400" id="Seg_6684" s="T399">a</ta>
            <ta e="T401" id="Seg_6685" s="T400">dĭ-m</ta>
            <ta e="T402" id="Seg_6686" s="T401">gijen=də</ta>
            <ta e="T403" id="Seg_6687" s="T402">naga</ta>
            <ta e="T404" id="Seg_6688" s="T403">üge</ta>
            <ta e="T405" id="Seg_6689" s="T404">šanaklʼu</ta>
            <ta e="T406" id="Seg_6690" s="T405">dĭgəttə</ta>
            <ta e="T407" id="Seg_6691" s="T406">prokuror</ta>
            <ta e="T408" id="Seg_6692" s="T407">pʼaŋdə-bi</ta>
            <ta e="T409" id="Seg_6693" s="T408">gibər=də</ta>
            <ta e="T869" id="Seg_6694" s="T409">kan-lAʔ</ta>
            <ta e="T410" id="Seg_6695" s="T869">tʼür-bi</ta>
            <ta e="T411" id="Seg_6696" s="T410">ej</ta>
            <ta e="T412" id="Seg_6697" s="T411">tĭmne-m</ta>
            <ta e="T413" id="Seg_6698" s="T412">Jelʼa</ta>
            <ta e="T414" id="Seg_6699" s="T413">kuʔkubi</ta>
            <ta e="T415" id="Seg_6700" s="T414">dʼodunʼim</ta>
            <ta e="T416" id="Seg_6701" s="T415">bar</ta>
            <ta e="T417" id="Seg_6702" s="T416">lotka-Tə</ta>
            <ta e="T418" id="Seg_6703" s="T417">bü-Tə</ta>
            <ta e="T419" id="Seg_6704" s="T418">kan-bi-jəʔ</ta>
            <ta e="T420" id="Seg_6705" s="T419">dĭgəttə</ta>
            <ta e="T421" id="Seg_6706" s="T420">girgit=də</ta>
            <ta e="T422" id="Seg_6707" s="T421">koʔbdo-ziʔ</ta>
            <ta e="T423" id="Seg_6708" s="T422">xatʼeli</ta>
            <ta e="T424" id="Seg_6709" s="T423">bü-Kən</ta>
            <ta e="T426" id="Seg_6710" s="T425">a</ta>
            <ta e="T427" id="Seg_6711" s="T426">bü</ta>
            <ta e="T428" id="Seg_6712" s="T427">ugaːndə</ta>
            <ta e="T429" id="Seg_6713" s="T428">urgo</ta>
            <ta e="T430" id="Seg_6714" s="T429">dĭ-zAŋ</ta>
            <ta e="T431" id="Seg_6715" s="T430">bar</ta>
            <ta e="T432" id="Seg_6716" s="T431">xatʼeli</ta>
            <ta e="T433" id="Seg_6717" s="T432">lotka-Tə</ta>
            <ta e="T434" id="Seg_6718" s="T433">sʼa-zittə</ta>
            <ta e="T435" id="Seg_6719" s="T434">da</ta>
            <ta e="T436" id="Seg_6720" s="T435">bar</ta>
            <ta e="T437" id="Seg_6721" s="T436">nereʔ-luʔbdə-bi-jəʔ</ta>
            <ta e="T439" id="Seg_6722" s="T437">dĭgəttə</ta>
            <ta e="T440" id="Seg_6723" s="T439">dĭgəttə</ta>
            <ta e="T441" id="Seg_6724" s="T440">dĭn</ta>
            <ta e="T442" id="Seg_6725" s="T441">nereʔ-luʔbdə-bi-jəʔ</ta>
            <ta e="T443" id="Seg_6726" s="T442">oʔbdə-bi</ta>
            <ta e="T445" id="Seg_6727" s="T444">ku-liA-t</ta>
            <ta e="T446" id="Seg_6728" s="T445">što</ta>
            <ta e="T447" id="Seg_6729" s="T446">dʼoduni</ta>
            <ta e="T448" id="Seg_6730" s="T447">kü-bi</ta>
            <ta e="T450" id="Seg_6731" s="T449">Mitrăfan-ə-n</ta>
            <ta e="T451" id="Seg_6732" s="T450">tura-t</ta>
            <ta e="T452" id="Seg_6733" s="T451">ĭššo</ta>
            <ta e="T453" id="Seg_6734" s="T452">nu-gA</ta>
            <ta e="T454" id="Seg_6735" s="T453">i</ta>
            <ta e="T456" id="Seg_6736" s="T455">i</ta>
            <ta e="T457" id="Seg_6737" s="T456">gurun</ta>
            <ta e="T459" id="Seg_6738" s="T458">tura</ta>
            <ta e="T460" id="Seg_6739" s="T459">ĭššo</ta>
            <ta e="T461" id="Seg_6740" s="T460">nu-gA</ta>
            <ta e="T466" id="Seg_6741" s="T465">tura</ta>
            <ta e="T467" id="Seg_6742" s="T466">ĭššo</ta>
            <ta e="T468" id="Seg_6743" s="T467">nu-gA</ta>
            <ta e="T471" id="Seg_6744" s="T470">tura</ta>
            <ta e="T472" id="Seg_6745" s="T471">ĭššo</ta>
            <ta e="T473" id="Seg_6746" s="T472">nu-gA</ta>
            <ta e="T476" id="Seg_6747" s="T475">tura</ta>
            <ta e="T477" id="Seg_6748" s="T476">ĭššo</ta>
            <ta e="T478" id="Seg_6749" s="T477">nu-gA</ta>
            <ta e="T479" id="Seg_6750" s="T478">i</ta>
            <ta e="T481" id="Seg_6751" s="T480">Vlas-Tə</ta>
            <ta e="T482" id="Seg_6752" s="T481">ĭššo</ta>
            <ta e="T483" id="Seg_6753" s="T482">tura</ta>
            <ta e="T484" id="Seg_6754" s="T483">nu-gA</ta>
            <ta e="T485" id="Seg_6755" s="T484">i</ta>
            <ta e="T489" id="Seg_6756" s="T488">ĭššo</ta>
            <ta e="T490" id="Seg_6757" s="T489">tura</ta>
            <ta e="T491" id="Seg_6758" s="T490">nu-gA</ta>
            <ta e="T492" id="Seg_6759" s="T491">kamən</ta>
            <ta e="T494" id="Seg_6760" s="T493">šo-bi</ta>
            <ta e="T495" id="Seg_6761" s="T494">tʼăbaktər-zittə</ta>
            <ta e="T496" id="Seg_6762" s="T495">döbər</ta>
            <ta e="T497" id="Seg_6763" s="T496">dön</ta>
            <ta e="T499" id="Seg_6764" s="T498">sumna</ta>
            <ta e="T500" id="Seg_6765" s="T499">biəʔ</ta>
            <ta e="T501" id="Seg_6766" s="T500">tura</ta>
            <ta e="T502" id="Seg_6767" s="T501">i-bi</ta>
            <ta e="T506" id="Seg_6768" s="T505">kü-laːm-bi</ta>
            <ta e="T507" id="Seg_6769" s="T506">dĭ</ta>
            <ta e="T508" id="Seg_6770" s="T507">kan-bi</ta>
            <ta e="T509" id="Seg_6771" s="T508">Permʼakovo-Tə</ta>
            <ta e="T510" id="Seg_6772" s="T509">dĭn</ta>
            <ta e="T511" id="Seg_6773" s="T510">ara</ta>
            <ta e="T512" id="Seg_6774" s="T511">bĭs-bi</ta>
            <ta e="T513" id="Seg_6775" s="T512">dĭn</ta>
            <ta e="T515" id="Seg_6776" s="T514">i-bi</ta>
            <ta e="T516" id="Seg_6777" s="T515">onʼiʔ</ta>
            <ta e="T518" id="Seg_6778" s="T517">i</ta>
            <ta e="T519" id="Seg_6779" s="T518">ne-t-ziʔ</ta>
            <ta e="T520" id="Seg_6780" s="T519">šo-bi</ta>
            <ta e="T521" id="Seg_6781" s="T520">dĭgəttə</ta>
            <ta e="T522" id="Seg_6782" s="T521">kan-bi</ta>
            <ta e="T523" id="Seg_6783" s="T522">krospa-jəʔ-Tə</ta>
            <ta e="T524" id="Seg_6784" s="T523">dĭgəttə</ta>
            <ta e="T525" id="Seg_6785" s="T524">kămnə-liA</ta>
            <ta e="T526" id="Seg_6786" s="T525">da</ta>
            <ta e="T527" id="Seg_6787" s="T526">kudo-nzə-liA</ta>
            <ta e="T528" id="Seg_6788" s="T527">bĭs-ə-ʔ</ta>
            <ta e="T529" id="Seg_6789" s="T528">nüke-m</ta>
            <ta e="T530" id="Seg_6790" s="T529">bĭs-ə-ʔ</ta>
            <ta e="T531" id="Seg_6791" s="T530">nüke-m</ta>
            <ta e="T532" id="Seg_6792" s="T531">a</ta>
            <ta e="T533" id="Seg_6793" s="T532">măn</ta>
            <ta e="T534" id="Seg_6794" s="T533">aba-m</ta>
            <ta e="T535" id="Seg_6795" s="T534">nu-gA</ta>
            <ta e="T536" id="Seg_6796" s="T535">da</ta>
            <ta e="T537" id="Seg_6797" s="T536">măndo-laʔbə</ta>
            <ta e="T538" id="Seg_6798" s="T537">da</ta>
            <ta e="T539" id="Seg_6799" s="T538">kakənar-laʔbə</ta>
            <ta e="T540" id="Seg_6800" s="T539">măn</ta>
            <ta e="T541" id="Seg_6801" s="T540">noʔ</ta>
            <ta e="T542" id="Seg_6802" s="T541">hʼaʔ-bi-m</ta>
            <ta e="T543" id="Seg_6803" s="T542">dĭgəttə</ta>
            <ta e="T544" id="Seg_6804" s="T543">oʔbdə-bi-m</ta>
            <ta e="T545" id="Seg_6805" s="T544">dĭgəttə</ta>
            <ta e="T546" id="Seg_6806" s="T545">stog-də</ta>
            <ta e="T547" id="Seg_6807" s="T546">hen-bi-m</ta>
            <ta e="T548" id="Seg_6808" s="T547">dĭ</ta>
            <ta e="T549" id="Seg_6809" s="T548">dʼüʔpi</ta>
            <ta e="T550" id="Seg_6810" s="T549">i-bi</ta>
            <ta e="T551" id="Seg_6811" s="T550">dĭgəttə</ta>
            <ta e="T552" id="Seg_6812" s="T551">nagur</ta>
            <ta e="T554" id="Seg_6813" s="T553">tʼala</ta>
            <ta e="T555" id="Seg_6814" s="T554">ej</ta>
            <ta e="T556" id="Seg_6815" s="T555">kan-bi-m</ta>
            <ta e="T557" id="Seg_6816" s="T556">ugaːndə</ta>
            <ta e="T558" id="Seg_6817" s="T557">dʼibige</ta>
            <ta e="T559" id="Seg_6818" s="T558">mo-laːm-bi</ta>
            <ta e="T562" id="Seg_6819" s="T561">măn</ta>
            <ta e="T563" id="Seg_6820" s="T562">šide</ta>
            <ta e="T564" id="Seg_6821" s="T563">ne</ta>
            <ta e="T565" id="Seg_6822" s="T564">i-bi-m</ta>
            <ta e="T566" id="Seg_6823" s="T565">kan-bi-m</ta>
            <ta e="T567" id="Seg_6824" s="T566">bar</ta>
            <ta e="T568" id="Seg_6825" s="T567">koː-bi</ta>
            <ta e="T569" id="Seg_6826" s="T568">da</ta>
            <ta e="T570" id="Seg_6827" s="T569">bazoʔ</ta>
            <ta e="T571" id="Seg_6828" s="T570">hen-bi-bAʔ</ta>
            <ta e="T572" id="Seg_6829" s="T571">kuvas</ta>
            <ta e="T574" id="Seg_6830" s="T573">stog</ta>
            <ta e="T575" id="Seg_6831" s="T574">măn</ta>
            <ta e="T576" id="Seg_6832" s="T575">amno-bi-m</ta>
            <ta e="T577" id="Seg_6833" s="T576">šide</ta>
            <ta e="T578" id="Seg_6834" s="T577">biəʔ</ta>
            <ta e="T579" id="Seg_6835" s="T578">pʼe</ta>
            <ta e="T580" id="Seg_6836" s="T579">bos-m</ta>
            <ta e="T581" id="Seg_6837" s="T580">noʔ</ta>
            <ta e="T582" id="Seg_6838" s="T581">hʼaʔ-bi-m</ta>
            <ta e="T583" id="Seg_6839" s="T582">bos-də</ta>
            <ta e="T584" id="Seg_6840" s="T583">oʔbdə-bi-m</ta>
            <ta e="T585" id="Seg_6841" s="T584">bos-də</ta>
            <ta e="T586" id="Seg_6842" s="T585">hen-bi-m</ta>
            <ta e="T587" id="Seg_6843" s="T586">ine-ziʔ</ta>
            <ta e="T588" id="Seg_6844" s="T587">maʔ-gənʼi</ta>
            <ta e="T589" id="Seg_6845" s="T588">tažor-bi-m</ta>
            <ta e="T590" id="Seg_6846" s="T589">bar</ta>
            <ta e="T591" id="Seg_6847" s="T590">tüžöj-jəʔ-m</ta>
            <ta e="T594" id="Seg_6848" s="T593">bădə-laʔbə-bi-m</ta>
            <ta e="T595" id="Seg_6849" s="T594">onʼiʔ</ta>
            <ta e="T596" id="Seg_6850" s="T595">ine</ta>
            <ta e="T597" id="Seg_6851" s="T596">i-bi-m</ta>
            <ta e="T598" id="Seg_6852" s="T597">dĭgəttə</ta>
            <ta e="T599" id="Seg_6853" s="T598">plemʼannica-t</ta>
            <ta e="T600" id="Seg_6854" s="T599">nʼi-n</ta>
            <ta e="T601" id="Seg_6855" s="T600">i-bi-m</ta>
            <ta e="T602" id="Seg_6856" s="T601">da</ta>
            <ta e="T603" id="Seg_6857" s="T602">ĭššo</ta>
            <ta e="T605" id="Seg_6858" s="T604">kan-bi-bAʔ</ta>
            <ta e="T606" id="Seg_6859" s="T605">noʔ</ta>
            <ta e="T607" id="Seg_6860" s="T606">hen-bi-bAʔ</ta>
            <ta e="T608" id="Seg_6861" s="T607">dĭgəttə</ta>
            <ta e="T609" id="Seg_6862" s="T608">šo-bi-bAʔ</ta>
            <ta e="T610" id="Seg_6863" s="T609">šo-bi-bAʔ</ta>
            <ta e="T611" id="Seg_6864" s="T610">miʔ</ta>
            <ta e="T612" id="Seg_6865" s="T611">noʔ</ta>
            <ta e="T867" id="Seg_6866" s="T613">üzə-bi </ta>
            <ta e="T615" id="Seg_6867" s="T614">dĭgəttə</ta>
            <ta e="T616" id="Seg_6868" s="T615">dʼildə-bi-bAʔ</ta>
            <ta e="T617" id="Seg_6869" s="T616">dʼildə-bi-bAʔ</ta>
            <ta e="T618" id="Seg_6870" s="T617">ej</ta>
            <ta e="T619" id="Seg_6871" s="T618">mo-liA-bAʔ</ta>
            <ta e="T620" id="Seg_6872" s="T619">dĭ</ta>
            <ta e="T621" id="Seg_6873" s="T620">măn-ntə</ta>
            <ta e="T622" id="Seg_6874" s="T621">tüjö</ta>
            <ta e="T623" id="Seg_6875" s="T622">ine</ta>
            <ta e="T626" id="Seg_6876" s="T625">körer-bi-m</ta>
            <ta e="T627" id="Seg_6877" s="T626">dĭgəttə</ta>
            <ta e="T628" id="Seg_6878" s="T627">dʼildə-lV-m</ta>
            <ta e="T629" id="Seg_6879" s="T628">a</ta>
            <ta e="T630" id="Seg_6880" s="T629">kădaʔ</ta>
            <ta e="T631" id="Seg_6881" s="T630">tăn</ta>
            <ta e="T632" id="Seg_6882" s="T631">dʼildə-lV-l</ta>
            <ta e="T633" id="Seg_6883" s="T632">a</ta>
            <ta e="T634" id="Seg_6884" s="T633">dărəʔ</ta>
            <ta e="T635" id="Seg_6885" s="T634">bar</ta>
            <ta e="T636" id="Seg_6886" s="T866">barəʔ-bi</ta>
            <ta e="T637" id="Seg_6887" s="T636">orša-jəʔ</ta>
            <ta e="T638" id="Seg_6888" s="T637">i</ta>
            <ta e="T639" id="Seg_6889" s="T638">dĭgəttə</ta>
            <ta e="T640" id="Seg_6890" s="T639">ine</ta>
            <ta e="T643" id="Seg_6891" s="T642">dĭgəttə</ta>
            <ta e="T644" id="Seg_6892" s="T643">maʔ</ta>
            <ta e="T646" id="Seg_6893" s="T645">šo-bi-bAʔ</ta>
            <ta e="T647" id="Seg_6894" s="T646">mĭn-bi-bAʔ</ta>
            <ta e="T648" id="Seg_6895" s="T647">noʔ</ta>
            <ta e="T649" id="Seg_6896" s="T648">dĭgəttə</ta>
            <ta e="T650" id="Seg_6897" s="T649">ine-m</ta>
            <ta e="T652" id="Seg_6898" s="T651">măn</ta>
            <ta e="T653" id="Seg_6899" s="T652">ugaːndə</ta>
            <ta e="T654" id="Seg_6900" s="T653">kăn-bi-m</ta>
            <ta e="T655" id="Seg_6901" s="T654">dĭgəttə</ta>
            <ta e="T656" id="Seg_6902" s="T655">kašelʼ</ta>
            <ta e="T657" id="Seg_6903" s="T656">šo-bi</ta>
            <ta e="T658" id="Seg_6904" s="T657">dĭgəttə</ta>
            <ta e="T659" id="Seg_6905" s="T658">măn</ta>
            <ta e="T660" id="Seg_6906" s="T659">sĭreʔp</ta>
            <ta e="T661" id="Seg_6907" s="T660">bar</ta>
            <ta e="T662" id="Seg_6908" s="T661">nendə-bi-m</ta>
            <ta e="T663" id="Seg_6909" s="T662">ara-Tə</ta>
            <ta e="T664" id="Seg_6910" s="T663">dĭgəttə</ta>
            <ta e="T665" id="Seg_6911" s="T664">bĭs-bi-m</ta>
            <ta e="T666" id="Seg_6912" s="T665">i</ta>
            <ta e="T667" id="Seg_6913" s="T666">gibər=də</ta>
            <ta e="T668" id="Seg_6914" s="T667">kan-lV-j</ta>
            <ta e="T669" id="Seg_6915" s="T668">kašelʼ-m</ta>
            <ta e="T673" id="Seg_6916" s="T672">girgit</ta>
            <ta e="T674" id="Seg_6917" s="T673">noʔ</ta>
            <ta e="T676" id="Seg_6918" s="T675">noʔ</ta>
            <ta e="T677" id="Seg_6919" s="T676">i-gA</ta>
            <ta e="T679" id="Seg_6920" s="T678">dĭ</ta>
            <ta e="T680" id="Seg_6921" s="T679">dudka-Tə</ta>
            <ta e="T681" id="Seg_6922" s="T680">i-zittə</ta>
            <ta e="T682" id="Seg_6923" s="T681">nadə</ta>
            <ta e="T683" id="Seg_6924" s="T682">nʼeʔbdə-zittə</ta>
            <ta e="T684" id="Seg_6925" s="T683">dĭ-m</ta>
            <ta e="T685" id="Seg_6926" s="T684">əbərdə</ta>
            <ta e="T686" id="Seg_6927" s="T685">bor</ta>
            <ta e="T687" id="Seg_6928" s="T686">amno-zittə</ta>
            <ta e="T688" id="Seg_6929" s="T687">dĭgəttə</ta>
            <ta e="T689" id="Seg_6930" s="T688">ej</ta>
            <ta e="T690" id="Seg_6931" s="T689">mo-lV-j</ta>
            <ta e="T691" id="Seg_6932" s="T690">kašelʼ</ta>
            <ta e="T692" id="Seg_6933" s="T691">nend-luʔbdə-bi</ta>
            <ta e="T693" id="Seg_6934" s="T692">i</ta>
            <ta e="T694" id="Seg_6935" s="T693">bor</ta>
            <ta e="T695" id="Seg_6936" s="T694">naga</ta>
            <ta e="T696" id="Seg_6937" s="T695">i</ta>
            <ta e="T697" id="Seg_6938" s="T696">ejü</ta>
            <ta e="T698" id="Seg_6939" s="T697">naga</ta>
            <ta e="T699" id="Seg_6940" s="T698">pa-jəʔ</ta>
            <ta e="T700" id="Seg_6941" s="T699">măna</ta>
            <ta e="T701" id="Seg_6942" s="T700">teinen</ta>
            <ta e="T702" id="Seg_6943" s="T701">ej</ta>
            <ta e="T703" id="Seg_6944" s="T702">toʔbdə-nar-zittə</ta>
            <ta e="T704" id="Seg_6945" s="T703">kabarləj</ta>
            <ta e="T706" id="Seg_6946" s="T705">dö</ta>
            <ta e="T707" id="Seg_6947" s="T706">nüdʼi-Tə</ta>
            <ta e="T708" id="Seg_6948" s="T707">pa-jəʔ</ta>
            <ta e="T709" id="Seg_6949" s="T708">măn</ta>
            <ta e="T710" id="Seg_6950" s="T709">iʔgö</ta>
            <ta e="T711" id="Seg_6951" s="T710">dö</ta>
            <ta e="T712" id="Seg_6952" s="T711">pʼe-Tə</ta>
            <ta e="T713" id="Seg_6953" s="T712">ĭššo</ta>
            <ta e="T714" id="Seg_6954" s="T713">kabar-lV-j</ta>
            <ta e="T715" id="Seg_6955" s="T714">šišəge</ta>
            <ta e="T716" id="Seg_6956" s="T715">ugaːndə</ta>
            <ta e="T717" id="Seg_6957" s="T716">iʔgö</ta>
            <ta e="T718" id="Seg_6958" s="T717">pa</ta>
            <ta e="T719" id="Seg_6959" s="T718">kan-ntə-gA-jəʔ</ta>
            <ta e="T720" id="Seg_6960" s="T719">pa-jəʔ</ta>
            <ta e="T721" id="Seg_6961" s="T720">bar</ta>
            <ta e="T722" id="Seg_6962" s="T721">surno-gəʔ</ta>
            <ta e="T723" id="Seg_6963" s="T722">sagər</ta>
            <ta e="T724" id="Seg_6964" s="T723">mo-bi-jəʔ</ta>
            <ta e="T725" id="Seg_6965" s="T724">ato</ta>
            <ta e="T726" id="Seg_6966" s="T725">sĭri</ta>
            <ta e="T727" id="Seg_6967" s="T726">i-bi-jəʔ</ta>
            <ta e="T728" id="Seg_6968" s="T727">măn</ta>
            <ta e="T729" id="Seg_6969" s="T728">büttona</ta>
            <ta e="T730" id="Seg_6970" s="T729">šomi</ta>
            <ta e="T731" id="Seg_6971" s="T730">pa-jəʔ</ta>
            <ta e="T732" id="Seg_6972" s="T731">sĭri</ta>
            <ta e="T733" id="Seg_6973" s="T732">pa-jəʔ</ta>
            <ta e="T734" id="Seg_6974" s="T733">bar</ta>
            <ta e="T735" id="Seg_6975" s="T734">bătek</ta>
            <ta e="T736" id="Seg_6976" s="T735">dĭ-zAŋ</ta>
            <ta e="T737" id="Seg_6977" s="T736">ugaːndə</ta>
            <ta e="T738" id="Seg_6978" s="T737">jakšə</ta>
            <ta e="T739" id="Seg_6979" s="T738">tăŋ</ta>
            <ta e="T740" id="Seg_6980" s="T739">nend-laʔbə-jəʔ</ta>
            <ta e="T741" id="Seg_6981" s="T740">i</ta>
            <ta e="T742" id="Seg_6982" s="T741">ejü</ta>
            <ta e="T743" id="Seg_6983" s="T742">dĭ-zAŋ</ta>
            <ta e="T744" id="Seg_6984" s="T743">dĭmbi</ta>
            <ta e="T745" id="Seg_6985" s="T744">nuldə-liA-l</ta>
            <ta e="T746" id="Seg_6986" s="T745">mĭndər-zittə</ta>
            <ta e="T748" id="Seg_6987" s="T747">kös</ta>
            <ta e="T749" id="Seg_6988" s="T748">ej</ta>
            <ta e="T750" id="Seg_6989" s="T749">ej</ta>
            <ta e="T751" id="Seg_6990" s="T750">păda-liA</ta>
            <ta e="T752" id="Seg_6991" s="T751">dĭbər</ta>
            <ta e="T754" id="Seg_6992" s="T753">ugaːndə</ta>
            <ta e="T755" id="Seg_6993" s="T754">kös</ta>
            <ta e="T756" id="Seg_6994" s="T755">iʔgö</ta>
            <ta e="T757" id="Seg_6995" s="T756">barəʔ-liA</ta>
            <ta e="T758" id="Seg_6996" s="T757">bobti</ta>
            <ta e="T759" id="Seg_6997" s="T758">măna</ta>
            <ta e="T762" id="Seg_6998" s="T761">bobti</ta>
            <ta e="T763" id="Seg_6999" s="T762">tože</ta>
            <ta e="T765" id="Seg_7000" s="T764">i</ta>
            <ta e="T766" id="Seg_7001" s="T765">tănar-bi</ta>
            <ta e="T767" id="Seg_7002" s="T766">dĭ</ta>
            <ta e="T768" id="Seg_7003" s="T767">a</ta>
            <ta e="T769" id="Seg_7004" s="T768">măn</ta>
            <ta e="T770" id="Seg_7005" s="T769">bos-m</ta>
            <ta e="T771" id="Seg_7006" s="T770">hen-bi-m</ta>
            <ta e="T772" id="Seg_7007" s="T771">traktăr-ziʔ</ta>
            <ta e="T773" id="Seg_7008" s="T772">det-bi-jəʔ</ta>
            <ta e="T774" id="Seg_7009" s="T773">kajaʔ</ta>
            <ta e="T775" id="Seg_7010" s="T774">i-bi-m</ta>
            <ta e="T776" id="Seg_7011" s="T775">traktăr-Tə</ta>
            <ta e="T777" id="Seg_7012" s="T776">tăŋ</ta>
            <ta e="T779" id="Seg_7013" s="T778">a</ta>
            <ta e="T780" id="Seg_7014" s="T779">măn</ta>
            <ta e="T781" id="Seg_7015" s="T780">bar</ta>
            <ta e="T782" id="Seg_7016" s="T781">pa</ta>
            <ta e="T783" id="Seg_7017" s="T782">tažor-bi-m</ta>
            <ta e="T784" id="Seg_7018" s="T783">bü-j-lAʔ</ta>
            <ta e="T785" id="Seg_7019" s="T784">mĭn-bi-m</ta>
            <ta e="T786" id="Seg_7020" s="T785">tüžöj-Tə</ta>
            <ta e="T787" id="Seg_7021" s="T786">nu-ʔ</ta>
            <ta e="T788" id="Seg_7022" s="T787">mĭ-bi-m</ta>
            <ta e="T789" id="Seg_7023" s="T788">surdo-bi-m</ta>
            <ta e="T790" id="Seg_7024" s="T789">dĭgəttə</ta>
            <ta e="T791" id="Seg_7025" s="T790">amnə-bi-m</ta>
            <ta e="T792" id="Seg_7026" s="T791">amor-zittə</ta>
            <ta e="T793" id="Seg_7027" s="T792">i</ta>
            <ta e="T794" id="Seg_7028" s="T793">tenö-laʔbə-m</ta>
            <ta e="T795" id="Seg_7029" s="T794">dĭ</ta>
            <ta e="T796" id="Seg_7030" s="T795">Jelʼa</ta>
            <ta e="T797" id="Seg_7031" s="T796">măna</ta>
            <ta e="T798" id="Seg_7032" s="T797">bar</ta>
            <ta e="T799" id="Seg_7033" s="T798">šʼaːm-bi</ta>
            <ta e="T800" id="Seg_7034" s="T799">aktʼa-t</ta>
            <ta e="T801" id="Seg_7035" s="T800">nagur</ta>
            <ta e="T802" id="Seg_7036" s="T801">šălkovej</ta>
            <ta e="T804" id="Seg_7037" s="T803">i-bi</ta>
            <ta e="T805" id="Seg_7038" s="T804">a</ta>
            <ta e="T806" id="Seg_7039" s="T805">măna</ta>
            <ta e="T807" id="Seg_7040" s="T806">i-bi</ta>
            <ta e="T808" id="Seg_7041" s="T807">onʼiʔ</ta>
            <ta e="T809" id="Seg_7042" s="T808">šălkovej</ta>
            <ta e="T810" id="Seg_7043" s="T809">šide</ta>
            <ta e="T811" id="Seg_7044" s="T810">biəʔ</ta>
            <ta e="T812" id="Seg_7045" s="T811">sumna</ta>
            <ta e="T813" id="Seg_7046" s="T812">dĭ-Tə</ta>
            <ta e="T815" id="Seg_7047" s="T814">i-zittə</ta>
            <ta e="T816" id="Seg_7048" s="T815">măna</ta>
            <ta e="T817" id="Seg_7049" s="T816">onʼiʔ</ta>
            <ta e="T818" id="Seg_7050" s="T817">šălkovej</ta>
            <ta e="T819" id="Seg_7051" s="T818">i</ta>
            <ta e="T820" id="Seg_7052" s="T819">biəʔ</ta>
            <ta e="T821" id="Seg_7053" s="T820">sumna</ta>
            <ta e="T822" id="Seg_7054" s="T821">kăpʼejka-jəʔ</ta>
            <ta e="T823" id="Seg_7055" s="T822">măn</ta>
            <ta e="T824" id="Seg_7056" s="T823">teinen</ta>
            <ta e="T825" id="Seg_7057" s="T824">bü-Tə</ta>
            <ta e="T826" id="Seg_7058" s="T825">kan-bi-m</ta>
            <ta e="T827" id="Seg_7059" s="T826">da</ta>
            <ta e="T828" id="Seg_7060" s="T827">ugaːndə</ta>
            <ta e="T829" id="Seg_7061" s="T828">tăŋ</ta>
            <ta e="T830" id="Seg_7062" s="T829">kănzə-laːm-bi</ta>
            <ta e="T831" id="Seg_7063" s="T830">măn</ta>
            <ta e="T832" id="Seg_7064" s="T831">par-bi-m</ta>
            <ta e="T833" id="Seg_7065" s="T832">baltu</ta>
            <ta e="T834" id="Seg_7066" s="T833">i-bi-m</ta>
            <ta e="T835" id="Seg_7067" s="T834">baltu-ziʔ</ta>
            <ta e="T836" id="Seg_7068" s="T835">hʼaʔ-bi-m</ta>
            <ta e="T837" id="Seg_7069" s="T836">hʼaʔ-bi-m</ta>
            <ta e="T838" id="Seg_7070" s="T837">bar</ta>
            <ta e="T839" id="Seg_7071" s="T838">sĭri</ta>
            <ta e="T842" id="Seg_7072" s="T841">kănzə-laːm-bi</ta>
            <ta e="T843" id="Seg_7073" s="T842">ugaːndə</ta>
            <ta e="T844" id="Seg_7074" s="T843">šišəge</ta>
            <ta e="T845" id="Seg_7075" s="T844">ertə</ta>
            <ta e="T846" id="Seg_7076" s="T845">i-bi</ta>
            <ta e="T847" id="Seg_7077" s="T846">maʔ-gənʼi</ta>
            <ta e="T848" id="Seg_7078" s="T847">šo-bi-m</ta>
            <ta e="T849" id="Seg_7079" s="T848">šiʔnʼileʔ</ta>
            <ta e="T850" id="Seg_7080" s="T849">edəʔ-bi-m</ta>
            <ta e="T851" id="Seg_7081" s="T850">i</ta>
            <ta e="T852" id="Seg_7082" s="T851">bü</ta>
            <ta e="T853" id="Seg_7083" s="T852">det-bi-m</ta>
            <ta e="T854" id="Seg_7084" s="T853">pʼeːš-Tə</ta>
            <ta e="T858" id="Seg_7085" s="T857">sʼa-bi-m</ta>
            <ta e="T859" id="Seg_7086" s="T858">amnə-bi-m</ta>
            <ta e="T860" id="Seg_7087" s="T859">i</ta>
            <ta e="T861" id="Seg_7088" s="T860">emneʔpəm</ta>
            <ta e="T862" id="Seg_7089" s="T861">dĭgəttə</ta>
            <ta e="T863" id="Seg_7090" s="T862">šo-bi-jəʔ</ta>
            <ta e="T864" id="Seg_7091" s="T863">i</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_7092" s="T0">we.NOM</ta>
            <ta e="T2" id="Seg_7093" s="T1">PTCL</ta>
            <ta e="T3" id="Seg_7094" s="T2">Sverdlovsk-LOC</ta>
            <ta e="T4" id="Seg_7095" s="T3">live-%1PL.PST</ta>
            <ta e="T7" id="Seg_7096" s="T6">iron-ADJZ</ta>
            <ta e="T8" id="Seg_7097" s="T7">iron-ADJZ</ta>
            <ta e="T9" id="Seg_7098" s="T8">bird-LAT</ta>
            <ta e="T10" id="Seg_7099" s="T9">sit-PST-1PL</ta>
            <ta e="T11" id="Seg_7100" s="T10">then</ta>
            <ta e="T12" id="Seg_7101" s="T11">fly-PST-1PL</ta>
            <ta e="T13" id="Seg_7102" s="T12">road-LOC</ta>
            <ta e="T14" id="Seg_7103" s="T13">NEG</ta>
            <ta e="T15" id="Seg_7104" s="T14">can-PRS.[3SG]</ta>
            <ta e="T17" id="Seg_7105" s="T16">fly-INF.LAT</ta>
            <ta e="T18" id="Seg_7106" s="T17">smoke-ADJZ.[NOM.SG]</ta>
            <ta e="T19" id="Seg_7107" s="T18">PTCL</ta>
            <ta e="T22" id="Seg_7108" s="T21">stand-DUR.[3SG]</ta>
            <ta e="T23" id="Seg_7109" s="T22">then</ta>
            <ta e="T24" id="Seg_7110" s="T23">come-PST-1PL</ta>
            <ta e="T25" id="Seg_7111" s="T24">Krasnoyarsk-LAT</ta>
            <ta e="T26" id="Seg_7112" s="T25">there</ta>
            <ta e="T27" id="Seg_7113" s="T26">sit-PST-1PL</ta>
            <ta e="T28" id="Seg_7114" s="T27">there</ta>
            <ta e="T29" id="Seg_7115" s="T28">live-PST-1SG</ta>
            <ta e="T30" id="Seg_7116" s="T29">seven.[NOM.SG]</ta>
            <ta e="T31" id="Seg_7117" s="T30">day.[NOM.SG]</ta>
            <ta e="T32" id="Seg_7118" s="T31">live-PST-1SG</ta>
            <ta e="T33" id="Seg_7119" s="T32">then</ta>
            <ta e="T34" id="Seg_7120" s="T33">sit-PST-1SG</ta>
            <ta e="T35" id="Seg_7121" s="T34">iron-ADJZ</ta>
            <ta e="T36" id="Seg_7122" s="T35">road-LAT</ta>
            <ta e="T37" id="Seg_7123" s="T36">come-PST-1SG</ta>
            <ta e="T38" id="Seg_7124" s="T37">Uyar-LAT</ta>
            <ta e="T39" id="Seg_7125" s="T38">then</ta>
            <ta e="T40" id="Seg_7126" s="T39">Uyar-ABL</ta>
            <ta e="T41" id="Seg_7127" s="T40">come-PST-1SG</ta>
            <ta e="T43" id="Seg_7128" s="T42">sit-PST-1SG</ta>
            <ta e="T44" id="Seg_7129" s="T43">bus-LAT</ta>
            <ta e="T45" id="Seg_7130" s="T44">come-PST-1SG</ta>
            <ta e="T46" id="Seg_7131" s="T45">Sayansk-GEN</ta>
            <ta e="T47" id="Seg_7132" s="T46">here</ta>
            <ta e="T48" id="Seg_7133" s="T47">live-PST-1SG</ta>
            <ta e="T51" id="Seg_7134" s="T50">two.[NOM.SG]</ta>
            <ta e="T52" id="Seg_7135" s="T51">day.[NOM.SG]</ta>
            <ta e="T53" id="Seg_7136" s="T52">then</ta>
            <ta e="T54" id="Seg_7137" s="T53">Abalakovo-LAT</ta>
            <ta e="T55" id="Seg_7138" s="T54">come-PST-1SG</ta>
            <ta e="T56" id="Seg_7139" s="T55">and</ta>
            <ta e="T57" id="Seg_7140" s="T56">you.DAT</ta>
            <ta e="T58" id="Seg_7141" s="T57">come-PST-1SG</ta>
            <ta e="T59" id="Seg_7142" s="T58">you.DAT</ta>
            <ta e="T60" id="Seg_7143" s="T59">come-PST-1SG</ta>
            <ta e="T61" id="Seg_7144" s="T60">speak-PST-1PL</ta>
            <ta e="T62" id="Seg_7145" s="T61">then</ta>
            <ta e="T63" id="Seg_7146" s="T62">you.NOM</ta>
            <ta e="T64" id="Seg_7147" s="T63">I.NOM</ta>
            <ta e="T65" id="Seg_7148" s="T64">I.LAT</ta>
            <ta e="T67" id="Seg_7149" s="T66">say-FUT-2SG</ta>
            <ta e="T68" id="Seg_7150" s="T67">sit-IMP.2SG</ta>
            <ta e="T69" id="Seg_7151" s="T68">eat-IMP.2PL</ta>
            <ta e="T71" id="Seg_7152" s="T70">be.hungry-PRS-2SG</ta>
            <ta e="T72" id="Seg_7153" s="T71">probably=PTCL</ta>
            <ta e="T73" id="Seg_7154" s="T72">then</ta>
            <ta e="T74" id="Seg_7155" s="T73">I.NOM</ta>
            <ta e="T75" id="Seg_7156" s="T74">sit-PST-1SG</ta>
            <ta e="T76" id="Seg_7157" s="T75">eat-PST-1SG</ta>
            <ta e="T77" id="Seg_7158" s="T76">go-PST-1SG</ta>
            <ta e="T78" id="Seg_7159" s="T77">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T79" id="Seg_7160" s="T78">lodgings-3SG-INS</ta>
            <ta e="T80" id="Seg_7161" s="T79">one.[NOM.SG]</ta>
            <ta e="T81" id="Seg_7162" s="T80">one.[NOM.SG]</ta>
            <ta e="T82" id="Seg_7163" s="T81">time.[NOM.SG]</ta>
            <ta e="T83" id="Seg_7164" s="T82">we.NOM</ta>
            <ta e="T84" id="Seg_7165" s="T83">go-PST-1PL</ta>
            <ta e="T85" id="Seg_7166" s="T84">three.[NOM.SG]</ta>
            <ta e="T86" id="Seg_7167" s="T85">woman.[NOM.SG]</ta>
            <ta e="T87" id="Seg_7168" s="T86">Zaozerka-LAT</ta>
            <ta e="T88" id="Seg_7169" s="T87">then</ta>
            <ta e="T89" id="Seg_7170" s="T88">from.there</ta>
            <ta e="T90" id="Seg_7171" s="T89">iron-ADJZ</ta>
            <ta e="T91" id="Seg_7172" s="T90">road-LAT</ta>
            <ta e="T93" id="Seg_7173" s="T92">sit-PST-1PL</ta>
            <ta e="T94" id="Seg_7174" s="T93">go-PST-1PL</ta>
            <ta e="T95" id="Seg_7175" s="T94">Uyar-LAT</ta>
            <ta e="T96" id="Seg_7176" s="T95">there</ta>
            <ta e="T97" id="Seg_7177" s="T96">God-LAT</ta>
            <ta e="T98" id="Seg_7178" s="T97">pray-INF.LAT</ta>
            <ta e="T99" id="Seg_7179" s="T98">there</ta>
            <ta e="T100" id="Seg_7180" s="T99">God-LAT</ta>
            <ta e="T101" id="Seg_7181" s="T100">pray-CVB-PST-1PL</ta>
            <ta e="T102" id="Seg_7182" s="T101">then</ta>
            <ta e="T103" id="Seg_7183" s="T102">return-MOM-PST-1PL</ta>
            <ta e="T104" id="Seg_7184" s="T103">again</ta>
            <ta e="T106" id="Seg_7185" s="T105">here</ta>
            <ta e="T107" id="Seg_7186" s="T106">Zaozerka-LAT</ta>
            <ta e="T108" id="Seg_7187" s="T107">there</ta>
            <ta e="T109" id="Seg_7188" s="T108">here</ta>
            <ta e="T110" id="Seg_7189" s="T109">spend.night-%1PL.PST</ta>
            <ta e="T111" id="Seg_7190" s="T110">many</ta>
            <ta e="T112" id="Seg_7191" s="T111">speak-CVB-PST-1PL</ta>
            <ta e="T113" id="Seg_7192" s="T112">then</ta>
            <ta e="T114" id="Seg_7193" s="T113">%%</ta>
            <ta e="T115" id="Seg_7194" s="T114">go-PST.[3SG]</ta>
            <ta e="T116" id="Seg_7195" s="T115">bus.[NOM.SG]</ta>
            <ta e="T117" id="Seg_7196" s="T116">look-FRQ-INF.LAT</ta>
            <ta e="T118" id="Seg_7197" s="T117">come-PST.[3SG]</ta>
            <ta e="T119" id="Seg_7198" s="T118">and</ta>
            <ta e="T120" id="Seg_7199" s="T119">%%</ta>
            <ta e="T868" id="Seg_7200" s="T120">go-CVB</ta>
            <ta e="T121" id="Seg_7201" s="T868">disappear-PST.[3SG]</ta>
            <ta e="T122" id="Seg_7202" s="T121">and</ta>
            <ta e="T123" id="Seg_7203" s="T122">we.NOM</ta>
            <ta e="T124" id="Seg_7204" s="T123">collect-PST-1PL</ta>
            <ta e="T125" id="Seg_7205" s="T124">and</ta>
            <ta e="T126" id="Seg_7206" s="T125">go-PST-1PL</ta>
            <ta e="T127" id="Seg_7207" s="T126">outwards</ta>
            <ta e="T128" id="Seg_7208" s="T127">here</ta>
            <ta e="T129" id="Seg_7209" s="T128">bus.[NOM.SG]</ta>
            <ta e="T130" id="Seg_7210" s="T129">come-PRS.[3SG]</ta>
            <ta e="T131" id="Seg_7211" s="T130">sit.down-TR-PST.[3SG]</ta>
            <ta e="T132" id="Seg_7212" s="T131">we.NOM</ta>
            <ta e="T133" id="Seg_7213" s="T132">and</ta>
            <ta e="T134" id="Seg_7214" s="T133">Pereyaslovka-LAT</ta>
            <ta e="T135" id="Seg_7215" s="T134">come-PST-1PL</ta>
            <ta e="T136" id="Seg_7216" s="T135">there</ta>
            <ta e="T139" id="Seg_7217" s="T137">we.ACC</ta>
            <ta e="T140" id="Seg_7218" s="T139">there</ta>
            <ta e="T141" id="Seg_7219" s="T140">we.NOM</ta>
            <ta e="T142" id="Seg_7220" s="T141">there</ta>
            <ta e="T143" id="Seg_7221" s="T142">descend-PST-1PL</ta>
            <ta e="T144" id="Seg_7222" s="T143">and</ta>
            <ta e="T145" id="Seg_7223" s="T144">stand-PST-1PL</ta>
            <ta e="T146" id="Seg_7224" s="T145">I.NOM</ta>
            <ta e="T147" id="Seg_7225" s="T146">say-IPFVZ-1SG</ta>
            <ta e="T148" id="Seg_7226" s="T147">what.[NOM.SG]</ta>
            <ta e="T149" id="Seg_7227" s="T148">stand-DUR-INF.LAT</ta>
            <ta e="T150" id="Seg_7228" s="T149">go-OPT.DU/PL-1PL</ta>
            <ta e="T151" id="Seg_7229" s="T150">on.foot</ta>
            <ta e="T152" id="Seg_7230" s="T151">then</ta>
            <ta e="T153" id="Seg_7231" s="T152">go-OPT.DU/PL-1PL</ta>
            <ta e="T154" id="Seg_7232" s="T153">walk-DUR-1PL</ta>
            <ta e="T155" id="Seg_7233" s="T154">machine.[NOM.SG]</ta>
            <ta e="T156" id="Seg_7234" s="T155">PTCL</ta>
            <ta e="T157" id="Seg_7235" s="T156">come-PRS.[3SG]</ta>
            <ta e="T158" id="Seg_7236" s="T157">sit-IMP.2PL</ta>
            <ta e="T159" id="Seg_7237" s="T158">we.NOM</ta>
            <ta e="T162" id="Seg_7238" s="T161">sit-IMP.2PL</ta>
            <ta e="T163" id="Seg_7239" s="T162">here</ta>
            <ta e="T164" id="Seg_7240" s="T163">then</ta>
            <ta e="T165" id="Seg_7241" s="T164">we.NOM</ta>
            <ta e="T166" id="Seg_7242" s="T165">sit-PST-1PL</ta>
            <ta e="T167" id="Seg_7243" s="T166">and</ta>
            <ta e="T168" id="Seg_7244" s="T167">come-PST-1PL</ta>
            <ta e="T169" id="Seg_7245" s="T168">Ust_Kandyga-LAT</ta>
            <ta e="T170" id="Seg_7246" s="T169">then</ta>
            <ta e="T171" id="Seg_7247" s="T170">there</ta>
            <ta e="T172" id="Seg_7248" s="T171">more</ta>
            <ta e="T173" id="Seg_7249" s="T172">seven.[NOM.SG]</ta>
            <ta e="T175" id="Seg_7250" s="T174">remain-MOM-PST.[3SG]</ta>
            <ta e="T176" id="Seg_7251" s="T175">Uner-NOM/GEN/ACC.3SG</ta>
            <ta e="T177" id="Seg_7252" s="T176">go-INF.LAT</ta>
            <ta e="T178" id="Seg_7253" s="T177">two.[NOM.SG]</ta>
            <ta e="T179" id="Seg_7254" s="T178">woman-PL</ta>
            <ta e="T180" id="Seg_7255" s="T179">go-PST-3PL</ta>
            <ta e="T181" id="Seg_7256" s="T180">house-LAT</ta>
            <ta e="T182" id="Seg_7257" s="T181">and</ta>
            <ta e="T183" id="Seg_7258" s="T182">I.NOM</ta>
            <ta e="T184" id="Seg_7259" s="T183">go-PST-1SG</ta>
            <ta e="T185" id="Seg_7260" s="T184">on.foot</ta>
            <ta e="T186" id="Seg_7261" s="T185">Uner-NOM/GEN/ACC.3SG</ta>
            <ta e="T187" id="Seg_7262" s="T186">come-PST-1SG</ta>
            <ta e="T188" id="Seg_7263" s="T187">bring-IMP.2PL</ta>
            <ta e="T189" id="Seg_7264" s="T188">I.LAT</ta>
            <ta e="T190" id="Seg_7265" s="T189">bread.[NOM.SG]</ta>
            <ta e="T191" id="Seg_7266" s="T190">sell-IMP.2PL</ta>
            <ta e="T192" id="Seg_7267" s="T191">bread.[NOM.SG]</ta>
            <ta e="T193" id="Seg_7268" s="T192">this-PL</ta>
            <ta e="T194" id="Seg_7269" s="T193">say-IPFVZ.[3SG]</ta>
            <ta e="T196" id="Seg_7270" s="T195">go-EP-IMP.2SG</ta>
            <ta e="T197" id="Seg_7271" s="T196">shop-LAT</ta>
            <ta e="T199" id="Seg_7272" s="T198">there</ta>
            <ta e="T200" id="Seg_7273" s="T199">take-IMP.2SG</ta>
            <ta e="T201" id="Seg_7274" s="T200">I.LAT</ta>
            <ta e="T202" id="Seg_7275" s="T201">many</ta>
            <ta e="T203" id="Seg_7276" s="T202">NEG</ta>
            <ta e="T204" id="Seg_7277" s="T203">one.needs</ta>
            <ta e="T205" id="Seg_7278" s="T204">two.[NOM.SG]</ta>
            <ta e="T206" id="Seg_7279" s="T205">gramm.[NOM.SG]</ta>
            <ta e="T207" id="Seg_7280" s="T206">give-PST-2PL</ta>
            <ta e="T208" id="Seg_7281" s="T207">this.[NOM.SG]</ta>
            <ta e="T209" id="Seg_7282" s="T208">I.LAT</ta>
            <ta e="T210" id="Seg_7283" s="T209">two.[NOM.SG]</ta>
            <ta e="T211" id="Seg_7284" s="T210">gramm.[NOM.SG]</ta>
            <ta e="T212" id="Seg_7285" s="T211">hang.up-PST.[3SG]</ta>
            <ta e="T213" id="Seg_7286" s="T212">I.NOM</ta>
            <ta e="T214" id="Seg_7287" s="T213">take-PST-1SG</ta>
            <ta e="T215" id="Seg_7288" s="T214">money.[NOM.SG]</ta>
            <ta e="T216" id="Seg_7289" s="T215">give-PST-1SG</ta>
            <ta e="T217" id="Seg_7290" s="T216">eat-PST-1SG</ta>
            <ta e="T218" id="Seg_7291" s="T217">and</ta>
            <ta e="T219" id="Seg_7292" s="T218">go-PST-1SG</ta>
            <ta e="T220" id="Seg_7293" s="T219">water.[NOM.SG]</ta>
            <ta e="T221" id="Seg_7294" s="T220">drink-PST-1SG</ta>
            <ta e="T222" id="Seg_7295" s="T221">then</ta>
            <ta e="T223" id="Seg_7296" s="T222">machine.[NOM.SG]</ta>
            <ta e="T224" id="Seg_7297" s="T223">catch.up-PST.[3SG]</ta>
            <ta e="T225" id="Seg_7298" s="T224">I.NOM</ta>
            <ta e="T227" id="Seg_7299" s="T226">sit-PST-1SG</ta>
            <ta e="T228" id="Seg_7300" s="T227">then</ta>
            <ta e="T229" id="Seg_7301" s="T228">descend-PST-1SG</ta>
            <ta e="T230" id="Seg_7302" s="T229">two.[NOM.SG]</ta>
            <ta e="T231" id="Seg_7303" s="T230">man.[NOM.SG]</ta>
            <ta e="T232" id="Seg_7304" s="T231">stand-PRS-3PL</ta>
            <ta e="T233" id="Seg_7305" s="T232">mountain-LOC</ta>
            <ta e="T234" id="Seg_7306" s="T233">I.NOM</ta>
            <ta e="T235" id="Seg_7307" s="T234">there</ta>
            <ta e="T237" id="Seg_7308" s="T236">climb-PST-1SG</ta>
            <ta e="T238" id="Seg_7309" s="T237">go-OPT.DU/PL-1DU</ta>
            <ta e="T239" id="Seg_7310" s="T238">%together</ta>
            <ta e="T240" id="Seg_7311" s="T239">no</ta>
            <ta e="T241" id="Seg_7312" s="T240">we.NOM</ta>
            <ta e="T242" id="Seg_7313" s="T241">wait-DUR.[3SG]</ta>
            <ta e="T243" id="Seg_7314" s="T242">machine.[NOM.SG]</ta>
            <ta e="T244" id="Seg_7315" s="T243">machine-INS</ta>
            <ta e="T245" id="Seg_7316" s="T244">go-OPT.DU/PL-1DU</ta>
            <ta e="T246" id="Seg_7317" s="T245">then</ta>
            <ta e="T247" id="Seg_7318" s="T246">machine.[NOM.SG]</ta>
            <ta e="T248" id="Seg_7319" s="T247">come-PST.[3SG]</ta>
            <ta e="T251" id="Seg_7320" s="T250">sit-PST-1SG</ta>
            <ta e="T252" id="Seg_7321" s="T251">and</ta>
            <ta e="T253" id="Seg_7322" s="T252">Sayanskij-LAT</ta>
            <ta e="T254" id="Seg_7323" s="T253">come-PST-1SG</ta>
            <ta e="T255" id="Seg_7324" s="T254">then</ta>
            <ta e="T256" id="Seg_7325" s="T255">%%</ta>
            <ta e="T258" id="Seg_7326" s="T257">take-PST-1SG</ta>
            <ta e="T259" id="Seg_7327" s="T258">bread.[NOM.SG]</ta>
            <ta e="T260" id="Seg_7328" s="T259">sugar.[NOM.SG]</ta>
            <ta e="T261" id="Seg_7329" s="T260">Vanka.[NOM.SG]</ta>
            <ta e="T262" id="Seg_7330" s="T261">hospital-LOC</ta>
            <ta e="T263" id="Seg_7331" s="T262">lie-DUR-PST.[3SG]</ta>
            <ta e="T264" id="Seg_7332" s="T263">bring-PST-1SG</ta>
            <ta e="T265" id="Seg_7333" s="T264">this-LAT</ta>
            <ta e="T266" id="Seg_7334" s="T265">I.LAT</ta>
            <ta e="T267" id="Seg_7335" s="T266">there</ta>
            <ta e="T268" id="Seg_7336" s="T267">tell-DUR-3PL</ta>
            <ta e="T269" id="Seg_7337" s="T268">here</ta>
            <ta e="T270" id="Seg_7338" s="T269">be-IMP.2PL</ta>
            <ta e="T271" id="Seg_7339" s="T270">we.NOM</ta>
            <ta e="T272" id="Seg_7340" s="T271">machine.[NOM.SG]</ta>
            <ta e="T273" id="Seg_7341" s="T272">go-EP-IMP.2SG</ta>
            <ta e="T274" id="Seg_7342" s="T273">I.NOM</ta>
            <ta e="T275" id="Seg_7343" s="T274">run-PST-1SG</ta>
            <ta e="T276" id="Seg_7344" s="T275">where=INDEF</ta>
            <ta e="T277" id="Seg_7345" s="T276">NEG</ta>
            <ta e="T278" id="Seg_7346" s="T277">find-PST-1SG</ta>
            <ta e="T279" id="Seg_7347" s="T278">then</ta>
            <ta e="T280" id="Seg_7348" s="T279">go-PST-1SG</ta>
            <ta e="T281" id="Seg_7349" s="T280">on.foot</ta>
            <ta e="T282" id="Seg_7350" s="T281">then</ta>
            <ta e="T283" id="Seg_7351" s="T282">again</ta>
            <ta e="T284" id="Seg_7352" s="T283">machine.[NOM.SG]</ta>
            <ta e="T285" id="Seg_7353" s="T284">come-PRS.[3SG]</ta>
            <ta e="T286" id="Seg_7354" s="T285">I.NOM</ta>
            <ta e="T287" id="Seg_7355" s="T286">sit-PST-1SG</ta>
            <ta e="T288" id="Seg_7356" s="T287">Malinovka-LAT</ta>
            <ta e="T289" id="Seg_7357" s="T288">come-PST-1SG</ta>
            <ta e="T290" id="Seg_7358" s="T289">then</ta>
            <ta e="T291" id="Seg_7359" s="T290">again</ta>
            <ta e="T292" id="Seg_7360" s="T291">foot-INS</ta>
            <ta e="T293" id="Seg_7361" s="T292">go-PST-1SG</ta>
            <ta e="T294" id="Seg_7362" s="T293">then</ta>
            <ta e="T295" id="Seg_7363" s="T294">again</ta>
            <ta e="T296" id="Seg_7364" s="T295">machine.[NOM.SG]</ta>
            <ta e="T297" id="Seg_7365" s="T296">catch.up-PST.[3SG]</ta>
            <ta e="T298" id="Seg_7366" s="T297">and</ta>
            <ta e="T299" id="Seg_7367" s="T298">house-LAT/LOC.3SG</ta>
            <ta e="T300" id="Seg_7368" s="T299">come-PST-1SG</ta>
            <ta e="T301" id="Seg_7369" s="T300">when</ta>
            <ta e="T302" id="Seg_7370" s="T301">Matveev.[NOM.SG]</ta>
            <ta e="T303" id="Seg_7371" s="T302">come-PST.[3SG]</ta>
            <ta e="T304" id="Seg_7372" s="T303">I.LAT</ta>
            <ta e="T306" id="Seg_7373" s="T305">speak-DUR.[3SG]</ta>
            <ta e="T307" id="Seg_7374" s="T306">Kamassian-TR-CVB</ta>
            <ta e="T308" id="Seg_7375" s="T307">and</ta>
            <ta e="T309" id="Seg_7376" s="T308">I.NOM</ta>
            <ta e="T310" id="Seg_7377" s="T309">Russian</ta>
            <ta e="T311" id="Seg_7378" s="T310">Russian</ta>
            <ta e="T312" id="Seg_7379" s="T311">speak-DUR-1SG</ta>
            <ta e="T313" id="Seg_7380" s="T312">and</ta>
            <ta e="T314" id="Seg_7381" s="T313">%%-DUR-1SG</ta>
            <ta e="T315" id="Seg_7382" s="T314">you.NOM</ta>
            <ta e="T316" id="Seg_7383" s="T315">now</ta>
            <ta e="T317" id="Seg_7384" s="T316">Kamassian.[NOM.SG]</ta>
            <ta e="T318" id="Seg_7385" s="T317">be-PRS-2SG</ta>
            <ta e="T319" id="Seg_7386" s="T318">and</ta>
            <ta e="T320" id="Seg_7387" s="T319">I.NOM</ta>
            <ta e="T321" id="Seg_7388" s="T320">Russian.[NOM.SG]</ta>
            <ta e="T322" id="Seg_7389" s="T321">be-PRS-1SG</ta>
            <ta e="T323" id="Seg_7390" s="T322">then</ta>
            <ta e="T324" id="Seg_7391" s="T323">this-PL</ta>
            <ta e="T325" id="Seg_7392" s="T324">forest-LAT</ta>
            <ta e="T326" id="Seg_7393" s="T325">go-PST-3PL</ta>
            <ta e="T327" id="Seg_7394" s="T326">Sayan.mountains-LAT</ta>
            <ta e="T329" id="Seg_7395" s="T328">fish.[NOM.SG]</ta>
            <ta e="T330" id="Seg_7396" s="T329">capture-PST-3PL</ta>
            <ta e="T331" id="Seg_7397" s="T330">there</ta>
            <ta e="T334" id="Seg_7398" s="T333">put-PST-3PL</ta>
            <ta e="T335" id="Seg_7399" s="T334">then</ta>
            <ta e="T336" id="Seg_7400" s="T335">one.[NOM.SG]</ta>
            <ta e="T337" id="Seg_7401" s="T336">girl.[NOM.SG]</ta>
            <ta e="T338" id="Seg_7402" s="T337">big.[NOM.SG]</ta>
            <ta e="T339" id="Seg_7403" s="T338">PTCL</ta>
            <ta e="T340" id="Seg_7404" s="T339">hurt-MOM-PST.[3SG]</ta>
            <ta e="T341" id="Seg_7405" s="T340">PTCL</ta>
            <ta e="T342" id="Seg_7406" s="T341">then</ta>
            <ta e="T343" id="Seg_7407" s="T342">this-PL</ta>
            <ta e="T344" id="Seg_7408" s="T343">come-PST-3PL</ta>
            <ta e="T345" id="Seg_7409" s="T344">house-LAT</ta>
            <ta e="T346" id="Seg_7410" s="T345">Kan_Okler-LAT</ta>
            <ta e="T347" id="Seg_7411" s="T346">and</ta>
            <ta e="T348" id="Seg_7412" s="T347">woman.[NOM.SG]</ta>
            <ta e="T349" id="Seg_7413" s="T348">take-PST-3PL</ta>
            <ta e="T350" id="Seg_7414" s="T349">and</ta>
            <ta e="T351" id="Seg_7415" s="T350">this-ACC</ta>
            <ta e="T352" id="Seg_7416" s="T351">bring-PST-3PL</ta>
            <ta e="T353" id="Seg_7417" s="T352">hospital-LAT</ta>
            <ta e="T354" id="Seg_7418" s="T353">there</ta>
            <ta e="T355" id="Seg_7419" s="T354">foot-GEN</ta>
            <ta e="T356" id="Seg_7420" s="T355">PTCL</ta>
            <ta e="T357" id="Seg_7421" s="T356">water.[NOM.SG]</ta>
            <ta e="T358" id="Seg_7422" s="T357">drink-PST-3PL</ta>
            <ta e="T359" id="Seg_7423" s="T358">and</ta>
            <ta e="T360" id="Seg_7424" s="T359">%%-PST-3PL</ta>
            <ta e="T361" id="Seg_7425" s="T360">heal-PST-3PL</ta>
            <ta e="T362" id="Seg_7426" s="T361">otherwise</ta>
            <ta e="T363" id="Seg_7427" s="T362">this.[NOM.SG]</ta>
            <ta e="T364" id="Seg_7428" s="T363">go-INF.LAT</ta>
            <ta e="T365" id="Seg_7429" s="T364">NEG</ta>
            <ta e="T367" id="Seg_7430" s="T366">NEG</ta>
            <ta e="T368" id="Seg_7431" s="T367">can-PRS.[3SG]</ta>
            <ta e="T369" id="Seg_7432" s="T368">I.NOM</ta>
            <ta e="T372" id="Seg_7433" s="T371">look-FRQ-PST-1SG</ta>
            <ta e="T373" id="Seg_7434" s="T372">look-FRQ-PST-1SG</ta>
            <ta e="T375" id="Seg_7435" s="T374">there</ta>
            <ta e="T376" id="Seg_7436" s="T375">write-PST-1SG</ta>
            <ta e="T377" id="Seg_7437" s="T376">paper.[NOM.SG]</ta>
            <ta e="T378" id="Seg_7438" s="T377">there</ta>
            <ta e="T380" id="Seg_7439" s="T379">write-PST-1SG</ta>
            <ta e="T381" id="Seg_7440" s="T380">who.[NOM.SG]</ta>
            <ta e="T383" id="Seg_7441" s="T381">I.NOM=PTCL</ta>
            <ta e="T384" id="Seg_7442" s="T383">see-RES-PST.[3SG]</ta>
            <ta e="T385" id="Seg_7443" s="T384">who.[NOM.SG]</ta>
            <ta e="T387" id="Seg_7444" s="T385">I.NOM=PTCL</ta>
            <ta e="T388" id="Seg_7445" s="T387">NEG</ta>
            <ta e="T389" id="Seg_7446" s="T388">see-RES-PST.[3SG]</ta>
            <ta e="T390" id="Seg_7447" s="T389">where=INDEF</ta>
            <ta e="T391" id="Seg_7448" s="T390">NEG.EX.[3SG]</ta>
            <ta e="T392" id="Seg_7449" s="T391">really-PL</ta>
            <ta e="T393" id="Seg_7450" s="T392">and</ta>
            <ta e="T394" id="Seg_7451" s="T393">I.NOM</ta>
            <ta e="T395" id="Seg_7452" s="T394">think-PST-1SG</ta>
            <ta e="T396" id="Seg_7453" s="T395">be-PRS.[3SG]</ta>
            <ta e="T397" id="Seg_7454" s="T396">everywhere</ta>
            <ta e="T398" id="Seg_7455" s="T397">really.[NOM.SG]</ta>
            <ta e="T399" id="Seg_7456" s="T398">be-PRS.[3SG]</ta>
            <ta e="T400" id="Seg_7457" s="T399">and</ta>
            <ta e="T401" id="Seg_7458" s="T400">this-ACC</ta>
            <ta e="T402" id="Seg_7459" s="T401">where=INDEF</ta>
            <ta e="T403" id="Seg_7460" s="T402">NEG.EX.[3SG]</ta>
            <ta e="T404" id="Seg_7461" s="T403">always</ta>
            <ta e="T405" id="Seg_7462" s="T404">%%</ta>
            <ta e="T406" id="Seg_7463" s="T405">then</ta>
            <ta e="T407" id="Seg_7464" s="T406">prosecutor.[NOM.SG]</ta>
            <ta e="T408" id="Seg_7465" s="T407">write-PST.[3SG]</ta>
            <ta e="T409" id="Seg_7466" s="T408">where.to=INDEF</ta>
            <ta e="T869" id="Seg_7467" s="T409">go-CVB</ta>
            <ta e="T410" id="Seg_7468" s="T869">disappear-PST.[3SG]</ta>
            <ta e="T411" id="Seg_7469" s="T410">NEG</ta>
            <ta e="T412" id="Seg_7470" s="T411">know-1SG</ta>
            <ta e="T413" id="Seg_7471" s="T412">Elya.[NOM.SG]</ta>
            <ta e="T414" id="Seg_7472" s="T413">%%</ta>
            <ta e="T415" id="Seg_7473" s="T414">%%</ta>
            <ta e="T416" id="Seg_7474" s="T415">PTCL</ta>
            <ta e="T417" id="Seg_7475" s="T416">boat-LAT</ta>
            <ta e="T418" id="Seg_7476" s="T417">river-LAT</ta>
            <ta e="T419" id="Seg_7477" s="T418">go-PST-3PL</ta>
            <ta e="T420" id="Seg_7478" s="T419">then</ta>
            <ta e="T421" id="Seg_7479" s="T420">what.kind=INDEF</ta>
            <ta e="T422" id="Seg_7480" s="T421">daughter-COM</ta>
            <ta e="T423" id="Seg_7481" s="T422">want.PST.PL</ta>
            <ta e="T424" id="Seg_7482" s="T423">river-LOC</ta>
            <ta e="T426" id="Seg_7483" s="T425">and</ta>
            <ta e="T427" id="Seg_7484" s="T426">river.[NOM.SG]</ta>
            <ta e="T428" id="Seg_7485" s="T427">very</ta>
            <ta e="T429" id="Seg_7486" s="T428">big.[NOM.SG]</ta>
            <ta e="T430" id="Seg_7487" s="T429">this-PL</ta>
            <ta e="T431" id="Seg_7488" s="T430">PTCL</ta>
            <ta e="T432" id="Seg_7489" s="T431">want.PST.PL</ta>
            <ta e="T433" id="Seg_7490" s="T432">boat-LAT</ta>
            <ta e="T434" id="Seg_7491" s="T433">climb-INF.LAT</ta>
            <ta e="T435" id="Seg_7492" s="T434">and</ta>
            <ta e="T436" id="Seg_7493" s="T435">PTCL</ta>
            <ta e="T437" id="Seg_7494" s="T436">frighten-MOM-PST-3PL</ta>
            <ta e="T439" id="Seg_7495" s="T437">then</ta>
            <ta e="T440" id="Seg_7496" s="T439">then</ta>
            <ta e="T441" id="Seg_7497" s="T440">there</ta>
            <ta e="T442" id="Seg_7498" s="T441">frighten-MOM-PST-3PL</ta>
            <ta e="T443" id="Seg_7499" s="T442">collect-PST.[3SG]</ta>
            <ta e="T445" id="Seg_7500" s="T444">see-PRS-3SG.O</ta>
            <ta e="T446" id="Seg_7501" s="T445">that</ta>
            <ta e="T447" id="Seg_7502" s="T446">%%</ta>
            <ta e="T448" id="Seg_7503" s="T447">die-PST.[3SG]</ta>
            <ta e="T450" id="Seg_7504" s="T449">Mitrofan-EP-GEN</ta>
            <ta e="T451" id="Seg_7505" s="T450">house-NOM/GEN.3SG</ta>
            <ta e="T452" id="Seg_7506" s="T451">more</ta>
            <ta e="T453" id="Seg_7507" s="T452">stand-PRS.[3SG]</ta>
            <ta e="T454" id="Seg_7508" s="T453">and</ta>
            <ta e="T456" id="Seg_7509" s="T455">and</ta>
            <ta e="T457" id="Seg_7510" s="T456">%%</ta>
            <ta e="T459" id="Seg_7511" s="T458">house.[NOM.SG]</ta>
            <ta e="T460" id="Seg_7512" s="T459">more</ta>
            <ta e="T461" id="Seg_7513" s="T460">stand-PRS.[3SG]</ta>
            <ta e="T466" id="Seg_7514" s="T465">house.[NOM.SG]</ta>
            <ta e="T467" id="Seg_7515" s="T466">more</ta>
            <ta e="T468" id="Seg_7516" s="T467">stand-PRS.[3SG]</ta>
            <ta e="T471" id="Seg_7517" s="T470">house.[NOM.SG]</ta>
            <ta e="T472" id="Seg_7518" s="T471">more</ta>
            <ta e="T473" id="Seg_7519" s="T472">stand-PRS.[3SG]</ta>
            <ta e="T476" id="Seg_7520" s="T475">house.[NOM.SG]</ta>
            <ta e="T477" id="Seg_7521" s="T476">more</ta>
            <ta e="T478" id="Seg_7522" s="T477">stand-PRS.[3SG]</ta>
            <ta e="T479" id="Seg_7523" s="T478">and</ta>
            <ta e="T481" id="Seg_7524" s="T480">Vlas-LAT</ta>
            <ta e="T482" id="Seg_7525" s="T481">more</ta>
            <ta e="T483" id="Seg_7526" s="T482">house.[NOM.SG]</ta>
            <ta e="T484" id="Seg_7527" s="T483">stand-PRS.[3SG]</ta>
            <ta e="T485" id="Seg_7528" s="T484">and</ta>
            <ta e="T489" id="Seg_7529" s="T488">more</ta>
            <ta e="T490" id="Seg_7530" s="T489">house.[NOM.SG]</ta>
            <ta e="T491" id="Seg_7531" s="T490">stand-PRS.[3SG]</ta>
            <ta e="T492" id="Seg_7532" s="T491">when</ta>
            <ta e="T494" id="Seg_7533" s="T493">come-PST.[3SG]</ta>
            <ta e="T495" id="Seg_7534" s="T494">speak-INF.LAT</ta>
            <ta e="T496" id="Seg_7535" s="T495">here</ta>
            <ta e="T497" id="Seg_7536" s="T496">here</ta>
            <ta e="T499" id="Seg_7537" s="T498">five.[NOM.SG]</ta>
            <ta e="T500" id="Seg_7538" s="T499">ten.[NOM.SG]</ta>
            <ta e="T501" id="Seg_7539" s="T500">house.[NOM.SG]</ta>
            <ta e="T502" id="Seg_7540" s="T501">be-PST.[3SG]</ta>
            <ta e="T506" id="Seg_7541" s="T505">die-RES-PST.[3SG]</ta>
            <ta e="T507" id="Seg_7542" s="T506">this.[NOM.SG]</ta>
            <ta e="T508" id="Seg_7543" s="T507">go-PST.[3SG]</ta>
            <ta e="T509" id="Seg_7544" s="T508">Permyakovo-LAT</ta>
            <ta e="T510" id="Seg_7545" s="T509">there</ta>
            <ta e="T511" id="Seg_7546" s="T510">vodka.[NOM.SG]</ta>
            <ta e="T512" id="Seg_7547" s="T511">drink-PST.[3SG]</ta>
            <ta e="T513" id="Seg_7548" s="T512">there</ta>
            <ta e="T515" id="Seg_7549" s="T514">be-PST.[3SG]</ta>
            <ta e="T516" id="Seg_7550" s="T515">one.[NOM.SG]</ta>
            <ta e="T518" id="Seg_7551" s="T517">and</ta>
            <ta e="T519" id="Seg_7552" s="T518">woman-3SG-COM</ta>
            <ta e="T520" id="Seg_7553" s="T519">come-PST.[3SG]</ta>
            <ta e="T521" id="Seg_7554" s="T520">then</ta>
            <ta e="T522" id="Seg_7555" s="T521">go-PST.[3SG]</ta>
            <ta e="T523" id="Seg_7556" s="T522">cross-PL-LAT</ta>
            <ta e="T524" id="Seg_7557" s="T523">then</ta>
            <ta e="T525" id="Seg_7558" s="T524">pour-PRS</ta>
            <ta e="T526" id="Seg_7559" s="T525">and</ta>
            <ta e="T527" id="Seg_7560" s="T526">scold-DES-PRS.[3SG]</ta>
            <ta e="T528" id="Seg_7561" s="T527">drink-EP-IMP.2SG</ta>
            <ta e="T529" id="Seg_7562" s="T528">woman-NOM/GEN/ACC.1SG</ta>
            <ta e="T530" id="Seg_7563" s="T529">drink-EP-IMP.2SG</ta>
            <ta e="T531" id="Seg_7564" s="T530">woman-NOM/GEN/ACC.1SG</ta>
            <ta e="T532" id="Seg_7565" s="T531">and</ta>
            <ta e="T533" id="Seg_7566" s="T532">I.NOM</ta>
            <ta e="T534" id="Seg_7567" s="T533">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T535" id="Seg_7568" s="T534">stand-PRS.[3SG]</ta>
            <ta e="T536" id="Seg_7569" s="T535">and</ta>
            <ta e="T537" id="Seg_7570" s="T536">look-DUR.[3SG]</ta>
            <ta e="T538" id="Seg_7571" s="T537">and</ta>
            <ta e="T539" id="Seg_7572" s="T538">laugh-DUR.[3SG]</ta>
            <ta e="T540" id="Seg_7573" s="T539">I.NOM</ta>
            <ta e="T541" id="Seg_7574" s="T540">grass.[NOM.SG]</ta>
            <ta e="T542" id="Seg_7575" s="T541">cut-PST-1SG</ta>
            <ta e="T543" id="Seg_7576" s="T542">then</ta>
            <ta e="T544" id="Seg_7577" s="T543">collect-PST-1SG</ta>
            <ta e="T545" id="Seg_7578" s="T544">then</ta>
            <ta e="T546" id="Seg_7579" s="T545">haystack-NOM/GEN/ACC.3SG</ta>
            <ta e="T547" id="Seg_7580" s="T546">put-PST-1SG</ta>
            <ta e="T548" id="Seg_7581" s="T547">this.[NOM.SG]</ta>
            <ta e="T549" id="Seg_7582" s="T548">wet.[NOM.SG]</ta>
            <ta e="T550" id="Seg_7583" s="T549">be-PST.[3SG]</ta>
            <ta e="T551" id="Seg_7584" s="T550">then</ta>
            <ta e="T552" id="Seg_7585" s="T551">three.[NOM.SG]</ta>
            <ta e="T554" id="Seg_7586" s="T553">day.[NOM.SG]</ta>
            <ta e="T555" id="Seg_7587" s="T554">NEG</ta>
            <ta e="T556" id="Seg_7588" s="T555">go-PST-1SG</ta>
            <ta e="T557" id="Seg_7589" s="T556">very</ta>
            <ta e="T558" id="Seg_7590" s="T557">warm.[NOM.SG]</ta>
            <ta e="T559" id="Seg_7591" s="T558">become-RES-PST.[3SG]</ta>
            <ta e="T562" id="Seg_7592" s="T561">I.NOM</ta>
            <ta e="T563" id="Seg_7593" s="T562">two.[NOM.SG]</ta>
            <ta e="T564" id="Seg_7594" s="T563">woman.[NOM.SG]</ta>
            <ta e="T565" id="Seg_7595" s="T564">take-PST-1SG</ta>
            <ta e="T566" id="Seg_7596" s="T565">go-PST-1SG</ta>
            <ta e="T567" id="Seg_7597" s="T566">PTCL</ta>
            <ta e="T568" id="Seg_7598" s="T567">become.dry-PST.[3SG]</ta>
            <ta e="T569" id="Seg_7599" s="T568">and</ta>
            <ta e="T570" id="Seg_7600" s="T569">again</ta>
            <ta e="T571" id="Seg_7601" s="T570">put-PST-1PL</ta>
            <ta e="T572" id="Seg_7602" s="T571">beautiful.[NOM.SG]</ta>
            <ta e="T574" id="Seg_7603" s="T573">haystack.[NOM.SG]</ta>
            <ta e="T575" id="Seg_7604" s="T574">I.NOM</ta>
            <ta e="T576" id="Seg_7605" s="T575">live-PST-1SG</ta>
            <ta e="T577" id="Seg_7606" s="T576">two.[NOM.SG]</ta>
            <ta e="T578" id="Seg_7607" s="T577">ten.[NOM.SG]</ta>
            <ta e="T579" id="Seg_7608" s="T578">year.[NOM.SG]</ta>
            <ta e="T580" id="Seg_7609" s="T579">self-NOM/GEN/ACC.1SG</ta>
            <ta e="T581" id="Seg_7610" s="T580">grass.[NOM.SG]</ta>
            <ta e="T582" id="Seg_7611" s="T581">cut-PST-1SG</ta>
            <ta e="T583" id="Seg_7612" s="T582">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T584" id="Seg_7613" s="T583">collect-PST-1SG</ta>
            <ta e="T585" id="Seg_7614" s="T584">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T586" id="Seg_7615" s="T585">put-PST-1SG</ta>
            <ta e="T587" id="Seg_7616" s="T586">horse-INS</ta>
            <ta e="T588" id="Seg_7617" s="T587">house-LAT/LOC.1SG</ta>
            <ta e="T589" id="Seg_7618" s="T588">carry-PST-1SG</ta>
            <ta e="T590" id="Seg_7619" s="T589">PTCL</ta>
            <ta e="T591" id="Seg_7620" s="T590">cow-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T594" id="Seg_7621" s="T593">feed-DUR-PST-1SG</ta>
            <ta e="T595" id="Seg_7622" s="T594">one.[NOM.SG]</ta>
            <ta e="T596" id="Seg_7623" s="T595">horse.[NOM.SG]</ta>
            <ta e="T597" id="Seg_7624" s="T596">be-PST-1SG</ta>
            <ta e="T598" id="Seg_7625" s="T597">then</ta>
            <ta e="T599" id="Seg_7626" s="T598">niece-NOM/GEN.3SG</ta>
            <ta e="T600" id="Seg_7627" s="T599">boy-GEN</ta>
            <ta e="T601" id="Seg_7628" s="T600">take-PST-1SG</ta>
            <ta e="T602" id="Seg_7629" s="T601">and</ta>
            <ta e="T603" id="Seg_7630" s="T602">more</ta>
            <ta e="T605" id="Seg_7631" s="T604">go-PST-1PL</ta>
            <ta e="T606" id="Seg_7632" s="T605">grass.[NOM.SG]</ta>
            <ta e="T607" id="Seg_7633" s="T606">put-PST-1PL</ta>
            <ta e="T608" id="Seg_7634" s="T607">then</ta>
            <ta e="T609" id="Seg_7635" s="T608">come-PST-1PL</ta>
            <ta e="T610" id="Seg_7636" s="T609">come-PST-1PL</ta>
            <ta e="T611" id="Seg_7637" s="T610">we.NOM</ta>
            <ta e="T612" id="Seg_7638" s="T611">grass.[NOM.SG]</ta>
            <ta e="T867" id="Seg_7639" s="T613">descend-PST.[3SG] </ta>
            <ta e="T615" id="Seg_7640" s="T614">then</ta>
            <ta e="T616" id="Seg_7641" s="T615">lift-PST-1PL</ta>
            <ta e="T617" id="Seg_7642" s="T616">lift-PST-1PL</ta>
            <ta e="T618" id="Seg_7643" s="T617">NEG</ta>
            <ta e="T619" id="Seg_7644" s="T618">can-PRS-1PL</ta>
            <ta e="T620" id="Seg_7645" s="T619">this</ta>
            <ta e="T621" id="Seg_7646" s="T620">say-IPFVZ.[3SG]</ta>
            <ta e="T622" id="Seg_7647" s="T621">soon</ta>
            <ta e="T623" id="Seg_7648" s="T622">horse.[NOM.SG]</ta>
            <ta e="T626" id="Seg_7649" s="T625">harness-PST-1SG</ta>
            <ta e="T627" id="Seg_7650" s="T626">then</ta>
            <ta e="T628" id="Seg_7651" s="T627">lift-FUT-1SG</ta>
            <ta e="T629" id="Seg_7652" s="T628">and</ta>
            <ta e="T630" id="Seg_7653" s="T629">how</ta>
            <ta e="T631" id="Seg_7654" s="T630">you.NOM</ta>
            <ta e="T632" id="Seg_7655" s="T631">lift-FUT-2SG</ta>
            <ta e="T633" id="Seg_7656" s="T632">and</ta>
            <ta e="T634" id="Seg_7657" s="T633">so</ta>
            <ta e="T635" id="Seg_7658" s="T634">PTCL</ta>
            <ta e="T636" id="Seg_7659" s="T866">throw.away-PST.[3SG]</ta>
            <ta e="T637" id="Seg_7660" s="T636">%%-PL</ta>
            <ta e="T638" id="Seg_7661" s="T637">and</ta>
            <ta e="T639" id="Seg_7662" s="T638">then</ta>
            <ta e="T640" id="Seg_7663" s="T639">horse.[NOM.SG]</ta>
            <ta e="T643" id="Seg_7664" s="T642">then</ta>
            <ta e="T644" id="Seg_7665" s="T643">house.[NOM.SG]</ta>
            <ta e="T646" id="Seg_7666" s="T645">come-PST-1PL</ta>
            <ta e="T647" id="Seg_7667" s="T646">go-PST-1PL</ta>
            <ta e="T648" id="Seg_7668" s="T647">grass.[NOM.SG]</ta>
            <ta e="T649" id="Seg_7669" s="T648">then</ta>
            <ta e="T650" id="Seg_7670" s="T649">horse-ACC</ta>
            <ta e="T652" id="Seg_7671" s="T651">I.NOM</ta>
            <ta e="T653" id="Seg_7672" s="T652">very</ta>
            <ta e="T654" id="Seg_7673" s="T653">freeze-PST-1SG</ta>
            <ta e="T655" id="Seg_7674" s="T654">then</ta>
            <ta e="T656" id="Seg_7675" s="T655">cough</ta>
            <ta e="T657" id="Seg_7676" s="T656">come-PST.[3SG]</ta>
            <ta e="T658" id="Seg_7677" s="T657">then</ta>
            <ta e="T659" id="Seg_7678" s="T658">I.NOM</ta>
            <ta e="T660" id="Seg_7679" s="T659">sugar.[NOM.SG]</ta>
            <ta e="T661" id="Seg_7680" s="T660">PTCL</ta>
            <ta e="T662" id="Seg_7681" s="T661">burn-PST-1SG</ta>
            <ta e="T663" id="Seg_7682" s="T662">vodka-LAT</ta>
            <ta e="T664" id="Seg_7683" s="T663">then</ta>
            <ta e="T665" id="Seg_7684" s="T664">drink-PST-1SG</ta>
            <ta e="T666" id="Seg_7685" s="T665">and</ta>
            <ta e="T667" id="Seg_7686" s="T666">where.to=INDEF</ta>
            <ta e="T668" id="Seg_7687" s="T667">go-FUT-3SG</ta>
            <ta e="T669" id="Seg_7688" s="T668">cough-NOM/GEN/ACC.1SG</ta>
            <ta e="T673" id="Seg_7689" s="T672">what.kind</ta>
            <ta e="T674" id="Seg_7690" s="T673">grass.[NOM.SG]</ta>
            <ta e="T676" id="Seg_7691" s="T675">grass.[NOM.SG]</ta>
            <ta e="T677" id="Seg_7692" s="T676">be-PRS.[3SG]</ta>
            <ta e="T679" id="Seg_7693" s="T678">this.[NOM.SG]</ta>
            <ta e="T680" id="Seg_7694" s="T679">stem-LAT</ta>
            <ta e="T681" id="Seg_7695" s="T680">take-INF.LAT</ta>
            <ta e="T682" id="Seg_7696" s="T681">one.should</ta>
            <ta e="T683" id="Seg_7697" s="T682">pull-INF.LAT</ta>
            <ta e="T684" id="Seg_7698" s="T683">this-ACC</ta>
            <ta e="T685" id="Seg_7699" s="T684">%%</ta>
            <ta e="T686" id="Seg_7700" s="T685">smoke.[NOM.SG]</ta>
            <ta e="T687" id="Seg_7701" s="T686">sit-INF.LAT</ta>
            <ta e="T688" id="Seg_7702" s="T687">then</ta>
            <ta e="T689" id="Seg_7703" s="T688">NEG</ta>
            <ta e="T690" id="Seg_7704" s="T689">become-FUT-3SG</ta>
            <ta e="T691" id="Seg_7705" s="T690">cough.[NOM.SG]</ta>
            <ta e="T692" id="Seg_7706" s="T691">burn-MOM-PST.[3SG]</ta>
            <ta e="T693" id="Seg_7707" s="T692">and</ta>
            <ta e="T694" id="Seg_7708" s="T693">smoke.[NOM.SG]</ta>
            <ta e="T695" id="Seg_7709" s="T694">NEG.EX.[3SG]</ta>
            <ta e="T696" id="Seg_7710" s="T695">and</ta>
            <ta e="T697" id="Seg_7711" s="T696">warm.[NOM.SG]</ta>
            <ta e="T698" id="Seg_7712" s="T697">NEG.EX.[3SG]</ta>
            <ta e="T699" id="Seg_7713" s="T698">tree-PL</ta>
            <ta e="T700" id="Seg_7714" s="T699">I.LAT</ta>
            <ta e="T701" id="Seg_7715" s="T700">today</ta>
            <ta e="T702" id="Seg_7716" s="T701">NEG</ta>
            <ta e="T703" id="Seg_7717" s="T702">hit-MULT-INF.LAT</ta>
            <ta e="T704" id="Seg_7718" s="T703">enough</ta>
            <ta e="T706" id="Seg_7719" s="T705">that.[NOM.SG]</ta>
            <ta e="T707" id="Seg_7720" s="T706">evening-LAT</ta>
            <ta e="T708" id="Seg_7721" s="T707">tree-PL</ta>
            <ta e="T709" id="Seg_7722" s="T708">I.NOM</ta>
            <ta e="T710" id="Seg_7723" s="T709">many</ta>
            <ta e="T711" id="Seg_7724" s="T710">that.[NOM.SG]</ta>
            <ta e="T712" id="Seg_7725" s="T711">year-LAT</ta>
            <ta e="T713" id="Seg_7726" s="T712">more</ta>
            <ta e="T714" id="Seg_7727" s="T713">grab-FUT-3SG</ta>
            <ta e="T715" id="Seg_7728" s="T714">cold.[NOM.SG]</ta>
            <ta e="T716" id="Seg_7729" s="T715">very</ta>
            <ta e="T717" id="Seg_7730" s="T716">many</ta>
            <ta e="T718" id="Seg_7731" s="T717">tree.[NOM.SG]</ta>
            <ta e="T719" id="Seg_7732" s="T718">go-IPFVZ-PRS-3PL</ta>
            <ta e="T720" id="Seg_7733" s="T719">tree-PL</ta>
            <ta e="T721" id="Seg_7734" s="T720">PTCL</ta>
            <ta e="T722" id="Seg_7735" s="T721">rain-ABL</ta>
            <ta e="T723" id="Seg_7736" s="T722">black.[NOM.SG]</ta>
            <ta e="T724" id="Seg_7737" s="T723">become-PST-3PL</ta>
            <ta e="T725" id="Seg_7738" s="T724">otherwise</ta>
            <ta e="T726" id="Seg_7739" s="T725">snow.[NOM.SG]</ta>
            <ta e="T727" id="Seg_7740" s="T726">be-PST-3PL</ta>
            <ta e="T728" id="Seg_7741" s="T727">I.NOM</ta>
            <ta e="T729" id="Seg_7742" s="T728">%%</ta>
            <ta e="T730" id="Seg_7743" s="T729">larch.[NOM.SG]</ta>
            <ta e="T731" id="Seg_7744" s="T730">tree-PL</ta>
            <ta e="T732" id="Seg_7745" s="T731">snow.[NOM.SG]</ta>
            <ta e="T733" id="Seg_7746" s="T732">tree-PL</ta>
            <ta e="T734" id="Seg_7747" s="T733">PTCL</ta>
            <ta e="T735" id="Seg_7748" s="T734">%%</ta>
            <ta e="T736" id="Seg_7749" s="T735">this-PL</ta>
            <ta e="T737" id="Seg_7750" s="T736">very</ta>
            <ta e="T738" id="Seg_7751" s="T737">good</ta>
            <ta e="T739" id="Seg_7752" s="T738">strongly</ta>
            <ta e="T740" id="Seg_7753" s="T739">burn-DUR-3PL</ta>
            <ta e="T741" id="Seg_7754" s="T740">and</ta>
            <ta e="T742" id="Seg_7755" s="T741">warm.[NOM.SG]</ta>
            <ta e="T743" id="Seg_7756" s="T742">this-PL</ta>
            <ta e="T744" id="Seg_7757" s="T743">%%</ta>
            <ta e="T745" id="Seg_7758" s="T744">place-PRS-2SG</ta>
            <ta e="T746" id="Seg_7759" s="T745">boil-INF.LAT</ta>
            <ta e="T748" id="Seg_7760" s="T747">coal.[NOM.SG]</ta>
            <ta e="T749" id="Seg_7761" s="T748">NEG</ta>
            <ta e="T750" id="Seg_7762" s="T749">NEG</ta>
            <ta e="T751" id="Seg_7763" s="T750">creep.into-PRS.[3SG]</ta>
            <ta e="T752" id="Seg_7764" s="T751">there</ta>
            <ta e="T754" id="Seg_7765" s="T753">very</ta>
            <ta e="T755" id="Seg_7766" s="T754">coal.[NOM.SG]</ta>
            <ta e="T756" id="Seg_7767" s="T755">many</ta>
            <ta e="T757" id="Seg_7768" s="T756">throw.away-PRS.[3SG]</ta>
            <ta e="T758" id="Seg_7769" s="T757">%%</ta>
            <ta e="T759" id="Seg_7770" s="T758">I.LAT</ta>
            <ta e="T762" id="Seg_7771" s="T761">%%</ta>
            <ta e="T763" id="Seg_7772" s="T762">also</ta>
            <ta e="T765" id="Seg_7773" s="T764">and</ta>
            <ta e="T766" id="Seg_7774" s="T765">%%-PST</ta>
            <ta e="T767" id="Seg_7775" s="T766">this.[NOM.SG]</ta>
            <ta e="T768" id="Seg_7776" s="T767">and</ta>
            <ta e="T769" id="Seg_7777" s="T768">I.NOM</ta>
            <ta e="T770" id="Seg_7778" s="T769">self-NOM/GEN/ACC.1SG</ta>
            <ta e="T771" id="Seg_7779" s="T770">put-PST-1SG</ta>
            <ta e="T772" id="Seg_7780" s="T771">tractor-INS</ta>
            <ta e="T773" id="Seg_7781" s="T772">bring-PST-3PL</ta>
            <ta e="T774" id="Seg_7782" s="T773">butter.[NOM.SG]</ta>
            <ta e="T775" id="Seg_7783" s="T774">take-PST-1SG</ta>
            <ta e="T776" id="Seg_7784" s="T775">tractor-LAT</ta>
            <ta e="T777" id="Seg_7785" s="T776">strongly</ta>
            <ta e="T779" id="Seg_7786" s="T778">and</ta>
            <ta e="T780" id="Seg_7787" s="T779">I.NOM</ta>
            <ta e="T781" id="Seg_7788" s="T780">PTCL</ta>
            <ta e="T782" id="Seg_7789" s="T781">tree.[NOM.SG]</ta>
            <ta e="T783" id="Seg_7790" s="T782">carry-PST-1SG</ta>
            <ta e="T784" id="Seg_7791" s="T783">water-VBLZ-CVB</ta>
            <ta e="T785" id="Seg_7792" s="T784">go-PST-1SG</ta>
            <ta e="T786" id="Seg_7793" s="T785">cow-LAT</ta>
            <ta e="T787" id="Seg_7794" s="T786">stand-IMP.2SG</ta>
            <ta e="T788" id="Seg_7795" s="T787">give-PST-1SG</ta>
            <ta e="T789" id="Seg_7796" s="T788">milk-PST-1SG</ta>
            <ta e="T790" id="Seg_7797" s="T789">then</ta>
            <ta e="T791" id="Seg_7798" s="T790">sit-PST-1SG</ta>
            <ta e="T792" id="Seg_7799" s="T791">eat-INF.LAT</ta>
            <ta e="T793" id="Seg_7800" s="T792">and</ta>
            <ta e="T794" id="Seg_7801" s="T793">think-DUR-1SG</ta>
            <ta e="T795" id="Seg_7802" s="T794">this.[NOM.SG]</ta>
            <ta e="T796" id="Seg_7803" s="T795">Elya.[NOM.SG]</ta>
            <ta e="T797" id="Seg_7804" s="T796">I.LAT</ta>
            <ta e="T798" id="Seg_7805" s="T797">PTCL</ta>
            <ta e="T799" id="Seg_7806" s="T798">lie-PST.[3SG]</ta>
            <ta e="T800" id="Seg_7807" s="T799">money-NOM/GEN.3SG</ta>
            <ta e="T801" id="Seg_7808" s="T800">three.[NOM.SG]</ta>
            <ta e="T802" id="Seg_7809" s="T801">rouble.[NOM.SG]</ta>
            <ta e="T804" id="Seg_7810" s="T803">be-PST.[3SG]</ta>
            <ta e="T805" id="Seg_7811" s="T804">and</ta>
            <ta e="T806" id="Seg_7812" s="T805">I.LAT</ta>
            <ta e="T807" id="Seg_7813" s="T806">be-PST.[3SG]</ta>
            <ta e="T808" id="Seg_7814" s="T807">one.[NOM.SG]</ta>
            <ta e="T809" id="Seg_7815" s="T808">rouble.[NOM.SG]</ta>
            <ta e="T810" id="Seg_7816" s="T809">two.[NOM.SG]</ta>
            <ta e="T811" id="Seg_7817" s="T810">ten.[NOM.SG]</ta>
            <ta e="T812" id="Seg_7818" s="T811">five.[NOM.SG]</ta>
            <ta e="T813" id="Seg_7819" s="T812">this-LAT</ta>
            <ta e="T815" id="Seg_7820" s="T814">take-INF.LAT</ta>
            <ta e="T816" id="Seg_7821" s="T815">I.LAT</ta>
            <ta e="T817" id="Seg_7822" s="T816">one.[NOM.SG]</ta>
            <ta e="T818" id="Seg_7823" s="T817">rouble.[NOM.SG]</ta>
            <ta e="T819" id="Seg_7824" s="T818">and</ta>
            <ta e="T820" id="Seg_7825" s="T819">ten.[NOM.SG]</ta>
            <ta e="T821" id="Seg_7826" s="T820">five.[NOM.SG]</ta>
            <ta e="T822" id="Seg_7827" s="T821">copeck-NOM/GEN/ACC.3PL</ta>
            <ta e="T823" id="Seg_7828" s="T822">I.NOM</ta>
            <ta e="T824" id="Seg_7829" s="T823">today</ta>
            <ta e="T825" id="Seg_7830" s="T824">water-LAT</ta>
            <ta e="T826" id="Seg_7831" s="T825">go-PST-1SG</ta>
            <ta e="T827" id="Seg_7832" s="T826">and</ta>
            <ta e="T828" id="Seg_7833" s="T827">very</ta>
            <ta e="T829" id="Seg_7834" s="T828">strongly</ta>
            <ta e="T830" id="Seg_7835" s="T829">freeze-RES-PST.[3SG]</ta>
            <ta e="T831" id="Seg_7836" s="T830">I.NOM</ta>
            <ta e="T832" id="Seg_7837" s="T831">return-PST-1SG</ta>
            <ta e="T833" id="Seg_7838" s="T832">axe.[NOM.SG]</ta>
            <ta e="T834" id="Seg_7839" s="T833">take-PST-1SG</ta>
            <ta e="T835" id="Seg_7840" s="T834">axe-INS</ta>
            <ta e="T836" id="Seg_7841" s="T835">cut-PST-1SG</ta>
            <ta e="T837" id="Seg_7842" s="T836">cut-PST-1SG</ta>
            <ta e="T838" id="Seg_7843" s="T837">PTCL</ta>
            <ta e="T839" id="Seg_7844" s="T838">snow.[NOM.SG]</ta>
            <ta e="T842" id="Seg_7845" s="T841">freeze-RES-PST.[3SG]</ta>
            <ta e="T843" id="Seg_7846" s="T842">very</ta>
            <ta e="T844" id="Seg_7847" s="T843">cold.[NOM.SG]</ta>
            <ta e="T845" id="Seg_7848" s="T844">morning.[NOM.SG]</ta>
            <ta e="T846" id="Seg_7849" s="T845">be-PST.[3SG]</ta>
            <ta e="T847" id="Seg_7850" s="T846">house-LAT/LOC.1SG</ta>
            <ta e="T848" id="Seg_7851" s="T847">come-PST-1SG</ta>
            <ta e="T849" id="Seg_7852" s="T848">you.PL.ACC</ta>
            <ta e="T850" id="Seg_7853" s="T849">wait-PST-1SG</ta>
            <ta e="T851" id="Seg_7854" s="T850">and</ta>
            <ta e="T852" id="Seg_7855" s="T851">water.[NOM.SG]</ta>
            <ta e="T853" id="Seg_7856" s="T852">bring-PST-1SG</ta>
            <ta e="T854" id="Seg_7857" s="T853">stove-LAT</ta>
            <ta e="T858" id="Seg_7858" s="T857">climb-PST-1SG</ta>
            <ta e="T859" id="Seg_7859" s="T858">sit-PST-1SG</ta>
            <ta e="T860" id="Seg_7860" s="T859">and</ta>
            <ta e="T861" id="Seg_7861" s="T860">%%</ta>
            <ta e="T862" id="Seg_7862" s="T861">then</ta>
            <ta e="T863" id="Seg_7863" s="T862">come-PST-3PL</ta>
            <ta e="T864" id="Seg_7864" s="T863">and</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_7865" s="T0">мы.NOM</ta>
            <ta e="T2" id="Seg_7866" s="T1">PTCL</ta>
            <ta e="T3" id="Seg_7867" s="T2">Свердловск-LOC</ta>
            <ta e="T4" id="Seg_7868" s="T3">жить-%1PL.PST</ta>
            <ta e="T7" id="Seg_7869" s="T6">железо-ADJZ</ta>
            <ta e="T8" id="Seg_7870" s="T7">железо-ADJZ</ta>
            <ta e="T9" id="Seg_7871" s="T8">птица-LAT</ta>
            <ta e="T10" id="Seg_7872" s="T9">сидеть-PST-1PL</ta>
            <ta e="T11" id="Seg_7873" s="T10">тогда</ta>
            <ta e="T12" id="Seg_7874" s="T11">лететь-PST-1PL</ta>
            <ta e="T13" id="Seg_7875" s="T12">дорога-LOC</ta>
            <ta e="T14" id="Seg_7876" s="T13">NEG</ta>
            <ta e="T15" id="Seg_7877" s="T14">мочь-PRS.[3SG]</ta>
            <ta e="T17" id="Seg_7878" s="T16">лететь-INF.LAT</ta>
            <ta e="T18" id="Seg_7879" s="T17">дым-ADJZ.[NOM.SG]</ta>
            <ta e="T19" id="Seg_7880" s="T18">PTCL</ta>
            <ta e="T22" id="Seg_7881" s="T21">стоять-DUR.[3SG]</ta>
            <ta e="T23" id="Seg_7882" s="T22">тогда</ta>
            <ta e="T24" id="Seg_7883" s="T23">прийти-PST-1PL</ta>
            <ta e="T25" id="Seg_7884" s="T24">Красноярск-LAT</ta>
            <ta e="T26" id="Seg_7885" s="T25">там</ta>
            <ta e="T27" id="Seg_7886" s="T26">сидеть-PST-1PL</ta>
            <ta e="T28" id="Seg_7887" s="T27">там</ta>
            <ta e="T29" id="Seg_7888" s="T28">жить-PST-1SG</ta>
            <ta e="T30" id="Seg_7889" s="T29">семь.[NOM.SG]</ta>
            <ta e="T31" id="Seg_7890" s="T30">день.[NOM.SG]</ta>
            <ta e="T32" id="Seg_7891" s="T31">жить-PST-1SG</ta>
            <ta e="T33" id="Seg_7892" s="T32">тогда</ta>
            <ta e="T34" id="Seg_7893" s="T33">сидеть-PST-1SG</ta>
            <ta e="T35" id="Seg_7894" s="T34">железо-ADJZ</ta>
            <ta e="T36" id="Seg_7895" s="T35">дорога-LAT</ta>
            <ta e="T37" id="Seg_7896" s="T36">прийти-PST-1SG</ta>
            <ta e="T38" id="Seg_7897" s="T37">Уяр-LAT</ta>
            <ta e="T39" id="Seg_7898" s="T38">тогда</ta>
            <ta e="T40" id="Seg_7899" s="T39">Уяр-ABL</ta>
            <ta e="T41" id="Seg_7900" s="T40">прийти-PST-1SG</ta>
            <ta e="T43" id="Seg_7901" s="T42">сидеть-PST-1SG</ta>
            <ta e="T44" id="Seg_7902" s="T43">автобус-LAT</ta>
            <ta e="T45" id="Seg_7903" s="T44">прийти-PST-1SG</ta>
            <ta e="T46" id="Seg_7904" s="T45">Саянск-GEN</ta>
            <ta e="T47" id="Seg_7905" s="T46">здесь</ta>
            <ta e="T48" id="Seg_7906" s="T47">жить-PST-1SG</ta>
            <ta e="T51" id="Seg_7907" s="T50">два.[NOM.SG]</ta>
            <ta e="T52" id="Seg_7908" s="T51">день.[NOM.SG]</ta>
            <ta e="T53" id="Seg_7909" s="T52">тогда</ta>
            <ta e="T54" id="Seg_7910" s="T53">Абалаково-LAT</ta>
            <ta e="T55" id="Seg_7911" s="T54">прийти-PST-1SG</ta>
            <ta e="T56" id="Seg_7912" s="T55">и</ta>
            <ta e="T57" id="Seg_7913" s="T56">ты.DAT</ta>
            <ta e="T58" id="Seg_7914" s="T57">прийти-PST-1SG</ta>
            <ta e="T59" id="Seg_7915" s="T58">ты.DAT</ta>
            <ta e="T60" id="Seg_7916" s="T59">прийти-PST-1SG</ta>
            <ta e="T61" id="Seg_7917" s="T60">говорить-PST-1PL</ta>
            <ta e="T62" id="Seg_7918" s="T61">тогда</ta>
            <ta e="T63" id="Seg_7919" s="T62">ты.NOM</ta>
            <ta e="T64" id="Seg_7920" s="T63">я.NOM</ta>
            <ta e="T65" id="Seg_7921" s="T64">я.LAT</ta>
            <ta e="T67" id="Seg_7922" s="T66">сказать-FUT-2SG</ta>
            <ta e="T68" id="Seg_7923" s="T67">сидеть-IMP.2SG</ta>
            <ta e="T69" id="Seg_7924" s="T68">есть-IMP.2PL</ta>
            <ta e="T71" id="Seg_7925" s="T70">быть.голодным-PRS-2SG</ta>
            <ta e="T72" id="Seg_7926" s="T71">наверное=PTCL</ta>
            <ta e="T73" id="Seg_7927" s="T72">тогда</ta>
            <ta e="T74" id="Seg_7928" s="T73">я.NOM</ta>
            <ta e="T75" id="Seg_7929" s="T74">сидеть-PST-1SG</ta>
            <ta e="T76" id="Seg_7930" s="T75">есть-PST-1SG</ta>
            <ta e="T77" id="Seg_7931" s="T76">пойти-PST-1SG</ta>
            <ta e="T78" id="Seg_7932" s="T77">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T79" id="Seg_7933" s="T78">квартира-3SG-INS</ta>
            <ta e="T80" id="Seg_7934" s="T79">один.[NOM.SG]</ta>
            <ta e="T81" id="Seg_7935" s="T80">один.[NOM.SG]</ta>
            <ta e="T82" id="Seg_7936" s="T81">раз.[NOM.SG]</ta>
            <ta e="T83" id="Seg_7937" s="T82">мы.NOM</ta>
            <ta e="T84" id="Seg_7938" s="T83">пойти-PST-1PL</ta>
            <ta e="T85" id="Seg_7939" s="T84">три.[NOM.SG]</ta>
            <ta e="T86" id="Seg_7940" s="T85">женщина.[NOM.SG]</ta>
            <ta e="T87" id="Seg_7941" s="T86">Заозерка-LAT</ta>
            <ta e="T88" id="Seg_7942" s="T87">тогда</ta>
            <ta e="T89" id="Seg_7943" s="T88">оттуда</ta>
            <ta e="T90" id="Seg_7944" s="T89">железо-ADJZ</ta>
            <ta e="T91" id="Seg_7945" s="T90">дорога-LAT</ta>
            <ta e="T93" id="Seg_7946" s="T92">сидеть-PST-1PL</ta>
            <ta e="T94" id="Seg_7947" s="T93">пойти-PST-1PL</ta>
            <ta e="T95" id="Seg_7948" s="T94">Уяр-LAT</ta>
            <ta e="T96" id="Seg_7949" s="T95">там</ta>
            <ta e="T97" id="Seg_7950" s="T96">бог-LAT</ta>
            <ta e="T98" id="Seg_7951" s="T97">молиться-INF.LAT</ta>
            <ta e="T99" id="Seg_7952" s="T98">там</ta>
            <ta e="T100" id="Seg_7953" s="T99">бог-LAT</ta>
            <ta e="T101" id="Seg_7954" s="T100">молиться-CVB-PST-1PL</ta>
            <ta e="T102" id="Seg_7955" s="T101">тогда</ta>
            <ta e="T103" id="Seg_7956" s="T102">вернуться-MOM-PST-1PL</ta>
            <ta e="T104" id="Seg_7957" s="T103">опять</ta>
            <ta e="T106" id="Seg_7958" s="T105">здесь</ta>
            <ta e="T107" id="Seg_7959" s="T106">Заозерка-LAT</ta>
            <ta e="T108" id="Seg_7960" s="T107">там</ta>
            <ta e="T109" id="Seg_7961" s="T108">здесь</ta>
            <ta e="T110" id="Seg_7962" s="T109">ночевать-%1PL.PST</ta>
            <ta e="T111" id="Seg_7963" s="T110">много</ta>
            <ta e="T112" id="Seg_7964" s="T111">говорить-CVB-PST-1PL</ta>
            <ta e="T113" id="Seg_7965" s="T112">тогда</ta>
            <ta e="T114" id="Seg_7966" s="T113">%%</ta>
            <ta e="T115" id="Seg_7967" s="T114">пойти-PST.[3SG]</ta>
            <ta e="T116" id="Seg_7968" s="T115">автобус.[NOM.SG]</ta>
            <ta e="T117" id="Seg_7969" s="T116">смотреть-FRQ-INF.LAT</ta>
            <ta e="T118" id="Seg_7970" s="T117">прийти-PST.[3SG]</ta>
            <ta e="T119" id="Seg_7971" s="T118">и</ta>
            <ta e="T120" id="Seg_7972" s="T119">%%</ta>
            <ta e="T868" id="Seg_7973" s="T120">пойти-CVB</ta>
            <ta e="T121" id="Seg_7974" s="T868">исчезнуть-PST.[3SG]</ta>
            <ta e="T122" id="Seg_7975" s="T121">а</ta>
            <ta e="T123" id="Seg_7976" s="T122">мы.NOM</ta>
            <ta e="T124" id="Seg_7977" s="T123">собирать-PST-1PL</ta>
            <ta e="T125" id="Seg_7978" s="T124">и</ta>
            <ta e="T126" id="Seg_7979" s="T125">пойти-PST-1PL</ta>
            <ta e="T127" id="Seg_7980" s="T126">наружу</ta>
            <ta e="T128" id="Seg_7981" s="T127">здесь</ta>
            <ta e="T129" id="Seg_7982" s="T128">автобус.[NOM.SG]</ta>
            <ta e="T130" id="Seg_7983" s="T129">прийти-PRS.[3SG]</ta>
            <ta e="T131" id="Seg_7984" s="T130">сесть-TR-PST.[3SG]</ta>
            <ta e="T132" id="Seg_7985" s="T131">мы.NOM</ta>
            <ta e="T133" id="Seg_7986" s="T132">и</ta>
            <ta e="T134" id="Seg_7987" s="T133">Переясловка-LAT</ta>
            <ta e="T135" id="Seg_7988" s="T134">прийти-PST-1PL</ta>
            <ta e="T136" id="Seg_7989" s="T135">там</ta>
            <ta e="T139" id="Seg_7990" s="T137">мы.ACC</ta>
            <ta e="T140" id="Seg_7991" s="T139">там</ta>
            <ta e="T141" id="Seg_7992" s="T140">мы.NOM</ta>
            <ta e="T142" id="Seg_7993" s="T141">там</ta>
            <ta e="T143" id="Seg_7994" s="T142">спуститься-PST-1PL</ta>
            <ta e="T144" id="Seg_7995" s="T143">и</ta>
            <ta e="T145" id="Seg_7996" s="T144">стоять-PST-1PL</ta>
            <ta e="T146" id="Seg_7997" s="T145">я.NOM</ta>
            <ta e="T147" id="Seg_7998" s="T146">сказать-IPFVZ-1SG</ta>
            <ta e="T148" id="Seg_7999" s="T147">что.[NOM.SG]</ta>
            <ta e="T149" id="Seg_8000" s="T148">стоять-DUR-INF.LAT</ta>
            <ta e="T150" id="Seg_8001" s="T149">пойти-OPT.DU/PL-1PL</ta>
            <ta e="T151" id="Seg_8002" s="T150">пешком</ta>
            <ta e="T152" id="Seg_8003" s="T151">тогда</ta>
            <ta e="T153" id="Seg_8004" s="T152">пойти-OPT.DU/PL-1PL</ta>
            <ta e="T154" id="Seg_8005" s="T153">идти-DUR-1PL</ta>
            <ta e="T155" id="Seg_8006" s="T154">машина.[NOM.SG]</ta>
            <ta e="T156" id="Seg_8007" s="T155">PTCL</ta>
            <ta e="T157" id="Seg_8008" s="T156">прийти-PRS.[3SG]</ta>
            <ta e="T158" id="Seg_8009" s="T157">сидеть-IMP.2PL</ta>
            <ta e="T159" id="Seg_8010" s="T158">мы.NOM</ta>
            <ta e="T162" id="Seg_8011" s="T161">сидеть-IMP.2PL</ta>
            <ta e="T163" id="Seg_8012" s="T162">здесь</ta>
            <ta e="T164" id="Seg_8013" s="T163">тогда</ta>
            <ta e="T165" id="Seg_8014" s="T164">мы.NOM</ta>
            <ta e="T166" id="Seg_8015" s="T165">сидеть-PST-1PL</ta>
            <ta e="T167" id="Seg_8016" s="T166">и</ta>
            <ta e="T168" id="Seg_8017" s="T167">прийти-PST-1PL</ta>
            <ta e="T169" id="Seg_8018" s="T168">Усть_Кандыга-LAT</ta>
            <ta e="T170" id="Seg_8019" s="T169">тогда</ta>
            <ta e="T171" id="Seg_8020" s="T170">там</ta>
            <ta e="T172" id="Seg_8021" s="T171">еще</ta>
            <ta e="T173" id="Seg_8022" s="T172">семь.[NOM.SG]</ta>
            <ta e="T175" id="Seg_8023" s="T174">остаться-MOM-PST.[3SG]</ta>
            <ta e="T176" id="Seg_8024" s="T175">Унер-NOM/GEN/ACC.3SG</ta>
            <ta e="T177" id="Seg_8025" s="T176">пойти-INF.LAT</ta>
            <ta e="T178" id="Seg_8026" s="T177">два.[NOM.SG]</ta>
            <ta e="T179" id="Seg_8027" s="T178">женщина-PL</ta>
            <ta e="T180" id="Seg_8028" s="T179">пойти-PST-3PL</ta>
            <ta e="T181" id="Seg_8029" s="T180">дом-LAT</ta>
            <ta e="T182" id="Seg_8030" s="T181">а</ta>
            <ta e="T183" id="Seg_8031" s="T182">я.NOM</ta>
            <ta e="T184" id="Seg_8032" s="T183">пойти-PST-1SG</ta>
            <ta e="T185" id="Seg_8033" s="T184">пешком</ta>
            <ta e="T186" id="Seg_8034" s="T185">Унер-NOM/GEN/ACC.3SG</ta>
            <ta e="T187" id="Seg_8035" s="T186">прийти-PST-1SG</ta>
            <ta e="T188" id="Seg_8036" s="T187">принести-IMP.2PL</ta>
            <ta e="T189" id="Seg_8037" s="T188">я.LAT</ta>
            <ta e="T190" id="Seg_8038" s="T189">хлеб.[NOM.SG]</ta>
            <ta e="T191" id="Seg_8039" s="T190">продавать-IMP.2PL</ta>
            <ta e="T192" id="Seg_8040" s="T191">хлеб.[NOM.SG]</ta>
            <ta e="T193" id="Seg_8041" s="T192">этот-PL</ta>
            <ta e="T194" id="Seg_8042" s="T193">сказать-IPFVZ.[3SG]</ta>
            <ta e="T196" id="Seg_8043" s="T195">пойти-EP-IMP.2SG</ta>
            <ta e="T197" id="Seg_8044" s="T196">магазин-LAT</ta>
            <ta e="T199" id="Seg_8045" s="T198">там</ta>
            <ta e="T200" id="Seg_8046" s="T199">взять-IMP.2SG</ta>
            <ta e="T201" id="Seg_8047" s="T200">я.LAT</ta>
            <ta e="T202" id="Seg_8048" s="T201">много</ta>
            <ta e="T203" id="Seg_8049" s="T202">NEG</ta>
            <ta e="T204" id="Seg_8050" s="T203">нужно</ta>
            <ta e="T205" id="Seg_8051" s="T204">два.[NOM.SG]</ta>
            <ta e="T206" id="Seg_8052" s="T205">грамм.[NOM.SG]</ta>
            <ta e="T207" id="Seg_8053" s="T206">дать-PST-2PL</ta>
            <ta e="T208" id="Seg_8054" s="T207">этот.[NOM.SG]</ta>
            <ta e="T209" id="Seg_8055" s="T208">я.LAT</ta>
            <ta e="T210" id="Seg_8056" s="T209">два.[NOM.SG]</ta>
            <ta e="T211" id="Seg_8057" s="T210">грамм.[NOM.SG]</ta>
            <ta e="T212" id="Seg_8058" s="T211">вешать-PST.[3SG]</ta>
            <ta e="T213" id="Seg_8059" s="T212">я.NOM</ta>
            <ta e="T214" id="Seg_8060" s="T213">взять-PST-1SG</ta>
            <ta e="T215" id="Seg_8061" s="T214">деньги.[NOM.SG]</ta>
            <ta e="T216" id="Seg_8062" s="T215">дать-PST-1SG</ta>
            <ta e="T217" id="Seg_8063" s="T216">съесть-PST-1SG</ta>
            <ta e="T218" id="Seg_8064" s="T217">и</ta>
            <ta e="T219" id="Seg_8065" s="T218">пойти-PST-1SG</ta>
            <ta e="T220" id="Seg_8066" s="T219">вода.[NOM.SG]</ta>
            <ta e="T221" id="Seg_8067" s="T220">пить-PST-1SG</ta>
            <ta e="T222" id="Seg_8068" s="T221">тогда</ta>
            <ta e="T223" id="Seg_8069" s="T222">машина.[NOM.SG]</ta>
            <ta e="T224" id="Seg_8070" s="T223">догонять-PST.[3SG]</ta>
            <ta e="T225" id="Seg_8071" s="T224">я.NOM</ta>
            <ta e="T227" id="Seg_8072" s="T226">сидеть-PST-1SG</ta>
            <ta e="T228" id="Seg_8073" s="T227">тогда</ta>
            <ta e="T229" id="Seg_8074" s="T228">спуститься-PST-1SG</ta>
            <ta e="T230" id="Seg_8075" s="T229">два.[NOM.SG]</ta>
            <ta e="T231" id="Seg_8076" s="T230">мужчина.[NOM.SG]</ta>
            <ta e="T232" id="Seg_8077" s="T231">стоять-PRS-3PL</ta>
            <ta e="T233" id="Seg_8078" s="T232">гора-LOC</ta>
            <ta e="T234" id="Seg_8079" s="T233">я.NOM</ta>
            <ta e="T235" id="Seg_8080" s="T234">там</ta>
            <ta e="T237" id="Seg_8081" s="T236">влезать-PST-1SG</ta>
            <ta e="T238" id="Seg_8082" s="T237">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T239" id="Seg_8083" s="T238">%вместе</ta>
            <ta e="T240" id="Seg_8084" s="T239">нет</ta>
            <ta e="T241" id="Seg_8085" s="T240">мы.NOM</ta>
            <ta e="T242" id="Seg_8086" s="T241">ждать-DUR.[3SG]</ta>
            <ta e="T243" id="Seg_8087" s="T242">машина.[NOM.SG]</ta>
            <ta e="T244" id="Seg_8088" s="T243">машина-INS</ta>
            <ta e="T245" id="Seg_8089" s="T244">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T246" id="Seg_8090" s="T245">тогда</ta>
            <ta e="T247" id="Seg_8091" s="T246">машина.[NOM.SG]</ta>
            <ta e="T248" id="Seg_8092" s="T247">прийти-PST.[3SG]</ta>
            <ta e="T251" id="Seg_8093" s="T250">сидеть-PST-1SG</ta>
            <ta e="T252" id="Seg_8094" s="T251">и</ta>
            <ta e="T253" id="Seg_8095" s="T252">Саянский-LAT</ta>
            <ta e="T254" id="Seg_8096" s="T253">прийти-PST-1SG</ta>
            <ta e="T255" id="Seg_8097" s="T254">тогда</ta>
            <ta e="T256" id="Seg_8098" s="T255">%%</ta>
            <ta e="T258" id="Seg_8099" s="T257">взять-PST-1SG</ta>
            <ta e="T259" id="Seg_8100" s="T258">хлеб.[NOM.SG]</ta>
            <ta e="T260" id="Seg_8101" s="T259">сахар.[NOM.SG]</ta>
            <ta e="T261" id="Seg_8102" s="T260">Ванька.[NOM.SG]</ta>
            <ta e="T262" id="Seg_8103" s="T261">больница-LOC</ta>
            <ta e="T263" id="Seg_8104" s="T262">лежать-DUR-PST.[3SG]</ta>
            <ta e="T264" id="Seg_8105" s="T263">нести-PST-1SG</ta>
            <ta e="T265" id="Seg_8106" s="T264">этот-LAT</ta>
            <ta e="T266" id="Seg_8107" s="T265">я.LAT</ta>
            <ta e="T267" id="Seg_8108" s="T266">там</ta>
            <ta e="T268" id="Seg_8109" s="T267">сказать-DUR-3PL</ta>
            <ta e="T269" id="Seg_8110" s="T268">здесь</ta>
            <ta e="T270" id="Seg_8111" s="T269">быть-IMP.2PL</ta>
            <ta e="T271" id="Seg_8112" s="T270">мы.NOM</ta>
            <ta e="T272" id="Seg_8113" s="T271">машина.[NOM.SG]</ta>
            <ta e="T273" id="Seg_8114" s="T272">пойти-EP-IMP.2SG</ta>
            <ta e="T274" id="Seg_8115" s="T273">я.NOM</ta>
            <ta e="T275" id="Seg_8116" s="T274">бежать-PST-1SG</ta>
            <ta e="T276" id="Seg_8117" s="T275">где=INDEF</ta>
            <ta e="T277" id="Seg_8118" s="T276">NEG</ta>
            <ta e="T278" id="Seg_8119" s="T277">найти-PST-1SG</ta>
            <ta e="T279" id="Seg_8120" s="T278">тогда</ta>
            <ta e="T280" id="Seg_8121" s="T279">пойти-PST-1SG</ta>
            <ta e="T281" id="Seg_8122" s="T280">пешком</ta>
            <ta e="T282" id="Seg_8123" s="T281">тогда</ta>
            <ta e="T283" id="Seg_8124" s="T282">опять</ta>
            <ta e="T284" id="Seg_8125" s="T283">машина.[NOM.SG]</ta>
            <ta e="T285" id="Seg_8126" s="T284">прийти-PRS.[3SG]</ta>
            <ta e="T286" id="Seg_8127" s="T285">я.NOM</ta>
            <ta e="T287" id="Seg_8128" s="T286">сидеть-PST-1SG</ta>
            <ta e="T288" id="Seg_8129" s="T287">Малиновка-LAT</ta>
            <ta e="T289" id="Seg_8130" s="T288">прийти-PST-1SG</ta>
            <ta e="T290" id="Seg_8131" s="T289">тогда</ta>
            <ta e="T291" id="Seg_8132" s="T290">опять</ta>
            <ta e="T292" id="Seg_8133" s="T291">нога-INS</ta>
            <ta e="T293" id="Seg_8134" s="T292">пойти-PST-1SG</ta>
            <ta e="T294" id="Seg_8135" s="T293">тогда</ta>
            <ta e="T295" id="Seg_8136" s="T294">опять</ta>
            <ta e="T296" id="Seg_8137" s="T295">машина.[NOM.SG]</ta>
            <ta e="T297" id="Seg_8138" s="T296">догонять-PST.[3SG]</ta>
            <ta e="T298" id="Seg_8139" s="T297">и</ta>
            <ta e="T299" id="Seg_8140" s="T298">дом-LAT/LOC.3SG</ta>
            <ta e="T300" id="Seg_8141" s="T299">прийти-PST-1SG</ta>
            <ta e="T301" id="Seg_8142" s="T300">когда</ta>
            <ta e="T302" id="Seg_8143" s="T301">Матвеев.[NOM.SG]</ta>
            <ta e="T303" id="Seg_8144" s="T302">прийти-PST.[3SG]</ta>
            <ta e="T304" id="Seg_8145" s="T303">я.LAT</ta>
            <ta e="T306" id="Seg_8146" s="T305">говорить-DUR.[3SG]</ta>
            <ta e="T307" id="Seg_8147" s="T306">камасинец-TR-CVB</ta>
            <ta e="T308" id="Seg_8148" s="T307">а</ta>
            <ta e="T309" id="Seg_8149" s="T308">я.NOM</ta>
            <ta e="T310" id="Seg_8150" s="T309">русский</ta>
            <ta e="T311" id="Seg_8151" s="T310">русский</ta>
            <ta e="T312" id="Seg_8152" s="T311">говорить-DUR-1SG</ta>
            <ta e="T313" id="Seg_8153" s="T312">и</ta>
            <ta e="T314" id="Seg_8154" s="T313">%%-DUR-1SG</ta>
            <ta e="T315" id="Seg_8155" s="T314">ты.NOM</ta>
            <ta e="T316" id="Seg_8156" s="T315">сейчас</ta>
            <ta e="T317" id="Seg_8157" s="T316">камасинец.[NOM.SG]</ta>
            <ta e="T318" id="Seg_8158" s="T317">быть-PRS-2SG</ta>
            <ta e="T319" id="Seg_8159" s="T318">а</ta>
            <ta e="T320" id="Seg_8160" s="T319">я.NOM</ta>
            <ta e="T321" id="Seg_8161" s="T320">русский.[NOM.SG]</ta>
            <ta e="T322" id="Seg_8162" s="T321">быть-PRS-1SG</ta>
            <ta e="T323" id="Seg_8163" s="T322">тогда</ta>
            <ta e="T324" id="Seg_8164" s="T323">этот-PL</ta>
            <ta e="T325" id="Seg_8165" s="T324">лес-LAT</ta>
            <ta e="T326" id="Seg_8166" s="T325">пойти-PST-3PL</ta>
            <ta e="T327" id="Seg_8167" s="T326">Саяны-LAT</ta>
            <ta e="T329" id="Seg_8168" s="T328">рыба.[NOM.SG]</ta>
            <ta e="T330" id="Seg_8169" s="T329">ловить-PST-3PL</ta>
            <ta e="T331" id="Seg_8170" s="T330">там</ta>
            <ta e="T334" id="Seg_8171" s="T333">класть-PST-3PL</ta>
            <ta e="T335" id="Seg_8172" s="T334">тогда</ta>
            <ta e="T336" id="Seg_8173" s="T335">один.[NOM.SG]</ta>
            <ta e="T337" id="Seg_8174" s="T336">девушка.[NOM.SG]</ta>
            <ta e="T338" id="Seg_8175" s="T337">большой.[NOM.SG]</ta>
            <ta e="T339" id="Seg_8176" s="T338">PTCL</ta>
            <ta e="T340" id="Seg_8177" s="T339">болеть-MOM-PST.[3SG]</ta>
            <ta e="T341" id="Seg_8178" s="T340">PTCL</ta>
            <ta e="T342" id="Seg_8179" s="T341">тогда</ta>
            <ta e="T343" id="Seg_8180" s="T342">этот-PL</ta>
            <ta e="T344" id="Seg_8181" s="T343">прийти-PST-3PL</ta>
            <ta e="T345" id="Seg_8182" s="T344">дом-LAT</ta>
            <ta e="T346" id="Seg_8183" s="T345">Кан_Оклер-LAT</ta>
            <ta e="T347" id="Seg_8184" s="T346">и</ta>
            <ta e="T348" id="Seg_8185" s="T347">женщина.[NOM.SG]</ta>
            <ta e="T349" id="Seg_8186" s="T348">взять-PST-3PL</ta>
            <ta e="T350" id="Seg_8187" s="T349">и</ta>
            <ta e="T351" id="Seg_8188" s="T350">этот-ACC</ta>
            <ta e="T352" id="Seg_8189" s="T351">принести-PST-3PL</ta>
            <ta e="T353" id="Seg_8190" s="T352">больница-LAT</ta>
            <ta e="T354" id="Seg_8191" s="T353">там</ta>
            <ta e="T355" id="Seg_8192" s="T354">нога-GEN</ta>
            <ta e="T356" id="Seg_8193" s="T355">PTCL</ta>
            <ta e="T357" id="Seg_8194" s="T356">вода.[NOM.SG]</ta>
            <ta e="T358" id="Seg_8195" s="T357">пить-PST-3PL</ta>
            <ta e="T359" id="Seg_8196" s="T358">и</ta>
            <ta e="T360" id="Seg_8197" s="T359">%%-PST-3PL</ta>
            <ta e="T361" id="Seg_8198" s="T360">вылечить-PST-3PL</ta>
            <ta e="T362" id="Seg_8199" s="T361">а.то</ta>
            <ta e="T363" id="Seg_8200" s="T362">этот.[NOM.SG]</ta>
            <ta e="T364" id="Seg_8201" s="T363">идти-INF.LAT</ta>
            <ta e="T365" id="Seg_8202" s="T364">NEG</ta>
            <ta e="T367" id="Seg_8203" s="T366">NEG</ta>
            <ta e="T368" id="Seg_8204" s="T367">мочь-PRS.[3SG]</ta>
            <ta e="T369" id="Seg_8205" s="T368">я.NOM</ta>
            <ta e="T372" id="Seg_8206" s="T371">смотреть-FRQ-PST-1SG</ta>
            <ta e="T373" id="Seg_8207" s="T372">смотреть-FRQ-PST-1SG</ta>
            <ta e="T375" id="Seg_8208" s="T374">там</ta>
            <ta e="T376" id="Seg_8209" s="T375">писать-PST-1SG</ta>
            <ta e="T377" id="Seg_8210" s="T376">бумага.[NOM.SG]</ta>
            <ta e="T378" id="Seg_8211" s="T377">там</ta>
            <ta e="T380" id="Seg_8212" s="T379">писать-PST-1SG</ta>
            <ta e="T381" id="Seg_8213" s="T380">кто.[NOM.SG]</ta>
            <ta e="T383" id="Seg_8214" s="T381">я.NOM=PTCL</ta>
            <ta e="T384" id="Seg_8215" s="T383">видеть-RES-PST.[3SG]</ta>
            <ta e="T385" id="Seg_8216" s="T384">кто.[NOM.SG]</ta>
            <ta e="T387" id="Seg_8217" s="T385">я.NOM=PTCL</ta>
            <ta e="T388" id="Seg_8218" s="T387">NEG</ta>
            <ta e="T389" id="Seg_8219" s="T388">видеть-RES-PST.[3SG]</ta>
            <ta e="T390" id="Seg_8220" s="T389">где=INDEF</ta>
            <ta e="T391" id="Seg_8221" s="T390">NEG.EX.[3SG]</ta>
            <ta e="T392" id="Seg_8222" s="T391">правда-PL</ta>
            <ta e="T393" id="Seg_8223" s="T392">а</ta>
            <ta e="T394" id="Seg_8224" s="T393">я.NOM</ta>
            <ta e="T395" id="Seg_8225" s="T394">думать-PST-1SG</ta>
            <ta e="T396" id="Seg_8226" s="T395">быть-PRS.[3SG]</ta>
            <ta e="T397" id="Seg_8227" s="T396">везде</ta>
            <ta e="T398" id="Seg_8228" s="T397">правда.[NOM.SG]</ta>
            <ta e="T399" id="Seg_8229" s="T398">быть-PRS.[3SG]</ta>
            <ta e="T400" id="Seg_8230" s="T399">а</ta>
            <ta e="T401" id="Seg_8231" s="T400">этот-ACC</ta>
            <ta e="T402" id="Seg_8232" s="T401">где=INDEF</ta>
            <ta e="T403" id="Seg_8233" s="T402">NEG.EX.[3SG]</ta>
            <ta e="T404" id="Seg_8234" s="T403">всегда</ta>
            <ta e="T405" id="Seg_8235" s="T404">%%</ta>
            <ta e="T406" id="Seg_8236" s="T405">тогда</ta>
            <ta e="T407" id="Seg_8237" s="T406">прокурор.[NOM.SG]</ta>
            <ta e="T408" id="Seg_8238" s="T407">писать-PST.[3SG]</ta>
            <ta e="T409" id="Seg_8239" s="T408">куда=INDEF</ta>
            <ta e="T869" id="Seg_8240" s="T409">пойти-CVB</ta>
            <ta e="T410" id="Seg_8241" s="T869">исчезнуть-PST.[3SG]</ta>
            <ta e="T411" id="Seg_8242" s="T410">NEG</ta>
            <ta e="T412" id="Seg_8243" s="T411">знать-1SG</ta>
            <ta e="T413" id="Seg_8244" s="T412">Эля.[NOM.SG]</ta>
            <ta e="T414" id="Seg_8245" s="T413">%%</ta>
            <ta e="T415" id="Seg_8246" s="T414">%%</ta>
            <ta e="T416" id="Seg_8247" s="T415">PTCL</ta>
            <ta e="T417" id="Seg_8248" s="T416">лодка-LAT</ta>
            <ta e="T418" id="Seg_8249" s="T417">река-LAT</ta>
            <ta e="T419" id="Seg_8250" s="T418">пойти-PST-3PL</ta>
            <ta e="T420" id="Seg_8251" s="T419">тогда</ta>
            <ta e="T421" id="Seg_8252" s="T420">какой=INDEF</ta>
            <ta e="T422" id="Seg_8253" s="T421">дочь-COM</ta>
            <ta e="T423" id="Seg_8254" s="T422">хотеть.PST.PL</ta>
            <ta e="T424" id="Seg_8255" s="T423">река-LOC</ta>
            <ta e="T426" id="Seg_8256" s="T425">а</ta>
            <ta e="T427" id="Seg_8257" s="T426">река.[NOM.SG]</ta>
            <ta e="T428" id="Seg_8258" s="T427">очень</ta>
            <ta e="T429" id="Seg_8259" s="T428">большой.[NOM.SG]</ta>
            <ta e="T430" id="Seg_8260" s="T429">этот-PL</ta>
            <ta e="T431" id="Seg_8261" s="T430">PTCL</ta>
            <ta e="T432" id="Seg_8262" s="T431">хотеть.PST.PL</ta>
            <ta e="T433" id="Seg_8263" s="T432">лодка-LAT</ta>
            <ta e="T434" id="Seg_8264" s="T433">влезать-INF.LAT</ta>
            <ta e="T435" id="Seg_8265" s="T434">и</ta>
            <ta e="T436" id="Seg_8266" s="T435">PTCL</ta>
            <ta e="T437" id="Seg_8267" s="T436">пугать-MOM-PST-3PL</ta>
            <ta e="T439" id="Seg_8268" s="T437">тогда</ta>
            <ta e="T440" id="Seg_8269" s="T439">тогда</ta>
            <ta e="T441" id="Seg_8270" s="T440">там</ta>
            <ta e="T442" id="Seg_8271" s="T441">пугать-MOM-PST-3PL</ta>
            <ta e="T443" id="Seg_8272" s="T442">собирать-PST.[3SG]</ta>
            <ta e="T445" id="Seg_8273" s="T444">видеть-PRS-3SG.O</ta>
            <ta e="T446" id="Seg_8274" s="T445">что</ta>
            <ta e="T447" id="Seg_8275" s="T446">%%</ta>
            <ta e="T448" id="Seg_8276" s="T447">умереть-PST.[3SG]</ta>
            <ta e="T450" id="Seg_8277" s="T449">Митрофан-EP-GEN</ta>
            <ta e="T451" id="Seg_8278" s="T450">дом-NOM/GEN.3SG</ta>
            <ta e="T452" id="Seg_8279" s="T451">еще</ta>
            <ta e="T453" id="Seg_8280" s="T452">стоять-PRS.[3SG]</ta>
            <ta e="T454" id="Seg_8281" s="T453">и</ta>
            <ta e="T456" id="Seg_8282" s="T455">и</ta>
            <ta e="T457" id="Seg_8283" s="T456">%%</ta>
            <ta e="T459" id="Seg_8284" s="T458">дом.[NOM.SG]</ta>
            <ta e="T460" id="Seg_8285" s="T459">еще</ta>
            <ta e="T461" id="Seg_8286" s="T460">стоять-PRS.[3SG]</ta>
            <ta e="T466" id="Seg_8287" s="T465">дом.[NOM.SG]</ta>
            <ta e="T467" id="Seg_8288" s="T466">еще</ta>
            <ta e="T468" id="Seg_8289" s="T467">стоять-PRS.[3SG]</ta>
            <ta e="T471" id="Seg_8290" s="T470">дом.[NOM.SG]</ta>
            <ta e="T472" id="Seg_8291" s="T471">еще</ta>
            <ta e="T473" id="Seg_8292" s="T472">стоять-PRS.[3SG]</ta>
            <ta e="T476" id="Seg_8293" s="T475">дом.[NOM.SG]</ta>
            <ta e="T477" id="Seg_8294" s="T476">еще</ta>
            <ta e="T478" id="Seg_8295" s="T477">стоять-PRS.[3SG]</ta>
            <ta e="T479" id="Seg_8296" s="T478">и</ta>
            <ta e="T481" id="Seg_8297" s="T480">Влас-LAT</ta>
            <ta e="T482" id="Seg_8298" s="T481">еще</ta>
            <ta e="T483" id="Seg_8299" s="T482">дом.[NOM.SG]</ta>
            <ta e="T484" id="Seg_8300" s="T483">стоять-PRS.[3SG]</ta>
            <ta e="T485" id="Seg_8301" s="T484">и</ta>
            <ta e="T489" id="Seg_8302" s="T488">еще</ta>
            <ta e="T490" id="Seg_8303" s="T489">дом.[NOM.SG]</ta>
            <ta e="T491" id="Seg_8304" s="T490">стоять-PRS.[3SG]</ta>
            <ta e="T492" id="Seg_8305" s="T491">когда</ta>
            <ta e="T494" id="Seg_8306" s="T493">прийти-PST.[3SG]</ta>
            <ta e="T495" id="Seg_8307" s="T494">говорить-INF.LAT</ta>
            <ta e="T496" id="Seg_8308" s="T495">здесь</ta>
            <ta e="T497" id="Seg_8309" s="T496">здесь</ta>
            <ta e="T499" id="Seg_8310" s="T498">пять.[NOM.SG]</ta>
            <ta e="T500" id="Seg_8311" s="T499">десять.[NOM.SG]</ta>
            <ta e="T501" id="Seg_8312" s="T500">дом.[NOM.SG]</ta>
            <ta e="T502" id="Seg_8313" s="T501">быть-PST.[3SG]</ta>
            <ta e="T506" id="Seg_8314" s="T505">умереть-RES-PST.[3SG]</ta>
            <ta e="T507" id="Seg_8315" s="T506">этот.[NOM.SG]</ta>
            <ta e="T508" id="Seg_8316" s="T507">пойти-PST.[3SG]</ta>
            <ta e="T509" id="Seg_8317" s="T508">Пермяково-LAT</ta>
            <ta e="T510" id="Seg_8318" s="T509">там</ta>
            <ta e="T511" id="Seg_8319" s="T510">водка.[NOM.SG]</ta>
            <ta e="T512" id="Seg_8320" s="T511">пить-PST.[3SG]</ta>
            <ta e="T513" id="Seg_8321" s="T512">там</ta>
            <ta e="T515" id="Seg_8322" s="T514">быть-PST.[3SG]</ta>
            <ta e="T516" id="Seg_8323" s="T515">один.[NOM.SG]</ta>
            <ta e="T518" id="Seg_8324" s="T517">и</ta>
            <ta e="T519" id="Seg_8325" s="T518">женщина-3SG-COM</ta>
            <ta e="T520" id="Seg_8326" s="T519">прийти-PST.[3SG]</ta>
            <ta e="T521" id="Seg_8327" s="T520">тогда</ta>
            <ta e="T522" id="Seg_8328" s="T521">пойти-PST.[3SG]</ta>
            <ta e="T523" id="Seg_8329" s="T522">крест-PL-LAT</ta>
            <ta e="T524" id="Seg_8330" s="T523">тогда</ta>
            <ta e="T525" id="Seg_8331" s="T524">лить-PRS</ta>
            <ta e="T526" id="Seg_8332" s="T525">и</ta>
            <ta e="T527" id="Seg_8333" s="T526">ругать-DES-PRS.[3SG]</ta>
            <ta e="T528" id="Seg_8334" s="T527">пить-EP-IMP.2SG</ta>
            <ta e="T529" id="Seg_8335" s="T528">женщина-NOM/GEN/ACC.1SG</ta>
            <ta e="T530" id="Seg_8336" s="T529">пить-EP-IMP.2SG</ta>
            <ta e="T531" id="Seg_8337" s="T530">женщина-NOM/GEN/ACC.1SG</ta>
            <ta e="T532" id="Seg_8338" s="T531">а</ta>
            <ta e="T533" id="Seg_8339" s="T532">я.NOM</ta>
            <ta e="T534" id="Seg_8340" s="T533">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T535" id="Seg_8341" s="T534">стоять-PRS.[3SG]</ta>
            <ta e="T536" id="Seg_8342" s="T535">и</ta>
            <ta e="T537" id="Seg_8343" s="T536">смотреть-DUR.[3SG]</ta>
            <ta e="T538" id="Seg_8344" s="T537">и</ta>
            <ta e="T539" id="Seg_8345" s="T538">смеяться-DUR.[3SG]</ta>
            <ta e="T540" id="Seg_8346" s="T539">я.NOM</ta>
            <ta e="T541" id="Seg_8347" s="T540">трава.[NOM.SG]</ta>
            <ta e="T542" id="Seg_8348" s="T541">резать-PST-1SG</ta>
            <ta e="T543" id="Seg_8349" s="T542">тогда</ta>
            <ta e="T544" id="Seg_8350" s="T543">собирать-PST-1SG</ta>
            <ta e="T545" id="Seg_8351" s="T544">тогда</ta>
            <ta e="T546" id="Seg_8352" s="T545">стог-NOM/GEN/ACC.3SG</ta>
            <ta e="T547" id="Seg_8353" s="T546">класть-PST-1SG</ta>
            <ta e="T548" id="Seg_8354" s="T547">этот.[NOM.SG]</ta>
            <ta e="T549" id="Seg_8355" s="T548">влажный.[NOM.SG]</ta>
            <ta e="T550" id="Seg_8356" s="T549">быть-PST.[3SG]</ta>
            <ta e="T551" id="Seg_8357" s="T550">тогда</ta>
            <ta e="T552" id="Seg_8358" s="T551">три.[NOM.SG]</ta>
            <ta e="T554" id="Seg_8359" s="T553">день.[NOM.SG]</ta>
            <ta e="T555" id="Seg_8360" s="T554">NEG</ta>
            <ta e="T556" id="Seg_8361" s="T555">пойти-PST-1SG</ta>
            <ta e="T557" id="Seg_8362" s="T556">очень</ta>
            <ta e="T558" id="Seg_8363" s="T557">теплый.[NOM.SG]</ta>
            <ta e="T559" id="Seg_8364" s="T558">стать-RES-PST.[3SG]</ta>
            <ta e="T562" id="Seg_8365" s="T561">я.NOM</ta>
            <ta e="T563" id="Seg_8366" s="T562">два.[NOM.SG]</ta>
            <ta e="T564" id="Seg_8367" s="T563">женщина.[NOM.SG]</ta>
            <ta e="T565" id="Seg_8368" s="T564">взять-PST-1SG</ta>
            <ta e="T566" id="Seg_8369" s="T565">пойти-PST-1SG</ta>
            <ta e="T567" id="Seg_8370" s="T566">PTCL</ta>
            <ta e="T568" id="Seg_8371" s="T567">высохнуть-PST.[3SG]</ta>
            <ta e="T569" id="Seg_8372" s="T568">и</ta>
            <ta e="T570" id="Seg_8373" s="T569">опять</ta>
            <ta e="T571" id="Seg_8374" s="T570">класть-PST-1PL</ta>
            <ta e="T572" id="Seg_8375" s="T571">красивый.[NOM.SG]</ta>
            <ta e="T574" id="Seg_8376" s="T573">стог.[NOM.SG]</ta>
            <ta e="T575" id="Seg_8377" s="T574">я.NOM</ta>
            <ta e="T576" id="Seg_8378" s="T575">жить-PST-1SG</ta>
            <ta e="T577" id="Seg_8379" s="T576">два.[NOM.SG]</ta>
            <ta e="T578" id="Seg_8380" s="T577">десять.[NOM.SG]</ta>
            <ta e="T579" id="Seg_8381" s="T578">год.[NOM.SG]</ta>
            <ta e="T580" id="Seg_8382" s="T579">сам-NOM/GEN/ACC.1SG</ta>
            <ta e="T581" id="Seg_8383" s="T580">трава.[NOM.SG]</ta>
            <ta e="T582" id="Seg_8384" s="T581">резать-PST-1SG</ta>
            <ta e="T583" id="Seg_8385" s="T582">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T584" id="Seg_8386" s="T583">собирать-PST-1SG</ta>
            <ta e="T585" id="Seg_8387" s="T584">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T586" id="Seg_8388" s="T585">класть-PST-1SG</ta>
            <ta e="T587" id="Seg_8389" s="T586">лошадь-INS</ta>
            <ta e="T588" id="Seg_8390" s="T587">дом-LAT/LOC.1SG</ta>
            <ta e="T589" id="Seg_8391" s="T588">носить-PST-1SG</ta>
            <ta e="T590" id="Seg_8392" s="T589">PTCL</ta>
            <ta e="T591" id="Seg_8393" s="T590">корова-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T594" id="Seg_8394" s="T593">кормить-DUR-PST-1SG</ta>
            <ta e="T595" id="Seg_8395" s="T594">один.[NOM.SG]</ta>
            <ta e="T596" id="Seg_8396" s="T595">лошадь.[NOM.SG]</ta>
            <ta e="T597" id="Seg_8397" s="T596">быть-PST-1SG</ta>
            <ta e="T598" id="Seg_8398" s="T597">тогда</ta>
            <ta e="T599" id="Seg_8399" s="T598">племянница-NOM/GEN.3SG</ta>
            <ta e="T600" id="Seg_8400" s="T599">мальчик-GEN</ta>
            <ta e="T601" id="Seg_8401" s="T600">взять-PST-1SG</ta>
            <ta e="T602" id="Seg_8402" s="T601">и</ta>
            <ta e="T603" id="Seg_8403" s="T602">еще</ta>
            <ta e="T605" id="Seg_8404" s="T604">пойти-PST-1PL</ta>
            <ta e="T606" id="Seg_8405" s="T605">трава.[NOM.SG]</ta>
            <ta e="T607" id="Seg_8406" s="T606">класть-PST-1PL</ta>
            <ta e="T608" id="Seg_8407" s="T607">тогда</ta>
            <ta e="T609" id="Seg_8408" s="T608">прийти-PST-1PL</ta>
            <ta e="T610" id="Seg_8409" s="T609">прийти-PST-1PL</ta>
            <ta e="T611" id="Seg_8410" s="T610">мы.NOM</ta>
            <ta e="T612" id="Seg_8411" s="T611">трава.[NOM.SG]</ta>
            <ta e="T867" id="Seg_8412" s="T613">спуститься-PST.[3SG] </ta>
            <ta e="T615" id="Seg_8413" s="T614">тогда</ta>
            <ta e="T616" id="Seg_8414" s="T615">поднимать-PST-1PL</ta>
            <ta e="T617" id="Seg_8415" s="T616">поднимать-PST-1PL</ta>
            <ta e="T618" id="Seg_8416" s="T617">NEG</ta>
            <ta e="T619" id="Seg_8417" s="T618">мочь-PRS-1PL</ta>
            <ta e="T620" id="Seg_8418" s="T619">этот</ta>
            <ta e="T621" id="Seg_8419" s="T620">сказать-IPFVZ.[3SG]</ta>
            <ta e="T622" id="Seg_8420" s="T621">скоро</ta>
            <ta e="T623" id="Seg_8421" s="T622">лошадь.[NOM.SG]</ta>
            <ta e="T626" id="Seg_8422" s="T625">запрячь-PST-1SG</ta>
            <ta e="T627" id="Seg_8423" s="T626">тогда</ta>
            <ta e="T628" id="Seg_8424" s="T627">поднимать-FUT-1SG</ta>
            <ta e="T629" id="Seg_8425" s="T628">а</ta>
            <ta e="T630" id="Seg_8426" s="T629">как</ta>
            <ta e="T631" id="Seg_8427" s="T630">ты.NOM</ta>
            <ta e="T632" id="Seg_8428" s="T631">поднимать-FUT-2SG</ta>
            <ta e="T633" id="Seg_8429" s="T632">а</ta>
            <ta e="T634" id="Seg_8430" s="T633">так</ta>
            <ta e="T635" id="Seg_8431" s="T634">PTCL</ta>
            <ta e="T636" id="Seg_8432" s="T866">выбросить-PST.[3SG]</ta>
            <ta e="T637" id="Seg_8433" s="T636">%%-PL</ta>
            <ta e="T638" id="Seg_8434" s="T637">и</ta>
            <ta e="T639" id="Seg_8435" s="T638">тогда</ta>
            <ta e="T640" id="Seg_8436" s="T639">лошадь.[NOM.SG]</ta>
            <ta e="T643" id="Seg_8437" s="T642">тогда</ta>
            <ta e="T644" id="Seg_8438" s="T643">дом.[NOM.SG]</ta>
            <ta e="T646" id="Seg_8439" s="T645">прийти-PST-1PL</ta>
            <ta e="T647" id="Seg_8440" s="T646">идти-PST-1PL</ta>
            <ta e="T648" id="Seg_8441" s="T647">трава.[NOM.SG]</ta>
            <ta e="T649" id="Seg_8442" s="T648">тогда</ta>
            <ta e="T650" id="Seg_8443" s="T649">лошадь-ACC</ta>
            <ta e="T652" id="Seg_8444" s="T651">я.NOM</ta>
            <ta e="T653" id="Seg_8445" s="T652">очень</ta>
            <ta e="T654" id="Seg_8446" s="T653">мерзнуть-PST-1SG</ta>
            <ta e="T655" id="Seg_8447" s="T654">тогда</ta>
            <ta e="T656" id="Seg_8448" s="T655">кашель</ta>
            <ta e="T657" id="Seg_8449" s="T656">прийти-PST.[3SG]</ta>
            <ta e="T658" id="Seg_8450" s="T657">тогда</ta>
            <ta e="T659" id="Seg_8451" s="T658">я.NOM</ta>
            <ta e="T660" id="Seg_8452" s="T659">сахар.[NOM.SG]</ta>
            <ta e="T661" id="Seg_8453" s="T660">PTCL</ta>
            <ta e="T662" id="Seg_8454" s="T661">жечь-PST-1SG</ta>
            <ta e="T663" id="Seg_8455" s="T662">водка-LAT</ta>
            <ta e="T664" id="Seg_8456" s="T663">тогда</ta>
            <ta e="T665" id="Seg_8457" s="T664">пить-PST-1SG</ta>
            <ta e="T666" id="Seg_8458" s="T665">и</ta>
            <ta e="T667" id="Seg_8459" s="T666">куда=INDEF</ta>
            <ta e="T668" id="Seg_8460" s="T667">пойти-FUT-3SG</ta>
            <ta e="T669" id="Seg_8461" s="T668">кашель-NOM/GEN/ACC.1SG</ta>
            <ta e="T673" id="Seg_8462" s="T672">какой</ta>
            <ta e="T674" id="Seg_8463" s="T673">трава.[NOM.SG]</ta>
            <ta e="T676" id="Seg_8464" s="T675">трава.[NOM.SG]</ta>
            <ta e="T677" id="Seg_8465" s="T676">быть-PRS.[3SG]</ta>
            <ta e="T679" id="Seg_8466" s="T678">этот.[NOM.SG]</ta>
            <ta e="T680" id="Seg_8467" s="T679">стебель-LAT</ta>
            <ta e="T681" id="Seg_8468" s="T680">взять-INF.LAT</ta>
            <ta e="T682" id="Seg_8469" s="T681">надо</ta>
            <ta e="T683" id="Seg_8470" s="T682">тянуть-INF.LAT</ta>
            <ta e="T684" id="Seg_8471" s="T683">этот-ACC</ta>
            <ta e="T685" id="Seg_8472" s="T684">%%</ta>
            <ta e="T686" id="Seg_8473" s="T685">дым.[NOM.SG]</ta>
            <ta e="T687" id="Seg_8474" s="T686">сидеть-INF.LAT</ta>
            <ta e="T688" id="Seg_8475" s="T687">тогда</ta>
            <ta e="T689" id="Seg_8476" s="T688">NEG</ta>
            <ta e="T690" id="Seg_8477" s="T689">стать-FUT-3SG</ta>
            <ta e="T691" id="Seg_8478" s="T690">кашель.[NOM.SG]</ta>
            <ta e="T692" id="Seg_8479" s="T691">гореть-MOM-PST.[3SG]</ta>
            <ta e="T693" id="Seg_8480" s="T692">и</ta>
            <ta e="T694" id="Seg_8481" s="T693">дым.[NOM.SG]</ta>
            <ta e="T695" id="Seg_8482" s="T694">NEG.EX.[3SG]</ta>
            <ta e="T696" id="Seg_8483" s="T695">и</ta>
            <ta e="T697" id="Seg_8484" s="T696">теплый.[NOM.SG]</ta>
            <ta e="T698" id="Seg_8485" s="T697">NEG.EX.[3SG]</ta>
            <ta e="T699" id="Seg_8486" s="T698">дерево-PL</ta>
            <ta e="T700" id="Seg_8487" s="T699">я.LAT</ta>
            <ta e="T701" id="Seg_8488" s="T700">сегодня</ta>
            <ta e="T702" id="Seg_8489" s="T701">NEG</ta>
            <ta e="T703" id="Seg_8490" s="T702">ударить-MULT-INF.LAT</ta>
            <ta e="T704" id="Seg_8491" s="T703">хватит</ta>
            <ta e="T706" id="Seg_8492" s="T705">тот.[NOM.SG]</ta>
            <ta e="T707" id="Seg_8493" s="T706">вечер-LAT</ta>
            <ta e="T708" id="Seg_8494" s="T707">дерево-PL</ta>
            <ta e="T709" id="Seg_8495" s="T708">я.NOM</ta>
            <ta e="T710" id="Seg_8496" s="T709">много</ta>
            <ta e="T711" id="Seg_8497" s="T710">тот.[NOM.SG]</ta>
            <ta e="T712" id="Seg_8498" s="T711">год-LAT</ta>
            <ta e="T713" id="Seg_8499" s="T712">еще</ta>
            <ta e="T714" id="Seg_8500" s="T713">хватать-FUT-3SG</ta>
            <ta e="T715" id="Seg_8501" s="T714">холодный.[NOM.SG]</ta>
            <ta e="T716" id="Seg_8502" s="T715">очень</ta>
            <ta e="T717" id="Seg_8503" s="T716">много</ta>
            <ta e="T718" id="Seg_8504" s="T717">дерево.[NOM.SG]</ta>
            <ta e="T719" id="Seg_8505" s="T718">пойти-IPFVZ-PRS-3PL</ta>
            <ta e="T720" id="Seg_8506" s="T719">дерево-PL</ta>
            <ta e="T721" id="Seg_8507" s="T720">PTCL</ta>
            <ta e="T722" id="Seg_8508" s="T721">дождь-ABL</ta>
            <ta e="T723" id="Seg_8509" s="T722">черный.[NOM.SG]</ta>
            <ta e="T724" id="Seg_8510" s="T723">стать-PST-3PL</ta>
            <ta e="T725" id="Seg_8511" s="T724">а.то</ta>
            <ta e="T726" id="Seg_8512" s="T725">снег.[NOM.SG]</ta>
            <ta e="T727" id="Seg_8513" s="T726">быть-PST-3PL</ta>
            <ta e="T728" id="Seg_8514" s="T727">я.NOM</ta>
            <ta e="T729" id="Seg_8515" s="T728">%%</ta>
            <ta e="T730" id="Seg_8516" s="T729">лиственница.[NOM.SG]</ta>
            <ta e="T731" id="Seg_8517" s="T730">дерево-PL</ta>
            <ta e="T732" id="Seg_8518" s="T731">снег.[NOM.SG]</ta>
            <ta e="T733" id="Seg_8519" s="T732">дерево-PL</ta>
            <ta e="T734" id="Seg_8520" s="T733">PTCL</ta>
            <ta e="T735" id="Seg_8521" s="T734">%%</ta>
            <ta e="T736" id="Seg_8522" s="T735">этот-PL</ta>
            <ta e="T737" id="Seg_8523" s="T736">очень</ta>
            <ta e="T738" id="Seg_8524" s="T737">хороший</ta>
            <ta e="T739" id="Seg_8525" s="T738">сильно</ta>
            <ta e="T740" id="Seg_8526" s="T739">гореть-DUR-3PL</ta>
            <ta e="T741" id="Seg_8527" s="T740">и</ta>
            <ta e="T742" id="Seg_8528" s="T741">теплый.[NOM.SG]</ta>
            <ta e="T743" id="Seg_8529" s="T742">этот-PL</ta>
            <ta e="T744" id="Seg_8530" s="T743">%%</ta>
            <ta e="T745" id="Seg_8531" s="T744">поставить-PRS-2SG</ta>
            <ta e="T746" id="Seg_8532" s="T745">варить-INF.LAT</ta>
            <ta e="T748" id="Seg_8533" s="T747">уголь.[NOM.SG]</ta>
            <ta e="T749" id="Seg_8534" s="T748">NEG</ta>
            <ta e="T750" id="Seg_8535" s="T749">NEG</ta>
            <ta e="T751" id="Seg_8536" s="T750">ползти-PRS.[3SG]</ta>
            <ta e="T752" id="Seg_8537" s="T751">там</ta>
            <ta e="T754" id="Seg_8538" s="T753">очень</ta>
            <ta e="T755" id="Seg_8539" s="T754">уголь.[NOM.SG]</ta>
            <ta e="T756" id="Seg_8540" s="T755">много</ta>
            <ta e="T757" id="Seg_8541" s="T756">выбросить-PRS.[3SG]</ta>
            <ta e="T758" id="Seg_8542" s="T757">%%</ta>
            <ta e="T759" id="Seg_8543" s="T758">я.LAT</ta>
            <ta e="T762" id="Seg_8544" s="T761">%%</ta>
            <ta e="T763" id="Seg_8545" s="T762">тоже</ta>
            <ta e="T765" id="Seg_8546" s="T764">и</ta>
            <ta e="T766" id="Seg_8547" s="T765">%%-PST</ta>
            <ta e="T767" id="Seg_8548" s="T766">этот.[NOM.SG]</ta>
            <ta e="T768" id="Seg_8549" s="T767">а</ta>
            <ta e="T769" id="Seg_8550" s="T768">я.NOM</ta>
            <ta e="T770" id="Seg_8551" s="T769">сам-NOM/GEN/ACC.1SG</ta>
            <ta e="T771" id="Seg_8552" s="T770">класть-PST-1SG</ta>
            <ta e="T772" id="Seg_8553" s="T771">трактор-INS</ta>
            <ta e="T773" id="Seg_8554" s="T772">принести-PST-3PL</ta>
            <ta e="T774" id="Seg_8555" s="T773">масло.[NOM.SG]</ta>
            <ta e="T775" id="Seg_8556" s="T774">взять-PST-1SG</ta>
            <ta e="T776" id="Seg_8557" s="T775">трактор-LAT</ta>
            <ta e="T777" id="Seg_8558" s="T776">сильно</ta>
            <ta e="T779" id="Seg_8559" s="T778">а</ta>
            <ta e="T780" id="Seg_8560" s="T779">я.NOM</ta>
            <ta e="T781" id="Seg_8561" s="T780">PTCL</ta>
            <ta e="T782" id="Seg_8562" s="T781">дерево.[NOM.SG]</ta>
            <ta e="T783" id="Seg_8563" s="T782">носить-PST-1SG</ta>
            <ta e="T784" id="Seg_8564" s="T783">вода-VBLZ-CVB</ta>
            <ta e="T785" id="Seg_8565" s="T784">идти-PST-1SG</ta>
            <ta e="T786" id="Seg_8566" s="T785">корова-LAT</ta>
            <ta e="T787" id="Seg_8567" s="T786">стоять-IMP.2SG</ta>
            <ta e="T788" id="Seg_8568" s="T787">дать-PST-1SG</ta>
            <ta e="T789" id="Seg_8569" s="T788">доить-PST-1SG</ta>
            <ta e="T790" id="Seg_8570" s="T789">тогда</ta>
            <ta e="T791" id="Seg_8571" s="T790">сидеть-PST-1SG</ta>
            <ta e="T792" id="Seg_8572" s="T791">есть-INF.LAT</ta>
            <ta e="T793" id="Seg_8573" s="T792">и</ta>
            <ta e="T794" id="Seg_8574" s="T793">думать-DUR-1SG</ta>
            <ta e="T795" id="Seg_8575" s="T794">этот.[NOM.SG]</ta>
            <ta e="T796" id="Seg_8576" s="T795">Эля.[NOM.SG]</ta>
            <ta e="T797" id="Seg_8577" s="T796">я.LAT</ta>
            <ta e="T798" id="Seg_8578" s="T797">PTCL</ta>
            <ta e="T799" id="Seg_8579" s="T798">лгать-PST.[3SG]</ta>
            <ta e="T800" id="Seg_8580" s="T799">деньги-NOM/GEN.3SG</ta>
            <ta e="T801" id="Seg_8581" s="T800">три.[NOM.SG]</ta>
            <ta e="T802" id="Seg_8582" s="T801">рубль.[NOM.SG]</ta>
            <ta e="T804" id="Seg_8583" s="T803">быть-PST.[3SG]</ta>
            <ta e="T805" id="Seg_8584" s="T804">а</ta>
            <ta e="T806" id="Seg_8585" s="T805">я.LAT</ta>
            <ta e="T807" id="Seg_8586" s="T806">быть-PST.[3SG]</ta>
            <ta e="T808" id="Seg_8587" s="T807">один.[NOM.SG]</ta>
            <ta e="T809" id="Seg_8588" s="T808">рубль.[NOM.SG]</ta>
            <ta e="T810" id="Seg_8589" s="T809">два.[NOM.SG]</ta>
            <ta e="T811" id="Seg_8590" s="T810">десять.[NOM.SG]</ta>
            <ta e="T812" id="Seg_8591" s="T811">пять.[NOM.SG]</ta>
            <ta e="T813" id="Seg_8592" s="T812">этот-LAT</ta>
            <ta e="T815" id="Seg_8593" s="T814">взять-INF.LAT</ta>
            <ta e="T816" id="Seg_8594" s="T815">я.LAT</ta>
            <ta e="T817" id="Seg_8595" s="T816">один.[NOM.SG]</ta>
            <ta e="T818" id="Seg_8596" s="T817">рубль.[NOM.SG]</ta>
            <ta e="T819" id="Seg_8597" s="T818">и</ta>
            <ta e="T820" id="Seg_8598" s="T819">десять.[NOM.SG]</ta>
            <ta e="T821" id="Seg_8599" s="T820">пять.[NOM.SG]</ta>
            <ta e="T822" id="Seg_8600" s="T821">копейка-NOM/GEN/ACC.3PL</ta>
            <ta e="T823" id="Seg_8601" s="T822">я.NOM</ta>
            <ta e="T824" id="Seg_8602" s="T823">сегодня</ta>
            <ta e="T825" id="Seg_8603" s="T824">вода-LAT</ta>
            <ta e="T826" id="Seg_8604" s="T825">пойти-PST-1SG</ta>
            <ta e="T827" id="Seg_8605" s="T826">и</ta>
            <ta e="T828" id="Seg_8606" s="T827">очень</ta>
            <ta e="T829" id="Seg_8607" s="T828">сильно</ta>
            <ta e="T830" id="Seg_8608" s="T829">замерзнуть-RES-PST.[3SG]</ta>
            <ta e="T831" id="Seg_8609" s="T830">я.NOM</ta>
            <ta e="T832" id="Seg_8610" s="T831">вернуться-PST-1SG</ta>
            <ta e="T833" id="Seg_8611" s="T832">топор.[NOM.SG]</ta>
            <ta e="T834" id="Seg_8612" s="T833">взять-PST-1SG</ta>
            <ta e="T835" id="Seg_8613" s="T834">топор-INS</ta>
            <ta e="T836" id="Seg_8614" s="T835">резать-PST-1SG</ta>
            <ta e="T837" id="Seg_8615" s="T836">резать-PST-1SG</ta>
            <ta e="T838" id="Seg_8616" s="T837">PTCL</ta>
            <ta e="T839" id="Seg_8617" s="T838">снег.[NOM.SG]</ta>
            <ta e="T842" id="Seg_8618" s="T841">замерзнуть-RES-PST.[3SG]</ta>
            <ta e="T843" id="Seg_8619" s="T842">очень</ta>
            <ta e="T844" id="Seg_8620" s="T843">холодный.[NOM.SG]</ta>
            <ta e="T845" id="Seg_8621" s="T844">утро.[NOM.SG]</ta>
            <ta e="T846" id="Seg_8622" s="T845">быть-PST.[3SG]</ta>
            <ta e="T847" id="Seg_8623" s="T846">дом-LAT/LOC.1SG</ta>
            <ta e="T848" id="Seg_8624" s="T847">прийти-PST-1SG</ta>
            <ta e="T849" id="Seg_8625" s="T848">вы.ACC</ta>
            <ta e="T850" id="Seg_8626" s="T849">ждать-PST-1SG</ta>
            <ta e="T851" id="Seg_8627" s="T850">и</ta>
            <ta e="T852" id="Seg_8628" s="T851">вода.[NOM.SG]</ta>
            <ta e="T853" id="Seg_8629" s="T852">принести-PST-1SG</ta>
            <ta e="T854" id="Seg_8630" s="T853">печь-LAT</ta>
            <ta e="T858" id="Seg_8631" s="T857">влезать-PST-1SG</ta>
            <ta e="T859" id="Seg_8632" s="T858">сидеть-PST-1SG</ta>
            <ta e="T860" id="Seg_8633" s="T859">и</ta>
            <ta e="T861" id="Seg_8634" s="T860">%%</ta>
            <ta e="T862" id="Seg_8635" s="T861">тогда</ta>
            <ta e="T863" id="Seg_8636" s="T862">прийти-PST-3PL</ta>
            <ta e="T864" id="Seg_8637" s="T863">и</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_8638" s="T0">pers</ta>
            <ta e="T2" id="Seg_8639" s="T1">ptcl</ta>
            <ta e="T3" id="Seg_8640" s="T2">propr-n:case</ta>
            <ta e="T4" id="Seg_8641" s="T3">v-any</ta>
            <ta e="T7" id="Seg_8642" s="T6">n-n&gt;adj</ta>
            <ta e="T8" id="Seg_8643" s="T7">n-n&gt;adj</ta>
            <ta e="T9" id="Seg_8644" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_8645" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_8646" s="T10">adv</ta>
            <ta e="T12" id="Seg_8647" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_8648" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_8649" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_8650" s="T14">v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_8651" s="T16">v-v:n.fin</ta>
            <ta e="T18" id="Seg_8652" s="T17">n-n&gt;adj-n:case</ta>
            <ta e="T19" id="Seg_8653" s="T18">ptcl</ta>
            <ta e="T22" id="Seg_8654" s="T21">v-v&gt;v-v:pn</ta>
            <ta e="T23" id="Seg_8655" s="T22">adv</ta>
            <ta e="T24" id="Seg_8656" s="T23">v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_8657" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_8658" s="T25">adv</ta>
            <ta e="T27" id="Seg_8659" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_8660" s="T27">adv</ta>
            <ta e="T29" id="Seg_8661" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_8662" s="T29">num-n:case</ta>
            <ta e="T31" id="Seg_8663" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_8664" s="T31">v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_8665" s="T32">adv</ta>
            <ta e="T34" id="Seg_8666" s="T33">v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_8667" s="T34">n-n&gt;adj</ta>
            <ta e="T36" id="Seg_8668" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_8669" s="T36">v-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_8670" s="T37">propr-n:case</ta>
            <ta e="T39" id="Seg_8671" s="T38">adv</ta>
            <ta e="T40" id="Seg_8672" s="T39">propr-n:case</ta>
            <ta e="T41" id="Seg_8673" s="T40">v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_8674" s="T42">v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_8675" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_8676" s="T44">v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_8677" s="T45">propr-n:case</ta>
            <ta e="T47" id="Seg_8678" s="T46">adv</ta>
            <ta e="T48" id="Seg_8679" s="T47">v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_8680" s="T50">num-n:case</ta>
            <ta e="T52" id="Seg_8681" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_8682" s="T52">adv</ta>
            <ta e="T54" id="Seg_8683" s="T53">propr-n:case</ta>
            <ta e="T55" id="Seg_8684" s="T54">v-v:tense-v:pn</ta>
            <ta e="T56" id="Seg_8685" s="T55">conj</ta>
            <ta e="T57" id="Seg_8686" s="T56">pers</ta>
            <ta e="T58" id="Seg_8687" s="T57">v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_8688" s="T58">pers</ta>
            <ta e="T60" id="Seg_8689" s="T59">v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_8690" s="T60">v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_8691" s="T61">adv</ta>
            <ta e="T63" id="Seg_8692" s="T62">pers</ta>
            <ta e="T64" id="Seg_8693" s="T63">pers</ta>
            <ta e="T65" id="Seg_8694" s="T64">pers</ta>
            <ta e="T67" id="Seg_8695" s="T66">v-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_8696" s="T67">v-v:mood.pn</ta>
            <ta e="T69" id="Seg_8697" s="T68">v-v:mood.pn</ta>
            <ta e="T71" id="Seg_8698" s="T70">v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_8699" s="T71">ptcl=ptcl</ta>
            <ta e="T73" id="Seg_8700" s="T72">adv</ta>
            <ta e="T74" id="Seg_8701" s="T73">pers</ta>
            <ta e="T75" id="Seg_8702" s="T74">v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_8703" s="T75">v-v:tense-v:pn</ta>
            <ta e="T77" id="Seg_8704" s="T76">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_8705" s="T77">refl-n:case.poss</ta>
            <ta e="T79" id="Seg_8706" s="T78">n-n:case.poss-n:case</ta>
            <ta e="T80" id="Seg_8707" s="T79">num-n:case</ta>
            <ta e="T81" id="Seg_8708" s="T80">num-n:case</ta>
            <ta e="T82" id="Seg_8709" s="T81">n-n:case</ta>
            <ta e="T83" id="Seg_8710" s="T82">pers</ta>
            <ta e="T84" id="Seg_8711" s="T83">v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_8712" s="T84">num-n:case</ta>
            <ta e="T86" id="Seg_8713" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_8714" s="T86">propr-n:case</ta>
            <ta e="T88" id="Seg_8715" s="T87">adv</ta>
            <ta e="T89" id="Seg_8716" s="T88">adv</ta>
            <ta e="T90" id="Seg_8717" s="T89">n-n&gt;adj</ta>
            <ta e="T91" id="Seg_8718" s="T90">n-n:case</ta>
            <ta e="T93" id="Seg_8719" s="T92">v-v:tense-v:pn</ta>
            <ta e="T94" id="Seg_8720" s="T93">v-v:tense-v:pn</ta>
            <ta e="T95" id="Seg_8721" s="T94">propr-n:case</ta>
            <ta e="T96" id="Seg_8722" s="T95">adv</ta>
            <ta e="T97" id="Seg_8723" s="T96">n-n:case</ta>
            <ta e="T98" id="Seg_8724" s="T97">v-v:n.fin</ta>
            <ta e="T99" id="Seg_8725" s="T98">adv</ta>
            <ta e="T100" id="Seg_8726" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_8727" s="T100">v-v:n.fin-v:tense-v:pn</ta>
            <ta e="T102" id="Seg_8728" s="T101">adv</ta>
            <ta e="T103" id="Seg_8729" s="T102">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T104" id="Seg_8730" s="T103">adv</ta>
            <ta e="T106" id="Seg_8731" s="T105">adv</ta>
            <ta e="T107" id="Seg_8732" s="T106">propr-n:case</ta>
            <ta e="T108" id="Seg_8733" s="T107">adv</ta>
            <ta e="T109" id="Seg_8734" s="T108">adv</ta>
            <ta e="T110" id="Seg_8735" s="T109">v-any</ta>
            <ta e="T111" id="Seg_8736" s="T110">quant</ta>
            <ta e="T112" id="Seg_8737" s="T111">v-v:n.fin-v:tense-v:pn</ta>
            <ta e="T113" id="Seg_8738" s="T112">adv</ta>
            <ta e="T114" id="Seg_8739" s="T113">%%</ta>
            <ta e="T115" id="Seg_8740" s="T114">v-v:tense-v:pn</ta>
            <ta e="T116" id="Seg_8741" s="T115">n-n:case</ta>
            <ta e="T117" id="Seg_8742" s="T116">v-v&gt;v-v:n.fin</ta>
            <ta e="T118" id="Seg_8743" s="T117">v-v:tense-v:pn</ta>
            <ta e="T119" id="Seg_8744" s="T118">conj</ta>
            <ta e="T120" id="Seg_8745" s="T119">%%</ta>
            <ta e="T868" id="Seg_8746" s="T120">v-v:n-fin</ta>
            <ta e="T121" id="Seg_8747" s="T868">v-v:tense-v:pn</ta>
            <ta e="T122" id="Seg_8748" s="T121">conj</ta>
            <ta e="T123" id="Seg_8749" s="T122">pers</ta>
            <ta e="T124" id="Seg_8750" s="T123">v-v:tense-v:pn</ta>
            <ta e="T125" id="Seg_8751" s="T124">conj</ta>
            <ta e="T126" id="Seg_8752" s="T125">v-v:tense-v:pn</ta>
            <ta e="T127" id="Seg_8753" s="T126">adv</ta>
            <ta e="T128" id="Seg_8754" s="T127">adv</ta>
            <ta e="T129" id="Seg_8755" s="T128">n-n:case</ta>
            <ta e="T130" id="Seg_8756" s="T129">v-v:tense-v:pn</ta>
            <ta e="T131" id="Seg_8757" s="T130">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_8758" s="T131">pers</ta>
            <ta e="T133" id="Seg_8759" s="T132">conj</ta>
            <ta e="T134" id="Seg_8760" s="T133">propr-n:case</ta>
            <ta e="T135" id="Seg_8761" s="T134">v-v:tense-v:pn</ta>
            <ta e="T136" id="Seg_8762" s="T135">adv</ta>
            <ta e="T139" id="Seg_8763" s="T137">pers</ta>
            <ta e="T140" id="Seg_8764" s="T139">adv</ta>
            <ta e="T141" id="Seg_8765" s="T140">pers</ta>
            <ta e="T142" id="Seg_8766" s="T141">adv</ta>
            <ta e="T143" id="Seg_8767" s="T142">v-v:tense-v:pn</ta>
            <ta e="T144" id="Seg_8768" s="T143">conj</ta>
            <ta e="T145" id="Seg_8769" s="T144">v-v:tense-v:pn</ta>
            <ta e="T146" id="Seg_8770" s="T145">pers</ta>
            <ta e="T147" id="Seg_8771" s="T146">v-v&gt;v-v:pn</ta>
            <ta e="T148" id="Seg_8772" s="T147">que-n:case</ta>
            <ta e="T149" id="Seg_8773" s="T148">v-v&gt;v-v:n.fin</ta>
            <ta e="T150" id="Seg_8774" s="T149">v-v:mood-v:pn</ta>
            <ta e="T151" id="Seg_8775" s="T150">adv</ta>
            <ta e="T152" id="Seg_8776" s="T151">adv</ta>
            <ta e="T153" id="Seg_8777" s="T152">v-v:mood-v:pn</ta>
            <ta e="T154" id="Seg_8778" s="T153">v-v&gt;v-v:pn</ta>
            <ta e="T155" id="Seg_8779" s="T154">n-n:case</ta>
            <ta e="T156" id="Seg_8780" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_8781" s="T156">v-v:tense-v:pn</ta>
            <ta e="T158" id="Seg_8782" s="T157">v-v:mood.pn</ta>
            <ta e="T159" id="Seg_8783" s="T158">pers</ta>
            <ta e="T162" id="Seg_8784" s="T161">v-v:mood.pn</ta>
            <ta e="T163" id="Seg_8785" s="T162">adv</ta>
            <ta e="T164" id="Seg_8786" s="T163">adv</ta>
            <ta e="T165" id="Seg_8787" s="T164">pers</ta>
            <ta e="T166" id="Seg_8788" s="T165">v-v:tense-v:pn</ta>
            <ta e="T167" id="Seg_8789" s="T166">conj</ta>
            <ta e="T168" id="Seg_8790" s="T167">v-v:tense-v:pn</ta>
            <ta e="T169" id="Seg_8791" s="T168">propr-n:case</ta>
            <ta e="T170" id="Seg_8792" s="T169">adv</ta>
            <ta e="T171" id="Seg_8793" s="T170">adv</ta>
            <ta e="T172" id="Seg_8794" s="T171">adv</ta>
            <ta e="T173" id="Seg_8795" s="T172">num-n:case</ta>
            <ta e="T175" id="Seg_8796" s="T174">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T176" id="Seg_8797" s="T175">propr-n:case.poss</ta>
            <ta e="T177" id="Seg_8798" s="T176">v-v:n.fin</ta>
            <ta e="T178" id="Seg_8799" s="T177">num-n:case</ta>
            <ta e="T179" id="Seg_8800" s="T178">n-n:num</ta>
            <ta e="T180" id="Seg_8801" s="T179">v-v:tense-v:pn</ta>
            <ta e="T181" id="Seg_8802" s="T180">n-n:case</ta>
            <ta e="T182" id="Seg_8803" s="T181">conj</ta>
            <ta e="T183" id="Seg_8804" s="T182">pers</ta>
            <ta e="T184" id="Seg_8805" s="T183">v-v:tense-v:pn</ta>
            <ta e="T185" id="Seg_8806" s="T184">adv</ta>
            <ta e="T186" id="Seg_8807" s="T185">propr-n:case.poss</ta>
            <ta e="T187" id="Seg_8808" s="T186">v-v:tense-v:pn</ta>
            <ta e="T188" id="Seg_8809" s="T187">v-v:mood.pn</ta>
            <ta e="T189" id="Seg_8810" s="T188">pers</ta>
            <ta e="T190" id="Seg_8811" s="T189">n-n:case</ta>
            <ta e="T191" id="Seg_8812" s="T190">v-v:mood.pn</ta>
            <ta e="T192" id="Seg_8813" s="T191">n-n:case</ta>
            <ta e="T193" id="Seg_8814" s="T192">dempro-n:num</ta>
            <ta e="T194" id="Seg_8815" s="T193">v-v&gt;v-v:pn</ta>
            <ta e="T196" id="Seg_8816" s="T195">v-v:ins-v:mood.pn</ta>
            <ta e="T197" id="Seg_8817" s="T196">n-n:case</ta>
            <ta e="T199" id="Seg_8818" s="T198">adv</ta>
            <ta e="T200" id="Seg_8819" s="T199">v-v:mood.pn</ta>
            <ta e="T201" id="Seg_8820" s="T200">pers</ta>
            <ta e="T202" id="Seg_8821" s="T201">quant</ta>
            <ta e="T203" id="Seg_8822" s="T202">ptcl</ta>
            <ta e="T204" id="Seg_8823" s="T203">adv</ta>
            <ta e="T205" id="Seg_8824" s="T204">num-n:case</ta>
            <ta e="T206" id="Seg_8825" s="T205">n-n:case</ta>
            <ta e="T207" id="Seg_8826" s="T206">v-v:tense-v:pn</ta>
            <ta e="T208" id="Seg_8827" s="T207">dempro-n:case</ta>
            <ta e="T209" id="Seg_8828" s="T208">pers</ta>
            <ta e="T210" id="Seg_8829" s="T209">num-n:case</ta>
            <ta e="T211" id="Seg_8830" s="T210">n-n:case</ta>
            <ta e="T212" id="Seg_8831" s="T211">v-v:tense-v:pn</ta>
            <ta e="T213" id="Seg_8832" s="T212">pers</ta>
            <ta e="T214" id="Seg_8833" s="T213">v-v:tense-v:pn</ta>
            <ta e="T215" id="Seg_8834" s="T214">n-n:case</ta>
            <ta e="T216" id="Seg_8835" s="T215">v-v:tense-v:pn</ta>
            <ta e="T217" id="Seg_8836" s="T216">v-v:tense-v:pn</ta>
            <ta e="T218" id="Seg_8837" s="T217">conj</ta>
            <ta e="T219" id="Seg_8838" s="T218">v-v:tense-v:pn</ta>
            <ta e="T220" id="Seg_8839" s="T219">n-n:case</ta>
            <ta e="T221" id="Seg_8840" s="T220">v-v:tense-v:pn</ta>
            <ta e="T222" id="Seg_8841" s="T221">adv</ta>
            <ta e="T223" id="Seg_8842" s="T222">n-n:case</ta>
            <ta e="T224" id="Seg_8843" s="T223">v-v:tense-v:pn</ta>
            <ta e="T225" id="Seg_8844" s="T224">pers</ta>
            <ta e="T227" id="Seg_8845" s="T226">v-v:tense-v:pn</ta>
            <ta e="T228" id="Seg_8846" s="T227">adv</ta>
            <ta e="T229" id="Seg_8847" s="T228">v-v:tense-v:pn</ta>
            <ta e="T230" id="Seg_8848" s="T229">num-n:case</ta>
            <ta e="T231" id="Seg_8849" s="T230">n-n:case</ta>
            <ta e="T232" id="Seg_8850" s="T231">v-v:tense-v:pn</ta>
            <ta e="T233" id="Seg_8851" s="T232">n-n:case</ta>
            <ta e="T234" id="Seg_8852" s="T233">pers</ta>
            <ta e="T235" id="Seg_8853" s="T234">adv</ta>
            <ta e="T237" id="Seg_8854" s="T236">v-v:tense-v:pn</ta>
            <ta e="T238" id="Seg_8855" s="T237">v-v:mood-v:pn</ta>
            <ta e="T239" id="Seg_8856" s="T238">adv</ta>
            <ta e="T240" id="Seg_8857" s="T239">ptcl</ta>
            <ta e="T241" id="Seg_8858" s="T240">pers</ta>
            <ta e="T242" id="Seg_8859" s="T241">v-v&gt;v-v:pn</ta>
            <ta e="T243" id="Seg_8860" s="T242">n-n:case</ta>
            <ta e="T244" id="Seg_8861" s="T243">n-n:case</ta>
            <ta e="T245" id="Seg_8862" s="T244">v-v:mood-v:pn</ta>
            <ta e="T246" id="Seg_8863" s="T245">adv</ta>
            <ta e="T247" id="Seg_8864" s="T246">n-n:case</ta>
            <ta e="T248" id="Seg_8865" s="T247">v-v:tense-v:pn</ta>
            <ta e="T251" id="Seg_8866" s="T250">v-v:tense-v:pn</ta>
            <ta e="T252" id="Seg_8867" s="T251">conj</ta>
            <ta e="T253" id="Seg_8868" s="T252">propr-n:case</ta>
            <ta e="T254" id="Seg_8869" s="T253">v-v:tense-v:pn</ta>
            <ta e="T255" id="Seg_8870" s="T254">adv</ta>
            <ta e="T256" id="Seg_8871" s="T255">%%</ta>
            <ta e="T258" id="Seg_8872" s="T257">v-v:tense-v:pn</ta>
            <ta e="T259" id="Seg_8873" s="T258">n-n:case</ta>
            <ta e="T260" id="Seg_8874" s="T259">n-n:case</ta>
            <ta e="T261" id="Seg_8875" s="T260">propr-n:case</ta>
            <ta e="T262" id="Seg_8876" s="T261">n-n:case</ta>
            <ta e="T263" id="Seg_8877" s="T262">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T264" id="Seg_8878" s="T263">v-v:tense-v:pn</ta>
            <ta e="T265" id="Seg_8879" s="T264">dempro-n:case</ta>
            <ta e="T266" id="Seg_8880" s="T265">pers</ta>
            <ta e="T267" id="Seg_8881" s="T266">adv</ta>
            <ta e="T268" id="Seg_8882" s="T267">v-v&gt;v-v:pn</ta>
            <ta e="T269" id="Seg_8883" s="T268">adv</ta>
            <ta e="T270" id="Seg_8884" s="T269">v-v:mood.pn</ta>
            <ta e="T271" id="Seg_8885" s="T270">pers</ta>
            <ta e="T272" id="Seg_8886" s="T271">n-n:case</ta>
            <ta e="T273" id="Seg_8887" s="T272">v-v:ins-v:mood.pn</ta>
            <ta e="T274" id="Seg_8888" s="T273">pers</ta>
            <ta e="T275" id="Seg_8889" s="T274">v-v:tense-v:pn</ta>
            <ta e="T276" id="Seg_8890" s="T275">que=ptcl</ta>
            <ta e="T277" id="Seg_8891" s="T276">ptcl</ta>
            <ta e="T278" id="Seg_8892" s="T277">v-v:tense-v:pn</ta>
            <ta e="T279" id="Seg_8893" s="T278">adv</ta>
            <ta e="T280" id="Seg_8894" s="T279">v-v:tense-v:pn</ta>
            <ta e="T281" id="Seg_8895" s="T280">adv</ta>
            <ta e="T282" id="Seg_8896" s="T281">adv</ta>
            <ta e="T283" id="Seg_8897" s="T282">adv</ta>
            <ta e="T284" id="Seg_8898" s="T283">n-n:case</ta>
            <ta e="T285" id="Seg_8899" s="T284">v-v:tense-v:pn</ta>
            <ta e="T286" id="Seg_8900" s="T285">pers</ta>
            <ta e="T287" id="Seg_8901" s="T286">v-v:tense-v:pn</ta>
            <ta e="T288" id="Seg_8902" s="T287">propr-n:case</ta>
            <ta e="T289" id="Seg_8903" s="T288">v-v:tense-v:pn</ta>
            <ta e="T290" id="Seg_8904" s="T289">adv</ta>
            <ta e="T291" id="Seg_8905" s="T290">adv</ta>
            <ta e="T292" id="Seg_8906" s="T291">n-n:case</ta>
            <ta e="T293" id="Seg_8907" s="T292">v-v:tense-v:pn</ta>
            <ta e="T294" id="Seg_8908" s="T293">adv</ta>
            <ta e="T295" id="Seg_8909" s="T294">adv</ta>
            <ta e="T296" id="Seg_8910" s="T295">n-n:case</ta>
            <ta e="T297" id="Seg_8911" s="T296">v-v:tense-v:pn</ta>
            <ta e="T298" id="Seg_8912" s="T297">conj</ta>
            <ta e="T299" id="Seg_8913" s="T298">n-n:case.poss</ta>
            <ta e="T300" id="Seg_8914" s="T299">v-v:tense-v:pn</ta>
            <ta e="T301" id="Seg_8915" s="T300">que</ta>
            <ta e="T302" id="Seg_8916" s="T301">propr-n:case</ta>
            <ta e="T303" id="Seg_8917" s="T302">v-v:tense-v:pn</ta>
            <ta e="T304" id="Seg_8918" s="T303">pers</ta>
            <ta e="T306" id="Seg_8919" s="T305">v-v&gt;v-v:pn</ta>
            <ta e="T307" id="Seg_8920" s="T306">n-n&gt;v-v:n.fin</ta>
            <ta e="T308" id="Seg_8921" s="T307">conj</ta>
            <ta e="T309" id="Seg_8922" s="T308">pers</ta>
            <ta e="T310" id="Seg_8923" s="T309">n</ta>
            <ta e="T311" id="Seg_8924" s="T310">n</ta>
            <ta e="T312" id="Seg_8925" s="T311">v-v&gt;v-v:pn</ta>
            <ta e="T313" id="Seg_8926" s="T312">conj</ta>
            <ta e="T314" id="Seg_8927" s="T313">v-v&gt;v-v:pn</ta>
            <ta e="T315" id="Seg_8928" s="T314">pers</ta>
            <ta e="T316" id="Seg_8929" s="T315">adv</ta>
            <ta e="T317" id="Seg_8930" s="T316">n-n:case</ta>
            <ta e="T318" id="Seg_8931" s="T317">v-v:tense-v:pn</ta>
            <ta e="T319" id="Seg_8932" s="T318">conj</ta>
            <ta e="T320" id="Seg_8933" s="T319">pers</ta>
            <ta e="T321" id="Seg_8934" s="T320">n-n:case</ta>
            <ta e="T322" id="Seg_8935" s="T321">v-v:tense-v:pn</ta>
            <ta e="T323" id="Seg_8936" s="T322">adv</ta>
            <ta e="T324" id="Seg_8937" s="T323">dempro-n:num</ta>
            <ta e="T325" id="Seg_8938" s="T324">n-n:case</ta>
            <ta e="T326" id="Seg_8939" s="T325">v-v:tense-v:pn</ta>
            <ta e="T327" id="Seg_8940" s="T326">propr-n:case</ta>
            <ta e="T329" id="Seg_8941" s="T328">n-n:case</ta>
            <ta e="T330" id="Seg_8942" s="T329">v-v:tense-v:pn</ta>
            <ta e="T331" id="Seg_8943" s="T330">adv</ta>
            <ta e="T334" id="Seg_8944" s="T333">v-v:tense-v:pn</ta>
            <ta e="T335" id="Seg_8945" s="T334">adv</ta>
            <ta e="T336" id="Seg_8946" s="T335">num-n:case</ta>
            <ta e="T337" id="Seg_8947" s="T336">n-n:case</ta>
            <ta e="T338" id="Seg_8948" s="T337">adj-n:case</ta>
            <ta e="T339" id="Seg_8949" s="T338">ptcl</ta>
            <ta e="T340" id="Seg_8950" s="T339">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T341" id="Seg_8951" s="T340">ptcl</ta>
            <ta e="T342" id="Seg_8952" s="T341">adv</ta>
            <ta e="T343" id="Seg_8953" s="T342">dempro-n:num</ta>
            <ta e="T344" id="Seg_8954" s="T343">v-v:tense-v:pn</ta>
            <ta e="T345" id="Seg_8955" s="T344">n-n:case</ta>
            <ta e="T346" id="Seg_8956" s="T345">propr-n:case</ta>
            <ta e="T347" id="Seg_8957" s="T346">conj</ta>
            <ta e="T348" id="Seg_8958" s="T347">n-n:case</ta>
            <ta e="T349" id="Seg_8959" s="T348">v-v:tense-v:pn</ta>
            <ta e="T350" id="Seg_8960" s="T349">conj</ta>
            <ta e="T351" id="Seg_8961" s="T350">dempro-n:case</ta>
            <ta e="T352" id="Seg_8962" s="T351">v-v:tense-v:pn</ta>
            <ta e="T353" id="Seg_8963" s="T352">n-n:case</ta>
            <ta e="T354" id="Seg_8964" s="T353">adv</ta>
            <ta e="T355" id="Seg_8965" s="T354">n-n:case</ta>
            <ta e="T356" id="Seg_8966" s="T355">ptcl</ta>
            <ta e="T357" id="Seg_8967" s="T356">n-n:case</ta>
            <ta e="T358" id="Seg_8968" s="T357">v-v:tense-v:pn</ta>
            <ta e="T359" id="Seg_8969" s="T358">conj</ta>
            <ta e="T360" id="Seg_8970" s="T359">v-v:tense-v:pn</ta>
            <ta e="T361" id="Seg_8971" s="T360">v-v:tense-v:pn</ta>
            <ta e="T362" id="Seg_8972" s="T361">ptcl</ta>
            <ta e="T363" id="Seg_8973" s="T362">dempro-n:case</ta>
            <ta e="T364" id="Seg_8974" s="T363">v-v:n.fin</ta>
            <ta e="T365" id="Seg_8975" s="T364">ptcl</ta>
            <ta e="T367" id="Seg_8976" s="T366">ptcl</ta>
            <ta e="T368" id="Seg_8977" s="T367">v-v:tense-v:pn</ta>
            <ta e="T369" id="Seg_8978" s="T368">pers</ta>
            <ta e="T372" id="Seg_8979" s="T371">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T373" id="Seg_8980" s="T372">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T375" id="Seg_8981" s="T374">adv</ta>
            <ta e="T376" id="Seg_8982" s="T375">v-v:tense-v:pn</ta>
            <ta e="T377" id="Seg_8983" s="T376">n-n:case</ta>
            <ta e="T378" id="Seg_8984" s="T377">adv</ta>
            <ta e="T380" id="Seg_8985" s="T379">v-v:tense-v:pn</ta>
            <ta e="T381" id="Seg_8986" s="T380">que-n:case</ta>
            <ta e="T383" id="Seg_8987" s="T381">pers=ptcl</ta>
            <ta e="T384" id="Seg_8988" s="T383">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T385" id="Seg_8989" s="T384">que-n:case</ta>
            <ta e="T387" id="Seg_8990" s="T385">pers=ptcl</ta>
            <ta e="T388" id="Seg_8991" s="T387">ptcl</ta>
            <ta e="T389" id="Seg_8992" s="T388">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T390" id="Seg_8993" s="T389">que=ptcl</ta>
            <ta e="T391" id="Seg_8994" s="T390">v-v:pn</ta>
            <ta e="T392" id="Seg_8995" s="T391">adv-n:num</ta>
            <ta e="T393" id="Seg_8996" s="T392">conj</ta>
            <ta e="T394" id="Seg_8997" s="T393">pers</ta>
            <ta e="T395" id="Seg_8998" s="T394">v-v:tense-v:pn</ta>
            <ta e="T396" id="Seg_8999" s="T395">v-v:tense-v:pn</ta>
            <ta e="T397" id="Seg_9000" s="T396">adv</ta>
            <ta e="T398" id="Seg_9001" s="T397">adv-n:case</ta>
            <ta e="T399" id="Seg_9002" s="T398">v-v:tense-v:pn</ta>
            <ta e="T400" id="Seg_9003" s="T399">conj</ta>
            <ta e="T401" id="Seg_9004" s="T400">dempro-n:case</ta>
            <ta e="T402" id="Seg_9005" s="T401">que=ptcl</ta>
            <ta e="T403" id="Seg_9006" s="T402">v-v:pn</ta>
            <ta e="T404" id="Seg_9007" s="T403">adv</ta>
            <ta e="T405" id="Seg_9008" s="T404">%%</ta>
            <ta e="T406" id="Seg_9009" s="T405">adv</ta>
            <ta e="T407" id="Seg_9010" s="T406">n-n:case</ta>
            <ta e="T408" id="Seg_9011" s="T407">v-v:tense-v:pn</ta>
            <ta e="T409" id="Seg_9012" s="T408">que=ptcl</ta>
            <ta e="T869" id="Seg_9013" s="T409">v-v:n-fin</ta>
            <ta e="T410" id="Seg_9014" s="T869">v-v:tense-v:pn</ta>
            <ta e="T411" id="Seg_9015" s="T410">ptcl</ta>
            <ta e="T412" id="Seg_9016" s="T411">v-v:pn</ta>
            <ta e="T413" id="Seg_9017" s="T412">propr-n:case</ta>
            <ta e="T414" id="Seg_9018" s="T413">%%</ta>
            <ta e="T415" id="Seg_9019" s="T414">%%</ta>
            <ta e="T416" id="Seg_9020" s="T415">ptcl</ta>
            <ta e="T417" id="Seg_9021" s="T416">n-n:case</ta>
            <ta e="T418" id="Seg_9022" s="T417">n-n:case</ta>
            <ta e="T419" id="Seg_9023" s="T418">v-v:tense-v:pn</ta>
            <ta e="T420" id="Seg_9024" s="T419">adv</ta>
            <ta e="T421" id="Seg_9025" s="T420">que=ptcl</ta>
            <ta e="T422" id="Seg_9026" s="T421">n-n:case</ta>
            <ta e="T423" id="Seg_9027" s="T422">v</ta>
            <ta e="T424" id="Seg_9028" s="T423">n-n:case</ta>
            <ta e="T426" id="Seg_9029" s="T425">conj</ta>
            <ta e="T427" id="Seg_9030" s="T426">n-n:case</ta>
            <ta e="T428" id="Seg_9031" s="T427">adv</ta>
            <ta e="T429" id="Seg_9032" s="T428">adj-n:case</ta>
            <ta e="T430" id="Seg_9033" s="T429">dempro-n:num</ta>
            <ta e="T431" id="Seg_9034" s="T430">ptcl</ta>
            <ta e="T432" id="Seg_9035" s="T431">v</ta>
            <ta e="T433" id="Seg_9036" s="T432">n-n:case</ta>
            <ta e="T434" id="Seg_9037" s="T433">v-v:n.fin</ta>
            <ta e="T435" id="Seg_9038" s="T434">conj</ta>
            <ta e="T436" id="Seg_9039" s="T435">ptcl</ta>
            <ta e="T437" id="Seg_9040" s="T436">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T439" id="Seg_9041" s="T437">adv</ta>
            <ta e="T440" id="Seg_9042" s="T439">adv</ta>
            <ta e="T441" id="Seg_9043" s="T440">adv</ta>
            <ta e="T442" id="Seg_9044" s="T441">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T443" id="Seg_9045" s="T442">v-v:tense-v:pn</ta>
            <ta e="T445" id="Seg_9046" s="T444">v-v:tense-v:pn</ta>
            <ta e="T446" id="Seg_9047" s="T445">conj</ta>
            <ta e="T447" id="Seg_9048" s="T446">%%</ta>
            <ta e="T448" id="Seg_9049" s="T447">v-v:tense-v:pn</ta>
            <ta e="T450" id="Seg_9050" s="T449">propr-n:ins-n:case</ta>
            <ta e="T451" id="Seg_9051" s="T450">n-n:case.poss</ta>
            <ta e="T452" id="Seg_9052" s="T451">adv</ta>
            <ta e="T453" id="Seg_9053" s="T452">v-v:tense-v:pn</ta>
            <ta e="T454" id="Seg_9054" s="T453">conj</ta>
            <ta e="T456" id="Seg_9055" s="T455">conj</ta>
            <ta e="T457" id="Seg_9056" s="T456">%%</ta>
            <ta e="T459" id="Seg_9057" s="T458">n-n:case</ta>
            <ta e="T460" id="Seg_9058" s="T459">adv</ta>
            <ta e="T461" id="Seg_9059" s="T460">v-v:tense-v:pn</ta>
            <ta e="T466" id="Seg_9060" s="T465">n-n:case</ta>
            <ta e="T467" id="Seg_9061" s="T466">adv</ta>
            <ta e="T468" id="Seg_9062" s="T467">v-v:tense-v:pn</ta>
            <ta e="T471" id="Seg_9063" s="T470">n-n:case</ta>
            <ta e="T472" id="Seg_9064" s="T471">adv</ta>
            <ta e="T473" id="Seg_9065" s="T472">v-v:tense-v:pn</ta>
            <ta e="T476" id="Seg_9066" s="T475">n-n:case</ta>
            <ta e="T477" id="Seg_9067" s="T476">adv</ta>
            <ta e="T478" id="Seg_9068" s="T477">v-v:tense-v:pn</ta>
            <ta e="T479" id="Seg_9069" s="T478">conj</ta>
            <ta e="T481" id="Seg_9070" s="T480">propr-n:case</ta>
            <ta e="T482" id="Seg_9071" s="T481">adv</ta>
            <ta e="T483" id="Seg_9072" s="T482">n-n:case</ta>
            <ta e="T484" id="Seg_9073" s="T483">v-v:tense-v:pn</ta>
            <ta e="T485" id="Seg_9074" s="T484">conj</ta>
            <ta e="T489" id="Seg_9075" s="T488">adv</ta>
            <ta e="T490" id="Seg_9076" s="T489">n-n:case</ta>
            <ta e="T491" id="Seg_9077" s="T490">v-v:tense-v:pn</ta>
            <ta e="T492" id="Seg_9078" s="T491">que</ta>
            <ta e="T494" id="Seg_9079" s="T493">v-v:tense-v:pn</ta>
            <ta e="T495" id="Seg_9080" s="T494">v-v:n.fin</ta>
            <ta e="T496" id="Seg_9081" s="T495">adv</ta>
            <ta e="T497" id="Seg_9082" s="T496">adv</ta>
            <ta e="T499" id="Seg_9083" s="T498">num-n:case</ta>
            <ta e="T500" id="Seg_9084" s="T499">num-n:case</ta>
            <ta e="T501" id="Seg_9085" s="T500">n-n:case</ta>
            <ta e="T502" id="Seg_9086" s="T501">v-v:tense-v:pn</ta>
            <ta e="T506" id="Seg_9087" s="T505">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T507" id="Seg_9088" s="T506">dempro-n:case</ta>
            <ta e="T508" id="Seg_9089" s="T507">v-v:tense-v:pn</ta>
            <ta e="T509" id="Seg_9090" s="T508">propr-n:case</ta>
            <ta e="T510" id="Seg_9091" s="T509">adv</ta>
            <ta e="T511" id="Seg_9092" s="T510">n-n:case</ta>
            <ta e="T512" id="Seg_9093" s="T511">v-v:tense-v:pn</ta>
            <ta e="T513" id="Seg_9094" s="T512">adv</ta>
            <ta e="T515" id="Seg_9095" s="T514">v-v:tense-v:pn</ta>
            <ta e="T516" id="Seg_9096" s="T515">num-n:case</ta>
            <ta e="T518" id="Seg_9097" s="T517">conj</ta>
            <ta e="T519" id="Seg_9098" s="T518">n-n:case.poss-n:case</ta>
            <ta e="T520" id="Seg_9099" s="T519">v-v:tense-v:pn</ta>
            <ta e="T521" id="Seg_9100" s="T520">adv</ta>
            <ta e="T522" id="Seg_9101" s="T521">v-v:tense-v:pn</ta>
            <ta e="T523" id="Seg_9102" s="T522">n-n:num-n:case</ta>
            <ta e="T524" id="Seg_9103" s="T523">adv</ta>
            <ta e="T525" id="Seg_9104" s="T524">v-v:tense</ta>
            <ta e="T526" id="Seg_9105" s="T525">conj</ta>
            <ta e="T527" id="Seg_9106" s="T526">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T528" id="Seg_9107" s="T527">v-v:ins-v:mood.pn</ta>
            <ta e="T529" id="Seg_9108" s="T528">n-n:case.poss</ta>
            <ta e="T530" id="Seg_9109" s="T529">v-v:ins-v:mood.pn</ta>
            <ta e="T531" id="Seg_9110" s="T530">n-n:case.poss</ta>
            <ta e="T532" id="Seg_9111" s="T531">conj</ta>
            <ta e="T533" id="Seg_9112" s="T532">pers</ta>
            <ta e="T534" id="Seg_9113" s="T533">n-n:case.poss</ta>
            <ta e="T535" id="Seg_9114" s="T534">v-v:tense-v:pn</ta>
            <ta e="T536" id="Seg_9115" s="T535">conj</ta>
            <ta e="T537" id="Seg_9116" s="T536">v-v&gt;v-v:pn</ta>
            <ta e="T538" id="Seg_9117" s="T537">conj</ta>
            <ta e="T539" id="Seg_9118" s="T538">v-v&gt;v-v:pn</ta>
            <ta e="T540" id="Seg_9119" s="T539">pers</ta>
            <ta e="T541" id="Seg_9120" s="T540">n-n:case</ta>
            <ta e="T542" id="Seg_9121" s="T541">v-v:tense-v:pn</ta>
            <ta e="T543" id="Seg_9122" s="T542">adv</ta>
            <ta e="T544" id="Seg_9123" s="T543">v-v:tense-v:pn</ta>
            <ta e="T545" id="Seg_9124" s="T544">adv</ta>
            <ta e="T546" id="Seg_9125" s="T545">n-n:case.poss</ta>
            <ta e="T547" id="Seg_9126" s="T546">v-v:tense-v:pn</ta>
            <ta e="T548" id="Seg_9127" s="T547">dempro-n:case</ta>
            <ta e="T549" id="Seg_9128" s="T548">adj-n:case</ta>
            <ta e="T550" id="Seg_9129" s="T549">v-v:tense-v:pn</ta>
            <ta e="T551" id="Seg_9130" s="T550">adv</ta>
            <ta e="T552" id="Seg_9131" s="T551">num-n:case</ta>
            <ta e="T554" id="Seg_9132" s="T553">n-n:case</ta>
            <ta e="T555" id="Seg_9133" s="T554">ptcl</ta>
            <ta e="T556" id="Seg_9134" s="T555">v-v:tense-v:pn</ta>
            <ta e="T557" id="Seg_9135" s="T556">adv</ta>
            <ta e="T558" id="Seg_9136" s="T557">adj-n:case</ta>
            <ta e="T559" id="Seg_9137" s="T558">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T562" id="Seg_9138" s="T561">pers</ta>
            <ta e="T563" id="Seg_9139" s="T562">num-n:case</ta>
            <ta e="T564" id="Seg_9140" s="T563">n-n:case</ta>
            <ta e="T565" id="Seg_9141" s="T564">v-v:tense-v:pn</ta>
            <ta e="T566" id="Seg_9142" s="T565">v-v:tense-v:pn</ta>
            <ta e="T567" id="Seg_9143" s="T566">ptcl</ta>
            <ta e="T568" id="Seg_9144" s="T567">v-v:tense-v:pn</ta>
            <ta e="T569" id="Seg_9145" s="T568">conj</ta>
            <ta e="T570" id="Seg_9146" s="T569">adv</ta>
            <ta e="T571" id="Seg_9147" s="T570">v-v:tense-v:pn</ta>
            <ta e="T572" id="Seg_9148" s="T571">adj-n:case</ta>
            <ta e="T574" id="Seg_9149" s="T573">n-n:case</ta>
            <ta e="T575" id="Seg_9150" s="T574">pers</ta>
            <ta e="T576" id="Seg_9151" s="T575">v-v:tense-v:pn</ta>
            <ta e="T577" id="Seg_9152" s="T576">num-n:case</ta>
            <ta e="T578" id="Seg_9153" s="T577">num-n:case</ta>
            <ta e="T579" id="Seg_9154" s="T578">n-n:case</ta>
            <ta e="T580" id="Seg_9155" s="T579">refl-n:case.poss</ta>
            <ta e="T581" id="Seg_9156" s="T580">n-n:case</ta>
            <ta e="T582" id="Seg_9157" s="T581">v-v:tense-v:pn</ta>
            <ta e="T583" id="Seg_9158" s="T582">refl-n:case.poss</ta>
            <ta e="T584" id="Seg_9159" s="T583">v-v:tense-v:pn</ta>
            <ta e="T585" id="Seg_9160" s="T584">refl-n:case.poss</ta>
            <ta e="T586" id="Seg_9161" s="T585">v-v:tense-v:pn</ta>
            <ta e="T587" id="Seg_9162" s="T586">n-n:case</ta>
            <ta e="T588" id="Seg_9163" s="T587">n-n:case.poss</ta>
            <ta e="T589" id="Seg_9164" s="T588">v-v:tense-v:pn</ta>
            <ta e="T590" id="Seg_9165" s="T589">ptcl</ta>
            <ta e="T591" id="Seg_9166" s="T590">n-n:num-n:case.poss</ta>
            <ta e="T594" id="Seg_9167" s="T593">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T595" id="Seg_9168" s="T594">num-n:case</ta>
            <ta e="T596" id="Seg_9169" s="T595">n-n:case</ta>
            <ta e="T597" id="Seg_9170" s="T596">v-v:tense-v:pn</ta>
            <ta e="T598" id="Seg_9171" s="T597">adv</ta>
            <ta e="T599" id="Seg_9172" s="T598">n-n:case.poss</ta>
            <ta e="T600" id="Seg_9173" s="T599">n-n:case</ta>
            <ta e="T601" id="Seg_9174" s="T600">v-v:tense-v:pn</ta>
            <ta e="T602" id="Seg_9175" s="T601">conj</ta>
            <ta e="T603" id="Seg_9176" s="T602">adv</ta>
            <ta e="T605" id="Seg_9177" s="T604">v-v:tense-v:pn</ta>
            <ta e="T606" id="Seg_9178" s="T605">n-n:case</ta>
            <ta e="T607" id="Seg_9179" s="T606">v-v:tense-v:pn</ta>
            <ta e="T608" id="Seg_9180" s="T607">adv</ta>
            <ta e="T609" id="Seg_9181" s="T608">v-v:tense-v:pn</ta>
            <ta e="T610" id="Seg_9182" s="T609">v-v:tense-v:pn</ta>
            <ta e="T611" id="Seg_9183" s="T610">pers</ta>
            <ta e="T612" id="Seg_9184" s="T611">n-n:case</ta>
            <ta e="T867" id="Seg_9185" s="T613">v-v:tense-v:pn </ta>
            <ta e="T615" id="Seg_9186" s="T614">adv</ta>
            <ta e="T616" id="Seg_9187" s="T615">v-v:tense-v:pn</ta>
            <ta e="T617" id="Seg_9188" s="T616">v-v:tense-v:pn</ta>
            <ta e="T618" id="Seg_9189" s="T617">ptcl</ta>
            <ta e="T619" id="Seg_9190" s="T618">v-v:tense-v:pn</ta>
            <ta e="T620" id="Seg_9191" s="T619">dempro</ta>
            <ta e="T621" id="Seg_9192" s="T620">v-v&gt;v-v:pn</ta>
            <ta e="T622" id="Seg_9193" s="T621">adv</ta>
            <ta e="T623" id="Seg_9194" s="T622">n-n:case</ta>
            <ta e="T626" id="Seg_9195" s="T625">v-v:tense-v:pn</ta>
            <ta e="T627" id="Seg_9196" s="T626">adv</ta>
            <ta e="T628" id="Seg_9197" s="T627">v-v:tense-v:pn</ta>
            <ta e="T629" id="Seg_9198" s="T628">conj</ta>
            <ta e="T630" id="Seg_9199" s="T629">que</ta>
            <ta e="T631" id="Seg_9200" s="T630">pers</ta>
            <ta e="T632" id="Seg_9201" s="T631">v-v:tense-v:pn</ta>
            <ta e="T633" id="Seg_9202" s="T632">conj</ta>
            <ta e="T634" id="Seg_9203" s="T633">ptcl</ta>
            <ta e="T635" id="Seg_9204" s="T634">ptcl</ta>
            <ta e="T636" id="Seg_9205" s="T866">v-v:tense-v:pn</ta>
            <ta e="T637" id="Seg_9206" s="T636">%%-n:num</ta>
            <ta e="T638" id="Seg_9207" s="T637">conj</ta>
            <ta e="T639" id="Seg_9208" s="T638">adv</ta>
            <ta e="T640" id="Seg_9209" s="T639">n-n:case</ta>
            <ta e="T643" id="Seg_9210" s="T642">adv</ta>
            <ta e="T644" id="Seg_9211" s="T643">n-n:case</ta>
            <ta e="T646" id="Seg_9212" s="T645">v-v:tense-v:pn</ta>
            <ta e="T647" id="Seg_9213" s="T646">v-v:tense-v:pn</ta>
            <ta e="T648" id="Seg_9214" s="T647">n-n:case</ta>
            <ta e="T649" id="Seg_9215" s="T648">adv</ta>
            <ta e="T650" id="Seg_9216" s="T649">n-n:case</ta>
            <ta e="T652" id="Seg_9217" s="T651">pers</ta>
            <ta e="T653" id="Seg_9218" s="T652">adv</ta>
            <ta e="T654" id="Seg_9219" s="T653">v-v:tense-v:pn</ta>
            <ta e="T655" id="Seg_9220" s="T654">adv</ta>
            <ta e="T656" id="Seg_9221" s="T655">n</ta>
            <ta e="T657" id="Seg_9222" s="T656">v-v:tense-v:pn</ta>
            <ta e="T658" id="Seg_9223" s="T657">adv</ta>
            <ta e="T659" id="Seg_9224" s="T658">pers</ta>
            <ta e="T660" id="Seg_9225" s="T659">n-n:case</ta>
            <ta e="T661" id="Seg_9226" s="T660">ptcl</ta>
            <ta e="T662" id="Seg_9227" s="T661">v-v:tense-v:pn</ta>
            <ta e="T663" id="Seg_9228" s="T662">n-n:case</ta>
            <ta e="T664" id="Seg_9229" s="T663">adv</ta>
            <ta e="T665" id="Seg_9230" s="T664">v-v:tense-v:pn</ta>
            <ta e="T666" id="Seg_9231" s="T665">conj</ta>
            <ta e="T667" id="Seg_9232" s="T666">que=ptcl</ta>
            <ta e="T668" id="Seg_9233" s="T667">v-v:tense-v:pn</ta>
            <ta e="T669" id="Seg_9234" s="T668">n-n:case.poss</ta>
            <ta e="T673" id="Seg_9235" s="T672">que</ta>
            <ta e="T674" id="Seg_9236" s="T673">n-n:case</ta>
            <ta e="T676" id="Seg_9237" s="T675">n-n:case</ta>
            <ta e="T677" id="Seg_9238" s="T676">v-v:tense-v:pn</ta>
            <ta e="T679" id="Seg_9239" s="T678">dempro-n:case</ta>
            <ta e="T680" id="Seg_9240" s="T679">n-n:case</ta>
            <ta e="T681" id="Seg_9241" s="T680">v-v:n.fin</ta>
            <ta e="T682" id="Seg_9242" s="T681">ptcl</ta>
            <ta e="T683" id="Seg_9243" s="T682">v-v:n.fin</ta>
            <ta e="T684" id="Seg_9244" s="T683">dempro-n:case</ta>
            <ta e="T685" id="Seg_9245" s="T684">%%</ta>
            <ta e="T686" id="Seg_9246" s="T685">n-n:case</ta>
            <ta e="T687" id="Seg_9247" s="T686">v-v:n.fin</ta>
            <ta e="T688" id="Seg_9248" s="T687">adv</ta>
            <ta e="T689" id="Seg_9249" s="T688">ptcl</ta>
            <ta e="T690" id="Seg_9250" s="T689">v-v:tense-v:pn</ta>
            <ta e="T691" id="Seg_9251" s="T690">n-n:case</ta>
            <ta e="T692" id="Seg_9252" s="T691">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T693" id="Seg_9253" s="T692">conj</ta>
            <ta e="T694" id="Seg_9254" s="T693">n-n:case</ta>
            <ta e="T695" id="Seg_9255" s="T694">v-v:pn</ta>
            <ta e="T696" id="Seg_9256" s="T695">conj</ta>
            <ta e="T697" id="Seg_9257" s="T696">adj-n:case</ta>
            <ta e="T698" id="Seg_9258" s="T697">v-v:pn</ta>
            <ta e="T699" id="Seg_9259" s="T698">n-n:num</ta>
            <ta e="T700" id="Seg_9260" s="T699">pers</ta>
            <ta e="T701" id="Seg_9261" s="T700">adv</ta>
            <ta e="T702" id="Seg_9262" s="T701">ptcl</ta>
            <ta e="T703" id="Seg_9263" s="T702">v-v&gt;v-v:n.fin</ta>
            <ta e="T704" id="Seg_9264" s="T703">ptcl</ta>
            <ta e="T706" id="Seg_9265" s="T705">dempro-n:case</ta>
            <ta e="T707" id="Seg_9266" s="T706">n-n:case</ta>
            <ta e="T708" id="Seg_9267" s="T707">n-n:num</ta>
            <ta e="T709" id="Seg_9268" s="T708">pers</ta>
            <ta e="T710" id="Seg_9269" s="T709">quant</ta>
            <ta e="T711" id="Seg_9270" s="T710">dempro-n:case</ta>
            <ta e="T712" id="Seg_9271" s="T711">n-n:case</ta>
            <ta e="T713" id="Seg_9272" s="T712">adv</ta>
            <ta e="T714" id="Seg_9273" s="T713">v-v:tense-v:pn</ta>
            <ta e="T715" id="Seg_9274" s="T714">adj-n:case</ta>
            <ta e="T716" id="Seg_9275" s="T715">adv</ta>
            <ta e="T717" id="Seg_9276" s="T716">quant</ta>
            <ta e="T718" id="Seg_9277" s="T717">n-n:case</ta>
            <ta e="T719" id="Seg_9278" s="T718">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T720" id="Seg_9279" s="T719">n-n:num</ta>
            <ta e="T721" id="Seg_9280" s="T720">ptcl</ta>
            <ta e="T722" id="Seg_9281" s="T721">n-n:case</ta>
            <ta e="T723" id="Seg_9282" s="T722">adj-n:case</ta>
            <ta e="T724" id="Seg_9283" s="T723">v-v:tense-v:pn</ta>
            <ta e="T725" id="Seg_9284" s="T724">ptcl</ta>
            <ta e="T726" id="Seg_9285" s="T725">n-n:case</ta>
            <ta e="T727" id="Seg_9286" s="T726">v-v:tense-v:pn</ta>
            <ta e="T728" id="Seg_9287" s="T727">pers</ta>
            <ta e="T729" id="Seg_9288" s="T728">%%</ta>
            <ta e="T730" id="Seg_9289" s="T729">n-n:case</ta>
            <ta e="T731" id="Seg_9290" s="T730">n-n:num</ta>
            <ta e="T732" id="Seg_9291" s="T731">n-n:case</ta>
            <ta e="T733" id="Seg_9292" s="T732">n-n:num</ta>
            <ta e="T734" id="Seg_9293" s="T733">ptcl</ta>
            <ta e="T735" id="Seg_9294" s="T734">%%</ta>
            <ta e="T736" id="Seg_9295" s="T735">dempro-n:num</ta>
            <ta e="T737" id="Seg_9296" s="T736">adv</ta>
            <ta e="T738" id="Seg_9297" s="T737">adj</ta>
            <ta e="T739" id="Seg_9298" s="T738">adv</ta>
            <ta e="T740" id="Seg_9299" s="T739">v-v&gt;v-v:pn</ta>
            <ta e="T741" id="Seg_9300" s="T740">conj</ta>
            <ta e="T742" id="Seg_9301" s="T741">adj-n:case</ta>
            <ta e="T743" id="Seg_9302" s="T742">dempro-n:num</ta>
            <ta e="T744" id="Seg_9303" s="T743">%%</ta>
            <ta e="T745" id="Seg_9304" s="T744">v-v:tense-v:pn</ta>
            <ta e="T746" id="Seg_9305" s="T745">v-v:n.fin</ta>
            <ta e="T748" id="Seg_9306" s="T747">n-n:case</ta>
            <ta e="T749" id="Seg_9307" s="T748">ptcl</ta>
            <ta e="T750" id="Seg_9308" s="T749">ptcl</ta>
            <ta e="T751" id="Seg_9309" s="T750">v-v:tense-v:pn</ta>
            <ta e="T752" id="Seg_9310" s="T751">adv</ta>
            <ta e="T754" id="Seg_9311" s="T753">adv</ta>
            <ta e="T755" id="Seg_9312" s="T754">n-n:case</ta>
            <ta e="T756" id="Seg_9313" s="T755">quant</ta>
            <ta e="T757" id="Seg_9314" s="T756">v-v:tense-v:pn</ta>
            <ta e="T758" id="Seg_9315" s="T757">%%</ta>
            <ta e="T759" id="Seg_9316" s="T758">pers</ta>
            <ta e="T762" id="Seg_9317" s="T761">%%</ta>
            <ta e="T763" id="Seg_9318" s="T762">ptcl</ta>
            <ta e="T765" id="Seg_9319" s="T764">conj</ta>
            <ta e="T766" id="Seg_9320" s="T765">%%-v:tense</ta>
            <ta e="T767" id="Seg_9321" s="T766">dempro-n:case</ta>
            <ta e="T768" id="Seg_9322" s="T767">conj</ta>
            <ta e="T769" id="Seg_9323" s="T768">pers</ta>
            <ta e="T770" id="Seg_9324" s="T769">refl-n:case.poss</ta>
            <ta e="T771" id="Seg_9325" s="T770">v-v:tense-v:pn</ta>
            <ta e="T772" id="Seg_9326" s="T771">n-n:case</ta>
            <ta e="T773" id="Seg_9327" s="T772">v-v:tense-v:pn</ta>
            <ta e="T774" id="Seg_9328" s="T773">n-n:case</ta>
            <ta e="T775" id="Seg_9329" s="T774">v-v:tense-v:pn</ta>
            <ta e="T776" id="Seg_9330" s="T775">n-n:case</ta>
            <ta e="T777" id="Seg_9331" s="T776">adv</ta>
            <ta e="T779" id="Seg_9332" s="T778">conj</ta>
            <ta e="T780" id="Seg_9333" s="T779">pers</ta>
            <ta e="T781" id="Seg_9334" s="T780">ptcl</ta>
            <ta e="T782" id="Seg_9335" s="T781">n-n:case</ta>
            <ta e="T783" id="Seg_9336" s="T782">v-v:tense-v:pn</ta>
            <ta e="T784" id="Seg_9337" s="T783">n-n&gt;v-v:n.fin</ta>
            <ta e="T785" id="Seg_9338" s="T784">v-v:tense-v:pn</ta>
            <ta e="T786" id="Seg_9339" s="T785">n-n:case</ta>
            <ta e="T787" id="Seg_9340" s="T786">v-v:mood.pn</ta>
            <ta e="T788" id="Seg_9341" s="T787">v-v:tense-v:pn</ta>
            <ta e="T789" id="Seg_9342" s="T788">v-v:tense-v:pn</ta>
            <ta e="T790" id="Seg_9343" s="T789">adv</ta>
            <ta e="T791" id="Seg_9344" s="T790">v-v:tense-v:pn</ta>
            <ta e="T792" id="Seg_9345" s="T791">v-v:n.fin</ta>
            <ta e="T793" id="Seg_9346" s="T792">conj</ta>
            <ta e="T794" id="Seg_9347" s="T793">v-v&gt;v-v:pn</ta>
            <ta e="T795" id="Seg_9348" s="T794">dempro-n:case</ta>
            <ta e="T796" id="Seg_9349" s="T795">propr-n:case</ta>
            <ta e="T797" id="Seg_9350" s="T796">pers</ta>
            <ta e="T798" id="Seg_9351" s="T797">ptcl</ta>
            <ta e="T799" id="Seg_9352" s="T798">v-v:tense-v:pn</ta>
            <ta e="T800" id="Seg_9353" s="T799">n-n:case.poss</ta>
            <ta e="T801" id="Seg_9354" s="T800">num-n:case</ta>
            <ta e="T802" id="Seg_9355" s="T801">n-n:case</ta>
            <ta e="T804" id="Seg_9356" s="T803">v-v:tense-v:pn</ta>
            <ta e="T805" id="Seg_9357" s="T804">conj</ta>
            <ta e="T806" id="Seg_9358" s="T805">pers</ta>
            <ta e="T807" id="Seg_9359" s="T806">v-v:tense-v:pn</ta>
            <ta e="T808" id="Seg_9360" s="T807">num-n:case</ta>
            <ta e="T809" id="Seg_9361" s="T808">n-n:case</ta>
            <ta e="T810" id="Seg_9362" s="T809">num-n:case</ta>
            <ta e="T811" id="Seg_9363" s="T810">num-n:case</ta>
            <ta e="T812" id="Seg_9364" s="T811">num-n:case</ta>
            <ta e="T813" id="Seg_9365" s="T812">dempro-n:case</ta>
            <ta e="T815" id="Seg_9366" s="T814">v-v:n.fin</ta>
            <ta e="T816" id="Seg_9367" s="T815">pers</ta>
            <ta e="T817" id="Seg_9368" s="T816">num-n:case</ta>
            <ta e="T818" id="Seg_9369" s="T817">n-n:case</ta>
            <ta e="T819" id="Seg_9370" s="T818">conj</ta>
            <ta e="T820" id="Seg_9371" s="T819">num-n:case</ta>
            <ta e="T821" id="Seg_9372" s="T820">num-n:case</ta>
            <ta e="T822" id="Seg_9373" s="T821">n-n:case.poss</ta>
            <ta e="T823" id="Seg_9374" s="T822">pers</ta>
            <ta e="T824" id="Seg_9375" s="T823">adv</ta>
            <ta e="T825" id="Seg_9376" s="T824">n-n:case</ta>
            <ta e="T826" id="Seg_9377" s="T825">v-v:tense-v:pn</ta>
            <ta e="T827" id="Seg_9378" s="T826">conj</ta>
            <ta e="T828" id="Seg_9379" s="T827">adv</ta>
            <ta e="T829" id="Seg_9380" s="T828">adv</ta>
            <ta e="T830" id="Seg_9381" s="T829">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T831" id="Seg_9382" s="T830">pers</ta>
            <ta e="T832" id="Seg_9383" s="T831">v-v:tense-v:pn</ta>
            <ta e="T833" id="Seg_9384" s="T832">n-n:case</ta>
            <ta e="T834" id="Seg_9385" s="T833">v-v:tense-v:pn</ta>
            <ta e="T835" id="Seg_9386" s="T834">n-n:case</ta>
            <ta e="T836" id="Seg_9387" s="T835">v-v:tense-v:pn</ta>
            <ta e="T837" id="Seg_9388" s="T836">v-v:tense-v:pn</ta>
            <ta e="T838" id="Seg_9389" s="T837">ptcl</ta>
            <ta e="T839" id="Seg_9390" s="T838">n-n:case</ta>
            <ta e="T842" id="Seg_9391" s="T841">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T843" id="Seg_9392" s="T842">adv</ta>
            <ta e="T844" id="Seg_9393" s="T843">adj-n:case</ta>
            <ta e="T845" id="Seg_9394" s="T844">n-n:case</ta>
            <ta e="T846" id="Seg_9395" s="T845">v-v:tense-v:pn</ta>
            <ta e="T847" id="Seg_9396" s="T846">n-n:case.poss</ta>
            <ta e="T848" id="Seg_9397" s="T847">v-v:tense-v:pn</ta>
            <ta e="T849" id="Seg_9398" s="T848">pers</ta>
            <ta e="T850" id="Seg_9399" s="T849">v-v:tense-v:pn</ta>
            <ta e="T851" id="Seg_9400" s="T850">conj</ta>
            <ta e="T852" id="Seg_9401" s="T851">n-n:case</ta>
            <ta e="T853" id="Seg_9402" s="T852">v-v:tense-v:pn</ta>
            <ta e="T854" id="Seg_9403" s="T853">n-n:case</ta>
            <ta e="T858" id="Seg_9404" s="T857">v-v:tense-v:pn</ta>
            <ta e="T859" id="Seg_9405" s="T858">v-v:tense-v:pn</ta>
            <ta e="T860" id="Seg_9406" s="T859">conj</ta>
            <ta e="T861" id="Seg_9407" s="T860">%%</ta>
            <ta e="T862" id="Seg_9408" s="T861">adv</ta>
            <ta e="T863" id="Seg_9409" s="T862">v-v:tense-v:pn</ta>
            <ta e="T864" id="Seg_9410" s="T863">conj</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_9411" s="T0">pers</ta>
            <ta e="T2" id="Seg_9412" s="T1">ptcl</ta>
            <ta e="T3" id="Seg_9413" s="T2">propr</ta>
            <ta e="T4" id="Seg_9414" s="T3">v</ta>
            <ta e="T7" id="Seg_9415" s="T6">adj</ta>
            <ta e="T8" id="Seg_9416" s="T7">adj</ta>
            <ta e="T9" id="Seg_9417" s="T8">n</ta>
            <ta e="T10" id="Seg_9418" s="T9">v</ta>
            <ta e="T11" id="Seg_9419" s="T10">adv</ta>
            <ta e="T12" id="Seg_9420" s="T11">v</ta>
            <ta e="T13" id="Seg_9421" s="T12">n</ta>
            <ta e="T14" id="Seg_9422" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_9423" s="T14">v</ta>
            <ta e="T17" id="Seg_9424" s="T16">v</ta>
            <ta e="T18" id="Seg_9425" s="T17">n</ta>
            <ta e="T19" id="Seg_9426" s="T18">ptcl</ta>
            <ta e="T22" id="Seg_9427" s="T21">v</ta>
            <ta e="T23" id="Seg_9428" s="T22">adv</ta>
            <ta e="T24" id="Seg_9429" s="T23">v</ta>
            <ta e="T25" id="Seg_9430" s="T24">n</ta>
            <ta e="T26" id="Seg_9431" s="T25">adv</ta>
            <ta e="T27" id="Seg_9432" s="T26">v</ta>
            <ta e="T28" id="Seg_9433" s="T27">adv</ta>
            <ta e="T29" id="Seg_9434" s="T28">v</ta>
            <ta e="T30" id="Seg_9435" s="T29">num</ta>
            <ta e="T31" id="Seg_9436" s="T30">n</ta>
            <ta e="T32" id="Seg_9437" s="T31">v</ta>
            <ta e="T33" id="Seg_9438" s="T32">adv</ta>
            <ta e="T34" id="Seg_9439" s="T33">v</ta>
            <ta e="T35" id="Seg_9440" s="T34">n</ta>
            <ta e="T36" id="Seg_9441" s="T35">n</ta>
            <ta e="T37" id="Seg_9442" s="T36">v</ta>
            <ta e="T38" id="Seg_9443" s="T37">propr</ta>
            <ta e="T39" id="Seg_9444" s="T38">adv</ta>
            <ta e="T40" id="Seg_9445" s="T39">propr</ta>
            <ta e="T41" id="Seg_9446" s="T40">v</ta>
            <ta e="T43" id="Seg_9447" s="T42">v</ta>
            <ta e="T44" id="Seg_9448" s="T43">n</ta>
            <ta e="T45" id="Seg_9449" s="T44">v</ta>
            <ta e="T46" id="Seg_9450" s="T45">propr</ta>
            <ta e="T47" id="Seg_9451" s="T46">adv</ta>
            <ta e="T48" id="Seg_9452" s="T47">v</ta>
            <ta e="T51" id="Seg_9453" s="T50">num</ta>
            <ta e="T52" id="Seg_9454" s="T51">n</ta>
            <ta e="T53" id="Seg_9455" s="T52">adv</ta>
            <ta e="T54" id="Seg_9456" s="T53">propr</ta>
            <ta e="T55" id="Seg_9457" s="T54">v</ta>
            <ta e="T56" id="Seg_9458" s="T55">conj</ta>
            <ta e="T57" id="Seg_9459" s="T56">pers</ta>
            <ta e="T58" id="Seg_9460" s="T57">v</ta>
            <ta e="T59" id="Seg_9461" s="T58">pers</ta>
            <ta e="T60" id="Seg_9462" s="T59">v</ta>
            <ta e="T61" id="Seg_9463" s="T60">v</ta>
            <ta e="T62" id="Seg_9464" s="T61">adv</ta>
            <ta e="T63" id="Seg_9465" s="T62">pers</ta>
            <ta e="T64" id="Seg_9466" s="T63">pers</ta>
            <ta e="T65" id="Seg_9467" s="T64">pers</ta>
            <ta e="T67" id="Seg_9468" s="T66">v</ta>
            <ta e="T68" id="Seg_9469" s="T67">v</ta>
            <ta e="T69" id="Seg_9470" s="T68">v</ta>
            <ta e="T71" id="Seg_9471" s="T70">v</ta>
            <ta e="T72" id="Seg_9472" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_9473" s="T72">adv</ta>
            <ta e="T74" id="Seg_9474" s="T73">pers</ta>
            <ta e="T75" id="Seg_9475" s="T74">v</ta>
            <ta e="T76" id="Seg_9476" s="T75">v</ta>
            <ta e="T77" id="Seg_9477" s="T76">v</ta>
            <ta e="T78" id="Seg_9478" s="T77">refl</ta>
            <ta e="T79" id="Seg_9479" s="T78">n</ta>
            <ta e="T80" id="Seg_9480" s="T79">num</ta>
            <ta e="T81" id="Seg_9481" s="T80">num</ta>
            <ta e="T82" id="Seg_9482" s="T81">n</ta>
            <ta e="T83" id="Seg_9483" s="T82">pers</ta>
            <ta e="T84" id="Seg_9484" s="T83">v</ta>
            <ta e="T85" id="Seg_9485" s="T84">num</ta>
            <ta e="T86" id="Seg_9486" s="T85">n</ta>
            <ta e="T87" id="Seg_9487" s="T86">propr</ta>
            <ta e="T88" id="Seg_9488" s="T87">adv</ta>
            <ta e="T89" id="Seg_9489" s="T88">adv</ta>
            <ta e="T90" id="Seg_9490" s="T89">n</ta>
            <ta e="T91" id="Seg_9491" s="T90">n</ta>
            <ta e="T93" id="Seg_9492" s="T92">v</ta>
            <ta e="T94" id="Seg_9493" s="T93">v</ta>
            <ta e="T95" id="Seg_9494" s="T94">propr</ta>
            <ta e="T96" id="Seg_9495" s="T95">adv</ta>
            <ta e="T97" id="Seg_9496" s="T96">n</ta>
            <ta e="T98" id="Seg_9497" s="T97">v</ta>
            <ta e="T99" id="Seg_9498" s="T98">adv</ta>
            <ta e="T100" id="Seg_9499" s="T99">n</ta>
            <ta e="T101" id="Seg_9500" s="T100">v</ta>
            <ta e="T102" id="Seg_9501" s="T101">adv</ta>
            <ta e="T103" id="Seg_9502" s="T102">v</ta>
            <ta e="T104" id="Seg_9503" s="T103">adv</ta>
            <ta e="T106" id="Seg_9504" s="T105">adv</ta>
            <ta e="T107" id="Seg_9505" s="T106">propr</ta>
            <ta e="T108" id="Seg_9506" s="T107">adv</ta>
            <ta e="T109" id="Seg_9507" s="T108">adv</ta>
            <ta e="T110" id="Seg_9508" s="T109">v</ta>
            <ta e="T111" id="Seg_9509" s="T110">quant</ta>
            <ta e="T112" id="Seg_9510" s="T111">v</ta>
            <ta e="T113" id="Seg_9511" s="T112">adv</ta>
            <ta e="T115" id="Seg_9512" s="T114">v</ta>
            <ta e="T116" id="Seg_9513" s="T115">n</ta>
            <ta e="T117" id="Seg_9514" s="T116">v</ta>
            <ta e="T118" id="Seg_9515" s="T117">v</ta>
            <ta e="T119" id="Seg_9516" s="T118">conj</ta>
            <ta e="T868" id="Seg_9517" s="T120">v</ta>
            <ta e="T121" id="Seg_9518" s="T868">v</ta>
            <ta e="T122" id="Seg_9519" s="T121">conj</ta>
            <ta e="T123" id="Seg_9520" s="T122">pers</ta>
            <ta e="T124" id="Seg_9521" s="T123">v</ta>
            <ta e="T125" id="Seg_9522" s="T124">conj</ta>
            <ta e="T126" id="Seg_9523" s="T125">v</ta>
            <ta e="T127" id="Seg_9524" s="T126">adv</ta>
            <ta e="T128" id="Seg_9525" s="T127">adv</ta>
            <ta e="T129" id="Seg_9526" s="T128">n</ta>
            <ta e="T130" id="Seg_9527" s="T129">v</ta>
            <ta e="T131" id="Seg_9528" s="T130">v</ta>
            <ta e="T132" id="Seg_9529" s="T131">pers</ta>
            <ta e="T133" id="Seg_9530" s="T132">conj</ta>
            <ta e="T134" id="Seg_9531" s="T133">propr</ta>
            <ta e="T135" id="Seg_9532" s="T134">v</ta>
            <ta e="T136" id="Seg_9533" s="T135">adv</ta>
            <ta e="T139" id="Seg_9534" s="T137">pers</ta>
            <ta e="T140" id="Seg_9535" s="T139">adv</ta>
            <ta e="T141" id="Seg_9536" s="T140">pers</ta>
            <ta e="T142" id="Seg_9537" s="T141">adv</ta>
            <ta e="T143" id="Seg_9538" s="T142">v</ta>
            <ta e="T144" id="Seg_9539" s="T143">conj</ta>
            <ta e="T146" id="Seg_9540" s="T145">pers</ta>
            <ta e="T147" id="Seg_9541" s="T146">v</ta>
            <ta e="T148" id="Seg_9542" s="T147">que</ta>
            <ta e="T149" id="Seg_9543" s="T148">v</ta>
            <ta e="T150" id="Seg_9544" s="T149">v</ta>
            <ta e="T151" id="Seg_9545" s="T150">adv</ta>
            <ta e="T152" id="Seg_9546" s="T151">adv</ta>
            <ta e="T153" id="Seg_9547" s="T152">v</ta>
            <ta e="T154" id="Seg_9548" s="T153">v</ta>
            <ta e="T155" id="Seg_9549" s="T154">n</ta>
            <ta e="T156" id="Seg_9550" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_9551" s="T156">v</ta>
            <ta e="T158" id="Seg_9552" s="T157">v</ta>
            <ta e="T159" id="Seg_9553" s="T158">pers</ta>
            <ta e="T162" id="Seg_9554" s="T161">v</ta>
            <ta e="T163" id="Seg_9555" s="T162">adv</ta>
            <ta e="T164" id="Seg_9556" s="T163">adv</ta>
            <ta e="T165" id="Seg_9557" s="T164">pers</ta>
            <ta e="T166" id="Seg_9558" s="T165">v</ta>
            <ta e="T167" id="Seg_9559" s="T166">conj</ta>
            <ta e="T168" id="Seg_9560" s="T167">v</ta>
            <ta e="T169" id="Seg_9561" s="T168">propr</ta>
            <ta e="T170" id="Seg_9562" s="T169">adv</ta>
            <ta e="T171" id="Seg_9563" s="T170">adv</ta>
            <ta e="T172" id="Seg_9564" s="T171">adv</ta>
            <ta e="T173" id="Seg_9565" s="T172">num</ta>
            <ta e="T175" id="Seg_9566" s="T174">v</ta>
            <ta e="T176" id="Seg_9567" s="T175">propr</ta>
            <ta e="T177" id="Seg_9568" s="T176">v</ta>
            <ta e="T178" id="Seg_9569" s="T177">num</ta>
            <ta e="T179" id="Seg_9570" s="T178">n</ta>
            <ta e="T180" id="Seg_9571" s="T179">v</ta>
            <ta e="T181" id="Seg_9572" s="T180">n</ta>
            <ta e="T182" id="Seg_9573" s="T181">conj</ta>
            <ta e="T183" id="Seg_9574" s="T182">pers</ta>
            <ta e="T184" id="Seg_9575" s="T183">v</ta>
            <ta e="T185" id="Seg_9576" s="T184">adv</ta>
            <ta e="T186" id="Seg_9577" s="T185">propr</ta>
            <ta e="T187" id="Seg_9578" s="T186">v</ta>
            <ta e="T188" id="Seg_9579" s="T187">v</ta>
            <ta e="T189" id="Seg_9580" s="T188">pers</ta>
            <ta e="T190" id="Seg_9581" s="T189">n</ta>
            <ta e="T191" id="Seg_9582" s="T190">v</ta>
            <ta e="T192" id="Seg_9583" s="T191">n</ta>
            <ta e="T193" id="Seg_9584" s="T192">dempro</ta>
            <ta e="T194" id="Seg_9585" s="T193">v</ta>
            <ta e="T196" id="Seg_9586" s="T195">v</ta>
            <ta e="T197" id="Seg_9587" s="T196">n</ta>
            <ta e="T199" id="Seg_9588" s="T198">adv</ta>
            <ta e="T200" id="Seg_9589" s="T199">v</ta>
            <ta e="T201" id="Seg_9590" s="T200">pers</ta>
            <ta e="T202" id="Seg_9591" s="T201">quant</ta>
            <ta e="T203" id="Seg_9592" s="T202">ptcl</ta>
            <ta e="T204" id="Seg_9593" s="T203">adv</ta>
            <ta e="T205" id="Seg_9594" s="T204">num</ta>
            <ta e="T206" id="Seg_9595" s="T205">n</ta>
            <ta e="T207" id="Seg_9596" s="T206">v</ta>
            <ta e="T208" id="Seg_9597" s="T207">dempro</ta>
            <ta e="T209" id="Seg_9598" s="T208">pers</ta>
            <ta e="T210" id="Seg_9599" s="T209">num</ta>
            <ta e="T211" id="Seg_9600" s="T210">n</ta>
            <ta e="T212" id="Seg_9601" s="T211">v</ta>
            <ta e="T213" id="Seg_9602" s="T212">pers</ta>
            <ta e="T214" id="Seg_9603" s="T213">v</ta>
            <ta e="T215" id="Seg_9604" s="T214">n</ta>
            <ta e="T216" id="Seg_9605" s="T215">v</ta>
            <ta e="T217" id="Seg_9606" s="T216">v</ta>
            <ta e="T218" id="Seg_9607" s="T217">conj</ta>
            <ta e="T219" id="Seg_9608" s="T218">v</ta>
            <ta e="T220" id="Seg_9609" s="T219">n</ta>
            <ta e="T221" id="Seg_9610" s="T220">v</ta>
            <ta e="T222" id="Seg_9611" s="T221">adv</ta>
            <ta e="T223" id="Seg_9612" s="T222">n</ta>
            <ta e="T224" id="Seg_9613" s="T223">v</ta>
            <ta e="T225" id="Seg_9614" s="T224">pers</ta>
            <ta e="T227" id="Seg_9615" s="T226">v</ta>
            <ta e="T228" id="Seg_9616" s="T227">adv</ta>
            <ta e="T229" id="Seg_9617" s="T228">v</ta>
            <ta e="T230" id="Seg_9618" s="T229">num</ta>
            <ta e="T231" id="Seg_9619" s="T230">n</ta>
            <ta e="T232" id="Seg_9620" s="T231">v</ta>
            <ta e="T233" id="Seg_9621" s="T232">n</ta>
            <ta e="T234" id="Seg_9622" s="T233">pers</ta>
            <ta e="T235" id="Seg_9623" s="T234">adv</ta>
            <ta e="T237" id="Seg_9624" s="T236">v</ta>
            <ta e="T238" id="Seg_9625" s="T237">v</ta>
            <ta e="T239" id="Seg_9626" s="T238">adv</ta>
            <ta e="T240" id="Seg_9627" s="T239">ptcl</ta>
            <ta e="T241" id="Seg_9628" s="T240">pers</ta>
            <ta e="T242" id="Seg_9629" s="T241">v</ta>
            <ta e="T243" id="Seg_9630" s="T242">n</ta>
            <ta e="T244" id="Seg_9631" s="T243">n</ta>
            <ta e="T245" id="Seg_9632" s="T244">v</ta>
            <ta e="T246" id="Seg_9633" s="T245">adv</ta>
            <ta e="T247" id="Seg_9634" s="T246">n</ta>
            <ta e="T248" id="Seg_9635" s="T247">v</ta>
            <ta e="T251" id="Seg_9636" s="T250">v</ta>
            <ta e="T252" id="Seg_9637" s="T251">conj</ta>
            <ta e="T253" id="Seg_9638" s="T252">propr</ta>
            <ta e="T254" id="Seg_9639" s="T253">v</ta>
            <ta e="T255" id="Seg_9640" s="T254">adv</ta>
            <ta e="T258" id="Seg_9641" s="T257">v</ta>
            <ta e="T259" id="Seg_9642" s="T258">n</ta>
            <ta e="T260" id="Seg_9643" s="T259">n</ta>
            <ta e="T261" id="Seg_9644" s="T260">propr</ta>
            <ta e="T262" id="Seg_9645" s="T261">n</ta>
            <ta e="T263" id="Seg_9646" s="T262">v</ta>
            <ta e="T264" id="Seg_9647" s="T263">v</ta>
            <ta e="T265" id="Seg_9648" s="T264">dempro</ta>
            <ta e="T266" id="Seg_9649" s="T265">pers</ta>
            <ta e="T267" id="Seg_9650" s="T266">adv</ta>
            <ta e="T268" id="Seg_9651" s="T267">v</ta>
            <ta e="T269" id="Seg_9652" s="T268">adv</ta>
            <ta e="T270" id="Seg_9653" s="T269">v</ta>
            <ta e="T271" id="Seg_9654" s="T270">pers</ta>
            <ta e="T272" id="Seg_9655" s="T271">n</ta>
            <ta e="T273" id="Seg_9656" s="T272">v</ta>
            <ta e="T274" id="Seg_9657" s="T273">pers</ta>
            <ta e="T275" id="Seg_9658" s="T274">v</ta>
            <ta e="T276" id="Seg_9659" s="T275">que</ta>
            <ta e="T277" id="Seg_9660" s="T276">ptcl</ta>
            <ta e="T278" id="Seg_9661" s="T277">v</ta>
            <ta e="T279" id="Seg_9662" s="T278">adv</ta>
            <ta e="T280" id="Seg_9663" s="T279">v</ta>
            <ta e="T281" id="Seg_9664" s="T280">adv</ta>
            <ta e="T282" id="Seg_9665" s="T281">adv</ta>
            <ta e="T283" id="Seg_9666" s="T282">adv</ta>
            <ta e="T284" id="Seg_9667" s="T283">n</ta>
            <ta e="T285" id="Seg_9668" s="T284">v</ta>
            <ta e="T286" id="Seg_9669" s="T285">pers</ta>
            <ta e="T287" id="Seg_9670" s="T286">v</ta>
            <ta e="T288" id="Seg_9671" s="T287">propr</ta>
            <ta e="T289" id="Seg_9672" s="T288">v</ta>
            <ta e="T290" id="Seg_9673" s="T289">adv</ta>
            <ta e="T291" id="Seg_9674" s="T290">adv</ta>
            <ta e="T292" id="Seg_9675" s="T291">n</ta>
            <ta e="T293" id="Seg_9676" s="T292">v</ta>
            <ta e="T294" id="Seg_9677" s="T293">adv</ta>
            <ta e="T295" id="Seg_9678" s="T294">adv</ta>
            <ta e="T296" id="Seg_9679" s="T295">n</ta>
            <ta e="T297" id="Seg_9680" s="T296">v</ta>
            <ta e="T298" id="Seg_9681" s="T297">conj</ta>
            <ta e="T299" id="Seg_9682" s="T298">n</ta>
            <ta e="T300" id="Seg_9683" s="T299">v</ta>
            <ta e="T301" id="Seg_9684" s="T300">conj</ta>
            <ta e="T302" id="Seg_9685" s="T301">propr</ta>
            <ta e="T303" id="Seg_9686" s="T302">v</ta>
            <ta e="T304" id="Seg_9687" s="T303">pers</ta>
            <ta e="T306" id="Seg_9688" s="T305">v</ta>
            <ta e="T307" id="Seg_9689" s="T306">v</ta>
            <ta e="T308" id="Seg_9690" s="T307">conj</ta>
            <ta e="T309" id="Seg_9691" s="T308">pers</ta>
            <ta e="T310" id="Seg_9692" s="T309">n</ta>
            <ta e="T311" id="Seg_9693" s="T310">n</ta>
            <ta e="T312" id="Seg_9694" s="T311">v</ta>
            <ta e="T313" id="Seg_9695" s="T312">conj</ta>
            <ta e="T314" id="Seg_9696" s="T313">v</ta>
            <ta e="T315" id="Seg_9697" s="T314">pers</ta>
            <ta e="T316" id="Seg_9698" s="T315">adv</ta>
            <ta e="T317" id="Seg_9699" s="T316">n</ta>
            <ta e="T318" id="Seg_9700" s="T317">v</ta>
            <ta e="T319" id="Seg_9701" s="T318">conj</ta>
            <ta e="T320" id="Seg_9702" s="T319">pers</ta>
            <ta e="T321" id="Seg_9703" s="T320">n</ta>
            <ta e="T322" id="Seg_9704" s="T321">v</ta>
            <ta e="T323" id="Seg_9705" s="T322">adv</ta>
            <ta e="T324" id="Seg_9706" s="T323">dempro</ta>
            <ta e="T325" id="Seg_9707" s="T324">n</ta>
            <ta e="T326" id="Seg_9708" s="T325">v</ta>
            <ta e="T327" id="Seg_9709" s="T326">propr</ta>
            <ta e="T329" id="Seg_9710" s="T328">n</ta>
            <ta e="T330" id="Seg_9711" s="T329">v</ta>
            <ta e="T331" id="Seg_9712" s="T330">adv</ta>
            <ta e="T334" id="Seg_9713" s="T333">v</ta>
            <ta e="T335" id="Seg_9714" s="T334">adv</ta>
            <ta e="T336" id="Seg_9715" s="T335">num</ta>
            <ta e="T337" id="Seg_9716" s="T336">n</ta>
            <ta e="T338" id="Seg_9717" s="T337">adj</ta>
            <ta e="T339" id="Seg_9718" s="T338">ptcl</ta>
            <ta e="T340" id="Seg_9719" s="T339">v</ta>
            <ta e="T341" id="Seg_9720" s="T340">ptcl</ta>
            <ta e="T342" id="Seg_9721" s="T341">adv</ta>
            <ta e="T343" id="Seg_9722" s="T342">dempro</ta>
            <ta e="T344" id="Seg_9723" s="T343">v</ta>
            <ta e="T345" id="Seg_9724" s="T344">n</ta>
            <ta e="T346" id="Seg_9725" s="T345">propr</ta>
            <ta e="T347" id="Seg_9726" s="T346">conj</ta>
            <ta e="T348" id="Seg_9727" s="T347">n</ta>
            <ta e="T349" id="Seg_9728" s="T348">v</ta>
            <ta e="T350" id="Seg_9729" s="T349">conj</ta>
            <ta e="T351" id="Seg_9730" s="T350">dempro</ta>
            <ta e="T352" id="Seg_9731" s="T351">v</ta>
            <ta e="T353" id="Seg_9732" s="T352">n</ta>
            <ta e="T354" id="Seg_9733" s="T353">adv</ta>
            <ta e="T355" id="Seg_9734" s="T354">n</ta>
            <ta e="T356" id="Seg_9735" s="T355">ptcl</ta>
            <ta e="T357" id="Seg_9736" s="T356">n</ta>
            <ta e="T358" id="Seg_9737" s="T357">v</ta>
            <ta e="T359" id="Seg_9738" s="T358">conj</ta>
            <ta e="T360" id="Seg_9739" s="T359">v</ta>
            <ta e="T361" id="Seg_9740" s="T360">v</ta>
            <ta e="T362" id="Seg_9741" s="T361">ptcl</ta>
            <ta e="T363" id="Seg_9742" s="T362">dempro</ta>
            <ta e="T364" id="Seg_9743" s="T363">v</ta>
            <ta e="T365" id="Seg_9744" s="T364">ptcl</ta>
            <ta e="T367" id="Seg_9745" s="T366">ptcl</ta>
            <ta e="T368" id="Seg_9746" s="T367">v</ta>
            <ta e="T369" id="Seg_9747" s="T368">pers</ta>
            <ta e="T372" id="Seg_9748" s="T371">v</ta>
            <ta e="T373" id="Seg_9749" s="T372">v</ta>
            <ta e="T375" id="Seg_9750" s="T374">adv</ta>
            <ta e="T376" id="Seg_9751" s="T375">v</ta>
            <ta e="T377" id="Seg_9752" s="T376">n</ta>
            <ta e="T378" id="Seg_9753" s="T377">adv</ta>
            <ta e="T380" id="Seg_9754" s="T379">v</ta>
            <ta e="T381" id="Seg_9755" s="T380">que</ta>
            <ta e="T383" id="Seg_9756" s="T381">pers</ta>
            <ta e="T384" id="Seg_9757" s="T383">v</ta>
            <ta e="T385" id="Seg_9758" s="T384">que</ta>
            <ta e="T387" id="Seg_9759" s="T385">pers</ta>
            <ta e="T388" id="Seg_9760" s="T387">ptcl</ta>
            <ta e="T389" id="Seg_9761" s="T388">v</ta>
            <ta e="T390" id="Seg_9762" s="T389">que</ta>
            <ta e="T391" id="Seg_9763" s="T390">v</ta>
            <ta e="T392" id="Seg_9764" s="T391">n</ta>
            <ta e="T393" id="Seg_9765" s="T392">conj</ta>
            <ta e="T394" id="Seg_9766" s="T393">pers</ta>
            <ta e="T395" id="Seg_9767" s="T394">v</ta>
            <ta e="T396" id="Seg_9768" s="T395">v</ta>
            <ta e="T397" id="Seg_9769" s="T396">adv</ta>
            <ta e="T398" id="Seg_9770" s="T397">n</ta>
            <ta e="T399" id="Seg_9771" s="T398">v</ta>
            <ta e="T400" id="Seg_9772" s="T399">conj</ta>
            <ta e="T401" id="Seg_9773" s="T400">dempro</ta>
            <ta e="T402" id="Seg_9774" s="T401">que</ta>
            <ta e="T403" id="Seg_9775" s="T402">v</ta>
            <ta e="T404" id="Seg_9776" s="T403">adv</ta>
            <ta e="T406" id="Seg_9777" s="T405">adv</ta>
            <ta e="T407" id="Seg_9778" s="T406">n</ta>
            <ta e="T408" id="Seg_9779" s="T407">v</ta>
            <ta e="T409" id="Seg_9780" s="T408">que</ta>
            <ta e="T869" id="Seg_9781" s="T409">v</ta>
            <ta e="T410" id="Seg_9782" s="T869">v</ta>
            <ta e="T411" id="Seg_9783" s="T410">ptcl</ta>
            <ta e="T412" id="Seg_9784" s="T411">v</ta>
            <ta e="T413" id="Seg_9785" s="T412">propr</ta>
            <ta e="T416" id="Seg_9786" s="T415">ptcl</ta>
            <ta e="T417" id="Seg_9787" s="T416">n</ta>
            <ta e="T418" id="Seg_9788" s="T417">n</ta>
            <ta e="T419" id="Seg_9789" s="T418">v</ta>
            <ta e="T420" id="Seg_9790" s="T419">adv</ta>
            <ta e="T421" id="Seg_9791" s="T420">que</ta>
            <ta e="T422" id="Seg_9792" s="T421">n</ta>
            <ta e="T423" id="Seg_9793" s="T422">v</ta>
            <ta e="T424" id="Seg_9794" s="T423">n</ta>
            <ta e="T426" id="Seg_9795" s="T425">conj</ta>
            <ta e="T427" id="Seg_9796" s="T426">n</ta>
            <ta e="T428" id="Seg_9797" s="T427">adv</ta>
            <ta e="T429" id="Seg_9798" s="T428">adj</ta>
            <ta e="T430" id="Seg_9799" s="T429">dempro</ta>
            <ta e="T431" id="Seg_9800" s="T430">ptcl</ta>
            <ta e="T432" id="Seg_9801" s="T431">v</ta>
            <ta e="T433" id="Seg_9802" s="T432">n</ta>
            <ta e="T434" id="Seg_9803" s="T433">v</ta>
            <ta e="T435" id="Seg_9804" s="T434">conj</ta>
            <ta e="T436" id="Seg_9805" s="T435">ptcl</ta>
            <ta e="T437" id="Seg_9806" s="T436">v</ta>
            <ta e="T439" id="Seg_9807" s="T437">adv</ta>
            <ta e="T440" id="Seg_9808" s="T439">adv</ta>
            <ta e="T441" id="Seg_9809" s="T440">adv</ta>
            <ta e="T442" id="Seg_9810" s="T441">v</ta>
            <ta e="T443" id="Seg_9811" s="T442">v</ta>
            <ta e="T445" id="Seg_9812" s="T444">v</ta>
            <ta e="T446" id="Seg_9813" s="T445">conj</ta>
            <ta e="T448" id="Seg_9814" s="T447">v</ta>
            <ta e="T450" id="Seg_9815" s="T449">propr</ta>
            <ta e="T451" id="Seg_9816" s="T450">n</ta>
            <ta e="T452" id="Seg_9817" s="T451">adv</ta>
            <ta e="T453" id="Seg_9818" s="T452">v</ta>
            <ta e="T454" id="Seg_9819" s="T453">conj</ta>
            <ta e="T456" id="Seg_9820" s="T455">conj</ta>
            <ta e="T459" id="Seg_9821" s="T458">n</ta>
            <ta e="T460" id="Seg_9822" s="T459">adv</ta>
            <ta e="T461" id="Seg_9823" s="T460">v</ta>
            <ta e="T466" id="Seg_9824" s="T465">n</ta>
            <ta e="T467" id="Seg_9825" s="T466">adv</ta>
            <ta e="T468" id="Seg_9826" s="T467">v</ta>
            <ta e="T471" id="Seg_9827" s="T470">n</ta>
            <ta e="T472" id="Seg_9828" s="T471">adv</ta>
            <ta e="T473" id="Seg_9829" s="T472">v</ta>
            <ta e="T476" id="Seg_9830" s="T475">n</ta>
            <ta e="T477" id="Seg_9831" s="T476">adv</ta>
            <ta e="T478" id="Seg_9832" s="T477">v</ta>
            <ta e="T479" id="Seg_9833" s="T478">conj</ta>
            <ta e="T481" id="Seg_9834" s="T480">propr</ta>
            <ta e="T482" id="Seg_9835" s="T481">adv</ta>
            <ta e="T483" id="Seg_9836" s="T482">n</ta>
            <ta e="T484" id="Seg_9837" s="T483">v</ta>
            <ta e="T485" id="Seg_9838" s="T484">conj</ta>
            <ta e="T489" id="Seg_9839" s="T488">adv</ta>
            <ta e="T490" id="Seg_9840" s="T489">n</ta>
            <ta e="T491" id="Seg_9841" s="T490">v</ta>
            <ta e="T492" id="Seg_9842" s="T491">conj</ta>
            <ta e="T494" id="Seg_9843" s="T493">v</ta>
            <ta e="T495" id="Seg_9844" s="T494">v</ta>
            <ta e="T496" id="Seg_9845" s="T495">adv</ta>
            <ta e="T497" id="Seg_9846" s="T496">adv</ta>
            <ta e="T499" id="Seg_9847" s="T498">num</ta>
            <ta e="T500" id="Seg_9848" s="T499">num</ta>
            <ta e="T501" id="Seg_9849" s="T500">n</ta>
            <ta e="T502" id="Seg_9850" s="T501">v</ta>
            <ta e="T506" id="Seg_9851" s="T505">v</ta>
            <ta e="T507" id="Seg_9852" s="T506">dempro</ta>
            <ta e="T508" id="Seg_9853" s="T507">v</ta>
            <ta e="T509" id="Seg_9854" s="T508">propr</ta>
            <ta e="T510" id="Seg_9855" s="T509">adv</ta>
            <ta e="T511" id="Seg_9856" s="T510">n</ta>
            <ta e="T512" id="Seg_9857" s="T511">v</ta>
            <ta e="T513" id="Seg_9858" s="T512">adv</ta>
            <ta e="T515" id="Seg_9859" s="T514">v</ta>
            <ta e="T516" id="Seg_9860" s="T515">num</ta>
            <ta e="T518" id="Seg_9861" s="T517">conj</ta>
            <ta e="T519" id="Seg_9862" s="T518">n</ta>
            <ta e="T520" id="Seg_9863" s="T519">v</ta>
            <ta e="T521" id="Seg_9864" s="T520">adv</ta>
            <ta e="T522" id="Seg_9865" s="T521">v</ta>
            <ta e="T523" id="Seg_9866" s="T522">n</ta>
            <ta e="T524" id="Seg_9867" s="T523">adv</ta>
            <ta e="T525" id="Seg_9868" s="T524">v</ta>
            <ta e="T526" id="Seg_9869" s="T525">conj</ta>
            <ta e="T527" id="Seg_9870" s="T526">v</ta>
            <ta e="T528" id="Seg_9871" s="T527">v</ta>
            <ta e="T529" id="Seg_9872" s="T528">n</ta>
            <ta e="T530" id="Seg_9873" s="T529">v</ta>
            <ta e="T531" id="Seg_9874" s="T530">n</ta>
            <ta e="T532" id="Seg_9875" s="T531">conj</ta>
            <ta e="T533" id="Seg_9876" s="T532">pers</ta>
            <ta e="T534" id="Seg_9877" s="T533">n</ta>
            <ta e="T535" id="Seg_9878" s="T534">v</ta>
            <ta e="T536" id="Seg_9879" s="T535">conj</ta>
            <ta e="T537" id="Seg_9880" s="T536">v</ta>
            <ta e="T538" id="Seg_9881" s="T537">conj</ta>
            <ta e="T539" id="Seg_9882" s="T538">v</ta>
            <ta e="T540" id="Seg_9883" s="T539">pers</ta>
            <ta e="T541" id="Seg_9884" s="T540">n</ta>
            <ta e="T542" id="Seg_9885" s="T541">v</ta>
            <ta e="T543" id="Seg_9886" s="T542">adv</ta>
            <ta e="T544" id="Seg_9887" s="T543">v</ta>
            <ta e="T545" id="Seg_9888" s="T544">adv</ta>
            <ta e="T546" id="Seg_9889" s="T545">n</ta>
            <ta e="T547" id="Seg_9890" s="T546">v</ta>
            <ta e="T548" id="Seg_9891" s="T547">dempro</ta>
            <ta e="T549" id="Seg_9892" s="T548">adj</ta>
            <ta e="T550" id="Seg_9893" s="T549">v</ta>
            <ta e="T551" id="Seg_9894" s="T550">adv</ta>
            <ta e="T552" id="Seg_9895" s="T551">num</ta>
            <ta e="T554" id="Seg_9896" s="T553">n</ta>
            <ta e="T555" id="Seg_9897" s="T554">ptcl</ta>
            <ta e="T556" id="Seg_9898" s="T555">v</ta>
            <ta e="T557" id="Seg_9899" s="T556">adv</ta>
            <ta e="T558" id="Seg_9900" s="T557">adj</ta>
            <ta e="T559" id="Seg_9901" s="T558">v</ta>
            <ta e="T562" id="Seg_9902" s="T561">pers</ta>
            <ta e="T563" id="Seg_9903" s="T562">num</ta>
            <ta e="T564" id="Seg_9904" s="T563">n</ta>
            <ta e="T565" id="Seg_9905" s="T564">v</ta>
            <ta e="T566" id="Seg_9906" s="T565">v</ta>
            <ta e="T567" id="Seg_9907" s="T566">ptcl</ta>
            <ta e="T568" id="Seg_9908" s="T567">v</ta>
            <ta e="T569" id="Seg_9909" s="T568">conj</ta>
            <ta e="T570" id="Seg_9910" s="T569">adv</ta>
            <ta e="T571" id="Seg_9911" s="T570">v</ta>
            <ta e="T572" id="Seg_9912" s="T571">adj</ta>
            <ta e="T574" id="Seg_9913" s="T573">n</ta>
            <ta e="T575" id="Seg_9914" s="T574">pers</ta>
            <ta e="T576" id="Seg_9915" s="T575">v</ta>
            <ta e="T577" id="Seg_9916" s="T576">num</ta>
            <ta e="T578" id="Seg_9917" s="T577">num</ta>
            <ta e="T579" id="Seg_9918" s="T578">n</ta>
            <ta e="T580" id="Seg_9919" s="T579">refl</ta>
            <ta e="T581" id="Seg_9920" s="T580">n</ta>
            <ta e="T582" id="Seg_9921" s="T581">v</ta>
            <ta e="T583" id="Seg_9922" s="T582">refl</ta>
            <ta e="T584" id="Seg_9923" s="T583">v</ta>
            <ta e="T585" id="Seg_9924" s="T584">refl</ta>
            <ta e="T586" id="Seg_9925" s="T585">v</ta>
            <ta e="T587" id="Seg_9926" s="T586">n</ta>
            <ta e="T588" id="Seg_9927" s="T587">n</ta>
            <ta e="T589" id="Seg_9928" s="T588">v</ta>
            <ta e="T590" id="Seg_9929" s="T589">ptcl</ta>
            <ta e="T591" id="Seg_9930" s="T590">n</ta>
            <ta e="T594" id="Seg_9931" s="T593">v</ta>
            <ta e="T595" id="Seg_9932" s="T594">num</ta>
            <ta e="T596" id="Seg_9933" s="T595">n</ta>
            <ta e="T597" id="Seg_9934" s="T596">v</ta>
            <ta e="T598" id="Seg_9935" s="T597">adv</ta>
            <ta e="T599" id="Seg_9936" s="T598">n</ta>
            <ta e="T600" id="Seg_9937" s="T599">n</ta>
            <ta e="T601" id="Seg_9938" s="T600">v</ta>
            <ta e="T602" id="Seg_9939" s="T601">conj</ta>
            <ta e="T603" id="Seg_9940" s="T602">adv</ta>
            <ta e="T605" id="Seg_9941" s="T604">v</ta>
            <ta e="T606" id="Seg_9942" s="T605">n</ta>
            <ta e="T607" id="Seg_9943" s="T606">v</ta>
            <ta e="T608" id="Seg_9944" s="T607">adv</ta>
            <ta e="T609" id="Seg_9945" s="T608">v</ta>
            <ta e="T610" id="Seg_9946" s="T609">v</ta>
            <ta e="T611" id="Seg_9947" s="T610">pers</ta>
            <ta e="T612" id="Seg_9948" s="T611">n</ta>
            <ta e="T867" id="Seg_9949" s="T613">v</ta>
            <ta e="T615" id="Seg_9950" s="T614">adv</ta>
            <ta e="T616" id="Seg_9951" s="T615">v</ta>
            <ta e="T617" id="Seg_9952" s="T616">v</ta>
            <ta e="T618" id="Seg_9953" s="T617">ptcl</ta>
            <ta e="T619" id="Seg_9954" s="T618">v</ta>
            <ta e="T620" id="Seg_9955" s="T619">dempro</ta>
            <ta e="T621" id="Seg_9956" s="T620">v</ta>
            <ta e="T622" id="Seg_9957" s="T621">adv</ta>
            <ta e="T623" id="Seg_9958" s="T622">n</ta>
            <ta e="T626" id="Seg_9959" s="T625">v</ta>
            <ta e="T627" id="Seg_9960" s="T626">adv</ta>
            <ta e="T628" id="Seg_9961" s="T627">v</ta>
            <ta e="T629" id="Seg_9962" s="T628">conj</ta>
            <ta e="T630" id="Seg_9963" s="T629">que</ta>
            <ta e="T631" id="Seg_9964" s="T630">pers</ta>
            <ta e="T632" id="Seg_9965" s="T631">v</ta>
            <ta e="T633" id="Seg_9966" s="T632">conj</ta>
            <ta e="T634" id="Seg_9967" s="T633">ptcl</ta>
            <ta e="T635" id="Seg_9968" s="T634">ptcl</ta>
            <ta e="T636" id="Seg_9969" s="T866">v</ta>
            <ta e="T638" id="Seg_9970" s="T637">conj</ta>
            <ta e="T639" id="Seg_9971" s="T638">adv</ta>
            <ta e="T640" id="Seg_9972" s="T639">n</ta>
            <ta e="T643" id="Seg_9973" s="T642">adv</ta>
            <ta e="T644" id="Seg_9974" s="T643">n</ta>
            <ta e="T646" id="Seg_9975" s="T645">v</ta>
            <ta e="T647" id="Seg_9976" s="T646">v</ta>
            <ta e="T648" id="Seg_9977" s="T647">n</ta>
            <ta e="T649" id="Seg_9978" s="T648">adv</ta>
            <ta e="T650" id="Seg_9979" s="T649">n</ta>
            <ta e="T652" id="Seg_9980" s="T651">pers</ta>
            <ta e="T653" id="Seg_9981" s="T652">adv</ta>
            <ta e="T654" id="Seg_9982" s="T653">v</ta>
            <ta e="T655" id="Seg_9983" s="T654">adv</ta>
            <ta e="T656" id="Seg_9984" s="T655">n</ta>
            <ta e="T657" id="Seg_9985" s="T656">v</ta>
            <ta e="T658" id="Seg_9986" s="T657">adv</ta>
            <ta e="T659" id="Seg_9987" s="T658">pers</ta>
            <ta e="T660" id="Seg_9988" s="T659">n</ta>
            <ta e="T661" id="Seg_9989" s="T660">ptcl</ta>
            <ta e="T662" id="Seg_9990" s="T661">v</ta>
            <ta e="T663" id="Seg_9991" s="T662">n</ta>
            <ta e="T664" id="Seg_9992" s="T663">adv</ta>
            <ta e="T665" id="Seg_9993" s="T664">v</ta>
            <ta e="T666" id="Seg_9994" s="T665">conj</ta>
            <ta e="T667" id="Seg_9995" s="T666">que</ta>
            <ta e="T668" id="Seg_9996" s="T667">v</ta>
            <ta e="T669" id="Seg_9997" s="T668">n</ta>
            <ta e="T673" id="Seg_9998" s="T672">que</ta>
            <ta e="T674" id="Seg_9999" s="T673">n</ta>
            <ta e="T676" id="Seg_10000" s="T675">n</ta>
            <ta e="T677" id="Seg_10001" s="T676">v</ta>
            <ta e="T679" id="Seg_10002" s="T678">dempro</ta>
            <ta e="T680" id="Seg_10003" s="T679">n</ta>
            <ta e="T681" id="Seg_10004" s="T680">v</ta>
            <ta e="T682" id="Seg_10005" s="T681">ptcl</ta>
            <ta e="T683" id="Seg_10006" s="T682">v</ta>
            <ta e="T684" id="Seg_10007" s="T683">dempro</ta>
            <ta e="T686" id="Seg_10008" s="T685">n</ta>
            <ta e="T687" id="Seg_10009" s="T686">v</ta>
            <ta e="T688" id="Seg_10010" s="T687">adv</ta>
            <ta e="T689" id="Seg_10011" s="T688">ptcl</ta>
            <ta e="T690" id="Seg_10012" s="T689">v</ta>
            <ta e="T691" id="Seg_10013" s="T690">n</ta>
            <ta e="T692" id="Seg_10014" s="T691">v</ta>
            <ta e="T693" id="Seg_10015" s="T692">conj</ta>
            <ta e="T694" id="Seg_10016" s="T693">n</ta>
            <ta e="T695" id="Seg_10017" s="T694">v</ta>
            <ta e="T696" id="Seg_10018" s="T695">conj</ta>
            <ta e="T697" id="Seg_10019" s="T696">adj</ta>
            <ta e="T698" id="Seg_10020" s="T697">v</ta>
            <ta e="T699" id="Seg_10021" s="T698">n</ta>
            <ta e="T700" id="Seg_10022" s="T699">pers</ta>
            <ta e="T701" id="Seg_10023" s="T700">adv</ta>
            <ta e="T702" id="Seg_10024" s="T701">ptcl</ta>
            <ta e="T703" id="Seg_10025" s="T702">v</ta>
            <ta e="T704" id="Seg_10026" s="T703">ptcl</ta>
            <ta e="T706" id="Seg_10027" s="T705">dempro</ta>
            <ta e="T707" id="Seg_10028" s="T706">n</ta>
            <ta e="T708" id="Seg_10029" s="T707">n</ta>
            <ta e="T709" id="Seg_10030" s="T708">pers</ta>
            <ta e="T710" id="Seg_10031" s="T709">quant</ta>
            <ta e="T711" id="Seg_10032" s="T710">dempro</ta>
            <ta e="T712" id="Seg_10033" s="T711">n</ta>
            <ta e="T713" id="Seg_10034" s="T712">adv</ta>
            <ta e="T714" id="Seg_10035" s="T713">v</ta>
            <ta e="T715" id="Seg_10036" s="T714">adj</ta>
            <ta e="T716" id="Seg_10037" s="T715">adv</ta>
            <ta e="T717" id="Seg_10038" s="T716">quant</ta>
            <ta e="T718" id="Seg_10039" s="T717">n</ta>
            <ta e="T719" id="Seg_10040" s="T718">v</ta>
            <ta e="T720" id="Seg_10041" s="T719">n</ta>
            <ta e="T721" id="Seg_10042" s="T720">ptcl</ta>
            <ta e="T722" id="Seg_10043" s="T721">n</ta>
            <ta e="T723" id="Seg_10044" s="T722">adj</ta>
            <ta e="T724" id="Seg_10045" s="T723">v</ta>
            <ta e="T725" id="Seg_10046" s="T724">ptcl</ta>
            <ta e="T726" id="Seg_10047" s="T725">n</ta>
            <ta e="T727" id="Seg_10048" s="T726">v</ta>
            <ta e="T728" id="Seg_10049" s="T727">pers</ta>
            <ta e="T730" id="Seg_10050" s="T729">n</ta>
            <ta e="T731" id="Seg_10051" s="T730">n</ta>
            <ta e="T732" id="Seg_10052" s="T731">n</ta>
            <ta e="T733" id="Seg_10053" s="T732">n</ta>
            <ta e="T734" id="Seg_10054" s="T733">ptcl</ta>
            <ta e="T736" id="Seg_10055" s="T735">dempro</ta>
            <ta e="T737" id="Seg_10056" s="T736">adv</ta>
            <ta e="T738" id="Seg_10057" s="T737">adj</ta>
            <ta e="T739" id="Seg_10058" s="T738">adv</ta>
            <ta e="T740" id="Seg_10059" s="T739">v</ta>
            <ta e="T741" id="Seg_10060" s="T740">conj</ta>
            <ta e="T742" id="Seg_10061" s="T741">adj</ta>
            <ta e="T743" id="Seg_10062" s="T742">dempro</ta>
            <ta e="T745" id="Seg_10063" s="T744">v</ta>
            <ta e="T746" id="Seg_10064" s="T745">v</ta>
            <ta e="T748" id="Seg_10065" s="T747">n</ta>
            <ta e="T749" id="Seg_10066" s="T748">ptcl</ta>
            <ta e="T750" id="Seg_10067" s="T749">ptcl</ta>
            <ta e="T751" id="Seg_10068" s="T750">v</ta>
            <ta e="T752" id="Seg_10069" s="T751">adv</ta>
            <ta e="T754" id="Seg_10070" s="T753">adv</ta>
            <ta e="T755" id="Seg_10071" s="T754">n</ta>
            <ta e="T756" id="Seg_10072" s="T755">quant</ta>
            <ta e="T757" id="Seg_10073" s="T756">v</ta>
            <ta e="T759" id="Seg_10074" s="T758">pers</ta>
            <ta e="T763" id="Seg_10075" s="T762">ptcl</ta>
            <ta e="T765" id="Seg_10076" s="T764">conj</ta>
            <ta e="T766" id="Seg_10077" s="T765">v</ta>
            <ta e="T767" id="Seg_10078" s="T766">dempro</ta>
            <ta e="T768" id="Seg_10079" s="T767">conj</ta>
            <ta e="T769" id="Seg_10080" s="T768">pers</ta>
            <ta e="T770" id="Seg_10081" s="T769">refl</ta>
            <ta e="T771" id="Seg_10082" s="T770">v</ta>
            <ta e="T772" id="Seg_10083" s="T771">n</ta>
            <ta e="T773" id="Seg_10084" s="T772">v</ta>
            <ta e="T774" id="Seg_10085" s="T773">n</ta>
            <ta e="T775" id="Seg_10086" s="T774">v</ta>
            <ta e="T776" id="Seg_10087" s="T775">n</ta>
            <ta e="T777" id="Seg_10088" s="T776">adv</ta>
            <ta e="T779" id="Seg_10089" s="T778">conj</ta>
            <ta e="T780" id="Seg_10090" s="T779">pers</ta>
            <ta e="T781" id="Seg_10091" s="T780">ptcl</ta>
            <ta e="T782" id="Seg_10092" s="T781">n</ta>
            <ta e="T783" id="Seg_10093" s="T782">v</ta>
            <ta e="T784" id="Seg_10094" s="T783">v</ta>
            <ta e="T785" id="Seg_10095" s="T784">v</ta>
            <ta e="T786" id="Seg_10096" s="T785">n</ta>
            <ta e="T787" id="Seg_10097" s="T786">v</ta>
            <ta e="T788" id="Seg_10098" s="T787">v</ta>
            <ta e="T789" id="Seg_10099" s="T788">v</ta>
            <ta e="T790" id="Seg_10100" s="T789">adv</ta>
            <ta e="T791" id="Seg_10101" s="T790">v</ta>
            <ta e="T792" id="Seg_10102" s="T791">v</ta>
            <ta e="T793" id="Seg_10103" s="T792">conj</ta>
            <ta e="T795" id="Seg_10104" s="T794">dempro</ta>
            <ta e="T796" id="Seg_10105" s="T795">propr</ta>
            <ta e="T797" id="Seg_10106" s="T796">pers</ta>
            <ta e="T798" id="Seg_10107" s="T797">ptcl</ta>
            <ta e="T800" id="Seg_10108" s="T799">n</ta>
            <ta e="T801" id="Seg_10109" s="T800">num</ta>
            <ta e="T802" id="Seg_10110" s="T801">n</ta>
            <ta e="T804" id="Seg_10111" s="T803">v</ta>
            <ta e="T805" id="Seg_10112" s="T804">conj</ta>
            <ta e="T806" id="Seg_10113" s="T805">pers</ta>
            <ta e="T807" id="Seg_10114" s="T806">v</ta>
            <ta e="T808" id="Seg_10115" s="T807">num</ta>
            <ta e="T809" id="Seg_10116" s="T808">n</ta>
            <ta e="T810" id="Seg_10117" s="T809">num</ta>
            <ta e="T811" id="Seg_10118" s="T810">num</ta>
            <ta e="T812" id="Seg_10119" s="T811">num</ta>
            <ta e="T813" id="Seg_10120" s="T812">dempro</ta>
            <ta e="T815" id="Seg_10121" s="T814">v</ta>
            <ta e="T816" id="Seg_10122" s="T815">pers</ta>
            <ta e="T817" id="Seg_10123" s="T816">num</ta>
            <ta e="T818" id="Seg_10124" s="T817">n</ta>
            <ta e="T819" id="Seg_10125" s="T818">conj</ta>
            <ta e="T820" id="Seg_10126" s="T819">num</ta>
            <ta e="T821" id="Seg_10127" s="T820">num</ta>
            <ta e="T822" id="Seg_10128" s="T821">n</ta>
            <ta e="T823" id="Seg_10129" s="T822">pers</ta>
            <ta e="T824" id="Seg_10130" s="T823">adv</ta>
            <ta e="T825" id="Seg_10131" s="T824">n</ta>
            <ta e="T826" id="Seg_10132" s="T825">v</ta>
            <ta e="T827" id="Seg_10133" s="T826">conj</ta>
            <ta e="T828" id="Seg_10134" s="T827">adv</ta>
            <ta e="T829" id="Seg_10135" s="T828">adv</ta>
            <ta e="T830" id="Seg_10136" s="T829">v</ta>
            <ta e="T831" id="Seg_10137" s="T830">pers</ta>
            <ta e="T832" id="Seg_10138" s="T831">v</ta>
            <ta e="T833" id="Seg_10139" s="T832">n</ta>
            <ta e="T834" id="Seg_10140" s="T833">v</ta>
            <ta e="T835" id="Seg_10141" s="T834">n</ta>
            <ta e="T836" id="Seg_10142" s="T835">v</ta>
            <ta e="T837" id="Seg_10143" s="T836">v</ta>
            <ta e="T838" id="Seg_10144" s="T837">ptcl</ta>
            <ta e="T839" id="Seg_10145" s="T838">n</ta>
            <ta e="T842" id="Seg_10146" s="T841">v</ta>
            <ta e="T843" id="Seg_10147" s="T842">adv</ta>
            <ta e="T844" id="Seg_10148" s="T843">adj</ta>
            <ta e="T845" id="Seg_10149" s="T844">n</ta>
            <ta e="T846" id="Seg_10150" s="T845">v</ta>
            <ta e="T847" id="Seg_10151" s="T846">n</ta>
            <ta e="T848" id="Seg_10152" s="T847">v</ta>
            <ta e="T849" id="Seg_10153" s="T848">pers</ta>
            <ta e="T850" id="Seg_10154" s="T849">v</ta>
            <ta e="T851" id="Seg_10155" s="T850">conj</ta>
            <ta e="T852" id="Seg_10156" s="T851">n</ta>
            <ta e="T853" id="Seg_10157" s="T852">v</ta>
            <ta e="T854" id="Seg_10158" s="T853">n</ta>
            <ta e="T858" id="Seg_10159" s="T857">v</ta>
            <ta e="T859" id="Seg_10160" s="T858">v</ta>
            <ta e="T860" id="Seg_10161" s="T859">conj</ta>
            <ta e="T862" id="Seg_10162" s="T861">adv</ta>
            <ta e="T863" id="Seg_10163" s="T862">v</ta>
            <ta e="T864" id="Seg_10164" s="T863">conj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_10165" s="T0">pro.h:E</ta>
            <ta e="T3" id="Seg_10166" s="T2">np:L</ta>
            <ta e="T9" id="Seg_10167" s="T8">np:L</ta>
            <ta e="T10" id="Seg_10168" s="T9">0.1.h:Th</ta>
            <ta e="T11" id="Seg_10169" s="T10">adv:Time</ta>
            <ta e="T12" id="Seg_10170" s="T11">0.1.h:A</ta>
            <ta e="T13" id="Seg_10171" s="T12">np:L</ta>
            <ta e="T15" id="Seg_10172" s="T14">0.3:A</ta>
            <ta e="T23" id="Seg_10173" s="T22">adv:Time</ta>
            <ta e="T24" id="Seg_10174" s="T23">0.1.h:A</ta>
            <ta e="T25" id="Seg_10175" s="T24">np:G</ta>
            <ta e="T26" id="Seg_10176" s="T25">adv:L</ta>
            <ta e="T27" id="Seg_10177" s="T26">0.1.h:Th</ta>
            <ta e="T28" id="Seg_10178" s="T27">adv:L</ta>
            <ta e="T29" id="Seg_10179" s="T28">0.1.h:E</ta>
            <ta e="T31" id="Seg_10180" s="T30">n:Time</ta>
            <ta e="T32" id="Seg_10181" s="T31">0.1.h:E</ta>
            <ta e="T33" id="Seg_10182" s="T32">adv:Time</ta>
            <ta e="T34" id="Seg_10183" s="T33">0.1.h:Th</ta>
            <ta e="T36" id="Seg_10184" s="T35">np:G</ta>
            <ta e="T37" id="Seg_10185" s="T36">0.1.h:A</ta>
            <ta e="T38" id="Seg_10186" s="T37">np:G</ta>
            <ta e="T39" id="Seg_10187" s="T38">adv:Time</ta>
            <ta e="T40" id="Seg_10188" s="T39">np:So</ta>
            <ta e="T41" id="Seg_10189" s="T40">0.1.h:A</ta>
            <ta e="T43" id="Seg_10190" s="T42">0.1.h:Th</ta>
            <ta e="T44" id="Seg_10191" s="T43">np:G</ta>
            <ta e="T45" id="Seg_10192" s="T44">0.1.h:A</ta>
            <ta e="T46" id="Seg_10193" s="T45">np:G</ta>
            <ta e="T47" id="Seg_10194" s="T46">adv:L</ta>
            <ta e="T48" id="Seg_10195" s="T47">0.1.h:E</ta>
            <ta e="T52" id="Seg_10196" s="T51">n:Time</ta>
            <ta e="T53" id="Seg_10197" s="T52">adv:Time</ta>
            <ta e="T54" id="Seg_10198" s="T53">np:G</ta>
            <ta e="T55" id="Seg_10199" s="T54">0.1.h:A</ta>
            <ta e="T57" id="Seg_10200" s="T56">pro:G</ta>
            <ta e="T58" id="Seg_10201" s="T57">0.1.h:A</ta>
            <ta e="T59" id="Seg_10202" s="T58">pro:G</ta>
            <ta e="T60" id="Seg_10203" s="T59">0.1.h:A</ta>
            <ta e="T61" id="Seg_10204" s="T60">0.1.h:A</ta>
            <ta e="T62" id="Seg_10205" s="T61">adv:Time</ta>
            <ta e="T63" id="Seg_10206" s="T62">pro.h:A</ta>
            <ta e="T65" id="Seg_10207" s="T64">pro.h:R</ta>
            <ta e="T68" id="Seg_10208" s="T67">0.2.h:A</ta>
            <ta e="T69" id="Seg_10209" s="T68">0.2.h:A</ta>
            <ta e="T71" id="Seg_10210" s="T70">0.2.h:E</ta>
            <ta e="T73" id="Seg_10211" s="T72">adv:Time</ta>
            <ta e="T74" id="Seg_10212" s="T73">pro.h:A</ta>
            <ta e="T76" id="Seg_10213" s="T75">0.1.h:A</ta>
            <ta e="T77" id="Seg_10214" s="T76">0.1.h:A</ta>
            <ta e="T83" id="Seg_10215" s="T82">pro.h:A</ta>
            <ta e="T87" id="Seg_10216" s="T86">np:G</ta>
            <ta e="T88" id="Seg_10217" s="T87">adv:Time</ta>
            <ta e="T91" id="Seg_10218" s="T90">np:G</ta>
            <ta e="T93" id="Seg_10219" s="T92">0.1.h:Th</ta>
            <ta e="T94" id="Seg_10220" s="T93">0.1.h:A</ta>
            <ta e="T95" id="Seg_10221" s="T94">np:G</ta>
            <ta e="T96" id="Seg_10222" s="T95">adv:L</ta>
            <ta e="T97" id="Seg_10223" s="T96">np:R</ta>
            <ta e="T99" id="Seg_10224" s="T98">adv:L</ta>
            <ta e="T100" id="Seg_10225" s="T99">np:R</ta>
            <ta e="T101" id="Seg_10226" s="T100">0.1.h:A</ta>
            <ta e="T102" id="Seg_10227" s="T101">adv:Time</ta>
            <ta e="T103" id="Seg_10228" s="T102">0.1.h:A</ta>
            <ta e="T107" id="Seg_10229" s="T106">np:G</ta>
            <ta e="T109" id="Seg_10230" s="T108">adv:L</ta>
            <ta e="T110" id="Seg_10231" s="T109">0.1.h:Th</ta>
            <ta e="T112" id="Seg_10232" s="T111">0.1.h:A</ta>
            <ta e="T113" id="Seg_10233" s="T112">adv:Time</ta>
            <ta e="T116" id="Seg_10234" s="T115">np:Th</ta>
            <ta e="T118" id="Seg_10235" s="T117">0.3.h:A</ta>
            <ta e="T121" id="Seg_10236" s="T868">0.3.h:A</ta>
            <ta e="T123" id="Seg_10237" s="T122">pro.h:A</ta>
            <ta e="T126" id="Seg_10238" s="T125">0.1.h:A</ta>
            <ta e="T127" id="Seg_10239" s="T126">adv:G</ta>
            <ta e="T128" id="Seg_10240" s="T127">adv:L</ta>
            <ta e="T129" id="Seg_10241" s="T128">np:A</ta>
            <ta e="T131" id="Seg_10242" s="T130">0.3:A</ta>
            <ta e="T132" id="Seg_10243" s="T131">pro.h:Th</ta>
            <ta e="T134" id="Seg_10244" s="T133">np:G</ta>
            <ta e="T135" id="Seg_10245" s="T134">0.1.h:A</ta>
            <ta e="T136" id="Seg_10246" s="T135">adv:L</ta>
            <ta e="T142" id="Seg_10247" s="T141">adv:L</ta>
            <ta e="T143" id="Seg_10248" s="T142">0.1.h:A</ta>
            <ta e="T145" id="Seg_10249" s="T144">0.1.h:A</ta>
            <ta e="T146" id="Seg_10250" s="T145">pro.h:A</ta>
            <ta e="T150" id="Seg_10251" s="T149">0.1.h:A</ta>
            <ta e="T154" id="Seg_10252" s="T153">0.1.h:A</ta>
            <ta e="T155" id="Seg_10253" s="T154">np:A</ta>
            <ta e="T158" id="Seg_10254" s="T157">0.2.h:A</ta>
            <ta e="T162" id="Seg_10255" s="T161">0.2.h:A</ta>
            <ta e="T163" id="Seg_10256" s="T162">adv:L</ta>
            <ta e="T164" id="Seg_10257" s="T163">adv:Time</ta>
            <ta e="T165" id="Seg_10258" s="T164">pro.h:A</ta>
            <ta e="T168" id="Seg_10259" s="T167">0.1.h:A</ta>
            <ta e="T169" id="Seg_10260" s="T168">np:G</ta>
            <ta e="T170" id="Seg_10261" s="T169">adv:Time</ta>
            <ta e="T171" id="Seg_10262" s="T170">adv:L</ta>
            <ta e="T173" id="Seg_10263" s="T172">np:Th</ta>
            <ta e="T176" id="Seg_10264" s="T175">np:G</ta>
            <ta e="T179" id="Seg_10265" s="T178">np.h:A</ta>
            <ta e="T181" id="Seg_10266" s="T180">np:G</ta>
            <ta e="T183" id="Seg_10267" s="T182">pro.h:A</ta>
            <ta e="T186" id="Seg_10268" s="T185">np:G</ta>
            <ta e="T187" id="Seg_10269" s="T186">0.1.h:A</ta>
            <ta e="T188" id="Seg_10270" s="T187">0.2.h:A</ta>
            <ta e="T189" id="Seg_10271" s="T188">pro.h:R</ta>
            <ta e="T190" id="Seg_10272" s="T189">np:Th</ta>
            <ta e="T191" id="Seg_10273" s="T190">0.2.h:A</ta>
            <ta e="T192" id="Seg_10274" s="T191">np:Th</ta>
            <ta e="T193" id="Seg_10275" s="T192">pro.h:A</ta>
            <ta e="T196" id="Seg_10276" s="T195">0.2.h:A</ta>
            <ta e="T197" id="Seg_10277" s="T196">np:L</ta>
            <ta e="T199" id="Seg_10278" s="T198">adv:L</ta>
            <ta e="T200" id="Seg_10279" s="T199">0.2.h:A</ta>
            <ta e="T201" id="Seg_10280" s="T200">pro.h:B</ta>
            <ta e="T202" id="Seg_10281" s="T201">np:Th</ta>
            <ta e="T206" id="Seg_10282" s="T205">np:Th</ta>
            <ta e="T207" id="Seg_10283" s="T206">0.2.h:A</ta>
            <ta e="T208" id="Seg_10284" s="T207">pro.h:A</ta>
            <ta e="T209" id="Seg_10285" s="T208">pro.h:B</ta>
            <ta e="T211" id="Seg_10286" s="T210">np:Th</ta>
            <ta e="T213" id="Seg_10287" s="T212">pro.h:A</ta>
            <ta e="T215" id="Seg_10288" s="T214">np:Th</ta>
            <ta e="T216" id="Seg_10289" s="T215">0.1.h:A</ta>
            <ta e="T217" id="Seg_10290" s="T216">0.1.h:A</ta>
            <ta e="T219" id="Seg_10291" s="T218">0.1.h:A</ta>
            <ta e="T220" id="Seg_10292" s="T219">np:P</ta>
            <ta e="T221" id="Seg_10293" s="T220">0.1.h:A</ta>
            <ta e="T222" id="Seg_10294" s="T221">adv:Time</ta>
            <ta e="T223" id="Seg_10295" s="T222">np:A</ta>
            <ta e="T225" id="Seg_10296" s="T224">pro.h:A</ta>
            <ta e="T228" id="Seg_10297" s="T227">adv:Time</ta>
            <ta e="T229" id="Seg_10298" s="T228">0.1.h:A</ta>
            <ta e="T231" id="Seg_10299" s="T230">np.h:A</ta>
            <ta e="T233" id="Seg_10300" s="T232">np:L</ta>
            <ta e="T234" id="Seg_10301" s="T233">pro.h:A</ta>
            <ta e="T235" id="Seg_10302" s="T234">adv:L</ta>
            <ta e="T238" id="Seg_10303" s="T237">0.1.h:A</ta>
            <ta e="T243" id="Seg_10304" s="T242">np:Th</ta>
            <ta e="T244" id="Seg_10305" s="T243">np:Ins</ta>
            <ta e="T245" id="Seg_10306" s="T244">0.1.h:A</ta>
            <ta e="T246" id="Seg_10307" s="T245">adv:Time</ta>
            <ta e="T247" id="Seg_10308" s="T246">np:A</ta>
            <ta e="T251" id="Seg_10309" s="T250">0.1.h:A</ta>
            <ta e="T253" id="Seg_10310" s="T252">np:G</ta>
            <ta e="T254" id="Seg_10311" s="T253">0.1.h:A</ta>
            <ta e="T255" id="Seg_10312" s="T254">adv:Time</ta>
            <ta e="T258" id="Seg_10313" s="T257">0.1.h:A</ta>
            <ta e="T259" id="Seg_10314" s="T258">np:Th</ta>
            <ta e="T260" id="Seg_10315" s="T259">np:Th</ta>
            <ta e="T261" id="Seg_10316" s="T260">np.h:Th</ta>
            <ta e="T262" id="Seg_10317" s="T261">np:L</ta>
            <ta e="T264" id="Seg_10318" s="T263">0.1.h:A</ta>
            <ta e="T265" id="Seg_10319" s="T264">pro:G</ta>
            <ta e="T266" id="Seg_10320" s="T265">pro.h:R</ta>
            <ta e="T267" id="Seg_10321" s="T266">adv:L</ta>
            <ta e="T268" id="Seg_10322" s="T267">0.3.h:A</ta>
            <ta e="T269" id="Seg_10323" s="T268">adv:L</ta>
            <ta e="T270" id="Seg_10324" s="T269">0.2.h:A</ta>
            <ta e="T271" id="Seg_10325" s="T270">pro.h:Poss</ta>
            <ta e="T272" id="Seg_10326" s="T271">np:A</ta>
            <ta e="T274" id="Seg_10327" s="T273">pro.h:A</ta>
            <ta e="T276" id="Seg_10328" s="T275">pro:L</ta>
            <ta e="T278" id="Seg_10329" s="T277">0.1.h:A</ta>
            <ta e="T279" id="Seg_10330" s="T278">adv:Time</ta>
            <ta e="T280" id="Seg_10331" s="T279">0.1.h:A</ta>
            <ta e="T282" id="Seg_10332" s="T281">adv:Time</ta>
            <ta e="T284" id="Seg_10333" s="T283">np:A</ta>
            <ta e="T286" id="Seg_10334" s="T285">pro.h:A</ta>
            <ta e="T288" id="Seg_10335" s="T287">np:G</ta>
            <ta e="T289" id="Seg_10336" s="T288">0.1.h:A</ta>
            <ta e="T290" id="Seg_10337" s="T289">adv:Time</ta>
            <ta e="T292" id="Seg_10338" s="T291">np:Ins</ta>
            <ta e="T293" id="Seg_10339" s="T292">0.1.h:A</ta>
            <ta e="T294" id="Seg_10340" s="T293">adv:Time</ta>
            <ta e="T296" id="Seg_10341" s="T295">np:A</ta>
            <ta e="T299" id="Seg_10342" s="T298">np:G</ta>
            <ta e="T300" id="Seg_10343" s="T299">0.1.h:A</ta>
            <ta e="T302" id="Seg_10344" s="T301">np.h:A</ta>
            <ta e="T304" id="Seg_10345" s="T303">pro:G</ta>
            <ta e="T306" id="Seg_10346" s="T305">0.3.h:A</ta>
            <ta e="T309" id="Seg_10347" s="T308">pro.h:A</ta>
            <ta e="T311" id="Seg_10348" s="T310">np:Th</ta>
            <ta e="T315" id="Seg_10349" s="T314">pro.h:Th</ta>
            <ta e="T316" id="Seg_10350" s="T315">adv:Time</ta>
            <ta e="T317" id="Seg_10351" s="T316">np:Th</ta>
            <ta e="T320" id="Seg_10352" s="T319">pro.h:Th</ta>
            <ta e="T321" id="Seg_10353" s="T320">np:Th</ta>
            <ta e="T323" id="Seg_10354" s="T322">adv:Time</ta>
            <ta e="T324" id="Seg_10355" s="T323">pro.h:A</ta>
            <ta e="T325" id="Seg_10356" s="T324">np:G</ta>
            <ta e="T327" id="Seg_10357" s="T326">np:G</ta>
            <ta e="T329" id="Seg_10358" s="T328">np:P</ta>
            <ta e="T330" id="Seg_10359" s="T329">0.3.h:A</ta>
            <ta e="T331" id="Seg_10360" s="T330">adv:L</ta>
            <ta e="T334" id="Seg_10361" s="T333">0.3.h:A</ta>
            <ta e="T335" id="Seg_10362" s="T334">adv:Time</ta>
            <ta e="T337" id="Seg_10363" s="T336">np.h:E</ta>
            <ta e="T342" id="Seg_10364" s="T341">adv:Time</ta>
            <ta e="T344" id="Seg_10365" s="T343">0.3.h:A</ta>
            <ta e="T345" id="Seg_10366" s="T344">np:L</ta>
            <ta e="T346" id="Seg_10367" s="T345">np:L</ta>
            <ta e="T348" id="Seg_10368" s="T347">np.h:Th</ta>
            <ta e="T349" id="Seg_10369" s="T348">0.3.h:A</ta>
            <ta e="T351" id="Seg_10370" s="T350">pro.h:Th</ta>
            <ta e="T352" id="Seg_10371" s="T351">0.3.h:A</ta>
            <ta e="T353" id="Seg_10372" s="T352">np:G</ta>
            <ta e="T354" id="Seg_10373" s="T353">adv:L</ta>
            <ta e="T361" id="Seg_10374" s="T360">0.3.h:A</ta>
            <ta e="T363" id="Seg_10375" s="T362">pro.h:A</ta>
            <ta e="T373" id="Seg_10376" s="T372">0.1.h:A</ta>
            <ta e="T375" id="Seg_10377" s="T374">adv:L</ta>
            <ta e="T376" id="Seg_10378" s="T375">0.1.h:A</ta>
            <ta e="T377" id="Seg_10379" s="T376">np:P</ta>
            <ta e="T378" id="Seg_10380" s="T377">adv:L</ta>
            <ta e="T380" id="Seg_10381" s="T379">0.1.h:A</ta>
            <ta e="T381" id="Seg_10382" s="T380">pro.h:E</ta>
            <ta e="T383" id="Seg_10383" s="T381">pro.h:Th</ta>
            <ta e="T385" id="Seg_10384" s="T384">pro.h:E</ta>
            <ta e="T387" id="Seg_10385" s="T385">pro.h:Th</ta>
            <ta e="T390" id="Seg_10386" s="T389">pro:L</ta>
            <ta e="T392" id="Seg_10387" s="T391">np:Th</ta>
            <ta e="T394" id="Seg_10388" s="T393">pro.h:E</ta>
            <ta e="T398" id="Seg_10389" s="T397">np:Th</ta>
            <ta e="T401" id="Seg_10390" s="T400">pro:Th</ta>
            <ta e="T402" id="Seg_10391" s="T401">adv:L</ta>
            <ta e="T406" id="Seg_10392" s="T405">adv:Time</ta>
            <ta e="T407" id="Seg_10393" s="T406">np.h:A</ta>
            <ta e="T409" id="Seg_10394" s="T408">pro:G</ta>
            <ta e="T410" id="Seg_10395" s="T869">0.3.h:A</ta>
            <ta e="T412" id="Seg_10396" s="T411">0.1.h:E</ta>
            <ta e="T417" id="Seg_10397" s="T416">np:G</ta>
            <ta e="T418" id="Seg_10398" s="T417">np:G</ta>
            <ta e="T419" id="Seg_10399" s="T418">0.3.h:A</ta>
            <ta e="T420" id="Seg_10400" s="T419">adv:Time</ta>
            <ta e="T422" id="Seg_10401" s="T421">np.h:Com</ta>
            <ta e="T423" id="Seg_10402" s="T422">v:pred</ta>
            <ta e="T424" id="Seg_10403" s="T423">np:L</ta>
            <ta e="T427" id="Seg_10404" s="T426">np:Th</ta>
            <ta e="T430" id="Seg_10405" s="T429">pro.h:A</ta>
            <ta e="T433" id="Seg_10406" s="T432">np:G</ta>
            <ta e="T437" id="Seg_10407" s="T436">0.3.h:E</ta>
            <ta e="T439" id="Seg_10408" s="T437">adv:Time</ta>
            <ta e="T440" id="Seg_10409" s="T439">adv:Time</ta>
            <ta e="T441" id="Seg_10410" s="T440">adv:L</ta>
            <ta e="T442" id="Seg_10411" s="T441">0.3.h:E</ta>
            <ta e="T443" id="Seg_10412" s="T442">0.3.h:A</ta>
            <ta e="T445" id="Seg_10413" s="T444">0.3.h:E</ta>
            <ta e="T448" id="Seg_10414" s="T447">0.3.h:P</ta>
            <ta e="T450" id="Seg_10415" s="T449">np.h:Poss</ta>
            <ta e="T451" id="Seg_10416" s="T450">np:Th</ta>
            <ta e="T459" id="Seg_10417" s="T458">np:Th</ta>
            <ta e="T466" id="Seg_10418" s="T465">np:Th</ta>
            <ta e="T471" id="Seg_10419" s="T470">np:Th</ta>
            <ta e="T476" id="Seg_10420" s="T475">np:Th</ta>
            <ta e="T481" id="Seg_10421" s="T480">np.h:Poss</ta>
            <ta e="T483" id="Seg_10422" s="T482">np:Th</ta>
            <ta e="T490" id="Seg_10423" s="T489">np:Th</ta>
            <ta e="T493" id="Seg_10424" s="T492">np.h:A</ta>
            <ta e="T496" id="Seg_10425" s="T495">adv:L</ta>
            <ta e="T497" id="Seg_10426" s="T496">adv:L</ta>
            <ta e="T501" id="Seg_10427" s="T500">np:Th</ta>
            <ta e="T504" id="Seg_10428" s="T503">np.h:P</ta>
            <ta e="T507" id="Seg_10429" s="T506">pro.h:A</ta>
            <ta e="T509" id="Seg_10430" s="T508">np:G</ta>
            <ta e="T510" id="Seg_10431" s="T509">adv:L</ta>
            <ta e="T511" id="Seg_10432" s="T510">np:P</ta>
            <ta e="T512" id="Seg_10433" s="T511">0.3.h:A</ta>
            <ta e="T513" id="Seg_10434" s="T512">adv:L</ta>
            <ta e="T516" id="Seg_10435" s="T515">np:Th</ta>
            <ta e="T519" id="Seg_10436" s="T518">np.h:Com</ta>
            <ta e="T520" id="Seg_10437" s="T519">0.3.h:A</ta>
            <ta e="T521" id="Seg_10438" s="T520">adv:Time</ta>
            <ta e="T522" id="Seg_10439" s="T521">0.3.h:A</ta>
            <ta e="T523" id="Seg_10440" s="T522">np:G</ta>
            <ta e="T524" id="Seg_10441" s="T523">adv:Time</ta>
            <ta e="T527" id="Seg_10442" s="T526">0.3.h:A</ta>
            <ta e="T528" id="Seg_10443" s="T527">0.2.h:A</ta>
            <ta e="T530" id="Seg_10444" s="T529">0.2.h:A</ta>
            <ta e="T533" id="Seg_10445" s="T532">pro.h:Poss</ta>
            <ta e="T534" id="Seg_10446" s="T533">np.h:Th</ta>
            <ta e="T537" id="Seg_10447" s="T536">0.3.h:A</ta>
            <ta e="T539" id="Seg_10448" s="T538">0.3.h:A</ta>
            <ta e="T540" id="Seg_10449" s="T539">pro.h:A</ta>
            <ta e="T541" id="Seg_10450" s="T540">np:P</ta>
            <ta e="T543" id="Seg_10451" s="T542">adv:Time</ta>
            <ta e="T544" id="Seg_10452" s="T543">0.1.h:A</ta>
            <ta e="T545" id="Seg_10453" s="T544">adv:Time</ta>
            <ta e="T546" id="Seg_10454" s="T545">np:L</ta>
            <ta e="T547" id="Seg_10455" s="T546">0.1.h:A</ta>
            <ta e="T548" id="Seg_10456" s="T547">pro:Th</ta>
            <ta e="T551" id="Seg_10457" s="T550">adv:Time</ta>
            <ta e="T556" id="Seg_10458" s="T555">0.1.h:A</ta>
            <ta e="T562" id="Seg_10459" s="T561">pro.h:A</ta>
            <ta e="T564" id="Seg_10460" s="T563">np.h:Th</ta>
            <ta e="T566" id="Seg_10461" s="T565">0.1.h:A</ta>
            <ta e="T568" id="Seg_10462" s="T567">0.3:P</ta>
            <ta e="T571" id="Seg_10463" s="T570">0.1.h:A</ta>
            <ta e="T574" id="Seg_10464" s="T573">np:L</ta>
            <ta e="T575" id="Seg_10465" s="T574">pro.h:E</ta>
            <ta e="T581" id="Seg_10466" s="T580">np:P</ta>
            <ta e="T582" id="Seg_10467" s="T581">0.1.h:A</ta>
            <ta e="T584" id="Seg_10468" s="T583">0.1.h:A</ta>
            <ta e="T586" id="Seg_10469" s="T585">0.1.h:A</ta>
            <ta e="T587" id="Seg_10470" s="T586">np:Ins</ta>
            <ta e="T588" id="Seg_10471" s="T587">np:G</ta>
            <ta e="T589" id="Seg_10472" s="T588">0.1.h:A</ta>
            <ta e="T591" id="Seg_10473" s="T590">np:R</ta>
            <ta e="T594" id="Seg_10474" s="T593">0.1.h:A</ta>
            <ta e="T596" id="Seg_10475" s="T595">np:Th</ta>
            <ta e="T597" id="Seg_10476" s="T596">0.1.h:Th</ta>
            <ta e="T598" id="Seg_10477" s="T597">adv:Time</ta>
            <ta e="T599" id="Seg_10478" s="T598">np.h:Poss</ta>
            <ta e="T600" id="Seg_10479" s="T599">np.h:Th</ta>
            <ta e="T601" id="Seg_10480" s="T600">0.1.h:A</ta>
            <ta e="T605" id="Seg_10481" s="T604">0.1.h:A</ta>
            <ta e="T606" id="Seg_10482" s="T605">np:Th</ta>
            <ta e="T607" id="Seg_10483" s="T606">0.1.h:A</ta>
            <ta e="T608" id="Seg_10484" s="T607">adv:Time</ta>
            <ta e="T609" id="Seg_10485" s="T608">0.1.h:A</ta>
            <ta e="T611" id="Seg_10486" s="T610">pro.h:A</ta>
            <ta e="T612" id="Seg_10487" s="T611">np:Th</ta>
            <ta e="T615" id="Seg_10488" s="T614">adv:Time</ta>
            <ta e="T616" id="Seg_10489" s="T615">0.1.h:A</ta>
            <ta e="T617" id="Seg_10490" s="T616">0.1.h:A</ta>
            <ta e="T619" id="Seg_10491" s="T618">0.1.h:A</ta>
            <ta e="T620" id="Seg_10492" s="T619">pro.h:A</ta>
            <ta e="T623" id="Seg_10493" s="T622">np:Th</ta>
            <ta e="T626" id="Seg_10494" s="T625">0.1.h:A</ta>
            <ta e="T627" id="Seg_10495" s="T626">adv:Time</ta>
            <ta e="T628" id="Seg_10496" s="T627">0.1.h:A</ta>
            <ta e="T631" id="Seg_10497" s="T630">pro.h:A</ta>
            <ta e="T643" id="Seg_10498" s="T642">adv:Time</ta>
            <ta e="T646" id="Seg_10499" s="T645">0.1.h:A</ta>
            <ta e="T647" id="Seg_10500" s="T646">0.1.h:A</ta>
            <ta e="T648" id="Seg_10501" s="T647">np:Th</ta>
            <ta e="T649" id="Seg_10502" s="T648">adv:Time</ta>
            <ta e="T650" id="Seg_10503" s="T649">np:P</ta>
            <ta e="T652" id="Seg_10504" s="T651">0.1.h:E</ta>
            <ta e="T655" id="Seg_10505" s="T654">adv:Time</ta>
            <ta e="T656" id="Seg_10506" s="T655">np:Th</ta>
            <ta e="T658" id="Seg_10507" s="T657">adv:Time</ta>
            <ta e="T659" id="Seg_10508" s="T658">pro.h:A</ta>
            <ta e="T660" id="Seg_10509" s="T659">np:P</ta>
            <ta e="T663" id="Seg_10510" s="T662">np:G</ta>
            <ta e="T664" id="Seg_10511" s="T663">adv:Time</ta>
            <ta e="T665" id="Seg_10512" s="T664">0.1.h:A</ta>
            <ta e="T667" id="Seg_10513" s="T666">pro:L</ta>
            <ta e="T669" id="Seg_10514" s="T668">np:Th</ta>
            <ta e="T676" id="Seg_10515" s="T675">np:Th</ta>
            <ta e="T679" id="Seg_10516" s="T678">pro.h:A</ta>
            <ta e="T680" id="Seg_10517" s="T679">np:L</ta>
            <ta e="T684" id="Seg_10518" s="T683">pro:Th</ta>
            <ta e="T688" id="Seg_10519" s="T687">adv:Time</ta>
            <ta e="T691" id="Seg_10520" s="T690">np:Th</ta>
            <ta e="T692" id="Seg_10521" s="T691">0.3:P</ta>
            <ta e="T694" id="Seg_10522" s="T693">np:Th</ta>
            <ta e="T697" id="Seg_10523" s="T696">np:Th</ta>
            <ta e="T699" id="Seg_10524" s="T698">np:P</ta>
            <ta e="T700" id="Seg_10525" s="T699">pro.h:A</ta>
            <ta e="T701" id="Seg_10526" s="T700">adv:Time</ta>
            <ta e="T706" id="Seg_10527" s="T705">pro:Th</ta>
            <ta e="T707" id="Seg_10528" s="T706">n:Time</ta>
            <ta e="T708" id="Seg_10529" s="T707">np:Th</ta>
            <ta e="T709" id="Seg_10530" s="T708">pro.h:Poss</ta>
            <ta e="T712" id="Seg_10531" s="T711">n:Time</ta>
            <ta e="T714" id="Seg_10532" s="T713">0.3:Th</ta>
            <ta e="T718" id="Seg_10533" s="T717">np:Th</ta>
            <ta e="T719" id="Seg_10534" s="T718">0.3.h:A</ta>
            <ta e="T720" id="Seg_10535" s="T719">np:P</ta>
            <ta e="T722" id="Seg_10536" s="T721">np:Cau</ta>
            <ta e="T727" id="Seg_10537" s="T726">0.3:Th</ta>
            <ta e="T728" id="Seg_10538" s="T727">pro.h:A</ta>
            <ta e="T731" id="Seg_10539" s="T730">np:P</ta>
            <ta e="T733" id="Seg_10540" s="T732">np:Th</ta>
            <ta e="T740" id="Seg_10541" s="T739">0.3.h:Th</ta>
            <ta e="T745" id="Seg_10542" s="T744">0.2.h:A</ta>
            <ta e="T748" id="Seg_10543" s="T747">np:Th</ta>
            <ta e="T752" id="Seg_10544" s="T751">adv:L</ta>
            <ta e="T755" id="Seg_10545" s="T754">np:Th</ta>
            <ta e="T769" id="Seg_10546" s="T768">pro.h:A</ta>
            <ta e="T770" id="Seg_10547" s="T769">pro.h:Th</ta>
            <ta e="T772" id="Seg_10548" s="T771">np:Ins</ta>
            <ta e="T773" id="Seg_10549" s="T772">0.3.h:A</ta>
            <ta e="T774" id="Seg_10550" s="T773">np:Th</ta>
            <ta e="T775" id="Seg_10551" s="T774">0.1.h:A</ta>
            <ta e="T776" id="Seg_10552" s="T775">np:B</ta>
            <ta e="T780" id="Seg_10553" s="T779">pro.h:A</ta>
            <ta e="T782" id="Seg_10554" s="T781">np:Th</ta>
            <ta e="T785" id="Seg_10555" s="T784">0.1.h:A</ta>
            <ta e="T786" id="Seg_10556" s="T785">np:R</ta>
            <ta e="T788" id="Seg_10557" s="T787">0.1.h:A</ta>
            <ta e="T789" id="Seg_10558" s="T788">0.1.h:A</ta>
            <ta e="T790" id="Seg_10559" s="T789">adv:Time</ta>
            <ta e="T791" id="Seg_10560" s="T790">0.1.h:A</ta>
            <ta e="T794" id="Seg_10561" s="T793">0.1.h:E</ta>
            <ta e="T796" id="Seg_10562" s="T795">np.h:A</ta>
            <ta e="T797" id="Seg_10563" s="T796">pro.h:R</ta>
            <ta e="T802" id="Seg_10564" s="T801">np:Th</ta>
            <ta e="T806" id="Seg_10565" s="T805">pro.h:Poss</ta>
            <ta e="T809" id="Seg_10566" s="T808">np:Th</ta>
            <ta e="T813" id="Seg_10567" s="T812">pro.h:A</ta>
            <ta e="T816" id="Seg_10568" s="T815">pro:So</ta>
            <ta e="T818" id="Seg_10569" s="T817">np:Th</ta>
            <ta e="T822" id="Seg_10570" s="T821">np:Th</ta>
            <ta e="T823" id="Seg_10571" s="T822">pro.h:A</ta>
            <ta e="T824" id="Seg_10572" s="T823">adv:Time</ta>
            <ta e="T825" id="Seg_10573" s="T824">np:G</ta>
            <ta e="T830" id="Seg_10574" s="T829">0.3:P</ta>
            <ta e="T831" id="Seg_10575" s="T830">pro.h:A</ta>
            <ta e="T833" id="Seg_10576" s="T832">np:Th</ta>
            <ta e="T834" id="Seg_10577" s="T833">0.1.h:A</ta>
            <ta e="T835" id="Seg_10578" s="T834">np:Ins</ta>
            <ta e="T836" id="Seg_10579" s="T835">0.1.h:A</ta>
            <ta e="T837" id="Seg_10580" s="T836">0.1.h:A</ta>
            <ta e="T839" id="Seg_10581" s="T838">np:P</ta>
            <ta e="T842" id="Seg_10582" s="T841">0.3:P</ta>
            <ta e="T845" id="Seg_10583" s="T844">np:Th</ta>
            <ta e="T847" id="Seg_10584" s="T846">np:G</ta>
            <ta e="T848" id="Seg_10585" s="T847">0.1.h:A</ta>
            <ta e="T852" id="Seg_10586" s="T851">np:Th</ta>
            <ta e="T853" id="Seg_10587" s="T852">0.1.h:A</ta>
            <ta e="T854" id="Seg_10588" s="T853">np:G</ta>
            <ta e="T858" id="Seg_10589" s="T857">0.1.h:A</ta>
            <ta e="T859" id="Seg_10590" s="T858">0.1.h:A</ta>
            <ta e="T862" id="Seg_10591" s="T861">adv:Time</ta>
            <ta e="T863" id="Seg_10592" s="T862">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_10593" s="T0">pro.h:S</ta>
            <ta e="T4" id="Seg_10594" s="T3">v:pred</ta>
            <ta e="T10" id="Seg_10595" s="T9">v:pred 0.1.h:S</ta>
            <ta e="T12" id="Seg_10596" s="T11">v:pred 0.1.h:S</ta>
            <ta e="T14" id="Seg_10597" s="T13">ptcl.neg</ta>
            <ta e="T15" id="Seg_10598" s="T14">v:pred 0.3:S</ta>
            <ta e="T22" id="Seg_10599" s="T21">v:pred</ta>
            <ta e="T24" id="Seg_10600" s="T23">v:pred 0.1.h:S</ta>
            <ta e="T27" id="Seg_10601" s="T26">v:pred 0.1.h:S</ta>
            <ta e="T29" id="Seg_10602" s="T28">v:pred 0.1.h:S</ta>
            <ta e="T32" id="Seg_10603" s="T31">v:pred 0.1.h:S</ta>
            <ta e="T34" id="Seg_10604" s="T33">v:pred 0.1.h:S</ta>
            <ta e="T37" id="Seg_10605" s="T36">0.1.h:S v:pred</ta>
            <ta e="T41" id="Seg_10606" s="T40">v:pred 0.1.h:S</ta>
            <ta e="T43" id="Seg_10607" s="T42">v:pred 0.1.h:S</ta>
            <ta e="T45" id="Seg_10608" s="T44">v:pred 0.1.h:S</ta>
            <ta e="T48" id="Seg_10609" s="T47">v:pred 0.1.h:S</ta>
            <ta e="T55" id="Seg_10610" s="T54">v:pred 0.1.h:S</ta>
            <ta e="T58" id="Seg_10611" s="T57">v:pred 0.1.h:S</ta>
            <ta e="T60" id="Seg_10612" s="T59">v:pred 0.1.h:S</ta>
            <ta e="T61" id="Seg_10613" s="T60">v:pred 0.1.h:S</ta>
            <ta e="T63" id="Seg_10614" s="T62">pro.h:S</ta>
            <ta e="T67" id="Seg_10615" s="T66">v:pred</ta>
            <ta e="T68" id="Seg_10616" s="T67">v:pred 0.2.h:S</ta>
            <ta e="T69" id="Seg_10617" s="T68">v:pred 0.2.h:S</ta>
            <ta e="T71" id="Seg_10618" s="T70">v:pred 0.2.h:S</ta>
            <ta e="T74" id="Seg_10619" s="T73">pro.h:S</ta>
            <ta e="T75" id="Seg_10620" s="T74">v:pred</ta>
            <ta e="T76" id="Seg_10621" s="T75">v:pred 0.1.h:S</ta>
            <ta e="T77" id="Seg_10622" s="T76">v:pred 0.1.h:S</ta>
            <ta e="T83" id="Seg_10623" s="T82">pro.h:S</ta>
            <ta e="T84" id="Seg_10624" s="T83">v:pred</ta>
            <ta e="T93" id="Seg_10625" s="T92">v:pred 0.1.h:S</ta>
            <ta e="T94" id="Seg_10626" s="T93">v:pred 0.1.h:S</ta>
            <ta e="T98" id="Seg_10627" s="T97">s:purp</ta>
            <ta e="T101" id="Seg_10628" s="T100">v:pred 0.1.h:S</ta>
            <ta e="T103" id="Seg_10629" s="T102">v:pred 0.1.h:S</ta>
            <ta e="T110" id="Seg_10630" s="T109">v:pred 0.1.h:S</ta>
            <ta e="T112" id="Seg_10631" s="T111">v:pred 0.1.h:S</ta>
            <ta e="T115" id="Seg_10632" s="T114">v:pred</ta>
            <ta e="T116" id="Seg_10633" s="T115">np:O</ta>
            <ta e="T117" id="Seg_10634" s="T116">s:purp</ta>
            <ta e="T118" id="Seg_10635" s="T117">v:pred 0.3.h:S</ta>
            <ta e="T868" id="Seg_10636" s="T120">conv:pred</ta>
            <ta e="T121" id="Seg_10637" s="T868">v:pred 0.3.h:S</ta>
            <ta e="T123" id="Seg_10638" s="T122">pro.h:S</ta>
            <ta e="T124" id="Seg_10639" s="T123">v:pred</ta>
            <ta e="T126" id="Seg_10640" s="T125">v:pred 0.1.h:S</ta>
            <ta e="T129" id="Seg_10641" s="T128">np:S</ta>
            <ta e="T130" id="Seg_10642" s="T129">v:pred</ta>
            <ta e="T131" id="Seg_10643" s="T130">v:pred 0.3:S</ta>
            <ta e="T132" id="Seg_10644" s="T131">pro.h:O</ta>
            <ta e="T135" id="Seg_10645" s="T134">v:pred 0.1.h:S</ta>
            <ta e="T143" id="Seg_10646" s="T142">v:pred 0.1.h:S</ta>
            <ta e="T145" id="Seg_10647" s="T144">v:pred 0.1.h:S</ta>
            <ta e="T146" id="Seg_10648" s="T145">pro.h:S</ta>
            <ta e="T147" id="Seg_10649" s="T146">v:pred</ta>
            <ta e="T149" id="Seg_10650" s="T148">v:pred</ta>
            <ta e="T150" id="Seg_10651" s="T149">v:pred 0.1.h:S</ta>
            <ta e="T154" id="Seg_10652" s="T153">v:pred 0.1.h:S</ta>
            <ta e="T155" id="Seg_10653" s="T154">np:S</ta>
            <ta e="T157" id="Seg_10654" s="T156">v:pred</ta>
            <ta e="T158" id="Seg_10655" s="T157">v:pred 0.2.h:S</ta>
            <ta e="T162" id="Seg_10656" s="T161">v:pred 0.2.h:S</ta>
            <ta e="T165" id="Seg_10657" s="T164">pro.h:S</ta>
            <ta e="T166" id="Seg_10658" s="T165">v:pred</ta>
            <ta e="T168" id="Seg_10659" s="T167">v:pred 0.1.h:S</ta>
            <ta e="T173" id="Seg_10660" s="T172">np:S</ta>
            <ta e="T175" id="Seg_10661" s="T174">v:pred</ta>
            <ta e="T179" id="Seg_10662" s="T178">np.h:S</ta>
            <ta e="T180" id="Seg_10663" s="T179">v:pred</ta>
            <ta e="T183" id="Seg_10664" s="T182">pro.h:S</ta>
            <ta e="T184" id="Seg_10665" s="T183">v:pred</ta>
            <ta e="T187" id="Seg_10666" s="T186">v:pred 0.1.h:S</ta>
            <ta e="T188" id="Seg_10667" s="T187">v:pred 0.2.h:S</ta>
            <ta e="T190" id="Seg_10668" s="T189">np:O</ta>
            <ta e="T191" id="Seg_10669" s="T190">v:pred 0.2.h:S</ta>
            <ta e="T192" id="Seg_10670" s="T191">np:O</ta>
            <ta e="T193" id="Seg_10671" s="T192">pro.h:S</ta>
            <ta e="T194" id="Seg_10672" s="T193">v:pred</ta>
            <ta e="T196" id="Seg_10673" s="T195">v:pred 0.2.h:S</ta>
            <ta e="T200" id="Seg_10674" s="T199">v:pred 0.2.h:S</ta>
            <ta e="T202" id="Seg_10675" s="T201">np:S</ta>
            <ta e="T203" id="Seg_10676" s="T202">ptcl.neg</ta>
            <ta e="T204" id="Seg_10677" s="T203">ptcl:pred</ta>
            <ta e="T206" id="Seg_10678" s="T205">np:O</ta>
            <ta e="T207" id="Seg_10679" s="T206">v:pred 0.2.h:S</ta>
            <ta e="T208" id="Seg_10680" s="T207">pro.h:S</ta>
            <ta e="T211" id="Seg_10681" s="T210">np:O</ta>
            <ta e="T212" id="Seg_10682" s="T211">v:pred</ta>
            <ta e="T213" id="Seg_10683" s="T212">pro.h:S</ta>
            <ta e="T214" id="Seg_10684" s="T213">v:pred</ta>
            <ta e="T215" id="Seg_10685" s="T214">np:O</ta>
            <ta e="T216" id="Seg_10686" s="T215">v:pred 0.1.h:S</ta>
            <ta e="T217" id="Seg_10687" s="T216">v:pred 0.1.h:S</ta>
            <ta e="T219" id="Seg_10688" s="T218">v:pred 0.1.h:S</ta>
            <ta e="T220" id="Seg_10689" s="T219">np:O</ta>
            <ta e="T221" id="Seg_10690" s="T220">v:pred 0.1.h:S</ta>
            <ta e="T223" id="Seg_10691" s="T222">np:S</ta>
            <ta e="T224" id="Seg_10692" s="T223">v:pred</ta>
            <ta e="T225" id="Seg_10693" s="T224">pro.h:S</ta>
            <ta e="T227" id="Seg_10694" s="T226">v:pred</ta>
            <ta e="T229" id="Seg_10695" s="T228">v:pred 0.1.h:S</ta>
            <ta e="T231" id="Seg_10696" s="T230">np.h:S</ta>
            <ta e="T232" id="Seg_10697" s="T231">v:pred</ta>
            <ta e="T234" id="Seg_10698" s="T233">pro.h:S</ta>
            <ta e="T237" id="Seg_10699" s="T236">v:pred</ta>
            <ta e="T238" id="Seg_10700" s="T237">v:pred 0.1.h:S</ta>
            <ta e="T242" id="Seg_10701" s="T241">v:pred</ta>
            <ta e="T243" id="Seg_10702" s="T242">np:S</ta>
            <ta e="T245" id="Seg_10703" s="T244">v:pred 0.1.h:S</ta>
            <ta e="T247" id="Seg_10704" s="T246">np:S</ta>
            <ta e="T248" id="Seg_10705" s="T247">v:pred</ta>
            <ta e="T251" id="Seg_10706" s="T250">v:pred 0.1.h:S</ta>
            <ta e="T254" id="Seg_10707" s="T253">v:pred 0.1.h:S</ta>
            <ta e="T258" id="Seg_10708" s="T257">v:pred 0.1.h:S</ta>
            <ta e="T259" id="Seg_10709" s="T258">np:O</ta>
            <ta e="T260" id="Seg_10710" s="T259">np:O</ta>
            <ta e="T261" id="Seg_10711" s="T260">np.h:S</ta>
            <ta e="T263" id="Seg_10712" s="T262">v:pred</ta>
            <ta e="T264" id="Seg_10713" s="T263">v:pred 0.1.h:S</ta>
            <ta e="T268" id="Seg_10714" s="T267">v:pred 0.3.h:S</ta>
            <ta e="T270" id="Seg_10715" s="T269">v:pred 0.2.h:S</ta>
            <ta e="T272" id="Seg_10716" s="T271">np:S</ta>
            <ta e="T273" id="Seg_10717" s="T272">v:pred</ta>
            <ta e="T274" id="Seg_10718" s="T273">pro.h:S</ta>
            <ta e="T275" id="Seg_10719" s="T274">v:pred</ta>
            <ta e="T277" id="Seg_10720" s="T276">ptcl.neg</ta>
            <ta e="T278" id="Seg_10721" s="T277">v:pred 0.1.h:S</ta>
            <ta e="T280" id="Seg_10722" s="T279">v:pred 0.1.h:S</ta>
            <ta e="T284" id="Seg_10723" s="T283">np:S</ta>
            <ta e="T285" id="Seg_10724" s="T284">v:pred</ta>
            <ta e="T286" id="Seg_10725" s="T285">pro.h:S</ta>
            <ta e="T287" id="Seg_10726" s="T286">v:pred</ta>
            <ta e="T289" id="Seg_10727" s="T288">v:pred 0.1.h:S</ta>
            <ta e="T293" id="Seg_10728" s="T292">v:pred 0.1.h:S</ta>
            <ta e="T296" id="Seg_10729" s="T295">np:S</ta>
            <ta e="T297" id="Seg_10730" s="T296">v:pred</ta>
            <ta e="T300" id="Seg_10731" s="T299">v:pred 0.1.h:S</ta>
            <ta e="T302" id="Seg_10732" s="T301">np.h:S</ta>
            <ta e="T303" id="Seg_10733" s="T302">v:pred</ta>
            <ta e="T306" id="Seg_10734" s="T305">v:pred 0.3.h:S</ta>
            <ta e="T307" id="Seg_10735" s="T306">conv:pred</ta>
            <ta e="T309" id="Seg_10736" s="T308">pro.h:S</ta>
            <ta e="T311" id="Seg_10737" s="T310">np:O</ta>
            <ta e="T312" id="Seg_10738" s="T311">v:pred</ta>
            <ta e="T315" id="Seg_10739" s="T314">pro.h:S</ta>
            <ta e="T317" id="Seg_10740" s="T316">n:pred</ta>
            <ta e="T318" id="Seg_10741" s="T317">cop</ta>
            <ta e="T320" id="Seg_10742" s="T319">pro.h:S</ta>
            <ta e="T321" id="Seg_10743" s="T320">n:pred</ta>
            <ta e="T322" id="Seg_10744" s="T321">cop</ta>
            <ta e="T324" id="Seg_10745" s="T323">pro.h:S</ta>
            <ta e="T326" id="Seg_10746" s="T325">v:pred</ta>
            <ta e="T329" id="Seg_10747" s="T328">np:O</ta>
            <ta e="T330" id="Seg_10748" s="T329">v:pred 0.3.h:S</ta>
            <ta e="T334" id="Seg_10749" s="T333">v:pred 0.3.h:S</ta>
            <ta e="T337" id="Seg_10750" s="T336">np.h:S</ta>
            <ta e="T340" id="Seg_10751" s="T339">v:pred</ta>
            <ta e="T344" id="Seg_10752" s="T343">v:pred 0.3.h:S</ta>
            <ta e="T348" id="Seg_10753" s="T347">np.h:O</ta>
            <ta e="T349" id="Seg_10754" s="T348">v:pred 0.3.h:S</ta>
            <ta e="T351" id="Seg_10755" s="T350">pro.h:O</ta>
            <ta e="T352" id="Seg_10756" s="T351">v:pred 0.3.h:S</ta>
            <ta e="T361" id="Seg_10757" s="T360">v:pred 0.3.h:S</ta>
            <ta e="T363" id="Seg_10758" s="T362">pro.h:S</ta>
            <ta e="T367" id="Seg_10759" s="T366">ptcl.neg</ta>
            <ta e="T368" id="Seg_10760" s="T367">v:pred</ta>
            <ta e="T369" id="Seg_10761" s="T368">pro.h:S</ta>
            <ta e="T372" id="Seg_10762" s="T371">v:pred</ta>
            <ta e="T373" id="Seg_10763" s="T372">v:pred 0.1.h:S</ta>
            <ta e="T376" id="Seg_10764" s="T375">v:pred 0.1.h:S</ta>
            <ta e="T377" id="Seg_10765" s="T376">np:O</ta>
            <ta e="T380" id="Seg_10766" s="T379">v:pred 0.1.h:S</ta>
            <ta e="T381" id="Seg_10767" s="T380">pro.h:S</ta>
            <ta e="T383" id="Seg_10768" s="T381">pro.h:O</ta>
            <ta e="T384" id="Seg_10769" s="T383">v:pred</ta>
            <ta e="T385" id="Seg_10770" s="T384">pro.h:S</ta>
            <ta e="T387" id="Seg_10771" s="T385">pro.h:O</ta>
            <ta e="T388" id="Seg_10772" s="T387">ptcl.neg</ta>
            <ta e="T389" id="Seg_10773" s="T388">v:pred</ta>
            <ta e="T391" id="Seg_10774" s="T390">v:pred</ta>
            <ta e="T392" id="Seg_10775" s="T391">np:S</ta>
            <ta e="T394" id="Seg_10776" s="T393">pro.h:S</ta>
            <ta e="T395" id="Seg_10777" s="T394">v:pred</ta>
            <ta e="T398" id="Seg_10778" s="T397">np:S</ta>
            <ta e="T399" id="Seg_10779" s="T398">v:pred</ta>
            <ta e="T401" id="Seg_10780" s="T400">pro:S</ta>
            <ta e="T403" id="Seg_10781" s="T402">v:pred</ta>
            <ta e="T407" id="Seg_10782" s="T406">np.h:S</ta>
            <ta e="T408" id="Seg_10783" s="T407">v:pred</ta>
            <ta e="T869" id="Seg_10784" s="T409">conv:pred</ta>
            <ta e="T410" id="Seg_10785" s="T869">v:pred 0.3.h:S</ta>
            <ta e="T411" id="Seg_10786" s="T410">ptcl.neg</ta>
            <ta e="T412" id="Seg_10787" s="T411">v:pred 0.1.h:S</ta>
            <ta e="T419" id="Seg_10788" s="T418">v:pred 0.3.h:S</ta>
            <ta e="T427" id="Seg_10789" s="T426">np:S</ta>
            <ta e="T429" id="Seg_10790" s="T428">adj:pred</ta>
            <ta e="T430" id="Seg_10791" s="T429">pro.h:S</ta>
            <ta e="T434" id="Seg_10792" s="T433">v:pred</ta>
            <ta e="T437" id="Seg_10793" s="T436">v:pred 0.3.h:S</ta>
            <ta e="T442" id="Seg_10794" s="T441">v:pred 0.3.h:S</ta>
            <ta e="T443" id="Seg_10795" s="T442">v:pred 0.3.h:S</ta>
            <ta e="T445" id="Seg_10796" s="T444">v:pred 0.3.h:S</ta>
            <ta e="T448" id="Seg_10797" s="T447">v:pred 0.3.h:S</ta>
            <ta e="T451" id="Seg_10798" s="T450">np:S</ta>
            <ta e="T453" id="Seg_10799" s="T452">v:pred</ta>
            <ta e="T459" id="Seg_10800" s="T458">np:S</ta>
            <ta e="T461" id="Seg_10801" s="T460">v:pred</ta>
            <ta e="T466" id="Seg_10802" s="T465">np:S</ta>
            <ta e="T468" id="Seg_10803" s="T467">v:pred</ta>
            <ta e="T471" id="Seg_10804" s="T470">np:S</ta>
            <ta e="T473" id="Seg_10805" s="T472">v:pred</ta>
            <ta e="T476" id="Seg_10806" s="T475">np:S</ta>
            <ta e="T478" id="Seg_10807" s="T477">v:pred</ta>
            <ta e="T483" id="Seg_10808" s="T482">np:S</ta>
            <ta e="T484" id="Seg_10809" s="T483">v:pred</ta>
            <ta e="T490" id="Seg_10810" s="T489">np:S</ta>
            <ta e="T491" id="Seg_10811" s="T490">v:pred</ta>
            <ta e="T493" id="Seg_10812" s="T492">np.h:S</ta>
            <ta e="T494" id="Seg_10813" s="T493">v:pred</ta>
            <ta e="T495" id="Seg_10814" s="T494">s:purp</ta>
            <ta e="T501" id="Seg_10815" s="T500">np:S</ta>
            <ta e="T502" id="Seg_10816" s="T501">v:pred</ta>
            <ta e="T504" id="Seg_10817" s="T503">np.h:S</ta>
            <ta e="T506" id="Seg_10818" s="T505">v:pred</ta>
            <ta e="T507" id="Seg_10819" s="T506">pro.h:S</ta>
            <ta e="T508" id="Seg_10820" s="T507">v:pred</ta>
            <ta e="T511" id="Seg_10821" s="T510">np:O</ta>
            <ta e="T512" id="Seg_10822" s="T511">v:pred 0.3.h:S</ta>
            <ta e="T515" id="Seg_10823" s="T514">v:pred</ta>
            <ta e="T516" id="Seg_10824" s="T515">np:S</ta>
            <ta e="T520" id="Seg_10825" s="T519">v:pred 0.3.h:S</ta>
            <ta e="T522" id="Seg_10826" s="T521">v:pred 0.3.h:S</ta>
            <ta e="T525" id="Seg_10827" s="T524">v:pred</ta>
            <ta e="T527" id="Seg_10828" s="T526">v:pred 0.3.h:S</ta>
            <ta e="T528" id="Seg_10829" s="T527">v:pred 0.2.h:S</ta>
            <ta e="T530" id="Seg_10830" s="T529">v:pred 0.2.h:S</ta>
            <ta e="T534" id="Seg_10831" s="T533">np.h:S</ta>
            <ta e="T535" id="Seg_10832" s="T534">v:pred</ta>
            <ta e="T537" id="Seg_10833" s="T536">v:pred 0.3.h:S</ta>
            <ta e="T539" id="Seg_10834" s="T538">v:pred 0.3.h:S</ta>
            <ta e="T540" id="Seg_10835" s="T539">pro.h:S</ta>
            <ta e="T541" id="Seg_10836" s="T540">np:O</ta>
            <ta e="T542" id="Seg_10837" s="T541">v:pred</ta>
            <ta e="T544" id="Seg_10838" s="T543">v:pred 0.1.h:S</ta>
            <ta e="T547" id="Seg_10839" s="T546">v:pred 0.1.h:S</ta>
            <ta e="T548" id="Seg_10840" s="T547">pro:S</ta>
            <ta e="T549" id="Seg_10841" s="T548">adj:pred</ta>
            <ta e="T550" id="Seg_10842" s="T549">cop</ta>
            <ta e="T555" id="Seg_10843" s="T554">ptcl.neg</ta>
            <ta e="T556" id="Seg_10844" s="T555">v:pred 0.1.h:S</ta>
            <ta e="T558" id="Seg_10845" s="T557">adj:pred</ta>
            <ta e="T559" id="Seg_10846" s="T558">cop 0.3:S</ta>
            <ta e="T562" id="Seg_10847" s="T561">pro.h:S</ta>
            <ta e="T564" id="Seg_10848" s="T563">np.h:O</ta>
            <ta e="T565" id="Seg_10849" s="T564">v:pred</ta>
            <ta e="T566" id="Seg_10850" s="T565">v:pred 0.1.h:S</ta>
            <ta e="T568" id="Seg_10851" s="T567">v:pred 0.3:S</ta>
            <ta e="T571" id="Seg_10852" s="T570">v:pred 0.1.h:S</ta>
            <ta e="T575" id="Seg_10853" s="T574">pro.h:S</ta>
            <ta e="T576" id="Seg_10854" s="T575">v:pred</ta>
            <ta e="T581" id="Seg_10855" s="T580">np:O</ta>
            <ta e="T582" id="Seg_10856" s="T581">v:pred 0.1.h:S</ta>
            <ta e="T584" id="Seg_10857" s="T583">v:pred 0.1.h:S</ta>
            <ta e="T586" id="Seg_10858" s="T585">v:pred 0.1.h:S</ta>
            <ta e="T589" id="Seg_10859" s="T588">v:pred 0.1.h:S</ta>
            <ta e="T594" id="Seg_10860" s="T593">v:pred 0.1.h:S</ta>
            <ta e="T596" id="Seg_10861" s="T595">np:O</ta>
            <ta e="T597" id="Seg_10862" s="T596">v:pred 0.1.h:S</ta>
            <ta e="T600" id="Seg_10863" s="T599">np.h:O</ta>
            <ta e="T601" id="Seg_10864" s="T600">v:pred 0.1.h:S</ta>
            <ta e="T605" id="Seg_10865" s="T604">v:pred 0.1.h:S</ta>
            <ta e="T606" id="Seg_10866" s="T605">np:O</ta>
            <ta e="T607" id="Seg_10867" s="T606">v:pred 0.1.h:S</ta>
            <ta e="T609" id="Seg_10868" s="T608">v:pred 0.1.h:S</ta>
            <ta e="T610" id="Seg_10869" s="T609">v:pred</ta>
            <ta e="T611" id="Seg_10870" s="T610">pro.h:S</ta>
            <ta e="T612" id="Seg_10871" s="T611">np:S</ta>
            <ta e="T867" id="Seg_10872" s="T613">v:pred</ta>
            <ta e="T616" id="Seg_10873" s="T615">v:pred 0.1.h:S</ta>
            <ta e="T617" id="Seg_10874" s="T616">v:pred 0.1.h:S</ta>
            <ta e="T618" id="Seg_10875" s="T617">ptcl.neg</ta>
            <ta e="T619" id="Seg_10876" s="T618">v:pred 0.1.h:S</ta>
            <ta e="T620" id="Seg_10877" s="T619">pro.h:S</ta>
            <ta e="T621" id="Seg_10878" s="T620">v:pred</ta>
            <ta e="T623" id="Seg_10879" s="T622">np:O</ta>
            <ta e="T626" id="Seg_10880" s="T625">v:pred 0.1.h:S</ta>
            <ta e="T628" id="Seg_10881" s="T627">v:pred 0.1.h:S</ta>
            <ta e="T631" id="Seg_10882" s="T630">pro.h:S</ta>
            <ta e="T632" id="Seg_10883" s="T631">v:pred</ta>
            <ta e="T646" id="Seg_10884" s="T645">v:pred 0.1.h:S</ta>
            <ta e="T647" id="Seg_10885" s="T646">v:pred 0.1.h:S</ta>
            <ta e="T648" id="Seg_10886" s="T647">np:O</ta>
            <ta e="T650" id="Seg_10887" s="T649">np:O</ta>
            <ta e="T652" id="Seg_10888" s="T651">pro.h:S</ta>
            <ta e="T654" id="Seg_10889" s="T653">v:pred</ta>
            <ta e="T656" id="Seg_10890" s="T655">np:S</ta>
            <ta e="T657" id="Seg_10891" s="T656">v:pred</ta>
            <ta e="T659" id="Seg_10892" s="T658">pro.h:S</ta>
            <ta e="T660" id="Seg_10893" s="T659">np:O</ta>
            <ta e="T662" id="Seg_10894" s="T661">v:pred</ta>
            <ta e="T665" id="Seg_10895" s="T664">v:pred 0.1.h:S</ta>
            <ta e="T668" id="Seg_10896" s="T667">v:pred</ta>
            <ta e="T669" id="Seg_10897" s="T668">np:S</ta>
            <ta e="T676" id="Seg_10898" s="T675">np:S</ta>
            <ta e="T677" id="Seg_10899" s="T676">v:pred</ta>
            <ta e="T682" id="Seg_10900" s="T681">ptcl:pred</ta>
            <ta e="T684" id="Seg_10901" s="T683">pro:O</ta>
            <ta e="T689" id="Seg_10902" s="T688">ptcl.neg</ta>
            <ta e="T690" id="Seg_10903" s="T689">v:pred</ta>
            <ta e="T691" id="Seg_10904" s="T690">np:S</ta>
            <ta e="T692" id="Seg_10905" s="T691">v:pred 0.3:S</ta>
            <ta e="T694" id="Seg_10906" s="T693">np:S</ta>
            <ta e="T695" id="Seg_10907" s="T694">v:pred</ta>
            <ta e="T697" id="Seg_10908" s="T696">np:S</ta>
            <ta e="T698" id="Seg_10909" s="T697">v:pred</ta>
            <ta e="T699" id="Seg_10910" s="T698">np:O</ta>
            <ta e="T700" id="Seg_10911" s="T699">pro.h:S</ta>
            <ta e="T702" id="Seg_10912" s="T701">ptcl.neg</ta>
            <ta e="T703" id="Seg_10913" s="T702">v:pred</ta>
            <ta e="T704" id="Seg_10914" s="T703">ptcl:pred</ta>
            <ta e="T706" id="Seg_10915" s="T705">pro:S</ta>
            <ta e="T708" id="Seg_10916" s="T707">np:S</ta>
            <ta e="T710" id="Seg_10917" s="T709">adj:pred</ta>
            <ta e="T714" id="Seg_10918" s="T713">v:pred 0.3:S</ta>
            <ta e="T715" id="Seg_10919" s="T714">adj:pred</ta>
            <ta e="T718" id="Seg_10920" s="T717">np:O</ta>
            <ta e="T719" id="Seg_10921" s="T718">v:pred 0.3.h:S</ta>
            <ta e="T720" id="Seg_10922" s="T719">np:S</ta>
            <ta e="T723" id="Seg_10923" s="T722">adj:pred</ta>
            <ta e="T724" id="Seg_10924" s="T723">cop</ta>
            <ta e="T726" id="Seg_10925" s="T725">adj:pred</ta>
            <ta e="T727" id="Seg_10926" s="T726">cop 0.3:S</ta>
            <ta e="T728" id="Seg_10927" s="T727">pro.h:S</ta>
            <ta e="T731" id="Seg_10928" s="T730">np:O</ta>
            <ta e="T733" id="Seg_10929" s="T732">np:S</ta>
            <ta e="T738" id="Seg_10930" s="T737">adj:pred</ta>
            <ta e="T740" id="Seg_10931" s="T739">v:pred 0.3.h:S</ta>
            <ta e="T745" id="Seg_10932" s="T744"> 0.2.h:S v:pred</ta>
            <ta e="T748" id="Seg_10933" s="T747">np:S</ta>
            <ta e="T750" id="Seg_10934" s="T749">ptcl.neg</ta>
            <ta e="T751" id="Seg_10935" s="T750">v:pred</ta>
            <ta e="T755" id="Seg_10936" s="T754">np:S</ta>
            <ta e="T756" id="Seg_10937" s="T755">adj:pred</ta>
            <ta e="T769" id="Seg_10938" s="T768">pro.h:S</ta>
            <ta e="T770" id="Seg_10939" s="T769">pro.h:O</ta>
            <ta e="T771" id="Seg_10940" s="T770">v:pred</ta>
            <ta e="T773" id="Seg_10941" s="T772">v:pred 0.3.h:S</ta>
            <ta e="T774" id="Seg_10942" s="T773">np:O</ta>
            <ta e="T775" id="Seg_10943" s="T774">v:pred 0.1.h:S</ta>
            <ta e="T780" id="Seg_10944" s="T779">pro.h:S</ta>
            <ta e="T782" id="Seg_10945" s="T781">np:O</ta>
            <ta e="T783" id="Seg_10946" s="T782">v:pred</ta>
            <ta e="T784" id="Seg_10947" s="T783">conv:pred</ta>
            <ta e="T785" id="Seg_10948" s="T784">v:pred 0.1.h:S</ta>
            <ta e="T788" id="Seg_10949" s="T787">v:pred 0.1.h:S</ta>
            <ta e="T789" id="Seg_10950" s="T788">v:pred 0.1.h:S</ta>
            <ta e="T791" id="Seg_10951" s="T790">v:pred 0.1.h:S</ta>
            <ta e="T792" id="Seg_10952" s="T791">s:purp</ta>
            <ta e="T794" id="Seg_10953" s="T793">v:pred 0.1.h:S</ta>
            <ta e="T796" id="Seg_10954" s="T795">np.h:S</ta>
            <ta e="T799" id="Seg_10955" s="T798">v:pred</ta>
            <ta e="T802" id="Seg_10956" s="T801">np:S</ta>
            <ta e="T804" id="Seg_10957" s="T803">v:pred</ta>
            <ta e="T807" id="Seg_10958" s="T806">v:pred</ta>
            <ta e="T809" id="Seg_10959" s="T808">np:S</ta>
            <ta e="T813" id="Seg_10960" s="T812">pro.h:S</ta>
            <ta e="T815" id="Seg_10961" s="T814">v:pred</ta>
            <ta e="T818" id="Seg_10962" s="T817">np:O</ta>
            <ta e="T822" id="Seg_10963" s="T821">np:O</ta>
            <ta e="T823" id="Seg_10964" s="T822">pro.h:S</ta>
            <ta e="T826" id="Seg_10965" s="T825">v:pred</ta>
            <ta e="T830" id="Seg_10966" s="T829">v:pred 0.3:S</ta>
            <ta e="T831" id="Seg_10967" s="T830">pro.h:S</ta>
            <ta e="T832" id="Seg_10968" s="T831">v:pred</ta>
            <ta e="T833" id="Seg_10969" s="T832">np:O</ta>
            <ta e="T834" id="Seg_10970" s="T833">v:pred 0.1.h:S</ta>
            <ta e="T836" id="Seg_10971" s="T835">v:pred 0.1.h:S</ta>
            <ta e="T837" id="Seg_10972" s="T836">v:pred 0.1.h:S</ta>
            <ta e="T839" id="Seg_10973" s="T838">np:O</ta>
            <ta e="T842" id="Seg_10974" s="T841">v:pred 0.3:S</ta>
            <ta e="T844" id="Seg_10975" s="T843">adj:pred</ta>
            <ta e="T845" id="Seg_10976" s="T844">np:S</ta>
            <ta e="T846" id="Seg_10977" s="T845">cop</ta>
            <ta e="T848" id="Seg_10978" s="T847">v:pred 0.1.h:S</ta>
            <ta e="T852" id="Seg_10979" s="T851">np:O</ta>
            <ta e="T853" id="Seg_10980" s="T852">v:pred 0.1.h:S</ta>
            <ta e="T858" id="Seg_10981" s="T857">v:pred 0.1.h:S</ta>
            <ta e="T859" id="Seg_10982" s="T858">v:pred 0.1.h:S</ta>
            <ta e="T863" id="Seg_10983" s="T862">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_10984" s="T1">TURK:disc</ta>
            <ta e="T19" id="Seg_10985" s="T18">TURK:disc</ta>
            <ta e="T44" id="Seg_10986" s="T43">RUS:cult</ta>
            <ta e="T56" id="Seg_10987" s="T55">RUS:gram</ta>
            <ta e="T61" id="Seg_10988" s="T60">%TURK:core</ta>
            <ta e="T72" id="Seg_10989" s="T71">RUS:mod=RUS:mod(PTCL)</ta>
            <ta e="T79" id="Seg_10990" s="T78">RUS:core</ta>
            <ta e="T82" id="Seg_10991" s="T81">RUS:core</ta>
            <ta e="T112" id="Seg_10992" s="T111">%TURK:core</ta>
            <ta e="T116" id="Seg_10993" s="T115">RUS:cult</ta>
            <ta e="T119" id="Seg_10994" s="T118">RUS:gram</ta>
            <ta e="T122" id="Seg_10995" s="T121">RUS:gram</ta>
            <ta e="T125" id="Seg_10996" s="T124">RUS:gram</ta>
            <ta e="T129" id="Seg_10997" s="T128">RUS:cult</ta>
            <ta e="T133" id="Seg_10998" s="T132">RUS:gram</ta>
            <ta e="T144" id="Seg_10999" s="T143">RUS:gram</ta>
            <ta e="T155" id="Seg_11000" s="T154">RUS:cult</ta>
            <ta e="T156" id="Seg_11001" s="T155">TURK:disc</ta>
            <ta e="T167" id="Seg_11002" s="T166">RUS:gram</ta>
            <ta e="T172" id="Seg_11003" s="T171">RUS:mod</ta>
            <ta e="T181" id="Seg_11004" s="T180">TAT:cult</ta>
            <ta e="T182" id="Seg_11005" s="T181">RUS:gram</ta>
            <ta e="T191" id="Seg_11006" s="T190">TURK:cult</ta>
            <ta e="T197" id="Seg_11007" s="T196">RUS:cult</ta>
            <ta e="T206" id="Seg_11008" s="T205">RUS:cult</ta>
            <ta e="T211" id="Seg_11009" s="T210">RUS:cult</ta>
            <ta e="T218" id="Seg_11010" s="T217">RUS:gram</ta>
            <ta e="T223" id="Seg_11011" s="T222">RUS:cult</ta>
            <ta e="T239" id="Seg_11012" s="T238">TURK:core</ta>
            <ta e="T240" id="Seg_11013" s="T239">TURK:disc</ta>
            <ta e="T243" id="Seg_11014" s="T242">RUS:cult</ta>
            <ta e="T244" id="Seg_11015" s="T243">RUS:cult</ta>
            <ta e="T247" id="Seg_11016" s="T246">RUS:cult</ta>
            <ta e="T252" id="Seg_11017" s="T251">RUS:gram</ta>
            <ta e="T262" id="Seg_11018" s="T261">RUS:cult</ta>
            <ta e="T272" id="Seg_11019" s="T271">RUS:cult</ta>
            <ta e="T276" id="Seg_11020" s="T275">TURK:gram(INDEF)</ta>
            <ta e="T284" id="Seg_11021" s="T283">RUS:cult</ta>
            <ta e="T288" id="Seg_11022" s="T287">RUS:cult</ta>
            <ta e="T296" id="Seg_11023" s="T295">RUS:cult</ta>
            <ta e="T298" id="Seg_11024" s="T297">RUS:gram</ta>
            <ta e="T306" id="Seg_11025" s="T305">%TURK:core</ta>
            <ta e="T308" id="Seg_11026" s="T307">RUS:gram</ta>
            <ta e="T310" id="Seg_11027" s="T309">RUS:cult</ta>
            <ta e="T311" id="Seg_11028" s="T310">RUS:cult</ta>
            <ta e="T312" id="Seg_11029" s="T311">%TURK:core</ta>
            <ta e="T313" id="Seg_11030" s="T312">RUS:gram</ta>
            <ta e="T319" id="Seg_11031" s="T318">RUS:gram</ta>
            <ta e="T321" id="Seg_11032" s="T320">RUS:cult</ta>
            <ta e="T339" id="Seg_11033" s="T338">TURK:disc</ta>
            <ta e="T341" id="Seg_11034" s="T340">TURK:disc</ta>
            <ta e="T345" id="Seg_11035" s="T344">TAT:cult</ta>
            <ta e="T347" id="Seg_11036" s="T346">RUS:gram</ta>
            <ta e="T350" id="Seg_11037" s="T349">RUS:gram</ta>
            <ta e="T353" id="Seg_11038" s="T352">RUS:cult</ta>
            <ta e="T356" id="Seg_11039" s="T355">TURK:disc</ta>
            <ta e="T359" id="Seg_11040" s="T358">RUS:gram</ta>
            <ta e="T362" id="Seg_11041" s="T361">RUS:gram</ta>
            <ta e="T383" id="Seg_11042" s="T381">TURK:mod(PTCL)</ta>
            <ta e="T387" id="Seg_11043" s="T385">TURK:mod(PTCL)</ta>
            <ta e="T390" id="Seg_11044" s="T389">TURK:gram(INDEF)</ta>
            <ta e="T392" id="Seg_11045" s="T391">RUS:mod</ta>
            <ta e="T393" id="Seg_11046" s="T392">RUS:gram</ta>
            <ta e="T397" id="Seg_11047" s="T396">RUS:core</ta>
            <ta e="T398" id="Seg_11048" s="T397">RUS:mod</ta>
            <ta e="T400" id="Seg_11049" s="T399">RUS:gram</ta>
            <ta e="T402" id="Seg_11050" s="T401">TURK:gram(INDEF)</ta>
            <ta e="T407" id="Seg_11051" s="T406">RUS:cult</ta>
            <ta e="T409" id="Seg_11052" s="T408">TURK:gram(INDEF)</ta>
            <ta e="T416" id="Seg_11053" s="T415">TURK:disc</ta>
            <ta e="T417" id="Seg_11054" s="T416">RUS:cult</ta>
            <ta e="T421" id="Seg_11055" s="T420">TURK:gram(INDEF)</ta>
            <ta e="T423" id="Seg_11056" s="T422">RUS:mod</ta>
            <ta e="T426" id="Seg_11057" s="T425">RUS:gram</ta>
            <ta e="T431" id="Seg_11058" s="T430">TURK:disc</ta>
            <ta e="T432" id="Seg_11059" s="T431">RUS:mod</ta>
            <ta e="T433" id="Seg_11060" s="T432">RUS:cult</ta>
            <ta e="T435" id="Seg_11061" s="T434">RUS:gram</ta>
            <ta e="T436" id="Seg_11062" s="T435">TURK:disc</ta>
            <ta e="T446" id="Seg_11063" s="T445">RUS:gram</ta>
            <ta e="T451" id="Seg_11064" s="T450">TAT:cult</ta>
            <ta e="T452" id="Seg_11065" s="T451">RUS:mod</ta>
            <ta e="T454" id="Seg_11066" s="T453">RUS:gram</ta>
            <ta e="T456" id="Seg_11067" s="T455">RUS:gram</ta>
            <ta e="T459" id="Seg_11068" s="T458">TAT:cult</ta>
            <ta e="T460" id="Seg_11069" s="T459">RUS:mod</ta>
            <ta e="T466" id="Seg_11070" s="T465">TAT:cult</ta>
            <ta e="T467" id="Seg_11071" s="T466">RUS:mod</ta>
            <ta e="T471" id="Seg_11072" s="T470">TAT:cult</ta>
            <ta e="T472" id="Seg_11073" s="T471">RUS:mod</ta>
            <ta e="T476" id="Seg_11074" s="T475">TAT:cult</ta>
            <ta e="T477" id="Seg_11075" s="T476">RUS:mod</ta>
            <ta e="T479" id="Seg_11076" s="T478">RUS:gram</ta>
            <ta e="T482" id="Seg_11077" s="T481">RUS:mod</ta>
            <ta e="T483" id="Seg_11078" s="T482">TAT:cult</ta>
            <ta e="T485" id="Seg_11079" s="T484">RUS:gram</ta>
            <ta e="T489" id="Seg_11080" s="T488">RUS:mod</ta>
            <ta e="T490" id="Seg_11081" s="T489">TAT:cult</ta>
            <ta e="T495" id="Seg_11082" s="T494">%TURK:core</ta>
            <ta e="T501" id="Seg_11083" s="T500">TAT:cult</ta>
            <ta e="T511" id="Seg_11084" s="T510">TURK:cult</ta>
            <ta e="T518" id="Seg_11085" s="T517">RUS:gram</ta>
            <ta e="T526" id="Seg_11086" s="T525">RUS:gram</ta>
            <ta e="T532" id="Seg_11087" s="T531">RUS:gram</ta>
            <ta e="T536" id="Seg_11088" s="T535">RUS:gram</ta>
            <ta e="T538" id="Seg_11089" s="T537">RUS:gram</ta>
            <ta e="T567" id="Seg_11090" s="T566">TURK:disc</ta>
            <ta e="T569" id="Seg_11091" s="T568">RUS:gram</ta>
            <ta e="T590" id="Seg_11092" s="T589">TURK:disc</ta>
            <ta e="T591" id="Seg_11093" s="T590">TURK:cult</ta>
            <ta e="T599" id="Seg_11094" s="T598">RUS:core</ta>
            <ta e="T602" id="Seg_11095" s="T601">RUS:gram</ta>
            <ta e="T603" id="Seg_11096" s="T602">RUS:mod</ta>
            <ta e="T629" id="Seg_11097" s="T628">RUS:gram</ta>
            <ta e="T633" id="Seg_11098" s="T632">RUS:gram</ta>
            <ta e="T635" id="Seg_11099" s="T634">TURK:disc</ta>
            <ta e="T638" id="Seg_11100" s="T637">RUS:gram</ta>
            <ta e="T656" id="Seg_11101" s="T655">RUS:core</ta>
            <ta e="T661" id="Seg_11102" s="T660">TURK:disc</ta>
            <ta e="T663" id="Seg_11103" s="T662">TURK:cult</ta>
            <ta e="T666" id="Seg_11104" s="T665">RUS:gram</ta>
            <ta e="T667" id="Seg_11105" s="T666">TURK:gram(INDEF)</ta>
            <ta e="T669" id="Seg_11106" s="T668">RUS:core</ta>
            <ta e="T680" id="Seg_11107" s="T679">RUS:core</ta>
            <ta e="T682" id="Seg_11108" s="T681">RUS:mod</ta>
            <ta e="T691" id="Seg_11109" s="T690">RUS:core</ta>
            <ta e="T693" id="Seg_11110" s="T692">RUS:gram</ta>
            <ta e="T696" id="Seg_11111" s="T695">RUS:gram</ta>
            <ta e="T713" id="Seg_11112" s="T712">RUS:mod</ta>
            <ta e="T721" id="Seg_11113" s="T720">TURK:disc</ta>
            <ta e="T725" id="Seg_11114" s="T724">RUS:gram</ta>
            <ta e="T734" id="Seg_11115" s="T733">TURK:disc</ta>
            <ta e="T738" id="Seg_11116" s="T737">TURK:core</ta>
            <ta e="T741" id="Seg_11117" s="T740">RUS:gram</ta>
            <ta e="T763" id="Seg_11118" s="T762">RUS:mod</ta>
            <ta e="T765" id="Seg_11119" s="T764">RUS:gram</ta>
            <ta e="T768" id="Seg_11120" s="T767">RUS:gram</ta>
            <ta e="T772" id="Seg_11121" s="T771">RUS:cult</ta>
            <ta e="T776" id="Seg_11122" s="T775">RUS:cult</ta>
            <ta e="T779" id="Seg_11123" s="T778">RUS:gram</ta>
            <ta e="T781" id="Seg_11124" s="T780">TURK:disc</ta>
            <ta e="T786" id="Seg_11125" s="T785">TURK:cult</ta>
            <ta e="T793" id="Seg_11126" s="T792">RUS:gram</ta>
            <ta e="T798" id="Seg_11127" s="T797">TURK:disc</ta>
            <ta e="T802" id="Seg_11128" s="T801">RUS:cult</ta>
            <ta e="T805" id="Seg_11129" s="T804">RUS:gram</ta>
            <ta e="T809" id="Seg_11130" s="T808">RUS:cult</ta>
            <ta e="T818" id="Seg_11131" s="T817">RUS:cult</ta>
            <ta e="T819" id="Seg_11132" s="T818">RUS:gram</ta>
            <ta e="T822" id="Seg_11133" s="T821">RUS:cult</ta>
            <ta e="T827" id="Seg_11134" s="T826">RUS:gram</ta>
            <ta e="T838" id="Seg_11135" s="T837">TURK:disc</ta>
            <ta e="T851" id="Seg_11136" s="T850">RUS:gram</ta>
            <ta e="T854" id="Seg_11137" s="T853">RUS:cult</ta>
            <ta e="T860" id="Seg_11138" s="T859">RUS:gram</ta>
            <ta e="T864" id="Seg_11139" s="T863">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T20" id="Seg_11140" s="T19">RUS:int</ta>
            <ta e="T49" id="Seg_11141" s="T48">RUS:int</ta>
            <ta e="T137" id="Seg_11142" s="T136">RUS:int</ta>
            <ta e="T174" id="Seg_11143" s="T173">RUS:int</ta>
            <ta e="T374" id="Seg_11144" s="T373">RUS:int</ta>
            <ta e="T425" id="Seg_11145" s="T424">RUS:int</ta>
            <ta e="T517" id="Seg_11146" s="T516">RUS:int</ta>
            <ta e="T645" id="Seg_11147" s="T644">RUS:int</ta>
            <ta e="T672" id="Seg_11148" s="T669">RUS:ext</ta>
            <ta e="T678" id="Seg_11149" s="T677">RUS:int</ta>
            <ta e="T841" id="Seg_11150" s="T839">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_11151" s="T0">Мы жили в Свердловске.</ta>
            <ta e="T10" id="Seg_11152" s="T4">Мы сели на железную птицу [=на самолет].</ta>
            <ta e="T12" id="Seg_11153" s="T10">Потом мы полетели.</ta>
            <ta e="T17" id="Seg_11154" s="T12">По дороге (?) мы не могли лететь.</ta>
            <ta e="T19" id="Seg_11155" s="T17">Дымно.</ta>
            <ta e="T22" id="Seg_11156" s="T19">Туман стоит.</ta>
            <ta e="T25" id="Seg_11157" s="T22">Потом мы прилетели в Красноярск.</ta>
            <ta e="T27" id="Seg_11158" s="T25">Там мы сели.</ta>
            <ta e="T29" id="Seg_11159" s="T27">Я там жила.</ta>
            <ta e="T32" id="Seg_11160" s="T29">Семь недель жила.</ta>
            <ta e="T36" id="Seg_11161" s="T32">Потом я села на поезд.</ta>
            <ta e="T38" id="Seg_11162" s="T36">Я приехала в Уяр.</ta>
            <ta e="T41" id="Seg_11163" s="T38">Потом уехала из Уяра.</ta>
            <ta e="T44" id="Seg_11164" s="T41">Села в автобус.</ta>
            <ta e="T46" id="Seg_11165" s="T44">Приехала в Саянск.</ta>
            <ta e="T52" id="Seg_11166" s="T46">Там я жила два дня.</ta>
            <ta e="T58" id="Seg_11167" s="T52">Потом приехала в Абалаково и пришла к тебе.</ta>
            <ta e="T67" id="Seg_11168" s="T58">Пришла к тебе, мы поговорили, потом ты мне (скажешь?):</ta>
            <ta e="T72" id="Seg_11169" s="T67">"Садись, поешь, ты наверное голодная".</ta>
            <ta e="T77" id="Seg_11170" s="T72">Тогда я села, поела, ушла.</ta>
            <ta e="T79" id="Seg_11171" s="T77">На(?) свою квартиру.</ta>
            <ta e="T87" id="Seg_11172" s="T79">Однажды мы, три женщины, мы поехали в Заозерный.</ta>
            <ta e="T93" id="Seg_11173" s="T87">Потом, оттуда, мы сели на поезд.</ta>
            <ta e="T98" id="Seg_11174" s="T93">Мы поехали в Уяр, чтобы там Богу помолиться.</ta>
            <ta e="T101" id="Seg_11175" s="T98">Там мы молились Богу.</ta>
            <ta e="T107" id="Seg_11176" s="T101">Потом мы вернулись туда, в Заозерный.</ta>
            <ta e="T112" id="Seg_11177" s="T107">Там мы переночевали, много разговаривали.</ta>
            <ta e="T115" id="Seg_11178" s="T112">Потом (?) пошла.</ta>
            <ta e="T121" id="Seg_11179" s="T115">Она пошла посмотреть автобус и (?) ушла.</ta>
            <ta e="T127" id="Seg_11180" s="T121">И мы встали и вышли наружу.</ta>
            <ta e="T130" id="Seg_11181" s="T127">Там подошел автобус.</ta>
            <ta e="T133" id="Seg_11182" s="T130">Он посадил нас.</ta>
            <ta e="T135" id="Seg_11183" s="T133">Мы приехали в Переясловку.</ta>
            <ta e="T139" id="Seg_11184" s="T135">Потом нас…</ta>
            <ta e="T145" id="Seg_11185" s="T139">Потом мы вышли из автобуса</ta>
            <ta e="T151" id="Seg_11186" s="T145">Я говорю: "Зачем стоять, пойдем пешком!"</ta>
            <ta e="T154" id="Seg_11187" s="T151">Мы идем.</ta>
            <ta e="T157" id="Seg_11188" s="T154">Машина едет.</ta>
            <ta e="T158" id="Seg_11189" s="T157">"Садитесь!"</ta>
            <ta e="T163" id="Seg_11190" s="T158">"Садитесь сюда!"</ta>
            <ta e="T166" id="Seg_11191" s="T163">Мы сели.</ta>
            <ta e="T169" id="Seg_11192" s="T166">И приехали в Усть-Кандыгу.</ta>
            <ta e="T177" id="Seg_11193" s="T169">И там еще оставалось идти семь верст до Унера.</ta>
            <ta e="T185" id="Seg_11194" s="T177">Две женщины пошли домой, а я пошла пешком.</ta>
            <ta e="T187" id="Seg_11195" s="T185">Пришла в Унер.</ta>
            <ta e="T190" id="Seg_11196" s="T187">"Дайте мне хлеба.</ta>
            <ta e="T192" id="Seg_11197" s="T190">Продайте хлеба".</ta>
            <ta e="T196" id="Seg_11198" s="T192">Они говорит: "Иди.</ta>
            <ta e="T200" id="Seg_11199" s="T196">В магазин, там купи". [?]</ta>
            <ta e="T204" id="Seg_11200" s="T200">"Мне много не надо.</ta>
            <ta e="T207" id="Seg_11201" s="T204">Дайте две[сти] граммов".</ta>
            <ta e="T221" id="Seg_11202" s="T207">Они мне взвесили две[сти] граммов, я взяла, деньги дала, поела и пошла, воды выпила.</ta>
            <ta e="T227" id="Seg_11203" s="T221">Потом меня догнала машина, я села.</ta>
            <ta e="T229" id="Seg_11204" s="T227">Потом вышла.</ta>
            <ta e="T233" id="Seg_11205" s="T229">Два человека стоят на холме.</ta>
            <ta e="T237" id="Seg_11206" s="T233">Я туда поднялась.</ta>
            <ta e="T239" id="Seg_11207" s="T237">Пойдем вместе".</ta>
            <ta e="T245" id="Seg_11208" s="T239">"Нет, нас ждет машина, поехали на машине".</ta>
            <ta e="T251" id="Seg_11209" s="T245">Потом машина пришла, я села.</ta>
            <ta e="T254" id="Seg_11210" s="T251">И приехала в Саянский.</ta>
            <ta e="T260" id="Seg_11211" s="T254">Я (?) купила хлеб, сахар.</ta>
            <ta e="T265" id="Seg_11212" s="T260">Ванька лежал в больнице, я отнесла [их] ему.</ta>
            <ta e="T268" id="Seg_11213" s="T265">Мне там говорят:</ta>
            <ta e="T270" id="Seg_11214" s="T268">"Будьте здесь.</ta>
            <ta e="T273" id="Seg_11215" s="T270">Наша машина скоро (пойдет?)".</ta>
            <ta e="T275" id="Seg_11216" s="T273">Я побежала</ta>
            <ta e="T276" id="Seg_11217" s="T275">Куда-то.</ta>
            <ta e="T278" id="Seg_11218" s="T276">Не нашла [ее].</ta>
            <ta e="T281" id="Seg_11219" s="T278">Тогда я пошла пешком.</ta>
            <ta e="T285" id="Seg_11220" s="T281">Потом опять машина едет.</ta>
            <ta e="T289" id="Seg_11221" s="T285">Я села, приехала в Малиновку.</ta>
            <ta e="T293" id="Seg_11222" s="T289">Потом опять пешком пошла.</ta>
            <ta e="T300" id="Seg_11223" s="T293">Потом [меня] опять машина догнала, и я приехала домой.</ta>
            <ta e="T307" id="Seg_11224" s="T300">Когда Матвеев приехал ко мне, мы говорили (по-камасински?).</ta>
            <ta e="T314" id="Seg_11225" s="T307">А я по-русски говорю и (?).</ta>
            <ta e="T322" id="Seg_11226" s="T314">"Теперь ты камасинец, а я русская".</ta>
            <ta e="T327" id="Seg_11227" s="T322">Потом они пошли в лес, на Белогорье.</ta>
            <ta e="T331" id="Seg_11228" s="T327">Рыбу ловили там.</ta>
            <ta e="T334" id="Seg_11229" s="T331">(…)</ta>
            <ta e="T341" id="Seg_11230" s="T334">Потом одна девушка сильно заболела.</ta>
            <ta e="T344" id="Seg_11231" s="T341">Они пришли.</ta>
            <ta e="T346" id="Seg_11232" s="T344">В [один] дом в Кан-Оклере.</ta>
            <ta e="T353" id="Seg_11233" s="T346">Они взяли женщину и отвезли ее в больницу.</ta>
            <ta e="T358" id="Seg_11234" s="T353">Там ногу (?).</ta>
            <ta e="T360" id="Seg_11235" s="T358">И (?).</ta>
            <ta e="T361" id="Seg_11236" s="T360">Вылечили.</ta>
            <ta e="T368" id="Seg_11237" s="T361">А то она ходить не может [=могла].</ta>
            <ta e="T374" id="Seg_11238" s="T368">Я искала-искала правду. [?]</ta>
            <ta e="T377" id="Seg_11239" s="T374">Я написала (туда?) письмо.</ta>
            <ta e="T380" id="Seg_11240" s="T377">Написала туда. [?]</ta>
            <ta e="T389" id="Seg_11241" s="T380">Кто меня видел, кто меня не видел (?)</ta>
            <ta e="T392" id="Seg_11242" s="T389">Нигде нет правды.</ta>
            <ta e="T399" id="Seg_11243" s="T392">Я думала, везде есть правда.</ta>
            <ta e="T405" id="Seg_11244" s="T399">А ее нигде нет, везде ложь. [?]</ta>
            <ta e="T412" id="Seg_11245" s="T405">Потом прокурор написал, (куда-то?) ушёл, не знаю.</ta>
            <ta e="T415" id="Seg_11246" s="T412">Эля (…)</ta>
            <ta e="T419" id="Seg_11247" s="T415">Они пошли к лодке, к реке.</ta>
            <ta e="T425" id="Seg_11248" s="T419">Хотели с какой-то девушкой по реке плавать.</ta>
            <ta e="T429" id="Seg_11249" s="T425">А река очень большая.</ta>
            <ta e="T434" id="Seg_11250" s="T429">Они хотели в лодку залезть.</ta>
            <ta e="T437" id="Seg_11251" s="T434">Но испугались.</ta>
            <ta e="T439" id="Seg_11252" s="T437">Потом…</ta>
            <ta e="T442" id="Seg_11253" s="T439">Они испугались.</ta>
            <ta e="T448" id="Seg_11254" s="T442">Собрала, видит, что (?) (умер?).</ta>
            <ta e="T453" id="Seg_11255" s="T448">Еще стоит дом Митрофана Соломатова.</ta>
            <ta e="T461" id="Seg_11256" s="T453">И (?) Анджигатова дом еще стоит.</ta>
            <ta e="T468" id="Seg_11257" s="T461">Джибьева Володи дом еще стоит.</ta>
            <ta e="T473" id="Seg_11258" s="T468">Кочерова Никиты дом еще стоит.</ta>
            <ta e="T478" id="Seg_11259" s="T473">(?) Афанасия дом еще стоит.</ta>
            <ta e="T484" id="Seg_11260" s="T478">И дом Власа Шайбина еще стоит.</ta>
            <ta e="T491" id="Seg_11261" s="T484">И дом Василия Соломатова еще стоит.</ta>
            <ta e="T502" id="Seg_11262" s="T491">Когда Тугаринов приезжал сюда разговаривать, тут было пятнадцать/пятьдесят домов.</ta>
            <ta e="T506" id="Seg_11263" s="T502">(Сарголов) Яков Филиппович умер.</ta>
            <ta e="T517" id="Seg_11264" s="T506">Он пошел в Пермяково, там водки выпил, там была одна бутылка.</ta>
            <ta e="T520" id="Seg_11265" s="T517">И с женой пошел.</ta>
            <ta e="T523" id="Seg_11266" s="T520">Потом он пошел на кладбище.</ta>
            <ta e="T527" id="Seg_11267" s="T523">Льет водку и ругается.</ta>
            <ta e="T531" id="Seg_11268" s="T527">"Пей, жена, пей, жена!"</ta>
            <ta e="T539" id="Seg_11269" s="T531">А мой отец стоит, смотрит и смеется.</ta>
            <ta e="T542" id="Seg_11270" s="T539">Я косила траву.</ta>
            <ta e="T544" id="Seg_11271" s="T542">Потом собрала [ее].</ta>
            <ta e="T547" id="Seg_11272" s="T544">Потом сложила в стог.</ta>
            <ta e="T550" id="Seg_11273" s="T547">Она была мокрая.</ta>
            <ta e="T556" id="Seg_11274" s="T550">Потом я три дня [туда] не ходила.</ta>
            <ta e="T559" id="Seg_11275" s="T556">Стало очень жарко.</ta>
            <ta e="T566" id="Seg_11276" s="T559">Я взяла двух женщин и пошла [туда].</ta>
            <ta e="T574" id="Seg_11277" s="T566">Оно высохло, мы его опять собрали в красивый (стог?)</ta>
            <ta e="T579" id="Seg_11278" s="T574">Я жила двадцать лет.</ta>
            <ta e="T582" id="Seg_11279" s="T579">Сама траву косила.</ta>
            <ta e="T584" id="Seg_11280" s="T582">Сама собирала.</ta>
            <ta e="T586" id="Seg_11281" s="T584">Сама складывала [в стога].</ta>
            <ta e="T590" id="Seg_11282" s="T586">Привозила его домой на лошади.</ta>
            <ta e="T594" id="Seg_11283" s="T590">Своих коров кормила.</ta>
            <ta e="T597" id="Seg_11284" s="T594">У меня была одна лошадь.</ta>
            <ta e="T607" id="Seg_11285" s="T597">Я взяла (сына?) племянницы, и мы пошли (?) складывать сено.</ta>
            <ta e="T611" id="Seg_11286" s="T607">Мы шли, шли.</ta>
            <ta e="T614" id="Seg_11287" s="T611">Сено сползло (вниз?). [?]</ta>
            <ta e="T619" id="Seg_11288" s="T614">Мы его поднимали-поднимали, не могли [поднять].</ta>
            <ta e="T626" id="Seg_11289" s="T619">Он говорит: "Я сейчас лошадь запрягу.</ta>
            <ta e="T628" id="Seg_11290" s="T626">И подниму [его]".</ta>
            <ta e="T632" id="Seg_11291" s="T628">"А как ты поднимешь?"</ta>
            <ta e="T642" id="Seg_11292" s="T632">(…)</ta>
            <ta e="T646" id="Seg_11293" s="T642">Потом мы, наверное, пришли. [?]</ta>
            <ta e="T648" id="Seg_11294" s="T646">Отдали(?) сено.</ta>
            <ta e="T651" id="Seg_11295" s="T648">Лошадь (высушили?).</ta>
            <ta e="T654" id="Seg_11296" s="T651">Я очень замерзла.</ta>
            <ta e="T657" id="Seg_11297" s="T654">Потом начался кашель.</ta>
            <ta e="T663" id="Seg_11298" s="T657">Потом я подожгла сахар в водке.</ta>
            <ta e="T669" id="Seg_11299" s="T663">Потом я выпила, и куда-то пропал мой кашель.</ta>
            <ta e="T672" id="Seg_11300" s="T669">Не стало кашля.</ta>
            <ta e="T678" id="Seg_11301" s="T672">Такая трава есть, крапива.</ta>
            <ta e="T684" id="Seg_11302" s="T678">Ее нужно взять за стебель, потянуть.</ta>
            <ta e="T691" id="Seg_11303" s="T684">(…) тогда не будет кашля.</ta>
            <ta e="T698" id="Seg_11304" s="T691">[Когда] она горит, и нет ни дыма, ни тепла. [?]</ta>
            <ta e="T703" id="Seg_11305" s="T698">Сегодня мне не надо рубить (дрова?).</ta>
            <ta e="T707" id="Seg_11306" s="T703">На сегодняшную ночь хватит.</ta>
            <ta e="T710" id="Seg_11307" s="T707">Дров у меня много.</ta>
            <ta e="T714" id="Seg_11308" s="T710">На этот год еще хватит.</ta>
            <ta e="T716" id="Seg_11309" s="T714">Очень холодно.</ta>
            <ta e="T719" id="Seg_11310" s="T716">Много дров уходит.</ta>
            <ta e="T724" id="Seg_11311" s="T719">Дрова от дождя черные стали.</ta>
            <ta e="T727" id="Seg_11312" s="T724">А то белые были.</ta>
            <ta e="T731" id="Seg_11313" s="T727">У меня (нарублены?) дрова из лиственницы.</ta>
            <ta e="T738" id="Seg_11314" s="T731">Белые [=березовые?] дрова (?) очень хорошие.</ta>
            <ta e="T744" id="Seg_11315" s="T738">Хорошо горят, и тепло (?).</ta>
            <ta e="T752" id="Seg_11316" s="T744">Ставишь варить, и уголь туда не лезет.</ta>
            <ta e="T757" id="Seg_11317" s="T752">(…) очень много угля, его (выбрасывают?)</ta>
            <ta e="T761" id="Seg_11318" s="T757">(?) меня Васька Шайбин.</ta>
            <ta e="T764" id="Seg_11319" s="T761">(?) тоже ты.</ta>
            <ta e="T771" id="Seg_11320" s="T764">И (?), а я сама положила.</ta>
            <ta e="T773" id="Seg_11321" s="T771">На тракторе привезли.</ta>
            <ta e="T776" id="Seg_11322" s="T773">Я(?) купил масло для трактора.</ta>
            <ta e="T785" id="Seg_11323" s="T776">(…) а я дрова возила, за водой ходила.</ta>
            <ta e="T788" id="Seg_11324" s="T785">Корове (сено?) давала.</ta>
            <ta e="T792" id="Seg_11325" s="T788">Доила, потом садилась есть.</ta>
            <ta e="T794" id="Seg_11326" s="T792">И думаю:</ta>
            <ta e="T799" id="Seg_11327" s="T794">Эта Эля мня обманула.</ta>
            <ta e="T804" id="Seg_11328" s="T799">У нее было три рубля.</ta>
            <ta e="T812" id="Seg_11329" s="T804">А у меня был рубль двадцать пять [копеек].</ta>
            <ta e="T822" id="Seg_11330" s="T812">Она должна была взять у меня (?) рубль пятнадцать копеек.</ta>
            <ta e="T830" id="Seg_11331" s="T822">Я сегодня ходила на речку и совсем (замерзла?).</ta>
            <ta e="T839" id="Seg_11332" s="T830">Я вернулась, взяла топор и топором рубила-рубила снег.</ta>
            <ta e="T846" id="Seg_11333" s="T839">Он замерз на целый вершок (?), очень холодно утро было.</ta>
            <ta e="T858" id="Seg_11334" s="T846">Я домой пришла, я (вас?) ждала, воду принесла, залезла на печь.</ta>
            <ta e="T861" id="Seg_11335" s="T858">Я села и (?).</ta>
            <ta e="T865" id="Seg_11336" s="T861">Потом они пришли и (?).</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_11337" s="T0">We lived in Sverdlovsk.</ta>
            <ta e="T10" id="Seg_11338" s="T4">We sat on an iron bird [= got on a plane].</ta>
            <ta e="T12" id="Seg_11339" s="T10">Then we flew.</ta>
            <ta e="T17" id="Seg_11340" s="T12">On the way (?) [it] cannot fly.</ta>
            <ta e="T19" id="Seg_11341" s="T17">It is smoky.</ta>
            <ta e="T22" id="Seg_11342" s="T19">There is a mist.</ta>
            <ta e="T25" id="Seg_11343" s="T22">Then we came to Krasnoyarsk.</ta>
            <ta e="T27" id="Seg_11344" s="T25">We landed there.</ta>
            <ta e="T29" id="Seg_11345" s="T27">I lived there.</ta>
            <ta e="T32" id="Seg_11346" s="T29">I lived seven days.</ta>
            <ta e="T36" id="Seg_11347" s="T32">Then I took a train.</ta>
            <ta e="T38" id="Seg_11348" s="T36">I came to Uyar.</ta>
            <ta e="T41" id="Seg_11349" s="T38">Then I left Uyar.</ta>
            <ta e="T44" id="Seg_11350" s="T41">I took a bus.</ta>
            <ta e="T46" id="Seg_11351" s="T44">I came to Sayansk.</ta>
            <ta e="T52" id="Seg_11352" s="T46">There I lived for two days.</ta>
            <ta e="T58" id="Seg_11353" s="T52">Then I came to Abalakovo and came to you.</ta>
            <ta e="T67" id="Seg_11354" s="T58">I came to you, we spoke, then you will (say?) me:</ta>
            <ta e="T72" id="Seg_11355" s="T67">"Sit down, eat, you must be hungry."</ta>
            <ta e="T77" id="Seg_11356" s="T72">Then I sat down, I ate, I went.</ta>
            <ta e="T79" id="Seg_11357" s="T77">To(?) my own lodgings.</ta>
            <ta e="T87" id="Seg_11358" s="T79">Once, three women, we went to Zaozerny.</ta>
            <ta e="T93" id="Seg_11359" s="T87">Then, from there, we took a train.</ta>
            <ta e="T98" id="Seg_11360" s="T93">We went to Uyar, to pray to God there.</ta>
            <ta e="T101" id="Seg_11361" s="T98">There we prayed to God.</ta>
            <ta e="T107" id="Seg_11362" s="T101">Then we returned back there, to Zaozerny.</ta>
            <ta e="T112" id="Seg_11363" s="T107">There we passed the night, we spoke a lot.</ta>
            <ta e="T115" id="Seg_11364" s="T112">Then (?) went.</ta>
            <ta e="T121" id="Seg_11365" s="T115">She came to look for a bus (?) and went to (?).</ta>
            <ta e="T127" id="Seg_11366" s="T121">And we stand up and went outside.</ta>
            <ta e="T130" id="Seg_11367" s="T127">There came an autobus.</ta>
            <ta e="T133" id="Seg_11368" s="T130">It seated us [=took us onboard], too.</ta>
            <ta e="T135" id="Seg_11369" s="T133">We came to Pereyaslovka.</ta>
            <ta e="T139" id="Seg_11370" s="T135">There we were…</ta>
            <ta e="T145" id="Seg_11371" s="T139">There we got off [from the bus] and stopped.</ta>
            <ta e="T151" id="Seg_11372" s="T145">I say: "Why would [we] stay, let's go by foot!"</ta>
            <ta e="T154" id="Seg_11373" s="T151">Then we're going.</ta>
            <ta e="T157" id="Seg_11374" s="T154">A car is coming.</ta>
            <ta e="T158" id="Seg_11375" s="T157">"Get in!"</ta>
            <ta e="T163" id="Seg_11376" s="T158">"Get in here!"</ta>
            <ta e="T166" id="Seg_11377" s="T163">Then we got in.</ta>
            <ta e="T169" id="Seg_11378" s="T166">And we came to Ust'-Kandyga.</ta>
            <ta e="T177" id="Seg_11379" s="T169">Then there left still seven versta till Uner.</ta>
            <ta e="T185" id="Seg_11380" s="T177">Two women went home, and I went by foot.</ta>
            <ta e="T187" id="Seg_11381" s="T185">I came to Uner.</ta>
            <ta e="T190" id="Seg_11382" s="T187">"Give me bread.</ta>
            <ta e="T192" id="Seg_11383" s="T190">Sell [me] bread."</ta>
            <ta e="T196" id="Seg_11384" s="T192">They said: "Go.</ta>
            <ta e="T200" id="Seg_11385" s="T196">Take [it] at the shop!" [?]</ta>
            <ta e="T204" id="Seg_11386" s="T200">"I don't need much.</ta>
            <ta e="T207" id="Seg_11387" s="T204">Give me two [hundred] grams."</ta>
            <ta e="T221" id="Seg_11388" s="T207">He weighed me two [hundred] gramm, I took [it], gave out the money, ate [the bread] and went, drank water.</ta>
            <ta e="T227" id="Seg_11389" s="T221">Then a car caught me up, I got in.</ta>
            <ta e="T229" id="Seg_11390" s="T227">Then I got out.</ta>
            <ta e="T233" id="Seg_11391" s="T229">Two men stand on the hill.</ta>
            <ta e="T237" id="Seg_11392" s="T233">I climbed there.</ta>
            <ta e="T239" id="Seg_11393" s="T237">"Let's go (together?)."</ta>
            <ta e="T245" id="Seg_11394" s="T239">"No, a car is waiting for us, let's go by car."</ta>
            <ta e="T251" id="Seg_11395" s="T245">Then the car came, I got in.</ta>
            <ta e="T254" id="Seg_11396" s="T251">And I came to Sayanskij.</ta>
            <ta e="T260" id="Seg_11397" s="T254">Then (?) I took bread, sugar.</ta>
            <ta e="T265" id="Seg_11398" s="T260">Van'ka is in the hospital, I carried [them] to him.</ta>
            <ta e="T268" id="Seg_11399" s="T265">There they say me:</ta>
            <ta e="T270" id="Seg_11400" s="T268">"Be here.</ta>
            <ta e="T273" id="Seg_11401" s="T270">Our car is going (soon?)."</ta>
            <ta e="T275" id="Seg_11402" s="T273">I ran.</ta>
            <ta e="T276" id="Seg_11403" s="T275">Somewhere.</ta>
            <ta e="T278" id="Seg_11404" s="T276">I didn't find [it].</ta>
            <ta e="T281" id="Seg_11405" s="T278">Then I went by foot.</ta>
            <ta e="T285" id="Seg_11406" s="T281">Then comes another car.</ta>
            <ta e="T289" id="Seg_11407" s="T285">I got in, came to Malinovka.</ta>
            <ta e="T293" id="Seg_11408" s="T289">Then I went by foot again.</ta>
            <ta e="T300" id="Seg_11409" s="T293">Then a car caught [me] up again, and I came home.</ta>
            <ta e="T307" id="Seg_11410" s="T300">When Matveev came to me, we spoke (Kamassian?).</ta>
            <ta e="T314" id="Seg_11411" s="T307">And I spoke Russian and was (?)-ing.</ta>
            <ta e="T322" id="Seg_11412" s="T314">"Now you are Kamassian, and I am Russian."</ta>
            <ta e="T327" id="Seg_11413" s="T322">Then they went to the forest, to the Sayan mountains.</ta>
            <ta e="T331" id="Seg_11414" s="T327">They caught fish there.</ta>
            <ta e="T334" id="Seg_11415" s="T331">(…)</ta>
            <ta e="T341" id="Seg_11416" s="T334">Then one girl fell seriously ill.</ta>
            <ta e="T344" id="Seg_11417" s="T341">Then they came.</ta>
            <ta e="T346" id="Seg_11418" s="T344">In a house in Kan-Okler.</ta>
            <ta e="T353" id="Seg_11419" s="T346">And they took the woman and brought her to the hospital.</ta>
            <ta e="T358" id="Seg_11420" s="T353">There they (?) foor.</ta>
            <ta e="T360" id="Seg_11421" s="T358">And (?).</ta>
            <ta e="T361" id="Seg_11422" s="T360">They healed [it].</ta>
            <ta e="T368" id="Seg_11423" s="T361">Because she can't [= couldn't] walk.</ta>
            <ta e="T374" id="Seg_11424" s="T368">I searched the truth. [?]</ta>
            <ta e="T377" id="Seg_11425" s="T374">I wrote (there?) a letter.</ta>
            <ta e="T380" id="Seg_11426" s="T377">I wrote there. [?]</ta>
            <ta e="T389" id="Seg_11427" s="T380">Who saw me, who didn't see me. [?]</ta>
            <ta e="T392" id="Seg_11428" s="T389">There is no truth anywhere.</ta>
            <ta e="T399" id="Seg_11429" s="T392">I thought, everywhere is the truth.</ta>
            <ta e="T405" id="Seg_11430" s="T399">But it isn't anywhere, [people] always (lies?).</ta>
            <ta e="T412" id="Seg_11431" s="T405">Then the prosecutor (wrote?) and went somewhere, I don't know.</ta>
            <ta e="T415" id="Seg_11432" s="T412">Elya (…)</ta>
            <ta e="T419" id="Seg_11433" s="T415">They went to the boat, to the river. [?]</ta>
            <ta e="T425" id="Seg_11434" s="T419">Then they wanted to swim in the water with some girl.</ta>
            <ta e="T429" id="Seg_11435" s="T425">But the water is very high.</ta>
            <ta e="T434" id="Seg_11436" s="T429">They wanted to get in a boat.</ta>
            <ta e="T437" id="Seg_11437" s="T434">But they were frightened.</ta>
            <ta e="T439" id="Seg_11438" s="T437">Then…</ta>
            <ta e="T442" id="Seg_11439" s="T439">They were frightened</ta>
            <ta e="T448" id="Seg_11440" s="T442">[S/he] stand up [and] sees that (?) (died?).</ta>
            <ta e="T453" id="Seg_11441" s="T448">There stands also Mitrofan Solomatov's house.</ta>
            <ta e="T461" id="Seg_11442" s="T453">And there stands also (?) Andzhigatov's house.</ta>
            <ta e="T468" id="Seg_11443" s="T461">There stands also Volodya Dzhibiev's house.</ta>
            <ta e="T473" id="Seg_11444" s="T468">There stands also Nikita Kocherov's house.</ta>
            <ta e="T478" id="Seg_11445" s="T473">There stands also (?) Afanasij's house.</ta>
            <ta e="T484" id="Seg_11446" s="T478">And there stands also Vlas Shajbin's house.</ta>
            <ta e="T491" id="Seg_11447" s="T484">And there stands also Vasilij Solomatov's house.</ta>
            <ta e="T502" id="Seg_11448" s="T491">When Tugarinov came to speak here, here were fifteen/fifty houses.</ta>
            <ta e="T506" id="Seg_11449" s="T502">Yakov Filippovich (Sargolov) died.</ta>
            <ta e="T517" id="Seg_11450" s="T506">He went to Permyakovo,drank vodka there, there was one bottle.</ta>
            <ta e="T520" id="Seg_11451" s="T517">He went with his wife.</ta>
            <ta e="T523" id="Seg_11452" s="T520">Then he went to the cemetery.</ta>
            <ta e="T527" id="Seg_11453" s="T523">Then he pours [vodka?] and (scolds?):</ta>
            <ta e="T531" id="Seg_11454" s="T527">"Drink, my wife, drink, my wife!"</ta>
            <ta e="T539" id="Seg_11455" s="T531">And my father stands, looks and laughs.</ta>
            <ta e="T542" id="Seg_11456" s="T539">I cut grass.</ta>
            <ta e="T544" id="Seg_11457" s="T542">Then I gathered [it].</ta>
            <ta e="T547" id="Seg_11458" s="T544">Then I put [it] into a haystack.</ta>
            <ta e="T550" id="Seg_11459" s="T547">It was humid.</ta>
            <ta e="T556" id="Seg_11460" s="T550">Then I didn't went [there] three days.</ta>
            <ta e="T559" id="Seg_11461" s="T556">It became very hot.</ta>
            <ta e="T566" id="Seg_11462" s="T559">I took two women and went [there].</ta>
            <ta e="T574" id="Seg_11463" s="T566">It dried, and we put it again in a beatiful (haystack?).</ta>
            <ta e="T579" id="Seg_11464" s="T574">I lived twenty years.</ta>
            <ta e="T582" id="Seg_11465" s="T579">I cut hay myself.</ta>
            <ta e="T584" id="Seg_11466" s="T582">I gathered [it] myself.</ta>
            <ta e="T586" id="Seg_11467" s="T584">I put [it] myself [into stacks].</ta>
            <ta e="T590" id="Seg_11468" s="T586">I carried it home with a horse.</ta>
            <ta e="T594" id="Seg_11469" s="T590">I feeded my cows.</ta>
            <ta e="T597" id="Seg_11470" s="T594">I had one horse.</ta>
            <ta e="T607" id="Seg_11471" s="T597">Then I took [my] niece's (son?) and we went (?), to put the hay [into stacks].</ta>
            <ta e="T611" id="Seg_11472" s="T607">Then we walked and walked.</ta>
            <ta e="T614" id="Seg_11473" s="T611">The hay fell down. [?]</ta>
            <ta e="T619" id="Seg_11474" s="T614">Then we [tried to] lift [it], [but] could not.</ta>
            <ta e="T626" id="Seg_11475" s="T619">[He] says: "Now I'll harness the horse.</ta>
            <ta e="T628" id="Seg_11476" s="T626">Then I'll lift [it]."</ta>
            <ta e="T632" id="Seg_11477" s="T628">"But how will you lift it?"</ta>
            <ta e="T642" id="Seg_11478" s="T632">(…)</ta>
            <ta e="T646" id="Seg_11479" s="T642">Then we probably came. [?]</ta>
            <ta e="T648" id="Seg_11480" s="T646">We (gave?) the hay.</ta>
            <ta e="T651" id="Seg_11481" s="T648">Then we (dried?) the horse.</ta>
            <ta e="T654" id="Seg_11482" s="T651">I was very cold.</ta>
            <ta e="T657" id="Seg_11483" s="T654">Then I had a cough.</ta>
            <ta e="T663" id="Seg_11484" s="T657">Then I burned sugar in[to] the vodka.</ta>
            <ta e="T669" id="Seg_11485" s="T663">Then I drank [it], and my cough disappeared somewhere.</ta>
            <ta e="T672" id="Seg_11486" s="T669">The cough disappeared.</ta>
            <ta e="T678" id="Seg_11487" s="T672">There is a grass, nettle.</ta>
            <ta e="T684" id="Seg_11488" s="T678">One should take, pick it by the stem.</ta>
            <ta e="T691" id="Seg_11489" s="T684">(…) then the cough will disappear.</ta>
            <ta e="T698" id="Seg_11490" s="T691">[When] it burns, there is neither smoke no heat. [?]</ta>
            <ta e="T703" id="Seg_11491" s="T698">I don't have to (chop?) wood today.</ta>
            <ta e="T707" id="Seg_11492" s="T703">It'll be enough for this evening.</ta>
            <ta e="T710" id="Seg_11493" s="T707">I have a lot of wood.</ta>
            <ta e="T714" id="Seg_11494" s="T710">It will be enough for this year.</ta>
            <ta e="T716" id="Seg_11495" s="T714">It is very cold.</ta>
            <ta e="T719" id="Seg_11496" s="T716">A lot of wood is spent.</ta>
            <ta e="T724" id="Seg_11497" s="T719">The wood became black because of the rain.</ta>
            <ta e="T727" id="Seg_11498" s="T724">[Before] it was white.</ta>
            <ta e="T731" id="Seg_11499" s="T727">I(?) chopped (?) larch wood.</ta>
            <ta e="T738" id="Seg_11500" s="T731">White wood [= birch?] (?) is very good.</ta>
            <ta e="T744" id="Seg_11501" s="T738">They burn good, and (?) warmth.</ta>
            <ta e="T752" id="Seg_11502" s="T744">You boil [something] to see, and there is no coal in.</ta>
            <ta e="T757" id="Seg_11503" s="T752">(…) there is very much coal, you throw it (away?).</ta>
            <ta e="T761" id="Seg_11504" s="T757">(?) me Vas'ka Shaibin.</ta>
            <ta e="T764" id="Seg_11505" s="T761">(?) (you?), too.</ta>
            <ta e="T771" id="Seg_11506" s="T764">And (?), and I put myself.</ta>
            <ta e="T773" id="Seg_11507" s="T771">They brought [it] with a tractor.</ta>
            <ta e="T776" id="Seg_11508" s="T773">I (bought?) the oil for the tractor. [?]</ta>
            <ta e="T785" id="Seg_11509" s="T776">(…), and I carried wood, went to fetch water.</ta>
            <ta e="T788" id="Seg_11510" s="T785">I gave (hay?) to the cow.</ta>
            <ta e="T792" id="Seg_11511" s="T788">I milked [it], then I sat down to eat.</ta>
            <ta e="T794" id="Seg_11512" s="T792">And I think:</ta>
            <ta e="T799" id="Seg_11513" s="T794">This Elya lied to me.</ta>
            <ta e="T804" id="Seg_11514" s="T799">She had three roubles.</ta>
            <ta e="T812" id="Seg_11515" s="T804">And I had one rouble twenty-five [kopecks].</ta>
            <ta e="T822" id="Seg_11516" s="T812">She should have taken (?) one rouble fifteen kopecks from me.</ta>
            <ta e="T830" id="Seg_11517" s="T822">Today I went to the river, and it was completely frozen. [?]</ta>
            <ta e="T839" id="Seg_11518" s="T830">I returned, took the axe and with the axe I chopped and chopped the snow.</ta>
            <ta e="T846" id="Seg_11519" s="T839">It was frozen to as deep as a vershok (?), the morning was very cold.</ta>
            <ta e="T858" id="Seg_11520" s="T846">I came home, I was waiting for (you?), I brought water, climbed on the overn.</ta>
            <ta e="T861" id="Seg_11521" s="T858">I sat down and (?).</ta>
            <ta e="T865" id="Seg_11522" s="T861">Then came and (?).</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_11523" s="T0">Wir wohnten in Sverdlovsk.</ta>
            <ta e="T10" id="Seg_11524" s="T4">Wir saßen auf einem eisernen Vogel [= saßen im Flugzeug].</ta>
            <ta e="T12" id="Seg_11525" s="T10">Dann flogen wir.</ta>
            <ta e="T17" id="Seg_11526" s="T12">Auf dem Weg kann [es] nicht fliegen.</ta>
            <ta e="T19" id="Seg_11527" s="T17">Es ist dunstig.</ta>
            <ta e="T22" id="Seg_11528" s="T19">Dort ist Nebel.</ta>
            <ta e="T25" id="Seg_11529" s="T22">Dann kamen wir nach Krasnojarsk.</ta>
            <ta e="T27" id="Seg_11530" s="T25">Wir sind dort gelandet.</ta>
            <ta e="T29" id="Seg_11531" s="T27">Ich wohnte dort.</ta>
            <ta e="T32" id="Seg_11532" s="T29">Ich wohnte dort sieben Tage.</ta>
            <ta e="T36" id="Seg_11533" s="T32">Dann nahm ich den Zug.</ta>
            <ta e="T38" id="Seg_11534" s="T36">Ich kam nach Ujar.</ta>
            <ta e="T41" id="Seg_11535" s="T38">Dann verließ ich Ujar.</ta>
            <ta e="T44" id="Seg_11536" s="T41">Ich nahm den Bus.</ta>
            <ta e="T46" id="Seg_11537" s="T44">Ich kam nach Sajansk.</ta>
            <ta e="T52" id="Seg_11538" s="T46">Dort wohnte ich zwei Tage.</ta>
            <ta e="T58" id="Seg_11539" s="T52">Dann kam ich nach Abalakovo und kam zu dir.</ta>
            <ta e="T67" id="Seg_11540" s="T58">Ich kam zu dir, wir sprachen, dann wirst du mir (sagen?):</ta>
            <ta e="T72" id="Seg_11541" s="T67">"Setz dich, iss, du musst Hunger haben."</ta>
            <ta e="T77" id="Seg_11542" s="T72">Dann setzte ich mich, ich aß, ich ging.</ta>
            <ta e="T79" id="Seg_11543" s="T77">Nach(?) meine eigene Wohnung.</ta>
            <ta e="T87" id="Seg_11544" s="T79">Einmal, drei Frauen, wir fuhren nach Zaozerny.</ta>
            <ta e="T93" id="Seg_11545" s="T87">Dann, von dort, nahmen wir den Zug.</ta>
            <ta e="T98" id="Seg_11546" s="T93">Wir fuhren nach Ujar, um dort zu Gott zu beten.</ta>
            <ta e="T101" id="Seg_11547" s="T98">Dort beteten wir zu Gott.</ta>
            <ta e="T107" id="Seg_11548" s="T101">Dann kehrten wir dorthin zurück, nach Zaozerny.</ta>
            <ta e="T112" id="Seg_11549" s="T107">Dort verbrachten wir die Nacht, wir redeten eine Menge.</ta>
            <ta e="T115" id="Seg_11550" s="T112">Dann (?) ging.</ta>
            <ta e="T121" id="Seg_11551" s="T115">Sie kam, um den Bus zu suchen (?) und ging nach (?).</ta>
            <ta e="T127" id="Seg_11552" s="T121">Und wir stehen auf und gehen hinaus.</ta>
            <ta e="T130" id="Seg_11553" s="T127">Es kam ein Bus.</ta>
            <ta e="T133" id="Seg_11554" s="T130">Es setzte [= nahm] uns auch.</ta>
            <ta e="T135" id="Seg_11555" s="T133">Wir kamen nach Perejaslovka.</ta>
            <ta e="T139" id="Seg_11556" s="T135">Dort wurden wir…</ta>
            <ta e="T145" id="Seg_11557" s="T139">Dort stiegen wir aus und hielten an.</ta>
            <ta e="T151" id="Seg_11558" s="T145">Ich sage: "Warum halten wir an, lass uns zu Fuß gehen!"</ta>
            <ta e="T154" id="Seg_11559" s="T151">Dann gehen wir.</ta>
            <ta e="T157" id="Seg_11560" s="T154">Ein Auto kommt.</ta>
            <ta e="T158" id="Seg_11561" s="T157">"Steigt ein!"</ta>
            <ta e="T163" id="Seg_11562" s="T158">"Steigt hier ein!"</ta>
            <ta e="T166" id="Seg_11563" s="T163">Dann stiegen wir ein.</ta>
            <ta e="T169" id="Seg_11564" s="T166">Und wir kamen nach Ust'-Kandyga.</ta>
            <ta e="T177" id="Seg_11565" s="T169">Dann blieben noch sieben Werst bis Uner übrig.</ta>
            <ta e="T185" id="Seg_11566" s="T177">Zwei Frauen gingen nach Hause und ich ging zu Fuß.</ta>
            <ta e="T187" id="Seg_11567" s="T185">Ich kam nach Uner.</ta>
            <ta e="T190" id="Seg_11568" s="T187">"Gebt mir Brot.</ta>
            <ta e="T192" id="Seg_11569" s="T190">Verkauft [mir] Brot."</ta>
            <ta e="T196" id="Seg_11570" s="T192">Sie sagten: "Geh.</ta>
            <ta e="T200" id="Seg_11571" s="T196">Hol [es] im Laden!" [?]</ta>
            <ta e="T204" id="Seg_11572" s="T200">"Ich brauche nicht viel.</ta>
            <ta e="T207" id="Seg_11573" s="T204">Gib mir zwei[hundert] Gramm."</ta>
            <ta e="T221" id="Seg_11574" s="T207">Er wog mir zwei[hundert] Gramm ab, ich nahm [es], gab das Geld, aß [das Brot] und ging, ich trank Wasser.</ta>
            <ta e="T227" id="Seg_11575" s="T221">Dann sammelte mich ein Auto auf, ich stieg ein.</ta>
            <ta e="T229" id="Seg_11576" s="T227">Dann stieg ich aus.</ta>
            <ta e="T233" id="Seg_11577" s="T229">Zwei Männer standen auf einem Hügel.</ta>
            <ta e="T237" id="Seg_11578" s="T233">Ich kletterte hinauf.</ta>
            <ta e="T239" id="Seg_11579" s="T237">"Lass uns (zusammen?) gehen."</ta>
            <ta e="T245" id="Seg_11580" s="T239">"Nein ein Auto wartet auf uns, lass uns mit dem Auto fahren."</ta>
            <ta e="T251" id="Seg_11581" s="T245">Dann kam das Auto, ich stieg ein.</ta>
            <ta e="T254" id="Seg_11582" s="T251">Und ich kam nach Sajanskij.</ta>
            <ta e="T260" id="Seg_11583" s="T254">Dann (?) nahm ich Brot, Zucker.</ta>
            <ta e="T265" id="Seg_11584" s="T260">Van'ka ist im Krankenhaus, ich trug [sie] zu ihm.</ta>
            <ta e="T268" id="Seg_11585" s="T265">Dort sagen sie mir:</ta>
            <ta e="T270" id="Seg_11586" s="T268">"Seien Sie hier.</ta>
            <ta e="T273" id="Seg_11587" s="T270">Unseres Auto fährt (ab?)."</ta>
            <ta e="T275" id="Seg_11588" s="T273">Ich rannte.</ta>
            <ta e="T276" id="Seg_11589" s="T275">Irgendwohin.</ta>
            <ta e="T278" id="Seg_11590" s="T276">Ich habe [es] nicht gefunden.</ta>
            <ta e="T281" id="Seg_11591" s="T278">Dann ging ich zu Fuß.</ta>
            <ta e="T285" id="Seg_11592" s="T281">Dann kam ein anderes Auto.</ta>
            <ta e="T289" id="Seg_11593" s="T285">Ich stieg ein, ich kam nach Malinovka.</ta>
            <ta e="T293" id="Seg_11594" s="T289">Dann ging ich wieder zu Fuß.</ta>
            <ta e="T300" id="Seg_11595" s="T293">Dann sammelte [mich] wieder ein Auto ein und ich kam nach Hause.</ta>
            <ta e="T307" id="Seg_11596" s="T300">Als Matveev zu mir kam, sprachen wir (Kamassisch?).</ta>
            <ta e="T314" id="Seg_11597" s="T307">Und ich sprach Russisch und (?).</ta>
            <ta e="T322" id="Seg_11598" s="T314">"Jetzt bist du Kamasse und ich bin Russin."</ta>
            <ta e="T327" id="Seg_11599" s="T322">Dann gingen sie in den Wald, in die Sajan-Berge.</ta>
            <ta e="T331" id="Seg_11600" s="T327">Sie fingen dort Fisch.</ta>
            <ta e="T334" id="Seg_11601" s="T331">(…)</ta>
            <ta e="T341" id="Seg_11602" s="T334">Dann wurde ein Mädchen ernsthaft krank.</ta>
            <ta e="T344" id="Seg_11603" s="T341">Dann kamen sie.</ta>
            <ta e="T346" id="Seg_11604" s="T344">In einem Haus in Kan-Okler.</ta>
            <ta e="T353" id="Seg_11605" s="T346">Und sie nahmen die Frau und brachten sie ins Krankenhaus.</ta>
            <ta e="T358" id="Seg_11606" s="T353">Dort sie (?) Fuß.</ta>
            <ta e="T360" id="Seg_11607" s="T358">Und (?).</ta>
            <ta e="T361" id="Seg_11608" s="T360">Sie heilten [sie].</ta>
            <ta e="T368" id="Seg_11609" s="T361">Weil sie nicht gehen kann [= konnte].</ta>
            <ta e="T374" id="Seg_11610" s="T368">Ich suchte und suchte die Wahrheit. [?]</ta>
            <ta e="T377" id="Seg_11611" s="T374">Ich schrieb einen Brief (dorthin?).</ta>
            <ta e="T380" id="Seg_11612" s="T377">Ich schrieb dorthin. [?]</ta>
            <ta e="T389" id="Seg_11613" s="T380">Wer mich sah, wer mich nicht sah. [?]</ta>
            <ta e="T392" id="Seg_11614" s="T389">Nirgendwo gibt es Wahrheit.</ta>
            <ta e="T399" id="Seg_11615" s="T392">Ich dachte, überall sei die Wahrheit.</ta>
            <ta e="T405" id="Seg_11616" s="T399">Aber sie ist nirgendwo, [die Leute] (lügen?) immer.</ta>
            <ta e="T412" id="Seg_11617" s="T405">Dann (schrieb?) der Staatsanwalt und ging irgendwohin, ich weiß es nicht.</ta>
            <ta e="T415" id="Seg_11618" s="T412">Elya (…)</ta>
            <ta e="T419" id="Seg_11619" s="T415">Sie gingen zum Boot, zum Fluß. [?]</ta>
            <ta e="T425" id="Seg_11620" s="T419">Dann wollten sie im Wasser mit einem Mädchen schwimmen.</ta>
            <ta e="T429" id="Seg_11621" s="T425">Aber das Wasser ist sehr hoch.</ta>
            <ta e="T434" id="Seg_11622" s="T429">Sie wollten in ein Boot steigen.</ta>
            <ta e="T437" id="Seg_11623" s="T434">Aber sie erschraken.</ta>
            <ta e="T439" id="Seg_11624" s="T437">Dann…</ta>
            <ta e="T442" id="Seg_11625" s="T439">Dann sie erschraken.</ta>
            <ta e="T448" id="Seg_11626" s="T442">Er/sie steht auf [und] sieht dass (?) (starb?).</ta>
            <ta e="T453" id="Seg_11627" s="T448">Dort steht auch Mitrofan Solomatows Haus.</ta>
            <ta e="T461" id="Seg_11628" s="T453">Und dort steht auch (?) Andschigatows Haus.</ta>
            <ta e="T468" id="Seg_11629" s="T461">Dort steht auch Volodja Dschibiews Haus.</ta>
            <ta e="T473" id="Seg_11630" s="T468">Dort steht auch Nikita Kotscherows Haus.</ta>
            <ta e="T478" id="Seg_11631" s="T473">Dort steht auch (?) Afanasijs Haus.</ta>
            <ta e="T484" id="Seg_11632" s="T478">Und dort steht auch Wlas Schajbins Haus.</ta>
            <ta e="T491" id="Seg_11633" s="T484">Und dort steht auch Vasilij Solomatows Haus.</ta>
            <ta e="T502" id="Seg_11634" s="T491">Als Tugarinow herkam, um zu sprechen, gab es hier fünfzehn/fünfzig Häuser.</ta>
            <ta e="T506" id="Seg_11635" s="T502">Jakov Filippowitsch (Sargolow) ist gestorben.</ta>
            <ta e="T517" id="Seg_11636" s="T506">Er ging nach Permjakowo, trank dort Wodka, dort gab es eine Flasche.</ta>
            <ta e="T520" id="Seg_11637" s="T517">Er ging mit seiner Frau.</ta>
            <ta e="T523" id="Seg_11638" s="T520">Dann ging er zum Friedhof.</ta>
            <ta e="T527" id="Seg_11639" s="T523">Dann gießt es [Wodka?] und (schimpft?):</ta>
            <ta e="T531" id="Seg_11640" s="T527">"Trink, meine Frau, trink, meine Frau!"</ta>
            <ta e="T539" id="Seg_11641" s="T531">Und mein Vater steht da, schaut und lacht.</ta>
            <ta e="T542" id="Seg_11642" s="T539">Ich schnitt Gras.</ta>
            <ta e="T544" id="Seg_11643" s="T542">Dann sammelte ich [es].</ta>
            <ta e="T547" id="Seg_11644" s="T544">Dann tat ich [es] auf einen Heuhaufen.</ta>
            <ta e="T550" id="Seg_11645" s="T547">Es war feucht.</ta>
            <ta e="T556" id="Seg_11646" s="T550">Dann ging ich drei Tage lang nicht dorthin.</ta>
            <ta e="T559" id="Seg_11647" s="T556">Es wurde sehr heiß.</ta>
            <ta e="T566" id="Seg_11648" s="T559">Ich nahm zwei Frauen und ging [dorthin].</ta>
            <ta e="T574" id="Seg_11649" s="T566">Es war getrocknet und wir legten es wieder auf einen schönen (Heuhaufen?).</ta>
            <ta e="T579" id="Seg_11650" s="T574">Ich lebte zwanzig Jahre.</ta>
            <ta e="T582" id="Seg_11651" s="T579">Ich schnitt selber Heu.</ta>
            <ta e="T584" id="Seg_11652" s="T582">Ich sammelte [es] selber.</ta>
            <ta e="T586" id="Seg_11653" s="T584">Ich legte [es] selber [auf Haufen].</ta>
            <ta e="T590" id="Seg_11654" s="T586">Ich trug es mit einem Pferd nach Hause.</ta>
            <ta e="T594" id="Seg_11655" s="T590">Ich fütterte meine Kühen.</ta>
            <ta e="T597" id="Seg_11656" s="T594">Ich hatte ein Pferd.</ta>
            <ta e="T607" id="Seg_11657" s="T597">Dann nahm ich den (Sohn?) [meiner] Nichte und wir gingen (?), wir legten das Heu [auf Haufen].</ta>
            <ta e="T611" id="Seg_11658" s="T607">Dann gingen und gingen wir.</ta>
            <ta e="T614" id="Seg_11659" s="T611">Das Heu fiel runter. [?]</ta>
            <ta e="T619" id="Seg_11660" s="T614">Dann [versuchten] wir, [es] hochzuheben, schafften es [aber] nicht.</ta>
            <ta e="T626" id="Seg_11661" s="T619">[Er] sagt: "Jetzt werde ich das Pferd. anspannen</ta>
            <ta e="T628" id="Seg_11662" s="T626">Dann werde ich [es] hochheben."</ta>
            <ta e="T632" id="Seg_11663" s="T628">"Aber wie willst du es hochheben?"</ta>
            <ta e="T642" id="Seg_11664" s="T632">(…)</ta>
            <ta e="T646" id="Seg_11665" s="T642">Dann kamen wir wahrscheinlich. [?]</ta>
            <ta e="T648" id="Seg_11666" s="T646">Wir (gaben?) das Heu ab.</ta>
            <ta e="T651" id="Seg_11667" s="T648">Dann (trockneten?) wir das Pferd.</ta>
            <ta e="T654" id="Seg_11668" s="T651">Ich habe sehr gefroren.</ta>
            <ta e="T657" id="Seg_11669" s="T654">Dann hatte ich Husten.</ta>
            <ta e="T663" id="Seg_11670" s="T657">Dann brannte ich Zucker in den Wodka.</ta>
            <ta e="T669" id="Seg_11671" s="T663">Dann trank ich [es] und mein Husten verschwand irgendwie.</ta>
            <ta e="T672" id="Seg_11672" s="T669">Der Husten verschwand.</ta>
            <ta e="T678" id="Seg_11673" s="T672">Es gibt Gras, Nesseln.</ta>
            <ta e="T684" id="Seg_11674" s="T678">Man soll es nehmen, am Stiel pflücken.</ta>
            <ta e="T691" id="Seg_11675" s="T684">(…) dann verschwindet der Husten.</ta>
            <ta e="T698" id="Seg_11676" s="T691">[Wenn] es brennt, gibt es keinen Rauch, keine Wärme. [?]</ta>
            <ta e="T703" id="Seg_11677" s="T698">Ich muss heute kein Holz (hacken?).</ta>
            <ta e="T707" id="Seg_11678" s="T703">Es reicht für heute Nacht.</ta>
            <ta e="T710" id="Seg_11679" s="T707">Ich habe viel Holz.</ta>
            <ta e="T714" id="Seg_11680" s="T710">Es reicht für dieses Jahr.</ta>
            <ta e="T716" id="Seg_11681" s="T714">Es ist sehr kalt.</ta>
            <ta e="T719" id="Seg_11682" s="T716">Es wird viel Holz verbraucht.</ta>
            <ta e="T724" id="Seg_11683" s="T719">Das Holz wurde schwarz wegen des Regen.</ta>
            <ta e="T727" id="Seg_11684" s="T724">[Vorher] war es weiß.</ta>
            <ta e="T731" id="Seg_11685" s="T727">Ich(?) hackte (?) Lärchenholz.</ta>
            <ta e="T738" id="Seg_11686" s="T731">Weißes Holz [= Birke?] (?) ist sehr gut.</ta>
            <ta e="T744" id="Seg_11687" s="T738">Die brennen gut und (?) Wärme.</ta>
            <ta e="T752" id="Seg_11688" s="T744">Wenn man etwas kocht, gerät die Kohle nicht hinein.</ta>
            <ta e="T757" id="Seg_11689" s="T752">(…) dort ist sehr viel Kohle, man wirft es (weg?).</ta>
            <ta e="T761" id="Seg_11690" s="T757">(?) mich Vas'ka Schajbin.</ta>
            <ta e="T764" id="Seg_11691" s="T761">(?) (du?) auch.</ta>
            <ta e="T771" id="Seg_11692" s="T764">Und (?), und ich habe es selbst hingetan.</ta>
            <ta e="T773" id="Seg_11693" s="T771">Sie brachten [es] mit einem Traktor.</ta>
            <ta e="T776" id="Seg_11694" s="T773">Ich (kaufte?) das Öl für den Traktor.</ta>
            <ta e="T785" id="Seg_11695" s="T776">(…), und ich (?) das Holz, ging Wasser holen.</ta>
            <ta e="T788" id="Seg_11696" s="T785">Ich gab der Kuh (Heu?).</ta>
            <ta e="T792" id="Seg_11697" s="T788">Ich molk [sie], dann setzte ich mich um zu essen.</ta>
            <ta e="T794" id="Seg_11698" s="T792">Und ich denke:</ta>
            <ta e="T799" id="Seg_11699" s="T794">Diese Elja log mich an.</ta>
            <ta e="T804" id="Seg_11700" s="T799">Sie hatte drei Rubel.</ta>
            <ta e="T812" id="Seg_11701" s="T804">Und ich hatte einen Rubel fünfundzwanzig [Kopeken].</ta>
            <ta e="T822" id="Seg_11702" s="T812">Sie sollte (?) einen Rubel, fünfzehn Kopeken von mir nehmen.</ta>
            <ta e="T830" id="Seg_11703" s="T822">Heute ging ich zum Fluss und er war ganz zugefroren. [?]</ta>
            <ta e="T839" id="Seg_11704" s="T830">Ich ging zurück, nahm die Axt und hackte und hackte mit der Axt den Schnee.</ta>
            <ta e="T846" id="Seg_11705" s="T839">Er war einen Werschok tief gefroren (?), der Morgen war sehr kalt.</ta>
            <ta e="T858" id="Seg_11706" s="T846">Ich kam nach Hause, ich wartete auf euch, ich brachte Wasser, kletterte auf dem Ofen.</ta>
            <ta e="T861" id="Seg_11707" s="T858">Ich setzte mich und (?).</ta>
            <ta e="T865" id="Seg_11708" s="T861">Dann kam und (?).</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T4" id="Seg_11709" s="T0">[GVY:] Sverdlovskagəʔ… ʔin</ta>
            <ta e="T10" id="Seg_11710" s="T4">[GVY:] Or süjön? Amnobibaʔ may be interpreted both as 'sat in' [='got on'] and as 'were sitting in'.</ta>
            <ta e="T17" id="Seg_11711" s="T12">[GVY:] Aktʼigən "during the flight (the weather changed)"? Nʼergösittə?</ta>
            <ta e="T19" id="Seg_11712" s="T17">[GVY:] 'smoke' rather means 'mist', see the following sentence.</ta>
            <ta e="T27" id="Seg_11713" s="T25">[GVY:] here amnobibaʔ is pronounced as [amna-], unlike [amno-] in other cases.</ta>
            <ta e="T32" id="Seg_11714" s="T29">[GVY:] NB amnóːbiam vs. amnobiám in the next sentence.</ta>
            <ta e="T46" id="Seg_11715" s="T44">[GVY:] Săjanskənə? Most probably, here is meant not Sajansk, but Sajanskij, a smaller village near Abalakovo, which is mentioned once more below.</ta>
            <ta e="T67" id="Seg_11716" s="T58">[GVY:] An unexpected switch to the future tense.</ta>
            <ta e="T79" id="Seg_11717" s="T77">[GVY:] фатера - Russ. dial. 'lodging' (from квартира). The INTSR form is unclear. </ta>
            <ta e="T87" id="Seg_11718" s="T79">[GVY:] Presumably, Zaozʼorka (Заозёрка, or maybe Заозёрки) is the colloquial local name for Zaozerny. There is a village named Zaozerka, too, but this is farther away.</ta>
            <ta e="T98" id="Seg_11719" s="T93">[GVY:] numan (üzəbi-) üzəsʼtə</ta>
            <ta e="T101" id="Seg_11720" s="T98">[GVY:] numan (üzə-) üzəleʔbibeʔ</ta>
            <ta e="T112" id="Seg_11721" s="T107">[GVY:] an epenthetic b: [dʼăbaktərblaʔpibeʔ].</ta>
            <ta e="T115" id="Seg_11722" s="T112">[GVY:] or bozʼa? A name of a woman?</ta>
            <ta e="T121" id="Seg_11723" s="T115">[GVY:] Unclear. May be the sentences should be divided in another way: Dĭgəttə (kozʼa) kambi avtobus măndərzittə. Šobi da (măndə: dĭ) kalladʼürbi '((unknown)) went to look for a bus. She came [back] and said: it has left.' If here 'left' would mean 'arrived', this interpretation would be plausible.</ta>
            <ta e="T133" id="Seg_11724" s="T130">[AAV] amnol- + extra TR?</ta>
            <ta e="T145" id="Seg_11725" s="T139">[GVY:] should be nulaʔbibaʔ 'we were standing', cf. the next sentence?</ta>
            <ta e="T207" id="Seg_11726" s="T204">[GVY:] mĭbileʔ = mĭgeʔ?</ta>
            <ta e="T221" id="Seg_11727" s="T207">[GVY:] edəbi 'hung' - calque of the Russian colloquial вешать 'weigh' or 'hang'.</ta>
            <ta e="T239" id="Seg_11728" s="T237">[GVY:] obbere 'together' = oʔb + bere from Turkic (cf. e.g. Khak. pIrge)?</ta>
            <ta e="T273" id="Seg_11729" s="T270">[GVY:] Unclear.</ta>
            <ta e="T289" id="Seg_11730" s="T285">[GVY:] Malʼinovkinə = Malʼinovkanə. There is no Malinovka there, but there is a village Kalinovka.</ta>
            <ta e="T307" id="Seg_11731" s="T300">[GVY:] from the stem nu-d- 'speak Kamassian'?</ta>
            <ta e="T334" id="Seg_11732" s="T331">[GVY:] Various interpretations are possible, e.g. "10 small-ACC put-PAST-3PL" 'they put (= brought) 10 small [fishes]'.</ta>
            <ta e="T344" id="Seg_11733" s="T341">[GVY:] sobiʔi. Initial s- instead of š- sees to be a frequent variant in PKZ's speech.</ta>
            <ta e="T360" id="Seg_11734" s="T358">[GVY:] wet(ted) - cf. dʼuʔpi 'wet' - that is, rubbed with an ointment?</ta>
            <ta e="T392" id="Seg_11735" s="T389">[GVY:] An unusual use of 'pravda' in plural.</ta>
            <ta e="T491" id="Seg_11736" s="T484">[GVY:] nugar - probably accidentally.</ta>
            <ta e="T642" id="Seg_11737" s="T632">[GVY:] This whole fragment is unintelligible.</ta>
            <ta e="T663" id="Seg_11738" s="T657">[GVY:] mixed the caramel (burnt sugar) with the vodka.</ta>
            <ta e="T669" id="Seg_11739" s="T663">[GVY:] FUT instead of PST?</ta>
            <ta e="T731" id="Seg_11740" s="T727">[GVY:] bĭttona - stative participle from bɯt-, bot- 'cut' (D 11a, 12a)?</ta>
            <ta e="T757" id="Seg_11741" s="T752">[AAV] ipadək: might be "i" 'and'? otherwise why intervocalic p?</ta>
            <ta e="T785" id="Seg_11742" s="T776">[AAV] From here on, the speed is irregular.</ta>
            <ta e="T788" id="Seg_11743" s="T785">[GVY:] nuʔ = noʔ?</ta>
            <ta e="T830" id="Seg_11744" s="T822">[GVY:] koʔnambi?</ta>
            <ta e="T846" id="Seg_11745" s="T839">[GVY:] konnambi?</ta>
            <ta e="T858" id="Seg_11746" s="T846">[GVY:] pʼeškə? šiʔnʼiŋɨʔ?</ta>
            <ta e="T861" id="Seg_11747" s="T858">[GVY:] nemneʔbəm 'I'm sleeping'?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T868" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T869" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T867" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T866" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
