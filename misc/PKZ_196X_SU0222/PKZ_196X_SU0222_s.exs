<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID9857CF31-3463-1AA2-3206-63AA1412873A">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_SU0222.wav" />
         <referenced-file url="PKZ_196X_SU0222.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_196X_SU0222\PKZ_196X_SU0222.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1480</ud-information>
            <ud-information attribute-name="# HIAT:w">861</ud-information>
            <ud-information attribute-name="# e">884</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">28</ud-information>
            <ud-information attribute-name="# HIAT:u">188</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="0.797" type="appl" />
         <tli id="T889" time="1.195" type="intp" />
         <tli id="T2" time="1.593" type="appl" />
         <tli id="T3" time="2.39" type="appl" />
         <tli id="T4" time="3.411" type="appl" />
         <tli id="T5" time="4.382" type="appl" />
         <tli id="T6" time="5.354" type="appl" />
         <tli id="T7" time="6.325" type="appl" />
         <tli id="T8" time="7.296" type="appl" />
         <tli id="T9" time="8.268" type="appl" />
         <tli id="T10" time="9.239" type="appl" />
         <tli id="T11" time="10.21" type="appl" />
         <tli id="T12" time="11.678" type="appl" />
         <tli id="T13" time="13.015" type="appl" />
         <tli id="T14" time="14.352" type="appl" />
         <tli id="T15" time="15.69" type="appl" />
         <tli id="T16" time="16.773" type="appl" />
         <tli id="T17" time="17.615" type="appl" />
         <tli id="T18" time="18.458" type="appl" />
         <tli id="T19" time="19.301" type="appl" />
         <tli id="T20" time="20.143" type="appl" />
         <tli id="T21" time="21.533180822397732" />
         <tli id="T22" time="22.203" type="appl" />
         <tli id="T23" time="22.863" type="appl" />
         <tli id="T24" time="23.523" type="appl" />
         <tli id="T25" time="24.183" type="appl" />
         <tli id="T26" time="26.199814437158846" />
         <tli id="T27" time="27.053" type="appl" />
         <tli id="T28" time="27.718" type="appl" />
         <tli id="T29" time="28.383" type="appl" />
         <tli id="T30" time="29.048" type="appl" />
         <tli id="T31" time="29.713" type="appl" />
         <tli id="T32" time="30.378" type="appl" />
         <tli id="T33" time="31.926440544415705" />
         <tli id="T34" time="32.837" type="appl" />
         <tli id="T35" time="33.644" type="appl" />
         <tli id="T36" time="34.452" type="appl" />
         <tli id="T37" time="35.259" type="appl" />
         <tli id="T38" time="36.066" type="appl" />
         <tli id="T39" time="36.874" type="appl" />
         <tli id="T40" time="37.681" type="appl" />
         <tli id="T41" time="38.488" type="appl" />
         <tli id="T42" time="39.222" type="appl" />
         <tli id="T43" time="39.849" type="appl" />
         <tli id="T44" time="40.476" type="appl" />
         <tli id="T45" time="41.104" type="appl" />
         <tli id="T46" time="42.12" type="appl" />
         <tli id="T47" time="43.01" type="appl" />
         <tli id="T48" time="43.9" type="appl" />
         <tli id="T49" time="44.79" type="appl" />
         <tli id="T50" time="45.68" type="appl" />
         <tli id="T51" time="46.57" type="appl" />
         <tli id="T52" time="47.46" type="appl" />
         <tli id="T53" time="48.35" type="appl" />
         <tli id="T54" time="50.89297287869482" />
         <tli id="T55" time="51.817" type="appl" />
         <tli id="T56" time="52.779" type="appl" />
         <tli id="T892" time="53.223" type="intp" />
         <tli id="T57" time="53.741" type="appl" />
         <tli id="T58" time="54.703" type="appl" />
         <tli id="T59" time="55.665" type="appl" />
         <tli id="T60" time="56.627" type="appl" />
         <tli id="T61" time="57.589" type="appl" />
         <tli id="T62" time="58.551" type="appl" />
         <tli id="T63" time="59.512" type="appl" />
         <tli id="T64" time="60.474" type="appl" />
         <tli id="T65" time="61.436" type="appl" />
         <tli id="T66" time="62.398" type="appl" />
         <tli id="T67" time="63.36" type="appl" />
         <tli id="T68" time="64.322" type="appl" />
         <tli id="T69" time="65.284" type="appl" />
         <tli id="T70" time="66.246" type="appl" />
         <tli id="T71" time="67.208" type="appl" />
         <tli id="T72" time="68.35284921735106" />
         <tli id="T73" time="70.09950351316165" />
         <tli id="T74" time="71.093" type="appl" />
         <tli id="T75" time="71.859" type="appl" />
         <tli id="T76" time="72.626" type="appl" />
         <tli id="T77" time="73.393" type="appl" />
         <tli id="T78" time="74.159" type="appl" />
         <tli id="T79" time="75.6194644174791" />
         <tli id="T80" time="76.816" type="appl" />
         <tli id="T81" time="77.672" type="appl" />
         <tli id="T82" time="78.529" type="appl" />
         <tli id="T83" time="80.57942928802518" />
         <tli id="T84" time="81.702" type="appl" />
         <tli id="T85" time="82.486" type="appl" />
         <tli id="T86" time="83.27" type="appl" />
         <tli id="T87" time="84.053" type="appl" />
         <tli id="T88" time="86.61938650941602" />
         <tli id="T89" time="89.106" type="appl" />
         <tli id="T90" time="90.076" type="appl" />
         <tli id="T91" time="90.819" type="appl" />
         <tli id="T92" time="91.562" type="appl" />
         <tli id="T93" time="92.304" type="appl" />
         <tli id="T94" time="93.047" type="appl" />
         <tli id="T95" time="93.79" type="appl" />
         <tli id="T96" time="94.533" type="appl" />
         <tli id="T97" time="95.503" type="appl" />
         <tli id="T98" time="96.426" type="appl" />
         <tli id="T99" time="97.35" type="appl" />
         <tli id="T100" time="98.273" type="appl" />
         <tli id="T101" time="100.28595638121642" />
         <tli id="T102" time="101.103" type="appl" />
         <tli id="T103" time="101.614" type="appl" />
         <tli id="T104" time="102.126" type="appl" />
         <tli id="T105" time="102.637" type="appl" />
         <tli id="T106" time="103.148" type="appl" />
         <tli id="T107" time="103.659" type="appl" />
         <tli id="T108" time="105.065" type="appl" />
         <tli id="T109" time="106.412" type="appl" />
         <tli id="T110" time="107.758" type="appl" />
         <tli id="T111" time="109.105" type="appl" />
         <tli id="T112" time="109.991" type="appl" />
         <tli id="T113" time="110.764" type="appl" />
         <tli id="T114" time="111.538" type="appl" />
         <tli id="T115" time="112.311" type="appl" />
         <tli id="T116" time="115.93917884901515" />
         <tli id="T117" time="117.074" type="appl" />
         <tli id="T118" time="118.464" type="appl" />
         <tli id="T119" time="119.854" type="appl" />
         <tli id="T120" time="121.244" type="appl" />
         <tli id="T121" time="122.61246491812356" />
         <tli id="T122" time="123.77" type="appl" />
         <tli id="T123" time="125.68577648441621" />
         <tli id="T124" time="126.45" type="appl" />
         <tli id="T125" time="126.954" type="appl" />
         <tli id="T126" time="127.458" type="appl" />
         <tli id="T127" time="127.961" type="appl" />
         <tli id="T128" time="128.465" type="appl" />
         <tli id="T129" time="128.969" type="appl" />
         <tli id="T130" time="129.473" type="appl" />
         <tli id="T131" time="130.029" type="appl" />
         <tli id="T132" time="130.584" type="appl" />
         <tli id="T133" time="131.14" type="appl" />
         <tli id="T134" time="131.904" type="appl" />
         <tli id="T135" time="132.615" type="appl" />
         <tli id="T136" time="133.326" type="appl" />
         <tli id="T137" time="133.992" type="appl" />
         <tli id="T138" time="134.646" type="appl" />
         <tli id="T139" time="135.299" type="appl" />
         <tli id="T140" time="136.396" type="appl" />
         <tli id="T141" time="137.439" type="appl" />
         <tli id="T142" time="138.483" type="appl" />
         <tli id="T143" time="139.526" type="appl" />
         <tli id="T144" time="141.19899994377212" />
         <tli id="T145" time="142.71232255884465" />
         <tli id="T146" time="143.992" type="appl" />
         <tli id="T147" time="144.939" type="appl" />
         <tli id="T148" time="145.885" type="appl" />
         <tli id="T149" time="146.832" type="appl" />
         <tli id="T150" time="147.97228530462542" />
         <tli id="T151" time="149.059" type="appl" />
         <tli id="T152" time="149.919" type="appl" />
         <tli id="T153" time="150.779" type="appl" />
         <tli id="T154" time="151.925590638273" />
         <tli id="T155" time="152.778" type="appl" />
         <tli id="T156" time="153.497" type="appl" />
         <tli id="T157" time="154.216" type="appl" />
         <tli id="T158" time="154.935" type="appl" />
         <tli id="T159" time="155.654" type="appl" />
         <tli id="T160" time="156.374" type="appl" />
         <tli id="T161" time="157.093" type="appl" />
         <tli id="T162" time="157.812" type="appl" />
         <tli id="T163" time="158.531" type="appl" />
         <tli id="T164" time="159.25" type="appl" />
         <tli id="T165" time="159.969" type="appl" />
         <tli id="T166" time="161.196" type="appl" />
         <tli id="T167" time="162.422" type="appl" />
         <tli id="T168" time="164.33883605353762" />
         <tli id="T169" time="165.317" type="appl" />
         <tli id="T170" time="166.272" type="appl" />
         <tli id="T171" time="167.226" type="appl" />
         <tli id="T172" time="168.181" type="appl" />
         <tli id="T173" time="169.136" type="appl" />
         <tli id="T174" time="170.69879100851202" />
         <tli id="T175" time="171.416" type="appl" />
         <tli id="T176" time="172.114" type="appl" />
         <tli id="T177" time="172.811" type="appl" />
         <tli id="T178" time="173.508" type="appl" />
         <tli id="T179" time="174.206" type="appl" />
         <tli id="T180" time="174.903" type="appl" />
         <tli id="T181" time="175.6" type="appl" />
         <tli id="T182" time="176.298" type="appl" />
         <tli id="T183" time="176.995" type="appl" />
         <tli id="T184" time="178.40540309231756" />
         <tli id="T891" time="178.62823076923075" type="intp" />
         <tli id="T185" time="179.339" type="appl" />
         <tli id="T186" time="179.932" type="appl" />
         <tli id="T187" time="180.526" type="appl" />
         <tli id="T188" time="181.119" type="appl" />
         <tli id="T189" time="181.712" type="appl" />
         <tli id="T190" time="182.306" type="appl" />
         <tli id="T191" time="182.899" type="appl" />
         <tli id="T192" time="183.492" type="appl" />
         <tli id="T193" time="184.536" type="appl" />
         <tli id="T194" time="185.279" type="appl" />
         <tli id="T195" time="186.022" type="appl" />
         <tli id="T196" time="186.766" type="appl" />
         <tli id="T197" time="188.116" type="appl" />
         <tli id="T198" time="189.423" type="appl" />
         <tli id="T199" time="193.9719595072707" />
         <tli id="T200" time="194.877" type="appl" />
         <tli id="T201" time="195.615" type="appl" />
         <tli id="T202" time="196.352" type="appl" />
         <tli id="T203" time="197.09" type="appl" />
         <tli id="T204" time="197.828" type="appl" />
         <tli id="T205" time="198.566" type="appl" />
         <tli id="T206" time="199.303" type="appl" />
         <tli id="T207" time="200.041" type="appl" />
         <tli id="T208" time="200.779" type="appl" />
         <tli id="T209" time="202.01856918300876" />
         <tli id="T210" time="202.731" type="appl" />
         <tli id="T211" time="203.296" type="appl" />
         <tli id="T212" time="203.86" type="appl" />
         <tli id="T213" time="204.425" type="appl" />
         <tli id="T214" time="204.99" type="appl" />
         <tli id="T215" time="205.555" type="appl" />
         <tli id="T216" time="206.12" type="appl" />
         <tli id="T217" time="206.685" type="appl" />
         <tli id="T218" time="207.249" type="appl" />
         <tli id="T219" time="207.814" type="appl" />
         <tli id="T220" time="208.33900618870643" />
         <tli id="T221" time="209.053" type="appl" />
         <tli id="T222" time="209.719" type="appl" />
         <tli id="T223" time="210.70517432591413" />
         <tli id="T224" time="211.644" type="appl" />
         <tli id="T225" time="212.315" type="appl" />
         <tli id="T226" time="212.987" type="appl" />
         <tli id="T227" time="213.659" type="appl" />
         <tli id="T228" time="214.33" type="appl" />
         <tli id="T229" time="215.002" type="appl" />
         <tli id="T230" time="215.673" type="appl" />
         <tli id="T231" time="216.345" type="appl" />
         <tli id="T232" time="217.017" type="appl" />
         <tli id="T233" time="217.688" type="appl" />
         <tli id="T234" time="218.31333658616717" />
         <tli id="T235" time="218.664" type="appl" />
         <tli id="T236" time="218.968" type="appl" />
         <tli id="T237" time="219.271" type="appl" />
         <tli id="T238" time="219.575" type="appl" />
         <tli id="T239" time="219.879" type="appl" />
         <tli id="T240" time="224.712" type="appl" />
         <tli id="T241" time="225.29" type="appl" />
         <tli id="T242" time="225.808" type="appl" />
         <tli id="T243" time="226.327" type="appl" />
         <tli id="T244" time="226.845" type="appl" />
         <tli id="T245" time="227.363" type="appl" />
         <tli id="T246" time="227.882" type="appl" />
         <tli id="T247" time="228.4" type="appl" />
         <tli id="T248" time="228.918" type="appl" />
         <tli id="T249" time="229.748" type="appl" />
         <tli id="T250" time="230.484" type="appl" />
         <tli id="T251" time="231.22" type="appl" />
         <tli id="T252" time="231.956" type="appl" />
         <tli id="T253" time="232.77866382152095" />
         <tli id="T254" time="233.612" type="appl" />
         <tli id="T255" time="234.526" type="appl" />
         <tli id="T256" time="235.439" type="appl" />
         <tli id="T257" time="236.352" type="appl" />
         <tli id="T258" time="237.266" type="appl" />
         <tli id="T259" time="238.179" type="appl" />
         <tli id="T260" time="239.10497318146037" />
         <tli id="T261" time="239.737" type="appl" />
         <tli id="T262" time="240.169" type="appl" />
         <tli id="T263" time="240.601" type="appl" />
         <tli id="T264" time="241.033" type="appl" />
         <tli id="T265" time="241.465" type="appl" />
         <tli id="T266" time="241.897" type="appl" />
         <tli id="T267" time="242.329" type="appl" />
         <tli id="T268" time="242.761" type="appl" />
         <tli id="T269" time="243.193" type="appl" />
         <tli id="T270" time="243.625" type="appl" />
         <tli id="T271" time="244.618" type="appl" />
         <tli id="T272" time="245.392" type="appl" />
         <tli id="T273" time="246.165" type="appl" />
         <tli id="T274" time="248.21824196914386" />
         <tli id="T275" time="249.284" type="appl" />
         <tli id="T276" time="250.058" type="appl" />
         <tli id="T277" time="250.831" type="appl" />
         <tli id="T278" time="252.101" type="appl" />
         <tli id="T279" time="253.216" type="appl" />
         <tli id="T280" time="254.331" type="appl" />
         <tli id="T281" time="255.446" type="appl" />
         <tli id="T282" time="256.561" type="appl" />
         <tli id="T283" time="257.676" type="appl" />
         <tli id="T284" time="258.791" type="appl" />
         <tli id="T285" time="259.906" type="appl" />
         <tli id="T286" time="262.83813842224544" />
         <tli id="T287" time="263.846" type="appl" />
         <tli id="T288" time="264.44" type="appl" />
         <tli id="T289" time="265.033" type="appl" />
         <tli id="T290" time="265.626" type="appl" />
         <tli id="T291" time="266.22" type="appl" />
         <tli id="T292" time="267.7781034344426" />
         <tli id="T293" time="268.88" type="appl" />
         <tli id="T294" time="269.48" type="appl" />
         <tli id="T295" time="270.08" type="appl" />
         <tli id="T296" time="270.68" type="appl" />
         <tli id="T297" time="271.28" type="appl" />
         <tli id="T298" time="272.075" type="appl" />
         <tli id="T299" time="272.563" type="appl" />
         <tli id="T300" time="273.051" type="appl" />
         <tli id="T301" time="273.539" type="appl" />
         <tli id="T302" time="274.027" type="appl" />
         <tli id="T303" time="274.515" type="appl" />
         <tli id="T304" time="275.003" type="appl" />
         <tli id="T305" time="275.491" type="appl" />
         <tli id="T306" time="275.979" type="appl" />
         <tli id="T307" time="276.467" type="appl" />
         <tli id="T308" time="277.338" type="appl" />
         <tli id="T309" time="277.995" type="appl" />
         <tli id="T310" time="279.4846871880434" />
         <tli id="T311" time="280.184" type="appl" />
         <tli id="T312" time="280.677" type="appl" />
         <tli id="T313" time="281.169" type="appl" />
         <tli id="T314" time="281.661" type="appl" />
         <tli id="T315" time="282.154" type="appl" />
         <tli id="T316" time="282.646" type="appl" />
         <tli id="T317" time="283.356" type="appl" />
         <tli id="T318" time="284.056" type="appl" />
         <tli id="T319" time="284.756" type="appl" />
         <tli id="T320" time="285.456" type="appl" />
         <tli id="T321" time="286.156" type="appl" />
         <tli id="T322" time="286.856" type="appl" />
         <tli id="T323" time="287.556" type="appl" />
         <tli id="T324" time="288.4112906311364" />
         <tli id="T325" time="289.145" type="appl" />
         <tli id="T326" time="289.739" type="appl" />
         <tli id="T327" time="290.332" type="appl" />
         <tli id="T328" time="290.925" type="appl" />
         <tli id="T329" time="292.4912617343275" />
         <tli id="T330" time="293.439" type="appl" />
         <tli id="T331" time="294.314" type="appl" />
         <tli id="T332" time="295.188" type="appl" />
         <tli id="T333" time="296.063" type="appl" />
         <tli id="T334" time="296.938" type="appl" />
         <tli id="T335" time="298.74" type="appl" />
         <tli id="T336" time="300.333" type="appl" />
         <tli id="T337" time="301.926" type="appl" />
         <tli id="T338" time="303.7178488875529" />
         <tli id="T339" time="305.018" type="appl" />
         <tli id="T340" time="306.333" type="appl" />
         <tli id="T341" time="307.647" type="appl" />
         <tli id="T342" time="308.961" type="appl" />
         <tli id="T343" time="310.275" type="appl" />
         <tli id="T344" time="311.59" type="appl" />
         <tli id="T345" time="312.904" type="appl" />
         <tli id="T346" time="313.574" type="appl" />
         <tli id="T347" time="314.229" type="appl" />
         <tli id="T348" time="314.884" type="appl" />
         <tli id="T349" time="315.539" type="appl" />
         <tli id="T350" time="316.194" type="appl" />
         <tli id="T351" time="316.85" type="appl" />
         <tli id="T352" time="317.505" type="appl" />
         <tli id="T353" time="318.16" type="appl" />
         <tli id="T354" time="318.815" type="appl" />
         <tli id="T355" time="319.47" type="appl" />
         <tli id="T356" time="320.80439453702826" />
         <tli id="T357" time="321.83" type="appl" />
         <tli id="T358" time="322.6" type="appl" />
         <tli id="T359" time="323.37" type="appl" />
         <tli id="T360" time="324.14" type="appl" />
         <tli id="T361" time="324.91" type="appl" />
         <tli id="T362" time="325.68" type="appl" />
         <tli id="T363" time="326.45" type="appl" />
         <tli id="T364" time="327.22" type="appl" />
         <tli id="T365" time="329.41100024653764" />
         <tli id="T366" time="330.426" type="appl" />
         <tli id="T367" time="331.252" type="appl" />
         <tli id="T368" time="332.079" type="appl" />
         <tli id="T369" time="332.905" type="appl" />
         <tli id="T370" time="333.732" type="appl" />
         <tli id="T371" time="335.304291840036" />
         <tli id="T372" time="336.27" type="appl" />
         <tli id="T373" time="336.893" type="appl" />
         <tli id="T374" time="337.516" type="appl" />
         <tli id="T375" time="338.14" type="appl" />
         <tli id="T376" time="339.047" type="appl" />
         <tli id="T377" time="339.673" type="appl" />
         <tli id="T378" time="340.299" type="appl" />
         <tli id="T379" time="340.926" type="appl" />
         <tli id="T380" time="341.93757819244644" />
         <tli id="T381" time="342.73" type="appl" />
         <tli id="T382" time="343.286" type="appl" />
         <tli id="T383" time="343.843" type="appl" />
         <tli id="T384" time="344.4" type="appl" />
         <tli id="T385" time="345.12" type="appl" />
         <tli id="T386" time="345.746" type="appl" />
         <tli id="T387" time="346.373" type="appl" />
         <tli id="T388" time="347.70420401640126" />
         <tli id="T389" time="348.53" type="appl" />
         <tli id="T390" time="349.3" type="appl" />
         <tli id="T391" time="350.071" type="appl" />
         <tli id="T392" time="350.841" type="appl" />
         <tli id="T393" time="351.518" type="appl" />
         <tli id="T394" time="352.08" type="appl" />
         <tli id="T395" time="352.641" type="appl" />
         <tli id="T396" time="353.202" type="appl" />
         <tli id="T397" time="353.764" type="appl" />
         <tli id="T398" time="354.325" type="appl" />
         <tli id="T399" time="354.886" type="appl" />
         <tli id="T400" time="355.448" type="appl" />
         <tli id="T401" time="356.344142823159" />
         <tli id="T402" time="356.933" type="appl" />
         <tli id="T403" time="357.857" type="appl" />
         <tli id="T404" time="358.9374577890762" />
         <tli id="T405" time="359.922" type="appl" />
         <tli id="T406" time="360.916" type="appl" />
         <tli id="T407" time="361.91" type="appl" />
         <tli id="T408" time="362.905" type="appl" />
         <tli id="T409" time="363.9" type="appl" />
         <tli id="T410" time="364.894" type="appl" />
         <tli id="T411" time="365.618" type="appl" />
         <tli id="T412" time="366.213" type="appl" />
         <tli id="T413" time="366.809" type="appl" />
         <tli id="T414" time="367.405" type="appl" />
         <tli id="T415" time="368.001" type="appl" />
         <tli id="T416" time="368.596" type="appl" />
         <tli id="T417" time="369.192" type="appl" />
         <tli id="T418" time="369.89" type="appl" />
         <tli id="T419" time="370.471" type="appl" />
         <tli id="T420" time="371.052" type="appl" />
         <tli id="T421" time="371.633" type="appl" />
         <tli id="T422" time="372.215" type="appl" />
         <tli id="T423" time="372.796" type="appl" />
         <tli id="T424" time="373.377" type="appl" />
         <tli id="T425" time="373.958" type="appl" />
         <tli id="T426" time="374.70401278751916" />
         <tli id="T427" time="375.106" type="appl" />
         <tli id="T428" time="375.555" type="appl" />
         <tli id="T429" time="376.004" type="appl" />
         <tli id="T430" time="376.453" type="appl" />
         <tli id="T431" time="376.902" type="appl" />
         <tli id="T432" time="377.351" type="appl" />
         <tli id="T433" time="377.8" type="appl" />
         <tli id="T434" time="378.35732024593216" />
         <tli id="T435" time="379.435" type="appl" />
         <tli id="T436" time="380.597" type="appl" />
         <tli id="T437" time="381.76" type="appl" />
         <tli id="T438" time="382.847" type="appl" />
         <tli id="T439" time="383.837" type="appl" />
         <tli id="T440" time="384.827" type="appl" />
         <tli id="T441" time="385.817" type="appl" />
         <tli id="T442" time="386.807" type="appl" />
         <tli id="T443" time="387.797" type="appl" />
         <tli id="T444" time="388.786" type="appl" />
         <tli id="T445" time="389.776" type="appl" />
         <tli id="T446" time="390.766" type="appl" />
         <tli id="T447" time="391.756" type="appl" />
         <tli id="T448" time="392.746" type="appl" />
         <tli id="T449" time="395.1505346396225" />
         <tli id="T450" time="397.217" type="appl" />
         <tli id="T451" time="398.958" type="appl" />
         <tli id="T452" time="400.699" type="appl" />
         <tli id="T453" time="401.91" type="appl" />
         <tli id="T454" time="403.121" type="appl" />
         <tli id="T455" time="403.879" type="appl" />
         <tli id="T456" time="404.518" type="appl" />
         <tli id="T457" time="405.156" type="appl" />
         <tli id="T458" time="405.795" type="appl" />
         <tli id="T459" time="406.3673301955311" />
         <tli id="T460" time="407.063" type="appl" />
         <tli id="T461" time="407.692" type="appl" />
         <tli id="T462" time="408.322" type="appl" />
         <tli id="T463" time="408.951" type="appl" />
         <tli id="T464" time="409.58" type="appl" />
         <tli id="T465" time="410.209" type="appl" />
         <tli id="T466" time="411.16" type="appl" />
         <tli id="T467" time="412.001" type="appl" />
         <tli id="T468" time="412.843" type="appl" />
         <tli id="T469" time="413.684" type="appl" />
         <tli id="T470" time="414.525" type="appl" />
         <tli id="T471" time="415.366" type="appl" />
         <tli id="T472" time="416.207" type="appl" />
         <tli id="T473" time="417.048" type="appl" />
         <tli id="T474" time="417.89" type="appl" />
         <tli id="T475" time="418.731" type="appl" />
         <tli id="T476" time="419.572" type="appl" />
         <tli id="T477" time="420.207" type="appl" />
         <tli id="T478" time="420.713" type="appl" />
         <tli id="T479" time="421.219" type="appl" />
         <tli id="T480" time="421.73834112045506" />
         <tli id="T481" time="422.408" type="appl" />
         <tli id="T482" time="423.09" type="appl" />
         <tli id="T483" time="423.773" type="appl" />
         <tli id="T484" time="424.456" type="appl" />
         <tli id="T485" time="426.2103146555825" />
         <tli id="T486" time="426.977" type="appl" />
         <tli id="T487" time="427.634" type="appl" />
         <tli id="T488" time="428.29" type="appl" />
         <tli id="T489" time="428.947" type="appl" />
         <tli id="T490" time="429.604" type="appl" />
         <tli id="T491" time="430.661" type="appl" />
         <tli id="T492" time="431.607" type="appl" />
         <tli id="T493" time="432.553" type="appl" />
         <tli id="T494" time="433.294" type="appl" />
         <tli id="T495" time="434.035" type="appl" />
         <tli id="T496" time="434.8835865595886" />
         <tli id="T497" time="435.618" type="appl" />
         <tli id="T498" time="436.352" type="appl" />
         <tli id="T499" time="437.085" type="appl" />
         <tli id="T500" time="437.819" type="appl" />
         <tli id="T501" time="441.08354264777125" />
         <tli id="T502" time="441.74" type="appl" />
         <tli id="T503" time="442.25" type="appl" />
         <tli id="T504" time="442.76" type="appl" />
         <tli id="T505" time="443.269" type="appl" />
         <tli id="T506" time="443.779" type="appl" />
         <tli id="T507" time="444.289" type="appl" />
         <tli id="T508" time="444.986" type="appl" />
         <tli id="T509" time="445.683" type="appl" />
         <tli id="T510" time="446.38" type="appl" />
         <tli id="T511" time="447.077" type="appl" />
         <tli id="T512" time="447.864" type="appl" />
         <tli id="T513" time="448.503" type="appl" />
         <tli id="T514" time="449.143" type="appl" />
         <tli id="T515" time="449.783" type="appl" />
         <tli id="T516" time="450.423" type="appl" />
         <tli id="T517" time="451.062" type="appl" />
         <tli id="T518" time="451.702" type="appl" />
         <tli id="T519" time="452.456" type="appl" />
         <tli id="T520" time="453.083" type="appl" />
         <tli id="T521" time="453.711" type="appl" />
         <tli id="T522" time="454.338" type="appl" />
         <tli id="T523" time="454.966" type="appl" />
         <tli id="T524" time="455.594" type="appl" />
         <tli id="T525" time="456.221" type="appl" />
         <tli id="T526" time="456.849" type="appl" />
         <tli id="T527" time="457.606" type="appl" />
         <tli id="T528" time="458.253" type="appl" />
         <tli id="T529" time="458.899" type="appl" />
         <tli id="T530" time="459.546" type="appl" />
         <tli id="T531" time="460.24340694609043" />
         <tli id="T532" time="461.121" type="appl" />
         <tli id="T533" time="462.05" type="appl" />
         <tli id="T534" time="462.98" type="appl" />
         <tli id="T535" time="463.909" type="appl" />
         <tli id="T536" time="464.838" type="appl" />
         <tli id="T537" time="465.768" type="appl" />
         <tli id="T538" time="466.697" type="appl" />
         <tli id="T539" time="468.3566828163252" />
         <tli id="T540" time="468.669" type="appl" />
         <tli id="T541" time="469.577" type="appl" />
         <tli id="T542" time="470.484" type="appl" />
         <tli id="T543" time="471.391" type="appl" />
         <tli id="T544" time="472.29233202502405" />
         <tli id="T545" time="473.206" type="appl" />
         <tli id="T546" time="473.995" type="appl" />
         <tli id="T547" time="474.677" type="appl" />
         <tli id="T548" time="475.36" type="appl" />
         <tli id="T549" time="477.97661468218274" />
         <tli id="T550" time="478.817" type="appl" />
         <tli id="T551" time="479.513" type="appl" />
         <tli id="T552" time="480.208" type="appl" />
         <tli id="T553" time="481.726" type="appl" />
         <tli id="T554" time="483.126" type="appl" />
         <tli id="T555" time="484.023" type="appl" />
         <tli id="T556" time="484.756" type="appl" />
         <tli id="T557" time="485.489" type="appl" />
         <tli id="T558" time="486.222" type="appl" />
         <tli id="T559" time="486.955" type="appl" />
         <tli id="T560" time="487.688" type="appl" />
         <tli id="T561" time="488.42" type="appl" />
         <tli id="T562" time="489.153" type="appl" />
         <tli id="T563" time="489.886" type="appl" />
         <tli id="T564" time="490.619" type="appl" />
         <tli id="T565" time="491.352" type="appl" />
         <tli id="T566" time="492.085" type="appl" />
         <tli id="T567" time="493.0831743551094" />
         <tli id="T568" time="494.016" type="appl" />
         <tli id="T569" time="494.663" type="appl" />
         <tli id="T570" time="495.31" type="appl" />
         <tli id="T571" time="495.956" type="appl" />
         <tli id="T572" time="496.603" type="appl" />
         <tli id="T573" time="497.25" type="appl" />
         <tli id="T574" time="497.897" type="appl" />
         <tli id="T575" time="498.544" type="appl" />
         <tli id="T576" time="499.191" type="appl" />
         <tli id="T577" time="499.837" type="appl" />
         <tli id="T578" time="500.484" type="appl" />
         <tli id="T579" time="501.131" type="appl" />
         <tli id="T580" time="501.778" type="appl" />
         <tli id="T581" time="504.26976179163677" />
         <tli id="T582" time="505.088" type="appl" />
         <tli id="T583" time="505.799" type="appl" />
         <tli id="T584" time="507.2830737828825" />
         <tli id="T585" time="508.442" type="appl" />
         <tli id="T586" time="509.388" type="appl" />
         <tli id="T587" time="510.804" type="appl" />
         <tli id="T588" time="511.722" type="appl" />
         <tli id="T589" time="512.361" type="appl" />
         <tli id="T590" time="513.0" type="appl" />
         <tli id="T591" time="513.638" type="appl" />
         <tli id="T592" time="514.277" type="appl" />
         <tli id="T593" time="514.916" type="appl" />
         <tli id="T594" time="515.555" type="appl" />
         <tli id="T595" time="516.194" type="appl" />
         <tli id="T596" time="516.873" type="appl" />
         <tli id="T597" time="517.552" type="appl" />
         <tli id="T598" time="519.3763214645635" />
         <tli id="T599" time="520.46" type="appl" />
         <tli id="T600" time="521.242" type="appl" />
         <tli id="T601" time="522.024" type="appl" />
         <tli id="T602" time="523.8629563541838" />
         <tli id="T603" time="524.735" type="appl" />
         <tli id="T604" time="525.266" type="appl" />
         <tli id="T605" time="525.798" type="appl" />
         <tli id="T606" time="526.329" type="appl" />
         <tli id="T607" time="526.86" type="appl" />
         <tli id="T608" time="527.391" type="appl" />
         <tli id="T609" time="527.922" type="appl" />
         <tli id="T610" time="528.453" type="appl" />
         <tli id="T611" time="528.985" type="appl" />
         <tli id="T612" time="529.516" type="appl" />
         <tli id="T613" time="530.047" type="appl" />
         <tli id="T614" time="530.762" type="appl" />
         <tli id="T615" time="531.478" type="appl" />
         <tli id="T616" time="532.193" type="appl" />
         <tli id="T617" time="532.908" type="appl" />
         <tli id="T618" time="533.623" type="appl" />
         <tli id="T619" time="534.339" type="appl" />
         <tli id="T620" time="536.1762024777034" />
         <tli id="T621" time="537.774" type="appl" />
         <tli id="T622" time="539.305" type="appl" />
         <tli id="T623" time="540.837" type="appl" />
         <tli id="T624" time="542.368" type="appl" />
         <tli id="T625" time="545.5094697072257" />
         <tli id="T626" time="546.392" type="appl" />
         <tli id="T627" time="547.088" type="appl" />
         <tli id="T628" time="547.785" type="appl" />
         <tli id="T629" time="548.481" type="appl" />
         <tli id="T630" time="549.178" type="appl" />
         <tli id="T631" time="549.874" type="appl" />
         <tli id="T632" time="550.571" type="appl" />
         <tli id="T633" time="551.267" type="appl" />
         <tli id="T634" time="551.964" type="appl" />
         <tli id="T635" time="552.677" type="appl" />
         <tli id="T636" time="553.391" type="appl" />
         <tli id="T637" time="554.104" type="appl" />
         <tli id="T638" time="554.817" type="appl" />
         <tli id="T639" time="555.53" type="appl" />
         <tli id="T640" time="556.244" type="appl" />
         <tli id="T641" time="556.957" type="appl" />
         <tli id="T642" time="557.974" type="appl" />
         <tli id="T643" time="558.698" type="appl" />
         <tli id="T644" time="559.421" type="appl" />
         <tli id="T645" time="560.145" type="appl" />
         <tli id="T646" time="560.868" type="appl" />
         <tli id="T647" time="561.592" type="appl" />
         <tli id="T648" time="562.442" type="appl" />
         <tli id="T649" time="563.198" type="appl" />
         <tli id="T650" time="563.955" type="appl" />
         <tli id="T651" time="564.711" type="appl" />
         <tli id="T652" time="565.468" type="appl" />
         <tli id="T653" time="566.224" type="appl" />
         <tli id="T654" time="566.981" type="appl" />
         <tli id="T655" time="567.823" type="appl" />
         <tli id="T656" time="568.527" type="appl" />
         <tli id="T657" time="569.231" type="appl" />
         <tli id="T658" time="570.311" type="appl" />
         <tli id="T659" time="571.271" type="appl" />
         <tli id="T660" time="572.232" type="appl" />
         <tli id="T661" time="573.192" type="appl" />
         <tli id="T662" time="574.2359329158053" />
         <tli id="T663" time="575.164" type="appl" />
         <tli id="T664" time="576.057" type="appl" />
         <tli id="T665" time="576.95" type="appl" />
         <tli id="T666" time="577.843" type="appl" />
         <tli id="T667" time="579.4358960865391" />
         <tli id="T668" time="580.372" type="appl" />
         <tli id="T669" time="581.276" type="appl" />
         <tli id="T670" time="582.18" type="appl" />
         <tli id="T671" time="583.084" type="appl" />
         <tli id="T672" time="583.988" type="appl" />
         <tli id="T673" time="587.2958404176866" />
         <tli id="T674" time="588.022" type="appl" />
         <tli id="T675" time="588.702" type="appl" />
         <tli id="T676" time="589.381" type="appl" />
         <tli id="T677" time="590.061" type="appl" />
         <tli id="T678" time="590.74" type="appl" />
         <tli id="T679" time="591.42" type="appl" />
         <tli id="T680" time="592.155805996488" />
         <tli id="T681" time="592.918" type="appl" />
         <tli id="T682" time="593.555" type="appl" />
         <tli id="T683" time="594.191" type="appl" />
         <tli id="T684" time="594.827" type="appl" />
         <tli id="T685" time="595.464" type="appl" />
         <tli id="T686" time="596.0666637141084" />
         <tli id="T687" time="596.517" type="appl" />
         <tli id="T688" time="596.932" type="appl" />
         <tli id="T689" time="597.346" type="appl" />
         <tli id="T690" time="597.76" type="appl" />
         <tli id="T691" time="598.174" type="appl" />
         <tli id="T692" time="598.589" type="appl" />
         <tli id="T693" time="598.9563307477489" />
         <tli id="T694" time="599.912" type="appl" />
         <tli id="T695" time="600.814" type="appl" />
         <tli id="T696" time="601.5757392788557" />
         <tli id="T697" time="602.36" type="appl" />
         <tli id="T698" time="603.005" type="appl" />
         <tli id="T699" time="603.775723697243" />
         <tli id="T700" time="605.11" type="appl" />
         <tli id="T701" time="606.37" type="appl" />
         <tli id="T702" time="607.63" type="appl" />
         <tli id="T703" time="608.117" type="appl" />
         <tli id="T704" time="608.584" type="appl" />
         <tli id="T705" time="609.05" type="appl" />
         <tli id="T706" time="609.517" type="appl" />
         <tli id="T707" time="609.984" type="appl" />
         <tli id="T708" time="610.451" type="appl" />
         <tli id="T709" time="611.201" type="appl" />
         <tli id="T710" time="611.938" type="appl" />
         <tli id="T711" time="612.675" type="appl" />
         <tli id="T712" time="613.411" type="appl" />
         <tli id="T713" time="614.148" type="appl" />
         <tli id="T714" time="616.1689692541587" />
         <tli id="T715" time="617.056" type="appl" />
         <tli id="T716" time="617.654" type="appl" />
         <tli id="T717" time="618.251" type="appl" />
         <tli id="T718" time="618.848" type="appl" />
         <tli id="T719" time="619.446" type="appl" />
         <tli id="T720" time="620.7022704799266" />
         <tli id="T721" time="621.80892930857" />
         <tli id="T722" time="624.132" type="appl" />
         <tli id="T723" time="625.283" type="appl" />
         <tli id="T724" time="626.427" type="appl" />
         <tli id="T725" time="627.57" type="appl" />
         <tli id="T726" time="628.713" type="appl" />
         <tli id="T727" time="629.857" type="appl" />
         <tli id="T728" time="631.0" type="appl" />
         <tli id="T729" time="632.143" type="appl" />
         <tli id="T730" time="633.287" type="appl" />
         <tli id="T731" time="634.43" type="appl" />
         <tli id="T732" time="635.366" type="appl" />
         <tli id="T733" time="636.128" type="appl" />
         <tli id="T734" time="636.889" type="appl" />
         <tli id="T735" time="637.65" type="appl" />
         <tli id="T736" time="638.746" type="appl" />
         <tli id="T737" time="640.5354633426614" />
         <tli id="T738" time="641.817" type="appl" />
         <tli id="T739" time="642.938" type="appl" />
         <tli id="T740" time="644.06" type="appl" />
         <tli id="T741" time="645.011" type="appl" />
         <tli id="T742" time="645.9287584774353" />
         <tli id="T743" time="647.244" type="appl" />
         <tli id="T744" time="648.572" type="appl" />
         <tli id="T745" time="649.968" type="appl" />
         <tli id="T746" time="651.363" type="appl" />
         <tli id="T747" time="652.758" type="appl" />
         <tli id="T748" time="654.154" type="appl" />
         <tli id="T749" time="655.021" type="appl" />
         <tli id="T750" time="655.762" type="appl" />
         <tli id="T751" time="656.504" type="appl" />
         <tli id="T752" time="657.245" type="appl" />
         <tli id="T753" time="657.986" type="appl" />
         <tli id="T754" time="658.682" type="appl" />
         <tli id="T755" time="659.353" type="appl" />
         <tli id="T756" time="660.025" type="appl" />
         <tli id="T757" time="660.697" type="appl" />
         <tli id="T758" time="663.6286331162793" />
         <tli id="T759" time="664.61" type="appl" />
         <tli id="T760" time="665.404" type="appl" />
         <tli id="T761" time="666.198" type="appl" />
         <tli id="T762" time="666.991" type="appl" />
         <tli id="T763" time="667.785" type="appl" />
         <tli id="T764" time="668.579" type="appl" />
         <tli id="T765" time="669.8219225850123" />
         <tli id="T766" time="670.753" type="appl" />
         <tli id="T767" time="671.7952419421111" />
         <tli id="T768" time="672.116" type="appl" />
         <tli id="T769" time="672.54" type="appl" />
         <tli id="T770" time="672.964" type="appl" />
         <tli id="T771" time="673.387" type="appl" />
         <tli id="T772" time="673.81" type="appl" />
         <tli id="T773" time="674.1939749528415" />
         <tli id="T774" time="674.89" type="appl" />
         <tli id="T775" time="675.546" type="appl" />
         <tli id="T776" time="676.201" type="appl" />
         <tli id="T777" time="677.2752031297308" />
         <tli id="T778" time="677.932" type="appl" />
         <tli id="T779" time="678.723" type="appl" />
         <tli id="T780" time="679.514" type="appl" />
         <tli id="T781" time="680.306" type="appl" />
         <tli id="T782" time="681.068" type="appl" />
         <tli id="T783" time="681.83" type="appl" />
         <tli id="T784" time="682.593" type="appl" />
         <tli id="T785" time="683.2816710049799" />
         <tli id="T786" time="683.998" type="appl" />
         <tli id="T787" time="684.642" type="appl" />
         <tli id="T788" time="685.285" type="appl" />
         <tli id="T789" time="685.928" type="appl" />
         <tli id="T790" time="686.429" type="appl" />
         <tli id="T791" time="686.93" type="appl" />
         <tli id="T792" time="689.2617849002171" />
         <tli id="T793" time="689.781" type="appl" />
         <tli id="T794" time="690.771" type="appl" />
         <tli id="T795" time="691.761" type="appl" />
         <tli id="T796" time="692.558" type="appl" />
         <tli id="T797" time="693.354" type="appl" />
         <tli id="T798" time="694.15" type="appl" />
         <tli id="T799" time="694.947" type="appl" />
         <tli id="T800" time="695.743" type="appl" />
         <tli id="T801" time="696.54" type="appl" />
         <tli id="T802" time="697.078" type="appl" />
         <tli id="T803" time="697.616" type="appl" />
         <tli id="T804" time="698.155" type="appl" />
         <tli id="T805" time="698.693" type="appl" />
         <tli id="T806" time="699.485" type="appl" />
         <tli id="T807" time="699.921" type="appl" />
         <tli id="T808" time="700.358" type="appl" />
         <tli id="T809" time="701.044" type="appl" />
         <tli id="T810" time="701.73" type="appl" />
         <tli id="T811" time="702.416" type="appl" />
         <tli id="T812" time="703.102" type="appl" />
         <tli id="T813" time="703.703" type="appl" />
         <tli id="T814" time="704.304" type="appl" />
         <tli id="T815" time="704.905" type="appl" />
         <tli id="T816" time="706.5816622304305" />
         <tli id="T817" time="707.83" type="appl" />
         <tli id="T818" time="708.721" type="appl" />
         <tli id="T819" time="709.359" type="appl" />
         <tli id="T820" time="709.997" type="appl" />
         <tli id="T821" time="710.636" type="appl" />
         <tli id="T822" time="711.274" type="appl" />
         <tli id="T823" time="711.9816239846542" />
         <tli id="T824" time="712.538" type="appl" />
         <tli id="T825" time="713.164" type="appl" />
         <tli id="T826" time="713.79" type="appl" />
         <tli id="T827" time="714.448" type="appl" />
         <tli id="T828" time="715.105" type="appl" />
         <tli id="T829" time="716.098" type="appl" />
         <tli id="T830" time="718.2215797895346" />
         <tli id="T831" time="718.917" type="appl" />
         <tli id="T832" time="719.641" type="appl" />
         <tli id="T833" time="720.366" type="appl" />
         <tli id="T834" time="721.09" type="appl" />
         <tli id="T835" time="721.6873364929617" />
         <tli id="T836" time="723.223" type="appl" />
         <tli id="T837" time="724.631" type="appl" />
         <tli id="T838" time="726.04" type="appl" />
         <tli id="T839" time="727.448" type="appl" />
         <tli id="T840" time="729.3348344121158" />
         <tli id="T841" time="730.348" type="appl" />
         <tli id="T842" time="731.325" type="appl" />
         <tli id="T843" time="732.4614789340058" />
         <tli id="T844" time="733.101" type="appl" />
         <tli id="T845" time="733.792" type="appl" />
         <tli id="T846" time="734.484" type="appl" />
         <tli id="T847" time="735.175" type="appl" />
         <tli id="T848" time="736.011" type="appl" />
         <tli id="T849" time="736.848" type="appl" />
         <tli id="T850" time="737.684" type="appl" />
         <tli id="T851" time="738.521" type="appl" />
         <tli id="T852" time="739.357" type="appl" />
         <tli id="T853" time="740.194" type="appl" />
         <tli id="T854" time="741.03" type="appl" />
         <tli id="T855" time="741.684" type="appl" />
         <tli id="T856" time="742.337" type="appl" />
         <tli id="T857" time="742.991" type="appl" />
         <tli id="T858" time="743.644" type="appl" />
         <tli id="T859" time="744.298" type="appl" />
         <tli id="T860" time="744.951" type="appl" />
         <tli id="T861" time="745.605" type="appl" />
         <tli id="T862" time="746.258" type="appl" />
         <tli id="T863" time="746.912" type="appl" />
         <tli id="T864" time="751.97" type="appl" />
         <tli id="T865" time="752.822" type="appl" />
         <tli id="T866" time="753.654" type="appl" />
         <tli id="T867" time="754.485" type="appl" />
         <tli id="T868" time="757.4613018702261" />
         <tli id="T869" time="758.522" type="appl" />
         <tli id="T870" time="759.264" type="appl" />
         <tli id="T871" time="760.006" type="appl" />
         <tli id="T872" time="760.748" type="appl" />
         <tli id="T873" time="761.49" type="appl" />
         <tli id="T874" time="762.233" type="appl" />
         <tli id="T875" time="762.975" type="appl" />
         <tli id="T876" time="763.717" type="appl" />
         <tli id="T877" time="764.459" type="appl" />
         <tli id="T878" time="765.201" type="appl" />
         <tli id="T879" time="765.738" type="appl" />
         <tli id="T880" time="766.275" type="appl" />
         <tli id="T881" time="766.812" type="appl" />
         <tli id="T882" time="767.348" type="appl" />
         <tli id="T883" time="767.885" type="appl" />
         <tli id="T884" time="768.422" type="appl" />
         <tli id="T885" time="768.952" type="appl" />
         <tli id="T886" time="769.481" type="appl" />
         <tli id="T887" time="770.01" type="appl" />
         <tli id="T888" time="770.54" type="appl" />
         <tli id="T890" time="770.667" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T2" start="T0">
            <tli id="T0.tx.1" />
         </timeline-fork>
         <timeline-fork end="T278" start="T277">
            <tli id="T277.tx.1" />
         </timeline-fork>
         <timeline-fork end="T321" start="T319">
            <tli id="T319.tx.1" />
         </timeline-fork>
         <timeline-fork end="T658" start="T657">
            <tli id="T657.tx.1" />
         </timeline-fork>
         <timeline-fork end="T687" start="T686">
            <tli id="T686.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T888" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <nts id="Seg_3" n="HIAT:ip">(</nts>
                  <nts id="Seg_4" n="HIAT:ip">(</nts>
                  <ats e="T0.tx.1" id="Seg_5" n="HIAT:non-pho" s="T0">DMG</ats>
                  <nts id="Seg_6" n="HIAT:ip">)</nts>
                  <nts id="Seg_7" n="HIAT:ip">)</nts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_9" n="HIAT:ip">(</nts>
                  <ts e="T2" id="Seg_11" n="HIAT:w" s="T0.tx.1">imsobie</ts>
                  <nts id="Seg_12" n="HIAT:ip">)</nts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_15" n="HIAT:w" s="T2">tarzittə</ts>
                  <nts id="Seg_16" n="HIAT:ip">.</nts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_19" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_21" n="HIAT:w" s="T3">Măn</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_24" n="HIAT:w" s="T4">dĭʔnə</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_27" n="HIAT:w" s="T5">embiem</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_30" n="HIAT:w" s="T6">beškeʔi</ts>
                  <nts id="Seg_31" n="HIAT:ip">,</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_34" n="HIAT:w" s="T7">kajak</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_37" n="HIAT:w" s="T8">mĭbiem</ts>
                  <nts id="Seg_38" n="HIAT:ip">,</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_41" n="HIAT:w" s="T9">aktʼa</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_44" n="HIAT:w" s="T10">mĭbiem</ts>
                  <nts id="Seg_45" n="HIAT:ip">.</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_48" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_50" n="HIAT:w" s="T11">Oʔb</ts>
                  <nts id="Seg_51" n="HIAT:ip">,</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_54" n="HIAT:w" s="T12">šide</ts>
                  <nts id="Seg_55" n="HIAT:ip">,</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_58" n="HIAT:w" s="T13">nagur</ts>
                  <nts id="Seg_59" n="HIAT:ip">,</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_62" n="HIAT:w" s="T14">teʔtə</ts>
                  <nts id="Seg_63" n="HIAT:ip">.</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_66" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_68" n="HIAT:w" s="T15">Dĭzeŋ</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_71" n="HIAT:w" s="T16">tura</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_74" n="HIAT:w" s="T17">ibiʔi</ts>
                  <nts id="Seg_75" n="HIAT:ip">,</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_78" n="HIAT:w" s="T18">šindidə</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_81" n="HIAT:w" s="T19">bar</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_84" n="HIAT:w" s="T20">sadarlaʔbə</ts>
                  <nts id="Seg_85" n="HIAT:ip">.</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_88" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_90" n="HIAT:w" s="T21">I</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_93" n="HIAT:w" s="T22">aktʼa</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_96" n="HIAT:w" s="T23">ej</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_99" n="HIAT:w" s="T24">kabarlia</ts>
                  <nts id="Seg_100" n="HIAT:ip">.</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_103" n="HIAT:u" s="T25">
                  <nts id="Seg_104" n="HIAT:ip">(</nts>
                  <nts id="Seg_105" n="HIAT:ip">(</nts>
                  <ats e="T26" id="Seg_106" n="HIAT:non-pho" s="T25">BRK</ats>
                  <nts id="Seg_107" n="HIAT:ip">)</nts>
                  <nts id="Seg_108" n="HIAT:ip">)</nts>
                  <nts id="Seg_109" n="HIAT:ip">.</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_112" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_114" n="HIAT:w" s="T26">Dĭzeŋ</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_117" n="HIAT:w" s="T27">dĭ</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_119" n="HIAT:ip">(</nts>
                  <ts e="T29" id="Seg_121" n="HIAT:w" s="T28">turab-</ts>
                  <nts id="Seg_122" n="HIAT:ip">)</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_125" n="HIAT:w" s="T29">tura</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_128" n="HIAT:w" s="T30">sadarla</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_131" n="HIAT:w" s="T31">ibiʔi</ts>
                  <nts id="Seg_132" n="HIAT:ip">.</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_135" n="HIAT:u" s="T32">
                  <nts id="Seg_136" n="HIAT:ip">(</nts>
                  <nts id="Seg_137" n="HIAT:ip">(</nts>
                  <ats e="T33" id="Seg_138" n="HIAT:non-pho" s="T32">BRK</ats>
                  <nts id="Seg_139" n="HIAT:ip">)</nts>
                  <nts id="Seg_140" n="HIAT:ip">)</nts>
                  <nts id="Seg_141" n="HIAT:ip">.</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_144" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_146" n="HIAT:w" s="T33">A</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_149" n="HIAT:w" s="T34">girgit</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_152" n="HIAT:w" s="T35">kuzagən</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_155" n="HIAT:w" s="T36">naga</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_157" n="HIAT:ip">(</nts>
                  <ts e="T38" id="Seg_159" n="HIAT:w" s="T37">iza-</ts>
                  <nts id="Seg_160" n="HIAT:ip">)</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_162" n="HIAT:ip">(</nts>
                  <ts e="T39" id="Seg_164" n="HIAT:w" s="T38">isaʔtə</ts>
                  <nts id="Seg_165" n="HIAT:ip">)</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_168" n="HIAT:w" s="T39">aktʼa</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_171" n="HIAT:w" s="T40">mĭzittə</ts>
                  <nts id="Seg_172" n="HIAT:ip">.</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_175" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_177" n="HIAT:w" s="T41">Tĭn</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_180" n="HIAT:w" s="T42">tüžöjdə</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_183" n="HIAT:w" s="T43">il</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_186" n="HIAT:w" s="T44">ibiʔi</ts>
                  <nts id="Seg_187" n="HIAT:ip">.</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_190" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_192" n="HIAT:w" s="T45">Ĭmbi</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_195" n="HIAT:w" s="T46">ige</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_198" n="HIAT:w" s="T47">bar</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_201" n="HIAT:w" s="T48">iluʔpiʔi</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_204" n="HIAT:w" s="T49">i</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_207" n="HIAT:w" s="T50">mĭbiʔi</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_210" n="HIAT:w" s="T51">aktʼa</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_213" n="HIAT:w" s="T52">dĭbər</ts>
                  <nts id="Seg_214" n="HIAT:ip">.</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_217" n="HIAT:u" s="T53">
                  <nts id="Seg_218" n="HIAT:ip">(</nts>
                  <nts id="Seg_219" n="HIAT:ip">(</nts>
                  <ats e="T54" id="Seg_220" n="HIAT:non-pho" s="T53">BRK</ats>
                  <nts id="Seg_221" n="HIAT:ip">)</nts>
                  <nts id="Seg_222" n="HIAT:ip">)</nts>
                  <nts id="Seg_223" n="HIAT:ip">.</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_226" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_228" n="HIAT:w" s="T54">Măn</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_231" n="HIAT:w" s="T55">tuganbə</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T892" id="Seg_234" n="HIAT:w" s="T56">kalla</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_237" n="HIAT:w" s="T892">dʼürbi</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_240" n="HIAT:w" s="T57">gorăttə</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_243" n="HIAT:w" s="T58">i</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_246" n="HIAT:w" s="T59">dĭn</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_249" n="HIAT:w" s="T60">mašina</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_252" n="HIAT:w" s="T61">iləj</ts>
                  <nts id="Seg_253" n="HIAT:ip">,</nts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_256" n="HIAT:w" s="T62">oldʼa</ts>
                  <nts id="Seg_257" n="HIAT:ip">,</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_260" n="HIAT:w" s="T63">bazəsʼtə</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_262" n="HIAT:ip">(</nts>
                  <ts e="T65" id="Seg_264" n="HIAT:w" s="T64">kujnek=</ts>
                  <nts id="Seg_265" n="HIAT:ip">)</nts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_268" n="HIAT:w" s="T65">kujnegəʔi</ts>
                  <nts id="Seg_269" n="HIAT:ip">,</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_272" n="HIAT:w" s="T66">piʔmeʔi</ts>
                  <nts id="Seg_273" n="HIAT:ip">,</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_276" n="HIAT:w" s="T67">bar</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_278" n="HIAT:ip">(</nts>
                  <ts e="T69" id="Seg_280" n="HIAT:w" s="T68">ombi-</ts>
                  <nts id="Seg_281" n="HIAT:ip">)</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_284" n="HIAT:w" s="T69">ĭmbi</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_287" n="HIAT:w" s="T70">ige</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_290" n="HIAT:w" s="T71">bazəsʼtə</ts>
                  <nts id="Seg_291" n="HIAT:ip">.</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_294" n="HIAT:u" s="T72">
                  <nts id="Seg_295" n="HIAT:ip">(</nts>
                  <nts id="Seg_296" n="HIAT:ip">(</nts>
                  <ats e="T73" id="Seg_297" n="HIAT:non-pho" s="T72">BRK</ats>
                  <nts id="Seg_298" n="HIAT:ip">)</nts>
                  <nts id="Seg_299" n="HIAT:ip">)</nts>
                  <nts id="Seg_300" n="HIAT:ip">.</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_303" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_305" n="HIAT:w" s="T73">Tibi</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_308" n="HIAT:w" s="T74">i</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_311" n="HIAT:w" s="T75">ne</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_314" n="HIAT:w" s="T76">amnobiʔi</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_317" n="HIAT:w" s="T77">ugandə</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_320" n="HIAT:w" s="T78">jakše</ts>
                  <nts id="Seg_321" n="HIAT:ip">.</nts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_324" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_326" n="HIAT:w" s="T79">Ajirbiʔi</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_328" n="HIAT:ip">(</nts>
                  <ts e="T81" id="Seg_330" n="HIAT:w" s="T80">один=</ts>
                  <nts id="Seg_331" n="HIAT:ip">)</nts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_334" n="HIAT:w" s="T81">onʼiʔ</ts>
                  <nts id="Seg_335" n="HIAT:ip">…</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_338" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_340" n="HIAT:w" s="T83">Onʼiʔ</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_343" n="HIAT:w" s="T84">onʼiʔtə</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_346" n="HIAT:w" s="T85">ajirbiʔi</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_349" n="HIAT:w" s="T86">bar</ts>
                  <nts id="Seg_350" n="HIAT:ip">.</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_353" n="HIAT:u" s="T87">
                  <nts id="Seg_354" n="HIAT:ip">(</nts>
                  <ts e="T88" id="Seg_356" n="HIAT:w" s="T87">Kamrolaʔtəʔi</ts>
                  <nts id="Seg_357" n="HIAT:ip">)</nts>
                  <nts id="Seg_358" n="HIAT:ip">.</nts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_361" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_363" n="HIAT:w" s="T88">Panarlaʔbəʔjə</ts>
                  <nts id="Seg_364" n="HIAT:ip">.</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_367" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_369" n="HIAT:w" s="T89">I</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_372" n="HIAT:w" s="T90">dĭzeŋ</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_375" n="HIAT:w" s="T91">esseŋdə</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_378" n="HIAT:w" s="T92">ibiʔi</ts>
                  <nts id="Seg_379" n="HIAT:ip">,</nts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_382" n="HIAT:w" s="T93">ugandə</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_385" n="HIAT:w" s="T94">esseŋdə</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_388" n="HIAT:w" s="T95">jakše</ts>
                  <nts id="Seg_389" n="HIAT:ip">.</nts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_392" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_394" n="HIAT:w" s="T96">Ĭmbi</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_397" n="HIAT:w" s="T97">măllal</ts>
                  <nts id="Seg_398" n="HIAT:ip">,</nts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_401" n="HIAT:w" s="T98">dĭzeŋ</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_404" n="HIAT:w" s="T99">nʼilgölaʔbəʔjə</ts>
                  <nts id="Seg_405" n="HIAT:ip">.</nts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_408" n="HIAT:u" s="T100">
                  <nts id="Seg_409" n="HIAT:ip">(</nts>
                  <nts id="Seg_410" n="HIAT:ip">(</nts>
                  <ats e="T101" id="Seg_411" n="HIAT:non-pho" s="T100">BRK</ats>
                  <nts id="Seg_412" n="HIAT:ip">)</nts>
                  <nts id="Seg_413" n="HIAT:ip">)</nts>
                  <nts id="Seg_414" n="HIAT:ip">.</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_417" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_419" n="HIAT:w" s="T101">A</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_422" n="HIAT:w" s="T102">miʔ</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_425" n="HIAT:w" s="T103">esseŋ</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_428" n="HIAT:w" s="T104">ugandə</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_431" n="HIAT:w" s="T105">ej</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_434" n="HIAT:w" s="T106">jakše</ts>
                  <nts id="Seg_435" n="HIAT:ip">.</nts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_438" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_440" n="HIAT:w" s="T107">Dʼabrolaʔbəʔjə</ts>
                  <nts id="Seg_441" n="HIAT:ip">,</nts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_444" n="HIAT:w" s="T108">kudonzlaʔbəʔjə</ts>
                  <nts id="Seg_445" n="HIAT:ip">,</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_448" n="HIAT:w" s="T109">ej</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_451" n="HIAT:w" s="T110">nʼilgölaʔbəʔjə</ts>
                  <nts id="Seg_452" n="HIAT:ip">.</nts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_455" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_457" n="HIAT:w" s="T111">Măn</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_460" n="HIAT:w" s="T112">dĭzem</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_463" n="HIAT:w" s="T113">bar</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_466" n="HIAT:w" s="T114">münörlaʔbəm</ts>
                  <nts id="Seg_467" n="HIAT:ip">.</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_470" n="HIAT:u" s="T115">
                  <nts id="Seg_471" n="HIAT:ip">(</nts>
                  <nts id="Seg_472" n="HIAT:ip">(</nts>
                  <ats e="T116" id="Seg_473" n="HIAT:non-pho" s="T115">BRK</ats>
                  <nts id="Seg_474" n="HIAT:ip">)</nts>
                  <nts id="Seg_475" n="HIAT:ip">)</nts>
                  <nts id="Seg_476" n="HIAT:ip">.</nts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_479" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_481" n="HIAT:w" s="T116">Esseŋbə</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_484" n="HIAT:w" s="T117">tüšəlliem</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_487" n="HIAT:w" s="T118">sazən</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_490" n="HIAT:w" s="T119">pʼaŋdəsʼtə</ts>
                  <nts id="Seg_491" n="HIAT:ip">.</nts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T121" id="Seg_494" n="HIAT:u" s="T120">
                  <nts id="Seg_495" n="HIAT:ip">(</nts>
                  <nts id="Seg_496" n="HIAT:ip">(</nts>
                  <ats e="T121" id="Seg_497" n="HIAT:non-pho" s="T120">BRK</ats>
                  <nts id="Seg_498" n="HIAT:ip">)</nts>
                  <nts id="Seg_499" n="HIAT:ip">)</nts>
                  <nts id="Seg_500" n="HIAT:ip">.</nts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_503" n="HIAT:u" s="T121">
                  <ts e="T122" id="Seg_505" n="HIAT:w" s="T121">Nuzaŋ</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_508" n="HIAT:w" s="T122">amnobiʔi</ts>
                  <nts id="Seg_509" n="HIAT:ip">.</nts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_512" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_514" n="HIAT:w" s="T123">Šindi</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_517" n="HIAT:w" s="T124">ugandə</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_520" n="HIAT:w" s="T125">aktʼa</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_523" n="HIAT:w" s="T126">iʔgö</ts>
                  <nts id="Seg_524" n="HIAT:ip">,</nts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_527" n="HIAT:w" s="T127">a</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_530" n="HIAT:w" s="T128">šindin</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_533" n="HIAT:w" s="T129">naga</ts>
                  <nts id="Seg_534" n="HIAT:ip">.</nts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_537" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_539" n="HIAT:w" s="T130">Dĭ</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_542" n="HIAT:w" s="T131">ara</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_545" n="HIAT:w" s="T132">bĭtlie</ts>
                  <nts id="Seg_546" n="HIAT:ip">.</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_549" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_551" n="HIAT:w" s="T133">Bar</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_554" n="HIAT:w" s="T134">aktʼa</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_557" n="HIAT:w" s="T135">bĭtluʔləj</ts>
                  <nts id="Seg_558" n="HIAT:ip">.</nts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_561" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_563" n="HIAT:w" s="T136">Dĭgəttə</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_566" n="HIAT:w" s="T137">ĭmbidə</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_569" n="HIAT:w" s="T138">naga</ts>
                  <nts id="Seg_570" n="HIAT:ip">.</nts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_573" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_575" n="HIAT:w" s="T139">Togonoria</ts>
                  <nts id="Seg_576" n="HIAT:ip">,</nts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_579" n="HIAT:w" s="T140">togonoria</ts>
                  <nts id="Seg_580" n="HIAT:ip">,</nts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_583" n="HIAT:w" s="T141">bar</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_586" n="HIAT:w" s="T142">bĭʔluʔləj</ts>
                  <nts id="Seg_587" n="HIAT:ip">.</nts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_590" n="HIAT:u" s="T143">
                  <nts id="Seg_591" n="HIAT:ip">(</nts>
                  <nts id="Seg_592" n="HIAT:ip">(</nts>
                  <ats e="T144" id="Seg_593" n="HIAT:non-pho" s="T143">BRK</ats>
                  <nts id="Seg_594" n="HIAT:ip">)</nts>
                  <nts id="Seg_595" n="HIAT:ip">)</nts>
                  <nts id="Seg_596" n="HIAT:ip">.</nts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_599" n="HIAT:u" s="T144">
                  <nts id="Seg_600" n="HIAT:ip">(</nts>
                  <nts id="Seg_601" n="HIAT:ip">(</nts>
                  <ats e="T145" id="Seg_602" n="HIAT:non-pho" s="T144">BRK</ats>
                  <nts id="Seg_603" n="HIAT:ip">)</nts>
                  <nts id="Seg_604" n="HIAT:ip">)</nts>
                  <nts id="Seg_605" n="HIAT:ip">.</nts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_608" n="HIAT:u" s="T145">
                  <ts e="T146" id="Seg_610" n="HIAT:w" s="T145">Sibirʼgən</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_613" n="HIAT:w" s="T146">ugandə</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_616" n="HIAT:w" s="T147">iʔgö</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_619" n="HIAT:w" s="T148">nuzaŋ</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_622" n="HIAT:w" s="T149">amnobiʔi</ts>
                  <nts id="Seg_623" n="HIAT:ip">.</nts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_626" n="HIAT:u" s="T150">
                  <ts e="T151" id="Seg_628" n="HIAT:w" s="T150">Dĭgəttə</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_631" n="HIAT:w" s="T151">sĭre</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_634" n="HIAT:w" s="T152">pa</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_637" n="HIAT:w" s="T153">özerluʔpi</ts>
                  <nts id="Seg_638" n="HIAT:ip">.</nts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_641" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_643" n="HIAT:w" s="T154">Dĭzeŋ</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_646" n="HIAT:w" s="T155">mălliaʔi:</ts>
                  <nts id="Seg_647" n="HIAT:ip">"</nts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_650" n="HIAT:w" s="T156">Nu</ts>
                  <nts id="Seg_651" n="HIAT:ip">,</nts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_653" n="HIAT:ip">(</nts>
                  <ts e="T158" id="Seg_655" n="HIAT:w" s="T157">kazaŋ-</ts>
                  <nts id="Seg_656" n="HIAT:ip">)</nts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_659" n="HIAT:w" s="T158">kazak</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_662" n="HIAT:w" s="T159">il</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_665" n="HIAT:w" s="T160">šoləʔjə</ts>
                  <nts id="Seg_666" n="HIAT:ip">,</nts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_669" n="HIAT:w" s="T161">miʔnʼibeʔ</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_672" n="HIAT:w" s="T162">ej</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_675" n="HIAT:w" s="T163">jakše</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_678" n="HIAT:w" s="T164">moləj</ts>
                  <nts id="Seg_679" n="HIAT:ip">"</nts>
                  <nts id="Seg_680" n="HIAT:ip">.</nts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_683" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_685" n="HIAT:w" s="T165">Dĭgəttə</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_688" n="HIAT:w" s="T166">labazəʔi</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_691" n="HIAT:w" s="T167">abiʔi</ts>
                  <nts id="Seg_692" n="HIAT:ip">.</nts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T174" id="Seg_695" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_697" n="HIAT:w" s="T168">Dĭbər</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_700" n="HIAT:w" s="T169">piʔi</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_703" n="HIAT:w" s="T170">tažerbiʔi</ts>
                  <nts id="Seg_704" n="HIAT:ip">,</nts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_707" n="HIAT:w" s="T171">i</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_710" n="HIAT:w" s="T172">baltu</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_713" n="HIAT:w" s="T173">ibiʔi</ts>
                  <nts id="Seg_714" n="HIAT:ip">.</nts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T183" id="Seg_717" n="HIAT:u" s="T174">
                  <ts e="T175" id="Seg_719" n="HIAT:w" s="T174">Jaʔpiʔi</ts>
                  <nts id="Seg_720" n="HIAT:ip">,</nts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_723" n="HIAT:w" s="T175">i</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_726" n="HIAT:w" s="T176">dĭ</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_729" n="HIAT:w" s="T177">saʔməluʔpi</ts>
                  <nts id="Seg_730" n="HIAT:ip">,</nts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_732" n="HIAT:ip">(</nts>
                  <ts e="T179" id="Seg_734" n="HIAT:w" s="T178">dĭzem</ts>
                  <nts id="Seg_735" n="HIAT:ip">)</nts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_738" n="HIAT:w" s="T179">dĭzeŋ</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_741" n="HIAT:w" s="T180">bar</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_744" n="HIAT:w" s="T181">dĭn</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_747" n="HIAT:w" s="T182">kübiʔi</ts>
                  <nts id="Seg_748" n="HIAT:ip">.</nts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_751" n="HIAT:u" s="T183">
                  <nts id="Seg_752" n="HIAT:ip">(</nts>
                  <nts id="Seg_753" n="HIAT:ip">(</nts>
                  <ats e="T184" id="Seg_754" n="HIAT:non-pho" s="T183">BRK</ats>
                  <nts id="Seg_755" n="HIAT:ip">)</nts>
                  <nts id="Seg_756" n="HIAT:ip">)</nts>
                  <nts id="Seg_757" n="HIAT:ip">.</nts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T192" id="Seg_760" n="HIAT:u" s="T184">
                  <ts e="T891" id="Seg_762" n="HIAT:w" s="T184">Lutʼšə</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_764" n="HIAT:ip">(</nts>
                  <ts e="T185" id="Seg_766" n="HIAT:w" s="T891">măna=</ts>
                  <nts id="Seg_767" n="HIAT:ip">)</nts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_770" n="HIAT:w" s="T185">măn</ts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_773" n="HIAT:w" s="T186">bɨ</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_776" n="HIAT:w" s="T187">kübiem</ts>
                  <nts id="Seg_777" n="HIAT:ip">,</nts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_780" n="HIAT:w" s="T188">чем</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_783" n="HIAT:w" s="T189">esseŋbə</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_786" n="HIAT:w" s="T190">dĭrgit</ts>
                  <nts id="Seg_787" n="HIAT:ip">.</nts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_790" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_792" n="HIAT:w" s="T192">Ej</ts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_795" n="HIAT:w" s="T193">jakšeʔi</ts>
                  <nts id="Seg_796" n="HIAT:ip">,</nts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_799" n="HIAT:w" s="T194">bar</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_802" n="HIAT:w" s="T195">tajirlaʔbəʔjə</ts>
                  <nts id="Seg_803" n="HIAT:ip">.</nts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_806" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_808" n="HIAT:w" s="T196">Ilzi</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_811" n="HIAT:w" s="T197">dʼabrolaʔbəʔjə</ts>
                  <nts id="Seg_812" n="HIAT:ip">.</nts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T199" id="Seg_815" n="HIAT:u" s="T198">
                  <nts id="Seg_816" n="HIAT:ip">(</nts>
                  <nts id="Seg_817" n="HIAT:ip">(</nts>
                  <ats e="T199" id="Seg_818" n="HIAT:non-pho" s="T198">BRK</ats>
                  <nts id="Seg_819" n="HIAT:ip">)</nts>
                  <nts id="Seg_820" n="HIAT:ip">)</nts>
                  <nts id="Seg_821" n="HIAT:ip">.</nts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T208" id="Seg_824" n="HIAT:u" s="T199">
                  <ts e="T200" id="Seg_826" n="HIAT:w" s="T199">Bar</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_829" n="HIAT:w" s="T200">ara</ts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_832" n="HIAT:w" s="T201">bĭtleʔbəʔjə</ts>
                  <nts id="Seg_833" n="HIAT:ip">,</nts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_836" n="HIAT:w" s="T202">ažnə</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_839" n="HIAT:w" s="T203">simazi</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_842" n="HIAT:w" s="T204">ej</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_845" n="HIAT:w" s="T205">molia</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_848" n="HIAT:w" s="T206">mănderzittə</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_851" n="HIAT:w" s="T207">dĭzeŋdə</ts>
                  <nts id="Seg_852" n="HIAT:ip">.</nts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_855" n="HIAT:u" s="T208">
                  <nts id="Seg_856" n="HIAT:ip">(</nts>
                  <nts id="Seg_857" n="HIAT:ip">(</nts>
                  <ats e="T209" id="Seg_858" n="HIAT:non-pho" s="T208">BRK</ats>
                  <nts id="Seg_859" n="HIAT:ip">)</nts>
                  <nts id="Seg_860" n="HIAT:ip">)</nts>
                  <nts id="Seg_861" n="HIAT:ip">.</nts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_864" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_866" n="HIAT:w" s="T209">Onʼiʔ</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_869" n="HIAT:w" s="T210">nen</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_872" n="HIAT:w" s="T211">nagur</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_875" n="HIAT:w" s="T212">nʼi</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_878" n="HIAT:w" s="T213">ibi</ts>
                  <nts id="Seg_879" n="HIAT:ip">,</nts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_882" n="HIAT:w" s="T214">šide</ts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_885" n="HIAT:w" s="T215">jakše</ts>
                  <nts id="Seg_886" n="HIAT:ip">,</nts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_889" n="HIAT:w" s="T216">a</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_892" n="HIAT:w" s="T217">onʼiʔ</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_895" n="HIAT:w" s="T218">ej</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_898" n="HIAT:w" s="T219">jakše</ts>
                  <nts id="Seg_899" n="HIAT:ip">.</nts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_902" n="HIAT:u" s="T220">
                  <ts e="T221" id="Seg_904" n="HIAT:w" s="T220">Bar</ts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_907" n="HIAT:w" s="T221">dĭm</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_910" n="HIAT:w" s="T222">münörbiʔi</ts>
                  <nts id="Seg_911" n="HIAT:ip">.</nts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_914" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_916" n="HIAT:w" s="T223">Üjüzi</ts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_919" n="HIAT:w" s="T224">bar</ts>
                  <nts id="Seg_920" n="HIAT:ip">,</nts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_923" n="HIAT:w" s="T225">i</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_925" n="HIAT:ip">(</nts>
                  <ts e="T227" id="Seg_927" n="HIAT:w" s="T226">muz-</ts>
                  <nts id="Seg_928" n="HIAT:ip">)</nts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_931" n="HIAT:w" s="T227">muzuruksi</ts>
                  <nts id="Seg_932" n="HIAT:ip">,</nts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_935" n="HIAT:w" s="T228">i</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_938" n="HIAT:w" s="T229">pazi</ts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_941" n="HIAT:w" s="T230">bar</ts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_943" n="HIAT:ip">(</nts>
                  <ts e="T232" id="Seg_945" n="HIAT:w" s="T231">kuno-</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_948" n="HIAT:w" s="T232">ku-</ts>
                  <nts id="Seg_949" n="HIAT:ip">)</nts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_952" n="HIAT:w" s="T233">toʔnarbiʔi</ts>
                  <nts id="Seg_953" n="HIAT:ip">.</nts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T239" id="Seg_956" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_958" n="HIAT:w" s="T234">A</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_961" n="HIAT:w" s="T235">măn</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_963" n="HIAT:ip">(</nts>
                  <ts e="T237" id="Seg_965" n="HIAT:w" s="T236">u</ts>
                  <nts id="Seg_966" n="HIAT:ip">)</nts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_969" n="HIAT:w" s="T237">mămbiam</ts>
                  <nts id="Seg_970" n="HIAT:ip">…</nts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T240" id="Seg_973" n="HIAT:u" s="T239">
                  <nts id="Seg_974" n="HIAT:ip">(</nts>
                  <nts id="Seg_975" n="HIAT:ip">(</nts>
                  <ats e="T240" id="Seg_976" n="HIAT:non-pho" s="T239">BRK</ats>
                  <nts id="Seg_977" n="HIAT:ip">)</nts>
                  <nts id="Seg_978" n="HIAT:ip">)</nts>
                  <nts id="Seg_979" n="HIAT:ip">.</nts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T248" id="Seg_982" n="HIAT:u" s="T240">
                  <ts e="T241" id="Seg_984" n="HIAT:w" s="T240">A</ts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_987" n="HIAT:w" s="T241">măn</ts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_989" n="HIAT:ip">(</nts>
                  <ts e="T243" id="Seg_991" n="HIAT:w" s="T242">u</ts>
                  <nts id="Seg_992" n="HIAT:ip">)</nts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_995" n="HIAT:w" s="T243">mămbiam:</ts>
                  <nts id="Seg_996" n="HIAT:ip">"</nts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_999" n="HIAT:w" s="T244">Iʔ</ts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_1002" n="HIAT:w" s="T245">üžüŋgeʔ</ts>
                  <nts id="Seg_1003" n="HIAT:ip">,</nts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_1006" n="HIAT:w" s="T246">možet</ts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_1009" n="HIAT:w" s="T247">özerləj</ts>
                  <nts id="Seg_1010" n="HIAT:ip">.</nts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T253" id="Seg_1013" n="HIAT:u" s="T248">
                  <ts e="T249" id="Seg_1015" n="HIAT:w" s="T248">Sagəštə</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_1018" n="HIAT:w" s="T249">iʔgö</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_1021" n="HIAT:w" s="T250">moləj</ts>
                  <nts id="Seg_1022" n="HIAT:ip">,</nts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_1025" n="HIAT:w" s="T251">urgo</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_1028" n="HIAT:w" s="T252">moləj</ts>
                  <nts id="Seg_1029" n="HIAT:ip">.</nts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T259" id="Seg_1032" n="HIAT:u" s="T253">
                  <ts e="T254" id="Seg_1034" n="HIAT:w" s="T253">Možet</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_1037" n="HIAT:w" s="T254">bar</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1039" n="HIAT:ip">(</nts>
                  <ts e="T256" id="Seg_1041" n="HIAT:w" s="T255">baruʔləj</ts>
                  <nts id="Seg_1042" n="HIAT:ip">)</nts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_1045" n="HIAT:w" s="T256">dĭgəttə</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_1048" n="HIAT:w" s="T257">jakše</ts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_1051" n="HIAT:w" s="T258">moləj</ts>
                  <nts id="Seg_1052" n="HIAT:ip">"</nts>
                  <nts id="Seg_1053" n="HIAT:ip">.</nts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_1056" n="HIAT:u" s="T259">
                  <nts id="Seg_1057" n="HIAT:ip">(</nts>
                  <nts id="Seg_1058" n="HIAT:ip">(</nts>
                  <ats e="T260" id="Seg_1059" n="HIAT:non-pho" s="T259">BRK</ats>
                  <nts id="Seg_1060" n="HIAT:ip">)</nts>
                  <nts id="Seg_1061" n="HIAT:ip">)</nts>
                  <nts id="Seg_1062" n="HIAT:ip">.</nts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T270" id="Seg_1065" n="HIAT:u" s="T260">
                  <ts e="T261" id="Seg_1067" n="HIAT:w" s="T260">A</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1070" n="HIAT:w" s="T261">dĭ</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1073" n="HIAT:w" s="T262">girgit</ts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1076" n="HIAT:w" s="T263">üdʼüge</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1079" n="HIAT:w" s="T264">ibi</ts>
                  <nts id="Seg_1080" n="HIAT:ip">,</nts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1083" n="HIAT:w" s="T265">i</ts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1086" n="HIAT:w" s="T266">urgo</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1088" n="HIAT:ip">(</nts>
                  <ts e="T268" id="Seg_1090" n="HIAT:w" s="T267">du-</ts>
                  <nts id="Seg_1091" n="HIAT:ip">)</nts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1094" n="HIAT:w" s="T268">dĭrgit</ts>
                  <nts id="Seg_1095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1097" n="HIAT:w" s="T269">že</ts>
                  <nts id="Seg_1098" n="HIAT:ip">.</nts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T273" id="Seg_1101" n="HIAT:u" s="T270">
                  <ts e="T271" id="Seg_1103" n="HIAT:w" s="T270">Ĭmbidə</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1106" n="HIAT:w" s="T271">naga</ts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1109" n="HIAT:w" s="T272">jakše</ts>
                  <nts id="Seg_1110" n="HIAT:ip">.</nts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T274" id="Seg_1113" n="HIAT:u" s="T273">
                  <nts id="Seg_1114" n="HIAT:ip">(</nts>
                  <nts id="Seg_1115" n="HIAT:ip">(</nts>
                  <ats e="T274" id="Seg_1116" n="HIAT:non-pho" s="T273">BRK</ats>
                  <nts id="Seg_1117" n="HIAT:ip">)</nts>
                  <nts id="Seg_1118" n="HIAT:ip">)</nts>
                  <nts id="Seg_1119" n="HIAT:ip">.</nts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T277" id="Seg_1122" n="HIAT:u" s="T274">
                  <ts e="T275" id="Seg_1124" n="HIAT:w" s="T274">Dĭ</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1127" n="HIAT:w" s="T275">armijanə</ts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1130" n="HIAT:w" s="T276">kambi</ts>
                  <nts id="Seg_1131" n="HIAT:ip">.</nts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T285" id="Seg_1134" n="HIAT:u" s="T277">
                  <ts e="T277.tx.1" id="Seg_1136" n="HIAT:w" s="T277">Vsʼo</ts>
                  <nts id="Seg_1137" n="HIAT:ip">_</nts>
                  <ts e="T278" id="Seg_1139" n="HIAT:w" s="T277.tx.1">ravno</ts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1142" n="HIAT:w" s="T278">šobi</ts>
                  <nts id="Seg_1143" n="HIAT:ip">,</nts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1146" n="HIAT:w" s="T279">gibər</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1149" n="HIAT:w" s="T280">togonorzittə</ts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1152" n="HIAT:w" s="T281">kaləj</ts>
                  <nts id="Seg_1153" n="HIAT:ip">,</nts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1156" n="HIAT:w" s="T282">dĭm</ts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1159" n="HIAT:w" s="T283">bar</ts>
                  <nts id="Seg_1160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1162" n="HIAT:w" s="T284">sürerleʔbəʔjə</ts>
                  <nts id="Seg_1163" n="HIAT:ip">.</nts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T286" id="Seg_1166" n="HIAT:u" s="T285">
                  <nts id="Seg_1167" n="HIAT:ip">(</nts>
                  <nts id="Seg_1168" n="HIAT:ip">(</nts>
                  <ats e="T286" id="Seg_1169" n="HIAT:non-pho" s="T285">BRK</ats>
                  <nts id="Seg_1170" n="HIAT:ip">)</nts>
                  <nts id="Seg_1171" n="HIAT:ip">)</nts>
                  <nts id="Seg_1172" n="HIAT:ip">.</nts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T292" id="Seg_1175" n="HIAT:u" s="T286">
                  <ts e="T287" id="Seg_1177" n="HIAT:w" s="T286">Onʼiʔ</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1180" n="HIAT:w" s="T287">nüke</ts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1183" n="HIAT:w" s="T288">nʼit</ts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1186" n="HIAT:w" s="T289">ibi</ts>
                  <nts id="Seg_1187" n="HIAT:ip">,</nts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1190" n="HIAT:w" s="T290">bar</ts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1193" n="HIAT:w" s="T291">jezerik</ts>
                  <nts id="Seg_1194" n="HIAT:ip">.</nts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1197" n="HIAT:u" s="T292">
                  <nts id="Seg_1198" n="HIAT:ip">"</nts>
                  <ts e="T293" id="Seg_1200" n="HIAT:w" s="T292">Tăn</ts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1203" n="HIAT:w" s="T293">ĭmbi</ts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1206" n="HIAT:w" s="T294">măna</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1209" n="HIAT:w" s="T295">ej</ts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1212" n="HIAT:w" s="T296">tüšəlbiel</ts>
                  <nts id="Seg_1213" n="HIAT:ip">?</nts>
                  <nts id="Seg_1214" n="HIAT:ip">"</nts>
                  <nts id="Seg_1215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T307" id="Seg_1217" n="HIAT:u" s="T297">
                  <ts e="T298" id="Seg_1219" n="HIAT:w" s="T297">A</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1222" n="HIAT:w" s="T298">dĭ</ts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1225" n="HIAT:w" s="T299">mămbi:</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1227" n="HIAT:ip">"</nts>
                  <ts e="T301" id="Seg_1229" n="HIAT:w" s="T300">Măn</ts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1232" n="HIAT:w" s="T301">tüšəlbiem</ts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1235" n="HIAT:w" s="T302">tănan</ts>
                  <nts id="Seg_1236" n="HIAT:ip">,</nts>
                  <nts id="Seg_1237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1239" n="HIAT:w" s="T303">ĭmbi</ts>
                  <nts id="Seg_1240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1242" n="HIAT:w" s="T304">kereʔ</ts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1244" n="HIAT:ip">(</nts>
                  <ts e="T306" id="Seg_1246" n="HIAT:w" s="T305">tăn=</ts>
                  <nts id="Seg_1247" n="HIAT:ip">)</nts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1250" n="HIAT:w" s="T306">tănan</ts>
                  <nts id="Seg_1251" n="HIAT:ip">?</nts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T310" id="Seg_1254" n="HIAT:u" s="T307">
                  <ts e="T308" id="Seg_1256" n="HIAT:w" s="T307">Tăn</ts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1259" n="HIAT:w" s="T308">диплом</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1262" n="HIAT:w" s="T309">ibiel</ts>
                  <nts id="Seg_1263" n="HIAT:ip">.</nts>
                  <nts id="Seg_1264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T316" id="Seg_1266" n="HIAT:u" s="T310">
                  <ts e="T311" id="Seg_1268" n="HIAT:w" s="T310">A</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1270" n="HIAT:ip">(</nts>
                  <ts e="T312" id="Seg_1272" n="HIAT:w" s="T311">dĭ</ts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1275" n="HIAT:w" s="T312">bar</ts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1278" n="HIAT:w" s="T313">muzu-</ts>
                  <nts id="Seg_1279" n="HIAT:ip">)</nts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1282" n="HIAT:w" s="T314">Ara</ts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1285" n="HIAT:w" s="T315">bĭtliel</ts>
                  <nts id="Seg_1286" n="HIAT:ip">!</nts>
                  <nts id="Seg_1287" n="HIAT:ip">"</nts>
                  <nts id="Seg_1288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T324" id="Seg_1290" n="HIAT:u" s="T316">
                  <ts e="T317" id="Seg_1292" n="HIAT:w" s="T316">A</ts>
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1295" n="HIAT:w" s="T317">dĭ</ts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1298" n="HIAT:w" s="T318">bar</ts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319.tx.1" id="Seg_1301" n="HIAT:w" s="T319">muzuruksi</ts>
                  <nts id="Seg_1302" n="HIAT:ip">_</nts>
                  <ts e="T321" id="Seg_1304" n="HIAT:w" s="T319.tx.1">da</ts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1307" n="HIAT:w" s="T321">iat</ts>
                  <nts id="Seg_1308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1310" n="HIAT:w" s="T322">xatʼel</ts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1313" n="HIAT:w" s="T323">münörzittə</ts>
                  <nts id="Seg_1314" n="HIAT:ip">.</nts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T328" id="Seg_1317" n="HIAT:u" s="T324">
                  <ts e="T325" id="Seg_1319" n="HIAT:w" s="T324">A</ts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1322" n="HIAT:w" s="T325">il</ts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1325" n="HIAT:w" s="T326">ej</ts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1328" n="HIAT:w" s="T327">mĭbiʔi</ts>
                  <nts id="Seg_1329" n="HIAT:ip">.</nts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T329" id="Seg_1332" n="HIAT:u" s="T328">
                  <nts id="Seg_1333" n="HIAT:ip">(</nts>
                  <nts id="Seg_1334" n="HIAT:ip">(</nts>
                  <ats e="T329" id="Seg_1335" n="HIAT:non-pho" s="T328">BRK</ats>
                  <nts id="Seg_1336" n="HIAT:ip">)</nts>
                  <nts id="Seg_1337" n="HIAT:ip">)</nts>
                  <nts id="Seg_1338" n="HIAT:ip">.</nts>
                  <nts id="Seg_1339" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T334" id="Seg_1341" n="HIAT:u" s="T329">
                  <ts e="T330" id="Seg_1343" n="HIAT:w" s="T329">Măn</ts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1346" n="HIAT:w" s="T330">kagam</ts>
                  <nts id="Seg_1347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1349" n="HIAT:w" s="T331">văjnagən</ts>
                  <nts id="Seg_1350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1351" n="HIAT:ip">(</nts>
                  <ts e="T333" id="Seg_1353" n="HIAT:w" s="T332">ibiel=</ts>
                  <nts id="Seg_1354" n="HIAT:ip">)</nts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1357" n="HIAT:w" s="T333">ibie</ts>
                  <nts id="Seg_1358" n="HIAT:ip">.</nts>
                  <nts id="Seg_1359" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T338" id="Seg_1361" n="HIAT:u" s="T334">
                  <ts e="T335" id="Seg_1363" n="HIAT:w" s="T334">I</ts>
                  <nts id="Seg_1364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1366" n="HIAT:w" s="T335">dĭm</ts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1369" n="HIAT:w" s="T336">ibiʔi</ts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1372" n="HIAT:w" s="T337">plendə</ts>
                  <nts id="Seg_1373" n="HIAT:ip">.</nts>
                  <nts id="Seg_1374" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T345" id="Seg_1376" n="HIAT:u" s="T338">
                  <ts e="T339" id="Seg_1378" n="HIAT:w" s="T338">I</ts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1381" n="HIAT:w" s="T339">pravăj</ts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1384" n="HIAT:w" s="T340">udat</ts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1387" n="HIAT:w" s="T341">müjötsi</ts>
                  <nts id="Seg_1388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1389" n="HIAT:ip">(</nts>
                  <ts e="T343" id="Seg_1391" n="HIAT:w" s="T342">xat-</ts>
                  <nts id="Seg_1392" n="HIAT:ip">)</nts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1395" n="HIAT:w" s="T343">dʼagarzittə</ts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1398" n="HIAT:w" s="T344">xatʼeli</ts>
                  <nts id="Seg_1399" n="HIAT:ip">.</nts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T355" id="Seg_1402" n="HIAT:u" s="T345">
                  <ts e="T346" id="Seg_1404" n="HIAT:w" s="T345">A</ts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1407" n="HIAT:w" s="T346">dĭ</ts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1410" n="HIAT:w" s="T347">bar</ts>
                  <nts id="Seg_1411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1413" n="HIAT:w" s="T348">dʼorbi</ts>
                  <nts id="Seg_1414" n="HIAT:ip">,</nts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1417" n="HIAT:w" s="T349">šide</ts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1420" n="HIAT:w" s="T350">müjə</ts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1422" n="HIAT:ip">(</nts>
                  <ts e="T352" id="Seg_1424" n="HIAT:w" s="T351">m-</ts>
                  <nts id="Seg_1425" n="HIAT:ip">)</nts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1427" n="HIAT:ip">(</nts>
                  <ts e="T353" id="Seg_1429" n="HIAT:w" s="T352">maːtəbiʔi</ts>
                  <nts id="Seg_1430" n="HIAT:ip">)</nts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1432" n="HIAT:ip">(</nts>
                  <ts e="T354" id="Seg_1434" n="HIAT:w" s="T353">ему</ts>
                  <nts id="Seg_1435" n="HIAT:ip">)</nts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1438" n="HIAT:w" s="T354">dĭʔnə</ts>
                  <nts id="Seg_1439" n="HIAT:ip">.</nts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T356" id="Seg_1442" n="HIAT:u" s="T355">
                  <nts id="Seg_1443" n="HIAT:ip">(</nts>
                  <nts id="Seg_1444" n="HIAT:ip">(</nts>
                  <ats e="T356" id="Seg_1445" n="HIAT:non-pho" s="T355">BRK</ats>
                  <nts id="Seg_1446" n="HIAT:ip">)</nts>
                  <nts id="Seg_1447" n="HIAT:ip">)</nts>
                  <nts id="Seg_1448" n="HIAT:ip">.</nts>
                  <nts id="Seg_1449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T364" id="Seg_1451" n="HIAT:u" s="T356">
                  <ts e="T357" id="Seg_1453" n="HIAT:w" s="T356">Măn</ts>
                  <nts id="Seg_1454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1455" n="HIAT:ip">(</nts>
                  <ts e="T358" id="Seg_1457" n="HIAT:w" s="T357">u</ts>
                  <nts id="Seg_1458" n="HIAT:ip">)</nts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1461" n="HIAT:w" s="T358">kagam</ts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1464" n="HIAT:w" s="T359">nagur</ts>
                  <nts id="Seg_1465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1467" n="HIAT:w" s="T360">koʔbdo</ts>
                  <nts id="Seg_1468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1470" n="HIAT:w" s="T361">i</ts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1473" n="HIAT:w" s="T362">nagur</ts>
                  <nts id="Seg_1474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1476" n="HIAT:w" s="T363">nʼi</ts>
                  <nts id="Seg_1477" n="HIAT:ip">.</nts>
                  <nts id="Seg_1478" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T365" id="Seg_1480" n="HIAT:u" s="T364">
                  <nts id="Seg_1481" n="HIAT:ip">(</nts>
                  <nts id="Seg_1482" n="HIAT:ip">(</nts>
                  <ats e="T365" id="Seg_1483" n="HIAT:non-pho" s="T364">BRK</ats>
                  <nts id="Seg_1484" n="HIAT:ip">)</nts>
                  <nts id="Seg_1485" n="HIAT:ip">)</nts>
                  <nts id="Seg_1486" n="HIAT:ip">.</nts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T370" id="Seg_1489" n="HIAT:u" s="T365">
                  <ts e="T366" id="Seg_1491" n="HIAT:w" s="T365">Nörbəzittə</ts>
                  <nts id="Seg_1492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1494" n="HIAT:w" s="T366">tănan</ts>
                  <nts id="Seg_1495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1496" n="HIAT:ip">(</nts>
                  <ts e="T368" id="Seg_1498" n="HIAT:w" s="T367">sĭre</ts>
                  <nts id="Seg_1499" n="HIAT:ip">)</nts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1502" n="HIAT:w" s="T368">sĭre</ts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1505" n="HIAT:w" s="T369">buga</ts>
                  <nts id="Seg_1506" n="HIAT:ip">?</nts>
                  <nts id="Seg_1507" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T371" id="Seg_1509" n="HIAT:u" s="T370">
                  <nts id="Seg_1510" n="HIAT:ip">(</nts>
                  <nts id="Seg_1511" n="HIAT:ip">(</nts>
                  <ats e="T371" id="Seg_1512" n="HIAT:non-pho" s="T370">BRK</ats>
                  <nts id="Seg_1513" n="HIAT:ip">)</nts>
                  <nts id="Seg_1514" n="HIAT:ip">)</nts>
                  <nts id="Seg_1515" n="HIAT:ip">.</nts>
                  <nts id="Seg_1516" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T375" id="Seg_1518" n="HIAT:u" s="T371">
                  <ts e="T372" id="Seg_1520" n="HIAT:w" s="T371">Tăn</ts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1523" n="HIAT:w" s="T372">nörbit</ts>
                  <nts id="Seg_1524" n="HIAT:ip">,</nts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1527" n="HIAT:w" s="T373">măn</ts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1530" n="HIAT:w" s="T374">nörbit</ts>
                  <nts id="Seg_1531" n="HIAT:ip">.</nts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T379" id="Seg_1534" n="HIAT:u" s="T375">
                  <ts e="T376" id="Seg_1536" n="HIAT:w" s="T375">Mănzittə</ts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1539" n="HIAT:w" s="T376">tănan</ts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1542" n="HIAT:w" s="T377">kömə</ts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1545" n="HIAT:w" s="T378">buga</ts>
                  <nts id="Seg_1546" n="HIAT:ip">?</nts>
                  <nts id="Seg_1547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T380" id="Seg_1549" n="HIAT:u" s="T379">
                  <nts id="Seg_1550" n="HIAT:ip">(</nts>
                  <nts id="Seg_1551" n="HIAT:ip">(</nts>
                  <ats e="T380" id="Seg_1552" n="HIAT:non-pho" s="T379">BRK</ats>
                  <nts id="Seg_1553" n="HIAT:ip">)</nts>
                  <nts id="Seg_1554" n="HIAT:ip">)</nts>
                  <nts id="Seg_1555" n="HIAT:ip">.</nts>
                  <nts id="Seg_1556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T384" id="Seg_1558" n="HIAT:u" s="T380">
                  <ts e="T381" id="Seg_1560" n="HIAT:w" s="T380">Tăn</ts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1563" n="HIAT:w" s="T381">nörbit</ts>
                  <nts id="Seg_1564" n="HIAT:ip">,</nts>
                  <nts id="Seg_1565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1567" n="HIAT:w" s="T382">măn</ts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1570" n="HIAT:w" s="T383">nörbit</ts>
                  <nts id="Seg_1571" n="HIAT:ip">.</nts>
                  <nts id="Seg_1572" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T388" id="Seg_1574" n="HIAT:u" s="T384">
                  <ts e="T385" id="Seg_1576" n="HIAT:w" s="T384">Mănzittə</ts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1579" n="HIAT:w" s="T385">tănan</ts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1582" n="HIAT:w" s="T386">sagər</ts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1585" n="HIAT:w" s="T387">buga</ts>
                  <nts id="Seg_1586" n="HIAT:ip">?</nts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T392" id="Seg_1589" n="HIAT:u" s="T388">
                  <ts e="T389" id="Seg_1591" n="HIAT:w" s="T388">Kodur</ts>
                  <nts id="Seg_1592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1594" n="HIAT:w" s="T389">Kodur</ts>
                  <nts id="Seg_1595" n="HIAT:ip">,</nts>
                  <nts id="Seg_1596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1598" n="HIAT:w" s="T390">kömə</ts>
                  <nts id="Seg_1599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1601" n="HIAT:w" s="T391">părga</ts>
                  <nts id="Seg_1602" n="HIAT:ip">.</nts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T401" id="Seg_1605" n="HIAT:u" s="T392">
                  <ts e="T393" id="Seg_1607" n="HIAT:w" s="T392">Šobi</ts>
                  <nts id="Seg_1608" n="HIAT:ip">,</nts>
                  <nts id="Seg_1609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1611" n="HIAT:w" s="T393">dön</ts>
                  <nts id="Seg_1612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1614" n="HIAT:w" s="T394">il</ts>
                  <nts id="Seg_1615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1617" n="HIAT:w" s="T395">amnobiʔi</ts>
                  <nts id="Seg_1618" n="HIAT:ip">,</nts>
                  <nts id="Seg_1619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1621" n="HIAT:w" s="T396">dĭ</ts>
                  <nts id="Seg_1622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1624" n="HIAT:w" s="T397">lăpatka</ts>
                  <nts id="Seg_1625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1627" n="HIAT:w" s="T398">kubi</ts>
                  <nts id="Seg_1628" n="HIAT:ip">,</nts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1631" n="HIAT:w" s="T399">uja</ts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1634" n="HIAT:w" s="T400">naga</ts>
                  <nts id="Seg_1635" n="HIAT:ip">.</nts>
                  <nts id="Seg_1636" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T404" id="Seg_1638" n="HIAT:u" s="T401">
                  <ts e="T402" id="Seg_1640" n="HIAT:w" s="T401">Dĭgəttə</ts>
                  <nts id="Seg_1641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1643" n="HIAT:w" s="T402">kambi</ts>
                  <nts id="Seg_1644" n="HIAT:ip">,</nts>
                  <nts id="Seg_1645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1647" n="HIAT:w" s="T403">kambi</ts>
                  <nts id="Seg_1648" n="HIAT:ip">.</nts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T410" id="Seg_1651" n="HIAT:u" s="T404">
                  <ts e="T405" id="Seg_1653" n="HIAT:w" s="T404">Nu</ts>
                  <nts id="Seg_1654" n="HIAT:ip">,</nts>
                  <nts id="Seg_1655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1657" n="HIAT:w" s="T405">dĭn</ts>
                  <nts id="Seg_1658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1660" n="HIAT:w" s="T406">onʼiʔ</ts>
                  <nts id="Seg_1661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1663" n="HIAT:w" s="T407">kuza</ts>
                  <nts id="Seg_1664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1666" n="HIAT:w" s="T408">amnobi</ts>
                  <nts id="Seg_1667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1669" n="HIAT:w" s="T409">neziʔ</ts>
                  <nts id="Seg_1670" n="HIAT:ip">.</nts>
                  <nts id="Seg_1671" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T417" id="Seg_1673" n="HIAT:u" s="T410">
                  <ts e="T411" id="Seg_1675" n="HIAT:w" s="T410">Dĭ</ts>
                  <nts id="Seg_1676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1678" n="HIAT:w" s="T411">šobi</ts>
                  <nts id="Seg_1679" n="HIAT:ip">,</nts>
                  <nts id="Seg_1680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1681" n="HIAT:ip">(</nts>
                  <ts e="T413" id="Seg_1683" n="HIAT:w" s="T412">dĭzeŋ=</ts>
                  <nts id="Seg_1684" n="HIAT:ip">)</nts>
                  <nts id="Seg_1685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1687" n="HIAT:w" s="T413">dĭzeŋdə</ts>
                  <nts id="Seg_1688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1690" n="HIAT:w" s="T414">ular</ts>
                  <nts id="Seg_1691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1693" n="HIAT:w" s="T415">ibi</ts>
                  <nts id="Seg_1694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1696" n="HIAT:w" s="T416">onʼiʔ</ts>
                  <nts id="Seg_1697" n="HIAT:ip">.</nts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T426" id="Seg_1700" n="HIAT:u" s="T417">
                  <ts e="T418" id="Seg_1702" n="HIAT:w" s="T417">Dĭ</ts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1705" n="HIAT:w" s="T418">măndə:</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1707" n="HIAT:ip">"</nts>
                  <ts e="T420" id="Seg_1709" n="HIAT:w" s="T419">Nada</ts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1712" n="HIAT:w" s="T420">segi</ts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1715" n="HIAT:w" s="T421">bü</ts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1718" n="HIAT:w" s="T422">mĭnzərzittə</ts>
                  <nts id="Seg_1719" n="HIAT:ip">,</nts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1722" n="HIAT:w" s="T423">kuzam</ts>
                  <nts id="Seg_1723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1724" n="HIAT:ip">(</nts>
                  <ts e="T425" id="Seg_1726" n="HIAT:w" s="T424">bĭz-</ts>
                  <nts id="Seg_1727" n="HIAT:ip">)</nts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1730" n="HIAT:w" s="T425">bădəsʼtə</ts>
                  <nts id="Seg_1731" n="HIAT:ip">"</nts>
                  <nts id="Seg_1732" n="HIAT:ip">.</nts>
                  <nts id="Seg_1733" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T434" id="Seg_1735" n="HIAT:u" s="T426">
                  <ts e="T427" id="Seg_1737" n="HIAT:w" s="T426">A</ts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1740" n="HIAT:w" s="T427">dĭ</ts>
                  <nts id="Seg_1741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1743" n="HIAT:w" s="T428">măndə:</ts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1745" n="HIAT:ip">"</nts>
                  <ts e="T430" id="Seg_1747" n="HIAT:w" s="T429">Iʔ</ts>
                  <nts id="Seg_1748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1750" n="HIAT:w" s="T430">mĭnzəreʔ</ts>
                  <nts id="Seg_1751" n="HIAT:ip">,</nts>
                  <nts id="Seg_1752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1754" n="HIAT:w" s="T431">măn</ts>
                  <nts id="Seg_1755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1757" n="HIAT:w" s="T432">ujam</ts>
                  <nts id="Seg_1758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1760" n="HIAT:w" s="T433">ige</ts>
                  <nts id="Seg_1761" n="HIAT:ip">"</nts>
                  <nts id="Seg_1762" n="HIAT:ip">.</nts>
                  <nts id="Seg_1763" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T437" id="Seg_1765" n="HIAT:u" s="T434">
                  <ts e="T435" id="Seg_1767" n="HIAT:w" s="T434">Dĭgəttə</ts>
                  <nts id="Seg_1768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1770" n="HIAT:w" s="T435">aspaʔ</ts>
                  <nts id="Seg_1771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1773" n="HIAT:w" s="T436">edəbiʔi</ts>
                  <nts id="Seg_1774" n="HIAT:ip">.</nts>
                  <nts id="Seg_1775" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T449" id="Seg_1777" n="HIAT:u" s="T437">
                  <ts e="T438" id="Seg_1779" n="HIAT:w" s="T437">Tus</ts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1782" n="HIAT:w" s="T438">embiʔi</ts>
                  <nts id="Seg_1783" n="HIAT:ip">,</nts>
                  <nts id="Seg_1784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1786" n="HIAT:w" s="T439">dĭ</ts>
                  <nts id="Seg_1787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1788" n="HIAT:ip">(</nts>
                  <ts e="T441" id="Seg_1790" n="HIAT:w" s="T440">pa-</ts>
                  <nts id="Seg_1791" n="HIAT:ip">)</nts>
                  <nts id="Seg_1792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1794" n="HIAT:w" s="T441">kambi</ts>
                  <nts id="Seg_1795" n="HIAT:ip">,</nts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1797" n="HIAT:ip">(</nts>
                  <ts e="T443" id="Seg_1799" n="HIAT:w" s="T442">püje=</ts>
                  <nts id="Seg_1800" n="HIAT:ip">)</nts>
                  <nts id="Seg_1801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1803" n="HIAT:w" s="T443">püjet</ts>
                  <nts id="Seg_1804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1806" n="HIAT:w" s="T444">toʔnaːrbi</ts>
                  <nts id="Seg_1807" n="HIAT:ip">,</nts>
                  <nts id="Seg_1808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1809" n="HIAT:ip">(</nts>
                  <ts e="T446" id="Seg_1811" n="HIAT:w" s="T445">kemziʔ=</ts>
                  <nts id="Seg_1812" n="HIAT:ip">)</nts>
                  <nts id="Seg_1813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1815" n="HIAT:w" s="T446">kemziʔ</ts>
                  <nts id="Seg_1816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1818" n="HIAT:w" s="T447">lăpatkam</ts>
                  <nts id="Seg_1819" n="HIAT:ip">…</nts>
                  <nts id="Seg_1820" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T452" id="Seg_1822" n="HIAT:u" s="T449">
                  <ts e="T450" id="Seg_1824" n="HIAT:w" s="T449">Dĭgəttə</ts>
                  <nts id="Seg_1825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1827" n="HIAT:w" s="T450">aspaʔdə</ts>
                  <nts id="Seg_1828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1830" n="HIAT:w" s="T451">barəʔluʔbi</ts>
                  <nts id="Seg_1831" n="HIAT:ip">.</nts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T454" id="Seg_1834" n="HIAT:u" s="T452">
                  <ts e="T453" id="Seg_1836" n="HIAT:w" s="T452">Mĭnzərbi</ts>
                  <nts id="Seg_1837" n="HIAT:ip">,</nts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1840" n="HIAT:w" s="T453">mĭnzərbi</ts>
                  <nts id="Seg_1841" n="HIAT:ip">.</nts>
                  <nts id="Seg_1842" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T459" id="Seg_1844" n="HIAT:u" s="T454">
                  <ts e="T455" id="Seg_1846" n="HIAT:w" s="T454">Dĭgəttə</ts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1849" n="HIAT:w" s="T455">uja</ts>
                  <nts id="Seg_1850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1852" n="HIAT:w" s="T456">naga</ts>
                  <nts id="Seg_1853" n="HIAT:ip">,</nts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1856" n="HIAT:w" s="T457">onʼiʔ</ts>
                  <nts id="Seg_1857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1859" n="HIAT:w" s="T458">lăpatka</ts>
                  <nts id="Seg_1860" n="HIAT:ip">.</nts>
                  <nts id="Seg_1861" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T465" id="Seg_1863" n="HIAT:u" s="T459">
                  <ts e="T460" id="Seg_1865" n="HIAT:w" s="T459">Dĭ</ts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1868" n="HIAT:w" s="T460">bar</ts>
                  <nts id="Seg_1869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1870" n="HIAT:ip">(</nts>
                  <ts e="T462" id="Seg_1872" n="HIAT:w" s="T461">š-</ts>
                  <nts id="Seg_1873" n="HIAT:ip">)</nts>
                  <nts id="Seg_1874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1876" n="HIAT:w" s="T462">saʔməluʔbi</ts>
                  <nts id="Seg_1877" n="HIAT:ip">,</nts>
                  <nts id="Seg_1878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1880" n="HIAT:w" s="T463">davaj</ts>
                  <nts id="Seg_1881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1883" n="HIAT:w" s="T464">dʼorzittə</ts>
                  <nts id="Seg_1884" n="HIAT:ip">.</nts>
                  <nts id="Seg_1885" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T476" id="Seg_1887" n="HIAT:u" s="T465">
                  <ts e="T466" id="Seg_1889" n="HIAT:w" s="T465">A</ts>
                  <nts id="Seg_1890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1892" n="HIAT:w" s="T466">dĭzeŋ</ts>
                  <nts id="Seg_1893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1894" n="HIAT:ip">(</nts>
                  <ts e="T468" id="Seg_1896" n="HIAT:w" s="T467">măn-</ts>
                  <nts id="Seg_1897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1899" n="HIAT:w" s="T468">măndla-</ts>
                  <nts id="Seg_1900" n="HIAT:ip">)</nts>
                  <nts id="Seg_1901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1903" n="HIAT:w" s="T469">mămbiʔi:</ts>
                  <nts id="Seg_1904" n="HIAT:ip">"</nts>
                  <nts id="Seg_1905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1907" n="HIAT:w" s="T470">Tüjö</ts>
                  <nts id="Seg_1908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1910" n="HIAT:w" s="T471">ular</ts>
                  <nts id="Seg_1911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1913" n="HIAT:w" s="T472">dʼăgarlim</ts>
                  <nts id="Seg_1914" n="HIAT:ip">,</nts>
                  <nts id="Seg_1915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1917" n="HIAT:w" s="T473">tănan</ts>
                  <nts id="Seg_1918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1920" n="HIAT:w" s="T474">lăpatkam</ts>
                  <nts id="Seg_1921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1922" n="HIAT:ip">(</nts>
                  <ts e="T476" id="Seg_1924" n="HIAT:w" s="T475">mĭmbiem</ts>
                  <nts id="Seg_1925" n="HIAT:ip">)</nts>
                  <nts id="Seg_1926" n="HIAT:ip">"</nts>
                  <nts id="Seg_1927" n="HIAT:ip">.</nts>
                  <nts id="Seg_1928" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T480" id="Seg_1930" n="HIAT:u" s="T476">
                  <nts id="Seg_1931" n="HIAT:ip">"</nts>
                  <ts e="T477" id="Seg_1933" n="HIAT:w" s="T476">Măna</ts>
                  <nts id="Seg_1934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1936" n="HIAT:w" s="T477">ej</ts>
                  <nts id="Seg_1937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1939" n="HIAT:w" s="T478">kereʔ</ts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1942" n="HIAT:w" s="T479">lăpatka</ts>
                  <nts id="Seg_1943" n="HIAT:ip">!</nts>
                  <nts id="Seg_1944" n="HIAT:ip">"</nts>
                  <nts id="Seg_1945" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T484" id="Seg_1947" n="HIAT:u" s="T480">
                  <ts e="T481" id="Seg_1949" n="HIAT:w" s="T480">Dĭgəttə</ts>
                  <nts id="Seg_1950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1952" n="HIAT:w" s="T481">dĭʔnə</ts>
                  <nts id="Seg_1953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1955" n="HIAT:w" s="T482">ular</ts>
                  <nts id="Seg_1956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1958" n="HIAT:w" s="T483">mĭbiʔi</ts>
                  <nts id="Seg_1959" n="HIAT:ip">.</nts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T485" id="Seg_1962" n="HIAT:u" s="T484">
                  <nts id="Seg_1963" n="HIAT:ip">(</nts>
                  <nts id="Seg_1964" n="HIAT:ip">(</nts>
                  <ats e="T485" id="Seg_1965" n="HIAT:non-pho" s="T484">BRK</ats>
                  <nts id="Seg_1966" n="HIAT:ip">)</nts>
                  <nts id="Seg_1967" n="HIAT:ip">)</nts>
                  <nts id="Seg_1968" n="HIAT:ip">.</nts>
                  <nts id="Seg_1969" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T490" id="Seg_1971" n="HIAT:u" s="T485">
                  <ts e="T486" id="Seg_1973" n="HIAT:w" s="T485">Dĭgəttə</ts>
                  <nts id="Seg_1974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1976" n="HIAT:w" s="T486">dĭ</ts>
                  <nts id="Seg_1977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1979" n="HIAT:w" s="T487">Kodur</ts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1981" n="HIAT:ip">(</nts>
                  <ts e="T489" id="Seg_1983" n="HIAT:w" s="T488">š-</ts>
                  <nts id="Seg_1984" n="HIAT:ip">)</nts>
                  <nts id="Seg_1985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1987" n="HIAT:w" s="T489">suʔmiluʔpi</ts>
                  <nts id="Seg_1988" n="HIAT:ip">.</nts>
                  <nts id="Seg_1989" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T493" id="Seg_1991" n="HIAT:u" s="T490">
                  <ts e="T491" id="Seg_1993" n="HIAT:w" s="T490">Băzəjdəbi</ts>
                  <nts id="Seg_1994" n="HIAT:ip">,</nts>
                  <nts id="Seg_1995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1997" n="HIAT:w" s="T491">kĭškəbi</ts>
                  <nts id="Seg_1998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_2000" n="HIAT:w" s="T492">simat</ts>
                  <nts id="Seg_2001" n="HIAT:ip">.</nts>
                  <nts id="Seg_2002" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T496" id="Seg_2004" n="HIAT:u" s="T493">
                  <ts e="T494" id="Seg_2006" n="HIAT:w" s="T493">Ulardə</ts>
                  <nts id="Seg_2007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_2009" n="HIAT:w" s="T494">ibi</ts>
                  <nts id="Seg_2010" n="HIAT:ip">,</nts>
                  <nts id="Seg_2011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_2013" n="HIAT:w" s="T495">kambi</ts>
                  <nts id="Seg_2014" n="HIAT:ip">.</nts>
                  <nts id="Seg_2015" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T501" id="Seg_2017" n="HIAT:u" s="T496">
                  <ts e="T497" id="Seg_2019" n="HIAT:w" s="T496">Dĭgəttə</ts>
                  <nts id="Seg_2020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2021" n="HIAT:ip">(</nts>
                  <ts e="T498" id="Seg_2023" n="HIAT:w" s="T497">šo-</ts>
                  <nts id="Seg_2024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_2026" n="HIAT:w" s="T498">il=</ts>
                  <nts id="Seg_2027" n="HIAT:ip">)</nts>
                  <nts id="Seg_2028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_2030" n="HIAT:w" s="T499">ildə</ts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_2033" n="HIAT:w" s="T500">šobi</ts>
                  <nts id="Seg_2034" n="HIAT:ip">.</nts>
                  <nts id="Seg_2035" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T507" id="Seg_2037" n="HIAT:u" s="T501">
                  <ts e="T502" id="Seg_2039" n="HIAT:w" s="T501">Dĭn</ts>
                  <nts id="Seg_2040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_2042" n="HIAT:w" s="T502">tože</ts>
                  <nts id="Seg_2043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_2045" n="HIAT:w" s="T503">šide</ts>
                  <nts id="Seg_2046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_2048" n="HIAT:w" s="T504">ular</ts>
                  <nts id="Seg_2049" n="HIAT:ip">,</nts>
                  <nts id="Seg_2050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_2052" n="HIAT:w" s="T505">nʼi</ts>
                  <nts id="Seg_2053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_2055" n="HIAT:w" s="T506">ibi</ts>
                  <nts id="Seg_2056" n="HIAT:ip">.</nts>
                  <nts id="Seg_2057" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T511" id="Seg_2059" n="HIAT:u" s="T507">
                  <ts e="T508" id="Seg_2061" n="HIAT:w" s="T507">I</ts>
                  <nts id="Seg_2062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_2064" n="HIAT:w" s="T508">nʼin</ts>
                  <nts id="Seg_2065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_2067" n="HIAT:w" s="T509">ne</ts>
                  <nts id="Seg_2068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_2070" n="HIAT:w" s="T510">ibi</ts>
                  <nts id="Seg_2071" n="HIAT:ip">.</nts>
                  <nts id="Seg_2072" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T518" id="Seg_2074" n="HIAT:u" s="T511">
                  <ts e="T512" id="Seg_2076" n="HIAT:w" s="T511">Dĭgəttə</ts>
                  <nts id="Seg_2077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_2079" n="HIAT:w" s="T512">mălliaʔi:</ts>
                  <nts id="Seg_2080" n="HIAT:ip">"</nts>
                  <nts id="Seg_2081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_2083" n="HIAT:w" s="T513">Öʔlit</ts>
                  <nts id="Seg_2084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_2086" n="HIAT:w" s="T514">tăn</ts>
                  <nts id="Seg_2087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_2089" n="HIAT:w" s="T515">ular</ts>
                  <nts id="Seg_2090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_2092" n="HIAT:w" s="T516">miʔ</ts>
                  <nts id="Seg_2093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_2095" n="HIAT:w" s="T517">ularziʔ</ts>
                  <nts id="Seg_2096" n="HIAT:ip">!</nts>
                  <nts id="Seg_2097" n="HIAT:ip">"</nts>
                  <nts id="Seg_2098" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T526" id="Seg_2100" n="HIAT:u" s="T518">
                  <nts id="Seg_2101" n="HIAT:ip">"</nts>
                  <ts e="T519" id="Seg_2103" n="HIAT:w" s="T518">Dʼok</ts>
                  <nts id="Seg_2104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_2106" n="HIAT:w" s="T519">ej</ts>
                  <nts id="Seg_2107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_2109" n="HIAT:w" s="T520">öʔlim</ts>
                  <nts id="Seg_2110" n="HIAT:ip">,</nts>
                  <nts id="Seg_2111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_2113" n="HIAT:w" s="T521">šiʔ</ts>
                  <nts id="Seg_2114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_2116" n="HIAT:w" s="T522">ular</ts>
                  <nts id="Seg_2117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_2119" n="HIAT:w" s="T523">măn</ts>
                  <nts id="Seg_2120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_2122" n="HIAT:w" s="T524">ularbə</ts>
                  <nts id="Seg_2123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_2125" n="HIAT:w" s="T525">amnuʔbəʔjə</ts>
                  <nts id="Seg_2126" n="HIAT:ip">"</nts>
                  <nts id="Seg_2127" n="HIAT:ip">.</nts>
                  <nts id="Seg_2128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T531" id="Seg_2130" n="HIAT:u" s="T526">
                  <ts e="T527" id="Seg_2132" n="HIAT:w" s="T526">A</ts>
                  <nts id="Seg_2133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_2135" n="HIAT:w" s="T527">bostə</ts>
                  <nts id="Seg_2136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_2138" n="HIAT:w" s="T528">vsʼo</ts>
                  <nts id="Seg_2139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_2141" n="HIAT:w" s="T529">dʼăbaktəria</ts>
                  <nts id="Seg_2142" n="HIAT:ip">,</nts>
                  <nts id="Seg_2143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_2145" n="HIAT:w" s="T530">dʼăbaktəria</ts>
                  <nts id="Seg_2146" n="HIAT:ip">.</nts>
                  <nts id="Seg_2147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T539" id="Seg_2149" n="HIAT:u" s="T531">
                  <ts e="T532" id="Seg_2151" n="HIAT:w" s="T531">Dĭ</ts>
                  <nts id="Seg_2152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2153" n="HIAT:ip">(</nts>
                  <ts e="T533" id="Seg_2155" n="HIAT:w" s="T532">nep-</ts>
                  <nts id="Seg_2156" n="HIAT:ip">)</nts>
                  <nts id="Seg_2157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_2159" n="HIAT:w" s="T533">nuʔməluʔpi</ts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_2162" n="HIAT:w" s="T534">da</ts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_2165" n="HIAT:w" s="T535">ulardə</ts>
                  <nts id="Seg_2166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_2168" n="HIAT:w" s="T536">öʔləbi</ts>
                  <nts id="Seg_2169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2171" n="HIAT:w" s="T537">dĭʔnə</ts>
                  <nts id="Seg_2172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2174" n="HIAT:w" s="T538">ulardə</ts>
                  <nts id="Seg_2175" n="HIAT:ip">.</nts>
                  <nts id="Seg_2176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T545" id="Seg_2178" n="HIAT:u" s="T539">
                  <ts e="T540" id="Seg_2180" n="HIAT:w" s="T539">A</ts>
                  <nts id="Seg_2181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_2183" n="HIAT:w" s="T540">dĭgəttə</ts>
                  <nts id="Seg_2184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2186" n="HIAT:w" s="T541">iʔpiʔi</ts>
                  <nts id="Seg_2187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_2189" n="HIAT:w" s="T542">kunolzittə</ts>
                  <nts id="Seg_2190" n="HIAT:ip">,</nts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2193" n="HIAT:w" s="T543">ertən</ts>
                  <nts id="Seg_2194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2196" n="HIAT:w" s="T544">uʔpiʔi</ts>
                  <nts id="Seg_2197" n="HIAT:ip">.</nts>
                  <nts id="Seg_2198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T548" id="Seg_2200" n="HIAT:u" s="T545">
                  <ts e="T546" id="Seg_2202" n="HIAT:w" s="T545">A</ts>
                  <nts id="Seg_2203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2205" n="HIAT:w" s="T546">ular</ts>
                  <nts id="Seg_2206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2208" n="HIAT:w" s="T547">naga</ts>
                  <nts id="Seg_2209" n="HIAT:ip">.</nts>
                  <nts id="Seg_2210" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T549" id="Seg_2212" n="HIAT:u" s="T548">
                  <nts id="Seg_2213" n="HIAT:ip">(</nts>
                  <nts id="Seg_2214" n="HIAT:ip">(</nts>
                  <ats e="T549" id="Seg_2215" n="HIAT:non-pho" s="T548">BRK</ats>
                  <nts id="Seg_2216" n="HIAT:ip">)</nts>
                  <nts id="Seg_2217" n="HIAT:ip">)</nts>
                  <nts id="Seg_2218" n="HIAT:ip">.</nts>
                  <nts id="Seg_2219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T552" id="Seg_2221" n="HIAT:u" s="T549">
                  <ts e="T550" id="Seg_2223" n="HIAT:w" s="T549">Dĭ</ts>
                  <nts id="Seg_2224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2226" n="HIAT:w" s="T550">Kodur</ts>
                  <nts id="Seg_2227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2229" n="HIAT:w" s="T551">uʔbdəbi</ts>
                  <nts id="Seg_2230" n="HIAT:ip">.</nts>
                  <nts id="Seg_2231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T554" id="Seg_2233" n="HIAT:u" s="T552">
                  <ts e="T553" id="Seg_2235" n="HIAT:w" s="T552">Ulardə</ts>
                  <nts id="Seg_2236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2238" n="HIAT:w" s="T553">sajnʼeʔpi</ts>
                  <nts id="Seg_2239" n="HIAT:ip">.</nts>
                  <nts id="Seg_2240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T567" id="Seg_2242" n="HIAT:u" s="T554">
                  <ts e="T555" id="Seg_2244" n="HIAT:w" s="T554">I</ts>
                  <nts id="Seg_2245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2247" n="HIAT:w" s="T555">dĭ</ts>
                  <nts id="Seg_2248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2250" n="HIAT:w" s="T556">dĭzen</ts>
                  <nts id="Seg_2251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2253" n="HIAT:w" s="T557">ulardə</ts>
                  <nts id="Seg_2254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2256" n="HIAT:w" s="T558">bar</ts>
                  <nts id="Seg_2257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2258" n="HIAT:ip">(</nts>
                  <ts e="T560" id="Seg_2260" n="HIAT:w" s="T559">kemziʔ</ts>
                  <nts id="Seg_2261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2263" n="HIAT:w" s="T560">b-</ts>
                  <nts id="Seg_2264" n="HIAT:ip">)</nts>
                  <nts id="Seg_2265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2267" n="HIAT:w" s="T561">kemziʔ</ts>
                  <nts id="Seg_2268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2270" n="HIAT:w" s="T562">bar</ts>
                  <nts id="Seg_2271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2273" n="HIAT:w" s="T563">dʼüʔdəbi</ts>
                  <nts id="Seg_2274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2276" n="HIAT:w" s="T564">püjet</ts>
                  <nts id="Seg_2277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2279" n="HIAT:w" s="T565">i</ts>
                  <nts id="Seg_2280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2282" n="HIAT:w" s="T566">aŋdə</ts>
                  <nts id="Seg_2283" n="HIAT:ip">.</nts>
                  <nts id="Seg_2284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T580" id="Seg_2286" n="HIAT:u" s="T567">
                  <ts e="T568" id="Seg_2288" n="HIAT:w" s="T567">A</ts>
                  <nts id="Seg_2289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2291" n="HIAT:w" s="T568">ertən</ts>
                  <nts id="Seg_2292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2294" n="HIAT:w" s="T569">uʔbdəbiʔi</ts>
                  <nts id="Seg_2295" n="HIAT:ip">,</nts>
                  <nts id="Seg_2296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2298" n="HIAT:w" s="T570">măndəʔi:</ts>
                  <nts id="Seg_2299" n="HIAT:ip">"</nts>
                  <nts id="Seg_2300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2302" n="HIAT:w" s="T571">Šănap</ts>
                  <nts id="Seg_2303" n="HIAT:ip">,</nts>
                  <nts id="Seg_2304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2305" n="HIAT:ip">(</nts>
                  <ts e="T573" id="Seg_2307" n="HIAT:w" s="T572">na-</ts>
                  <nts id="Seg_2308" n="HIAT:ip">)</nts>
                  <nts id="Seg_2309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2311" n="HIAT:w" s="T573">miʔ</ts>
                  <nts id="Seg_2312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2314" n="HIAT:w" s="T574">ulardə</ts>
                  <nts id="Seg_2315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2317" n="HIAT:w" s="T575">dĭ</ts>
                  <nts id="Seg_2318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2320" n="HIAT:w" s="T576">dĭm</ts>
                  <nts id="Seg_2321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2323" n="HIAT:w" s="T577">ulardə</ts>
                  <nts id="Seg_2324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2326" n="HIAT:w" s="T578">bar</ts>
                  <nts id="Seg_2327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2329" n="HIAT:w" s="T579">sajnüžəʔpiʔi</ts>
                  <nts id="Seg_2330" n="HIAT:ip">"</nts>
                  <nts id="Seg_2331" n="HIAT:ip">.</nts>
                  <nts id="Seg_2332" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T581" id="Seg_2334" n="HIAT:u" s="T580">
                  <nts id="Seg_2335" n="HIAT:ip">(</nts>
                  <nts id="Seg_2336" n="HIAT:ip">(</nts>
                  <ats e="T581" id="Seg_2337" n="HIAT:non-pho" s="T580">BRK</ats>
                  <nts id="Seg_2338" n="HIAT:ip">)</nts>
                  <nts id="Seg_2339" n="HIAT:ip">)</nts>
                  <nts id="Seg_2340" n="HIAT:ip">.</nts>
                  <nts id="Seg_2341" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T584" id="Seg_2343" n="HIAT:u" s="T581">
                  <ts e="T582" id="Seg_2345" n="HIAT:w" s="T581">Dĭgəttə</ts>
                  <nts id="Seg_2346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2348" n="HIAT:w" s="T582">dĭ</ts>
                  <nts id="Seg_2349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2351" n="HIAT:w" s="T583">uʔbdəbi</ts>
                  <nts id="Seg_2352" n="HIAT:ip">.</nts>
                  <nts id="Seg_2353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T586" id="Seg_2355" n="HIAT:u" s="T584">
                  <ts e="T585" id="Seg_2357" n="HIAT:w" s="T584">Simabə</ts>
                  <nts id="Seg_2358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2360" n="HIAT:w" s="T585">băzəbi</ts>
                  <nts id="Seg_2361" n="HIAT:ip">.</nts>
                  <nts id="Seg_2362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T587" id="Seg_2364" n="HIAT:u" s="T586">
                  <nts id="Seg_2365" n="HIAT:ip">(</nts>
                  <ts e="T587" id="Seg_2367" n="HIAT:w" s="T586">Kĭškəbi</ts>
                  <nts id="Seg_2368" n="HIAT:ip">)</nts>
                  <nts id="Seg_2369" n="HIAT:ip">.</nts>
                  <nts id="Seg_2370" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T595" id="Seg_2372" n="HIAT:u" s="T587">
                  <ts e="T588" id="Seg_2374" n="HIAT:w" s="T587">Šide</ts>
                  <nts id="Seg_2375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2377" n="HIAT:w" s="T588">ular</ts>
                  <nts id="Seg_2378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2380" n="HIAT:w" s="T589">ibi</ts>
                  <nts id="Seg_2381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2383" n="HIAT:w" s="T590">i</ts>
                  <nts id="Seg_2384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2386" n="HIAT:w" s="T591">kandəga</ts>
                  <nts id="Seg_2387" n="HIAT:ip">,</nts>
                  <nts id="Seg_2388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2389" n="HIAT:ip">(</nts>
                  <ts e="T593" id="Seg_2391" n="HIAT:w" s="T592">šʼuʔ-</ts>
                  <nts id="Seg_2392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2394" n="HIAT:w" s="T593">ka-</ts>
                  <nts id="Seg_2395" n="HIAT:ip">)</nts>
                  <nts id="Seg_2396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2398" n="HIAT:w" s="T594">kalluʔpi</ts>
                  <nts id="Seg_2399" n="HIAT:ip">.</nts>
                  <nts id="Seg_2400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T598" id="Seg_2402" n="HIAT:u" s="T595">
                  <ts e="T596" id="Seg_2404" n="HIAT:w" s="T595">Dĭgəttə</ts>
                  <nts id="Seg_2405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2407" n="HIAT:w" s="T596">šonəga</ts>
                  <nts id="Seg_2408" n="HIAT:ip">,</nts>
                  <nts id="Seg_2409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2411" n="HIAT:w" s="T597">šonəga</ts>
                  <nts id="Seg_2412" n="HIAT:ip">.</nts>
                  <nts id="Seg_2413" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T602" id="Seg_2415" n="HIAT:u" s="T598">
                  <ts e="T599" id="Seg_2417" n="HIAT:w" s="T598">Dĭn</ts>
                  <nts id="Seg_2418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2420" n="HIAT:w" s="T599">bar</ts>
                  <nts id="Seg_2421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2423" n="HIAT:w" s="T600">krospa</ts>
                  <nts id="Seg_2424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2426" n="HIAT:w" s="T601">nulaʔbə</ts>
                  <nts id="Seg_2427" n="HIAT:ip">.</nts>
                  <nts id="Seg_2428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T613" id="Seg_2430" n="HIAT:u" s="T602">
                  <ts e="T603" id="Seg_2432" n="HIAT:w" s="T602">Dĭ</ts>
                  <nts id="Seg_2433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2435" n="HIAT:w" s="T603">tĭlbi</ts>
                  <nts id="Seg_2436" n="HIAT:ip">,</nts>
                  <nts id="Seg_2437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2439" n="HIAT:w" s="T604">dĭn</ts>
                  <nts id="Seg_2440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2442" n="HIAT:w" s="T605">nüke</ts>
                  <nts id="Seg_2443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2445" n="HIAT:w" s="T606">iʔbolaʔbə</ts>
                  <nts id="Seg_2446" n="HIAT:ip">,</nts>
                  <nts id="Seg_2447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2449" n="HIAT:w" s="T607">dĭ</ts>
                  <nts id="Seg_2450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2452" n="HIAT:w" s="T608">nüke</ts>
                  <nts id="Seg_2453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2455" n="HIAT:w" s="T609">ibi</ts>
                  <nts id="Seg_2456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2458" n="HIAT:w" s="T610">i</ts>
                  <nts id="Seg_2459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2461" n="HIAT:w" s="T611">šide</ts>
                  <nts id="Seg_2462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2464" n="HIAT:w" s="T612">ular</ts>
                  <nts id="Seg_2465" n="HIAT:ip">.</nts>
                  <nts id="Seg_2466" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T620" id="Seg_2468" n="HIAT:u" s="T613">
                  <ts e="T614" id="Seg_2470" n="HIAT:w" s="T613">I</ts>
                  <nts id="Seg_2471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2473" n="HIAT:w" s="T614">dĭ</ts>
                  <nts id="Seg_2474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2476" n="HIAT:w" s="T615">ularzaŋdə</ts>
                  <nts id="Seg_2477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2479" n="HIAT:w" s="T616">dĭ</ts>
                  <nts id="Seg_2480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2482" n="HIAT:w" s="T617">nükenə</ts>
                  <nts id="Seg_2483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2485" n="HIAT:w" s="T618">udazaŋdə</ts>
                  <nts id="Seg_2486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2488" n="HIAT:w" s="T619">sarbi</ts>
                  <nts id="Seg_2489" n="HIAT:ip">.</nts>
                  <nts id="Seg_2490" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T625" id="Seg_2492" n="HIAT:u" s="T620">
                  <nts id="Seg_2493" n="HIAT:ip">(</nts>
                  <ts e="T621" id="Seg_2495" n="HIAT:w" s="T620">Sĭjdə</ts>
                  <nts id="Seg_2496" n="HIAT:ip">)</nts>
                  <nts id="Seg_2497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2499" n="HIAT:w" s="T621">Sĭjgəndə</ts>
                  <nts id="Seg_2500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2502" n="HIAT:w" s="T622">tagaj</ts>
                  <nts id="Seg_2503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2504" n="HIAT:ip">(</nts>
                  <ts e="T624" id="Seg_2506" n="HIAT:w" s="T623">păʔ-</ts>
                  <nts id="Seg_2507" n="HIAT:ip">)</nts>
                  <nts id="Seg_2508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2510" n="HIAT:w" s="T624">păʔluʔpi</ts>
                  <nts id="Seg_2511" n="HIAT:ip">.</nts>
                  <nts id="Seg_2512" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T634" id="Seg_2514" n="HIAT:u" s="T625">
                  <ts e="T626" id="Seg_2516" n="HIAT:w" s="T625">Dĭgəttə</ts>
                  <nts id="Seg_2517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2519" n="HIAT:w" s="T626">šobi</ts>
                  <nts id="Seg_2520" n="HIAT:ip">,</nts>
                  <nts id="Seg_2521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2522" n="HIAT:ip">(</nts>
                  <ts e="T628" id="Seg_2524" n="HIAT:w" s="T627">dĭze-</ts>
                  <nts id="Seg_2525" n="HIAT:ip">)</nts>
                  <nts id="Seg_2526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2528" n="HIAT:w" s="T628">dĭn</ts>
                  <nts id="Seg_2529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2531" n="HIAT:w" s="T629">šide</ts>
                  <nts id="Seg_2532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2534" n="HIAT:w" s="T630">koʔbdo</ts>
                  <nts id="Seg_2535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2537" n="HIAT:w" s="T631">i</ts>
                  <nts id="Seg_2538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2540" n="HIAT:w" s="T632">büzʼe</ts>
                  <nts id="Seg_2541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2543" n="HIAT:w" s="T633">amnolaʔbə</ts>
                  <nts id="Seg_2544" n="HIAT:ip">.</nts>
                  <nts id="Seg_2545" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T641" id="Seg_2547" n="HIAT:u" s="T634">
                  <ts e="T635" id="Seg_2549" n="HIAT:w" s="T634">Dʼăbaktərlia</ts>
                  <nts id="Seg_2550" n="HIAT:ip">,</nts>
                  <nts id="Seg_2551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2553" n="HIAT:w" s="T635">dʼăbaktərlia:</ts>
                  <nts id="Seg_2554" n="HIAT:ip">"</nts>
                  <nts id="Seg_2555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2557" n="HIAT:w" s="T636">Măn</ts>
                  <nts id="Seg_2558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2560" n="HIAT:w" s="T637">dĭn</ts>
                  <nts id="Seg_2561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2563" n="HIAT:w" s="T638">nüke</ts>
                  <nts id="Seg_2564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2566" n="HIAT:w" s="T639">amnolaʔbə</ts>
                  <nts id="Seg_2567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2569" n="HIAT:w" s="T640">ularziʔ</ts>
                  <nts id="Seg_2570" n="HIAT:ip">"</nts>
                  <nts id="Seg_2571" n="HIAT:ip">.</nts>
                  <nts id="Seg_2572" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T647" id="Seg_2574" n="HIAT:u" s="T641">
                  <ts e="T642" id="Seg_2576" n="HIAT:w" s="T641">A</ts>
                  <nts id="Seg_2577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2579" n="HIAT:w" s="T642">bostə</ts>
                  <nts id="Seg_2580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2581" n="HIAT:ip">(</nts>
                  <ts e="T644" id="Seg_2583" n="HIAT:w" s="T643">šĭjgən-</ts>
                  <nts id="Seg_2584" n="HIAT:ip">)</nts>
                  <nts id="Seg_2585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2587" n="HIAT:w" s="T644">sĭjgəndə</ts>
                  <nts id="Seg_2588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2590" n="HIAT:w" s="T645">dagaj</ts>
                  <nts id="Seg_2591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2593" n="HIAT:w" s="T646">amnolaʔbə</ts>
                  <nts id="Seg_2594" n="HIAT:ip">.</nts>
                  <nts id="Seg_2595" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T654" id="Seg_2597" n="HIAT:u" s="T647">
                  <ts e="T648" id="Seg_2599" n="HIAT:w" s="T647">A</ts>
                  <nts id="Seg_2600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2602" n="HIAT:w" s="T648">koʔbsaŋ:</ts>
                  <nts id="Seg_2603" n="HIAT:ip">"</nts>
                  <nts id="Seg_2604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2606" n="HIAT:w" s="T649">Miʔ</ts>
                  <nts id="Seg_2607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2609" n="HIAT:w" s="T650">kalləbəj</ts>
                  <nts id="Seg_2610" n="HIAT:ip">,</nts>
                  <nts id="Seg_2611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2613" n="HIAT:w" s="T651">deʔləbəj</ts>
                  <nts id="Seg_2614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2616" n="HIAT:w" s="T652">nükel</ts>
                  <nts id="Seg_2617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2619" n="HIAT:w" s="T653">tăn</ts>
                  <nts id="Seg_2620" n="HIAT:ip">"</nts>
                  <nts id="Seg_2621" n="HIAT:ip">.</nts>
                  <nts id="Seg_2622" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T657" id="Seg_2624" n="HIAT:u" s="T654">
                  <nts id="Seg_2625" n="HIAT:ip">"</nts>
                  <ts e="T655" id="Seg_2627" n="HIAT:w" s="T654">Dĭ</ts>
                  <nts id="Seg_2628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2630" n="HIAT:w" s="T655">ugandə</ts>
                  <nts id="Seg_2631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2633" n="HIAT:w" s="T656">pimnie</ts>
                  <nts id="Seg_2634" n="HIAT:ip">.</nts>
                  <nts id="Seg_2635" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T662" id="Seg_2637" n="HIAT:u" s="T657">
                  <ts e="T657.tx.1" id="Seg_2639" n="HIAT:w" s="T657">A</ts>
                  <nts id="Seg_2640" n="HIAT:ip">_</nts>
                  <ts e="T658" id="Seg_2642" n="HIAT:w" s="T657.tx.1">to</ts>
                  <nts id="Seg_2643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2645" n="HIAT:w" s="T658">dʼagarləj</ts>
                  <nts id="Seg_2646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2647" n="HIAT:ip">(</nts>
                  <ts e="T660" id="Seg_2649" n="HIAT:w" s="T659">bostə=</ts>
                  <nts id="Seg_2650" n="HIAT:ip">)</nts>
                  <nts id="Seg_2651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2653" n="HIAT:w" s="T660">bostə</ts>
                  <nts id="Seg_2654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2656" n="HIAT:w" s="T661">себя</ts>
                  <nts id="Seg_2657" n="HIAT:ip">"</nts>
                  <nts id="Seg_2658" n="HIAT:ip">.</nts>
                  <nts id="Seg_2659" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T667" id="Seg_2661" n="HIAT:u" s="T662">
                  <ts e="T663" id="Seg_2663" n="HIAT:w" s="T662">Dĭgəttə</ts>
                  <nts id="Seg_2664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2666" n="HIAT:w" s="T663">dĭzeŋ</ts>
                  <nts id="Seg_2667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2669" n="HIAT:w" s="T664">koʔbsaŋ</ts>
                  <nts id="Seg_2670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2672" n="HIAT:w" s="T665">nuʔməluʔbiʔi</ts>
                  <nts id="Seg_2673" n="HIAT:ip">,</nts>
                  <nts id="Seg_2674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2676" n="HIAT:w" s="T666">šobiʔi</ts>
                  <nts id="Seg_2677" n="HIAT:ip">.</nts>
                  <nts id="Seg_2678" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T673" id="Seg_2680" n="HIAT:u" s="T667">
                  <nts id="Seg_2681" n="HIAT:ip">"</nts>
                  <ts e="T668" id="Seg_2683" n="HIAT:w" s="T667">Dʼagarluʔpi</ts>
                  <nts id="Seg_2684" n="HIAT:ip">,</nts>
                  <nts id="Seg_2685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2687" n="HIAT:w" s="T668">ulardə</ts>
                  <nts id="Seg_2688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2690" n="HIAT:w" s="T669">bar</ts>
                  <nts id="Seg_2691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2692" n="HIAT:ip">(</nts>
                  <ts e="T671" id="Seg_2694" n="HIAT:w" s="T670">убе-</ts>
                  <nts id="Seg_2695" n="HIAT:ip">)</nts>
                  <nts id="Seg_2696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2698" n="HIAT:w" s="T671">nuʔməluʔbiʔi</ts>
                  <nts id="Seg_2699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2701" n="HIAT:w" s="T672">nükem</ts>
                  <nts id="Seg_2702" n="HIAT:ip">"</nts>
                  <nts id="Seg_2703" n="HIAT:ip">.</nts>
                  <nts id="Seg_2704" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T680" id="Seg_2706" n="HIAT:u" s="T673">
                  <ts e="T674" id="Seg_2708" n="HIAT:w" s="T673">Dĭgəttə</ts>
                  <nts id="Seg_2709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2711" n="HIAT:w" s="T674">dĭ</ts>
                  <nts id="Seg_2712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2714" n="HIAT:w" s="T675">bar</ts>
                  <nts id="Seg_2715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2717" n="HIAT:w" s="T676">saʔməluʔbi</ts>
                  <nts id="Seg_2718" n="HIAT:ip">,</nts>
                  <nts id="Seg_2719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2721" n="HIAT:w" s="T677">Kodur</ts>
                  <nts id="Seg_2722" n="HIAT:ip">,</nts>
                  <nts id="Seg_2723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2725" n="HIAT:w" s="T678">dʼorbi</ts>
                  <nts id="Seg_2726" n="HIAT:ip">,</nts>
                  <nts id="Seg_2727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2729" n="HIAT:w" s="T679">dʼorbi</ts>
                  <nts id="Seg_2730" n="HIAT:ip">.</nts>
                  <nts id="Seg_2731" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T686" id="Seg_2733" n="HIAT:u" s="T680">
                  <ts e="T681" id="Seg_2735" n="HIAT:w" s="T680">Dĭ</ts>
                  <nts id="Seg_2736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2738" n="HIAT:w" s="T681">büzʼe</ts>
                  <nts id="Seg_2739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2741" n="HIAT:w" s="T682">măndə:</ts>
                  <nts id="Seg_2742" n="HIAT:ip">"</nts>
                  <nts id="Seg_2743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2745" n="HIAT:w" s="T683">It</ts>
                  <nts id="Seg_2746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2748" n="HIAT:w" s="T684">onʼiʔ</ts>
                  <nts id="Seg_2749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2751" n="HIAT:w" s="T685">koʔbdo</ts>
                  <nts id="Seg_2752" n="HIAT:ip">"</nts>
                  <nts id="Seg_2753" n="HIAT:ip">.</nts>
                  <nts id="Seg_2754" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T693" id="Seg_2756" n="HIAT:u" s="T686">
                  <nts id="Seg_2757" n="HIAT:ip">"</nts>
                  <ts e="T686.tx.1" id="Seg_2759" n="HIAT:w" s="T686">Na</ts>
                  <nts id="Seg_2760" n="HIAT:ip">_</nts>
                  <ts e="T687" id="Seg_2762" n="HIAT:w" s="T686.tx.1">što</ts>
                  <nts id="Seg_2763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2765" n="HIAT:w" s="T687">măna</ts>
                  <nts id="Seg_2766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2768" n="HIAT:w" s="T688">onʼiʔ</ts>
                  <nts id="Seg_2769" n="HIAT:ip">,</nts>
                  <nts id="Seg_2770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2772" n="HIAT:w" s="T689">măn</ts>
                  <nts id="Seg_2773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2775" n="HIAT:w" s="T690">nükem</ts>
                  <nts id="Seg_2776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2778" n="HIAT:w" s="T691">lutʼšə</ts>
                  <nts id="Seg_2779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2781" n="HIAT:w" s="T692">ibi</ts>
                  <nts id="Seg_2782" n="HIAT:ip">"</nts>
                  <nts id="Seg_2783" n="HIAT:ip">.</nts>
                  <nts id="Seg_2784" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T696" id="Seg_2786" n="HIAT:u" s="T693">
                  <nts id="Seg_2787" n="HIAT:ip">"</nts>
                  <ts e="T694" id="Seg_2789" n="HIAT:w" s="T693">No</ts>
                  <nts id="Seg_2790" n="HIAT:ip">,</nts>
                  <nts id="Seg_2791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2793" n="HIAT:w" s="T694">it</ts>
                  <nts id="Seg_2794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2796" n="HIAT:w" s="T695">šide</ts>
                  <nts id="Seg_2797" n="HIAT:ip">!</nts>
                  <nts id="Seg_2798" n="HIAT:ip">"</nts>
                  <nts id="Seg_2799" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T699" id="Seg_2801" n="HIAT:u" s="T696">
                  <ts e="T697" id="Seg_2803" n="HIAT:w" s="T696">Dĭgəttə</ts>
                  <nts id="Seg_2804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2806" n="HIAT:w" s="T697">dĭ</ts>
                  <nts id="Seg_2807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2809" n="HIAT:w" s="T698">uʔbdəbi</ts>
                  <nts id="Seg_2810" n="HIAT:ip">.</nts>
                  <nts id="Seg_2811" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T702" id="Seg_2813" n="HIAT:u" s="T699">
                  <ts e="T700" id="Seg_2815" n="HIAT:w" s="T699">Bazoʔ</ts>
                  <nts id="Seg_2816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2818" n="HIAT:w" s="T700">băzəjdəbi</ts>
                  <nts id="Seg_2819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2821" n="HIAT:w" s="T701">kadəldə</ts>
                  <nts id="Seg_2822" n="HIAT:ip">.</nts>
                  <nts id="Seg_2823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T708" id="Seg_2825" n="HIAT:u" s="T702">
                  <ts e="T703" id="Seg_2827" n="HIAT:w" s="T702">I</ts>
                  <nts id="Seg_2828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2830" n="HIAT:w" s="T703">šide</ts>
                  <nts id="Seg_2831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2833" n="HIAT:w" s="T704">koʔbdo</ts>
                  <nts id="Seg_2834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2836" n="HIAT:w" s="T705">ibi</ts>
                  <nts id="Seg_2837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2839" n="HIAT:w" s="T706">i</ts>
                  <nts id="Seg_2840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2842" n="HIAT:w" s="T707">kambi</ts>
                  <nts id="Seg_2843" n="HIAT:ip">.</nts>
                  <nts id="Seg_2844" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T714" id="Seg_2846" n="HIAT:u" s="T708">
                  <ts e="T709" id="Seg_2848" n="HIAT:w" s="T708">Dĭgəttə</ts>
                  <nts id="Seg_2849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2851" n="HIAT:w" s="T709">šonəga</ts>
                  <nts id="Seg_2852" n="HIAT:ip">,</nts>
                  <nts id="Seg_2853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2855" n="HIAT:w" s="T710">šonəga</ts>
                  <nts id="Seg_2856" n="HIAT:ip">,</nts>
                  <nts id="Seg_2857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2859" n="HIAT:w" s="T711">dĭn</ts>
                  <nts id="Seg_2860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2862" n="HIAT:w" s="T712">ularəʔi</ts>
                  <nts id="Seg_2863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2865" n="HIAT:w" s="T713">mĭlleʔbəʔjə</ts>
                  <nts id="Seg_2866" n="HIAT:ip">.</nts>
                  <nts id="Seg_2867" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T720" id="Seg_2869" n="HIAT:u" s="T714">
                  <ts e="T715" id="Seg_2871" n="HIAT:w" s="T714">Dĭgəttə</ts>
                  <nts id="Seg_2872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2874" n="HIAT:w" s="T715">šobi</ts>
                  <nts id="Seg_2875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2877" n="HIAT:w" s="T716">pastuxtə:</ts>
                  <nts id="Seg_2878" n="HIAT:ip">"</nts>
                  <nts id="Seg_2879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2881" n="HIAT:w" s="T717">Dön</ts>
                  <nts id="Seg_2882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2884" n="HIAT:w" s="T718">măn</ts>
                  <nts id="Seg_2885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2887" n="HIAT:w" s="T719">ular</ts>
                  <nts id="Seg_2888" n="HIAT:ip">!</nts>
                  <nts id="Seg_2889" n="HIAT:ip">"</nts>
                  <nts id="Seg_2890" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T721" id="Seg_2892" n="HIAT:u" s="T720">
                  <nts id="Seg_2893" n="HIAT:ip">(</nts>
                  <ts e="T721" id="Seg_2895" n="HIAT:w" s="T720">Da</ts>
                  <nts id="Seg_2896" n="HIAT:ip">)</nts>
                  <nts id="Seg_2897" n="HIAT:ip">.</nts>
                  <nts id="Seg_2898" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T722" id="Seg_2900" n="HIAT:u" s="T721">
                  <nts id="Seg_2901" n="HIAT:ip">(</nts>
                  <nts id="Seg_2902" n="HIAT:ip">(</nts>
                  <ats e="T722" id="Seg_2903" n="HIAT:non-pho" s="T721">BRK</ats>
                  <nts id="Seg_2904" n="HIAT:ip">)</nts>
                  <nts id="Seg_2905" n="HIAT:ip">)</nts>
                  <nts id="Seg_2906" n="HIAT:ip">.</nts>
                  <nts id="Seg_2907" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T731" id="Seg_2909" n="HIAT:u" s="T722">
                  <ts e="T723" id="Seg_2911" n="HIAT:w" s="T722">Onʼiʔ</ts>
                  <nts id="Seg_2912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2913" n="HIAT:ip">(</nts>
                  <ts e="T724" id="Seg_2915" n="HIAT:w" s="T723">m-</ts>
                  <nts id="Seg_2916" n="HIAT:ip">)</nts>
                  <nts id="Seg_2917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2919" n="HIAT:w" s="T724">amnəlbi</ts>
                  <nts id="Seg_2920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2922" n="HIAT:w" s="T725">bünə</ts>
                  <nts id="Seg_2923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2924" n="HIAT:ip">(</nts>
                  <ts e="T727" id="Seg_2926" n="HIAT:w" s="T726">tol-</ts>
                  <nts id="Seg_2927" n="HIAT:ip">)</nts>
                  <nts id="Seg_2928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2930" n="HIAT:w" s="T727">toʔndə</ts>
                  <nts id="Seg_2931" n="HIAT:ip">,</nts>
                  <nts id="Seg_2932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2934" n="HIAT:w" s="T728">onʼiʔ</ts>
                  <nts id="Seg_2935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2937" n="HIAT:w" s="T729">amnəlbəbi</ts>
                  <nts id="Seg_2938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2940" n="HIAT:w" s="T730">dʼijegən</ts>
                  <nts id="Seg_2941" n="HIAT:ip">.</nts>
                  <nts id="Seg_2942" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T735" id="Seg_2944" n="HIAT:u" s="T731">
                  <ts e="T732" id="Seg_2946" n="HIAT:w" s="T731">Dĭgəttə</ts>
                  <nts id="Seg_2947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2949" n="HIAT:w" s="T732">măllaʔbə:</ts>
                  <nts id="Seg_2950" n="HIAT:ip">"</nts>
                  <nts id="Seg_2951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2953" n="HIAT:w" s="T733">Măn</ts>
                  <nts id="Seg_2954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2956" n="HIAT:w" s="T734">ular</ts>
                  <nts id="Seg_2957" n="HIAT:ip">!</nts>
                  <nts id="Seg_2958" n="HIAT:ip">"</nts>
                  <nts id="Seg_2959" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T737" id="Seg_2961" n="HIAT:u" s="T735">
                  <nts id="Seg_2962" n="HIAT:ip">"</nts>
                  <ts e="T736" id="Seg_2964" n="HIAT:w" s="T735">No</ts>
                  <nts id="Seg_2965" n="HIAT:ip">,</nts>
                  <nts id="Seg_2966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2968" n="HIAT:w" s="T736">surardə</ts>
                  <nts id="Seg_2969" n="HIAT:ip">.</nts>
                  <nts id="Seg_2970" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T740" id="Seg_2972" n="HIAT:u" s="T737">
                  <nts id="Seg_2973" n="HIAT:ip">(</nts>
                  <ts e="T738" id="Seg_2975" n="HIAT:w" s="T737">У</ts>
                  <nts id="Seg_2976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2978" n="HIAT:w" s="T738">одинного</ts>
                  <nts id="Seg_2979" n="HIAT:ip">)</nts>
                  <nts id="Seg_2980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2982" n="HIAT:w" s="T739">surardə</ts>
                  <nts id="Seg_2983" n="HIAT:ip">"</nts>
                  <nts id="Seg_2984" n="HIAT:ip">.</nts>
                  <nts id="Seg_2985" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T742" id="Seg_2987" n="HIAT:u" s="T740">
                  <ts e="T741" id="Seg_2989" n="HIAT:w" s="T740">Dĭ</ts>
                  <nts id="Seg_2990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2992" n="HIAT:w" s="T741">surarluʔpi</ts>
                  <nts id="Seg_2993" n="HIAT:ip">.</nts>
                  <nts id="Seg_2994" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T744" id="Seg_2996" n="HIAT:u" s="T742">
                  <nts id="Seg_2997" n="HIAT:ip">"</nts>
                  <ts e="T743" id="Seg_2999" n="HIAT:w" s="T742">Kodurən</ts>
                  <nts id="Seg_3000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_3002" n="HIAT:w" s="T743">ularzaŋdə</ts>
                  <nts id="Seg_3003" n="HIAT:ip">!</nts>
                  <nts id="Seg_3004" n="HIAT:ip">"</nts>
                  <nts id="Seg_3005" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T748" id="Seg_3007" n="HIAT:u" s="T744">
                  <nts id="Seg_3008" n="HIAT:ip">"</nts>
                  <ts e="T745" id="Seg_3010" n="HIAT:w" s="T744">Dĭgəttə</ts>
                  <nts id="Seg_3011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_3013" n="HIAT:w" s="T745">dʼijegən</ts>
                  <nts id="Seg_3014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_3016" n="HIAT:w" s="T746">kuzanə</ts>
                  <nts id="Seg_3017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_3019" n="HIAT:w" s="T747">surardə</ts>
                  <nts id="Seg_3020" n="HIAT:ip">!</nts>
                  <nts id="Seg_3021" n="HIAT:ip">"</nts>
                  <nts id="Seg_3022" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T753" id="Seg_3024" n="HIAT:u" s="T748">
                  <ts e="T749" id="Seg_3026" n="HIAT:w" s="T748">Surarbi</ts>
                  <nts id="Seg_3027" n="HIAT:ip">,</nts>
                  <nts id="Seg_3028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_3030" n="HIAT:w" s="T749">dĭ</ts>
                  <nts id="Seg_3031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_3033" n="HIAT:w" s="T750">măndə:</ts>
                  <nts id="Seg_3034" n="HIAT:ip">"</nts>
                  <nts id="Seg_3035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_3037" n="HIAT:w" s="T751">Kodurən</ts>
                  <nts id="Seg_3038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_3040" n="HIAT:w" s="T752">ulardə</ts>
                  <nts id="Seg_3041" n="HIAT:ip">!</nts>
                  <nts id="Seg_3042" n="HIAT:ip">"</nts>
                  <nts id="Seg_3043" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T757" id="Seg_3045" n="HIAT:u" s="T753">
                  <ts e="T754" id="Seg_3047" n="HIAT:w" s="T753">Dĭgəttə</ts>
                  <nts id="Seg_3048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3049" n="HIAT:ip">(</nts>
                  <ts e="T755" id="Seg_3051" n="HIAT:w" s="T754">ibɨ</ts>
                  <nts id="Seg_3052" n="HIAT:ip">)</nts>
                  <nts id="Seg_3053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_3055" n="HIAT:w" s="T755">ibi</ts>
                  <nts id="Seg_3056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_3058" n="HIAT:w" s="T756">ulardə</ts>
                  <nts id="Seg_3059" n="HIAT:ip">.</nts>
                  <nts id="Seg_3060" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T758" id="Seg_3062" n="HIAT:u" s="T757">
                  <nts id="Seg_3063" n="HIAT:ip">(</nts>
                  <nts id="Seg_3064" n="HIAT:ip">(</nts>
                  <ats e="T758" id="Seg_3065" n="HIAT:non-pho" s="T757">BRK</ats>
                  <nts id="Seg_3066" n="HIAT:ip">)</nts>
                  <nts id="Seg_3067" n="HIAT:ip">)</nts>
                  <nts id="Seg_3068" n="HIAT:ip">.</nts>
                  <nts id="Seg_3069" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T765" id="Seg_3071" n="HIAT:u" s="T758">
                  <ts e="T759" id="Seg_3073" n="HIAT:w" s="T758">Bazoʔ</ts>
                  <nts id="Seg_3074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3075" n="HIAT:ip">(</nts>
                  <ts e="T760" id="Seg_3077" n="HIAT:w" s="T759">s-</ts>
                  <nts id="Seg_3078" n="HIAT:ip">)</nts>
                  <nts id="Seg_3079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_3081" n="HIAT:w" s="T760">šobi</ts>
                  <nts id="Seg_3082" n="HIAT:ip">,</nts>
                  <nts id="Seg_3083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_3085" n="HIAT:w" s="T761">kuza</ts>
                  <nts id="Seg_3086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_3088" n="HIAT:w" s="T762">ineʔi</ts>
                  <nts id="Seg_3089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3090" n="HIAT:ip">(</nts>
                  <ts e="T764" id="Seg_3092" n="HIAT:w" s="T763">mĭn-</ts>
                  <nts id="Seg_3093" n="HIAT:ip">)</nts>
                  <nts id="Seg_3094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_3096" n="HIAT:w" s="T764">amnolaʔbə</ts>
                  <nts id="Seg_3097" n="HIAT:ip">.</nts>
                  <nts id="Seg_3098" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T767" id="Seg_3100" n="HIAT:u" s="T765">
                  <nts id="Seg_3101" n="HIAT:ip">"</nts>
                  <ts e="T766" id="Seg_3103" n="HIAT:w" s="T765">Măn</ts>
                  <nts id="Seg_3104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_3106" n="HIAT:w" s="T766">ineʔi</ts>
                  <nts id="Seg_3107" n="HIAT:ip">!</nts>
                  <nts id="Seg_3108" n="HIAT:ip">"</nts>
                  <nts id="Seg_3109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T773" id="Seg_3111" n="HIAT:u" s="T767">
                  <ts e="T768" id="Seg_3113" n="HIAT:w" s="T767">A</ts>
                  <nts id="Seg_3114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_3116" n="HIAT:w" s="T768">dĭ</ts>
                  <nts id="Seg_3117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_3119" n="HIAT:w" s="T769">măndə:</ts>
                  <nts id="Seg_3120" n="HIAT:ip">"</nts>
                  <nts id="Seg_3121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_3123" n="HIAT:w" s="T770">Dʼok</ts>
                  <nts id="Seg_3124" n="HIAT:ip">,</nts>
                  <nts id="Seg_3125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_3127" n="HIAT:w" s="T771">măn</ts>
                  <nts id="Seg_3128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_3130" n="HIAT:w" s="T772">ineʔi</ts>
                  <nts id="Seg_3131" n="HIAT:ip">!</nts>
                  <nts id="Seg_3132" n="HIAT:ip">"</nts>
                  <nts id="Seg_3133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T777" id="Seg_3135" n="HIAT:u" s="T773">
                  <nts id="Seg_3136" n="HIAT:ip">"</nts>
                  <ts e="T774" id="Seg_3138" n="HIAT:w" s="T773">No</ts>
                  <nts id="Seg_3139" n="HIAT:ip">,</nts>
                  <nts id="Seg_3140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_3142" n="HIAT:w" s="T774">surardə</ts>
                  <nts id="Seg_3143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_3145" n="HIAT:w" s="T775">bünə</ts>
                  <nts id="Seg_3146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_3148" n="HIAT:w" s="T776">kuzam</ts>
                  <nts id="Seg_3149" n="HIAT:ip">!</nts>
                  <nts id="Seg_3150" n="HIAT:ip">"</nts>
                  <nts id="Seg_3151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T781" id="Seg_3153" n="HIAT:u" s="T777">
                  <ts e="T778" id="Seg_3155" n="HIAT:w" s="T777">Dĭ</ts>
                  <nts id="Seg_3156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_3158" n="HIAT:w" s="T778">surarbi:</ts>
                  <nts id="Seg_3159" n="HIAT:ip">"</nts>
                  <nts id="Seg_3160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_3162" n="HIAT:w" s="T779">Kodurən</ts>
                  <nts id="Seg_3163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_3165" n="HIAT:w" s="T780">ineʔi</ts>
                  <nts id="Seg_3166" n="HIAT:ip">!</nts>
                  <nts id="Seg_3167" n="HIAT:ip">"</nts>
                  <nts id="Seg_3168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T785" id="Seg_3170" n="HIAT:u" s="T781">
                  <nts id="Seg_3171" n="HIAT:ip">"</nts>
                  <ts e="T782" id="Seg_3173" n="HIAT:w" s="T781">Dĭgəttə</ts>
                  <nts id="Seg_3174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_3176" n="HIAT:w" s="T782">dʼijen</ts>
                  <nts id="Seg_3177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_3179" n="HIAT:w" s="T783">kuzanə</ts>
                  <nts id="Seg_3180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_3182" n="HIAT:w" s="T784">surardə</ts>
                  <nts id="Seg_3183" n="HIAT:ip">!</nts>
                  <nts id="Seg_3184" n="HIAT:ip">"</nts>
                  <nts id="Seg_3185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T789" id="Seg_3187" n="HIAT:u" s="T785">
                  <ts e="T786" id="Seg_3189" n="HIAT:w" s="T785">Dĭ</ts>
                  <nts id="Seg_3190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_3192" n="HIAT:w" s="T786">surarbi:</ts>
                  <nts id="Seg_3193" n="HIAT:ip">"</nts>
                  <nts id="Seg_3194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_3196" n="HIAT:w" s="T787">Kodurən</ts>
                  <nts id="Seg_3197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_3199" n="HIAT:w" s="T788">ineʔi</ts>
                  <nts id="Seg_3200" n="HIAT:ip">!</nts>
                  <nts id="Seg_3201" n="HIAT:ip">"</nts>
                  <nts id="Seg_3202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T792" id="Seg_3204" n="HIAT:u" s="T789">
                  <ts e="T790" id="Seg_3206" n="HIAT:w" s="T789">Dĭ</ts>
                  <nts id="Seg_3207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3208" n="HIAT:ip">(</nts>
                  <ts e="T791" id="Seg_3210" n="HIAT:w" s="T790">bar</ts>
                  <nts id="Seg_3211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_3213" n="HIAT:w" s="T791">öʔluʔpi</ts>
                  <nts id="Seg_3214" n="HIAT:ip">)</nts>
                  <nts id="Seg_3215" n="HIAT:ip">.</nts>
                  <nts id="Seg_3216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T795" id="Seg_3218" n="HIAT:u" s="T792">
                  <ts e="T793" id="Seg_3220" n="HIAT:w" s="T792">Dĭgəttə</ts>
                  <nts id="Seg_3221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_3223" n="HIAT:w" s="T793">tüžöjəʔi</ts>
                  <nts id="Seg_3224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_3226" n="HIAT:w" s="T794">mĭlleʔbəʔjə</ts>
                  <nts id="Seg_3227" n="HIAT:ip">.</nts>
                  <nts id="Seg_3228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T801" id="Seg_3230" n="HIAT:u" s="T795">
                  <nts id="Seg_3231" n="HIAT:ip">(</nts>
                  <ts e="T796" id="Seg_3233" n="HIAT:w" s="T795">I</ts>
                  <nts id="Seg_3234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_3236" n="HIAT:w" s="T796">tĭn</ts>
                  <nts id="Seg_3237" n="HIAT:ip">)</nts>
                  <nts id="Seg_3238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_3240" n="HIAT:w" s="T797">tože</ts>
                  <nts id="Seg_3241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_3243" n="HIAT:w" s="T798">kuza</ts>
                  <nts id="Seg_3244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_3246" n="HIAT:w" s="T799">măndəlaʔbə</ts>
                  <nts id="Seg_3247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_3249" n="HIAT:w" s="T800">tüžöjəʔi</ts>
                  <nts id="Seg_3250" n="HIAT:ip">.</nts>
                  <nts id="Seg_3251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T805" id="Seg_3253" n="HIAT:u" s="T801">
                  <ts e="T802" id="Seg_3255" n="HIAT:w" s="T801">Dĭ</ts>
                  <nts id="Seg_3256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_3258" n="HIAT:w" s="T802">šobi:</ts>
                  <nts id="Seg_3259" n="HIAT:ip">"</nts>
                  <nts id="Seg_3260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_3262" n="HIAT:w" s="T803">Măn</ts>
                  <nts id="Seg_3263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_3265" n="HIAT:w" s="T804">tüžöjəʔjə</ts>
                  <nts id="Seg_3266" n="HIAT:ip">!</nts>
                  <nts id="Seg_3267" n="HIAT:ip">"</nts>
                  <nts id="Seg_3268" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T808" id="Seg_3270" n="HIAT:u" s="T805">
                  <nts id="Seg_3271" n="HIAT:ip">"</nts>
                  <ts e="T806" id="Seg_3273" n="HIAT:w" s="T805">Dʼok</ts>
                  <nts id="Seg_3274" n="HIAT:ip">,</nts>
                  <nts id="Seg_3275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_3277" n="HIAT:w" s="T806">ej</ts>
                  <nts id="Seg_3278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_3280" n="HIAT:w" s="T807">tăn</ts>
                  <nts id="Seg_3281" n="HIAT:ip">!</nts>
                  <nts id="Seg_3282" n="HIAT:ip">"</nts>
                  <nts id="Seg_3283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T812" id="Seg_3285" n="HIAT:u" s="T808">
                  <nts id="Seg_3286" n="HIAT:ip">"</nts>
                  <ts e="T809" id="Seg_3288" n="HIAT:w" s="T808">No</ts>
                  <nts id="Seg_3289" n="HIAT:ip">,</nts>
                  <nts id="Seg_3290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_3292" n="HIAT:w" s="T809">suraraʔ</ts>
                  <nts id="Seg_3293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_3295" n="HIAT:w" s="T810">bügən</ts>
                  <nts id="Seg_3296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_3298" n="HIAT:w" s="T811">kuza</ts>
                  <nts id="Seg_3299" n="HIAT:ip">!</nts>
                  <nts id="Seg_3300" n="HIAT:ip">"</nts>
                  <nts id="Seg_3301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T816" id="Seg_3303" n="HIAT:u" s="T812">
                  <ts e="T813" id="Seg_3305" n="HIAT:w" s="T812">Dĭgəttə</ts>
                  <nts id="Seg_3306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_3308" n="HIAT:w" s="T813">dʼijegən</ts>
                  <nts id="Seg_3309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_3311" n="HIAT:w" s="T814">dĭ</ts>
                  <nts id="Seg_3312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_3314" n="HIAT:w" s="T815">surarbi</ts>
                  <nts id="Seg_3315" n="HIAT:ip">.</nts>
                  <nts id="Seg_3316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T818" id="Seg_3318" n="HIAT:u" s="T816">
                  <nts id="Seg_3319" n="HIAT:ip">"</nts>
                  <ts e="T817" id="Seg_3321" n="HIAT:w" s="T816">Šindən</ts>
                  <nts id="Seg_3322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_3324" n="HIAT:w" s="T817">tüžöjdə</ts>
                  <nts id="Seg_3325" n="HIAT:ip">?</nts>
                  <nts id="Seg_3326" n="HIAT:ip">"</nts>
                  <nts id="Seg_3327" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T823" id="Seg_3329" n="HIAT:u" s="T818">
                  <nts id="Seg_3330" n="HIAT:ip">(</nts>
                  <ts e="T819" id="Seg_3332" n="HIAT:w" s="T818">Dĭ=</ts>
                  <nts id="Seg_3333" n="HIAT:ip">)</nts>
                  <nts id="Seg_3334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_3336" n="HIAT:w" s="T819">Dĭ</ts>
                  <nts id="Seg_3337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_3339" n="HIAT:w" s="T820">mămbi:</ts>
                  <nts id="Seg_3340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3341" n="HIAT:ip">"</nts>
                  <ts e="T822" id="Seg_3343" n="HIAT:w" s="T821">Kodurən</ts>
                  <nts id="Seg_3344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_3346" n="HIAT:w" s="T822">tüžöjdə</ts>
                  <nts id="Seg_3347" n="HIAT:ip">!</nts>
                  <nts id="Seg_3348" n="HIAT:ip">"</nts>
                  <nts id="Seg_3349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T828" id="Seg_3351" n="HIAT:u" s="T823">
                  <ts e="T824" id="Seg_3353" n="HIAT:w" s="T823">I</ts>
                  <nts id="Seg_3354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3356" n="HIAT:w" s="T824">dʼijegən</ts>
                  <nts id="Seg_3357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_3359" n="HIAT:w" s="T825">surarbi:</ts>
                  <nts id="Seg_3360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3361" n="HIAT:ip">"</nts>
                  <ts e="T827" id="Seg_3363" n="HIAT:w" s="T826">Šindən</ts>
                  <nts id="Seg_3364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3365" n="HIAT:ip">(</nts>
                  <ts e="T828" id="Seg_3367" n="HIAT:w" s="T827">tüžöjdə</ts>
                  <nts id="Seg_3368" n="HIAT:ip">)</nts>
                  <nts id="Seg_3369" n="HIAT:ip">?</nts>
                  <nts id="Seg_3370" n="HIAT:ip">"</nts>
                  <nts id="Seg_3371" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T830" id="Seg_3373" n="HIAT:u" s="T828">
                  <nts id="Seg_3374" n="HIAT:ip">"</nts>
                  <ts e="T829" id="Seg_3376" n="HIAT:w" s="T828">Kodurən</ts>
                  <nts id="Seg_3377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_3379" n="HIAT:w" s="T829">tüžöjdə</ts>
                  <nts id="Seg_3380" n="HIAT:ip">!</nts>
                  <nts id="Seg_3381" n="HIAT:ip">"</nts>
                  <nts id="Seg_3382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T835" id="Seg_3384" n="HIAT:u" s="T830">
                  <ts e="T831" id="Seg_3386" n="HIAT:w" s="T830">Dĭgəttə</ts>
                  <nts id="Seg_3387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3389" n="HIAT:w" s="T831">dĭ</ts>
                  <nts id="Seg_3390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_3392" n="HIAT:w" s="T832">davaj</ts>
                  <nts id="Seg_3393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_3395" n="HIAT:w" s="T833">nüjnəsʼtə</ts>
                  <nts id="Seg_3396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_3398" n="HIAT:w" s="T834">nüjnə</ts>
                  <nts id="Seg_3399" n="HIAT:ip">.</nts>
                  <nts id="Seg_3400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T840" id="Seg_3402" n="HIAT:u" s="T835">
                  <nts id="Seg_3403" n="HIAT:ip">"</nts>
                  <ts e="T836" id="Seg_3405" n="HIAT:w" s="T835">Măn</ts>
                  <nts id="Seg_3406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_3408" n="HIAT:w" s="T836">lăpatka</ts>
                  <nts id="Seg_3409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_3411" n="HIAT:w" s="T837">сухой</ts>
                  <nts id="Seg_3412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3413" n="HIAT:ip">(</nts>
                  <ts e="T839" id="Seg_3415" n="HIAT:w" s="T838">ibim-</ts>
                  <nts id="Seg_3416" n="HIAT:ip">)</nts>
                  <nts id="Seg_3417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_3419" n="HIAT:w" s="T839">ibiem</ts>
                  <nts id="Seg_3420" n="HIAT:ip">.</nts>
                  <nts id="Seg_3421" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T843" id="Seg_3423" n="HIAT:u" s="T840">
                  <nts id="Seg_3424" n="HIAT:ip">(</nts>
                  <ts e="T841" id="Seg_3426" n="HIAT:w" s="T840">Ula-</ts>
                  <nts id="Seg_3427" n="HIAT:ip">)</nts>
                  <nts id="Seg_3428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_3430" n="HIAT:w" s="T841">Ular</ts>
                  <nts id="Seg_3431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_3433" n="HIAT:w" s="T842">ibiem</ts>
                  <nts id="Seg_3434" n="HIAT:ip">.</nts>
                  <nts id="Seg_3435" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T847" id="Seg_3437" n="HIAT:u" s="T843">
                  <ts e="T844" id="Seg_3439" n="HIAT:w" s="T843">Ulardə</ts>
                  <nts id="Seg_3440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_3442" n="HIAT:w" s="T844">šide</ts>
                  <nts id="Seg_3443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_3445" n="HIAT:w" s="T845">ular</ts>
                  <nts id="Seg_3446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_3448" n="HIAT:w" s="T846">ibiem</ts>
                  <nts id="Seg_3449" n="HIAT:ip">.</nts>
                  <nts id="Seg_3450" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T854" id="Seg_3452" n="HIAT:u" s="T847">
                  <ts e="T848" id="Seg_3454" n="HIAT:w" s="T847">Šide</ts>
                  <nts id="Seg_3455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_3457" n="HIAT:w" s="T848">ular</ts>
                  <nts id="Seg_3458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_3460" n="HIAT:w" s="T849">ibiem</ts>
                  <nts id="Seg_3461" n="HIAT:ip">,</nts>
                  <nts id="Seg_3462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_3464" n="HIAT:w" s="T850">kambiam</ts>
                  <nts id="Seg_3465" n="HIAT:ip">,</nts>
                  <nts id="Seg_3466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_3468" n="HIAT:w" s="T851">nüke</ts>
                  <nts id="Seg_3469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_3471" n="HIAT:w" s="T852">dʼügən</ts>
                  <nts id="Seg_3472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_3474" n="HIAT:w" s="T853">ibiem</ts>
                  <nts id="Seg_3475" n="HIAT:ip">.</nts>
                  <nts id="Seg_3476" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T863" id="Seg_3478" n="HIAT:u" s="T854">
                  <ts e="T855" id="Seg_3480" n="HIAT:w" s="T854">I</ts>
                  <nts id="Seg_3481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_3483" n="HIAT:w" s="T855">šide</ts>
                  <nts id="Seg_3484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_3486" n="HIAT:w" s="T856">ular</ts>
                  <nts id="Seg_3487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_3489" n="HIAT:w" s="T857">i</ts>
                  <nts id="Seg_3490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T859" id="Seg_3492" n="HIAT:w" s="T858">nüke</ts>
                  <nts id="Seg_3493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_3495" n="HIAT:w" s="T859">dagaj</ts>
                  <nts id="Seg_3496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3497" n="HIAT:ip">(</nts>
                  <ts e="T861" id="Seg_3499" n="HIAT:w" s="T860">š-</ts>
                  <nts id="Seg_3500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_3502" n="HIAT:w" s="T861">sĭjdə=</ts>
                  <nts id="Seg_3503" n="HIAT:ip">)</nts>
                  <nts id="Seg_3504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_3506" n="HIAT:w" s="T862">sĭjdə</ts>
                  <nts id="Seg_3507" n="HIAT:ip">"</nts>
                  <nts id="Seg_3508" n="HIAT:ip">.</nts>
                  <nts id="Seg_3509" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T864" id="Seg_3511" n="HIAT:u" s="T863">
                  <nts id="Seg_3512" n="HIAT:ip">(</nts>
                  <nts id="Seg_3513" n="HIAT:ip">(</nts>
                  <ats e="T864" id="Seg_3514" n="HIAT:non-pho" s="T863">BRK</ats>
                  <nts id="Seg_3515" n="HIAT:ip">)</nts>
                  <nts id="Seg_3516" n="HIAT:ip">)</nts>
                  <nts id="Seg_3517" n="HIAT:ip">.</nts>
                  <nts id="Seg_3518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T868" id="Seg_3520" n="HIAT:u" s="T864">
                  <ts e="T865" id="Seg_3522" n="HIAT:w" s="T864">Dagaj</ts>
                  <nts id="Seg_3523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_3525" n="HIAT:w" s="T865">sĭjdə</ts>
                  <nts id="Seg_3526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3527" n="HIAT:ip">(</nts>
                  <ts e="T867" id="Seg_3529" n="HIAT:w" s="T866">mün-</ts>
                  <nts id="Seg_3530" n="HIAT:ip">)</nts>
                  <nts id="Seg_3531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_3533" n="HIAT:w" s="T867">müʔluʔpi</ts>
                  <nts id="Seg_3534" n="HIAT:ip">.</nts>
                  <nts id="Seg_3535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T878" id="Seg_3537" n="HIAT:u" s="T868">
                  <ts e="T869" id="Seg_3539" n="HIAT:w" s="T868">A</ts>
                  <nts id="Seg_3540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_3542" n="HIAT:w" s="T869">bostə</ts>
                  <nts id="Seg_3543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_3545" n="HIAT:w" s="T870">šobi:</ts>
                  <nts id="Seg_3546" n="HIAT:ip">"</nts>
                  <nts id="Seg_3547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_3549" n="HIAT:w" s="T871">Măn</ts>
                  <nts id="Seg_3550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_3552" n="HIAT:w" s="T872">dĭn</ts>
                  <nts id="Seg_3553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_3555" n="HIAT:w" s="T873">nükem</ts>
                  <nts id="Seg_3556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_3558" n="HIAT:w" s="T874">ige</ts>
                  <nts id="Seg_3559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_3561" n="HIAT:w" s="T875">i</ts>
                  <nts id="Seg_3562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_3564" n="HIAT:w" s="T876">šide</ts>
                  <nts id="Seg_3565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_3567" n="HIAT:w" s="T877">ular</ts>
                  <nts id="Seg_3568" n="HIAT:ip">"</nts>
                  <nts id="Seg_3569" n="HIAT:ip">.</nts>
                  <nts id="Seg_3570" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T884" id="Seg_3572" n="HIAT:u" s="T878">
                  <ts e="T879" id="Seg_3574" n="HIAT:w" s="T878">I</ts>
                  <nts id="Seg_3575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T880" id="Seg_3577" n="HIAT:w" s="T879">šide</ts>
                  <nts id="Seg_3578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T881" id="Seg_3580" n="HIAT:w" s="T880">koʔbsaŋ</ts>
                  <nts id="Seg_3581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_3583" n="HIAT:w" s="T881">i</ts>
                  <nts id="Seg_3584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_3586" n="HIAT:w" s="T882">büzʼe</ts>
                  <nts id="Seg_3587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_3589" n="HIAT:w" s="T883">amnolaʔbə</ts>
                  <nts id="Seg_3590" n="HIAT:ip">.</nts>
                  <nts id="Seg_3591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T888" id="Seg_3593" n="HIAT:u" s="T884">
                  <ts e="T885" id="Seg_3595" n="HIAT:w" s="T884">Dĭ</ts>
                  <nts id="Seg_3596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_3598" n="HIAT:w" s="T885">bar</ts>
                  <nts id="Seg_3599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887" id="Seg_3601" n="HIAT:w" s="T886">büzʼe</ts>
                  <nts id="Seg_3602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T888" id="Seg_3604" n="HIAT:w" s="T887">dʼăbaktəria</ts>
                  <nts id="Seg_3605" n="HIAT:ip">…</nts>
                  <nts id="Seg_3606" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T888" id="Seg_3607" n="sc" s="T0">
               <ts e="T2" id="Seg_3609" n="e" s="T0">((DMG)) (imsobie) </ts>
               <ts e="T3" id="Seg_3611" n="e" s="T2">tarzittə. </ts>
               <ts e="T4" id="Seg_3613" n="e" s="T3">Măn </ts>
               <ts e="T5" id="Seg_3615" n="e" s="T4">dĭʔnə </ts>
               <ts e="T6" id="Seg_3617" n="e" s="T5">embiem </ts>
               <ts e="T7" id="Seg_3619" n="e" s="T6">beškeʔi, </ts>
               <ts e="T8" id="Seg_3621" n="e" s="T7">kajak </ts>
               <ts e="T9" id="Seg_3623" n="e" s="T8">mĭbiem, </ts>
               <ts e="T10" id="Seg_3625" n="e" s="T9">aktʼa </ts>
               <ts e="T11" id="Seg_3627" n="e" s="T10">mĭbiem. </ts>
               <ts e="T12" id="Seg_3629" n="e" s="T11">Oʔb, </ts>
               <ts e="T13" id="Seg_3631" n="e" s="T12">šide, </ts>
               <ts e="T14" id="Seg_3633" n="e" s="T13">nagur, </ts>
               <ts e="T15" id="Seg_3635" n="e" s="T14">teʔtə. </ts>
               <ts e="T16" id="Seg_3637" n="e" s="T15">Dĭzeŋ </ts>
               <ts e="T17" id="Seg_3639" n="e" s="T16">tura </ts>
               <ts e="T18" id="Seg_3641" n="e" s="T17">ibiʔi, </ts>
               <ts e="T19" id="Seg_3643" n="e" s="T18">šindidə </ts>
               <ts e="T20" id="Seg_3645" n="e" s="T19">bar </ts>
               <ts e="T21" id="Seg_3647" n="e" s="T20">sadarlaʔbə. </ts>
               <ts e="T22" id="Seg_3649" n="e" s="T21">I </ts>
               <ts e="T23" id="Seg_3651" n="e" s="T22">aktʼa </ts>
               <ts e="T24" id="Seg_3653" n="e" s="T23">ej </ts>
               <ts e="T25" id="Seg_3655" n="e" s="T24">kabarlia. </ts>
               <ts e="T26" id="Seg_3657" n="e" s="T25">((BRK)). </ts>
               <ts e="T27" id="Seg_3659" n="e" s="T26">Dĭzeŋ </ts>
               <ts e="T28" id="Seg_3661" n="e" s="T27">dĭ </ts>
               <ts e="T29" id="Seg_3663" n="e" s="T28">(turab-) </ts>
               <ts e="T30" id="Seg_3665" n="e" s="T29">tura </ts>
               <ts e="T31" id="Seg_3667" n="e" s="T30">sadarla </ts>
               <ts e="T32" id="Seg_3669" n="e" s="T31">ibiʔi. </ts>
               <ts e="T33" id="Seg_3671" n="e" s="T32">((BRK)). </ts>
               <ts e="T34" id="Seg_3673" n="e" s="T33">A </ts>
               <ts e="T35" id="Seg_3675" n="e" s="T34">girgit </ts>
               <ts e="T36" id="Seg_3677" n="e" s="T35">kuzagən </ts>
               <ts e="T37" id="Seg_3679" n="e" s="T36">naga </ts>
               <ts e="T38" id="Seg_3681" n="e" s="T37">(iza-) </ts>
               <ts e="T39" id="Seg_3683" n="e" s="T38">(isaʔtə) </ts>
               <ts e="T40" id="Seg_3685" n="e" s="T39">aktʼa </ts>
               <ts e="T41" id="Seg_3687" n="e" s="T40">mĭzittə. </ts>
               <ts e="T42" id="Seg_3689" n="e" s="T41">Tĭn </ts>
               <ts e="T43" id="Seg_3691" n="e" s="T42">tüžöjdə </ts>
               <ts e="T44" id="Seg_3693" n="e" s="T43">il </ts>
               <ts e="T45" id="Seg_3695" n="e" s="T44">ibiʔi. </ts>
               <ts e="T46" id="Seg_3697" n="e" s="T45">Ĭmbi </ts>
               <ts e="T47" id="Seg_3699" n="e" s="T46">ige </ts>
               <ts e="T48" id="Seg_3701" n="e" s="T47">bar </ts>
               <ts e="T49" id="Seg_3703" n="e" s="T48">iluʔpiʔi </ts>
               <ts e="T50" id="Seg_3705" n="e" s="T49">i </ts>
               <ts e="T51" id="Seg_3707" n="e" s="T50">mĭbiʔi </ts>
               <ts e="T52" id="Seg_3709" n="e" s="T51">aktʼa </ts>
               <ts e="T53" id="Seg_3711" n="e" s="T52">dĭbər. </ts>
               <ts e="T54" id="Seg_3713" n="e" s="T53">((BRK)). </ts>
               <ts e="T55" id="Seg_3715" n="e" s="T54">Măn </ts>
               <ts e="T56" id="Seg_3717" n="e" s="T55">tuganbə </ts>
               <ts e="T892" id="Seg_3719" n="e" s="T56">kalla </ts>
               <ts e="T57" id="Seg_3721" n="e" s="T892">dʼürbi </ts>
               <ts e="T58" id="Seg_3723" n="e" s="T57">gorăttə </ts>
               <ts e="T59" id="Seg_3725" n="e" s="T58">i </ts>
               <ts e="T60" id="Seg_3727" n="e" s="T59">dĭn </ts>
               <ts e="T61" id="Seg_3729" n="e" s="T60">mašina </ts>
               <ts e="T62" id="Seg_3731" n="e" s="T61">iləj, </ts>
               <ts e="T63" id="Seg_3733" n="e" s="T62">oldʼa, </ts>
               <ts e="T64" id="Seg_3735" n="e" s="T63">bazəsʼtə </ts>
               <ts e="T65" id="Seg_3737" n="e" s="T64">(kujnek=) </ts>
               <ts e="T66" id="Seg_3739" n="e" s="T65">kujnegəʔi, </ts>
               <ts e="T67" id="Seg_3741" n="e" s="T66">piʔmeʔi, </ts>
               <ts e="T68" id="Seg_3743" n="e" s="T67">bar </ts>
               <ts e="T69" id="Seg_3745" n="e" s="T68">(ombi-) </ts>
               <ts e="T70" id="Seg_3747" n="e" s="T69">ĭmbi </ts>
               <ts e="T71" id="Seg_3749" n="e" s="T70">ige </ts>
               <ts e="T72" id="Seg_3751" n="e" s="T71">bazəsʼtə. </ts>
               <ts e="T73" id="Seg_3753" n="e" s="T72">((BRK)). </ts>
               <ts e="T74" id="Seg_3755" n="e" s="T73">Tibi </ts>
               <ts e="T75" id="Seg_3757" n="e" s="T74">i </ts>
               <ts e="T76" id="Seg_3759" n="e" s="T75">ne </ts>
               <ts e="T77" id="Seg_3761" n="e" s="T76">amnobiʔi </ts>
               <ts e="T78" id="Seg_3763" n="e" s="T77">ugandə </ts>
               <ts e="T79" id="Seg_3765" n="e" s="T78">jakše. </ts>
               <ts e="T80" id="Seg_3767" n="e" s="T79">Ajirbiʔi </ts>
               <ts e="T81" id="Seg_3769" n="e" s="T80">(один=) </ts>
               <ts e="T83" id="Seg_3771" n="e" s="T81">onʼiʔ… </ts>
               <ts e="T84" id="Seg_3773" n="e" s="T83">Onʼiʔ </ts>
               <ts e="T85" id="Seg_3775" n="e" s="T84">onʼiʔtə </ts>
               <ts e="T86" id="Seg_3777" n="e" s="T85">ajirbiʔi </ts>
               <ts e="T87" id="Seg_3779" n="e" s="T86">bar. </ts>
               <ts e="T88" id="Seg_3781" n="e" s="T87">(Kamrolaʔtəʔi). </ts>
               <ts e="T89" id="Seg_3783" n="e" s="T88">Panarlaʔbəʔjə. </ts>
               <ts e="T90" id="Seg_3785" n="e" s="T89">I </ts>
               <ts e="T91" id="Seg_3787" n="e" s="T90">dĭzeŋ </ts>
               <ts e="T92" id="Seg_3789" n="e" s="T91">esseŋdə </ts>
               <ts e="T93" id="Seg_3791" n="e" s="T92">ibiʔi, </ts>
               <ts e="T94" id="Seg_3793" n="e" s="T93">ugandə </ts>
               <ts e="T95" id="Seg_3795" n="e" s="T94">esseŋdə </ts>
               <ts e="T96" id="Seg_3797" n="e" s="T95">jakše. </ts>
               <ts e="T97" id="Seg_3799" n="e" s="T96">Ĭmbi </ts>
               <ts e="T98" id="Seg_3801" n="e" s="T97">măllal, </ts>
               <ts e="T99" id="Seg_3803" n="e" s="T98">dĭzeŋ </ts>
               <ts e="T100" id="Seg_3805" n="e" s="T99">nʼilgölaʔbəʔjə. </ts>
               <ts e="T101" id="Seg_3807" n="e" s="T100">((BRK)). </ts>
               <ts e="T102" id="Seg_3809" n="e" s="T101">A </ts>
               <ts e="T103" id="Seg_3811" n="e" s="T102">miʔ </ts>
               <ts e="T104" id="Seg_3813" n="e" s="T103">esseŋ </ts>
               <ts e="T105" id="Seg_3815" n="e" s="T104">ugandə </ts>
               <ts e="T106" id="Seg_3817" n="e" s="T105">ej </ts>
               <ts e="T107" id="Seg_3819" n="e" s="T106">jakše. </ts>
               <ts e="T108" id="Seg_3821" n="e" s="T107">Dʼabrolaʔbəʔjə, </ts>
               <ts e="T109" id="Seg_3823" n="e" s="T108">kudonzlaʔbəʔjə, </ts>
               <ts e="T110" id="Seg_3825" n="e" s="T109">ej </ts>
               <ts e="T111" id="Seg_3827" n="e" s="T110">nʼilgölaʔbəʔjə. </ts>
               <ts e="T112" id="Seg_3829" n="e" s="T111">Măn </ts>
               <ts e="T113" id="Seg_3831" n="e" s="T112">dĭzem </ts>
               <ts e="T114" id="Seg_3833" n="e" s="T113">bar </ts>
               <ts e="T115" id="Seg_3835" n="e" s="T114">münörlaʔbəm. </ts>
               <ts e="T116" id="Seg_3837" n="e" s="T115">((BRK)). </ts>
               <ts e="T117" id="Seg_3839" n="e" s="T116">Esseŋbə </ts>
               <ts e="T118" id="Seg_3841" n="e" s="T117">tüšəlliem </ts>
               <ts e="T119" id="Seg_3843" n="e" s="T118">sazən </ts>
               <ts e="T120" id="Seg_3845" n="e" s="T119">pʼaŋdəsʼtə. </ts>
               <ts e="T121" id="Seg_3847" n="e" s="T120">((BRK)). </ts>
               <ts e="T122" id="Seg_3849" n="e" s="T121">Nuzaŋ </ts>
               <ts e="T123" id="Seg_3851" n="e" s="T122">amnobiʔi. </ts>
               <ts e="T124" id="Seg_3853" n="e" s="T123">Šindi </ts>
               <ts e="T125" id="Seg_3855" n="e" s="T124">ugandə </ts>
               <ts e="T126" id="Seg_3857" n="e" s="T125">aktʼa </ts>
               <ts e="T127" id="Seg_3859" n="e" s="T126">iʔgö, </ts>
               <ts e="T128" id="Seg_3861" n="e" s="T127">a </ts>
               <ts e="T129" id="Seg_3863" n="e" s="T128">šindin </ts>
               <ts e="T130" id="Seg_3865" n="e" s="T129">naga. </ts>
               <ts e="T131" id="Seg_3867" n="e" s="T130">Dĭ </ts>
               <ts e="T132" id="Seg_3869" n="e" s="T131">ara </ts>
               <ts e="T133" id="Seg_3871" n="e" s="T132">bĭtlie. </ts>
               <ts e="T134" id="Seg_3873" n="e" s="T133">Bar </ts>
               <ts e="T135" id="Seg_3875" n="e" s="T134">aktʼa </ts>
               <ts e="T136" id="Seg_3877" n="e" s="T135">bĭtluʔləj. </ts>
               <ts e="T137" id="Seg_3879" n="e" s="T136">Dĭgəttə </ts>
               <ts e="T138" id="Seg_3881" n="e" s="T137">ĭmbidə </ts>
               <ts e="T139" id="Seg_3883" n="e" s="T138">naga. </ts>
               <ts e="T140" id="Seg_3885" n="e" s="T139">Togonoria, </ts>
               <ts e="T141" id="Seg_3887" n="e" s="T140">togonoria, </ts>
               <ts e="T142" id="Seg_3889" n="e" s="T141">bar </ts>
               <ts e="T143" id="Seg_3891" n="e" s="T142">bĭʔluʔləj. </ts>
               <ts e="T144" id="Seg_3893" n="e" s="T143">((BRK)). </ts>
               <ts e="T145" id="Seg_3895" n="e" s="T144">((BRK)). </ts>
               <ts e="T146" id="Seg_3897" n="e" s="T145">Sibirʼgən </ts>
               <ts e="T147" id="Seg_3899" n="e" s="T146">ugandə </ts>
               <ts e="T148" id="Seg_3901" n="e" s="T147">iʔgö </ts>
               <ts e="T149" id="Seg_3903" n="e" s="T148">nuzaŋ </ts>
               <ts e="T150" id="Seg_3905" n="e" s="T149">amnobiʔi. </ts>
               <ts e="T151" id="Seg_3907" n="e" s="T150">Dĭgəttə </ts>
               <ts e="T152" id="Seg_3909" n="e" s="T151">sĭre </ts>
               <ts e="T153" id="Seg_3911" n="e" s="T152">pa </ts>
               <ts e="T154" id="Seg_3913" n="e" s="T153">özerluʔpi. </ts>
               <ts e="T155" id="Seg_3915" n="e" s="T154">Dĭzeŋ </ts>
               <ts e="T156" id="Seg_3917" n="e" s="T155">mălliaʔi:" </ts>
               <ts e="T157" id="Seg_3919" n="e" s="T156">Nu, </ts>
               <ts e="T158" id="Seg_3921" n="e" s="T157">(kazaŋ-) </ts>
               <ts e="T159" id="Seg_3923" n="e" s="T158">kazak </ts>
               <ts e="T160" id="Seg_3925" n="e" s="T159">il </ts>
               <ts e="T161" id="Seg_3927" n="e" s="T160">šoləʔjə, </ts>
               <ts e="T162" id="Seg_3929" n="e" s="T161">miʔnʼibeʔ </ts>
               <ts e="T163" id="Seg_3931" n="e" s="T162">ej </ts>
               <ts e="T164" id="Seg_3933" n="e" s="T163">jakše </ts>
               <ts e="T165" id="Seg_3935" n="e" s="T164">moləj". </ts>
               <ts e="T166" id="Seg_3937" n="e" s="T165">Dĭgəttə </ts>
               <ts e="T167" id="Seg_3939" n="e" s="T166">labazəʔi </ts>
               <ts e="T168" id="Seg_3941" n="e" s="T167">abiʔi. </ts>
               <ts e="T169" id="Seg_3943" n="e" s="T168">Dĭbər </ts>
               <ts e="T170" id="Seg_3945" n="e" s="T169">piʔi </ts>
               <ts e="T171" id="Seg_3947" n="e" s="T170">tažerbiʔi, </ts>
               <ts e="T172" id="Seg_3949" n="e" s="T171">i </ts>
               <ts e="T173" id="Seg_3951" n="e" s="T172">baltu </ts>
               <ts e="T174" id="Seg_3953" n="e" s="T173">ibiʔi. </ts>
               <ts e="T175" id="Seg_3955" n="e" s="T174">Jaʔpiʔi, </ts>
               <ts e="T176" id="Seg_3957" n="e" s="T175">i </ts>
               <ts e="T177" id="Seg_3959" n="e" s="T176">dĭ </ts>
               <ts e="T178" id="Seg_3961" n="e" s="T177">saʔməluʔpi, </ts>
               <ts e="T179" id="Seg_3963" n="e" s="T178">(dĭzem) </ts>
               <ts e="T180" id="Seg_3965" n="e" s="T179">dĭzeŋ </ts>
               <ts e="T181" id="Seg_3967" n="e" s="T180">bar </ts>
               <ts e="T182" id="Seg_3969" n="e" s="T181">dĭn </ts>
               <ts e="T183" id="Seg_3971" n="e" s="T182">kübiʔi. </ts>
               <ts e="T184" id="Seg_3973" n="e" s="T183">((BRK)). </ts>
               <ts e="T891" id="Seg_3975" n="e" s="T184">Lutʼšə </ts>
               <ts e="T185" id="Seg_3977" n="e" s="T891">(măna=) </ts>
               <ts e="T186" id="Seg_3979" n="e" s="T185">măn </ts>
               <ts e="T187" id="Seg_3981" n="e" s="T186">bɨ </ts>
               <ts e="T188" id="Seg_3983" n="e" s="T187">kübiem, </ts>
               <ts e="T189" id="Seg_3985" n="e" s="T188">чем </ts>
               <ts e="T190" id="Seg_3987" n="e" s="T189">esseŋbə </ts>
               <ts e="T192" id="Seg_3989" n="e" s="T190">dĭrgit. </ts>
               <ts e="T193" id="Seg_3991" n="e" s="T192">Ej </ts>
               <ts e="T194" id="Seg_3993" n="e" s="T193">jakšeʔi, </ts>
               <ts e="T195" id="Seg_3995" n="e" s="T194">bar </ts>
               <ts e="T196" id="Seg_3997" n="e" s="T195">tajirlaʔbəʔjə. </ts>
               <ts e="T197" id="Seg_3999" n="e" s="T196">Ilzi </ts>
               <ts e="T198" id="Seg_4001" n="e" s="T197">dʼabrolaʔbəʔjə. </ts>
               <ts e="T199" id="Seg_4003" n="e" s="T198">((BRK)). </ts>
               <ts e="T200" id="Seg_4005" n="e" s="T199">Bar </ts>
               <ts e="T201" id="Seg_4007" n="e" s="T200">ara </ts>
               <ts e="T202" id="Seg_4009" n="e" s="T201">bĭtleʔbəʔjə, </ts>
               <ts e="T203" id="Seg_4011" n="e" s="T202">ažnə </ts>
               <ts e="T204" id="Seg_4013" n="e" s="T203">simazi </ts>
               <ts e="T205" id="Seg_4015" n="e" s="T204">ej </ts>
               <ts e="T206" id="Seg_4017" n="e" s="T205">molia </ts>
               <ts e="T207" id="Seg_4019" n="e" s="T206">mănderzittə </ts>
               <ts e="T208" id="Seg_4021" n="e" s="T207">dĭzeŋdə. </ts>
               <ts e="T209" id="Seg_4023" n="e" s="T208">((BRK)). </ts>
               <ts e="T210" id="Seg_4025" n="e" s="T209">Onʼiʔ </ts>
               <ts e="T211" id="Seg_4027" n="e" s="T210">nen </ts>
               <ts e="T212" id="Seg_4029" n="e" s="T211">nagur </ts>
               <ts e="T213" id="Seg_4031" n="e" s="T212">nʼi </ts>
               <ts e="T214" id="Seg_4033" n="e" s="T213">ibi, </ts>
               <ts e="T215" id="Seg_4035" n="e" s="T214">šide </ts>
               <ts e="T216" id="Seg_4037" n="e" s="T215">jakše, </ts>
               <ts e="T217" id="Seg_4039" n="e" s="T216">a </ts>
               <ts e="T218" id="Seg_4041" n="e" s="T217">onʼiʔ </ts>
               <ts e="T219" id="Seg_4043" n="e" s="T218">ej </ts>
               <ts e="T220" id="Seg_4045" n="e" s="T219">jakše. </ts>
               <ts e="T221" id="Seg_4047" n="e" s="T220">Bar </ts>
               <ts e="T222" id="Seg_4049" n="e" s="T221">dĭm </ts>
               <ts e="T223" id="Seg_4051" n="e" s="T222">münörbiʔi. </ts>
               <ts e="T224" id="Seg_4053" n="e" s="T223">Üjüzi </ts>
               <ts e="T225" id="Seg_4055" n="e" s="T224">bar, </ts>
               <ts e="T226" id="Seg_4057" n="e" s="T225">i </ts>
               <ts e="T227" id="Seg_4059" n="e" s="T226">(muz-) </ts>
               <ts e="T228" id="Seg_4061" n="e" s="T227">muzuruksi, </ts>
               <ts e="T229" id="Seg_4063" n="e" s="T228">i </ts>
               <ts e="T230" id="Seg_4065" n="e" s="T229">pazi </ts>
               <ts e="T231" id="Seg_4067" n="e" s="T230">bar </ts>
               <ts e="T232" id="Seg_4069" n="e" s="T231">(kuno- </ts>
               <ts e="T233" id="Seg_4071" n="e" s="T232">ku-) </ts>
               <ts e="T234" id="Seg_4073" n="e" s="T233">toʔnarbiʔi. </ts>
               <ts e="T235" id="Seg_4075" n="e" s="T234">A </ts>
               <ts e="T236" id="Seg_4077" n="e" s="T235">măn </ts>
               <ts e="T237" id="Seg_4079" n="e" s="T236">(u) </ts>
               <ts e="T239" id="Seg_4081" n="e" s="T237">mămbiam… </ts>
               <ts e="T240" id="Seg_4083" n="e" s="T239">((BRK)). </ts>
               <ts e="T241" id="Seg_4085" n="e" s="T240">A </ts>
               <ts e="T242" id="Seg_4087" n="e" s="T241">măn </ts>
               <ts e="T243" id="Seg_4089" n="e" s="T242">(u) </ts>
               <ts e="T244" id="Seg_4091" n="e" s="T243">mămbiam:" </ts>
               <ts e="T245" id="Seg_4093" n="e" s="T244">Iʔ </ts>
               <ts e="T246" id="Seg_4095" n="e" s="T245">üžüŋgeʔ, </ts>
               <ts e="T247" id="Seg_4097" n="e" s="T246">možet </ts>
               <ts e="T248" id="Seg_4099" n="e" s="T247">özerləj. </ts>
               <ts e="T249" id="Seg_4101" n="e" s="T248">Sagəštə </ts>
               <ts e="T250" id="Seg_4103" n="e" s="T249">iʔgö </ts>
               <ts e="T251" id="Seg_4105" n="e" s="T250">moləj, </ts>
               <ts e="T252" id="Seg_4107" n="e" s="T251">urgo </ts>
               <ts e="T253" id="Seg_4109" n="e" s="T252">moləj. </ts>
               <ts e="T254" id="Seg_4111" n="e" s="T253">Možet </ts>
               <ts e="T255" id="Seg_4113" n="e" s="T254">bar </ts>
               <ts e="T256" id="Seg_4115" n="e" s="T255">(baruʔləj) </ts>
               <ts e="T257" id="Seg_4117" n="e" s="T256">dĭgəttə </ts>
               <ts e="T258" id="Seg_4119" n="e" s="T257">jakše </ts>
               <ts e="T259" id="Seg_4121" n="e" s="T258">moləj". </ts>
               <ts e="T260" id="Seg_4123" n="e" s="T259">((BRK)). </ts>
               <ts e="T261" id="Seg_4125" n="e" s="T260">A </ts>
               <ts e="T262" id="Seg_4127" n="e" s="T261">dĭ </ts>
               <ts e="T263" id="Seg_4129" n="e" s="T262">girgit </ts>
               <ts e="T264" id="Seg_4131" n="e" s="T263">üdʼüge </ts>
               <ts e="T265" id="Seg_4133" n="e" s="T264">ibi, </ts>
               <ts e="T266" id="Seg_4135" n="e" s="T265">i </ts>
               <ts e="T267" id="Seg_4137" n="e" s="T266">urgo </ts>
               <ts e="T268" id="Seg_4139" n="e" s="T267">(du-) </ts>
               <ts e="T269" id="Seg_4141" n="e" s="T268">dĭrgit </ts>
               <ts e="T270" id="Seg_4143" n="e" s="T269">že. </ts>
               <ts e="T271" id="Seg_4145" n="e" s="T270">Ĭmbidə </ts>
               <ts e="T272" id="Seg_4147" n="e" s="T271">naga </ts>
               <ts e="T273" id="Seg_4149" n="e" s="T272">jakše. </ts>
               <ts e="T274" id="Seg_4151" n="e" s="T273">((BRK)). </ts>
               <ts e="T275" id="Seg_4153" n="e" s="T274">Dĭ </ts>
               <ts e="T276" id="Seg_4155" n="e" s="T275">armijanə </ts>
               <ts e="T277" id="Seg_4157" n="e" s="T276">kambi. </ts>
               <ts e="T278" id="Seg_4159" n="e" s="T277">Vsʼo_ravno </ts>
               <ts e="T279" id="Seg_4161" n="e" s="T278">šobi, </ts>
               <ts e="T280" id="Seg_4163" n="e" s="T279">gibər </ts>
               <ts e="T281" id="Seg_4165" n="e" s="T280">togonorzittə </ts>
               <ts e="T282" id="Seg_4167" n="e" s="T281">kaləj, </ts>
               <ts e="T283" id="Seg_4169" n="e" s="T282">dĭm </ts>
               <ts e="T284" id="Seg_4171" n="e" s="T283">bar </ts>
               <ts e="T285" id="Seg_4173" n="e" s="T284">sürerleʔbəʔjə. </ts>
               <ts e="T286" id="Seg_4175" n="e" s="T285">((BRK)). </ts>
               <ts e="T287" id="Seg_4177" n="e" s="T286">Onʼiʔ </ts>
               <ts e="T288" id="Seg_4179" n="e" s="T287">nüke </ts>
               <ts e="T289" id="Seg_4181" n="e" s="T288">nʼit </ts>
               <ts e="T290" id="Seg_4183" n="e" s="T289">ibi, </ts>
               <ts e="T291" id="Seg_4185" n="e" s="T290">bar </ts>
               <ts e="T292" id="Seg_4187" n="e" s="T291">jezerik. </ts>
               <ts e="T293" id="Seg_4189" n="e" s="T292">"Tăn </ts>
               <ts e="T294" id="Seg_4191" n="e" s="T293">ĭmbi </ts>
               <ts e="T295" id="Seg_4193" n="e" s="T294">măna </ts>
               <ts e="T296" id="Seg_4195" n="e" s="T295">ej </ts>
               <ts e="T297" id="Seg_4197" n="e" s="T296">tüšəlbiel?" </ts>
               <ts e="T298" id="Seg_4199" n="e" s="T297">A </ts>
               <ts e="T299" id="Seg_4201" n="e" s="T298">dĭ </ts>
               <ts e="T300" id="Seg_4203" n="e" s="T299">mămbi: </ts>
               <ts e="T301" id="Seg_4205" n="e" s="T300">"Măn </ts>
               <ts e="T302" id="Seg_4207" n="e" s="T301">tüšəlbiem </ts>
               <ts e="T303" id="Seg_4209" n="e" s="T302">tănan, </ts>
               <ts e="T304" id="Seg_4211" n="e" s="T303">ĭmbi </ts>
               <ts e="T305" id="Seg_4213" n="e" s="T304">kereʔ </ts>
               <ts e="T306" id="Seg_4215" n="e" s="T305">(tăn=) </ts>
               <ts e="T307" id="Seg_4217" n="e" s="T306">tănan? </ts>
               <ts e="T308" id="Seg_4219" n="e" s="T307">Tăn </ts>
               <ts e="T309" id="Seg_4221" n="e" s="T308">диплом </ts>
               <ts e="T310" id="Seg_4223" n="e" s="T309">ibiel. </ts>
               <ts e="T311" id="Seg_4225" n="e" s="T310">A </ts>
               <ts e="T312" id="Seg_4227" n="e" s="T311">(dĭ </ts>
               <ts e="T313" id="Seg_4229" n="e" s="T312">bar </ts>
               <ts e="T314" id="Seg_4231" n="e" s="T313">muzu-) </ts>
               <ts e="T315" id="Seg_4233" n="e" s="T314">Ara </ts>
               <ts e="T316" id="Seg_4235" n="e" s="T315">bĭtliel!" </ts>
               <ts e="T317" id="Seg_4237" n="e" s="T316">A </ts>
               <ts e="T318" id="Seg_4239" n="e" s="T317">dĭ </ts>
               <ts e="T319" id="Seg_4241" n="e" s="T318">bar </ts>
               <ts e="T321" id="Seg_4243" n="e" s="T319">muzuruksi_da </ts>
               <ts e="T322" id="Seg_4245" n="e" s="T321">iat </ts>
               <ts e="T323" id="Seg_4247" n="e" s="T322">xatʼel </ts>
               <ts e="T324" id="Seg_4249" n="e" s="T323">münörzittə. </ts>
               <ts e="T325" id="Seg_4251" n="e" s="T324">A </ts>
               <ts e="T326" id="Seg_4253" n="e" s="T325">il </ts>
               <ts e="T327" id="Seg_4255" n="e" s="T326">ej </ts>
               <ts e="T328" id="Seg_4257" n="e" s="T327">mĭbiʔi. </ts>
               <ts e="T329" id="Seg_4259" n="e" s="T328">((BRK)). </ts>
               <ts e="T330" id="Seg_4261" n="e" s="T329">Măn </ts>
               <ts e="T331" id="Seg_4263" n="e" s="T330">kagam </ts>
               <ts e="T332" id="Seg_4265" n="e" s="T331">văjnagən </ts>
               <ts e="T333" id="Seg_4267" n="e" s="T332">(ibiel=) </ts>
               <ts e="T334" id="Seg_4269" n="e" s="T333">ibie. </ts>
               <ts e="T335" id="Seg_4271" n="e" s="T334">I </ts>
               <ts e="T336" id="Seg_4273" n="e" s="T335">dĭm </ts>
               <ts e="T337" id="Seg_4275" n="e" s="T336">ibiʔi </ts>
               <ts e="T338" id="Seg_4277" n="e" s="T337">plendə. </ts>
               <ts e="T339" id="Seg_4279" n="e" s="T338">I </ts>
               <ts e="T340" id="Seg_4281" n="e" s="T339">pravăj </ts>
               <ts e="T341" id="Seg_4283" n="e" s="T340">udat </ts>
               <ts e="T342" id="Seg_4285" n="e" s="T341">müjötsi </ts>
               <ts e="T343" id="Seg_4287" n="e" s="T342">(xat-) </ts>
               <ts e="T344" id="Seg_4289" n="e" s="T343">dʼagarzittə </ts>
               <ts e="T345" id="Seg_4291" n="e" s="T344">xatʼeli. </ts>
               <ts e="T346" id="Seg_4293" n="e" s="T345">A </ts>
               <ts e="T347" id="Seg_4295" n="e" s="T346">dĭ </ts>
               <ts e="T348" id="Seg_4297" n="e" s="T347">bar </ts>
               <ts e="T349" id="Seg_4299" n="e" s="T348">dʼorbi, </ts>
               <ts e="T350" id="Seg_4301" n="e" s="T349">šide </ts>
               <ts e="T351" id="Seg_4303" n="e" s="T350">müjə </ts>
               <ts e="T352" id="Seg_4305" n="e" s="T351">(m-) </ts>
               <ts e="T353" id="Seg_4307" n="e" s="T352">(maːtəbiʔi) </ts>
               <ts e="T354" id="Seg_4309" n="e" s="T353">(ему) </ts>
               <ts e="T355" id="Seg_4311" n="e" s="T354">dĭʔnə. </ts>
               <ts e="T356" id="Seg_4313" n="e" s="T355">((BRK)). </ts>
               <ts e="T357" id="Seg_4315" n="e" s="T356">Măn </ts>
               <ts e="T358" id="Seg_4317" n="e" s="T357">(u) </ts>
               <ts e="T359" id="Seg_4319" n="e" s="T358">kagam </ts>
               <ts e="T360" id="Seg_4321" n="e" s="T359">nagur </ts>
               <ts e="T361" id="Seg_4323" n="e" s="T360">koʔbdo </ts>
               <ts e="T362" id="Seg_4325" n="e" s="T361">i </ts>
               <ts e="T363" id="Seg_4327" n="e" s="T362">nagur </ts>
               <ts e="T364" id="Seg_4329" n="e" s="T363">nʼi. </ts>
               <ts e="T365" id="Seg_4331" n="e" s="T364">((BRK)). </ts>
               <ts e="T366" id="Seg_4333" n="e" s="T365">Nörbəzittə </ts>
               <ts e="T367" id="Seg_4335" n="e" s="T366">tănan </ts>
               <ts e="T368" id="Seg_4337" n="e" s="T367">(sĭre) </ts>
               <ts e="T369" id="Seg_4339" n="e" s="T368">sĭre </ts>
               <ts e="T370" id="Seg_4341" n="e" s="T369">buga? </ts>
               <ts e="T371" id="Seg_4343" n="e" s="T370">((BRK)). </ts>
               <ts e="T372" id="Seg_4345" n="e" s="T371">Tăn </ts>
               <ts e="T373" id="Seg_4347" n="e" s="T372">nörbit, </ts>
               <ts e="T374" id="Seg_4349" n="e" s="T373">măn </ts>
               <ts e="T375" id="Seg_4351" n="e" s="T374">nörbit. </ts>
               <ts e="T376" id="Seg_4353" n="e" s="T375">Mănzittə </ts>
               <ts e="T377" id="Seg_4355" n="e" s="T376">tănan </ts>
               <ts e="T378" id="Seg_4357" n="e" s="T377">kömə </ts>
               <ts e="T379" id="Seg_4359" n="e" s="T378">buga? </ts>
               <ts e="T380" id="Seg_4361" n="e" s="T379">((BRK)). </ts>
               <ts e="T381" id="Seg_4363" n="e" s="T380">Tăn </ts>
               <ts e="T382" id="Seg_4365" n="e" s="T381">nörbit, </ts>
               <ts e="T383" id="Seg_4367" n="e" s="T382">măn </ts>
               <ts e="T384" id="Seg_4369" n="e" s="T383">nörbit. </ts>
               <ts e="T385" id="Seg_4371" n="e" s="T384">Mănzittə </ts>
               <ts e="T386" id="Seg_4373" n="e" s="T385">tănan </ts>
               <ts e="T387" id="Seg_4375" n="e" s="T386">sagər </ts>
               <ts e="T388" id="Seg_4377" n="e" s="T387">buga? </ts>
               <ts e="T389" id="Seg_4379" n="e" s="T388">Kodur </ts>
               <ts e="T390" id="Seg_4381" n="e" s="T389">Kodur, </ts>
               <ts e="T391" id="Seg_4383" n="e" s="T390">kömə </ts>
               <ts e="T392" id="Seg_4385" n="e" s="T391">părga. </ts>
               <ts e="T393" id="Seg_4387" n="e" s="T392">Šobi, </ts>
               <ts e="T394" id="Seg_4389" n="e" s="T393">dön </ts>
               <ts e="T395" id="Seg_4391" n="e" s="T394">il </ts>
               <ts e="T396" id="Seg_4393" n="e" s="T395">amnobiʔi, </ts>
               <ts e="T397" id="Seg_4395" n="e" s="T396">dĭ </ts>
               <ts e="T398" id="Seg_4397" n="e" s="T397">lăpatka </ts>
               <ts e="T399" id="Seg_4399" n="e" s="T398">kubi, </ts>
               <ts e="T400" id="Seg_4401" n="e" s="T399">uja </ts>
               <ts e="T401" id="Seg_4403" n="e" s="T400">naga. </ts>
               <ts e="T402" id="Seg_4405" n="e" s="T401">Dĭgəttə </ts>
               <ts e="T403" id="Seg_4407" n="e" s="T402">kambi, </ts>
               <ts e="T404" id="Seg_4409" n="e" s="T403">kambi. </ts>
               <ts e="T405" id="Seg_4411" n="e" s="T404">Nu, </ts>
               <ts e="T406" id="Seg_4413" n="e" s="T405">dĭn </ts>
               <ts e="T407" id="Seg_4415" n="e" s="T406">onʼiʔ </ts>
               <ts e="T408" id="Seg_4417" n="e" s="T407">kuza </ts>
               <ts e="T409" id="Seg_4419" n="e" s="T408">amnobi </ts>
               <ts e="T410" id="Seg_4421" n="e" s="T409">neziʔ. </ts>
               <ts e="T411" id="Seg_4423" n="e" s="T410">Dĭ </ts>
               <ts e="T412" id="Seg_4425" n="e" s="T411">šobi, </ts>
               <ts e="T413" id="Seg_4427" n="e" s="T412">(dĭzeŋ=) </ts>
               <ts e="T414" id="Seg_4429" n="e" s="T413">dĭzeŋdə </ts>
               <ts e="T415" id="Seg_4431" n="e" s="T414">ular </ts>
               <ts e="T416" id="Seg_4433" n="e" s="T415">ibi </ts>
               <ts e="T417" id="Seg_4435" n="e" s="T416">onʼiʔ. </ts>
               <ts e="T418" id="Seg_4437" n="e" s="T417">Dĭ </ts>
               <ts e="T419" id="Seg_4439" n="e" s="T418">măndə: </ts>
               <ts e="T420" id="Seg_4441" n="e" s="T419">"Nada </ts>
               <ts e="T421" id="Seg_4443" n="e" s="T420">segi </ts>
               <ts e="T422" id="Seg_4445" n="e" s="T421">bü </ts>
               <ts e="T423" id="Seg_4447" n="e" s="T422">mĭnzərzittə, </ts>
               <ts e="T424" id="Seg_4449" n="e" s="T423">kuzam </ts>
               <ts e="T425" id="Seg_4451" n="e" s="T424">(bĭz-) </ts>
               <ts e="T426" id="Seg_4453" n="e" s="T425">bădəsʼtə". </ts>
               <ts e="T427" id="Seg_4455" n="e" s="T426">A </ts>
               <ts e="T428" id="Seg_4457" n="e" s="T427">dĭ </ts>
               <ts e="T429" id="Seg_4459" n="e" s="T428">măndə: </ts>
               <ts e="T430" id="Seg_4461" n="e" s="T429">"Iʔ </ts>
               <ts e="T431" id="Seg_4463" n="e" s="T430">mĭnzəreʔ, </ts>
               <ts e="T432" id="Seg_4465" n="e" s="T431">măn </ts>
               <ts e="T433" id="Seg_4467" n="e" s="T432">ujam </ts>
               <ts e="T434" id="Seg_4469" n="e" s="T433">ige". </ts>
               <ts e="T435" id="Seg_4471" n="e" s="T434">Dĭgəttə </ts>
               <ts e="T436" id="Seg_4473" n="e" s="T435">aspaʔ </ts>
               <ts e="T437" id="Seg_4475" n="e" s="T436">edəbiʔi. </ts>
               <ts e="T438" id="Seg_4477" n="e" s="T437">Tus </ts>
               <ts e="T439" id="Seg_4479" n="e" s="T438">embiʔi, </ts>
               <ts e="T440" id="Seg_4481" n="e" s="T439">dĭ </ts>
               <ts e="T441" id="Seg_4483" n="e" s="T440">(pa-) </ts>
               <ts e="T442" id="Seg_4485" n="e" s="T441">kambi, </ts>
               <ts e="T443" id="Seg_4487" n="e" s="T442">(püje=) </ts>
               <ts e="T444" id="Seg_4489" n="e" s="T443">püjet </ts>
               <ts e="T445" id="Seg_4491" n="e" s="T444">toʔnaːrbi, </ts>
               <ts e="T446" id="Seg_4493" n="e" s="T445">(kemziʔ=) </ts>
               <ts e="T447" id="Seg_4495" n="e" s="T446">kemziʔ </ts>
               <ts e="T449" id="Seg_4497" n="e" s="T447">lăpatkam… </ts>
               <ts e="T450" id="Seg_4499" n="e" s="T449">Dĭgəttə </ts>
               <ts e="T451" id="Seg_4501" n="e" s="T450">aspaʔdə </ts>
               <ts e="T452" id="Seg_4503" n="e" s="T451">barəʔluʔbi. </ts>
               <ts e="T453" id="Seg_4505" n="e" s="T452">Mĭnzərbi, </ts>
               <ts e="T454" id="Seg_4507" n="e" s="T453">mĭnzərbi. </ts>
               <ts e="T455" id="Seg_4509" n="e" s="T454">Dĭgəttə </ts>
               <ts e="T456" id="Seg_4511" n="e" s="T455">uja </ts>
               <ts e="T457" id="Seg_4513" n="e" s="T456">naga, </ts>
               <ts e="T458" id="Seg_4515" n="e" s="T457">onʼiʔ </ts>
               <ts e="T459" id="Seg_4517" n="e" s="T458">lăpatka. </ts>
               <ts e="T460" id="Seg_4519" n="e" s="T459">Dĭ </ts>
               <ts e="T461" id="Seg_4521" n="e" s="T460">bar </ts>
               <ts e="T462" id="Seg_4523" n="e" s="T461">(š-) </ts>
               <ts e="T463" id="Seg_4525" n="e" s="T462">saʔməluʔbi, </ts>
               <ts e="T464" id="Seg_4527" n="e" s="T463">davaj </ts>
               <ts e="T465" id="Seg_4529" n="e" s="T464">dʼorzittə. </ts>
               <ts e="T466" id="Seg_4531" n="e" s="T465">A </ts>
               <ts e="T467" id="Seg_4533" n="e" s="T466">dĭzeŋ </ts>
               <ts e="T468" id="Seg_4535" n="e" s="T467">(măn- </ts>
               <ts e="T469" id="Seg_4537" n="e" s="T468">măndla-) </ts>
               <ts e="T470" id="Seg_4539" n="e" s="T469">mămbiʔi:" </ts>
               <ts e="T471" id="Seg_4541" n="e" s="T470">Tüjö </ts>
               <ts e="T472" id="Seg_4543" n="e" s="T471">ular </ts>
               <ts e="T473" id="Seg_4545" n="e" s="T472">dʼăgarlim, </ts>
               <ts e="T474" id="Seg_4547" n="e" s="T473">tănan </ts>
               <ts e="T475" id="Seg_4549" n="e" s="T474">lăpatkam </ts>
               <ts e="T476" id="Seg_4551" n="e" s="T475">(mĭmbiem)". </ts>
               <ts e="T477" id="Seg_4553" n="e" s="T476">"Măna </ts>
               <ts e="T478" id="Seg_4555" n="e" s="T477">ej </ts>
               <ts e="T479" id="Seg_4557" n="e" s="T478">kereʔ </ts>
               <ts e="T480" id="Seg_4559" n="e" s="T479">lăpatka!" </ts>
               <ts e="T481" id="Seg_4561" n="e" s="T480">Dĭgəttə </ts>
               <ts e="T482" id="Seg_4563" n="e" s="T481">dĭʔnə </ts>
               <ts e="T483" id="Seg_4565" n="e" s="T482">ular </ts>
               <ts e="T484" id="Seg_4567" n="e" s="T483">mĭbiʔi. </ts>
               <ts e="T485" id="Seg_4569" n="e" s="T484">((BRK)). </ts>
               <ts e="T486" id="Seg_4571" n="e" s="T485">Dĭgəttə </ts>
               <ts e="T487" id="Seg_4573" n="e" s="T486">dĭ </ts>
               <ts e="T488" id="Seg_4575" n="e" s="T487">Kodur </ts>
               <ts e="T489" id="Seg_4577" n="e" s="T488">(š-) </ts>
               <ts e="T490" id="Seg_4579" n="e" s="T489">suʔmiluʔpi. </ts>
               <ts e="T491" id="Seg_4581" n="e" s="T490">Băzəjdəbi, </ts>
               <ts e="T492" id="Seg_4583" n="e" s="T491">kĭškəbi </ts>
               <ts e="T493" id="Seg_4585" n="e" s="T492">simat. </ts>
               <ts e="T494" id="Seg_4587" n="e" s="T493">Ulardə </ts>
               <ts e="T495" id="Seg_4589" n="e" s="T494">ibi, </ts>
               <ts e="T496" id="Seg_4591" n="e" s="T495">kambi. </ts>
               <ts e="T497" id="Seg_4593" n="e" s="T496">Dĭgəttə </ts>
               <ts e="T498" id="Seg_4595" n="e" s="T497">(šo- </ts>
               <ts e="T499" id="Seg_4597" n="e" s="T498">il=) </ts>
               <ts e="T500" id="Seg_4599" n="e" s="T499">ildə </ts>
               <ts e="T501" id="Seg_4601" n="e" s="T500">šobi. </ts>
               <ts e="T502" id="Seg_4603" n="e" s="T501">Dĭn </ts>
               <ts e="T503" id="Seg_4605" n="e" s="T502">tože </ts>
               <ts e="T504" id="Seg_4607" n="e" s="T503">šide </ts>
               <ts e="T505" id="Seg_4609" n="e" s="T504">ular, </ts>
               <ts e="T506" id="Seg_4611" n="e" s="T505">nʼi </ts>
               <ts e="T507" id="Seg_4613" n="e" s="T506">ibi. </ts>
               <ts e="T508" id="Seg_4615" n="e" s="T507">I </ts>
               <ts e="T509" id="Seg_4617" n="e" s="T508">nʼin </ts>
               <ts e="T510" id="Seg_4619" n="e" s="T509">ne </ts>
               <ts e="T511" id="Seg_4621" n="e" s="T510">ibi. </ts>
               <ts e="T512" id="Seg_4623" n="e" s="T511">Dĭgəttə </ts>
               <ts e="T513" id="Seg_4625" n="e" s="T512">mălliaʔi:" </ts>
               <ts e="T514" id="Seg_4627" n="e" s="T513">Öʔlit </ts>
               <ts e="T515" id="Seg_4629" n="e" s="T514">tăn </ts>
               <ts e="T516" id="Seg_4631" n="e" s="T515">ular </ts>
               <ts e="T517" id="Seg_4633" n="e" s="T516">miʔ </ts>
               <ts e="T518" id="Seg_4635" n="e" s="T517">ularziʔ!" </ts>
               <ts e="T519" id="Seg_4637" n="e" s="T518">"Dʼok </ts>
               <ts e="T520" id="Seg_4639" n="e" s="T519">ej </ts>
               <ts e="T521" id="Seg_4641" n="e" s="T520">öʔlim, </ts>
               <ts e="T522" id="Seg_4643" n="e" s="T521">šiʔ </ts>
               <ts e="T523" id="Seg_4645" n="e" s="T522">ular </ts>
               <ts e="T524" id="Seg_4647" n="e" s="T523">măn </ts>
               <ts e="T525" id="Seg_4649" n="e" s="T524">ularbə </ts>
               <ts e="T526" id="Seg_4651" n="e" s="T525">amnuʔbəʔjə". </ts>
               <ts e="T527" id="Seg_4653" n="e" s="T526">A </ts>
               <ts e="T528" id="Seg_4655" n="e" s="T527">bostə </ts>
               <ts e="T529" id="Seg_4657" n="e" s="T528">vsʼo </ts>
               <ts e="T530" id="Seg_4659" n="e" s="T529">dʼăbaktəria, </ts>
               <ts e="T531" id="Seg_4661" n="e" s="T530">dʼăbaktəria. </ts>
               <ts e="T532" id="Seg_4663" n="e" s="T531">Dĭ </ts>
               <ts e="T533" id="Seg_4665" n="e" s="T532">(nep-) </ts>
               <ts e="T534" id="Seg_4667" n="e" s="T533">nuʔməluʔpi </ts>
               <ts e="T535" id="Seg_4669" n="e" s="T534">da </ts>
               <ts e="T536" id="Seg_4671" n="e" s="T535">ulardə </ts>
               <ts e="T537" id="Seg_4673" n="e" s="T536">öʔləbi </ts>
               <ts e="T538" id="Seg_4675" n="e" s="T537">dĭʔnə </ts>
               <ts e="T539" id="Seg_4677" n="e" s="T538">ulardə. </ts>
               <ts e="T540" id="Seg_4679" n="e" s="T539">A </ts>
               <ts e="T541" id="Seg_4681" n="e" s="T540">dĭgəttə </ts>
               <ts e="T542" id="Seg_4683" n="e" s="T541">iʔpiʔi </ts>
               <ts e="T543" id="Seg_4685" n="e" s="T542">kunolzittə, </ts>
               <ts e="T544" id="Seg_4687" n="e" s="T543">ertən </ts>
               <ts e="T545" id="Seg_4689" n="e" s="T544">uʔpiʔi. </ts>
               <ts e="T546" id="Seg_4691" n="e" s="T545">A </ts>
               <ts e="T547" id="Seg_4693" n="e" s="T546">ular </ts>
               <ts e="T548" id="Seg_4695" n="e" s="T547">naga. </ts>
               <ts e="T549" id="Seg_4697" n="e" s="T548">((BRK)). </ts>
               <ts e="T550" id="Seg_4699" n="e" s="T549">Dĭ </ts>
               <ts e="T551" id="Seg_4701" n="e" s="T550">Kodur </ts>
               <ts e="T552" id="Seg_4703" n="e" s="T551">uʔbdəbi. </ts>
               <ts e="T553" id="Seg_4705" n="e" s="T552">Ulardə </ts>
               <ts e="T554" id="Seg_4707" n="e" s="T553">sajnʼeʔpi. </ts>
               <ts e="T555" id="Seg_4709" n="e" s="T554">I </ts>
               <ts e="T556" id="Seg_4711" n="e" s="T555">dĭ </ts>
               <ts e="T557" id="Seg_4713" n="e" s="T556">dĭzen </ts>
               <ts e="T558" id="Seg_4715" n="e" s="T557">ulardə </ts>
               <ts e="T559" id="Seg_4717" n="e" s="T558">bar </ts>
               <ts e="T560" id="Seg_4719" n="e" s="T559">(kemziʔ </ts>
               <ts e="T561" id="Seg_4721" n="e" s="T560">b-) </ts>
               <ts e="T562" id="Seg_4723" n="e" s="T561">kemziʔ </ts>
               <ts e="T563" id="Seg_4725" n="e" s="T562">bar </ts>
               <ts e="T564" id="Seg_4727" n="e" s="T563">dʼüʔdəbi </ts>
               <ts e="T565" id="Seg_4729" n="e" s="T564">püjet </ts>
               <ts e="T566" id="Seg_4731" n="e" s="T565">i </ts>
               <ts e="T567" id="Seg_4733" n="e" s="T566">aŋdə. </ts>
               <ts e="T568" id="Seg_4735" n="e" s="T567">A </ts>
               <ts e="T569" id="Seg_4737" n="e" s="T568">ertən </ts>
               <ts e="T570" id="Seg_4739" n="e" s="T569">uʔbdəbiʔi, </ts>
               <ts e="T571" id="Seg_4741" n="e" s="T570">măndəʔi:" </ts>
               <ts e="T572" id="Seg_4743" n="e" s="T571">Šănap, </ts>
               <ts e="T573" id="Seg_4745" n="e" s="T572">(na-) </ts>
               <ts e="T574" id="Seg_4747" n="e" s="T573">miʔ </ts>
               <ts e="T575" id="Seg_4749" n="e" s="T574">ulardə </ts>
               <ts e="T576" id="Seg_4751" n="e" s="T575">dĭ </ts>
               <ts e="T577" id="Seg_4753" n="e" s="T576">dĭm </ts>
               <ts e="T578" id="Seg_4755" n="e" s="T577">ulardə </ts>
               <ts e="T579" id="Seg_4757" n="e" s="T578">bar </ts>
               <ts e="T580" id="Seg_4759" n="e" s="T579">sajnüžəʔpiʔi". </ts>
               <ts e="T581" id="Seg_4761" n="e" s="T580">((BRK)). </ts>
               <ts e="T582" id="Seg_4763" n="e" s="T581">Dĭgəttə </ts>
               <ts e="T583" id="Seg_4765" n="e" s="T582">dĭ </ts>
               <ts e="T584" id="Seg_4767" n="e" s="T583">uʔbdəbi. </ts>
               <ts e="T585" id="Seg_4769" n="e" s="T584">Simabə </ts>
               <ts e="T586" id="Seg_4771" n="e" s="T585">băzəbi. </ts>
               <ts e="T587" id="Seg_4773" n="e" s="T586">(Kĭškəbi). </ts>
               <ts e="T588" id="Seg_4775" n="e" s="T587">Šide </ts>
               <ts e="T589" id="Seg_4777" n="e" s="T588">ular </ts>
               <ts e="T590" id="Seg_4779" n="e" s="T589">ibi </ts>
               <ts e="T591" id="Seg_4781" n="e" s="T590">i </ts>
               <ts e="T592" id="Seg_4783" n="e" s="T591">kandəga, </ts>
               <ts e="T593" id="Seg_4785" n="e" s="T592">(šʼuʔ- </ts>
               <ts e="T594" id="Seg_4787" n="e" s="T593">ka-) </ts>
               <ts e="T595" id="Seg_4789" n="e" s="T594">kalluʔpi. </ts>
               <ts e="T596" id="Seg_4791" n="e" s="T595">Dĭgəttə </ts>
               <ts e="T597" id="Seg_4793" n="e" s="T596">šonəga, </ts>
               <ts e="T598" id="Seg_4795" n="e" s="T597">šonəga. </ts>
               <ts e="T599" id="Seg_4797" n="e" s="T598">Dĭn </ts>
               <ts e="T600" id="Seg_4799" n="e" s="T599">bar </ts>
               <ts e="T601" id="Seg_4801" n="e" s="T600">krospa </ts>
               <ts e="T602" id="Seg_4803" n="e" s="T601">nulaʔbə. </ts>
               <ts e="T603" id="Seg_4805" n="e" s="T602">Dĭ </ts>
               <ts e="T604" id="Seg_4807" n="e" s="T603">tĭlbi, </ts>
               <ts e="T605" id="Seg_4809" n="e" s="T604">dĭn </ts>
               <ts e="T606" id="Seg_4811" n="e" s="T605">nüke </ts>
               <ts e="T607" id="Seg_4813" n="e" s="T606">iʔbolaʔbə, </ts>
               <ts e="T608" id="Seg_4815" n="e" s="T607">dĭ </ts>
               <ts e="T609" id="Seg_4817" n="e" s="T608">nüke </ts>
               <ts e="T610" id="Seg_4819" n="e" s="T609">ibi </ts>
               <ts e="T611" id="Seg_4821" n="e" s="T610">i </ts>
               <ts e="T612" id="Seg_4823" n="e" s="T611">šide </ts>
               <ts e="T613" id="Seg_4825" n="e" s="T612">ular. </ts>
               <ts e="T614" id="Seg_4827" n="e" s="T613">I </ts>
               <ts e="T615" id="Seg_4829" n="e" s="T614">dĭ </ts>
               <ts e="T616" id="Seg_4831" n="e" s="T615">ularzaŋdə </ts>
               <ts e="T617" id="Seg_4833" n="e" s="T616">dĭ </ts>
               <ts e="T618" id="Seg_4835" n="e" s="T617">nükenə </ts>
               <ts e="T619" id="Seg_4837" n="e" s="T618">udazaŋdə </ts>
               <ts e="T620" id="Seg_4839" n="e" s="T619">sarbi. </ts>
               <ts e="T621" id="Seg_4841" n="e" s="T620">(Sĭjdə) </ts>
               <ts e="T622" id="Seg_4843" n="e" s="T621">Sĭjgəndə </ts>
               <ts e="T623" id="Seg_4845" n="e" s="T622">tagaj </ts>
               <ts e="T624" id="Seg_4847" n="e" s="T623">(păʔ-) </ts>
               <ts e="T625" id="Seg_4849" n="e" s="T624">păʔluʔpi. </ts>
               <ts e="T626" id="Seg_4851" n="e" s="T625">Dĭgəttə </ts>
               <ts e="T627" id="Seg_4853" n="e" s="T626">šobi, </ts>
               <ts e="T628" id="Seg_4855" n="e" s="T627">(dĭze-) </ts>
               <ts e="T629" id="Seg_4857" n="e" s="T628">dĭn </ts>
               <ts e="T630" id="Seg_4859" n="e" s="T629">šide </ts>
               <ts e="T631" id="Seg_4861" n="e" s="T630">koʔbdo </ts>
               <ts e="T632" id="Seg_4863" n="e" s="T631">i </ts>
               <ts e="T633" id="Seg_4865" n="e" s="T632">büzʼe </ts>
               <ts e="T634" id="Seg_4867" n="e" s="T633">amnolaʔbə. </ts>
               <ts e="T635" id="Seg_4869" n="e" s="T634">Dʼăbaktərlia, </ts>
               <ts e="T636" id="Seg_4871" n="e" s="T635">dʼăbaktərlia:" </ts>
               <ts e="T637" id="Seg_4873" n="e" s="T636">Măn </ts>
               <ts e="T638" id="Seg_4875" n="e" s="T637">dĭn </ts>
               <ts e="T639" id="Seg_4877" n="e" s="T638">nüke </ts>
               <ts e="T640" id="Seg_4879" n="e" s="T639">amnolaʔbə </ts>
               <ts e="T641" id="Seg_4881" n="e" s="T640">ularziʔ". </ts>
               <ts e="T642" id="Seg_4883" n="e" s="T641">A </ts>
               <ts e="T643" id="Seg_4885" n="e" s="T642">bostə </ts>
               <ts e="T644" id="Seg_4887" n="e" s="T643">(šĭjgən-) </ts>
               <ts e="T645" id="Seg_4889" n="e" s="T644">sĭjgəndə </ts>
               <ts e="T646" id="Seg_4891" n="e" s="T645">dagaj </ts>
               <ts e="T647" id="Seg_4893" n="e" s="T646">amnolaʔbə. </ts>
               <ts e="T648" id="Seg_4895" n="e" s="T647">A </ts>
               <ts e="T649" id="Seg_4897" n="e" s="T648">koʔbsaŋ:" </ts>
               <ts e="T650" id="Seg_4899" n="e" s="T649">Miʔ </ts>
               <ts e="T651" id="Seg_4901" n="e" s="T650">kalləbəj, </ts>
               <ts e="T652" id="Seg_4903" n="e" s="T651">deʔləbəj </ts>
               <ts e="T653" id="Seg_4905" n="e" s="T652">nükel </ts>
               <ts e="T654" id="Seg_4907" n="e" s="T653">tăn". </ts>
               <ts e="T655" id="Seg_4909" n="e" s="T654">"Dĭ </ts>
               <ts e="T656" id="Seg_4911" n="e" s="T655">ugandə </ts>
               <ts e="T657" id="Seg_4913" n="e" s="T656">pimnie. </ts>
               <ts e="T658" id="Seg_4915" n="e" s="T657">A_to </ts>
               <ts e="T659" id="Seg_4917" n="e" s="T658">dʼagarləj </ts>
               <ts e="T660" id="Seg_4919" n="e" s="T659">(bostə=) </ts>
               <ts e="T661" id="Seg_4921" n="e" s="T660">bostə </ts>
               <ts e="T662" id="Seg_4923" n="e" s="T661">себя". </ts>
               <ts e="T663" id="Seg_4925" n="e" s="T662">Dĭgəttə </ts>
               <ts e="T664" id="Seg_4927" n="e" s="T663">dĭzeŋ </ts>
               <ts e="T665" id="Seg_4929" n="e" s="T664">koʔbsaŋ </ts>
               <ts e="T666" id="Seg_4931" n="e" s="T665">nuʔməluʔbiʔi, </ts>
               <ts e="T667" id="Seg_4933" n="e" s="T666">šobiʔi. </ts>
               <ts e="T668" id="Seg_4935" n="e" s="T667">"Dʼagarluʔpi, </ts>
               <ts e="T669" id="Seg_4937" n="e" s="T668">ulardə </ts>
               <ts e="T670" id="Seg_4939" n="e" s="T669">bar </ts>
               <ts e="T671" id="Seg_4941" n="e" s="T670">(убе-) </ts>
               <ts e="T672" id="Seg_4943" n="e" s="T671">nuʔməluʔbiʔi </ts>
               <ts e="T673" id="Seg_4945" n="e" s="T672">nükem". </ts>
               <ts e="T674" id="Seg_4947" n="e" s="T673">Dĭgəttə </ts>
               <ts e="T675" id="Seg_4949" n="e" s="T674">dĭ </ts>
               <ts e="T676" id="Seg_4951" n="e" s="T675">bar </ts>
               <ts e="T677" id="Seg_4953" n="e" s="T676">saʔməluʔbi, </ts>
               <ts e="T678" id="Seg_4955" n="e" s="T677">Kodur, </ts>
               <ts e="T679" id="Seg_4957" n="e" s="T678">dʼorbi, </ts>
               <ts e="T680" id="Seg_4959" n="e" s="T679">dʼorbi. </ts>
               <ts e="T681" id="Seg_4961" n="e" s="T680">Dĭ </ts>
               <ts e="T682" id="Seg_4963" n="e" s="T681">büzʼe </ts>
               <ts e="T683" id="Seg_4965" n="e" s="T682">măndə:" </ts>
               <ts e="T684" id="Seg_4967" n="e" s="T683">It </ts>
               <ts e="T685" id="Seg_4969" n="e" s="T684">onʼiʔ </ts>
               <ts e="T686" id="Seg_4971" n="e" s="T685">koʔbdo". </ts>
               <ts e="T687" id="Seg_4973" n="e" s="T686">"Na_što </ts>
               <ts e="T688" id="Seg_4975" n="e" s="T687">măna </ts>
               <ts e="T689" id="Seg_4977" n="e" s="T688">onʼiʔ, </ts>
               <ts e="T690" id="Seg_4979" n="e" s="T689">măn </ts>
               <ts e="T691" id="Seg_4981" n="e" s="T690">nükem </ts>
               <ts e="T692" id="Seg_4983" n="e" s="T691">lutʼšə </ts>
               <ts e="T693" id="Seg_4985" n="e" s="T692">ibi". </ts>
               <ts e="T694" id="Seg_4987" n="e" s="T693">"No, </ts>
               <ts e="T695" id="Seg_4989" n="e" s="T694">it </ts>
               <ts e="T696" id="Seg_4991" n="e" s="T695">šide!" </ts>
               <ts e="T697" id="Seg_4993" n="e" s="T696">Dĭgəttə </ts>
               <ts e="T698" id="Seg_4995" n="e" s="T697">dĭ </ts>
               <ts e="T699" id="Seg_4997" n="e" s="T698">uʔbdəbi. </ts>
               <ts e="T700" id="Seg_4999" n="e" s="T699">Bazoʔ </ts>
               <ts e="T701" id="Seg_5001" n="e" s="T700">băzəjdəbi </ts>
               <ts e="T702" id="Seg_5003" n="e" s="T701">kadəldə. </ts>
               <ts e="T703" id="Seg_5005" n="e" s="T702">I </ts>
               <ts e="T704" id="Seg_5007" n="e" s="T703">šide </ts>
               <ts e="T705" id="Seg_5009" n="e" s="T704">koʔbdo </ts>
               <ts e="T706" id="Seg_5011" n="e" s="T705">ibi </ts>
               <ts e="T707" id="Seg_5013" n="e" s="T706">i </ts>
               <ts e="T708" id="Seg_5015" n="e" s="T707">kambi. </ts>
               <ts e="T709" id="Seg_5017" n="e" s="T708">Dĭgəttə </ts>
               <ts e="T710" id="Seg_5019" n="e" s="T709">šonəga, </ts>
               <ts e="T711" id="Seg_5021" n="e" s="T710">šonəga, </ts>
               <ts e="T712" id="Seg_5023" n="e" s="T711">dĭn </ts>
               <ts e="T713" id="Seg_5025" n="e" s="T712">ularəʔi </ts>
               <ts e="T714" id="Seg_5027" n="e" s="T713">mĭlleʔbəʔjə. </ts>
               <ts e="T715" id="Seg_5029" n="e" s="T714">Dĭgəttə </ts>
               <ts e="T716" id="Seg_5031" n="e" s="T715">šobi </ts>
               <ts e="T717" id="Seg_5033" n="e" s="T716">pastuxtə:" </ts>
               <ts e="T718" id="Seg_5035" n="e" s="T717">Dön </ts>
               <ts e="T719" id="Seg_5037" n="e" s="T718">măn </ts>
               <ts e="T720" id="Seg_5039" n="e" s="T719">ular!" </ts>
               <ts e="T721" id="Seg_5041" n="e" s="T720">(Da). </ts>
               <ts e="T722" id="Seg_5043" n="e" s="T721">((BRK)). </ts>
               <ts e="T723" id="Seg_5045" n="e" s="T722">Onʼiʔ </ts>
               <ts e="T724" id="Seg_5047" n="e" s="T723">(m-) </ts>
               <ts e="T725" id="Seg_5049" n="e" s="T724">amnəlbi </ts>
               <ts e="T726" id="Seg_5051" n="e" s="T725">bünə </ts>
               <ts e="T727" id="Seg_5053" n="e" s="T726">(tol-) </ts>
               <ts e="T728" id="Seg_5055" n="e" s="T727">toʔndə, </ts>
               <ts e="T729" id="Seg_5057" n="e" s="T728">onʼiʔ </ts>
               <ts e="T730" id="Seg_5059" n="e" s="T729">amnəlbəbi </ts>
               <ts e="T731" id="Seg_5061" n="e" s="T730">dʼijegən. </ts>
               <ts e="T732" id="Seg_5063" n="e" s="T731">Dĭgəttə </ts>
               <ts e="T733" id="Seg_5065" n="e" s="T732">măllaʔbə:" </ts>
               <ts e="T734" id="Seg_5067" n="e" s="T733">Măn </ts>
               <ts e="T735" id="Seg_5069" n="e" s="T734">ular!" </ts>
               <ts e="T736" id="Seg_5071" n="e" s="T735">"No, </ts>
               <ts e="T737" id="Seg_5073" n="e" s="T736">surardə. </ts>
               <ts e="T738" id="Seg_5075" n="e" s="T737">(У </ts>
               <ts e="T739" id="Seg_5077" n="e" s="T738">одинного) </ts>
               <ts e="T740" id="Seg_5079" n="e" s="T739">surardə". </ts>
               <ts e="T741" id="Seg_5081" n="e" s="T740">Dĭ </ts>
               <ts e="T742" id="Seg_5083" n="e" s="T741">surarluʔpi. </ts>
               <ts e="T743" id="Seg_5085" n="e" s="T742">"Kodurən </ts>
               <ts e="T744" id="Seg_5087" n="e" s="T743">ularzaŋdə!" </ts>
               <ts e="T745" id="Seg_5089" n="e" s="T744">"Dĭgəttə </ts>
               <ts e="T746" id="Seg_5091" n="e" s="T745">dʼijegən </ts>
               <ts e="T747" id="Seg_5093" n="e" s="T746">kuzanə </ts>
               <ts e="T748" id="Seg_5095" n="e" s="T747">surardə!" </ts>
               <ts e="T749" id="Seg_5097" n="e" s="T748">Surarbi, </ts>
               <ts e="T750" id="Seg_5099" n="e" s="T749">dĭ </ts>
               <ts e="T751" id="Seg_5101" n="e" s="T750">măndə:" </ts>
               <ts e="T752" id="Seg_5103" n="e" s="T751">Kodurən </ts>
               <ts e="T753" id="Seg_5105" n="e" s="T752">ulardə!" </ts>
               <ts e="T754" id="Seg_5107" n="e" s="T753">Dĭgəttə </ts>
               <ts e="T755" id="Seg_5109" n="e" s="T754">(ibɨ) </ts>
               <ts e="T756" id="Seg_5111" n="e" s="T755">ibi </ts>
               <ts e="T757" id="Seg_5113" n="e" s="T756">ulardə. </ts>
               <ts e="T758" id="Seg_5115" n="e" s="T757">((BRK)). </ts>
               <ts e="T759" id="Seg_5117" n="e" s="T758">Bazoʔ </ts>
               <ts e="T760" id="Seg_5119" n="e" s="T759">(s-) </ts>
               <ts e="T761" id="Seg_5121" n="e" s="T760">šobi, </ts>
               <ts e="T762" id="Seg_5123" n="e" s="T761">kuza </ts>
               <ts e="T763" id="Seg_5125" n="e" s="T762">ineʔi </ts>
               <ts e="T764" id="Seg_5127" n="e" s="T763">(mĭn-) </ts>
               <ts e="T765" id="Seg_5129" n="e" s="T764">amnolaʔbə. </ts>
               <ts e="T766" id="Seg_5131" n="e" s="T765">"Măn </ts>
               <ts e="T767" id="Seg_5133" n="e" s="T766">ineʔi!" </ts>
               <ts e="T768" id="Seg_5135" n="e" s="T767">A </ts>
               <ts e="T769" id="Seg_5137" n="e" s="T768">dĭ </ts>
               <ts e="T770" id="Seg_5139" n="e" s="T769">măndə:" </ts>
               <ts e="T771" id="Seg_5141" n="e" s="T770">Dʼok, </ts>
               <ts e="T772" id="Seg_5143" n="e" s="T771">măn </ts>
               <ts e="T773" id="Seg_5145" n="e" s="T772">ineʔi!" </ts>
               <ts e="T774" id="Seg_5147" n="e" s="T773">"No, </ts>
               <ts e="T775" id="Seg_5149" n="e" s="T774">surardə </ts>
               <ts e="T776" id="Seg_5151" n="e" s="T775">bünə </ts>
               <ts e="T777" id="Seg_5153" n="e" s="T776">kuzam!" </ts>
               <ts e="T778" id="Seg_5155" n="e" s="T777">Dĭ </ts>
               <ts e="T779" id="Seg_5157" n="e" s="T778">surarbi:" </ts>
               <ts e="T780" id="Seg_5159" n="e" s="T779">Kodurən </ts>
               <ts e="T781" id="Seg_5161" n="e" s="T780">ineʔi!" </ts>
               <ts e="T782" id="Seg_5163" n="e" s="T781">"Dĭgəttə </ts>
               <ts e="T783" id="Seg_5165" n="e" s="T782">dʼijen </ts>
               <ts e="T784" id="Seg_5167" n="e" s="T783">kuzanə </ts>
               <ts e="T785" id="Seg_5169" n="e" s="T784">surardə!" </ts>
               <ts e="T786" id="Seg_5171" n="e" s="T785">Dĭ </ts>
               <ts e="T787" id="Seg_5173" n="e" s="T786">surarbi:" </ts>
               <ts e="T788" id="Seg_5175" n="e" s="T787">Kodurən </ts>
               <ts e="T789" id="Seg_5177" n="e" s="T788">ineʔi!" </ts>
               <ts e="T790" id="Seg_5179" n="e" s="T789">Dĭ </ts>
               <ts e="T791" id="Seg_5181" n="e" s="T790">(bar </ts>
               <ts e="T792" id="Seg_5183" n="e" s="T791">öʔluʔpi). </ts>
               <ts e="T793" id="Seg_5185" n="e" s="T792">Dĭgəttə </ts>
               <ts e="T794" id="Seg_5187" n="e" s="T793">tüžöjəʔi </ts>
               <ts e="T795" id="Seg_5189" n="e" s="T794">mĭlleʔbəʔjə. </ts>
               <ts e="T796" id="Seg_5191" n="e" s="T795">(I </ts>
               <ts e="T797" id="Seg_5193" n="e" s="T796">tĭn) </ts>
               <ts e="T798" id="Seg_5195" n="e" s="T797">tože </ts>
               <ts e="T799" id="Seg_5197" n="e" s="T798">kuza </ts>
               <ts e="T800" id="Seg_5199" n="e" s="T799">măndəlaʔbə </ts>
               <ts e="T801" id="Seg_5201" n="e" s="T800">tüžöjəʔi. </ts>
               <ts e="T802" id="Seg_5203" n="e" s="T801">Dĭ </ts>
               <ts e="T803" id="Seg_5205" n="e" s="T802">šobi:" </ts>
               <ts e="T804" id="Seg_5207" n="e" s="T803">Măn </ts>
               <ts e="T805" id="Seg_5209" n="e" s="T804">tüžöjəʔjə!" </ts>
               <ts e="T806" id="Seg_5211" n="e" s="T805">"Dʼok, </ts>
               <ts e="T807" id="Seg_5213" n="e" s="T806">ej </ts>
               <ts e="T808" id="Seg_5215" n="e" s="T807">tăn!" </ts>
               <ts e="T809" id="Seg_5217" n="e" s="T808">"No, </ts>
               <ts e="T810" id="Seg_5219" n="e" s="T809">suraraʔ </ts>
               <ts e="T811" id="Seg_5221" n="e" s="T810">bügən </ts>
               <ts e="T812" id="Seg_5223" n="e" s="T811">kuza!" </ts>
               <ts e="T813" id="Seg_5225" n="e" s="T812">Dĭgəttə </ts>
               <ts e="T814" id="Seg_5227" n="e" s="T813">dʼijegən </ts>
               <ts e="T815" id="Seg_5229" n="e" s="T814">dĭ </ts>
               <ts e="T816" id="Seg_5231" n="e" s="T815">surarbi. </ts>
               <ts e="T817" id="Seg_5233" n="e" s="T816">"Šindən </ts>
               <ts e="T818" id="Seg_5235" n="e" s="T817">tüžöjdə?" </ts>
               <ts e="T819" id="Seg_5237" n="e" s="T818">(Dĭ=) </ts>
               <ts e="T820" id="Seg_5239" n="e" s="T819">Dĭ </ts>
               <ts e="T821" id="Seg_5241" n="e" s="T820">mămbi: </ts>
               <ts e="T822" id="Seg_5243" n="e" s="T821">"Kodurən </ts>
               <ts e="T823" id="Seg_5245" n="e" s="T822">tüžöjdə!" </ts>
               <ts e="T824" id="Seg_5247" n="e" s="T823">I </ts>
               <ts e="T825" id="Seg_5249" n="e" s="T824">dʼijegən </ts>
               <ts e="T826" id="Seg_5251" n="e" s="T825">surarbi: </ts>
               <ts e="T827" id="Seg_5253" n="e" s="T826">"Šindən </ts>
               <ts e="T828" id="Seg_5255" n="e" s="T827">(tüžöjdə)?" </ts>
               <ts e="T829" id="Seg_5257" n="e" s="T828">"Kodurən </ts>
               <ts e="T830" id="Seg_5259" n="e" s="T829">tüžöjdə!" </ts>
               <ts e="T831" id="Seg_5261" n="e" s="T830">Dĭgəttə </ts>
               <ts e="T832" id="Seg_5263" n="e" s="T831">dĭ </ts>
               <ts e="T833" id="Seg_5265" n="e" s="T832">davaj </ts>
               <ts e="T834" id="Seg_5267" n="e" s="T833">nüjnəsʼtə </ts>
               <ts e="T835" id="Seg_5269" n="e" s="T834">nüjnə. </ts>
               <ts e="T836" id="Seg_5271" n="e" s="T835">"Măn </ts>
               <ts e="T837" id="Seg_5273" n="e" s="T836">lăpatka </ts>
               <ts e="T838" id="Seg_5275" n="e" s="T837">сухой </ts>
               <ts e="T839" id="Seg_5277" n="e" s="T838">(ibim-) </ts>
               <ts e="T840" id="Seg_5279" n="e" s="T839">ibiem. </ts>
               <ts e="T841" id="Seg_5281" n="e" s="T840">(Ula-) </ts>
               <ts e="T842" id="Seg_5283" n="e" s="T841">Ular </ts>
               <ts e="T843" id="Seg_5285" n="e" s="T842">ibiem. </ts>
               <ts e="T844" id="Seg_5287" n="e" s="T843">Ulardə </ts>
               <ts e="T845" id="Seg_5289" n="e" s="T844">šide </ts>
               <ts e="T846" id="Seg_5291" n="e" s="T845">ular </ts>
               <ts e="T847" id="Seg_5293" n="e" s="T846">ibiem. </ts>
               <ts e="T848" id="Seg_5295" n="e" s="T847">Šide </ts>
               <ts e="T849" id="Seg_5297" n="e" s="T848">ular </ts>
               <ts e="T850" id="Seg_5299" n="e" s="T849">ibiem, </ts>
               <ts e="T851" id="Seg_5301" n="e" s="T850">kambiam, </ts>
               <ts e="T852" id="Seg_5303" n="e" s="T851">nüke </ts>
               <ts e="T853" id="Seg_5305" n="e" s="T852">dʼügən </ts>
               <ts e="T854" id="Seg_5307" n="e" s="T853">ibiem. </ts>
               <ts e="T855" id="Seg_5309" n="e" s="T854">I </ts>
               <ts e="T856" id="Seg_5311" n="e" s="T855">šide </ts>
               <ts e="T857" id="Seg_5313" n="e" s="T856">ular </ts>
               <ts e="T858" id="Seg_5315" n="e" s="T857">i </ts>
               <ts e="T859" id="Seg_5317" n="e" s="T858">nüke </ts>
               <ts e="T860" id="Seg_5319" n="e" s="T859">dagaj </ts>
               <ts e="T861" id="Seg_5321" n="e" s="T860">(š- </ts>
               <ts e="T862" id="Seg_5323" n="e" s="T861">sĭjdə=) </ts>
               <ts e="T863" id="Seg_5325" n="e" s="T862">sĭjdə". </ts>
               <ts e="T864" id="Seg_5327" n="e" s="T863">((BRK)). </ts>
               <ts e="T865" id="Seg_5329" n="e" s="T864">Dagaj </ts>
               <ts e="T866" id="Seg_5331" n="e" s="T865">sĭjdə </ts>
               <ts e="T867" id="Seg_5333" n="e" s="T866">(mün-) </ts>
               <ts e="T868" id="Seg_5335" n="e" s="T867">müʔluʔpi. </ts>
               <ts e="T869" id="Seg_5337" n="e" s="T868">A </ts>
               <ts e="T870" id="Seg_5339" n="e" s="T869">bostə </ts>
               <ts e="T871" id="Seg_5341" n="e" s="T870">šobi:" </ts>
               <ts e="T872" id="Seg_5343" n="e" s="T871">Măn </ts>
               <ts e="T873" id="Seg_5345" n="e" s="T872">dĭn </ts>
               <ts e="T874" id="Seg_5347" n="e" s="T873">nükem </ts>
               <ts e="T875" id="Seg_5349" n="e" s="T874">ige </ts>
               <ts e="T876" id="Seg_5351" n="e" s="T875">i </ts>
               <ts e="T877" id="Seg_5353" n="e" s="T876">šide </ts>
               <ts e="T878" id="Seg_5355" n="e" s="T877">ular". </ts>
               <ts e="T879" id="Seg_5357" n="e" s="T878">I </ts>
               <ts e="T880" id="Seg_5359" n="e" s="T879">šide </ts>
               <ts e="T881" id="Seg_5361" n="e" s="T880">koʔbsaŋ </ts>
               <ts e="T882" id="Seg_5363" n="e" s="T881">i </ts>
               <ts e="T883" id="Seg_5365" n="e" s="T882">büzʼe </ts>
               <ts e="T884" id="Seg_5367" n="e" s="T883">amnolaʔbə. </ts>
               <ts e="T885" id="Seg_5369" n="e" s="T884">Dĭ </ts>
               <ts e="T886" id="Seg_5371" n="e" s="T885">bar </ts>
               <ts e="T887" id="Seg_5373" n="e" s="T886">büzʼe </ts>
               <ts e="T888" id="Seg_5375" n="e" s="T887">dʼăbaktəria… </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_5376" s="T0">PKZ_196X_SU0222.001 (001)</ta>
            <ta e="T11" id="Seg_5377" s="T3">PKZ_196X_SU0222.002 (002)</ta>
            <ta e="T15" id="Seg_5378" s="T11">PKZ_196X_SU0222.003 (003)</ta>
            <ta e="T21" id="Seg_5379" s="T15">PKZ_196X_SU0222.004 (004)</ta>
            <ta e="T25" id="Seg_5380" s="T21">PKZ_196X_SU0222.005 (005)</ta>
            <ta e="T26" id="Seg_5381" s="T25">PKZ_196X_SU0222.006 (006)</ta>
            <ta e="T32" id="Seg_5382" s="T26">PKZ_196X_SU0222.007 (007)</ta>
            <ta e="T33" id="Seg_5383" s="T32">PKZ_196X_SU0222.008 (008)</ta>
            <ta e="T41" id="Seg_5384" s="T33">PKZ_196X_SU0222.009 (009)</ta>
            <ta e="T45" id="Seg_5385" s="T41">PKZ_196X_SU0222.010 (010)</ta>
            <ta e="T53" id="Seg_5386" s="T45">PKZ_196X_SU0222.011 (011)</ta>
            <ta e="T54" id="Seg_5387" s="T53">PKZ_196X_SU0222.012 (012)</ta>
            <ta e="T72" id="Seg_5388" s="T54">PKZ_196X_SU0222.013 (013)</ta>
            <ta e="T73" id="Seg_5389" s="T72">PKZ_196X_SU0222.014 (014)</ta>
            <ta e="T79" id="Seg_5390" s="T73">PKZ_196X_SU0222.015 (015)</ta>
            <ta e="T83" id="Seg_5391" s="T79">PKZ_196X_SU0222.016 (016)</ta>
            <ta e="T87" id="Seg_5392" s="T83">PKZ_196X_SU0222.017 (017)</ta>
            <ta e="T88" id="Seg_5393" s="T87">PKZ_196X_SU0222.018 (018)</ta>
            <ta e="T89" id="Seg_5394" s="T88">PKZ_196X_SU0222.019 (019)</ta>
            <ta e="T96" id="Seg_5395" s="T89">PKZ_196X_SU0222.020 (020)</ta>
            <ta e="T100" id="Seg_5396" s="T96">PKZ_196X_SU0222.021 (021)</ta>
            <ta e="T101" id="Seg_5397" s="T100">PKZ_196X_SU0222.022 (022)</ta>
            <ta e="T107" id="Seg_5398" s="T101">PKZ_196X_SU0222.023 (023)</ta>
            <ta e="T111" id="Seg_5399" s="T107">PKZ_196X_SU0222.024 (024)</ta>
            <ta e="T115" id="Seg_5400" s="T111">PKZ_196X_SU0222.025 (025)</ta>
            <ta e="T116" id="Seg_5401" s="T115">PKZ_196X_SU0222.026 (026)</ta>
            <ta e="T120" id="Seg_5402" s="T116">PKZ_196X_SU0222.027 (027)</ta>
            <ta e="T121" id="Seg_5403" s="T120">PKZ_196X_SU0222.028 (028)</ta>
            <ta e="T123" id="Seg_5404" s="T121">PKZ_196X_SU0222.029 (029)</ta>
            <ta e="T130" id="Seg_5405" s="T123">PKZ_196X_SU0222.030 (030)</ta>
            <ta e="T133" id="Seg_5406" s="T130">PKZ_196X_SU0222.031 (031)</ta>
            <ta e="T136" id="Seg_5407" s="T133">PKZ_196X_SU0222.032 (032)</ta>
            <ta e="T139" id="Seg_5408" s="T136">PKZ_196X_SU0222.033 (033)</ta>
            <ta e="T143" id="Seg_5409" s="T139">PKZ_196X_SU0222.034 (034)</ta>
            <ta e="T144" id="Seg_5410" s="T143">PKZ_196X_SU0222.035 (035)</ta>
            <ta e="T145" id="Seg_5411" s="T144">PKZ_196X_SU0222.036 (036)</ta>
            <ta e="T150" id="Seg_5412" s="T145">PKZ_196X_SU0222.037 (037)</ta>
            <ta e="T154" id="Seg_5413" s="T150">PKZ_196X_SU0222.038 (038)</ta>
            <ta e="T165" id="Seg_5414" s="T154">PKZ_196X_SU0222.039 (039)</ta>
            <ta e="T168" id="Seg_5415" s="T165">PKZ_196X_SU0222.040 (040)</ta>
            <ta e="T174" id="Seg_5416" s="T168">PKZ_196X_SU0222.041 (041)</ta>
            <ta e="T183" id="Seg_5417" s="T174">PKZ_196X_SU0222.042 (042)</ta>
            <ta e="T184" id="Seg_5418" s="T183">PKZ_196X_SU0222.043 (043)</ta>
            <ta e="T192" id="Seg_5419" s="T184">PKZ_196X_SU0222.044 (044)</ta>
            <ta e="T196" id="Seg_5420" s="T192">PKZ_196X_SU0222.045 (045)</ta>
            <ta e="T198" id="Seg_5421" s="T196">PKZ_196X_SU0222.046 (046)</ta>
            <ta e="T199" id="Seg_5422" s="T198">PKZ_196X_SU0222.047 (047)</ta>
            <ta e="T208" id="Seg_5423" s="T199">PKZ_196X_SU0222.048 (048)</ta>
            <ta e="T209" id="Seg_5424" s="T208">PKZ_196X_SU0222.049 (049)</ta>
            <ta e="T220" id="Seg_5425" s="T209">PKZ_196X_SU0222.050 (050)</ta>
            <ta e="T223" id="Seg_5426" s="T220">PKZ_196X_SU0222.051 (051)</ta>
            <ta e="T234" id="Seg_5427" s="T223">PKZ_196X_SU0222.052 (052)</ta>
            <ta e="T239" id="Seg_5428" s="T234">PKZ_196X_SU0222.053 (053)</ta>
            <ta e="T240" id="Seg_5429" s="T239">PKZ_196X_SU0222.054 (054)</ta>
            <ta e="T248" id="Seg_5430" s="T240">PKZ_196X_SU0222.055 (055)</ta>
            <ta e="T253" id="Seg_5431" s="T248">PKZ_196X_SU0222.056 (056)</ta>
            <ta e="T259" id="Seg_5432" s="T253">PKZ_196X_SU0222.057 (057)</ta>
            <ta e="T260" id="Seg_5433" s="T259">PKZ_196X_SU0222.058 (058)</ta>
            <ta e="T270" id="Seg_5434" s="T260">PKZ_196X_SU0222.059 (059)</ta>
            <ta e="T273" id="Seg_5435" s="T270">PKZ_196X_SU0222.060 (060)</ta>
            <ta e="T274" id="Seg_5436" s="T273">PKZ_196X_SU0222.061 (061)</ta>
            <ta e="T277" id="Seg_5437" s="T274">PKZ_196X_SU0222.062 (062)</ta>
            <ta e="T285" id="Seg_5438" s="T277">PKZ_196X_SU0222.063 (063)</ta>
            <ta e="T286" id="Seg_5439" s="T285">PKZ_196X_SU0222.064 (064)</ta>
            <ta e="T292" id="Seg_5440" s="T286">PKZ_196X_SU0222.065 (065)</ta>
            <ta e="T297" id="Seg_5441" s="T292">PKZ_196X_SU0222.066 (066)</ta>
            <ta e="T307" id="Seg_5442" s="T297">PKZ_196X_SU0222.067 (067)</ta>
            <ta e="T310" id="Seg_5443" s="T307">PKZ_196X_SU0222.068 (068)</ta>
            <ta e="T316" id="Seg_5444" s="T310">PKZ_196X_SU0222.069 (069)</ta>
            <ta e="T324" id="Seg_5445" s="T316">PKZ_196X_SU0222.070 (070)</ta>
            <ta e="T328" id="Seg_5446" s="T324">PKZ_196X_SU0222.071 (071)</ta>
            <ta e="T329" id="Seg_5447" s="T328">PKZ_196X_SU0222.072 (072)</ta>
            <ta e="T334" id="Seg_5448" s="T329">PKZ_196X_SU0222.073 (073)</ta>
            <ta e="T338" id="Seg_5449" s="T334">PKZ_196X_SU0222.074 (074)</ta>
            <ta e="T345" id="Seg_5450" s="T338">PKZ_196X_SU0222.075 (075)</ta>
            <ta e="T355" id="Seg_5451" s="T345">PKZ_196X_SU0222.076 (076)</ta>
            <ta e="T356" id="Seg_5452" s="T355">PKZ_196X_SU0222.077 (077)</ta>
            <ta e="T364" id="Seg_5453" s="T356">PKZ_196X_SU0222.078 (078)</ta>
            <ta e="T365" id="Seg_5454" s="T364">PKZ_196X_SU0222.079 (079)</ta>
            <ta e="T370" id="Seg_5455" s="T365">PKZ_196X_SU0222.080 (080)</ta>
            <ta e="T371" id="Seg_5456" s="T370">PKZ_196X_SU0222.081 (081)</ta>
            <ta e="T375" id="Seg_5457" s="T371">PKZ_196X_SU0222.082 (082)</ta>
            <ta e="T379" id="Seg_5458" s="T375">PKZ_196X_SU0222.083 (083)</ta>
            <ta e="T380" id="Seg_5459" s="T379">PKZ_196X_SU0222.084 (084)</ta>
            <ta e="T384" id="Seg_5460" s="T380">PKZ_196X_SU0222.085 (085)</ta>
            <ta e="T388" id="Seg_5461" s="T384">PKZ_196X_SU0222.086 (086)</ta>
            <ta e="T392" id="Seg_5462" s="T388">PKZ_196X_SU0222.087 (087)</ta>
            <ta e="T401" id="Seg_5463" s="T392">PKZ_196X_SU0222.088 (088)</ta>
            <ta e="T404" id="Seg_5464" s="T401">PKZ_196X_SU0222.089 (089)</ta>
            <ta e="T410" id="Seg_5465" s="T404">PKZ_196X_SU0222.090 (090)</ta>
            <ta e="T417" id="Seg_5466" s="T410">PKZ_196X_SU0222.091 (091)</ta>
            <ta e="T426" id="Seg_5467" s="T417">PKZ_196X_SU0222.092 (092)</ta>
            <ta e="T434" id="Seg_5468" s="T426">PKZ_196X_SU0222.093 (093)</ta>
            <ta e="T437" id="Seg_5469" s="T434">PKZ_196X_SU0222.094 (094)</ta>
            <ta e="T449" id="Seg_5470" s="T437">PKZ_196X_SU0222.095 (095)</ta>
            <ta e="T452" id="Seg_5471" s="T449">PKZ_196X_SU0222.096 (096)</ta>
            <ta e="T454" id="Seg_5472" s="T452">PKZ_196X_SU0222.097 (097)</ta>
            <ta e="T459" id="Seg_5473" s="T454">PKZ_196X_SU0222.098 (098)</ta>
            <ta e="T465" id="Seg_5474" s="T459">PKZ_196X_SU0222.099 (099)</ta>
            <ta e="T476" id="Seg_5475" s="T465">PKZ_196X_SU0222.100 (100)</ta>
            <ta e="T480" id="Seg_5476" s="T476">PKZ_196X_SU0222.101 (101)</ta>
            <ta e="T484" id="Seg_5477" s="T480">PKZ_196X_SU0222.102 (102)</ta>
            <ta e="T485" id="Seg_5478" s="T484">PKZ_196X_SU0222.103 (103)</ta>
            <ta e="T490" id="Seg_5479" s="T485">PKZ_196X_SU0222.104 (104)</ta>
            <ta e="T493" id="Seg_5480" s="T490">PKZ_196X_SU0222.105 (105)</ta>
            <ta e="T496" id="Seg_5481" s="T493">PKZ_196X_SU0222.106 (106)</ta>
            <ta e="T501" id="Seg_5482" s="T496">PKZ_196X_SU0222.107 (107)</ta>
            <ta e="T507" id="Seg_5483" s="T501">PKZ_196X_SU0222.108 (108)</ta>
            <ta e="T511" id="Seg_5484" s="T507">PKZ_196X_SU0222.109 (109)</ta>
            <ta e="T518" id="Seg_5485" s="T511">PKZ_196X_SU0222.110 (110)</ta>
            <ta e="T526" id="Seg_5486" s="T518">PKZ_196X_SU0222.111 (111)</ta>
            <ta e="T531" id="Seg_5487" s="T526">PKZ_196X_SU0222.112 (112)</ta>
            <ta e="T539" id="Seg_5488" s="T531">PKZ_196X_SU0222.113 (113)</ta>
            <ta e="T545" id="Seg_5489" s="T539">PKZ_196X_SU0222.114 (114)</ta>
            <ta e="T548" id="Seg_5490" s="T545">PKZ_196X_SU0222.115 (115)</ta>
            <ta e="T549" id="Seg_5491" s="T548">PKZ_196X_SU0222.116 (116)</ta>
            <ta e="T552" id="Seg_5492" s="T549">PKZ_196X_SU0222.117 (117)</ta>
            <ta e="T554" id="Seg_5493" s="T552">PKZ_196X_SU0222.118 (118)</ta>
            <ta e="T567" id="Seg_5494" s="T554">PKZ_196X_SU0222.119 (119)</ta>
            <ta e="T580" id="Seg_5495" s="T567">PKZ_196X_SU0222.120 (120)</ta>
            <ta e="T581" id="Seg_5496" s="T580">PKZ_196X_SU0222.121 (121)</ta>
            <ta e="T584" id="Seg_5497" s="T581">PKZ_196X_SU0222.122 (122)</ta>
            <ta e="T586" id="Seg_5498" s="T584">PKZ_196X_SU0222.123 (123)</ta>
            <ta e="T587" id="Seg_5499" s="T586">PKZ_196X_SU0222.124 (124)</ta>
            <ta e="T595" id="Seg_5500" s="T587">PKZ_196X_SU0222.125 (125)</ta>
            <ta e="T598" id="Seg_5501" s="T595">PKZ_196X_SU0222.126 (126)</ta>
            <ta e="T602" id="Seg_5502" s="T598">PKZ_196X_SU0222.127 (127)</ta>
            <ta e="T613" id="Seg_5503" s="T602">PKZ_196X_SU0222.128 (128)</ta>
            <ta e="T620" id="Seg_5504" s="T613">PKZ_196X_SU0222.129 (129)</ta>
            <ta e="T625" id="Seg_5505" s="T620">PKZ_196X_SU0222.130 (130)</ta>
            <ta e="T634" id="Seg_5506" s="T625">PKZ_196X_SU0222.131 (131)</ta>
            <ta e="T641" id="Seg_5507" s="T634">PKZ_196X_SU0222.132 (132)</ta>
            <ta e="T647" id="Seg_5508" s="T641">PKZ_196X_SU0222.133 (133)</ta>
            <ta e="T654" id="Seg_5509" s="T647">PKZ_196X_SU0222.134 (134)</ta>
            <ta e="T657" id="Seg_5510" s="T654">PKZ_196X_SU0222.135 (135)</ta>
            <ta e="T662" id="Seg_5511" s="T657">PKZ_196X_SU0222.136 (136)</ta>
            <ta e="T667" id="Seg_5512" s="T662">PKZ_196X_SU0222.137 (137)</ta>
            <ta e="T673" id="Seg_5513" s="T667">PKZ_196X_SU0222.138 (138)</ta>
            <ta e="T680" id="Seg_5514" s="T673">PKZ_196X_SU0222.139 (139)</ta>
            <ta e="T686" id="Seg_5515" s="T680">PKZ_196X_SU0222.140 (140)</ta>
            <ta e="T693" id="Seg_5516" s="T686">PKZ_196X_SU0222.141 (141)</ta>
            <ta e="T696" id="Seg_5517" s="T693">PKZ_196X_SU0222.142 (142)</ta>
            <ta e="T699" id="Seg_5518" s="T696">PKZ_196X_SU0222.143 (143)</ta>
            <ta e="T702" id="Seg_5519" s="T699">PKZ_196X_SU0222.144 (144)</ta>
            <ta e="T708" id="Seg_5520" s="T702">PKZ_196X_SU0222.145 (145)</ta>
            <ta e="T714" id="Seg_5521" s="T708">PKZ_196X_SU0222.146 (146)</ta>
            <ta e="T720" id="Seg_5522" s="T714">PKZ_196X_SU0222.147 (147)</ta>
            <ta e="T721" id="Seg_5523" s="T720">PKZ_196X_SU0222.148 (148)</ta>
            <ta e="T722" id="Seg_5524" s="T721">PKZ_196X_SU0222.149 (149)</ta>
            <ta e="T731" id="Seg_5525" s="T722">PKZ_196X_SU0222.150 (150)</ta>
            <ta e="T735" id="Seg_5526" s="T731">PKZ_196X_SU0222.151 (151)</ta>
            <ta e="T737" id="Seg_5527" s="T735">PKZ_196X_SU0222.152 (152)</ta>
            <ta e="T740" id="Seg_5528" s="T737">PKZ_196X_SU0222.153 (153)</ta>
            <ta e="T742" id="Seg_5529" s="T740">PKZ_196X_SU0222.154 (154)</ta>
            <ta e="T744" id="Seg_5530" s="T742">PKZ_196X_SU0222.155 (155)</ta>
            <ta e="T748" id="Seg_5531" s="T744">PKZ_196X_SU0222.156 (156)</ta>
            <ta e="T753" id="Seg_5532" s="T748">PKZ_196X_SU0222.157 (157)</ta>
            <ta e="T757" id="Seg_5533" s="T753">PKZ_196X_SU0222.158 (158)</ta>
            <ta e="T758" id="Seg_5534" s="T757">PKZ_196X_SU0222.159 (159)</ta>
            <ta e="T765" id="Seg_5535" s="T758">PKZ_196X_SU0222.160 (160)</ta>
            <ta e="T767" id="Seg_5536" s="T765">PKZ_196X_SU0222.161 (161)</ta>
            <ta e="T773" id="Seg_5537" s="T767">PKZ_196X_SU0222.162 (162)</ta>
            <ta e="T777" id="Seg_5538" s="T773">PKZ_196X_SU0222.163 (163)</ta>
            <ta e="T781" id="Seg_5539" s="T777">PKZ_196X_SU0222.164 (164)</ta>
            <ta e="T785" id="Seg_5540" s="T781">PKZ_196X_SU0222.165 (165)</ta>
            <ta e="T789" id="Seg_5541" s="T785">PKZ_196X_SU0222.166 (166)</ta>
            <ta e="T792" id="Seg_5542" s="T789">PKZ_196X_SU0222.167 (167)</ta>
            <ta e="T795" id="Seg_5543" s="T792">PKZ_196X_SU0222.168 (168)</ta>
            <ta e="T801" id="Seg_5544" s="T795">PKZ_196X_SU0222.169 (169)</ta>
            <ta e="T805" id="Seg_5545" s="T801">PKZ_196X_SU0222.170 (170)</ta>
            <ta e="T808" id="Seg_5546" s="T805">PKZ_196X_SU0222.171 (171)</ta>
            <ta e="T812" id="Seg_5547" s="T808">PKZ_196X_SU0222.172 (172)</ta>
            <ta e="T816" id="Seg_5548" s="T812">PKZ_196X_SU0222.173 (173)</ta>
            <ta e="T818" id="Seg_5549" s="T816">PKZ_196X_SU0222.174 (174)</ta>
            <ta e="T823" id="Seg_5550" s="T818">PKZ_196X_SU0222.175 (175)</ta>
            <ta e="T828" id="Seg_5551" s="T823">PKZ_196X_SU0222.176 (176) </ta>
            <ta e="T830" id="Seg_5552" s="T828">PKZ_196X_SU0222.177 (178)</ta>
            <ta e="T835" id="Seg_5553" s="T830">PKZ_196X_SU0222.178 (179)</ta>
            <ta e="T840" id="Seg_5554" s="T835">PKZ_196X_SU0222.179 (180)</ta>
            <ta e="T843" id="Seg_5555" s="T840">PKZ_196X_SU0222.180 (181)</ta>
            <ta e="T847" id="Seg_5556" s="T843">PKZ_196X_SU0222.181 (182)</ta>
            <ta e="T854" id="Seg_5557" s="T847">PKZ_196X_SU0222.182 (183)</ta>
            <ta e="T863" id="Seg_5558" s="T854">PKZ_196X_SU0222.183 (184)</ta>
            <ta e="T864" id="Seg_5559" s="T863">PKZ_196X_SU0222.184 (185)</ta>
            <ta e="T868" id="Seg_5560" s="T864">PKZ_196X_SU0222.185 (186)</ta>
            <ta e="T878" id="Seg_5561" s="T868">PKZ_196X_SU0222.186 (187)</ta>
            <ta e="T884" id="Seg_5562" s="T878">PKZ_196X_SU0222.187 (188)</ta>
            <ta e="T888" id="Seg_5563" s="T884">PKZ_196X_SU0222.188 (189)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_5564" s="T0">((DMG)) (imsobie) tarzittə. </ta>
            <ta e="T11" id="Seg_5565" s="T3">Măn dĭʔnə embiem beškeʔi, kajak mĭbiem, aktʼa mĭbiem. </ta>
            <ta e="T15" id="Seg_5566" s="T11">Oʔb, šide, nagur, teʔtə. </ta>
            <ta e="T21" id="Seg_5567" s="T15">Dĭzeŋ tura ibiʔi, šindidə bar sadarlaʔbə. </ta>
            <ta e="T25" id="Seg_5568" s="T21">I aktʼa ej kabarlia. </ta>
            <ta e="T26" id="Seg_5569" s="T25">((BRK)). </ta>
            <ta e="T32" id="Seg_5570" s="T26">Dĭzeŋ dĭ (turab-) tura sadarla ibiʔi. </ta>
            <ta e="T33" id="Seg_5571" s="T32">((BRK)). </ta>
            <ta e="T41" id="Seg_5572" s="T33">A girgit kuzagən naga (iza-) (isaʔtə) aktʼa mĭzittə. </ta>
            <ta e="T45" id="Seg_5573" s="T41">Tĭn tüžöjdə il ibiʔi. </ta>
            <ta e="T53" id="Seg_5574" s="T45">Ĭmbi ige bar iluʔpiʔi i mĭbiʔi aktʼa dĭbər. </ta>
            <ta e="T54" id="Seg_5575" s="T53">((BRK)). </ta>
            <ta e="T72" id="Seg_5576" s="T54">Măn tuganbə kalla dʼürbi gorăttə i dĭn mašina iləj, oldʼa, bazəsʼtə (kujnek=) kujnegəʔi, piʔmeʔi, bar (ombi-) ĭmbi ige bazəsʼtə. </ta>
            <ta e="T73" id="Seg_5577" s="T72">((BRK)). </ta>
            <ta e="T79" id="Seg_5578" s="T73">Tibi i ne amnobiʔi ugandə jakše. </ta>
            <ta e="T83" id="Seg_5579" s="T79">Ajirbiʔi (один=) onʼiʔ … </ta>
            <ta e="T87" id="Seg_5580" s="T83">Onʼiʔ onʼiʔtə ajirbiʔi bar. </ta>
            <ta e="T88" id="Seg_5581" s="T87">(Kamrolaʔtəʔi). </ta>
            <ta e="T89" id="Seg_5582" s="T88">Panarlaʔbəʔjə. </ta>
            <ta e="T96" id="Seg_5583" s="T89">I dĭzeŋ esseŋdə ibiʔi, ugandə esseŋdə jakše. </ta>
            <ta e="T100" id="Seg_5584" s="T96">Ĭmbi măllal, dĭzeŋ nʼilgölaʔbəʔjə. </ta>
            <ta e="T101" id="Seg_5585" s="T100">((BRK)). </ta>
            <ta e="T107" id="Seg_5586" s="T101">A miʔ esseŋ ugandə ej jakše. </ta>
            <ta e="T111" id="Seg_5587" s="T107">Dʼabrolaʔbəʔjə, kudonzlaʔbəʔjə, ej nʼilgölaʔbəʔjə. </ta>
            <ta e="T115" id="Seg_5588" s="T111">Măn dĭzem bar münörlaʔbəm. </ta>
            <ta e="T116" id="Seg_5589" s="T115">((BRK)). </ta>
            <ta e="T120" id="Seg_5590" s="T116">Esseŋbə tüšəlliem sazən pʼaŋdəsʼtə. </ta>
            <ta e="T121" id="Seg_5591" s="T120">((BRK)). </ta>
            <ta e="T123" id="Seg_5592" s="T121">Nuzaŋ amnobiʔi. </ta>
            <ta e="T130" id="Seg_5593" s="T123">Šindi ugandə aktʼa iʔgö, a šindin naga. </ta>
            <ta e="T133" id="Seg_5594" s="T130">Dĭ ara bĭtlie. </ta>
            <ta e="T136" id="Seg_5595" s="T133">Bar aktʼa bĭtluʔləj. </ta>
            <ta e="T139" id="Seg_5596" s="T136">Dĭgəttə ĭmbidə naga. </ta>
            <ta e="T143" id="Seg_5597" s="T139">Togonoria, togonoria, bar bĭʔluʔləj. </ta>
            <ta e="T144" id="Seg_5598" s="T143">((BRK)). </ta>
            <ta e="T145" id="Seg_5599" s="T144">((BRK)). </ta>
            <ta e="T150" id="Seg_5600" s="T145">Sibirʼgən ugandə iʔgö nuzaŋ amnobiʔi. </ta>
            <ta e="T154" id="Seg_5601" s="T150">Dĭgəttə sĭre pa özerluʔpi. </ta>
            <ta e="T165" id="Seg_5602" s="T154">Dĭzeŋ mălliaʔi:" Nu, (kazaŋ-) kazak il šoləʔjə, miʔnʼibeʔ ej jakše moləj". </ta>
            <ta e="T168" id="Seg_5603" s="T165">Dĭgəttə labazəʔi abiʔi. </ta>
            <ta e="T174" id="Seg_5604" s="T168">Dĭbər piʔi tažerbiʔi, i baltu ibiʔi. </ta>
            <ta e="T183" id="Seg_5605" s="T174">Jaʔpiʔi, i dĭ saʔməluʔpi, (dĭzem) dĭzeŋ bar dĭn kübiʔi. </ta>
            <ta e="T184" id="Seg_5606" s="T183">((BRK)). </ta>
            <ta e="T192" id="Seg_5607" s="T184">Lutʼšə (măna=) măn bɨ kübiem, чем esseŋbə dĭrgit. </ta>
            <ta e="T196" id="Seg_5608" s="T192">Ej jakšeʔi, bar tajirlaʔbəʔjə. </ta>
            <ta e="T198" id="Seg_5609" s="T196">Ilzi dʼabrolaʔbəʔjə. </ta>
            <ta e="T199" id="Seg_5610" s="T198">((BRK)). </ta>
            <ta e="T208" id="Seg_5611" s="T199">Bar ara bĭtleʔbəʔjə, ažnə simazi ej molia mănderzittə dĭzeŋdə. </ta>
            <ta e="T209" id="Seg_5612" s="T208">((BRK)). </ta>
            <ta e="T220" id="Seg_5613" s="T209">Onʼiʔ nen nagur nʼi ibi, šide jakše, a onʼiʔ ej jakše. </ta>
            <ta e="T223" id="Seg_5614" s="T220">Bar dĭm münörbiʔi. </ta>
            <ta e="T234" id="Seg_5615" s="T223">Üjüzi bar, i (muz-) muzuruksi, i pazi bar (kuno- ku-) toʔnarbiʔi. </ta>
            <ta e="T239" id="Seg_5616" s="T234">A măn (u) mămbiam … </ta>
            <ta e="T240" id="Seg_5617" s="T239">((BRK)). </ta>
            <ta e="T248" id="Seg_5618" s="T240">A măn (u) mămbiam:" Iʔ üžüŋgeʔ, možet özerləj. </ta>
            <ta e="T253" id="Seg_5619" s="T248">Sagəštə iʔgö moləj, urgo moləj. </ta>
            <ta e="T259" id="Seg_5620" s="T253">Možet bar (baruʔləj) dĭgəttə jakše moləj". </ta>
            <ta e="T260" id="Seg_5621" s="T259">((BRK)). </ta>
            <ta e="T270" id="Seg_5622" s="T260">A dĭ girgit üdʼüge ibi, i urgo (du-) dĭrgit že. </ta>
            <ta e="T273" id="Seg_5623" s="T270">Ĭmbidə naga jakše. </ta>
            <ta e="T274" id="Seg_5624" s="T273">((BRK)). </ta>
            <ta e="T277" id="Seg_5625" s="T274">Dĭ armijanə kambi. </ta>
            <ta e="T285" id="Seg_5626" s="T277">Vsʼo ravno šobi, gibər togonorzittə kaləj, dĭm bar sürerleʔbəʔjə. </ta>
            <ta e="T286" id="Seg_5627" s="T285">((BRK)). </ta>
            <ta e="T292" id="Seg_5628" s="T286">Onʼiʔ nüke nʼit ibi, bar jezerik. </ta>
            <ta e="T297" id="Seg_5629" s="T292">"Tăn ĭmbi măna ej tüšəlbiel?" </ta>
            <ta e="T307" id="Seg_5630" s="T297">A dĭ mămbi:" Măn tüšəlbiem tănan, ĭmbi kereʔ (tăn=) tănan? </ta>
            <ta e="T310" id="Seg_5631" s="T307">Tăn диплом ibiel. </ta>
            <ta e="T316" id="Seg_5632" s="T310">A (dĭ bar muzu-) Ara bĭtliel!" </ta>
            <ta e="T324" id="Seg_5633" s="T316">A dĭ bar muzuruksi da iat xatʼel münörzittə. </ta>
            <ta e="T328" id="Seg_5634" s="T324">A il ej mĭbiʔi. </ta>
            <ta e="T329" id="Seg_5635" s="T328">((BRK)). </ta>
            <ta e="T334" id="Seg_5636" s="T329">Măn kagam văjnagən (ibiel=) ibie. </ta>
            <ta e="T338" id="Seg_5637" s="T334">I dĭm ibiʔi plendə. </ta>
            <ta e="T345" id="Seg_5638" s="T338">I pravăj udat müjötsi (xat-) dʼagarzittə xatʼeli. </ta>
            <ta e="T355" id="Seg_5639" s="T345">A dĭ bar dʼorbi, šide müjə (m-) (maːtəbiʔi) (ему) dĭʔnə. </ta>
            <ta e="T356" id="Seg_5640" s="T355">((BRK)). </ta>
            <ta e="T364" id="Seg_5641" s="T356">Măn (u) kagam nagur koʔbdo i nagur nʼi. </ta>
            <ta e="T365" id="Seg_5642" s="T364">((BRK)). </ta>
            <ta e="T370" id="Seg_5643" s="T365">Nörbəzittə tănan (sĭre) sĭre buga? </ta>
            <ta e="T371" id="Seg_5644" s="T370">((BRK)). </ta>
            <ta e="T375" id="Seg_5645" s="T371">Tăn nörbit, măn nörbit. </ta>
            <ta e="T379" id="Seg_5646" s="T375">Mănzittə tănan kömə buga? </ta>
            <ta e="T380" id="Seg_5647" s="T379">((BRK)). </ta>
            <ta e="T384" id="Seg_5648" s="T380">Tăn nörbit, măn nörbit. </ta>
            <ta e="T388" id="Seg_5649" s="T384">Mănzittə tănan sagər buga? </ta>
            <ta e="T392" id="Seg_5650" s="T388">Kodur Kodur, kömə părga. </ta>
            <ta e="T401" id="Seg_5651" s="T392">Šobi, dön il amnobiʔi, dĭ lăpatka kubi, uja naga. </ta>
            <ta e="T404" id="Seg_5652" s="T401">Dĭgəttə kambi, kambi. </ta>
            <ta e="T410" id="Seg_5653" s="T404">Nu, dĭn onʼiʔ kuza amnobi neziʔ. </ta>
            <ta e="T417" id="Seg_5654" s="T410">Dĭ šobi, (dĭzeŋ=) dĭzeŋdə ular ibi onʼiʔ. </ta>
            <ta e="T426" id="Seg_5655" s="T417">Dĭ măndə:" Nada segi bü mĭnzərzittə, kuzam (bĭz-) bădəsʼtə". </ta>
            <ta e="T434" id="Seg_5656" s="T426">A dĭ măndə:" Iʔ mĭnzəreʔ, măn ujam ige". </ta>
            <ta e="T437" id="Seg_5657" s="T434">Dĭgəttə aspaʔ edəbiʔi. </ta>
            <ta e="T449" id="Seg_5658" s="T437">Tus embiʔi, dĭ (pa-) kambi, (püje=) püjet toʔnaːrbi, (kemziʔ=) kemziʔ lăpatkam … </ta>
            <ta e="T452" id="Seg_5659" s="T449">Dĭgəttə aspaʔdə barəʔluʔbi. </ta>
            <ta e="T454" id="Seg_5660" s="T452">Mĭnzərbi, mĭnzərbi. </ta>
            <ta e="T459" id="Seg_5661" s="T454">Dĭgəttə uja naga, onʼiʔ lăpatka. </ta>
            <ta e="T465" id="Seg_5662" s="T459">Dĭ bar (š-) saʔməluʔbi, davaj dʼorzittə. </ta>
            <ta e="T476" id="Seg_5663" s="T465">A dĭzeŋ (măn- măndla-) mămbiʔi:" Tüjö ular dʼăgarlim, tănan lăpatkam (mĭmbiem)". </ta>
            <ta e="T480" id="Seg_5664" s="T476">"Măna ej kereʔ lăpatka!" </ta>
            <ta e="T484" id="Seg_5665" s="T480">Dĭgəttə dĭʔnə ular mĭbiʔi. </ta>
            <ta e="T485" id="Seg_5666" s="T484">((BRK)). </ta>
            <ta e="T490" id="Seg_5667" s="T485">Dĭgəttə dĭ Kodur (š-) suʔmiluʔpi. </ta>
            <ta e="T493" id="Seg_5668" s="T490">Băzəjdəbi, kĭškəbi simat. </ta>
            <ta e="T496" id="Seg_5669" s="T493">Ulardə ibi, kambi. </ta>
            <ta e="T501" id="Seg_5670" s="T496">Dĭgəttə (šo- il=) ildə šobi. </ta>
            <ta e="T507" id="Seg_5671" s="T501">Dĭn tože šide ular, nʼi ibi. </ta>
            <ta e="T511" id="Seg_5672" s="T507">I nʼin ne ibi. </ta>
            <ta e="T518" id="Seg_5673" s="T511">Dĭgəttə mălliaʔi:" Öʔlit tăn ular miʔ ularziʔ!" </ta>
            <ta e="T526" id="Seg_5674" s="T518">"Dʼok ej öʔlim, šiʔ ular măn ularbə amnuʔbəʔjə". </ta>
            <ta e="T531" id="Seg_5675" s="T526">A bostə все dʼăbaktəria, dʼăbaktəria. </ta>
            <ta e="T539" id="Seg_5676" s="T531">Dĭ (nep-) nuʔməluʔpi da ulardə öʔləbi dĭʔnə ulardə. </ta>
            <ta e="T545" id="Seg_5677" s="T539">A dĭgəttə iʔpiʔi kunolzittə, ertən uʔpiʔi. </ta>
            <ta e="T548" id="Seg_5678" s="T545">A ular naga. </ta>
            <ta e="T549" id="Seg_5679" s="T548">((BRK)). </ta>
            <ta e="T552" id="Seg_5680" s="T549">Dĭ Kodur uʔbdəbi. </ta>
            <ta e="T554" id="Seg_5681" s="T552">Ulardə sajnʼeʔpi. </ta>
            <ta e="T567" id="Seg_5682" s="T554">I dĭ dĭzen ulardə bar (kemziʔ b-) kemziʔ bar dʼüʔdəbi püjet i aŋdə. </ta>
            <ta e="T580" id="Seg_5683" s="T567">A ertən uʔbdəbiʔi, măndəʔi:" Šănap, (na-) miʔ ulardə dĭ dĭm ulardə bar sajnüžəʔpiʔi". </ta>
            <ta e="T581" id="Seg_5684" s="T580">((BRK)). </ta>
            <ta e="T584" id="Seg_5685" s="T581">Dĭgəttə dĭ uʔbdəbi. </ta>
            <ta e="T586" id="Seg_5686" s="T584">Simabə băzəbi. </ta>
            <ta e="T587" id="Seg_5687" s="T586">(Kĭškəbi). </ta>
            <ta e="T595" id="Seg_5688" s="T587">Šide ular ibi i kandəga, (šʼuʔ- ka-) kalluʔpi. </ta>
            <ta e="T598" id="Seg_5689" s="T595">Dĭgəttə šonəga, šonəga. </ta>
            <ta e="T602" id="Seg_5690" s="T598">Dĭn bar krospa nulaʔbə. </ta>
            <ta e="T613" id="Seg_5691" s="T602">Dĭ tĭlbi, dĭn nüke iʔbolaʔbə, dĭ nüke ibi i šide ular. </ta>
            <ta e="T620" id="Seg_5692" s="T613">I dĭ ularzaŋdə dĭ nükenə udazaŋdə sarbi. </ta>
            <ta e="T625" id="Seg_5693" s="T620">(Sĭjdə) Sĭjgəndə tagaj (păʔ-) păʔluʔpi. </ta>
            <ta e="T634" id="Seg_5694" s="T625">Dĭgəttə šobi, (dĭze-) dĭn šide koʔbdo i büzʼe amnolaʔbə. </ta>
            <ta e="T641" id="Seg_5695" s="T634">Dʼăbaktərlia, dʼăbaktərlia:" Măn dĭn nüke amnolaʔbə ularziʔ". </ta>
            <ta e="T647" id="Seg_5696" s="T641">A bostə (šĭjgən-) sĭjgəndə dagaj amnolaʔbə. </ta>
            <ta e="T654" id="Seg_5697" s="T647">A koʔbsaŋ:" Miʔ kalləbəj, deʔləbəj nükel tăn". </ta>
            <ta e="T657" id="Seg_5698" s="T654">"Dĭ ugandə pimnie. </ta>
            <ta e="T662" id="Seg_5699" s="T657">A to dʼagarləj (bostə=) bostə себя". </ta>
            <ta e="T667" id="Seg_5700" s="T662">Dĭgəttə dĭzeŋ koʔbsaŋ nuʔməluʔbiʔi, šobiʔi. </ta>
            <ta e="T673" id="Seg_5701" s="T667">"Dʼagarluʔpi, ulardə bar (убе-) nuʔməluʔbiʔi nükem". </ta>
            <ta e="T680" id="Seg_5702" s="T673">Dĭgəttə dĭ bar saʔməluʔbi, Kodur, dʼorbi, dʼorbi. </ta>
            <ta e="T686" id="Seg_5703" s="T680">Dĭ büzʼe măndə:" It onʼiʔ koʔbdo". </ta>
            <ta e="T693" id="Seg_5704" s="T686">"Na što măna onʼiʔ, măn nükem lutʼšə ibi". </ta>
            <ta e="T696" id="Seg_5705" s="T693">"No, it šide!" </ta>
            <ta e="T699" id="Seg_5706" s="T696">Dĭgəttə dĭ uʔbdəbi. </ta>
            <ta e="T702" id="Seg_5707" s="T699">Bazoʔ băzəjdəbi kadəldə. </ta>
            <ta e="T708" id="Seg_5708" s="T702">I šide koʔbdo ibi i kambi. </ta>
            <ta e="T714" id="Seg_5709" s="T708">Dĭgəttə šonəga, šonəga, dĭn ularəʔi mĭlleʔbəʔjə. </ta>
            <ta e="T720" id="Seg_5710" s="T714">Dĭgəttə šobi pastuxtə:" Dön măn ular!" </ta>
            <ta e="T721" id="Seg_5711" s="T720">(Da). </ta>
            <ta e="T722" id="Seg_5712" s="T721">((BRK)). </ta>
            <ta e="T731" id="Seg_5713" s="T722">Onʼiʔ (m-) amnəlbi bünə (tol-) toʔndə, onʼiʔ amnəlbəbi dʼijegən. </ta>
            <ta e="T735" id="Seg_5714" s="T731">Dĭgəttə măllaʔbə:" Măn ular!" </ta>
            <ta e="T737" id="Seg_5715" s="T735">"No, surardə. </ta>
            <ta e="T740" id="Seg_5716" s="T737">(У одинного) surardə". </ta>
            <ta e="T742" id="Seg_5717" s="T740">Dĭ surarluʔpi. </ta>
            <ta e="T744" id="Seg_5718" s="T742">"Kodurən ularzaŋdə!" </ta>
            <ta e="T748" id="Seg_5719" s="T744">"Dĭgəttə dʼijegən kuzanə surardə!" </ta>
            <ta e="T753" id="Seg_5720" s="T748">Surarbi, dĭ măndə:" Kodurən ulardə!" </ta>
            <ta e="T757" id="Seg_5721" s="T753">Dĭgəttə (ibɨ) ibi ulardə. </ta>
            <ta e="T758" id="Seg_5722" s="T757">((BRK)). </ta>
            <ta e="T765" id="Seg_5723" s="T758">Bazoʔ (s-) šobi, kuza ineʔi (mĭn-) amnolaʔbə. </ta>
            <ta e="T767" id="Seg_5724" s="T765">"Măn ineʔi!" </ta>
            <ta e="T773" id="Seg_5725" s="T767">A dĭ măndə:" Dʼok, măn ineʔi!" </ta>
            <ta e="T777" id="Seg_5726" s="T773">"No, surardə bünə kuzam!" </ta>
            <ta e="T781" id="Seg_5727" s="T777">Dĭ surarbi:" Kodurən ineʔi!" </ta>
            <ta e="T785" id="Seg_5728" s="T781">"Dĭgəttə dʼijen kuzanə surardə!" </ta>
            <ta e="T789" id="Seg_5729" s="T785">Dĭ surarbi:" Kodurən ineʔi!" </ta>
            <ta e="T792" id="Seg_5730" s="T789">Dĭ (bar öʔluʔpi). </ta>
            <ta e="T795" id="Seg_5731" s="T792">Dĭgəttə tüžöjəʔi mĭlleʔbəʔjə. </ta>
            <ta e="T801" id="Seg_5732" s="T795">(I tĭn) tože kuza măndəlaʔbə tüžöjəʔi. </ta>
            <ta e="T805" id="Seg_5733" s="T801">Dĭ šobi:" Măn tüžöjəʔjə!" </ta>
            <ta e="T808" id="Seg_5734" s="T805">"Dʼok, ej tăn!" </ta>
            <ta e="T812" id="Seg_5735" s="T808">"No, suraraʔ bügən kuza!" </ta>
            <ta e="T816" id="Seg_5736" s="T812">Dĭgəttə dʼijegən dĭ surarbi. </ta>
            <ta e="T818" id="Seg_5737" s="T816">"Šindən tüžöjdə?" </ta>
            <ta e="T823" id="Seg_5738" s="T818">(Dĭ=) Dĭ mămbi:" Kodurən tüžöjdə!" </ta>
            <ta e="T828" id="Seg_5739" s="T823">I dʼijegən surarbi: "Šindən (tüžöjdə)?" </ta>
            <ta e="T830" id="Seg_5740" s="T828">"Kodurən tüžöjdə!" </ta>
            <ta e="T835" id="Seg_5741" s="T830">Dĭgəttə dĭ davaj nüjnəsʼtə nüjnə. </ta>
            <ta e="T840" id="Seg_5742" s="T835">"Măn lăpatka сухой (ibim-) ibiem. </ta>
            <ta e="T843" id="Seg_5743" s="T840">(Ula-) Ular ibiem. </ta>
            <ta e="T847" id="Seg_5744" s="T843">Ulardə šide ular ibiem. </ta>
            <ta e="T854" id="Seg_5745" s="T847">Šide ular ibiem, kambiam, nüke dʼügən ibiem. </ta>
            <ta e="T863" id="Seg_5746" s="T854">I šide ular i nüke dagaj (š- sĭjdə=) sĭjdə". </ta>
            <ta e="T864" id="Seg_5747" s="T863">((BRK)). </ta>
            <ta e="T868" id="Seg_5748" s="T864">Dagaj sĭjdə (mün-) müʔluʔpi. </ta>
            <ta e="T878" id="Seg_5749" s="T868">A bostə šobi:" Măn dĭn nükem ige i šide ular". </ta>
            <ta e="T884" id="Seg_5750" s="T878">I šide koʔbsaŋ i büzʼe amnolaʔbə. </ta>
            <ta e="T888" id="Seg_5751" s="T884">Dĭ bar büzʼe dʼăbaktəria… </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T3" id="Seg_5752" s="T2">tar-zittə</ta>
            <ta e="T4" id="Seg_5753" s="T3">măn</ta>
            <ta e="T5" id="Seg_5754" s="T4">dĭʔ-nə</ta>
            <ta e="T6" id="Seg_5755" s="T5">em-bie-m</ta>
            <ta e="T7" id="Seg_5756" s="T6">beške-ʔi</ta>
            <ta e="T8" id="Seg_5757" s="T7">kajak</ta>
            <ta e="T9" id="Seg_5758" s="T8">mĭ-bie-m</ta>
            <ta e="T10" id="Seg_5759" s="T9">aktʼa</ta>
            <ta e="T11" id="Seg_5760" s="T10">mĭ-bie-m</ta>
            <ta e="T12" id="Seg_5761" s="T11">oʔb</ta>
            <ta e="T13" id="Seg_5762" s="T12">šide</ta>
            <ta e="T14" id="Seg_5763" s="T13">nagur</ta>
            <ta e="T15" id="Seg_5764" s="T14">teʔtə</ta>
            <ta e="T16" id="Seg_5765" s="T15">dĭ-zeŋ</ta>
            <ta e="T17" id="Seg_5766" s="T16">tura</ta>
            <ta e="T18" id="Seg_5767" s="T17">i-bi-ʔi</ta>
            <ta e="T19" id="Seg_5768" s="T18">šindi=də</ta>
            <ta e="T20" id="Seg_5769" s="T19">bar</ta>
            <ta e="T21" id="Seg_5770" s="T20">sadar-laʔbə</ta>
            <ta e="T22" id="Seg_5771" s="T21">i</ta>
            <ta e="T23" id="Seg_5772" s="T22">aktʼa</ta>
            <ta e="T24" id="Seg_5773" s="T23">ej</ta>
            <ta e="T25" id="Seg_5774" s="T24">kabar-lia</ta>
            <ta e="T27" id="Seg_5775" s="T26">dĭ-zeŋ</ta>
            <ta e="T28" id="Seg_5776" s="T27">dĭ</ta>
            <ta e="T30" id="Seg_5777" s="T29">tura</ta>
            <ta e="T31" id="Seg_5778" s="T30">sadar-la</ta>
            <ta e="T32" id="Seg_5779" s="T31">i-bi-ʔi</ta>
            <ta e="T34" id="Seg_5780" s="T33">a</ta>
            <ta e="T35" id="Seg_5781" s="T34">girgit</ta>
            <ta e="T36" id="Seg_5782" s="T35">kuza-gən</ta>
            <ta e="T37" id="Seg_5783" s="T36">naga</ta>
            <ta e="T39" id="Seg_5784" s="T38">isaʔtə</ta>
            <ta e="T40" id="Seg_5785" s="T39">aktʼa</ta>
            <ta e="T41" id="Seg_5786" s="T40">mĭ-zittə</ta>
            <ta e="T42" id="Seg_5787" s="T41">tĭ-n</ta>
            <ta e="T43" id="Seg_5788" s="T42">tüžöj-də</ta>
            <ta e="T44" id="Seg_5789" s="T43">il</ta>
            <ta e="T45" id="Seg_5790" s="T44">i-bi-ʔi</ta>
            <ta e="T46" id="Seg_5791" s="T45">ĭmbi</ta>
            <ta e="T47" id="Seg_5792" s="T46">i-ge</ta>
            <ta e="T48" id="Seg_5793" s="T47">bar</ta>
            <ta e="T49" id="Seg_5794" s="T48">i-luʔ-pi-ʔi</ta>
            <ta e="T50" id="Seg_5795" s="T49">i</ta>
            <ta e="T51" id="Seg_5796" s="T50">mĭ-bi-ʔi</ta>
            <ta e="T52" id="Seg_5797" s="T51">aktʼa</ta>
            <ta e="T53" id="Seg_5798" s="T52">dĭbər</ta>
            <ta e="T55" id="Seg_5799" s="T54">măn</ta>
            <ta e="T56" id="Seg_5800" s="T55">tugan-bə</ta>
            <ta e="T892" id="Seg_5801" s="T56">kal-la</ta>
            <ta e="T57" id="Seg_5802" s="T892">dʼür-bi</ta>
            <ta e="T58" id="Seg_5803" s="T57">gorăt-tə</ta>
            <ta e="T59" id="Seg_5804" s="T58">i</ta>
            <ta e="T60" id="Seg_5805" s="T59">dĭn</ta>
            <ta e="T61" id="Seg_5806" s="T60">mašina</ta>
            <ta e="T62" id="Seg_5807" s="T61">i-lə-j</ta>
            <ta e="T63" id="Seg_5808" s="T62">oldʼa</ta>
            <ta e="T64" id="Seg_5809" s="T63">bazə-sʼtə</ta>
            <ta e="T65" id="Seg_5810" s="T64">kujnek</ta>
            <ta e="T66" id="Seg_5811" s="T65">kujneg-əʔi</ta>
            <ta e="T67" id="Seg_5812" s="T66">piʔme-ʔi</ta>
            <ta e="T68" id="Seg_5813" s="T67">bar</ta>
            <ta e="T70" id="Seg_5814" s="T69">ĭmbi</ta>
            <ta e="T71" id="Seg_5815" s="T70">i-ge</ta>
            <ta e="T72" id="Seg_5816" s="T71">bazə-sʼtə</ta>
            <ta e="T74" id="Seg_5817" s="T73">tibi</ta>
            <ta e="T75" id="Seg_5818" s="T74">i</ta>
            <ta e="T76" id="Seg_5819" s="T75">ne</ta>
            <ta e="T77" id="Seg_5820" s="T76">amno-bi-ʔi</ta>
            <ta e="T78" id="Seg_5821" s="T77">ugandə</ta>
            <ta e="T79" id="Seg_5822" s="T78">jakše</ta>
            <ta e="T80" id="Seg_5823" s="T79">ajir-bi-ʔi</ta>
            <ta e="T83" id="Seg_5824" s="T81">onʼiʔ</ta>
            <ta e="T84" id="Seg_5825" s="T83">onʼiʔ</ta>
            <ta e="T85" id="Seg_5826" s="T84">onʼiʔ-tə</ta>
            <ta e="T86" id="Seg_5827" s="T85">ajir-bi-ʔi</ta>
            <ta e="T87" id="Seg_5828" s="T86">bar</ta>
            <ta e="T88" id="Seg_5829" s="T87">kamro-laʔtə-ʔi</ta>
            <ta e="T89" id="Seg_5830" s="T88">panar-laʔbə-ʔjə</ta>
            <ta e="T90" id="Seg_5831" s="T89">i</ta>
            <ta e="T91" id="Seg_5832" s="T90">dĭ-zeŋ</ta>
            <ta e="T92" id="Seg_5833" s="T91">es-seŋ-də</ta>
            <ta e="T93" id="Seg_5834" s="T92">i-bi-ʔi</ta>
            <ta e="T94" id="Seg_5835" s="T93">ugandə</ta>
            <ta e="T95" id="Seg_5836" s="T94">es-seŋ-də</ta>
            <ta e="T96" id="Seg_5837" s="T95">jakše</ta>
            <ta e="T97" id="Seg_5838" s="T96">ĭmbi</ta>
            <ta e="T98" id="Seg_5839" s="T97">măl-la-l</ta>
            <ta e="T99" id="Seg_5840" s="T98">dĭ-zeŋ</ta>
            <ta e="T100" id="Seg_5841" s="T99">nʼilgö-laʔbə-ʔjə</ta>
            <ta e="T102" id="Seg_5842" s="T101">a</ta>
            <ta e="T103" id="Seg_5843" s="T102">miʔ</ta>
            <ta e="T104" id="Seg_5844" s="T103">es-seŋ</ta>
            <ta e="T105" id="Seg_5845" s="T104">ugandə</ta>
            <ta e="T106" id="Seg_5846" s="T105">ej</ta>
            <ta e="T107" id="Seg_5847" s="T106">jakše</ta>
            <ta e="T108" id="Seg_5848" s="T107">dʼabro-laʔbə-ʔjə</ta>
            <ta e="T109" id="Seg_5849" s="T108">kudo-nz-laʔbə-ʔjə</ta>
            <ta e="T110" id="Seg_5850" s="T109">ej</ta>
            <ta e="T111" id="Seg_5851" s="T110">nʼilgö-laʔbə-ʔjə</ta>
            <ta e="T112" id="Seg_5852" s="T111">măn</ta>
            <ta e="T113" id="Seg_5853" s="T112">dĭ-zem</ta>
            <ta e="T114" id="Seg_5854" s="T113">bar</ta>
            <ta e="T115" id="Seg_5855" s="T114">münör-laʔbə-m</ta>
            <ta e="T117" id="Seg_5856" s="T116">es-seŋ-bə</ta>
            <ta e="T118" id="Seg_5857" s="T117">tüšə-l-lie-m</ta>
            <ta e="T119" id="Seg_5858" s="T118">sazən</ta>
            <ta e="T120" id="Seg_5859" s="T119">pʼaŋdə-sʼtə</ta>
            <ta e="T122" id="Seg_5860" s="T121">nu-zaŋ</ta>
            <ta e="T123" id="Seg_5861" s="T122">amno-bi-ʔi</ta>
            <ta e="T124" id="Seg_5862" s="T123">šindi</ta>
            <ta e="T125" id="Seg_5863" s="T124">ugandə</ta>
            <ta e="T126" id="Seg_5864" s="T125">aktʼa</ta>
            <ta e="T127" id="Seg_5865" s="T126">iʔgö</ta>
            <ta e="T128" id="Seg_5866" s="T127">a</ta>
            <ta e="T129" id="Seg_5867" s="T128">šindi-n</ta>
            <ta e="T130" id="Seg_5868" s="T129">naga</ta>
            <ta e="T131" id="Seg_5869" s="T130">dĭ</ta>
            <ta e="T132" id="Seg_5870" s="T131">ara</ta>
            <ta e="T133" id="Seg_5871" s="T132">bĭt-lie</ta>
            <ta e="T134" id="Seg_5872" s="T133">bar</ta>
            <ta e="T135" id="Seg_5873" s="T134">aktʼa</ta>
            <ta e="T136" id="Seg_5874" s="T135">bĭt-luʔ-lə-j</ta>
            <ta e="T137" id="Seg_5875" s="T136">dĭgəttə</ta>
            <ta e="T138" id="Seg_5876" s="T137">ĭmbi=də</ta>
            <ta e="T139" id="Seg_5877" s="T138">naga</ta>
            <ta e="T140" id="Seg_5878" s="T139">togonor-ia</ta>
            <ta e="T141" id="Seg_5879" s="T140">togonor-ia</ta>
            <ta e="T142" id="Seg_5880" s="T141">bar</ta>
            <ta e="T143" id="Seg_5881" s="T142">bĭʔ-luʔ-lə-j</ta>
            <ta e="T146" id="Seg_5882" s="T145">Sibirʼ-gən</ta>
            <ta e="T147" id="Seg_5883" s="T146">ugandə</ta>
            <ta e="T148" id="Seg_5884" s="T147">iʔgö</ta>
            <ta e="T149" id="Seg_5885" s="T148">nu-zaŋ</ta>
            <ta e="T150" id="Seg_5886" s="T149">amno-bi-ʔi</ta>
            <ta e="T151" id="Seg_5887" s="T150">dĭgəttə</ta>
            <ta e="T152" id="Seg_5888" s="T151">sĭre</ta>
            <ta e="T153" id="Seg_5889" s="T152">pa</ta>
            <ta e="T154" id="Seg_5890" s="T153">özer-luʔ-pi</ta>
            <ta e="T155" id="Seg_5891" s="T154">dĭ-zeŋ</ta>
            <ta e="T156" id="Seg_5892" s="T155">măl-lia-ʔi</ta>
            <ta e="T157" id="Seg_5893" s="T156">nu</ta>
            <ta e="T159" id="Seg_5894" s="T158">kazak</ta>
            <ta e="T160" id="Seg_5895" s="T159">il</ta>
            <ta e="T161" id="Seg_5896" s="T160">šo-lə-ʔjə</ta>
            <ta e="T162" id="Seg_5897" s="T161">miʔnʼibeʔ</ta>
            <ta e="T163" id="Seg_5898" s="T162">ej</ta>
            <ta e="T164" id="Seg_5899" s="T163">jakše</ta>
            <ta e="T165" id="Seg_5900" s="T164">mo-lə-j</ta>
            <ta e="T166" id="Seg_5901" s="T165">dĭgəttə</ta>
            <ta e="T167" id="Seg_5902" s="T166">labaz-əʔi</ta>
            <ta e="T168" id="Seg_5903" s="T167">a-bi-ʔi</ta>
            <ta e="T169" id="Seg_5904" s="T168">dĭbər</ta>
            <ta e="T170" id="Seg_5905" s="T169">pi-ʔi</ta>
            <ta e="T171" id="Seg_5906" s="T170">tažer-bi-ʔi</ta>
            <ta e="T172" id="Seg_5907" s="T171">i</ta>
            <ta e="T173" id="Seg_5908" s="T172">baltu</ta>
            <ta e="T174" id="Seg_5909" s="T173">i-bi-ʔi</ta>
            <ta e="T175" id="Seg_5910" s="T174">jaʔ-pi-ʔi</ta>
            <ta e="T176" id="Seg_5911" s="T175">i</ta>
            <ta e="T177" id="Seg_5912" s="T176">dĭ</ta>
            <ta e="T178" id="Seg_5913" s="T177">saʔmə-luʔ-pi</ta>
            <ta e="T179" id="Seg_5914" s="T178">dĭ-zem</ta>
            <ta e="T180" id="Seg_5915" s="T179">dĭ-zeŋ</ta>
            <ta e="T181" id="Seg_5916" s="T180">bar</ta>
            <ta e="T182" id="Seg_5917" s="T181">dĭn</ta>
            <ta e="T183" id="Seg_5918" s="T182">kü-bi-ʔi</ta>
            <ta e="T891" id="Seg_5919" s="T184">lutʼšə</ta>
            <ta e="T185" id="Seg_5920" s="T891">măna</ta>
            <ta e="T186" id="Seg_5921" s="T185">măn</ta>
            <ta e="T187" id="Seg_5922" s="T186">bɨ</ta>
            <ta e="T188" id="Seg_5923" s="T187">kü-bie-m</ta>
            <ta e="T190" id="Seg_5924" s="T189">es-seŋ-bə</ta>
            <ta e="T192" id="Seg_5925" s="T190">dĭrgit</ta>
            <ta e="T193" id="Seg_5926" s="T192">ej</ta>
            <ta e="T194" id="Seg_5927" s="T193">jakše-ʔi</ta>
            <ta e="T195" id="Seg_5928" s="T194">bar</ta>
            <ta e="T196" id="Seg_5929" s="T195">tajir-laʔbə-ʔjə</ta>
            <ta e="T197" id="Seg_5930" s="T196">il-zi</ta>
            <ta e="T198" id="Seg_5931" s="T197">dʼabro-laʔbə-ʔjə</ta>
            <ta e="T200" id="Seg_5932" s="T199">bar</ta>
            <ta e="T201" id="Seg_5933" s="T200">ara</ta>
            <ta e="T202" id="Seg_5934" s="T201">bĭt-leʔbə-ʔjə</ta>
            <ta e="T203" id="Seg_5935" s="T202">ažnə</ta>
            <ta e="T204" id="Seg_5936" s="T203">sima-zi</ta>
            <ta e="T205" id="Seg_5937" s="T204">ej</ta>
            <ta e="T206" id="Seg_5938" s="T205">mo-lia</ta>
            <ta e="T207" id="Seg_5939" s="T206">măndə-r-zittə</ta>
            <ta e="T208" id="Seg_5940" s="T207">dĭ-zeŋ-də</ta>
            <ta e="T210" id="Seg_5941" s="T209">onʼiʔ</ta>
            <ta e="T211" id="Seg_5942" s="T210">ne-n</ta>
            <ta e="T212" id="Seg_5943" s="T211">nagur</ta>
            <ta e="T213" id="Seg_5944" s="T212">nʼi</ta>
            <ta e="T214" id="Seg_5945" s="T213">i-bi</ta>
            <ta e="T215" id="Seg_5946" s="T214">šide</ta>
            <ta e="T216" id="Seg_5947" s="T215">jakše</ta>
            <ta e="T217" id="Seg_5948" s="T216">a</ta>
            <ta e="T218" id="Seg_5949" s="T217">onʼiʔ</ta>
            <ta e="T219" id="Seg_5950" s="T218">ej</ta>
            <ta e="T220" id="Seg_5951" s="T219">jakše</ta>
            <ta e="T221" id="Seg_5952" s="T220">bar</ta>
            <ta e="T222" id="Seg_5953" s="T221">dĭ-m</ta>
            <ta e="T223" id="Seg_5954" s="T222">münör-bi-ʔi</ta>
            <ta e="T224" id="Seg_5955" s="T223">üjü-zi</ta>
            <ta e="T225" id="Seg_5956" s="T224">bar</ta>
            <ta e="T226" id="Seg_5957" s="T225">i</ta>
            <ta e="T228" id="Seg_5958" s="T227">muzuruk-si</ta>
            <ta e="T229" id="Seg_5959" s="T228">i</ta>
            <ta e="T230" id="Seg_5960" s="T229">pa-zi</ta>
            <ta e="T231" id="Seg_5961" s="T230">bar</ta>
            <ta e="T234" id="Seg_5962" s="T233">toʔ-nar-bi-ʔi</ta>
            <ta e="T235" id="Seg_5963" s="T234">a</ta>
            <ta e="T236" id="Seg_5964" s="T235">măn</ta>
            <ta e="T237" id="Seg_5965" s="T236">u</ta>
            <ta e="T239" id="Seg_5966" s="T237">măm-bia-m</ta>
            <ta e="T241" id="Seg_5967" s="T240">a</ta>
            <ta e="T242" id="Seg_5968" s="T241">măn</ta>
            <ta e="T243" id="Seg_5969" s="T242">u</ta>
            <ta e="T244" id="Seg_5970" s="T243">măm-bia-m</ta>
            <ta e="T245" id="Seg_5971" s="T244">i-ʔ</ta>
            <ta e="T246" id="Seg_5972" s="T245">üžüŋge-ʔ</ta>
            <ta e="T247" id="Seg_5973" s="T246">možet</ta>
            <ta e="T248" id="Seg_5974" s="T247">özer-lə-j</ta>
            <ta e="T249" id="Seg_5975" s="T248">sagəštə</ta>
            <ta e="T250" id="Seg_5976" s="T249">iʔgö</ta>
            <ta e="T251" id="Seg_5977" s="T250">mo-lə-j</ta>
            <ta e="T252" id="Seg_5978" s="T251">urgo</ta>
            <ta e="T253" id="Seg_5979" s="T252">mo-lə-j</ta>
            <ta e="T254" id="Seg_5980" s="T253">možet</ta>
            <ta e="T255" id="Seg_5981" s="T254">bar</ta>
            <ta e="T256" id="Seg_5982" s="T255">baruʔ-lə-j</ta>
            <ta e="T257" id="Seg_5983" s="T256">dĭgəttə</ta>
            <ta e="T258" id="Seg_5984" s="T257">jakše</ta>
            <ta e="T259" id="Seg_5985" s="T258">mo-lə-j</ta>
            <ta e="T261" id="Seg_5986" s="T260">a</ta>
            <ta e="T262" id="Seg_5987" s="T261">dĭ</ta>
            <ta e="T263" id="Seg_5988" s="T262">girgit</ta>
            <ta e="T264" id="Seg_5989" s="T263">üdʼüge</ta>
            <ta e="T265" id="Seg_5990" s="T264">i-bi</ta>
            <ta e="T266" id="Seg_5991" s="T265">i</ta>
            <ta e="T267" id="Seg_5992" s="T266">urgo</ta>
            <ta e="T269" id="Seg_5993" s="T268">dĭrgit</ta>
            <ta e="T270" id="Seg_5994" s="T269">že</ta>
            <ta e="T271" id="Seg_5995" s="T270">ĭmbi=də</ta>
            <ta e="T272" id="Seg_5996" s="T271">naga</ta>
            <ta e="T273" id="Seg_5997" s="T272">jakše</ta>
            <ta e="T275" id="Seg_5998" s="T274">dĭ</ta>
            <ta e="T276" id="Seg_5999" s="T275">armija-nə</ta>
            <ta e="T277" id="Seg_6000" s="T276">kam-bi</ta>
            <ta e="T278" id="Seg_6001" s="T277">vsʼoravno</ta>
            <ta e="T279" id="Seg_6002" s="T278">šo-bi</ta>
            <ta e="T280" id="Seg_6003" s="T279">gibər</ta>
            <ta e="T281" id="Seg_6004" s="T280">togonor-zittə</ta>
            <ta e="T282" id="Seg_6005" s="T281">ka-lə-j</ta>
            <ta e="T283" id="Seg_6006" s="T282">dĭ-m</ta>
            <ta e="T284" id="Seg_6007" s="T283">bar</ta>
            <ta e="T285" id="Seg_6008" s="T284">sürer-leʔbə-ʔjə</ta>
            <ta e="T287" id="Seg_6009" s="T286">onʼiʔ</ta>
            <ta e="T288" id="Seg_6010" s="T287">nüke</ta>
            <ta e="T289" id="Seg_6011" s="T288">nʼi-t</ta>
            <ta e="T290" id="Seg_6012" s="T289">i-bi</ta>
            <ta e="T291" id="Seg_6013" s="T290">bar</ta>
            <ta e="T292" id="Seg_6014" s="T291">jezerik</ta>
            <ta e="T293" id="Seg_6015" s="T292">tăn</ta>
            <ta e="T294" id="Seg_6016" s="T293">ĭmbi</ta>
            <ta e="T295" id="Seg_6017" s="T294">măna</ta>
            <ta e="T296" id="Seg_6018" s="T295">ej</ta>
            <ta e="T297" id="Seg_6019" s="T296">tüšə-l-bie-l</ta>
            <ta e="T298" id="Seg_6020" s="T297">a</ta>
            <ta e="T299" id="Seg_6021" s="T298">dĭ</ta>
            <ta e="T300" id="Seg_6022" s="T299">măm-bi</ta>
            <ta e="T301" id="Seg_6023" s="T300">măn</ta>
            <ta e="T302" id="Seg_6024" s="T301">tüšə-l-bie-m</ta>
            <ta e="T303" id="Seg_6025" s="T302">tănan</ta>
            <ta e="T304" id="Seg_6026" s="T303">ĭmbi</ta>
            <ta e="T305" id="Seg_6027" s="T304">kereʔ</ta>
            <ta e="T306" id="Seg_6028" s="T305">tăn</ta>
            <ta e="T307" id="Seg_6029" s="T306">tănan</ta>
            <ta e="T308" id="Seg_6030" s="T307">tăn</ta>
            <ta e="T310" id="Seg_6031" s="T309">i-bie-l</ta>
            <ta e="T311" id="Seg_6032" s="T310">a</ta>
            <ta e="T312" id="Seg_6033" s="T311">dĭ</ta>
            <ta e="T313" id="Seg_6034" s="T312">bar</ta>
            <ta e="T315" id="Seg_6035" s="T314">ara</ta>
            <ta e="T316" id="Seg_6036" s="T315">bĭt-lie-l</ta>
            <ta e="T317" id="Seg_6037" s="T316">a</ta>
            <ta e="T318" id="Seg_6038" s="T317">dĭ</ta>
            <ta e="T319" id="Seg_6039" s="T318">bar</ta>
            <ta e="T321" id="Seg_6040" s="T319">muzuruk-si=da</ta>
            <ta e="T322" id="Seg_6041" s="T321">ia-t</ta>
            <ta e="T323" id="Seg_6042" s="T322">xatʼel</ta>
            <ta e="T324" id="Seg_6043" s="T323">münör-zittə</ta>
            <ta e="T325" id="Seg_6044" s="T324">a</ta>
            <ta e="T326" id="Seg_6045" s="T325">il</ta>
            <ta e="T327" id="Seg_6046" s="T326">ej</ta>
            <ta e="T328" id="Seg_6047" s="T327">mĭ-bi-ʔi</ta>
            <ta e="T330" id="Seg_6048" s="T329">măn</ta>
            <ta e="T331" id="Seg_6049" s="T330">kaga-m</ta>
            <ta e="T332" id="Seg_6050" s="T331">văjna-gən</ta>
            <ta e="T333" id="Seg_6051" s="T332">i-bie-l</ta>
            <ta e="T334" id="Seg_6052" s="T333">i-bie</ta>
            <ta e="T335" id="Seg_6053" s="T334">i</ta>
            <ta e="T336" id="Seg_6054" s="T335">dĭ-m</ta>
            <ta e="T337" id="Seg_6055" s="T336">i-bi-ʔi</ta>
            <ta e="T338" id="Seg_6056" s="T337">plen-də</ta>
            <ta e="T339" id="Seg_6057" s="T338">i</ta>
            <ta e="T340" id="Seg_6058" s="T339">pravăj</ta>
            <ta e="T341" id="Seg_6059" s="T340">uda-t</ta>
            <ta e="T342" id="Seg_6060" s="T341">müjö-t-si</ta>
            <ta e="T344" id="Seg_6061" s="T343">dʼagar-zittə</ta>
            <ta e="T345" id="Seg_6062" s="T344">xatʼeli</ta>
            <ta e="T346" id="Seg_6063" s="T345">a</ta>
            <ta e="T347" id="Seg_6064" s="T346">dĭ</ta>
            <ta e="T348" id="Seg_6065" s="T347">bar</ta>
            <ta e="T349" id="Seg_6066" s="T348">dʼor-bi</ta>
            <ta e="T350" id="Seg_6067" s="T349">šide</ta>
            <ta e="T351" id="Seg_6068" s="T350">müjə</ta>
            <ta e="T353" id="Seg_6069" s="T352">maː-tə-bi-ʔi</ta>
            <ta e="T355" id="Seg_6070" s="T354">dĭʔ-nə</ta>
            <ta e="T357" id="Seg_6071" s="T356">măn</ta>
            <ta e="T358" id="Seg_6072" s="T357">u</ta>
            <ta e="T359" id="Seg_6073" s="T358">kaga-m</ta>
            <ta e="T360" id="Seg_6074" s="T359">nagur</ta>
            <ta e="T361" id="Seg_6075" s="T360">koʔbdo</ta>
            <ta e="T362" id="Seg_6076" s="T361">i</ta>
            <ta e="T363" id="Seg_6077" s="T362">nagur</ta>
            <ta e="T364" id="Seg_6078" s="T363">nʼi</ta>
            <ta e="T366" id="Seg_6079" s="T365">nörbə-zittə</ta>
            <ta e="T367" id="Seg_6080" s="T366">tănan</ta>
            <ta e="T368" id="Seg_6081" s="T367">sĭre</ta>
            <ta e="T369" id="Seg_6082" s="T368">sĭre</ta>
            <ta e="T370" id="Seg_6083" s="T369">buga</ta>
            <ta e="T372" id="Seg_6084" s="T371">tăn</ta>
            <ta e="T373" id="Seg_6085" s="T372">nörbi-t</ta>
            <ta e="T374" id="Seg_6086" s="T373">măn</ta>
            <ta e="T375" id="Seg_6087" s="T374">nörbi-t</ta>
            <ta e="T376" id="Seg_6088" s="T375">măn-zittə</ta>
            <ta e="T377" id="Seg_6089" s="T376">tănan</ta>
            <ta e="T378" id="Seg_6090" s="T377">kömə</ta>
            <ta e="T379" id="Seg_6091" s="T378">buga</ta>
            <ta e="T381" id="Seg_6092" s="T380">tăn</ta>
            <ta e="T382" id="Seg_6093" s="T381">nörbi-t</ta>
            <ta e="T383" id="Seg_6094" s="T382">măn</ta>
            <ta e="T384" id="Seg_6095" s="T383">nörbi-t</ta>
            <ta e="T385" id="Seg_6096" s="T384">măn-zittə</ta>
            <ta e="T386" id="Seg_6097" s="T385">tănan</ta>
            <ta e="T387" id="Seg_6098" s="T386">sagər</ta>
            <ta e="T388" id="Seg_6099" s="T387">buga</ta>
            <ta e="T389" id="Seg_6100" s="T388">Kodur</ta>
            <ta e="T390" id="Seg_6101" s="T389">Kodur</ta>
            <ta e="T391" id="Seg_6102" s="T390">kömə</ta>
            <ta e="T392" id="Seg_6103" s="T391">părga</ta>
            <ta e="T393" id="Seg_6104" s="T392">šo-bi</ta>
            <ta e="T394" id="Seg_6105" s="T393">dön</ta>
            <ta e="T395" id="Seg_6106" s="T394">il</ta>
            <ta e="T396" id="Seg_6107" s="T395">amno-bi-ʔi</ta>
            <ta e="T397" id="Seg_6108" s="T396">dĭ</ta>
            <ta e="T398" id="Seg_6109" s="T397">lăpatka</ta>
            <ta e="T399" id="Seg_6110" s="T398">ku-bi</ta>
            <ta e="T400" id="Seg_6111" s="T399">uja</ta>
            <ta e="T401" id="Seg_6112" s="T400">naga</ta>
            <ta e="T402" id="Seg_6113" s="T401">dĭgəttə</ta>
            <ta e="T403" id="Seg_6114" s="T402">kam-bi</ta>
            <ta e="T404" id="Seg_6115" s="T403">kam-bi</ta>
            <ta e="T405" id="Seg_6116" s="T404">nu</ta>
            <ta e="T406" id="Seg_6117" s="T405">dĭn</ta>
            <ta e="T407" id="Seg_6118" s="T406">onʼiʔ</ta>
            <ta e="T408" id="Seg_6119" s="T407">kuza</ta>
            <ta e="T409" id="Seg_6120" s="T408">amno-bi</ta>
            <ta e="T410" id="Seg_6121" s="T409">ne-ziʔ</ta>
            <ta e="T411" id="Seg_6122" s="T410">dĭ</ta>
            <ta e="T412" id="Seg_6123" s="T411">šo-bi</ta>
            <ta e="T413" id="Seg_6124" s="T412">dĭ-zeŋ</ta>
            <ta e="T414" id="Seg_6125" s="T413">dĭ-zeŋ-də</ta>
            <ta e="T415" id="Seg_6126" s="T414">ular</ta>
            <ta e="T416" id="Seg_6127" s="T415">i-bi</ta>
            <ta e="T417" id="Seg_6128" s="T416">onʼiʔ</ta>
            <ta e="T418" id="Seg_6129" s="T417">dĭ</ta>
            <ta e="T419" id="Seg_6130" s="T418">măn-də</ta>
            <ta e="T420" id="Seg_6131" s="T419">nada</ta>
            <ta e="T421" id="Seg_6132" s="T420">segi</ta>
            <ta e="T422" id="Seg_6133" s="T421">bü</ta>
            <ta e="T423" id="Seg_6134" s="T422">mĭnzər-zittə</ta>
            <ta e="T424" id="Seg_6135" s="T423">kuza-m</ta>
            <ta e="T426" id="Seg_6136" s="T425">bădə-sʼtə</ta>
            <ta e="T427" id="Seg_6137" s="T426">a</ta>
            <ta e="T428" id="Seg_6138" s="T427">dĭ</ta>
            <ta e="T429" id="Seg_6139" s="T428">măn-də</ta>
            <ta e="T430" id="Seg_6140" s="T429">i-ʔ</ta>
            <ta e="T431" id="Seg_6141" s="T430">mĭnzər-e-ʔ</ta>
            <ta e="T432" id="Seg_6142" s="T431">măn</ta>
            <ta e="T433" id="Seg_6143" s="T432">uja-m</ta>
            <ta e="T434" id="Seg_6144" s="T433">i-ge</ta>
            <ta e="T435" id="Seg_6145" s="T434">dĭgəttə</ta>
            <ta e="T436" id="Seg_6146" s="T435">aspaʔ</ta>
            <ta e="T437" id="Seg_6147" s="T436">edə-bi-ʔi</ta>
            <ta e="T438" id="Seg_6148" s="T437">tus</ta>
            <ta e="T439" id="Seg_6149" s="T438">em-bi-ʔi</ta>
            <ta e="T440" id="Seg_6150" s="T439">dĭ</ta>
            <ta e="T442" id="Seg_6151" s="T441">kam-bi</ta>
            <ta e="T443" id="Seg_6152" s="T442">püje</ta>
            <ta e="T444" id="Seg_6153" s="T443">püje-t</ta>
            <ta e="T445" id="Seg_6154" s="T444">toʔ-nar-bi</ta>
            <ta e="T446" id="Seg_6155" s="T445">kem-ziʔ</ta>
            <ta e="T447" id="Seg_6156" s="T446">kem-ziʔ</ta>
            <ta e="T449" id="Seg_6157" s="T447">lăpatka-m</ta>
            <ta e="T450" id="Seg_6158" s="T449">dĭgəttə</ta>
            <ta e="T451" id="Seg_6159" s="T450">aspaʔ-də</ta>
            <ta e="T452" id="Seg_6160" s="T451">barəʔ-luʔ-bi</ta>
            <ta e="T453" id="Seg_6161" s="T452">mĭnzər-bi</ta>
            <ta e="T454" id="Seg_6162" s="T453">mĭnzər-bi</ta>
            <ta e="T455" id="Seg_6163" s="T454">dĭgəttə</ta>
            <ta e="T456" id="Seg_6164" s="T455">uja</ta>
            <ta e="T457" id="Seg_6165" s="T456">naga</ta>
            <ta e="T458" id="Seg_6166" s="T457">onʼiʔ</ta>
            <ta e="T459" id="Seg_6167" s="T458">lăpatka</ta>
            <ta e="T460" id="Seg_6168" s="T459">dĭ</ta>
            <ta e="T461" id="Seg_6169" s="T460">bar</ta>
            <ta e="T463" id="Seg_6170" s="T462">saʔmə-luʔ-bi</ta>
            <ta e="T464" id="Seg_6171" s="T463">davaj</ta>
            <ta e="T465" id="Seg_6172" s="T464">dʼor-zittə</ta>
            <ta e="T466" id="Seg_6173" s="T465">a</ta>
            <ta e="T467" id="Seg_6174" s="T466">dĭ-zeŋ</ta>
            <ta e="T470" id="Seg_6175" s="T469">măm-bi-ʔi</ta>
            <ta e="T471" id="Seg_6176" s="T470">tüjö</ta>
            <ta e="T472" id="Seg_6177" s="T471">ular</ta>
            <ta e="T473" id="Seg_6178" s="T472">dʼăgar-li-m</ta>
            <ta e="T474" id="Seg_6179" s="T473">tănan</ta>
            <ta e="T475" id="Seg_6180" s="T474">lăpatka-m</ta>
            <ta e="T476" id="Seg_6181" s="T475">mĭm-bie-m</ta>
            <ta e="T477" id="Seg_6182" s="T476">măna</ta>
            <ta e="T478" id="Seg_6183" s="T477">ej</ta>
            <ta e="T479" id="Seg_6184" s="T478">kereʔ</ta>
            <ta e="T480" id="Seg_6185" s="T479">lăpatka</ta>
            <ta e="T481" id="Seg_6186" s="T480">dĭgəttə</ta>
            <ta e="T482" id="Seg_6187" s="T481">dĭʔ-nə</ta>
            <ta e="T483" id="Seg_6188" s="T482">ular</ta>
            <ta e="T484" id="Seg_6189" s="T483">mĭ-bi-ʔi</ta>
            <ta e="T486" id="Seg_6190" s="T485">dĭgəttə</ta>
            <ta e="T487" id="Seg_6191" s="T486">dĭ</ta>
            <ta e="T488" id="Seg_6192" s="T487">Kodur</ta>
            <ta e="T490" id="Seg_6193" s="T489">suʔmi-luʔ-pi</ta>
            <ta e="T491" id="Seg_6194" s="T490">băzə-jdə-bi</ta>
            <ta e="T492" id="Seg_6195" s="T491">kĭškə-bi</ta>
            <ta e="T493" id="Seg_6196" s="T492">sima-t</ta>
            <ta e="T494" id="Seg_6197" s="T493">ular-də</ta>
            <ta e="T495" id="Seg_6198" s="T494">i-bi</ta>
            <ta e="T496" id="Seg_6199" s="T495">kam-bi</ta>
            <ta e="T497" id="Seg_6200" s="T496">dĭgəttə</ta>
            <ta e="T499" id="Seg_6201" s="T498">il</ta>
            <ta e="T500" id="Seg_6202" s="T499">il-də</ta>
            <ta e="T501" id="Seg_6203" s="T500">šo-bi</ta>
            <ta e="T502" id="Seg_6204" s="T501">dĭn</ta>
            <ta e="T503" id="Seg_6205" s="T502">tože</ta>
            <ta e="T504" id="Seg_6206" s="T503">šide</ta>
            <ta e="T505" id="Seg_6207" s="T504">ular</ta>
            <ta e="T506" id="Seg_6208" s="T505">nʼi</ta>
            <ta e="T507" id="Seg_6209" s="T506">i-bi</ta>
            <ta e="T508" id="Seg_6210" s="T507">i</ta>
            <ta e="T509" id="Seg_6211" s="T508">nʼi-n</ta>
            <ta e="T510" id="Seg_6212" s="T509">ne</ta>
            <ta e="T511" id="Seg_6213" s="T510">i-bi</ta>
            <ta e="T512" id="Seg_6214" s="T511">dĭgəttə</ta>
            <ta e="T513" id="Seg_6215" s="T512">măl-lia-ʔi</ta>
            <ta e="T514" id="Seg_6216" s="T513">öʔli-t</ta>
            <ta e="T515" id="Seg_6217" s="T514">tăn</ta>
            <ta e="T516" id="Seg_6218" s="T515">ular</ta>
            <ta e="T517" id="Seg_6219" s="T516">miʔ</ta>
            <ta e="T518" id="Seg_6220" s="T517">ular-ziʔ</ta>
            <ta e="T519" id="Seg_6221" s="T518">dʼok</ta>
            <ta e="T520" id="Seg_6222" s="T519">ej</ta>
            <ta e="T521" id="Seg_6223" s="T520">öʔ-li-m</ta>
            <ta e="T522" id="Seg_6224" s="T521">šiʔ</ta>
            <ta e="T523" id="Seg_6225" s="T522">ular</ta>
            <ta e="T524" id="Seg_6226" s="T523">măn</ta>
            <ta e="T525" id="Seg_6227" s="T524">ular-bə</ta>
            <ta e="T526" id="Seg_6228" s="T525">am-nuʔbə-ʔjə</ta>
            <ta e="T527" id="Seg_6229" s="T526">a</ta>
            <ta e="T528" id="Seg_6230" s="T527">bos-tə</ta>
            <ta e="T530" id="Seg_6231" s="T529">dʼăbaktər-ia</ta>
            <ta e="T531" id="Seg_6232" s="T530">dʼăbaktər-ia</ta>
            <ta e="T532" id="Seg_6233" s="T531">dĭ</ta>
            <ta e="T534" id="Seg_6234" s="T533">nuʔmə-luʔ-pi</ta>
            <ta e="T535" id="Seg_6235" s="T534">da</ta>
            <ta e="T536" id="Seg_6236" s="T535">ular-də</ta>
            <ta e="T537" id="Seg_6237" s="T536">öʔlə-bi</ta>
            <ta e="T538" id="Seg_6238" s="T537">dĭʔ-nə</ta>
            <ta e="T539" id="Seg_6239" s="T538">ular-də</ta>
            <ta e="T540" id="Seg_6240" s="T539">a</ta>
            <ta e="T541" id="Seg_6241" s="T540">dĭgəttə</ta>
            <ta e="T542" id="Seg_6242" s="T541">iʔ-pi-ʔi</ta>
            <ta e="T543" id="Seg_6243" s="T542">kunol-zittə</ta>
            <ta e="T544" id="Seg_6244" s="T543">ertə-n</ta>
            <ta e="T545" id="Seg_6245" s="T544">uʔ-pi-ʔi</ta>
            <ta e="T546" id="Seg_6246" s="T545">a</ta>
            <ta e="T547" id="Seg_6247" s="T546">ular</ta>
            <ta e="T548" id="Seg_6248" s="T547">naga</ta>
            <ta e="T550" id="Seg_6249" s="T549">dĭ</ta>
            <ta e="T551" id="Seg_6250" s="T550">Kodur</ta>
            <ta e="T552" id="Seg_6251" s="T551">uʔbdə-bi</ta>
            <ta e="T553" id="Seg_6252" s="T552">ular-də</ta>
            <ta e="T554" id="Seg_6253" s="T553">saj-nʼeʔ-pi</ta>
            <ta e="T555" id="Seg_6254" s="T554">i</ta>
            <ta e="T556" id="Seg_6255" s="T555">dĭ</ta>
            <ta e="T557" id="Seg_6256" s="T556">dĭ-zen</ta>
            <ta e="T558" id="Seg_6257" s="T557">ular-də</ta>
            <ta e="T559" id="Seg_6258" s="T558">bar</ta>
            <ta e="T560" id="Seg_6259" s="T559">kem-ziʔ</ta>
            <ta e="T562" id="Seg_6260" s="T561">kem-ziʔ</ta>
            <ta e="T563" id="Seg_6261" s="T562">bar</ta>
            <ta e="T564" id="Seg_6262" s="T563">dʼüʔ-də-bi</ta>
            <ta e="T565" id="Seg_6263" s="T564">püje-t</ta>
            <ta e="T566" id="Seg_6264" s="T565">i</ta>
            <ta e="T567" id="Seg_6265" s="T566">aŋ-də</ta>
            <ta e="T568" id="Seg_6266" s="T567">a</ta>
            <ta e="T569" id="Seg_6267" s="T568">ertə-n</ta>
            <ta e="T570" id="Seg_6268" s="T569">uʔbdə-bi-ʔi</ta>
            <ta e="T571" id="Seg_6269" s="T570">măn-də-ʔi</ta>
            <ta e="T572" id="Seg_6270" s="T571">šănap</ta>
            <ta e="T574" id="Seg_6271" s="T573">miʔ</ta>
            <ta e="T575" id="Seg_6272" s="T574">ular-də</ta>
            <ta e="T576" id="Seg_6273" s="T575">dĭ</ta>
            <ta e="T577" id="Seg_6274" s="T576">dĭ-m</ta>
            <ta e="T578" id="Seg_6275" s="T577">ular-də</ta>
            <ta e="T579" id="Seg_6276" s="T578">bar</ta>
            <ta e="T580" id="Seg_6277" s="T579">saj-nüžəʔ-pi-ʔi</ta>
            <ta e="T582" id="Seg_6278" s="T581">dĭgəttə</ta>
            <ta e="T583" id="Seg_6279" s="T582">dĭ</ta>
            <ta e="T584" id="Seg_6280" s="T583">uʔbdə-bi</ta>
            <ta e="T585" id="Seg_6281" s="T584">sima-bə</ta>
            <ta e="T586" id="Seg_6282" s="T585">băzə-bi</ta>
            <ta e="T587" id="Seg_6283" s="T586">kĭškə-bi</ta>
            <ta e="T588" id="Seg_6284" s="T587">šide</ta>
            <ta e="T589" id="Seg_6285" s="T588">ular</ta>
            <ta e="T590" id="Seg_6286" s="T589">i-bi</ta>
            <ta e="T591" id="Seg_6287" s="T590">i</ta>
            <ta e="T592" id="Seg_6288" s="T591">kandə-ga</ta>
            <ta e="T595" id="Seg_6289" s="T594">kal-luʔ-pi</ta>
            <ta e="T596" id="Seg_6290" s="T595">dĭgəttə</ta>
            <ta e="T597" id="Seg_6291" s="T596">šonə-ga</ta>
            <ta e="T598" id="Seg_6292" s="T597">šonə-ga</ta>
            <ta e="T599" id="Seg_6293" s="T598">dĭn</ta>
            <ta e="T600" id="Seg_6294" s="T599">bar</ta>
            <ta e="T601" id="Seg_6295" s="T600">krospa</ta>
            <ta e="T602" id="Seg_6296" s="T601">nu-laʔbə</ta>
            <ta e="T603" id="Seg_6297" s="T602">dĭ</ta>
            <ta e="T604" id="Seg_6298" s="T603">tĭl-bi</ta>
            <ta e="T605" id="Seg_6299" s="T604">dĭn</ta>
            <ta e="T606" id="Seg_6300" s="T605">nüke</ta>
            <ta e="T607" id="Seg_6301" s="T606">iʔbo-laʔbə</ta>
            <ta e="T608" id="Seg_6302" s="T607">dĭ</ta>
            <ta e="T609" id="Seg_6303" s="T608">nüke</ta>
            <ta e="T610" id="Seg_6304" s="T609">i-bi</ta>
            <ta e="T611" id="Seg_6305" s="T610">i</ta>
            <ta e="T612" id="Seg_6306" s="T611">šide</ta>
            <ta e="T613" id="Seg_6307" s="T612">ular</ta>
            <ta e="T614" id="Seg_6308" s="T613">i</ta>
            <ta e="T615" id="Seg_6309" s="T614">dĭ</ta>
            <ta e="T616" id="Seg_6310" s="T615">ular-zaŋ-də</ta>
            <ta e="T617" id="Seg_6311" s="T616">dĭ</ta>
            <ta e="T618" id="Seg_6312" s="T617">nüke-nə</ta>
            <ta e="T619" id="Seg_6313" s="T618">uda-zaŋ-də</ta>
            <ta e="T620" id="Seg_6314" s="T619">sar-bi</ta>
            <ta e="T621" id="Seg_6315" s="T620">sĭj-də</ta>
            <ta e="T622" id="Seg_6316" s="T621">sĭj-gəndə</ta>
            <ta e="T623" id="Seg_6317" s="T622">tagaj</ta>
            <ta e="T625" id="Seg_6318" s="T624">păʔ-luʔ-pi</ta>
            <ta e="T626" id="Seg_6319" s="T625">dĭgəttə</ta>
            <ta e="T627" id="Seg_6320" s="T626">šo-bi</ta>
            <ta e="T629" id="Seg_6321" s="T628">dĭn</ta>
            <ta e="T630" id="Seg_6322" s="T629">šide</ta>
            <ta e="T631" id="Seg_6323" s="T630">koʔbdo</ta>
            <ta e="T632" id="Seg_6324" s="T631">i</ta>
            <ta e="T633" id="Seg_6325" s="T632">büzʼe</ta>
            <ta e="T634" id="Seg_6326" s="T633">amno-laʔbə</ta>
            <ta e="T635" id="Seg_6327" s="T634">dʼăbaktər-lia</ta>
            <ta e="T636" id="Seg_6328" s="T635">dʼăbaktər-lia</ta>
            <ta e="T637" id="Seg_6329" s="T636">măn</ta>
            <ta e="T638" id="Seg_6330" s="T637">dĭn</ta>
            <ta e="T639" id="Seg_6331" s="T638">nüke</ta>
            <ta e="T640" id="Seg_6332" s="T639">amno-laʔbə</ta>
            <ta e="T641" id="Seg_6333" s="T640">ular-ziʔ</ta>
            <ta e="T642" id="Seg_6334" s="T641">a</ta>
            <ta e="T643" id="Seg_6335" s="T642">bos-tə</ta>
            <ta e="T645" id="Seg_6336" s="T644">sĭj-gəndə</ta>
            <ta e="T646" id="Seg_6337" s="T645">dagaj</ta>
            <ta e="T647" id="Seg_6338" s="T646">amno-laʔbə</ta>
            <ta e="T648" id="Seg_6339" s="T647">a</ta>
            <ta e="T649" id="Seg_6340" s="T648">koʔb-saŋ</ta>
            <ta e="T650" id="Seg_6341" s="T649">miʔ</ta>
            <ta e="T651" id="Seg_6342" s="T650">kal-lə-bəj</ta>
            <ta e="T652" id="Seg_6343" s="T651">deʔ-lə-bəj</ta>
            <ta e="T653" id="Seg_6344" s="T652">nüke-l</ta>
            <ta e="T654" id="Seg_6345" s="T653">tăn</ta>
            <ta e="T655" id="Seg_6346" s="T654">dĭ</ta>
            <ta e="T656" id="Seg_6347" s="T655">ugandə</ta>
            <ta e="T657" id="Seg_6348" s="T656">pim-nie</ta>
            <ta e="T658" id="Seg_6349" s="T657">ato</ta>
            <ta e="T659" id="Seg_6350" s="T658">dʼagar-lə-j</ta>
            <ta e="T660" id="Seg_6351" s="T659">bos-tə</ta>
            <ta e="T661" id="Seg_6352" s="T660">bos-tə</ta>
            <ta e="T663" id="Seg_6353" s="T662">dĭgəttə</ta>
            <ta e="T664" id="Seg_6354" s="T663">dĭ-zeŋ</ta>
            <ta e="T665" id="Seg_6355" s="T664">koʔb-saŋ</ta>
            <ta e="T666" id="Seg_6356" s="T665">nuʔmə-luʔ-bi-ʔi</ta>
            <ta e="T667" id="Seg_6357" s="T666">šo-bi-ʔi</ta>
            <ta e="T668" id="Seg_6358" s="T667">dʼagar-luʔ-pi</ta>
            <ta e="T669" id="Seg_6359" s="T668">ular-də</ta>
            <ta e="T670" id="Seg_6360" s="T669">bar</ta>
            <ta e="T672" id="Seg_6361" s="T671">nuʔmə-luʔ-bi-ʔi</ta>
            <ta e="T673" id="Seg_6362" s="T672">nüke-m</ta>
            <ta e="T674" id="Seg_6363" s="T673">dĭgəttə</ta>
            <ta e="T675" id="Seg_6364" s="T674">dĭ</ta>
            <ta e="T676" id="Seg_6365" s="T675">bar</ta>
            <ta e="T677" id="Seg_6366" s="T676">saʔmə-luʔ-bi</ta>
            <ta e="T678" id="Seg_6367" s="T677">Kodur</ta>
            <ta e="T679" id="Seg_6368" s="T678">dʼor-bi</ta>
            <ta e="T680" id="Seg_6369" s="T679">dʼor-bi</ta>
            <ta e="T681" id="Seg_6370" s="T680">dĭ</ta>
            <ta e="T682" id="Seg_6371" s="T681">büzʼe</ta>
            <ta e="T683" id="Seg_6372" s="T682">măn-də</ta>
            <ta e="T684" id="Seg_6373" s="T683">i-t</ta>
            <ta e="T685" id="Seg_6374" s="T684">onʼiʔ</ta>
            <ta e="T686" id="Seg_6375" s="T685">koʔbdo</ta>
            <ta e="T687" id="Seg_6376" s="T686">našto</ta>
            <ta e="T688" id="Seg_6377" s="T687">măna</ta>
            <ta e="T689" id="Seg_6378" s="T688">onʼiʔ</ta>
            <ta e="T690" id="Seg_6379" s="T689">măn</ta>
            <ta e="T691" id="Seg_6380" s="T690">nüke-m</ta>
            <ta e="T692" id="Seg_6381" s="T691">lutʼšə</ta>
            <ta e="T693" id="Seg_6382" s="T692">i-bi</ta>
            <ta e="T694" id="Seg_6383" s="T693">no</ta>
            <ta e="T695" id="Seg_6384" s="T694">i-t</ta>
            <ta e="T696" id="Seg_6385" s="T695">šide</ta>
            <ta e="T697" id="Seg_6386" s="T696">dĭgəttə</ta>
            <ta e="T698" id="Seg_6387" s="T697">dĭ</ta>
            <ta e="T699" id="Seg_6388" s="T698">uʔbdə-bi</ta>
            <ta e="T700" id="Seg_6389" s="T699">bazoʔ</ta>
            <ta e="T701" id="Seg_6390" s="T700">băzə-jdə-bi</ta>
            <ta e="T702" id="Seg_6391" s="T701">kadəl-də</ta>
            <ta e="T703" id="Seg_6392" s="T702">i</ta>
            <ta e="T704" id="Seg_6393" s="T703">šide</ta>
            <ta e="T705" id="Seg_6394" s="T704">koʔbdo</ta>
            <ta e="T706" id="Seg_6395" s="T705">i-bi</ta>
            <ta e="T707" id="Seg_6396" s="T706">i</ta>
            <ta e="T708" id="Seg_6397" s="T707">kam-bi</ta>
            <ta e="T709" id="Seg_6398" s="T708">dĭgəttə</ta>
            <ta e="T710" id="Seg_6399" s="T709">šonə-ga</ta>
            <ta e="T711" id="Seg_6400" s="T710">šonə-ga</ta>
            <ta e="T712" id="Seg_6401" s="T711">dĭn</ta>
            <ta e="T713" id="Seg_6402" s="T712">ular-əʔi</ta>
            <ta e="T714" id="Seg_6403" s="T713">mĭl-leʔbə-ʔjə</ta>
            <ta e="T715" id="Seg_6404" s="T714">dĭgəttə</ta>
            <ta e="T716" id="Seg_6405" s="T715">šo-bi</ta>
            <ta e="T717" id="Seg_6406" s="T716">pastux-tə</ta>
            <ta e="T718" id="Seg_6407" s="T717">dön</ta>
            <ta e="T719" id="Seg_6408" s="T718">măn</ta>
            <ta e="T720" id="Seg_6409" s="T719">ular</ta>
            <ta e="T721" id="Seg_6410" s="T720">da</ta>
            <ta e="T723" id="Seg_6411" s="T722">onʼiʔ</ta>
            <ta e="T725" id="Seg_6412" s="T724">amnəl-bi</ta>
            <ta e="T726" id="Seg_6413" s="T725">bü-nə</ta>
            <ta e="T728" id="Seg_6414" s="T727">toʔ-ndə</ta>
            <ta e="T729" id="Seg_6415" s="T728">onʼiʔ</ta>
            <ta e="T730" id="Seg_6416" s="T729">amnəl-bə-bi</ta>
            <ta e="T731" id="Seg_6417" s="T730">dʼije-gən</ta>
            <ta e="T732" id="Seg_6418" s="T731">dĭgəttə</ta>
            <ta e="T733" id="Seg_6419" s="T732">măl-laʔbə</ta>
            <ta e="T734" id="Seg_6420" s="T733">măn</ta>
            <ta e="T735" id="Seg_6421" s="T734">ular</ta>
            <ta e="T736" id="Seg_6422" s="T735">no</ta>
            <ta e="T737" id="Seg_6423" s="T736">surar-də</ta>
            <ta e="T740" id="Seg_6424" s="T739">surar-də</ta>
            <ta e="T741" id="Seg_6425" s="T740">dĭ</ta>
            <ta e="T742" id="Seg_6426" s="T741">surar-luʔ-pi</ta>
            <ta e="T743" id="Seg_6427" s="T742">Kodur-ə-n</ta>
            <ta e="T744" id="Seg_6428" s="T743">ular-zaŋ-də</ta>
            <ta e="T745" id="Seg_6429" s="T744">dĭgəttə</ta>
            <ta e="T746" id="Seg_6430" s="T745">dʼije-gən</ta>
            <ta e="T747" id="Seg_6431" s="T746">kuza-nə</ta>
            <ta e="T748" id="Seg_6432" s="T747">surar-də</ta>
            <ta e="T749" id="Seg_6433" s="T748">surar-bi</ta>
            <ta e="T750" id="Seg_6434" s="T749">dĭ</ta>
            <ta e="T751" id="Seg_6435" s="T750">măn-də</ta>
            <ta e="T752" id="Seg_6436" s="T751">Kodur-ə-n</ta>
            <ta e="T753" id="Seg_6437" s="T752">ular-də</ta>
            <ta e="T754" id="Seg_6438" s="T753">dĭgəttə</ta>
            <ta e="T756" id="Seg_6439" s="T755">i-bi</ta>
            <ta e="T757" id="Seg_6440" s="T756">ular-də</ta>
            <ta e="T759" id="Seg_6441" s="T758">bazoʔ</ta>
            <ta e="T761" id="Seg_6442" s="T760">šo-bi</ta>
            <ta e="T762" id="Seg_6443" s="T761">kuza</ta>
            <ta e="T763" id="Seg_6444" s="T762">ine-ʔi</ta>
            <ta e="T765" id="Seg_6445" s="T764">amno-laʔbə</ta>
            <ta e="T766" id="Seg_6446" s="T765">măn</ta>
            <ta e="T767" id="Seg_6447" s="T766">ine-ʔi</ta>
            <ta e="T768" id="Seg_6448" s="T767">a</ta>
            <ta e="T769" id="Seg_6449" s="T768">dĭ</ta>
            <ta e="T770" id="Seg_6450" s="T769">măn-də</ta>
            <ta e="T771" id="Seg_6451" s="T770">dʼok</ta>
            <ta e="T772" id="Seg_6452" s="T771">măn</ta>
            <ta e="T773" id="Seg_6453" s="T772">ine-ʔi</ta>
            <ta e="T774" id="Seg_6454" s="T773">no</ta>
            <ta e="T775" id="Seg_6455" s="T774">surar-də</ta>
            <ta e="T776" id="Seg_6456" s="T775">bü-nə</ta>
            <ta e="T777" id="Seg_6457" s="T776">kuza-m</ta>
            <ta e="T778" id="Seg_6458" s="T777">dĭ</ta>
            <ta e="T779" id="Seg_6459" s="T778">surar-bi</ta>
            <ta e="T780" id="Seg_6460" s="T779">Kodur-ə-n</ta>
            <ta e="T781" id="Seg_6461" s="T780">ine-ʔi</ta>
            <ta e="T782" id="Seg_6462" s="T781">dĭgəttə</ta>
            <ta e="T783" id="Seg_6463" s="T782">dʼije-n</ta>
            <ta e="T784" id="Seg_6464" s="T783">kuza-nə</ta>
            <ta e="T785" id="Seg_6465" s="T784">surar-də</ta>
            <ta e="T786" id="Seg_6466" s="T785">dĭ</ta>
            <ta e="T787" id="Seg_6467" s="T786">surar-bi</ta>
            <ta e="T788" id="Seg_6468" s="T787">Kodur-ə-n</ta>
            <ta e="T789" id="Seg_6469" s="T788">ine-ʔi</ta>
            <ta e="T790" id="Seg_6470" s="T789">dĭ</ta>
            <ta e="T791" id="Seg_6471" s="T790">bar</ta>
            <ta e="T792" id="Seg_6472" s="T791">öʔluʔ-pi</ta>
            <ta e="T793" id="Seg_6473" s="T792">dĭgəttə</ta>
            <ta e="T794" id="Seg_6474" s="T793">tüžöj-əʔi</ta>
            <ta e="T795" id="Seg_6475" s="T794">mĭl-leʔbə-ʔjə</ta>
            <ta e="T796" id="Seg_6476" s="T795">i</ta>
            <ta e="T797" id="Seg_6477" s="T796">tĭn</ta>
            <ta e="T798" id="Seg_6478" s="T797">tože</ta>
            <ta e="T799" id="Seg_6479" s="T798">kuza</ta>
            <ta e="T800" id="Seg_6480" s="T799">măndə-laʔbə</ta>
            <ta e="T801" id="Seg_6481" s="T800">tüžöj-əʔi</ta>
            <ta e="T802" id="Seg_6482" s="T801">dĭ</ta>
            <ta e="T803" id="Seg_6483" s="T802">šo-bi</ta>
            <ta e="T804" id="Seg_6484" s="T803">măn</ta>
            <ta e="T805" id="Seg_6485" s="T804">tüžöj-əʔjə</ta>
            <ta e="T806" id="Seg_6486" s="T805">dʼok</ta>
            <ta e="T807" id="Seg_6487" s="T806">ej</ta>
            <ta e="T808" id="Seg_6488" s="T807">tăn</ta>
            <ta e="T809" id="Seg_6489" s="T808">no</ta>
            <ta e="T810" id="Seg_6490" s="T809">surar-ʔ</ta>
            <ta e="T811" id="Seg_6491" s="T810">bü-gən</ta>
            <ta e="T812" id="Seg_6492" s="T811">kuza</ta>
            <ta e="T813" id="Seg_6493" s="T812">dĭgəttə</ta>
            <ta e="T814" id="Seg_6494" s="T813">dʼije-gən</ta>
            <ta e="T815" id="Seg_6495" s="T814">dĭ</ta>
            <ta e="T816" id="Seg_6496" s="T815">surar-bi</ta>
            <ta e="T817" id="Seg_6497" s="T816">šində-n</ta>
            <ta e="T818" id="Seg_6498" s="T817">tüžöj-də</ta>
            <ta e="T819" id="Seg_6499" s="T818">dĭ</ta>
            <ta e="T820" id="Seg_6500" s="T819">dĭ</ta>
            <ta e="T821" id="Seg_6501" s="T820">măm-bi</ta>
            <ta e="T822" id="Seg_6502" s="T821">Kodur-ən</ta>
            <ta e="T823" id="Seg_6503" s="T822">tüžöj-də</ta>
            <ta e="T824" id="Seg_6504" s="T823">i</ta>
            <ta e="T825" id="Seg_6505" s="T824">dʼije-gən</ta>
            <ta e="T826" id="Seg_6506" s="T825">surar-bi</ta>
            <ta e="T827" id="Seg_6507" s="T826">šində-n</ta>
            <ta e="T828" id="Seg_6508" s="T827">tüžöj-də</ta>
            <ta e="T829" id="Seg_6509" s="T828">Kodur-ə-n</ta>
            <ta e="T830" id="Seg_6510" s="T829">tüžöj-də</ta>
            <ta e="T831" id="Seg_6511" s="T830">dĭgəttə</ta>
            <ta e="T832" id="Seg_6512" s="T831">dĭ</ta>
            <ta e="T833" id="Seg_6513" s="T832">davaj</ta>
            <ta e="T834" id="Seg_6514" s="T833">nüjnə-sʼtə</ta>
            <ta e="T835" id="Seg_6515" s="T834">nüjnə</ta>
            <ta e="T836" id="Seg_6516" s="T835">măn</ta>
            <ta e="T837" id="Seg_6517" s="T836">lăpatka</ta>
            <ta e="T840" id="Seg_6518" s="T839">i-bie-m</ta>
            <ta e="T842" id="Seg_6519" s="T841">ular</ta>
            <ta e="T843" id="Seg_6520" s="T842">i-bie-m</ta>
            <ta e="T844" id="Seg_6521" s="T843">ular-də</ta>
            <ta e="T845" id="Seg_6522" s="T844">šide</ta>
            <ta e="T846" id="Seg_6523" s="T845">ular</ta>
            <ta e="T847" id="Seg_6524" s="T846">i-bie-m</ta>
            <ta e="T848" id="Seg_6525" s="T847">šide</ta>
            <ta e="T849" id="Seg_6526" s="T848">ular</ta>
            <ta e="T850" id="Seg_6527" s="T849">i-bie-m</ta>
            <ta e="T851" id="Seg_6528" s="T850">kam-bia-m</ta>
            <ta e="T852" id="Seg_6529" s="T851">nüke</ta>
            <ta e="T853" id="Seg_6530" s="T852">dʼü-gən</ta>
            <ta e="T854" id="Seg_6531" s="T853">i-bie-m</ta>
            <ta e="T855" id="Seg_6532" s="T854">i</ta>
            <ta e="T856" id="Seg_6533" s="T855">šide</ta>
            <ta e="T857" id="Seg_6534" s="T856">ular</ta>
            <ta e="T858" id="Seg_6535" s="T857">i</ta>
            <ta e="T859" id="Seg_6536" s="T858">nüke</ta>
            <ta e="T860" id="Seg_6537" s="T859">dagaj</ta>
            <ta e="T863" id="Seg_6538" s="T862">sĭj-də</ta>
            <ta e="T865" id="Seg_6539" s="T864">dagaj</ta>
            <ta e="T866" id="Seg_6540" s="T865">sĭj-də</ta>
            <ta e="T868" id="Seg_6541" s="T867">müʔ-luʔ-pi</ta>
            <ta e="T869" id="Seg_6542" s="T868">a</ta>
            <ta e="T870" id="Seg_6543" s="T869">bos-tə</ta>
            <ta e="T871" id="Seg_6544" s="T870">šo-bi</ta>
            <ta e="T872" id="Seg_6545" s="T871">măn</ta>
            <ta e="T873" id="Seg_6546" s="T872">dĭn</ta>
            <ta e="T874" id="Seg_6547" s="T873">nüke-m</ta>
            <ta e="T875" id="Seg_6548" s="T874">i-ge</ta>
            <ta e="T876" id="Seg_6549" s="T875">i</ta>
            <ta e="T877" id="Seg_6550" s="T876">šide</ta>
            <ta e="T878" id="Seg_6551" s="T877">ular</ta>
            <ta e="T879" id="Seg_6552" s="T878">i</ta>
            <ta e="T880" id="Seg_6553" s="T879">šide</ta>
            <ta e="T881" id="Seg_6554" s="T880">koʔb-saŋ</ta>
            <ta e="T882" id="Seg_6555" s="T881">i</ta>
            <ta e="T883" id="Seg_6556" s="T882">büzʼe</ta>
            <ta e="T884" id="Seg_6557" s="T883">amno-laʔbə</ta>
            <ta e="T885" id="Seg_6558" s="T884">dĭ</ta>
            <ta e="T886" id="Seg_6559" s="T885">bar</ta>
            <ta e="T887" id="Seg_6560" s="T886">büzʼe</ta>
            <ta e="T888" id="Seg_6561" s="T887">dʼăbaktər-ia</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T3" id="Seg_6562" s="T2">tar-zittə</ta>
            <ta e="T4" id="Seg_6563" s="T3">măn</ta>
            <ta e="T5" id="Seg_6564" s="T4">dĭ-Tə</ta>
            <ta e="T6" id="Seg_6565" s="T5">hen-bi-m</ta>
            <ta e="T7" id="Seg_6566" s="T6">beške-jəʔ</ta>
            <ta e="T8" id="Seg_6567" s="T7">kajaʔ</ta>
            <ta e="T9" id="Seg_6568" s="T8">mĭ-bi-m</ta>
            <ta e="T10" id="Seg_6569" s="T9">aktʼa</ta>
            <ta e="T11" id="Seg_6570" s="T10">mĭ-bi-m</ta>
            <ta e="T12" id="Seg_6571" s="T11">oʔb</ta>
            <ta e="T13" id="Seg_6572" s="T12">šide</ta>
            <ta e="T14" id="Seg_6573" s="T13">nagur</ta>
            <ta e="T15" id="Seg_6574" s="T14">teʔdə</ta>
            <ta e="T16" id="Seg_6575" s="T15">dĭ-zAŋ</ta>
            <ta e="T17" id="Seg_6576" s="T16">tura</ta>
            <ta e="T18" id="Seg_6577" s="T17">i-bi-jəʔ</ta>
            <ta e="T19" id="Seg_6578" s="T18">šində=də</ta>
            <ta e="T20" id="Seg_6579" s="T19">bar</ta>
            <ta e="T21" id="Seg_6580" s="T20">sădar-laʔbə</ta>
            <ta e="T22" id="Seg_6581" s="T21">i</ta>
            <ta e="T23" id="Seg_6582" s="T22">aktʼa</ta>
            <ta e="T24" id="Seg_6583" s="T23">ej</ta>
            <ta e="T25" id="Seg_6584" s="T24">kabar-liA</ta>
            <ta e="T27" id="Seg_6585" s="T26">dĭ-zAŋ</ta>
            <ta e="T28" id="Seg_6586" s="T27">dĭ</ta>
            <ta e="T30" id="Seg_6587" s="T29">tura</ta>
            <ta e="T31" id="Seg_6588" s="T30">sădar-lAʔ</ta>
            <ta e="T32" id="Seg_6589" s="T31">i-bi-jəʔ</ta>
            <ta e="T34" id="Seg_6590" s="T33">a</ta>
            <ta e="T35" id="Seg_6591" s="T34">girgit</ta>
            <ta e="T36" id="Seg_6592" s="T35">kuza-Kən</ta>
            <ta e="T37" id="Seg_6593" s="T36">naga</ta>
            <ta e="T39" id="Seg_6594" s="T38">isaʔtə</ta>
            <ta e="T40" id="Seg_6595" s="T39">aktʼa</ta>
            <ta e="T41" id="Seg_6596" s="T40">mĭ-zittə</ta>
            <ta e="T42" id="Seg_6597" s="T41">dĭ-n</ta>
            <ta e="T43" id="Seg_6598" s="T42">tüžöj-də</ta>
            <ta e="T44" id="Seg_6599" s="T43">il</ta>
            <ta e="T45" id="Seg_6600" s="T44">i-bi-jəʔ</ta>
            <ta e="T46" id="Seg_6601" s="T45">ĭmbi</ta>
            <ta e="T47" id="Seg_6602" s="T46">i-gA</ta>
            <ta e="T48" id="Seg_6603" s="T47">bar</ta>
            <ta e="T49" id="Seg_6604" s="T48">i-luʔbdə-bi-jəʔ</ta>
            <ta e="T50" id="Seg_6605" s="T49">i</ta>
            <ta e="T51" id="Seg_6606" s="T50">mĭ-bi-jəʔ</ta>
            <ta e="T52" id="Seg_6607" s="T51">aktʼa</ta>
            <ta e="T53" id="Seg_6608" s="T52">dĭbər</ta>
            <ta e="T55" id="Seg_6609" s="T54">măn</ta>
            <ta e="T56" id="Seg_6610" s="T55">tugan-m</ta>
            <ta e="T892" id="Seg_6611" s="T56">kan-lAʔ</ta>
            <ta e="T57" id="Seg_6612" s="T892">tʼür-bi</ta>
            <ta e="T58" id="Seg_6613" s="T57">gorăt-Tə</ta>
            <ta e="T59" id="Seg_6614" s="T58">i</ta>
            <ta e="T60" id="Seg_6615" s="T59">dĭn</ta>
            <ta e="T61" id="Seg_6616" s="T60">mašina</ta>
            <ta e="T62" id="Seg_6617" s="T61">i-lV-j</ta>
            <ta e="T63" id="Seg_6618" s="T62">oldʼa</ta>
            <ta e="T64" id="Seg_6619" s="T63">bazə-zittə</ta>
            <ta e="T65" id="Seg_6620" s="T64">kujnek</ta>
            <ta e="T66" id="Seg_6621" s="T65">kujnek-jəʔ</ta>
            <ta e="T67" id="Seg_6622" s="T66">piʔme-jəʔ</ta>
            <ta e="T68" id="Seg_6623" s="T67">bar</ta>
            <ta e="T70" id="Seg_6624" s="T69">ĭmbi</ta>
            <ta e="T71" id="Seg_6625" s="T70">i-gA</ta>
            <ta e="T72" id="Seg_6626" s="T71">bazə-zittə</ta>
            <ta e="T74" id="Seg_6627" s="T73">tibi</ta>
            <ta e="T75" id="Seg_6628" s="T74">i</ta>
            <ta e="T76" id="Seg_6629" s="T75">ne</ta>
            <ta e="T77" id="Seg_6630" s="T76">amno-bi-jəʔ</ta>
            <ta e="T78" id="Seg_6631" s="T77">ugaːndə</ta>
            <ta e="T79" id="Seg_6632" s="T78">jakšə</ta>
            <ta e="T80" id="Seg_6633" s="T79">ajir-bi-jəʔ</ta>
            <ta e="T83" id="Seg_6634" s="T81">onʼiʔ</ta>
            <ta e="T84" id="Seg_6635" s="T83">onʼiʔ</ta>
            <ta e="T85" id="Seg_6636" s="T84">onʼiʔ-Tə</ta>
            <ta e="T86" id="Seg_6637" s="T85">ajir-bi-jəʔ</ta>
            <ta e="T87" id="Seg_6638" s="T86">bar</ta>
            <ta e="T88" id="Seg_6639" s="T87">kamro-laʔtə-jəʔ</ta>
            <ta e="T89" id="Seg_6640" s="T88">panar-laʔbə-jəʔ</ta>
            <ta e="T90" id="Seg_6641" s="T89">i</ta>
            <ta e="T91" id="Seg_6642" s="T90">dĭ-zAŋ</ta>
            <ta e="T92" id="Seg_6643" s="T91">ešši-zAŋ-Tə</ta>
            <ta e="T93" id="Seg_6644" s="T92">i-bi-jəʔ</ta>
            <ta e="T94" id="Seg_6645" s="T93">ugaːndə</ta>
            <ta e="T95" id="Seg_6646" s="T94">ešši-zAŋ-Tə</ta>
            <ta e="T96" id="Seg_6647" s="T95">jakšə</ta>
            <ta e="T97" id="Seg_6648" s="T96">ĭmbi</ta>
            <ta e="T98" id="Seg_6649" s="T97">măn-lV-l</ta>
            <ta e="T99" id="Seg_6650" s="T98">dĭ-zAŋ</ta>
            <ta e="T100" id="Seg_6651" s="T99">nʼilgö-laʔbə-jəʔ</ta>
            <ta e="T102" id="Seg_6652" s="T101">a</ta>
            <ta e="T103" id="Seg_6653" s="T102">miʔ</ta>
            <ta e="T104" id="Seg_6654" s="T103">ešši-zAŋ</ta>
            <ta e="T105" id="Seg_6655" s="T104">ugaːndə</ta>
            <ta e="T106" id="Seg_6656" s="T105">ej</ta>
            <ta e="T107" id="Seg_6657" s="T106">jakšə</ta>
            <ta e="T108" id="Seg_6658" s="T107">tʼabəro-laʔbə-jəʔ</ta>
            <ta e="T109" id="Seg_6659" s="T108">kudo-nzə-laʔbə-jəʔ</ta>
            <ta e="T110" id="Seg_6660" s="T109">ej</ta>
            <ta e="T111" id="Seg_6661" s="T110">nʼilgö-laʔbə-jəʔ</ta>
            <ta e="T112" id="Seg_6662" s="T111">măn</ta>
            <ta e="T113" id="Seg_6663" s="T112">dĭ-zem</ta>
            <ta e="T114" id="Seg_6664" s="T113">bar</ta>
            <ta e="T115" id="Seg_6665" s="T114">münör-laʔbə-m</ta>
            <ta e="T117" id="Seg_6666" s="T116">ešši-zAŋ-m</ta>
            <ta e="T118" id="Seg_6667" s="T117">tüšə-lə-liA-m</ta>
            <ta e="T119" id="Seg_6668" s="T118">sazən</ta>
            <ta e="T120" id="Seg_6669" s="T119">pʼaŋdə-zittə</ta>
            <ta e="T122" id="Seg_6670" s="T121">nu-zAŋ</ta>
            <ta e="T123" id="Seg_6671" s="T122">amno-bi-jəʔ</ta>
            <ta e="T124" id="Seg_6672" s="T123">šində</ta>
            <ta e="T125" id="Seg_6673" s="T124">ugaːndə</ta>
            <ta e="T126" id="Seg_6674" s="T125">aktʼa</ta>
            <ta e="T127" id="Seg_6675" s="T126">iʔgö</ta>
            <ta e="T128" id="Seg_6676" s="T127">a</ta>
            <ta e="T129" id="Seg_6677" s="T128">šində-n</ta>
            <ta e="T130" id="Seg_6678" s="T129">naga</ta>
            <ta e="T131" id="Seg_6679" s="T130">dĭ</ta>
            <ta e="T132" id="Seg_6680" s="T131">ara</ta>
            <ta e="T133" id="Seg_6681" s="T132">bĭs-liA</ta>
            <ta e="T134" id="Seg_6682" s="T133">bar</ta>
            <ta e="T135" id="Seg_6683" s="T134">aktʼa</ta>
            <ta e="T136" id="Seg_6684" s="T135">bĭs-luʔbdə-lV-j</ta>
            <ta e="T137" id="Seg_6685" s="T136">dĭgəttə</ta>
            <ta e="T138" id="Seg_6686" s="T137">ĭmbi=də</ta>
            <ta e="T139" id="Seg_6687" s="T138">naga</ta>
            <ta e="T140" id="Seg_6688" s="T139">togonər-liA</ta>
            <ta e="T141" id="Seg_6689" s="T140">togonər-liA</ta>
            <ta e="T142" id="Seg_6690" s="T141">bar</ta>
            <ta e="T143" id="Seg_6691" s="T142">bĭs-luʔbdə-lV-j</ta>
            <ta e="T146" id="Seg_6692" s="T145">Sibirʼ-Kən</ta>
            <ta e="T147" id="Seg_6693" s="T146">ugaːndə</ta>
            <ta e="T148" id="Seg_6694" s="T147">iʔgö</ta>
            <ta e="T149" id="Seg_6695" s="T148">nu-zAŋ</ta>
            <ta e="T150" id="Seg_6696" s="T149">amno-bi-jəʔ</ta>
            <ta e="T151" id="Seg_6697" s="T150">dĭgəttə</ta>
            <ta e="T152" id="Seg_6698" s="T151">sĭri</ta>
            <ta e="T153" id="Seg_6699" s="T152">pa</ta>
            <ta e="T154" id="Seg_6700" s="T153">özer-luʔbdə-bi</ta>
            <ta e="T155" id="Seg_6701" s="T154">dĭ-zAŋ</ta>
            <ta e="T156" id="Seg_6702" s="T155">măn-liA-jəʔ</ta>
            <ta e="T157" id="Seg_6703" s="T156">nu</ta>
            <ta e="T159" id="Seg_6704" s="T158">kazak</ta>
            <ta e="T160" id="Seg_6705" s="T159">il</ta>
            <ta e="T161" id="Seg_6706" s="T160">šo-lV-jəʔ</ta>
            <ta e="T162" id="Seg_6707" s="T161">miʔnʼibeʔ</ta>
            <ta e="T163" id="Seg_6708" s="T162">ej</ta>
            <ta e="T164" id="Seg_6709" s="T163">jakšə</ta>
            <ta e="T165" id="Seg_6710" s="T164">mo-lV-j</ta>
            <ta e="T166" id="Seg_6711" s="T165">dĭgəttə</ta>
            <ta e="T167" id="Seg_6712" s="T166">labaz-jəʔ</ta>
            <ta e="T168" id="Seg_6713" s="T167">a-bi-jəʔ</ta>
            <ta e="T169" id="Seg_6714" s="T168">dĭbər</ta>
            <ta e="T170" id="Seg_6715" s="T169">pi-jəʔ</ta>
            <ta e="T171" id="Seg_6716" s="T170">tažor-bi-jəʔ</ta>
            <ta e="T172" id="Seg_6717" s="T171">i</ta>
            <ta e="T173" id="Seg_6718" s="T172">baltu</ta>
            <ta e="T174" id="Seg_6719" s="T173">i-bi-jəʔ</ta>
            <ta e="T175" id="Seg_6720" s="T174">hʼaʔ-bi-jəʔ</ta>
            <ta e="T176" id="Seg_6721" s="T175">i</ta>
            <ta e="T177" id="Seg_6722" s="T176">dĭ</ta>
            <ta e="T178" id="Seg_6723" s="T177">saʔmə-luʔbdə-bi</ta>
            <ta e="T179" id="Seg_6724" s="T178">dĭ-zem</ta>
            <ta e="T180" id="Seg_6725" s="T179">dĭ-zAŋ</ta>
            <ta e="T181" id="Seg_6726" s="T180">bar</ta>
            <ta e="T182" id="Seg_6727" s="T181">dĭn</ta>
            <ta e="T183" id="Seg_6728" s="T182">kü-bi-jəʔ</ta>
            <ta e="T891" id="Seg_6729" s="T184">lutʼšə</ta>
            <ta e="T185" id="Seg_6730" s="T891">măna</ta>
            <ta e="T186" id="Seg_6731" s="T185">măn</ta>
            <ta e="T187" id="Seg_6732" s="T186">bɨ</ta>
            <ta e="T188" id="Seg_6733" s="T187">kü-bi-m</ta>
            <ta e="T190" id="Seg_6734" s="T189">ešši-zAŋ-m</ta>
            <ta e="T192" id="Seg_6735" s="T190">dĭrgit</ta>
            <ta e="T193" id="Seg_6736" s="T192">ej</ta>
            <ta e="T194" id="Seg_6737" s="T193">jakšə-jəʔ</ta>
            <ta e="T195" id="Seg_6738" s="T194">bar</ta>
            <ta e="T196" id="Seg_6739" s="T195">tojar-laʔbə-jəʔ</ta>
            <ta e="T197" id="Seg_6740" s="T196">il-ziʔ</ta>
            <ta e="T198" id="Seg_6741" s="T197">tʼabəro-laʔbə-jəʔ</ta>
            <ta e="T200" id="Seg_6742" s="T199">bar</ta>
            <ta e="T201" id="Seg_6743" s="T200">ara</ta>
            <ta e="T202" id="Seg_6744" s="T201">bĭs-laʔbə-jəʔ</ta>
            <ta e="T203" id="Seg_6745" s="T202">ažnə</ta>
            <ta e="T204" id="Seg_6746" s="T203">sima-ziʔ</ta>
            <ta e="T205" id="Seg_6747" s="T204">ej</ta>
            <ta e="T206" id="Seg_6748" s="T205">mo-liA</ta>
            <ta e="T207" id="Seg_6749" s="T206">măndo-r-zittə</ta>
            <ta e="T208" id="Seg_6750" s="T207">dĭ-zAŋ-Tə</ta>
            <ta e="T210" id="Seg_6751" s="T209">onʼiʔ</ta>
            <ta e="T211" id="Seg_6752" s="T210">ne-n</ta>
            <ta e="T212" id="Seg_6753" s="T211">nagur</ta>
            <ta e="T213" id="Seg_6754" s="T212">nʼi</ta>
            <ta e="T214" id="Seg_6755" s="T213">i-bi</ta>
            <ta e="T215" id="Seg_6756" s="T214">šide</ta>
            <ta e="T216" id="Seg_6757" s="T215">jakšə</ta>
            <ta e="T217" id="Seg_6758" s="T216">a</ta>
            <ta e="T218" id="Seg_6759" s="T217">onʼiʔ</ta>
            <ta e="T219" id="Seg_6760" s="T218">ej</ta>
            <ta e="T220" id="Seg_6761" s="T219">jakšə</ta>
            <ta e="T221" id="Seg_6762" s="T220">bar</ta>
            <ta e="T222" id="Seg_6763" s="T221">dĭ-m</ta>
            <ta e="T223" id="Seg_6764" s="T222">münör-bi-jəʔ</ta>
            <ta e="T224" id="Seg_6765" s="T223">üjü-ziʔ</ta>
            <ta e="T225" id="Seg_6766" s="T224">bar</ta>
            <ta e="T226" id="Seg_6767" s="T225">i</ta>
            <ta e="T228" id="Seg_6768" s="T227">muzuruk-ziʔ</ta>
            <ta e="T229" id="Seg_6769" s="T228">i</ta>
            <ta e="T230" id="Seg_6770" s="T229">pa-ziʔ</ta>
            <ta e="T231" id="Seg_6771" s="T230">bar</ta>
            <ta e="T234" id="Seg_6772" s="T233">toʔbdə-nar-bi-jəʔ</ta>
            <ta e="T235" id="Seg_6773" s="T234">a</ta>
            <ta e="T236" id="Seg_6774" s="T235">măn</ta>
            <ta e="T237" id="Seg_6775" s="T236">u</ta>
            <ta e="T239" id="Seg_6776" s="T237">măn-bi-m</ta>
            <ta e="T241" id="Seg_6777" s="T240">a</ta>
            <ta e="T242" id="Seg_6778" s="T241">măn</ta>
            <ta e="T243" id="Seg_6779" s="T242">u</ta>
            <ta e="T244" id="Seg_6780" s="T243">măn-bi-m</ta>
            <ta e="T245" id="Seg_6781" s="T244">e-ʔ</ta>
            <ta e="T246" id="Seg_6782" s="T245">üžüŋge-ʔ</ta>
            <ta e="T247" id="Seg_6783" s="T246">možet</ta>
            <ta e="T248" id="Seg_6784" s="T247">özer-lV-j</ta>
            <ta e="T249" id="Seg_6785" s="T248">sagəštə</ta>
            <ta e="T250" id="Seg_6786" s="T249">iʔgö</ta>
            <ta e="T251" id="Seg_6787" s="T250">mo-lV-j</ta>
            <ta e="T252" id="Seg_6788" s="T251">urgo</ta>
            <ta e="T253" id="Seg_6789" s="T252">mo-lV-j</ta>
            <ta e="T254" id="Seg_6790" s="T253">možet</ta>
            <ta e="T255" id="Seg_6791" s="T254">bar</ta>
            <ta e="T256" id="Seg_6792" s="T255">barəʔ-lV-j</ta>
            <ta e="T257" id="Seg_6793" s="T256">dĭgəttə</ta>
            <ta e="T258" id="Seg_6794" s="T257">jakšə</ta>
            <ta e="T259" id="Seg_6795" s="T258">mo-lV-j</ta>
            <ta e="T261" id="Seg_6796" s="T260">a</ta>
            <ta e="T262" id="Seg_6797" s="T261">dĭ</ta>
            <ta e="T263" id="Seg_6798" s="T262">girgit</ta>
            <ta e="T264" id="Seg_6799" s="T263">üdʼüge</ta>
            <ta e="T265" id="Seg_6800" s="T264">i-bi</ta>
            <ta e="T266" id="Seg_6801" s="T265">i</ta>
            <ta e="T267" id="Seg_6802" s="T266">urgo</ta>
            <ta e="T269" id="Seg_6803" s="T268">dĭrgit</ta>
            <ta e="T270" id="Seg_6804" s="T269">že</ta>
            <ta e="T271" id="Seg_6805" s="T270">ĭmbi=də</ta>
            <ta e="T272" id="Seg_6806" s="T271">naga</ta>
            <ta e="T273" id="Seg_6807" s="T272">jakšə</ta>
            <ta e="T275" id="Seg_6808" s="T274">dĭ</ta>
            <ta e="T276" id="Seg_6809" s="T275">armija-Tə</ta>
            <ta e="T277" id="Seg_6810" s="T276">kan-bi</ta>
            <ta e="T278" id="Seg_6811" s="T277">vsʼoravno</ta>
            <ta e="T279" id="Seg_6812" s="T278">šo-bi</ta>
            <ta e="T280" id="Seg_6813" s="T279">gibər</ta>
            <ta e="T281" id="Seg_6814" s="T280">togonər-zittə</ta>
            <ta e="T282" id="Seg_6815" s="T281">kan-lV-j</ta>
            <ta e="T283" id="Seg_6816" s="T282">dĭ-m</ta>
            <ta e="T284" id="Seg_6817" s="T283">bar</ta>
            <ta e="T285" id="Seg_6818" s="T284">sürer-laʔbə-jəʔ</ta>
            <ta e="T287" id="Seg_6819" s="T286">onʼiʔ</ta>
            <ta e="T288" id="Seg_6820" s="T287">nüke</ta>
            <ta e="T289" id="Seg_6821" s="T288">nʼi-t</ta>
            <ta e="T290" id="Seg_6822" s="T289">i-bi</ta>
            <ta e="T291" id="Seg_6823" s="T290">bar</ta>
            <ta e="T292" id="Seg_6824" s="T291">jezerik</ta>
            <ta e="T293" id="Seg_6825" s="T292">tăn</ta>
            <ta e="T294" id="Seg_6826" s="T293">ĭmbi</ta>
            <ta e="T295" id="Seg_6827" s="T294">măna</ta>
            <ta e="T296" id="Seg_6828" s="T295">ej</ta>
            <ta e="T297" id="Seg_6829" s="T296">tüšə-l-bi-l</ta>
            <ta e="T298" id="Seg_6830" s="T297">a</ta>
            <ta e="T299" id="Seg_6831" s="T298">dĭ</ta>
            <ta e="T300" id="Seg_6832" s="T299">măn-bi</ta>
            <ta e="T301" id="Seg_6833" s="T300">măn</ta>
            <ta e="T302" id="Seg_6834" s="T301">tüšə-lə-bi-m</ta>
            <ta e="T303" id="Seg_6835" s="T302">tănan</ta>
            <ta e="T304" id="Seg_6836" s="T303">ĭmbi</ta>
            <ta e="T305" id="Seg_6837" s="T304">kereʔ</ta>
            <ta e="T306" id="Seg_6838" s="T305">tăn</ta>
            <ta e="T307" id="Seg_6839" s="T306">tănan</ta>
            <ta e="T308" id="Seg_6840" s="T307">tăn</ta>
            <ta e="T310" id="Seg_6841" s="T309">i-bi-l</ta>
            <ta e="T311" id="Seg_6842" s="T310">a</ta>
            <ta e="T312" id="Seg_6843" s="T311">dĭ</ta>
            <ta e="T313" id="Seg_6844" s="T312">bar</ta>
            <ta e="T315" id="Seg_6845" s="T314">ara</ta>
            <ta e="T316" id="Seg_6846" s="T315">bĭs-liA-l</ta>
            <ta e="T317" id="Seg_6847" s="T316">a</ta>
            <ta e="T318" id="Seg_6848" s="T317">dĭ</ta>
            <ta e="T319" id="Seg_6849" s="T318">bar</ta>
            <ta e="T321" id="Seg_6850" s="T319">muzuruk-ziʔ=da</ta>
            <ta e="T322" id="Seg_6851" s="T321">ija-t</ta>
            <ta e="T323" id="Seg_6852" s="T322">xatʼel</ta>
            <ta e="T324" id="Seg_6853" s="T323">münör-zittə</ta>
            <ta e="T325" id="Seg_6854" s="T324">a</ta>
            <ta e="T326" id="Seg_6855" s="T325">il</ta>
            <ta e="T327" id="Seg_6856" s="T326">ej</ta>
            <ta e="T328" id="Seg_6857" s="T327">mĭ-bi-jəʔ</ta>
            <ta e="T330" id="Seg_6858" s="T329">măn</ta>
            <ta e="T331" id="Seg_6859" s="T330">kaga-m</ta>
            <ta e="T332" id="Seg_6860" s="T331">văjna-Kən</ta>
            <ta e="T333" id="Seg_6861" s="T332">i-bi-l</ta>
            <ta e="T334" id="Seg_6862" s="T333">i-bi</ta>
            <ta e="T335" id="Seg_6863" s="T334">i</ta>
            <ta e="T336" id="Seg_6864" s="T335">dĭ-m</ta>
            <ta e="T337" id="Seg_6865" s="T336">i-bi-jəʔ</ta>
            <ta e="T338" id="Seg_6866" s="T337">plen-Tə</ta>
            <ta e="T339" id="Seg_6867" s="T338">i</ta>
            <ta e="T340" id="Seg_6868" s="T339">pravăj</ta>
            <ta e="T341" id="Seg_6869" s="T340">uda-t</ta>
            <ta e="T342" id="Seg_6870" s="T341">müjə-t-ziʔ</ta>
            <ta e="T344" id="Seg_6871" s="T343">dʼagar-zittə</ta>
            <ta e="T345" id="Seg_6872" s="T344">xatʼeli</ta>
            <ta e="T346" id="Seg_6873" s="T345">a</ta>
            <ta e="T347" id="Seg_6874" s="T346">dĭ</ta>
            <ta e="T348" id="Seg_6875" s="T347">bar</ta>
            <ta e="T349" id="Seg_6876" s="T348">tʼor-bi</ta>
            <ta e="T350" id="Seg_6877" s="T349">šide</ta>
            <ta e="T351" id="Seg_6878" s="T350">müjə</ta>
            <ta e="T353" id="Seg_6879" s="T352">ma-də-bi-jəʔ</ta>
            <ta e="T355" id="Seg_6880" s="T354">dĭ-Tə</ta>
            <ta e="T357" id="Seg_6881" s="T356">măn</ta>
            <ta e="T358" id="Seg_6882" s="T357">u</ta>
            <ta e="T359" id="Seg_6883" s="T358">kaga-m</ta>
            <ta e="T360" id="Seg_6884" s="T359">nagur</ta>
            <ta e="T361" id="Seg_6885" s="T360">koʔbdo</ta>
            <ta e="T362" id="Seg_6886" s="T361">i</ta>
            <ta e="T363" id="Seg_6887" s="T362">nagur</ta>
            <ta e="T364" id="Seg_6888" s="T363">nʼi</ta>
            <ta e="T366" id="Seg_6889" s="T365">nörbə-zittə</ta>
            <ta e="T367" id="Seg_6890" s="T366">tănan</ta>
            <ta e="T368" id="Seg_6891" s="T367">sĭri</ta>
            <ta e="T369" id="Seg_6892" s="T368">sĭri</ta>
            <ta e="T370" id="Seg_6893" s="T369">buga</ta>
            <ta e="T372" id="Seg_6894" s="T371">tăn</ta>
            <ta e="T373" id="Seg_6895" s="T372">nörbə-t</ta>
            <ta e="T374" id="Seg_6896" s="T373">măn</ta>
            <ta e="T375" id="Seg_6897" s="T374">nörbə-t</ta>
            <ta e="T376" id="Seg_6898" s="T375">măn-zittə</ta>
            <ta e="T377" id="Seg_6899" s="T376">tănan</ta>
            <ta e="T378" id="Seg_6900" s="T377">kömə</ta>
            <ta e="T379" id="Seg_6901" s="T378">buga</ta>
            <ta e="T381" id="Seg_6902" s="T380">tăn</ta>
            <ta e="T382" id="Seg_6903" s="T381">nörbə-t</ta>
            <ta e="T383" id="Seg_6904" s="T382">măn</ta>
            <ta e="T384" id="Seg_6905" s="T383">nörbə-t</ta>
            <ta e="T385" id="Seg_6906" s="T384">măn-zittə</ta>
            <ta e="T386" id="Seg_6907" s="T385">tănan</ta>
            <ta e="T387" id="Seg_6908" s="T386">sagər</ta>
            <ta e="T388" id="Seg_6909" s="T387">buga</ta>
            <ta e="T389" id="Seg_6910" s="T388">Kodur</ta>
            <ta e="T390" id="Seg_6911" s="T389">Kodur</ta>
            <ta e="T391" id="Seg_6912" s="T390">kömə</ta>
            <ta e="T392" id="Seg_6913" s="T391">parga</ta>
            <ta e="T393" id="Seg_6914" s="T392">šo-bi</ta>
            <ta e="T394" id="Seg_6915" s="T393">dön</ta>
            <ta e="T395" id="Seg_6916" s="T394">il</ta>
            <ta e="T396" id="Seg_6917" s="T395">amno-bi-jəʔ</ta>
            <ta e="T397" id="Seg_6918" s="T396">dĭ</ta>
            <ta e="T398" id="Seg_6919" s="T397">lăpatka</ta>
            <ta e="T399" id="Seg_6920" s="T398">ku-bi</ta>
            <ta e="T400" id="Seg_6921" s="T399">uja</ta>
            <ta e="T401" id="Seg_6922" s="T400">naga</ta>
            <ta e="T402" id="Seg_6923" s="T401">dĭgəttə</ta>
            <ta e="T403" id="Seg_6924" s="T402">kan-bi</ta>
            <ta e="T404" id="Seg_6925" s="T403">kan-bi</ta>
            <ta e="T405" id="Seg_6926" s="T404">nu</ta>
            <ta e="T406" id="Seg_6927" s="T405">dĭn</ta>
            <ta e="T407" id="Seg_6928" s="T406">onʼiʔ</ta>
            <ta e="T408" id="Seg_6929" s="T407">kuza</ta>
            <ta e="T409" id="Seg_6930" s="T408">amno-bi</ta>
            <ta e="T410" id="Seg_6931" s="T409">ne-ziʔ</ta>
            <ta e="T411" id="Seg_6932" s="T410">dĭ</ta>
            <ta e="T412" id="Seg_6933" s="T411">šo-bi</ta>
            <ta e="T413" id="Seg_6934" s="T412">dĭ-zAŋ</ta>
            <ta e="T414" id="Seg_6935" s="T413">dĭ-zAŋ-Tə</ta>
            <ta e="T415" id="Seg_6936" s="T414">ular</ta>
            <ta e="T416" id="Seg_6937" s="T415">i-bi</ta>
            <ta e="T417" id="Seg_6938" s="T416">onʼiʔ</ta>
            <ta e="T418" id="Seg_6939" s="T417">dĭ</ta>
            <ta e="T419" id="Seg_6940" s="T418">măn-ntə</ta>
            <ta e="T420" id="Seg_6941" s="T419">nadə</ta>
            <ta e="T421" id="Seg_6942" s="T420">segi</ta>
            <ta e="T422" id="Seg_6943" s="T421">bü</ta>
            <ta e="T423" id="Seg_6944" s="T422">mĭnzər-zittə</ta>
            <ta e="T424" id="Seg_6945" s="T423">kuza-m</ta>
            <ta e="T426" id="Seg_6946" s="T425">bădə-zittə</ta>
            <ta e="T427" id="Seg_6947" s="T426">a</ta>
            <ta e="T428" id="Seg_6948" s="T427">dĭ</ta>
            <ta e="T429" id="Seg_6949" s="T428">măn-ntə</ta>
            <ta e="T430" id="Seg_6950" s="T429">e-ʔ</ta>
            <ta e="T431" id="Seg_6951" s="T430">mĭnzər-ə-ʔ</ta>
            <ta e="T432" id="Seg_6952" s="T431">măn</ta>
            <ta e="T433" id="Seg_6953" s="T432">uja-m</ta>
            <ta e="T434" id="Seg_6954" s="T433">i-gA</ta>
            <ta e="T435" id="Seg_6955" s="T434">dĭgəttə</ta>
            <ta e="T436" id="Seg_6956" s="T435">aspaʔ</ta>
            <ta e="T437" id="Seg_6957" s="T436">edə-bi-jəʔ</ta>
            <ta e="T438" id="Seg_6958" s="T437">tus</ta>
            <ta e="T439" id="Seg_6959" s="T438">hen-bi-jəʔ</ta>
            <ta e="T440" id="Seg_6960" s="T439">dĭ</ta>
            <ta e="T442" id="Seg_6961" s="T441">kan-bi</ta>
            <ta e="T443" id="Seg_6962" s="T442">püje</ta>
            <ta e="T444" id="Seg_6963" s="T443">püje-t</ta>
            <ta e="T445" id="Seg_6964" s="T444">toʔbdə-nar-bi</ta>
            <ta e="T446" id="Seg_6965" s="T445">kem-ziʔ</ta>
            <ta e="T447" id="Seg_6966" s="T446">kem-ziʔ</ta>
            <ta e="T449" id="Seg_6967" s="T447">lăpatka-m</ta>
            <ta e="T450" id="Seg_6968" s="T449">dĭgəttə</ta>
            <ta e="T451" id="Seg_6969" s="T450">aspaʔ-də</ta>
            <ta e="T452" id="Seg_6970" s="T451">barəʔ-luʔbdə-bi</ta>
            <ta e="T453" id="Seg_6971" s="T452">mĭnzər-bi</ta>
            <ta e="T454" id="Seg_6972" s="T453">mĭnzər-bi</ta>
            <ta e="T455" id="Seg_6973" s="T454">dĭgəttə</ta>
            <ta e="T456" id="Seg_6974" s="T455">uja</ta>
            <ta e="T457" id="Seg_6975" s="T456">naga</ta>
            <ta e="T458" id="Seg_6976" s="T457">onʼiʔ</ta>
            <ta e="T459" id="Seg_6977" s="T458">lăpatka</ta>
            <ta e="T460" id="Seg_6978" s="T459">dĭ</ta>
            <ta e="T461" id="Seg_6979" s="T460">bar</ta>
            <ta e="T463" id="Seg_6980" s="T462">saʔmə-luʔbdə-bi</ta>
            <ta e="T464" id="Seg_6981" s="T463">davaj</ta>
            <ta e="T465" id="Seg_6982" s="T464">tʼor-zittə</ta>
            <ta e="T466" id="Seg_6983" s="T465">a</ta>
            <ta e="T467" id="Seg_6984" s="T466">dĭ-zAŋ</ta>
            <ta e="T470" id="Seg_6985" s="T469">măn-bi-jəʔ</ta>
            <ta e="T471" id="Seg_6986" s="T470">tüjö</ta>
            <ta e="T472" id="Seg_6987" s="T471">ular</ta>
            <ta e="T473" id="Seg_6988" s="T472">dʼagar-lV-m</ta>
            <ta e="T474" id="Seg_6989" s="T473">tănan</ta>
            <ta e="T475" id="Seg_6990" s="T474">lăpatka-m</ta>
            <ta e="T476" id="Seg_6991" s="T475">mĭn-bi-m</ta>
            <ta e="T477" id="Seg_6992" s="T476">măna</ta>
            <ta e="T478" id="Seg_6993" s="T477">ej</ta>
            <ta e="T479" id="Seg_6994" s="T478">kereʔ</ta>
            <ta e="T480" id="Seg_6995" s="T479">lăpatka</ta>
            <ta e="T481" id="Seg_6996" s="T480">dĭgəttə</ta>
            <ta e="T482" id="Seg_6997" s="T481">dĭ-Tə</ta>
            <ta e="T483" id="Seg_6998" s="T482">ular</ta>
            <ta e="T484" id="Seg_6999" s="T483">mĭ-bi-jəʔ</ta>
            <ta e="T486" id="Seg_7000" s="T485">dĭgəttə</ta>
            <ta e="T487" id="Seg_7001" s="T486">dĭ</ta>
            <ta e="T488" id="Seg_7002" s="T487">Kodur</ta>
            <ta e="T490" id="Seg_7003" s="T489">süʔmə-luʔbdə-bi</ta>
            <ta e="T491" id="Seg_7004" s="T490">bazə-jd-bi</ta>
            <ta e="T492" id="Seg_7005" s="T491">kĭškə-bi</ta>
            <ta e="T493" id="Seg_7006" s="T492">sima-t</ta>
            <ta e="T494" id="Seg_7007" s="T493">ular-də</ta>
            <ta e="T495" id="Seg_7008" s="T494">i-bi</ta>
            <ta e="T496" id="Seg_7009" s="T495">kan-bi</ta>
            <ta e="T497" id="Seg_7010" s="T496">dĭgəttə</ta>
            <ta e="T499" id="Seg_7011" s="T498">il</ta>
            <ta e="T500" id="Seg_7012" s="T499">il-Tə</ta>
            <ta e="T501" id="Seg_7013" s="T500">šo-bi</ta>
            <ta e="T502" id="Seg_7014" s="T501">dĭn</ta>
            <ta e="T503" id="Seg_7015" s="T502">tože</ta>
            <ta e="T504" id="Seg_7016" s="T503">šide</ta>
            <ta e="T505" id="Seg_7017" s="T504">ular</ta>
            <ta e="T506" id="Seg_7018" s="T505">nʼi</ta>
            <ta e="T507" id="Seg_7019" s="T506">i-bi</ta>
            <ta e="T508" id="Seg_7020" s="T507">i</ta>
            <ta e="T509" id="Seg_7021" s="T508">nʼi-n</ta>
            <ta e="T510" id="Seg_7022" s="T509">ne</ta>
            <ta e="T511" id="Seg_7023" s="T510">i-bi</ta>
            <ta e="T512" id="Seg_7024" s="T511">dĭgəttə</ta>
            <ta e="T513" id="Seg_7025" s="T512">măn-liA-jəʔ</ta>
            <ta e="T514" id="Seg_7026" s="T513">öʔlu-t</ta>
            <ta e="T515" id="Seg_7027" s="T514">tăn</ta>
            <ta e="T516" id="Seg_7028" s="T515">ular</ta>
            <ta e="T517" id="Seg_7029" s="T516">miʔ</ta>
            <ta e="T518" id="Seg_7030" s="T517">ular-ziʔ</ta>
            <ta e="T519" id="Seg_7031" s="T518">dʼok</ta>
            <ta e="T520" id="Seg_7032" s="T519">ej</ta>
            <ta e="T521" id="Seg_7033" s="T520">öʔ-lV-m</ta>
            <ta e="T522" id="Seg_7034" s="T521">šiʔ</ta>
            <ta e="T523" id="Seg_7035" s="T522">ular</ta>
            <ta e="T524" id="Seg_7036" s="T523">măn</ta>
            <ta e="T525" id="Seg_7037" s="T524">ular-m</ta>
            <ta e="T526" id="Seg_7038" s="T525">am-luʔbdə-jəʔ</ta>
            <ta e="T527" id="Seg_7039" s="T526">a</ta>
            <ta e="T528" id="Seg_7040" s="T527">bos-də</ta>
            <ta e="T530" id="Seg_7041" s="T529">tʼăbaktər-liA</ta>
            <ta e="T531" id="Seg_7042" s="T530">tʼăbaktər-liA</ta>
            <ta e="T532" id="Seg_7043" s="T531">dĭ</ta>
            <ta e="T534" id="Seg_7044" s="T533">nuʔmə-luʔbdə-bi</ta>
            <ta e="T535" id="Seg_7045" s="T534">da</ta>
            <ta e="T536" id="Seg_7046" s="T535">ular-də</ta>
            <ta e="T537" id="Seg_7047" s="T536">öʔ-bi</ta>
            <ta e="T538" id="Seg_7048" s="T537">dĭ-Tə</ta>
            <ta e="T539" id="Seg_7049" s="T538">ular-də</ta>
            <ta e="T540" id="Seg_7050" s="T539">a</ta>
            <ta e="T541" id="Seg_7051" s="T540">dĭgəttə</ta>
            <ta e="T542" id="Seg_7052" s="T541">iʔbə-bi-jəʔ</ta>
            <ta e="T543" id="Seg_7053" s="T542">kunol-zittə</ta>
            <ta e="T544" id="Seg_7054" s="T543">ertə-n</ta>
            <ta e="T545" id="Seg_7055" s="T544">uʔbdə-bi-jəʔ</ta>
            <ta e="T546" id="Seg_7056" s="T545">a</ta>
            <ta e="T547" id="Seg_7057" s="T546">ular</ta>
            <ta e="T548" id="Seg_7058" s="T547">naga</ta>
            <ta e="T550" id="Seg_7059" s="T549">dĭ</ta>
            <ta e="T551" id="Seg_7060" s="T550">Kodur</ta>
            <ta e="T552" id="Seg_7061" s="T551">uʔbdə-bi</ta>
            <ta e="T553" id="Seg_7062" s="T552">ular-də</ta>
            <ta e="T554" id="Seg_7063" s="T553">săj-nʼeʔbdə-bi</ta>
            <ta e="T555" id="Seg_7064" s="T554">i</ta>
            <ta e="T556" id="Seg_7065" s="T555">dĭ</ta>
            <ta e="T557" id="Seg_7066" s="T556">dĭ-zen</ta>
            <ta e="T558" id="Seg_7067" s="T557">ular-də</ta>
            <ta e="T559" id="Seg_7068" s="T558">bar</ta>
            <ta e="T560" id="Seg_7069" s="T559">kem-ziʔ</ta>
            <ta e="T562" id="Seg_7070" s="T561">kem-ziʔ</ta>
            <ta e="T563" id="Seg_7071" s="T562">bar</ta>
            <ta e="T564" id="Seg_7072" s="T563">dʼüʔ-də-bi</ta>
            <ta e="T565" id="Seg_7073" s="T564">püje-t</ta>
            <ta e="T566" id="Seg_7074" s="T565">i</ta>
            <ta e="T567" id="Seg_7075" s="T566">aŋ-də</ta>
            <ta e="T568" id="Seg_7076" s="T567">a</ta>
            <ta e="T569" id="Seg_7077" s="T568">ertə-n</ta>
            <ta e="T570" id="Seg_7078" s="T569">uʔbdə-bi-jəʔ</ta>
            <ta e="T571" id="Seg_7079" s="T570">măn-ntə-jəʔ</ta>
            <ta e="T572" id="Seg_7080" s="T571">šănap</ta>
            <ta e="T574" id="Seg_7081" s="T573">miʔ</ta>
            <ta e="T575" id="Seg_7082" s="T574">ular-də</ta>
            <ta e="T576" id="Seg_7083" s="T575">dĭ</ta>
            <ta e="T577" id="Seg_7084" s="T576">dĭ-m</ta>
            <ta e="T578" id="Seg_7085" s="T577">ular-də</ta>
            <ta e="T579" id="Seg_7086" s="T578">bar</ta>
            <ta e="T580" id="Seg_7087" s="T579">săj-nüzə-bi-jəʔ</ta>
            <ta e="T582" id="Seg_7088" s="T581">dĭgəttə</ta>
            <ta e="T583" id="Seg_7089" s="T582">dĭ</ta>
            <ta e="T584" id="Seg_7090" s="T583">uʔbdə-bi</ta>
            <ta e="T585" id="Seg_7091" s="T584">sima-bə</ta>
            <ta e="T586" id="Seg_7092" s="T585">bazə-bi</ta>
            <ta e="T587" id="Seg_7093" s="T586">kĭškə-bi</ta>
            <ta e="T588" id="Seg_7094" s="T587">šide</ta>
            <ta e="T589" id="Seg_7095" s="T588">ular</ta>
            <ta e="T590" id="Seg_7096" s="T589">i-bi</ta>
            <ta e="T591" id="Seg_7097" s="T590">i</ta>
            <ta e="T592" id="Seg_7098" s="T591">kandə-gA</ta>
            <ta e="T595" id="Seg_7099" s="T594">kan-luʔbdə-bi</ta>
            <ta e="T596" id="Seg_7100" s="T595">dĭgəttə</ta>
            <ta e="T597" id="Seg_7101" s="T596">šonə-gA</ta>
            <ta e="T598" id="Seg_7102" s="T597">šonə-gA</ta>
            <ta e="T599" id="Seg_7103" s="T598">dĭn</ta>
            <ta e="T600" id="Seg_7104" s="T599">bar</ta>
            <ta e="T601" id="Seg_7105" s="T600">krospa</ta>
            <ta e="T602" id="Seg_7106" s="T601">nu-laʔbə</ta>
            <ta e="T603" id="Seg_7107" s="T602">dĭ</ta>
            <ta e="T604" id="Seg_7108" s="T603">tĭl-bi</ta>
            <ta e="T605" id="Seg_7109" s="T604">dĭn</ta>
            <ta e="T606" id="Seg_7110" s="T605">nüke</ta>
            <ta e="T607" id="Seg_7111" s="T606">iʔbö-laʔbə</ta>
            <ta e="T608" id="Seg_7112" s="T607">dĭ</ta>
            <ta e="T609" id="Seg_7113" s="T608">nüke</ta>
            <ta e="T610" id="Seg_7114" s="T609">i-bi</ta>
            <ta e="T611" id="Seg_7115" s="T610">i</ta>
            <ta e="T612" id="Seg_7116" s="T611">šide</ta>
            <ta e="T613" id="Seg_7117" s="T612">ular</ta>
            <ta e="T614" id="Seg_7118" s="T613">i</ta>
            <ta e="T615" id="Seg_7119" s="T614">dĭ</ta>
            <ta e="T616" id="Seg_7120" s="T615">ular-zAŋ-də</ta>
            <ta e="T617" id="Seg_7121" s="T616">dĭ</ta>
            <ta e="T618" id="Seg_7122" s="T617">nüke-Tə</ta>
            <ta e="T619" id="Seg_7123" s="T618">uda-zAŋ-Tə</ta>
            <ta e="T620" id="Seg_7124" s="T619">sar-bi</ta>
            <ta e="T621" id="Seg_7125" s="T620">sĭj-də</ta>
            <ta e="T622" id="Seg_7126" s="T621">sĭj-gəndə</ta>
            <ta e="T623" id="Seg_7127" s="T622">tagaj</ta>
            <ta e="T625" id="Seg_7128" s="T624">păda-luʔbdə-bi</ta>
            <ta e="T626" id="Seg_7129" s="T625">dĭgəttə</ta>
            <ta e="T627" id="Seg_7130" s="T626">šo-bi</ta>
            <ta e="T629" id="Seg_7131" s="T628">dĭn</ta>
            <ta e="T630" id="Seg_7132" s="T629">šide</ta>
            <ta e="T631" id="Seg_7133" s="T630">koʔbdo</ta>
            <ta e="T632" id="Seg_7134" s="T631">i</ta>
            <ta e="T633" id="Seg_7135" s="T632">büzʼe</ta>
            <ta e="T634" id="Seg_7136" s="T633">amno-laʔbə</ta>
            <ta e="T635" id="Seg_7137" s="T634">tʼăbaktər-liA</ta>
            <ta e="T636" id="Seg_7138" s="T635">tʼăbaktər-liA</ta>
            <ta e="T637" id="Seg_7139" s="T636">măn</ta>
            <ta e="T638" id="Seg_7140" s="T637">dĭn</ta>
            <ta e="T639" id="Seg_7141" s="T638">nüke</ta>
            <ta e="T640" id="Seg_7142" s="T639">amno-laʔbə</ta>
            <ta e="T641" id="Seg_7143" s="T640">ular-ziʔ</ta>
            <ta e="T642" id="Seg_7144" s="T641">a</ta>
            <ta e="T643" id="Seg_7145" s="T642">bos-də</ta>
            <ta e="T645" id="Seg_7146" s="T644">sĭj-gəndə</ta>
            <ta e="T646" id="Seg_7147" s="T645">tagaj</ta>
            <ta e="T647" id="Seg_7148" s="T646">amno-laʔbə</ta>
            <ta e="T648" id="Seg_7149" s="T647">a</ta>
            <ta e="T649" id="Seg_7150" s="T648">koʔbdo-zAŋ</ta>
            <ta e="T650" id="Seg_7151" s="T649">miʔ</ta>
            <ta e="T651" id="Seg_7152" s="T650">kan-lV-bəj</ta>
            <ta e="T652" id="Seg_7153" s="T651">det-lV-bəj</ta>
            <ta e="T653" id="Seg_7154" s="T652">nüke-l</ta>
            <ta e="T654" id="Seg_7155" s="T653">tăn</ta>
            <ta e="T655" id="Seg_7156" s="T654">dĭ</ta>
            <ta e="T656" id="Seg_7157" s="T655">ugaːndə</ta>
            <ta e="T657" id="Seg_7158" s="T656">pim-liA</ta>
            <ta e="T658" id="Seg_7159" s="T657">ato</ta>
            <ta e="T659" id="Seg_7160" s="T658">dʼagar-lV-j</ta>
            <ta e="T660" id="Seg_7161" s="T659">bos-də</ta>
            <ta e="T661" id="Seg_7162" s="T660">bos-də</ta>
            <ta e="T663" id="Seg_7163" s="T662">dĭgəttə</ta>
            <ta e="T664" id="Seg_7164" s="T663">dĭ-zAŋ</ta>
            <ta e="T665" id="Seg_7165" s="T664">koʔbdo-zAŋ</ta>
            <ta e="T666" id="Seg_7166" s="T665">nuʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T667" id="Seg_7167" s="T666">šo-bi-jəʔ</ta>
            <ta e="T668" id="Seg_7168" s="T667">dʼagar-luʔbdə-bi</ta>
            <ta e="T669" id="Seg_7169" s="T668">ular-də</ta>
            <ta e="T670" id="Seg_7170" s="T669">bar</ta>
            <ta e="T672" id="Seg_7171" s="T671">nuʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T673" id="Seg_7172" s="T672">nüke-m</ta>
            <ta e="T674" id="Seg_7173" s="T673">dĭgəttə</ta>
            <ta e="T675" id="Seg_7174" s="T674">dĭ</ta>
            <ta e="T676" id="Seg_7175" s="T675">bar</ta>
            <ta e="T677" id="Seg_7176" s="T676">saʔmə-luʔbdə-bi</ta>
            <ta e="T678" id="Seg_7177" s="T677">Kodur</ta>
            <ta e="T679" id="Seg_7178" s="T678">tʼor-bi</ta>
            <ta e="T680" id="Seg_7179" s="T679">tʼor-bi</ta>
            <ta e="T681" id="Seg_7180" s="T680">dĭ</ta>
            <ta e="T682" id="Seg_7181" s="T681">büzʼe</ta>
            <ta e="T683" id="Seg_7182" s="T682">măn-ntə</ta>
            <ta e="T684" id="Seg_7183" s="T683">i-t</ta>
            <ta e="T685" id="Seg_7184" s="T684">onʼiʔ</ta>
            <ta e="T686" id="Seg_7185" s="T685">koʔbdo</ta>
            <ta e="T687" id="Seg_7186" s="T686">našto</ta>
            <ta e="T688" id="Seg_7187" s="T687">măna</ta>
            <ta e="T689" id="Seg_7188" s="T688">onʼiʔ</ta>
            <ta e="T690" id="Seg_7189" s="T689">măn</ta>
            <ta e="T691" id="Seg_7190" s="T690">nüke-m</ta>
            <ta e="T692" id="Seg_7191" s="T691">lutʼšə</ta>
            <ta e="T693" id="Seg_7192" s="T692">i-bi</ta>
            <ta e="T694" id="Seg_7193" s="T693">no</ta>
            <ta e="T695" id="Seg_7194" s="T694">i-t</ta>
            <ta e="T696" id="Seg_7195" s="T695">šide</ta>
            <ta e="T697" id="Seg_7196" s="T696">dĭgəttə</ta>
            <ta e="T698" id="Seg_7197" s="T697">dĭ</ta>
            <ta e="T699" id="Seg_7198" s="T698">uʔbdə-bi</ta>
            <ta e="T700" id="Seg_7199" s="T699">bazoʔ</ta>
            <ta e="T701" id="Seg_7200" s="T700">bazə-jd-bi</ta>
            <ta e="T702" id="Seg_7201" s="T701">kadul-də</ta>
            <ta e="T703" id="Seg_7202" s="T702">i</ta>
            <ta e="T704" id="Seg_7203" s="T703">šide</ta>
            <ta e="T705" id="Seg_7204" s="T704">koʔbdo</ta>
            <ta e="T706" id="Seg_7205" s="T705">i-bi</ta>
            <ta e="T707" id="Seg_7206" s="T706">i</ta>
            <ta e="T708" id="Seg_7207" s="T707">kan-bi</ta>
            <ta e="T709" id="Seg_7208" s="T708">dĭgəttə</ta>
            <ta e="T710" id="Seg_7209" s="T709">šonə-gA</ta>
            <ta e="T711" id="Seg_7210" s="T710">šonə-gA</ta>
            <ta e="T712" id="Seg_7211" s="T711">dĭn</ta>
            <ta e="T713" id="Seg_7212" s="T712">ular-jəʔ</ta>
            <ta e="T714" id="Seg_7213" s="T713">mĭn-laʔbə-jəʔ</ta>
            <ta e="T715" id="Seg_7214" s="T714">dĭgəttə</ta>
            <ta e="T716" id="Seg_7215" s="T715">šo-bi</ta>
            <ta e="T717" id="Seg_7216" s="T716">pastux-Tə</ta>
            <ta e="T718" id="Seg_7217" s="T717">dön</ta>
            <ta e="T719" id="Seg_7218" s="T718">măn</ta>
            <ta e="T720" id="Seg_7219" s="T719">ular</ta>
            <ta e="T721" id="Seg_7220" s="T720">da</ta>
            <ta e="T723" id="Seg_7221" s="T722">onʼiʔ</ta>
            <ta e="T725" id="Seg_7222" s="T724">amnəl-bi</ta>
            <ta e="T726" id="Seg_7223" s="T725">bü-Tə</ta>
            <ta e="T728" id="Seg_7224" s="T727">toʔ-gəndə</ta>
            <ta e="T729" id="Seg_7225" s="T728">onʼiʔ</ta>
            <ta e="T730" id="Seg_7226" s="T729">amnəl-bə-bi</ta>
            <ta e="T731" id="Seg_7227" s="T730">dʼije-Kən</ta>
            <ta e="T732" id="Seg_7228" s="T731">dĭgəttə</ta>
            <ta e="T733" id="Seg_7229" s="T732">măn-laʔbə</ta>
            <ta e="T734" id="Seg_7230" s="T733">măn</ta>
            <ta e="T735" id="Seg_7231" s="T734">ular</ta>
            <ta e="T736" id="Seg_7232" s="T735">no</ta>
            <ta e="T737" id="Seg_7233" s="T736">surar-t</ta>
            <ta e="T740" id="Seg_7234" s="T739">surar-t</ta>
            <ta e="T741" id="Seg_7235" s="T740">dĭ</ta>
            <ta e="T742" id="Seg_7236" s="T741">surar-luʔbdə-bi</ta>
            <ta e="T743" id="Seg_7237" s="T742">Kodur-ə-n</ta>
            <ta e="T744" id="Seg_7238" s="T743">ular-zAŋ-də</ta>
            <ta e="T745" id="Seg_7239" s="T744">dĭgəttə</ta>
            <ta e="T746" id="Seg_7240" s="T745">dʼije-Kən</ta>
            <ta e="T747" id="Seg_7241" s="T746">kuza-Tə</ta>
            <ta e="T748" id="Seg_7242" s="T747">surar-t</ta>
            <ta e="T749" id="Seg_7243" s="T748">surar-bi</ta>
            <ta e="T750" id="Seg_7244" s="T749">dĭ</ta>
            <ta e="T751" id="Seg_7245" s="T750">măn-ntə</ta>
            <ta e="T752" id="Seg_7246" s="T751">Kodur-ə-n</ta>
            <ta e="T753" id="Seg_7247" s="T752">ular-də</ta>
            <ta e="T754" id="Seg_7248" s="T753">dĭgəttə</ta>
            <ta e="T756" id="Seg_7249" s="T755">i-bi</ta>
            <ta e="T757" id="Seg_7250" s="T756">ular-də</ta>
            <ta e="T759" id="Seg_7251" s="T758">bazoʔ</ta>
            <ta e="T761" id="Seg_7252" s="T760">šo-bi</ta>
            <ta e="T762" id="Seg_7253" s="T761">kuza</ta>
            <ta e="T763" id="Seg_7254" s="T762">ine-jəʔ</ta>
            <ta e="T765" id="Seg_7255" s="T764">amno-laʔbə</ta>
            <ta e="T766" id="Seg_7256" s="T765">măn</ta>
            <ta e="T767" id="Seg_7257" s="T766">ine-jəʔ</ta>
            <ta e="T768" id="Seg_7258" s="T767">a</ta>
            <ta e="T769" id="Seg_7259" s="T768">dĭ</ta>
            <ta e="T770" id="Seg_7260" s="T769">măn-ntə</ta>
            <ta e="T771" id="Seg_7261" s="T770">dʼok</ta>
            <ta e="T772" id="Seg_7262" s="T771">măn</ta>
            <ta e="T773" id="Seg_7263" s="T772">ine-jəʔ</ta>
            <ta e="T774" id="Seg_7264" s="T773">no</ta>
            <ta e="T775" id="Seg_7265" s="T774">surar-t</ta>
            <ta e="T776" id="Seg_7266" s="T775">bü-Tə</ta>
            <ta e="T777" id="Seg_7267" s="T776">kuza-m</ta>
            <ta e="T778" id="Seg_7268" s="T777">dĭ</ta>
            <ta e="T779" id="Seg_7269" s="T778">surar-bi</ta>
            <ta e="T780" id="Seg_7270" s="T779">Kodur-ə-n</ta>
            <ta e="T781" id="Seg_7271" s="T780">ine-jəʔ</ta>
            <ta e="T782" id="Seg_7272" s="T781">dĭgəttə</ta>
            <ta e="T783" id="Seg_7273" s="T782">dʼije-n</ta>
            <ta e="T784" id="Seg_7274" s="T783">kuza-Tə</ta>
            <ta e="T785" id="Seg_7275" s="T784">surar-t</ta>
            <ta e="T786" id="Seg_7276" s="T785">dĭ</ta>
            <ta e="T787" id="Seg_7277" s="T786">surar-bi</ta>
            <ta e="T788" id="Seg_7278" s="T787">Kodur-ə-n</ta>
            <ta e="T789" id="Seg_7279" s="T788">ine-jəʔ</ta>
            <ta e="T790" id="Seg_7280" s="T789">dĭ</ta>
            <ta e="T791" id="Seg_7281" s="T790">bar</ta>
            <ta e="T792" id="Seg_7282" s="T791">öʔlu-bi</ta>
            <ta e="T793" id="Seg_7283" s="T792">dĭgəttə</ta>
            <ta e="T794" id="Seg_7284" s="T793">tüžöj-jəʔ</ta>
            <ta e="T795" id="Seg_7285" s="T794">mĭn-laʔbə-jəʔ</ta>
            <ta e="T796" id="Seg_7286" s="T795">i</ta>
            <ta e="T797" id="Seg_7287" s="T796">dĭn</ta>
            <ta e="T798" id="Seg_7288" s="T797">tože</ta>
            <ta e="T799" id="Seg_7289" s="T798">kuza</ta>
            <ta e="T800" id="Seg_7290" s="T799">măndo-laʔbə</ta>
            <ta e="T801" id="Seg_7291" s="T800">tüžöj-jəʔ</ta>
            <ta e="T802" id="Seg_7292" s="T801">dĭ</ta>
            <ta e="T803" id="Seg_7293" s="T802">šo-bi</ta>
            <ta e="T804" id="Seg_7294" s="T803">măn</ta>
            <ta e="T805" id="Seg_7295" s="T804">tüžöj-jəʔ</ta>
            <ta e="T806" id="Seg_7296" s="T805">dʼok</ta>
            <ta e="T807" id="Seg_7297" s="T806">ej</ta>
            <ta e="T808" id="Seg_7298" s="T807">tăn</ta>
            <ta e="T809" id="Seg_7299" s="T808">no</ta>
            <ta e="T810" id="Seg_7300" s="T809">surar-ʔ</ta>
            <ta e="T811" id="Seg_7301" s="T810">bü-Kən</ta>
            <ta e="T812" id="Seg_7302" s="T811">kuza</ta>
            <ta e="T813" id="Seg_7303" s="T812">dĭgəttə</ta>
            <ta e="T814" id="Seg_7304" s="T813">dʼije-Kən</ta>
            <ta e="T815" id="Seg_7305" s="T814">dĭ</ta>
            <ta e="T816" id="Seg_7306" s="T815">surar-bi</ta>
            <ta e="T817" id="Seg_7307" s="T816">šində-n</ta>
            <ta e="T818" id="Seg_7308" s="T817">tüžöj-də</ta>
            <ta e="T819" id="Seg_7309" s="T818">dĭ</ta>
            <ta e="T820" id="Seg_7310" s="T819">dĭ</ta>
            <ta e="T821" id="Seg_7311" s="T820">măn-bi</ta>
            <ta e="T822" id="Seg_7312" s="T821">Kodur-n</ta>
            <ta e="T823" id="Seg_7313" s="T822">tüžöj-də</ta>
            <ta e="T824" id="Seg_7314" s="T823">i</ta>
            <ta e="T825" id="Seg_7315" s="T824">dʼije-Kən</ta>
            <ta e="T826" id="Seg_7316" s="T825">surar-bi</ta>
            <ta e="T827" id="Seg_7317" s="T826">šində-n</ta>
            <ta e="T828" id="Seg_7318" s="T827">tüžöj-də</ta>
            <ta e="T829" id="Seg_7319" s="T828">Kodur-ə-n</ta>
            <ta e="T830" id="Seg_7320" s="T829">tüžöj-də</ta>
            <ta e="T831" id="Seg_7321" s="T830">dĭgəttə</ta>
            <ta e="T832" id="Seg_7322" s="T831">dĭ</ta>
            <ta e="T833" id="Seg_7323" s="T832">davaj</ta>
            <ta e="T834" id="Seg_7324" s="T833">nüj-zittə</ta>
            <ta e="T835" id="Seg_7325" s="T834">nüjnə</ta>
            <ta e="T836" id="Seg_7326" s="T835">măn</ta>
            <ta e="T837" id="Seg_7327" s="T836">lăpatka</ta>
            <ta e="T840" id="Seg_7328" s="T839">i-bi-m</ta>
            <ta e="T842" id="Seg_7329" s="T841">ular</ta>
            <ta e="T843" id="Seg_7330" s="T842">i-bi-m</ta>
            <ta e="T844" id="Seg_7331" s="T843">ular-də</ta>
            <ta e="T845" id="Seg_7332" s="T844">šide</ta>
            <ta e="T846" id="Seg_7333" s="T845">ular</ta>
            <ta e="T847" id="Seg_7334" s="T846">i-bi-m</ta>
            <ta e="T848" id="Seg_7335" s="T847">šide</ta>
            <ta e="T849" id="Seg_7336" s="T848">ular</ta>
            <ta e="T850" id="Seg_7337" s="T849">i-bi-m</ta>
            <ta e="T851" id="Seg_7338" s="T850">kan-bi-m</ta>
            <ta e="T852" id="Seg_7339" s="T851">nüke</ta>
            <ta e="T853" id="Seg_7340" s="T852">tʼo-Kən</ta>
            <ta e="T854" id="Seg_7341" s="T853">i-bi-m</ta>
            <ta e="T855" id="Seg_7342" s="T854">i</ta>
            <ta e="T856" id="Seg_7343" s="T855">šide</ta>
            <ta e="T857" id="Seg_7344" s="T856">ular</ta>
            <ta e="T858" id="Seg_7345" s="T857">i</ta>
            <ta e="T859" id="Seg_7346" s="T858">nüke</ta>
            <ta e="T860" id="Seg_7347" s="T859">tagaj</ta>
            <ta e="T863" id="Seg_7348" s="T862">sĭj-də</ta>
            <ta e="T865" id="Seg_7349" s="T864">tagaj</ta>
            <ta e="T866" id="Seg_7350" s="T865">sĭj-də</ta>
            <ta e="T868" id="Seg_7351" s="T867">müʔbdə-luʔbdə-bi</ta>
            <ta e="T869" id="Seg_7352" s="T868">a</ta>
            <ta e="T870" id="Seg_7353" s="T869">bos-də</ta>
            <ta e="T871" id="Seg_7354" s="T870">šo-bi</ta>
            <ta e="T872" id="Seg_7355" s="T871">măn</ta>
            <ta e="T873" id="Seg_7356" s="T872">dĭn</ta>
            <ta e="T874" id="Seg_7357" s="T873">nüke-m</ta>
            <ta e="T875" id="Seg_7358" s="T874">i-gA</ta>
            <ta e="T876" id="Seg_7359" s="T875">i</ta>
            <ta e="T877" id="Seg_7360" s="T876">šide</ta>
            <ta e="T878" id="Seg_7361" s="T877">ular</ta>
            <ta e="T879" id="Seg_7362" s="T878">i</ta>
            <ta e="T880" id="Seg_7363" s="T879">šide</ta>
            <ta e="T881" id="Seg_7364" s="T880">koʔbdo-zAŋ</ta>
            <ta e="T882" id="Seg_7365" s="T881">i</ta>
            <ta e="T883" id="Seg_7366" s="T882">büzʼe</ta>
            <ta e="T884" id="Seg_7367" s="T883">amno-laʔbə</ta>
            <ta e="T885" id="Seg_7368" s="T884">dĭ</ta>
            <ta e="T886" id="Seg_7369" s="T885">bar</ta>
            <ta e="T887" id="Seg_7370" s="T886">büzʼe</ta>
            <ta e="T888" id="Seg_7371" s="T887">tʼăbaktər-liA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T3" id="Seg_7372" s="T2">share-INF.LAT</ta>
            <ta e="T4" id="Seg_7373" s="T3">I.NOM</ta>
            <ta e="T5" id="Seg_7374" s="T4">this-LAT</ta>
            <ta e="T6" id="Seg_7375" s="T5">put-PST-1SG</ta>
            <ta e="T7" id="Seg_7376" s="T6">mushroom-NOM/GEN/ACC.3PL</ta>
            <ta e="T8" id="Seg_7377" s="T7">butter.[NOM.SG]</ta>
            <ta e="T9" id="Seg_7378" s="T8">give-PST-1SG</ta>
            <ta e="T10" id="Seg_7379" s="T9">money.[NOM.SG]</ta>
            <ta e="T11" id="Seg_7380" s="T10">give-PST-1SG</ta>
            <ta e="T12" id="Seg_7381" s="T11">one.[NOM.SG]</ta>
            <ta e="T13" id="Seg_7382" s="T12">two.[NOM.SG]</ta>
            <ta e="T14" id="Seg_7383" s="T13">three.[NOM.SG]</ta>
            <ta e="T15" id="Seg_7384" s="T14">four.[NOM.SG]</ta>
            <ta e="T16" id="Seg_7385" s="T15">this-PL</ta>
            <ta e="T17" id="Seg_7386" s="T16">house.[NOM.SG]</ta>
            <ta e="T18" id="Seg_7387" s="T17">take-PST-3PL</ta>
            <ta e="T19" id="Seg_7388" s="T18">who.[NOM.SG]=INDEF</ta>
            <ta e="T20" id="Seg_7389" s="T19">PTCL</ta>
            <ta e="T21" id="Seg_7390" s="T20">sell-DUR.[3SG]</ta>
            <ta e="T22" id="Seg_7391" s="T21">and</ta>
            <ta e="T23" id="Seg_7392" s="T22">money.[NOM.SG]</ta>
            <ta e="T24" id="Seg_7393" s="T23">NEG</ta>
            <ta e="T25" id="Seg_7394" s="T24">suffice-PRS.[3SG]</ta>
            <ta e="T27" id="Seg_7395" s="T26">this-PL</ta>
            <ta e="T28" id="Seg_7396" s="T27">this.[NOM.SG]</ta>
            <ta e="T30" id="Seg_7397" s="T29">house.[NOM.SG]</ta>
            <ta e="T31" id="Seg_7398" s="T30">sell-CVB</ta>
            <ta e="T32" id="Seg_7399" s="T31">take-PST-3PL</ta>
            <ta e="T34" id="Seg_7400" s="T33">and</ta>
            <ta e="T35" id="Seg_7401" s="T34">what.kind</ta>
            <ta e="T36" id="Seg_7402" s="T35">man-LOC</ta>
            <ta e="T37" id="Seg_7403" s="T36">NEG.EX.[3SG]</ta>
            <ta e="T39" id="Seg_7404" s="T38">%borrow</ta>
            <ta e="T40" id="Seg_7405" s="T39">money.[NOM.SG]</ta>
            <ta e="T41" id="Seg_7406" s="T40">give-INF.LAT</ta>
            <ta e="T42" id="Seg_7407" s="T41">this-GEN</ta>
            <ta e="T43" id="Seg_7408" s="T42">cow-NOM/GEN/ACC.3SG</ta>
            <ta e="T44" id="Seg_7409" s="T43">people.[NOM.SG]</ta>
            <ta e="T45" id="Seg_7410" s="T44">take-PST-3PL</ta>
            <ta e="T46" id="Seg_7411" s="T45">what.[NOM.SG]</ta>
            <ta e="T47" id="Seg_7412" s="T46">be-PRS.[3SG]</ta>
            <ta e="T48" id="Seg_7413" s="T47">PTCL</ta>
            <ta e="T49" id="Seg_7414" s="T48">take-MOM-PST-3PL</ta>
            <ta e="T50" id="Seg_7415" s="T49">and</ta>
            <ta e="T51" id="Seg_7416" s="T50">give-PST-3PL</ta>
            <ta e="T52" id="Seg_7417" s="T51">money.[NOM.SG]</ta>
            <ta e="T53" id="Seg_7418" s="T52">there</ta>
            <ta e="T55" id="Seg_7419" s="T54">I.NOM</ta>
            <ta e="T56" id="Seg_7420" s="T55">relative-NOM/GEN/ACC.1SG</ta>
            <ta e="T892" id="Seg_7421" s="T56">go-CVB</ta>
            <ta e="T57" id="Seg_7422" s="T892">disappear-PST.[3SG]</ta>
            <ta e="T58" id="Seg_7423" s="T57">town-LAT</ta>
            <ta e="T59" id="Seg_7424" s="T58">and</ta>
            <ta e="T60" id="Seg_7425" s="T59">there</ta>
            <ta e="T61" id="Seg_7426" s="T60">machine.[NOM.SG]</ta>
            <ta e="T62" id="Seg_7427" s="T61">take-FUT-3SG</ta>
            <ta e="T63" id="Seg_7428" s="T62">clothing.[NOM.SG]</ta>
            <ta e="T64" id="Seg_7429" s="T63">wash-INF.LAT</ta>
            <ta e="T65" id="Seg_7430" s="T64">shirt.[NOM.SG]</ta>
            <ta e="T66" id="Seg_7431" s="T65">shirt-PL</ta>
            <ta e="T67" id="Seg_7432" s="T66">pants-PL</ta>
            <ta e="T68" id="Seg_7433" s="T67">all</ta>
            <ta e="T70" id="Seg_7434" s="T69">what.[NOM.SG]</ta>
            <ta e="T71" id="Seg_7435" s="T70">be-PRS.[3SG]</ta>
            <ta e="T72" id="Seg_7436" s="T71">wash-INF.LAT</ta>
            <ta e="T74" id="Seg_7437" s="T73">man.[NOM.SG]</ta>
            <ta e="T75" id="Seg_7438" s="T74">and</ta>
            <ta e="T76" id="Seg_7439" s="T75">woman.[NOM.SG]</ta>
            <ta e="T77" id="Seg_7440" s="T76">live-PST-3PL</ta>
            <ta e="T78" id="Seg_7441" s="T77">very</ta>
            <ta e="T79" id="Seg_7442" s="T78">good</ta>
            <ta e="T80" id="Seg_7443" s="T79">pity-PST-3PL</ta>
            <ta e="T83" id="Seg_7444" s="T81">one.[NOM.SG]</ta>
            <ta e="T84" id="Seg_7445" s="T83">one.[NOM.SG]</ta>
            <ta e="T85" id="Seg_7446" s="T84">one-LAT</ta>
            <ta e="T86" id="Seg_7447" s="T85">pity-PST-3PL</ta>
            <ta e="T87" id="Seg_7448" s="T86">PTCL</ta>
            <ta e="T88" id="Seg_7449" s="T87">%%-%%-3PL</ta>
            <ta e="T89" id="Seg_7450" s="T88">kiss-DUR-3PL</ta>
            <ta e="T90" id="Seg_7451" s="T89">and</ta>
            <ta e="T91" id="Seg_7452" s="T90">this-PL</ta>
            <ta e="T92" id="Seg_7453" s="T91">child-PL-LAT</ta>
            <ta e="T93" id="Seg_7454" s="T92">be-PST-3PL</ta>
            <ta e="T94" id="Seg_7455" s="T93">very</ta>
            <ta e="T95" id="Seg_7456" s="T94">child-PL-LAT</ta>
            <ta e="T96" id="Seg_7457" s="T95">good</ta>
            <ta e="T97" id="Seg_7458" s="T96">what.[NOM.SG]</ta>
            <ta e="T98" id="Seg_7459" s="T97">say-FUT-2SG</ta>
            <ta e="T99" id="Seg_7460" s="T98">this-PL</ta>
            <ta e="T100" id="Seg_7461" s="T99">listen-DUR-3PL</ta>
            <ta e="T102" id="Seg_7462" s="T101">and</ta>
            <ta e="T103" id="Seg_7463" s="T102">we.NOM</ta>
            <ta e="T104" id="Seg_7464" s="T103">child-PL</ta>
            <ta e="T105" id="Seg_7465" s="T104">very</ta>
            <ta e="T106" id="Seg_7466" s="T105">NEG</ta>
            <ta e="T107" id="Seg_7467" s="T106">good</ta>
            <ta e="T108" id="Seg_7468" s="T107">fight-DUR-3PL</ta>
            <ta e="T109" id="Seg_7469" s="T108">scold-DES-DUR-3PL</ta>
            <ta e="T110" id="Seg_7470" s="T109">NEG</ta>
            <ta e="T111" id="Seg_7471" s="T110">listen-DUR-3PL</ta>
            <ta e="T112" id="Seg_7472" s="T111">I.NOM</ta>
            <ta e="T113" id="Seg_7473" s="T112">this-ACC.PL</ta>
            <ta e="T114" id="Seg_7474" s="T113">PTCL</ta>
            <ta e="T115" id="Seg_7475" s="T114">beat-DUR-1SG</ta>
            <ta e="T117" id="Seg_7476" s="T116">child-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T118" id="Seg_7477" s="T117">learn-TR-PRS-1SG</ta>
            <ta e="T119" id="Seg_7478" s="T118">paper.[NOM.SG]</ta>
            <ta e="T120" id="Seg_7479" s="T119">write-INF.LAT</ta>
            <ta e="T122" id="Seg_7480" s="T121">Kamassian-PL</ta>
            <ta e="T123" id="Seg_7481" s="T122">live-PST-3PL</ta>
            <ta e="T124" id="Seg_7482" s="T123">who.[NOM.SG]</ta>
            <ta e="T125" id="Seg_7483" s="T124">very</ta>
            <ta e="T126" id="Seg_7484" s="T125">money.[NOM.SG]</ta>
            <ta e="T127" id="Seg_7485" s="T126">many</ta>
            <ta e="T128" id="Seg_7486" s="T127">and</ta>
            <ta e="T129" id="Seg_7487" s="T128">who-GEN</ta>
            <ta e="T130" id="Seg_7488" s="T129">NEG.EX.[3SG]</ta>
            <ta e="T131" id="Seg_7489" s="T130">this.[NOM.SG]</ta>
            <ta e="T132" id="Seg_7490" s="T131">vodka.[NOM.SG]</ta>
            <ta e="T133" id="Seg_7491" s="T132">drink-PRS.[3SG]</ta>
            <ta e="T134" id="Seg_7492" s="T133">PTCL</ta>
            <ta e="T135" id="Seg_7493" s="T134">money.[NOM.SG]</ta>
            <ta e="T136" id="Seg_7494" s="T135">drink-MOM-FUT-3SG</ta>
            <ta e="T137" id="Seg_7495" s="T136">then</ta>
            <ta e="T138" id="Seg_7496" s="T137">what.[NOM.SG]=INDEF</ta>
            <ta e="T139" id="Seg_7497" s="T138">NEG.EX.[3SG]</ta>
            <ta e="T140" id="Seg_7498" s="T139">work-PRS.[3SG]</ta>
            <ta e="T141" id="Seg_7499" s="T140">work-PRS.[3SG]</ta>
            <ta e="T142" id="Seg_7500" s="T141">PTCL</ta>
            <ta e="T143" id="Seg_7501" s="T142">drink-MOM-FUT-3SG</ta>
            <ta e="T146" id="Seg_7502" s="T145">Siberia-LOC</ta>
            <ta e="T147" id="Seg_7503" s="T146">very</ta>
            <ta e="T148" id="Seg_7504" s="T147">many</ta>
            <ta e="T149" id="Seg_7505" s="T148">Kamassian-PL</ta>
            <ta e="T150" id="Seg_7506" s="T149">live-PST-3PL</ta>
            <ta e="T151" id="Seg_7507" s="T150">then</ta>
            <ta e="T152" id="Seg_7508" s="T151">snow.[NOM.SG]</ta>
            <ta e="T153" id="Seg_7509" s="T152">tree.[NOM.SG]</ta>
            <ta e="T154" id="Seg_7510" s="T153">grow-MOM-PST.[3SG]</ta>
            <ta e="T155" id="Seg_7511" s="T154">this-PL</ta>
            <ta e="T156" id="Seg_7512" s="T155">say-PRS-3PL</ta>
            <ta e="T157" id="Seg_7513" s="T156">well</ta>
            <ta e="T159" id="Seg_7514" s="T158">Russian</ta>
            <ta e="T160" id="Seg_7515" s="T159">people.[NOM.SG]</ta>
            <ta e="T161" id="Seg_7516" s="T160">come-FUT-3PL</ta>
            <ta e="T162" id="Seg_7517" s="T161">we.LAT</ta>
            <ta e="T163" id="Seg_7518" s="T162">NEG</ta>
            <ta e="T164" id="Seg_7519" s="T163">good</ta>
            <ta e="T165" id="Seg_7520" s="T164">become-FUT-3SG</ta>
            <ta e="T166" id="Seg_7521" s="T165">then</ta>
            <ta e="T167" id="Seg_7522" s="T166">storehouse-PL</ta>
            <ta e="T168" id="Seg_7523" s="T167">make-PST-3PL</ta>
            <ta e="T169" id="Seg_7524" s="T168">there</ta>
            <ta e="T170" id="Seg_7525" s="T169">stone-PL</ta>
            <ta e="T171" id="Seg_7526" s="T170">carry-PST-3PL</ta>
            <ta e="T172" id="Seg_7527" s="T171">and</ta>
            <ta e="T173" id="Seg_7528" s="T172">axe.[NOM.SG]</ta>
            <ta e="T174" id="Seg_7529" s="T173">take-PST-3PL</ta>
            <ta e="T175" id="Seg_7530" s="T174">cut-PST-3PL</ta>
            <ta e="T176" id="Seg_7531" s="T175">and</ta>
            <ta e="T177" id="Seg_7532" s="T176">this.[NOM.SG]</ta>
            <ta e="T178" id="Seg_7533" s="T177">fall-MOM-PST.[3SG]</ta>
            <ta e="T179" id="Seg_7534" s="T178">this-ACC.PL</ta>
            <ta e="T180" id="Seg_7535" s="T179">this-PL</ta>
            <ta e="T181" id="Seg_7536" s="T180">PTCL</ta>
            <ta e="T182" id="Seg_7537" s="T181">there</ta>
            <ta e="T183" id="Seg_7538" s="T182">die-PST-3PL</ta>
            <ta e="T891" id="Seg_7539" s="T184">better</ta>
            <ta e="T185" id="Seg_7540" s="T891">I.LAT</ta>
            <ta e="T186" id="Seg_7541" s="T185">I.NOM</ta>
            <ta e="T187" id="Seg_7542" s="T186">IRREAL</ta>
            <ta e="T188" id="Seg_7543" s="T187">die-PST-1SG</ta>
            <ta e="T190" id="Seg_7544" s="T189">child-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T192" id="Seg_7545" s="T190">such.[NOM.SG]</ta>
            <ta e="T193" id="Seg_7546" s="T192">NEG</ta>
            <ta e="T194" id="Seg_7547" s="T193">good-PL</ta>
            <ta e="T195" id="Seg_7548" s="T194">PTCL</ta>
            <ta e="T196" id="Seg_7549" s="T195">steal-DUR-3PL</ta>
            <ta e="T197" id="Seg_7550" s="T196">people-COM</ta>
            <ta e="T198" id="Seg_7551" s="T197">fight-DUR-3PL</ta>
            <ta e="T200" id="Seg_7552" s="T199">PTCL</ta>
            <ta e="T201" id="Seg_7553" s="T200">vodka.[NOM.SG]</ta>
            <ta e="T202" id="Seg_7554" s="T201">drink-DUR-3PL</ta>
            <ta e="T203" id="Seg_7555" s="T202">even</ta>
            <ta e="T204" id="Seg_7556" s="T203">eye-INS</ta>
            <ta e="T205" id="Seg_7557" s="T204">NEG</ta>
            <ta e="T206" id="Seg_7558" s="T205">want-PRS.[3SG]</ta>
            <ta e="T207" id="Seg_7559" s="T206">look-FRQ-INF.LAT</ta>
            <ta e="T208" id="Seg_7560" s="T207">this-PL-LAT</ta>
            <ta e="T210" id="Seg_7561" s="T209">one.[NOM.SG]</ta>
            <ta e="T211" id="Seg_7562" s="T210">woman-GEN</ta>
            <ta e="T212" id="Seg_7563" s="T211">three.[NOM.SG]</ta>
            <ta e="T213" id="Seg_7564" s="T212">son.[NOM.SG]</ta>
            <ta e="T214" id="Seg_7565" s="T213">be-PST.[3SG]</ta>
            <ta e="T215" id="Seg_7566" s="T214">two.[NOM.SG]</ta>
            <ta e="T216" id="Seg_7567" s="T215">good</ta>
            <ta e="T217" id="Seg_7568" s="T216">and</ta>
            <ta e="T218" id="Seg_7569" s="T217">one.[NOM.SG]</ta>
            <ta e="T219" id="Seg_7570" s="T218">NEG</ta>
            <ta e="T220" id="Seg_7571" s="T219">good</ta>
            <ta e="T221" id="Seg_7572" s="T220">all</ta>
            <ta e="T222" id="Seg_7573" s="T221">this-ACC</ta>
            <ta e="T223" id="Seg_7574" s="T222">beat-PST-3PL</ta>
            <ta e="T224" id="Seg_7575" s="T223">foot-INS</ta>
            <ta e="T225" id="Seg_7576" s="T224">PTCL</ta>
            <ta e="T226" id="Seg_7577" s="T225">and</ta>
            <ta e="T228" id="Seg_7578" s="T227">fist-INS</ta>
            <ta e="T229" id="Seg_7579" s="T228">and</ta>
            <ta e="T230" id="Seg_7580" s="T229">tree-INS</ta>
            <ta e="T231" id="Seg_7581" s="T230">PTCL</ta>
            <ta e="T234" id="Seg_7582" s="T233">hit-MULT-PST-3PL</ta>
            <ta e="T235" id="Seg_7583" s="T234">and</ta>
            <ta e="T236" id="Seg_7584" s="T235">I.NOM</ta>
            <ta e="T237" id="Seg_7585" s="T236">%%</ta>
            <ta e="T239" id="Seg_7586" s="T237">say-PST-1SG</ta>
            <ta e="T241" id="Seg_7587" s="T240">and</ta>
            <ta e="T242" id="Seg_7588" s="T241">I.NOM</ta>
            <ta e="T243" id="Seg_7589" s="T242">%%</ta>
            <ta e="T244" id="Seg_7590" s="T243">say-PST-1SG</ta>
            <ta e="T245" id="Seg_7591" s="T244">NEG.AUX-IMP.2SG</ta>
            <ta e="T246" id="Seg_7592" s="T245">%beat-CNG</ta>
            <ta e="T247" id="Seg_7593" s="T246">maybe</ta>
            <ta e="T248" id="Seg_7594" s="T247">grow-FUT-3SG</ta>
            <ta e="T249" id="Seg_7595" s="T248">silly.[NOM.SG]</ta>
            <ta e="T250" id="Seg_7596" s="T249">many</ta>
            <ta e="T251" id="Seg_7597" s="T250">become-FUT-3SG</ta>
            <ta e="T252" id="Seg_7598" s="T251">big.[NOM.SG]</ta>
            <ta e="T253" id="Seg_7599" s="T252">become-FUT-3SG</ta>
            <ta e="T254" id="Seg_7600" s="T253">maybe</ta>
            <ta e="T255" id="Seg_7601" s="T254">PTCL</ta>
            <ta e="T256" id="Seg_7602" s="T255">throw.away-FUT-3SG</ta>
            <ta e="T257" id="Seg_7603" s="T256">then</ta>
            <ta e="T258" id="Seg_7604" s="T257">good</ta>
            <ta e="T259" id="Seg_7605" s="T258">become-FUT-3SG</ta>
            <ta e="T261" id="Seg_7606" s="T260">and</ta>
            <ta e="T262" id="Seg_7607" s="T261">this.[NOM.SG]</ta>
            <ta e="T263" id="Seg_7608" s="T262">what.kind</ta>
            <ta e="T264" id="Seg_7609" s="T263">small.[NOM.SG]</ta>
            <ta e="T265" id="Seg_7610" s="T264">be-PST.[3SG]</ta>
            <ta e="T266" id="Seg_7611" s="T265">and</ta>
            <ta e="T267" id="Seg_7612" s="T266">big.[NOM.SG]</ta>
            <ta e="T269" id="Seg_7613" s="T268">such.[NOM.SG]</ta>
            <ta e="T270" id="Seg_7614" s="T269">PTCL</ta>
            <ta e="T271" id="Seg_7615" s="T270">what.[NOM.SG]=INDEF</ta>
            <ta e="T272" id="Seg_7616" s="T271">NEG.EX.[3SG]</ta>
            <ta e="T273" id="Seg_7617" s="T272">good</ta>
            <ta e="T275" id="Seg_7618" s="T274">this.[NOM.SG]</ta>
            <ta e="T276" id="Seg_7619" s="T275">army-LAT</ta>
            <ta e="T277" id="Seg_7620" s="T276">go-PST.[3SG]</ta>
            <ta e="T278" id="Seg_7621" s="T277">anyway</ta>
            <ta e="T279" id="Seg_7622" s="T278">come-PST.[3SG]</ta>
            <ta e="T280" id="Seg_7623" s="T279">where.to</ta>
            <ta e="T281" id="Seg_7624" s="T280">work-INF.LAT</ta>
            <ta e="T282" id="Seg_7625" s="T281">go-FUT-3SG</ta>
            <ta e="T283" id="Seg_7626" s="T282">this-ACC</ta>
            <ta e="T284" id="Seg_7627" s="T283">PTCL</ta>
            <ta e="T285" id="Seg_7628" s="T284">drive-DUR-3PL</ta>
            <ta e="T287" id="Seg_7629" s="T286">one.[NOM.SG]</ta>
            <ta e="T288" id="Seg_7630" s="T287">woman.[NOM.SG]</ta>
            <ta e="T289" id="Seg_7631" s="T288">son-NOM/GEN.3SG</ta>
            <ta e="T290" id="Seg_7632" s="T289">be-PST.[3SG]</ta>
            <ta e="T291" id="Seg_7633" s="T290">PTCL</ta>
            <ta e="T292" id="Seg_7634" s="T291">drunk.[NOM.SG]</ta>
            <ta e="T293" id="Seg_7635" s="T292">you.NOM</ta>
            <ta e="T294" id="Seg_7636" s="T293">what.[NOM.SG]</ta>
            <ta e="T295" id="Seg_7637" s="T294">I.LAT</ta>
            <ta e="T296" id="Seg_7638" s="T295">NEG</ta>
            <ta e="T297" id="Seg_7639" s="T296">learn-2SG-PST-2SG</ta>
            <ta e="T298" id="Seg_7640" s="T297">and</ta>
            <ta e="T299" id="Seg_7641" s="T298">this.[NOM.SG]</ta>
            <ta e="T300" id="Seg_7642" s="T299">say-PST.[3SG]</ta>
            <ta e="T301" id="Seg_7643" s="T300">I.NOM</ta>
            <ta e="T302" id="Seg_7644" s="T301">learn-TR-PST-1SG</ta>
            <ta e="T303" id="Seg_7645" s="T302">you.ACC</ta>
            <ta e="T304" id="Seg_7646" s="T303">what.[NOM.SG]</ta>
            <ta e="T305" id="Seg_7647" s="T304">one.needs</ta>
            <ta e="T306" id="Seg_7648" s="T305">you.NOM</ta>
            <ta e="T307" id="Seg_7649" s="T306">you.DAT</ta>
            <ta e="T308" id="Seg_7650" s="T307">you.NOM</ta>
            <ta e="T310" id="Seg_7651" s="T309">take-PST-2SG</ta>
            <ta e="T311" id="Seg_7652" s="T310">and</ta>
            <ta e="T312" id="Seg_7653" s="T311">this.[NOM.SG]</ta>
            <ta e="T313" id="Seg_7654" s="T312">PTCL</ta>
            <ta e="T315" id="Seg_7655" s="T314">vodka.[NOM.SG]</ta>
            <ta e="T316" id="Seg_7656" s="T315">drink-PRS-2SG</ta>
            <ta e="T317" id="Seg_7657" s="T316">and</ta>
            <ta e="T318" id="Seg_7658" s="T317">this.[NOM.SG]</ta>
            <ta e="T319" id="Seg_7659" s="T318">PTCL</ta>
            <ta e="T321" id="Seg_7660" s="T319">fist-INS=PTCL</ta>
            <ta e="T322" id="Seg_7661" s="T321">mother-NOM/GEN.3SG</ta>
            <ta e="T323" id="Seg_7662" s="T322">want.PST.M.SG</ta>
            <ta e="T324" id="Seg_7663" s="T323">beat-INF.LAT</ta>
            <ta e="T325" id="Seg_7664" s="T324">and</ta>
            <ta e="T326" id="Seg_7665" s="T325">people.[NOM.SG]</ta>
            <ta e="T327" id="Seg_7666" s="T326">NEG</ta>
            <ta e="T328" id="Seg_7667" s="T327">give-PST-3PL</ta>
            <ta e="T330" id="Seg_7668" s="T329">I.NOM</ta>
            <ta e="T331" id="Seg_7669" s="T330">brother-NOM/GEN/ACC.1SG</ta>
            <ta e="T332" id="Seg_7670" s="T331">war-LOC</ta>
            <ta e="T333" id="Seg_7671" s="T332">be-PST-2SG</ta>
            <ta e="T334" id="Seg_7672" s="T333">be-PST.[3SG]</ta>
            <ta e="T335" id="Seg_7673" s="T334">and</ta>
            <ta e="T336" id="Seg_7674" s="T335">this-ACC</ta>
            <ta e="T337" id="Seg_7675" s="T336">take-PST-3PL</ta>
            <ta e="T338" id="Seg_7676" s="T337">captivity-LAT</ta>
            <ta e="T339" id="Seg_7677" s="T338">and</ta>
            <ta e="T340" id="Seg_7678" s="T339">right</ta>
            <ta e="T341" id="Seg_7679" s="T340">hand-NOM/GEN.3SG</ta>
            <ta e="T342" id="Seg_7680" s="T341">finger-3SG-INS</ta>
            <ta e="T344" id="Seg_7681" s="T343">stab-INF.LAT</ta>
            <ta e="T345" id="Seg_7682" s="T344">want.PST.PL</ta>
            <ta e="T346" id="Seg_7683" s="T345">and</ta>
            <ta e="T347" id="Seg_7684" s="T346">this.[NOM.SG]</ta>
            <ta e="T348" id="Seg_7685" s="T347">PTCL</ta>
            <ta e="T349" id="Seg_7686" s="T348">cry-PST.[3SG]</ta>
            <ta e="T350" id="Seg_7687" s="T349">two.[NOM.SG]</ta>
            <ta e="T351" id="Seg_7688" s="T350">finger</ta>
            <ta e="T353" id="Seg_7689" s="T352">remain-TR-PST-3PL</ta>
            <ta e="T355" id="Seg_7690" s="T354">this-LAT</ta>
            <ta e="T357" id="Seg_7691" s="T356">I.NOM</ta>
            <ta e="T358" id="Seg_7692" s="T357">%%</ta>
            <ta e="T359" id="Seg_7693" s="T358">brother-NOM/GEN/ACC.1SG</ta>
            <ta e="T360" id="Seg_7694" s="T359">three.[NOM.SG]</ta>
            <ta e="T361" id="Seg_7695" s="T360">girl.[NOM.SG]</ta>
            <ta e="T362" id="Seg_7696" s="T361">and</ta>
            <ta e="T363" id="Seg_7697" s="T362">three.[NOM.SG]</ta>
            <ta e="T364" id="Seg_7698" s="T363">boy.[NOM.SG]</ta>
            <ta e="T366" id="Seg_7699" s="T365">tell-INF.LAT</ta>
            <ta e="T367" id="Seg_7700" s="T366">you.DAT</ta>
            <ta e="T368" id="Seg_7701" s="T367">white.[NOM.SG]</ta>
            <ta e="T369" id="Seg_7702" s="T368">white.[NOM.SG]</ta>
            <ta e="T370" id="Seg_7703" s="T369">bull.[NOM.SG]</ta>
            <ta e="T372" id="Seg_7704" s="T371">you.NOM</ta>
            <ta e="T373" id="Seg_7705" s="T372">tell-IMP.2SG.O</ta>
            <ta e="T374" id="Seg_7706" s="T373">I.NOM</ta>
            <ta e="T375" id="Seg_7707" s="T374">tell-IMP.2SG.O</ta>
            <ta e="T376" id="Seg_7708" s="T375">say-INF.LAT</ta>
            <ta e="T377" id="Seg_7709" s="T376">you.DAT</ta>
            <ta e="T378" id="Seg_7710" s="T377">red.[NOM.SG]</ta>
            <ta e="T379" id="Seg_7711" s="T378">bull.[NOM.SG]</ta>
            <ta e="T381" id="Seg_7712" s="T380">you.NOM</ta>
            <ta e="T382" id="Seg_7713" s="T381">tell-IMP.2SG.O</ta>
            <ta e="T383" id="Seg_7714" s="T382">I.NOM</ta>
            <ta e="T384" id="Seg_7715" s="T383">tell-IMP.2SG.O</ta>
            <ta e="T385" id="Seg_7716" s="T384">say-INF.LAT</ta>
            <ta e="T386" id="Seg_7717" s="T385">you.DAT</ta>
            <ta e="T387" id="Seg_7718" s="T386">black.[NOM.SG]</ta>
            <ta e="T388" id="Seg_7719" s="T387">bull.[NOM.SG]</ta>
            <ta e="T389" id="Seg_7720" s="T388">Kodur.[NOM.SG]</ta>
            <ta e="T390" id="Seg_7721" s="T389">Kodur.[NOM.SG]</ta>
            <ta e="T391" id="Seg_7722" s="T390">red.[NOM.SG]</ta>
            <ta e="T392" id="Seg_7723" s="T391">fur.coat.[NOM.SG]</ta>
            <ta e="T393" id="Seg_7724" s="T392">come-PST.[3SG]</ta>
            <ta e="T394" id="Seg_7725" s="T393">here</ta>
            <ta e="T395" id="Seg_7726" s="T394">people.[NOM.SG]</ta>
            <ta e="T396" id="Seg_7727" s="T395">live-PST-3PL</ta>
            <ta e="T397" id="Seg_7728" s="T396">this.[NOM.SG]</ta>
            <ta e="T398" id="Seg_7729" s="T397">blade.bone.[NOM.SG]</ta>
            <ta e="T399" id="Seg_7730" s="T398">find-PST.[3SG]</ta>
            <ta e="T400" id="Seg_7731" s="T399">meat.[NOM.SG]</ta>
            <ta e="T401" id="Seg_7732" s="T400">NEG.EX.[3SG]</ta>
            <ta e="T402" id="Seg_7733" s="T401">then</ta>
            <ta e="T403" id="Seg_7734" s="T402">go-PST.[3SG]</ta>
            <ta e="T404" id="Seg_7735" s="T403">go-PST.[3SG]</ta>
            <ta e="T405" id="Seg_7736" s="T404">well</ta>
            <ta e="T406" id="Seg_7737" s="T405">there</ta>
            <ta e="T407" id="Seg_7738" s="T406">one.[NOM.SG]</ta>
            <ta e="T408" id="Seg_7739" s="T407">man.[NOM.SG]</ta>
            <ta e="T409" id="Seg_7740" s="T408">live-PST.[3SG]</ta>
            <ta e="T410" id="Seg_7741" s="T409">woman-COM</ta>
            <ta e="T411" id="Seg_7742" s="T410">this.[NOM.SG]</ta>
            <ta e="T412" id="Seg_7743" s="T411">come-PST.[3SG]</ta>
            <ta e="T413" id="Seg_7744" s="T412">this-PL</ta>
            <ta e="T414" id="Seg_7745" s="T413">this-PL-LAT</ta>
            <ta e="T415" id="Seg_7746" s="T414">sheep.[NOM.SG]</ta>
            <ta e="T416" id="Seg_7747" s="T415">be-PST.[3SG]</ta>
            <ta e="T417" id="Seg_7748" s="T416">one.[NOM.SG]</ta>
            <ta e="T418" id="Seg_7749" s="T417">this.[NOM.SG]</ta>
            <ta e="T419" id="Seg_7750" s="T418">say-IPFVZ.[3SG]</ta>
            <ta e="T420" id="Seg_7751" s="T419">one.should</ta>
            <ta e="T421" id="Seg_7752" s="T420">yellow.[NOM.SG]</ta>
            <ta e="T422" id="Seg_7753" s="T421">water.[NOM.SG]</ta>
            <ta e="T423" id="Seg_7754" s="T422">boil-INF.LAT</ta>
            <ta e="T424" id="Seg_7755" s="T423">man-ACC</ta>
            <ta e="T426" id="Seg_7756" s="T425">feed-INF.LAT</ta>
            <ta e="T427" id="Seg_7757" s="T426">and</ta>
            <ta e="T428" id="Seg_7758" s="T427">this.[NOM.SG]</ta>
            <ta e="T429" id="Seg_7759" s="T428">say-IPFVZ.[3SG]</ta>
            <ta e="T430" id="Seg_7760" s="T429">NEG.AUX-IMP.2SG</ta>
            <ta e="T431" id="Seg_7761" s="T430">boil-EP-CNG</ta>
            <ta e="T432" id="Seg_7762" s="T431">I.NOM</ta>
            <ta e="T433" id="Seg_7763" s="T432">meat-NOM/GEN/ACC.1SG</ta>
            <ta e="T434" id="Seg_7764" s="T433">be-PRS.[3SG]</ta>
            <ta e="T435" id="Seg_7765" s="T434">then</ta>
            <ta e="T436" id="Seg_7766" s="T435">cauldron.[NOM.SG]</ta>
            <ta e="T437" id="Seg_7767" s="T436">hang.up-PST-3PL</ta>
            <ta e="T438" id="Seg_7768" s="T437">salt.[NOM.SG]</ta>
            <ta e="T439" id="Seg_7769" s="T438">put-PST-3PL</ta>
            <ta e="T440" id="Seg_7770" s="T439">this.[NOM.SG]</ta>
            <ta e="T442" id="Seg_7771" s="T441">go-PST.[3SG]</ta>
            <ta e="T443" id="Seg_7772" s="T442">nose</ta>
            <ta e="T444" id="Seg_7773" s="T443">nose-NOM/GEN.3SG</ta>
            <ta e="T445" id="Seg_7774" s="T444">hit-MULT-PST.[3SG]</ta>
            <ta e="T446" id="Seg_7775" s="T445">blood-INS</ta>
            <ta e="T447" id="Seg_7776" s="T446">blood-INS</ta>
            <ta e="T449" id="Seg_7777" s="T447">blade.bone-ACC</ta>
            <ta e="T450" id="Seg_7778" s="T449">then</ta>
            <ta e="T451" id="Seg_7779" s="T450">cauldron-NOM/GEN/ACC.3SG</ta>
            <ta e="T452" id="Seg_7780" s="T451">throw.away-MOM-PST.[3SG]</ta>
            <ta e="T453" id="Seg_7781" s="T452">boil-PST.[3SG]</ta>
            <ta e="T454" id="Seg_7782" s="T453">boil-PST.[3SG]</ta>
            <ta e="T455" id="Seg_7783" s="T454">then</ta>
            <ta e="T456" id="Seg_7784" s="T455">meat.[NOM.SG]</ta>
            <ta e="T457" id="Seg_7785" s="T456">NEG.EX.[3SG]</ta>
            <ta e="T458" id="Seg_7786" s="T457">one.[NOM.SG]</ta>
            <ta e="T459" id="Seg_7787" s="T458">blade.bone.[NOM.SG]</ta>
            <ta e="T460" id="Seg_7788" s="T459">this.[NOM.SG]</ta>
            <ta e="T461" id="Seg_7789" s="T460">PTCL</ta>
            <ta e="T463" id="Seg_7790" s="T462">fall-MOM-PST.[3SG]</ta>
            <ta e="T464" id="Seg_7791" s="T463">INCH</ta>
            <ta e="T465" id="Seg_7792" s="T464">cry-INF.LAT</ta>
            <ta e="T466" id="Seg_7793" s="T465">and</ta>
            <ta e="T467" id="Seg_7794" s="T466">this-PL</ta>
            <ta e="T470" id="Seg_7795" s="T469">say-PST-3PL</ta>
            <ta e="T471" id="Seg_7796" s="T470">soon</ta>
            <ta e="T472" id="Seg_7797" s="T471">sheep.[NOM.SG]</ta>
            <ta e="T473" id="Seg_7798" s="T472">stab-FUT-1SG</ta>
            <ta e="T474" id="Seg_7799" s="T473">you.DAT</ta>
            <ta e="T475" id="Seg_7800" s="T474">blade.bone-ACC</ta>
            <ta e="T476" id="Seg_7801" s="T475">go-PST-1SG</ta>
            <ta e="T477" id="Seg_7802" s="T476">I.LAT</ta>
            <ta e="T478" id="Seg_7803" s="T477">NEG</ta>
            <ta e="T479" id="Seg_7804" s="T478">one.needs</ta>
            <ta e="T480" id="Seg_7805" s="T479">blade.bone.[NOM.SG]</ta>
            <ta e="T481" id="Seg_7806" s="T480">then</ta>
            <ta e="T482" id="Seg_7807" s="T481">this-LAT</ta>
            <ta e="T483" id="Seg_7808" s="T482">sheep.[NOM.SG]</ta>
            <ta e="T484" id="Seg_7809" s="T483">give-PST-3PL</ta>
            <ta e="T486" id="Seg_7810" s="T485">then</ta>
            <ta e="T487" id="Seg_7811" s="T486">this.[NOM.SG]</ta>
            <ta e="T488" id="Seg_7812" s="T487">Kodur.[NOM.SG]</ta>
            <ta e="T490" id="Seg_7813" s="T489">jump-MOM-PST.[3SG]</ta>
            <ta e="T491" id="Seg_7814" s="T490">wash-DRV-PST.[3SG]</ta>
            <ta e="T492" id="Seg_7815" s="T491">rub-PST.[3SG]</ta>
            <ta e="T493" id="Seg_7816" s="T492">eye-NOM/GEN.3SG</ta>
            <ta e="T494" id="Seg_7817" s="T493">sheep-NOM/GEN/ACC.3SG</ta>
            <ta e="T495" id="Seg_7818" s="T494">take-PST.[3SG]</ta>
            <ta e="T496" id="Seg_7819" s="T495">go-PST.[3SG]</ta>
            <ta e="T497" id="Seg_7820" s="T496">then</ta>
            <ta e="T499" id="Seg_7821" s="T498">people.[NOM.SG]</ta>
            <ta e="T500" id="Seg_7822" s="T499">people-LAT</ta>
            <ta e="T501" id="Seg_7823" s="T500">come-PST.[3SG]</ta>
            <ta e="T502" id="Seg_7824" s="T501">there</ta>
            <ta e="T503" id="Seg_7825" s="T502">also</ta>
            <ta e="T504" id="Seg_7826" s="T503">two.[NOM.SG]</ta>
            <ta e="T505" id="Seg_7827" s="T504">sheep.[NOM.SG]</ta>
            <ta e="T506" id="Seg_7828" s="T505">boy.[NOM.SG]</ta>
            <ta e="T507" id="Seg_7829" s="T506">be-PST.[3SG]</ta>
            <ta e="T508" id="Seg_7830" s="T507">and</ta>
            <ta e="T509" id="Seg_7831" s="T508">boy-GEN</ta>
            <ta e="T510" id="Seg_7832" s="T509">woman.[NOM.SG]</ta>
            <ta e="T511" id="Seg_7833" s="T510">be-PST.[3SG]</ta>
            <ta e="T512" id="Seg_7834" s="T511">then</ta>
            <ta e="T513" id="Seg_7835" s="T512">say-PRS-3PL</ta>
            <ta e="T514" id="Seg_7836" s="T513">send-IMP.2SG.O</ta>
            <ta e="T515" id="Seg_7837" s="T514">you.NOM</ta>
            <ta e="T516" id="Seg_7838" s="T515">sheep.[NOM.SG]</ta>
            <ta e="T517" id="Seg_7839" s="T516">we.NOM</ta>
            <ta e="T518" id="Seg_7840" s="T517">sheep-INS</ta>
            <ta e="T519" id="Seg_7841" s="T518">no</ta>
            <ta e="T520" id="Seg_7842" s="T519">NEG</ta>
            <ta e="T521" id="Seg_7843" s="T520">let-FUT-1SG</ta>
            <ta e="T522" id="Seg_7844" s="T521">you.PL.NOM</ta>
            <ta e="T523" id="Seg_7845" s="T522">sheep.[NOM.SG]</ta>
            <ta e="T524" id="Seg_7846" s="T523">I.NOM</ta>
            <ta e="T525" id="Seg_7847" s="T524">sheep-NOM/GEN/ACC.1SG</ta>
            <ta e="T526" id="Seg_7848" s="T525">eat-MOM-3PL</ta>
            <ta e="T527" id="Seg_7849" s="T526">and</ta>
            <ta e="T528" id="Seg_7850" s="T527">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T530" id="Seg_7851" s="T529">speak-PRS.[3SG]</ta>
            <ta e="T531" id="Seg_7852" s="T530">speak-PRS.[3SG]</ta>
            <ta e="T532" id="Seg_7853" s="T531">this.[NOM.SG]</ta>
            <ta e="T534" id="Seg_7854" s="T533">run-MOM-PST.[3SG]</ta>
            <ta e="T535" id="Seg_7855" s="T534">and</ta>
            <ta e="T536" id="Seg_7856" s="T535">sheep-NOM/GEN/ACC.3SG</ta>
            <ta e="T537" id="Seg_7857" s="T536">let-PST.[3SG]</ta>
            <ta e="T538" id="Seg_7858" s="T537">this-LAT</ta>
            <ta e="T539" id="Seg_7859" s="T538">sheep-NOM/GEN/ACC.3SG</ta>
            <ta e="T540" id="Seg_7860" s="T539">and</ta>
            <ta e="T541" id="Seg_7861" s="T540">then</ta>
            <ta e="T542" id="Seg_7862" s="T541">lie.down-PST-3PL</ta>
            <ta e="T543" id="Seg_7863" s="T542">sleep-INF.LAT</ta>
            <ta e="T544" id="Seg_7864" s="T543">morning-GEN</ta>
            <ta e="T545" id="Seg_7865" s="T544">get.up-PST-3PL</ta>
            <ta e="T546" id="Seg_7866" s="T545">and</ta>
            <ta e="T547" id="Seg_7867" s="T546">sheep.[NOM.SG]</ta>
            <ta e="T548" id="Seg_7868" s="T547">NEG.EX.[3SG]</ta>
            <ta e="T550" id="Seg_7869" s="T549">this.[NOM.SG]</ta>
            <ta e="T551" id="Seg_7870" s="T550">Kodur.[NOM.SG]</ta>
            <ta e="T552" id="Seg_7871" s="T551">get.up-PST.[3SG]</ta>
            <ta e="T553" id="Seg_7872" s="T552">sheep-NOM/GEN/ACC.3SG</ta>
            <ta e="T554" id="Seg_7873" s="T553">off-pull-PST.[3SG]</ta>
            <ta e="T555" id="Seg_7874" s="T554">and</ta>
            <ta e="T556" id="Seg_7875" s="T555">this.[NOM.SG]</ta>
            <ta e="T557" id="Seg_7876" s="T556">this-GEN.PL</ta>
            <ta e="T558" id="Seg_7877" s="T557">sheep-NOM/GEN/ACC.3SG</ta>
            <ta e="T559" id="Seg_7878" s="T558">PTCL</ta>
            <ta e="T560" id="Seg_7879" s="T559">blood-INS</ta>
            <ta e="T562" id="Seg_7880" s="T561">blood-INS</ta>
            <ta e="T563" id="Seg_7881" s="T562">PTCL</ta>
            <ta e="T564" id="Seg_7882" s="T563">smear-TR-PST.[3SG]</ta>
            <ta e="T565" id="Seg_7883" s="T564">nose-NOM/GEN.3SG</ta>
            <ta e="T566" id="Seg_7884" s="T565">and</ta>
            <ta e="T567" id="Seg_7885" s="T566">mouth-NOM/GEN/ACC.3SG</ta>
            <ta e="T568" id="Seg_7886" s="T567">and</ta>
            <ta e="T569" id="Seg_7887" s="T568">morning-GEN</ta>
            <ta e="T570" id="Seg_7888" s="T569">get.up-PST-3PL</ta>
            <ta e="T571" id="Seg_7889" s="T570">say-IPFVZ-3PL</ta>
            <ta e="T572" id="Seg_7890" s="T571">really</ta>
            <ta e="T574" id="Seg_7891" s="T573">we.NOM</ta>
            <ta e="T575" id="Seg_7892" s="T574">sheep-NOM/GEN/ACC.3SG</ta>
            <ta e="T576" id="Seg_7893" s="T575">this.[NOM.SG]</ta>
            <ta e="T577" id="Seg_7894" s="T576">this-ACC</ta>
            <ta e="T578" id="Seg_7895" s="T577">sheep-NOM/GEN/ACC.3SG</ta>
            <ta e="T579" id="Seg_7896" s="T578">PTCL</ta>
            <ta e="T580" id="Seg_7897" s="T579">off-tear-PST-3PL</ta>
            <ta e="T582" id="Seg_7898" s="T581">then</ta>
            <ta e="T583" id="Seg_7899" s="T582">this.[NOM.SG]</ta>
            <ta e="T584" id="Seg_7900" s="T583">get.up-PST.[3SG]</ta>
            <ta e="T585" id="Seg_7901" s="T584">eye-ACC.3SG</ta>
            <ta e="T586" id="Seg_7902" s="T585">wash-PST.[3SG]</ta>
            <ta e="T587" id="Seg_7903" s="T586">rub-PST.[3SG]</ta>
            <ta e="T588" id="Seg_7904" s="T587">two.[NOM.SG]</ta>
            <ta e="T589" id="Seg_7905" s="T588">sheep.[NOM.SG]</ta>
            <ta e="T590" id="Seg_7906" s="T589">take-PST.[3SG]</ta>
            <ta e="T591" id="Seg_7907" s="T590">and</ta>
            <ta e="T592" id="Seg_7908" s="T591">walk-PRS.[3SG]</ta>
            <ta e="T595" id="Seg_7909" s="T594">go-MOM-PST.[3SG]</ta>
            <ta e="T596" id="Seg_7910" s="T595">then</ta>
            <ta e="T597" id="Seg_7911" s="T596">come-PRS.[3SG]</ta>
            <ta e="T598" id="Seg_7912" s="T597">come-PRS.[3SG]</ta>
            <ta e="T599" id="Seg_7913" s="T598">there</ta>
            <ta e="T600" id="Seg_7914" s="T599">PTCL</ta>
            <ta e="T601" id="Seg_7915" s="T600">cross.[NOM.SG]</ta>
            <ta e="T602" id="Seg_7916" s="T601">stand-DUR.[3SG]</ta>
            <ta e="T603" id="Seg_7917" s="T602">this.[NOM.SG]</ta>
            <ta e="T604" id="Seg_7918" s="T603">dig-PST.[3SG]</ta>
            <ta e="T605" id="Seg_7919" s="T604">there</ta>
            <ta e="T606" id="Seg_7920" s="T605">woman.[NOM.SG]</ta>
            <ta e="T607" id="Seg_7921" s="T606">lie-DUR.[3SG]</ta>
            <ta e="T608" id="Seg_7922" s="T607">this.[NOM.SG]</ta>
            <ta e="T609" id="Seg_7923" s="T608">woman.[NOM.SG]</ta>
            <ta e="T610" id="Seg_7924" s="T609">be-PST.[3SG]</ta>
            <ta e="T611" id="Seg_7925" s="T610">and</ta>
            <ta e="T612" id="Seg_7926" s="T611">two.[NOM.SG]</ta>
            <ta e="T613" id="Seg_7927" s="T612">sheep.[NOM.SG]</ta>
            <ta e="T614" id="Seg_7928" s="T613">and</ta>
            <ta e="T615" id="Seg_7929" s="T614">this.[NOM.SG]</ta>
            <ta e="T616" id="Seg_7930" s="T615">sheep-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T617" id="Seg_7931" s="T616">this.[NOM.SG]</ta>
            <ta e="T618" id="Seg_7932" s="T617">woman-LAT</ta>
            <ta e="T619" id="Seg_7933" s="T618">hand-PL-LAT</ta>
            <ta e="T620" id="Seg_7934" s="T619">bind-PST.[3SG]</ta>
            <ta e="T621" id="Seg_7935" s="T620">heart-NOM/GEN/ACC.3SG</ta>
            <ta e="T622" id="Seg_7936" s="T621">heart-LAT/LOC.3SG</ta>
            <ta e="T623" id="Seg_7937" s="T622">knife.[NOM.SG]</ta>
            <ta e="T625" id="Seg_7938" s="T624">creep.into-MOM-PST.[3SG]</ta>
            <ta e="T626" id="Seg_7939" s="T625">then</ta>
            <ta e="T627" id="Seg_7940" s="T626">come-PST.[3SG]</ta>
            <ta e="T629" id="Seg_7941" s="T628">there</ta>
            <ta e="T630" id="Seg_7942" s="T629">two.[NOM.SG]</ta>
            <ta e="T631" id="Seg_7943" s="T630">girl.[NOM.SG]</ta>
            <ta e="T632" id="Seg_7944" s="T631">and</ta>
            <ta e="T633" id="Seg_7945" s="T632">man.[NOM.SG]</ta>
            <ta e="T634" id="Seg_7946" s="T633">live-DUR.[3SG]</ta>
            <ta e="T635" id="Seg_7947" s="T634">speak-PRS.[3SG]</ta>
            <ta e="T636" id="Seg_7948" s="T635">speak-PRS.[3SG]</ta>
            <ta e="T637" id="Seg_7949" s="T636">I.NOM</ta>
            <ta e="T638" id="Seg_7950" s="T637">there</ta>
            <ta e="T639" id="Seg_7951" s="T638">woman.[NOM.SG]</ta>
            <ta e="T640" id="Seg_7952" s="T639">sit-DUR.[3SG]</ta>
            <ta e="T641" id="Seg_7953" s="T640">sheep-INS</ta>
            <ta e="T642" id="Seg_7954" s="T641">and</ta>
            <ta e="T643" id="Seg_7955" s="T642">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T645" id="Seg_7956" s="T644">heart-LAT/LOC.3SG</ta>
            <ta e="T646" id="Seg_7957" s="T645">knife.[NOM.SG]</ta>
            <ta e="T647" id="Seg_7958" s="T646">sit-DUR.[3SG]</ta>
            <ta e="T648" id="Seg_7959" s="T647">and</ta>
            <ta e="T649" id="Seg_7960" s="T648">daughter-PL</ta>
            <ta e="T650" id="Seg_7961" s="T649">we.NOM</ta>
            <ta e="T651" id="Seg_7962" s="T650">go-FUT-1DU</ta>
            <ta e="T652" id="Seg_7963" s="T651">bring-FUT-1DU</ta>
            <ta e="T653" id="Seg_7964" s="T652">woman-NOM/GEN/ACC.2SG</ta>
            <ta e="T654" id="Seg_7965" s="T653">you.NOM</ta>
            <ta e="T655" id="Seg_7966" s="T654">this.[3SG]</ta>
            <ta e="T656" id="Seg_7967" s="T655">very</ta>
            <ta e="T657" id="Seg_7968" s="T656">fear-PRS.[3SG]</ta>
            <ta e="T658" id="Seg_7969" s="T657">otherwise</ta>
            <ta e="T659" id="Seg_7970" s="T658">stab-FUT-3SG</ta>
            <ta e="T660" id="Seg_7971" s="T659">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T661" id="Seg_7972" s="T660">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T663" id="Seg_7973" s="T662">then</ta>
            <ta e="T664" id="Seg_7974" s="T663">this-PL</ta>
            <ta e="T665" id="Seg_7975" s="T664">daughter-PL</ta>
            <ta e="T666" id="Seg_7976" s="T665">run-MOM-PST-3PL</ta>
            <ta e="T667" id="Seg_7977" s="T666">come-PST-3PL</ta>
            <ta e="T668" id="Seg_7978" s="T667">stab-MOM-PST.[3SG]</ta>
            <ta e="T669" id="Seg_7979" s="T668">sheep-NOM/GEN/ACC.3SG</ta>
            <ta e="T670" id="Seg_7980" s="T669">PTCL</ta>
            <ta e="T672" id="Seg_7981" s="T671">run-MOM-PST-3PL</ta>
            <ta e="T673" id="Seg_7982" s="T672">woman-NOM/GEN/ACC.1SG</ta>
            <ta e="T674" id="Seg_7983" s="T673">then</ta>
            <ta e="T675" id="Seg_7984" s="T674">this.[NOM.SG]</ta>
            <ta e="T676" id="Seg_7985" s="T675">PTCL</ta>
            <ta e="T677" id="Seg_7986" s="T676">fall-MOM-PST.[3SG]</ta>
            <ta e="T678" id="Seg_7987" s="T677">Kodur.[NOM.SG]</ta>
            <ta e="T679" id="Seg_7988" s="T678">cry-PST.[3SG]</ta>
            <ta e="T680" id="Seg_7989" s="T679">cry-PST.[3SG]</ta>
            <ta e="T681" id="Seg_7990" s="T680">this.[NOM.SG]</ta>
            <ta e="T682" id="Seg_7991" s="T681">man.[NOM.SG]</ta>
            <ta e="T683" id="Seg_7992" s="T682">say-IPFVZ.[3SG]</ta>
            <ta e="T684" id="Seg_7993" s="T683">take-IMP.2SG.O</ta>
            <ta e="T685" id="Seg_7994" s="T684">one.[NOM.SG]</ta>
            <ta e="T686" id="Seg_7995" s="T685">girl.[NOM.SG]</ta>
            <ta e="T687" id="Seg_7996" s="T686">what.for</ta>
            <ta e="T688" id="Seg_7997" s="T687">I.LAT</ta>
            <ta e="T689" id="Seg_7998" s="T688">one.[NOM.SG]</ta>
            <ta e="T690" id="Seg_7999" s="T689">I.NOM</ta>
            <ta e="T691" id="Seg_8000" s="T690">woman-NOM/GEN/ACC.1SG</ta>
            <ta e="T692" id="Seg_8001" s="T691">better</ta>
            <ta e="T693" id="Seg_8002" s="T692">be-PST.[3SG]</ta>
            <ta e="T694" id="Seg_8003" s="T693">well</ta>
            <ta e="T695" id="Seg_8004" s="T694">take-IMP.2SG.O</ta>
            <ta e="T696" id="Seg_8005" s="T695">two.[NOM.SG]</ta>
            <ta e="T697" id="Seg_8006" s="T696">then</ta>
            <ta e="T698" id="Seg_8007" s="T697">this.[NOM.SG]</ta>
            <ta e="T699" id="Seg_8008" s="T698">get.up-PST.[3SG]</ta>
            <ta e="T700" id="Seg_8009" s="T699">again</ta>
            <ta e="T701" id="Seg_8010" s="T700">wash-DRV-PST.[3SG]</ta>
            <ta e="T702" id="Seg_8011" s="T701">face-NOM/GEN/ACC.3SG</ta>
            <ta e="T703" id="Seg_8012" s="T702">and</ta>
            <ta e="T704" id="Seg_8013" s="T703">two.[NOM.SG]</ta>
            <ta e="T705" id="Seg_8014" s="T704">girl.[NOM.SG]</ta>
            <ta e="T706" id="Seg_8015" s="T705">take-PST.[3SG]</ta>
            <ta e="T707" id="Seg_8016" s="T706">and</ta>
            <ta e="T708" id="Seg_8017" s="T707">go-PST.[3SG]</ta>
            <ta e="T709" id="Seg_8018" s="T708">then</ta>
            <ta e="T710" id="Seg_8019" s="T709">come-PRS.[3SG]</ta>
            <ta e="T711" id="Seg_8020" s="T710">come-PRS.[3SG]</ta>
            <ta e="T712" id="Seg_8021" s="T711">there</ta>
            <ta e="T713" id="Seg_8022" s="T712">sheep-PL</ta>
            <ta e="T714" id="Seg_8023" s="T713">go-DUR-3PL</ta>
            <ta e="T715" id="Seg_8024" s="T714">then</ta>
            <ta e="T716" id="Seg_8025" s="T715">come-PST.[3SG]</ta>
            <ta e="T717" id="Seg_8026" s="T716">herdsman-LAT</ta>
            <ta e="T718" id="Seg_8027" s="T717">here</ta>
            <ta e="T719" id="Seg_8028" s="T718">I.NOM</ta>
            <ta e="T720" id="Seg_8029" s="T719">sheep.[NOM.SG]</ta>
            <ta e="T721" id="Seg_8030" s="T720">and</ta>
            <ta e="T723" id="Seg_8031" s="T722">one.[NOM.SG]</ta>
            <ta e="T725" id="Seg_8032" s="T724">sit.down-PST.[3SG]</ta>
            <ta e="T726" id="Seg_8033" s="T725">water-LAT</ta>
            <ta e="T728" id="Seg_8034" s="T727">shore-LAT/LOC.3SG</ta>
            <ta e="T729" id="Seg_8035" s="T728">one.[NOM.SG]</ta>
            <ta e="T730" id="Seg_8036" s="T729">sit.down-%%-PST.[3SG]</ta>
            <ta e="T731" id="Seg_8037" s="T730">forest-LOC</ta>
            <ta e="T732" id="Seg_8038" s="T731">then</ta>
            <ta e="T733" id="Seg_8039" s="T732">say-DUR.[3SG]</ta>
            <ta e="T734" id="Seg_8040" s="T733">I.NOM</ta>
            <ta e="T735" id="Seg_8041" s="T734">sheep.[NOM.SG]</ta>
            <ta e="T736" id="Seg_8042" s="T735">well</ta>
            <ta e="T737" id="Seg_8043" s="T736">ask-IMP.2SG.O</ta>
            <ta e="T740" id="Seg_8044" s="T739">ask-IMP.2SG.O</ta>
            <ta e="T741" id="Seg_8045" s="T740">this.[NOM.SG]</ta>
            <ta e="T742" id="Seg_8046" s="T741">ask-MOM-PST.[3SG]</ta>
            <ta e="T743" id="Seg_8047" s="T742">Kodur-EP-GEN</ta>
            <ta e="T744" id="Seg_8048" s="T743">sheep-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T745" id="Seg_8049" s="T744">then</ta>
            <ta e="T746" id="Seg_8050" s="T745">forest-LOC</ta>
            <ta e="T747" id="Seg_8051" s="T746">man-LAT</ta>
            <ta e="T748" id="Seg_8052" s="T747">ask-IMP.2SG.O</ta>
            <ta e="T749" id="Seg_8053" s="T748">ask-PST.[3SG]</ta>
            <ta e="T750" id="Seg_8054" s="T749">this.[NOM.SG]</ta>
            <ta e="T751" id="Seg_8055" s="T750">say-IPFVZ.[3SG]</ta>
            <ta e="T752" id="Seg_8056" s="T751">Kodur-EP-GEN</ta>
            <ta e="T753" id="Seg_8057" s="T752">sheep-NOM/GEN/ACC.3SG</ta>
            <ta e="T754" id="Seg_8058" s="T753">then</ta>
            <ta e="T756" id="Seg_8059" s="T755">take-PST.[3SG]</ta>
            <ta e="T757" id="Seg_8060" s="T756">sheep-NOM/GEN/ACC.3SG</ta>
            <ta e="T759" id="Seg_8061" s="T758">again</ta>
            <ta e="T761" id="Seg_8062" s="T760">come-PST.[3SG]</ta>
            <ta e="T762" id="Seg_8063" s="T761">man.[NOM.SG]</ta>
            <ta e="T763" id="Seg_8064" s="T762">horse-PL</ta>
            <ta e="T765" id="Seg_8065" s="T764">live-DUR.[3SG]</ta>
            <ta e="T766" id="Seg_8066" s="T765">I.NOM</ta>
            <ta e="T767" id="Seg_8067" s="T766">horse-PL</ta>
            <ta e="T768" id="Seg_8068" s="T767">and</ta>
            <ta e="T769" id="Seg_8069" s="T768">this.[NOM.SG]</ta>
            <ta e="T770" id="Seg_8070" s="T769">say-IPFVZ.[3SG]</ta>
            <ta e="T771" id="Seg_8071" s="T770">no</ta>
            <ta e="T772" id="Seg_8072" s="T771">I.NOM</ta>
            <ta e="T773" id="Seg_8073" s="T772">horse-PL</ta>
            <ta e="T774" id="Seg_8074" s="T773">well</ta>
            <ta e="T775" id="Seg_8075" s="T774">ask-IMP.2SG.O</ta>
            <ta e="T776" id="Seg_8076" s="T775">water-LAT</ta>
            <ta e="T777" id="Seg_8077" s="T776">man-ACC</ta>
            <ta e="T778" id="Seg_8078" s="T777">this.[NOM.SG]</ta>
            <ta e="T779" id="Seg_8079" s="T778">ask-PST.[3SG]</ta>
            <ta e="T780" id="Seg_8080" s="T779">Kodur-EP-GEN</ta>
            <ta e="T781" id="Seg_8081" s="T780">horse-PL</ta>
            <ta e="T782" id="Seg_8082" s="T781">then</ta>
            <ta e="T783" id="Seg_8083" s="T782">forest-GEN</ta>
            <ta e="T784" id="Seg_8084" s="T783">man-LAT</ta>
            <ta e="T785" id="Seg_8085" s="T784">ask-IMP.2SG.O</ta>
            <ta e="T786" id="Seg_8086" s="T785">this.[NOM.SG]</ta>
            <ta e="T787" id="Seg_8087" s="T786">ask-PST.[3SG]</ta>
            <ta e="T788" id="Seg_8088" s="T787">Kodur-EP-GEN</ta>
            <ta e="T789" id="Seg_8089" s="T788">horse-PL</ta>
            <ta e="T790" id="Seg_8090" s="T789">this.[NOM.SG]</ta>
            <ta e="T791" id="Seg_8091" s="T790">PTCL</ta>
            <ta e="T792" id="Seg_8092" s="T791">send-PST.[3SG]</ta>
            <ta e="T793" id="Seg_8093" s="T792">then</ta>
            <ta e="T794" id="Seg_8094" s="T793">cow-PL</ta>
            <ta e="T795" id="Seg_8095" s="T794">go-DUR-3PL</ta>
            <ta e="T796" id="Seg_8096" s="T795">and</ta>
            <ta e="T797" id="Seg_8097" s="T796">there</ta>
            <ta e="T798" id="Seg_8098" s="T797">also</ta>
            <ta e="T799" id="Seg_8099" s="T798">man.[NOM.SG]</ta>
            <ta e="T800" id="Seg_8100" s="T799">look-DUR.[3SG]</ta>
            <ta e="T801" id="Seg_8101" s="T800">cow-PL</ta>
            <ta e="T802" id="Seg_8102" s="T801">this.[NOM.SG]</ta>
            <ta e="T803" id="Seg_8103" s="T802">come-PST.[3SG]</ta>
            <ta e="T804" id="Seg_8104" s="T803">I.NOM</ta>
            <ta e="T805" id="Seg_8105" s="T804">cow-PL</ta>
            <ta e="T806" id="Seg_8106" s="T805">no</ta>
            <ta e="T807" id="Seg_8107" s="T806">NEG</ta>
            <ta e="T808" id="Seg_8108" s="T807">you.NOM</ta>
            <ta e="T809" id="Seg_8109" s="T808">well</ta>
            <ta e="T810" id="Seg_8110" s="T809">ask-IMP.2SG</ta>
            <ta e="T811" id="Seg_8111" s="T810">water-LOC</ta>
            <ta e="T812" id="Seg_8112" s="T811">man.[NOM.SG]</ta>
            <ta e="T813" id="Seg_8113" s="T812">then</ta>
            <ta e="T814" id="Seg_8114" s="T813">forest-LOC</ta>
            <ta e="T815" id="Seg_8115" s="T814">this.[NOM.SG]</ta>
            <ta e="T816" id="Seg_8116" s="T815">ask-PST.[3SG]</ta>
            <ta e="T817" id="Seg_8117" s="T816">who-GEN</ta>
            <ta e="T818" id="Seg_8118" s="T817">cow-NOM/GEN/ACC.3SG</ta>
            <ta e="T819" id="Seg_8119" s="T818">this</ta>
            <ta e="T820" id="Seg_8120" s="T819">this</ta>
            <ta e="T821" id="Seg_8121" s="T820">say-PST.[3SG]</ta>
            <ta e="T822" id="Seg_8122" s="T821">Kodur-GEN</ta>
            <ta e="T823" id="Seg_8123" s="T822">cow-NOM/GEN/ACC.3SG</ta>
            <ta e="T824" id="Seg_8124" s="T823">and</ta>
            <ta e="T825" id="Seg_8125" s="T824">forest-LOC</ta>
            <ta e="T826" id="Seg_8126" s="T825">ask-PST.[3SG]</ta>
            <ta e="T827" id="Seg_8127" s="T826">who-GEN</ta>
            <ta e="T828" id="Seg_8128" s="T827">cow-NOM/GEN/ACC.3SG</ta>
            <ta e="T829" id="Seg_8129" s="T828">Kodur-EP-GEN</ta>
            <ta e="T830" id="Seg_8130" s="T829">cow-NOM/GEN/ACC.3SG</ta>
            <ta e="T831" id="Seg_8131" s="T830">then</ta>
            <ta e="T832" id="Seg_8132" s="T831">this.[NOM.SG]</ta>
            <ta e="T833" id="Seg_8133" s="T832">INCH</ta>
            <ta e="T834" id="Seg_8134" s="T833">sing-INF.LAT</ta>
            <ta e="T835" id="Seg_8135" s="T834">song.[NOM.SG]</ta>
            <ta e="T836" id="Seg_8136" s="T835">I.NOM</ta>
            <ta e="T837" id="Seg_8137" s="T836">blade.bone.[NOM.SG]</ta>
            <ta e="T840" id="Seg_8138" s="T839">take-PST-1SG</ta>
            <ta e="T842" id="Seg_8139" s="T841">sheep.[NOM.SG]</ta>
            <ta e="T843" id="Seg_8140" s="T842">take-PST-1SG</ta>
            <ta e="T844" id="Seg_8141" s="T843">sheep-NOM/GEN/ACC.3SG</ta>
            <ta e="T845" id="Seg_8142" s="T844">two.[NOM.SG]</ta>
            <ta e="T846" id="Seg_8143" s="T845">sheep.[NOM.SG]</ta>
            <ta e="T847" id="Seg_8144" s="T846">take-PST-1SG</ta>
            <ta e="T848" id="Seg_8145" s="T847">two.[NOM.SG]</ta>
            <ta e="T849" id="Seg_8146" s="T848">sheep.[NOM.SG]</ta>
            <ta e="T850" id="Seg_8147" s="T849">take-PST-1SG</ta>
            <ta e="T851" id="Seg_8148" s="T850">go-PST-1SG</ta>
            <ta e="T852" id="Seg_8149" s="T851">woman.[NOM.SG]</ta>
            <ta e="T853" id="Seg_8150" s="T852">earth-LOC</ta>
            <ta e="T854" id="Seg_8151" s="T853">take-PST-1SG</ta>
            <ta e="T855" id="Seg_8152" s="T854">and</ta>
            <ta e="T856" id="Seg_8153" s="T855">two.[NOM.SG]</ta>
            <ta e="T857" id="Seg_8154" s="T856">sheep.[NOM.SG]</ta>
            <ta e="T858" id="Seg_8155" s="T857">and</ta>
            <ta e="T859" id="Seg_8156" s="T858">woman.[NOM.SG]</ta>
            <ta e="T860" id="Seg_8157" s="T859">knife.[NOM.SG]</ta>
            <ta e="T863" id="Seg_8158" s="T862">heart-NOM/GEN/ACC.3SG</ta>
            <ta e="T865" id="Seg_8159" s="T864">knife.[NOM.SG]</ta>
            <ta e="T866" id="Seg_8160" s="T865">heart-NOM/GEN/ACC.3SG</ta>
            <ta e="T868" id="Seg_8161" s="T867">punch-MOM-PST.[3SG]</ta>
            <ta e="T869" id="Seg_8162" s="T868">and</ta>
            <ta e="T870" id="Seg_8163" s="T869">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T871" id="Seg_8164" s="T870">come-PST.[3SG]</ta>
            <ta e="T872" id="Seg_8165" s="T871">I.NOM</ta>
            <ta e="T873" id="Seg_8166" s="T872">there</ta>
            <ta e="T874" id="Seg_8167" s="T873">woman-NOM/GEN/ACC.1SG</ta>
            <ta e="T875" id="Seg_8168" s="T874">be-PRS.[3SG]</ta>
            <ta e="T876" id="Seg_8169" s="T875">and</ta>
            <ta e="T877" id="Seg_8170" s="T876">two.[NOM.SG]</ta>
            <ta e="T878" id="Seg_8171" s="T877">sheep.[NOM.SG]</ta>
            <ta e="T879" id="Seg_8172" s="T878">and</ta>
            <ta e="T880" id="Seg_8173" s="T879">two.[NOM.SG]</ta>
            <ta e="T881" id="Seg_8174" s="T880">daughter-PL</ta>
            <ta e="T882" id="Seg_8175" s="T881">and</ta>
            <ta e="T883" id="Seg_8176" s="T882">man.[NOM.SG]</ta>
            <ta e="T884" id="Seg_8177" s="T883">live-DUR.[3SG]</ta>
            <ta e="T885" id="Seg_8178" s="T884">this.[NOM.SG]</ta>
            <ta e="T886" id="Seg_8179" s="T885">PTCL</ta>
            <ta e="T887" id="Seg_8180" s="T886">man.[NOM.SG]</ta>
            <ta e="T888" id="Seg_8181" s="T887">speak-PRS.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T3" id="Seg_8182" s="T2">делить-INF.LAT</ta>
            <ta e="T4" id="Seg_8183" s="T3">я.NOM</ta>
            <ta e="T5" id="Seg_8184" s="T4">этот-LAT</ta>
            <ta e="T6" id="Seg_8185" s="T5">класть-PST-1SG</ta>
            <ta e="T7" id="Seg_8186" s="T6">гриб-NOM/GEN/ACC.3PL</ta>
            <ta e="T8" id="Seg_8187" s="T7">масло.[NOM.SG]</ta>
            <ta e="T9" id="Seg_8188" s="T8">дать-PST-1SG</ta>
            <ta e="T10" id="Seg_8189" s="T9">деньги.[NOM.SG]</ta>
            <ta e="T11" id="Seg_8190" s="T10">дать-PST-1SG</ta>
            <ta e="T12" id="Seg_8191" s="T11">один.[NOM.SG]</ta>
            <ta e="T13" id="Seg_8192" s="T12">два.[NOM.SG]</ta>
            <ta e="T14" id="Seg_8193" s="T13">три.[NOM.SG]</ta>
            <ta e="T15" id="Seg_8194" s="T14">четыре.[NOM.SG]</ta>
            <ta e="T16" id="Seg_8195" s="T15">этот-PL</ta>
            <ta e="T17" id="Seg_8196" s="T16">дом.[NOM.SG]</ta>
            <ta e="T18" id="Seg_8197" s="T17">взять-PST-3PL</ta>
            <ta e="T19" id="Seg_8198" s="T18">кто.[NOM.SG]=INDEF</ta>
            <ta e="T20" id="Seg_8199" s="T19">PTCL</ta>
            <ta e="T21" id="Seg_8200" s="T20">продавать-DUR.[3SG]</ta>
            <ta e="T22" id="Seg_8201" s="T21">и</ta>
            <ta e="T23" id="Seg_8202" s="T22">деньги.[NOM.SG]</ta>
            <ta e="T24" id="Seg_8203" s="T23">NEG</ta>
            <ta e="T25" id="Seg_8204" s="T24">хватать-PRS.[3SG]</ta>
            <ta e="T27" id="Seg_8205" s="T26">этот-PL</ta>
            <ta e="T28" id="Seg_8206" s="T27">этот.[NOM.SG]</ta>
            <ta e="T30" id="Seg_8207" s="T29">дом.[NOM.SG]</ta>
            <ta e="T31" id="Seg_8208" s="T30">продавать-CVB</ta>
            <ta e="T32" id="Seg_8209" s="T31">взять-PST-3PL</ta>
            <ta e="T34" id="Seg_8210" s="T33">а</ta>
            <ta e="T35" id="Seg_8211" s="T34">какой</ta>
            <ta e="T36" id="Seg_8212" s="T35">мужчина-LOC</ta>
            <ta e="T37" id="Seg_8213" s="T36">NEG.EX.[3SG]</ta>
            <ta e="T39" id="Seg_8214" s="T38">%в.долг</ta>
            <ta e="T40" id="Seg_8215" s="T39">деньги.[NOM.SG]</ta>
            <ta e="T41" id="Seg_8216" s="T40">дать-INF.LAT</ta>
            <ta e="T42" id="Seg_8217" s="T41">этот-GEN</ta>
            <ta e="T43" id="Seg_8218" s="T42">корова-NOM/GEN/ACC.3SG</ta>
            <ta e="T44" id="Seg_8219" s="T43">люди.[NOM.SG]</ta>
            <ta e="T45" id="Seg_8220" s="T44">взять-PST-3PL</ta>
            <ta e="T46" id="Seg_8221" s="T45">что.[NOM.SG]</ta>
            <ta e="T47" id="Seg_8222" s="T46">быть-PRS.[3SG]</ta>
            <ta e="T48" id="Seg_8223" s="T47">PTCL</ta>
            <ta e="T49" id="Seg_8224" s="T48">взять-MOM-PST-3PL</ta>
            <ta e="T50" id="Seg_8225" s="T49">и</ta>
            <ta e="T51" id="Seg_8226" s="T50">дать-PST-3PL</ta>
            <ta e="T52" id="Seg_8227" s="T51">деньги.[NOM.SG]</ta>
            <ta e="T53" id="Seg_8228" s="T52">там</ta>
            <ta e="T55" id="Seg_8229" s="T54">я.NOM</ta>
            <ta e="T56" id="Seg_8230" s="T55">родственник-NOM/GEN/ACC.1SG</ta>
            <ta e="T892" id="Seg_8231" s="T56">пойти-CVB</ta>
            <ta e="T57" id="Seg_8232" s="T892">исчезнуть-PST.[3SG]</ta>
            <ta e="T58" id="Seg_8233" s="T57">город-LAT</ta>
            <ta e="T59" id="Seg_8234" s="T58">и</ta>
            <ta e="T60" id="Seg_8235" s="T59">там</ta>
            <ta e="T61" id="Seg_8236" s="T60">машина.[NOM.SG]</ta>
            <ta e="T62" id="Seg_8237" s="T61">взять-FUT-3SG</ta>
            <ta e="T63" id="Seg_8238" s="T62">одежда.[NOM.SG]</ta>
            <ta e="T64" id="Seg_8239" s="T63">мыть-INF.LAT</ta>
            <ta e="T65" id="Seg_8240" s="T64">рубашка.[NOM.SG]</ta>
            <ta e="T66" id="Seg_8241" s="T65">рубашка-PL</ta>
            <ta e="T67" id="Seg_8242" s="T66">штаны-PL</ta>
            <ta e="T68" id="Seg_8243" s="T67">весь</ta>
            <ta e="T70" id="Seg_8244" s="T69">что.[NOM.SG]</ta>
            <ta e="T71" id="Seg_8245" s="T70">быть-PRS.[3SG]</ta>
            <ta e="T72" id="Seg_8246" s="T71">мыть-INF.LAT</ta>
            <ta e="T74" id="Seg_8247" s="T73">мужчина.[NOM.SG]</ta>
            <ta e="T75" id="Seg_8248" s="T74">и</ta>
            <ta e="T76" id="Seg_8249" s="T75">женщина.[NOM.SG]</ta>
            <ta e="T77" id="Seg_8250" s="T76">жить-PST-3PL</ta>
            <ta e="T78" id="Seg_8251" s="T77">очень</ta>
            <ta e="T79" id="Seg_8252" s="T78">хороший</ta>
            <ta e="T80" id="Seg_8253" s="T79">жалеть-PST-3PL</ta>
            <ta e="T83" id="Seg_8254" s="T81">один.[NOM.SG]</ta>
            <ta e="T84" id="Seg_8255" s="T83">один.[NOM.SG]</ta>
            <ta e="T85" id="Seg_8256" s="T84">один-LAT</ta>
            <ta e="T86" id="Seg_8257" s="T85">жалеть-PST-3PL</ta>
            <ta e="T87" id="Seg_8258" s="T86">PTCL</ta>
            <ta e="T88" id="Seg_8259" s="T87">%%-%%-3PL</ta>
            <ta e="T89" id="Seg_8260" s="T88">целовать-DUR-3PL</ta>
            <ta e="T90" id="Seg_8261" s="T89">и</ta>
            <ta e="T91" id="Seg_8262" s="T90">этот-PL</ta>
            <ta e="T92" id="Seg_8263" s="T91">ребенок-PL-LAT</ta>
            <ta e="T93" id="Seg_8264" s="T92">быть-PST-3PL</ta>
            <ta e="T94" id="Seg_8265" s="T93">очень</ta>
            <ta e="T95" id="Seg_8266" s="T94">ребенок-PL-LAT</ta>
            <ta e="T96" id="Seg_8267" s="T95">хороший</ta>
            <ta e="T97" id="Seg_8268" s="T96">что.[NOM.SG]</ta>
            <ta e="T98" id="Seg_8269" s="T97">сказать-FUT-2SG</ta>
            <ta e="T99" id="Seg_8270" s="T98">этот-PL</ta>
            <ta e="T100" id="Seg_8271" s="T99">слушать-DUR-3PL</ta>
            <ta e="T102" id="Seg_8272" s="T101">а</ta>
            <ta e="T103" id="Seg_8273" s="T102">мы.NOM</ta>
            <ta e="T104" id="Seg_8274" s="T103">ребенок-PL</ta>
            <ta e="T105" id="Seg_8275" s="T104">очень</ta>
            <ta e="T106" id="Seg_8276" s="T105">NEG</ta>
            <ta e="T107" id="Seg_8277" s="T106">хороший</ta>
            <ta e="T108" id="Seg_8278" s="T107">бороться-DUR-3PL</ta>
            <ta e="T109" id="Seg_8279" s="T108">ругать-DES-DUR-3PL</ta>
            <ta e="T110" id="Seg_8280" s="T109">NEG</ta>
            <ta e="T111" id="Seg_8281" s="T110">слушать-DUR-3PL</ta>
            <ta e="T112" id="Seg_8282" s="T111">я.NOM</ta>
            <ta e="T113" id="Seg_8283" s="T112">этот-ACC.PL</ta>
            <ta e="T114" id="Seg_8284" s="T113">PTCL</ta>
            <ta e="T115" id="Seg_8285" s="T114">бить-DUR-1SG</ta>
            <ta e="T117" id="Seg_8286" s="T116">ребенок-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T118" id="Seg_8287" s="T117">научиться-TR-PRS-1SG</ta>
            <ta e="T119" id="Seg_8288" s="T118">бумага.[NOM.SG]</ta>
            <ta e="T120" id="Seg_8289" s="T119">писать-INF.LAT</ta>
            <ta e="T122" id="Seg_8290" s="T121">камасинец-PL</ta>
            <ta e="T123" id="Seg_8291" s="T122">жить-PST-3PL</ta>
            <ta e="T124" id="Seg_8292" s="T123">кто.[NOM.SG]</ta>
            <ta e="T125" id="Seg_8293" s="T124">очень</ta>
            <ta e="T126" id="Seg_8294" s="T125">деньги.[NOM.SG]</ta>
            <ta e="T127" id="Seg_8295" s="T126">много</ta>
            <ta e="T128" id="Seg_8296" s="T127">а</ta>
            <ta e="T129" id="Seg_8297" s="T128">кто-GEN</ta>
            <ta e="T130" id="Seg_8298" s="T129">NEG.EX.[3SG]</ta>
            <ta e="T131" id="Seg_8299" s="T130">этот.[NOM.SG]</ta>
            <ta e="T132" id="Seg_8300" s="T131">водка.[NOM.SG]</ta>
            <ta e="T133" id="Seg_8301" s="T132">пить-PRS.[3SG]</ta>
            <ta e="T134" id="Seg_8302" s="T133">PTCL</ta>
            <ta e="T135" id="Seg_8303" s="T134">деньги.[NOM.SG]</ta>
            <ta e="T136" id="Seg_8304" s="T135">пить-MOM-FUT-3SG</ta>
            <ta e="T137" id="Seg_8305" s="T136">тогда</ta>
            <ta e="T138" id="Seg_8306" s="T137">что.[NOM.SG]=INDEF</ta>
            <ta e="T139" id="Seg_8307" s="T138">NEG.EX.[3SG]</ta>
            <ta e="T140" id="Seg_8308" s="T139">работать-PRS.[3SG]</ta>
            <ta e="T141" id="Seg_8309" s="T140">работать-PRS.[3SG]</ta>
            <ta e="T142" id="Seg_8310" s="T141">PTCL</ta>
            <ta e="T143" id="Seg_8311" s="T142">пить-MOM-FUT-3SG</ta>
            <ta e="T146" id="Seg_8312" s="T145">Сибирь-LOC</ta>
            <ta e="T147" id="Seg_8313" s="T146">очень</ta>
            <ta e="T148" id="Seg_8314" s="T147">много</ta>
            <ta e="T149" id="Seg_8315" s="T148">камасинец-PL</ta>
            <ta e="T150" id="Seg_8316" s="T149">жить-PST-3PL</ta>
            <ta e="T151" id="Seg_8317" s="T150">тогда</ta>
            <ta e="T152" id="Seg_8318" s="T151">снег.[NOM.SG]</ta>
            <ta e="T153" id="Seg_8319" s="T152">дерево.[NOM.SG]</ta>
            <ta e="T154" id="Seg_8320" s="T153">расти-MOM-PST.[3SG]</ta>
            <ta e="T155" id="Seg_8321" s="T154">этот-PL</ta>
            <ta e="T156" id="Seg_8322" s="T155">сказать-PRS-3PL</ta>
            <ta e="T157" id="Seg_8323" s="T156">ну</ta>
            <ta e="T159" id="Seg_8324" s="T158">русский</ta>
            <ta e="T160" id="Seg_8325" s="T159">люди.[NOM.SG]</ta>
            <ta e="T161" id="Seg_8326" s="T160">прийти-FUT-3PL</ta>
            <ta e="T162" id="Seg_8327" s="T161">мы.LAT</ta>
            <ta e="T163" id="Seg_8328" s="T162">NEG</ta>
            <ta e="T164" id="Seg_8329" s="T163">хороший</ta>
            <ta e="T165" id="Seg_8330" s="T164">стать-FUT-3SG</ta>
            <ta e="T166" id="Seg_8331" s="T165">тогда</ta>
            <ta e="T167" id="Seg_8332" s="T166">лабаз-PL</ta>
            <ta e="T168" id="Seg_8333" s="T167">делать-PST-3PL</ta>
            <ta e="T169" id="Seg_8334" s="T168">там</ta>
            <ta e="T170" id="Seg_8335" s="T169">камень-PL</ta>
            <ta e="T171" id="Seg_8336" s="T170">носить-PST-3PL</ta>
            <ta e="T172" id="Seg_8337" s="T171">и</ta>
            <ta e="T173" id="Seg_8338" s="T172">топор.[NOM.SG]</ta>
            <ta e="T174" id="Seg_8339" s="T173">взять-PST-3PL</ta>
            <ta e="T175" id="Seg_8340" s="T174">резать-PST-3PL</ta>
            <ta e="T176" id="Seg_8341" s="T175">и</ta>
            <ta e="T177" id="Seg_8342" s="T176">этот.[NOM.SG]</ta>
            <ta e="T178" id="Seg_8343" s="T177">упасть-MOM-PST.[3SG]</ta>
            <ta e="T179" id="Seg_8344" s="T178">этот-ACC.PL</ta>
            <ta e="T180" id="Seg_8345" s="T179">этот-PL</ta>
            <ta e="T181" id="Seg_8346" s="T180">PTCL</ta>
            <ta e="T182" id="Seg_8347" s="T181">там</ta>
            <ta e="T183" id="Seg_8348" s="T182">умереть-PST-3PL</ta>
            <ta e="T891" id="Seg_8349" s="T184">лучше</ta>
            <ta e="T185" id="Seg_8350" s="T891">я.LAT</ta>
            <ta e="T186" id="Seg_8351" s="T185">я.NOM</ta>
            <ta e="T187" id="Seg_8352" s="T186">IRREAL</ta>
            <ta e="T188" id="Seg_8353" s="T187">умереть-PST-1SG</ta>
            <ta e="T190" id="Seg_8354" s="T189">ребенок-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T192" id="Seg_8355" s="T190">такой.[NOM.SG]</ta>
            <ta e="T193" id="Seg_8356" s="T192">NEG</ta>
            <ta e="T194" id="Seg_8357" s="T193">хороший-PL</ta>
            <ta e="T195" id="Seg_8358" s="T194">PTCL</ta>
            <ta e="T196" id="Seg_8359" s="T195">украсть-DUR-3PL</ta>
            <ta e="T197" id="Seg_8360" s="T196">люди-COM</ta>
            <ta e="T198" id="Seg_8361" s="T197">бороться-DUR-3PL</ta>
            <ta e="T200" id="Seg_8362" s="T199">PTCL</ta>
            <ta e="T201" id="Seg_8363" s="T200">водка.[NOM.SG]</ta>
            <ta e="T202" id="Seg_8364" s="T201">пить-DUR-3PL</ta>
            <ta e="T203" id="Seg_8365" s="T202">даже</ta>
            <ta e="T204" id="Seg_8366" s="T203">глаз-INS</ta>
            <ta e="T205" id="Seg_8367" s="T204">NEG</ta>
            <ta e="T206" id="Seg_8368" s="T205">мочь-PRS.[3SG]</ta>
            <ta e="T207" id="Seg_8369" s="T206">смотреть-FRQ-INF.LAT</ta>
            <ta e="T208" id="Seg_8370" s="T207">этот-PL-LAT</ta>
            <ta e="T210" id="Seg_8371" s="T209">один.[NOM.SG]</ta>
            <ta e="T211" id="Seg_8372" s="T210">женщина-GEN</ta>
            <ta e="T212" id="Seg_8373" s="T211">три.[NOM.SG]</ta>
            <ta e="T213" id="Seg_8374" s="T212">сын.[NOM.SG]</ta>
            <ta e="T214" id="Seg_8375" s="T213">быть-PST.[3SG]</ta>
            <ta e="T215" id="Seg_8376" s="T214">два.[NOM.SG]</ta>
            <ta e="T216" id="Seg_8377" s="T215">хороший</ta>
            <ta e="T217" id="Seg_8378" s="T216">а</ta>
            <ta e="T218" id="Seg_8379" s="T217">один.[NOM.SG]</ta>
            <ta e="T219" id="Seg_8380" s="T218">NEG</ta>
            <ta e="T220" id="Seg_8381" s="T219">хороший</ta>
            <ta e="T221" id="Seg_8382" s="T220">весь</ta>
            <ta e="T222" id="Seg_8383" s="T221">этот-ACC</ta>
            <ta e="T223" id="Seg_8384" s="T222">бить-PST-3PL</ta>
            <ta e="T224" id="Seg_8385" s="T223">нога-INS</ta>
            <ta e="T225" id="Seg_8386" s="T224">PTCL</ta>
            <ta e="T226" id="Seg_8387" s="T225">и</ta>
            <ta e="T228" id="Seg_8388" s="T227">кулак-INS</ta>
            <ta e="T229" id="Seg_8389" s="T228">и</ta>
            <ta e="T230" id="Seg_8390" s="T229">дерево-INS</ta>
            <ta e="T231" id="Seg_8391" s="T230">PTCL</ta>
            <ta e="T234" id="Seg_8392" s="T233">ударить-MULT-PST-3PL</ta>
            <ta e="T235" id="Seg_8393" s="T234">а</ta>
            <ta e="T236" id="Seg_8394" s="T235">я.NOM</ta>
            <ta e="T237" id="Seg_8395" s="T236">%%</ta>
            <ta e="T239" id="Seg_8396" s="T237">сказать-PST-1SG</ta>
            <ta e="T241" id="Seg_8397" s="T240">а</ta>
            <ta e="T242" id="Seg_8398" s="T241">я.NOM</ta>
            <ta e="T243" id="Seg_8399" s="T242">%%</ta>
            <ta e="T244" id="Seg_8400" s="T243">сказать-PST-1SG</ta>
            <ta e="T245" id="Seg_8401" s="T244">NEG.AUX-IMP.2SG</ta>
            <ta e="T246" id="Seg_8402" s="T245">%бить-CNG</ta>
            <ta e="T247" id="Seg_8403" s="T246">может.быть</ta>
            <ta e="T248" id="Seg_8404" s="T247">расти-FUT-3SG</ta>
            <ta e="T249" id="Seg_8405" s="T248">глупый.[NOM.SG]</ta>
            <ta e="T250" id="Seg_8406" s="T249">много</ta>
            <ta e="T251" id="Seg_8407" s="T250">стать-FUT-3SG</ta>
            <ta e="T252" id="Seg_8408" s="T251">большой.[NOM.SG]</ta>
            <ta e="T253" id="Seg_8409" s="T252">стать-FUT-3SG</ta>
            <ta e="T254" id="Seg_8410" s="T253">может.быть</ta>
            <ta e="T255" id="Seg_8411" s="T254">PTCL</ta>
            <ta e="T256" id="Seg_8412" s="T255">выбросить-FUT-3SG</ta>
            <ta e="T257" id="Seg_8413" s="T256">тогда</ta>
            <ta e="T258" id="Seg_8414" s="T257">хороший</ta>
            <ta e="T259" id="Seg_8415" s="T258">стать-FUT-3SG</ta>
            <ta e="T261" id="Seg_8416" s="T260">а</ta>
            <ta e="T262" id="Seg_8417" s="T261">этот.[NOM.SG]</ta>
            <ta e="T263" id="Seg_8418" s="T262">какой</ta>
            <ta e="T264" id="Seg_8419" s="T263">маленький.[NOM.SG]</ta>
            <ta e="T265" id="Seg_8420" s="T264">быть-PST.[3SG]</ta>
            <ta e="T266" id="Seg_8421" s="T265">и</ta>
            <ta e="T267" id="Seg_8422" s="T266">большой.[NOM.SG]</ta>
            <ta e="T269" id="Seg_8423" s="T268">такой.[NOM.SG]</ta>
            <ta e="T270" id="Seg_8424" s="T269">же</ta>
            <ta e="T271" id="Seg_8425" s="T270">что.[NOM.SG]=INDEF</ta>
            <ta e="T272" id="Seg_8426" s="T271">NEG.EX.[3SG]</ta>
            <ta e="T273" id="Seg_8427" s="T272">хороший</ta>
            <ta e="T275" id="Seg_8428" s="T274">этот.[NOM.SG]</ta>
            <ta e="T276" id="Seg_8429" s="T275">армия-LAT</ta>
            <ta e="T277" id="Seg_8430" s="T276">пойти-PST.[3SG]</ta>
            <ta e="T278" id="Seg_8431" s="T277">всё.равно</ta>
            <ta e="T279" id="Seg_8432" s="T278">прийти-PST.[3SG]</ta>
            <ta e="T280" id="Seg_8433" s="T279">куда</ta>
            <ta e="T281" id="Seg_8434" s="T280">работать-INF.LAT</ta>
            <ta e="T282" id="Seg_8435" s="T281">пойти-FUT-3SG</ta>
            <ta e="T283" id="Seg_8436" s="T282">этот-ACC</ta>
            <ta e="T284" id="Seg_8437" s="T283">PTCL</ta>
            <ta e="T285" id="Seg_8438" s="T284">гнать-DUR-3PL</ta>
            <ta e="T287" id="Seg_8439" s="T286">один.[NOM.SG]</ta>
            <ta e="T288" id="Seg_8440" s="T287">женщина.[NOM.SG]</ta>
            <ta e="T289" id="Seg_8441" s="T288">сын-NOM/GEN.3SG</ta>
            <ta e="T290" id="Seg_8442" s="T289">быть-PST.[3SG]</ta>
            <ta e="T291" id="Seg_8443" s="T290">PTCL</ta>
            <ta e="T292" id="Seg_8444" s="T291">пьяный.[NOM.SG]</ta>
            <ta e="T293" id="Seg_8445" s="T292">ты.NOM</ta>
            <ta e="T294" id="Seg_8446" s="T293">что.[NOM.SG]</ta>
            <ta e="T295" id="Seg_8447" s="T294">я.LAT</ta>
            <ta e="T296" id="Seg_8448" s="T295">NEG</ta>
            <ta e="T297" id="Seg_8449" s="T296">научиться-2SG-PST-2SG</ta>
            <ta e="T298" id="Seg_8450" s="T297">а</ta>
            <ta e="T299" id="Seg_8451" s="T298">этот.[NOM.SG]</ta>
            <ta e="T300" id="Seg_8452" s="T299">сказать-PST.[3SG]</ta>
            <ta e="T301" id="Seg_8453" s="T300">я.NOM</ta>
            <ta e="T302" id="Seg_8454" s="T301">научиться-TR-PST-1SG</ta>
            <ta e="T303" id="Seg_8455" s="T302">ты.ACC</ta>
            <ta e="T304" id="Seg_8456" s="T303">что.[NOM.SG]</ta>
            <ta e="T305" id="Seg_8457" s="T304">нужно</ta>
            <ta e="T306" id="Seg_8458" s="T305">ты.NOM</ta>
            <ta e="T307" id="Seg_8459" s="T306">ты.DAT</ta>
            <ta e="T308" id="Seg_8460" s="T307">ты.NOM</ta>
            <ta e="T310" id="Seg_8461" s="T309">взять-PST-2SG</ta>
            <ta e="T311" id="Seg_8462" s="T310">а</ta>
            <ta e="T312" id="Seg_8463" s="T311">этот.[NOM.SG]</ta>
            <ta e="T313" id="Seg_8464" s="T312">PTCL</ta>
            <ta e="T315" id="Seg_8465" s="T314">водка.[NOM.SG]</ta>
            <ta e="T316" id="Seg_8466" s="T315">пить-PRS-2SG</ta>
            <ta e="T317" id="Seg_8467" s="T316">а</ta>
            <ta e="T318" id="Seg_8468" s="T317">этот.[NOM.SG]</ta>
            <ta e="T319" id="Seg_8469" s="T318">PTCL</ta>
            <ta e="T321" id="Seg_8470" s="T319">кулак-INS=PTCL</ta>
            <ta e="T322" id="Seg_8471" s="T321">мать-NOM/GEN.3SG</ta>
            <ta e="T323" id="Seg_8472" s="T322">хотеть.PST.M.SG</ta>
            <ta e="T324" id="Seg_8473" s="T323">бить-INF.LAT</ta>
            <ta e="T325" id="Seg_8474" s="T324">а</ta>
            <ta e="T326" id="Seg_8475" s="T325">люди.[NOM.SG]</ta>
            <ta e="T327" id="Seg_8476" s="T326">NEG</ta>
            <ta e="T328" id="Seg_8477" s="T327">дать-PST-3PL</ta>
            <ta e="T330" id="Seg_8478" s="T329">я.NOM</ta>
            <ta e="T331" id="Seg_8479" s="T330">брат-NOM/GEN/ACC.1SG</ta>
            <ta e="T332" id="Seg_8480" s="T331">война-LOC</ta>
            <ta e="T333" id="Seg_8481" s="T332">быть-PST-2SG</ta>
            <ta e="T334" id="Seg_8482" s="T333">быть-PST.[3SG]</ta>
            <ta e="T335" id="Seg_8483" s="T334">и</ta>
            <ta e="T336" id="Seg_8484" s="T335">этот-ACC</ta>
            <ta e="T337" id="Seg_8485" s="T336">взять-PST-3PL</ta>
            <ta e="T338" id="Seg_8486" s="T337">плен-LAT</ta>
            <ta e="T339" id="Seg_8487" s="T338">и</ta>
            <ta e="T340" id="Seg_8488" s="T339">правый</ta>
            <ta e="T341" id="Seg_8489" s="T340">рука-NOM/GEN.3SG</ta>
            <ta e="T342" id="Seg_8490" s="T341">палец-3SG-INS</ta>
            <ta e="T344" id="Seg_8491" s="T343">зарезать-INF.LAT</ta>
            <ta e="T345" id="Seg_8492" s="T344">хотеть.PST.PL</ta>
            <ta e="T346" id="Seg_8493" s="T345">а</ta>
            <ta e="T347" id="Seg_8494" s="T346">этот.[NOM.SG]</ta>
            <ta e="T348" id="Seg_8495" s="T347">PTCL</ta>
            <ta e="T349" id="Seg_8496" s="T348">плакать-PST.[3SG]</ta>
            <ta e="T350" id="Seg_8497" s="T349">два.[NOM.SG]</ta>
            <ta e="T351" id="Seg_8498" s="T350">палец</ta>
            <ta e="T353" id="Seg_8499" s="T352">остаться-TR-PST-3PL</ta>
            <ta e="T355" id="Seg_8500" s="T354">этот-LAT</ta>
            <ta e="T357" id="Seg_8501" s="T356">я.NOM</ta>
            <ta e="T358" id="Seg_8502" s="T357">%%</ta>
            <ta e="T359" id="Seg_8503" s="T358">брат-NOM/GEN/ACC.1SG</ta>
            <ta e="T360" id="Seg_8504" s="T359">три.[NOM.SG]</ta>
            <ta e="T361" id="Seg_8505" s="T360">девушка.[NOM.SG]</ta>
            <ta e="T362" id="Seg_8506" s="T361">и</ta>
            <ta e="T363" id="Seg_8507" s="T362">три.[NOM.SG]</ta>
            <ta e="T364" id="Seg_8508" s="T363">мальчик.[NOM.SG]</ta>
            <ta e="T366" id="Seg_8509" s="T365">сказать-INF.LAT</ta>
            <ta e="T367" id="Seg_8510" s="T366">ты.DAT</ta>
            <ta e="T368" id="Seg_8511" s="T367">белый.[NOM.SG]</ta>
            <ta e="T369" id="Seg_8512" s="T368">белый.[NOM.SG]</ta>
            <ta e="T370" id="Seg_8513" s="T369">бык.[NOM.SG]</ta>
            <ta e="T372" id="Seg_8514" s="T371">ты.NOM</ta>
            <ta e="T373" id="Seg_8515" s="T372">сказать-IMP.2SG.O</ta>
            <ta e="T374" id="Seg_8516" s="T373">я.NOM</ta>
            <ta e="T375" id="Seg_8517" s="T374">сказать-IMP.2SG.O</ta>
            <ta e="T376" id="Seg_8518" s="T375">сказать-INF.LAT</ta>
            <ta e="T377" id="Seg_8519" s="T376">ты.DAT</ta>
            <ta e="T378" id="Seg_8520" s="T377">красный.[NOM.SG]</ta>
            <ta e="T379" id="Seg_8521" s="T378">бык.[NOM.SG]</ta>
            <ta e="T381" id="Seg_8522" s="T380">ты.NOM</ta>
            <ta e="T382" id="Seg_8523" s="T381">сказать-IMP.2SG.O</ta>
            <ta e="T383" id="Seg_8524" s="T382">я.NOM</ta>
            <ta e="T384" id="Seg_8525" s="T383">сказать-IMP.2SG.O</ta>
            <ta e="T385" id="Seg_8526" s="T384">сказать-INF.LAT</ta>
            <ta e="T386" id="Seg_8527" s="T385">ты.DAT</ta>
            <ta e="T387" id="Seg_8528" s="T386">черный.[NOM.SG]</ta>
            <ta e="T388" id="Seg_8529" s="T387">бык.[NOM.SG]</ta>
            <ta e="T389" id="Seg_8530" s="T388">Кодур.[NOM.SG]</ta>
            <ta e="T390" id="Seg_8531" s="T389">Кодур.[NOM.SG]</ta>
            <ta e="T391" id="Seg_8532" s="T390">красный.[NOM.SG]</ta>
            <ta e="T392" id="Seg_8533" s="T391">парка.[NOM.SG]</ta>
            <ta e="T393" id="Seg_8534" s="T392">прийти-PST.[3SG]</ta>
            <ta e="T394" id="Seg_8535" s="T393">здесь</ta>
            <ta e="T395" id="Seg_8536" s="T394">люди.[NOM.SG]</ta>
            <ta e="T396" id="Seg_8537" s="T395">жить-PST-3PL</ta>
            <ta e="T397" id="Seg_8538" s="T396">этот.[NOM.SG]</ta>
            <ta e="T398" id="Seg_8539" s="T397">лопатка.[NOM.SG]</ta>
            <ta e="T399" id="Seg_8540" s="T398">найти-PST.[3SG]</ta>
            <ta e="T400" id="Seg_8541" s="T399">мясо.[NOM.SG]</ta>
            <ta e="T401" id="Seg_8542" s="T400">NEG.EX.[3SG]</ta>
            <ta e="T402" id="Seg_8543" s="T401">тогда</ta>
            <ta e="T403" id="Seg_8544" s="T402">пойти-PST.[3SG]</ta>
            <ta e="T404" id="Seg_8545" s="T403">пойти-PST.[3SG]</ta>
            <ta e="T405" id="Seg_8546" s="T404">ну</ta>
            <ta e="T406" id="Seg_8547" s="T405">там</ta>
            <ta e="T407" id="Seg_8548" s="T406">один.[NOM.SG]</ta>
            <ta e="T408" id="Seg_8549" s="T407">мужчина.[NOM.SG]</ta>
            <ta e="T409" id="Seg_8550" s="T408">жить-PST.[3SG]</ta>
            <ta e="T410" id="Seg_8551" s="T409">женщина-COM</ta>
            <ta e="T411" id="Seg_8552" s="T410">этот.[NOM.SG]</ta>
            <ta e="T412" id="Seg_8553" s="T411">прийти-PST.[3SG]</ta>
            <ta e="T413" id="Seg_8554" s="T412">этот-PL</ta>
            <ta e="T414" id="Seg_8555" s="T413">этот-PL-LAT</ta>
            <ta e="T415" id="Seg_8556" s="T414">овца.[NOM.SG]</ta>
            <ta e="T416" id="Seg_8557" s="T415">быть-PST.[3SG]</ta>
            <ta e="T417" id="Seg_8558" s="T416">один.[NOM.SG]</ta>
            <ta e="T418" id="Seg_8559" s="T417">этот.[NOM.SG]</ta>
            <ta e="T419" id="Seg_8560" s="T418">сказать-IPFVZ.[3SG]</ta>
            <ta e="T420" id="Seg_8561" s="T419">надо</ta>
            <ta e="T421" id="Seg_8562" s="T420">желтый.[NOM.SG]</ta>
            <ta e="T422" id="Seg_8563" s="T421">вода.[NOM.SG]</ta>
            <ta e="T423" id="Seg_8564" s="T422">кипятить-INF.LAT</ta>
            <ta e="T424" id="Seg_8565" s="T423">мужчина-ACC</ta>
            <ta e="T426" id="Seg_8566" s="T425">кормить-INF.LAT</ta>
            <ta e="T427" id="Seg_8567" s="T426">а</ta>
            <ta e="T428" id="Seg_8568" s="T427">этот.[NOM.SG]</ta>
            <ta e="T429" id="Seg_8569" s="T428">сказать-IPFVZ.[3SG]</ta>
            <ta e="T430" id="Seg_8570" s="T429">NEG.AUX-IMP.2SG</ta>
            <ta e="T431" id="Seg_8571" s="T430">кипятить-EP-CNG</ta>
            <ta e="T432" id="Seg_8572" s="T431">я.NOM</ta>
            <ta e="T433" id="Seg_8573" s="T432">мясо-NOM/GEN/ACC.1SG</ta>
            <ta e="T434" id="Seg_8574" s="T433">быть-PRS.[3SG]</ta>
            <ta e="T435" id="Seg_8575" s="T434">тогда</ta>
            <ta e="T436" id="Seg_8576" s="T435">котел.[NOM.SG]</ta>
            <ta e="T437" id="Seg_8577" s="T436">вешать-PST-3PL</ta>
            <ta e="T438" id="Seg_8578" s="T437">соль.[NOM.SG]</ta>
            <ta e="T439" id="Seg_8579" s="T438">класть-PST-3PL</ta>
            <ta e="T440" id="Seg_8580" s="T439">этот.[NOM.SG]</ta>
            <ta e="T442" id="Seg_8581" s="T441">пойти-PST.[3SG]</ta>
            <ta e="T443" id="Seg_8582" s="T442">нос</ta>
            <ta e="T444" id="Seg_8583" s="T443">нос-NOM/GEN.3SG</ta>
            <ta e="T445" id="Seg_8584" s="T444">ударить-MULT-PST.[3SG]</ta>
            <ta e="T446" id="Seg_8585" s="T445">кровь-INS</ta>
            <ta e="T447" id="Seg_8586" s="T446">кровь-INS</ta>
            <ta e="T449" id="Seg_8587" s="T447">лопатка-ACC</ta>
            <ta e="T450" id="Seg_8588" s="T449">тогда</ta>
            <ta e="T451" id="Seg_8589" s="T450">котел-NOM/GEN/ACC.3SG</ta>
            <ta e="T452" id="Seg_8590" s="T451">выбросить-MOM-PST.[3SG]</ta>
            <ta e="T453" id="Seg_8591" s="T452">кипятить-PST.[3SG]</ta>
            <ta e="T454" id="Seg_8592" s="T453">кипятить-PST.[3SG]</ta>
            <ta e="T455" id="Seg_8593" s="T454">тогда</ta>
            <ta e="T456" id="Seg_8594" s="T455">мясо.[NOM.SG]</ta>
            <ta e="T457" id="Seg_8595" s="T456">NEG.EX.[3SG]</ta>
            <ta e="T458" id="Seg_8596" s="T457">один.[NOM.SG]</ta>
            <ta e="T459" id="Seg_8597" s="T458">лопатка.[NOM.SG]</ta>
            <ta e="T460" id="Seg_8598" s="T459">этот.[NOM.SG]</ta>
            <ta e="T461" id="Seg_8599" s="T460">PTCL</ta>
            <ta e="T463" id="Seg_8600" s="T462">упасть-MOM-PST.[3SG]</ta>
            <ta e="T464" id="Seg_8601" s="T463">INCH</ta>
            <ta e="T465" id="Seg_8602" s="T464">плакать-INF.LAT</ta>
            <ta e="T466" id="Seg_8603" s="T465">а</ta>
            <ta e="T467" id="Seg_8604" s="T466">этот-PL</ta>
            <ta e="T470" id="Seg_8605" s="T469">сказать-PST-3PL</ta>
            <ta e="T471" id="Seg_8606" s="T470">скоро</ta>
            <ta e="T472" id="Seg_8607" s="T471">овца.[NOM.SG]</ta>
            <ta e="T473" id="Seg_8608" s="T472">зарезать-FUT-1SG</ta>
            <ta e="T474" id="Seg_8609" s="T473">ты.DAT</ta>
            <ta e="T475" id="Seg_8610" s="T474">лопатка-ACC</ta>
            <ta e="T476" id="Seg_8611" s="T475">идти-PST-1SG</ta>
            <ta e="T477" id="Seg_8612" s="T476">я.LAT</ta>
            <ta e="T478" id="Seg_8613" s="T477">NEG</ta>
            <ta e="T479" id="Seg_8614" s="T478">нужно</ta>
            <ta e="T480" id="Seg_8615" s="T479">лопатка.[NOM.SG]</ta>
            <ta e="T481" id="Seg_8616" s="T480">тогда</ta>
            <ta e="T482" id="Seg_8617" s="T481">этот-LAT</ta>
            <ta e="T483" id="Seg_8618" s="T482">овца.[NOM.SG]</ta>
            <ta e="T484" id="Seg_8619" s="T483">дать-PST-3PL</ta>
            <ta e="T486" id="Seg_8620" s="T485">тогда</ta>
            <ta e="T487" id="Seg_8621" s="T486">этот.[NOM.SG]</ta>
            <ta e="T488" id="Seg_8622" s="T487">Кодур.[NOM.SG]</ta>
            <ta e="T490" id="Seg_8623" s="T489">прыгнуть-MOM-PST.[3SG]</ta>
            <ta e="T491" id="Seg_8624" s="T490">мыть-DRV-PST.[3SG]</ta>
            <ta e="T492" id="Seg_8625" s="T491">тереть-PST.[3SG]</ta>
            <ta e="T493" id="Seg_8626" s="T492">глаз-NOM/GEN.3SG</ta>
            <ta e="T494" id="Seg_8627" s="T493">овца-NOM/GEN/ACC.3SG</ta>
            <ta e="T495" id="Seg_8628" s="T494">взять-PST.[3SG]</ta>
            <ta e="T496" id="Seg_8629" s="T495">пойти-PST.[3SG]</ta>
            <ta e="T497" id="Seg_8630" s="T496">тогда</ta>
            <ta e="T499" id="Seg_8631" s="T498">люди.[NOM.SG]</ta>
            <ta e="T500" id="Seg_8632" s="T499">люди-LAT</ta>
            <ta e="T501" id="Seg_8633" s="T500">прийти-PST.[3SG]</ta>
            <ta e="T502" id="Seg_8634" s="T501">там</ta>
            <ta e="T503" id="Seg_8635" s="T502">тоже</ta>
            <ta e="T504" id="Seg_8636" s="T503">два.[NOM.SG]</ta>
            <ta e="T505" id="Seg_8637" s="T504">овца.[NOM.SG]</ta>
            <ta e="T506" id="Seg_8638" s="T505">мальчик.[NOM.SG]</ta>
            <ta e="T507" id="Seg_8639" s="T506">быть-PST.[3SG]</ta>
            <ta e="T508" id="Seg_8640" s="T507">и</ta>
            <ta e="T509" id="Seg_8641" s="T508">мальчик-GEN</ta>
            <ta e="T510" id="Seg_8642" s="T509">женщина.[NOM.SG]</ta>
            <ta e="T511" id="Seg_8643" s="T510">быть-PST.[3SG]</ta>
            <ta e="T512" id="Seg_8644" s="T511">тогда</ta>
            <ta e="T513" id="Seg_8645" s="T512">сказать-PRS-3PL</ta>
            <ta e="T514" id="Seg_8646" s="T513">посылать-IMP.2SG.O</ta>
            <ta e="T515" id="Seg_8647" s="T514">ты.NOM</ta>
            <ta e="T516" id="Seg_8648" s="T515">овца.[NOM.SG]</ta>
            <ta e="T517" id="Seg_8649" s="T516">мы.NOM</ta>
            <ta e="T518" id="Seg_8650" s="T517">овца-INS</ta>
            <ta e="T519" id="Seg_8651" s="T518">нет</ta>
            <ta e="T520" id="Seg_8652" s="T519">NEG</ta>
            <ta e="T521" id="Seg_8653" s="T520">пускать-FUT-1SG</ta>
            <ta e="T522" id="Seg_8654" s="T521">вы.NOM</ta>
            <ta e="T523" id="Seg_8655" s="T522">овца.[NOM.SG]</ta>
            <ta e="T524" id="Seg_8656" s="T523">я.NOM</ta>
            <ta e="T525" id="Seg_8657" s="T524">овца-NOM/GEN/ACC.1SG</ta>
            <ta e="T526" id="Seg_8658" s="T525">съесть-MOM-3PL</ta>
            <ta e="T527" id="Seg_8659" s="T526">а</ta>
            <ta e="T528" id="Seg_8660" s="T527">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T530" id="Seg_8661" s="T529">говорить-PRS.[3SG]</ta>
            <ta e="T531" id="Seg_8662" s="T530">говорить-PRS.[3SG]</ta>
            <ta e="T532" id="Seg_8663" s="T531">этот.[NOM.SG]</ta>
            <ta e="T534" id="Seg_8664" s="T533">бежать-MOM-PST.[3SG]</ta>
            <ta e="T535" id="Seg_8665" s="T534">и</ta>
            <ta e="T536" id="Seg_8666" s="T535">овца-NOM/GEN/ACC.3SG</ta>
            <ta e="T537" id="Seg_8667" s="T536">пускать-PST.[3SG]</ta>
            <ta e="T538" id="Seg_8668" s="T537">этот-LAT</ta>
            <ta e="T539" id="Seg_8669" s="T538">овца-NOM/GEN/ACC.3SG</ta>
            <ta e="T540" id="Seg_8670" s="T539">а</ta>
            <ta e="T541" id="Seg_8671" s="T540">тогда</ta>
            <ta e="T542" id="Seg_8672" s="T541">ложиться-PST-3PL</ta>
            <ta e="T543" id="Seg_8673" s="T542">спать-INF.LAT</ta>
            <ta e="T544" id="Seg_8674" s="T543">утро-GEN</ta>
            <ta e="T545" id="Seg_8675" s="T544">встать-PST-3PL</ta>
            <ta e="T546" id="Seg_8676" s="T545">а</ta>
            <ta e="T547" id="Seg_8677" s="T546">овца.[NOM.SG]</ta>
            <ta e="T548" id="Seg_8678" s="T547">NEG.EX.[3SG]</ta>
            <ta e="T550" id="Seg_8679" s="T549">этот.[NOM.SG]</ta>
            <ta e="T551" id="Seg_8680" s="T550">Кодур.[NOM.SG]</ta>
            <ta e="T552" id="Seg_8681" s="T551">встать-PST.[3SG]</ta>
            <ta e="T553" id="Seg_8682" s="T552">овца-NOM/GEN/ACC.3SG</ta>
            <ta e="T554" id="Seg_8683" s="T553">от-тянуть-PST.[3SG]</ta>
            <ta e="T555" id="Seg_8684" s="T554">и</ta>
            <ta e="T556" id="Seg_8685" s="T555">этот.[NOM.SG]</ta>
            <ta e="T557" id="Seg_8686" s="T556">этот-GEN.PL</ta>
            <ta e="T558" id="Seg_8687" s="T557">овца-NOM/GEN/ACC.3SG</ta>
            <ta e="T559" id="Seg_8688" s="T558">PTCL</ta>
            <ta e="T560" id="Seg_8689" s="T559">кровь-INS</ta>
            <ta e="T562" id="Seg_8690" s="T561">кровь-INS</ta>
            <ta e="T563" id="Seg_8691" s="T562">PTCL</ta>
            <ta e="T564" id="Seg_8692" s="T563">мазать-TR-PST.[3SG]</ta>
            <ta e="T565" id="Seg_8693" s="T564">нос-NOM/GEN.3SG</ta>
            <ta e="T566" id="Seg_8694" s="T565">и</ta>
            <ta e="T567" id="Seg_8695" s="T566">рот-NOM/GEN/ACC.3SG</ta>
            <ta e="T568" id="Seg_8696" s="T567">а</ta>
            <ta e="T569" id="Seg_8697" s="T568">утро-GEN</ta>
            <ta e="T570" id="Seg_8698" s="T569">встать-PST-3PL</ta>
            <ta e="T571" id="Seg_8699" s="T570">сказать-IPFVZ-3PL</ta>
            <ta e="T572" id="Seg_8700" s="T571">правда</ta>
            <ta e="T574" id="Seg_8701" s="T573">мы.NOM</ta>
            <ta e="T575" id="Seg_8702" s="T574">овца-NOM/GEN/ACC.3SG</ta>
            <ta e="T576" id="Seg_8703" s="T575">этот.[NOM.SG]</ta>
            <ta e="T577" id="Seg_8704" s="T576">этот-ACC</ta>
            <ta e="T578" id="Seg_8705" s="T577">овца-NOM/GEN/ACC.3SG</ta>
            <ta e="T579" id="Seg_8706" s="T578">PTCL</ta>
            <ta e="T580" id="Seg_8707" s="T579">от-рвать-PST-3PL</ta>
            <ta e="T582" id="Seg_8708" s="T581">тогда</ta>
            <ta e="T583" id="Seg_8709" s="T582">этот.[NOM.SG]</ta>
            <ta e="T584" id="Seg_8710" s="T583">встать-PST.[3SG]</ta>
            <ta e="T585" id="Seg_8711" s="T584">глаз-ACC.3SG</ta>
            <ta e="T586" id="Seg_8712" s="T585">мыть-PST.[3SG]</ta>
            <ta e="T587" id="Seg_8713" s="T586">тереть-PST.[3SG]</ta>
            <ta e="T588" id="Seg_8714" s="T587">два.[NOM.SG]</ta>
            <ta e="T589" id="Seg_8715" s="T588">овца.[NOM.SG]</ta>
            <ta e="T590" id="Seg_8716" s="T589">взять-PST.[3SG]</ta>
            <ta e="T591" id="Seg_8717" s="T590">и</ta>
            <ta e="T592" id="Seg_8718" s="T591">идти-PRS.[3SG]</ta>
            <ta e="T595" id="Seg_8719" s="T594">пойти-MOM-PST.[3SG]</ta>
            <ta e="T596" id="Seg_8720" s="T595">тогда</ta>
            <ta e="T597" id="Seg_8721" s="T596">прийти-PRS.[3SG]</ta>
            <ta e="T598" id="Seg_8722" s="T597">прийти-PRS.[3SG]</ta>
            <ta e="T599" id="Seg_8723" s="T598">там</ta>
            <ta e="T600" id="Seg_8724" s="T599">PTCL</ta>
            <ta e="T601" id="Seg_8725" s="T600">крест.[NOM.SG]</ta>
            <ta e="T602" id="Seg_8726" s="T601">стоять-DUR.[3SG]</ta>
            <ta e="T603" id="Seg_8727" s="T602">этот.[NOM.SG]</ta>
            <ta e="T604" id="Seg_8728" s="T603">копать-PST.[3SG]</ta>
            <ta e="T605" id="Seg_8729" s="T604">там</ta>
            <ta e="T606" id="Seg_8730" s="T605">женщина.[NOM.SG]</ta>
            <ta e="T607" id="Seg_8731" s="T606">лежать-DUR.[3SG]</ta>
            <ta e="T608" id="Seg_8732" s="T607">этот.[NOM.SG]</ta>
            <ta e="T609" id="Seg_8733" s="T608">женщина.[NOM.SG]</ta>
            <ta e="T610" id="Seg_8734" s="T609">быть-PST.[3SG]</ta>
            <ta e="T611" id="Seg_8735" s="T610">и</ta>
            <ta e="T612" id="Seg_8736" s="T611">два.[NOM.SG]</ta>
            <ta e="T613" id="Seg_8737" s="T612">овца.[NOM.SG]</ta>
            <ta e="T614" id="Seg_8738" s="T613">и</ta>
            <ta e="T615" id="Seg_8739" s="T614">этот.[NOM.SG]</ta>
            <ta e="T616" id="Seg_8740" s="T615">овца-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T617" id="Seg_8741" s="T616">этот.[NOM.SG]</ta>
            <ta e="T618" id="Seg_8742" s="T617">женщина-LAT</ta>
            <ta e="T619" id="Seg_8743" s="T618">рука-PL-LAT</ta>
            <ta e="T620" id="Seg_8744" s="T619">завязать-PST.[3SG]</ta>
            <ta e="T621" id="Seg_8745" s="T620">сердце-NOM/GEN/ACC.3SG</ta>
            <ta e="T622" id="Seg_8746" s="T621">сердце-LAT/LOC.3SG</ta>
            <ta e="T623" id="Seg_8747" s="T622">нож.[NOM.SG]</ta>
            <ta e="T625" id="Seg_8748" s="T624">ползти-MOM-PST.[3SG]</ta>
            <ta e="T626" id="Seg_8749" s="T625">тогда</ta>
            <ta e="T627" id="Seg_8750" s="T626">прийти-PST.[3SG]</ta>
            <ta e="T629" id="Seg_8751" s="T628">там</ta>
            <ta e="T630" id="Seg_8752" s="T629">два.[NOM.SG]</ta>
            <ta e="T631" id="Seg_8753" s="T630">девушка.[NOM.SG]</ta>
            <ta e="T632" id="Seg_8754" s="T631">и</ta>
            <ta e="T633" id="Seg_8755" s="T632">мужчина.[NOM.SG]</ta>
            <ta e="T634" id="Seg_8756" s="T633">жить-DUR.[3SG]</ta>
            <ta e="T635" id="Seg_8757" s="T634">говорить-PRS.[3SG]</ta>
            <ta e="T636" id="Seg_8758" s="T635">говорить-PRS.[3SG]</ta>
            <ta e="T637" id="Seg_8759" s="T636">я.NOM</ta>
            <ta e="T638" id="Seg_8760" s="T637">там</ta>
            <ta e="T639" id="Seg_8761" s="T638">женщина.[NOM.SG]</ta>
            <ta e="T640" id="Seg_8762" s="T639">сидеть-DUR.[3SG]</ta>
            <ta e="T641" id="Seg_8763" s="T640">овца-INS</ta>
            <ta e="T642" id="Seg_8764" s="T641">а</ta>
            <ta e="T643" id="Seg_8765" s="T642">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T645" id="Seg_8766" s="T644">сердце-LAT/LOC.3SG</ta>
            <ta e="T646" id="Seg_8767" s="T645">нож.[NOM.SG]</ta>
            <ta e="T647" id="Seg_8768" s="T646">сидеть-DUR.[3SG]</ta>
            <ta e="T648" id="Seg_8769" s="T647">а</ta>
            <ta e="T649" id="Seg_8770" s="T648">дочь-PL</ta>
            <ta e="T650" id="Seg_8771" s="T649">мы.NOM</ta>
            <ta e="T651" id="Seg_8772" s="T650">пойти-FUT-1DU</ta>
            <ta e="T652" id="Seg_8773" s="T651">принести-FUT-1DU</ta>
            <ta e="T653" id="Seg_8774" s="T652">женщина-NOM/GEN/ACC.2SG</ta>
            <ta e="T654" id="Seg_8775" s="T653">ты.NOM</ta>
            <ta e="T655" id="Seg_8776" s="T654">этот.[3SG]</ta>
            <ta e="T656" id="Seg_8777" s="T655">очень</ta>
            <ta e="T657" id="Seg_8778" s="T656">бояться-PRS.[3SG]</ta>
            <ta e="T658" id="Seg_8779" s="T657">а.то</ta>
            <ta e="T659" id="Seg_8780" s="T658">зарезать-FUT-3SG</ta>
            <ta e="T660" id="Seg_8781" s="T659">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T661" id="Seg_8782" s="T660">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T663" id="Seg_8783" s="T662">тогда</ta>
            <ta e="T664" id="Seg_8784" s="T663">этот-PL</ta>
            <ta e="T665" id="Seg_8785" s="T664">дочь-PL</ta>
            <ta e="T666" id="Seg_8786" s="T665">бежать-MOM-PST-3PL</ta>
            <ta e="T667" id="Seg_8787" s="T666">прийти-PST-3PL</ta>
            <ta e="T668" id="Seg_8788" s="T667">зарезать-MOM-PST.[3SG]</ta>
            <ta e="T669" id="Seg_8789" s="T668">овца-NOM/GEN/ACC.3SG</ta>
            <ta e="T670" id="Seg_8790" s="T669">PTCL</ta>
            <ta e="T672" id="Seg_8791" s="T671">бежать-MOM-PST-3PL</ta>
            <ta e="T673" id="Seg_8792" s="T672">женщина-NOM/GEN/ACC.1SG</ta>
            <ta e="T674" id="Seg_8793" s="T673">тогда</ta>
            <ta e="T675" id="Seg_8794" s="T674">этот.[NOM.SG]</ta>
            <ta e="T676" id="Seg_8795" s="T675">PTCL</ta>
            <ta e="T677" id="Seg_8796" s="T676">упасть-MOM-PST.[3SG]</ta>
            <ta e="T678" id="Seg_8797" s="T677">Кодур.[NOM.SG]</ta>
            <ta e="T679" id="Seg_8798" s="T678">плакать-PST.[3SG]</ta>
            <ta e="T680" id="Seg_8799" s="T679">плакать-PST.[3SG]</ta>
            <ta e="T681" id="Seg_8800" s="T680">этот.[NOM.SG]</ta>
            <ta e="T682" id="Seg_8801" s="T681">мужчина.[NOM.SG]</ta>
            <ta e="T683" id="Seg_8802" s="T682">сказать-IPFVZ.[3SG]</ta>
            <ta e="T684" id="Seg_8803" s="T683">взять-IMP.2SG.O</ta>
            <ta e="T685" id="Seg_8804" s="T684">один.[NOM.SG]</ta>
            <ta e="T686" id="Seg_8805" s="T685">девушка.[NOM.SG]</ta>
            <ta e="T687" id="Seg_8806" s="T686">зачем</ta>
            <ta e="T688" id="Seg_8807" s="T687">я.LAT</ta>
            <ta e="T689" id="Seg_8808" s="T688">один.[NOM.SG]</ta>
            <ta e="T690" id="Seg_8809" s="T689">я.NOM</ta>
            <ta e="T691" id="Seg_8810" s="T690">женщина-NOM/GEN/ACC.1SG</ta>
            <ta e="T692" id="Seg_8811" s="T691">лучше</ta>
            <ta e="T693" id="Seg_8812" s="T692">быть-PST.[3SG]</ta>
            <ta e="T694" id="Seg_8813" s="T693">ну</ta>
            <ta e="T695" id="Seg_8814" s="T694">взять-IMP.2SG.O</ta>
            <ta e="T696" id="Seg_8815" s="T695">два.[NOM.SG]</ta>
            <ta e="T697" id="Seg_8816" s="T696">тогда</ta>
            <ta e="T698" id="Seg_8817" s="T697">этот.[NOM.SG]</ta>
            <ta e="T699" id="Seg_8818" s="T698">встать-PST.[3SG]</ta>
            <ta e="T700" id="Seg_8819" s="T699">опять</ta>
            <ta e="T701" id="Seg_8820" s="T700">мыть-DRV-PST.[3SG]</ta>
            <ta e="T702" id="Seg_8821" s="T701">лицо-NOM/GEN/ACC.3SG</ta>
            <ta e="T703" id="Seg_8822" s="T702">и</ta>
            <ta e="T704" id="Seg_8823" s="T703">два.[NOM.SG]</ta>
            <ta e="T705" id="Seg_8824" s="T704">девушка.[NOM.SG]</ta>
            <ta e="T706" id="Seg_8825" s="T705">взять-PST.[3SG]</ta>
            <ta e="T707" id="Seg_8826" s="T706">и</ta>
            <ta e="T708" id="Seg_8827" s="T707">пойти-PST.[3SG]</ta>
            <ta e="T709" id="Seg_8828" s="T708">тогда</ta>
            <ta e="T710" id="Seg_8829" s="T709">прийти-PRS.[3SG]</ta>
            <ta e="T711" id="Seg_8830" s="T710">прийти-PRS.[3SG]</ta>
            <ta e="T712" id="Seg_8831" s="T711">там</ta>
            <ta e="T713" id="Seg_8832" s="T712">овца-PL</ta>
            <ta e="T714" id="Seg_8833" s="T713">идти-DUR-3PL</ta>
            <ta e="T715" id="Seg_8834" s="T714">тогда</ta>
            <ta e="T716" id="Seg_8835" s="T715">прийти-PST.[3SG]</ta>
            <ta e="T717" id="Seg_8836" s="T716">пастух-LAT</ta>
            <ta e="T718" id="Seg_8837" s="T717">здесь</ta>
            <ta e="T719" id="Seg_8838" s="T718">я.NOM</ta>
            <ta e="T720" id="Seg_8839" s="T719">овца.[NOM.SG]</ta>
            <ta e="T721" id="Seg_8840" s="T720">и</ta>
            <ta e="T723" id="Seg_8841" s="T722">один.[NOM.SG]</ta>
            <ta e="T725" id="Seg_8842" s="T724">сесть-PST.[3SG]</ta>
            <ta e="T726" id="Seg_8843" s="T725">вода-LAT</ta>
            <ta e="T728" id="Seg_8844" s="T727">берег-LAT/LOC.3SG</ta>
            <ta e="T729" id="Seg_8845" s="T728">один.[NOM.SG]</ta>
            <ta e="T730" id="Seg_8846" s="T729">сесть-%%-PST.[3SG]</ta>
            <ta e="T731" id="Seg_8847" s="T730">лес-LOC</ta>
            <ta e="T732" id="Seg_8848" s="T731">тогда</ta>
            <ta e="T733" id="Seg_8849" s="T732">сказать-DUR.[3SG]</ta>
            <ta e="T734" id="Seg_8850" s="T733">я.NOM</ta>
            <ta e="T735" id="Seg_8851" s="T734">овца.[NOM.SG]</ta>
            <ta e="T736" id="Seg_8852" s="T735">ну</ta>
            <ta e="T737" id="Seg_8853" s="T736">спросить-IMP.2SG.O</ta>
            <ta e="T740" id="Seg_8854" s="T739">спросить-IMP.2SG.O</ta>
            <ta e="T741" id="Seg_8855" s="T740">этот.[NOM.SG]</ta>
            <ta e="T742" id="Seg_8856" s="T741">спросить-MOM-PST.[3SG]</ta>
            <ta e="T743" id="Seg_8857" s="T742">Кодур-EP-GEN</ta>
            <ta e="T744" id="Seg_8858" s="T743">овца-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T745" id="Seg_8859" s="T744">тогда</ta>
            <ta e="T746" id="Seg_8860" s="T745">лес-LOC</ta>
            <ta e="T747" id="Seg_8861" s="T746">мужчина-LAT</ta>
            <ta e="T748" id="Seg_8862" s="T747">спросить-IMP.2SG.O</ta>
            <ta e="T749" id="Seg_8863" s="T748">спросить-PST.[3SG]</ta>
            <ta e="T750" id="Seg_8864" s="T749">этот.[NOM.SG]</ta>
            <ta e="T751" id="Seg_8865" s="T750">сказать-IPFVZ.[3SG]</ta>
            <ta e="T752" id="Seg_8866" s="T751">Кодур-EP-GEN</ta>
            <ta e="T753" id="Seg_8867" s="T752">овца-NOM/GEN/ACC.3SG</ta>
            <ta e="T754" id="Seg_8868" s="T753">тогда</ta>
            <ta e="T756" id="Seg_8869" s="T755">взять-PST.[3SG]</ta>
            <ta e="T757" id="Seg_8870" s="T756">овца-NOM/GEN/ACC.3SG</ta>
            <ta e="T759" id="Seg_8871" s="T758">опять</ta>
            <ta e="T761" id="Seg_8872" s="T760">прийти-PST.[3SG]</ta>
            <ta e="T762" id="Seg_8873" s="T761">мужчина.[NOM.SG]</ta>
            <ta e="T763" id="Seg_8874" s="T762">лошадь-PL</ta>
            <ta e="T765" id="Seg_8875" s="T764">жить-DUR.[3SG]</ta>
            <ta e="T766" id="Seg_8876" s="T765">я.NOM</ta>
            <ta e="T767" id="Seg_8877" s="T766">лошадь-PL</ta>
            <ta e="T768" id="Seg_8878" s="T767">а</ta>
            <ta e="T769" id="Seg_8879" s="T768">этот.[NOM.SG]</ta>
            <ta e="T770" id="Seg_8880" s="T769">сказать-IPFVZ.[3SG]</ta>
            <ta e="T771" id="Seg_8881" s="T770">нет</ta>
            <ta e="T772" id="Seg_8882" s="T771">я.NOM</ta>
            <ta e="T773" id="Seg_8883" s="T772">лошадь-PL</ta>
            <ta e="T774" id="Seg_8884" s="T773">ну</ta>
            <ta e="T775" id="Seg_8885" s="T774">спросить-IMP.2SG.O</ta>
            <ta e="T776" id="Seg_8886" s="T775">вода-LAT</ta>
            <ta e="T777" id="Seg_8887" s="T776">мужчина-ACC</ta>
            <ta e="T778" id="Seg_8888" s="T777">этот.[NOM.SG]</ta>
            <ta e="T779" id="Seg_8889" s="T778">спросить-PST.[3SG]</ta>
            <ta e="T780" id="Seg_8890" s="T779">Кодур-EP-GEN</ta>
            <ta e="T781" id="Seg_8891" s="T780">лошадь-PL</ta>
            <ta e="T782" id="Seg_8892" s="T781">тогда</ta>
            <ta e="T783" id="Seg_8893" s="T782">лес-GEN</ta>
            <ta e="T784" id="Seg_8894" s="T783">мужчина-LAT</ta>
            <ta e="T785" id="Seg_8895" s="T784">спросить-IMP.2SG.O</ta>
            <ta e="T786" id="Seg_8896" s="T785">этот.[NOM.SG]</ta>
            <ta e="T787" id="Seg_8897" s="T786">спросить-PST.[3SG]</ta>
            <ta e="T788" id="Seg_8898" s="T787">Кодур-EP-GEN</ta>
            <ta e="T789" id="Seg_8899" s="T788">лошадь-PL</ta>
            <ta e="T790" id="Seg_8900" s="T789">этот.[NOM.SG]</ta>
            <ta e="T791" id="Seg_8901" s="T790">PTCL</ta>
            <ta e="T792" id="Seg_8902" s="T791">посылать-PST.[3SG]</ta>
            <ta e="T793" id="Seg_8903" s="T792">тогда</ta>
            <ta e="T794" id="Seg_8904" s="T793">корова-PL</ta>
            <ta e="T795" id="Seg_8905" s="T794">идти-DUR-3PL</ta>
            <ta e="T796" id="Seg_8906" s="T795">и</ta>
            <ta e="T797" id="Seg_8907" s="T796">там</ta>
            <ta e="T798" id="Seg_8908" s="T797">тоже</ta>
            <ta e="T799" id="Seg_8909" s="T798">мужчина.[NOM.SG]</ta>
            <ta e="T800" id="Seg_8910" s="T799">смотреть-DUR.[3SG]</ta>
            <ta e="T801" id="Seg_8911" s="T800">корова-PL</ta>
            <ta e="T802" id="Seg_8912" s="T801">этот.[NOM.SG]</ta>
            <ta e="T803" id="Seg_8913" s="T802">прийти-PST.[3SG]</ta>
            <ta e="T804" id="Seg_8914" s="T803">я.NOM</ta>
            <ta e="T805" id="Seg_8915" s="T804">корова-PL</ta>
            <ta e="T806" id="Seg_8916" s="T805">нет</ta>
            <ta e="T807" id="Seg_8917" s="T806">NEG</ta>
            <ta e="T808" id="Seg_8918" s="T807">ты.NOM</ta>
            <ta e="T809" id="Seg_8919" s="T808">ну</ta>
            <ta e="T810" id="Seg_8920" s="T809">спросить-IMP.2SG</ta>
            <ta e="T811" id="Seg_8921" s="T810">вода-LOC</ta>
            <ta e="T812" id="Seg_8922" s="T811">мужчина.[NOM.SG]</ta>
            <ta e="T813" id="Seg_8923" s="T812">тогда</ta>
            <ta e="T814" id="Seg_8924" s="T813">лес-LOC</ta>
            <ta e="T815" id="Seg_8925" s="T814">этот.[NOM.SG]</ta>
            <ta e="T816" id="Seg_8926" s="T815">спросить-PST.[3SG]</ta>
            <ta e="T817" id="Seg_8927" s="T816">кто-GEN</ta>
            <ta e="T818" id="Seg_8928" s="T817">корова-NOM/GEN/ACC.3SG</ta>
            <ta e="T819" id="Seg_8929" s="T818">этот</ta>
            <ta e="T820" id="Seg_8930" s="T819">этот</ta>
            <ta e="T821" id="Seg_8931" s="T820">сказать-PST.[3SG]</ta>
            <ta e="T822" id="Seg_8932" s="T821">Кодур-GEN</ta>
            <ta e="T823" id="Seg_8933" s="T822">корова-NOM/GEN/ACC.3SG</ta>
            <ta e="T824" id="Seg_8934" s="T823">и</ta>
            <ta e="T825" id="Seg_8935" s="T824">лес-LOC</ta>
            <ta e="T826" id="Seg_8936" s="T825">спросить-PST.[3SG]</ta>
            <ta e="T827" id="Seg_8937" s="T826">кто-GEN</ta>
            <ta e="T828" id="Seg_8938" s="T827">корова-NOM/GEN/ACC.3SG</ta>
            <ta e="T829" id="Seg_8939" s="T828">Кодур-EP-GEN</ta>
            <ta e="T830" id="Seg_8940" s="T829">корова-NOM/GEN/ACC.3SG</ta>
            <ta e="T831" id="Seg_8941" s="T830">тогда</ta>
            <ta e="T832" id="Seg_8942" s="T831">этот.[NOM.SG]</ta>
            <ta e="T833" id="Seg_8943" s="T832">INCH</ta>
            <ta e="T834" id="Seg_8944" s="T833">петь-INF.LAT</ta>
            <ta e="T835" id="Seg_8945" s="T834">песня.[NOM.SG]</ta>
            <ta e="T836" id="Seg_8946" s="T835">я.NOM</ta>
            <ta e="T837" id="Seg_8947" s="T836">лопатка.[NOM.SG]</ta>
            <ta e="T840" id="Seg_8948" s="T839">взять-PST-1SG</ta>
            <ta e="T842" id="Seg_8949" s="T841">овца.[NOM.SG]</ta>
            <ta e="T843" id="Seg_8950" s="T842">взять-PST-1SG</ta>
            <ta e="T844" id="Seg_8951" s="T843">овца-NOM/GEN/ACC.3SG</ta>
            <ta e="T845" id="Seg_8952" s="T844">два.[NOM.SG]</ta>
            <ta e="T846" id="Seg_8953" s="T845">овца.[NOM.SG]</ta>
            <ta e="T847" id="Seg_8954" s="T846">взять-PST-1SG</ta>
            <ta e="T848" id="Seg_8955" s="T847">два.[NOM.SG]</ta>
            <ta e="T849" id="Seg_8956" s="T848">овца.[NOM.SG]</ta>
            <ta e="T850" id="Seg_8957" s="T849">взять-PST-1SG</ta>
            <ta e="T851" id="Seg_8958" s="T850">пойти-PST-1SG</ta>
            <ta e="T852" id="Seg_8959" s="T851">женщина.[NOM.SG]</ta>
            <ta e="T853" id="Seg_8960" s="T852">земля-LOC</ta>
            <ta e="T854" id="Seg_8961" s="T853">взять-PST-1SG</ta>
            <ta e="T855" id="Seg_8962" s="T854">и</ta>
            <ta e="T856" id="Seg_8963" s="T855">два.[NOM.SG]</ta>
            <ta e="T857" id="Seg_8964" s="T856">овца.[NOM.SG]</ta>
            <ta e="T858" id="Seg_8965" s="T857">и</ta>
            <ta e="T859" id="Seg_8966" s="T858">женщина.[NOM.SG]</ta>
            <ta e="T860" id="Seg_8967" s="T859">нож.[NOM.SG]</ta>
            <ta e="T863" id="Seg_8968" s="T862">сердце-NOM/GEN/ACC.3SG</ta>
            <ta e="T865" id="Seg_8969" s="T864">нож.[NOM.SG]</ta>
            <ta e="T866" id="Seg_8970" s="T865">сердце-NOM/GEN/ACC.3SG</ta>
            <ta e="T868" id="Seg_8971" s="T867">уколоть-MOM-PST.[3SG]</ta>
            <ta e="T869" id="Seg_8972" s="T868">а</ta>
            <ta e="T870" id="Seg_8973" s="T869">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T871" id="Seg_8974" s="T870">прийти-PST.[3SG]</ta>
            <ta e="T872" id="Seg_8975" s="T871">я.NOM</ta>
            <ta e="T873" id="Seg_8976" s="T872">там</ta>
            <ta e="T874" id="Seg_8977" s="T873">женщина-NOM/GEN/ACC.1SG</ta>
            <ta e="T875" id="Seg_8978" s="T874">быть-PRS.[3SG]</ta>
            <ta e="T876" id="Seg_8979" s="T875">и</ta>
            <ta e="T877" id="Seg_8980" s="T876">два.[NOM.SG]</ta>
            <ta e="T878" id="Seg_8981" s="T877">овца.[NOM.SG]</ta>
            <ta e="T879" id="Seg_8982" s="T878">и</ta>
            <ta e="T880" id="Seg_8983" s="T879">два.[NOM.SG]</ta>
            <ta e="T881" id="Seg_8984" s="T880">дочь-PL</ta>
            <ta e="T882" id="Seg_8985" s="T881">и</ta>
            <ta e="T883" id="Seg_8986" s="T882">мужчина.[NOM.SG]</ta>
            <ta e="T884" id="Seg_8987" s="T883">жить-DUR.[3SG]</ta>
            <ta e="T885" id="Seg_8988" s="T884">этот.[NOM.SG]</ta>
            <ta e="T886" id="Seg_8989" s="T885">PTCL</ta>
            <ta e="T887" id="Seg_8990" s="T886">мужчина.[NOM.SG]</ta>
            <ta e="T888" id="Seg_8991" s="T887">говорить-PRS.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T3" id="Seg_8992" s="T2">v-v:n.fin</ta>
            <ta e="T4" id="Seg_8993" s="T3">pers</ta>
            <ta e="T5" id="Seg_8994" s="T4">dempro-n:case</ta>
            <ta e="T6" id="Seg_8995" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_8996" s="T6">n-n:case.poss</ta>
            <ta e="T8" id="Seg_8997" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_8998" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_8999" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_9000" s="T10">v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_9001" s="T11">num-n:case</ta>
            <ta e="T13" id="Seg_9002" s="T12">num-n:case</ta>
            <ta e="T14" id="Seg_9003" s="T13">num-n:case</ta>
            <ta e="T15" id="Seg_9004" s="T14">num-n:case</ta>
            <ta e="T16" id="Seg_9005" s="T15">dempro-n:num</ta>
            <ta e="T17" id="Seg_9006" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_9007" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_9008" s="T18">que-n:case=ptcl</ta>
            <ta e="T20" id="Seg_9009" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_9010" s="T20">v-v&gt;v-v:pn</ta>
            <ta e="T22" id="Seg_9011" s="T21">conj</ta>
            <ta e="T23" id="Seg_9012" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_9013" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_9014" s="T24">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_9015" s="T26">dempro-n:num</ta>
            <ta e="T28" id="Seg_9016" s="T27">dempro-n:case</ta>
            <ta e="T30" id="Seg_9017" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_9018" s="T30">v-v:n.fin</ta>
            <ta e="T32" id="Seg_9019" s="T31">v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_9020" s="T33">conj</ta>
            <ta e="T35" id="Seg_9021" s="T34">que</ta>
            <ta e="T36" id="Seg_9022" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_9023" s="T36">v-v:pn</ta>
            <ta e="T39" id="Seg_9024" s="T38">%%</ta>
            <ta e="T40" id="Seg_9025" s="T39">n-n:case</ta>
            <ta e="T41" id="Seg_9026" s="T40">v-v:n.fin</ta>
            <ta e="T42" id="Seg_9027" s="T41">dempro-n:case</ta>
            <ta e="T43" id="Seg_9028" s="T42">n-n:case.poss</ta>
            <ta e="T44" id="Seg_9029" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_9030" s="T44">v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_9031" s="T45">que-n:case</ta>
            <ta e="T47" id="Seg_9032" s="T46">v-v:tense-v:pn</ta>
            <ta e="T48" id="Seg_9033" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_9034" s="T48">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_9035" s="T49">conj</ta>
            <ta e="T51" id="Seg_9036" s="T50">v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_9037" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_9038" s="T52">adv</ta>
            <ta e="T55" id="Seg_9039" s="T54">pers</ta>
            <ta e="T56" id="Seg_9040" s="T55">n-n:case.poss</ta>
            <ta e="T892" id="Seg_9041" s="T56">v-v:n-fin</ta>
            <ta e="T57" id="Seg_9042" s="T892">v-v:tense-v:pn</ta>
            <ta e="T58" id="Seg_9043" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_9044" s="T58">conj</ta>
            <ta e="T60" id="Seg_9045" s="T59">adv</ta>
            <ta e="T61" id="Seg_9046" s="T60">n-n:case</ta>
            <ta e="T62" id="Seg_9047" s="T61">v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_9048" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_9049" s="T63">v-v:n.fin</ta>
            <ta e="T65" id="Seg_9050" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_9051" s="T65">n-n:num</ta>
            <ta e="T67" id="Seg_9052" s="T66">n-n:num</ta>
            <ta e="T68" id="Seg_9053" s="T67">quant</ta>
            <ta e="T70" id="Seg_9054" s="T69">que-n:case</ta>
            <ta e="T71" id="Seg_9055" s="T70">v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_9056" s="T71">v-v:n.fin</ta>
            <ta e="T74" id="Seg_9057" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_9058" s="T74">conj</ta>
            <ta e="T76" id="Seg_9059" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_9060" s="T76">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_9061" s="T77">adv</ta>
            <ta e="T79" id="Seg_9062" s="T78">adj</ta>
            <ta e="T80" id="Seg_9063" s="T79">v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_9064" s="T81">num-n:case</ta>
            <ta e="T84" id="Seg_9065" s="T83">num-n:case</ta>
            <ta e="T85" id="Seg_9066" s="T84">num-n:case</ta>
            <ta e="T86" id="Seg_9067" s="T85">v-v:tense-v:pn</ta>
            <ta e="T87" id="Seg_9068" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_9069" s="T87">v-%%-v:pn</ta>
            <ta e="T89" id="Seg_9070" s="T88">v-v&gt;v-v:pn</ta>
            <ta e="T90" id="Seg_9071" s="T89">conj</ta>
            <ta e="T91" id="Seg_9072" s="T90">dempro-n:num</ta>
            <ta e="T92" id="Seg_9073" s="T91">n-n:num-n:case</ta>
            <ta e="T93" id="Seg_9074" s="T92">v-v:tense-v:pn</ta>
            <ta e="T94" id="Seg_9075" s="T93">adv</ta>
            <ta e="T95" id="Seg_9076" s="T94">n-n:num-n:case</ta>
            <ta e="T96" id="Seg_9077" s="T95">adj</ta>
            <ta e="T97" id="Seg_9078" s="T96">que-n:case</ta>
            <ta e="T98" id="Seg_9079" s="T97">v-v:tense-v:pn</ta>
            <ta e="T99" id="Seg_9080" s="T98">dempro-n:num</ta>
            <ta e="T100" id="Seg_9081" s="T99">v-v&gt;v-v:pn</ta>
            <ta e="T102" id="Seg_9082" s="T101">conj</ta>
            <ta e="T103" id="Seg_9083" s="T102">pers</ta>
            <ta e="T104" id="Seg_9084" s="T103">n-n:num</ta>
            <ta e="T105" id="Seg_9085" s="T104">adv</ta>
            <ta e="T106" id="Seg_9086" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_9087" s="T106">adj</ta>
            <ta e="T108" id="Seg_9088" s="T107">v-v&gt;v-v:pn</ta>
            <ta e="T109" id="Seg_9089" s="T108">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T110" id="Seg_9090" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_9091" s="T110">v-v&gt;v-v:pn</ta>
            <ta e="T112" id="Seg_9092" s="T111">pers</ta>
            <ta e="T113" id="Seg_9093" s="T112">dempro-n:case</ta>
            <ta e="T114" id="Seg_9094" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_9095" s="T114">v-v&gt;v-v:pn</ta>
            <ta e="T117" id="Seg_9096" s="T116">n-n:num-n:case.poss</ta>
            <ta e="T118" id="Seg_9097" s="T117">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T119" id="Seg_9098" s="T118">n-n:case</ta>
            <ta e="T120" id="Seg_9099" s="T119">v-v:n.fin</ta>
            <ta e="T122" id="Seg_9100" s="T121">n-n:num</ta>
            <ta e="T123" id="Seg_9101" s="T122">v-v:tense-v:pn</ta>
            <ta e="T124" id="Seg_9102" s="T123">que-n:case</ta>
            <ta e="T125" id="Seg_9103" s="T124">adv</ta>
            <ta e="T126" id="Seg_9104" s="T125">n-n:case</ta>
            <ta e="T127" id="Seg_9105" s="T126">quant</ta>
            <ta e="T128" id="Seg_9106" s="T127">conj</ta>
            <ta e="T129" id="Seg_9107" s="T128">que-n:case</ta>
            <ta e="T130" id="Seg_9108" s="T129">v-v:pn</ta>
            <ta e="T131" id="Seg_9109" s="T130">dempro-n:case</ta>
            <ta e="T132" id="Seg_9110" s="T131">n-n:case</ta>
            <ta e="T133" id="Seg_9111" s="T132">v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_9112" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_9113" s="T134">n-n:case</ta>
            <ta e="T136" id="Seg_9114" s="T135">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T137" id="Seg_9115" s="T136">adv</ta>
            <ta e="T138" id="Seg_9116" s="T137">que-n:case=ptcl</ta>
            <ta e="T139" id="Seg_9117" s="T138">v-v:pn</ta>
            <ta e="T140" id="Seg_9118" s="T139">v-v:tense-v:pn</ta>
            <ta e="T141" id="Seg_9119" s="T140">v-v:tense-v:pn</ta>
            <ta e="T142" id="Seg_9120" s="T141">ptcl</ta>
            <ta e="T143" id="Seg_9121" s="T142">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T146" id="Seg_9122" s="T145">propr-n:case</ta>
            <ta e="T147" id="Seg_9123" s="T146">adv</ta>
            <ta e="T148" id="Seg_9124" s="T147">quant</ta>
            <ta e="T149" id="Seg_9125" s="T148">n-n:num</ta>
            <ta e="T150" id="Seg_9126" s="T149">v-v:tense-v:pn</ta>
            <ta e="T151" id="Seg_9127" s="T150">adv</ta>
            <ta e="T152" id="Seg_9128" s="T151">n-n:case</ta>
            <ta e="T153" id="Seg_9129" s="T152">n-n:case</ta>
            <ta e="T154" id="Seg_9130" s="T153">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T155" id="Seg_9131" s="T154">dempro-n:num</ta>
            <ta e="T156" id="Seg_9132" s="T155">v-v:tense-v:pn</ta>
            <ta e="T157" id="Seg_9133" s="T156">ptcl</ta>
            <ta e="T159" id="Seg_9134" s="T158">n</ta>
            <ta e="T160" id="Seg_9135" s="T159">n-n:case</ta>
            <ta e="T161" id="Seg_9136" s="T160">v-v:tense-v:pn</ta>
            <ta e="T162" id="Seg_9137" s="T161">pers</ta>
            <ta e="T163" id="Seg_9138" s="T162">ptcl</ta>
            <ta e="T164" id="Seg_9139" s="T163">adj</ta>
            <ta e="T165" id="Seg_9140" s="T164">v-v:tense-v:pn</ta>
            <ta e="T166" id="Seg_9141" s="T165">adv</ta>
            <ta e="T167" id="Seg_9142" s="T166">n-n:num</ta>
            <ta e="T168" id="Seg_9143" s="T167">v-v:tense-v:pn</ta>
            <ta e="T169" id="Seg_9144" s="T168">adv</ta>
            <ta e="T170" id="Seg_9145" s="T169">n-n:num</ta>
            <ta e="T171" id="Seg_9146" s="T170">v-v:tense-v:pn</ta>
            <ta e="T172" id="Seg_9147" s="T171">conj</ta>
            <ta e="T173" id="Seg_9148" s="T172">n-n:case</ta>
            <ta e="T174" id="Seg_9149" s="T173">v-v:tense-v:pn</ta>
            <ta e="T175" id="Seg_9150" s="T174">v-v:tense-v:pn</ta>
            <ta e="T176" id="Seg_9151" s="T175">conj</ta>
            <ta e="T177" id="Seg_9152" s="T176">dempro-n:case</ta>
            <ta e="T178" id="Seg_9153" s="T177">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T179" id="Seg_9154" s="T178">dempro-n:case</ta>
            <ta e="T180" id="Seg_9155" s="T179">dempro-n:num</ta>
            <ta e="T181" id="Seg_9156" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_9157" s="T181">adv</ta>
            <ta e="T183" id="Seg_9158" s="T182">v-v:tense-v:pn</ta>
            <ta e="T891" id="Seg_9159" s="T184">adv</ta>
            <ta e="T185" id="Seg_9160" s="T891">pers</ta>
            <ta e="T186" id="Seg_9161" s="T185">pers</ta>
            <ta e="T187" id="Seg_9162" s="T186">ptcl</ta>
            <ta e="T188" id="Seg_9163" s="T187">v-v:tense-v:pn</ta>
            <ta e="T190" id="Seg_9164" s="T189">n-n:num-n:case.poss</ta>
            <ta e="T192" id="Seg_9165" s="T190">adj-n:case</ta>
            <ta e="T193" id="Seg_9166" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_9167" s="T193">adj-n:num</ta>
            <ta e="T195" id="Seg_9168" s="T194">ptcl</ta>
            <ta e="T196" id="Seg_9169" s="T195">v-v&gt;v-v:pn</ta>
            <ta e="T197" id="Seg_9170" s="T196">n-n:case</ta>
            <ta e="T198" id="Seg_9171" s="T197">v-v&gt;v-v:pn</ta>
            <ta e="T200" id="Seg_9172" s="T199">ptcl</ta>
            <ta e="T201" id="Seg_9173" s="T200">n-n:case</ta>
            <ta e="T202" id="Seg_9174" s="T201">v-v&gt;v-v:pn</ta>
            <ta e="T203" id="Seg_9175" s="T202">adv</ta>
            <ta e="T204" id="Seg_9176" s="T203">n-n:case</ta>
            <ta e="T205" id="Seg_9177" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_9178" s="T205">v-v:tense-v:pn</ta>
            <ta e="T207" id="Seg_9179" s="T206">v-v&gt;v-v:n.fin</ta>
            <ta e="T208" id="Seg_9180" s="T207">dempro-n:num-n:case</ta>
            <ta e="T210" id="Seg_9181" s="T209">num-n:case</ta>
            <ta e="T211" id="Seg_9182" s="T210">n-n:case</ta>
            <ta e="T212" id="Seg_9183" s="T211">num-n:case</ta>
            <ta e="T213" id="Seg_9184" s="T212">n-n:case</ta>
            <ta e="T214" id="Seg_9185" s="T213">v-v:tense-v:pn</ta>
            <ta e="T215" id="Seg_9186" s="T214">num-n:case</ta>
            <ta e="T216" id="Seg_9187" s="T215">adj</ta>
            <ta e="T217" id="Seg_9188" s="T216">conj</ta>
            <ta e="T218" id="Seg_9189" s="T217">num-n:case</ta>
            <ta e="T219" id="Seg_9190" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_9191" s="T219">adj</ta>
            <ta e="T221" id="Seg_9192" s="T220">quant</ta>
            <ta e="T222" id="Seg_9193" s="T221">dempro-n:case</ta>
            <ta e="T223" id="Seg_9194" s="T222">v-v:tense-v:pn</ta>
            <ta e="T224" id="Seg_9195" s="T223">n-n:case</ta>
            <ta e="T225" id="Seg_9196" s="T224">ptcl</ta>
            <ta e="T226" id="Seg_9197" s="T225">conj</ta>
            <ta e="T228" id="Seg_9198" s="T227">n-n:case</ta>
            <ta e="T229" id="Seg_9199" s="T228">conj</ta>
            <ta e="T230" id="Seg_9200" s="T229">n-n:case</ta>
            <ta e="T231" id="Seg_9201" s="T230">ptcl</ta>
            <ta e="T234" id="Seg_9202" s="T233">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T235" id="Seg_9203" s="T234">conj</ta>
            <ta e="T236" id="Seg_9204" s="T235">pers</ta>
            <ta e="T237" id="Seg_9205" s="T236">%%</ta>
            <ta e="T239" id="Seg_9206" s="T237">v-v:tense-v:pn</ta>
            <ta e="T241" id="Seg_9207" s="T240">conj</ta>
            <ta e="T242" id="Seg_9208" s="T241">pers</ta>
            <ta e="T243" id="Seg_9209" s="T242">%%</ta>
            <ta e="T244" id="Seg_9210" s="T243">v-v:tense-v:pn</ta>
            <ta e="T245" id="Seg_9211" s="T244">aux-v:mood.pn</ta>
            <ta e="T246" id="Seg_9212" s="T245">v-v:mood.pn</ta>
            <ta e="T247" id="Seg_9213" s="T246">ptcl</ta>
            <ta e="T248" id="Seg_9214" s="T247">v-v:tense-v:pn</ta>
            <ta e="T249" id="Seg_9215" s="T248">adj-n:case</ta>
            <ta e="T250" id="Seg_9216" s="T249">quant</ta>
            <ta e="T251" id="Seg_9217" s="T250">v-v:tense-v:pn</ta>
            <ta e="T252" id="Seg_9218" s="T251">adj-n:case</ta>
            <ta e="T253" id="Seg_9219" s="T252">v-v:tense-v:pn</ta>
            <ta e="T254" id="Seg_9220" s="T253">ptcl</ta>
            <ta e="T255" id="Seg_9221" s="T254">ptcl</ta>
            <ta e="T256" id="Seg_9222" s="T255">v-v:tense-v:pn</ta>
            <ta e="T257" id="Seg_9223" s="T256">adv</ta>
            <ta e="T258" id="Seg_9224" s="T257">adj</ta>
            <ta e="T259" id="Seg_9225" s="T258">v-v:tense-v:pn</ta>
            <ta e="T261" id="Seg_9226" s="T260">conj</ta>
            <ta e="T262" id="Seg_9227" s="T261">dempro-n:case</ta>
            <ta e="T263" id="Seg_9228" s="T262">que</ta>
            <ta e="T264" id="Seg_9229" s="T263">adj-n:case</ta>
            <ta e="T265" id="Seg_9230" s="T264">v-v:tense-v:pn</ta>
            <ta e="T266" id="Seg_9231" s="T265">conj</ta>
            <ta e="T267" id="Seg_9232" s="T266">adj-n:case</ta>
            <ta e="T269" id="Seg_9233" s="T268">adj-n:case</ta>
            <ta e="T270" id="Seg_9234" s="T269">ptcl</ta>
            <ta e="T271" id="Seg_9235" s="T270">que-n:case=ptcl</ta>
            <ta e="T272" id="Seg_9236" s="T271">v-v:pn</ta>
            <ta e="T273" id="Seg_9237" s="T272">adj</ta>
            <ta e="T275" id="Seg_9238" s="T274">dempro-n:case</ta>
            <ta e="T276" id="Seg_9239" s="T275">n-n:case</ta>
            <ta e="T277" id="Seg_9240" s="T276">v-v:tense-v:pn</ta>
            <ta e="T278" id="Seg_9241" s="T277">adv</ta>
            <ta e="T279" id="Seg_9242" s="T278">v-v:tense-v:pn</ta>
            <ta e="T280" id="Seg_9243" s="T279">que</ta>
            <ta e="T281" id="Seg_9244" s="T280">v-v:n.fin</ta>
            <ta e="T282" id="Seg_9245" s="T281">v-v:tense-v:pn</ta>
            <ta e="T283" id="Seg_9246" s="T282">dempro-n:case</ta>
            <ta e="T284" id="Seg_9247" s="T283">ptcl</ta>
            <ta e="T285" id="Seg_9248" s="T284">v-v&gt;v-v:pn</ta>
            <ta e="T287" id="Seg_9249" s="T286">num-n:case</ta>
            <ta e="T288" id="Seg_9250" s="T287">n-n:case</ta>
            <ta e="T289" id="Seg_9251" s="T288">n-n:case.poss</ta>
            <ta e="T290" id="Seg_9252" s="T289">v-v:tense-v:pn</ta>
            <ta e="T291" id="Seg_9253" s="T290">ptcl</ta>
            <ta e="T292" id="Seg_9254" s="T291">adj-n:case</ta>
            <ta e="T293" id="Seg_9255" s="T292">pers</ta>
            <ta e="T294" id="Seg_9256" s="T293">que-n:case</ta>
            <ta e="T295" id="Seg_9257" s="T294">pers</ta>
            <ta e="T296" id="Seg_9258" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_9259" s="T296">v-v:pn-v:tense-v:pn</ta>
            <ta e="T298" id="Seg_9260" s="T297">conj</ta>
            <ta e="T299" id="Seg_9261" s="T298">dempro-n:case</ta>
            <ta e="T300" id="Seg_9262" s="T299">v-v:tense-v:pn</ta>
            <ta e="T301" id="Seg_9263" s="T300">pers</ta>
            <ta e="T302" id="Seg_9264" s="T301">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T303" id="Seg_9265" s="T302">pers</ta>
            <ta e="T304" id="Seg_9266" s="T303">que-n:case</ta>
            <ta e="T305" id="Seg_9267" s="T304">adv</ta>
            <ta e="T306" id="Seg_9268" s="T305">pers</ta>
            <ta e="T307" id="Seg_9269" s="T306">pers</ta>
            <ta e="T308" id="Seg_9270" s="T307">pers</ta>
            <ta e="T310" id="Seg_9271" s="T309">v-v:tense-v:pn</ta>
            <ta e="T311" id="Seg_9272" s="T310">conj</ta>
            <ta e="T312" id="Seg_9273" s="T311">dempro-n:case</ta>
            <ta e="T313" id="Seg_9274" s="T312">ptcl</ta>
            <ta e="T315" id="Seg_9275" s="T314">n-n:case</ta>
            <ta e="T316" id="Seg_9276" s="T315">v-v:tense-v:pn</ta>
            <ta e="T317" id="Seg_9277" s="T316">conj</ta>
            <ta e="T318" id="Seg_9278" s="T317">dempro-n:case</ta>
            <ta e="T319" id="Seg_9279" s="T318">ptcl</ta>
            <ta e="T321" id="Seg_9280" s="T319">n-n:case=ptcl</ta>
            <ta e="T322" id="Seg_9281" s="T321">n-n:case.poss</ta>
            <ta e="T323" id="Seg_9282" s="T322">v</ta>
            <ta e="T324" id="Seg_9283" s="T323">v-v:n.fin</ta>
            <ta e="T325" id="Seg_9284" s="T324">conj</ta>
            <ta e="T326" id="Seg_9285" s="T325">n-n:case</ta>
            <ta e="T327" id="Seg_9286" s="T326">ptcl</ta>
            <ta e="T328" id="Seg_9287" s="T327">v-v:tense-v:pn</ta>
            <ta e="T330" id="Seg_9288" s="T329">pers</ta>
            <ta e="T331" id="Seg_9289" s="T330">n-n:case.poss</ta>
            <ta e="T332" id="Seg_9290" s="T331">n-n:case</ta>
            <ta e="T333" id="Seg_9291" s="T332">v-v:tense-v:pn</ta>
            <ta e="T334" id="Seg_9292" s="T333">v-v:tense-v:pn</ta>
            <ta e="T335" id="Seg_9293" s="T334">conj</ta>
            <ta e="T336" id="Seg_9294" s="T335">dempro-n:case</ta>
            <ta e="T337" id="Seg_9295" s="T336">v-v:tense-v:pn</ta>
            <ta e="T338" id="Seg_9296" s="T337">n-n:case</ta>
            <ta e="T339" id="Seg_9297" s="T338">conj</ta>
            <ta e="T340" id="Seg_9298" s="T339">adj</ta>
            <ta e="T341" id="Seg_9299" s="T340">n-n:case.poss</ta>
            <ta e="T342" id="Seg_9300" s="T341">n-n:case.poss-n:case</ta>
            <ta e="T344" id="Seg_9301" s="T343">v-v:n.fin</ta>
            <ta e="T345" id="Seg_9302" s="T344">v</ta>
            <ta e="T346" id="Seg_9303" s="T345">conj</ta>
            <ta e="T347" id="Seg_9304" s="T346">dempro-n:case</ta>
            <ta e="T348" id="Seg_9305" s="T347">ptcl</ta>
            <ta e="T349" id="Seg_9306" s="T348">v-v:tense-v:pn</ta>
            <ta e="T350" id="Seg_9307" s="T349">num-n:case</ta>
            <ta e="T351" id="Seg_9308" s="T350">n</ta>
            <ta e="T353" id="Seg_9309" s="T352">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T355" id="Seg_9310" s="T354">dempro-n:case</ta>
            <ta e="T357" id="Seg_9311" s="T356">pers</ta>
            <ta e="T358" id="Seg_9312" s="T357">%%</ta>
            <ta e="T359" id="Seg_9313" s="T358">n-n:case.poss</ta>
            <ta e="T360" id="Seg_9314" s="T359">num-n:case</ta>
            <ta e="T361" id="Seg_9315" s="T360">n-n:case</ta>
            <ta e="T362" id="Seg_9316" s="T361">conj</ta>
            <ta e="T363" id="Seg_9317" s="T362">num-n:case</ta>
            <ta e="T364" id="Seg_9318" s="T363">n-n:case</ta>
            <ta e="T366" id="Seg_9319" s="T365">v-v:n.fin</ta>
            <ta e="T367" id="Seg_9320" s="T366">pers</ta>
            <ta e="T368" id="Seg_9321" s="T367">adj-n:case</ta>
            <ta e="T369" id="Seg_9322" s="T368">adj-n:case</ta>
            <ta e="T370" id="Seg_9323" s="T369">n-n:case</ta>
            <ta e="T372" id="Seg_9324" s="T371">pers</ta>
            <ta e="T373" id="Seg_9325" s="T372">v-v:mood.pn</ta>
            <ta e="T374" id="Seg_9326" s="T373">pers</ta>
            <ta e="T375" id="Seg_9327" s="T374">v-v:mood.pn</ta>
            <ta e="T376" id="Seg_9328" s="T375">v-v:n.fin</ta>
            <ta e="T377" id="Seg_9329" s="T376">pers</ta>
            <ta e="T378" id="Seg_9330" s="T377">adj-n:case</ta>
            <ta e="T379" id="Seg_9331" s="T378">n-n:case</ta>
            <ta e="T381" id="Seg_9332" s="T380">pers</ta>
            <ta e="T382" id="Seg_9333" s="T381">v-v:mood.pn</ta>
            <ta e="T383" id="Seg_9334" s="T382">pers</ta>
            <ta e="T384" id="Seg_9335" s="T383">v-v:mood.pn</ta>
            <ta e="T385" id="Seg_9336" s="T384">v-v:n.fin</ta>
            <ta e="T386" id="Seg_9337" s="T385">pers</ta>
            <ta e="T387" id="Seg_9338" s="T386">adj-n:case</ta>
            <ta e="T388" id="Seg_9339" s="T387">n-n:case</ta>
            <ta e="T389" id="Seg_9340" s="T388">propr-n:case</ta>
            <ta e="T390" id="Seg_9341" s="T389">propr-n:case</ta>
            <ta e="T391" id="Seg_9342" s="T390">adj-n:case</ta>
            <ta e="T392" id="Seg_9343" s="T391">n-n:case</ta>
            <ta e="T393" id="Seg_9344" s="T392">v-v:tense-v:pn</ta>
            <ta e="T394" id="Seg_9345" s="T393">adv</ta>
            <ta e="T395" id="Seg_9346" s="T394">n-n:case</ta>
            <ta e="T396" id="Seg_9347" s="T395">v-v:tense-v:pn</ta>
            <ta e="T397" id="Seg_9348" s="T396">dempro-n:case</ta>
            <ta e="T398" id="Seg_9349" s="T397">n-n:case</ta>
            <ta e="T399" id="Seg_9350" s="T398">v-v:tense-v:pn</ta>
            <ta e="T400" id="Seg_9351" s="T399">n-n:case</ta>
            <ta e="T401" id="Seg_9352" s="T400">v-v:pn</ta>
            <ta e="T402" id="Seg_9353" s="T401">adv</ta>
            <ta e="T403" id="Seg_9354" s="T402">v-v:tense-v:pn</ta>
            <ta e="T404" id="Seg_9355" s="T403">v-v:tense-v:pn</ta>
            <ta e="T405" id="Seg_9356" s="T404">ptcl</ta>
            <ta e="T406" id="Seg_9357" s="T405">adv</ta>
            <ta e="T407" id="Seg_9358" s="T406">num-n:case</ta>
            <ta e="T408" id="Seg_9359" s="T407">n-n:case</ta>
            <ta e="T409" id="Seg_9360" s="T408">v-v:tense-v:pn</ta>
            <ta e="T410" id="Seg_9361" s="T409">n-n:case</ta>
            <ta e="T411" id="Seg_9362" s="T410">dempro-n:case</ta>
            <ta e="T412" id="Seg_9363" s="T411">v-v:tense-v:pn</ta>
            <ta e="T413" id="Seg_9364" s="T412">dempro-n:num</ta>
            <ta e="T414" id="Seg_9365" s="T413">dempro-n:num-n:case</ta>
            <ta e="T415" id="Seg_9366" s="T414">n-n:case</ta>
            <ta e="T416" id="Seg_9367" s="T415">v-v:tense-v:pn</ta>
            <ta e="T417" id="Seg_9368" s="T416">num-n:case</ta>
            <ta e="T418" id="Seg_9369" s="T417">dempro-n:case</ta>
            <ta e="T419" id="Seg_9370" s="T418">v-v&gt;v-v:pn</ta>
            <ta e="T420" id="Seg_9371" s="T419">ptcl</ta>
            <ta e="T421" id="Seg_9372" s="T420">adj-n:case</ta>
            <ta e="T422" id="Seg_9373" s="T421">n-n:case</ta>
            <ta e="T423" id="Seg_9374" s="T422">v-v:n.fin</ta>
            <ta e="T424" id="Seg_9375" s="T423">n-n:case</ta>
            <ta e="T426" id="Seg_9376" s="T425">v-v:n.fin</ta>
            <ta e="T427" id="Seg_9377" s="T426">conj</ta>
            <ta e="T428" id="Seg_9378" s="T427">dempro-n:case</ta>
            <ta e="T429" id="Seg_9379" s="T428">v-v&gt;v-v:pn</ta>
            <ta e="T430" id="Seg_9380" s="T429">aux-v:mood.pn</ta>
            <ta e="T431" id="Seg_9381" s="T430">v-v:ins-v:mood.pn</ta>
            <ta e="T432" id="Seg_9382" s="T431">pers</ta>
            <ta e="T433" id="Seg_9383" s="T432">n-n:case.poss</ta>
            <ta e="T434" id="Seg_9384" s="T433">v-v:tense-v:pn</ta>
            <ta e="T435" id="Seg_9385" s="T434">adv</ta>
            <ta e="T436" id="Seg_9386" s="T435">n-n:case</ta>
            <ta e="T437" id="Seg_9387" s="T436">v-v:tense-v:pn</ta>
            <ta e="T438" id="Seg_9388" s="T437">n-n:case</ta>
            <ta e="T439" id="Seg_9389" s="T438">v-v:tense-v:pn</ta>
            <ta e="T440" id="Seg_9390" s="T439">dempro-n:case</ta>
            <ta e="T442" id="Seg_9391" s="T441">v-v:tense-v:pn</ta>
            <ta e="T443" id="Seg_9392" s="T442">n</ta>
            <ta e="T444" id="Seg_9393" s="T443">n-n:case.poss</ta>
            <ta e="T445" id="Seg_9394" s="T444">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T446" id="Seg_9395" s="T445">n-n:case</ta>
            <ta e="T447" id="Seg_9396" s="T446">n-n:case</ta>
            <ta e="T449" id="Seg_9397" s="T447">n-n:case</ta>
            <ta e="T450" id="Seg_9398" s="T449">adv</ta>
            <ta e="T451" id="Seg_9399" s="T450">n-n:case.poss</ta>
            <ta e="T452" id="Seg_9400" s="T451">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T453" id="Seg_9401" s="T452">v-v:tense-v:pn</ta>
            <ta e="T454" id="Seg_9402" s="T453">v-v:tense-v:pn</ta>
            <ta e="T455" id="Seg_9403" s="T454">adv</ta>
            <ta e="T456" id="Seg_9404" s="T455">n-n:case</ta>
            <ta e="T457" id="Seg_9405" s="T456">v-v:pn</ta>
            <ta e="T458" id="Seg_9406" s="T457">num-n:case</ta>
            <ta e="T459" id="Seg_9407" s="T458">n-n:case</ta>
            <ta e="T460" id="Seg_9408" s="T459">dempro-n:case</ta>
            <ta e="T461" id="Seg_9409" s="T460">ptcl</ta>
            <ta e="T463" id="Seg_9410" s="T462">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T464" id="Seg_9411" s="T463">ptcl</ta>
            <ta e="T465" id="Seg_9412" s="T464">v-v:n.fin</ta>
            <ta e="T466" id="Seg_9413" s="T465">conj</ta>
            <ta e="T467" id="Seg_9414" s="T466">dempro-n:num</ta>
            <ta e="T470" id="Seg_9415" s="T469">v-v:tense-v:pn</ta>
            <ta e="T471" id="Seg_9416" s="T470">adv</ta>
            <ta e="T472" id="Seg_9417" s="T471">n-n:case</ta>
            <ta e="T473" id="Seg_9418" s="T472">v-v:tense-v:pn</ta>
            <ta e="T474" id="Seg_9419" s="T473">pers</ta>
            <ta e="T475" id="Seg_9420" s="T474">n-n:case</ta>
            <ta e="T476" id="Seg_9421" s="T475">v-v:tense-v:pn</ta>
            <ta e="T477" id="Seg_9422" s="T476">pers</ta>
            <ta e="T478" id="Seg_9423" s="T477">ptcl</ta>
            <ta e="T479" id="Seg_9424" s="T478">adv</ta>
            <ta e="T480" id="Seg_9425" s="T479">n-n:case</ta>
            <ta e="T481" id="Seg_9426" s="T480">adv</ta>
            <ta e="T482" id="Seg_9427" s="T481">dempro-n:case</ta>
            <ta e="T483" id="Seg_9428" s="T482">n-n:case</ta>
            <ta e="T484" id="Seg_9429" s="T483">v-v:tense-v:pn</ta>
            <ta e="T486" id="Seg_9430" s="T485">adv</ta>
            <ta e="T487" id="Seg_9431" s="T486">dempro-n:case</ta>
            <ta e="T488" id="Seg_9432" s="T487">propr-n:case</ta>
            <ta e="T490" id="Seg_9433" s="T489">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T491" id="Seg_9434" s="T490">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T492" id="Seg_9435" s="T491">v-v:tense-v:pn</ta>
            <ta e="T493" id="Seg_9436" s="T492">n-n:case.poss</ta>
            <ta e="T494" id="Seg_9437" s="T493">n-n:case.poss</ta>
            <ta e="T495" id="Seg_9438" s="T494">v-v:tense-v:pn</ta>
            <ta e="T496" id="Seg_9439" s="T495">v-v:tense-v:pn</ta>
            <ta e="T497" id="Seg_9440" s="T496">adv</ta>
            <ta e="T499" id="Seg_9441" s="T498">n-n:case</ta>
            <ta e="T500" id="Seg_9442" s="T499">n-n:case</ta>
            <ta e="T501" id="Seg_9443" s="T500">v-v:tense-v:pn</ta>
            <ta e="T502" id="Seg_9444" s="T501">adv</ta>
            <ta e="T503" id="Seg_9445" s="T502">ptcl</ta>
            <ta e="T504" id="Seg_9446" s="T503">num-n:case</ta>
            <ta e="T505" id="Seg_9447" s="T504">n-n:case</ta>
            <ta e="T506" id="Seg_9448" s="T505">n-n:case</ta>
            <ta e="T507" id="Seg_9449" s="T506">v-v:tense-v:pn</ta>
            <ta e="T508" id="Seg_9450" s="T507">conj</ta>
            <ta e="T509" id="Seg_9451" s="T508">n-n:case</ta>
            <ta e="T510" id="Seg_9452" s="T509">n-n:case</ta>
            <ta e="T511" id="Seg_9453" s="T510">v-v:tense-v:pn</ta>
            <ta e="T512" id="Seg_9454" s="T511">adv</ta>
            <ta e="T513" id="Seg_9455" s="T512">v-v:tense-v:pn</ta>
            <ta e="T514" id="Seg_9456" s="T513">v-v:mood.pn</ta>
            <ta e="T515" id="Seg_9457" s="T514">pers</ta>
            <ta e="T516" id="Seg_9458" s="T515">n-n:case</ta>
            <ta e="T517" id="Seg_9459" s="T516">pers</ta>
            <ta e="T518" id="Seg_9460" s="T517">n-n:case</ta>
            <ta e="T519" id="Seg_9461" s="T518">ptcl</ta>
            <ta e="T520" id="Seg_9462" s="T519">ptcl</ta>
            <ta e="T521" id="Seg_9463" s="T520">v-v:tense-v:pn</ta>
            <ta e="T522" id="Seg_9464" s="T521">pers</ta>
            <ta e="T523" id="Seg_9465" s="T522">n-n:case</ta>
            <ta e="T524" id="Seg_9466" s="T523">pers</ta>
            <ta e="T525" id="Seg_9467" s="T524">n-n:case.poss</ta>
            <ta e="T526" id="Seg_9468" s="T525">v-v&gt;v-v:pn</ta>
            <ta e="T527" id="Seg_9469" s="T526">conj</ta>
            <ta e="T528" id="Seg_9470" s="T527">refl-n:case.poss</ta>
            <ta e="T530" id="Seg_9471" s="T529">v-v:tense-v:pn</ta>
            <ta e="T531" id="Seg_9472" s="T530">v-v:tense-v:pn</ta>
            <ta e="T532" id="Seg_9473" s="T531">dempro-n:case</ta>
            <ta e="T534" id="Seg_9474" s="T533">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T535" id="Seg_9475" s="T534">conj</ta>
            <ta e="T536" id="Seg_9476" s="T535">n-n:case.poss</ta>
            <ta e="T537" id="Seg_9477" s="T536">v-v:tense-v:pn</ta>
            <ta e="T538" id="Seg_9478" s="T537">dempro-n:case</ta>
            <ta e="T539" id="Seg_9479" s="T538">n-n:case.poss</ta>
            <ta e="T540" id="Seg_9480" s="T539">conj</ta>
            <ta e="T541" id="Seg_9481" s="T540">adv</ta>
            <ta e="T542" id="Seg_9482" s="T541">v-v:tense-v:pn</ta>
            <ta e="T543" id="Seg_9483" s="T542">v-v:n.fin</ta>
            <ta e="T544" id="Seg_9484" s="T543">n-n:case</ta>
            <ta e="T545" id="Seg_9485" s="T544">v-v:tense-v:pn</ta>
            <ta e="T546" id="Seg_9486" s="T545">conj</ta>
            <ta e="T547" id="Seg_9487" s="T546">n-n:case</ta>
            <ta e="T548" id="Seg_9488" s="T547">v-v:pn</ta>
            <ta e="T550" id="Seg_9489" s="T549">dempro-n:case</ta>
            <ta e="T551" id="Seg_9490" s="T550">propr-n:case</ta>
            <ta e="T552" id="Seg_9491" s="T551">v-v:tense-v:pn</ta>
            <ta e="T553" id="Seg_9492" s="T552">n-n:case.poss</ta>
            <ta e="T554" id="Seg_9493" s="T553">v&gt;v-v-v:tense-v:pn</ta>
            <ta e="T555" id="Seg_9494" s="T554">conj</ta>
            <ta e="T556" id="Seg_9495" s="T555">dempro-n:case</ta>
            <ta e="T557" id="Seg_9496" s="T556">dempro-n:case</ta>
            <ta e="T558" id="Seg_9497" s="T557">n-n:case.poss</ta>
            <ta e="T559" id="Seg_9498" s="T558">ptcl</ta>
            <ta e="T560" id="Seg_9499" s="T559">n-n:case</ta>
            <ta e="T562" id="Seg_9500" s="T561">n-n:case</ta>
            <ta e="T563" id="Seg_9501" s="T562">ptcl</ta>
            <ta e="T564" id="Seg_9502" s="T563">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T565" id="Seg_9503" s="T564">n-n:case.poss</ta>
            <ta e="T566" id="Seg_9504" s="T565">conj</ta>
            <ta e="T567" id="Seg_9505" s="T566">n-n:case.poss</ta>
            <ta e="T568" id="Seg_9506" s="T567">conj</ta>
            <ta e="T569" id="Seg_9507" s="T568">n-n:case</ta>
            <ta e="T570" id="Seg_9508" s="T569">v-v:tense-v:pn</ta>
            <ta e="T571" id="Seg_9509" s="T570">v-v&gt;v-v:pn</ta>
            <ta e="T572" id="Seg_9510" s="T571">adv</ta>
            <ta e="T574" id="Seg_9511" s="T573">pers</ta>
            <ta e="T575" id="Seg_9512" s="T574">n-n:case.poss</ta>
            <ta e="T576" id="Seg_9513" s="T575">dempro-n:case</ta>
            <ta e="T577" id="Seg_9514" s="T576">dempro-n:case</ta>
            <ta e="T578" id="Seg_9515" s="T577">n-n:case.poss</ta>
            <ta e="T579" id="Seg_9516" s="T578">ptcl</ta>
            <ta e="T580" id="Seg_9517" s="T579">v&gt;v-v-v:tense-v:pn</ta>
            <ta e="T582" id="Seg_9518" s="T581">adv</ta>
            <ta e="T583" id="Seg_9519" s="T582">dempro-n:case</ta>
            <ta e="T584" id="Seg_9520" s="T583">v-v:tense-v:pn</ta>
            <ta e="T585" id="Seg_9521" s="T584">n-n:case.poss</ta>
            <ta e="T586" id="Seg_9522" s="T585">v-v:tense-v:pn</ta>
            <ta e="T587" id="Seg_9523" s="T586">v-v:tense-v:pn</ta>
            <ta e="T588" id="Seg_9524" s="T587">num-n:case</ta>
            <ta e="T589" id="Seg_9525" s="T588">n-n:case</ta>
            <ta e="T590" id="Seg_9526" s="T589">v-v:tense-v:pn</ta>
            <ta e="T591" id="Seg_9527" s="T590">conj</ta>
            <ta e="T592" id="Seg_9528" s="T591">v-v:tense-v:pn</ta>
            <ta e="T595" id="Seg_9529" s="T594">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T596" id="Seg_9530" s="T595">adv</ta>
            <ta e="T597" id="Seg_9531" s="T596">v-v:tense-v:pn</ta>
            <ta e="T598" id="Seg_9532" s="T597">v-v:tense-v:pn</ta>
            <ta e="T599" id="Seg_9533" s="T598">adv</ta>
            <ta e="T600" id="Seg_9534" s="T599">ptcl</ta>
            <ta e="T601" id="Seg_9535" s="T600">n-n:case</ta>
            <ta e="T602" id="Seg_9536" s="T601">v-v&gt;v-v:pn</ta>
            <ta e="T603" id="Seg_9537" s="T602">dempro-n:case</ta>
            <ta e="T604" id="Seg_9538" s="T603">v-v:tense-v:pn</ta>
            <ta e="T605" id="Seg_9539" s="T604">adv</ta>
            <ta e="T606" id="Seg_9540" s="T605">n-n:case</ta>
            <ta e="T607" id="Seg_9541" s="T606">v-v&gt;v-v:pn</ta>
            <ta e="T608" id="Seg_9542" s="T607">dempro-n:case</ta>
            <ta e="T609" id="Seg_9543" s="T608">n-n:case</ta>
            <ta e="T610" id="Seg_9544" s="T609">v-v:tense-v:pn</ta>
            <ta e="T611" id="Seg_9545" s="T610">conj</ta>
            <ta e="T612" id="Seg_9546" s="T611">num-n:case</ta>
            <ta e="T613" id="Seg_9547" s="T612">n-n:case</ta>
            <ta e="T614" id="Seg_9548" s="T613">conj</ta>
            <ta e="T615" id="Seg_9549" s="T614">dempro-n:case</ta>
            <ta e="T616" id="Seg_9550" s="T615">n-n:num-n:case.poss</ta>
            <ta e="T617" id="Seg_9551" s="T616">dempro-n:case</ta>
            <ta e="T618" id="Seg_9552" s="T617">n-n:case</ta>
            <ta e="T619" id="Seg_9553" s="T618">n-n:num-n:case</ta>
            <ta e="T620" id="Seg_9554" s="T619">v-v:tense-v:pn</ta>
            <ta e="T621" id="Seg_9555" s="T620">n-n:case.poss</ta>
            <ta e="T622" id="Seg_9556" s="T621">n-n:case.poss</ta>
            <ta e="T623" id="Seg_9557" s="T622">n-n:case</ta>
            <ta e="T625" id="Seg_9558" s="T624">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T626" id="Seg_9559" s="T625">adv</ta>
            <ta e="T627" id="Seg_9560" s="T626">v-v:tense-v:pn</ta>
            <ta e="T629" id="Seg_9561" s="T628">adv</ta>
            <ta e="T630" id="Seg_9562" s="T629">num-n:case</ta>
            <ta e="T631" id="Seg_9563" s="T630">n-n:case</ta>
            <ta e="T632" id="Seg_9564" s="T631">conj</ta>
            <ta e="T633" id="Seg_9565" s="T632">n-n:case</ta>
            <ta e="T634" id="Seg_9566" s="T633">v-v&gt;v-v:pn</ta>
            <ta e="T635" id="Seg_9567" s="T634">v-v:tense-v:pn</ta>
            <ta e="T636" id="Seg_9568" s="T635">v-v:tense-v:pn</ta>
            <ta e="T637" id="Seg_9569" s="T636">pers</ta>
            <ta e="T638" id="Seg_9570" s="T637">adv</ta>
            <ta e="T639" id="Seg_9571" s="T638">n-n:case</ta>
            <ta e="T640" id="Seg_9572" s="T639">v-v&gt;v-v:pn</ta>
            <ta e="T641" id="Seg_9573" s="T640">n-n:case</ta>
            <ta e="T642" id="Seg_9574" s="T641">conj</ta>
            <ta e="T643" id="Seg_9575" s="T642">refl-n:case.poss</ta>
            <ta e="T645" id="Seg_9576" s="T644">n-n:case.poss</ta>
            <ta e="T646" id="Seg_9577" s="T645">n-n:case</ta>
            <ta e="T647" id="Seg_9578" s="T646">v-v&gt;v-v:pn</ta>
            <ta e="T648" id="Seg_9579" s="T647">conj</ta>
            <ta e="T649" id="Seg_9580" s="T648">n-n:num</ta>
            <ta e="T650" id="Seg_9581" s="T649">pers</ta>
            <ta e="T651" id="Seg_9582" s="T650">v-v:tense-v:pn</ta>
            <ta e="T652" id="Seg_9583" s="T651">v-v:tense-v:pn</ta>
            <ta e="T653" id="Seg_9584" s="T652">n-n:case.poss</ta>
            <ta e="T654" id="Seg_9585" s="T653">pers</ta>
            <ta e="T655" id="Seg_9586" s="T654">dempro-v:pn</ta>
            <ta e="T656" id="Seg_9587" s="T655">adv</ta>
            <ta e="T657" id="Seg_9588" s="T656">v-v:tense-v:pn</ta>
            <ta e="T658" id="Seg_9589" s="T657">ptcl</ta>
            <ta e="T659" id="Seg_9590" s="T658">v-v:tense-v:pn</ta>
            <ta e="T660" id="Seg_9591" s="T659">refl-n:case.poss</ta>
            <ta e="T661" id="Seg_9592" s="T660">refl-n:case.poss</ta>
            <ta e="T663" id="Seg_9593" s="T662">adv</ta>
            <ta e="T664" id="Seg_9594" s="T663">dempro-n:num</ta>
            <ta e="T665" id="Seg_9595" s="T664">n-n:num</ta>
            <ta e="T666" id="Seg_9596" s="T665">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T667" id="Seg_9597" s="T666">v-v:tense-v:pn</ta>
            <ta e="T668" id="Seg_9598" s="T667">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T669" id="Seg_9599" s="T668">n-n:case.poss</ta>
            <ta e="T670" id="Seg_9600" s="T669">ptcl</ta>
            <ta e="T672" id="Seg_9601" s="T671">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T673" id="Seg_9602" s="T672">n-n:case.poss</ta>
            <ta e="T674" id="Seg_9603" s="T673">adv</ta>
            <ta e="T675" id="Seg_9604" s="T674">dempro-n:case</ta>
            <ta e="T676" id="Seg_9605" s="T675">ptcl</ta>
            <ta e="T677" id="Seg_9606" s="T676">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T678" id="Seg_9607" s="T677">propr-n:case</ta>
            <ta e="T679" id="Seg_9608" s="T678">v-v:tense-v:pn</ta>
            <ta e="T680" id="Seg_9609" s="T679">v-v:tense-v:pn</ta>
            <ta e="T681" id="Seg_9610" s="T680">dempro-n:case</ta>
            <ta e="T682" id="Seg_9611" s="T681">n-n:case</ta>
            <ta e="T683" id="Seg_9612" s="T682">v-v&gt;v-v:pn</ta>
            <ta e="T684" id="Seg_9613" s="T683">v-v:mood.pn</ta>
            <ta e="T685" id="Seg_9614" s="T684">num-n:case</ta>
            <ta e="T686" id="Seg_9615" s="T685">n-n:case</ta>
            <ta e="T687" id="Seg_9616" s="T686">adv</ta>
            <ta e="T688" id="Seg_9617" s="T687">pers</ta>
            <ta e="T689" id="Seg_9618" s="T688">num-n:case</ta>
            <ta e="T690" id="Seg_9619" s="T689">pers</ta>
            <ta e="T691" id="Seg_9620" s="T690">n-n:case.poss</ta>
            <ta e="T692" id="Seg_9621" s="T691">adv</ta>
            <ta e="T693" id="Seg_9622" s="T692">v-v:tense-v:pn</ta>
            <ta e="T694" id="Seg_9623" s="T693">ptcl</ta>
            <ta e="T695" id="Seg_9624" s="T694">v-v:mood.pn</ta>
            <ta e="T696" id="Seg_9625" s="T695">num-n:case</ta>
            <ta e="T697" id="Seg_9626" s="T696">adv</ta>
            <ta e="T698" id="Seg_9627" s="T697">dempro-n:case</ta>
            <ta e="T699" id="Seg_9628" s="T698">v-v:tense-v:pn</ta>
            <ta e="T700" id="Seg_9629" s="T699">adv</ta>
            <ta e="T701" id="Seg_9630" s="T700">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T702" id="Seg_9631" s="T701">n-n:case.poss</ta>
            <ta e="T703" id="Seg_9632" s="T702">conj</ta>
            <ta e="T704" id="Seg_9633" s="T703">num-n:case</ta>
            <ta e="T705" id="Seg_9634" s="T704">n-n:case</ta>
            <ta e="T706" id="Seg_9635" s="T705">v-v:tense-v:pn</ta>
            <ta e="T707" id="Seg_9636" s="T706">conj</ta>
            <ta e="T708" id="Seg_9637" s="T707">v-v:tense-v:pn</ta>
            <ta e="T709" id="Seg_9638" s="T708">adv</ta>
            <ta e="T710" id="Seg_9639" s="T709">v-v:tense-v:pn</ta>
            <ta e="T711" id="Seg_9640" s="T710">v-v:tense-v:pn</ta>
            <ta e="T712" id="Seg_9641" s="T711">adv</ta>
            <ta e="T713" id="Seg_9642" s="T712">n-n:num</ta>
            <ta e="T714" id="Seg_9643" s="T713">v-v&gt;v-v:pn</ta>
            <ta e="T715" id="Seg_9644" s="T714">adv</ta>
            <ta e="T716" id="Seg_9645" s="T715">v-v:tense-v:pn</ta>
            <ta e="T717" id="Seg_9646" s="T716">n-n:case</ta>
            <ta e="T718" id="Seg_9647" s="T717">adv</ta>
            <ta e="T719" id="Seg_9648" s="T718">pers</ta>
            <ta e="T720" id="Seg_9649" s="T719">n-n:case</ta>
            <ta e="T721" id="Seg_9650" s="T720">conj</ta>
            <ta e="T723" id="Seg_9651" s="T722">num-n:case</ta>
            <ta e="T725" id="Seg_9652" s="T724">v-v:tense-v:pn</ta>
            <ta e="T726" id="Seg_9653" s="T725">n-n:case</ta>
            <ta e="T728" id="Seg_9654" s="T727">n-n:case.poss</ta>
            <ta e="T729" id="Seg_9655" s="T728">num-n:case</ta>
            <ta e="T730" id="Seg_9656" s="T729">v-%%-v:tense-v:pn</ta>
            <ta e="T731" id="Seg_9657" s="T730">n-n:case</ta>
            <ta e="T732" id="Seg_9658" s="T731">adv</ta>
            <ta e="T733" id="Seg_9659" s="T732">v-v&gt;v-v:pn</ta>
            <ta e="T734" id="Seg_9660" s="T733">pers</ta>
            <ta e="T735" id="Seg_9661" s="T734">n-n:case</ta>
            <ta e="T736" id="Seg_9662" s="T735">ptcl</ta>
            <ta e="T737" id="Seg_9663" s="T736">v-v:mood.pn</ta>
            <ta e="T740" id="Seg_9664" s="T739">v-v:mood.pn</ta>
            <ta e="T741" id="Seg_9665" s="T740">dempro-n:case</ta>
            <ta e="T742" id="Seg_9666" s="T741">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T743" id="Seg_9667" s="T742">propr-n:ins-n:case</ta>
            <ta e="T744" id="Seg_9668" s="T743">n-n:num-n:case.poss</ta>
            <ta e="T745" id="Seg_9669" s="T744">adv</ta>
            <ta e="T746" id="Seg_9670" s="T745">n-n:case</ta>
            <ta e="T747" id="Seg_9671" s="T746">n-n:case</ta>
            <ta e="T748" id="Seg_9672" s="T747">v-v:mood.pn</ta>
            <ta e="T749" id="Seg_9673" s="T748">v-v:tense-v:pn</ta>
            <ta e="T750" id="Seg_9674" s="T749">dempro-n:case</ta>
            <ta e="T751" id="Seg_9675" s="T750">v-v&gt;v-v:pn</ta>
            <ta e="T752" id="Seg_9676" s="T751">propr-n:ins-n:case</ta>
            <ta e="T753" id="Seg_9677" s="T752">n-n:case.poss</ta>
            <ta e="T754" id="Seg_9678" s="T753">adv</ta>
            <ta e="T756" id="Seg_9679" s="T755">v-v:tense-v:pn</ta>
            <ta e="T757" id="Seg_9680" s="T756">n-n:case.poss</ta>
            <ta e="T759" id="Seg_9681" s="T758">adv</ta>
            <ta e="T761" id="Seg_9682" s="T760">v-v:tense-v:pn</ta>
            <ta e="T762" id="Seg_9683" s="T761">n-n:case</ta>
            <ta e="T763" id="Seg_9684" s="T762">n-n:num</ta>
            <ta e="T765" id="Seg_9685" s="T764">v-v&gt;v-v:pn</ta>
            <ta e="T766" id="Seg_9686" s="T765">pers</ta>
            <ta e="T767" id="Seg_9687" s="T766">n-n:num</ta>
            <ta e="T768" id="Seg_9688" s="T767">conj</ta>
            <ta e="T769" id="Seg_9689" s="T768">dempro-n:case</ta>
            <ta e="T770" id="Seg_9690" s="T769">v-v&gt;v-v:pn</ta>
            <ta e="T771" id="Seg_9691" s="T770">ptcl</ta>
            <ta e="T772" id="Seg_9692" s="T771">pers</ta>
            <ta e="T773" id="Seg_9693" s="T772">n-n:num</ta>
            <ta e="T774" id="Seg_9694" s="T773">ptcl</ta>
            <ta e="T775" id="Seg_9695" s="T774">v-v:mood.pn</ta>
            <ta e="T776" id="Seg_9696" s="T775">n-n:case</ta>
            <ta e="T777" id="Seg_9697" s="T776">n-n:case</ta>
            <ta e="T778" id="Seg_9698" s="T777">dempro-n:case</ta>
            <ta e="T779" id="Seg_9699" s="T778">v-v:tense-v:pn</ta>
            <ta e="T780" id="Seg_9700" s="T779">propr-n:ins-n:case</ta>
            <ta e="T781" id="Seg_9701" s="T780">n-n:num</ta>
            <ta e="T782" id="Seg_9702" s="T781">adv</ta>
            <ta e="T783" id="Seg_9703" s="T782">n-n:case</ta>
            <ta e="T784" id="Seg_9704" s="T783">n-n:case</ta>
            <ta e="T785" id="Seg_9705" s="T784">v-v:mood.pn</ta>
            <ta e="T786" id="Seg_9706" s="T785">dempro-n:case</ta>
            <ta e="T787" id="Seg_9707" s="T786">v-v:tense-v:pn</ta>
            <ta e="T788" id="Seg_9708" s="T787">propr-n:ins-n:case</ta>
            <ta e="T789" id="Seg_9709" s="T788">n-n:num</ta>
            <ta e="T790" id="Seg_9710" s="T789">dempro-n:case</ta>
            <ta e="T791" id="Seg_9711" s="T790">ptcl</ta>
            <ta e="T792" id="Seg_9712" s="T791">v-v:tense-v:pn</ta>
            <ta e="T793" id="Seg_9713" s="T792">adv</ta>
            <ta e="T794" id="Seg_9714" s="T793">n-n:num</ta>
            <ta e="T795" id="Seg_9715" s="T794">v-v&gt;v-v:pn</ta>
            <ta e="T796" id="Seg_9716" s="T795">conj</ta>
            <ta e="T797" id="Seg_9717" s="T796">adv</ta>
            <ta e="T798" id="Seg_9718" s="T797">ptcl</ta>
            <ta e="T799" id="Seg_9719" s="T798">n-n:case</ta>
            <ta e="T800" id="Seg_9720" s="T799">v-v&gt;v-v:pn</ta>
            <ta e="T801" id="Seg_9721" s="T800">n-n:num</ta>
            <ta e="T802" id="Seg_9722" s="T801">dempro-n:case</ta>
            <ta e="T803" id="Seg_9723" s="T802">v-v:tense-v:pn</ta>
            <ta e="T804" id="Seg_9724" s="T803">pers</ta>
            <ta e="T805" id="Seg_9725" s="T804">n-n:num</ta>
            <ta e="T806" id="Seg_9726" s="T805">ptcl</ta>
            <ta e="T807" id="Seg_9727" s="T806">ptcl</ta>
            <ta e="T808" id="Seg_9728" s="T807">pers</ta>
            <ta e="T809" id="Seg_9729" s="T808">ptcl</ta>
            <ta e="T810" id="Seg_9730" s="T809">v-v:mood.pn</ta>
            <ta e="T811" id="Seg_9731" s="T810">n-n:case</ta>
            <ta e="T812" id="Seg_9732" s="T811">n-n:case</ta>
            <ta e="T813" id="Seg_9733" s="T812">adv</ta>
            <ta e="T814" id="Seg_9734" s="T813">n-n:case</ta>
            <ta e="T815" id="Seg_9735" s="T814">dempro-n:case</ta>
            <ta e="T816" id="Seg_9736" s="T815">v-v:tense-v:pn</ta>
            <ta e="T817" id="Seg_9737" s="T816">que-n:case</ta>
            <ta e="T818" id="Seg_9738" s="T817">n-n:case.poss</ta>
            <ta e="T819" id="Seg_9739" s="T818">dempro</ta>
            <ta e="T820" id="Seg_9740" s="T819">dempro</ta>
            <ta e="T821" id="Seg_9741" s="T820">v-v:tense-v:pn</ta>
            <ta e="T822" id="Seg_9742" s="T821">propr-n:case</ta>
            <ta e="T823" id="Seg_9743" s="T822">n-n:case.poss</ta>
            <ta e="T824" id="Seg_9744" s="T823">conj</ta>
            <ta e="T825" id="Seg_9745" s="T824">n-n:case</ta>
            <ta e="T826" id="Seg_9746" s="T825">v-v:tense-v:pn</ta>
            <ta e="T827" id="Seg_9747" s="T826">que-n:case</ta>
            <ta e="T828" id="Seg_9748" s="T827">n-n:case.poss</ta>
            <ta e="T829" id="Seg_9749" s="T828">propr-n:ins-n:case</ta>
            <ta e="T830" id="Seg_9750" s="T829">n-n:case.poss</ta>
            <ta e="T831" id="Seg_9751" s="T830">adv</ta>
            <ta e="T832" id="Seg_9752" s="T831">dempro-n:case</ta>
            <ta e="T833" id="Seg_9753" s="T832">ptcl</ta>
            <ta e="T834" id="Seg_9754" s="T833">v-v:n.fin</ta>
            <ta e="T835" id="Seg_9755" s="T834">n-n:case</ta>
            <ta e="T836" id="Seg_9756" s="T835">pers</ta>
            <ta e="T837" id="Seg_9757" s="T836">n-n:case</ta>
            <ta e="T840" id="Seg_9758" s="T839">v-v:tense-v:pn</ta>
            <ta e="T842" id="Seg_9759" s="T841">n-n:case</ta>
            <ta e="T843" id="Seg_9760" s="T842">v-v:tense-v:pn</ta>
            <ta e="T844" id="Seg_9761" s="T843">n-n:case.poss</ta>
            <ta e="T845" id="Seg_9762" s="T844">num-n:case</ta>
            <ta e="T846" id="Seg_9763" s="T845">n-n:case</ta>
            <ta e="T847" id="Seg_9764" s="T846">v-v:tense-v:pn</ta>
            <ta e="T848" id="Seg_9765" s="T847">num-n:case</ta>
            <ta e="T849" id="Seg_9766" s="T848">n-n:case</ta>
            <ta e="T850" id="Seg_9767" s="T849">v-v:tense-v:pn</ta>
            <ta e="T851" id="Seg_9768" s="T850">v-v:tense-v:pn</ta>
            <ta e="T852" id="Seg_9769" s="T851">n-n:case</ta>
            <ta e="T853" id="Seg_9770" s="T852">n-n:case</ta>
            <ta e="T854" id="Seg_9771" s="T853">v-v:tense-v:pn</ta>
            <ta e="T855" id="Seg_9772" s="T854">conj</ta>
            <ta e="T856" id="Seg_9773" s="T855">num-n:case</ta>
            <ta e="T857" id="Seg_9774" s="T856">n-n:case</ta>
            <ta e="T858" id="Seg_9775" s="T857">conj</ta>
            <ta e="T859" id="Seg_9776" s="T858">n-n:case</ta>
            <ta e="T860" id="Seg_9777" s="T859">n-n:case</ta>
            <ta e="T863" id="Seg_9778" s="T862">n-n:case.poss</ta>
            <ta e="T865" id="Seg_9779" s="T864">n-n:case</ta>
            <ta e="T866" id="Seg_9780" s="T865">n-n:case.poss</ta>
            <ta e="T868" id="Seg_9781" s="T867">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T869" id="Seg_9782" s="T868">conj</ta>
            <ta e="T870" id="Seg_9783" s="T869">refl-n:case.poss</ta>
            <ta e="T871" id="Seg_9784" s="T870">v-v:tense-v:pn</ta>
            <ta e="T872" id="Seg_9785" s="T871">pers</ta>
            <ta e="T873" id="Seg_9786" s="T872">adv</ta>
            <ta e="T874" id="Seg_9787" s="T873">n-n:case.poss</ta>
            <ta e="T875" id="Seg_9788" s="T874">v-v:tense-v:pn</ta>
            <ta e="T876" id="Seg_9789" s="T875">conj</ta>
            <ta e="T877" id="Seg_9790" s="T876">num-n:case</ta>
            <ta e="T878" id="Seg_9791" s="T877">n-n:case</ta>
            <ta e="T879" id="Seg_9792" s="T878">conj</ta>
            <ta e="T880" id="Seg_9793" s="T879">num-n:case</ta>
            <ta e="T881" id="Seg_9794" s="T880">n-n:num</ta>
            <ta e="T882" id="Seg_9795" s="T881">conj</ta>
            <ta e="T883" id="Seg_9796" s="T882">n-n:case</ta>
            <ta e="T884" id="Seg_9797" s="T883">v-v&gt;v-v:pn</ta>
            <ta e="T885" id="Seg_9798" s="T884">dempro-n:case</ta>
            <ta e="T886" id="Seg_9799" s="T885">ptcl</ta>
            <ta e="T887" id="Seg_9800" s="T886">n-n:case</ta>
            <ta e="T888" id="Seg_9801" s="T887">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T3" id="Seg_9802" s="T2">v</ta>
            <ta e="T4" id="Seg_9803" s="T3">pers</ta>
            <ta e="T5" id="Seg_9804" s="T4">dempro</ta>
            <ta e="T6" id="Seg_9805" s="T5">v</ta>
            <ta e="T7" id="Seg_9806" s="T6">n</ta>
            <ta e="T8" id="Seg_9807" s="T7">n</ta>
            <ta e="T9" id="Seg_9808" s="T8">v</ta>
            <ta e="T10" id="Seg_9809" s="T9">n</ta>
            <ta e="T11" id="Seg_9810" s="T10">v</ta>
            <ta e="T12" id="Seg_9811" s="T11">num</ta>
            <ta e="T13" id="Seg_9812" s="T12">num</ta>
            <ta e="T14" id="Seg_9813" s="T13">num</ta>
            <ta e="T15" id="Seg_9814" s="T14">num</ta>
            <ta e="T16" id="Seg_9815" s="T15">dempro</ta>
            <ta e="T17" id="Seg_9816" s="T16">n</ta>
            <ta e="T18" id="Seg_9817" s="T17">v</ta>
            <ta e="T19" id="Seg_9818" s="T18">que</ta>
            <ta e="T20" id="Seg_9819" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_9820" s="T20">v</ta>
            <ta e="T22" id="Seg_9821" s="T21">conj</ta>
            <ta e="T23" id="Seg_9822" s="T22">n</ta>
            <ta e="T24" id="Seg_9823" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_9824" s="T24">v</ta>
            <ta e="T27" id="Seg_9825" s="T26">dempro</ta>
            <ta e="T28" id="Seg_9826" s="T27">dempro</ta>
            <ta e="T30" id="Seg_9827" s="T29">n</ta>
            <ta e="T31" id="Seg_9828" s="T30">v</ta>
            <ta e="T32" id="Seg_9829" s="T31">v</ta>
            <ta e="T34" id="Seg_9830" s="T33">conj</ta>
            <ta e="T35" id="Seg_9831" s="T34">que</ta>
            <ta e="T36" id="Seg_9832" s="T35">n</ta>
            <ta e="T37" id="Seg_9833" s="T36">v</ta>
            <ta e="T40" id="Seg_9834" s="T39">n</ta>
            <ta e="T41" id="Seg_9835" s="T40">v</ta>
            <ta e="T42" id="Seg_9836" s="T41">dempro</ta>
            <ta e="T43" id="Seg_9837" s="T42">n</ta>
            <ta e="T44" id="Seg_9838" s="T43">n</ta>
            <ta e="T45" id="Seg_9839" s="T44">v</ta>
            <ta e="T46" id="Seg_9840" s="T45">que</ta>
            <ta e="T47" id="Seg_9841" s="T46">v</ta>
            <ta e="T48" id="Seg_9842" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_9843" s="T48">v</ta>
            <ta e="T50" id="Seg_9844" s="T49">conj</ta>
            <ta e="T51" id="Seg_9845" s="T50">v</ta>
            <ta e="T52" id="Seg_9846" s="T51">n</ta>
            <ta e="T53" id="Seg_9847" s="T52">adv</ta>
            <ta e="T55" id="Seg_9848" s="T54">pers</ta>
            <ta e="T56" id="Seg_9849" s="T55">n</ta>
            <ta e="T892" id="Seg_9850" s="T56">v</ta>
            <ta e="T57" id="Seg_9851" s="T892">v</ta>
            <ta e="T58" id="Seg_9852" s="T57">n</ta>
            <ta e="T59" id="Seg_9853" s="T58">conj</ta>
            <ta e="T60" id="Seg_9854" s="T59">adv</ta>
            <ta e="T61" id="Seg_9855" s="T60">n</ta>
            <ta e="T62" id="Seg_9856" s="T61">v</ta>
            <ta e="T63" id="Seg_9857" s="T62">n</ta>
            <ta e="T64" id="Seg_9858" s="T63">v</ta>
            <ta e="T65" id="Seg_9859" s="T64">n</ta>
            <ta e="T66" id="Seg_9860" s="T65">n</ta>
            <ta e="T67" id="Seg_9861" s="T66">n</ta>
            <ta e="T68" id="Seg_9862" s="T67">quant</ta>
            <ta e="T70" id="Seg_9863" s="T69">que</ta>
            <ta e="T71" id="Seg_9864" s="T70">v</ta>
            <ta e="T72" id="Seg_9865" s="T71">v</ta>
            <ta e="T74" id="Seg_9866" s="T73">n</ta>
            <ta e="T75" id="Seg_9867" s="T74">conj</ta>
            <ta e="T76" id="Seg_9868" s="T75">n</ta>
            <ta e="T77" id="Seg_9869" s="T76">v</ta>
            <ta e="T78" id="Seg_9870" s="T77">adv</ta>
            <ta e="T79" id="Seg_9871" s="T78">adj</ta>
            <ta e="T80" id="Seg_9872" s="T79">v</ta>
            <ta e="T83" id="Seg_9873" s="T81">num</ta>
            <ta e="T84" id="Seg_9874" s="T83">num</ta>
            <ta e="T85" id="Seg_9875" s="T84">num</ta>
            <ta e="T86" id="Seg_9876" s="T85">v</ta>
            <ta e="T87" id="Seg_9877" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_9878" s="T87">v</ta>
            <ta e="T89" id="Seg_9879" s="T88">v</ta>
            <ta e="T90" id="Seg_9880" s="T89">conj</ta>
            <ta e="T91" id="Seg_9881" s="T90">dempro</ta>
            <ta e="T92" id="Seg_9882" s="T91">n</ta>
            <ta e="T93" id="Seg_9883" s="T92">v</ta>
            <ta e="T94" id="Seg_9884" s="T93">adv</ta>
            <ta e="T95" id="Seg_9885" s="T94">n</ta>
            <ta e="T96" id="Seg_9886" s="T95">adj</ta>
            <ta e="T97" id="Seg_9887" s="T96">que</ta>
            <ta e="T98" id="Seg_9888" s="T97">v</ta>
            <ta e="T99" id="Seg_9889" s="T98">dempro</ta>
            <ta e="T100" id="Seg_9890" s="T99">v</ta>
            <ta e="T102" id="Seg_9891" s="T101">conj</ta>
            <ta e="T103" id="Seg_9892" s="T102">pers</ta>
            <ta e="T104" id="Seg_9893" s="T103">n</ta>
            <ta e="T105" id="Seg_9894" s="T104">adv</ta>
            <ta e="T106" id="Seg_9895" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_9896" s="T106">adj</ta>
            <ta e="T108" id="Seg_9897" s="T107">v</ta>
            <ta e="T109" id="Seg_9898" s="T108">v</ta>
            <ta e="T110" id="Seg_9899" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_9900" s="T110">v</ta>
            <ta e="T112" id="Seg_9901" s="T111">pers</ta>
            <ta e="T113" id="Seg_9902" s="T112">dempro</ta>
            <ta e="T114" id="Seg_9903" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_9904" s="T114">v</ta>
            <ta e="T117" id="Seg_9905" s="T116">n</ta>
            <ta e="T118" id="Seg_9906" s="T117">v</ta>
            <ta e="T119" id="Seg_9907" s="T118">n</ta>
            <ta e="T120" id="Seg_9908" s="T119">v</ta>
            <ta e="T122" id="Seg_9909" s="T121">n</ta>
            <ta e="T123" id="Seg_9910" s="T122">v</ta>
            <ta e="T124" id="Seg_9911" s="T123">que</ta>
            <ta e="T125" id="Seg_9912" s="T124">adv</ta>
            <ta e="T126" id="Seg_9913" s="T125">n</ta>
            <ta e="T127" id="Seg_9914" s="T126">quant</ta>
            <ta e="T128" id="Seg_9915" s="T127">conj</ta>
            <ta e="T129" id="Seg_9916" s="T128">que</ta>
            <ta e="T130" id="Seg_9917" s="T129">v</ta>
            <ta e="T131" id="Seg_9918" s="T130">dempro</ta>
            <ta e="T132" id="Seg_9919" s="T131">n</ta>
            <ta e="T133" id="Seg_9920" s="T132">v</ta>
            <ta e="T134" id="Seg_9921" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_9922" s="T134">n</ta>
            <ta e="T136" id="Seg_9923" s="T135">v</ta>
            <ta e="T137" id="Seg_9924" s="T136">adv</ta>
            <ta e="T138" id="Seg_9925" s="T137">que</ta>
            <ta e="T139" id="Seg_9926" s="T138">v</ta>
            <ta e="T140" id="Seg_9927" s="T139">v</ta>
            <ta e="T141" id="Seg_9928" s="T140">v</ta>
            <ta e="T142" id="Seg_9929" s="T141">ptcl</ta>
            <ta e="T143" id="Seg_9930" s="T142">v</ta>
            <ta e="T146" id="Seg_9931" s="T145">propr</ta>
            <ta e="T147" id="Seg_9932" s="T146">adv</ta>
            <ta e="T148" id="Seg_9933" s="T147">quant</ta>
            <ta e="T149" id="Seg_9934" s="T148">n</ta>
            <ta e="T150" id="Seg_9935" s="T149">v</ta>
            <ta e="T151" id="Seg_9936" s="T150">adv</ta>
            <ta e="T152" id="Seg_9937" s="T151">n</ta>
            <ta e="T153" id="Seg_9938" s="T152">n</ta>
            <ta e="T154" id="Seg_9939" s="T153">v</ta>
            <ta e="T155" id="Seg_9940" s="T154">dempro</ta>
            <ta e="T156" id="Seg_9941" s="T155">v</ta>
            <ta e="T157" id="Seg_9942" s="T156">ptcl</ta>
            <ta e="T159" id="Seg_9943" s="T158">n</ta>
            <ta e="T160" id="Seg_9944" s="T159">n</ta>
            <ta e="T161" id="Seg_9945" s="T160">v</ta>
            <ta e="T162" id="Seg_9946" s="T161">pers</ta>
            <ta e="T163" id="Seg_9947" s="T162">ptcl</ta>
            <ta e="T164" id="Seg_9948" s="T163">adj</ta>
            <ta e="T165" id="Seg_9949" s="T164">v</ta>
            <ta e="T166" id="Seg_9950" s="T165">adv</ta>
            <ta e="T167" id="Seg_9951" s="T166">n</ta>
            <ta e="T168" id="Seg_9952" s="T167">v</ta>
            <ta e="T169" id="Seg_9953" s="T168">adv</ta>
            <ta e="T170" id="Seg_9954" s="T169">n</ta>
            <ta e="T171" id="Seg_9955" s="T170">v</ta>
            <ta e="T172" id="Seg_9956" s="T171">conj</ta>
            <ta e="T173" id="Seg_9957" s="T172">n</ta>
            <ta e="T174" id="Seg_9958" s="T173">v</ta>
            <ta e="T175" id="Seg_9959" s="T174">v</ta>
            <ta e="T176" id="Seg_9960" s="T175">conj</ta>
            <ta e="T177" id="Seg_9961" s="T176">dempro</ta>
            <ta e="T178" id="Seg_9962" s="T177">v</ta>
            <ta e="T179" id="Seg_9963" s="T178">dempro</ta>
            <ta e="T180" id="Seg_9964" s="T179">dempro</ta>
            <ta e="T181" id="Seg_9965" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_9966" s="T181">adv</ta>
            <ta e="T183" id="Seg_9967" s="T182">v</ta>
            <ta e="T891" id="Seg_9968" s="T184">adv</ta>
            <ta e="T185" id="Seg_9969" s="T891">pers</ta>
            <ta e="T186" id="Seg_9970" s="T185">pers</ta>
            <ta e="T187" id="Seg_9971" s="T186">ptcl</ta>
            <ta e="T188" id="Seg_9972" s="T187">v</ta>
            <ta e="T190" id="Seg_9973" s="T189">n</ta>
            <ta e="T192" id="Seg_9974" s="T190">adj</ta>
            <ta e="T193" id="Seg_9975" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_9976" s="T193">adj</ta>
            <ta e="T195" id="Seg_9977" s="T194">ptcl</ta>
            <ta e="T196" id="Seg_9978" s="T195">v</ta>
            <ta e="T197" id="Seg_9979" s="T196">n</ta>
            <ta e="T198" id="Seg_9980" s="T197">v</ta>
            <ta e="T200" id="Seg_9981" s="T199">ptcl</ta>
            <ta e="T201" id="Seg_9982" s="T200">n</ta>
            <ta e="T202" id="Seg_9983" s="T201">v</ta>
            <ta e="T203" id="Seg_9984" s="T202">adv</ta>
            <ta e="T204" id="Seg_9985" s="T203">n</ta>
            <ta e="T205" id="Seg_9986" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_9987" s="T205">v</ta>
            <ta e="T207" id="Seg_9988" s="T206">v</ta>
            <ta e="T208" id="Seg_9989" s="T207">dempro</ta>
            <ta e="T210" id="Seg_9990" s="T209">num</ta>
            <ta e="T211" id="Seg_9991" s="T210">n</ta>
            <ta e="T212" id="Seg_9992" s="T211">num</ta>
            <ta e="T213" id="Seg_9993" s="T212">n</ta>
            <ta e="T214" id="Seg_9994" s="T213">v</ta>
            <ta e="T215" id="Seg_9995" s="T214">num</ta>
            <ta e="T216" id="Seg_9996" s="T215">adj</ta>
            <ta e="T217" id="Seg_9997" s="T216">conj</ta>
            <ta e="T218" id="Seg_9998" s="T217">num</ta>
            <ta e="T219" id="Seg_9999" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_10000" s="T219">adj</ta>
            <ta e="T221" id="Seg_10001" s="T220">quant</ta>
            <ta e="T222" id="Seg_10002" s="T221">dempro</ta>
            <ta e="T223" id="Seg_10003" s="T222">v</ta>
            <ta e="T224" id="Seg_10004" s="T223">n</ta>
            <ta e="T225" id="Seg_10005" s="T224">ptcl</ta>
            <ta e="T226" id="Seg_10006" s="T225">conj</ta>
            <ta e="T229" id="Seg_10007" s="T228">conj</ta>
            <ta e="T230" id="Seg_10008" s="T229">n</ta>
            <ta e="T231" id="Seg_10009" s="T230">ptcl</ta>
            <ta e="T234" id="Seg_10010" s="T233">v</ta>
            <ta e="T235" id="Seg_10011" s="T234">conj</ta>
            <ta e="T236" id="Seg_10012" s="T235">pers</ta>
            <ta e="T239" id="Seg_10013" s="T237">v</ta>
            <ta e="T241" id="Seg_10014" s="T240">conj</ta>
            <ta e="T242" id="Seg_10015" s="T241">pers</ta>
            <ta e="T244" id="Seg_10016" s="T243">v</ta>
            <ta e="T245" id="Seg_10017" s="T244">aux</ta>
            <ta e="T246" id="Seg_10018" s="T245">v</ta>
            <ta e="T247" id="Seg_10019" s="T246">ptcl</ta>
            <ta e="T248" id="Seg_10020" s="T247">v</ta>
            <ta e="T249" id="Seg_10021" s="T248">adj</ta>
            <ta e="T250" id="Seg_10022" s="T249">quant</ta>
            <ta e="T251" id="Seg_10023" s="T250">v</ta>
            <ta e="T252" id="Seg_10024" s="T251">adj</ta>
            <ta e="T253" id="Seg_10025" s="T252">v</ta>
            <ta e="T254" id="Seg_10026" s="T253">ptcl</ta>
            <ta e="T255" id="Seg_10027" s="T254">ptcl</ta>
            <ta e="T256" id="Seg_10028" s="T255">v</ta>
            <ta e="T257" id="Seg_10029" s="T256">adv</ta>
            <ta e="T258" id="Seg_10030" s="T257">adj</ta>
            <ta e="T259" id="Seg_10031" s="T258">v</ta>
            <ta e="T261" id="Seg_10032" s="T260">conj</ta>
            <ta e="T262" id="Seg_10033" s="T261">dempro</ta>
            <ta e="T263" id="Seg_10034" s="T262">que</ta>
            <ta e="T264" id="Seg_10035" s="T263">adj</ta>
            <ta e="T265" id="Seg_10036" s="T264">v</ta>
            <ta e="T266" id="Seg_10037" s="T265">conj</ta>
            <ta e="T267" id="Seg_10038" s="T266">adj</ta>
            <ta e="T269" id="Seg_10039" s="T268">adj</ta>
            <ta e="T270" id="Seg_10040" s="T269">ptcl</ta>
            <ta e="T271" id="Seg_10041" s="T270">que</ta>
            <ta e="T272" id="Seg_10042" s="T271">v</ta>
            <ta e="T273" id="Seg_10043" s="T272">adj</ta>
            <ta e="T275" id="Seg_10044" s="T274">dempro</ta>
            <ta e="T276" id="Seg_10045" s="T275">n</ta>
            <ta e="T277" id="Seg_10046" s="T276">v</ta>
            <ta e="T278" id="Seg_10047" s="T277">adv</ta>
            <ta e="T279" id="Seg_10048" s="T278">v</ta>
            <ta e="T280" id="Seg_10049" s="T279">que</ta>
            <ta e="T281" id="Seg_10050" s="T280">v</ta>
            <ta e="T282" id="Seg_10051" s="T281">v</ta>
            <ta e="T283" id="Seg_10052" s="T282">dempro</ta>
            <ta e="T284" id="Seg_10053" s="T283">ptcl</ta>
            <ta e="T285" id="Seg_10054" s="T284">v</ta>
            <ta e="T287" id="Seg_10055" s="T286">num</ta>
            <ta e="T288" id="Seg_10056" s="T287">n</ta>
            <ta e="T289" id="Seg_10057" s="T288">n</ta>
            <ta e="T290" id="Seg_10058" s="T289">v</ta>
            <ta e="T291" id="Seg_10059" s="T290">ptcl</ta>
            <ta e="T292" id="Seg_10060" s="T291">adj</ta>
            <ta e="T293" id="Seg_10061" s="T292">pers</ta>
            <ta e="T294" id="Seg_10062" s="T293">que</ta>
            <ta e="T295" id="Seg_10063" s="T294">pers</ta>
            <ta e="T296" id="Seg_10064" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_10065" s="T296">v</ta>
            <ta e="T298" id="Seg_10066" s="T297">conj</ta>
            <ta e="T299" id="Seg_10067" s="T298">dempro</ta>
            <ta e="T300" id="Seg_10068" s="T299">v</ta>
            <ta e="T301" id="Seg_10069" s="T300">pers</ta>
            <ta e="T302" id="Seg_10070" s="T301">v</ta>
            <ta e="T303" id="Seg_10071" s="T302">pers</ta>
            <ta e="T304" id="Seg_10072" s="T303">que</ta>
            <ta e="T305" id="Seg_10073" s="T304">adv</ta>
            <ta e="T306" id="Seg_10074" s="T305">pers</ta>
            <ta e="T307" id="Seg_10075" s="T306">pers</ta>
            <ta e="T308" id="Seg_10076" s="T307">pers</ta>
            <ta e="T310" id="Seg_10077" s="T309">v</ta>
            <ta e="T311" id="Seg_10078" s="T310">conj</ta>
            <ta e="T312" id="Seg_10079" s="T311">dempro</ta>
            <ta e="T313" id="Seg_10080" s="T312">ptcl</ta>
            <ta e="T315" id="Seg_10081" s="T314">n</ta>
            <ta e="T316" id="Seg_10082" s="T315">v</ta>
            <ta e="T317" id="Seg_10083" s="T316">conj</ta>
            <ta e="T318" id="Seg_10084" s="T317">dempro</ta>
            <ta e="T319" id="Seg_10085" s="T318">ptcl</ta>
            <ta e="T321" id="Seg_10086" s="T319">n=ptcl</ta>
            <ta e="T322" id="Seg_10087" s="T321">n</ta>
            <ta e="T323" id="Seg_10088" s="T322">v</ta>
            <ta e="T324" id="Seg_10089" s="T323">v</ta>
            <ta e="T325" id="Seg_10090" s="T324">conj</ta>
            <ta e="T326" id="Seg_10091" s="T325">n</ta>
            <ta e="T327" id="Seg_10092" s="T326">ptcl</ta>
            <ta e="T328" id="Seg_10093" s="T327">v</ta>
            <ta e="T330" id="Seg_10094" s="T329">pers</ta>
            <ta e="T331" id="Seg_10095" s="T330">n</ta>
            <ta e="T332" id="Seg_10096" s="T331">n</ta>
            <ta e="T333" id="Seg_10097" s="T332">v</ta>
            <ta e="T334" id="Seg_10098" s="T333">v</ta>
            <ta e="T335" id="Seg_10099" s="T334">conj</ta>
            <ta e="T336" id="Seg_10100" s="T335">dempro</ta>
            <ta e="T337" id="Seg_10101" s="T336">v</ta>
            <ta e="T338" id="Seg_10102" s="T337">n</ta>
            <ta e="T339" id="Seg_10103" s="T338">conj</ta>
            <ta e="T340" id="Seg_10104" s="T339">adj</ta>
            <ta e="T341" id="Seg_10105" s="T340">n</ta>
            <ta e="T342" id="Seg_10106" s="T341">n</ta>
            <ta e="T344" id="Seg_10107" s="T343">v</ta>
            <ta e="T345" id="Seg_10108" s="T344">v</ta>
            <ta e="T346" id="Seg_10109" s="T345">conj</ta>
            <ta e="T347" id="Seg_10110" s="T346">dempro</ta>
            <ta e="T348" id="Seg_10111" s="T347">ptcl</ta>
            <ta e="T349" id="Seg_10112" s="T348">v</ta>
            <ta e="T350" id="Seg_10113" s="T349">num</ta>
            <ta e="T351" id="Seg_10114" s="T350">n</ta>
            <ta e="T353" id="Seg_10115" s="T352">v</ta>
            <ta e="T355" id="Seg_10116" s="T354">dempro</ta>
            <ta e="T357" id="Seg_10117" s="T356">pers</ta>
            <ta e="T359" id="Seg_10118" s="T358">n</ta>
            <ta e="T360" id="Seg_10119" s="T359">num</ta>
            <ta e="T361" id="Seg_10120" s="T360">n</ta>
            <ta e="T362" id="Seg_10121" s="T361">conj</ta>
            <ta e="T363" id="Seg_10122" s="T362">num</ta>
            <ta e="T364" id="Seg_10123" s="T363">n</ta>
            <ta e="T366" id="Seg_10124" s="T365">v</ta>
            <ta e="T367" id="Seg_10125" s="T366">pers</ta>
            <ta e="T368" id="Seg_10126" s="T367">adj</ta>
            <ta e="T369" id="Seg_10127" s="T368">adj</ta>
            <ta e="T370" id="Seg_10128" s="T369">n</ta>
            <ta e="T372" id="Seg_10129" s="T371">pers</ta>
            <ta e="T373" id="Seg_10130" s="T372">v</ta>
            <ta e="T374" id="Seg_10131" s="T373">pers</ta>
            <ta e="T375" id="Seg_10132" s="T374">v</ta>
            <ta e="T376" id="Seg_10133" s="T375">v</ta>
            <ta e="T377" id="Seg_10134" s="T376">pers</ta>
            <ta e="T378" id="Seg_10135" s="T377">adj</ta>
            <ta e="T379" id="Seg_10136" s="T378">n</ta>
            <ta e="T381" id="Seg_10137" s="T380">pers</ta>
            <ta e="T382" id="Seg_10138" s="T381">v</ta>
            <ta e="T383" id="Seg_10139" s="T382">pers</ta>
            <ta e="T384" id="Seg_10140" s="T383">v</ta>
            <ta e="T385" id="Seg_10141" s="T384">v</ta>
            <ta e="T386" id="Seg_10142" s="T385">pers</ta>
            <ta e="T387" id="Seg_10143" s="T386">adj</ta>
            <ta e="T388" id="Seg_10144" s="T387">n</ta>
            <ta e="T389" id="Seg_10145" s="T388">propr</ta>
            <ta e="T390" id="Seg_10146" s="T389">propr</ta>
            <ta e="T391" id="Seg_10147" s="T390">adj</ta>
            <ta e="T392" id="Seg_10148" s="T391">n</ta>
            <ta e="T393" id="Seg_10149" s="T392">v</ta>
            <ta e="T394" id="Seg_10150" s="T393">adv</ta>
            <ta e="T395" id="Seg_10151" s="T394">n</ta>
            <ta e="T396" id="Seg_10152" s="T395">v</ta>
            <ta e="T397" id="Seg_10153" s="T396">dempro</ta>
            <ta e="T398" id="Seg_10154" s="T397">n</ta>
            <ta e="T399" id="Seg_10155" s="T398">v</ta>
            <ta e="T400" id="Seg_10156" s="T399">n</ta>
            <ta e="T401" id="Seg_10157" s="T400">v</ta>
            <ta e="T402" id="Seg_10158" s="T401">adv</ta>
            <ta e="T403" id="Seg_10159" s="T402">v</ta>
            <ta e="T404" id="Seg_10160" s="T403">v</ta>
            <ta e="T405" id="Seg_10161" s="T404">ptcl</ta>
            <ta e="T406" id="Seg_10162" s="T405">adv</ta>
            <ta e="T407" id="Seg_10163" s="T406">num</ta>
            <ta e="T408" id="Seg_10164" s="T407">n</ta>
            <ta e="T409" id="Seg_10165" s="T408">v</ta>
            <ta e="T410" id="Seg_10166" s="T409">n</ta>
            <ta e="T411" id="Seg_10167" s="T410">dempro</ta>
            <ta e="T412" id="Seg_10168" s="T411">v</ta>
            <ta e="T413" id="Seg_10169" s="T412">dempro</ta>
            <ta e="T414" id="Seg_10170" s="T413">dempro</ta>
            <ta e="T415" id="Seg_10171" s="T414">n</ta>
            <ta e="T416" id="Seg_10172" s="T415">v</ta>
            <ta e="T417" id="Seg_10173" s="T416">num</ta>
            <ta e="T418" id="Seg_10174" s="T417">dempro</ta>
            <ta e="T419" id="Seg_10175" s="T418">v</ta>
            <ta e="T420" id="Seg_10176" s="T419">ptcl</ta>
            <ta e="T421" id="Seg_10177" s="T420">adj</ta>
            <ta e="T422" id="Seg_10178" s="T421">n</ta>
            <ta e="T423" id="Seg_10179" s="T422">v</ta>
            <ta e="T424" id="Seg_10180" s="T423">n</ta>
            <ta e="T426" id="Seg_10181" s="T425">v</ta>
            <ta e="T427" id="Seg_10182" s="T426">conj</ta>
            <ta e="T428" id="Seg_10183" s="T427">dempro</ta>
            <ta e="T429" id="Seg_10184" s="T428">v</ta>
            <ta e="T430" id="Seg_10185" s="T429">aux</ta>
            <ta e="T431" id="Seg_10186" s="T430">v</ta>
            <ta e="T432" id="Seg_10187" s="T431">pers</ta>
            <ta e="T433" id="Seg_10188" s="T432">n</ta>
            <ta e="T434" id="Seg_10189" s="T433">v</ta>
            <ta e="T435" id="Seg_10190" s="T434">adv</ta>
            <ta e="T436" id="Seg_10191" s="T435">n</ta>
            <ta e="T437" id="Seg_10192" s="T436">v</ta>
            <ta e="T438" id="Seg_10193" s="T437">n</ta>
            <ta e="T439" id="Seg_10194" s="T438">v</ta>
            <ta e="T440" id="Seg_10195" s="T439">dempro</ta>
            <ta e="T442" id="Seg_10196" s="T441">v</ta>
            <ta e="T443" id="Seg_10197" s="T442">n</ta>
            <ta e="T444" id="Seg_10198" s="T443">n</ta>
            <ta e="T445" id="Seg_10199" s="T444">v</ta>
            <ta e="T446" id="Seg_10200" s="T445">n</ta>
            <ta e="T447" id="Seg_10201" s="T446">n</ta>
            <ta e="T449" id="Seg_10202" s="T447">n</ta>
            <ta e="T450" id="Seg_10203" s="T449">adv</ta>
            <ta e="T451" id="Seg_10204" s="T450">n</ta>
            <ta e="T452" id="Seg_10205" s="T451">v</ta>
            <ta e="T453" id="Seg_10206" s="T452">v</ta>
            <ta e="T454" id="Seg_10207" s="T453">v</ta>
            <ta e="T455" id="Seg_10208" s="T454">adv</ta>
            <ta e="T456" id="Seg_10209" s="T455">n</ta>
            <ta e="T457" id="Seg_10210" s="T456">v</ta>
            <ta e="T458" id="Seg_10211" s="T457">num</ta>
            <ta e="T459" id="Seg_10212" s="T458">n</ta>
            <ta e="T460" id="Seg_10213" s="T459">dempro</ta>
            <ta e="T461" id="Seg_10214" s="T460">ptcl</ta>
            <ta e="T463" id="Seg_10215" s="T462">v</ta>
            <ta e="T464" id="Seg_10216" s="T463">ptcl</ta>
            <ta e="T465" id="Seg_10217" s="T464">v</ta>
            <ta e="T466" id="Seg_10218" s="T465">conj</ta>
            <ta e="T467" id="Seg_10219" s="T466">dempro</ta>
            <ta e="T470" id="Seg_10220" s="T469">v</ta>
            <ta e="T471" id="Seg_10221" s="T470">adv</ta>
            <ta e="T472" id="Seg_10222" s="T471">n</ta>
            <ta e="T473" id="Seg_10223" s="T472">v</ta>
            <ta e="T474" id="Seg_10224" s="T473">pers</ta>
            <ta e="T475" id="Seg_10225" s="T474">n</ta>
            <ta e="T476" id="Seg_10226" s="T475">v</ta>
            <ta e="T477" id="Seg_10227" s="T476">pers</ta>
            <ta e="T478" id="Seg_10228" s="T477">ptcl</ta>
            <ta e="T479" id="Seg_10229" s="T478">adv</ta>
            <ta e="T480" id="Seg_10230" s="T479">n</ta>
            <ta e="T481" id="Seg_10231" s="T480">adv</ta>
            <ta e="T482" id="Seg_10232" s="T481">dempro</ta>
            <ta e="T483" id="Seg_10233" s="T482">n</ta>
            <ta e="T484" id="Seg_10234" s="T483">v</ta>
            <ta e="T486" id="Seg_10235" s="T485">adv</ta>
            <ta e="T487" id="Seg_10236" s="T486">dempro</ta>
            <ta e="T488" id="Seg_10237" s="T487">propr</ta>
            <ta e="T490" id="Seg_10238" s="T489">v</ta>
            <ta e="T491" id="Seg_10239" s="T490">v</ta>
            <ta e="T492" id="Seg_10240" s="T491">v</ta>
            <ta e="T493" id="Seg_10241" s="T492">n</ta>
            <ta e="T494" id="Seg_10242" s="T493">n</ta>
            <ta e="T495" id="Seg_10243" s="T494">v</ta>
            <ta e="T496" id="Seg_10244" s="T495">v</ta>
            <ta e="T497" id="Seg_10245" s="T496">adv</ta>
            <ta e="T499" id="Seg_10246" s="T498">n</ta>
            <ta e="T500" id="Seg_10247" s="T499">n</ta>
            <ta e="T501" id="Seg_10248" s="T500">v</ta>
            <ta e="T502" id="Seg_10249" s="T501">adv</ta>
            <ta e="T503" id="Seg_10250" s="T502">ptcl</ta>
            <ta e="T504" id="Seg_10251" s="T503">num</ta>
            <ta e="T505" id="Seg_10252" s="T504">n</ta>
            <ta e="T506" id="Seg_10253" s="T505">n</ta>
            <ta e="T507" id="Seg_10254" s="T506">v</ta>
            <ta e="T508" id="Seg_10255" s="T507">conj</ta>
            <ta e="T509" id="Seg_10256" s="T508">n</ta>
            <ta e="T510" id="Seg_10257" s="T509">n</ta>
            <ta e="T511" id="Seg_10258" s="T510">v</ta>
            <ta e="T512" id="Seg_10259" s="T511">adv</ta>
            <ta e="T513" id="Seg_10260" s="T512">v</ta>
            <ta e="T514" id="Seg_10261" s="T513">v</ta>
            <ta e="T515" id="Seg_10262" s="T514">pers</ta>
            <ta e="T516" id="Seg_10263" s="T515">n</ta>
            <ta e="T517" id="Seg_10264" s="T516">pers</ta>
            <ta e="T518" id="Seg_10265" s="T517">n</ta>
            <ta e="T519" id="Seg_10266" s="T518">ptcl</ta>
            <ta e="T520" id="Seg_10267" s="T519">ptcl</ta>
            <ta e="T521" id="Seg_10268" s="T520">v</ta>
            <ta e="T522" id="Seg_10269" s="T521">pers</ta>
            <ta e="T523" id="Seg_10270" s="T522">n</ta>
            <ta e="T524" id="Seg_10271" s="T523">pers</ta>
            <ta e="T525" id="Seg_10272" s="T524">n</ta>
            <ta e="T526" id="Seg_10273" s="T525">v</ta>
            <ta e="T527" id="Seg_10274" s="T526">conj</ta>
            <ta e="T528" id="Seg_10275" s="T527">refl</ta>
            <ta e="T530" id="Seg_10276" s="T529">v</ta>
            <ta e="T531" id="Seg_10277" s="T530">v</ta>
            <ta e="T532" id="Seg_10278" s="T531">dempro</ta>
            <ta e="T534" id="Seg_10279" s="T533">v</ta>
            <ta e="T535" id="Seg_10280" s="T534">conj</ta>
            <ta e="T536" id="Seg_10281" s="T535">n</ta>
            <ta e="T537" id="Seg_10282" s="T536">v</ta>
            <ta e="T538" id="Seg_10283" s="T537">dempro</ta>
            <ta e="T539" id="Seg_10284" s="T538">n</ta>
            <ta e="T540" id="Seg_10285" s="T539">conj</ta>
            <ta e="T541" id="Seg_10286" s="T540">adv</ta>
            <ta e="T542" id="Seg_10287" s="T541">v</ta>
            <ta e="T543" id="Seg_10288" s="T542">v</ta>
            <ta e="T544" id="Seg_10289" s="T543">n</ta>
            <ta e="T545" id="Seg_10290" s="T544">v</ta>
            <ta e="T546" id="Seg_10291" s="T545">conj</ta>
            <ta e="T547" id="Seg_10292" s="T546">n</ta>
            <ta e="T548" id="Seg_10293" s="T547">v</ta>
            <ta e="T550" id="Seg_10294" s="T549">dempro</ta>
            <ta e="T551" id="Seg_10295" s="T550">propr</ta>
            <ta e="T552" id="Seg_10296" s="T551">v</ta>
            <ta e="T553" id="Seg_10297" s="T552">n</ta>
            <ta e="T554" id="Seg_10298" s="T553">v</ta>
            <ta e="T555" id="Seg_10299" s="T554">conj</ta>
            <ta e="T556" id="Seg_10300" s="T555">dempro</ta>
            <ta e="T557" id="Seg_10301" s="T556">dempro</ta>
            <ta e="T558" id="Seg_10302" s="T557">n</ta>
            <ta e="T559" id="Seg_10303" s="T558">ptcl</ta>
            <ta e="T560" id="Seg_10304" s="T559">n</ta>
            <ta e="T562" id="Seg_10305" s="T561">n</ta>
            <ta e="T563" id="Seg_10306" s="T562">ptcl</ta>
            <ta e="T564" id="Seg_10307" s="T563">v</ta>
            <ta e="T565" id="Seg_10308" s="T564">n</ta>
            <ta e="T566" id="Seg_10309" s="T565">conj</ta>
            <ta e="T567" id="Seg_10310" s="T566">n</ta>
            <ta e="T568" id="Seg_10311" s="T567">conj</ta>
            <ta e="T569" id="Seg_10312" s="T568">n</ta>
            <ta e="T570" id="Seg_10313" s="T569">v</ta>
            <ta e="T571" id="Seg_10314" s="T570">v</ta>
            <ta e="T572" id="Seg_10315" s="T571">adv</ta>
            <ta e="T574" id="Seg_10316" s="T573">pers</ta>
            <ta e="T575" id="Seg_10317" s="T574">n</ta>
            <ta e="T576" id="Seg_10318" s="T575">dempro</ta>
            <ta e="T577" id="Seg_10319" s="T576">dempro</ta>
            <ta e="T578" id="Seg_10320" s="T577">n</ta>
            <ta e="T579" id="Seg_10321" s="T578">ptcl</ta>
            <ta e="T580" id="Seg_10322" s="T579">v</ta>
            <ta e="T582" id="Seg_10323" s="T581">adv</ta>
            <ta e="T583" id="Seg_10324" s="T582">dempro</ta>
            <ta e="T584" id="Seg_10325" s="T583">v</ta>
            <ta e="T585" id="Seg_10326" s="T584">n</ta>
            <ta e="T586" id="Seg_10327" s="T585">v</ta>
            <ta e="T587" id="Seg_10328" s="T586">v</ta>
            <ta e="T588" id="Seg_10329" s="T587">num</ta>
            <ta e="T589" id="Seg_10330" s="T588">n</ta>
            <ta e="T590" id="Seg_10331" s="T589">v</ta>
            <ta e="T591" id="Seg_10332" s="T590">conj</ta>
            <ta e="T592" id="Seg_10333" s="T591">v</ta>
            <ta e="T595" id="Seg_10334" s="T594">v</ta>
            <ta e="T596" id="Seg_10335" s="T595">adv</ta>
            <ta e="T597" id="Seg_10336" s="T596">v</ta>
            <ta e="T598" id="Seg_10337" s="T597">v</ta>
            <ta e="T599" id="Seg_10338" s="T598">adv</ta>
            <ta e="T600" id="Seg_10339" s="T599">ptcl</ta>
            <ta e="T601" id="Seg_10340" s="T600">n</ta>
            <ta e="T602" id="Seg_10341" s="T601">v</ta>
            <ta e="T603" id="Seg_10342" s="T602">dempro</ta>
            <ta e="T604" id="Seg_10343" s="T603">v</ta>
            <ta e="T605" id="Seg_10344" s="T604">adv</ta>
            <ta e="T606" id="Seg_10345" s="T605">n</ta>
            <ta e="T607" id="Seg_10346" s="T606">v</ta>
            <ta e="T608" id="Seg_10347" s="T607">dempro</ta>
            <ta e="T609" id="Seg_10348" s="T608">n</ta>
            <ta e="T610" id="Seg_10349" s="T609">v</ta>
            <ta e="T611" id="Seg_10350" s="T610">conj</ta>
            <ta e="T612" id="Seg_10351" s="T611">num</ta>
            <ta e="T613" id="Seg_10352" s="T612">n</ta>
            <ta e="T614" id="Seg_10353" s="T613">conj</ta>
            <ta e="T615" id="Seg_10354" s="T614">dempro</ta>
            <ta e="T616" id="Seg_10355" s="T615">n</ta>
            <ta e="T617" id="Seg_10356" s="T616">dempro</ta>
            <ta e="T618" id="Seg_10357" s="T617">n</ta>
            <ta e="T619" id="Seg_10358" s="T618">n</ta>
            <ta e="T620" id="Seg_10359" s="T619">v</ta>
            <ta e="T621" id="Seg_10360" s="T620">n</ta>
            <ta e="T622" id="Seg_10361" s="T621">n</ta>
            <ta e="T623" id="Seg_10362" s="T622">n</ta>
            <ta e="T625" id="Seg_10363" s="T624">v</ta>
            <ta e="T626" id="Seg_10364" s="T625">adv</ta>
            <ta e="T627" id="Seg_10365" s="T626">v</ta>
            <ta e="T629" id="Seg_10366" s="T628">adv</ta>
            <ta e="T630" id="Seg_10367" s="T629">num</ta>
            <ta e="T631" id="Seg_10368" s="T630">n</ta>
            <ta e="T632" id="Seg_10369" s="T631">conj</ta>
            <ta e="T633" id="Seg_10370" s="T632">n</ta>
            <ta e="T634" id="Seg_10371" s="T633">v</ta>
            <ta e="T635" id="Seg_10372" s="T634">v</ta>
            <ta e="T636" id="Seg_10373" s="T635">v</ta>
            <ta e="T637" id="Seg_10374" s="T636">pers</ta>
            <ta e="T638" id="Seg_10375" s="T637">adv</ta>
            <ta e="T639" id="Seg_10376" s="T638">n</ta>
            <ta e="T640" id="Seg_10377" s="T639">v</ta>
            <ta e="T641" id="Seg_10378" s="T640">n</ta>
            <ta e="T642" id="Seg_10379" s="T641">conj</ta>
            <ta e="T643" id="Seg_10380" s="T642">refl</ta>
            <ta e="T645" id="Seg_10381" s="T644">n</ta>
            <ta e="T646" id="Seg_10382" s="T645">n</ta>
            <ta e="T647" id="Seg_10383" s="T646">v</ta>
            <ta e="T648" id="Seg_10384" s="T647">conj</ta>
            <ta e="T649" id="Seg_10385" s="T648">n</ta>
            <ta e="T650" id="Seg_10386" s="T649">pers</ta>
            <ta e="T651" id="Seg_10387" s="T650">v</ta>
            <ta e="T652" id="Seg_10388" s="T651">v</ta>
            <ta e="T653" id="Seg_10389" s="T652">n</ta>
            <ta e="T654" id="Seg_10390" s="T653">pers</ta>
            <ta e="T655" id="Seg_10391" s="T654">dempro</ta>
            <ta e="T656" id="Seg_10392" s="T655">adv</ta>
            <ta e="T657" id="Seg_10393" s="T656">v</ta>
            <ta e="T658" id="Seg_10394" s="T657">ptcl</ta>
            <ta e="T659" id="Seg_10395" s="T658">v</ta>
            <ta e="T660" id="Seg_10396" s="T659">refl</ta>
            <ta e="T661" id="Seg_10397" s="T660">refl</ta>
            <ta e="T663" id="Seg_10398" s="T662">adv</ta>
            <ta e="T664" id="Seg_10399" s="T663">dempro</ta>
            <ta e="T665" id="Seg_10400" s="T664">n</ta>
            <ta e="T666" id="Seg_10401" s="T665">v</ta>
            <ta e="T667" id="Seg_10402" s="T666">v</ta>
            <ta e="T668" id="Seg_10403" s="T667">v</ta>
            <ta e="T669" id="Seg_10404" s="T668">n</ta>
            <ta e="T670" id="Seg_10405" s="T669">ptcl</ta>
            <ta e="T672" id="Seg_10406" s="T671">v</ta>
            <ta e="T673" id="Seg_10407" s="T672">n</ta>
            <ta e="T674" id="Seg_10408" s="T673">adv</ta>
            <ta e="T675" id="Seg_10409" s="T674">dempro</ta>
            <ta e="T676" id="Seg_10410" s="T675">ptcl</ta>
            <ta e="T677" id="Seg_10411" s="T676">v</ta>
            <ta e="T678" id="Seg_10412" s="T677">propr</ta>
            <ta e="T679" id="Seg_10413" s="T678">v</ta>
            <ta e="T680" id="Seg_10414" s="T679">v</ta>
            <ta e="T681" id="Seg_10415" s="T680">dempro</ta>
            <ta e="T682" id="Seg_10416" s="T681">n</ta>
            <ta e="T683" id="Seg_10417" s="T682">v</ta>
            <ta e="T684" id="Seg_10418" s="T683">v</ta>
            <ta e="T685" id="Seg_10419" s="T684">num</ta>
            <ta e="T686" id="Seg_10420" s="T685">n</ta>
            <ta e="T687" id="Seg_10421" s="T686">adv</ta>
            <ta e="T688" id="Seg_10422" s="T687">pers</ta>
            <ta e="T689" id="Seg_10423" s="T688">num</ta>
            <ta e="T690" id="Seg_10424" s="T689">pers</ta>
            <ta e="T691" id="Seg_10425" s="T690">n</ta>
            <ta e="T692" id="Seg_10426" s="T691">adv</ta>
            <ta e="T693" id="Seg_10427" s="T692">v</ta>
            <ta e="T694" id="Seg_10428" s="T693">ptcl</ta>
            <ta e="T695" id="Seg_10429" s="T694">v</ta>
            <ta e="T696" id="Seg_10430" s="T695">num</ta>
            <ta e="T697" id="Seg_10431" s="T696">adv</ta>
            <ta e="T698" id="Seg_10432" s="T697">dempro</ta>
            <ta e="T699" id="Seg_10433" s="T698">v</ta>
            <ta e="T700" id="Seg_10434" s="T699">adv</ta>
            <ta e="T701" id="Seg_10435" s="T700">v</ta>
            <ta e="T702" id="Seg_10436" s="T701">n</ta>
            <ta e="T703" id="Seg_10437" s="T702">conj</ta>
            <ta e="T704" id="Seg_10438" s="T703">num</ta>
            <ta e="T705" id="Seg_10439" s="T704">n</ta>
            <ta e="T706" id="Seg_10440" s="T705">v</ta>
            <ta e="T707" id="Seg_10441" s="T706">conj</ta>
            <ta e="T708" id="Seg_10442" s="T707">v</ta>
            <ta e="T709" id="Seg_10443" s="T708">adv</ta>
            <ta e="T710" id="Seg_10444" s="T709">v</ta>
            <ta e="T711" id="Seg_10445" s="T710">v</ta>
            <ta e="T712" id="Seg_10446" s="T711">adv</ta>
            <ta e="T713" id="Seg_10447" s="T712">n</ta>
            <ta e="T714" id="Seg_10448" s="T713">v</ta>
            <ta e="T715" id="Seg_10449" s="T714">adv</ta>
            <ta e="T716" id="Seg_10450" s="T715">v</ta>
            <ta e="T717" id="Seg_10451" s="T716">n</ta>
            <ta e="T718" id="Seg_10452" s="T717">adv</ta>
            <ta e="T719" id="Seg_10453" s="T718">pers</ta>
            <ta e="T720" id="Seg_10454" s="T719">n</ta>
            <ta e="T721" id="Seg_10455" s="T720">conj</ta>
            <ta e="T723" id="Seg_10456" s="T722">num</ta>
            <ta e="T725" id="Seg_10457" s="T724">v</ta>
            <ta e="T726" id="Seg_10458" s="T725">n</ta>
            <ta e="T728" id="Seg_10459" s="T727">n</ta>
            <ta e="T729" id="Seg_10460" s="T728">num</ta>
            <ta e="T730" id="Seg_10461" s="T729">v</ta>
            <ta e="T731" id="Seg_10462" s="T730">n</ta>
            <ta e="T732" id="Seg_10463" s="T731">adv</ta>
            <ta e="T733" id="Seg_10464" s="T732">v</ta>
            <ta e="T734" id="Seg_10465" s="T733">pers</ta>
            <ta e="T735" id="Seg_10466" s="T734">n</ta>
            <ta e="T736" id="Seg_10467" s="T735">ptcl</ta>
            <ta e="T737" id="Seg_10468" s="T736">v</ta>
            <ta e="T740" id="Seg_10469" s="T739">v</ta>
            <ta e="T741" id="Seg_10470" s="T740">dempro</ta>
            <ta e="T742" id="Seg_10471" s="T741">v</ta>
            <ta e="T743" id="Seg_10472" s="T742">propr</ta>
            <ta e="T744" id="Seg_10473" s="T743">n</ta>
            <ta e="T745" id="Seg_10474" s="T744">adv</ta>
            <ta e="T746" id="Seg_10475" s="T745">n</ta>
            <ta e="T747" id="Seg_10476" s="T746">n</ta>
            <ta e="T748" id="Seg_10477" s="T747">v</ta>
            <ta e="T749" id="Seg_10478" s="T748">v</ta>
            <ta e="T750" id="Seg_10479" s="T749">dempro</ta>
            <ta e="T751" id="Seg_10480" s="T750">v</ta>
            <ta e="T752" id="Seg_10481" s="T751">propr</ta>
            <ta e="T753" id="Seg_10482" s="T752">n</ta>
            <ta e="T754" id="Seg_10483" s="T753">adv</ta>
            <ta e="T756" id="Seg_10484" s="T755">v</ta>
            <ta e="T757" id="Seg_10485" s="T756">n</ta>
            <ta e="T759" id="Seg_10486" s="T758">adv</ta>
            <ta e="T761" id="Seg_10487" s="T760">v</ta>
            <ta e="T762" id="Seg_10488" s="T761">n</ta>
            <ta e="T763" id="Seg_10489" s="T762">n</ta>
            <ta e="T765" id="Seg_10490" s="T764">v</ta>
            <ta e="T766" id="Seg_10491" s="T765">pers</ta>
            <ta e="T767" id="Seg_10492" s="T766">n</ta>
            <ta e="T768" id="Seg_10493" s="T767">conj</ta>
            <ta e="T769" id="Seg_10494" s="T768">dempro</ta>
            <ta e="T770" id="Seg_10495" s="T769">v</ta>
            <ta e="T771" id="Seg_10496" s="T770">ptcl</ta>
            <ta e="T772" id="Seg_10497" s="T771">pers</ta>
            <ta e="T773" id="Seg_10498" s="T772">n</ta>
            <ta e="T774" id="Seg_10499" s="T773">ptcl</ta>
            <ta e="T775" id="Seg_10500" s="T774">v</ta>
            <ta e="T776" id="Seg_10501" s="T775">n</ta>
            <ta e="T777" id="Seg_10502" s="T776">n</ta>
            <ta e="T778" id="Seg_10503" s="T777">dempro</ta>
            <ta e="T779" id="Seg_10504" s="T778">v</ta>
            <ta e="T780" id="Seg_10505" s="T779">propr</ta>
            <ta e="T781" id="Seg_10506" s="T780">n</ta>
            <ta e="T782" id="Seg_10507" s="T781">adv</ta>
            <ta e="T783" id="Seg_10508" s="T782">n</ta>
            <ta e="T784" id="Seg_10509" s="T783">n</ta>
            <ta e="T785" id="Seg_10510" s="T784">v</ta>
            <ta e="T786" id="Seg_10511" s="T785">dempro</ta>
            <ta e="T787" id="Seg_10512" s="T786">v</ta>
            <ta e="T788" id="Seg_10513" s="T787">propr</ta>
            <ta e="T789" id="Seg_10514" s="T788">n</ta>
            <ta e="T790" id="Seg_10515" s="T789">dempro</ta>
            <ta e="T791" id="Seg_10516" s="T790">ptcl</ta>
            <ta e="T792" id="Seg_10517" s="T791">v</ta>
            <ta e="T793" id="Seg_10518" s="T792">adv</ta>
            <ta e="T794" id="Seg_10519" s="T793">n</ta>
            <ta e="T795" id="Seg_10520" s="T794">v</ta>
            <ta e="T796" id="Seg_10521" s="T795">conj</ta>
            <ta e="T797" id="Seg_10522" s="T796">adv</ta>
            <ta e="T798" id="Seg_10523" s="T797">ptcl</ta>
            <ta e="T799" id="Seg_10524" s="T798">n</ta>
            <ta e="T800" id="Seg_10525" s="T799">v</ta>
            <ta e="T801" id="Seg_10526" s="T800">n</ta>
            <ta e="T802" id="Seg_10527" s="T801">dempro</ta>
            <ta e="T803" id="Seg_10528" s="T802">v</ta>
            <ta e="T804" id="Seg_10529" s="T803">pers</ta>
            <ta e="T805" id="Seg_10530" s="T804">n</ta>
            <ta e="T806" id="Seg_10531" s="T805">ptcl</ta>
            <ta e="T807" id="Seg_10532" s="T806">ptcl</ta>
            <ta e="T808" id="Seg_10533" s="T807">pers</ta>
            <ta e="T809" id="Seg_10534" s="T808">ptcl</ta>
            <ta e="T810" id="Seg_10535" s="T809">v</ta>
            <ta e="T811" id="Seg_10536" s="T810">n</ta>
            <ta e="T812" id="Seg_10537" s="T811">n</ta>
            <ta e="T813" id="Seg_10538" s="T812">adv</ta>
            <ta e="T814" id="Seg_10539" s="T813">n</ta>
            <ta e="T815" id="Seg_10540" s="T814">dempro</ta>
            <ta e="T816" id="Seg_10541" s="T815">v</ta>
            <ta e="T817" id="Seg_10542" s="T816">que</ta>
            <ta e="T818" id="Seg_10543" s="T817">n</ta>
            <ta e="T819" id="Seg_10544" s="T818">dempro</ta>
            <ta e="T820" id="Seg_10545" s="T819">dempro</ta>
            <ta e="T821" id="Seg_10546" s="T820">v</ta>
            <ta e="T822" id="Seg_10547" s="T821">propr</ta>
            <ta e="T823" id="Seg_10548" s="T822">n</ta>
            <ta e="T824" id="Seg_10549" s="T823">conj</ta>
            <ta e="T825" id="Seg_10550" s="T824">n</ta>
            <ta e="T826" id="Seg_10551" s="T825">v</ta>
            <ta e="T827" id="Seg_10552" s="T826">que</ta>
            <ta e="T828" id="Seg_10553" s="T827">n</ta>
            <ta e="T829" id="Seg_10554" s="T828">propr</ta>
            <ta e="T830" id="Seg_10555" s="T829">n</ta>
            <ta e="T831" id="Seg_10556" s="T830">adv</ta>
            <ta e="T832" id="Seg_10557" s="T831">dempro</ta>
            <ta e="T833" id="Seg_10558" s="T832">ptcl</ta>
            <ta e="T834" id="Seg_10559" s="T833">v</ta>
            <ta e="T835" id="Seg_10560" s="T834">n</ta>
            <ta e="T836" id="Seg_10561" s="T835">pers</ta>
            <ta e="T837" id="Seg_10562" s="T836">n</ta>
            <ta e="T840" id="Seg_10563" s="T839">v</ta>
            <ta e="T842" id="Seg_10564" s="T841">n</ta>
            <ta e="T843" id="Seg_10565" s="T842">v</ta>
            <ta e="T844" id="Seg_10566" s="T843">n</ta>
            <ta e="T845" id="Seg_10567" s="T844">num</ta>
            <ta e="T846" id="Seg_10568" s="T845">n</ta>
            <ta e="T847" id="Seg_10569" s="T846">v</ta>
            <ta e="T848" id="Seg_10570" s="T847">num</ta>
            <ta e="T849" id="Seg_10571" s="T848">n</ta>
            <ta e="T850" id="Seg_10572" s="T849">v</ta>
            <ta e="T851" id="Seg_10573" s="T850">v</ta>
            <ta e="T852" id="Seg_10574" s="T851">n</ta>
            <ta e="T853" id="Seg_10575" s="T852">n</ta>
            <ta e="T854" id="Seg_10576" s="T853">v</ta>
            <ta e="T855" id="Seg_10577" s="T854">conj</ta>
            <ta e="T856" id="Seg_10578" s="T855">num</ta>
            <ta e="T857" id="Seg_10579" s="T856">n</ta>
            <ta e="T858" id="Seg_10580" s="T857">conj</ta>
            <ta e="T859" id="Seg_10581" s="T858">n</ta>
            <ta e="T860" id="Seg_10582" s="T859">n</ta>
            <ta e="T863" id="Seg_10583" s="T862">n</ta>
            <ta e="T865" id="Seg_10584" s="T864">n</ta>
            <ta e="T866" id="Seg_10585" s="T865">n</ta>
            <ta e="T868" id="Seg_10586" s="T867">v</ta>
            <ta e="T869" id="Seg_10587" s="T868">conj</ta>
            <ta e="T870" id="Seg_10588" s="T869">refl</ta>
            <ta e="T871" id="Seg_10589" s="T870">v</ta>
            <ta e="T872" id="Seg_10590" s="T871">pers</ta>
            <ta e="T873" id="Seg_10591" s="T872">adv</ta>
            <ta e="T874" id="Seg_10592" s="T873">n</ta>
            <ta e="T875" id="Seg_10593" s="T874">v</ta>
            <ta e="T876" id="Seg_10594" s="T875">conj</ta>
            <ta e="T877" id="Seg_10595" s="T876">num</ta>
            <ta e="T878" id="Seg_10596" s="T877">n</ta>
            <ta e="T879" id="Seg_10597" s="T878">conj</ta>
            <ta e="T880" id="Seg_10598" s="T879">num</ta>
            <ta e="T881" id="Seg_10599" s="T880">n</ta>
            <ta e="T882" id="Seg_10600" s="T881">conj</ta>
            <ta e="T883" id="Seg_10601" s="T882">n</ta>
            <ta e="T884" id="Seg_10602" s="T883">v</ta>
            <ta e="T885" id="Seg_10603" s="T884">dempro</ta>
            <ta e="T886" id="Seg_10604" s="T885">ptcl</ta>
            <ta e="T887" id="Seg_10605" s="T886">n</ta>
            <ta e="T888" id="Seg_10606" s="T887">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T4" id="Seg_10607" s="T3">pro.h:A</ta>
            <ta e="T5" id="Seg_10608" s="T4">pro.h:R</ta>
            <ta e="T7" id="Seg_10609" s="T6">np:Th</ta>
            <ta e="T8" id="Seg_10610" s="T7">np:Th</ta>
            <ta e="T9" id="Seg_10611" s="T8">0.1.h:A</ta>
            <ta e="T10" id="Seg_10612" s="T9">np:Th</ta>
            <ta e="T11" id="Seg_10613" s="T10">0.1.h:A</ta>
            <ta e="T16" id="Seg_10614" s="T15">pro.h:A</ta>
            <ta e="T17" id="Seg_10615" s="T16">np:Th</ta>
            <ta e="T19" id="Seg_10616" s="T18">pro.h:A</ta>
            <ta e="T23" id="Seg_10617" s="T22">np:Th</ta>
            <ta e="T27" id="Seg_10618" s="T26">pro.h:A</ta>
            <ta e="T30" id="Seg_10619" s="T29">np:Th</ta>
            <ta e="T42" id="Seg_10620" s="T41">pro.h:Poss</ta>
            <ta e="T43" id="Seg_10621" s="T42">np:Th</ta>
            <ta e="T44" id="Seg_10622" s="T43">np.h:A</ta>
            <ta e="T46" id="Seg_10623" s="T45">pro:Th</ta>
            <ta e="T49" id="Seg_10624" s="T48">0.3.h:A</ta>
            <ta e="T51" id="Seg_10625" s="T50">0.3.h:A</ta>
            <ta e="T52" id="Seg_10626" s="T51">np:Th</ta>
            <ta e="T53" id="Seg_10627" s="T52">adv:L</ta>
            <ta e="T55" id="Seg_10628" s="T54">pro.h:Poss</ta>
            <ta e="T56" id="Seg_10629" s="T55">np.h:A</ta>
            <ta e="T58" id="Seg_10630" s="T57">np:G</ta>
            <ta e="T60" id="Seg_10631" s="T59">adv:L</ta>
            <ta e="T61" id="Seg_10632" s="T60">np:Th</ta>
            <ta e="T62" id="Seg_10633" s="T61">0.3.h:A</ta>
            <ta e="T63" id="Seg_10634" s="T62">np:P</ta>
            <ta e="T66" id="Seg_10635" s="T65">np:P</ta>
            <ta e="T67" id="Seg_10636" s="T66">np:P</ta>
            <ta e="T68" id="Seg_10637" s="T67">np:P</ta>
            <ta e="T70" id="Seg_10638" s="T69">pro:Th</ta>
            <ta e="T74" id="Seg_10639" s="T73">np.h:E</ta>
            <ta e="T76" id="Seg_10640" s="T75">np.h:E</ta>
            <ta e="T80" id="Seg_10641" s="T79">0.3.h:E</ta>
            <ta e="T84" id="Seg_10642" s="T83">np.h:E</ta>
            <ta e="T85" id="Seg_10643" s="T84">np.h:B</ta>
            <ta e="T89" id="Seg_10644" s="T88">0.3.h:A</ta>
            <ta e="T91" id="Seg_10645" s="T90">pro.h:Poss</ta>
            <ta e="T92" id="Seg_10646" s="T91">np.h:Th</ta>
            <ta e="T95" id="Seg_10647" s="T94">np.h:Th</ta>
            <ta e="T97" id="Seg_10648" s="T96">pro:Th</ta>
            <ta e="T98" id="Seg_10649" s="T97">0.2.h:A</ta>
            <ta e="T99" id="Seg_10650" s="T98">pro.h:A</ta>
            <ta e="T103" id="Seg_10651" s="T102">pro.h:Poss</ta>
            <ta e="T104" id="Seg_10652" s="T103">np.h:Th</ta>
            <ta e="T108" id="Seg_10653" s="T107">0.3.h:A</ta>
            <ta e="T109" id="Seg_10654" s="T108">0.3.h:A</ta>
            <ta e="T111" id="Seg_10655" s="T110">0.3.h:A</ta>
            <ta e="T112" id="Seg_10656" s="T111">pro.h:A</ta>
            <ta e="T113" id="Seg_10657" s="T112">pro.h:E</ta>
            <ta e="T117" id="Seg_10658" s="T116">np.h:R</ta>
            <ta e="T118" id="Seg_10659" s="T117">0.1.h:A</ta>
            <ta e="T119" id="Seg_10660" s="T118">np:P</ta>
            <ta e="T122" id="Seg_10661" s="T121">np.h:E</ta>
            <ta e="T124" id="Seg_10662" s="T123">pro.h:Poss</ta>
            <ta e="T126" id="Seg_10663" s="T125">np:Th</ta>
            <ta e="T129" id="Seg_10664" s="T128">pro.h:Poss np:Th</ta>
            <ta e="T131" id="Seg_10665" s="T130">pro.h:A</ta>
            <ta e="T132" id="Seg_10666" s="T131">np:P</ta>
            <ta e="T135" id="Seg_10667" s="T134">np:P</ta>
            <ta e="T136" id="Seg_10668" s="T135">0.3.h:A</ta>
            <ta e="T137" id="Seg_10669" s="T136">adv:Time</ta>
            <ta e="T138" id="Seg_10670" s="T137">pro:Th</ta>
            <ta e="T140" id="Seg_10671" s="T139">0.3.h:A</ta>
            <ta e="T141" id="Seg_10672" s="T140">0.3.h:A</ta>
            <ta e="T143" id="Seg_10673" s="T142">0.3.h:A</ta>
            <ta e="T146" id="Seg_10674" s="T145">np:L</ta>
            <ta e="T149" id="Seg_10675" s="T148">np.h:E</ta>
            <ta e="T151" id="Seg_10676" s="T150">adv:Time</ta>
            <ta e="T153" id="Seg_10677" s="T152">np:P</ta>
            <ta e="T155" id="Seg_10678" s="T154">pro.h:A</ta>
            <ta e="T160" id="Seg_10679" s="T159">np.h:A</ta>
            <ta e="T162" id="Seg_10680" s="T161">pro:G</ta>
            <ta e="T165" id="Seg_10681" s="T164">0.3:P</ta>
            <ta e="T166" id="Seg_10682" s="T165">adv:Time</ta>
            <ta e="T167" id="Seg_10683" s="T166">np:P</ta>
            <ta e="T168" id="Seg_10684" s="T167">0.3.h:A</ta>
            <ta e="T169" id="Seg_10685" s="T168">adv:L</ta>
            <ta e="T170" id="Seg_10686" s="T169">np:Th</ta>
            <ta e="T171" id="Seg_10687" s="T170">0.3.h:A</ta>
            <ta e="T173" id="Seg_10688" s="T172">np:Th</ta>
            <ta e="T174" id="Seg_10689" s="T173">0.3.h:A</ta>
            <ta e="T175" id="Seg_10690" s="T174">0.3.h:A</ta>
            <ta e="T177" id="Seg_10691" s="T176">pro:P</ta>
            <ta e="T180" id="Seg_10692" s="T179">pro.h:P</ta>
            <ta e="T182" id="Seg_10693" s="T181">adv:L</ta>
            <ta e="T186" id="Seg_10694" s="T185">pro.h:P</ta>
            <ta e="T196" id="Seg_10695" s="T195">0.3.h:A</ta>
            <ta e="T197" id="Seg_10696" s="T196">np.h:Com</ta>
            <ta e="T198" id="Seg_10697" s="T197">0.3.h:A</ta>
            <ta e="T201" id="Seg_10698" s="T200">np:P</ta>
            <ta e="T202" id="Seg_10699" s="T201">0.3.h:A</ta>
            <ta e="T204" id="Seg_10700" s="T203">np:Ins</ta>
            <ta e="T208" id="Seg_10701" s="T207">pro.h:Th</ta>
            <ta e="T211" id="Seg_10702" s="T210">np.h:Poss</ta>
            <ta e="T213" id="Seg_10703" s="T212">np.h:Th</ta>
            <ta e="T215" id="Seg_10704" s="T214">np.h:Th</ta>
            <ta e="T218" id="Seg_10705" s="T217">np.h:Th</ta>
            <ta e="T221" id="Seg_10706" s="T220">np.h:A</ta>
            <ta e="T222" id="Seg_10707" s="T221">pro.h:E</ta>
            <ta e="T224" id="Seg_10708" s="T223">np:Ins</ta>
            <ta e="T228" id="Seg_10709" s="T227">np:Ins</ta>
            <ta e="T230" id="Seg_10710" s="T229">np:Ins</ta>
            <ta e="T234" id="Seg_10711" s="T233">0.3.h:A</ta>
            <ta e="T236" id="Seg_10712" s="T235">pro.h:A</ta>
            <ta e="T242" id="Seg_10713" s="T241">pro.h:A</ta>
            <ta e="T245" id="Seg_10714" s="T244">0.2.h:A</ta>
            <ta e="T248" id="Seg_10715" s="T247">0.3.h:P</ta>
            <ta e="T251" id="Seg_10716" s="T250">0.3.h:P</ta>
            <ta e="T253" id="Seg_10717" s="T252">0.3.h:P</ta>
            <ta e="T257" id="Seg_10718" s="T256">adv:Time</ta>
            <ta e="T259" id="Seg_10719" s="T258">0.3.h:P</ta>
            <ta e="T262" id="Seg_10720" s="T261">pro.h:Th</ta>
            <ta e="T271" id="Seg_10721" s="T270">pro:Th</ta>
            <ta e="T275" id="Seg_10722" s="T274">pro.h:A</ta>
            <ta e="T276" id="Seg_10723" s="T275">np:G</ta>
            <ta e="T279" id="Seg_10724" s="T278">0.3.h:A</ta>
            <ta e="T280" id="Seg_10725" s="T279">pro:L</ta>
            <ta e="T282" id="Seg_10726" s="T281">0.3.h:A</ta>
            <ta e="T283" id="Seg_10727" s="T282">pro.h:Th</ta>
            <ta e="T285" id="Seg_10728" s="T284">0.3.h:A</ta>
            <ta e="T288" id="Seg_10729" s="T287">np.h:Poss</ta>
            <ta e="T289" id="Seg_10730" s="T288">np.h:Th</ta>
            <ta e="T293" id="Seg_10731" s="T292">pro.h:A</ta>
            <ta e="T295" id="Seg_10732" s="T294">pro.h:R</ta>
            <ta e="T299" id="Seg_10733" s="T298">pro.h:A</ta>
            <ta e="T301" id="Seg_10734" s="T300">pro.h:A</ta>
            <ta e="T303" id="Seg_10735" s="T302">pro.h:R</ta>
            <ta e="T304" id="Seg_10736" s="T303">pro:Th</ta>
            <ta e="T307" id="Seg_10737" s="T306">pro.h:B</ta>
            <ta e="T308" id="Seg_10738" s="T307">pro.h:A</ta>
            <ta e="T315" id="Seg_10739" s="T314">np:P</ta>
            <ta e="T316" id="Seg_10740" s="T315">0.2.h:A</ta>
            <ta e="T318" id="Seg_10741" s="T317">pro.h:A</ta>
            <ta e="T321" id="Seg_10742" s="T319">np:Ins</ta>
            <ta e="T322" id="Seg_10743" s="T321">np.h:E</ta>
            <ta e="T326" id="Seg_10744" s="T325">np.h:A</ta>
            <ta e="T330" id="Seg_10745" s="T329">pro.h:Poss</ta>
            <ta e="T331" id="Seg_10746" s="T330">np.h:Th</ta>
            <ta e="T332" id="Seg_10747" s="T331">np:L</ta>
            <ta e="T336" id="Seg_10748" s="T335">pro.h:Th</ta>
            <ta e="T337" id="Seg_10749" s="T336">0.3.h:A</ta>
            <ta e="T338" id="Seg_10750" s="T337">np:G</ta>
            <ta e="T341" id="Seg_10751" s="T340">np:P</ta>
            <ta e="T347" id="Seg_10752" s="T346">pro.h:E</ta>
            <ta e="T351" id="Seg_10753" s="T350">np:Th</ta>
            <ta e="T355" id="Seg_10754" s="T354">pro.h:B</ta>
            <ta e="T357" id="Seg_10755" s="T356">pro.h:Poss</ta>
            <ta e="T359" id="Seg_10756" s="T358">np.h:Th</ta>
            <ta e="T361" id="Seg_10757" s="T360">np.h:Th</ta>
            <ta e="T364" id="Seg_10758" s="T363">np.h:Th</ta>
            <ta e="T367" id="Seg_10759" s="T366">pro.h:R</ta>
            <ta e="T370" id="Seg_10760" s="T369">np:Th</ta>
            <ta e="T372" id="Seg_10761" s="T371">pro.h:A</ta>
            <ta e="T374" id="Seg_10762" s="T373">pro.h:A</ta>
            <ta e="T377" id="Seg_10763" s="T376">pro.h:R</ta>
            <ta e="T379" id="Seg_10764" s="T378">np:Th</ta>
            <ta e="T381" id="Seg_10765" s="T380">pro.h:A</ta>
            <ta e="T383" id="Seg_10766" s="T382">pro.h:A</ta>
            <ta e="T386" id="Seg_10767" s="T385">pro.h:R</ta>
            <ta e="T388" id="Seg_10768" s="T387">np:Th</ta>
            <ta e="T393" id="Seg_10769" s="T392">0.3.h:A</ta>
            <ta e="T394" id="Seg_10770" s="T393">adv:L</ta>
            <ta e="T395" id="Seg_10771" s="T394">np.h:E</ta>
            <ta e="T397" id="Seg_10772" s="T396">pro.h:E</ta>
            <ta e="T398" id="Seg_10773" s="T397">np:Th</ta>
            <ta e="T400" id="Seg_10774" s="T399">np:Th</ta>
            <ta e="T402" id="Seg_10775" s="T401">adv:Time</ta>
            <ta e="T403" id="Seg_10776" s="T402">0.3.h:A</ta>
            <ta e="T404" id="Seg_10777" s="T403">0.3.h:A</ta>
            <ta e="T406" id="Seg_10778" s="T405">adv:L</ta>
            <ta e="T408" id="Seg_10779" s="T407">np.h:E</ta>
            <ta e="T410" id="Seg_10780" s="T409">np.h:Com</ta>
            <ta e="T411" id="Seg_10781" s="T410">pro.h:A</ta>
            <ta e="T414" id="Seg_10782" s="T413">pro:G</ta>
            <ta e="T415" id="Seg_10783" s="T414">np:Th</ta>
            <ta e="T418" id="Seg_10784" s="T417">pro.h:A</ta>
            <ta e="T422" id="Seg_10785" s="T421">np:P</ta>
            <ta e="T424" id="Seg_10786" s="T423">np.h:R</ta>
            <ta e="T428" id="Seg_10787" s="T427">pro.h:A</ta>
            <ta e="T430" id="Seg_10788" s="T429">0.2.h:A</ta>
            <ta e="T432" id="Seg_10789" s="T431">pro.h:Poss</ta>
            <ta e="T433" id="Seg_10790" s="T432">np:Th</ta>
            <ta e="T435" id="Seg_10791" s="T434">adv:Time</ta>
            <ta e="T436" id="Seg_10792" s="T435">np:Th</ta>
            <ta e="T437" id="Seg_10793" s="T436">0.3.h:A</ta>
            <ta e="T438" id="Seg_10794" s="T437">np:Th</ta>
            <ta e="T439" id="Seg_10795" s="T438">0.3.h:A</ta>
            <ta e="T440" id="Seg_10796" s="T439">pro.h:A</ta>
            <ta e="T444" id="Seg_10797" s="T443">np:E</ta>
            <ta e="T445" id="Seg_10798" s="T444">0.3.h:A</ta>
            <ta e="T447" id="Seg_10799" s="T446">np:Ins</ta>
            <ta e="T449" id="Seg_10800" s="T447">np:Th</ta>
            <ta e="T450" id="Seg_10801" s="T449">adv:Time</ta>
            <ta e="T451" id="Seg_10802" s="T450">np:G</ta>
            <ta e="T452" id="Seg_10803" s="T451">0.3.h:A</ta>
            <ta e="T453" id="Seg_10804" s="T452">0.3.h:Th</ta>
            <ta e="T454" id="Seg_10805" s="T453">0.3.h:Th</ta>
            <ta e="T455" id="Seg_10806" s="T454">adv:Time</ta>
            <ta e="T456" id="Seg_10807" s="T455">np:Th</ta>
            <ta e="T460" id="Seg_10808" s="T459">pro.h:E</ta>
            <ta e="T467" id="Seg_10809" s="T466">pro.h:A</ta>
            <ta e="T471" id="Seg_10810" s="T470">adv:Time</ta>
            <ta e="T472" id="Seg_10811" s="T471">np:P</ta>
            <ta e="T473" id="Seg_10812" s="T472">0.1.h:A</ta>
            <ta e="T474" id="Seg_10813" s="T473">pro.h:R</ta>
            <ta e="T475" id="Seg_10814" s="T474">np:Th</ta>
            <ta e="T476" id="Seg_10815" s="T475">0.1.h:A</ta>
            <ta e="T477" id="Seg_10816" s="T476">pro.h:B</ta>
            <ta e="T480" id="Seg_10817" s="T479">np:Th</ta>
            <ta e="T481" id="Seg_10818" s="T480">adv:Time</ta>
            <ta e="T482" id="Seg_10819" s="T481">pro.h:R</ta>
            <ta e="T483" id="Seg_10820" s="T482">np:Th</ta>
            <ta e="T484" id="Seg_10821" s="T483">0.3.h:A</ta>
            <ta e="T486" id="Seg_10822" s="T485">adv:Time</ta>
            <ta e="T488" id="Seg_10823" s="T487">np.h:A</ta>
            <ta e="T491" id="Seg_10824" s="T490">0.3.h:A</ta>
            <ta e="T492" id="Seg_10825" s="T491">0.3.h:A</ta>
            <ta e="T493" id="Seg_10826" s="T492">np:E</ta>
            <ta e="T494" id="Seg_10827" s="T493">np:Th</ta>
            <ta e="T495" id="Seg_10828" s="T494">0.3.h:A</ta>
            <ta e="T496" id="Seg_10829" s="T495">0.3.h:A</ta>
            <ta e="T497" id="Seg_10830" s="T496">adv:Time</ta>
            <ta e="T500" id="Seg_10831" s="T499">np:G</ta>
            <ta e="T501" id="Seg_10832" s="T500">0.3.h:A</ta>
            <ta e="T502" id="Seg_10833" s="T501">adv:L</ta>
            <ta e="T505" id="Seg_10834" s="T504">np:Th</ta>
            <ta e="T506" id="Seg_10835" s="T505">np.h:Th</ta>
            <ta e="T509" id="Seg_10836" s="T508">np.h:Poss</ta>
            <ta e="T510" id="Seg_10837" s="T509">np.h:Th</ta>
            <ta e="T512" id="Seg_10838" s="T511">adv:Time</ta>
            <ta e="T513" id="Seg_10839" s="T512">0.3.h:A</ta>
            <ta e="T514" id="Seg_10840" s="T513">0.2.h:A</ta>
            <ta e="T515" id="Seg_10841" s="T514">pro.h:Poss</ta>
            <ta e="T516" id="Seg_10842" s="T515">np:Th</ta>
            <ta e="T517" id="Seg_10843" s="T516">pro.h:Poss</ta>
            <ta e="T518" id="Seg_10844" s="T517">np:Th</ta>
            <ta e="T521" id="Seg_10845" s="T520">0.1.h:A</ta>
            <ta e="T522" id="Seg_10846" s="T521">pro.h:Poss</ta>
            <ta e="T523" id="Seg_10847" s="T522">np:A</ta>
            <ta e="T524" id="Seg_10848" s="T523">pro.h:Poss</ta>
            <ta e="T525" id="Seg_10849" s="T524">np:P</ta>
            <ta e="T528" id="Seg_10850" s="T527">pro.h:A</ta>
            <ta e="T531" id="Seg_10851" s="T530">0.3.h:A</ta>
            <ta e="T532" id="Seg_10852" s="T531">pro.h:A</ta>
            <ta e="T536" id="Seg_10853" s="T535">np:Th</ta>
            <ta e="T537" id="Seg_10854" s="T536">0.3.h:A</ta>
            <ta e="T538" id="Seg_10855" s="T537">pro:G</ta>
            <ta e="T539" id="Seg_10856" s="T538">0.3.h:Poss</ta>
            <ta e="T541" id="Seg_10857" s="T540">adv:Time</ta>
            <ta e="T542" id="Seg_10858" s="T541">0.3.h:A</ta>
            <ta e="T544" id="Seg_10859" s="T543">n:Time</ta>
            <ta e="T545" id="Seg_10860" s="T544">0.3.h:A</ta>
            <ta e="T547" id="Seg_10861" s="T546">np:Th</ta>
            <ta e="T551" id="Seg_10862" s="T550">np.h:A</ta>
            <ta e="T553" id="Seg_10863" s="T552">np:Th</ta>
            <ta e="T554" id="Seg_10864" s="T553">0.3.h:A</ta>
            <ta e="T556" id="Seg_10865" s="T555">pro.h:A</ta>
            <ta e="T557" id="Seg_10866" s="T556">pro:Poss</ta>
            <ta e="T558" id="Seg_10867" s="T557">np:Poss</ta>
            <ta e="T562" id="Seg_10868" s="T561">np:Ins</ta>
            <ta e="T565" id="Seg_10869" s="T564">np:Th</ta>
            <ta e="T567" id="Seg_10870" s="T566">np:Th</ta>
            <ta e="T569" id="Seg_10871" s="T568">n:Time</ta>
            <ta e="T570" id="Seg_10872" s="T569">0.3.h:A</ta>
            <ta e="T571" id="Seg_10873" s="T570">0.3.h:A</ta>
            <ta e="T574" id="Seg_10874" s="T573">pro.h:Poss</ta>
            <ta e="T575" id="Seg_10875" s="T574">np:A</ta>
            <ta e="T577" id="Seg_10876" s="T576">pro.h:Poss</ta>
            <ta e="T578" id="Seg_10877" s="T577">np:P</ta>
            <ta e="T582" id="Seg_10878" s="T581">adv:Time</ta>
            <ta e="T583" id="Seg_10879" s="T582">pro.h:A</ta>
            <ta e="T585" id="Seg_10880" s="T584">np:P</ta>
            <ta e="T586" id="Seg_10881" s="T585">0.3.h:A</ta>
            <ta e="T587" id="Seg_10882" s="T586">0.3.h:A</ta>
            <ta e="T589" id="Seg_10883" s="T588">np:Th</ta>
            <ta e="T590" id="Seg_10884" s="T589">0.3.h:A</ta>
            <ta e="T592" id="Seg_10885" s="T591">0.3.h:A</ta>
            <ta e="T595" id="Seg_10886" s="T594">0.3.h:A</ta>
            <ta e="T596" id="Seg_10887" s="T595">adv:Time</ta>
            <ta e="T597" id="Seg_10888" s="T596">0.3.h:A</ta>
            <ta e="T598" id="Seg_10889" s="T597">0.3.h:A</ta>
            <ta e="T599" id="Seg_10890" s="T598">adv:L</ta>
            <ta e="T601" id="Seg_10891" s="T600">np:Th</ta>
            <ta e="T603" id="Seg_10892" s="T602">pro.h:A</ta>
            <ta e="T605" id="Seg_10893" s="T604">adv:L</ta>
            <ta e="T606" id="Seg_10894" s="T605">np.h:Th</ta>
            <ta e="T609" id="Seg_10895" s="T608">np.h:Th</ta>
            <ta e="T613" id="Seg_10896" s="T612">np:Th</ta>
            <ta e="T615" id="Seg_10897" s="T614">pro.h:A</ta>
            <ta e="T616" id="Seg_10898" s="T615">np:Th</ta>
            <ta e="T618" id="Seg_10899" s="T617">np.h:Poss</ta>
            <ta e="T619" id="Seg_10900" s="T618">np:G</ta>
            <ta e="T622" id="Seg_10901" s="T621">np:G</ta>
            <ta e="T623" id="Seg_10902" s="T622">np:Th</ta>
            <ta e="T625" id="Seg_10903" s="T624">0.3.h:A</ta>
            <ta e="T626" id="Seg_10904" s="T625">adv:Time</ta>
            <ta e="T627" id="Seg_10905" s="T626">0.3.h:A</ta>
            <ta e="T629" id="Seg_10906" s="T628">adv:L</ta>
            <ta e="T631" id="Seg_10907" s="T630">np.h:Th</ta>
            <ta e="T633" id="Seg_10908" s="T632">np.h:Th</ta>
            <ta e="T635" id="Seg_10909" s="T634">0.3.h:A</ta>
            <ta e="T636" id="Seg_10910" s="T635">0.3.h:A</ta>
            <ta e="T637" id="Seg_10911" s="T636">pro.h:Poss</ta>
            <ta e="T638" id="Seg_10912" s="T637">adv:L</ta>
            <ta e="T639" id="Seg_10913" s="T638">np.h:Th</ta>
            <ta e="T641" id="Seg_10914" s="T640">np:Com</ta>
            <ta e="T645" id="Seg_10915" s="T644">np:L</ta>
            <ta e="T646" id="Seg_10916" s="T645">np:Th</ta>
            <ta e="T649" id="Seg_10917" s="T648">np.h:A</ta>
            <ta e="T650" id="Seg_10918" s="T649">pro.h:A</ta>
            <ta e="T652" id="Seg_10919" s="T651">0.1.h:A</ta>
            <ta e="T653" id="Seg_10920" s="T652">np.h:Th</ta>
            <ta e="T654" id="Seg_10921" s="T653">pro.h:Poss</ta>
            <ta e="T655" id="Seg_10922" s="T654">pro.h:E</ta>
            <ta e="T659" id="Seg_10923" s="T658">0.3.h:A</ta>
            <ta e="T661" id="Seg_10924" s="T660">pro.h:E</ta>
            <ta e="T663" id="Seg_10925" s="T662">adv:Time</ta>
            <ta e="T664" id="Seg_10926" s="T663">pro.h:Poss</ta>
            <ta e="T665" id="Seg_10927" s="T664">np.h:A</ta>
            <ta e="T667" id="Seg_10928" s="T666">0.3.h:A</ta>
            <ta e="T668" id="Seg_10929" s="T667">0.3.h:A</ta>
            <ta e="T669" id="Seg_10930" s="T668">np:A</ta>
            <ta e="T674" id="Seg_10931" s="T673">adv:Time</ta>
            <ta e="T675" id="Seg_10932" s="T674">pro.h:A</ta>
            <ta e="T679" id="Seg_10933" s="T678">0.3.h:A</ta>
            <ta e="T680" id="Seg_10934" s="T679">0.3.h:A</ta>
            <ta e="T682" id="Seg_10935" s="T681">np.h:A</ta>
            <ta e="T684" id="Seg_10936" s="T683">0.2.h:A</ta>
            <ta e="T686" id="Seg_10937" s="T685">np.h:Th</ta>
            <ta e="T688" id="Seg_10938" s="T687">pro.h:B</ta>
            <ta e="T690" id="Seg_10939" s="T689">pro.h:Poss</ta>
            <ta e="T691" id="Seg_10940" s="T690">np.h:Th</ta>
            <ta e="T695" id="Seg_10941" s="T694">0.2.h:A</ta>
            <ta e="T696" id="Seg_10942" s="T695">np.h:Th</ta>
            <ta e="T697" id="Seg_10943" s="T696">adv:Time</ta>
            <ta e="T698" id="Seg_10944" s="T697">pro.h:A</ta>
            <ta e="T701" id="Seg_10945" s="T700">0.3.h:A</ta>
            <ta e="T702" id="Seg_10946" s="T701">np:P</ta>
            <ta e="T705" id="Seg_10947" s="T704">np.h:Th</ta>
            <ta e="T706" id="Seg_10948" s="T705">0.3.h:A</ta>
            <ta e="T708" id="Seg_10949" s="T707">0.3.h:A</ta>
            <ta e="T709" id="Seg_10950" s="T708">adv:Time</ta>
            <ta e="T710" id="Seg_10951" s="T709">0.3.h:A</ta>
            <ta e="T711" id="Seg_10952" s="T710">0.3.h:A</ta>
            <ta e="T712" id="Seg_10953" s="T711">adv:L</ta>
            <ta e="T713" id="Seg_10954" s="T712">np:A</ta>
            <ta e="T715" id="Seg_10955" s="T714">adv:Time</ta>
            <ta e="T716" id="Seg_10956" s="T715">0.3.h:A</ta>
            <ta e="T717" id="Seg_10957" s="T716">np:G</ta>
            <ta e="T718" id="Seg_10958" s="T717">adv:L</ta>
            <ta e="T719" id="Seg_10959" s="T718">pro.h:Poss</ta>
            <ta e="T720" id="Seg_10960" s="T719">np:Th</ta>
            <ta e="T725" id="Seg_10961" s="T724">0.3.h:A</ta>
            <ta e="T726" id="Seg_10962" s="T725">np:G</ta>
            <ta e="T728" id="Seg_10963" s="T727">np:G</ta>
            <ta e="T729" id="Seg_10964" s="T728">np.h:E</ta>
            <ta e="T731" id="Seg_10965" s="T730">np:L</ta>
            <ta e="T732" id="Seg_10966" s="T731">adv:Time</ta>
            <ta e="T733" id="Seg_10967" s="T732">0.3.h:A</ta>
            <ta e="T734" id="Seg_10968" s="T733">pro.h:Poss</ta>
            <ta e="T737" id="Seg_10969" s="T736">0.2.h:A</ta>
            <ta e="T740" id="Seg_10970" s="T739">0.2.h:A</ta>
            <ta e="T741" id="Seg_10971" s="T740">pro.h:A</ta>
            <ta e="T743" id="Seg_10972" s="T742">np.h:Poss</ta>
            <ta e="T745" id="Seg_10973" s="T744">adv:Time</ta>
            <ta e="T746" id="Seg_10974" s="T745">np:L</ta>
            <ta e="T747" id="Seg_10975" s="T746">np.h:R</ta>
            <ta e="T748" id="Seg_10976" s="T747">0.2.h:A</ta>
            <ta e="T749" id="Seg_10977" s="T748">0.3.h:A</ta>
            <ta e="T750" id="Seg_10978" s="T749">pro.h:A</ta>
            <ta e="T752" id="Seg_10979" s="T751">np.h:Poss</ta>
            <ta e="T754" id="Seg_10980" s="T753">adv:Time</ta>
            <ta e="T756" id="Seg_10981" s="T755">0.3.h:A</ta>
            <ta e="T757" id="Seg_10982" s="T756">np:Th</ta>
            <ta e="T761" id="Seg_10983" s="T760">0.3.h:A</ta>
            <ta e="T762" id="Seg_10984" s="T761">np.h:E</ta>
            <ta e="T763" id="Seg_10985" s="T762">np:E</ta>
            <ta e="T766" id="Seg_10986" s="T765">pro.h:Poss</ta>
            <ta e="T769" id="Seg_10987" s="T768">pro.h:A</ta>
            <ta e="T772" id="Seg_10988" s="T771">pro.h:Poss</ta>
            <ta e="T775" id="Seg_10989" s="T774">0.2.h:A</ta>
            <ta e="T777" id="Seg_10990" s="T776">np.h:R</ta>
            <ta e="T778" id="Seg_10991" s="T777">pro.h:A</ta>
            <ta e="T780" id="Seg_10992" s="T779">np.h:Poss</ta>
            <ta e="T782" id="Seg_10993" s="T781">adv:Time</ta>
            <ta e="T783" id="Seg_10994" s="T782">np:Poss</ta>
            <ta e="T784" id="Seg_10995" s="T783">np.h:R</ta>
            <ta e="T785" id="Seg_10996" s="T784">0.2.h:A</ta>
            <ta e="T786" id="Seg_10997" s="T785">pro.h:A</ta>
            <ta e="T788" id="Seg_10998" s="T787">np.h:Poss</ta>
            <ta e="T790" id="Seg_10999" s="T789">pro.h:A</ta>
            <ta e="T793" id="Seg_11000" s="T792">adv:Time</ta>
            <ta e="T794" id="Seg_11001" s="T793">np:A</ta>
            <ta e="T797" id="Seg_11002" s="T796">adv:L</ta>
            <ta e="T799" id="Seg_11003" s="T798">np.h:A</ta>
            <ta e="T801" id="Seg_11004" s="T800">np:Th</ta>
            <ta e="T802" id="Seg_11005" s="T801">pro.h:A</ta>
            <ta e="T804" id="Seg_11006" s="T803">pro.h:Poss</ta>
            <ta e="T808" id="Seg_11007" s="T807">pro.h:Poss</ta>
            <ta e="T810" id="Seg_11008" s="T809">0.2.h:A</ta>
            <ta e="T811" id="Seg_11009" s="T810">np:L</ta>
            <ta e="T812" id="Seg_11010" s="T811">np.h:R</ta>
            <ta e="T813" id="Seg_11011" s="T812">adv:Time</ta>
            <ta e="T814" id="Seg_11012" s="T813">np:L</ta>
            <ta e="T815" id="Seg_11013" s="T814">pro.h:A</ta>
            <ta e="T817" id="Seg_11014" s="T816">pro.h:Poss</ta>
            <ta e="T820" id="Seg_11015" s="T819">pro.h:A</ta>
            <ta e="T822" id="Seg_11016" s="T821">np.h:Poss</ta>
            <ta e="T825" id="Seg_11017" s="T824">np:L</ta>
            <ta e="T826" id="Seg_11018" s="T825">0.3.h:A</ta>
            <ta e="T827" id="Seg_11019" s="T826">pro.h:Poss</ta>
            <ta e="T829" id="Seg_11020" s="T828">np.h:Poss</ta>
            <ta e="T831" id="Seg_11021" s="T830">adv:Time</ta>
            <ta e="T832" id="Seg_11022" s="T831">pro.h:A</ta>
            <ta e="T835" id="Seg_11023" s="T834">np:Th</ta>
            <ta e="T836" id="Seg_11024" s="T835">pro.h:A</ta>
            <ta e="T837" id="Seg_11025" s="T836">np:Th</ta>
            <ta e="T842" id="Seg_11026" s="T841">np:Th</ta>
            <ta e="T843" id="Seg_11027" s="T842">0.1.h:A</ta>
            <ta e="T846" id="Seg_11028" s="T845">np:Th</ta>
            <ta e="T847" id="Seg_11029" s="T846">0.1.h:A</ta>
            <ta e="T849" id="Seg_11030" s="T848">np:Th</ta>
            <ta e="T850" id="Seg_11031" s="T849">0.1.h:A</ta>
            <ta e="T851" id="Seg_11032" s="T850">0.1.h:A</ta>
            <ta e="T852" id="Seg_11033" s="T851">np.h:Th</ta>
            <ta e="T853" id="Seg_11034" s="T852">np:So</ta>
            <ta e="T854" id="Seg_11035" s="T853">0.1.h:A</ta>
            <ta e="T865" id="Seg_11036" s="T864">np:Th</ta>
            <ta e="T866" id="Seg_11037" s="T865">np:G</ta>
            <ta e="T868" id="Seg_11038" s="T867">0.3.h:A</ta>
            <ta e="T870" id="Seg_11039" s="T869">pro.h:A</ta>
            <ta e="T872" id="Seg_11040" s="T871">pro.h:Poss</ta>
            <ta e="T873" id="Seg_11041" s="T872">adv:L</ta>
            <ta e="T874" id="Seg_11042" s="T873">np.h:Th</ta>
            <ta e="T878" id="Seg_11043" s="T877">np:Th</ta>
            <ta e="T881" id="Seg_11044" s="T880">np.h:E</ta>
            <ta e="T883" id="Seg_11045" s="T882">np.h:E</ta>
            <ta e="T885" id="Seg_11046" s="T884">pro.h:A</ta>
            <ta e="T887" id="Seg_11047" s="T886">np.h:R</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_11048" s="T3">pro.h:S</ta>
            <ta e="T6" id="Seg_11049" s="T5">v:pred</ta>
            <ta e="T7" id="Seg_11050" s="T6">np:O</ta>
            <ta e="T8" id="Seg_11051" s="T7">np:O</ta>
            <ta e="T9" id="Seg_11052" s="T8">v:pred 0.1.h:S</ta>
            <ta e="T10" id="Seg_11053" s="T9">np:O</ta>
            <ta e="T11" id="Seg_11054" s="T10">v:pred 0.1.h:S</ta>
            <ta e="T16" id="Seg_11055" s="T15">pro.h:S</ta>
            <ta e="T17" id="Seg_11056" s="T16">np:O</ta>
            <ta e="T18" id="Seg_11057" s="T17">v:pred</ta>
            <ta e="T19" id="Seg_11058" s="T18">pro.h:S</ta>
            <ta e="T21" id="Seg_11059" s="T20">v:pred</ta>
            <ta e="T23" id="Seg_11060" s="T22">np:S</ta>
            <ta e="T24" id="Seg_11061" s="T23">ptcl.neg</ta>
            <ta e="T25" id="Seg_11062" s="T24">v:pred</ta>
            <ta e="T27" id="Seg_11063" s="T26">pro.h:S</ta>
            <ta e="T30" id="Seg_11064" s="T29">np:O</ta>
            <ta e="T31" id="Seg_11065" s="T30">conv:pred</ta>
            <ta e="T32" id="Seg_11066" s="T31">v:pred</ta>
            <ta e="T43" id="Seg_11067" s="T42">np:O</ta>
            <ta e="T44" id="Seg_11068" s="T43">np.h:S</ta>
            <ta e="T45" id="Seg_11069" s="T44">v:pred</ta>
            <ta e="T46" id="Seg_11070" s="T45">pro:S</ta>
            <ta e="T47" id="Seg_11071" s="T46">v:pred</ta>
            <ta e="T49" id="Seg_11072" s="T48">v:pred 0.3.h:S</ta>
            <ta e="T51" id="Seg_11073" s="T50">v:pred 0.3.h:S</ta>
            <ta e="T52" id="Seg_11074" s="T51">np:O</ta>
            <ta e="T56" id="Seg_11075" s="T55">np.h:S</ta>
            <ta e="T892" id="Seg_11076" s="T56">conv:pred</ta>
            <ta e="T57" id="Seg_11077" s="T892">v:pred</ta>
            <ta e="T61" id="Seg_11078" s="T60">np:O</ta>
            <ta e="T62" id="Seg_11079" s="T61">v:pred 0.3.h:S</ta>
            <ta e="T63" id="Seg_11080" s="T62">np:O</ta>
            <ta e="T64" id="Seg_11081" s="T63">s:purp</ta>
            <ta e="T68" id="Seg_11082" s="T67">np:O</ta>
            <ta e="T70" id="Seg_11083" s="T69">pro:S</ta>
            <ta e="T71" id="Seg_11084" s="T70">v:pred</ta>
            <ta e="T72" id="Seg_11085" s="T71">s:purp</ta>
            <ta e="T74" id="Seg_11086" s="T73">np.h:S</ta>
            <ta e="T76" id="Seg_11087" s="T75">np.h:S</ta>
            <ta e="T77" id="Seg_11088" s="T76">v:pred</ta>
            <ta e="T80" id="Seg_11089" s="T79">v:pred 0.3.h:S</ta>
            <ta e="T84" id="Seg_11090" s="T83">np.h:S</ta>
            <ta e="T86" id="Seg_11091" s="T85">v:pred</ta>
            <ta e="T89" id="Seg_11092" s="T88">v:pred 0.3.h:S</ta>
            <ta e="T92" id="Seg_11093" s="T91">np.h:S</ta>
            <ta e="T93" id="Seg_11094" s="T92">v:pred</ta>
            <ta e="T95" id="Seg_11095" s="T94">np.h:S</ta>
            <ta e="T96" id="Seg_11096" s="T95">adj:pred</ta>
            <ta e="T97" id="Seg_11097" s="T96">pro:O</ta>
            <ta e="T98" id="Seg_11098" s="T97">v:pred 0.2.h:S</ta>
            <ta e="T99" id="Seg_11099" s="T98">pro.h:S</ta>
            <ta e="T100" id="Seg_11100" s="T99">v:pred</ta>
            <ta e="T104" id="Seg_11101" s="T103">np.h:S</ta>
            <ta e="T106" id="Seg_11102" s="T105">ptcl.neg</ta>
            <ta e="T107" id="Seg_11103" s="T106">adj:pred</ta>
            <ta e="T108" id="Seg_11104" s="T107">v:pred 0.3.h:S</ta>
            <ta e="T109" id="Seg_11105" s="T108">v:pred 0.3.h:S</ta>
            <ta e="T110" id="Seg_11106" s="T109">ptcl.neg</ta>
            <ta e="T111" id="Seg_11107" s="T110">v:pred 0.3.h:S</ta>
            <ta e="T112" id="Seg_11108" s="T111">pro.h:S</ta>
            <ta e="T113" id="Seg_11109" s="T112">pro.h:O</ta>
            <ta e="T115" id="Seg_11110" s="T114">v:pred</ta>
            <ta e="T117" id="Seg_11111" s="T116">np.h:O</ta>
            <ta e="T118" id="Seg_11112" s="T117">v:pred 0.1.h:S</ta>
            <ta e="T119" id="Seg_11113" s="T118">np:O</ta>
            <ta e="T122" id="Seg_11114" s="T121">np.h:S</ta>
            <ta e="T123" id="Seg_11115" s="T122">v:pred</ta>
            <ta e="T126" id="Seg_11116" s="T125">np:S</ta>
            <ta e="T127" id="Seg_11117" s="T126">adj:pred</ta>
            <ta e="T129" id="Seg_11118" s="T128">np:S</ta>
            <ta e="T130" id="Seg_11119" s="T129">v:pred</ta>
            <ta e="T131" id="Seg_11120" s="T130">pro.h:S</ta>
            <ta e="T132" id="Seg_11121" s="T131">np:O</ta>
            <ta e="T133" id="Seg_11122" s="T132">v:pred</ta>
            <ta e="T135" id="Seg_11123" s="T134">np:O</ta>
            <ta e="T136" id="Seg_11124" s="T135">v:pred 0.3.h:S</ta>
            <ta e="T138" id="Seg_11125" s="T137">pro:S</ta>
            <ta e="T139" id="Seg_11126" s="T138">v:pred</ta>
            <ta e="T140" id="Seg_11127" s="T139">v:pred 0.3.h:S</ta>
            <ta e="T141" id="Seg_11128" s="T140">v:pred 0.3.h:S</ta>
            <ta e="T143" id="Seg_11129" s="T142">v:pred 0.3.h:S</ta>
            <ta e="T149" id="Seg_11130" s="T148">np.h:S</ta>
            <ta e="T150" id="Seg_11131" s="T149">v:pred</ta>
            <ta e="T153" id="Seg_11132" s="T152">np:S</ta>
            <ta e="T154" id="Seg_11133" s="T153">v:pred</ta>
            <ta e="T155" id="Seg_11134" s="T154">pro.h:S</ta>
            <ta e="T156" id="Seg_11135" s="T155">v:pred</ta>
            <ta e="T160" id="Seg_11136" s="T159">np.h:S</ta>
            <ta e="T161" id="Seg_11137" s="T160">v:pred</ta>
            <ta e="T163" id="Seg_11138" s="T162">ptcl.neg</ta>
            <ta e="T164" id="Seg_11139" s="T163">adj:pred</ta>
            <ta e="T165" id="Seg_11140" s="T164">cop 0.3:S</ta>
            <ta e="T167" id="Seg_11141" s="T166">np:O</ta>
            <ta e="T168" id="Seg_11142" s="T167">v:pred 0.3.h:S</ta>
            <ta e="T170" id="Seg_11143" s="T169">np:O</ta>
            <ta e="T171" id="Seg_11144" s="T170">v:pred 0.3.h:S</ta>
            <ta e="T173" id="Seg_11145" s="T172">np:O</ta>
            <ta e="T174" id="Seg_11146" s="T173">v:pred 0.3.h:S</ta>
            <ta e="T175" id="Seg_11147" s="T174">v:pred 0.3.h:S</ta>
            <ta e="T177" id="Seg_11148" s="T176">pro:S</ta>
            <ta e="T178" id="Seg_11149" s="T177">v:pred</ta>
            <ta e="T180" id="Seg_11150" s="T179">pro.h:S</ta>
            <ta e="T183" id="Seg_11151" s="T182">v:pred</ta>
            <ta e="T186" id="Seg_11152" s="T185">pro.h:S</ta>
            <ta e="T188" id="Seg_11153" s="T187">v:pred</ta>
            <ta e="T193" id="Seg_11154" s="T192">ptcl.neg</ta>
            <ta e="T194" id="Seg_11155" s="T193">adj:pred</ta>
            <ta e="T196" id="Seg_11156" s="T195">v:pred 0.3.h:S</ta>
            <ta e="T198" id="Seg_11157" s="T197">v:pred 0.3.h:S</ta>
            <ta e="T201" id="Seg_11158" s="T200">np:O</ta>
            <ta e="T202" id="Seg_11159" s="T201">v:pred 0.3.h:S</ta>
            <ta e="T205" id="Seg_11160" s="T204">ptcl.neg</ta>
            <ta e="T206" id="Seg_11161" s="T205">v:pred 0.3.h:S</ta>
            <ta e="T213" id="Seg_11162" s="T212">np.h:S</ta>
            <ta e="T214" id="Seg_11163" s="T213">v:pred</ta>
            <ta e="T215" id="Seg_11164" s="T214">np.h:S</ta>
            <ta e="T216" id="Seg_11165" s="T215">adj:pred</ta>
            <ta e="T218" id="Seg_11166" s="T217">np.h:S</ta>
            <ta e="T219" id="Seg_11167" s="T218">ptcl.neg</ta>
            <ta e="T220" id="Seg_11168" s="T219">adj:pred</ta>
            <ta e="T221" id="Seg_11169" s="T220">np.h:S</ta>
            <ta e="T222" id="Seg_11170" s="T221">pro.h:O</ta>
            <ta e="T223" id="Seg_11171" s="T222">v:pred</ta>
            <ta e="T234" id="Seg_11172" s="T233">v:pred 0.3.h:S</ta>
            <ta e="T236" id="Seg_11173" s="T235">pro.h:S</ta>
            <ta e="T239" id="Seg_11174" s="T237">v:pred</ta>
            <ta e="T242" id="Seg_11175" s="T241">pro.h:S</ta>
            <ta e="T244" id="Seg_11176" s="T243">v:pred</ta>
            <ta e="T245" id="Seg_11177" s="T244">v:pred 0.2.h:S</ta>
            <ta e="T248" id="Seg_11178" s="T247">v:pred 0.3.h:S</ta>
            <ta e="T249" id="Seg_11179" s="T248">adj:pred</ta>
            <ta e="T251" id="Seg_11180" s="T250">cop 0.3.h:S</ta>
            <ta e="T252" id="Seg_11181" s="T251">adj:pred</ta>
            <ta e="T253" id="Seg_11182" s="T252">cop 0.3.h:S</ta>
            <ta e="T258" id="Seg_11183" s="T257">adj:pred</ta>
            <ta e="T259" id="Seg_11184" s="T258">cop 0.3.h:S</ta>
            <ta e="T262" id="Seg_11185" s="T261">pro.h:S</ta>
            <ta e="T264" id="Seg_11186" s="T263">adj:pred</ta>
            <ta e="T265" id="Seg_11187" s="T264">cop</ta>
            <ta e="T267" id="Seg_11188" s="T266">adj:pred</ta>
            <ta e="T271" id="Seg_11189" s="T270">pro:S</ta>
            <ta e="T272" id="Seg_11190" s="T271">v:pred</ta>
            <ta e="T273" id="Seg_11191" s="T272">adj:pred</ta>
            <ta e="T275" id="Seg_11192" s="T274">pro.h:S</ta>
            <ta e="T277" id="Seg_11193" s="T276">v:pred</ta>
            <ta e="T279" id="Seg_11194" s="T278">v:pred 0.3.h:S</ta>
            <ta e="T281" id="Seg_11195" s="T280">s:purp</ta>
            <ta e="T282" id="Seg_11196" s="T281">v:pred 0.3.h:S</ta>
            <ta e="T283" id="Seg_11197" s="T282">pro.h:O</ta>
            <ta e="T285" id="Seg_11198" s="T284">v:pred 0.3.h:S</ta>
            <ta e="T289" id="Seg_11199" s="T288">np.h:S</ta>
            <ta e="T290" id="Seg_11200" s="T289">cop</ta>
            <ta e="T292" id="Seg_11201" s="T291">adj:pred</ta>
            <ta e="T293" id="Seg_11202" s="T292">pro.h:S</ta>
            <ta e="T296" id="Seg_11203" s="T295">ptcl.neg</ta>
            <ta e="T297" id="Seg_11204" s="T296">v:pred</ta>
            <ta e="T299" id="Seg_11205" s="T298">pro.h:S</ta>
            <ta e="T300" id="Seg_11206" s="T299">v:pred</ta>
            <ta e="T301" id="Seg_11207" s="T300">pro.h:S</ta>
            <ta e="T302" id="Seg_11208" s="T301">v:pred</ta>
            <ta e="T303" id="Seg_11209" s="T302">pro.h:O</ta>
            <ta e="T304" id="Seg_11210" s="T303">pro:S</ta>
            <ta e="T305" id="Seg_11211" s="T304">adj:pred</ta>
            <ta e="T308" id="Seg_11212" s="T307">pro.h:S</ta>
            <ta e="T310" id="Seg_11213" s="T309">v:pred</ta>
            <ta e="T315" id="Seg_11214" s="T314">np:O</ta>
            <ta e="T316" id="Seg_11215" s="T315">v:pred 0.2.h:S</ta>
            <ta e="T318" id="Seg_11216" s="T317">pro.h:S</ta>
            <ta e="T322" id="Seg_11217" s="T321">np.h:O</ta>
            <ta e="T323" id="Seg_11218" s="T322">ptcl:pred</ta>
            <ta e="T326" id="Seg_11219" s="T325">np.h:S</ta>
            <ta e="T327" id="Seg_11220" s="T326">ptcl.neg</ta>
            <ta e="T328" id="Seg_11221" s="T327">v:pred</ta>
            <ta e="T331" id="Seg_11222" s="T330">np.h:S</ta>
            <ta e="T334" id="Seg_11223" s="T333">v:pred</ta>
            <ta e="T336" id="Seg_11224" s="T335">pro.h:O</ta>
            <ta e="T337" id="Seg_11225" s="T336">v:pred 0.3.h:S</ta>
            <ta e="T341" id="Seg_11226" s="T340">np:O</ta>
            <ta e="T345" id="Seg_11227" s="T343">v:pred</ta>
            <ta e="T347" id="Seg_11228" s="T346">pro.h:S</ta>
            <ta e="T349" id="Seg_11229" s="T348">v:pred</ta>
            <ta e="T351" id="Seg_11230" s="T350">np:S</ta>
            <ta e="T353" id="Seg_11231" s="T352">v:pred</ta>
            <ta e="T359" id="Seg_11232" s="T358">np.h:S</ta>
            <ta e="T361" id="Seg_11233" s="T360">n:pred</ta>
            <ta e="T364" id="Seg_11234" s="T363">n:pred</ta>
            <ta e="T366" id="Seg_11235" s="T365">v:pred</ta>
            <ta e="T370" id="Seg_11236" s="T369">np:O</ta>
            <ta e="T372" id="Seg_11237" s="T371">pro.h:S</ta>
            <ta e="T373" id="Seg_11238" s="T372">v:pred</ta>
            <ta e="T374" id="Seg_11239" s="T373">pro.h:S</ta>
            <ta e="T375" id="Seg_11240" s="T374">v:pred</ta>
            <ta e="T376" id="Seg_11241" s="T375">v:pred</ta>
            <ta e="T379" id="Seg_11242" s="T378">np:O</ta>
            <ta e="T381" id="Seg_11243" s="T380">pro.h:S</ta>
            <ta e="T382" id="Seg_11244" s="T381">v:pred</ta>
            <ta e="T383" id="Seg_11245" s="T382">pro.h:S</ta>
            <ta e="T384" id="Seg_11246" s="T383">v:pred</ta>
            <ta e="T385" id="Seg_11247" s="T384">v:pred</ta>
            <ta e="T388" id="Seg_11248" s="T387">np:O</ta>
            <ta e="T393" id="Seg_11249" s="T392">v:pred 0.3.h:S</ta>
            <ta e="T395" id="Seg_11250" s="T394">np.h:S</ta>
            <ta e="T396" id="Seg_11251" s="T395">v:pred</ta>
            <ta e="T397" id="Seg_11252" s="T396">pro.h:S</ta>
            <ta e="T398" id="Seg_11253" s="T397">np:O</ta>
            <ta e="T399" id="Seg_11254" s="T398">v:pred</ta>
            <ta e="T400" id="Seg_11255" s="T399">np:S</ta>
            <ta e="T401" id="Seg_11256" s="T400">v:pred</ta>
            <ta e="T403" id="Seg_11257" s="T402">v:pred 0.3.h:S</ta>
            <ta e="T404" id="Seg_11258" s="T403">v:pred 0.3.h:S</ta>
            <ta e="T408" id="Seg_11259" s="T407">np.h:S</ta>
            <ta e="T409" id="Seg_11260" s="T408">v:pred</ta>
            <ta e="T411" id="Seg_11261" s="T410">pro.h:S</ta>
            <ta e="T412" id="Seg_11262" s="T411">v:pred</ta>
            <ta e="T415" id="Seg_11263" s="T414">np:S</ta>
            <ta e="T416" id="Seg_11264" s="T415">v:pred</ta>
            <ta e="T418" id="Seg_11265" s="T417">pro.h:S</ta>
            <ta e="T419" id="Seg_11266" s="T418">v:pred</ta>
            <ta e="T420" id="Seg_11267" s="T419">ptcl:pred</ta>
            <ta e="T422" id="Seg_11268" s="T421">np:O</ta>
            <ta e="T424" id="Seg_11269" s="T423">np.h:O</ta>
            <ta e="T428" id="Seg_11270" s="T427">pro.h:S</ta>
            <ta e="T429" id="Seg_11271" s="T428">v:pred</ta>
            <ta e="T430" id="Seg_11272" s="T429">v:pred 0.2.h:S</ta>
            <ta e="T433" id="Seg_11273" s="T432">np:S</ta>
            <ta e="T434" id="Seg_11274" s="T433">v:pred</ta>
            <ta e="T436" id="Seg_11275" s="T435">np:O</ta>
            <ta e="T437" id="Seg_11276" s="T436">v:pred 0.3.h:S</ta>
            <ta e="T438" id="Seg_11277" s="T437">np:O</ta>
            <ta e="T439" id="Seg_11278" s="T438">v:pred 0.3.h:S</ta>
            <ta e="T440" id="Seg_11279" s="T439">pro.h:S</ta>
            <ta e="T442" id="Seg_11280" s="T441">v:pred</ta>
            <ta e="T444" id="Seg_11281" s="T443">np:O</ta>
            <ta e="T445" id="Seg_11282" s="T444">v:pred 0.3.h:S</ta>
            <ta e="T449" id="Seg_11283" s="T447">np:O</ta>
            <ta e="T452" id="Seg_11284" s="T451">v:pred 0.3.h:S</ta>
            <ta e="T453" id="Seg_11285" s="T452">v:pred 0.3.h:S</ta>
            <ta e="T454" id="Seg_11286" s="T453">v:pred 0.3.h:S</ta>
            <ta e="T456" id="Seg_11287" s="T455">np:S</ta>
            <ta e="T457" id="Seg_11288" s="T456">v:pred</ta>
            <ta e="T460" id="Seg_11289" s="T459">pro.h:S</ta>
            <ta e="T463" id="Seg_11290" s="T462">v:pred</ta>
            <ta e="T464" id="Seg_11291" s="T463">ptcl:pred</ta>
            <ta e="T467" id="Seg_11292" s="T466">pro.h:S</ta>
            <ta e="T470" id="Seg_11293" s="T469">v:pred</ta>
            <ta e="T472" id="Seg_11294" s="T471">np:O</ta>
            <ta e="T473" id="Seg_11295" s="T472">v:pred 0.1.h:S</ta>
            <ta e="T475" id="Seg_11296" s="T474">np:O</ta>
            <ta e="T476" id="Seg_11297" s="T475">v:pred 0.1.h:S</ta>
            <ta e="T478" id="Seg_11298" s="T477">ptcl.neg</ta>
            <ta e="T479" id="Seg_11299" s="T478">adj:pred</ta>
            <ta e="T480" id="Seg_11300" s="T479">np:S</ta>
            <ta e="T483" id="Seg_11301" s="T482">np:O</ta>
            <ta e="T484" id="Seg_11302" s="T483">v:pred 0.3.h:S</ta>
            <ta e="T488" id="Seg_11303" s="T487">np.h:S</ta>
            <ta e="T490" id="Seg_11304" s="T489">v:pred</ta>
            <ta e="T491" id="Seg_11305" s="T490">v:pred 0.3.h:S</ta>
            <ta e="T492" id="Seg_11306" s="T491">v:pred 0.3.h:S</ta>
            <ta e="T493" id="Seg_11307" s="T492">np:O</ta>
            <ta e="T494" id="Seg_11308" s="T493">np:O</ta>
            <ta e="T495" id="Seg_11309" s="T494">v:pred 0.3.h:S</ta>
            <ta e="T496" id="Seg_11310" s="T495">v:pred 0.3.h:S</ta>
            <ta e="T501" id="Seg_11311" s="T500">v:pred 0.3.h:S</ta>
            <ta e="T505" id="Seg_11312" s="T504">np:S</ta>
            <ta e="T506" id="Seg_11313" s="T505">np.h:S</ta>
            <ta e="T507" id="Seg_11314" s="T506">v:pred</ta>
            <ta e="T510" id="Seg_11315" s="T509">np.h:S</ta>
            <ta e="T511" id="Seg_11316" s="T510">v:pred</ta>
            <ta e="T513" id="Seg_11317" s="T512">v:pred 0.3.h:S</ta>
            <ta e="T514" id="Seg_11318" s="T513">v:pred 0.2.h:S</ta>
            <ta e="T516" id="Seg_11319" s="T515">np:O</ta>
            <ta e="T519" id="Seg_11320" s="T518">ptcl.neg</ta>
            <ta e="T520" id="Seg_11321" s="T519">ptcl.neg</ta>
            <ta e="T521" id="Seg_11322" s="T520">v:pred 0.1.h:S</ta>
            <ta e="T523" id="Seg_11323" s="T522">np:S</ta>
            <ta e="T525" id="Seg_11324" s="T524">np:O</ta>
            <ta e="T526" id="Seg_11325" s="T525">v:pred</ta>
            <ta e="T528" id="Seg_11326" s="T527">pro.h:S</ta>
            <ta e="T530" id="Seg_11327" s="T529">v:pred</ta>
            <ta e="T531" id="Seg_11328" s="T530">v:pred 0.3.h:S</ta>
            <ta e="T532" id="Seg_11329" s="T531">pro.h:S</ta>
            <ta e="T534" id="Seg_11330" s="T533">v:pred</ta>
            <ta e="T536" id="Seg_11331" s="T535">np:O</ta>
            <ta e="T537" id="Seg_11332" s="T536">v:pred 0.3.h:S</ta>
            <ta e="T542" id="Seg_11333" s="T541">v:pred 0.3.h:S</ta>
            <ta e="T543" id="Seg_11334" s="T542">s:purp</ta>
            <ta e="T545" id="Seg_11335" s="T544">v:pred 0.3.h:S</ta>
            <ta e="T547" id="Seg_11336" s="T546">np:S</ta>
            <ta e="T548" id="Seg_11337" s="T547">v:pred</ta>
            <ta e="T551" id="Seg_11338" s="T550">np.h:S</ta>
            <ta e="T552" id="Seg_11339" s="T551">v:pred</ta>
            <ta e="T553" id="Seg_11340" s="T552">np:O</ta>
            <ta e="T554" id="Seg_11341" s="T553">v:pred 0.3.h:S</ta>
            <ta e="T556" id="Seg_11342" s="T555">pro.h:S</ta>
            <ta e="T564" id="Seg_11343" s="T563">v:pred</ta>
            <ta e="T565" id="Seg_11344" s="T564">np:O</ta>
            <ta e="T567" id="Seg_11345" s="T566">np:O</ta>
            <ta e="T570" id="Seg_11346" s="T569">v:pred 0.3.h:S</ta>
            <ta e="T571" id="Seg_11347" s="T570">v:pred 0.3.h:S</ta>
            <ta e="T575" id="Seg_11348" s="T574">np:S</ta>
            <ta e="T578" id="Seg_11349" s="T577">np:O</ta>
            <ta e="T580" id="Seg_11350" s="T579">v:pred</ta>
            <ta e="T583" id="Seg_11351" s="T582">pro.h:S</ta>
            <ta e="T584" id="Seg_11352" s="T583">v:pred</ta>
            <ta e="T585" id="Seg_11353" s="T584">np:O</ta>
            <ta e="T586" id="Seg_11354" s="T585">v:pred 0.3.h:S</ta>
            <ta e="T587" id="Seg_11355" s="T586">v:pred 0.3.h:S</ta>
            <ta e="T589" id="Seg_11356" s="T588">np:O</ta>
            <ta e="T590" id="Seg_11357" s="T589">v:pred 0.3.h:S</ta>
            <ta e="T592" id="Seg_11358" s="T591">v:pred 0.3.h:S</ta>
            <ta e="T595" id="Seg_11359" s="T594">v:pred 0.3.h:S</ta>
            <ta e="T597" id="Seg_11360" s="T596">v:pred 0.3.h:S</ta>
            <ta e="T598" id="Seg_11361" s="T597">v:pred 0.3.h:S</ta>
            <ta e="T601" id="Seg_11362" s="T600">np:S</ta>
            <ta e="T602" id="Seg_11363" s="T601">v:pred</ta>
            <ta e="T603" id="Seg_11364" s="T602">pro.h:S</ta>
            <ta e="T604" id="Seg_11365" s="T603">v:pred</ta>
            <ta e="T606" id="Seg_11366" s="T605">np.h:S</ta>
            <ta e="T607" id="Seg_11367" s="T606">v:pred</ta>
            <ta e="T609" id="Seg_11368" s="T608">np.h:S</ta>
            <ta e="T610" id="Seg_11369" s="T609">v:pred</ta>
            <ta e="T613" id="Seg_11370" s="T612">np:S</ta>
            <ta e="T615" id="Seg_11371" s="T614">pro.h:S</ta>
            <ta e="T616" id="Seg_11372" s="T615">np:O</ta>
            <ta e="T620" id="Seg_11373" s="T619">v:pred</ta>
            <ta e="T623" id="Seg_11374" s="T622">np:O</ta>
            <ta e="T625" id="Seg_11375" s="T624">v:pred 0.3.h:S</ta>
            <ta e="T627" id="Seg_11376" s="T626">v:pred 0.3.h:S</ta>
            <ta e="T631" id="Seg_11377" s="T630">np.h:S</ta>
            <ta e="T633" id="Seg_11378" s="T632">np.h:S</ta>
            <ta e="T634" id="Seg_11379" s="T633">v:pred</ta>
            <ta e="T635" id="Seg_11380" s="T634">v:pred 0.3.h:S</ta>
            <ta e="T636" id="Seg_11381" s="T635">v:pred 0.3.h:S</ta>
            <ta e="T639" id="Seg_11382" s="T638">np.h:S</ta>
            <ta e="T640" id="Seg_11383" s="T639">v:pred</ta>
            <ta e="T646" id="Seg_11384" s="T645">np:S</ta>
            <ta e="T647" id="Seg_11385" s="T646">v:pred</ta>
            <ta e="T649" id="Seg_11386" s="T648">np.h:S</ta>
            <ta e="T650" id="Seg_11387" s="T649">pro.h:S</ta>
            <ta e="T651" id="Seg_11388" s="T650">v:pred</ta>
            <ta e="T652" id="Seg_11389" s="T651">v:pred 0.1.h:S</ta>
            <ta e="T653" id="Seg_11390" s="T652">np.h:O</ta>
            <ta e="T655" id="Seg_11391" s="T654">pro.h:S</ta>
            <ta e="T657" id="Seg_11392" s="T656">v:pred</ta>
            <ta e="T659" id="Seg_11393" s="T658">v:pred 0.3.h:S</ta>
            <ta e="T661" id="Seg_11394" s="T660">pro.h:O</ta>
            <ta e="T665" id="Seg_11395" s="T664">np.h:S</ta>
            <ta e="T666" id="Seg_11396" s="T665">v:pred</ta>
            <ta e="T667" id="Seg_11397" s="T666">v:pred 0.3.h:S</ta>
            <ta e="T668" id="Seg_11398" s="T667">v:pred 0.3.h:S</ta>
            <ta e="T669" id="Seg_11399" s="T668">np:S</ta>
            <ta e="T672" id="Seg_11400" s="T671">v:pred</ta>
            <ta e="T675" id="Seg_11401" s="T674">pro.h:S</ta>
            <ta e="T677" id="Seg_11402" s="T676">v:pred</ta>
            <ta e="T679" id="Seg_11403" s="T678">v:pred 0.3.h:S</ta>
            <ta e="T680" id="Seg_11404" s="T679">v:pred 0.3.h:S</ta>
            <ta e="T682" id="Seg_11405" s="T681">np.h:S</ta>
            <ta e="T683" id="Seg_11406" s="T682">v:pred</ta>
            <ta e="T684" id="Seg_11407" s="T683">v:pred 0.2.h:S</ta>
            <ta e="T686" id="Seg_11408" s="T685">np.h:O</ta>
            <ta e="T691" id="Seg_11409" s="T690">np.h:S</ta>
            <ta e="T693" id="Seg_11410" s="T692">v:pred</ta>
            <ta e="T695" id="Seg_11411" s="T694">v:pred 0.2.h:S</ta>
            <ta e="T696" id="Seg_11412" s="T695">np.h:O</ta>
            <ta e="T698" id="Seg_11413" s="T697">pro.h:S</ta>
            <ta e="T699" id="Seg_11414" s="T698">v:pred</ta>
            <ta e="T701" id="Seg_11415" s="T700">v:pred 0.3.h:S</ta>
            <ta e="T702" id="Seg_11416" s="T701">np:O</ta>
            <ta e="T705" id="Seg_11417" s="T704">np.h:O</ta>
            <ta e="T706" id="Seg_11418" s="T705">v:pred 0.3.h:S</ta>
            <ta e="T708" id="Seg_11419" s="T707">v:pred 0.3.h:S</ta>
            <ta e="T710" id="Seg_11420" s="T709">v:pred 0.3.h:S</ta>
            <ta e="T711" id="Seg_11421" s="T710">v:pred 0.3.h:S</ta>
            <ta e="T713" id="Seg_11422" s="T712">np:S</ta>
            <ta e="T714" id="Seg_11423" s="T713">v:pred</ta>
            <ta e="T716" id="Seg_11424" s="T715">v:pred 0.3.h:S</ta>
            <ta e="T720" id="Seg_11425" s="T719">np:S</ta>
            <ta e="T725" id="Seg_11426" s="T724">v:pred 0.3.h:S</ta>
            <ta e="T729" id="Seg_11427" s="T728">np.h:S</ta>
            <ta e="T730" id="Seg_11428" s="T729">v:pred</ta>
            <ta e="T733" id="Seg_11429" s="T732">v:pred 0.3.h:S</ta>
            <ta e="T737" id="Seg_11430" s="T736">v:pred 0.2.h:S</ta>
            <ta e="T740" id="Seg_11431" s="T739">v:pred 0.2.h:S</ta>
            <ta e="T741" id="Seg_11432" s="T740">pro.h:S</ta>
            <ta e="T742" id="Seg_11433" s="T741">v:pred</ta>
            <ta e="T748" id="Seg_11434" s="T747">v:pred 0.2.h:S</ta>
            <ta e="T749" id="Seg_11435" s="T748">v:pred 0.3.h:S</ta>
            <ta e="T750" id="Seg_11436" s="T749">pro.h:S</ta>
            <ta e="T751" id="Seg_11437" s="T750">v:pred</ta>
            <ta e="T756" id="Seg_11438" s="T755">v:pred 0.3.h:S</ta>
            <ta e="T757" id="Seg_11439" s="T756">np:O</ta>
            <ta e="T761" id="Seg_11440" s="T760">v:pred 0.3.h:S</ta>
            <ta e="T762" id="Seg_11441" s="T761">np.h:S</ta>
            <ta e="T763" id="Seg_11442" s="T762">np:S</ta>
            <ta e="T765" id="Seg_11443" s="T764">v:pred</ta>
            <ta e="T769" id="Seg_11444" s="T768">pro.h:S</ta>
            <ta e="T770" id="Seg_11445" s="T769">v:pred</ta>
            <ta e="T775" id="Seg_11446" s="T774">v:pred 0.2.h:S</ta>
            <ta e="T777" id="Seg_11447" s="T776">np.h:O</ta>
            <ta e="T778" id="Seg_11448" s="T777">pro.h:S</ta>
            <ta e="T779" id="Seg_11449" s="T778">v:pred</ta>
            <ta e="T785" id="Seg_11450" s="T784">v:pred 0.2.h:S</ta>
            <ta e="T786" id="Seg_11451" s="T785">pro.h:S</ta>
            <ta e="T787" id="Seg_11452" s="T786">v:pred</ta>
            <ta e="T790" id="Seg_11453" s="T789">pro.h:S</ta>
            <ta e="T792" id="Seg_11454" s="T791">v:pred</ta>
            <ta e="T794" id="Seg_11455" s="T793">np:S</ta>
            <ta e="T795" id="Seg_11456" s="T794">v:pred</ta>
            <ta e="T799" id="Seg_11457" s="T798">np.h:S</ta>
            <ta e="T800" id="Seg_11458" s="T799">v:pred</ta>
            <ta e="T801" id="Seg_11459" s="T800">np:O</ta>
            <ta e="T802" id="Seg_11460" s="T801">pro.h:S</ta>
            <ta e="T803" id="Seg_11461" s="T802">v:pred</ta>
            <ta e="T807" id="Seg_11462" s="T806">ptcl.neg</ta>
            <ta e="T810" id="Seg_11463" s="T809">v:pred 0.2.h:S</ta>
            <ta e="T812" id="Seg_11464" s="T811">np.h:O</ta>
            <ta e="T815" id="Seg_11465" s="T814">pro.h:S</ta>
            <ta e="T816" id="Seg_11466" s="T815">v:pred</ta>
            <ta e="T820" id="Seg_11467" s="T819">pro.h:S</ta>
            <ta e="T821" id="Seg_11468" s="T820">v:pred</ta>
            <ta e="T826" id="Seg_11469" s="T825">v:pred 0.3.h:S</ta>
            <ta e="T833" id="Seg_11470" s="T832">ptcl:pred</ta>
            <ta e="T835" id="Seg_11471" s="T834">np:O</ta>
            <ta e="T836" id="Seg_11472" s="T835">pro.h:S</ta>
            <ta e="T837" id="Seg_11473" s="T836">np:O</ta>
            <ta e="T840" id="Seg_11474" s="T839">v:pred</ta>
            <ta e="T842" id="Seg_11475" s="T841">np:O</ta>
            <ta e="T843" id="Seg_11476" s="T842">v:pred 0.1.h:S</ta>
            <ta e="T846" id="Seg_11477" s="T845">np:O</ta>
            <ta e="T847" id="Seg_11478" s="T846">v:pred 0.1.h:S</ta>
            <ta e="T849" id="Seg_11479" s="T848">np:O</ta>
            <ta e="T850" id="Seg_11480" s="T849">v:pred 0.1.h:S</ta>
            <ta e="T851" id="Seg_11481" s="T850">v:pred 0.1.h:S</ta>
            <ta e="T852" id="Seg_11482" s="T851">np.h:O</ta>
            <ta e="T854" id="Seg_11483" s="T853">v:pred 0.1.h:S</ta>
            <ta e="T865" id="Seg_11484" s="T864">np:O</ta>
            <ta e="T868" id="Seg_11485" s="T867">v:pred 0.3.h:S</ta>
            <ta e="T870" id="Seg_11486" s="T869">pro.h:S</ta>
            <ta e="T871" id="Seg_11487" s="T870">v:pred</ta>
            <ta e="T874" id="Seg_11488" s="T873">np.h:S</ta>
            <ta e="T875" id="Seg_11489" s="T874">v:pred</ta>
            <ta e="T878" id="Seg_11490" s="T877">np:S</ta>
            <ta e="T881" id="Seg_11491" s="T880">np.h:S</ta>
            <ta e="T883" id="Seg_11492" s="T882">np.h:S</ta>
            <ta e="T884" id="Seg_11493" s="T883">v:pred</ta>
            <ta e="T885" id="Seg_11494" s="T884">pro.h:S</ta>
            <ta e="T888" id="Seg_11495" s="T887">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T17" id="Seg_11496" s="T16">TAT:cult</ta>
            <ta e="T19" id="Seg_11497" s="T18">TURK:gram(INDEF)</ta>
            <ta e="T20" id="Seg_11498" s="T19">TURK:disc</ta>
            <ta e="T21" id="Seg_11499" s="T20">TURK:cult</ta>
            <ta e="T22" id="Seg_11500" s="T21">RUS:gram</ta>
            <ta e="T30" id="Seg_11501" s="T29">TAT:cult</ta>
            <ta e="T31" id="Seg_11502" s="T30">TURK:cult</ta>
            <ta e="T34" id="Seg_11503" s="T33">RUS:gram</ta>
            <ta e="T43" id="Seg_11504" s="T42">TURK:cult</ta>
            <ta e="T48" id="Seg_11505" s="T47">TURK:disc</ta>
            <ta e="T50" id="Seg_11506" s="T49">RUS:gram</ta>
            <ta e="T56" id="Seg_11507" s="T55">TURK:core</ta>
            <ta e="T58" id="Seg_11508" s="T57">RUS:cult</ta>
            <ta e="T59" id="Seg_11509" s="T58">RUS:gram</ta>
            <ta e="T61" id="Seg_11510" s="T60">RUS:cult</ta>
            <ta e="T68" id="Seg_11511" s="T67">TURK:disc</ta>
            <ta e="T75" id="Seg_11512" s="T74">RUS:gram</ta>
            <ta e="T79" id="Seg_11513" s="T78">TURK:core</ta>
            <ta e="T87" id="Seg_11514" s="T86">TURK:disc</ta>
            <ta e="T90" id="Seg_11515" s="T89">RUS:gram</ta>
            <ta e="T96" id="Seg_11516" s="T95">TURK:core</ta>
            <ta e="T102" id="Seg_11517" s="T101">RUS:gram</ta>
            <ta e="T107" id="Seg_11518" s="T106">TURK:core</ta>
            <ta e="T114" id="Seg_11519" s="T113">TURK:disc</ta>
            <ta e="T128" id="Seg_11520" s="T127">RUS:gram</ta>
            <ta e="T132" id="Seg_11521" s="T131">TURK:cult</ta>
            <ta e="T134" id="Seg_11522" s="T133">TURK:disc</ta>
            <ta e="T138" id="Seg_11523" s="T137">TURK:gram(INDEF)</ta>
            <ta e="T142" id="Seg_11524" s="T141">TURK:disc</ta>
            <ta e="T159" id="Seg_11525" s="T158">RUS:cult</ta>
            <ta e="T164" id="Seg_11526" s="T163">TURK:core</ta>
            <ta e="T167" id="Seg_11527" s="T166">RUS:cult</ta>
            <ta e="T172" id="Seg_11528" s="T171">RUS:gram</ta>
            <ta e="T176" id="Seg_11529" s="T175">RUS:gram</ta>
            <ta e="T181" id="Seg_11530" s="T180">TURK:disc</ta>
            <ta e="T891" id="Seg_11531" s="T184">RUS:core</ta>
            <ta e="T187" id="Seg_11532" s="T186">RUS:gram</ta>
            <ta e="T194" id="Seg_11533" s="T193">TURK:core</ta>
            <ta e="T195" id="Seg_11534" s="T194">TURK:disc</ta>
            <ta e="T200" id="Seg_11535" s="T199">TURK:disc</ta>
            <ta e="T201" id="Seg_11536" s="T200">TURK:cult</ta>
            <ta e="T203" id="Seg_11537" s="T202">RUS:mod</ta>
            <ta e="T216" id="Seg_11538" s="T215">TURK:core</ta>
            <ta e="T217" id="Seg_11539" s="T216">RUS:gram</ta>
            <ta e="T220" id="Seg_11540" s="T219">TURK:core</ta>
            <ta e="T221" id="Seg_11541" s="T220">TURK:disc</ta>
            <ta e="T225" id="Seg_11542" s="T224">TURK:disc</ta>
            <ta e="T226" id="Seg_11543" s="T225">RUS:gram</ta>
            <ta e="T229" id="Seg_11544" s="T228">RUS:gram</ta>
            <ta e="T231" id="Seg_11545" s="T230">TURK:disc</ta>
            <ta e="T235" id="Seg_11546" s="T234">RUS:gram</ta>
            <ta e="T241" id="Seg_11547" s="T240">RUS:gram</ta>
            <ta e="T247" id="Seg_11548" s="T246">RUS:mod</ta>
            <ta e="T254" id="Seg_11549" s="T253">RUS:mod</ta>
            <ta e="T255" id="Seg_11550" s="T254">TURK:disc</ta>
            <ta e="T258" id="Seg_11551" s="T257">TURK:core</ta>
            <ta e="T261" id="Seg_11552" s="T260">RUS:gram</ta>
            <ta e="T266" id="Seg_11553" s="T265">RUS:gram</ta>
            <ta e="T270" id="Seg_11554" s="T269">RUS:disc</ta>
            <ta e="T271" id="Seg_11555" s="T270">TURK:gram(INDEF)</ta>
            <ta e="T273" id="Seg_11556" s="T272">TURK:core</ta>
            <ta e="T276" id="Seg_11557" s="T275">RUS:cult</ta>
            <ta e="T278" id="Seg_11558" s="T277">RUS:mod</ta>
            <ta e="T284" id="Seg_11559" s="T283">TURK:disc</ta>
            <ta e="T291" id="Seg_11560" s="T290">TURK:disc</ta>
            <ta e="T298" id="Seg_11561" s="T297">RUS:gram</ta>
            <ta e="T311" id="Seg_11562" s="T310">RUS:gram</ta>
            <ta e="T313" id="Seg_11563" s="T312">TURK:disc</ta>
            <ta e="T315" id="Seg_11564" s="T314">TURK:cult</ta>
            <ta e="T317" id="Seg_11565" s="T316">RUS:gram</ta>
            <ta e="T319" id="Seg_11566" s="T318">TURK:disc</ta>
            <ta e="T321" id="Seg_11567" s="T319">TURK:mod(PTCL)</ta>
            <ta e="T323" id="Seg_11568" s="T322">RUS:mod</ta>
            <ta e="T325" id="Seg_11569" s="T324">RUS:gram</ta>
            <ta e="T332" id="Seg_11570" s="T331">RUS:cult</ta>
            <ta e="T335" id="Seg_11571" s="T334">RUS:gram</ta>
            <ta e="T338" id="Seg_11572" s="T337">RUS:cult</ta>
            <ta e="T339" id="Seg_11573" s="T338">RUS:gram</ta>
            <ta e="T340" id="Seg_11574" s="T339">RUS:core</ta>
            <ta e="T345" id="Seg_11575" s="T344">RUS:mod</ta>
            <ta e="T346" id="Seg_11576" s="T345">RUS:gram</ta>
            <ta e="T348" id="Seg_11577" s="T347">TURK:disc</ta>
            <ta e="T362" id="Seg_11578" s="T361">RUS:gram</ta>
            <ta e="T398" id="Seg_11579" s="T397">RUS:core</ta>
            <ta e="T420" id="Seg_11580" s="T419">RUS:mod</ta>
            <ta e="T427" id="Seg_11581" s="T426">RUS:gram</ta>
            <ta e="T438" id="Seg_11582" s="T437">TURK:cult</ta>
            <ta e="T449" id="Seg_11583" s="T447">RUS:core</ta>
            <ta e="T459" id="Seg_11584" s="T458">RUS:core</ta>
            <ta e="T461" id="Seg_11585" s="T460">TURK:disc</ta>
            <ta e="T464" id="Seg_11586" s="T463">RUS:gram</ta>
            <ta e="T466" id="Seg_11587" s="T465">RUS:gram</ta>
            <ta e="T475" id="Seg_11588" s="T474">RUS:core</ta>
            <ta e="T480" id="Seg_11589" s="T479">RUS:core</ta>
            <ta e="T503" id="Seg_11590" s="T502">RUS:mod</ta>
            <ta e="T508" id="Seg_11591" s="T507">RUS:gram</ta>
            <ta e="T519" id="Seg_11592" s="T518">TURK:disc</ta>
            <ta e="T527" id="Seg_11593" s="T526">RUS:gram</ta>
            <ta e="T530" id="Seg_11594" s="T529">%TURK:core</ta>
            <ta e="T531" id="Seg_11595" s="T530">%TURK:core</ta>
            <ta e="T535" id="Seg_11596" s="T534">RUS:gram</ta>
            <ta e="T540" id="Seg_11597" s="T539">RUS:gram</ta>
            <ta e="T546" id="Seg_11598" s="T545">RUS:gram</ta>
            <ta e="T555" id="Seg_11599" s="T554">RUS:gram</ta>
            <ta e="T559" id="Seg_11600" s="T558">TURK:disc</ta>
            <ta e="T563" id="Seg_11601" s="T562">TURK:disc</ta>
            <ta e="T566" id="Seg_11602" s="T565">RUS:gram</ta>
            <ta e="T568" id="Seg_11603" s="T567">RUS:gram</ta>
            <ta e="T579" id="Seg_11604" s="T578">TURK:disc</ta>
            <ta e="T591" id="Seg_11605" s="T590">RUS:gram</ta>
            <ta e="T600" id="Seg_11606" s="T599">TURK:disc</ta>
            <ta e="T611" id="Seg_11607" s="T610">RUS:gram</ta>
            <ta e="T614" id="Seg_11608" s="T613">RUS:gram</ta>
            <ta e="T632" id="Seg_11609" s="T631">RUS:gram</ta>
            <ta e="T635" id="Seg_11610" s="T634">%TURK:core</ta>
            <ta e="T636" id="Seg_11611" s="T635">%TURK:core</ta>
            <ta e="T642" id="Seg_11612" s="T641">RUS:gram</ta>
            <ta e="T648" id="Seg_11613" s="T647">RUS:gram</ta>
            <ta e="T658" id="Seg_11614" s="T657">RUS:gram</ta>
            <ta e="T670" id="Seg_11615" s="T669">TURK:disc</ta>
            <ta e="T676" id="Seg_11616" s="T675">TURK:disc</ta>
            <ta e="T687" id="Seg_11617" s="T686">RUS:mod</ta>
            <ta e="T692" id="Seg_11618" s="T691">RUS:core</ta>
            <ta e="T694" id="Seg_11619" s="T693">RUS:disc</ta>
            <ta e="T703" id="Seg_11620" s="T702">RUS:gram</ta>
            <ta e="T707" id="Seg_11621" s="T706">RUS:gram</ta>
            <ta e="T717" id="Seg_11622" s="T716">RUS:cult</ta>
            <ta e="T721" id="Seg_11623" s="T720">RUS:gram</ta>
            <ta e="T736" id="Seg_11624" s="T735">RUS:disc</ta>
            <ta e="T768" id="Seg_11625" s="T767">RUS:gram</ta>
            <ta e="T771" id="Seg_11626" s="T770">TURK:disc</ta>
            <ta e="T774" id="Seg_11627" s="T773">RUS:disc</ta>
            <ta e="T791" id="Seg_11628" s="T790">TURK:disc</ta>
            <ta e="T794" id="Seg_11629" s="T793">TURK:cult</ta>
            <ta e="T796" id="Seg_11630" s="T795">RUS:gram</ta>
            <ta e="T798" id="Seg_11631" s="T797">RUS:mod</ta>
            <ta e="T801" id="Seg_11632" s="T800">TURK:cult</ta>
            <ta e="T805" id="Seg_11633" s="T804">TURK:cult</ta>
            <ta e="T806" id="Seg_11634" s="T805">TURK:disc</ta>
            <ta e="T809" id="Seg_11635" s="T808">RUS:disc</ta>
            <ta e="T818" id="Seg_11636" s="T817">TURK:cult</ta>
            <ta e="T823" id="Seg_11637" s="T822">TURK:cult</ta>
            <ta e="T824" id="Seg_11638" s="T823">RUS:gram</ta>
            <ta e="T828" id="Seg_11639" s="T827">TURK:cult</ta>
            <ta e="T830" id="Seg_11640" s="T829">TURK:cult</ta>
            <ta e="T833" id="Seg_11641" s="T832">RUS:gram</ta>
            <ta e="T837" id="Seg_11642" s="T836">RUS:core</ta>
            <ta e="T855" id="Seg_11643" s="T854">RUS:gram</ta>
            <ta e="T858" id="Seg_11644" s="T857">RUS:gram</ta>
            <ta e="T869" id="Seg_11645" s="T868">RUS:gram</ta>
            <ta e="T876" id="Seg_11646" s="T875">RUS:gram</ta>
            <ta e="T879" id="Seg_11647" s="T878">RUS:gram</ta>
            <ta e="T882" id="Seg_11648" s="T881">RUS:gram</ta>
            <ta e="T886" id="Seg_11649" s="T885">TURK:disc</ta>
            <ta e="T888" id="Seg_11650" s="T887">%TURK:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T81" id="Seg_11651" s="T80">RUS:int</ta>
            <ta e="T189" id="Seg_11652" s="T188">RUS:int</ta>
            <ta e="T309" id="Seg_11653" s="T308">RUS:int</ta>
            <ta e="T354" id="Seg_11654" s="T353">RUS:int</ta>
            <ta e="T529" id="Seg_11655" s="T528">RUS:int</ta>
            <ta e="T662" id="Seg_11656" s="T661">RUS:int</ta>
            <ta e="T671" id="Seg_11657" s="T670">RUS:int</ta>
            <ta e="T739" id="Seg_11658" s="T737">RUS:int</ta>
            <ta e="T838" id="Seg_11659" s="T837">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_11660" s="T0">(…) делить.</ta>
            <ta e="T11" id="Seg_11661" s="T3">Я положила ему грибов, масло дала, деньги дала.</ta>
            <ta e="T15" id="Seg_11662" s="T11">Один, два, три, четыре.</ta>
            <ta e="T21" id="Seg_11663" s="T15">Они дом купили, кто-то его продавал.</ta>
            <ta e="T25" id="Seg_11664" s="T21">И денег не хватает.</ta>
            <ta e="T32" id="Seg_11665" s="T26">Они этот дом купили.</ta>
            <ta e="T41" id="Seg_11666" s="T33">[?]</ta>
            <ta e="T45" id="Seg_11667" s="T41">[Те] покупали его корову.</ta>
            <ta e="T53" id="Seg_11668" s="T45">Что есть, все купили, и деньги они отдали туда.</ta>
            <ta e="T72" id="Seg_11669" s="T54">Мой родственник поехал в город, он купит там машину, чтобы стирать одежду — рубашки, штаны, все, что есть, стирать.</ta>
            <ta e="T79" id="Seg_11670" s="T73">Муж с женой жили очень хорошо.</ta>
            <ta e="T83" id="Seg_11671" s="T79">Любили…</ta>
            <ta e="T87" id="Seg_11672" s="T83">Любили друг друга.</ta>
            <ta e="T88" id="Seg_11673" s="T87">(…).</ta>
            <ta e="T89" id="Seg_11674" s="T88">Целовались.</ta>
            <ta e="T96" id="Seg_11675" s="T89">И у них были дети, дети очень хорошие.</ta>
            <ta e="T100" id="Seg_11676" s="T96">Что скажешь, они слушаются.</ta>
            <ta e="T107" id="Seg_11677" s="T101">А наши дети очень плохие.</ta>
            <ta e="T111" id="Seg_11678" s="T107">Дерутся, ругаются, не слушаются.</ta>
            <ta e="T115" id="Seg_11679" s="T111">Я их бью.</ta>
            <ta e="T120" id="Seg_11680" s="T116">Я учу детей писать.</ta>
            <ta e="T123" id="Seg_11681" s="T121">Камасинцы жили.</ta>
            <ta e="T130" id="Seg_11682" s="T123">У кого много денег, у кого мало.</ta>
            <ta e="T133" id="Seg_11683" s="T130">Он пьет водку.</ta>
            <ta e="T136" id="Seg_11684" s="T133">Все деньги пропьет.</ta>
            <ta e="T139" id="Seg_11685" s="T136">Потом [у него] ничего нет.</ta>
            <ta e="T143" id="Seg_11686" s="T139">Работает-работает, и все пропьет.</ta>
            <ta e="T150" id="Seg_11687" s="T145">В Сибири жило очень много камасинцев.</ta>
            <ta e="T154" id="Seg_11688" s="T150">Потом выросли березы.</ta>
            <ta e="T165" id="Seg_11689" s="T154">Они говорят: "Ну, русские люди придут, нам плохо будет".</ta>
            <ta e="T168" id="Seg_11690" s="T165">Они сделали лабазы.</ta>
            <ta e="T174" id="Seg_11691" s="T168">Они принесли [туда] камни, взяли топоры.</ta>
            <ta e="T183" id="Seg_11692" s="T174">Подрубили [опоры], и они упали, и там все погибли.</ta>
            <ta e="T192" id="Seg_11693" s="T184">Лучше бы я умерла, чем [иметь] таких детей.</ta>
            <ta e="T196" id="Seg_11694" s="T192">Они плохие, они все воруют.</ta>
            <ta e="T198" id="Seg_11695" s="T196">С людьми дерутся.</ta>
            <ta e="T208" id="Seg_11696" s="T199">[Столько] водки пьют, аж глазами на них невозможно смотреть.</ta>
            <ta e="T220" id="Seg_11697" s="T209">У одной женщины было три сына, двое хорошие, а один плохой.</ta>
            <ta e="T223" id="Seg_11698" s="T220">Все(?) его били.</ta>
            <ta e="T234" id="Seg_11699" s="T223">Ногами, и кулаками, и палками его били.</ta>
            <ta e="T239" id="Seg_11700" s="T234">А я (сказала?)…</ta>
            <ta e="T248" id="Seg_11701" s="T240">А я говорю: "Не (бейте?) его, может, он вырастет.</ta>
            <ta e="T253" id="Seg_11702" s="T248">Он умным станет, большим станет.</ta>
            <ta e="T259" id="Seg_11703" s="T253">Может, он перестанет [делать так, как сейчас делает], тогда он будет хороший".</ta>
            <ta e="T270" id="Seg_11704" s="T260">А он какой маленький был, и большой такой же.</ta>
            <ta e="T273" id="Seg_11705" s="T270">Нет ничего хорошего.</ta>
            <ta e="T277" id="Seg_11706" s="T274">Он пошел в армию.</ta>
            <ta e="T285" id="Seg_11707" s="T277">Все равно пришел [такой же], куда он пойдет работать, его прогоняют.</ta>
            <ta e="T292" id="Seg_11708" s="T286">У одной женщины был сын, пьяница.</ta>
            <ta e="T297" id="Seg_11709" s="T292">"Почему ты меня не учила?"</ta>
            <ta e="T307" id="Seg_11710" s="T297">Она сказала: "Я выучила тебя, чего тебе [еще] нужно?</ta>
            <ta e="T310" id="Seg_11711" s="T307">Ты получил диплом.</ta>
            <ta e="T316" id="Seg_11712" s="T310">А ты водку пил!"</ta>
            <ta e="T324" id="Seg_11713" s="T316">А он хотел мать кулаком ударить.</ta>
            <ta e="T328" id="Seg_11714" s="T324">Люди не дали.</ta>
            <ta e="T334" id="Seg_11715" s="T329">Мой брат был на войне.</ta>
            <ta e="T338" id="Seg_11716" s="T334">И его взяли в плен.</ta>
            <ta e="T345" id="Seg_11717" s="T338">Ему хотели отрезать правую руку с пальцами.</ta>
            <ta e="T355" id="Seg_11718" s="T345">Он плакал, и ему оставили два пальца.</ta>
            <ta e="T364" id="Seg_11719" s="T356">У меня три брата и три сестры. [?]</ta>
            <ta e="T370" id="Seg_11720" s="T365">Рассказать тебе сказку про белого бычка?</ta>
            <ta e="T375" id="Seg_11721" s="T371">Ты расскажи, я расскажи.</ta>
            <ta e="T379" id="Seg_11722" s="T375">Рассказать тебе сказку про красного бычка?</ta>
            <ta e="T384" id="Seg_11723" s="T380">Ты расскажи, я расскажи.</ta>
            <ta e="T388" id="Seg_11724" s="T384">Рассказать тебе сказку про черного бычка?</ta>
            <ta e="T392" id="Seg_11725" s="T388">Кодур, Кодур, красная шуба.</ta>
            <ta e="T401" id="Seg_11726" s="T392">Он пришел, там люди живут, он нашел лопатку, мяса [на ней] нет.</ta>
            <ta e="T404" id="Seg_11727" s="T401">Потом он шел, шел.</ta>
            <ta e="T410" id="Seg_11728" s="T404">Ну, там один человек живет с женой.</ta>
            <ta e="T417" id="Seg_11729" s="T410">Он пришел [к ним], у них была одна овца.</ta>
            <ta e="T426" id="Seg_11730" s="T417">Тот говорит: "Надо чай вскипятить, человека накормить".</ta>
            <ta e="T434" id="Seg_11731" s="T426">А он говорит: "Не кипятите, у меня мясо есть".</ta>
            <ta e="T437" id="Seg_11732" s="T434">Тогда они повесили котел.</ta>
            <ta e="T449" id="Seg_11733" s="T437">Соль положили, [а] он вышел, ударил себя по носу, кровью лопатку [измазал].</ta>
            <ta e="T452" id="Seg_11734" s="T449">Потом в котел [ее] бросил.</ta>
            <ta e="T454" id="Seg_11735" s="T452">Она варилась, варилась.</ta>
            <ta e="T459" id="Seg_11736" s="T454">Потом [они смотрят]: мяса [на ней] нет, только лопатка.</ta>
            <ta e="T465" id="Seg_11737" s="T459">Он упал, стал плакать.</ta>
            <ta e="T476" id="Seg_11738" s="T465">Они говорят: "Сейчас я зарежу овцу, (дам?) тебе лопатку".</ta>
            <ta e="T480" id="Seg_11739" s="T476">"Мне не нужна лопатка!"</ta>
            <ta e="T484" id="Seg_11740" s="T480">Тогда ему дали овцу.</ta>
            <ta e="T490" id="Seg_11741" s="T485">Тогда этот Кодур вскочил.</ta>
            <ta e="T493" id="Seg_11742" s="T490">Он помылся, протер глаза.</ta>
            <ta e="T496" id="Seg_11743" s="T493">Взял овцу, пошел.</ta>
            <ta e="T501" id="Seg_11744" s="T496">Потом пришел к людям.</ta>
            <ta e="T507" id="Seg_11745" s="T501">Там тоже две овцы, [и] парень был.</ta>
            <ta e="T511" id="Seg_11746" s="T507">И парня жена.</ta>
            <ta e="T518" id="Seg_11747" s="T511">Они говорят: "Пусти свою овцу [пастись] вместе с нашими овцами!"</ta>
            <ta e="T526" id="Seg_11748" s="T518">"Нет, не пущу, ваши овцы мою овцу съедят!"</ta>
            <ta e="T531" id="Seg_11749" s="T526">А сам всё говорит, говорит.</ta>
            <ta e="T539" id="Seg_11750" s="T531">[А сам] побежал и отпустил свою овцу к тем овцам.</ta>
            <ta e="T545" id="Seg_11751" s="T539">Потом они легли спать, утром встали.</ta>
            <ta e="T548" id="Seg_11752" s="T545">А овцы нет.</ta>
            <ta e="T552" id="Seg_11753" s="T549">Кодур встал.</ta>
            <ta e="T554" id="Seg_11754" s="T552">Свою овцу отделил.</ta>
            <ta e="T567" id="Seg_11755" s="T554">И намазал их овцам нос и рот кровью.</ta>
            <ta e="T580" id="Seg_11756" s="T567">Утром они встали, говорят: "Правда, наша овца разорвала его овцу".</ta>
            <ta e="T584" id="Seg_11757" s="T581">Тогда он встал.</ta>
            <ta e="T586" id="Seg_11758" s="T584">Помыл глаза.</ta>
            <ta e="T587" id="Seg_11759" s="T586">Протер.</ta>
            <ta e="T595" id="Seg_11760" s="T587">Взял двух овец и идет.</ta>
            <ta e="T598" id="Seg_11761" s="T595">Идет, идет.</ta>
            <ta e="T602" id="Seg_11762" s="T598">Там стоит крест.</ta>
            <ta e="T613" id="Seg_11763" s="T602">Он стал копать, там женщина лежит, он взял эту женщину и двух овец.</ta>
            <ta e="T620" id="Seg_11764" s="T613">И привязал своих овец к рукам женщины.</ta>
            <ta e="T625" id="Seg_11765" s="T620">И воткнул нож ей в сердце.</ta>
            <ta e="T634" id="Seg_11766" s="T625">Потом пришел, там живет мужчина с двумя дочерьми.</ta>
            <ta e="T641" id="Seg_11767" s="T634">Говорит, говорит: "Там сидит моя жена с овцами."</ta>
            <ta e="T647" id="Seg_11768" s="T641">А у нее в сердце нож.</ta>
            <ta e="T654" id="Seg_11769" s="T647">А дочери: "Мы пойдем, приведем твою жену".</ta>
            <ta e="T657" id="Seg_11770" s="T654">"Она очень пугливая.</ta>
            <ta e="T662" id="Seg_11771" s="T657">Может себя ножом заколоть".</ta>
            <ta e="T667" id="Seg_11772" s="T662">Тогда эти дочери побежали, вернулись.</ta>
            <ta e="T673" id="Seg_11773" s="T667">"Она [себя] заколола, овцы убежали, (женщина?)".</ta>
            <ta e="T680" id="Seg_11774" s="T673">Тогда Кодур упал, плакал, плакал.</ta>
            <ta e="T686" id="Seg_11775" s="T680">Тот человек говорит: "Возьми одну мою дочь".</ta>
            <ta e="T693" id="Seg_11776" s="T686">"Зачем мне одна, моя жена была лучше".</ta>
            <ta e="T696" id="Seg_11777" s="T693">"Ну, возьми двух!"</ta>
            <ta e="T699" id="Seg_11778" s="T696">Тогда он встал.</ta>
            <ta e="T702" id="Seg_11779" s="T699">Снова помыл лицо.</ta>
            <ta e="T708" id="Seg_11780" s="T702">Взял двух дочерей и ушел.</ta>
            <ta e="T714" id="Seg_11781" s="T708">Идет, идет там овцы ходят.</ta>
            <ta e="T720" id="Seg_11782" s="T714">Он подошел к пастуху: "Это мои овцы!"</ta>
            <ta e="T721" id="Seg_11783" s="T720">Да(?).</ta>
            <ta e="T731" id="Seg_11784" s="T722">Он посадил одну [девушку] на берегу реки, другую в лесу.</ta>
            <ta e="T735" id="Seg_11785" s="T731">Потом говорит: "Мои овцы!"</ta>
            <ta e="T737" id="Seg_11786" s="T735">"Ну спроси.</ta>
            <ta e="T740" id="Seg_11787" s="T737">У одной спроси!"</ta>
            <ta e="T742" id="Seg_11788" s="T740">Тот спросил.</ta>
            <ta e="T744" id="Seg_11789" s="T742">"[Это] овцы Кодура!"</ta>
            <ta e="T748" id="Seg_11790" s="T744">"Теперь спроси человека тайги".</ta>
            <ta e="T753" id="Seg_11791" s="T748">Тот спросил, та [девушка] говорит: "[Это] овцы Кодура".</ta>
            <ta e="T757" id="Seg_11792" s="T753">Тогда он взял его овец.</ta>
            <ta e="T765" id="Seg_11793" s="T758">Снова идет, человек живет [с] лошадями.</ta>
            <ta e="T767" id="Seg_11794" s="T765">"Мои лошади!"</ta>
            <ta e="T773" id="Seg_11795" s="T767">А тот говорит: "Нет, мои лошади!"</ta>
            <ta e="T777" id="Seg_11796" s="T773">"Ну спроси речного человека!"</ta>
            <ta e="T781" id="Seg_11797" s="T777">Тот спросил: "Кодура лошади!"</ta>
            <ta e="T785" id="Seg_11798" s="T781">"Теперь спроси лесного человека!"</ta>
            <ta e="T789" id="Seg_11799" s="T785">Тот спросил: "Кодура лошади!"</ta>
            <ta e="T792" id="Seg_11800" s="T789">Тот отдал [лошадей].</ta>
            <ta e="T795" id="Seg_11801" s="T792">Потом коровы ходят.</ta>
            <ta e="T801" id="Seg_11802" s="T795">И там тоже человек за ними смотрит.</ta>
            <ta e="T805" id="Seg_11803" s="T801">Он пришел: "Мои коровы!"</ta>
            <ta e="T808" id="Seg_11804" s="T805">"Нет, не твои!"</ta>
            <ta e="T812" id="Seg_11805" s="T808">"Ну, спроси человека у воды".</ta>
            <ta e="T816" id="Seg_11806" s="T812">Потом [человека] в лесу он спросил:</ta>
            <ta e="T818" id="Seg_11807" s="T816">"Чьи [это] коровы?"</ta>
            <ta e="T823" id="Seg_11808" s="T818">Та [девушка] сказала: "Кодура коровы!"</ta>
            <ta e="T826" id="Seg_11809" s="T823">И в лесу он спросил:</ta>
            <ta e="T828" id="Seg_11810" s="T826">"Чьи [это] коровы?"</ta>
            <ta e="T830" id="Seg_11811" s="T828">"Кодура коровы!"</ta>
            <ta e="T835" id="Seg_11812" s="T830">Тогда он стал петь песню.</ta>
            <ta e="T840" id="Seg_11813" s="T835">"Я взял сухую лопатку.</ta>
            <ta e="T843" id="Seg_11814" s="T840">Я взял овцу.</ta>
            <ta e="T847" id="Seg_11815" s="T843">За овцу я взял двух овец.</ta>
            <ta e="T854" id="Seg_11816" s="T847">Я взял двух овец, пошел, взял женщину из земли.</ta>
            <ta e="T863" id="Seg_11817" s="T854">И двух овец и жену [с] ножом в сердце".</ta>
            <ta e="T868" id="Seg_11818" s="T864">[Он] воткнул ей нож в сердце.</ta>
            <ta e="T878" id="Seg_11819" s="T868">А сам пришел: "У меня там есть жена и две овцы".</ta>
            <ta e="T884" id="Seg_11820" s="T878">И две дочери и мужчина живут.</ta>
            <ta e="T888" id="Seg_11821" s="T884">Он мужчине говорит…</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_11822" s="T0">(…) to share.</ta>
            <ta e="T11" id="Seg_11823" s="T3">I gave him mushrooms, butter, money.</ta>
            <ta e="T15" id="Seg_11824" s="T11">One, two, three, four.</ta>
            <ta e="T21" id="Seg_11825" s="T15">They bought a house, somebody was selling it.</ta>
            <ta e="T25" id="Seg_11826" s="T21">And they don't have enough money.</ta>
            <ta e="T32" id="Seg_11827" s="T26">They bought this house.</ta>
            <ta e="T41" id="Seg_11828" s="T33">And who have no money to borrow. [?]</ta>
            <ta e="T45" id="Seg_11829" s="T41">They bought his cow.</ta>
            <ta e="T53" id="Seg_11830" s="T45">People bought all they had, and they paid out the money.</ta>
            <ta e="T72" id="Seg_11831" s="T54">My relative went to the town, he'll buy there a machine to wash clothes: shirts, trousers, to wash everything there is.</ta>
            <ta e="T79" id="Seg_11832" s="T73">A man and a woman lived very well.</ta>
            <ta e="T83" id="Seg_11833" s="T79">They loved [each other].</ta>
            <ta e="T87" id="Seg_11834" s="T83">They loved each other.</ta>
            <ta e="T88" id="Seg_11835" s="T87">(…).</ta>
            <ta e="T89" id="Seg_11836" s="T88">They kiss each other.</ta>
            <ta e="T96" id="Seg_11837" s="T89">And they had children, their children were very good.</ta>
            <ta e="T100" id="Seg_11838" s="T96">Whatever you say, they obey.</ta>
            <ta e="T107" id="Seg_11839" s="T101">And our children are very bad.</ta>
            <ta e="T111" id="Seg_11840" s="T107">They fight, they swear, they don't obey.</ta>
            <ta e="T115" id="Seg_11841" s="T111">I beat them.</ta>
            <ta e="T120" id="Seg_11842" s="T116">I teach children to write.</ta>
            <ta e="T123" id="Seg_11843" s="T121">Kamassians lived.</ta>
            <ta e="T130" id="Seg_11844" s="T123">Some have very much money, and some have nothing.</ta>
            <ta e="T133" id="Seg_11845" s="T130">He drinks vodka.</ta>
            <ta e="T136" id="Seg_11846" s="T133">He'll drink away all the money.</ta>
            <ta e="T139" id="Seg_11847" s="T136">Then [he] has nothing.</ta>
            <ta e="T143" id="Seg_11848" s="T139">He works and works, and drinks away everything.</ta>
            <ta e="T150" id="Seg_11849" s="T145">There lived very much Kamassians in Siberia.</ta>
            <ta e="T154" id="Seg_11850" s="T150">Then a birch grew.</ta>
            <ta e="T165" id="Seg_11851" s="T154">They say: "Well, Russian people will come, we won't be well."</ta>
            <ta e="T168" id="Seg_11852" s="T165">Then they made platforms on pillars. [?]</ta>
            <ta e="T174" id="Seg_11853" s="T168">They brought [there] stones, they took axes.</ta>
            <ta e="T183" id="Seg_11854" s="T174">They chopped [it], and they fell down, they all died there.</ta>
            <ta e="T192" id="Seg_11855" s="T184">I would rather die than [have] such children. [?]</ta>
            <ta e="T196" id="Seg_11856" s="T192">They aren't good, they steal everything.</ta>
            <ta e="T198" id="Seg_11857" s="T196">They fight with people.</ta>
            <ta e="T208" id="Seg_11858" s="T199">They drink vodka [so much], that one doesn't want to see them.</ta>
            <ta e="T220" id="Seg_11859" s="T209">One woman had three sons, two [were] good and one [was] not good.</ta>
            <ta e="T223" id="Seg_11860" s="T220">Everyone(?) beated him.</ta>
            <ta e="T234" id="Seg_11861" s="T223">They kicked, and punched, and caned [him].</ta>
            <ta e="T239" id="Seg_11862" s="T234">And I (say?)…</ta>
            <ta e="T248" id="Seg_11863" s="T240">And I (say?): "Don't (beat?) him, maybe he'll grow up.</ta>
            <ta e="T253" id="Seg_11864" s="T248">He'll be clever, he'll be big.</ta>
            <ta e="T259" id="Seg_11865" s="T253">Maybe he'll stop [doing what he's doing now] everything, then he'll be good."</ta>
            <ta e="T270" id="Seg_11866" s="T260">But he's big the same as it was small.</ta>
            <ta e="T273" id="Seg_11867" s="T270">There is nothing good.</ta>
            <ta e="T277" id="Seg_11868" s="T274">He went to the army.</ta>
            <ta e="T285" id="Seg_11869" s="T277">He came back, and it was the same: whereever he goes to work, he's fired.</ta>
            <ta e="T292" id="Seg_11870" s="T286">One woman had a son, a drunkard.</ta>
            <ta e="T297" id="Seg_11871" s="T292">"Why didn't you teach me?".</ta>
            <ta e="T307" id="Seg_11872" s="T297">She said: "I taught you, what [else] do you want?</ta>
            <ta e="T310" id="Seg_11873" s="T307">You had a diploma.</ta>
            <ta e="T316" id="Seg_11874" s="T310">But you were drinking vodka!"</ta>
            <ta e="T324" id="Seg_11875" s="T316">He wanted to punch his mother.</ta>
            <ta e="T328" id="Seg_11876" s="T324">Others didn't let him [to do so].</ta>
            <ta e="T334" id="Seg_11877" s="T329">My brother was in the war.</ta>
            <ta e="T338" id="Seg_11878" s="T334">And he was captured.</ta>
            <ta e="T345" id="Seg_11879" s="T338">They were going to cut off his right hand with the fingers.</ta>
            <ta e="T355" id="Seg_11880" s="T345">He cried, and he got to (keep?) two fingers.</ta>
            <ta e="T364" id="Seg_11881" s="T356">My siblings are three girls and three boys. [?]</ta>
            <ta e="T370" id="Seg_11882" s="T365">Do you want me to tell you the tale of the white bull?</ta>
            <ta e="T375" id="Seg_11883" s="T371">You tell, me tell.</ta>
            <ta e="T379" id="Seg_11884" s="T375">Do you want me to tell you tale of the red bullock?</ta>
            <ta e="T384" id="Seg_11885" s="T380">You tell, me tell.</ta>
            <ta e="T388" id="Seg_11886" s="T384">Do you want me to tell you the tale of the black bullock?</ta>
            <ta e="T392" id="Seg_11887" s="T388">Kodur, Kodur, red coat.</ta>
            <ta e="T401" id="Seg_11888" s="T392">He came, people lived there, he found a shoulder blade, there is no meat [on it].</ta>
            <ta e="T404" id="Seg_11889" s="T401">Then he went, went.</ta>
            <ta e="T410" id="Seg_11890" s="T404">Well, one man lives here with his wife.</ta>
            <ta e="T417" id="Seg_11891" s="T410">He came [to them], they had one sheep.</ta>
            <ta e="T426" id="Seg_11892" s="T417">He [that man] says: "We should make tea, to feed the man."</ta>
            <ta e="T434" id="Seg_11893" s="T426">And he says: "Don't boil, I have meat!"</ta>
            <ta e="T437" id="Seg_11894" s="T434">Then they hung up the cauldron.</ta>
            <ta e="T449" id="Seg_11895" s="T437">They put salt, he went, hit his nose, with blood, he [painted] the shoulder blade with [his] blood.</ta>
            <ta e="T452" id="Seg_11896" s="T449">Then he threw [it] in the cauldron.</ta>
            <ta e="T454" id="Seg_11897" s="T452">It boiled, it boiled.</ta>
            <ta e="T459" id="Seg_11898" s="T454">Then [they look]: there is no meat [on it], only the shoulder blade [= the bone].</ta>
            <ta e="T465" id="Seg_11899" s="T459">He fell down, began to cry.</ta>
            <ta e="T476" id="Seg_11900" s="T465">They said: "Now I will kill a sheep, I will (give?) you the shoulder blade."</ta>
            <ta e="T480" id="Seg_11901" s="T476">"I don’t need a shoulder blade!"</ta>
            <ta e="T484" id="Seg_11902" s="T480">Then they gave him a sheep.</ta>
            <ta e="T490" id="Seg_11903" s="T485">Then this Kodur jumped up.</ta>
            <ta e="T493" id="Seg_11904" s="T490">He washed, wiped his eyes.</ta>
            <ta e="T496" id="Seg_11905" s="T493">He took his sheep, went.</ta>
            <ta e="T501" id="Seg_11906" s="T496">Then he came to people.</ta>
            <ta e="T507" id="Seg_11907" s="T501">Here were also two sheep, (there) was a guy.</ta>
            <ta e="T511" id="Seg_11908" s="T507">And the guy's wife.</ta>
            <ta e="T518" id="Seg_11909" s="T511">Then they say: "Let your sheep among our sheep!"</ta>
            <ta e="T526" id="Seg_11910" s="T518">"No, I will not let, your sheep will eat up my sheep!"</ta>
            <ta e="T531" id="Seg_11911" s="T526">But himself speaks and speaks.</ta>
            <ta e="T539" id="Seg_11912" s="T531">[But] he ran and let his sheep to these sheep.</ta>
            <ta e="T545" id="Seg_11913" s="T539">Then they lied down to sleep, in the morning they got up.</ta>
            <ta e="T548" id="Seg_11914" s="T545">But there is no sheep.</ta>
            <ta e="T552" id="Seg_11915" s="T549">This Kodur got up.</ta>
            <ta e="T554" id="Seg_11916" s="T552">Separated his sheep.</ta>
            <ta e="T567" id="Seg_11917" s="T554">And he smeared their sheep's nose and mouth with blood.</ta>
            <ta e="T580" id="Seg_11918" s="T567">In the morning they got up, the said: "Right, our sheep tore apart his sheep."</ta>
            <ta e="T584" id="Seg_11919" s="T581">Then he got up.</ta>
            <ta e="T586" id="Seg_11920" s="T584">Washed his eye(s).</ta>
            <ta e="T587" id="Seg_11921" s="T586">Wiped.</ta>
            <ta e="T595" id="Seg_11922" s="T587">Took two sheep and went, took off.</ta>
            <ta e="T598" id="Seg_11923" s="T595">Then he comes, comes.</ta>
            <ta e="T602" id="Seg_11924" s="T598">There is a wooden cross standing.</ta>
            <ta e="T613" id="Seg_11925" s="T602">He dug, there is a woman lying, he took that woman and two sheep.</ta>
            <ta e="T620" id="Seg_11926" s="T613">And he tied the sheep to the woman’s hands.</ta>
            <ta e="T625" id="Seg_11927" s="T620">He stabbed the knife into her heart.</ta>
            <ta e="T634" id="Seg_11928" s="T625">Then he came, there lived two daughters and a man.</ta>
            <ta e="T641" id="Seg_11929" s="T634">He speaks, he speaks: "My wife is sitting there with sheep."</ta>
            <ta e="T647" id="Seg_11930" s="T641">And she has a knife sitting in her heart.</ta>
            <ta e="T654" id="Seg_11931" s="T647">But the daughters: "We will go, bring your wife."</ta>
            <ta e="T657" id="Seg_11932" s="T654">"She is very timid.</ta>
            <ta e="T662" id="Seg_11933" s="T657">She may stab herself [with the knife]."</ta>
            <ta e="T667" id="Seg_11934" s="T662">Then those daughters ran, came.</ta>
            <ta e="T673" id="Seg_11935" s="T667">"She stabbed [herself], the sheep ran off, (the wife?)."</ta>
            <ta e="T680" id="Seg_11936" s="T673">Then he fell down, Kodur, cried and cried.</ta>
            <ta e="T686" id="Seg_11937" s="T680">This man says: "Take one [of my] daughters."</ta>
            <ta e="T693" id="Seg_11938" s="T686">"Why do I need one, my wife was better."</ta>
            <ta e="T696" id="Seg_11939" s="T693">"Well, then take both!"</ta>
            <ta e="T699" id="Seg_11940" s="T696">Then he got up.</ta>
            <ta e="T702" id="Seg_11941" s="T699">He washed his face again.</ta>
            <ta e="T708" id="Seg_11942" s="T702">He took the two daughters and left.</ta>
            <ta e="T714" id="Seg_11943" s="T708">Then comes, comes, there are grazing sheep.</ta>
            <ta e="T720" id="Seg_11944" s="T714">Then he came to the shepherd: "There [are] my sheep!"</ta>
            <ta e="T721" id="Seg_11945" s="T720">Yes(?).</ta>
            <ta e="T731" id="Seg_11946" s="T722">He seated one by the rivershore, one in the forest.</ta>
            <ta e="T735" id="Seg_11947" s="T731">Then he is saying: "My sheep!"</ta>
            <ta e="T737" id="Seg_11948" s="T735">"Well, ask.</ta>
            <ta e="T740" id="Seg_11949" s="T737">Ask one [of them]!"</ta>
            <ta e="T742" id="Seg_11950" s="T740">He asked.</ta>
            <ta e="T744" id="Seg_11951" s="T742">"[These are] Kodur’s sheep!"</ta>
            <ta e="T748" id="Seg_11952" s="T744">"Now ask the taiga man."</ta>
            <ta e="T753" id="Seg_11953" s="T748">He asked, that [person] said: ”Kodur’s sheep”.</ta>
            <ta e="T757" id="Seg_11954" s="T753">Then he took his sheep.</ta>
            <ta e="T765" id="Seg_11955" s="T758">Again he came, a man [and] horses are living.</ta>
            <ta e="T767" id="Seg_11956" s="T765">”My horses!".</ta>
            <ta e="T773" id="Seg_11957" s="T767">[The other] says: "No, my horses!"</ta>
            <ta e="T777" id="Seg_11958" s="T773">”Well, ask the river man!”.</ta>
            <ta e="T781" id="Seg_11959" s="T777">He asked: ”Kodur’s horses?”.</ta>
            <ta e="T785" id="Seg_11960" s="T781">"Then ask the taiga man!"</ta>
            <ta e="T789" id="Seg_11961" s="T785">He asked: ”Kodur’s horses!”.</ta>
            <ta e="T792" id="Seg_11962" s="T789">He let [them] go.</ta>
            <ta e="T795" id="Seg_11963" s="T792">Then cows are wandering.</ta>
            <ta e="T801" id="Seg_11964" s="T795">And there is also a man looking after the cows.</ta>
            <ta e="T805" id="Seg_11965" s="T801">He came: ”My cows!".</ta>
            <ta e="T808" id="Seg_11966" s="T805">"No, not yours!".</ta>
            <ta e="T812" id="Seg_11967" s="T808">”Well, ask the man by the river!".</ta>
            <ta e="T816" id="Seg_11968" s="T812">Then he asked [the man] in the taiga:</ta>
            <ta e="T818" id="Seg_11969" s="T816">"Whose cows [are these]?"</ta>
            <ta e="T823" id="Seg_11970" s="T818">That [girl] said: "Kodur's cows!".</ta>
            <ta e="T826" id="Seg_11971" s="T823">And in the taiga he asked:</ta>
            <ta e="T828" id="Seg_11972" s="T826">"Whose cows [are these]?".</ta>
            <ta e="T830" id="Seg_11973" s="T828">”Kodur’s cows!”.</ta>
            <ta e="T835" id="Seg_11974" s="T830">Then he started to sing a song.</ta>
            <ta e="T840" id="Seg_11975" s="T835">"I took a dry shoulder blade.</ta>
            <ta e="T843" id="Seg_11976" s="T840">I took a sheep.</ta>
            <ta e="T847" id="Seg_11977" s="T843">For a sheep, I took two sheep.</ta>
            <ta e="T854" id="Seg_11978" s="T847">I took two sheep, I went, took [= dug out] a woman from the ground.</ta>
            <ta e="T863" id="Seg_11979" s="T854">And two sheep and wife(s?), the knife in the heart."</ta>
            <ta e="T868" id="Seg_11980" s="T864">[He] pushed the knife into the heart.</ta>
            <ta e="T878" id="Seg_11981" s="T868">And himself came: ”I have a wife and two sheep there”.</ta>
            <ta e="T884" id="Seg_11982" s="T878">And two daughters and a man are living.</ta>
            <ta e="T888" id="Seg_11983" s="T884">He tells the man…</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_11984" s="T0">(…) wegziehen.</ta>
            <ta e="T11" id="Seg_11985" s="T3">Ich gab ihm Pilze, Butter, Honig.</ta>
            <ta e="T15" id="Seg_11986" s="T11">Eins, zwei, drei, vier.</ta>
            <ta e="T21" id="Seg_11987" s="T15">Sie kauften ein Haus, jemand verkaufte es.</ta>
            <ta e="T25" id="Seg_11988" s="T21">Und sie hatten nicht genug Geld.</ta>
            <ta e="T32" id="Seg_11989" s="T26">Sie kauften dieses Haus.</ta>
            <ta e="T41" id="Seg_11990" s="T33">Wer konnte [ihnen] Geld leihen. [?]</ta>
            <ta e="T45" id="Seg_11991" s="T41">Jemand kaufte seine Kuh.</ta>
            <ta e="T53" id="Seg_11992" s="T45">Leute kauften alles, was sie hatten, und sie zahlten das Geld aus.</ta>
            <ta e="T72" id="Seg_11993" s="T54">Mein Verwandte ging in die Stadt, er kauft dort eine Maschine zum Waschen von Kleidung, um Hemden, Hosen, alles zu waschen, was es gibt.</ta>
            <ta e="T79" id="Seg_11994" s="T73">Ein Mann und ein Frau lebten sehr gut.</ta>
            <ta e="T83" id="Seg_11995" s="T79">Sie liebten [einander].</ta>
            <ta e="T87" id="Seg_11996" s="T83">Sie liebten[einander].</ta>
            <ta e="T88" id="Seg_11997" s="T87">(…).</ta>
            <ta e="T89" id="Seg_11998" s="T88">Sie küssen einander.</ta>
            <ta e="T96" id="Seg_11999" s="T89">Und sie hatten Kinder, ihre Kinder waren sehr artig. </ta>
            <ta e="T100" id="Seg_12000" s="T96">Was auch immer man ihnen sagt, sie gehorchen.</ta>
            <ta e="T107" id="Seg_12001" s="T101">Und unsere Kinder sind sehr unartig.</ta>
            <ta e="T111" id="Seg_12002" s="T107">Sie raufen, sie fluchen, sie gehorchen nicht.</ta>
            <ta e="T115" id="Seg_12003" s="T111">Ich schlage sie.</ta>
            <ta e="T120" id="Seg_12004" s="T116">Ich unterrichte Kinder im Schreiben.</ta>
            <ta e="T123" id="Seg_12005" s="T121">Kamassia lebten.</ta>
            <ta e="T130" id="Seg_12006" s="T123">Einige hatten sehr viel Geld, und einige hatten nichts.</ta>
            <ta e="T133" id="Seg_12007" s="T130">Er wird Vodka trinken.</ta>
            <ta e="T136" id="Seg_12008" s="T133">Er wird sein ganzes Geld versaufen.</ta>
            <ta e="T139" id="Seg_12009" s="T136">Dann hat [er] nichts.</ta>
            <ta e="T143" id="Seg_12010" s="T139">Er arbeitet und arbeitet, und versauft alles.</ta>
            <ta e="T150" id="Seg_12011" s="T145">Es leben sehr viele Kamassia in Siberien.</ta>
            <ta e="T154" id="Seg_12012" s="T150">Dann wuchs eine Birke.</ta>
            <ta e="T165" id="Seg_12013" s="T154">Sie sagen: „Also, russische Leute werden kommen, uns wird es nicht gut gehen.“</ta>
            <ta e="T168" id="Seg_12014" s="T165">Dann bauten sie Lagerhäuser.</ta>
            <ta e="T174" id="Seg_12015" s="T168">Sie (warfen?) Steine gegen sie, sie nahmen Axte.</ta>
            <ta e="T183" id="Seg_12016" s="T174">Sie hackten [es], und es fiel um, sie starben alle dort.</ta>
            <ta e="T192" id="Seg_12017" s="T184">Ich würde lieber sterben als solche Kinder [haben]. [?]</ta>
            <ta e="T196" id="Seg_12018" s="T192">Sie sind nicht artig, sie klauen alles.</ta>
            <ta e="T198" id="Seg_12019" s="T196">Sie raufen mit Leuten.</ta>
            <ta e="T208" id="Seg_12020" s="T199">Sie trinken Vodka [so viel], so das man sie nicht sehen will.</ta>
            <ta e="T220" id="Seg_12021" s="T209">Eine Frau hatte drei Söhne, zwei [waren] artig und einer [war] nicht artig.</ta>
            <ta e="T223" id="Seg_12022" s="T220">[Sie] schlugen ihn.</ta>
            <ta e="T234" id="Seg_12023" s="T223">Sie traten, und schlugen [ihn] mit Fäusten und der Rute.</ta>
            <ta e="T239" id="Seg_12024" s="T234">Und ich sage (?)…</ta>
            <ta e="T248" id="Seg_12025" s="T240">Und ich (sage?): „(?) ihn nicht, vielleicht wird er erwachsen.</ta>
            <ta e="T253" id="Seg_12026" s="T248">Er wird klug, er wird groß sein.</ta>
            <ta e="T259" id="Seg_12027" s="T253">Vielleicht wird er alles (aufgeben?), dann wird er artig sein.“</ta>
            <ta e="T270" id="Seg_12028" s="T260">Groß ist man genau so, wie wenn man klein war.</ta>
            <ta e="T273" id="Seg_12029" s="T270">Es ist nichts Gutes.</ta>
            <ta e="T277" id="Seg_12030" s="T274">Er ging zur Armee.</ta>
            <ta e="T285" id="Seg_12031" s="T277">Er kam zurück, und es war noch beim Alten: wo auch immer er zum Arbeiten ging, wurde er gefeuert.</ta>
            <ta e="T292" id="Seg_12032" s="T286">Eine Frau hatte einen Sohn, er ist ein Säufer.</ta>
            <ta e="T297" id="Seg_12033" s="T292">„Warum hast du mir nichts gelehrt?“</ta>
            <ta e="T307" id="Seg_12034" s="T297">Sie sagte: „Ich habe dir gelehrt, was willst du [mehr]?</ta>
            <ta e="T310" id="Seg_12035" s="T307">Du hattest ein Diplom.</ta>
            <ta e="T316" id="Seg_12036" s="T310">Aber du hast Vodka getrunken!“</ta>
            <ta e="T324" id="Seg_12037" s="T316">Er wollte seine Mutter mit (?) schlagen.</ta>
            <ta e="T328" id="Seg_12038" s="T324">Ander liessen dir es nicht [tun].</ta>
            <ta e="T334" id="Seg_12039" s="T329">Mein Bruder war im Krieg.</ta>
            <ta e="T338" id="Seg_12040" s="T334">Und er wurde gefangen genommen.</ta>
            <ta e="T345" id="Seg_12041" s="T338">Sie wollten seine rechte Hand abschneiden, samt den Fingern.</ta>
            <ta e="T355" id="Seg_12042" s="T345">Er weinte, sie schnitten ihm zwei Finger ab.</ta>
            <ta e="T364" id="Seg_12043" s="T356">Meine Geschwister sind drei Mädchen und drei Jungen. [?]</ta>
            <ta e="T370" id="Seg_12044" s="T365">Wollst du, dass ich dir weiße (?) erzähle?</ta>
            <ta e="T375" id="Seg_12045" s="T371">Hörst du, hörst du mir zu. [?]</ta>
            <ta e="T379" id="Seg_12046" s="T375">Willst du dass ich dir rote (?) erzähle?</ta>
            <ta e="T384" id="Seg_12047" s="T380">Hörst du, hörst du mir zu. [?]</ta>
            <ta e="T388" id="Seg_12048" s="T384">Willst du, dass ich dir gelbe (?) erzähle?</ta>
            <ta e="T392" id="Seg_12049" s="T388">Kodur, Kodur, Rotmantel.</ta>
            <ta e="T401" id="Seg_12050" s="T392">Er kam, Leute lebten dort, er fand ein Schulterblatt, es ist kein Fleisch [daran].</ta>
            <ta e="T404" id="Seg_12051" s="T401">Dann ging er, ging.</ta>
            <ta e="T410" id="Seg_12052" s="T404">Also, ein Mann lebt hier mit einer Frau.</ta>
            <ta e="T417" id="Seg_12053" s="T410">Er kam [zu ihnen], sie hatten ein Schaf.</ta>
            <ta e="T426" id="Seg_12054" s="T417">Er [einer von ihnen] sagt: „Wir sollten Tee kochen, um den Mann zu füttern.“</ta>
            <ta e="T434" id="Seg_12055" s="T426">Aber er sagt: „Koch nicht, ich habe Fleisch!“</ta>
            <ta e="T437" id="Seg_12056" s="T434">Dann hingen sie den Kessel auf.</ta>
            <ta e="T449" id="Seg_12057" s="T437">Sie streuten Salz, er ging, schlug sich auf die Nase, mit Blut, [bestrich] er das Schulterblatt mit [seinem] Blut.</ta>
            <ta e="T452" id="Seg_12058" s="T449">Dann warf er es in den Kessel.</ta>
            <ta e="T454" id="Seg_12059" s="T452">Es kocht, es kocht.</ta>
            <ta e="T459" id="Seg_12060" s="T454">Dann [schauen sie]: es ist kein Fleisch, nur das Schulterblatt [= der Knochen].</ta>
            <ta e="T465" id="Seg_12061" s="T459">Er fiel hin, fing an zu weinen.</ta>
            <ta e="T476" id="Seg_12062" s="T465">Sie sagten: „Nun werde ich ein Schaf töten, ich gebe dir das Schulterblatt.“</ta>
            <ta e="T480" id="Seg_12063" s="T476">„Ich brauche kein Schulterblatt!“</ta>
            <ta e="T484" id="Seg_12064" s="T480">Dann geben sie ihm ein Schaf.</ta>
            <ta e="T490" id="Seg_12065" s="T485">Dann sprang dieser Kodur auf.</ta>
            <ta e="T493" id="Seg_12066" s="T490">Er waschte, wischte sich die Augen.</ta>
            <ta e="T496" id="Seg_12067" s="T493">Er nahm sein Schaf, ging.</ta>
            <ta e="T501" id="Seg_12068" s="T496">Dann kam er zu Leuten.</ta>
            <ta e="T507" id="Seg_12069" s="T501">Hier waren auch zwei Schafe, (es) war ein Kerl.</ta>
            <ta e="T511" id="Seg_12070" s="T507">Und die Frau des Kerls.</ta>
            <ta e="T518" id="Seg_12071" s="T511">Dann sagen sie: „Lasse dein Schaf zu unseren Schafen!“</ta>
            <ta e="T526" id="Seg_12072" s="T518">„Nein, das lasse ich nicht, eure Schafe werden mein Schaf auffressen!“</ta>
            <ta e="T531" id="Seg_12073" s="T526">Aber selbst spricht und spricht.</ta>
            <ta e="T539" id="Seg_12074" s="T531">Er lief, ließ sein Schaf zu ihren (Schafen?).</ta>
            <ta e="T545" id="Seg_12075" s="T539">Dann legten sie sich zum Schlafen hin, am Morgen standen sie auf.</ta>
            <ta e="T548" id="Seg_12076" s="T545">Aber es ist kein Schaf.</ta>
            <ta e="T552" id="Seg_12077" s="T549">Dieser Kodur stand auf.</ta>
            <ta e="T554" id="Seg_12078" s="T552">Trennte sein Schaf.</ta>
            <ta e="T567" id="Seg_12079" s="T554">Und er beschmierte Nase und Mund ihrer Schafe mit [seinem] Blut.</ta>
            <ta e="T580" id="Seg_12080" s="T567">Aber am Morgen stand sie auf, sie sagten: „Gut, unsere Schafe haben sein Schaf gerissen.“</ta>
            <ta e="T584" id="Seg_12081" s="T581">Dann stand er auf.</ta>
            <ta e="T586" id="Seg_12082" s="T584">Wusch sich die Augen.</ta>
            <ta e="T587" id="Seg_12083" s="T586">Wischte.</ta>
            <ta e="T595" id="Seg_12084" s="T587">Nahm zwei Schafe und ging, haute ab.</ta>
            <ta e="T598" id="Seg_12085" s="T595">Dann kommt er, kommt.</ta>
            <ta e="T602" id="Seg_12086" s="T598">Es steht ein hölzernes Kreuz. </ta>
            <ta e="T613" id="Seg_12087" s="T602">Er grub, es liegt eine Frau, er nahm die Frau und zwei Schafe.</ta>
            <ta e="T620" id="Seg_12088" s="T613">Und er band die Schafe an die Hände der Frau.</ta>
            <ta e="T625" id="Seg_12089" s="T620">Er stach das Messer in ihr Herz.</ta>
            <ta e="T634" id="Seg_12090" s="T625">Dann kam er, es lebten zwei Töchter und ein Mann.</ta>
            <ta e="T641" id="Seg_12091" s="T634">Er spricht, er spricht: „Meine Frau sitzt dort mit Schafen.“</ta>
            <ta e="T647" id="Seg_12092" s="T641">Aber sie selbst hat ein Messer im Herzen sitzen.</ta>
            <ta e="T654" id="Seg_12093" s="T647">Aber die Töchter: „Wir werden gehen, deine Frau holen.“</ta>
            <ta e="T657" id="Seg_12094" s="T654">„Sie ist sehr schüchtern.</ta>
            <ta e="T662" id="Seg_12095" s="T657">Sie könnte sich [mit dem Messer] stechen.“</ta>
            <ta e="T667" id="Seg_12096" s="T662">Dann rannten diese Töchter, kamen.</ta>
            <ta e="T673" id="Seg_12097" s="T667">„Sie hat [sich] gestochen, die Schafe sind fortgerannt, (die Frau?).“ </ta>
            <ta e="T680" id="Seg_12098" s="T673">Dann fiel er hin – Kodur – weinte, weinte.</ta>
            <ta e="T686" id="Seg_12099" s="T680">Dieser Mann sagt: „Nimm eine [meiner] Töchter.“</ta>
            <ta e="T693" id="Seg_12100" s="T686">„Wozu brauche ich eine, meine Frau war besser.“</ta>
            <ta e="T696" id="Seg_12101" s="T693">„Also, dann nimm beide!“</ta>
            <ta e="T699" id="Seg_12102" s="T696">Dann stand er auf.</ta>
            <ta e="T702" id="Seg_12103" s="T699">Er wusch sich wieder das Gesicht.</ta>
            <ta e="T708" id="Seg_12104" s="T702">Er nahm die zwei Töchter und ging.</ta>
            <ta e="T714" id="Seg_12105" s="T708">Dann kommt, kommt, es grasen Schafe.</ta>
            <ta e="T720" id="Seg_12106" s="T714">Dann kam er zum Schäfer: „Da [sind] meine Schafe!“</ta>
            <ta e="T721" id="Seg_12107" s="T720">Ja!</ta>
            <ta e="T731" id="Seg_12108" s="T722">Er setzte eines am Flussufer, ein im Wald.</ta>
            <ta e="T735" id="Seg_12109" s="T731">Dann sagt er: „Meine Schafe!“</ta>
            <ta e="T737" id="Seg_12110" s="T735">„Also, frag (ihn/ihr?!).</ta>
            <ta e="T740" id="Seg_12111" s="T737">Frag eines [von ihnen]!“</ta>
            <ta e="T742" id="Seg_12112" s="T740">Er fragte.</ta>
            <ta e="T744" id="Seg_12113" s="T742">„[Diese sind] Kodur’s Schafe!“</ta>
            <ta e="T748" id="Seg_12114" s="T744">„Nun frag den Taiga-Mann.“</ta>
            <ta e="T753" id="Seg_12115" s="T748">Er fragte, jene [Person] sagte: „Kodurs Schafe“.</ta>
            <ta e="T757" id="Seg_12116" s="T753">Dann nahm er seine Schafe.</ta>
            <ta e="T765" id="Seg_12117" s="T758">Wieder kam er, ein Mann [und] Pferde leben.</ta>
            <ta e="T767" id="Seg_12118" s="T765">„Meine Pferde!“</ta>
            <ta e="T773" id="Seg_12119" s="T767">Aber er sagt: „Nein, meine Pferde!“</ta>
            <ta e="T777" id="Seg_12120" s="T773">„Also, frag den Fluss-Mann!“</ta>
            <ta e="T781" id="Seg_12121" s="T777">Er fragte: „Kodurs Pferde?“</ta>
            <ta e="T785" id="Seg_12122" s="T781">„Dann frag den Taiga-Mann!“</ta>
            <ta e="T789" id="Seg_12123" s="T785">Sie/Er fragte: „Kodurs Pferde!“.</ta>
            <ta e="T792" id="Seg_12124" s="T789">Er ließ [sie] gehen.</ta>
            <ta e="T795" id="Seg_12125" s="T792">Dann wandern Kühe.</ta>
            <ta e="T801" id="Seg_12126" s="T795">Und es ist auch ein Mann, der nach die Kühe schaut.</ta>
            <ta e="T805" id="Seg_12127" s="T801">Er kam: „Meine Kühe!“.</ta>
            <ta e="T808" id="Seg_12128" s="T805">„Nein, nicht deine!“</ta>
            <ta e="T812" id="Seg_12129" s="T808">„Also, frag den Mann am Fluss!“.</ta>
            <ta e="T816" id="Seg_12130" s="T812">Dann in der Taiga fragte er:</ta>
            <ta e="T818" id="Seg_12131" s="T816">„Wessen Kühe [sind diese]?“</ta>
            <ta e="T823" id="Seg_12132" s="T818">Er sagte: „Kodurs Kühe!“.</ta>
            <ta e="T826" id="Seg_12133" s="T823">Und in der Taiga fragte er:</ta>
            <ta e="T828" id="Seg_12134" s="T826">„Wessen Kühe [sind diese]?“</ta>
            <ta e="T830" id="Seg_12135" s="T828">„Kodurs Kühe!“.</ta>
            <ta e="T835" id="Seg_12136" s="T830">Dann fing er an, ein Lied zu singen.</ta>
            <ta e="T840" id="Seg_12137" s="T835">„Ich nahm ein trockenes Schulterblatt.</ta>
            <ta e="T843" id="Seg_12138" s="T840">Ich nahm ein Schaf.</ta>
            <ta e="T847" id="Seg_12139" s="T843">Für ein Schaf, nahm ich zwei Schafe.</ta>
            <ta e="T854" id="Seg_12140" s="T847">Ich nahm zwei Schafe, ich ging, nahm [= grub aus] eine Frau aus der Erde.</ta>
            <ta e="T863" id="Seg_12141" s="T854">… und zwei Schafe und Frau(en?), das Messer im Herzen.“</ta>
            <ta e="T868" id="Seg_12142" s="T864">[Er] drückte das Messer ins Herz.</ta>
            <ta e="T878" id="Seg_12143" s="T868">Aber er selbst kam: „Ich habe eine Frau und zwei Schafe dort“.</ta>
            <ta e="T884" id="Seg_12144" s="T878">Und zwei Töchter und ein Mann leben.</ta>
            <ta e="T888" id="Seg_12145" s="T884">Er erzählt dem Mann…</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T32" id="Seg_12146" s="T26">[GVY:] sadarla ibiʔi: a calque from Turkic (cf. Khak. садын ал-)?</ta>
            <ta e="T41" id="Seg_12147" s="T33">[GVY:] Unclear, the translation of this fragment is very tentative.</ta>
            <ta e="T72" id="Seg_12148" s="T54">[GVY:] or is barombi a separate word?</ta>
            <ta e="T83" id="Seg_12149" s="T79">[GVY:] ag'in.</ta>
            <ta e="T88" id="Seg_12150" s="T87">[GVY:] Kamrolaʔbəʔi?</ta>
            <ta e="T150" id="Seg_12151" s="T145">[GVY:] It seems that nuzaŋ rather refers to all aboriginal peoples, as opposed to kazak- 'Russians'.</ta>
            <ta e="T234" id="Seg_12152" s="T223">[GVY:] muruzuksi</ta>
            <ta e="T248" id="Seg_12153" s="T240">[GVY:] можеть. </ta>
            <ta e="T253" id="Seg_12154" s="T248">[GVY:] KPZ regularly uses sagəštə as 'clever'.</ta>
            <ta e="T364" id="Seg_12155" s="T356">[GVY:] Or 'my brother has three daughters and one son'?</ta>
            <ta e="T545" id="Seg_12156" s="T539">[GVY:] nuʔpiʔi.</ta>
            <ta e="T554" id="Seg_12157" s="T552">[GVY:] Or slaughtered?</ta>
            <ta e="T673" id="Seg_12158" s="T667">[GVY:] The function of the last word is not clear.</ta>
            <ta e="T731" id="Seg_12159" s="T722">[GVY:] amnolbiʔi? Unclear, maybe some fragment is missing. [AAV] cf. PKZ_196X_FireBird_flk.131 (135)</ta>
            <ta e="T740" id="Seg_12160" s="T737">[GVY:] adʼinnovo - maybe a result of a hesitation (один… одного).</ta>
            <ta e="T777" id="Seg_12161" s="T773">[GVY:] bünə = bün [water-GEN]?</ta>
            <ta e="T792" id="Seg_12162" s="T789">[GVY:] the owner of the horses gave up the horses to Kodur? || </ta>
            <ta e="T812" id="Seg_12163" s="T808">[GVY:] büʔən</ta>
            <ta e="T828" id="Seg_12164" s="T826">[GVY:] sindin</ta>
            <ta e="T863" id="Seg_12165" s="T854">[GVY:] side [AAV] dagajšə (seems to be one word)</ta>
            <ta e="T868" id="Seg_12166" s="T864">[AAV] dagajšə again</ta>
            <ta e="T878" id="Seg_12167" s="T868">[AAV] very low speed</ta>
            <ta e="T888" id="Seg_12168" s="T884">[AAV] zʼăbaktərja [AAV] continued in PKZ_196X_SU0227, tape AEDKL SU0227</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T889" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T892" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T891" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T890" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
