<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDE356FFA5-71DE-D635-624F-243C1A3904EB">
   <head>
      <meta-information>
         <project-name>Kamas</project-name>
         <transcription-name>PKZ_19700819_09344-1az</transcription-name>
         <referenced-file url="PKZ_19700819_09344-1az.wav" />
         <referenced-file url="PKZ_19700819_09344-1az.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_19700819_09344-1az\PKZ_19700819_09344-1az.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">396</ud-information>
            <ud-information attribute-name="# HIAT:w">245</ud-information>
            <ud-information attribute-name="# e">250</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">5</ud-information>
            <ud-information attribute-name="# HIAT:u">47</ud-information>
            <ud-information attribute-name="# sc">80</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="KA">
            <abbreviation>KA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.28" type="appl" />
         <tli id="T2" time="0.7366666666666666" type="appl" />
         <tli id="T3" time="1.1933333333333334" type="appl" />
         <tli id="T4" time="1.7633303074264262" />
         <tli id="T5" time="1.91" type="appl" />
         <tli id="T6" time="2.5533333333333332" type="appl" />
         <tli id="T7" time="3.1966666666666663" type="appl" />
         <tli id="T8" time="3.84" type="appl" />
         <tli id="T9" time="3.92" type="appl" />
         <tli id="T10" time="3.97" type="appl" />
         <tli id="T11" time="4.783333333333333" type="appl" />
         <tli id="T12" time="5.6466666666666665" type="appl" />
         <tli id="T13" time="6.11" type="appl" />
         <tli id="T14" time="6.756660157102043" />
         <tli id="T15" time="6.91" type="appl" />
         <tli id="T16" time="12.02" type="appl" />
         <tli id="T17" time="12.03" type="appl" />
         <tli id="T18" time="19.744" type="appl" />
         <tli id="T19" time="20.65" type="appl" />
         <tli id="T20" time="21.0175" type="appl" />
         <tli id="T21" time="21.384999999999998" type="appl" />
         <tli id="T22" time="21.7525" type="appl" />
         <tli id="T23" time="22.233329642456983" />
         <tli id="T24" time="22.31" type="appl" />
         <tli id="T25" time="22.91" type="appl" />
         <tli id="T26" time="23.509999999999998" type="appl" />
         <tli id="T27" time="24.11" type="appl" />
         <tli id="T28" time="24.709999999999997" type="appl" />
         <tli id="T29" time="25.31" type="appl" />
         <tli id="T30" time="25.75307692307692" type="appl" />
         <tli id="T31" time="26.196153846153845" type="appl" />
         <tli id="T32" time="26.639230769230767" type="appl" />
         <tli id="T33" time="27.08230769230769" type="appl" />
         <tli id="T34" time="27.525384615384613" type="appl" />
         <tli id="T35" time="27.96846153846154" type="appl" />
         <tli id="T36" time="28.411538461538463" type="appl" />
         <tli id="T37" time="28.854615384615386" type="appl" />
         <tli id="T38" time="29.27" type="appl" />
         <tli id="T39" time="29.29769230769231" type="appl" />
         <tli id="T40" time="29.65125" type="appl" />
         <tli id="T41" time="29.74076923076923" type="appl" />
         <tli id="T42" time="30.0325" type="appl" />
         <tli id="T43" time="30.183846153846154" type="appl" />
         <tli id="T44" time="30.41375" type="appl" />
         <tli id="T45" time="30.626923076923077" type="appl" />
         <tli id="T46" time="30.795" type="appl" />
         <tli id="T47" time="31.07" type="appl" />
         <tli id="T48" time="31.17625" type="appl" />
         <tli id="T49" time="31.5575" type="appl" />
         <tli id="T50" time="31.7" type="appl" />
         <tli id="T51" time="31.93875" type="appl" />
         <tli id="T52" time="32.10666666666667" type="appl" />
         <tli id="T53" time="32.32" type="appl" />
         <tli id="T54" time="32.49" type="appl" />
         <tli id="T55" time="32.513333333333335" type="appl" />
         <tli id="T56" time="32.92" type="appl" />
         <tli id="T57" time="33.115" type="appl" />
         <tli id="T58" time="33.74" type="appl" />
         <tli id="T59" time="34.365" type="appl" />
         <tli id="T60" time="34.44" type="appl" />
         <tli id="T61" time="34.99" type="appl" />
         <tli id="T62" time="35.05" type="appl" />
         <tli id="T63" time="35.52" type="appl" />
         <tli id="T64" time="35.72" type="appl" />
         <tli id="T65" time="36.050000000000004" type="appl" />
         <tli id="T66" time="36.580000000000005" type="appl" />
         <tli id="T67" time="37.11" type="appl" />
         <tli id="T68" time="37.40667155996892" />
         <tli id="T69" time="37.42667430081158" />
         <tli id="T70" time="37.946" type="appl" />
         <tli id="T71" time="38.17" type="appl" />
         <tli id="T72" time="38.172" type="appl" />
         <tli id="T73" time="38.398" type="appl" />
         <tli id="T74" time="38.624" type="appl" />
         <tli id="T75" time="38.7" type="appl" />
         <tli id="T76" time="39.15665911154277" />
         <tli id="T77" time="39.21857142857143" type="appl" />
         <tli id="T78" time="39.58714285714286" type="appl" />
         <tli id="T79" time="39.955714285714286" type="appl" />
         <tli id="T80" time="40.324285714285715" type="appl" />
         <tli id="T81" time="40.69285714285714" type="appl" />
         <tli id="T82" time="41.06142857142857" type="appl" />
         <tli id="T83" time="41.43" type="appl" />
         <tli id="T84" time="45.24" type="appl" />
         <tli id="T85" time="45.885000000000005" type="appl" />
         <tli id="T86" time="46.44" type="appl" />
         <tli id="T87" time="46.53" type="appl" />
         <tli id="T88" time="46.995" type="appl" />
         <tli id="T89" time="47.55" type="appl" />
         <tli id="T90" time="48.54" type="appl" />
         <tli id="T91" time="49.605000000000004" type="appl" />
         <tli id="T92" time="50.67" type="appl" />
         <tli id="T93" time="51.17" type="appl" />
         <tli id="T94" time="51.99333333333333" type="appl" />
         <tli id="T95" time="52.81666666666667" type="appl" />
         <tli id="T96" time="53.64" type="appl" />
         <tli id="T97" time="54.46" type="appl" />
         <tli id="T98" time="55.28" type="appl" />
         <tli id="T99" time="56.1" type="appl" />
         <tli id="T100" time="56.913334664355055" />
         <tli id="T101" time="56.920001159594385" />
         <tli id="T102" time="57.979166666666664" type="appl" />
         <tli id="T103" time="59.01833333333333" type="appl" />
         <tli id="T104" time="60.0575" type="appl" />
         <tli id="T105" time="61.096666666666664" type="appl" />
         <tli id="T106" time="62.13583333333333" type="appl" />
         <tli id="T107" time="63.175" type="appl" />
         <tli id="T108" time="64.21416666666667" type="appl" />
         <tli id="T109" time="65.25333333333333" type="appl" />
         <tli id="T110" time="66.29249999999999" type="appl" />
         <tli id="T111" time="67.33166666666666" type="appl" />
         <tli id="T112" time="68.37083333333334" type="appl" />
         <tli id="T113" time="69.41" type="appl" />
         <tli id="T114" time="69.46" type="appl" />
         <tli id="T115" time="70.36166666666666" type="appl" />
         <tli id="T116" time="71.26333333333334" type="appl" />
         <tli id="T117" time="72.16499999999999" type="appl" />
         <tli id="T118" time="73.06666666666666" type="appl" />
         <tli id="T119" time="73.96833333333333" type="appl" />
         <tli id="T120" time="74.87" type="appl" />
         <tli id="T121" time="77.43" type="appl" />
         <tli id="T122" time="78.14285714285715" type="appl" />
         <tli id="T123" time="78.85571428571428" type="appl" />
         <tli id="T124" time="79.56857142857143" type="appl" />
         <tli id="T125" time="80.28142857142858" type="appl" />
         <tli id="T126" time="80.99428571428572" type="appl" />
         <tli id="T127" time="81.70714285714286" type="appl" />
         <tli id="T128" time="82.42" type="appl" />
         <tli id="T129" time="82.43" type="appl" />
         <tli id="T130" time="83.64722222222223" type="appl" />
         <tli id="T131" time="84.86444444444444" type="appl" />
         <tli id="T132" time="86.08166666666668" type="appl" />
         <tli id="T133" time="87.2988888888889" type="appl" />
         <tli id="T134" time="88.51611111111112" type="appl" />
         <tli id="T135" time="89.73333333333333" type="appl" />
         <tli id="T136" time="90.95055555555555" type="appl" />
         <tli id="T137" time="92.16777777777779" type="appl" />
         <tli id="T138" time="93.385" type="appl" />
         <tli id="T139" time="94.72" type="appl" />
         <tli id="T140" time="96.02" type="appl" />
         <tli id="T141" time="96.6" type="appl" />
         <tli id="T142" time="96.90266666666666" type="appl" />
         <tli id="T143" time="97.20533333333333" type="appl" />
         <tli id="T144" time="97.508" type="appl" />
         <tli id="T145" time="97.81066666666666" type="appl" />
         <tli id="T146" time="98.11333333333333" type="appl" />
         <tli id="T147" time="98.416" type="appl" />
         <tli id="T148" time="98.71866666666666" type="appl" />
         <tli id="T149" time="99.02133333333333" type="appl" />
         <tli id="T150" time="99.324" type="appl" />
         <tli id="T151" time="99.62666666666667" type="appl" />
         <tli id="T152" time="99.92933333333333" type="appl" />
         <tli id="T153" time="100.232" type="appl" />
         <tli id="T154" time="100.53466666666667" type="appl" />
         <tli id="T155" time="100.83733333333333" type="appl" />
         <tli id="T156" time="101.14" type="appl" />
         <tli id="T157" time="101.16" type="appl" />
         <tli id="T158" time="101.70318181818182" type="appl" />
         <tli id="T159" time="102.24636363636364" type="appl" />
         <tli id="T160" time="102.78954545454545" type="appl" />
         <tli id="T161" time="103.33272727272727" type="appl" />
         <tli id="T162" time="103.87590909090909" type="appl" />
         <tli id="T163" time="104.41909090909091" type="appl" />
         <tli id="T164" time="104.96227272727272" type="appl" />
         <tli id="T165" time="105.50545454545454" type="appl" />
         <tli id="T166" time="106.04863636363636" type="appl" />
         <tli id="T167" time="106.59181818181818" type="appl" />
         <tli id="T168" time="107.13499999999999" type="appl" />
         <tli id="T169" time="107.67818181818181" type="appl" />
         <tli id="T170" time="108.22136363636363" type="appl" />
         <tli id="T171" time="108.76454545454546" type="appl" />
         <tli id="T172" time="109.30772727272728" type="appl" />
         <tli id="T173" time="109.85090909090908" type="appl" />
         <tli id="T174" time="110.3940909090909" type="appl" />
         <tli id="T175" time="110.93727272727273" type="appl" />
         <tli id="T176" time="111.48045454545455" type="appl" />
         <tli id="T177" time="112.02363636363637" type="appl" />
         <tli id="T178" time="112.56681818181818" type="appl" />
         <tli id="T179" time="113.11" type="appl" />
         <tli id="T180" time="114.0" type="appl" />
         <tli id="T181" time="114.75766666666667" type="appl" />
         <tli id="T182" time="115.51533333333333" type="appl" />
         <tli id="T183" time="116.273" type="appl" />
         <tli id="T184" time="117.03066666666666" type="appl" />
         <tli id="T185" time="117.78833333333334" type="appl" />
         <tli id="T186" time="118.546" type="appl" />
         <tli id="T187" time="119.30366666666667" type="appl" />
         <tli id="T188" time="120.06133333333334" type="appl" />
         <tli id="T189" time="120.85233501957134" />
         <tli id="T190" time="120.88" type="appl" />
         <tli id="T191" time="121.58" type="appl" />
         <tli id="T192" time="122.28" type="appl" />
         <tli id="T193" time="122.98" type="appl" />
         <tli id="T194" time="123.68" type="appl" />
         <tli id="T195" time="123.88" type="appl" />
         <tli id="T196" time="124.51333333333334" type="appl" />
         <tli id="T197" time="125.14666666666666" type="appl" />
         <tli id="T198" time="125.78" type="appl" />
         <tli id="T199" time="126.41333333333334" type="appl" />
         <tli id="T200" time="127.04666666666667" type="appl" />
         <tli id="T201" time="127.68" type="appl" />
         <tli id="T202" time="128.31333333333333" type="appl" />
         <tli id="T203" time="128.9466666666667" type="appl" />
         <tli id="T204" time="129.58" type="appl" />
         <tli id="T205" time="130.02" type="appl" />
         <tli id="T206" time="130.91" type="appl" />
         <tli id="T207" time="131.8" type="appl" />
         <tli id="T208" time="132.69" type="appl" />
         <tli id="T209" time="133.58" type="appl" />
         <tli id="T210" time="134.47" type="appl" />
         <tli id="T211" time="134.95" type="appl" />
         <tli id="T212" time="135.43" type="appl" />
         <tli id="T213" time="135.91" type="appl" />
         <tli id="T214" time="136.06" type="appl" />
         <tli id="T215" time="136.54777777777778" type="appl" />
         <tli id="T216" time="137.03555555555556" type="appl" />
         <tli id="T217" time="137.52333333333334" type="appl" />
         <tli id="T218" time="138.01111111111112" type="appl" />
         <tli id="T219" time="138.49888888888887" type="appl" />
         <tli id="T220" time="138.98666666666665" type="appl" />
         <tli id="T221" time="139.47444444444443" type="appl" />
         <tli id="T222" time="139.9622222222222" type="appl" />
         <tli id="T223" time="140.45" type="appl" />
         <tli id="T224" time="141.25" type="appl" />
         <tli id="T225" time="141.92333333333335" type="appl" />
         <tli id="T226" time="142.59666666666666" type="appl" />
         <tli id="T227" time="143.27" type="appl" />
         <tli id="T228" time="143.34" type="appl" />
         <tli id="T229" time="144.165" type="appl" />
         <tli id="T230" time="144.99" type="appl" />
         <tli id="T231" time="145.815" type="appl" />
         <tli id="T232" time="146.64" type="appl" />
         <tli id="T233" time="146.77" type="appl" />
         <tli id="T234" time="147.58285714285716" type="appl" />
         <tli id="T235" time="148.3957142857143" type="appl" />
         <tli id="T236" time="149.20857142857145" type="appl" />
         <tli id="T237" time="150.02142857142857" type="appl" />
         <tli id="T238" time="150.83428571428573" type="appl" />
         <tli id="T239" time="151.64714285714285" type="appl" />
         <tli id="T240" time="152.46" type="appl" />
         <tli id="T241" time="152.65" type="appl" />
         <tli id="T242" time="153.595" type="appl" />
         <tli id="T243" time="154.54000000000002" type="appl" />
         <tli id="T244" time="155.485" type="appl" />
         <tli id="T245" time="156.43" type="appl" />
         <tli id="T246" time="156.6" type="appl" />
         <tli id="T247" time="157.63" type="appl" />
         <tli id="T248" time="158.66" type="appl" />
         <tli id="T249" time="158.78" type="appl" />
         <tli id="T250" time="159.34142857142857" type="appl" />
         <tli id="T251" time="159.90285714285716" type="appl" />
         <tli id="T252" time="160.46428571428572" type="appl" />
         <tli id="T253" time="161.0257142857143" type="appl" />
         <tli id="T254" time="161.58714285714285" type="appl" />
         <tli id="T255" time="162.14857142857144" type="appl" />
         <tli id="T256" time="162.71" type="appl" />
         <tli id="T257" time="162.99" type="appl" />
         <tli id="T258" time="163.732" type="appl" />
         <tli id="T259" time="164.474" type="appl" />
         <tli id="T260" time="165.216" type="appl" />
         <tli id="T261" time="165.958" type="appl" />
         <tli id="T262" time="166.48" type="appl" />
         <tli id="T263" time="167.20044444444443" type="appl" />
         <tli id="T264" time="167.92088888888887" type="appl" />
         <tli id="T265" time="168.64133333333334" type="appl" />
         <tli id="T266" time="169.36177777777777" type="appl" />
         <tli id="T267" time="170.0822222222222" type="appl" />
         <tli id="T268" time="170.80266666666665" type="appl" />
         <tli id="T269" time="171.52311111111112" type="appl" />
         <tli id="T270" time="172.24355555555556" type="appl" />
         <tli id="T271" time="172.964" type="appl" />
         <tli id="T272" time="177.92" type="appl" />
         <tli id="T273" time="178.61" type="appl" />
         <tli id="T274" time="181.23" type="appl" />
         <tli id="T275" time="181.5" type="appl" />
         <tli id="T276" time="181.76999999999998" type="appl" />
         <tli id="T277" time="182.04" type="appl" />
         <tli id="T278" time="182.31" type="appl" />
         <tli id="T279" time="182.57999999999998" type="appl" />
         <tli id="T280" time="182.85" type="appl" />
         <tli id="T281" time="183.11285714285714" type="appl" />
         <tli id="T282" time="183.37571428571428" type="appl" />
         <tli id="T283" time="183.63857142857142" type="appl" />
         <tli id="T284" time="183.90142857142857" type="appl" />
         <tli id="T285" time="184.1642857142857" type="appl" />
         <tli id="T286" time="184.42714285714285" type="appl" />
         <tli id="T287" time="185.5352290061583" />
         <tli id="T288" time="185.5633272419522" />
         <tli id="T289" time="186.405" type="appl" />
         <tli id="T290" time="187.34" type="appl" />
         <tli id="T0" time="190.795" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-PKZ"
                      id="tx-PKZ"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PKZ">
            <ts e="T4" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">он</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">на</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">кровати</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T8" id="Seg_13" n="sc" s="T5">
               <ts e="T8" id="Seg_15" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_17" n="HIAT:w" s="T5">Черный</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_20" n="HIAT:w" s="T6">как</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_23" n="HIAT:w" s="T7">головешка</ts>
                  <nts id="Seg_24" n="HIAT:ip">.</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T14" id="Seg_26" n="sc" s="T9">
               <ts e="T14" id="Seg_28" n="HIAT:u" s="T9">
                  <ts e="T11" id="Seg_30" n="HIAT:w" s="T9">И</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_33" n="HIAT:w" s="T11">схоронили</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_36" n="HIAT:w" s="T12">обоих</ts>
                  <nts id="Seg_37" n="HIAT:ip">.</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T53" id="Seg_39" n="sc" s="T38">
               <ts e="T53" id="Seg_41" n="HIAT:u" s="T38">
                  <ts e="T40" id="Seg_43" n="HIAT:w" s="T38">Нет</ts>
                  <nts id="Seg_44" n="HIAT:ip">,</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_47" n="HIAT:w" s="T40">не</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_50" n="HIAT:w" s="T42">магазин</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_53" n="HIAT:w" s="T44">не</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_56" n="HIAT:w" s="T46">сгорел</ts>
                  <nts id="Seg_57" n="HIAT:ip">,</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_60" n="HIAT:w" s="T48">а</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_63" n="HIAT:w" s="T49">изба</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_66" n="HIAT:w" s="T51">сгорела</ts>
                  <nts id="Seg_67" n="HIAT:ip">.</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T75" id="Seg_69" n="sc" s="T54">
               <ts e="T61" id="Seg_71" n="HIAT:u" s="T54">
                  <ts e="T57" id="Seg_73" n="HIAT:w" s="T54">Магазин</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_76" n="HIAT:w" s="T57">сейчас</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_79" n="HIAT:w" s="T58">тот</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_82" n="HIAT:w" s="T59">стоит</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_86" n="HIAT:u" s="T61">
                  <ts e="T63" id="Seg_88" n="HIAT:w" s="T61">Там</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_91" n="HIAT:w" s="T63">тоже</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_94" n="HIAT:w" s="T65">кладут</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_97" n="HIAT:w" s="T66">соль</ts>
                  <nts id="Seg_98" n="HIAT:ip">,</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_101" n="HIAT:w" s="T67">да</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_104" n="HIAT:w" s="T68">чай</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_106" n="HIAT:ip">(</nts>
                  <nts id="Seg_107" n="HIAT:ip">(</nts>
                  <ats e="T75" id="Seg_108" n="HIAT:non-pho" s="T71">…</ats>
                  <nts id="Seg_109" n="HIAT:ip">)</nts>
                  <nts id="Seg_110" n="HIAT:ip">)</nts>
                  <nts id="Seg_111" n="HIAT:ip">.</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T87" id="Seg_113" n="sc" s="T84">
               <ts e="T87" id="Seg_115" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_117" n="HIAT:w" s="T84">Наладил</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_120" n="HIAT:w" s="T85">уж</ts>
                  <nts id="Seg_121" n="HIAT:ip">?</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T92" id="Seg_123" n="sc" s="T90">
               <ts e="T92" id="Seg_125" n="HIAT:u" s="T90">
                  <nts id="Seg_126" n="HIAT:ip">(</nts>
                  <ts e="T91" id="Seg_128" n="HIAT:w" s="T90">Miʔ-</ts>
                  <nts id="Seg_129" n="HIAT:ip">)</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_132" n="HIAT:w" s="T91">Miʔnibeʔ</ts>
                  <nts id="Seg_133" n="HIAT:ip">…</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T100" id="Seg_135" n="sc" s="T93">
               <ts e="T96" id="Seg_137" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_139" n="HIAT:w" s="T93">Miʔnʼibeʔ</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_142" n="HIAT:w" s="T94">tura</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_145" n="HIAT:w" s="T95">nenʼəluʔpi</ts>
                  <nts id="Seg_146" n="HIAT:ip">.</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_149" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_151" n="HIAT:w" s="T96">Tüj</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_154" n="HIAT:w" s="T97">nuldəbiʔi</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_157" n="HIAT:w" s="T98">baška</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_160" n="HIAT:w" s="T99">magazin</ts>
                  <nts id="Seg_161" n="HIAT:ip">.</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T113" id="Seg_163" n="sc" s="T101">
               <ts e="T113" id="Seg_165" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_167" n="HIAT:w" s="T101">Dĭn</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_170" n="HIAT:w" s="T102">ara</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_173" n="HIAT:w" s="T103">sadarlaʔbəʔjə</ts>
                  <nts id="Seg_174" n="HIAT:ip">,</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_177" n="HIAT:w" s="T104">pivo</ts>
                  <nts id="Seg_178" n="HIAT:ip">,</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_181" n="HIAT:w" s="T105">vsjakoj</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_184" n="HIAT:w" s="T106">oldʼaʔi</ts>
                  <nts id="Seg_185" n="HIAT:ip">,</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_188" n="HIAT:w" s="T107">i</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_190" n="HIAT:ip">(</nts>
                  <ts e="T109" id="Seg_192" n="HIAT:w" s="T108">nĭʔnʼe</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_195" n="HIAT:w" s="T109">kö</ts>
                  <nts id="Seg_196" n="HIAT:ip">)</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_199" n="HIAT:w" s="T110">i</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_202" n="HIAT:w" s="T111">spirt</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_205" n="HIAT:w" s="T112">mĭlieʔi</ts>
                  <nts id="Seg_206" n="HIAT:ip">.</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T120" id="Seg_208" n="sc" s="T114">
               <ts e="T120" id="Seg_210" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_212" n="HIAT:w" s="T114">Girgit</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_215" n="HIAT:w" s="T115">idʼiʔe</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_218" n="HIAT:w" s="T116">bĭtliel</ts>
                  <nts id="Seg_219" n="HIAT:ip">,</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_222" n="HIAT:w" s="T117">ĭzəmzittə</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_225" n="HIAT:w" s="T118">ej</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_228" n="HIAT:w" s="T119">molial</ts>
                  <nts id="Seg_229" n="HIAT:ip">.</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T128" id="Seg_231" n="sc" s="T121">
               <ts e="T128" id="Seg_233" n="HIAT:u" s="T121">
                  <ts e="T122" id="Seg_235" n="HIAT:w" s="T121">Dĭ</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_238" n="HIAT:w" s="T122">tura</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_241" n="HIAT:w" s="T123">nenʼiluʔpi</ts>
                  <nts id="Seg_242" n="HIAT:ip">,</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_245" n="HIAT:w" s="T124">i</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_248" n="HIAT:w" s="T125">šide</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_251" n="HIAT:w" s="T126">kuza</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_254" n="HIAT:w" s="T127">nenʼiluʔpiʔi</ts>
                  <nts id="Seg_255" n="HIAT:ip">.</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T138" id="Seg_257" n="sc" s="T129">
               <ts e="T138" id="Seg_259" n="HIAT:u" s="T129">
                  <nts id="Seg_260" n="HIAT:ip">(</nts>
                  <ts e="T130" id="Seg_262" n="HIAT:w" s="T129">Dĭ</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_265" n="HIAT:w" s="T130">dĭzeŋ=</ts>
                  <nts id="Seg_266" n="HIAT:ip">)</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_269" n="HIAT:w" s="T131">Dĭzem</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_272" n="HIAT:w" s="T132">tĭlbiʔi</ts>
                  <nts id="Seg_273" n="HIAT:ip">,</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_276" n="HIAT:w" s="T133">embiʔi</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_279" n="HIAT:w" s="T134">dʼünə</ts>
                  <nts id="Seg_280" n="HIAT:ip">,</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_283" n="HIAT:w" s="T135">i</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_286" n="HIAT:w" s="T136">külambiʔi</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_289" n="HIAT:w" s="T137">dĭzeŋ</ts>
                  <nts id="Seg_290" n="HIAT:ip">.</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T140" id="Seg_292" n="sc" s="T139">
               <ts e="T140" id="Seg_294" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_296" n="HIAT:w" s="T139">Kabarləj</ts>
                  <nts id="Seg_297" n="HIAT:ip">.</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T189" id="Seg_299" n="sc" s="T180">
               <ts e="T189" id="Seg_301" n="HIAT:u" s="T180">
                  <ts e="T181" id="Seg_303" n="HIAT:w" s="T180">Dĭ</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_306" n="HIAT:w" s="T181">magazinnə</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_308" n="HIAT:ip">(</nts>
                  <nts id="Seg_309" n="HIAT:ip">(</nts>
                  <ats e="T183" id="Seg_310" n="HIAT:non-pho" s="T182">…</ats>
                  <nts id="Seg_311" n="HIAT:ip">)</nts>
                  <nts id="Seg_312" n="HIAT:ip">)</nts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_315" n="HIAT:w" s="T183">un</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_318" n="HIAT:w" s="T184">detleʔbəʔjə</ts>
                  <nts id="Seg_319" n="HIAT:ip">,</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_322" n="HIAT:w" s="T185">iʔgö</ts>
                  <nts id="Seg_323" n="HIAT:ip">,</nts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_326" n="HIAT:w" s="T186">i</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_329" n="HIAT:w" s="T187">dĭn</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_332" n="HIAT:w" s="T188">nuldlaʔbəʔjə</ts>
                  <nts id="Seg_333" n="HIAT:ip">.</nts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T194" id="Seg_335" n="sc" s="T190">
               <ts e="T194" id="Seg_337" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_339" n="HIAT:w" s="T190">I</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_341" n="HIAT:ip">(</nts>
                  <ts e="T192" id="Seg_343" n="HIAT:w" s="T191">il-</ts>
                  <nts id="Seg_344" n="HIAT:ip">)</nts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_347" n="HIAT:w" s="T192">ildə</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_350" n="HIAT:w" s="T193">sadarlaʔbəʔjə</ts>
                  <nts id="Seg_351" n="HIAT:ip">.</nts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T204" id="Seg_353" n="sc" s="T195">
               <ts e="T204" id="Seg_355" n="HIAT:u" s="T195">
                  <ts e="T196" id="Seg_357" n="HIAT:w" s="T195">Onʼiʔ</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_360" n="HIAT:w" s="T196">măn</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_363" n="HIAT:w" s="T197">mămbiam</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_366" n="HIAT:w" s="T198">dĭʔnə:</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_368" n="HIAT:ip">"</nts>
                  <ts e="T200" id="Seg_370" n="HIAT:w" s="T199">Măna</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_373" n="HIAT:w" s="T200">maːʔtə</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_376" n="HIAT:w" s="T201">onʼiʔ</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_379" n="HIAT:w" s="T202">bura</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_382" n="HIAT:w" s="T203">un</ts>
                  <nts id="Seg_383" n="HIAT:ip">"</nts>
                  <nts id="Seg_384" n="HIAT:ip">.</nts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T213" id="Seg_386" n="sc" s="T205">
               <ts e="T210" id="Seg_388" n="HIAT:u" s="T205">
                  <ts e="T206" id="Seg_390" n="HIAT:w" s="T205">Dĭgəttə</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_393" n="HIAT:w" s="T206">šobiam:</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_395" n="HIAT:ip">"</nts>
                  <nts id="Seg_396" n="HIAT:ip">(</nts>
                  <ts e="T208" id="Seg_398" n="HIAT:w" s="T207">Maː-</ts>
                  <nts id="Seg_399" n="HIAT:ip">)</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_402" n="HIAT:w" s="T208">Maːʔpi</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_405" n="HIAT:w" s="T209">un</ts>
                  <nts id="Seg_406" n="HIAT:ip">?</nts>
                  <nts id="Seg_407" n="HIAT:ip">"</nts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T213" id="Seg_410" n="HIAT:u" s="T210">
                  <ts e="T211" id="Seg_412" n="HIAT:w" s="T210">Dĭ</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_415" n="HIAT:w" s="T211">măndə:</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_417" n="HIAT:ip">"</nts>
                  <ts e="T213" id="Seg_419" n="HIAT:w" s="T212">Ej</ts>
                  <nts id="Seg_420" n="HIAT:ip">"</nts>
                  <nts id="Seg_421" n="HIAT:ip">.</nts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T223" id="Seg_423" n="sc" s="T214">
               <ts e="T223" id="Seg_425" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_427" n="HIAT:w" s="T214">Dĭgəttə</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_429" n="HIAT:ip">(</nts>
                  <ts e="T216" id="Seg_431" n="HIAT:w" s="T215">măm-</ts>
                  <nts id="Seg_432" n="HIAT:ip">)</nts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_435" n="HIAT:w" s="T216">măn</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_438" n="HIAT:w" s="T217">mămbiam:</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_440" n="HIAT:ip">"</nts>
                  <ts e="T219" id="Seg_442" n="HIAT:w" s="T218">Tănan</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_444" n="HIAT:ip">(</nts>
                  <ts e="T220" id="Seg_446" n="HIAT:w" s="T219">navernă</ts>
                  <nts id="Seg_447" n="HIAT:ip">)</nts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_450" n="HIAT:w" s="T220">aktʼa</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_453" n="HIAT:w" s="T221">mĭzittə</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_456" n="HIAT:w" s="T222">nada</ts>
                  <nts id="Seg_457" n="HIAT:ip">.</nts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T227" id="Seg_459" n="sc" s="T224">
               <ts e="T227" id="Seg_461" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_463" n="HIAT:w" s="T224">Dĭgəttə</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_466" n="HIAT:w" s="T225">tăn</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_469" n="HIAT:w" s="T226">maːtləl</ts>
                  <nts id="Seg_470" n="HIAT:ip">"</nts>
                  <nts id="Seg_471" n="HIAT:ip">.</nts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T232" id="Seg_473" n="sc" s="T228">
               <ts e="T232" id="Seg_475" n="HIAT:u" s="T228">
                  <ts e="T229" id="Seg_477" n="HIAT:w" s="T228">Dĭgəttə</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_480" n="HIAT:w" s="T229">măn</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_483" n="HIAT:w" s="T230">unukam</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_486" n="HIAT:w" s="T231">amnolaʔbə</ts>
                  <nts id="Seg_487" n="HIAT:ip">.</nts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T240" id="Seg_489" n="sc" s="T233">
               <ts e="T240" id="Seg_491" n="HIAT:u" s="T233">
                  <nts id="Seg_492" n="HIAT:ip">"</nts>
                  <ts e="T234" id="Seg_494" n="HIAT:w" s="T233">Ige</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_497" n="HIAT:w" s="T234">aktʼa</ts>
                  <nts id="Seg_498" n="HIAT:ip">,</nts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_501" n="HIAT:w" s="T235">măn</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_504" n="HIAT:w" s="T236">sadarlam</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_507" n="HIAT:w" s="T237">bostə</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_509" n="HIAT:ip">(</nts>
                  <ts e="T239" id="Seg_511" n="HIAT:w" s="T238">m-</ts>
                  <nts id="Seg_512" n="HIAT:ip">)</nts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_515" n="HIAT:w" s="T239">un</ts>
                  <nts id="Seg_516" n="HIAT:ip">"</nts>
                  <nts id="Seg_517" n="HIAT:ip">.</nts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T245" id="Seg_519" n="sc" s="T241">
               <ts e="T245" id="Seg_521" n="HIAT:u" s="T241">
                  <ts e="T242" id="Seg_523" n="HIAT:w" s="T241">Dĭgəttə</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_526" n="HIAT:w" s="T242">măn</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_529" n="HIAT:w" s="T243">dĭʔnə</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_532" n="HIAT:w" s="T244">mĭliem</ts>
                  <nts id="Seg_533" n="HIAT:ip">.</nts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T248" id="Seg_535" n="sc" s="T246">
               <ts e="T248" id="Seg_537" n="HIAT:u" s="T246">
                  <nts id="Seg_538" n="HIAT:ip">"</nts>
                  <ts e="T247" id="Seg_540" n="HIAT:w" s="T246">Kumen</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_543" n="HIAT:w" s="T247">aktʼa</ts>
                  <nts id="Seg_544" n="HIAT:ip">?</nts>
                  <nts id="Seg_545" n="HIAT:ip">"</nts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T256" id="Seg_547" n="sc" s="T249">
               <ts e="T256" id="Seg_549" n="HIAT:u" s="T249">
                  <ts e="T250" id="Seg_551" n="HIAT:w" s="T249">Dĭ</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_554" n="HIAT:w" s="T250">mămbi</ts>
                  <nts id="Seg_555" n="HIAT:ip">,</nts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_558" n="HIAT:w" s="T251">kumen</ts>
                  <nts id="Seg_559" n="HIAT:ip">,</nts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_562" n="HIAT:w" s="T252">măn</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_565" n="HIAT:w" s="T253">mĭbiem</ts>
                  <nts id="Seg_566" n="HIAT:ip">,</nts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_569" n="HIAT:w" s="T254">dĭgəttə</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_572" n="HIAT:w" s="T255">dĭ</ts>
                  <nts id="Seg_573" n="HIAT:ip">…</nts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T261" id="Seg_575" n="sc" s="T257">
               <ts e="T261" id="Seg_577" n="HIAT:u" s="T257">
                  <ts e="T258" id="Seg_579" n="HIAT:w" s="T257">Un</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_582" n="HIAT:w" s="T258">ibiem</ts>
                  <nts id="Seg_583" n="HIAT:ip">,</nts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_586" n="HIAT:w" s="T259">maːʔnʼi</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_589" n="HIAT:w" s="T260">kunlambiam</ts>
                  <nts id="Seg_590" n="HIAT:ip">.</nts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T271" id="Seg_592" n="sc" s="T262">
               <ts e="T271" id="Seg_594" n="HIAT:u" s="T262">
                  <ts e="T263" id="Seg_596" n="HIAT:w" s="T262">I</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_599" n="HIAT:w" s="T263">kămnəbiam</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_602" n="HIAT:w" s="T264">jašiktə</ts>
                  <nts id="Seg_603" n="HIAT:ip">,</nts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_606" n="HIAT:w" s="T265">dĭgəttə</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_608" n="HIAT:ip">(</nts>
                  <ts e="T267" id="Seg_610" n="HIAT:w" s="T266">de-</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_613" n="HIAT:w" s="T267">detle-</ts>
                  <nts id="Seg_614" n="HIAT:ip">)</nts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_617" n="HIAT:w" s="T268">deʔpiem</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_620" n="HIAT:w" s="T269">dĭʔnə</ts>
                  <nts id="Seg_621" n="HIAT:ip">,</nts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_624" n="HIAT:w" s="T270">mĭluʔpiam</ts>
                  <nts id="Seg_625" n="HIAT:ip">.</nts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T290" id="Seg_627" n="sc" s="T288">
               <ts e="T290" id="Seg_629" n="HIAT:u" s="T288">
                  <ts e="T289" id="Seg_631" n="HIAT:w" s="T288">Ну</ts>
                  <nts id="Seg_632" n="HIAT:ip">,</nts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_635" n="HIAT:w" s="T289">глядите</ts>
                  <nts id="Seg_636" n="HIAT:ip">.</nts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PKZ">
            <ts e="T4" id="Seg_638" n="sc" s="T1">
               <ts e="T2" id="Seg_640" n="e" s="T1">он </ts>
               <ts e="T3" id="Seg_642" n="e" s="T2">на </ts>
               <ts e="T4" id="Seg_644" n="e" s="T3">кровати. </ts>
            </ts>
            <ts e="T8" id="Seg_645" n="sc" s="T5">
               <ts e="T6" id="Seg_647" n="e" s="T5">Черный </ts>
               <ts e="T7" id="Seg_649" n="e" s="T6">как </ts>
               <ts e="T8" id="Seg_651" n="e" s="T7">головешка. </ts>
            </ts>
            <ts e="T14" id="Seg_652" n="sc" s="T9">
               <ts e="T11" id="Seg_654" n="e" s="T9">И </ts>
               <ts e="T12" id="Seg_656" n="e" s="T11">схоронили </ts>
               <ts e="T14" id="Seg_658" n="e" s="T12">обоих. </ts>
            </ts>
            <ts e="T53" id="Seg_659" n="sc" s="T38">
               <ts e="T40" id="Seg_661" n="e" s="T38">Нет, </ts>
               <ts e="T42" id="Seg_663" n="e" s="T40">не </ts>
               <ts e="T44" id="Seg_665" n="e" s="T42">магазин </ts>
               <ts e="T46" id="Seg_667" n="e" s="T44">не </ts>
               <ts e="T48" id="Seg_669" n="e" s="T46">сгорел, </ts>
               <ts e="T49" id="Seg_671" n="e" s="T48">а </ts>
               <ts e="T51" id="Seg_673" n="e" s="T49">изба </ts>
               <ts e="T53" id="Seg_675" n="e" s="T51">сгорела. </ts>
            </ts>
            <ts e="T75" id="Seg_676" n="sc" s="T54">
               <ts e="T57" id="Seg_678" n="e" s="T54">Магазин </ts>
               <ts e="T58" id="Seg_680" n="e" s="T57">сейчас </ts>
               <ts e="T59" id="Seg_682" n="e" s="T58">тот </ts>
               <ts e="T61" id="Seg_684" n="e" s="T59">стоит. </ts>
               <ts e="T63" id="Seg_686" n="e" s="T61">Там </ts>
               <ts e="T65" id="Seg_688" n="e" s="T63">тоже </ts>
               <ts e="T66" id="Seg_690" n="e" s="T65">кладут </ts>
               <ts e="T67" id="Seg_692" n="e" s="T66">соль, </ts>
               <ts e="T68" id="Seg_694" n="e" s="T67">да </ts>
               <ts e="T71" id="Seg_696" n="e" s="T68">чай </ts>
               <ts e="T75" id="Seg_698" n="e" s="T71">((…)). </ts>
            </ts>
            <ts e="T87" id="Seg_699" n="sc" s="T84">
               <ts e="T85" id="Seg_701" n="e" s="T84">Наладил </ts>
               <ts e="T87" id="Seg_703" n="e" s="T85">уж? </ts>
            </ts>
            <ts e="T92" id="Seg_704" n="sc" s="T90">
               <ts e="T91" id="Seg_706" n="e" s="T90">(Miʔ-) </ts>
               <ts e="T92" id="Seg_708" n="e" s="T91">Miʔnibeʔ… </ts>
            </ts>
            <ts e="T100" id="Seg_709" n="sc" s="T93">
               <ts e="T94" id="Seg_711" n="e" s="T93">Miʔnʼibeʔ </ts>
               <ts e="T95" id="Seg_713" n="e" s="T94">tura </ts>
               <ts e="T96" id="Seg_715" n="e" s="T95">nenʼəluʔpi. </ts>
               <ts e="T97" id="Seg_717" n="e" s="T96">Tüj </ts>
               <ts e="T98" id="Seg_719" n="e" s="T97">nuldəbiʔi </ts>
               <ts e="T99" id="Seg_721" n="e" s="T98">baška </ts>
               <ts e="T100" id="Seg_723" n="e" s="T99">magazin. </ts>
            </ts>
            <ts e="T113" id="Seg_724" n="sc" s="T101">
               <ts e="T102" id="Seg_726" n="e" s="T101">Dĭn </ts>
               <ts e="T103" id="Seg_728" n="e" s="T102">ara </ts>
               <ts e="T104" id="Seg_730" n="e" s="T103">sadarlaʔbəʔjə, </ts>
               <ts e="T105" id="Seg_732" n="e" s="T104">pivo, </ts>
               <ts e="T106" id="Seg_734" n="e" s="T105">vsjakoj </ts>
               <ts e="T107" id="Seg_736" n="e" s="T106">oldʼaʔi, </ts>
               <ts e="T108" id="Seg_738" n="e" s="T107">i </ts>
               <ts e="T109" id="Seg_740" n="e" s="T108">(nĭʔnʼe </ts>
               <ts e="T110" id="Seg_742" n="e" s="T109">kö) </ts>
               <ts e="T111" id="Seg_744" n="e" s="T110">i </ts>
               <ts e="T112" id="Seg_746" n="e" s="T111">spirt </ts>
               <ts e="T113" id="Seg_748" n="e" s="T112">mĭlieʔi. </ts>
            </ts>
            <ts e="T120" id="Seg_749" n="sc" s="T114">
               <ts e="T115" id="Seg_751" n="e" s="T114">Girgit </ts>
               <ts e="T116" id="Seg_753" n="e" s="T115">idʼiʔe </ts>
               <ts e="T117" id="Seg_755" n="e" s="T116">bĭtliel, </ts>
               <ts e="T118" id="Seg_757" n="e" s="T117">ĭzəmzittə </ts>
               <ts e="T119" id="Seg_759" n="e" s="T118">ej </ts>
               <ts e="T120" id="Seg_761" n="e" s="T119">molial. </ts>
            </ts>
            <ts e="T128" id="Seg_762" n="sc" s="T121">
               <ts e="T122" id="Seg_764" n="e" s="T121">Dĭ </ts>
               <ts e="T123" id="Seg_766" n="e" s="T122">tura </ts>
               <ts e="T124" id="Seg_768" n="e" s="T123">nenʼiluʔpi, </ts>
               <ts e="T125" id="Seg_770" n="e" s="T124">i </ts>
               <ts e="T126" id="Seg_772" n="e" s="T125">šide </ts>
               <ts e="T127" id="Seg_774" n="e" s="T126">kuza </ts>
               <ts e="T128" id="Seg_776" n="e" s="T127">nenʼiluʔpiʔi. </ts>
            </ts>
            <ts e="T138" id="Seg_777" n="sc" s="T129">
               <ts e="T130" id="Seg_779" n="e" s="T129">(Dĭ </ts>
               <ts e="T131" id="Seg_781" n="e" s="T130">dĭzeŋ=) </ts>
               <ts e="T132" id="Seg_783" n="e" s="T131">Dĭzem </ts>
               <ts e="T133" id="Seg_785" n="e" s="T132">tĭlbiʔi, </ts>
               <ts e="T134" id="Seg_787" n="e" s="T133">embiʔi </ts>
               <ts e="T135" id="Seg_789" n="e" s="T134">dʼünə, </ts>
               <ts e="T136" id="Seg_791" n="e" s="T135">i </ts>
               <ts e="T137" id="Seg_793" n="e" s="T136">külambiʔi </ts>
               <ts e="T138" id="Seg_795" n="e" s="T137">dĭzeŋ. </ts>
            </ts>
            <ts e="T140" id="Seg_796" n="sc" s="T139">
               <ts e="T140" id="Seg_798" n="e" s="T139">Kabarləj. </ts>
            </ts>
            <ts e="T189" id="Seg_799" n="sc" s="T180">
               <ts e="T181" id="Seg_801" n="e" s="T180">Dĭ </ts>
               <ts e="T182" id="Seg_803" n="e" s="T181">magazinnə </ts>
               <ts e="T183" id="Seg_805" n="e" s="T182">((…)) </ts>
               <ts e="T184" id="Seg_807" n="e" s="T183">un </ts>
               <ts e="T185" id="Seg_809" n="e" s="T184">detleʔbəʔjə, </ts>
               <ts e="T186" id="Seg_811" n="e" s="T185">iʔgö, </ts>
               <ts e="T187" id="Seg_813" n="e" s="T186">i </ts>
               <ts e="T188" id="Seg_815" n="e" s="T187">dĭn </ts>
               <ts e="T189" id="Seg_817" n="e" s="T188">nuldlaʔbəʔjə. </ts>
            </ts>
            <ts e="T194" id="Seg_818" n="sc" s="T190">
               <ts e="T191" id="Seg_820" n="e" s="T190">I </ts>
               <ts e="T192" id="Seg_822" n="e" s="T191">(il-) </ts>
               <ts e="T193" id="Seg_824" n="e" s="T192">ildə </ts>
               <ts e="T194" id="Seg_826" n="e" s="T193">sadarlaʔbəʔjə. </ts>
            </ts>
            <ts e="T204" id="Seg_827" n="sc" s="T195">
               <ts e="T196" id="Seg_829" n="e" s="T195">Onʼiʔ </ts>
               <ts e="T197" id="Seg_831" n="e" s="T196">măn </ts>
               <ts e="T198" id="Seg_833" n="e" s="T197">mămbiam </ts>
               <ts e="T199" id="Seg_835" n="e" s="T198">dĭʔnə: </ts>
               <ts e="T200" id="Seg_837" n="e" s="T199">"Măna </ts>
               <ts e="T201" id="Seg_839" n="e" s="T200">maːʔtə </ts>
               <ts e="T202" id="Seg_841" n="e" s="T201">onʼiʔ </ts>
               <ts e="T203" id="Seg_843" n="e" s="T202">bura </ts>
               <ts e="T204" id="Seg_845" n="e" s="T203">un". </ts>
            </ts>
            <ts e="T213" id="Seg_846" n="sc" s="T205">
               <ts e="T206" id="Seg_848" n="e" s="T205">Dĭgəttə </ts>
               <ts e="T207" id="Seg_850" n="e" s="T206">šobiam: </ts>
               <ts e="T208" id="Seg_852" n="e" s="T207">"(Maː-) </ts>
               <ts e="T209" id="Seg_854" n="e" s="T208">Maːʔpi </ts>
               <ts e="T210" id="Seg_856" n="e" s="T209">un?" </ts>
               <ts e="T211" id="Seg_858" n="e" s="T210">Dĭ </ts>
               <ts e="T212" id="Seg_860" n="e" s="T211">măndə: </ts>
               <ts e="T213" id="Seg_862" n="e" s="T212">"Ej". </ts>
            </ts>
            <ts e="T223" id="Seg_863" n="sc" s="T214">
               <ts e="T215" id="Seg_865" n="e" s="T214">Dĭgəttə </ts>
               <ts e="T216" id="Seg_867" n="e" s="T215">(măm-) </ts>
               <ts e="T217" id="Seg_869" n="e" s="T216">măn </ts>
               <ts e="T218" id="Seg_871" n="e" s="T217">mămbiam: </ts>
               <ts e="T219" id="Seg_873" n="e" s="T218">"Tănan </ts>
               <ts e="T220" id="Seg_875" n="e" s="T219">(navernă) </ts>
               <ts e="T221" id="Seg_877" n="e" s="T220">aktʼa </ts>
               <ts e="T222" id="Seg_879" n="e" s="T221">mĭzittə </ts>
               <ts e="T223" id="Seg_881" n="e" s="T222">nada. </ts>
            </ts>
            <ts e="T227" id="Seg_882" n="sc" s="T224">
               <ts e="T225" id="Seg_884" n="e" s="T224">Dĭgəttə </ts>
               <ts e="T226" id="Seg_886" n="e" s="T225">tăn </ts>
               <ts e="T227" id="Seg_888" n="e" s="T226">maːtləl". </ts>
            </ts>
            <ts e="T232" id="Seg_889" n="sc" s="T228">
               <ts e="T229" id="Seg_891" n="e" s="T228">Dĭgəttə </ts>
               <ts e="T230" id="Seg_893" n="e" s="T229">măn </ts>
               <ts e="T231" id="Seg_895" n="e" s="T230">unukam </ts>
               <ts e="T232" id="Seg_897" n="e" s="T231">amnolaʔbə. </ts>
            </ts>
            <ts e="T240" id="Seg_898" n="sc" s="T233">
               <ts e="T234" id="Seg_900" n="e" s="T233">"Ige </ts>
               <ts e="T235" id="Seg_902" n="e" s="T234">aktʼa, </ts>
               <ts e="T236" id="Seg_904" n="e" s="T235">măn </ts>
               <ts e="T237" id="Seg_906" n="e" s="T236">sadarlam </ts>
               <ts e="T238" id="Seg_908" n="e" s="T237">bostə </ts>
               <ts e="T239" id="Seg_910" n="e" s="T238">(m-) </ts>
               <ts e="T240" id="Seg_912" n="e" s="T239">un". </ts>
            </ts>
            <ts e="T245" id="Seg_913" n="sc" s="T241">
               <ts e="T242" id="Seg_915" n="e" s="T241">Dĭgəttə </ts>
               <ts e="T243" id="Seg_917" n="e" s="T242">măn </ts>
               <ts e="T244" id="Seg_919" n="e" s="T243">dĭʔnə </ts>
               <ts e="T245" id="Seg_921" n="e" s="T244">mĭliem. </ts>
            </ts>
            <ts e="T248" id="Seg_922" n="sc" s="T246">
               <ts e="T247" id="Seg_924" n="e" s="T246">"Kumen </ts>
               <ts e="T248" id="Seg_926" n="e" s="T247">aktʼa?" </ts>
            </ts>
            <ts e="T256" id="Seg_927" n="sc" s="T249">
               <ts e="T250" id="Seg_929" n="e" s="T249">Dĭ </ts>
               <ts e="T251" id="Seg_931" n="e" s="T250">mămbi, </ts>
               <ts e="T252" id="Seg_933" n="e" s="T251">kumen, </ts>
               <ts e="T253" id="Seg_935" n="e" s="T252">măn </ts>
               <ts e="T254" id="Seg_937" n="e" s="T253">mĭbiem, </ts>
               <ts e="T255" id="Seg_939" n="e" s="T254">dĭgəttə </ts>
               <ts e="T256" id="Seg_941" n="e" s="T255">dĭ… </ts>
            </ts>
            <ts e="T261" id="Seg_942" n="sc" s="T257">
               <ts e="T258" id="Seg_944" n="e" s="T257">Un </ts>
               <ts e="T259" id="Seg_946" n="e" s="T258">ibiem, </ts>
               <ts e="T260" id="Seg_948" n="e" s="T259">maːʔnʼi </ts>
               <ts e="T261" id="Seg_950" n="e" s="T260">kunlambiam. </ts>
            </ts>
            <ts e="T271" id="Seg_951" n="sc" s="T262">
               <ts e="T263" id="Seg_953" n="e" s="T262">I </ts>
               <ts e="T264" id="Seg_955" n="e" s="T263">kămnəbiam </ts>
               <ts e="T265" id="Seg_957" n="e" s="T264">jašiktə, </ts>
               <ts e="T266" id="Seg_959" n="e" s="T265">dĭgəttə </ts>
               <ts e="T267" id="Seg_961" n="e" s="T266">(de- </ts>
               <ts e="T268" id="Seg_963" n="e" s="T267">detle-) </ts>
               <ts e="T269" id="Seg_965" n="e" s="T268">deʔpiem </ts>
               <ts e="T270" id="Seg_967" n="e" s="T269">dĭʔnə, </ts>
               <ts e="T271" id="Seg_969" n="e" s="T270">mĭluʔpiam. </ts>
            </ts>
            <ts e="T290" id="Seg_970" n="sc" s="T288">
               <ts e="T289" id="Seg_972" n="e" s="T288">Ну, </ts>
               <ts e="T290" id="Seg_974" n="e" s="T289">глядите. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PKZ">
            <ta e="T4" id="Seg_975" s="T1">PKZ_19700819_09344-1az.PKZ.001 (001)</ta>
            <ta e="T8" id="Seg_976" s="T5">PKZ_19700819_09344-1az.PKZ.002 (002)</ta>
            <ta e="T14" id="Seg_977" s="T9">PKZ_19700819_09344-1az.PKZ.003 (003)</ta>
            <ta e="T53" id="Seg_978" s="T38">PKZ_19700819_09344-1az.PKZ.004 (010)</ta>
            <ta e="T61" id="Seg_979" s="T54">PKZ_19700819_09344-1az.PKZ.005 (012)</ta>
            <ta e="T75" id="Seg_980" s="T61">PKZ_19700819_09344-1az.PKZ.006 (014)</ta>
            <ta e="T87" id="Seg_981" s="T84">PKZ_19700819_09344-1az.PKZ.007 (018)</ta>
            <ta e="T92" id="Seg_982" s="T90">PKZ_19700819_09344-1az.PKZ.008 (020)</ta>
            <ta e="T96" id="Seg_983" s="T93">PKZ_19700819_09344-1az.PKZ.009 (021)</ta>
            <ta e="T100" id="Seg_984" s="T96">PKZ_19700819_09344-1az.PKZ.010 (022)</ta>
            <ta e="T113" id="Seg_985" s="T101">PKZ_19700819_09344-1az.PKZ.011 (023)</ta>
            <ta e="T120" id="Seg_986" s="T114">PKZ_19700819_09344-1az.PKZ.012 (024)</ta>
            <ta e="T128" id="Seg_987" s="T121">PKZ_19700819_09344-1az.PKZ.013 (025)</ta>
            <ta e="T138" id="Seg_988" s="T129">PKZ_19700819_09344-1az.PKZ.014 (026)</ta>
            <ta e="T140" id="Seg_989" s="T139">PKZ_19700819_09344-1az.PKZ.015 (027)</ta>
            <ta e="T189" id="Seg_990" s="T180">PKZ_19700819_09344-1az.PKZ.016 (030)</ta>
            <ta e="T194" id="Seg_991" s="T190">PKZ_19700819_09344-1az.PKZ.017 (031)</ta>
            <ta e="T204" id="Seg_992" s="T195">PKZ_19700819_09344-1az.PKZ.018 (032)</ta>
            <ta e="T210" id="Seg_993" s="T205">PKZ_19700819_09344-1az.PKZ.019 (033)</ta>
            <ta e="T213" id="Seg_994" s="T210">PKZ_19700819_09344-1az.PKZ.020 (034)</ta>
            <ta e="T223" id="Seg_995" s="T214">PKZ_19700819_09344-1az.PKZ.021 (035)</ta>
            <ta e="T227" id="Seg_996" s="T224">PKZ_19700819_09344-1az.PKZ.022 (036)</ta>
            <ta e="T232" id="Seg_997" s="T228">PKZ_19700819_09344-1az.PKZ.023 (037)</ta>
            <ta e="T240" id="Seg_998" s="T233">PKZ_19700819_09344-1az.PKZ.024 (038)</ta>
            <ta e="T245" id="Seg_999" s="T241">PKZ_19700819_09344-1az.PKZ.025 (039)</ta>
            <ta e="T248" id="Seg_1000" s="T246">PKZ_19700819_09344-1az.PKZ.026 (040)</ta>
            <ta e="T256" id="Seg_1001" s="T249">PKZ_19700819_09344-1az.PKZ.027 (041)</ta>
            <ta e="T261" id="Seg_1002" s="T257">PKZ_19700819_09344-1az.PKZ.028 (042)</ta>
            <ta e="T271" id="Seg_1003" s="T262">PKZ_19700819_09344-1az.PKZ.029 (043)</ta>
            <ta e="T290" id="Seg_1004" s="T288">PKZ_19700819_09344-1az.PKZ.030 (047)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PKZ">
            <ta e="T4" id="Seg_1005" s="T1">он на кровати. </ta>
            <ta e="T8" id="Seg_1006" s="T5">Черный как головешка. </ta>
            <ta e="T14" id="Seg_1007" s="T9">И схоронили обоих. </ta>
            <ta e="T53" id="Seg_1008" s="T38">Нет, не магазин не сгорел, а изба сгорела. </ta>
            <ta e="T61" id="Seg_1009" s="T54">Магазин сейчас тот стоит. </ta>
            <ta e="T75" id="Seg_1010" s="T61">Там тоже кладут соль, да чай ((…)). </ta>
            <ta e="T87" id="Seg_1011" s="T84">Наладил уж? </ta>
            <ta e="T92" id="Seg_1012" s="T90">(Miʔ-) Miʔnibeʔ… </ta>
            <ta e="T96" id="Seg_1013" s="T93">Miʔnʼibeʔ tura nenʼəluʔpi. </ta>
            <ta e="T100" id="Seg_1014" s="T96">Tüj nuldəbiʔi baška magazin. </ta>
            <ta e="T113" id="Seg_1015" s="T101">Dĭn ara sadarlaʔbəʔjə, pivo, vsjakoj oldʼaʔi, i (nĭʔnʼe kö) i spirt mĭlieʔi. </ta>
            <ta e="T120" id="Seg_1016" s="T114">Girgit idʼiʔe bĭtliel, ĭzəmzittə ej molial. </ta>
            <ta e="T128" id="Seg_1017" s="T121">Dĭ tura nenʼiluʔpi, i šide kuza nenʼiluʔpiʔi. </ta>
            <ta e="T138" id="Seg_1018" s="T129">(Dĭ dĭzeŋ=) Dĭzem tĭlbiʔi, embiʔi dʼünə, i külambiʔi dĭzeŋ. </ta>
            <ta e="T140" id="Seg_1019" s="T139">Kabarləj. </ta>
            <ta e="T189" id="Seg_1020" s="T180">Dĭ magazinnə ((…)) un detleʔbəʔjə, iʔgö, i dĭn nuldlaʔbəʔjə. </ta>
            <ta e="T194" id="Seg_1021" s="T190">I (il-) ildə sadarlaʔbəʔjə. </ta>
            <ta e="T204" id="Seg_1022" s="T195">Onʼiʔ măn mămbiam dĭʔnə: "Măna maːʔtə onʼiʔ bura un". </ta>
            <ta e="T210" id="Seg_1023" s="T205">Dĭgəttə šobiam: "(Maː-) Maːʔpi un?" </ta>
            <ta e="T213" id="Seg_1024" s="T210">Dĭ măndə: "Ej". </ta>
            <ta e="T223" id="Seg_1025" s="T214">Dĭgəttə (măm-) măn mămbiam: "Tănan (navernă) aktʼa mĭzittə nada. </ta>
            <ta e="T227" id="Seg_1026" s="T224">Dĭgəttə tăn maːtləl". </ta>
            <ta e="T232" id="Seg_1027" s="T228">Dĭgəttə măn unukam amnolaʔbə. </ta>
            <ta e="T240" id="Seg_1028" s="T233">"Ige aktʼa, măn sadarlam bostə (m-) un". </ta>
            <ta e="T245" id="Seg_1029" s="T241">Dĭgəttə măn dĭʔnə mĭliem. </ta>
            <ta e="T248" id="Seg_1030" s="T246">"Kumen aktʼa?" </ta>
            <ta e="T256" id="Seg_1031" s="T249">Dĭ mămbi, kumen, măn mĭbiem, dĭgəttə dĭ… </ta>
            <ta e="T261" id="Seg_1032" s="T257">Un ibiem, maːʔnʼi kunlambiam. </ta>
            <ta e="T271" id="Seg_1033" s="T262">I kămnəbiam jašiktə, dĭgəttə (de- detle-) deʔpiem dĭʔnə, mĭluʔpiam. </ta>
            <ta e="T290" id="Seg_1034" s="T288">Ну, глядите. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-PKZ">
            <ta e="T94" id="Seg_1035" s="T93">miʔnʼibeʔ</ta>
            <ta e="T95" id="Seg_1036" s="T94">tura</ta>
            <ta e="T96" id="Seg_1037" s="T95">nenʼ-ə-luʔpi</ta>
            <ta e="T97" id="Seg_1038" s="T96">tüj</ta>
            <ta e="T98" id="Seg_1039" s="T97">nuldə-bi-ʔi</ta>
            <ta e="T99" id="Seg_1040" s="T98">baška</ta>
            <ta e="T100" id="Seg_1041" s="T99">magazin</ta>
            <ta e="T102" id="Seg_1042" s="T101">dĭn</ta>
            <ta e="T103" id="Seg_1043" s="T102">ara</ta>
            <ta e="T104" id="Seg_1044" s="T103">sadar-laʔbə-ʔjə</ta>
            <ta e="T105" id="Seg_1045" s="T104">pivo</ta>
            <ta e="T106" id="Seg_1046" s="T105">vsjakoj</ta>
            <ta e="T107" id="Seg_1047" s="T106">oldʼa-ʔi</ta>
            <ta e="T108" id="Seg_1048" s="T107">i</ta>
            <ta e="T110" id="Seg_1049" s="T109">kö</ta>
            <ta e="T111" id="Seg_1050" s="T110">i</ta>
            <ta e="T112" id="Seg_1051" s="T111">spirt</ta>
            <ta e="T113" id="Seg_1052" s="T112">mĭ-lie-ʔi</ta>
            <ta e="T115" id="Seg_1053" s="T114">girgit</ta>
            <ta e="T116" id="Seg_1054" s="T115">idʼiʔe</ta>
            <ta e="T117" id="Seg_1055" s="T116">bĭt-lie-l</ta>
            <ta e="T118" id="Seg_1056" s="T117">ĭzəm-zittə</ta>
            <ta e="T119" id="Seg_1057" s="T118">ej</ta>
            <ta e="T120" id="Seg_1058" s="T119">mo-lia-l</ta>
            <ta e="T122" id="Seg_1059" s="T121">dĭ</ta>
            <ta e="T123" id="Seg_1060" s="T122">tura</ta>
            <ta e="T124" id="Seg_1061" s="T123">nenʼ-i-luʔpi</ta>
            <ta e="T125" id="Seg_1062" s="T124">i</ta>
            <ta e="T126" id="Seg_1063" s="T125">šide</ta>
            <ta e="T127" id="Seg_1064" s="T126">kuza</ta>
            <ta e="T128" id="Seg_1065" s="T127">nenʼ-i-luʔpi-ʔi</ta>
            <ta e="T130" id="Seg_1066" s="T129">dĭ</ta>
            <ta e="T131" id="Seg_1067" s="T130">dĭ-zeŋ</ta>
            <ta e="T132" id="Seg_1068" s="T131">dĭ-zem</ta>
            <ta e="T133" id="Seg_1069" s="T132">tĭl-bi-ʔi</ta>
            <ta e="T134" id="Seg_1070" s="T133">em-bi-ʔi</ta>
            <ta e="T135" id="Seg_1071" s="T134">dʼü-nə</ta>
            <ta e="T136" id="Seg_1072" s="T135">i</ta>
            <ta e="T137" id="Seg_1073" s="T136">kü-lam-bi-ʔi</ta>
            <ta e="T138" id="Seg_1074" s="T137">dĭ-zeŋ</ta>
            <ta e="T140" id="Seg_1075" s="T139">kabarləj</ta>
            <ta e="T181" id="Seg_1076" s="T180">dĭ</ta>
            <ta e="T182" id="Seg_1077" s="T181">magazin-nə</ta>
            <ta e="T184" id="Seg_1078" s="T183">un</ta>
            <ta e="T185" id="Seg_1079" s="T184">det-leʔbə-ʔjə</ta>
            <ta e="T186" id="Seg_1080" s="T185">iʔgö</ta>
            <ta e="T187" id="Seg_1081" s="T186">i</ta>
            <ta e="T188" id="Seg_1082" s="T187">dĭn</ta>
            <ta e="T189" id="Seg_1083" s="T188">nuld-laʔbə-ʔjə</ta>
            <ta e="T191" id="Seg_1084" s="T190">i</ta>
            <ta e="T193" id="Seg_1085" s="T192">il-də</ta>
            <ta e="T194" id="Seg_1086" s="T193">sadar-laʔbə-ʔjə</ta>
            <ta e="T196" id="Seg_1087" s="T195">onʼiʔ</ta>
            <ta e="T197" id="Seg_1088" s="T196">măn</ta>
            <ta e="T198" id="Seg_1089" s="T197">măm-bia-m</ta>
            <ta e="T199" id="Seg_1090" s="T198">dĭʔ-nə</ta>
            <ta e="T200" id="Seg_1091" s="T199">măna</ta>
            <ta e="T201" id="Seg_1092" s="T200">maː-ʔtə</ta>
            <ta e="T202" id="Seg_1093" s="T201">onʼiʔ</ta>
            <ta e="T203" id="Seg_1094" s="T202">bura</ta>
            <ta e="T204" id="Seg_1095" s="T203">un</ta>
            <ta e="T206" id="Seg_1096" s="T205">dĭgəttə</ta>
            <ta e="T207" id="Seg_1097" s="T206">šo-bia-m</ta>
            <ta e="T208" id="Seg_1098" s="T207">maː-</ta>
            <ta e="T209" id="Seg_1099" s="T208">maː-ʔpi</ta>
            <ta e="T210" id="Seg_1100" s="T209">un</ta>
            <ta e="T211" id="Seg_1101" s="T210">dĭ</ta>
            <ta e="T212" id="Seg_1102" s="T211">măn-də</ta>
            <ta e="T213" id="Seg_1103" s="T212">ej</ta>
            <ta e="T215" id="Seg_1104" s="T214">dĭgəttə</ta>
            <ta e="T217" id="Seg_1105" s="T216">măn</ta>
            <ta e="T218" id="Seg_1106" s="T217">măm-bia-m</ta>
            <ta e="T219" id="Seg_1107" s="T218">tănan</ta>
            <ta e="T220" id="Seg_1108" s="T219">navernă</ta>
            <ta e="T221" id="Seg_1109" s="T220">aktʼa</ta>
            <ta e="T222" id="Seg_1110" s="T221">mĭ-zittə</ta>
            <ta e="T223" id="Seg_1111" s="T222">nada</ta>
            <ta e="T225" id="Seg_1112" s="T224">dĭgəttə</ta>
            <ta e="T226" id="Seg_1113" s="T225">tăn</ta>
            <ta e="T227" id="Seg_1114" s="T226">maː-t-lə-l</ta>
            <ta e="T229" id="Seg_1115" s="T228">dĭgəttə</ta>
            <ta e="T230" id="Seg_1116" s="T229">măn</ta>
            <ta e="T231" id="Seg_1117" s="T230">unuk-a-m</ta>
            <ta e="T232" id="Seg_1118" s="T231">amno-laʔbə</ta>
            <ta e="T234" id="Seg_1119" s="T233">i-ge</ta>
            <ta e="T235" id="Seg_1120" s="T234">aktʼa</ta>
            <ta e="T236" id="Seg_1121" s="T235">măn</ta>
            <ta e="T237" id="Seg_1122" s="T236">sadar-la-m</ta>
            <ta e="T238" id="Seg_1123" s="T237">bos-tə</ta>
            <ta e="T240" id="Seg_1124" s="T239">un</ta>
            <ta e="T242" id="Seg_1125" s="T241">dĭgəttə</ta>
            <ta e="T243" id="Seg_1126" s="T242">măn</ta>
            <ta e="T244" id="Seg_1127" s="T243">dĭʔ-nə</ta>
            <ta e="T245" id="Seg_1128" s="T244">mĭ-lie-m</ta>
            <ta e="T247" id="Seg_1129" s="T246">kumen</ta>
            <ta e="T248" id="Seg_1130" s="T247">aktʼa</ta>
            <ta e="T250" id="Seg_1131" s="T249">dĭ</ta>
            <ta e="T251" id="Seg_1132" s="T250">măm-bi</ta>
            <ta e="T252" id="Seg_1133" s="T251">kumen</ta>
            <ta e="T253" id="Seg_1134" s="T252">măn</ta>
            <ta e="T254" id="Seg_1135" s="T253">mĭ-bie-m</ta>
            <ta e="T255" id="Seg_1136" s="T254">dĭgəttə</ta>
            <ta e="T256" id="Seg_1137" s="T255">dĭ</ta>
            <ta e="T258" id="Seg_1138" s="T257">un</ta>
            <ta e="T259" id="Seg_1139" s="T258">i-bie-m</ta>
            <ta e="T260" id="Seg_1140" s="T259">maːʔ-nʼi</ta>
            <ta e="T261" id="Seg_1141" s="T260">kun-lam-bia-m</ta>
            <ta e="T263" id="Seg_1142" s="T262">i</ta>
            <ta e="T264" id="Seg_1143" s="T263">kămnə-bia-m</ta>
            <ta e="T265" id="Seg_1144" s="T264">jašik-tə</ta>
            <ta e="T266" id="Seg_1145" s="T265">dĭgəttə</ta>
            <ta e="T269" id="Seg_1146" s="T268">deʔ-pie-m</ta>
            <ta e="T270" id="Seg_1147" s="T269">dĭʔ-nə</ta>
            <ta e="T271" id="Seg_1148" s="T270">mĭ-luʔ-pia-m</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PKZ">
            <ta e="T94" id="Seg_1149" s="T93">miʔnʼibeʔ</ta>
            <ta e="T95" id="Seg_1150" s="T94">tura</ta>
            <ta e="T96" id="Seg_1151" s="T95">nen-ə-luʔbdə</ta>
            <ta e="T97" id="Seg_1152" s="T96">tüj</ta>
            <ta e="T98" id="Seg_1153" s="T97">nuldə-bi-jəʔ</ta>
            <ta e="T99" id="Seg_1154" s="T98">baška</ta>
            <ta e="T100" id="Seg_1155" s="T99">măgăzin</ta>
            <ta e="T102" id="Seg_1156" s="T101">dĭn</ta>
            <ta e="T103" id="Seg_1157" s="T102">ara</ta>
            <ta e="T104" id="Seg_1158" s="T103">sădar-laʔbə-jəʔ</ta>
            <ta e="T105" id="Seg_1159" s="T104">pivo</ta>
            <ta e="T106" id="Seg_1160" s="T105">vsjakoj</ta>
            <ta e="T107" id="Seg_1161" s="T106">oldʼa-jəʔ</ta>
            <ta e="T108" id="Seg_1162" s="T107">i</ta>
            <ta e="T110" id="Seg_1163" s="T109">kö</ta>
            <ta e="T111" id="Seg_1164" s="T110">i</ta>
            <ta e="T112" id="Seg_1165" s="T111">spirt</ta>
            <ta e="T113" id="Seg_1166" s="T112">mĭ-liA-jəʔ</ta>
            <ta e="T115" id="Seg_1167" s="T114">girgit</ta>
            <ta e="T116" id="Seg_1168" s="T115">idʼiʔeʔe</ta>
            <ta e="T117" id="Seg_1169" s="T116">bĭs-liA-l</ta>
            <ta e="T118" id="Seg_1170" s="T117">ĭzem-zittə</ta>
            <ta e="T119" id="Seg_1171" s="T118">ej</ta>
            <ta e="T120" id="Seg_1172" s="T119">mo-liA-l</ta>
            <ta e="T122" id="Seg_1173" s="T121">dĭ</ta>
            <ta e="T123" id="Seg_1174" s="T122">tura</ta>
            <ta e="T124" id="Seg_1175" s="T123">nen-ə-luʔbdə</ta>
            <ta e="T125" id="Seg_1176" s="T124">i</ta>
            <ta e="T126" id="Seg_1177" s="T125">šide</ta>
            <ta e="T127" id="Seg_1178" s="T126">kuza</ta>
            <ta e="T128" id="Seg_1179" s="T127">nen-ə-luʔbdə-jəʔ</ta>
            <ta e="T130" id="Seg_1180" s="T129">dĭ</ta>
            <ta e="T131" id="Seg_1181" s="T130">dĭ-zAŋ</ta>
            <ta e="T132" id="Seg_1182" s="T131">dĭ-zem</ta>
            <ta e="T133" id="Seg_1183" s="T132">tĭl-bi-jəʔ</ta>
            <ta e="T134" id="Seg_1184" s="T133">hen-bi-jəʔ</ta>
            <ta e="T135" id="Seg_1185" s="T134">tʼo-Tə</ta>
            <ta e="T136" id="Seg_1186" s="T135">i</ta>
            <ta e="T137" id="Seg_1187" s="T136">kü-laːm-bi-jəʔ</ta>
            <ta e="T138" id="Seg_1188" s="T137">dĭ-zAŋ</ta>
            <ta e="T140" id="Seg_1189" s="T139">kabarləj</ta>
            <ta e="T181" id="Seg_1190" s="T180">dĭ</ta>
            <ta e="T182" id="Seg_1191" s="T181">măgăzin-Tə</ta>
            <ta e="T184" id="Seg_1192" s="T183">un</ta>
            <ta e="T185" id="Seg_1193" s="T184">det-laʔbə-jəʔ</ta>
            <ta e="T186" id="Seg_1194" s="T185">iʔgö</ta>
            <ta e="T187" id="Seg_1195" s="T186">i</ta>
            <ta e="T188" id="Seg_1196" s="T187">dĭn</ta>
            <ta e="T189" id="Seg_1197" s="T188">nuldə-laʔbə-jəʔ</ta>
            <ta e="T191" id="Seg_1198" s="T190">i</ta>
            <ta e="T193" id="Seg_1199" s="T192">il-Tə</ta>
            <ta e="T194" id="Seg_1200" s="T193">sădar-laʔbə-jəʔ</ta>
            <ta e="T196" id="Seg_1201" s="T195">onʼiʔ</ta>
            <ta e="T197" id="Seg_1202" s="T196">măn</ta>
            <ta e="T198" id="Seg_1203" s="T197">măn-bi-m</ta>
            <ta e="T199" id="Seg_1204" s="T198">dĭ-Tə</ta>
            <ta e="T200" id="Seg_1205" s="T199">măna</ta>
            <ta e="T201" id="Seg_1206" s="T200">ma-ʔ</ta>
            <ta e="T202" id="Seg_1207" s="T201">onʼiʔ</ta>
            <ta e="T203" id="Seg_1208" s="T202">băra</ta>
            <ta e="T204" id="Seg_1209" s="T203">un</ta>
            <ta e="T206" id="Seg_1210" s="T205">dĭgəttə</ta>
            <ta e="T207" id="Seg_1211" s="T206">šo-bi-m</ta>
            <ta e="T209" id="Seg_1212" s="T208">ma-bi</ta>
            <ta e="T210" id="Seg_1213" s="T209">un</ta>
            <ta e="T211" id="Seg_1214" s="T210">dĭ</ta>
            <ta e="T212" id="Seg_1215" s="T211">măn-ntə</ta>
            <ta e="T213" id="Seg_1216" s="T212">ej</ta>
            <ta e="T215" id="Seg_1217" s="T214">dĭgəttə</ta>
            <ta e="T217" id="Seg_1218" s="T216">măn</ta>
            <ta e="T218" id="Seg_1219" s="T217">măn-bi-m</ta>
            <ta e="T219" id="Seg_1220" s="T218">tănan</ta>
            <ta e="T220" id="Seg_1221" s="T219">naverna</ta>
            <ta e="T221" id="Seg_1222" s="T220">aktʼa</ta>
            <ta e="T222" id="Seg_1223" s="T221">mĭ-zittə</ta>
            <ta e="T223" id="Seg_1224" s="T222">nadə</ta>
            <ta e="T225" id="Seg_1225" s="T224">dĭgəttə</ta>
            <ta e="T226" id="Seg_1226" s="T225">tăn</ta>
            <ta e="T227" id="Seg_1227" s="T226">ma-də-lV-l</ta>
            <ta e="T229" id="Seg_1228" s="T228">dĭgəttə</ta>
            <ta e="T230" id="Seg_1229" s="T229">măn</ta>
            <ta e="T231" id="Seg_1230" s="T230">unuk-ə-m</ta>
            <ta e="T232" id="Seg_1231" s="T231">amno-laʔbə</ta>
            <ta e="T234" id="Seg_1232" s="T233">i-gA</ta>
            <ta e="T235" id="Seg_1233" s="T234">aktʼa</ta>
            <ta e="T236" id="Seg_1234" s="T235">măn</ta>
            <ta e="T237" id="Seg_1235" s="T236">sădar-lV-m</ta>
            <ta e="T238" id="Seg_1236" s="T237">bos-də</ta>
            <ta e="T240" id="Seg_1237" s="T239">un</ta>
            <ta e="T242" id="Seg_1238" s="T241">dĭgəttə</ta>
            <ta e="T243" id="Seg_1239" s="T242">măn</ta>
            <ta e="T244" id="Seg_1240" s="T243">dĭ-Tə</ta>
            <ta e="T245" id="Seg_1241" s="T244">mĭ-liA-m</ta>
            <ta e="T247" id="Seg_1242" s="T246">kumən</ta>
            <ta e="T248" id="Seg_1243" s="T247">aktʼa</ta>
            <ta e="T250" id="Seg_1244" s="T249">dĭ</ta>
            <ta e="T251" id="Seg_1245" s="T250">măn-bi</ta>
            <ta e="T252" id="Seg_1246" s="T251">kumən</ta>
            <ta e="T253" id="Seg_1247" s="T252">măn</ta>
            <ta e="T254" id="Seg_1248" s="T253">mĭ-bi-m</ta>
            <ta e="T255" id="Seg_1249" s="T254">dĭgəttə</ta>
            <ta e="T256" id="Seg_1250" s="T255">dĭ</ta>
            <ta e="T258" id="Seg_1251" s="T257">un</ta>
            <ta e="T259" id="Seg_1252" s="T258">i-bi-m</ta>
            <ta e="T260" id="Seg_1253" s="T259">maʔ-gənʼi</ta>
            <ta e="T261" id="Seg_1254" s="T260">kun-laːm-bi-m</ta>
            <ta e="T263" id="Seg_1255" s="T262">i</ta>
            <ta e="T264" id="Seg_1256" s="T263">kămnə-bi-m</ta>
            <ta e="T265" id="Seg_1257" s="T264">jašɨk-Tə</ta>
            <ta e="T266" id="Seg_1258" s="T265">dĭgəttə</ta>
            <ta e="T269" id="Seg_1259" s="T268">det-bi-m</ta>
            <ta e="T270" id="Seg_1260" s="T269">dĭ-Tə</ta>
            <ta e="T271" id="Seg_1261" s="T270">mĭ-luʔbdə-bi-m</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PKZ">
            <ta e="T94" id="Seg_1262" s="T93">we.LAT</ta>
            <ta e="T95" id="Seg_1263" s="T94">house.[NOM.SG]</ta>
            <ta e="T96" id="Seg_1264" s="T95">burn-EP-MOM.[3SG]</ta>
            <ta e="T97" id="Seg_1265" s="T96">now</ta>
            <ta e="T98" id="Seg_1266" s="T97">set.up-PST-3PL</ta>
            <ta e="T99" id="Seg_1267" s="T98">another.[NOM.SG]</ta>
            <ta e="T100" id="Seg_1268" s="T99">shop.[NOM.SG]</ta>
            <ta e="T102" id="Seg_1269" s="T101">there</ta>
            <ta e="T103" id="Seg_1270" s="T102">vodka.[NOM.SG]</ta>
            <ta e="T104" id="Seg_1271" s="T103">sell-DUR-3PL</ta>
            <ta e="T105" id="Seg_1272" s="T104">beer.[NOM.SG]</ta>
            <ta e="T106" id="Seg_1273" s="T105">all.kind</ta>
            <ta e="T107" id="Seg_1274" s="T106">clothing-PL</ta>
            <ta e="T108" id="Seg_1275" s="T107">and</ta>
            <ta e="T110" id="Seg_1276" s="T109">winter.[NOM.SG]</ta>
            <ta e="T111" id="Seg_1277" s="T110">and</ta>
            <ta e="T112" id="Seg_1278" s="T111">alcohol.[NOM.SG]</ta>
            <ta e="T113" id="Seg_1279" s="T112">give-PRS-3PL</ta>
            <ta e="T115" id="Seg_1280" s="T114">what.kind</ta>
            <ta e="T116" id="Seg_1281" s="T115">a.few</ta>
            <ta e="T117" id="Seg_1282" s="T116">drink-PRS-2SG</ta>
            <ta e="T118" id="Seg_1283" s="T117">hurt-INF.LAT</ta>
            <ta e="T119" id="Seg_1284" s="T118">NEG</ta>
            <ta e="T120" id="Seg_1285" s="T119">become-PRS-2SG</ta>
            <ta e="T122" id="Seg_1286" s="T121">this.[NOM.SG]</ta>
            <ta e="T123" id="Seg_1287" s="T122">house.[NOM.SG]</ta>
            <ta e="T124" id="Seg_1288" s="T123">burn-EP-MOM.[3SG]</ta>
            <ta e="T125" id="Seg_1289" s="T124">and</ta>
            <ta e="T126" id="Seg_1290" s="T125">two.[NOM.SG]</ta>
            <ta e="T127" id="Seg_1291" s="T126">man.[NOM.SG]</ta>
            <ta e="T128" id="Seg_1292" s="T127">burn-EP-MOM-3PL</ta>
            <ta e="T130" id="Seg_1293" s="T129">this</ta>
            <ta e="T131" id="Seg_1294" s="T130">this-PL</ta>
            <ta e="T132" id="Seg_1295" s="T131">this-ACC.PL</ta>
            <ta e="T133" id="Seg_1296" s="T132">dig-PST-3PL</ta>
            <ta e="T134" id="Seg_1297" s="T133">put-PST-3PL</ta>
            <ta e="T135" id="Seg_1298" s="T134">place-LAT</ta>
            <ta e="T136" id="Seg_1299" s="T135">and</ta>
            <ta e="T137" id="Seg_1300" s="T136">die-RES-PST-3PL</ta>
            <ta e="T138" id="Seg_1301" s="T137">this-PL</ta>
            <ta e="T140" id="Seg_1302" s="T139">enough</ta>
            <ta e="T181" id="Seg_1303" s="T180">this.[NOM.SG]</ta>
            <ta e="T182" id="Seg_1304" s="T181">shop-LAT</ta>
            <ta e="T184" id="Seg_1305" s="T183">flour.[NOM.SG]</ta>
            <ta e="T185" id="Seg_1306" s="T184">bring-DUR-3PL</ta>
            <ta e="T186" id="Seg_1307" s="T185">many</ta>
            <ta e="T187" id="Seg_1308" s="T186">and</ta>
            <ta e="T188" id="Seg_1309" s="T187">there</ta>
            <ta e="T189" id="Seg_1310" s="T188">place-DUR-3PL</ta>
            <ta e="T191" id="Seg_1311" s="T190">and</ta>
            <ta e="T193" id="Seg_1312" s="T192">people-LAT</ta>
            <ta e="T194" id="Seg_1313" s="T193">sell-DUR-3PL</ta>
            <ta e="T196" id="Seg_1314" s="T195">one.[NOM.SG]</ta>
            <ta e="T197" id="Seg_1315" s="T196">I.NOM</ta>
            <ta e="T198" id="Seg_1316" s="T197">say-PST-1SG</ta>
            <ta e="T199" id="Seg_1317" s="T198">this-LAT</ta>
            <ta e="T200" id="Seg_1318" s="T199">I.LAT</ta>
            <ta e="T201" id="Seg_1319" s="T200">remain-IMP.2SG</ta>
            <ta e="T202" id="Seg_1320" s="T201">one.[NOM.SG]</ta>
            <ta e="T203" id="Seg_1321" s="T202">sack.[NOM.SG]</ta>
            <ta e="T204" id="Seg_1322" s="T203">flour.[NOM.SG]</ta>
            <ta e="T206" id="Seg_1323" s="T205">then</ta>
            <ta e="T207" id="Seg_1324" s="T206">come-PST-1SG</ta>
            <ta e="T209" id="Seg_1325" s="T208">remain-PST.[3SG]</ta>
            <ta e="T210" id="Seg_1326" s="T209">flour.[NOM.SG]</ta>
            <ta e="T211" id="Seg_1327" s="T210">this.[NOM.SG]</ta>
            <ta e="T212" id="Seg_1328" s="T211">say-IPFVZ.[3SG]</ta>
            <ta e="T213" id="Seg_1329" s="T212">NEG</ta>
            <ta e="T215" id="Seg_1330" s="T214">then</ta>
            <ta e="T217" id="Seg_1331" s="T216">I.NOM</ta>
            <ta e="T218" id="Seg_1332" s="T217">say-PST-1SG</ta>
            <ta e="T219" id="Seg_1333" s="T218">you.DAT</ta>
            <ta e="T220" id="Seg_1334" s="T219">probably</ta>
            <ta e="T221" id="Seg_1335" s="T220">money.[NOM.SG]</ta>
            <ta e="T222" id="Seg_1336" s="T221">give-INF.LAT</ta>
            <ta e="T223" id="Seg_1337" s="T222">one.should</ta>
            <ta e="T225" id="Seg_1338" s="T224">then</ta>
            <ta e="T226" id="Seg_1339" s="T225">you.NOM</ta>
            <ta e="T227" id="Seg_1340" s="T226">remain-TR-FUT-2SG</ta>
            <ta e="T229" id="Seg_1341" s="T228">then</ta>
            <ta e="T230" id="Seg_1342" s="T229">I.NOM</ta>
            <ta e="T231" id="Seg_1343" s="T230">grandchild-EP-NOM/GEN/ACC.1SG</ta>
            <ta e="T232" id="Seg_1344" s="T231">sit-DUR.[3SG]</ta>
            <ta e="T234" id="Seg_1345" s="T233">be-PRS.[3SG]</ta>
            <ta e="T235" id="Seg_1346" s="T234">money.[NOM.SG]</ta>
            <ta e="T236" id="Seg_1347" s="T235">I.NOM</ta>
            <ta e="T237" id="Seg_1348" s="T236">sell-FUT-1SG</ta>
            <ta e="T238" id="Seg_1349" s="T237">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T240" id="Seg_1350" s="T239">flour.[NOM.SG]</ta>
            <ta e="T242" id="Seg_1351" s="T241">then</ta>
            <ta e="T243" id="Seg_1352" s="T242">I.NOM</ta>
            <ta e="T244" id="Seg_1353" s="T243">this-LAT</ta>
            <ta e="T245" id="Seg_1354" s="T244">give-PRS-1SG</ta>
            <ta e="T247" id="Seg_1355" s="T246">how.much</ta>
            <ta e="T248" id="Seg_1356" s="T247">money.[NOM.SG]</ta>
            <ta e="T250" id="Seg_1357" s="T249">this.[NOM.SG]</ta>
            <ta e="T251" id="Seg_1358" s="T250">say-PST.[3SG]</ta>
            <ta e="T252" id="Seg_1359" s="T251">how.much</ta>
            <ta e="T253" id="Seg_1360" s="T252">I.NOM</ta>
            <ta e="T254" id="Seg_1361" s="T253">give-PST-1SG</ta>
            <ta e="T255" id="Seg_1362" s="T254">then</ta>
            <ta e="T256" id="Seg_1363" s="T255">this.[NOM.SG]</ta>
            <ta e="T258" id="Seg_1364" s="T257">flour.[NOM.SG]</ta>
            <ta e="T259" id="Seg_1365" s="T258">take-PST-1SG</ta>
            <ta e="T260" id="Seg_1366" s="T259">house-LAT/LOC.1SG</ta>
            <ta e="T261" id="Seg_1367" s="T260">bring-RES-PST-1SG</ta>
            <ta e="T263" id="Seg_1368" s="T262">and</ta>
            <ta e="T264" id="Seg_1369" s="T263">pour-PST-1SG</ta>
            <ta e="T265" id="Seg_1370" s="T264">drawer-LAT</ta>
            <ta e="T266" id="Seg_1371" s="T265">then</ta>
            <ta e="T269" id="Seg_1372" s="T268">bring-PST-1SG</ta>
            <ta e="T270" id="Seg_1373" s="T269">this-LAT</ta>
            <ta e="T271" id="Seg_1374" s="T270">give-MOM-PST-1SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PKZ">
            <ta e="T94" id="Seg_1375" s="T93">мы.LAT</ta>
            <ta e="T95" id="Seg_1376" s="T94">дом.[NOM.SG]</ta>
            <ta e="T96" id="Seg_1377" s="T95">гореть-EP-MOM.[3SG]</ta>
            <ta e="T97" id="Seg_1378" s="T96">сейчас</ta>
            <ta e="T98" id="Seg_1379" s="T97">установить-PST-3PL</ta>
            <ta e="T99" id="Seg_1380" s="T98">другой.[NOM.SG]</ta>
            <ta e="T100" id="Seg_1381" s="T99">магазин.[NOM.SG]</ta>
            <ta e="T102" id="Seg_1382" s="T101">там</ta>
            <ta e="T103" id="Seg_1383" s="T102">водка.[NOM.SG]</ta>
            <ta e="T104" id="Seg_1384" s="T103">продавать-DUR-3PL</ta>
            <ta e="T105" id="Seg_1385" s="T104">пиво.[NOM.SG]</ta>
            <ta e="T106" id="Seg_1386" s="T105">всякий</ta>
            <ta e="T107" id="Seg_1387" s="T106">одежда-PL</ta>
            <ta e="T108" id="Seg_1388" s="T107">и</ta>
            <ta e="T110" id="Seg_1389" s="T109">зима.[NOM.SG]</ta>
            <ta e="T111" id="Seg_1390" s="T110">и</ta>
            <ta e="T112" id="Seg_1391" s="T111">спирт.[NOM.SG]</ta>
            <ta e="T113" id="Seg_1392" s="T112">дать-PRS-3PL</ta>
            <ta e="T115" id="Seg_1393" s="T114">какой</ta>
            <ta e="T116" id="Seg_1394" s="T115">немного</ta>
            <ta e="T117" id="Seg_1395" s="T116">пить-PRS-2SG</ta>
            <ta e="T118" id="Seg_1396" s="T117">болеть-INF.LAT</ta>
            <ta e="T119" id="Seg_1397" s="T118">NEG</ta>
            <ta e="T120" id="Seg_1398" s="T119">стать-PRS-2SG</ta>
            <ta e="T122" id="Seg_1399" s="T121">этот.[NOM.SG]</ta>
            <ta e="T123" id="Seg_1400" s="T122">дом.[NOM.SG]</ta>
            <ta e="T124" id="Seg_1401" s="T123">гореть-EP-MOM.[3SG]</ta>
            <ta e="T125" id="Seg_1402" s="T124">и</ta>
            <ta e="T126" id="Seg_1403" s="T125">два.[NOM.SG]</ta>
            <ta e="T127" id="Seg_1404" s="T126">мужчина.[NOM.SG]</ta>
            <ta e="T128" id="Seg_1405" s="T127">гореть-EP-MOM-3PL</ta>
            <ta e="T130" id="Seg_1406" s="T129">этот</ta>
            <ta e="T131" id="Seg_1407" s="T130">этот-PL</ta>
            <ta e="T132" id="Seg_1408" s="T131">этот-ACC.PL</ta>
            <ta e="T133" id="Seg_1409" s="T132">копать-PST-3PL</ta>
            <ta e="T134" id="Seg_1410" s="T133">класть-PST-3PL</ta>
            <ta e="T135" id="Seg_1411" s="T134">место-LAT</ta>
            <ta e="T136" id="Seg_1412" s="T135">и</ta>
            <ta e="T137" id="Seg_1413" s="T136">умереть-RES-PST-3PL</ta>
            <ta e="T138" id="Seg_1414" s="T137">этот-PL</ta>
            <ta e="T140" id="Seg_1415" s="T139">хватит</ta>
            <ta e="T181" id="Seg_1416" s="T180">этот.[NOM.SG]</ta>
            <ta e="T182" id="Seg_1417" s="T181">магазин-LAT</ta>
            <ta e="T184" id="Seg_1418" s="T183">мука.[NOM.SG]</ta>
            <ta e="T185" id="Seg_1419" s="T184">принести-DUR-3PL</ta>
            <ta e="T186" id="Seg_1420" s="T185">много</ta>
            <ta e="T187" id="Seg_1421" s="T186">и</ta>
            <ta e="T188" id="Seg_1422" s="T187">там</ta>
            <ta e="T189" id="Seg_1423" s="T188">поставить-DUR-3PL</ta>
            <ta e="T191" id="Seg_1424" s="T190">и</ta>
            <ta e="T193" id="Seg_1425" s="T192">люди-LAT</ta>
            <ta e="T194" id="Seg_1426" s="T193">продавать-DUR-3PL</ta>
            <ta e="T196" id="Seg_1427" s="T195">один.[NOM.SG]</ta>
            <ta e="T197" id="Seg_1428" s="T196">я.NOM</ta>
            <ta e="T198" id="Seg_1429" s="T197">сказать-PST-1SG</ta>
            <ta e="T199" id="Seg_1430" s="T198">этот-LAT</ta>
            <ta e="T200" id="Seg_1431" s="T199">я.LAT</ta>
            <ta e="T201" id="Seg_1432" s="T200">остаться-IMP.2SG</ta>
            <ta e="T202" id="Seg_1433" s="T201">один.[NOM.SG]</ta>
            <ta e="T203" id="Seg_1434" s="T202">мешок.[NOM.SG]</ta>
            <ta e="T204" id="Seg_1435" s="T203">мука.[NOM.SG]</ta>
            <ta e="T206" id="Seg_1436" s="T205">тогда</ta>
            <ta e="T207" id="Seg_1437" s="T206">прийти-PST-1SG</ta>
            <ta e="T209" id="Seg_1438" s="T208">остаться-PST.[3SG]</ta>
            <ta e="T210" id="Seg_1439" s="T209">мука.[NOM.SG]</ta>
            <ta e="T211" id="Seg_1440" s="T210">этот.[NOM.SG]</ta>
            <ta e="T212" id="Seg_1441" s="T211">сказать-IPFVZ.[3SG]</ta>
            <ta e="T213" id="Seg_1442" s="T212">NEG</ta>
            <ta e="T215" id="Seg_1443" s="T214">тогда</ta>
            <ta e="T217" id="Seg_1444" s="T216">я.NOM</ta>
            <ta e="T218" id="Seg_1445" s="T217">сказать-PST-1SG</ta>
            <ta e="T219" id="Seg_1446" s="T218">ты.DAT</ta>
            <ta e="T220" id="Seg_1447" s="T219">наверное</ta>
            <ta e="T221" id="Seg_1448" s="T220">деньги.[NOM.SG]</ta>
            <ta e="T222" id="Seg_1449" s="T221">дать-INF.LAT</ta>
            <ta e="T223" id="Seg_1450" s="T222">надо</ta>
            <ta e="T225" id="Seg_1451" s="T224">тогда</ta>
            <ta e="T226" id="Seg_1452" s="T225">ты.NOM</ta>
            <ta e="T227" id="Seg_1453" s="T226">остаться-TR-FUT-2SG</ta>
            <ta e="T229" id="Seg_1454" s="T228">тогда</ta>
            <ta e="T230" id="Seg_1455" s="T229">я.NOM</ta>
            <ta e="T231" id="Seg_1456" s="T230">внук-EP-NOM/GEN/ACC.1SG</ta>
            <ta e="T232" id="Seg_1457" s="T231">сидеть-DUR.[3SG]</ta>
            <ta e="T234" id="Seg_1458" s="T233">быть-PRS.[3SG]</ta>
            <ta e="T235" id="Seg_1459" s="T234">деньги.[NOM.SG]</ta>
            <ta e="T236" id="Seg_1460" s="T235">я.NOM</ta>
            <ta e="T237" id="Seg_1461" s="T236">продавать-FUT-1SG</ta>
            <ta e="T238" id="Seg_1462" s="T237">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T240" id="Seg_1463" s="T239">мука.[NOM.SG]</ta>
            <ta e="T242" id="Seg_1464" s="T241">тогда</ta>
            <ta e="T243" id="Seg_1465" s="T242">я.NOM</ta>
            <ta e="T244" id="Seg_1466" s="T243">этот-LAT</ta>
            <ta e="T245" id="Seg_1467" s="T244">дать-PRS-1SG</ta>
            <ta e="T247" id="Seg_1468" s="T246">сколько</ta>
            <ta e="T248" id="Seg_1469" s="T247">деньги.[NOM.SG]</ta>
            <ta e="T250" id="Seg_1470" s="T249">этот.[NOM.SG]</ta>
            <ta e="T251" id="Seg_1471" s="T250">сказать-PST.[3SG]</ta>
            <ta e="T252" id="Seg_1472" s="T251">сколько</ta>
            <ta e="T253" id="Seg_1473" s="T252">я.NOM</ta>
            <ta e="T254" id="Seg_1474" s="T253">дать-PST-1SG</ta>
            <ta e="T255" id="Seg_1475" s="T254">тогда</ta>
            <ta e="T256" id="Seg_1476" s="T255">этот.[NOM.SG]</ta>
            <ta e="T258" id="Seg_1477" s="T257">мука.[NOM.SG]</ta>
            <ta e="T259" id="Seg_1478" s="T258">взять-PST-1SG</ta>
            <ta e="T260" id="Seg_1479" s="T259">дом-LAT/LOC.1SG</ta>
            <ta e="T261" id="Seg_1480" s="T260">нести-RES-PST-1SG</ta>
            <ta e="T263" id="Seg_1481" s="T262">и</ta>
            <ta e="T264" id="Seg_1482" s="T263">лить-PST-1SG</ta>
            <ta e="T265" id="Seg_1483" s="T264">ящик-LAT</ta>
            <ta e="T266" id="Seg_1484" s="T265">тогда</ta>
            <ta e="T269" id="Seg_1485" s="T268">принести-PST-1SG</ta>
            <ta e="T270" id="Seg_1486" s="T269">этот-LAT</ta>
            <ta e="T271" id="Seg_1487" s="T270">дать-MOM-PST-1SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PKZ">
            <ta e="T94" id="Seg_1488" s="T93">pers</ta>
            <ta e="T95" id="Seg_1489" s="T94">n.[n:case]</ta>
            <ta e="T96" id="Seg_1490" s="T95">v-v:ins-v&gt;v.[v:pn]</ta>
            <ta e="T97" id="Seg_1491" s="T96">adv</ta>
            <ta e="T98" id="Seg_1492" s="T97">v-v:tense-v:pn</ta>
            <ta e="T99" id="Seg_1493" s="T98">adj.[n:case]</ta>
            <ta e="T100" id="Seg_1494" s="T99">n.[n:case]</ta>
            <ta e="T102" id="Seg_1495" s="T101">adv</ta>
            <ta e="T103" id="Seg_1496" s="T102">n.[n:case]</ta>
            <ta e="T104" id="Seg_1497" s="T103">v-v&gt;v-v:pn</ta>
            <ta e="T105" id="Seg_1498" s="T104">n.[n:case]</ta>
            <ta e="T106" id="Seg_1499" s="T105">quant</ta>
            <ta e="T107" id="Seg_1500" s="T106">n-n:num</ta>
            <ta e="T108" id="Seg_1501" s="T107">conj</ta>
            <ta e="T110" id="Seg_1502" s="T109">n.[n:case]</ta>
            <ta e="T111" id="Seg_1503" s="T110">conj</ta>
            <ta e="T112" id="Seg_1504" s="T111">n.[n:case]</ta>
            <ta e="T113" id="Seg_1505" s="T112">v-v:tense-v:pn</ta>
            <ta e="T115" id="Seg_1506" s="T114">que</ta>
            <ta e="T116" id="Seg_1507" s="T115">adv</ta>
            <ta e="T117" id="Seg_1508" s="T116">v-v:tense-v:pn</ta>
            <ta e="T118" id="Seg_1509" s="T117">v-v:n.fin</ta>
            <ta e="T119" id="Seg_1510" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_1511" s="T119">v-v:tense-v:pn</ta>
            <ta e="T122" id="Seg_1512" s="T121">dempro.[n:case]</ta>
            <ta e="T123" id="Seg_1513" s="T122">n.[n:case]</ta>
            <ta e="T124" id="Seg_1514" s="T123">v-v:ins-v&gt;v.[v:pn]</ta>
            <ta e="T125" id="Seg_1515" s="T124">conj</ta>
            <ta e="T126" id="Seg_1516" s="T125">num.[n:case]</ta>
            <ta e="T127" id="Seg_1517" s="T126">n.[n:case]</ta>
            <ta e="T128" id="Seg_1518" s="T127">v-v:ins-v&gt;v-v:pn</ta>
            <ta e="T130" id="Seg_1519" s="T129">dempro</ta>
            <ta e="T131" id="Seg_1520" s="T130">dempro-n:num</ta>
            <ta e="T132" id="Seg_1521" s="T131">dempro-n:case</ta>
            <ta e="T133" id="Seg_1522" s="T132">v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_1523" s="T133">v-v:tense-v:pn</ta>
            <ta e="T135" id="Seg_1524" s="T134">n-n:case</ta>
            <ta e="T136" id="Seg_1525" s="T135">conj</ta>
            <ta e="T137" id="Seg_1526" s="T136">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T138" id="Seg_1527" s="T137">dempro-n:num</ta>
            <ta e="T140" id="Seg_1528" s="T139">ptcl</ta>
            <ta e="T181" id="Seg_1529" s="T180">dempro.[n:case]</ta>
            <ta e="T182" id="Seg_1530" s="T181">n-n:case</ta>
            <ta e="T184" id="Seg_1531" s="T183">n.[n:case]</ta>
            <ta e="T185" id="Seg_1532" s="T184">v-v&gt;v-v:pn</ta>
            <ta e="T186" id="Seg_1533" s="T185">quant</ta>
            <ta e="T187" id="Seg_1534" s="T186">conj</ta>
            <ta e="T188" id="Seg_1535" s="T187">adv</ta>
            <ta e="T189" id="Seg_1536" s="T188">v-v&gt;v-v:pn</ta>
            <ta e="T191" id="Seg_1537" s="T190">conj</ta>
            <ta e="T193" id="Seg_1538" s="T192">n-n:case</ta>
            <ta e="T194" id="Seg_1539" s="T193">v-v&gt;v-v:pn</ta>
            <ta e="T196" id="Seg_1540" s="T195">num.[n:case]</ta>
            <ta e="T197" id="Seg_1541" s="T196">pers</ta>
            <ta e="T198" id="Seg_1542" s="T197">v-v:tense-v:pn</ta>
            <ta e="T199" id="Seg_1543" s="T198">dempro-n:case</ta>
            <ta e="T200" id="Seg_1544" s="T199">pers</ta>
            <ta e="T201" id="Seg_1545" s="T200">v-v:mood.pn</ta>
            <ta e="T202" id="Seg_1546" s="T201">num.[n:case]</ta>
            <ta e="T203" id="Seg_1547" s="T202">n.[n:case]</ta>
            <ta e="T204" id="Seg_1548" s="T203">n.[n:case]</ta>
            <ta e="T206" id="Seg_1549" s="T205">adv</ta>
            <ta e="T207" id="Seg_1550" s="T206">v-v:tense-v:pn</ta>
            <ta e="T209" id="Seg_1551" s="T208">v-v:tense.[v:pn]</ta>
            <ta e="T210" id="Seg_1552" s="T209">n.[n:case]</ta>
            <ta e="T211" id="Seg_1553" s="T210">dempro.[n:case]</ta>
            <ta e="T212" id="Seg_1554" s="T211">v-v&gt;v.[v:pn]</ta>
            <ta e="T213" id="Seg_1555" s="T212">ptcl</ta>
            <ta e="T215" id="Seg_1556" s="T214">adv</ta>
            <ta e="T217" id="Seg_1557" s="T216">pers</ta>
            <ta e="T218" id="Seg_1558" s="T217">v-v:tense-v:pn</ta>
            <ta e="T219" id="Seg_1559" s="T218">pers</ta>
            <ta e="T220" id="Seg_1560" s="T219">adv</ta>
            <ta e="T221" id="Seg_1561" s="T220">n.[n:case]</ta>
            <ta e="T222" id="Seg_1562" s="T221">v-v:n.fin</ta>
            <ta e="T223" id="Seg_1563" s="T222">ptcl</ta>
            <ta e="T225" id="Seg_1564" s="T224">adv</ta>
            <ta e="T226" id="Seg_1565" s="T225">pers</ta>
            <ta e="T227" id="Seg_1566" s="T226">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T229" id="Seg_1567" s="T228">adv</ta>
            <ta e="T230" id="Seg_1568" s="T229">pers</ta>
            <ta e="T231" id="Seg_1569" s="T230">n-n:ins-n:case.poss</ta>
            <ta e="T232" id="Seg_1570" s="T231">v-v&gt;v.[v:pn]</ta>
            <ta e="T234" id="Seg_1571" s="T233">v-v:tense.[v:pn]</ta>
            <ta e="T235" id="Seg_1572" s="T234">n.[n:case]</ta>
            <ta e="T236" id="Seg_1573" s="T235">pers</ta>
            <ta e="T237" id="Seg_1574" s="T236">v-v:tense-v:pn</ta>
            <ta e="T238" id="Seg_1575" s="T237">refl-n:case.poss</ta>
            <ta e="T240" id="Seg_1576" s="T239">n.[n:case]</ta>
            <ta e="T242" id="Seg_1577" s="T241">adv</ta>
            <ta e="T243" id="Seg_1578" s="T242">pers</ta>
            <ta e="T244" id="Seg_1579" s="T243">dempro-n:case</ta>
            <ta e="T245" id="Seg_1580" s="T244">v-v:tense-v:pn</ta>
            <ta e="T247" id="Seg_1581" s="T246">adv</ta>
            <ta e="T248" id="Seg_1582" s="T247">n.[n:case]</ta>
            <ta e="T250" id="Seg_1583" s="T249">dempro.[n:case]</ta>
            <ta e="T251" id="Seg_1584" s="T250">v-v:tense.[v:pn]</ta>
            <ta e="T252" id="Seg_1585" s="T251">adv</ta>
            <ta e="T253" id="Seg_1586" s="T252">pers</ta>
            <ta e="T254" id="Seg_1587" s="T253">v-v:tense-v:pn</ta>
            <ta e="T255" id="Seg_1588" s="T254">adv</ta>
            <ta e="T256" id="Seg_1589" s="T255">dempro.[n:case]</ta>
            <ta e="T258" id="Seg_1590" s="T257">n.[n:case]</ta>
            <ta e="T259" id="Seg_1591" s="T258">v-v:tense-v:pn</ta>
            <ta e="T260" id="Seg_1592" s="T259">n-n:case.poss</ta>
            <ta e="T261" id="Seg_1593" s="T260">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T263" id="Seg_1594" s="T262">conj</ta>
            <ta e="T264" id="Seg_1595" s="T263">v-v:tense-v:pn</ta>
            <ta e="T265" id="Seg_1596" s="T264">n-n:case</ta>
            <ta e="T266" id="Seg_1597" s="T265">adv</ta>
            <ta e="T269" id="Seg_1598" s="T268">v-v:tense-v:pn</ta>
            <ta e="T270" id="Seg_1599" s="T269">dempro-n:case</ta>
            <ta e="T271" id="Seg_1600" s="T270">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PKZ">
            <ta e="T94" id="Seg_1601" s="T93">pers</ta>
            <ta e="T95" id="Seg_1602" s="T94">n</ta>
            <ta e="T96" id="Seg_1603" s="T95">v</ta>
            <ta e="T97" id="Seg_1604" s="T96">adv</ta>
            <ta e="T98" id="Seg_1605" s="T97">v</ta>
            <ta e="T99" id="Seg_1606" s="T98">adj</ta>
            <ta e="T100" id="Seg_1607" s="T99">n</ta>
            <ta e="T102" id="Seg_1608" s="T101">adv</ta>
            <ta e="T103" id="Seg_1609" s="T102">n</ta>
            <ta e="T104" id="Seg_1610" s="T103">v</ta>
            <ta e="T105" id="Seg_1611" s="T104">n</ta>
            <ta e="T106" id="Seg_1612" s="T105">quant</ta>
            <ta e="T107" id="Seg_1613" s="T106">n</ta>
            <ta e="T108" id="Seg_1614" s="T107">conj</ta>
            <ta e="T110" id="Seg_1615" s="T109">n</ta>
            <ta e="T111" id="Seg_1616" s="T110">conj</ta>
            <ta e="T112" id="Seg_1617" s="T111">n</ta>
            <ta e="T113" id="Seg_1618" s="T112">v</ta>
            <ta e="T115" id="Seg_1619" s="T114">que</ta>
            <ta e="T116" id="Seg_1620" s="T115">adj</ta>
            <ta e="T117" id="Seg_1621" s="T116">v</ta>
            <ta e="T118" id="Seg_1622" s="T117">v</ta>
            <ta e="T119" id="Seg_1623" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_1624" s="T119">v</ta>
            <ta e="T122" id="Seg_1625" s="T121">dempro</ta>
            <ta e="T123" id="Seg_1626" s="T122">n</ta>
            <ta e="T124" id="Seg_1627" s="T123">v</ta>
            <ta e="T125" id="Seg_1628" s="T124">conj</ta>
            <ta e="T126" id="Seg_1629" s="T125">num</ta>
            <ta e="T127" id="Seg_1630" s="T126">n</ta>
            <ta e="T128" id="Seg_1631" s="T127">v</ta>
            <ta e="T130" id="Seg_1632" s="T129">dempro</ta>
            <ta e="T131" id="Seg_1633" s="T130">dempro</ta>
            <ta e="T132" id="Seg_1634" s="T131">dempro</ta>
            <ta e="T133" id="Seg_1635" s="T132">v</ta>
            <ta e="T134" id="Seg_1636" s="T133">v</ta>
            <ta e="T135" id="Seg_1637" s="T134">n</ta>
            <ta e="T136" id="Seg_1638" s="T135">conj</ta>
            <ta e="T137" id="Seg_1639" s="T136">v</ta>
            <ta e="T138" id="Seg_1640" s="T137">dempro</ta>
            <ta e="T140" id="Seg_1641" s="T139">ptcl</ta>
            <ta e="T181" id="Seg_1642" s="T180">dempro</ta>
            <ta e="T182" id="Seg_1643" s="T181">n</ta>
            <ta e="T184" id="Seg_1644" s="T183">n</ta>
            <ta e="T185" id="Seg_1645" s="T184">v</ta>
            <ta e="T186" id="Seg_1646" s="T185">quant</ta>
            <ta e="T187" id="Seg_1647" s="T186">conj</ta>
            <ta e="T188" id="Seg_1648" s="T187">adv</ta>
            <ta e="T189" id="Seg_1649" s="T188">v</ta>
            <ta e="T191" id="Seg_1650" s="T190">conj</ta>
            <ta e="T193" id="Seg_1651" s="T192">n</ta>
            <ta e="T194" id="Seg_1652" s="T193">v</ta>
            <ta e="T196" id="Seg_1653" s="T195">num</ta>
            <ta e="T197" id="Seg_1654" s="T196">pers</ta>
            <ta e="T198" id="Seg_1655" s="T197">v</ta>
            <ta e="T199" id="Seg_1656" s="T198">dempro</ta>
            <ta e="T200" id="Seg_1657" s="T199">pers</ta>
            <ta e="T201" id="Seg_1658" s="T200">v</ta>
            <ta e="T202" id="Seg_1659" s="T201">num</ta>
            <ta e="T203" id="Seg_1660" s="T202">n</ta>
            <ta e="T204" id="Seg_1661" s="T203">n</ta>
            <ta e="T206" id="Seg_1662" s="T205">adv</ta>
            <ta e="T207" id="Seg_1663" s="T206">v</ta>
            <ta e="T209" id="Seg_1664" s="T208">v</ta>
            <ta e="T210" id="Seg_1665" s="T209">n</ta>
            <ta e="T211" id="Seg_1666" s="T210">dempro</ta>
            <ta e="T212" id="Seg_1667" s="T211">v</ta>
            <ta e="T213" id="Seg_1668" s="T212">ptcl</ta>
            <ta e="T215" id="Seg_1669" s="T214">adv</ta>
            <ta e="T217" id="Seg_1670" s="T216">pers</ta>
            <ta e="T218" id="Seg_1671" s="T217">v</ta>
            <ta e="T219" id="Seg_1672" s="T218">pers</ta>
            <ta e="T220" id="Seg_1673" s="T219">adv</ta>
            <ta e="T221" id="Seg_1674" s="T220">n</ta>
            <ta e="T222" id="Seg_1675" s="T221">v</ta>
            <ta e="T223" id="Seg_1676" s="T222">ptcl</ta>
            <ta e="T225" id="Seg_1677" s="T224">adv</ta>
            <ta e="T226" id="Seg_1678" s="T225">pers</ta>
            <ta e="T227" id="Seg_1679" s="T226">v</ta>
            <ta e="T229" id="Seg_1680" s="T228">adv</ta>
            <ta e="T230" id="Seg_1681" s="T229">pers</ta>
            <ta e="T231" id="Seg_1682" s="T230">n</ta>
            <ta e="T232" id="Seg_1683" s="T231">v</ta>
            <ta e="T234" id="Seg_1684" s="T233">v</ta>
            <ta e="T235" id="Seg_1685" s="T234">n</ta>
            <ta e="T236" id="Seg_1686" s="T235">pers</ta>
            <ta e="T237" id="Seg_1687" s="T236">v</ta>
            <ta e="T238" id="Seg_1688" s="T237">refl</ta>
            <ta e="T240" id="Seg_1689" s="T239">n</ta>
            <ta e="T242" id="Seg_1690" s="T241">adv</ta>
            <ta e="T243" id="Seg_1691" s="T242">pers</ta>
            <ta e="T244" id="Seg_1692" s="T243">dempro</ta>
            <ta e="T245" id="Seg_1693" s="T244">v</ta>
            <ta e="T247" id="Seg_1694" s="T246">adv</ta>
            <ta e="T248" id="Seg_1695" s="T247">n</ta>
            <ta e="T250" id="Seg_1696" s="T249">dempro</ta>
            <ta e="T251" id="Seg_1697" s="T250">v</ta>
            <ta e="T252" id="Seg_1698" s="T251">adv</ta>
            <ta e="T253" id="Seg_1699" s="T252">pers</ta>
            <ta e="T254" id="Seg_1700" s="T253">v</ta>
            <ta e="T255" id="Seg_1701" s="T254">adv</ta>
            <ta e="T256" id="Seg_1702" s="T255">dempro</ta>
            <ta e="T258" id="Seg_1703" s="T257">n</ta>
            <ta e="T259" id="Seg_1704" s="T258">v</ta>
            <ta e="T260" id="Seg_1705" s="T259">n</ta>
            <ta e="T261" id="Seg_1706" s="T260">v</ta>
            <ta e="T263" id="Seg_1707" s="T262">conj</ta>
            <ta e="T264" id="Seg_1708" s="T263">v</ta>
            <ta e="T265" id="Seg_1709" s="T264">n</ta>
            <ta e="T266" id="Seg_1710" s="T265">adv</ta>
            <ta e="T269" id="Seg_1711" s="T268">v</ta>
            <ta e="T270" id="Seg_1712" s="T269">dempro</ta>
            <ta e="T271" id="Seg_1713" s="T270">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PKZ">
            <ta e="T94" id="Seg_1714" s="T93">pro.h:Poss</ta>
            <ta e="T95" id="Seg_1715" s="T94">np:P</ta>
            <ta e="T97" id="Seg_1716" s="T96">adv:Time</ta>
            <ta e="T98" id="Seg_1717" s="T97">0.3.h:A</ta>
            <ta e="T100" id="Seg_1718" s="T99">np:P</ta>
            <ta e="T102" id="Seg_1719" s="T101">adv:L</ta>
            <ta e="T103" id="Seg_1720" s="T102">np:Th</ta>
            <ta e="T104" id="Seg_1721" s="T103">0.3.h:A</ta>
            <ta e="T105" id="Seg_1722" s="T104">np:Th</ta>
            <ta e="T107" id="Seg_1723" s="T106">np:Th</ta>
            <ta e="T112" id="Seg_1724" s="T111">np:Th</ta>
            <ta e="T113" id="Seg_1725" s="T112">0.3.h:A</ta>
            <ta e="T116" id="Seg_1726" s="T115">np:P</ta>
            <ta e="T117" id="Seg_1727" s="T116">0.2.h:A</ta>
            <ta e="T120" id="Seg_1728" s="T119">0.2.h:P</ta>
            <ta e="T123" id="Seg_1729" s="T122">np:P</ta>
            <ta e="T127" id="Seg_1730" s="T126">np.h:P</ta>
            <ta e="T132" id="Seg_1731" s="T131">pro.h:Th</ta>
            <ta e="T133" id="Seg_1732" s="T132">0.3.h:A</ta>
            <ta e="T134" id="Seg_1733" s="T133">0.3.h:A</ta>
            <ta e="T135" id="Seg_1734" s="T134">np:G</ta>
            <ta e="T138" id="Seg_1735" s="T137">pro.h:P</ta>
            <ta e="T181" id="Seg_1736" s="T180">pro.h:A</ta>
            <ta e="T182" id="Seg_1737" s="T181">np:G</ta>
            <ta e="T184" id="Seg_1738" s="T183">np:Th</ta>
            <ta e="T188" id="Seg_1739" s="T187">adv:L</ta>
            <ta e="T189" id="Seg_1740" s="T188">0.3.h:A</ta>
            <ta e="T193" id="Seg_1741" s="T192">np.h:R</ta>
            <ta e="T194" id="Seg_1742" s="T193">0.3.h:A</ta>
            <ta e="T197" id="Seg_1743" s="T196">pro.h:A</ta>
            <ta e="T199" id="Seg_1744" s="T198">pro.h:R</ta>
            <ta e="T200" id="Seg_1745" s="T199">pro.h:B</ta>
            <ta e="T201" id="Seg_1746" s="T200">0.2.h:Th</ta>
            <ta e="T203" id="Seg_1747" s="T202">np:Th</ta>
            <ta e="T206" id="Seg_1748" s="T205">adv:Time</ta>
            <ta e="T207" id="Seg_1749" s="T206">0.1.h:A</ta>
            <ta e="T210" id="Seg_1750" s="T209">np:Th</ta>
            <ta e="T211" id="Seg_1751" s="T210">pro.h:A</ta>
            <ta e="T215" id="Seg_1752" s="T214">adv:Time</ta>
            <ta e="T217" id="Seg_1753" s="T216">pro.h:A</ta>
            <ta e="T219" id="Seg_1754" s="T218">pro.h:R</ta>
            <ta e="T221" id="Seg_1755" s="T220">np:Th</ta>
            <ta e="T225" id="Seg_1756" s="T224">adv:Time</ta>
            <ta e="T226" id="Seg_1757" s="T225">pro.h:Th</ta>
            <ta e="T229" id="Seg_1758" s="T228">adv:Time</ta>
            <ta e="T230" id="Seg_1759" s="T229">pro.h:Poss</ta>
            <ta e="T231" id="Seg_1760" s="T230">np.h:Th</ta>
            <ta e="T235" id="Seg_1761" s="T234">np:Th</ta>
            <ta e="T236" id="Seg_1762" s="T235">pro.h:A</ta>
            <ta e="T240" id="Seg_1763" s="T239">np:Th</ta>
            <ta e="T242" id="Seg_1764" s="T241">adv:Time</ta>
            <ta e="T243" id="Seg_1765" s="T242">pro.h:A</ta>
            <ta e="T244" id="Seg_1766" s="T243">pro.h:R</ta>
            <ta e="T250" id="Seg_1767" s="T249">pro.h:A</ta>
            <ta e="T253" id="Seg_1768" s="T252">pro.h:A</ta>
            <ta e="T255" id="Seg_1769" s="T254">adv:Time</ta>
            <ta e="T258" id="Seg_1770" s="T257">np:Th</ta>
            <ta e="T259" id="Seg_1771" s="T258">0.1.h:A</ta>
            <ta e="T260" id="Seg_1772" s="T259">np:G</ta>
            <ta e="T261" id="Seg_1773" s="T260">0.1.h:A</ta>
            <ta e="T264" id="Seg_1774" s="T263">0.1.h:A</ta>
            <ta e="T265" id="Seg_1775" s="T264">np:G</ta>
            <ta e="T266" id="Seg_1776" s="T265">adv:Time</ta>
            <ta e="T269" id="Seg_1777" s="T268">0.1.h:A</ta>
            <ta e="T270" id="Seg_1778" s="T269">pro.h:R</ta>
            <ta e="T271" id="Seg_1779" s="T270">0.1.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-PKZ">
            <ta e="T95" id="Seg_1780" s="T94">np:S</ta>
            <ta e="T96" id="Seg_1781" s="T95">v:pred</ta>
            <ta e="T98" id="Seg_1782" s="T97">v:pred 0.3.h:S</ta>
            <ta e="T100" id="Seg_1783" s="T99">np:O</ta>
            <ta e="T103" id="Seg_1784" s="T102">np:O</ta>
            <ta e="T104" id="Seg_1785" s="T103">v:pred 0.3.h:S</ta>
            <ta e="T105" id="Seg_1786" s="T104">np:O</ta>
            <ta e="T107" id="Seg_1787" s="T106">np:O</ta>
            <ta e="T112" id="Seg_1788" s="T111">np:O</ta>
            <ta e="T113" id="Seg_1789" s="T112">v:pred 0.3.h:S</ta>
            <ta e="T116" id="Seg_1790" s="T115">np:O</ta>
            <ta e="T117" id="Seg_1791" s="T116">v:pred 0.2.h:S</ta>
            <ta e="T119" id="Seg_1792" s="T118">ptcl.neg</ta>
            <ta e="T120" id="Seg_1793" s="T119">cop 0.2.h:S</ta>
            <ta e="T123" id="Seg_1794" s="T122">np:S</ta>
            <ta e="T124" id="Seg_1795" s="T123">v:pred</ta>
            <ta e="T127" id="Seg_1796" s="T126">np.h:S</ta>
            <ta e="T128" id="Seg_1797" s="T127">v:pred</ta>
            <ta e="T132" id="Seg_1798" s="T131">pro.h:O</ta>
            <ta e="T133" id="Seg_1799" s="T132">v:pred 0.3.h:S</ta>
            <ta e="T134" id="Seg_1800" s="T133">v:pred 0.3.h:S</ta>
            <ta e="T137" id="Seg_1801" s="T136">v:pred</ta>
            <ta e="T138" id="Seg_1802" s="T137">pro.h:S</ta>
            <ta e="T181" id="Seg_1803" s="T180">pro.h:S</ta>
            <ta e="T184" id="Seg_1804" s="T183">np:O</ta>
            <ta e="T185" id="Seg_1805" s="T184">v:pred</ta>
            <ta e="T189" id="Seg_1806" s="T188">v:pred 0.3.h:S</ta>
            <ta e="T194" id="Seg_1807" s="T193">v:pred 0.3.h:S</ta>
            <ta e="T197" id="Seg_1808" s="T196">pro.h:S</ta>
            <ta e="T198" id="Seg_1809" s="T197">v:pred</ta>
            <ta e="T201" id="Seg_1810" s="T200">v:pred 0.2.h:S</ta>
            <ta e="T203" id="Seg_1811" s="T202">np:O</ta>
            <ta e="T207" id="Seg_1812" s="T206">v:pred 0.1.h:S</ta>
            <ta e="T209" id="Seg_1813" s="T208">v:pred</ta>
            <ta e="T210" id="Seg_1814" s="T209">np:S</ta>
            <ta e="T211" id="Seg_1815" s="T210">pro.h:S</ta>
            <ta e="T212" id="Seg_1816" s="T211">v:pred</ta>
            <ta e="T213" id="Seg_1817" s="T212">ptcl.neg</ta>
            <ta e="T217" id="Seg_1818" s="T216">pro.h:S</ta>
            <ta e="T218" id="Seg_1819" s="T217">v:pred</ta>
            <ta e="T221" id="Seg_1820" s="T220">np:O</ta>
            <ta e="T223" id="Seg_1821" s="T222">ptcl:pred</ta>
            <ta e="T226" id="Seg_1822" s="T225">pro.h:S</ta>
            <ta e="T227" id="Seg_1823" s="T226">v:pred</ta>
            <ta e="T231" id="Seg_1824" s="T230">np.h:S</ta>
            <ta e="T232" id="Seg_1825" s="T231">v:pred</ta>
            <ta e="T234" id="Seg_1826" s="T233">v:pred</ta>
            <ta e="T235" id="Seg_1827" s="T234">np:S</ta>
            <ta e="T236" id="Seg_1828" s="T235">pro.h:S</ta>
            <ta e="T237" id="Seg_1829" s="T236">v:pred</ta>
            <ta e="T240" id="Seg_1830" s="T239">np:O</ta>
            <ta e="T243" id="Seg_1831" s="T242">pro.h:S</ta>
            <ta e="T245" id="Seg_1832" s="T244">v:pred</ta>
            <ta e="T250" id="Seg_1833" s="T249">pro.h:S</ta>
            <ta e="T251" id="Seg_1834" s="T250">v:pred</ta>
            <ta e="T253" id="Seg_1835" s="T252">pro.h:S</ta>
            <ta e="T254" id="Seg_1836" s="T253">v:pred</ta>
            <ta e="T258" id="Seg_1837" s="T257">np:O</ta>
            <ta e="T259" id="Seg_1838" s="T258">v:pred 0.1.h:S</ta>
            <ta e="T261" id="Seg_1839" s="T260">v:pred 0.1.h:S</ta>
            <ta e="T264" id="Seg_1840" s="T263">v:pred 0.1.h:S</ta>
            <ta e="T269" id="Seg_1841" s="T268">v:pred 0.1.h:S</ta>
            <ta e="T271" id="Seg_1842" s="T270">v:pred 0.1.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST-PKZ" />
         <annotation name="BOR" tierref="BOR-PKZ">
            <ta e="T95" id="Seg_1843" s="T94">TAT:cult</ta>
            <ta e="T99" id="Seg_1844" s="T98">TURK:core</ta>
            <ta e="T100" id="Seg_1845" s="T99">RUS:cult</ta>
            <ta e="T103" id="Seg_1846" s="T102">TURK:cult</ta>
            <ta e="T104" id="Seg_1847" s="T103">TURK:cult</ta>
            <ta e="T105" id="Seg_1848" s="T104">RUS:cult</ta>
            <ta e="T106" id="Seg_1849" s="T105">RUS:core</ta>
            <ta e="T108" id="Seg_1850" s="T107">RUS:gram</ta>
            <ta e="T111" id="Seg_1851" s="T110">RUS:gram</ta>
            <ta e="T112" id="Seg_1852" s="T111">RUS:cult</ta>
            <ta e="T123" id="Seg_1853" s="T122">TAT:cult</ta>
            <ta e="T125" id="Seg_1854" s="T124">RUS:gram</ta>
            <ta e="T136" id="Seg_1855" s="T135">RUS:gram</ta>
            <ta e="T182" id="Seg_1856" s="T181">RUS:cult</ta>
            <ta e="T187" id="Seg_1857" s="T186">RUS:gram</ta>
            <ta e="T191" id="Seg_1858" s="T190">RUS:gram</ta>
            <ta e="T194" id="Seg_1859" s="T193">TURK:cult</ta>
            <ta e="T220" id="Seg_1860" s="T219">RUS:mod</ta>
            <ta e="T221" id="Seg_1861" s="T220">TAT:cult</ta>
            <ta e="T223" id="Seg_1862" s="T222">RUS:mod</ta>
            <ta e="T231" id="Seg_1863" s="T230">RUS:cult</ta>
            <ta e="T235" id="Seg_1864" s="T234">TAT:cult</ta>
            <ta e="T237" id="Seg_1865" s="T236">TURK:cult</ta>
            <ta e="T248" id="Seg_1866" s="T247">TAT:cult</ta>
            <ta e="T263" id="Seg_1867" s="T262">RUS:gram</ta>
            <ta e="T265" id="Seg_1868" s="T264">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-PKZ" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-PKZ" />
         <annotation name="CS" tierref="CS-PKZ">
            <ta e="T4" id="Seg_1869" s="T1">RUS:ext</ta>
            <ta e="T8" id="Seg_1870" s="T5">RUS:ext</ta>
            <ta e="T14" id="Seg_1871" s="T9">RUS:ext</ta>
            <ta e="T53" id="Seg_1872" s="T38">RUS:ext</ta>
            <ta e="T61" id="Seg_1873" s="T54">RUS:ext</ta>
            <ta e="T75" id="Seg_1874" s="T61">RUS:ext</ta>
            <ta e="T87" id="Seg_1875" s="T84">RUS:ext</ta>
            <ta e="T290" id="Seg_1876" s="T288">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-PKZ">
            <ta e="T96" id="Seg_1877" s="T93">У нас дом сгорел.</ta>
            <ta e="T100" id="Seg_1878" s="T96">Теперь построили еще один магазин.</ta>
            <ta e="T113" id="Seg_1879" s="T101">Там водку продают пиво, всякую одежду, и ([с] прошлого года?) и спирт дают.</ta>
            <ta e="T120" id="Seg_1880" s="T114">Немножко выпьешь — болеть не будешь. [?]</ta>
            <ta e="T128" id="Seg_1881" s="T121">Тот дом сгорел, и два человека сгорели.</ta>
            <ta e="T138" id="Seg_1882" s="T129">Их похоронили, положили в землю, и… умерли они.</ta>
            <ta e="T140" id="Seg_1883" s="T139">Всё.</ta>
            <ta e="T189" id="Seg_1884" s="T180">В этот магазин муку привозят, много, и она там стоит.</ta>
            <ta e="T194" id="Seg_1885" s="T190">И людям продают.</ta>
            <ta e="T204" id="Seg_1886" s="T195">Однажды я сказала ей: "Оставь мне один мешок муки".</ta>
            <ta e="T210" id="Seg_1887" s="T205">Потом я пришла: "Осталась мука?"</ta>
            <ta e="T213" id="Seg_1888" s="T210">Он(а) говорит: "Нет".</ta>
            <ta e="T223" id="Seg_1889" s="T214">Тогда я сказала: "Тебе, наверное, надо денег дать.</ta>
            <ta e="T227" id="Seg_1890" s="T224">Тогда ты оставишь".</ta>
            <ta e="T232" id="Seg_1891" s="T228">А мой внук сидит [там и говорит]:</ta>
            <ta e="T240" id="Seg_1892" s="T233">"Есть деньги, я сам куплю муку".</ta>
            <ta e="T245" id="Seg_1893" s="T241">Тогда я ему/ей даю.</ta>
            <ta e="T248" id="Seg_1894" s="T246">"Сколько денег?"</ta>
            <ta e="T256" id="Seg_1895" s="T249">Он(а) сказал(а), сколько, я дала, тогда он(а)…</ta>
            <ta e="T261" id="Seg_1896" s="T257">Я взяла муку, отнесла домой.</ta>
            <ta e="T271" id="Seg_1897" s="T262">Насыпала в ящик, потом отнесла (ей, отдала?). </ta>
         </annotation>
         <annotation name="fe" tierref="fe-PKZ">
            <ta e="T4" id="Seg_1898" s="T1">He is on the bed.</ta>
            <ta e="T8" id="Seg_1899" s="T5">Black as a firebrand.</ta>
            <ta e="T14" id="Seg_1900" s="T9">And they buried both.</ta>
            <ta e="T53" id="Seg_1901" s="T38">No, the store didn’t burn out, but the hut burned out.</ta>
            <ta e="T61" id="Seg_1902" s="T54">The store is now standing.</ta>
            <ta e="T75" id="Seg_1903" s="T61">There they also put salt and tea (…).</ta>
            <ta e="T87" id="Seg_1904" s="T84">Is it adjusted already?</ta>
            <ta e="T96" id="Seg_1905" s="T93">Our house took fire.</ta>
            <ta e="T100" id="Seg_1906" s="T96">Now they've built another shop.</ta>
            <ta e="T113" id="Seg_1907" s="T101">There vodka and beer are sold, all kinds of clothes, and ([since] last year?) also alcohol.</ta>
            <ta e="T120" id="Seg_1908" s="T114">If you drink a little, you won't be ill [anymore]. [?]</ta>
            <ta e="T128" id="Seg_1909" s="T121">That house was burnt, and two persons dead in the fire.</ta>
            <ta e="T138" id="Seg_1910" s="T129">They were buried, put into the ground, and…. they had died.</ta>
            <ta e="T140" id="Seg_1911" s="T139">That's all.</ta>
            <ta e="T189" id="Seg_1912" s="T180">To this shop, they bring flour, much of it, and it stay there.</ta>
            <ta e="T194" id="Seg_1913" s="T190">And the sell it to people.</ta>
            <ta e="T204" id="Seg_1914" s="T195">Once I said her: "Keep one bag of flour for me."</ta>
            <ta e="T210" id="Seg_1915" s="T205">Later I came: "Is there any flour left?'</ta>
            <ta e="T213" id="Seg_1916" s="T210">S/he says: "No."</ta>
            <ta e="T223" id="Seg_1917" s="T214">Then I said: "Probably I should give you money.</ta>
            <ta e="T227" id="Seg_1918" s="T224">Then you'll keep [it for me]."</ta>
            <ta e="T232" id="Seg_1919" s="T228">My grandson is sitting [there and says]:</ta>
            <ta e="T240" id="Seg_1920" s="T233">"I have money, I'll buy flour myself."</ta>
            <ta e="T245" id="Seg_1921" s="T241">Then I'm giving [it] to him/her.</ta>
            <ta e="T248" id="Seg_1922" s="T246">"How much?"</ta>
            <ta e="T256" id="Seg_1923" s="T249">S/he said how much, I gave, then s/he…</ta>
            <ta e="T261" id="Seg_1924" s="T257">I took the flour, carried it home.</ta>
            <ta e="T271" id="Seg_1925" s="T262">I poured it to a chest, then brought to (her, gave out?).</ta>
            <ta e="T290" id="Seg_1926" s="T288">Well, look.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PKZ">
            <ta e="T4" id="Seg_1927" s="T1">Er liegt auf dem Bett.</ta>
            <ta e="T8" id="Seg_1928" s="T5">Schwarz wie eine Fackel.</ta>
            <ta e="T14" id="Seg_1929" s="T9">Und sie begruben beide.</ta>
            <ta e="T53" id="Seg_1930" s="T38">Nein, der Laden ist nicht ausgebrannt, aber die Hütte ist ausgebrannt.</ta>
            <ta e="T61" id="Seg_1931" s="T54">Der Laden steht jetzt dort.</ta>
            <ta e="T75" id="Seg_1932" s="T61">Sie hatten dort auch Salz und Tee (…).</ta>
            <ta e="T87" id="Seg_1933" s="T84">Ist es schon eingestellt?</ta>
            <ta e="T96" id="Seg_1934" s="T93">Unser Haus hat angefangen zu brennen.</ta>
            <ta e="T100" id="Seg_1935" s="T96">Jetzt haben wir einen anderen Laden gebaut.</ta>
            <ta e="T113" id="Seg_1936" s="T101">Da wird Wodka und Bier verkauft, alle Arten von Kleidung und ([seit] letztem Jahr?) auch Alkohol.</ta>
            <ta e="T120" id="Seg_1937" s="T114">Wenn du ein bisschen trinkst, wirst du [nicht mehr] krank. [?]</ta>
            <ta e="T128" id="Seg_1938" s="T121">Das Haus war verbrannt und zwei Personen starben im Feuer. </ta>
            <ta e="T138" id="Seg_1939" s="T129">Sie wurden begraben, in den Boden gelegt und… Sie waren gestorben.</ta>
            <ta e="T140" id="Seg_1940" s="T139">Das ist alles.</ta>
            <ta e="T189" id="Seg_1941" s="T180">Zu diesem Laden, sie bringen Mehl, viel davon, es bleibt hier.</ta>
            <ta e="T194" id="Seg_1942" s="T190">Und sie verkaufen es an die Leute.</ta>
            <ta e="T204" id="Seg_1943" s="T195">Einmal sagte ich zu ihr: "Behalte eine Packung Mehl für mich."</ta>
            <ta e="T210" id="Seg_1944" s="T205">Später bin ich gekommen: "Ist da noch etwas Mehl?"</ta>
            <ta e="T213" id="Seg_1945" s="T210">Er/Sie sagt: "Nein."</ta>
            <ta e="T223" id="Seg_1946" s="T214">Dann sagte ich: "Wahrscheinlich sollte ich dir Geld geben.</ta>
            <ta e="T227" id="Seg_1947" s="T224">Dann hebst du [es für mich] auf."</ta>
            <ta e="T232" id="Seg_1948" s="T228">Mein Enkel sitzt [da und sagt]:</ta>
            <ta e="T240" id="Seg_1949" s="T233">"Ich habe Geld, ich werde mir selbst Geld kaufen."</ta>
            <ta e="T245" id="Seg_1950" s="T241">Dann gab ich [es] ihm/ihr. </ta>
            <ta e="T248" id="Seg_1951" s="T246">"Wie viel?"</ta>
            <ta e="T256" id="Seg_1952" s="T249">Er/Sie sagte wie viel, ich gab [es], dann er/sie…</ta>
            <ta e="T261" id="Seg_1953" s="T257">Ich nahm das Mehl, trug es nach Hause.</ta>
            <ta e="T271" id="Seg_1954" s="T262">Ich goß es in eine Truhe und brachte es dann zu (ihr, gab es aus?).</ta>
            <ta e="T290" id="Seg_1955" s="T288">Na sieh mal.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-PKZ">
            <ta e="T96" id="Seg_1956" s="T93">[GVY:] nenʼuluʔpi?</ta>
            <ta e="T113" id="Seg_1957" s="T101">[GVY:] "nĭʔnʼe kö" is transcribed very tentatively</ta>
            <ta e="T189" id="Seg_1958" s="T180">[GVY:] self-correction magazinnə … -ngən (LAT -&gt; LOC)?</ta>
            <ta e="T223" id="Seg_1959" s="T214">[GVY:] Or vernă.</ta>
            <ta e="T232" id="Seg_1960" s="T228">[GVY:] One of the saleswoman was the wife of PKZ's grandson.</ta>
            <ta e="T245" id="Seg_1961" s="T241">[GVY:] Unclear.</ta>
            <ta e="T271" id="Seg_1962" s="T262">[GVY:] PKZ brought the flour home, then brought the money she owed to the saleswoman and gave her?</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-KA"
                      id="tx-KA"
                      speaker="KA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KA">
            <ts e="T13" id="Seg_1963" n="sc" s="T10">
               <ts e="T13" id="Seg_1965" n="HIAT:u" s="T10">
                  <nts id="Seg_1966" n="HIAT:ip">(</nts>
                  <nts id="Seg_1967" n="HIAT:ip">(</nts>
                  <ats e="T13" id="Seg_1968" n="HIAT:non-pho" s="T10">…</ats>
                  <nts id="Seg_1969" n="HIAT:ip">)</nts>
                  <nts id="Seg_1970" n="HIAT:ip">)</nts>
                  <nts id="Seg_1971" n="HIAT:ip">.</nts>
                  <nts id="Seg_1972" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T16" id="Seg_1973" n="sc" s="T15">
               <ts e="T16" id="Seg_1975" n="HIAT:u" s="T15">
                  <nts id="Seg_1976" n="HIAT:ip">(</nts>
                  <nts id="Seg_1977" n="HIAT:ip">(</nts>
                  <ats e="T16" id="Seg_1978" n="HIAT:non-pho" s="T15">…</ats>
                  <nts id="Seg_1979" n="HIAT:ip">)</nts>
                  <nts id="Seg_1980" n="HIAT:ip">)</nts>
                  <nts id="Seg_1981" n="HIAT:ip">.</nts>
                  <nts id="Seg_1982" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T18" id="Seg_1983" n="sc" s="T17">
               <ts e="T18" id="Seg_1985" n="HIAT:u" s="T17">
                  <nts id="Seg_1986" n="HIAT:ip">(</nts>
                  <nts id="Seg_1987" n="HIAT:ip">(</nts>
                  <ats e="T18" id="Seg_1988" n="HIAT:non-pho" s="T17">…</ats>
                  <nts id="Seg_1989" n="HIAT:ip">)</nts>
                  <nts id="Seg_1990" n="HIAT:ip">)</nts>
                  <nts id="Seg_1991" n="HIAT:ip">.</nts>
                  <nts id="Seg_1992" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T23" id="Seg_1993" n="sc" s="T19">
               <ts e="T23" id="Seg_1995" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_1997" n="HIAT:w" s="T19">Так</ts>
                  <nts id="Seg_1998" n="HIAT:ip">,</nts>
                  <nts id="Seg_1999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_2001" n="HIAT:w" s="T20">значит</ts>
                  <nts id="Seg_2002" n="HIAT:ip">,</nts>
                  <nts id="Seg_2003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_2005" n="HIAT:w" s="T21">сделаем</ts>
                  <nts id="Seg_2006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_2008" n="HIAT:w" s="T22">так</ts>
                  <nts id="Seg_2009" n="HIAT:ip">.</nts>
                  <nts id="Seg_2010" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T47" id="Seg_2011" n="sc" s="T24">
               <ts e="T29" id="Seg_2013" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_2015" n="HIAT:w" s="T24">Сначала</ts>
                  <nts id="Seg_2016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_2018" n="HIAT:w" s="T25">по-камасински</ts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_2021" n="HIAT:w" s="T26">насчет</ts>
                  <nts id="Seg_2022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_2024" n="HIAT:w" s="T27">этого</ts>
                  <nts id="Seg_2025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_2027" n="HIAT:w" s="T28">спирта</ts>
                  <nts id="Seg_2028" n="HIAT:ip">.</nts>
                  <nts id="Seg_2029" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_2031" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_2033" n="HIAT:w" s="T29">А</ts>
                  <nts id="Seg_2034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2035" n="HIAT:ip">(</nts>
                  <ts e="T31" id="Seg_2037" n="HIAT:w" s="T30">затом-</ts>
                  <nts id="Seg_2038" n="HIAT:ip">)</nts>
                  <nts id="Seg_2039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_2041" n="HIAT:w" s="T31">затем</ts>
                  <nts id="Seg_2042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_2044" n="HIAT:w" s="T32">как</ts>
                  <nts id="Seg_2045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_2047" n="HIAT:w" s="T33">магазин</ts>
                  <nts id="Seg_2048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_2050" n="HIAT:w" s="T34">сгорел</ts>
                  <nts id="Seg_2051" n="HIAT:ip">,</nts>
                  <nts id="Seg_2052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_2054" n="HIAT:w" s="T35">как</ts>
                  <nts id="Seg_2055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_2057" n="HIAT:w" s="T36">новый</ts>
                  <nts id="Seg_2058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_2060" n="HIAT:w" s="T37">построил</ts>
                  <nts id="Seg_2061" n="HIAT:ip">,</nts>
                  <nts id="Seg_2062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_2064" n="HIAT:w" s="T39">какие</ts>
                  <nts id="Seg_2065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_2067" n="HIAT:w" s="T41">там</ts>
                  <nts id="Seg_2068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_2070" n="HIAT:w" s="T43">новые</ts>
                  <nts id="Seg_2071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_2073" n="HIAT:w" s="T45">товары</ts>
                  <nts id="Seg_2074" n="HIAT:ip">.</nts>
                  <nts id="Seg_2075" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T56" id="Seg_2076" n="sc" s="T50">
               <ts e="T56" id="Seg_2078" n="HIAT:u" s="T50">
                  <nts id="Seg_2079" n="HIAT:ip">(</nts>
                  <ts e="T52" id="Seg_2081" n="HIAT:w" s="T50">Из-</ts>
                  <nts id="Seg_2082" n="HIAT:ip">)</nts>
                  <nts id="Seg_2083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_2085" n="HIAT:w" s="T52">Изба</ts>
                  <nts id="Seg_2086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_2088" n="HIAT:w" s="T55">сгорела</ts>
                  <nts id="Seg_2089" n="HIAT:ip">.</nts>
                  <nts id="Seg_2090" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T64" id="Seg_2091" n="sc" s="T60">
               <ts e="T62" id="Seg_2093" n="HIAT:u" s="T60">
                  <ts e="T62" id="Seg_2095" n="HIAT:w" s="T60">Ага</ts>
                  <nts id="Seg_2096" n="HIAT:ip">.</nts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_2099" n="HIAT:u" s="T62">
                  <ts e="T64" id="Seg_2101" n="HIAT:w" s="T62">Ага</ts>
                  <nts id="Seg_2102" n="HIAT:ip">.</nts>
                  <nts id="Seg_2103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T83" id="Seg_2104" n="sc" s="T69">
               <ts e="T76" id="Seg_2106" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_2108" n="HIAT:w" s="T69">Ага</ts>
                  <nts id="Seg_2109" n="HIAT:ip">,</nts>
                  <nts id="Seg_2110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_2112" n="HIAT:w" s="T70">это</ts>
                  <nts id="Seg_2113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_2115" n="HIAT:w" s="T72">склад</ts>
                  <nts id="Seg_2116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_2118" n="HIAT:w" s="T73">пока</ts>
                  <nts id="Seg_2119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_2121" n="HIAT:w" s="T74">что</ts>
                  <nts id="Seg_2122" n="HIAT:ip">.</nts>
                  <nts id="Seg_2123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_2125" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_2127" n="HIAT:w" s="T76">Ну</ts>
                  <nts id="Seg_2128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_2130" n="HIAT:w" s="T77">хорошо</ts>
                  <nts id="Seg_2131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_2133" n="HIAT:w" s="T78">вот</ts>
                  <nts id="Seg_2134" n="HIAT:ip">,</nts>
                  <nts id="Seg_2135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_2137" n="HIAT:w" s="T79">начинаем</ts>
                  <nts id="Seg_2138" n="HIAT:ip">,</nts>
                  <nts id="Seg_2139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_2141" n="HIAT:w" s="T80">значит</ts>
                  <nts id="Seg_2142" n="HIAT:ip">,</nts>
                  <nts id="Seg_2143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2144" n="HIAT:ip">(</nts>
                  <ts e="T82" id="Seg_2146" n="HIAT:w" s="T81">с</ts>
                  <nts id="Seg_2147" n="HIAT:ip">)</nts>
                  <nts id="Seg_2148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_2150" n="HIAT:w" s="T82">спирту</ts>
                  <nts id="Seg_2151" n="HIAT:ip">.</nts>
                  <nts id="Seg_2152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T89" id="Seg_2153" n="sc" s="T86">
               <ts e="T89" id="Seg_2155" n="HIAT:u" s="T86">
                  <ts e="T88" id="Seg_2157" n="HIAT:w" s="T86">Да</ts>
                  <nts id="Seg_2158" n="HIAT:ip">,</nts>
                  <nts id="Seg_2159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_2161" n="HIAT:w" s="T88">можно</ts>
                  <nts id="Seg_2162" n="HIAT:ip">.</nts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T156" id="Seg_2164" n="sc" s="T141">
               <ts e="T156" id="Seg_2166" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_2168" n="HIAT:w" s="T141">Ты</ts>
                  <nts id="Seg_2169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_2171" n="HIAT:w" s="T142">еще</ts>
                  <nts id="Seg_2172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_2174" n="HIAT:w" s="T143">рассказывала</ts>
                  <nts id="Seg_2175" n="HIAT:ip">,</nts>
                  <nts id="Seg_2176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_2178" n="HIAT:w" s="T144">что</ts>
                  <nts id="Seg_2179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_2181" n="HIAT:w" s="T145">там</ts>
                  <nts id="Seg_2182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_2184" n="HIAT:w" s="T146">в</ts>
                  <nts id="Seg_2185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_2187" n="HIAT:w" s="T147">новом</ts>
                  <nts id="Seg_2188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_2190" n="HIAT:w" s="T148">магазине</ts>
                  <nts id="Seg_2191" n="HIAT:ip">,</nts>
                  <nts id="Seg_2192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_2194" n="HIAT:w" s="T149">как</ts>
                  <nts id="Seg_2195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_2197" n="HIAT:w" s="T150">это</ts>
                  <nts id="Seg_2198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_2200" n="HIAT:w" s="T151">всё</ts>
                  <nts id="Seg_2201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_2203" n="HIAT:w" s="T152">сделано</ts>
                  <nts id="Seg_2204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_2206" n="HIAT:w" s="T153">и</ts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_2209" n="HIAT:w" s="T154">что</ts>
                  <nts id="Seg_2210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_2212" n="HIAT:w" s="T155">продают</ts>
                  <nts id="Seg_2213" n="HIAT:ip">.</nts>
                  <nts id="Seg_2214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T179" id="Seg_2215" n="sc" s="T157">
               <ts e="T179" id="Seg_2217" n="HIAT:u" s="T157">
                  <nts id="Seg_2218" n="HIAT:ip">(</nts>
                  <ts e="T158" id="Seg_2220" n="HIAT:w" s="T157">Вот=</ts>
                  <nts id="Seg_2221" n="HIAT:ip">)</nts>
                  <nts id="Seg_2222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_2224" n="HIAT:w" s="T158">Нет</ts>
                  <nts id="Seg_2225" n="HIAT:ip">,</nts>
                  <nts id="Seg_2226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_2228" n="HIAT:w" s="T159">говорила</ts>
                  <nts id="Seg_2229" n="HIAT:ip">,</nts>
                  <nts id="Seg_2230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_2232" n="HIAT:w" s="T160">как</ts>
                  <nts id="Seg_2233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_2235" n="HIAT:w" s="T161">это</ts>
                  <nts id="Seg_2236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_2238" n="HIAT:w" s="T162">ты</ts>
                  <nts id="Seg_2239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_2241" n="HIAT:w" s="T163">сказала</ts>
                  <nts id="Seg_2242" n="HIAT:ip">,</nts>
                  <nts id="Seg_2243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_2245" n="HIAT:w" s="T164">я</ts>
                  <nts id="Seg_2246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_2248" n="HIAT:w" s="T165">сейчас</ts>
                  <nts id="Seg_2249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_2251" n="HIAT:w" s="T166">не</ts>
                  <nts id="Seg_2252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_2254" n="HIAT:w" s="T167">заметил</ts>
                  <nts id="Seg_2255" n="HIAT:ip">,</nts>
                  <nts id="Seg_2256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_2258" n="HIAT:w" s="T168">вот</ts>
                  <nts id="Seg_2259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_2261" n="HIAT:w" s="T169">как</ts>
                  <nts id="Seg_2262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_2264" n="HIAT:w" s="T170">привозят</ts>
                  <nts id="Seg_2265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2266" n="HIAT:ip">(</nts>
                  <ts e="T172" id="Seg_2268" n="HIAT:w" s="T171">это=</ts>
                  <nts id="Seg_2269" n="HIAT:ip">)</nts>
                  <nts id="Seg_2270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_2272" n="HIAT:w" s="T172">мешки</ts>
                  <nts id="Seg_2273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_2275" n="HIAT:w" s="T173">с</ts>
                  <nts id="Seg_2276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_2278" n="HIAT:w" s="T174">мукой</ts>
                  <nts id="Seg_2279" n="HIAT:ip">,</nts>
                  <nts id="Seg_2280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_2282" n="HIAT:w" s="T175">ставят</ts>
                  <nts id="Seg_2283" n="HIAT:ip">,</nts>
                  <nts id="Seg_2284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_2286" n="HIAT:w" s="T176">вот</ts>
                  <nts id="Seg_2287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_2289" n="HIAT:w" s="T177">это</ts>
                  <nts id="Seg_2290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_2292" n="HIAT:w" s="T178">еще</ts>
                  <nts id="Seg_2293" n="HIAT:ip">.</nts>
                  <nts id="Seg_2294" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T273" id="Seg_2295" n="sc" s="T272">
               <ts e="T273" id="Seg_2297" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_2299" n="HIAT:w" s="T272">Так</ts>
                  <nts id="Seg_2300" n="HIAT:ip">.</nts>
                  <nts id="Seg_2301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T287" id="Seg_2302" n="sc" s="T274">
               <ts e="T280" id="Seg_2304" n="HIAT:u" s="T274">
                  <ts e="T275" id="Seg_2306" n="HIAT:w" s="T274">Придется</ts>
                  <nts id="Seg_2307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_2309" n="HIAT:w" s="T275">нам</ts>
                  <nts id="Seg_2310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_2312" n="HIAT:w" s="T276">кончать</ts>
                  <nts id="Seg_2313" n="HIAT:ip">,</nts>
                  <nts id="Seg_2314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_2316" n="HIAT:w" s="T277">видимо</ts>
                  <nts id="Seg_2317" n="HIAT:ip">,</nts>
                  <nts id="Seg_2318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_2320" n="HIAT:w" s="T278">на</ts>
                  <nts id="Seg_2321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_2323" n="HIAT:w" s="T279">сегодня</ts>
                  <nts id="Seg_2324" n="HIAT:ip">.</nts>
                  <nts id="Seg_2325" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T287" id="Seg_2327" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_2329" n="HIAT:w" s="T280">За</ts>
                  <nts id="Seg_2330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_2332" n="HIAT:w" s="T281">нами</ts>
                  <nts id="Seg_2333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_2335" n="HIAT:w" s="T282">машина</ts>
                  <nts id="Seg_2336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_2338" n="HIAT:w" s="T283">придет</ts>
                  <nts id="Seg_2339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_2341" n="HIAT:w" s="T284">через</ts>
                  <nts id="Seg_2342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_2344" n="HIAT:w" s="T285">пятнадцать</ts>
                  <nts id="Seg_2345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_2347" n="HIAT:w" s="T286">минут</ts>
                  <nts id="Seg_2348" n="HIAT:ip">.</nts>
                  <nts id="Seg_2349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KA">
            <ts e="T13" id="Seg_2350" n="sc" s="T10">
               <ts e="T13" id="Seg_2352" n="e" s="T10">((…)). </ts>
            </ts>
            <ts e="T16" id="Seg_2353" n="sc" s="T15">
               <ts e="T16" id="Seg_2355" n="e" s="T15">((…)). </ts>
            </ts>
            <ts e="T18" id="Seg_2356" n="sc" s="T17">
               <ts e="T18" id="Seg_2358" n="e" s="T17">((…)). </ts>
            </ts>
            <ts e="T23" id="Seg_2359" n="sc" s="T19">
               <ts e="T20" id="Seg_2361" n="e" s="T19">Так, </ts>
               <ts e="T21" id="Seg_2363" n="e" s="T20">значит, </ts>
               <ts e="T22" id="Seg_2365" n="e" s="T21">сделаем </ts>
               <ts e="T23" id="Seg_2367" n="e" s="T22">так. </ts>
            </ts>
            <ts e="T47" id="Seg_2368" n="sc" s="T24">
               <ts e="T25" id="Seg_2370" n="e" s="T24">Сначала </ts>
               <ts e="T26" id="Seg_2372" n="e" s="T25">по-камасински </ts>
               <ts e="T27" id="Seg_2374" n="e" s="T26">насчет </ts>
               <ts e="T28" id="Seg_2376" n="e" s="T27">этого </ts>
               <ts e="T29" id="Seg_2378" n="e" s="T28">спирта. </ts>
               <ts e="T30" id="Seg_2380" n="e" s="T29">А </ts>
               <ts e="T31" id="Seg_2382" n="e" s="T30">(затом-) </ts>
               <ts e="T32" id="Seg_2384" n="e" s="T31">затем </ts>
               <ts e="T33" id="Seg_2386" n="e" s="T32">как </ts>
               <ts e="T34" id="Seg_2388" n="e" s="T33">магазин </ts>
               <ts e="T35" id="Seg_2390" n="e" s="T34">сгорел, </ts>
               <ts e="T36" id="Seg_2392" n="e" s="T35">как </ts>
               <ts e="T37" id="Seg_2394" n="e" s="T36">новый </ts>
               <ts e="T39" id="Seg_2396" n="e" s="T37">построил, </ts>
               <ts e="T41" id="Seg_2398" n="e" s="T39">какие </ts>
               <ts e="T43" id="Seg_2400" n="e" s="T41">там </ts>
               <ts e="T45" id="Seg_2402" n="e" s="T43">новые </ts>
               <ts e="T47" id="Seg_2404" n="e" s="T45">товары. </ts>
            </ts>
            <ts e="T56" id="Seg_2405" n="sc" s="T50">
               <ts e="T52" id="Seg_2407" n="e" s="T50">(Из-) </ts>
               <ts e="T55" id="Seg_2409" n="e" s="T52">Изба </ts>
               <ts e="T56" id="Seg_2411" n="e" s="T55">сгорела. </ts>
            </ts>
            <ts e="T64" id="Seg_2412" n="sc" s="T60">
               <ts e="T62" id="Seg_2414" n="e" s="T60">Ага. </ts>
               <ts e="T64" id="Seg_2416" n="e" s="T62">Ага. </ts>
            </ts>
            <ts e="T83" id="Seg_2417" n="sc" s="T69">
               <ts e="T70" id="Seg_2419" n="e" s="T69">Ага, </ts>
               <ts e="T72" id="Seg_2421" n="e" s="T70">это </ts>
               <ts e="T73" id="Seg_2423" n="e" s="T72">склад </ts>
               <ts e="T74" id="Seg_2425" n="e" s="T73">пока </ts>
               <ts e="T76" id="Seg_2427" n="e" s="T74">что. </ts>
               <ts e="T77" id="Seg_2429" n="e" s="T76">Ну </ts>
               <ts e="T78" id="Seg_2431" n="e" s="T77">хорошо </ts>
               <ts e="T79" id="Seg_2433" n="e" s="T78">вот, </ts>
               <ts e="T80" id="Seg_2435" n="e" s="T79">начинаем, </ts>
               <ts e="T81" id="Seg_2437" n="e" s="T80">значит, </ts>
               <ts e="T82" id="Seg_2439" n="e" s="T81">(с) </ts>
               <ts e="T83" id="Seg_2441" n="e" s="T82">спирту. </ts>
            </ts>
            <ts e="T89" id="Seg_2442" n="sc" s="T86">
               <ts e="T88" id="Seg_2444" n="e" s="T86">Да, </ts>
               <ts e="T89" id="Seg_2446" n="e" s="T88">можно. </ts>
            </ts>
            <ts e="T156" id="Seg_2447" n="sc" s="T141">
               <ts e="T142" id="Seg_2449" n="e" s="T141">Ты </ts>
               <ts e="T143" id="Seg_2451" n="e" s="T142">еще </ts>
               <ts e="T144" id="Seg_2453" n="e" s="T143">рассказывала, </ts>
               <ts e="T145" id="Seg_2455" n="e" s="T144">что </ts>
               <ts e="T146" id="Seg_2457" n="e" s="T145">там </ts>
               <ts e="T147" id="Seg_2459" n="e" s="T146">в </ts>
               <ts e="T148" id="Seg_2461" n="e" s="T147">новом </ts>
               <ts e="T149" id="Seg_2463" n="e" s="T148">магазине, </ts>
               <ts e="T150" id="Seg_2465" n="e" s="T149">как </ts>
               <ts e="T151" id="Seg_2467" n="e" s="T150">это </ts>
               <ts e="T152" id="Seg_2469" n="e" s="T151">всё </ts>
               <ts e="T153" id="Seg_2471" n="e" s="T152">сделано </ts>
               <ts e="T154" id="Seg_2473" n="e" s="T153">и </ts>
               <ts e="T155" id="Seg_2475" n="e" s="T154">что </ts>
               <ts e="T156" id="Seg_2477" n="e" s="T155">продают. </ts>
            </ts>
            <ts e="T179" id="Seg_2478" n="sc" s="T157">
               <ts e="T158" id="Seg_2480" n="e" s="T157">(Вот=) </ts>
               <ts e="T159" id="Seg_2482" n="e" s="T158">Нет, </ts>
               <ts e="T160" id="Seg_2484" n="e" s="T159">говорила, </ts>
               <ts e="T161" id="Seg_2486" n="e" s="T160">как </ts>
               <ts e="T162" id="Seg_2488" n="e" s="T161">это </ts>
               <ts e="T163" id="Seg_2490" n="e" s="T162">ты </ts>
               <ts e="T164" id="Seg_2492" n="e" s="T163">сказала, </ts>
               <ts e="T165" id="Seg_2494" n="e" s="T164">я </ts>
               <ts e="T166" id="Seg_2496" n="e" s="T165">сейчас </ts>
               <ts e="T167" id="Seg_2498" n="e" s="T166">не </ts>
               <ts e="T168" id="Seg_2500" n="e" s="T167">заметил, </ts>
               <ts e="T169" id="Seg_2502" n="e" s="T168">вот </ts>
               <ts e="T170" id="Seg_2504" n="e" s="T169">как </ts>
               <ts e="T171" id="Seg_2506" n="e" s="T170">привозят </ts>
               <ts e="T172" id="Seg_2508" n="e" s="T171">(это=) </ts>
               <ts e="T173" id="Seg_2510" n="e" s="T172">мешки </ts>
               <ts e="T174" id="Seg_2512" n="e" s="T173">с </ts>
               <ts e="T175" id="Seg_2514" n="e" s="T174">мукой, </ts>
               <ts e="T176" id="Seg_2516" n="e" s="T175">ставят, </ts>
               <ts e="T177" id="Seg_2518" n="e" s="T176">вот </ts>
               <ts e="T178" id="Seg_2520" n="e" s="T177">это </ts>
               <ts e="T179" id="Seg_2522" n="e" s="T178">еще. </ts>
            </ts>
            <ts e="T273" id="Seg_2523" n="sc" s="T272">
               <ts e="T273" id="Seg_2525" n="e" s="T272">Так. </ts>
            </ts>
            <ts e="T287" id="Seg_2526" n="sc" s="T274">
               <ts e="T275" id="Seg_2528" n="e" s="T274">Придется </ts>
               <ts e="T276" id="Seg_2530" n="e" s="T275">нам </ts>
               <ts e="T277" id="Seg_2532" n="e" s="T276">кончать, </ts>
               <ts e="T278" id="Seg_2534" n="e" s="T277">видимо, </ts>
               <ts e="T279" id="Seg_2536" n="e" s="T278">на </ts>
               <ts e="T280" id="Seg_2538" n="e" s="T279">сегодня. </ts>
               <ts e="T281" id="Seg_2540" n="e" s="T280">За </ts>
               <ts e="T282" id="Seg_2542" n="e" s="T281">нами </ts>
               <ts e="T283" id="Seg_2544" n="e" s="T282">машина </ts>
               <ts e="T284" id="Seg_2546" n="e" s="T283">придет </ts>
               <ts e="T285" id="Seg_2548" n="e" s="T284">через </ts>
               <ts e="T286" id="Seg_2550" n="e" s="T285">пятнадцать </ts>
               <ts e="T287" id="Seg_2552" n="e" s="T286">минут. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KA">
            <ta e="T13" id="Seg_2553" s="T10">PKZ_19700819_09344-1az.KA.001 (004)</ta>
            <ta e="T16" id="Seg_2554" s="T15">PKZ_19700819_09344-1az.KA.002 (005)</ta>
            <ta e="T18" id="Seg_2555" s="T17">PKZ_19700819_09344-1az.KA.003 (006)</ta>
            <ta e="T23" id="Seg_2556" s="T19">PKZ_19700819_09344-1az.KA.004 (007)</ta>
            <ta e="T29" id="Seg_2557" s="T24">PKZ_19700819_09344-1az.KA.005 (008)</ta>
            <ta e="T47" id="Seg_2558" s="T29">PKZ_19700819_09344-1az.KA.006 (009)</ta>
            <ta e="T56" id="Seg_2559" s="T50">PKZ_19700819_09344-1az.KA.007 (011)</ta>
            <ta e="T62" id="Seg_2560" s="T60">PKZ_19700819_09344-1az.KA.008 (013)</ta>
            <ta e="T64" id="Seg_2561" s="T62">PKZ_19700819_09344-1az.KA.009 (015)</ta>
            <ta e="T76" id="Seg_2562" s="T69">PKZ_19700819_09344-1az.KA.010 (016)</ta>
            <ta e="T83" id="Seg_2563" s="T76">PKZ_19700819_09344-1az.KA.011 (017)</ta>
            <ta e="T89" id="Seg_2564" s="T86">PKZ_19700819_09344-1az.KA.012 (019)</ta>
            <ta e="T156" id="Seg_2565" s="T141">PKZ_19700819_09344-1az.KA.013 (028)</ta>
            <ta e="T179" id="Seg_2566" s="T157">PKZ_19700819_09344-1az.KA.014 (029)</ta>
            <ta e="T273" id="Seg_2567" s="T272">PKZ_19700819_09344-1az.KA.015 (044)</ta>
            <ta e="T280" id="Seg_2568" s="T274">PKZ_19700819_09344-1az.KA.016 (045)</ta>
            <ta e="T287" id="Seg_2569" s="T280">PKZ_19700819_09344-1az.KA.017 (046)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KA">
            <ta e="T13" id="Seg_2570" s="T10">((…)). </ta>
            <ta e="T16" id="Seg_2571" s="T15">((…)). </ta>
            <ta e="T18" id="Seg_2572" s="T17">((…)). </ta>
            <ta e="T23" id="Seg_2573" s="T19">Так, значит, сделаем так. </ta>
            <ta e="T29" id="Seg_2574" s="T24">Сначала по-камасински насчет этого спирта. </ta>
            <ta e="T47" id="Seg_2575" s="T29">А (затом-) затем как магазин сгорел, как новый построил, какие там новые товары. </ta>
            <ta e="T56" id="Seg_2576" s="T50">(Из-) Изба сгорела. </ta>
            <ta e="T62" id="Seg_2577" s="T60">Ага. </ta>
            <ta e="T64" id="Seg_2578" s="T62">Ага. </ta>
            <ta e="T76" id="Seg_2579" s="T69">Ага, это склад пока что. </ta>
            <ta e="T83" id="Seg_2580" s="T76">Ну хорошо вот, начинаем, значит, (с) спирту. </ta>
            <ta e="T89" id="Seg_2581" s="T86">Да, можно. </ta>
            <ta e="T156" id="Seg_2582" s="T141">Ты еще рассказывала, что там в новом магазине, как это всё сделано и что продают. </ta>
            <ta e="T179" id="Seg_2583" s="T157">(Вот=) Нет, говорила, как это ты сказала, я сейчас не заметил, вот как привозят (это=) мешки с мукой, ставят, вот это еще. </ta>
            <ta e="T273" id="Seg_2584" s="T272">Так. </ta>
            <ta e="T280" id="Seg_2585" s="T274">Придется нам кончать, видимо, на сегодня. </ta>
            <ta e="T287" id="Seg_2586" s="T280">За нами машина придет через пятнадцать минут. </ta>
         </annotation>
         <annotation name="CS" tierref="CS-KA">
            <ta e="T13" id="Seg_2587" s="T10">FIN:ext</ta>
            <ta e="T16" id="Seg_2588" s="T15">FIN:ext</ta>
            <ta e="T18" id="Seg_2589" s="T17">FIN:ext</ta>
            <ta e="T23" id="Seg_2590" s="T19">RUS:ext</ta>
            <ta e="T29" id="Seg_2591" s="T24">RUS:ext</ta>
            <ta e="T47" id="Seg_2592" s="T29">RUS:ext</ta>
            <ta e="T56" id="Seg_2593" s="T50">RUS:ext</ta>
            <ta e="T62" id="Seg_2594" s="T60">RUS:ext</ta>
            <ta e="T64" id="Seg_2595" s="T62">RUS:ext</ta>
            <ta e="T76" id="Seg_2596" s="T69">RUS:ext</ta>
            <ta e="T83" id="Seg_2597" s="T76">RUS:ext</ta>
            <ta e="T89" id="Seg_2598" s="T86">RUS:ext</ta>
            <ta e="T156" id="Seg_2599" s="T141">RUS:ext</ta>
            <ta e="T179" id="Seg_2600" s="T157">RUS:ext</ta>
            <ta e="T273" id="Seg_2601" s="T272">RUS:ext</ta>
            <ta e="T280" id="Seg_2602" s="T274">RUS:ext</ta>
            <ta e="T287" id="Seg_2603" s="T280">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KA" />
         <annotation name="fe" tierref="fe-KA">
            <ta e="T23" id="Seg_2604" s="T19">So, then we will do so.</ta>
            <ta e="T29" id="Seg_2605" s="T24">First, in Kamass about this alcohol.</ta>
            <ta e="T47" id="Seg_2606" s="T29">And then, how the store burned out, how the new one built, what new goods there.</ta>
            <ta e="T56" id="Seg_2607" s="T50">The hut burned down.</ta>
            <ta e="T62" id="Seg_2608" s="T60">Yes.</ta>
            <ta e="T64" id="Seg_2609" s="T62">Yes.</ta>
            <ta e="T76" id="Seg_2610" s="T69">Yeah, this is a warehouse for now.</ta>
            <ta e="T83" id="Seg_2611" s="T76">Well, here we go, that means with the alcohol.</ta>
            <ta e="T89" id="Seg_2612" s="T86">Yes, you can.</ta>
            <ta e="T156" id="Seg_2613" s="T141">You also said that there is a new store, how it is all done and what they sell.</ta>
            <ta e="T179" id="Seg_2614" s="T157">No, you said how … you said it, I haven’t noticed right now, here’s how they bring (this =) bags of flour, put them there, that's it.</ta>
            <ta e="T273" id="Seg_2615" s="T272">Well.</ta>
            <ta e="T280" id="Seg_2616" s="T274">We will have to finish, apparently, for today.</ta>
            <ta e="T287" id="Seg_2617" s="T280">The car will come for us in fifteen minutes.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KA">
            <ta e="T23" id="Seg_2618" s="T19">Also, dann werden wir es tun.</ta>
            <ta e="T29" id="Seg_2619" s="T24">Von Beginn an auf Kamassisch über diesen Alkohol.</ta>
            <ta e="T47" id="Seg_2620" s="T29">Und dann, als der Laden ausgebrannt ist, als der Neue gebaut hat, welche neuen Waren sind da.</ta>
            <ta e="T56" id="Seg_2621" s="T50">Die Hütte ist niedergebrannt.</ta>
            <ta e="T62" id="Seg_2622" s="T60">Ja.</ta>
            <ta e="T64" id="Seg_2623" s="T62">Ja.</ta>
            <ta e="T76" id="Seg_2624" s="T69">Ja, es ist vorläufig das Lager. </ta>
            <ta e="T83" id="Seg_2625" s="T76">Also los geht's, also mit dem Alkohol.</ta>
            <ta e="T89" id="Seg_2626" s="T86">Ja, du kannst.</ta>
            <ta e="T156" id="Seg_2627" s="T141">Du hast auch erzählt, dass es ein neues Geschäft gibt, wie es gemacht wird und was sie verkaufen.</ta>
            <ta e="T179" id="Seg_2628" s="T157">Nein, du hast gesagt, wie … du hast es gesagt, ich habe es im Moment nicht bemerkt, so bringen sie (dies =) Säcke Mehl, stellten sie hin, das war es.</ta>
            <ta e="T273" id="Seg_2629" s="T272">So.</ta>
            <ta e="T280" id="Seg_2630" s="T274">Wir werden für heute zum Ende kommen müssen.</ta>
            <ta e="T287" id="Seg_2631" s="T280">Das Auto wird uns in fünfzehn Minuten abholen.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KA" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-PKZ"
                          name="ref"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PKZ"
                          name="ts"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PKZ"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PKZ"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PKZ"
                          name="mb"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PKZ"
                          name="mp"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PKZ"
                          name="ge"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PKZ"
                          name="gr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PKZ"
                          name="mc"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PKZ"
                          name="ps"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PKZ"
                          name="SeR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PKZ"
                          name="SyF"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PKZ"
                          name="IST"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PKZ"
                          name="BOR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PKZ"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PKZ"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PKZ"
                          name="CS"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PKZ"
                          name="fr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PKZ"
                          name="fe"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PKZ"
                          name="fg"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PKZ"
                          name="nt"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-KA"
                          name="ref"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KA"
                          name="ts"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KA"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KA"
                          type="t" />
         <conversion-tier category="CS"
                          display-name="CS-KA"
                          name="CS"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KA"
                          name="fr"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KA"
                          name="fe"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KA"
                          name="fg"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KA"
                          name="nt"
                          segmented-tier-id="tx-KA"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
