<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID5E3FEBBC-7C61-E46E-81C5-DDCD2DBFA2DE">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_1964_SU0206.wav" />
         <referenced-file url="PKZ_1964_SU0206.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_1964_SU0206\PKZ_1964_SU0206.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1022</ud-information>
            <ud-information attribute-name="# HIAT:w">690</ud-information>
            <ud-information attribute-name="# e">681</ud-information>
            <ud-information attribute-name="# HIAT:u">192</ud-information>
            <ud-information attribute-name="# sc">24</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KA">
            <abbreviation>KA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.216" type="appl" />
         <tli id="T1" time="1.256" type="appl" />
         <tli id="T2" time="2.296" type="appl" />
         <tli id="T3" time="3.335" type="appl" />
         <tli id="T4" time="5.739984661449101" />
         <tli id="T5" time="6.755" type="appl" />
         <tli id="T6" time="7.459" type="appl" />
         <tli id="T7" time="8.163" type="appl" />
         <tli id="T8" time="8.867" type="appl" />
         <tli id="T9" time="9.407" type="appl" />
         <tli id="T10" time="9.946" type="appl" />
         <tli id="T11" time="10.486" type="appl" />
         <tli id="T12" time="11.246636613083195" />
         <tli id="T13" time="12.032" type="appl" />
         <tli id="T14" time="12.75" type="appl" />
         <tli id="T15" time="13.467" type="appl" />
         <tli id="T16" time="14.185" type="appl" />
         <tli id="T17" time="15.506625229419985" />
         <tli id="T18" time="16.144" type="appl" />
         <tli id="T19" time="16.752" type="appl" />
         <tli id="T20" time="17.766619190199595" />
         <tli id="T21" time="18.704" type="appl" />
         <tli id="T22" time="20.766611173535363" />
         <tli id="T23" time="21.465" type="appl" />
         <tli id="T24" time="22.138" type="appl" />
         <tli id="T25" time="22.81" type="appl" />
         <tli id="T26" time="23.483" type="appl" />
         <tli id="T27" time="24.559934370242143" />
         <tli id="T28" time="25.288" type="appl" />
         <tli id="T29" time="26.084" type="appl" />
         <tli id="T30" time="26.648" type="appl" />
         <tli id="T31" time="27.211" type="appl" />
         <tli id="T32" time="28.85992287969007" />
         <tli id="T33" time="29.905" type="appl" />
         <tli id="T34" time="30.474" type="appl" />
         <tli id="T35" time="31.044" type="appl" />
         <tli id="T36" time="31.614" type="appl" />
         <tli id="T37" time="32.183" type="appl" />
         <tli id="T38" time="32.753" type="appl" />
         <tli id="T39" time="33.996" type="appl" />
         <tli id="T40" time="35.106" type="appl" />
         <tli id="T41" time="36.216" type="appl" />
         <tli id="T42" time="37.325" type="appl" />
         <tli id="T43" time="38.097" type="appl" />
         <tli id="T44" time="38.87" type="appl" />
         <tli id="T45" time="39.642" type="appl" />
         <tli id="T46" time="40.531" type="appl" />
         <tli id="T47" time="41.229" type="appl" />
         <tli id="T48" time="41.927" type="appl" />
         <tli id="T49" time="42.624" type="appl" />
         <tli id="T50" time="43.322" type="appl" />
         <tli id="T51" time="44.45988119303607" />
         <tli id="T52" time="45.466" type="appl" />
         <tli id="T53" time="46.79987494003796" />
         <tli id="T54" time="49.17986858015101" />
         <tli id="T55" time="50.171" type="appl" />
         <tli id="T56" time="51.027" type="appl" />
         <tli id="T57" time="51.578" type="appl" />
         <tli id="T58" time="52.128" type="appl" />
         <tli id="T59" time="53.37985735682107" />
         <tli id="T60" time="55.67318456185997" />
         <tli id="T61" time="57.98651171345444" />
         <tli id="T62" time="58.865" type="appl" />
         <tli id="T63" time="59.71" type="appl" />
         <tli id="T64" time="60.457" type="appl" />
         <tli id="T65" time="61.203" type="appl" />
         <tli id="T66" time="61.916" type="appl" />
         <tli id="T67" time="62.77983223793981" />
         <tli id="T68" time="65.01315960331199" />
         <tli id="T69" time="65.683" type="appl" />
         <tli id="T70" time="66.409" type="appl" />
         <tli id="T71" time="67.134" type="appl" />
         <tli id="T72" time="67.86" type="appl" />
         <tli id="T73" time="68.585" type="appl" />
         <tli id="T74" time="69.198" type="appl" />
         <tli id="T75" time="69.719" type="appl" />
         <tli id="T76" time="70.241" type="appl" />
         <tli id="T77" time="70.762" type="appl" />
         <tli id="T78" time="71.283" type="appl" />
         <tli id="T79" time="71.888" type="appl" />
         <tli id="T80" time="72.494" type="appl" />
         <tli id="T81" time="73.099" type="appl" />
         <tli id="T82" time="73.686" type="appl" />
         <tli id="T83" time="74.273" type="appl" />
         <tli id="T84" time="74.86" type="appl" />
         <tli id="T85" time="75.645" type="appl" />
         <tli id="T86" time="76.429" type="appl" />
         <tli id="T87" time="77.43312641432206" />
         <tli id="T88" time="78.166" type="appl" />
         <tli id="T89" time="78.945" type="appl" />
         <tli id="T90" time="79.74645356591653" />
         <tli id="T91" time="80.65" type="appl" />
         <tli id="T92" time="81.576" type="appl" />
         <tli id="T93" time="82.502" type="appl" />
         <tli id="T94" time="83.429" type="appl" />
         <tli id="T95" time="85.29310541066178" />
         <tli id="T96" time="86.374" type="appl" />
         <tli id="T97" time="87.204" type="appl" />
         <tli id="T98" time="87.856" type="appl" />
         <tli id="T99" time="88.508" type="appl" />
         <tli id="T100" time="89.16" type="appl" />
         <tli id="T101" time="89.83" type="appl" />
         <tli id="T102" time="90.501" type="appl" />
         <tli id="T103" time="91.105" type="appl" />
         <tli id="T104" time="92.41308638444532" />
         <tli id="T105" time="93.154" type="appl" />
         <tli id="T106" time="95.48641150515152" />
         <tli id="T107" time="96.416" type="appl" />
         <tli id="T108" time="97.51973940496796" />
         <tli id="T109" time="98.108" type="appl" />
         <tli id="T110" time="98.595" type="appl" />
         <tli id="T111" time="99.081" type="appl" />
         <tli id="T112" time="99.568" type="appl" />
         <tli id="T113" time="100.054" type="appl" />
         <tli id="T114" time="100.54" type="appl" />
         <tli id="T115" time="101.027" type="appl" />
         <tli id="T116" time="101.513" type="appl" />
         <tli id="T117" time="102.0" type="appl" />
         <tli id="T118" time="103.44639023426909" />
         <tli id="T119" time="104.46" type="appl" />
         <tli id="T120" time="105.22" type="appl" />
         <tli id="T121" time="105.981" type="appl" />
         <tli id="T122" time="107.14638034704987" />
         <tli id="T123" time="109.5730405291259" />
         <tli id="T124" time="110.706" type="appl" />
         <tli id="T125" time="111.56100396683324" />
         <tli id="T126" time="111.966" type="appl" />
         <tli id="T127" time="112.312" type="appl" />
         <tli id="T128" time="112.657" type="appl" />
         <tli id="T129" time="113.002" type="appl" />
         <tli id="T130" time="113.348" type="appl" />
         <tli id="T131" time="113.693" type="appl" />
         <tli id="T132" time="114.038" type="appl" />
         <tli id="T133" time="114.384" type="appl" />
         <tli id="T134" time="114.729" type="appl" />
         <tli id="T135" time="115.075" type="appl" />
         <tli id="T136" time="115.42" type="appl" />
         <tli id="T137" time="115.765" type="appl" />
         <tli id="T138" time="116.111" type="appl" />
         <tli id="T139" time="116.456" type="appl" />
         <tli id="T140" time="118.69301615846665" />
         <tli id="T141" time="119.529" type="appl" />
         <tli id="T142" time="120.219" type="appl" />
         <tli id="T143" time="120.908" type="appl" />
         <tli id="T144" time="121.564" type="appl" />
         <tli id="T145" time="122.22" type="appl" />
         <tli id="T146" time="122.876" type="appl" />
         <tli id="T147" time="124.31966778943415" />
         <tli id="T148" time="125.037" type="appl" />
         <tli id="T149" time="127.3529930170292" />
         <tli id="T150" time="127.99" type="appl" />
         <tli id="T151" time="128.579" type="appl" />
         <tli id="T152" time="129.167" type="appl" />
         <tli id="T153" time="129.756" type="appl" />
         <tli id="T154" time="130.76631722918015" />
         <tli id="T155" time="131.548" type="appl" />
         <tli id="T156" time="132.165" type="appl" />
         <tli id="T157" time="132.782" type="appl" />
         <tli id="T158" time="133.399" type="appl" />
         <tli id="T159" time="134.016" type="appl" />
         <tli id="T160" time="134.633" type="appl" />
         <tli id="T161" time="135.25" type="appl" />
         <tli id="T162" time="135.867" type="appl" />
         <tli id="T163" time="139.09296164514987" />
         <tli id="T164" time="140.058" type="appl" />
         <tli id="T165" time="140.845" type="appl" />
         <tli id="T166" time="141.631" type="appl" />
         <tli id="T167" time="142.418" type="appl" />
         <tli id="T168" time="143.5596163758942" />
         <tli id="T169" time="144.563" type="appl" />
         <tli id="T170" time="145.60627757341442" />
         <tli id="T171" time="146.466" type="appl" />
         <tli id="T172" time="148.73960253378732" />
         <tli id="T173" time="149.509" type="appl" />
         <tli id="T174" time="150.245" type="appl" />
         <tli id="T175" time="151.7795944102342" />
         <tli id="T176" time="154.47292054638456" />
         <tli id="T692" time="154.484" type="intp" />
         <tli id="T177" time="156.43958195768244" />
         <tli id="T178" time="157.532" type="appl" />
         <tli id="T179" time="158.285" type="appl" />
         <tli id="T180" time="159.038" type="appl" />
         <tli id="T181" time="160.5329043527228" />
         <tli id="T182" time="161.345" type="appl" />
         <tli id="T183" time="161.997" type="appl" />
         <tli id="T184" time="162.648" type="appl" />
         <tli id="T185" time="163.3" type="appl" />
         <tli id="T186" time="163.951" type="appl" />
         <tli id="T187" time="164.731" type="appl" />
         <tli id="T188" time="165.51" type="appl" />
         <tli id="T189" time="166.29" type="appl" />
         <tli id="T190" time="167.07" type="appl" />
         <tli id="T191" time="168.314" type="appl" />
         <tli id="T192" time="169.287" type="appl" />
         <tli id="T193" time="170.77287698917553" />
         <tli id="T194" time="171.862" type="appl" />
         <tli id="T195" time="173.51953631614072" />
         <tli id="T196" time="174.18" type="appl" />
         <tli id="T197" time="174.804" type="appl" />
         <tli id="T198" time="175.8928633074019" />
         <tli id="T199" time="176.876" type="appl" />
         <tli id="T200" time="177.789" type="appl" />
         <tli id="T201" time="179.49285368740482" />
         <tli id="T202" time="180.414" type="appl" />
         <tli id="T203" time="181.138" type="appl" />
         <tli id="T204" time="181.91" type="appl" />
         <tli id="T205" time="182.682" type="appl" />
         <tli id="T206" time="183.455" type="appl" />
         <tli id="T207" time="184.57284011252008" />
         <tli id="T208" time="185.187" type="appl" />
         <tli id="T209" time="185.806" type="appl" />
         <tli id="T210" time="187.07283343196656" />
         <tli id="T211" time="187.821" type="appl" />
         <tli id="T212" time="188.538" type="appl" />
         <tli id="T213" time="191.3728219414145" />
         <tli id="T214" time="191.913" type="appl" />
         <tli id="T215" time="192.649" type="appl" />
         <tli id="T216" time="193.9261484516758" />
         <tli id="T217" time="194.62" type="appl" />
         <tli id="T218" time="195.292" type="appl" />
         <tli id="T219" time="196.5928079924187" />
         <tli id="T220" time="197.511" type="appl" />
         <tli id="T221" time="198.268" type="appl" />
         <tli id="T222" time="199.65946646427304" />
         <tli id="T223" time="200.407" type="appl" />
         <tli id="T224" time="201.102" type="appl" />
         <tli id="T225" time="202.41945908894198" />
         <tli id="T226" time="205.03945208772186" />
         <tli id="T227" time="205.74" type="appl" />
         <tli id="T228" time="206.58" type="appl" />
         <tli id="T229" time="207.419" type="appl" />
         <tli id="T230" time="208.02" type="appl" />
         <tli id="T231" time="208.62" type="appl" />
         <tli id="T232" time="209.221" type="appl" />
         <tli id="T233" time="209.821" type="appl" />
         <tli id="T234" time="210.507" type="appl" />
         <tli id="T235" time="212.11943316839427" />
         <tli id="T236" time="212.598" type="appl" />
         <tli id="T237" time="213.037" type="appl" />
         <tli id="T238" time="213.477" type="appl" />
         <tli id="T239" time="213.917" type="appl" />
         <tli id="T240" time="214.356" type="appl" />
         <tli id="T241" time="215.79275668576764" />
         <tli id="T242" time="216.781" type="appl" />
         <tli id="T243" time="217.526" type="appl" />
         <tli id="T244" time="218.272" type="appl" />
         <tli id="T245" time="219.054" type="appl" />
         <tli id="T246" time="219.837" type="appl" />
         <tli id="T247" time="224.39273370466347" />
         <tli id="T248" time="225.35" type="appl" />
         <tli id="T249" time="226.045" type="appl" />
         <tli id="T250" time="226.74" type="appl" />
         <tli id="T251" time="227.65272499322168" />
         <tli id="T252" time="228.897" type="appl" />
         <tli id="T253" time="230.38605102248314" />
         <tli id="T254" time="231.04" type="appl" />
         <tli id="T255" time="231.532" type="appl" />
         <tli id="T256" time="233.69270885300435" />
         <tli id="T257" time="234.352" type="appl" />
         <tli id="T258" time="234.828" type="appl" />
         <tli id="T259" time="235.304" type="appl" />
         <tli id="T260" time="235.78" type="appl" />
         <tli id="T261" time="236.256" type="appl" />
         <tli id="T262" time="236.732" type="appl" />
         <tli id="T263" time="237.208" type="appl" />
         <tli id="T264" time="241.33935508608462" />
         <tli id="T265" time="242.035" type="appl" />
         <tli id="T266" time="242.414" type="appl" />
         <tli id="T267" time="242.794" type="appl" />
         <tli id="T268" time="243.173" type="appl" />
         <tli id="T269" time="244.1993474435314" />
         <tli id="T270" time="244.758" type="appl" />
         <tli id="T271" time="245.325" type="appl" />
         <tli id="T272" time="245.892" type="appl" />
         <tli id="T273" time="254.99931858354014" />
         <tli id="T274" time="255.568" type="appl" />
         <tli id="T275" time="256.177" type="appl" />
         <tli id="T276" time="256.785" type="appl" />
         <tli id="T277" time="257.394" type="appl" />
         <tli id="T278" time="260.92596941284125" />
         <tli id="T279" time="261.804" type="appl" />
         <tli id="T280" time="263.20596332017647" />
         <tli id="T281" time="264.386" type="appl" />
         <tli id="T282" time="265.166" type="appl" />
         <tli id="T283" time="265.947" type="appl" />
         <tli id="T284" time="268.25928314988425" />
         <tli id="T285" time="268.648" type="appl" />
         <tli id="T286" time="269.11" type="appl" />
         <tli id="T287" time="269.572" type="appl" />
         <tli id="T288" time="270.034" type="appl" />
         <tli id="T289" time="270.496" type="appl" />
         <tli id="T290" time="270.958" type="appl" />
         <tli id="T291" time="271.98" type="appl" />
         <tli id="T292" time="272.99" type="appl" />
         <tli id="T293" time="273.652" type="appl" />
         <tli id="T294" time="274.314" type="appl" />
         <tli id="T295" time="274.977" type="appl" />
         <tli id="T296" time="282.3325788761549" />
         <tli id="T297" time="283.005" type="appl" />
         <tli id="T298" time="283.561" type="appl" />
         <tli id="T299" time="284.116" type="appl" />
         <tli id="T300" time="285.32590421063884" />
         <tli id="T301" time="286.075" type="appl" />
         <tli id="T302" time="286.72" type="appl" />
         <tli id="T303" time="287.4058986524183" />
         <tli id="T304" time="288.087" type="appl" />
         <tli id="T305" time="288.809" type="appl" />
         <tli id="T306" time="289.532" type="appl" />
         <tli id="T307" time="290.254" type="appl" />
         <tli id="T308" time="291.28588828419925" />
         <tli id="T309" time="293.0792168253488" />
         <tli id="T310" time="294.086" type="appl" />
         <tli id="T311" time="294.852" type="appl" />
         <tli id="T312" time="295.538" type="appl" />
         <tli id="T313" time="296.69920715190733" />
         <tli id="T314" time="297.554" type="appl" />
         <tli id="T315" time="298.407" type="appl" />
         <tli id="T316" time="300.4391971577992" />
         <tli id="T317" time="301.436" type="appl" />
         <tli id="T318" time="302.216" type="appl" />
         <tli id="T319" time="303.002" type="appl" />
         <tli id="T320" time="303.789" type="appl" />
         <tli id="T321" time="305.16585119376606" />
         <tli id="T322" time="305.998" type="appl" />
         <tli id="T323" time="306.62" type="appl" />
         <tli id="T324" time="307.242" type="appl" />
         <tli id="T325" time="307.865" type="appl" />
         <tli id="T326" time="308.472" type="appl" />
         <tli id="T327" time="309.08" type="appl" />
         <tli id="T328" time="309.687" type="appl" />
         <tli id="T329" time="311.59916733580826" />
         <tli id="T330" time="312.566" type="appl" />
         <tli id="T331" time="313.279" type="appl" />
         <tli id="T332" time="315.43915707447803" />
         <tli id="T333" time="316.577" type="appl" />
         <tli id="T334" time="317.551" type="appl" />
         <tli id="T335" time="318.344" type="appl" />
         <tli id="T336" time="319.9591449960373" />
         <tli id="T337" time="320.799" type="appl" />
         <tli id="T338" time="321.494" type="appl" />
         <tli id="T339" time="322.19" type="appl" />
         <tli id="T340" time="322.886" type="appl" />
         <tli id="T341" time="323.582" type="appl" />
         <tli id="T342" time="324.277" type="appl" />
         <tli id="T343" time="324.973" type="appl" />
         <tli id="T344" time="325.641" type="appl" />
         <tli id="T345" time="326.309" type="appl" />
         <tli id="T346" time="326.978" type="appl" />
         <tli id="T347" time="327.646" type="appl" />
         <tli id="T348" time="328.6524550988592" />
         <tli id="T349" time="329.356" type="appl" />
         <tli id="T350" time="329.868" type="appl" />
         <tli id="T351" time="330.379" type="appl" />
         <tli id="T352" time="330.891" type="appl" />
         <tli id="T353" time="333.61244184464095" />
         <tli id="T354" time="334.787" type="appl" />
         <tli id="T355" time="335.849" type="appl" />
         <tli id="T356" time="336.912" type="appl" />
         <tli id="T357" time="337.974" type="appl" />
         <tli id="T358" time="339.9390916050535" />
         <tli id="T359" time="340.614" type="appl" />
         <tli id="T360" time="341.265" type="appl" />
         <tli id="T361" time="341.915" type="appl" />
         <tli id="T362" time="342.565" type="appl" />
         <tli id="T363" time="343.215" type="appl" />
         <tli id="T364" time="343.866" type="appl" />
         <tli id="T365" time="344.9390782439464" />
         <tli id="T366" time="345.31" type="appl" />
         <tli id="T367" time="345.735" type="appl" />
         <tli id="T368" time="346.161" type="appl" />
         <tli id="T369" time="346.587" type="appl" />
         <tli id="T370" time="347.013" type="appl" />
         <tli id="T371" time="347.438" type="appl" />
         <tli id="T372" time="349.5323993028761" />
         <tli id="T373" time="350.451" type="appl" />
         <tli id="T374" time="351.067" type="appl" />
         <tli id="T375" time="351.684" type="appl" />
         <tli id="T376" time="352.5057246908044" />
         <tli id="T377" time="355.8790490098442" />
         <tli id="T378" time="357.082" type="appl" />
         <tli id="T379" time="358.129" type="appl" />
         <tli id="T380" time="359.175" type="appl" />
         <tli id="T381" time="362.3590316938495" />
         <tli id="T382" time="363.242" type="appl" />
         <tli id="T383" time="363.969" type="appl" />
         <tli id="T384" time="364.696" type="appl" />
         <tli id="T385" time="365.423" type="appl" />
         <tli id="T386" time="366.15" type="appl" />
         <tli id="T387" time="366.877" type="appl" />
         <tli id="T388" time="367.604" type="appl" />
         <tli id="T389" time="368.331" type="appl" />
         <tli id="T390" time="369.669" type="appl" />
         <tli id="T391" time="370.903" type="appl" />
         <tli id="T392" time="372.136" type="appl" />
         <tli id="T393" time="373.37" type="appl" />
         <tli id="T394" time="377.95899000719544" />
         <tli id="T395" time="378.8" type="appl" />
         <tli id="T396" time="379.598" type="appl" />
         <tli id="T397" time="380.395" type="appl" />
         <tli id="T398" time="381.192" type="appl" />
         <tli id="T399" time="381.989" type="appl" />
         <tli id="T400" time="382.787" type="appl" />
         <tli id="T401" time="384.752305187238" />
         <tli id="T402" time="385.692" type="appl" />
         <tli id="T403" time="386.542" type="appl" />
         <tli id="T404" time="387.393" type="appl" />
         <tli id="T405" time="388.243" type="appl" />
         <tli id="T406" time="391.37895414598415" />
         <tli id="T407" time="392.216" type="appl" />
         <tli id="T408" time="392.932" type="appl" />
         <tli id="T409" time="394.1389467706531" />
         <tli id="T410" time="395.622" type="appl" />
         <tli id="T411" time="396.943" type="appl" />
         <tli id="T412" time="398.8922674020272" />
         <tli id="T413" time="399.806" type="appl" />
         <tli id="T414" time="400.9522618972511" />
         <tli id="T415" time="401.826" type="appl" />
         <tli id="T416" time="405.4255832768473" />
         <tli id="T417" time="406.206" type="appl" />
         <tli id="T418" time="406.92" type="appl" />
         <tli id="T419" time="407.633" type="appl" />
         <tli id="T420" time="408.346" type="appl" />
         <tli id="T421" time="409.06" type="appl" />
         <tli id="T422" time="409.773" type="appl" />
         <tli id="T423" time="410.993" type="appl" />
         <tli id="T424" time="412.2722316477047" />
         <tli id="T425" time="413.111" type="appl" />
         <tli id="T426" time="414.023" type="appl" />
         <tli id="T427" time="414.934" type="appl" />
         <tli id="T428" time="415.885" type="appl" />
         <tli id="T429" time="416.661" type="appl" />
         <tli id="T430" time="417.438" type="appl" />
         <tli id="T431" time="418.215" type="appl" />
         <tli id="T432" time="418.991" type="appl" />
         <tli id="T433" time="419.768" type="appl" />
         <tli id="T434" time="421.032" type="appl" />
         <tli id="T435" time="422.297" type="appl" />
         <tli id="T436" time="423.1" type="appl" />
         <tli id="T437" time="423.904" type="appl" />
         <tli id="T438" time="425.094" type="appl" />
         <tli id="T439" time="426.284" type="appl" />
         <tli id="T440" time="429.2988528153482" />
         <tli id="T441" time="429.905" type="appl" />
         <tli id="T442" time="430.4388497690158" />
         <tli id="T443" time="431.093" type="appl" />
         <tli id="T444" time="431.786" type="appl" />
         <tli id="T445" time="432.479" type="appl" />
         <tli id="T446" time="433.172" type="appl" />
         <tli id="T447" time="433.865" type="appl" />
         <tli id="T448" time="434.558" type="appl" />
         <tli id="T449" time="435.251" type="appl" />
         <tli id="T450" time="435.944" type="appl" />
         <tli id="T451" time="436.637" type="appl" />
         <tli id="T452" time="438.30549541420737" />
         <tli id="T453" time="439.362" type="appl" />
         <tli id="T454" time="440.076" type="appl" />
         <tli id="T455" time="440.791" type="appl" />
         <tli id="T456" time="441.8854858476547" />
         <tli id="T457" time="443.022" type="appl" />
         <tli id="T458" time="444.205" type="appl" />
         <tli id="T459" time="445.579" type="appl" />
         <tli id="T460" time="446.813" type="appl" />
         <tli id="T461" time="450.27213010329115" />
         <tli id="T462" time="451.004" type="appl" />
         <tli id="T463" time="451.648" type="appl" />
         <tli id="T464" time="452.293" type="appl" />
         <tli id="T465" time="455.9454482762216" />
         <tli id="T466" time="456.558" type="appl" />
         <tli id="T467" time="457.172" type="appl" />
         <tli id="T468" time="457.959" type="appl" />
         <tli id="T469" time="458.746" type="appl" />
         <tli id="T470" time="460.5254360374476" />
         <tli id="T471" time="461.153" type="appl" />
         <tli id="T472" time="461.621" type="appl" />
         <tli id="T473" time="462.088" type="appl" />
         <tli id="T474" time="462.31209792974533" />
         <tli id="T475" time="462.768" type="appl" />
         <tli id="T476" time="463.374" type="appl" />
         <tli id="T477" time="463.98" type="appl" />
         <tli id="T478" time="464.982" type="appl" />
         <tli id="T479" time="465.698" type="appl" />
         <tli id="T480" time="466.3216705485843" />
         <tli id="T481" time="466.903" type="appl" />
         <tli id="T482" time="467.39" type="appl" />
         <tli id="T483" time="467.878" type="appl" />
         <tli id="T484" time="468.365" type="appl" />
         <tli id="T485" time="468.852" type="appl" />
         <tli id="T486" time="469.34" type="appl" />
         <tli id="T487" time="469.842" type="appl" />
         <tli id="T488" time="470.988" type="appl" />
         <tli id="T489" time="473.165402260569" />
         <tli id="T490" time="474.0" type="appl" />
         <tli id="T491" time="474.72" type="appl" />
         <tli id="T492" time="475.44" type="appl" />
         <tli id="T493" time="476.161" type="appl" />
         <tli id="T494" time="477.019" type="appl" />
         <tli id="T495" time="477.757" type="appl" />
         <tli id="T496" time="479.3253857996851" />
         <tli id="T497" time="480.047" type="appl" />
         <tli id="T498" time="480.612" type="appl" />
         <tli id="T499" time="481.177" type="appl" />
         <tli id="T500" time="481.741" type="appl" />
         <tli id="T501" time="482.306" type="appl" />
         <tli id="T502" time="482.871" type="appl" />
         <tli id="T503" time="483.606" type="appl" />
         <tli id="T504" time="484.316" type="appl" />
         <tli id="T505" time="485.048" type="appl" />
         <tli id="T506" time="485.781" type="appl" />
         <tli id="T507" time="486.513" type="appl" />
         <tli id="T508" time="487.166" type="appl" />
         <tli id="T509" time="487.697" type="appl" />
         <tli id="T510" time="488.228" type="appl" />
         <tli id="T511" time="488.76" type="appl" />
         <tli id="T512" time="489.292" type="appl" />
         <tli id="T513" time="490.8386883668425" />
         <tli id="T514" time="491.502" type="appl" />
         <tli id="T515" time="492.049" type="appl" />
         <tli id="T516" time="492.595" type="appl" />
         <tli id="T517" time="493.141" type="appl" />
         <tli id="T518" time="493.688" type="appl" />
         <tli id="T519" time="494.234" type="appl" />
         <tli id="T520" time="494.972" type="appl" />
         <tli id="T521" time="495.71" type="appl" />
         <tli id="T522" time="496.84533898236594" />
         <tli id="T523" time="497.558" type="appl" />
         <tli id="T524" time="498.25" type="appl" />
         <tli id="T525" time="498.942" type="appl" />
         <tli id="T526" time="499.9453306984796" />
         <tli id="T527" time="500.659" type="appl" />
         <tli id="T528" time="501.192" type="appl" />
         <tli id="T529" time="501.724" type="appl" />
         <tli id="T530" time="502.257" type="appl" />
         <tli id="T531" time="502.79" type="appl" />
         <tli id="T532" time="504.5653183528167" />
         <tli id="T533" time="505.314" type="appl" />
         <tli id="T534" time="506.163" type="appl" />
         <tli id="T535" time="507.012" type="appl" />
         <tli id="T536" time="507.8409866828155" />
         <tli id="T537" time="508.602" type="appl" />
         <tli id="T538" time="509.342" type="appl" />
         <tli id="T539" time="510.083" type="appl" />
         <tli id="T540" time="510.823" type="appl" />
         <tli id="T541" time="513.0386290435273" />
         <tli id="T542" time="513.819" type="appl" />
         <tli id="T543" time="514.248" type="appl" />
         <tli id="T544" time="514.677" type="appl" />
         <tli id="T545" time="515.106" type="appl" />
         <tli id="T546" time="515.535" type="appl" />
         <tli id="T547" time="516.465" type="appl" />
         <tli id="T548" time="517.396" type="appl" />
         <tli id="T549" time="518.5052811020502" />
         <tli id="T550" time="519.572" type="appl" />
         <tli id="T551" time="520.498" type="appl" />
         <tli id="T552" time="523.7986002904248" />
         <tli id="T553" time="524.771931022796" />
         <tli id="T554" time="525.796" type="appl" />
         <tli id="T555" time="527.1185914186498" />
         <tli id="T556" time="529.037" type="appl" />
         <tli id="T557" time="531.3319134930235" />
         <tli id="T558" time="531.836" type="appl" />
         <tli id="T559" time="532.225" type="appl" />
         <tli id="T560" time="532.615" type="appl" />
         <tli id="T561" time="533.005" type="appl" />
         <tli id="T562" time="533.395" type="appl" />
         <tli id="T563" time="533.784" type="appl" />
         <tli id="T564" time="534.6119047281373" />
         <tli id="T565" time="535.091" type="appl" />
         <tli id="T566" time="535.57" type="appl" />
         <tli id="T567" time="536.05" type="appl" />
         <tli id="T568" time="536.53" type="appl" />
         <tli id="T569" time="537.009" type="appl" />
         <tli id="T570" time="537.489" type="appl" />
         <tli id="T571" time="537.969" type="appl" />
         <tli id="T572" time="538.448" type="appl" />
         <tli id="T573" time="540.2185564125492" />
         <tli id="T574" time="540.837" type="appl" />
         <tli id="T575" time="541.08" type="appl" />
         <tli id="T576" time="541.323" type="appl" />
         <tli id="T577" time="542.1385512818841" />
         <tli id="T578" time="543.085" type="appl" />
         <tli id="T579" time="544.033" type="appl" />
         <tli id="T580" time="544.981" type="appl" />
         <tli id="T581" time="546.3652066539616" />
         <tli id="T582" time="547.129" type="appl" />
         <tli id="T583" time="547.728" type="appl" />
         <tli id="T584" time="548.327" type="appl" />
         <tli id="T585" time="548.926" type="appl" />
         <tli id="T586" time="549.457" type="appl" />
         <tli id="T587" time="549.987" type="appl" />
         <tli id="T588" time="550.5780078963938" />
         <tli id="T589" time="551.244" type="appl" />
         <tli id="T590" time="551.969" type="appl" />
         <tli id="T591" time="552.694" type="appl" />
         <tli id="T592" time="555.171849787265" />
         <tli id="T593" time="555.885" type="appl" />
         <tli id="T594" time="556.529" type="appl" />
         <tli id="T595" time="557.174" type="appl" />
         <tli id="T596" time="557.962" type="appl" />
         <tli id="T597" time="558.751" type="appl" />
         <tli id="T598" time="559.581" type="appl" />
         <tli id="T599" time="560.185" type="appl" />
         <tli id="T600" time="560.788" type="appl" />
         <tli id="T601" time="561.7984987460113" />
         <tli id="T602" time="563.45" type="appl" />
         <tli id="T603" time="564.834" type="appl" />
         <tli id="T604" time="565.275" type="appl" />
         <tli id="T605" time="565.716" type="appl" />
         <tli id="T606" time="566.158" type="appl" />
         <tli id="T607" time="566.599" type="appl" />
         <tli id="T608" time="567.04" type="appl" />
         <tli id="T609" time="567.482" type="appl" />
         <tli id="T610" time="567.923" type="appl" />
         <tli id="T611" time="571.5184727720191" />
         <tli id="T612" time="571.858" type="appl" />
         <tli id="T613" time="571.996" type="appl" />
         <tli id="T614" time="572.133" type="appl" />
         <tli id="T615" time="572.271" type="appl" />
         <tli id="T616" time="572.408" type="appl" />
         <tli id="T617" time="572.546" type="appl" />
         <tli id="T618" time="572.683" type="appl" />
         <tli id="T619" time="572.821" type="appl" />
         <tli id="T620" time="578.6917869366175" />
         <tli id="T621" time="579.4384516080255" />
         <tli id="T622" time="580.0917831955076" />
         <tli id="T623" time="580.3584491495818" />
         <tli id="T624" time="580.4851154777671" />
         <tli id="T625" time="581.2784466911381" />
         <tli id="T626" time="582.2651107212131" />
         <tli id="T627" time="582.938" type="appl" />
         <tli id="T628" time="583.556" type="appl" />
         <tli id="T629" time="584.175" type="appl" />
         <tli id="T630" time="586.145100352994" />
         <tli id="T631" time="586.987" type="appl" />
         <tli id="T632" time="587.925" type="appl" />
         <tli id="T633" time="588.952" type="appl" />
         <tli id="T634" time="589.852" type="appl" />
         <tli id="T635" time="590.822" type="appl" />
         <tli id="T636" time="591.473" type="appl" />
         <tli id="T637" time="592.125" type="appl" />
         <tli id="T638" time="592.776" type="appl" />
         <tli id="T639" time="593.428" type="appl" />
         <tli id="T640" time="594.132" type="appl" />
         <tli id="T641" time="594.617" type="appl" />
         <tli id="T642" time="595.101" type="appl" />
         <tli id="T643" time="595.586" type="appl" />
         <tli id="T644" time="596.575" type="appl" />
         <tli id="T645" time="597.493" type="appl" />
         <tli id="T646" time="600.105063048783" />
         <tli id="T647" time="600.87" type="appl" />
         <tli id="T648" time="601.453" type="appl" />
         <tli id="T649" time="602.0826619308535" />
         <tli id="T650" time="602.657" type="appl" />
         <tli id="T651" time="603.27" type="appl" />
         <tli id="T652" time="603.882" type="appl" />
         <tli id="T653" time="605.985047336121" />
         <tli id="T654" time="606.573" type="appl" />
         <tli id="T655" time="607.205" type="appl" />
         <tli id="T656" time="607.837" type="appl" />
         <tli id="T657" time="608.469" type="appl" />
         <tli id="T658" time="609.101" type="appl" />
         <tli id="T659" time="610.057" type="appl" />
         <tli id="T660" time="611.013" type="appl" />
         <tli id="T661" time="611.968" type="appl" />
         <tli id="T662" time="612.924" type="appl" />
         <tli id="T663" time="614.4783579733872" />
         <tli id="T664" time="615.451" type="appl" />
         <tli id="T665" time="616.269" type="appl" />
         <tli id="T666" time="617.088" type="appl" />
         <tli id="T667" time="617.907" type="appl" />
         <tli id="T668" time="618.726" type="appl" />
         <tli id="T669" time="619.544" type="appl" />
         <tli id="T670" time="620.363" type="appl" />
         <tli id="T671" time="621.117" type="appl" />
         <tli id="T672" time="621.87" type="appl" />
         <tli id="T673" time="622.589" type="appl" />
         <tli id="T674" time="623.308" type="appl" />
         <tli id="T675" time="624.162" type="appl" />
         <tli id="T676" time="625.2649958156924" />
         <tli id="T677" time="626.274" type="appl" />
         <tli id="T678" time="627.055" type="appl" />
         <tli id="T679" time="627.969" type="appl" />
         <tli id="T680" time="629.198318638288" />
         <tli id="T681" time="630.229" type="appl" />
         <tli id="T682" time="631.115" type="appl" />
         <tli id="T683" time="631.988" type="appl" />
         <tli id="T684" time="634.0383057047364" />
         <tli id="T685" time="635.236" type="appl" />
         <tli id="T686" time="636.004" type="appl" />
         <tli id="T687" time="636.772" type="appl" />
         <tli id="T688" time="637.539" type="appl" />
         <tli id="T689" time="638.474" type="appl" />
         <tli id="T690" time="639.275" type="appl" />
         <tli id="T691" time="639.291" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-KA"
                      id="tx-KA"
                      speaker="KA"
                      type="t">
         <timeline-fork end="T467" start="T465">
            <tli id="T465.tx-KA.1" />
         </timeline-fork>
         <timeline-fork end="T473" start="T470">
            <tli id="T470.tx-KA.1" />
            <tli id="T470.tx-KA.2" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KA">
            <ts e="T4" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Klavdia</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">Plotnikova</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_11" n="HIAT:w" s="T2">kolmas</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">lint</ts>
                  <nts id="Seg_15" n="HIAT:ip">.</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T177" id="Seg_17" n="sc" s="T176">
               <ts e="T177" id="Seg_19" n="HIAT:u" s="T176">
                  <ts e="T692" id="Seg_21" n="HIAT:w" s="T176">Можно</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_24" n="HIAT:w" s="T692">сначала</ts>
                  <nts id="Seg_25" n="HIAT:ip">.</nts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T309" id="Seg_27" n="sc" s="T308">
               <ts e="T309" id="Seg_29" n="HIAT:u" s="T308">
                  <ts e="T309" id="Seg_31" n="HIAT:w" s="T308">Mhmh</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T467" id="Seg_34" n="sc" s="T465">
               <ts e="T467" id="Seg_36" n="HIAT:u" s="T465">
                  <ts e="T465.tx-KA.1" id="Seg_38" n="HIAT:w" s="T465">Пьяные</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_41" n="HIAT:w" s="T465.tx-KA.1">были</ts>
                  <nts id="Seg_42" n="HIAT:ip">?</nts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T473" id="Seg_44" n="sc" s="T470">
               <ts e="T473" id="Seg_46" n="HIAT:u" s="T470">
                  <ts e="T470.tx-KA.1" id="Seg_48" n="HIAT:w" s="T470">Сегодня</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470.tx-KA.2" id="Seg_51" n="HIAT:w" s="T470.tx-KA.1">с</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_54" n="HIAT:w" s="T470.tx-KA.2">похмелья</ts>
                  <nts id="Seg_55" n="HIAT:ip">.</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T487" id="Seg_57" n="sc" s="T486">
               <ts e="T487" id="Seg_59" n="HIAT:u" s="T486">
                  <ts e="T487" id="Seg_61" n="HIAT:w" s="T486">Mhmh</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KA">
            <ts e="T4" id="Seg_64" n="sc" s="T0">
               <ts e="T1" id="Seg_66" n="e" s="T0">Klavdia </ts>
               <ts e="T2" id="Seg_68" n="e" s="T1">Plotnikova, </ts>
               <ts e="T3" id="Seg_70" n="e" s="T2">kolmas </ts>
               <ts e="T4" id="Seg_72" n="e" s="T3">lint. </ts>
            </ts>
            <ts e="T177" id="Seg_73" n="sc" s="T176">
               <ts e="T692" id="Seg_75" n="e" s="T176">Можно </ts>
               <ts e="T177" id="Seg_77" n="e" s="T692">сначала. </ts>
            </ts>
            <ts e="T309" id="Seg_78" n="sc" s="T308">
               <ts e="T309" id="Seg_80" n="e" s="T308">Mhmh. </ts>
            </ts>
            <ts e="T467" id="Seg_81" n="sc" s="T465">
               <ts e="T467" id="Seg_83" n="e" s="T465">Пьяные были? </ts>
            </ts>
            <ts e="T473" id="Seg_84" n="sc" s="T470">
               <ts e="T473" id="Seg_86" n="e" s="T470">Сегодня с похмелья. </ts>
            </ts>
            <ts e="T487" id="Seg_87" n="sc" s="T486">
               <ts e="T487" id="Seg_89" n="e" s="T486">Mhmh. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KA">
            <ta e="T4" id="Seg_90" s="T0">PKZ_1964_SU0206.KA.001 (001)</ta>
            <ta e="T177" id="Seg_91" s="T176">PKZ_1964_SU0206.KA.002 (011)</ta>
            <ta e="T309" id="Seg_92" s="T308">PKZ_1964_SU0206.KA.003 (020.007)</ta>
            <ta e="T467" id="Seg_93" s="T465">PKZ_1964_SU0206.KA.004 (031.002)</ta>
            <ta e="T473" id="Seg_94" s="T470">PKZ_1964_SU0206.KA.005 (031.004)</ta>
            <ta e="T487" id="Seg_95" s="T486">PKZ_1964_SU0206.KA.006 (031.008)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KA">
            <ta e="T4" id="Seg_96" s="T0">Klavdia Plotnikova, kolmas lint. </ta>
            <ta e="T177" id="Seg_97" s="T176">Можно сначала.</ta>
            <ta e="T309" id="Seg_98" s="T308">Mhmh. </ta>
            <ta e="T467" id="Seg_99" s="T465">Пьяные были? </ta>
            <ta e="T473" id="Seg_100" s="T470">Сегодня с похмелья. </ta>
            <ta e="T487" id="Seg_101" s="T486">Mhmh. </ta>
         </annotation>
         <annotation name="CS" tierref="CS-KA">
            <ta e="T177" id="Seg_102" s="T176">RUS:ext</ta>
            <ta e="T467" id="Seg_103" s="T465">RUS:ext</ta>
            <ta e="T473" id="Seg_104" s="T470">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KA">
            <ta e="T4" id="Seg_105" s="T0">Клавдия Плотникова, третья пленка</ta>
            <ta e="T177" id="Seg_106" s="T176">Можно сначала. </ta>
         </annotation>
         <annotation name="fe" tierref="fe-KA">
            <ta e="T4" id="Seg_107" s="T0">Klavdia Plotnikova, third tape.</ta>
            <ta e="T177" id="Seg_108" s="T176">You can start from the beginning.</ta>
            <ta e="T467" id="Seg_109" s="T465">Were you/they drunk?</ta>
            <ta e="T473" id="Seg_110" s="T470">Today hung over.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KA">
            <ta e="T4" id="Seg_111" s="T0">Klavdia Plotnikova, drittes Band.</ta>
            <ta e="T467" id="Seg_112" s="T465">Warst du / Waren sie betrunken?</ta>
            <ta e="T473" id="Seg_113" s="T470">Heute einen Kater.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KA" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-PKZ"
                      id="tx-PKZ"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T119" start="T118">
            <tli id="T118.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T146" start="T145">
            <tli id="T145.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T500" start="T499">
            <tli id="T499.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T577" start="T573">
            <tli id="T573.tx-PKZ.1" />
            <tli id="T573.tx-PKZ.2" />
            <tli id="T573.tx-PKZ.3" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PKZ">
            <ts e="T176" id="Seg_114" n="sc" s="T4">
               <ts e="T8" id="Seg_116" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_118" n="HIAT:w" s="T4">Nezeŋ</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_121" n="HIAT:w" s="T5">ugaːndə</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_124" n="HIAT:w" s="T6">iʔgö</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_127" n="HIAT:w" s="T7">šonəgaʔi</ts>
                  <nts id="Seg_128" n="HIAT:ip">.</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_131" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_133" n="HIAT:w" s="T8">Măn</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_136" n="HIAT:w" s="T9">mămbiam:</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_139" n="HIAT:w" s="T10">gijen</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_142" n="HIAT:w" s="T11">ibileʔ</ts>
                  <nts id="Seg_143" n="HIAT:ip">?</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_146" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_148" n="HIAT:w" s="T12">Dĭzeŋ</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_151" n="HIAT:w" s="T13">mănliaʔi:</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_154" n="HIAT:w" s="T14">tüžöj</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_157" n="HIAT:w" s="T15">surdəsʼtə</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_160" n="HIAT:w" s="T16">mĭmbibeʔ</ts>
                  <nts id="Seg_161" n="HIAT:ip">.</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_164" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_166" n="HIAT:w" s="T17">Deʔkeʔ</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_169" n="HIAT:w" s="T18">măna</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_172" n="HIAT:w" s="T19">sut</ts>
                  <nts id="Seg_173" n="HIAT:ip">!</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_176" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_178" n="HIAT:w" s="T20">Kabarləj</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_181" n="HIAT:w" s="T21">daška</ts>
                  <nts id="Seg_182" n="HIAT:ip">.</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_185" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_187" n="HIAT:w" s="T22">Ešši</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_190" n="HIAT:w" s="T23">bar</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_193" n="HIAT:w" s="T24">tüʔpi</ts>
                  <nts id="Seg_194" n="HIAT:ip">,</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_197" n="HIAT:w" s="T25">ugaːndə</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_200" n="HIAT:w" s="T26">sagər</ts>
                  <nts id="Seg_201" n="HIAT:ip">.</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_204" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_206" n="HIAT:w" s="T27">Bar</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_209" n="HIAT:w" s="T28">koluʔpi</ts>
                  <nts id="Seg_210" n="HIAT:ip">.</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_213" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_215" n="HIAT:w" s="T29">Nada</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_218" n="HIAT:w" s="T30">bazəsʼtə</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_221" n="HIAT:w" s="T31">dĭm</ts>
                  <nts id="Seg_222" n="HIAT:ip">.</nts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_225" n="HIAT:u" s="T32">
                  <nts id="Seg_226" n="HIAT:ip">(</nts>
                  <ts e="T33" id="Seg_228" n="HIAT:w" s="T32">Dĭ</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_231" n="HIAT:w" s="T33">-</ts>
                  <nts id="Seg_232" n="HIAT:ip">)</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_235" n="HIAT:w" s="T34">Kĭškəsʼtə</ts>
                  <nts id="Seg_236" n="HIAT:ip">,</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_239" n="HIAT:w" s="T35">dĭgəttə</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_242" n="HIAT:w" s="T36">bazəsʼtə</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_245" n="HIAT:w" s="T37">nada</ts>
                  <nts id="Seg_246" n="HIAT:ip">.</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_249" n="HIAT:u" s="T38">
                  <nts id="Seg_250" n="HIAT:ip">(</nts>
                  <ts e="T39" id="Seg_252" n="HIAT:w" s="T38">Am</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_255" n="HIAT:w" s="T39">-</ts>
                  <nts id="Seg_256" n="HIAT:ip">)</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_259" n="HIAT:w" s="T40">Kürzittə</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_262" n="HIAT:w" s="T41">nada</ts>
                  <nts id="Seg_263" n="HIAT:ip">.</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_266" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_268" n="HIAT:w" s="T42">Dĭgəttə</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_271" n="HIAT:w" s="T43">amzittə</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_274" n="HIAT:w" s="T44">mĭzittə</ts>
                  <nts id="Seg_275" n="HIAT:ip">.</nts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_278" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_280" n="HIAT:w" s="T45">Dĭgəttə</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_283" n="HIAT:w" s="T46">kunolzittə</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_285" n="HIAT:ip">(</nts>
                  <ts e="T48" id="Seg_287" n="HIAT:w" s="T47">iʔbəs</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_290" n="HIAT:w" s="T48">-</ts>
                  <nts id="Seg_291" n="HIAT:ip">)</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_293" n="HIAT:ip">(</nts>
                  <ts e="T50" id="Seg_295" n="HIAT:w" s="T49">iʔbətʼsʼiʔ</ts>
                  <nts id="Seg_296" n="HIAT:ip">)</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_299" n="HIAT:w" s="T50">nada</ts>
                  <nts id="Seg_300" n="HIAT:ip">.</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_303" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_305" n="HIAT:w" s="T51">Ordəʔ</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_308" n="HIAT:w" s="T52">eššim</ts>
                  <nts id="Seg_309" n="HIAT:ip">!</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_312" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_314" n="HIAT:w" s="T53">Baːr</ts>
                  <nts id="Seg_315" n="HIAT:ip">!</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_318" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_320" n="HIAT:w" s="T54">Amnoʔ</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_323" n="HIAT:w" s="T55">turagən</ts>
                  <nts id="Seg_324" n="HIAT:ip">!</nts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_327" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_329" n="HIAT:w" s="T56">Măn</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_332" n="HIAT:w" s="T57">nʼiʔdə</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_335" n="HIAT:w" s="T58">kallam</ts>
                  <nts id="Seg_336" n="HIAT:ip">!</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_339" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_341" n="HIAT:w" s="T59">Kĭnzəsʼtə</ts>
                  <nts id="Seg_342" n="HIAT:ip">.</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_345" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_347" n="HIAT:w" s="T60">Tüʔsittə</ts>
                  <nts id="Seg_348" n="HIAT:ip">.</nts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_351" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_353" n="HIAT:w" s="T61">Dĭgəttə</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_356" n="HIAT:w" s="T62">šolam</ts>
                  <nts id="Seg_357" n="HIAT:ip">.</nts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_360" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_362" n="HIAT:w" s="T63">Amnolam</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_365" n="HIAT:w" s="T64">tănziʔ</ts>
                  <nts id="Seg_366" n="HIAT:ip">.</nts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_369" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_371" n="HIAT:w" s="T65">I</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_374" n="HIAT:w" s="T66">dʼăbaktərləbaʔ</ts>
                  <nts id="Seg_375" n="HIAT:ip">!</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_378" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_380" n="HIAT:w" s="T67">Kabarləj</ts>
                  <nts id="Seg_381" n="HIAT:ip">.</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_384" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_386" n="HIAT:w" s="T68">Măn</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_389" n="HIAT:w" s="T69">kötenbə</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_392" n="HIAT:w" s="T70">kuvas</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_395" n="HIAT:w" s="T71">tăn</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_398" n="HIAT:w" s="T72">kadəldə</ts>
                  <nts id="Seg_399" n="HIAT:ip">.</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_402" n="HIAT:u" s="T73">
                  <nts id="Seg_403" n="HIAT:ip">(</nts>
                  <ts e="T74" id="Seg_405" n="HIAT:w" s="T73">Uba</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_408" n="HIAT:w" s="T74">-</ts>
                  <nts id="Seg_409" n="HIAT:ip">)</nts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_412" n="HIAT:w" s="T75">Ugaːndə</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_415" n="HIAT:w" s="T76">nʼešpək</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_418" n="HIAT:w" s="T77">köten</ts>
                  <nts id="Seg_419" n="HIAT:ip">.</nts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_422" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_424" n="HIAT:w" s="T78">Ugaːndə</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_427" n="HIAT:w" s="T79">sagər</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_430" n="HIAT:w" s="T80">köten</ts>
                  <nts id="Seg_431" n="HIAT:ip">.</nts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_434" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_436" n="HIAT:w" s="T81">Ugaːndə</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_439" n="HIAT:w" s="T82">komu</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_442" n="HIAT:w" s="T83">köten</ts>
                  <nts id="Seg_443" n="HIAT:ip">.</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_446" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_448" n="HIAT:w" s="T84">Ugaːndə</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_451" n="HIAT:w" s="T85">sĭre</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_454" n="HIAT:w" s="T86">köten</ts>
                  <nts id="Seg_455" n="HIAT:ip">.</nts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_458" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_460" n="HIAT:w" s="T87">Ugaːndə</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_463" n="HIAT:w" s="T88">nʼešpək</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_466" n="HIAT:w" s="T89">köten</ts>
                  <nts id="Seg_467" n="HIAT:ip">.</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_470" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_472" n="HIAT:w" s="T90">Ugaːndə</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_475" n="HIAT:w" s="T91">todam</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_477" n="HIAT:ip">(</nts>
                  <ts e="T93" id="Seg_479" n="HIAT:w" s="T92">kötek-</ts>
                  <nts id="Seg_480" n="HIAT:ip">)</nts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_483" n="HIAT:w" s="T93">köten</ts>
                  <nts id="Seg_484" n="HIAT:ip">.</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_487" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_489" n="HIAT:w" s="T94">Kabarləj</ts>
                  <nts id="Seg_490" n="HIAT:ip">.</nts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_493" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_495" n="HIAT:w" s="T95">Ešši</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_498" n="HIAT:w" s="T96">šonəgal</ts>
                  <nts id="Seg_499" n="HIAT:ip">.</nts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_502" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_504" n="HIAT:w" s="T97">Udazaŋdə</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_507" n="HIAT:w" s="T98">bar</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_510" n="HIAT:w" s="T99">balgaš</ts>
                  <nts id="Seg_511" n="HIAT:ip">.</nts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_514" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_516" n="HIAT:w" s="T100">Nada</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_519" n="HIAT:w" s="T101">bazəsʼtə</ts>
                  <nts id="Seg_520" n="HIAT:ip">.</nts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_523" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_525" n="HIAT:w" s="T102">Šoʔ</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_528" n="HIAT:w" s="T103">döbər</ts>
                  <nts id="Seg_529" n="HIAT:ip">!</nts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_532" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_534" n="HIAT:w" s="T104">Iʔ</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_537" n="HIAT:w" s="T105">dʼoraʔ</ts>
                  <nts id="Seg_538" n="HIAT:ip">!</nts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_541" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_543" n="HIAT:w" s="T106">Kanaʔ</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_546" n="HIAT:w" s="T107">maʔnəl</ts>
                  <nts id="Seg_547" n="HIAT:ip">!</nts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_550" n="HIAT:u" s="T108">
                  <nts id="Seg_551" n="HIAT:ip">(</nts>
                  <ts e="T109" id="Seg_553" n="HIAT:w" s="T108">Tăna</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_556" n="HIAT:w" s="T109">-</ts>
                  <nts id="Seg_557" n="HIAT:ip">)</nts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_560" n="HIAT:w" s="T110">Tănan</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_563" n="HIAT:w" s="T111">padʼi</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_565" n="HIAT:ip">(</nts>
                  <ts e="T113" id="Seg_567" n="HIAT:w" s="T112">mondro</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_570" n="HIAT:w" s="T113">-</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_573" n="HIAT:w" s="T114">mando</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_576" n="HIAT:w" s="T115">-</ts>
                  <nts id="Seg_577" n="HIAT:ip">)</nts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_580" n="HIAT:w" s="T116">măndolaʔbəʔjə</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_583" n="HIAT:w" s="T117">bar</ts>
                  <nts id="Seg_584" n="HIAT:ip">.</nts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_587" n="HIAT:u" s="T118">
                  <ts e="T118.tx-PKZ.1" id="Seg_589" n="HIAT:w" s="T118">A</ts>
                  <nts id="Seg_590" n="HIAT:ip">_</nts>
                  <ts e="T119" id="Seg_592" n="HIAT:w" s="T118.tx-PKZ.1">to</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_595" n="HIAT:w" s="T119">nabəʔi</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_597" n="HIAT:ip">(</nts>
                  <ts e="T121" id="Seg_599" n="HIAT:w" s="T120">bargamunəʔi</ts>
                  <nts id="Seg_600" n="HIAT:ip">)</nts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_603" n="HIAT:w" s="T121">tănan</ts>
                  <nts id="Seg_604" n="HIAT:ip">.</nts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_607" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_609" n="HIAT:w" s="T122">Bar</ts>
                  <nts id="Seg_610" n="HIAT:ip">.</nts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_613" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_615" n="HIAT:w" s="T123">Gibər</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_617" n="HIAT:ip">(</nts>
                  <ts e="T125" id="Seg_619" n="HIAT:w" s="T124">paʔlaːngəl</ts>
                  <nts id="Seg_620" n="HIAT:ip">)</nts>
                  <nts id="Seg_621" n="HIAT:ip">.</nts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_624" n="HIAT:u" s="T125">
                  <nts id="Seg_625" n="HIAT:ip">(</nts>
                  <ts e="T126" id="Seg_627" n="HIAT:w" s="T125">Iʔ</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_630" n="HIAT:w" s="T126">pănaʔ</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_633" n="HIAT:w" s="T127">-</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_636" n="HIAT:w" s="T128">i</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_639" n="HIAT:w" s="T129">-</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_642" n="HIAT:w" s="T130">iʔ</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_645" n="HIAT:w" s="T131">păʔ</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_648" n="HIAT:w" s="T132">-</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_651" n="HIAT:w" s="T133">iʔ</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_654" n="HIAT:w" s="T134">păllaʔ</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_657" n="HIAT:w" s="T135">-</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_660" n="HIAT:w" s="T136">iʔ</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_663" n="HIAT:w" s="T137">pănaʔgaʔ</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_666" n="HIAT:w" s="T138">-</ts>
                  <nts id="Seg_667" n="HIAT:ip">)</nts>
                  <nts id="Seg_668" n="HIAT:ip">.</nts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_671" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_673" n="HIAT:w" s="T139">Господи</ts>
                  <nts id="Seg_674" n="HIAT:ip">.</nts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_677" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_679" n="HIAT:w" s="T140">Gibər</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_681" n="HIAT:ip">(</nts>
                  <ts e="T142" id="Seg_683" n="HIAT:w" s="T141">pă-</ts>
                  <nts id="Seg_684" n="HIAT:ip">)</nts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_687" n="HIAT:w" s="T142">paʔlaːndəgal</ts>
                  <nts id="Seg_688" n="HIAT:ip">?</nts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_691" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_693" n="HIAT:w" s="T143">Iʔ</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_696" n="HIAT:w" s="T144">pădaʔ</ts>
                  <nts id="Seg_697" n="HIAT:ip">,</nts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145.tx-PKZ.1" id="Seg_700" n="HIAT:w" s="T145">a</ts>
                  <nts id="Seg_701" n="HIAT:ip">_</nts>
                  <ts e="T146" id="Seg_703" n="HIAT:w" s="T145.tx-PKZ.1">to</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_706" n="HIAT:w" s="T146">saʔməluʔləl</ts>
                  <nts id="Seg_707" n="HIAT:ip">!</nts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_710" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_712" n="HIAT:w" s="T147">Šoʔ</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_715" n="HIAT:w" s="T148">döbər</ts>
                  <nts id="Seg_716" n="HIAT:ip">!</nts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_719" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_721" n="HIAT:w" s="T149">Dĭ</ts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_724" n="HIAT:w" s="T150">koʔbdo</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_727" n="HIAT:w" s="T151">amnolaʔbə</ts>
                  <nts id="Seg_728" n="HIAT:ip">,</nts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_731" n="HIAT:w" s="T152">todam</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_734" n="HIAT:w" s="T153">bar</ts>
                  <nts id="Seg_735" n="HIAT:ip">.</nts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_738" n="HIAT:u" s="T154">
                  <nts id="Seg_739" n="HIAT:ip">(</nts>
                  <ts e="T155" id="Seg_741" n="HIAT:w" s="T154">M</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_744" n="HIAT:w" s="T155">e</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_747" n="HIAT:w" s="T156">ej</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_750" n="HIAT:w" s="T157">m</ts>
                  <nts id="Seg_751" n="HIAT:ip">)</nts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_754" n="HIAT:w" s="T158">Ĭmbi</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_757" n="HIAT:w" s="T159">măndəliam</ts>
                  <nts id="Seg_758" n="HIAT:ip">,</nts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_761" n="HIAT:w" s="T160">dĭ</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_764" n="HIAT:w" s="T161">ej</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_767" n="HIAT:w" s="T162">nʼilgölaʔbə</ts>
                  <nts id="Seg_768" n="HIAT:ip">.</nts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_771" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_773" n="HIAT:w" s="T163">Dʼijenə</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_775" n="HIAT:ip">(</nts>
                  <ts e="T165" id="Seg_777" n="HIAT:w" s="T164">kandləʔ</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_780" n="HIAT:w" s="T165">-</ts>
                  <nts id="Seg_781" n="HIAT:ip">)</nts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_784" n="HIAT:w" s="T166">kandəgaʔi</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_787" n="HIAT:w" s="T167">ineʔsiʔ</ts>
                  <nts id="Seg_788" n="HIAT:ip">.</nts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_791" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_793" n="HIAT:w" s="T168">Kola</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_796" n="HIAT:w" s="T169">dʼabəlaʔi</ts>
                  <nts id="Seg_797" n="HIAT:ip">.</nts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_800" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_802" n="HIAT:w" s="T170">Kola</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_805" n="HIAT:w" s="T171">deʔleʔi</ts>
                  <nts id="Seg_806" n="HIAT:ip">.</nts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_809" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_811" n="HIAT:w" s="T172">Măna</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_814" n="HIAT:w" s="T173">možet</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_817" n="HIAT:w" s="T174">mĭleʔi</ts>
                  <nts id="Seg_818" n="HIAT:ip">.</nts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T176" id="Seg_821" n="HIAT:u" s="T175">
                  <ts e="T176" id="Seg_823" n="HIAT:w" s="T175">Kabarləj</ts>
                  <nts id="Seg_824" n="HIAT:ip">.</nts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T308" id="Seg_826" n="sc" s="T177">
               <ts e="T181" id="Seg_828" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_830" n="HIAT:w" s="T177">Dʼagagən</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_833" n="HIAT:w" s="T178">udaziʔ</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_836" n="HIAT:w" s="T179">kola</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_839" n="HIAT:w" s="T180">dʼaːpiʔi</ts>
                  <nts id="Seg_840" n="HIAT:ip">.</nts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_843" n="HIAT:u" s="T181">
                  <nts id="Seg_844" n="HIAT:ip">(</nts>
                  <ts e="T182" id="Seg_846" n="HIAT:w" s="T181">Dʼünə</ts>
                  <nts id="Seg_847" n="HIAT:ip">)</nts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_850" n="HIAT:w" s="T182">Dʼünə</ts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_852" n="HIAT:ip">(</nts>
                  <ts e="T184" id="Seg_854" n="HIAT:w" s="T183">kə</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_857" n="HIAT:w" s="T184">-</ts>
                  <nts id="Seg_858" n="HIAT:ip">)</nts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_861" n="HIAT:w" s="T185">embiʔi</ts>
                  <nts id="Seg_862" n="HIAT:ip">.</nts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_865" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_867" n="HIAT:w" s="T186">Dĭgəttə</ts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_869" n="HIAT:ip">(</nts>
                  <ts e="T188" id="Seg_871" n="HIAT:w" s="T187">tuzʼdʼa</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_874" n="HIAT:w" s="T188">-</ts>
                  <nts id="Seg_875" n="HIAT:ip">)</nts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_878" n="HIAT:w" s="T189">tustʼarbiʔi</ts>
                  <nts id="Seg_879" n="HIAT:ip">.</nts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T192" id="Seg_882" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_884" n="HIAT:w" s="T190">Dĭgəttə</ts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_887" n="HIAT:w" s="T191">sadarbiʔi</ts>
                  <nts id="Seg_888" n="HIAT:ip">.</nts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_891" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_893" n="HIAT:w" s="T192">Kabar</ts>
                  <nts id="Seg_894" n="HIAT:ip">.</nts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T195" id="Seg_897" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_899" n="HIAT:w" s="T193">Šĭšəge</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_902" n="HIAT:w" s="T194">molaːmbi</ts>
                  <nts id="Seg_903" n="HIAT:ip">.</nts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_906" n="HIAT:u" s="T195">
                  <ts e="T196" id="Seg_908" n="HIAT:w" s="T195">Ejü</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_911" n="HIAT:w" s="T196">kalla</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_914" n="HIAT:w" s="T197">dʼürbi</ts>
                  <nts id="Seg_915" n="HIAT:ip">.</nts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T201" id="Seg_918" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_920" n="HIAT:w" s="T198">Kuja</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_923" n="HIAT:w" s="T199">amga</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_926" n="HIAT:w" s="T200">măndolaʔbə</ts>
                  <nts id="Seg_927" n="HIAT:ip">.</nts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T203" id="Seg_930" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_932" n="HIAT:w" s="T201">Sĭre</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_935" n="HIAT:w" s="T202">šonəga</ts>
                  <nts id="Seg_936" n="HIAT:ip">.</nts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_939" n="HIAT:u" s="T203">
                  <ts e="T204" id="Seg_941" n="HIAT:w" s="T203">Beržə</ts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_944" n="HIAT:w" s="T204">bar</ts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_947" n="HIAT:w" s="T205">ugaːndə</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_950" n="HIAT:w" s="T206">šĭšəge</ts>
                  <nts id="Seg_951" n="HIAT:ip">.</nts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T210" id="Seg_954" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_956" n="HIAT:w" s="T207">Măn</ts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_959" n="HIAT:w" s="T208">pa</ts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_962" n="HIAT:w" s="T209">iʔgö</ts>
                  <nts id="Seg_963" n="HIAT:ip">.</nts>
                  <nts id="Seg_964" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T213" id="Seg_966" n="HIAT:u" s="T210">
                  <ts e="T211" id="Seg_968" n="HIAT:w" s="T210">Dĭ</ts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_971" n="HIAT:w" s="T211">könə</ts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_974" n="HIAT:w" s="T212">kabarləj</ts>
                  <nts id="Seg_975" n="HIAT:ip">.</nts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T216" id="Seg_978" n="HIAT:u" s="T213">
                  <ts e="T214" id="Seg_980" n="HIAT:w" s="T213">Ĭmbi</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_983" n="HIAT:w" s="T214">ulul</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_986" n="HIAT:w" s="T215">tĭlliel</ts>
                  <nts id="Seg_987" n="HIAT:ip">?</nts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T219" id="Seg_990" n="HIAT:u" s="T216">
                  <ts e="T217" id="Seg_992" n="HIAT:w" s="T216">Padʼi</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_995" n="HIAT:w" s="T217">unuʔi</ts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_998" n="HIAT:w" s="T218">ige</ts>
                  <nts id="Seg_999" n="HIAT:ip">.</nts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T222" id="Seg_1002" n="HIAT:u" s="T219">
                  <ts e="T220" id="Seg_1004" n="HIAT:w" s="T219">Ugaːndə</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_1007" n="HIAT:w" s="T220">iʔgö</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_1010" n="HIAT:w" s="T221">unu</ts>
                  <nts id="Seg_1011" n="HIAT:ip">.</nts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_1014" n="HIAT:u" s="T222">
                  <ts e="T223" id="Seg_1016" n="HIAT:w" s="T222">Kuʔsʼittə</ts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_1019" n="HIAT:w" s="T223">nada</ts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_1022" n="HIAT:w" s="T224">dĭzeŋ</ts>
                  <nts id="Seg_1023" n="HIAT:ip">.</nts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_1026" n="HIAT:u" s="T225">
                  <ts e="T226" id="Seg_1028" n="HIAT:w" s="T225">Kabarləj</ts>
                  <nts id="Seg_1029" n="HIAT:ip">.</nts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_1032" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_1034" n="HIAT:w" s="T226">Măn</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_1037" n="HIAT:w" s="T227">iʔbəbiem</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_1040" n="HIAT:w" s="T228">kunolzittə</ts>
                  <nts id="Seg_1041" n="HIAT:ip">.</nts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T233" id="Seg_1044" n="HIAT:u" s="T229">
                  <ts e="T230" id="Seg_1046" n="HIAT:w" s="T229">Kuriza</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_1049" n="HIAT:w" s="T230">ej</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_1052" n="HIAT:w" s="T231">mĭbi</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_1055" n="HIAT:w" s="T232">kunolzittə</ts>
                  <nts id="Seg_1056" n="HIAT:ip">.</nts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T235" id="Seg_1059" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_1061" n="HIAT:w" s="T233">Măn</ts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_1064" n="HIAT:w" s="T234">uʔbdəbiam</ts>
                  <nts id="Seg_1065" n="HIAT:ip">.</nts>
                  <nts id="Seg_1066" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T241" id="Seg_1068" n="HIAT:u" s="T235">
                  <ts e="T236" id="Seg_1070" n="HIAT:w" s="T235">I</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1072" n="HIAT:ip">(</nts>
                  <ts e="T237" id="Seg_1074" n="HIAT:w" s="T236">da</ts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_1077" n="HIAT:w" s="T237">-</ts>
                  <nts id="Seg_1078" n="HIAT:ip">)</nts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_1081" n="HIAT:w" s="T238">daška</ts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_1084" n="HIAT:w" s="T239">ej</ts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_1087" n="HIAT:w" s="T240">iʔbiem</ts>
                  <nts id="Seg_1088" n="HIAT:ip">.</nts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_1091" n="HIAT:u" s="T241">
                  <ts e="T242" id="Seg_1093" n="HIAT:w" s="T241">Kuvas</ts>
                  <nts id="Seg_1094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1095" n="HIAT:ip">(</nts>
                  <ts e="T243" id="Seg_1097" n="HIAT:w" s="T242">nüke</ts>
                  <nts id="Seg_1098" n="HIAT:ip">)</nts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_1101" n="HIAT:w" s="T243">amnolaʔbə</ts>
                  <nts id="Seg_1102" n="HIAT:ip">.</nts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T247" id="Seg_1105" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_1107" n="HIAT:w" s="T244">Kuvas</ts>
                  <nts id="Seg_1108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_1110" n="HIAT:w" s="T245">nʼi</ts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_1113" n="HIAT:w" s="T246">amnolaʔbə</ts>
                  <nts id="Seg_1114" n="HIAT:ip">.</nts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_1117" n="HIAT:u" s="T247">
                  <ts e="T248" id="Seg_1119" n="HIAT:w" s="T247">Măn</ts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_1122" n="HIAT:w" s="T248">ugaːndə</ts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_1125" n="HIAT:w" s="T249">dʼaktə</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_1128" n="HIAT:w" s="T250">molaːmbiam</ts>
                  <nts id="Seg_1129" n="HIAT:ip">.</nts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T253" id="Seg_1132" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_1134" n="HIAT:w" s="T251">Šidegöʔ</ts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_1137" n="HIAT:w" s="T252">malaʔpibaʔ</ts>
                  <nts id="Seg_1138" n="HIAT:ip">.</nts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_1141" n="HIAT:u" s="T253">
                  <ts e="T254" id="Seg_1143" n="HIAT:w" s="T253">Măn</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_1146" n="HIAT:w" s="T254">da</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_1149" n="HIAT:w" s="T255">kagam</ts>
                  <nts id="Seg_1150" n="HIAT:ip">.</nts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T264" id="Seg_1153" n="HIAT:u" s="T256">
                  <nts id="Seg_1154" n="HIAT:ip">(</nts>
                  <ts e="T257" id="Seg_1156" n="HIAT:w" s="T256">Dĭ</ts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_1159" n="HIAT:w" s="T257">-</ts>
                  <nts id="Seg_1160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_1162" n="HIAT:w" s="T258">-</ts>
                  <nts id="Seg_1163" n="HIAT:ip">)</nts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_1166" n="HIAT:w" s="T259">A</ts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_1169" n="HIAT:w" s="T260">dĭzeŋ</ts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1172" n="HIAT:w" s="T261">bar</ts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1175" n="HIAT:w" s="T262">külaːmbiʔi</ts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1178" n="HIAT:w" s="T263">bar</ts>
                  <nts id="Seg_1179" n="HIAT:ip">.</nts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T269" id="Seg_1182" n="HIAT:u" s="T264">
                  <ts e="T265" id="Seg_1184" n="HIAT:w" s="T264">Ну</ts>
                  <nts id="Seg_1185" n="HIAT:ip">,</nts>
                  <nts id="Seg_1186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1188" n="HIAT:w" s="T265">наверное</ts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1191" n="HIAT:w" s="T266">все</ts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1194" n="HIAT:w" s="T267">таперь</ts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1197" n="HIAT:w" s="T268">уже</ts>
                  <nts id="Seg_1198" n="HIAT:ip">.</nts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T273" id="Seg_1201" n="HIAT:u" s="T269">
                  <ts e="T270" id="Seg_1203" n="HIAT:w" s="T269">Iʔgö</ts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1206" n="HIAT:w" s="T270">pʼe</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1209" n="HIAT:w" s="T271">kambi</ts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1212" n="HIAT:w" s="T272">bar</ts>
                  <nts id="Seg_1213" n="HIAT:ip">.</nts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T278" id="Seg_1216" n="HIAT:u" s="T273">
                  <ts e="T274" id="Seg_1218" n="HIAT:w" s="T273">Bor</ts>
                  <nts id="Seg_1219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1221" n="HIAT:w" s="T274">dʼünə</ts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1223" n="HIAT:ip">(</nts>
                  <ts e="T276" id="Seg_1225" n="HIAT:w" s="T275">am</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1228" n="HIAT:w" s="T276">-</ts>
                  <nts id="Seg_1229" n="HIAT:ip">)</nts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1232" n="HIAT:w" s="T277">amnolial</ts>
                  <nts id="Seg_1233" n="HIAT:ip">.</nts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T280" id="Seg_1236" n="HIAT:u" s="T278">
                  <ts e="T279" id="Seg_1238" n="HIAT:w" s="T278">Surno</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1241" n="HIAT:w" s="T279">moləj</ts>
                  <nts id="Seg_1242" n="HIAT:ip">.</nts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T284" id="Seg_1245" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_1247" n="HIAT:w" s="T280">Udam</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1250" n="HIAT:w" s="T281">dʼükəmnie</ts>
                  <nts id="Seg_1251" n="HIAT:ip">,</nts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1254" n="HIAT:w" s="T282">nada</ts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1257" n="HIAT:w" s="T283">kădəsʼtə</ts>
                  <nts id="Seg_1258" n="HIAT:ip">.</nts>
                  <nts id="Seg_1259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T290" id="Seg_1261" n="HIAT:u" s="T284">
                  <ts e="T285" id="Seg_1263" n="HIAT:w" s="T284">Tăn</ts>
                  <nts id="Seg_1264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1266" n="HIAT:w" s="T285">ugaːndə</ts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1268" n="HIAT:ip">(</nts>
                  <ts e="T287" id="Seg_1270" n="HIAT:w" s="T286">š</ts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1273" n="HIAT:w" s="T287">-</ts>
                  <nts id="Seg_1274" n="HIAT:ip">)</nts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1277" n="HIAT:w" s="T288">šĭkel</ts>
                  <nts id="Seg_1278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1280" n="HIAT:w" s="T289">numo</ts>
                  <nts id="Seg_1281" n="HIAT:ip">.</nts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T292" id="Seg_1284" n="HIAT:u" s="T290">
                  <ts e="T291" id="Seg_1286" n="HIAT:w" s="T290">Kudonzlial</ts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1289" n="HIAT:w" s="T291">tăn</ts>
                  <nts id="Seg_1290" n="HIAT:ip">.</nts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T296" id="Seg_1293" n="HIAT:u" s="T292">
                  <ts e="T293" id="Seg_1295" n="HIAT:w" s="T292">Tăn</ts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1298" n="HIAT:w" s="T293">tĭmel</ts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1301" n="HIAT:w" s="T294">ugaːndə</ts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1304" n="HIAT:w" s="T295">numo</ts>
                  <nts id="Seg_1305" n="HIAT:ip">.</nts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_1308" n="HIAT:u" s="T296">
                  <ts e="T297" id="Seg_1310" n="HIAT:w" s="T296">Püjel</ts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1312" n="HIAT:ip">(</nts>
                  <ts e="T298" id="Seg_1314" n="HIAT:w" s="T297">nuŋo</ts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1317" n="HIAT:w" s="T298">-</ts>
                  <nts id="Seg_1318" n="HIAT:ip">)</nts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1321" n="HIAT:w" s="T299">numo</ts>
                  <nts id="Seg_1322" n="HIAT:ip">.</nts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303" id="Seg_1325" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_1327" n="HIAT:w" s="T300">Kuʔlə</ts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1330" n="HIAT:w" s="T301">bar</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1333" n="HIAT:w" s="T302">mʼaŋŋaʔbə</ts>
                  <nts id="Seg_1334" n="HIAT:ip">.</nts>
                  <nts id="Seg_1335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T308" id="Seg_1337" n="HIAT:u" s="T303">
                  <nts id="Seg_1338" n="HIAT:ip">(</nts>
                  <ts e="T304" id="Seg_1340" n="HIAT:w" s="T303">Monzaŋbə</ts>
                  <nts id="Seg_1341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1343" n="HIAT:w" s="T304">guan</ts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1346" n="HIAT:w" s="T305">-</ts>
                  <nts id="Seg_1347" n="HIAT:ip">)</nts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1350" n="HIAT:w" s="T306">Monzaŋdə</ts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1353" n="HIAT:w" s="T307">nʼešpək</ts>
                  <nts id="Seg_1354" n="HIAT:ip">.</nts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T465" id="Seg_1356" n="sc" s="T309">
               <ts e="T310" id="Seg_1358" n="HIAT:u" s="T309">
                  <ts e="T310" id="Seg_1360" n="HIAT:w" s="T309">Ну-ну-ну</ts>
                  <nts id="Seg_1361" n="HIAT:ip">.</nts>
                  <nts id="Seg_1362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T313" id="Seg_1364" n="HIAT:u" s="T310">
                  <ts e="T311" id="Seg_1366" n="HIAT:w" s="T310">Ugaːndə</ts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1369" n="HIAT:w" s="T311">eʔbdəl</ts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1372" n="HIAT:w" s="T312">numo</ts>
                  <nts id="Seg_1373" n="HIAT:ip">.</nts>
                  <nts id="Seg_1374" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T316" id="Seg_1376" n="HIAT:u" s="T313">
                  <ts e="T314" id="Seg_1378" n="HIAT:w" s="T313">Ugaːndə</ts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1381" n="HIAT:w" s="T314">kuzaŋdə</ts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1384" n="HIAT:w" s="T315">numo</ts>
                  <nts id="Seg_1385" n="HIAT:ip">.</nts>
                  <nts id="Seg_1386" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T318" id="Seg_1388" n="HIAT:u" s="T316">
                  <ts e="T317" id="Seg_1390" n="HIAT:w" s="T316">Sʼimal</ts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1393" n="HIAT:w" s="T317">urgo</ts>
                  <nts id="Seg_1394" n="HIAT:ip">.</nts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T321" id="Seg_1397" n="HIAT:u" s="T318">
                  <ts e="T319" id="Seg_1399" n="HIAT:w" s="T318">Ulul</ts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1402" n="HIAT:w" s="T319">ugaːndə</ts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1405" n="HIAT:w" s="T320">urgo</ts>
                  <nts id="Seg_1406" n="HIAT:ip">.</nts>
                  <nts id="Seg_1407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T325" id="Seg_1409" n="HIAT:u" s="T321">
                  <nts id="Seg_1410" n="HIAT:ip">(</nts>
                  <ts e="T322" id="Seg_1412" n="HIAT:w" s="T321">Uda</ts>
                  <nts id="Seg_1413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1415" n="HIAT:w" s="T322">-</ts>
                  <nts id="Seg_1416" n="HIAT:ip">)</nts>
                  <nts id="Seg_1417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1419" n="HIAT:w" s="T323">Udazaŋdə</ts>
                  <nts id="Seg_1420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1422" n="HIAT:w" s="T324">numo</ts>
                  <nts id="Seg_1423" n="HIAT:ip">.</nts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T329" id="Seg_1426" n="HIAT:u" s="T325">
                  <nts id="Seg_1427" n="HIAT:ip">(</nts>
                  <ts e="T326" id="Seg_1429" n="HIAT:w" s="T325">Uju</ts>
                  <nts id="Seg_1430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1432" n="HIAT:w" s="T326">-</ts>
                  <nts id="Seg_1433" n="HIAT:ip">)</nts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1436" n="HIAT:w" s="T327">Ujuzaŋdə</ts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1439" n="HIAT:w" s="T328">numo</ts>
                  <nts id="Seg_1440" n="HIAT:ip">.</nts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T332" id="Seg_1443" n="HIAT:u" s="T329">
                  <ts e="T330" id="Seg_1445" n="HIAT:w" s="T329">Bostə</ts>
                  <nts id="Seg_1446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1448" n="HIAT:w" s="T330">bar</ts>
                  <nts id="Seg_1449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1451" n="HIAT:w" s="T331">todam</ts>
                  <nts id="Seg_1452" n="HIAT:ip">.</nts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T334" id="Seg_1455" n="HIAT:u" s="T332">
                  <ts e="T333" id="Seg_1457" n="HIAT:w" s="T332">Müjö</ts>
                  <nts id="Seg_1458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1460" n="HIAT:w" s="T333">nʼergölaʔbə</ts>
                  <nts id="Seg_1461" n="HIAT:ip">.</nts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T336" id="Seg_1464" n="HIAT:u" s="T334">
                  <ts e="T335" id="Seg_1466" n="HIAT:w" s="T334">Panə</ts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1469" n="HIAT:w" s="T335">amnobi</ts>
                  <nts id="Seg_1470" n="HIAT:ip">.</nts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T343" id="Seg_1473" n="HIAT:u" s="T336">
                  <ts e="T337" id="Seg_1475" n="HIAT:w" s="T336">Iʔgö</ts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1477" n="HIAT:ip">(</nts>
                  <ts e="T338" id="Seg_1479" n="HIAT:w" s="T337">nüjö</ts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1482" n="HIAT:w" s="T338">-</ts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1485" n="HIAT:w" s="T339">nu</ts>
                  <nts id="Seg_1486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1488" n="HIAT:w" s="T340">-</ts>
                  <nts id="Seg_1489" n="HIAT:ip">)</nts>
                  <nts id="Seg_1490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1492" n="HIAT:w" s="T341">süjöʔi</ts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1495" n="HIAT:w" s="T342">nʼergölaʔbəʔjə</ts>
                  <nts id="Seg_1496" n="HIAT:ip">.</nts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T348" id="Seg_1499" n="HIAT:u" s="T343">
                  <ts e="T344" id="Seg_1501" n="HIAT:w" s="T343">Karatʼgaj</ts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1504" n="HIAT:w" s="T344">bar</ts>
                  <nts id="Seg_1505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1506" n="HIAT:ip">(</nts>
                  <ts e="T346" id="Seg_1508" n="HIAT:w" s="T345">nʼiʔgə</ts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1511" n="HIAT:w" s="T346">-</ts>
                  <nts id="Seg_1512" n="HIAT:ip">)</nts>
                  <nts id="Seg_1513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1515" n="HIAT:w" s="T347">nʼilgölaʔbə</ts>
                  <nts id="Seg_1516" n="HIAT:ip">.</nts>
                  <nts id="Seg_1517" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T353" id="Seg_1519" n="HIAT:u" s="T348">
                  <ts e="T349" id="Seg_1521" n="HIAT:w" s="T348">Pogəraš</ts>
                  <nts id="Seg_1522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1524" n="HIAT:w" s="T349">nʼilgölaʔ</ts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1526" n="HIAT:ip">(</nts>
                  <ts e="T351" id="Seg_1528" n="HIAT:w" s="T350">s</ts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1531" n="HIAT:w" s="T351">-</ts>
                  <nts id="Seg_1532" n="HIAT:ip">)</nts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1535" n="HIAT:w" s="T352">šobi</ts>
                  <nts id="Seg_1536" n="HIAT:ip">.</nts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T358" id="Seg_1539" n="HIAT:u" s="T353">
                  <ts e="T354" id="Seg_1541" n="HIAT:w" s="T353">Kuriza</ts>
                  <nts id="Seg_1542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1544" n="HIAT:w" s="T354">bar</ts>
                  <nts id="Seg_1545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1547" n="HIAT:w" s="T355">kadəʔlia</ts>
                  <nts id="Seg_1548" n="HIAT:ip">,</nts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1551" n="HIAT:w" s="T356">amzittə</ts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1554" n="HIAT:w" s="T357">măndərlia</ts>
                  <nts id="Seg_1555" n="HIAT:ip">.</nts>
                  <nts id="Seg_1556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T365" id="Seg_1558" n="HIAT:u" s="T358">
                  <ts e="T359" id="Seg_1560" n="HIAT:w" s="T358">Nöməlbiom</ts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1563" n="HIAT:w" s="T359">bar</ts>
                  <nts id="Seg_1564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1565" n="HIAT:ip">(</nts>
                  <ts e="T361" id="Seg_1567" n="HIAT:w" s="T360">budə</ts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1570" n="HIAT:w" s="T361">-</ts>
                  <nts id="Seg_1571" n="HIAT:ip">)</nts>
                  <nts id="Seg_1572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1574" n="HIAT:w" s="T362">büm</ts>
                  <nts id="Seg_1575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1577" n="HIAT:w" s="T363">nuldəsʼtə</ts>
                  <nts id="Seg_1578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1580" n="HIAT:w" s="T364">pʼeːšdə</ts>
                  <nts id="Seg_1581" n="HIAT:ip">.</nts>
                  <nts id="Seg_1582" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T372" id="Seg_1584" n="HIAT:u" s="T365">
                  <ts e="T366" id="Seg_1586" n="HIAT:w" s="T365">Dʼübige</ts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1589" n="HIAT:w" s="T366">bü</ts>
                  <nts id="Seg_1590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1592" n="HIAT:w" s="T367">naga</ts>
                  <nts id="Seg_1593" n="HIAT:ip">,</nts>
                  <nts id="Seg_1594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1595" n="HIAT:ip">(</nts>
                  <ts e="T369" id="Seg_1597" n="HIAT:w" s="T368">na</ts>
                  <nts id="Seg_1598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1600" n="HIAT:w" s="T369">-</ts>
                  <nts id="Seg_1601" n="HIAT:ip">)</nts>
                  <nts id="Seg_1602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1604" n="HIAT:w" s="T370">măna</ts>
                  <nts id="Seg_1605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1607" n="HIAT:w" s="T371">kereʔ</ts>
                  <nts id="Seg_1608" n="HIAT:ip">.</nts>
                  <nts id="Seg_1609" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_1611" n="HIAT:u" s="T372">
                  <ts e="T373" id="Seg_1613" n="HIAT:w" s="T372">Oroma</ts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1615" n="HIAT:ip">(</nts>
                  <ts e="T374" id="Seg_1617" n="HIAT:w" s="T373">kămn</ts>
                  <nts id="Seg_1618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1620" n="HIAT:w" s="T374">-</ts>
                  <nts id="Seg_1621" n="HIAT:ip">)</nts>
                  <nts id="Seg_1622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1624" n="HIAT:w" s="T375">kămnəlim</ts>
                  <nts id="Seg_1625" n="HIAT:ip">.</nts>
                  <nts id="Seg_1626" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T377" id="Seg_1628" n="HIAT:u" s="T376">
                  <ts e="T377" id="Seg_1630" n="HIAT:w" s="T376">Bargarim</ts>
                  <nts id="Seg_1631" n="HIAT:ip">.</nts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_1634" n="HIAT:u" s="T377">
                  <ts e="T378" id="Seg_1636" n="HIAT:w" s="T377">Dĭgəttə</ts>
                  <nts id="Seg_1637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1639" n="HIAT:w" s="T378">bazəsʼtə</ts>
                  <nts id="Seg_1640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1642" n="HIAT:w" s="T379">nada</ts>
                  <nts id="Seg_1643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1645" n="HIAT:w" s="T380">takšeʔi</ts>
                  <nts id="Seg_1646" n="HIAT:ip">.</nts>
                  <nts id="Seg_1647" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T389" id="Seg_1649" n="HIAT:u" s="T381">
                  <ts e="T382" id="Seg_1651" n="HIAT:w" s="T381">Sĭre</ts>
                  <nts id="Seg_1652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1654" n="HIAT:w" s="T382">pa</ts>
                  <nts id="Seg_1655" n="HIAT:ip">,</nts>
                  <nts id="Seg_1656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1658" n="HIAT:w" s="T383">šomne</ts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1661" n="HIAT:w" s="T384">pa</ts>
                  <nts id="Seg_1662" n="HIAT:ip">,</nts>
                  <nts id="Seg_1663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1665" n="HIAT:w" s="T385">komu</ts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1668" n="HIAT:w" s="T386">pa</ts>
                  <nts id="Seg_1669" n="HIAT:ip">,</nts>
                  <nts id="Seg_1670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1672" n="HIAT:w" s="T387">san</ts>
                  <nts id="Seg_1673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1675" n="HIAT:w" s="T388">pa</ts>
                  <nts id="Seg_1676" n="HIAT:ip">.</nts>
                  <nts id="Seg_1677" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T394" id="Seg_1679" n="HIAT:u" s="T389">
                  <nts id="Seg_1680" n="HIAT:ip">(</nts>
                  <ts e="T390" id="Seg_1682" n="HIAT:w" s="T389">Sagən-</ts>
                  <nts id="Seg_1683" n="HIAT:ip">)</nts>
                  <nts id="Seg_1684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1686" n="HIAT:w" s="T390">pagən</ts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1689" n="HIAT:w" s="T391">bar</ts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1692" n="HIAT:w" s="T392">šiškaʔi</ts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1695" n="HIAT:w" s="T393">özerleʔbəʔjə</ts>
                  <nts id="Seg_1696" n="HIAT:ip">.</nts>
                  <nts id="Seg_1697" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T401" id="Seg_1699" n="HIAT:u" s="T394">
                  <ts e="T395" id="Seg_1701" n="HIAT:w" s="T394">San</ts>
                  <nts id="Seg_1702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1703" n="HIAT:ip">(</nts>
                  <ts e="T396" id="Seg_1705" n="HIAT:w" s="T395">pag</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1708" n="HIAT:w" s="T396">-</ts>
                  <nts id="Seg_1709" n="HIAT:ip">)</nts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1712" n="HIAT:w" s="T397">pagən</ts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1715" n="HIAT:w" s="T398">bar</ts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1718" n="HIAT:w" s="T399">šiškaʔi</ts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1721" n="HIAT:w" s="T400">amnolaʔbəʔjə</ts>
                  <nts id="Seg_1722" n="HIAT:ip">.</nts>
                  <nts id="Seg_1723" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T406" id="Seg_1725" n="HIAT:u" s="T401">
                  <ts e="T402" id="Seg_1727" n="HIAT:w" s="T401">Sĭre</ts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1730" n="HIAT:w" s="T402">pagən</ts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1733" n="HIAT:w" s="T403">bar</ts>
                  <nts id="Seg_1734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1736" n="HIAT:w" s="T404">takšə</ts>
                  <nts id="Seg_1737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1739" n="HIAT:w" s="T405">alaʔbəʔjə</ts>
                  <nts id="Seg_1740" n="HIAT:ip">.</nts>
                  <nts id="Seg_1741" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T409" id="Seg_1743" n="HIAT:u" s="T406">
                  <ts e="T407" id="Seg_1745" n="HIAT:w" s="T406">Segi</ts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1748" n="HIAT:w" s="T407">bü</ts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1751" n="HIAT:w" s="T408">bĭtleʔbəʔjə</ts>
                  <nts id="Seg_1752" n="HIAT:ip">.</nts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T412" id="Seg_1755" n="HIAT:u" s="T409">
                  <nts id="Seg_1756" n="HIAT:ip">(</nts>
                  <ts e="T410" id="Seg_1758" n="HIAT:w" s="T409">Paj</ts>
                  <nts id="Seg_1759" n="HIAT:ip">)</nts>
                  <nts id="Seg_1760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1762" n="HIAT:w" s="T410">Paj</ts>
                  <nts id="Seg_1763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1765" n="HIAT:w" s="T411">šamnak</ts>
                  <nts id="Seg_1766" n="HIAT:ip">.</nts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T414" id="Seg_1769" n="HIAT:u" s="T412">
                  <ts e="T413" id="Seg_1771" n="HIAT:w" s="T412">Bazaj</ts>
                  <nts id="Seg_1772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1774" n="HIAT:w" s="T413">šamnak</ts>
                  <nts id="Seg_1775" n="HIAT:ip">.</nts>
                  <nts id="Seg_1776" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T416" id="Seg_1778" n="HIAT:u" s="T414">
                  <ts e="T415" id="Seg_1780" n="HIAT:w" s="T414">Takšə</ts>
                  <nts id="Seg_1781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1783" n="HIAT:w" s="T415">bazaj</ts>
                  <nts id="Seg_1784" n="HIAT:ip">.</nts>
                  <nts id="Seg_1785" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T422" id="Seg_1787" n="HIAT:u" s="T416">
                  <ts e="T417" id="Seg_1789" n="HIAT:w" s="T416">Ugaːndə</ts>
                  <nts id="Seg_1790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1792" n="HIAT:w" s="T417">paʔi</ts>
                  <nts id="Seg_1793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1795" n="HIAT:w" s="T418">iʔgö</ts>
                  <nts id="Seg_1796" n="HIAT:ip">,</nts>
                  <nts id="Seg_1797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1799" n="HIAT:w" s="T419">dĭzeŋ</ts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1802" n="HIAT:w" s="T420">nada</ts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1805" n="HIAT:w" s="T421">jaʔsʼittə</ts>
                  <nts id="Seg_1806" n="HIAT:ip">.</nts>
                  <nts id="Seg_1807" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T424" id="Seg_1809" n="HIAT:u" s="T422">
                  <ts e="T423" id="Seg_1811" n="HIAT:w" s="T422">Dĭgəttə</ts>
                  <nts id="Seg_1812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1813" n="HIAT:ip">(</nts>
                  <ts e="T424" id="Seg_1815" n="HIAT:w" s="T423">kürzittə</ts>
                  <nts id="Seg_1816" n="HIAT:ip">)</nts>
                  <nts id="Seg_1817" n="HIAT:ip">.</nts>
                  <nts id="Seg_1818" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T427" id="Seg_1820" n="HIAT:u" s="T424">
                  <ts e="T425" id="Seg_1822" n="HIAT:w" s="T424">Dĭgəttə</ts>
                  <nts id="Seg_1823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1825" n="HIAT:w" s="T425">tura</ts>
                  <nts id="Seg_1826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1827" n="HIAT:ip">(</nts>
                  <ts e="T427" id="Seg_1829" n="HIAT:w" s="T426">kazittə</ts>
                  <nts id="Seg_1830" n="HIAT:ip">)</nts>
                  <nts id="Seg_1831" n="HIAT:ip">.</nts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T433" id="Seg_1834" n="HIAT:u" s="T427">
                  <ts e="T428" id="Seg_1836" n="HIAT:w" s="T427">Dĭgəttə</ts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1839" n="HIAT:w" s="T428">dĭ</ts>
                  <nts id="Seg_1840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1842" n="HIAT:w" s="T429">paʔizʼiʔ</ts>
                  <nts id="Seg_1843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1845" n="HIAT:w" s="T430">tura</ts>
                  <nts id="Seg_1846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1848" n="HIAT:w" s="T431">jaʔsʼittə</ts>
                  <nts id="Seg_1849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1851" n="HIAT:w" s="T432">nada</ts>
                  <nts id="Seg_1852" n="HIAT:ip">.</nts>
                  <nts id="Seg_1853" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T435" id="Seg_1855" n="HIAT:u" s="T433">
                  <ts e="T434" id="Seg_1857" n="HIAT:w" s="T433">Amnozʼittə</ts>
                  <nts id="Seg_1858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1860" n="HIAT:w" s="T434">turagən</ts>
                  <nts id="Seg_1861" n="HIAT:ip">.</nts>
                  <nts id="Seg_1862" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T437" id="Seg_1864" n="HIAT:u" s="T435">
                  <ts e="T436" id="Seg_1866" n="HIAT:w" s="T435">Pʼeːš</ts>
                  <nts id="Seg_1867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1869" n="HIAT:w" s="T436">enzittə</ts>
                  <nts id="Seg_1870" n="HIAT:ip">.</nts>
                  <nts id="Seg_1871" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T440" id="Seg_1873" n="HIAT:u" s="T437">
                  <ts e="T438" id="Seg_1875" n="HIAT:w" s="T437">Pʼeːšgən</ts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1878" n="HIAT:w" s="T438">pürzittə</ts>
                  <nts id="Seg_1879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1881" n="HIAT:w" s="T439">ipek</ts>
                  <nts id="Seg_1882" n="HIAT:ip">.</nts>
                  <nts id="Seg_1883" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T442" id="Seg_1885" n="HIAT:u" s="T440">
                  <ts e="T441" id="Seg_1887" n="HIAT:w" s="T440">Bĭdeʔ</ts>
                  <nts id="Seg_1888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1890" n="HIAT:w" s="T441">bü</ts>
                  <nts id="Seg_1891" n="HIAT:ip">!</nts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T452" id="Seg_1894" n="HIAT:u" s="T442">
                  <ts e="T443" id="Seg_1896" n="HIAT:w" s="T442">Măn</ts>
                  <nts id="Seg_1897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1899" n="HIAT:w" s="T443">dibər</ts>
                  <nts id="Seg_1900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1901" n="HIAT:ip">(</nts>
                  <ts e="T445" id="Seg_1903" n="HIAT:w" s="T444">kăm</ts>
                  <nts id="Seg_1904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1906" n="HIAT:w" s="T445">-</ts>
                  <nts id="Seg_1907" n="HIAT:ip">)</nts>
                  <nts id="Seg_1908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1910" n="HIAT:w" s="T446">embiem</ts>
                  <nts id="Seg_1911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1913" n="HIAT:w" s="T447">sĭreʔpne</ts>
                  <nts id="Seg_1914" n="HIAT:ip">,</nts>
                  <nts id="Seg_1915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1916" n="HIAT:ip">(</nts>
                  <ts e="T449" id="Seg_1918" n="HIAT:w" s="T448">bü</ts>
                  <nts id="Seg_1919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1921" n="HIAT:w" s="T449">-</ts>
                  <nts id="Seg_1922" n="HIAT:ip">)</nts>
                  <nts id="Seg_1923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1925" n="HIAT:w" s="T450">bü</ts>
                  <nts id="Seg_1926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1928" n="HIAT:w" s="T451">kămnabiam</ts>
                  <nts id="Seg_1929" n="HIAT:ip">.</nts>
                  <nts id="Seg_1930" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T456" id="Seg_1932" n="HIAT:u" s="T452">
                  <nts id="Seg_1933" n="HIAT:ip">(</nts>
                  <ts e="T453" id="Seg_1935" n="HIAT:w" s="T452">Na</ts>
                  <nts id="Seg_1936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1938" n="HIAT:w" s="T453">-</ts>
                  <nts id="Seg_1939" n="HIAT:ip">)</nts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1942" n="HIAT:w" s="T454">Namzəbi</ts>
                  <nts id="Seg_1943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1945" n="HIAT:w" s="T455">bar</ts>
                  <nts id="Seg_1946" n="HIAT:ip">.</nts>
                  <nts id="Seg_1947" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T458" id="Seg_1949" n="HIAT:u" s="T456">
                  <ts e="T457" id="Seg_1951" n="HIAT:w" s="T456">Ipek</ts>
                  <nts id="Seg_1952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1954" n="HIAT:w" s="T457">endəbiem</ts>
                  <nts id="Seg_1955" n="HIAT:ip">.</nts>
                  <nts id="Seg_1956" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T461" id="Seg_1958" n="HIAT:u" s="T458">
                  <ts e="T459" id="Seg_1960" n="HIAT:w" s="T458">Bĭdeʔ</ts>
                  <nts id="Seg_1961" n="HIAT:ip">,</nts>
                  <nts id="Seg_1962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1964" n="HIAT:w" s="T459">ugaːndə</ts>
                  <nts id="Seg_1965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1967" n="HIAT:w" s="T460">jakšə</ts>
                  <nts id="Seg_1968" n="HIAT:ip">.</nts>
                  <nts id="Seg_1969" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T465" id="Seg_1971" n="HIAT:u" s="T461">
                  <ts e="T462" id="Seg_1973" n="HIAT:w" s="T461">Šiʔ</ts>
                  <nts id="Seg_1974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1976" n="HIAT:w" s="T462">taldʼen</ts>
                  <nts id="Seg_1977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1979" n="HIAT:w" s="T463">ara</ts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1982" n="HIAT:w" s="T464">bĭʔpileʔ</ts>
                  <nts id="Seg_1983" n="HIAT:ip">.</nts>
                  <nts id="Seg_1984" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T470" id="Seg_1985" n="sc" s="T467">
               <ts e="T470" id="Seg_1987" n="HIAT:u" s="T467">
                  <ts e="T468" id="Seg_1989" n="HIAT:w" s="T467">Jezerik</ts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1992" n="HIAT:w" s="T468">ibileʔ</ts>
                  <nts id="Seg_1993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1995" n="HIAT:w" s="T469">bar</ts>
                  <nts id="Seg_1996" n="HIAT:ip">?</nts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T486" id="Seg_1998" n="sc" s="T472">
               <ts e="T477" id="Seg_2000" n="HIAT:u" s="T472">
                  <ts e="T474" id="Seg_2002" n="HIAT:w" s="T472">Teinen</ts>
                  <nts id="Seg_2003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_2005" n="HIAT:w" s="T474">bar</ts>
                  <nts id="Seg_2006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_2008" n="HIAT:w" s="T475">в</ts>
                  <nts id="Seg_2009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_2011" n="HIAT:w" s="T476">похмелье</ts>
                  <nts id="Seg_2012" n="HIAT:ip">.</nts>
                  <nts id="Seg_2013" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T480" id="Seg_2015" n="HIAT:u" s="T477">
                  <ts e="T478" id="Seg_2017" n="HIAT:w" s="T477">Bĭdeʔ</ts>
                  <nts id="Seg_2018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_2020" n="HIAT:w" s="T478">komu</ts>
                  <nts id="Seg_2021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_2023" n="HIAT:w" s="T479">bü</ts>
                  <nts id="Seg_2024" n="HIAT:ip">.</nts>
                  <nts id="Seg_2025" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T486" id="Seg_2027" n="HIAT:u" s="T480">
                  <ts e="T481" id="Seg_2029" n="HIAT:w" s="T480">Măn</ts>
                  <nts id="Seg_2030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_2032" n="HIAT:w" s="T481">dibər</ts>
                  <nts id="Seg_2033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2034" n="HIAT:ip">(</nts>
                  <ts e="T483" id="Seg_2036" n="HIAT:w" s="T482">sin</ts>
                  <nts id="Seg_2037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_2039" n="HIAT:w" s="T483">-</ts>
                  <nts id="Seg_2040" n="HIAT:ip">)</nts>
                  <nts id="Seg_2041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_2043" n="HIAT:w" s="T484">sĭreʔpne</ts>
                  <nts id="Seg_2044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_2046" n="HIAT:w" s="T485">embiem</ts>
                  <nts id="Seg_2047" n="HIAT:ip">.</nts>
                  <nts id="Seg_2048" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T690" id="Seg_2049" n="sc" s="T487">
               <ts e="T489" id="Seg_2051" n="HIAT:u" s="T487">
                  <ts e="T488" id="Seg_2053" n="HIAT:w" s="T487">Bü</ts>
                  <nts id="Seg_2054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_2056" n="HIAT:w" s="T488">kămnəbiam</ts>
                  <nts id="Seg_2057" n="HIAT:ip">.</nts>
                  <nts id="Seg_2058" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T493" id="Seg_2060" n="HIAT:u" s="T489">
                  <nts id="Seg_2061" n="HIAT:ip">(</nts>
                  <ts e="T490" id="Seg_2063" n="HIAT:w" s="T489">Namzə</ts>
                  <nts id="Seg_2064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_2066" n="HIAT:w" s="T490">-</ts>
                  <nts id="Seg_2067" n="HIAT:ip">)</nts>
                  <nts id="Seg_2068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_2070" n="HIAT:w" s="T491">Namzəga</ts>
                  <nts id="Seg_2071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_2073" n="HIAT:w" s="T492">molaːmbi</ts>
                  <nts id="Seg_2074" n="HIAT:ip">.</nts>
                  <nts id="Seg_2075" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T496" id="Seg_2077" n="HIAT:u" s="T493">
                  <ts e="T494" id="Seg_2079" n="HIAT:w" s="T493">Jakšə</ts>
                  <nts id="Seg_2080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_2082" n="HIAT:w" s="T494">moləj</ts>
                  <nts id="Seg_2083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_2085" n="HIAT:w" s="T495">tănan</ts>
                  <nts id="Seg_2086" n="HIAT:ip">.</nts>
                  <nts id="Seg_2087" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T502" id="Seg_2089" n="HIAT:u" s="T496">
                  <ts e="T497" id="Seg_2091" n="HIAT:w" s="T496">Ara</ts>
                  <nts id="Seg_2092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_2094" n="HIAT:w" s="T497">iʔ</ts>
                  <nts id="Seg_2095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_2097" n="HIAT:w" s="T498">bĭdeʔ</ts>
                  <nts id="Seg_2098" n="HIAT:ip">,</nts>
                  <nts id="Seg_2099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499.tx-PKZ.1" id="Seg_2101" n="HIAT:w" s="T499">a</ts>
                  <nts id="Seg_2102" n="HIAT:ip">_</nts>
                  <ts e="T500" id="Seg_2104" n="HIAT:w" s="T499.tx-PKZ.1">to</ts>
                  <nts id="Seg_2105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_2107" n="HIAT:w" s="T500">ulul</ts>
                  <nts id="Seg_2108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_2110" n="HIAT:w" s="T501">ĭzemnie</ts>
                  <nts id="Seg_2111" n="HIAT:ip">.</nts>
                  <nts id="Seg_2112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T504" id="Seg_2114" n="HIAT:u" s="T502">
                  <ts e="T503" id="Seg_2116" n="HIAT:w" s="T502">Sĭjdə</ts>
                  <nts id="Seg_2117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_2119" n="HIAT:w" s="T503">ĭzemnie</ts>
                  <nts id="Seg_2120" n="HIAT:ip">.</nts>
                  <nts id="Seg_2121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T507" id="Seg_2123" n="HIAT:u" s="T504">
                  <ts e="T505" id="Seg_2125" n="HIAT:w" s="T504">Bĭtlel</ts>
                  <nts id="Seg_2126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_2128" n="HIAT:w" s="T505">dak</ts>
                  <nts id="Seg_2129" n="HIAT:ip">,</nts>
                  <nts id="Seg_2130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_2132" n="HIAT:w" s="T506">külalləl</ts>
                  <nts id="Seg_2133" n="HIAT:ip">.</nts>
                  <nts id="Seg_2134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T513" id="Seg_2136" n="HIAT:u" s="T507">
                  <ts e="T508" id="Seg_2138" n="HIAT:w" s="T507">A</ts>
                  <nts id="Seg_2139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_2141" n="HIAT:w" s="T508">ej</ts>
                  <nts id="Seg_2142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_2144" n="HIAT:w" s="T509">bĭtlel</ts>
                  <nts id="Seg_2145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_2147" n="HIAT:w" s="T510">dak</ts>
                  <nts id="Seg_2148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_2150" n="HIAT:w" s="T511">kondʼo</ts>
                  <nts id="Seg_2151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_2153" n="HIAT:w" s="T512">amnolal</ts>
                  <nts id="Seg_2154" n="HIAT:ip">.</nts>
                  <nts id="Seg_2155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T519" id="Seg_2157" n="HIAT:u" s="T513">
                  <ts e="T514" id="Seg_2159" n="HIAT:w" s="T513">Mašina</ts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2161" n="HIAT:ip">(</nts>
                  <ts e="T515" id="Seg_2163" n="HIAT:w" s="T514">bɨl</ts>
                  <nts id="Seg_2164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_2166" n="HIAT:w" s="T515">-</ts>
                  <nts id="Seg_2167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_2169" n="HIAT:w" s="T516">tojir</ts>
                  <nts id="Seg_2170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_2172" n="HIAT:w" s="T517">-</ts>
                  <nts id="Seg_2173" n="HIAT:ip">)</nts>
                  <nts id="Seg_2174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2175" n="HIAT:ip">(</nts>
                  <ts e="T519" id="Seg_2177" n="HIAT:w" s="T518">talerlaʔbə</ts>
                  <nts id="Seg_2178" n="HIAT:ip">)</nts>
                  <nts id="Seg_2179" n="HIAT:ip">.</nts>
                  <nts id="Seg_2180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T522" id="Seg_2182" n="HIAT:u" s="T519">
                  <ts e="T520" id="Seg_2184" n="HIAT:w" s="T519">Dĭn</ts>
                  <nts id="Seg_2185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_2187" n="HIAT:w" s="T520">kuza</ts>
                  <nts id="Seg_2188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_2190" n="HIAT:w" s="T521">amnolaʔbə</ts>
                  <nts id="Seg_2191" n="HIAT:ip">.</nts>
                  <nts id="Seg_2192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T526" id="Seg_2194" n="HIAT:u" s="T522">
                  <ts e="T523" id="Seg_2196" n="HIAT:w" s="T522">Mašinagəʔ</ts>
                  <nts id="Seg_2197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_2199" n="HIAT:w" s="T523">bar</ts>
                  <nts id="Seg_2200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_2202" n="HIAT:w" s="T524">bər</ts>
                  <nts id="Seg_2203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_2205" n="HIAT:w" s="T525">šonəga</ts>
                  <nts id="Seg_2206" n="HIAT:ip">.</nts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T532" id="Seg_2209" n="HIAT:u" s="T526">
                  <ts e="T527" id="Seg_2211" n="HIAT:w" s="T526">Dĭ</ts>
                  <nts id="Seg_2212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_2214" n="HIAT:w" s="T527">bar</ts>
                  <nts id="Seg_2215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_2217" n="HIAT:w" s="T528">dibər</ts>
                  <nts id="Seg_2218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2219" n="HIAT:ip">(</nts>
                  <ts e="T530" id="Seg_2221" n="HIAT:w" s="T529">šo-</ts>
                  <nts id="Seg_2222" n="HIAT:ip">)</nts>
                  <nts id="Seg_2223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_2225" n="HIAT:w" s="T530">döbər</ts>
                  <nts id="Seg_2226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_2228" n="HIAT:w" s="T531">kallaʔbə</ts>
                  <nts id="Seg_2229" n="HIAT:ip">.</nts>
                  <nts id="Seg_2230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T536" id="Seg_2232" n="HIAT:u" s="T532">
                  <ts e="T533" id="Seg_2234" n="HIAT:w" s="T532">Abam</ts>
                  <nts id="Seg_2235" n="HIAT:ip">,</nts>
                  <nts id="Seg_2236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_2238" n="HIAT:w" s="T533">ijam</ts>
                  <nts id="Seg_2239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_2241" n="HIAT:w" s="T534">koʔbdo</ts>
                  <nts id="Seg_2242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_2244" n="HIAT:w" s="T535">kubiʔi</ts>
                  <nts id="Seg_2245" n="HIAT:ip">.</nts>
                  <nts id="Seg_2246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T541" id="Seg_2248" n="HIAT:u" s="T536">
                  <nts id="Seg_2249" n="HIAT:ip">(</nts>
                  <ts e="T537" id="Seg_2251" n="HIAT:w" s="T536">Măn</ts>
                  <nts id="Seg_2252" n="HIAT:ip">)</nts>
                  <nts id="Seg_2253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2255" n="HIAT:w" s="T537">Măna</ts>
                  <nts id="Seg_2256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2258" n="HIAT:w" s="T538">mănliaʔi:</ts>
                  <nts id="Seg_2259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_2261" n="HIAT:w" s="T539">kanaʔ</ts>
                  <nts id="Seg_2262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_2264" n="HIAT:w" s="T540">dĭzʼiʔ</ts>
                  <nts id="Seg_2265" n="HIAT:ip">!</nts>
                  <nts id="Seg_2266" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T546" id="Seg_2268" n="HIAT:u" s="T541">
                  <nts id="Seg_2269" n="HIAT:ip">(</nts>
                  <ts e="T542" id="Seg_2271" n="HIAT:w" s="T541">Tĭmne</ts>
                  <nts id="Seg_2272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_2274" n="HIAT:w" s="T542">-</ts>
                  <nts id="Seg_2275" n="HIAT:ip">)</nts>
                  <nts id="Seg_2276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2278" n="HIAT:w" s="T543">Tĭmnit</ts>
                  <nts id="Seg_2279" n="HIAT:ip">,</nts>
                  <nts id="Seg_2280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2282" n="HIAT:w" s="T544">girgit</ts>
                  <nts id="Seg_2283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2285" n="HIAT:w" s="T545">di</ts>
                  <nts id="Seg_2286" n="HIAT:ip">.</nts>
                  <nts id="Seg_2287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T549" id="Seg_2289" n="HIAT:u" s="T546">
                  <ts e="T547" id="Seg_2291" n="HIAT:w" s="T546">Dĭgəttə</ts>
                  <nts id="Seg_2292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2294" n="HIAT:w" s="T547">tibinə</ts>
                  <nts id="Seg_2295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2297" n="HIAT:w" s="T548">iliel</ts>
                  <nts id="Seg_2298" n="HIAT:ip">.</nts>
                  <nts id="Seg_2299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T552" id="Seg_2301" n="HIAT:u" s="T549">
                  <ts e="T550" id="Seg_2303" n="HIAT:w" s="T549">Dĭgəttə</ts>
                  <nts id="Seg_2304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2306" n="HIAT:w" s="T550">esseŋdə</ts>
                  <nts id="Seg_2307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2309" n="HIAT:w" s="T551">moləʔjə</ts>
                  <nts id="Seg_2310" n="HIAT:ip">.</nts>
                  <nts id="Seg_2311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T553" id="Seg_2313" n="HIAT:u" s="T552">
                  <ts e="T553" id="Seg_2315" n="HIAT:w" s="T552">Погоди</ts>
                  <nts id="Seg_2316" n="HIAT:ip">.</nts>
                  <nts id="Seg_2317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T555" id="Seg_2319" n="HIAT:u" s="T553">
                  <ts e="T554" id="Seg_2321" n="HIAT:w" s="T553">Esseŋlə</ts>
                  <nts id="Seg_2322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2324" n="HIAT:w" s="T554">özerleʔjə</ts>
                  <nts id="Seg_2325" n="HIAT:ip">.</nts>
                  <nts id="Seg_2326" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T557" id="Seg_2328" n="HIAT:u" s="T555">
                  <ts e="T556" id="Seg_2330" n="HIAT:w" s="T555">Tănan</ts>
                  <nts id="Seg_2331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2333" n="HIAT:w" s="T556">kabažarlaʔjə</ts>
                  <nts id="Seg_2334" n="HIAT:ip">.</nts>
                  <nts id="Seg_2335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T564" id="Seg_2337" n="HIAT:u" s="T557">
                  <ts e="T558" id="Seg_2339" n="HIAT:w" s="T557">Nem</ts>
                  <nts id="Seg_2340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2341" n="HIAT:ip">(</nts>
                  <ts e="T559" id="Seg_2343" n="HIAT:w" s="T558">ə</ts>
                  <nts id="Seg_2344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2346" n="HIAT:w" s="T559">-</ts>
                  <nts id="Seg_2347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2349" n="HIAT:w" s="T560">i</ts>
                  <nts id="Seg_2350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2352" n="HIAT:w" s="T561">-</ts>
                  <nts id="Seg_2353" n="HIAT:ip">)</nts>
                  <nts id="Seg_2354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2356" n="HIAT:w" s="T562">iʔ</ts>
                  <nts id="Seg_2357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2359" n="HIAT:w" s="T563">münöraʔ</ts>
                  <nts id="Seg_2360" n="HIAT:ip">!</nts>
                  <nts id="Seg_2361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T573" id="Seg_2363" n="HIAT:u" s="T564">
                  <nts id="Seg_2364" n="HIAT:ip">(</nts>
                  <ts e="T565" id="Seg_2366" n="HIAT:w" s="T564">Nʼilgöt</ts>
                  <nts id="Seg_2367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2369" n="HIAT:w" s="T565">ab</ts>
                  <nts id="Seg_2370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2372" n="HIAT:w" s="T566">-</ts>
                  <nts id="Seg_2373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2375" n="HIAT:w" s="T567">abat</ts>
                  <nts id="Seg_2376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2378" n="HIAT:w" s="T568">-</ts>
                  <nts id="Seg_2379" n="HIAT:ip">)</nts>
                  <nts id="Seg_2380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2382" n="HIAT:w" s="T569">Abam</ts>
                  <nts id="Seg_2383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2385" n="HIAT:w" s="T570">da</ts>
                  <nts id="Seg_2386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2388" n="HIAT:w" s="T571">ijam</ts>
                  <nts id="Seg_2389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2391" n="HIAT:w" s="T572">nʼilgöt</ts>
                  <nts id="Seg_2392" n="HIAT:ip">!</nts>
                  <nts id="Seg_2393" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T577" id="Seg_2395" n="HIAT:u" s="T573">
                  <ts e="T573.tx-PKZ.1" id="Seg_2397" n="HIAT:w" s="T573">Все</ts>
                  <nts id="Seg_2398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573.tx-PKZ.2" id="Seg_2400" n="HIAT:w" s="T573.tx-PKZ.1">ли</ts>
                  <nts id="Seg_2401" n="HIAT:ip">,</nts>
                  <nts id="Seg_2402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573.tx-PKZ.3" id="Seg_2404" n="HIAT:w" s="T573.tx-PKZ.2">что</ts>
                  <nts id="Seg_2405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2407" n="HIAT:w" s="T573.tx-PKZ.3">ли</ts>
                  <nts id="Seg_2408" n="HIAT:ip">.</nts>
                  <nts id="Seg_2409" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T581" id="Seg_2411" n="HIAT:u" s="T577">
                  <ts e="T578" id="Seg_2413" n="HIAT:w" s="T577">Teinen</ts>
                  <nts id="Seg_2414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2416" n="HIAT:w" s="T578">bar</ts>
                  <nts id="Seg_2417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2419" n="HIAT:w" s="T579">kalla</ts>
                  <nts id="Seg_2420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2422" n="HIAT:w" s="T580">dʼürbiʔi</ts>
                  <nts id="Seg_2423" n="HIAT:ip">.</nts>
                  <nts id="Seg_2424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T585" id="Seg_2426" n="HIAT:u" s="T581">
                  <ts e="T582" id="Seg_2428" n="HIAT:w" s="T581">Abat</ts>
                  <nts id="Seg_2429" n="HIAT:ip">,</nts>
                  <nts id="Seg_2430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2432" n="HIAT:w" s="T582">ijat</ts>
                  <nts id="Seg_2433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2435" n="HIAT:w" s="T583">kalla</ts>
                  <nts id="Seg_2436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2438" n="HIAT:w" s="T584">dʼürbiʔi</ts>
                  <nts id="Seg_2439" n="HIAT:ip">.</nts>
                  <nts id="Seg_2440" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T588" id="Seg_2442" n="HIAT:u" s="T585">
                  <ts e="T586" id="Seg_2444" n="HIAT:w" s="T585">Esseŋ</ts>
                  <nts id="Seg_2445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2447" n="HIAT:w" s="T586">kalla</ts>
                  <nts id="Seg_2448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2450" n="HIAT:w" s="T587">dʼürbiʔi</ts>
                  <nts id="Seg_2451" n="HIAT:ip">.</nts>
                  <nts id="Seg_2452" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T592" id="Seg_2454" n="HIAT:u" s="T588">
                  <ts e="T589" id="Seg_2456" n="HIAT:w" s="T588">Măn</ts>
                  <nts id="Seg_2457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2459" n="HIAT:w" s="T589">unnʼa</ts>
                  <nts id="Seg_2460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2462" n="HIAT:w" s="T590">maʔnal</ts>
                  <nts id="Seg_2463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2465" n="HIAT:w" s="T591">amnolaʔbəm</ts>
                  <nts id="Seg_2466" n="HIAT:ip">.</nts>
                  <nts id="Seg_2467" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T595" id="Seg_2469" n="HIAT:u" s="T592">
                  <ts e="T593" id="Seg_2471" n="HIAT:w" s="T592">Nada</ts>
                  <nts id="Seg_2472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2474" n="HIAT:w" s="T593">dʼü</ts>
                  <nts id="Seg_2475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2477" n="HIAT:w" s="T594">tĭlzittə</ts>
                  <nts id="Seg_2478" n="HIAT:ip">.</nts>
                  <nts id="Seg_2479" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T597" id="Seg_2481" n="HIAT:u" s="T595">
                  <ts e="T596" id="Seg_2483" n="HIAT:w" s="T595">Kuza</ts>
                  <nts id="Seg_2484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2486" n="HIAT:w" s="T596">külaːmbi</ts>
                  <nts id="Seg_2487" n="HIAT:ip">.</nts>
                  <nts id="Seg_2488" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T601" id="Seg_2490" n="HIAT:u" s="T597">
                  <ts e="T598" id="Seg_2492" n="HIAT:w" s="T597">Nada</ts>
                  <nts id="Seg_2493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2495" n="HIAT:w" s="T598">dĭm</ts>
                  <nts id="Seg_2496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2498" n="HIAT:w" s="T599">dibər</ts>
                  <nts id="Seg_2499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2501" n="HIAT:w" s="T600">enzittə</ts>
                  <nts id="Seg_2502" n="HIAT:ip">.</nts>
                  <nts id="Seg_2503" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T603" id="Seg_2505" n="HIAT:u" s="T601">
                  <ts e="T602" id="Seg_2507" n="HIAT:w" s="T601">Dĭgəttə</ts>
                  <nts id="Seg_2508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2510" n="HIAT:w" s="T602">kămnasʼtə</ts>
                  <nts id="Seg_2511" n="HIAT:ip">.</nts>
                  <nts id="Seg_2512" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T611" id="Seg_2514" n="HIAT:u" s="T603">
                  <nts id="Seg_2515" n="HIAT:ip">(</nts>
                  <ts e="T604" id="Seg_2517" n="HIAT:w" s="T603">Dʼo</ts>
                  <nts id="Seg_2518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2520" n="HIAT:w" s="T604">-</ts>
                  <nts id="Seg_2521" n="HIAT:ip">)</nts>
                  <nts id="Seg_2522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2524" n="HIAT:w" s="T605">dĭgəttə</ts>
                  <nts id="Seg_2525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2526" n="HIAT:ip">(</nts>
                  <ts e="T607" id="Seg_2528" n="HIAT:w" s="T606">tʼu</ts>
                  <nts id="Seg_2529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2531" n="HIAT:w" s="T607">-</ts>
                  <nts id="Seg_2532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2534" n="HIAT:w" s="T608">dʼo</ts>
                  <nts id="Seg_2535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2537" n="HIAT:w" s="T609">-</ts>
                  <nts id="Seg_2538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2540" n="HIAT:w" s="T610">dʼora</ts>
                  <nts id="Seg_2541" n="HIAT:ip">)</nts>
                  <nts id="Seg_2542" n="HIAT:ip">.</nts>
                  <nts id="Seg_2543" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T620" id="Seg_2545" n="HIAT:u" s="T611">
                  <ts e="T613" id="Seg_2547" n="HIAT:w" s="T611">Dĭgəttə</ts>
                  <nts id="Seg_2548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2550" n="HIAT:w" s="T613">Dĭgəttə</ts>
                  <nts id="Seg_2551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2552" n="HIAT:ip">(</nts>
                  <ts e="T615" id="Seg_2554" n="HIAT:w" s="T614">dujzʼ</ts>
                  <nts id="Seg_2555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2557" n="HIAT:w" s="T615">-</ts>
                  <nts id="Seg_2558" n="HIAT:ip">)</nts>
                  <nts id="Seg_2559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2561" n="HIAT:w" s="T616">dʼüzʼiʔ</ts>
                  <nts id="Seg_2562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2564" n="HIAT:w" s="T617">nada</ts>
                  <nts id="Seg_2565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2567" n="HIAT:w" s="T618">kajzittə</ts>
                  <nts id="Seg_2568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2570" n="HIAT:w" s="T619">dĭm</ts>
                  <nts id="Seg_2571" n="HIAT:ip">.</nts>
                  <nts id="Seg_2572" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T630" id="Seg_2574" n="HIAT:u" s="T620">
                  <ts e="T621" id="Seg_2576" n="HIAT:w" s="T620">Dĭgəttə</ts>
                  <nts id="Seg_2577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2579" n="HIAT:w" s="T621">maʔnal</ts>
                  <nts id="Seg_2580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2581" n="HIAT:ip">(</nts>
                  <ts e="T623" id="Seg_2583" n="HIAT:w" s="T622">su</ts>
                  <nts id="Seg_2584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2586" n="HIAT:w" s="T623">-</ts>
                  <nts id="Seg_2587" n="HIAT:ip">)</nts>
                  <nts id="Seg_2588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2590" n="HIAT:w" s="T624">šozittə</ts>
                  <nts id="Seg_2591" n="HIAT:ip">,</nts>
                  <nts id="Seg_2592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2594" n="HIAT:w" s="T625">amzittə</ts>
                  <nts id="Seg_2595" n="HIAT:ip">,</nts>
                  <nts id="Seg_2596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2597" n="HIAT:ip">(</nts>
                  <ts e="T627" id="Seg_2599" n="HIAT:w" s="T626">amo</ts>
                  <nts id="Seg_2600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2602" n="HIAT:w" s="T627">-</ts>
                  <nts id="Seg_2603" n="HIAT:ip">)</nts>
                  <nts id="Seg_2604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2606" n="HIAT:w" s="T628">amnaʔ</ts>
                  <nts id="Seg_2607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2609" n="HIAT:w" s="T629">amzittə</ts>
                  <nts id="Seg_2610" n="HIAT:ip">.</nts>
                  <nts id="Seg_2611" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T632" id="Seg_2613" n="HIAT:u" s="T630">
                  <ts e="T631" id="Seg_2615" n="HIAT:w" s="T630">Kuza</ts>
                  <nts id="Seg_2616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2618" n="HIAT:w" s="T631">ĭzembi</ts>
                  <nts id="Seg_2619" n="HIAT:ip">.</nts>
                  <nts id="Seg_2620" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T634" id="Seg_2622" n="HIAT:u" s="T632">
                  <ts e="T633" id="Seg_2624" n="HIAT:w" s="T632">Šamandə</ts>
                  <nts id="Seg_2625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2627" n="HIAT:w" s="T633">mĭmbi</ts>
                  <nts id="Seg_2628" n="HIAT:ip">.</nts>
                  <nts id="Seg_2629" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T639" id="Seg_2631" n="HIAT:u" s="T634">
                  <ts e="T635" id="Seg_2633" n="HIAT:w" s="T634">Šaman</ts>
                  <nts id="Seg_2634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2636" n="HIAT:w" s="T635">ej</ts>
                  <nts id="Seg_2637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2639" n="HIAT:w" s="T636">mobi</ts>
                  <nts id="Seg_2640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2642" n="HIAT:w" s="T637">dĭm</ts>
                  <nts id="Seg_2643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2645" n="HIAT:w" s="T638">dʼazirzittə</ts>
                  <nts id="Seg_2646" n="HIAT:ip">.</nts>
                  <nts id="Seg_2647" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T643" id="Seg_2649" n="HIAT:u" s="T639">
                  <ts e="T641" id="Seg_2651" n="HIAT:w" s="T639">Все-таки</ts>
                  <nts id="Seg_2652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2654" n="HIAT:w" s="T641">külaːmbi</ts>
                  <nts id="Seg_2655" n="HIAT:ip">.</nts>
                  <nts id="Seg_2656" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T646" id="Seg_2658" n="HIAT:u" s="T643">
                  <ts e="T644" id="Seg_2660" n="HIAT:w" s="T643">Kumən</ts>
                  <nts id="Seg_2661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2663" n="HIAT:w" s="T644">ej</ts>
                  <nts id="Seg_2664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2666" n="HIAT:w" s="T645">ĭzembi</ts>
                  <nts id="Seg_2667" n="HIAT:ip">.</nts>
                  <nts id="Seg_2668" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T649" id="Seg_2670" n="HIAT:u" s="T646">
                  <ts e="T647" id="Seg_2672" n="HIAT:w" s="T646">Tüj</ts>
                  <nts id="Seg_2673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2675" n="HIAT:w" s="T647">bar</ts>
                  <nts id="Seg_2676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2678" n="HIAT:w" s="T648">iʔbolaʔbə</ts>
                  <nts id="Seg_2679" n="HIAT:ip">.</nts>
                  <nts id="Seg_2680" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T653" id="Seg_2682" n="HIAT:u" s="T649">
                  <ts e="T651" id="Seg_2684" n="HIAT:w" s="T649">Ĭmbizʼiʔdə</ts>
                  <nts id="Seg_2685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2687" n="HIAT:w" s="T651">ej</ts>
                  <nts id="Seg_2688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2690" n="HIAT:w" s="T652">mĭngəlie</ts>
                  <nts id="Seg_2691" n="HIAT:ip">.</nts>
                  <nts id="Seg_2692" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T658" id="Seg_2694" n="HIAT:u" s="T653">
                  <ts e="T654" id="Seg_2696" n="HIAT:w" s="T653">Măna</ts>
                  <nts id="Seg_2697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2699" n="HIAT:w" s="T654">bar</ts>
                  <nts id="Seg_2700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2702" n="HIAT:w" s="T655">münörbiʔi</ts>
                  <nts id="Seg_2703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2705" n="HIAT:w" s="T656">ugaːndə</ts>
                  <nts id="Seg_2706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2708" n="HIAT:w" s="T657">tăŋ</ts>
                  <nts id="Seg_2709" n="HIAT:ip">.</nts>
                  <nts id="Seg_2710" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T663" id="Seg_2712" n="HIAT:u" s="T658">
                  <ts e="T659" id="Seg_2714" n="HIAT:w" s="T658">Bospə</ts>
                  <nts id="Seg_2715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2717" n="HIAT:w" s="T659">ej</ts>
                  <nts id="Seg_2718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2720" n="HIAT:w" s="T660">tĭmnem</ts>
                  <nts id="Seg_2721" n="HIAT:ip">,</nts>
                  <nts id="Seg_2722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2724" n="HIAT:w" s="T661">ĭmbizʼiʔ</ts>
                  <nts id="Seg_2725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2727" n="HIAT:w" s="T662">münörbiʔi</ts>
                  <nts id="Seg_2728" n="HIAT:ip">.</nts>
                  <nts id="Seg_2729" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T670" id="Seg_2731" n="HIAT:u" s="T663">
                  <nts id="Seg_2732" n="HIAT:ip">(</nts>
                  <ts e="T664" id="Seg_2734" n="HIAT:w" s="T663">Baram</ts>
                  <nts id="Seg_2735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2737" n="HIAT:w" s="T664">-</ts>
                  <nts id="Seg_2738" n="HIAT:ip">)</nts>
                  <nts id="Seg_2739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2741" n="HIAT:w" s="T665">Măn</ts>
                  <nts id="Seg_2742" n="HIAT:ip">,</nts>
                  <nts id="Seg_2743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2745" n="HIAT:w" s="T666">măn</ts>
                  <nts id="Seg_2746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2748" n="HIAT:w" s="T667">bar</ts>
                  <nts id="Seg_2749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2751" n="HIAT:w" s="T668">ĭzembi</ts>
                  <nts id="Seg_2752" n="HIAT:ip">,</nts>
                  <nts id="Seg_2753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2755" n="HIAT:w" s="T669">ĭzemniem</ts>
                  <nts id="Seg_2756" n="HIAT:ip">.</nts>
                  <nts id="Seg_2757" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T672" id="Seg_2759" n="HIAT:u" s="T670">
                  <ts e="T671" id="Seg_2761" n="HIAT:w" s="T670">Ulum</ts>
                  <nts id="Seg_2762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2764" n="HIAT:w" s="T671">ĭzemnie</ts>
                  <nts id="Seg_2765" n="HIAT:ip">.</nts>
                  <nts id="Seg_2766" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T674" id="Seg_2768" n="HIAT:u" s="T672">
                  <ts e="T673" id="Seg_2770" n="HIAT:w" s="T672">Kum</ts>
                  <nts id="Seg_2771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2773" n="HIAT:w" s="T673">ĭzemnie</ts>
                  <nts id="Seg_2774" n="HIAT:ip">.</nts>
                  <nts id="Seg_2775" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T676" id="Seg_2777" n="HIAT:u" s="T674">
                  <ts e="T675" id="Seg_2779" n="HIAT:w" s="T674">Sʼimam</ts>
                  <nts id="Seg_2780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2782" n="HIAT:w" s="T675">ĭzemnie</ts>
                  <nts id="Seg_2783" n="HIAT:ip">.</nts>
                  <nts id="Seg_2784" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T678" id="Seg_2786" n="HIAT:u" s="T676">
                  <ts e="T677" id="Seg_2788" n="HIAT:w" s="T676">Tĭmem</ts>
                  <nts id="Seg_2789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2791" n="HIAT:w" s="T677">ĭzemnie</ts>
                  <nts id="Seg_2792" n="HIAT:ip">.</nts>
                  <nts id="Seg_2793" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T680" id="Seg_2795" n="HIAT:u" s="T678">
                  <ts e="T679" id="Seg_2797" n="HIAT:w" s="T678">Püjem</ts>
                  <nts id="Seg_2798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2800" n="HIAT:w" s="T679">ĭzemnie</ts>
                  <nts id="Seg_2801" n="HIAT:ip">.</nts>
                  <nts id="Seg_2802" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T682" id="Seg_2804" n="HIAT:u" s="T680">
                  <ts e="T681" id="Seg_2806" n="HIAT:w" s="T680">Udam</ts>
                  <nts id="Seg_2807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2809" n="HIAT:w" s="T681">ĭzemnie</ts>
                  <nts id="Seg_2810" n="HIAT:ip">.</nts>
                  <nts id="Seg_2811" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T684" id="Seg_2813" n="HIAT:u" s="T682">
                  <ts e="T683" id="Seg_2815" n="HIAT:w" s="T682">Ujum</ts>
                  <nts id="Seg_2816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2818" n="HIAT:w" s="T683">ĭzemnie</ts>
                  <nts id="Seg_2819" n="HIAT:ip">.</nts>
                  <nts id="Seg_2820" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T688" id="Seg_2822" n="HIAT:u" s="T684">
                  <ts e="T685" id="Seg_2824" n="HIAT:w" s="T684">Nanəm</ts>
                  <nts id="Seg_2825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2827" n="HIAT:w" s="T685">ĭzemnie</ts>
                  <nts id="Seg_2828" n="HIAT:ip">,</nts>
                  <nts id="Seg_2829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2831" n="HIAT:w" s="T686">sĭjbə</ts>
                  <nts id="Seg_2832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2834" n="HIAT:w" s="T687">ĭzemnie</ts>
                  <nts id="Seg_2835" n="HIAT:ip">.</nts>
                  <nts id="Seg_2836" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T690" id="Seg_2838" n="HIAT:u" s="T688">
                  <nts id="Seg_2839" n="HIAT:ip">(</nts>
                  <ts e="T689" id="Seg_2841" n="HIAT:w" s="T688">Bögelbə</ts>
                  <nts id="Seg_2842" n="HIAT:ip">)</nts>
                  <nts id="Seg_2843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2845" n="HIAT:w" s="T689">ĭzemnie</ts>
                  <nts id="Seg_2846" n="HIAT:ip">.</nts>
                  <nts id="Seg_2847" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PKZ">
            <ts e="T176" id="Seg_2848" n="sc" s="T4">
               <ts e="T5" id="Seg_2850" n="e" s="T4">Nezeŋ </ts>
               <ts e="T6" id="Seg_2852" n="e" s="T5">ugaːndə </ts>
               <ts e="T7" id="Seg_2854" n="e" s="T6">iʔgö </ts>
               <ts e="T8" id="Seg_2856" n="e" s="T7">šonəgaʔi. </ts>
               <ts e="T9" id="Seg_2858" n="e" s="T8">Măn </ts>
               <ts e="T10" id="Seg_2860" n="e" s="T9">mămbiam: </ts>
               <ts e="T11" id="Seg_2862" n="e" s="T10">gijen </ts>
               <ts e="T12" id="Seg_2864" n="e" s="T11">ibileʔ? </ts>
               <ts e="T13" id="Seg_2866" n="e" s="T12">Dĭzeŋ </ts>
               <ts e="T14" id="Seg_2868" n="e" s="T13">mănliaʔi: </ts>
               <ts e="T15" id="Seg_2870" n="e" s="T14">tüžöj </ts>
               <ts e="T16" id="Seg_2872" n="e" s="T15">surdəsʼtə </ts>
               <ts e="T17" id="Seg_2874" n="e" s="T16">mĭmbibeʔ. </ts>
               <ts e="T18" id="Seg_2876" n="e" s="T17">Deʔkeʔ </ts>
               <ts e="T19" id="Seg_2878" n="e" s="T18">măna </ts>
               <ts e="T20" id="Seg_2880" n="e" s="T19">sut! </ts>
               <ts e="T21" id="Seg_2882" n="e" s="T20">Kabarləj </ts>
               <ts e="T22" id="Seg_2884" n="e" s="T21">daška. </ts>
               <ts e="T23" id="Seg_2886" n="e" s="T22">Ešši </ts>
               <ts e="T24" id="Seg_2888" n="e" s="T23">bar </ts>
               <ts e="T25" id="Seg_2890" n="e" s="T24">tüʔpi, </ts>
               <ts e="T26" id="Seg_2892" n="e" s="T25">ugaːndə </ts>
               <ts e="T27" id="Seg_2894" n="e" s="T26">sagər. </ts>
               <ts e="T28" id="Seg_2896" n="e" s="T27">Bar </ts>
               <ts e="T29" id="Seg_2898" n="e" s="T28">koluʔpi. </ts>
               <ts e="T30" id="Seg_2900" n="e" s="T29">Nada </ts>
               <ts e="T31" id="Seg_2902" n="e" s="T30">bazəsʼtə </ts>
               <ts e="T32" id="Seg_2904" n="e" s="T31">dĭm. </ts>
               <ts e="T33" id="Seg_2906" n="e" s="T32">(Dĭ </ts>
               <ts e="T34" id="Seg_2908" n="e" s="T33">-) </ts>
               <ts e="T35" id="Seg_2910" n="e" s="T34">Kĭškəsʼtə, </ts>
               <ts e="T36" id="Seg_2912" n="e" s="T35">dĭgəttə </ts>
               <ts e="T37" id="Seg_2914" n="e" s="T36">bazəsʼtə </ts>
               <ts e="T38" id="Seg_2916" n="e" s="T37">nada. </ts>
               <ts e="T39" id="Seg_2918" n="e" s="T38">(Am </ts>
               <ts e="T40" id="Seg_2920" n="e" s="T39">-) </ts>
               <ts e="T41" id="Seg_2922" n="e" s="T40">Kürzittə </ts>
               <ts e="T42" id="Seg_2924" n="e" s="T41">nada. </ts>
               <ts e="T43" id="Seg_2926" n="e" s="T42">Dĭgəttə </ts>
               <ts e="T44" id="Seg_2928" n="e" s="T43">amzittə </ts>
               <ts e="T45" id="Seg_2930" n="e" s="T44">mĭzittə. </ts>
               <ts e="T46" id="Seg_2932" n="e" s="T45">Dĭgəttə </ts>
               <ts e="T47" id="Seg_2934" n="e" s="T46">kunolzittə </ts>
               <ts e="T48" id="Seg_2936" n="e" s="T47">(iʔbəs </ts>
               <ts e="T49" id="Seg_2938" n="e" s="T48">-) </ts>
               <ts e="T50" id="Seg_2940" n="e" s="T49">(iʔbətʼsʼiʔ) </ts>
               <ts e="T51" id="Seg_2942" n="e" s="T50">nada. </ts>
               <ts e="T52" id="Seg_2944" n="e" s="T51">Ordəʔ </ts>
               <ts e="T53" id="Seg_2946" n="e" s="T52">eššim! </ts>
               <ts e="T54" id="Seg_2948" n="e" s="T53">Baːr! </ts>
               <ts e="T55" id="Seg_2950" n="e" s="T54">Amnoʔ </ts>
               <ts e="T56" id="Seg_2952" n="e" s="T55">turagən! </ts>
               <ts e="T57" id="Seg_2954" n="e" s="T56">Măn </ts>
               <ts e="T58" id="Seg_2956" n="e" s="T57">nʼiʔdə </ts>
               <ts e="T59" id="Seg_2958" n="e" s="T58">kallam! </ts>
               <ts e="T60" id="Seg_2960" n="e" s="T59">Kĭnzəsʼtə. </ts>
               <ts e="T61" id="Seg_2962" n="e" s="T60">Tüʔsittə. </ts>
               <ts e="T62" id="Seg_2964" n="e" s="T61">Dĭgəttə </ts>
               <ts e="T63" id="Seg_2966" n="e" s="T62">šolam. </ts>
               <ts e="T64" id="Seg_2968" n="e" s="T63">Amnolam </ts>
               <ts e="T65" id="Seg_2970" n="e" s="T64">tănziʔ. </ts>
               <ts e="T66" id="Seg_2972" n="e" s="T65">I </ts>
               <ts e="T67" id="Seg_2974" n="e" s="T66">dʼăbaktərləbaʔ! </ts>
               <ts e="T68" id="Seg_2976" n="e" s="T67">Kabarləj. </ts>
               <ts e="T69" id="Seg_2978" n="e" s="T68">Măn </ts>
               <ts e="T70" id="Seg_2980" n="e" s="T69">kötenbə </ts>
               <ts e="T71" id="Seg_2982" n="e" s="T70">kuvas </ts>
               <ts e="T72" id="Seg_2984" n="e" s="T71">tăn </ts>
               <ts e="T73" id="Seg_2986" n="e" s="T72">kadəldə. </ts>
               <ts e="T74" id="Seg_2988" n="e" s="T73">(Uba </ts>
               <ts e="T75" id="Seg_2990" n="e" s="T74">-) </ts>
               <ts e="T76" id="Seg_2992" n="e" s="T75">Ugaːndə </ts>
               <ts e="T77" id="Seg_2994" n="e" s="T76">nʼešpək </ts>
               <ts e="T78" id="Seg_2996" n="e" s="T77">köten. </ts>
               <ts e="T79" id="Seg_2998" n="e" s="T78">Ugaːndə </ts>
               <ts e="T80" id="Seg_3000" n="e" s="T79">sagər </ts>
               <ts e="T81" id="Seg_3002" n="e" s="T80">köten. </ts>
               <ts e="T82" id="Seg_3004" n="e" s="T81">Ugaːndə </ts>
               <ts e="T83" id="Seg_3006" n="e" s="T82">komu </ts>
               <ts e="T84" id="Seg_3008" n="e" s="T83">köten. </ts>
               <ts e="T85" id="Seg_3010" n="e" s="T84">Ugaːndə </ts>
               <ts e="T86" id="Seg_3012" n="e" s="T85">sĭre </ts>
               <ts e="T87" id="Seg_3014" n="e" s="T86">köten. </ts>
               <ts e="T88" id="Seg_3016" n="e" s="T87">Ugaːndə </ts>
               <ts e="T89" id="Seg_3018" n="e" s="T88">nʼešpək </ts>
               <ts e="T90" id="Seg_3020" n="e" s="T89">köten. </ts>
               <ts e="T91" id="Seg_3022" n="e" s="T90">Ugaːndə </ts>
               <ts e="T92" id="Seg_3024" n="e" s="T91">todam </ts>
               <ts e="T93" id="Seg_3026" n="e" s="T92">(kötek-) </ts>
               <ts e="T94" id="Seg_3028" n="e" s="T93">köten. </ts>
               <ts e="T95" id="Seg_3030" n="e" s="T94">Kabarləj. </ts>
               <ts e="T96" id="Seg_3032" n="e" s="T95">Ešši </ts>
               <ts e="T97" id="Seg_3034" n="e" s="T96">šonəgal. </ts>
               <ts e="T98" id="Seg_3036" n="e" s="T97">Udazaŋdə </ts>
               <ts e="T99" id="Seg_3038" n="e" s="T98">bar </ts>
               <ts e="T100" id="Seg_3040" n="e" s="T99">balgaš. </ts>
               <ts e="T101" id="Seg_3042" n="e" s="T100">Nada </ts>
               <ts e="T102" id="Seg_3044" n="e" s="T101">bazəsʼtə. </ts>
               <ts e="T103" id="Seg_3046" n="e" s="T102">Šoʔ </ts>
               <ts e="T104" id="Seg_3048" n="e" s="T103">döbər! </ts>
               <ts e="T105" id="Seg_3050" n="e" s="T104">Iʔ </ts>
               <ts e="T106" id="Seg_3052" n="e" s="T105">dʼoraʔ! </ts>
               <ts e="T107" id="Seg_3054" n="e" s="T106">Kanaʔ </ts>
               <ts e="T108" id="Seg_3056" n="e" s="T107">maʔnəl! </ts>
               <ts e="T109" id="Seg_3058" n="e" s="T108">(Tăna </ts>
               <ts e="T110" id="Seg_3060" n="e" s="T109">-) </ts>
               <ts e="T111" id="Seg_3062" n="e" s="T110">Tănan </ts>
               <ts e="T112" id="Seg_3064" n="e" s="T111">padʼi </ts>
               <ts e="T113" id="Seg_3066" n="e" s="T112">(mondro </ts>
               <ts e="T114" id="Seg_3068" n="e" s="T113">- </ts>
               <ts e="T115" id="Seg_3070" n="e" s="T114">mando </ts>
               <ts e="T116" id="Seg_3072" n="e" s="T115">-) </ts>
               <ts e="T117" id="Seg_3074" n="e" s="T116">măndolaʔbəʔjə </ts>
               <ts e="T118" id="Seg_3076" n="e" s="T117">bar. </ts>
               <ts e="T119" id="Seg_3078" n="e" s="T118">A_to </ts>
               <ts e="T120" id="Seg_3080" n="e" s="T119">nabəʔi </ts>
               <ts e="T121" id="Seg_3082" n="e" s="T120">(bargamunəʔi) </ts>
               <ts e="T122" id="Seg_3084" n="e" s="T121">tănan. </ts>
               <ts e="T123" id="Seg_3086" n="e" s="T122">Bar. </ts>
               <ts e="T124" id="Seg_3088" n="e" s="T123">Gibər </ts>
               <ts e="T125" id="Seg_3090" n="e" s="T124">(paʔlaːngəl). </ts>
               <ts e="T126" id="Seg_3092" n="e" s="T125">(Iʔ </ts>
               <ts e="T127" id="Seg_3094" n="e" s="T126">pănaʔ </ts>
               <ts e="T128" id="Seg_3096" n="e" s="T127">- </ts>
               <ts e="T129" id="Seg_3098" n="e" s="T128">i </ts>
               <ts e="T130" id="Seg_3100" n="e" s="T129">- </ts>
               <ts e="T131" id="Seg_3102" n="e" s="T130">iʔ </ts>
               <ts e="T132" id="Seg_3104" n="e" s="T131">păʔ </ts>
               <ts e="T133" id="Seg_3106" n="e" s="T132">- </ts>
               <ts e="T134" id="Seg_3108" n="e" s="T133">iʔ </ts>
               <ts e="T135" id="Seg_3110" n="e" s="T134">păllaʔ </ts>
               <ts e="T136" id="Seg_3112" n="e" s="T135">- </ts>
               <ts e="T137" id="Seg_3114" n="e" s="T136">iʔ </ts>
               <ts e="T138" id="Seg_3116" n="e" s="T137">pănaʔgaʔ </ts>
               <ts e="T139" id="Seg_3118" n="e" s="T138">-). </ts>
               <ts e="T140" id="Seg_3120" n="e" s="T139">Господи. </ts>
               <ts e="T141" id="Seg_3122" n="e" s="T140">Gibər </ts>
               <ts e="T142" id="Seg_3124" n="e" s="T141">(pă-) </ts>
               <ts e="T143" id="Seg_3126" n="e" s="T142">paʔlaːndəgal? </ts>
               <ts e="T144" id="Seg_3128" n="e" s="T143">Iʔ </ts>
               <ts e="T145" id="Seg_3130" n="e" s="T144">pădaʔ, </ts>
               <ts e="T146" id="Seg_3132" n="e" s="T145">a_to </ts>
               <ts e="T147" id="Seg_3134" n="e" s="T146">saʔməluʔləl! </ts>
               <ts e="T148" id="Seg_3136" n="e" s="T147">Šoʔ </ts>
               <ts e="T149" id="Seg_3138" n="e" s="T148">döbər! </ts>
               <ts e="T150" id="Seg_3140" n="e" s="T149">Dĭ </ts>
               <ts e="T151" id="Seg_3142" n="e" s="T150">koʔbdo </ts>
               <ts e="T152" id="Seg_3144" n="e" s="T151">amnolaʔbə, </ts>
               <ts e="T153" id="Seg_3146" n="e" s="T152">todam </ts>
               <ts e="T154" id="Seg_3148" n="e" s="T153">bar. </ts>
               <ts e="T155" id="Seg_3150" n="e" s="T154">(M </ts>
               <ts e="T156" id="Seg_3152" n="e" s="T155">e </ts>
               <ts e="T157" id="Seg_3154" n="e" s="T156">ej </ts>
               <ts e="T158" id="Seg_3156" n="e" s="T157">m) </ts>
               <ts e="T159" id="Seg_3158" n="e" s="T158">Ĭmbi </ts>
               <ts e="T160" id="Seg_3160" n="e" s="T159">măndəliam, </ts>
               <ts e="T161" id="Seg_3162" n="e" s="T160">dĭ </ts>
               <ts e="T162" id="Seg_3164" n="e" s="T161">ej </ts>
               <ts e="T163" id="Seg_3166" n="e" s="T162">nʼilgölaʔbə. </ts>
               <ts e="T164" id="Seg_3168" n="e" s="T163">Dʼijenə </ts>
               <ts e="T165" id="Seg_3170" n="e" s="T164">(kandləʔ </ts>
               <ts e="T166" id="Seg_3172" n="e" s="T165">-) </ts>
               <ts e="T167" id="Seg_3174" n="e" s="T166">kandəgaʔi </ts>
               <ts e="T168" id="Seg_3176" n="e" s="T167">ineʔsiʔ. </ts>
               <ts e="T169" id="Seg_3178" n="e" s="T168">Kola </ts>
               <ts e="T170" id="Seg_3180" n="e" s="T169">dʼabəlaʔi. </ts>
               <ts e="T171" id="Seg_3182" n="e" s="T170">Kola </ts>
               <ts e="T172" id="Seg_3184" n="e" s="T171">deʔleʔi. </ts>
               <ts e="T173" id="Seg_3186" n="e" s="T172">Măna </ts>
               <ts e="T174" id="Seg_3188" n="e" s="T173">možet </ts>
               <ts e="T175" id="Seg_3190" n="e" s="T174">mĭleʔi. </ts>
               <ts e="T176" id="Seg_3192" n="e" s="T175">Kabarləj. </ts>
            </ts>
            <ts e="T308" id="Seg_3193" n="sc" s="T177">
               <ts e="T178" id="Seg_3195" n="e" s="T177">Dʼagagən </ts>
               <ts e="T179" id="Seg_3197" n="e" s="T178">udaziʔ </ts>
               <ts e="T180" id="Seg_3199" n="e" s="T179">kola </ts>
               <ts e="T181" id="Seg_3201" n="e" s="T180">dʼaːpiʔi. </ts>
               <ts e="T182" id="Seg_3203" n="e" s="T181">(Dʼünə) </ts>
               <ts e="T183" id="Seg_3205" n="e" s="T182">Dʼünə </ts>
               <ts e="T184" id="Seg_3207" n="e" s="T183">(kə </ts>
               <ts e="T185" id="Seg_3209" n="e" s="T184">-) </ts>
               <ts e="T186" id="Seg_3211" n="e" s="T185">embiʔi. </ts>
               <ts e="T187" id="Seg_3213" n="e" s="T186">Dĭgəttə </ts>
               <ts e="T188" id="Seg_3215" n="e" s="T187">(tuzʼdʼa </ts>
               <ts e="T189" id="Seg_3217" n="e" s="T188">-) </ts>
               <ts e="T190" id="Seg_3219" n="e" s="T189">tustʼarbiʔi. </ts>
               <ts e="T191" id="Seg_3221" n="e" s="T190">Dĭgəttə </ts>
               <ts e="T192" id="Seg_3223" n="e" s="T191">sadarbiʔi. </ts>
               <ts e="T193" id="Seg_3225" n="e" s="T192">Kabar. </ts>
               <ts e="T194" id="Seg_3227" n="e" s="T193">Šĭšəge </ts>
               <ts e="T195" id="Seg_3229" n="e" s="T194">molaːmbi. </ts>
               <ts e="T196" id="Seg_3231" n="e" s="T195">Ejü </ts>
               <ts e="T197" id="Seg_3233" n="e" s="T196">kalla </ts>
               <ts e="T198" id="Seg_3235" n="e" s="T197">dʼürbi. </ts>
               <ts e="T199" id="Seg_3237" n="e" s="T198">Kuja </ts>
               <ts e="T200" id="Seg_3239" n="e" s="T199">amga </ts>
               <ts e="T201" id="Seg_3241" n="e" s="T200">măndolaʔbə. </ts>
               <ts e="T202" id="Seg_3243" n="e" s="T201">Sĭre </ts>
               <ts e="T203" id="Seg_3245" n="e" s="T202">šonəga. </ts>
               <ts e="T204" id="Seg_3247" n="e" s="T203">Beržə </ts>
               <ts e="T205" id="Seg_3249" n="e" s="T204">bar </ts>
               <ts e="T206" id="Seg_3251" n="e" s="T205">ugaːndə </ts>
               <ts e="T207" id="Seg_3253" n="e" s="T206">šĭšəge. </ts>
               <ts e="T208" id="Seg_3255" n="e" s="T207">Măn </ts>
               <ts e="T209" id="Seg_3257" n="e" s="T208">pa </ts>
               <ts e="T210" id="Seg_3259" n="e" s="T209">iʔgö. </ts>
               <ts e="T211" id="Seg_3261" n="e" s="T210">Dĭ </ts>
               <ts e="T212" id="Seg_3263" n="e" s="T211">könə </ts>
               <ts e="T213" id="Seg_3265" n="e" s="T212">kabarləj. </ts>
               <ts e="T214" id="Seg_3267" n="e" s="T213">Ĭmbi </ts>
               <ts e="T215" id="Seg_3269" n="e" s="T214">ulul </ts>
               <ts e="T216" id="Seg_3271" n="e" s="T215">tĭlliel? </ts>
               <ts e="T217" id="Seg_3273" n="e" s="T216">Padʼi </ts>
               <ts e="T218" id="Seg_3275" n="e" s="T217">unuʔi </ts>
               <ts e="T219" id="Seg_3277" n="e" s="T218">ige. </ts>
               <ts e="T220" id="Seg_3279" n="e" s="T219">Ugaːndə </ts>
               <ts e="T221" id="Seg_3281" n="e" s="T220">iʔgö </ts>
               <ts e="T222" id="Seg_3283" n="e" s="T221">unu. </ts>
               <ts e="T223" id="Seg_3285" n="e" s="T222">Kuʔsʼittə </ts>
               <ts e="T224" id="Seg_3287" n="e" s="T223">nada </ts>
               <ts e="T225" id="Seg_3289" n="e" s="T224">dĭzeŋ. </ts>
               <ts e="T226" id="Seg_3291" n="e" s="T225">Kabarləj. </ts>
               <ts e="T227" id="Seg_3293" n="e" s="T226">Măn </ts>
               <ts e="T228" id="Seg_3295" n="e" s="T227">iʔbəbiem </ts>
               <ts e="T229" id="Seg_3297" n="e" s="T228">kunolzittə. </ts>
               <ts e="T230" id="Seg_3299" n="e" s="T229">Kuriza </ts>
               <ts e="T231" id="Seg_3301" n="e" s="T230">ej </ts>
               <ts e="T232" id="Seg_3303" n="e" s="T231">mĭbi </ts>
               <ts e="T233" id="Seg_3305" n="e" s="T232">kunolzittə. </ts>
               <ts e="T234" id="Seg_3307" n="e" s="T233">Măn </ts>
               <ts e="T235" id="Seg_3309" n="e" s="T234">uʔbdəbiam. </ts>
               <ts e="T236" id="Seg_3311" n="e" s="T235">I </ts>
               <ts e="T237" id="Seg_3313" n="e" s="T236">(da </ts>
               <ts e="T238" id="Seg_3315" n="e" s="T237">-) </ts>
               <ts e="T239" id="Seg_3317" n="e" s="T238">daška </ts>
               <ts e="T240" id="Seg_3319" n="e" s="T239">ej </ts>
               <ts e="T241" id="Seg_3321" n="e" s="T240">iʔbiem. </ts>
               <ts e="T242" id="Seg_3323" n="e" s="T241">Kuvas </ts>
               <ts e="T243" id="Seg_3325" n="e" s="T242">(nüke) </ts>
               <ts e="T244" id="Seg_3327" n="e" s="T243">amnolaʔbə. </ts>
               <ts e="T245" id="Seg_3329" n="e" s="T244">Kuvas </ts>
               <ts e="T246" id="Seg_3331" n="e" s="T245">nʼi </ts>
               <ts e="T247" id="Seg_3333" n="e" s="T246">amnolaʔbə. </ts>
               <ts e="T248" id="Seg_3335" n="e" s="T247">Măn </ts>
               <ts e="T249" id="Seg_3337" n="e" s="T248">ugaːndə </ts>
               <ts e="T250" id="Seg_3339" n="e" s="T249">dʼaktə </ts>
               <ts e="T251" id="Seg_3341" n="e" s="T250">molaːmbiam. </ts>
               <ts e="T252" id="Seg_3343" n="e" s="T251">Šidegöʔ </ts>
               <ts e="T253" id="Seg_3345" n="e" s="T252">malaʔpibaʔ. </ts>
               <ts e="T254" id="Seg_3347" n="e" s="T253">Măn </ts>
               <ts e="T255" id="Seg_3349" n="e" s="T254">da </ts>
               <ts e="T256" id="Seg_3351" n="e" s="T255">kagam. </ts>
               <ts e="T257" id="Seg_3353" n="e" s="T256">(Dĭ </ts>
               <ts e="T258" id="Seg_3355" n="e" s="T257">- </ts>
               <ts e="T259" id="Seg_3357" n="e" s="T258">-) </ts>
               <ts e="T260" id="Seg_3359" n="e" s="T259">A </ts>
               <ts e="T261" id="Seg_3361" n="e" s="T260">dĭzeŋ </ts>
               <ts e="T262" id="Seg_3363" n="e" s="T261">bar </ts>
               <ts e="T263" id="Seg_3365" n="e" s="T262">külaːmbiʔi </ts>
               <ts e="T264" id="Seg_3367" n="e" s="T263">bar. </ts>
               <ts e="T265" id="Seg_3369" n="e" s="T264">Ну, </ts>
               <ts e="T266" id="Seg_3371" n="e" s="T265">наверное </ts>
               <ts e="T267" id="Seg_3373" n="e" s="T266">все </ts>
               <ts e="T268" id="Seg_3375" n="e" s="T267">таперь </ts>
               <ts e="T269" id="Seg_3377" n="e" s="T268">уже. </ts>
               <ts e="T270" id="Seg_3379" n="e" s="T269">Iʔgö </ts>
               <ts e="T271" id="Seg_3381" n="e" s="T270">pʼe </ts>
               <ts e="T272" id="Seg_3383" n="e" s="T271">kambi </ts>
               <ts e="T273" id="Seg_3385" n="e" s="T272">bar. </ts>
               <ts e="T274" id="Seg_3387" n="e" s="T273">Bor </ts>
               <ts e="T275" id="Seg_3389" n="e" s="T274">dʼünə </ts>
               <ts e="T276" id="Seg_3391" n="e" s="T275">(am </ts>
               <ts e="T277" id="Seg_3393" n="e" s="T276">-) </ts>
               <ts e="T278" id="Seg_3395" n="e" s="T277">amnolial. </ts>
               <ts e="T279" id="Seg_3397" n="e" s="T278">Surno </ts>
               <ts e="T280" id="Seg_3399" n="e" s="T279">moləj. </ts>
               <ts e="T281" id="Seg_3401" n="e" s="T280">Udam </ts>
               <ts e="T282" id="Seg_3403" n="e" s="T281">dʼükəmnie, </ts>
               <ts e="T283" id="Seg_3405" n="e" s="T282">nada </ts>
               <ts e="T284" id="Seg_3407" n="e" s="T283">kădəsʼtə. </ts>
               <ts e="T285" id="Seg_3409" n="e" s="T284">Tăn </ts>
               <ts e="T286" id="Seg_3411" n="e" s="T285">ugaːndə </ts>
               <ts e="T287" id="Seg_3413" n="e" s="T286">(š </ts>
               <ts e="T288" id="Seg_3415" n="e" s="T287">-) </ts>
               <ts e="T289" id="Seg_3417" n="e" s="T288">šĭkel </ts>
               <ts e="T290" id="Seg_3419" n="e" s="T289">numo. </ts>
               <ts e="T291" id="Seg_3421" n="e" s="T290">Kudonzlial </ts>
               <ts e="T292" id="Seg_3423" n="e" s="T291">tăn. </ts>
               <ts e="T293" id="Seg_3425" n="e" s="T292">Tăn </ts>
               <ts e="T294" id="Seg_3427" n="e" s="T293">tĭmel </ts>
               <ts e="T295" id="Seg_3429" n="e" s="T294">ugaːndə </ts>
               <ts e="T296" id="Seg_3431" n="e" s="T295">numo. </ts>
               <ts e="T297" id="Seg_3433" n="e" s="T296">Püjel </ts>
               <ts e="T298" id="Seg_3435" n="e" s="T297">(nuŋo </ts>
               <ts e="T299" id="Seg_3437" n="e" s="T298">-) </ts>
               <ts e="T300" id="Seg_3439" n="e" s="T299">numo. </ts>
               <ts e="T301" id="Seg_3441" n="e" s="T300">Kuʔlə </ts>
               <ts e="T302" id="Seg_3443" n="e" s="T301">bar </ts>
               <ts e="T303" id="Seg_3445" n="e" s="T302">mʼaŋŋaʔbə. </ts>
               <ts e="T304" id="Seg_3447" n="e" s="T303">(Monzaŋbə </ts>
               <ts e="T305" id="Seg_3449" n="e" s="T304">guan </ts>
               <ts e="T306" id="Seg_3451" n="e" s="T305">-) </ts>
               <ts e="T307" id="Seg_3453" n="e" s="T306">Monzaŋdə </ts>
               <ts e="T308" id="Seg_3455" n="e" s="T307">nʼešpək. </ts>
            </ts>
            <ts e="T465" id="Seg_3456" n="sc" s="T309">
               <ts e="T310" id="Seg_3458" n="e" s="T309">Ну-ну-ну. </ts>
               <ts e="T311" id="Seg_3460" n="e" s="T310">Ugaːndə </ts>
               <ts e="T312" id="Seg_3462" n="e" s="T311">eʔbdəl </ts>
               <ts e="T313" id="Seg_3464" n="e" s="T312">numo. </ts>
               <ts e="T314" id="Seg_3466" n="e" s="T313">Ugaːndə </ts>
               <ts e="T315" id="Seg_3468" n="e" s="T314">kuzaŋdə </ts>
               <ts e="T316" id="Seg_3470" n="e" s="T315">numo. </ts>
               <ts e="T317" id="Seg_3472" n="e" s="T316">Sʼimal </ts>
               <ts e="T318" id="Seg_3474" n="e" s="T317">urgo. </ts>
               <ts e="T319" id="Seg_3476" n="e" s="T318">Ulul </ts>
               <ts e="T320" id="Seg_3478" n="e" s="T319">ugaːndə </ts>
               <ts e="T321" id="Seg_3480" n="e" s="T320">urgo. </ts>
               <ts e="T322" id="Seg_3482" n="e" s="T321">(Uda </ts>
               <ts e="T323" id="Seg_3484" n="e" s="T322">-) </ts>
               <ts e="T324" id="Seg_3486" n="e" s="T323">Udazaŋdə </ts>
               <ts e="T325" id="Seg_3488" n="e" s="T324">numo. </ts>
               <ts e="T326" id="Seg_3490" n="e" s="T325">(Uju </ts>
               <ts e="T327" id="Seg_3492" n="e" s="T326">-) </ts>
               <ts e="T328" id="Seg_3494" n="e" s="T327">Ujuzaŋdə </ts>
               <ts e="T329" id="Seg_3496" n="e" s="T328">numo. </ts>
               <ts e="T330" id="Seg_3498" n="e" s="T329">Bostə </ts>
               <ts e="T331" id="Seg_3500" n="e" s="T330">bar </ts>
               <ts e="T332" id="Seg_3502" n="e" s="T331">todam. </ts>
               <ts e="T333" id="Seg_3504" n="e" s="T332">Müjö </ts>
               <ts e="T334" id="Seg_3506" n="e" s="T333">nʼergölaʔbə. </ts>
               <ts e="T335" id="Seg_3508" n="e" s="T334">Panə </ts>
               <ts e="T336" id="Seg_3510" n="e" s="T335">amnobi. </ts>
               <ts e="T337" id="Seg_3512" n="e" s="T336">Iʔgö </ts>
               <ts e="T338" id="Seg_3514" n="e" s="T337">(nüjö </ts>
               <ts e="T339" id="Seg_3516" n="e" s="T338">- </ts>
               <ts e="T340" id="Seg_3518" n="e" s="T339">nu </ts>
               <ts e="T341" id="Seg_3520" n="e" s="T340">-) </ts>
               <ts e="T342" id="Seg_3522" n="e" s="T341">süjöʔi </ts>
               <ts e="T343" id="Seg_3524" n="e" s="T342">nʼergölaʔbəʔjə. </ts>
               <ts e="T344" id="Seg_3526" n="e" s="T343">Karatʼgaj </ts>
               <ts e="T345" id="Seg_3528" n="e" s="T344">bar </ts>
               <ts e="T346" id="Seg_3530" n="e" s="T345">(nʼiʔgə </ts>
               <ts e="T347" id="Seg_3532" n="e" s="T346">-) </ts>
               <ts e="T348" id="Seg_3534" n="e" s="T347">nʼilgölaʔbə. </ts>
               <ts e="T349" id="Seg_3536" n="e" s="T348">Pogəraš </ts>
               <ts e="T350" id="Seg_3538" n="e" s="T349">nʼilgölaʔ </ts>
               <ts e="T351" id="Seg_3540" n="e" s="T350">(s </ts>
               <ts e="T352" id="Seg_3542" n="e" s="T351">-) </ts>
               <ts e="T353" id="Seg_3544" n="e" s="T352">šobi. </ts>
               <ts e="T354" id="Seg_3546" n="e" s="T353">Kuriza </ts>
               <ts e="T355" id="Seg_3548" n="e" s="T354">bar </ts>
               <ts e="T356" id="Seg_3550" n="e" s="T355">kadəʔlia, </ts>
               <ts e="T357" id="Seg_3552" n="e" s="T356">amzittə </ts>
               <ts e="T358" id="Seg_3554" n="e" s="T357">măndərlia. </ts>
               <ts e="T359" id="Seg_3556" n="e" s="T358">Nöməlbiom </ts>
               <ts e="T360" id="Seg_3558" n="e" s="T359">bar </ts>
               <ts e="T361" id="Seg_3560" n="e" s="T360">(budə </ts>
               <ts e="T362" id="Seg_3562" n="e" s="T361">-) </ts>
               <ts e="T363" id="Seg_3564" n="e" s="T362">büm </ts>
               <ts e="T364" id="Seg_3566" n="e" s="T363">nuldəsʼtə </ts>
               <ts e="T365" id="Seg_3568" n="e" s="T364">pʼeːšdə. </ts>
               <ts e="T366" id="Seg_3570" n="e" s="T365">Dʼübige </ts>
               <ts e="T367" id="Seg_3572" n="e" s="T366">bü </ts>
               <ts e="T368" id="Seg_3574" n="e" s="T367">naga, </ts>
               <ts e="T369" id="Seg_3576" n="e" s="T368">(na </ts>
               <ts e="T370" id="Seg_3578" n="e" s="T369">-) </ts>
               <ts e="T371" id="Seg_3580" n="e" s="T370">măna </ts>
               <ts e="T372" id="Seg_3582" n="e" s="T371">kereʔ. </ts>
               <ts e="T373" id="Seg_3584" n="e" s="T372">Oroma </ts>
               <ts e="T374" id="Seg_3586" n="e" s="T373">(kămn </ts>
               <ts e="T375" id="Seg_3588" n="e" s="T374">-) </ts>
               <ts e="T376" id="Seg_3590" n="e" s="T375">kămnəlim. </ts>
               <ts e="T377" id="Seg_3592" n="e" s="T376">Bargarim. </ts>
               <ts e="T378" id="Seg_3594" n="e" s="T377">Dĭgəttə </ts>
               <ts e="T379" id="Seg_3596" n="e" s="T378">bazəsʼtə </ts>
               <ts e="T380" id="Seg_3598" n="e" s="T379">nada </ts>
               <ts e="T381" id="Seg_3600" n="e" s="T380">takšeʔi. </ts>
               <ts e="T382" id="Seg_3602" n="e" s="T381">Sĭre </ts>
               <ts e="T383" id="Seg_3604" n="e" s="T382">pa, </ts>
               <ts e="T384" id="Seg_3606" n="e" s="T383">šomne </ts>
               <ts e="T385" id="Seg_3608" n="e" s="T384">pa, </ts>
               <ts e="T386" id="Seg_3610" n="e" s="T385">komu </ts>
               <ts e="T387" id="Seg_3612" n="e" s="T386">pa, </ts>
               <ts e="T388" id="Seg_3614" n="e" s="T387">san </ts>
               <ts e="T389" id="Seg_3616" n="e" s="T388">pa. </ts>
               <ts e="T390" id="Seg_3618" n="e" s="T389">(Sagən-) </ts>
               <ts e="T391" id="Seg_3620" n="e" s="T390">pagən </ts>
               <ts e="T392" id="Seg_3622" n="e" s="T391">bar </ts>
               <ts e="T393" id="Seg_3624" n="e" s="T392">šiškaʔi </ts>
               <ts e="T394" id="Seg_3626" n="e" s="T393">özerleʔbəʔjə. </ts>
               <ts e="T395" id="Seg_3628" n="e" s="T394">San </ts>
               <ts e="T396" id="Seg_3630" n="e" s="T395">(pag </ts>
               <ts e="T397" id="Seg_3632" n="e" s="T396">-) </ts>
               <ts e="T398" id="Seg_3634" n="e" s="T397">pagən </ts>
               <ts e="T399" id="Seg_3636" n="e" s="T398">bar </ts>
               <ts e="T400" id="Seg_3638" n="e" s="T399">šiškaʔi </ts>
               <ts e="T401" id="Seg_3640" n="e" s="T400">amnolaʔbəʔjə. </ts>
               <ts e="T402" id="Seg_3642" n="e" s="T401">Sĭre </ts>
               <ts e="T403" id="Seg_3644" n="e" s="T402">pagən </ts>
               <ts e="T404" id="Seg_3646" n="e" s="T403">bar </ts>
               <ts e="T405" id="Seg_3648" n="e" s="T404">takšə </ts>
               <ts e="T406" id="Seg_3650" n="e" s="T405">alaʔbəʔjə. </ts>
               <ts e="T407" id="Seg_3652" n="e" s="T406">Segi </ts>
               <ts e="T408" id="Seg_3654" n="e" s="T407">bü </ts>
               <ts e="T409" id="Seg_3656" n="e" s="T408">bĭtleʔbəʔjə. </ts>
               <ts e="T410" id="Seg_3658" n="e" s="T409">(Paj) </ts>
               <ts e="T411" id="Seg_3660" n="e" s="T410">Paj </ts>
               <ts e="T412" id="Seg_3662" n="e" s="T411">šamnak. </ts>
               <ts e="T413" id="Seg_3664" n="e" s="T412">Bazaj </ts>
               <ts e="T414" id="Seg_3666" n="e" s="T413">šamnak. </ts>
               <ts e="T415" id="Seg_3668" n="e" s="T414">Takšə </ts>
               <ts e="T416" id="Seg_3670" n="e" s="T415">bazaj. </ts>
               <ts e="T417" id="Seg_3672" n="e" s="T416">Ugaːndə </ts>
               <ts e="T418" id="Seg_3674" n="e" s="T417">paʔi </ts>
               <ts e="T419" id="Seg_3676" n="e" s="T418">iʔgö, </ts>
               <ts e="T420" id="Seg_3678" n="e" s="T419">dĭzeŋ </ts>
               <ts e="T421" id="Seg_3680" n="e" s="T420">nada </ts>
               <ts e="T422" id="Seg_3682" n="e" s="T421">jaʔsʼittə. </ts>
               <ts e="T423" id="Seg_3684" n="e" s="T422">Dĭgəttə </ts>
               <ts e="T424" id="Seg_3686" n="e" s="T423">(kürzittə). </ts>
               <ts e="T425" id="Seg_3688" n="e" s="T424">Dĭgəttə </ts>
               <ts e="T426" id="Seg_3690" n="e" s="T425">tura </ts>
               <ts e="T427" id="Seg_3692" n="e" s="T426">(kazittə). </ts>
               <ts e="T428" id="Seg_3694" n="e" s="T427">Dĭgəttə </ts>
               <ts e="T429" id="Seg_3696" n="e" s="T428">dĭ </ts>
               <ts e="T430" id="Seg_3698" n="e" s="T429">paʔizʼiʔ </ts>
               <ts e="T431" id="Seg_3700" n="e" s="T430">tura </ts>
               <ts e="T432" id="Seg_3702" n="e" s="T431">jaʔsʼittə </ts>
               <ts e="T433" id="Seg_3704" n="e" s="T432">nada. </ts>
               <ts e="T434" id="Seg_3706" n="e" s="T433">Amnozʼittə </ts>
               <ts e="T435" id="Seg_3708" n="e" s="T434">turagən. </ts>
               <ts e="T436" id="Seg_3710" n="e" s="T435">Pʼeːš </ts>
               <ts e="T437" id="Seg_3712" n="e" s="T436">enzittə. </ts>
               <ts e="T438" id="Seg_3714" n="e" s="T437">Pʼeːšgən </ts>
               <ts e="T439" id="Seg_3716" n="e" s="T438">pürzittə </ts>
               <ts e="T440" id="Seg_3718" n="e" s="T439">ipek. </ts>
               <ts e="T441" id="Seg_3720" n="e" s="T440">Bĭdeʔ </ts>
               <ts e="T442" id="Seg_3722" n="e" s="T441">bü! </ts>
               <ts e="T443" id="Seg_3724" n="e" s="T442">Măn </ts>
               <ts e="T444" id="Seg_3726" n="e" s="T443">dibər </ts>
               <ts e="T445" id="Seg_3728" n="e" s="T444">(kăm </ts>
               <ts e="T446" id="Seg_3730" n="e" s="T445">-) </ts>
               <ts e="T447" id="Seg_3732" n="e" s="T446">embiem </ts>
               <ts e="T448" id="Seg_3734" n="e" s="T447">sĭreʔpne, </ts>
               <ts e="T449" id="Seg_3736" n="e" s="T448">(bü </ts>
               <ts e="T450" id="Seg_3738" n="e" s="T449">-) </ts>
               <ts e="T451" id="Seg_3740" n="e" s="T450">bü </ts>
               <ts e="T452" id="Seg_3742" n="e" s="T451">kămnabiam. </ts>
               <ts e="T453" id="Seg_3744" n="e" s="T452">(Na </ts>
               <ts e="T454" id="Seg_3746" n="e" s="T453">-) </ts>
               <ts e="T455" id="Seg_3748" n="e" s="T454">Namzəbi </ts>
               <ts e="T456" id="Seg_3750" n="e" s="T455">bar. </ts>
               <ts e="T457" id="Seg_3752" n="e" s="T456">Ipek </ts>
               <ts e="T458" id="Seg_3754" n="e" s="T457">endəbiem. </ts>
               <ts e="T459" id="Seg_3756" n="e" s="T458">Bĭdeʔ, </ts>
               <ts e="T460" id="Seg_3758" n="e" s="T459">ugaːndə </ts>
               <ts e="T461" id="Seg_3760" n="e" s="T460">jakšə. </ts>
               <ts e="T462" id="Seg_3762" n="e" s="T461">Šiʔ </ts>
               <ts e="T463" id="Seg_3764" n="e" s="T462">taldʼen </ts>
               <ts e="T464" id="Seg_3766" n="e" s="T463">ara </ts>
               <ts e="T465" id="Seg_3768" n="e" s="T464">bĭʔpileʔ. </ts>
            </ts>
            <ts e="T470" id="Seg_3769" n="sc" s="T467">
               <ts e="T468" id="Seg_3771" n="e" s="T467">Jezerik </ts>
               <ts e="T469" id="Seg_3773" n="e" s="T468">ibileʔ </ts>
               <ts e="T470" id="Seg_3775" n="e" s="T469">bar? </ts>
            </ts>
            <ts e="T486" id="Seg_3776" n="sc" s="T472">
               <ts e="T474" id="Seg_3778" n="e" s="T472">Teinen </ts>
               <ts e="T475" id="Seg_3780" n="e" s="T474">bar </ts>
               <ts e="T476" id="Seg_3782" n="e" s="T475">в </ts>
               <ts e="T477" id="Seg_3784" n="e" s="T476">похмелье. </ts>
               <ts e="T478" id="Seg_3786" n="e" s="T477">Bĭdeʔ </ts>
               <ts e="T479" id="Seg_3788" n="e" s="T478">komu </ts>
               <ts e="T480" id="Seg_3790" n="e" s="T479">bü. </ts>
               <ts e="T481" id="Seg_3792" n="e" s="T480">Măn </ts>
               <ts e="T482" id="Seg_3794" n="e" s="T481">dibər </ts>
               <ts e="T483" id="Seg_3796" n="e" s="T482">(sin </ts>
               <ts e="T484" id="Seg_3798" n="e" s="T483">-) </ts>
               <ts e="T485" id="Seg_3800" n="e" s="T484">sĭreʔpne </ts>
               <ts e="T486" id="Seg_3802" n="e" s="T485">embiem. </ts>
            </ts>
            <ts e="T690" id="Seg_3803" n="sc" s="T487">
               <ts e="T488" id="Seg_3805" n="e" s="T487">Bü </ts>
               <ts e="T489" id="Seg_3807" n="e" s="T488">kămnəbiam. </ts>
               <ts e="T490" id="Seg_3809" n="e" s="T489">(Namzə </ts>
               <ts e="T491" id="Seg_3811" n="e" s="T490">-) </ts>
               <ts e="T492" id="Seg_3813" n="e" s="T491">Namzəga </ts>
               <ts e="T493" id="Seg_3815" n="e" s="T492">molaːmbi. </ts>
               <ts e="T494" id="Seg_3817" n="e" s="T493">Jakšə </ts>
               <ts e="T495" id="Seg_3819" n="e" s="T494">moləj </ts>
               <ts e="T496" id="Seg_3821" n="e" s="T495">tănan. </ts>
               <ts e="T497" id="Seg_3823" n="e" s="T496">Ara </ts>
               <ts e="T498" id="Seg_3825" n="e" s="T497">iʔ </ts>
               <ts e="T499" id="Seg_3827" n="e" s="T498">bĭdeʔ, </ts>
               <ts e="T500" id="Seg_3829" n="e" s="T499">a_to </ts>
               <ts e="T501" id="Seg_3831" n="e" s="T500">ulul </ts>
               <ts e="T502" id="Seg_3833" n="e" s="T501">ĭzemnie. </ts>
               <ts e="T503" id="Seg_3835" n="e" s="T502">Sĭjdə </ts>
               <ts e="T504" id="Seg_3837" n="e" s="T503">ĭzemnie. </ts>
               <ts e="T505" id="Seg_3839" n="e" s="T504">Bĭtlel </ts>
               <ts e="T506" id="Seg_3841" n="e" s="T505">dak, </ts>
               <ts e="T507" id="Seg_3843" n="e" s="T506">külalləl. </ts>
               <ts e="T508" id="Seg_3845" n="e" s="T507">A </ts>
               <ts e="T509" id="Seg_3847" n="e" s="T508">ej </ts>
               <ts e="T510" id="Seg_3849" n="e" s="T509">bĭtlel </ts>
               <ts e="T511" id="Seg_3851" n="e" s="T510">dak </ts>
               <ts e="T512" id="Seg_3853" n="e" s="T511">kondʼo </ts>
               <ts e="T513" id="Seg_3855" n="e" s="T512">amnolal. </ts>
               <ts e="T514" id="Seg_3857" n="e" s="T513">Mašina </ts>
               <ts e="T515" id="Seg_3859" n="e" s="T514">(bɨl </ts>
               <ts e="T516" id="Seg_3861" n="e" s="T515">- </ts>
               <ts e="T517" id="Seg_3863" n="e" s="T516">tojir </ts>
               <ts e="T518" id="Seg_3865" n="e" s="T517">-) </ts>
               <ts e="T519" id="Seg_3867" n="e" s="T518">(talerlaʔbə). </ts>
               <ts e="T520" id="Seg_3869" n="e" s="T519">Dĭn </ts>
               <ts e="T521" id="Seg_3871" n="e" s="T520">kuza </ts>
               <ts e="T522" id="Seg_3873" n="e" s="T521">amnolaʔbə. </ts>
               <ts e="T523" id="Seg_3875" n="e" s="T522">Mašinagəʔ </ts>
               <ts e="T524" id="Seg_3877" n="e" s="T523">bar </ts>
               <ts e="T525" id="Seg_3879" n="e" s="T524">bər </ts>
               <ts e="T526" id="Seg_3881" n="e" s="T525">šonəga. </ts>
               <ts e="T527" id="Seg_3883" n="e" s="T526">Dĭ </ts>
               <ts e="T528" id="Seg_3885" n="e" s="T527">bar </ts>
               <ts e="T529" id="Seg_3887" n="e" s="T528">dibər </ts>
               <ts e="T530" id="Seg_3889" n="e" s="T529">(šo-) </ts>
               <ts e="T531" id="Seg_3891" n="e" s="T530">döbər </ts>
               <ts e="T532" id="Seg_3893" n="e" s="T531">kallaʔbə. </ts>
               <ts e="T533" id="Seg_3895" n="e" s="T532">Abam, </ts>
               <ts e="T534" id="Seg_3897" n="e" s="T533">ijam </ts>
               <ts e="T535" id="Seg_3899" n="e" s="T534">koʔbdo </ts>
               <ts e="T536" id="Seg_3901" n="e" s="T535">kubiʔi. </ts>
               <ts e="T537" id="Seg_3903" n="e" s="T536">(Măn) </ts>
               <ts e="T538" id="Seg_3905" n="e" s="T537">Măna </ts>
               <ts e="T539" id="Seg_3907" n="e" s="T538">mănliaʔi: </ts>
               <ts e="T540" id="Seg_3909" n="e" s="T539">kanaʔ </ts>
               <ts e="T541" id="Seg_3911" n="e" s="T540">dĭzʼiʔ! </ts>
               <ts e="T542" id="Seg_3913" n="e" s="T541">(Tĭmne </ts>
               <ts e="T543" id="Seg_3915" n="e" s="T542">-) </ts>
               <ts e="T544" id="Seg_3917" n="e" s="T543">Tĭmnit, </ts>
               <ts e="T545" id="Seg_3919" n="e" s="T544">girgit </ts>
               <ts e="T546" id="Seg_3921" n="e" s="T545">di. </ts>
               <ts e="T547" id="Seg_3923" n="e" s="T546">Dĭgəttə </ts>
               <ts e="T548" id="Seg_3925" n="e" s="T547">tibinə </ts>
               <ts e="T549" id="Seg_3927" n="e" s="T548">iliel. </ts>
               <ts e="T550" id="Seg_3929" n="e" s="T549">Dĭgəttə </ts>
               <ts e="T551" id="Seg_3931" n="e" s="T550">esseŋdə </ts>
               <ts e="T552" id="Seg_3933" n="e" s="T551">moləʔjə. </ts>
               <ts e="T553" id="Seg_3935" n="e" s="T552">Погоди. </ts>
               <ts e="T554" id="Seg_3937" n="e" s="T553">Esseŋlə </ts>
               <ts e="T555" id="Seg_3939" n="e" s="T554">özerleʔjə. </ts>
               <ts e="T556" id="Seg_3941" n="e" s="T555">Tănan </ts>
               <ts e="T557" id="Seg_3943" n="e" s="T556">kabažarlaʔjə. </ts>
               <ts e="T558" id="Seg_3945" n="e" s="T557">Nem </ts>
               <ts e="T559" id="Seg_3947" n="e" s="T558">(ə </ts>
               <ts e="T560" id="Seg_3949" n="e" s="T559">- </ts>
               <ts e="T561" id="Seg_3951" n="e" s="T560">i </ts>
               <ts e="T562" id="Seg_3953" n="e" s="T561">-) </ts>
               <ts e="T563" id="Seg_3955" n="e" s="T562">iʔ </ts>
               <ts e="T564" id="Seg_3957" n="e" s="T563">münöraʔ! </ts>
               <ts e="T565" id="Seg_3959" n="e" s="T564">(Nʼilgöt </ts>
               <ts e="T566" id="Seg_3961" n="e" s="T565">ab </ts>
               <ts e="T567" id="Seg_3963" n="e" s="T566">- </ts>
               <ts e="T568" id="Seg_3965" n="e" s="T567">abat </ts>
               <ts e="T569" id="Seg_3967" n="e" s="T568">-) </ts>
               <ts e="T570" id="Seg_3969" n="e" s="T569">Abam </ts>
               <ts e="T571" id="Seg_3971" n="e" s="T570">da </ts>
               <ts e="T572" id="Seg_3973" n="e" s="T571">ijam </ts>
               <ts e="T573" id="Seg_3975" n="e" s="T572">nʼilgöt! </ts>
               <ts e="T577" id="Seg_3977" n="e" s="T573">Все ли, что ли. </ts>
               <ts e="T578" id="Seg_3979" n="e" s="T577">Teinen </ts>
               <ts e="T579" id="Seg_3981" n="e" s="T578">bar </ts>
               <ts e="T580" id="Seg_3983" n="e" s="T579">kalla </ts>
               <ts e="T581" id="Seg_3985" n="e" s="T580">dʼürbiʔi. </ts>
               <ts e="T582" id="Seg_3987" n="e" s="T581">Abat, </ts>
               <ts e="T583" id="Seg_3989" n="e" s="T582">ijat </ts>
               <ts e="T584" id="Seg_3991" n="e" s="T583">kalla </ts>
               <ts e="T585" id="Seg_3993" n="e" s="T584">dʼürbiʔi. </ts>
               <ts e="T586" id="Seg_3995" n="e" s="T585">Esseŋ </ts>
               <ts e="T587" id="Seg_3997" n="e" s="T586">kalla </ts>
               <ts e="T588" id="Seg_3999" n="e" s="T587">dʼürbiʔi. </ts>
               <ts e="T589" id="Seg_4001" n="e" s="T588">Măn </ts>
               <ts e="T590" id="Seg_4003" n="e" s="T589">unnʼa </ts>
               <ts e="T591" id="Seg_4005" n="e" s="T590">maʔnal </ts>
               <ts e="T592" id="Seg_4007" n="e" s="T591">amnolaʔbəm. </ts>
               <ts e="T593" id="Seg_4009" n="e" s="T592">Nada </ts>
               <ts e="T594" id="Seg_4011" n="e" s="T593">dʼü </ts>
               <ts e="T595" id="Seg_4013" n="e" s="T594">tĭlzittə. </ts>
               <ts e="T596" id="Seg_4015" n="e" s="T595">Kuza </ts>
               <ts e="T597" id="Seg_4017" n="e" s="T596">külaːmbi. </ts>
               <ts e="T598" id="Seg_4019" n="e" s="T597">Nada </ts>
               <ts e="T599" id="Seg_4021" n="e" s="T598">dĭm </ts>
               <ts e="T600" id="Seg_4023" n="e" s="T599">dibər </ts>
               <ts e="T601" id="Seg_4025" n="e" s="T600">enzittə. </ts>
               <ts e="T602" id="Seg_4027" n="e" s="T601">Dĭgəttə </ts>
               <ts e="T603" id="Seg_4029" n="e" s="T602">kămnasʼtə. </ts>
               <ts e="T604" id="Seg_4031" n="e" s="T603">(Dʼo </ts>
               <ts e="T605" id="Seg_4033" n="e" s="T604">-) </ts>
               <ts e="T606" id="Seg_4035" n="e" s="T605">dĭgəttə </ts>
               <ts e="T607" id="Seg_4037" n="e" s="T606">(tʼu </ts>
               <ts e="T608" id="Seg_4039" n="e" s="T607">- </ts>
               <ts e="T609" id="Seg_4041" n="e" s="T608">dʼo </ts>
               <ts e="T610" id="Seg_4043" n="e" s="T609">- </ts>
               <ts e="T611" id="Seg_4045" n="e" s="T610">dʼora). </ts>
               <ts e="T613" id="Seg_4047" n="e" s="T611">Dĭgəttə </ts>
               <ts e="T614" id="Seg_4049" n="e" s="T613">Dĭgəttə </ts>
               <ts e="T615" id="Seg_4051" n="e" s="T614">(dujzʼ </ts>
               <ts e="T616" id="Seg_4053" n="e" s="T615">-) </ts>
               <ts e="T617" id="Seg_4055" n="e" s="T616">dʼüzʼiʔ </ts>
               <ts e="T618" id="Seg_4057" n="e" s="T617">nada </ts>
               <ts e="T619" id="Seg_4059" n="e" s="T618">kajzittə </ts>
               <ts e="T620" id="Seg_4061" n="e" s="T619">dĭm. </ts>
               <ts e="T621" id="Seg_4063" n="e" s="T620">Dĭgəttə </ts>
               <ts e="T622" id="Seg_4065" n="e" s="T621">maʔnal </ts>
               <ts e="T623" id="Seg_4067" n="e" s="T622">(su </ts>
               <ts e="T624" id="Seg_4069" n="e" s="T623">-) </ts>
               <ts e="T625" id="Seg_4071" n="e" s="T624">šozittə, </ts>
               <ts e="T626" id="Seg_4073" n="e" s="T625">amzittə, </ts>
               <ts e="T627" id="Seg_4075" n="e" s="T626">(amo </ts>
               <ts e="T628" id="Seg_4077" n="e" s="T627">-) </ts>
               <ts e="T629" id="Seg_4079" n="e" s="T628">amnaʔ </ts>
               <ts e="T630" id="Seg_4081" n="e" s="T629">amzittə. </ts>
               <ts e="T631" id="Seg_4083" n="e" s="T630">Kuza </ts>
               <ts e="T632" id="Seg_4085" n="e" s="T631">ĭzembi. </ts>
               <ts e="T633" id="Seg_4087" n="e" s="T632">Šamandə </ts>
               <ts e="T634" id="Seg_4089" n="e" s="T633">mĭmbi. </ts>
               <ts e="T635" id="Seg_4091" n="e" s="T634">Šaman </ts>
               <ts e="T636" id="Seg_4093" n="e" s="T635">ej </ts>
               <ts e="T637" id="Seg_4095" n="e" s="T636">mobi </ts>
               <ts e="T638" id="Seg_4097" n="e" s="T637">dĭm </ts>
               <ts e="T639" id="Seg_4099" n="e" s="T638">dʼazirzittə. </ts>
               <ts e="T641" id="Seg_4101" n="e" s="T639">Все-таки </ts>
               <ts e="T643" id="Seg_4103" n="e" s="T641">külaːmbi. </ts>
               <ts e="T644" id="Seg_4105" n="e" s="T643">Kumən </ts>
               <ts e="T645" id="Seg_4107" n="e" s="T644">ej </ts>
               <ts e="T646" id="Seg_4109" n="e" s="T645">ĭzembi. </ts>
               <ts e="T647" id="Seg_4111" n="e" s="T646">Tüj </ts>
               <ts e="T648" id="Seg_4113" n="e" s="T647">bar </ts>
               <ts e="T649" id="Seg_4115" n="e" s="T648">iʔbolaʔbə. </ts>
               <ts e="T651" id="Seg_4117" n="e" s="T649">Ĭmbizʼiʔdə </ts>
               <ts e="T652" id="Seg_4119" n="e" s="T651">ej </ts>
               <ts e="T653" id="Seg_4121" n="e" s="T652">mĭngəlie. </ts>
               <ts e="T654" id="Seg_4123" n="e" s="T653">Măna </ts>
               <ts e="T655" id="Seg_4125" n="e" s="T654">bar </ts>
               <ts e="T656" id="Seg_4127" n="e" s="T655">münörbiʔi </ts>
               <ts e="T657" id="Seg_4129" n="e" s="T656">ugaːndə </ts>
               <ts e="T658" id="Seg_4131" n="e" s="T657">tăŋ. </ts>
               <ts e="T659" id="Seg_4133" n="e" s="T658">Bospə </ts>
               <ts e="T660" id="Seg_4135" n="e" s="T659">ej </ts>
               <ts e="T661" id="Seg_4137" n="e" s="T660">tĭmnem, </ts>
               <ts e="T662" id="Seg_4139" n="e" s="T661">ĭmbizʼiʔ </ts>
               <ts e="T663" id="Seg_4141" n="e" s="T662">münörbiʔi. </ts>
               <ts e="T664" id="Seg_4143" n="e" s="T663">(Baram </ts>
               <ts e="T665" id="Seg_4145" n="e" s="T664">-) </ts>
               <ts e="T666" id="Seg_4147" n="e" s="T665">Măn, </ts>
               <ts e="T667" id="Seg_4149" n="e" s="T666">măn </ts>
               <ts e="T668" id="Seg_4151" n="e" s="T667">bar </ts>
               <ts e="T669" id="Seg_4153" n="e" s="T668">ĭzembi, </ts>
               <ts e="T670" id="Seg_4155" n="e" s="T669">ĭzemniem. </ts>
               <ts e="T671" id="Seg_4157" n="e" s="T670">Ulum </ts>
               <ts e="T672" id="Seg_4159" n="e" s="T671">ĭzemnie. </ts>
               <ts e="T673" id="Seg_4161" n="e" s="T672">Kum </ts>
               <ts e="T674" id="Seg_4163" n="e" s="T673">ĭzemnie. </ts>
               <ts e="T675" id="Seg_4165" n="e" s="T674">Sʼimam </ts>
               <ts e="T676" id="Seg_4167" n="e" s="T675">ĭzemnie. </ts>
               <ts e="T677" id="Seg_4169" n="e" s="T676">Tĭmem </ts>
               <ts e="T678" id="Seg_4171" n="e" s="T677">ĭzemnie. </ts>
               <ts e="T679" id="Seg_4173" n="e" s="T678">Püjem </ts>
               <ts e="T680" id="Seg_4175" n="e" s="T679">ĭzemnie. </ts>
               <ts e="T681" id="Seg_4177" n="e" s="T680">Udam </ts>
               <ts e="T682" id="Seg_4179" n="e" s="T681">ĭzemnie. </ts>
               <ts e="T683" id="Seg_4181" n="e" s="T682">Ujum </ts>
               <ts e="T684" id="Seg_4183" n="e" s="T683">ĭzemnie. </ts>
               <ts e="T685" id="Seg_4185" n="e" s="T684">Nanəm </ts>
               <ts e="T686" id="Seg_4187" n="e" s="T685">ĭzemnie, </ts>
               <ts e="T687" id="Seg_4189" n="e" s="T686">sĭjbə </ts>
               <ts e="T688" id="Seg_4191" n="e" s="T687">ĭzemnie. </ts>
               <ts e="T689" id="Seg_4193" n="e" s="T688">(Bögelbə) </ts>
               <ts e="T690" id="Seg_4195" n="e" s="T689">ĭzemnie. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PKZ">
            <ta e="T8" id="Seg_4196" s="T4">PKZ_1964_SU0206.PKZ.001 (002.001)</ta>
            <ta e="T12" id="Seg_4197" s="T8">PKZ_1964_SU0206.PKZ.002 (002.002)</ta>
            <ta e="T17" id="Seg_4198" s="T12">PKZ_1964_SU0206.PKZ.003 (002.003)</ta>
            <ta e="T20" id="Seg_4199" s="T17">PKZ_1964_SU0206.PKZ.004 (002.004)</ta>
            <ta e="T22" id="Seg_4200" s="T20">PKZ_1964_SU0206.PKZ.005 (002.005)</ta>
            <ta e="T27" id="Seg_4201" s="T22">PKZ_1964_SU0206.PKZ.006 (003.001)</ta>
            <ta e="T29" id="Seg_4202" s="T27">PKZ_1964_SU0206.PKZ.007 (003.002)</ta>
            <ta e="T32" id="Seg_4203" s="T29">PKZ_1964_SU0206.PKZ.008 (003.003)</ta>
            <ta e="T38" id="Seg_4204" s="T32">PKZ_1964_SU0206.PKZ.009 (003.004)</ta>
            <ta e="T42" id="Seg_4205" s="T38">PKZ_1964_SU0206.PKZ.010 (003.005)</ta>
            <ta e="T45" id="Seg_4206" s="T42">PKZ_1964_SU0206.PKZ.011 (003.006)</ta>
            <ta e="T51" id="Seg_4207" s="T45">PKZ_1964_SU0206.PKZ.012 (003.007)</ta>
            <ta e="T53" id="Seg_4208" s="T51">PKZ_1964_SU0206.PKZ.013 (003.008)</ta>
            <ta e="T54" id="Seg_4209" s="T53">PKZ_1964_SU0206.PKZ.014 (003.009)</ta>
            <ta e="T56" id="Seg_4210" s="T54">PKZ_1964_SU0206.PKZ.015 (004.001)</ta>
            <ta e="T59" id="Seg_4211" s="T56">PKZ_1964_SU0206.PKZ.016 (004.002)</ta>
            <ta e="T60" id="Seg_4212" s="T59">PKZ_1964_SU0206.PKZ.017 (004.003)</ta>
            <ta e="T61" id="Seg_4213" s="T60">PKZ_1964_SU0206.PKZ.018 (004.004)</ta>
            <ta e="T63" id="Seg_4214" s="T61">PKZ_1964_SU0206.PKZ.019 (004.005)</ta>
            <ta e="T65" id="Seg_4215" s="T63">PKZ_1964_SU0206.PKZ.020 (004.006)</ta>
            <ta e="T67" id="Seg_4216" s="T65">PKZ_1964_SU0206.PKZ.021 (004.007)</ta>
            <ta e="T68" id="Seg_4217" s="T67">PKZ_1964_SU0206.PKZ.022 (004.008)</ta>
            <ta e="T73" id="Seg_4218" s="T68">PKZ_1964_SU0206.PKZ.023 (005.001)</ta>
            <ta e="T78" id="Seg_4219" s="T73">PKZ_1964_SU0206.PKZ.024 (005.002)</ta>
            <ta e="T81" id="Seg_4220" s="T78">PKZ_1964_SU0206.PKZ.025 (005.003)</ta>
            <ta e="T84" id="Seg_4221" s="T81">PKZ_1964_SU0206.PKZ.026 (005.004)</ta>
            <ta e="T87" id="Seg_4222" s="T84">PKZ_1964_SU0206.PKZ.027 (005.005)</ta>
            <ta e="T90" id="Seg_4223" s="T87">PKZ_1964_SU0206.PKZ.028 (005.006)</ta>
            <ta e="T94" id="Seg_4224" s="T90">PKZ_1964_SU0206.PKZ.029 (005.007)</ta>
            <ta e="T95" id="Seg_4225" s="T94">PKZ_1964_SU0206.PKZ.030 (005.008)</ta>
            <ta e="T97" id="Seg_4226" s="T95">PKZ_1964_SU0206.PKZ.031 (006.001)</ta>
            <ta e="T100" id="Seg_4227" s="T97">PKZ_1964_SU0206.PKZ.032 (006.002)</ta>
            <ta e="T102" id="Seg_4228" s="T100">PKZ_1964_SU0206.PKZ.033 (006.003)</ta>
            <ta e="T104" id="Seg_4229" s="T102">PKZ_1964_SU0206.PKZ.034 (006.004)</ta>
            <ta e="T106" id="Seg_4230" s="T104">PKZ_1964_SU0206.PKZ.035 (006.005)</ta>
            <ta e="T108" id="Seg_4231" s="T106">PKZ_1964_SU0206.PKZ.036 (006.006)</ta>
            <ta e="T118" id="Seg_4232" s="T108">PKZ_1964_SU0206.PKZ.037 (006.007)</ta>
            <ta e="T122" id="Seg_4233" s="T118">PKZ_1964_SU0206.PKZ.038 (006.008)</ta>
            <ta e="T123" id="Seg_4234" s="T122">PKZ_1964_SU0206.PKZ.039 (006.009)</ta>
            <ta e="T125" id="Seg_4235" s="T123">PKZ_1964_SU0206.PKZ.040 (007.001)</ta>
            <ta e="T139" id="Seg_4236" s="T125">PKZ_1964_SU0206.PKZ.041 (007.002)</ta>
            <ta e="T140" id="Seg_4237" s="T139">PKZ_1964_SU0206.PKZ.042 (007.003)</ta>
            <ta e="T143" id="Seg_4238" s="T140">PKZ_1964_SU0206.PKZ.043 (008.001)</ta>
            <ta e="T147" id="Seg_4239" s="T143">PKZ_1964_SU0206.PKZ.044 (008.002)</ta>
            <ta e="T149" id="Seg_4240" s="T147">PKZ_1964_SU0206.PKZ.045 (008.003)</ta>
            <ta e="T154" id="Seg_4241" s="T149">PKZ_1964_SU0206.PKZ.046 (009.001)</ta>
            <ta e="T163" id="Seg_4242" s="T154">PKZ_1964_SU0206.PKZ.047 (009.002)</ta>
            <ta e="T168" id="Seg_4243" s="T163">PKZ_1964_SU0206.PKZ.048 (010.001)</ta>
            <ta e="T170" id="Seg_4244" s="T168">PKZ_1964_SU0206.PKZ.049 (010.002)</ta>
            <ta e="T172" id="Seg_4245" s="T170">PKZ_1964_SU0206.PKZ.050 (010.003)</ta>
            <ta e="T175" id="Seg_4246" s="T172">PKZ_1964_SU0206.PKZ.051 (010.004)</ta>
            <ta e="T176" id="Seg_4247" s="T175">PKZ_1964_SU0206.PKZ.052 (010.005)</ta>
            <ta e="T181" id="Seg_4248" s="T177">PKZ_1964_SU0206.PKZ.053 (012.001)</ta>
            <ta e="T186" id="Seg_4249" s="T181">PKZ_1964_SU0206.PKZ.054 (012.002)</ta>
            <ta e="T190" id="Seg_4250" s="T186">PKZ_1964_SU0206.PKZ.055 (012.003)</ta>
            <ta e="T192" id="Seg_4251" s="T190">PKZ_1964_SU0206.PKZ.056 (012.004)</ta>
            <ta e="T193" id="Seg_4252" s="T192">PKZ_1964_SU0206.PKZ.057 (012.005)</ta>
            <ta e="T195" id="Seg_4253" s="T193">PKZ_1964_SU0206.PKZ.058 (013.001)</ta>
            <ta e="T198" id="Seg_4254" s="T195">PKZ_1964_SU0206.PKZ.059 (013.002)</ta>
            <ta e="T201" id="Seg_4255" s="T198">PKZ_1964_SU0206.PKZ.060 (013.003)</ta>
            <ta e="T203" id="Seg_4256" s="T201">PKZ_1964_SU0206.PKZ.061 (013.004)</ta>
            <ta e="T207" id="Seg_4257" s="T203">PKZ_1964_SU0206.PKZ.062 (013.005)</ta>
            <ta e="T210" id="Seg_4258" s="T207">PKZ_1964_SU0206.PKZ.063 (013.006)</ta>
            <ta e="T213" id="Seg_4259" s="T210">PKZ_1964_SU0206.PKZ.064 (013.007)</ta>
            <ta e="T216" id="Seg_4260" s="T213">PKZ_1964_SU0206.PKZ.065 (014.001)</ta>
            <ta e="T219" id="Seg_4261" s="T216">PKZ_1964_SU0206.PKZ.066 (014.002)</ta>
            <ta e="T222" id="Seg_4262" s="T219">PKZ_1964_SU0206.PKZ.067 (014.003)</ta>
            <ta e="T225" id="Seg_4263" s="T222">PKZ_1964_SU0206.PKZ.068 (014.004)</ta>
            <ta e="T226" id="Seg_4264" s="T225">PKZ_1964_SU0206.PKZ.069 (014.005)</ta>
            <ta e="T229" id="Seg_4265" s="T226">PKZ_1964_SU0206.PKZ.070 (015.001)</ta>
            <ta e="T233" id="Seg_4266" s="T229">PKZ_1964_SU0206.PKZ.071 (015.002)</ta>
            <ta e="T235" id="Seg_4267" s="T233">PKZ_1964_SU0206.PKZ.072 (015.003)</ta>
            <ta e="T241" id="Seg_4268" s="T235">PKZ_1964_SU0206.PKZ.073 (015.004)</ta>
            <ta e="T244" id="Seg_4269" s="T241">PKZ_1964_SU0206.PKZ.074 (016.001)</ta>
            <ta e="T247" id="Seg_4270" s="T244">PKZ_1964_SU0206.PKZ.075 (016.002)</ta>
            <ta e="T251" id="Seg_4271" s="T247">PKZ_1964_SU0206.PKZ.076 (017.001)</ta>
            <ta e="T253" id="Seg_4272" s="T251">PKZ_1964_SU0206.PKZ.077 (017.002)</ta>
            <ta e="T256" id="Seg_4273" s="T253">PKZ_1964_SU0206.PKZ.078 (017.003)</ta>
            <ta e="T264" id="Seg_4274" s="T256">PKZ_1964_SU0206.PKZ.079 (017.004)</ta>
            <ta e="T269" id="Seg_4275" s="T264">PKZ_1964_SU0206.PKZ.080 (017.005)</ta>
            <ta e="T273" id="Seg_4276" s="T269">PKZ_1964_SU0206.PKZ.081 (018.001)</ta>
            <ta e="T278" id="Seg_4277" s="T273">PKZ_1964_SU0206.PKZ.082 (018.002)</ta>
            <ta e="T280" id="Seg_4278" s="T278">PKZ_1964_SU0206.PKZ.083 (018.003)</ta>
            <ta e="T284" id="Seg_4279" s="T280">PKZ_1964_SU0206.PKZ.084 (019)</ta>
            <ta e="T290" id="Seg_4280" s="T284">PKZ_1964_SU0206.PKZ.085 (020.001)</ta>
            <ta e="T292" id="Seg_4281" s="T290">PKZ_1964_SU0206.PKZ.086 (020.002)</ta>
            <ta e="T296" id="Seg_4282" s="T292">PKZ_1964_SU0206.PKZ.087 (020.003)</ta>
            <ta e="T300" id="Seg_4283" s="T296">PKZ_1964_SU0206.PKZ.088 (020.004)</ta>
            <ta e="T303" id="Seg_4284" s="T300">PKZ_1964_SU0206.PKZ.089 (020.005)</ta>
            <ta e="T308" id="Seg_4285" s="T303">PKZ_1964_SU0206.PKZ.090 (020.006)</ta>
            <ta e="T310" id="Seg_4286" s="T309">PKZ_1964_SU0206.PKZ.091 (021)</ta>
            <ta e="T313" id="Seg_4287" s="T310">PKZ_1964_SU0206.PKZ.092 (022.001)</ta>
            <ta e="T316" id="Seg_4288" s="T313">PKZ_1964_SU0206.PKZ.093 (022.002)</ta>
            <ta e="T318" id="Seg_4289" s="T316">PKZ_1964_SU0206.PKZ.094 (022.003)</ta>
            <ta e="T321" id="Seg_4290" s="T318">PKZ_1964_SU0206.PKZ.095 (022.004)</ta>
            <ta e="T325" id="Seg_4291" s="T321">PKZ_1964_SU0206.PKZ.096 (022.005)</ta>
            <ta e="T329" id="Seg_4292" s="T325">PKZ_1964_SU0206.PKZ.097 (022.006)</ta>
            <ta e="T332" id="Seg_4293" s="T329">PKZ_1964_SU0206.PKZ.098 (023)</ta>
            <ta e="T334" id="Seg_4294" s="T332">PKZ_1964_SU0206.PKZ.099 (024.001)</ta>
            <ta e="T336" id="Seg_4295" s="T334">PKZ_1964_SU0206.PKZ.100 (024.002)</ta>
            <ta e="T343" id="Seg_4296" s="T336">PKZ_1964_SU0206.PKZ.101 (024.003)</ta>
            <ta e="T348" id="Seg_4297" s="T343">PKZ_1964_SU0206.PKZ.102 (025.001)</ta>
            <ta e="T353" id="Seg_4298" s="T348">PKZ_1964_SU0206.PKZ.103 (025.002)</ta>
            <ta e="T358" id="Seg_4299" s="T353">PKZ_1964_SU0206.PKZ.104 (026)</ta>
            <ta e="T365" id="Seg_4300" s="T358">PKZ_1964_SU0206.PKZ.105 (027.001)</ta>
            <ta e="T372" id="Seg_4301" s="T365">PKZ_1964_SU0206.PKZ.106 (027.002)</ta>
            <ta e="T376" id="Seg_4302" s="T372">PKZ_1964_SU0206.PKZ.107 (027.003)</ta>
            <ta e="T377" id="Seg_4303" s="T376">PKZ_1964_SU0206.PKZ.108 (027.004)</ta>
            <ta e="T381" id="Seg_4304" s="T377">PKZ_1964_SU0206.PKZ.109 (027.005)</ta>
            <ta e="T389" id="Seg_4305" s="T381">PKZ_1964_SU0206.PKZ.110 (028.001)</ta>
            <ta e="T394" id="Seg_4306" s="T389">PKZ_1964_SU0206.PKZ.111 (028.002)</ta>
            <ta e="T401" id="Seg_4307" s="T394">PKZ_1964_SU0206.PKZ.112 (028.003)</ta>
            <ta e="T406" id="Seg_4308" s="T401">PKZ_1964_SU0206.PKZ.113 (028.004)</ta>
            <ta e="T409" id="Seg_4309" s="T406">PKZ_1964_SU0206.PKZ.114 (028.005)</ta>
            <ta e="T412" id="Seg_4310" s="T409">PKZ_1964_SU0206.PKZ.115 (028.006)</ta>
            <ta e="T414" id="Seg_4311" s="T412">PKZ_1964_SU0206.PKZ.116 (028.007)</ta>
            <ta e="T416" id="Seg_4312" s="T414">PKZ_1964_SU0206.PKZ.117 (028.008)</ta>
            <ta e="T422" id="Seg_4313" s="T416">PKZ_1964_SU0206.PKZ.118 (029.001)</ta>
            <ta e="T424" id="Seg_4314" s="T422">PKZ_1964_SU0206.PKZ.119 (029.002)</ta>
            <ta e="T427" id="Seg_4315" s="T424">PKZ_1964_SU0206.PKZ.120 (029.003)</ta>
            <ta e="T433" id="Seg_4316" s="T427">PKZ_1964_SU0206.PKZ.121 (029.004)</ta>
            <ta e="T435" id="Seg_4317" s="T433">PKZ_1964_SU0206.PKZ.122 (029.005)</ta>
            <ta e="T437" id="Seg_4318" s="T435">PKZ_1964_SU0206.PKZ.123 (029.006)</ta>
            <ta e="T440" id="Seg_4319" s="T437">PKZ_1964_SU0206.PKZ.124 (029.007)</ta>
            <ta e="T442" id="Seg_4320" s="T440">PKZ_1964_SU0206.PKZ.125 (030.001)</ta>
            <ta e="T452" id="Seg_4321" s="T442">PKZ_1964_SU0206.PKZ.126 (030.002)</ta>
            <ta e="T456" id="Seg_4322" s="T452">PKZ_1964_SU0206.PKZ.127 (030.003)</ta>
            <ta e="T458" id="Seg_4323" s="T456">PKZ_1964_SU0206.PKZ.128 (030.004)</ta>
            <ta e="T461" id="Seg_4324" s="T458">PKZ_1964_SU0206.PKZ.129 (030.005)</ta>
            <ta e="T465" id="Seg_4325" s="T461">PKZ_1964_SU0206.PKZ.130 (031.001)</ta>
            <ta e="T470" id="Seg_4326" s="T467">PKZ_1964_SU0206.PKZ.131 (031.003)</ta>
            <ta e="T477" id="Seg_4327" s="T472">PKZ_1964_SU0206.PKZ.132 (031.005)</ta>
            <ta e="T480" id="Seg_4328" s="T477">PKZ_1964_SU0206.PKZ.133 (031.006)</ta>
            <ta e="T486" id="Seg_4329" s="T480">PKZ_1964_SU0206.PKZ.134 (031.007)</ta>
            <ta e="T489" id="Seg_4330" s="T487">PKZ_1964_SU0206.PKZ.135 (031.009)</ta>
            <ta e="T493" id="Seg_4331" s="T489">PKZ_1964_SU0206.PKZ.136 (031.010)</ta>
            <ta e="T496" id="Seg_4332" s="T493">PKZ_1964_SU0206.PKZ.137 (031.011)</ta>
            <ta e="T502" id="Seg_4333" s="T496">PKZ_1964_SU0206.PKZ.138 (031.012)</ta>
            <ta e="T504" id="Seg_4334" s="T502">PKZ_1964_SU0206.PKZ.139 (031.013)</ta>
            <ta e="T507" id="Seg_4335" s="T504">PKZ_1964_SU0206.PKZ.140 (031.014)</ta>
            <ta e="T513" id="Seg_4336" s="T507">PKZ_1964_SU0206.PKZ.141 (031.015)</ta>
            <ta e="T519" id="Seg_4337" s="T513">PKZ_1964_SU0206.PKZ.142 (032.001)</ta>
            <ta e="T522" id="Seg_4338" s="T519">PKZ_1964_SU0206.PKZ.143 (032.002)</ta>
            <ta e="T526" id="Seg_4339" s="T522">PKZ_1964_SU0206.PKZ.144 (032.003)</ta>
            <ta e="T532" id="Seg_4340" s="T526">PKZ_1964_SU0206.PKZ.145 (032.004)</ta>
            <ta e="T536" id="Seg_4341" s="T532">PKZ_1964_SU0206.PKZ.146 (033.001)</ta>
            <ta e="T541" id="Seg_4342" s="T536">PKZ_1964_SU0206.PKZ.147 (033.002)</ta>
            <ta e="T546" id="Seg_4343" s="T541">PKZ_1964_SU0206.PKZ.148 (033.003)</ta>
            <ta e="T549" id="Seg_4344" s="T546">PKZ_1964_SU0206.PKZ.149 (033.004)</ta>
            <ta e="T552" id="Seg_4345" s="T549">PKZ_1964_SU0206.PKZ.150 (033.005)</ta>
            <ta e="T553" id="Seg_4346" s="T552">PKZ_1964_SU0206.PKZ.151 (033.006)</ta>
            <ta e="T555" id="Seg_4347" s="T553">PKZ_1964_SU0206.PKZ.152 (033.007)</ta>
            <ta e="T557" id="Seg_4348" s="T555">PKZ_1964_SU0206.PKZ.153 (033.008)</ta>
            <ta e="T564" id="Seg_4349" s="T557">PKZ_1964_SU0206.PKZ.154 (033.009)</ta>
            <ta e="T573" id="Seg_4350" s="T564">PKZ_1964_SU0206.PKZ.155 (033.010)</ta>
            <ta e="T577" id="Seg_4351" s="T573">PKZ_1964_SU0206.PKZ.156 (033.011)</ta>
            <ta e="T581" id="Seg_4352" s="T577">PKZ_1964_SU0206.PKZ.157 (034.001)</ta>
            <ta e="T585" id="Seg_4353" s="T581">PKZ_1964_SU0206.PKZ.158 (034.002)</ta>
            <ta e="T588" id="Seg_4354" s="T585">PKZ_1964_SU0206.PKZ.159 (034.003)</ta>
            <ta e="T592" id="Seg_4355" s="T588">PKZ_1964_SU0206.PKZ.160 (034.004)</ta>
            <ta e="T595" id="Seg_4356" s="T592">PKZ_1964_SU0206.PKZ.161 (034.005)</ta>
            <ta e="T597" id="Seg_4357" s="T595">PKZ_1964_SU0206.PKZ.162 (035.001)</ta>
            <ta e="T601" id="Seg_4358" s="T597">PKZ_1964_SU0206.PKZ.163 (035.002)</ta>
            <ta e="T603" id="Seg_4359" s="T601">PKZ_1964_SU0206.PKZ.164 (035.003)</ta>
            <ta e="T611" id="Seg_4360" s="T603">PKZ_1964_SU0206.PKZ.165 (035.004)</ta>
            <ta e="T620" id="Seg_4361" s="T611">PKZ_1964_SU0206.PKZ.166 (035.005)</ta>
            <ta e="T630" id="Seg_4362" s="T620">PKZ_1964_SU0206.PKZ.167 (035.006)</ta>
            <ta e="T632" id="Seg_4363" s="T630">PKZ_1964_SU0206.PKZ.168 (036.001)</ta>
            <ta e="T634" id="Seg_4364" s="T632">PKZ_1964_SU0206.PKZ.169 (036.002)</ta>
            <ta e="T639" id="Seg_4365" s="T634">PKZ_1964_SU0206.PKZ.170 (036.003)</ta>
            <ta e="T643" id="Seg_4366" s="T639">PKZ_1964_SU0206.PKZ.171 (036.004)</ta>
            <ta e="T646" id="Seg_4367" s="T643">PKZ_1964_SU0206.PKZ.172 (036.005)</ta>
            <ta e="T649" id="Seg_4368" s="T646">PKZ_1964_SU0206.PKZ.173 (036.006)</ta>
            <ta e="T653" id="Seg_4369" s="T649">PKZ_1964_SU0206.PKZ.174 (036.007)</ta>
            <ta e="T658" id="Seg_4370" s="T653">PKZ_1964_SU0206.PKZ.175 (037.001)</ta>
            <ta e="T663" id="Seg_4371" s="T658">PKZ_1964_SU0206.PKZ.176 (037.002)</ta>
            <ta e="T670" id="Seg_4372" s="T663">PKZ_1964_SU0206.PKZ.177 (037.003)</ta>
            <ta e="T672" id="Seg_4373" s="T670">PKZ_1964_SU0206.PKZ.178 (037.004)</ta>
            <ta e="T674" id="Seg_4374" s="T672">PKZ_1964_SU0206.PKZ.179 (037.005)</ta>
            <ta e="T676" id="Seg_4375" s="T674">PKZ_1964_SU0206.PKZ.180 (037.006)</ta>
            <ta e="T678" id="Seg_4376" s="T676">PKZ_1964_SU0206.PKZ.181 (037.007)</ta>
            <ta e="T680" id="Seg_4377" s="T678">PKZ_1964_SU0206.PKZ.182 (037.008)</ta>
            <ta e="T682" id="Seg_4378" s="T680">PKZ_1964_SU0206.PKZ.183 (037.009)</ta>
            <ta e="T684" id="Seg_4379" s="T682">PKZ_1964_SU0206.PKZ.184 (037.010)</ta>
            <ta e="T688" id="Seg_4380" s="T684">PKZ_1964_SU0206.PKZ.185 (037.011)</ta>
            <ta e="T690" id="Seg_4381" s="T688">PKZ_1964_SU0206.PKZ.186 (037.012)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PKZ">
            <ta e="T8" id="Seg_4382" s="T4">Nezeŋ ugaːndə iʔgö šonəgaʔi. </ta>
            <ta e="T12" id="Seg_4383" s="T8">Măn mămbiam: gijen ibileʔ? </ta>
            <ta e="T17" id="Seg_4384" s="T12">Dĭzeŋ mănliaʔi: tüžöj surdəsʼtə mĭmbibeʔ. </ta>
            <ta e="T20" id="Seg_4385" s="T17">Deʔkeʔ măna sut! </ta>
            <ta e="T22" id="Seg_4386" s="T20">Kabarləj daška. </ta>
            <ta e="T27" id="Seg_4387" s="T22">Ešši bar tüʔpi, ugaːndə sagər. </ta>
            <ta e="T29" id="Seg_4388" s="T27">Bar koluʔpi. </ta>
            <ta e="T32" id="Seg_4389" s="T29">Nada bazəsʼtə dĭm. </ta>
            <ta e="T38" id="Seg_4390" s="T32">(Dĭ -) Kĭškəsʼtə, dĭgəttə bazəsʼtə nada. </ta>
            <ta e="T42" id="Seg_4391" s="T38">(Am -) Kürzittə nada. </ta>
            <ta e="T45" id="Seg_4392" s="T42">Dĭgəttə amzittə mĭzittə. </ta>
            <ta e="T51" id="Seg_4393" s="T45">Dĭgəttə kunolzittə (iʔbəs -) (iʔbətʼsʼiʔ) nada. </ta>
            <ta e="T53" id="Seg_4394" s="T51">Ordəʔ eššim! </ta>
            <ta e="T54" id="Seg_4395" s="T53">Baːr! </ta>
            <ta e="T56" id="Seg_4396" s="T54">Amnoʔ turagən! </ta>
            <ta e="T59" id="Seg_4397" s="T56">Măn nʼiʔdə kallam! </ta>
            <ta e="T60" id="Seg_4398" s="T59">Kĭnzəsʼtə. </ta>
            <ta e="T61" id="Seg_4399" s="T60">Tüʔsittə. </ta>
            <ta e="T63" id="Seg_4400" s="T61">Dĭgəttə šolam. </ta>
            <ta e="T65" id="Seg_4401" s="T63">Amnolam tănziʔ. </ta>
            <ta e="T67" id="Seg_4402" s="T65">I dʼăbaktərləbaʔ! </ta>
            <ta e="T68" id="Seg_4403" s="T67">Kabarləj. </ta>
            <ta e="T73" id="Seg_4404" s="T68">Măn kötenbə kuvas tăn kadəldə. </ta>
            <ta e="T78" id="Seg_4405" s="T73">(Uba -) Ugaːndə nʼešpək köten. </ta>
            <ta e="T81" id="Seg_4406" s="T78">Ugaːndə sagər köten. </ta>
            <ta e="T84" id="Seg_4407" s="T81">Ugaːndə komu köten. </ta>
            <ta e="T87" id="Seg_4408" s="T84">Ugaːndə sĭre köten. </ta>
            <ta e="T90" id="Seg_4409" s="T87">Ugaːndə nʼešpək köten. </ta>
            <ta e="T94" id="Seg_4410" s="T90">Ugaːndə todam (kötek-) köten. </ta>
            <ta e="T95" id="Seg_4411" s="T94">Kabarləj. </ta>
            <ta e="T97" id="Seg_4412" s="T95">Ešši šonəgal. </ta>
            <ta e="T100" id="Seg_4413" s="T97">Udazaŋdə bar balgaš. </ta>
            <ta e="T102" id="Seg_4414" s="T100">Nada bazəsʼtə. </ta>
            <ta e="T104" id="Seg_4415" s="T102">Šoʔ döbər! </ta>
            <ta e="T106" id="Seg_4416" s="T104">Iʔ dʼoraʔ! </ta>
            <ta e="T108" id="Seg_4417" s="T106">Kanaʔ maʔnəl! </ta>
            <ta e="T118" id="Seg_4418" s="T108">(Tăna -) Tănan padʼi (mondro - mando -) măndolaʔbəʔjə bar. </ta>
            <ta e="T122" id="Seg_4419" s="T118">A to nabəʔi (bargamunəʔi) tănan. </ta>
            <ta e="T123" id="Seg_4420" s="T122">Bar. </ta>
            <ta e="T125" id="Seg_4421" s="T123">Gibər (paʔlaːngəl). </ta>
            <ta e="T139" id="Seg_4422" s="T125">(Iʔ pănaʔ - i - iʔ păʔ - iʔ păllaʔ - iʔ pănaʔgaʔ -). </ta>
            <ta e="T140" id="Seg_4423" s="T139">Господи. </ta>
            <ta e="T143" id="Seg_4424" s="T140">Gibər (pă-) paʔlaːndəgal? </ta>
            <ta e="T147" id="Seg_4425" s="T143">Iʔ pădaʔ, a to saʔməluʔləl! </ta>
            <ta e="T149" id="Seg_4426" s="T147">Šoʔ döbər! </ta>
            <ta e="T154" id="Seg_4427" s="T149">Dĭ koʔbdo amnolaʔbə, todam bar. </ta>
            <ta e="T163" id="Seg_4428" s="T154">(M e ej m) Ĭmbi măndəliam, dĭ ej nʼilgölaʔbə. </ta>
            <ta e="T168" id="Seg_4429" s="T163">Dʼijenə (kandləʔ -) kandəgaʔi ineʔsiʔ. </ta>
            <ta e="T170" id="Seg_4430" s="T168">Kola dʼabəlaʔi. </ta>
            <ta e="T172" id="Seg_4431" s="T170">Kola deʔleʔi. </ta>
            <ta e="T175" id="Seg_4432" s="T172">Măna možet mĭleʔi. </ta>
            <ta e="T176" id="Seg_4433" s="T175">Kabarləj. </ta>
            <ta e="T181" id="Seg_4434" s="T177">Dʼagagən udaziʔ kola dʼaːpiʔi. </ta>
            <ta e="T186" id="Seg_4435" s="T181">(Dʼünə) Dʼünə (kə-) embiʔi. </ta>
            <ta e="T190" id="Seg_4436" s="T186">Dĭgəttə (tuzʼdʼa -) tustʼarbiʔi. </ta>
            <ta e="T192" id="Seg_4437" s="T190">Dĭgəttə sadarbiʔi. </ta>
            <ta e="T193" id="Seg_4438" s="T192">Kabar. </ta>
            <ta e="T195" id="Seg_4439" s="T193">Šĭšəge molaːmbi. ((DMG))</ta>
            <ta e="T198" id="Seg_4440" s="T195">Ejü kalla dʼürbi. </ta>
            <ta e="T201" id="Seg_4441" s="T198">Kuja amga măndolaʔbə. </ta>
            <ta e="T203" id="Seg_4442" s="T201">Sĭre šonəga. </ta>
            <ta e="T207" id="Seg_4443" s="T203">Beržə bar ugaːndə šĭšəge. </ta>
            <ta e="T210" id="Seg_4444" s="T207">Măn pa iʔgö. </ta>
            <ta e="T213" id="Seg_4445" s="T210">Dĭ könə kabarləj. </ta>
            <ta e="T216" id="Seg_4446" s="T213">Ĭmbi ulul tĭlliel? </ta>
            <ta e="T219" id="Seg_4447" s="T216">Padʼi unuʔi ige. </ta>
            <ta e="T222" id="Seg_4448" s="T219">Ugaːndə iʔgö unu. </ta>
            <ta e="T225" id="Seg_4449" s="T222">Kuʔsʼittə nada dĭzeŋ. </ta>
            <ta e="T226" id="Seg_4450" s="T225">Kabarləj. </ta>
            <ta e="T229" id="Seg_4451" s="T226">Măn iʔbəbiem kunolzittə. </ta>
            <ta e="T233" id="Seg_4452" s="T229">Kuriza ej mĭbi kunolzittə. </ta>
            <ta e="T235" id="Seg_4453" s="T233">Măn uʔbdəbiam. </ta>
            <ta e="T241" id="Seg_4454" s="T235">I (da -) daška ej iʔbiem. </ta>
            <ta e="T244" id="Seg_4455" s="T241">Kuvas (nüke) amnolaʔbə. </ta>
            <ta e="T247" id="Seg_4456" s="T244">Kuvas nʼi amnolaʔbə. </ta>
            <ta e="T251" id="Seg_4457" s="T247">Măn ugaːndə dʼaktə molaːmbiam. </ta>
            <ta e="T253" id="Seg_4458" s="T251">Šidegöʔ malaʔpibaʔ. </ta>
            <ta e="T256" id="Seg_4459" s="T253">Măn da kagam. </ta>
            <ta e="T264" id="Seg_4460" s="T256">(Dĭ - a -) Dĭzeŋ bar külaːmbiʔi bar. </ta>
            <ta e="T269" id="Seg_4461" s="T264">Ну, наверное все таперь уже. </ta>
            <ta e="T273" id="Seg_4462" s="T269">Iʔgö pʼe kambi bar. </ta>
            <ta e="T278" id="Seg_4463" s="T273">Bor dʼünə (am -) amnolial. </ta>
            <ta e="T280" id="Seg_4464" s="T278">Surno moləj. </ta>
            <ta e="T284" id="Seg_4465" s="T280">Udam dʼükəmnie, nada kădəsʼtə. </ta>
            <ta e="T290" id="Seg_4466" s="T284">Tăn ugaːndə (š -) šĭkel numo. </ta>
            <ta e="T292" id="Seg_4467" s="T290">Kudonzlial tăn. </ta>
            <ta e="T296" id="Seg_4468" s="T292">Tăn tĭmel ugaːndə numo. </ta>
            <ta e="T300" id="Seg_4469" s="T296">Püjel (nuŋo -) numo. </ta>
            <ta e="T303" id="Seg_4470" s="T300">Kuʔlə bar mʼaŋŋaʔbə. </ta>
            <ta e="T308" id="Seg_4471" s="T303">(Monzaŋbə guan -) Monzaŋdə nʼešpək. </ta>
            <ta e="T310" id="Seg_4472" s="T309">Ну-ну-ну. </ta>
            <ta e="T313" id="Seg_4473" s="T310">Ugaːndə eʔbdəl numo. </ta>
            <ta e="T316" id="Seg_4474" s="T313">Ugaːndə kuzaŋdə numo. </ta>
            <ta e="T318" id="Seg_4475" s="T316">Sʼimal urgo. </ta>
            <ta e="T321" id="Seg_4476" s="T318">Ulul ugaːndə urgo. </ta>
            <ta e="T325" id="Seg_4477" s="T321">(Uda -) Udazaŋdə numo. </ta>
            <ta e="T329" id="Seg_4478" s="T325">(Uju -) Ujuzaŋdə numo. </ta>
            <ta e="T332" id="Seg_4479" s="T329">Bostə bar todam. </ta>
            <ta e="T334" id="Seg_4480" s="T332">Müjö nʼergölaʔbə. </ta>
            <ta e="T336" id="Seg_4481" s="T334">Panə amnobi. </ta>
            <ta e="T343" id="Seg_4482" s="T336">Iʔgö (nüjö - nu -) süjöʔi nʼergölaʔbəʔjə. </ta>
            <ta e="T348" id="Seg_4483" s="T343">Karatʼgaj bar (nʼiʔgə -) nʼilgölaʔbə. </ta>
            <ta e="T353" id="Seg_4484" s="T348">Pogəraš nʼilgölaʔ (s -) šobi. </ta>
            <ta e="T358" id="Seg_4485" s="T353">Kuriza bar kadəʔlia, amzittə măndərlia. </ta>
            <ta e="T365" id="Seg_4486" s="T358">Nöməlbiom bar (budə -) büm nuldəsʼtə pʼeːšdə. </ta>
            <ta e="T372" id="Seg_4487" s="T365">Dʼübige bü naga, (na -) măna kereʔ. </ta>
            <ta e="T376" id="Seg_4488" s="T372">Oroma (kămn -) kămnəlim. </ta>
            <ta e="T377" id="Seg_4489" s="T376">Bargarim. </ta>
            <ta e="T381" id="Seg_4490" s="T377">Dĭgəttə bazəsʼtə nada takšeʔi. </ta>
            <ta e="T389" id="Seg_4491" s="T381">Sĭre pa, šomne pa, komu pa, san pa. </ta>
            <ta e="T394" id="Seg_4492" s="T389">(Sagən-) pagən bar šiškaʔi özerleʔbəʔjə. </ta>
            <ta e="T401" id="Seg_4493" s="T394">San (pag -) pagən bar šiškaʔi amnolaʔbəʔjə. </ta>
            <ta e="T406" id="Seg_4494" s="T401">Sĭre pagən bar takšə alaʔbəʔjə. </ta>
            <ta e="T409" id="Seg_4495" s="T406">Segi bü bĭtleʔbəʔjə. </ta>
            <ta e="T412" id="Seg_4496" s="T409">(Paj) Paj šamnak. </ta>
            <ta e="T414" id="Seg_4497" s="T412">Bazaj šamnak. </ta>
            <ta e="T416" id="Seg_4498" s="T414">Takšə bazaj. </ta>
            <ta e="T422" id="Seg_4499" s="T416">Ugaːndə paʔi iʔgö, dĭzeŋ nada jaʔsʼittə. </ta>
            <ta e="T424" id="Seg_4500" s="T422">Dĭgəttə (kürzittə). </ta>
            <ta e="T427" id="Seg_4501" s="T424">Dĭgəttə tura (kazittə). </ta>
            <ta e="T433" id="Seg_4502" s="T427">Dĭgəttə dĭ paʔizʼiʔ tura jaʔsʼittə nada. </ta>
            <ta e="T435" id="Seg_4503" s="T433">Amnozʼittə turagən. </ta>
            <ta e="T437" id="Seg_4504" s="T435">Pʼeːš enzittə. </ta>
            <ta e="T440" id="Seg_4505" s="T437">Pʼeːšgən pürzittə ipek. </ta>
            <ta e="T442" id="Seg_4506" s="T440">Bĭdeʔ bü! </ta>
            <ta e="T452" id="Seg_4507" s="T442">Măn dibər (kăm -) embiem sĭreʔpne, (bü -) bü kămnabiam. </ta>
            <ta e="T456" id="Seg_4508" s="T452">(Na -) Namzəbi bar. </ta>
            <ta e="T458" id="Seg_4509" s="T456">Ipek endəbiem. </ta>
            <ta e="T461" id="Seg_4510" s="T458">Bĭdeʔ, ugaːndə jakšə. </ta>
            <ta e="T465" id="Seg_4511" s="T461">Šiʔ taldʼen ara bĭʔpileʔ. </ta>
            <ta e="T470" id="Seg_4512" s="T467">Jezerik ibileʔ bar? </ta>
            <ta e="T477" id="Seg_4513" s="T472">Teinen bar в похмелье. </ta>
            <ta e="T480" id="Seg_4514" s="T477">Bĭdeʔ komu bü. </ta>
            <ta e="T486" id="Seg_4515" s="T480">Măn dibər (sin -) sĭreʔpne embiem. </ta>
            <ta e="T489" id="Seg_4516" s="T487">Bü kămnəbiam. </ta>
            <ta e="T493" id="Seg_4517" s="T489">(Namzə -) Namzəga molaːmbi. </ta>
            <ta e="T496" id="Seg_4518" s="T493">Jakšə moləj tănan. </ta>
            <ta e="T502" id="Seg_4519" s="T496">Ara iʔ bĭdeʔ, a to ulul ĭzemnie. </ta>
            <ta e="T504" id="Seg_4520" s="T502">Sĭjdə ĭzemnie. </ta>
            <ta e="T507" id="Seg_4521" s="T504">Bĭtlel dak, külalləl. </ta>
            <ta e="T513" id="Seg_4522" s="T507">A ej bĭtlel dak kondʼo amnolal. </ta>
            <ta e="T519" id="Seg_4523" s="T513">Mašina (bɨl - tojir -) (talerlaʔbə). </ta>
            <ta e="T522" id="Seg_4524" s="T519">Dĭn kuza amnolaʔbə. </ta>
            <ta e="T526" id="Seg_4525" s="T522">Mašinagəʔ bar bər šonəga. </ta>
            <ta e="T532" id="Seg_4526" s="T526">Dĭ bar dibər (šo-) döbər kallaʔbə. </ta>
            <ta e="T536" id="Seg_4527" s="T532">Abam, ijam koʔbdo kubiʔi. </ta>
            <ta e="T541" id="Seg_4528" s="T536">(Măn) Măna mănliaʔi: kanaʔ dĭzʼiʔ! </ta>
            <ta e="T546" id="Seg_4529" s="T541">(Tĭmne -) Tĭmnit, girgit di. </ta>
            <ta e="T549" id="Seg_4530" s="T546">Dĭgəttə tibinə iliel. </ta>
            <ta e="T552" id="Seg_4531" s="T549">Dĭgəttə esseŋdə moləʔjə. </ta>
            <ta e="T553" id="Seg_4532" s="T552">Погоди. ((DMG)). </ta>
            <ta e="T555" id="Seg_4533" s="T553">Esseŋlə özerleʔjə. </ta>
            <ta e="T557" id="Seg_4534" s="T555">Tănan kabažarlaʔjə. </ta>
            <ta e="T564" id="Seg_4535" s="T557">Nem (ə - i -) iʔ münöraʔ! </ta>
            <ta e="T573" id="Seg_4536" s="T564">(Nʼilgöt ab - abat -) Abam da ijam nʼilgöt! </ta>
            <ta e="T577" id="Seg_4537" s="T573">Все ли, что ли. ((DMG))</ta>
            <ta e="T581" id="Seg_4538" s="T577">Teinen bar kalla dʼürbiʔi. </ta>
            <ta e="T585" id="Seg_4539" s="T581">Abat, ijat kalla dʼürbiʔi. </ta>
            <ta e="T588" id="Seg_4540" s="T585">Esseŋ kalla dʼürbiʔi. </ta>
            <ta e="T592" id="Seg_4541" s="T588">Măn unnʼa maʔnal amnolaʔbəm. </ta>
            <ta e="T595" id="Seg_4542" s="T592">Nada dʼü tĭlzittə. </ta>
            <ta e="T597" id="Seg_4543" s="T595">Kuza külaːmbi. </ta>
            <ta e="T601" id="Seg_4544" s="T597">Nada dĭm dibər enzittə. </ta>
            <ta e="T603" id="Seg_4545" s="T601">Dĭgəttə kămnasʼtə. </ta>
            <ta e="T611" id="Seg_4546" s="T603">(Dʼo -) dĭgəttə (tʼu - dʼo - dʼora). </ta>
            <ta e="T620" id="Seg_4547" s="T611">Dĭgəttə ((PAUSE)) ((DMG)) Dĭgəttə (dujzʼ -) dʼüzʼiʔ nada kajzittə dĭm. </ta>
            <ta e="T630" id="Seg_4548" s="T620">Dĭgəttə maʔnal (su -) šozittə, amzittə, (amo -) amnaʔ amzittə. </ta>
            <ta e="T632" id="Seg_4549" s="T630">Kuza ĭzembi. </ta>
            <ta e="T634" id="Seg_4550" s="T632">Šamandə mĭmbi. </ta>
            <ta e="T639" id="Seg_4551" s="T634">Šaman ej möbi dĭm dʼazirzittə. </ta>
            <ta e="T643" id="Seg_4552" s="T639">Все-таки külaːmbi. </ta>
            <ta e="T646" id="Seg_4553" s="T643">Kumən ej ĭzembi. </ta>
            <ta e="T649" id="Seg_4554" s="T646">Tüj bar iʔbolaʔbə. </ta>
            <ta e="T653" id="Seg_4555" s="T649">Ĭmbizʼiʔdə ej mĭngəlie. </ta>
            <ta e="T658" id="Seg_4556" s="T653">Măna bar münörbiʔi ugaːndə tăŋ. </ta>
            <ta e="T663" id="Seg_4557" s="T658">Bospə ej tĭmnem, ĭmbizʼiʔ münörbiʔi. </ta>
            <ta e="T670" id="Seg_4558" s="T663">(Baram -) Măn, măn bar ĭzembi, ĭzemniem. </ta>
            <ta e="T672" id="Seg_4559" s="T670">Ulum ĭzemnie. </ta>
            <ta e="T674" id="Seg_4560" s="T672">Kum ĭzemnie. </ta>
            <ta e="T676" id="Seg_4561" s="T674">Sʼimam ĭzemnie. </ta>
            <ta e="T678" id="Seg_4562" s="T676">Tĭmem ĭzemnie. </ta>
            <ta e="T680" id="Seg_4563" s="T678">Püjem ĭzemnie. </ta>
            <ta e="T682" id="Seg_4564" s="T680">Udam ĭzemnie. </ta>
            <ta e="T684" id="Seg_4565" s="T682">Ujum ĭzemnie. </ta>
            <ta e="T688" id="Seg_4566" s="T684">Nanəm ĭzemnie, sĭjbə ĭzemnie. </ta>
            <ta e="T690" id="Seg_4567" s="T688">(Bögelbə) ĭzemnie. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-PKZ">
            <ta e="T5" id="Seg_4568" s="T4">ne-zeŋ</ta>
            <ta e="T6" id="Seg_4569" s="T5">ugaːndə</ta>
            <ta e="T7" id="Seg_4570" s="T6">iʔgö</ta>
            <ta e="T8" id="Seg_4571" s="T7">šonə-ga-ʔi</ta>
            <ta e="T9" id="Seg_4572" s="T8">măn</ta>
            <ta e="T10" id="Seg_4573" s="T9">măm-bia-m</ta>
            <ta e="T11" id="Seg_4574" s="T10">gijen</ta>
            <ta e="T12" id="Seg_4575" s="T11">i-bi-leʔ</ta>
            <ta e="T13" id="Seg_4576" s="T12">dĭ-zeŋ</ta>
            <ta e="T14" id="Seg_4577" s="T13">măn-lia-ʔi</ta>
            <ta e="T15" id="Seg_4578" s="T14">tüžöj</ta>
            <ta e="T16" id="Seg_4579" s="T15">surdə-sʼtə</ta>
            <ta e="T17" id="Seg_4580" s="T16">mĭm-bi-beʔ</ta>
            <ta e="T18" id="Seg_4581" s="T17">deʔ-keʔ</ta>
            <ta e="T19" id="Seg_4582" s="T18">măna</ta>
            <ta e="T20" id="Seg_4583" s="T19">sut</ta>
            <ta e="T21" id="Seg_4584" s="T20">kabarləj</ta>
            <ta e="T22" id="Seg_4585" s="T21">daška</ta>
            <ta e="T23" id="Seg_4586" s="T22">ešši</ta>
            <ta e="T24" id="Seg_4587" s="T23">bar</ta>
            <ta e="T25" id="Seg_4588" s="T24">tüʔ-pi</ta>
            <ta e="T26" id="Seg_4589" s="T25">ugaːndə</ta>
            <ta e="T27" id="Seg_4590" s="T26">sagər</ta>
            <ta e="T28" id="Seg_4591" s="T27">bar</ta>
            <ta e="T29" id="Seg_4592" s="T28">ko-luʔ-pi</ta>
            <ta e="T30" id="Seg_4593" s="T29">nada</ta>
            <ta e="T31" id="Seg_4594" s="T30">bazə-sʼtə</ta>
            <ta e="T32" id="Seg_4595" s="T31">dĭ-m</ta>
            <ta e="T35" id="Seg_4596" s="T34">kĭškə-sʼtə</ta>
            <ta e="T36" id="Seg_4597" s="T35">dĭgəttə</ta>
            <ta e="T37" id="Seg_4598" s="T36">bazə-sʼtə</ta>
            <ta e="T38" id="Seg_4599" s="T37">nada</ta>
            <ta e="T41" id="Seg_4600" s="T40">kür-zittə</ta>
            <ta e="T42" id="Seg_4601" s="T41">nada</ta>
            <ta e="T43" id="Seg_4602" s="T42">dĭgəttə</ta>
            <ta e="T44" id="Seg_4603" s="T43">am-zittə</ta>
            <ta e="T45" id="Seg_4604" s="T44">mĭ-zittə</ta>
            <ta e="T46" id="Seg_4605" s="T45">dĭgəttə</ta>
            <ta e="T47" id="Seg_4606" s="T46">kunol-zittə</ta>
            <ta e="T50" id="Seg_4607" s="T49">iʔbətʼsʼiʔ</ta>
            <ta e="T51" id="Seg_4608" s="T50">nada</ta>
            <ta e="T52" id="Seg_4609" s="T51">ordə-ʔ</ta>
            <ta e="T53" id="Seg_4610" s="T52">ešši-m</ta>
            <ta e="T54" id="Seg_4611" s="T53">baːr</ta>
            <ta e="T55" id="Seg_4612" s="T54">amno-ʔ</ta>
            <ta e="T56" id="Seg_4613" s="T55">tura-gən</ta>
            <ta e="T57" id="Seg_4614" s="T56">măn</ta>
            <ta e="T58" id="Seg_4615" s="T57">nʼiʔdə</ta>
            <ta e="T59" id="Seg_4616" s="T58">kal-la-m</ta>
            <ta e="T60" id="Seg_4617" s="T59">kĭnzə-sʼtə</ta>
            <ta e="T61" id="Seg_4618" s="T60">tüʔ-sittə</ta>
            <ta e="T62" id="Seg_4619" s="T61">dĭgəttə</ta>
            <ta e="T63" id="Seg_4620" s="T62">šo-la-m</ta>
            <ta e="T64" id="Seg_4621" s="T63">amno-la-m</ta>
            <ta e="T65" id="Seg_4622" s="T64">tăn-ziʔ</ta>
            <ta e="T66" id="Seg_4623" s="T65">i</ta>
            <ta e="T67" id="Seg_4624" s="T66">dʼăbaktər-lə-baʔ</ta>
            <ta e="T68" id="Seg_4625" s="T67">kabarləj</ta>
            <ta e="T69" id="Seg_4626" s="T68">măn</ta>
            <ta e="T70" id="Seg_4627" s="T69">köten-bə</ta>
            <ta e="T71" id="Seg_4628" s="T70">kuvas</ta>
            <ta e="T72" id="Seg_4629" s="T71">tăn</ta>
            <ta e="T73" id="Seg_4630" s="T72">kadəl-də</ta>
            <ta e="T76" id="Seg_4631" s="T75">ugaːndə</ta>
            <ta e="T77" id="Seg_4632" s="T76">nʼešpək</ta>
            <ta e="T78" id="Seg_4633" s="T77">köten</ta>
            <ta e="T79" id="Seg_4634" s="T78">ugaːndə</ta>
            <ta e="T80" id="Seg_4635" s="T79">sagər</ta>
            <ta e="T81" id="Seg_4636" s="T80">köten</ta>
            <ta e="T82" id="Seg_4637" s="T81">ugaːndə</ta>
            <ta e="T83" id="Seg_4638" s="T82">komu</ta>
            <ta e="T84" id="Seg_4639" s="T83">köten</ta>
            <ta e="T85" id="Seg_4640" s="T84">ugaːndə</ta>
            <ta e="T86" id="Seg_4641" s="T85">sĭre</ta>
            <ta e="T87" id="Seg_4642" s="T86">köten</ta>
            <ta e="T88" id="Seg_4643" s="T87">ugaːndə</ta>
            <ta e="T89" id="Seg_4644" s="T88">nʼešpək</ta>
            <ta e="T90" id="Seg_4645" s="T89">köten</ta>
            <ta e="T91" id="Seg_4646" s="T90">ugaːndə</ta>
            <ta e="T92" id="Seg_4647" s="T91">todam</ta>
            <ta e="T94" id="Seg_4648" s="T93">köten</ta>
            <ta e="T95" id="Seg_4649" s="T94">kabarləj</ta>
            <ta e="T96" id="Seg_4650" s="T95">ešši</ta>
            <ta e="T97" id="Seg_4651" s="T96">šonə-ga-l</ta>
            <ta e="T98" id="Seg_4652" s="T97">uda-zaŋ-də</ta>
            <ta e="T99" id="Seg_4653" s="T98">bar</ta>
            <ta e="T100" id="Seg_4654" s="T99">balgaš</ta>
            <ta e="T101" id="Seg_4655" s="T100">nada</ta>
            <ta e="T102" id="Seg_4656" s="T101">bazə-sʼtə</ta>
            <ta e="T103" id="Seg_4657" s="T102">šo-ʔ</ta>
            <ta e="T104" id="Seg_4658" s="T103">döbər</ta>
            <ta e="T105" id="Seg_4659" s="T104">i-ʔ</ta>
            <ta e="T106" id="Seg_4660" s="T105">dʼor-a-ʔ</ta>
            <ta e="T107" id="Seg_4661" s="T106">kan-a-ʔ</ta>
            <ta e="T108" id="Seg_4662" s="T107">maʔ-nə-l</ta>
            <ta e="T111" id="Seg_4663" s="T110">tănan</ta>
            <ta e="T112" id="Seg_4664" s="T111">padʼi</ta>
            <ta e="T117" id="Seg_4665" s="T116">măndo-laʔbə-ʔjə</ta>
            <ta e="T118" id="Seg_4666" s="T117">bar</ta>
            <ta e="T119" id="Seg_4667" s="T118">ato</ta>
            <ta e="T120" id="Seg_4668" s="T119">nabə-ʔi</ta>
            <ta e="T121" id="Seg_4669" s="T120">bargamunə-ʔi</ta>
            <ta e="T122" id="Seg_4670" s="T121">tănan</ta>
            <ta e="T123" id="Seg_4671" s="T122">bar</ta>
            <ta e="T124" id="Seg_4672" s="T123">gibər</ta>
            <ta e="T125" id="Seg_4673" s="T124">paʔlaːn-gə-l</ta>
            <ta e="T141" id="Seg_4674" s="T140">gibər</ta>
            <ta e="T143" id="Seg_4675" s="T142">paʔ-laːndə-ga-l</ta>
            <ta e="T144" id="Seg_4676" s="T143">i-ʔ</ta>
            <ta e="T145" id="Seg_4677" s="T144">păd-a-ʔ</ta>
            <ta e="T146" id="Seg_4678" s="T145">ato</ta>
            <ta e="T147" id="Seg_4679" s="T146">saʔmə-luʔ-lə-l</ta>
            <ta e="T148" id="Seg_4680" s="T147">šo-ʔ</ta>
            <ta e="T149" id="Seg_4681" s="T148">döbər</ta>
            <ta e="T150" id="Seg_4682" s="T149">dĭ</ta>
            <ta e="T151" id="Seg_4683" s="T150">koʔbdo</ta>
            <ta e="T152" id="Seg_4684" s="T151">amno-laʔbə</ta>
            <ta e="T153" id="Seg_4685" s="T152">todam</ta>
            <ta e="T154" id="Seg_4686" s="T153">bar</ta>
            <ta e="T159" id="Seg_4687" s="T158">ĭmbi</ta>
            <ta e="T160" id="Seg_4688" s="T159">măn-də-lia-m</ta>
            <ta e="T161" id="Seg_4689" s="T160">dĭ</ta>
            <ta e="T162" id="Seg_4690" s="T161">ej</ta>
            <ta e="T163" id="Seg_4691" s="T162">nʼilgö-laʔbə</ta>
            <ta e="T164" id="Seg_4692" s="T163">dʼije-nə</ta>
            <ta e="T167" id="Seg_4693" s="T166">kan-də-ga-ʔi</ta>
            <ta e="T168" id="Seg_4694" s="T167">ine-ʔ-siʔ</ta>
            <ta e="T169" id="Seg_4695" s="T168">kola</ta>
            <ta e="T170" id="Seg_4696" s="T169">dʼabə-la-ʔi</ta>
            <ta e="T171" id="Seg_4697" s="T170">kola</ta>
            <ta e="T172" id="Seg_4698" s="T171">deʔ-le-ʔi</ta>
            <ta e="T173" id="Seg_4699" s="T172">măna</ta>
            <ta e="T174" id="Seg_4700" s="T173">možet</ta>
            <ta e="T175" id="Seg_4701" s="T174">mĭ-le-ʔi</ta>
            <ta e="T176" id="Seg_4702" s="T175">kabarləj</ta>
            <ta e="T178" id="Seg_4703" s="T177">dʼaga-gən</ta>
            <ta e="T179" id="Seg_4704" s="T178">uda-ziʔ</ta>
            <ta e="T180" id="Seg_4705" s="T179">kola</ta>
            <ta e="T181" id="Seg_4706" s="T180">dʼaː-pi-ʔi</ta>
            <ta e="T182" id="Seg_4707" s="T181">dʼü-nə</ta>
            <ta e="T183" id="Seg_4708" s="T182">dʼü-nə</ta>
            <ta e="T186" id="Seg_4709" s="T185">em-bi-ʔi</ta>
            <ta e="T187" id="Seg_4710" s="T186">dĭgəttə</ta>
            <ta e="T190" id="Seg_4711" s="T189">tustʼar-bi-ʔi</ta>
            <ta e="T191" id="Seg_4712" s="T190">dĭgəttə</ta>
            <ta e="T192" id="Seg_4713" s="T191">sadar-bi-ʔi</ta>
            <ta e="T193" id="Seg_4714" s="T192">kabar</ta>
            <ta e="T194" id="Seg_4715" s="T193">šĭšəge</ta>
            <ta e="T195" id="Seg_4716" s="T194">mo-laːm-bi</ta>
            <ta e="T196" id="Seg_4717" s="T195">ejü</ta>
            <ta e="T197" id="Seg_4718" s="T196">kal-la</ta>
            <ta e="T198" id="Seg_4719" s="T197">dʼür-bi</ta>
            <ta e="T199" id="Seg_4720" s="T198">kuja</ta>
            <ta e="T200" id="Seg_4721" s="T199">amga</ta>
            <ta e="T201" id="Seg_4722" s="T200">măndo-laʔbǝ</ta>
            <ta e="T202" id="Seg_4723" s="T201">sĭre</ta>
            <ta e="T203" id="Seg_4724" s="T202">šonə-ga</ta>
            <ta e="T204" id="Seg_4725" s="T203">beržə</ta>
            <ta e="T205" id="Seg_4726" s="T204">bar</ta>
            <ta e="T206" id="Seg_4727" s="T205">ugaːndə</ta>
            <ta e="T207" id="Seg_4728" s="T206">šĭšəge</ta>
            <ta e="T208" id="Seg_4729" s="T207">măn</ta>
            <ta e="T209" id="Seg_4730" s="T208">pa</ta>
            <ta e="T210" id="Seg_4731" s="T209">iʔgö</ta>
            <ta e="T211" id="Seg_4732" s="T210">dĭ</ta>
            <ta e="T212" id="Seg_4733" s="T211">kö-nə</ta>
            <ta e="T213" id="Seg_4734" s="T212">kabar-lə-j</ta>
            <ta e="T214" id="Seg_4735" s="T213">ĭmbi</ta>
            <ta e="T215" id="Seg_4736" s="T214">ulu-l</ta>
            <ta e="T216" id="Seg_4737" s="T215">tĭl-lie-l</ta>
            <ta e="T217" id="Seg_4738" s="T216">padʼi</ta>
            <ta e="T218" id="Seg_4739" s="T217">unu-ʔi</ta>
            <ta e="T219" id="Seg_4740" s="T218">i-ge</ta>
            <ta e="T220" id="Seg_4741" s="T219">ugaːndə</ta>
            <ta e="T221" id="Seg_4742" s="T220">iʔgö</ta>
            <ta e="T222" id="Seg_4743" s="T221">unu</ta>
            <ta e="T223" id="Seg_4744" s="T222">kuʔ-sʼittə</ta>
            <ta e="T224" id="Seg_4745" s="T223">nada</ta>
            <ta e="T225" id="Seg_4746" s="T224">dĭ-zeŋ</ta>
            <ta e="T226" id="Seg_4747" s="T225">kabarləj</ta>
            <ta e="T227" id="Seg_4748" s="T226">măn</ta>
            <ta e="T228" id="Seg_4749" s="T227">iʔbə-bie-m</ta>
            <ta e="T229" id="Seg_4750" s="T228">kunol-zittə</ta>
            <ta e="T230" id="Seg_4751" s="T229">kuriza</ta>
            <ta e="T231" id="Seg_4752" s="T230">ej</ta>
            <ta e="T232" id="Seg_4753" s="T231">mĭ-bi</ta>
            <ta e="T233" id="Seg_4754" s="T232">kunol-zittə</ta>
            <ta e="T234" id="Seg_4755" s="T233">măn</ta>
            <ta e="T235" id="Seg_4756" s="T234">uʔbdə-bia-m</ta>
            <ta e="T236" id="Seg_4757" s="T235">i</ta>
            <ta e="T239" id="Seg_4758" s="T238">daška</ta>
            <ta e="T240" id="Seg_4759" s="T239">ej</ta>
            <ta e="T241" id="Seg_4760" s="T240">iʔ-bie-m</ta>
            <ta e="T242" id="Seg_4761" s="T241">kuvas</ta>
            <ta e="T243" id="Seg_4762" s="T242">nüke</ta>
            <ta e="T244" id="Seg_4763" s="T243">amno-laʔbə</ta>
            <ta e="T245" id="Seg_4764" s="T244">kuvas</ta>
            <ta e="T246" id="Seg_4765" s="T245">nʼi</ta>
            <ta e="T247" id="Seg_4766" s="T246">amno-laʔbə</ta>
            <ta e="T248" id="Seg_4767" s="T247">măn</ta>
            <ta e="T249" id="Seg_4768" s="T248">ugaːndə</ta>
            <ta e="T250" id="Seg_4769" s="T249">dʼaktə</ta>
            <ta e="T251" id="Seg_4770" s="T250">mo-laːm-bia-m</ta>
            <ta e="T252" id="Seg_4771" s="T251">šide-göʔ</ta>
            <ta e="T253" id="Seg_4772" s="T252">ma-laʔpi-baʔ</ta>
            <ta e="T254" id="Seg_4773" s="T253">măn</ta>
            <ta e="T255" id="Seg_4774" s="T254">da</ta>
            <ta e="T256" id="Seg_4775" s="T255">kaga-m</ta>
            <ta e="T260" id="Seg_4776" s="T259">a</ta>
            <ta e="T261" id="Seg_4777" s="T260">dĭ-zeŋ</ta>
            <ta e="T262" id="Seg_4778" s="T261">bar</ta>
            <ta e="T263" id="Seg_4779" s="T262">kü-laːm-bi-ʔi</ta>
            <ta e="T264" id="Seg_4780" s="T263">bar</ta>
            <ta e="T270" id="Seg_4781" s="T269">iʔgö</ta>
            <ta e="T271" id="Seg_4782" s="T270">pʼe</ta>
            <ta e="T272" id="Seg_4783" s="T271">kam-bi</ta>
            <ta e="T273" id="Seg_4784" s="T272">bar</ta>
            <ta e="T274" id="Seg_4785" s="T273">bor</ta>
            <ta e="T275" id="Seg_4786" s="T274">dʼü-nə</ta>
            <ta e="T278" id="Seg_4787" s="T277">amno-lia-l</ta>
            <ta e="T279" id="Seg_4788" s="T278">surno</ta>
            <ta e="T280" id="Seg_4789" s="T279">mo-lə-j</ta>
            <ta e="T281" id="Seg_4790" s="T280">uda-m</ta>
            <ta e="T282" id="Seg_4791" s="T281">dʼükəm-nie</ta>
            <ta e="T283" id="Seg_4792" s="T282">nada</ta>
            <ta e="T284" id="Seg_4793" s="T283">kădə-sʼtə</ta>
            <ta e="T285" id="Seg_4794" s="T284">tăn</ta>
            <ta e="T286" id="Seg_4795" s="T285">ugaːndə</ta>
            <ta e="T289" id="Seg_4796" s="T288">šĭke-l</ta>
            <ta e="T290" id="Seg_4797" s="T289">numo</ta>
            <ta e="T291" id="Seg_4798" s="T290">kudo-nz-lia-l</ta>
            <ta e="T292" id="Seg_4799" s="T291">tăn</ta>
            <ta e="T293" id="Seg_4800" s="T292">tăn</ta>
            <ta e="T294" id="Seg_4801" s="T293">tĭme-l</ta>
            <ta e="T295" id="Seg_4802" s="T294">ugaːndə</ta>
            <ta e="T296" id="Seg_4803" s="T295">numo</ta>
            <ta e="T297" id="Seg_4804" s="T296">püje-l</ta>
            <ta e="T300" id="Seg_4805" s="T299">numo</ta>
            <ta e="T301" id="Seg_4806" s="T300">kuʔ-lə</ta>
            <ta e="T302" id="Seg_4807" s="T301">bar</ta>
            <ta e="T303" id="Seg_4808" s="T302">mʼaŋ-ŋaʔbə</ta>
            <ta e="T304" id="Seg_4809" s="T303">mon-zaŋ-bə</ta>
            <ta e="T307" id="Seg_4810" s="T306">mon-zaŋ-də</ta>
            <ta e="T308" id="Seg_4811" s="T307">nʼešpək</ta>
            <ta e="T311" id="Seg_4812" s="T310">ugaːndə</ta>
            <ta e="T312" id="Seg_4813" s="T311">eʔbdə-l</ta>
            <ta e="T313" id="Seg_4814" s="T312">numo</ta>
            <ta e="T314" id="Seg_4815" s="T313">ugaːndə</ta>
            <ta e="T315" id="Seg_4816" s="T314">ku-zaŋ-də</ta>
            <ta e="T316" id="Seg_4817" s="T315">numo</ta>
            <ta e="T317" id="Seg_4818" s="T316">sʼima-l</ta>
            <ta e="T318" id="Seg_4819" s="T317">urgo</ta>
            <ta e="T319" id="Seg_4820" s="T318">ulu-l</ta>
            <ta e="T320" id="Seg_4821" s="T319">ugaːndə</ta>
            <ta e="T321" id="Seg_4822" s="T320">urgo</ta>
            <ta e="T324" id="Seg_4823" s="T323">uda-zaŋ-də</ta>
            <ta e="T325" id="Seg_4824" s="T324">numo</ta>
            <ta e="T328" id="Seg_4825" s="T327">uju-zaŋ-də</ta>
            <ta e="T329" id="Seg_4826" s="T328">numo</ta>
            <ta e="T330" id="Seg_4827" s="T329">bos-tə</ta>
            <ta e="T331" id="Seg_4828" s="T330">bar</ta>
            <ta e="T332" id="Seg_4829" s="T331">todam</ta>
            <ta e="T333" id="Seg_4830" s="T332">müjö</ta>
            <ta e="T334" id="Seg_4831" s="T333">nʼergö-laʔbə</ta>
            <ta e="T335" id="Seg_4832" s="T334">pa-nə</ta>
            <ta e="T336" id="Seg_4833" s="T335">amno-bi</ta>
            <ta e="T337" id="Seg_4834" s="T336">iʔgö</ta>
            <ta e="T342" id="Seg_4835" s="T341">süjö-ʔi</ta>
            <ta e="T343" id="Seg_4836" s="T342">nʼergö-laʔbə-ʔjə</ta>
            <ta e="T344" id="Seg_4837" s="T343">karatʼgaj</ta>
            <ta e="T345" id="Seg_4838" s="T344">bar</ta>
            <ta e="T348" id="Seg_4839" s="T347">nʼilgö-laʔbə</ta>
            <ta e="T349" id="Seg_4840" s="T348">pogəraš</ta>
            <ta e="T350" id="Seg_4841" s="T349">nʼilgö-laʔ</ta>
            <ta e="T353" id="Seg_4842" s="T352">šo-bi</ta>
            <ta e="T354" id="Seg_4843" s="T353">kuriza</ta>
            <ta e="T355" id="Seg_4844" s="T354">bar</ta>
            <ta e="T356" id="Seg_4845" s="T355">kadəʔ-lia</ta>
            <ta e="T357" id="Seg_4846" s="T356">am-zittə</ta>
            <ta e="T358" id="Seg_4847" s="T357">măndə-r-lia</ta>
            <ta e="T359" id="Seg_4848" s="T358">nöməl-bio-m</ta>
            <ta e="T360" id="Seg_4849" s="T359">bar</ta>
            <ta e="T363" id="Seg_4850" s="T362">bü-m</ta>
            <ta e="T364" id="Seg_4851" s="T363">nuldə-sʼtə</ta>
            <ta e="T365" id="Seg_4852" s="T364">pʼeːš-də</ta>
            <ta e="T366" id="Seg_4853" s="T365">dʼübige</ta>
            <ta e="T367" id="Seg_4854" s="T366">bü</ta>
            <ta e="T368" id="Seg_4855" s="T367">naga</ta>
            <ta e="T371" id="Seg_4856" s="T370">măna</ta>
            <ta e="T372" id="Seg_4857" s="T371">kereʔ</ta>
            <ta e="T373" id="Seg_4858" s="T372">oroma</ta>
            <ta e="T376" id="Seg_4859" s="T375">kămnə-li-m</ta>
            <ta e="T377" id="Seg_4860" s="T376">bargari-m</ta>
            <ta e="T378" id="Seg_4861" s="T377">dĭgəttə</ta>
            <ta e="T379" id="Seg_4862" s="T378">bazə-sʼtə</ta>
            <ta e="T380" id="Seg_4863" s="T379">nada</ta>
            <ta e="T381" id="Seg_4864" s="T380">takše-ʔi</ta>
            <ta e="T382" id="Seg_4865" s="T381">sĭre</ta>
            <ta e="T383" id="Seg_4866" s="T382">pa</ta>
            <ta e="T384" id="Seg_4867" s="T383">šomne</ta>
            <ta e="T385" id="Seg_4868" s="T384">pa</ta>
            <ta e="T386" id="Seg_4869" s="T385">komu</ta>
            <ta e="T387" id="Seg_4870" s="T386">pa</ta>
            <ta e="T388" id="Seg_4871" s="T387">san</ta>
            <ta e="T389" id="Seg_4872" s="T388">pa</ta>
            <ta e="T390" id="Seg_4873" s="T389">sagən</ta>
            <ta e="T391" id="Seg_4874" s="T390">pa-gən</ta>
            <ta e="T392" id="Seg_4875" s="T391">bar</ta>
            <ta e="T393" id="Seg_4876" s="T392">šiška-ʔi</ta>
            <ta e="T394" id="Seg_4877" s="T393">özer-leʔbə-ʔjə</ta>
            <ta e="T395" id="Seg_4878" s="T394">san</ta>
            <ta e="T398" id="Seg_4879" s="T397">pa-gən</ta>
            <ta e="T399" id="Seg_4880" s="T398">bar</ta>
            <ta e="T400" id="Seg_4881" s="T399">šiška-ʔi</ta>
            <ta e="T401" id="Seg_4882" s="T400">amno-laʔbə-ʔjə</ta>
            <ta e="T402" id="Seg_4883" s="T401">sĭre</ta>
            <ta e="T403" id="Seg_4884" s="T402">pa-gən</ta>
            <ta e="T404" id="Seg_4885" s="T403">bar</ta>
            <ta e="T405" id="Seg_4886" s="T404">takšə</ta>
            <ta e="T406" id="Seg_4887" s="T405">a-laʔbə-ʔjə</ta>
            <ta e="T407" id="Seg_4888" s="T406">segi</ta>
            <ta e="T408" id="Seg_4889" s="T407">bü</ta>
            <ta e="T409" id="Seg_4890" s="T408">bĭt-leʔbə-ʔjə</ta>
            <ta e="T410" id="Seg_4891" s="T409">pa-j</ta>
            <ta e="T411" id="Seg_4892" s="T410">pa-j</ta>
            <ta e="T412" id="Seg_4893" s="T411">šamnak</ta>
            <ta e="T413" id="Seg_4894" s="T412">baza-j</ta>
            <ta e="T414" id="Seg_4895" s="T413">šamnak</ta>
            <ta e="T415" id="Seg_4896" s="T414">takšə</ta>
            <ta e="T416" id="Seg_4897" s="T415">baza-j</ta>
            <ta e="T417" id="Seg_4898" s="T416">ugaːndə</ta>
            <ta e="T418" id="Seg_4899" s="T417">pa-ʔi</ta>
            <ta e="T419" id="Seg_4900" s="T418">iʔgö</ta>
            <ta e="T420" id="Seg_4901" s="T419">dĭ-zeŋ</ta>
            <ta e="T421" id="Seg_4902" s="T420">nada</ta>
            <ta e="T422" id="Seg_4903" s="T421">jaʔ-sʼittə</ta>
            <ta e="T423" id="Seg_4904" s="T422">dĭgəttə</ta>
            <ta e="T424" id="Seg_4905" s="T423">kür-zittə</ta>
            <ta e="T425" id="Seg_4906" s="T424">dĭgəttə</ta>
            <ta e="T426" id="Seg_4907" s="T425">tura</ta>
            <ta e="T427" id="Seg_4908" s="T426">ka-zittə</ta>
            <ta e="T428" id="Seg_4909" s="T427">dĭgəttə</ta>
            <ta e="T429" id="Seg_4910" s="T428">dĭ</ta>
            <ta e="T430" id="Seg_4911" s="T429">pa-ʔi-zʼiʔ</ta>
            <ta e="T431" id="Seg_4912" s="T430">tura</ta>
            <ta e="T432" id="Seg_4913" s="T431">jaʔ-sʼittə</ta>
            <ta e="T433" id="Seg_4914" s="T432">nada</ta>
            <ta e="T434" id="Seg_4915" s="T433">amno-zʼittə</ta>
            <ta e="T435" id="Seg_4916" s="T434">tura-gən</ta>
            <ta e="T436" id="Seg_4917" s="T435">pʼeːš</ta>
            <ta e="T437" id="Seg_4918" s="T436">en-zittə</ta>
            <ta e="T438" id="Seg_4919" s="T437">pʼeːš-gən</ta>
            <ta e="T439" id="Seg_4920" s="T438">pür-zittə</ta>
            <ta e="T440" id="Seg_4921" s="T439">ipek</ta>
            <ta e="T441" id="Seg_4922" s="T440">bĭd-e-ʔ</ta>
            <ta e="T442" id="Seg_4923" s="T441">bü</ta>
            <ta e="T443" id="Seg_4924" s="T442">măn</ta>
            <ta e="T444" id="Seg_4925" s="T443">dibər</ta>
            <ta e="T447" id="Seg_4926" s="T446">em-bie-m</ta>
            <ta e="T448" id="Seg_4927" s="T447">sĭreʔpne</ta>
            <ta e="T451" id="Seg_4928" s="T450">bü</ta>
            <ta e="T452" id="Seg_4929" s="T451">kămna-bia-m</ta>
            <ta e="T455" id="Seg_4930" s="T454">namzəbi</ta>
            <ta e="T456" id="Seg_4931" s="T455">bar</ta>
            <ta e="T457" id="Seg_4932" s="T456">ipek</ta>
            <ta e="T458" id="Seg_4933" s="T457">en-də-bie-m</ta>
            <ta e="T459" id="Seg_4934" s="T458">bĭd-e-ʔ</ta>
            <ta e="T460" id="Seg_4935" s="T459">ugaːndə</ta>
            <ta e="T461" id="Seg_4936" s="T460">jakšə</ta>
            <ta e="T462" id="Seg_4937" s="T461">šiʔ</ta>
            <ta e="T463" id="Seg_4938" s="T462">taldʼen</ta>
            <ta e="T464" id="Seg_4939" s="T463">ara</ta>
            <ta e="T465" id="Seg_4940" s="T464">bĭʔ-pi-leʔ</ta>
            <ta e="T468" id="Seg_4941" s="T467">jezerik</ta>
            <ta e="T469" id="Seg_4942" s="T468">i-bi-leʔ</ta>
            <ta e="T470" id="Seg_4943" s="T469">bar</ta>
            <ta e="T474" id="Seg_4944" s="T472">teinen</ta>
            <ta e="T475" id="Seg_4945" s="T474">bar</ta>
            <ta e="T478" id="Seg_4946" s="T477">bĭd-e-ʔ</ta>
            <ta e="T479" id="Seg_4947" s="T478">komu</ta>
            <ta e="T480" id="Seg_4948" s="T479">bü</ta>
            <ta e="T481" id="Seg_4949" s="T480">măn</ta>
            <ta e="T482" id="Seg_4950" s="T481">dibər</ta>
            <ta e="T485" id="Seg_4951" s="T484">sĭreʔpne</ta>
            <ta e="T486" id="Seg_4952" s="T485">em-bie-m</ta>
            <ta e="T488" id="Seg_4953" s="T487">bü</ta>
            <ta e="T489" id="Seg_4954" s="T488">kămnə-bia-m</ta>
            <ta e="T492" id="Seg_4955" s="T491">namzəga</ta>
            <ta e="T493" id="Seg_4956" s="T492">mo-laːm-bi</ta>
            <ta e="T494" id="Seg_4957" s="T493">jakšə</ta>
            <ta e="T495" id="Seg_4958" s="T494">mo-lə-j</ta>
            <ta e="T496" id="Seg_4959" s="T495">tănan</ta>
            <ta e="T497" id="Seg_4960" s="T496">ara</ta>
            <ta e="T498" id="Seg_4961" s="T497">i-ʔ</ta>
            <ta e="T499" id="Seg_4962" s="T498">bĭd-e-ʔ</ta>
            <ta e="T500" id="Seg_4963" s="T499">ato</ta>
            <ta e="T501" id="Seg_4964" s="T500">ulu-l</ta>
            <ta e="T502" id="Seg_4965" s="T501">ĭzem-nie</ta>
            <ta e="T503" id="Seg_4966" s="T502">sĭj-də</ta>
            <ta e="T504" id="Seg_4967" s="T503">ĭzem-nie</ta>
            <ta e="T505" id="Seg_4968" s="T504">bĭt-le-l</ta>
            <ta e="T506" id="Seg_4969" s="T505">dak</ta>
            <ta e="T507" id="Seg_4970" s="T506">kü-lal-lə-l</ta>
            <ta e="T508" id="Seg_4971" s="T507">a</ta>
            <ta e="T509" id="Seg_4972" s="T508">ej</ta>
            <ta e="T510" id="Seg_4973" s="T509">bĭt-le-l</ta>
            <ta e="T511" id="Seg_4974" s="T510">dak</ta>
            <ta e="T512" id="Seg_4975" s="T511">kondʼo</ta>
            <ta e="T513" id="Seg_4976" s="T512">amno-la-l</ta>
            <ta e="T514" id="Seg_4977" s="T513">mašina</ta>
            <ta e="T519" id="Seg_4978" s="T518">taler-laʔbə</ta>
            <ta e="T520" id="Seg_4979" s="T519">dĭn</ta>
            <ta e="T521" id="Seg_4980" s="T520">kuza</ta>
            <ta e="T522" id="Seg_4981" s="T521">amno-laʔbə</ta>
            <ta e="T523" id="Seg_4982" s="T522">mašina-gəʔ</ta>
            <ta e="T524" id="Seg_4983" s="T523">bar</ta>
            <ta e="T525" id="Seg_4984" s="T524">bər</ta>
            <ta e="T526" id="Seg_4985" s="T525">šonə-ga</ta>
            <ta e="T527" id="Seg_4986" s="T526">dĭ</ta>
            <ta e="T528" id="Seg_4987" s="T527">bar</ta>
            <ta e="T529" id="Seg_4988" s="T528">dibər</ta>
            <ta e="T531" id="Seg_4989" s="T530">döbər</ta>
            <ta e="T532" id="Seg_4990" s="T531">kal-laʔbə</ta>
            <ta e="T533" id="Seg_4991" s="T532">aba-m</ta>
            <ta e="T534" id="Seg_4992" s="T533">ija-m</ta>
            <ta e="T535" id="Seg_4993" s="T534">koʔbdo</ta>
            <ta e="T536" id="Seg_4994" s="T535">ku-bi-ʔi</ta>
            <ta e="T537" id="Seg_4995" s="T536">măn</ta>
            <ta e="T538" id="Seg_4996" s="T537">măna</ta>
            <ta e="T539" id="Seg_4997" s="T538">măn-lia-ʔi</ta>
            <ta e="T540" id="Seg_4998" s="T539">kan-a-ʔ</ta>
            <ta e="T541" id="Seg_4999" s="T540">dĭ-zʼiʔ</ta>
            <ta e="T544" id="Seg_5000" s="T543">tĭmni-t</ta>
            <ta e="T545" id="Seg_5001" s="T544">girgit</ta>
            <ta e="T546" id="Seg_5002" s="T545">di</ta>
            <ta e="T547" id="Seg_5003" s="T546">dĭgəttə</ta>
            <ta e="T548" id="Seg_5004" s="T547">tibi-nə</ta>
            <ta e="T549" id="Seg_5005" s="T548">i-lie-l</ta>
            <ta e="T550" id="Seg_5006" s="T549">dĭgəttə</ta>
            <ta e="T551" id="Seg_5007" s="T550">es-seŋ-də</ta>
            <ta e="T552" id="Seg_5008" s="T551">mo-lə-ʔjə</ta>
            <ta e="T554" id="Seg_5009" s="T553">es-seŋ-lə</ta>
            <ta e="T555" id="Seg_5010" s="T554">özer-le-ʔjə</ta>
            <ta e="T556" id="Seg_5011" s="T555">tănan</ta>
            <ta e="T557" id="Seg_5012" s="T556">kabažar-la-ʔjə</ta>
            <ta e="T558" id="Seg_5013" s="T557">ne-m</ta>
            <ta e="T563" id="Seg_5014" s="T562">i-ʔ</ta>
            <ta e="T564" id="Seg_5015" s="T563">münör-a-ʔ</ta>
            <ta e="T565" id="Seg_5016" s="T564">nʼilgö-t</ta>
            <ta e="T570" id="Seg_5017" s="T569">aba-m</ta>
            <ta e="T571" id="Seg_5018" s="T570">da</ta>
            <ta e="T572" id="Seg_5019" s="T571">ija-m</ta>
            <ta e="T573" id="Seg_5020" s="T572">nʼilgö-t</ta>
            <ta e="T578" id="Seg_5021" s="T577">teinen</ta>
            <ta e="T579" id="Seg_5022" s="T578">bar</ta>
            <ta e="T580" id="Seg_5023" s="T579">kal-la</ta>
            <ta e="T581" id="Seg_5024" s="T580">dʼür-bi-ʔi</ta>
            <ta e="T582" id="Seg_5025" s="T581">aba-t</ta>
            <ta e="T583" id="Seg_5026" s="T582">ija-t</ta>
            <ta e="T584" id="Seg_5027" s="T583">kal-la</ta>
            <ta e="T585" id="Seg_5028" s="T584">dʼür-bi-ʔi</ta>
            <ta e="T586" id="Seg_5029" s="T585">es-seŋ</ta>
            <ta e="T587" id="Seg_5030" s="T586">kal-la</ta>
            <ta e="T588" id="Seg_5031" s="T587">dʼür-bi-ʔi</ta>
            <ta e="T589" id="Seg_5032" s="T588">măn</ta>
            <ta e="T590" id="Seg_5033" s="T589">unnʼa</ta>
            <ta e="T591" id="Seg_5034" s="T590">maʔ-na-l</ta>
            <ta e="T592" id="Seg_5035" s="T591">amno-laʔbə-m</ta>
            <ta e="T593" id="Seg_5036" s="T592">nada</ta>
            <ta e="T594" id="Seg_5037" s="T593">dʼü</ta>
            <ta e="T595" id="Seg_5038" s="T594">tĭl-zittə</ta>
            <ta e="T596" id="Seg_5039" s="T595">kuza</ta>
            <ta e="T597" id="Seg_5040" s="T596">kü-laːm-bi</ta>
            <ta e="T598" id="Seg_5041" s="T597">nada</ta>
            <ta e="T599" id="Seg_5042" s="T598">dĭ-m</ta>
            <ta e="T600" id="Seg_5043" s="T599">dibər</ta>
            <ta e="T601" id="Seg_5044" s="T600">en-zittə</ta>
            <ta e="T602" id="Seg_5045" s="T601">dĭgəttə</ta>
            <ta e="T603" id="Seg_5046" s="T602">kămna-sʼtə</ta>
            <ta e="T606" id="Seg_5047" s="T605">dĭgəttə</ta>
            <ta e="T611" id="Seg_5048" s="T610">dʼora</ta>
            <ta e="T613" id="Seg_5049" s="T611">dĭgəttə</ta>
            <ta e="T614" id="Seg_5050" s="T613">dĭgəttə</ta>
            <ta e="T617" id="Seg_5051" s="T616">dʼü-zʼiʔ</ta>
            <ta e="T618" id="Seg_5052" s="T617">nada</ta>
            <ta e="T619" id="Seg_5053" s="T618">kaj-zittə</ta>
            <ta e="T620" id="Seg_5054" s="T619">dĭ-m</ta>
            <ta e="T621" id="Seg_5055" s="T620">dĭgəttə</ta>
            <ta e="T622" id="Seg_5056" s="T621">maʔ-na-l</ta>
            <ta e="T625" id="Seg_5057" s="T624">šo-zittə</ta>
            <ta e="T626" id="Seg_5058" s="T625">am-zittə</ta>
            <ta e="T629" id="Seg_5059" s="T628">amna-ʔ</ta>
            <ta e="T630" id="Seg_5060" s="T629">am-zittə</ta>
            <ta e="T631" id="Seg_5061" s="T630">kuza</ta>
            <ta e="T632" id="Seg_5062" s="T631">ĭzem-bi</ta>
            <ta e="T633" id="Seg_5063" s="T632">šaman-də</ta>
            <ta e="T634" id="Seg_5064" s="T633">mĭm-bi</ta>
            <ta e="T635" id="Seg_5065" s="T634">šaman</ta>
            <ta e="T636" id="Seg_5066" s="T635">ej</ta>
            <ta e="T637" id="Seg_5067" s="T636">mo-bi</ta>
            <ta e="T638" id="Seg_5068" s="T637">dĭ-m</ta>
            <ta e="T639" id="Seg_5069" s="T638">dʼazir-zittə</ta>
            <ta e="T643" id="Seg_5070" s="T641">kü-laːm-bi</ta>
            <ta e="T644" id="Seg_5071" s="T643">kumən</ta>
            <ta e="T645" id="Seg_5072" s="T644">ej</ta>
            <ta e="T646" id="Seg_5073" s="T645">ĭzem-bi</ta>
            <ta e="T647" id="Seg_5074" s="T646">tüj</ta>
            <ta e="T648" id="Seg_5075" s="T647">bar</ta>
            <ta e="T649" id="Seg_5076" s="T648">iʔbo-laʔbə</ta>
            <ta e="T651" id="Seg_5077" s="T649">ĭmbi-zʼiʔ=də</ta>
            <ta e="T652" id="Seg_5078" s="T651">ej</ta>
            <ta e="T653" id="Seg_5079" s="T652">mĭngə-lie</ta>
            <ta e="T654" id="Seg_5080" s="T653">măna</ta>
            <ta e="T655" id="Seg_5081" s="T654">bar</ta>
            <ta e="T656" id="Seg_5082" s="T655">münör-bi-ʔi</ta>
            <ta e="T657" id="Seg_5083" s="T656">ugaːndə</ta>
            <ta e="T658" id="Seg_5084" s="T657">tăŋ</ta>
            <ta e="T659" id="Seg_5085" s="T658">bos-pə</ta>
            <ta e="T660" id="Seg_5086" s="T659">ej</ta>
            <ta e="T661" id="Seg_5087" s="T660">tĭmne-m</ta>
            <ta e="T662" id="Seg_5088" s="T661">ĭmbi-zʼiʔ</ta>
            <ta e="T663" id="Seg_5089" s="T662">münör-bi-ʔi</ta>
            <ta e="T666" id="Seg_5090" s="T665">măn</ta>
            <ta e="T667" id="Seg_5091" s="T666">măn</ta>
            <ta e="T668" id="Seg_5092" s="T667">bar</ta>
            <ta e="T669" id="Seg_5093" s="T668">ĭzem-bi</ta>
            <ta e="T670" id="Seg_5094" s="T669">ĭzem-nie-m</ta>
            <ta e="T671" id="Seg_5095" s="T670">ulu-m</ta>
            <ta e="T672" id="Seg_5096" s="T671">ĭzem-nie</ta>
            <ta e="T673" id="Seg_5097" s="T672">ku-m</ta>
            <ta e="T674" id="Seg_5098" s="T673">ĭzem-nie</ta>
            <ta e="T675" id="Seg_5099" s="T674">sʼima-m</ta>
            <ta e="T676" id="Seg_5100" s="T675">ĭzem-nie</ta>
            <ta e="T677" id="Seg_5101" s="T676">tĭme-m</ta>
            <ta e="T678" id="Seg_5102" s="T677">ĭzem-nie</ta>
            <ta e="T679" id="Seg_5103" s="T678">püje-m</ta>
            <ta e="T680" id="Seg_5104" s="T679">ĭzem-nie</ta>
            <ta e="T681" id="Seg_5105" s="T680">uda-m</ta>
            <ta e="T682" id="Seg_5106" s="T681">ĭzem-nie</ta>
            <ta e="T683" id="Seg_5107" s="T682">uju-m</ta>
            <ta e="T684" id="Seg_5108" s="T683">ĭzem-nie</ta>
            <ta e="T685" id="Seg_5109" s="T684">nanə-m</ta>
            <ta e="T686" id="Seg_5110" s="T685">ĭzem-nie</ta>
            <ta e="T687" id="Seg_5111" s="T686">sĭj-bə</ta>
            <ta e="T688" id="Seg_5112" s="T687">ĭzem-nie</ta>
            <ta e="T689" id="Seg_5113" s="T688">bögel-bə</ta>
            <ta e="T690" id="Seg_5114" s="T689">ĭzem-nie</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PKZ">
            <ta e="T5" id="Seg_5115" s="T4">ne-zAŋ</ta>
            <ta e="T6" id="Seg_5116" s="T5">ugaːndə</ta>
            <ta e="T7" id="Seg_5117" s="T6">iʔgö</ta>
            <ta e="T8" id="Seg_5118" s="T7">šonə-gA-jəʔ</ta>
            <ta e="T9" id="Seg_5119" s="T8">măn</ta>
            <ta e="T10" id="Seg_5120" s="T9">măn-bi-m</ta>
            <ta e="T11" id="Seg_5121" s="T10">gijen</ta>
            <ta e="T12" id="Seg_5122" s="T11">i-bi-lAʔ</ta>
            <ta e="T13" id="Seg_5123" s="T12">dĭ-zAŋ</ta>
            <ta e="T14" id="Seg_5124" s="T13">măn-liA-jəʔ</ta>
            <ta e="T15" id="Seg_5125" s="T14">tüžöj</ta>
            <ta e="T16" id="Seg_5126" s="T15">surdo-zittə</ta>
            <ta e="T17" id="Seg_5127" s="T16">mĭn-bi-bAʔ</ta>
            <ta e="T18" id="Seg_5128" s="T17">det-KAʔ</ta>
            <ta e="T19" id="Seg_5129" s="T18">măna</ta>
            <ta e="T20" id="Seg_5130" s="T19">süt</ta>
            <ta e="T21" id="Seg_5131" s="T20">kabarləj</ta>
            <ta e="T22" id="Seg_5132" s="T21">daška</ta>
            <ta e="T23" id="Seg_5133" s="T22">ešši</ta>
            <ta e="T24" id="Seg_5134" s="T23">bar</ta>
            <ta e="T25" id="Seg_5135" s="T24">tüʔ-bi</ta>
            <ta e="T26" id="Seg_5136" s="T25">ugaːndə</ta>
            <ta e="T27" id="Seg_5137" s="T26">sagər</ta>
            <ta e="T28" id="Seg_5138" s="T27">bar</ta>
            <ta e="T29" id="Seg_5139" s="T28">koː-luʔbdə-bi</ta>
            <ta e="T30" id="Seg_5140" s="T29">nadə</ta>
            <ta e="T31" id="Seg_5141" s="T30">bazə-zittə</ta>
            <ta e="T32" id="Seg_5142" s="T31">dĭ-m</ta>
            <ta e="T35" id="Seg_5143" s="T34">kĭškə-zittə</ta>
            <ta e="T36" id="Seg_5144" s="T35">dĭgəttə</ta>
            <ta e="T37" id="Seg_5145" s="T36">bazə-zittə</ta>
            <ta e="T38" id="Seg_5146" s="T37">nadə</ta>
            <ta e="T41" id="Seg_5147" s="T40">kür-zittə</ta>
            <ta e="T42" id="Seg_5148" s="T41">nadə</ta>
            <ta e="T43" id="Seg_5149" s="T42">dĭgəttə</ta>
            <ta e="T44" id="Seg_5150" s="T43">am-zittə</ta>
            <ta e="T45" id="Seg_5151" s="T44">mĭ-zittə</ta>
            <ta e="T46" id="Seg_5152" s="T45">dĭgəttə</ta>
            <ta e="T47" id="Seg_5153" s="T46">kunol-zittə</ta>
            <ta e="T50" id="Seg_5154" s="T49">iʔbə</ta>
            <ta e="T51" id="Seg_5155" s="T50">nadə</ta>
            <ta e="T52" id="Seg_5156" s="T51">oːrdə-ʔ</ta>
            <ta e="T53" id="Seg_5157" s="T52">ešši-m</ta>
            <ta e="T54" id="Seg_5158" s="T53">bar</ta>
            <ta e="T55" id="Seg_5159" s="T54">amno-ʔ</ta>
            <ta e="T56" id="Seg_5160" s="T55">tura-Kən</ta>
            <ta e="T57" id="Seg_5161" s="T56">măn</ta>
            <ta e="T58" id="Seg_5162" s="T57">nʼiʔdə</ta>
            <ta e="T59" id="Seg_5163" s="T58">kan-lV-m</ta>
            <ta e="T60" id="Seg_5164" s="T59">kĭnzə-zittə</ta>
            <ta e="T61" id="Seg_5165" s="T60">tüʔ-zittə</ta>
            <ta e="T62" id="Seg_5166" s="T61">dĭgəttə</ta>
            <ta e="T63" id="Seg_5167" s="T62">šo-lV-m</ta>
            <ta e="T64" id="Seg_5168" s="T63">amno-lV-m</ta>
            <ta e="T65" id="Seg_5169" s="T64">tăn-ziʔ</ta>
            <ta e="T66" id="Seg_5170" s="T65">i</ta>
            <ta e="T67" id="Seg_5171" s="T66">tʼăbaktər-lV-bAʔ</ta>
            <ta e="T68" id="Seg_5172" s="T67">kabarləj</ta>
            <ta e="T69" id="Seg_5173" s="T68">măn</ta>
            <ta e="T70" id="Seg_5174" s="T69">köten-m</ta>
            <ta e="T71" id="Seg_5175" s="T70">kuvas</ta>
            <ta e="T72" id="Seg_5176" s="T71">tăn</ta>
            <ta e="T73" id="Seg_5177" s="T72">kadul-də</ta>
            <ta e="T76" id="Seg_5178" s="T75">ugaːndə</ta>
            <ta e="T77" id="Seg_5179" s="T76">nʼešpək</ta>
            <ta e="T78" id="Seg_5180" s="T77">köten</ta>
            <ta e="T79" id="Seg_5181" s="T78">ugaːndə</ta>
            <ta e="T80" id="Seg_5182" s="T79">sagər</ta>
            <ta e="T81" id="Seg_5183" s="T80">köten</ta>
            <ta e="T82" id="Seg_5184" s="T81">ugaːndə</ta>
            <ta e="T83" id="Seg_5185" s="T82">kömə</ta>
            <ta e="T84" id="Seg_5186" s="T83">köten</ta>
            <ta e="T85" id="Seg_5187" s="T84">ugaːndə</ta>
            <ta e="T86" id="Seg_5188" s="T85">sĭri</ta>
            <ta e="T87" id="Seg_5189" s="T86">köten</ta>
            <ta e="T88" id="Seg_5190" s="T87">ugaːndə</ta>
            <ta e="T89" id="Seg_5191" s="T88">nʼešpək</ta>
            <ta e="T90" id="Seg_5192" s="T89">köten</ta>
            <ta e="T91" id="Seg_5193" s="T90">ugaːndə</ta>
            <ta e="T92" id="Seg_5194" s="T91">todam</ta>
            <ta e="T94" id="Seg_5195" s="T93">köten</ta>
            <ta e="T95" id="Seg_5196" s="T94">kabarləj</ta>
            <ta e="T96" id="Seg_5197" s="T95">ešši</ta>
            <ta e="T97" id="Seg_5198" s="T96">šonə-gA-l</ta>
            <ta e="T98" id="Seg_5199" s="T97">uda-zAŋ-də</ta>
            <ta e="T99" id="Seg_5200" s="T98">bar</ta>
            <ta e="T100" id="Seg_5201" s="T99">balgaš</ta>
            <ta e="T101" id="Seg_5202" s="T100">nadə</ta>
            <ta e="T102" id="Seg_5203" s="T101">bazə-zittə</ta>
            <ta e="T103" id="Seg_5204" s="T102">šo-ʔ</ta>
            <ta e="T104" id="Seg_5205" s="T103">döbər</ta>
            <ta e="T105" id="Seg_5206" s="T104">e-ʔ</ta>
            <ta e="T106" id="Seg_5207" s="T105">tʼor-ə-ʔ</ta>
            <ta e="T107" id="Seg_5208" s="T106">kan-ə-ʔ</ta>
            <ta e="T108" id="Seg_5209" s="T107">maʔ-Tə-l</ta>
            <ta e="T111" id="Seg_5210" s="T110">tănan</ta>
            <ta e="T112" id="Seg_5211" s="T111">padʼi</ta>
            <ta e="T117" id="Seg_5212" s="T116">măndo-laʔbə-jəʔ</ta>
            <ta e="T118" id="Seg_5213" s="T117">bar</ta>
            <ta e="T119" id="Seg_5214" s="T118">ato</ta>
            <ta e="T120" id="Seg_5215" s="T119">nabə-jəʔ</ta>
            <ta e="T121" id="Seg_5216" s="T120">bargamunə-jəʔ</ta>
            <ta e="T122" id="Seg_5217" s="T121">tănan</ta>
            <ta e="T123" id="Seg_5218" s="T122">bar</ta>
            <ta e="T124" id="Seg_5219" s="T123">gibər</ta>
            <ta e="T125" id="Seg_5220" s="T124">paʔ-gA-l</ta>
            <ta e="T141" id="Seg_5221" s="T140">gibər</ta>
            <ta e="T143" id="Seg_5222" s="T142">pada-laːndə-gA-l</ta>
            <ta e="T144" id="Seg_5223" s="T143">e-ʔ</ta>
            <ta e="T145" id="Seg_5224" s="T144">paʔ-ə-ʔ</ta>
            <ta e="T146" id="Seg_5225" s="T145">ato</ta>
            <ta e="T147" id="Seg_5226" s="T146">saʔmə-luʔbdə-lV-l</ta>
            <ta e="T148" id="Seg_5227" s="T147">šo-ʔ</ta>
            <ta e="T149" id="Seg_5228" s="T148">döbər</ta>
            <ta e="T150" id="Seg_5229" s="T149">dĭ</ta>
            <ta e="T151" id="Seg_5230" s="T150">koʔbdo</ta>
            <ta e="T152" id="Seg_5231" s="T151">amno-laʔbə</ta>
            <ta e="T153" id="Seg_5232" s="T152">todam</ta>
            <ta e="T154" id="Seg_5233" s="T153">bar</ta>
            <ta e="T159" id="Seg_5234" s="T158">ĭmbi</ta>
            <ta e="T160" id="Seg_5235" s="T159">măn-ntə-liA-m</ta>
            <ta e="T161" id="Seg_5236" s="T160">dĭ</ta>
            <ta e="T162" id="Seg_5237" s="T161">ej</ta>
            <ta e="T163" id="Seg_5238" s="T162">nʼilgö-laʔbə</ta>
            <ta e="T164" id="Seg_5239" s="T163">dʼije-Tə</ta>
            <ta e="T167" id="Seg_5240" s="T166">kan-ntə-gA-jəʔ</ta>
            <ta e="T168" id="Seg_5241" s="T167">ine-jəʔ-ziʔ</ta>
            <ta e="T169" id="Seg_5242" s="T168">kola</ta>
            <ta e="T170" id="Seg_5243" s="T169">dʼabə-lV-jəʔ</ta>
            <ta e="T171" id="Seg_5244" s="T170">kola</ta>
            <ta e="T172" id="Seg_5245" s="T171">det-lV-jəʔ</ta>
            <ta e="T173" id="Seg_5246" s="T172">măna</ta>
            <ta e="T174" id="Seg_5247" s="T173">možet</ta>
            <ta e="T175" id="Seg_5248" s="T174">mĭ-lV-jəʔ</ta>
            <ta e="T176" id="Seg_5249" s="T175">kabarləj</ta>
            <ta e="T178" id="Seg_5250" s="T177">tʼăga-Kən</ta>
            <ta e="T179" id="Seg_5251" s="T178">uda-ziʔ</ta>
            <ta e="T180" id="Seg_5252" s="T179">kola</ta>
            <ta e="T181" id="Seg_5253" s="T180">dʼabə-bi-jəʔ</ta>
            <ta e="T182" id="Seg_5254" s="T181">tʼo-Tə</ta>
            <ta e="T183" id="Seg_5255" s="T182">tʼo-Tə</ta>
            <ta e="T186" id="Seg_5256" s="T185">hen-bi-jəʔ</ta>
            <ta e="T187" id="Seg_5257" s="T186">dĭgəttə</ta>
            <ta e="T190" id="Seg_5258" s="T189">tustʼar-bi-jəʔ</ta>
            <ta e="T191" id="Seg_5259" s="T190">dĭgəttə</ta>
            <ta e="T192" id="Seg_5260" s="T191">sădar-bi-jəʔ</ta>
            <ta e="T193" id="Seg_5261" s="T192">kabar</ta>
            <ta e="T194" id="Seg_5262" s="T193">šišəge</ta>
            <ta e="T195" id="Seg_5263" s="T194">mo-laːm-bi</ta>
            <ta e="T196" id="Seg_5264" s="T195">ejü</ta>
            <ta e="T197" id="Seg_5265" s="T196">kan-lAʔ</ta>
            <ta e="T198" id="Seg_5266" s="T197">tʼür-bi</ta>
            <ta e="T199" id="Seg_5267" s="T198">kuja</ta>
            <ta e="T200" id="Seg_5268" s="T199">amka</ta>
            <ta e="T201" id="Seg_5269" s="T200">măndo-laʔbə</ta>
            <ta e="T202" id="Seg_5270" s="T201">sĭri</ta>
            <ta e="T203" id="Seg_5271" s="T202">šonə-gA</ta>
            <ta e="T204" id="Seg_5272" s="T203">beržə</ta>
            <ta e="T205" id="Seg_5273" s="T204">bar</ta>
            <ta e="T206" id="Seg_5274" s="T205">ugaːndə</ta>
            <ta e="T207" id="Seg_5275" s="T206">šišəge</ta>
            <ta e="T208" id="Seg_5276" s="T207">măn</ta>
            <ta e="T209" id="Seg_5277" s="T208">pa</ta>
            <ta e="T210" id="Seg_5278" s="T209">iʔgö</ta>
            <ta e="T211" id="Seg_5279" s="T210">dĭ</ta>
            <ta e="T212" id="Seg_5280" s="T211">kö-Tə</ta>
            <ta e="T213" id="Seg_5281" s="T212">kabar-lV-j</ta>
            <ta e="T214" id="Seg_5282" s="T213">ĭmbi</ta>
            <ta e="T215" id="Seg_5283" s="T214">ulu-l</ta>
            <ta e="T216" id="Seg_5284" s="T215">tĭl-liA-l</ta>
            <ta e="T217" id="Seg_5285" s="T216">padʼi</ta>
            <ta e="T218" id="Seg_5286" s="T217">unu-jəʔ</ta>
            <ta e="T219" id="Seg_5287" s="T218">i-gA</ta>
            <ta e="T220" id="Seg_5288" s="T219">ugaːndə</ta>
            <ta e="T221" id="Seg_5289" s="T220">iʔgö</ta>
            <ta e="T222" id="Seg_5290" s="T221">unu</ta>
            <ta e="T223" id="Seg_5291" s="T222">kut-zittə</ta>
            <ta e="T224" id="Seg_5292" s="T223">nadə</ta>
            <ta e="T225" id="Seg_5293" s="T224">dĭ-zAŋ</ta>
            <ta e="T226" id="Seg_5294" s="T225">kabarləj</ta>
            <ta e="T227" id="Seg_5295" s="T226">măn</ta>
            <ta e="T228" id="Seg_5296" s="T227">iʔbə-bi-m</ta>
            <ta e="T229" id="Seg_5297" s="T228">kunol-zittə</ta>
            <ta e="T230" id="Seg_5298" s="T229">kuriza</ta>
            <ta e="T231" id="Seg_5299" s="T230">ej</ta>
            <ta e="T232" id="Seg_5300" s="T231">mĭ-bi</ta>
            <ta e="T233" id="Seg_5301" s="T232">kunol-zittə</ta>
            <ta e="T234" id="Seg_5302" s="T233">măn</ta>
            <ta e="T235" id="Seg_5303" s="T234">uʔbdə-bi-m</ta>
            <ta e="T236" id="Seg_5304" s="T235">i</ta>
            <ta e="T239" id="Seg_5305" s="T238">daška</ta>
            <ta e="T240" id="Seg_5306" s="T239">ej</ta>
            <ta e="T241" id="Seg_5307" s="T240">iʔbə-bi-m</ta>
            <ta e="T242" id="Seg_5308" s="T241">kuvas</ta>
            <ta e="T243" id="Seg_5309" s="T242">nüke</ta>
            <ta e="T244" id="Seg_5310" s="T243">amno-laʔbə</ta>
            <ta e="T245" id="Seg_5311" s="T244">kuvas</ta>
            <ta e="T246" id="Seg_5312" s="T245">nʼi</ta>
            <ta e="T247" id="Seg_5313" s="T246">amno-laʔbə</ta>
            <ta e="T248" id="Seg_5314" s="T247">măn</ta>
            <ta e="T249" id="Seg_5315" s="T248">ugaːndə</ta>
            <ta e="T250" id="Seg_5316" s="T249">tʼaktə</ta>
            <ta e="T251" id="Seg_5317" s="T250">mo-laːm-bi-m</ta>
            <ta e="T252" id="Seg_5318" s="T251">šide-göʔ</ta>
            <ta e="T253" id="Seg_5319" s="T252">ma-laʔpi-bAʔ</ta>
            <ta e="T254" id="Seg_5320" s="T253">măn</ta>
            <ta e="T255" id="Seg_5321" s="T254">da</ta>
            <ta e="T256" id="Seg_5322" s="T255">kaga-m</ta>
            <ta e="T260" id="Seg_5323" s="T259">a</ta>
            <ta e="T261" id="Seg_5324" s="T260">dĭ-zAŋ</ta>
            <ta e="T262" id="Seg_5325" s="T261">bar</ta>
            <ta e="T263" id="Seg_5326" s="T262">kü-laːm-bi-jəʔ</ta>
            <ta e="T264" id="Seg_5327" s="T263">bar</ta>
            <ta e="T270" id="Seg_5328" s="T269">iʔgö</ta>
            <ta e="T271" id="Seg_5329" s="T270">pʼe</ta>
            <ta e="T272" id="Seg_5330" s="T271">kan-bi</ta>
            <ta e="T273" id="Seg_5331" s="T272">bar</ta>
            <ta e="T274" id="Seg_5332" s="T273">bor</ta>
            <ta e="T275" id="Seg_5333" s="T274">tʼo-Tə</ta>
            <ta e="T278" id="Seg_5334" s="T277">amnə-liA-l</ta>
            <ta e="T279" id="Seg_5335" s="T278">surno</ta>
            <ta e="T280" id="Seg_5336" s="T279">mo-lV-j</ta>
            <ta e="T281" id="Seg_5337" s="T280">uda-m</ta>
            <ta e="T282" id="Seg_5338" s="T281">dʼükəm-liA</ta>
            <ta e="T283" id="Seg_5339" s="T282">nadə</ta>
            <ta e="T284" id="Seg_5340" s="T283">kădə-zittə</ta>
            <ta e="T285" id="Seg_5341" s="T284">tăn</ta>
            <ta e="T286" id="Seg_5342" s="T285">ugaːndə</ta>
            <ta e="T289" id="Seg_5343" s="T288">šĭkə-l</ta>
            <ta e="T290" id="Seg_5344" s="T289">numo</ta>
            <ta e="T291" id="Seg_5345" s="T290">kudo-nzə-liA-l</ta>
            <ta e="T292" id="Seg_5346" s="T291">tăn</ta>
            <ta e="T293" id="Seg_5347" s="T292">tăn</ta>
            <ta e="T294" id="Seg_5348" s="T293">tĭme-l</ta>
            <ta e="T295" id="Seg_5349" s="T294">ugaːndə</ta>
            <ta e="T296" id="Seg_5350" s="T295">numo</ta>
            <ta e="T297" id="Seg_5351" s="T296">püje-l</ta>
            <ta e="T300" id="Seg_5352" s="T299">numo</ta>
            <ta e="T301" id="Seg_5353" s="T300">kuʔ-l</ta>
            <ta e="T302" id="Seg_5354" s="T301">bar</ta>
            <ta e="T303" id="Seg_5355" s="T302">mʼaŋ-laʔbə</ta>
            <ta e="T304" id="Seg_5356" s="T303">mon-zAŋ-m</ta>
            <ta e="T307" id="Seg_5357" s="T306">mon-zAŋ-l</ta>
            <ta e="T308" id="Seg_5358" s="T307">nʼešpək</ta>
            <ta e="T311" id="Seg_5359" s="T310">ugaːndə</ta>
            <ta e="T312" id="Seg_5360" s="T311">eʔbdə-l</ta>
            <ta e="T313" id="Seg_5361" s="T312">numo</ta>
            <ta e="T314" id="Seg_5362" s="T313">ugaːndə</ta>
            <ta e="T315" id="Seg_5363" s="T314">ku-zAŋ-l</ta>
            <ta e="T316" id="Seg_5364" s="T315">numo</ta>
            <ta e="T317" id="Seg_5365" s="T316">sima-l</ta>
            <ta e="T318" id="Seg_5366" s="T317">urgo</ta>
            <ta e="T319" id="Seg_5367" s="T318">ulu-l</ta>
            <ta e="T320" id="Seg_5368" s="T319">ugaːndə</ta>
            <ta e="T321" id="Seg_5369" s="T320">urgo</ta>
            <ta e="T324" id="Seg_5370" s="T323">uda-zAŋ-l</ta>
            <ta e="T325" id="Seg_5371" s="T324">numo</ta>
            <ta e="T328" id="Seg_5372" s="T327">üjü-zAŋ-l</ta>
            <ta e="T329" id="Seg_5373" s="T328">numo</ta>
            <ta e="T330" id="Seg_5374" s="T329">bos-də</ta>
            <ta e="T331" id="Seg_5375" s="T330">bar</ta>
            <ta e="T332" id="Seg_5376" s="T331">todam</ta>
            <ta e="T333" id="Seg_5377" s="T332">müjə</ta>
            <ta e="T334" id="Seg_5378" s="T333">nʼergö-laʔbə</ta>
            <ta e="T335" id="Seg_5379" s="T334">pa-Tə</ta>
            <ta e="T336" id="Seg_5380" s="T335">amnə-bi</ta>
            <ta e="T337" id="Seg_5381" s="T336">iʔgö</ta>
            <ta e="T342" id="Seg_5382" s="T341">süjö-jəʔ</ta>
            <ta e="T343" id="Seg_5383" s="T342">nʼergö-laʔbə-jəʔ</ta>
            <ta e="T344" id="Seg_5384" s="T343">karatʼgaj</ta>
            <ta e="T345" id="Seg_5385" s="T344">bar</ta>
            <ta e="T348" id="Seg_5386" s="T347">nʼilgö-laʔbə</ta>
            <ta e="T349" id="Seg_5387" s="T348">pogəraš</ta>
            <ta e="T350" id="Seg_5388" s="T349">nʼilgö-lAʔ</ta>
            <ta e="T353" id="Seg_5389" s="T352">šo-bi</ta>
            <ta e="T354" id="Seg_5390" s="T353">kuriza</ta>
            <ta e="T355" id="Seg_5391" s="T354">bar</ta>
            <ta e="T356" id="Seg_5392" s="T355">kădə-liA</ta>
            <ta e="T357" id="Seg_5393" s="T356">am-zittə</ta>
            <ta e="T358" id="Seg_5394" s="T357">măndo-r-liA</ta>
            <ta e="T359" id="Seg_5395" s="T358">nöməl-bi-m</ta>
            <ta e="T360" id="Seg_5396" s="T359">bar</ta>
            <ta e="T363" id="Seg_5397" s="T362">bü-m</ta>
            <ta e="T364" id="Seg_5398" s="T363">nuldə-zittə</ta>
            <ta e="T365" id="Seg_5399" s="T364">pʼeːš-Tə</ta>
            <ta e="T366" id="Seg_5400" s="T365">dʼibige</ta>
            <ta e="T367" id="Seg_5401" s="T366">bü</ta>
            <ta e="T368" id="Seg_5402" s="T367">naga</ta>
            <ta e="T371" id="Seg_5403" s="T370">măna</ta>
            <ta e="T372" id="Seg_5404" s="T371">kereʔ</ta>
            <ta e="T373" id="Seg_5405" s="T372">oroma</ta>
            <ta e="T376" id="Seg_5406" s="T375">kămnə-lV-m</ta>
            <ta e="T377" id="Seg_5407" s="T376">bargari-m</ta>
            <ta e="T378" id="Seg_5408" s="T377">dĭgəttə</ta>
            <ta e="T379" id="Seg_5409" s="T378">bazə-zittə</ta>
            <ta e="T380" id="Seg_5410" s="T379">nadə</ta>
            <ta e="T381" id="Seg_5411" s="T380">takše-jəʔ</ta>
            <ta e="T382" id="Seg_5412" s="T381">sĭri</ta>
            <ta e="T383" id="Seg_5413" s="T382">pa</ta>
            <ta e="T384" id="Seg_5414" s="T383">šomne</ta>
            <ta e="T385" id="Seg_5415" s="T384">pa</ta>
            <ta e="T386" id="Seg_5416" s="T385">kömə</ta>
            <ta e="T387" id="Seg_5417" s="T386">pa</ta>
            <ta e="T388" id="Seg_5418" s="T387">sanə</ta>
            <ta e="T389" id="Seg_5419" s="T388">pa</ta>
            <ta e="T390" id="Seg_5420" s="T389">sagən</ta>
            <ta e="T391" id="Seg_5421" s="T390">pa-Kən</ta>
            <ta e="T392" id="Seg_5422" s="T391">bar</ta>
            <ta e="T393" id="Seg_5423" s="T392">šiška-jəʔ</ta>
            <ta e="T394" id="Seg_5424" s="T393">özer-laʔbə-jəʔ</ta>
            <ta e="T395" id="Seg_5425" s="T394">sanə</ta>
            <ta e="T398" id="Seg_5426" s="T397">pa-Kən</ta>
            <ta e="T399" id="Seg_5427" s="T398">bar</ta>
            <ta e="T400" id="Seg_5428" s="T399">šiška-jəʔ</ta>
            <ta e="T401" id="Seg_5429" s="T400">amno-laʔbə-jəʔ</ta>
            <ta e="T402" id="Seg_5430" s="T401">sĭri</ta>
            <ta e="T403" id="Seg_5431" s="T402">pa-Kən</ta>
            <ta e="T404" id="Seg_5432" s="T403">bar</ta>
            <ta e="T405" id="Seg_5433" s="T404">takše</ta>
            <ta e="T406" id="Seg_5434" s="T405">a-laʔbə-jəʔ</ta>
            <ta e="T407" id="Seg_5435" s="T406">segi</ta>
            <ta e="T408" id="Seg_5436" s="T407">bü</ta>
            <ta e="T409" id="Seg_5437" s="T408">bĭs-laʔbə-jəʔ</ta>
            <ta e="T410" id="Seg_5438" s="T409">pa-j</ta>
            <ta e="T411" id="Seg_5439" s="T410">pa-j</ta>
            <ta e="T412" id="Seg_5440" s="T411">šamnak</ta>
            <ta e="T413" id="Seg_5441" s="T412">baza-j</ta>
            <ta e="T414" id="Seg_5442" s="T413">šamnak</ta>
            <ta e="T415" id="Seg_5443" s="T414">takše</ta>
            <ta e="T416" id="Seg_5444" s="T415">baza-j</ta>
            <ta e="T417" id="Seg_5445" s="T416">ugaːndə</ta>
            <ta e="T418" id="Seg_5446" s="T417">pa-jəʔ</ta>
            <ta e="T419" id="Seg_5447" s="T418">iʔgö</ta>
            <ta e="T420" id="Seg_5448" s="T419">dĭ-zAŋ</ta>
            <ta e="T421" id="Seg_5449" s="T420">nadə</ta>
            <ta e="T422" id="Seg_5450" s="T421">hʼaʔ-zittə</ta>
            <ta e="T423" id="Seg_5451" s="T422">dĭgəttə</ta>
            <ta e="T424" id="Seg_5452" s="T423">kür-zittə</ta>
            <ta e="T425" id="Seg_5453" s="T424">dĭgəttə</ta>
            <ta e="T426" id="Seg_5454" s="T425">tura</ta>
            <ta e="T427" id="Seg_5455" s="T426">kaj-zittə</ta>
            <ta e="T428" id="Seg_5456" s="T427">dĭgəttə</ta>
            <ta e="T429" id="Seg_5457" s="T428">dĭ</ta>
            <ta e="T430" id="Seg_5458" s="T429">pa-jəʔ-ziʔ</ta>
            <ta e="T431" id="Seg_5459" s="T430">tura</ta>
            <ta e="T432" id="Seg_5460" s="T431">hʼaʔ-zittə</ta>
            <ta e="T433" id="Seg_5461" s="T432">nadə</ta>
            <ta e="T434" id="Seg_5462" s="T433">amno-zittə</ta>
            <ta e="T435" id="Seg_5463" s="T434">tura-Kən</ta>
            <ta e="T436" id="Seg_5464" s="T435">pʼeːš</ta>
            <ta e="T437" id="Seg_5465" s="T436">hen-zittə</ta>
            <ta e="T438" id="Seg_5466" s="T437">pʼeːš-Kən</ta>
            <ta e="T439" id="Seg_5467" s="T438">pür-zittə</ta>
            <ta e="T440" id="Seg_5468" s="T439">ipek</ta>
            <ta e="T441" id="Seg_5469" s="T440">bĭs-ə-ʔ</ta>
            <ta e="T442" id="Seg_5470" s="T441">bü</ta>
            <ta e="T443" id="Seg_5471" s="T442">măn</ta>
            <ta e="T444" id="Seg_5472" s="T443">dĭbər</ta>
            <ta e="T447" id="Seg_5473" s="T446">hen-bi-m</ta>
            <ta e="T448" id="Seg_5474" s="T447">sĭreʔp</ta>
            <ta e="T451" id="Seg_5475" s="T450">bü</ta>
            <ta e="T452" id="Seg_5476" s="T451">kămnə-bi-m</ta>
            <ta e="T455" id="Seg_5477" s="T454">namzəbi</ta>
            <ta e="T456" id="Seg_5478" s="T455">bar</ta>
            <ta e="T457" id="Seg_5479" s="T456">ipek</ta>
            <ta e="T458" id="Seg_5480" s="T457">hen-ntə-bi-m</ta>
            <ta e="T459" id="Seg_5481" s="T458">bĭs-ə-ʔ</ta>
            <ta e="T460" id="Seg_5482" s="T459">ugaːndə</ta>
            <ta e="T461" id="Seg_5483" s="T460">jakšə</ta>
            <ta e="T462" id="Seg_5484" s="T461">šiʔ</ta>
            <ta e="T463" id="Seg_5485" s="T462">taldʼen</ta>
            <ta e="T464" id="Seg_5486" s="T463">ara</ta>
            <ta e="T465" id="Seg_5487" s="T464">bĭs-bi-lAʔ</ta>
            <ta e="T468" id="Seg_5488" s="T467">jezerik</ta>
            <ta e="T469" id="Seg_5489" s="T468">i-bi-lAʔ</ta>
            <ta e="T470" id="Seg_5490" s="T469">bar</ta>
            <ta e="T474" id="Seg_5491" s="T472">teinen</ta>
            <ta e="T475" id="Seg_5492" s="T474">bar</ta>
            <ta e="T478" id="Seg_5493" s="T477">bĭs-ə-ʔ</ta>
            <ta e="T479" id="Seg_5494" s="T478">kömə</ta>
            <ta e="T480" id="Seg_5495" s="T479">bü</ta>
            <ta e="T481" id="Seg_5496" s="T480">măn</ta>
            <ta e="T482" id="Seg_5497" s="T481">dĭbər</ta>
            <ta e="T485" id="Seg_5498" s="T484">sĭreʔp</ta>
            <ta e="T486" id="Seg_5499" s="T485">hen-bi-m</ta>
            <ta e="T488" id="Seg_5500" s="T487">bü</ta>
            <ta e="T489" id="Seg_5501" s="T488">kămnə-bi-m</ta>
            <ta e="T492" id="Seg_5502" s="T491">namzəga</ta>
            <ta e="T493" id="Seg_5503" s="T492">mo-laːm-bi</ta>
            <ta e="T494" id="Seg_5504" s="T493">jakšə</ta>
            <ta e="T495" id="Seg_5505" s="T494">mo-lV-j</ta>
            <ta e="T496" id="Seg_5506" s="T495">tănan</ta>
            <ta e="T497" id="Seg_5507" s="T496">ara</ta>
            <ta e="T498" id="Seg_5508" s="T497">e-ʔ</ta>
            <ta e="T499" id="Seg_5509" s="T498">bĭs-ə-ʔ</ta>
            <ta e="T500" id="Seg_5510" s="T499">ato</ta>
            <ta e="T501" id="Seg_5511" s="T500">ulu-l</ta>
            <ta e="T502" id="Seg_5512" s="T501">ĭzem-liA</ta>
            <ta e="T503" id="Seg_5513" s="T502">sĭj-l</ta>
            <ta e="T504" id="Seg_5514" s="T503">ĭzem-liA</ta>
            <ta e="T505" id="Seg_5515" s="T504">bĭs-lV-l</ta>
            <ta e="T506" id="Seg_5516" s="T505">tak</ta>
            <ta e="T507" id="Seg_5517" s="T506">kü-laːm-lV-l</ta>
            <ta e="T508" id="Seg_5518" s="T507">a</ta>
            <ta e="T509" id="Seg_5519" s="T508">ej</ta>
            <ta e="T510" id="Seg_5520" s="T509">bĭs-lV-l</ta>
            <ta e="T511" id="Seg_5521" s="T510">tak</ta>
            <ta e="T512" id="Seg_5522" s="T511">kondʼo</ta>
            <ta e="T513" id="Seg_5523" s="T512">amno-lV-l</ta>
            <ta e="T514" id="Seg_5524" s="T513">mašina</ta>
            <ta e="T519" id="Seg_5525" s="T518">tajər-laʔbə</ta>
            <ta e="T520" id="Seg_5526" s="T519">dĭn</ta>
            <ta e="T521" id="Seg_5527" s="T520">kuza</ta>
            <ta e="T522" id="Seg_5528" s="T521">amno-laʔbə</ta>
            <ta e="T523" id="Seg_5529" s="T522">mašina-gəʔ</ta>
            <ta e="T524" id="Seg_5530" s="T523">bar</ta>
            <ta e="T525" id="Seg_5531" s="T524">bor</ta>
            <ta e="T526" id="Seg_5532" s="T525">šonə-gA</ta>
            <ta e="T527" id="Seg_5533" s="T526">dĭ</ta>
            <ta e="T528" id="Seg_5534" s="T527">bar</ta>
            <ta e="T529" id="Seg_5535" s="T528">dĭbər</ta>
            <ta e="T531" id="Seg_5536" s="T530">döbər</ta>
            <ta e="T532" id="Seg_5537" s="T531">kan-laʔbə</ta>
            <ta e="T533" id="Seg_5538" s="T532">aba-m</ta>
            <ta e="T534" id="Seg_5539" s="T533">ija-m</ta>
            <ta e="T535" id="Seg_5540" s="T534">koʔbdo</ta>
            <ta e="T536" id="Seg_5541" s="T535">ku-bi-jəʔ</ta>
            <ta e="T537" id="Seg_5542" s="T536">măn</ta>
            <ta e="T538" id="Seg_5543" s="T537">măna</ta>
            <ta e="T539" id="Seg_5544" s="T538">măn-liA-jəʔ</ta>
            <ta e="T540" id="Seg_5545" s="T539">kan-ə-ʔ</ta>
            <ta e="T541" id="Seg_5546" s="T540">dĭ-ziʔ</ta>
            <ta e="T544" id="Seg_5547" s="T543">tĭmne-t</ta>
            <ta e="T545" id="Seg_5548" s="T544">girgit</ta>
            <ta e="T546" id="Seg_5549" s="T545">dĭ</ta>
            <ta e="T547" id="Seg_5550" s="T546">dĭgəttə</ta>
            <ta e="T548" id="Seg_5551" s="T547">tibi-Tə</ta>
            <ta e="T549" id="Seg_5552" s="T548">i-liA-l</ta>
            <ta e="T550" id="Seg_5553" s="T549">dĭgəttə</ta>
            <ta e="T551" id="Seg_5554" s="T550">ešši-zAŋ-Tə</ta>
            <ta e="T552" id="Seg_5555" s="T551">mo-lV-jəʔ</ta>
            <ta e="T554" id="Seg_5556" s="T553">ešši-zAŋ-l</ta>
            <ta e="T555" id="Seg_5557" s="T554">özer-lV-jəʔ</ta>
            <ta e="T556" id="Seg_5558" s="T555">tănan</ta>
            <ta e="T557" id="Seg_5559" s="T556">kabažar-lV-jəʔ</ta>
            <ta e="T558" id="Seg_5560" s="T557">ne-m</ta>
            <ta e="T563" id="Seg_5561" s="T562">e-ʔ</ta>
            <ta e="T564" id="Seg_5562" s="T563">münör-ə-ʔ</ta>
            <ta e="T565" id="Seg_5563" s="T564">nʼilgö-t</ta>
            <ta e="T570" id="Seg_5564" s="T569">aba-m</ta>
            <ta e="T571" id="Seg_5565" s="T570">da</ta>
            <ta e="T572" id="Seg_5566" s="T571">ija-m</ta>
            <ta e="T573" id="Seg_5567" s="T572">nʼilgö-t</ta>
            <ta e="T578" id="Seg_5568" s="T577">teinen</ta>
            <ta e="T579" id="Seg_5569" s="T578">bar</ta>
            <ta e="T580" id="Seg_5570" s="T579">kan-lAʔ</ta>
            <ta e="T581" id="Seg_5571" s="T580">tʼür-bi-jəʔ</ta>
            <ta e="T582" id="Seg_5572" s="T581">aba-t</ta>
            <ta e="T583" id="Seg_5573" s="T582">ija-t</ta>
            <ta e="T584" id="Seg_5574" s="T583">kan-lAʔ</ta>
            <ta e="T585" id="Seg_5575" s="T584">tʼür-bi-jəʔ</ta>
            <ta e="T586" id="Seg_5576" s="T585">ešši-zAŋ</ta>
            <ta e="T587" id="Seg_5577" s="T586">kan-lAʔ</ta>
            <ta e="T588" id="Seg_5578" s="T587">tʼür-bi-jəʔ</ta>
            <ta e="T589" id="Seg_5579" s="T588">măn</ta>
            <ta e="T590" id="Seg_5580" s="T589">unʼə</ta>
            <ta e="T591" id="Seg_5581" s="T590">maʔ-Tə-l</ta>
            <ta e="T592" id="Seg_5582" s="T591">amno-laʔbə-m</ta>
            <ta e="T593" id="Seg_5583" s="T592">nadə</ta>
            <ta e="T594" id="Seg_5584" s="T593">tʼo</ta>
            <ta e="T595" id="Seg_5585" s="T594">tĭl-zittə</ta>
            <ta e="T596" id="Seg_5586" s="T595">kuza</ta>
            <ta e="T597" id="Seg_5587" s="T596">kü-laːm-bi</ta>
            <ta e="T598" id="Seg_5588" s="T597">nadə</ta>
            <ta e="T599" id="Seg_5589" s="T598">dĭ-m</ta>
            <ta e="T600" id="Seg_5590" s="T599">dĭbər</ta>
            <ta e="T601" id="Seg_5591" s="T600">hen-zittə</ta>
            <ta e="T602" id="Seg_5592" s="T601">dĭgəttə</ta>
            <ta e="T603" id="Seg_5593" s="T602">kămnə-zittə</ta>
            <ta e="T606" id="Seg_5594" s="T605">dĭgəttə</ta>
            <ta e="T611" id="Seg_5595" s="T610">tʼor</ta>
            <ta e="T613" id="Seg_5596" s="T611">dĭgəttə</ta>
            <ta e="T614" id="Seg_5597" s="T613">dĭgəttə</ta>
            <ta e="T617" id="Seg_5598" s="T616">tʼo-ziʔ</ta>
            <ta e="T618" id="Seg_5599" s="T617">nadə</ta>
            <ta e="T619" id="Seg_5600" s="T618">kaj-zittə</ta>
            <ta e="T620" id="Seg_5601" s="T619">dĭ-m</ta>
            <ta e="T621" id="Seg_5602" s="T620">dĭgəttə</ta>
            <ta e="T622" id="Seg_5603" s="T621">maʔ-Tə-l</ta>
            <ta e="T625" id="Seg_5604" s="T624">šo-zittə</ta>
            <ta e="T626" id="Seg_5605" s="T625">am-zittə</ta>
            <ta e="T629" id="Seg_5606" s="T628">amnə-ʔ</ta>
            <ta e="T630" id="Seg_5607" s="T629">am-zittə</ta>
            <ta e="T631" id="Seg_5608" s="T630">kuza</ta>
            <ta e="T632" id="Seg_5609" s="T631">ĭzem-bi</ta>
            <ta e="T633" id="Seg_5610" s="T632">šaman-də</ta>
            <ta e="T634" id="Seg_5611" s="T633">mĭn-bi</ta>
            <ta e="T635" id="Seg_5612" s="T634">šaman</ta>
            <ta e="T636" id="Seg_5613" s="T635">ej</ta>
            <ta e="T637" id="Seg_5614" s="T636">mo-bi</ta>
            <ta e="T638" id="Seg_5615" s="T637">dĭ-m</ta>
            <ta e="T639" id="Seg_5616" s="T638">tʼezer-zittə</ta>
            <ta e="T643" id="Seg_5617" s="T641">kü-laːm-bi</ta>
            <ta e="T644" id="Seg_5618" s="T643">kumən</ta>
            <ta e="T645" id="Seg_5619" s="T644">ej</ta>
            <ta e="T646" id="Seg_5620" s="T645">ĭzem-bi</ta>
            <ta e="T647" id="Seg_5621" s="T646">tüj</ta>
            <ta e="T648" id="Seg_5622" s="T647">bar</ta>
            <ta e="T649" id="Seg_5623" s="T648">iʔbö-laʔbə</ta>
            <ta e="T651" id="Seg_5624" s="T649">ĭmbi-ziʔ=də</ta>
            <ta e="T652" id="Seg_5625" s="T651">ej</ta>
            <ta e="T653" id="Seg_5626" s="T652">mĭnžə-liA</ta>
            <ta e="T654" id="Seg_5627" s="T653">măna</ta>
            <ta e="T655" id="Seg_5628" s="T654">bar</ta>
            <ta e="T656" id="Seg_5629" s="T655">münör-bi-jəʔ</ta>
            <ta e="T657" id="Seg_5630" s="T656">ugaːndə</ta>
            <ta e="T658" id="Seg_5631" s="T657">tăŋ</ta>
            <ta e="T659" id="Seg_5632" s="T658">bos-m</ta>
            <ta e="T660" id="Seg_5633" s="T659">ej</ta>
            <ta e="T661" id="Seg_5634" s="T660">tĭmne-m</ta>
            <ta e="T662" id="Seg_5635" s="T661">ĭmbi-ziʔ</ta>
            <ta e="T663" id="Seg_5636" s="T662">münör-bi-jəʔ</ta>
            <ta e="T666" id="Seg_5637" s="T665">măn</ta>
            <ta e="T667" id="Seg_5638" s="T666">măn</ta>
            <ta e="T668" id="Seg_5639" s="T667">bar</ta>
            <ta e="T669" id="Seg_5640" s="T668">ĭzem-bi</ta>
            <ta e="T670" id="Seg_5641" s="T669">ĭzem-liA-m</ta>
            <ta e="T671" id="Seg_5642" s="T670">ulu-m</ta>
            <ta e="T672" id="Seg_5643" s="T671">ĭzem-liA</ta>
            <ta e="T673" id="Seg_5644" s="T672">ku-m</ta>
            <ta e="T674" id="Seg_5645" s="T673">ĭzem-liA</ta>
            <ta e="T675" id="Seg_5646" s="T674">sima-m</ta>
            <ta e="T676" id="Seg_5647" s="T675">ĭzem-liA</ta>
            <ta e="T677" id="Seg_5648" s="T676">tĭme-m</ta>
            <ta e="T678" id="Seg_5649" s="T677">ĭzem-liA</ta>
            <ta e="T679" id="Seg_5650" s="T678">püje-m</ta>
            <ta e="T680" id="Seg_5651" s="T679">ĭzem-liA</ta>
            <ta e="T681" id="Seg_5652" s="T680">uda-m</ta>
            <ta e="T682" id="Seg_5653" s="T681">ĭzem-liA</ta>
            <ta e="T683" id="Seg_5654" s="T682">üjü-m</ta>
            <ta e="T684" id="Seg_5655" s="T683">ĭzem-liA</ta>
            <ta e="T685" id="Seg_5656" s="T684">nanə-m</ta>
            <ta e="T686" id="Seg_5657" s="T685">ĭzem-liA</ta>
            <ta e="T687" id="Seg_5658" s="T686">sĭj-m</ta>
            <ta e="T688" id="Seg_5659" s="T687">ĭzem-liA</ta>
            <ta e="T689" id="Seg_5660" s="T688">bukle-bə</ta>
            <ta e="T690" id="Seg_5661" s="T689">ĭzem-liA</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PKZ">
            <ta e="T5" id="Seg_5662" s="T4">woman-PL</ta>
            <ta e="T6" id="Seg_5663" s="T5">very</ta>
            <ta e="T7" id="Seg_5664" s="T6">many</ta>
            <ta e="T8" id="Seg_5665" s="T7">come-PRS-3PL</ta>
            <ta e="T9" id="Seg_5666" s="T8">I.NOM</ta>
            <ta e="T10" id="Seg_5667" s="T9">say-PST-1SG</ta>
            <ta e="T11" id="Seg_5668" s="T10">where</ta>
            <ta e="T12" id="Seg_5669" s="T11">be-PST-2PL</ta>
            <ta e="T13" id="Seg_5670" s="T12">this-PL</ta>
            <ta e="T14" id="Seg_5671" s="T13">say-PRS-3PL</ta>
            <ta e="T15" id="Seg_5672" s="T14">cow.[NOM.SG]</ta>
            <ta e="T16" id="Seg_5673" s="T15">milk-INF.LAT</ta>
            <ta e="T17" id="Seg_5674" s="T16">go-PST-1PL</ta>
            <ta e="T18" id="Seg_5675" s="T17">bring-IMP.2PL</ta>
            <ta e="T19" id="Seg_5676" s="T18">I.LAT</ta>
            <ta e="T20" id="Seg_5677" s="T19">milk.[NOM.SG]</ta>
            <ta e="T21" id="Seg_5678" s="T20">enough</ta>
            <ta e="T22" id="Seg_5679" s="T21">else</ta>
            <ta e="T23" id="Seg_5680" s="T22">child.[NOM.SG]</ta>
            <ta e="T24" id="Seg_5681" s="T23">all</ta>
            <ta e="T25" id="Seg_5682" s="T24">shit-PST.[3SG]</ta>
            <ta e="T26" id="Seg_5683" s="T25">very</ta>
            <ta e="T27" id="Seg_5684" s="T26">black.[NOM.SG]</ta>
            <ta e="T28" id="Seg_5685" s="T27">all</ta>
            <ta e="T29" id="Seg_5686" s="T28">become.dry-MOM-PST.[3SG]</ta>
            <ta e="T30" id="Seg_5687" s="T29">one.should</ta>
            <ta e="T31" id="Seg_5688" s="T30">wash-INF.LAT</ta>
            <ta e="T32" id="Seg_5689" s="T31">this-ACC</ta>
            <ta e="T35" id="Seg_5690" s="T34">rub-INF.LAT</ta>
            <ta e="T36" id="Seg_5691" s="T35">then</ta>
            <ta e="T37" id="Seg_5692" s="T36">wash-INF.LAT</ta>
            <ta e="T38" id="Seg_5693" s="T37">one.should</ta>
            <ta e="T41" id="Seg_5694" s="T40">peel.bark-INF.LAT</ta>
            <ta e="T42" id="Seg_5695" s="T41">one.should</ta>
            <ta e="T43" id="Seg_5696" s="T42">then</ta>
            <ta e="T44" id="Seg_5697" s="T43">eat-INF.LAT</ta>
            <ta e="T45" id="Seg_5698" s="T44">give-INF.LAT</ta>
            <ta e="T46" id="Seg_5699" s="T45">then</ta>
            <ta e="T47" id="Seg_5700" s="T46">sleep-INF.LAT</ta>
            <ta e="T50" id="Seg_5701" s="T49">lie.down</ta>
            <ta e="T51" id="Seg_5702" s="T50">one.should</ta>
            <ta e="T52" id="Seg_5703" s="T51">lull-IMP.2SG</ta>
            <ta e="T53" id="Seg_5704" s="T52">child-NOM/GEN/ACC.1SG</ta>
            <ta e="T54" id="Seg_5705" s="T53">all</ta>
            <ta e="T55" id="Seg_5706" s="T54">sit-IMP.2SG</ta>
            <ta e="T56" id="Seg_5707" s="T55">house-LOC</ta>
            <ta e="T57" id="Seg_5708" s="T56">I.NOM</ta>
            <ta e="T58" id="Seg_5709" s="T57">outwards</ta>
            <ta e="T59" id="Seg_5710" s="T58">go-FUT-1SG</ta>
            <ta e="T60" id="Seg_5711" s="T59">piss-INF.LAT</ta>
            <ta e="T61" id="Seg_5712" s="T60">shit-INF.LAT</ta>
            <ta e="T62" id="Seg_5713" s="T61">then</ta>
            <ta e="T63" id="Seg_5714" s="T62">come-FUT-1SG</ta>
            <ta e="T64" id="Seg_5715" s="T63">sit-FUT-1SG</ta>
            <ta e="T65" id="Seg_5716" s="T64">you.NOM-INS</ta>
            <ta e="T66" id="Seg_5717" s="T65">and</ta>
            <ta e="T67" id="Seg_5718" s="T66">speak-FUT-1PL</ta>
            <ta e="T68" id="Seg_5719" s="T67">enough</ta>
            <ta e="T69" id="Seg_5720" s="T68">I.NOM</ta>
            <ta e="T70" id="Seg_5721" s="T69">ass-NOM/GEN/ACC.1SG</ta>
            <ta e="T71" id="Seg_5722" s="T70">beautiful.[NOM.SG]</ta>
            <ta e="T72" id="Seg_5723" s="T71">you.GEN</ta>
            <ta e="T73" id="Seg_5724" s="T72">face-NOM/GEN/ACC.3SG</ta>
            <ta e="T76" id="Seg_5725" s="T75">very</ta>
            <ta e="T77" id="Seg_5726" s="T76">thick.[NOM.SG]</ta>
            <ta e="T78" id="Seg_5727" s="T77">ass.[NOM.SG]</ta>
            <ta e="T79" id="Seg_5728" s="T78">very</ta>
            <ta e="T80" id="Seg_5729" s="T79">black.[NOM.SG]</ta>
            <ta e="T81" id="Seg_5730" s="T80">ass.[NOM.SG]</ta>
            <ta e="T82" id="Seg_5731" s="T81">very</ta>
            <ta e="T83" id="Seg_5732" s="T82">red.[NOM.SG]</ta>
            <ta e="T84" id="Seg_5733" s="T83">ass.[NOM.SG]</ta>
            <ta e="T85" id="Seg_5734" s="T84">very</ta>
            <ta e="T86" id="Seg_5735" s="T85">snow.[NOM.SG]</ta>
            <ta e="T87" id="Seg_5736" s="T86">ass.[NOM.SG]</ta>
            <ta e="T88" id="Seg_5737" s="T87">very</ta>
            <ta e="T89" id="Seg_5738" s="T88">thick.[NOM.SG]</ta>
            <ta e="T90" id="Seg_5739" s="T89">ass.[NOM.SG]</ta>
            <ta e="T91" id="Seg_5740" s="T90">very</ta>
            <ta e="T92" id="Seg_5741" s="T91">thin.[NOM.SG]</ta>
            <ta e="T94" id="Seg_5742" s="T93">ass.[NOM.SG]</ta>
            <ta e="T95" id="Seg_5743" s="T94">enough</ta>
            <ta e="T96" id="Seg_5744" s="T95">child.[NOM.SG]</ta>
            <ta e="T97" id="Seg_5745" s="T96">come-PRS-2SG</ta>
            <ta e="T98" id="Seg_5746" s="T97">hand-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T99" id="Seg_5747" s="T98">all</ta>
            <ta e="T100" id="Seg_5748" s="T99">dirty.[NOM.SG]</ta>
            <ta e="T101" id="Seg_5749" s="T100">one.should</ta>
            <ta e="T102" id="Seg_5750" s="T101">wash-INF.LAT</ta>
            <ta e="T103" id="Seg_5751" s="T102">come-IMP.2SG</ta>
            <ta e="T104" id="Seg_5752" s="T103">here</ta>
            <ta e="T105" id="Seg_5753" s="T104">NEG.AUX-IMP.2SG</ta>
            <ta e="T106" id="Seg_5754" s="T105">cry-EP-CNG</ta>
            <ta e="T107" id="Seg_5755" s="T106">go-EP-IMP.2SG</ta>
            <ta e="T108" id="Seg_5756" s="T107">tent-LAT-2SG</ta>
            <ta e="T111" id="Seg_5757" s="T110">you.DAT</ta>
            <ta e="T112" id="Seg_5758" s="T111">probably</ta>
            <ta e="T117" id="Seg_5759" s="T116">look-DUR-3PL</ta>
            <ta e="T118" id="Seg_5760" s="T117">PTCL</ta>
            <ta e="T119" id="Seg_5761" s="T118">otherwise</ta>
            <ta e="T120" id="Seg_5762" s="T119">duck-PL</ta>
            <ta e="T121" id="Seg_5763" s="T120">%%-3PL</ta>
            <ta e="T122" id="Seg_5764" s="T121">you.DAT</ta>
            <ta e="T123" id="Seg_5765" s="T122">all</ta>
            <ta e="T124" id="Seg_5766" s="T123">where.to</ta>
            <ta e="T125" id="Seg_5767" s="T124">climb-PRS-2SG</ta>
            <ta e="T141" id="Seg_5768" s="T140">where.to</ta>
            <ta e="T143" id="Seg_5769" s="T142">cook-DUR-PRS-2SG</ta>
            <ta e="T144" id="Seg_5770" s="T143">NEG.AUX-IMP.2SG</ta>
            <ta e="T145" id="Seg_5771" s="T144">climb-EP-CNG</ta>
            <ta e="T146" id="Seg_5772" s="T145">otherwise</ta>
            <ta e="T147" id="Seg_5773" s="T146">fall-MOM-FUT-2SG</ta>
            <ta e="T148" id="Seg_5774" s="T147">come-IMP.2SG</ta>
            <ta e="T149" id="Seg_5775" s="T148">here</ta>
            <ta e="T150" id="Seg_5776" s="T149">this.[NOM.SG]</ta>
            <ta e="T151" id="Seg_5777" s="T150">girl.[NOM.SG]</ta>
            <ta e="T152" id="Seg_5778" s="T151">sit-DUR.[3SG]</ta>
            <ta e="T153" id="Seg_5779" s="T152">thin.[NOM.SG]</ta>
            <ta e="T154" id="Seg_5780" s="T153">all</ta>
            <ta e="T159" id="Seg_5781" s="T158">what.[NOM.SG]</ta>
            <ta e="T160" id="Seg_5782" s="T159">say-IPFVZ-PRS-1SG</ta>
            <ta e="T161" id="Seg_5783" s="T160">this.[NOM.SG]</ta>
            <ta e="T162" id="Seg_5784" s="T161">NEG</ta>
            <ta e="T163" id="Seg_5785" s="T162">listen-DUR.[3SG]</ta>
            <ta e="T164" id="Seg_5786" s="T163">forest-LAT</ta>
            <ta e="T167" id="Seg_5787" s="T166">go-IPFVZ-PRS-3PL</ta>
            <ta e="T168" id="Seg_5788" s="T167">horse-PL-INS</ta>
            <ta e="T169" id="Seg_5789" s="T168">fish.[NOM.SG]</ta>
            <ta e="T170" id="Seg_5790" s="T169">capture-FUT-3PL</ta>
            <ta e="T171" id="Seg_5791" s="T170">fish.[NOM.SG]</ta>
            <ta e="T172" id="Seg_5792" s="T171">bring-FUT-3PL</ta>
            <ta e="T173" id="Seg_5793" s="T172">I.LAT</ta>
            <ta e="T174" id="Seg_5794" s="T173">maybe</ta>
            <ta e="T175" id="Seg_5795" s="T174">give-FUT-3PL</ta>
            <ta e="T176" id="Seg_5796" s="T175">enough</ta>
            <ta e="T178" id="Seg_5797" s="T177">river-LOC</ta>
            <ta e="T179" id="Seg_5798" s="T178">hand-INS</ta>
            <ta e="T180" id="Seg_5799" s="T179">fish.[NOM.SG]</ta>
            <ta e="T181" id="Seg_5800" s="T180">capture-PST-3PL</ta>
            <ta e="T182" id="Seg_5801" s="T181">place-LAT</ta>
            <ta e="T183" id="Seg_5802" s="T182">place-LAT</ta>
            <ta e="T186" id="Seg_5803" s="T185">put-PST-3PL</ta>
            <ta e="T187" id="Seg_5804" s="T186">then</ta>
            <ta e="T190" id="Seg_5805" s="T189">salt-PST-3PL</ta>
            <ta e="T191" id="Seg_5806" s="T190">then</ta>
            <ta e="T192" id="Seg_5807" s="T191">sell-PST-3PL</ta>
            <ta e="T193" id="Seg_5808" s="T192">grab</ta>
            <ta e="T194" id="Seg_5809" s="T193">cold.[NOM.SG]</ta>
            <ta e="T195" id="Seg_5810" s="T194">become-RES-PST.[3SG]</ta>
            <ta e="T196" id="Seg_5811" s="T195">warm.[NOM.SG]</ta>
            <ta e="T197" id="Seg_5812" s="T196">go-CVB</ta>
            <ta e="T198" id="Seg_5813" s="T197">disappear-PST.[3SG]</ta>
            <ta e="T199" id="Seg_5814" s="T198">sun.[NOM.SG]</ta>
            <ta e="T200" id="Seg_5815" s="T199">few</ta>
            <ta e="T201" id="Seg_5816" s="T200">shine-DUR.[3SG]</ta>
            <ta e="T202" id="Seg_5817" s="T201">snow.[NOM.SG]</ta>
            <ta e="T203" id="Seg_5818" s="T202">come-PRS.[3SG]</ta>
            <ta e="T204" id="Seg_5819" s="T203">wind.[NOM.SG]</ta>
            <ta e="T205" id="Seg_5820" s="T204">all</ta>
            <ta e="T206" id="Seg_5821" s="T205">very</ta>
            <ta e="T207" id="Seg_5822" s="T206">cold.[NOM.SG]</ta>
            <ta e="T208" id="Seg_5823" s="T207">I.NOM</ta>
            <ta e="T209" id="Seg_5824" s="T208">tree.[NOM.SG]</ta>
            <ta e="T210" id="Seg_5825" s="T209">many</ta>
            <ta e="T211" id="Seg_5826" s="T210">this.[NOM.SG]</ta>
            <ta e="T212" id="Seg_5827" s="T211">winter-LAT</ta>
            <ta e="T213" id="Seg_5828" s="T212">suffice-FUT-3SG</ta>
            <ta e="T214" id="Seg_5829" s="T213">what</ta>
            <ta e="T215" id="Seg_5830" s="T214">head-NOM/GEN/ACC.2SG</ta>
            <ta e="T216" id="Seg_5831" s="T215">dig-PRS-2SG</ta>
            <ta e="T217" id="Seg_5832" s="T216">probably</ta>
            <ta e="T218" id="Seg_5833" s="T217">louse-NOM/GEN/ACC.3PL</ta>
            <ta e="T219" id="Seg_5834" s="T218">be-PRS.[3SG]</ta>
            <ta e="T220" id="Seg_5835" s="T219">very</ta>
            <ta e="T221" id="Seg_5836" s="T220">many</ta>
            <ta e="T222" id="Seg_5837" s="T221">louse.[NOM.SG]</ta>
            <ta e="T223" id="Seg_5838" s="T222">kill-INF.LAT</ta>
            <ta e="T224" id="Seg_5839" s="T223">one.should</ta>
            <ta e="T225" id="Seg_5840" s="T224">this-PL</ta>
            <ta e="T226" id="Seg_5841" s="T225">enough</ta>
            <ta e="T227" id="Seg_5842" s="T226">I.NOM</ta>
            <ta e="T228" id="Seg_5843" s="T227">lie.down-PST-1SG</ta>
            <ta e="T229" id="Seg_5844" s="T228">sleep-INF.LAT</ta>
            <ta e="T230" id="Seg_5845" s="T229">hen</ta>
            <ta e="T231" id="Seg_5846" s="T230">NEG</ta>
            <ta e="T232" id="Seg_5847" s="T231">give-PST.[3SG]</ta>
            <ta e="T233" id="Seg_5848" s="T232">sleep-INF.LAT</ta>
            <ta e="T234" id="Seg_5849" s="T233">I.NOM</ta>
            <ta e="T235" id="Seg_5850" s="T234">get.up-PST-1SG</ta>
            <ta e="T236" id="Seg_5851" s="T235">and</ta>
            <ta e="T239" id="Seg_5852" s="T238">else</ta>
            <ta e="T240" id="Seg_5853" s="T239">NEG</ta>
            <ta e="T241" id="Seg_5854" s="T240">lie.down-PST-1SG</ta>
            <ta e="T242" id="Seg_5855" s="T241">beautiful.[NOM.SG]</ta>
            <ta e="T243" id="Seg_5856" s="T242">woman.[NOM.SG]</ta>
            <ta e="T244" id="Seg_5857" s="T243">sit-DUR.[3SG]</ta>
            <ta e="T245" id="Seg_5858" s="T244">beautiful.[NOM.SG]</ta>
            <ta e="T246" id="Seg_5859" s="T245">boy.[NOM.SG]</ta>
            <ta e="T247" id="Seg_5860" s="T246">sit-DUR.[3SG]</ta>
            <ta e="T248" id="Seg_5861" s="T247">I.NOM</ta>
            <ta e="T249" id="Seg_5862" s="T248">very</ta>
            <ta e="T250" id="Seg_5863" s="T249">old.[NOM.SG]</ta>
            <ta e="T251" id="Seg_5864" s="T250">become-RES-PST-1SG</ta>
            <ta e="T252" id="Seg_5865" s="T251">two-COLL</ta>
            <ta e="T253" id="Seg_5866" s="T252">remain-DUR.PST-1PL</ta>
            <ta e="T254" id="Seg_5867" s="T253">I.NOM</ta>
            <ta e="T255" id="Seg_5868" s="T254">and</ta>
            <ta e="T256" id="Seg_5869" s="T255">brother-NOM/GEN/ACC.1SG</ta>
            <ta e="T260" id="Seg_5870" s="T259">and</ta>
            <ta e="T261" id="Seg_5871" s="T260">this-PL</ta>
            <ta e="T262" id="Seg_5872" s="T261">PTCL</ta>
            <ta e="T263" id="Seg_5873" s="T262">die-RES-PST-3PL</ta>
            <ta e="T264" id="Seg_5874" s="T263">all</ta>
            <ta e="T270" id="Seg_5875" s="T269">many</ta>
            <ta e="T271" id="Seg_5876" s="T270">year.[NOM.SG]</ta>
            <ta e="T272" id="Seg_5877" s="T271">go-PST.[3SG]</ta>
            <ta e="T273" id="Seg_5878" s="T272">all</ta>
            <ta e="T274" id="Seg_5879" s="T273">smoke.[NOM.SG]</ta>
            <ta e="T275" id="Seg_5880" s="T274">place-LAT</ta>
            <ta e="T278" id="Seg_5881" s="T277">sit-PRS-2SG</ta>
            <ta e="T279" id="Seg_5882" s="T278">rain.[NOM.SG]</ta>
            <ta e="T280" id="Seg_5883" s="T279">become-FUT-3SG</ta>
            <ta e="T281" id="Seg_5884" s="T280">hand-NOM/GEN/ACC.1SG</ta>
            <ta e="T282" id="Seg_5885" s="T281">itch-PRS.[3SG]</ta>
            <ta e="T283" id="Seg_5886" s="T282">one.should</ta>
            <ta e="T284" id="Seg_5887" s="T283">scratch-INF.LAT</ta>
            <ta e="T285" id="Seg_5888" s="T284">you.GEN</ta>
            <ta e="T286" id="Seg_5889" s="T285">very</ta>
            <ta e="T289" id="Seg_5890" s="T288">tongue-NOM/GEN/ACC.2SG</ta>
            <ta e="T290" id="Seg_5891" s="T289">long.[NOM.SG]</ta>
            <ta e="T291" id="Seg_5892" s="T290">scold-DES-PRS-2SG</ta>
            <ta e="T292" id="Seg_5893" s="T291">you.NOM</ta>
            <ta e="T293" id="Seg_5894" s="T292">you.GEN</ta>
            <ta e="T294" id="Seg_5895" s="T293">tooth-NOM/GEN/ACC.2SG</ta>
            <ta e="T295" id="Seg_5896" s="T294">very</ta>
            <ta e="T296" id="Seg_5897" s="T295">long.[NOM.SG]</ta>
            <ta e="T297" id="Seg_5898" s="T296">nose-NOM/GEN/ACC.2SG</ta>
            <ta e="T300" id="Seg_5899" s="T299">long.[NOM.SG]</ta>
            <ta e="T301" id="Seg_5900" s="T300">snot-NOM/GEN/ACC.2SG</ta>
            <ta e="T302" id="Seg_5901" s="T301">all</ta>
            <ta e="T303" id="Seg_5902" s="T302">flow-DUR.[3SG]</ta>
            <ta e="T304" id="Seg_5903" s="T303">lip-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T307" id="Seg_5904" s="T306">lip-PL-NOM/GEN/ACC.2SG</ta>
            <ta e="T308" id="Seg_5905" s="T307">thick.[NOM.SG]</ta>
            <ta e="T311" id="Seg_5906" s="T310">very</ta>
            <ta e="T312" id="Seg_5907" s="T311">hair-NOM/GEN/ACC.2SG</ta>
            <ta e="T313" id="Seg_5908" s="T312">long.[NOM.SG]</ta>
            <ta e="T314" id="Seg_5909" s="T313">very</ta>
            <ta e="T315" id="Seg_5910" s="T314">ear-PL-NOM/GEN/ACC.2SG</ta>
            <ta e="T316" id="Seg_5911" s="T315">long.[NOM.SG]</ta>
            <ta e="T317" id="Seg_5912" s="T316">eye-NOM/GEN/ACC.2SG</ta>
            <ta e="T318" id="Seg_5913" s="T317">big.[NOM.SG]</ta>
            <ta e="T319" id="Seg_5914" s="T318">head-NOM/GEN/ACC.2SG</ta>
            <ta e="T320" id="Seg_5915" s="T319">very</ta>
            <ta e="T321" id="Seg_5916" s="T320">big.[NOM.SG]</ta>
            <ta e="T324" id="Seg_5917" s="T323">hand-PL-NOM/GEN/ACC.2SG</ta>
            <ta e="T325" id="Seg_5918" s="T324">long.[NOM.SG]</ta>
            <ta e="T328" id="Seg_5919" s="T327">foot-PL-NOM/GEN/ACC.2SG</ta>
            <ta e="T329" id="Seg_5920" s="T328">long.[NOM.SG]</ta>
            <ta e="T330" id="Seg_5921" s="T329">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T331" id="Seg_5922" s="T330">all</ta>
            <ta e="T332" id="Seg_5923" s="T331">thin.[NOM.SG]</ta>
            <ta e="T333" id="Seg_5924" s="T332">finger.[NOM.SG]</ta>
            <ta e="T334" id="Seg_5925" s="T333">fly-DUR.[3SG]</ta>
            <ta e="T335" id="Seg_5926" s="T334">tree-LAT</ta>
            <ta e="T336" id="Seg_5927" s="T335">sit.down-PST.[3SG]</ta>
            <ta e="T337" id="Seg_5928" s="T336">many</ta>
            <ta e="T342" id="Seg_5929" s="T341">bird-PL</ta>
            <ta e="T343" id="Seg_5930" s="T342">fly-DUR-3PL</ta>
            <ta e="T344" id="Seg_5931" s="T343">swallow.[NOM.SG]</ta>
            <ta e="T345" id="Seg_5932" s="T344">all</ta>
            <ta e="T348" id="Seg_5933" s="T347">listen-DUR.[3SG]</ta>
            <ta e="T349" id="Seg_5934" s="T348">owl.[NOM.SG]</ta>
            <ta e="T350" id="Seg_5935" s="T349">listen-CVB</ta>
            <ta e="T353" id="Seg_5936" s="T352">come-PST.[3SG]</ta>
            <ta e="T354" id="Seg_5937" s="T353">hen</ta>
            <ta e="T355" id="Seg_5938" s="T354">all</ta>
            <ta e="T356" id="Seg_5939" s="T355">scratch-PRS.[3SG]</ta>
            <ta e="T357" id="Seg_5940" s="T356">eat-INF.LAT</ta>
            <ta e="T358" id="Seg_5941" s="T357">look-FRQ-PRS.[3SG]</ta>
            <ta e="T359" id="Seg_5942" s="T358">forget-PST-1SG</ta>
            <ta e="T360" id="Seg_5943" s="T359">all</ta>
            <ta e="T363" id="Seg_5944" s="T362">water-ACC</ta>
            <ta e="T364" id="Seg_5945" s="T363">place-INF.LAT</ta>
            <ta e="T365" id="Seg_5946" s="T364">stove-LAT</ta>
            <ta e="T366" id="Seg_5947" s="T365">warm</ta>
            <ta e="T367" id="Seg_5948" s="T366">water.[NOM.SG]</ta>
            <ta e="T368" id="Seg_5949" s="T367">NEG.EX.[3SG]</ta>
            <ta e="T371" id="Seg_5950" s="T370">I.LAT</ta>
            <ta e="T372" id="Seg_5951" s="T371">one.needs</ta>
            <ta e="T373" id="Seg_5952" s="T372">cream.[NOM.SG]</ta>
            <ta e="T376" id="Seg_5953" s="T375">pour-FUT-1SG</ta>
            <ta e="T377" id="Seg_5954" s="T376">%%-1SG</ta>
            <ta e="T378" id="Seg_5955" s="T377">then</ta>
            <ta e="T379" id="Seg_5956" s="T378">wash-INF.LAT</ta>
            <ta e="T380" id="Seg_5957" s="T379">one.should</ta>
            <ta e="T381" id="Seg_5958" s="T380">cup-NOM/GEN/ACC.3PL</ta>
            <ta e="T382" id="Seg_5959" s="T381">snow.[NOM.SG]</ta>
            <ta e="T383" id="Seg_5960" s="T382">tree.[NOM.SG]</ta>
            <ta e="T384" id="Seg_5961" s="T383">%%</ta>
            <ta e="T385" id="Seg_5962" s="T384">tree.[NOM.SG]</ta>
            <ta e="T386" id="Seg_5963" s="T385">red.[NOM.SG]</ta>
            <ta e="T387" id="Seg_5964" s="T386">tree.[NOM.SG]</ta>
            <ta e="T388" id="Seg_5965" s="T387">pine.nut.[NOM.SG]</ta>
            <ta e="T389" id="Seg_5966" s="T388">tree.[NOM.SG]</ta>
            <ta e="T390" id="Seg_5967" s="T389">%%</ta>
            <ta e="T391" id="Seg_5968" s="T390">tree-LOC</ta>
            <ta e="T392" id="Seg_5969" s="T391">all</ta>
            <ta e="T393" id="Seg_5970" s="T392">cone-NOM/GEN/ACC.3PL</ta>
            <ta e="T394" id="Seg_5971" s="T393">grow-DUR-3PL</ta>
            <ta e="T395" id="Seg_5972" s="T394">pine.nut.[NOM.SG]</ta>
            <ta e="T398" id="Seg_5973" s="T397">tree-LOC</ta>
            <ta e="T399" id="Seg_5974" s="T398">all</ta>
            <ta e="T400" id="Seg_5975" s="T399">cone-NOM/GEN/ACC.3PL</ta>
            <ta e="T401" id="Seg_5976" s="T400">sit-DUR-3PL</ta>
            <ta e="T402" id="Seg_5977" s="T401">snow.[NOM.SG]</ta>
            <ta e="T403" id="Seg_5978" s="T402">tree-LOC</ta>
            <ta e="T404" id="Seg_5979" s="T403">all</ta>
            <ta e="T405" id="Seg_5980" s="T404">cup.[NOM.SG]</ta>
            <ta e="T406" id="Seg_5981" s="T405">make-DUR-3PL</ta>
            <ta e="T407" id="Seg_5982" s="T406">yellow.[NOM.SG]</ta>
            <ta e="T408" id="Seg_5983" s="T407">water.[NOM.SG]</ta>
            <ta e="T409" id="Seg_5984" s="T408">drink-DUR-3PL</ta>
            <ta e="T410" id="Seg_5985" s="T409">tree-ADJZ</ta>
            <ta e="T411" id="Seg_5986" s="T410">tree-ADJZ</ta>
            <ta e="T412" id="Seg_5987" s="T411">spoon.[NOM.SG]</ta>
            <ta e="T413" id="Seg_5988" s="T412">iron-ADJZ.[NOM.SG]</ta>
            <ta e="T414" id="Seg_5989" s="T413">spoon.[NOM.SG]</ta>
            <ta e="T415" id="Seg_5990" s="T414">cup.[NOM.SG]</ta>
            <ta e="T416" id="Seg_5991" s="T415">iron-ADJZ.[NOM.SG]</ta>
            <ta e="T417" id="Seg_5992" s="T416">very</ta>
            <ta e="T418" id="Seg_5993" s="T417">tree-PL</ta>
            <ta e="T419" id="Seg_5994" s="T418">many</ta>
            <ta e="T420" id="Seg_5995" s="T419">this-PL</ta>
            <ta e="T421" id="Seg_5996" s="T420">one.should</ta>
            <ta e="T422" id="Seg_5997" s="T421">cut-INF.LAT</ta>
            <ta e="T423" id="Seg_5998" s="T422">then</ta>
            <ta e="T424" id="Seg_5999" s="T423">peel.bark-INF.LAT</ta>
            <ta e="T425" id="Seg_6000" s="T424">then</ta>
            <ta e="T426" id="Seg_6001" s="T425">house.[NOM.SG]</ta>
            <ta e="T427" id="Seg_6002" s="T426">close-INF.LAT</ta>
            <ta e="T428" id="Seg_6003" s="T427">then</ta>
            <ta e="T429" id="Seg_6004" s="T428">this.[NOM.SG]</ta>
            <ta e="T430" id="Seg_6005" s="T429">tree-3PL-INS</ta>
            <ta e="T431" id="Seg_6006" s="T430">house.[NOM.SG]</ta>
            <ta e="T432" id="Seg_6007" s="T431">cut-INF.LAT</ta>
            <ta e="T433" id="Seg_6008" s="T432">one.should</ta>
            <ta e="T434" id="Seg_6009" s="T433">live-INF.LAT</ta>
            <ta e="T435" id="Seg_6010" s="T434">house-LOC</ta>
            <ta e="T436" id="Seg_6011" s="T435">stove.[NOM.SG]</ta>
            <ta e="T437" id="Seg_6012" s="T436">put-INF.LAT</ta>
            <ta e="T438" id="Seg_6013" s="T437">stove-LOC</ta>
            <ta e="T439" id="Seg_6014" s="T438">bake-INF.LAT</ta>
            <ta e="T440" id="Seg_6015" s="T439">bread.[NOM.SG]</ta>
            <ta e="T441" id="Seg_6016" s="T440">drink-EP-IMP.2SG</ta>
            <ta e="T442" id="Seg_6017" s="T441">water.[NOM.SG]</ta>
            <ta e="T443" id="Seg_6018" s="T442">I.NOM</ta>
            <ta e="T444" id="Seg_6019" s="T443">there</ta>
            <ta e="T447" id="Seg_6020" s="T446">put-PST-1SG</ta>
            <ta e="T448" id="Seg_6021" s="T447">sugar.[NOM.SG]</ta>
            <ta e="T451" id="Seg_6022" s="T450">water.[NOM.SG]</ta>
            <ta e="T452" id="Seg_6023" s="T451">pour-PST-1SG</ta>
            <ta e="T455" id="Seg_6024" s="T454">sweet.[NOM.SG]</ta>
            <ta e="T456" id="Seg_6025" s="T455">PTCL</ta>
            <ta e="T457" id="Seg_6026" s="T456">bread.[NOM.SG]</ta>
            <ta e="T458" id="Seg_6027" s="T457">put-IPFVZ-PST-1SG</ta>
            <ta e="T459" id="Seg_6028" s="T458">drink-EP-IMP.2SG</ta>
            <ta e="T460" id="Seg_6029" s="T459">very</ta>
            <ta e="T461" id="Seg_6030" s="T460">good.[NOM.SG]</ta>
            <ta e="T462" id="Seg_6031" s="T461">you.PL.NOM</ta>
            <ta e="T463" id="Seg_6032" s="T462">yesterday</ta>
            <ta e="T464" id="Seg_6033" s="T463">vodka.[NOM.SG]</ta>
            <ta e="T465" id="Seg_6034" s="T464">drink-PST-2PL</ta>
            <ta e="T468" id="Seg_6035" s="T467">drunk.[NOM.SG]</ta>
            <ta e="T469" id="Seg_6036" s="T468">be-PST-2PL</ta>
            <ta e="T470" id="Seg_6037" s="T469">all</ta>
            <ta e="T474" id="Seg_6038" s="T472">today</ta>
            <ta e="T475" id="Seg_6039" s="T474">all</ta>
            <ta e="T478" id="Seg_6040" s="T477">drink-EP-IMP.2SG</ta>
            <ta e="T479" id="Seg_6041" s="T478">red.[NOM.SG]</ta>
            <ta e="T480" id="Seg_6042" s="T479">water.[NOM.SG]</ta>
            <ta e="T481" id="Seg_6043" s="T480">I.NOM</ta>
            <ta e="T482" id="Seg_6044" s="T481">there</ta>
            <ta e="T485" id="Seg_6045" s="T484">sugar.[NOM.SG]</ta>
            <ta e="T486" id="Seg_6046" s="T485">put-PST-1SG</ta>
            <ta e="T488" id="Seg_6047" s="T487">water.[NOM.SG]</ta>
            <ta e="T489" id="Seg_6048" s="T488">pour-PST-1SG</ta>
            <ta e="T492" id="Seg_6049" s="T491">sour.[NOM.SG]</ta>
            <ta e="T493" id="Seg_6050" s="T492">become-RES-PST.[3SG]</ta>
            <ta e="T494" id="Seg_6051" s="T493">good.[NOM.SG]</ta>
            <ta e="T495" id="Seg_6052" s="T494">become-FUT-3SG</ta>
            <ta e="T496" id="Seg_6053" s="T495">you.DAT</ta>
            <ta e="T497" id="Seg_6054" s="T496">vodka.[NOM.SG]</ta>
            <ta e="T498" id="Seg_6055" s="T497">NEG.AUX-IMP.2SG</ta>
            <ta e="T499" id="Seg_6056" s="T498">drink-EP-CNG</ta>
            <ta e="T500" id="Seg_6057" s="T499">otherwise</ta>
            <ta e="T501" id="Seg_6058" s="T500">head-NOM/GEN/ACC.2SG</ta>
            <ta e="T502" id="Seg_6059" s="T501">hurt-PRS.[3SG]</ta>
            <ta e="T503" id="Seg_6060" s="T502">heart-NOM/GEN/ACC.2SG</ta>
            <ta e="T504" id="Seg_6061" s="T503">hurt-PRS.[3SG]</ta>
            <ta e="T505" id="Seg_6062" s="T504">drink-FUT-2SG</ta>
            <ta e="T506" id="Seg_6063" s="T505">so</ta>
            <ta e="T507" id="Seg_6064" s="T506">die-RES-FUT-2SG</ta>
            <ta e="T508" id="Seg_6065" s="T507">and</ta>
            <ta e="T509" id="Seg_6066" s="T508">NEG</ta>
            <ta e="T510" id="Seg_6067" s="T509">drink-FUT-2SG</ta>
            <ta e="T511" id="Seg_6068" s="T510">so</ta>
            <ta e="T512" id="Seg_6069" s="T511">long.time</ta>
            <ta e="T513" id="Seg_6070" s="T512">live-FUT-2SG</ta>
            <ta e="T514" id="Seg_6071" s="T513">machine.[NOM.SG]</ta>
            <ta e="T519" id="Seg_6072" s="T518">plough-DUR.[3SG]</ta>
            <ta e="T520" id="Seg_6073" s="T519">there</ta>
            <ta e="T521" id="Seg_6074" s="T520">man.[NOM.SG]</ta>
            <ta e="T522" id="Seg_6075" s="T521">sit-DUR.[3SG]</ta>
            <ta e="T523" id="Seg_6076" s="T522">machine-ABL</ta>
            <ta e="T524" id="Seg_6077" s="T523">all</ta>
            <ta e="T525" id="Seg_6078" s="T524">smoke.[NOM.SG]</ta>
            <ta e="T526" id="Seg_6079" s="T525">come-PRS.[3SG]</ta>
            <ta e="T527" id="Seg_6080" s="T526">this.[NOM.SG]</ta>
            <ta e="T528" id="Seg_6081" s="T527">all</ta>
            <ta e="T529" id="Seg_6082" s="T528">there</ta>
            <ta e="T531" id="Seg_6083" s="T530">here</ta>
            <ta e="T532" id="Seg_6084" s="T531">go-DUR.[3SG]</ta>
            <ta e="T533" id="Seg_6085" s="T532">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T534" id="Seg_6086" s="T533">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T535" id="Seg_6087" s="T534">girl.[NOM.SG]</ta>
            <ta e="T536" id="Seg_6088" s="T535">find-PST-3PL</ta>
            <ta e="T537" id="Seg_6089" s="T536">I.NOM</ta>
            <ta e="T538" id="Seg_6090" s="T537">I.LAT</ta>
            <ta e="T539" id="Seg_6091" s="T538">say-PRS-3PL</ta>
            <ta e="T540" id="Seg_6092" s="T539">go-EP-IMP.2SG</ta>
            <ta e="T541" id="Seg_6093" s="T540">this-INS</ta>
            <ta e="T544" id="Seg_6094" s="T543">know-IMP.2SG.O</ta>
            <ta e="T545" id="Seg_6095" s="T544">what.kind</ta>
            <ta e="T546" id="Seg_6096" s="T545">this.[NOM.SG]</ta>
            <ta e="T547" id="Seg_6097" s="T546">then</ta>
            <ta e="T548" id="Seg_6098" s="T547">man-LAT</ta>
            <ta e="T549" id="Seg_6099" s="T548">take-PRS-2SG</ta>
            <ta e="T550" id="Seg_6100" s="T549">then</ta>
            <ta e="T551" id="Seg_6101" s="T550">child-PL-LAT</ta>
            <ta e="T552" id="Seg_6102" s="T551">become-FUT-3PL</ta>
            <ta e="T554" id="Seg_6103" s="T553">child-PL-NOM/GEN/ACC.2SG</ta>
            <ta e="T555" id="Seg_6104" s="T554">grow-FUT-3PL</ta>
            <ta e="T556" id="Seg_6105" s="T555">you.DAT</ta>
            <ta e="T557" id="Seg_6106" s="T556">help-FUT-3PL</ta>
            <ta e="T558" id="Seg_6107" s="T557">woman-ACC</ta>
            <ta e="T563" id="Seg_6108" s="T562">NEG.AUX-IMP.2SG</ta>
            <ta e="T564" id="Seg_6109" s="T563">beat-EP-CNG</ta>
            <ta e="T565" id="Seg_6110" s="T564">listen-IMP.2SG.O</ta>
            <ta e="T570" id="Seg_6111" s="T569">father-ACC</ta>
            <ta e="T571" id="Seg_6112" s="T570">and</ta>
            <ta e="T572" id="Seg_6113" s="T571">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T573" id="Seg_6114" s="T572">listen-IMP.2SG.O</ta>
            <ta e="T578" id="Seg_6115" s="T577">today</ta>
            <ta e="T579" id="Seg_6116" s="T578">all</ta>
            <ta e="T580" id="Seg_6117" s="T579">go-CVB</ta>
            <ta e="T581" id="Seg_6118" s="T580">disappear-PST-3PL</ta>
            <ta e="T582" id="Seg_6119" s="T581">father-NOM/GEN.3SG</ta>
            <ta e="T583" id="Seg_6120" s="T582">mother-NOM/GEN.3SG</ta>
            <ta e="T584" id="Seg_6121" s="T583">go-CVB</ta>
            <ta e="T585" id="Seg_6122" s="T584">disappear-PST-3PL</ta>
            <ta e="T586" id="Seg_6123" s="T585">child-PL</ta>
            <ta e="T587" id="Seg_6124" s="T586">go-CVB</ta>
            <ta e="T588" id="Seg_6125" s="T587">disappear-PST-3PL</ta>
            <ta e="T589" id="Seg_6126" s="T588">I.NOM</ta>
            <ta e="T590" id="Seg_6127" s="T589">alone</ta>
            <ta e="T591" id="Seg_6128" s="T590">tent-LAT-2SG</ta>
            <ta e="T592" id="Seg_6129" s="T591">live-DUR-1SG</ta>
            <ta e="T593" id="Seg_6130" s="T592">one.should</ta>
            <ta e="T594" id="Seg_6131" s="T593">earth.[NOM.SG]</ta>
            <ta e="T595" id="Seg_6132" s="T594">dig-INF.LAT</ta>
            <ta e="T596" id="Seg_6133" s="T595">man.[NOM.SG]</ta>
            <ta e="T597" id="Seg_6134" s="T596">see-RES-PST.[3SG]</ta>
            <ta e="T598" id="Seg_6135" s="T597">one.should</ta>
            <ta e="T599" id="Seg_6136" s="T598">this-ACC</ta>
            <ta e="T600" id="Seg_6137" s="T599">there</ta>
            <ta e="T601" id="Seg_6138" s="T600">put-INF.LAT</ta>
            <ta e="T602" id="Seg_6139" s="T601">then</ta>
            <ta e="T603" id="Seg_6140" s="T602">pour-INF.LAT</ta>
            <ta e="T606" id="Seg_6141" s="T605">then</ta>
            <ta e="T611" id="Seg_6142" s="T610">cry</ta>
            <ta e="T613" id="Seg_6143" s="T611">then</ta>
            <ta e="T614" id="Seg_6144" s="T613">then</ta>
            <ta e="T617" id="Seg_6145" s="T616">earth-INS</ta>
            <ta e="T618" id="Seg_6146" s="T617">one.should</ta>
            <ta e="T619" id="Seg_6147" s="T618">close-INF.LAT</ta>
            <ta e="T620" id="Seg_6148" s="T619">this-ACC</ta>
            <ta e="T621" id="Seg_6149" s="T620">then</ta>
            <ta e="T622" id="Seg_6150" s="T621">tent-LAT-2SG</ta>
            <ta e="T625" id="Seg_6151" s="T624">come-INF.LAT</ta>
            <ta e="T626" id="Seg_6152" s="T625">eat-INF.LAT</ta>
            <ta e="T629" id="Seg_6153" s="T628">sit-IMP.2SG</ta>
            <ta e="T630" id="Seg_6154" s="T629">eat-INF.LAT</ta>
            <ta e="T631" id="Seg_6155" s="T630">man.[NOM.SG]</ta>
            <ta e="T632" id="Seg_6156" s="T631">hurt-PST.[3SG]</ta>
            <ta e="T633" id="Seg_6157" s="T632">shaman-NOM/GEN/ACC.3SG</ta>
            <ta e="T634" id="Seg_6158" s="T633">go-PST.[3SG]</ta>
            <ta e="T635" id="Seg_6159" s="T634">shaman.[NOM.SG]</ta>
            <ta e="T636" id="Seg_6160" s="T635">NEG</ta>
            <ta e="T637" id="Seg_6161" s="T636">can-PST.[3SG]</ta>
            <ta e="T638" id="Seg_6162" s="T637">this-ACC</ta>
            <ta e="T639" id="Seg_6163" s="T638">heal-INF.LAT</ta>
            <ta e="T643" id="Seg_6164" s="T641">die-RES-PST</ta>
            <ta e="T644" id="Seg_6165" s="T643">how.much</ta>
            <ta e="T645" id="Seg_6166" s="T644">NEG</ta>
            <ta e="T646" id="Seg_6167" s="T645">hurt-PST.[3SG]</ta>
            <ta e="T647" id="Seg_6168" s="T646">now</ta>
            <ta e="T648" id="Seg_6169" s="T647">all</ta>
            <ta e="T649" id="Seg_6170" s="T648">lie-DUR.[3SG]</ta>
            <ta e="T651" id="Seg_6171" s="T649">what-INS=INDEF</ta>
            <ta e="T652" id="Seg_6172" s="T651">NEG</ta>
            <ta e="T653" id="Seg_6173" s="T652">lift-PRS</ta>
            <ta e="T654" id="Seg_6174" s="T653">I.ACC</ta>
            <ta e="T655" id="Seg_6175" s="T654">all</ta>
            <ta e="T656" id="Seg_6176" s="T655">beat-PST-3PL</ta>
            <ta e="T657" id="Seg_6177" s="T656">very</ta>
            <ta e="T658" id="Seg_6178" s="T657">strongly</ta>
            <ta e="T659" id="Seg_6179" s="T658">self-NOM/GEN/ACC.1SG</ta>
            <ta e="T660" id="Seg_6180" s="T659">NEG</ta>
            <ta e="T661" id="Seg_6181" s="T660">know-1SG</ta>
            <ta e="T662" id="Seg_6182" s="T661">what-INS</ta>
            <ta e="T663" id="Seg_6183" s="T662">beat-PST-3PL</ta>
            <ta e="T666" id="Seg_6184" s="T665">I.NOM</ta>
            <ta e="T667" id="Seg_6185" s="T666">I.NOM</ta>
            <ta e="T668" id="Seg_6186" s="T667">all</ta>
            <ta e="T669" id="Seg_6187" s="T668">hurt-PST.[3SG]</ta>
            <ta e="T670" id="Seg_6188" s="T669">hurt-PRS-1SG</ta>
            <ta e="T671" id="Seg_6189" s="T670">head-NOM/GEN/ACC.1SG</ta>
            <ta e="T672" id="Seg_6190" s="T671">hurt-PRS.[3SG]</ta>
            <ta e="T673" id="Seg_6191" s="T672">ear-NOM/GEN/ACC.1SG</ta>
            <ta e="T674" id="Seg_6192" s="T673">hurt-PRS.[3SG]</ta>
            <ta e="T675" id="Seg_6193" s="T674">eye-NOM/GEN/ACC.1SG</ta>
            <ta e="T676" id="Seg_6194" s="T675">hurt-PRS.[3SG]</ta>
            <ta e="T677" id="Seg_6195" s="T676">tooth-NOM/GEN/ACC.1SG</ta>
            <ta e="T678" id="Seg_6196" s="T677">hurt-PRS.[3SG]</ta>
            <ta e="T679" id="Seg_6197" s="T678">nose-NOM/GEN/ACC.1SG</ta>
            <ta e="T680" id="Seg_6198" s="T679">hurt-PRS.[3SG]</ta>
            <ta e="T681" id="Seg_6199" s="T680">hand-NOM/GEN/ACC.1SG</ta>
            <ta e="T682" id="Seg_6200" s="T681">hurt-PRS.[3SG]</ta>
            <ta e="T683" id="Seg_6201" s="T682">foot-NOM/GEN/ACC.1SG</ta>
            <ta e="T684" id="Seg_6202" s="T683">hurt-PRS.[3SG]</ta>
            <ta e="T685" id="Seg_6203" s="T684">belly-NOM/GEN/ACC.1SG</ta>
            <ta e="T686" id="Seg_6204" s="T685">hurt-PRS.[3SG]</ta>
            <ta e="T687" id="Seg_6205" s="T686">heart-NOM/GEN/ACC.1SG</ta>
            <ta e="T688" id="Seg_6206" s="T687">hurt-PRS.[3SG]</ta>
            <ta e="T689" id="Seg_6207" s="T688">whole-ACC.3SG</ta>
            <ta e="T690" id="Seg_6208" s="T689">hurt-PRS.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PKZ">
            <ta e="T5" id="Seg_6209" s="T4">женщина-PL</ta>
            <ta e="T6" id="Seg_6210" s="T5">очень</ta>
            <ta e="T7" id="Seg_6211" s="T6">много</ta>
            <ta e="T8" id="Seg_6212" s="T7">прийти-PRS-3PL</ta>
            <ta e="T9" id="Seg_6213" s="T8">я.NOM</ta>
            <ta e="T10" id="Seg_6214" s="T9">сказать-PST-1SG</ta>
            <ta e="T11" id="Seg_6215" s="T10">где</ta>
            <ta e="T12" id="Seg_6216" s="T11">быть-PST-2PL</ta>
            <ta e="T13" id="Seg_6217" s="T12">этот-PL</ta>
            <ta e="T14" id="Seg_6218" s="T13">сказать-PRS-3PL</ta>
            <ta e="T15" id="Seg_6219" s="T14">корова.[NOM.SG]</ta>
            <ta e="T16" id="Seg_6220" s="T15">доить-INF.LAT</ta>
            <ta e="T17" id="Seg_6221" s="T16">идти-PST-1PL</ta>
            <ta e="T18" id="Seg_6222" s="T17">принести-IMP.2PL</ta>
            <ta e="T19" id="Seg_6223" s="T18">я.LAT</ta>
            <ta e="T20" id="Seg_6224" s="T19">молоко.[NOM.SG]</ta>
            <ta e="T21" id="Seg_6225" s="T20">хватит</ta>
            <ta e="T22" id="Seg_6226" s="T21">еще</ta>
            <ta e="T23" id="Seg_6227" s="T22">ребенок.[NOM.SG]</ta>
            <ta e="T24" id="Seg_6228" s="T23">весь</ta>
            <ta e="T25" id="Seg_6229" s="T24">испражняться-PST.[3SG]</ta>
            <ta e="T26" id="Seg_6230" s="T25">очень</ta>
            <ta e="T27" id="Seg_6231" s="T26">черный.[NOM.SG]</ta>
            <ta e="T28" id="Seg_6232" s="T27">весь</ta>
            <ta e="T29" id="Seg_6233" s="T28">высохнуть-MOM-PST.[3SG]</ta>
            <ta e="T30" id="Seg_6234" s="T29">надо</ta>
            <ta e="T31" id="Seg_6235" s="T30">мыть-INF.LAT</ta>
            <ta e="T32" id="Seg_6236" s="T31">этот-ACC</ta>
            <ta e="T35" id="Seg_6237" s="T34">тереть-INF.LAT</ta>
            <ta e="T36" id="Seg_6238" s="T35">тогда</ta>
            <ta e="T37" id="Seg_6239" s="T36">мыть-INF.LAT</ta>
            <ta e="T38" id="Seg_6240" s="T37">надо</ta>
            <ta e="T41" id="Seg_6241" s="T40">снимать.кору-INF.LAT</ta>
            <ta e="T42" id="Seg_6242" s="T41">надо</ta>
            <ta e="T43" id="Seg_6243" s="T42">тогда</ta>
            <ta e="T44" id="Seg_6244" s="T43">съесть-INF.LAT</ta>
            <ta e="T45" id="Seg_6245" s="T44">дать-INF.LAT</ta>
            <ta e="T46" id="Seg_6246" s="T45">тогда</ta>
            <ta e="T47" id="Seg_6247" s="T46">спать-INF.LAT</ta>
            <ta e="T50" id="Seg_6248" s="T49">ложиться</ta>
            <ta e="T51" id="Seg_6249" s="T50">надо</ta>
            <ta e="T52" id="Seg_6250" s="T51">баюкать-IMP.2SG</ta>
            <ta e="T53" id="Seg_6251" s="T52">ребенок-NOM/GEN/ACC.1SG</ta>
            <ta e="T54" id="Seg_6252" s="T53">весь</ta>
            <ta e="T55" id="Seg_6253" s="T54">сидеть-IMP.2SG</ta>
            <ta e="T56" id="Seg_6254" s="T55">дом-LOC</ta>
            <ta e="T57" id="Seg_6255" s="T56">я.NOM</ta>
            <ta e="T58" id="Seg_6256" s="T57">наружу</ta>
            <ta e="T59" id="Seg_6257" s="T58">пойти-FUT-1SG</ta>
            <ta e="T60" id="Seg_6258" s="T59">мочиться-INF.LAT</ta>
            <ta e="T61" id="Seg_6259" s="T60">испражняться-INF.LAT</ta>
            <ta e="T62" id="Seg_6260" s="T61">тогда</ta>
            <ta e="T63" id="Seg_6261" s="T62">прийти-FUT-1SG</ta>
            <ta e="T64" id="Seg_6262" s="T63">сидеть-FUT-1SG</ta>
            <ta e="T65" id="Seg_6263" s="T64">ты.NOM-INS</ta>
            <ta e="T66" id="Seg_6264" s="T65">и</ta>
            <ta e="T67" id="Seg_6265" s="T66">говорить-FUT-1PL</ta>
            <ta e="T68" id="Seg_6266" s="T67">хватит</ta>
            <ta e="T69" id="Seg_6267" s="T68">я.NOM</ta>
            <ta e="T70" id="Seg_6268" s="T69">зад-NOM/GEN/ACC.1SG</ta>
            <ta e="T71" id="Seg_6269" s="T70">красивый.[NOM.SG]</ta>
            <ta e="T72" id="Seg_6270" s="T71">ты.GEN</ta>
            <ta e="T73" id="Seg_6271" s="T72">лицо-NOM/GEN/ACC.3SG</ta>
            <ta e="T76" id="Seg_6272" s="T75">очень</ta>
            <ta e="T77" id="Seg_6273" s="T76">толстый.[NOM.SG]</ta>
            <ta e="T78" id="Seg_6274" s="T77">зад.[NOM.SG]</ta>
            <ta e="T79" id="Seg_6275" s="T78">очень</ta>
            <ta e="T80" id="Seg_6276" s="T79">черный.[NOM.SG]</ta>
            <ta e="T81" id="Seg_6277" s="T80">зад.[NOM.SG]</ta>
            <ta e="T82" id="Seg_6278" s="T81">очень</ta>
            <ta e="T83" id="Seg_6279" s="T82">красный.[NOM.SG]</ta>
            <ta e="T84" id="Seg_6280" s="T83">зад.[NOM.SG]</ta>
            <ta e="T85" id="Seg_6281" s="T84">очень</ta>
            <ta e="T86" id="Seg_6282" s="T85">снег.[NOM.SG]</ta>
            <ta e="T87" id="Seg_6283" s="T86">зад.[NOM.SG]</ta>
            <ta e="T88" id="Seg_6284" s="T87">очень</ta>
            <ta e="T89" id="Seg_6285" s="T88">толстый.[NOM.SG]</ta>
            <ta e="T90" id="Seg_6286" s="T89">зад.[NOM.SG]</ta>
            <ta e="T91" id="Seg_6287" s="T90">очень</ta>
            <ta e="T92" id="Seg_6288" s="T91">худой.[NOM.SG]</ta>
            <ta e="T94" id="Seg_6289" s="T93">зад.[NOM.SG]</ta>
            <ta e="T95" id="Seg_6290" s="T94">хватит</ta>
            <ta e="T96" id="Seg_6291" s="T95">ребенок.[NOM.SG]</ta>
            <ta e="T97" id="Seg_6292" s="T96">прийти-PRS-2SG</ta>
            <ta e="T98" id="Seg_6293" s="T97">рука-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T99" id="Seg_6294" s="T98">весь</ta>
            <ta e="T100" id="Seg_6295" s="T99">грязный.[NOM.SG]</ta>
            <ta e="T101" id="Seg_6296" s="T100">надо</ta>
            <ta e="T102" id="Seg_6297" s="T101">мыть-INF.LAT</ta>
            <ta e="T103" id="Seg_6298" s="T102">прийти-IMP.2SG</ta>
            <ta e="T104" id="Seg_6299" s="T103">здесь</ta>
            <ta e="T105" id="Seg_6300" s="T104">NEG.AUX-IMP.2SG</ta>
            <ta e="T106" id="Seg_6301" s="T105">плакать-EP-CNG</ta>
            <ta e="T107" id="Seg_6302" s="T106">пойти-EP-IMP.2SG</ta>
            <ta e="T108" id="Seg_6303" s="T107">чум-LAT-2SG</ta>
            <ta e="T111" id="Seg_6304" s="T110">ты.DAT</ta>
            <ta e="T112" id="Seg_6305" s="T111">наверное</ta>
            <ta e="T117" id="Seg_6306" s="T116">смотреть-DUR-3PL</ta>
            <ta e="T118" id="Seg_6307" s="T117">PTCL</ta>
            <ta e="T119" id="Seg_6308" s="T118">а.то</ta>
            <ta e="T120" id="Seg_6309" s="T119">утка-PL</ta>
            <ta e="T121" id="Seg_6310" s="T120">%%-3PL</ta>
            <ta e="T122" id="Seg_6311" s="T121">ты.DAT</ta>
            <ta e="T123" id="Seg_6312" s="T122">весь</ta>
            <ta e="T124" id="Seg_6313" s="T123">куда</ta>
            <ta e="T125" id="Seg_6314" s="T124">лезть-PRS-2SG</ta>
            <ta e="T141" id="Seg_6315" s="T140">куда</ta>
            <ta e="T143" id="Seg_6316" s="T142">варить-DUR-PRS-2SG</ta>
            <ta e="T144" id="Seg_6317" s="T143">NEG.AUX-IMP.2SG</ta>
            <ta e="T145" id="Seg_6318" s="T144">лезть-EP-CNG</ta>
            <ta e="T146" id="Seg_6319" s="T145">а.то</ta>
            <ta e="T147" id="Seg_6320" s="T146">упасть-MOM-FUT-2SG</ta>
            <ta e="T148" id="Seg_6321" s="T147">прийти-IMP.2SG</ta>
            <ta e="T149" id="Seg_6322" s="T148">здесь</ta>
            <ta e="T150" id="Seg_6323" s="T149">этот.[NOM.SG]</ta>
            <ta e="T151" id="Seg_6324" s="T150">девушка.[NOM.SG]</ta>
            <ta e="T152" id="Seg_6325" s="T151">сидеть-DUR.[3SG]</ta>
            <ta e="T153" id="Seg_6326" s="T152">худой.[NOM.SG]</ta>
            <ta e="T154" id="Seg_6327" s="T153">весь</ta>
            <ta e="T159" id="Seg_6328" s="T158">что.[NOM.SG]</ta>
            <ta e="T160" id="Seg_6329" s="T159">сказать-IPFVZ-PRS-1SG</ta>
            <ta e="T161" id="Seg_6330" s="T160">этот.[NOM.SG]</ta>
            <ta e="T162" id="Seg_6331" s="T161">NEG</ta>
            <ta e="T163" id="Seg_6332" s="T162">слушать-DUR.[3SG]</ta>
            <ta e="T164" id="Seg_6333" s="T163">лес-LAT</ta>
            <ta e="T167" id="Seg_6334" s="T166">пойти-IPFVZ-PRS-3PL</ta>
            <ta e="T168" id="Seg_6335" s="T167">лошадь-PL-INS</ta>
            <ta e="T169" id="Seg_6336" s="T168">рыба.[NOM.SG]</ta>
            <ta e="T170" id="Seg_6337" s="T169">ловить-FUT-3PL</ta>
            <ta e="T171" id="Seg_6338" s="T170">рыба.[NOM.SG]</ta>
            <ta e="T172" id="Seg_6339" s="T171">принести-FUT-3PL</ta>
            <ta e="T173" id="Seg_6340" s="T172">я.LAT</ta>
            <ta e="T174" id="Seg_6341" s="T173">может.быть</ta>
            <ta e="T175" id="Seg_6342" s="T174">дать-FUT-3PL</ta>
            <ta e="T176" id="Seg_6343" s="T175">хватит</ta>
            <ta e="T178" id="Seg_6344" s="T177">река-LOC</ta>
            <ta e="T179" id="Seg_6345" s="T178">рука-INS</ta>
            <ta e="T180" id="Seg_6346" s="T179">рыба.[NOM.SG]</ta>
            <ta e="T181" id="Seg_6347" s="T180">ловить-PST-3PL</ta>
            <ta e="T182" id="Seg_6348" s="T181">место-LAT</ta>
            <ta e="T183" id="Seg_6349" s="T182">место-LAT</ta>
            <ta e="T186" id="Seg_6350" s="T185">класть-PST-3PL</ta>
            <ta e="T187" id="Seg_6351" s="T186">тогда</ta>
            <ta e="T190" id="Seg_6352" s="T189">солить-PST-3PL</ta>
            <ta e="T191" id="Seg_6353" s="T190">тогда</ta>
            <ta e="T192" id="Seg_6354" s="T191">продавать-PST-3PL</ta>
            <ta e="T193" id="Seg_6355" s="T192">хватать</ta>
            <ta e="T194" id="Seg_6356" s="T193">холодный.[NOM.SG]</ta>
            <ta e="T195" id="Seg_6357" s="T194">стать-RES-PST.[3SG]</ta>
            <ta e="T196" id="Seg_6358" s="T195">теплый.[NOM.SG]</ta>
            <ta e="T197" id="Seg_6359" s="T196">пойти-CVB</ta>
            <ta e="T198" id="Seg_6360" s="T197">исчезнуть-PST.[3SG]</ta>
            <ta e="T199" id="Seg_6361" s="T198">солнце.[NOM.SG]</ta>
            <ta e="T200" id="Seg_6362" s="T199">мало</ta>
            <ta e="T201" id="Seg_6363" s="T200">сверкать-DUR.[3SG]</ta>
            <ta e="T202" id="Seg_6364" s="T201">снег.[NOM.SG]</ta>
            <ta e="T203" id="Seg_6365" s="T202">прийти-PRS.[3SG]</ta>
            <ta e="T204" id="Seg_6366" s="T203">ветер.[NOM.SG]</ta>
            <ta e="T205" id="Seg_6367" s="T204">весь</ta>
            <ta e="T206" id="Seg_6368" s="T205">очень</ta>
            <ta e="T207" id="Seg_6369" s="T206">холодный.[NOM.SG]</ta>
            <ta e="T208" id="Seg_6370" s="T207">я.NOM</ta>
            <ta e="T209" id="Seg_6371" s="T208">дерево.[NOM.SG]</ta>
            <ta e="T210" id="Seg_6372" s="T209">много</ta>
            <ta e="T211" id="Seg_6373" s="T210">этот.[NOM.SG]</ta>
            <ta e="T212" id="Seg_6374" s="T211">зима-LAT</ta>
            <ta e="T213" id="Seg_6375" s="T212">хватать-FUT-3SG</ta>
            <ta e="T214" id="Seg_6376" s="T213">что</ta>
            <ta e="T215" id="Seg_6377" s="T214">голова-NOM/GEN/ACC.2SG</ta>
            <ta e="T216" id="Seg_6378" s="T215">копать-PRS-2SG</ta>
            <ta e="T217" id="Seg_6379" s="T216">наверное</ta>
            <ta e="T218" id="Seg_6380" s="T217">вошь-NOM/GEN/ACC.3PL</ta>
            <ta e="T219" id="Seg_6381" s="T218">быть-PRS.[3SG]</ta>
            <ta e="T220" id="Seg_6382" s="T219">очень</ta>
            <ta e="T221" id="Seg_6383" s="T220">много</ta>
            <ta e="T222" id="Seg_6384" s="T221">вошь.[NOM.SG]</ta>
            <ta e="T223" id="Seg_6385" s="T222">убить-INF.LAT</ta>
            <ta e="T224" id="Seg_6386" s="T223">надо</ta>
            <ta e="T225" id="Seg_6387" s="T224">этот-PL</ta>
            <ta e="T226" id="Seg_6388" s="T225">хватит</ta>
            <ta e="T227" id="Seg_6389" s="T226">я.NOM</ta>
            <ta e="T228" id="Seg_6390" s="T227">ложиться-PST-1SG</ta>
            <ta e="T229" id="Seg_6391" s="T228">спать-INF.LAT</ta>
            <ta e="T230" id="Seg_6392" s="T229">курица</ta>
            <ta e="T231" id="Seg_6393" s="T230">NEG</ta>
            <ta e="T232" id="Seg_6394" s="T231">дать-PST.[3SG]</ta>
            <ta e="T233" id="Seg_6395" s="T232">спать-INF.LAT</ta>
            <ta e="T234" id="Seg_6396" s="T233">я.NOM</ta>
            <ta e="T235" id="Seg_6397" s="T234">встать-PST-1SG</ta>
            <ta e="T236" id="Seg_6398" s="T235">и</ta>
            <ta e="T239" id="Seg_6399" s="T238">еще</ta>
            <ta e="T240" id="Seg_6400" s="T239">NEG</ta>
            <ta e="T241" id="Seg_6401" s="T240">ложиться-PST-1SG</ta>
            <ta e="T242" id="Seg_6402" s="T241">красивый.[NOM.SG]</ta>
            <ta e="T243" id="Seg_6403" s="T242">женщина.[NOM.SG]</ta>
            <ta e="T244" id="Seg_6404" s="T243">сидеть-DUR.[3SG]</ta>
            <ta e="T245" id="Seg_6405" s="T244">красивый.[NOM.SG]</ta>
            <ta e="T246" id="Seg_6406" s="T245">мальчик.[NOM.SG]</ta>
            <ta e="T247" id="Seg_6407" s="T246">сидеть-DUR.[3SG]</ta>
            <ta e="T248" id="Seg_6408" s="T247">я.NOM</ta>
            <ta e="T249" id="Seg_6409" s="T248">очень</ta>
            <ta e="T250" id="Seg_6410" s="T249">старый.[NOM.SG]</ta>
            <ta e="T251" id="Seg_6411" s="T250">стать-RES-PST-1SG</ta>
            <ta e="T252" id="Seg_6412" s="T251">два-COLL</ta>
            <ta e="T253" id="Seg_6413" s="T252">остаться-DUR.PST-1PL</ta>
            <ta e="T254" id="Seg_6414" s="T253">я.NOM</ta>
            <ta e="T255" id="Seg_6415" s="T254">и</ta>
            <ta e="T256" id="Seg_6416" s="T255">брат-NOM/GEN/ACC.1SG</ta>
            <ta e="T260" id="Seg_6417" s="T259">а</ta>
            <ta e="T261" id="Seg_6418" s="T260">этот-PL</ta>
            <ta e="T262" id="Seg_6419" s="T261">PTCL</ta>
            <ta e="T263" id="Seg_6420" s="T262">умереть-RES-PST-3PL</ta>
            <ta e="T264" id="Seg_6421" s="T263">весь</ta>
            <ta e="T270" id="Seg_6422" s="T269">много</ta>
            <ta e="T271" id="Seg_6423" s="T270">год.[NOM.SG]</ta>
            <ta e="T272" id="Seg_6424" s="T271">пойти-PST.[3SG]</ta>
            <ta e="T273" id="Seg_6425" s="T272">весь</ta>
            <ta e="T274" id="Seg_6426" s="T273">дым.[NOM.SG]</ta>
            <ta e="T275" id="Seg_6427" s="T274">место-LAT</ta>
            <ta e="T278" id="Seg_6428" s="T277">сидеть-PRS-2SG</ta>
            <ta e="T279" id="Seg_6429" s="T278">дождь.[NOM.SG]</ta>
            <ta e="T280" id="Seg_6430" s="T279">стать-FUT-3SG</ta>
            <ta e="T281" id="Seg_6431" s="T280">рука-NOM/GEN/ACC.1SG</ta>
            <ta e="T282" id="Seg_6432" s="T281">чесаться-PRS.[3SG]</ta>
            <ta e="T283" id="Seg_6433" s="T282">надо</ta>
            <ta e="T284" id="Seg_6434" s="T283">чесать-INF.LAT</ta>
            <ta e="T285" id="Seg_6435" s="T284">ты.GEN</ta>
            <ta e="T286" id="Seg_6436" s="T285">очень</ta>
            <ta e="T289" id="Seg_6437" s="T288">язык-NOM/GEN/ACC.2SG</ta>
            <ta e="T290" id="Seg_6438" s="T289">длинный.[NOM.SG]</ta>
            <ta e="T291" id="Seg_6439" s="T290">ругать-DES-PRS-2SG</ta>
            <ta e="T292" id="Seg_6440" s="T291">ты.NOM</ta>
            <ta e="T293" id="Seg_6441" s="T292">ты.GEN</ta>
            <ta e="T294" id="Seg_6442" s="T293">зуб-NOM/GEN/ACC.2SG</ta>
            <ta e="T295" id="Seg_6443" s="T294">очень</ta>
            <ta e="T296" id="Seg_6444" s="T295">длинный.[NOM.SG]</ta>
            <ta e="T297" id="Seg_6445" s="T296">нос-NOM/GEN/ACC.2SG</ta>
            <ta e="T300" id="Seg_6446" s="T299">длинный.[NOM.SG]</ta>
            <ta e="T301" id="Seg_6447" s="T300">сопли-NOM/GEN/ACC.2SG</ta>
            <ta e="T302" id="Seg_6448" s="T301">весь</ta>
            <ta e="T303" id="Seg_6449" s="T302">течь-DUR.[3SG]</ta>
            <ta e="T304" id="Seg_6450" s="T303">губа-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T307" id="Seg_6451" s="T306">губа-PL-NOM/GEN/ACC.2SG</ta>
            <ta e="T308" id="Seg_6452" s="T307">толстый.[NOM.SG]</ta>
            <ta e="T311" id="Seg_6453" s="T310">очень</ta>
            <ta e="T312" id="Seg_6454" s="T311">волосы-NOM/GEN/ACC.2SG</ta>
            <ta e="T313" id="Seg_6455" s="T312">длинный.[NOM.SG]</ta>
            <ta e="T314" id="Seg_6456" s="T313">очень</ta>
            <ta e="T315" id="Seg_6457" s="T314">ухо-PL-NOM/GEN/ACC.2SG</ta>
            <ta e="T316" id="Seg_6458" s="T315">длинный.[NOM.SG]</ta>
            <ta e="T317" id="Seg_6459" s="T316">глаз-NOM/GEN/ACC.2SG</ta>
            <ta e="T318" id="Seg_6460" s="T317">большой.[NOM.SG]</ta>
            <ta e="T319" id="Seg_6461" s="T318">голова-NOM/GEN/ACC.2SG</ta>
            <ta e="T320" id="Seg_6462" s="T319">очень</ta>
            <ta e="T321" id="Seg_6463" s="T320">большой.[NOM.SG]</ta>
            <ta e="T324" id="Seg_6464" s="T323">рука-PL-NOM/GEN/ACC.2SG</ta>
            <ta e="T325" id="Seg_6465" s="T324">длинный.[NOM.SG]</ta>
            <ta e="T328" id="Seg_6466" s="T327">нога-PL-NOM/GEN/ACC.2SG</ta>
            <ta e="T329" id="Seg_6467" s="T328">длинный.[NOM.SG]</ta>
            <ta e="T330" id="Seg_6468" s="T329">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T331" id="Seg_6469" s="T330">весь</ta>
            <ta e="T332" id="Seg_6470" s="T331">худой.[NOM.SG]</ta>
            <ta e="T333" id="Seg_6471" s="T332">палец.[NOM.SG]</ta>
            <ta e="T334" id="Seg_6472" s="T333">лететь-DUR.[3SG]</ta>
            <ta e="T335" id="Seg_6473" s="T334">дерево-LAT</ta>
            <ta e="T336" id="Seg_6474" s="T335">сесть-PST.[3SG]</ta>
            <ta e="T337" id="Seg_6475" s="T336">много</ta>
            <ta e="T342" id="Seg_6476" s="T341">птица-PL</ta>
            <ta e="T343" id="Seg_6477" s="T342">лететь-DUR-3PL</ta>
            <ta e="T344" id="Seg_6478" s="T343">ласточка.[NOM.SG]</ta>
            <ta e="T345" id="Seg_6479" s="T344">весь</ta>
            <ta e="T348" id="Seg_6480" s="T347">слушать-DUR.[3SG]</ta>
            <ta e="T349" id="Seg_6481" s="T348">сова.[NOM.SG]</ta>
            <ta e="T350" id="Seg_6482" s="T349">слушать-CVB</ta>
            <ta e="T353" id="Seg_6483" s="T352">прийти-PST.[3SG]</ta>
            <ta e="T354" id="Seg_6484" s="T353">курица</ta>
            <ta e="T355" id="Seg_6485" s="T354">весь</ta>
            <ta e="T356" id="Seg_6486" s="T355">чесать-PRS.[3SG]</ta>
            <ta e="T357" id="Seg_6487" s="T356">съесть-INF.LAT</ta>
            <ta e="T358" id="Seg_6488" s="T357">смотреть-FRQ-PRS.[3SG]</ta>
            <ta e="T359" id="Seg_6489" s="T358">забыть-PST-1SG</ta>
            <ta e="T360" id="Seg_6490" s="T359">весь</ta>
            <ta e="T363" id="Seg_6491" s="T362">вода-ACC</ta>
            <ta e="T364" id="Seg_6492" s="T363">поставить-INF.LAT</ta>
            <ta e="T365" id="Seg_6493" s="T364">печь-LAT</ta>
            <ta e="T366" id="Seg_6494" s="T365">теплый</ta>
            <ta e="T367" id="Seg_6495" s="T366">вода.[NOM.SG]</ta>
            <ta e="T368" id="Seg_6496" s="T367">NEG.EX.[3SG]</ta>
            <ta e="T371" id="Seg_6497" s="T370">я.LAT</ta>
            <ta e="T372" id="Seg_6498" s="T371">нужно</ta>
            <ta e="T373" id="Seg_6499" s="T372">сметана.[NOM.SG]</ta>
            <ta e="T376" id="Seg_6500" s="T375">лить-FUT-1SG</ta>
            <ta e="T377" id="Seg_6501" s="T376">%%-1SG</ta>
            <ta e="T378" id="Seg_6502" s="T377">тогда</ta>
            <ta e="T379" id="Seg_6503" s="T378">мыть-INF.LAT</ta>
            <ta e="T380" id="Seg_6504" s="T379">надо</ta>
            <ta e="T381" id="Seg_6505" s="T380">чашка-NOM/GEN/ACC.3PL</ta>
            <ta e="T382" id="Seg_6506" s="T381">снег.[NOM.SG]</ta>
            <ta e="T383" id="Seg_6507" s="T382">дерево.[NOM.SG]</ta>
            <ta e="T384" id="Seg_6508" s="T383">%%</ta>
            <ta e="T385" id="Seg_6509" s="T384">дерево.[NOM.SG]</ta>
            <ta e="T386" id="Seg_6510" s="T385">красный.[NOM.SG]</ta>
            <ta e="T387" id="Seg_6511" s="T386">дерево.[NOM.SG]</ta>
            <ta e="T388" id="Seg_6512" s="T387">кедровый.орех.[NOM.SG]</ta>
            <ta e="T389" id="Seg_6513" s="T388">дерево.[NOM.SG]</ta>
            <ta e="T390" id="Seg_6514" s="T389">%%</ta>
            <ta e="T391" id="Seg_6515" s="T390">дерево-LOC</ta>
            <ta e="T392" id="Seg_6516" s="T391">весь</ta>
            <ta e="T393" id="Seg_6517" s="T392">шишка-NOM/GEN/ACC.3PL</ta>
            <ta e="T394" id="Seg_6518" s="T393">расти-DUR-3PL</ta>
            <ta e="T395" id="Seg_6519" s="T394">кедровый.орех.[NOM.SG]</ta>
            <ta e="T398" id="Seg_6520" s="T397">дерево-LOC</ta>
            <ta e="T399" id="Seg_6521" s="T398">весь</ta>
            <ta e="T400" id="Seg_6522" s="T399">шишка-NOM/GEN/ACC.3PL</ta>
            <ta e="T401" id="Seg_6523" s="T400">сидеть-DUR-3PL</ta>
            <ta e="T402" id="Seg_6524" s="T401">снег.[NOM.SG]</ta>
            <ta e="T403" id="Seg_6525" s="T402">дерево-LOC</ta>
            <ta e="T404" id="Seg_6526" s="T403">весь</ta>
            <ta e="T405" id="Seg_6527" s="T404">чашка.[NOM.SG]</ta>
            <ta e="T406" id="Seg_6528" s="T405">делать-DUR-3PL</ta>
            <ta e="T407" id="Seg_6529" s="T406">желтый.[NOM.SG]</ta>
            <ta e="T408" id="Seg_6530" s="T407">вода.[NOM.SG]</ta>
            <ta e="T409" id="Seg_6531" s="T408">пить-DUR-3PL</ta>
            <ta e="T410" id="Seg_6532" s="T409">дерево-ADJZ</ta>
            <ta e="T411" id="Seg_6533" s="T410">дерево-ADJZ</ta>
            <ta e="T412" id="Seg_6534" s="T411">ложка.[NOM.SG]</ta>
            <ta e="T413" id="Seg_6535" s="T412">железо-ADJZ.[NOM.SG]</ta>
            <ta e="T414" id="Seg_6536" s="T413">ложка.[NOM.SG]</ta>
            <ta e="T415" id="Seg_6537" s="T414">чашка.[NOM.SG]</ta>
            <ta e="T416" id="Seg_6538" s="T415">железо-ADJZ.[NOM.SG]</ta>
            <ta e="T417" id="Seg_6539" s="T416">очень</ta>
            <ta e="T418" id="Seg_6540" s="T417">дерево-PL</ta>
            <ta e="T419" id="Seg_6541" s="T418">много</ta>
            <ta e="T420" id="Seg_6542" s="T419">этот-PL</ta>
            <ta e="T421" id="Seg_6543" s="T420">надо</ta>
            <ta e="T422" id="Seg_6544" s="T421">резать-INF.LAT</ta>
            <ta e="T423" id="Seg_6545" s="T422">тогда</ta>
            <ta e="T424" id="Seg_6546" s="T423">снимать.кору-INF.LAT</ta>
            <ta e="T425" id="Seg_6547" s="T424">тогда</ta>
            <ta e="T426" id="Seg_6548" s="T425">дом.[NOM.SG]</ta>
            <ta e="T427" id="Seg_6549" s="T426">закрыть-INF.LAT</ta>
            <ta e="T428" id="Seg_6550" s="T427">тогда</ta>
            <ta e="T429" id="Seg_6551" s="T428">этот.[NOM.SG]</ta>
            <ta e="T430" id="Seg_6552" s="T429">дерево-3PL-INS</ta>
            <ta e="T431" id="Seg_6553" s="T430">дом.[NOM.SG]</ta>
            <ta e="T432" id="Seg_6554" s="T431">резать-INF.LAT</ta>
            <ta e="T433" id="Seg_6555" s="T432">надо</ta>
            <ta e="T434" id="Seg_6556" s="T433">жить-INF.LAT</ta>
            <ta e="T435" id="Seg_6557" s="T434">дом-LOC</ta>
            <ta e="T436" id="Seg_6558" s="T435">печь.[NOM.SG]</ta>
            <ta e="T437" id="Seg_6559" s="T436">класть-INF.LAT</ta>
            <ta e="T438" id="Seg_6560" s="T437">печь-LOC</ta>
            <ta e="T439" id="Seg_6561" s="T438">печь-INF.LAT</ta>
            <ta e="T440" id="Seg_6562" s="T439">хлеб.[NOM.SG]</ta>
            <ta e="T441" id="Seg_6563" s="T440">пить-EP-IMP.2SG</ta>
            <ta e="T442" id="Seg_6564" s="T441">вода.[NOM.SG]</ta>
            <ta e="T443" id="Seg_6565" s="T442">я.NOM</ta>
            <ta e="T444" id="Seg_6566" s="T443">там</ta>
            <ta e="T447" id="Seg_6567" s="T446">класть-PST-1SG</ta>
            <ta e="T448" id="Seg_6568" s="T447">сахар.[NOM.SG]</ta>
            <ta e="T451" id="Seg_6569" s="T450">вода.[NOM.SG]</ta>
            <ta e="T452" id="Seg_6570" s="T451">лить-PST-1SG</ta>
            <ta e="T455" id="Seg_6571" s="T454">сладкий.[NOM.SG]</ta>
            <ta e="T456" id="Seg_6572" s="T455">PTCL</ta>
            <ta e="T457" id="Seg_6573" s="T456">хлеб.[NOM.SG]</ta>
            <ta e="T458" id="Seg_6574" s="T457">класть-IPFVZ-PST-1SG</ta>
            <ta e="T459" id="Seg_6575" s="T458">пить-EP-IMP.2SG</ta>
            <ta e="T460" id="Seg_6576" s="T459">очень</ta>
            <ta e="T461" id="Seg_6577" s="T460">хороший.[NOM.SG]</ta>
            <ta e="T462" id="Seg_6578" s="T461">вы.NOM</ta>
            <ta e="T463" id="Seg_6579" s="T462">вчера</ta>
            <ta e="T464" id="Seg_6580" s="T463">водка.[NOM.SG]</ta>
            <ta e="T465" id="Seg_6581" s="T464">пить-PST-2PL</ta>
            <ta e="T468" id="Seg_6582" s="T467">пьяный.[NOM.SG]</ta>
            <ta e="T469" id="Seg_6583" s="T468">быть-PST-2PL</ta>
            <ta e="T470" id="Seg_6584" s="T469">весь</ta>
            <ta e="T474" id="Seg_6585" s="T472">сегодня</ta>
            <ta e="T475" id="Seg_6586" s="T474">весь</ta>
            <ta e="T478" id="Seg_6587" s="T477">пить-EP-IMP.2SG</ta>
            <ta e="T479" id="Seg_6588" s="T478">красный.[NOM.SG]</ta>
            <ta e="T480" id="Seg_6589" s="T479">вода.[NOM.SG]</ta>
            <ta e="T481" id="Seg_6590" s="T480">я.NOM</ta>
            <ta e="T482" id="Seg_6591" s="T481">там</ta>
            <ta e="T485" id="Seg_6592" s="T484">сахар.[NOM.SG]</ta>
            <ta e="T486" id="Seg_6593" s="T485">класть-PST-1SG</ta>
            <ta e="T488" id="Seg_6594" s="T487">вода.[NOM.SG]</ta>
            <ta e="T489" id="Seg_6595" s="T488">лить-PST-1SG</ta>
            <ta e="T492" id="Seg_6596" s="T491">кислый.[NOM.SG]</ta>
            <ta e="T493" id="Seg_6597" s="T492">стать-RES-PST.[3SG]</ta>
            <ta e="T494" id="Seg_6598" s="T493">хороший.[NOM.SG]</ta>
            <ta e="T495" id="Seg_6599" s="T494">стать-FUT-3SG</ta>
            <ta e="T496" id="Seg_6600" s="T495">ты.DAT</ta>
            <ta e="T497" id="Seg_6601" s="T496">водка.[NOM.SG]</ta>
            <ta e="T498" id="Seg_6602" s="T497">NEG.AUX-IMP.2SG</ta>
            <ta e="T499" id="Seg_6603" s="T498">пить-EP-CNG</ta>
            <ta e="T500" id="Seg_6604" s="T499">а.то</ta>
            <ta e="T501" id="Seg_6605" s="T500">голова-NOM/GEN/ACC.2SG</ta>
            <ta e="T502" id="Seg_6606" s="T501">болеть-PRS.[3SG]</ta>
            <ta e="T503" id="Seg_6607" s="T502">сердце-NOM/GEN/ACC.2SG</ta>
            <ta e="T504" id="Seg_6608" s="T503">болеть-PRS.[3SG]</ta>
            <ta e="T505" id="Seg_6609" s="T504">пить-FUT-2SG</ta>
            <ta e="T506" id="Seg_6610" s="T505">так</ta>
            <ta e="T507" id="Seg_6611" s="T506">умереть-RES-FUT-2SG</ta>
            <ta e="T508" id="Seg_6612" s="T507">а</ta>
            <ta e="T509" id="Seg_6613" s="T508">NEG</ta>
            <ta e="T510" id="Seg_6614" s="T509">пить-FUT-2SG</ta>
            <ta e="T511" id="Seg_6615" s="T510">так</ta>
            <ta e="T512" id="Seg_6616" s="T511">долго</ta>
            <ta e="T513" id="Seg_6617" s="T512">жить-FUT-2SG</ta>
            <ta e="T514" id="Seg_6618" s="T513">машина.[NOM.SG]</ta>
            <ta e="T519" id="Seg_6619" s="T518">пахать-DUR.[3SG]</ta>
            <ta e="T520" id="Seg_6620" s="T519">там</ta>
            <ta e="T521" id="Seg_6621" s="T520">мужчина.[NOM.SG]</ta>
            <ta e="T522" id="Seg_6622" s="T521">сидеть-DUR.[3SG]</ta>
            <ta e="T523" id="Seg_6623" s="T522">машина-ABL</ta>
            <ta e="T524" id="Seg_6624" s="T523">весь</ta>
            <ta e="T525" id="Seg_6625" s="T524">дым.[NOM.SG]</ta>
            <ta e="T526" id="Seg_6626" s="T525">прийти-PRS.[3SG]</ta>
            <ta e="T527" id="Seg_6627" s="T526">этот.[NOM.SG]</ta>
            <ta e="T528" id="Seg_6628" s="T527">весь</ta>
            <ta e="T529" id="Seg_6629" s="T528">там</ta>
            <ta e="T531" id="Seg_6630" s="T530">здесь</ta>
            <ta e="T532" id="Seg_6631" s="T531">пойти-DUR.[3SG]</ta>
            <ta e="T533" id="Seg_6632" s="T532">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T534" id="Seg_6633" s="T533">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T535" id="Seg_6634" s="T534">девушка.[NOM.SG]</ta>
            <ta e="T536" id="Seg_6635" s="T535">найти-PST-3PL</ta>
            <ta e="T537" id="Seg_6636" s="T536">я.NOM</ta>
            <ta e="T538" id="Seg_6637" s="T537">я.LAT</ta>
            <ta e="T539" id="Seg_6638" s="T538">сказать-PRS-3PL</ta>
            <ta e="T540" id="Seg_6639" s="T539">пойти-EP-IMP.2SG</ta>
            <ta e="T541" id="Seg_6640" s="T540">этот-INS</ta>
            <ta e="T544" id="Seg_6641" s="T543">знать-IMP.2SG.O</ta>
            <ta e="T545" id="Seg_6642" s="T544">какой</ta>
            <ta e="T546" id="Seg_6643" s="T545">этот.[NOM.SG]</ta>
            <ta e="T547" id="Seg_6644" s="T546">тогда</ta>
            <ta e="T548" id="Seg_6645" s="T547">мужчина-LAT</ta>
            <ta e="T549" id="Seg_6646" s="T548">взять-PRS-2SG</ta>
            <ta e="T550" id="Seg_6647" s="T549">тогда</ta>
            <ta e="T551" id="Seg_6648" s="T550">ребенок-PL-LAT</ta>
            <ta e="T552" id="Seg_6649" s="T551">стать-FUT-3PL</ta>
            <ta e="T554" id="Seg_6650" s="T553">ребенок-PL-NOM/GEN/ACC.2SG</ta>
            <ta e="T555" id="Seg_6651" s="T554">расти-FUT-3PL</ta>
            <ta e="T556" id="Seg_6652" s="T555">ты.DAT</ta>
            <ta e="T557" id="Seg_6653" s="T556">помогать-FUT-3PL</ta>
            <ta e="T558" id="Seg_6654" s="T557">женщина-ACC</ta>
            <ta e="T563" id="Seg_6655" s="T562">NEG.AUX-IMP.2SG</ta>
            <ta e="T564" id="Seg_6656" s="T563">бить-EP-CNG</ta>
            <ta e="T565" id="Seg_6657" s="T564">слушать-IMP.2SG.O</ta>
            <ta e="T570" id="Seg_6658" s="T569">отец-ACC</ta>
            <ta e="T571" id="Seg_6659" s="T570">и</ta>
            <ta e="T572" id="Seg_6660" s="T571">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T573" id="Seg_6661" s="T572">слушать-IMP.2SG.O</ta>
            <ta e="T578" id="Seg_6662" s="T577">сегодня</ta>
            <ta e="T579" id="Seg_6663" s="T578">весь</ta>
            <ta e="T580" id="Seg_6664" s="T579">пойти-CVB</ta>
            <ta e="T581" id="Seg_6665" s="T580">исчезнуть-PST-3PL</ta>
            <ta e="T582" id="Seg_6666" s="T581">отец-NOM/GEN.3SG</ta>
            <ta e="T583" id="Seg_6667" s="T582">мать-NOM/GEN.3SG</ta>
            <ta e="T584" id="Seg_6668" s="T583">пойти-CVB</ta>
            <ta e="T585" id="Seg_6669" s="T584">исчезнуть-PST-3PL</ta>
            <ta e="T586" id="Seg_6670" s="T585">ребенок-PL</ta>
            <ta e="T587" id="Seg_6671" s="T586">пойти-CVB</ta>
            <ta e="T588" id="Seg_6672" s="T587">исчезнуть-PST-3PL</ta>
            <ta e="T589" id="Seg_6673" s="T588">я.NOM</ta>
            <ta e="T590" id="Seg_6674" s="T589">один</ta>
            <ta e="T591" id="Seg_6675" s="T590">чум-LAT-2SG</ta>
            <ta e="T592" id="Seg_6676" s="T591">жить-DUR-1SG</ta>
            <ta e="T593" id="Seg_6677" s="T592">надо</ta>
            <ta e="T594" id="Seg_6678" s="T593">земля.[NOM.SG]</ta>
            <ta e="T595" id="Seg_6679" s="T594">копать-INF.LAT</ta>
            <ta e="T596" id="Seg_6680" s="T595">мужчина.[NOM.SG]</ta>
            <ta e="T597" id="Seg_6681" s="T596">видеть-RES-PST.[3SG]</ta>
            <ta e="T598" id="Seg_6682" s="T597">надо</ta>
            <ta e="T599" id="Seg_6683" s="T598">этот-ACC</ta>
            <ta e="T600" id="Seg_6684" s="T599">там</ta>
            <ta e="T601" id="Seg_6685" s="T600">класть-INF.LAT</ta>
            <ta e="T602" id="Seg_6686" s="T601">тогда</ta>
            <ta e="T603" id="Seg_6687" s="T602">лить-INF.LAT</ta>
            <ta e="T606" id="Seg_6688" s="T605">тогда</ta>
            <ta e="T611" id="Seg_6689" s="T610">плакать</ta>
            <ta e="T613" id="Seg_6690" s="T611">тогда</ta>
            <ta e="T614" id="Seg_6691" s="T613">тогда</ta>
            <ta e="T617" id="Seg_6692" s="T616">земля-INS</ta>
            <ta e="T618" id="Seg_6693" s="T617">надо</ta>
            <ta e="T619" id="Seg_6694" s="T618">закрыть-INF.LAT</ta>
            <ta e="T620" id="Seg_6695" s="T619">этот-ACC</ta>
            <ta e="T621" id="Seg_6696" s="T620">тогда</ta>
            <ta e="T622" id="Seg_6697" s="T621">чум-LAT-2SG</ta>
            <ta e="T625" id="Seg_6698" s="T624">прийти-INF.LAT</ta>
            <ta e="T626" id="Seg_6699" s="T625">съесть-INF.LAT</ta>
            <ta e="T629" id="Seg_6700" s="T628">сидеть-IMP.2SG</ta>
            <ta e="T630" id="Seg_6701" s="T629">съесть-INF.LAT</ta>
            <ta e="T631" id="Seg_6702" s="T630">мужчина.[NOM.SG]</ta>
            <ta e="T632" id="Seg_6703" s="T631">болеть-PST.[3SG]</ta>
            <ta e="T633" id="Seg_6704" s="T632">шаман-NOM/GEN/ACC.3SG</ta>
            <ta e="T634" id="Seg_6705" s="T633">идти-PST.[3SG]</ta>
            <ta e="T635" id="Seg_6706" s="T634">шаман.[NOM.SG]</ta>
            <ta e="T636" id="Seg_6707" s="T635">NEG</ta>
            <ta e="T637" id="Seg_6708" s="T636">мочь-PST.[3SG]</ta>
            <ta e="T638" id="Seg_6709" s="T637">этот-ACC</ta>
            <ta e="T639" id="Seg_6710" s="T638">вылечить-INF.LAT</ta>
            <ta e="T643" id="Seg_6711" s="T641">умереть-RES-PST</ta>
            <ta e="T644" id="Seg_6712" s="T643">сколько</ta>
            <ta e="T645" id="Seg_6713" s="T644">NEG</ta>
            <ta e="T646" id="Seg_6714" s="T645">болеть-PST.[3SG]</ta>
            <ta e="T647" id="Seg_6715" s="T646">сейчас</ta>
            <ta e="T648" id="Seg_6716" s="T647">весь</ta>
            <ta e="T649" id="Seg_6717" s="T648">лежать-DUR.[3SG]</ta>
            <ta e="T651" id="Seg_6718" s="T649">что-INS=INDEF</ta>
            <ta e="T652" id="Seg_6719" s="T651">NEG</ta>
            <ta e="T653" id="Seg_6720" s="T652">поднять-PRS</ta>
            <ta e="T654" id="Seg_6721" s="T653">я.ACC</ta>
            <ta e="T655" id="Seg_6722" s="T654">весь</ta>
            <ta e="T656" id="Seg_6723" s="T655">бить-PST-3PL</ta>
            <ta e="T657" id="Seg_6724" s="T656">очень</ta>
            <ta e="T658" id="Seg_6725" s="T657">сильно</ta>
            <ta e="T659" id="Seg_6726" s="T658">сам-NOM/GEN/ACC.1SG</ta>
            <ta e="T660" id="Seg_6727" s="T659">NEG</ta>
            <ta e="T661" id="Seg_6728" s="T660">знать-1SG</ta>
            <ta e="T662" id="Seg_6729" s="T661">что-INS</ta>
            <ta e="T663" id="Seg_6730" s="T662">бить-PST-3PL</ta>
            <ta e="T666" id="Seg_6731" s="T665">я.NOM</ta>
            <ta e="T667" id="Seg_6732" s="T666">я.NOM</ta>
            <ta e="T668" id="Seg_6733" s="T667">весь</ta>
            <ta e="T669" id="Seg_6734" s="T668">болеть-PST.[3SG]</ta>
            <ta e="T670" id="Seg_6735" s="T669">болеть-PRS-1SG</ta>
            <ta e="T671" id="Seg_6736" s="T670">голова-NOM/GEN/ACC.1SG</ta>
            <ta e="T672" id="Seg_6737" s="T671">болеть-PRS.[3SG]</ta>
            <ta e="T673" id="Seg_6738" s="T672">ухо-NOM/GEN/ACC.1SG</ta>
            <ta e="T674" id="Seg_6739" s="T673">болеть-PRS.[3SG]</ta>
            <ta e="T675" id="Seg_6740" s="T674">глаз-NOM/GEN/ACC.1SG</ta>
            <ta e="T676" id="Seg_6741" s="T675">болеть-PRS.[3SG]</ta>
            <ta e="T677" id="Seg_6742" s="T676">зуб-NOM/GEN/ACC.1SG</ta>
            <ta e="T678" id="Seg_6743" s="T677">болеть-PRS.[3SG]</ta>
            <ta e="T679" id="Seg_6744" s="T678">нос-NOM/GEN/ACC.1SG</ta>
            <ta e="T680" id="Seg_6745" s="T679">болеть-PRS.[3SG]</ta>
            <ta e="T681" id="Seg_6746" s="T680">рука-NOM/GEN/ACC.1SG</ta>
            <ta e="T682" id="Seg_6747" s="T681">болеть-PRS.[3SG]</ta>
            <ta e="T683" id="Seg_6748" s="T682">нога-NOM/GEN/ACC.1SG</ta>
            <ta e="T684" id="Seg_6749" s="T683">болеть-PRS.[3SG]</ta>
            <ta e="T685" id="Seg_6750" s="T684">живот-NOM/GEN/ACC.1SG</ta>
            <ta e="T686" id="Seg_6751" s="T685">болеть-PRS.[3SG]</ta>
            <ta e="T687" id="Seg_6752" s="T686">сердце-NOM/GEN/ACC.1SG</ta>
            <ta e="T688" id="Seg_6753" s="T687">болеть-PRS.[3SG]</ta>
            <ta e="T689" id="Seg_6754" s="T688">целый-ACC.3SG</ta>
            <ta e="T690" id="Seg_6755" s="T689">болеть-PRS.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PKZ">
            <ta e="T5" id="Seg_6756" s="T4">n-n:num</ta>
            <ta e="T6" id="Seg_6757" s="T5">adv</ta>
            <ta e="T7" id="Seg_6758" s="T6">quant</ta>
            <ta e="T8" id="Seg_6759" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_6760" s="T8">pers</ta>
            <ta e="T10" id="Seg_6761" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_6762" s="T10">que</ta>
            <ta e="T12" id="Seg_6763" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_6764" s="T12">dempro-n:num</ta>
            <ta e="T14" id="Seg_6765" s="T13">v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_6766" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_6767" s="T15">v-v:n.fin</ta>
            <ta e="T17" id="Seg_6768" s="T16">v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_6769" s="T17">v-v:mood.pn</ta>
            <ta e="T19" id="Seg_6770" s="T18">pers</ta>
            <ta e="T20" id="Seg_6771" s="T19">n.[n:case]</ta>
            <ta e="T21" id="Seg_6772" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_6773" s="T21">adv</ta>
            <ta e="T23" id="Seg_6774" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_6775" s="T23">quant</ta>
            <ta e="T25" id="Seg_6776" s="T24">v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_6777" s="T25">adv</ta>
            <ta e="T27" id="Seg_6778" s="T26">adj-n:case</ta>
            <ta e="T28" id="Seg_6779" s="T27">quant</ta>
            <ta e="T29" id="Seg_6780" s="T28">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_6781" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_6782" s="T30">v-v:n.fin</ta>
            <ta e="T32" id="Seg_6783" s="T31">dempro-n:case</ta>
            <ta e="T35" id="Seg_6784" s="T34">v-v:n.fin</ta>
            <ta e="T36" id="Seg_6785" s="T35">adv</ta>
            <ta e="T37" id="Seg_6786" s="T36">v-v:n.fin</ta>
            <ta e="T38" id="Seg_6787" s="T37">ptcl</ta>
            <ta e="T41" id="Seg_6788" s="T40">v-v:n.fin</ta>
            <ta e="T42" id="Seg_6789" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_6790" s="T42">adv</ta>
            <ta e="T44" id="Seg_6791" s="T43">v-v:n.fin</ta>
            <ta e="T45" id="Seg_6792" s="T44">v-v:n.fin</ta>
            <ta e="T46" id="Seg_6793" s="T45">adv</ta>
            <ta e="T47" id="Seg_6794" s="T46">v-v:n.fin</ta>
            <ta e="T50" id="Seg_6795" s="T49">v</ta>
            <ta e="T51" id="Seg_6796" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_6797" s="T51">v-v:mood.pn</ta>
            <ta e="T53" id="Seg_6798" s="T52">n-n:case.poss</ta>
            <ta e="T54" id="Seg_6799" s="T53">quant</ta>
            <ta e="T55" id="Seg_6800" s="T54">v-v:mood.pn</ta>
            <ta e="T56" id="Seg_6801" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_6802" s="T56">pers</ta>
            <ta e="T58" id="Seg_6803" s="T57">adv</ta>
            <ta e="T59" id="Seg_6804" s="T58">v-v:tense-v:pn</ta>
            <ta e="T60" id="Seg_6805" s="T59">v-v:n.fin</ta>
            <ta e="T61" id="Seg_6806" s="T60">v-v:n.fin</ta>
            <ta e="T62" id="Seg_6807" s="T61">adv</ta>
            <ta e="T63" id="Seg_6808" s="T62">v-v:tense-v:pn</ta>
            <ta e="T64" id="Seg_6809" s="T63">v-v:tense-v:pn</ta>
            <ta e="T65" id="Seg_6810" s="T64">pers-n:case</ta>
            <ta e="T66" id="Seg_6811" s="T65">conj</ta>
            <ta e="T67" id="Seg_6812" s="T66">v-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_6813" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_6814" s="T68">pers</ta>
            <ta e="T70" id="Seg_6815" s="T69">n-n:case.poss</ta>
            <ta e="T71" id="Seg_6816" s="T70">adj-n:case</ta>
            <ta e="T72" id="Seg_6817" s="T71">pers</ta>
            <ta e="T73" id="Seg_6818" s="T72">n-n:case.poss</ta>
            <ta e="T76" id="Seg_6819" s="T75">adv</ta>
            <ta e="T77" id="Seg_6820" s="T76">adj-n:case</ta>
            <ta e="T78" id="Seg_6821" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_6822" s="T78">adv</ta>
            <ta e="T80" id="Seg_6823" s="T79">adj-n:case</ta>
            <ta e="T81" id="Seg_6824" s="T80">n-n:case</ta>
            <ta e="T82" id="Seg_6825" s="T81">adv</ta>
            <ta e="T83" id="Seg_6826" s="T82">adj-n:case</ta>
            <ta e="T84" id="Seg_6827" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_6828" s="T84">adv</ta>
            <ta e="T86" id="Seg_6829" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_6830" s="T86">n-n:case</ta>
            <ta e="T88" id="Seg_6831" s="T87">adv</ta>
            <ta e="T89" id="Seg_6832" s="T88">adj-n:case</ta>
            <ta e="T90" id="Seg_6833" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_6834" s="T90">adv</ta>
            <ta e="T92" id="Seg_6835" s="T91">adj-n:case</ta>
            <ta e="T94" id="Seg_6836" s="T93">n-n:case</ta>
            <ta e="T95" id="Seg_6837" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_6838" s="T95">n-n:case</ta>
            <ta e="T97" id="Seg_6839" s="T96">v-v:tense-v:pn</ta>
            <ta e="T98" id="Seg_6840" s="T97">n-n:num-n:case.poss</ta>
            <ta e="T99" id="Seg_6841" s="T98">quant</ta>
            <ta e="T100" id="Seg_6842" s="T99">adj-n:case</ta>
            <ta e="T101" id="Seg_6843" s="T100">ptcl</ta>
            <ta e="T102" id="Seg_6844" s="T101">v-v:n.fin</ta>
            <ta e="T103" id="Seg_6845" s="T102">v-v:mood.pn</ta>
            <ta e="T104" id="Seg_6846" s="T103">adv</ta>
            <ta e="T105" id="Seg_6847" s="T104">aux-v:mood.pn</ta>
            <ta e="T106" id="Seg_6848" s="T105">v-v:ins-v:n.fin</ta>
            <ta e="T107" id="Seg_6849" s="T106">v-v:ins-v:mood.pn</ta>
            <ta e="T108" id="Seg_6850" s="T107">n-n:case-n:case.poss</ta>
            <ta e="T111" id="Seg_6851" s="T110">pers</ta>
            <ta e="T112" id="Seg_6852" s="T111">ptcl</ta>
            <ta e="T117" id="Seg_6853" s="T116">v-v&gt;v-v:pn</ta>
            <ta e="T118" id="Seg_6854" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_6855" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_6856" s="T119">n-n:num</ta>
            <ta e="T121" id="Seg_6857" s="T120">v-v:pn</ta>
            <ta e="T122" id="Seg_6858" s="T121">pers</ta>
            <ta e="T123" id="Seg_6859" s="T122">quant</ta>
            <ta e="T124" id="Seg_6860" s="T123">que</ta>
            <ta e="T125" id="Seg_6861" s="T124">v-v:tense-v:pn</ta>
            <ta e="T141" id="Seg_6862" s="T140">que</ta>
            <ta e="T143" id="Seg_6863" s="T142">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T144" id="Seg_6864" s="T143">aux-v:mood.pn</ta>
            <ta e="T145" id="Seg_6865" s="T144">v-v:ins-v:n.fin</ta>
            <ta e="T146" id="Seg_6866" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_6867" s="T146">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T148" id="Seg_6868" s="T147">v-v:mood.pn</ta>
            <ta e="T149" id="Seg_6869" s="T148">adv</ta>
            <ta e="T150" id="Seg_6870" s="T149">dempro-n:case</ta>
            <ta e="T151" id="Seg_6871" s="T150">n-n:case</ta>
            <ta e="T152" id="Seg_6872" s="T151">v-v&gt;v-v:pn</ta>
            <ta e="T153" id="Seg_6873" s="T152">adj-n:case</ta>
            <ta e="T154" id="Seg_6874" s="T153">quant</ta>
            <ta e="T159" id="Seg_6875" s="T158">que-n:case</ta>
            <ta e="T160" id="Seg_6876" s="T159">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T161" id="Seg_6877" s="T160">dempro-n:case</ta>
            <ta e="T162" id="Seg_6878" s="T161">ptcl</ta>
            <ta e="T163" id="Seg_6879" s="T162">v-v&gt;v-v:pn</ta>
            <ta e="T164" id="Seg_6880" s="T163">n-n:case</ta>
            <ta e="T167" id="Seg_6881" s="T166">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T168" id="Seg_6882" s="T167">n-n:num-n:case</ta>
            <ta e="T169" id="Seg_6883" s="T168">n-n:case</ta>
            <ta e="T170" id="Seg_6884" s="T169">v-v:tense-v:pn</ta>
            <ta e="T171" id="Seg_6885" s="T170">n-n:case</ta>
            <ta e="T172" id="Seg_6886" s="T171">v-v:tense-v:pn</ta>
            <ta e="T173" id="Seg_6887" s="T172">pers</ta>
            <ta e="T174" id="Seg_6888" s="T173">ptcl</ta>
            <ta e="T175" id="Seg_6889" s="T174">v-v:tense-v:pn</ta>
            <ta e="T176" id="Seg_6890" s="T175">ptcl</ta>
            <ta e="T178" id="Seg_6891" s="T177">n-n:case</ta>
            <ta e="T179" id="Seg_6892" s="T178">n-n:case</ta>
            <ta e="T180" id="Seg_6893" s="T179">n-n:case</ta>
            <ta e="T181" id="Seg_6894" s="T180">v-v:tense-v:pn</ta>
            <ta e="T182" id="Seg_6895" s="T181">n-n:case</ta>
            <ta e="T183" id="Seg_6896" s="T182">n-n:case</ta>
            <ta e="T186" id="Seg_6897" s="T185">v-v:tense-v:pn</ta>
            <ta e="T187" id="Seg_6898" s="T186">adv</ta>
            <ta e="T190" id="Seg_6899" s="T189">v-v:tense-v:pn</ta>
            <ta e="T191" id="Seg_6900" s="T190">adv</ta>
            <ta e="T192" id="Seg_6901" s="T191">v-v:tense-v:pn</ta>
            <ta e="T193" id="Seg_6902" s="T192">v</ta>
            <ta e="T194" id="Seg_6903" s="T193">adj-n:case</ta>
            <ta e="T195" id="Seg_6904" s="T194">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T196" id="Seg_6905" s="T195">adj-n:case</ta>
            <ta e="T197" id="Seg_6906" s="T196">v-v:n.fin</ta>
            <ta e="T198" id="Seg_6907" s="T197">v-v:tense-v:pn</ta>
            <ta e="T199" id="Seg_6908" s="T198">n-n:case</ta>
            <ta e="T200" id="Seg_6909" s="T199">adv</ta>
            <ta e="T201" id="Seg_6910" s="T200">v-v&gt;v-v:pn</ta>
            <ta e="T202" id="Seg_6911" s="T201">n-n:case</ta>
            <ta e="T203" id="Seg_6912" s="T202">v-v:tense-v:pn</ta>
            <ta e="T204" id="Seg_6913" s="T203">n-n:case</ta>
            <ta e="T205" id="Seg_6914" s="T204">quant</ta>
            <ta e="T206" id="Seg_6915" s="T205">adv</ta>
            <ta e="T207" id="Seg_6916" s="T206">adj-n:case</ta>
            <ta e="T208" id="Seg_6917" s="T207">pers</ta>
            <ta e="T209" id="Seg_6918" s="T208">n-n:case</ta>
            <ta e="T210" id="Seg_6919" s="T209">quant</ta>
            <ta e="T211" id="Seg_6920" s="T210">dempro-n:case</ta>
            <ta e="T212" id="Seg_6921" s="T211">n-n:case</ta>
            <ta e="T213" id="Seg_6922" s="T212">v-v:tense-v:pn</ta>
            <ta e="T214" id="Seg_6923" s="T213">que</ta>
            <ta e="T215" id="Seg_6924" s="T214">n-n:case.poss</ta>
            <ta e="T216" id="Seg_6925" s="T215">v-v:tense-v:pn</ta>
            <ta e="T217" id="Seg_6926" s="T216">ptcl</ta>
            <ta e="T218" id="Seg_6927" s="T217">n-n:case.poss</ta>
            <ta e="T219" id="Seg_6928" s="T218">v-v:tense-v:pn</ta>
            <ta e="T220" id="Seg_6929" s="T219">adv</ta>
            <ta e="T221" id="Seg_6930" s="T220">quant</ta>
            <ta e="T222" id="Seg_6931" s="T221">n-n:case</ta>
            <ta e="T223" id="Seg_6932" s="T222">v-v:n.fin</ta>
            <ta e="T224" id="Seg_6933" s="T223">ptcl</ta>
            <ta e="T225" id="Seg_6934" s="T224">dempro-n:num</ta>
            <ta e="T226" id="Seg_6935" s="T225">ptcl</ta>
            <ta e="T227" id="Seg_6936" s="T226">pers</ta>
            <ta e="T228" id="Seg_6937" s="T227">v-v:tense-v:pn</ta>
            <ta e="T229" id="Seg_6938" s="T228">v-v:n.fin</ta>
            <ta e="T230" id="Seg_6939" s="T229">n</ta>
            <ta e="T231" id="Seg_6940" s="T230">ptcl</ta>
            <ta e="T232" id="Seg_6941" s="T231">v-v:tense-v:pn</ta>
            <ta e="T233" id="Seg_6942" s="T232">v-v:n.fin</ta>
            <ta e="T234" id="Seg_6943" s="T233">pers</ta>
            <ta e="T235" id="Seg_6944" s="T234">v-v:tense-v:pn</ta>
            <ta e="T236" id="Seg_6945" s="T235">conj</ta>
            <ta e="T239" id="Seg_6946" s="T238">adv</ta>
            <ta e="T240" id="Seg_6947" s="T239">ptcl</ta>
            <ta e="T241" id="Seg_6948" s="T240">v-v:tense-v:pn</ta>
            <ta e="T242" id="Seg_6949" s="T241">adj-n:case</ta>
            <ta e="T243" id="Seg_6950" s="T242">n-n:case</ta>
            <ta e="T244" id="Seg_6951" s="T243">v-v&gt;v-v:pn</ta>
            <ta e="T245" id="Seg_6952" s="T244">adj-n:case</ta>
            <ta e="T246" id="Seg_6953" s="T245">n-n:case</ta>
            <ta e="T247" id="Seg_6954" s="T246">v-v&gt;v-v:pn</ta>
            <ta e="T248" id="Seg_6955" s="T247">pers</ta>
            <ta e="T249" id="Seg_6956" s="T248">adv</ta>
            <ta e="T250" id="Seg_6957" s="T249">adj-n:case</ta>
            <ta e="T251" id="Seg_6958" s="T250">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T252" id="Seg_6959" s="T251">num-num&gt;num</ta>
            <ta e="T253" id="Seg_6960" s="T252">v-v:tense-v:pn</ta>
            <ta e="T254" id="Seg_6961" s="T253">pers</ta>
            <ta e="T255" id="Seg_6962" s="T254">conj</ta>
            <ta e="T256" id="Seg_6963" s="T255">n-n:case.poss</ta>
            <ta e="T260" id="Seg_6964" s="T259">conj</ta>
            <ta e="T261" id="Seg_6965" s="T260">dempro-n:num</ta>
            <ta e="T262" id="Seg_6966" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_6967" s="T262">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T264" id="Seg_6968" s="T263">quant</ta>
            <ta e="T270" id="Seg_6969" s="T269">quant</ta>
            <ta e="T271" id="Seg_6970" s="T270">n-n:case</ta>
            <ta e="T272" id="Seg_6971" s="T271">v-v:tense-v:pn</ta>
            <ta e="T273" id="Seg_6972" s="T272">quant</ta>
            <ta e="T274" id="Seg_6973" s="T273">n-n:case</ta>
            <ta e="T275" id="Seg_6974" s="T274">n-n:case</ta>
            <ta e="T278" id="Seg_6975" s="T277">v-v:tense-v:pn</ta>
            <ta e="T279" id="Seg_6976" s="T278">n-n:case</ta>
            <ta e="T280" id="Seg_6977" s="T279">v-v:tense-v:pn</ta>
            <ta e="T281" id="Seg_6978" s="T280">n-n:case.poss</ta>
            <ta e="T282" id="Seg_6979" s="T281">v-v:tense-v:pn</ta>
            <ta e="T283" id="Seg_6980" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_6981" s="T283">v-v:n.fin</ta>
            <ta e="T285" id="Seg_6982" s="T284">pers</ta>
            <ta e="T286" id="Seg_6983" s="T285">adv</ta>
            <ta e="T289" id="Seg_6984" s="T288">n-n:case.poss</ta>
            <ta e="T290" id="Seg_6985" s="T289">adj-n:case</ta>
            <ta e="T291" id="Seg_6986" s="T290">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T292" id="Seg_6987" s="T291">pers</ta>
            <ta e="T293" id="Seg_6988" s="T292">pers</ta>
            <ta e="T294" id="Seg_6989" s="T293">n-n:case.poss</ta>
            <ta e="T295" id="Seg_6990" s="T294">adv</ta>
            <ta e="T296" id="Seg_6991" s="T295">adj-n:case</ta>
            <ta e="T297" id="Seg_6992" s="T296">n-n:case.poss</ta>
            <ta e="T300" id="Seg_6993" s="T299">adj-n:case</ta>
            <ta e="T301" id="Seg_6994" s="T300">n-n:case.poss</ta>
            <ta e="T302" id="Seg_6995" s="T301">quant</ta>
            <ta e="T303" id="Seg_6996" s="T302">v-v&gt;v-v:pn</ta>
            <ta e="T304" id="Seg_6997" s="T303">n-n:num-n:case.poss</ta>
            <ta e="T307" id="Seg_6998" s="T306">n-n:num-n:case.poss</ta>
            <ta e="T308" id="Seg_6999" s="T307">adj-n:case</ta>
            <ta e="T311" id="Seg_7000" s="T310">adv</ta>
            <ta e="T312" id="Seg_7001" s="T311">n-n:case.poss</ta>
            <ta e="T313" id="Seg_7002" s="T312">adj-n:case</ta>
            <ta e="T314" id="Seg_7003" s="T313">adv</ta>
            <ta e="T315" id="Seg_7004" s="T314">n-n:num-n:case.poss</ta>
            <ta e="T316" id="Seg_7005" s="T315">adj-n:case</ta>
            <ta e="T317" id="Seg_7006" s="T316">n-n:case.poss</ta>
            <ta e="T318" id="Seg_7007" s="T317">adj-n:case</ta>
            <ta e="T319" id="Seg_7008" s="T318">n-n:case.poss</ta>
            <ta e="T320" id="Seg_7009" s="T319">adv</ta>
            <ta e="T321" id="Seg_7010" s="T320">adj-n:case</ta>
            <ta e="T324" id="Seg_7011" s="T323">n-n:num-n:case.poss</ta>
            <ta e="T325" id="Seg_7012" s="T324">adj-n:case</ta>
            <ta e="T328" id="Seg_7013" s="T327">n-n:num-n:case.poss</ta>
            <ta e="T329" id="Seg_7014" s="T328">adj-n:case</ta>
            <ta e="T330" id="Seg_7015" s="T329">refl-n:case.poss</ta>
            <ta e="T331" id="Seg_7016" s="T330">quant</ta>
            <ta e="T332" id="Seg_7017" s="T331">adj-n:case</ta>
            <ta e="T333" id="Seg_7018" s="T332">n-n:case</ta>
            <ta e="T334" id="Seg_7019" s="T333">v-v&gt;v-v:pn</ta>
            <ta e="T335" id="Seg_7020" s="T334">n-n:case</ta>
            <ta e="T336" id="Seg_7021" s="T335">v-v:tense-v:pn</ta>
            <ta e="T337" id="Seg_7022" s="T336">quant</ta>
            <ta e="T342" id="Seg_7023" s="T341">n-n:num</ta>
            <ta e="T343" id="Seg_7024" s="T342">v-v&gt;v-v:pn</ta>
            <ta e="T344" id="Seg_7025" s="T343">n-n:case</ta>
            <ta e="T345" id="Seg_7026" s="T344">quant</ta>
            <ta e="T348" id="Seg_7027" s="T347">v-v&gt;v-v:pn</ta>
            <ta e="T349" id="Seg_7028" s="T348">n-n:case</ta>
            <ta e="T350" id="Seg_7029" s="T349">v-v:n.fin</ta>
            <ta e="T353" id="Seg_7030" s="T352">v-v:tense-v:pn</ta>
            <ta e="T354" id="Seg_7031" s="T353">n</ta>
            <ta e="T355" id="Seg_7032" s="T354">quant</ta>
            <ta e="T356" id="Seg_7033" s="T355">v-v:tense-v:pn</ta>
            <ta e="T357" id="Seg_7034" s="T356">v-v:n.fin</ta>
            <ta e="T358" id="Seg_7035" s="T357">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T359" id="Seg_7036" s="T358">v-v:tense-v:pn</ta>
            <ta e="T360" id="Seg_7037" s="T359">quant</ta>
            <ta e="T363" id="Seg_7038" s="T362">n-n:case</ta>
            <ta e="T364" id="Seg_7039" s="T363">v-v:n.fin</ta>
            <ta e="T365" id="Seg_7040" s="T364">n-n:case</ta>
            <ta e="T366" id="Seg_7041" s="T365">adj</ta>
            <ta e="T367" id="Seg_7042" s="T366">n-n:case</ta>
            <ta e="T368" id="Seg_7043" s="T367">v-v:pn</ta>
            <ta e="T371" id="Seg_7044" s="T370">pers</ta>
            <ta e="T372" id="Seg_7045" s="T371">adv</ta>
            <ta e="T373" id="Seg_7046" s="T372">n-n:case</ta>
            <ta e="T376" id="Seg_7047" s="T375">v-v:tense-v:pn</ta>
            <ta e="T377" id="Seg_7048" s="T376">%%-v:pn</ta>
            <ta e="T378" id="Seg_7049" s="T377">adv</ta>
            <ta e="T379" id="Seg_7050" s="T378">v-v:n.fin</ta>
            <ta e="T380" id="Seg_7051" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_7052" s="T380">n-n:case.poss</ta>
            <ta e="T382" id="Seg_7053" s="T381">n-n:case</ta>
            <ta e="T383" id="Seg_7054" s="T382">n-n:case</ta>
            <ta e="T384" id="Seg_7055" s="T383">%%</ta>
            <ta e="T385" id="Seg_7056" s="T384">n-n:case</ta>
            <ta e="T386" id="Seg_7057" s="T385">adj-n:case</ta>
            <ta e="T387" id="Seg_7058" s="T386">n-n:case</ta>
            <ta e="T388" id="Seg_7059" s="T387">n-n:case</ta>
            <ta e="T389" id="Seg_7060" s="T388">n-n:case</ta>
            <ta e="T390" id="Seg_7061" s="T389">%%</ta>
            <ta e="T391" id="Seg_7062" s="T390">n-n:case</ta>
            <ta e="T392" id="Seg_7063" s="T391">quant</ta>
            <ta e="T393" id="Seg_7064" s="T392">n-n:case.poss</ta>
            <ta e="T394" id="Seg_7065" s="T393">v-v&gt;v-v:pn</ta>
            <ta e="T395" id="Seg_7066" s="T394">n-n:case</ta>
            <ta e="T398" id="Seg_7067" s="T397">n-n:case</ta>
            <ta e="T399" id="Seg_7068" s="T398">quant</ta>
            <ta e="T400" id="Seg_7069" s="T399">n-n:case.poss</ta>
            <ta e="T401" id="Seg_7070" s="T400">v-v&gt;v-v:pn</ta>
            <ta e="T402" id="Seg_7071" s="T401">n-n:case</ta>
            <ta e="T403" id="Seg_7072" s="T402">n-n:case</ta>
            <ta e="T404" id="Seg_7073" s="T403">quant</ta>
            <ta e="T405" id="Seg_7074" s="T404">n-n:case</ta>
            <ta e="T406" id="Seg_7075" s="T405">v-v&gt;v-v:pn</ta>
            <ta e="T407" id="Seg_7076" s="T406">adj-n:case</ta>
            <ta e="T408" id="Seg_7077" s="T407">n-n:case</ta>
            <ta e="T409" id="Seg_7078" s="T408">v-v&gt;v-v:pn</ta>
            <ta e="T410" id="Seg_7079" s="T409">n-n&gt;adj</ta>
            <ta e="T411" id="Seg_7080" s="T410">n-n&gt;adj</ta>
            <ta e="T412" id="Seg_7081" s="T411">n-n:case</ta>
            <ta e="T413" id="Seg_7082" s="T412">n-n&gt;adj-n:case</ta>
            <ta e="T414" id="Seg_7083" s="T413">n-n:case</ta>
            <ta e="T415" id="Seg_7084" s="T414">n-n:case</ta>
            <ta e="T416" id="Seg_7085" s="T415">n-n&gt;adj-n:case</ta>
            <ta e="T417" id="Seg_7086" s="T416">adv</ta>
            <ta e="T418" id="Seg_7087" s="T417">n-n:num</ta>
            <ta e="T419" id="Seg_7088" s="T418">quant</ta>
            <ta e="T420" id="Seg_7089" s="T419">dempro-n:num</ta>
            <ta e="T421" id="Seg_7090" s="T420">ptcl</ta>
            <ta e="T422" id="Seg_7091" s="T421">v-v:n.fin</ta>
            <ta e="T423" id="Seg_7092" s="T422">adv</ta>
            <ta e="T424" id="Seg_7093" s="T423">v-v:n.fin</ta>
            <ta e="T425" id="Seg_7094" s="T424">adv</ta>
            <ta e="T426" id="Seg_7095" s="T425">n-n:case</ta>
            <ta e="T427" id="Seg_7096" s="T426">v-v:n.fin</ta>
            <ta e="T428" id="Seg_7097" s="T427">adv</ta>
            <ta e="T429" id="Seg_7098" s="T428">dempro-n:case</ta>
            <ta e="T430" id="Seg_7099" s="T429">n-n:case.poss-n:case</ta>
            <ta e="T431" id="Seg_7100" s="T430">n-n:case</ta>
            <ta e="T432" id="Seg_7101" s="T431">v-v:n.fin</ta>
            <ta e="T433" id="Seg_7102" s="T432">ptcl</ta>
            <ta e="T434" id="Seg_7103" s="T433">v-v:n.fin</ta>
            <ta e="T435" id="Seg_7104" s="T434">n-n:case</ta>
            <ta e="T436" id="Seg_7105" s="T435">n-n:case</ta>
            <ta e="T437" id="Seg_7106" s="T436">v-v:n.fin</ta>
            <ta e="T438" id="Seg_7107" s="T437">n-n:case</ta>
            <ta e="T439" id="Seg_7108" s="T438">v-v:n.fin</ta>
            <ta e="T440" id="Seg_7109" s="T439">n-n:case</ta>
            <ta e="T441" id="Seg_7110" s="T440">v-v:ins-v:mood.pn</ta>
            <ta e="T442" id="Seg_7111" s="T441">n-n:case</ta>
            <ta e="T443" id="Seg_7112" s="T442">pers</ta>
            <ta e="T444" id="Seg_7113" s="T443">adv</ta>
            <ta e="T447" id="Seg_7114" s="T446">v-v:tense-v:pn</ta>
            <ta e="T448" id="Seg_7115" s="T447">n-n:case</ta>
            <ta e="T451" id="Seg_7116" s="T450">n-n:case</ta>
            <ta e="T452" id="Seg_7117" s="T451">v-v:tense-v:pn</ta>
            <ta e="T455" id="Seg_7118" s="T454">adj-n:case</ta>
            <ta e="T456" id="Seg_7119" s="T455">ptcl</ta>
            <ta e="T457" id="Seg_7120" s="T456">n-n:case</ta>
            <ta e="T458" id="Seg_7121" s="T457">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T459" id="Seg_7122" s="T458">v-v:ins-v:mood.pn</ta>
            <ta e="T460" id="Seg_7123" s="T459">adv</ta>
            <ta e="T461" id="Seg_7124" s="T460">adj-n:case</ta>
            <ta e="T462" id="Seg_7125" s="T461">pers</ta>
            <ta e="T463" id="Seg_7126" s="T462">adv</ta>
            <ta e="T464" id="Seg_7127" s="T463">n-n:case</ta>
            <ta e="T465" id="Seg_7128" s="T464">v-v:tense-v:pn</ta>
            <ta e="T468" id="Seg_7129" s="T467">adj-n:case</ta>
            <ta e="T469" id="Seg_7130" s="T468">v-v:tense-v:pn</ta>
            <ta e="T470" id="Seg_7131" s="T469">quant</ta>
            <ta e="T474" id="Seg_7132" s="T472">adv</ta>
            <ta e="T475" id="Seg_7133" s="T474">quant</ta>
            <ta e="T478" id="Seg_7134" s="T477">v-v:ins-v:mood.pn</ta>
            <ta e="T479" id="Seg_7135" s="T478">adj-n:case</ta>
            <ta e="T480" id="Seg_7136" s="T479">n-n:case</ta>
            <ta e="T481" id="Seg_7137" s="T480">pers</ta>
            <ta e="T482" id="Seg_7138" s="T481">adv</ta>
            <ta e="T485" id="Seg_7139" s="T484">n-n:case</ta>
            <ta e="T486" id="Seg_7140" s="T485">v-v:tense-v:pn</ta>
            <ta e="T488" id="Seg_7141" s="T487">n-n:case</ta>
            <ta e="T489" id="Seg_7142" s="T488">v-v:tense-v:pn</ta>
            <ta e="T492" id="Seg_7143" s="T491">adj-n:case</ta>
            <ta e="T493" id="Seg_7144" s="T492">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T494" id="Seg_7145" s="T493">adj-n:case</ta>
            <ta e="T495" id="Seg_7146" s="T494">v-v:tense-v:pn</ta>
            <ta e="T496" id="Seg_7147" s="T495">pers</ta>
            <ta e="T497" id="Seg_7148" s="T496">n-n:case</ta>
            <ta e="T498" id="Seg_7149" s="T497">aux-v:mood.pn</ta>
            <ta e="T499" id="Seg_7150" s="T498">v-v:ins-v:n.fin</ta>
            <ta e="T500" id="Seg_7151" s="T499">ptcl</ta>
            <ta e="T501" id="Seg_7152" s="T500">n-n:case.poss</ta>
            <ta e="T502" id="Seg_7153" s="T501">v-v:tense-v:pn</ta>
            <ta e="T503" id="Seg_7154" s="T502">n-n:case.poss</ta>
            <ta e="T504" id="Seg_7155" s="T503">v-v:tense-v:pn</ta>
            <ta e="T505" id="Seg_7156" s="T504">v-v:tense-v:pn</ta>
            <ta e="T506" id="Seg_7157" s="T505">ptcl</ta>
            <ta e="T507" id="Seg_7158" s="T506">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T508" id="Seg_7159" s="T507">conj</ta>
            <ta e="T509" id="Seg_7160" s="T508">ptcl</ta>
            <ta e="T510" id="Seg_7161" s="T509">v-v:tense-v:pn</ta>
            <ta e="T511" id="Seg_7162" s="T510">ptcl</ta>
            <ta e="T512" id="Seg_7163" s="T511">adv</ta>
            <ta e="T513" id="Seg_7164" s="T512">v-v:tense-v:pn</ta>
            <ta e="T514" id="Seg_7165" s="T513">n-n:case</ta>
            <ta e="T519" id="Seg_7166" s="T518">v-v&gt;v-v:pn</ta>
            <ta e="T520" id="Seg_7167" s="T519">adv</ta>
            <ta e="T521" id="Seg_7168" s="T520">n-n:case</ta>
            <ta e="T522" id="Seg_7169" s="T521">v-v&gt;v-v:pn</ta>
            <ta e="T523" id="Seg_7170" s="T522">n-n:case</ta>
            <ta e="T524" id="Seg_7171" s="T523">quant</ta>
            <ta e="T525" id="Seg_7172" s="T524">n-n:case</ta>
            <ta e="T526" id="Seg_7173" s="T525">v-v:tense-v:pn</ta>
            <ta e="T527" id="Seg_7174" s="T526">dempro-n:case</ta>
            <ta e="T528" id="Seg_7175" s="T527">quant</ta>
            <ta e="T529" id="Seg_7176" s="T528">adv</ta>
            <ta e="T531" id="Seg_7177" s="T530">adv</ta>
            <ta e="T532" id="Seg_7178" s="T531">v-v&gt;v-v:pn</ta>
            <ta e="T533" id="Seg_7179" s="T532">n-n:case.poss</ta>
            <ta e="T534" id="Seg_7180" s="T533">n-n:case.poss</ta>
            <ta e="T535" id="Seg_7181" s="T534">n-n:case</ta>
            <ta e="T536" id="Seg_7182" s="T535">v-v:tense-v:pn</ta>
            <ta e="T537" id="Seg_7183" s="T536">pers</ta>
            <ta e="T538" id="Seg_7184" s="T537">pers</ta>
            <ta e="T539" id="Seg_7185" s="T538">v-v:tense-v:pn</ta>
            <ta e="T540" id="Seg_7186" s="T539">v-v:ins-v:mood.pn</ta>
            <ta e="T541" id="Seg_7187" s="T540">dempro-n:case</ta>
            <ta e="T544" id="Seg_7188" s="T543">v-v:mood.pn</ta>
            <ta e="T545" id="Seg_7189" s="T544">que</ta>
            <ta e="T546" id="Seg_7190" s="T545">dempro-n:case</ta>
            <ta e="T547" id="Seg_7191" s="T546">adv</ta>
            <ta e="T548" id="Seg_7192" s="T547">n-n:case</ta>
            <ta e="T549" id="Seg_7193" s="T548">v-v:tense-v:pn</ta>
            <ta e="T550" id="Seg_7194" s="T549">adv</ta>
            <ta e="T551" id="Seg_7195" s="T550">n-n:num-n:case</ta>
            <ta e="T552" id="Seg_7196" s="T551">v-v:tense-v:pn</ta>
            <ta e="T554" id="Seg_7197" s="T553">n-n:num-n:case.poss</ta>
            <ta e="T555" id="Seg_7198" s="T554">v-v:tense-v:pn</ta>
            <ta e="T556" id="Seg_7199" s="T555">pers</ta>
            <ta e="T557" id="Seg_7200" s="T556">v-v:tense-v:pn</ta>
            <ta e="T558" id="Seg_7201" s="T557">n-n:case</ta>
            <ta e="T563" id="Seg_7202" s="T562">aux-v:mood.pn</ta>
            <ta e="T564" id="Seg_7203" s="T563">v-v:ins-v:n.fin</ta>
            <ta e="T565" id="Seg_7204" s="T564">v-v:mood.pn</ta>
            <ta e="T570" id="Seg_7205" s="T569">n-n:case</ta>
            <ta e="T571" id="Seg_7206" s="T570">conj</ta>
            <ta e="T572" id="Seg_7207" s="T571">n-n:case.poss</ta>
            <ta e="T573" id="Seg_7208" s="T572">v-v:mood.pn</ta>
            <ta e="T578" id="Seg_7209" s="T577">adv</ta>
            <ta e="T579" id="Seg_7210" s="T578">quant</ta>
            <ta e="T580" id="Seg_7211" s="T579">v-v:n.fin</ta>
            <ta e="T581" id="Seg_7212" s="T580">v-v:tense-v:pn</ta>
            <ta e="T582" id="Seg_7213" s="T581">n-n:case.poss</ta>
            <ta e="T583" id="Seg_7214" s="T582">n-n:case.poss</ta>
            <ta e="T584" id="Seg_7215" s="T583">v-v:n.fin</ta>
            <ta e="T585" id="Seg_7216" s="T584">v-v:tense-v:pn</ta>
            <ta e="T586" id="Seg_7217" s="T585">n-n:num</ta>
            <ta e="T587" id="Seg_7218" s="T586">v-v:n.fin</ta>
            <ta e="T588" id="Seg_7219" s="T587">v-v:tense-v:pn</ta>
            <ta e="T589" id="Seg_7220" s="T588">pers</ta>
            <ta e="T590" id="Seg_7221" s="T589">adv</ta>
            <ta e="T591" id="Seg_7222" s="T590">n-n:case-n:case.poss</ta>
            <ta e="T592" id="Seg_7223" s="T591">v-v&gt;v-v:pn</ta>
            <ta e="T593" id="Seg_7224" s="T592">ptcl</ta>
            <ta e="T594" id="Seg_7225" s="T593">n-n:case</ta>
            <ta e="T595" id="Seg_7226" s="T594">v-v:n.fin</ta>
            <ta e="T596" id="Seg_7227" s="T595">n-n:case</ta>
            <ta e="T597" id="Seg_7228" s="T596">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T598" id="Seg_7229" s="T597">ptcl</ta>
            <ta e="T599" id="Seg_7230" s="T598">dempro-n:case</ta>
            <ta e="T600" id="Seg_7231" s="T599">adv</ta>
            <ta e="T601" id="Seg_7232" s="T600">v-v:n.fin</ta>
            <ta e="T602" id="Seg_7233" s="T601">adv</ta>
            <ta e="T603" id="Seg_7234" s="T602">v-v:n.fin</ta>
            <ta e="T606" id="Seg_7235" s="T605">adv</ta>
            <ta e="T611" id="Seg_7236" s="T610">v</ta>
            <ta e="T613" id="Seg_7237" s="T611">adv</ta>
            <ta e="T614" id="Seg_7238" s="T613">adv</ta>
            <ta e="T617" id="Seg_7239" s="T616">n-n:case</ta>
            <ta e="T618" id="Seg_7240" s="T617">ptcl</ta>
            <ta e="T619" id="Seg_7241" s="T618">v-v:n.fin</ta>
            <ta e="T620" id="Seg_7242" s="T619">dempro-n:case</ta>
            <ta e="T621" id="Seg_7243" s="T620">adv</ta>
            <ta e="T622" id="Seg_7244" s="T621">n-n:case-n:case.poss</ta>
            <ta e="T625" id="Seg_7245" s="T624">v-v:n.fin</ta>
            <ta e="T626" id="Seg_7246" s="T625">v-v:n.fin</ta>
            <ta e="T629" id="Seg_7247" s="T628">v-v:mood.pn</ta>
            <ta e="T630" id="Seg_7248" s="T629">v-v:n.fin</ta>
            <ta e="T631" id="Seg_7249" s="T630">n-n:case</ta>
            <ta e="T632" id="Seg_7250" s="T631">v-v:tense-v:pn</ta>
            <ta e="T633" id="Seg_7251" s="T632">n-n:case.poss</ta>
            <ta e="T634" id="Seg_7252" s="T633">v-v:tense-v:pn</ta>
            <ta e="T635" id="Seg_7253" s="T634">n-n:case</ta>
            <ta e="T636" id="Seg_7254" s="T635">ptcl</ta>
            <ta e="T637" id="Seg_7255" s="T636">v-v:tense-v:pn</ta>
            <ta e="T638" id="Seg_7256" s="T637">dempro-n:case</ta>
            <ta e="T639" id="Seg_7257" s="T638">v-v:n.fin</ta>
            <ta e="T643" id="Seg_7258" s="T641">v-v&gt;v-v:tense</ta>
            <ta e="T644" id="Seg_7259" s="T643">adv</ta>
            <ta e="T645" id="Seg_7260" s="T644">ptcl</ta>
            <ta e="T646" id="Seg_7261" s="T645">v-v:tense-v:pn</ta>
            <ta e="T647" id="Seg_7262" s="T646">adv</ta>
            <ta e="T648" id="Seg_7263" s="T647">quant</ta>
            <ta e="T649" id="Seg_7264" s="T648">v-v&gt;v-v:pn</ta>
            <ta e="T651" id="Seg_7265" s="T649">que-n:case=ptcl</ta>
            <ta e="T652" id="Seg_7266" s="T651">ptcl</ta>
            <ta e="T653" id="Seg_7267" s="T652">v-v:tense</ta>
            <ta e="T654" id="Seg_7268" s="T653">pers</ta>
            <ta e="T655" id="Seg_7269" s="T654">quant</ta>
            <ta e="T656" id="Seg_7270" s="T655">v-v:tense-v:pn</ta>
            <ta e="T657" id="Seg_7271" s="T656">adv</ta>
            <ta e="T658" id="Seg_7272" s="T657">adv</ta>
            <ta e="T659" id="Seg_7273" s="T658">refl-n:case.poss</ta>
            <ta e="T660" id="Seg_7274" s="T659">ptcl</ta>
            <ta e="T661" id="Seg_7275" s="T660">v-v:pn</ta>
            <ta e="T662" id="Seg_7276" s="T661">que-n:case</ta>
            <ta e="T663" id="Seg_7277" s="T662">v-v:tense-v:pn</ta>
            <ta e="T666" id="Seg_7278" s="T665">pers</ta>
            <ta e="T667" id="Seg_7279" s="T666">pers</ta>
            <ta e="T668" id="Seg_7280" s="T667">quant</ta>
            <ta e="T669" id="Seg_7281" s="T668">v-v:tense-v:pn</ta>
            <ta e="T670" id="Seg_7282" s="T669">v-v:tense-v:pn</ta>
            <ta e="T671" id="Seg_7283" s="T670">n-n:case.poss</ta>
            <ta e="T672" id="Seg_7284" s="T671">v-v:tense-v:pn</ta>
            <ta e="T673" id="Seg_7285" s="T672">n-n:case.poss</ta>
            <ta e="T674" id="Seg_7286" s="T673">v-v:tense-v:pn</ta>
            <ta e="T675" id="Seg_7287" s="T674">n-n:case.poss</ta>
            <ta e="T676" id="Seg_7288" s="T675">v-v:tense-v:pn</ta>
            <ta e="T677" id="Seg_7289" s="T676">n-n:case.poss</ta>
            <ta e="T678" id="Seg_7290" s="T677">v-v:tense-v:pn</ta>
            <ta e="T679" id="Seg_7291" s="T678">n-n:case.poss</ta>
            <ta e="T680" id="Seg_7292" s="T679">v-v:tense-v:pn</ta>
            <ta e="T681" id="Seg_7293" s="T680">n-n:case.poss</ta>
            <ta e="T682" id="Seg_7294" s="T681">v-v:tense-v:pn</ta>
            <ta e="T683" id="Seg_7295" s="T682">n-n:case.poss</ta>
            <ta e="T684" id="Seg_7296" s="T683">v-v:tense-v:pn</ta>
            <ta e="T685" id="Seg_7297" s="T684">n-n:case.poss</ta>
            <ta e="T686" id="Seg_7298" s="T685">v-v:tense-v:pn</ta>
            <ta e="T687" id="Seg_7299" s="T686">n-n:case.poss</ta>
            <ta e="T688" id="Seg_7300" s="T687">v-v:tense-v:pn</ta>
            <ta e="T689" id="Seg_7301" s="T688">n-n:case.poss</ta>
            <ta e="T690" id="Seg_7302" s="T689">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PKZ">
            <ta e="T5" id="Seg_7303" s="T4">n</ta>
            <ta e="T6" id="Seg_7304" s="T5">adv</ta>
            <ta e="T7" id="Seg_7305" s="T6">quant</ta>
            <ta e="T8" id="Seg_7306" s="T7">v</ta>
            <ta e="T9" id="Seg_7307" s="T8">pers</ta>
            <ta e="T10" id="Seg_7308" s="T9">v</ta>
            <ta e="T11" id="Seg_7309" s="T10">que</ta>
            <ta e="T12" id="Seg_7310" s="T11">v</ta>
            <ta e="T13" id="Seg_7311" s="T12">dempro</ta>
            <ta e="T14" id="Seg_7312" s="T13">v</ta>
            <ta e="T15" id="Seg_7313" s="T14">n</ta>
            <ta e="T16" id="Seg_7314" s="T15">v</ta>
            <ta e="T17" id="Seg_7315" s="T16">v</ta>
            <ta e="T18" id="Seg_7316" s="T17">v</ta>
            <ta e="T19" id="Seg_7317" s="T18">pers</ta>
            <ta e="T20" id="Seg_7318" s="T19">n</ta>
            <ta e="T21" id="Seg_7319" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_7320" s="T21">adv</ta>
            <ta e="T23" id="Seg_7321" s="T22">n</ta>
            <ta e="T24" id="Seg_7322" s="T23">quant</ta>
            <ta e="T25" id="Seg_7323" s="T24">v</ta>
            <ta e="T26" id="Seg_7324" s="T25">adv</ta>
            <ta e="T27" id="Seg_7325" s="T26">adj</ta>
            <ta e="T28" id="Seg_7326" s="T27">quant</ta>
            <ta e="T29" id="Seg_7327" s="T28">v</ta>
            <ta e="T30" id="Seg_7328" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_7329" s="T30">v</ta>
            <ta e="T32" id="Seg_7330" s="T31">dempro</ta>
            <ta e="T35" id="Seg_7331" s="T34">v</ta>
            <ta e="T36" id="Seg_7332" s="T35">adv</ta>
            <ta e="T37" id="Seg_7333" s="T36">v</ta>
            <ta e="T38" id="Seg_7334" s="T37">ptcl</ta>
            <ta e="T41" id="Seg_7335" s="T40">v</ta>
            <ta e="T42" id="Seg_7336" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_7337" s="T42">adv</ta>
            <ta e="T44" id="Seg_7338" s="T43">v</ta>
            <ta e="T45" id="Seg_7339" s="T44">v</ta>
            <ta e="T46" id="Seg_7340" s="T45">adv</ta>
            <ta e="T47" id="Seg_7341" s="T46">v</ta>
            <ta e="T50" id="Seg_7342" s="T49">v</ta>
            <ta e="T51" id="Seg_7343" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_7344" s="T51">v</ta>
            <ta e="T53" id="Seg_7345" s="T52">n</ta>
            <ta e="T54" id="Seg_7346" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_7347" s="T54">v</ta>
            <ta e="T56" id="Seg_7348" s="T55">n</ta>
            <ta e="T57" id="Seg_7349" s="T56">pers</ta>
            <ta e="T58" id="Seg_7350" s="T57">adv</ta>
            <ta e="T59" id="Seg_7351" s="T58">v</ta>
            <ta e="T60" id="Seg_7352" s="T59">v</ta>
            <ta e="T61" id="Seg_7353" s="T60">v</ta>
            <ta e="T62" id="Seg_7354" s="T61">adv</ta>
            <ta e="T63" id="Seg_7355" s="T62">v</ta>
            <ta e="T64" id="Seg_7356" s="T63">v</ta>
            <ta e="T65" id="Seg_7357" s="T64">pers</ta>
            <ta e="T66" id="Seg_7358" s="T65">conj</ta>
            <ta e="T67" id="Seg_7359" s="T66">v</ta>
            <ta e="T68" id="Seg_7360" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_7361" s="T68">pers</ta>
            <ta e="T70" id="Seg_7362" s="T69">n</ta>
            <ta e="T71" id="Seg_7363" s="T70">adj</ta>
            <ta e="T72" id="Seg_7364" s="T71">pers</ta>
            <ta e="T73" id="Seg_7365" s="T72">n</ta>
            <ta e="T76" id="Seg_7366" s="T75">adv</ta>
            <ta e="T77" id="Seg_7367" s="T76">adj</ta>
            <ta e="T78" id="Seg_7368" s="T77">n</ta>
            <ta e="T79" id="Seg_7369" s="T78">adv</ta>
            <ta e="T80" id="Seg_7370" s="T79">adj</ta>
            <ta e="T81" id="Seg_7371" s="T80">n</ta>
            <ta e="T82" id="Seg_7372" s="T81">adv</ta>
            <ta e="T83" id="Seg_7373" s="T82">adj</ta>
            <ta e="T84" id="Seg_7374" s="T83">n</ta>
            <ta e="T85" id="Seg_7375" s="T84">adv</ta>
            <ta e="T86" id="Seg_7376" s="T85">n</ta>
            <ta e="T87" id="Seg_7377" s="T86">n</ta>
            <ta e="T88" id="Seg_7378" s="T87">adv</ta>
            <ta e="T89" id="Seg_7379" s="T88">adj</ta>
            <ta e="T90" id="Seg_7380" s="T89">n</ta>
            <ta e="T91" id="Seg_7381" s="T90">adv</ta>
            <ta e="T92" id="Seg_7382" s="T91">adj</ta>
            <ta e="T94" id="Seg_7383" s="T93">n</ta>
            <ta e="T95" id="Seg_7384" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_7385" s="T95">n</ta>
            <ta e="T97" id="Seg_7386" s="T96">v</ta>
            <ta e="T98" id="Seg_7387" s="T97">n</ta>
            <ta e="T99" id="Seg_7388" s="T98">quant</ta>
            <ta e="T100" id="Seg_7389" s="T99">adj</ta>
            <ta e="T101" id="Seg_7390" s="T100">ptcl</ta>
            <ta e="T102" id="Seg_7391" s="T101">v</ta>
            <ta e="T103" id="Seg_7392" s="T102">v</ta>
            <ta e="T104" id="Seg_7393" s="T103">adv</ta>
            <ta e="T105" id="Seg_7394" s="T104">aux</ta>
            <ta e="T106" id="Seg_7395" s="T105">v</ta>
            <ta e="T107" id="Seg_7396" s="T106">v</ta>
            <ta e="T108" id="Seg_7397" s="T107">n</ta>
            <ta e="T111" id="Seg_7398" s="T110">pers</ta>
            <ta e="T112" id="Seg_7399" s="T111">ptcl</ta>
            <ta e="T117" id="Seg_7400" s="T116">v</ta>
            <ta e="T118" id="Seg_7401" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_7402" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_7403" s="T119">n</ta>
            <ta e="T121" id="Seg_7404" s="T120">v</ta>
            <ta e="T122" id="Seg_7405" s="T121">pers</ta>
            <ta e="T123" id="Seg_7406" s="T122">quant</ta>
            <ta e="T124" id="Seg_7407" s="T123">que</ta>
            <ta e="T125" id="Seg_7408" s="T124">v</ta>
            <ta e="T141" id="Seg_7409" s="T140">que</ta>
            <ta e="T143" id="Seg_7410" s="T142">v</ta>
            <ta e="T144" id="Seg_7411" s="T143">aux</ta>
            <ta e="T145" id="Seg_7412" s="T144">v</ta>
            <ta e="T146" id="Seg_7413" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_7414" s="T146">v</ta>
            <ta e="T148" id="Seg_7415" s="T147">v</ta>
            <ta e="T149" id="Seg_7416" s="T148">adv</ta>
            <ta e="T150" id="Seg_7417" s="T149">dempro</ta>
            <ta e="T151" id="Seg_7418" s="T150">n</ta>
            <ta e="T152" id="Seg_7419" s="T151">v</ta>
            <ta e="T153" id="Seg_7420" s="T152">adj</ta>
            <ta e="T154" id="Seg_7421" s="T153">quant</ta>
            <ta e="T159" id="Seg_7422" s="T158">que</ta>
            <ta e="T160" id="Seg_7423" s="T159">v</ta>
            <ta e="T161" id="Seg_7424" s="T160">dempro</ta>
            <ta e="T162" id="Seg_7425" s="T161">ptcl</ta>
            <ta e="T163" id="Seg_7426" s="T162">v</ta>
            <ta e="T164" id="Seg_7427" s="T163">n</ta>
            <ta e="T167" id="Seg_7428" s="T166">v</ta>
            <ta e="T168" id="Seg_7429" s="T167">n</ta>
            <ta e="T169" id="Seg_7430" s="T168">n</ta>
            <ta e="T170" id="Seg_7431" s="T169">v</ta>
            <ta e="T171" id="Seg_7432" s="T170">n</ta>
            <ta e="T172" id="Seg_7433" s="T171">v</ta>
            <ta e="T173" id="Seg_7434" s="T172">pers</ta>
            <ta e="T174" id="Seg_7435" s="T173">ptcl</ta>
            <ta e="T175" id="Seg_7436" s="T174">v</ta>
            <ta e="T176" id="Seg_7437" s="T175">ptcl</ta>
            <ta e="T178" id="Seg_7438" s="T177">n</ta>
            <ta e="T179" id="Seg_7439" s="T178">n</ta>
            <ta e="T180" id="Seg_7440" s="T179">n</ta>
            <ta e="T181" id="Seg_7441" s="T180">v</ta>
            <ta e="T182" id="Seg_7442" s="T181">n</ta>
            <ta e="T183" id="Seg_7443" s="T182">n</ta>
            <ta e="T186" id="Seg_7444" s="T185">v</ta>
            <ta e="T187" id="Seg_7445" s="T186">adv</ta>
            <ta e="T190" id="Seg_7446" s="T189">v</ta>
            <ta e="T191" id="Seg_7447" s="T190">adv</ta>
            <ta e="T192" id="Seg_7448" s="T191">v</ta>
            <ta e="T193" id="Seg_7449" s="T192">v</ta>
            <ta e="T194" id="Seg_7450" s="T193">adj</ta>
            <ta e="T195" id="Seg_7451" s="T194">v</ta>
            <ta e="T196" id="Seg_7452" s="T195">adj</ta>
            <ta e="T197" id="Seg_7453" s="T196">v</ta>
            <ta e="T198" id="Seg_7454" s="T197">v</ta>
            <ta e="T199" id="Seg_7455" s="T198">n</ta>
            <ta e="T200" id="Seg_7456" s="T199">adv</ta>
            <ta e="T201" id="Seg_7457" s="T200">v</ta>
            <ta e="T202" id="Seg_7458" s="T201">n</ta>
            <ta e="T203" id="Seg_7459" s="T202">v</ta>
            <ta e="T204" id="Seg_7460" s="T203">n</ta>
            <ta e="T205" id="Seg_7461" s="T204">quant</ta>
            <ta e="T206" id="Seg_7462" s="T205">adv</ta>
            <ta e="T207" id="Seg_7463" s="T206">adj</ta>
            <ta e="T208" id="Seg_7464" s="T207">pers</ta>
            <ta e="T209" id="Seg_7465" s="T208">n</ta>
            <ta e="T210" id="Seg_7466" s="T209">quant</ta>
            <ta e="T211" id="Seg_7467" s="T210">dempro</ta>
            <ta e="T212" id="Seg_7468" s="T211">n</ta>
            <ta e="T213" id="Seg_7469" s="T212">v</ta>
            <ta e="T214" id="Seg_7470" s="T213">que</ta>
            <ta e="T215" id="Seg_7471" s="T214">n</ta>
            <ta e="T216" id="Seg_7472" s="T215">v</ta>
            <ta e="T217" id="Seg_7473" s="T216">ptcl</ta>
            <ta e="T218" id="Seg_7474" s="T217">n</ta>
            <ta e="T219" id="Seg_7475" s="T218">v</ta>
            <ta e="T220" id="Seg_7476" s="T219">adv</ta>
            <ta e="T221" id="Seg_7477" s="T220">quant</ta>
            <ta e="T222" id="Seg_7478" s="T221">n</ta>
            <ta e="T223" id="Seg_7479" s="T222">v</ta>
            <ta e="T224" id="Seg_7480" s="T223">ptcl</ta>
            <ta e="T225" id="Seg_7481" s="T224">dempro</ta>
            <ta e="T226" id="Seg_7482" s="T225">ptcl</ta>
            <ta e="T227" id="Seg_7483" s="T226">pers</ta>
            <ta e="T228" id="Seg_7484" s="T227">v</ta>
            <ta e="T229" id="Seg_7485" s="T228">v</ta>
            <ta e="T230" id="Seg_7486" s="T229">n</ta>
            <ta e="T231" id="Seg_7487" s="T230">ptcl</ta>
            <ta e="T232" id="Seg_7488" s="T231">v</ta>
            <ta e="T233" id="Seg_7489" s="T232">v</ta>
            <ta e="T234" id="Seg_7490" s="T233">pers</ta>
            <ta e="T235" id="Seg_7491" s="T234">v</ta>
            <ta e="T236" id="Seg_7492" s="T235">conj</ta>
            <ta e="T239" id="Seg_7493" s="T238">adv</ta>
            <ta e="T240" id="Seg_7494" s="T239">ptcl</ta>
            <ta e="T241" id="Seg_7495" s="T240">v</ta>
            <ta e="T242" id="Seg_7496" s="T241">adj</ta>
            <ta e="T243" id="Seg_7497" s="T242">n</ta>
            <ta e="T244" id="Seg_7498" s="T243">v</ta>
            <ta e="T245" id="Seg_7499" s="T244">adj</ta>
            <ta e="T246" id="Seg_7500" s="T245">n</ta>
            <ta e="T247" id="Seg_7501" s="T246">v</ta>
            <ta e="T248" id="Seg_7502" s="T247">pers</ta>
            <ta e="T249" id="Seg_7503" s="T248">adv</ta>
            <ta e="T250" id="Seg_7504" s="T249">adj</ta>
            <ta e="T251" id="Seg_7505" s="T250">v</ta>
            <ta e="T252" id="Seg_7506" s="T251">num</ta>
            <ta e="T253" id="Seg_7507" s="T252">v</ta>
            <ta e="T254" id="Seg_7508" s="T253">pers</ta>
            <ta e="T255" id="Seg_7509" s="T254">conj</ta>
            <ta e="T256" id="Seg_7510" s="T255">n</ta>
            <ta e="T260" id="Seg_7511" s="T259">conj</ta>
            <ta e="T261" id="Seg_7512" s="T260">dempro</ta>
            <ta e="T262" id="Seg_7513" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_7514" s="T262">v</ta>
            <ta e="T264" id="Seg_7515" s="T263">quant</ta>
            <ta e="T270" id="Seg_7516" s="T269">quant</ta>
            <ta e="T271" id="Seg_7517" s="T270">n</ta>
            <ta e="T272" id="Seg_7518" s="T271">v</ta>
            <ta e="T273" id="Seg_7519" s="T272">quant</ta>
            <ta e="T274" id="Seg_7520" s="T273">n</ta>
            <ta e="T275" id="Seg_7521" s="T274">n</ta>
            <ta e="T278" id="Seg_7522" s="T277">v</ta>
            <ta e="T279" id="Seg_7523" s="T278">n</ta>
            <ta e="T280" id="Seg_7524" s="T279">v</ta>
            <ta e="T281" id="Seg_7525" s="T280">n</ta>
            <ta e="T282" id="Seg_7526" s="T281">v</ta>
            <ta e="T283" id="Seg_7527" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_7528" s="T283">v</ta>
            <ta e="T285" id="Seg_7529" s="T284">pers</ta>
            <ta e="T286" id="Seg_7530" s="T285">adv</ta>
            <ta e="T289" id="Seg_7531" s="T288">v</ta>
            <ta e="T290" id="Seg_7532" s="T289">adj</ta>
            <ta e="T291" id="Seg_7533" s="T290">v</ta>
            <ta e="T292" id="Seg_7534" s="T291">pers</ta>
            <ta e="T293" id="Seg_7535" s="T292">pers</ta>
            <ta e="T294" id="Seg_7536" s="T293">n</ta>
            <ta e="T295" id="Seg_7537" s="T294">adv</ta>
            <ta e="T296" id="Seg_7538" s="T295">adj</ta>
            <ta e="T297" id="Seg_7539" s="T296">n</ta>
            <ta e="T300" id="Seg_7540" s="T299">adj</ta>
            <ta e="T301" id="Seg_7541" s="T300">v</ta>
            <ta e="T302" id="Seg_7542" s="T301">quant</ta>
            <ta e="T303" id="Seg_7543" s="T302">v</ta>
            <ta e="T304" id="Seg_7544" s="T303">n</ta>
            <ta e="T307" id="Seg_7545" s="T306">n</ta>
            <ta e="T308" id="Seg_7546" s="T307">adj</ta>
            <ta e="T311" id="Seg_7547" s="T310">adv</ta>
            <ta e="T312" id="Seg_7548" s="T311">n</ta>
            <ta e="T313" id="Seg_7549" s="T312">adj</ta>
            <ta e="T314" id="Seg_7550" s="T313">adv</ta>
            <ta e="T315" id="Seg_7551" s="T314">n</ta>
            <ta e="T316" id="Seg_7552" s="T315">adj</ta>
            <ta e="T317" id="Seg_7553" s="T316">v</ta>
            <ta e="T318" id="Seg_7554" s="T317">adj</ta>
            <ta e="T319" id="Seg_7555" s="T318">n</ta>
            <ta e="T320" id="Seg_7556" s="T319">adv</ta>
            <ta e="T321" id="Seg_7557" s="T320">adj</ta>
            <ta e="T324" id="Seg_7558" s="T323">n</ta>
            <ta e="T325" id="Seg_7559" s="T324">adj</ta>
            <ta e="T328" id="Seg_7560" s="T327">n</ta>
            <ta e="T329" id="Seg_7561" s="T328">adj</ta>
            <ta e="T330" id="Seg_7562" s="T329">refl</ta>
            <ta e="T331" id="Seg_7563" s="T330">quant</ta>
            <ta e="T332" id="Seg_7564" s="T331">adj</ta>
            <ta e="T333" id="Seg_7565" s="T332">n</ta>
            <ta e="T334" id="Seg_7566" s="T333">v</ta>
            <ta e="T335" id="Seg_7567" s="T334">n</ta>
            <ta e="T336" id="Seg_7568" s="T335">v</ta>
            <ta e="T337" id="Seg_7569" s="T336">quant</ta>
            <ta e="T342" id="Seg_7570" s="T341">n</ta>
            <ta e="T343" id="Seg_7571" s="T342">v</ta>
            <ta e="T344" id="Seg_7572" s="T343">n</ta>
            <ta e="T345" id="Seg_7573" s="T344">quant</ta>
            <ta e="T348" id="Seg_7574" s="T347">v</ta>
            <ta e="T349" id="Seg_7575" s="T348">n</ta>
            <ta e="T350" id="Seg_7576" s="T349">v</ta>
            <ta e="T353" id="Seg_7577" s="T352">v</ta>
            <ta e="T354" id="Seg_7578" s="T353">n</ta>
            <ta e="T355" id="Seg_7579" s="T354">quant</ta>
            <ta e="T356" id="Seg_7580" s="T355">v</ta>
            <ta e="T357" id="Seg_7581" s="T356">v</ta>
            <ta e="T358" id="Seg_7582" s="T357">v</ta>
            <ta e="T359" id="Seg_7583" s="T358">v</ta>
            <ta e="T360" id="Seg_7584" s="T359">quant</ta>
            <ta e="T363" id="Seg_7585" s="T362">n</ta>
            <ta e="T364" id="Seg_7586" s="T363">v</ta>
            <ta e="T365" id="Seg_7587" s="T364">n</ta>
            <ta e="T366" id="Seg_7588" s="T365">adj</ta>
            <ta e="T367" id="Seg_7589" s="T366">n</ta>
            <ta e="T368" id="Seg_7590" s="T367">v</ta>
            <ta e="T371" id="Seg_7591" s="T370">pers</ta>
            <ta e="T372" id="Seg_7592" s="T371">adv</ta>
            <ta e="T373" id="Seg_7593" s="T372">n</ta>
            <ta e="T376" id="Seg_7594" s="T375">v</ta>
            <ta e="T377" id="Seg_7595" s="T376">v</ta>
            <ta e="T378" id="Seg_7596" s="T377">adv</ta>
            <ta e="T379" id="Seg_7597" s="T378">v</ta>
            <ta e="T380" id="Seg_7598" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_7599" s="T380">n</ta>
            <ta e="T382" id="Seg_7600" s="T381">n</ta>
            <ta e="T383" id="Seg_7601" s="T382">n</ta>
            <ta e="T385" id="Seg_7602" s="T384">n</ta>
            <ta e="T386" id="Seg_7603" s="T385">adj</ta>
            <ta e="T387" id="Seg_7604" s="T386">n</ta>
            <ta e="T388" id="Seg_7605" s="T387">n</ta>
            <ta e="T389" id="Seg_7606" s="T388">n</ta>
            <ta e="T391" id="Seg_7607" s="T390">n</ta>
            <ta e="T392" id="Seg_7608" s="T391">quant</ta>
            <ta e="T393" id="Seg_7609" s="T392">n</ta>
            <ta e="T394" id="Seg_7610" s="T393">v</ta>
            <ta e="T395" id="Seg_7611" s="T394">n</ta>
            <ta e="T398" id="Seg_7612" s="T397">n</ta>
            <ta e="T399" id="Seg_7613" s="T398">quant</ta>
            <ta e="T400" id="Seg_7614" s="T399">n</ta>
            <ta e="T401" id="Seg_7615" s="T400">v</ta>
            <ta e="T402" id="Seg_7616" s="T401">n</ta>
            <ta e="T403" id="Seg_7617" s="T402">n</ta>
            <ta e="T404" id="Seg_7618" s="T403">quant</ta>
            <ta e="T405" id="Seg_7619" s="T404">n</ta>
            <ta e="T406" id="Seg_7620" s="T405">v</ta>
            <ta e="T407" id="Seg_7621" s="T406">adj</ta>
            <ta e="T408" id="Seg_7622" s="T407">n</ta>
            <ta e="T409" id="Seg_7623" s="T408">v</ta>
            <ta e="T410" id="Seg_7624" s="T409">adj</ta>
            <ta e="T411" id="Seg_7625" s="T410">adj</ta>
            <ta e="T412" id="Seg_7626" s="T411">n</ta>
            <ta e="T413" id="Seg_7627" s="T412">n</ta>
            <ta e="T414" id="Seg_7628" s="T413">n</ta>
            <ta e="T415" id="Seg_7629" s="T414">n</ta>
            <ta e="T416" id="Seg_7630" s="T415">n</ta>
            <ta e="T417" id="Seg_7631" s="T416">adv</ta>
            <ta e="T418" id="Seg_7632" s="T417">n</ta>
            <ta e="T419" id="Seg_7633" s="T418">quant</ta>
            <ta e="T420" id="Seg_7634" s="T419">dempro</ta>
            <ta e="T421" id="Seg_7635" s="T420">ptcl</ta>
            <ta e="T422" id="Seg_7636" s="T421">v</ta>
            <ta e="T423" id="Seg_7637" s="T422">adv</ta>
            <ta e="T424" id="Seg_7638" s="T423">v</ta>
            <ta e="T425" id="Seg_7639" s="T424">adv</ta>
            <ta e="T426" id="Seg_7640" s="T425">n</ta>
            <ta e="T427" id="Seg_7641" s="T426">v</ta>
            <ta e="T428" id="Seg_7642" s="T427">adv</ta>
            <ta e="T429" id="Seg_7643" s="T428">dempro</ta>
            <ta e="T430" id="Seg_7644" s="T429">n</ta>
            <ta e="T431" id="Seg_7645" s="T430">n</ta>
            <ta e="T432" id="Seg_7646" s="T431">v</ta>
            <ta e="T433" id="Seg_7647" s="T432">ptcl</ta>
            <ta e="T434" id="Seg_7648" s="T433">v</ta>
            <ta e="T435" id="Seg_7649" s="T434">n</ta>
            <ta e="T436" id="Seg_7650" s="T435">n</ta>
            <ta e="T437" id="Seg_7651" s="T436">v</ta>
            <ta e="T438" id="Seg_7652" s="T437">n</ta>
            <ta e="T439" id="Seg_7653" s="T438">v</ta>
            <ta e="T440" id="Seg_7654" s="T439">n</ta>
            <ta e="T441" id="Seg_7655" s="T440">v</ta>
            <ta e="T442" id="Seg_7656" s="T441">n</ta>
            <ta e="T443" id="Seg_7657" s="T442">pers</ta>
            <ta e="T444" id="Seg_7658" s="T443">adv</ta>
            <ta e="T447" id="Seg_7659" s="T446">v</ta>
            <ta e="T448" id="Seg_7660" s="T447">n</ta>
            <ta e="T451" id="Seg_7661" s="T450">n</ta>
            <ta e="T452" id="Seg_7662" s="T451">v</ta>
            <ta e="T455" id="Seg_7663" s="T454">adj</ta>
            <ta e="T456" id="Seg_7664" s="T455">ptcl</ta>
            <ta e="T457" id="Seg_7665" s="T456">n</ta>
            <ta e="T458" id="Seg_7666" s="T457">v</ta>
            <ta e="T459" id="Seg_7667" s="T458">v</ta>
            <ta e="T460" id="Seg_7668" s="T459">adv</ta>
            <ta e="T461" id="Seg_7669" s="T460">adj</ta>
            <ta e="T462" id="Seg_7670" s="T461">pers</ta>
            <ta e="T463" id="Seg_7671" s="T462">adv</ta>
            <ta e="T464" id="Seg_7672" s="T463">n</ta>
            <ta e="T465" id="Seg_7673" s="T464">v</ta>
            <ta e="T468" id="Seg_7674" s="T467">adj</ta>
            <ta e="T469" id="Seg_7675" s="T468">v</ta>
            <ta e="T470" id="Seg_7676" s="T469">quant</ta>
            <ta e="T474" id="Seg_7677" s="T472">adv</ta>
            <ta e="T475" id="Seg_7678" s="T474">quant</ta>
            <ta e="T478" id="Seg_7679" s="T477">v</ta>
            <ta e="T479" id="Seg_7680" s="T478">adj</ta>
            <ta e="T480" id="Seg_7681" s="T479">n</ta>
            <ta e="T481" id="Seg_7682" s="T480">pers</ta>
            <ta e="T482" id="Seg_7683" s="T481">adv</ta>
            <ta e="T485" id="Seg_7684" s="T484">n</ta>
            <ta e="T486" id="Seg_7685" s="T485">v</ta>
            <ta e="T488" id="Seg_7686" s="T487">n</ta>
            <ta e="T489" id="Seg_7687" s="T488">v</ta>
            <ta e="T492" id="Seg_7688" s="T491">adj</ta>
            <ta e="T493" id="Seg_7689" s="T492">v</ta>
            <ta e="T494" id="Seg_7690" s="T493">adj</ta>
            <ta e="T495" id="Seg_7691" s="T494">v</ta>
            <ta e="T496" id="Seg_7692" s="T495">pers</ta>
            <ta e="T497" id="Seg_7693" s="T496">n</ta>
            <ta e="T498" id="Seg_7694" s="T497">aux</ta>
            <ta e="T499" id="Seg_7695" s="T498">v</ta>
            <ta e="T500" id="Seg_7696" s="T499">ptcl</ta>
            <ta e="T501" id="Seg_7697" s="T500">n</ta>
            <ta e="T502" id="Seg_7698" s="T501">v</ta>
            <ta e="T503" id="Seg_7699" s="T502">n</ta>
            <ta e="T504" id="Seg_7700" s="T503">v</ta>
            <ta e="T505" id="Seg_7701" s="T504">v</ta>
            <ta e="T506" id="Seg_7702" s="T505">ptcl</ta>
            <ta e="T507" id="Seg_7703" s="T506">v</ta>
            <ta e="T508" id="Seg_7704" s="T507">conj</ta>
            <ta e="T509" id="Seg_7705" s="T508">ptcl</ta>
            <ta e="T510" id="Seg_7706" s="T509">v</ta>
            <ta e="T511" id="Seg_7707" s="T510">ptcl</ta>
            <ta e="T512" id="Seg_7708" s="T511">adv</ta>
            <ta e="T513" id="Seg_7709" s="T512">v</ta>
            <ta e="T514" id="Seg_7710" s="T513">n</ta>
            <ta e="T519" id="Seg_7711" s="T518">v</ta>
            <ta e="T520" id="Seg_7712" s="T519">adv</ta>
            <ta e="T521" id="Seg_7713" s="T520">n</ta>
            <ta e="T522" id="Seg_7714" s="T521">v</ta>
            <ta e="T523" id="Seg_7715" s="T522">n</ta>
            <ta e="T524" id="Seg_7716" s="T523">quant</ta>
            <ta e="T525" id="Seg_7717" s="T524">n</ta>
            <ta e="T526" id="Seg_7718" s="T525">v</ta>
            <ta e="T527" id="Seg_7719" s="T526">dempro</ta>
            <ta e="T528" id="Seg_7720" s="T527">quant</ta>
            <ta e="T529" id="Seg_7721" s="T528">adv</ta>
            <ta e="T531" id="Seg_7722" s="T530">adv</ta>
            <ta e="T532" id="Seg_7723" s="T531">v</ta>
            <ta e="T533" id="Seg_7724" s="T532">n</ta>
            <ta e="T534" id="Seg_7725" s="T533">n</ta>
            <ta e="T535" id="Seg_7726" s="T534">n</ta>
            <ta e="T536" id="Seg_7727" s="T535">v</ta>
            <ta e="T537" id="Seg_7728" s="T536">pers</ta>
            <ta e="T538" id="Seg_7729" s="T537">pers</ta>
            <ta e="T539" id="Seg_7730" s="T538">v</ta>
            <ta e="T540" id="Seg_7731" s="T539">v</ta>
            <ta e="T541" id="Seg_7732" s="T540">dempro</ta>
            <ta e="T544" id="Seg_7733" s="T543">v</ta>
            <ta e="T545" id="Seg_7734" s="T544">que</ta>
            <ta e="T546" id="Seg_7735" s="T545">dempro</ta>
            <ta e="T547" id="Seg_7736" s="T546">adv</ta>
            <ta e="T548" id="Seg_7737" s="T547">n</ta>
            <ta e="T549" id="Seg_7738" s="T548">v</ta>
            <ta e="T550" id="Seg_7739" s="T549">adv</ta>
            <ta e="T551" id="Seg_7740" s="T550">n</ta>
            <ta e="T552" id="Seg_7741" s="T551">v</ta>
            <ta e="T554" id="Seg_7742" s="T553">n</ta>
            <ta e="T555" id="Seg_7743" s="T554">v</ta>
            <ta e="T556" id="Seg_7744" s="T555">pers</ta>
            <ta e="T557" id="Seg_7745" s="T556">v</ta>
            <ta e="T558" id="Seg_7746" s="T557">n</ta>
            <ta e="T563" id="Seg_7747" s="T562">aux</ta>
            <ta e="T564" id="Seg_7748" s="T563">v</ta>
            <ta e="T565" id="Seg_7749" s="T564">v</ta>
            <ta e="T570" id="Seg_7750" s="T569">n</ta>
            <ta e="T571" id="Seg_7751" s="T570">conj</ta>
            <ta e="T572" id="Seg_7752" s="T571">n</ta>
            <ta e="T573" id="Seg_7753" s="T572">v</ta>
            <ta e="T578" id="Seg_7754" s="T577">adv</ta>
            <ta e="T579" id="Seg_7755" s="T578">quant</ta>
            <ta e="T580" id="Seg_7756" s="T579">v</ta>
            <ta e="T581" id="Seg_7757" s="T580">v</ta>
            <ta e="T582" id="Seg_7758" s="T581">n</ta>
            <ta e="T583" id="Seg_7759" s="T582">n</ta>
            <ta e="T584" id="Seg_7760" s="T583">v</ta>
            <ta e="T585" id="Seg_7761" s="T584">v</ta>
            <ta e="T586" id="Seg_7762" s="T585">n</ta>
            <ta e="T587" id="Seg_7763" s="T586">v</ta>
            <ta e="T588" id="Seg_7764" s="T587">v</ta>
            <ta e="T589" id="Seg_7765" s="T588">pers</ta>
            <ta e="T590" id="Seg_7766" s="T589">adv</ta>
            <ta e="T591" id="Seg_7767" s="T590">n</ta>
            <ta e="T592" id="Seg_7768" s="T591">v</ta>
            <ta e="T593" id="Seg_7769" s="T592">ptcl</ta>
            <ta e="T594" id="Seg_7770" s="T593">n</ta>
            <ta e="T595" id="Seg_7771" s="T594">v</ta>
            <ta e="T596" id="Seg_7772" s="T595">n</ta>
            <ta e="T597" id="Seg_7773" s="T596">v</ta>
            <ta e="T598" id="Seg_7774" s="T597">ptcl</ta>
            <ta e="T599" id="Seg_7775" s="T598">dempro</ta>
            <ta e="T600" id="Seg_7776" s="T599">adv</ta>
            <ta e="T601" id="Seg_7777" s="T600">v</ta>
            <ta e="T602" id="Seg_7778" s="T601">adv</ta>
            <ta e="T603" id="Seg_7779" s="T602">v</ta>
            <ta e="T606" id="Seg_7780" s="T605">adv</ta>
            <ta e="T611" id="Seg_7781" s="T610">v</ta>
            <ta e="T613" id="Seg_7782" s="T611">adv</ta>
            <ta e="T614" id="Seg_7783" s="T613">adv</ta>
            <ta e="T617" id="Seg_7784" s="T616">n</ta>
            <ta e="T618" id="Seg_7785" s="T617">ptcl</ta>
            <ta e="T619" id="Seg_7786" s="T618">v</ta>
            <ta e="T620" id="Seg_7787" s="T619">dempro</ta>
            <ta e="T621" id="Seg_7788" s="T620">adv</ta>
            <ta e="T622" id="Seg_7789" s="T621">n</ta>
            <ta e="T625" id="Seg_7790" s="T624">v</ta>
            <ta e="T626" id="Seg_7791" s="T625">v</ta>
            <ta e="T629" id="Seg_7792" s="T628">v</ta>
            <ta e="T630" id="Seg_7793" s="T629">v</ta>
            <ta e="T631" id="Seg_7794" s="T630">n</ta>
            <ta e="T632" id="Seg_7795" s="T631">v</ta>
            <ta e="T633" id="Seg_7796" s="T632">n</ta>
            <ta e="T634" id="Seg_7797" s="T633">v</ta>
            <ta e="T635" id="Seg_7798" s="T634">n</ta>
            <ta e="T636" id="Seg_7799" s="T635">ptcl</ta>
            <ta e="T637" id="Seg_7800" s="T636">v</ta>
            <ta e="T638" id="Seg_7801" s="T637">dempro</ta>
            <ta e="T639" id="Seg_7802" s="T638">v</ta>
            <ta e="T643" id="Seg_7803" s="T641">v</ta>
            <ta e="T644" id="Seg_7804" s="T643">adv</ta>
            <ta e="T645" id="Seg_7805" s="T644">ptcl</ta>
            <ta e="T646" id="Seg_7806" s="T645">v</ta>
            <ta e="T647" id="Seg_7807" s="T646">adv</ta>
            <ta e="T648" id="Seg_7808" s="T647">quant</ta>
            <ta e="T649" id="Seg_7809" s="T648">v</ta>
            <ta e="T651" id="Seg_7810" s="T649">que</ta>
            <ta e="T652" id="Seg_7811" s="T651">ptcl</ta>
            <ta e="T653" id="Seg_7812" s="T652">v</ta>
            <ta e="T654" id="Seg_7813" s="T653">pers</ta>
            <ta e="T655" id="Seg_7814" s="T654">quant</ta>
            <ta e="T656" id="Seg_7815" s="T655">v</ta>
            <ta e="T657" id="Seg_7816" s="T656">adv</ta>
            <ta e="T658" id="Seg_7817" s="T657">adv</ta>
            <ta e="T659" id="Seg_7818" s="T658">refl</ta>
            <ta e="T660" id="Seg_7819" s="T659">ptcl</ta>
            <ta e="T661" id="Seg_7820" s="T660">v</ta>
            <ta e="T662" id="Seg_7821" s="T661">que</ta>
            <ta e="T663" id="Seg_7822" s="T662">v</ta>
            <ta e="T666" id="Seg_7823" s="T665">pers</ta>
            <ta e="T667" id="Seg_7824" s="T666">pers</ta>
            <ta e="T668" id="Seg_7825" s="T667">quant</ta>
            <ta e="T669" id="Seg_7826" s="T668">v</ta>
            <ta e="T670" id="Seg_7827" s="T669">v</ta>
            <ta e="T671" id="Seg_7828" s="T670">n</ta>
            <ta e="T672" id="Seg_7829" s="T671">v</ta>
            <ta e="T673" id="Seg_7830" s="T672">n</ta>
            <ta e="T674" id="Seg_7831" s="T673">v</ta>
            <ta e="T675" id="Seg_7832" s="T674">n</ta>
            <ta e="T676" id="Seg_7833" s="T675">v</ta>
            <ta e="T677" id="Seg_7834" s="T676">n</ta>
            <ta e="T678" id="Seg_7835" s="T677">v</ta>
            <ta e="T679" id="Seg_7836" s="T678">n</ta>
            <ta e="T680" id="Seg_7837" s="T679">v</ta>
            <ta e="T681" id="Seg_7838" s="T680">n</ta>
            <ta e="T682" id="Seg_7839" s="T681">v</ta>
            <ta e="T683" id="Seg_7840" s="T682">n</ta>
            <ta e="T684" id="Seg_7841" s="T683">v</ta>
            <ta e="T685" id="Seg_7842" s="T684">n</ta>
            <ta e="T686" id="Seg_7843" s="T685">v</ta>
            <ta e="T687" id="Seg_7844" s="T686">n</ta>
            <ta e="T688" id="Seg_7845" s="T687">v</ta>
            <ta e="T689" id="Seg_7846" s="T688">n</ta>
            <ta e="T690" id="Seg_7847" s="T689">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PKZ">
            <ta e="T5" id="Seg_7848" s="T4">np.h:A</ta>
            <ta e="T9" id="Seg_7849" s="T8">pro.h:A</ta>
            <ta e="T12" id="Seg_7850" s="T11">0.2.h:Th</ta>
            <ta e="T13" id="Seg_7851" s="T12">pro.h:A</ta>
            <ta e="T15" id="Seg_7852" s="T14">np:Th</ta>
            <ta e="T17" id="Seg_7853" s="T16">0.1.h:A</ta>
            <ta e="T18" id="Seg_7854" s="T17">0.2.h:A</ta>
            <ta e="T19" id="Seg_7855" s="T18">pro.h:R</ta>
            <ta e="T23" id="Seg_7856" s="T22">np.h:A</ta>
            <ta e="T29" id="Seg_7857" s="T28">0.3.h:P</ta>
            <ta e="T32" id="Seg_7858" s="T31">pro.h:P</ta>
            <ta e="T36" id="Seg_7859" s="T35">adv:Time</ta>
            <ta e="T43" id="Seg_7860" s="T42">adv:Time</ta>
            <ta e="T46" id="Seg_7861" s="T45">adv:Time</ta>
            <ta e="T52" id="Seg_7862" s="T51">0.2.h:A</ta>
            <ta e="T53" id="Seg_7863" s="T52">np.h:Th</ta>
            <ta e="T55" id="Seg_7864" s="T54">0.2.h:E</ta>
            <ta e="T56" id="Seg_7865" s="T55">np:L</ta>
            <ta e="T57" id="Seg_7866" s="T56">pro.h:A</ta>
            <ta e="T58" id="Seg_7867" s="T57">adv:So</ta>
            <ta e="T62" id="Seg_7868" s="T61">adv:Time</ta>
            <ta e="T63" id="Seg_7869" s="T62">0.1.h:A</ta>
            <ta e="T64" id="Seg_7870" s="T63">0.1.h:A</ta>
            <ta e="T65" id="Seg_7871" s="T64">pro:L</ta>
            <ta e="T67" id="Seg_7872" s="T66">0.1.h:A</ta>
            <ta e="T69" id="Seg_7873" s="T68">pro.h:Poss</ta>
            <ta e="T70" id="Seg_7874" s="T69">np:Th</ta>
            <ta e="T72" id="Seg_7875" s="T71">pro.h:Poss</ta>
            <ta e="T73" id="Seg_7876" s="T72">np:Th</ta>
            <ta e="T96" id="Seg_7877" s="T95">np.h:A</ta>
            <ta e="T98" id="Seg_7878" s="T97">np:Th 0.3.h:Poss</ta>
            <ta e="T103" id="Seg_7879" s="T102">0.2.h:A</ta>
            <ta e="T104" id="Seg_7880" s="T103">adv:L</ta>
            <ta e="T105" id="Seg_7881" s="T104">0.2.h:A</ta>
            <ta e="T107" id="Seg_7882" s="T106">0.2.h:A</ta>
            <ta e="T108" id="Seg_7883" s="T107">np:G</ta>
            <ta e="T111" id="Seg_7884" s="T110">pro.h:Th</ta>
            <ta e="T117" id="Seg_7885" s="T116">0.3.h:A</ta>
            <ta e="T125" id="Seg_7886" s="T124">0.2.h:A</ta>
            <ta e="T143" id="Seg_7887" s="T142">0.2.h:A</ta>
            <ta e="T144" id="Seg_7888" s="T143">0.2.h:A</ta>
            <ta e="T147" id="Seg_7889" s="T146">0.2.h:E</ta>
            <ta e="T148" id="Seg_7890" s="T147">0.2.h:A</ta>
            <ta e="T149" id="Seg_7891" s="T148">adv:L</ta>
            <ta e="T151" id="Seg_7892" s="T150">np.h:E</ta>
            <ta e="T160" id="Seg_7893" s="T159">0.1.h:A</ta>
            <ta e="T161" id="Seg_7894" s="T160">pro.h:A</ta>
            <ta e="T164" id="Seg_7895" s="T163">np:G</ta>
            <ta e="T167" id="Seg_7896" s="T166">0.3.h:A</ta>
            <ta e="T168" id="Seg_7897" s="T167">np:Ins</ta>
            <ta e="T169" id="Seg_7898" s="T168">np:P</ta>
            <ta e="T170" id="Seg_7899" s="T169">0.3.h:A</ta>
            <ta e="T171" id="Seg_7900" s="T170">np:Th</ta>
            <ta e="T172" id="Seg_7901" s="T171">0.3.h:A</ta>
            <ta e="T173" id="Seg_7902" s="T172">pro.h:R</ta>
            <ta e="T175" id="Seg_7903" s="T174">0.3.h:A</ta>
            <ta e="T178" id="Seg_7904" s="T177">np:L</ta>
            <ta e="T179" id="Seg_7905" s="T178">np:Ins</ta>
            <ta e="T180" id="Seg_7906" s="T179">np:P</ta>
            <ta e="T181" id="Seg_7907" s="T180">0.3.h:A</ta>
            <ta e="T183" id="Seg_7908" s="T182">np:G</ta>
            <ta e="T186" id="Seg_7909" s="T185">0.3.h:A</ta>
            <ta e="T187" id="Seg_7910" s="T186">adv:Time</ta>
            <ta e="T190" id="Seg_7911" s="T189">0.3.h:A</ta>
            <ta e="T191" id="Seg_7912" s="T190">adv:Time</ta>
            <ta e="T192" id="Seg_7913" s="T191">0.3.h:A</ta>
            <ta e="T195" id="Seg_7914" s="T194">0.3:P</ta>
            <ta e="T196" id="Seg_7915" s="T195">np:Th</ta>
            <ta e="T199" id="Seg_7916" s="T198">np:Th</ta>
            <ta e="T202" id="Seg_7917" s="T201">np:Th</ta>
            <ta e="T204" id="Seg_7918" s="T203">np:Th</ta>
            <ta e="T208" id="Seg_7919" s="T207">pro.h:Th</ta>
            <ta e="T209" id="Seg_7920" s="T208">np:Th</ta>
            <ta e="T211" id="Seg_7921" s="T210">pro:Th</ta>
            <ta e="T214" id="Seg_7922" s="T213">pro:A</ta>
            <ta e="T215" id="Seg_7923" s="T214">np:Th</ta>
            <ta e="T218" id="Seg_7924" s="T217">np:Th</ta>
            <ta e="T225" id="Seg_7925" s="T224">pro:P</ta>
            <ta e="T227" id="Seg_7926" s="T226">pro.h:A</ta>
            <ta e="T230" id="Seg_7927" s="T229">np:A</ta>
            <ta e="T234" id="Seg_7928" s="T233">pro.h:A</ta>
            <ta e="T241" id="Seg_7929" s="T240">0.1.h:E</ta>
            <ta e="T243" id="Seg_7930" s="T242">np.h:E</ta>
            <ta e="T246" id="Seg_7931" s="T245">np.h:E</ta>
            <ta e="T248" id="Seg_7932" s="T247">pro.h:P</ta>
            <ta e="T253" id="Seg_7933" s="T252">0.1.h:Th</ta>
            <ta e="T261" id="Seg_7934" s="T260">pro.h:P</ta>
            <ta e="T271" id="Seg_7935" s="T270">np:Th</ta>
            <ta e="T274" id="Seg_7936" s="T273">np:Th</ta>
            <ta e="T275" id="Seg_7937" s="T274">np:L</ta>
            <ta e="T279" id="Seg_7938" s="T278">np:Th</ta>
            <ta e="T281" id="Seg_7939" s="T280">np:E</ta>
            <ta e="T285" id="Seg_7940" s="T284">pro.h:Poss</ta>
            <ta e="T289" id="Seg_7941" s="T288">np:Th</ta>
            <ta e="T292" id="Seg_7942" s="T291">pro.h:A</ta>
            <ta e="T293" id="Seg_7943" s="T292">pro.h:Poss</ta>
            <ta e="T294" id="Seg_7944" s="T293">np:Th</ta>
            <ta e="T297" id="Seg_7945" s="T296">np:Th 0.2.h:Poss</ta>
            <ta e="T301" id="Seg_7946" s="T300">np:Th 0.2.h:Poss</ta>
            <ta e="T307" id="Seg_7947" s="T306">np:Th 0.2.h:Poss</ta>
            <ta e="T312" id="Seg_7948" s="T311">np:Th</ta>
            <ta e="T317" id="Seg_7949" s="T316">np:Th 0.2.h:Poss</ta>
            <ta e="T319" id="Seg_7950" s="T318">np:Th 0.2.h:Poss</ta>
            <ta e="T324" id="Seg_7951" s="T323">np:Th 0.2.h:Poss</ta>
            <ta e="T328" id="Seg_7952" s="T327">np:Th 0.2.h:Poss</ta>
            <ta e="T330" id="Seg_7953" s="T329">pro.h:Th</ta>
            <ta e="T333" id="Seg_7954" s="T332">np:A</ta>
            <ta e="T335" id="Seg_7955" s="T334">np:L</ta>
            <ta e="T336" id="Seg_7956" s="T335">0.3:A</ta>
            <ta e="T342" id="Seg_7957" s="T341">np:A</ta>
            <ta e="T344" id="Seg_7958" s="T343">np:A</ta>
            <ta e="T349" id="Seg_7959" s="T348">np:A</ta>
            <ta e="T354" id="Seg_7960" s="T353">np:A</ta>
            <ta e="T358" id="Seg_7961" s="T357">0.3:A</ta>
            <ta e="T359" id="Seg_7962" s="T358">0.1.h:E</ta>
            <ta e="T363" id="Seg_7963" s="T362">np:Th</ta>
            <ta e="T365" id="Seg_7964" s="T364">np:G</ta>
            <ta e="T367" id="Seg_7965" s="T366">np:Th</ta>
            <ta e="T371" id="Seg_7966" s="T370">pro.h:R</ta>
            <ta e="T373" id="Seg_7967" s="T372">np:P</ta>
            <ta e="T376" id="Seg_7968" s="T375">0.1.h:A</ta>
            <ta e="T377" id="Seg_7969" s="T376">0.1.h:A</ta>
            <ta e="T378" id="Seg_7970" s="T377">adv:Time</ta>
            <ta e="T381" id="Seg_7971" s="T380">np:P</ta>
            <ta e="T393" id="Seg_7972" s="T392">np:P</ta>
            <ta e="T398" id="Seg_7973" s="T397">np:L</ta>
            <ta e="T400" id="Seg_7974" s="T399">np:Th</ta>
            <ta e="T403" id="Seg_7975" s="T402">np:Ins</ta>
            <ta e="T405" id="Seg_7976" s="T404">np:P</ta>
            <ta e="T406" id="Seg_7977" s="T405">0.3.h:A</ta>
            <ta e="T408" id="Seg_7978" s="T407">np:P</ta>
            <ta e="T409" id="Seg_7979" s="T408">0.3.h:A</ta>
            <ta e="T420" id="Seg_7980" s="T419">pro:Th</ta>
            <ta e="T423" id="Seg_7981" s="T422">adv:Time</ta>
            <ta e="T425" id="Seg_7982" s="T424">adv:Time</ta>
            <ta e="T426" id="Seg_7983" s="T425">np:Th</ta>
            <ta e="T428" id="Seg_7984" s="T427">adv:Time</ta>
            <ta e="T430" id="Seg_7985" s="T429">np:Ins</ta>
            <ta e="T431" id="Seg_7986" s="T430">np:P</ta>
            <ta e="T435" id="Seg_7987" s="T434">np:L</ta>
            <ta e="T436" id="Seg_7988" s="T435">np:Th</ta>
            <ta e="T438" id="Seg_7989" s="T437">np:L</ta>
            <ta e="T440" id="Seg_7990" s="T439">np:P</ta>
            <ta e="T441" id="Seg_7991" s="T440">0.2.h:A</ta>
            <ta e="T442" id="Seg_7992" s="T441">np:P</ta>
            <ta e="T443" id="Seg_7993" s="T442">pro.h:A</ta>
            <ta e="T444" id="Seg_7994" s="T443">adv:L</ta>
            <ta e="T448" id="Seg_7995" s="T447">np:Th</ta>
            <ta e="T451" id="Seg_7996" s="T450">np:Th</ta>
            <ta e="T452" id="Seg_7997" s="T451">0.1.h:A</ta>
            <ta e="T457" id="Seg_7998" s="T456">np:Th</ta>
            <ta e="T458" id="Seg_7999" s="T457">0.1.h:A</ta>
            <ta e="T459" id="Seg_8000" s="T458">0.2.h:A</ta>
            <ta e="T462" id="Seg_8001" s="T461">pro.h:A</ta>
            <ta e="T463" id="Seg_8002" s="T462">adv:Time</ta>
            <ta e="T464" id="Seg_8003" s="T463">np:P</ta>
            <ta e="T469" id="Seg_8004" s="T468">0.2.h:Th</ta>
            <ta e="T474" id="Seg_8005" s="T472">adv:Time</ta>
            <ta e="T478" id="Seg_8006" s="T477">0.2.h:A</ta>
            <ta e="T480" id="Seg_8007" s="T479">np:P</ta>
            <ta e="T481" id="Seg_8008" s="T480">pro.h:A</ta>
            <ta e="T482" id="Seg_8009" s="T481">adv:L</ta>
            <ta e="T485" id="Seg_8010" s="T484">np:Th</ta>
            <ta e="T488" id="Seg_8011" s="T487">np:Th</ta>
            <ta e="T489" id="Seg_8012" s="T488">0.1.h:A</ta>
            <ta e="T493" id="Seg_8013" s="T492">0.3:P</ta>
            <ta e="T495" id="Seg_8014" s="T494">0.3:Th</ta>
            <ta e="T496" id="Seg_8015" s="T495">pro.h:B</ta>
            <ta e="T497" id="Seg_8016" s="T496">np:P</ta>
            <ta e="T498" id="Seg_8017" s="T497">0.2.h:A</ta>
            <ta e="T501" id="Seg_8018" s="T500">np:E</ta>
            <ta e="T503" id="Seg_8019" s="T502">np:E 0.2.h:Poss</ta>
            <ta e="T505" id="Seg_8020" s="T504">0.2.h:A</ta>
            <ta e="T507" id="Seg_8021" s="T506">0.2.h:P</ta>
            <ta e="T510" id="Seg_8022" s="T509">0.2.h:A</ta>
            <ta e="T513" id="Seg_8023" s="T512">0.2.h:E</ta>
            <ta e="T514" id="Seg_8024" s="T513">np:A</ta>
            <ta e="T520" id="Seg_8025" s="T519">adv:L</ta>
            <ta e="T521" id="Seg_8026" s="T520">np.h:E</ta>
            <ta e="T523" id="Seg_8027" s="T522">np:So</ta>
            <ta e="T525" id="Seg_8028" s="T524">np:A</ta>
            <ta e="T527" id="Seg_8029" s="T526">pro.h:A</ta>
            <ta e="T529" id="Seg_8030" s="T528">adv:L</ta>
            <ta e="T531" id="Seg_8031" s="T530">adv:L</ta>
            <ta e="T533" id="Seg_8032" s="T532">np.h:A</ta>
            <ta e="T534" id="Seg_8033" s="T533">np.h:A</ta>
            <ta e="T535" id="Seg_8034" s="T534">np.h:Th</ta>
            <ta e="T538" id="Seg_8035" s="T537">pro.h:R</ta>
            <ta e="T539" id="Seg_8036" s="T538">0.3.h:A</ta>
            <ta e="T540" id="Seg_8037" s="T539">0.2.h:A</ta>
            <ta e="T541" id="Seg_8038" s="T540">pro.h:Com</ta>
            <ta e="T544" id="Seg_8039" s="T543">0.2.h:E</ta>
            <ta e="T546" id="Seg_8040" s="T545">pro.h:Th</ta>
            <ta e="T547" id="Seg_8041" s="T546">adv:Time</ta>
            <ta e="T549" id="Seg_8042" s="T548">0.2.h:A</ta>
            <ta e="T550" id="Seg_8043" s="T549">adv:Time</ta>
            <ta e="T551" id="Seg_8044" s="T550">np.h:Th</ta>
            <ta e="T554" id="Seg_8045" s="T553">np.h:P 0.2.h:Poss</ta>
            <ta e="T556" id="Seg_8046" s="T555">pro.h:B</ta>
            <ta e="T557" id="Seg_8047" s="T556">0.3.h:A</ta>
            <ta e="T558" id="Seg_8048" s="T557">np.h:E</ta>
            <ta e="T563" id="Seg_8049" s="T562">0.2.h:A</ta>
            <ta e="T570" id="Seg_8050" s="T569">np.h:Th</ta>
            <ta e="T572" id="Seg_8051" s="T571">np.h:Th</ta>
            <ta e="T573" id="Seg_8052" s="T572">0.2.h:A</ta>
            <ta e="T578" id="Seg_8053" s="T577">adv:Time</ta>
            <ta e="T581" id="Seg_8054" s="T580">0.3.h:A</ta>
            <ta e="T582" id="Seg_8055" s="T581">np.h:A</ta>
            <ta e="T583" id="Seg_8056" s="T582">np.h:A</ta>
            <ta e="T586" id="Seg_8057" s="T585">np.h:A</ta>
            <ta e="T589" id="Seg_8058" s="T588">pro.h:A</ta>
            <ta e="T591" id="Seg_8059" s="T590">np:L</ta>
            <ta e="T594" id="Seg_8060" s="T593">np:P</ta>
            <ta e="T596" id="Seg_8061" s="T595">np.h:P</ta>
            <ta e="T599" id="Seg_8062" s="T598">pro.h:Th</ta>
            <ta e="T600" id="Seg_8063" s="T599">adv:L</ta>
            <ta e="T602" id="Seg_8064" s="T601">adv:Time</ta>
            <ta e="T606" id="Seg_8065" s="T605">adv:Time</ta>
            <ta e="T613" id="Seg_8066" s="T611">adv:Time</ta>
            <ta e="T614" id="Seg_8067" s="T613">adv:Time</ta>
            <ta e="T617" id="Seg_8068" s="T616">np:Ins</ta>
            <ta e="T620" id="Seg_8069" s="T619">pro.h:Th</ta>
            <ta e="T621" id="Seg_8070" s="T620">adv:Time</ta>
            <ta e="T622" id="Seg_8071" s="T621">np:G</ta>
            <ta e="T629" id="Seg_8072" s="T628">0.2.h:A</ta>
            <ta e="T631" id="Seg_8073" s="T630">np.h:E</ta>
            <ta e="T633" id="Seg_8074" s="T632">np:G</ta>
            <ta e="T634" id="Seg_8075" s="T633">0.3.h:A</ta>
            <ta e="T635" id="Seg_8076" s="T634">np.h:A</ta>
            <ta e="T638" id="Seg_8077" s="T637">pro.h:P</ta>
            <ta e="T646" id="Seg_8078" s="T645">0.3.h:E</ta>
            <ta e="T647" id="Seg_8079" s="T646">adv:Time</ta>
            <ta e="T649" id="Seg_8080" s="T648">0.3.h:E</ta>
            <ta e="T651" id="Seg_8081" s="T649">pro:Th</ta>
            <ta e="T654" id="Seg_8082" s="T653">pro.h:E</ta>
            <ta e="T656" id="Seg_8083" s="T655">0.3.h:A</ta>
            <ta e="T661" id="Seg_8084" s="T660">0.1.h:E</ta>
            <ta e="T663" id="Seg_8085" s="T662">0.3.h:A</ta>
            <ta e="T666" id="Seg_8086" s="T665">pro.h:E</ta>
            <ta e="T667" id="Seg_8087" s="T666">pro.h:E</ta>
            <ta e="T670" id="Seg_8088" s="T669">0.1.h:E</ta>
            <ta e="T671" id="Seg_8089" s="T670">np:E 0.1.h:Poss</ta>
            <ta e="T673" id="Seg_8090" s="T672">np:E 0.1.h:Poss</ta>
            <ta e="T675" id="Seg_8091" s="T674">np:E 0.1.h:Poss</ta>
            <ta e="T677" id="Seg_8092" s="T676">np:E 0.1.h:Poss</ta>
            <ta e="T679" id="Seg_8093" s="T678">np:E 0.1.h:Poss</ta>
            <ta e="T681" id="Seg_8094" s="T680">np:E 0.1.h:Poss</ta>
            <ta e="T683" id="Seg_8095" s="T682">np:E 0.1.h:Poss</ta>
            <ta e="T685" id="Seg_8096" s="T684">np:E 0.1.h:Poss</ta>
            <ta e="T687" id="Seg_8097" s="T686">np:E 0.1.h:Poss</ta>
            <ta e="T689" id="Seg_8098" s="T688">np:E</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-PKZ">
            <ta e="T5" id="Seg_8099" s="T4">np.h:S</ta>
            <ta e="T8" id="Seg_8100" s="T7">v:pred</ta>
            <ta e="T9" id="Seg_8101" s="T8">pro.h:S</ta>
            <ta e="T10" id="Seg_8102" s="T9">v:pred</ta>
            <ta e="T12" id="Seg_8103" s="T11">v:pred 0.2.h:S</ta>
            <ta e="T13" id="Seg_8104" s="T12">pro.h:S</ta>
            <ta e="T14" id="Seg_8105" s="T13">v:pred</ta>
            <ta e="T15" id="Seg_8106" s="T14">np:O</ta>
            <ta e="T17" id="Seg_8107" s="T16">v:pred 0.1.h:S</ta>
            <ta e="T18" id="Seg_8108" s="T17">v:pred 0.2.h:S</ta>
            <ta e="T20" id="Seg_8109" s="T19">np:O</ta>
            <ta e="T23" id="Seg_8110" s="T22">np.h:S</ta>
            <ta e="T25" id="Seg_8111" s="T24">v:pred</ta>
            <ta e="T27" id="Seg_8112" s="T26">adj:pred</ta>
            <ta e="T29" id="Seg_8113" s="T28">v:pred 0.3.h:S</ta>
            <ta e="T30" id="Seg_8114" s="T29">ptcl:pred</ta>
            <ta e="T32" id="Seg_8115" s="T31">pro.h:O</ta>
            <ta e="T38" id="Seg_8116" s="T37">ptcl:pred</ta>
            <ta e="T42" id="Seg_8117" s="T41">ptcl:pred</ta>
            <ta e="T45" id="Seg_8118" s="T44">v:pred</ta>
            <ta e="T51" id="Seg_8119" s="T50">ptcl:pred</ta>
            <ta e="T52" id="Seg_8120" s="T51">v:pred 0.2.h:S</ta>
            <ta e="T53" id="Seg_8121" s="T52">np.h:O</ta>
            <ta e="T55" id="Seg_8122" s="T54">v:pred 0.2.h:S</ta>
            <ta e="T57" id="Seg_8123" s="T56">pro.h:S</ta>
            <ta e="T59" id="Seg_8124" s="T58">v:pred</ta>
            <ta e="T60" id="Seg_8125" s="T59">s:purp</ta>
            <ta e="T61" id="Seg_8126" s="T60">s:purp</ta>
            <ta e="T63" id="Seg_8127" s="T62">v:pred 0.1.h:S</ta>
            <ta e="T64" id="Seg_8128" s="T63">v:pred 0.1.h:S</ta>
            <ta e="T67" id="Seg_8129" s="T66">v:pred 0.1.h:S</ta>
            <ta e="T70" id="Seg_8130" s="T69">np:S</ta>
            <ta e="T71" id="Seg_8131" s="T70">adj:pred</ta>
            <ta e="T96" id="Seg_8132" s="T95">np.h:S</ta>
            <ta e="T97" id="Seg_8133" s="T96">v:pred</ta>
            <ta e="T98" id="Seg_8134" s="T97">np:S</ta>
            <ta e="T100" id="Seg_8135" s="T99">adj:pred</ta>
            <ta e="T101" id="Seg_8136" s="T100">ptcl:pred</ta>
            <ta e="T103" id="Seg_8137" s="T102">v:pred 0.2.h:S</ta>
            <ta e="T105" id="Seg_8138" s="T104">v:pred 0.2.h:S</ta>
            <ta e="T107" id="Seg_8139" s="T106">v:pred 0.2.h:S</ta>
            <ta e="T117" id="Seg_8140" s="T116">v:pred 0.3.h:S</ta>
            <ta e="T125" id="Seg_8141" s="T124">v:pred 0.2.h:S</ta>
            <ta e="T143" id="Seg_8142" s="T142">v:pred 0.2.h:S</ta>
            <ta e="T144" id="Seg_8143" s="T143">v:pred 0.2.h:S</ta>
            <ta e="T147" id="Seg_8144" s="T146">v:pred 0.2.h:S</ta>
            <ta e="T148" id="Seg_8145" s="T147">v:pred 0.2.h:S</ta>
            <ta e="T151" id="Seg_8146" s="T150">np.h:S</ta>
            <ta e="T152" id="Seg_8147" s="T151">v:pred</ta>
            <ta e="T153" id="Seg_8148" s="T152">adj:pred</ta>
            <ta e="T160" id="Seg_8149" s="T159">v:pred 0.1.h:S</ta>
            <ta e="T161" id="Seg_8150" s="T160">pro.h:S</ta>
            <ta e="T162" id="Seg_8151" s="T161">ptcl.neg</ta>
            <ta e="T163" id="Seg_8152" s="T162">v:pred</ta>
            <ta e="T167" id="Seg_8153" s="T166">v:pred 0.3.h:S</ta>
            <ta e="T169" id="Seg_8154" s="T168">np:O</ta>
            <ta e="T170" id="Seg_8155" s="T169">v:pred 0.3.h:S</ta>
            <ta e="T171" id="Seg_8156" s="T170">np:O</ta>
            <ta e="T172" id="Seg_8157" s="T171">v:pred 0.3.h:S</ta>
            <ta e="T175" id="Seg_8158" s="T174">v:pred 0.3.h:S</ta>
            <ta e="T180" id="Seg_8159" s="T179">np:O</ta>
            <ta e="T181" id="Seg_8160" s="T180">v:pred 0.3.h:S</ta>
            <ta e="T186" id="Seg_8161" s="T185">v:pred 0.3.h:S</ta>
            <ta e="T190" id="Seg_8162" s="T189">v:pred 0.3.h:S</ta>
            <ta e="T192" id="Seg_8163" s="T191">v:pred 0.3.h:S</ta>
            <ta e="T194" id="Seg_8164" s="T193">adj:pred</ta>
            <ta e="T195" id="Seg_8165" s="T194">cop 0.3:S</ta>
            <ta e="T196" id="Seg_8166" s="T195">np:S</ta>
            <ta e="T197" id="Seg_8167" s="T196">conv:pred</ta>
            <ta e="T198" id="Seg_8168" s="T197">v:pred</ta>
            <ta e="T199" id="Seg_8169" s="T198">np:S</ta>
            <ta e="T201" id="Seg_8170" s="T200">v:pred</ta>
            <ta e="T202" id="Seg_8171" s="T201">np:S</ta>
            <ta e="T203" id="Seg_8172" s="T202">v:pred</ta>
            <ta e="T204" id="Seg_8173" s="T203">np:S</ta>
            <ta e="T207" id="Seg_8174" s="T206">adj:pred</ta>
            <ta e="T208" id="Seg_8175" s="T207">pro.h:S</ta>
            <ta e="T209" id="Seg_8176" s="T208">np:O</ta>
            <ta e="T211" id="Seg_8177" s="T210">pro:S</ta>
            <ta e="T213" id="Seg_8178" s="T212">v:pred</ta>
            <ta e="T214" id="Seg_8179" s="T213">pro:S</ta>
            <ta e="T215" id="Seg_8180" s="T214">np:O</ta>
            <ta e="T216" id="Seg_8181" s="T215">v:pred</ta>
            <ta e="T218" id="Seg_8182" s="T217">np:S</ta>
            <ta e="T219" id="Seg_8183" s="T218">v:pred</ta>
            <ta e="T224" id="Seg_8184" s="T223">ptcl:pred</ta>
            <ta e="T225" id="Seg_8185" s="T224">pro:O</ta>
            <ta e="T227" id="Seg_8186" s="T226">pro.h:S</ta>
            <ta e="T228" id="Seg_8187" s="T227">v:pred</ta>
            <ta e="T230" id="Seg_8188" s="T229">np:S</ta>
            <ta e="T231" id="Seg_8189" s="T230">ptcl.neg</ta>
            <ta e="T232" id="Seg_8190" s="T231">v:pred</ta>
            <ta e="T234" id="Seg_8191" s="T233">pro.h:S</ta>
            <ta e="T235" id="Seg_8192" s="T234">v:pred</ta>
            <ta e="T240" id="Seg_8193" s="T239">ptcl.neg</ta>
            <ta e="T241" id="Seg_8194" s="T240">v:pred 0.1.h:S</ta>
            <ta e="T243" id="Seg_8195" s="T242">np.h:S</ta>
            <ta e="T244" id="Seg_8196" s="T243">v:pred</ta>
            <ta e="T246" id="Seg_8197" s="T245">np.h:S</ta>
            <ta e="T247" id="Seg_8198" s="T246">v:pred</ta>
            <ta e="T248" id="Seg_8199" s="T247">pro.h:S</ta>
            <ta e="T250" id="Seg_8200" s="T249">adj:pred</ta>
            <ta e="T251" id="Seg_8201" s="T250">cop</ta>
            <ta e="T253" id="Seg_8202" s="T252">v:pred 0.1.h:S</ta>
            <ta e="T261" id="Seg_8203" s="T260">pro.h:S</ta>
            <ta e="T263" id="Seg_8204" s="T262">v:pred</ta>
            <ta e="T271" id="Seg_8205" s="T270">np:S</ta>
            <ta e="T272" id="Seg_8206" s="T271">v:pred</ta>
            <ta e="T274" id="Seg_8207" s="T273">np:S</ta>
            <ta e="T278" id="Seg_8208" s="T277">v:pred</ta>
            <ta e="T279" id="Seg_8209" s="T278">np:S</ta>
            <ta e="T280" id="Seg_8210" s="T279">v:pred</ta>
            <ta e="T281" id="Seg_8211" s="T280">np:S</ta>
            <ta e="T282" id="Seg_8212" s="T281">v:pred</ta>
            <ta e="T283" id="Seg_8213" s="T282">ptcl:pred</ta>
            <ta e="T289" id="Seg_8214" s="T288">np:S</ta>
            <ta e="T290" id="Seg_8215" s="T289">adj:pred</ta>
            <ta e="T291" id="Seg_8216" s="T290">v:pred</ta>
            <ta e="T292" id="Seg_8217" s="T291">pro.h:S</ta>
            <ta e="T294" id="Seg_8218" s="T293">np:S</ta>
            <ta e="T296" id="Seg_8219" s="T295">adj:pred</ta>
            <ta e="T297" id="Seg_8220" s="T296">np:S</ta>
            <ta e="T300" id="Seg_8221" s="T299">adj:pred</ta>
            <ta e="T301" id="Seg_8222" s="T300">np:S</ta>
            <ta e="T303" id="Seg_8223" s="T302">v:pred</ta>
            <ta e="T307" id="Seg_8224" s="T306">np:S</ta>
            <ta e="T308" id="Seg_8225" s="T307">adj:pred</ta>
            <ta e="T312" id="Seg_8226" s="T311">np:S</ta>
            <ta e="T313" id="Seg_8227" s="T312">adj:pred</ta>
            <ta e="T316" id="Seg_8228" s="T315">adj:pred</ta>
            <ta e="T317" id="Seg_8229" s="T316">np:S</ta>
            <ta e="T318" id="Seg_8230" s="T317">adj:pred</ta>
            <ta e="T319" id="Seg_8231" s="T318">np:S</ta>
            <ta e="T321" id="Seg_8232" s="T320">adj:pred</ta>
            <ta e="T324" id="Seg_8233" s="T323">np:S</ta>
            <ta e="T325" id="Seg_8234" s="T324">adj:pred</ta>
            <ta e="T328" id="Seg_8235" s="T327">np:S</ta>
            <ta e="T329" id="Seg_8236" s="T328">adj:pred</ta>
            <ta e="T330" id="Seg_8237" s="T329">pro.h:S</ta>
            <ta e="T332" id="Seg_8238" s="T331">adj:pred</ta>
            <ta e="T333" id="Seg_8239" s="T332">np:S</ta>
            <ta e="T334" id="Seg_8240" s="T333">v:pred</ta>
            <ta e="T336" id="Seg_8241" s="T335">v:pred 0.3:S</ta>
            <ta e="T342" id="Seg_8242" s="T341">np:S</ta>
            <ta e="T343" id="Seg_8243" s="T342">v:pred</ta>
            <ta e="T344" id="Seg_8244" s="T343">np:S</ta>
            <ta e="T348" id="Seg_8245" s="T347">v:pred</ta>
            <ta e="T349" id="Seg_8246" s="T348">np:S</ta>
            <ta e="T350" id="Seg_8247" s="T349">conv:pred</ta>
            <ta e="T353" id="Seg_8248" s="T352">v:pred</ta>
            <ta e="T354" id="Seg_8249" s="T353">np:S</ta>
            <ta e="T356" id="Seg_8250" s="T355">v:pred</ta>
            <ta e="T357" id="Seg_8251" s="T356">s:purp</ta>
            <ta e="T358" id="Seg_8252" s="T357">v:pred 0.3:S</ta>
            <ta e="T359" id="Seg_8253" s="T358">v:pred 0.1.h:S</ta>
            <ta e="T363" id="Seg_8254" s="T362">np:O</ta>
            <ta e="T364" id="Seg_8255" s="T363">v:pred</ta>
            <ta e="T367" id="Seg_8256" s="T366">np:S</ta>
            <ta e="T368" id="Seg_8257" s="T367">v:pred</ta>
            <ta e="T372" id="Seg_8258" s="T371">ptcl:pred</ta>
            <ta e="T373" id="Seg_8259" s="T372">np:O</ta>
            <ta e="T376" id="Seg_8260" s="T375">v:pred 0.1.h:S</ta>
            <ta e="T377" id="Seg_8261" s="T376">v:pred 0.1.h:S</ta>
            <ta e="T380" id="Seg_8262" s="T379">ptcl:pred</ta>
            <ta e="T381" id="Seg_8263" s="T380">np:O</ta>
            <ta e="T393" id="Seg_8264" s="T392">np:S</ta>
            <ta e="T394" id="Seg_8265" s="T393">v:pred</ta>
            <ta e="T400" id="Seg_8266" s="T399">np:S</ta>
            <ta e="T401" id="Seg_8267" s="T400">v:pred</ta>
            <ta e="T405" id="Seg_8268" s="T404">np:O</ta>
            <ta e="T406" id="Seg_8269" s="T405">v:pred 0.3.h:S</ta>
            <ta e="T408" id="Seg_8270" s="T407">np:O</ta>
            <ta e="T409" id="Seg_8271" s="T408">v:pred 0.3.h:S</ta>
            <ta e="T421" id="Seg_8272" s="T420">ptcl:pred</ta>
            <ta e="T424" id="Seg_8273" s="T423">v:pred</ta>
            <ta e="T426" id="Seg_8274" s="T425">np:O</ta>
            <ta e="T427" id="Seg_8275" s="T426">v:pred</ta>
            <ta e="T431" id="Seg_8276" s="T430">np:O</ta>
            <ta e="T433" id="Seg_8277" s="T432">ptcl:pred</ta>
            <ta e="T434" id="Seg_8278" s="T433">v:pred</ta>
            <ta e="T436" id="Seg_8279" s="T435">np:O</ta>
            <ta e="T437" id="Seg_8280" s="T436">v:pred</ta>
            <ta e="T439" id="Seg_8281" s="T438">v:pred</ta>
            <ta e="T440" id="Seg_8282" s="T439">np:O</ta>
            <ta e="T441" id="Seg_8283" s="T440">v:pred 0.2.h:S</ta>
            <ta e="T442" id="Seg_8284" s="T441">np:O</ta>
            <ta e="T443" id="Seg_8285" s="T442">pro.h:S</ta>
            <ta e="T447" id="Seg_8286" s="T446">v:pred</ta>
            <ta e="T448" id="Seg_8287" s="T447">np:O</ta>
            <ta e="T451" id="Seg_8288" s="T450">np:O</ta>
            <ta e="T452" id="Seg_8289" s="T451">v:pred 0.1.h:S</ta>
            <ta e="T455" id="Seg_8290" s="T454">adj:pred</ta>
            <ta e="T457" id="Seg_8291" s="T456">np:O</ta>
            <ta e="T458" id="Seg_8292" s="T457">v:pred 0.1.h:S</ta>
            <ta e="T459" id="Seg_8293" s="T458">v:pred 0.2.h:S</ta>
            <ta e="T462" id="Seg_8294" s="T461">pro.h:S</ta>
            <ta e="T464" id="Seg_8295" s="T463">np:O</ta>
            <ta e="T465" id="Seg_8296" s="T464">v:pred</ta>
            <ta e="T468" id="Seg_8297" s="T467">adj:pred</ta>
            <ta e="T469" id="Seg_8298" s="T468">cop 0.2.h:S</ta>
            <ta e="T478" id="Seg_8299" s="T477">v:pred 0.2.h:S</ta>
            <ta e="T480" id="Seg_8300" s="T479">np:O</ta>
            <ta e="T481" id="Seg_8301" s="T480">pro.h:S</ta>
            <ta e="T485" id="Seg_8302" s="T484">np:O</ta>
            <ta e="T486" id="Seg_8303" s="T485">v:pred</ta>
            <ta e="T488" id="Seg_8304" s="T487">np:O</ta>
            <ta e="T489" id="Seg_8305" s="T488">v:pred 0.1.h:S</ta>
            <ta e="T492" id="Seg_8306" s="T491">adj:pred</ta>
            <ta e="T493" id="Seg_8307" s="T492">cop 0.3:S</ta>
            <ta e="T494" id="Seg_8308" s="T493">adj:pred</ta>
            <ta e="T495" id="Seg_8309" s="T494">cop 0.3:S</ta>
            <ta e="T497" id="Seg_8310" s="T496">np:O</ta>
            <ta e="T498" id="Seg_8311" s="T497">v:pred 0.2.h:S</ta>
            <ta e="T501" id="Seg_8312" s="T500">np:S</ta>
            <ta e="T502" id="Seg_8313" s="T501">v:pred</ta>
            <ta e="T503" id="Seg_8314" s="T502">np:S</ta>
            <ta e="T504" id="Seg_8315" s="T503">v:pred</ta>
            <ta e="T505" id="Seg_8316" s="T504">v:pred 0.2.h:S</ta>
            <ta e="T507" id="Seg_8317" s="T506">v:pred 0.2.h:S</ta>
            <ta e="T509" id="Seg_8318" s="T508">ptcl.neg</ta>
            <ta e="T510" id="Seg_8319" s="T509">v:pred 0.2.h:S</ta>
            <ta e="T513" id="Seg_8320" s="T512">v:pred 0.2.h:S</ta>
            <ta e="T514" id="Seg_8321" s="T513">np:S</ta>
            <ta e="T519" id="Seg_8322" s="T518">v:pred</ta>
            <ta e="T521" id="Seg_8323" s="T520">np.h:S</ta>
            <ta e="T522" id="Seg_8324" s="T521">v:pred</ta>
            <ta e="T525" id="Seg_8325" s="T524">np:S</ta>
            <ta e="T526" id="Seg_8326" s="T525">v:pred</ta>
            <ta e="T527" id="Seg_8327" s="T526">pro.h:S</ta>
            <ta e="T532" id="Seg_8328" s="T531">v:pred</ta>
            <ta e="T533" id="Seg_8329" s="T532">np.h:S</ta>
            <ta e="T534" id="Seg_8330" s="T533">np.h:S</ta>
            <ta e="T535" id="Seg_8331" s="T534">np.h:O</ta>
            <ta e="T536" id="Seg_8332" s="T535">v:pred</ta>
            <ta e="T539" id="Seg_8333" s="T538">v:pred 0.3.h:S</ta>
            <ta e="T540" id="Seg_8334" s="T539">v:pred 0.2.h:S</ta>
            <ta e="T544" id="Seg_8335" s="T543">v:pred 0.2.h:S</ta>
            <ta e="T546" id="Seg_8336" s="T545">pro.h:S</ta>
            <ta e="T549" id="Seg_8337" s="T548">v:pred 0.2.h:S</ta>
            <ta e="T551" id="Seg_8338" s="T550">np.h:S</ta>
            <ta e="T552" id="Seg_8339" s="T551">v:pred</ta>
            <ta e="T554" id="Seg_8340" s="T553">np.h:S</ta>
            <ta e="T555" id="Seg_8341" s="T554">v:pred</ta>
            <ta e="T557" id="Seg_8342" s="T556">v:pred 0.3.h:S</ta>
            <ta e="T558" id="Seg_8343" s="T557">np.h:O</ta>
            <ta e="T563" id="Seg_8344" s="T562">v:pred 0.2.h:S</ta>
            <ta e="T570" id="Seg_8345" s="T569">np.h:O</ta>
            <ta e="T572" id="Seg_8346" s="T571">np.h:O</ta>
            <ta e="T573" id="Seg_8347" s="T572">v:pred 0.2.h:S</ta>
            <ta e="T580" id="Seg_8348" s="T579">conv:pred</ta>
            <ta e="T581" id="Seg_8349" s="T580">v:pred 0.3.h:S</ta>
            <ta e="T582" id="Seg_8350" s="T581">np.h:S</ta>
            <ta e="T583" id="Seg_8351" s="T582">np.h:S</ta>
            <ta e="T584" id="Seg_8352" s="T583">conv:pred</ta>
            <ta e="T585" id="Seg_8353" s="T584">v:pred</ta>
            <ta e="T586" id="Seg_8354" s="T585">np.h:S</ta>
            <ta e="T587" id="Seg_8355" s="T586">conv:pred</ta>
            <ta e="T588" id="Seg_8356" s="T587">v:pred</ta>
            <ta e="T589" id="Seg_8357" s="T588">pro.h:S</ta>
            <ta e="T592" id="Seg_8358" s="T591">v:pred</ta>
            <ta e="T593" id="Seg_8359" s="T592">ptcl:pred</ta>
            <ta e="T594" id="Seg_8360" s="T593">np:O</ta>
            <ta e="T596" id="Seg_8361" s="T595">np.h:S</ta>
            <ta e="T597" id="Seg_8362" s="T596">v:pred</ta>
            <ta e="T598" id="Seg_8363" s="T597">ptcl:pred</ta>
            <ta e="T599" id="Seg_8364" s="T598">pro.h:O</ta>
            <ta e="T618" id="Seg_8365" s="T617">ptcl:pred</ta>
            <ta e="T620" id="Seg_8366" s="T619">pro.h:O</ta>
            <ta e="T625" id="Seg_8367" s="T624">v:pred</ta>
            <ta e="T626" id="Seg_8368" s="T625">v:pred</ta>
            <ta e="T629" id="Seg_8369" s="T628">v:pred 0.2.h:S</ta>
            <ta e="T630" id="Seg_8370" s="T629">s:purp</ta>
            <ta e="T631" id="Seg_8371" s="T630">np.h:S</ta>
            <ta e="T632" id="Seg_8372" s="T631">v:pred</ta>
            <ta e="T634" id="Seg_8373" s="T633">v:pred 0.3.h:S</ta>
            <ta e="T635" id="Seg_8374" s="T634">np.h:S</ta>
            <ta e="T636" id="Seg_8375" s="T635">ptcl.neg</ta>
            <ta e="T637" id="Seg_8376" s="T636">v:pred</ta>
            <ta e="T638" id="Seg_8377" s="T637">pro.h:O</ta>
            <ta e="T643" id="Seg_8378" s="T641">v:pred</ta>
            <ta e="T645" id="Seg_8379" s="T644">ptcl.neg</ta>
            <ta e="T646" id="Seg_8380" s="T645">v:pred 0.3.h:S</ta>
            <ta e="T649" id="Seg_8381" s="T648">v:pred 0.3.h:S</ta>
            <ta e="T651" id="Seg_8382" s="T649">pro:O</ta>
            <ta e="T652" id="Seg_8383" s="T651">ptcl.neg</ta>
            <ta e="T653" id="Seg_8384" s="T652">v:pred</ta>
            <ta e="T654" id="Seg_8385" s="T653">pro.h:O</ta>
            <ta e="T656" id="Seg_8386" s="T655">v:pred 0.3.h:S</ta>
            <ta e="T660" id="Seg_8387" s="T659">ptcl.neg</ta>
            <ta e="T661" id="Seg_8388" s="T660">v:pred 0.1.h:S</ta>
            <ta e="T663" id="Seg_8389" s="T662">v:pred 0.3.h:S</ta>
            <ta e="T666" id="Seg_8390" s="T665">pro.h:S</ta>
            <ta e="T667" id="Seg_8391" s="T666">pro.h:S</ta>
            <ta e="T669" id="Seg_8392" s="T668">v:pred</ta>
            <ta e="T670" id="Seg_8393" s="T669">v:pred 0.1.h:S</ta>
            <ta e="T671" id="Seg_8394" s="T670">np:S</ta>
            <ta e="T672" id="Seg_8395" s="T671">v:pred</ta>
            <ta e="T673" id="Seg_8396" s="T672">np:S</ta>
            <ta e="T674" id="Seg_8397" s="T673">v:pred</ta>
            <ta e="T675" id="Seg_8398" s="T674">np:S</ta>
            <ta e="T676" id="Seg_8399" s="T675">v:pred</ta>
            <ta e="T677" id="Seg_8400" s="T676">np:S</ta>
            <ta e="T678" id="Seg_8401" s="T677">v:pred</ta>
            <ta e="T679" id="Seg_8402" s="T678">np:S</ta>
            <ta e="T680" id="Seg_8403" s="T679">v:pred</ta>
            <ta e="T681" id="Seg_8404" s="T680">np:S</ta>
            <ta e="T682" id="Seg_8405" s="T681">v:pred</ta>
            <ta e="T683" id="Seg_8406" s="T682">np:S</ta>
            <ta e="T684" id="Seg_8407" s="T683">v:pred</ta>
            <ta e="T685" id="Seg_8408" s="T684">np:S</ta>
            <ta e="T686" id="Seg_8409" s="T685">v:pred</ta>
            <ta e="T687" id="Seg_8410" s="T686">np:S</ta>
            <ta e="T688" id="Seg_8411" s="T687">v:pred</ta>
            <ta e="T689" id="Seg_8412" s="T688">np:S</ta>
            <ta e="T690" id="Seg_8413" s="T689">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST-PKZ" />
         <annotation name="BOR" tierref="BOR-PKZ">
            <ta e="T15" id="Seg_8414" s="T14">TURK:cult</ta>
            <ta e="T20" id="Seg_8415" s="T19">TURK:cult</ta>
            <ta e="T24" id="Seg_8416" s="T23">TURK:disc</ta>
            <ta e="T28" id="Seg_8417" s="T27">TURK:disc</ta>
            <ta e="T30" id="Seg_8418" s="T29">RUS:mod</ta>
            <ta e="T38" id="Seg_8419" s="T37">RUS:mod</ta>
            <ta e="T42" id="Seg_8420" s="T41">RUS:mod</ta>
            <ta e="T51" id="Seg_8421" s="T50">RUS:mod</ta>
            <ta e="T54" id="Seg_8422" s="T53">TURK:disc</ta>
            <ta e="T56" id="Seg_8423" s="T55">TAT:cult</ta>
            <ta e="T66" id="Seg_8424" s="T65">RUS:gram</ta>
            <ta e="T67" id="Seg_8425" s="T66">%TURK:core</ta>
            <ta e="T99" id="Seg_8426" s="T98">TURK:disc</ta>
            <ta e="T101" id="Seg_8427" s="T100">RUS:mod</ta>
            <ta e="T112" id="Seg_8428" s="T111">RUS:mod</ta>
            <ta e="T118" id="Seg_8429" s="T117">TURK:disc</ta>
            <ta e="T119" id="Seg_8430" s="T118">RUS:gram</ta>
            <ta e="T123" id="Seg_8431" s="T122">TURK:disc</ta>
            <ta e="T146" id="Seg_8432" s="T145">RUS:gram</ta>
            <ta e="T154" id="Seg_8433" s="T153">TURK:disc</ta>
            <ta e="T174" id="Seg_8434" s="T173">RUS:mod</ta>
            <ta e="T192" id="Seg_8435" s="T191">TURK:cult</ta>
            <ta e="T205" id="Seg_8436" s="T204">TURK:disc</ta>
            <ta e="T217" id="Seg_8437" s="T216">RUS:mod</ta>
            <ta e="T224" id="Seg_8438" s="T223">RUS:mod</ta>
            <ta e="T230" id="Seg_8439" s="T229">RUS:cult</ta>
            <ta e="T236" id="Seg_8440" s="T235">RUS:gram</ta>
            <ta e="T255" id="Seg_8441" s="T254">RUS:gram</ta>
            <ta e="T260" id="Seg_8442" s="T259">RUS:gram</ta>
            <ta e="T262" id="Seg_8443" s="T261">TURK:disc</ta>
            <ta e="T264" id="Seg_8444" s="T263">TURK:disc</ta>
            <ta e="T273" id="Seg_8445" s="T272">TURK:disc</ta>
            <ta e="T283" id="Seg_8446" s="T282">RUS:mod</ta>
            <ta e="T302" id="Seg_8447" s="T301">TURK:disc</ta>
            <ta e="T331" id="Seg_8448" s="T330">TURK:disc</ta>
            <ta e="T345" id="Seg_8449" s="T344">TURK:disc</ta>
            <ta e="T354" id="Seg_8450" s="T353">RUS:cult</ta>
            <ta e="T355" id="Seg_8451" s="T354">TURK:disc</ta>
            <ta e="T360" id="Seg_8452" s="T359">TURK:disc</ta>
            <ta e="T365" id="Seg_8453" s="T364">RUS:cult</ta>
            <ta e="T373" id="Seg_8454" s="T372">TURK:cult</ta>
            <ta e="T380" id="Seg_8455" s="T379">RUS:mod</ta>
            <ta e="T392" id="Seg_8456" s="T391">TURK:disc</ta>
            <ta e="T393" id="Seg_8457" s="T392">RUS:cult</ta>
            <ta e="T399" id="Seg_8458" s="T398">TURK:disc</ta>
            <ta e="T400" id="Seg_8459" s="T399">RUS:cult</ta>
            <ta e="T404" id="Seg_8460" s="T403">TURK:disc</ta>
            <ta e="T421" id="Seg_8461" s="T420">RUS:mod</ta>
            <ta e="T426" id="Seg_8462" s="T425">TAT:cult</ta>
            <ta e="T431" id="Seg_8463" s="T430">TAT:cult</ta>
            <ta e="T433" id="Seg_8464" s="T432">RUS:mod</ta>
            <ta e="T435" id="Seg_8465" s="T434">TAT:cult</ta>
            <ta e="T436" id="Seg_8466" s="T435">RUS:cult</ta>
            <ta e="T438" id="Seg_8467" s="T437">RUS:cult</ta>
            <ta e="T456" id="Seg_8468" s="T455">TURK:disc</ta>
            <ta e="T461" id="Seg_8469" s="T460">TURK:core</ta>
            <ta e="T464" id="Seg_8470" s="T463">TURK:cult</ta>
            <ta e="T470" id="Seg_8471" s="T469">TURK:disc</ta>
            <ta e="T475" id="Seg_8472" s="T474">TURK:disc</ta>
            <ta e="T494" id="Seg_8473" s="T493">TURK:core</ta>
            <ta e="T497" id="Seg_8474" s="T496">TURK:cult</ta>
            <ta e="T500" id="Seg_8475" s="T499">RUS:gram</ta>
            <ta e="T506" id="Seg_8476" s="T505">RUS:gram</ta>
            <ta e="T508" id="Seg_8477" s="T507">RUS:gram</ta>
            <ta e="T511" id="Seg_8478" s="T510">RUS:gram</ta>
            <ta e="T514" id="Seg_8479" s="T513">RUS:cult</ta>
            <ta e="T523" id="Seg_8480" s="T522">RUS:cult</ta>
            <ta e="T524" id="Seg_8481" s="T523">TURK:disc</ta>
            <ta e="T528" id="Seg_8482" s="T527">TURK:disc</ta>
            <ta e="T571" id="Seg_8483" s="T570">RUS:gram</ta>
            <ta e="T579" id="Seg_8484" s="T578">TURK:disc</ta>
            <ta e="T593" id="Seg_8485" s="T592">RUS:mod</ta>
            <ta e="T598" id="Seg_8486" s="T597">RUS:mod</ta>
            <ta e="T618" id="Seg_8487" s="T617">RUS:mod</ta>
            <ta e="T633" id="Seg_8488" s="T632">RUS:cult</ta>
            <ta e="T635" id="Seg_8489" s="T634">RUS:cult</ta>
            <ta e="T648" id="Seg_8490" s="T647">TURK:disc</ta>
            <ta e="T651" id="Seg_8491" s="T649">TURK:gram(INDEF)</ta>
            <ta e="T655" id="Seg_8492" s="T654">TURK:disc</ta>
            <ta e="T668" id="Seg_8493" s="T667">TURK:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-PKZ" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-PKZ" />
         <annotation name="CS" tierref="CS-PKZ">
            <ta e="T140" id="Seg_8494" s="T139">RUS:ext</ta>
            <ta e="T269" id="Seg_8495" s="T264">RUS:ext</ta>
            <ta e="T310" id="Seg_8496" s="T309">RUS:ext</ta>
            <ta e="T477" id="Seg_8497" s="T475">RUS:int</ta>
            <ta e="T553" id="Seg_8498" s="T552">RUS:ext</ta>
            <ta e="T577" id="Seg_8499" s="T573">RUS:ext</ta>
            <ta e="T641" id="Seg_8500" s="T639">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr-PKZ">
            <ta e="T8" id="Seg_8501" s="T4">Женщин очень много пришло.</ta>
            <ta e="T12" id="Seg_8502" s="T8">Я сказала: где вы были?</ta>
            <ta e="T17" id="Seg_8503" s="T12">Они говорят: мы ходили доить коров.</ta>
            <ta e="T20" id="Seg_8504" s="T17">Принесите мне молока!</ta>
            <ta e="T22" id="Seg_8505" s="T20">Хватит, больше не надо (?)</ta>
            <ta e="T27" id="Seg_8506" s="T22">Ребенок покакал, очень грязный.</ta>
            <ta e="T29" id="Seg_8507" s="T27">Все высохло.</ta>
            <ta e="T32" id="Seg_8508" s="T29">Надо его помыть.</ta>
            <ta e="T38" id="Seg_8509" s="T32">Потереть, потом помыть надо.</ta>
            <ta e="T42" id="Seg_8510" s="T38">Снять [грязь] надо. [?]</ta>
            <ta e="T45" id="Seg_8511" s="T42">Потом надо покормить.</ta>
            <ta e="T51" id="Seg_8512" s="T45">Потом положить спать надо.</ta>
            <ta e="T53" id="Seg_8513" s="T51">Укачай ребенка!</ta>
            <ta e="T54" id="Seg_8514" s="T53">Все!</ta>
            <ta e="T56" id="Seg_8515" s="T54">Сиди дома!</ta>
            <ta e="T59" id="Seg_8516" s="T56">Я выйду на улицу.</ta>
            <ta e="T60" id="Seg_8517" s="T59">Пописать.</ta>
            <ta e="T61" id="Seg_8518" s="T60">Облегчиться.</ta>
            <ta e="T63" id="Seg_8519" s="T61">Потом я приду.</ta>
            <ta e="T65" id="Seg_8520" s="T63">Посижу с тобой.</ta>
            <ta e="T67" id="Seg_8521" s="T65">И мы будет разговаривать!</ta>
            <ta e="T68" id="Seg_8522" s="T67">Хватит!</ta>
            <ta e="T73" id="Seg_8523" s="T68">Мой зад красивее, чем твое лицо. [?]</ta>
            <ta e="T78" id="Seg_8524" s="T73">Очень толстый зад.</ta>
            <ta e="T81" id="Seg_8525" s="T78">Очень грязный зад.</ta>
            <ta e="T84" id="Seg_8526" s="T81">Очень красный зад.</ta>
            <ta e="T87" id="Seg_8527" s="T84">Очень белый зад.</ta>
            <ta e="T90" id="Seg_8528" s="T87">Очень толстый зад.</ta>
            <ta e="T94" id="Seg_8529" s="T90">Очень худой зад.</ta>
            <ta e="T95" id="Seg_8530" s="T94">Хватит!</ta>
            <ta e="T97" id="Seg_8531" s="T95">Ребенок приходил.</ta>
            <ta e="T100" id="Seg_8532" s="T97">У него руки все грязные.</ta>
            <ta e="T102" id="Seg_8533" s="T100">Надо помыть.</ta>
            <ta e="T104" id="Seg_8534" s="T102">Иди сюда!</ta>
            <ta e="T106" id="Seg_8535" s="T104">Не плачь!</ta>
            <ta e="T108" id="Seg_8536" s="T106">Иди домой!</ta>
            <ta e="T118" id="Seg_8537" s="T108">Тебя, наверное, ищут.</ta>
            <ta e="T122" id="Seg_8538" s="T118">А то птицы (?) тебя.</ta>
            <ta e="T123" id="Seg_8539" s="T122">Все</ta>
            <ta e="T125" id="Seg_8540" s="T123">Куда ты (лезешь?)</ta>
            <ta e="T143" id="Seg_8541" s="T140">Куда ты лезешь?</ta>
            <ta e="T147" id="Seg_8542" s="T143">Не лезь, а то упадешь!</ta>
            <ta e="T149" id="Seg_8543" s="T147">Иди сюда!</ta>
            <ta e="T154" id="Seg_8544" s="T149">Девушка сидит, худая.</ta>
            <ta e="T163" id="Seg_8545" s="T154">Что я говорю, она не слушает.</ta>
            <ta e="T168" id="Seg_8546" s="T163">Они едут в лес на лошади.</ta>
            <ta e="T170" id="Seg_8547" s="T168">Рыбу будут ловить.</ta>
            <ta e="T172" id="Seg_8548" s="T170">Рыбу принесут.</ta>
            <ta e="T175" id="Seg_8549" s="T172">Мне, может, дадут.</ta>
            <ta e="T176" id="Seg_8550" s="T175">Хватит!</ta>
            <ta e="T181" id="Seg_8551" s="T177">В реке руками рыбу ловили.</ta>
            <ta e="T186" id="Seg_8552" s="T181">На землю клали.</ta>
            <ta e="T190" id="Seg_8553" s="T186">Потом солили.</ta>
            <ta e="T192" id="Seg_8554" s="T190">Потом продавали.</ta>
            <ta e="T193" id="Seg_8555" s="T192">Хватит!</ta>
            <ta e="T195" id="Seg_8556" s="T193">Холодно стало.</ta>
            <ta e="T198" id="Seg_8557" s="T195">Тепло ушло.</ta>
            <ta e="T201" id="Seg_8558" s="T198">Солнце слабо светит.</ta>
            <ta e="T203" id="Seg_8559" s="T201">Снег идет.</ta>
            <ta e="T207" id="Seg_8560" s="T203">Ветер очень холодный.</ta>
            <ta e="T210" id="Seg_8561" s="T207">У меня много дров.</ta>
            <ta e="T213" id="Seg_8562" s="T210">На эту зиму хватит.</ta>
            <ta e="T216" id="Seg_8563" s="T213">Почему ты чешешь голову?</ta>
            <ta e="T219" id="Seg_8564" s="T216">Наверное, [там] вши.</ta>
            <ta e="T222" id="Seg_8565" s="T219">Очень много вшей.</ta>
            <ta e="T225" id="Seg_8566" s="T222">Надо их убить.</ta>
            <ta e="T226" id="Seg_8567" s="T225">Хватит!</ta>
            <ta e="T229" id="Seg_8568" s="T226">Я легла спать.</ta>
            <ta e="T233" id="Seg_8569" s="T229">Курица не дала мне спать.</ta>
            <ta e="T235" id="Seg_8570" s="T233">Я встала.</ta>
            <ta e="T241" id="Seg_8571" s="T235">И больше не ложилась.</ta>
            <ta e="T244" id="Seg_8572" s="T241">Красивая женщина сидит.</ta>
            <ta e="T247" id="Seg_8573" s="T244">Красивый парень сидит.</ta>
            <ta e="T251" id="Seg_8574" s="T247">Я стала очень старая.</ta>
            <ta e="T253" id="Seg_8575" s="T251">Мы вдвоем остались.</ta>
            <ta e="T256" id="Seg_8576" s="T253">Я и мой брат.</ta>
            <ta e="T264" id="Seg_8577" s="T256">А они все умерли.</ta>
            <ta e="T273" id="Seg_8578" s="T269">Много лет прошло.</ta>
            <ta e="T278" id="Seg_8579" s="T273">Дым [= туман] на земле. [?]</ta>
            <ta e="T280" id="Seg_8580" s="T278">Дождь будет.</ta>
            <ta e="T284" id="Seg_8581" s="T280">Рука у меня чешется, надо почесать.</ta>
            <ta e="T290" id="Seg_8582" s="T284">У тебя язык очень длинный.</ta>
            <ta e="T292" id="Seg_8583" s="T290">Ты ругаешься.</ta>
            <ta e="T296" id="Seg_8584" s="T292">У тебя зубы очень длинные.</ta>
            <ta e="T300" id="Seg_8585" s="T296">У тебя нос очень длинный.</ta>
            <ta e="T303" id="Seg_8586" s="T300">У тебя сопли текут.</ta>
            <ta e="T308" id="Seg_8587" s="T303">У него толстые губы.</ta>
            <ta e="T310" id="Seg_8588" s="T309">Ну-ну-ну. </ta>
            <ta e="T313" id="Seg_8589" s="T310">У тебя волосы очень длинные.</ta>
            <ta e="T316" id="Seg_8590" s="T313">Очень уши длинные.</ta>
            <ta e="T318" id="Seg_8591" s="T316">У тебя большие глаза.</ta>
            <ta e="T321" id="Seg_8592" s="T318">У тебя голова очень большая.</ta>
            <ta e="T325" id="Seg_8593" s="T321">У него длинные руки.</ta>
            <ta e="T329" id="Seg_8594" s="T325">У него длинные ноги.</ta>
            <ta e="T332" id="Seg_8595" s="T329">А сам худой.</ta>
            <ta e="T334" id="Seg_8596" s="T332">Птица летит.</ta>
            <ta e="T336" id="Seg_8597" s="T334">Села на дерево.</ta>
            <ta e="T343" id="Seg_8598" s="T336">Много птиц летит.</ta>
            <ta e="T348" id="Seg_8599" s="T343">Ласточка слушает [= летит].</ta>
            <ta e="T353" id="Seg_8600" s="T348">Длинноухая сова прилетела.</ta>
            <ta e="T358" id="Seg_8601" s="T353">Курица копается в земле, еду ищет.</ta>
            <ta e="T365" id="Seg_8602" s="T358">Я забыла поставить воду на печь.</ta>
            <ta e="T372" id="Seg_8603" s="T365">Горячей воды нет, мне надо.</ta>
            <ta e="T376" id="Seg_8604" s="T372">Сметану налью.</ta>
            <ta e="T377" id="Seg_8605" s="T376">Я буду (резать?)</ta>
            <ta e="T381" id="Seg_8606" s="T377">Потом надо помыть чашки.</ta>
            <ta e="T389" id="Seg_8607" s="T381">Белое дерево, лиственница, красное дерево, ореховое дерево [= сибирский кедр].</ta>
            <ta e="T394" id="Seg_8608" s="T389">На кедре шишки растут.</ta>
            <ta e="T401" id="Seg_8609" s="T394">На кедре шишки растут. </ta>
            <ta e="T406" id="Seg_8610" s="T401">Из березы [= бересты] делают чашки.</ta>
            <ta e="T409" id="Seg_8611" s="T406">Чай пьют.</ta>
            <ta e="T412" id="Seg_8612" s="T409">Деревянная ложка.</ta>
            <ta e="T414" id="Seg_8613" s="T412">Железная ложка.</ta>
            <ta e="T416" id="Seg_8614" s="T414">Чашка железная.</ta>
            <ta e="T422" id="Seg_8615" s="T416">Очень много деревьев, их надо срубить.</ta>
            <ta e="T424" id="Seg_8616" s="T422">Потом снять кору.</ta>
            <ta e="T427" id="Seg_8617" s="T424">Потом (покрыть?) дом.</ta>
            <ta e="T433" id="Seg_8618" s="T427">Потом из этих деревьев надо построить дом.</ta>
            <ta e="T435" id="Seg_8619" s="T433">Жить в доме.</ta>
            <ta e="T437" id="Seg_8620" s="T435">Поставить печь.</ta>
            <ta e="T440" id="Seg_8621" s="T437">В печи печь хлеб.</ta>
            <ta e="T442" id="Seg_8622" s="T440">Пей воду!</ta>
            <ta e="T452" id="Seg_8623" s="T442">Я туда сахар положила, воды налила.</ta>
            <ta e="T456" id="Seg_8624" s="T452">Он (сладкий?).</ta>
            <ta e="T458" id="Seg_8625" s="T456">Я кладу хлеб.</ta>
            <ta e="T461" id="Seg_8626" s="T458">Пей, очень хороший!</ta>
            <ta e="T465" id="Seg_8627" s="T461">Вы вчера пили водку.</ta>
            <ta e="T470" id="Seg_8628" s="T467">Вы были пьяные.</ta>
            <ta e="T477" id="Seg_8629" s="T472">Сегодня в похмелье.</ta>
            <ta e="T480" id="Seg_8630" s="T477">Выпей красной воды!</ta>
            <ta e="T486" id="Seg_8631" s="T480">Я туда сахар положила.</ta>
            <ta e="T489" id="Seg_8632" s="T487">Воду налила.</ta>
            <ta e="T493" id="Seg_8633" s="T489">Сладко стало.</ta>
            <ta e="T496" id="Seg_8634" s="T493">Тебе хорошо будет.</ta>
            <ta e="T502" id="Seg_8635" s="T496">Не пей водку, а то голова будет болеть.</ta>
            <ta e="T504" id="Seg_8636" s="T502">Сердце будет болеть.</ta>
            <ta e="T507" id="Seg_8637" s="T504">Если будешь пить, умрешь.</ta>
            <ta e="T513" id="Seg_8638" s="T507">А если не будешь пить, долго будешь жить.</ta>
            <ta e="T519" id="Seg_8639" s="T513">Машина пашет.</ta>
            <ta e="T522" id="Seg_8640" s="T519">Там человек сидит.</ta>
            <ta e="T526" id="Seg_8641" s="T522">От машины дым идет.</ta>
            <ta e="T532" id="Seg_8642" s="T526">Она ездит туда-сюда.</ta>
            <ta e="T536" id="Seg_8643" s="T532">Мои родители нашли девушку.</ta>
            <ta e="T541" id="Seg_8644" s="T536">Говорят мне: иди с ней.</ta>
            <ta e="T546" id="Seg_8645" s="T541">Узнай, какая она.</ta>
            <ta e="T549" id="Seg_8646" s="T546">Потом замуж возьмешь.</ta>
            <ta e="T552" id="Seg_8647" s="T549">Потом появятся дети.</ta>
            <ta e="T553" id="Seg_8648" s="T552">Погоди.</ta>
            <ta e="T555" id="Seg_8649" s="T553">Твои дети вырастут.</ta>
            <ta e="T557" id="Seg_8650" s="T555">Будут помогать тебе.</ta>
            <ta e="T564" id="Seg_8651" s="T557">Жену не бей!</ta>
            <ta e="T573" id="Seg_8652" s="T564">Слушайся родителей!</ta>
            <ta e="T577" id="Seg_8653" s="T573">Всё, что ли.</ta>
            <ta e="T581" id="Seg_8654" s="T577">Сегодня они ушли.</ta>
            <ta e="T585" id="Seg_8655" s="T581">Родители ушли.</ta>
            <ta e="T588" id="Seg_8656" s="T585">Дети ушли</ta>
            <ta e="T592" id="Seg_8657" s="T588">Я одна живу в доме.</ta>
            <ta e="T595" id="Seg_8658" s="T592">Надо землю копать.</ta>
            <ta e="T597" id="Seg_8659" s="T595">Человек умер.</ta>
            <ta e="T601" id="Seg_8660" s="T597">Надо его туда положить.</ta>
            <ta e="T603" id="Seg_8661" s="T601">Потом землей засыпать.</ta>
            <ta e="T611" id="Seg_8662" s="T603">Потом плакать.</ta>
            <ta e="T620" id="Seg_8663" s="T611">Потом… Потом надо землей закрыть его.</ta>
            <ta e="T630" id="Seg_8664" s="T620">Потом домой прийти, поесть, садись есть.</ta>
            <ta e="T632" id="Seg_8665" s="T630">Человек болел.</ta>
            <ta e="T634" id="Seg_8666" s="T632">Он пошел к шаману.</ta>
            <ta e="T639" id="Seg_8667" s="T634">Шаман не смог его вылечить.</ta>
            <ta e="T643" id="Seg_8668" s="T639">Он все-таки умер.</ta>
            <ta e="T646" id="Seg_8669" s="T643">Сколько он не болел.</ta>
            <ta e="T649" id="Seg_8670" s="T646">Теперь он лежит.</ta>
            <ta e="T653" id="Seg_8671" s="T649">Ничем не шевелит.</ta>
            <ta e="T658" id="Seg_8672" s="T653">Меня очень сильно побили.</ta>
            <ta e="T663" id="Seg_8673" s="T658">Я сама не знаю, чем они били.</ta>
            <ta e="T670" id="Seg_8674" s="T663">Мне было больно, мне больно.</ta>
            <ta e="T672" id="Seg_8675" s="T670">У меня голова болит.</ta>
            <ta e="T674" id="Seg_8676" s="T672">У меня ухо болит.</ta>
            <ta e="T676" id="Seg_8677" s="T674">У меня глаз болит.</ta>
            <ta e="T678" id="Seg_8678" s="T676">У меня зуб болит.</ta>
            <ta e="T680" id="Seg_8679" s="T678">У меня нос болит.</ta>
            <ta e="T682" id="Seg_8680" s="T680">У меня рука болит.</ta>
            <ta e="T684" id="Seg_8681" s="T682">У меня нога болит.</ta>
            <ta e="T688" id="Seg_8682" s="T684">У меня живот болит, у меня сердце болит.</ta>
            <ta e="T690" id="Seg_8683" s="T688">Все у меня болит.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-PKZ">
            <ta e="T8" id="Seg_8684" s="T4">There came very much women.</ta>
            <ta e="T12" id="Seg_8685" s="T8">I said: where were you?</ta>
            <ta e="T17" id="Seg_8686" s="T12">They say: we went to milk the cows.</ta>
            <ta e="T20" id="Seg_8687" s="T17">Bring me milk!</ta>
            <ta e="T22" id="Seg_8688" s="T20">Enough, no more. [?]</ta>
            <ta e="T27" id="Seg_8689" s="T22">The child shitted, [it is] very dirty.</ta>
            <ta e="T29" id="Seg_8690" s="T27">It all dried up.</ta>
            <ta e="T32" id="Seg_8691" s="T29">I should wash him/her.</ta>
            <ta e="T38" id="Seg_8692" s="T32">Rub, then one should wash.</ta>
            <ta e="T42" id="Seg_8693" s="T38">I have to take away (the dirt). [?]</ta>
            <ta e="T45" id="Seg_8694" s="T42">Then I should give him food.</ta>
            <ta e="T51" id="Seg_8695" s="T45">Then I should put him down to sleep.</ta>
            <ta e="T53" id="Seg_8696" s="T51">Rock the baby!</ta>
            <ta e="T54" id="Seg_8697" s="T53">That's all!</ta>
            <ta e="T56" id="Seg_8698" s="T54">Sit in the house!</ta>
            <ta e="T59" id="Seg_8699" s="T56">I will go out!</ta>
            <ta e="T60" id="Seg_8700" s="T59">To piss.</ta>
            <ta e="T61" id="Seg_8701" s="T60">To take a shit.</ta>
            <ta e="T63" id="Seg_8702" s="T61">Then I will come.</ta>
            <ta e="T65" id="Seg_8703" s="T63">I will sit with you.</ta>
            <ta e="T67" id="Seg_8704" s="T65">Аnd we will speak.</ta>
            <ta e="T68" id="Seg_8705" s="T67">Enough!</ta>
            <ta e="T73" id="Seg_8706" s="T68">My ass is [more] beautiful than your face. [?]</ta>
            <ta e="T78" id="Seg_8707" s="T73">Very fat ass.</ta>
            <ta e="T81" id="Seg_8708" s="T78">Very dirty ass.</ta>
            <ta e="T84" id="Seg_8709" s="T81">Very red ass.</ta>
            <ta e="T87" id="Seg_8710" s="T84">Very white ass.</ta>
            <ta e="T90" id="Seg_8711" s="T87">Very fat ass.</ta>
            <ta e="T94" id="Seg_8712" s="T90">Very thin ass.</ta>
            <ta e="T95" id="Seg_8713" s="T94">Enough!</ta>
            <ta e="T97" id="Seg_8714" s="T95">A child is coming.</ta>
            <ta e="T100" id="Seg_8715" s="T97">His hands are all dirty.</ta>
            <ta e="T102" id="Seg_8716" s="T100">I need to wash [him/her].</ta>
            <ta e="T104" id="Seg_8717" s="T102">Come here!</ta>
            <ta e="T106" id="Seg_8718" s="T104">Don't cry!</ta>
            <ta e="T108" id="Seg_8719" s="T106">Go (to your) home!</ta>
            <ta e="T118" id="Seg_8720" s="T108">Perhaps they are looking for you. [?]</ta>
            <ta e="T122" id="Seg_8721" s="T118">Otherwise ducks (?) you.</ta>
            <ta e="T123" id="Seg_8722" s="T122">That's all!</ta>
            <ta e="T125" id="Seg_8723" s="T123">Where are you (climbing?).</ta>
            <ta e="T140" id="Seg_8724" s="T139">Oh God.</ta>
            <ta e="T143" id="Seg_8725" s="T140">Where are you climbing?</ta>
            <ta e="T147" id="Seg_8726" s="T143">Don’t climb or you will fall!</ta>
            <ta e="T149" id="Seg_8727" s="T147">Come here!</ta>
            <ta e="T154" id="Seg_8728" s="T149">This girl is sitting, all slim.</ta>
            <ta e="T163" id="Seg_8729" s="T154">What do I say, she is not listening.</ta>
            <ta e="T168" id="Seg_8730" s="T163">They go to taiga with a horse.</ta>
            <ta e="T170" id="Seg_8731" s="T168">They will catch fish.</ta>
            <ta e="T172" id="Seg_8732" s="T170">They will bring fish.</ta>
            <ta e="T175" id="Seg_8733" s="T172">Maybe they will give [it to me].</ta>
            <ta e="T176" id="Seg_8734" s="T175">Enough!</ta>
            <ta e="T181" id="Seg_8735" s="T177">In the river, they caught fish with their bare hands.</ta>
            <ta e="T186" id="Seg_8736" s="T181">They put it on the ground.</ta>
            <ta e="T190" id="Seg_8737" s="T186">Then they salted it.</ta>
            <ta e="T192" id="Seg_8738" s="T190">Then they sold it.</ta>
            <ta e="T193" id="Seg_8739" s="T192">Enough!</ta>
            <ta e="T195" id="Seg_8740" s="T193">It became cold.</ta>
            <ta e="T198" id="Seg_8741" s="T195">Warm went away.</ta>
            <ta e="T201" id="Seg_8742" s="T198">Sun is shining a little.</ta>
            <ta e="T203" id="Seg_8743" s="T201">It snows.</ta>
            <ta e="T207" id="Seg_8744" s="T203">Wind is very cold.</ta>
            <ta e="T210" id="Seg_8745" s="T207">I have a lot of wood.</ta>
            <ta e="T213" id="Seg_8746" s="T210">It is enough for this winter.</ta>
            <ta e="T216" id="Seg_8747" s="T213">Why are you scratching your head?</ta>
            <ta e="T219" id="Seg_8748" s="T216">Perhaps there are lice.</ta>
            <ta e="T222" id="Seg_8749" s="T219">Very many lice.</ta>
            <ta e="T225" id="Seg_8750" s="T222">You should kill them.</ta>
            <ta e="T226" id="Seg_8751" s="T225">Enough!</ta>
            <ta e="T229" id="Seg_8752" s="T226">I lied down to sleep.</ta>
            <ta e="T233" id="Seg_8753" s="T229">A chicken did not let me sleep.</ta>
            <ta e="T235" id="Seg_8754" s="T233">I got up.</ta>
            <ta e="T241" id="Seg_8755" s="T235">I didn’t lie down any more.</ta>
            <ta e="T244" id="Seg_8756" s="T241">A beautiful woman is sitting.</ta>
            <ta e="T247" id="Seg_8757" s="T244">A beautiful boy is sitting.</ta>
            <ta e="T251" id="Seg_8758" s="T247">I have become very old.</ta>
            <ta e="T253" id="Seg_8759" s="T251">We were left, the two of us.</ta>
            <ta e="T256" id="Seg_8760" s="T253">Me and my brother.</ta>
            <ta e="T264" id="Seg_8761" s="T256">And they all died.</ta>
            <ta e="T269" id="Seg_8762" s="T264">Well, perhaps that's all now.</ta>
            <ta e="T273" id="Seg_8763" s="T269">Many years went by.</ta>
            <ta e="T278" id="Seg_8764" s="T273">There is smoke [= mist] on the ground.</ta>
            <ta e="T280" id="Seg_8765" s="T278">Rain will come.</ta>
            <ta e="T284" id="Seg_8766" s="T280">My hand is itching, I should scratch it.</ta>
            <ta e="T290" id="Seg_8767" s="T284">Your tongue is very long.</ta>
            <ta e="T292" id="Seg_8768" s="T290">You swear.</ta>
            <ta e="T296" id="Seg_8769" s="T292">Your teeth are very long.</ta>
            <ta e="T300" id="Seg_8770" s="T296">Your nose is long.</ta>
            <ta e="T303" id="Seg_8771" s="T300">Your snot is flowing.</ta>
            <ta e="T308" id="Seg_8772" s="T303">His lips are fat.</ta>
            <ta e="T310" id="Seg_8773" s="T309">Well, well.</ta>
            <ta e="T313" id="Seg_8774" s="T310">Your hair is very long.</ta>
            <ta e="T316" id="Seg_8775" s="T313">Very long ears.</ta>
            <ta e="T318" id="Seg_8776" s="T316">Your eyes are big.</ta>
            <ta e="T321" id="Seg_8777" s="T318">Your head is very big.</ta>
            <ta e="T325" id="Seg_8778" s="T321">His/her hands are long.</ta>
            <ta e="T329" id="Seg_8779" s="T325">His/her feet are long.</ta>
            <ta e="T332" id="Seg_8780" s="T329">Him-/herself is slim.</ta>
            <ta e="T334" id="Seg_8781" s="T332">A bird is flying.</ta>
            <ta e="T336" id="Seg_8782" s="T334">It sat down on a tree.</ta>
            <ta e="T343" id="Seg_8783" s="T336">Many birds are flying.</ta>
            <ta e="T348" id="Seg_8784" s="T343">A swallow is listening [= flying].</ta>
            <ta e="T353" id="Seg_8785" s="T348">The long-eared owl came flying.</ta>
            <ta e="T358" id="Seg_8786" s="T353">The chicken is digging (the earth), it is looking for something to eat.</ta>
            <ta e="T365" id="Seg_8787" s="T358">I forgot to put water on the stove.</ta>
            <ta e="T372" id="Seg_8788" s="T365">There is no warm water, I need.</ta>
            <ta e="T376" id="Seg_8789" s="T372">I will pour cream.</ta>
            <ta e="T377" id="Seg_8790" s="T376">I will (carve/cut)</ta>
            <ta e="T381" id="Seg_8791" s="T377">Then I should wash the cups.</ta>
            <ta e="T389" id="Seg_8792" s="T381">White tree, larch tree, red tree, pine nut tree.</ta>
            <ta e="T394" id="Seg_8793" s="T389">On the pine nut pine cones are growing.</ta>
            <ta e="T401" id="Seg_8794" s="T394">On the pine trees there are pine cones.</ta>
            <ta e="T406" id="Seg_8795" s="T401">From the birch [= birchbark] people make cups.</ta>
            <ta e="T409" id="Seg_8796" s="T406">People drink tea.</ta>
            <ta e="T412" id="Seg_8797" s="T409">Wooden spoon.</ta>
            <ta e="T414" id="Seg_8798" s="T412">Iron spoon.</ta>
            <ta e="T416" id="Seg_8799" s="T414">Iron cup.</ta>
            <ta e="T422" id="Seg_8800" s="T416">Very many trees, they need to be cut.</ta>
            <ta e="T424" id="Seg_8801" s="T422">Then peel the bark.</ta>
            <ta e="T427" id="Seg_8802" s="T424">Then (cover?) a house.</ta>
            <ta e="T433" id="Seg_8803" s="T427">Then one should build a house with this wood.</ta>
            <ta e="T435" id="Seg_8804" s="T433">To live in the house.</ta>
            <ta e="T437" id="Seg_8805" s="T435">To put (=set up) a stove.</ta>
            <ta e="T440" id="Seg_8806" s="T437">To bake bread in the stove.</ta>
            <ta e="T442" id="Seg_8807" s="T440">Drink water!</ta>
            <ta e="T452" id="Seg_8808" s="T442">I put sugar in there, I poured water.</ta>
            <ta e="T456" id="Seg_8809" s="T452">It is (sweet).</ta>
            <ta e="T458" id="Seg_8810" s="T456">I'm laying bread.</ta>
            <ta e="T461" id="Seg_8811" s="T458">Drink, very good!</ta>
            <ta e="T465" id="Seg_8812" s="T461">You drank vodka yesterday.</ta>
            <ta e="T470" id="Seg_8813" s="T467">You were drunk.</ta>
            <ta e="T477" id="Seg_8814" s="T472">Today hung over.</ta>
            <ta e="T480" id="Seg_8815" s="T477">Drink red water!</ta>
            <ta e="T486" id="Seg_8816" s="T480">I put sugar in there.</ta>
            <ta e="T489" id="Seg_8817" s="T487">I poured water.</ta>
            <ta e="T493" id="Seg_8818" s="T489">It became sweet.</ta>
            <ta e="T496" id="Seg_8819" s="T493">It will be good for you.</ta>
            <ta e="T502" id="Seg_8820" s="T496">Don’t drink vodka or your head will hurt.</ta>
            <ta e="T504" id="Seg_8821" s="T502">[Your] heart will hurt.</ta>
            <ta e="T507" id="Seg_8822" s="T504">If you drink, you will die.</ta>
            <ta e="T513" id="Seg_8823" s="T507">And if you don’t drink, you will live for a long time.</ta>
            <ta e="T519" id="Seg_8824" s="T513">A machine is ploughing.</ta>
            <ta e="T522" id="Seg_8825" s="T519">A man is sitting there.</ta>
            <ta e="T526" id="Seg_8826" s="T522">Smoke comes from the machine.</ta>
            <ta e="T532" id="Seg_8827" s="T526">He is going here and there. [?]</ta>
            <ta e="T536" id="Seg_8828" s="T532">My parents found a girl.</ta>
            <ta e="T541" id="Seg_8829" s="T536">They say to me: go with her.</ta>
            <ta e="T546" id="Seg_8830" s="T541">Find out what she is like.</ta>
            <ta e="T549" id="Seg_8831" s="T546">Then you will marry her.</ta>
            <ta e="T552" id="Seg_8832" s="T549">Then [you] will have children.</ta>
            <ta e="T553" id="Seg_8833" s="T552">Wait.</ta>
            <ta e="T555" id="Seg_8834" s="T553">Your children will grow.</ta>
            <ta e="T557" id="Seg_8835" s="T555">They will help you.</ta>
            <ta e="T564" id="Seg_8836" s="T557">Do not beat the wife!</ta>
            <ta e="T573" id="Seg_8837" s="T564">Listen to your father and mother!</ta>
            <ta e="T577" id="Seg_8838" s="T573">That's all, or not.</ta>
            <ta e="T581" id="Seg_8839" s="T577">Today they went away.</ta>
            <ta e="T585" id="Seg_8840" s="T581">The parents left.</ta>
            <ta e="T588" id="Seg_8841" s="T585">The children left.</ta>
            <ta e="T592" id="Seg_8842" s="T588">I live alone at home.</ta>
            <ta e="T595" id="Seg_8843" s="T592">One should dig the ground.</ta>
            <ta e="T597" id="Seg_8844" s="T595">A man died.</ta>
            <ta e="T601" id="Seg_8845" s="T597">One should put him there.</ta>
            <ta e="T603" id="Seg_8846" s="T601">Then pour [= cover] with earth.</ta>
            <ta e="T611" id="Seg_8847" s="T603">Then cry.</ta>
            <ta e="T620" id="Seg_8848" s="T611">Then… Then one should cover him with the soil.</ta>
            <ta e="T630" id="Seg_8849" s="T620">Then come home, eat, sit down to eat.</ta>
            <ta e="T632" id="Seg_8850" s="T630">A man was ill.</ta>
            <ta e="T634" id="Seg_8851" s="T632">He went to the shaman.</ta>
            <ta e="T639" id="Seg_8852" s="T634">The shaman could not heal him.</ta>
            <ta e="T643" id="Seg_8853" s="T639">He died anyway.</ta>
            <ta e="T646" id="Seg_8854" s="T643">How long wasn't he sick. [?]</ta>
            <ta e="T649" id="Seg_8855" s="T646">Now he is lying.</ta>
            <ta e="T653" id="Seg_8856" s="T649">He does not move anything. [?]</ta>
            <ta e="T658" id="Seg_8857" s="T653">They beat me very hard.</ta>
            <ta e="T663" id="Seg_8858" s="T658">I don't know myself, with what they were beating.</ta>
            <ta e="T670" id="Seg_8859" s="T663">I was hurt, I am hurt.</ta>
            <ta e="T672" id="Seg_8860" s="T670">My head hurts.</ta>
            <ta e="T674" id="Seg_8861" s="T672">My ear hurts.</ta>
            <ta e="T676" id="Seg_8862" s="T674">My eye hurts.</ta>
            <ta e="T678" id="Seg_8863" s="T676">My tooth hurts.</ta>
            <ta e="T680" id="Seg_8864" s="T678">My nose hurts.</ta>
            <ta e="T682" id="Seg_8865" s="T680">My hand hurts.</ta>
            <ta e="T684" id="Seg_8866" s="T682">My foot hurts.</ta>
            <ta e="T688" id="Seg_8867" s="T684">My stomach hurts, my heart hurts.</ta>
            <ta e="T690" id="Seg_8868" s="T688">All my (body) hurts.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PKZ">
            <ta e="T8" id="Seg_8869" s="T4">Sehr viele Frauen kamen.</ta>
            <ta e="T12" id="Seg_8870" s="T8">Ich sagte: wo ward ihr?</ta>
            <ta e="T17" id="Seg_8871" s="T12">Sie sagten: wir gingen, die Kühe melken.</ta>
            <ta e="T20" id="Seg_8872" s="T17">Hole mir Milch!</ta>
            <ta e="T22" id="Seg_8873" s="T20">Genug, nichts mehr. [?]</ta>
            <ta e="T27" id="Seg_8874" s="T22">Das Kind hat geschissen, [es ist] sehr schmutzig.</ta>
            <ta e="T29" id="Seg_8875" s="T27">Es ist alles getrocknet.</ta>
            <ta e="T32" id="Seg_8876" s="T29">Ich sollte ihn/sie waschen.</ta>
            <ta e="T38" id="Seg_8877" s="T32">Abreiben, dann sollte man waschen.</ta>
            <ta e="T42" id="Seg_8878" s="T38">Ich muss (den Dreck) (?) weg machen.</ta>
            <ta e="T45" id="Seg_8879" s="T42">Dann sollte ich ihn zu essen geben</ta>
            <ta e="T51" id="Seg_8880" s="T45">Dann sollte ich ihn schlafen legen.</ta>
            <ta e="T53" id="Seg_8881" s="T51">Wiege das Baby!</ta>
            <ta e="T54" id="Seg_8882" s="T53">Das ist alles!</ta>
            <ta e="T56" id="Seg_8883" s="T54">Sitz im Haus!</ta>
            <ta e="T59" id="Seg_8884" s="T56">Ich werde hinaus gehen!</ta>
            <ta e="T60" id="Seg_8885" s="T59">Um zu pissen.</ta>
            <ta e="T61" id="Seg_8886" s="T60">Um zu scheißen.</ta>
            <ta e="T63" id="Seg_8887" s="T61">Dann werde ich kommen.</ta>
            <ta e="T65" id="Seg_8888" s="T63">Ich werde mich zu dir sitzen.</ta>
            <ta e="T67" id="Seg_8889" s="T65">Und wir werden sprechen.</ta>
            <ta e="T68" id="Seg_8890" s="T67">Genug!</ta>
            <ta e="T73" id="Seg_8891" s="T68">Mein Arsch ist schön[er] als dein Gesicht (?)</ta>
            <ta e="T78" id="Seg_8892" s="T73">Sehr fetter Arsch.</ta>
            <ta e="T81" id="Seg_8893" s="T78">Sehr dreckiger Arsch.</ta>
            <ta e="T84" id="Seg_8894" s="T81">Sehr roter Arsch.</ta>
            <ta e="T87" id="Seg_8895" s="T84">Sehr weißer Arsch.</ta>
            <ta e="T90" id="Seg_8896" s="T87">Sehr fetter Arsch.</ta>
            <ta e="T94" id="Seg_8897" s="T90">Sehr dünner Arsch.</ta>
            <ta e="T95" id="Seg_8898" s="T94">Genug!</ta>
            <ta e="T97" id="Seg_8899" s="T95">Ein Kind kommt.</ta>
            <ta e="T100" id="Seg_8900" s="T97">Seine Hände sind ganz schmutzig.</ta>
            <ta e="T102" id="Seg_8901" s="T100">Ich muss [ihn] waschen.</ta>
            <ta e="T104" id="Seg_8902" s="T102">Komm her!</ta>
            <ta e="T106" id="Seg_8903" s="T104">Weine nicht!</ta>
            <ta e="T108" id="Seg_8904" s="T106">Go (zu dir) nach Hause!</ta>
            <ta e="T118" id="Seg_8905" s="T108">Vielleicht suchen sie dich (?)</ta>
            <ta e="T122" id="Seg_8906" s="T118">Sonst (?) dich die Enten.</ta>
            <ta e="T123" id="Seg_8907" s="T122">Das ist alles!</ta>
            <ta e="T125" id="Seg_8908" s="T123">Wohin (kletterst?) du.</ta>
            <ta e="T140" id="Seg_8909" s="T139">Oh Gott.</ta>
            <ta e="T143" id="Seg_8910" s="T140">Wohin kletterst du?</ta>
            <ta e="T147" id="Seg_8911" s="T143">Klettere nicht, sonst wirst du fallen!</ta>
            <ta e="T149" id="Seg_8912" s="T147">Komm her!</ta>
            <ta e="T154" id="Seg_8913" s="T149">Das Mädchen sitzt, ganz schlank.</ta>
            <ta e="T163" id="Seg_8914" s="T154">Was sage ich, sie hört nicht zu.</ta>
            <ta e="T168" id="Seg_8915" s="T163">Sie gehen in die Taiga mit einem Pferd.</ta>
            <ta e="T170" id="Seg_8916" s="T168">Sie werden Fische fangen.</ta>
            <ta e="T172" id="Seg_8917" s="T170">Sie werden Fische bringen.</ta>
            <ta e="T175" id="Seg_8918" s="T172">Vielleicht werden sie [es mir] geben.</ta>
            <ta e="T176" id="Seg_8919" s="T175">Genug!</ta>
            <ta e="T181" id="Seg_8920" s="T177">Im Fluss fingen sie Fische mit bloßen Händen.</ta>
            <ta e="T186" id="Seg_8921" s="T181">Sie legten sie auf die Erde.</ta>
            <ta e="T190" id="Seg_8922" s="T186">Dann salzten sie sie.</ta>
            <ta e="T192" id="Seg_8923" s="T190">Dann verkauften sie sie.</ta>
            <ta e="T193" id="Seg_8924" s="T192">Genug!</ta>
            <ta e="T195" id="Seg_8925" s="T193">Es wurde kalt.</ta>
            <ta e="T198" id="Seg_8926" s="T195">Warm ging fort.</ta>
            <ta e="T201" id="Seg_8927" s="T198">Die Sonne scheint ein wenig.</ta>
            <ta e="T203" id="Seg_8928" s="T201">Es fällt Schnee.</ta>
            <ta e="T207" id="Seg_8929" s="T203">Der Wind ist sehr kalt.</ta>
            <ta e="T210" id="Seg_8930" s="T207">Ich habe viel Holz.</ta>
            <ta e="T213" id="Seg_8931" s="T210">Es ist genug für diesen Winter</ta>
            <ta e="T216" id="Seg_8932" s="T213">Warum kratzt du dich am Kopf?</ta>
            <ta e="T219" id="Seg_8933" s="T216">Vielleicht sind es Läuse.</ta>
            <ta e="T222" id="Seg_8934" s="T219">Sehr viele Läuse.</ta>
            <ta e="T225" id="Seg_8935" s="T222">Du solltest sie töten.</ta>
            <ta e="T226" id="Seg_8936" s="T225">Genug!</ta>
            <ta e="T229" id="Seg_8937" s="T226">Ich legte mich hin um zu schlafen.</ta>
            <ta e="T233" id="Seg_8938" s="T229">Ein Huhn ließ mich nicht schlafen.</ta>
            <ta e="T235" id="Seg_8939" s="T233">Ich stand auf.</ta>
            <ta e="T241" id="Seg_8940" s="T235">Ich legte mich nicht wieder hin.</ta>
            <ta e="T244" id="Seg_8941" s="T241">Eine schöne Frau sitzt.</ta>
            <ta e="T247" id="Seg_8942" s="T244">Ein schöner Junge sitzt.</ta>
            <ta e="T251" id="Seg_8943" s="T247">Ich bin sehr alt geworden.</ta>
            <ta e="T253" id="Seg_8944" s="T251">Wir blieben übrig, wir zwei.</ta>
            <ta e="T256" id="Seg_8945" s="T253">Ich und mein Bruder.</ta>
            <ta e="T264" id="Seg_8946" s="T256">Und sie sind alle gestorben.</ta>
            <ta e="T269" id="Seg_8947" s="T264">Also, vielleicht ist das nun alles.</ta>
            <ta e="T273" id="Seg_8948" s="T269">Viele Jahre vergingen.</ta>
            <ta e="T278" id="Seg_8949" s="T273">Rauch [= Nebel] sitzt auf der Erde.</ta>
            <ta e="T280" id="Seg_8950" s="T278">Regen wird kommen.</ta>
            <ta e="T284" id="Seg_8951" s="T280">Die Hand juckt mir, ich sollte sie kratzen.</ta>
            <ta e="T290" id="Seg_8952" s="T284">Deine Zunge ist sehr lang.</ta>
            <ta e="T292" id="Seg_8953" s="T290">Du fluchst.</ta>
            <ta e="T296" id="Seg_8954" s="T292">Deine Zähne sind sehr lang.</ta>
            <ta e="T300" id="Seg_8955" s="T296">Deine Nase ist sehr lang.</ta>
            <ta e="T303" id="Seg_8956" s="T300">Deine Rotze fließt.</ta>
            <ta e="T308" id="Seg_8957" s="T303">Seine Lippen sind dick.</ta>
            <ta e="T310" id="Seg_8958" s="T309">Na schau einmal her.</ta>
            <ta e="T313" id="Seg_8959" s="T310">Dein Haar ist sehr lang.</ta>
            <ta e="T316" id="Seg_8960" s="T313">Sehr lange Ohren.</ta>
            <ta e="T318" id="Seg_8961" s="T316">Deine Augen sind groß.</ta>
            <ta e="T321" id="Seg_8962" s="T318">Dein Kopf ist sehr groß.</ta>
            <ta e="T325" id="Seg_8963" s="T321">Seine/Ihre Hände sind lang.</ta>
            <ta e="T329" id="Seg_8964" s="T325">Seine/Ihre Füße sind lang.</ta>
            <ta e="T332" id="Seg_8965" s="T329">Er/Sie selbst ist schlank.</ta>
            <ta e="T334" id="Seg_8966" s="T332">Ein Vogel fliegt.</ta>
            <ta e="T336" id="Seg_8967" s="T334">Er saß auf einen Baum.</ta>
            <ta e="T343" id="Seg_8968" s="T336">Viele Vögel fliegen.</ta>
            <ta e="T348" id="Seg_8969" s="T343">Ein Schwalbe horcht [= fliegt].</ta>
            <ta e="T353" id="Seg_8970" s="T348">Die Langohr-Eule kam im Flug.</ta>
            <ta e="T358" id="Seg_8971" s="T353">Das Huhn gräbt (die Erde), es schaut nach etwas zu essen.</ta>
            <ta e="T365" id="Seg_8972" s="T358">Ich habe vergessen Wasser auf den Herd zu setzen.</ta>
            <ta e="T372" id="Seg_8973" s="T365">Es ist kein warmes Wasser, ich brauche.</ta>
            <ta e="T376" id="Seg_8974" s="T372">Ich werde Sahne gießen.</ta>
            <ta e="T377" id="Seg_8975" s="T376">Ich werde (tranchieren/schneiden)</ta>
            <ta e="T381" id="Seg_8976" s="T377">Dann sollte ich die Tassen waschen.</ta>
            <ta e="T389" id="Seg_8977" s="T381">Weißer Baum, Lärchen-Baum, roter Baum, Pinienkern-Baum</ta>
            <ta e="T394" id="Seg_8978" s="T389">Am Pinienkern-Baum wachsen Zapfen.</ta>
            <ta e="T401" id="Seg_8979" s="T394">An den Kiefer-Bäumen sind Kiefer-Zapfen.</ta>
            <ta e="T406" id="Seg_8980" s="T401">Aus Birken [= Birkenrinde] macht man Tassen.</ta>
            <ta e="T409" id="Seg_8981" s="T406">Man trinkt Tee.</ta>
            <ta e="T412" id="Seg_8982" s="T409">Holzlöffel.</ta>
            <ta e="T414" id="Seg_8983" s="T412">Eisenlöffel.</ta>
            <ta e="T416" id="Seg_8984" s="T414">Eiserne Tasse.</ta>
            <ta e="T422" id="Seg_8985" s="T416">Sehr viele Bäume, sie müssen gefällt werden.</ta>
            <ta e="T424" id="Seg_8986" s="T422">Dann entrinden.</ta>
            <ta e="T427" id="Seg_8987" s="T424">Dann (bedecken?) ein Haus</ta>
            <ta e="T433" id="Seg_8988" s="T427">Dann sollte man mit diesem Holz ein Haus bauen.</ta>
            <ta e="T435" id="Seg_8989" s="T433">Um in dem Haus zu wohnen.</ta>
            <ta e="T437" id="Seg_8990" s="T435">Um zu setzen (=aufstellen) einen Ofen.</ta>
            <ta e="T440" id="Seg_8991" s="T437">Um Brot im Ofen zu backen.</ta>
            <ta e="T442" id="Seg_8992" s="T440">Trink Wasser!</ta>
            <ta e="T452" id="Seg_8993" s="T442">Ich tat Zucker hinein, Ich goss Wasser.</ta>
            <ta e="T456" id="Seg_8994" s="T452">Es ist (süß)</ta>
            <ta e="T458" id="Seg_8995" s="T456">Ich lege Brot.</ta>
            <ta e="T461" id="Seg_8996" s="T458">Trink, sehr gut!</ta>
            <ta e="T465" id="Seg_8997" s="T461">Gestern habt ihr Vodka getrunken.</ta>
            <ta e="T470" id="Seg_8998" s="T467">Ihr wart betrunken.</ta>
            <ta e="T477" id="Seg_8999" s="T472">Heute habt irh einen Kater.</ta>
            <ta e="T480" id="Seg_9000" s="T477">Trink rotes Wasser!</ta>
            <ta e="T486" id="Seg_9001" s="T480">Ich tue Zucker da hinein.</ta>
            <ta e="T489" id="Seg_9002" s="T487">Ich goss Wasser.</ta>
            <ta e="T493" id="Seg_9003" s="T489">Es wurde süß.</ta>
            <ta e="T496" id="Seg_9004" s="T493">Es wird dir gut tun.</ta>
            <ta e="T502" id="Seg_9005" s="T496">Trink nicht Vodka sonst wird dir dein Kopf weh tun.</ta>
            <ta e="T504" id="Seg_9006" s="T502">[Dein] Herz wird schmerzen.</ta>
            <ta e="T507" id="Seg_9007" s="T504">Wenn du trinkst, wirst du sterben.</ta>
            <ta e="T513" id="Seg_9008" s="T507">Und wenn du nicht trinkst, wirst du lange leben.</ta>
            <ta e="T519" id="Seg_9009" s="T513">Eine Maschine pflügt.</ta>
            <ta e="T522" id="Seg_9010" s="T519">Ein Mann sitzt dort.</ta>
            <ta e="T526" id="Seg_9011" s="T522">Rauch kommt aus der Maschine.</ta>
            <ta e="T532" id="Seg_9012" s="T526">Er fährt hier und dort. [?]</ta>
            <ta e="T536" id="Seg_9013" s="T532">Meine Eltern fanden ein Mädchen.</ta>
            <ta e="T541" id="Seg_9014" s="T536">Sie sagen mir: geh mit ihr.</ta>
            <ta e="T546" id="Seg_9015" s="T541">Finde heraus, wie sie ist.</ta>
            <ta e="T549" id="Seg_9016" s="T546">Dann wirst sie heiraten.</ta>
            <ta e="T552" id="Seg_9017" s="T549">Dann bekommt [ihr] Kinder.</ta>
            <ta e="T553" id="Seg_9018" s="T552">Warte.</ta>
            <ta e="T555" id="Seg_9019" s="T553">Deine Kinder werden wachsen.</ta>
            <ta e="T557" id="Seg_9020" s="T555">Sie werden dir helfen.</ta>
            <ta e="T564" id="Seg_9021" s="T557">Schlag nicht die Ehefrau!</ta>
            <ta e="T573" id="Seg_9022" s="T564">Gehorche deinem Vater und deiner Mutter</ta>
            <ta e="T577" id="Seg_9023" s="T573">Ist das alles oder nicht.</ta>
            <ta e="T581" id="Seg_9024" s="T577">Heute gingen sie fort.</ta>
            <ta e="T585" id="Seg_9025" s="T581">Die Eltern gingen.</ta>
            <ta e="T588" id="Seg_9026" s="T585">Die Kinder gingen.</ta>
            <ta e="T592" id="Seg_9027" s="T588">Ich wohne allein zu Hause.</ta>
            <ta e="T595" id="Seg_9028" s="T592">Man sollte in die Erde graben.</ta>
            <ta e="T597" id="Seg_9029" s="T595">Ein Mann starb.</ta>
            <ta e="T601" id="Seg_9030" s="T597">Man sollte ihn dort hinein tun.</ta>
            <ta e="T603" id="Seg_9031" s="T601">Dann schütte [= bedecke] mit Erde.</ta>
            <ta e="T611" id="Seg_9032" s="T603">Dann weinen.</ta>
            <ta e="T620" id="Seg_9033" s="T611">Dann… Dann sollte man ihn mit Erde einschließen bedecken.</ta>
            <ta e="T630" id="Seg_9034" s="T620">Dann nach Hause kommen, essen, setze dich hin, um zu essen.</ta>
            <ta e="T632" id="Seg_9035" s="T630">Ein Mann war krank.</ta>
            <ta e="T634" id="Seg_9036" s="T632">Er ging zum Schamanen.</ta>
            <ta e="T639" id="Seg_9037" s="T634">Der Schamane konnte ihn nicht heilen.</ta>
            <ta e="T643" id="Seg_9038" s="T639">Er starb sowieso.</ta>
            <ta e="T646" id="Seg_9039" s="T643">Wie lang war er krank (?)</ta>
            <ta e="T649" id="Seg_9040" s="T646">Nun liegt er.</ta>
            <ta e="T653" id="Seg_9041" s="T649">Er bewegt sich gar nicht. [?]</ta>
            <ta e="T658" id="Seg_9042" s="T653">Sie schlugen mich sehr fest.</ta>
            <ta e="T663" id="Seg_9043" s="T658">Ich weiß selber nicht, womit sie mich schlugen.</ta>
            <ta e="T670" id="Seg_9044" s="T663">Ich war verletzt, ich bin verletzt.</ta>
            <ta e="T672" id="Seg_9045" s="T670">Mein Kopf tut weh.</ta>
            <ta e="T674" id="Seg_9046" s="T672">Mein Ohr tut weh.</ta>
            <ta e="T676" id="Seg_9047" s="T674">Mein Auge tut weh.</ta>
            <ta e="T678" id="Seg_9048" s="T676">Mein Zahn tut weh.</ta>
            <ta e="T680" id="Seg_9049" s="T678">Mein Nase tut weh.</ta>
            <ta e="T682" id="Seg_9050" s="T680">Meine Hand tut weh.</ta>
            <ta e="T684" id="Seg_9051" s="T682">Mein Fuß tut weh.</ta>
            <ta e="T688" id="Seg_9052" s="T684">Mein Magen tut weh, mein Herz tut weh.</ta>
            <ta e="T690" id="Seg_9053" s="T688">Mein ganzer (Körper) tut weh.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-PKZ">
            <ta e="T97" id="Seg_9054" s="T95">[KIT]: šonəgal = šonəga?</ta>
            <ta e="T125" id="Seg_9055" s="T123">[GVY]: paʔlaːngəl = paʔlaːndəgal</ta>
            <ta e="T216" id="Seg_9056" s="T213">[GVY:] Russ. "копаешься в голове".</ta>
            <ta e="T308" id="Seg_9057" s="T303">[GVY]: The Px2Sg suffix after the plural marker -zəŋ should be -nə; however, PKZ uses -də very consistently.</ta>
            <ta e="T332" id="Seg_9058" s="T329">[KIT]: Or 'yourself'?</ta>
            <ta e="T334" id="Seg_9059" s="T332">[GVY]: müjö should be süjö 'bird' (cf. the following sentences).</ta>
            <ta e="T348" id="Seg_9060" s="T343">[KIT]: probably mixes up nʼilgö- ’hear’ and nʼergö- ’fly'.</ta>
            <ta e="T377" id="Seg_9061" s="T376">[KIT]: pargə ’carve’?</ta>
            <ta e="T389" id="Seg_9062" s="T381">[GVY]: šomne = šomi 'larch'? San pa "nut tree" - cedar?</ta>
            <ta e="T394" id="Seg_9063" s="T389">[GVY:] sagən = san.</ta>
            <ta e="T406" id="Seg_9064" s="T401">[KIT]:=from birchbark, probably wrong case.</ta>
            <ta e="T427" id="Seg_9065" s="T424">[KIT]: cf. Donner-Joki kaj- 'zuschliessen, bedecken'.</ta>
            <ta e="T433" id="Seg_9066" s="T427">jaʔsittə unclear</ta>
            <ta e="T456" id="Seg_9067" s="T452">[KIT]: namzəga? </ta>
            <ta e="T480" id="Seg_9068" s="T477">[GVY:] Unclear.</ta>
            <ta e="T504" id="Seg_9069" s="T502">[GVY]: Note another instance of 2SG -də after a consonant (resonant?).</ta>
            <ta e="T536" id="Seg_9070" s="T532">[GVY:] This fragment is unnclear. Probably, it is told in the name of a boy.</ta>
            <ta e="T552" id="Seg_9071" s="T549">[KIT]: POSS could refer to any person.</ta>
            <ta e="T592" id="Seg_9072" s="T588">[KIT]: POSS.2SG, unexpected.</ta>
            <ta e="T674" id="Seg_9073" s="T672">[GVY:] Or 'ears hurt'.</ta>
            <ta e="T678" id="Seg_9074" s="T676">[GVY:] Or 'teeth'.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T692" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-KA"
                          name="ref"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KA"
                          name="ts"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KA"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KA"
                          type="t" />
         <conversion-tier category="CS"
                          display-name="CS-KA"
                          name="CS"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KA"
                          name="fr"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KA"
                          name="fe"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KA"
                          name="fg"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KA"
                          name="nt"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-PKZ"
                          name="ref"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PKZ"
                          name="ts"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PKZ"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PKZ"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PKZ"
                          name="mb"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PKZ"
                          name="mp"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PKZ"
                          name="ge"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PKZ"
                          name="gr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PKZ"
                          name="mc"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PKZ"
                          name="ps"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PKZ"
                          name="SeR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PKZ"
                          name="SyF"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PKZ"
                          name="IST"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PKZ"
                          name="BOR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PKZ"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PKZ"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PKZ"
                          name="CS"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PKZ"
                          name="fr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PKZ"
                          name="fe"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PKZ"
                          name="fg"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PKZ"
                          name="nt"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
