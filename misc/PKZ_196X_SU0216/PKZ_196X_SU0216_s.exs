<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID20F2E26B-2771-87A5-D9AA-8E448CF66B80">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_SU0216.wav" />
         <referenced-file url="PKZ_196X_SU0216.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_196X_SU0216\PKZ_196X_SU0216.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1651</ud-information>
            <ud-information attribute-name="# HIAT:w">978</ud-information>
            <ud-information attribute-name="# e">1008</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">32</ud-information>
            <ud-information attribute-name="# HIAT:u">218</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.21999897187906373" />
         <tli id="T1" time="1.146" type="appl" />
         <tli id="T2" time="1.842" type="appl" />
         <tli id="T3" time="2.538" type="appl" />
         <tli id="T4" time="3.234" type="appl" />
         <tli id="T5" time="4.153313923656264" />
         <tli id="T6" time="5.062" type="appl" />
         <tli id="T7" time="5.933" type="appl" />
         <tli id="T8" time="6.805" type="appl" />
         <tli id="T9" time="7.677" type="appl" />
         <tli id="T10" time="8.548" type="appl" />
         <tli id="T11" time="9.42" type="appl" />
         <tli id="T12" time="10.23" type="appl" />
         <tli id="T13" time="10.92" type="appl" />
         <tli id="T14" time="11.61" type="appl" />
         <tli id="T15" time="12.58660784568704" />
         <tli id="T16" time="13.541" type="appl" />
         <tli id="T17" time="14.442" type="appl" />
         <tli id="T18" time="15.343" type="appl" />
         <tli id="T19" time="16.244" type="appl" />
         <tli id="T20" time="17.145" type="appl" />
         <tli id="T21" time="18.233248123916344" />
         <tli id="T22" time="19.111" type="appl" />
         <tli id="T23" time="19.922" type="appl" />
         <tli id="T24" time="20.732" type="appl" />
         <tli id="T25" time="21.543" type="appl" />
         <tli id="T26" time="22.354" type="appl" />
         <tli id="T27" time="23.165" type="appl" />
         <tli id="T28" time="23.975" type="appl" />
         <tli id="T29" time="24.786" type="appl" />
         <tli id="T30" time="25.597" type="appl" />
         <tli id="T31" time="26.408" type="appl" />
         <tli id="T32" time="27.218" type="appl" />
         <tli id="T33" time="28.029" type="appl" />
         <tli id="T34" time="33.10651195004335" />
         <tli id="T35" time="34.338" type="appl" />
         <tli id="T36" time="35.422" type="appl" />
         <tli id="T37" time="37.04649353733203" />
         <tli id="T38" time="37.864" type="appl" />
         <tli id="T39" time="38.668" type="appl" />
         <tli id="T40" time="39.471" type="appl" />
         <tli id="T41" time="40.275" type="appl" />
         <tli id="T42" time="41.079" type="appl" />
         <tli id="T43" time="41.882" type="appl" />
         <tli id="T44" time="42.686" type="appl" />
         <tli id="T45" time="43.49" type="appl" />
         <tli id="T46" time="44.153" type="appl" />
         <tli id="T47" time="44.695" type="appl" />
         <tli id="T48" time="45.238" type="appl" />
         <tli id="T49" time="45.78" type="appl" />
         <tli id="T50" time="46.323" type="appl" />
         <tli id="T51" time="46.866" type="appl" />
         <tli id="T52" time="47.408" type="appl" />
         <tli id="T53" time="48.27310773867577" />
         <tli id="T54" time="48.968" type="appl" />
         <tli id="T55" time="49.715" type="appl" />
         <tli id="T56" time="50.462" type="appl" />
         <tli id="T57" time="51.21" type="appl" />
         <tli id="T58" time="51.957" type="appl" />
         <tli id="T59" time="52.705" type="appl" />
         <tli id="T60" time="53.338" type="appl" />
         <tli id="T61" time="53.972" type="appl" />
         <tli id="T62" time="55.146408951018635" />
         <tli id="T63" time="56.357" type="appl" />
         <tli id="T64" time="58.11972838914174" />
         <tli id="T65" time="58.823" type="appl" />
         <tli id="T66" time="59.552" type="appl" />
         <tli id="T67" time="60.282" type="appl" />
         <tli id="T68" time="61.011" type="appl" />
         <tli id="T69" time="62.37970848098179" />
         <tli id="T70" time="63.31" type="appl" />
         <tli id="T71" time="64.13" type="appl" />
         <tli id="T72" time="64.75969735858257" />
         <tli id="T73" time="65.686" type="appl" />
         <tli id="T74" time="66.348" type="appl" />
         <tli id="T75" time="67.009" type="appl" />
         <tli id="T76" time="67.671" type="appl" />
         <tli id="T77" time="68.332" type="appl" />
         <tli id="T78" time="68.994" type="appl" />
         <tli id="T79" time="70.18633866493282" />
         <tli id="T80" time="70.918" type="appl" />
         <tli id="T81" time="71.767" type="appl" />
         <tli id="T82" time="72.615" type="appl" />
         <tli id="T83" time="73.463" type="appl" />
         <tli id="T84" time="74.312" type="appl" />
         <tli id="T85" time="76.13297754117902" />
         <tli id="T86" time="77.312" type="appl" />
         <tli id="T87" time="78.444" type="appl" />
         <tli id="T88" time="79.575" type="appl" />
         <tli id="T89" time="80.707" type="appl" />
         <tli id="T90" time="82.18628258560902" />
         <tli id="T91" time="82.72" type="appl" />
         <tli id="T92" time="83.391" type="appl" />
         <tli id="T93" time="84.061" type="appl" />
         <tli id="T94" time="84.732" type="appl" />
         <tli id="T95" time="85.402" type="appl" />
         <tli id="T96" time="86.072" type="appl" />
         <tli id="T97" time="86.743" type="appl" />
         <tli id="T98" time="87.413" type="appl" />
         <tli id="T99" time="88.084" type="appl" />
         <tli id="T100" time="88.754" type="appl" />
         <tli id="T101" time="89.757" type="appl" />
         <tli id="T102" time="90.747" type="appl" />
         <tli id="T103" time="91.472" type="appl" />
         <tli id="T104" time="92.192" type="appl" />
         <tli id="T105" time="94.25289286140007" />
         <tli id="T106" time="95.373" type="appl" />
         <tli id="T107" time="96.447" type="appl" />
         <tli id="T108" time="98.71287201858473" />
         <tli id="T109" time="99.668" type="appl" />
         <tli id="T110" time="100.251" type="appl" />
         <tli id="T111" time="100.835" type="appl" />
         <tli id="T112" time="101.418" type="appl" />
         <tli id="T113" time="102.1595225780234" />
         <tli id="T114" time="103.795" type="appl" />
         <tli id="T115" time="105.325" type="appl" />
         <tli id="T116" time="110.87948182704811" />
         <tli id="T117" time="112.164" type="appl" />
         <tli id="T118" time="112.979" type="appl" />
         <tli id="T119" time="113.793" type="appl" />
         <tli id="T120" time="114.607" type="appl" />
         <tli id="T121" time="115.421" type="appl" />
         <tli id="T122" time="116.236" type="appl" />
         <tli id="T123" time="117.05" type="appl" />
         <tli id="T124" time="118.123" type="appl" />
         <tli id="T125" time="119.187" type="appl" />
         <tli id="T126" time="120.25" type="appl" />
         <tli id="T127" time="120.86" type="appl" />
         <tli id="T128" time="122.31276172924795" />
         <tli id="T129" time="123.192" type="appl" />
         <tli id="T130" time="123.94" type="appl" />
         <tli id="T131" time="124.688" type="appl" />
         <tli id="T132" time="125.70607920459472" />
         <tli id="T133" time="126.483" type="appl" />
         <tli id="T134" time="127.346" type="appl" />
         <tli id="T135" time="128.209" type="appl" />
         <tli id="T136" time="129.071" type="appl" />
         <tli id="T137" time="129.934" type="appl" />
         <tli id="T138" time="130.797" type="appl" />
         <tli id="T139" time="131.79938406209362" />
         <tli id="T140" time="133.313" type="appl" />
         <tli id="T141" time="134.817" type="appl" />
         <tli id="T142" time="136.32" type="appl" />
         <tli id="T143" time="138.159354340052" />
         <tli id="T144" time="138.888" type="appl" />
         <tli id="T145" time="139.646" type="appl" />
         <tli id="T146" time="140.404" type="appl" />
         <tli id="T147" time="141.162" type="appl" />
         <tli id="T148" time="141.92" type="appl" />
         <tli id="T149" time="142.46" type="appl" />
         <tli id="T150" time="143.0" type="appl" />
         <tli id="T151" time="143.95932723504552" />
         <tli id="T152" time="144.722" type="appl" />
         <tli id="T153" time="145.388" type="appl" />
         <tli id="T154" time="146.53264854247942" />
         <tli id="T155" time="147.25" type="appl" />
         <tli id="T156" time="148.17" type="appl" />
         <tli id="T157" time="149.09" type="appl" />
         <tli id="T158" time="150.79929526983094" />
         <tli id="T159" time="151.876" type="appl" />
         <tli id="T160" time="153.11" type="appl" />
         <tli id="T161" time="155.24594115599265" />
         <tli id="T162" time="156.197" type="appl" />
         <tli id="T163" time="157.187" type="appl" />
         <tli id="T164" time="159.43925489271783" />
         <tli id="T165" time="160.089" type="appl" />
         <tli id="T166" time="160.818" type="appl" />
         <tli id="T167" time="161.546" type="appl" />
         <tli id="T168" time="162.275" type="appl" />
         <tli id="T169" time="163.004" type="appl" />
         <tli id="T170" time="163.733" type="appl" />
         <tli id="T171" time="164.462" type="appl" />
         <tli id="T172" time="165.19" type="appl" />
         <tli id="T173" time="165.919" type="appl" />
         <tli id="T174" time="168.09921442213914" />
         <tli id="T175" time="169.324" type="appl" />
         <tli id="T176" time="170.238" type="appl" />
         <tli id="T177" time="171.152" type="appl" />
         <tli id="T178" time="172.066" type="appl" />
         <tli id="T179" time="173.43918946684005" />
         <tli id="T180" time="174.496" type="appl" />
         <tli id="T181" time="175.552" type="appl" />
         <tli id="T182" time="176.609" type="appl" />
         <tli id="T183" time="177.665" type="appl" />
         <tli id="T184" time="178.721" type="appl" />
         <tli id="T185" time="179.777" type="appl" />
         <tli id="T186" time="180.834" type="appl" />
         <tli id="T187" time="181.89" type="appl" />
         <tli id="T188" time="182.74" type="appl" />
         <tli id="T189" time="185.7657985275791" />
         <tli id="T190" time="186.636" type="appl" />
         <tli id="T191" time="187.361" type="appl" />
         <tli id="T192" time="188.087" type="appl" />
         <tli id="T193" time="188.812" type="appl" />
         <tli id="T194" time="189.538" type="appl" />
         <tli id="T195" time="190.263" type="appl" />
         <tli id="T196" time="190.989" type="appl" />
         <tli id="T197" time="191.714" type="appl" />
         <tli id="T198" time="192.60576656236452" />
         <tli id="T199" time="193.095" type="appl" />
         <tli id="T200" time="193.62" type="appl" />
         <tli id="T201" time="194.145" type="appl" />
         <tli id="T202" time="195.12575478570653" />
         <tli id="T203" time="196.013" type="appl" />
         <tli id="T204" time="197.037" type="appl" />
         <tli id="T205" time="200.68572880228652" />
         <tli id="T206" time="201.365" type="appl" />
         <tli id="T207" time="202.4" type="appl" />
         <tli id="T208" time="203.435" type="appl" />
         <tli id="T209" time="204.47" type="appl" />
         <tli id="T210" time="205.39" type="appl" />
         <tli id="T211" time="206.24" type="appl" />
         <tli id="T212" time="207.09" type="appl" />
         <tli id="T213" time="207.94" type="appl" />
         <tli id="T214" time="208.79" type="appl" />
         <tli id="T215" time="209.64" type="appl" />
         <tli id="T216" time="210.49" type="appl" />
         <tli id="T217" time="211.34" type="appl" />
         <tli id="T218" time="213.1056707601864" />
         <tli id="T219" time="213.768" type="appl" />
         <tli id="T220" time="214.515" type="appl" />
         <tli id="T221" time="215.262" type="appl" />
         <tli id="T222" time="216.01" type="appl" />
         <tli id="T223" time="216.896" type="appl" />
         <tli id="T224" time="217.702" type="appl" />
         <tli id="T225" time="218.509" type="appl" />
         <tli id="T226" time="220.05230496315562" />
         <tli id="T227" time="220.837" type="appl" />
         <tli id="T228" time="221.787" type="appl" />
         <tli id="T229" time="222.737" type="appl" />
         <tli id="T230" time="223.80562075612266" />
         <tli id="T231" time="224.674" type="appl" />
         <tli id="T232" time="225.456" type="appl" />
         <tli id="T233" time="226.238" type="appl" />
         <tli id="T234" time="227.09893869879713" />
         <tli id="T235" time="227.935" type="appl" />
         <tli id="T236" time="228.85" type="appl" />
         <tli id="T237" time="229.765" type="appl" />
         <tli id="T238" time="230.93225411790206" />
         <tli id="T239" time="231.68" type="appl" />
         <tli id="T240" time="232.68" type="appl" />
         <tli id="T241" time="233.68" type="appl" />
         <tli id="T242" time="234.68" type="appl" />
         <tli id="T243" time="235.68" type="appl" />
         <tli id="T244" time="236.65" type="appl" />
         <tli id="T245" time="237.575" type="appl" />
         <tli id="T246" time="240.55220916097744" />
         <tli id="T247" time="241.468" type="appl" />
         <tli id="T248" time="242.307" type="appl" />
         <tli id="T249" time="243.145" type="appl" />
         <tli id="T250" time="243.983" type="appl" />
         <tli id="T251" time="244.822" type="appl" />
         <tli id="T252" time="245.818851215052" />
         <tli id="T253" time="246.642" type="appl" />
         <tli id="T254" time="247.454" type="appl" />
         <tli id="T255" time="248.265" type="appl" />
         <tli id="T256" time="249.16550224181836" />
         <tli id="T257" time="250.285" type="appl" />
         <tli id="T258" time="251.123" type="appl" />
         <tli id="T259" time="251.96" type="appl" />
         <tli id="T260" time="252.798" type="appl" />
         <tli id="T261" time="253.635" type="appl" />
         <tli id="T262" time="254.473" type="appl" />
         <tli id="T263" time="255.33214008994366" />
         <tli id="T264" time="256.6" type="appl" />
         <tli id="T265" time="257.66" type="appl" />
         <tli id="T266" time="258.72" type="appl" />
         <tli id="T267" time="259.78" type="appl" />
         <tli id="T268" time="260.84" type="appl" />
         <tli id="T269" time="261.9" type="appl" />
         <tli id="T270" time="262.96" type="appl" />
         <tli id="T271" time="264.02" type="appl" />
         <tli id="T272" time="264.73" type="appl" />
         <tli id="T273" time="265.42667624849304" />
         <tli id="T274" time="266.234" type="appl" />
         <tli id="T275" time="267.002" type="appl" />
         <tli id="T276" time="267.771" type="appl" />
         <tli id="T277" time="268.5266617613344" />
         <tli id="T278" time="268.904" type="appl" />
         <tli id="T279" time="269.268" type="appl" />
         <tli id="T280" time="269.632" type="appl" />
         <tli id="T281" time="269.996" type="appl" />
         <tli id="T282" time="270.36" type="appl" />
         <tli id="T283" time="270.724" type="appl" />
         <tli id="T284" time="271.59" type="appl" />
         <tli id="T285" time="272.454" type="appl" />
         <tli id="T286" time="273.317" type="appl" />
         <tli id="T287" time="275.4653793346337" />
         <tli id="T288" time="276.834" type="appl" />
         <tli id="T289" time="278.088" type="appl" />
         <tli id="T290" time="279.342" type="appl" />
         <tli id="T291" time="280.596" type="appl" />
         <tli id="T292" time="281.91667731079053" />
         <tli id="T293" time="282.938" type="appl" />
         <tli id="T294" time="284.026" type="appl" />
         <tli id="T295" time="285.115" type="appl" />
         <tli id="T296" time="286.203" type="appl" />
         <tli id="T297" time="287.291" type="appl" />
         <tli id="T298" time="288.43198540447554" />
         <tli id="T299" time="289.665" type="appl" />
         <tli id="T300" time="290.645" type="appl" />
         <tli id="T301" time="291.625" type="appl" />
         <tli id="T302" time="292.605" type="appl" />
         <tli id="T303" time="293.477" type="appl" />
         <tli id="T304" time="294.255" type="appl" />
         <tli id="T305" time="295.032" type="appl" />
         <tli id="T306" time="295.9186170838752" />
         <tli id="T307" time="297.43860998049416" />
         <tli id="T308" time="298.202" type="appl" />
         <tli id="T309" time="298.74" type="appl" />
         <tli id="T310" time="299.278" type="appl" />
         <tli id="T311" time="299.9652648393477" />
         <tli id="T312" time="301.785" type="appl" />
         <tli id="T313" time="303.77858035191804" />
         <tli id="T314" time="304.698" type="appl" />
         <tli id="T315" time="305.487" type="appl" />
         <tli id="T316" time="306.275" type="appl" />
         <tli id="T317" time="307.063" type="appl" />
         <tli id="T318" time="307.852" type="appl" />
         <tli id="T319" time="308.3318924062635" />
         <tli id="T320" time="309.99188464862374" />
         <tli id="T321" time="310.648" type="appl" />
         <tli id="T322" time="311.205" type="appl" />
         <tli id="T323" time="311.763" type="appl" />
         <tli id="T324" time="312.32" type="appl" />
         <tli id="T325" time="312.98520399328135" />
         <tli id="T326" time="313.79853352568267" />
         <tli id="T327" time="314.203" type="appl" />
         <tli id="T328" time="314.772" type="appl" />
         <tli id="T329" time="315.34" type="appl" />
         <tli id="T330" time="315.908" type="appl" />
         <tli id="T331" time="316.477" type="appl" />
         <tli id="T332" time="317.045" type="appl" />
         <tli id="T333" time="317.613" type="appl" />
         <tli id="T334" time="318.182" type="appl" />
         <tli id="T335" time="319.3918407198201" />
         <tli id="T336" time="320.097" type="appl" />
         <tli id="T337" time="321.073" type="appl" />
         <tli id="T338" time="321.978495298277" />
         <tli id="T339" time="323.8518198770048" />
         <tli id="T340" time="324.91" type="appl" />
         <tli id="T341" time="325.565" type="appl" />
         <tli id="T342" time="326.22" type="appl" />
         <tli id="T343" time="326.875" type="appl" />
         <tli id="T344" time="327.53" type="appl" />
         <tli id="T345" time="328.9184628657347" />
         <tli id="T346" time="329.634" type="appl" />
         <tli id="T347" time="330.342" type="appl" />
         <tli id="T348" time="331.051" type="appl" />
         <tli id="T349" time="331.76" type="appl" />
         <tli id="T350" time="333.072" type="appl" />
         <tli id="T351" time="334.385" type="appl" />
         <tli id="T352" time="335.698" type="appl" />
         <tli id="T353" time="337.32509024571954" />
         <tli id="T354" time="338.197" type="appl" />
         <tli id="T355" time="339.083" type="appl" />
         <tli id="T356" time="340.34507613242306" />
         <tli id="T357" time="341.189" type="appl" />
         <tli id="T358" time="341.852" type="appl" />
         <tli id="T359" time="342.516" type="appl" />
         <tli id="T360" time="343.179" type="appl" />
         <tli id="T361" time="343.843" type="appl" />
         <tli id="T362" time="344.506" type="appl" />
         <tli id="T363" time="345.5117186538253" />
         <tli id="T364" time="346.335" type="appl" />
         <tli id="T365" time="347.108" type="appl" />
         <tli id="T366" time="347.882" type="appl" />
         <tli id="T367" time="349.2317012692349" />
         <tli id="T368" time="350.012" type="appl" />
         <tli id="T369" time="350.525" type="appl" />
         <tli id="T370" time="351.038" type="appl" />
         <tli id="T371" time="351.55" type="appl" />
         <tli id="T372" time="352.062" type="appl" />
         <tli id="T373" time="352.575" type="appl" />
         <tli id="T374" time="353.088" type="appl" />
         <tli id="T375" time="353.6983470619311" />
         <tli id="T376" time="354.367" type="appl" />
         <tli id="T377" time="355.113" type="appl" />
         <tli id="T378" time="355.86" type="appl" />
         <tli id="T379" time="356.607" type="appl" />
         <tli id="T380" time="357.353" type="appl" />
         <tli id="T381" time="359.6583192092003" />
         <tli id="T382" time="360.628" type="appl" />
         <tli id="T383" time="361.365" type="appl" />
         <tli id="T384" time="362.102" type="appl" />
         <tli id="T385" time="362.84" type="appl" />
         <tli id="T386" time="363.578" type="appl" />
         <tli id="T387" time="364.315" type="appl" />
         <tli id="T388" time="365.052" type="appl" />
         <tli id="T389" time="365.87829014141744" />
         <tli id="T390" time="366.4" type="appl" />
         <tli id="T391" time="368.53827771050067" />
         <tli id="T1011" time="368.8947301202861" type="intp" />
         <tli id="T392" time="369.37" type="appl" />
         <tli id="T393" time="370.01" type="appl" />
         <tli id="T394" time="370.65" type="appl" />
         <tli id="T395" time="371.29" type="appl" />
         <tli id="T396" time="371.93" type="appl" />
         <tli id="T397" time="372.569" type="appl" />
         <tli id="T398" time="373.209" type="appl" />
         <tli id="T399" time="373.849" type="appl" />
         <tli id="T400" time="374.489" type="appl" />
         <tli id="T401" time="375.129" type="appl" />
         <tli id="T402" time="375.885" type="appl" />
         <tli id="T403" time="376.52" type="appl" />
         <tli id="T404" time="377.155" type="appl" />
         <tli id="T405" time="377.79" type="appl" />
         <tli id="T406" time="378.425" type="appl" />
         <tli id="T407" time="380.8715534067512" />
         <tli id="T408" time="381.845" type="appl" />
         <tli id="T409" time="382.6016755297068" />
         <tli id="T410" time="383.282" type="appl" />
         <tli id="T411" time="383.949" type="appl" />
         <tli id="T412" time="384.616" type="appl" />
         <tli id="T413" time="385.283" type="appl" />
         <tli id="T414" time="385.95" type="appl" />
         <tli id="T415" time="386.679" type="appl" />
         <tli id="T416" time="387.301" type="appl" />
         <tli id="T417" time="387.923" type="appl" />
         <tli id="T418" time="388.545" type="appl" />
         <tli id="T419" time="389.2515142446901" />
         <tli id="T420" time="390.084" type="appl" />
         <tli id="T421" time="390.848" type="appl" />
         <tli id="T422" time="391.612" type="appl" />
         <tli id="T423" time="392.376" type="appl" />
         <tli id="T424" time="393.99815872886865" />
         <tli id="T425" time="395.29" type="appl" />
         <tli id="T426" time="396.21" type="appl" />
         <tli id="T427" time="397.13" type="appl" />
         <tli id="T428" time="397.98998903218614" />
         <tli id="T429" time="398.616" type="appl" />
         <tli id="T430" time="399.178" type="appl" />
         <tli id="T431" time="399.739" type="appl" />
         <tli id="T432" time="400.3" type="appl" />
         <tli id="T433" time="400.862" type="appl" />
         <tli id="T434" time="401.423" type="appl" />
         <tli id="T435" time="401.985" type="appl" />
         <tli id="T436" time="402.546" type="appl" />
         <tli id="T437" time="403.107" type="appl" />
         <tli id="T438" time="403.669" type="appl" />
         <tli id="T439" time="404.23" type="appl" />
         <tli id="T440" time="405.187" type="appl" />
         <tli id="T441" time="406.138" type="appl" />
         <tli id="T442" time="407.09" type="appl" />
         <tli id="T443" time="408.041" type="appl" />
         <tli id="T444" time="409.1580878819896" />
         <tli id="T445" time="410.514" type="appl" />
         <tli id="T446" time="411.71140928288906" />
         <tli id="T447" time="414.2713973193" />
         <tli id="T448" time="415.402" type="appl" />
         <tli id="T449" time="416.393" type="appl" />
         <tli id="T450" time="417.385" type="appl" />
         <tli id="T451" time="418.377" type="appl" />
         <tli id="T452" time="419.368" type="appl" />
         <tli id="T453" time="420.36" type="appl" />
         <tli id="T454" time="421.738" type="appl" />
         <tli id="T455" time="423.115" type="appl" />
         <tli id="T456" time="424.492" type="appl" />
         <tli id="T457" time="425.87" type="appl" />
         <tli id="T458" time="426.355" type="appl" />
         <tli id="T459" time="426.84" type="appl" />
         <tli id="T460" time="427.325" type="appl" />
         <tli id="T461" time="428.08466609910056" />
         <tli id="T462" time="429.012" type="appl" />
         <tli id="T463" time="429.888" type="appl" />
         <tli id="T464" time="431.0646521727351" />
         <tli id="T465" time="431.713" type="appl" />
         <tli id="T466" time="432.506" type="appl" />
         <tli id="T1010" time="432.8715384615384" type="intp" />
         <tli id="T467" time="433.298" type="appl" />
         <tli id="T468" time="434.091" type="appl" />
         <tli id="T469" time="434.884" type="appl" />
         <tli id="T470" time="435.677" type="appl" />
         <tli id="T471" time="436.469" type="appl" />
         <tli id="T472" time="437.40462254415905" />
         <tli id="T473" time="439.96461058057" />
         <tli id="T474" time="441.355" type="appl" />
         <tli id="T475" time="442.57" type="appl" />
         <tli id="T476" time="443.785" type="appl" />
         <tli id="T477" time="445.0" type="appl" />
         <tli id="T478" time="446.215" type="appl" />
         <tli id="T479" time="447.43" type="appl" />
         <tli id="T480" time="448.245" type="appl" />
         <tli id="T481" time="449.04" type="appl" />
         <tli id="T482" time="449.835" type="appl" />
         <tli id="T483" time="450.63" type="appl" />
         <tli id="T484" time="451.3" type="appl" />
         <tli id="T485" time="451.97" type="appl" />
         <tli id="T486" time="452.64" type="appl" />
         <tli id="T487" time="453.47" type="appl" />
         <tli id="T488" time="454.28" type="appl" />
         <tli id="T489" time="455.33787207005855" />
         <tli id="T490" time="456.032" type="appl" />
         <tli id="T491" time="456.645" type="appl" />
         <tli id="T492" time="457.258" type="appl" />
         <tli id="T493" time="457.9366620084699" />
         <tli id="T494" time="458.632" type="appl" />
         <tli id="T495" time="459.374" type="appl" />
         <tli id="T496" time="460.116" type="appl" />
         <tli id="T497" time="460.858" type="appl" />
         <tli id="T498" time="461.6" type="appl" />
         <tli id="T499" time="462.329" type="appl" />
         <tli id="T500" time="462.898" type="appl" />
         <tli id="T501" time="463.466" type="appl" />
         <tli id="T502" time="464.034" type="appl" />
         <tli id="T503" time="464.602" type="appl" />
         <tli id="T504" time="465.171" type="appl" />
         <tli id="T505" time="465.739" type="appl" />
         <tli id="T506" time="466.401" type="appl" />
         <tli id="T507" time="467.051" type="appl" />
         <tli id="T508" time="467.702" type="appl" />
         <tli id="T509" time="469.5844721581058" />
         <tli id="T510" time="470.548" type="appl" />
         <tli id="T511" time="471.246" type="appl" />
         <tli id="T512" time="471.944" type="appl" />
         <tli id="T513" time="472.642" type="appl" />
         <tli id="T514" time="473.34" type="appl" />
         <tli id="T515" time="474.056" type="appl" />
         <tli id="T516" time="474.771" type="appl" />
         <tli id="T517" time="475.486" type="appl" />
         <tli id="T518" time="476.202" type="appl" />
         <tli id="T519" time="476.918" type="appl" />
         <tli id="T520" time="477.633" type="appl" />
         <tli id="T521" time="478.348" type="appl" />
         <tli id="T522" time="479.064" type="appl" />
         <tli id="T523" time="479.814" type="appl" />
         <tli id="T524" time="480.549" type="appl" />
         <tli id="T525" time="481.284" type="appl" />
         <tli id="T526" time="482.018" type="appl" />
         <tli id="T527" time="482.752" type="appl" />
         <tli id="T528" time="483.487" type="appl" />
         <tli id="T529" time="485.23106570356526" />
         <tli id="T530" time="486.316" type="appl" />
         <tli id="T531" time="487.23" type="appl" />
         <tli id="T532" time="488.143" type="appl" />
         <tli id="T533" time="490.69770682298434" />
         <tli id="T534" time="491.628" type="appl" />
         <tli id="T535" time="492.598" type="appl" />
         <tli id="T536" time="493.7843590648028" />
         <tli id="T537" time="494.63768841027303" />
         <tli id="T538" time="495.255" type="appl" />
         <tli id="T539" time="495.782" type="appl" />
         <tli id="T540" time="496.31" type="appl" />
         <tli id="T541" time="496.837" type="appl" />
         <tli id="T542" time="497.364" type="appl" />
         <tli id="T543" time="497.891" type="appl" />
         <tli id="T544" time="498.418" type="appl" />
         <tli id="T545" time="498.946" type="appl" />
         <tli id="T546" time="499.473" type="appl" />
         <tli id="T547" time="500.0" type="appl" />
         <tli id="T548" time="500.78" type="appl" />
         <tli id="T549" time="501.56" type="appl" />
         <tli id="T550" time="502.34" type="appl" />
         <tli id="T551" time="503.12" type="appl" />
         <tli id="T552" time="503.9" type="appl" />
         <tli id="T553" time="504.552" type="appl" />
         <tli id="T554" time="505.204" type="appl" />
         <tli id="T555" time="505.856" type="appl" />
         <tli id="T556" time="506.508" type="appl" />
         <tli id="T557" time="507.93762625568917" />
         <tli id="T558" time="508.941" type="appl" />
         <tli id="T559" time="509.928" type="appl" />
         <tli id="T560" time="510.916" type="appl" />
         <tli id="T561" time="511.904" type="appl" />
         <tli id="T562" time="512.891" type="appl" />
         <tli id="T563" time="513.879" type="appl" />
         <tli id="T564" time="514.866" type="appl" />
         <tli id="T565" time="516.1375879348179" />
         <tli id="T566" time="516.897" type="appl" />
         <tli id="T567" time="517.714" type="appl" />
         <tli id="T568" time="518.532" type="appl" />
         <tli id="T569" time="519.349" type="appl" />
         <tli id="T570" time="520.166" type="appl" />
         <tli id="T571" time="520.983" type="appl" />
         <tli id="T572" time="521.801" type="appl" />
         <tli id="T573" time="522.618" type="appl" />
         <tli id="T574" time="523.435" type="appl" />
         <tli id="T575" time="524.158" type="appl" />
         <tli id="T576" time="524.882" type="appl" />
         <tli id="T577" time="525.5108774639683" />
         <tli id="T578" time="526.84" type="appl" />
         <tli id="T579" time="528.07" type="appl" />
         <tli id="T580" time="529.3" type="appl" />
         <tli id="T581" time="530.7575196115084" />
         <tli id="T582" time="532.34" type="appl" />
         <tli id="T583" time="534.8108340024925" />
         <tli id="T584" time="535.682" type="appl" />
         <tli id="T585" time="536.444" type="appl" />
         <tli id="T586" time="537.206" type="appl" />
         <tli id="T587" time="537.968" type="appl" />
         <tli id="T588" time="538.73" type="appl" />
         <tli id="T589" time="539.534" type="appl" />
         <tli id="T590" time="540.308" type="appl" />
         <tli id="T591" time="541.082" type="appl" />
         <tli id="T592" time="541.856" type="appl" />
         <tli id="T593" time="542.63" type="appl" />
         <tli id="T594" time="543.404" type="appl" />
         <tli id="T595" time="544.178" type="appl" />
         <tli id="T596" time="544.952" type="appl" />
         <tli id="T597" time="545.726" type="appl" />
         <tli id="T598" time="546.5" type="appl" />
         <tli id="T599" time="547.272" type="appl" />
         <tli id="T600" time="547.974" type="appl" />
         <tli id="T601" time="548.677" type="appl" />
         <tli id="T602" time="549.379" type="appl" />
         <tli id="T603" time="550.081" type="appl" />
         <tli id="T604" time="550.783" type="appl" />
         <tli id="T605" time="551.486" type="appl" />
         <tli id="T606" time="552.188" type="appl" />
         <tli id="T607" time="552.89" type="appl" />
         <tli id="T608" time="554.1974100698959" />
         <tli id="T609" time="555.233" type="appl" />
         <tli id="T610" time="556.245" type="appl" />
         <tli id="T611" time="557.258" type="appl" />
         <tli id="T612" time="558.27" type="appl" />
         <tli id="T613" time="559.283" type="appl" />
         <tli id="T614" time="560.295" type="appl" />
         <tli id="T615" time="561.308" type="appl" />
         <tli id="T616" time="562.135" type="appl" />
         <tli id="T617" time="562.9" type="appl" />
         <tli id="T618" time="563.665" type="appl" />
         <tli id="T619" time="564.43" type="appl" />
         <tli id="T620" time="565.195" type="appl" />
         <tli id="T621" time="565.96" type="appl" />
         <tli id="T622" time="566.821" type="appl" />
         <tli id="T623" time="567.581" type="appl" />
         <tli id="T624" time="568.342" type="appl" />
         <tli id="T625" time="569.103" type="appl" />
         <tli id="T626" time="569.864" type="appl" />
         <tli id="T627" time="570.624" type="appl" />
         <tli id="T628" time="571.385" type="appl" />
         <tli id="T629" time="572.146" type="appl" />
         <tli id="T630" time="572.906" type="appl" />
         <tli id="T631" time="573.667" type="appl" />
         <tli id="T632" time="574.428" type="appl" />
         <tli id="T633" time="575.189" type="appl" />
         <tli id="T634" time="575.949" type="appl" />
         <tli id="T635" time="576.71" type="appl" />
         <tli id="T636" time="577.545" type="appl" />
         <tli id="T637" time="578.38" type="appl" />
         <tli id="T638" time="579.215" type="appl" />
         <tli id="T639" time="580.1172889385566" />
         <tli id="T640" time="583.8172716474317" />
         <tli id="T641" time="585.017" type="appl" />
         <tli id="T642" time="586.023" type="appl" />
         <tli id="T643" time="587.03" type="appl" />
         <tli id="T644" time="587.942" type="appl" />
         <tli id="T645" time="588.669" type="appl" />
         <tli id="T646" time="589.396" type="appl" />
         <tli id="T647" time="590.123" type="appl" />
         <tli id="T648" time="590.98390482228" />
         <tli id="T649" time="592.05" type="appl" />
         <tli id="T650" time="593.234" type="appl" />
         <tli id="T651" time="594.417" type="appl" />
         <tli id="T652" time="595.342" type="appl" />
         <tli id="T653" time="596.217" type="appl" />
         <tli id="T654" time="597.092" type="appl" />
         <tli id="T655" time="598.1172048195709" />
         <tli id="T656" time="599.154" type="appl" />
         <tli id="T657" time="600.098" type="appl" />
         <tli id="T658" time="601.042" type="appl" />
         <tli id="T659" time="601.987" type="appl" />
         <tli id="T660" time="602.931" type="appl" />
         <tli id="T661" time="603.875" type="appl" />
         <tli id="T662" time="604.819" type="appl" />
         <tli id="T663" time="605.592" type="appl" />
         <tli id="T664" time="606.325" type="appl" />
         <tli id="T665" time="607.058" type="appl" />
         <tli id="T666" time="607.79" type="appl" />
         <tli id="T667" time="608.522" type="appl" />
         <tli id="T668" time="609.255" type="appl" />
         <tli id="T669" time="609.988" type="appl" />
         <tli id="T670" time="610.9638114502059" />
         <tli id="T671" time="611.785" type="appl" />
         <tli id="T672" time="612.659" type="appl" />
         <tli id="T673" time="613.534" type="appl" />
         <tli id="T674" time="614.408" type="appl" />
         <tli id="T675" time="615.283" type="appl" />
         <tli id="T676" time="616.158" type="appl" />
         <tli id="T677" time="617.032" type="appl" />
         <tli id="T678" time="617.907" type="appl" />
         <tli id="T679" time="618.782" type="appl" />
         <tli id="T680" time="619.656" type="appl" />
         <tli id="T681" time="620.531" type="appl" />
         <tli id="T682" time="621.405" type="appl" />
         <tli id="T683" time="622.28" type="appl" />
         <tli id="T684" time="623.59" type="appl" />
         <tli id="T685" time="625.269" type="appl" />
         <tli id="T686" time="626.619" type="appl" />
         <tli id="T687" time="627.968" type="appl" />
         <tli id="T688" time="629.317" type="appl" />
         <tli id="T689" time="630.666" type="appl" />
         <tli id="T690" time="632.016" type="appl" />
         <tli id="T691" time="633.4770395724968" />
         <tli id="T692" time="634.47" type="appl" />
         <tli id="T693" time="635.479" type="appl" />
         <tli id="T694" time="636.489" type="appl" />
         <tli id="T695" time="637.498" type="appl" />
         <tli id="T696" time="638.508" type="appl" />
         <tli id="T697" time="639.518" type="appl" />
         <tli id="T698" time="640.527" type="appl" />
         <tli id="T699" time="641.537" type="appl" />
         <tli id="T700" time="642.429" type="appl" />
         <tli id="T701" time="643.138" type="appl" />
         <tli id="T702" time="643.847" type="appl" />
         <tli id="T703" time="644.556" type="appl" />
         <tli id="T704" time="645.266" type="appl" />
         <tli id="T705" time="645.975" type="appl" />
         <tli id="T706" time="646.684" type="appl" />
         <tli id="T707" time="647.393" type="appl" />
         <tli id="T708" time="649.72363031399" />
         <tli id="T709" time="650.55" type="appl" />
         <tli id="T710" time="651.355" type="appl" />
         <tli id="T711" time="652.16" type="appl" />
         <tli id="T712" time="653.1769475089402" />
         <tli id="T713" time="653.973" type="appl" />
         <tli id="T714" time="654.667" type="appl" />
         <tli id="T715" time="655.36" type="appl" />
         <tli id="T716" time="656.054" type="appl" />
         <tli id="T717" time="656.747" type="appl" />
         <tli id="T718" time="657.44" type="appl" />
         <tli id="T719" time="658.134" type="appl" />
         <tli id="T720" time="658.827" type="appl" />
         <tli id="T721" time="659.807" type="appl" />
         <tli id="T722" time="660.555" type="appl" />
         <tli id="T723" time="661.302" type="appl" />
         <tli id="T724" time="662.05" type="appl" />
         <tli id="T725" time="662.792" type="appl" />
         <tli id="T726" time="663.525" type="appl" />
         <tli id="T727" time="664.257" type="appl" />
         <tli id="T728" time="664.99" type="appl" />
         <tli id="T729" time="665.408" type="appl" />
         <tli id="T730" time="665.805" type="appl" />
         <tli id="T731" time="666.202" type="appl" />
         <tli id="T732" time="666.9168832981144" />
         <tli id="T733" time="668.0302114285328" />
         <tli id="T734" time="668.842" type="appl" />
         <tli id="T735" time="669.548" type="appl" />
         <tli id="T736" time="670.255" type="appl" />
         <tli id="T737" time="670.962" type="appl" />
         <tli id="T738" time="671.668" type="appl" />
         <tli id="T739" time="672.375" type="appl" />
         <tli id="T740" time="673.313" type="appl" />
         <tli id="T741" time="674.156" type="appl" />
         <tli id="T742" time="674.999" type="appl" />
         <tli id="T743" time="675.841" type="appl" />
         <tli id="T744" time="676.684" type="appl" />
         <tli id="T745" time="677.527" type="appl" />
         <tli id="T746" time="678.37" type="appl" />
         <tli id="T747" time="679.263492265388" />
         <tli id="T748" time="679.972" type="appl" />
         <tli id="T749" time="680.889" type="appl" />
         <tli id="T750" time="681.806" type="appl" />
         <tli id="T751" time="682.5368103015279" />
         <tli id="T752" time="683.584" type="appl" />
         <tli id="T753" time="684.414" type="appl" />
         <tli id="T754" time="685.244" type="appl" />
         <tli id="T755" time="686.1434601132424" />
         <tli id="T756" time="687.106" type="appl" />
         <tli id="T757" time="688.132" type="appl" />
         <tli id="T758" time="689.158" type="appl" />
         <tli id="T759" time="690.184" type="appl" />
         <tli id="T760" time="691.3501024477135" />
         <tli id="T761" time="692.285" type="appl" />
         <tli id="T762" time="693.08" type="appl" />
         <tli id="T763" time="693.875" type="appl" />
         <tli id="T764" time="698.396736183355" />
         <tli id="T765" time="699.255" type="appl" />
         <tli id="T766" time="700.6167258086801" />
         <tli id="T767" time="701.8233868362591" />
         <tli id="T768" time="702.772" type="appl" />
         <tli id="T769" time="703.455" type="appl" />
         <tli id="T770" time="704.139" type="appl" />
         <tli id="T771" time="704.822" type="appl" />
         <tli id="T772" time="705.505" type="appl" />
         <tli id="T773" time="706.188" type="appl" />
         <tli id="T774" time="706.872" type="appl" />
         <tli id="T775" time="707.555" type="appl" />
         <tli id="T776" time="708.584" type="appl" />
         <tli id="T777" time="709.571" type="appl" />
         <tli id="T778" time="710.557" type="appl" />
         <tli id="T779" time="711.544" type="appl" />
         <tli id="T780" time="712.9166683273733" />
         <tli id="T781" time="713.542" type="appl" />
         <tli id="T782" time="714.274" type="appl" />
         <tli id="T783" time="715.006" type="appl" />
         <tli id="T784" time="715.738" type="appl" />
         <tli id="T785" time="716.543318045622" />
         <tli id="T786" time="717.207" type="appl" />
         <tli id="T787" time="717.939" type="appl" />
         <tli id="T788" time="718.671" type="appl" />
         <tli id="T789" time="719.403" type="appl" />
         <tli id="T790" time="720.135" type="appl" />
         <tli id="T791" time="721.38" type="appl" />
         <tli id="T792" time="722.625" type="appl" />
         <tli id="T793" time="724.0232830895102" />
         <tli id="T794" time="724.983" type="appl" />
         <tli id="T795" time="725.847" type="appl" />
         <tli id="T796" time="726.71" type="appl" />
         <tli id="T797" time="727.574" type="appl" />
         <tli id="T798" time="728.437" type="appl" />
         <tli id="T799" time="729.301" type="appl" />
         <tli id="T800" time="730.164" type="appl" />
         <tli id="T801" time="731.028" type="appl" />
         <tli id="T802" time="731.9376731865335" />
         <tli id="T803" time="733.142" type="appl" />
         <tli id="T804" time="734.379" type="appl" />
         <tli id="T805" time="735.617" type="appl" />
         <tli id="T806" time="736.855" type="appl" />
         <tli id="T807" time="738.092" type="appl" />
         <tli id="T808" time="739.4099820162016" />
         <tli id="T809" time="740.002" type="appl" />
         <tli id="T810" time="740.654" type="appl" />
         <tli id="T811" time="741.306" type="appl" />
         <tli id="T812" time="741.958" type="appl" />
         <tli id="T813" time="742.3966868084375" />
         <tli id="T814" time="744.3098549509644" />
         <tli id="T815" time="745.1565176609233" />
         <tli id="T816" time="746.027" type="appl" />
         <tli id="T817" time="746.783" type="appl" />
         <tli id="T818" time="747.54" type="appl" />
         <tli id="T819" time="748.297" type="appl" />
         <tli id="T820" time="749.053" type="appl" />
         <tli id="T821" time="750.0098283132857" />
         <tli id="T822" time="751.5764876584851" />
         <tli id="T823" time="752.892" type="appl" />
         <tli id="T824" time="753.878" type="appl" />
         <tli id="T825" time="754.865" type="appl" />
         <tli id="T826" time="755.712" type="appl" />
         <tli id="T827" time="756.505" type="appl" />
         <tli id="T828" time="757.298" type="appl" />
         <tli id="T829" time="758.3231227961096" />
         <tli id="T830" time="759.176" type="appl" />
         <tli id="T831" time="759.912" type="appl" />
         <tli id="T832" time="760.648" type="appl" />
         <tli id="T833" time="761.384" type="appl" />
         <tli id="T834" time="762.3164374674902" />
         <tli id="T835" time="763.42" type="appl" />
         <tli id="T836" time="765.1964240084526" />
         <tli id="T837" time="766.145" type="appl" />
         <tli id="T838" time="767.4697467178695" />
         <tli id="T839" time="768.373" type="appl" />
         <tli id="T840" time="769.107" type="appl" />
         <tli id="T841" time="769.84" type="appl" />
         <tli id="T842" time="770.985" type="appl" />
         <tli id="T843" time="773.3363859679779" />
         <tli id="T844" time="774.186" type="appl" />
         <tli id="T845" time="775.002" type="appl" />
         <tli id="T846" time="775.818" type="appl" />
         <tli id="T847" time="776.634" type="appl" />
         <tli id="T848" time="777.476" type="appl" />
         <tli id="T849" time="778.213" type="appl" />
         <tli id="T850" time="778.949" type="appl" />
         <tli id="T851" time="779.685" type="appl" />
         <tli id="T852" time="780.422" type="appl" />
         <tli id="T853" time="781.158" type="appl" />
         <tli id="T854" time="781.895" type="appl" />
         <tli id="T855" time="782.631" type="appl" />
         <tli id="T856" time="783.367" type="appl" />
         <tli id="T857" time="784.104" type="appl" />
         <tli id="T858" time="785.2429969914933" />
         <tli id="T859" time="786.425" type="appl" />
         <tli id="T860" time="788.0429839063178" />
         <tli id="T861" time="789.173" type="appl" />
         <tli id="T862" time="790.057" type="appl" />
         <tli id="T863" time="790.94" type="appl" />
         <tli id="T864" time="791.823" type="appl" />
         <tli id="T865" time="792.707" type="appl" />
         <tli id="T866" time="793.59" type="appl" />
         <tli id="T867" time="795.14" type="appl" />
         <tli id="T868" time="796.595" type="appl" />
         <tli id="T869" time="798.05" type="appl" />
         <tli id="T870" time="798.915" type="appl" />
         <tli id="T871" time="799.52" type="appl" />
         <tli id="T872" time="800.126" type="appl" />
         <tli id="T873" time="800.731" type="appl" />
         <tli id="T874" time="801.336" type="appl" />
         <tli id="T875" time="801.941" type="appl" />
         <tli id="T876" time="802.546" type="appl" />
         <tli id="T877" time="803.152" type="appl" />
         <tli id="T878" time="803.757" type="appl" />
         <tli id="T879" time="806.069566329378" />
         <tli id="T880" time="806.897" type="appl" />
         <tli id="T881" time="807.923" type="appl" />
         <tli id="T882" time="808.95" type="appl" />
         <tli id="T883" time="809.977" type="appl" />
         <tli id="T884" time="811.003" type="appl" />
         <tli id="T885" time="812.03" type="appl" />
         <tli id="T886" time="813.057" type="appl" />
         <tli id="T887" time="814.083" type="appl" />
         <tli id="T888" time="815.11" type="appl" />
         <tli id="T889" time="816.137" type="appl" />
         <tli id="T890" time="817.163" type="appl" />
         <tli id="T891" time="819.0228391281968" />
         <tli id="T892" time="819.882" type="appl" />
         <tli id="T893" time="820.765" type="appl" />
         <tli id="T894" time="821.648" type="appl" />
         <tli id="T895" time="824.2161481916451" />
         <tli id="T896" time="825.562" type="appl" />
         <tli id="T897" time="826.613" type="appl" />
         <tli id="T898" time="827.664" type="appl" />
         <tli id="T899" time="828.716" type="appl" />
         <tli id="T900" time="829.896" type="appl" />
         <tli id="T901" time="831.076" type="appl" />
         <tli id="T902" time="832.256" type="appl" />
         <tli id="T903" time="833.437" type="appl" />
         <tli id="T904" time="834.617" type="appl" />
         <tli id="T905" time="835.8636458427412" />
         <tli id="T906" time="836.661" type="appl" />
         <tli id="T907" time="837.519" type="appl" />
         <tli id="T908" time="838.377" type="appl" />
         <tli id="T909" time="839.236" type="appl" />
         <tli id="T910" time="840.094" type="appl" />
         <tli id="T911" time="840.952" type="appl" />
         <tli id="T912" time="841.81" type="appl" />
         <tli id="T913" time="843.346" type="appl" />
         <tli id="T914" time="844.325" type="appl" />
         <tli id="T915" time="845.305" type="appl" />
         <tli id="T916" time="847.182707528717" />
         <tli id="T917" time="847.898" type="appl" />
         <tli id="T918" time="848.7760334159623" />
         <tli id="T919" time="850.1026938827481" />
         <tli id="T920" time="850.819" type="appl" />
         <tli id="T921" time="851.425" type="appl" />
         <tli id="T922" time="852.03" type="appl" />
         <tli id="T923" time="852.636" type="appl" />
         <tli id="T924" time="853.241" type="appl" />
         <tli id="T925" time="853.959" type="appl" />
         <tli id="T926" time="854.591" type="appl" />
         <tli id="T927" time="855.222" type="appl" />
         <tli id="T928" time="855.853" type="appl" />
         <tli id="T929" time="856.484" type="appl" />
         <tli id="T930" time="857.116" type="appl" />
         <tli id="T931" time="857.989323692837" />
         <tli id="T932" time="858.692" type="appl" />
         <tli id="T933" time="859.273" type="appl" />
         <tli id="T934" time="859.854" type="appl" />
         <tli id="T935" time="860.435" type="appl" />
         <tli id="T936" time="861.744" type="appl" />
         <tli id="T937" time="862.937" type="appl" />
         <tli id="T938" time="864.2626277091462" />
         <tli id="T939" time="865.809287147811" />
         <tli id="T940" time="866.728" type="appl" />
         <tli id="T941" time="867.502" type="appl" />
         <tli id="T942" time="868.276" type="appl" />
         <tli id="T943" time="869.051" type="appl" />
         <tli id="T944" time="869.825" type="appl" />
         <tli id="T945" time="870.599" type="appl" />
         <tli id="T946" time="871.373" type="appl" />
         <tli id="T947" time="872.529" type="appl" />
         <tli id="T948" time="873.523" type="appl" />
         <tli id="T949" time="874.518" type="appl" />
         <tli id="T950" time="875.512" type="appl" />
         <tli id="T951" time="876.507" type="appl" />
         <tli id="T952" time="878.0158967693433" />
         <tli id="T953" time="878.944" type="appl" />
         <tli id="T954" time="879.653" type="appl" />
         <tli id="T955" time="880.362" type="appl" />
         <tli id="T956" time="880.994" type="appl" />
         <tli id="T957" time="881.626" type="appl" />
         <tli id="T958" time="882.259" type="appl" />
         <tli id="T959" time="882.891" type="appl" />
         <tli id="T960" time="883.523" type="appl" />
         <tli id="T961" time="884.155" type="appl" />
         <tli id="T962" time="884.788" type="appl" />
         <tli id="T963" time="885.42" type="appl" />
         <tli id="T964" time="886.052" type="appl" />
         <tli id="T965" time="886.684" type="appl" />
         <tli id="T966" time="887.317" type="appl" />
         <tli id="T967" time="887.949" type="appl" />
         <tli id="T968" time="888.9625122792047" />
         <tli id="T969" time="890.2291730263871" />
         <tli id="T970" time="891.107" type="appl" />
         <tli id="T971" time="891.887" type="appl" />
         <tli id="T972" time="892.666" type="appl" />
         <tli id="T973" time="893.445" type="appl" />
         <tli id="T974" time="894.225" type="appl" />
         <tli id="T975" time="895.004" type="appl" />
         <tli id="T976" time="895.783" type="appl" />
         <tli id="T977" time="896.562" type="appl" />
         <tli id="T978" time="897.342" type="appl" />
         <tli id="T979" time="898.121" type="appl" />
         <tli id="T980" time="898.9" type="appl" />
         <tli id="T981" time="899.68" type="appl" />
         <tli id="T982" time="900.459" type="appl" />
         <tli id="T983" time="901.246" type="appl" />
         <tli id="T984" time="902.032" type="appl" />
         <tli id="T985" time="902.818" type="appl" />
         <tli id="T986" time="903.605" type="appl" />
         <tli id="T987" time="904.392" type="appl" />
         <tli id="T988" time="905.178" type="appl" />
         <tli id="T989" time="905.964" type="appl" />
         <tli id="T990" time="906.751" type="appl" />
         <tli id="T991" time="907.537" type="appl" />
         <tli id="T992" time="908.324" type="appl" />
         <tli id="T993" time="909.24" type="appl" />
         <tli id="T994" time="910.097" type="appl" />
         <tli id="T995" time="910.953" type="appl" />
         <tli id="T996" time="911.81" type="appl" />
         <tli id="T997" time="912.8490673168617" />
         <tli id="T998" time="914.186" type="appl" />
         <tli id="T999" time="915.315" type="appl" />
         <tli id="T1000" time="916.444" type="appl" />
         <tli id="T1001" time="917.572" type="appl" />
         <tli id="T1002" time="918.298" type="appl" />
         <tli id="T1003" time="918.989" type="appl" />
         <tli id="T1004" time="919.68" type="appl" />
         <tli id="T1005" time="920.372" type="appl" />
         <tli id="T1006" time="921.063" type="appl" />
         <tli id="T1007" time="921.754" type="appl" />
         <tli id="T1008" time="922.3890227337993" />
         <tli id="T1009" time="922.795" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T453" start="T451">
            <tli id="T451.tx.1" />
         </timeline-fork>
         <timeline-fork end="T498" start="T497">
            <tli id="T497.tx.1" />
         </timeline-fork>
         <timeline-fork end="T766" start="T764">
            <tli id="T764.tx.1" />
         </timeline-fork>
         <timeline-fork end="T932" start="T931">
            <tli id="T931.tx.1" />
         </timeline-fork>
         <timeline-fork end="T985" start="T984">
            <tli id="T984.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T1008" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Măn</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">teinen</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">šiʔnʼileʔ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_12" n="HIAT:ip">(</nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">kono-</ts>
                  <nts id="Seg_15" n="HIAT:ip">)</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_18" n="HIAT:w" s="T4">kudolbiam</ts>
                  <nts id="Seg_19" n="HIAT:ip">.</nts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_22" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_24" n="HIAT:w" s="T5">Šobilaʔ</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_27" n="HIAT:w" s="T6">dʼăbaktərzittə</ts>
                  <nts id="Seg_28" n="HIAT:ip">,</nts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_30" n="HIAT:ip">(</nts>
                  <ts e="T8" id="Seg_32" n="HIAT:w" s="T7">a</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_35" n="HIAT:w" s="T8">ĭmbidə</ts>
                  <nts id="Seg_36" n="HIAT:ip">)</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_39" n="HIAT:w" s="T9">ej</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_42" n="HIAT:w" s="T10">nʼilgölaʔbəleʔ</ts>
                  <nts id="Seg_43" n="HIAT:ip">.</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_46" n="HIAT:u" s="T11">
                  <nts id="Seg_47" n="HIAT:ip">(</nts>
                  <ts e="T12" id="Seg_49" n="HIAT:w" s="T11">Ej</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_52" n="HIAT:w" s="T12">măndə-</ts>
                  <nts id="Seg_53" n="HIAT:ip">)</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_56" n="HIAT:w" s="T13">Ej</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_59" n="HIAT:w" s="T14">măndəlialaʔ</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_63" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_65" n="HIAT:w" s="T15">Радио</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_68" n="HIAT:w" s="T16">teinen</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_71" n="HIAT:w" s="T17">dʼăbaktərbi</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_74" n="HIAT:w" s="T18">jakše</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_77" n="HIAT:w" s="T19">передача</ts>
                  <nts id="Seg_78" n="HIAT:ip">,</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_81" n="HIAT:w" s="T20">dʼăbaktərbi</ts>
                  <nts id="Seg_82" n="HIAT:ip">.</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_85" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_87" n="HIAT:w" s="T21">Măn</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_89" n="HIAT:ip">(</nts>
                  <ts e="T23" id="Seg_91" n="HIAT:w" s="T22">nʼilgöbiam</ts>
                  <nts id="Seg_92" n="HIAT:ip">)</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_95" n="HIAT:w" s="T23">da</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_98" n="HIAT:w" s="T24">ej</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_101" n="HIAT:w" s="T25">bar</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_103" n="HIAT:ip">(</nts>
                  <nts id="Seg_104" n="HIAT:ip">(</nts>
                  <ats e="T27" id="Seg_105" n="HIAT:non-pho" s="T26">PAUSE</ats>
                  <nts id="Seg_106" n="HIAT:ip">)</nts>
                  <nts id="Seg_107" n="HIAT:ip">)</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_110" n="HIAT:w" s="T27">üge</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_113" n="HIAT:w" s="T28">togonoriam</ts>
                  <nts id="Seg_114" n="HIAT:ip">,</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_117" n="HIAT:w" s="T29">nʼilgösʼtə</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_119" n="HIAT:ip">(</nts>
                  <ts e="T31" id="Seg_121" n="HIAT:w" s="T30">na-</ts>
                  <nts id="Seg_122" n="HIAT:ip">)</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_125" n="HIAT:w" s="T31">kădedə</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_128" n="HIAT:w" s="T32">ej</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_131" n="HIAT:w" s="T33">moliam</ts>
                  <nts id="Seg_132" n="HIAT:ip">.</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_135" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_137" n="HIAT:w" s="T34">Šindidə</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_140" n="HIAT:w" s="T35">dʼăbaktərlaʔbə</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_143" n="HIAT:w" s="T36">sĭrezi</ts>
                  <nts id="Seg_144" n="HIAT:ip">.</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_147" n="HIAT:u" s="T37">
                  <nts id="Seg_148" n="HIAT:ip">"</nts>
                  <ts e="T38" id="Seg_150" n="HIAT:w" s="T37">Tăn</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_153" n="HIAT:w" s="T38">iʔbəʔ</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_156" n="HIAT:w" s="T39">dʼünə</ts>
                  <nts id="Seg_157" n="HIAT:ip">,</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_160" n="HIAT:w" s="T40">iʔboʔ</ts>
                  <nts id="Seg_161" n="HIAT:ip">,</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_164" n="HIAT:w" s="T41">iʔ</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_167" n="HIAT:w" s="T42">dʼăbaktəraʔ</ts>
                  <nts id="Seg_168" n="HIAT:ip">,</nts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_171" n="HIAT:w" s="T43">iʔ</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_174" n="HIAT:w" s="T44">kirgaraʔ</ts>
                  <nts id="Seg_175" n="HIAT:ip">"</nts>
                  <nts id="Seg_176" n="HIAT:ip">.</nts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_179" n="HIAT:u" s="T45">
                  <nts id="Seg_180" n="HIAT:ip">"</nts>
                  <ts e="T46" id="Seg_182" n="HIAT:w" s="T45">A</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_185" n="HIAT:w" s="T46">ĭmbi</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_188" n="HIAT:w" s="T47">măn</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_191" n="HIAT:w" s="T48">ej</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_194" n="HIAT:w" s="T49">dʼăbaktərlam</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_197" n="HIAT:w" s="T50">da</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_200" n="HIAT:w" s="T51">ej</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_203" n="HIAT:w" s="T52">kirgarlam</ts>
                  <nts id="Seg_204" n="HIAT:ip">?</nts>
                  <nts id="Seg_205" n="HIAT:ip">"</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_208" n="HIAT:u" s="T53">
                  <nts id="Seg_209" n="HIAT:ip">"</nts>
                  <ts e="T54" id="Seg_211" n="HIAT:w" s="T53">Da</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_213" n="HIAT:ip">(</nts>
                  <ts e="T55" id="Seg_215" n="HIAT:w" s="T54">ko-</ts>
                  <nts id="Seg_216" n="HIAT:ip">)</nts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_219" n="HIAT:w" s="T55">kondʼo</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_222" n="HIAT:w" s="T56">amnolal</ts>
                  <nts id="Seg_223" n="HIAT:ip">,</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_226" n="HIAT:w" s="T57">ej</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_229" n="HIAT:w" s="T58">külel</ts>
                  <nts id="Seg_230" n="HIAT:ip">.</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_233" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_235" n="HIAT:w" s="T59">Jakše</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_238" n="HIAT:w" s="T60">moləj</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_241" n="HIAT:w" s="T61">tănan</ts>
                  <nts id="Seg_242" n="HIAT:ip">.</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_245" n="HIAT:u" s="T62">
                  <nts id="Seg_246" n="HIAT:ip">(</nts>
                  <ts e="T63" id="Seg_248" n="HIAT:w" s="T62">Iʔboʔ</ts>
                  <nts id="Seg_249" n="HIAT:ip">)</nts>
                  <nts id="Seg_250" n="HIAT:ip">,</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_253" n="HIAT:w" s="T63">iʔboʔ</ts>
                  <nts id="Seg_254" n="HIAT:ip">"</nts>
                  <nts id="Seg_255" n="HIAT:ip">.</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_258" n="HIAT:u" s="T64">
                  <nts id="Seg_259" n="HIAT:ip">(</nts>
                  <ts e="T65" id="Seg_261" n="HIAT:w" s="T64">Dĭgəttə</ts>
                  <nts id="Seg_262" n="HIAT:ip">)</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_265" n="HIAT:w" s="T65">šindidə</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_268" n="HIAT:w" s="T66">bar</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_271" n="HIAT:w" s="T67">nʼergöluʔpi</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_274" n="HIAT:w" s="T68">dĭm</ts>
                  <nts id="Seg_275" n="HIAT:ip">.</nts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_278" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_280" n="HIAT:w" s="T69">I</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_282" n="HIAT:ip">(</nts>
                  <ts e="T71" id="Seg_284" n="HIAT:w" s="T70">üžümbi</ts>
                  <nts id="Seg_285" n="HIAT:ip">)</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_288" n="HIAT:w" s="T71">dĭm</ts>
                  <nts id="Seg_289" n="HIAT:ip">.</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_292" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_294" n="HIAT:w" s="T72">Dĭgəttə</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_297" n="HIAT:w" s="T73">šišəge</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_300" n="HIAT:w" s="T74">dĭm</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_303" n="HIAT:w" s="T75">ugandə</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_306" n="HIAT:w" s="T76">tăŋ</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_309" n="HIAT:w" s="T77">kămbi</ts>
                  <nts id="Seg_310" n="HIAT:ip">,</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_313" n="HIAT:w" s="T78">kămbi</ts>
                  <nts id="Seg_314" n="HIAT:ip">.</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_317" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_319" n="HIAT:w" s="T79">A</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_322" n="HIAT:w" s="T80">dĭgəttə</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_325" n="HIAT:w" s="T81">ejü</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_328" n="HIAT:w" s="T82">molambi</ts>
                  <nts id="Seg_329" n="HIAT:ip">,</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_332" n="HIAT:w" s="T83">kuja</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_335" n="HIAT:w" s="T84">uʔbdəbi</ts>
                  <nts id="Seg_336" n="HIAT:ip">.</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_339" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_341" n="HIAT:w" s="T85">Nʼuʔdə</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_344" n="HIAT:w" s="T86">uʔbdəbi</ts>
                  <nts id="Seg_345" n="HIAT:ip">,</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_348" n="HIAT:w" s="T87">sĭre</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_351" n="HIAT:w" s="T88">bar</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_354" n="HIAT:w" s="T89">nereʔluʔpi</ts>
                  <nts id="Seg_355" n="HIAT:ip">.</nts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_358" n="HIAT:u" s="T90">
                  <nts id="Seg_359" n="HIAT:ip">(</nts>
                  <ts e="T91" id="Seg_361" n="HIAT:w" s="T90">I</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_364" n="HIAT:w" s="T91">dĭgəttə</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_367" n="HIAT:w" s="T92">bar</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_370" n="HIAT:w" s="T93">ma-</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_373" n="HIAT:w" s="T94">нача</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_376" n="HIAT:w" s="T95">-</ts>
                  <nts id="Seg_377" n="HIAT:ip">)</nts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_380" n="HIAT:w" s="T96">Dĭgəttə</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_383" n="HIAT:w" s="T97">bar</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_386" n="HIAT:w" s="T98">таять</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_389" n="HIAT:w" s="T99">начал</ts>
                  <nts id="Seg_390" n="HIAT:ip">.</nts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_393" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_395" n="HIAT:w" s="T100">Bü</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_398" n="HIAT:w" s="T101">mʼaŋŋaʔbə</ts>
                  <nts id="Seg_399" n="HIAT:ip">.</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_402" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_404" n="HIAT:w" s="T102">I</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_407" n="HIAT:w" s="T103">sĭre</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_410" n="HIAT:w" s="T104">külambi</ts>
                  <nts id="Seg_411" n="HIAT:ip">.</nts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_414" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_416" n="HIAT:w" s="T105">Лазарь</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_419" n="HIAT:w" s="T106">bar</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_422" n="HIAT:w" s="T107">jezirerluʔpi</ts>
                  <nts id="Seg_423" n="HIAT:ip">.</nts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_426" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_428" n="HIAT:w" s="T108">I</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_431" n="HIAT:w" s="T109">bar</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_433" n="HIAT:ip">(</nts>
                  <ts e="T111" id="Seg_435" n="HIAT:w" s="T110">ananugosʼtə</ts>
                  <nts id="Seg_436" n="HIAT:ip">)</nts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_439" n="HIAT:w" s="T111">ibi</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_442" n="HIAT:w" s="T112">udandə</ts>
                  <nts id="Seg_443" n="HIAT:ip">.</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_446" n="HIAT:u" s="T113">
                  <nts id="Seg_447" n="HIAT:ip">"</nts>
                  <ts e="T114" id="Seg_449" n="HIAT:w" s="T113">Tüjö</ts>
                  <nts id="Seg_450" n="HIAT:ip">,</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_453" n="HIAT:w" s="T114">măndə</ts>
                  <nts id="Seg_454" n="HIAT:ip">,</nts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_457" n="HIAT:w" s="T115">toʔnarlam</ts>
                  <nts id="Seg_458" n="HIAT:ip">"</nts>
                  <nts id="Seg_459" n="HIAT:ip">.</nts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_462" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_464" n="HIAT:w" s="T116">Măn</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_467" n="HIAT:w" s="T117">iam</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_469" n="HIAT:ip">(</nts>
                  <ts e="T119" id="Seg_471" n="HIAT:w" s="T118">ešši</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_474" n="HIAT:w" s="T119">deʔpi=</ts>
                  <nts id="Seg_475" n="HIAT:ip">)</nts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_478" n="HIAT:w" s="T120">nʼi</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_481" n="HIAT:w" s="T121">deʔpi</ts>
                  <nts id="Seg_482" n="HIAT:ip">,</nts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_485" n="HIAT:w" s="T122">turagən</ts>
                  <nts id="Seg_486" n="HIAT:ip">.</nts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_489" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_491" n="HIAT:w" s="T123">Bar</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_494" n="HIAT:w" s="T124">ĭzembi</ts>
                  <nts id="Seg_495" n="HIAT:ip">,</nts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_498" n="HIAT:w" s="T125">kirgarbi</ts>
                  <nts id="Seg_499" n="HIAT:ip">.</nts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_502" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_504" n="HIAT:w" s="T126">Urgaja</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_507" n="HIAT:w" s="T127">ibi</ts>
                  <nts id="Seg_508" n="HIAT:ip">.</nts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_511" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_513" n="HIAT:w" s="T128">Dĭ</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_516" n="HIAT:w" s="T129">deʔpi</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_518" n="HIAT:ip">(</nts>
                  <ts e="T131" id="Seg_520" n="HIAT:w" s="T130">ets-</ts>
                  <nts id="Seg_521" n="HIAT:ip">)</nts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_524" n="HIAT:w" s="T131">ešši</ts>
                  <nts id="Seg_525" n="HIAT:ip">.</nts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_528" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_530" n="HIAT:w" s="T132">Dĭ</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_533" n="HIAT:w" s="T133">bar</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_535" n="HIAT:ip">(</nts>
                  <ts e="T135" id="Seg_537" n="HIAT:w" s="T134">băʔ-</ts>
                  <nts id="Seg_538" n="HIAT:ip">)</nts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_541" n="HIAT:w" s="T135">băʔtəbi</ts>
                  <nts id="Seg_542" n="HIAT:ip">,</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_545" n="HIAT:w" s="T136">sarbi</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_548" n="HIAT:w" s="T137">i</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_551" n="HIAT:w" s="T138">băzəbi</ts>
                  <nts id="Seg_552" n="HIAT:ip">.</nts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_555" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_557" n="HIAT:w" s="T139">Dĭgəttə</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_559" n="HIAT:ip">(</nts>
                  <ts e="T142" id="Seg_561" n="HIAT:w" s="T140">ku-</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_563" n="HIAT:ip">)</nts>
                  <nts id="Seg_564" n="HIAT:ip">…</nts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_567" n="HIAT:u" s="T142">
                  <nts id="Seg_568" n="HIAT:ip">(</nts>
                  <nts id="Seg_569" n="HIAT:ip">(</nts>
                  <ats e="T143" id="Seg_570" n="HIAT:non-pho" s="T142">BRK</ats>
                  <nts id="Seg_571" n="HIAT:ip">)</nts>
                  <nts id="Seg_572" n="HIAT:ip">)</nts>
                  <nts id="Seg_573" n="HIAT:ip">.</nts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_576" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_578" n="HIAT:w" s="T143">Dĭgəttə</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_581" n="HIAT:w" s="T144">kubi</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_584" n="HIAT:w" s="T145">trʼapkaʔi</ts>
                  <nts id="Seg_585" n="HIAT:ip">,</nts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_588" n="HIAT:w" s="T146">kürbi</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_591" n="HIAT:w" s="T147">dĭm</ts>
                  <nts id="Seg_592" n="HIAT:ip">.</nts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_595" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_597" n="HIAT:w" s="T148">I</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_600" n="HIAT:w" s="T149">embi</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_603" n="HIAT:w" s="T150">pʼeštə</ts>
                  <nts id="Seg_604" n="HIAT:ip">.</nts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_607" n="HIAT:u" s="T151">
                  <ts e="T152" id="Seg_609" n="HIAT:w" s="T151">Dĭ</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_612" n="HIAT:w" s="T152">bar</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_615" n="HIAT:w" s="T153">iʔbolaʔbə</ts>
                  <nts id="Seg_616" n="HIAT:ip">.</nts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_619" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_621" n="HIAT:w" s="T154">A</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_624" n="HIAT:w" s="T155">dĭm</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_626" n="HIAT:ip">(</nts>
                  <ts e="T157" id="Seg_628" n="HIAT:w" s="T156">numəj-</ts>
                  <nts id="Seg_629" n="HIAT:ip">)</nts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_632" n="HIAT:w" s="T157">numəjleʔbəʔjə</ts>
                  <nts id="Seg_633" n="HIAT:ip">.</nts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_636" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_638" n="HIAT:w" s="T158">Dĭgəttə</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_641" n="HIAT:w" s="T159">kumbiʔi</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_644" n="HIAT:w" s="T160">abəstə</ts>
                  <nts id="Seg_645" n="HIAT:ip">.</nts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T164" id="Seg_648" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_650" n="HIAT:w" s="T161">I</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_653" n="HIAT:w" s="T162">numəjleʔbəʔjə</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_656" n="HIAT:w" s="T163">Максим</ts>
                  <nts id="Seg_657" n="HIAT:ip">.</nts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T174" id="Seg_660" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_662" n="HIAT:w" s="T164">A</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_665" n="HIAT:w" s="T165">măn</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_668" n="HIAT:w" s="T166">iam</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_671" n="HIAT:w" s="T167">dĭm</ts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_674" n="HIAT:w" s="T168">deʔpi</ts>
                  <nts id="Seg_675" n="HIAT:ip">,</nts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_678" n="HIAT:w" s="T169">măna</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_681" n="HIAT:w" s="T170">bʼeʔ</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_684" n="HIAT:w" s="T171">šide</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_687" n="HIAT:w" s="T172">kö</ts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_690" n="HIAT:w" s="T173">ibi</ts>
                  <nts id="Seg_691" n="HIAT:ip">.</nts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_694" n="HIAT:u" s="T174">
                  <ts e="T175" id="Seg_696" n="HIAT:w" s="T174">Dĭgəttə</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_699" n="HIAT:w" s="T175">dĭ</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_702" n="HIAT:w" s="T176">Максим</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_705" n="HIAT:w" s="T177">özerbi</ts>
                  <nts id="Seg_706" n="HIAT:ip">,</nts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_709" n="HIAT:w" s="T178">özerbi</ts>
                  <nts id="Seg_710" n="HIAT:ip">.</nts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T187" id="Seg_713" n="HIAT:u" s="T179">
                  <nts id="Seg_714" n="HIAT:ip">(</nts>
                  <ts e="T180" id="Seg_716" n="HIAT:w" s="T179">Bʼeʔ</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_719" n="HIAT:w" s="T180">šide=</ts>
                  <nts id="Seg_720" n="HIAT:ip">)</nts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_723" n="HIAT:w" s="T181">Bʼeʔ</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_726" n="HIAT:w" s="T182">šide</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_729" n="HIAT:w" s="T183">kö</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_732" n="HIAT:w" s="T184">ibi</ts>
                  <nts id="Seg_733" n="HIAT:ip">,</nts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_736" n="HIAT:w" s="T185">iat</ts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_739" n="HIAT:w" s="T186">külambi</ts>
                  <nts id="Seg_740" n="HIAT:ip">.</nts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T189" id="Seg_743" n="HIAT:u" s="T187">
                  <ts e="T188" id="Seg_745" n="HIAT:w" s="T187">Dĭ</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_748" n="HIAT:w" s="T188">maːbi</ts>
                  <nts id="Seg_749" n="HIAT:ip">.</nts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_752" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_754" n="HIAT:w" s="T189">A</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_756" n="HIAT:ip">(</nts>
                  <ts e="T191" id="Seg_758" n="HIAT:w" s="T190">Maksimzen</ts>
                  <nts id="Seg_759" n="HIAT:ip">)</nts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_761" n="HIAT:ip">(</nts>
                  <ts e="T192" id="Seg_763" n="HIAT:w" s="T191">ia-</ts>
                  <nts id="Seg_764" n="HIAT:ip">)</nts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_767" n="HIAT:w" s="T192">iam</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_770" n="HIAT:w" s="T193">kuʔpiʔi</ts>
                  <nts id="Seg_771" n="HIAT:ip">,</nts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_774" n="HIAT:w" s="T194">dĭm</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_777" n="HIAT:w" s="T195">kuʔpi</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_780" n="HIAT:w" s="T196">крестник</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_783" n="HIAT:w" s="T197">dĭn</ts>
                  <nts id="Seg_784" n="HIAT:ip">.</nts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T202" id="Seg_787" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_789" n="HIAT:w" s="T198">I</ts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_792" n="HIAT:w" s="T199">dĭ</ts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_795" n="HIAT:w" s="T200">bar</ts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_798" n="HIAT:w" s="T201">külambi</ts>
                  <nts id="Seg_799" n="HIAT:ip">.</nts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T205" id="Seg_802" n="HIAT:u" s="T202">
                  <ts e="T203" id="Seg_804" n="HIAT:w" s="T202">Inezi</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_807" n="HIAT:w" s="T203">maːʔndə</ts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_810" n="HIAT:w" s="T204">deʔpiʔi</ts>
                  <nts id="Seg_811" n="HIAT:ip">.</nts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_814" n="HIAT:u" s="T205">
                  <ts e="T206" id="Seg_816" n="HIAT:w" s="T205">Măn</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_819" n="HIAT:w" s="T206">abam</ts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_822" n="HIAT:w" s="T207">dʼijegən</ts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_825" n="HIAT:w" s="T208">külambi</ts>
                  <nts id="Seg_826" n="HIAT:ip">.</nts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_829" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_831" n="HIAT:w" s="T209">Dĭgəttə</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_834" n="HIAT:w" s="T210">sʼestram</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_836" n="HIAT:ip">(</nts>
                  <ts e="T212" id="Seg_838" n="HIAT:w" s="T211">i</ts>
                  <nts id="Seg_839" n="HIAT:ip">)</nts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_842" n="HIAT:w" s="T212">măn</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_845" n="HIAT:w" s="T213">tibim</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_848" n="HIAT:w" s="T214">šide</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_851" n="HIAT:w" s="T215">inegətsi</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_854" n="HIAT:w" s="T216">deʔpiʔi</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_857" n="HIAT:w" s="T217">maʔnʼibəj</ts>
                  <nts id="Seg_858" n="HIAT:ip">.</nts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T222" id="Seg_861" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_863" n="HIAT:w" s="T218">Măn</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_866" n="HIAT:w" s="T219">ugandə</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_869" n="HIAT:w" s="T220">tăŋ</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_872" n="HIAT:w" s="T221">dʼorbiam</ts>
                  <nts id="Seg_873" n="HIAT:ip">.</nts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_876" n="HIAT:u" s="T222">
                  <ts e="T223" id="Seg_878" n="HIAT:w" s="T222">Urgo</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_881" n="HIAT:w" s="T223">dʼala</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_884" n="HIAT:w" s="T224">ibi</ts>
                  <nts id="Seg_885" n="HIAT:ip">,</nts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_888" n="HIAT:w" s="T225">воскресенье</ts>
                  <nts id="Seg_889" n="HIAT:ip">.</nts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T230" id="Seg_892" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_894" n="HIAT:w" s="T226">Dĭgəttə</ts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_896" n="HIAT:ip">(</nts>
                  <ts e="T228" id="Seg_898" n="HIAT:w" s="T227">dʼü=</ts>
                  <nts id="Seg_899" n="HIAT:ip">)</nts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_902" n="HIAT:w" s="T228">dʼü</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_905" n="HIAT:w" s="T229">tĭlbibeʔ</ts>
                  <nts id="Seg_906" n="HIAT:ip">.</nts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_909" n="HIAT:u" s="T230">
                  <ts e="T231" id="Seg_911" n="HIAT:w" s="T230">Maʔ</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_914" n="HIAT:w" s="T231">abibeʔ</ts>
                  <nts id="Seg_915" n="HIAT:ip">,</nts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_918" n="HIAT:w" s="T232">krospa</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_921" n="HIAT:w" s="T233">abibeʔ</ts>
                  <nts id="Seg_922" n="HIAT:ip">.</nts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_925" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_927" n="HIAT:w" s="T234">I</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_930" n="HIAT:w" s="T235">dʼünə</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_933" n="HIAT:w" s="T236">embibeʔ</ts>
                  <nts id="Seg_934" n="HIAT:ip">,</nts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_937" n="HIAT:w" s="T237">kămnabibaʔ</ts>
                  <nts id="Seg_938" n="HIAT:ip">.</nts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_941" n="HIAT:u" s="T238">
                  <nts id="Seg_942" n="HIAT:ip">(</nts>
                  <ts e="T239" id="Seg_944" n="HIAT:w" s="T238">Dĭgəttə</ts>
                  <nts id="Seg_945" n="HIAT:ip">)</nts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_948" n="HIAT:w" s="T239">bü</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_951" n="HIAT:w" s="T240">bĭʔpibeʔ</ts>
                  <nts id="Seg_952" n="HIAT:ip">,</nts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_955" n="HIAT:w" s="T241">ipek</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_958" n="HIAT:w" s="T242">ambibaʔ</ts>
                  <nts id="Seg_959" n="HIAT:ip">.</nts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T245" id="Seg_962" n="HIAT:u" s="T243">
                  <ts e="T244" id="Seg_964" n="HIAT:w" s="T243">Il</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_967" n="HIAT:w" s="T244">ibiʔi</ts>
                  <nts id="Seg_968" n="HIAT:ip">.</nts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T246" id="Seg_971" n="HIAT:u" s="T245">
                  <ts e="T246" id="Seg_973" n="HIAT:w" s="T245">Kabarləj</ts>
                  <nts id="Seg_974" n="HIAT:ip">.</nts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T252" id="Seg_977" n="HIAT:u" s="T246">
                  <ts e="T247" id="Seg_979" n="HIAT:w" s="T246">Šide</ts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_982" n="HIAT:w" s="T247">nuzaŋ</ts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_985" n="HIAT:w" s="T248">amnobiʔi</ts>
                  <nts id="Seg_986" n="HIAT:ip">,</nts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_989" n="HIAT:w" s="T249">nüke</ts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_992" n="HIAT:w" s="T250">i</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_995" n="HIAT:w" s="T251">büzʼe</ts>
                  <nts id="Seg_996" n="HIAT:ip">.</nts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_999" n="HIAT:u" s="T252">
                  <ts e="T253" id="Seg_1001" n="HIAT:w" s="T252">Ara</ts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_1004" n="HIAT:w" s="T253">bĭʔpiʔi</ts>
                  <nts id="Seg_1005" n="HIAT:ip">,</nts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_1008" n="HIAT:w" s="T254">bar</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1010" n="HIAT:ip">(</nts>
                  <ts e="T256" id="Seg_1012" n="HIAT:w" s="T255">dʼabrobiʔi</ts>
                  <nts id="Seg_1013" n="HIAT:ip">)</nts>
                  <nts id="Seg_1014" n="HIAT:ip">.</nts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_1017" n="HIAT:u" s="T256">
                  <ts e="T257" id="Seg_1019" n="HIAT:w" s="T256">Dĭgəttə</ts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_1022" n="HIAT:w" s="T257">părogəndə</ts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_1025" n="HIAT:w" s="T258">amnobiʔi</ts>
                  <nts id="Seg_1026" n="HIAT:ip">,</nts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1028" n="HIAT:ip">(</nts>
                  <ts e="T260" id="Seg_1030" n="HIAT:w" s="T259">dĭ</ts>
                  <nts id="Seg_1031" n="HIAT:ip">)</nts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_1034" n="HIAT:w" s="T260">ulundə</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1037" n="HIAT:w" s="T261">teʔtə</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1040" n="HIAT:w" s="T262">ibi</ts>
                  <nts id="Seg_1041" n="HIAT:ip">.</nts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T271" id="Seg_1044" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_1046" n="HIAT:w" s="T263">Nüke</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1049" n="HIAT:w" s="T264">i</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1052" n="HIAT:w" s="T265">büzʼet</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1055" n="HIAT:w" s="T266">nʼeʔdolaʔpi</ts>
                  <nts id="Seg_1056" n="HIAT:ip">,</nts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1059" n="HIAT:w" s="T267">nʼiʔdolaʔpi</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1062" n="HIAT:w" s="T268">i</ts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1065" n="HIAT:w" s="T269">dăre</ts>
                  <nts id="Seg_1066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1067" n="HIAT:ip">(</nts>
                  <ts e="T271" id="Seg_1069" n="HIAT:w" s="T270">kunolluʔpiʔi</ts>
                  <nts id="Seg_1070" n="HIAT:ip">)</nts>
                  <nts id="Seg_1071" n="HIAT:ip">.</nts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T273" id="Seg_1074" n="HIAT:u" s="T271">
                  <ts e="T272" id="Seg_1076" n="HIAT:w" s="T271">Ertən</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1079" n="HIAT:w" s="T272">uʔpiʔi</ts>
                  <nts id="Seg_1080" n="HIAT:ip">.</nts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T277" id="Seg_1083" n="HIAT:u" s="T273">
                  <nts id="Seg_1084" n="HIAT:ip">"</nts>
                  <ts e="T274" id="Seg_1086" n="HIAT:w" s="T273">Măn</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1089" n="HIAT:w" s="T274">ĭmbidə</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1092" n="HIAT:w" s="T275">bar</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1095" n="HIAT:w" s="T276">ĭzemniem</ts>
                  <nts id="Seg_1096" n="HIAT:ip">"</nts>
                  <nts id="Seg_1097" n="HIAT:ip">.</nts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T283" id="Seg_1100" n="HIAT:u" s="T277">
                  <ts e="T278" id="Seg_1102" n="HIAT:w" s="T277">A</ts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1105" n="HIAT:w" s="T278">dĭ</ts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1108" n="HIAT:w" s="T279">măndə:</ts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1110" n="HIAT:ip">"</nts>
                  <ts e="T281" id="Seg_1112" n="HIAT:w" s="T280">Măn</ts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1115" n="HIAT:w" s="T281">tože</ts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1118" n="HIAT:w" s="T282">ĭzemniem</ts>
                  <nts id="Seg_1119" n="HIAT:ip">.</nts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T286" id="Seg_1122" n="HIAT:u" s="T283">
                  <ts e="T284" id="Seg_1124" n="HIAT:w" s="T283">Naverna</ts>
                  <nts id="Seg_1125" n="HIAT:ip">,</nts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1128" n="HIAT:w" s="T284">miʔ</ts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1131" n="HIAT:w" s="T285">dʼabrobibaʔ</ts>
                  <nts id="Seg_1132" n="HIAT:ip">.</nts>
                  <nts id="Seg_1133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T287" id="Seg_1135" n="HIAT:u" s="T286">
                  <nts id="Seg_1136" n="HIAT:ip">(</nts>
                  <nts id="Seg_1137" n="HIAT:ip">(</nts>
                  <ats e="T287" id="Seg_1138" n="HIAT:non-pho" s="T286">BRK</ats>
                  <nts id="Seg_1139" n="HIAT:ip">)</nts>
                  <nts id="Seg_1140" n="HIAT:ip">)</nts>
                  <nts id="Seg_1141" n="HIAT:ip">.</nts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T292" id="Seg_1144" n="HIAT:u" s="T287">
                  <ts e="T288" id="Seg_1146" n="HIAT:w" s="T287">Dĭm</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1149" n="HIAT:w" s="T288">kăštəbiʔi</ts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1152" n="HIAT:w" s="T289">Тугойин</ts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1155" n="HIAT:w" s="T290">Василий</ts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1158" n="HIAT:w" s="T291">Микитич</ts>
                  <nts id="Seg_1159" n="HIAT:ip">.</nts>
                  <nts id="Seg_1160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1162" n="HIAT:u" s="T292">
                  <ts e="T293" id="Seg_1164" n="HIAT:w" s="T292">A</ts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1167" n="HIAT:w" s="T293">nükem</ts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1170" n="HIAT:w" s="T294">kăštəbiʔi</ts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1173" n="HIAT:w" s="T295">Vera</ts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1176" n="HIAT:w" s="T296">Gordeevna</ts>
                  <nts id="Seg_1177" n="HIAT:ip">.</nts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T298" id="Seg_1180" n="HIAT:u" s="T297">
                  <nts id="Seg_1181" n="HIAT:ip">(</nts>
                  <nts id="Seg_1182" n="HIAT:ip">(</nts>
                  <ats e="T298" id="Seg_1183" n="HIAT:non-pho" s="T297">BRK</ats>
                  <nts id="Seg_1184" n="HIAT:ip">)</nts>
                  <nts id="Seg_1185" n="HIAT:ip">)</nts>
                  <nts id="Seg_1186" n="HIAT:ip">.</nts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T302" id="Seg_1189" n="HIAT:u" s="T298">
                  <ts e="T299" id="Seg_1191" n="HIAT:w" s="T298">Dĭzen</ts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1194" n="HIAT:w" s="T299">turazaŋdə</ts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1197" n="HIAT:w" s="T300">üdʼüge</ts>
                  <nts id="Seg_1198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1200" n="HIAT:w" s="T301">ibiʔi</ts>
                  <nts id="Seg_1201" n="HIAT:ip">.</nts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T306" id="Seg_1204" n="HIAT:u" s="T302">
                  <ts e="T303" id="Seg_1206" n="HIAT:w" s="T302">I</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1209" n="HIAT:w" s="T303">dĭn</ts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1211" n="HIAT:ip">(</nts>
                  <ts e="T305" id="Seg_1213" n="HIAT:w" s="T304">töštekkən</ts>
                  <nts id="Seg_1214" n="HIAT:ip">)</nts>
                  <nts id="Seg_1215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1217" n="HIAT:w" s="T305">nulaʔpi</ts>
                  <nts id="Seg_1218" n="HIAT:ip">.</nts>
                  <nts id="Seg_1219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T307" id="Seg_1221" n="HIAT:u" s="T306">
                  <nts id="Seg_1222" n="HIAT:ip">(</nts>
                  <nts id="Seg_1223" n="HIAT:ip">(</nts>
                  <ats e="T307" id="Seg_1224" n="HIAT:non-pho" s="T306">BRK</ats>
                  <nts id="Seg_1225" n="HIAT:ip">)</nts>
                  <nts id="Seg_1226" n="HIAT:ip">)</nts>
                  <nts id="Seg_1227" n="HIAT:ip">.</nts>
                  <nts id="Seg_1228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T311" id="Seg_1230" n="HIAT:u" s="T307">
                  <ts e="T308" id="Seg_1232" n="HIAT:w" s="T307">Dĭn</ts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1235" n="HIAT:w" s="T308">tolʼko</ts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1238" n="HIAT:w" s="T309">sʼestrat</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1241" n="HIAT:w" s="T310">ibi</ts>
                  <nts id="Seg_1242" n="HIAT:ip">.</nts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T313" id="Seg_1245" n="HIAT:u" s="T311">
                  <ts e="T312" id="Seg_1247" n="HIAT:w" s="T311">Kăštəbiʔi</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1250" n="HIAT:w" s="T312">Fanasʼejaʔizi</ts>
                  <nts id="Seg_1251" n="HIAT:ip">.</nts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_1254" n="HIAT:u" s="T313">
                  <ts e="T314" id="Seg_1256" n="HIAT:w" s="T313">Esseŋdə</ts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1259" n="HIAT:w" s="T314">nagobiʔi</ts>
                  <nts id="Seg_1260" n="HIAT:ip">,</nts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1263" n="HIAT:w" s="T315">dăre</ts>
                  <nts id="Seg_1264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1265" n="HIAT:ip">(</nts>
                  <ts e="T317" id="Seg_1267" n="HIAT:w" s="T316">bar=</ts>
                  <nts id="Seg_1268" n="HIAT:ip">)</nts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1271" n="HIAT:w" s="T317">bar</ts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1274" n="HIAT:w" s="T318">külambiʔi</ts>
                  <nts id="Seg_1275" n="HIAT:ip">.</nts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T320" id="Seg_1278" n="HIAT:u" s="T319">
                  <nts id="Seg_1279" n="HIAT:ip">(</nts>
                  <nts id="Seg_1280" n="HIAT:ip">(</nts>
                  <ats e="T320" id="Seg_1281" n="HIAT:non-pho" s="T319">BRK</ats>
                  <nts id="Seg_1282" n="HIAT:ip">)</nts>
                  <nts id="Seg_1283" n="HIAT:ip">)</nts>
                  <nts id="Seg_1284" n="HIAT:ip">.</nts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T325" id="Seg_1287" n="HIAT:u" s="T320">
                  <ts e="T321" id="Seg_1289" n="HIAT:w" s="T320">Bar</ts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1292" n="HIAT:w" s="T321">nagur</ts>
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1295" n="HIAT:w" s="T322">külambiʔi</ts>
                  <nts id="Seg_1296" n="HIAT:ip">,</nts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1299" n="HIAT:w" s="T323">все</ts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1302" n="HIAT:w" s="T324">трое</ts>
                  <nts id="Seg_1303" n="HIAT:ip">.</nts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T326" id="Seg_1306" n="HIAT:u" s="T325">
                  <nts id="Seg_1307" n="HIAT:ip">(</nts>
                  <nts id="Seg_1308" n="HIAT:ip">(</nts>
                  <ats e="T326" id="Seg_1309" n="HIAT:non-pho" s="T325">BRK</ats>
                  <nts id="Seg_1310" n="HIAT:ip">)</nts>
                  <nts id="Seg_1311" n="HIAT:ip">)</nts>
                  <nts id="Seg_1312" n="HIAT:ip">.</nts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T335" id="Seg_1315" n="HIAT:u" s="T326">
                  <ts e="T327" id="Seg_1317" n="HIAT:w" s="T326">Sʼestrat:</ts>
                  <nts id="Seg_1318" n="HIAT:ip">"</nts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1321" n="HIAT:w" s="T327">Ĭmbi</ts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1324" n="HIAT:w" s="T328">šiʔ</ts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1327" n="HIAT:w" s="T329">dʼabrobilaʔ</ts>
                  <nts id="Seg_1328" n="HIAT:ip">,</nts>
                  <nts id="Seg_1329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1331" n="HIAT:w" s="T330">a</ts>
                  <nts id="Seg_1332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1334" n="HIAT:w" s="T331">măn</ts>
                  <nts id="Seg_1335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1337" n="HIAT:w" s="T332">šiʔnʼileʔ</ts>
                  <nts id="Seg_1338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1340" n="HIAT:w" s="T333">ej</ts>
                  <nts id="Seg_1341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1343" n="HIAT:w" s="T334">izenbim</ts>
                  <nts id="Seg_1344" n="HIAT:ip">.</nts>
                  <nts id="Seg_1345" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T338" id="Seg_1347" n="HIAT:u" s="T335">
                  <nts id="Seg_1348" n="HIAT:ip">"</nts>
                  <ts e="T336" id="Seg_1350" n="HIAT:w" s="T335">Dăre</ts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1353" n="HIAT:w" s="T336">bar</ts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1356" n="HIAT:w" s="T337">kunolluʔpileʔ</ts>
                  <nts id="Seg_1357" n="HIAT:ip">"</nts>
                  <nts id="Seg_1358" n="HIAT:ip">.</nts>
                  <nts id="Seg_1359" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T339" id="Seg_1361" n="HIAT:u" s="T338">
                  <nts id="Seg_1362" n="HIAT:ip">(</nts>
                  <nts id="Seg_1363" n="HIAT:ip">(</nts>
                  <ats e="T339" id="Seg_1364" n="HIAT:non-pho" s="T338">BRK</ats>
                  <nts id="Seg_1365" n="HIAT:ip">)</nts>
                  <nts id="Seg_1366" n="HIAT:ip">)</nts>
                  <nts id="Seg_1367" n="HIAT:ip">.</nts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T345" id="Seg_1370" n="HIAT:u" s="T339">
                  <ts e="T340" id="Seg_1372" n="HIAT:w" s="T339">Nuzaŋgən</ts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1375" n="HIAT:w" s="T340">ibi</ts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1378" n="HIAT:w" s="T341">urgo</ts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1381" n="HIAT:w" s="T342">dʼala</ts>
                  <nts id="Seg_1382" n="HIAT:ip">,</nts>
                  <nts id="Seg_1383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1385" n="HIAT:w" s="T343">Михайло</ts>
                  <nts id="Seg_1386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1388" n="HIAT:w" s="T344">dʼala</ts>
                  <nts id="Seg_1389" n="HIAT:ip">.</nts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T349" id="Seg_1392" n="HIAT:u" s="T345">
                  <ts e="T346" id="Seg_1394" n="HIAT:w" s="T345">Onʼiʔ</ts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1397" n="HIAT:w" s="T346">turanə</ts>
                  <nts id="Seg_1398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1400" n="HIAT:w" s="T347">oʔbdəliaʔi</ts>
                  <nts id="Seg_1401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1403" n="HIAT:w" s="T348">bar</ts>
                  <nts id="Seg_1404" n="HIAT:ip">.</nts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T353" id="Seg_1407" n="HIAT:u" s="T349">
                  <ts e="T350" id="Seg_1409" n="HIAT:w" s="T349">Dĭgəttə</ts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1412" n="HIAT:w" s="T350">stoldə</ts>
                  <nts id="Seg_1413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1415" n="HIAT:w" s="T351">oʔbdəliaʔi</ts>
                  <nts id="Seg_1416" n="HIAT:ip">,</nts>
                  <nts id="Seg_1417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1418" n="HIAT:ip">(</nts>
                  <ts e="T353" id="Seg_1420" n="HIAT:w" s="T352">akămnaʔliaʔi</ts>
                  <nts id="Seg_1421" n="HIAT:ip">)</nts>
                  <nts id="Seg_1422" n="HIAT:ip">.</nts>
                  <nts id="Seg_1423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T356" id="Seg_1425" n="HIAT:u" s="T353">
                  <ts e="T354" id="Seg_1427" n="HIAT:w" s="T353">I</ts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1430" n="HIAT:w" s="T354">bĭtleʔbəʔjə</ts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1433" n="HIAT:w" s="T355">ara</ts>
                  <nts id="Seg_1434" n="HIAT:ip">.</nts>
                  <nts id="Seg_1435" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T363" id="Seg_1437" n="HIAT:u" s="T356">
                  <ts e="T357" id="Seg_1439" n="HIAT:w" s="T356">I</ts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1442" n="HIAT:w" s="T357">amnaʔbəʔjə</ts>
                  <nts id="Seg_1443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1445" n="HIAT:w" s="T358">uja</ts>
                  <nts id="Seg_1446" n="HIAT:ip">,</nts>
                  <nts id="Seg_1447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1449" n="HIAT:w" s="T359">ipek</ts>
                  <nts id="Seg_1450" n="HIAT:ip">,</nts>
                  <nts id="Seg_1451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1453" n="HIAT:w" s="T360">ĭmbi</ts>
                  <nts id="Seg_1454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1456" n="HIAT:w" s="T361">ige</ts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1459" n="HIAT:w" s="T362">stolgən</ts>
                  <nts id="Seg_1460" n="HIAT:ip">.</nts>
                  <nts id="Seg_1461" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T367" id="Seg_1463" n="HIAT:u" s="T363">
                  <ts e="T364" id="Seg_1465" n="HIAT:w" s="T363">Dĭgəttə</ts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1468" n="HIAT:w" s="T364">baška</ts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1471" n="HIAT:w" s="T365">turanə</ts>
                  <nts id="Seg_1472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1474" n="HIAT:w" s="T366">kalaʔi</ts>
                  <nts id="Seg_1475" n="HIAT:ip">.</nts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T375" id="Seg_1478" n="HIAT:u" s="T367">
                  <ts e="T368" id="Seg_1480" n="HIAT:w" s="T367">Dĭn</ts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1483" n="HIAT:w" s="T368">dăre</ts>
                  <nts id="Seg_1484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1486" n="HIAT:w" s="T369">že</ts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1489" n="HIAT:w" s="T370">bĭtleʔi</ts>
                  <nts id="Seg_1490" n="HIAT:ip">,</nts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1493" n="HIAT:w" s="T371">dĭgəttə</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1496" n="HIAT:w" s="T372">išo</ts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1499" n="HIAT:w" s="T373">onʼiʔ</ts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1502" n="HIAT:w" s="T374">turanə</ts>
                  <nts id="Seg_1503" n="HIAT:ip">.</nts>
                  <nts id="Seg_1504" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_1506" n="HIAT:u" s="T375">
                  <ts e="T376" id="Seg_1508" n="HIAT:w" s="T375">Dĭgəttə</ts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1511" n="HIAT:w" s="T376">teʔtə</ts>
                  <nts id="Seg_1512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1514" n="HIAT:w" s="T377">turanə</ts>
                  <nts id="Seg_1515" n="HIAT:ip">,</nts>
                  <nts id="Seg_1516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1518" n="HIAT:w" s="T378">sumna</ts>
                  <nts id="Seg_1519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1520" n="HIAT:ip">(</nts>
                  <ts e="T380" id="Seg_1522" n="HIAT:w" s="T379">sɨle-</ts>
                  <nts id="Seg_1523" n="HIAT:ip">)</nts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1525" n="HIAT:ip">(</nts>
                  <ts e="T381" id="Seg_1527" n="HIAT:w" s="T380">sɨləj</ts>
                  <nts id="Seg_1528" n="HIAT:ip">)</nts>
                  <nts id="Seg_1529" n="HIAT:ip">.</nts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T389" id="Seg_1532" n="HIAT:u" s="T381">
                  <ts e="T382" id="Seg_1534" n="HIAT:w" s="T381">Sumna</ts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1537" n="HIAT:w" s="T382">dʼala</ts>
                  <nts id="Seg_1538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1540" n="HIAT:w" s="T383">bĭtleʔbəʔjə</ts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1543" n="HIAT:w" s="T384">üge</ts>
                  <nts id="Seg_1544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1546" n="HIAT:w" s="T385">ara</ts>
                  <nts id="Seg_1547" n="HIAT:ip">,</nts>
                  <nts id="Seg_1548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1550" n="HIAT:w" s="T386">i</ts>
                  <nts id="Seg_1551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1553" n="HIAT:w" s="T387">dĭgəttə</ts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1556" n="HIAT:w" s="T388">kabarləj</ts>
                  <nts id="Seg_1557" n="HIAT:ip">.</nts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T390" id="Seg_1560" n="HIAT:u" s="T389">
                  <nts id="Seg_1561" n="HIAT:ip">(</nts>
                  <nts id="Seg_1562" n="HIAT:ip">(</nts>
                  <ats e="T390" id="Seg_1563" n="HIAT:non-pho" s="T389">BRK</ats>
                  <nts id="Seg_1564" n="HIAT:ip">)</nts>
                  <nts id="Seg_1565" n="HIAT:ip">)</nts>
                  <nts id="Seg_1566" n="HIAT:ip">.</nts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T391" id="Seg_1569" n="HIAT:u" s="T390">
                  <nts id="Seg_1570" n="HIAT:ip">(</nts>
                  <nts id="Seg_1571" n="HIAT:ip">(</nts>
                  <ats e="T391" id="Seg_1572" n="HIAT:non-pho" s="T390">BRK</ats>
                  <nts id="Seg_1573" n="HIAT:ip">)</nts>
                  <nts id="Seg_1574" n="HIAT:ip">)</nts>
                  <nts id="Seg_1575" n="HIAT:ip">.</nts>
                  <nts id="Seg_1576" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T401" id="Seg_1578" n="HIAT:u" s="T391">
                  <ts e="T1011" id="Seg_1580" n="HIAT:w" s="T391">Kazan</ts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1583" n="HIAT:w" s="T1011">turagən</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1586" n="HIAT:w" s="T392">кабак</ts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1589" n="HIAT:w" s="T393">ibi</ts>
                  <nts id="Seg_1590" n="HIAT:ip">,</nts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1593" n="HIAT:w" s="T394">dĭzeŋ</ts>
                  <nts id="Seg_1594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1596" n="HIAT:w" s="T395">dibər</ts>
                  <nts id="Seg_1597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1599" n="HIAT:w" s="T396">kambiʔi</ts>
                  <nts id="Seg_1600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1602" n="HIAT:w" s="T397">i</ts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1605" n="HIAT:w" s="T398">dĭn</ts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1608" n="HIAT:w" s="T399">ara</ts>
                  <nts id="Seg_1609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1611" n="HIAT:w" s="T400">ibiʔi</ts>
                  <nts id="Seg_1612" n="HIAT:ip">.</nts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T407" id="Seg_1615" n="HIAT:u" s="T401">
                  <ts e="T402" id="Seg_1617" n="HIAT:w" s="T401">Šindin</ts>
                  <nts id="Seg_1618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1620" n="HIAT:w" s="T402">aktʼa</ts>
                  <nts id="Seg_1621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1623" n="HIAT:w" s="T403">iʔgö</ts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1626" n="HIAT:w" s="T404">dăk</ts>
                  <nts id="Seg_1627" n="HIAT:ip">,</nts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1630" n="HIAT:w" s="T405">iʔgö</ts>
                  <nts id="Seg_1631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1633" n="HIAT:w" s="T406">ilieʔi</ts>
                  <nts id="Seg_1634" n="HIAT:ip">.</nts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T409" id="Seg_1637" n="HIAT:u" s="T407">
                  <ts e="T408" id="Seg_1639" n="HIAT:w" s="T407">Sumna</ts>
                  <nts id="Seg_1640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1642" n="HIAT:w" s="T408">igəj</ts>
                  <nts id="Seg_1643" n="HIAT:ip">.</nts>
                  <nts id="Seg_1644" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T414" id="Seg_1646" n="HIAT:u" s="T409">
                  <ts e="T410" id="Seg_1648" n="HIAT:w" s="T409">A</ts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1651" n="HIAT:w" s="T410">šindin</ts>
                  <nts id="Seg_1652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1654" n="HIAT:w" s="T411">amga</ts>
                  <nts id="Seg_1655" n="HIAT:ip">,</nts>
                  <nts id="Seg_1656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1658" n="HIAT:w" s="T412">teʔtə</ts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1661" n="HIAT:w" s="T413">iləj</ts>
                  <nts id="Seg_1662" n="HIAT:ip">.</nts>
                  <nts id="Seg_1663" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T419" id="Seg_1665" n="HIAT:u" s="T414">
                  <ts e="T415" id="Seg_1667" n="HIAT:w" s="T414">Šindin</ts>
                  <nts id="Seg_1668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1670" n="HIAT:w" s="T415">išo</ts>
                  <nts id="Seg_1671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1673" n="HIAT:w" s="T416">amga</ts>
                  <nts id="Seg_1674" n="HIAT:ip">,</nts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1677" n="HIAT:w" s="T417">šide</ts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1680" n="HIAT:w" s="T418">iləj</ts>
                  <nts id="Seg_1681" n="HIAT:ip">.</nts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T424" id="Seg_1684" n="HIAT:u" s="T419">
                  <ts e="T420" id="Seg_1686" n="HIAT:w" s="T419">I</ts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1689" n="HIAT:w" s="T420">dĭgəttə</ts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1692" n="HIAT:w" s="T421">urgo</ts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1695" n="HIAT:w" s="T422">dʼalagən</ts>
                  <nts id="Seg_1696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1697" n="HIAT:ip">(</nts>
                  <ts e="T424" id="Seg_1699" n="HIAT:w" s="T423">bĭʔpiʔi</ts>
                  <nts id="Seg_1700" n="HIAT:ip">)</nts>
                  <nts id="Seg_1701" n="HIAT:ip">.</nts>
                  <nts id="Seg_1702" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T428" id="Seg_1704" n="HIAT:u" s="T424">
                  <ts e="T425" id="Seg_1706" n="HIAT:w" s="T424">Üdʼüge</ts>
                  <nts id="Seg_1707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1709" n="HIAT:w" s="T425">dĭrgit</ts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1712" n="HIAT:w" s="T426">rʼumkanə</ts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1715" n="HIAT:w" s="T427">kămnəbiʔi</ts>
                  <nts id="Seg_1716" n="HIAT:ip">.</nts>
                  <nts id="Seg_1717" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T439" id="Seg_1719" n="HIAT:u" s="T428">
                  <ts e="T429" id="Seg_1721" n="HIAT:w" s="T428">Onʼiʔtə</ts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1724" n="HIAT:w" s="T429">mĭləj</ts>
                  <nts id="Seg_1725" n="HIAT:ip">,</nts>
                  <nts id="Seg_1726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1728" n="HIAT:w" s="T430">dĭgəttə</ts>
                  <nts id="Seg_1729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1731" n="HIAT:w" s="T431">bazoʔ</ts>
                  <nts id="Seg_1732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1734" n="HIAT:w" s="T432">dak</ts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1737" n="HIAT:w" s="T433">tăre</ts>
                  <nts id="Seg_1738" n="HIAT:ip">,</nts>
                  <nts id="Seg_1739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1741" n="HIAT:w" s="T434">tăre</ts>
                  <nts id="Seg_1742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1744" n="HIAT:w" s="T435">mĭlie</ts>
                  <nts id="Seg_1745" n="HIAT:ip">,</nts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1748" n="HIAT:w" s="T436">mĭlie</ts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1751" n="HIAT:w" s="T437">bar</ts>
                  <nts id="Seg_1752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1753" n="HIAT:ip">(</nts>
                  <ts e="T439" id="Seg_1755" n="HIAT:w" s="T438">jude</ts>
                  <nts id="Seg_1756" n="HIAT:ip">)</nts>
                  <nts id="Seg_1757" n="HIAT:ip">.</nts>
                  <nts id="Seg_1758" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T444" id="Seg_1760" n="HIAT:u" s="T439">
                  <ts e="T440" id="Seg_1762" n="HIAT:w" s="T439">Dĭgəttə</ts>
                  <nts id="Seg_1763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1765" n="HIAT:w" s="T440">amorlaʔbəʔjə</ts>
                  <nts id="Seg_1766" n="HIAT:ip">,</nts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1769" n="HIAT:w" s="T441">amnaʔbəʔjə</ts>
                  <nts id="Seg_1770" n="HIAT:ip">,</nts>
                  <nts id="Seg_1771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1773" n="HIAT:w" s="T442">bar</ts>
                  <nts id="Seg_1774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1776" n="HIAT:w" s="T443">dʼăbaktərlaʔbəʔjə</ts>
                  <nts id="Seg_1777" n="HIAT:ip">.</nts>
                  <nts id="Seg_1778" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T446" id="Seg_1780" n="HIAT:u" s="T444">
                  <ts e="T445" id="Seg_1782" n="HIAT:w" s="T444">Nüjnə</ts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1785" n="HIAT:w" s="T445">nüjleʔbəʔjə</ts>
                  <nts id="Seg_1786" n="HIAT:ip">.</nts>
                  <nts id="Seg_1787" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T447" id="Seg_1789" n="HIAT:u" s="T446">
                  <nts id="Seg_1790" n="HIAT:ip">(</nts>
                  <nts id="Seg_1791" n="HIAT:ip">(</nts>
                  <ats e="T447" id="Seg_1792" n="HIAT:non-pho" s="T446">BRK</ats>
                  <nts id="Seg_1793" n="HIAT:ip">)</nts>
                  <nts id="Seg_1794" n="HIAT:ip">)</nts>
                  <nts id="Seg_1795" n="HIAT:ip">.</nts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T453" id="Seg_1798" n="HIAT:u" s="T447">
                  <ts e="T448" id="Seg_1800" n="HIAT:w" s="T447">Алексадр</ts>
                  <nts id="Seg_1801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1803" n="HIAT:w" s="T448">Капитонович</ts>
                  <nts id="Seg_1804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1806" n="HIAT:w" s="T449">Ашпуров</ts>
                  <nts id="Seg_1807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1809" n="HIAT:w" s="T450">kambi</ts>
                  <nts id="Seg_1810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451.tx.1" id="Seg_1812" n="HIAT:w" s="T451">Kazan</ts>
                  <nts id="Seg_1813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1815" n="HIAT:w" s="T451.tx.1">turanə</ts>
                  <nts id="Seg_1816" n="HIAT:ip">…</nts>
                  <nts id="Seg_1817" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T457" id="Seg_1819" n="HIAT:u" s="T453">
                  <ts e="T454" id="Seg_1821" n="HIAT:w" s="T453">Nʼikola</ts>
                  <nts id="Seg_1822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1824" n="HIAT:w" s="T454">ibi</ts>
                  <nts id="Seg_1825" n="HIAT:ip">,</nts>
                  <nts id="Seg_1826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1828" n="HIAT:w" s="T455">urgo</ts>
                  <nts id="Seg_1829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1831" n="HIAT:w" s="T456">dʼala</ts>
                  <nts id="Seg_1832" n="HIAT:ip">.</nts>
                  <nts id="Seg_1833" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T461" id="Seg_1835" n="HIAT:u" s="T457">
                  <ts e="T458" id="Seg_1837" n="HIAT:w" s="T457">Dĭn</ts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1840" n="HIAT:w" s="T458">ara</ts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1843" n="HIAT:w" s="T459">bĭʔpiʔi</ts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1846" n="HIAT:w" s="T460">bar</ts>
                  <nts id="Seg_1847" n="HIAT:ip">.</nts>
                  <nts id="Seg_1848" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T464" id="Seg_1850" n="HIAT:u" s="T461">
                  <ts e="T462" id="Seg_1852" n="HIAT:w" s="T461">Kažnej</ts>
                  <nts id="Seg_1853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1855" n="HIAT:w" s="T462">turanə</ts>
                  <nts id="Seg_1856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1858" n="HIAT:w" s="T463">mĭmbiʔi</ts>
                  <nts id="Seg_1859" n="HIAT:ip">.</nts>
                  <nts id="Seg_1860" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T472" id="Seg_1862" n="HIAT:u" s="T464">
                  <ts e="T465" id="Seg_1864" n="HIAT:w" s="T464">Dĭ</ts>
                  <nts id="Seg_1865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1867" n="HIAT:w" s="T465">nüdʼin</ts>
                  <nts id="Seg_1868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1010" id="Seg_1870" n="HIAT:w" s="T466">kalla</ts>
                  <nts id="Seg_1871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1873" n="HIAT:w" s="T1010">dʼürbi</ts>
                  <nts id="Seg_1874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1876" n="HIAT:w" s="T467">Anʼžanə</ts>
                  <nts id="Seg_1877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1879" n="HIAT:w" s="T468">i</ts>
                  <nts id="Seg_1880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1882" n="HIAT:w" s="T469">dĭn</ts>
                  <nts id="Seg_1883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1885" n="HIAT:w" s="T470">kănnambi</ts>
                  <nts id="Seg_1886" n="HIAT:ip">,</nts>
                  <nts id="Seg_1887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1889" n="HIAT:w" s="T471">külambi</ts>
                  <nts id="Seg_1890" n="HIAT:ip">.</nts>
                  <nts id="Seg_1891" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T473" id="Seg_1893" n="HIAT:u" s="T472">
                  <nts id="Seg_1894" n="HIAT:ip">(</nts>
                  <nts id="Seg_1895" n="HIAT:ip">(</nts>
                  <ats e="T473" id="Seg_1896" n="HIAT:non-pho" s="T472">BRK</ats>
                  <nts id="Seg_1897" n="HIAT:ip">)</nts>
                  <nts id="Seg_1898" n="HIAT:ip">)</nts>
                  <nts id="Seg_1899" n="HIAT:ip">.</nts>
                  <nts id="Seg_1900" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T479" id="Seg_1902" n="HIAT:u" s="T473">
                  <ts e="T474" id="Seg_1904" n="HIAT:w" s="T473">Dĭ</ts>
                  <nts id="Seg_1905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1906" n="HIAT:ip">(</nts>
                  <ts e="T475" id="Seg_1908" n="HIAT:w" s="T474">maːndə</ts>
                  <nts id="Seg_1909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1911" n="HIAT:w" s="T475">ka-</ts>
                  <nts id="Seg_1912" n="HIAT:ip">)</nts>
                  <nts id="Seg_1913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1915" n="HIAT:w" s="T476">maʔəndə</ts>
                  <nts id="Seg_1916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1918" n="HIAT:w" s="T477">šobi</ts>
                  <nts id="Seg_1919" n="HIAT:ip">,</nts>
                  <nts id="Seg_1920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1922" n="HIAT:w" s="T478">nʼiʔtə</ts>
                  <nts id="Seg_1923" n="HIAT:ip">.</nts>
                  <nts id="Seg_1924" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T483" id="Seg_1926" n="HIAT:u" s="T479">
                  <ts e="T480" id="Seg_1928" n="HIAT:w" s="T479">Šide</ts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1931" n="HIAT:w" s="T480">kuza</ts>
                  <nts id="Seg_1932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1934" n="HIAT:w" s="T481">dĭʔnə</ts>
                  <nts id="Seg_1935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1937" n="HIAT:w" s="T482">šobiʔi</ts>
                  <nts id="Seg_1938" n="HIAT:ip">.</nts>
                  <nts id="Seg_1939" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T486" id="Seg_1941" n="HIAT:u" s="T483">
                  <nts id="Seg_1942" n="HIAT:ip">"</nts>
                  <ts e="T484" id="Seg_1944" n="HIAT:w" s="T483">Kanžəbəj</ts>
                  <nts id="Seg_1945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1946" n="HIAT:ip">(</nts>
                  <ts e="T485" id="Seg_1948" n="HIAT:w" s="T484">maʔsʼ-</ts>
                  <nts id="Seg_1949" n="HIAT:ip">)</nts>
                  <nts id="Seg_1950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1952" n="HIAT:w" s="T485">mănzi</ts>
                  <nts id="Seg_1953" n="HIAT:ip">"</nts>
                  <nts id="Seg_1954" n="HIAT:ip">.</nts>
                  <nts id="Seg_1955" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T489" id="Seg_1957" n="HIAT:u" s="T486">
                  <ts e="T487" id="Seg_1959" n="HIAT:w" s="T486">Dĭgəttə</ts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1962" n="HIAT:w" s="T487">kambi</ts>
                  <nts id="Seg_1963" n="HIAT:ip">,</nts>
                  <nts id="Seg_1964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1966" n="HIAT:w" s="T488">kambi</ts>
                  <nts id="Seg_1967" n="HIAT:ip">.</nts>
                  <nts id="Seg_1968" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T493" id="Seg_1970" n="HIAT:u" s="T489">
                  <ts e="T490" id="Seg_1972" n="HIAT:w" s="T489">Kuliot</ts>
                  <nts id="Seg_1973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1975" n="HIAT:w" s="T490">bar:</ts>
                  <nts id="Seg_1976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1977" n="HIAT:ip">(</nts>
                  <ts e="T492" id="Seg_1979" n="HIAT:w" s="T491">kulʼa</ts>
                  <nts id="Seg_1980" n="HIAT:ip">)</nts>
                  <nts id="Seg_1981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1983" n="HIAT:w" s="T492">bar</ts>
                  <nts id="Seg_1984" n="HIAT:ip">.</nts>
                  <nts id="Seg_1985" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T498" id="Seg_1987" n="HIAT:u" s="T493">
                  <nts id="Seg_1988" n="HIAT:ip">(</nts>
                  <ts e="T494" id="Seg_1990" n="HIAT:w" s="T493">Bu-</ts>
                  <nts id="Seg_1991" n="HIAT:ip">)</nts>
                  <nts id="Seg_1992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1994" n="HIAT:w" s="T494">dĭgəttə</ts>
                  <nts id="Seg_1995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1997" n="HIAT:w" s="T495">kudajdə</ts>
                  <nts id="Seg_1998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1999" n="HIAT:ip">(</nts>
                  <ts e="T497" id="Seg_2001" n="HIAT:w" s="T496">uman-</ts>
                  <nts id="Seg_2002" n="HIAT:ip">)</nts>
                  <nts id="Seg_2003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497.tx.1" id="Seg_2005" n="HIAT:w" s="T497">numan</ts>
                  <nts id="Seg_2006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_2008" n="HIAT:w" s="T497.tx.1">üzəbi</ts>
                  <nts id="Seg_2009" n="HIAT:ip">.</nts>
                  <nts id="Seg_2010" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T505" id="Seg_2012" n="HIAT:u" s="T498">
                  <ts e="T499" id="Seg_2014" n="HIAT:w" s="T498">Dĭʔə</ts>
                  <nts id="Seg_2015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_2017" n="HIAT:w" s="T499">ildə</ts>
                  <nts id="Seg_2018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_2020" n="HIAT:w" s="T500">nuʔməluʔpiʔi</ts>
                  <nts id="Seg_2021" n="HIAT:ip">,</nts>
                  <nts id="Seg_2022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_2024" n="HIAT:w" s="T501">a</ts>
                  <nts id="Seg_2025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_2027" n="HIAT:w" s="T502">dĭ</ts>
                  <nts id="Seg_2028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_2030" n="HIAT:w" s="T503">maːʔndə</ts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_2033" n="HIAT:w" s="T504">šobi</ts>
                  <nts id="Seg_2034" n="HIAT:ip">.</nts>
                  <nts id="Seg_2035" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T508" id="Seg_2037" n="HIAT:u" s="T505">
                  <ts e="T506" id="Seg_2039" n="HIAT:w" s="T505">Bostə</ts>
                  <nts id="Seg_2040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_2042" n="HIAT:w" s="T506">sĭre</ts>
                  <nts id="Seg_2043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_2045" n="HIAT:w" s="T507">mobi</ts>
                  <nts id="Seg_2046" n="HIAT:ip">.</nts>
                  <nts id="Seg_2047" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T509" id="Seg_2049" n="HIAT:u" s="T508">
                  <nts id="Seg_2050" n="HIAT:ip">(</nts>
                  <nts id="Seg_2051" n="HIAT:ip">(</nts>
                  <ats e="T509" id="Seg_2052" n="HIAT:non-pho" s="T508">BRK</ats>
                  <nts id="Seg_2053" n="HIAT:ip">)</nts>
                  <nts id="Seg_2054" n="HIAT:ip">)</nts>
                  <nts id="Seg_2055" n="HIAT:ip">.</nts>
                  <nts id="Seg_2056" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T514" id="Seg_2058" n="HIAT:u" s="T509">
                  <ts e="T510" id="Seg_2060" n="HIAT:w" s="T509">Dĭzeŋ</ts>
                  <nts id="Seg_2061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_2063" n="HIAT:w" s="T510">ugandə</ts>
                  <nts id="Seg_2064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_2066" n="HIAT:w" s="T511">ara</ts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_2069" n="HIAT:w" s="T512">iʔgö</ts>
                  <nts id="Seg_2070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_2072" n="HIAT:w" s="T513">bĭtleʔbəʔjə</ts>
                  <nts id="Seg_2073" n="HIAT:ip">.</nts>
                  <nts id="Seg_2074" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T522" id="Seg_2076" n="HIAT:u" s="T514">
                  <ts e="T515" id="Seg_2078" n="HIAT:w" s="T514">Kumən</ts>
                  <nts id="Seg_2079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_2081" n="HIAT:w" s="T515">xotʼ</ts>
                  <nts id="Seg_2082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2083" n="HIAT:ip">(</nts>
                  <ts e="T517" id="Seg_2085" n="HIAT:w" s="T516">m-</ts>
                  <nts id="Seg_2086" n="HIAT:ip">)</nts>
                  <nts id="Seg_2087" n="HIAT:ip">,</nts>
                  <nts id="Seg_2088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_2090" n="HIAT:w" s="T517">üge</ts>
                  <nts id="Seg_2091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_2093" n="HIAT:w" s="T518">bĭtleʔbəʔjə</ts>
                  <nts id="Seg_2094" n="HIAT:ip">,</nts>
                  <nts id="Seg_2095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_2097" n="HIAT:w" s="T519">bĭtleʔbəʔjə</ts>
                  <nts id="Seg_2098" n="HIAT:ip">,</nts>
                  <nts id="Seg_2099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_2101" n="HIAT:w" s="T520">išo</ts>
                  <nts id="Seg_2102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_2104" n="HIAT:w" s="T521">pileʔbəʔjə</ts>
                  <nts id="Seg_2105" n="HIAT:ip">.</nts>
                  <nts id="Seg_2106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T528" id="Seg_2108" n="HIAT:u" s="T522">
                  <ts e="T523" id="Seg_2110" n="HIAT:w" s="T522">Gijen</ts>
                  <nts id="Seg_2111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_2113" n="HIAT:w" s="T523">ige</ts>
                  <nts id="Seg_2114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_2116" n="HIAT:w" s="T524">dăk</ts>
                  <nts id="Seg_2117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_2119" n="HIAT:w" s="T525">dibər</ts>
                  <nts id="Seg_2120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_2122" n="HIAT:w" s="T526">nuʔməluʔbəʔjə</ts>
                  <nts id="Seg_2123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_2125" n="HIAT:w" s="T527">bar</ts>
                  <nts id="Seg_2126" n="HIAT:ip">.</nts>
                  <nts id="Seg_2127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T529" id="Seg_2129" n="HIAT:u" s="T528">
                  <nts id="Seg_2130" n="HIAT:ip">(</nts>
                  <nts id="Seg_2131" n="HIAT:ip">(</nts>
                  <ats e="T529" id="Seg_2132" n="HIAT:non-pho" s="T528">BRK</ats>
                  <nts id="Seg_2133" n="HIAT:ip">)</nts>
                  <nts id="Seg_2134" n="HIAT:ip">)</nts>
                  <nts id="Seg_2135" n="HIAT:ip">.</nts>
                  <nts id="Seg_2136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T532" id="Seg_2138" n="HIAT:u" s="T529">
                  <ts e="T530" id="Seg_2140" n="HIAT:w" s="T529">Dĭgəttə</ts>
                  <nts id="Seg_2141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_2143" n="HIAT:w" s="T530">bar</ts>
                  <nts id="Seg_2144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_2146" n="HIAT:w" s="T531">nüjleʔbəʔjə</ts>
                  <nts id="Seg_2147" n="HIAT:ip">.</nts>
                  <nts id="Seg_2148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T533" id="Seg_2150" n="HIAT:u" s="T532">
                  <ts e="T533" id="Seg_2152" n="HIAT:w" s="T532">Suʔmileʔbəʔjə</ts>
                  <nts id="Seg_2153" n="HIAT:ip">.</nts>
                  <nts id="Seg_2154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T536" id="Seg_2156" n="HIAT:u" s="T533">
                  <ts e="T534" id="Seg_2158" n="HIAT:w" s="T533">Dĭgəttə</ts>
                  <nts id="Seg_2159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_2161" n="HIAT:w" s="T534">bar</ts>
                  <nts id="Seg_2162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_2164" n="HIAT:w" s="T535">dʼabrolaʔbəʔjə</ts>
                  <nts id="Seg_2165" n="HIAT:ip">.</nts>
                  <nts id="Seg_2166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T537" id="Seg_2168" n="HIAT:u" s="T536">
                  <nts id="Seg_2169" n="HIAT:ip">(</nts>
                  <nts id="Seg_2170" n="HIAT:ip">(</nts>
                  <ats e="T537" id="Seg_2171" n="HIAT:non-pho" s="T536">BRK</ats>
                  <nts id="Seg_2172" n="HIAT:ip">)</nts>
                  <nts id="Seg_2173" n="HIAT:ip">)</nts>
                  <nts id="Seg_2174" n="HIAT:ip">.</nts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T547" id="Seg_2177" n="HIAT:u" s="T537">
                  <ts e="T538" id="Seg_2179" n="HIAT:w" s="T537">Šindi</ts>
                  <nts id="Seg_2180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2182" n="HIAT:w" s="T538">bar</ts>
                  <nts id="Seg_2183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_2185" n="HIAT:w" s="T539">iʔgö</ts>
                  <nts id="Seg_2186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2187" n="HIAT:ip">(</nts>
                  <ts e="T541" id="Seg_2189" n="HIAT:w" s="T540">bĭt-</ts>
                  <nts id="Seg_2190" n="HIAT:ip">)</nts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2193" n="HIAT:w" s="T541">bĭtlie</ts>
                  <nts id="Seg_2194" n="HIAT:ip">,</nts>
                  <nts id="Seg_2195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_2197" n="HIAT:w" s="T542">bĭtlie</ts>
                  <nts id="Seg_2198" n="HIAT:ip">,</nts>
                  <nts id="Seg_2199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2201" n="HIAT:w" s="T543">dĭgəttə</ts>
                  <nts id="Seg_2202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2204" n="HIAT:w" s="T544">aragəʔ</ts>
                  <nts id="Seg_2205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2207" n="HIAT:w" s="T545">bar</ts>
                  <nts id="Seg_2208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2209" n="HIAT:ip">(</nts>
                  <ts e="T547" id="Seg_2211" n="HIAT:w" s="T546">nanʼileʔbə</ts>
                  <nts id="Seg_2212" n="HIAT:ip">)</nts>
                  <nts id="Seg_2213" n="HIAT:ip">.</nts>
                  <nts id="Seg_2214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T552" id="Seg_2216" n="HIAT:u" s="T547">
                  <nts id="Seg_2217" n="HIAT:ip">(</nts>
                  <ts e="T548" id="Seg_2219" n="HIAT:w" s="T547">Dĭ=</ts>
                  <nts id="Seg_2220" n="HIAT:ip">)</nts>
                  <nts id="Seg_2221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2223" n="HIAT:w" s="T548">Dĭʔnə</ts>
                  <nts id="Seg_2224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2226" n="HIAT:w" s="T549">aŋdə</ts>
                  <nts id="Seg_2227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2229" n="HIAT:w" s="T550">bar</ts>
                  <nts id="Seg_2230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2232" n="HIAT:w" s="T551">kĭnzleʔbəʔjə</ts>
                  <nts id="Seg_2233" n="HIAT:ip">.</nts>
                  <nts id="Seg_2234" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T556" id="Seg_2236" n="HIAT:u" s="T552">
                  <ts e="T553" id="Seg_2238" n="HIAT:w" s="T552">Dĭgəttə</ts>
                  <nts id="Seg_2239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2241" n="HIAT:w" s="T553">dĭ</ts>
                  <nts id="Seg_2242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2243" n="HIAT:ip">(</nts>
                  <ts e="T555" id="Seg_2245" n="HIAT:w" s="T554">o-</ts>
                  <nts id="Seg_2246" n="HIAT:ip">)</nts>
                  <nts id="Seg_2247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2248" n="HIAT:ip">(</nts>
                  <ts e="T556" id="Seg_2250" n="HIAT:w" s="T555">uʔləj</ts>
                  <nts id="Seg_2251" n="HIAT:ip">)</nts>
                  <nts id="Seg_2252" n="HIAT:ip">.</nts>
                  <nts id="Seg_2253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T557" id="Seg_2255" n="HIAT:u" s="T556">
                  <nts id="Seg_2256" n="HIAT:ip">(</nts>
                  <nts id="Seg_2257" n="HIAT:ip">(</nts>
                  <ats e="T557" id="Seg_2258" n="HIAT:non-pho" s="T556">BRK</ats>
                  <nts id="Seg_2259" n="HIAT:ip">)</nts>
                  <nts id="Seg_2260" n="HIAT:ip">)</nts>
                  <nts id="Seg_2261" n="HIAT:ip">.</nts>
                  <nts id="Seg_2262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T565" id="Seg_2264" n="HIAT:u" s="T557">
                  <nts id="Seg_2265" n="HIAT:ip">(</nts>
                  <ts e="T558" id="Seg_2267" n="HIAT:w" s="T557">Onʼiʔ</ts>
                  <nts id="Seg_2268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2270" n="HIAT:w" s="T558">onʼiʔ=</ts>
                  <nts id="Seg_2271" n="HIAT:ip">)</nts>
                  <nts id="Seg_2272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2274" n="HIAT:w" s="T559">măn</ts>
                  <nts id="Seg_2275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2276" n="HIAT:ip">(</nts>
                  <ts e="T561" id="Seg_2278" n="HIAT:w" s="T560">kum</ts>
                  <nts id="Seg_2279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2281" n="HIAT:w" s="T561">ĭzemnuʔpi</ts>
                  <nts id="Seg_2282" n="HIAT:ip">)</nts>
                  <nts id="Seg_2283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2285" n="HIAT:w" s="T562">bar</ts>
                  <nts id="Seg_2286" n="HIAT:ip">,</nts>
                  <nts id="Seg_2287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2289" n="HIAT:w" s="T563">dĭn</ts>
                  <nts id="Seg_2290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2292" n="HIAT:w" s="T564">müʔleʔbə</ts>
                  <nts id="Seg_2293" n="HIAT:ip">.</nts>
                  <nts id="Seg_2294" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T574" id="Seg_2296" n="HIAT:u" s="T565">
                  <ts e="T566" id="Seg_2298" n="HIAT:w" s="T565">Măn</ts>
                  <nts id="Seg_2299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2301" n="HIAT:w" s="T566">dĭgəttə</ts>
                  <nts id="Seg_2302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2304" n="HIAT:w" s="T567">kubiam</ts>
                  <nts id="Seg_2305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2307" n="HIAT:w" s="T568">trʼapka</ts>
                  <nts id="Seg_2308" n="HIAT:ip">,</nts>
                  <nts id="Seg_2309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2311" n="HIAT:w" s="T569">kĭnzəbiem</ts>
                  <nts id="Seg_2312" n="HIAT:ip">,</nts>
                  <nts id="Seg_2313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2314" n="HIAT:ip">(</nts>
                  <ts e="T571" id="Seg_2316" n="HIAT:w" s="T570">tĭgə</ts>
                  <nts id="Seg_2317" n="HIAT:ip">)</nts>
                  <nts id="Seg_2318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2319" n="HIAT:ip">(</nts>
                  <ts e="T572" id="Seg_2321" n="HIAT:w" s="T571">a-</ts>
                  <nts id="Seg_2322" n="HIAT:ip">)</nts>
                  <nts id="Seg_2323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2325" n="HIAT:w" s="T572">ažnə</ts>
                  <nts id="Seg_2326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2328" n="HIAT:w" s="T573">mʼaŋnuʔpi</ts>
                  <nts id="Seg_2329" n="HIAT:ip">.</nts>
                  <nts id="Seg_2330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T577" id="Seg_2332" n="HIAT:u" s="T574">
                  <ts e="T575" id="Seg_2334" n="HIAT:w" s="T574">Măn</ts>
                  <nts id="Seg_2335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2337" n="HIAT:w" s="T575">kunə</ts>
                  <nts id="Seg_2338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2340" n="HIAT:w" s="T576">embiem</ts>
                  <nts id="Seg_2341" n="HIAT:ip">.</nts>
                  <nts id="Seg_2342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T581" id="Seg_2344" n="HIAT:u" s="T577">
                  <nts id="Seg_2345" n="HIAT:ip">(</nts>
                  <ts e="T578" id="Seg_2347" n="HIAT:w" s="T577">Dĭ-</ts>
                  <nts id="Seg_2348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2350" n="HIAT:w" s="T578">dĭ-</ts>
                  <nts id="Seg_2351" n="HIAT:ip">)</nts>
                  <nts id="Seg_2352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2354" n="HIAT:w" s="T579">Dĭgəttə</ts>
                  <nts id="Seg_2355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2357" n="HIAT:w" s="T580">sarbiom</ts>
                  <nts id="Seg_2358" n="HIAT:ip">.</nts>
                  <nts id="Seg_2359" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T583" id="Seg_2361" n="HIAT:u" s="T581">
                  <ts e="T582" id="Seg_2363" n="HIAT:w" s="T581">Iʔbəbiem</ts>
                  <nts id="Seg_2364" n="HIAT:ip">,</nts>
                  <nts id="Seg_2365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2367" n="HIAT:w" s="T582">kunolluʔpiem</ts>
                  <nts id="Seg_2368" n="HIAT:ip">.</nts>
                  <nts id="Seg_2369" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T588" id="Seg_2371" n="HIAT:u" s="T583">
                  <ts e="T584" id="Seg_2373" n="HIAT:w" s="T583">Uʔpiam</ts>
                  <nts id="Seg_2374" n="HIAT:ip">,</nts>
                  <nts id="Seg_2375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2376" n="HIAT:ip">(</nts>
                  <ts e="T585" id="Seg_2378" n="HIAT:w" s="T584">ku-</ts>
                  <nts id="Seg_2379" n="HIAT:ip">)</nts>
                  <nts id="Seg_2380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2382" n="HIAT:w" s="T585">kum</ts>
                  <nts id="Seg_2383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2385" n="HIAT:w" s="T586">ej</ts>
                  <nts id="Seg_2386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2388" n="HIAT:w" s="T587">ĭzemnie</ts>
                  <nts id="Seg_2389" n="HIAT:ip">.</nts>
                  <nts id="Seg_2390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T598" id="Seg_2392" n="HIAT:u" s="T588">
                  <ts e="T589" id="Seg_2394" n="HIAT:w" s="T588">A</ts>
                  <nts id="Seg_2395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2397" n="HIAT:w" s="T589">dĭgəttə</ts>
                  <nts id="Seg_2398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2400" n="HIAT:w" s="T590">ulum</ts>
                  <nts id="Seg_2401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2403" n="HIAT:w" s="T591">ĭzemnie</ts>
                  <nts id="Seg_2404" n="HIAT:ip">,</nts>
                  <nts id="Seg_2405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2407" n="HIAT:w" s="T592">măn</ts>
                  <nts id="Seg_2408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2410" n="HIAT:w" s="T593">tože</ts>
                  <nts id="Seg_2411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2413" n="HIAT:w" s="T594">kĭnzlem</ts>
                  <nts id="Seg_2414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2416" n="HIAT:w" s="T595">trʼapkaʔinə</ts>
                  <nts id="Seg_2417" n="HIAT:ip">,</nts>
                  <nts id="Seg_2418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2420" n="HIAT:w" s="T596">ulundə</ts>
                  <nts id="Seg_2421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2423" n="HIAT:w" s="T597">sarlim</ts>
                  <nts id="Seg_2424" n="HIAT:ip">.</nts>
                  <nts id="Seg_2425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T607" id="Seg_2427" n="HIAT:u" s="T598">
                  <ts e="T599" id="Seg_2429" n="HIAT:w" s="T598">Iʔbəlem</ts>
                  <nts id="Seg_2430" n="HIAT:ip">,</nts>
                  <nts id="Seg_2431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2433" n="HIAT:w" s="T599">dĭ</ts>
                  <nts id="Seg_2434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2435" n="HIAT:ip">(</nts>
                  <ts e="T601" id="Seg_2437" n="HIAT:w" s="T600">kolə-</ts>
                  <nts id="Seg_2438" n="HIAT:ip">)</nts>
                  <nts id="Seg_2439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2441" n="HIAT:w" s="T601">koluʔləj</ts>
                  <nts id="Seg_2442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2444" n="HIAT:w" s="T602">dĭ</ts>
                  <nts id="Seg_2445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2447" n="HIAT:w" s="T603">trʼapka</ts>
                  <nts id="Seg_2448" n="HIAT:ip">,</nts>
                  <nts id="Seg_2449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2451" n="HIAT:w" s="T604">ulum</ts>
                  <nts id="Seg_2452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2454" n="HIAT:w" s="T605">ej</ts>
                  <nts id="Seg_2455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2457" n="HIAT:w" s="T606">ĭzemnie</ts>
                  <nts id="Seg_2458" n="HIAT:ip">.</nts>
                  <nts id="Seg_2459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T608" id="Seg_2461" n="HIAT:u" s="T607">
                  <nts id="Seg_2462" n="HIAT:ip">(</nts>
                  <nts id="Seg_2463" n="HIAT:ip">(</nts>
                  <ats e="T608" id="Seg_2464" n="HIAT:non-pho" s="T607">BRK</ats>
                  <nts id="Seg_2465" n="HIAT:ip">)</nts>
                  <nts id="Seg_2466" n="HIAT:ip">)</nts>
                  <nts id="Seg_2467" n="HIAT:ip">.</nts>
                  <nts id="Seg_2468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T615" id="Seg_2470" n="HIAT:u" s="T608">
                  <nts id="Seg_2471" n="HIAT:ip">(</nts>
                  <ts e="T609" id="Seg_2473" n="HIAT:w" s="T608">Onʼiʔ=</ts>
                  <nts id="Seg_2474" n="HIAT:ip">)</nts>
                  <nts id="Seg_2475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2477" n="HIAT:w" s="T609">Onʼiʔ</ts>
                  <nts id="Seg_2478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2480" n="HIAT:w" s="T610">raz</ts>
                  <nts id="Seg_2481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2483" n="HIAT:w" s="T611">miʔ</ts>
                  <nts id="Seg_2484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2486" n="HIAT:w" s="T612">ipek</ts>
                  <nts id="Seg_2487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2489" n="HIAT:w" s="T613">toʔnarbibaʔ</ts>
                  <nts id="Seg_2490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2492" n="HIAT:w" s="T614">mašinaʔizi</ts>
                  <nts id="Seg_2493" n="HIAT:ip">.</nts>
                  <nts id="Seg_2494" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T621" id="Seg_2496" n="HIAT:u" s="T615">
                  <ts e="T616" id="Seg_2498" n="HIAT:w" s="T615">Tĭn</ts>
                  <nts id="Seg_2499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2501" n="HIAT:w" s="T616">dagaj</ts>
                  <nts id="Seg_2502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2504" n="HIAT:w" s="T617">ibi</ts>
                  <nts id="Seg_2505" n="HIAT:ip">,</nts>
                  <nts id="Seg_2506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2508" n="HIAT:w" s="T618">snăpɨʔi</ts>
                  <nts id="Seg_2509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2510" n="HIAT:ip">(</nts>
                  <ts e="T620" id="Seg_2512" n="HIAT:w" s="T619">dʼaba-</ts>
                  <nts id="Seg_2513" n="HIAT:ip">)</nts>
                  <nts id="Seg_2514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2516" n="HIAT:w" s="T620">dʼagarzittə</ts>
                  <nts id="Seg_2517" n="HIAT:ip">.</nts>
                  <nts id="Seg_2518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T635" id="Seg_2520" n="HIAT:u" s="T621">
                  <ts e="T622" id="Seg_2522" n="HIAT:w" s="T621">A</ts>
                  <nts id="Seg_2523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2525" n="HIAT:w" s="T622">dĭ</ts>
                  <nts id="Seg_2526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2527" n="HIAT:ip">(</nts>
                  <ts e="T624" id="Seg_2529" n="HIAT:w" s="T623">ked-</ts>
                  <nts id="Seg_2530" n="HIAT:ip">)</nts>
                  <nts id="Seg_2531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2533" n="HIAT:w" s="T624">kădedə</ts>
                  <nts id="Seg_2534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2536" n="HIAT:w" s="T625">udabə</ts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2539" n="HIAT:w" s="T626">bar</ts>
                  <nts id="Seg_2540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2542" n="HIAT:w" s="T627">băʔpi</ts>
                  <nts id="Seg_2543" n="HIAT:ip">,</nts>
                  <nts id="Seg_2544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2546" n="HIAT:w" s="T628">măn</ts>
                  <nts id="Seg_2547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2549" n="HIAT:w" s="T629">măndəm:</ts>
                  <nts id="Seg_2550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2551" n="HIAT:ip">"</nts>
                  <ts e="T631" id="Seg_2553" n="HIAT:w" s="T630">Kĭnzeʔ</ts>
                  <nts id="Seg_2554" n="HIAT:ip">"</nts>
                  <nts id="Seg_2555" n="HIAT:ip">,</nts>
                  <nts id="Seg_2556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2558" n="HIAT:w" s="T631">dĭ</ts>
                  <nts id="Seg_2559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2561" n="HIAT:w" s="T632">kĭnzəbi</ts>
                  <nts id="Seg_2562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2564" n="HIAT:w" s="T633">i</ts>
                  <nts id="Seg_2565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2567" n="HIAT:w" s="T634">sarbi</ts>
                  <nts id="Seg_2568" n="HIAT:ip">.</nts>
                  <nts id="Seg_2569" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T639" id="Seg_2571" n="HIAT:u" s="T635">
                  <ts e="T636" id="Seg_2573" n="HIAT:w" s="T635">Dĭgəttə</ts>
                  <nts id="Seg_2574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2576" n="HIAT:w" s="T636">ej</ts>
                  <nts id="Seg_2577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2579" n="HIAT:w" s="T637">ĭzembi</ts>
                  <nts id="Seg_2580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2582" n="HIAT:w" s="T638">udat</ts>
                  <nts id="Seg_2583" n="HIAT:ip">.</nts>
                  <nts id="Seg_2584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T640" id="Seg_2586" n="HIAT:u" s="T639">
                  <nts id="Seg_2587" n="HIAT:ip">(</nts>
                  <nts id="Seg_2588" n="HIAT:ip">(</nts>
                  <ats e="T640" id="Seg_2589" n="HIAT:non-pho" s="T639">BRK</ats>
                  <nts id="Seg_2590" n="HIAT:ip">)</nts>
                  <nts id="Seg_2591" n="HIAT:ip">)</nts>
                  <nts id="Seg_2592" n="HIAT:ip">.</nts>
                  <nts id="Seg_2593" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T643" id="Seg_2595" n="HIAT:u" s="T640">
                  <ts e="T641" id="Seg_2597" n="HIAT:w" s="T640">Miʔ</ts>
                  <nts id="Seg_2598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2600" n="HIAT:w" s="T641">nagobiʔi</ts>
                  <nts id="Seg_2601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2603" n="HIAT:w" s="T642">doktărəʔi</ts>
                  <nts id="Seg_2604" n="HIAT:ip">.</nts>
                  <nts id="Seg_2605" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T648" id="Seg_2607" n="HIAT:u" s="T643">
                  <ts e="T644" id="Seg_2609" n="HIAT:w" s="T643">Miʔ</ts>
                  <nts id="Seg_2610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2611" n="HIAT:ip">(</nts>
                  <ts e="T645" id="Seg_2613" n="HIAT:w" s="T644">noʔ-</ts>
                  <nts id="Seg_2614" n="HIAT:ip">)</nts>
                  <nts id="Seg_2615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2617" n="HIAT:w" s="T645">nozi</ts>
                  <nts id="Seg_2618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2620" n="HIAT:w" s="T646">bar</ts>
                  <nts id="Seg_2621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2623" n="HIAT:w" s="T647">dʼazirbibaʔ</ts>
                  <nts id="Seg_2624" n="HIAT:ip">.</nts>
                  <nts id="Seg_2625" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T651" id="Seg_2627" n="HIAT:u" s="T648">
                  <ts e="T649" id="Seg_2629" n="HIAT:w" s="T648">Mĭnzerleʔbə</ts>
                  <nts id="Seg_2630" n="HIAT:ip">,</nts>
                  <nts id="Seg_2631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2633" n="HIAT:w" s="T649">dĭgəttə</ts>
                  <nts id="Seg_2634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2636" n="HIAT:w" s="T650">bĭtleʔbə</ts>
                  <nts id="Seg_2637" n="HIAT:ip">.</nts>
                  <nts id="Seg_2638" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T655" id="Seg_2640" n="HIAT:u" s="T651">
                  <ts e="T652" id="Seg_2642" n="HIAT:w" s="T651">Onʼiʔ</ts>
                  <nts id="Seg_2643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2645" n="HIAT:w" s="T652">no</ts>
                  <nts id="Seg_2646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2648" n="HIAT:w" s="T653">ige</ts>
                  <nts id="Seg_2649" n="HIAT:ip">,</nts>
                  <nts id="Seg_2650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2652" n="HIAT:w" s="T654">putʼəga</ts>
                  <nts id="Seg_2653" n="HIAT:ip">.</nts>
                  <nts id="Seg_2654" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T662" id="Seg_2656" n="HIAT:u" s="T655">
                  <ts e="T656" id="Seg_2658" n="HIAT:w" s="T655">Žaludkə</ts>
                  <nts id="Seg_2659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2661" n="HIAT:w" s="T656">ĭzemnie</ts>
                  <nts id="Seg_2662" n="HIAT:ip">,</nts>
                  <nts id="Seg_2663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2665" n="HIAT:w" s="T657">dĭm</ts>
                  <nts id="Seg_2666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2668" n="HIAT:w" s="T658">mĭnzerliel</ts>
                  <nts id="Seg_2669" n="HIAT:ip">,</nts>
                  <nts id="Seg_2670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2672" n="HIAT:w" s="T659">bĭtliel</ts>
                  <nts id="Seg_2673" n="HIAT:ip">,</nts>
                  <nts id="Seg_2674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2676" n="HIAT:w" s="T660">dĭgəttə</ts>
                  <nts id="Seg_2677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2679" n="HIAT:w" s="T661">amorlal</ts>
                  <nts id="Seg_2680" n="HIAT:ip">.</nts>
                  <nts id="Seg_2681" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T670" id="Seg_2683" n="HIAT:u" s="T662">
                  <ts e="T663" id="Seg_2685" n="HIAT:w" s="T662">A</ts>
                  <nts id="Seg_2686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2688" n="HIAT:w" s="T663">onʼiʔ</ts>
                  <nts id="Seg_2689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2690" n="HIAT:ip">(</nts>
                  <ts e="T665" id="Seg_2692" n="HIAT:w" s="T664">no-</ts>
                  <nts id="Seg_2693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2695" n="HIAT:w" s="T665">i-</ts>
                  <nts id="Seg_2696" n="HIAT:ip">)</nts>
                  <nts id="Seg_2697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2699" n="HIAT:w" s="T666">noʔ</ts>
                  <nts id="Seg_2700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2702" n="HIAT:w" s="T667">ige</ts>
                  <nts id="Seg_2703" n="HIAT:ip">,</nts>
                  <nts id="Seg_2704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2705" n="HIAT:ip">(</nts>
                  <ts e="T669" id="Seg_2707" n="HIAT:w" s="T668">filitʼagɨ</ts>
                  <nts id="Seg_2708" n="HIAT:ip">)</nts>
                  <nts id="Seg_2709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2711" n="HIAT:w" s="T669">numəjlieʔi</ts>
                  <nts id="Seg_2712" n="HIAT:ip">.</nts>
                  <nts id="Seg_2713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T683" id="Seg_2715" n="HIAT:u" s="T670">
                  <ts e="T671" id="Seg_2717" n="HIAT:w" s="T670">Dĭm</ts>
                  <nts id="Seg_2718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2720" n="HIAT:w" s="T671">ellil</ts>
                  <nts id="Seg_2721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2723" n="HIAT:w" s="T672">aranə</ts>
                  <nts id="Seg_2724" n="HIAT:ip">,</nts>
                  <nts id="Seg_2725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2727" n="HIAT:w" s="T673">dĭ</ts>
                  <nts id="Seg_2728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2730" n="HIAT:w" s="T674">nugəj</ts>
                  <nts id="Seg_2731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2733" n="HIAT:w" s="T675">bʼeʔ</ts>
                  <nts id="Seg_2734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2736" n="HIAT:w" s="T676">šide</ts>
                  <nts id="Seg_2737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2739" n="HIAT:w" s="T677">dʼala</ts>
                  <nts id="Seg_2740" n="HIAT:ip">,</nts>
                  <nts id="Seg_2741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2743" n="HIAT:w" s="T678">dĭgəttə</ts>
                  <nts id="Seg_2744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2746" n="HIAT:w" s="T679">bĭtlel</ts>
                  <nts id="Seg_2747" n="HIAT:ip">,</nts>
                  <nts id="Seg_2748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2750" n="HIAT:w" s="T680">sĭjdə</ts>
                  <nts id="Seg_2751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2753" n="HIAT:w" s="T681">jakšə</ts>
                  <nts id="Seg_2754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2756" n="HIAT:w" s="T682">molal</ts>
                  <nts id="Seg_2757" n="HIAT:ip">.</nts>
                  <nts id="Seg_2758" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T684" id="Seg_2760" n="HIAT:u" s="T683">
                  <nts id="Seg_2761" n="HIAT:ip">(</nts>
                  <nts id="Seg_2762" n="HIAT:ip">(</nts>
                  <ats e="T684" id="Seg_2763" n="HIAT:non-pho" s="T683">BRK</ats>
                  <nts id="Seg_2764" n="HIAT:ip">)</nts>
                  <nts id="Seg_2765" n="HIAT:ip">)</nts>
                  <nts id="Seg_2766" n="HIAT:ip">.</nts>
                  <nts id="Seg_2767" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T691" id="Seg_2769" n="HIAT:u" s="T684">
                  <nts id="Seg_2770" n="HIAT:ip">(</nts>
                  <ts e="T685" id="Seg_2772" n="HIAT:w" s="T684">Tăn</ts>
                  <nts id="Seg_2773" n="HIAT:ip">)</nts>
                  <nts id="Seg_2774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2776" n="HIAT:w" s="T685">miʔ</ts>
                  <nts id="Seg_2777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2779" n="HIAT:w" s="T686">nuzaŋdən</ts>
                  <nts id="Seg_2780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2781" n="HIAT:ip">(</nts>
                  <ts e="T688" id="Seg_2783" n="HIAT:w" s="T687">Maslenkagən</ts>
                  <nts id="Seg_2784" n="HIAT:ip">)</nts>
                  <nts id="Seg_2785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2787" n="HIAT:w" s="T688">bar</ts>
                  <nts id="Seg_2788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2789" n="HIAT:ip">(</nts>
                  <ts e="T690" id="Seg_2791" n="HIAT:w" s="T689">inetsi</ts>
                  <nts id="Seg_2792" n="HIAT:ip">)</nts>
                  <nts id="Seg_2793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2795" n="HIAT:w" s="T690">tunolaʔbəʔjə</ts>
                  <nts id="Seg_2796" n="HIAT:ip">.</nts>
                  <nts id="Seg_2797" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T699" id="Seg_2799" n="HIAT:u" s="T691">
                  <ts e="T692" id="Seg_2801" n="HIAT:w" s="T691">Šindin</ts>
                  <nts id="Seg_2802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2804" n="HIAT:w" s="T692">inet</ts>
                  <nts id="Seg_2805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2807" n="HIAT:w" s="T693">büžüj</ts>
                  <nts id="Seg_2808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2810" n="HIAT:w" s="T694">šoləj</ts>
                  <nts id="Seg_2811" n="HIAT:ip">,</nts>
                  <nts id="Seg_2812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2814" n="HIAT:w" s="T695">a</ts>
                  <nts id="Seg_2815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2817" n="HIAT:w" s="T696">šindin</ts>
                  <nts id="Seg_2818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2820" n="HIAT:w" s="T697">inet</ts>
                  <nts id="Seg_2821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2823" n="HIAT:w" s="T698">maləj</ts>
                  <nts id="Seg_2824" n="HIAT:ip">.</nts>
                  <nts id="Seg_2825" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T707" id="Seg_2827" n="HIAT:u" s="T699">
                  <ts e="T700" id="Seg_2829" n="HIAT:w" s="T699">Dĭ</ts>
                  <nts id="Seg_2830" n="HIAT:ip">,</nts>
                  <nts id="Seg_2831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2833" n="HIAT:w" s="T700">šindin</ts>
                  <nts id="Seg_2834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2836" n="HIAT:w" s="T701">maləj</ts>
                  <nts id="Seg_2837" n="HIAT:ip">,</nts>
                  <nts id="Seg_2838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2840" n="HIAT:w" s="T702">dĭ</ts>
                  <nts id="Seg_2841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2843" n="HIAT:w" s="T703">четверть</ts>
                  <nts id="Seg_2844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2845" n="HIAT:ip">(</nts>
                  <ts e="T705" id="Seg_2847" n="HIAT:w" s="T704">ar-</ts>
                  <nts id="Seg_2848" n="HIAT:ip">)</nts>
                  <nts id="Seg_2849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2851" n="HIAT:w" s="T705">ara</ts>
                  <nts id="Seg_2852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2854" n="HIAT:w" s="T706">nuldələj</ts>
                  <nts id="Seg_2855" n="HIAT:ip">.</nts>
                  <nts id="Seg_2856" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T708" id="Seg_2858" n="HIAT:u" s="T707">
                  <nts id="Seg_2859" n="HIAT:ip">(</nts>
                  <nts id="Seg_2860" n="HIAT:ip">(</nts>
                  <ats e="T708" id="Seg_2861" n="HIAT:non-pho" s="T707">BRK</ats>
                  <nts id="Seg_2862" n="HIAT:ip">)</nts>
                  <nts id="Seg_2863" n="HIAT:ip">)</nts>
                  <nts id="Seg_2864" n="HIAT:ip">.</nts>
                  <nts id="Seg_2865" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T712" id="Seg_2867" n="HIAT:u" s="T708">
                  <ts e="T709" id="Seg_2869" n="HIAT:w" s="T708">Dĭgəttə</ts>
                  <nts id="Seg_2870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2872" n="HIAT:w" s="T709">baška</ts>
                  <nts id="Seg_2873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2875" n="HIAT:w" s="T710">ine</ts>
                  <nts id="Seg_2876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2878" n="HIAT:w" s="T711">detləʔi</ts>
                  <nts id="Seg_2879" n="HIAT:ip">.</nts>
                  <nts id="Seg_2880" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T720" id="Seg_2882" n="HIAT:u" s="T712">
                  <ts e="T713" id="Seg_2884" n="HIAT:w" s="T712">A</ts>
                  <nts id="Seg_2885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2886" n="HIAT:ip">(</nts>
                  <ts e="T714" id="Seg_2888" n="HIAT:w" s="T713">dĭ=</ts>
                  <nts id="Seg_2889" n="HIAT:ip">)</nts>
                  <nts id="Seg_2890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2892" n="HIAT:w" s="T714">dö</ts>
                  <nts id="Seg_2893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2895" n="HIAT:w" s="T715">ine</ts>
                  <nts id="Seg_2896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2898" n="HIAT:w" s="T716">ugandə</ts>
                  <nts id="Seg_2899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2901" n="HIAT:w" s="T717">бегунец</ts>
                  <nts id="Seg_2902" n="HIAT:ip">,</nts>
                  <nts id="Seg_2903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2905" n="HIAT:w" s="T718">mănliaʔi</ts>
                  <nts id="Seg_2906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2908" n="HIAT:w" s="T719">dăre</ts>
                  <nts id="Seg_2909" n="HIAT:ip">.</nts>
                  <nts id="Seg_2910" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T724" id="Seg_2912" n="HIAT:u" s="T720">
                  <ts e="T721" id="Seg_2914" n="HIAT:w" s="T720">Dĭgəttə</ts>
                  <nts id="Seg_2915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2917" n="HIAT:w" s="T721">dĭm</ts>
                  <nts id="Seg_2918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2920" n="HIAT:w" s="T722">tože</ts>
                  <nts id="Seg_2921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2923" n="HIAT:w" s="T723">maːluʔləj</ts>
                  <nts id="Seg_2924" n="HIAT:ip">.</nts>
                  <nts id="Seg_2925" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T728" id="Seg_2927" n="HIAT:u" s="T724">
                  <ts e="T725" id="Seg_2929" n="HIAT:w" s="T724">Döbər</ts>
                  <nts id="Seg_2930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2931" n="HIAT:ip">(</nts>
                  <ts e="T726" id="Seg_2933" n="HIAT:w" s="T725">šo-</ts>
                  <nts id="Seg_2934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2936" n="HIAT:w" s="T726">šo-</ts>
                  <nts id="Seg_2937" n="HIAT:ip">)</nts>
                  <nts id="Seg_2938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2940" n="HIAT:w" s="T727">nuʔməluʔləj</ts>
                  <nts id="Seg_2941" n="HIAT:ip">.</nts>
                  <nts id="Seg_2942" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T732" id="Seg_2944" n="HIAT:u" s="T728">
                  <ts e="T729" id="Seg_2946" n="HIAT:w" s="T728">A</ts>
                  <nts id="Seg_2947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2949" n="HIAT:w" s="T729">dĭ</ts>
                  <nts id="Seg_2950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2952" n="HIAT:w" s="T730">ine</ts>
                  <nts id="Seg_2953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2955" n="HIAT:w" s="T731">maːluʔləj</ts>
                  <nts id="Seg_2956" n="HIAT:ip">.</nts>
                  <nts id="Seg_2957" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T733" id="Seg_2959" n="HIAT:u" s="T732">
                  <nts id="Seg_2960" n="HIAT:ip">(</nts>
                  <nts id="Seg_2961" n="HIAT:ip">(</nts>
                  <ats e="T733" id="Seg_2962" n="HIAT:non-pho" s="T732">BRK</ats>
                  <nts id="Seg_2963" n="HIAT:ip">)</nts>
                  <nts id="Seg_2964" n="HIAT:ip">)</nts>
                  <nts id="Seg_2965" n="HIAT:ip">.</nts>
                  <nts id="Seg_2966" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T739" id="Seg_2968" n="HIAT:u" s="T733">
                  <ts e="T734" id="Seg_2970" n="HIAT:w" s="T733">Dĭgəttə</ts>
                  <nts id="Seg_2971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2973" n="HIAT:w" s="T734">dĭ</ts>
                  <nts id="Seg_2974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2976" n="HIAT:w" s="T735">inem</ts>
                  <nts id="Seg_2977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2978" n="HIAT:ip">(</nts>
                  <ts e="T737" id="Seg_2980" n="HIAT:w" s="T736">пот</ts>
                  <nts id="Seg_2981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2983" n="HIAT:w" s="T737">ilie-</ts>
                  <nts id="Seg_2984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2986" n="HIAT:w" s="T738">ilie</ts>
                  <nts id="Seg_2987" n="HIAT:ip">)</nts>
                  <nts id="Seg_2988" n="HIAT:ip">.</nts>
                  <nts id="Seg_2989" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T746" id="Seg_2991" n="HIAT:u" s="T739">
                  <ts e="T740" id="Seg_2993" n="HIAT:w" s="T739">I</ts>
                  <nts id="Seg_2994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2996" n="HIAT:w" s="T740">mĭŋge</ts>
                  <nts id="Seg_2997" n="HIAT:ip">,</nts>
                  <nts id="Seg_2998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_3000" n="HIAT:w" s="T741">mĭnge</ts>
                  <nts id="Seg_3001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_3003" n="HIAT:w" s="T742">koldʼəŋ</ts>
                  <nts id="Seg_3004" n="HIAT:ip">,</nts>
                  <nts id="Seg_3005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_3007" n="HIAT:w" s="T743">štobɨ</ts>
                  <nts id="Seg_3008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_3010" n="HIAT:w" s="T744">ej</ts>
                  <nts id="Seg_3011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_3013" n="HIAT:w" s="T745">nubi</ts>
                  <nts id="Seg_3014" n="HIAT:ip">.</nts>
                  <nts id="Seg_3015" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T747" id="Seg_3017" n="HIAT:u" s="T746">
                  <nts id="Seg_3018" n="HIAT:ip">(</nts>
                  <nts id="Seg_3019" n="HIAT:ip">(</nts>
                  <ats e="T747" id="Seg_3020" n="HIAT:non-pho" s="T746">BRK</ats>
                  <nts id="Seg_3021" n="HIAT:ip">)</nts>
                  <nts id="Seg_3022" n="HIAT:ip">)</nts>
                  <nts id="Seg_3023" n="HIAT:ip">.</nts>
                  <nts id="Seg_3024" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T751" id="Seg_3026" n="HIAT:u" s="T747">
                  <ts e="T748" id="Seg_3028" n="HIAT:w" s="T747">Dĭgəttə</ts>
                  <nts id="Seg_3029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_3031" n="HIAT:w" s="T748">ara</ts>
                  <nts id="Seg_3032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_3034" n="HIAT:w" s="T749">iʔgö</ts>
                  <nts id="Seg_3035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_3037" n="HIAT:w" s="T750">bĭtluʔləʔi</ts>
                  <nts id="Seg_3038" n="HIAT:ip">.</nts>
                  <nts id="Seg_3039" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T754" id="Seg_3041" n="HIAT:u" s="T751">
                  <ts e="T752" id="Seg_3043" n="HIAT:w" s="T751">I</ts>
                  <nts id="Seg_3044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_3046" n="HIAT:w" s="T752">bar</ts>
                  <nts id="Seg_3047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_3049" n="HIAT:w" s="T753">saʔməluʔləʔjə</ts>
                  <nts id="Seg_3050" n="HIAT:ip">.</nts>
                  <nts id="Seg_3051" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T755" id="Seg_3053" n="HIAT:u" s="T754">
                  <nts id="Seg_3054" n="HIAT:ip">(</nts>
                  <nts id="Seg_3055" n="HIAT:ip">(</nts>
                  <ats e="T755" id="Seg_3056" n="HIAT:non-pho" s="T754">BRK</ats>
                  <nts id="Seg_3057" n="HIAT:ip">)</nts>
                  <nts id="Seg_3058" n="HIAT:ip">)</nts>
                  <nts id="Seg_3059" n="HIAT:ip">.</nts>
                  <nts id="Seg_3060" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T760" id="Seg_3062" n="HIAT:u" s="T755">
                  <ts e="T756" id="Seg_3064" n="HIAT:w" s="T755">Dĭgəttə</ts>
                  <nts id="Seg_3065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_3067" n="HIAT:w" s="T756">tibizeŋ</ts>
                  <nts id="Seg_3068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_3070" n="HIAT:w" s="T757">paʔi</ts>
                  <nts id="Seg_3071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_3073" n="HIAT:w" s="T758">iləʔi</ts>
                  <nts id="Seg_3074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_3076" n="HIAT:w" s="T759">udandə</ts>
                  <nts id="Seg_3077" n="HIAT:ip">.</nts>
                  <nts id="Seg_3078" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T764" id="Seg_3080" n="HIAT:u" s="T760">
                  <ts e="T761" id="Seg_3082" n="HIAT:w" s="T760">I</ts>
                  <nts id="Seg_3083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_3085" n="HIAT:w" s="T761">dĭgəttə</ts>
                  <nts id="Seg_3086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3087" n="HIAT:ip">(</nts>
                  <ts e="T764" id="Seg_3089" n="HIAT:w" s="T762">nu-</ts>
                  <nts id="Seg_3090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3091" n="HIAT:ip">)</nts>
                  <nts id="Seg_3092" n="HIAT:ip">…</nts>
                  <nts id="Seg_3093" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T766" id="Seg_3095" n="HIAT:u" s="T764">
                  <ts e="T764.tx.1" id="Seg_3097" n="HIAT:w" s="T764">Останови</ts>
                  <nts id="Seg_3098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_3100" n="HIAT:w" s="T764.tx.1">маленько</ts>
                  <nts id="Seg_3101" n="HIAT:ip">.</nts>
                  <nts id="Seg_3102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T767" id="Seg_3104" n="HIAT:u" s="T766">
                  <nts id="Seg_3105" n="HIAT:ip">(</nts>
                  <nts id="Seg_3106" n="HIAT:ip">(</nts>
                  <ats e="T767" id="Seg_3107" n="HIAT:non-pho" s="T766">BRK</ats>
                  <nts id="Seg_3108" n="HIAT:ip">)</nts>
                  <nts id="Seg_3109" n="HIAT:ip">)</nts>
                  <nts id="Seg_3110" n="HIAT:ip">.</nts>
                  <nts id="Seg_3111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T768" id="Seg_3113" n="HIAT:u" s="T767">
                  <nts id="Seg_3114" n="HIAT:ip">(</nts>
                  <ts e="T768" id="Seg_3116" n="HIAT:w" s="T767">-parnuʔ</ts>
                  <nts id="Seg_3117" n="HIAT:ip">)</nts>
                  <nts id="Seg_3118" n="HIAT:ip">.</nts>
                  <nts id="Seg_3119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T775" id="Seg_3121" n="HIAT:u" s="T768">
                  <ts e="T769" id="Seg_3123" n="HIAT:w" s="T768">Dĭgəttə</ts>
                  <nts id="Seg_3124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_3126" n="HIAT:w" s="T769">onʼiʔ</ts>
                  <nts id="Seg_3127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3128" n="HIAT:ip">(</nts>
                  <ts e="T771" id="Seg_3130" n="HIAT:w" s="T770">onʼiʔtə</ts>
                  <nts id="Seg_3131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_3133" n="HIAT:w" s="T771">bar</ts>
                  <nts id="Seg_3134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_3136" n="HIAT:w" s="T772">nuʔ</ts>
                  <nts id="Seg_3137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3138" n="HIAT:ip">(</nts>
                  <ts e="T774" id="Seg_3140" n="HIAT:w" s="T773">n-</ts>
                  <nts id="Seg_3141" n="HIAT:ip">)</nts>
                  <nts id="Seg_3142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_3144" n="HIAT:w" s="T774">nʼotlaʔbə</ts>
                  <nts id="Seg_3145" n="HIAT:ip">.</nts>
                  <nts id="Seg_3146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T780" id="Seg_3148" n="HIAT:u" s="T775">
                  <ts e="T776" id="Seg_3150" n="HIAT:w" s="T775">Dĭgəttə</ts>
                  <nts id="Seg_3151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_3153" n="HIAT:w" s="T776">onʼiʔ</ts>
                  <nts id="Seg_3154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_3156" n="HIAT:w" s="T777">kuštü</ts>
                  <nts id="Seg_3157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3158" n="HIAT:ip">(</nts>
                  <ts e="T780" id="Seg_3160" n="HIAT:w" s="T778">bašk-</ts>
                  <nts id="Seg_3161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3162" n="HIAT:ip">)</nts>
                  <nts id="Seg_3163" n="HIAT:ip">…</nts>
                  <nts id="Seg_3164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T785" id="Seg_3166" n="HIAT:u" s="T780">
                  <nts id="Seg_3167" n="HIAT:ip">(</nts>
                  <ts e="T781" id="Seg_3169" n="HIAT:w" s="T780">Dĭ-</ts>
                  <nts id="Seg_3170" n="HIAT:ip">)</nts>
                  <nts id="Seg_3171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_3173" n="HIAT:w" s="T781">Dĭm</ts>
                  <nts id="Seg_3174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_3176" n="HIAT:w" s="T782">bar</ts>
                  <nts id="Seg_3177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_3179" n="HIAT:w" s="T783">ujundə</ts>
                  <nts id="Seg_3180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_3182" n="HIAT:w" s="T784">nuldəluləj</ts>
                  <nts id="Seg_3183" n="HIAT:ip">.</nts>
                  <nts id="Seg_3184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T790" id="Seg_3186" n="HIAT:u" s="T785">
                  <ts e="T786" id="Seg_3188" n="HIAT:w" s="T785">Dĭ</ts>
                  <nts id="Seg_3189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_3191" n="HIAT:w" s="T786">bar</ts>
                  <nts id="Seg_3192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_3194" n="HIAT:w" s="T787">dĭgəttə</ts>
                  <nts id="Seg_3195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_3197" n="HIAT:w" s="T788">ara</ts>
                  <nts id="Seg_3198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_3200" n="HIAT:w" s="T789">nuluʔləj</ts>
                  <nts id="Seg_3201" n="HIAT:ip">.</nts>
                  <nts id="Seg_3202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T792" id="Seg_3204" n="HIAT:u" s="T790">
                  <ts e="T791" id="Seg_3206" n="HIAT:w" s="T790">Dĭgəttə</ts>
                  <nts id="Seg_3207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_3209" n="HIAT:w" s="T791">bĭtlieʔi</ts>
                  <nts id="Seg_3210" n="HIAT:ip">.</nts>
                  <nts id="Seg_3211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T793" id="Seg_3213" n="HIAT:u" s="T792">
                  <nts id="Seg_3214" n="HIAT:ip">(</nts>
                  <nts id="Seg_3215" n="HIAT:ip">(</nts>
                  <ats e="T793" id="Seg_3216" n="HIAT:non-pho" s="T792">BRK</ats>
                  <nts id="Seg_3217" n="HIAT:ip">)</nts>
                  <nts id="Seg_3218" n="HIAT:ip">)</nts>
                  <nts id="Seg_3219" n="HIAT:ip">.</nts>
                  <nts id="Seg_3220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T802" id="Seg_3222" n="HIAT:u" s="T793">
                  <ts e="T794" id="Seg_3224" n="HIAT:w" s="T793">Dĭgəttə</ts>
                  <nts id="Seg_3225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3226" n="HIAT:ip">(</nts>
                  <ts e="T795" id="Seg_3228" n="HIAT:w" s="T794">šidegöʔ</ts>
                  <nts id="Seg_3229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_3231" n="HIAT:w" s="T795">nuzaŋ</ts>
                  <nts id="Seg_3232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_3234" n="HIAT:w" s="T796">a-</ts>
                  <nts id="Seg_3235" n="HIAT:ip">)</nts>
                  <nts id="Seg_3236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_3238" n="HIAT:w" s="T797">šidegöʔ</ts>
                  <nts id="Seg_3239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_3241" n="HIAT:w" s="T798">nuzaŋ</ts>
                  <nts id="Seg_3242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_3244" n="HIAT:w" s="T799">iləʔi</ts>
                  <nts id="Seg_3245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_3247" n="HIAT:w" s="T800">onʼiʔ</ts>
                  <nts id="Seg_3248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_3250" n="HIAT:w" s="T801">onʼiʔtə</ts>
                  <nts id="Seg_3251" n="HIAT:ip">.</nts>
                  <nts id="Seg_3252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T808" id="Seg_3254" n="HIAT:u" s="T802">
                  <ts e="T803" id="Seg_3256" n="HIAT:w" s="T802">Dĭgəttə</ts>
                  <nts id="Seg_3257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_3259" n="HIAT:w" s="T803">kajət</ts>
                  <nts id="Seg_3260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_3262" n="HIAT:w" s="T804">kuštü</ts>
                  <nts id="Seg_3263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_3265" n="HIAT:w" s="T805">döm</ts>
                  <nts id="Seg_3266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3267" n="HIAT:ip">(</nts>
                  <ts e="T807" id="Seg_3269" n="HIAT:w" s="T806">parəʔluj</ts>
                  <nts id="Seg_3270" n="HIAT:ip">)</nts>
                  <nts id="Seg_3271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_3273" n="HIAT:w" s="T807">dʼünə</ts>
                  <nts id="Seg_3274" n="HIAT:ip">.</nts>
                  <nts id="Seg_3275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T813" id="Seg_3277" n="HIAT:u" s="T808">
                  <ts e="T809" id="Seg_3279" n="HIAT:w" s="T808">Dĭgəttə</ts>
                  <nts id="Seg_3280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_3282" n="HIAT:w" s="T809">dĭ</ts>
                  <nts id="Seg_3283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_3285" n="HIAT:w" s="T810">ara</ts>
                  <nts id="Seg_3286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_3288" n="HIAT:w" s="T811">nuldlia</ts>
                  <nts id="Seg_3289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3290" n="HIAT:ip">(</nts>
                  <ts e="T813" id="Seg_3292" n="HIAT:w" s="T812">dĭʔnə</ts>
                  <nts id="Seg_3293" n="HIAT:ip">)</nts>
                  <nts id="Seg_3294" n="HIAT:ip">.</nts>
                  <nts id="Seg_3295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T814" id="Seg_3297" n="HIAT:u" s="T813">
                  <ts e="T814" id="Seg_3299" n="HIAT:w" s="T813">Bĭtleʔbəʔjə</ts>
                  <nts id="Seg_3300" n="HIAT:ip">.</nts>
                  <nts id="Seg_3301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T815" id="Seg_3303" n="HIAT:u" s="T814">
                  <nts id="Seg_3304" n="HIAT:ip">(</nts>
                  <nts id="Seg_3305" n="HIAT:ip">(</nts>
                  <ats e="T815" id="Seg_3306" n="HIAT:non-pho" s="T814">BRK</ats>
                  <nts id="Seg_3307" n="HIAT:ip">)</nts>
                  <nts id="Seg_3308" n="HIAT:ip">)</nts>
                  <nts id="Seg_3309" n="HIAT:ip">.</nts>
                  <nts id="Seg_3310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T821" id="Seg_3312" n="HIAT:u" s="T815">
                  <ts e="T816" id="Seg_3314" n="HIAT:w" s="T815">Dĭgəttə</ts>
                  <nts id="Seg_3315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_3317" n="HIAT:w" s="T816">dʼünə</ts>
                  <nts id="Seg_3318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_3320" n="HIAT:w" s="T817">barəʔluj</ts>
                  <nts id="Seg_3321" n="HIAT:ip">,</nts>
                  <nts id="Seg_3322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_3324" n="HIAT:w" s="T818">udabə</ts>
                  <nts id="Seg_3325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_3327" n="HIAT:w" s="T819">bar</ts>
                  <nts id="Seg_3328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_3330" n="HIAT:w" s="T820">büldləj</ts>
                  <nts id="Seg_3331" n="HIAT:ip">.</nts>
                  <nts id="Seg_3332" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T822" id="Seg_3334" n="HIAT:u" s="T821">
                  <nts id="Seg_3335" n="HIAT:ip">(</nts>
                  <nts id="Seg_3336" n="HIAT:ip">(</nts>
                  <ats e="T822" id="Seg_3337" n="HIAT:non-pho" s="T821">BRK</ats>
                  <nts id="Seg_3338" n="HIAT:ip">)</nts>
                  <nts id="Seg_3339" n="HIAT:ip">)</nts>
                  <nts id="Seg_3340" n="HIAT:ip">.</nts>
                  <nts id="Seg_3341" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T825" id="Seg_3343" n="HIAT:u" s="T822">
                  <ts e="T823" id="Seg_3345" n="HIAT:w" s="T822">Вот</ts>
                  <nts id="Seg_3346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_3348" n="HIAT:w" s="T823">vărotaʔi</ts>
                  <nts id="Seg_3349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3351" n="HIAT:w" s="T824">nulaʔbəʔjə</ts>
                  <nts id="Seg_3352" n="HIAT:ip">.</nts>
                  <nts id="Seg_3353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T829" id="Seg_3355" n="HIAT:u" s="T825">
                  <ts e="T826" id="Seg_3357" n="HIAT:w" s="T825">Kanaʔ</ts>
                  <nts id="Seg_3358" n="HIAT:ip">,</nts>
                  <nts id="Seg_3359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_3361" n="HIAT:w" s="T826">girgit</ts>
                  <nts id="Seg_3362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_3364" n="HIAT:w" s="T827">aʔtʼinə</ts>
                  <nts id="Seg_3365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_3367" n="HIAT:w" s="T828">kalal</ts>
                  <nts id="Seg_3368" n="HIAT:ip">.</nts>
                  <nts id="Seg_3369" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T834" id="Seg_3371" n="HIAT:u" s="T829">
                  <ts e="T830" id="Seg_3373" n="HIAT:w" s="T829">Pravăj</ts>
                  <nts id="Seg_3374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T831" id="Seg_3376" n="HIAT:w" s="T830">aʔtʼi</ts>
                  <nts id="Seg_3377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3379" n="HIAT:w" s="T831">bar</ts>
                  <nts id="Seg_3380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_3382" n="HIAT:w" s="T832">ej</ts>
                  <nts id="Seg_3383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_3385" n="HIAT:w" s="T833">jakše</ts>
                  <nts id="Seg_3386" n="HIAT:ip">.</nts>
                  <nts id="Seg_3387" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T836" id="Seg_3389" n="HIAT:u" s="T834">
                  <ts e="T835" id="Seg_3391" n="HIAT:w" s="T834">Bar</ts>
                  <nts id="Seg_3392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_3394" n="HIAT:w" s="T835">bălotăʔi</ts>
                  <nts id="Seg_3395" n="HIAT:ip">.</nts>
                  <nts id="Seg_3396" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T838" id="Seg_3398" n="HIAT:u" s="T836">
                  <ts e="T837" id="Seg_3400" n="HIAT:w" s="T836">Paʔi</ts>
                  <nts id="Seg_3401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_3403" n="HIAT:w" s="T837">nugaʔi</ts>
                  <nts id="Seg_3404" n="HIAT:ip">.</nts>
                  <nts id="Seg_3405" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T841" id="Seg_3407" n="HIAT:u" s="T838">
                  <ts e="T839" id="Seg_3409" n="HIAT:w" s="T838">I</ts>
                  <nts id="Seg_3410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_3412" n="HIAT:w" s="T839">bar</ts>
                  <nts id="Seg_3413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_3415" n="HIAT:w" s="T840">grozaʔi</ts>
                  <nts id="Seg_3416" n="HIAT:ip">.</nts>
                  <nts id="Seg_3417" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T843" id="Seg_3419" n="HIAT:u" s="T841">
                  <ts e="T842" id="Seg_3421" n="HIAT:w" s="T841">Beržə</ts>
                  <nts id="Seg_3422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_3424" n="HIAT:w" s="T842">bar</ts>
                  <nts id="Seg_3425" n="HIAT:ip">.</nts>
                  <nts id="Seg_3426" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T847" id="Seg_3428" n="HIAT:u" s="T843">
                  <ts e="T844" id="Seg_3430" n="HIAT:w" s="T843">Bazaj</ts>
                  <nts id="Seg_3431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_3433" n="HIAT:w" s="T844">kălʼučkaʔi</ts>
                  <nts id="Seg_3434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_3436" n="HIAT:w" s="T845">bar</ts>
                  <nts id="Seg_3437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_3439" n="HIAT:w" s="T846">mündüʔleʔbəʔjə</ts>
                  <nts id="Seg_3440" n="HIAT:ip">.</nts>
                  <nts id="Seg_3441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T858" id="Seg_3443" n="HIAT:u" s="T847">
                  <ts e="T848" id="Seg_3445" n="HIAT:w" s="T847">A</ts>
                  <nts id="Seg_3446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_3448" n="HIAT:w" s="T848">dĭbər</ts>
                  <nts id="Seg_3449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_3451" n="HIAT:w" s="T849">kuŋgəŋ</ts>
                  <nts id="Seg_3452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_3454" n="HIAT:w" s="T850">kalal</ts>
                  <nts id="Seg_3455" n="HIAT:ip">,</nts>
                  <nts id="Seg_3456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_3458" n="HIAT:w" s="T851">dĭn</ts>
                  <nts id="Seg_3459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_3461" n="HIAT:w" s="T852">bar</ts>
                  <nts id="Seg_3462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_3464" n="HIAT:w" s="T853">jakše</ts>
                  <nts id="Seg_3465" n="HIAT:ip">,</nts>
                  <nts id="Seg_3466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_3468" n="HIAT:w" s="T854">paʔi</ts>
                  <nts id="Seg_3469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_3471" n="HIAT:w" s="T855">naga</ts>
                  <nts id="Seg_3472" n="HIAT:ip">,</nts>
                  <nts id="Seg_3473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_3475" n="HIAT:w" s="T856">ĭmbidə</ts>
                  <nts id="Seg_3476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_3478" n="HIAT:w" s="T857">naga</ts>
                  <nts id="Seg_3479" n="HIAT:ip">.</nts>
                  <nts id="Seg_3480" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T860" id="Seg_3482" n="HIAT:u" s="T858">
                  <ts e="T859" id="Seg_3484" n="HIAT:w" s="T858">Ugandə</ts>
                  <nts id="Seg_3485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_3487" n="HIAT:w" s="T859">kuvas</ts>
                  <nts id="Seg_3488" n="HIAT:ip">.</nts>
                  <nts id="Seg_3489" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T866" id="Seg_3491" n="HIAT:u" s="T860">
                  <ts e="T861" id="Seg_3493" n="HIAT:w" s="T860">Noʔ</ts>
                  <nts id="Seg_3494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_3496" n="HIAT:w" s="T861">kuvas</ts>
                  <nts id="Seg_3497" n="HIAT:ip">,</nts>
                  <nts id="Seg_3498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_3500" n="HIAT:w" s="T862">всякий</ts>
                  <nts id="Seg_3501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_3503" n="HIAT:w" s="T863">svʼetogəʔi</ts>
                  <nts id="Seg_3504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_3506" n="HIAT:w" s="T864">sĭre</ts>
                  <nts id="Seg_3507" n="HIAT:ip">,</nts>
                  <nts id="Seg_3508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_3510" n="HIAT:w" s="T865">kömə</ts>
                  <nts id="Seg_3511" n="HIAT:ip">.</nts>
                  <nts id="Seg_3512" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T869" id="Seg_3514" n="HIAT:u" s="T866">
                  <ts e="T867" id="Seg_3516" n="HIAT:w" s="T866">Виноград</ts>
                  <nts id="Seg_3517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_3519" n="HIAT:w" s="T867">özerleʔbə</ts>
                  <nts id="Seg_3520" n="HIAT:ip">,</nts>
                  <nts id="Seg_3521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_3523" n="HIAT:w" s="T868">jablăkăʔi</ts>
                  <nts id="Seg_3524" n="HIAT:ip">.</nts>
                  <nts id="Seg_3525" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T879" id="Seg_3527" n="HIAT:u" s="T869">
                  <ts e="T870" id="Seg_3529" n="HIAT:w" s="T869">Kuja</ts>
                  <nts id="Seg_3530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_3532" n="HIAT:w" s="T870">mĭndlaʔbə</ts>
                  <nts id="Seg_3533" n="HIAT:ip">,</nts>
                  <nts id="Seg_3534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_3536" n="HIAT:w" s="T871">ugandə</ts>
                  <nts id="Seg_3537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_3539" n="HIAT:w" s="T872">ejü</ts>
                  <nts id="Seg_3540" n="HIAT:ip">,</nts>
                  <nts id="Seg_3541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_3543" n="HIAT:w" s="T873">xotʼ</ts>
                  <nts id="Seg_3544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_3546" n="HIAT:w" s="T874">iʔbeʔ</ts>
                  <nts id="Seg_3547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_3549" n="HIAT:w" s="T875">da</ts>
                  <nts id="Seg_3550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_3552" n="HIAT:w" s="T876">kunolaʔ</ts>
                  <nts id="Seg_3553" n="HIAT:ip">,</nts>
                  <nts id="Seg_3554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_3556" n="HIAT:w" s="T877">jakše</ts>
                  <nts id="Seg_3557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_3559" n="HIAT:w" s="T878">dĭn</ts>
                  <nts id="Seg_3560" n="HIAT:ip">.</nts>
                  <nts id="Seg_3561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T891" id="Seg_3563" n="HIAT:u" s="T879">
                  <ts e="T880" id="Seg_3565" n="HIAT:w" s="T879">A</ts>
                  <nts id="Seg_3566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T881" id="Seg_3568" n="HIAT:w" s="T880">lʼevăj</ts>
                  <nts id="Seg_3569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_3571" n="HIAT:w" s="T881">udandə</ts>
                  <nts id="Seg_3572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_3574" n="HIAT:w" s="T882">kandəga</ts>
                  <nts id="Seg_3575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_3577" n="HIAT:w" s="T883">ugandə</ts>
                  <nts id="Seg_3578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_3580" n="HIAT:w" s="T884">aktʼi</ts>
                  <nts id="Seg_3581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_3583" n="HIAT:w" s="T885">urgo</ts>
                  <nts id="Seg_3584" n="HIAT:ip">,</nts>
                  <nts id="Seg_3585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887" id="Seg_3587" n="HIAT:w" s="T886">il</ts>
                  <nts id="Seg_3588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T888" id="Seg_3590" n="HIAT:w" s="T887">iʔgö</ts>
                  <nts id="Seg_3591" n="HIAT:ip">,</nts>
                  <nts id="Seg_3592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T889" id="Seg_3594" n="HIAT:w" s="T888">bar</ts>
                  <nts id="Seg_3595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_3597" n="HIAT:w" s="T889">ara</ts>
                  <nts id="Seg_3598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_3600" n="HIAT:w" s="T890">bĭtlieʔi</ts>
                  <nts id="Seg_3601" n="HIAT:ip">.</nts>
                  <nts id="Seg_3602" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T895" id="Seg_3604" n="HIAT:u" s="T891">
                  <ts e="T892" id="Seg_3606" n="HIAT:w" s="T891">Bar</ts>
                  <nts id="Seg_3607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_3609" n="HIAT:w" s="T892">ĭmbi</ts>
                  <nts id="Seg_3610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_3612" n="HIAT:w" s="T893">amniaʔi</ts>
                  <nts id="Seg_3613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_3615" n="HIAT:w" s="T894">bar</ts>
                  <nts id="Seg_3616" n="HIAT:ip">.</nts>
                  <nts id="Seg_3617" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T899" id="Seg_3619" n="HIAT:u" s="T895">
                  <ts e="T896" id="Seg_3621" n="HIAT:w" s="T895">Suʔmileʔbəʔjə</ts>
                  <nts id="Seg_3622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_3624" n="HIAT:w" s="T896">bar</ts>
                  <nts id="Seg_3625" n="HIAT:ip">,</nts>
                  <nts id="Seg_3626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_3628" n="HIAT:w" s="T897">nüjnə</ts>
                  <nts id="Seg_3629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_3631" n="HIAT:w" s="T898">nüjleʔbəʔjə</ts>
                  <nts id="Seg_3632" n="HIAT:ip">.</nts>
                  <nts id="Seg_3633" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T905" id="Seg_3635" n="HIAT:u" s="T899">
                  <ts e="T900" id="Seg_3637" n="HIAT:w" s="T899">A</ts>
                  <nts id="Seg_3638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3639" n="HIAT:ip">(</nts>
                  <ts e="T901" id="Seg_3641" n="HIAT:w" s="T900">dĭ-</ts>
                  <nts id="Seg_3642" n="HIAT:ip">)</nts>
                  <nts id="Seg_3643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_3645" n="HIAT:w" s="T901">dĭbər</ts>
                  <nts id="Seg_3646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_3648" n="HIAT:w" s="T902">kuŋgə</ts>
                  <nts id="Seg_3649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3650" n="HIAT:ip">(</nts>
                  <ts e="T904" id="Seg_3652" n="HIAT:w" s="T903">ka-</ts>
                  <nts id="Seg_3653" n="HIAT:ip">)</nts>
                  <nts id="Seg_3654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T905" id="Seg_3656" n="HIAT:w" s="T904">kalal</ts>
                  <nts id="Seg_3657" n="HIAT:ip">.</nts>
                  <nts id="Seg_3658" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T912" id="Seg_3660" n="HIAT:u" s="T905">
                  <ts e="T906" id="Seg_3662" n="HIAT:w" s="T905">Dĭn</ts>
                  <nts id="Seg_3663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T907" id="Seg_3665" n="HIAT:w" s="T906">bar</ts>
                  <nts id="Seg_3666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_3668" n="HIAT:w" s="T907">šü</ts>
                  <nts id="Seg_3669" n="HIAT:ip">,</nts>
                  <nts id="Seg_3670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_3672" n="HIAT:w" s="T908">bar</ts>
                  <nts id="Seg_3673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_3675" n="HIAT:w" s="T909">piʔi</ts>
                  <nts id="Seg_3676" n="HIAT:ip">,</nts>
                  <nts id="Seg_3677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_3679" n="HIAT:w" s="T910">bar</ts>
                  <nts id="Seg_3680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T912" id="Seg_3682" n="HIAT:w" s="T911">grozaʔi</ts>
                  <nts id="Seg_3683" n="HIAT:ip">.</nts>
                  <nts id="Seg_3684" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T916" id="Seg_3686" n="HIAT:u" s="T912">
                  <ts e="T913" id="Seg_3688" n="HIAT:w" s="T912">Šü</ts>
                  <nts id="Seg_3689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T914" id="Seg_3691" n="HIAT:w" s="T913">iləm</ts>
                  <nts id="Seg_3692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T915" id="Seg_3694" n="HIAT:w" s="T914">bar</ts>
                  <nts id="Seg_3695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_3697" n="HIAT:w" s="T915">nendlaʔbə</ts>
                  <nts id="Seg_3698" n="HIAT:ip">.</nts>
                  <nts id="Seg_3699" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T918" id="Seg_3701" n="HIAT:u" s="T916">
                  <ts e="T917" id="Seg_3703" n="HIAT:w" s="T916">Tüj</ts>
                  <nts id="Seg_3704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T918" id="Seg_3706" n="HIAT:w" s="T917">bar</ts>
                  <nts id="Seg_3707" n="HIAT:ip">.</nts>
                  <nts id="Seg_3708" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T919" id="Seg_3710" n="HIAT:u" s="T918">
                  <nts id="Seg_3711" n="HIAT:ip">(</nts>
                  <nts id="Seg_3712" n="HIAT:ip">(</nts>
                  <ats e="T919" id="Seg_3713" n="HIAT:non-pho" s="T918">BRK</ats>
                  <nts id="Seg_3714" n="HIAT:ip">)</nts>
                  <nts id="Seg_3715" n="HIAT:ip">)</nts>
                  <nts id="Seg_3716" n="HIAT:ip">.</nts>
                  <nts id="Seg_3717" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T924" id="Seg_3719" n="HIAT:u" s="T919">
                  <ts e="T920" id="Seg_3721" n="HIAT:w" s="T919">Šiʔ</ts>
                  <nts id="Seg_3722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T921" id="Seg_3724" n="HIAT:w" s="T920">kaŋgaʔ</ts>
                  <nts id="Seg_3725" n="HIAT:ip">,</nts>
                  <nts id="Seg_3726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_3728" n="HIAT:w" s="T921">a</ts>
                  <nts id="Seg_3729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_3731" n="HIAT:w" s="T922">măn</ts>
                  <nts id="Seg_3732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3733" n="HIAT:ip">(</nts>
                  <ts e="T924" id="Seg_3735" n="HIAT:w" s="T923">üjilgerlam</ts>
                  <nts id="Seg_3736" n="HIAT:ip">)</nts>
                  <nts id="Seg_3737" n="HIAT:ip">.</nts>
                  <nts id="Seg_3738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T931" id="Seg_3740" n="HIAT:u" s="T924">
                  <ts e="T925" id="Seg_3742" n="HIAT:w" s="T924">Nada</ts>
                  <nts id="Seg_3743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T926" id="Seg_3745" n="HIAT:w" s="T925">măna</ts>
                  <nts id="Seg_3746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T927" id="Seg_3748" n="HIAT:w" s="T926">ipek</ts>
                  <nts id="Seg_3749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T928" id="Seg_3751" n="HIAT:w" s="T927">nuldəsʼtə</ts>
                  <nts id="Seg_3752" n="HIAT:ip">,</nts>
                  <nts id="Seg_3753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T929" id="Seg_3755" n="HIAT:w" s="T928">pürzittə</ts>
                  <nts id="Seg_3756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_3758" n="HIAT:w" s="T929">ipek</ts>
                  <nts id="Seg_3759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T931" id="Seg_3761" n="HIAT:w" s="T930">nada</ts>
                  <nts id="Seg_3762" n="HIAT:ip">.</nts>
                  <nts id="Seg_3763" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T935" id="Seg_3765" n="HIAT:u" s="T931">
                  <ts e="T931.tx.1" id="Seg_3767" n="HIAT:w" s="T931">A</ts>
                  <nts id="Seg_3768" n="HIAT:ip">_</nts>
                  <ts e="T932" id="Seg_3770" n="HIAT:w" s="T931.tx.1">to</ts>
                  <nts id="Seg_3771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T933" id="Seg_3773" n="HIAT:w" s="T932">amzittə</ts>
                  <nts id="Seg_3774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T934" id="Seg_3776" n="HIAT:w" s="T933">naga</ts>
                  <nts id="Seg_3777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_3779" n="HIAT:w" s="T934">ipek</ts>
                  <nts id="Seg_3780" n="HIAT:ip">.</nts>
                  <nts id="Seg_3781" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T938" id="Seg_3783" n="HIAT:u" s="T935">
                  <ts e="T936" id="Seg_3785" n="HIAT:w" s="T935">Šiʔnʼileʔ</ts>
                  <nts id="Seg_3786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T937" id="Seg_3788" n="HIAT:w" s="T936">mĭlem</ts>
                  <nts id="Seg_3789" n="HIAT:ip">,</nts>
                  <nts id="Seg_3790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T938" id="Seg_3792" n="HIAT:w" s="T937">amorbilaʔ</ts>
                  <nts id="Seg_3793" n="HIAT:ip">.</nts>
                  <nts id="Seg_3794" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T939" id="Seg_3796" n="HIAT:u" s="T938">
                  <nts id="Seg_3797" n="HIAT:ip">(</nts>
                  <nts id="Seg_3798" n="HIAT:ip">(</nts>
                  <ats e="T939" id="Seg_3799" n="HIAT:non-pho" s="T938">BRK</ats>
                  <nts id="Seg_3800" n="HIAT:ip">)</nts>
                  <nts id="Seg_3801" n="HIAT:ip">)</nts>
                  <nts id="Seg_3802" n="HIAT:ip">.</nts>
                  <nts id="Seg_3803" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T946" id="Seg_3805" n="HIAT:u" s="T939">
                  <nts id="Seg_3806" n="HIAT:ip">(</nts>
                  <ts e="T940" id="Seg_3808" n="HIAT:w" s="T939">Iʔ</ts>
                  <nts id="Seg_3809" n="HIAT:ip">)</nts>
                  <nts id="Seg_3810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_3812" n="HIAT:w" s="T940">kanaʔ</ts>
                  <nts id="Seg_3813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T942" id="Seg_3815" n="HIAT:w" s="T941">tibinə</ts>
                  <nts id="Seg_3816" n="HIAT:ip">,</nts>
                  <nts id="Seg_3817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_3819" n="HIAT:w" s="T942">amnoʔ</ts>
                  <nts id="Seg_3820" n="HIAT:ip">,</nts>
                  <nts id="Seg_3821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T944" id="Seg_3823" n="HIAT:w" s="T943">unnʼanə</ts>
                  <nts id="Seg_3824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T945" id="Seg_3826" n="HIAT:w" s="T944">jakše</ts>
                  <nts id="Seg_3827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_3829" n="HIAT:w" s="T945">amnozittə</ts>
                  <nts id="Seg_3830" n="HIAT:ip">.</nts>
                  <nts id="Seg_3831" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T951" id="Seg_3833" n="HIAT:u" s="T946">
                  <ts e="T947" id="Seg_3835" n="HIAT:w" s="T946">Giber-nʼibudʼ</ts>
                  <nts id="Seg_3836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_3838" n="HIAT:w" s="T947">kalam</ts>
                  <nts id="Seg_3839" n="HIAT:ip">,</nts>
                  <nts id="Seg_3840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T949" id="Seg_3842" n="HIAT:w" s="T948">šindinədə</ts>
                  <nts id="Seg_3843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T950" id="Seg_3845" n="HIAT:w" s="T949">ej</ts>
                  <nts id="Seg_3846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T951" id="Seg_3848" n="HIAT:w" s="T950">surarbiam</ts>
                  <nts id="Seg_3849" n="HIAT:ip">.</nts>
                  <nts id="Seg_3850" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T952" id="Seg_3852" n="HIAT:u" s="T951">
                  <nts id="Seg_3853" n="HIAT:ip">(</nts>
                  <nts id="Seg_3854" n="HIAT:ip">(</nts>
                  <ats e="T952" id="Seg_3855" n="HIAT:non-pho" s="T951">BRK</ats>
                  <nts id="Seg_3856" n="HIAT:ip">)</nts>
                  <nts id="Seg_3857" n="HIAT:ip">)</nts>
                  <nts id="Seg_3858" n="HIAT:ip">.</nts>
                  <nts id="Seg_3859" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T955" id="Seg_3861" n="HIAT:u" s="T952">
                  <ts e="T953" id="Seg_3863" n="HIAT:w" s="T952">Bü</ts>
                  <nts id="Seg_3864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T954" id="Seg_3866" n="HIAT:w" s="T953">mʼaŋnaʔbə</ts>
                  <nts id="Seg_3867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T955" id="Seg_3869" n="HIAT:w" s="T954">dĭ</ts>
                  <nts id="Seg_3870" n="HIAT:ip">.</nts>
                  <nts id="Seg_3871" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T968" id="Seg_3873" n="HIAT:u" s="T955">
                  <ts e="T956" id="Seg_3875" n="HIAT:w" s="T955">Urgo</ts>
                  <nts id="Seg_3876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T957" id="Seg_3878" n="HIAT:w" s="T956">Ilʼbinʼ</ts>
                  <nts id="Seg_3879" n="HIAT:ip">,</nts>
                  <nts id="Seg_3880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T958" id="Seg_3882" n="HIAT:w" s="T957">üdʼüge</ts>
                  <nts id="Seg_3883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T959" id="Seg_3885" n="HIAT:w" s="T958">Ilʼbinʼ</ts>
                  <nts id="Seg_3886" n="HIAT:ip">,</nts>
                  <nts id="Seg_3887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T960" id="Seg_3889" n="HIAT:w" s="T959">a</ts>
                  <nts id="Seg_3890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T961" id="Seg_3892" n="HIAT:w" s="T960">gijen</ts>
                  <nts id="Seg_3893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T962" id="Seg_3895" n="HIAT:w" s="T961">bü</ts>
                  <nts id="Seg_3896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T963" id="Seg_3898" n="HIAT:w" s="T962">ileʔbə</ts>
                  <nts id="Seg_3899" n="HIAT:ip">,</nts>
                  <nts id="Seg_3900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964" id="Seg_3902" n="HIAT:w" s="T963">dĭn</ts>
                  <nts id="Seg_3903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_3905" n="HIAT:w" s="T964">bar</ts>
                  <nts id="Seg_3906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T966" id="Seg_3908" n="HIAT:w" s="T965">numəjlieʔi</ts>
                  <nts id="Seg_3909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T967" id="Seg_3911" n="HIAT:w" s="T966">bar</ts>
                  <nts id="Seg_3912" n="HIAT:ip">,</nts>
                  <nts id="Seg_3913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T968" id="Seg_3915" n="HIAT:w" s="T967">talis</ts>
                  <nts id="Seg_3916" n="HIAT:ip">.</nts>
                  <nts id="Seg_3917" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T969" id="Seg_3919" n="HIAT:u" s="T968">
                  <nts id="Seg_3920" n="HIAT:ip">(</nts>
                  <nts id="Seg_3921" n="HIAT:ip">(</nts>
                  <ats e="T969" id="Seg_3922" n="HIAT:non-pho" s="T968">BRK</ats>
                  <nts id="Seg_3923" n="HIAT:ip">)</nts>
                  <nts id="Seg_3924" n="HIAT:ip">)</nts>
                  <nts id="Seg_3925" n="HIAT:ip">.</nts>
                  <nts id="Seg_3926" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T982" id="Seg_3928" n="HIAT:u" s="T969">
                  <ts e="T970" id="Seg_3930" n="HIAT:w" s="T969">Onʼiʔ</ts>
                  <nts id="Seg_3931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T971" id="Seg_3933" n="HIAT:w" s="T970">kö</ts>
                  <nts id="Seg_3934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T972" id="Seg_3936" n="HIAT:w" s="T971">bar</ts>
                  <nts id="Seg_3937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T973" id="Seg_3939" n="HIAT:w" s="T972">miʔ</ts>
                  <nts id="Seg_3940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T974" id="Seg_3942" n="HIAT:w" s="T973">aš</ts>
                  <nts id="Seg_3943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T975" id="Seg_3945" n="HIAT:w" s="T974">pĭdebibeʔ</ts>
                  <nts id="Seg_3946" n="HIAT:ip">,</nts>
                  <nts id="Seg_3947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T976" id="Seg_3949" n="HIAT:w" s="T975">dĭzeŋ</ts>
                  <nts id="Seg_3950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T977" id="Seg_3952" n="HIAT:w" s="T976">bar</ts>
                  <nts id="Seg_3953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T978" id="Seg_3955" n="HIAT:w" s="T977">üge</ts>
                  <nts id="Seg_3956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T979" id="Seg_3958" n="HIAT:w" s="T978">amnolaʔbəʔjə</ts>
                  <nts id="Seg_3959" n="HIAT:ip">,</nts>
                  <nts id="Seg_3960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T980" id="Seg_3962" n="HIAT:w" s="T979">amnolaʔbəʔjə</ts>
                  <nts id="Seg_3963" n="HIAT:ip">,</nts>
                  <nts id="Seg_3964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T981" id="Seg_3966" n="HIAT:w" s="T980">dʼăbaktərlaʔbəʔjə</ts>
                  <nts id="Seg_3967" n="HIAT:ip">,</nts>
                  <nts id="Seg_3968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T982" id="Seg_3970" n="HIAT:w" s="T981">kaknarlaʔbəʔjə</ts>
                  <nts id="Seg_3971" n="HIAT:ip">.</nts>
                  <nts id="Seg_3972" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T992" id="Seg_3974" n="HIAT:u" s="T982">
                  <ts e="T983" id="Seg_3976" n="HIAT:w" s="T982">Kanžəbaʔ</ts>
                  <nts id="Seg_3977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T984" id="Seg_3979" n="HIAT:w" s="T983">pĭdesʼtə</ts>
                  <nts id="Seg_3980" n="HIAT:ip">,</nts>
                  <nts id="Seg_3981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T984.tx.1" id="Seg_3983" n="HIAT:w" s="T984">a</ts>
                  <nts id="Seg_3984" n="HIAT:ip">_</nts>
                  <ts e="T985" id="Seg_3986" n="HIAT:w" s="T984.tx.1">to</ts>
                  <nts id="Seg_3987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T986" id="Seg_3989" n="HIAT:w" s="T985">šišəge</ts>
                  <nts id="Seg_3990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T987" id="Seg_3992" n="HIAT:w" s="T986">moləj</ts>
                  <nts id="Seg_3993" n="HIAT:ip">,</nts>
                  <nts id="Seg_3994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T988" id="Seg_3996" n="HIAT:w" s="T987">все</ts>
                  <nts id="Seg_3997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T989" id="Seg_3999" n="HIAT:w" s="T988">равно</ts>
                  <nts id="Seg_4000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T990" id="Seg_4002" n="HIAT:w" s="T989">miʔnʼibeʔ</ts>
                  <nts id="Seg_4003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T991" id="Seg_4005" n="HIAT:w" s="T990">nada</ts>
                  <nts id="Seg_4006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T992" id="Seg_4008" n="HIAT:w" s="T991">pĭdesʼtə</ts>
                  <nts id="Seg_4009" n="HIAT:ip">.</nts>
                  <nts id="Seg_4010" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T997" id="Seg_4012" n="HIAT:u" s="T992">
                  <nts id="Seg_4013" n="HIAT:ip">(</nts>
                  <ts e="T993" id="Seg_4015" n="HIAT:w" s="T992">Tănan=</ts>
                  <nts id="Seg_4016" n="HIAT:ip">)</nts>
                  <nts id="Seg_4017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T994" id="Seg_4019" n="HIAT:w" s="T993">Tănan</ts>
                  <nts id="Seg_4020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T995" id="Seg_4022" n="HIAT:w" s="T994">üge</ts>
                  <nts id="Seg_4023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T996" id="Seg_4025" n="HIAT:w" s="T995">iʔgö</ts>
                  <nts id="Seg_4026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T997" id="Seg_4028" n="HIAT:w" s="T996">kereʔ</ts>
                  <nts id="Seg_4029" n="HIAT:ip">.</nts>
                  <nts id="Seg_4030" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1001" id="Seg_4032" n="HIAT:u" s="T997">
                  <ts e="T998" id="Seg_4034" n="HIAT:w" s="T997">Dĭgəttə</ts>
                  <nts id="Seg_4035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T999" id="Seg_4037" n="HIAT:w" s="T998">udazaŋdə</ts>
                  <nts id="Seg_4038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1000" id="Seg_4040" n="HIAT:w" s="T999">bar</ts>
                  <nts id="Seg_4041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1001" id="Seg_4043" n="HIAT:w" s="T1000">kăndlaʔbəʔjə</ts>
                  <nts id="Seg_4044" n="HIAT:ip">.</nts>
                  <nts id="Seg_4045" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1008" id="Seg_4047" n="HIAT:u" s="T1001">
                  <ts e="T1002" id="Seg_4049" n="HIAT:w" s="T1001">Sĭre</ts>
                  <nts id="Seg_4050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1003" id="Seg_4052" n="HIAT:w" s="T1002">saʔməluʔləj</ts>
                  <nts id="Seg_4053" n="HIAT:ip">,</nts>
                  <nts id="Seg_4054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1004" id="Seg_4056" n="HIAT:w" s="T1003">pa</ts>
                  <nts id="Seg_4057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1005" id="Seg_4059" n="HIAT:w" s="T1004">bazo</ts>
                  <nts id="Seg_4060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1006" id="Seg_4062" n="HIAT:w" s="T1005">miʔ</ts>
                  <nts id="Seg_4063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4064" n="HIAT:ip">(</nts>
                  <ts e="T1007" id="Seg_4066" n="HIAT:w" s="T1006">pide-</ts>
                  <nts id="Seg_4067" n="HIAT:ip">)</nts>
                  <nts id="Seg_4068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4069" n="HIAT:ip">(</nts>
                  <nts id="Seg_4070" n="HIAT:ip">(</nts>
                  <ats e="T1008" id="Seg_4071" n="HIAT:non-pho" s="T1007">DMG</ats>
                  <nts id="Seg_4072" n="HIAT:ip">)</nts>
                  <nts id="Seg_4073" n="HIAT:ip">)</nts>
                  <nts id="Seg_4074" n="HIAT:ip">.</nts>
                  <nts id="Seg_4075" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T1008" id="Seg_4076" n="sc" s="T0">
               <ts e="T1" id="Seg_4078" n="e" s="T0">Măn </ts>
               <ts e="T2" id="Seg_4080" n="e" s="T1">teinen </ts>
               <ts e="T3" id="Seg_4082" n="e" s="T2">šiʔnʼileʔ </ts>
               <ts e="T4" id="Seg_4084" n="e" s="T3">(kono-) </ts>
               <ts e="T5" id="Seg_4086" n="e" s="T4">kudolbiam. </ts>
               <ts e="T6" id="Seg_4088" n="e" s="T5">Šobilaʔ </ts>
               <ts e="T7" id="Seg_4090" n="e" s="T6">dʼăbaktərzittə, </ts>
               <ts e="T8" id="Seg_4092" n="e" s="T7">(a </ts>
               <ts e="T9" id="Seg_4094" n="e" s="T8">ĭmbidə) </ts>
               <ts e="T10" id="Seg_4096" n="e" s="T9">ej </ts>
               <ts e="T11" id="Seg_4098" n="e" s="T10">nʼilgölaʔbəleʔ. </ts>
               <ts e="T12" id="Seg_4100" n="e" s="T11">(Ej </ts>
               <ts e="T13" id="Seg_4102" n="e" s="T12">măndə-) </ts>
               <ts e="T14" id="Seg_4104" n="e" s="T13">Ej </ts>
               <ts e="T15" id="Seg_4106" n="e" s="T14">măndəlialaʔ. </ts>
               <ts e="T16" id="Seg_4108" n="e" s="T15">Радио </ts>
               <ts e="T17" id="Seg_4110" n="e" s="T16">teinen </ts>
               <ts e="T18" id="Seg_4112" n="e" s="T17">dʼăbaktərbi </ts>
               <ts e="T19" id="Seg_4114" n="e" s="T18">jakše </ts>
               <ts e="T20" id="Seg_4116" n="e" s="T19">передача, </ts>
               <ts e="T21" id="Seg_4118" n="e" s="T20">dʼăbaktərbi. </ts>
               <ts e="T22" id="Seg_4120" n="e" s="T21">Măn </ts>
               <ts e="T23" id="Seg_4122" n="e" s="T22">(nʼilgöbiam) </ts>
               <ts e="T24" id="Seg_4124" n="e" s="T23">da </ts>
               <ts e="T25" id="Seg_4126" n="e" s="T24">ej </ts>
               <ts e="T26" id="Seg_4128" n="e" s="T25">bar </ts>
               <ts e="T27" id="Seg_4130" n="e" s="T26">((PAUSE)) </ts>
               <ts e="T28" id="Seg_4132" n="e" s="T27">üge </ts>
               <ts e="T29" id="Seg_4134" n="e" s="T28">togonoriam, </ts>
               <ts e="T30" id="Seg_4136" n="e" s="T29">nʼilgösʼtə </ts>
               <ts e="T31" id="Seg_4138" n="e" s="T30">(na-) </ts>
               <ts e="T32" id="Seg_4140" n="e" s="T31">kădedə </ts>
               <ts e="T33" id="Seg_4142" n="e" s="T32">ej </ts>
               <ts e="T34" id="Seg_4144" n="e" s="T33">moliam. </ts>
               <ts e="T35" id="Seg_4146" n="e" s="T34">Šindidə </ts>
               <ts e="T36" id="Seg_4148" n="e" s="T35">dʼăbaktərlaʔbə </ts>
               <ts e="T37" id="Seg_4150" n="e" s="T36">sĭrezi. </ts>
               <ts e="T38" id="Seg_4152" n="e" s="T37">"Tăn </ts>
               <ts e="T39" id="Seg_4154" n="e" s="T38">iʔbəʔ </ts>
               <ts e="T40" id="Seg_4156" n="e" s="T39">dʼünə, </ts>
               <ts e="T41" id="Seg_4158" n="e" s="T40">iʔboʔ, </ts>
               <ts e="T42" id="Seg_4160" n="e" s="T41">iʔ </ts>
               <ts e="T43" id="Seg_4162" n="e" s="T42">dʼăbaktəraʔ, </ts>
               <ts e="T44" id="Seg_4164" n="e" s="T43">iʔ </ts>
               <ts e="T45" id="Seg_4166" n="e" s="T44">kirgaraʔ". </ts>
               <ts e="T46" id="Seg_4168" n="e" s="T45">"A </ts>
               <ts e="T47" id="Seg_4170" n="e" s="T46">ĭmbi </ts>
               <ts e="T48" id="Seg_4172" n="e" s="T47">măn </ts>
               <ts e="T49" id="Seg_4174" n="e" s="T48">ej </ts>
               <ts e="T50" id="Seg_4176" n="e" s="T49">dʼăbaktərlam </ts>
               <ts e="T51" id="Seg_4178" n="e" s="T50">da </ts>
               <ts e="T52" id="Seg_4180" n="e" s="T51">ej </ts>
               <ts e="T53" id="Seg_4182" n="e" s="T52">kirgarlam?" </ts>
               <ts e="T54" id="Seg_4184" n="e" s="T53">"Da </ts>
               <ts e="T55" id="Seg_4186" n="e" s="T54">(ko-) </ts>
               <ts e="T56" id="Seg_4188" n="e" s="T55">kondʼo </ts>
               <ts e="T57" id="Seg_4190" n="e" s="T56">amnolal, </ts>
               <ts e="T58" id="Seg_4192" n="e" s="T57">ej </ts>
               <ts e="T59" id="Seg_4194" n="e" s="T58">külel. </ts>
               <ts e="T60" id="Seg_4196" n="e" s="T59">Jakše </ts>
               <ts e="T61" id="Seg_4198" n="e" s="T60">moləj </ts>
               <ts e="T62" id="Seg_4200" n="e" s="T61">tănan. </ts>
               <ts e="T63" id="Seg_4202" n="e" s="T62">(Iʔboʔ), </ts>
               <ts e="T64" id="Seg_4204" n="e" s="T63">iʔboʔ". </ts>
               <ts e="T65" id="Seg_4206" n="e" s="T64">(Dĭgəttə) </ts>
               <ts e="T66" id="Seg_4208" n="e" s="T65">šindidə </ts>
               <ts e="T67" id="Seg_4210" n="e" s="T66">bar </ts>
               <ts e="T68" id="Seg_4212" n="e" s="T67">nʼergöluʔpi </ts>
               <ts e="T69" id="Seg_4214" n="e" s="T68">dĭm. </ts>
               <ts e="T70" id="Seg_4216" n="e" s="T69">I </ts>
               <ts e="T71" id="Seg_4218" n="e" s="T70">(üžümbi) </ts>
               <ts e="T72" id="Seg_4220" n="e" s="T71">dĭm. </ts>
               <ts e="T73" id="Seg_4222" n="e" s="T72">Dĭgəttə </ts>
               <ts e="T74" id="Seg_4224" n="e" s="T73">šišəge </ts>
               <ts e="T75" id="Seg_4226" n="e" s="T74">dĭm </ts>
               <ts e="T76" id="Seg_4228" n="e" s="T75">ugandə </ts>
               <ts e="T77" id="Seg_4230" n="e" s="T76">tăŋ </ts>
               <ts e="T78" id="Seg_4232" n="e" s="T77">kămbi, </ts>
               <ts e="T79" id="Seg_4234" n="e" s="T78">kămbi. </ts>
               <ts e="T80" id="Seg_4236" n="e" s="T79">A </ts>
               <ts e="T81" id="Seg_4238" n="e" s="T80">dĭgəttə </ts>
               <ts e="T82" id="Seg_4240" n="e" s="T81">ejü </ts>
               <ts e="T83" id="Seg_4242" n="e" s="T82">molambi, </ts>
               <ts e="T84" id="Seg_4244" n="e" s="T83">kuja </ts>
               <ts e="T85" id="Seg_4246" n="e" s="T84">uʔbdəbi. </ts>
               <ts e="T86" id="Seg_4248" n="e" s="T85">Nʼuʔdə </ts>
               <ts e="T87" id="Seg_4250" n="e" s="T86">uʔbdəbi, </ts>
               <ts e="T88" id="Seg_4252" n="e" s="T87">sĭre </ts>
               <ts e="T89" id="Seg_4254" n="e" s="T88">bar </ts>
               <ts e="T90" id="Seg_4256" n="e" s="T89">nereʔluʔpi. </ts>
               <ts e="T91" id="Seg_4258" n="e" s="T90">(I </ts>
               <ts e="T92" id="Seg_4260" n="e" s="T91">dĭgəttə </ts>
               <ts e="T93" id="Seg_4262" n="e" s="T92">bar </ts>
               <ts e="T94" id="Seg_4264" n="e" s="T93">ma- </ts>
               <ts e="T95" id="Seg_4266" n="e" s="T94">нача </ts>
               <ts e="T96" id="Seg_4268" n="e" s="T95">-) </ts>
               <ts e="T97" id="Seg_4270" n="e" s="T96">Dĭgəttə </ts>
               <ts e="T98" id="Seg_4272" n="e" s="T97">bar </ts>
               <ts e="T99" id="Seg_4274" n="e" s="T98">таять </ts>
               <ts e="T100" id="Seg_4276" n="e" s="T99">начал. </ts>
               <ts e="T101" id="Seg_4278" n="e" s="T100">Bü </ts>
               <ts e="T102" id="Seg_4280" n="e" s="T101">mʼaŋŋaʔbə. </ts>
               <ts e="T103" id="Seg_4282" n="e" s="T102">I </ts>
               <ts e="T104" id="Seg_4284" n="e" s="T103">sĭre </ts>
               <ts e="T105" id="Seg_4286" n="e" s="T104">külambi. </ts>
               <ts e="T106" id="Seg_4288" n="e" s="T105">Лазарь </ts>
               <ts e="T107" id="Seg_4290" n="e" s="T106">bar </ts>
               <ts e="T108" id="Seg_4292" n="e" s="T107">jezirerluʔpi. </ts>
               <ts e="T109" id="Seg_4294" n="e" s="T108">I </ts>
               <ts e="T110" id="Seg_4296" n="e" s="T109">bar </ts>
               <ts e="T111" id="Seg_4298" n="e" s="T110">(ananugosʼtə) </ts>
               <ts e="T112" id="Seg_4300" n="e" s="T111">ibi </ts>
               <ts e="T113" id="Seg_4302" n="e" s="T112">udandə. </ts>
               <ts e="T114" id="Seg_4304" n="e" s="T113">"Tüjö, </ts>
               <ts e="T115" id="Seg_4306" n="e" s="T114">măndə, </ts>
               <ts e="T116" id="Seg_4308" n="e" s="T115">toʔnarlam". </ts>
               <ts e="T117" id="Seg_4310" n="e" s="T116">Măn </ts>
               <ts e="T118" id="Seg_4312" n="e" s="T117">iam </ts>
               <ts e="T119" id="Seg_4314" n="e" s="T118">(ešši </ts>
               <ts e="T120" id="Seg_4316" n="e" s="T119">deʔpi=) </ts>
               <ts e="T121" id="Seg_4318" n="e" s="T120">nʼi </ts>
               <ts e="T122" id="Seg_4320" n="e" s="T121">deʔpi, </ts>
               <ts e="T123" id="Seg_4322" n="e" s="T122">turagən. </ts>
               <ts e="T124" id="Seg_4324" n="e" s="T123">Bar </ts>
               <ts e="T125" id="Seg_4326" n="e" s="T124">ĭzembi, </ts>
               <ts e="T126" id="Seg_4328" n="e" s="T125">kirgarbi. </ts>
               <ts e="T127" id="Seg_4330" n="e" s="T126">Urgaja </ts>
               <ts e="T128" id="Seg_4332" n="e" s="T127">ibi. </ts>
               <ts e="T129" id="Seg_4334" n="e" s="T128">Dĭ </ts>
               <ts e="T130" id="Seg_4336" n="e" s="T129">deʔpi </ts>
               <ts e="T131" id="Seg_4338" n="e" s="T130">(ets-) </ts>
               <ts e="T132" id="Seg_4340" n="e" s="T131">ešši. </ts>
               <ts e="T133" id="Seg_4342" n="e" s="T132">Dĭ </ts>
               <ts e="T134" id="Seg_4344" n="e" s="T133">bar </ts>
               <ts e="T135" id="Seg_4346" n="e" s="T134">(băʔ-) </ts>
               <ts e="T136" id="Seg_4348" n="e" s="T135">băʔtəbi, </ts>
               <ts e="T137" id="Seg_4350" n="e" s="T136">sarbi </ts>
               <ts e="T138" id="Seg_4352" n="e" s="T137">i </ts>
               <ts e="T139" id="Seg_4354" n="e" s="T138">băzəbi. </ts>
               <ts e="T140" id="Seg_4356" n="e" s="T139">Dĭgəttə </ts>
               <ts e="T141" id="Seg_4358" n="e" s="T140">(ku- </ts>
               <ts e="T142" id="Seg_4360" n="e" s="T141">)… </ts>
               <ts e="T143" id="Seg_4362" n="e" s="T142">((BRK)). </ts>
               <ts e="T144" id="Seg_4364" n="e" s="T143">Dĭgəttə </ts>
               <ts e="T145" id="Seg_4366" n="e" s="T144">kubi </ts>
               <ts e="T146" id="Seg_4368" n="e" s="T145">trʼapkaʔi, </ts>
               <ts e="T147" id="Seg_4370" n="e" s="T146">kürbi </ts>
               <ts e="T148" id="Seg_4372" n="e" s="T147">dĭm. </ts>
               <ts e="T149" id="Seg_4374" n="e" s="T148">I </ts>
               <ts e="T150" id="Seg_4376" n="e" s="T149">embi </ts>
               <ts e="T151" id="Seg_4378" n="e" s="T150">pʼeštə. </ts>
               <ts e="T152" id="Seg_4380" n="e" s="T151">Dĭ </ts>
               <ts e="T153" id="Seg_4382" n="e" s="T152">bar </ts>
               <ts e="T154" id="Seg_4384" n="e" s="T153">iʔbolaʔbə. </ts>
               <ts e="T155" id="Seg_4386" n="e" s="T154">A </ts>
               <ts e="T156" id="Seg_4388" n="e" s="T155">dĭm </ts>
               <ts e="T157" id="Seg_4390" n="e" s="T156">(numəj-) </ts>
               <ts e="T158" id="Seg_4392" n="e" s="T157">numəjleʔbəʔjə. </ts>
               <ts e="T159" id="Seg_4394" n="e" s="T158">Dĭgəttə </ts>
               <ts e="T160" id="Seg_4396" n="e" s="T159">kumbiʔi </ts>
               <ts e="T161" id="Seg_4398" n="e" s="T160">abəstə. </ts>
               <ts e="T162" id="Seg_4400" n="e" s="T161">I </ts>
               <ts e="T163" id="Seg_4402" n="e" s="T162">numəjleʔbəʔjə </ts>
               <ts e="T164" id="Seg_4404" n="e" s="T163">Максим. </ts>
               <ts e="T165" id="Seg_4406" n="e" s="T164">A </ts>
               <ts e="T166" id="Seg_4408" n="e" s="T165">măn </ts>
               <ts e="T167" id="Seg_4410" n="e" s="T166">iam </ts>
               <ts e="T168" id="Seg_4412" n="e" s="T167">dĭm </ts>
               <ts e="T169" id="Seg_4414" n="e" s="T168">deʔpi, </ts>
               <ts e="T170" id="Seg_4416" n="e" s="T169">măna </ts>
               <ts e="T171" id="Seg_4418" n="e" s="T170">bʼeʔ </ts>
               <ts e="T172" id="Seg_4420" n="e" s="T171">šide </ts>
               <ts e="T173" id="Seg_4422" n="e" s="T172">kö </ts>
               <ts e="T174" id="Seg_4424" n="e" s="T173">ibi. </ts>
               <ts e="T175" id="Seg_4426" n="e" s="T174">Dĭgəttə </ts>
               <ts e="T176" id="Seg_4428" n="e" s="T175">dĭ </ts>
               <ts e="T177" id="Seg_4430" n="e" s="T176">Максим </ts>
               <ts e="T178" id="Seg_4432" n="e" s="T177">özerbi, </ts>
               <ts e="T179" id="Seg_4434" n="e" s="T178">özerbi. </ts>
               <ts e="T180" id="Seg_4436" n="e" s="T179">(Bʼeʔ </ts>
               <ts e="T181" id="Seg_4438" n="e" s="T180">šide=) </ts>
               <ts e="T182" id="Seg_4440" n="e" s="T181">Bʼeʔ </ts>
               <ts e="T183" id="Seg_4442" n="e" s="T182">šide </ts>
               <ts e="T184" id="Seg_4444" n="e" s="T183">kö </ts>
               <ts e="T185" id="Seg_4446" n="e" s="T184">ibi, </ts>
               <ts e="T186" id="Seg_4448" n="e" s="T185">iat </ts>
               <ts e="T187" id="Seg_4450" n="e" s="T186">külambi. </ts>
               <ts e="T188" id="Seg_4452" n="e" s="T187">Dĭ </ts>
               <ts e="T189" id="Seg_4454" n="e" s="T188">maːbi. </ts>
               <ts e="T190" id="Seg_4456" n="e" s="T189">A </ts>
               <ts e="T191" id="Seg_4458" n="e" s="T190">(Maksimzen) </ts>
               <ts e="T192" id="Seg_4460" n="e" s="T191">(ia-) </ts>
               <ts e="T193" id="Seg_4462" n="e" s="T192">iam </ts>
               <ts e="T194" id="Seg_4464" n="e" s="T193">kuʔpiʔi, </ts>
               <ts e="T195" id="Seg_4466" n="e" s="T194">dĭm </ts>
               <ts e="T196" id="Seg_4468" n="e" s="T195">kuʔpi </ts>
               <ts e="T197" id="Seg_4470" n="e" s="T196">крестник </ts>
               <ts e="T198" id="Seg_4472" n="e" s="T197">dĭn. </ts>
               <ts e="T199" id="Seg_4474" n="e" s="T198">I </ts>
               <ts e="T200" id="Seg_4476" n="e" s="T199">dĭ </ts>
               <ts e="T201" id="Seg_4478" n="e" s="T200">bar </ts>
               <ts e="T202" id="Seg_4480" n="e" s="T201">külambi. </ts>
               <ts e="T203" id="Seg_4482" n="e" s="T202">Inezi </ts>
               <ts e="T204" id="Seg_4484" n="e" s="T203">maːʔndə </ts>
               <ts e="T205" id="Seg_4486" n="e" s="T204">deʔpiʔi. </ts>
               <ts e="T206" id="Seg_4488" n="e" s="T205">Măn </ts>
               <ts e="T207" id="Seg_4490" n="e" s="T206">abam </ts>
               <ts e="T208" id="Seg_4492" n="e" s="T207">dʼijegən </ts>
               <ts e="T209" id="Seg_4494" n="e" s="T208">külambi. </ts>
               <ts e="T210" id="Seg_4496" n="e" s="T209">Dĭgəttə </ts>
               <ts e="T211" id="Seg_4498" n="e" s="T210">sʼestram </ts>
               <ts e="T212" id="Seg_4500" n="e" s="T211">(i) </ts>
               <ts e="T213" id="Seg_4502" n="e" s="T212">măn </ts>
               <ts e="T214" id="Seg_4504" n="e" s="T213">tibim </ts>
               <ts e="T215" id="Seg_4506" n="e" s="T214">šide </ts>
               <ts e="T216" id="Seg_4508" n="e" s="T215">inegətsi </ts>
               <ts e="T217" id="Seg_4510" n="e" s="T216">deʔpiʔi </ts>
               <ts e="T218" id="Seg_4512" n="e" s="T217">maʔnʼibəj. </ts>
               <ts e="T219" id="Seg_4514" n="e" s="T218">Măn </ts>
               <ts e="T220" id="Seg_4516" n="e" s="T219">ugandə </ts>
               <ts e="T221" id="Seg_4518" n="e" s="T220">tăŋ </ts>
               <ts e="T222" id="Seg_4520" n="e" s="T221">dʼorbiam. </ts>
               <ts e="T223" id="Seg_4522" n="e" s="T222">Urgo </ts>
               <ts e="T224" id="Seg_4524" n="e" s="T223">dʼala </ts>
               <ts e="T225" id="Seg_4526" n="e" s="T224">ibi, </ts>
               <ts e="T226" id="Seg_4528" n="e" s="T225">воскресенье. </ts>
               <ts e="T227" id="Seg_4530" n="e" s="T226">Dĭgəttə </ts>
               <ts e="T228" id="Seg_4532" n="e" s="T227">(dʼü=) </ts>
               <ts e="T229" id="Seg_4534" n="e" s="T228">dʼü </ts>
               <ts e="T230" id="Seg_4536" n="e" s="T229">tĭlbibeʔ. </ts>
               <ts e="T231" id="Seg_4538" n="e" s="T230">Maʔ </ts>
               <ts e="T232" id="Seg_4540" n="e" s="T231">abibeʔ, </ts>
               <ts e="T233" id="Seg_4542" n="e" s="T232">krospa </ts>
               <ts e="T234" id="Seg_4544" n="e" s="T233">abibeʔ. </ts>
               <ts e="T235" id="Seg_4546" n="e" s="T234">I </ts>
               <ts e="T236" id="Seg_4548" n="e" s="T235">dʼünə </ts>
               <ts e="T237" id="Seg_4550" n="e" s="T236">embibeʔ, </ts>
               <ts e="T238" id="Seg_4552" n="e" s="T237">kămnabibaʔ. </ts>
               <ts e="T239" id="Seg_4554" n="e" s="T238">(Dĭgəttə) </ts>
               <ts e="T240" id="Seg_4556" n="e" s="T239">bü </ts>
               <ts e="T241" id="Seg_4558" n="e" s="T240">bĭʔpibeʔ, </ts>
               <ts e="T242" id="Seg_4560" n="e" s="T241">ipek </ts>
               <ts e="T243" id="Seg_4562" n="e" s="T242">ambibaʔ. </ts>
               <ts e="T244" id="Seg_4564" n="e" s="T243">Il </ts>
               <ts e="T245" id="Seg_4566" n="e" s="T244">ibiʔi. </ts>
               <ts e="T246" id="Seg_4568" n="e" s="T245">Kabarləj. </ts>
               <ts e="T247" id="Seg_4570" n="e" s="T246">Šide </ts>
               <ts e="T248" id="Seg_4572" n="e" s="T247">nuzaŋ </ts>
               <ts e="T249" id="Seg_4574" n="e" s="T248">amnobiʔi, </ts>
               <ts e="T250" id="Seg_4576" n="e" s="T249">nüke </ts>
               <ts e="T251" id="Seg_4578" n="e" s="T250">i </ts>
               <ts e="T252" id="Seg_4580" n="e" s="T251">büzʼe. </ts>
               <ts e="T253" id="Seg_4582" n="e" s="T252">Ara </ts>
               <ts e="T254" id="Seg_4584" n="e" s="T253">bĭʔpiʔi, </ts>
               <ts e="T255" id="Seg_4586" n="e" s="T254">bar </ts>
               <ts e="T256" id="Seg_4588" n="e" s="T255">(dʼabrobiʔi). </ts>
               <ts e="T257" id="Seg_4590" n="e" s="T256">Dĭgəttə </ts>
               <ts e="T258" id="Seg_4592" n="e" s="T257">părogəndə </ts>
               <ts e="T259" id="Seg_4594" n="e" s="T258">amnobiʔi, </ts>
               <ts e="T260" id="Seg_4596" n="e" s="T259">(dĭ) </ts>
               <ts e="T261" id="Seg_4598" n="e" s="T260">ulundə </ts>
               <ts e="T262" id="Seg_4600" n="e" s="T261">teʔtə </ts>
               <ts e="T263" id="Seg_4602" n="e" s="T262">ibi. </ts>
               <ts e="T264" id="Seg_4604" n="e" s="T263">Nüke </ts>
               <ts e="T265" id="Seg_4606" n="e" s="T264">i </ts>
               <ts e="T266" id="Seg_4608" n="e" s="T265">büzʼet </ts>
               <ts e="T267" id="Seg_4610" n="e" s="T266">nʼeʔdolaʔpi, </ts>
               <ts e="T268" id="Seg_4612" n="e" s="T267">nʼiʔdolaʔpi </ts>
               <ts e="T269" id="Seg_4614" n="e" s="T268">i </ts>
               <ts e="T270" id="Seg_4616" n="e" s="T269">dăre </ts>
               <ts e="T271" id="Seg_4618" n="e" s="T270">(kunolluʔpiʔi). </ts>
               <ts e="T272" id="Seg_4620" n="e" s="T271">Ertən </ts>
               <ts e="T273" id="Seg_4622" n="e" s="T272">uʔpiʔi. </ts>
               <ts e="T274" id="Seg_4624" n="e" s="T273">"Măn </ts>
               <ts e="T275" id="Seg_4626" n="e" s="T274">ĭmbidə </ts>
               <ts e="T276" id="Seg_4628" n="e" s="T275">bar </ts>
               <ts e="T277" id="Seg_4630" n="e" s="T276">ĭzemniem". </ts>
               <ts e="T278" id="Seg_4632" n="e" s="T277">A </ts>
               <ts e="T279" id="Seg_4634" n="e" s="T278">dĭ </ts>
               <ts e="T280" id="Seg_4636" n="e" s="T279">măndə: </ts>
               <ts e="T281" id="Seg_4638" n="e" s="T280">"Măn </ts>
               <ts e="T282" id="Seg_4640" n="e" s="T281">tože </ts>
               <ts e="T283" id="Seg_4642" n="e" s="T282">ĭzemniem. </ts>
               <ts e="T284" id="Seg_4644" n="e" s="T283">Naverna, </ts>
               <ts e="T285" id="Seg_4646" n="e" s="T284">miʔ </ts>
               <ts e="T286" id="Seg_4648" n="e" s="T285">dʼabrobibaʔ. </ts>
               <ts e="T287" id="Seg_4650" n="e" s="T286">((BRK)). </ts>
               <ts e="T288" id="Seg_4652" n="e" s="T287">Dĭm </ts>
               <ts e="T289" id="Seg_4654" n="e" s="T288">kăštəbiʔi </ts>
               <ts e="T290" id="Seg_4656" n="e" s="T289">Тугойин </ts>
               <ts e="T291" id="Seg_4658" n="e" s="T290">Василий </ts>
               <ts e="T292" id="Seg_4660" n="e" s="T291">Микитич. </ts>
               <ts e="T293" id="Seg_4662" n="e" s="T292">A </ts>
               <ts e="T294" id="Seg_4664" n="e" s="T293">nükem </ts>
               <ts e="T295" id="Seg_4666" n="e" s="T294">kăštəbiʔi </ts>
               <ts e="T296" id="Seg_4668" n="e" s="T295">Vera </ts>
               <ts e="T297" id="Seg_4670" n="e" s="T296">Gordeevna. </ts>
               <ts e="T298" id="Seg_4672" n="e" s="T297">((BRK)). </ts>
               <ts e="T299" id="Seg_4674" n="e" s="T298">Dĭzen </ts>
               <ts e="T300" id="Seg_4676" n="e" s="T299">turazaŋdə </ts>
               <ts e="T301" id="Seg_4678" n="e" s="T300">üdʼüge </ts>
               <ts e="T302" id="Seg_4680" n="e" s="T301">ibiʔi. </ts>
               <ts e="T303" id="Seg_4682" n="e" s="T302">I </ts>
               <ts e="T304" id="Seg_4684" n="e" s="T303">dĭn </ts>
               <ts e="T305" id="Seg_4686" n="e" s="T304">(töštekkən) </ts>
               <ts e="T306" id="Seg_4688" n="e" s="T305">nulaʔpi. </ts>
               <ts e="T307" id="Seg_4690" n="e" s="T306">((BRK)). </ts>
               <ts e="T308" id="Seg_4692" n="e" s="T307">Dĭn </ts>
               <ts e="T309" id="Seg_4694" n="e" s="T308">tolʼko </ts>
               <ts e="T310" id="Seg_4696" n="e" s="T309">sʼestrat </ts>
               <ts e="T311" id="Seg_4698" n="e" s="T310">ibi. </ts>
               <ts e="T312" id="Seg_4700" n="e" s="T311">Kăštəbiʔi </ts>
               <ts e="T313" id="Seg_4702" n="e" s="T312">Fanasʼejaʔizi. </ts>
               <ts e="T314" id="Seg_4704" n="e" s="T313">Esseŋdə </ts>
               <ts e="T315" id="Seg_4706" n="e" s="T314">nagobiʔi, </ts>
               <ts e="T316" id="Seg_4708" n="e" s="T315">dăre </ts>
               <ts e="T317" id="Seg_4710" n="e" s="T316">(bar=) </ts>
               <ts e="T318" id="Seg_4712" n="e" s="T317">bar </ts>
               <ts e="T319" id="Seg_4714" n="e" s="T318">külambiʔi. </ts>
               <ts e="T320" id="Seg_4716" n="e" s="T319">((BRK)). </ts>
               <ts e="T321" id="Seg_4718" n="e" s="T320">Bar </ts>
               <ts e="T322" id="Seg_4720" n="e" s="T321">nagur </ts>
               <ts e="T323" id="Seg_4722" n="e" s="T322">külambiʔi, </ts>
               <ts e="T324" id="Seg_4724" n="e" s="T323">все </ts>
               <ts e="T325" id="Seg_4726" n="e" s="T324">трое. </ts>
               <ts e="T326" id="Seg_4728" n="e" s="T325">((BRK)). </ts>
               <ts e="T327" id="Seg_4730" n="e" s="T326">Sʼestrat:" </ts>
               <ts e="T328" id="Seg_4732" n="e" s="T327">Ĭmbi </ts>
               <ts e="T329" id="Seg_4734" n="e" s="T328">šiʔ </ts>
               <ts e="T330" id="Seg_4736" n="e" s="T329">dʼabrobilaʔ, </ts>
               <ts e="T331" id="Seg_4738" n="e" s="T330">a </ts>
               <ts e="T332" id="Seg_4740" n="e" s="T331">măn </ts>
               <ts e="T333" id="Seg_4742" n="e" s="T332">šiʔnʼileʔ </ts>
               <ts e="T334" id="Seg_4744" n="e" s="T333">ej </ts>
               <ts e="T335" id="Seg_4746" n="e" s="T334">izenbim. </ts>
               <ts e="T336" id="Seg_4748" n="e" s="T335">"Dăre </ts>
               <ts e="T337" id="Seg_4750" n="e" s="T336">bar </ts>
               <ts e="T338" id="Seg_4752" n="e" s="T337">kunolluʔpileʔ". </ts>
               <ts e="T339" id="Seg_4754" n="e" s="T338">((BRK)). </ts>
               <ts e="T340" id="Seg_4756" n="e" s="T339">Nuzaŋgən </ts>
               <ts e="T341" id="Seg_4758" n="e" s="T340">ibi </ts>
               <ts e="T342" id="Seg_4760" n="e" s="T341">urgo </ts>
               <ts e="T343" id="Seg_4762" n="e" s="T342">dʼala, </ts>
               <ts e="T344" id="Seg_4764" n="e" s="T343">Михайло </ts>
               <ts e="T345" id="Seg_4766" n="e" s="T344">dʼala. </ts>
               <ts e="T346" id="Seg_4768" n="e" s="T345">Onʼiʔ </ts>
               <ts e="T347" id="Seg_4770" n="e" s="T346">turanə </ts>
               <ts e="T348" id="Seg_4772" n="e" s="T347">oʔbdəliaʔi </ts>
               <ts e="T349" id="Seg_4774" n="e" s="T348">bar. </ts>
               <ts e="T350" id="Seg_4776" n="e" s="T349">Dĭgəttə </ts>
               <ts e="T351" id="Seg_4778" n="e" s="T350">stoldə </ts>
               <ts e="T352" id="Seg_4780" n="e" s="T351">oʔbdəliaʔi, </ts>
               <ts e="T353" id="Seg_4782" n="e" s="T352">(akămnaʔliaʔi). </ts>
               <ts e="T354" id="Seg_4784" n="e" s="T353">I </ts>
               <ts e="T355" id="Seg_4786" n="e" s="T354">bĭtleʔbəʔjə </ts>
               <ts e="T356" id="Seg_4788" n="e" s="T355">ara. </ts>
               <ts e="T357" id="Seg_4790" n="e" s="T356">I </ts>
               <ts e="T358" id="Seg_4792" n="e" s="T357">amnaʔbəʔjə </ts>
               <ts e="T359" id="Seg_4794" n="e" s="T358">uja, </ts>
               <ts e="T360" id="Seg_4796" n="e" s="T359">ipek, </ts>
               <ts e="T361" id="Seg_4798" n="e" s="T360">ĭmbi </ts>
               <ts e="T362" id="Seg_4800" n="e" s="T361">ige </ts>
               <ts e="T363" id="Seg_4802" n="e" s="T362">stolgən. </ts>
               <ts e="T364" id="Seg_4804" n="e" s="T363">Dĭgəttə </ts>
               <ts e="T365" id="Seg_4806" n="e" s="T364">baška </ts>
               <ts e="T366" id="Seg_4808" n="e" s="T365">turanə </ts>
               <ts e="T367" id="Seg_4810" n="e" s="T366">kalaʔi. </ts>
               <ts e="T368" id="Seg_4812" n="e" s="T367">Dĭn </ts>
               <ts e="T369" id="Seg_4814" n="e" s="T368">dăre </ts>
               <ts e="T370" id="Seg_4816" n="e" s="T369">že </ts>
               <ts e="T371" id="Seg_4818" n="e" s="T370">bĭtleʔi, </ts>
               <ts e="T372" id="Seg_4820" n="e" s="T371">dĭgəttə </ts>
               <ts e="T373" id="Seg_4822" n="e" s="T372">išo </ts>
               <ts e="T374" id="Seg_4824" n="e" s="T373">onʼiʔ </ts>
               <ts e="T375" id="Seg_4826" n="e" s="T374">turanə. </ts>
               <ts e="T376" id="Seg_4828" n="e" s="T375">Dĭgəttə </ts>
               <ts e="T377" id="Seg_4830" n="e" s="T376">teʔtə </ts>
               <ts e="T378" id="Seg_4832" n="e" s="T377">turanə, </ts>
               <ts e="T379" id="Seg_4834" n="e" s="T378">sumna </ts>
               <ts e="T380" id="Seg_4836" n="e" s="T379">(sɨle-) </ts>
               <ts e="T381" id="Seg_4838" n="e" s="T380">(sɨləj). </ts>
               <ts e="T382" id="Seg_4840" n="e" s="T381">Sumna </ts>
               <ts e="T383" id="Seg_4842" n="e" s="T382">dʼala </ts>
               <ts e="T384" id="Seg_4844" n="e" s="T383">bĭtleʔbəʔjə </ts>
               <ts e="T385" id="Seg_4846" n="e" s="T384">üge </ts>
               <ts e="T386" id="Seg_4848" n="e" s="T385">ara, </ts>
               <ts e="T387" id="Seg_4850" n="e" s="T386">i </ts>
               <ts e="T388" id="Seg_4852" n="e" s="T387">dĭgəttə </ts>
               <ts e="T389" id="Seg_4854" n="e" s="T388">kabarləj. </ts>
               <ts e="T390" id="Seg_4856" n="e" s="T389">((BRK)). </ts>
               <ts e="T391" id="Seg_4858" n="e" s="T390">((BRK)). </ts>
               <ts e="T1011" id="Seg_4860" n="e" s="T391">Kazan </ts>
               <ts e="T392" id="Seg_4862" n="e" s="T1011">turagən </ts>
               <ts e="T393" id="Seg_4864" n="e" s="T392">кабак </ts>
               <ts e="T394" id="Seg_4866" n="e" s="T393">ibi, </ts>
               <ts e="T395" id="Seg_4868" n="e" s="T394">dĭzeŋ </ts>
               <ts e="T396" id="Seg_4870" n="e" s="T395">dibər </ts>
               <ts e="T397" id="Seg_4872" n="e" s="T396">kambiʔi </ts>
               <ts e="T398" id="Seg_4874" n="e" s="T397">i </ts>
               <ts e="T399" id="Seg_4876" n="e" s="T398">dĭn </ts>
               <ts e="T400" id="Seg_4878" n="e" s="T399">ara </ts>
               <ts e="T401" id="Seg_4880" n="e" s="T400">ibiʔi. </ts>
               <ts e="T402" id="Seg_4882" n="e" s="T401">Šindin </ts>
               <ts e="T403" id="Seg_4884" n="e" s="T402">aktʼa </ts>
               <ts e="T404" id="Seg_4886" n="e" s="T403">iʔgö </ts>
               <ts e="T405" id="Seg_4888" n="e" s="T404">dăk, </ts>
               <ts e="T406" id="Seg_4890" n="e" s="T405">iʔgö </ts>
               <ts e="T407" id="Seg_4892" n="e" s="T406">ilieʔi. </ts>
               <ts e="T408" id="Seg_4894" n="e" s="T407">Sumna </ts>
               <ts e="T409" id="Seg_4896" n="e" s="T408">igəj. </ts>
               <ts e="T410" id="Seg_4898" n="e" s="T409">A </ts>
               <ts e="T411" id="Seg_4900" n="e" s="T410">šindin </ts>
               <ts e="T412" id="Seg_4902" n="e" s="T411">amga, </ts>
               <ts e="T413" id="Seg_4904" n="e" s="T412">teʔtə </ts>
               <ts e="T414" id="Seg_4906" n="e" s="T413">iləj. </ts>
               <ts e="T415" id="Seg_4908" n="e" s="T414">Šindin </ts>
               <ts e="T416" id="Seg_4910" n="e" s="T415">išo </ts>
               <ts e="T417" id="Seg_4912" n="e" s="T416">amga, </ts>
               <ts e="T418" id="Seg_4914" n="e" s="T417">šide </ts>
               <ts e="T419" id="Seg_4916" n="e" s="T418">iləj. </ts>
               <ts e="T420" id="Seg_4918" n="e" s="T419">I </ts>
               <ts e="T421" id="Seg_4920" n="e" s="T420">dĭgəttə </ts>
               <ts e="T422" id="Seg_4922" n="e" s="T421">urgo </ts>
               <ts e="T423" id="Seg_4924" n="e" s="T422">dʼalagən </ts>
               <ts e="T424" id="Seg_4926" n="e" s="T423">(bĭʔpiʔi). </ts>
               <ts e="T425" id="Seg_4928" n="e" s="T424">Üdʼüge </ts>
               <ts e="T426" id="Seg_4930" n="e" s="T425">dĭrgit </ts>
               <ts e="T427" id="Seg_4932" n="e" s="T426">rʼumkanə </ts>
               <ts e="T428" id="Seg_4934" n="e" s="T427">kămnəbiʔi. </ts>
               <ts e="T429" id="Seg_4936" n="e" s="T428">Onʼiʔtə </ts>
               <ts e="T430" id="Seg_4938" n="e" s="T429">mĭləj, </ts>
               <ts e="T431" id="Seg_4940" n="e" s="T430">dĭgəttə </ts>
               <ts e="T432" id="Seg_4942" n="e" s="T431">bazoʔ </ts>
               <ts e="T433" id="Seg_4944" n="e" s="T432">dak </ts>
               <ts e="T434" id="Seg_4946" n="e" s="T433">tăre, </ts>
               <ts e="T435" id="Seg_4948" n="e" s="T434">tăre </ts>
               <ts e="T436" id="Seg_4950" n="e" s="T435">mĭlie, </ts>
               <ts e="T437" id="Seg_4952" n="e" s="T436">mĭlie </ts>
               <ts e="T438" id="Seg_4954" n="e" s="T437">bar </ts>
               <ts e="T439" id="Seg_4956" n="e" s="T438">(jude). </ts>
               <ts e="T440" id="Seg_4958" n="e" s="T439">Dĭgəttə </ts>
               <ts e="T441" id="Seg_4960" n="e" s="T440">amorlaʔbəʔjə, </ts>
               <ts e="T442" id="Seg_4962" n="e" s="T441">amnaʔbəʔjə, </ts>
               <ts e="T443" id="Seg_4964" n="e" s="T442">bar </ts>
               <ts e="T444" id="Seg_4966" n="e" s="T443">dʼăbaktərlaʔbəʔjə. </ts>
               <ts e="T445" id="Seg_4968" n="e" s="T444">Nüjnə </ts>
               <ts e="T446" id="Seg_4970" n="e" s="T445">nüjleʔbəʔjə. </ts>
               <ts e="T447" id="Seg_4972" n="e" s="T446">((BRK)). </ts>
               <ts e="T448" id="Seg_4974" n="e" s="T447">Алексадр </ts>
               <ts e="T449" id="Seg_4976" n="e" s="T448">Капитонович </ts>
               <ts e="T450" id="Seg_4978" n="e" s="T449">Ашпуров </ts>
               <ts e="T451" id="Seg_4980" n="e" s="T450">kambi </ts>
               <ts e="T453" id="Seg_4982" n="e" s="T451">Kazan turanə… </ts>
               <ts e="T454" id="Seg_4984" n="e" s="T453">Nʼikola </ts>
               <ts e="T455" id="Seg_4986" n="e" s="T454">ibi, </ts>
               <ts e="T456" id="Seg_4988" n="e" s="T455">urgo </ts>
               <ts e="T457" id="Seg_4990" n="e" s="T456">dʼala. </ts>
               <ts e="T458" id="Seg_4992" n="e" s="T457">Dĭn </ts>
               <ts e="T459" id="Seg_4994" n="e" s="T458">ara </ts>
               <ts e="T460" id="Seg_4996" n="e" s="T459">bĭʔpiʔi </ts>
               <ts e="T461" id="Seg_4998" n="e" s="T460">bar. </ts>
               <ts e="T462" id="Seg_5000" n="e" s="T461">Kažnej </ts>
               <ts e="T463" id="Seg_5002" n="e" s="T462">turanə </ts>
               <ts e="T464" id="Seg_5004" n="e" s="T463">mĭmbiʔi. </ts>
               <ts e="T465" id="Seg_5006" n="e" s="T464">Dĭ </ts>
               <ts e="T466" id="Seg_5008" n="e" s="T465">nüdʼin </ts>
               <ts e="T1010" id="Seg_5010" n="e" s="T466">kalla </ts>
               <ts e="T467" id="Seg_5012" n="e" s="T1010">dʼürbi </ts>
               <ts e="T468" id="Seg_5014" n="e" s="T467">Anʼžanə </ts>
               <ts e="T469" id="Seg_5016" n="e" s="T468">i </ts>
               <ts e="T470" id="Seg_5018" n="e" s="T469">dĭn </ts>
               <ts e="T471" id="Seg_5020" n="e" s="T470">kănnambi, </ts>
               <ts e="T472" id="Seg_5022" n="e" s="T471">külambi. </ts>
               <ts e="T473" id="Seg_5024" n="e" s="T472">((BRK)). </ts>
               <ts e="T474" id="Seg_5026" n="e" s="T473">Dĭ </ts>
               <ts e="T475" id="Seg_5028" n="e" s="T474">(maːndə </ts>
               <ts e="T476" id="Seg_5030" n="e" s="T475">ka-) </ts>
               <ts e="T477" id="Seg_5032" n="e" s="T476">maʔəndə </ts>
               <ts e="T478" id="Seg_5034" n="e" s="T477">šobi, </ts>
               <ts e="T479" id="Seg_5036" n="e" s="T478">nʼiʔtə. </ts>
               <ts e="T480" id="Seg_5038" n="e" s="T479">Šide </ts>
               <ts e="T481" id="Seg_5040" n="e" s="T480">kuza </ts>
               <ts e="T482" id="Seg_5042" n="e" s="T481">dĭʔnə </ts>
               <ts e="T483" id="Seg_5044" n="e" s="T482">šobiʔi. </ts>
               <ts e="T484" id="Seg_5046" n="e" s="T483">"Kanžəbəj </ts>
               <ts e="T485" id="Seg_5048" n="e" s="T484">(maʔsʼ-) </ts>
               <ts e="T486" id="Seg_5050" n="e" s="T485">mănzi". </ts>
               <ts e="T487" id="Seg_5052" n="e" s="T486">Dĭgəttə </ts>
               <ts e="T488" id="Seg_5054" n="e" s="T487">kambi, </ts>
               <ts e="T489" id="Seg_5056" n="e" s="T488">kambi. </ts>
               <ts e="T490" id="Seg_5058" n="e" s="T489">Kuliot </ts>
               <ts e="T491" id="Seg_5060" n="e" s="T490">bar: </ts>
               <ts e="T492" id="Seg_5062" n="e" s="T491">(kulʼa) </ts>
               <ts e="T493" id="Seg_5064" n="e" s="T492">bar. </ts>
               <ts e="T494" id="Seg_5066" n="e" s="T493">(Bu-) </ts>
               <ts e="T495" id="Seg_5068" n="e" s="T494">dĭgəttə </ts>
               <ts e="T496" id="Seg_5070" n="e" s="T495">kudajdə </ts>
               <ts e="T497" id="Seg_5072" n="e" s="T496">(uman-) </ts>
               <ts e="T498" id="Seg_5074" n="e" s="T497">numan üzəbi. </ts>
               <ts e="T499" id="Seg_5076" n="e" s="T498">Dĭʔə </ts>
               <ts e="T500" id="Seg_5078" n="e" s="T499">ildə </ts>
               <ts e="T501" id="Seg_5080" n="e" s="T500">nuʔməluʔpiʔi, </ts>
               <ts e="T502" id="Seg_5082" n="e" s="T501">a </ts>
               <ts e="T503" id="Seg_5084" n="e" s="T502">dĭ </ts>
               <ts e="T504" id="Seg_5086" n="e" s="T503">maːʔndə </ts>
               <ts e="T505" id="Seg_5088" n="e" s="T504">šobi. </ts>
               <ts e="T506" id="Seg_5090" n="e" s="T505">Bostə </ts>
               <ts e="T507" id="Seg_5092" n="e" s="T506">sĭre </ts>
               <ts e="T508" id="Seg_5094" n="e" s="T507">mobi. </ts>
               <ts e="T509" id="Seg_5096" n="e" s="T508">((BRK)). </ts>
               <ts e="T510" id="Seg_5098" n="e" s="T509">Dĭzeŋ </ts>
               <ts e="T511" id="Seg_5100" n="e" s="T510">ugandə </ts>
               <ts e="T512" id="Seg_5102" n="e" s="T511">ara </ts>
               <ts e="T513" id="Seg_5104" n="e" s="T512">iʔgö </ts>
               <ts e="T514" id="Seg_5106" n="e" s="T513">bĭtleʔbəʔjə. </ts>
               <ts e="T515" id="Seg_5108" n="e" s="T514">Kumən </ts>
               <ts e="T516" id="Seg_5110" n="e" s="T515">xotʼ </ts>
               <ts e="T517" id="Seg_5112" n="e" s="T516">(m-), </ts>
               <ts e="T518" id="Seg_5114" n="e" s="T517">üge </ts>
               <ts e="T519" id="Seg_5116" n="e" s="T518">bĭtleʔbəʔjə, </ts>
               <ts e="T520" id="Seg_5118" n="e" s="T519">bĭtleʔbəʔjə, </ts>
               <ts e="T521" id="Seg_5120" n="e" s="T520">išo </ts>
               <ts e="T522" id="Seg_5122" n="e" s="T521">pileʔbəʔjə. </ts>
               <ts e="T523" id="Seg_5124" n="e" s="T522">Gijen </ts>
               <ts e="T524" id="Seg_5126" n="e" s="T523">ige </ts>
               <ts e="T525" id="Seg_5128" n="e" s="T524">dăk </ts>
               <ts e="T526" id="Seg_5130" n="e" s="T525">dibər </ts>
               <ts e="T527" id="Seg_5132" n="e" s="T526">nuʔməluʔbəʔjə </ts>
               <ts e="T528" id="Seg_5134" n="e" s="T527">bar. </ts>
               <ts e="T529" id="Seg_5136" n="e" s="T528">((BRK)). </ts>
               <ts e="T530" id="Seg_5138" n="e" s="T529">Dĭgəttə </ts>
               <ts e="T531" id="Seg_5140" n="e" s="T530">bar </ts>
               <ts e="T532" id="Seg_5142" n="e" s="T531">nüjleʔbəʔjə. </ts>
               <ts e="T533" id="Seg_5144" n="e" s="T532">Suʔmileʔbəʔjə. </ts>
               <ts e="T534" id="Seg_5146" n="e" s="T533">Dĭgəttə </ts>
               <ts e="T535" id="Seg_5148" n="e" s="T534">bar </ts>
               <ts e="T536" id="Seg_5150" n="e" s="T535">dʼabrolaʔbəʔjə. </ts>
               <ts e="T537" id="Seg_5152" n="e" s="T536">((BRK)). </ts>
               <ts e="T538" id="Seg_5154" n="e" s="T537">Šindi </ts>
               <ts e="T539" id="Seg_5156" n="e" s="T538">bar </ts>
               <ts e="T540" id="Seg_5158" n="e" s="T539">iʔgö </ts>
               <ts e="T541" id="Seg_5160" n="e" s="T540">(bĭt-) </ts>
               <ts e="T542" id="Seg_5162" n="e" s="T541">bĭtlie, </ts>
               <ts e="T543" id="Seg_5164" n="e" s="T542">bĭtlie, </ts>
               <ts e="T544" id="Seg_5166" n="e" s="T543">dĭgəttə </ts>
               <ts e="T545" id="Seg_5168" n="e" s="T544">aragəʔ </ts>
               <ts e="T546" id="Seg_5170" n="e" s="T545">bar </ts>
               <ts e="T547" id="Seg_5172" n="e" s="T546">(nanʼileʔbə). </ts>
               <ts e="T548" id="Seg_5174" n="e" s="T547">(Dĭ=) </ts>
               <ts e="T549" id="Seg_5176" n="e" s="T548">Dĭʔnə </ts>
               <ts e="T550" id="Seg_5178" n="e" s="T549">aŋdə </ts>
               <ts e="T551" id="Seg_5180" n="e" s="T550">bar </ts>
               <ts e="T552" id="Seg_5182" n="e" s="T551">kĭnzleʔbəʔjə. </ts>
               <ts e="T553" id="Seg_5184" n="e" s="T552">Dĭgəttə </ts>
               <ts e="T554" id="Seg_5186" n="e" s="T553">dĭ </ts>
               <ts e="T555" id="Seg_5188" n="e" s="T554">(o-) </ts>
               <ts e="T556" id="Seg_5190" n="e" s="T555">(uʔləj). </ts>
               <ts e="T557" id="Seg_5192" n="e" s="T556">((BRK)). </ts>
               <ts e="T558" id="Seg_5194" n="e" s="T557">(Onʼiʔ </ts>
               <ts e="T559" id="Seg_5196" n="e" s="T558">onʼiʔ=) </ts>
               <ts e="T560" id="Seg_5198" n="e" s="T559">măn </ts>
               <ts e="T561" id="Seg_5200" n="e" s="T560">(kum </ts>
               <ts e="T562" id="Seg_5202" n="e" s="T561">ĭzemnuʔpi) </ts>
               <ts e="T563" id="Seg_5204" n="e" s="T562">bar, </ts>
               <ts e="T564" id="Seg_5206" n="e" s="T563">dĭn </ts>
               <ts e="T565" id="Seg_5208" n="e" s="T564">müʔleʔbə. </ts>
               <ts e="T566" id="Seg_5210" n="e" s="T565">Măn </ts>
               <ts e="T567" id="Seg_5212" n="e" s="T566">dĭgəttə </ts>
               <ts e="T568" id="Seg_5214" n="e" s="T567">kubiam </ts>
               <ts e="T569" id="Seg_5216" n="e" s="T568">trʼapka, </ts>
               <ts e="T570" id="Seg_5218" n="e" s="T569">kĭnzəbiem, </ts>
               <ts e="T571" id="Seg_5220" n="e" s="T570">(tĭgə) </ts>
               <ts e="T572" id="Seg_5222" n="e" s="T571">(a-) </ts>
               <ts e="T573" id="Seg_5224" n="e" s="T572">ažnə </ts>
               <ts e="T574" id="Seg_5226" n="e" s="T573">mʼaŋnuʔpi. </ts>
               <ts e="T575" id="Seg_5228" n="e" s="T574">Măn </ts>
               <ts e="T576" id="Seg_5230" n="e" s="T575">kunə </ts>
               <ts e="T577" id="Seg_5232" n="e" s="T576">embiem. </ts>
               <ts e="T578" id="Seg_5234" n="e" s="T577">(Dĭ- </ts>
               <ts e="T579" id="Seg_5236" n="e" s="T578">dĭ-) </ts>
               <ts e="T580" id="Seg_5238" n="e" s="T579">Dĭgəttə </ts>
               <ts e="T581" id="Seg_5240" n="e" s="T580">sarbiom. </ts>
               <ts e="T582" id="Seg_5242" n="e" s="T581">Iʔbəbiem, </ts>
               <ts e="T583" id="Seg_5244" n="e" s="T582">kunolluʔpiem. </ts>
               <ts e="T584" id="Seg_5246" n="e" s="T583">Uʔpiam, </ts>
               <ts e="T585" id="Seg_5248" n="e" s="T584">(ku-) </ts>
               <ts e="T586" id="Seg_5250" n="e" s="T585">kum </ts>
               <ts e="T587" id="Seg_5252" n="e" s="T586">ej </ts>
               <ts e="T588" id="Seg_5254" n="e" s="T587">ĭzemnie. </ts>
               <ts e="T589" id="Seg_5256" n="e" s="T588">A </ts>
               <ts e="T590" id="Seg_5258" n="e" s="T589">dĭgəttə </ts>
               <ts e="T591" id="Seg_5260" n="e" s="T590">ulum </ts>
               <ts e="T592" id="Seg_5262" n="e" s="T591">ĭzemnie, </ts>
               <ts e="T593" id="Seg_5264" n="e" s="T592">măn </ts>
               <ts e="T594" id="Seg_5266" n="e" s="T593">tože </ts>
               <ts e="T595" id="Seg_5268" n="e" s="T594">kĭnzlem </ts>
               <ts e="T596" id="Seg_5270" n="e" s="T595">trʼapkaʔinə, </ts>
               <ts e="T597" id="Seg_5272" n="e" s="T596">ulundə </ts>
               <ts e="T598" id="Seg_5274" n="e" s="T597">sarlim. </ts>
               <ts e="T599" id="Seg_5276" n="e" s="T598">Iʔbəlem, </ts>
               <ts e="T600" id="Seg_5278" n="e" s="T599">dĭ </ts>
               <ts e="T601" id="Seg_5280" n="e" s="T600">(kolə-) </ts>
               <ts e="T602" id="Seg_5282" n="e" s="T601">koluʔləj </ts>
               <ts e="T603" id="Seg_5284" n="e" s="T602">dĭ </ts>
               <ts e="T604" id="Seg_5286" n="e" s="T603">trʼapka, </ts>
               <ts e="T605" id="Seg_5288" n="e" s="T604">ulum </ts>
               <ts e="T606" id="Seg_5290" n="e" s="T605">ej </ts>
               <ts e="T607" id="Seg_5292" n="e" s="T606">ĭzemnie. </ts>
               <ts e="T608" id="Seg_5294" n="e" s="T607">((BRK)). </ts>
               <ts e="T609" id="Seg_5296" n="e" s="T608">(Onʼiʔ=) </ts>
               <ts e="T610" id="Seg_5298" n="e" s="T609">Onʼiʔ </ts>
               <ts e="T611" id="Seg_5300" n="e" s="T610">raz </ts>
               <ts e="T612" id="Seg_5302" n="e" s="T611">miʔ </ts>
               <ts e="T613" id="Seg_5304" n="e" s="T612">ipek </ts>
               <ts e="T614" id="Seg_5306" n="e" s="T613">toʔnarbibaʔ </ts>
               <ts e="T615" id="Seg_5308" n="e" s="T614">mašinaʔizi. </ts>
               <ts e="T616" id="Seg_5310" n="e" s="T615">Tĭn </ts>
               <ts e="T617" id="Seg_5312" n="e" s="T616">dagaj </ts>
               <ts e="T618" id="Seg_5314" n="e" s="T617">ibi, </ts>
               <ts e="T619" id="Seg_5316" n="e" s="T618">snăpɨʔi </ts>
               <ts e="T620" id="Seg_5318" n="e" s="T619">(dʼaba-) </ts>
               <ts e="T621" id="Seg_5320" n="e" s="T620">dʼagarzittə. </ts>
               <ts e="T622" id="Seg_5322" n="e" s="T621">A </ts>
               <ts e="T623" id="Seg_5324" n="e" s="T622">dĭ </ts>
               <ts e="T624" id="Seg_5326" n="e" s="T623">(ked-) </ts>
               <ts e="T625" id="Seg_5328" n="e" s="T624">kădedə </ts>
               <ts e="T626" id="Seg_5330" n="e" s="T625">udabə </ts>
               <ts e="T627" id="Seg_5332" n="e" s="T626">bar </ts>
               <ts e="T628" id="Seg_5334" n="e" s="T627">băʔpi, </ts>
               <ts e="T629" id="Seg_5336" n="e" s="T628">măn </ts>
               <ts e="T630" id="Seg_5338" n="e" s="T629">măndəm: </ts>
               <ts e="T631" id="Seg_5340" n="e" s="T630">"Kĭnzeʔ", </ts>
               <ts e="T632" id="Seg_5342" n="e" s="T631">dĭ </ts>
               <ts e="T633" id="Seg_5344" n="e" s="T632">kĭnzəbi </ts>
               <ts e="T634" id="Seg_5346" n="e" s="T633">i </ts>
               <ts e="T635" id="Seg_5348" n="e" s="T634">sarbi. </ts>
               <ts e="T636" id="Seg_5350" n="e" s="T635">Dĭgəttə </ts>
               <ts e="T637" id="Seg_5352" n="e" s="T636">ej </ts>
               <ts e="T638" id="Seg_5354" n="e" s="T637">ĭzembi </ts>
               <ts e="T639" id="Seg_5356" n="e" s="T638">udat. </ts>
               <ts e="T640" id="Seg_5358" n="e" s="T639">((BRK)). </ts>
               <ts e="T641" id="Seg_5360" n="e" s="T640">Miʔ </ts>
               <ts e="T642" id="Seg_5362" n="e" s="T641">nagobiʔi </ts>
               <ts e="T643" id="Seg_5364" n="e" s="T642">doktărəʔi. </ts>
               <ts e="T644" id="Seg_5366" n="e" s="T643">Miʔ </ts>
               <ts e="T645" id="Seg_5368" n="e" s="T644">(noʔ-) </ts>
               <ts e="T646" id="Seg_5370" n="e" s="T645">nozi </ts>
               <ts e="T647" id="Seg_5372" n="e" s="T646">bar </ts>
               <ts e="T648" id="Seg_5374" n="e" s="T647">dʼazirbibaʔ. </ts>
               <ts e="T649" id="Seg_5376" n="e" s="T648">Mĭnzerleʔbə, </ts>
               <ts e="T650" id="Seg_5378" n="e" s="T649">dĭgəttə </ts>
               <ts e="T651" id="Seg_5380" n="e" s="T650">bĭtleʔbə. </ts>
               <ts e="T652" id="Seg_5382" n="e" s="T651">Onʼiʔ </ts>
               <ts e="T653" id="Seg_5384" n="e" s="T652">no </ts>
               <ts e="T654" id="Seg_5386" n="e" s="T653">ige, </ts>
               <ts e="T655" id="Seg_5388" n="e" s="T654">putʼəga. </ts>
               <ts e="T656" id="Seg_5390" n="e" s="T655">Žaludkə </ts>
               <ts e="T657" id="Seg_5392" n="e" s="T656">ĭzemnie, </ts>
               <ts e="T658" id="Seg_5394" n="e" s="T657">dĭm </ts>
               <ts e="T659" id="Seg_5396" n="e" s="T658">mĭnzerliel, </ts>
               <ts e="T660" id="Seg_5398" n="e" s="T659">bĭtliel, </ts>
               <ts e="T661" id="Seg_5400" n="e" s="T660">dĭgəttə </ts>
               <ts e="T662" id="Seg_5402" n="e" s="T661">amorlal. </ts>
               <ts e="T663" id="Seg_5404" n="e" s="T662">A </ts>
               <ts e="T664" id="Seg_5406" n="e" s="T663">onʼiʔ </ts>
               <ts e="T665" id="Seg_5408" n="e" s="T664">(no- </ts>
               <ts e="T666" id="Seg_5410" n="e" s="T665">i-) </ts>
               <ts e="T667" id="Seg_5412" n="e" s="T666">noʔ </ts>
               <ts e="T668" id="Seg_5414" n="e" s="T667">ige, </ts>
               <ts e="T669" id="Seg_5416" n="e" s="T668">(filitʼagɨ) </ts>
               <ts e="T670" id="Seg_5418" n="e" s="T669">numəjlieʔi. </ts>
               <ts e="T671" id="Seg_5420" n="e" s="T670">Dĭm </ts>
               <ts e="T672" id="Seg_5422" n="e" s="T671">ellil </ts>
               <ts e="T673" id="Seg_5424" n="e" s="T672">aranə, </ts>
               <ts e="T674" id="Seg_5426" n="e" s="T673">dĭ </ts>
               <ts e="T675" id="Seg_5428" n="e" s="T674">nugəj </ts>
               <ts e="T676" id="Seg_5430" n="e" s="T675">bʼeʔ </ts>
               <ts e="T677" id="Seg_5432" n="e" s="T676">šide </ts>
               <ts e="T678" id="Seg_5434" n="e" s="T677">dʼala, </ts>
               <ts e="T679" id="Seg_5436" n="e" s="T678">dĭgəttə </ts>
               <ts e="T680" id="Seg_5438" n="e" s="T679">bĭtlel, </ts>
               <ts e="T681" id="Seg_5440" n="e" s="T680">sĭjdə </ts>
               <ts e="T682" id="Seg_5442" n="e" s="T681">jakšə </ts>
               <ts e="T683" id="Seg_5444" n="e" s="T682">molal. </ts>
               <ts e="T684" id="Seg_5446" n="e" s="T683">((BRK)). </ts>
               <ts e="T685" id="Seg_5448" n="e" s="T684">(Tăn) </ts>
               <ts e="T686" id="Seg_5450" n="e" s="T685">miʔ </ts>
               <ts e="T687" id="Seg_5452" n="e" s="T686">nuzaŋdən </ts>
               <ts e="T688" id="Seg_5454" n="e" s="T687">(Maslenkagən) </ts>
               <ts e="T689" id="Seg_5456" n="e" s="T688">bar </ts>
               <ts e="T690" id="Seg_5458" n="e" s="T689">(inetsi) </ts>
               <ts e="T691" id="Seg_5460" n="e" s="T690">tunolaʔbəʔjə. </ts>
               <ts e="T692" id="Seg_5462" n="e" s="T691">Šindin </ts>
               <ts e="T693" id="Seg_5464" n="e" s="T692">inet </ts>
               <ts e="T694" id="Seg_5466" n="e" s="T693">büžüj </ts>
               <ts e="T695" id="Seg_5468" n="e" s="T694">šoləj, </ts>
               <ts e="T696" id="Seg_5470" n="e" s="T695">a </ts>
               <ts e="T697" id="Seg_5472" n="e" s="T696">šindin </ts>
               <ts e="T698" id="Seg_5474" n="e" s="T697">inet </ts>
               <ts e="T699" id="Seg_5476" n="e" s="T698">maləj. </ts>
               <ts e="T700" id="Seg_5478" n="e" s="T699">Dĭ, </ts>
               <ts e="T701" id="Seg_5480" n="e" s="T700">šindin </ts>
               <ts e="T702" id="Seg_5482" n="e" s="T701">maləj, </ts>
               <ts e="T703" id="Seg_5484" n="e" s="T702">dĭ </ts>
               <ts e="T704" id="Seg_5486" n="e" s="T703">четверть </ts>
               <ts e="T705" id="Seg_5488" n="e" s="T704">(ar-) </ts>
               <ts e="T706" id="Seg_5490" n="e" s="T705">ara </ts>
               <ts e="T707" id="Seg_5492" n="e" s="T706">nuldələj. </ts>
               <ts e="T708" id="Seg_5494" n="e" s="T707">((BRK)). </ts>
               <ts e="T709" id="Seg_5496" n="e" s="T708">Dĭgəttə </ts>
               <ts e="T710" id="Seg_5498" n="e" s="T709">baška </ts>
               <ts e="T711" id="Seg_5500" n="e" s="T710">ine </ts>
               <ts e="T712" id="Seg_5502" n="e" s="T711">detləʔi. </ts>
               <ts e="T713" id="Seg_5504" n="e" s="T712">A </ts>
               <ts e="T714" id="Seg_5506" n="e" s="T713">(dĭ=) </ts>
               <ts e="T715" id="Seg_5508" n="e" s="T714">dö </ts>
               <ts e="T716" id="Seg_5510" n="e" s="T715">ine </ts>
               <ts e="T717" id="Seg_5512" n="e" s="T716">ugandə </ts>
               <ts e="T718" id="Seg_5514" n="e" s="T717">бегунец, </ts>
               <ts e="T719" id="Seg_5516" n="e" s="T718">mănliaʔi </ts>
               <ts e="T720" id="Seg_5518" n="e" s="T719">dăre. </ts>
               <ts e="T721" id="Seg_5520" n="e" s="T720">Dĭgəttə </ts>
               <ts e="T722" id="Seg_5522" n="e" s="T721">dĭm </ts>
               <ts e="T723" id="Seg_5524" n="e" s="T722">tože </ts>
               <ts e="T724" id="Seg_5526" n="e" s="T723">maːluʔləj. </ts>
               <ts e="T725" id="Seg_5528" n="e" s="T724">Döbər </ts>
               <ts e="T726" id="Seg_5530" n="e" s="T725">(šo- </ts>
               <ts e="T727" id="Seg_5532" n="e" s="T726">šo-) </ts>
               <ts e="T728" id="Seg_5534" n="e" s="T727">nuʔməluʔləj. </ts>
               <ts e="T729" id="Seg_5536" n="e" s="T728">A </ts>
               <ts e="T730" id="Seg_5538" n="e" s="T729">dĭ </ts>
               <ts e="T731" id="Seg_5540" n="e" s="T730">ine </ts>
               <ts e="T732" id="Seg_5542" n="e" s="T731">maːluʔləj. </ts>
               <ts e="T733" id="Seg_5544" n="e" s="T732">((BRK)). </ts>
               <ts e="T734" id="Seg_5546" n="e" s="T733">Dĭgəttə </ts>
               <ts e="T735" id="Seg_5548" n="e" s="T734">dĭ </ts>
               <ts e="T736" id="Seg_5550" n="e" s="T735">inem </ts>
               <ts e="T737" id="Seg_5552" n="e" s="T736">(пот </ts>
               <ts e="T738" id="Seg_5554" n="e" s="T737">ilie- </ts>
               <ts e="T739" id="Seg_5556" n="e" s="T738">ilie). </ts>
               <ts e="T740" id="Seg_5558" n="e" s="T739">I </ts>
               <ts e="T741" id="Seg_5560" n="e" s="T740">mĭŋge, </ts>
               <ts e="T742" id="Seg_5562" n="e" s="T741">mĭnge </ts>
               <ts e="T743" id="Seg_5564" n="e" s="T742">koldʼəŋ, </ts>
               <ts e="T744" id="Seg_5566" n="e" s="T743">štobɨ </ts>
               <ts e="T745" id="Seg_5568" n="e" s="T744">ej </ts>
               <ts e="T746" id="Seg_5570" n="e" s="T745">nubi. </ts>
               <ts e="T747" id="Seg_5572" n="e" s="T746">((BRK)). </ts>
               <ts e="T748" id="Seg_5574" n="e" s="T747">Dĭgəttə </ts>
               <ts e="T749" id="Seg_5576" n="e" s="T748">ara </ts>
               <ts e="T750" id="Seg_5578" n="e" s="T749">iʔgö </ts>
               <ts e="T751" id="Seg_5580" n="e" s="T750">bĭtluʔləʔi. </ts>
               <ts e="T752" id="Seg_5582" n="e" s="T751">I </ts>
               <ts e="T753" id="Seg_5584" n="e" s="T752">bar </ts>
               <ts e="T754" id="Seg_5586" n="e" s="T753">saʔməluʔləʔjə. </ts>
               <ts e="T755" id="Seg_5588" n="e" s="T754">((BRK)). </ts>
               <ts e="T756" id="Seg_5590" n="e" s="T755">Dĭgəttə </ts>
               <ts e="T757" id="Seg_5592" n="e" s="T756">tibizeŋ </ts>
               <ts e="T758" id="Seg_5594" n="e" s="T757">paʔi </ts>
               <ts e="T759" id="Seg_5596" n="e" s="T758">iləʔi </ts>
               <ts e="T760" id="Seg_5598" n="e" s="T759">udandə. </ts>
               <ts e="T761" id="Seg_5600" n="e" s="T760">I </ts>
               <ts e="T762" id="Seg_5602" n="e" s="T761">dĭgəttə </ts>
               <ts e="T763" id="Seg_5604" n="e" s="T762">(nu- </ts>
               <ts e="T764" id="Seg_5606" n="e" s="T763">)… </ts>
               <ts e="T766" id="Seg_5608" n="e" s="T764">Останови маленько. </ts>
               <ts e="T767" id="Seg_5610" n="e" s="T766">((BRK)). </ts>
               <ts e="T768" id="Seg_5612" n="e" s="T767">(-parnuʔ). </ts>
               <ts e="T769" id="Seg_5614" n="e" s="T768">Dĭgəttə </ts>
               <ts e="T770" id="Seg_5616" n="e" s="T769">onʼiʔ </ts>
               <ts e="T771" id="Seg_5618" n="e" s="T770">(onʼiʔtə </ts>
               <ts e="T772" id="Seg_5620" n="e" s="T771">bar </ts>
               <ts e="T773" id="Seg_5622" n="e" s="T772">nuʔ </ts>
               <ts e="T774" id="Seg_5624" n="e" s="T773">(n-) </ts>
               <ts e="T775" id="Seg_5626" n="e" s="T774">nʼotlaʔbə. </ts>
               <ts e="T776" id="Seg_5628" n="e" s="T775">Dĭgəttə </ts>
               <ts e="T777" id="Seg_5630" n="e" s="T776">onʼiʔ </ts>
               <ts e="T778" id="Seg_5632" n="e" s="T777">kuštü </ts>
               <ts e="T779" id="Seg_5634" n="e" s="T778">(bašk- </ts>
               <ts e="T780" id="Seg_5636" n="e" s="T779">)… </ts>
               <ts e="T781" id="Seg_5638" n="e" s="T780">(Dĭ-) </ts>
               <ts e="T782" id="Seg_5640" n="e" s="T781">Dĭm </ts>
               <ts e="T783" id="Seg_5642" n="e" s="T782">bar </ts>
               <ts e="T784" id="Seg_5644" n="e" s="T783">ujundə </ts>
               <ts e="T785" id="Seg_5646" n="e" s="T784">nuldəluləj. </ts>
               <ts e="T786" id="Seg_5648" n="e" s="T785">Dĭ </ts>
               <ts e="T787" id="Seg_5650" n="e" s="T786">bar </ts>
               <ts e="T788" id="Seg_5652" n="e" s="T787">dĭgəttə </ts>
               <ts e="T789" id="Seg_5654" n="e" s="T788">ara </ts>
               <ts e="T790" id="Seg_5656" n="e" s="T789">nuluʔləj. </ts>
               <ts e="T791" id="Seg_5658" n="e" s="T790">Dĭgəttə </ts>
               <ts e="T792" id="Seg_5660" n="e" s="T791">bĭtlieʔi. </ts>
               <ts e="T793" id="Seg_5662" n="e" s="T792">((BRK)). </ts>
               <ts e="T794" id="Seg_5664" n="e" s="T793">Dĭgəttə </ts>
               <ts e="T795" id="Seg_5666" n="e" s="T794">(šidegöʔ </ts>
               <ts e="T796" id="Seg_5668" n="e" s="T795">nuzaŋ </ts>
               <ts e="T797" id="Seg_5670" n="e" s="T796">a-) </ts>
               <ts e="T798" id="Seg_5672" n="e" s="T797">šidegöʔ </ts>
               <ts e="T799" id="Seg_5674" n="e" s="T798">nuzaŋ </ts>
               <ts e="T800" id="Seg_5676" n="e" s="T799">iləʔi </ts>
               <ts e="T801" id="Seg_5678" n="e" s="T800">onʼiʔ </ts>
               <ts e="T802" id="Seg_5680" n="e" s="T801">onʼiʔtə. </ts>
               <ts e="T803" id="Seg_5682" n="e" s="T802">Dĭgəttə </ts>
               <ts e="T804" id="Seg_5684" n="e" s="T803">kajət </ts>
               <ts e="T805" id="Seg_5686" n="e" s="T804">kuštü </ts>
               <ts e="T806" id="Seg_5688" n="e" s="T805">döm </ts>
               <ts e="T807" id="Seg_5690" n="e" s="T806">(parəʔluj) </ts>
               <ts e="T808" id="Seg_5692" n="e" s="T807">dʼünə. </ts>
               <ts e="T809" id="Seg_5694" n="e" s="T808">Dĭgəttə </ts>
               <ts e="T810" id="Seg_5696" n="e" s="T809">dĭ </ts>
               <ts e="T811" id="Seg_5698" n="e" s="T810">ara </ts>
               <ts e="T812" id="Seg_5700" n="e" s="T811">nuldlia </ts>
               <ts e="T813" id="Seg_5702" n="e" s="T812">(dĭʔnə). </ts>
               <ts e="T814" id="Seg_5704" n="e" s="T813">Bĭtleʔbəʔjə. </ts>
               <ts e="T815" id="Seg_5706" n="e" s="T814">((BRK)). </ts>
               <ts e="T816" id="Seg_5708" n="e" s="T815">Dĭgəttə </ts>
               <ts e="T817" id="Seg_5710" n="e" s="T816">dʼünə </ts>
               <ts e="T818" id="Seg_5712" n="e" s="T817">barəʔluj, </ts>
               <ts e="T819" id="Seg_5714" n="e" s="T818">udabə </ts>
               <ts e="T820" id="Seg_5716" n="e" s="T819">bar </ts>
               <ts e="T821" id="Seg_5718" n="e" s="T820">büldləj. </ts>
               <ts e="T822" id="Seg_5720" n="e" s="T821">((BRK)). </ts>
               <ts e="T823" id="Seg_5722" n="e" s="T822">Вот </ts>
               <ts e="T824" id="Seg_5724" n="e" s="T823">vărotaʔi </ts>
               <ts e="T825" id="Seg_5726" n="e" s="T824">nulaʔbəʔjə. </ts>
               <ts e="T826" id="Seg_5728" n="e" s="T825">Kanaʔ, </ts>
               <ts e="T827" id="Seg_5730" n="e" s="T826">girgit </ts>
               <ts e="T828" id="Seg_5732" n="e" s="T827">aʔtʼinə </ts>
               <ts e="T829" id="Seg_5734" n="e" s="T828">kalal. </ts>
               <ts e="T830" id="Seg_5736" n="e" s="T829">Pravăj </ts>
               <ts e="T831" id="Seg_5738" n="e" s="T830">aʔtʼi </ts>
               <ts e="T832" id="Seg_5740" n="e" s="T831">bar </ts>
               <ts e="T833" id="Seg_5742" n="e" s="T832">ej </ts>
               <ts e="T834" id="Seg_5744" n="e" s="T833">jakše. </ts>
               <ts e="T835" id="Seg_5746" n="e" s="T834">Bar </ts>
               <ts e="T836" id="Seg_5748" n="e" s="T835">bălotăʔi. </ts>
               <ts e="T837" id="Seg_5750" n="e" s="T836">Paʔi </ts>
               <ts e="T838" id="Seg_5752" n="e" s="T837">nugaʔi. </ts>
               <ts e="T839" id="Seg_5754" n="e" s="T838">I </ts>
               <ts e="T840" id="Seg_5756" n="e" s="T839">bar </ts>
               <ts e="T841" id="Seg_5758" n="e" s="T840">grozaʔi. </ts>
               <ts e="T842" id="Seg_5760" n="e" s="T841">Beržə </ts>
               <ts e="T843" id="Seg_5762" n="e" s="T842">bar. </ts>
               <ts e="T844" id="Seg_5764" n="e" s="T843">Bazaj </ts>
               <ts e="T845" id="Seg_5766" n="e" s="T844">kălʼučkaʔi </ts>
               <ts e="T846" id="Seg_5768" n="e" s="T845">bar </ts>
               <ts e="T847" id="Seg_5770" n="e" s="T846">mündüʔleʔbəʔjə. </ts>
               <ts e="T848" id="Seg_5772" n="e" s="T847">A </ts>
               <ts e="T849" id="Seg_5774" n="e" s="T848">dĭbər </ts>
               <ts e="T850" id="Seg_5776" n="e" s="T849">kuŋgəŋ </ts>
               <ts e="T851" id="Seg_5778" n="e" s="T850">kalal, </ts>
               <ts e="T852" id="Seg_5780" n="e" s="T851">dĭn </ts>
               <ts e="T853" id="Seg_5782" n="e" s="T852">bar </ts>
               <ts e="T854" id="Seg_5784" n="e" s="T853">jakše, </ts>
               <ts e="T855" id="Seg_5786" n="e" s="T854">paʔi </ts>
               <ts e="T856" id="Seg_5788" n="e" s="T855">naga, </ts>
               <ts e="T857" id="Seg_5790" n="e" s="T856">ĭmbidə </ts>
               <ts e="T858" id="Seg_5792" n="e" s="T857">naga. </ts>
               <ts e="T859" id="Seg_5794" n="e" s="T858">Ugandə </ts>
               <ts e="T860" id="Seg_5796" n="e" s="T859">kuvas. </ts>
               <ts e="T861" id="Seg_5798" n="e" s="T860">Noʔ </ts>
               <ts e="T862" id="Seg_5800" n="e" s="T861">kuvas, </ts>
               <ts e="T863" id="Seg_5802" n="e" s="T862">всякий </ts>
               <ts e="T864" id="Seg_5804" n="e" s="T863">svʼetogəʔi </ts>
               <ts e="T865" id="Seg_5806" n="e" s="T864">sĭre, </ts>
               <ts e="T866" id="Seg_5808" n="e" s="T865">kömə. </ts>
               <ts e="T867" id="Seg_5810" n="e" s="T866">Виноград </ts>
               <ts e="T868" id="Seg_5812" n="e" s="T867">özerleʔbə, </ts>
               <ts e="T869" id="Seg_5814" n="e" s="T868">jablăkăʔi. </ts>
               <ts e="T870" id="Seg_5816" n="e" s="T869">Kuja </ts>
               <ts e="T871" id="Seg_5818" n="e" s="T870">mĭndlaʔbə, </ts>
               <ts e="T872" id="Seg_5820" n="e" s="T871">ugandə </ts>
               <ts e="T873" id="Seg_5822" n="e" s="T872">ejü, </ts>
               <ts e="T874" id="Seg_5824" n="e" s="T873">xotʼ </ts>
               <ts e="T875" id="Seg_5826" n="e" s="T874">iʔbeʔ </ts>
               <ts e="T876" id="Seg_5828" n="e" s="T875">da </ts>
               <ts e="T877" id="Seg_5830" n="e" s="T876">kunolaʔ, </ts>
               <ts e="T878" id="Seg_5832" n="e" s="T877">jakše </ts>
               <ts e="T879" id="Seg_5834" n="e" s="T878">dĭn. </ts>
               <ts e="T880" id="Seg_5836" n="e" s="T879">A </ts>
               <ts e="T881" id="Seg_5838" n="e" s="T880">lʼevăj </ts>
               <ts e="T882" id="Seg_5840" n="e" s="T881">udandə </ts>
               <ts e="T883" id="Seg_5842" n="e" s="T882">kandəga </ts>
               <ts e="T884" id="Seg_5844" n="e" s="T883">ugandə </ts>
               <ts e="T885" id="Seg_5846" n="e" s="T884">aktʼi </ts>
               <ts e="T886" id="Seg_5848" n="e" s="T885">urgo, </ts>
               <ts e="T887" id="Seg_5850" n="e" s="T886">il </ts>
               <ts e="T888" id="Seg_5852" n="e" s="T887">iʔgö, </ts>
               <ts e="T889" id="Seg_5854" n="e" s="T888">bar </ts>
               <ts e="T890" id="Seg_5856" n="e" s="T889">ara </ts>
               <ts e="T891" id="Seg_5858" n="e" s="T890">bĭtlieʔi. </ts>
               <ts e="T892" id="Seg_5860" n="e" s="T891">Bar </ts>
               <ts e="T893" id="Seg_5862" n="e" s="T892">ĭmbi </ts>
               <ts e="T894" id="Seg_5864" n="e" s="T893">amniaʔi </ts>
               <ts e="T895" id="Seg_5866" n="e" s="T894">bar. </ts>
               <ts e="T896" id="Seg_5868" n="e" s="T895">Suʔmileʔbəʔjə </ts>
               <ts e="T897" id="Seg_5870" n="e" s="T896">bar, </ts>
               <ts e="T898" id="Seg_5872" n="e" s="T897">nüjnə </ts>
               <ts e="T899" id="Seg_5874" n="e" s="T898">nüjleʔbəʔjə. </ts>
               <ts e="T900" id="Seg_5876" n="e" s="T899">A </ts>
               <ts e="T901" id="Seg_5878" n="e" s="T900">(dĭ-) </ts>
               <ts e="T902" id="Seg_5880" n="e" s="T901">dĭbər </ts>
               <ts e="T903" id="Seg_5882" n="e" s="T902">kuŋgə </ts>
               <ts e="T904" id="Seg_5884" n="e" s="T903">(ka-) </ts>
               <ts e="T905" id="Seg_5886" n="e" s="T904">kalal. </ts>
               <ts e="T906" id="Seg_5888" n="e" s="T905">Dĭn </ts>
               <ts e="T907" id="Seg_5890" n="e" s="T906">bar </ts>
               <ts e="T908" id="Seg_5892" n="e" s="T907">šü, </ts>
               <ts e="T909" id="Seg_5894" n="e" s="T908">bar </ts>
               <ts e="T910" id="Seg_5896" n="e" s="T909">piʔi, </ts>
               <ts e="T911" id="Seg_5898" n="e" s="T910">bar </ts>
               <ts e="T912" id="Seg_5900" n="e" s="T911">grozaʔi. </ts>
               <ts e="T913" id="Seg_5902" n="e" s="T912">Šü </ts>
               <ts e="T914" id="Seg_5904" n="e" s="T913">iləm </ts>
               <ts e="T915" id="Seg_5906" n="e" s="T914">bar </ts>
               <ts e="T916" id="Seg_5908" n="e" s="T915">nendlaʔbə. </ts>
               <ts e="T917" id="Seg_5910" n="e" s="T916">Tüj </ts>
               <ts e="T918" id="Seg_5912" n="e" s="T917">bar. </ts>
               <ts e="T919" id="Seg_5914" n="e" s="T918">((BRK)). </ts>
               <ts e="T920" id="Seg_5916" n="e" s="T919">Šiʔ </ts>
               <ts e="T921" id="Seg_5918" n="e" s="T920">kaŋgaʔ, </ts>
               <ts e="T922" id="Seg_5920" n="e" s="T921">a </ts>
               <ts e="T923" id="Seg_5922" n="e" s="T922">măn </ts>
               <ts e="T924" id="Seg_5924" n="e" s="T923">(üjilgerlam). </ts>
               <ts e="T925" id="Seg_5926" n="e" s="T924">Nada </ts>
               <ts e="T926" id="Seg_5928" n="e" s="T925">măna </ts>
               <ts e="T927" id="Seg_5930" n="e" s="T926">ipek </ts>
               <ts e="T928" id="Seg_5932" n="e" s="T927">nuldəsʼtə, </ts>
               <ts e="T929" id="Seg_5934" n="e" s="T928">pürzittə </ts>
               <ts e="T930" id="Seg_5936" n="e" s="T929">ipek </ts>
               <ts e="T931" id="Seg_5938" n="e" s="T930">nada. </ts>
               <ts e="T932" id="Seg_5940" n="e" s="T931">A_to </ts>
               <ts e="T933" id="Seg_5942" n="e" s="T932">amzittə </ts>
               <ts e="T934" id="Seg_5944" n="e" s="T933">naga </ts>
               <ts e="T935" id="Seg_5946" n="e" s="T934">ipek. </ts>
               <ts e="T936" id="Seg_5948" n="e" s="T935">Šiʔnʼileʔ </ts>
               <ts e="T937" id="Seg_5950" n="e" s="T936">mĭlem, </ts>
               <ts e="T938" id="Seg_5952" n="e" s="T937">amorbilaʔ. </ts>
               <ts e="T939" id="Seg_5954" n="e" s="T938">((BRK)). </ts>
               <ts e="T940" id="Seg_5956" n="e" s="T939">(Iʔ) </ts>
               <ts e="T941" id="Seg_5958" n="e" s="T940">kanaʔ </ts>
               <ts e="T942" id="Seg_5960" n="e" s="T941">tibinə, </ts>
               <ts e="T943" id="Seg_5962" n="e" s="T942">amnoʔ, </ts>
               <ts e="T944" id="Seg_5964" n="e" s="T943">unnʼanə </ts>
               <ts e="T945" id="Seg_5966" n="e" s="T944">jakše </ts>
               <ts e="T946" id="Seg_5968" n="e" s="T945">amnozittə. </ts>
               <ts e="T947" id="Seg_5970" n="e" s="T946">Giber-nʼibudʼ </ts>
               <ts e="T948" id="Seg_5972" n="e" s="T947">kalam, </ts>
               <ts e="T949" id="Seg_5974" n="e" s="T948">šindinədə </ts>
               <ts e="T950" id="Seg_5976" n="e" s="T949">ej </ts>
               <ts e="T951" id="Seg_5978" n="e" s="T950">surarbiam. </ts>
               <ts e="T952" id="Seg_5980" n="e" s="T951">((BRK)). </ts>
               <ts e="T953" id="Seg_5982" n="e" s="T952">Bü </ts>
               <ts e="T954" id="Seg_5984" n="e" s="T953">mʼaŋnaʔbə </ts>
               <ts e="T955" id="Seg_5986" n="e" s="T954">dĭ. </ts>
               <ts e="T956" id="Seg_5988" n="e" s="T955">Urgo </ts>
               <ts e="T957" id="Seg_5990" n="e" s="T956">Ilʼbinʼ, </ts>
               <ts e="T958" id="Seg_5992" n="e" s="T957">üdʼüge </ts>
               <ts e="T959" id="Seg_5994" n="e" s="T958">Ilʼbinʼ, </ts>
               <ts e="T960" id="Seg_5996" n="e" s="T959">a </ts>
               <ts e="T961" id="Seg_5998" n="e" s="T960">gijen </ts>
               <ts e="T962" id="Seg_6000" n="e" s="T961">bü </ts>
               <ts e="T963" id="Seg_6002" n="e" s="T962">ileʔbə, </ts>
               <ts e="T964" id="Seg_6004" n="e" s="T963">dĭn </ts>
               <ts e="T965" id="Seg_6006" n="e" s="T964">bar </ts>
               <ts e="T966" id="Seg_6008" n="e" s="T965">numəjlieʔi </ts>
               <ts e="T967" id="Seg_6010" n="e" s="T966">bar, </ts>
               <ts e="T968" id="Seg_6012" n="e" s="T967">talis. </ts>
               <ts e="T969" id="Seg_6014" n="e" s="T968">((BRK)). </ts>
               <ts e="T970" id="Seg_6016" n="e" s="T969">Onʼiʔ </ts>
               <ts e="T971" id="Seg_6018" n="e" s="T970">kö </ts>
               <ts e="T972" id="Seg_6020" n="e" s="T971">bar </ts>
               <ts e="T973" id="Seg_6022" n="e" s="T972">miʔ </ts>
               <ts e="T974" id="Seg_6024" n="e" s="T973">aš </ts>
               <ts e="T975" id="Seg_6026" n="e" s="T974">pĭdebibeʔ, </ts>
               <ts e="T976" id="Seg_6028" n="e" s="T975">dĭzeŋ </ts>
               <ts e="T977" id="Seg_6030" n="e" s="T976">bar </ts>
               <ts e="T978" id="Seg_6032" n="e" s="T977">üge </ts>
               <ts e="T979" id="Seg_6034" n="e" s="T978">amnolaʔbəʔjə, </ts>
               <ts e="T980" id="Seg_6036" n="e" s="T979">amnolaʔbəʔjə, </ts>
               <ts e="T981" id="Seg_6038" n="e" s="T980">dʼăbaktərlaʔbəʔjə, </ts>
               <ts e="T982" id="Seg_6040" n="e" s="T981">kaknarlaʔbəʔjə. </ts>
               <ts e="T983" id="Seg_6042" n="e" s="T982">Kanžəbaʔ </ts>
               <ts e="T984" id="Seg_6044" n="e" s="T983">pĭdesʼtə, </ts>
               <ts e="T985" id="Seg_6046" n="e" s="T984">a_to </ts>
               <ts e="T986" id="Seg_6048" n="e" s="T985">šišəge </ts>
               <ts e="T987" id="Seg_6050" n="e" s="T986">moləj, </ts>
               <ts e="T988" id="Seg_6052" n="e" s="T987">все </ts>
               <ts e="T989" id="Seg_6054" n="e" s="T988">равно </ts>
               <ts e="T990" id="Seg_6056" n="e" s="T989">miʔnʼibeʔ </ts>
               <ts e="T991" id="Seg_6058" n="e" s="T990">nada </ts>
               <ts e="T992" id="Seg_6060" n="e" s="T991">pĭdesʼtə. </ts>
               <ts e="T993" id="Seg_6062" n="e" s="T992">(Tănan=) </ts>
               <ts e="T994" id="Seg_6064" n="e" s="T993">Tănan </ts>
               <ts e="T995" id="Seg_6066" n="e" s="T994">üge </ts>
               <ts e="T996" id="Seg_6068" n="e" s="T995">iʔgö </ts>
               <ts e="T997" id="Seg_6070" n="e" s="T996">kereʔ. </ts>
               <ts e="T998" id="Seg_6072" n="e" s="T997">Dĭgəttə </ts>
               <ts e="T999" id="Seg_6074" n="e" s="T998">udazaŋdə </ts>
               <ts e="T1000" id="Seg_6076" n="e" s="T999">bar </ts>
               <ts e="T1001" id="Seg_6078" n="e" s="T1000">kăndlaʔbəʔjə. </ts>
               <ts e="T1002" id="Seg_6080" n="e" s="T1001">Sĭre </ts>
               <ts e="T1003" id="Seg_6082" n="e" s="T1002">saʔməluʔləj, </ts>
               <ts e="T1004" id="Seg_6084" n="e" s="T1003">pa </ts>
               <ts e="T1005" id="Seg_6086" n="e" s="T1004">bazo </ts>
               <ts e="T1006" id="Seg_6088" n="e" s="T1005">miʔ </ts>
               <ts e="T1007" id="Seg_6090" n="e" s="T1006">(pide-) </ts>
               <ts e="T1008" id="Seg_6092" n="e" s="T1007">((DMG)). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_6093" s="T0">PKZ_196X_SU0216.001 (001)</ta>
            <ta e="T11" id="Seg_6094" s="T5">PKZ_196X_SU0216.002 (002)</ta>
            <ta e="T15" id="Seg_6095" s="T11">PKZ_196X_SU0216.003 (003)</ta>
            <ta e="T21" id="Seg_6096" s="T15">PKZ_196X_SU0216.004 (004)</ta>
            <ta e="T34" id="Seg_6097" s="T21">PKZ_196X_SU0216.005 (005)</ta>
            <ta e="T37" id="Seg_6098" s="T34">PKZ_196X_SU0216.006 (006)</ta>
            <ta e="T45" id="Seg_6099" s="T37">PKZ_196X_SU0216.007 (007)</ta>
            <ta e="T53" id="Seg_6100" s="T45">PKZ_196X_SU0216.008 (008)</ta>
            <ta e="T59" id="Seg_6101" s="T53">PKZ_196X_SU0216.009 (009)</ta>
            <ta e="T62" id="Seg_6102" s="T59">PKZ_196X_SU0216.010 (010)</ta>
            <ta e="T64" id="Seg_6103" s="T62">PKZ_196X_SU0216.011 (011)</ta>
            <ta e="T69" id="Seg_6104" s="T64">PKZ_196X_SU0216.012 (012)</ta>
            <ta e="T72" id="Seg_6105" s="T69">PKZ_196X_SU0216.013 (013)</ta>
            <ta e="T79" id="Seg_6106" s="T72">PKZ_196X_SU0216.014 (014)</ta>
            <ta e="T85" id="Seg_6107" s="T79">PKZ_196X_SU0216.015 (015)</ta>
            <ta e="T90" id="Seg_6108" s="T85">PKZ_196X_SU0216.016 (016)</ta>
            <ta e="T100" id="Seg_6109" s="T90">PKZ_196X_SU0216.017 (017)</ta>
            <ta e="T102" id="Seg_6110" s="T100">PKZ_196X_SU0216.018 (018)</ta>
            <ta e="T105" id="Seg_6111" s="T102">PKZ_196X_SU0216.019 (019)</ta>
            <ta e="T108" id="Seg_6112" s="T105">PKZ_196X_SU0216.020 (020)</ta>
            <ta e="T113" id="Seg_6113" s="T108">PKZ_196X_SU0216.021 (021)</ta>
            <ta e="T116" id="Seg_6114" s="T113">PKZ_196X_SU0216.022 (022)</ta>
            <ta e="T123" id="Seg_6115" s="T116">PKZ_196X_SU0216.023 (023)</ta>
            <ta e="T126" id="Seg_6116" s="T123">PKZ_196X_SU0216.024 (024)</ta>
            <ta e="T128" id="Seg_6117" s="T126">PKZ_196X_SU0216.025 (025)</ta>
            <ta e="T132" id="Seg_6118" s="T128">PKZ_196X_SU0216.026 (026)</ta>
            <ta e="T139" id="Seg_6119" s="T132">PKZ_196X_SU0216.027 (027)</ta>
            <ta e="T142" id="Seg_6120" s="T139">PKZ_196X_SU0216.028 (028)</ta>
            <ta e="T143" id="Seg_6121" s="T142">PKZ_196X_SU0216.029 (029)</ta>
            <ta e="T148" id="Seg_6122" s="T143">PKZ_196X_SU0216.030 (030)</ta>
            <ta e="T151" id="Seg_6123" s="T148">PKZ_196X_SU0216.031 (031)</ta>
            <ta e="T154" id="Seg_6124" s="T151">PKZ_196X_SU0216.032 (032)</ta>
            <ta e="T158" id="Seg_6125" s="T154">PKZ_196X_SU0216.033 (033)</ta>
            <ta e="T161" id="Seg_6126" s="T158">PKZ_196X_SU0216.034 (034)</ta>
            <ta e="T164" id="Seg_6127" s="T161">PKZ_196X_SU0216.035 (035)</ta>
            <ta e="T174" id="Seg_6128" s="T164">PKZ_196X_SU0216.036 (036)</ta>
            <ta e="T179" id="Seg_6129" s="T174">PKZ_196X_SU0216.037 (037)</ta>
            <ta e="T187" id="Seg_6130" s="T179">PKZ_196X_SU0216.038 (038)</ta>
            <ta e="T189" id="Seg_6131" s="T187">PKZ_196X_SU0216.039 (039)</ta>
            <ta e="T198" id="Seg_6132" s="T189">PKZ_196X_SU0216.040 (040)</ta>
            <ta e="T202" id="Seg_6133" s="T198">PKZ_196X_SU0216.041 (041)</ta>
            <ta e="T205" id="Seg_6134" s="T202">PKZ_196X_SU0216.042 (042)</ta>
            <ta e="T209" id="Seg_6135" s="T205">PKZ_196X_SU0216.043 (043)</ta>
            <ta e="T218" id="Seg_6136" s="T209">PKZ_196X_SU0216.044 (044)</ta>
            <ta e="T222" id="Seg_6137" s="T218">PKZ_196X_SU0216.045 (045)</ta>
            <ta e="T226" id="Seg_6138" s="T222">PKZ_196X_SU0216.046 (046)</ta>
            <ta e="T230" id="Seg_6139" s="T226">PKZ_196X_SU0216.047 (047)</ta>
            <ta e="T234" id="Seg_6140" s="T230">PKZ_196X_SU0216.048 (048)</ta>
            <ta e="T238" id="Seg_6141" s="T234">PKZ_196X_SU0216.049 (049)</ta>
            <ta e="T243" id="Seg_6142" s="T238">PKZ_196X_SU0216.050 (050)</ta>
            <ta e="T245" id="Seg_6143" s="T243">PKZ_196X_SU0216.051 (051)</ta>
            <ta e="T246" id="Seg_6144" s="T245">PKZ_196X_SU0216.052 (052)</ta>
            <ta e="T252" id="Seg_6145" s="T246">PKZ_196X_SU0216.053 (053)</ta>
            <ta e="T256" id="Seg_6146" s="T252">PKZ_196X_SU0216.054 (054)</ta>
            <ta e="T263" id="Seg_6147" s="T256">PKZ_196X_SU0216.055 (055)</ta>
            <ta e="T271" id="Seg_6148" s="T263">PKZ_196X_SU0216.056 (056)</ta>
            <ta e="T273" id="Seg_6149" s="T271">PKZ_196X_SU0216.057 (057)</ta>
            <ta e="T277" id="Seg_6150" s="T273">PKZ_196X_SU0216.058 (058)</ta>
            <ta e="T283" id="Seg_6151" s="T277">PKZ_196X_SU0216.059 (059)</ta>
            <ta e="T286" id="Seg_6152" s="T283">PKZ_196X_SU0216.060 (060)</ta>
            <ta e="T287" id="Seg_6153" s="T286">PKZ_196X_SU0216.061 (061)</ta>
            <ta e="T292" id="Seg_6154" s="T287">PKZ_196X_SU0216.062 (062)</ta>
            <ta e="T297" id="Seg_6155" s="T292">PKZ_196X_SU0216.063 (063)</ta>
            <ta e="T298" id="Seg_6156" s="T297">PKZ_196X_SU0216.064 (064)</ta>
            <ta e="T302" id="Seg_6157" s="T298">PKZ_196X_SU0216.065 (065)</ta>
            <ta e="T306" id="Seg_6158" s="T302">PKZ_196X_SU0216.066 (066)</ta>
            <ta e="T307" id="Seg_6159" s="T306">PKZ_196X_SU0216.067 (067)</ta>
            <ta e="T311" id="Seg_6160" s="T307">PKZ_196X_SU0216.068 (068)</ta>
            <ta e="T313" id="Seg_6161" s="T311">PKZ_196X_SU0216.069 (069)</ta>
            <ta e="T319" id="Seg_6162" s="T313">PKZ_196X_SU0216.070 (070)</ta>
            <ta e="T320" id="Seg_6163" s="T319">PKZ_196X_SU0216.071 (071)</ta>
            <ta e="T325" id="Seg_6164" s="T320">PKZ_196X_SU0216.072 (072)</ta>
            <ta e="T326" id="Seg_6165" s="T325">PKZ_196X_SU0216.073 (073)</ta>
            <ta e="T335" id="Seg_6166" s="T326">PKZ_196X_SU0216.074 (074)</ta>
            <ta e="T338" id="Seg_6167" s="T335">PKZ_196X_SU0216.075 (075)</ta>
            <ta e="T339" id="Seg_6168" s="T338">PKZ_196X_SU0216.076 (076)</ta>
            <ta e="T345" id="Seg_6169" s="T339">PKZ_196X_SU0216.077 (077)</ta>
            <ta e="T349" id="Seg_6170" s="T345">PKZ_196X_SU0216.078 (078)</ta>
            <ta e="T353" id="Seg_6171" s="T349">PKZ_196X_SU0216.079 (079)</ta>
            <ta e="T356" id="Seg_6172" s="T353">PKZ_196X_SU0216.080 (080)</ta>
            <ta e="T363" id="Seg_6173" s="T356">PKZ_196X_SU0216.081 (081)</ta>
            <ta e="T367" id="Seg_6174" s="T363">PKZ_196X_SU0216.082 (082)</ta>
            <ta e="T375" id="Seg_6175" s="T367">PKZ_196X_SU0216.083 (083)</ta>
            <ta e="T381" id="Seg_6176" s="T375">PKZ_196X_SU0216.084 (084)</ta>
            <ta e="T389" id="Seg_6177" s="T381">PKZ_196X_SU0216.085 (085)</ta>
            <ta e="T390" id="Seg_6178" s="T389">PKZ_196X_SU0216.086 (086)</ta>
            <ta e="T391" id="Seg_6179" s="T390">PKZ_196X_SU0216.087 (087)</ta>
            <ta e="T401" id="Seg_6180" s="T391">PKZ_196X_SU0216.088 (088)</ta>
            <ta e="T407" id="Seg_6181" s="T401">PKZ_196X_SU0216.089 (089)</ta>
            <ta e="T409" id="Seg_6182" s="T407">PKZ_196X_SU0216.090 (090)</ta>
            <ta e="T414" id="Seg_6183" s="T409">PKZ_196X_SU0216.091 (091)</ta>
            <ta e="T419" id="Seg_6184" s="T414">PKZ_196X_SU0216.092 (092)</ta>
            <ta e="T424" id="Seg_6185" s="T419">PKZ_196X_SU0216.093 (093)</ta>
            <ta e="T428" id="Seg_6186" s="T424">PKZ_196X_SU0216.094 (094)</ta>
            <ta e="T439" id="Seg_6187" s="T428">PKZ_196X_SU0216.095 (095)</ta>
            <ta e="T444" id="Seg_6188" s="T439">PKZ_196X_SU0216.096 (096)</ta>
            <ta e="T446" id="Seg_6189" s="T444">PKZ_196X_SU0216.097 (097)</ta>
            <ta e="T447" id="Seg_6190" s="T446">PKZ_196X_SU0216.098 (098)</ta>
            <ta e="T453" id="Seg_6191" s="T447">PKZ_196X_SU0216.099 (099)</ta>
            <ta e="T457" id="Seg_6192" s="T453">PKZ_196X_SU0216.100 (100)</ta>
            <ta e="T461" id="Seg_6193" s="T457">PKZ_196X_SU0216.101 (101)</ta>
            <ta e="T464" id="Seg_6194" s="T461">PKZ_196X_SU0216.102 (102)</ta>
            <ta e="T472" id="Seg_6195" s="T464">PKZ_196X_SU0216.103 (103)</ta>
            <ta e="T473" id="Seg_6196" s="T472">PKZ_196X_SU0216.104 (104)</ta>
            <ta e="T479" id="Seg_6197" s="T473">PKZ_196X_SU0216.105 (105)</ta>
            <ta e="T483" id="Seg_6198" s="T479">PKZ_196X_SU0216.106 (106)</ta>
            <ta e="T486" id="Seg_6199" s="T483">PKZ_196X_SU0216.107 (107)</ta>
            <ta e="T489" id="Seg_6200" s="T486">PKZ_196X_SU0216.108 (108)</ta>
            <ta e="T493" id="Seg_6201" s="T489">PKZ_196X_SU0216.109 (109)</ta>
            <ta e="T498" id="Seg_6202" s="T493">PKZ_196X_SU0216.110 (110)</ta>
            <ta e="T505" id="Seg_6203" s="T498">PKZ_196X_SU0216.111 (111)</ta>
            <ta e="T508" id="Seg_6204" s="T505">PKZ_196X_SU0216.112 (112)</ta>
            <ta e="T509" id="Seg_6205" s="T508">PKZ_196X_SU0216.113 (113)</ta>
            <ta e="T514" id="Seg_6206" s="T509">PKZ_196X_SU0216.114 (114)</ta>
            <ta e="T522" id="Seg_6207" s="T514">PKZ_196X_SU0216.115 (115)</ta>
            <ta e="T528" id="Seg_6208" s="T522">PKZ_196X_SU0216.116 (116)</ta>
            <ta e="T529" id="Seg_6209" s="T528">PKZ_196X_SU0216.117 (117)</ta>
            <ta e="T532" id="Seg_6210" s="T529">PKZ_196X_SU0216.118 (118)</ta>
            <ta e="T533" id="Seg_6211" s="T532">PKZ_196X_SU0216.119 (119)</ta>
            <ta e="T536" id="Seg_6212" s="T533">PKZ_196X_SU0216.120 (120)</ta>
            <ta e="T537" id="Seg_6213" s="T536">PKZ_196X_SU0216.121 (121)</ta>
            <ta e="T547" id="Seg_6214" s="T537">PKZ_196X_SU0216.122 (122)</ta>
            <ta e="T552" id="Seg_6215" s="T547">PKZ_196X_SU0216.123 (123)</ta>
            <ta e="T556" id="Seg_6216" s="T552">PKZ_196X_SU0216.124 (124)</ta>
            <ta e="T557" id="Seg_6217" s="T556">PKZ_196X_SU0216.125 (125)</ta>
            <ta e="T565" id="Seg_6218" s="T557">PKZ_196X_SU0216.126 (126)</ta>
            <ta e="T574" id="Seg_6219" s="T565">PKZ_196X_SU0216.127 (127)</ta>
            <ta e="T577" id="Seg_6220" s="T574">PKZ_196X_SU0216.128 (128)</ta>
            <ta e="T581" id="Seg_6221" s="T577">PKZ_196X_SU0216.129 (129)</ta>
            <ta e="T583" id="Seg_6222" s="T581">PKZ_196X_SU0216.130 (130)</ta>
            <ta e="T588" id="Seg_6223" s="T583">PKZ_196X_SU0216.131 (131)</ta>
            <ta e="T598" id="Seg_6224" s="T588">PKZ_196X_SU0216.132 (132)</ta>
            <ta e="T607" id="Seg_6225" s="T598">PKZ_196X_SU0216.133 (133)</ta>
            <ta e="T608" id="Seg_6226" s="T607">PKZ_196X_SU0216.134 (134)</ta>
            <ta e="T615" id="Seg_6227" s="T608">PKZ_196X_SU0216.135 (135)</ta>
            <ta e="T621" id="Seg_6228" s="T615">PKZ_196X_SU0216.136 (136)</ta>
            <ta e="T635" id="Seg_6229" s="T621">PKZ_196X_SU0216.137 (137)</ta>
            <ta e="T639" id="Seg_6230" s="T635">PKZ_196X_SU0216.138 (138)</ta>
            <ta e="T640" id="Seg_6231" s="T639">PKZ_196X_SU0216.139 (139)</ta>
            <ta e="T643" id="Seg_6232" s="T640">PKZ_196X_SU0216.140 (140)</ta>
            <ta e="T648" id="Seg_6233" s="T643">PKZ_196X_SU0216.141 (141)</ta>
            <ta e="T651" id="Seg_6234" s="T648">PKZ_196X_SU0216.142 (142)</ta>
            <ta e="T655" id="Seg_6235" s="T651">PKZ_196X_SU0216.143 (143)</ta>
            <ta e="T662" id="Seg_6236" s="T655">PKZ_196X_SU0216.144 (144)</ta>
            <ta e="T670" id="Seg_6237" s="T662">PKZ_196X_SU0216.145 (145)</ta>
            <ta e="T683" id="Seg_6238" s="T670">PKZ_196X_SU0216.146 (146)</ta>
            <ta e="T684" id="Seg_6239" s="T683">PKZ_196X_SU0216.147 (147)</ta>
            <ta e="T691" id="Seg_6240" s="T684">PKZ_196X_SU0216.148 (148)</ta>
            <ta e="T699" id="Seg_6241" s="T691">PKZ_196X_SU0216.149 (149)</ta>
            <ta e="T707" id="Seg_6242" s="T699">PKZ_196X_SU0216.150 (150)</ta>
            <ta e="T708" id="Seg_6243" s="T707">PKZ_196X_SU0216.151 (151)</ta>
            <ta e="T712" id="Seg_6244" s="T708">PKZ_196X_SU0216.152 (152)</ta>
            <ta e="T720" id="Seg_6245" s="T712">PKZ_196X_SU0216.153 (153)</ta>
            <ta e="T724" id="Seg_6246" s="T720">PKZ_196X_SU0216.154 (154)</ta>
            <ta e="T728" id="Seg_6247" s="T724">PKZ_196X_SU0216.155 (155)</ta>
            <ta e="T732" id="Seg_6248" s="T728">PKZ_196X_SU0216.156 (156)</ta>
            <ta e="T733" id="Seg_6249" s="T732">PKZ_196X_SU0216.157 (157)</ta>
            <ta e="T739" id="Seg_6250" s="T733">PKZ_196X_SU0216.158 (158)</ta>
            <ta e="T746" id="Seg_6251" s="T739">PKZ_196X_SU0216.159 (159)</ta>
            <ta e="T747" id="Seg_6252" s="T746">PKZ_196X_SU0216.160 (160)</ta>
            <ta e="T751" id="Seg_6253" s="T747">PKZ_196X_SU0216.161 (161)</ta>
            <ta e="T754" id="Seg_6254" s="T751">PKZ_196X_SU0216.162 (162)</ta>
            <ta e="T755" id="Seg_6255" s="T754">PKZ_196X_SU0216.163 (163)</ta>
            <ta e="T760" id="Seg_6256" s="T755">PKZ_196X_SU0216.164 (164)</ta>
            <ta e="T764" id="Seg_6257" s="T760">PKZ_196X_SU0216.165 (165)</ta>
            <ta e="T766" id="Seg_6258" s="T764">PKZ_196X_SU0216.166 (166)</ta>
            <ta e="T767" id="Seg_6259" s="T766">PKZ_196X_SU0216.167 (167)</ta>
            <ta e="T768" id="Seg_6260" s="T767">PKZ_196X_SU0216.168 (168)</ta>
            <ta e="T775" id="Seg_6261" s="T768">PKZ_196X_SU0216.169 (169)</ta>
            <ta e="T780" id="Seg_6262" s="T775">PKZ_196X_SU0216.170 (170)</ta>
            <ta e="T785" id="Seg_6263" s="T780">PKZ_196X_SU0216.171 (171)</ta>
            <ta e="T790" id="Seg_6264" s="T785">PKZ_196X_SU0216.172 (172)</ta>
            <ta e="T792" id="Seg_6265" s="T790">PKZ_196X_SU0216.173 (173)</ta>
            <ta e="T793" id="Seg_6266" s="T792">PKZ_196X_SU0216.174 (174)</ta>
            <ta e="T802" id="Seg_6267" s="T793">PKZ_196X_SU0216.175 (175)</ta>
            <ta e="T808" id="Seg_6268" s="T802">PKZ_196X_SU0216.176 (176)</ta>
            <ta e="T813" id="Seg_6269" s="T808">PKZ_196X_SU0216.177 (177)</ta>
            <ta e="T814" id="Seg_6270" s="T813">PKZ_196X_SU0216.178 (178)</ta>
            <ta e="T815" id="Seg_6271" s="T814">PKZ_196X_SU0216.179 (179)</ta>
            <ta e="T821" id="Seg_6272" s="T815">PKZ_196X_SU0216.180 (180)</ta>
            <ta e="T822" id="Seg_6273" s="T821">PKZ_196X_SU0216.181 (181)</ta>
            <ta e="T825" id="Seg_6274" s="T822">PKZ_196X_SU0216.182 (182)</ta>
            <ta e="T829" id="Seg_6275" s="T825">PKZ_196X_SU0216.183 (183)</ta>
            <ta e="T834" id="Seg_6276" s="T829">PKZ_196X_SU0216.184 (184)</ta>
            <ta e="T836" id="Seg_6277" s="T834">PKZ_196X_SU0216.185 (185)</ta>
            <ta e="T838" id="Seg_6278" s="T836">PKZ_196X_SU0216.186 (186)</ta>
            <ta e="T841" id="Seg_6279" s="T838">PKZ_196X_SU0216.187 (187)</ta>
            <ta e="T843" id="Seg_6280" s="T841">PKZ_196X_SU0216.188 (188)</ta>
            <ta e="T847" id="Seg_6281" s="T843">PKZ_196X_SU0216.189 (189)</ta>
            <ta e="T858" id="Seg_6282" s="T847">PKZ_196X_SU0216.190 (190)</ta>
            <ta e="T860" id="Seg_6283" s="T858">PKZ_196X_SU0216.191 (191)</ta>
            <ta e="T866" id="Seg_6284" s="T860">PKZ_196X_SU0216.192 (192)</ta>
            <ta e="T869" id="Seg_6285" s="T866">PKZ_196X_SU0216.193 (193)</ta>
            <ta e="T879" id="Seg_6286" s="T869">PKZ_196X_SU0216.194 (194)</ta>
            <ta e="T891" id="Seg_6287" s="T879">PKZ_196X_SU0216.195 (195)</ta>
            <ta e="T895" id="Seg_6288" s="T891">PKZ_196X_SU0216.196 (196)</ta>
            <ta e="T899" id="Seg_6289" s="T895">PKZ_196X_SU0216.197 (197)</ta>
            <ta e="T905" id="Seg_6290" s="T899">PKZ_196X_SU0216.198 (198)</ta>
            <ta e="T912" id="Seg_6291" s="T905">PKZ_196X_SU0216.199 (199)</ta>
            <ta e="T916" id="Seg_6292" s="T912">PKZ_196X_SU0216.200 (200)</ta>
            <ta e="T918" id="Seg_6293" s="T916">PKZ_196X_SU0216.201 (201)</ta>
            <ta e="T919" id="Seg_6294" s="T918">PKZ_196X_SU0216.202 (202)</ta>
            <ta e="T924" id="Seg_6295" s="T919">PKZ_196X_SU0216.203 (203)</ta>
            <ta e="T931" id="Seg_6296" s="T924">PKZ_196X_SU0216.204 (204)</ta>
            <ta e="T935" id="Seg_6297" s="T931">PKZ_196X_SU0216.205 (205)</ta>
            <ta e="T938" id="Seg_6298" s="T935">PKZ_196X_SU0216.206 (206)</ta>
            <ta e="T939" id="Seg_6299" s="T938">PKZ_196X_SU0216.207 (207)</ta>
            <ta e="T946" id="Seg_6300" s="T939">PKZ_196X_SU0216.208 (208)</ta>
            <ta e="T951" id="Seg_6301" s="T946">PKZ_196X_SU0216.209 (209)</ta>
            <ta e="T952" id="Seg_6302" s="T951">PKZ_196X_SU0216.210 (210)</ta>
            <ta e="T955" id="Seg_6303" s="T952">PKZ_196X_SU0216.211 (211)</ta>
            <ta e="T968" id="Seg_6304" s="T955">PKZ_196X_SU0216.212 (212)</ta>
            <ta e="T969" id="Seg_6305" s="T968">PKZ_196X_SU0216.213 (213)</ta>
            <ta e="T982" id="Seg_6306" s="T969">PKZ_196X_SU0216.214 (214)</ta>
            <ta e="T992" id="Seg_6307" s="T982">PKZ_196X_SU0216.215 (215)</ta>
            <ta e="T997" id="Seg_6308" s="T992">PKZ_196X_SU0216.216 (216)</ta>
            <ta e="T1001" id="Seg_6309" s="T997">PKZ_196X_SU0216.217 (217)</ta>
            <ta e="T1008" id="Seg_6310" s="T1001">PKZ_196X_SU0216.218 (218)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_6311" s="T0">Măn teinen šiʔnʼileʔ (kono-) kudolbiam. </ta>
            <ta e="T11" id="Seg_6312" s="T5">Šobilaʔ dʼăbaktərzittə, (a ĭmbidə) ej nʼilgölaʔbəleʔ. </ta>
            <ta e="T15" id="Seg_6313" s="T11">(Ej măndə-) Ej măndəlialaʔ. </ta>
            <ta e="T21" id="Seg_6314" s="T15">Радио teinen dʼăbaktərbi jakše передача, dʼăbaktərbi. </ta>
            <ta e="T34" id="Seg_6315" s="T21">Măn (nʼilgöbiam) da ej bar ((PAUSE)) üge togonoriam, nʼilgösʼtə (na-) kădedə ej moliam. </ta>
            <ta e="T37" id="Seg_6316" s="T34">Šindidə dʼăbaktərlaʔbə sĭrezi. </ta>
            <ta e="T45" id="Seg_6317" s="T37">"Tăn iʔbəʔ dʼünə, iʔboʔ, iʔ dʼăbaktəraʔ, iʔ kirgaraʔ". </ta>
            <ta e="T53" id="Seg_6318" s="T45">"A ĭmbi măn ej dʼăbaktərlam da ej kirgarlam?" </ta>
            <ta e="T59" id="Seg_6319" s="T53">"Da (ko-) kondʼo amnolal, ej külel. </ta>
            <ta e="T62" id="Seg_6320" s="T59">Jakše moləj tănan. </ta>
            <ta e="T64" id="Seg_6321" s="T62">(Iʔboʔ), iʔboʔ". </ta>
            <ta e="T69" id="Seg_6322" s="T64">(Dĭgəttə) šindidə bar nʼergöluʔpi dĭm. </ta>
            <ta e="T72" id="Seg_6323" s="T69">I (üžümbi) dĭm. </ta>
            <ta e="T79" id="Seg_6324" s="T72">Dĭgəttə šišəge dĭm ugandə tăŋ kămbi, kămbi. </ta>
            <ta e="T85" id="Seg_6325" s="T79">A dĭgəttə ejü molambi, kuja uʔbdəbi. </ta>
            <ta e="T90" id="Seg_6326" s="T85">Nʼuʔdə uʔbdəbi, sĭre bar nereʔluʔpi. </ta>
            <ta e="T100" id="Seg_6327" s="T90">(I dĭgəttə bar ma- нача -) Dĭgəttə bar таять начал. </ta>
            <ta e="T102" id="Seg_6328" s="T100">Bü mʼaŋŋaʔbə. </ta>
            <ta e="T105" id="Seg_6329" s="T102">I sĭre külambi. </ta>
            <ta e="T108" id="Seg_6330" s="T105">Лазарь bar jezirerluʔpi. </ta>
            <ta e="T113" id="Seg_6331" s="T108">I bar (ananugosʼtə) ibi udandə. </ta>
            <ta e="T116" id="Seg_6332" s="T113">"Tüjö, măndə, toʔnarlam". </ta>
            <ta e="T123" id="Seg_6333" s="T116">Măn iam (ešši deʔpi=) nʼi deʔpi, turagən. </ta>
            <ta e="T126" id="Seg_6334" s="T123">Bar ĭzembi, kirgarbi. </ta>
            <ta e="T128" id="Seg_6335" s="T126">Urgaja ibi. </ta>
            <ta e="T132" id="Seg_6336" s="T128">Dĭ deʔpi (ets-) ešši. </ta>
            <ta e="T139" id="Seg_6337" s="T132">Dĭ bar (băʔ-) băʔtəbi, sarbi i băzəbi. </ta>
            <ta e="T142" id="Seg_6338" s="T139">Dĭgəttə (ku- )… </ta>
            <ta e="T143" id="Seg_6339" s="T142">((BRK)). </ta>
            <ta e="T148" id="Seg_6340" s="T143">Dĭgəttə kubi trʼapkaʔi, kürbi dĭm. </ta>
            <ta e="T151" id="Seg_6341" s="T148">I embi pʼeštə. </ta>
            <ta e="T154" id="Seg_6342" s="T151">Dĭ bar iʔbolaʔbə. </ta>
            <ta e="T158" id="Seg_6343" s="T154">A dĭm (numəj-) numəjleʔbəʔjə. </ta>
            <ta e="T161" id="Seg_6344" s="T158">Dĭgəttə kumbiʔi abəstə. </ta>
            <ta e="T164" id="Seg_6345" s="T161">I numəjleʔbəʔjə Максим. </ta>
            <ta e="T174" id="Seg_6346" s="T164">A măn iam dĭm deʔpi, măna bʼeʔ šide kö ibi. </ta>
            <ta e="T179" id="Seg_6347" s="T174">Dĭgəttə dĭ Максим özerbi, özerbi. </ta>
            <ta e="T187" id="Seg_6348" s="T179">(Bʼeʔ šide=) Bʼeʔ šide kö ibi, iat külambi. </ta>
            <ta e="T189" id="Seg_6349" s="T187">Dĭ maːbi. </ta>
            <ta e="T198" id="Seg_6350" s="T189">A (Maksimzen) (ia-) iam kuʔpiʔi, dĭm kuʔpi крестник dĭn. </ta>
            <ta e="T202" id="Seg_6351" s="T198">I dĭ bar külambi. </ta>
            <ta e="T205" id="Seg_6352" s="T202">Inezi maːʔndə deʔpiʔi. </ta>
            <ta e="T209" id="Seg_6353" s="T205">Măn abam dʼijegən külambi. </ta>
            <ta e="T218" id="Seg_6354" s="T209">Dĭgəttə sʼestram (i) măn tibim šide inegətsi deʔpiʔi maʔnʼibəj. </ta>
            <ta e="T222" id="Seg_6355" s="T218">Măn ugandə tăŋ dʼorbiam. </ta>
            <ta e="T226" id="Seg_6356" s="T222">Urgo dʼala ibi, воскресенье. </ta>
            <ta e="T230" id="Seg_6357" s="T226">Dĭgəttə (dʼü=) dʼü tĭlbibeʔ. </ta>
            <ta e="T234" id="Seg_6358" s="T230">Maʔ abibeʔ, krospa abibeʔ. </ta>
            <ta e="T238" id="Seg_6359" s="T234">I dʼünə embibeʔ, kămnabibaʔ. </ta>
            <ta e="T243" id="Seg_6360" s="T238">(Dĭgəttə) bü bĭʔpibeʔ, ipek ambibaʔ. </ta>
            <ta e="T245" id="Seg_6361" s="T243">Il ibiʔi. </ta>
            <ta e="T246" id="Seg_6362" s="T245">Kabarləj. </ta>
            <ta e="T252" id="Seg_6363" s="T246">Šide nuzaŋ amnobiʔi, nüke i büzʼe. </ta>
            <ta e="T256" id="Seg_6364" s="T252">Ara bĭʔpiʔi, bar (dʼabrobiʔi). </ta>
            <ta e="T263" id="Seg_6365" s="T256">Dĭgəttə părogəndə amnobiʔi, (dĭ) ulundə teʔtə ibi. </ta>
            <ta e="T271" id="Seg_6366" s="T263">Nüke i büzʼet nʼeʔdolaʔpi, nʼiʔdolaʔpi i dăre (kunolluʔpiʔi). </ta>
            <ta e="T273" id="Seg_6367" s="T271">Ertən uʔpiʔi. </ta>
            <ta e="T277" id="Seg_6368" s="T273">"Măn ĭmbidə bar ĭzemniem". </ta>
            <ta e="T283" id="Seg_6369" s="T277">A dĭ măndə:" Măn tože ĭzemniem. </ta>
            <ta e="T286" id="Seg_6370" s="T283">Naverna, miʔ dʼabrobibaʔ. </ta>
            <ta e="T287" id="Seg_6371" s="T286">((BRK)). </ta>
            <ta e="T292" id="Seg_6372" s="T287">Dĭm kăštəbiʔi Тугойин Василий Микитич. </ta>
            <ta e="T297" id="Seg_6373" s="T292">A nükem kăštəbiʔi Vera Gordeevna. </ta>
            <ta e="T298" id="Seg_6374" s="T297">((BRK)). </ta>
            <ta e="T302" id="Seg_6375" s="T298">Dĭzen turazaŋdə üdʼüge ibiʔi. </ta>
            <ta e="T306" id="Seg_6376" s="T302">I dĭn (töštekkən) nulaʔpi. </ta>
            <ta e="T307" id="Seg_6377" s="T306">((BRK)). </ta>
            <ta e="T311" id="Seg_6378" s="T307">Dĭn tolʼko sʼestrat ibi. </ta>
            <ta e="T313" id="Seg_6379" s="T311">Kăštəbiʔi Fanasʼejaʔizi. </ta>
            <ta e="T319" id="Seg_6380" s="T313">Esseŋdə nagobiʔi, dăre (bar=) bar külambiʔi. </ta>
            <ta e="T320" id="Seg_6381" s="T319">((BRK)). </ta>
            <ta e="T325" id="Seg_6382" s="T320">Bar nagur külambiʔi, все трое. </ta>
            <ta e="T326" id="Seg_6383" s="T325">((BRK)). </ta>
            <ta e="T335" id="Seg_6384" s="T326">Sʼestrat:" Ĭmbi šiʔ dʼabrobilaʔ, a măn šiʔnʼileʔ ej izenbim. </ta>
            <ta e="T338" id="Seg_6385" s="T335">"Dăre bar kunolluʔpileʔ". </ta>
            <ta e="T339" id="Seg_6386" s="T338">((BRK)). </ta>
            <ta e="T345" id="Seg_6387" s="T339">Nuzaŋgən ibi urgo dʼala, Михайло dʼala. </ta>
            <ta e="T349" id="Seg_6388" s="T345">Onʼiʔ turanə oʔbdəliaʔi bar. </ta>
            <ta e="T353" id="Seg_6389" s="T349">Dĭgəttə stoldə oʔbdəliaʔi, (akămnaʔliaʔi). </ta>
            <ta e="T356" id="Seg_6390" s="T353">I bĭtleʔbəʔjə ara. </ta>
            <ta e="T363" id="Seg_6391" s="T356">I amnaʔbəʔjə uja, ipek, ĭmbi ige stolgən. </ta>
            <ta e="T367" id="Seg_6392" s="T363">Dĭgəttə baška turanə kalaʔi. </ta>
            <ta e="T375" id="Seg_6393" s="T367">Dĭn dăre že bĭtleʔi, dĭgəttə išo onʼiʔ turanə. </ta>
            <ta e="T381" id="Seg_6394" s="T375">Dĭgəttə teʔtə turanə, sumna (sɨle-) (sɨləj). </ta>
            <ta e="T389" id="Seg_6395" s="T381">Sumna dʼala bĭtleʔbəʔjə üge ara, i dĭgəttə kabarləj. </ta>
            <ta e="T390" id="Seg_6396" s="T389">((BRK)). </ta>
            <ta e="T391" id="Seg_6397" s="T390">((BRK)). </ta>
            <ta e="T401" id="Seg_6398" s="T391">Kazan turagən кабак ibi, dĭzeŋ dibər kambiʔi i dĭn ara ibiʔi. </ta>
            <ta e="T407" id="Seg_6399" s="T401">Šindin aktʼa iʔgö dăk, iʔgö ilieʔi. </ta>
            <ta e="T409" id="Seg_6400" s="T407">Sumna igəj. </ta>
            <ta e="T414" id="Seg_6401" s="T409">A šindin amga, teʔtə iləj. </ta>
            <ta e="T419" id="Seg_6402" s="T414">Šindin išo amga, šide iləj. </ta>
            <ta e="T424" id="Seg_6403" s="T419">I dĭgəttə urgo dʼalagən (bĭʔpiʔi). </ta>
            <ta e="T428" id="Seg_6404" s="T424">Üdʼüge dĭrgit rʼumkanə kămnəbiʔi. </ta>
            <ta e="T439" id="Seg_6405" s="T428">Onʼiʔtə mĭləj, dĭgəttə bazoʔ dak tăre, tăre mĭlie, mĭlie bar (jude). </ta>
            <ta e="T444" id="Seg_6406" s="T439">Dĭgəttə amorlaʔbəʔjə, amnaʔbəʔjə, bar dʼăbaktərlaʔbəʔjə. </ta>
            <ta e="T446" id="Seg_6407" s="T444">Nüjnə nüjleʔbəʔjə. </ta>
            <ta e="T447" id="Seg_6408" s="T446">((BRK)). </ta>
            <ta e="T453" id="Seg_6409" s="T447">Алексадр Капитонович Ашпуров kambi Kazan turanə … </ta>
            <ta e="T457" id="Seg_6410" s="T453">Nʼikola ibi, urgo dʼala. </ta>
            <ta e="T461" id="Seg_6411" s="T457">Dĭn ara bĭʔpiʔi bar. </ta>
            <ta e="T464" id="Seg_6412" s="T461">Kažnej turanə mĭmbiʔi. </ta>
            <ta e="T472" id="Seg_6413" s="T464">Dĭ nüdʼin kalla dʼürbi Anʼžanə i dĭn kănnambi, külambi. </ta>
            <ta e="T473" id="Seg_6414" s="T472">((BRK)). </ta>
            <ta e="T479" id="Seg_6415" s="T473">Dĭ (maːndə ka-) maʔəndə šobi, nʼiʔtə. </ta>
            <ta e="T483" id="Seg_6416" s="T479">Šide kuza dĭʔnə šobiʔi. </ta>
            <ta e="T486" id="Seg_6417" s="T483">"Kanžəbəj (maʔsʼ-) mănzi". </ta>
            <ta e="T489" id="Seg_6418" s="T486">Dĭgəttə kambi, kambi. </ta>
            <ta e="T493" id="Seg_6419" s="T489">Kuliot bar: (kulʼa) bar. </ta>
            <ta e="T498" id="Seg_6420" s="T493">(Bu-) dĭgəttə kudajdə (uman-) numan üzəbi. </ta>
            <ta e="T505" id="Seg_6421" s="T498">Dĭʔə ildə nuʔməluʔpiʔi, a dĭ maːʔndə šobi. </ta>
            <ta e="T508" id="Seg_6422" s="T505">Bostə sĭre mobi. </ta>
            <ta e="T509" id="Seg_6423" s="T508">((BRK)). </ta>
            <ta e="T514" id="Seg_6424" s="T509">Dĭzeŋ ugandə ara iʔgö bĭtleʔbəʔjə. </ta>
            <ta e="T522" id="Seg_6425" s="T514">Kumən xotʼ (m-), üge bĭtleʔbəʔjə, bĭtleʔbəʔjə, išo pileʔbəʔjə. </ta>
            <ta e="T528" id="Seg_6426" s="T522">Gijen ige dăk dibər nuʔməluʔbəʔjə bar. </ta>
            <ta e="T529" id="Seg_6427" s="T528">((BRK)). </ta>
            <ta e="T532" id="Seg_6428" s="T529">Dĭgəttə bar nüjleʔbəʔjə. </ta>
            <ta e="T533" id="Seg_6429" s="T532">Suʔmileʔbəʔjə. </ta>
            <ta e="T536" id="Seg_6430" s="T533">Dĭgəttə bar dʼabrolaʔbəʔjə. </ta>
            <ta e="T537" id="Seg_6431" s="T536">((BRK)). </ta>
            <ta e="T547" id="Seg_6432" s="T537">Šindi bar iʔgö (bĭt-) bĭtlie, bĭtlie, dĭgəttə aragəʔ bar (nanʼileʔbə). </ta>
            <ta e="T552" id="Seg_6433" s="T547">(Dĭ=) Dĭʔnə aŋdə bar kĭnzleʔbəʔjə. </ta>
            <ta e="T556" id="Seg_6434" s="T552">Dĭgəttə dĭ (o-) (uʔləj). </ta>
            <ta e="T557" id="Seg_6435" s="T556">((BRK)). </ta>
            <ta e="T565" id="Seg_6436" s="T557">(Onʼiʔ onʼiʔ=) măn (kum ĭzemnuʔpi) bar, dĭn müʔleʔbə. </ta>
            <ta e="T574" id="Seg_6437" s="T565">Măn dĭgəttə kubiam trʼapka, kĭnzəbiem, (tĭgə) (a-) ažnə mʼaŋnuʔpi. </ta>
            <ta e="T577" id="Seg_6438" s="T574">Măn kunə embiem. </ta>
            <ta e="T581" id="Seg_6439" s="T577">(Dĭ- dĭ-) Dĭgəttə sarbiom. </ta>
            <ta e="T583" id="Seg_6440" s="T581">Iʔbəbiem, kunolluʔpiem. </ta>
            <ta e="T588" id="Seg_6441" s="T583">Uʔpiam, (ku-) kum ej ĭzemnie. </ta>
            <ta e="T598" id="Seg_6442" s="T588">A dĭgəttə ulum ĭzemnie, măn tože kĭnzlem trʼapkaʔinə, ulundə sarlim. </ta>
            <ta e="T607" id="Seg_6443" s="T598">Iʔbəlem, dĭ (kolə-) koluʔləj dĭ trʼapka, ulum ej ĭzemnie. </ta>
            <ta e="T608" id="Seg_6444" s="T607">((BRK)). </ta>
            <ta e="T615" id="Seg_6445" s="T608">(Onʼiʔ=) Onʼiʔ raz miʔ ipek toʔnarbibaʔ mašinaʔizi. </ta>
            <ta e="T621" id="Seg_6446" s="T615">Tĭn dagaj ibi, snăpɨʔi (dʼaba-) dʼagarzittə. </ta>
            <ta e="T635" id="Seg_6447" s="T621">A dĭ (ked-) kădedə udabə bar băʔpi, măn măndəm:" Kĭnzeʔ", dĭ kĭnzəbi i sarbi. </ta>
            <ta e="T639" id="Seg_6448" s="T635">Dĭgəttə ej ĭzembi udat. </ta>
            <ta e="T640" id="Seg_6449" s="T639">((BRK)). </ta>
            <ta e="T643" id="Seg_6450" s="T640">Miʔ nagobiʔi doktărəʔi. </ta>
            <ta e="T648" id="Seg_6451" s="T643">Miʔ (noʔ-) nozi bar dʼazirbibaʔ. </ta>
            <ta e="T651" id="Seg_6452" s="T648">Mĭnzerleʔbə, dĭgəttə bĭtleʔbə. </ta>
            <ta e="T655" id="Seg_6453" s="T651">Onʼiʔ no ige, putʼəga. </ta>
            <ta e="T662" id="Seg_6454" s="T655">Žaludkə ĭzemnie, dĭm mĭnzerliel, bĭtliel, dĭgəttə amorlal. </ta>
            <ta e="T670" id="Seg_6455" s="T662">A onʼiʔ (no- i-) noʔ ige, (filitʼagɨ) numəjlieʔi. </ta>
            <ta e="T683" id="Seg_6456" s="T670">Dĭm ellil aranə, dĭ nugəj bʼeʔ šide dʼala, dĭgəttə bĭtlel, sĭjdə jakšə molal. </ta>
            <ta e="T684" id="Seg_6457" s="T683">((BRK)). </ta>
            <ta e="T691" id="Seg_6458" s="T684">(Tăn) miʔ nuzaŋdən (Maslenkagən) bar (inetsi) tunolaʔbəʔjə. </ta>
            <ta e="T699" id="Seg_6459" s="T691">Šindin inet büžüj šoləj, a šindin inet maləj. </ta>
            <ta e="T707" id="Seg_6460" s="T699">Dĭ, šindin maləj, dĭ четверть (ar-) ara nuldələj. </ta>
            <ta e="T708" id="Seg_6461" s="T707">((BRK)). </ta>
            <ta e="T712" id="Seg_6462" s="T708">Dĭgəttə baška ine detləʔi. </ta>
            <ta e="T720" id="Seg_6463" s="T712">A (dĭ=) dö ine ugandə бегунец, mănliaʔi dăre. </ta>
            <ta e="T724" id="Seg_6464" s="T720">Dĭgəttə dĭm tože maːluʔləj. </ta>
            <ta e="T728" id="Seg_6465" s="T724">Döbər (šo- šo-) nuʔməluʔləj. </ta>
            <ta e="T732" id="Seg_6466" s="T728">A dĭ ine maːluʔləj. </ta>
            <ta e="T733" id="Seg_6467" s="T732">((BRK)). </ta>
            <ta e="T739" id="Seg_6468" s="T733">Dĭgəttə dĭ inem (пот ilie- ilie). </ta>
            <ta e="T746" id="Seg_6469" s="T739">I mĭŋge, mĭnge koldʼəŋ, štobɨ ej nubi. </ta>
            <ta e="T747" id="Seg_6470" s="T746">((BRK)). </ta>
            <ta e="T751" id="Seg_6471" s="T747">Dĭgəttə ara iʔgö bĭtluʔləʔi. </ta>
            <ta e="T754" id="Seg_6472" s="T751">I bar saʔməluʔləʔjə. </ta>
            <ta e="T755" id="Seg_6473" s="T754">((BRK)). </ta>
            <ta e="T760" id="Seg_6474" s="T755">Dĭgəttə tibizeŋ paʔi iləʔi udandə. </ta>
            <ta e="T764" id="Seg_6475" s="T760">I dĭgəttə (nu- )… </ta>
            <ta e="T766" id="Seg_6476" s="T764">Останови маленько. </ta>
            <ta e="T767" id="Seg_6477" s="T766">((BRK)). </ta>
            <ta e="T768" id="Seg_6478" s="T767">(-parnuʔ). </ta>
            <ta e="T775" id="Seg_6479" s="T768">Dĭgəttə onʼiʔ (onʼiʔtə bar nuʔ (n-) nʼotlaʔbə. </ta>
            <ta e="T780" id="Seg_6480" s="T775">Dĭgəttə onʼiʔ kuštü (bašk- )… </ta>
            <ta e="T785" id="Seg_6481" s="T780">(Dĭ-) Dĭm bar ujundə nuldəluləj. </ta>
            <ta e="T790" id="Seg_6482" s="T785">Dĭ bar dĭgəttə ara nuluʔləj. </ta>
            <ta e="T792" id="Seg_6483" s="T790">Dĭgəttə bĭtlieʔi. </ta>
            <ta e="T793" id="Seg_6484" s="T792">((BRK)). </ta>
            <ta e="T802" id="Seg_6485" s="T793">Dĭgəttə (šidegöʔ nuzaŋ a-) šidegöʔ nuzaŋ iləʔi onʼiʔ onʼiʔtə. </ta>
            <ta e="T808" id="Seg_6486" s="T802">Dĭgəttə kajət kuštü döm (parəʔluj) dʼünə. </ta>
            <ta e="T813" id="Seg_6487" s="T808">Dĭgəttə dĭ ara nuldlia (dĭʔnə). </ta>
            <ta e="T814" id="Seg_6488" s="T813">Bĭtleʔbəʔjə. </ta>
            <ta e="T815" id="Seg_6489" s="T814">((BRK)). </ta>
            <ta e="T821" id="Seg_6490" s="T815">Dĭgəttə dʼünə barəʔluj, udabə bar büldləj. </ta>
            <ta e="T822" id="Seg_6491" s="T821">((BRK)). </ta>
            <ta e="T825" id="Seg_6492" s="T822">Вот vărotaʔi nulaʔbəʔjə. </ta>
            <ta e="T829" id="Seg_6493" s="T825">Kanaʔ, girgit aʔtʼinə kalal. </ta>
            <ta e="T834" id="Seg_6494" s="T829">Pravăj aʔtʼi bar ej jakše. </ta>
            <ta e="T836" id="Seg_6495" s="T834">Bar bălotăʔi. </ta>
            <ta e="T838" id="Seg_6496" s="T836">Paʔi nugaʔi. </ta>
            <ta e="T841" id="Seg_6497" s="T838">I bar grozaʔi. </ta>
            <ta e="T843" id="Seg_6498" s="T841">Beržə bar. </ta>
            <ta e="T847" id="Seg_6499" s="T843">Bazaj kălʼučkaʔi bar mündüʔleʔbəʔjə. </ta>
            <ta e="T858" id="Seg_6500" s="T847">A dĭbər kuŋgəŋ kalal, dĭn bar jakše, paʔi naga, ĭmbidə naga. </ta>
            <ta e="T860" id="Seg_6501" s="T858">Ugandə kuvas. </ta>
            <ta e="T866" id="Seg_6502" s="T860">Noʔ kuvas, всякий svʼetogəʔi sĭre, kömə. </ta>
            <ta e="T869" id="Seg_6503" s="T866">Виноград özerleʔbə, jablăkăʔi. </ta>
            <ta e="T879" id="Seg_6504" s="T869">Kuja mĭndlaʔbə, ugandə ejü, xotʼ iʔbeʔ da kunolaʔ, jakše dĭn. </ta>
            <ta e="T891" id="Seg_6505" s="T879">A lʼevăj udandə kandəga ugandə aktʼi urgo, il iʔgö, bar ara bĭtlieʔi. </ta>
            <ta e="T895" id="Seg_6506" s="T891">Bar ĭmbi amniaʔi bar. </ta>
            <ta e="T899" id="Seg_6507" s="T895">Suʔmileʔbəʔjə bar, nüjnə nüjleʔbəʔjə. </ta>
            <ta e="T905" id="Seg_6508" s="T899">A (dĭ-) dĭbər kuŋgə (ka-) kalal. </ta>
            <ta e="T912" id="Seg_6509" s="T905">Dĭn bar šü, bar piʔi, bar grozaʔi. </ta>
            <ta e="T916" id="Seg_6510" s="T912">Šü iləm bar nendlaʔbə. </ta>
            <ta e="T918" id="Seg_6511" s="T916">Tüj bar. </ta>
            <ta e="T919" id="Seg_6512" s="T918">((BRK)). </ta>
            <ta e="T924" id="Seg_6513" s="T919">Šiʔ kaŋgaʔ, a măn (üjilgerlam). </ta>
            <ta e="T931" id="Seg_6514" s="T924">Nada măna ipek nuldəsʼtə, pürzittə ipek nada. </ta>
            <ta e="T935" id="Seg_6515" s="T931">A to amzittə naga ipek. </ta>
            <ta e="T938" id="Seg_6516" s="T935">Šiʔnʼileʔ mĭlem, amorbilaʔ. </ta>
            <ta e="T939" id="Seg_6517" s="T938">((BRK)). </ta>
            <ta e="T946" id="Seg_6518" s="T939">(Iʔ) kanaʔ tibinə, amnoʔ, unnʼanə jakše amnozittə. </ta>
            <ta e="T951" id="Seg_6519" s="T946">Giber-nʼibudʼ kalam, šindinədə ej surarbiam. </ta>
            <ta e="T952" id="Seg_6520" s="T951">((BRK)). </ta>
            <ta e="T955" id="Seg_6521" s="T952">Bü mʼaŋnaʔbə dĭ. </ta>
            <ta e="T968" id="Seg_6522" s="T955">Urgo Ilʼbinʼ, üdʼüge Ilʼbinʼ, a gijen bü ileʔbə, dĭn bar numəjlieʔi bar, talis. </ta>
            <ta e="T969" id="Seg_6523" s="T968">((BRK)). </ta>
            <ta e="T982" id="Seg_6524" s="T969">Onʼiʔ kö bar miʔ aš pĭdebibeʔ, dĭzeŋ bar üge amnolaʔbəʔjə, amnolaʔbəʔjə, dʼăbaktərlaʔbəʔjə, kaknarlaʔbəʔjə. </ta>
            <ta e="T992" id="Seg_6525" s="T982">Kanžəbaʔ pĭdesʼtə, a to šišəge moləj, все равно miʔnʼibeʔ nada pĭdesʼtə. </ta>
            <ta e="T997" id="Seg_6526" s="T992">(Tănan=) Tănan üge iʔgö kereʔ. </ta>
            <ta e="T1001" id="Seg_6527" s="T997">Dĭgəttə udazaŋdə bar kăndlaʔbəʔjə. </ta>
            <ta e="T1008" id="Seg_6528" s="T1001">Sĭre saʔməluʔləj, pa bazo miʔ (pide-) ((DMG)). </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_6529" s="T0">măn</ta>
            <ta e="T2" id="Seg_6530" s="T1">teinen</ta>
            <ta e="T3" id="Seg_6531" s="T2">šiʔnʼileʔ</ta>
            <ta e="T5" id="Seg_6532" s="T4">kudol-bia-m</ta>
            <ta e="T6" id="Seg_6533" s="T5">šo-bi-laʔ</ta>
            <ta e="T7" id="Seg_6534" s="T6">dʼăbaktər-zittə</ta>
            <ta e="T8" id="Seg_6535" s="T7">a</ta>
            <ta e="T9" id="Seg_6536" s="T8">ĭmbi=də</ta>
            <ta e="T10" id="Seg_6537" s="T9">ej</ta>
            <ta e="T11" id="Seg_6538" s="T10">nʼilgö-laʔbə-leʔ</ta>
            <ta e="T12" id="Seg_6539" s="T11">ej</ta>
            <ta e="T14" id="Seg_6540" s="T13">ej</ta>
            <ta e="T15" id="Seg_6541" s="T14">măn-də-lia-laʔ</ta>
            <ta e="T17" id="Seg_6542" s="T16">teinen</ta>
            <ta e="T18" id="Seg_6543" s="T17">dʼăbaktər-bi</ta>
            <ta e="T19" id="Seg_6544" s="T18">jakše</ta>
            <ta e="T21" id="Seg_6545" s="T20">dʼăbaktər-bi</ta>
            <ta e="T22" id="Seg_6546" s="T21">măn</ta>
            <ta e="T23" id="Seg_6547" s="T22">nʼilgö-bia-m</ta>
            <ta e="T24" id="Seg_6548" s="T23">da</ta>
            <ta e="T25" id="Seg_6549" s="T24">ej</ta>
            <ta e="T26" id="Seg_6550" s="T25">bar</ta>
            <ta e="T28" id="Seg_6551" s="T27">üge</ta>
            <ta e="T29" id="Seg_6552" s="T28">togonor-ia-m</ta>
            <ta e="T30" id="Seg_6553" s="T29">nʼilgö-sʼtə</ta>
            <ta e="T32" id="Seg_6554" s="T31">kăde=də</ta>
            <ta e="T33" id="Seg_6555" s="T32">ej</ta>
            <ta e="T34" id="Seg_6556" s="T33">mo-lia-m</ta>
            <ta e="T35" id="Seg_6557" s="T34">šindi=də</ta>
            <ta e="T36" id="Seg_6558" s="T35">dʼăbaktər-laʔbə</ta>
            <ta e="T37" id="Seg_6559" s="T36">sĭre-zi</ta>
            <ta e="T38" id="Seg_6560" s="T37">tăn</ta>
            <ta e="T39" id="Seg_6561" s="T38">iʔbə-ʔ</ta>
            <ta e="T40" id="Seg_6562" s="T39">dʼü-nə</ta>
            <ta e="T41" id="Seg_6563" s="T40">iʔbo-ʔ</ta>
            <ta e="T42" id="Seg_6564" s="T41">i-ʔ</ta>
            <ta e="T43" id="Seg_6565" s="T42">dʼăbaktər-a-ʔ</ta>
            <ta e="T44" id="Seg_6566" s="T43">i-ʔ</ta>
            <ta e="T45" id="Seg_6567" s="T44">kirgar-a-ʔ</ta>
            <ta e="T46" id="Seg_6568" s="T45">a</ta>
            <ta e="T47" id="Seg_6569" s="T46">ĭmbi</ta>
            <ta e="T48" id="Seg_6570" s="T47">măn</ta>
            <ta e="T49" id="Seg_6571" s="T48">ej</ta>
            <ta e="T50" id="Seg_6572" s="T49">dʼăbaktər-la-m</ta>
            <ta e="T51" id="Seg_6573" s="T50">da</ta>
            <ta e="T52" id="Seg_6574" s="T51">ej</ta>
            <ta e="T53" id="Seg_6575" s="T52">kirgar-la-m</ta>
            <ta e="T54" id="Seg_6576" s="T53">da</ta>
            <ta e="T56" id="Seg_6577" s="T55">kondʼo</ta>
            <ta e="T57" id="Seg_6578" s="T56">amno-la-l</ta>
            <ta e="T58" id="Seg_6579" s="T57">ej</ta>
            <ta e="T59" id="Seg_6580" s="T58">kü-le-l</ta>
            <ta e="T60" id="Seg_6581" s="T59">jakše</ta>
            <ta e="T61" id="Seg_6582" s="T60">mo-lə-j</ta>
            <ta e="T62" id="Seg_6583" s="T61">tănan</ta>
            <ta e="T63" id="Seg_6584" s="T62">iʔbo-ʔ</ta>
            <ta e="T64" id="Seg_6585" s="T63">iʔbo-ʔ</ta>
            <ta e="T65" id="Seg_6586" s="T64">dĭgəttə</ta>
            <ta e="T66" id="Seg_6587" s="T65">šindi=də</ta>
            <ta e="T67" id="Seg_6588" s="T66">bar</ta>
            <ta e="T68" id="Seg_6589" s="T67">nʼergö-luʔ-pi</ta>
            <ta e="T69" id="Seg_6590" s="T68">dĭ-m</ta>
            <ta e="T70" id="Seg_6591" s="T69">i</ta>
            <ta e="T71" id="Seg_6592" s="T70">üžüm-bi</ta>
            <ta e="T72" id="Seg_6593" s="T71">dĭ-m</ta>
            <ta e="T73" id="Seg_6594" s="T72">dĭgəttə</ta>
            <ta e="T74" id="Seg_6595" s="T73">šišəge</ta>
            <ta e="T75" id="Seg_6596" s="T74">dĭ-m</ta>
            <ta e="T76" id="Seg_6597" s="T75">ugandə</ta>
            <ta e="T77" id="Seg_6598" s="T76">tăŋ</ta>
            <ta e="T78" id="Seg_6599" s="T77">kăm-bi</ta>
            <ta e="T79" id="Seg_6600" s="T78">kăm-bi</ta>
            <ta e="T80" id="Seg_6601" s="T79">a</ta>
            <ta e="T81" id="Seg_6602" s="T80">dĭgəttə</ta>
            <ta e="T82" id="Seg_6603" s="T81">ejü</ta>
            <ta e="T83" id="Seg_6604" s="T82">mo-lam-bi</ta>
            <ta e="T84" id="Seg_6605" s="T83">kuja</ta>
            <ta e="T85" id="Seg_6606" s="T84">uʔbdə-bi</ta>
            <ta e="T86" id="Seg_6607" s="T85">nʼuʔdə</ta>
            <ta e="T87" id="Seg_6608" s="T86">uʔbdə-bi</ta>
            <ta e="T88" id="Seg_6609" s="T87">sĭre</ta>
            <ta e="T89" id="Seg_6610" s="T88">bar</ta>
            <ta e="T90" id="Seg_6611" s="T89">nereʔ-luʔ-pi</ta>
            <ta e="T91" id="Seg_6612" s="T90">i</ta>
            <ta e="T92" id="Seg_6613" s="T91">dĭgəttə</ta>
            <ta e="T93" id="Seg_6614" s="T92">bar</ta>
            <ta e="T97" id="Seg_6615" s="T96">dĭgəttə</ta>
            <ta e="T98" id="Seg_6616" s="T97">bar</ta>
            <ta e="T101" id="Seg_6617" s="T100">bü</ta>
            <ta e="T102" id="Seg_6618" s="T101">mʼaŋ-ŋaʔbə</ta>
            <ta e="T103" id="Seg_6619" s="T102">i</ta>
            <ta e="T104" id="Seg_6620" s="T103">sĭre</ta>
            <ta e="T105" id="Seg_6621" s="T104">kü-lam-bi</ta>
            <ta e="T107" id="Seg_6622" s="T106">bar</ta>
            <ta e="T108" id="Seg_6623" s="T107">jezirer-luʔ-pi</ta>
            <ta e="T109" id="Seg_6624" s="T108">i</ta>
            <ta e="T110" id="Seg_6625" s="T109">bar</ta>
            <ta e="T111" id="Seg_6626" s="T110">ananugosʼtə</ta>
            <ta e="T112" id="Seg_6627" s="T111">i-bi</ta>
            <ta e="T113" id="Seg_6628" s="T112">uda-ndə</ta>
            <ta e="T114" id="Seg_6629" s="T113">tüjö</ta>
            <ta e="T115" id="Seg_6630" s="T114">măn-də</ta>
            <ta e="T116" id="Seg_6631" s="T115">toʔ-nar-la-m</ta>
            <ta e="T117" id="Seg_6632" s="T116">măn</ta>
            <ta e="T118" id="Seg_6633" s="T117">ia-m</ta>
            <ta e="T119" id="Seg_6634" s="T118">ešši</ta>
            <ta e="T120" id="Seg_6635" s="T119">deʔ-pi</ta>
            <ta e="T121" id="Seg_6636" s="T120">nʼi</ta>
            <ta e="T122" id="Seg_6637" s="T121">deʔ-pi</ta>
            <ta e="T123" id="Seg_6638" s="T122">tura-gən</ta>
            <ta e="T124" id="Seg_6639" s="T123">bar</ta>
            <ta e="T125" id="Seg_6640" s="T124">ĭzem-bi</ta>
            <ta e="T126" id="Seg_6641" s="T125">kirgar-bi</ta>
            <ta e="T127" id="Seg_6642" s="T126">urgaja</ta>
            <ta e="T128" id="Seg_6643" s="T127">i-bi</ta>
            <ta e="T129" id="Seg_6644" s="T128">dĭ</ta>
            <ta e="T130" id="Seg_6645" s="T129">deʔ-pi</ta>
            <ta e="T132" id="Seg_6646" s="T131">ešši</ta>
            <ta e="T133" id="Seg_6647" s="T132">dĭ</ta>
            <ta e="T134" id="Seg_6648" s="T133">bar</ta>
            <ta e="T136" id="Seg_6649" s="T135">băʔtə-bi</ta>
            <ta e="T137" id="Seg_6650" s="T136">sar-bi</ta>
            <ta e="T138" id="Seg_6651" s="T137">i</ta>
            <ta e="T139" id="Seg_6652" s="T138">băzə-bi</ta>
            <ta e="T140" id="Seg_6653" s="T139">dĭgəttə</ta>
            <ta e="T144" id="Seg_6654" s="T143">dĭgəttə</ta>
            <ta e="T145" id="Seg_6655" s="T144">ku-bi</ta>
            <ta e="T146" id="Seg_6656" s="T145">trʼapka-ʔi</ta>
            <ta e="T147" id="Seg_6657" s="T146">kür-bi</ta>
            <ta e="T148" id="Seg_6658" s="T147">dĭ-m</ta>
            <ta e="T149" id="Seg_6659" s="T148">i</ta>
            <ta e="T150" id="Seg_6660" s="T149">em-bi</ta>
            <ta e="T151" id="Seg_6661" s="T150">pʼeš-tə</ta>
            <ta e="T152" id="Seg_6662" s="T151">dĭ</ta>
            <ta e="T153" id="Seg_6663" s="T152">bar</ta>
            <ta e="T154" id="Seg_6664" s="T153">iʔbo-laʔbə</ta>
            <ta e="T155" id="Seg_6665" s="T154">a</ta>
            <ta e="T156" id="Seg_6666" s="T155">dĭ-m</ta>
            <ta e="T158" id="Seg_6667" s="T157">numəj-leʔbə-ʔjə</ta>
            <ta e="T159" id="Seg_6668" s="T158">dĭgəttə</ta>
            <ta e="T160" id="Seg_6669" s="T159">kum-bi-ʔi</ta>
            <ta e="T161" id="Seg_6670" s="T160">abəs-tə</ta>
            <ta e="T162" id="Seg_6671" s="T161">i</ta>
            <ta e="T163" id="Seg_6672" s="T162">numəj-leʔbə-ʔjə</ta>
            <ta e="T165" id="Seg_6673" s="T164">a</ta>
            <ta e="T166" id="Seg_6674" s="T165">măn</ta>
            <ta e="T167" id="Seg_6675" s="T166">ia-m</ta>
            <ta e="T168" id="Seg_6676" s="T167">dĭ-m</ta>
            <ta e="T169" id="Seg_6677" s="T168">deʔ-pi</ta>
            <ta e="T170" id="Seg_6678" s="T169">măna</ta>
            <ta e="T171" id="Seg_6679" s="T170">bʼeʔ</ta>
            <ta e="T172" id="Seg_6680" s="T171">šide</ta>
            <ta e="T173" id="Seg_6681" s="T172">kö</ta>
            <ta e="T174" id="Seg_6682" s="T173">i-bi</ta>
            <ta e="T175" id="Seg_6683" s="T174">dĭgəttə</ta>
            <ta e="T176" id="Seg_6684" s="T175">dĭ</ta>
            <ta e="T178" id="Seg_6685" s="T177">özer-bi</ta>
            <ta e="T179" id="Seg_6686" s="T178">özer-bi</ta>
            <ta e="T180" id="Seg_6687" s="T179">bʼeʔ</ta>
            <ta e="T181" id="Seg_6688" s="T180">šide</ta>
            <ta e="T182" id="Seg_6689" s="T181">bʼeʔ</ta>
            <ta e="T183" id="Seg_6690" s="T182">šide</ta>
            <ta e="T184" id="Seg_6691" s="T183">kö</ta>
            <ta e="T185" id="Seg_6692" s="T184">i-bi</ta>
            <ta e="T186" id="Seg_6693" s="T185">ia-t</ta>
            <ta e="T187" id="Seg_6694" s="T186">kü-lam-bi</ta>
            <ta e="T188" id="Seg_6695" s="T187">dĭ</ta>
            <ta e="T189" id="Seg_6696" s="T188">ma-bi</ta>
            <ta e="T190" id="Seg_6697" s="T189">a</ta>
            <ta e="T193" id="Seg_6698" s="T192">ia-m</ta>
            <ta e="T194" id="Seg_6699" s="T193">kuʔ-pi-ʔi</ta>
            <ta e="T195" id="Seg_6700" s="T194">dĭ-m</ta>
            <ta e="T196" id="Seg_6701" s="T195">kuʔ-pi</ta>
            <ta e="T198" id="Seg_6702" s="T197">dĭ-n</ta>
            <ta e="T199" id="Seg_6703" s="T198">i</ta>
            <ta e="T200" id="Seg_6704" s="T199">dĭ</ta>
            <ta e="T201" id="Seg_6705" s="T200">bar</ta>
            <ta e="T202" id="Seg_6706" s="T201">kü-lam-bi</ta>
            <ta e="T203" id="Seg_6707" s="T202">ine-zi</ta>
            <ta e="T204" id="Seg_6708" s="T203">maːʔ-ndə</ta>
            <ta e="T205" id="Seg_6709" s="T204">deʔ-pi-ʔi</ta>
            <ta e="T206" id="Seg_6710" s="T205">măn</ta>
            <ta e="T207" id="Seg_6711" s="T206">aba-m</ta>
            <ta e="T208" id="Seg_6712" s="T207">dʼije-gən</ta>
            <ta e="T209" id="Seg_6713" s="T208">kü-lam-bi</ta>
            <ta e="T210" id="Seg_6714" s="T209">dĭgəttə</ta>
            <ta e="T211" id="Seg_6715" s="T210">sʼestra-m</ta>
            <ta e="T212" id="Seg_6716" s="T211">i</ta>
            <ta e="T213" id="Seg_6717" s="T212">măn</ta>
            <ta e="T214" id="Seg_6718" s="T213">tibi-m</ta>
            <ta e="T215" id="Seg_6719" s="T214">šide</ta>
            <ta e="T216" id="Seg_6720" s="T215">ine-gə-t-si</ta>
            <ta e="T217" id="Seg_6721" s="T216">deʔ-pi-ʔi</ta>
            <ta e="T218" id="Seg_6722" s="T217">maʔ-nʼibəj</ta>
            <ta e="T219" id="Seg_6723" s="T218">măn</ta>
            <ta e="T220" id="Seg_6724" s="T219">ugandə</ta>
            <ta e="T221" id="Seg_6725" s="T220">tăŋ</ta>
            <ta e="T222" id="Seg_6726" s="T221">dʼor-bia-m</ta>
            <ta e="T223" id="Seg_6727" s="T222">urgo</ta>
            <ta e="T224" id="Seg_6728" s="T223">dʼala</ta>
            <ta e="T225" id="Seg_6729" s="T224">i-bi</ta>
            <ta e="T227" id="Seg_6730" s="T226">dĭgəttə</ta>
            <ta e="T228" id="Seg_6731" s="T227">dʼü</ta>
            <ta e="T229" id="Seg_6732" s="T228">dʼü</ta>
            <ta e="T230" id="Seg_6733" s="T229">tĭl-bi-beʔ</ta>
            <ta e="T231" id="Seg_6734" s="T230">maʔ</ta>
            <ta e="T232" id="Seg_6735" s="T231">a-bi-beʔ</ta>
            <ta e="T233" id="Seg_6736" s="T232">krospa</ta>
            <ta e="T234" id="Seg_6737" s="T233">a-bi-beʔ</ta>
            <ta e="T235" id="Seg_6738" s="T234">i</ta>
            <ta e="T236" id="Seg_6739" s="T235">dʼü-nə</ta>
            <ta e="T237" id="Seg_6740" s="T236">em-bi-beʔ</ta>
            <ta e="T238" id="Seg_6741" s="T237">kămna-bi-baʔ</ta>
            <ta e="T239" id="Seg_6742" s="T238">dĭgəttə</ta>
            <ta e="T240" id="Seg_6743" s="T239">bü</ta>
            <ta e="T241" id="Seg_6744" s="T240">bĭʔ-pi-beʔ</ta>
            <ta e="T242" id="Seg_6745" s="T241">ipek</ta>
            <ta e="T243" id="Seg_6746" s="T242">am-bi-baʔ</ta>
            <ta e="T244" id="Seg_6747" s="T243">il</ta>
            <ta e="T245" id="Seg_6748" s="T244">i-bi-ʔi</ta>
            <ta e="T246" id="Seg_6749" s="T245">kabarləj</ta>
            <ta e="T247" id="Seg_6750" s="T246">šide</ta>
            <ta e="T248" id="Seg_6751" s="T247">nu-zaŋ</ta>
            <ta e="T249" id="Seg_6752" s="T248">amno-bi-ʔi</ta>
            <ta e="T250" id="Seg_6753" s="T249">nüke</ta>
            <ta e="T251" id="Seg_6754" s="T250">i</ta>
            <ta e="T252" id="Seg_6755" s="T251">büzʼe</ta>
            <ta e="T253" id="Seg_6756" s="T252">ara</ta>
            <ta e="T254" id="Seg_6757" s="T253">bĭʔ-pi-ʔi</ta>
            <ta e="T255" id="Seg_6758" s="T254">bar</ta>
            <ta e="T256" id="Seg_6759" s="T255">dʼabro-bi-ʔi</ta>
            <ta e="T257" id="Seg_6760" s="T256">dĭgəttə</ta>
            <ta e="T258" id="Seg_6761" s="T257">părog-əndə</ta>
            <ta e="T259" id="Seg_6762" s="T258">amno-bi-ʔi</ta>
            <ta e="T260" id="Seg_6763" s="T259">dĭ</ta>
            <ta e="T261" id="Seg_6764" s="T260">ulu-ndə</ta>
            <ta e="T262" id="Seg_6765" s="T261">teʔtə</ta>
            <ta e="T263" id="Seg_6766" s="T262">i-bi</ta>
            <ta e="T264" id="Seg_6767" s="T263">nüke</ta>
            <ta e="T265" id="Seg_6768" s="T264">i</ta>
            <ta e="T266" id="Seg_6769" s="T265">büzʼe-t</ta>
            <ta e="T267" id="Seg_6770" s="T266">nʼeʔ-do-laʔ-pi</ta>
            <ta e="T268" id="Seg_6771" s="T267">nʼiʔ-do-laʔ-pi</ta>
            <ta e="T269" id="Seg_6772" s="T268">i</ta>
            <ta e="T270" id="Seg_6773" s="T269">dăre</ta>
            <ta e="T271" id="Seg_6774" s="T270">kunol-luʔ-pi-ʔi</ta>
            <ta e="T272" id="Seg_6775" s="T271">ertə-n</ta>
            <ta e="T273" id="Seg_6776" s="T272">uʔ-pi-ʔi</ta>
            <ta e="T274" id="Seg_6777" s="T273">măn</ta>
            <ta e="T275" id="Seg_6778" s="T274">ĭmbi=də</ta>
            <ta e="T276" id="Seg_6779" s="T275">bar</ta>
            <ta e="T277" id="Seg_6780" s="T276">ĭzem-nie-m</ta>
            <ta e="T278" id="Seg_6781" s="T277">a</ta>
            <ta e="T279" id="Seg_6782" s="T278">dĭ</ta>
            <ta e="T280" id="Seg_6783" s="T279">măn-də</ta>
            <ta e="T281" id="Seg_6784" s="T280">măn</ta>
            <ta e="T282" id="Seg_6785" s="T281">tože</ta>
            <ta e="T283" id="Seg_6786" s="T282">ĭzem-nie-m</ta>
            <ta e="T284" id="Seg_6787" s="T283">naverna</ta>
            <ta e="T285" id="Seg_6788" s="T284">miʔ</ta>
            <ta e="T286" id="Seg_6789" s="T285">dʼabro-bi-baʔ</ta>
            <ta e="T288" id="Seg_6790" s="T287">dĭ-m</ta>
            <ta e="T289" id="Seg_6791" s="T288">kăštə-bi-ʔi</ta>
            <ta e="T293" id="Seg_6792" s="T292">a</ta>
            <ta e="T294" id="Seg_6793" s="T293">nüke-m</ta>
            <ta e="T295" id="Seg_6794" s="T294">kăštə-bi-ʔi</ta>
            <ta e="T299" id="Seg_6795" s="T298">dĭ-zen</ta>
            <ta e="T300" id="Seg_6796" s="T299">tura-zaŋ-də</ta>
            <ta e="T301" id="Seg_6797" s="T300">üdʼüge</ta>
            <ta e="T302" id="Seg_6798" s="T301">i-bi-ʔi</ta>
            <ta e="T303" id="Seg_6799" s="T302">i</ta>
            <ta e="T304" id="Seg_6800" s="T303">dĭn</ta>
            <ta e="T305" id="Seg_6801" s="T304">töštek-kən</ta>
            <ta e="T306" id="Seg_6802" s="T305">nu-laʔ-pi</ta>
            <ta e="T308" id="Seg_6803" s="T307">dĭ-n</ta>
            <ta e="T309" id="Seg_6804" s="T308">tolʼko</ta>
            <ta e="T310" id="Seg_6805" s="T309">sʼestra-t</ta>
            <ta e="T311" id="Seg_6806" s="T310">i-bi</ta>
            <ta e="T312" id="Seg_6807" s="T311">kăštə-bi-ʔi</ta>
            <ta e="T313" id="Seg_6808" s="T312">Fanasʼeja-ʔi-zi</ta>
            <ta e="T314" id="Seg_6809" s="T313">es-seŋ-də</ta>
            <ta e="T315" id="Seg_6810" s="T314">nago-bi-ʔi</ta>
            <ta e="T316" id="Seg_6811" s="T315">dăre</ta>
            <ta e="T317" id="Seg_6812" s="T316">bar</ta>
            <ta e="T318" id="Seg_6813" s="T317">bar</ta>
            <ta e="T319" id="Seg_6814" s="T318">kü-lam-bi-ʔi</ta>
            <ta e="T321" id="Seg_6815" s="T320">bar</ta>
            <ta e="T322" id="Seg_6816" s="T321">nagur</ta>
            <ta e="T323" id="Seg_6817" s="T322">kü-lam-bi-ʔi</ta>
            <ta e="T327" id="Seg_6818" s="T326">sʼestra-t</ta>
            <ta e="T328" id="Seg_6819" s="T327">ĭmbi</ta>
            <ta e="T329" id="Seg_6820" s="T328">šiʔ</ta>
            <ta e="T330" id="Seg_6821" s="T329">dʼabro-bi-laʔ</ta>
            <ta e="T331" id="Seg_6822" s="T330">a</ta>
            <ta e="T332" id="Seg_6823" s="T331">măn</ta>
            <ta e="T333" id="Seg_6824" s="T332">šiʔnʼileʔ</ta>
            <ta e="T334" id="Seg_6825" s="T333">ej</ta>
            <ta e="T335" id="Seg_6826" s="T334">izen-bi-m</ta>
            <ta e="T336" id="Seg_6827" s="T335">dăre</ta>
            <ta e="T337" id="Seg_6828" s="T336">bar</ta>
            <ta e="T338" id="Seg_6829" s="T337">kunol-luʔ-pi-leʔ</ta>
            <ta e="T340" id="Seg_6830" s="T339">nu-zaŋ-gən</ta>
            <ta e="T341" id="Seg_6831" s="T340">i-bi</ta>
            <ta e="T342" id="Seg_6832" s="T341">urgo</ta>
            <ta e="T343" id="Seg_6833" s="T342">dʼala</ta>
            <ta e="T345" id="Seg_6834" s="T344">dʼala</ta>
            <ta e="T346" id="Seg_6835" s="T345">onʼiʔ</ta>
            <ta e="T347" id="Seg_6836" s="T346">tura-nə</ta>
            <ta e="T348" id="Seg_6837" s="T347">oʔbdə-lia-ʔi</ta>
            <ta e="T349" id="Seg_6838" s="T348">bar</ta>
            <ta e="T350" id="Seg_6839" s="T349">dĭgəttə</ta>
            <ta e="T351" id="Seg_6840" s="T350">stol-də</ta>
            <ta e="T352" id="Seg_6841" s="T351">oʔbdə-lia-ʔi</ta>
            <ta e="T353" id="Seg_6842" s="T352">akămnaʔ-lia-ʔi</ta>
            <ta e="T354" id="Seg_6843" s="T353">i</ta>
            <ta e="T355" id="Seg_6844" s="T354">bĭt-leʔbə-ʔjə</ta>
            <ta e="T356" id="Seg_6845" s="T355">ara</ta>
            <ta e="T357" id="Seg_6846" s="T356">i</ta>
            <ta e="T358" id="Seg_6847" s="T357">am-naʔbə-ʔjə</ta>
            <ta e="T359" id="Seg_6848" s="T358">uja</ta>
            <ta e="T360" id="Seg_6849" s="T359">ipek</ta>
            <ta e="T361" id="Seg_6850" s="T360">ĭmbi</ta>
            <ta e="T362" id="Seg_6851" s="T361">i-ge</ta>
            <ta e="T363" id="Seg_6852" s="T362">stol-gən</ta>
            <ta e="T364" id="Seg_6853" s="T363">dĭgəttə</ta>
            <ta e="T365" id="Seg_6854" s="T364">baška</ta>
            <ta e="T366" id="Seg_6855" s="T365">tura-nə</ta>
            <ta e="T367" id="Seg_6856" s="T366">ka-la-ʔi</ta>
            <ta e="T368" id="Seg_6857" s="T367">dĭn</ta>
            <ta e="T369" id="Seg_6858" s="T368">dăre</ta>
            <ta e="T370" id="Seg_6859" s="T369">že</ta>
            <ta e="T371" id="Seg_6860" s="T370">bĭt-le-ʔi</ta>
            <ta e="T372" id="Seg_6861" s="T371">dĭgəttə</ta>
            <ta e="T373" id="Seg_6862" s="T372">išo</ta>
            <ta e="T374" id="Seg_6863" s="T373">onʼiʔ</ta>
            <ta e="T375" id="Seg_6864" s="T374">tura-nə</ta>
            <ta e="T376" id="Seg_6865" s="T375">dĭgəttə</ta>
            <ta e="T377" id="Seg_6866" s="T376">teʔtə</ta>
            <ta e="T378" id="Seg_6867" s="T377">tura-nə</ta>
            <ta e="T379" id="Seg_6868" s="T378">sumna</ta>
            <ta e="T381" id="Seg_6869" s="T380">sɨləj</ta>
            <ta e="T382" id="Seg_6870" s="T381">sumna</ta>
            <ta e="T383" id="Seg_6871" s="T382">dʼala</ta>
            <ta e="T384" id="Seg_6872" s="T383">bĭt-leʔbə-ʔjə</ta>
            <ta e="T385" id="Seg_6873" s="T384">üge</ta>
            <ta e="T386" id="Seg_6874" s="T385">ara</ta>
            <ta e="T387" id="Seg_6875" s="T386">i</ta>
            <ta e="T388" id="Seg_6876" s="T387">dĭgəttə</ta>
            <ta e="T389" id="Seg_6877" s="T388">kabarləj</ta>
            <ta e="T1011" id="Seg_6878" s="T391">Kazan</ta>
            <ta e="T392" id="Seg_6879" s="T1011">tura-gən</ta>
            <ta e="T394" id="Seg_6880" s="T393">i-bi</ta>
            <ta e="T395" id="Seg_6881" s="T394">dĭ-zeŋ</ta>
            <ta e="T396" id="Seg_6882" s="T395">dibər</ta>
            <ta e="T397" id="Seg_6883" s="T396">kam-bi-ʔi</ta>
            <ta e="T398" id="Seg_6884" s="T397">i</ta>
            <ta e="T399" id="Seg_6885" s="T398">dĭn</ta>
            <ta e="T400" id="Seg_6886" s="T399">ara</ta>
            <ta e="T401" id="Seg_6887" s="T400">i-bi-ʔi</ta>
            <ta e="T402" id="Seg_6888" s="T401">šindi-n</ta>
            <ta e="T403" id="Seg_6889" s="T402">aktʼa</ta>
            <ta e="T404" id="Seg_6890" s="T403">iʔgö</ta>
            <ta e="T405" id="Seg_6891" s="T404">dăk</ta>
            <ta e="T406" id="Seg_6892" s="T405">iʔgö</ta>
            <ta e="T407" id="Seg_6893" s="T406">i-lie-ʔi</ta>
            <ta e="T408" id="Seg_6894" s="T407">sumna</ta>
            <ta e="T410" id="Seg_6895" s="T409">a</ta>
            <ta e="T411" id="Seg_6896" s="T410">šindi-n</ta>
            <ta e="T412" id="Seg_6897" s="T411">amga</ta>
            <ta e="T413" id="Seg_6898" s="T412">teʔtə</ta>
            <ta e="T414" id="Seg_6899" s="T413">i-lə-j</ta>
            <ta e="T415" id="Seg_6900" s="T414">šindi-n</ta>
            <ta e="T416" id="Seg_6901" s="T415">išo</ta>
            <ta e="T417" id="Seg_6902" s="T416">amga</ta>
            <ta e="T418" id="Seg_6903" s="T417">šide</ta>
            <ta e="T419" id="Seg_6904" s="T418">i-lə-j</ta>
            <ta e="T420" id="Seg_6905" s="T419">i</ta>
            <ta e="T421" id="Seg_6906" s="T420">dĭgəttə</ta>
            <ta e="T422" id="Seg_6907" s="T421">urgo</ta>
            <ta e="T423" id="Seg_6908" s="T422">dʼala-gən</ta>
            <ta e="T424" id="Seg_6909" s="T423">bĭʔ-pi-ʔi</ta>
            <ta e="T425" id="Seg_6910" s="T424">üdʼüge</ta>
            <ta e="T426" id="Seg_6911" s="T425">dĭrgit</ta>
            <ta e="T427" id="Seg_6912" s="T426">rʼumka-nə</ta>
            <ta e="T428" id="Seg_6913" s="T427">kămnə-bi-ʔi</ta>
            <ta e="T429" id="Seg_6914" s="T428">onʼiʔ-tə</ta>
            <ta e="T430" id="Seg_6915" s="T429">mĭ-lə-j</ta>
            <ta e="T431" id="Seg_6916" s="T430">dĭgəttə</ta>
            <ta e="T432" id="Seg_6917" s="T431">bazoʔ</ta>
            <ta e="T433" id="Seg_6918" s="T432">dak</ta>
            <ta e="T434" id="Seg_6919" s="T433">tăre</ta>
            <ta e="T435" id="Seg_6920" s="T434">tăre</ta>
            <ta e="T436" id="Seg_6921" s="T435">mĭ-lie</ta>
            <ta e="T437" id="Seg_6922" s="T436">mĭ-lie</ta>
            <ta e="T438" id="Seg_6923" s="T437">bar</ta>
            <ta e="T439" id="Seg_6924" s="T438">jude</ta>
            <ta e="T440" id="Seg_6925" s="T439">dĭgəttə</ta>
            <ta e="T441" id="Seg_6926" s="T440">amor-laʔbə-ʔjə</ta>
            <ta e="T442" id="Seg_6927" s="T441">am-naʔbə-ʔjə</ta>
            <ta e="T443" id="Seg_6928" s="T442">bar</ta>
            <ta e="T444" id="Seg_6929" s="T443">dʼăbaktər-laʔbə-ʔjə</ta>
            <ta e="T445" id="Seg_6930" s="T444">nüjnə</ta>
            <ta e="T446" id="Seg_6931" s="T445">nüj-leʔbə-ʔjə</ta>
            <ta e="T451" id="Seg_6932" s="T450">kam-bi</ta>
            <ta e="T453" id="Seg_6933" s="T451">Kazan_tura-nə</ta>
            <ta e="T455" id="Seg_6934" s="T454">i-bi</ta>
            <ta e="T456" id="Seg_6935" s="T455">urgo</ta>
            <ta e="T457" id="Seg_6936" s="T456">dʼala</ta>
            <ta e="T458" id="Seg_6937" s="T457">dĭn</ta>
            <ta e="T459" id="Seg_6938" s="T458">ara</ta>
            <ta e="T460" id="Seg_6939" s="T459">bĭʔ-pi-ʔi</ta>
            <ta e="T461" id="Seg_6940" s="T460">bar</ta>
            <ta e="T462" id="Seg_6941" s="T461">kažnej</ta>
            <ta e="T463" id="Seg_6942" s="T462">tura-nə</ta>
            <ta e="T464" id="Seg_6943" s="T463">mĭm-bi-ʔi</ta>
            <ta e="T465" id="Seg_6944" s="T464">dĭ</ta>
            <ta e="T466" id="Seg_6945" s="T465">nüdʼi-n</ta>
            <ta e="T1010" id="Seg_6946" s="T466">kal-la</ta>
            <ta e="T467" id="Seg_6947" s="T1010">dʼür-bi</ta>
            <ta e="T468" id="Seg_6948" s="T467">Anʼža-nə</ta>
            <ta e="T469" id="Seg_6949" s="T468">i</ta>
            <ta e="T470" id="Seg_6950" s="T469">dĭn</ta>
            <ta e="T471" id="Seg_6951" s="T470">kăn-nam-bi</ta>
            <ta e="T472" id="Seg_6952" s="T471">kü-lam-bi</ta>
            <ta e="T474" id="Seg_6953" s="T473">dĭ</ta>
            <ta e="T475" id="Seg_6954" s="T474">ma-ndə</ta>
            <ta e="T477" id="Seg_6955" s="T476">ma-ʔəndə</ta>
            <ta e="T478" id="Seg_6956" s="T477">šo-bi</ta>
            <ta e="T479" id="Seg_6957" s="T478">nʼiʔtə</ta>
            <ta e="T480" id="Seg_6958" s="T479">šide</ta>
            <ta e="T481" id="Seg_6959" s="T480">kuza</ta>
            <ta e="T482" id="Seg_6960" s="T481">dĭʔ-nə</ta>
            <ta e="T483" id="Seg_6961" s="T482">šo-bi-ʔi</ta>
            <ta e="T484" id="Seg_6962" s="T483">kan-žə-bəj</ta>
            <ta e="T486" id="Seg_6963" s="T485">măn-zi</ta>
            <ta e="T487" id="Seg_6964" s="T486">dĭgəttə</ta>
            <ta e="T488" id="Seg_6965" s="T487">kam-bi</ta>
            <ta e="T489" id="Seg_6966" s="T488">kam-bi</ta>
            <ta e="T490" id="Seg_6967" s="T489">ku-lio-t</ta>
            <ta e="T491" id="Seg_6968" s="T490">bar</ta>
            <ta e="T492" id="Seg_6969" s="T491">kulʼa</ta>
            <ta e="T493" id="Seg_6970" s="T492">bar</ta>
            <ta e="T495" id="Seg_6971" s="T494">dĭgəttə</ta>
            <ta e="T496" id="Seg_6972" s="T495">kudaj-də</ta>
            <ta e="T498" id="Seg_6973" s="T497">numan üzə-bi</ta>
            <ta e="T499" id="Seg_6974" s="T498">dĭʔə</ta>
            <ta e="T500" id="Seg_6975" s="T499">il-də</ta>
            <ta e="T501" id="Seg_6976" s="T500">nuʔmə-luʔ-pi-ʔi</ta>
            <ta e="T502" id="Seg_6977" s="T501">a</ta>
            <ta e="T503" id="Seg_6978" s="T502">dĭ</ta>
            <ta e="T504" id="Seg_6979" s="T503">maːʔ-ndə</ta>
            <ta e="T505" id="Seg_6980" s="T504">šo-bi</ta>
            <ta e="T506" id="Seg_6981" s="T505">bos-tə</ta>
            <ta e="T507" id="Seg_6982" s="T506">sĭre</ta>
            <ta e="T508" id="Seg_6983" s="T507">mo-bi</ta>
            <ta e="T510" id="Seg_6984" s="T509">dĭ-zeŋ</ta>
            <ta e="T511" id="Seg_6985" s="T510">ugandə</ta>
            <ta e="T512" id="Seg_6986" s="T511">ara</ta>
            <ta e="T513" id="Seg_6987" s="T512">iʔgö</ta>
            <ta e="T514" id="Seg_6988" s="T513">bĭt-leʔbə-ʔjə</ta>
            <ta e="T515" id="Seg_6989" s="T514">kumən</ta>
            <ta e="T516" id="Seg_6990" s="T515">xotʼ</ta>
            <ta e="T518" id="Seg_6991" s="T517">üge</ta>
            <ta e="T519" id="Seg_6992" s="T518">bĭt-leʔbə-ʔjə</ta>
            <ta e="T520" id="Seg_6993" s="T519">bĭt-leʔbə-ʔjə</ta>
            <ta e="T521" id="Seg_6994" s="T520">išo</ta>
            <ta e="T522" id="Seg_6995" s="T521">pi-leʔbə-ʔjə</ta>
            <ta e="T523" id="Seg_6996" s="T522">gijen</ta>
            <ta e="T524" id="Seg_6997" s="T523">i-ge</ta>
            <ta e="T525" id="Seg_6998" s="T524">dăk</ta>
            <ta e="T526" id="Seg_6999" s="T525">dibər</ta>
            <ta e="T527" id="Seg_7000" s="T526">nuʔmə-luʔbə-ʔjə</ta>
            <ta e="T528" id="Seg_7001" s="T527">bar</ta>
            <ta e="T530" id="Seg_7002" s="T529">dĭgəttə</ta>
            <ta e="T531" id="Seg_7003" s="T530">bar</ta>
            <ta e="T532" id="Seg_7004" s="T531">nüj-leʔbə-ʔjə</ta>
            <ta e="T533" id="Seg_7005" s="T532">suʔmi-leʔbə-ʔjə</ta>
            <ta e="T534" id="Seg_7006" s="T533">dĭgəttə</ta>
            <ta e="T535" id="Seg_7007" s="T534">bar</ta>
            <ta e="T536" id="Seg_7008" s="T535">dʼabro-laʔbə-ʔjə</ta>
            <ta e="T538" id="Seg_7009" s="T537">šindi</ta>
            <ta e="T539" id="Seg_7010" s="T538">bar</ta>
            <ta e="T540" id="Seg_7011" s="T539">iʔgö</ta>
            <ta e="T542" id="Seg_7012" s="T541">bĭt-lie</ta>
            <ta e="T543" id="Seg_7013" s="T542">bĭt-lie</ta>
            <ta e="T544" id="Seg_7014" s="T543">dĭgəttə</ta>
            <ta e="T545" id="Seg_7015" s="T544">ara-gəʔ</ta>
            <ta e="T546" id="Seg_7016" s="T545">bar</ta>
            <ta e="T547" id="Seg_7017" s="T546">nanʼi-leʔbə</ta>
            <ta e="T548" id="Seg_7018" s="T547">dĭ</ta>
            <ta e="T549" id="Seg_7019" s="T548">dĭʔ-nə</ta>
            <ta e="T550" id="Seg_7020" s="T549">aŋ-də</ta>
            <ta e="T551" id="Seg_7021" s="T550">bar</ta>
            <ta e="T552" id="Seg_7022" s="T551">kĭnz-leʔbə-ʔjə</ta>
            <ta e="T553" id="Seg_7023" s="T552">dĭgəttə</ta>
            <ta e="T554" id="Seg_7024" s="T553">dĭ</ta>
            <ta e="T556" id="Seg_7025" s="T555">uʔ-lə-j</ta>
            <ta e="T558" id="Seg_7026" s="T557">onʼiʔ</ta>
            <ta e="T559" id="Seg_7027" s="T558">onʼiʔ</ta>
            <ta e="T560" id="Seg_7028" s="T559">măn</ta>
            <ta e="T561" id="Seg_7029" s="T560">ku-m</ta>
            <ta e="T562" id="Seg_7030" s="T561">ĭzem-nuʔ-pi</ta>
            <ta e="T563" id="Seg_7031" s="T562">bar</ta>
            <ta e="T564" id="Seg_7032" s="T563">dĭn</ta>
            <ta e="T565" id="Seg_7033" s="T564">müʔ-leʔbə</ta>
            <ta e="T566" id="Seg_7034" s="T565">măn</ta>
            <ta e="T567" id="Seg_7035" s="T566">dĭgəttə</ta>
            <ta e="T568" id="Seg_7036" s="T567">ku-bia-m</ta>
            <ta e="T569" id="Seg_7037" s="T568">trʼapka</ta>
            <ta e="T570" id="Seg_7038" s="T569">kĭnzə-bie-m</ta>
            <ta e="T571" id="Seg_7039" s="T570">tĭ-gə</ta>
            <ta e="T573" id="Seg_7040" s="T572">ažnə</ta>
            <ta e="T574" id="Seg_7041" s="T573">mʼaŋ-nuʔ-pi</ta>
            <ta e="T575" id="Seg_7042" s="T574">măn</ta>
            <ta e="T576" id="Seg_7043" s="T575">ku-nə</ta>
            <ta e="T577" id="Seg_7044" s="T576">em-bie-m</ta>
            <ta e="T580" id="Seg_7045" s="T579">dĭgəttə</ta>
            <ta e="T581" id="Seg_7046" s="T580">sar-bio-m</ta>
            <ta e="T582" id="Seg_7047" s="T581">iʔbə-bie-m</ta>
            <ta e="T583" id="Seg_7048" s="T582">kunol-luʔ-pie-m</ta>
            <ta e="T584" id="Seg_7049" s="T583">uʔ-pia-m</ta>
            <ta e="T586" id="Seg_7050" s="T585">ku-m</ta>
            <ta e="T587" id="Seg_7051" s="T586">ej</ta>
            <ta e="T588" id="Seg_7052" s="T587">ĭzem-nie</ta>
            <ta e="T589" id="Seg_7053" s="T588">a</ta>
            <ta e="T590" id="Seg_7054" s="T589">dĭgəttə</ta>
            <ta e="T591" id="Seg_7055" s="T590">ulu-m</ta>
            <ta e="T592" id="Seg_7056" s="T591">ĭzem-nie</ta>
            <ta e="T593" id="Seg_7057" s="T592">măn</ta>
            <ta e="T594" id="Seg_7058" s="T593">tože</ta>
            <ta e="T595" id="Seg_7059" s="T594">kĭnz-le-m</ta>
            <ta e="T596" id="Seg_7060" s="T595">trʼapka-ʔi-nə</ta>
            <ta e="T597" id="Seg_7061" s="T596">ulu-ndə</ta>
            <ta e="T598" id="Seg_7062" s="T597">sar-li-m</ta>
            <ta e="T599" id="Seg_7063" s="T598">iʔbə-le-m</ta>
            <ta e="T600" id="Seg_7064" s="T599">dĭ</ta>
            <ta e="T602" id="Seg_7065" s="T601">ko-luʔ-lə-j</ta>
            <ta e="T603" id="Seg_7066" s="T602">dĭ</ta>
            <ta e="T604" id="Seg_7067" s="T603">trʼapka</ta>
            <ta e="T605" id="Seg_7068" s="T604">ulu-m</ta>
            <ta e="T606" id="Seg_7069" s="T605">ej</ta>
            <ta e="T607" id="Seg_7070" s="T606">ĭzem-nie</ta>
            <ta e="T609" id="Seg_7071" s="T608">onʼiʔ</ta>
            <ta e="T610" id="Seg_7072" s="T609">onʼiʔ</ta>
            <ta e="T611" id="Seg_7073" s="T610">raz</ta>
            <ta e="T612" id="Seg_7074" s="T611">miʔ</ta>
            <ta e="T613" id="Seg_7075" s="T612">ipek</ta>
            <ta e="T614" id="Seg_7076" s="T613">toʔ-nar-bi-baʔ</ta>
            <ta e="T615" id="Seg_7077" s="T614">mašina-ʔi-zi</ta>
            <ta e="T616" id="Seg_7078" s="T615">tĭn</ta>
            <ta e="T617" id="Seg_7079" s="T616">dagaj</ta>
            <ta e="T618" id="Seg_7080" s="T617">i-bi</ta>
            <ta e="T619" id="Seg_7081" s="T618">snăpɨ-ʔi</ta>
            <ta e="T621" id="Seg_7082" s="T620">dʼagar-zittə</ta>
            <ta e="T622" id="Seg_7083" s="T621">a</ta>
            <ta e="T623" id="Seg_7084" s="T622">dĭ</ta>
            <ta e="T625" id="Seg_7085" s="T624">kăde=də</ta>
            <ta e="T626" id="Seg_7086" s="T625">uda-bə</ta>
            <ta e="T627" id="Seg_7087" s="T626">bar</ta>
            <ta e="T628" id="Seg_7088" s="T627">băʔ-pi</ta>
            <ta e="T629" id="Seg_7089" s="T628">măn</ta>
            <ta e="T630" id="Seg_7090" s="T629">măn-də-m</ta>
            <ta e="T631" id="Seg_7091" s="T630">kĭnz-eʔ</ta>
            <ta e="T632" id="Seg_7092" s="T631">dĭ</ta>
            <ta e="T633" id="Seg_7093" s="T632">kĭnzə-bi</ta>
            <ta e="T634" id="Seg_7094" s="T633">i</ta>
            <ta e="T635" id="Seg_7095" s="T634">sar-bi</ta>
            <ta e="T636" id="Seg_7096" s="T635">dĭgəttə</ta>
            <ta e="T637" id="Seg_7097" s="T636">ej</ta>
            <ta e="T638" id="Seg_7098" s="T637">ĭzem-bi</ta>
            <ta e="T639" id="Seg_7099" s="T638">uda-t</ta>
            <ta e="T641" id="Seg_7100" s="T640">miʔ</ta>
            <ta e="T642" id="Seg_7101" s="T641">nago-bi-ʔi</ta>
            <ta e="T643" id="Seg_7102" s="T642">doktăr-əʔi</ta>
            <ta e="T644" id="Seg_7103" s="T643">miʔ</ta>
            <ta e="T646" id="Seg_7104" s="T645">no-zi</ta>
            <ta e="T647" id="Seg_7105" s="T646">bar</ta>
            <ta e="T648" id="Seg_7106" s="T647">dʼazir-bi-baʔ</ta>
            <ta e="T649" id="Seg_7107" s="T648">mĭnzer-leʔbə</ta>
            <ta e="T650" id="Seg_7108" s="T649">dĭgəttə</ta>
            <ta e="T651" id="Seg_7109" s="T650">bĭt-leʔbə</ta>
            <ta e="T652" id="Seg_7110" s="T651">onʼiʔ</ta>
            <ta e="T653" id="Seg_7111" s="T652">no</ta>
            <ta e="T654" id="Seg_7112" s="T653">i-ge</ta>
            <ta e="T655" id="Seg_7113" s="T654">putʼəga</ta>
            <ta e="T656" id="Seg_7114" s="T655">žaludkə</ta>
            <ta e="T657" id="Seg_7115" s="T656">ĭzem-nie</ta>
            <ta e="T658" id="Seg_7116" s="T657">dĭ-m</ta>
            <ta e="T659" id="Seg_7117" s="T658">mĭnzer-lie-l</ta>
            <ta e="T660" id="Seg_7118" s="T659">bĭt-lie-l</ta>
            <ta e="T661" id="Seg_7119" s="T660">dĭgəttə</ta>
            <ta e="T662" id="Seg_7120" s="T661">amor-la-l</ta>
            <ta e="T663" id="Seg_7121" s="T662">a</ta>
            <ta e="T664" id="Seg_7122" s="T663">onʼiʔ</ta>
            <ta e="T667" id="Seg_7123" s="T666">noʔ</ta>
            <ta e="T668" id="Seg_7124" s="T667">i-ge</ta>
            <ta e="T669" id="Seg_7125" s="T668">filitʼagɨ</ta>
            <ta e="T670" id="Seg_7126" s="T669">numəj-lie-ʔi</ta>
            <ta e="T671" id="Seg_7127" s="T670">dĭ-m</ta>
            <ta e="T672" id="Seg_7128" s="T671">el-li-l</ta>
            <ta e="T673" id="Seg_7129" s="T672">ara-nə</ta>
            <ta e="T674" id="Seg_7130" s="T673">dĭ</ta>
            <ta e="T675" id="Seg_7131" s="T674">nu-gə-j</ta>
            <ta e="T676" id="Seg_7132" s="T675">bʼeʔ</ta>
            <ta e="T677" id="Seg_7133" s="T676">šide</ta>
            <ta e="T678" id="Seg_7134" s="T677">dʼala</ta>
            <ta e="T679" id="Seg_7135" s="T678">dĭgəttə</ta>
            <ta e="T680" id="Seg_7136" s="T679">bĭt-le-l</ta>
            <ta e="T681" id="Seg_7137" s="T680">sĭj-də</ta>
            <ta e="T682" id="Seg_7138" s="T681">jakšə</ta>
            <ta e="T683" id="Seg_7139" s="T682">mo-la-l</ta>
            <ta e="T685" id="Seg_7140" s="T684">tăn</ta>
            <ta e="T686" id="Seg_7141" s="T685">miʔ</ta>
            <ta e="T687" id="Seg_7142" s="T686">nu-zaŋ-dən</ta>
            <ta e="T688" id="Seg_7143" s="T687">maslenka-gən</ta>
            <ta e="T689" id="Seg_7144" s="T688">bar</ta>
            <ta e="T690" id="Seg_7145" s="T689">ine-t-si</ta>
            <ta e="T691" id="Seg_7146" s="T690">tuno-laʔbə-ʔjə</ta>
            <ta e="T692" id="Seg_7147" s="T691">šindi-n</ta>
            <ta e="T693" id="Seg_7148" s="T692">ine-t</ta>
            <ta e="T694" id="Seg_7149" s="T693">büžüj</ta>
            <ta e="T695" id="Seg_7150" s="T694">šo-lə-j</ta>
            <ta e="T696" id="Seg_7151" s="T695">a</ta>
            <ta e="T697" id="Seg_7152" s="T696">šindi-n</ta>
            <ta e="T698" id="Seg_7153" s="T697">ine-t</ta>
            <ta e="T699" id="Seg_7154" s="T698">ma-lə-j</ta>
            <ta e="T700" id="Seg_7155" s="T699">dĭ</ta>
            <ta e="T701" id="Seg_7156" s="T700">šindi-n</ta>
            <ta e="T702" id="Seg_7157" s="T701">ma-lə-j</ta>
            <ta e="T703" id="Seg_7158" s="T702">dĭ</ta>
            <ta e="T706" id="Seg_7159" s="T705">ara</ta>
            <ta e="T707" id="Seg_7160" s="T706">nuldə-lə-j</ta>
            <ta e="T709" id="Seg_7161" s="T708">dĭgəttə</ta>
            <ta e="T710" id="Seg_7162" s="T709">baška</ta>
            <ta e="T711" id="Seg_7163" s="T710">ine</ta>
            <ta e="T712" id="Seg_7164" s="T711">det-lə-ʔi</ta>
            <ta e="T713" id="Seg_7165" s="T712">a</ta>
            <ta e="T714" id="Seg_7166" s="T713">dĭ</ta>
            <ta e="T715" id="Seg_7167" s="T714">dö</ta>
            <ta e="T716" id="Seg_7168" s="T715">ine</ta>
            <ta e="T717" id="Seg_7169" s="T716">ugandə</ta>
            <ta e="T719" id="Seg_7170" s="T718">măn-lia-ʔi</ta>
            <ta e="T720" id="Seg_7171" s="T719">dăre</ta>
            <ta e="T721" id="Seg_7172" s="T720">dĭgəttə</ta>
            <ta e="T722" id="Seg_7173" s="T721">dĭ-m</ta>
            <ta e="T723" id="Seg_7174" s="T722">tože</ta>
            <ta e="T724" id="Seg_7175" s="T723">maː-luʔ-ləj</ta>
            <ta e="T725" id="Seg_7176" s="T724">döbər</ta>
            <ta e="T728" id="Seg_7177" s="T727">nuʔmə-luʔ-lə-j</ta>
            <ta e="T729" id="Seg_7178" s="T728">a</ta>
            <ta e="T730" id="Seg_7179" s="T729">dĭ</ta>
            <ta e="T731" id="Seg_7180" s="T730">ine</ta>
            <ta e="T732" id="Seg_7181" s="T731">maː-luʔ-ləj</ta>
            <ta e="T734" id="Seg_7182" s="T733">dĭgəttə</ta>
            <ta e="T735" id="Seg_7183" s="T734">dĭ</ta>
            <ta e="T736" id="Seg_7184" s="T735">ine-m</ta>
            <ta e="T739" id="Seg_7185" s="T738">i-lie</ta>
            <ta e="T740" id="Seg_7186" s="T739">i</ta>
            <ta e="T741" id="Seg_7187" s="T740">mĭŋ-ge</ta>
            <ta e="T742" id="Seg_7188" s="T741">mĭn-ge</ta>
            <ta e="T743" id="Seg_7189" s="T742">koldʼəŋ</ta>
            <ta e="T744" id="Seg_7190" s="T743">štobɨ</ta>
            <ta e="T745" id="Seg_7191" s="T744">ej</ta>
            <ta e="T746" id="Seg_7192" s="T745">nu-bi</ta>
            <ta e="T748" id="Seg_7193" s="T747">dĭgəttə</ta>
            <ta e="T749" id="Seg_7194" s="T748">ara</ta>
            <ta e="T750" id="Seg_7195" s="T749">iʔgö</ta>
            <ta e="T751" id="Seg_7196" s="T750">bĭt-luʔ-lə-ʔi</ta>
            <ta e="T752" id="Seg_7197" s="T751">i</ta>
            <ta e="T753" id="Seg_7198" s="T752">bar</ta>
            <ta e="T754" id="Seg_7199" s="T753">saʔmə-luʔ-lə-ʔjə</ta>
            <ta e="T756" id="Seg_7200" s="T755">dĭgəttə</ta>
            <ta e="T757" id="Seg_7201" s="T756">tibi-zeŋ</ta>
            <ta e="T758" id="Seg_7202" s="T757">pa-ʔi</ta>
            <ta e="T759" id="Seg_7203" s="T758">i-lə-ʔi</ta>
            <ta e="T760" id="Seg_7204" s="T759">uda-ndə</ta>
            <ta e="T761" id="Seg_7205" s="T760">i</ta>
            <ta e="T762" id="Seg_7206" s="T761">dĭgəttə</ta>
            <ta e="T769" id="Seg_7207" s="T768">dĭgəttə</ta>
            <ta e="T770" id="Seg_7208" s="T769">onʼiʔ</ta>
            <ta e="T771" id="Seg_7209" s="T770">onʼiʔ-tə</ta>
            <ta e="T772" id="Seg_7210" s="T771">bar</ta>
            <ta e="T773" id="Seg_7211" s="T772">nuʔ</ta>
            <ta e="T775" id="Seg_7212" s="T774">nʼot-laʔbə</ta>
            <ta e="T776" id="Seg_7213" s="T775">dĭgəttə</ta>
            <ta e="T777" id="Seg_7214" s="T776">onʼiʔ</ta>
            <ta e="T778" id="Seg_7215" s="T777">kuštü</ta>
            <ta e="T782" id="Seg_7216" s="T781">dĭ-m</ta>
            <ta e="T783" id="Seg_7217" s="T782">bar</ta>
            <ta e="T784" id="Seg_7218" s="T783">uju-ndə</ta>
            <ta e="T785" id="Seg_7219" s="T784">nuldə-lu-lə-j</ta>
            <ta e="T786" id="Seg_7220" s="T785">dĭ</ta>
            <ta e="T787" id="Seg_7221" s="T786">bar</ta>
            <ta e="T788" id="Seg_7222" s="T787">dĭgəttə</ta>
            <ta e="T789" id="Seg_7223" s="T788">ara</ta>
            <ta e="T790" id="Seg_7224" s="T789">nu-luʔ-lə-j</ta>
            <ta e="T791" id="Seg_7225" s="T790">dĭgəttə</ta>
            <ta e="T792" id="Seg_7226" s="T791">bĭt-lie-ʔi</ta>
            <ta e="T794" id="Seg_7227" s="T793">dĭgəttə</ta>
            <ta e="T795" id="Seg_7228" s="T794">šide-göʔ</ta>
            <ta e="T796" id="Seg_7229" s="T795">nu-zaŋ</ta>
            <ta e="T798" id="Seg_7230" s="T797">šide-göʔ</ta>
            <ta e="T799" id="Seg_7231" s="T798">nu-zaŋ</ta>
            <ta e="T800" id="Seg_7232" s="T799">i-lə-ʔi</ta>
            <ta e="T801" id="Seg_7233" s="T800">onʼiʔ</ta>
            <ta e="T802" id="Seg_7234" s="T801">onʼiʔ-tə</ta>
            <ta e="T803" id="Seg_7235" s="T802">dĭgəttə</ta>
            <ta e="T804" id="Seg_7236" s="T803">kajət</ta>
            <ta e="T805" id="Seg_7237" s="T804">kuštü</ta>
            <ta e="T806" id="Seg_7238" s="T805">dö-m</ta>
            <ta e="T807" id="Seg_7239" s="T806">parəʔ-lu-j</ta>
            <ta e="T808" id="Seg_7240" s="T807">dʼü-nə</ta>
            <ta e="T809" id="Seg_7241" s="T808">dĭgəttə</ta>
            <ta e="T810" id="Seg_7242" s="T809">dĭ</ta>
            <ta e="T811" id="Seg_7243" s="T810">ara</ta>
            <ta e="T812" id="Seg_7244" s="T811">nuld-lia</ta>
            <ta e="T813" id="Seg_7245" s="T812">dĭʔ-nə</ta>
            <ta e="T814" id="Seg_7246" s="T813">bĭt-leʔbə-ʔjə</ta>
            <ta e="T816" id="Seg_7247" s="T815">dĭgəttə</ta>
            <ta e="T817" id="Seg_7248" s="T816">dʼü-nə</ta>
            <ta e="T818" id="Seg_7249" s="T817">barəʔ-lu-j</ta>
            <ta e="T819" id="Seg_7250" s="T818">uda-bə</ta>
            <ta e="T820" id="Seg_7251" s="T819">bar</ta>
            <ta e="T821" id="Seg_7252" s="T820">büld-lə-j</ta>
            <ta e="T824" id="Seg_7253" s="T823">vărota-ʔi</ta>
            <ta e="T825" id="Seg_7254" s="T824">nu-laʔbə-ʔjə</ta>
            <ta e="T826" id="Seg_7255" s="T825">kan-a-ʔ</ta>
            <ta e="T827" id="Seg_7256" s="T826">girgit</ta>
            <ta e="T828" id="Seg_7257" s="T827">aʔtʼi-nə</ta>
            <ta e="T829" id="Seg_7258" s="T828">ka-la-l</ta>
            <ta e="T830" id="Seg_7259" s="T829">pravăj</ta>
            <ta e="T831" id="Seg_7260" s="T830">aʔtʼi</ta>
            <ta e="T832" id="Seg_7261" s="T831">bar</ta>
            <ta e="T833" id="Seg_7262" s="T832">ej</ta>
            <ta e="T834" id="Seg_7263" s="T833">jakše</ta>
            <ta e="T835" id="Seg_7264" s="T834">bar</ta>
            <ta e="T836" id="Seg_7265" s="T835">bălotă-ʔi</ta>
            <ta e="T837" id="Seg_7266" s="T836">pa-ʔi</ta>
            <ta e="T838" id="Seg_7267" s="T837">nu-ga-ʔi</ta>
            <ta e="T839" id="Seg_7268" s="T838">i</ta>
            <ta e="T840" id="Seg_7269" s="T839">bar</ta>
            <ta e="T841" id="Seg_7270" s="T840">groza-ʔi</ta>
            <ta e="T842" id="Seg_7271" s="T841">beržə</ta>
            <ta e="T843" id="Seg_7272" s="T842">bar</ta>
            <ta e="T844" id="Seg_7273" s="T843">baza-j</ta>
            <ta e="T845" id="Seg_7274" s="T844">kălʼučka-ʔi</ta>
            <ta e="T846" id="Seg_7275" s="T845">bar</ta>
            <ta e="T847" id="Seg_7276" s="T846">mündüʔ-leʔbə-ʔjə</ta>
            <ta e="T848" id="Seg_7277" s="T847">a</ta>
            <ta e="T849" id="Seg_7278" s="T848">dĭbər</ta>
            <ta e="T850" id="Seg_7279" s="T849">kuŋgə-ŋ</ta>
            <ta e="T851" id="Seg_7280" s="T850">ka-la-l</ta>
            <ta e="T852" id="Seg_7281" s="T851">dĭn</ta>
            <ta e="T853" id="Seg_7282" s="T852">bar</ta>
            <ta e="T854" id="Seg_7283" s="T853">jakše</ta>
            <ta e="T855" id="Seg_7284" s="T854">pa-ʔi</ta>
            <ta e="T856" id="Seg_7285" s="T855">naga</ta>
            <ta e="T857" id="Seg_7286" s="T856">ĭmbi=də</ta>
            <ta e="T858" id="Seg_7287" s="T857">naga</ta>
            <ta e="T859" id="Seg_7288" s="T858">ugandə</ta>
            <ta e="T860" id="Seg_7289" s="T859">kuvas</ta>
            <ta e="T861" id="Seg_7290" s="T860">noʔ</ta>
            <ta e="T862" id="Seg_7291" s="T861">kuvas</ta>
            <ta e="T864" id="Seg_7292" s="T863">svʼetog-əʔi</ta>
            <ta e="T865" id="Seg_7293" s="T864">sĭre</ta>
            <ta e="T866" id="Seg_7294" s="T865">kömə</ta>
            <ta e="T868" id="Seg_7295" s="T867">özer-leʔbə</ta>
            <ta e="T869" id="Seg_7296" s="T868">jablăkă-ʔi</ta>
            <ta e="T870" id="Seg_7297" s="T869">kuja</ta>
            <ta e="T871" id="Seg_7298" s="T870">mĭnd-laʔbə</ta>
            <ta e="T872" id="Seg_7299" s="T871">ugandə</ta>
            <ta e="T873" id="Seg_7300" s="T872">ejü</ta>
            <ta e="T874" id="Seg_7301" s="T873">xotʼ</ta>
            <ta e="T875" id="Seg_7302" s="T874">iʔb-eʔ</ta>
            <ta e="T876" id="Seg_7303" s="T875">da</ta>
            <ta e="T877" id="Seg_7304" s="T876">kunol-a-ʔ</ta>
            <ta e="T878" id="Seg_7305" s="T877">jakše</ta>
            <ta e="T879" id="Seg_7306" s="T878">dĭn</ta>
            <ta e="T880" id="Seg_7307" s="T879">a</ta>
            <ta e="T881" id="Seg_7308" s="T880">lʼevăj</ta>
            <ta e="T882" id="Seg_7309" s="T881">uda-ndə</ta>
            <ta e="T883" id="Seg_7310" s="T882">kandə-ga</ta>
            <ta e="T884" id="Seg_7311" s="T883">ugandə</ta>
            <ta e="T885" id="Seg_7312" s="T884">aktʼi</ta>
            <ta e="T886" id="Seg_7313" s="T885">urgo</ta>
            <ta e="T887" id="Seg_7314" s="T886">il</ta>
            <ta e="T888" id="Seg_7315" s="T887">iʔgö</ta>
            <ta e="T889" id="Seg_7316" s="T888">bar</ta>
            <ta e="T890" id="Seg_7317" s="T889">ara</ta>
            <ta e="T891" id="Seg_7318" s="T890">bĭt-lie-ʔi</ta>
            <ta e="T892" id="Seg_7319" s="T891">bar</ta>
            <ta e="T893" id="Seg_7320" s="T892">ĭmbi</ta>
            <ta e="T894" id="Seg_7321" s="T893">am-nia-ʔi</ta>
            <ta e="T895" id="Seg_7322" s="T894">bar</ta>
            <ta e="T896" id="Seg_7323" s="T895">suʔmi-leʔbə-ʔjə</ta>
            <ta e="T897" id="Seg_7324" s="T896">bar</ta>
            <ta e="T898" id="Seg_7325" s="T897">nüjnə</ta>
            <ta e="T899" id="Seg_7326" s="T898">nüj-leʔbə-ʔjə</ta>
            <ta e="T900" id="Seg_7327" s="T899">a</ta>
            <ta e="T902" id="Seg_7328" s="T901">dĭbər</ta>
            <ta e="T903" id="Seg_7329" s="T902">kuŋgə</ta>
            <ta e="T905" id="Seg_7330" s="T904">ka-la-l</ta>
            <ta e="T906" id="Seg_7331" s="T905">dĭn</ta>
            <ta e="T907" id="Seg_7332" s="T906">bar</ta>
            <ta e="T908" id="Seg_7333" s="T907">šü</ta>
            <ta e="T909" id="Seg_7334" s="T908">bar</ta>
            <ta e="T910" id="Seg_7335" s="T909">pi-ʔi</ta>
            <ta e="T911" id="Seg_7336" s="T910">bar</ta>
            <ta e="T912" id="Seg_7337" s="T911">groza-ʔi</ta>
            <ta e="T913" id="Seg_7338" s="T912">šü</ta>
            <ta e="T914" id="Seg_7339" s="T913">il-ə-m</ta>
            <ta e="T915" id="Seg_7340" s="T914">bar</ta>
            <ta e="T916" id="Seg_7341" s="T915">nend-laʔbə</ta>
            <ta e="T917" id="Seg_7342" s="T916">tüj</ta>
            <ta e="T918" id="Seg_7343" s="T917">bar</ta>
            <ta e="T920" id="Seg_7344" s="T919">šiʔ</ta>
            <ta e="T921" id="Seg_7345" s="T920">kaŋ-gaʔ</ta>
            <ta e="T922" id="Seg_7346" s="T921">a</ta>
            <ta e="T923" id="Seg_7347" s="T922">măn</ta>
            <ta e="T924" id="Seg_7348" s="T923">üjilger-la-m</ta>
            <ta e="T925" id="Seg_7349" s="T924">nada</ta>
            <ta e="T926" id="Seg_7350" s="T925">măna</ta>
            <ta e="T927" id="Seg_7351" s="T926">ipek</ta>
            <ta e="T928" id="Seg_7352" s="T927">nuldə-sʼtə</ta>
            <ta e="T929" id="Seg_7353" s="T928">pür-zittə</ta>
            <ta e="T930" id="Seg_7354" s="T929">ipek</ta>
            <ta e="T931" id="Seg_7355" s="T930">nada</ta>
            <ta e="T932" id="Seg_7356" s="T931">ato</ta>
            <ta e="T933" id="Seg_7357" s="T932">am-zittə</ta>
            <ta e="T934" id="Seg_7358" s="T933">naga</ta>
            <ta e="T935" id="Seg_7359" s="T934">ipek</ta>
            <ta e="T936" id="Seg_7360" s="T935">šiʔnʼileʔ</ta>
            <ta e="T937" id="Seg_7361" s="T936">mĭ-le-m</ta>
            <ta e="T938" id="Seg_7362" s="T937">amor-bi-laʔ</ta>
            <ta e="T940" id="Seg_7363" s="T939">i-ʔ</ta>
            <ta e="T941" id="Seg_7364" s="T940">kan-a-ʔ</ta>
            <ta e="T942" id="Seg_7365" s="T941">tibi-nə</ta>
            <ta e="T943" id="Seg_7366" s="T942">amno-ʔ</ta>
            <ta e="T944" id="Seg_7367" s="T943">unnʼa-nə</ta>
            <ta e="T945" id="Seg_7368" s="T944">jakše</ta>
            <ta e="T946" id="Seg_7369" s="T945">amno-zittə</ta>
            <ta e="T947" id="Seg_7370" s="T946">giber=nʼibudʼ</ta>
            <ta e="T948" id="Seg_7371" s="T947">ka-la-m</ta>
            <ta e="T949" id="Seg_7372" s="T948">šindi-nə=də</ta>
            <ta e="T950" id="Seg_7373" s="T949">ej</ta>
            <ta e="T951" id="Seg_7374" s="T950">surar-bia-m</ta>
            <ta e="T953" id="Seg_7375" s="T952">bü</ta>
            <ta e="T954" id="Seg_7376" s="T953">mʼaŋ-naʔbə</ta>
            <ta e="T955" id="Seg_7377" s="T954">dĭ</ta>
            <ta e="T956" id="Seg_7378" s="T955">urgo</ta>
            <ta e="T957" id="Seg_7379" s="T956">Ilʼbinʼ</ta>
            <ta e="T958" id="Seg_7380" s="T957">üdʼüge</ta>
            <ta e="T959" id="Seg_7381" s="T958">Ilʼbinʼ</ta>
            <ta e="T960" id="Seg_7382" s="T959">a</ta>
            <ta e="T961" id="Seg_7383" s="T960">gijen</ta>
            <ta e="T962" id="Seg_7384" s="T961">bü</ta>
            <ta e="T963" id="Seg_7385" s="T962">i-leʔbə</ta>
            <ta e="T964" id="Seg_7386" s="T963">dĭn</ta>
            <ta e="T965" id="Seg_7387" s="T964">bar</ta>
            <ta e="T966" id="Seg_7388" s="T965">numəj-lie-ʔi</ta>
            <ta e="T967" id="Seg_7389" s="T966">bar</ta>
            <ta e="T970" id="Seg_7390" s="T969">onʼiʔ</ta>
            <ta e="T971" id="Seg_7391" s="T970">kö</ta>
            <ta e="T972" id="Seg_7392" s="T971">bar</ta>
            <ta e="T973" id="Seg_7393" s="T972">miʔ</ta>
            <ta e="T974" id="Seg_7394" s="T973">aš</ta>
            <ta e="T975" id="Seg_7395" s="T974">pĭde-bi-beʔ</ta>
            <ta e="T976" id="Seg_7396" s="T975">dĭ-zeŋ</ta>
            <ta e="T977" id="Seg_7397" s="T976">bar</ta>
            <ta e="T978" id="Seg_7398" s="T977">üge</ta>
            <ta e="T979" id="Seg_7399" s="T978">amno-laʔbə-ʔjə</ta>
            <ta e="T980" id="Seg_7400" s="T979">amno-laʔbə-ʔjə</ta>
            <ta e="T981" id="Seg_7401" s="T980">dʼăbaktər-laʔbə-ʔjə</ta>
            <ta e="T982" id="Seg_7402" s="T981">kaknar-laʔbə-ʔjə</ta>
            <ta e="T983" id="Seg_7403" s="T982">kan-žə-baʔ</ta>
            <ta e="T984" id="Seg_7404" s="T983">pĭde-sʼtə</ta>
            <ta e="T985" id="Seg_7405" s="T984">ato</ta>
            <ta e="T986" id="Seg_7406" s="T985">šišəge</ta>
            <ta e="T987" id="Seg_7407" s="T986">mo-lə-j</ta>
            <ta e="T990" id="Seg_7408" s="T989">miʔnʼibeʔ</ta>
            <ta e="T991" id="Seg_7409" s="T990">nada</ta>
            <ta e="T992" id="Seg_7410" s="T991">pĭde-sʼtə</ta>
            <ta e="T993" id="Seg_7411" s="T992">tănan</ta>
            <ta e="T994" id="Seg_7412" s="T993">tănan</ta>
            <ta e="T995" id="Seg_7413" s="T994">üge</ta>
            <ta e="T996" id="Seg_7414" s="T995">iʔgö</ta>
            <ta e="T997" id="Seg_7415" s="T996">kereʔ</ta>
            <ta e="T998" id="Seg_7416" s="T997">dĭgəttə</ta>
            <ta e="T999" id="Seg_7417" s="T998">uda-zaŋ-də</ta>
            <ta e="T1000" id="Seg_7418" s="T999">bar</ta>
            <ta e="T1001" id="Seg_7419" s="T1000">kănd-laʔbə-ʔjə</ta>
            <ta e="T1002" id="Seg_7420" s="T1001">sĭre</ta>
            <ta e="T1003" id="Seg_7421" s="T1002">saʔmə-luʔ-lə-j</ta>
            <ta e="T1004" id="Seg_7422" s="T1003">pa</ta>
            <ta e="T1005" id="Seg_7423" s="T1004">bazo</ta>
            <ta e="T1006" id="Seg_7424" s="T1005">miʔ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_7425" s="T0">măn</ta>
            <ta e="T2" id="Seg_7426" s="T1">teinen</ta>
            <ta e="T3" id="Seg_7427" s="T2">šiʔnʼileʔ</ta>
            <ta e="T5" id="Seg_7428" s="T4">kudonz-bi-m</ta>
            <ta e="T6" id="Seg_7429" s="T5">šo-bi-lAʔ</ta>
            <ta e="T7" id="Seg_7430" s="T6">tʼăbaktər-zittə</ta>
            <ta e="T8" id="Seg_7431" s="T7">a</ta>
            <ta e="T9" id="Seg_7432" s="T8">ĭmbi=də</ta>
            <ta e="T10" id="Seg_7433" s="T9">ej</ta>
            <ta e="T11" id="Seg_7434" s="T10">nʼilgö-laʔbə-lAʔ</ta>
            <ta e="T12" id="Seg_7435" s="T11">ej</ta>
            <ta e="T14" id="Seg_7436" s="T13">ej</ta>
            <ta e="T15" id="Seg_7437" s="T14">măn-ntə-liA-lAʔ</ta>
            <ta e="T17" id="Seg_7438" s="T16">teinen</ta>
            <ta e="T18" id="Seg_7439" s="T17">tʼăbaktər-bi</ta>
            <ta e="T19" id="Seg_7440" s="T18">jakšə</ta>
            <ta e="T21" id="Seg_7441" s="T20">tʼăbaktər-bi</ta>
            <ta e="T22" id="Seg_7442" s="T21">măn</ta>
            <ta e="T23" id="Seg_7443" s="T22">nʼilgö-bi-m</ta>
            <ta e="T24" id="Seg_7444" s="T23">da</ta>
            <ta e="T25" id="Seg_7445" s="T24">ej</ta>
            <ta e="T26" id="Seg_7446" s="T25">bar</ta>
            <ta e="T28" id="Seg_7447" s="T27">üge</ta>
            <ta e="T29" id="Seg_7448" s="T28">togonər-liA-m</ta>
            <ta e="T30" id="Seg_7449" s="T29">nʼilgö-zittə</ta>
            <ta e="T32" id="Seg_7450" s="T31">kădaʔ=də</ta>
            <ta e="T33" id="Seg_7451" s="T32">ej</ta>
            <ta e="T34" id="Seg_7452" s="T33">mo-liA-m</ta>
            <ta e="T35" id="Seg_7453" s="T34">šində=də</ta>
            <ta e="T36" id="Seg_7454" s="T35">tʼăbaktər-laʔbə</ta>
            <ta e="T37" id="Seg_7455" s="T36">sĭri-ziʔ</ta>
            <ta e="T38" id="Seg_7456" s="T37">tăn</ta>
            <ta e="T39" id="Seg_7457" s="T38">iʔbə-ʔ</ta>
            <ta e="T40" id="Seg_7458" s="T39">tʼo-Tə</ta>
            <ta e="T41" id="Seg_7459" s="T40">iʔbö-ʔ</ta>
            <ta e="T42" id="Seg_7460" s="T41">e-ʔ</ta>
            <ta e="T43" id="Seg_7461" s="T42">tʼăbaktər-ə-ʔ</ta>
            <ta e="T44" id="Seg_7462" s="T43">e-ʔ</ta>
            <ta e="T45" id="Seg_7463" s="T44">kirgaːr-ə-ʔ</ta>
            <ta e="T46" id="Seg_7464" s="T45">a</ta>
            <ta e="T47" id="Seg_7465" s="T46">ĭmbi</ta>
            <ta e="T48" id="Seg_7466" s="T47">măn</ta>
            <ta e="T49" id="Seg_7467" s="T48">ej</ta>
            <ta e="T50" id="Seg_7468" s="T49">tʼăbaktər-lV-m</ta>
            <ta e="T51" id="Seg_7469" s="T50">da</ta>
            <ta e="T52" id="Seg_7470" s="T51">ej</ta>
            <ta e="T53" id="Seg_7471" s="T52">kirgaːr-lV-m</ta>
            <ta e="T54" id="Seg_7472" s="T53">da</ta>
            <ta e="T56" id="Seg_7473" s="T55">kondʼo</ta>
            <ta e="T57" id="Seg_7474" s="T56">amno-lV-l</ta>
            <ta e="T58" id="Seg_7475" s="T57">ej</ta>
            <ta e="T59" id="Seg_7476" s="T58">kü-lV-l</ta>
            <ta e="T60" id="Seg_7477" s="T59">jakšə</ta>
            <ta e="T61" id="Seg_7478" s="T60">mo-lV-j</ta>
            <ta e="T62" id="Seg_7479" s="T61">tănan</ta>
            <ta e="T63" id="Seg_7480" s="T62">iʔbö-ʔ</ta>
            <ta e="T64" id="Seg_7481" s="T63">iʔbö-ʔ</ta>
            <ta e="T65" id="Seg_7482" s="T64">dĭgəttə</ta>
            <ta e="T66" id="Seg_7483" s="T65">šində=də</ta>
            <ta e="T67" id="Seg_7484" s="T66">bar</ta>
            <ta e="T68" id="Seg_7485" s="T67">nʼergö-luʔbdə-bi</ta>
            <ta e="T69" id="Seg_7486" s="T68">dĭ-m</ta>
            <ta e="T70" id="Seg_7487" s="T69">i</ta>
            <ta e="T71" id="Seg_7488" s="T70">üžüm-bi</ta>
            <ta e="T72" id="Seg_7489" s="T71">dĭ-m</ta>
            <ta e="T73" id="Seg_7490" s="T72">dĭgəttə</ta>
            <ta e="T74" id="Seg_7491" s="T73">šišəge</ta>
            <ta e="T75" id="Seg_7492" s="T74">dĭ-m</ta>
            <ta e="T76" id="Seg_7493" s="T75">ugaːndə</ta>
            <ta e="T77" id="Seg_7494" s="T76">tăŋ</ta>
            <ta e="T78" id="Seg_7495" s="T77">kăn-bi</ta>
            <ta e="T79" id="Seg_7496" s="T78">kăn-bi</ta>
            <ta e="T80" id="Seg_7497" s="T79">a</ta>
            <ta e="T81" id="Seg_7498" s="T80">dĭgəttə</ta>
            <ta e="T82" id="Seg_7499" s="T81">ejü</ta>
            <ta e="T83" id="Seg_7500" s="T82">mo-laːm-bi</ta>
            <ta e="T84" id="Seg_7501" s="T83">kuja</ta>
            <ta e="T85" id="Seg_7502" s="T84">uʔbdə-bi</ta>
            <ta e="T86" id="Seg_7503" s="T85">nʼuʔdə</ta>
            <ta e="T87" id="Seg_7504" s="T86">uʔbdə-bi</ta>
            <ta e="T88" id="Seg_7505" s="T87">sĭri</ta>
            <ta e="T89" id="Seg_7506" s="T88">bar</ta>
            <ta e="T90" id="Seg_7507" s="T89">nereʔ-luʔbdə-bi</ta>
            <ta e="T91" id="Seg_7508" s="T90">i</ta>
            <ta e="T92" id="Seg_7509" s="T91">dĭgəttə</ta>
            <ta e="T93" id="Seg_7510" s="T92">bar</ta>
            <ta e="T97" id="Seg_7511" s="T96">dĭgəttə</ta>
            <ta e="T98" id="Seg_7512" s="T97">bar</ta>
            <ta e="T101" id="Seg_7513" s="T100">bü</ta>
            <ta e="T102" id="Seg_7514" s="T101">mʼaŋ-laʔbə</ta>
            <ta e="T103" id="Seg_7515" s="T102">i</ta>
            <ta e="T104" id="Seg_7516" s="T103">sĭri</ta>
            <ta e="T105" id="Seg_7517" s="T104">kü-laːm-bi</ta>
            <ta e="T107" id="Seg_7518" s="T106">bar</ta>
            <ta e="T108" id="Seg_7519" s="T107">jezirer-luʔbdə-bi</ta>
            <ta e="T109" id="Seg_7520" s="T108">i</ta>
            <ta e="T110" id="Seg_7521" s="T109">bar</ta>
            <ta e="T111" id="Seg_7522" s="T110">ananugosʼtə</ta>
            <ta e="T112" id="Seg_7523" s="T111">i-bi</ta>
            <ta e="T113" id="Seg_7524" s="T112">uda-gəndə</ta>
            <ta e="T114" id="Seg_7525" s="T113">tüjö</ta>
            <ta e="T115" id="Seg_7526" s="T114">măn-ntə</ta>
            <ta e="T116" id="Seg_7527" s="T115">toʔbdə-nar-lV-m</ta>
            <ta e="T117" id="Seg_7528" s="T116">măn</ta>
            <ta e="T118" id="Seg_7529" s="T117">ija-m</ta>
            <ta e="T119" id="Seg_7530" s="T118">ešši</ta>
            <ta e="T120" id="Seg_7531" s="T119">det-bi</ta>
            <ta e="T121" id="Seg_7532" s="T120">nʼi</ta>
            <ta e="T122" id="Seg_7533" s="T121">det-bi</ta>
            <ta e="T123" id="Seg_7534" s="T122">tura-Kən</ta>
            <ta e="T124" id="Seg_7535" s="T123">bar</ta>
            <ta e="T125" id="Seg_7536" s="T124">ĭzem-bi</ta>
            <ta e="T126" id="Seg_7537" s="T125">kirgaːr-bi</ta>
            <ta e="T127" id="Seg_7538" s="T126">urgaja</ta>
            <ta e="T128" id="Seg_7539" s="T127">i-bi</ta>
            <ta e="T129" id="Seg_7540" s="T128">dĭ</ta>
            <ta e="T130" id="Seg_7541" s="T129">det-bi</ta>
            <ta e="T132" id="Seg_7542" s="T131">ešši</ta>
            <ta e="T133" id="Seg_7543" s="T132">dĭ</ta>
            <ta e="T134" id="Seg_7544" s="T133">bar</ta>
            <ta e="T136" id="Seg_7545" s="T135">băʔtə-bi</ta>
            <ta e="T137" id="Seg_7546" s="T136">sar-bi</ta>
            <ta e="T138" id="Seg_7547" s="T137">i</ta>
            <ta e="T139" id="Seg_7548" s="T138">bazə-bi</ta>
            <ta e="T140" id="Seg_7549" s="T139">dĭgəttə</ta>
            <ta e="T144" id="Seg_7550" s="T143">dĭgəttə</ta>
            <ta e="T145" id="Seg_7551" s="T144">ku-bi</ta>
            <ta e="T146" id="Seg_7552" s="T145">trʼapka-jəʔ</ta>
            <ta e="T147" id="Seg_7553" s="T146">kür-bi</ta>
            <ta e="T148" id="Seg_7554" s="T147">dĭ-m</ta>
            <ta e="T149" id="Seg_7555" s="T148">i</ta>
            <ta e="T150" id="Seg_7556" s="T149">hen-bi</ta>
            <ta e="T151" id="Seg_7557" s="T150">pʼeːš-Tə</ta>
            <ta e="T152" id="Seg_7558" s="T151">dĭ</ta>
            <ta e="T153" id="Seg_7559" s="T152">bar</ta>
            <ta e="T154" id="Seg_7560" s="T153">iʔbö-laʔbə</ta>
            <ta e="T155" id="Seg_7561" s="T154">a</ta>
            <ta e="T156" id="Seg_7562" s="T155">dĭ-m</ta>
            <ta e="T158" id="Seg_7563" s="T157">numəj-laʔbə-jəʔ</ta>
            <ta e="T159" id="Seg_7564" s="T158">dĭgəttə</ta>
            <ta e="T160" id="Seg_7565" s="T159">kun-bi-jəʔ</ta>
            <ta e="T161" id="Seg_7566" s="T160">abəs-Tə</ta>
            <ta e="T162" id="Seg_7567" s="T161">i</ta>
            <ta e="T163" id="Seg_7568" s="T162">numəj-laʔbə-jəʔ</ta>
            <ta e="T165" id="Seg_7569" s="T164">a</ta>
            <ta e="T166" id="Seg_7570" s="T165">măn</ta>
            <ta e="T167" id="Seg_7571" s="T166">ija-m</ta>
            <ta e="T168" id="Seg_7572" s="T167">dĭ-m</ta>
            <ta e="T169" id="Seg_7573" s="T168">det-bi</ta>
            <ta e="T170" id="Seg_7574" s="T169">măna</ta>
            <ta e="T171" id="Seg_7575" s="T170">biəʔ</ta>
            <ta e="T172" id="Seg_7576" s="T171">šide</ta>
            <ta e="T173" id="Seg_7577" s="T172">kö</ta>
            <ta e="T174" id="Seg_7578" s="T173">i-bi</ta>
            <ta e="T175" id="Seg_7579" s="T174">dĭgəttə</ta>
            <ta e="T176" id="Seg_7580" s="T175">dĭ</ta>
            <ta e="T178" id="Seg_7581" s="T177">özer-bi</ta>
            <ta e="T179" id="Seg_7582" s="T178">özer-bi</ta>
            <ta e="T180" id="Seg_7583" s="T179">biəʔ</ta>
            <ta e="T181" id="Seg_7584" s="T180">šide</ta>
            <ta e="T182" id="Seg_7585" s="T181">biəʔ</ta>
            <ta e="T183" id="Seg_7586" s="T182">šide</ta>
            <ta e="T184" id="Seg_7587" s="T183">kö</ta>
            <ta e="T185" id="Seg_7588" s="T184">i-bi</ta>
            <ta e="T186" id="Seg_7589" s="T185">ija-t</ta>
            <ta e="T187" id="Seg_7590" s="T186">kü-laːm-bi</ta>
            <ta e="T188" id="Seg_7591" s="T187">dĭ</ta>
            <ta e="T189" id="Seg_7592" s="T188">ma-bi</ta>
            <ta e="T190" id="Seg_7593" s="T189">a</ta>
            <ta e="T193" id="Seg_7594" s="T192">ija-m</ta>
            <ta e="T194" id="Seg_7595" s="T193">kut-bi-jəʔ</ta>
            <ta e="T195" id="Seg_7596" s="T194">dĭ-m</ta>
            <ta e="T196" id="Seg_7597" s="T195">kut-bi</ta>
            <ta e="T198" id="Seg_7598" s="T197">dĭ-n</ta>
            <ta e="T199" id="Seg_7599" s="T198">i</ta>
            <ta e="T200" id="Seg_7600" s="T199">dĭ</ta>
            <ta e="T201" id="Seg_7601" s="T200">bar</ta>
            <ta e="T202" id="Seg_7602" s="T201">kü-laːm-bi</ta>
            <ta e="T203" id="Seg_7603" s="T202">ine-ziʔ</ta>
            <ta e="T204" id="Seg_7604" s="T203">maʔ-gəndə</ta>
            <ta e="T205" id="Seg_7605" s="T204">det-bi-jəʔ</ta>
            <ta e="T206" id="Seg_7606" s="T205">măn</ta>
            <ta e="T207" id="Seg_7607" s="T206">aba-m</ta>
            <ta e="T208" id="Seg_7608" s="T207">dʼije-Kən</ta>
            <ta e="T209" id="Seg_7609" s="T208">kü-laːm-bi</ta>
            <ta e="T210" id="Seg_7610" s="T209">dĭgəttə</ta>
            <ta e="T211" id="Seg_7611" s="T210">sʼestra-m</ta>
            <ta e="T212" id="Seg_7612" s="T211">i</ta>
            <ta e="T213" id="Seg_7613" s="T212">măn</ta>
            <ta e="T214" id="Seg_7614" s="T213">tibi-m</ta>
            <ta e="T215" id="Seg_7615" s="T214">šide</ta>
            <ta e="T216" id="Seg_7616" s="T215">ine-gəj-t-ziʔ</ta>
            <ta e="T217" id="Seg_7617" s="T216">det-bi-jəʔ</ta>
            <ta e="T218" id="Seg_7618" s="T217">maʔ-gənʼibəj</ta>
            <ta e="T219" id="Seg_7619" s="T218">măn</ta>
            <ta e="T220" id="Seg_7620" s="T219">ugaːndə</ta>
            <ta e="T221" id="Seg_7621" s="T220">tăŋ</ta>
            <ta e="T222" id="Seg_7622" s="T221">tʼor-bi-m</ta>
            <ta e="T223" id="Seg_7623" s="T222">urgo</ta>
            <ta e="T224" id="Seg_7624" s="T223">tʼala</ta>
            <ta e="T225" id="Seg_7625" s="T224">i-bi</ta>
            <ta e="T227" id="Seg_7626" s="T226">dĭgəttə</ta>
            <ta e="T228" id="Seg_7627" s="T227">tʼo</ta>
            <ta e="T229" id="Seg_7628" s="T228">tʼo</ta>
            <ta e="T230" id="Seg_7629" s="T229">tĭl-bi-bAʔ</ta>
            <ta e="T231" id="Seg_7630" s="T230">maʔ</ta>
            <ta e="T232" id="Seg_7631" s="T231">a-bi-bAʔ</ta>
            <ta e="T233" id="Seg_7632" s="T232">krospa</ta>
            <ta e="T234" id="Seg_7633" s="T233">a-bi-bAʔ</ta>
            <ta e="T235" id="Seg_7634" s="T234">i</ta>
            <ta e="T236" id="Seg_7635" s="T235">tʼo-Tə</ta>
            <ta e="T237" id="Seg_7636" s="T236">hen-bi-bAʔ</ta>
            <ta e="T238" id="Seg_7637" s="T237">kămnə-bi-bAʔ</ta>
            <ta e="T239" id="Seg_7638" s="T238">dĭgəttə</ta>
            <ta e="T240" id="Seg_7639" s="T239">bü</ta>
            <ta e="T241" id="Seg_7640" s="T240">bĭs-bi-bAʔ</ta>
            <ta e="T242" id="Seg_7641" s="T241">ipek</ta>
            <ta e="T243" id="Seg_7642" s="T242">am-bi-bAʔ</ta>
            <ta e="T244" id="Seg_7643" s="T243">il</ta>
            <ta e="T245" id="Seg_7644" s="T244">i-bi-jəʔ</ta>
            <ta e="T246" id="Seg_7645" s="T245">kabarləj</ta>
            <ta e="T247" id="Seg_7646" s="T246">šide</ta>
            <ta e="T248" id="Seg_7647" s="T247">nu-zAŋ</ta>
            <ta e="T249" id="Seg_7648" s="T248">amno-bi-jəʔ</ta>
            <ta e="T250" id="Seg_7649" s="T249">nüke</ta>
            <ta e="T251" id="Seg_7650" s="T250">i</ta>
            <ta e="T252" id="Seg_7651" s="T251">büzʼe</ta>
            <ta e="T253" id="Seg_7652" s="T252">ara</ta>
            <ta e="T254" id="Seg_7653" s="T253">bĭs-bi-jəʔ</ta>
            <ta e="T255" id="Seg_7654" s="T254">bar</ta>
            <ta e="T256" id="Seg_7655" s="T255">tʼabəro-bi-jəʔ</ta>
            <ta e="T257" id="Seg_7656" s="T256">dĭgəttə</ta>
            <ta e="T258" id="Seg_7657" s="T257">părog-gəndə</ta>
            <ta e="T259" id="Seg_7658" s="T258">amnə-bi-jəʔ</ta>
            <ta e="T260" id="Seg_7659" s="T259">dĭ</ta>
            <ta e="T261" id="Seg_7660" s="T260">ulu-gəndə</ta>
            <ta e="T262" id="Seg_7661" s="T261">teʔdə</ta>
            <ta e="T263" id="Seg_7662" s="T262">i-bi</ta>
            <ta e="T264" id="Seg_7663" s="T263">nüke</ta>
            <ta e="T265" id="Seg_7664" s="T264">i</ta>
            <ta e="T266" id="Seg_7665" s="T265">büzʼe-t</ta>
            <ta e="T267" id="Seg_7666" s="T266">nʼeʔbdə-do-laʔbə-bi</ta>
            <ta e="T268" id="Seg_7667" s="T267">nʼiʔ-do-laʔbə-bi</ta>
            <ta e="T269" id="Seg_7668" s="T268">i</ta>
            <ta e="T270" id="Seg_7669" s="T269">dărəʔ</ta>
            <ta e="T271" id="Seg_7670" s="T270">kunol-luʔbdə-bi-jəʔ</ta>
            <ta e="T272" id="Seg_7671" s="T271">ertə-n</ta>
            <ta e="T273" id="Seg_7672" s="T272">uʔbdə-bi-jəʔ</ta>
            <ta e="T274" id="Seg_7673" s="T273">măn</ta>
            <ta e="T275" id="Seg_7674" s="T274">ĭmbi=də</ta>
            <ta e="T276" id="Seg_7675" s="T275">bar</ta>
            <ta e="T277" id="Seg_7676" s="T276">ĭzem-liA-m</ta>
            <ta e="T278" id="Seg_7677" s="T277">a</ta>
            <ta e="T279" id="Seg_7678" s="T278">dĭ</ta>
            <ta e="T280" id="Seg_7679" s="T279">măn-ntə</ta>
            <ta e="T281" id="Seg_7680" s="T280">măn</ta>
            <ta e="T282" id="Seg_7681" s="T281">tože</ta>
            <ta e="T283" id="Seg_7682" s="T282">ĭzem-liA-m</ta>
            <ta e="T284" id="Seg_7683" s="T283">naverna</ta>
            <ta e="T285" id="Seg_7684" s="T284">miʔ</ta>
            <ta e="T286" id="Seg_7685" s="T285">tʼabəro-bi-bAʔ</ta>
            <ta e="T288" id="Seg_7686" s="T287">dĭ-m</ta>
            <ta e="T289" id="Seg_7687" s="T288">kăštə-bi-jəʔ</ta>
            <ta e="T293" id="Seg_7688" s="T292">a</ta>
            <ta e="T294" id="Seg_7689" s="T293">nüke-m</ta>
            <ta e="T295" id="Seg_7690" s="T294">kăštə-bi-jəʔ</ta>
            <ta e="T299" id="Seg_7691" s="T298">dĭ-zen</ta>
            <ta e="T300" id="Seg_7692" s="T299">tura-zAŋ-də</ta>
            <ta e="T301" id="Seg_7693" s="T300">üdʼüge</ta>
            <ta e="T302" id="Seg_7694" s="T301">i-bi-jəʔ</ta>
            <ta e="T303" id="Seg_7695" s="T302">i</ta>
            <ta e="T304" id="Seg_7696" s="T303">dĭn</ta>
            <ta e="T305" id="Seg_7697" s="T304">töštek-Kən</ta>
            <ta e="T306" id="Seg_7698" s="T305">nu-lAʔ-bi</ta>
            <ta e="T308" id="Seg_7699" s="T307">dĭ-n</ta>
            <ta e="T309" id="Seg_7700" s="T308">tolʼko</ta>
            <ta e="T310" id="Seg_7701" s="T309">sʼestra-t</ta>
            <ta e="T311" id="Seg_7702" s="T310">i-bi</ta>
            <ta e="T312" id="Seg_7703" s="T311">kăštə-bi-jəʔ</ta>
            <ta e="T313" id="Seg_7704" s="T312">Afanasʼa-jəʔ-ziʔ</ta>
            <ta e="T314" id="Seg_7705" s="T313">ešši-zAŋ-də</ta>
            <ta e="T315" id="Seg_7706" s="T314">naga-bi-jəʔ</ta>
            <ta e="T316" id="Seg_7707" s="T315">dărəʔ</ta>
            <ta e="T317" id="Seg_7708" s="T316">bar</ta>
            <ta e="T318" id="Seg_7709" s="T317">bar</ta>
            <ta e="T319" id="Seg_7710" s="T318">kü-laːm-bi-jəʔ</ta>
            <ta e="T321" id="Seg_7711" s="T320">bar</ta>
            <ta e="T322" id="Seg_7712" s="T321">nagur</ta>
            <ta e="T323" id="Seg_7713" s="T322">kü-laːm-bi-jəʔ</ta>
            <ta e="T327" id="Seg_7714" s="T326">sʼestra-t</ta>
            <ta e="T328" id="Seg_7715" s="T327">ĭmbi</ta>
            <ta e="T329" id="Seg_7716" s="T328">šiʔ</ta>
            <ta e="T330" id="Seg_7717" s="T329">tʼabəro-bi-lAʔ</ta>
            <ta e="T331" id="Seg_7718" s="T330">a</ta>
            <ta e="T332" id="Seg_7719" s="T331">măn</ta>
            <ta e="T333" id="Seg_7720" s="T332">šiʔnʼileʔ</ta>
            <ta e="T334" id="Seg_7721" s="T333">ej</ta>
            <ta e="T335" id="Seg_7722" s="T334">izen-bi-m</ta>
            <ta e="T336" id="Seg_7723" s="T335">dărəʔ</ta>
            <ta e="T337" id="Seg_7724" s="T336">bar</ta>
            <ta e="T338" id="Seg_7725" s="T337">kunol-luʔbdə-bi-lAʔ</ta>
            <ta e="T340" id="Seg_7726" s="T339">nu-zAŋ-Kən</ta>
            <ta e="T341" id="Seg_7727" s="T340">i-bi</ta>
            <ta e="T342" id="Seg_7728" s="T341">urgo</ta>
            <ta e="T343" id="Seg_7729" s="T342">tʼala</ta>
            <ta e="T345" id="Seg_7730" s="T344">tʼala</ta>
            <ta e="T346" id="Seg_7731" s="T345">onʼiʔ</ta>
            <ta e="T347" id="Seg_7732" s="T346">tura-Tə</ta>
            <ta e="T348" id="Seg_7733" s="T347">oʔbdə-liA-jəʔ</ta>
            <ta e="T349" id="Seg_7734" s="T348">bar</ta>
            <ta e="T350" id="Seg_7735" s="T349">dĭgəttə</ta>
            <ta e="T351" id="Seg_7736" s="T350">stol-də</ta>
            <ta e="T352" id="Seg_7737" s="T351">oʔbdə-liA-jəʔ</ta>
            <ta e="T353" id="Seg_7738" s="T352">akămnaʔ-liA-jəʔ</ta>
            <ta e="T354" id="Seg_7739" s="T353">i</ta>
            <ta e="T355" id="Seg_7740" s="T354">bĭs-laʔbə-jəʔ</ta>
            <ta e="T356" id="Seg_7741" s="T355">ara</ta>
            <ta e="T357" id="Seg_7742" s="T356">i</ta>
            <ta e="T358" id="Seg_7743" s="T357">am-laʔbə-jəʔ</ta>
            <ta e="T359" id="Seg_7744" s="T358">uja</ta>
            <ta e="T360" id="Seg_7745" s="T359">ipek</ta>
            <ta e="T361" id="Seg_7746" s="T360">ĭmbi</ta>
            <ta e="T362" id="Seg_7747" s="T361">i-gA</ta>
            <ta e="T363" id="Seg_7748" s="T362">stol-Kən</ta>
            <ta e="T364" id="Seg_7749" s="T363">dĭgəttə</ta>
            <ta e="T365" id="Seg_7750" s="T364">baška</ta>
            <ta e="T366" id="Seg_7751" s="T365">tura-Tə</ta>
            <ta e="T367" id="Seg_7752" s="T366">kan-lV-jəʔ</ta>
            <ta e="T368" id="Seg_7753" s="T367">dĭn</ta>
            <ta e="T369" id="Seg_7754" s="T368">dărəʔ</ta>
            <ta e="T370" id="Seg_7755" s="T369">že</ta>
            <ta e="T371" id="Seg_7756" s="T370">bĭs-lV-jəʔ</ta>
            <ta e="T372" id="Seg_7757" s="T371">dĭgəttə</ta>
            <ta e="T373" id="Seg_7758" s="T372">ĭššo</ta>
            <ta e="T374" id="Seg_7759" s="T373">onʼiʔ</ta>
            <ta e="T375" id="Seg_7760" s="T374">tura-Tə</ta>
            <ta e="T376" id="Seg_7761" s="T375">dĭgəttə</ta>
            <ta e="T377" id="Seg_7762" s="T376">teʔdə</ta>
            <ta e="T378" id="Seg_7763" s="T377">tura-Tə</ta>
            <ta e="T379" id="Seg_7764" s="T378">sumna</ta>
            <ta e="T381" id="Seg_7765" s="T380">sɨləj</ta>
            <ta e="T382" id="Seg_7766" s="T381">sumna</ta>
            <ta e="T383" id="Seg_7767" s="T382">tʼala</ta>
            <ta e="T384" id="Seg_7768" s="T383">bĭs-laʔbə-jəʔ</ta>
            <ta e="T385" id="Seg_7769" s="T384">üge</ta>
            <ta e="T386" id="Seg_7770" s="T385">ara</ta>
            <ta e="T387" id="Seg_7771" s="T386">i</ta>
            <ta e="T388" id="Seg_7772" s="T387">dĭgəttə</ta>
            <ta e="T389" id="Seg_7773" s="T388">kabarləj</ta>
            <ta e="T1011" id="Seg_7774" s="T391">Kazan</ta>
            <ta e="T392" id="Seg_7775" s="T1011">tura-Kən</ta>
            <ta e="T394" id="Seg_7776" s="T393">i-bi</ta>
            <ta e="T395" id="Seg_7777" s="T394">dĭ-zAŋ</ta>
            <ta e="T396" id="Seg_7778" s="T395">dĭbər</ta>
            <ta e="T397" id="Seg_7779" s="T396">kan-bi-jəʔ</ta>
            <ta e="T398" id="Seg_7780" s="T397">i</ta>
            <ta e="T399" id="Seg_7781" s="T398">dĭn</ta>
            <ta e="T400" id="Seg_7782" s="T399">ara</ta>
            <ta e="T401" id="Seg_7783" s="T400">i-bi-jəʔ</ta>
            <ta e="T402" id="Seg_7784" s="T401">šində-n</ta>
            <ta e="T403" id="Seg_7785" s="T402">aktʼa</ta>
            <ta e="T404" id="Seg_7786" s="T403">iʔgö</ta>
            <ta e="T405" id="Seg_7787" s="T404">tak</ta>
            <ta e="T406" id="Seg_7788" s="T405">iʔgö</ta>
            <ta e="T407" id="Seg_7789" s="T406">i-liA-jəʔ</ta>
            <ta e="T408" id="Seg_7790" s="T407">sumna</ta>
            <ta e="T410" id="Seg_7791" s="T409">a</ta>
            <ta e="T411" id="Seg_7792" s="T410">šində-n</ta>
            <ta e="T412" id="Seg_7793" s="T411">amka</ta>
            <ta e="T413" id="Seg_7794" s="T412">teʔdə</ta>
            <ta e="T414" id="Seg_7795" s="T413">i-lV-j</ta>
            <ta e="T415" id="Seg_7796" s="T414">šində-n</ta>
            <ta e="T416" id="Seg_7797" s="T415">ĭššo</ta>
            <ta e="T417" id="Seg_7798" s="T416">amka</ta>
            <ta e="T418" id="Seg_7799" s="T417">šide</ta>
            <ta e="T419" id="Seg_7800" s="T418">i-lV-j</ta>
            <ta e="T420" id="Seg_7801" s="T419">i</ta>
            <ta e="T421" id="Seg_7802" s="T420">dĭgəttə</ta>
            <ta e="T422" id="Seg_7803" s="T421">urgo</ta>
            <ta e="T423" id="Seg_7804" s="T422">tʼala-Kən</ta>
            <ta e="T424" id="Seg_7805" s="T423">bĭs-bi-jəʔ</ta>
            <ta e="T425" id="Seg_7806" s="T424">üdʼüge</ta>
            <ta e="T426" id="Seg_7807" s="T425">dĭrgit</ta>
            <ta e="T427" id="Seg_7808" s="T426">rʼumka-Tə</ta>
            <ta e="T428" id="Seg_7809" s="T427">kămnə-bi-jəʔ</ta>
            <ta e="T429" id="Seg_7810" s="T428">onʼiʔ-Tə</ta>
            <ta e="T430" id="Seg_7811" s="T429">mĭ-lV-j</ta>
            <ta e="T431" id="Seg_7812" s="T430">dĭgəttə</ta>
            <ta e="T432" id="Seg_7813" s="T431">bazoʔ</ta>
            <ta e="T433" id="Seg_7814" s="T432">tak</ta>
            <ta e="T434" id="Seg_7815" s="T433">dărəʔ</ta>
            <ta e="T435" id="Seg_7816" s="T434">dărəʔ</ta>
            <ta e="T436" id="Seg_7817" s="T435">mĭ-liA</ta>
            <ta e="T437" id="Seg_7818" s="T436">mĭ-liA</ta>
            <ta e="T438" id="Seg_7819" s="T437">bar</ta>
            <ta e="T439" id="Seg_7820" s="T438">jude</ta>
            <ta e="T440" id="Seg_7821" s="T439">dĭgəttə</ta>
            <ta e="T441" id="Seg_7822" s="T440">amor-laʔbə-jəʔ</ta>
            <ta e="T442" id="Seg_7823" s="T441">am-laʔbə-jəʔ</ta>
            <ta e="T443" id="Seg_7824" s="T442">bar</ta>
            <ta e="T444" id="Seg_7825" s="T443">tʼăbaktər-laʔbə-jəʔ</ta>
            <ta e="T445" id="Seg_7826" s="T444">nüjnə</ta>
            <ta e="T446" id="Seg_7827" s="T445">nüj-laʔbə-jəʔ</ta>
            <ta e="T451" id="Seg_7828" s="T450">kan-bi</ta>
            <ta e="T453" id="Seg_7829" s="T451">Kazan_tura-Tə</ta>
            <ta e="T455" id="Seg_7830" s="T454">i-bi</ta>
            <ta e="T456" id="Seg_7831" s="T455">urgo</ta>
            <ta e="T457" id="Seg_7832" s="T456">tʼala</ta>
            <ta e="T458" id="Seg_7833" s="T457">dĭn</ta>
            <ta e="T459" id="Seg_7834" s="T458">ara</ta>
            <ta e="T460" id="Seg_7835" s="T459">bĭs-bi-jəʔ</ta>
            <ta e="T461" id="Seg_7836" s="T460">bar</ta>
            <ta e="T462" id="Seg_7837" s="T461">kažən</ta>
            <ta e="T463" id="Seg_7838" s="T462">tura-Tə</ta>
            <ta e="T464" id="Seg_7839" s="T463">mĭn-bi-jəʔ</ta>
            <ta e="T465" id="Seg_7840" s="T464">dĭ</ta>
            <ta e="T466" id="Seg_7841" s="T465">nüdʼi-n</ta>
            <ta e="T1010" id="Seg_7842" s="T466">kan-lAʔ</ta>
            <ta e="T467" id="Seg_7843" s="T1010">tʼür-bi</ta>
            <ta e="T468" id="Seg_7844" s="T467">Anʼža-Tə</ta>
            <ta e="T469" id="Seg_7845" s="T468">i</ta>
            <ta e="T470" id="Seg_7846" s="T469">dĭn</ta>
            <ta e="T471" id="Seg_7847" s="T470">kănzə-laːm-bi</ta>
            <ta e="T472" id="Seg_7848" s="T471">kü-laːm-bi</ta>
            <ta e="T474" id="Seg_7849" s="T473">dĭ</ta>
            <ta e="T475" id="Seg_7850" s="T474">maʔ-gəndə</ta>
            <ta e="T477" id="Seg_7851" s="T476">maʔ-gəndə</ta>
            <ta e="T478" id="Seg_7852" s="T477">šo-bi</ta>
            <ta e="T479" id="Seg_7853" s="T478">nʼiʔdə</ta>
            <ta e="T480" id="Seg_7854" s="T479">šide</ta>
            <ta e="T481" id="Seg_7855" s="T480">kuza</ta>
            <ta e="T482" id="Seg_7856" s="T481">dĭ-Tə</ta>
            <ta e="T483" id="Seg_7857" s="T482">šo-bi-jəʔ</ta>
            <ta e="T484" id="Seg_7858" s="T483">kan-žə-bəj</ta>
            <ta e="T486" id="Seg_7859" s="T485">măn-ziʔ</ta>
            <ta e="T487" id="Seg_7860" s="T486">dĭgəttə</ta>
            <ta e="T488" id="Seg_7861" s="T487">kan-bi</ta>
            <ta e="T489" id="Seg_7862" s="T488">kan-bi</ta>
            <ta e="T490" id="Seg_7863" s="T489">ku-liA-t</ta>
            <ta e="T491" id="Seg_7864" s="T490">bar</ta>
            <ta e="T492" id="Seg_7865" s="T491">kulʼa</ta>
            <ta e="T493" id="Seg_7866" s="T492">bar</ta>
            <ta e="T495" id="Seg_7867" s="T494">dĭgəttə</ta>
            <ta e="T496" id="Seg_7868" s="T495">kudaj-Tə</ta>
            <ta e="T498" id="Seg_7869" s="T497">numan üzə-bi</ta>
            <ta e="T499" id="Seg_7870" s="T498">dĭʔə</ta>
            <ta e="T500" id="Seg_7871" s="T499">il-də</ta>
            <ta e="T501" id="Seg_7872" s="T500">nuʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T502" id="Seg_7873" s="T501">a</ta>
            <ta e="T503" id="Seg_7874" s="T502">dĭ</ta>
            <ta e="T504" id="Seg_7875" s="T503">maʔ-gəndə</ta>
            <ta e="T505" id="Seg_7876" s="T504">šo-bi</ta>
            <ta e="T506" id="Seg_7877" s="T505">bos-də</ta>
            <ta e="T507" id="Seg_7878" s="T506">sĭri</ta>
            <ta e="T508" id="Seg_7879" s="T507">mo-bi</ta>
            <ta e="T510" id="Seg_7880" s="T509">dĭ-zAŋ</ta>
            <ta e="T511" id="Seg_7881" s="T510">ugaːndə</ta>
            <ta e="T512" id="Seg_7882" s="T511">ara</ta>
            <ta e="T513" id="Seg_7883" s="T512">iʔgö</ta>
            <ta e="T514" id="Seg_7884" s="T513">bĭs-laʔbə-jəʔ</ta>
            <ta e="T515" id="Seg_7885" s="T514">kumən</ta>
            <ta e="T516" id="Seg_7886" s="T515">xotʼ</ta>
            <ta e="T518" id="Seg_7887" s="T517">üge</ta>
            <ta e="T519" id="Seg_7888" s="T518">bĭs-laʔbə-jəʔ</ta>
            <ta e="T520" id="Seg_7889" s="T519">bĭs-laʔbə-jəʔ</ta>
            <ta e="T521" id="Seg_7890" s="T520">ĭššo</ta>
            <ta e="T522" id="Seg_7891" s="T521">pi-laʔbə-jəʔ</ta>
            <ta e="T523" id="Seg_7892" s="T522">gijen</ta>
            <ta e="T524" id="Seg_7893" s="T523">i-gA</ta>
            <ta e="T525" id="Seg_7894" s="T524">tak</ta>
            <ta e="T526" id="Seg_7895" s="T525">dĭbər</ta>
            <ta e="T527" id="Seg_7896" s="T526">nuʔmə-luʔbə-jəʔ</ta>
            <ta e="T528" id="Seg_7897" s="T527">bar</ta>
            <ta e="T530" id="Seg_7898" s="T529">dĭgəttə</ta>
            <ta e="T531" id="Seg_7899" s="T530">bar</ta>
            <ta e="T532" id="Seg_7900" s="T531">nüj-laʔbə-jəʔ</ta>
            <ta e="T533" id="Seg_7901" s="T532">süʔmə-laʔbə-jəʔ</ta>
            <ta e="T534" id="Seg_7902" s="T533">dĭgəttə</ta>
            <ta e="T535" id="Seg_7903" s="T534">bar</ta>
            <ta e="T536" id="Seg_7904" s="T535">tʼabəro-laʔbə-jəʔ</ta>
            <ta e="T538" id="Seg_7905" s="T537">šində</ta>
            <ta e="T539" id="Seg_7906" s="T538">bar</ta>
            <ta e="T540" id="Seg_7907" s="T539">iʔgö</ta>
            <ta e="T542" id="Seg_7908" s="T541">bĭs-liA</ta>
            <ta e="T543" id="Seg_7909" s="T542">bĭs-liA</ta>
            <ta e="T544" id="Seg_7910" s="T543">dĭgəttə</ta>
            <ta e="T545" id="Seg_7911" s="T544">ara-gəʔ</ta>
            <ta e="T546" id="Seg_7912" s="T545">bar</ta>
            <ta e="T547" id="Seg_7913" s="T546">nanʼi-laʔbə</ta>
            <ta e="T548" id="Seg_7914" s="T547">dĭ</ta>
            <ta e="T549" id="Seg_7915" s="T548">dĭ-Tə</ta>
            <ta e="T550" id="Seg_7916" s="T549">aŋ-Tə</ta>
            <ta e="T551" id="Seg_7917" s="T550">bar</ta>
            <ta e="T552" id="Seg_7918" s="T551">kĭnzə-laʔbə-jəʔ</ta>
            <ta e="T553" id="Seg_7919" s="T552">dĭgəttə</ta>
            <ta e="T554" id="Seg_7920" s="T553">dĭ</ta>
            <ta e="T556" id="Seg_7921" s="T555">uʔbdə-lV-j</ta>
            <ta e="T558" id="Seg_7922" s="T557">onʼiʔ</ta>
            <ta e="T559" id="Seg_7923" s="T558">onʼiʔ</ta>
            <ta e="T560" id="Seg_7924" s="T559">măn</ta>
            <ta e="T561" id="Seg_7925" s="T560">ku-m</ta>
            <ta e="T562" id="Seg_7926" s="T561">ĭzem-luʔbdə-bi</ta>
            <ta e="T563" id="Seg_7927" s="T562">bar</ta>
            <ta e="T564" id="Seg_7928" s="T563">dĭn</ta>
            <ta e="T565" id="Seg_7929" s="T564">müʔbdə-laʔbə</ta>
            <ta e="T566" id="Seg_7930" s="T565">măn</ta>
            <ta e="T567" id="Seg_7931" s="T566">dĭgəttə</ta>
            <ta e="T568" id="Seg_7932" s="T567">ku-bi-m</ta>
            <ta e="T569" id="Seg_7933" s="T568">trʼapka</ta>
            <ta e="T570" id="Seg_7934" s="T569">kĭnzə-bi-m</ta>
            <ta e="T571" id="Seg_7935" s="T570">dĭ-gəʔ</ta>
            <ta e="T573" id="Seg_7936" s="T572">ažnə</ta>
            <ta e="T574" id="Seg_7937" s="T573">mʼaŋ-luʔbdə-bi</ta>
            <ta e="T575" id="Seg_7938" s="T574">măn</ta>
            <ta e="T576" id="Seg_7939" s="T575">ku-Tə</ta>
            <ta e="T577" id="Seg_7940" s="T576">hen-bi-m</ta>
            <ta e="T580" id="Seg_7941" s="T579">dĭgəttə</ta>
            <ta e="T581" id="Seg_7942" s="T580">sar-bi-m</ta>
            <ta e="T582" id="Seg_7943" s="T581">iʔbə-bi-m</ta>
            <ta e="T583" id="Seg_7944" s="T582">kunol-luʔbdə-bi-m</ta>
            <ta e="T584" id="Seg_7945" s="T583">uʔbdə-bi-m</ta>
            <ta e="T586" id="Seg_7946" s="T585">ku-m</ta>
            <ta e="T587" id="Seg_7947" s="T586">ej</ta>
            <ta e="T588" id="Seg_7948" s="T587">ĭzem-liA</ta>
            <ta e="T589" id="Seg_7949" s="T588">a</ta>
            <ta e="T590" id="Seg_7950" s="T589">dĭgəttə</ta>
            <ta e="T591" id="Seg_7951" s="T590">ulu-m</ta>
            <ta e="T592" id="Seg_7952" s="T591">ĭzem-liA</ta>
            <ta e="T593" id="Seg_7953" s="T592">măn</ta>
            <ta e="T594" id="Seg_7954" s="T593">tože</ta>
            <ta e="T595" id="Seg_7955" s="T594">kĭnzə-lV-m</ta>
            <ta e="T596" id="Seg_7956" s="T595">trʼapka-jəʔ-Tə</ta>
            <ta e="T597" id="Seg_7957" s="T596">ulu-gəndə</ta>
            <ta e="T598" id="Seg_7958" s="T597">sar-lV-m</ta>
            <ta e="T599" id="Seg_7959" s="T598">iʔbə-lV-m</ta>
            <ta e="T600" id="Seg_7960" s="T599">dĭ</ta>
            <ta e="T602" id="Seg_7961" s="T601">koː-luʔbdə-lV-j</ta>
            <ta e="T603" id="Seg_7962" s="T602">dĭ</ta>
            <ta e="T604" id="Seg_7963" s="T603">trʼapka</ta>
            <ta e="T605" id="Seg_7964" s="T604">ulu-m</ta>
            <ta e="T606" id="Seg_7965" s="T605">ej</ta>
            <ta e="T607" id="Seg_7966" s="T606">ĭzem-liA</ta>
            <ta e="T609" id="Seg_7967" s="T608">onʼiʔ</ta>
            <ta e="T610" id="Seg_7968" s="T609">onʼiʔ</ta>
            <ta e="T611" id="Seg_7969" s="T610">raz</ta>
            <ta e="T612" id="Seg_7970" s="T611">miʔ</ta>
            <ta e="T613" id="Seg_7971" s="T612">ipek</ta>
            <ta e="T614" id="Seg_7972" s="T613">toʔbdə-nar-bi-bAʔ</ta>
            <ta e="T615" id="Seg_7973" s="T614">mašina-jəʔ-ziʔ</ta>
            <ta e="T616" id="Seg_7974" s="T615">dĭn</ta>
            <ta e="T617" id="Seg_7975" s="T616">tagaj</ta>
            <ta e="T618" id="Seg_7976" s="T617">i-bi</ta>
            <ta e="T619" id="Seg_7977" s="T618">snăpɨ-jəʔ</ta>
            <ta e="T621" id="Seg_7978" s="T620">dʼagar-zittə</ta>
            <ta e="T622" id="Seg_7979" s="T621">a</ta>
            <ta e="T623" id="Seg_7980" s="T622">dĭ</ta>
            <ta e="T625" id="Seg_7981" s="T624">kădaʔ=də</ta>
            <ta e="T626" id="Seg_7982" s="T625">uda-bə</ta>
            <ta e="T627" id="Seg_7983" s="T626">bar</ta>
            <ta e="T628" id="Seg_7984" s="T627">băt-bi</ta>
            <ta e="T629" id="Seg_7985" s="T628">măn</ta>
            <ta e="T630" id="Seg_7986" s="T629">măn-ntə-m</ta>
            <ta e="T631" id="Seg_7987" s="T630">kĭnzə-ʔ</ta>
            <ta e="T632" id="Seg_7988" s="T631">dĭ</ta>
            <ta e="T633" id="Seg_7989" s="T632">kĭnzə-bi</ta>
            <ta e="T634" id="Seg_7990" s="T633">i</ta>
            <ta e="T635" id="Seg_7991" s="T634">sar-bi</ta>
            <ta e="T636" id="Seg_7992" s="T635">dĭgəttə</ta>
            <ta e="T637" id="Seg_7993" s="T636">ej</ta>
            <ta e="T638" id="Seg_7994" s="T637">ĭzem-bi</ta>
            <ta e="T639" id="Seg_7995" s="T638">uda-t</ta>
            <ta e="T641" id="Seg_7996" s="T640">miʔ</ta>
            <ta e="T642" id="Seg_7997" s="T641">naga-bi-jəʔ</ta>
            <ta e="T643" id="Seg_7998" s="T642">doktăr-jəʔ</ta>
            <ta e="T644" id="Seg_7999" s="T643">miʔ</ta>
            <ta e="T646" id="Seg_8000" s="T645">noʔ-ziʔ</ta>
            <ta e="T647" id="Seg_8001" s="T646">bar</ta>
            <ta e="T648" id="Seg_8002" s="T647">tʼezer-bi-bAʔ</ta>
            <ta e="T649" id="Seg_8003" s="T648">mĭnzər-laʔbə</ta>
            <ta e="T650" id="Seg_8004" s="T649">dĭgəttə</ta>
            <ta e="T651" id="Seg_8005" s="T650">bĭs-laʔbə</ta>
            <ta e="T652" id="Seg_8006" s="T651">onʼiʔ</ta>
            <ta e="T653" id="Seg_8007" s="T652">noʔ</ta>
            <ta e="T654" id="Seg_8008" s="T653">i-gA</ta>
            <ta e="T655" id="Seg_8009" s="T654">putʼəga</ta>
            <ta e="T656" id="Seg_8010" s="T655">žaludkə</ta>
            <ta e="T657" id="Seg_8011" s="T656">ĭzem-liA</ta>
            <ta e="T658" id="Seg_8012" s="T657">dĭ-m</ta>
            <ta e="T659" id="Seg_8013" s="T658">mĭnzər-liA-l</ta>
            <ta e="T660" id="Seg_8014" s="T659">bĭs-liA-l</ta>
            <ta e="T661" id="Seg_8015" s="T660">dĭgəttə</ta>
            <ta e="T662" id="Seg_8016" s="T661">amor-lV-l</ta>
            <ta e="T663" id="Seg_8017" s="T662">a</ta>
            <ta e="T664" id="Seg_8018" s="T663">onʼiʔ</ta>
            <ta e="T667" id="Seg_8019" s="T666">noʔ</ta>
            <ta e="T668" id="Seg_8020" s="T667">i-gA</ta>
            <ta e="T669" id="Seg_8021" s="T668">filitʼagɨ</ta>
            <ta e="T670" id="Seg_8022" s="T669">numəj-liA-jəʔ</ta>
            <ta e="T671" id="Seg_8023" s="T670">dĭ-m</ta>
            <ta e="T672" id="Seg_8024" s="T671">hen-lV-l</ta>
            <ta e="T673" id="Seg_8025" s="T672">ara-Tə</ta>
            <ta e="T674" id="Seg_8026" s="T673">dĭ</ta>
            <ta e="T675" id="Seg_8027" s="T674">nu-KV-j</ta>
            <ta e="T676" id="Seg_8028" s="T675">biəʔ</ta>
            <ta e="T677" id="Seg_8029" s="T676">šide</ta>
            <ta e="T678" id="Seg_8030" s="T677">tʼala</ta>
            <ta e="T679" id="Seg_8031" s="T678">dĭgəttə</ta>
            <ta e="T680" id="Seg_8032" s="T679">bĭs-lV-l</ta>
            <ta e="T681" id="Seg_8033" s="T680">sĭj-də</ta>
            <ta e="T682" id="Seg_8034" s="T681">jakšə</ta>
            <ta e="T683" id="Seg_8035" s="T682">mo-lV-l</ta>
            <ta e="T685" id="Seg_8036" s="T684">tăn</ta>
            <ta e="T686" id="Seg_8037" s="T685">miʔ</ta>
            <ta e="T687" id="Seg_8038" s="T686">nu-zAŋ-dən</ta>
            <ta e="T688" id="Seg_8039" s="T687">maslenka-Kən</ta>
            <ta e="T689" id="Seg_8040" s="T688">bar</ta>
            <ta e="T690" id="Seg_8041" s="T689">ine-t-ziʔ</ta>
            <ta e="T691" id="Seg_8042" s="T690">tuno-laʔbə-jəʔ</ta>
            <ta e="T692" id="Seg_8043" s="T691">šində-n</ta>
            <ta e="T693" id="Seg_8044" s="T692">ine-t</ta>
            <ta e="T694" id="Seg_8045" s="T693">büžü</ta>
            <ta e="T695" id="Seg_8046" s="T694">šo-lV-j</ta>
            <ta e="T696" id="Seg_8047" s="T695">a</ta>
            <ta e="T697" id="Seg_8048" s="T696">šində-n</ta>
            <ta e="T698" id="Seg_8049" s="T697">ine-t</ta>
            <ta e="T699" id="Seg_8050" s="T698">ma-lV-j</ta>
            <ta e="T700" id="Seg_8051" s="T699">dĭ</ta>
            <ta e="T701" id="Seg_8052" s="T700">šində-n</ta>
            <ta e="T702" id="Seg_8053" s="T701">ma-lV-j</ta>
            <ta e="T703" id="Seg_8054" s="T702">dĭ</ta>
            <ta e="T706" id="Seg_8055" s="T705">ara</ta>
            <ta e="T707" id="Seg_8056" s="T706">nuldə-lV-j</ta>
            <ta e="T709" id="Seg_8057" s="T708">dĭgəttə</ta>
            <ta e="T710" id="Seg_8058" s="T709">baška</ta>
            <ta e="T711" id="Seg_8059" s="T710">ine</ta>
            <ta e="T712" id="Seg_8060" s="T711">det-lV-jəʔ</ta>
            <ta e="T713" id="Seg_8061" s="T712">a</ta>
            <ta e="T714" id="Seg_8062" s="T713">dĭ</ta>
            <ta e="T715" id="Seg_8063" s="T714">dö</ta>
            <ta e="T716" id="Seg_8064" s="T715">ine</ta>
            <ta e="T717" id="Seg_8065" s="T716">ugaːndə</ta>
            <ta e="T719" id="Seg_8066" s="T718">măn-liA-jəʔ</ta>
            <ta e="T720" id="Seg_8067" s="T719">dărəʔ</ta>
            <ta e="T721" id="Seg_8068" s="T720">dĭgəttə</ta>
            <ta e="T722" id="Seg_8069" s="T721">dĭ-m</ta>
            <ta e="T723" id="Seg_8070" s="T722">tože</ta>
            <ta e="T724" id="Seg_8071" s="T723">ma-luʔbdə-ləj</ta>
            <ta e="T725" id="Seg_8072" s="T724">döbər</ta>
            <ta e="T728" id="Seg_8073" s="T727">nuʔmə-luʔbdə-lV-j</ta>
            <ta e="T729" id="Seg_8074" s="T728">a</ta>
            <ta e="T730" id="Seg_8075" s="T729">dĭ</ta>
            <ta e="T731" id="Seg_8076" s="T730">ine</ta>
            <ta e="T732" id="Seg_8077" s="T731">ma-luʔbdə-ləj</ta>
            <ta e="T734" id="Seg_8078" s="T733">dĭgəttə</ta>
            <ta e="T735" id="Seg_8079" s="T734">dĭ</ta>
            <ta e="T736" id="Seg_8080" s="T735">ine-m</ta>
            <ta e="T739" id="Seg_8081" s="T738">i-liA</ta>
            <ta e="T740" id="Seg_8082" s="T739">i</ta>
            <ta e="T741" id="Seg_8083" s="T740">mĭn-gA</ta>
            <ta e="T742" id="Seg_8084" s="T741">mĭn-gA</ta>
            <ta e="T743" id="Seg_8085" s="T742">koldʼəŋ</ta>
            <ta e="T744" id="Seg_8086" s="T743">štobɨ</ta>
            <ta e="T745" id="Seg_8087" s="T744">ej</ta>
            <ta e="T746" id="Seg_8088" s="T745">nu-bi</ta>
            <ta e="T748" id="Seg_8089" s="T747">dĭgəttə</ta>
            <ta e="T749" id="Seg_8090" s="T748">ara</ta>
            <ta e="T750" id="Seg_8091" s="T749">iʔgö</ta>
            <ta e="T751" id="Seg_8092" s="T750">bĭs-luʔbdə-lV-jəʔ</ta>
            <ta e="T752" id="Seg_8093" s="T751">i</ta>
            <ta e="T753" id="Seg_8094" s="T752">bar</ta>
            <ta e="T754" id="Seg_8095" s="T753">saʔmə-luʔbdə-lV-jəʔ</ta>
            <ta e="T756" id="Seg_8096" s="T755">dĭgəttə</ta>
            <ta e="T757" id="Seg_8097" s="T756">tibi-zAŋ</ta>
            <ta e="T758" id="Seg_8098" s="T757">pa-jəʔ</ta>
            <ta e="T759" id="Seg_8099" s="T758">i-lV-jəʔ</ta>
            <ta e="T760" id="Seg_8100" s="T759">uda-gəndə</ta>
            <ta e="T761" id="Seg_8101" s="T760">i</ta>
            <ta e="T762" id="Seg_8102" s="T761">dĭgəttə</ta>
            <ta e="T769" id="Seg_8103" s="T768">dĭgəttə</ta>
            <ta e="T770" id="Seg_8104" s="T769">onʼiʔ</ta>
            <ta e="T771" id="Seg_8105" s="T770">onʼiʔ-Tə</ta>
            <ta e="T772" id="Seg_8106" s="T771">bar</ta>
            <ta e="T773" id="Seg_8107" s="T772">nuʔ</ta>
            <ta e="T775" id="Seg_8108" s="T774">nʼot-laʔbə</ta>
            <ta e="T776" id="Seg_8109" s="T775">dĭgəttə</ta>
            <ta e="T777" id="Seg_8110" s="T776">onʼiʔ</ta>
            <ta e="T778" id="Seg_8111" s="T777">kuštü</ta>
            <ta e="T782" id="Seg_8112" s="T781">dĭ-m</ta>
            <ta e="T783" id="Seg_8113" s="T782">bar</ta>
            <ta e="T784" id="Seg_8114" s="T783">üjü-gəndə</ta>
            <ta e="T785" id="Seg_8115" s="T784">nuldə-lV-lV-j</ta>
            <ta e="T786" id="Seg_8116" s="T785">dĭ</ta>
            <ta e="T787" id="Seg_8117" s="T786">bar</ta>
            <ta e="T788" id="Seg_8118" s="T787">dĭgəttə</ta>
            <ta e="T789" id="Seg_8119" s="T788">ara</ta>
            <ta e="T790" id="Seg_8120" s="T789">nu-luʔbdə-lV-j</ta>
            <ta e="T791" id="Seg_8121" s="T790">dĭgəttə</ta>
            <ta e="T792" id="Seg_8122" s="T791">bĭs-liA-jəʔ</ta>
            <ta e="T794" id="Seg_8123" s="T793">dĭgəttə</ta>
            <ta e="T795" id="Seg_8124" s="T794">šide-göʔ</ta>
            <ta e="T796" id="Seg_8125" s="T795">nu-zAŋ</ta>
            <ta e="T798" id="Seg_8126" s="T797">šide-göʔ</ta>
            <ta e="T799" id="Seg_8127" s="T798">nu-zAŋ</ta>
            <ta e="T800" id="Seg_8128" s="T799">i-lV-jəʔ</ta>
            <ta e="T801" id="Seg_8129" s="T800">onʼiʔ</ta>
            <ta e="T802" id="Seg_8130" s="T801">onʼiʔ-Tə</ta>
            <ta e="T803" id="Seg_8131" s="T802">dĭgəttə</ta>
            <ta e="T804" id="Seg_8132" s="T803">kajət</ta>
            <ta e="T805" id="Seg_8133" s="T804">kuštü</ta>
            <ta e="T806" id="Seg_8134" s="T805">dö-m</ta>
            <ta e="T807" id="Seg_8135" s="T806">barəʔ-lV-j</ta>
            <ta e="T808" id="Seg_8136" s="T807">tʼo-Tə</ta>
            <ta e="T809" id="Seg_8137" s="T808">dĭgəttə</ta>
            <ta e="T810" id="Seg_8138" s="T809">dĭ</ta>
            <ta e="T811" id="Seg_8139" s="T810">ara</ta>
            <ta e="T812" id="Seg_8140" s="T811">nuldə-liA</ta>
            <ta e="T813" id="Seg_8141" s="T812">dĭ-Tə</ta>
            <ta e="T814" id="Seg_8142" s="T813">bĭs-laʔbə-jəʔ</ta>
            <ta e="T816" id="Seg_8143" s="T815">dĭgəttə</ta>
            <ta e="T817" id="Seg_8144" s="T816">tʼo-Tə</ta>
            <ta e="T818" id="Seg_8145" s="T817">barəʔ-lV-j</ta>
            <ta e="T819" id="Seg_8146" s="T818">uda-bə</ta>
            <ta e="T820" id="Seg_8147" s="T819">bar</ta>
            <ta e="T821" id="Seg_8148" s="T820">băldə-lV-j</ta>
            <ta e="T824" id="Seg_8149" s="T823">varota-jəʔ</ta>
            <ta e="T825" id="Seg_8150" s="T824">nu-laʔbə-jəʔ</ta>
            <ta e="T826" id="Seg_8151" s="T825">kan-ə-ʔ</ta>
            <ta e="T827" id="Seg_8152" s="T826">girgit</ta>
            <ta e="T828" id="Seg_8153" s="T827">aʔtʼi-Tə</ta>
            <ta e="T829" id="Seg_8154" s="T828">kan-lV-l</ta>
            <ta e="T830" id="Seg_8155" s="T829">pravăj</ta>
            <ta e="T831" id="Seg_8156" s="T830">aʔtʼi</ta>
            <ta e="T832" id="Seg_8157" s="T831">bar</ta>
            <ta e="T833" id="Seg_8158" s="T832">ej</ta>
            <ta e="T834" id="Seg_8159" s="T833">jakšə</ta>
            <ta e="T835" id="Seg_8160" s="T834">bar</ta>
            <ta e="T836" id="Seg_8161" s="T835">bălotă-jəʔ</ta>
            <ta e="T837" id="Seg_8162" s="T836">pa-jəʔ</ta>
            <ta e="T838" id="Seg_8163" s="T837">nu-gA-jəʔ</ta>
            <ta e="T839" id="Seg_8164" s="T838">i</ta>
            <ta e="T840" id="Seg_8165" s="T839">bar</ta>
            <ta e="T841" id="Seg_8166" s="T840">groza-jəʔ</ta>
            <ta e="T842" id="Seg_8167" s="T841">beržə</ta>
            <ta e="T843" id="Seg_8168" s="T842">bar</ta>
            <ta e="T844" id="Seg_8169" s="T843">baza-j</ta>
            <ta e="T845" id="Seg_8170" s="T844">kălʼučka-jəʔ</ta>
            <ta e="T846" id="Seg_8171" s="T845">bar</ta>
            <ta e="T847" id="Seg_8172" s="T846">mündüʔ-laʔbə-jəʔ</ta>
            <ta e="T848" id="Seg_8173" s="T847">a</ta>
            <ta e="T849" id="Seg_8174" s="T848">dĭbər</ta>
            <ta e="T850" id="Seg_8175" s="T849">kuŋgə-ŋ</ta>
            <ta e="T851" id="Seg_8176" s="T850">kan-lV-l</ta>
            <ta e="T852" id="Seg_8177" s="T851">dĭn</ta>
            <ta e="T853" id="Seg_8178" s="T852">bar</ta>
            <ta e="T854" id="Seg_8179" s="T853">jakšə</ta>
            <ta e="T855" id="Seg_8180" s="T854">pa-jəʔ</ta>
            <ta e="T856" id="Seg_8181" s="T855">naga</ta>
            <ta e="T857" id="Seg_8182" s="T856">ĭmbi=də</ta>
            <ta e="T858" id="Seg_8183" s="T857">naga</ta>
            <ta e="T859" id="Seg_8184" s="T858">ugaːndə</ta>
            <ta e="T860" id="Seg_8185" s="T859">kuvas</ta>
            <ta e="T861" id="Seg_8186" s="T860">noʔ</ta>
            <ta e="T862" id="Seg_8187" s="T861">kuvas</ta>
            <ta e="T864" id="Seg_8188" s="T863">svetok-jəʔ</ta>
            <ta e="T865" id="Seg_8189" s="T864">sĭri</ta>
            <ta e="T866" id="Seg_8190" s="T865">kömə</ta>
            <ta e="T868" id="Seg_8191" s="T867">özer-laʔbə</ta>
            <ta e="T869" id="Seg_8192" s="T868">jablokă-jəʔ</ta>
            <ta e="T870" id="Seg_8193" s="T869">kuja</ta>
            <ta e="T871" id="Seg_8194" s="T870">mĭn-laʔbə</ta>
            <ta e="T872" id="Seg_8195" s="T871">ugaːndə</ta>
            <ta e="T873" id="Seg_8196" s="T872">ejü</ta>
            <ta e="T874" id="Seg_8197" s="T873">xotʼ</ta>
            <ta e="T875" id="Seg_8198" s="T874">iʔbə-ʔ</ta>
            <ta e="T876" id="Seg_8199" s="T875">da</ta>
            <ta e="T877" id="Seg_8200" s="T876">kunol-ə-ʔ</ta>
            <ta e="T878" id="Seg_8201" s="T877">jakšə</ta>
            <ta e="T879" id="Seg_8202" s="T878">dĭn</ta>
            <ta e="T880" id="Seg_8203" s="T879">a</ta>
            <ta e="T881" id="Seg_8204" s="T880">lʼevăj</ta>
            <ta e="T882" id="Seg_8205" s="T881">uda-gəndə</ta>
            <ta e="T883" id="Seg_8206" s="T882">kandə-gA</ta>
            <ta e="T884" id="Seg_8207" s="T883">ugaːndə</ta>
            <ta e="T885" id="Seg_8208" s="T884">aʔtʼi</ta>
            <ta e="T886" id="Seg_8209" s="T885">urgo</ta>
            <ta e="T887" id="Seg_8210" s="T886">il</ta>
            <ta e="T888" id="Seg_8211" s="T887">iʔgö</ta>
            <ta e="T889" id="Seg_8212" s="T888">bar</ta>
            <ta e="T890" id="Seg_8213" s="T889">ara</ta>
            <ta e="T891" id="Seg_8214" s="T890">bĭs-liA-jəʔ</ta>
            <ta e="T892" id="Seg_8215" s="T891">bar</ta>
            <ta e="T893" id="Seg_8216" s="T892">ĭmbi</ta>
            <ta e="T894" id="Seg_8217" s="T893">am-liA-jəʔ</ta>
            <ta e="T895" id="Seg_8218" s="T894">bar</ta>
            <ta e="T896" id="Seg_8219" s="T895">süʔmə-laʔbə-jəʔ</ta>
            <ta e="T897" id="Seg_8220" s="T896">bar</ta>
            <ta e="T898" id="Seg_8221" s="T897">nüjnə</ta>
            <ta e="T899" id="Seg_8222" s="T898">nüj-laʔbə-jəʔ</ta>
            <ta e="T900" id="Seg_8223" s="T899">a</ta>
            <ta e="T902" id="Seg_8224" s="T901">dĭbər</ta>
            <ta e="T903" id="Seg_8225" s="T902">kuŋgə</ta>
            <ta e="T905" id="Seg_8226" s="T904">kan-lV-l</ta>
            <ta e="T906" id="Seg_8227" s="T905">dĭn</ta>
            <ta e="T907" id="Seg_8228" s="T906">bar</ta>
            <ta e="T908" id="Seg_8229" s="T907">šü</ta>
            <ta e="T909" id="Seg_8230" s="T908">bar</ta>
            <ta e="T910" id="Seg_8231" s="T909">pi-jəʔ</ta>
            <ta e="T911" id="Seg_8232" s="T910">bar</ta>
            <ta e="T912" id="Seg_8233" s="T911">groza-jəʔ</ta>
            <ta e="T913" id="Seg_8234" s="T912">šü</ta>
            <ta e="T914" id="Seg_8235" s="T913">il-ə-m</ta>
            <ta e="T915" id="Seg_8236" s="T914">bar</ta>
            <ta e="T916" id="Seg_8237" s="T915">nend-laʔbə</ta>
            <ta e="T917" id="Seg_8238" s="T916">tüj</ta>
            <ta e="T918" id="Seg_8239" s="T917">bar</ta>
            <ta e="T920" id="Seg_8240" s="T919">šiʔ</ta>
            <ta e="T921" id="Seg_8241" s="T920">kan-KAʔ</ta>
            <ta e="T922" id="Seg_8242" s="T921">a</ta>
            <ta e="T923" id="Seg_8243" s="T922">măn</ta>
            <ta e="T924" id="Seg_8244" s="T923">üjilger-lV-m</ta>
            <ta e="T925" id="Seg_8245" s="T924">nadə</ta>
            <ta e="T926" id="Seg_8246" s="T925">măna</ta>
            <ta e="T927" id="Seg_8247" s="T926">ipek</ta>
            <ta e="T928" id="Seg_8248" s="T927">nuldə-zittə</ta>
            <ta e="T929" id="Seg_8249" s="T928">pür-zittə</ta>
            <ta e="T930" id="Seg_8250" s="T929">ipek</ta>
            <ta e="T931" id="Seg_8251" s="T930">nadə</ta>
            <ta e="T932" id="Seg_8252" s="T931">ato</ta>
            <ta e="T933" id="Seg_8253" s="T932">am-zittə</ta>
            <ta e="T934" id="Seg_8254" s="T933">naga</ta>
            <ta e="T935" id="Seg_8255" s="T934">ipek</ta>
            <ta e="T936" id="Seg_8256" s="T935">šiʔnʼileʔ</ta>
            <ta e="T937" id="Seg_8257" s="T936">mĭ-lV-m</ta>
            <ta e="T938" id="Seg_8258" s="T937">amor-bi-lAʔ</ta>
            <ta e="T940" id="Seg_8259" s="T939">e-ʔ</ta>
            <ta e="T941" id="Seg_8260" s="T940">kan-ə-ʔ</ta>
            <ta e="T942" id="Seg_8261" s="T941">tibi-Tə</ta>
            <ta e="T943" id="Seg_8262" s="T942">amno-ʔ</ta>
            <ta e="T944" id="Seg_8263" s="T943">unʼə-Tə</ta>
            <ta e="T945" id="Seg_8264" s="T944">jakšə</ta>
            <ta e="T946" id="Seg_8265" s="T945">amno-zittə</ta>
            <ta e="T947" id="Seg_8266" s="T946">gibər=nʼibudʼ</ta>
            <ta e="T948" id="Seg_8267" s="T947">kan-lV-m</ta>
            <ta e="T949" id="Seg_8268" s="T948">šində-Tə=də</ta>
            <ta e="T950" id="Seg_8269" s="T949">ej</ta>
            <ta e="T951" id="Seg_8270" s="T950">surar-bi-m</ta>
            <ta e="T953" id="Seg_8271" s="T952">bü</ta>
            <ta e="T954" id="Seg_8272" s="T953">mʼaŋ-laʔbə</ta>
            <ta e="T955" id="Seg_8273" s="T954">dĭ</ta>
            <ta e="T956" id="Seg_8274" s="T955">urgo</ta>
            <ta e="T957" id="Seg_8275" s="T956">Ilʼbinʼ</ta>
            <ta e="T958" id="Seg_8276" s="T957">üdʼüge</ta>
            <ta e="T959" id="Seg_8277" s="T958">Ilʼbinʼ</ta>
            <ta e="T960" id="Seg_8278" s="T959">a</ta>
            <ta e="T961" id="Seg_8279" s="T960">gijen</ta>
            <ta e="T962" id="Seg_8280" s="T961">bü</ta>
            <ta e="T963" id="Seg_8281" s="T962">i-laʔbə</ta>
            <ta e="T964" id="Seg_8282" s="T963">dĭn</ta>
            <ta e="T965" id="Seg_8283" s="T964">bar</ta>
            <ta e="T966" id="Seg_8284" s="T965">numəj-liA-jəʔ</ta>
            <ta e="T967" id="Seg_8285" s="T966">bar</ta>
            <ta e="T970" id="Seg_8286" s="T969">onʼiʔ</ta>
            <ta e="T971" id="Seg_8287" s="T970">kö</ta>
            <ta e="T972" id="Seg_8288" s="T971">bar</ta>
            <ta e="T973" id="Seg_8289" s="T972">miʔ</ta>
            <ta e="T974" id="Seg_8290" s="T973">aš</ta>
            <ta e="T975" id="Seg_8291" s="T974">pĭde-bi-bAʔ</ta>
            <ta e="T976" id="Seg_8292" s="T975">dĭ-zAŋ</ta>
            <ta e="T977" id="Seg_8293" s="T976">bar</ta>
            <ta e="T978" id="Seg_8294" s="T977">üge</ta>
            <ta e="T979" id="Seg_8295" s="T978">amno-laʔbə-jəʔ</ta>
            <ta e="T980" id="Seg_8296" s="T979">amno-laʔbə-jəʔ</ta>
            <ta e="T981" id="Seg_8297" s="T980">tʼăbaktər-laʔbə-jəʔ</ta>
            <ta e="T982" id="Seg_8298" s="T981">kakənar-laʔbə-jəʔ</ta>
            <ta e="T983" id="Seg_8299" s="T982">kan-žə-bAʔ</ta>
            <ta e="T984" id="Seg_8300" s="T983">pĭde-zittə</ta>
            <ta e="T985" id="Seg_8301" s="T984">ato</ta>
            <ta e="T986" id="Seg_8302" s="T985">šišəge</ta>
            <ta e="T987" id="Seg_8303" s="T986">mo-lV-j</ta>
            <ta e="T990" id="Seg_8304" s="T989">miʔnʼibeʔ</ta>
            <ta e="T991" id="Seg_8305" s="T990">nadə</ta>
            <ta e="T992" id="Seg_8306" s="T991">pĭde-zittə</ta>
            <ta e="T993" id="Seg_8307" s="T992">tănan</ta>
            <ta e="T994" id="Seg_8308" s="T993">tănan</ta>
            <ta e="T995" id="Seg_8309" s="T994">üge</ta>
            <ta e="T996" id="Seg_8310" s="T995">iʔgö</ta>
            <ta e="T997" id="Seg_8311" s="T996">kereʔ</ta>
            <ta e="T998" id="Seg_8312" s="T997">dĭgəttə</ta>
            <ta e="T999" id="Seg_8313" s="T998">uda-zAŋ-də</ta>
            <ta e="T1000" id="Seg_8314" s="T999">bar</ta>
            <ta e="T1001" id="Seg_8315" s="T1000">kăn-laʔbə-jəʔ</ta>
            <ta e="T1002" id="Seg_8316" s="T1001">sĭri</ta>
            <ta e="T1003" id="Seg_8317" s="T1002">saʔmə-luʔbdə-lV-j</ta>
            <ta e="T1004" id="Seg_8318" s="T1003">pa</ta>
            <ta e="T1005" id="Seg_8319" s="T1004">bazoʔ</ta>
            <ta e="T1006" id="Seg_8320" s="T1005">miʔ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_8321" s="T0">I.NOM</ta>
            <ta e="T2" id="Seg_8322" s="T1">today</ta>
            <ta e="T3" id="Seg_8323" s="T2">you.PL.ACC</ta>
            <ta e="T5" id="Seg_8324" s="T4">scold-PST-1SG</ta>
            <ta e="T6" id="Seg_8325" s="T5">come-PST-2PL</ta>
            <ta e="T7" id="Seg_8326" s="T6">speak-INF.LAT</ta>
            <ta e="T8" id="Seg_8327" s="T7">and</ta>
            <ta e="T9" id="Seg_8328" s="T8">what.[NOM.SG]=INDEF</ta>
            <ta e="T10" id="Seg_8329" s="T9">NEG</ta>
            <ta e="T11" id="Seg_8330" s="T10">listen-DUR-2PL</ta>
            <ta e="T12" id="Seg_8331" s="T11">NEG</ta>
            <ta e="T14" id="Seg_8332" s="T13">NEG</ta>
            <ta e="T15" id="Seg_8333" s="T14">say-IPFVZ-PRS-2PL</ta>
            <ta e="T17" id="Seg_8334" s="T16">today</ta>
            <ta e="T18" id="Seg_8335" s="T17">speak-PST.[3SG]</ta>
            <ta e="T19" id="Seg_8336" s="T18">good</ta>
            <ta e="T21" id="Seg_8337" s="T20">speak-PST.[3SG]</ta>
            <ta e="T22" id="Seg_8338" s="T21">I.NOM</ta>
            <ta e="T23" id="Seg_8339" s="T22">listen-PST-1SG</ta>
            <ta e="T24" id="Seg_8340" s="T23">and</ta>
            <ta e="T25" id="Seg_8341" s="T24">NEG</ta>
            <ta e="T26" id="Seg_8342" s="T25">PTCL</ta>
            <ta e="T28" id="Seg_8343" s="T27">always</ta>
            <ta e="T29" id="Seg_8344" s="T28">work-PRS-1SG</ta>
            <ta e="T30" id="Seg_8345" s="T29">listen-INF.LAT</ta>
            <ta e="T32" id="Seg_8346" s="T31">how=INDEF</ta>
            <ta e="T33" id="Seg_8347" s="T32">NEG</ta>
            <ta e="T34" id="Seg_8348" s="T33">can-PRS-1SG</ta>
            <ta e="T35" id="Seg_8349" s="T34">who.[NOM.SG]=INDEF</ta>
            <ta e="T36" id="Seg_8350" s="T35">speak-DUR.[3SG]</ta>
            <ta e="T37" id="Seg_8351" s="T36">white-INS</ta>
            <ta e="T38" id="Seg_8352" s="T37">you.NOM</ta>
            <ta e="T39" id="Seg_8353" s="T38">lie.down-IMP.2SG</ta>
            <ta e="T40" id="Seg_8354" s="T39">place-LAT</ta>
            <ta e="T41" id="Seg_8355" s="T40">lie-IMP.2SG</ta>
            <ta e="T42" id="Seg_8356" s="T41">NEG.AUX-IMP.2SG</ta>
            <ta e="T43" id="Seg_8357" s="T42">speak-EP-CNG</ta>
            <ta e="T44" id="Seg_8358" s="T43">NEG.AUX-IMP.2SG</ta>
            <ta e="T45" id="Seg_8359" s="T44">shout-EP-CNG</ta>
            <ta e="T46" id="Seg_8360" s="T45">and</ta>
            <ta e="T47" id="Seg_8361" s="T46">what.[NOM.SG]</ta>
            <ta e="T48" id="Seg_8362" s="T47">I.NOM</ta>
            <ta e="T49" id="Seg_8363" s="T48">NEG</ta>
            <ta e="T50" id="Seg_8364" s="T49">speak-FUT-1SG</ta>
            <ta e="T51" id="Seg_8365" s="T50">and</ta>
            <ta e="T52" id="Seg_8366" s="T51">NEG</ta>
            <ta e="T53" id="Seg_8367" s="T52">shout-FUT-1SG</ta>
            <ta e="T54" id="Seg_8368" s="T53">and</ta>
            <ta e="T56" id="Seg_8369" s="T55">long.time</ta>
            <ta e="T57" id="Seg_8370" s="T56">live-FUT-2SG</ta>
            <ta e="T58" id="Seg_8371" s="T57">NEG</ta>
            <ta e="T59" id="Seg_8372" s="T58">die-FUT-2SG</ta>
            <ta e="T60" id="Seg_8373" s="T59">good</ta>
            <ta e="T61" id="Seg_8374" s="T60">become-FUT-3SG</ta>
            <ta e="T62" id="Seg_8375" s="T61">you.DAT</ta>
            <ta e="T63" id="Seg_8376" s="T62">lie-IMP.2SG</ta>
            <ta e="T64" id="Seg_8377" s="T63">lie-IMP.2SG</ta>
            <ta e="T65" id="Seg_8378" s="T64">then</ta>
            <ta e="T66" id="Seg_8379" s="T65">who.[NOM.SG]=INDEF</ta>
            <ta e="T67" id="Seg_8380" s="T66">PTCL</ta>
            <ta e="T68" id="Seg_8381" s="T67">fly-MOM-PST.[3SG]</ta>
            <ta e="T69" id="Seg_8382" s="T68">this-ACC</ta>
            <ta e="T70" id="Seg_8383" s="T69">and</ta>
            <ta e="T71" id="Seg_8384" s="T70">%%-PST.[3SG]</ta>
            <ta e="T72" id="Seg_8385" s="T71">this-ACC</ta>
            <ta e="T73" id="Seg_8386" s="T72">then</ta>
            <ta e="T74" id="Seg_8387" s="T73">cold.[NOM.SG]</ta>
            <ta e="T75" id="Seg_8388" s="T74">this-ACC</ta>
            <ta e="T76" id="Seg_8389" s="T75">very</ta>
            <ta e="T77" id="Seg_8390" s="T76">strongly</ta>
            <ta e="T78" id="Seg_8391" s="T77">freeze-PST.[3SG]</ta>
            <ta e="T79" id="Seg_8392" s="T78">freeze-PST.[3SG]</ta>
            <ta e="T80" id="Seg_8393" s="T79">and</ta>
            <ta e="T81" id="Seg_8394" s="T80">then</ta>
            <ta e="T82" id="Seg_8395" s="T81">warm.[NOM.SG]</ta>
            <ta e="T83" id="Seg_8396" s="T82">become-RES-PST.[3SG]</ta>
            <ta e="T84" id="Seg_8397" s="T83">sun.[NOM.SG]</ta>
            <ta e="T85" id="Seg_8398" s="T84">get.up-PST.[3SG]</ta>
            <ta e="T86" id="Seg_8399" s="T85">up</ta>
            <ta e="T87" id="Seg_8400" s="T86">get.up-PST.[3SG]</ta>
            <ta e="T88" id="Seg_8401" s="T87">snow.[NOM.SG]</ta>
            <ta e="T89" id="Seg_8402" s="T88">PTCL</ta>
            <ta e="T90" id="Seg_8403" s="T89">frighten-MOM-PST.[3SG]</ta>
            <ta e="T91" id="Seg_8404" s="T90">and</ta>
            <ta e="T92" id="Seg_8405" s="T91">then</ta>
            <ta e="T93" id="Seg_8406" s="T92">PTCL</ta>
            <ta e="T97" id="Seg_8407" s="T96">then</ta>
            <ta e="T98" id="Seg_8408" s="T97">PTCL</ta>
            <ta e="T101" id="Seg_8409" s="T100">water.[NOM.SG]</ta>
            <ta e="T102" id="Seg_8410" s="T101">flow-DUR.[3SG]</ta>
            <ta e="T103" id="Seg_8411" s="T102">and</ta>
            <ta e="T104" id="Seg_8412" s="T103">snow.[NOM.SG]</ta>
            <ta e="T105" id="Seg_8413" s="T104">die-RES-PST.[3SG]</ta>
            <ta e="T107" id="Seg_8414" s="T106">PTCL</ta>
            <ta e="T108" id="Seg_8415" s="T107">get.drunk-MOM-PST.[3SG]</ta>
            <ta e="T109" id="Seg_8416" s="T108">and</ta>
            <ta e="T110" id="Seg_8417" s="T109">PTCL</ta>
            <ta e="T111" id="Seg_8418" s="T110">%%</ta>
            <ta e="T112" id="Seg_8419" s="T111">take-PST.[3SG]</ta>
            <ta e="T113" id="Seg_8420" s="T112">hand-LAT/LOC.3SG</ta>
            <ta e="T114" id="Seg_8421" s="T113">soon</ta>
            <ta e="T115" id="Seg_8422" s="T114">say-IPFVZ.[3SG]</ta>
            <ta e="T116" id="Seg_8423" s="T115">hit-MULT-FUT-1SG</ta>
            <ta e="T117" id="Seg_8424" s="T116">I.NOM</ta>
            <ta e="T118" id="Seg_8425" s="T117">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T119" id="Seg_8426" s="T118">child.[NOM.SG]</ta>
            <ta e="T120" id="Seg_8427" s="T119">bring-PST.[3SG]</ta>
            <ta e="T121" id="Seg_8428" s="T120">boy.[NOM.SG]</ta>
            <ta e="T122" id="Seg_8429" s="T121">bring-PST.[3SG]</ta>
            <ta e="T123" id="Seg_8430" s="T122">house-LOC</ta>
            <ta e="T124" id="Seg_8431" s="T123">PTCL</ta>
            <ta e="T125" id="Seg_8432" s="T124">hurt-PST.[3SG]</ta>
            <ta e="T126" id="Seg_8433" s="T125">shout-PST.[3SG]</ta>
            <ta e="T127" id="Seg_8434" s="T126">grandmother.[NOM.SG]</ta>
            <ta e="T128" id="Seg_8435" s="T127">be-PST.[3SG]</ta>
            <ta e="T129" id="Seg_8436" s="T128">this.[NOM.SG]</ta>
            <ta e="T130" id="Seg_8437" s="T129">bring-PST.[3SG]</ta>
            <ta e="T132" id="Seg_8438" s="T131">child.[NOM.SG]</ta>
            <ta e="T133" id="Seg_8439" s="T132">this.[NOM.SG]</ta>
            <ta e="T134" id="Seg_8440" s="T133">PTCL</ta>
            <ta e="T136" id="Seg_8441" s="T135">%%-PST.[3SG]</ta>
            <ta e="T137" id="Seg_8442" s="T136">bind-PST.[3SG]</ta>
            <ta e="T138" id="Seg_8443" s="T137">and</ta>
            <ta e="T139" id="Seg_8444" s="T138">wash-PST.[3SG]</ta>
            <ta e="T140" id="Seg_8445" s="T139">then</ta>
            <ta e="T144" id="Seg_8446" s="T143">then</ta>
            <ta e="T145" id="Seg_8447" s="T144">find-PST.[3SG]</ta>
            <ta e="T146" id="Seg_8448" s="T145">rag-PL</ta>
            <ta e="T147" id="Seg_8449" s="T146">plait-PST.[3SG]</ta>
            <ta e="T148" id="Seg_8450" s="T147">this-ACC</ta>
            <ta e="T149" id="Seg_8451" s="T148">and</ta>
            <ta e="T150" id="Seg_8452" s="T149">put-PST.[3SG]</ta>
            <ta e="T151" id="Seg_8453" s="T150">stove-LAT</ta>
            <ta e="T152" id="Seg_8454" s="T151">this.[NOM.SG]</ta>
            <ta e="T153" id="Seg_8455" s="T152">PTCL</ta>
            <ta e="T154" id="Seg_8456" s="T153">lie-DUR.[3SG]</ta>
            <ta e="T155" id="Seg_8457" s="T154">and</ta>
            <ta e="T156" id="Seg_8458" s="T155">this-ACC</ta>
            <ta e="T158" id="Seg_8459" s="T157">name-DUR-3PL</ta>
            <ta e="T159" id="Seg_8460" s="T158">then</ta>
            <ta e="T160" id="Seg_8461" s="T159">bring-PST-3PL</ta>
            <ta e="T161" id="Seg_8462" s="T160">priest-LAT</ta>
            <ta e="T162" id="Seg_8463" s="T161">and</ta>
            <ta e="T163" id="Seg_8464" s="T162">name-DUR-3PL</ta>
            <ta e="T165" id="Seg_8465" s="T164">and</ta>
            <ta e="T166" id="Seg_8466" s="T165">I.NOM</ta>
            <ta e="T167" id="Seg_8467" s="T166">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T168" id="Seg_8468" s="T167">this-ACC</ta>
            <ta e="T169" id="Seg_8469" s="T168">bring-PST.[3SG]</ta>
            <ta e="T170" id="Seg_8470" s="T169">I.LAT</ta>
            <ta e="T171" id="Seg_8471" s="T170">ten.[NOM.SG]</ta>
            <ta e="T172" id="Seg_8472" s="T171">two.[NOM.SG]</ta>
            <ta e="T173" id="Seg_8473" s="T172">winter.[NOM.SG]</ta>
            <ta e="T174" id="Seg_8474" s="T173">be-PST.[3SG]</ta>
            <ta e="T175" id="Seg_8475" s="T174">then</ta>
            <ta e="T176" id="Seg_8476" s="T175">this</ta>
            <ta e="T178" id="Seg_8477" s="T177">grow-PST.[3SG]</ta>
            <ta e="T179" id="Seg_8478" s="T178">grow-PST.[3SG]</ta>
            <ta e="T180" id="Seg_8479" s="T179">ten.[NOM.SG]</ta>
            <ta e="T181" id="Seg_8480" s="T180">two.[NOM.SG]</ta>
            <ta e="T182" id="Seg_8481" s="T181">ten.[NOM.SG]</ta>
            <ta e="T183" id="Seg_8482" s="T182">two.[NOM.SG]</ta>
            <ta e="T184" id="Seg_8483" s="T183">winter.[NOM.SG]</ta>
            <ta e="T185" id="Seg_8484" s="T184">be-PST.[3SG]</ta>
            <ta e="T186" id="Seg_8485" s="T185">mother-NOM/GEN.3SG</ta>
            <ta e="T187" id="Seg_8486" s="T186">die-RES-PST.[3SG]</ta>
            <ta e="T188" id="Seg_8487" s="T187">this.[NOM.SG]</ta>
            <ta e="T189" id="Seg_8488" s="T188">remain-PST.[3SG]</ta>
            <ta e="T190" id="Seg_8489" s="T189">and</ta>
            <ta e="T193" id="Seg_8490" s="T192">mother-ACC</ta>
            <ta e="T194" id="Seg_8491" s="T193">kill-PST-3PL</ta>
            <ta e="T195" id="Seg_8492" s="T194">this-ACC</ta>
            <ta e="T196" id="Seg_8493" s="T195">kill-PST.[3SG]</ta>
            <ta e="T198" id="Seg_8494" s="T197">this-GEN</ta>
            <ta e="T199" id="Seg_8495" s="T198">and</ta>
            <ta e="T200" id="Seg_8496" s="T199">this.[NOM.SG]</ta>
            <ta e="T201" id="Seg_8497" s="T200">PTCL</ta>
            <ta e="T202" id="Seg_8498" s="T201">die-RES-PST.[3SG]</ta>
            <ta e="T203" id="Seg_8499" s="T202">horse-INS</ta>
            <ta e="T204" id="Seg_8500" s="T203">house-LAT/LOC.3SG</ta>
            <ta e="T205" id="Seg_8501" s="T204">bring-PST-3PL</ta>
            <ta e="T206" id="Seg_8502" s="T205">I.NOM</ta>
            <ta e="T207" id="Seg_8503" s="T206">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T208" id="Seg_8504" s="T207">forest-LOC</ta>
            <ta e="T209" id="Seg_8505" s="T208">die-RES-PST.[3SG]</ta>
            <ta e="T210" id="Seg_8506" s="T209">then</ta>
            <ta e="T211" id="Seg_8507" s="T210">sister-NOM/GEN/ACC.1SG</ta>
            <ta e="T212" id="Seg_8508" s="T211">and</ta>
            <ta e="T213" id="Seg_8509" s="T212">I.NOM</ta>
            <ta e="T214" id="Seg_8510" s="T213">man-NOM/GEN/ACC.1SG</ta>
            <ta e="T215" id="Seg_8511" s="T214">two.[NOM.SG]</ta>
            <ta e="T216" id="Seg_8512" s="T215">horse-DU-3SG-INS</ta>
            <ta e="T217" id="Seg_8513" s="T216">bring-PST-3PL</ta>
            <ta e="T218" id="Seg_8514" s="T217">house-LAT/LOC.1DU</ta>
            <ta e="T219" id="Seg_8515" s="T218">I.NOM</ta>
            <ta e="T220" id="Seg_8516" s="T219">very</ta>
            <ta e="T221" id="Seg_8517" s="T220">strongly</ta>
            <ta e="T222" id="Seg_8518" s="T221">cry-PST-1SG</ta>
            <ta e="T223" id="Seg_8519" s="T222">big.[NOM.SG]</ta>
            <ta e="T224" id="Seg_8520" s="T223">day.[NOM.SG]</ta>
            <ta e="T225" id="Seg_8521" s="T224">be-PST.[3SG]</ta>
            <ta e="T227" id="Seg_8522" s="T226">then</ta>
            <ta e="T228" id="Seg_8523" s="T227">earth.[NOM.SG]</ta>
            <ta e="T229" id="Seg_8524" s="T228">earth.[NOM.SG]</ta>
            <ta e="T230" id="Seg_8525" s="T229">dig-PST-1PL</ta>
            <ta e="T231" id="Seg_8526" s="T230">house.[NOM.SG]</ta>
            <ta e="T232" id="Seg_8527" s="T231">make-PST-1PL</ta>
            <ta e="T233" id="Seg_8528" s="T232">cross.[NOM.SG]</ta>
            <ta e="T234" id="Seg_8529" s="T233">make-PST-1PL</ta>
            <ta e="T235" id="Seg_8530" s="T234">and</ta>
            <ta e="T236" id="Seg_8531" s="T235">place-LAT</ta>
            <ta e="T237" id="Seg_8532" s="T236">put-PST-1PL</ta>
            <ta e="T238" id="Seg_8533" s="T237">pour-PST-1PL</ta>
            <ta e="T239" id="Seg_8534" s="T238">then</ta>
            <ta e="T240" id="Seg_8535" s="T239">water.[NOM.SG]</ta>
            <ta e="T241" id="Seg_8536" s="T240">drink-PST-1PL</ta>
            <ta e="T242" id="Seg_8537" s="T241">bread.[NOM.SG]</ta>
            <ta e="T243" id="Seg_8538" s="T242">eat-PST-1PL</ta>
            <ta e="T244" id="Seg_8539" s="T243">people.[NOM.SG]</ta>
            <ta e="T245" id="Seg_8540" s="T244">be-PST-3PL</ta>
            <ta e="T246" id="Seg_8541" s="T245">enough</ta>
            <ta e="T247" id="Seg_8542" s="T246">two.[NOM.SG]</ta>
            <ta e="T248" id="Seg_8543" s="T247">Kamassian-PL</ta>
            <ta e="T249" id="Seg_8544" s="T248">live-PST-3PL</ta>
            <ta e="T250" id="Seg_8545" s="T249">woman.[NOM.SG]</ta>
            <ta e="T251" id="Seg_8546" s="T250">and</ta>
            <ta e="T252" id="Seg_8547" s="T251">man.[NOM.SG]</ta>
            <ta e="T253" id="Seg_8548" s="T252">vodka.[NOM.SG]</ta>
            <ta e="T254" id="Seg_8549" s="T253">drink-PST-3PL</ta>
            <ta e="T255" id="Seg_8550" s="T254">PTCL</ta>
            <ta e="T256" id="Seg_8551" s="T255">fight-PST-3PL</ta>
            <ta e="T257" id="Seg_8552" s="T256">then</ta>
            <ta e="T258" id="Seg_8553" s="T257">threshold-LAT/LOC.3SG</ta>
            <ta e="T259" id="Seg_8554" s="T258">sit.down-PST-3PL</ta>
            <ta e="T260" id="Seg_8555" s="T259">this.[NOM.SG]</ta>
            <ta e="T261" id="Seg_8556" s="T260">head-LAT/LOC.3SG</ta>
            <ta e="T262" id="Seg_8557" s="T261">four.[NOM.SG]</ta>
            <ta e="T263" id="Seg_8558" s="T262">be-PST.[3SG]</ta>
            <ta e="T264" id="Seg_8559" s="T263">woman.[NOM.SG]</ta>
            <ta e="T265" id="Seg_8560" s="T264">and</ta>
            <ta e="T266" id="Seg_8561" s="T265">man-NOM/GEN.3SG</ta>
            <ta e="T267" id="Seg_8562" s="T266">blow-%%-DUR-PST.[3SG]</ta>
            <ta e="T268" id="Seg_8563" s="T267">%%-%%-DUR-PST.[3SG]</ta>
            <ta e="T269" id="Seg_8564" s="T268">and</ta>
            <ta e="T270" id="Seg_8565" s="T269">so</ta>
            <ta e="T271" id="Seg_8566" s="T270">sleep-MOM-PST-3PL</ta>
            <ta e="T272" id="Seg_8567" s="T271">morning-LOC.ADV</ta>
            <ta e="T273" id="Seg_8568" s="T272">get.up-PST-3PL</ta>
            <ta e="T274" id="Seg_8569" s="T273">I.NOM</ta>
            <ta e="T275" id="Seg_8570" s="T274">what.[NOM.SG]=INDEF</ta>
            <ta e="T276" id="Seg_8571" s="T275">PTCL</ta>
            <ta e="T277" id="Seg_8572" s="T276">hurt-PRS-1SG</ta>
            <ta e="T278" id="Seg_8573" s="T277">and</ta>
            <ta e="T279" id="Seg_8574" s="T278">this</ta>
            <ta e="T280" id="Seg_8575" s="T279">say-IPFVZ.[3SG]</ta>
            <ta e="T281" id="Seg_8576" s="T280">I.NOM</ta>
            <ta e="T282" id="Seg_8577" s="T281">also</ta>
            <ta e="T283" id="Seg_8578" s="T282">hurt-PRS-1SG</ta>
            <ta e="T284" id="Seg_8579" s="T283">probably</ta>
            <ta e="T285" id="Seg_8580" s="T284">we.NOM</ta>
            <ta e="T286" id="Seg_8581" s="T285">fight-PST-1PL</ta>
            <ta e="T288" id="Seg_8582" s="T287">this-ACC</ta>
            <ta e="T289" id="Seg_8583" s="T288">call-PST-3PL</ta>
            <ta e="T293" id="Seg_8584" s="T292">and</ta>
            <ta e="T294" id="Seg_8585" s="T293">woman-NOM/GEN/ACC.1SG</ta>
            <ta e="T295" id="Seg_8586" s="T294">call-PST-3PL</ta>
            <ta e="T299" id="Seg_8587" s="T298">this-GEN.PL</ta>
            <ta e="T300" id="Seg_8588" s="T299">house-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T301" id="Seg_8589" s="T300">small.[NOM.SG]</ta>
            <ta e="T302" id="Seg_8590" s="T301">be-PST-3PL</ta>
            <ta e="T303" id="Seg_8591" s="T302">and</ta>
            <ta e="T304" id="Seg_8592" s="T303">there</ta>
            <ta e="T305" id="Seg_8593" s="T304">%%-LOC</ta>
            <ta e="T306" id="Seg_8594" s="T305">stand-CVB-PST.[3SG]</ta>
            <ta e="T308" id="Seg_8595" s="T307">this-GEN</ta>
            <ta e="T309" id="Seg_8596" s="T308">only</ta>
            <ta e="T310" id="Seg_8597" s="T309">sister-NOM/GEN.3SG</ta>
            <ta e="T311" id="Seg_8598" s="T310">be-PST.[3SG]</ta>
            <ta e="T312" id="Seg_8599" s="T311">call-PST-3PL</ta>
            <ta e="T313" id="Seg_8600" s="T312">Afanasia-PL-INS</ta>
            <ta e="T314" id="Seg_8601" s="T313">child-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T315" id="Seg_8602" s="T314">NEG.EX-PST-3PL</ta>
            <ta e="T316" id="Seg_8603" s="T315">so</ta>
            <ta e="T317" id="Seg_8604" s="T316">PTCL</ta>
            <ta e="T318" id="Seg_8605" s="T317">all</ta>
            <ta e="T319" id="Seg_8606" s="T318">die-RES-PST-3PL</ta>
            <ta e="T321" id="Seg_8607" s="T320">all</ta>
            <ta e="T322" id="Seg_8608" s="T321">three.[NOM.SG]</ta>
            <ta e="T323" id="Seg_8609" s="T322">die-RES-PST-3PL</ta>
            <ta e="T327" id="Seg_8610" s="T326">sister-NOM/GEN.3SG</ta>
            <ta e="T328" id="Seg_8611" s="T327">what.[NOM.SG]</ta>
            <ta e="T329" id="Seg_8612" s="T328">you.PL.NOM</ta>
            <ta e="T330" id="Seg_8613" s="T329">fight-PST-2PL</ta>
            <ta e="T331" id="Seg_8614" s="T330">and</ta>
            <ta e="T332" id="Seg_8615" s="T331">I.NOM</ta>
            <ta e="T333" id="Seg_8616" s="T332">you.PL.ACC</ta>
            <ta e="T334" id="Seg_8617" s="T333">NEG</ta>
            <ta e="T335" id="Seg_8618" s="T334">%%-PST-1SG</ta>
            <ta e="T336" id="Seg_8619" s="T335">so</ta>
            <ta e="T337" id="Seg_8620" s="T336">PTCL</ta>
            <ta e="T338" id="Seg_8621" s="T337">sleep-MOM-PST-2PL</ta>
            <ta e="T340" id="Seg_8622" s="T339">Kamassian-PL-LOC</ta>
            <ta e="T341" id="Seg_8623" s="T340">be-PST.[3SG]</ta>
            <ta e="T342" id="Seg_8624" s="T341">big.[NOM.SG]</ta>
            <ta e="T343" id="Seg_8625" s="T342">day.[NOM.SG]</ta>
            <ta e="T345" id="Seg_8626" s="T344">day.[NOM.SG]</ta>
            <ta e="T346" id="Seg_8627" s="T345">one.[NOM.SG]</ta>
            <ta e="T347" id="Seg_8628" s="T346">house-LAT</ta>
            <ta e="T348" id="Seg_8629" s="T347">collect-PRS-3PL</ta>
            <ta e="T349" id="Seg_8630" s="T348">PTCL</ta>
            <ta e="T350" id="Seg_8631" s="T349">then</ta>
            <ta e="T351" id="Seg_8632" s="T350">table-NOM/GEN/ACC.3SG</ta>
            <ta e="T352" id="Seg_8633" s="T351">collect-PRS-3PL</ta>
            <ta e="T353" id="Seg_8634" s="T352">%%-PRS-3PL</ta>
            <ta e="T354" id="Seg_8635" s="T353">and</ta>
            <ta e="T355" id="Seg_8636" s="T354">drink-DUR-3PL</ta>
            <ta e="T356" id="Seg_8637" s="T355">vodka.[NOM.SG]</ta>
            <ta e="T357" id="Seg_8638" s="T356">and</ta>
            <ta e="T358" id="Seg_8639" s="T357">eat-DUR-3PL</ta>
            <ta e="T359" id="Seg_8640" s="T358">meat.[NOM.SG]</ta>
            <ta e="T360" id="Seg_8641" s="T359">bread.[NOM.SG]</ta>
            <ta e="T361" id="Seg_8642" s="T360">what.[NOM.SG]</ta>
            <ta e="T362" id="Seg_8643" s="T361">be-PRS.[3SG]</ta>
            <ta e="T363" id="Seg_8644" s="T362">table-LOC</ta>
            <ta e="T364" id="Seg_8645" s="T363">then</ta>
            <ta e="T365" id="Seg_8646" s="T364">another.[NOM.SG]</ta>
            <ta e="T366" id="Seg_8647" s="T365">house-LAT</ta>
            <ta e="T367" id="Seg_8648" s="T366">go-FUT-3PL</ta>
            <ta e="T368" id="Seg_8649" s="T367">there</ta>
            <ta e="T369" id="Seg_8650" s="T368">so</ta>
            <ta e="T370" id="Seg_8651" s="T369">PTCL</ta>
            <ta e="T371" id="Seg_8652" s="T370">drink-FUT-3PL</ta>
            <ta e="T372" id="Seg_8653" s="T371">then</ta>
            <ta e="T373" id="Seg_8654" s="T372">more</ta>
            <ta e="T374" id="Seg_8655" s="T373">one.[NOM.SG]</ta>
            <ta e="T375" id="Seg_8656" s="T374">house-LAT</ta>
            <ta e="T376" id="Seg_8657" s="T375">then</ta>
            <ta e="T377" id="Seg_8658" s="T376">four.[NOM.SG]</ta>
            <ta e="T378" id="Seg_8659" s="T377">house-LAT</ta>
            <ta e="T379" id="Seg_8660" s="T378">five.[NOM.SG]</ta>
            <ta e="T381" id="Seg_8661" s="T380">%%</ta>
            <ta e="T382" id="Seg_8662" s="T381">five.[NOM.SG]</ta>
            <ta e="T383" id="Seg_8663" s="T382">day.[NOM.SG]</ta>
            <ta e="T384" id="Seg_8664" s="T383">drink-DUR-3PL</ta>
            <ta e="T385" id="Seg_8665" s="T384">always</ta>
            <ta e="T386" id="Seg_8666" s="T385">vodka.[NOM.SG]</ta>
            <ta e="T387" id="Seg_8667" s="T386">and</ta>
            <ta e="T388" id="Seg_8668" s="T387">then</ta>
            <ta e="T389" id="Seg_8669" s="T388">enough</ta>
            <ta e="T1011" id="Seg_8670" s="T391">Aginskoe</ta>
            <ta e="T392" id="Seg_8671" s="T1011">settlement-LOC</ta>
            <ta e="T394" id="Seg_8672" s="T393">be-PST.[3SG]</ta>
            <ta e="T395" id="Seg_8673" s="T394">this-PL</ta>
            <ta e="T396" id="Seg_8674" s="T395">there</ta>
            <ta e="T397" id="Seg_8675" s="T396">go-PST-3PL</ta>
            <ta e="T398" id="Seg_8676" s="T397">and</ta>
            <ta e="T399" id="Seg_8677" s="T398">there</ta>
            <ta e="T400" id="Seg_8678" s="T399">vodka.[NOM.SG]</ta>
            <ta e="T401" id="Seg_8679" s="T400">take-PST-3PL</ta>
            <ta e="T402" id="Seg_8680" s="T401">who-GEN</ta>
            <ta e="T403" id="Seg_8681" s="T402">money.[NOM.SG]</ta>
            <ta e="T404" id="Seg_8682" s="T403">many</ta>
            <ta e="T405" id="Seg_8683" s="T404">so</ta>
            <ta e="T406" id="Seg_8684" s="T405">many</ta>
            <ta e="T407" id="Seg_8685" s="T406">take-PRS-3PL</ta>
            <ta e="T408" id="Seg_8686" s="T407">five.[NOM.SG]</ta>
            <ta e="T410" id="Seg_8687" s="T409">and</ta>
            <ta e="T411" id="Seg_8688" s="T410">who-GEN</ta>
            <ta e="T412" id="Seg_8689" s="T411">few</ta>
            <ta e="T413" id="Seg_8690" s="T412">four.[NOM.SG]</ta>
            <ta e="T414" id="Seg_8691" s="T413">take-FUT-3SG</ta>
            <ta e="T415" id="Seg_8692" s="T414">who-GEN</ta>
            <ta e="T416" id="Seg_8693" s="T415">more</ta>
            <ta e="T417" id="Seg_8694" s="T416">few</ta>
            <ta e="T418" id="Seg_8695" s="T417">two.[NOM.SG]</ta>
            <ta e="T419" id="Seg_8696" s="T418">take-FUT-3SG</ta>
            <ta e="T420" id="Seg_8697" s="T419">and</ta>
            <ta e="T421" id="Seg_8698" s="T420">then</ta>
            <ta e="T422" id="Seg_8699" s="T421">big.[NOM.SG]</ta>
            <ta e="T423" id="Seg_8700" s="T422">day-LOC</ta>
            <ta e="T424" id="Seg_8701" s="T423">drink-PST-3PL</ta>
            <ta e="T425" id="Seg_8702" s="T424">small.[NOM.SG]</ta>
            <ta e="T426" id="Seg_8703" s="T425">such.[NOM.SG]</ta>
            <ta e="T427" id="Seg_8704" s="T426">goblet-LAT</ta>
            <ta e="T428" id="Seg_8705" s="T427">pour-PST-3PL</ta>
            <ta e="T429" id="Seg_8706" s="T428">one-LAT</ta>
            <ta e="T430" id="Seg_8707" s="T429">give-FUT-3SG</ta>
            <ta e="T431" id="Seg_8708" s="T430">then</ta>
            <ta e="T432" id="Seg_8709" s="T431">again</ta>
            <ta e="T433" id="Seg_8710" s="T432">so</ta>
            <ta e="T434" id="Seg_8711" s="T433">so</ta>
            <ta e="T435" id="Seg_8712" s="T434">so</ta>
            <ta e="T436" id="Seg_8713" s="T435">give-PRS.[3SG]</ta>
            <ta e="T437" id="Seg_8714" s="T436">give-PRS.[3SG]</ta>
            <ta e="T438" id="Seg_8715" s="T437">PTCL</ta>
            <ta e="T439" id="Seg_8716" s="T438">%%</ta>
            <ta e="T440" id="Seg_8717" s="T439">then</ta>
            <ta e="T441" id="Seg_8718" s="T440">eat-DUR-3PL</ta>
            <ta e="T442" id="Seg_8719" s="T441">eat-DUR-3PL</ta>
            <ta e="T443" id="Seg_8720" s="T442">PTCL</ta>
            <ta e="T444" id="Seg_8721" s="T443">speak-DUR-3PL</ta>
            <ta e="T445" id="Seg_8722" s="T444">song.[NOM.SG]</ta>
            <ta e="T446" id="Seg_8723" s="T445">sing-DUR-3PL</ta>
            <ta e="T451" id="Seg_8724" s="T450">go-PST.[3SG]</ta>
            <ta e="T453" id="Seg_8725" s="T451">Aginskoe-LAT</ta>
            <ta e="T455" id="Seg_8726" s="T454">be-PST.[3SG]</ta>
            <ta e="T456" id="Seg_8727" s="T455">big.[NOM.SG]</ta>
            <ta e="T457" id="Seg_8728" s="T456">day.[NOM.SG]</ta>
            <ta e="T458" id="Seg_8729" s="T457">there</ta>
            <ta e="T459" id="Seg_8730" s="T458">vodka.[NOM.SG]</ta>
            <ta e="T460" id="Seg_8731" s="T459">drink-PST-3PL</ta>
            <ta e="T461" id="Seg_8732" s="T460">PTCL</ta>
            <ta e="T462" id="Seg_8733" s="T461">each</ta>
            <ta e="T463" id="Seg_8734" s="T462">house-LAT</ta>
            <ta e="T464" id="Seg_8735" s="T463">go-PST-3PL</ta>
            <ta e="T465" id="Seg_8736" s="T464">this.[NOM.SG]</ta>
            <ta e="T466" id="Seg_8737" s="T465">evening-LOC.ADV</ta>
            <ta e="T1010" id="Seg_8738" s="T466">go-CVB</ta>
            <ta e="T467" id="Seg_8739" s="T1010">disappear-PST.[3SG]</ta>
            <ta e="T468" id="Seg_8740" s="T467">Anzha-LAT</ta>
            <ta e="T469" id="Seg_8741" s="T468">and</ta>
            <ta e="T470" id="Seg_8742" s="T469">there</ta>
            <ta e="T471" id="Seg_8743" s="T470">freeze-RES-PST.[3SG]</ta>
            <ta e="T472" id="Seg_8744" s="T471">die-RES-PST.[3SG]</ta>
            <ta e="T474" id="Seg_8745" s="T473">this.[NOM.SG]</ta>
            <ta e="T475" id="Seg_8746" s="T474">house-LAT/LOC.3SG</ta>
            <ta e="T477" id="Seg_8747" s="T476">house-LAT/LOC.3SG</ta>
            <ta e="T478" id="Seg_8748" s="T477">come-PST.[3SG]</ta>
            <ta e="T479" id="Seg_8749" s="T478">outwards</ta>
            <ta e="T480" id="Seg_8750" s="T479">two.[NOM.SG]</ta>
            <ta e="T481" id="Seg_8751" s="T480">man.[NOM.SG]</ta>
            <ta e="T482" id="Seg_8752" s="T481">this-LAT</ta>
            <ta e="T483" id="Seg_8753" s="T482">come-PST-3PL</ta>
            <ta e="T484" id="Seg_8754" s="T483">go-OPT.DU/PL-1DU</ta>
            <ta e="T486" id="Seg_8755" s="T485">I.NOM-INS</ta>
            <ta e="T487" id="Seg_8756" s="T486">then</ta>
            <ta e="T488" id="Seg_8757" s="T487">go-PST.[3SG]</ta>
            <ta e="T489" id="Seg_8758" s="T488">go-PST.[3SG]</ta>
            <ta e="T490" id="Seg_8759" s="T489">see-PRS-3SG.O</ta>
            <ta e="T491" id="Seg_8760" s="T490">PTCL</ta>
            <ta e="T492" id="Seg_8761" s="T491">%%</ta>
            <ta e="T493" id="Seg_8762" s="T492">PTCL</ta>
            <ta e="T495" id="Seg_8763" s="T494">then</ta>
            <ta e="T496" id="Seg_8764" s="T495">God-LAT</ta>
            <ta e="T498" id="Seg_8765" s="T497">bow-PST.[3SG]</ta>
            <ta e="T499" id="Seg_8766" s="T498">from.there</ta>
            <ta e="T500" id="Seg_8767" s="T499">people-NOM/GEN/ACC.3SG</ta>
            <ta e="T501" id="Seg_8768" s="T500">run-MOM-PST-3PL</ta>
            <ta e="T502" id="Seg_8769" s="T501">and</ta>
            <ta e="T503" id="Seg_8770" s="T502">this.[NOM.SG]</ta>
            <ta e="T504" id="Seg_8771" s="T503">house-LAT/LOC.3SG</ta>
            <ta e="T505" id="Seg_8772" s="T504">come-PST.[3SG]</ta>
            <ta e="T506" id="Seg_8773" s="T505">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T507" id="Seg_8774" s="T506">snow.[NOM.SG]</ta>
            <ta e="T508" id="Seg_8775" s="T507">become-PST.[3SG]</ta>
            <ta e="T510" id="Seg_8776" s="T509">this-PL</ta>
            <ta e="T511" id="Seg_8777" s="T510">very</ta>
            <ta e="T512" id="Seg_8778" s="T511">vodka.[NOM.SG]</ta>
            <ta e="T513" id="Seg_8779" s="T512">many</ta>
            <ta e="T514" id="Seg_8780" s="T513">drink-DUR-3PL</ta>
            <ta e="T515" id="Seg_8781" s="T514">how.much</ta>
            <ta e="T516" id="Seg_8782" s="T515">although</ta>
            <ta e="T518" id="Seg_8783" s="T517">always</ta>
            <ta e="T519" id="Seg_8784" s="T518">drink-DUR-3PL</ta>
            <ta e="T520" id="Seg_8785" s="T519">drink-DUR-3PL</ta>
            <ta e="T521" id="Seg_8786" s="T520">more</ta>
            <ta e="T522" id="Seg_8787" s="T521">look.for-DUR-3PL</ta>
            <ta e="T523" id="Seg_8788" s="T522">where</ta>
            <ta e="T524" id="Seg_8789" s="T523">be-PRS.[3SG]</ta>
            <ta e="T525" id="Seg_8790" s="T524">so</ta>
            <ta e="T526" id="Seg_8791" s="T525">there</ta>
            <ta e="T527" id="Seg_8792" s="T526">run-MOM.PRS-3PL</ta>
            <ta e="T528" id="Seg_8793" s="T527">PTCL</ta>
            <ta e="T530" id="Seg_8794" s="T529">then</ta>
            <ta e="T531" id="Seg_8795" s="T530">PTCL</ta>
            <ta e="T532" id="Seg_8796" s="T531">sing-DUR-3PL</ta>
            <ta e="T533" id="Seg_8797" s="T532">jump-DUR-3PL</ta>
            <ta e="T534" id="Seg_8798" s="T533">then</ta>
            <ta e="T535" id="Seg_8799" s="T534">PTCL</ta>
            <ta e="T536" id="Seg_8800" s="T535">fight-DUR-3PL</ta>
            <ta e="T538" id="Seg_8801" s="T537">who.[NOM.SG]</ta>
            <ta e="T539" id="Seg_8802" s="T538">PTCL</ta>
            <ta e="T540" id="Seg_8803" s="T539">many</ta>
            <ta e="T542" id="Seg_8804" s="T541">drink-PRS.[3SG]</ta>
            <ta e="T543" id="Seg_8805" s="T542">drink-PRS.[3SG]</ta>
            <ta e="T544" id="Seg_8806" s="T543">then</ta>
            <ta e="T545" id="Seg_8807" s="T544">vodka-ABL</ta>
            <ta e="T546" id="Seg_8808" s="T545">PTCL</ta>
            <ta e="T547" id="Seg_8809" s="T546">burn-DUR.[3SG]</ta>
            <ta e="T548" id="Seg_8810" s="T547">this</ta>
            <ta e="T549" id="Seg_8811" s="T548">this-LAT</ta>
            <ta e="T550" id="Seg_8812" s="T549">mouth-LAT</ta>
            <ta e="T551" id="Seg_8813" s="T550">PTCL</ta>
            <ta e="T552" id="Seg_8814" s="T551">piss-DUR-3PL</ta>
            <ta e="T553" id="Seg_8815" s="T552">then</ta>
            <ta e="T554" id="Seg_8816" s="T553">this.[NOM.SG]</ta>
            <ta e="T556" id="Seg_8817" s="T555">get.up-FUT-3SG</ta>
            <ta e="T558" id="Seg_8818" s="T557">one.[NOM.SG]</ta>
            <ta e="T559" id="Seg_8819" s="T558">one.[NOM.SG]</ta>
            <ta e="T560" id="Seg_8820" s="T559">I.NOM</ta>
            <ta e="T561" id="Seg_8821" s="T560">ear-NOM/GEN/ACC.1SG</ta>
            <ta e="T562" id="Seg_8822" s="T561">hurt-MOM-PST.[3SG]</ta>
            <ta e="T563" id="Seg_8823" s="T562">PTCL</ta>
            <ta e="T564" id="Seg_8824" s="T563">there</ta>
            <ta e="T565" id="Seg_8825" s="T564">punch-DUR.[3SG]</ta>
            <ta e="T566" id="Seg_8826" s="T565">I.NOM</ta>
            <ta e="T567" id="Seg_8827" s="T566">then</ta>
            <ta e="T568" id="Seg_8828" s="T567">find-PST-1SG</ta>
            <ta e="T569" id="Seg_8829" s="T568">rag.[NOM.SG]</ta>
            <ta e="T570" id="Seg_8830" s="T569">piss-PST-1SG</ta>
            <ta e="T571" id="Seg_8831" s="T570">this-ABL</ta>
            <ta e="T573" id="Seg_8832" s="T572">even</ta>
            <ta e="T574" id="Seg_8833" s="T573">flow-MOM-PST.[3SG]</ta>
            <ta e="T575" id="Seg_8834" s="T574">I.NOM</ta>
            <ta e="T576" id="Seg_8835" s="T575">ear-LAT</ta>
            <ta e="T577" id="Seg_8836" s="T576">put-PST-1SG</ta>
            <ta e="T580" id="Seg_8837" s="T579">then</ta>
            <ta e="T581" id="Seg_8838" s="T580">bind-PST-1SG</ta>
            <ta e="T582" id="Seg_8839" s="T581">lie.down-PST-1SG</ta>
            <ta e="T583" id="Seg_8840" s="T582">sleep-MOM-PST-1SG</ta>
            <ta e="T584" id="Seg_8841" s="T583">get.up-PST-1SG</ta>
            <ta e="T586" id="Seg_8842" s="T585">ear-NOM/GEN/ACC.1SG</ta>
            <ta e="T587" id="Seg_8843" s="T586">NEG</ta>
            <ta e="T588" id="Seg_8844" s="T587">hurt-PRS.[3SG]</ta>
            <ta e="T589" id="Seg_8845" s="T588">and</ta>
            <ta e="T590" id="Seg_8846" s="T589">then</ta>
            <ta e="T591" id="Seg_8847" s="T590">head-NOM/GEN/ACC.1SG</ta>
            <ta e="T592" id="Seg_8848" s="T591">hurt-PRS.[3SG]</ta>
            <ta e="T593" id="Seg_8849" s="T592">I.NOM</ta>
            <ta e="T594" id="Seg_8850" s="T593">also</ta>
            <ta e="T595" id="Seg_8851" s="T594">piss-FUT-1SG</ta>
            <ta e="T596" id="Seg_8852" s="T595">rag-3PL-LAT</ta>
            <ta e="T597" id="Seg_8853" s="T596">head-LAT/LOC.3SG</ta>
            <ta e="T598" id="Seg_8854" s="T597">bind-FUT-1SG</ta>
            <ta e="T599" id="Seg_8855" s="T598">lie.down-FUT-1SG</ta>
            <ta e="T600" id="Seg_8856" s="T599">this.[NOM.SG]</ta>
            <ta e="T602" id="Seg_8857" s="T601">become.dry-MOM-FUT-3SG</ta>
            <ta e="T603" id="Seg_8858" s="T602">this</ta>
            <ta e="T604" id="Seg_8859" s="T603">rag.[NOM.SG]</ta>
            <ta e="T605" id="Seg_8860" s="T604">head-NOM/GEN/ACC.1SG</ta>
            <ta e="T606" id="Seg_8861" s="T605">NEG</ta>
            <ta e="T607" id="Seg_8862" s="T606">hurt-PRS.[3SG]</ta>
            <ta e="T609" id="Seg_8863" s="T608">one.[NOM.SG]</ta>
            <ta e="T610" id="Seg_8864" s="T609">one.[NOM.SG]</ta>
            <ta e="T611" id="Seg_8865" s="T610">time.[NOM.SG]</ta>
            <ta e="T612" id="Seg_8866" s="T611">we.NOM</ta>
            <ta e="T613" id="Seg_8867" s="T612">bread.[NOM.SG]</ta>
            <ta e="T614" id="Seg_8868" s="T613">hit-MULT-PST-1PL</ta>
            <ta e="T615" id="Seg_8869" s="T614">machine-3PL-INS</ta>
            <ta e="T616" id="Seg_8870" s="T615">there</ta>
            <ta e="T617" id="Seg_8871" s="T616">knife.[NOM.SG]</ta>
            <ta e="T618" id="Seg_8872" s="T617">be-PST.[3SG]</ta>
            <ta e="T619" id="Seg_8873" s="T618">sheave.PL-NOM/GEN/ACC.3PL</ta>
            <ta e="T621" id="Seg_8874" s="T620">stab-INF.LAT</ta>
            <ta e="T622" id="Seg_8875" s="T621">and</ta>
            <ta e="T623" id="Seg_8876" s="T622">this.[NOM.SG]</ta>
            <ta e="T625" id="Seg_8877" s="T624">how=INDEF</ta>
            <ta e="T626" id="Seg_8878" s="T625">hand-ACC.3SG</ta>
            <ta e="T627" id="Seg_8879" s="T626">PTCL</ta>
            <ta e="T628" id="Seg_8880" s="T627">cut-PST.[3SG]</ta>
            <ta e="T629" id="Seg_8881" s="T628">I.NOM</ta>
            <ta e="T630" id="Seg_8882" s="T629">say-IPFVZ-1SG</ta>
            <ta e="T631" id="Seg_8883" s="T630">piss-IMP.2SG</ta>
            <ta e="T632" id="Seg_8884" s="T631">this.[NOM.SG]</ta>
            <ta e="T633" id="Seg_8885" s="T632">piss-PST.[3SG]</ta>
            <ta e="T634" id="Seg_8886" s="T633">and</ta>
            <ta e="T635" id="Seg_8887" s="T634">bind-PST.[3SG]</ta>
            <ta e="T636" id="Seg_8888" s="T635">then</ta>
            <ta e="T637" id="Seg_8889" s="T636">NEG</ta>
            <ta e="T638" id="Seg_8890" s="T637">hurt-PST.[3SG]</ta>
            <ta e="T639" id="Seg_8891" s="T638">hand-NOM/GEN.3SG</ta>
            <ta e="T641" id="Seg_8892" s="T640">we.NOM</ta>
            <ta e="T642" id="Seg_8893" s="T641">NEG.EX-PST-3PL</ta>
            <ta e="T643" id="Seg_8894" s="T642">doctor-PL</ta>
            <ta e="T644" id="Seg_8895" s="T643">we.NOM</ta>
            <ta e="T646" id="Seg_8896" s="T645">grass-INS</ta>
            <ta e="T647" id="Seg_8897" s="T646">PTCL</ta>
            <ta e="T648" id="Seg_8898" s="T647">heal-PST-1PL</ta>
            <ta e="T649" id="Seg_8899" s="T648">boil-DUR.[3SG]</ta>
            <ta e="T650" id="Seg_8900" s="T649">then</ta>
            <ta e="T651" id="Seg_8901" s="T650">drink-DUR.[3SG]</ta>
            <ta e="T652" id="Seg_8902" s="T651">one.[NOM.SG]</ta>
            <ta e="T653" id="Seg_8903" s="T652">grass.[NOM.SG]</ta>
            <ta e="T654" id="Seg_8904" s="T653">be-PRS.[3SG]</ta>
            <ta e="T655" id="Seg_8905" s="T654">stinking.[NOM.SG]</ta>
            <ta e="T656" id="Seg_8906" s="T655">stomach.[NOM.SG]</ta>
            <ta e="T657" id="Seg_8907" s="T656">hurt-PRS.[3SG]</ta>
            <ta e="T658" id="Seg_8908" s="T657">this-ACC</ta>
            <ta e="T659" id="Seg_8909" s="T658">boil-PRS-2SG</ta>
            <ta e="T660" id="Seg_8910" s="T659">drink-PRS-2SG</ta>
            <ta e="T661" id="Seg_8911" s="T660">then</ta>
            <ta e="T662" id="Seg_8912" s="T661">eat-FUT-2SG</ta>
            <ta e="T663" id="Seg_8913" s="T662">and</ta>
            <ta e="T664" id="Seg_8914" s="T663">one.[NOM.SG]</ta>
            <ta e="T667" id="Seg_8915" s="T666">grass.[NOM.SG]</ta>
            <ta e="T668" id="Seg_8916" s="T667">be-PRS.[3SG]</ta>
            <ta e="T669" id="Seg_8917" s="T668">%%</ta>
            <ta e="T670" id="Seg_8918" s="T669">name-PRS-3PL</ta>
            <ta e="T671" id="Seg_8919" s="T670">this-ACC</ta>
            <ta e="T672" id="Seg_8920" s="T671">put-FUT-2SG</ta>
            <ta e="T673" id="Seg_8921" s="T672">vodka-LAT</ta>
            <ta e="T674" id="Seg_8922" s="T673">this.[NOM.SG]</ta>
            <ta e="T675" id="Seg_8923" s="T674">stand-IMP-3SG</ta>
            <ta e="T676" id="Seg_8924" s="T675">ten.[NOM.SG]</ta>
            <ta e="T677" id="Seg_8925" s="T676">two.[NOM.SG]</ta>
            <ta e="T678" id="Seg_8926" s="T677">day.[NOM.SG]</ta>
            <ta e="T679" id="Seg_8927" s="T678">then</ta>
            <ta e="T680" id="Seg_8928" s="T679">drink-FUT-2SG</ta>
            <ta e="T681" id="Seg_8929" s="T680">heart-NOM/GEN/ACC.3SG</ta>
            <ta e="T682" id="Seg_8930" s="T681">good.[NOM.SG]</ta>
            <ta e="T683" id="Seg_8931" s="T682">become-FUT-2SG</ta>
            <ta e="T685" id="Seg_8932" s="T684">you.NOM</ta>
            <ta e="T686" id="Seg_8933" s="T685">we.NOM</ta>
            <ta e="T687" id="Seg_8934" s="T686">Kamassian-PL-NOM/GEN/ACC.3PL</ta>
            <ta e="T688" id="Seg_8935" s="T687">Carnival-LOC</ta>
            <ta e="T689" id="Seg_8936" s="T688">PTCL</ta>
            <ta e="T690" id="Seg_8937" s="T689">horse-3SG-INS</ta>
            <ta e="T691" id="Seg_8938" s="T690">gallop-DUR-3PL</ta>
            <ta e="T692" id="Seg_8939" s="T691">who-GEN</ta>
            <ta e="T693" id="Seg_8940" s="T692">horse-NOM/GEN.3SG</ta>
            <ta e="T694" id="Seg_8941" s="T693">soon</ta>
            <ta e="T695" id="Seg_8942" s="T694">come-FUT-3SG</ta>
            <ta e="T696" id="Seg_8943" s="T695">and</ta>
            <ta e="T697" id="Seg_8944" s="T696">who-GEN</ta>
            <ta e="T698" id="Seg_8945" s="T697">horse-NOM/GEN.3SG</ta>
            <ta e="T699" id="Seg_8946" s="T698">remain-FUT-3SG</ta>
            <ta e="T700" id="Seg_8947" s="T699">this.[NOM.SG]</ta>
            <ta e="T701" id="Seg_8948" s="T700">who-GEN</ta>
            <ta e="T702" id="Seg_8949" s="T701">remain-FUT-3SG</ta>
            <ta e="T703" id="Seg_8950" s="T702">this.[NOM.SG]</ta>
            <ta e="T706" id="Seg_8951" s="T705">vodka.[NOM.SG]</ta>
            <ta e="T707" id="Seg_8952" s="T706">place-FUT-3SG</ta>
            <ta e="T709" id="Seg_8953" s="T708">then</ta>
            <ta e="T710" id="Seg_8954" s="T709">another.[NOM.SG]</ta>
            <ta e="T711" id="Seg_8955" s="T710">horse.[NOM.SG]</ta>
            <ta e="T712" id="Seg_8956" s="T711">bring-FUT-3PL</ta>
            <ta e="T713" id="Seg_8957" s="T712">and</ta>
            <ta e="T714" id="Seg_8958" s="T713">this.[NOM.SG]</ta>
            <ta e="T715" id="Seg_8959" s="T714">that.[NOM.SG]</ta>
            <ta e="T716" id="Seg_8960" s="T715">horse.[NOM.SG]</ta>
            <ta e="T717" id="Seg_8961" s="T716">very</ta>
            <ta e="T719" id="Seg_8962" s="T718">say-PRS-3PL</ta>
            <ta e="T720" id="Seg_8963" s="T719">so</ta>
            <ta e="T721" id="Seg_8964" s="T720">then</ta>
            <ta e="T722" id="Seg_8965" s="T721">this-ACC</ta>
            <ta e="T723" id="Seg_8966" s="T722">also</ta>
            <ta e="T724" id="Seg_8967" s="T723">remain-MOM-2DU</ta>
            <ta e="T725" id="Seg_8968" s="T724">here</ta>
            <ta e="T728" id="Seg_8969" s="T727">run-MOM-FUT-3SG</ta>
            <ta e="T729" id="Seg_8970" s="T728">and</ta>
            <ta e="T730" id="Seg_8971" s="T729">this</ta>
            <ta e="T731" id="Seg_8972" s="T730">horse.[NOM.SG]</ta>
            <ta e="T732" id="Seg_8973" s="T731">remain-MOM-2DU</ta>
            <ta e="T734" id="Seg_8974" s="T733">then</ta>
            <ta e="T735" id="Seg_8975" s="T734">this</ta>
            <ta e="T736" id="Seg_8976" s="T735">horse-ACC</ta>
            <ta e="T739" id="Seg_8977" s="T738">take-PRS.[3SG]</ta>
            <ta e="T740" id="Seg_8978" s="T739">and</ta>
            <ta e="T741" id="Seg_8979" s="T740">go-PRS.[3SG]</ta>
            <ta e="T742" id="Seg_8980" s="T741">go-PRS.[3SG]</ta>
            <ta e="T743" id="Seg_8981" s="T742">softly</ta>
            <ta e="T744" id="Seg_8982" s="T743">so.that</ta>
            <ta e="T745" id="Seg_8983" s="T744">NEG</ta>
            <ta e="T746" id="Seg_8984" s="T745">stand-PST.[3SG]</ta>
            <ta e="T748" id="Seg_8985" s="T747">then</ta>
            <ta e="T749" id="Seg_8986" s="T748">vodka.[NOM.SG]</ta>
            <ta e="T750" id="Seg_8987" s="T749">many</ta>
            <ta e="T751" id="Seg_8988" s="T750">drink-MOM-FUT-3PL</ta>
            <ta e="T752" id="Seg_8989" s="T751">and</ta>
            <ta e="T753" id="Seg_8990" s="T752">PTCL</ta>
            <ta e="T754" id="Seg_8991" s="T753">fall-MOM-FUT-3PL</ta>
            <ta e="T756" id="Seg_8992" s="T755">then</ta>
            <ta e="T757" id="Seg_8993" s="T756">man-PL</ta>
            <ta e="T758" id="Seg_8994" s="T757">tree-PL</ta>
            <ta e="T759" id="Seg_8995" s="T758">take-FUT-3PL</ta>
            <ta e="T760" id="Seg_8996" s="T759">hand-LAT/LOC.3SG</ta>
            <ta e="T761" id="Seg_8997" s="T760">and</ta>
            <ta e="T762" id="Seg_8998" s="T761">then</ta>
            <ta e="T769" id="Seg_8999" s="T768">then</ta>
            <ta e="T770" id="Seg_9000" s="T769">one.[NOM.SG]</ta>
            <ta e="T771" id="Seg_9001" s="T770">one-LAT</ta>
            <ta e="T772" id="Seg_9002" s="T771">PTCL</ta>
            <ta e="T773" id="Seg_9003" s="T772">%%</ta>
            <ta e="T775" id="Seg_9004" s="T774">%%-DUR.[3SG]</ta>
            <ta e="T776" id="Seg_9005" s="T775">then</ta>
            <ta e="T777" id="Seg_9006" s="T776">one.[NOM.SG]</ta>
            <ta e="T778" id="Seg_9007" s="T777">powerful.[NOM.SG]</ta>
            <ta e="T782" id="Seg_9008" s="T781">this-ACC</ta>
            <ta e="T783" id="Seg_9009" s="T782">PTCL</ta>
            <ta e="T784" id="Seg_9010" s="T783">foot-LAT/LOC.3SG</ta>
            <ta e="T785" id="Seg_9011" s="T784">place-FUT-FUT-3SG</ta>
            <ta e="T786" id="Seg_9012" s="T785">this.[NOM.SG]</ta>
            <ta e="T787" id="Seg_9013" s="T786">PTCL</ta>
            <ta e="T788" id="Seg_9014" s="T787">then</ta>
            <ta e="T789" id="Seg_9015" s="T788">vodka.[NOM.SG]</ta>
            <ta e="T790" id="Seg_9016" s="T789">stand-MOM-FUT-3SG</ta>
            <ta e="T791" id="Seg_9017" s="T790">then</ta>
            <ta e="T792" id="Seg_9018" s="T791">drink-PRS-3PL</ta>
            <ta e="T794" id="Seg_9019" s="T793">then</ta>
            <ta e="T795" id="Seg_9020" s="T794">two-COLL</ta>
            <ta e="T796" id="Seg_9021" s="T795">Kamassian-PL</ta>
            <ta e="T798" id="Seg_9022" s="T797">two-COLL</ta>
            <ta e="T799" id="Seg_9023" s="T798">Kamassian-PL</ta>
            <ta e="T800" id="Seg_9024" s="T799">take-FUT-3PL</ta>
            <ta e="T801" id="Seg_9025" s="T800">one.[NOM.SG]</ta>
            <ta e="T802" id="Seg_9026" s="T801">one-LAT</ta>
            <ta e="T803" id="Seg_9027" s="T802">then</ta>
            <ta e="T804" id="Seg_9028" s="T803">how</ta>
            <ta e="T805" id="Seg_9029" s="T804">powerful.[NOM.SG]</ta>
            <ta e="T806" id="Seg_9030" s="T805">that-ACC</ta>
            <ta e="T807" id="Seg_9031" s="T806">throw.away-FUT-3SG</ta>
            <ta e="T808" id="Seg_9032" s="T807">place-LAT</ta>
            <ta e="T809" id="Seg_9033" s="T808">then</ta>
            <ta e="T810" id="Seg_9034" s="T809">this.[NOM.SG]</ta>
            <ta e="T811" id="Seg_9035" s="T810">vodka.[NOM.SG]</ta>
            <ta e="T812" id="Seg_9036" s="T811">place-PRS.[3SG]</ta>
            <ta e="T813" id="Seg_9037" s="T812">this-LAT</ta>
            <ta e="T814" id="Seg_9038" s="T813">drink-DUR-3PL</ta>
            <ta e="T816" id="Seg_9039" s="T815">then</ta>
            <ta e="T817" id="Seg_9040" s="T816">place-LAT</ta>
            <ta e="T818" id="Seg_9041" s="T817">throw.away-FUT-3SG</ta>
            <ta e="T819" id="Seg_9042" s="T818">hand-ACC.3SG</ta>
            <ta e="T820" id="Seg_9043" s="T819">PTCL</ta>
            <ta e="T821" id="Seg_9044" s="T820">break-FUT-3SG</ta>
            <ta e="T824" id="Seg_9045" s="T823">gate-PL</ta>
            <ta e="T825" id="Seg_9046" s="T824">stand-DUR-3PL</ta>
            <ta e="T826" id="Seg_9047" s="T825">go-EP-IMP.2SG</ta>
            <ta e="T827" id="Seg_9048" s="T826">what.kind</ta>
            <ta e="T828" id="Seg_9049" s="T827">road-LAT</ta>
            <ta e="T829" id="Seg_9050" s="T828">go-FUT-2SG</ta>
            <ta e="T830" id="Seg_9051" s="T829">right</ta>
            <ta e="T831" id="Seg_9052" s="T830">road.[NOM.SG]</ta>
            <ta e="T832" id="Seg_9053" s="T831">PTCL</ta>
            <ta e="T833" id="Seg_9054" s="T832">NEG</ta>
            <ta e="T834" id="Seg_9055" s="T833">good</ta>
            <ta e="T835" id="Seg_9056" s="T834">PTCL</ta>
            <ta e="T836" id="Seg_9057" s="T835">swamp-NOM/GEN/ACC.3PL</ta>
            <ta e="T837" id="Seg_9058" s="T836">tree-PL</ta>
            <ta e="T838" id="Seg_9059" s="T837">stand-PRS-3PL</ta>
            <ta e="T839" id="Seg_9060" s="T838">and</ta>
            <ta e="T840" id="Seg_9061" s="T839">PTCL</ta>
            <ta e="T841" id="Seg_9062" s="T840">thunderstorm-PL</ta>
            <ta e="T842" id="Seg_9063" s="T841">wind.[NOM.SG]</ta>
            <ta e="T843" id="Seg_9064" s="T842">PTCL</ta>
            <ta e="T844" id="Seg_9065" s="T843">iron-ADJZ</ta>
            <ta e="T845" id="Seg_9066" s="T844">thorn-NOM/GEN/ACC.3PL</ta>
            <ta e="T846" id="Seg_9067" s="T845">PTCL</ta>
            <ta e="T847" id="Seg_9068" s="T846">stick-DUR-3PL</ta>
            <ta e="T848" id="Seg_9069" s="T847">and</ta>
            <ta e="T849" id="Seg_9070" s="T848">there</ta>
            <ta e="T850" id="Seg_9071" s="T849">far-LAT.ADV</ta>
            <ta e="T851" id="Seg_9072" s="T850">go-FUT-2SG</ta>
            <ta e="T852" id="Seg_9073" s="T851">there</ta>
            <ta e="T853" id="Seg_9074" s="T852">PTCL</ta>
            <ta e="T854" id="Seg_9075" s="T853">good</ta>
            <ta e="T855" id="Seg_9076" s="T854">tree-PL</ta>
            <ta e="T856" id="Seg_9077" s="T855">NEG.EX.[3SG]</ta>
            <ta e="T857" id="Seg_9078" s="T856">what.[NOM.SG]=INDEF</ta>
            <ta e="T858" id="Seg_9079" s="T857">NEG.EX.[3SG]</ta>
            <ta e="T859" id="Seg_9080" s="T858">very</ta>
            <ta e="T860" id="Seg_9081" s="T859">beautiful.[NOM.SG]</ta>
            <ta e="T861" id="Seg_9082" s="T860">grass.[NOM.SG]</ta>
            <ta e="T862" id="Seg_9083" s="T861">beautiful.[NOM.SG]</ta>
            <ta e="T864" id="Seg_9084" s="T863">flower-PL</ta>
            <ta e="T865" id="Seg_9085" s="T864">snow.[NOM.SG]</ta>
            <ta e="T866" id="Seg_9086" s="T865">red.[NOM.SG]</ta>
            <ta e="T868" id="Seg_9087" s="T867">grow-DUR.[3SG]</ta>
            <ta e="T869" id="Seg_9088" s="T868">apple.tree-NOM/GEN/ACC.3PL</ta>
            <ta e="T870" id="Seg_9089" s="T869">sun.[NOM.SG]</ta>
            <ta e="T871" id="Seg_9090" s="T870">go-DUR</ta>
            <ta e="T872" id="Seg_9091" s="T871">very</ta>
            <ta e="T873" id="Seg_9092" s="T872">warm.[NOM.SG]</ta>
            <ta e="T874" id="Seg_9093" s="T873">although</ta>
            <ta e="T875" id="Seg_9094" s="T874">lie.down-IMP.2SG</ta>
            <ta e="T876" id="Seg_9095" s="T875">and</ta>
            <ta e="T877" id="Seg_9096" s="T876">sleep-EP-IMP.2SG</ta>
            <ta e="T878" id="Seg_9097" s="T877">good</ta>
            <ta e="T879" id="Seg_9098" s="T878">there</ta>
            <ta e="T880" id="Seg_9099" s="T879">and</ta>
            <ta e="T881" id="Seg_9100" s="T880">left</ta>
            <ta e="T882" id="Seg_9101" s="T881">hand-LAT/LOC.3SG</ta>
            <ta e="T883" id="Seg_9102" s="T882">walk-PRS.[3SG]</ta>
            <ta e="T884" id="Seg_9103" s="T883">very</ta>
            <ta e="T885" id="Seg_9104" s="T884">road</ta>
            <ta e="T886" id="Seg_9105" s="T885">big.[NOM.SG]</ta>
            <ta e="T887" id="Seg_9106" s="T886">people.[NOM.SG]</ta>
            <ta e="T888" id="Seg_9107" s="T887">many</ta>
            <ta e="T889" id="Seg_9108" s="T888">all</ta>
            <ta e="T890" id="Seg_9109" s="T889">vodka.[NOM.SG]</ta>
            <ta e="T891" id="Seg_9110" s="T890">drink-PRS-3PL</ta>
            <ta e="T892" id="Seg_9111" s="T891">all</ta>
            <ta e="T893" id="Seg_9112" s="T892">what.[NOM.SG]</ta>
            <ta e="T894" id="Seg_9113" s="T893">eat-PRS-3PL</ta>
            <ta e="T895" id="Seg_9114" s="T894">PTCL</ta>
            <ta e="T896" id="Seg_9115" s="T895">jump-DUR-3PL</ta>
            <ta e="T897" id="Seg_9116" s="T896">PTCL</ta>
            <ta e="T898" id="Seg_9117" s="T897">song.[NOM.SG]</ta>
            <ta e="T899" id="Seg_9118" s="T898">sing-DUR-3PL</ta>
            <ta e="T900" id="Seg_9119" s="T899">and</ta>
            <ta e="T902" id="Seg_9120" s="T901">there</ta>
            <ta e="T903" id="Seg_9121" s="T902">far</ta>
            <ta e="T905" id="Seg_9122" s="T904">go-FUT-2SG</ta>
            <ta e="T906" id="Seg_9123" s="T905">there</ta>
            <ta e="T907" id="Seg_9124" s="T906">PTCL</ta>
            <ta e="T908" id="Seg_9125" s="T907">fire.[NOM.SG]</ta>
            <ta e="T909" id="Seg_9126" s="T908">PTCL</ta>
            <ta e="T910" id="Seg_9127" s="T909">stone-PL</ta>
            <ta e="T911" id="Seg_9128" s="T910">PTCL</ta>
            <ta e="T912" id="Seg_9129" s="T911">thunderstorm-PL</ta>
            <ta e="T913" id="Seg_9130" s="T912">fire.[NOM.SG]</ta>
            <ta e="T914" id="Seg_9131" s="T913">people-EP-ACC</ta>
            <ta e="T915" id="Seg_9132" s="T914">PTCL</ta>
            <ta e="T916" id="Seg_9133" s="T915">burn-DUR.[3SG]</ta>
            <ta e="T917" id="Seg_9134" s="T916">now</ta>
            <ta e="T918" id="Seg_9135" s="T917">PTCL</ta>
            <ta e="T920" id="Seg_9136" s="T919">you.PL.NOM</ta>
            <ta e="T921" id="Seg_9137" s="T920">go-IMP.2PL</ta>
            <ta e="T922" id="Seg_9138" s="T921">and</ta>
            <ta e="T923" id="Seg_9139" s="T922">I.NOM</ta>
            <ta e="T924" id="Seg_9140" s="T923">%%-FUT-1SG</ta>
            <ta e="T925" id="Seg_9141" s="T924">one.should</ta>
            <ta e="T926" id="Seg_9142" s="T925">I.LAT</ta>
            <ta e="T927" id="Seg_9143" s="T926">bread.[NOM.SG]</ta>
            <ta e="T928" id="Seg_9144" s="T927">place-INF.LAT</ta>
            <ta e="T929" id="Seg_9145" s="T928">bake-INF.LAT</ta>
            <ta e="T930" id="Seg_9146" s="T929">bread.[NOM.SG]</ta>
            <ta e="T931" id="Seg_9147" s="T930">one.should</ta>
            <ta e="T932" id="Seg_9148" s="T931">otherwise</ta>
            <ta e="T933" id="Seg_9149" s="T932">eat-INF.LAT</ta>
            <ta e="T934" id="Seg_9150" s="T933">NEG.EX.[3SG]</ta>
            <ta e="T935" id="Seg_9151" s="T934">bread.[NOM.SG]</ta>
            <ta e="T936" id="Seg_9152" s="T935">you.PL.ACC</ta>
            <ta e="T937" id="Seg_9153" s="T936">give-FUT-1SG</ta>
            <ta e="T938" id="Seg_9154" s="T937">eat-PST-CVB</ta>
            <ta e="T940" id="Seg_9155" s="T939">NEG.AUX-IMP.2SG</ta>
            <ta e="T941" id="Seg_9156" s="T940">go-EP-CNG</ta>
            <ta e="T942" id="Seg_9157" s="T941">man-LAT</ta>
            <ta e="T943" id="Seg_9158" s="T942">live-IMP.2SG</ta>
            <ta e="T944" id="Seg_9159" s="T943">alone-LAT</ta>
            <ta e="T945" id="Seg_9160" s="T944">good</ta>
            <ta e="T946" id="Seg_9161" s="T945">live-INF.LAT</ta>
            <ta e="T947" id="Seg_9162" s="T946">where.to=INDEF</ta>
            <ta e="T948" id="Seg_9163" s="T947">go-FUT-1SG</ta>
            <ta e="T949" id="Seg_9164" s="T948">who-LAT=INDEF</ta>
            <ta e="T950" id="Seg_9165" s="T949">NEG</ta>
            <ta e="T951" id="Seg_9166" s="T950">ask-PST-1SG</ta>
            <ta e="T953" id="Seg_9167" s="T952">water.[NOM.SG]</ta>
            <ta e="T954" id="Seg_9168" s="T953">flow-DUR.[3SG]</ta>
            <ta e="T955" id="Seg_9169" s="T954">this.[NOM.SG]</ta>
            <ta e="T956" id="Seg_9170" s="T955">big.[NOM.SG]</ta>
            <ta e="T957" id="Seg_9171" s="T956">Ilbin.[NOM.SG]</ta>
            <ta e="T958" id="Seg_9172" s="T957">small.[NOM.SG]</ta>
            <ta e="T959" id="Seg_9173" s="T958">Ilbin.[NOM.SG]</ta>
            <ta e="T960" id="Seg_9174" s="T959">and</ta>
            <ta e="T961" id="Seg_9175" s="T960">where</ta>
            <ta e="T962" id="Seg_9176" s="T961">water.[NOM.SG]</ta>
            <ta e="T963" id="Seg_9177" s="T962">take-DUR.[3SG]</ta>
            <ta e="T964" id="Seg_9178" s="T963">there</ta>
            <ta e="T965" id="Seg_9179" s="T964">PTCL</ta>
            <ta e="T966" id="Seg_9180" s="T965">name-PRS-3PL</ta>
            <ta e="T967" id="Seg_9181" s="T966">PTCL</ta>
            <ta e="T970" id="Seg_9182" s="T969">one.[NOM.SG]</ta>
            <ta e="T971" id="Seg_9183" s="T970">winter.[NOM.SG]</ta>
            <ta e="T972" id="Seg_9184" s="T971">PTCL</ta>
            <ta e="T973" id="Seg_9185" s="T972">we.NOM</ta>
            <ta e="T974" id="Seg_9186" s="T973">rye.[NOM.SG]</ta>
            <ta e="T975" id="Seg_9187" s="T974">%%-PST-1PL</ta>
            <ta e="T976" id="Seg_9188" s="T975">this-PL</ta>
            <ta e="T977" id="Seg_9189" s="T976">PTCL</ta>
            <ta e="T978" id="Seg_9190" s="T977">always</ta>
            <ta e="T979" id="Seg_9191" s="T978">sit-DUR-3PL</ta>
            <ta e="T980" id="Seg_9192" s="T979">sit-DUR-3PL</ta>
            <ta e="T981" id="Seg_9193" s="T980">speak-DUR-3PL</ta>
            <ta e="T982" id="Seg_9194" s="T981">laugh-DUR-3PL</ta>
            <ta e="T983" id="Seg_9195" s="T982">go-OPT.DU/PL-1PL</ta>
            <ta e="T984" id="Seg_9196" s="T983">%%-INF.LAT</ta>
            <ta e="T985" id="Seg_9197" s="T984">otherwise</ta>
            <ta e="T986" id="Seg_9198" s="T985">cold.[NOM.SG]</ta>
            <ta e="T987" id="Seg_9199" s="T986">become-FUT-3SG</ta>
            <ta e="T990" id="Seg_9200" s="T989">we.LAT</ta>
            <ta e="T991" id="Seg_9201" s="T990">one.should</ta>
            <ta e="T992" id="Seg_9202" s="T991">%%-INF.LAT</ta>
            <ta e="T993" id="Seg_9203" s="T992">you.DAT</ta>
            <ta e="T994" id="Seg_9204" s="T993">you.DAT</ta>
            <ta e="T995" id="Seg_9205" s="T994">always</ta>
            <ta e="T996" id="Seg_9206" s="T995">many</ta>
            <ta e="T997" id="Seg_9207" s="T996">one.needs</ta>
            <ta e="T998" id="Seg_9208" s="T997">then</ta>
            <ta e="T999" id="Seg_9209" s="T998">hand-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T1000" id="Seg_9210" s="T999">PTCL</ta>
            <ta e="T1001" id="Seg_9211" s="T1000">freeze-DUR-3PL</ta>
            <ta e="T1002" id="Seg_9212" s="T1001">snow.[NOM.SG]</ta>
            <ta e="T1003" id="Seg_9213" s="T1002">fall-MOM-FUT-3SG</ta>
            <ta e="T1004" id="Seg_9214" s="T1003">tree.[NOM.SG]</ta>
            <ta e="T1005" id="Seg_9215" s="T1004">again</ta>
            <ta e="T1006" id="Seg_9216" s="T1005">we.NOM</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_9217" s="T0">я.NOM</ta>
            <ta e="T2" id="Seg_9218" s="T1">сегодня</ta>
            <ta e="T3" id="Seg_9219" s="T2">вы.ACC</ta>
            <ta e="T5" id="Seg_9220" s="T4">ругать-PST-1SG</ta>
            <ta e="T6" id="Seg_9221" s="T5">прийти-PST-2PL</ta>
            <ta e="T7" id="Seg_9222" s="T6">говорить-INF.LAT</ta>
            <ta e="T8" id="Seg_9223" s="T7">а</ta>
            <ta e="T9" id="Seg_9224" s="T8">что.[NOM.SG]=INDEF</ta>
            <ta e="T10" id="Seg_9225" s="T9">NEG</ta>
            <ta e="T11" id="Seg_9226" s="T10">слушать-DUR-2PL</ta>
            <ta e="T12" id="Seg_9227" s="T11">NEG</ta>
            <ta e="T14" id="Seg_9228" s="T13">NEG</ta>
            <ta e="T15" id="Seg_9229" s="T14">сказать-IPFVZ-PRS-2PL</ta>
            <ta e="T17" id="Seg_9230" s="T16">сегодня</ta>
            <ta e="T18" id="Seg_9231" s="T17">говорить-PST.[3SG]</ta>
            <ta e="T19" id="Seg_9232" s="T18">хороший</ta>
            <ta e="T21" id="Seg_9233" s="T20">говорить-PST.[3SG]</ta>
            <ta e="T22" id="Seg_9234" s="T21">я.NOM</ta>
            <ta e="T23" id="Seg_9235" s="T22">слушать-PST-1SG</ta>
            <ta e="T24" id="Seg_9236" s="T23">и</ta>
            <ta e="T25" id="Seg_9237" s="T24">NEG</ta>
            <ta e="T26" id="Seg_9238" s="T25">PTCL</ta>
            <ta e="T28" id="Seg_9239" s="T27">всегда</ta>
            <ta e="T29" id="Seg_9240" s="T28">работать-PRS-1SG</ta>
            <ta e="T30" id="Seg_9241" s="T29">слушать-INF.LAT</ta>
            <ta e="T32" id="Seg_9242" s="T31">как=INDEF</ta>
            <ta e="T33" id="Seg_9243" s="T32">NEG</ta>
            <ta e="T34" id="Seg_9244" s="T33">мочь-PRS-1SG</ta>
            <ta e="T35" id="Seg_9245" s="T34">кто.[NOM.SG]=INDEF</ta>
            <ta e="T36" id="Seg_9246" s="T35">говорить-DUR.[3SG]</ta>
            <ta e="T37" id="Seg_9247" s="T36">белый-INS</ta>
            <ta e="T38" id="Seg_9248" s="T37">ты.NOM</ta>
            <ta e="T39" id="Seg_9249" s="T38">ложиться-IMP.2SG</ta>
            <ta e="T40" id="Seg_9250" s="T39">место-LAT</ta>
            <ta e="T41" id="Seg_9251" s="T40">лежать-IMP.2SG</ta>
            <ta e="T42" id="Seg_9252" s="T41">NEG.AUX-IMP.2SG</ta>
            <ta e="T43" id="Seg_9253" s="T42">говорить-EP-CNG</ta>
            <ta e="T44" id="Seg_9254" s="T43">NEG.AUX-IMP.2SG</ta>
            <ta e="T45" id="Seg_9255" s="T44">кричать-EP-CNG</ta>
            <ta e="T46" id="Seg_9256" s="T45">а</ta>
            <ta e="T47" id="Seg_9257" s="T46">что.[NOM.SG]</ta>
            <ta e="T48" id="Seg_9258" s="T47">я.NOM</ta>
            <ta e="T49" id="Seg_9259" s="T48">NEG</ta>
            <ta e="T50" id="Seg_9260" s="T49">говорить-FUT-1SG</ta>
            <ta e="T51" id="Seg_9261" s="T50">и</ta>
            <ta e="T52" id="Seg_9262" s="T51">NEG</ta>
            <ta e="T53" id="Seg_9263" s="T52">кричать-FUT-1SG</ta>
            <ta e="T54" id="Seg_9264" s="T53">и</ta>
            <ta e="T56" id="Seg_9265" s="T55">долго</ta>
            <ta e="T57" id="Seg_9266" s="T56">жить-FUT-2SG</ta>
            <ta e="T58" id="Seg_9267" s="T57">NEG</ta>
            <ta e="T59" id="Seg_9268" s="T58">умереть-FUT-2SG</ta>
            <ta e="T60" id="Seg_9269" s="T59">хороший</ta>
            <ta e="T61" id="Seg_9270" s="T60">стать-FUT-3SG</ta>
            <ta e="T62" id="Seg_9271" s="T61">ты.DAT</ta>
            <ta e="T63" id="Seg_9272" s="T62">лежать-IMP.2SG</ta>
            <ta e="T64" id="Seg_9273" s="T63">лежать-IMP.2SG</ta>
            <ta e="T65" id="Seg_9274" s="T64">тогда</ta>
            <ta e="T66" id="Seg_9275" s="T65">кто.[NOM.SG]=INDEF</ta>
            <ta e="T67" id="Seg_9276" s="T66">PTCL</ta>
            <ta e="T68" id="Seg_9277" s="T67">лететь-MOM-PST.[3SG]</ta>
            <ta e="T69" id="Seg_9278" s="T68">этот-ACC</ta>
            <ta e="T70" id="Seg_9279" s="T69">и</ta>
            <ta e="T71" id="Seg_9280" s="T70">%%-PST.[3SG]</ta>
            <ta e="T72" id="Seg_9281" s="T71">этот-ACC</ta>
            <ta e="T73" id="Seg_9282" s="T72">тогда</ta>
            <ta e="T74" id="Seg_9283" s="T73">холодный.[NOM.SG]</ta>
            <ta e="T75" id="Seg_9284" s="T74">этот-ACC</ta>
            <ta e="T76" id="Seg_9285" s="T75">очень</ta>
            <ta e="T77" id="Seg_9286" s="T76">сильно</ta>
            <ta e="T78" id="Seg_9287" s="T77">мерзнуть-PST.[3SG]</ta>
            <ta e="T79" id="Seg_9288" s="T78">мерзнуть-PST.[3SG]</ta>
            <ta e="T80" id="Seg_9289" s="T79">а</ta>
            <ta e="T81" id="Seg_9290" s="T80">тогда</ta>
            <ta e="T82" id="Seg_9291" s="T81">теплый.[NOM.SG]</ta>
            <ta e="T83" id="Seg_9292" s="T82">стать-RES-PST.[3SG]</ta>
            <ta e="T84" id="Seg_9293" s="T83">солнце.[NOM.SG]</ta>
            <ta e="T85" id="Seg_9294" s="T84">встать-PST.[3SG]</ta>
            <ta e="T86" id="Seg_9295" s="T85">вверх</ta>
            <ta e="T87" id="Seg_9296" s="T86">встать-PST.[3SG]</ta>
            <ta e="T88" id="Seg_9297" s="T87">снег.[NOM.SG]</ta>
            <ta e="T89" id="Seg_9298" s="T88">PTCL</ta>
            <ta e="T90" id="Seg_9299" s="T89">пугать-MOM-PST.[3SG]</ta>
            <ta e="T91" id="Seg_9300" s="T90">и</ta>
            <ta e="T92" id="Seg_9301" s="T91">тогда</ta>
            <ta e="T93" id="Seg_9302" s="T92">PTCL</ta>
            <ta e="T97" id="Seg_9303" s="T96">тогда</ta>
            <ta e="T98" id="Seg_9304" s="T97">PTCL</ta>
            <ta e="T101" id="Seg_9305" s="T100">вода.[NOM.SG]</ta>
            <ta e="T102" id="Seg_9306" s="T101">течь-DUR.[3SG]</ta>
            <ta e="T103" id="Seg_9307" s="T102">и</ta>
            <ta e="T104" id="Seg_9308" s="T103">снег.[NOM.SG]</ta>
            <ta e="T105" id="Seg_9309" s="T104">умереть-RES-PST.[3SG]</ta>
            <ta e="T107" id="Seg_9310" s="T106">PTCL</ta>
            <ta e="T108" id="Seg_9311" s="T107">опьянеть-MOM-PST.[3SG]</ta>
            <ta e="T109" id="Seg_9312" s="T108">и</ta>
            <ta e="T110" id="Seg_9313" s="T109">PTCL</ta>
            <ta e="T111" id="Seg_9314" s="T110">%%</ta>
            <ta e="T112" id="Seg_9315" s="T111">взять-PST.[3SG]</ta>
            <ta e="T113" id="Seg_9316" s="T112">рука-LAT/LOC.3SG</ta>
            <ta e="T114" id="Seg_9317" s="T113">скоро</ta>
            <ta e="T115" id="Seg_9318" s="T114">сказать-IPFVZ.[3SG]</ta>
            <ta e="T116" id="Seg_9319" s="T115">ударить-MULT-FUT-1SG</ta>
            <ta e="T117" id="Seg_9320" s="T116">я.NOM</ta>
            <ta e="T118" id="Seg_9321" s="T117">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T119" id="Seg_9322" s="T118">ребенок.[NOM.SG]</ta>
            <ta e="T120" id="Seg_9323" s="T119">принести-PST.[3SG]</ta>
            <ta e="T121" id="Seg_9324" s="T120">мальчик.[NOM.SG]</ta>
            <ta e="T122" id="Seg_9325" s="T121">принести-PST.[3SG]</ta>
            <ta e="T123" id="Seg_9326" s="T122">дом-LOC</ta>
            <ta e="T124" id="Seg_9327" s="T123">PTCL</ta>
            <ta e="T125" id="Seg_9328" s="T124">болеть-PST.[3SG]</ta>
            <ta e="T126" id="Seg_9329" s="T125">кричать-PST.[3SG]</ta>
            <ta e="T127" id="Seg_9330" s="T126">бабушка.[NOM.SG]</ta>
            <ta e="T128" id="Seg_9331" s="T127">быть-PST.[3SG]</ta>
            <ta e="T129" id="Seg_9332" s="T128">этот.[NOM.SG]</ta>
            <ta e="T130" id="Seg_9333" s="T129">принести-PST.[3SG]</ta>
            <ta e="T132" id="Seg_9334" s="T131">ребенок.[NOM.SG]</ta>
            <ta e="T133" id="Seg_9335" s="T132">этот.[NOM.SG]</ta>
            <ta e="T134" id="Seg_9336" s="T133">PTCL</ta>
            <ta e="T136" id="Seg_9337" s="T135">%%-PST.[3SG]</ta>
            <ta e="T137" id="Seg_9338" s="T136">завязать-PST.[3SG]</ta>
            <ta e="T138" id="Seg_9339" s="T137">и</ta>
            <ta e="T139" id="Seg_9340" s="T138">мыть-PST.[3SG]</ta>
            <ta e="T140" id="Seg_9341" s="T139">тогда</ta>
            <ta e="T144" id="Seg_9342" s="T143">тогда</ta>
            <ta e="T145" id="Seg_9343" s="T144">найти-PST.[3SG]</ta>
            <ta e="T146" id="Seg_9344" s="T145">тряпка-PL</ta>
            <ta e="T147" id="Seg_9345" s="T146">плести-PST.[3SG]</ta>
            <ta e="T148" id="Seg_9346" s="T147">этот-ACC</ta>
            <ta e="T149" id="Seg_9347" s="T148">и</ta>
            <ta e="T150" id="Seg_9348" s="T149">класть-PST.[3SG]</ta>
            <ta e="T151" id="Seg_9349" s="T150">печь-LAT</ta>
            <ta e="T152" id="Seg_9350" s="T151">этот.[NOM.SG]</ta>
            <ta e="T153" id="Seg_9351" s="T152">PTCL</ta>
            <ta e="T154" id="Seg_9352" s="T153">лежать-DUR.[3SG]</ta>
            <ta e="T155" id="Seg_9353" s="T154">а</ta>
            <ta e="T156" id="Seg_9354" s="T155">этот-ACC</ta>
            <ta e="T158" id="Seg_9355" s="T157">называть-DUR-3PL</ta>
            <ta e="T159" id="Seg_9356" s="T158">тогда</ta>
            <ta e="T160" id="Seg_9357" s="T159">нести-PST-3PL</ta>
            <ta e="T161" id="Seg_9358" s="T160">священник-LAT</ta>
            <ta e="T162" id="Seg_9359" s="T161">и</ta>
            <ta e="T163" id="Seg_9360" s="T162">называть-DUR-3PL</ta>
            <ta e="T165" id="Seg_9361" s="T164">а</ta>
            <ta e="T166" id="Seg_9362" s="T165">я.NOM</ta>
            <ta e="T167" id="Seg_9363" s="T166">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T168" id="Seg_9364" s="T167">этот-ACC</ta>
            <ta e="T169" id="Seg_9365" s="T168">принести-PST.[3SG]</ta>
            <ta e="T170" id="Seg_9366" s="T169">я.LAT</ta>
            <ta e="T171" id="Seg_9367" s="T170">десять.[NOM.SG]</ta>
            <ta e="T172" id="Seg_9368" s="T171">два.[NOM.SG]</ta>
            <ta e="T173" id="Seg_9369" s="T172">зима.[NOM.SG]</ta>
            <ta e="T174" id="Seg_9370" s="T173">быть-PST.[3SG]</ta>
            <ta e="T175" id="Seg_9371" s="T174">тогда</ta>
            <ta e="T176" id="Seg_9372" s="T175">этот</ta>
            <ta e="T178" id="Seg_9373" s="T177">расти-PST.[3SG]</ta>
            <ta e="T179" id="Seg_9374" s="T178">расти-PST.[3SG]</ta>
            <ta e="T180" id="Seg_9375" s="T179">десять.[NOM.SG]</ta>
            <ta e="T181" id="Seg_9376" s="T180">два.[NOM.SG]</ta>
            <ta e="T182" id="Seg_9377" s="T181">десять.[NOM.SG]</ta>
            <ta e="T183" id="Seg_9378" s="T182">два.[NOM.SG]</ta>
            <ta e="T184" id="Seg_9379" s="T183">зима.[NOM.SG]</ta>
            <ta e="T185" id="Seg_9380" s="T184">быть-PST.[3SG]</ta>
            <ta e="T186" id="Seg_9381" s="T185">мать-NOM/GEN.3SG</ta>
            <ta e="T187" id="Seg_9382" s="T186">умереть-RES-PST.[3SG]</ta>
            <ta e="T188" id="Seg_9383" s="T187">этот.[NOM.SG]</ta>
            <ta e="T189" id="Seg_9384" s="T188">остаться-PST.[3SG]</ta>
            <ta e="T190" id="Seg_9385" s="T189">а</ta>
            <ta e="T193" id="Seg_9386" s="T192">мать-ACC</ta>
            <ta e="T194" id="Seg_9387" s="T193">убить-PST-3PL</ta>
            <ta e="T195" id="Seg_9388" s="T194">этот-ACC</ta>
            <ta e="T196" id="Seg_9389" s="T195">убить-PST.[3SG]</ta>
            <ta e="T198" id="Seg_9390" s="T197">этот-GEN</ta>
            <ta e="T199" id="Seg_9391" s="T198">и</ta>
            <ta e="T200" id="Seg_9392" s="T199">этот.[NOM.SG]</ta>
            <ta e="T201" id="Seg_9393" s="T200">PTCL</ta>
            <ta e="T202" id="Seg_9394" s="T201">умереть-RES-PST.[3SG]</ta>
            <ta e="T203" id="Seg_9395" s="T202">лошадь-INS</ta>
            <ta e="T204" id="Seg_9396" s="T203">дом-LAT/LOC.3SG</ta>
            <ta e="T205" id="Seg_9397" s="T204">принести-PST-3PL</ta>
            <ta e="T206" id="Seg_9398" s="T205">я.NOM</ta>
            <ta e="T207" id="Seg_9399" s="T206">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T208" id="Seg_9400" s="T207">лес-LOC</ta>
            <ta e="T209" id="Seg_9401" s="T208">умереть-RES-PST.[3SG]</ta>
            <ta e="T210" id="Seg_9402" s="T209">тогда</ta>
            <ta e="T211" id="Seg_9403" s="T210">сестра-NOM/GEN/ACC.1SG</ta>
            <ta e="T212" id="Seg_9404" s="T211">и</ta>
            <ta e="T213" id="Seg_9405" s="T212">я.NOM</ta>
            <ta e="T214" id="Seg_9406" s="T213">мужчина-NOM/GEN/ACC.1SG</ta>
            <ta e="T215" id="Seg_9407" s="T214">два.[NOM.SG]</ta>
            <ta e="T216" id="Seg_9408" s="T215">лошадь-DU-3SG-INS</ta>
            <ta e="T217" id="Seg_9409" s="T216">принести-PST-3PL</ta>
            <ta e="T218" id="Seg_9410" s="T217">дом-LAT/LOC.1DU</ta>
            <ta e="T219" id="Seg_9411" s="T218">я.NOM</ta>
            <ta e="T220" id="Seg_9412" s="T219">очень</ta>
            <ta e="T221" id="Seg_9413" s="T220">сильно</ta>
            <ta e="T222" id="Seg_9414" s="T221">плакать-PST-1SG</ta>
            <ta e="T223" id="Seg_9415" s="T222">большой.[NOM.SG]</ta>
            <ta e="T224" id="Seg_9416" s="T223">день.[NOM.SG]</ta>
            <ta e="T225" id="Seg_9417" s="T224">быть-PST.[3SG]</ta>
            <ta e="T227" id="Seg_9418" s="T226">тогда</ta>
            <ta e="T228" id="Seg_9419" s="T227">земля.[NOM.SG]</ta>
            <ta e="T229" id="Seg_9420" s="T228">земля.[NOM.SG]</ta>
            <ta e="T230" id="Seg_9421" s="T229">копать-PST-1PL</ta>
            <ta e="T231" id="Seg_9422" s="T230">дом.[NOM.SG]</ta>
            <ta e="T232" id="Seg_9423" s="T231">делать-PST-1PL</ta>
            <ta e="T233" id="Seg_9424" s="T232">крест.[NOM.SG]</ta>
            <ta e="T234" id="Seg_9425" s="T233">делать-PST-1PL</ta>
            <ta e="T235" id="Seg_9426" s="T234">и</ta>
            <ta e="T236" id="Seg_9427" s="T235">место-LAT</ta>
            <ta e="T237" id="Seg_9428" s="T236">класть-PST-1PL</ta>
            <ta e="T238" id="Seg_9429" s="T237">лить-PST-1PL</ta>
            <ta e="T239" id="Seg_9430" s="T238">тогда</ta>
            <ta e="T240" id="Seg_9431" s="T239">вода.[NOM.SG]</ta>
            <ta e="T241" id="Seg_9432" s="T240">пить-PST-1PL</ta>
            <ta e="T242" id="Seg_9433" s="T241">хлеб.[NOM.SG]</ta>
            <ta e="T243" id="Seg_9434" s="T242">съесть-PST-1PL</ta>
            <ta e="T244" id="Seg_9435" s="T243">люди.[NOM.SG]</ta>
            <ta e="T245" id="Seg_9436" s="T244">быть-PST-3PL</ta>
            <ta e="T246" id="Seg_9437" s="T245">хватит</ta>
            <ta e="T247" id="Seg_9438" s="T246">два.[NOM.SG]</ta>
            <ta e="T248" id="Seg_9439" s="T247">камасинец-PL</ta>
            <ta e="T249" id="Seg_9440" s="T248">жить-PST-3PL</ta>
            <ta e="T250" id="Seg_9441" s="T249">женщина.[NOM.SG]</ta>
            <ta e="T251" id="Seg_9442" s="T250">и</ta>
            <ta e="T252" id="Seg_9443" s="T251">мужчина.[NOM.SG]</ta>
            <ta e="T253" id="Seg_9444" s="T252">водка.[NOM.SG]</ta>
            <ta e="T254" id="Seg_9445" s="T253">пить-PST-3PL</ta>
            <ta e="T255" id="Seg_9446" s="T254">PTCL</ta>
            <ta e="T256" id="Seg_9447" s="T255">бороться-PST-3PL</ta>
            <ta e="T257" id="Seg_9448" s="T256">тогда</ta>
            <ta e="T258" id="Seg_9449" s="T257">порог-LAT/LOC.3SG</ta>
            <ta e="T259" id="Seg_9450" s="T258">сесть-PST-3PL</ta>
            <ta e="T260" id="Seg_9451" s="T259">этот.[NOM.SG]</ta>
            <ta e="T261" id="Seg_9452" s="T260">голова-LAT/LOC.3SG</ta>
            <ta e="T262" id="Seg_9453" s="T261">четыре.[NOM.SG]</ta>
            <ta e="T263" id="Seg_9454" s="T262">быть-PST.[3SG]</ta>
            <ta e="T264" id="Seg_9455" s="T263">женщина.[NOM.SG]</ta>
            <ta e="T265" id="Seg_9456" s="T264">и</ta>
            <ta e="T266" id="Seg_9457" s="T265">мужчина-NOM/GEN.3SG</ta>
            <ta e="T267" id="Seg_9458" s="T266">дуть-%%-DUR-PST.[3SG]</ta>
            <ta e="T268" id="Seg_9459" s="T267">%%-%%-DUR-PST.[3SG]</ta>
            <ta e="T269" id="Seg_9460" s="T268">и</ta>
            <ta e="T270" id="Seg_9461" s="T269">так</ta>
            <ta e="T271" id="Seg_9462" s="T270">спать-MOM-PST-3PL</ta>
            <ta e="T272" id="Seg_9463" s="T271">утро-LOC.ADV</ta>
            <ta e="T273" id="Seg_9464" s="T272">встать-PST-3PL</ta>
            <ta e="T274" id="Seg_9465" s="T273">я.NOM</ta>
            <ta e="T275" id="Seg_9466" s="T274">что.[NOM.SG]=INDEF</ta>
            <ta e="T276" id="Seg_9467" s="T275">PTCL</ta>
            <ta e="T277" id="Seg_9468" s="T276">болеть-PRS-1SG</ta>
            <ta e="T278" id="Seg_9469" s="T277">а</ta>
            <ta e="T279" id="Seg_9470" s="T278">этот</ta>
            <ta e="T280" id="Seg_9471" s="T279">сказать-IPFVZ.[3SG]</ta>
            <ta e="T281" id="Seg_9472" s="T280">я.NOM</ta>
            <ta e="T282" id="Seg_9473" s="T281">тоже</ta>
            <ta e="T283" id="Seg_9474" s="T282">болеть-PRS-1SG</ta>
            <ta e="T284" id="Seg_9475" s="T283">наверное</ta>
            <ta e="T285" id="Seg_9476" s="T284">мы.NOM</ta>
            <ta e="T286" id="Seg_9477" s="T285">бороться-PST-1PL</ta>
            <ta e="T288" id="Seg_9478" s="T287">этот-ACC</ta>
            <ta e="T289" id="Seg_9479" s="T288">позвать-PST-3PL</ta>
            <ta e="T293" id="Seg_9480" s="T292">а</ta>
            <ta e="T294" id="Seg_9481" s="T293">женщина-NOM/GEN/ACC.1SG</ta>
            <ta e="T295" id="Seg_9482" s="T294">позвать-PST-3PL</ta>
            <ta e="T299" id="Seg_9483" s="T298">этот-GEN.PL</ta>
            <ta e="T300" id="Seg_9484" s="T299">дом-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T301" id="Seg_9485" s="T300">маленький.[NOM.SG]</ta>
            <ta e="T302" id="Seg_9486" s="T301">быть-PST-3PL</ta>
            <ta e="T303" id="Seg_9487" s="T302">и</ta>
            <ta e="T304" id="Seg_9488" s="T303">там</ta>
            <ta e="T305" id="Seg_9489" s="T304">%%-LOC</ta>
            <ta e="T306" id="Seg_9490" s="T305">стоять-CVB-PST.[3SG]</ta>
            <ta e="T308" id="Seg_9491" s="T307">этот-GEN</ta>
            <ta e="T309" id="Seg_9492" s="T308">только</ta>
            <ta e="T310" id="Seg_9493" s="T309">сестра-NOM/GEN.3SG</ta>
            <ta e="T311" id="Seg_9494" s="T310">быть-PST.[3SG]</ta>
            <ta e="T312" id="Seg_9495" s="T311">позвать-PST-3PL</ta>
            <ta e="T313" id="Seg_9496" s="T312">Афанасия-PL-INS</ta>
            <ta e="T314" id="Seg_9497" s="T313">ребенок-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T315" id="Seg_9498" s="T314">NEG.EX-PST-3PL</ta>
            <ta e="T316" id="Seg_9499" s="T315">так</ta>
            <ta e="T317" id="Seg_9500" s="T316">PTCL</ta>
            <ta e="T318" id="Seg_9501" s="T317">весь</ta>
            <ta e="T319" id="Seg_9502" s="T318">умереть-RES-PST-3PL</ta>
            <ta e="T321" id="Seg_9503" s="T320">весь</ta>
            <ta e="T322" id="Seg_9504" s="T321">три.[NOM.SG]</ta>
            <ta e="T323" id="Seg_9505" s="T322">умереть-RES-PST-3PL</ta>
            <ta e="T327" id="Seg_9506" s="T326">сестра-NOM/GEN.3SG</ta>
            <ta e="T328" id="Seg_9507" s="T327">что.[NOM.SG]</ta>
            <ta e="T329" id="Seg_9508" s="T328">вы.NOM</ta>
            <ta e="T330" id="Seg_9509" s="T329">бороться-PST-2PL</ta>
            <ta e="T331" id="Seg_9510" s="T330">а</ta>
            <ta e="T332" id="Seg_9511" s="T331">я.NOM</ta>
            <ta e="T333" id="Seg_9512" s="T332">вы.ACC</ta>
            <ta e="T334" id="Seg_9513" s="T333">NEG</ta>
            <ta e="T335" id="Seg_9514" s="T334">%%-PST-1SG</ta>
            <ta e="T336" id="Seg_9515" s="T335">так</ta>
            <ta e="T337" id="Seg_9516" s="T336">PTCL</ta>
            <ta e="T338" id="Seg_9517" s="T337">спать-MOM-PST-2PL</ta>
            <ta e="T340" id="Seg_9518" s="T339">камасинец-PL-LOC</ta>
            <ta e="T341" id="Seg_9519" s="T340">быть-PST.[3SG]</ta>
            <ta e="T342" id="Seg_9520" s="T341">большой.[NOM.SG]</ta>
            <ta e="T343" id="Seg_9521" s="T342">день.[NOM.SG]</ta>
            <ta e="T345" id="Seg_9522" s="T344">день.[NOM.SG]</ta>
            <ta e="T346" id="Seg_9523" s="T345">один.[NOM.SG]</ta>
            <ta e="T347" id="Seg_9524" s="T346">дом-LAT</ta>
            <ta e="T348" id="Seg_9525" s="T347">собирать-PRS-3PL</ta>
            <ta e="T349" id="Seg_9526" s="T348">PTCL</ta>
            <ta e="T350" id="Seg_9527" s="T349">тогда</ta>
            <ta e="T351" id="Seg_9528" s="T350">стол-NOM/GEN/ACC.3SG</ta>
            <ta e="T352" id="Seg_9529" s="T351">собирать-PRS-3PL</ta>
            <ta e="T353" id="Seg_9530" s="T352">%%-PRS-3PL</ta>
            <ta e="T354" id="Seg_9531" s="T353">и</ta>
            <ta e="T355" id="Seg_9532" s="T354">пить-DUR-3PL</ta>
            <ta e="T356" id="Seg_9533" s="T355">водка.[NOM.SG]</ta>
            <ta e="T357" id="Seg_9534" s="T356">и</ta>
            <ta e="T358" id="Seg_9535" s="T357">съесть-DUR-3PL</ta>
            <ta e="T359" id="Seg_9536" s="T358">мясо.[NOM.SG]</ta>
            <ta e="T360" id="Seg_9537" s="T359">хлеб.[NOM.SG]</ta>
            <ta e="T361" id="Seg_9538" s="T360">что.[NOM.SG]</ta>
            <ta e="T362" id="Seg_9539" s="T361">быть-PRS.[3SG]</ta>
            <ta e="T363" id="Seg_9540" s="T362">стол-LOC</ta>
            <ta e="T364" id="Seg_9541" s="T363">тогда</ta>
            <ta e="T365" id="Seg_9542" s="T364">другой.[NOM.SG]</ta>
            <ta e="T366" id="Seg_9543" s="T365">дом-LAT</ta>
            <ta e="T367" id="Seg_9544" s="T366">пойти-FUT-3PL</ta>
            <ta e="T368" id="Seg_9545" s="T367">там</ta>
            <ta e="T369" id="Seg_9546" s="T368">так</ta>
            <ta e="T370" id="Seg_9547" s="T369">же</ta>
            <ta e="T371" id="Seg_9548" s="T370">пить-FUT-3PL</ta>
            <ta e="T372" id="Seg_9549" s="T371">тогда</ta>
            <ta e="T373" id="Seg_9550" s="T372">еще</ta>
            <ta e="T374" id="Seg_9551" s="T373">один.[NOM.SG]</ta>
            <ta e="T375" id="Seg_9552" s="T374">дом-LAT</ta>
            <ta e="T376" id="Seg_9553" s="T375">тогда</ta>
            <ta e="T377" id="Seg_9554" s="T376">четыре.[NOM.SG]</ta>
            <ta e="T378" id="Seg_9555" s="T377">дом-LAT</ta>
            <ta e="T379" id="Seg_9556" s="T378">пять.[NOM.SG]</ta>
            <ta e="T381" id="Seg_9557" s="T380">%%</ta>
            <ta e="T382" id="Seg_9558" s="T381">пять.[NOM.SG]</ta>
            <ta e="T383" id="Seg_9559" s="T382">день.[NOM.SG]</ta>
            <ta e="T384" id="Seg_9560" s="T383">пить-DUR-3PL</ta>
            <ta e="T385" id="Seg_9561" s="T384">всегда</ta>
            <ta e="T386" id="Seg_9562" s="T385">водка.[NOM.SG]</ta>
            <ta e="T387" id="Seg_9563" s="T386">и</ta>
            <ta e="T388" id="Seg_9564" s="T387">тогда</ta>
            <ta e="T389" id="Seg_9565" s="T388">хватит</ta>
            <ta e="T1011" id="Seg_9566" s="T391">Агинское</ta>
            <ta e="T392" id="Seg_9567" s="T1011">поселение-LOC</ta>
            <ta e="T394" id="Seg_9568" s="T393">быть-PST.[3SG]</ta>
            <ta e="T395" id="Seg_9569" s="T394">этот-PL</ta>
            <ta e="T396" id="Seg_9570" s="T395">там</ta>
            <ta e="T397" id="Seg_9571" s="T396">пойти-PST-3PL</ta>
            <ta e="T398" id="Seg_9572" s="T397">и</ta>
            <ta e="T399" id="Seg_9573" s="T398">там</ta>
            <ta e="T400" id="Seg_9574" s="T399">водка.[NOM.SG]</ta>
            <ta e="T401" id="Seg_9575" s="T400">взять-PST-3PL</ta>
            <ta e="T402" id="Seg_9576" s="T401">кто-GEN</ta>
            <ta e="T403" id="Seg_9577" s="T402">деньги.[NOM.SG]</ta>
            <ta e="T404" id="Seg_9578" s="T403">много</ta>
            <ta e="T405" id="Seg_9579" s="T404">так</ta>
            <ta e="T406" id="Seg_9580" s="T405">много</ta>
            <ta e="T407" id="Seg_9581" s="T406">взять-PRS-3PL</ta>
            <ta e="T408" id="Seg_9582" s="T407">пять.[NOM.SG]</ta>
            <ta e="T410" id="Seg_9583" s="T409">а</ta>
            <ta e="T411" id="Seg_9584" s="T410">кто-GEN</ta>
            <ta e="T412" id="Seg_9585" s="T411">мало</ta>
            <ta e="T413" id="Seg_9586" s="T412">четыре.[NOM.SG]</ta>
            <ta e="T414" id="Seg_9587" s="T413">взять-FUT-3SG</ta>
            <ta e="T415" id="Seg_9588" s="T414">кто-GEN</ta>
            <ta e="T416" id="Seg_9589" s="T415">еще</ta>
            <ta e="T417" id="Seg_9590" s="T416">мало</ta>
            <ta e="T418" id="Seg_9591" s="T417">два.[NOM.SG]</ta>
            <ta e="T419" id="Seg_9592" s="T418">взять-FUT-3SG</ta>
            <ta e="T420" id="Seg_9593" s="T419">и</ta>
            <ta e="T421" id="Seg_9594" s="T420">тогда</ta>
            <ta e="T422" id="Seg_9595" s="T421">большой.[NOM.SG]</ta>
            <ta e="T423" id="Seg_9596" s="T422">день-LOC</ta>
            <ta e="T424" id="Seg_9597" s="T423">пить-PST-3PL</ta>
            <ta e="T425" id="Seg_9598" s="T424">маленький.[NOM.SG]</ta>
            <ta e="T426" id="Seg_9599" s="T425">такой.[NOM.SG]</ta>
            <ta e="T427" id="Seg_9600" s="T426">рюмка-LAT</ta>
            <ta e="T428" id="Seg_9601" s="T427">лить-PST-3PL</ta>
            <ta e="T429" id="Seg_9602" s="T428">один-LAT</ta>
            <ta e="T430" id="Seg_9603" s="T429">дать-FUT-3SG</ta>
            <ta e="T431" id="Seg_9604" s="T430">тогда</ta>
            <ta e="T432" id="Seg_9605" s="T431">опять</ta>
            <ta e="T433" id="Seg_9606" s="T432">так</ta>
            <ta e="T434" id="Seg_9607" s="T433">так</ta>
            <ta e="T435" id="Seg_9608" s="T434">так</ta>
            <ta e="T436" id="Seg_9609" s="T435">дать-PRS.[3SG]</ta>
            <ta e="T437" id="Seg_9610" s="T436">дать-PRS.[3SG]</ta>
            <ta e="T438" id="Seg_9611" s="T437">PTCL</ta>
            <ta e="T439" id="Seg_9612" s="T438">%%</ta>
            <ta e="T440" id="Seg_9613" s="T439">тогда</ta>
            <ta e="T441" id="Seg_9614" s="T440">есть-DUR-3PL</ta>
            <ta e="T442" id="Seg_9615" s="T441">съесть-DUR-3PL</ta>
            <ta e="T443" id="Seg_9616" s="T442">PTCL</ta>
            <ta e="T444" id="Seg_9617" s="T443">говорить-DUR-3PL</ta>
            <ta e="T445" id="Seg_9618" s="T444">песня.[NOM.SG]</ta>
            <ta e="T446" id="Seg_9619" s="T445">петь-DUR-3PL</ta>
            <ta e="T451" id="Seg_9620" s="T450">пойти-PST.[3SG]</ta>
            <ta e="T453" id="Seg_9621" s="T451">Агинское-LAT</ta>
            <ta e="T455" id="Seg_9622" s="T454">быть-PST.[3SG]</ta>
            <ta e="T456" id="Seg_9623" s="T455">большой.[NOM.SG]</ta>
            <ta e="T457" id="Seg_9624" s="T456">день.[NOM.SG]</ta>
            <ta e="T458" id="Seg_9625" s="T457">там</ta>
            <ta e="T459" id="Seg_9626" s="T458">водка.[NOM.SG]</ta>
            <ta e="T460" id="Seg_9627" s="T459">пить-PST-3PL</ta>
            <ta e="T461" id="Seg_9628" s="T460">PTCL</ta>
            <ta e="T462" id="Seg_9629" s="T461">каждый</ta>
            <ta e="T463" id="Seg_9630" s="T462">дом-LAT</ta>
            <ta e="T464" id="Seg_9631" s="T463">идти-PST-3PL</ta>
            <ta e="T465" id="Seg_9632" s="T464">этот.[NOM.SG]</ta>
            <ta e="T466" id="Seg_9633" s="T465">вечер-LOC.ADV</ta>
            <ta e="T1010" id="Seg_9634" s="T466">пойти-CVB</ta>
            <ta e="T467" id="Seg_9635" s="T1010">исчезнуть-PST.[3SG]</ta>
            <ta e="T468" id="Seg_9636" s="T467">Анжа-LAT</ta>
            <ta e="T469" id="Seg_9637" s="T468">и</ta>
            <ta e="T470" id="Seg_9638" s="T469">там</ta>
            <ta e="T471" id="Seg_9639" s="T470">замерзнуть-RES-PST.[3SG]</ta>
            <ta e="T472" id="Seg_9640" s="T471">умереть-RES-PST.[3SG]</ta>
            <ta e="T474" id="Seg_9641" s="T473">этот.[NOM.SG]</ta>
            <ta e="T475" id="Seg_9642" s="T474">дом-LAT/LOC.3SG</ta>
            <ta e="T477" id="Seg_9643" s="T476">дом-LAT/LOC.3SG</ta>
            <ta e="T478" id="Seg_9644" s="T477">прийти-PST.[3SG]</ta>
            <ta e="T479" id="Seg_9645" s="T478">наружу</ta>
            <ta e="T480" id="Seg_9646" s="T479">два.[NOM.SG]</ta>
            <ta e="T481" id="Seg_9647" s="T480">мужчина.[NOM.SG]</ta>
            <ta e="T482" id="Seg_9648" s="T481">этот-LAT</ta>
            <ta e="T483" id="Seg_9649" s="T482">прийти-PST-3PL</ta>
            <ta e="T484" id="Seg_9650" s="T483">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T486" id="Seg_9651" s="T485">я.NOM-INS</ta>
            <ta e="T487" id="Seg_9652" s="T486">тогда</ta>
            <ta e="T488" id="Seg_9653" s="T487">пойти-PST.[3SG]</ta>
            <ta e="T489" id="Seg_9654" s="T488">пойти-PST.[3SG]</ta>
            <ta e="T490" id="Seg_9655" s="T489">видеть-PRS-3SG.O</ta>
            <ta e="T491" id="Seg_9656" s="T490">PTCL</ta>
            <ta e="T492" id="Seg_9657" s="T491">%%</ta>
            <ta e="T493" id="Seg_9658" s="T492">PTCL</ta>
            <ta e="T495" id="Seg_9659" s="T494">тогда</ta>
            <ta e="T496" id="Seg_9660" s="T495">Бог-LAT</ta>
            <ta e="T498" id="Seg_9661" s="T497">кланяться-PST.[3SG]</ta>
            <ta e="T499" id="Seg_9662" s="T498">оттуда</ta>
            <ta e="T500" id="Seg_9663" s="T499">люди-NOM/GEN/ACC.3SG</ta>
            <ta e="T501" id="Seg_9664" s="T500">бежать-MOM-PST-3PL</ta>
            <ta e="T502" id="Seg_9665" s="T501">а</ta>
            <ta e="T503" id="Seg_9666" s="T502">этот.[NOM.SG]</ta>
            <ta e="T504" id="Seg_9667" s="T503">дом-LAT/LOC.3SG</ta>
            <ta e="T505" id="Seg_9668" s="T504">прийти-PST.[3SG]</ta>
            <ta e="T506" id="Seg_9669" s="T505">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T507" id="Seg_9670" s="T506">снег.[NOM.SG]</ta>
            <ta e="T508" id="Seg_9671" s="T507">стать-PST.[3SG]</ta>
            <ta e="T510" id="Seg_9672" s="T509">этот-PL</ta>
            <ta e="T511" id="Seg_9673" s="T510">очень</ta>
            <ta e="T512" id="Seg_9674" s="T511">водка.[NOM.SG]</ta>
            <ta e="T513" id="Seg_9675" s="T512">много</ta>
            <ta e="T514" id="Seg_9676" s="T513">пить-DUR-3PL</ta>
            <ta e="T515" id="Seg_9677" s="T514">сколько</ta>
            <ta e="T516" id="Seg_9678" s="T515">хотя</ta>
            <ta e="T518" id="Seg_9679" s="T517">всегда</ta>
            <ta e="T519" id="Seg_9680" s="T518">пить-DUR-3PL</ta>
            <ta e="T520" id="Seg_9681" s="T519">пить-DUR-3PL</ta>
            <ta e="T521" id="Seg_9682" s="T520">еще</ta>
            <ta e="T522" id="Seg_9683" s="T521">искать-DUR-3PL</ta>
            <ta e="T523" id="Seg_9684" s="T522">где</ta>
            <ta e="T524" id="Seg_9685" s="T523">быть-PRS.[3SG]</ta>
            <ta e="T525" id="Seg_9686" s="T524">так</ta>
            <ta e="T526" id="Seg_9687" s="T525">там</ta>
            <ta e="T527" id="Seg_9688" s="T526">бежать-MOM.PRS-3PL</ta>
            <ta e="T528" id="Seg_9689" s="T527">PTCL</ta>
            <ta e="T530" id="Seg_9690" s="T529">тогда</ta>
            <ta e="T531" id="Seg_9691" s="T530">PTCL</ta>
            <ta e="T532" id="Seg_9692" s="T531">петь-DUR-3PL</ta>
            <ta e="T533" id="Seg_9693" s="T532">прыгнуть-DUR-3PL</ta>
            <ta e="T534" id="Seg_9694" s="T533">тогда</ta>
            <ta e="T535" id="Seg_9695" s="T534">PTCL</ta>
            <ta e="T536" id="Seg_9696" s="T535">бороться-DUR-3PL</ta>
            <ta e="T538" id="Seg_9697" s="T537">кто.[NOM.SG]</ta>
            <ta e="T539" id="Seg_9698" s="T538">PTCL</ta>
            <ta e="T540" id="Seg_9699" s="T539">много</ta>
            <ta e="T542" id="Seg_9700" s="T541">пить-PRS.[3SG]</ta>
            <ta e="T543" id="Seg_9701" s="T542">пить-PRS.[3SG]</ta>
            <ta e="T544" id="Seg_9702" s="T543">тогда</ta>
            <ta e="T545" id="Seg_9703" s="T544">водка-ABL</ta>
            <ta e="T546" id="Seg_9704" s="T545">PTCL</ta>
            <ta e="T547" id="Seg_9705" s="T546">гореть-DUR.[3SG]</ta>
            <ta e="T548" id="Seg_9706" s="T547">этот</ta>
            <ta e="T549" id="Seg_9707" s="T548">этот-LAT</ta>
            <ta e="T550" id="Seg_9708" s="T549">рот-LAT</ta>
            <ta e="T551" id="Seg_9709" s="T550">PTCL</ta>
            <ta e="T552" id="Seg_9710" s="T551">мочиться-DUR-3PL</ta>
            <ta e="T553" id="Seg_9711" s="T552">тогда</ta>
            <ta e="T554" id="Seg_9712" s="T553">этот.[NOM.SG]</ta>
            <ta e="T556" id="Seg_9713" s="T555">встать-FUT-3SG</ta>
            <ta e="T558" id="Seg_9714" s="T557">один.[NOM.SG]</ta>
            <ta e="T559" id="Seg_9715" s="T558">один.[NOM.SG]</ta>
            <ta e="T560" id="Seg_9716" s="T559">я.NOM</ta>
            <ta e="T561" id="Seg_9717" s="T560">ухо-NOM/GEN/ACC.1SG</ta>
            <ta e="T562" id="Seg_9718" s="T561">болеть-MOM-PST.[3SG]</ta>
            <ta e="T563" id="Seg_9719" s="T562">PTCL</ta>
            <ta e="T564" id="Seg_9720" s="T563">там</ta>
            <ta e="T565" id="Seg_9721" s="T564">уколоть-DUR.[3SG]</ta>
            <ta e="T566" id="Seg_9722" s="T565">я.NOM</ta>
            <ta e="T567" id="Seg_9723" s="T566">тогда</ta>
            <ta e="T568" id="Seg_9724" s="T567">найти-PST-1SG</ta>
            <ta e="T569" id="Seg_9725" s="T568">тряпка.[NOM.SG]</ta>
            <ta e="T570" id="Seg_9726" s="T569">мочиться-PST-1SG</ta>
            <ta e="T571" id="Seg_9727" s="T570">этот-ABL</ta>
            <ta e="T573" id="Seg_9728" s="T572">даже</ta>
            <ta e="T574" id="Seg_9729" s="T573">течь-MOM-PST.[3SG]</ta>
            <ta e="T575" id="Seg_9730" s="T574">я.NOM</ta>
            <ta e="T576" id="Seg_9731" s="T575">ухо-LAT</ta>
            <ta e="T577" id="Seg_9732" s="T576">класть-PST-1SG</ta>
            <ta e="T580" id="Seg_9733" s="T579">тогда</ta>
            <ta e="T581" id="Seg_9734" s="T580">завязать-PST-1SG</ta>
            <ta e="T582" id="Seg_9735" s="T581">ложиться-PST-1SG</ta>
            <ta e="T583" id="Seg_9736" s="T582">спать-MOM-PST-1SG</ta>
            <ta e="T584" id="Seg_9737" s="T583">встать-PST-1SG</ta>
            <ta e="T586" id="Seg_9738" s="T585">ухо-NOM/GEN/ACC.1SG</ta>
            <ta e="T587" id="Seg_9739" s="T586">NEG</ta>
            <ta e="T588" id="Seg_9740" s="T587">болеть-PRS.[3SG]</ta>
            <ta e="T589" id="Seg_9741" s="T588">а</ta>
            <ta e="T590" id="Seg_9742" s="T589">тогда</ta>
            <ta e="T591" id="Seg_9743" s="T590">голова-NOM/GEN/ACC.1SG</ta>
            <ta e="T592" id="Seg_9744" s="T591">болеть-PRS.[3SG]</ta>
            <ta e="T593" id="Seg_9745" s="T592">я.NOM</ta>
            <ta e="T594" id="Seg_9746" s="T593">тоже</ta>
            <ta e="T595" id="Seg_9747" s="T594">мочиться-FUT-1SG</ta>
            <ta e="T596" id="Seg_9748" s="T595">тряпка-3PL-LAT</ta>
            <ta e="T597" id="Seg_9749" s="T596">голова-LAT/LOC.3SG</ta>
            <ta e="T598" id="Seg_9750" s="T597">завязать-FUT-1SG</ta>
            <ta e="T599" id="Seg_9751" s="T598">ложиться-FUT-1SG</ta>
            <ta e="T600" id="Seg_9752" s="T599">этот.[NOM.SG]</ta>
            <ta e="T602" id="Seg_9753" s="T601">высохнуть-MOM-FUT-3SG</ta>
            <ta e="T603" id="Seg_9754" s="T602">этот</ta>
            <ta e="T604" id="Seg_9755" s="T603">тряпка.[NOM.SG]</ta>
            <ta e="T605" id="Seg_9756" s="T604">голова-NOM/GEN/ACC.1SG</ta>
            <ta e="T606" id="Seg_9757" s="T605">NEG</ta>
            <ta e="T607" id="Seg_9758" s="T606">болеть-PRS.[3SG]</ta>
            <ta e="T609" id="Seg_9759" s="T608">один.[NOM.SG]</ta>
            <ta e="T610" id="Seg_9760" s="T609">один.[NOM.SG]</ta>
            <ta e="T611" id="Seg_9761" s="T610">раз.[NOM.SG]</ta>
            <ta e="T612" id="Seg_9762" s="T611">мы.NOM</ta>
            <ta e="T613" id="Seg_9763" s="T612">хлеб.[NOM.SG]</ta>
            <ta e="T614" id="Seg_9764" s="T613">ударить-MULT-PST-1PL</ta>
            <ta e="T615" id="Seg_9765" s="T614">машина-3PL-INS</ta>
            <ta e="T616" id="Seg_9766" s="T615">там</ta>
            <ta e="T617" id="Seg_9767" s="T616">нож.[NOM.SG]</ta>
            <ta e="T618" id="Seg_9768" s="T617">быть-PST.[3SG]</ta>
            <ta e="T619" id="Seg_9769" s="T618">сноп.PL-NOM/GEN/ACC.3PL</ta>
            <ta e="T621" id="Seg_9770" s="T620">зарезать-INF.LAT</ta>
            <ta e="T622" id="Seg_9771" s="T621">а</ta>
            <ta e="T623" id="Seg_9772" s="T622">этот.[NOM.SG]</ta>
            <ta e="T625" id="Seg_9773" s="T624">как=INDEF</ta>
            <ta e="T626" id="Seg_9774" s="T625">рука-ACC.3SG</ta>
            <ta e="T627" id="Seg_9775" s="T626">PTCL</ta>
            <ta e="T628" id="Seg_9776" s="T627">резать-PST.[3SG]</ta>
            <ta e="T629" id="Seg_9777" s="T628">я.NOM</ta>
            <ta e="T630" id="Seg_9778" s="T629">сказать-IPFVZ-1SG</ta>
            <ta e="T631" id="Seg_9779" s="T630">мочиться-IMP.2SG</ta>
            <ta e="T632" id="Seg_9780" s="T631">этот.[NOM.SG]</ta>
            <ta e="T633" id="Seg_9781" s="T632">мочиться-PST.[3SG]</ta>
            <ta e="T634" id="Seg_9782" s="T633">и</ta>
            <ta e="T635" id="Seg_9783" s="T634">завязать-PST.[3SG]</ta>
            <ta e="T636" id="Seg_9784" s="T635">тогда</ta>
            <ta e="T637" id="Seg_9785" s="T636">NEG</ta>
            <ta e="T638" id="Seg_9786" s="T637">болеть-PST.[3SG]</ta>
            <ta e="T639" id="Seg_9787" s="T638">рука-NOM/GEN.3SG</ta>
            <ta e="T641" id="Seg_9788" s="T640">мы.NOM</ta>
            <ta e="T642" id="Seg_9789" s="T641">NEG.EX-PST-3PL</ta>
            <ta e="T643" id="Seg_9790" s="T642">доктор-PL</ta>
            <ta e="T644" id="Seg_9791" s="T643">мы.NOM</ta>
            <ta e="T646" id="Seg_9792" s="T645">трава-INS</ta>
            <ta e="T647" id="Seg_9793" s="T646">PTCL</ta>
            <ta e="T648" id="Seg_9794" s="T647">вылечить-PST-1PL</ta>
            <ta e="T649" id="Seg_9795" s="T648">кипятить-DUR.[3SG]</ta>
            <ta e="T650" id="Seg_9796" s="T649">тогда</ta>
            <ta e="T651" id="Seg_9797" s="T650">пить-DUR.[3SG]</ta>
            <ta e="T652" id="Seg_9798" s="T651">один.[NOM.SG]</ta>
            <ta e="T653" id="Seg_9799" s="T652">трава.[NOM.SG]</ta>
            <ta e="T654" id="Seg_9800" s="T653">быть-PRS.[3SG]</ta>
            <ta e="T655" id="Seg_9801" s="T654">вонючий.[NOM.SG]</ta>
            <ta e="T656" id="Seg_9802" s="T655">желудок.[NOM.SG]</ta>
            <ta e="T657" id="Seg_9803" s="T656">болеть-PRS.[3SG]</ta>
            <ta e="T658" id="Seg_9804" s="T657">этот-ACC</ta>
            <ta e="T659" id="Seg_9805" s="T658">кипятить-PRS-2SG</ta>
            <ta e="T660" id="Seg_9806" s="T659">пить-PRS-2SG</ta>
            <ta e="T661" id="Seg_9807" s="T660">тогда</ta>
            <ta e="T662" id="Seg_9808" s="T661">есть-FUT-2SG</ta>
            <ta e="T663" id="Seg_9809" s="T662">а</ta>
            <ta e="T664" id="Seg_9810" s="T663">один.[NOM.SG]</ta>
            <ta e="T667" id="Seg_9811" s="T666">трава.[NOM.SG]</ta>
            <ta e="T668" id="Seg_9812" s="T667">быть-PRS.[3SG]</ta>
            <ta e="T669" id="Seg_9813" s="T668">%%</ta>
            <ta e="T670" id="Seg_9814" s="T669">называть-PRS-3PL</ta>
            <ta e="T671" id="Seg_9815" s="T670">этот-ACC</ta>
            <ta e="T672" id="Seg_9816" s="T671">класть-FUT-2SG</ta>
            <ta e="T673" id="Seg_9817" s="T672">водка-LAT</ta>
            <ta e="T674" id="Seg_9818" s="T673">этот.[NOM.SG]</ta>
            <ta e="T675" id="Seg_9819" s="T674">стоять-IMP-3SG</ta>
            <ta e="T676" id="Seg_9820" s="T675">десять.[NOM.SG]</ta>
            <ta e="T677" id="Seg_9821" s="T676">два.[NOM.SG]</ta>
            <ta e="T678" id="Seg_9822" s="T677">день.[NOM.SG]</ta>
            <ta e="T679" id="Seg_9823" s="T678">тогда</ta>
            <ta e="T680" id="Seg_9824" s="T679">пить-FUT-2SG</ta>
            <ta e="T681" id="Seg_9825" s="T680">сердце-NOM/GEN/ACC.3SG</ta>
            <ta e="T682" id="Seg_9826" s="T681">хороший.[NOM.SG]</ta>
            <ta e="T683" id="Seg_9827" s="T682">стать-FUT-2SG</ta>
            <ta e="T685" id="Seg_9828" s="T684">ты.NOM</ta>
            <ta e="T686" id="Seg_9829" s="T685">мы.NOM</ta>
            <ta e="T687" id="Seg_9830" s="T686">камасинец-PL-NOM/GEN/ACC.3PL</ta>
            <ta e="T688" id="Seg_9831" s="T687">Масленица-LOC</ta>
            <ta e="T689" id="Seg_9832" s="T688">PTCL</ta>
            <ta e="T690" id="Seg_9833" s="T689">лошадь-3SG-INS</ta>
            <ta e="T691" id="Seg_9834" s="T690">скакать-DUR-3PL</ta>
            <ta e="T692" id="Seg_9835" s="T691">кто-GEN</ta>
            <ta e="T693" id="Seg_9836" s="T692">лошадь-NOM/GEN.3SG</ta>
            <ta e="T694" id="Seg_9837" s="T693">скоро</ta>
            <ta e="T695" id="Seg_9838" s="T694">прийти-FUT-3SG</ta>
            <ta e="T696" id="Seg_9839" s="T695">а</ta>
            <ta e="T697" id="Seg_9840" s="T696">кто-GEN</ta>
            <ta e="T698" id="Seg_9841" s="T697">лошадь-NOM/GEN.3SG</ta>
            <ta e="T699" id="Seg_9842" s="T698">остаться-FUT-3SG</ta>
            <ta e="T700" id="Seg_9843" s="T699">этот.[NOM.SG]</ta>
            <ta e="T701" id="Seg_9844" s="T700">кто-GEN</ta>
            <ta e="T702" id="Seg_9845" s="T701">остаться-FUT-3SG</ta>
            <ta e="T703" id="Seg_9846" s="T702">этот.[NOM.SG]</ta>
            <ta e="T706" id="Seg_9847" s="T705">водка.[NOM.SG]</ta>
            <ta e="T707" id="Seg_9848" s="T706">поставить-FUT-3SG</ta>
            <ta e="T709" id="Seg_9849" s="T708">тогда</ta>
            <ta e="T710" id="Seg_9850" s="T709">другой.[NOM.SG]</ta>
            <ta e="T711" id="Seg_9851" s="T710">лошадь.[NOM.SG]</ta>
            <ta e="T712" id="Seg_9852" s="T711">принести-FUT-3PL</ta>
            <ta e="T713" id="Seg_9853" s="T712">а</ta>
            <ta e="T714" id="Seg_9854" s="T713">этот.[NOM.SG]</ta>
            <ta e="T715" id="Seg_9855" s="T714">тот.[NOM.SG]</ta>
            <ta e="T716" id="Seg_9856" s="T715">лошадь.[NOM.SG]</ta>
            <ta e="T717" id="Seg_9857" s="T716">очень</ta>
            <ta e="T719" id="Seg_9858" s="T718">сказать-PRS-3PL</ta>
            <ta e="T720" id="Seg_9859" s="T719">так</ta>
            <ta e="T721" id="Seg_9860" s="T720">тогда</ta>
            <ta e="T722" id="Seg_9861" s="T721">этот-ACC</ta>
            <ta e="T723" id="Seg_9862" s="T722">тоже</ta>
            <ta e="T724" id="Seg_9863" s="T723">остаться-MOM-2DU</ta>
            <ta e="T725" id="Seg_9864" s="T724">здесь</ta>
            <ta e="T728" id="Seg_9865" s="T727">бежать-MOM-FUT-3SG</ta>
            <ta e="T729" id="Seg_9866" s="T728">а</ta>
            <ta e="T730" id="Seg_9867" s="T729">этот</ta>
            <ta e="T731" id="Seg_9868" s="T730">лошадь.[NOM.SG]</ta>
            <ta e="T732" id="Seg_9869" s="T731">остаться-MOM-2DU</ta>
            <ta e="T734" id="Seg_9870" s="T733">тогда</ta>
            <ta e="T735" id="Seg_9871" s="T734">этот</ta>
            <ta e="T736" id="Seg_9872" s="T735">лошадь-ACC</ta>
            <ta e="T739" id="Seg_9873" s="T738">взять-PRS.[3SG]</ta>
            <ta e="T740" id="Seg_9874" s="T739">и</ta>
            <ta e="T741" id="Seg_9875" s="T740">идти-PRS.[3SG]</ta>
            <ta e="T742" id="Seg_9876" s="T741">идти-PRS.[3SG]</ta>
            <ta e="T743" id="Seg_9877" s="T742">мягко</ta>
            <ta e="T744" id="Seg_9878" s="T743">чтобы</ta>
            <ta e="T745" id="Seg_9879" s="T744">NEG</ta>
            <ta e="T746" id="Seg_9880" s="T745">стоять-PST.[3SG]</ta>
            <ta e="T748" id="Seg_9881" s="T747">тогда</ta>
            <ta e="T749" id="Seg_9882" s="T748">водка.[NOM.SG]</ta>
            <ta e="T750" id="Seg_9883" s="T749">много</ta>
            <ta e="T751" id="Seg_9884" s="T750">пить-MOM-FUT-3PL</ta>
            <ta e="T752" id="Seg_9885" s="T751">и</ta>
            <ta e="T753" id="Seg_9886" s="T752">PTCL</ta>
            <ta e="T754" id="Seg_9887" s="T753">упасть-MOM-FUT-3PL</ta>
            <ta e="T756" id="Seg_9888" s="T755">тогда</ta>
            <ta e="T757" id="Seg_9889" s="T756">мужчина-PL</ta>
            <ta e="T758" id="Seg_9890" s="T757">дерево-PL</ta>
            <ta e="T759" id="Seg_9891" s="T758">взять-FUT-3PL</ta>
            <ta e="T760" id="Seg_9892" s="T759">рука-LAT/LOC.3SG</ta>
            <ta e="T761" id="Seg_9893" s="T760">и</ta>
            <ta e="T762" id="Seg_9894" s="T761">тогда</ta>
            <ta e="T769" id="Seg_9895" s="T768">тогда</ta>
            <ta e="T770" id="Seg_9896" s="T769">один.[NOM.SG]</ta>
            <ta e="T771" id="Seg_9897" s="T770">один-LAT</ta>
            <ta e="T772" id="Seg_9898" s="T771">PTCL</ta>
            <ta e="T773" id="Seg_9899" s="T772">%%</ta>
            <ta e="T775" id="Seg_9900" s="T774">%%-DUR.[3SG]</ta>
            <ta e="T776" id="Seg_9901" s="T775">тогда</ta>
            <ta e="T777" id="Seg_9902" s="T776">один.[NOM.SG]</ta>
            <ta e="T778" id="Seg_9903" s="T777">сильный.[NOM.SG]</ta>
            <ta e="T782" id="Seg_9904" s="T781">этот-ACC</ta>
            <ta e="T783" id="Seg_9905" s="T782">PTCL</ta>
            <ta e="T784" id="Seg_9906" s="T783">нога-LAT/LOC.3SG</ta>
            <ta e="T785" id="Seg_9907" s="T784">поставить-FUT-FUT-3SG</ta>
            <ta e="T786" id="Seg_9908" s="T785">этот.[NOM.SG]</ta>
            <ta e="T787" id="Seg_9909" s="T786">PTCL</ta>
            <ta e="T788" id="Seg_9910" s="T787">тогда</ta>
            <ta e="T789" id="Seg_9911" s="T788">водка.[NOM.SG]</ta>
            <ta e="T790" id="Seg_9912" s="T789">стоять-MOM-FUT-3SG</ta>
            <ta e="T791" id="Seg_9913" s="T790">тогда</ta>
            <ta e="T792" id="Seg_9914" s="T791">пить-PRS-3PL</ta>
            <ta e="T794" id="Seg_9915" s="T793">тогда</ta>
            <ta e="T795" id="Seg_9916" s="T794">два-COLL</ta>
            <ta e="T796" id="Seg_9917" s="T795">камасинец-PL</ta>
            <ta e="T798" id="Seg_9918" s="T797">два-COLL</ta>
            <ta e="T799" id="Seg_9919" s="T798">камасинец-PL</ta>
            <ta e="T800" id="Seg_9920" s="T799">взять-FUT-3PL</ta>
            <ta e="T801" id="Seg_9921" s="T800">один.[NOM.SG]</ta>
            <ta e="T802" id="Seg_9922" s="T801">один-LAT</ta>
            <ta e="T803" id="Seg_9923" s="T802">тогда</ta>
            <ta e="T804" id="Seg_9924" s="T803">как</ta>
            <ta e="T805" id="Seg_9925" s="T804">сильный.[NOM.SG]</ta>
            <ta e="T806" id="Seg_9926" s="T805">тот-ACC</ta>
            <ta e="T807" id="Seg_9927" s="T806">выбросить-FUT-3SG</ta>
            <ta e="T808" id="Seg_9928" s="T807">место-LAT</ta>
            <ta e="T809" id="Seg_9929" s="T808">тогда</ta>
            <ta e="T810" id="Seg_9930" s="T809">этот.[NOM.SG]</ta>
            <ta e="T811" id="Seg_9931" s="T810">водка.[NOM.SG]</ta>
            <ta e="T812" id="Seg_9932" s="T811">поставить-PRS.[3SG]</ta>
            <ta e="T813" id="Seg_9933" s="T812">этот-LAT</ta>
            <ta e="T814" id="Seg_9934" s="T813">пить-DUR-3PL</ta>
            <ta e="T816" id="Seg_9935" s="T815">тогда</ta>
            <ta e="T817" id="Seg_9936" s="T816">место-LAT</ta>
            <ta e="T818" id="Seg_9937" s="T817">выбросить-FUT-3SG</ta>
            <ta e="T819" id="Seg_9938" s="T818">рука-ACC.3SG</ta>
            <ta e="T820" id="Seg_9939" s="T819">PTCL</ta>
            <ta e="T821" id="Seg_9940" s="T820">сломать-FUT-3SG</ta>
            <ta e="T824" id="Seg_9941" s="T823">ворота-PL</ta>
            <ta e="T825" id="Seg_9942" s="T824">стоять-DUR-3PL</ta>
            <ta e="T826" id="Seg_9943" s="T825">пойти-EP-IMP.2SG</ta>
            <ta e="T827" id="Seg_9944" s="T826">какой</ta>
            <ta e="T828" id="Seg_9945" s="T827">дорога-LAT</ta>
            <ta e="T829" id="Seg_9946" s="T828">пойти-FUT-2SG</ta>
            <ta e="T830" id="Seg_9947" s="T829">правый</ta>
            <ta e="T831" id="Seg_9948" s="T830">дорога.[NOM.SG]</ta>
            <ta e="T832" id="Seg_9949" s="T831">PTCL</ta>
            <ta e="T833" id="Seg_9950" s="T832">NEG</ta>
            <ta e="T834" id="Seg_9951" s="T833">хороший</ta>
            <ta e="T835" id="Seg_9952" s="T834">PTCL</ta>
            <ta e="T836" id="Seg_9953" s="T835">болото-NOM/GEN/ACC.3PL</ta>
            <ta e="T837" id="Seg_9954" s="T836">дерево-PL</ta>
            <ta e="T838" id="Seg_9955" s="T837">стоять-PRS-3PL</ta>
            <ta e="T839" id="Seg_9956" s="T838">и</ta>
            <ta e="T840" id="Seg_9957" s="T839">PTCL</ta>
            <ta e="T841" id="Seg_9958" s="T840">гроза-PL</ta>
            <ta e="T842" id="Seg_9959" s="T841">ветер.[NOM.SG]</ta>
            <ta e="T843" id="Seg_9960" s="T842">PTCL</ta>
            <ta e="T844" id="Seg_9961" s="T843">железо-ADJZ</ta>
            <ta e="T845" id="Seg_9962" s="T844">колючка-NOM/GEN/ACC.3PL</ta>
            <ta e="T846" id="Seg_9963" s="T845">PTCL</ta>
            <ta e="T847" id="Seg_9964" s="T846">колоть-DUR-3PL</ta>
            <ta e="T848" id="Seg_9965" s="T847">а</ta>
            <ta e="T849" id="Seg_9966" s="T848">там</ta>
            <ta e="T850" id="Seg_9967" s="T849">далеко-LAT.ADV</ta>
            <ta e="T851" id="Seg_9968" s="T850">пойти-FUT-2SG</ta>
            <ta e="T852" id="Seg_9969" s="T851">там</ta>
            <ta e="T853" id="Seg_9970" s="T852">PTCL</ta>
            <ta e="T854" id="Seg_9971" s="T853">хороший</ta>
            <ta e="T855" id="Seg_9972" s="T854">дерево-PL</ta>
            <ta e="T856" id="Seg_9973" s="T855">NEG.EX.[3SG]</ta>
            <ta e="T857" id="Seg_9974" s="T856">что.[NOM.SG]=INDEF</ta>
            <ta e="T858" id="Seg_9975" s="T857">NEG.EX.[3SG]</ta>
            <ta e="T859" id="Seg_9976" s="T858">очень</ta>
            <ta e="T860" id="Seg_9977" s="T859">красивый.[NOM.SG]</ta>
            <ta e="T861" id="Seg_9978" s="T860">трава.[NOM.SG]</ta>
            <ta e="T862" id="Seg_9979" s="T861">красивый.[NOM.SG]</ta>
            <ta e="T864" id="Seg_9980" s="T863">цветок-PL</ta>
            <ta e="T865" id="Seg_9981" s="T864">снег.[NOM.SG]</ta>
            <ta e="T866" id="Seg_9982" s="T865">красный.[NOM.SG]</ta>
            <ta e="T868" id="Seg_9983" s="T867">расти-DUR.[3SG]</ta>
            <ta e="T869" id="Seg_9984" s="T868">яблоня-NOM/GEN/ACC.3PL</ta>
            <ta e="T870" id="Seg_9985" s="T869">солнце.[NOM.SG]</ta>
            <ta e="T871" id="Seg_9986" s="T870">идти-DUR</ta>
            <ta e="T872" id="Seg_9987" s="T871">очень</ta>
            <ta e="T873" id="Seg_9988" s="T872">теплый.[NOM.SG]</ta>
            <ta e="T874" id="Seg_9989" s="T873">хотя</ta>
            <ta e="T875" id="Seg_9990" s="T874">ложиться-IMP.2SG</ta>
            <ta e="T876" id="Seg_9991" s="T875">и</ta>
            <ta e="T877" id="Seg_9992" s="T876">спать-EP-IMP.2SG</ta>
            <ta e="T878" id="Seg_9993" s="T877">хороший</ta>
            <ta e="T879" id="Seg_9994" s="T878">там</ta>
            <ta e="T880" id="Seg_9995" s="T879">а</ta>
            <ta e="T881" id="Seg_9996" s="T880">левый</ta>
            <ta e="T882" id="Seg_9997" s="T881">рука-LAT/LOC.3SG</ta>
            <ta e="T883" id="Seg_9998" s="T882">идти-PRS.[3SG]</ta>
            <ta e="T884" id="Seg_9999" s="T883">очень</ta>
            <ta e="T885" id="Seg_10000" s="T884">дорога</ta>
            <ta e="T886" id="Seg_10001" s="T885">большой.[NOM.SG]</ta>
            <ta e="T887" id="Seg_10002" s="T886">люди.[NOM.SG]</ta>
            <ta e="T888" id="Seg_10003" s="T887">много</ta>
            <ta e="T889" id="Seg_10004" s="T888">весь</ta>
            <ta e="T890" id="Seg_10005" s="T889">водка.[NOM.SG]</ta>
            <ta e="T891" id="Seg_10006" s="T890">пить-PRS-3PL</ta>
            <ta e="T892" id="Seg_10007" s="T891">весь</ta>
            <ta e="T893" id="Seg_10008" s="T892">что.[NOM.SG]</ta>
            <ta e="T894" id="Seg_10009" s="T893">съесть-PRS-3PL</ta>
            <ta e="T895" id="Seg_10010" s="T894">PTCL</ta>
            <ta e="T896" id="Seg_10011" s="T895">прыгнуть-DUR-3PL</ta>
            <ta e="T897" id="Seg_10012" s="T896">PTCL</ta>
            <ta e="T898" id="Seg_10013" s="T897">песня.[NOM.SG]</ta>
            <ta e="T899" id="Seg_10014" s="T898">петь-DUR-3PL</ta>
            <ta e="T900" id="Seg_10015" s="T899">а</ta>
            <ta e="T902" id="Seg_10016" s="T901">там</ta>
            <ta e="T903" id="Seg_10017" s="T902">далеко</ta>
            <ta e="T905" id="Seg_10018" s="T904">пойти-FUT-2SG</ta>
            <ta e="T906" id="Seg_10019" s="T905">там</ta>
            <ta e="T907" id="Seg_10020" s="T906">PTCL</ta>
            <ta e="T908" id="Seg_10021" s="T907">огонь.[NOM.SG]</ta>
            <ta e="T909" id="Seg_10022" s="T908">PTCL</ta>
            <ta e="T910" id="Seg_10023" s="T909">камень-PL</ta>
            <ta e="T911" id="Seg_10024" s="T910">PTCL</ta>
            <ta e="T912" id="Seg_10025" s="T911">гроза-PL</ta>
            <ta e="T913" id="Seg_10026" s="T912">огонь.[NOM.SG]</ta>
            <ta e="T914" id="Seg_10027" s="T913">люди-EP-ACC</ta>
            <ta e="T915" id="Seg_10028" s="T914">PTCL</ta>
            <ta e="T916" id="Seg_10029" s="T915">гореть-DUR.[3SG]</ta>
            <ta e="T917" id="Seg_10030" s="T916">сейчас</ta>
            <ta e="T918" id="Seg_10031" s="T917">PTCL</ta>
            <ta e="T920" id="Seg_10032" s="T919">вы.NOM</ta>
            <ta e="T921" id="Seg_10033" s="T920">пойти-IMP.2PL</ta>
            <ta e="T922" id="Seg_10034" s="T921">а</ta>
            <ta e="T923" id="Seg_10035" s="T922">я.NOM</ta>
            <ta e="T924" id="Seg_10036" s="T923">%%-FUT-1SG</ta>
            <ta e="T925" id="Seg_10037" s="T924">надо</ta>
            <ta e="T926" id="Seg_10038" s="T925">я.LAT</ta>
            <ta e="T927" id="Seg_10039" s="T926">хлеб.[NOM.SG]</ta>
            <ta e="T928" id="Seg_10040" s="T927">поставить-INF.LAT</ta>
            <ta e="T929" id="Seg_10041" s="T928">печь-INF.LAT</ta>
            <ta e="T930" id="Seg_10042" s="T929">хлеб.[NOM.SG]</ta>
            <ta e="T931" id="Seg_10043" s="T930">надо</ta>
            <ta e="T932" id="Seg_10044" s="T931">а.то</ta>
            <ta e="T933" id="Seg_10045" s="T932">съесть-INF.LAT</ta>
            <ta e="T934" id="Seg_10046" s="T933">NEG.EX.[3SG]</ta>
            <ta e="T935" id="Seg_10047" s="T934">хлеб.[NOM.SG]</ta>
            <ta e="T936" id="Seg_10048" s="T935">вы.ACC</ta>
            <ta e="T937" id="Seg_10049" s="T936">дать-FUT-1SG</ta>
            <ta e="T938" id="Seg_10050" s="T937">есть-PST-CVB</ta>
            <ta e="T940" id="Seg_10051" s="T939">NEG.AUX-IMP.2SG</ta>
            <ta e="T941" id="Seg_10052" s="T940">пойти-EP-CNG</ta>
            <ta e="T942" id="Seg_10053" s="T941">мужчина-LAT</ta>
            <ta e="T943" id="Seg_10054" s="T942">жить-IMP.2SG</ta>
            <ta e="T944" id="Seg_10055" s="T943">один-LAT</ta>
            <ta e="T945" id="Seg_10056" s="T944">хороший</ta>
            <ta e="T946" id="Seg_10057" s="T945">жить-INF.LAT</ta>
            <ta e="T947" id="Seg_10058" s="T946">куда=INDEF</ta>
            <ta e="T948" id="Seg_10059" s="T947">пойти-FUT-1SG</ta>
            <ta e="T949" id="Seg_10060" s="T948">кто-LAT=INDEF</ta>
            <ta e="T950" id="Seg_10061" s="T949">NEG</ta>
            <ta e="T951" id="Seg_10062" s="T950">спросить-PST-1SG</ta>
            <ta e="T953" id="Seg_10063" s="T952">вода.[NOM.SG]</ta>
            <ta e="T954" id="Seg_10064" s="T953">течь-DUR.[3SG]</ta>
            <ta e="T955" id="Seg_10065" s="T954">этот.[NOM.SG]</ta>
            <ta e="T956" id="Seg_10066" s="T955">большой.[NOM.SG]</ta>
            <ta e="T957" id="Seg_10067" s="T956">Ильбинь.[NOM.SG]</ta>
            <ta e="T958" id="Seg_10068" s="T957">маленький.[NOM.SG]</ta>
            <ta e="T959" id="Seg_10069" s="T958">Ильбинь.[NOM.SG]</ta>
            <ta e="T960" id="Seg_10070" s="T959">а</ta>
            <ta e="T961" id="Seg_10071" s="T960">где</ta>
            <ta e="T962" id="Seg_10072" s="T961">вода.[NOM.SG]</ta>
            <ta e="T963" id="Seg_10073" s="T962">взять-DUR.[3SG]</ta>
            <ta e="T964" id="Seg_10074" s="T963">там</ta>
            <ta e="T965" id="Seg_10075" s="T964">PTCL</ta>
            <ta e="T966" id="Seg_10076" s="T965">называть-PRS-3PL</ta>
            <ta e="T967" id="Seg_10077" s="T966">PTCL</ta>
            <ta e="T970" id="Seg_10078" s="T969">один.[NOM.SG]</ta>
            <ta e="T971" id="Seg_10079" s="T970">зима.[NOM.SG]</ta>
            <ta e="T972" id="Seg_10080" s="T971">PTCL</ta>
            <ta e="T973" id="Seg_10081" s="T972">мы.NOM</ta>
            <ta e="T974" id="Seg_10082" s="T973">рожь.[NOM.SG]</ta>
            <ta e="T975" id="Seg_10083" s="T974">%%-PST-1PL</ta>
            <ta e="T976" id="Seg_10084" s="T975">этот-PL</ta>
            <ta e="T977" id="Seg_10085" s="T976">PTCL</ta>
            <ta e="T978" id="Seg_10086" s="T977">всегда</ta>
            <ta e="T979" id="Seg_10087" s="T978">сидеть-DUR-3PL</ta>
            <ta e="T980" id="Seg_10088" s="T979">сидеть-DUR-3PL</ta>
            <ta e="T981" id="Seg_10089" s="T980">говорить-DUR-3PL</ta>
            <ta e="T982" id="Seg_10090" s="T981">смеяться-DUR-3PL</ta>
            <ta e="T983" id="Seg_10091" s="T982">пойти-OPT.DU/PL-1PL</ta>
            <ta e="T984" id="Seg_10092" s="T983">%%-INF.LAT</ta>
            <ta e="T985" id="Seg_10093" s="T984">а.то</ta>
            <ta e="T986" id="Seg_10094" s="T985">холодный.[NOM.SG]</ta>
            <ta e="T987" id="Seg_10095" s="T986">стать-FUT-3SG</ta>
            <ta e="T990" id="Seg_10096" s="T989">мы.LAT</ta>
            <ta e="T991" id="Seg_10097" s="T990">надо</ta>
            <ta e="T992" id="Seg_10098" s="T991">%%-INF.LAT</ta>
            <ta e="T993" id="Seg_10099" s="T992">ты.DAT</ta>
            <ta e="T994" id="Seg_10100" s="T993">ты.DAT</ta>
            <ta e="T995" id="Seg_10101" s="T994">всегда</ta>
            <ta e="T996" id="Seg_10102" s="T995">много</ta>
            <ta e="T997" id="Seg_10103" s="T996">нужно</ta>
            <ta e="T998" id="Seg_10104" s="T997">тогда</ta>
            <ta e="T999" id="Seg_10105" s="T998">рука-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T1000" id="Seg_10106" s="T999">PTCL</ta>
            <ta e="T1001" id="Seg_10107" s="T1000">мерзнуть-DUR-3PL</ta>
            <ta e="T1002" id="Seg_10108" s="T1001">снег.[NOM.SG]</ta>
            <ta e="T1003" id="Seg_10109" s="T1002">упасть-MOM-FUT-3SG</ta>
            <ta e="T1004" id="Seg_10110" s="T1003">дерево.[NOM.SG]</ta>
            <ta e="T1005" id="Seg_10111" s="T1004">опять</ta>
            <ta e="T1006" id="Seg_10112" s="T1005">мы.NOM</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_10113" s="T0">pers</ta>
            <ta e="T2" id="Seg_10114" s="T1">adv</ta>
            <ta e="T3" id="Seg_10115" s="T2">pers</ta>
            <ta e="T5" id="Seg_10116" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_10117" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_10118" s="T6">v-v:n.fin</ta>
            <ta e="T8" id="Seg_10119" s="T7">conj</ta>
            <ta e="T9" id="Seg_10120" s="T8">que-n:case=ptcl</ta>
            <ta e="T10" id="Seg_10121" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_10122" s="T10">v-v&gt;v-v:pn</ta>
            <ta e="T12" id="Seg_10123" s="T11">ptcl</ta>
            <ta e="T14" id="Seg_10124" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_10125" s="T14">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_10126" s="T16">adv</ta>
            <ta e="T18" id="Seg_10127" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_10128" s="T18">adj</ta>
            <ta e="T21" id="Seg_10129" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_10130" s="T21">pers</ta>
            <ta e="T23" id="Seg_10131" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_10132" s="T23">conj</ta>
            <ta e="T25" id="Seg_10133" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_10134" s="T25">ptcl</ta>
            <ta e="T28" id="Seg_10135" s="T27">adv</ta>
            <ta e="T29" id="Seg_10136" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_10137" s="T29">v-v:n.fin</ta>
            <ta e="T32" id="Seg_10138" s="T31">que=ptcl</ta>
            <ta e="T33" id="Seg_10139" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_10140" s="T33">v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_10141" s="T34">que-n:case=ptcl</ta>
            <ta e="T36" id="Seg_10142" s="T35">v-v&gt;v-v:pn</ta>
            <ta e="T37" id="Seg_10143" s="T36">adj-n:case</ta>
            <ta e="T38" id="Seg_10144" s="T37">pers</ta>
            <ta e="T39" id="Seg_10145" s="T38">v-v:mood.pn</ta>
            <ta e="T40" id="Seg_10146" s="T39">n-n:case</ta>
            <ta e="T41" id="Seg_10147" s="T40">v-v:mood.pn</ta>
            <ta e="T42" id="Seg_10148" s="T41">aux-v:mood.pn</ta>
            <ta e="T43" id="Seg_10149" s="T42">v-v:ins-v:mood.pn</ta>
            <ta e="T44" id="Seg_10150" s="T43">aux-v:mood.pn</ta>
            <ta e="T45" id="Seg_10151" s="T44">v-v:ins-v:mood.pn</ta>
            <ta e="T46" id="Seg_10152" s="T45">conj</ta>
            <ta e="T47" id="Seg_10153" s="T46">que-n:case</ta>
            <ta e="T48" id="Seg_10154" s="T47">pers</ta>
            <ta e="T49" id="Seg_10155" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_10156" s="T49">v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_10157" s="T50">conj</ta>
            <ta e="T52" id="Seg_10158" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_10159" s="T52">v-v:tense-v:pn</ta>
            <ta e="T54" id="Seg_10160" s="T53">conj</ta>
            <ta e="T56" id="Seg_10161" s="T55">adv</ta>
            <ta e="T57" id="Seg_10162" s="T56">v-v:tense-v:pn</ta>
            <ta e="T58" id="Seg_10163" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_10164" s="T58">v-v:tense-v:pn</ta>
            <ta e="T60" id="Seg_10165" s="T59">adj</ta>
            <ta e="T61" id="Seg_10166" s="T60">v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_10167" s="T61">pers</ta>
            <ta e="T63" id="Seg_10168" s="T62">v-v:mood.pn</ta>
            <ta e="T64" id="Seg_10169" s="T63">v-v:mood.pn</ta>
            <ta e="T65" id="Seg_10170" s="T64">adv</ta>
            <ta e="T66" id="Seg_10171" s="T65">que-n:case=ptcl</ta>
            <ta e="T67" id="Seg_10172" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_10173" s="T67">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_10174" s="T68">dempro-n:case</ta>
            <ta e="T70" id="Seg_10175" s="T69">conj</ta>
            <ta e="T71" id="Seg_10176" s="T70">v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_10177" s="T71">dempro-n:case</ta>
            <ta e="T73" id="Seg_10178" s="T72">adv</ta>
            <ta e="T74" id="Seg_10179" s="T73">adj-n:case</ta>
            <ta e="T75" id="Seg_10180" s="T74">dempro-n:case</ta>
            <ta e="T76" id="Seg_10181" s="T75">adv</ta>
            <ta e="T77" id="Seg_10182" s="T76">adv</ta>
            <ta e="T78" id="Seg_10183" s="T77">v-v:tense-v:pn</ta>
            <ta e="T79" id="Seg_10184" s="T78">v-v:tense-v:pn</ta>
            <ta e="T80" id="Seg_10185" s="T79">conj</ta>
            <ta e="T81" id="Seg_10186" s="T80">adv</ta>
            <ta e="T82" id="Seg_10187" s="T81">adj-n:case</ta>
            <ta e="T83" id="Seg_10188" s="T82">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T84" id="Seg_10189" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_10190" s="T84">v-v:tense-v:pn</ta>
            <ta e="T86" id="Seg_10191" s="T85">adv</ta>
            <ta e="T87" id="Seg_10192" s="T86">v-v:tense-v:pn</ta>
            <ta e="T88" id="Seg_10193" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_10194" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_10195" s="T89">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_10196" s="T90">conj</ta>
            <ta e="T92" id="Seg_10197" s="T91">adv</ta>
            <ta e="T93" id="Seg_10198" s="T92">ptcl</ta>
            <ta e="T97" id="Seg_10199" s="T96">adv</ta>
            <ta e="T98" id="Seg_10200" s="T97">ptcl</ta>
            <ta e="T101" id="Seg_10201" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_10202" s="T101">v-v&gt;v-v:pn</ta>
            <ta e="T103" id="Seg_10203" s="T102">conj</ta>
            <ta e="T104" id="Seg_10204" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_10205" s="T104">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T107" id="Seg_10206" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_10207" s="T107">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T109" id="Seg_10208" s="T108">conj</ta>
            <ta e="T110" id="Seg_10209" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_10210" s="T110">%%</ta>
            <ta e="T112" id="Seg_10211" s="T111">v-v:tense-v:pn</ta>
            <ta e="T113" id="Seg_10212" s="T112">n-n:case.poss</ta>
            <ta e="T114" id="Seg_10213" s="T113">adv</ta>
            <ta e="T115" id="Seg_10214" s="T114">v-v&gt;v-v:pn</ta>
            <ta e="T116" id="Seg_10215" s="T115">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T117" id="Seg_10216" s="T116">pers</ta>
            <ta e="T118" id="Seg_10217" s="T117">n-n:case.poss</ta>
            <ta e="T119" id="Seg_10218" s="T118">n-n:case</ta>
            <ta e="T120" id="Seg_10219" s="T119">v-v:tense-v:pn</ta>
            <ta e="T121" id="Seg_10220" s="T120">n-n:case</ta>
            <ta e="T122" id="Seg_10221" s="T121">v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_10222" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_10223" s="T123">ptcl</ta>
            <ta e="T125" id="Seg_10224" s="T124">v-v:tense-v:pn</ta>
            <ta e="T126" id="Seg_10225" s="T125">v-v:tense-v:pn</ta>
            <ta e="T127" id="Seg_10226" s="T126">n-n:case</ta>
            <ta e="T128" id="Seg_10227" s="T127">v-v:tense-v:pn</ta>
            <ta e="T129" id="Seg_10228" s="T128">dempro-n:case</ta>
            <ta e="T130" id="Seg_10229" s="T129">v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_10230" s="T131">n-n:case</ta>
            <ta e="T133" id="Seg_10231" s="T132">dempro-n:case</ta>
            <ta e="T134" id="Seg_10232" s="T133">ptcl</ta>
            <ta e="T136" id="Seg_10233" s="T135">v-v:tense-v:pn</ta>
            <ta e="T137" id="Seg_10234" s="T136">v-v:tense-v:pn</ta>
            <ta e="T138" id="Seg_10235" s="T137">conj</ta>
            <ta e="T139" id="Seg_10236" s="T138">v-v:tense-v:pn</ta>
            <ta e="T140" id="Seg_10237" s="T139">adv</ta>
            <ta e="T144" id="Seg_10238" s="T143">adv</ta>
            <ta e="T145" id="Seg_10239" s="T144">v-v:tense-v:pn</ta>
            <ta e="T146" id="Seg_10240" s="T145">n-n:num</ta>
            <ta e="T147" id="Seg_10241" s="T146">v-v:tense-v:pn</ta>
            <ta e="T148" id="Seg_10242" s="T147">dempro-n:case</ta>
            <ta e="T149" id="Seg_10243" s="T148">conj</ta>
            <ta e="T150" id="Seg_10244" s="T149">v-v:tense-v:pn</ta>
            <ta e="T151" id="Seg_10245" s="T150">n-n:case</ta>
            <ta e="T152" id="Seg_10246" s="T151">dempro-n:case</ta>
            <ta e="T153" id="Seg_10247" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_10248" s="T153">v-v&gt;v-v:pn</ta>
            <ta e="T155" id="Seg_10249" s="T154">conj</ta>
            <ta e="T156" id="Seg_10250" s="T155">dempro-n:case</ta>
            <ta e="T158" id="Seg_10251" s="T157">v-v&gt;v-v:pn</ta>
            <ta e="T159" id="Seg_10252" s="T158">adv</ta>
            <ta e="T160" id="Seg_10253" s="T159">v-v:tense-v:pn</ta>
            <ta e="T161" id="Seg_10254" s="T160">n-n:case</ta>
            <ta e="T162" id="Seg_10255" s="T161">conj</ta>
            <ta e="T163" id="Seg_10256" s="T162">v-v&gt;v-v:pn</ta>
            <ta e="T165" id="Seg_10257" s="T164">conj</ta>
            <ta e="T166" id="Seg_10258" s="T165">pers</ta>
            <ta e="T167" id="Seg_10259" s="T166">n-n:case.poss</ta>
            <ta e="T168" id="Seg_10260" s="T167">dempro-n:case</ta>
            <ta e="T169" id="Seg_10261" s="T168">v-v:tense-v:pn</ta>
            <ta e="T170" id="Seg_10262" s="T169">pers</ta>
            <ta e="T171" id="Seg_10263" s="T170">num-n:case</ta>
            <ta e="T172" id="Seg_10264" s="T171">num-n:case</ta>
            <ta e="T173" id="Seg_10265" s="T172">n-n:case</ta>
            <ta e="T174" id="Seg_10266" s="T173">v-v:tense-v:pn</ta>
            <ta e="T175" id="Seg_10267" s="T174">adv</ta>
            <ta e="T176" id="Seg_10268" s="T175">dempro</ta>
            <ta e="T178" id="Seg_10269" s="T177">v-v:tense-v:pn</ta>
            <ta e="T179" id="Seg_10270" s="T178">v-v:tense-v:pn</ta>
            <ta e="T180" id="Seg_10271" s="T179">num-n:case</ta>
            <ta e="T181" id="Seg_10272" s="T180">num-n:case</ta>
            <ta e="T182" id="Seg_10273" s="T181">num-n:case</ta>
            <ta e="T183" id="Seg_10274" s="T182">num-n:case</ta>
            <ta e="T184" id="Seg_10275" s="T183">n-n:case</ta>
            <ta e="T185" id="Seg_10276" s="T184">v-v:tense-v:pn</ta>
            <ta e="T186" id="Seg_10277" s="T185">n-n:case.poss</ta>
            <ta e="T187" id="Seg_10278" s="T186">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T188" id="Seg_10279" s="T187">dempro-n:case</ta>
            <ta e="T189" id="Seg_10280" s="T188">v-v:tense-v:pn</ta>
            <ta e="T190" id="Seg_10281" s="T189">conj</ta>
            <ta e="T193" id="Seg_10282" s="T192">n-n:case</ta>
            <ta e="T194" id="Seg_10283" s="T193">v-v:tense-v:pn</ta>
            <ta e="T195" id="Seg_10284" s="T194">dempro-n:case</ta>
            <ta e="T196" id="Seg_10285" s="T195">v-v:tense-v:pn</ta>
            <ta e="T198" id="Seg_10286" s="T197">dempro-n:case</ta>
            <ta e="T199" id="Seg_10287" s="T198">conj</ta>
            <ta e="T200" id="Seg_10288" s="T199">dempro-n:case</ta>
            <ta e="T201" id="Seg_10289" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_10290" s="T201">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T203" id="Seg_10291" s="T202">n-n:case</ta>
            <ta e="T204" id="Seg_10292" s="T203">n-n:case.poss</ta>
            <ta e="T205" id="Seg_10293" s="T204">v-v:tense-v:pn</ta>
            <ta e="T206" id="Seg_10294" s="T205">pers</ta>
            <ta e="T207" id="Seg_10295" s="T206">n-n:case.poss</ta>
            <ta e="T208" id="Seg_10296" s="T207">n-n:case</ta>
            <ta e="T209" id="Seg_10297" s="T208">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T210" id="Seg_10298" s="T209">adv</ta>
            <ta e="T211" id="Seg_10299" s="T210">n-n:case.poss</ta>
            <ta e="T212" id="Seg_10300" s="T211">conj</ta>
            <ta e="T213" id="Seg_10301" s="T212">pers</ta>
            <ta e="T214" id="Seg_10302" s="T213">n-n:case.poss</ta>
            <ta e="T215" id="Seg_10303" s="T214">num-n:case</ta>
            <ta e="T216" id="Seg_10304" s="T215">n-n:num-n:case.poss-n:case</ta>
            <ta e="T217" id="Seg_10305" s="T216">v-v:tense-v:pn</ta>
            <ta e="T218" id="Seg_10306" s="T217">n-n:case.poss</ta>
            <ta e="T219" id="Seg_10307" s="T218">pers</ta>
            <ta e="T220" id="Seg_10308" s="T219">adv</ta>
            <ta e="T221" id="Seg_10309" s="T220">adv</ta>
            <ta e="T222" id="Seg_10310" s="T221">v-v:tense-v:pn</ta>
            <ta e="T223" id="Seg_10311" s="T222">adj-n:case</ta>
            <ta e="T224" id="Seg_10312" s="T223">n-n:case</ta>
            <ta e="T225" id="Seg_10313" s="T224">v-v:tense-v:pn</ta>
            <ta e="T227" id="Seg_10314" s="T226">adv</ta>
            <ta e="T228" id="Seg_10315" s="T227">n-n:case</ta>
            <ta e="T229" id="Seg_10316" s="T228">n-n:case</ta>
            <ta e="T230" id="Seg_10317" s="T229">v-v:tense-v:pn</ta>
            <ta e="T231" id="Seg_10318" s="T230">n-n:case</ta>
            <ta e="T232" id="Seg_10319" s="T231">v-v:tense-v:pn</ta>
            <ta e="T233" id="Seg_10320" s="T232">n-n:case</ta>
            <ta e="T234" id="Seg_10321" s="T233">v-v:tense-v:pn</ta>
            <ta e="T235" id="Seg_10322" s="T234">conj</ta>
            <ta e="T236" id="Seg_10323" s="T235">n-n:case</ta>
            <ta e="T237" id="Seg_10324" s="T236">v-v:tense-v:pn</ta>
            <ta e="T238" id="Seg_10325" s="T237">v-v:tense-v:pn</ta>
            <ta e="T239" id="Seg_10326" s="T238">adv</ta>
            <ta e="T240" id="Seg_10327" s="T239">n-n:case</ta>
            <ta e="T241" id="Seg_10328" s="T240">v-v:tense-v:pn</ta>
            <ta e="T242" id="Seg_10329" s="T241">n-n:case</ta>
            <ta e="T243" id="Seg_10330" s="T242">v-v:tense-v:pn</ta>
            <ta e="T244" id="Seg_10331" s="T243">n-n:case</ta>
            <ta e="T245" id="Seg_10332" s="T244">v-v:tense-v:pn</ta>
            <ta e="T246" id="Seg_10333" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_10334" s="T246">num-n:case</ta>
            <ta e="T248" id="Seg_10335" s="T247">n-n:num</ta>
            <ta e="T249" id="Seg_10336" s="T248">v-v:tense-v:pn</ta>
            <ta e="T250" id="Seg_10337" s="T249">n-n:case</ta>
            <ta e="T251" id="Seg_10338" s="T250">conj</ta>
            <ta e="T252" id="Seg_10339" s="T251">n-n:case</ta>
            <ta e="T253" id="Seg_10340" s="T252">n-n:case</ta>
            <ta e="T254" id="Seg_10341" s="T253">v-v:tense-v:pn</ta>
            <ta e="T255" id="Seg_10342" s="T254">ptcl</ta>
            <ta e="T256" id="Seg_10343" s="T255">v-v:tense-v:pn</ta>
            <ta e="T257" id="Seg_10344" s="T256">adv</ta>
            <ta e="T258" id="Seg_10345" s="T257">n-n:case.poss</ta>
            <ta e="T259" id="Seg_10346" s="T258">v-v:tense-v:pn</ta>
            <ta e="T260" id="Seg_10347" s="T259">dempro-n:case</ta>
            <ta e="T261" id="Seg_10348" s="T260">n-n:case.poss</ta>
            <ta e="T262" id="Seg_10349" s="T261">num-n:case</ta>
            <ta e="T263" id="Seg_10350" s="T262">v-v:tense-v:pn</ta>
            <ta e="T264" id="Seg_10351" s="T263">n-n:case</ta>
            <ta e="T265" id="Seg_10352" s="T264">conj</ta>
            <ta e="T266" id="Seg_10353" s="T265">n-n:case.poss</ta>
            <ta e="T267" id="Seg_10354" s="T266">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T268" id="Seg_10355" s="T267">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T269" id="Seg_10356" s="T268">conj</ta>
            <ta e="T270" id="Seg_10357" s="T269">ptcl</ta>
            <ta e="T271" id="Seg_10358" s="T270">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T272" id="Seg_10359" s="T271">n-adv:case</ta>
            <ta e="T273" id="Seg_10360" s="T272">v-v:tense-v:pn</ta>
            <ta e="T274" id="Seg_10361" s="T273">pers</ta>
            <ta e="T275" id="Seg_10362" s="T274">que-n:case=ptcl</ta>
            <ta e="T276" id="Seg_10363" s="T275">ptcl</ta>
            <ta e="T277" id="Seg_10364" s="T276">v-v:tense-v:pn</ta>
            <ta e="T278" id="Seg_10365" s="T277">conj</ta>
            <ta e="T279" id="Seg_10366" s="T278">dempro</ta>
            <ta e="T280" id="Seg_10367" s="T279">v-v&gt;v-v:pn</ta>
            <ta e="T281" id="Seg_10368" s="T280">pers</ta>
            <ta e="T282" id="Seg_10369" s="T281">ptcl</ta>
            <ta e="T283" id="Seg_10370" s="T282">v-v:tense-v:pn</ta>
            <ta e="T284" id="Seg_10371" s="T283">adv</ta>
            <ta e="T285" id="Seg_10372" s="T284">pers</ta>
            <ta e="T286" id="Seg_10373" s="T285">v-v:tense-v:pn</ta>
            <ta e="T288" id="Seg_10374" s="T287">dempro-n:case</ta>
            <ta e="T289" id="Seg_10375" s="T288">v-v:tense-v:pn</ta>
            <ta e="T293" id="Seg_10376" s="T292">conj</ta>
            <ta e="T294" id="Seg_10377" s="T293">n-n:case.poss</ta>
            <ta e="T295" id="Seg_10378" s="T294">v-v:tense-v:pn</ta>
            <ta e="T299" id="Seg_10379" s="T298">dempro-n:case</ta>
            <ta e="T300" id="Seg_10380" s="T299">n-n:num-n:case.poss</ta>
            <ta e="T301" id="Seg_10381" s="T300">adj-n:case</ta>
            <ta e="T302" id="Seg_10382" s="T301">v-v:tense-v:pn</ta>
            <ta e="T303" id="Seg_10383" s="T302">conj</ta>
            <ta e="T304" id="Seg_10384" s="T303">adv</ta>
            <ta e="T305" id="Seg_10385" s="T304">n-n:case</ta>
            <ta e="T306" id="Seg_10386" s="T305">v-v:n.fin-v:tense-v:pn</ta>
            <ta e="T308" id="Seg_10387" s="T307">dempro-n:case</ta>
            <ta e="T309" id="Seg_10388" s="T308">ptcl</ta>
            <ta e="T310" id="Seg_10389" s="T309">n-n:case.poss</ta>
            <ta e="T311" id="Seg_10390" s="T310">v-v:tense-v:pn</ta>
            <ta e="T312" id="Seg_10391" s="T311">v-v:tense-v:pn</ta>
            <ta e="T313" id="Seg_10392" s="T312">propr-n:case-n:case</ta>
            <ta e="T314" id="Seg_10393" s="T313">n-n:num-n:case.poss</ta>
            <ta e="T315" id="Seg_10394" s="T314">v-v:tense-v:pn</ta>
            <ta e="T316" id="Seg_10395" s="T315">ptcl</ta>
            <ta e="T317" id="Seg_10396" s="T316">ptcl</ta>
            <ta e="T318" id="Seg_10397" s="T317">quant</ta>
            <ta e="T319" id="Seg_10398" s="T318">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T321" id="Seg_10399" s="T320">quant</ta>
            <ta e="T322" id="Seg_10400" s="T321">num-n:case</ta>
            <ta e="T323" id="Seg_10401" s="T322">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T327" id="Seg_10402" s="T326">n-n:case.poss</ta>
            <ta e="T328" id="Seg_10403" s="T327">que-n:case</ta>
            <ta e="T329" id="Seg_10404" s="T328">pers</ta>
            <ta e="T330" id="Seg_10405" s="T329">v-v:tense-v:pn</ta>
            <ta e="T331" id="Seg_10406" s="T330">conj</ta>
            <ta e="T332" id="Seg_10407" s="T331">pers</ta>
            <ta e="T333" id="Seg_10408" s="T332">pers</ta>
            <ta e="T334" id="Seg_10409" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_10410" s="T334">v-v:tense-v:pn</ta>
            <ta e="T336" id="Seg_10411" s="T335">ptcl</ta>
            <ta e="T337" id="Seg_10412" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_10413" s="T337">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T340" id="Seg_10414" s="T339">n-n:num-n:case</ta>
            <ta e="T341" id="Seg_10415" s="T340">v-v:tense-v:pn</ta>
            <ta e="T342" id="Seg_10416" s="T341">adj-n:case</ta>
            <ta e="T343" id="Seg_10417" s="T342">n-n:case</ta>
            <ta e="T345" id="Seg_10418" s="T344">n-n:case</ta>
            <ta e="T346" id="Seg_10419" s="T345">num-n:case</ta>
            <ta e="T347" id="Seg_10420" s="T346">n-n:case</ta>
            <ta e="T348" id="Seg_10421" s="T347">v-v:tense-v:pn</ta>
            <ta e="T349" id="Seg_10422" s="T348">ptcl</ta>
            <ta e="T350" id="Seg_10423" s="T349">adv</ta>
            <ta e="T351" id="Seg_10424" s="T350">n-n:case.poss</ta>
            <ta e="T352" id="Seg_10425" s="T351">v-v:tense-v:pn</ta>
            <ta e="T353" id="Seg_10426" s="T352">v-v:tense-v:pn</ta>
            <ta e="T354" id="Seg_10427" s="T353">conj</ta>
            <ta e="T355" id="Seg_10428" s="T354">v-v&gt;v-v:pn</ta>
            <ta e="T356" id="Seg_10429" s="T355">n-n:case</ta>
            <ta e="T357" id="Seg_10430" s="T356">conj</ta>
            <ta e="T358" id="Seg_10431" s="T357">v-v&gt;v-v:pn</ta>
            <ta e="T359" id="Seg_10432" s="T358">n-n:case</ta>
            <ta e="T360" id="Seg_10433" s="T359">n-n:case</ta>
            <ta e="T361" id="Seg_10434" s="T360">que-n:case</ta>
            <ta e="T362" id="Seg_10435" s="T361">v-v:tense-v:pn</ta>
            <ta e="T363" id="Seg_10436" s="T362">n-n:case</ta>
            <ta e="T364" id="Seg_10437" s="T363">adv</ta>
            <ta e="T365" id="Seg_10438" s="T364">adj-n:case</ta>
            <ta e="T366" id="Seg_10439" s="T365">n-n:case</ta>
            <ta e="T367" id="Seg_10440" s="T366">v-v:tense-v:pn</ta>
            <ta e="T368" id="Seg_10441" s="T367">adv</ta>
            <ta e="T369" id="Seg_10442" s="T368">ptcl</ta>
            <ta e="T370" id="Seg_10443" s="T369">ptcl</ta>
            <ta e="T371" id="Seg_10444" s="T370">v-v:tense-v:pn</ta>
            <ta e="T372" id="Seg_10445" s="T371">adv</ta>
            <ta e="T373" id="Seg_10446" s="T372">adv</ta>
            <ta e="T374" id="Seg_10447" s="T373">num-n:case</ta>
            <ta e="T375" id="Seg_10448" s="T374">n-n:case</ta>
            <ta e="T376" id="Seg_10449" s="T375">adv</ta>
            <ta e="T377" id="Seg_10450" s="T376">num-n:case</ta>
            <ta e="T378" id="Seg_10451" s="T377">n-n:case</ta>
            <ta e="T379" id="Seg_10452" s="T378">num-n:case</ta>
            <ta e="T381" id="Seg_10453" s="T380">%%</ta>
            <ta e="T382" id="Seg_10454" s="T381">num-n:case</ta>
            <ta e="T383" id="Seg_10455" s="T382">n-n:case</ta>
            <ta e="T384" id="Seg_10456" s="T383">v-v&gt;v-v:pn</ta>
            <ta e="T385" id="Seg_10457" s="T384">adv</ta>
            <ta e="T386" id="Seg_10458" s="T385">n-n:case</ta>
            <ta e="T387" id="Seg_10459" s="T386">conj</ta>
            <ta e="T388" id="Seg_10460" s="T387">adv</ta>
            <ta e="T389" id="Seg_10461" s="T388">ptcl</ta>
            <ta e="T1011" id="Seg_10462" s="T391">propr</ta>
            <ta e="T392" id="Seg_10463" s="T1011">n-n:case</ta>
            <ta e="T394" id="Seg_10464" s="T393">v-v:tense-v:pn</ta>
            <ta e="T395" id="Seg_10465" s="T394">dempro-n:num</ta>
            <ta e="T396" id="Seg_10466" s="T395">adv</ta>
            <ta e="T397" id="Seg_10467" s="T396">v-v:tense-v:pn</ta>
            <ta e="T398" id="Seg_10468" s="T397">conj</ta>
            <ta e="T399" id="Seg_10469" s="T398">adv</ta>
            <ta e="T400" id="Seg_10470" s="T399">n-n:case</ta>
            <ta e="T401" id="Seg_10471" s="T400">v-v:tense-v:pn</ta>
            <ta e="T402" id="Seg_10472" s="T401">que-n:case</ta>
            <ta e="T403" id="Seg_10473" s="T402">n-n:case</ta>
            <ta e="T404" id="Seg_10474" s="T403">quant</ta>
            <ta e="T405" id="Seg_10475" s="T404">ptcl</ta>
            <ta e="T406" id="Seg_10476" s="T405">quant</ta>
            <ta e="T407" id="Seg_10477" s="T406">v-v:tense-v:pn</ta>
            <ta e="T408" id="Seg_10478" s="T407">num-n:case</ta>
            <ta e="T410" id="Seg_10479" s="T409">conj</ta>
            <ta e="T411" id="Seg_10480" s="T410">que-n:case</ta>
            <ta e="T412" id="Seg_10481" s="T411">adv</ta>
            <ta e="T413" id="Seg_10482" s="T412">num-n:case</ta>
            <ta e="T414" id="Seg_10483" s="T413">v-v:tense-v:pn</ta>
            <ta e="T415" id="Seg_10484" s="T414">que-n:case</ta>
            <ta e="T416" id="Seg_10485" s="T415">adv</ta>
            <ta e="T417" id="Seg_10486" s="T416">adv</ta>
            <ta e="T418" id="Seg_10487" s="T417">num-n:case</ta>
            <ta e="T419" id="Seg_10488" s="T418">v-v:tense-v:pn</ta>
            <ta e="T420" id="Seg_10489" s="T419">conj</ta>
            <ta e="T421" id="Seg_10490" s="T420">adv</ta>
            <ta e="T422" id="Seg_10491" s="T421">adj-n:case</ta>
            <ta e="T423" id="Seg_10492" s="T422">n-n:case</ta>
            <ta e="T424" id="Seg_10493" s="T423">v-v:tense-v:pn</ta>
            <ta e="T425" id="Seg_10494" s="T424">adj-n:case</ta>
            <ta e="T426" id="Seg_10495" s="T425">adj-n:case</ta>
            <ta e="T427" id="Seg_10496" s="T426">n-n:case</ta>
            <ta e="T428" id="Seg_10497" s="T427">v-v:tense-v:pn</ta>
            <ta e="T429" id="Seg_10498" s="T428">num-n:case</ta>
            <ta e="T430" id="Seg_10499" s="T429">v-v:tense-v:pn</ta>
            <ta e="T431" id="Seg_10500" s="T430">adv</ta>
            <ta e="T432" id="Seg_10501" s="T431">adv</ta>
            <ta e="T433" id="Seg_10502" s="T432">ptcl</ta>
            <ta e="T434" id="Seg_10503" s="T433">ptcl</ta>
            <ta e="T435" id="Seg_10504" s="T434">ptcl</ta>
            <ta e="T436" id="Seg_10505" s="T435">v-v:tense-v:pn</ta>
            <ta e="T437" id="Seg_10506" s="T436">v-v:tense-v:pn</ta>
            <ta e="T438" id="Seg_10507" s="T437">ptcl</ta>
            <ta e="T439" id="Seg_10508" s="T438">%%</ta>
            <ta e="T440" id="Seg_10509" s="T439">adv</ta>
            <ta e="T441" id="Seg_10510" s="T440">v-v&gt;v-v:pn</ta>
            <ta e="T442" id="Seg_10511" s="T441">v-v&gt;v-v:pn</ta>
            <ta e="T443" id="Seg_10512" s="T442">ptcl</ta>
            <ta e="T444" id="Seg_10513" s="T443">v-v&gt;v-v:pn</ta>
            <ta e="T445" id="Seg_10514" s="T444">n-n:case</ta>
            <ta e="T446" id="Seg_10515" s="T445">v-v&gt;v-v:pn</ta>
            <ta e="T451" id="Seg_10516" s="T450">v-v:tense-v:pn</ta>
            <ta e="T453" id="Seg_10517" s="T451">propr-n:case</ta>
            <ta e="T455" id="Seg_10518" s="T454">v-v:tense-v:pn</ta>
            <ta e="T456" id="Seg_10519" s="T455">adj-n:case</ta>
            <ta e="T457" id="Seg_10520" s="T456">n-n:case</ta>
            <ta e="T458" id="Seg_10521" s="T457">adv</ta>
            <ta e="T459" id="Seg_10522" s="T458">n-n:case</ta>
            <ta e="T460" id="Seg_10523" s="T459">v-v:tense-v:pn</ta>
            <ta e="T461" id="Seg_10524" s="T460">ptcl</ta>
            <ta e="T462" id="Seg_10525" s="T461">adj</ta>
            <ta e="T463" id="Seg_10526" s="T462">n-n:case</ta>
            <ta e="T464" id="Seg_10527" s="T463">v-v:tense-v:pn</ta>
            <ta e="T465" id="Seg_10528" s="T464">dempro-n:case</ta>
            <ta e="T466" id="Seg_10529" s="T465">n-n:case</ta>
            <ta e="T1010" id="Seg_10530" s="T466">v-v:n-fin</ta>
            <ta e="T467" id="Seg_10531" s="T1010">v-v:tense-v:pn</ta>
            <ta e="T468" id="Seg_10532" s="T467">propr-n:case</ta>
            <ta e="T469" id="Seg_10533" s="T468">conj</ta>
            <ta e="T470" id="Seg_10534" s="T469">adv</ta>
            <ta e="T471" id="Seg_10535" s="T470">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T472" id="Seg_10536" s="T471">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T474" id="Seg_10537" s="T473">dempro-n:case</ta>
            <ta e="T475" id="Seg_10538" s="T474">n-n:case.poss</ta>
            <ta e="T477" id="Seg_10539" s="T476">n-n:case.poss</ta>
            <ta e="T478" id="Seg_10540" s="T477">v-v:tense-v:pn</ta>
            <ta e="T479" id="Seg_10541" s="T478">adv</ta>
            <ta e="T480" id="Seg_10542" s="T479">num-n:case</ta>
            <ta e="T481" id="Seg_10543" s="T480">n-n:case</ta>
            <ta e="T482" id="Seg_10544" s="T481">dempro-n:case</ta>
            <ta e="T483" id="Seg_10545" s="T482">v-v:tense-v:pn</ta>
            <ta e="T484" id="Seg_10546" s="T483">v-v:mood-v:pn</ta>
            <ta e="T486" id="Seg_10547" s="T485">pers-n:case</ta>
            <ta e="T487" id="Seg_10548" s="T486">adv</ta>
            <ta e="T488" id="Seg_10549" s="T487">v-v:tense-v:pn</ta>
            <ta e="T489" id="Seg_10550" s="T488">v-v:tense-v:pn</ta>
            <ta e="T490" id="Seg_10551" s="T489">v-v:tense-v:pn</ta>
            <ta e="T491" id="Seg_10552" s="T490">ptcl</ta>
            <ta e="T492" id="Seg_10553" s="T491">%%</ta>
            <ta e="T493" id="Seg_10554" s="T492">ptcl</ta>
            <ta e="T495" id="Seg_10555" s="T494">adv</ta>
            <ta e="T496" id="Seg_10556" s="T495">n-n:case</ta>
            <ta e="T498" id="Seg_10557" s="T497">v-v:tense-v:pn</ta>
            <ta e="T499" id="Seg_10558" s="T498">adv</ta>
            <ta e="T500" id="Seg_10559" s="T499">n-n:case.poss</ta>
            <ta e="T501" id="Seg_10560" s="T500">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T502" id="Seg_10561" s="T501">conj</ta>
            <ta e="T503" id="Seg_10562" s="T502">dempro-n:case</ta>
            <ta e="T504" id="Seg_10563" s="T503">n-n:case.poss</ta>
            <ta e="T505" id="Seg_10564" s="T504">v-v:tense-v:pn</ta>
            <ta e="T506" id="Seg_10565" s="T505">refl-n:case.poss</ta>
            <ta e="T507" id="Seg_10566" s="T506">n-n:case</ta>
            <ta e="T508" id="Seg_10567" s="T507">v-v:tense-v:pn</ta>
            <ta e="T510" id="Seg_10568" s="T509">dempro-n:num</ta>
            <ta e="T511" id="Seg_10569" s="T510">adv</ta>
            <ta e="T512" id="Seg_10570" s="T511">n-n:case</ta>
            <ta e="T513" id="Seg_10571" s="T512">quant</ta>
            <ta e="T514" id="Seg_10572" s="T513">v-v&gt;v-v:pn</ta>
            <ta e="T515" id="Seg_10573" s="T514">adv</ta>
            <ta e="T516" id="Seg_10574" s="T515">conj</ta>
            <ta e="T518" id="Seg_10575" s="T517">adv</ta>
            <ta e="T519" id="Seg_10576" s="T518">v-v&gt;v-v:pn</ta>
            <ta e="T520" id="Seg_10577" s="T519">v-v&gt;v-v:pn</ta>
            <ta e="T521" id="Seg_10578" s="T520">adv</ta>
            <ta e="T522" id="Seg_10579" s="T521">v-v&gt;v-v:pn</ta>
            <ta e="T523" id="Seg_10580" s="T522">que</ta>
            <ta e="T524" id="Seg_10581" s="T523">v-v:tense-v:pn</ta>
            <ta e="T525" id="Seg_10582" s="T524">ptcl</ta>
            <ta e="T526" id="Seg_10583" s="T525">adv</ta>
            <ta e="T527" id="Seg_10584" s="T526">v-v:tense-v:pn</ta>
            <ta e="T528" id="Seg_10585" s="T527">ptcl</ta>
            <ta e="T530" id="Seg_10586" s="T529">adv</ta>
            <ta e="T531" id="Seg_10587" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_10588" s="T531">v-v&gt;v-v:pn</ta>
            <ta e="T533" id="Seg_10589" s="T532">v-v&gt;v-v:pn</ta>
            <ta e="T534" id="Seg_10590" s="T533">adv</ta>
            <ta e="T535" id="Seg_10591" s="T534">ptcl</ta>
            <ta e="T536" id="Seg_10592" s="T535">v-v&gt;v-v:pn</ta>
            <ta e="T538" id="Seg_10593" s="T537">que-n:case</ta>
            <ta e="T539" id="Seg_10594" s="T538">ptcl</ta>
            <ta e="T540" id="Seg_10595" s="T539">quant</ta>
            <ta e="T542" id="Seg_10596" s="T541">v-v:tense-v:pn</ta>
            <ta e="T543" id="Seg_10597" s="T542">v-v:tense-v:pn</ta>
            <ta e="T544" id="Seg_10598" s="T543">adv</ta>
            <ta e="T545" id="Seg_10599" s="T544">n-n:case</ta>
            <ta e="T546" id="Seg_10600" s="T545">ptcl</ta>
            <ta e="T547" id="Seg_10601" s="T546">v-v&gt;v-v:pn</ta>
            <ta e="T548" id="Seg_10602" s="T547">dempro</ta>
            <ta e="T549" id="Seg_10603" s="T548">dempro-n:case</ta>
            <ta e="T550" id="Seg_10604" s="T549">n-n:case</ta>
            <ta e="T551" id="Seg_10605" s="T550">ptcl</ta>
            <ta e="T552" id="Seg_10606" s="T551">v-v&gt;v-v:pn</ta>
            <ta e="T553" id="Seg_10607" s="T552">adv</ta>
            <ta e="T554" id="Seg_10608" s="T553">dempro-n:case</ta>
            <ta e="T556" id="Seg_10609" s="T555">v-v:tense-v:pn</ta>
            <ta e="T558" id="Seg_10610" s="T557">num-n:case</ta>
            <ta e="T559" id="Seg_10611" s="T558">num-n:case</ta>
            <ta e="T560" id="Seg_10612" s="T559">pers</ta>
            <ta e="T561" id="Seg_10613" s="T560">n-n:case.poss</ta>
            <ta e="T562" id="Seg_10614" s="T561">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T563" id="Seg_10615" s="T562">ptcl</ta>
            <ta e="T564" id="Seg_10616" s="T563">adv</ta>
            <ta e="T565" id="Seg_10617" s="T564">v-v&gt;v-v:pn</ta>
            <ta e="T566" id="Seg_10618" s="T565">pers</ta>
            <ta e="T567" id="Seg_10619" s="T566">adv</ta>
            <ta e="T568" id="Seg_10620" s="T567">v-v:tense-v:pn</ta>
            <ta e="T569" id="Seg_10621" s="T568">n-n:case</ta>
            <ta e="T570" id="Seg_10622" s="T569">v-v:tense-v:pn</ta>
            <ta e="T571" id="Seg_10623" s="T570">dempro-n:case</ta>
            <ta e="T573" id="Seg_10624" s="T572">adv</ta>
            <ta e="T574" id="Seg_10625" s="T573">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T575" id="Seg_10626" s="T574">pers</ta>
            <ta e="T576" id="Seg_10627" s="T575">n-n:case</ta>
            <ta e="T577" id="Seg_10628" s="T576">v-v:tense-v:pn</ta>
            <ta e="T580" id="Seg_10629" s="T579">adv</ta>
            <ta e="T581" id="Seg_10630" s="T580">v-v:tense-v:pn</ta>
            <ta e="T582" id="Seg_10631" s="T581">v-v:tense-v:pn</ta>
            <ta e="T583" id="Seg_10632" s="T582">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T584" id="Seg_10633" s="T583">v-v:tense-v:pn</ta>
            <ta e="T586" id="Seg_10634" s="T585">n-n:case.poss</ta>
            <ta e="T587" id="Seg_10635" s="T586">ptcl</ta>
            <ta e="T588" id="Seg_10636" s="T587">v-v:tense-v:pn</ta>
            <ta e="T589" id="Seg_10637" s="T588">conj</ta>
            <ta e="T590" id="Seg_10638" s="T589">adv</ta>
            <ta e="T591" id="Seg_10639" s="T590">n-n:case.poss</ta>
            <ta e="T592" id="Seg_10640" s="T591">v-v:tense-v:pn</ta>
            <ta e="T593" id="Seg_10641" s="T592">pers</ta>
            <ta e="T594" id="Seg_10642" s="T593">ptcl</ta>
            <ta e="T595" id="Seg_10643" s="T594">v-v:tense-v:pn</ta>
            <ta e="T596" id="Seg_10644" s="T595">n-n:case.poss-n:case</ta>
            <ta e="T597" id="Seg_10645" s="T596">n-n:case.poss</ta>
            <ta e="T598" id="Seg_10646" s="T597">v-v:tense-v:pn</ta>
            <ta e="T599" id="Seg_10647" s="T598">v-v:tense-v:pn</ta>
            <ta e="T600" id="Seg_10648" s="T599">dempro-n:case</ta>
            <ta e="T602" id="Seg_10649" s="T601">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T603" id="Seg_10650" s="T602">dempro</ta>
            <ta e="T604" id="Seg_10651" s="T603">n-n:case</ta>
            <ta e="T605" id="Seg_10652" s="T604">n-n:case.poss</ta>
            <ta e="T606" id="Seg_10653" s="T605">ptcl</ta>
            <ta e="T607" id="Seg_10654" s="T606">v-v:tense-v:pn</ta>
            <ta e="T609" id="Seg_10655" s="T608">num-n:case</ta>
            <ta e="T610" id="Seg_10656" s="T609">num-n:case</ta>
            <ta e="T611" id="Seg_10657" s="T610">n-n:case</ta>
            <ta e="T612" id="Seg_10658" s="T611">pers</ta>
            <ta e="T613" id="Seg_10659" s="T612">n-n:case</ta>
            <ta e="T614" id="Seg_10660" s="T613">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T615" id="Seg_10661" s="T614">n-n:case.poss-n:case</ta>
            <ta e="T616" id="Seg_10662" s="T615">adv</ta>
            <ta e="T617" id="Seg_10663" s="T616">n-n:case</ta>
            <ta e="T618" id="Seg_10664" s="T617">v-v:tense-v:pn</ta>
            <ta e="T619" id="Seg_10665" s="T618">n-n:case.poss</ta>
            <ta e="T621" id="Seg_10666" s="T620">v-v:n.fin</ta>
            <ta e="T622" id="Seg_10667" s="T621">conj</ta>
            <ta e="T623" id="Seg_10668" s="T622">dempro-n:case</ta>
            <ta e="T625" id="Seg_10669" s="T624">que=ptcl</ta>
            <ta e="T626" id="Seg_10670" s="T625">n-n:case.poss</ta>
            <ta e="T627" id="Seg_10671" s="T626">ptcl</ta>
            <ta e="T628" id="Seg_10672" s="T627">v-v:tense-v:pn</ta>
            <ta e="T629" id="Seg_10673" s="T628">pers</ta>
            <ta e="T630" id="Seg_10674" s="T629">v-v&gt;v-v:pn</ta>
            <ta e="T631" id="Seg_10675" s="T630">v-v:mood.pn</ta>
            <ta e="T632" id="Seg_10676" s="T631">dempro-n:case</ta>
            <ta e="T633" id="Seg_10677" s="T632">v-v:tense-v:pn</ta>
            <ta e="T634" id="Seg_10678" s="T633">conj</ta>
            <ta e="T635" id="Seg_10679" s="T634">v-v:tense-v:pn</ta>
            <ta e="T636" id="Seg_10680" s="T635">adv</ta>
            <ta e="T637" id="Seg_10681" s="T636">ptcl</ta>
            <ta e="T638" id="Seg_10682" s="T637">v-v:tense-v:pn</ta>
            <ta e="T639" id="Seg_10683" s="T638">n-n:case.poss</ta>
            <ta e="T641" id="Seg_10684" s="T640">pers</ta>
            <ta e="T642" id="Seg_10685" s="T641">v-v:tense-v:pn</ta>
            <ta e="T643" id="Seg_10686" s="T642">n-n:num</ta>
            <ta e="T644" id="Seg_10687" s="T643">pers</ta>
            <ta e="T646" id="Seg_10688" s="T645">n-n:case</ta>
            <ta e="T647" id="Seg_10689" s="T646">ptcl</ta>
            <ta e="T648" id="Seg_10690" s="T647">v-v:tense-v:pn</ta>
            <ta e="T649" id="Seg_10691" s="T648">v-v&gt;v-v:pn</ta>
            <ta e="T650" id="Seg_10692" s="T649">adv</ta>
            <ta e="T651" id="Seg_10693" s="T650">v-v&gt;v-v:pn</ta>
            <ta e="T652" id="Seg_10694" s="T651">num-n:case</ta>
            <ta e="T653" id="Seg_10695" s="T652">n-n:case</ta>
            <ta e="T654" id="Seg_10696" s="T653">v-v:tense-v:pn</ta>
            <ta e="T655" id="Seg_10697" s="T654">adj-n:case</ta>
            <ta e="T656" id="Seg_10698" s="T655">n-n:case</ta>
            <ta e="T657" id="Seg_10699" s="T656">v-v:tense-v:pn</ta>
            <ta e="T658" id="Seg_10700" s="T657">dempro-n:case</ta>
            <ta e="T659" id="Seg_10701" s="T658">v-v:tense-v:pn</ta>
            <ta e="T660" id="Seg_10702" s="T659">v-v:tense-v:pn</ta>
            <ta e="T661" id="Seg_10703" s="T660">adv</ta>
            <ta e="T662" id="Seg_10704" s="T661">v-v:tense-v:pn</ta>
            <ta e="T663" id="Seg_10705" s="T662">conj</ta>
            <ta e="T664" id="Seg_10706" s="T663">num-n:case</ta>
            <ta e="T667" id="Seg_10707" s="T666">n-n:case</ta>
            <ta e="T668" id="Seg_10708" s="T667">v-v:tense-v:pn</ta>
            <ta e="T669" id="Seg_10709" s="T668">n</ta>
            <ta e="T670" id="Seg_10710" s="T669">v-v:tense-v:pn</ta>
            <ta e="T671" id="Seg_10711" s="T670">dempro-n:case</ta>
            <ta e="T672" id="Seg_10712" s="T671">v-v:tense-v:pn</ta>
            <ta e="T673" id="Seg_10713" s="T672">n-n:case</ta>
            <ta e="T674" id="Seg_10714" s="T673">dempro-n:case</ta>
            <ta e="T675" id="Seg_10715" s="T674">v-v:mood-v:pn</ta>
            <ta e="T676" id="Seg_10716" s="T675">num-n:case</ta>
            <ta e="T677" id="Seg_10717" s="T676">num-n:case</ta>
            <ta e="T678" id="Seg_10718" s="T677">n-n:case</ta>
            <ta e="T679" id="Seg_10719" s="T678">adv</ta>
            <ta e="T680" id="Seg_10720" s="T679">v-v:tense-v:pn</ta>
            <ta e="T681" id="Seg_10721" s="T680">n-n:case.poss</ta>
            <ta e="T682" id="Seg_10722" s="T681">adj-n:case</ta>
            <ta e="T683" id="Seg_10723" s="T682">v-v:tense-v:pn</ta>
            <ta e="T685" id="Seg_10724" s="T684">pers</ta>
            <ta e="T686" id="Seg_10725" s="T685">pers</ta>
            <ta e="T687" id="Seg_10726" s="T686">n-n:num-n:case.poss</ta>
            <ta e="T688" id="Seg_10727" s="T687">n-n:case</ta>
            <ta e="T689" id="Seg_10728" s="T688">ptcl</ta>
            <ta e="T690" id="Seg_10729" s="T689">n-n:case.poss-n:case</ta>
            <ta e="T691" id="Seg_10730" s="T690">v-v&gt;v-v:pn</ta>
            <ta e="T692" id="Seg_10731" s="T691">que-n:case</ta>
            <ta e="T693" id="Seg_10732" s="T692">n-n:case.poss</ta>
            <ta e="T694" id="Seg_10733" s="T693">adv</ta>
            <ta e="T695" id="Seg_10734" s="T694">v-v:tense-v:pn</ta>
            <ta e="T696" id="Seg_10735" s="T695">conj</ta>
            <ta e="T697" id="Seg_10736" s="T696">que-n:case</ta>
            <ta e="T698" id="Seg_10737" s="T697">n-n:case.poss</ta>
            <ta e="T699" id="Seg_10738" s="T698">v-v:tense-v:pn</ta>
            <ta e="T700" id="Seg_10739" s="T699">dempro-n:case</ta>
            <ta e="T701" id="Seg_10740" s="T700">que-n:case</ta>
            <ta e="T702" id="Seg_10741" s="T701">v-v:tense-v:pn</ta>
            <ta e="T703" id="Seg_10742" s="T702">dempro-n:case</ta>
            <ta e="T706" id="Seg_10743" s="T705">n-n:case</ta>
            <ta e="T707" id="Seg_10744" s="T706">v-v:tense-v:pn</ta>
            <ta e="T709" id="Seg_10745" s="T708">adv</ta>
            <ta e="T710" id="Seg_10746" s="T709">adj-n:case</ta>
            <ta e="T711" id="Seg_10747" s="T710">n-n:case</ta>
            <ta e="T712" id="Seg_10748" s="T711">v-v:tense-v:pn</ta>
            <ta e="T713" id="Seg_10749" s="T712">conj</ta>
            <ta e="T714" id="Seg_10750" s="T713">dempro-n:case</ta>
            <ta e="T715" id="Seg_10751" s="T714">dempro-n:case</ta>
            <ta e="T716" id="Seg_10752" s="T715">n-n:case</ta>
            <ta e="T717" id="Seg_10753" s="T716">adv</ta>
            <ta e="T719" id="Seg_10754" s="T718">v-v:tense-v:pn</ta>
            <ta e="T720" id="Seg_10755" s="T719">ptcl</ta>
            <ta e="T721" id="Seg_10756" s="T720">adv</ta>
            <ta e="T722" id="Seg_10757" s="T721">dempro-n:case</ta>
            <ta e="T723" id="Seg_10758" s="T722">ptcl</ta>
            <ta e="T724" id="Seg_10759" s="T723">v-v&gt;v-v:pn</ta>
            <ta e="T725" id="Seg_10760" s="T724">adv</ta>
            <ta e="T728" id="Seg_10761" s="T727">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T729" id="Seg_10762" s="T728">conj</ta>
            <ta e="T730" id="Seg_10763" s="T729">dempro</ta>
            <ta e="T731" id="Seg_10764" s="T730">n-n:case</ta>
            <ta e="T732" id="Seg_10765" s="T731">v-v&gt;v-v:pn</ta>
            <ta e="T734" id="Seg_10766" s="T733">adv</ta>
            <ta e="T735" id="Seg_10767" s="T734">dempro</ta>
            <ta e="T736" id="Seg_10768" s="T735">n-n:case</ta>
            <ta e="T739" id="Seg_10769" s="T738">v-v:tense-v:pn</ta>
            <ta e="T740" id="Seg_10770" s="T739">conj</ta>
            <ta e="T741" id="Seg_10771" s="T740">v-v:tense-v:pn</ta>
            <ta e="T742" id="Seg_10772" s="T741">v-v:tense-v:pn</ta>
            <ta e="T743" id="Seg_10773" s="T742">adv</ta>
            <ta e="T744" id="Seg_10774" s="T743">conj</ta>
            <ta e="T745" id="Seg_10775" s="T744">ptcl</ta>
            <ta e="T746" id="Seg_10776" s="T745">v-v:tense-v:pn</ta>
            <ta e="T748" id="Seg_10777" s="T747">adv</ta>
            <ta e="T749" id="Seg_10778" s="T748">n-n:case</ta>
            <ta e="T750" id="Seg_10779" s="T749">quant</ta>
            <ta e="T751" id="Seg_10780" s="T750">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T752" id="Seg_10781" s="T751">conj</ta>
            <ta e="T753" id="Seg_10782" s="T752">ptcl</ta>
            <ta e="T754" id="Seg_10783" s="T753">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T756" id="Seg_10784" s="T755">adv</ta>
            <ta e="T757" id="Seg_10785" s="T756">n-n:num</ta>
            <ta e="T758" id="Seg_10786" s="T757">n-n:num</ta>
            <ta e="T759" id="Seg_10787" s="T758">v-v:tense-v:pn</ta>
            <ta e="T760" id="Seg_10788" s="T759">n-n:case.poss</ta>
            <ta e="T761" id="Seg_10789" s="T760">conj</ta>
            <ta e="T762" id="Seg_10790" s="T761">adv</ta>
            <ta e="T769" id="Seg_10791" s="T768">adv</ta>
            <ta e="T770" id="Seg_10792" s="T769">num-n:case</ta>
            <ta e="T771" id="Seg_10793" s="T770">num-n:case</ta>
            <ta e="T772" id="Seg_10794" s="T771">ptcl</ta>
            <ta e="T773" id="Seg_10795" s="T772">%%</ta>
            <ta e="T775" id="Seg_10796" s="T774">v-v&gt;v-v:pn</ta>
            <ta e="T776" id="Seg_10797" s="T775">adv</ta>
            <ta e="T777" id="Seg_10798" s="T776">num-n:case</ta>
            <ta e="T778" id="Seg_10799" s="T777">adj-n:case</ta>
            <ta e="T782" id="Seg_10800" s="T781">dempro-n:case</ta>
            <ta e="T783" id="Seg_10801" s="T782">ptcl</ta>
            <ta e="T784" id="Seg_10802" s="T783">n-n:case.poss</ta>
            <ta e="T785" id="Seg_10803" s="T784">v-v:tense-v:tense-v:pn</ta>
            <ta e="T786" id="Seg_10804" s="T785">dempro-n:case</ta>
            <ta e="T787" id="Seg_10805" s="T786">ptcl</ta>
            <ta e="T788" id="Seg_10806" s="T787">adv</ta>
            <ta e="T789" id="Seg_10807" s="T788">n-n:case</ta>
            <ta e="T790" id="Seg_10808" s="T789">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T791" id="Seg_10809" s="T790">adv</ta>
            <ta e="T792" id="Seg_10810" s="T791">v-v:tense-v:pn</ta>
            <ta e="T794" id="Seg_10811" s="T793">adv</ta>
            <ta e="T795" id="Seg_10812" s="T794">num-num&gt;num</ta>
            <ta e="T796" id="Seg_10813" s="T795">n-n:num</ta>
            <ta e="T798" id="Seg_10814" s="T797">num-num&gt;num</ta>
            <ta e="T799" id="Seg_10815" s="T798">n-n:num</ta>
            <ta e="T800" id="Seg_10816" s="T799">v-v:tense-v:pn</ta>
            <ta e="T801" id="Seg_10817" s="T800">num-n:case</ta>
            <ta e="T802" id="Seg_10818" s="T801">num-n:case</ta>
            <ta e="T803" id="Seg_10819" s="T802">adv</ta>
            <ta e="T804" id="Seg_10820" s="T803">que</ta>
            <ta e="T805" id="Seg_10821" s="T804">adj-n:case</ta>
            <ta e="T806" id="Seg_10822" s="T805">dempro-n:case</ta>
            <ta e="T807" id="Seg_10823" s="T806">v-v:tense-v:pn</ta>
            <ta e="T808" id="Seg_10824" s="T807">n-n:case</ta>
            <ta e="T809" id="Seg_10825" s="T808">adv</ta>
            <ta e="T810" id="Seg_10826" s="T809">dempro-n:case</ta>
            <ta e="T811" id="Seg_10827" s="T810">n-n:case</ta>
            <ta e="T812" id="Seg_10828" s="T811">v-v:tense-v:pn</ta>
            <ta e="T813" id="Seg_10829" s="T812">dempro-n:case</ta>
            <ta e="T814" id="Seg_10830" s="T813">v-v&gt;v-v:pn</ta>
            <ta e="T816" id="Seg_10831" s="T815">adv</ta>
            <ta e="T817" id="Seg_10832" s="T816">n-n:case</ta>
            <ta e="T818" id="Seg_10833" s="T817">v-v:tense-v:pn</ta>
            <ta e="T819" id="Seg_10834" s="T818">n-n:case.poss</ta>
            <ta e="T820" id="Seg_10835" s="T819">ptcl</ta>
            <ta e="T821" id="Seg_10836" s="T820">v-v:tense-v:pn</ta>
            <ta e="T824" id="Seg_10837" s="T823">n-n:num</ta>
            <ta e="T825" id="Seg_10838" s="T824">v-v&gt;v-v:pn</ta>
            <ta e="T826" id="Seg_10839" s="T825">v-v:ins-v:mood.pn</ta>
            <ta e="T827" id="Seg_10840" s="T826">que</ta>
            <ta e="T828" id="Seg_10841" s="T827">n-n:case</ta>
            <ta e="T829" id="Seg_10842" s="T828">v-v:tense-v:pn</ta>
            <ta e="T830" id="Seg_10843" s="T829">adj</ta>
            <ta e="T831" id="Seg_10844" s="T830">n-n:case</ta>
            <ta e="T832" id="Seg_10845" s="T831">ptcl</ta>
            <ta e="T833" id="Seg_10846" s="T832">ptcl</ta>
            <ta e="T834" id="Seg_10847" s="T833">adj</ta>
            <ta e="T835" id="Seg_10848" s="T834">ptcl</ta>
            <ta e="T836" id="Seg_10849" s="T835">n-n:case.poss</ta>
            <ta e="T837" id="Seg_10850" s="T836">n-n:num</ta>
            <ta e="T838" id="Seg_10851" s="T837">v-v:tense-v:pn</ta>
            <ta e="T839" id="Seg_10852" s="T838">conj</ta>
            <ta e="T840" id="Seg_10853" s="T839">ptcl</ta>
            <ta e="T841" id="Seg_10854" s="T840">n-n:num</ta>
            <ta e="T842" id="Seg_10855" s="T841">n-n:case</ta>
            <ta e="T843" id="Seg_10856" s="T842">ptcl</ta>
            <ta e="T844" id="Seg_10857" s="T843">n-n&gt;adj</ta>
            <ta e="T845" id="Seg_10858" s="T844">n-n:case.poss</ta>
            <ta e="T846" id="Seg_10859" s="T845">ptcl</ta>
            <ta e="T847" id="Seg_10860" s="T846">v-v&gt;v-v:pn</ta>
            <ta e="T848" id="Seg_10861" s="T847">conj</ta>
            <ta e="T849" id="Seg_10862" s="T848">adv</ta>
            <ta e="T850" id="Seg_10863" s="T849">adv-adv&gt;adv</ta>
            <ta e="T851" id="Seg_10864" s="T850">v-v:tense-v:pn</ta>
            <ta e="T852" id="Seg_10865" s="T851">adv</ta>
            <ta e="T853" id="Seg_10866" s="T852">ptcl</ta>
            <ta e="T854" id="Seg_10867" s="T853">adj</ta>
            <ta e="T855" id="Seg_10868" s="T854">n-n:num</ta>
            <ta e="T856" id="Seg_10869" s="T855">v-v:pn</ta>
            <ta e="T857" id="Seg_10870" s="T856">que-n:case=ptcl</ta>
            <ta e="T858" id="Seg_10871" s="T857">v-v:pn</ta>
            <ta e="T859" id="Seg_10872" s="T858">adv</ta>
            <ta e="T860" id="Seg_10873" s="T859">adj-n:case</ta>
            <ta e="T861" id="Seg_10874" s="T860">n-n:case</ta>
            <ta e="T862" id="Seg_10875" s="T861">adj-n:case</ta>
            <ta e="T864" id="Seg_10876" s="T863">n-n:num</ta>
            <ta e="T865" id="Seg_10877" s="T864">n-n:case</ta>
            <ta e="T866" id="Seg_10878" s="T865">adj-n:case</ta>
            <ta e="T868" id="Seg_10879" s="T867">v-v&gt;v-v:pn</ta>
            <ta e="T869" id="Seg_10880" s="T868">n-n:case.poss</ta>
            <ta e="T870" id="Seg_10881" s="T869">n-n:case</ta>
            <ta e="T871" id="Seg_10882" s="T870">v-v&gt;v</ta>
            <ta e="T872" id="Seg_10883" s="T871">adv</ta>
            <ta e="T873" id="Seg_10884" s="T872">adj-n:case</ta>
            <ta e="T874" id="Seg_10885" s="T873">conj</ta>
            <ta e="T875" id="Seg_10886" s="T874">v-v:mood.pn</ta>
            <ta e="T876" id="Seg_10887" s="T875">conj</ta>
            <ta e="T877" id="Seg_10888" s="T876">v-v:ins-v:mood.pn</ta>
            <ta e="T878" id="Seg_10889" s="T877">adj</ta>
            <ta e="T879" id="Seg_10890" s="T878">adv</ta>
            <ta e="T880" id="Seg_10891" s="T879">conj</ta>
            <ta e="T881" id="Seg_10892" s="T880">adj</ta>
            <ta e="T882" id="Seg_10893" s="T881">n-n:case.poss</ta>
            <ta e="T883" id="Seg_10894" s="T882">v-v:tense-v:pn</ta>
            <ta e="T884" id="Seg_10895" s="T883">adv</ta>
            <ta e="T885" id="Seg_10896" s="T884">n</ta>
            <ta e="T886" id="Seg_10897" s="T885">adj-n:case</ta>
            <ta e="T887" id="Seg_10898" s="T886">n-n:case</ta>
            <ta e="T888" id="Seg_10899" s="T887">quant</ta>
            <ta e="T889" id="Seg_10900" s="T888">quant</ta>
            <ta e="T890" id="Seg_10901" s="T889">n-n:case</ta>
            <ta e="T891" id="Seg_10902" s="T890">v-v:tense-v:pn</ta>
            <ta e="T892" id="Seg_10903" s="T891">quant</ta>
            <ta e="T893" id="Seg_10904" s="T892">que-n:case</ta>
            <ta e="T894" id="Seg_10905" s="T893">v-v:tense-v:pn</ta>
            <ta e="T895" id="Seg_10906" s="T894">ptcl</ta>
            <ta e="T896" id="Seg_10907" s="T895">v-v&gt;v-v:pn</ta>
            <ta e="T897" id="Seg_10908" s="T896">ptcl</ta>
            <ta e="T898" id="Seg_10909" s="T897">n-n:case</ta>
            <ta e="T899" id="Seg_10910" s="T898">v-v&gt;v-v:pn</ta>
            <ta e="T900" id="Seg_10911" s="T899">conj</ta>
            <ta e="T902" id="Seg_10912" s="T901">adv</ta>
            <ta e="T903" id="Seg_10913" s="T902">adv</ta>
            <ta e="T905" id="Seg_10914" s="T904">v-v:tense-v:pn</ta>
            <ta e="T906" id="Seg_10915" s="T905">adv</ta>
            <ta e="T907" id="Seg_10916" s="T906">ptcl</ta>
            <ta e="T908" id="Seg_10917" s="T907">n-n:case</ta>
            <ta e="T909" id="Seg_10918" s="T908">ptcl</ta>
            <ta e="T910" id="Seg_10919" s="T909">n-n:num</ta>
            <ta e="T911" id="Seg_10920" s="T910">ptcl</ta>
            <ta e="T912" id="Seg_10921" s="T911">n-n:num</ta>
            <ta e="T913" id="Seg_10922" s="T912">n-n:case</ta>
            <ta e="T914" id="Seg_10923" s="T913">n-n:ins-n:case</ta>
            <ta e="T915" id="Seg_10924" s="T914">ptcl</ta>
            <ta e="T916" id="Seg_10925" s="T915">v-v&gt;v-v:pn</ta>
            <ta e="T917" id="Seg_10926" s="T916">adv</ta>
            <ta e="T918" id="Seg_10927" s="T917">ptcl</ta>
            <ta e="T920" id="Seg_10928" s="T919">pers</ta>
            <ta e="T921" id="Seg_10929" s="T920">v-v:mood.pn</ta>
            <ta e="T922" id="Seg_10930" s="T921">conj</ta>
            <ta e="T923" id="Seg_10931" s="T922">pers</ta>
            <ta e="T924" id="Seg_10932" s="T923">v-v:tense-v:pn</ta>
            <ta e="T925" id="Seg_10933" s="T924">ptcl</ta>
            <ta e="T926" id="Seg_10934" s="T925">pers</ta>
            <ta e="T927" id="Seg_10935" s="T926">n-n:case</ta>
            <ta e="T928" id="Seg_10936" s="T927">v-v:n.fin</ta>
            <ta e="T929" id="Seg_10937" s="T928">v-v:n.fin</ta>
            <ta e="T930" id="Seg_10938" s="T929">n-n:case</ta>
            <ta e="T931" id="Seg_10939" s="T930">ptcl</ta>
            <ta e="T932" id="Seg_10940" s="T931">ptcl</ta>
            <ta e="T933" id="Seg_10941" s="T932">v-v:n.fin</ta>
            <ta e="T934" id="Seg_10942" s="T933">v-v:pn</ta>
            <ta e="T935" id="Seg_10943" s="T934">n-n:case</ta>
            <ta e="T936" id="Seg_10944" s="T935">pers</ta>
            <ta e="T937" id="Seg_10945" s="T936">v-v:tense-v:pn</ta>
            <ta e="T938" id="Seg_10946" s="T937">v-v:tense-v:n.fin</ta>
            <ta e="T940" id="Seg_10947" s="T939">aux-v:mood.pn</ta>
            <ta e="T941" id="Seg_10948" s="T940">v-v:ins-v:mood.pn</ta>
            <ta e="T942" id="Seg_10949" s="T941">n-n:case</ta>
            <ta e="T943" id="Seg_10950" s="T942">v-v:mood.pn</ta>
            <ta e="T944" id="Seg_10951" s="T943">adv-n:case</ta>
            <ta e="T945" id="Seg_10952" s="T944">adj</ta>
            <ta e="T946" id="Seg_10953" s="T945">v-v:n.fin</ta>
            <ta e="T947" id="Seg_10954" s="T946">que=ptcl</ta>
            <ta e="T948" id="Seg_10955" s="T947">v-v:tense-v:pn</ta>
            <ta e="T949" id="Seg_10956" s="T948">que-n:case=ptcl</ta>
            <ta e="T950" id="Seg_10957" s="T949">ptcl</ta>
            <ta e="T951" id="Seg_10958" s="T950">v-v:tense-v:pn</ta>
            <ta e="T953" id="Seg_10959" s="T952">n-n:case</ta>
            <ta e="T954" id="Seg_10960" s="T953">v-v&gt;v-v:pn</ta>
            <ta e="T955" id="Seg_10961" s="T954">dempro-n:case</ta>
            <ta e="T956" id="Seg_10962" s="T955">adj-n:case</ta>
            <ta e="T957" id="Seg_10963" s="T956">propr-n:case</ta>
            <ta e="T958" id="Seg_10964" s="T957">adj-n:case</ta>
            <ta e="T959" id="Seg_10965" s="T958">propr-n:case</ta>
            <ta e="T960" id="Seg_10966" s="T959">conj</ta>
            <ta e="T961" id="Seg_10967" s="T960">que</ta>
            <ta e="T962" id="Seg_10968" s="T961">n-n:case</ta>
            <ta e="T963" id="Seg_10969" s="T962">v-v&gt;v-v:pn</ta>
            <ta e="T964" id="Seg_10970" s="T963">adv</ta>
            <ta e="T965" id="Seg_10971" s="T964">ptcl</ta>
            <ta e="T966" id="Seg_10972" s="T965">v-v:tense-v:pn</ta>
            <ta e="T967" id="Seg_10973" s="T966">ptcl</ta>
            <ta e="T970" id="Seg_10974" s="T969">num-n:case</ta>
            <ta e="T971" id="Seg_10975" s="T970">n-n:case</ta>
            <ta e="T972" id="Seg_10976" s="T971">ptcl</ta>
            <ta e="T973" id="Seg_10977" s="T972">pers</ta>
            <ta e="T974" id="Seg_10978" s="T973">n-n:case</ta>
            <ta e="T975" id="Seg_10979" s="T974">v-v:tense-v:pn</ta>
            <ta e="T976" id="Seg_10980" s="T975">dempro-n:num</ta>
            <ta e="T977" id="Seg_10981" s="T976">ptcl</ta>
            <ta e="T978" id="Seg_10982" s="T977">adv</ta>
            <ta e="T979" id="Seg_10983" s="T978">v-v&gt;v-v:pn</ta>
            <ta e="T980" id="Seg_10984" s="T979">v-v&gt;v-v:pn</ta>
            <ta e="T981" id="Seg_10985" s="T980">v-v&gt;v-v:pn</ta>
            <ta e="T982" id="Seg_10986" s="T981">v-v&gt;v-v:pn</ta>
            <ta e="T983" id="Seg_10987" s="T982">v-v:mood-v:pn</ta>
            <ta e="T984" id="Seg_10988" s="T983">v-v:n.fin</ta>
            <ta e="T985" id="Seg_10989" s="T984">ptcl</ta>
            <ta e="T986" id="Seg_10990" s="T985">adj-n:case</ta>
            <ta e="T987" id="Seg_10991" s="T986">v-v:tense-v:pn</ta>
            <ta e="T990" id="Seg_10992" s="T989">pers</ta>
            <ta e="T991" id="Seg_10993" s="T990">ptcl</ta>
            <ta e="T992" id="Seg_10994" s="T991">v-v:n.fin</ta>
            <ta e="T993" id="Seg_10995" s="T992">pers</ta>
            <ta e="T994" id="Seg_10996" s="T993">pers</ta>
            <ta e="T995" id="Seg_10997" s="T994">adv</ta>
            <ta e="T996" id="Seg_10998" s="T995">quant</ta>
            <ta e="T997" id="Seg_10999" s="T996">adv</ta>
            <ta e="T998" id="Seg_11000" s="T997">adv</ta>
            <ta e="T999" id="Seg_11001" s="T998">n-n:num-n:case.poss</ta>
            <ta e="T1000" id="Seg_11002" s="T999">ptcl</ta>
            <ta e="T1001" id="Seg_11003" s="T1000">v-v&gt;v-v:pn</ta>
            <ta e="T1002" id="Seg_11004" s="T1001">n-n:case</ta>
            <ta e="T1003" id="Seg_11005" s="T1002">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1004" id="Seg_11006" s="T1003">n-n:case</ta>
            <ta e="T1005" id="Seg_11007" s="T1004">adv</ta>
            <ta e="T1006" id="Seg_11008" s="T1005">pers</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_11009" s="T0">pers</ta>
            <ta e="T2" id="Seg_11010" s="T1">adv</ta>
            <ta e="T3" id="Seg_11011" s="T2">pers</ta>
            <ta e="T5" id="Seg_11012" s="T4">v</ta>
            <ta e="T6" id="Seg_11013" s="T5">v</ta>
            <ta e="T7" id="Seg_11014" s="T6">v</ta>
            <ta e="T8" id="Seg_11015" s="T7">conj</ta>
            <ta e="T9" id="Seg_11016" s="T8">que</ta>
            <ta e="T10" id="Seg_11017" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_11018" s="T10">v</ta>
            <ta e="T12" id="Seg_11019" s="T11">ptcl</ta>
            <ta e="T14" id="Seg_11020" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_11021" s="T14">v</ta>
            <ta e="T17" id="Seg_11022" s="T16">adv</ta>
            <ta e="T18" id="Seg_11023" s="T17">v</ta>
            <ta e="T19" id="Seg_11024" s="T18">adj</ta>
            <ta e="T21" id="Seg_11025" s="T20">v</ta>
            <ta e="T22" id="Seg_11026" s="T21">pers</ta>
            <ta e="T23" id="Seg_11027" s="T22">v</ta>
            <ta e="T24" id="Seg_11028" s="T23">conj</ta>
            <ta e="T25" id="Seg_11029" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_11030" s="T25">ptcl</ta>
            <ta e="T28" id="Seg_11031" s="T27">adv</ta>
            <ta e="T29" id="Seg_11032" s="T28">v</ta>
            <ta e="T30" id="Seg_11033" s="T29">v</ta>
            <ta e="T32" id="Seg_11034" s="T31">que</ta>
            <ta e="T33" id="Seg_11035" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_11036" s="T33">v</ta>
            <ta e="T35" id="Seg_11037" s="T34">que</ta>
            <ta e="T36" id="Seg_11038" s="T35">v</ta>
            <ta e="T37" id="Seg_11039" s="T36">adj</ta>
            <ta e="T38" id="Seg_11040" s="T37">pers</ta>
            <ta e="T39" id="Seg_11041" s="T38">v</ta>
            <ta e="T40" id="Seg_11042" s="T39">n</ta>
            <ta e="T41" id="Seg_11043" s="T40">v</ta>
            <ta e="T42" id="Seg_11044" s="T41">aux</ta>
            <ta e="T43" id="Seg_11045" s="T42">v</ta>
            <ta e="T44" id="Seg_11046" s="T43">aux</ta>
            <ta e="T45" id="Seg_11047" s="T44">v</ta>
            <ta e="T46" id="Seg_11048" s="T45">conj</ta>
            <ta e="T47" id="Seg_11049" s="T46">que</ta>
            <ta e="T48" id="Seg_11050" s="T47">pers</ta>
            <ta e="T49" id="Seg_11051" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_11052" s="T49">v</ta>
            <ta e="T51" id="Seg_11053" s="T50">conj</ta>
            <ta e="T52" id="Seg_11054" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_11055" s="T52">v</ta>
            <ta e="T54" id="Seg_11056" s="T53">conj</ta>
            <ta e="T56" id="Seg_11057" s="T55">adv</ta>
            <ta e="T57" id="Seg_11058" s="T56">v</ta>
            <ta e="T58" id="Seg_11059" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_11060" s="T58">v</ta>
            <ta e="T60" id="Seg_11061" s="T59">adj</ta>
            <ta e="T61" id="Seg_11062" s="T60">v</ta>
            <ta e="T62" id="Seg_11063" s="T61">pers</ta>
            <ta e="T63" id="Seg_11064" s="T62">v</ta>
            <ta e="T64" id="Seg_11065" s="T63">v</ta>
            <ta e="T65" id="Seg_11066" s="T64">adv</ta>
            <ta e="T66" id="Seg_11067" s="T65">que</ta>
            <ta e="T67" id="Seg_11068" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_11069" s="T67">v</ta>
            <ta e="T69" id="Seg_11070" s="T68">dempro</ta>
            <ta e="T70" id="Seg_11071" s="T69">conj</ta>
            <ta e="T71" id="Seg_11072" s="T70">v</ta>
            <ta e="T72" id="Seg_11073" s="T71">dempro</ta>
            <ta e="T73" id="Seg_11074" s="T72">adv</ta>
            <ta e="T74" id="Seg_11075" s="T73">adj</ta>
            <ta e="T75" id="Seg_11076" s="T74">dempro</ta>
            <ta e="T76" id="Seg_11077" s="T75">adv</ta>
            <ta e="T77" id="Seg_11078" s="T76">adv</ta>
            <ta e="T78" id="Seg_11079" s="T77">v</ta>
            <ta e="T79" id="Seg_11080" s="T78">v</ta>
            <ta e="T80" id="Seg_11081" s="T79">conj</ta>
            <ta e="T81" id="Seg_11082" s="T80">adv</ta>
            <ta e="T82" id="Seg_11083" s="T81">adj</ta>
            <ta e="T83" id="Seg_11084" s="T82">v</ta>
            <ta e="T84" id="Seg_11085" s="T83">n</ta>
            <ta e="T85" id="Seg_11086" s="T84">v</ta>
            <ta e="T86" id="Seg_11087" s="T85">adv</ta>
            <ta e="T87" id="Seg_11088" s="T86">v</ta>
            <ta e="T88" id="Seg_11089" s="T87">n</ta>
            <ta e="T89" id="Seg_11090" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_11091" s="T89">v</ta>
            <ta e="T91" id="Seg_11092" s="T90">conj</ta>
            <ta e="T92" id="Seg_11093" s="T91">adv</ta>
            <ta e="T93" id="Seg_11094" s="T92">ptcl</ta>
            <ta e="T97" id="Seg_11095" s="T96">adv</ta>
            <ta e="T98" id="Seg_11096" s="T97">ptcl</ta>
            <ta e="T101" id="Seg_11097" s="T100">n</ta>
            <ta e="T102" id="Seg_11098" s="T101">v</ta>
            <ta e="T103" id="Seg_11099" s="T102">conj</ta>
            <ta e="T104" id="Seg_11100" s="T103">n</ta>
            <ta e="T105" id="Seg_11101" s="T104">v</ta>
            <ta e="T107" id="Seg_11102" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_11103" s="T107">v</ta>
            <ta e="T109" id="Seg_11104" s="T108">conj</ta>
            <ta e="T110" id="Seg_11105" s="T109">ptcl</ta>
            <ta e="T112" id="Seg_11106" s="T111">v</ta>
            <ta e="T113" id="Seg_11107" s="T112">n</ta>
            <ta e="T114" id="Seg_11108" s="T113">adv</ta>
            <ta e="T115" id="Seg_11109" s="T114">v</ta>
            <ta e="T116" id="Seg_11110" s="T115">v</ta>
            <ta e="T117" id="Seg_11111" s="T116">pers</ta>
            <ta e="T118" id="Seg_11112" s="T117">n</ta>
            <ta e="T119" id="Seg_11113" s="T118">n</ta>
            <ta e="T120" id="Seg_11114" s="T119">v</ta>
            <ta e="T121" id="Seg_11115" s="T120">n</ta>
            <ta e="T122" id="Seg_11116" s="T121">v</ta>
            <ta e="T123" id="Seg_11117" s="T122">n</ta>
            <ta e="T124" id="Seg_11118" s="T123">ptcl</ta>
            <ta e="T125" id="Seg_11119" s="T124">v</ta>
            <ta e="T126" id="Seg_11120" s="T125">v</ta>
            <ta e="T127" id="Seg_11121" s="T126">n</ta>
            <ta e="T128" id="Seg_11122" s="T127">v</ta>
            <ta e="T129" id="Seg_11123" s="T128">dempro</ta>
            <ta e="T130" id="Seg_11124" s="T129">v</ta>
            <ta e="T132" id="Seg_11125" s="T131">n</ta>
            <ta e="T133" id="Seg_11126" s="T132">dempro</ta>
            <ta e="T134" id="Seg_11127" s="T133">ptcl</ta>
            <ta e="T136" id="Seg_11128" s="T135">v</ta>
            <ta e="T137" id="Seg_11129" s="T136">v</ta>
            <ta e="T138" id="Seg_11130" s="T137">conj</ta>
            <ta e="T139" id="Seg_11131" s="T138">v</ta>
            <ta e="T140" id="Seg_11132" s="T139">adv</ta>
            <ta e="T144" id="Seg_11133" s="T143">adv</ta>
            <ta e="T145" id="Seg_11134" s="T144">v</ta>
            <ta e="T146" id="Seg_11135" s="T145">n</ta>
            <ta e="T147" id="Seg_11136" s="T146">v</ta>
            <ta e="T148" id="Seg_11137" s="T147">dempro</ta>
            <ta e="T149" id="Seg_11138" s="T148">conj</ta>
            <ta e="T150" id="Seg_11139" s="T149">v</ta>
            <ta e="T151" id="Seg_11140" s="T150">n</ta>
            <ta e="T152" id="Seg_11141" s="T151">dempro</ta>
            <ta e="T153" id="Seg_11142" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_11143" s="T153">v</ta>
            <ta e="T155" id="Seg_11144" s="T154">conj</ta>
            <ta e="T156" id="Seg_11145" s="T155">dempro</ta>
            <ta e="T158" id="Seg_11146" s="T157">v</ta>
            <ta e="T159" id="Seg_11147" s="T158">adv</ta>
            <ta e="T160" id="Seg_11148" s="T159">v</ta>
            <ta e="T161" id="Seg_11149" s="T160">n</ta>
            <ta e="T162" id="Seg_11150" s="T161">conj</ta>
            <ta e="T163" id="Seg_11151" s="T162">v</ta>
            <ta e="T165" id="Seg_11152" s="T164">conj</ta>
            <ta e="T166" id="Seg_11153" s="T165">pers</ta>
            <ta e="T167" id="Seg_11154" s="T166">n</ta>
            <ta e="T168" id="Seg_11155" s="T167">dempro</ta>
            <ta e="T169" id="Seg_11156" s="T168">v</ta>
            <ta e="T170" id="Seg_11157" s="T169">pers</ta>
            <ta e="T171" id="Seg_11158" s="T170">num</ta>
            <ta e="T172" id="Seg_11159" s="T171">num</ta>
            <ta e="T173" id="Seg_11160" s="T172">n</ta>
            <ta e="T174" id="Seg_11161" s="T173">v</ta>
            <ta e="T175" id="Seg_11162" s="T174">adv</ta>
            <ta e="T176" id="Seg_11163" s="T175">dempro</ta>
            <ta e="T178" id="Seg_11164" s="T177">v</ta>
            <ta e="T179" id="Seg_11165" s="T178">v</ta>
            <ta e="T180" id="Seg_11166" s="T179">num</ta>
            <ta e="T181" id="Seg_11167" s="T180">num</ta>
            <ta e="T182" id="Seg_11168" s="T181">num</ta>
            <ta e="T183" id="Seg_11169" s="T182">num</ta>
            <ta e="T184" id="Seg_11170" s="T183">n</ta>
            <ta e="T185" id="Seg_11171" s="T184">v</ta>
            <ta e="T186" id="Seg_11172" s="T185">n</ta>
            <ta e="T187" id="Seg_11173" s="T186">v</ta>
            <ta e="T188" id="Seg_11174" s="T187">dempro</ta>
            <ta e="T189" id="Seg_11175" s="T188">v</ta>
            <ta e="T190" id="Seg_11176" s="T189">conj</ta>
            <ta e="T193" id="Seg_11177" s="T192">n</ta>
            <ta e="T194" id="Seg_11178" s="T193">v</ta>
            <ta e="T195" id="Seg_11179" s="T194">dempro</ta>
            <ta e="T196" id="Seg_11180" s="T195">v</ta>
            <ta e="T198" id="Seg_11181" s="T197">dempro</ta>
            <ta e="T199" id="Seg_11182" s="T198">conj</ta>
            <ta e="T200" id="Seg_11183" s="T199">dempro</ta>
            <ta e="T201" id="Seg_11184" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_11185" s="T201">v</ta>
            <ta e="T203" id="Seg_11186" s="T202">n</ta>
            <ta e="T204" id="Seg_11187" s="T203">n</ta>
            <ta e="T205" id="Seg_11188" s="T204">v</ta>
            <ta e="T206" id="Seg_11189" s="T205">pers</ta>
            <ta e="T207" id="Seg_11190" s="T206">n</ta>
            <ta e="T208" id="Seg_11191" s="T207">n</ta>
            <ta e="T209" id="Seg_11192" s="T208">v</ta>
            <ta e="T210" id="Seg_11193" s="T209">adv</ta>
            <ta e="T211" id="Seg_11194" s="T210">n</ta>
            <ta e="T212" id="Seg_11195" s="T211">conj</ta>
            <ta e="T213" id="Seg_11196" s="T212">pers</ta>
            <ta e="T214" id="Seg_11197" s="T213">n</ta>
            <ta e="T215" id="Seg_11198" s="T214">num</ta>
            <ta e="T216" id="Seg_11199" s="T215">n</ta>
            <ta e="T217" id="Seg_11200" s="T216">v</ta>
            <ta e="T218" id="Seg_11201" s="T217">n</ta>
            <ta e="T219" id="Seg_11202" s="T218">pers</ta>
            <ta e="T220" id="Seg_11203" s="T219">adv</ta>
            <ta e="T221" id="Seg_11204" s="T220">adv</ta>
            <ta e="T222" id="Seg_11205" s="T221">v</ta>
            <ta e="T223" id="Seg_11206" s="T222">adj</ta>
            <ta e="T224" id="Seg_11207" s="T223">n</ta>
            <ta e="T225" id="Seg_11208" s="T224">v</ta>
            <ta e="T227" id="Seg_11209" s="T226">adv</ta>
            <ta e="T228" id="Seg_11210" s="T227">n</ta>
            <ta e="T229" id="Seg_11211" s="T228">n</ta>
            <ta e="T230" id="Seg_11212" s="T229">v</ta>
            <ta e="T231" id="Seg_11213" s="T230">n</ta>
            <ta e="T232" id="Seg_11214" s="T231">v</ta>
            <ta e="T233" id="Seg_11215" s="T232">n</ta>
            <ta e="T234" id="Seg_11216" s="T233">v</ta>
            <ta e="T235" id="Seg_11217" s="T234">conj</ta>
            <ta e="T236" id="Seg_11218" s="T235">n</ta>
            <ta e="T237" id="Seg_11219" s="T236">v</ta>
            <ta e="T238" id="Seg_11220" s="T237">v</ta>
            <ta e="T239" id="Seg_11221" s="T238">adv</ta>
            <ta e="T240" id="Seg_11222" s="T239">n</ta>
            <ta e="T241" id="Seg_11223" s="T240">v</ta>
            <ta e="T242" id="Seg_11224" s="T241">n</ta>
            <ta e="T243" id="Seg_11225" s="T242">v</ta>
            <ta e="T244" id="Seg_11226" s="T243">n</ta>
            <ta e="T245" id="Seg_11227" s="T244">v</ta>
            <ta e="T246" id="Seg_11228" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_11229" s="T246">num</ta>
            <ta e="T248" id="Seg_11230" s="T247">n</ta>
            <ta e="T249" id="Seg_11231" s="T248">v</ta>
            <ta e="T250" id="Seg_11232" s="T249">n</ta>
            <ta e="T251" id="Seg_11233" s="T250">conj</ta>
            <ta e="T252" id="Seg_11234" s="T251">n</ta>
            <ta e="T253" id="Seg_11235" s="T252">n</ta>
            <ta e="T254" id="Seg_11236" s="T253">v</ta>
            <ta e="T255" id="Seg_11237" s="T254">ptcl</ta>
            <ta e="T256" id="Seg_11238" s="T255">v</ta>
            <ta e="T257" id="Seg_11239" s="T256">adv</ta>
            <ta e="T258" id="Seg_11240" s="T257">n</ta>
            <ta e="T259" id="Seg_11241" s="T258">v</ta>
            <ta e="T260" id="Seg_11242" s="T259">dempro</ta>
            <ta e="T261" id="Seg_11243" s="T260">n</ta>
            <ta e="T262" id="Seg_11244" s="T261">num</ta>
            <ta e="T263" id="Seg_11245" s="T262">v</ta>
            <ta e="T264" id="Seg_11246" s="T263">n</ta>
            <ta e="T265" id="Seg_11247" s="T264">conj</ta>
            <ta e="T266" id="Seg_11248" s="T265">n</ta>
            <ta e="T267" id="Seg_11249" s="T266">v</ta>
            <ta e="T268" id="Seg_11250" s="T267">v</ta>
            <ta e="T269" id="Seg_11251" s="T268">conj</ta>
            <ta e="T270" id="Seg_11252" s="T269">ptcl</ta>
            <ta e="T271" id="Seg_11253" s="T270">v</ta>
            <ta e="T272" id="Seg_11254" s="T271">adv</ta>
            <ta e="T273" id="Seg_11255" s="T272">v</ta>
            <ta e="T274" id="Seg_11256" s="T273">pers</ta>
            <ta e="T275" id="Seg_11257" s="T274">que</ta>
            <ta e="T276" id="Seg_11258" s="T275">ptcl</ta>
            <ta e="T277" id="Seg_11259" s="T276">v</ta>
            <ta e="T278" id="Seg_11260" s="T277">conj</ta>
            <ta e="T279" id="Seg_11261" s="T278">dempro</ta>
            <ta e="T280" id="Seg_11262" s="T279">v</ta>
            <ta e="T281" id="Seg_11263" s="T280">pers</ta>
            <ta e="T282" id="Seg_11264" s="T281">ptcl</ta>
            <ta e="T283" id="Seg_11265" s="T282">v</ta>
            <ta e="T284" id="Seg_11266" s="T283">adv</ta>
            <ta e="T285" id="Seg_11267" s="T284">pers</ta>
            <ta e="T286" id="Seg_11268" s="T285">v</ta>
            <ta e="T288" id="Seg_11269" s="T287">dempro</ta>
            <ta e="T289" id="Seg_11270" s="T288">v</ta>
            <ta e="T293" id="Seg_11271" s="T292">conj</ta>
            <ta e="T294" id="Seg_11272" s="T293">n</ta>
            <ta e="T295" id="Seg_11273" s="T294">v</ta>
            <ta e="T299" id="Seg_11274" s="T298">dempro</ta>
            <ta e="T300" id="Seg_11275" s="T299">n</ta>
            <ta e="T301" id="Seg_11276" s="T300">adj</ta>
            <ta e="T302" id="Seg_11277" s="T301">v</ta>
            <ta e="T303" id="Seg_11278" s="T302">conj</ta>
            <ta e="T304" id="Seg_11279" s="T303">adv</ta>
            <ta e="T305" id="Seg_11280" s="T304">n</ta>
            <ta e="T306" id="Seg_11281" s="T305">v</ta>
            <ta e="T308" id="Seg_11282" s="T307">dempro</ta>
            <ta e="T309" id="Seg_11283" s="T308">ptcl</ta>
            <ta e="T310" id="Seg_11284" s="T309">n</ta>
            <ta e="T311" id="Seg_11285" s="T310">v</ta>
            <ta e="T312" id="Seg_11286" s="T311">v</ta>
            <ta e="T313" id="Seg_11287" s="T312">propr</ta>
            <ta e="T314" id="Seg_11288" s="T313">n</ta>
            <ta e="T315" id="Seg_11289" s="T314">v</ta>
            <ta e="T316" id="Seg_11290" s="T315">ptcl</ta>
            <ta e="T317" id="Seg_11291" s="T316">ptcl</ta>
            <ta e="T318" id="Seg_11292" s="T317">quant</ta>
            <ta e="T319" id="Seg_11293" s="T318">v</ta>
            <ta e="T321" id="Seg_11294" s="T320">quant</ta>
            <ta e="T322" id="Seg_11295" s="T321">num</ta>
            <ta e="T323" id="Seg_11296" s="T322">v</ta>
            <ta e="T327" id="Seg_11297" s="T326">n</ta>
            <ta e="T328" id="Seg_11298" s="T327">que</ta>
            <ta e="T329" id="Seg_11299" s="T328">pers</ta>
            <ta e="T330" id="Seg_11300" s="T329">v</ta>
            <ta e="T331" id="Seg_11301" s="T330">conj</ta>
            <ta e="T332" id="Seg_11302" s="T331">pers</ta>
            <ta e="T333" id="Seg_11303" s="T332">pers</ta>
            <ta e="T334" id="Seg_11304" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_11305" s="T334">v</ta>
            <ta e="T336" id="Seg_11306" s="T335">ptcl</ta>
            <ta e="T337" id="Seg_11307" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_11308" s="T337">v</ta>
            <ta e="T340" id="Seg_11309" s="T339">n</ta>
            <ta e="T341" id="Seg_11310" s="T340">v</ta>
            <ta e="T342" id="Seg_11311" s="T341">adj</ta>
            <ta e="T343" id="Seg_11312" s="T342">n</ta>
            <ta e="T345" id="Seg_11313" s="T344">n</ta>
            <ta e="T346" id="Seg_11314" s="T345">num</ta>
            <ta e="T347" id="Seg_11315" s="T346">n</ta>
            <ta e="T348" id="Seg_11316" s="T347">v</ta>
            <ta e="T349" id="Seg_11317" s="T348">ptcl</ta>
            <ta e="T350" id="Seg_11318" s="T349">adv</ta>
            <ta e="T351" id="Seg_11319" s="T350">n</ta>
            <ta e="T352" id="Seg_11320" s="T351">v</ta>
            <ta e="T353" id="Seg_11321" s="T352">v</ta>
            <ta e="T354" id="Seg_11322" s="T353">conj</ta>
            <ta e="T355" id="Seg_11323" s="T354">v</ta>
            <ta e="T356" id="Seg_11324" s="T355">n</ta>
            <ta e="T357" id="Seg_11325" s="T356">conj</ta>
            <ta e="T358" id="Seg_11326" s="T357">v</ta>
            <ta e="T359" id="Seg_11327" s="T358">n</ta>
            <ta e="T360" id="Seg_11328" s="T359">n</ta>
            <ta e="T361" id="Seg_11329" s="T360">que</ta>
            <ta e="T362" id="Seg_11330" s="T361">v</ta>
            <ta e="T363" id="Seg_11331" s="T362">n</ta>
            <ta e="T364" id="Seg_11332" s="T363">adv</ta>
            <ta e="T365" id="Seg_11333" s="T364">adj</ta>
            <ta e="T366" id="Seg_11334" s="T365">n</ta>
            <ta e="T367" id="Seg_11335" s="T366">v</ta>
            <ta e="T368" id="Seg_11336" s="T367">adv</ta>
            <ta e="T369" id="Seg_11337" s="T368">ptcl</ta>
            <ta e="T370" id="Seg_11338" s="T369">ptcl</ta>
            <ta e="T371" id="Seg_11339" s="T370">v</ta>
            <ta e="T372" id="Seg_11340" s="T371">adv</ta>
            <ta e="T373" id="Seg_11341" s="T372">adv</ta>
            <ta e="T374" id="Seg_11342" s="T373">num</ta>
            <ta e="T375" id="Seg_11343" s="T374">n</ta>
            <ta e="T376" id="Seg_11344" s="T375">adv</ta>
            <ta e="T377" id="Seg_11345" s="T376">num</ta>
            <ta e="T378" id="Seg_11346" s="T377">n</ta>
            <ta e="T379" id="Seg_11347" s="T378">num</ta>
            <ta e="T382" id="Seg_11348" s="T381">num</ta>
            <ta e="T383" id="Seg_11349" s="T382">n</ta>
            <ta e="T384" id="Seg_11350" s="T383">v</ta>
            <ta e="T385" id="Seg_11351" s="T384">adv</ta>
            <ta e="T386" id="Seg_11352" s="T385">n</ta>
            <ta e="T387" id="Seg_11353" s="T386">conj</ta>
            <ta e="T388" id="Seg_11354" s="T387">adv</ta>
            <ta e="T389" id="Seg_11355" s="T388">ptcl</ta>
            <ta e="T1011" id="Seg_11356" s="T391">propr</ta>
            <ta e="T392" id="Seg_11357" s="T1011">n</ta>
            <ta e="T394" id="Seg_11358" s="T393">v</ta>
            <ta e="T395" id="Seg_11359" s="T394">dempro</ta>
            <ta e="T396" id="Seg_11360" s="T395">adv</ta>
            <ta e="T397" id="Seg_11361" s="T396">v</ta>
            <ta e="T398" id="Seg_11362" s="T397">conj</ta>
            <ta e="T399" id="Seg_11363" s="T398">adv</ta>
            <ta e="T400" id="Seg_11364" s="T399">n</ta>
            <ta e="T401" id="Seg_11365" s="T400">v</ta>
            <ta e="T402" id="Seg_11366" s="T401">que</ta>
            <ta e="T403" id="Seg_11367" s="T402">n</ta>
            <ta e="T404" id="Seg_11368" s="T403">quant</ta>
            <ta e="T405" id="Seg_11369" s="T404">ptcl</ta>
            <ta e="T406" id="Seg_11370" s="T405">quant</ta>
            <ta e="T407" id="Seg_11371" s="T406">v</ta>
            <ta e="T408" id="Seg_11372" s="T407">num</ta>
            <ta e="T410" id="Seg_11373" s="T409">conj</ta>
            <ta e="T411" id="Seg_11374" s="T410">que</ta>
            <ta e="T412" id="Seg_11375" s="T411">adv</ta>
            <ta e="T413" id="Seg_11376" s="T412">num</ta>
            <ta e="T414" id="Seg_11377" s="T413">v</ta>
            <ta e="T415" id="Seg_11378" s="T414">que</ta>
            <ta e="T416" id="Seg_11379" s="T415">adv</ta>
            <ta e="T417" id="Seg_11380" s="T416">adv</ta>
            <ta e="T418" id="Seg_11381" s="T417">num</ta>
            <ta e="T419" id="Seg_11382" s="T418">v</ta>
            <ta e="T420" id="Seg_11383" s="T419">conj</ta>
            <ta e="T421" id="Seg_11384" s="T420">adv</ta>
            <ta e="T422" id="Seg_11385" s="T421">adj</ta>
            <ta e="T423" id="Seg_11386" s="T422">n</ta>
            <ta e="T424" id="Seg_11387" s="T423">v</ta>
            <ta e="T425" id="Seg_11388" s="T424">adj</ta>
            <ta e="T426" id="Seg_11389" s="T425">adj</ta>
            <ta e="T427" id="Seg_11390" s="T426">n</ta>
            <ta e="T428" id="Seg_11391" s="T427">v</ta>
            <ta e="T429" id="Seg_11392" s="T428">num</ta>
            <ta e="T430" id="Seg_11393" s="T429">v</ta>
            <ta e="T431" id="Seg_11394" s="T430">adv</ta>
            <ta e="T432" id="Seg_11395" s="T431">adv</ta>
            <ta e="T433" id="Seg_11396" s="T432">ptcl</ta>
            <ta e="T434" id="Seg_11397" s="T433">ptcl</ta>
            <ta e="T435" id="Seg_11398" s="T434">ptcl</ta>
            <ta e="T436" id="Seg_11399" s="T435">v</ta>
            <ta e="T437" id="Seg_11400" s="T436">v</ta>
            <ta e="T438" id="Seg_11401" s="T437">ptcl</ta>
            <ta e="T440" id="Seg_11402" s="T439">adv</ta>
            <ta e="T441" id="Seg_11403" s="T440">v</ta>
            <ta e="T442" id="Seg_11404" s="T441">v</ta>
            <ta e="T443" id="Seg_11405" s="T442">ptcl</ta>
            <ta e="T444" id="Seg_11406" s="T443">v</ta>
            <ta e="T445" id="Seg_11407" s="T444">n</ta>
            <ta e="T446" id="Seg_11408" s="T445">v</ta>
            <ta e="T451" id="Seg_11409" s="T450">v</ta>
            <ta e="T453" id="Seg_11410" s="T451">propr</ta>
            <ta e="T455" id="Seg_11411" s="T454">v</ta>
            <ta e="T456" id="Seg_11412" s="T455">adj</ta>
            <ta e="T457" id="Seg_11413" s="T456">n</ta>
            <ta e="T458" id="Seg_11414" s="T457">adv</ta>
            <ta e="T459" id="Seg_11415" s="T458">n</ta>
            <ta e="T460" id="Seg_11416" s="T459">v</ta>
            <ta e="T461" id="Seg_11417" s="T460">ptcl</ta>
            <ta e="T462" id="Seg_11418" s="T461">adj</ta>
            <ta e="T463" id="Seg_11419" s="T462">n</ta>
            <ta e="T464" id="Seg_11420" s="T463">v</ta>
            <ta e="T465" id="Seg_11421" s="T464">dempro</ta>
            <ta e="T466" id="Seg_11422" s="T465">n</ta>
            <ta e="T1010" id="Seg_11423" s="T466">v</ta>
            <ta e="T467" id="Seg_11424" s="T1010">v</ta>
            <ta e="T468" id="Seg_11425" s="T467">propr</ta>
            <ta e="T469" id="Seg_11426" s="T468">conj</ta>
            <ta e="T470" id="Seg_11427" s="T469">adv</ta>
            <ta e="T471" id="Seg_11428" s="T470">v</ta>
            <ta e="T472" id="Seg_11429" s="T471">v</ta>
            <ta e="T474" id="Seg_11430" s="T473">dempro</ta>
            <ta e="T475" id="Seg_11431" s="T474">n</ta>
            <ta e="T477" id="Seg_11432" s="T476">n</ta>
            <ta e="T478" id="Seg_11433" s="T477">v</ta>
            <ta e="T479" id="Seg_11434" s="T478">adv</ta>
            <ta e="T480" id="Seg_11435" s="T479">num</ta>
            <ta e="T481" id="Seg_11436" s="T480">n</ta>
            <ta e="T482" id="Seg_11437" s="T481">dempro</ta>
            <ta e="T483" id="Seg_11438" s="T482">v</ta>
            <ta e="T484" id="Seg_11439" s="T483">v</ta>
            <ta e="T486" id="Seg_11440" s="T485">pers</ta>
            <ta e="T487" id="Seg_11441" s="T486">adv</ta>
            <ta e="T488" id="Seg_11442" s="T487">v</ta>
            <ta e="T489" id="Seg_11443" s="T488">v</ta>
            <ta e="T490" id="Seg_11444" s="T489">v</ta>
            <ta e="T491" id="Seg_11445" s="T490">ptcl</ta>
            <ta e="T493" id="Seg_11446" s="T492">ptcl</ta>
            <ta e="T495" id="Seg_11447" s="T494">adv</ta>
            <ta e="T496" id="Seg_11448" s="T495">n</ta>
            <ta e="T498" id="Seg_11449" s="T497">v</ta>
            <ta e="T499" id="Seg_11450" s="T498">adv</ta>
            <ta e="T500" id="Seg_11451" s="T499">n</ta>
            <ta e="T501" id="Seg_11452" s="T500">v</ta>
            <ta e="T502" id="Seg_11453" s="T501">conj</ta>
            <ta e="T503" id="Seg_11454" s="T502">dempro</ta>
            <ta e="T504" id="Seg_11455" s="T503">n</ta>
            <ta e="T505" id="Seg_11456" s="T504">v</ta>
            <ta e="T506" id="Seg_11457" s="T505">refl</ta>
            <ta e="T507" id="Seg_11458" s="T506">n</ta>
            <ta e="T508" id="Seg_11459" s="T507">v</ta>
            <ta e="T510" id="Seg_11460" s="T509">dempro</ta>
            <ta e="T511" id="Seg_11461" s="T510">adv</ta>
            <ta e="T512" id="Seg_11462" s="T511">n</ta>
            <ta e="T513" id="Seg_11463" s="T512">quant</ta>
            <ta e="T514" id="Seg_11464" s="T513">v</ta>
            <ta e="T515" id="Seg_11465" s="T514">adv</ta>
            <ta e="T516" id="Seg_11466" s="T515">conj</ta>
            <ta e="T518" id="Seg_11467" s="T517">adv</ta>
            <ta e="T519" id="Seg_11468" s="T518">v</ta>
            <ta e="T520" id="Seg_11469" s="T519">v</ta>
            <ta e="T521" id="Seg_11470" s="T520">adv</ta>
            <ta e="T522" id="Seg_11471" s="T521">v</ta>
            <ta e="T523" id="Seg_11472" s="T522">que</ta>
            <ta e="T524" id="Seg_11473" s="T523">v</ta>
            <ta e="T525" id="Seg_11474" s="T524">ptcl</ta>
            <ta e="T526" id="Seg_11475" s="T525">adv</ta>
            <ta e="T527" id="Seg_11476" s="T526">v</ta>
            <ta e="T528" id="Seg_11477" s="T527">ptcl</ta>
            <ta e="T530" id="Seg_11478" s="T529">adv</ta>
            <ta e="T531" id="Seg_11479" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_11480" s="T531">v</ta>
            <ta e="T533" id="Seg_11481" s="T532">v</ta>
            <ta e="T534" id="Seg_11482" s="T533">adv</ta>
            <ta e="T535" id="Seg_11483" s="T534">ptcl</ta>
            <ta e="T536" id="Seg_11484" s="T535">v</ta>
            <ta e="T538" id="Seg_11485" s="T537">que</ta>
            <ta e="T539" id="Seg_11486" s="T538">ptcl</ta>
            <ta e="T540" id="Seg_11487" s="T539">quant</ta>
            <ta e="T542" id="Seg_11488" s="T541">v</ta>
            <ta e="T543" id="Seg_11489" s="T542">v</ta>
            <ta e="T544" id="Seg_11490" s="T543">adv</ta>
            <ta e="T545" id="Seg_11491" s="T544">n</ta>
            <ta e="T546" id="Seg_11492" s="T545">ptcl</ta>
            <ta e="T547" id="Seg_11493" s="T546">v</ta>
            <ta e="T548" id="Seg_11494" s="T547">dempro</ta>
            <ta e="T549" id="Seg_11495" s="T548">dempro</ta>
            <ta e="T550" id="Seg_11496" s="T549">n</ta>
            <ta e="T551" id="Seg_11497" s="T550">ptcl</ta>
            <ta e="T552" id="Seg_11498" s="T551">v</ta>
            <ta e="T553" id="Seg_11499" s="T552">adv</ta>
            <ta e="T554" id="Seg_11500" s="T553">dempro</ta>
            <ta e="T556" id="Seg_11501" s="T555">v</ta>
            <ta e="T558" id="Seg_11502" s="T557">num</ta>
            <ta e="T559" id="Seg_11503" s="T558">num</ta>
            <ta e="T560" id="Seg_11504" s="T559">pers</ta>
            <ta e="T561" id="Seg_11505" s="T560">v</ta>
            <ta e="T562" id="Seg_11506" s="T561">v</ta>
            <ta e="T563" id="Seg_11507" s="T562">ptcl</ta>
            <ta e="T564" id="Seg_11508" s="T563">adv</ta>
            <ta e="T565" id="Seg_11509" s="T564">v</ta>
            <ta e="T566" id="Seg_11510" s="T565">pers</ta>
            <ta e="T567" id="Seg_11511" s="T566">adv</ta>
            <ta e="T568" id="Seg_11512" s="T567">v</ta>
            <ta e="T569" id="Seg_11513" s="T568">n</ta>
            <ta e="T570" id="Seg_11514" s="T569">v</ta>
            <ta e="T571" id="Seg_11515" s="T570">dempro</ta>
            <ta e="T573" id="Seg_11516" s="T572">adv</ta>
            <ta e="T574" id="Seg_11517" s="T573">v</ta>
            <ta e="T575" id="Seg_11518" s="T574">pers</ta>
            <ta e="T576" id="Seg_11519" s="T575">n</ta>
            <ta e="T577" id="Seg_11520" s="T576">v</ta>
            <ta e="T580" id="Seg_11521" s="T579">adv</ta>
            <ta e="T581" id="Seg_11522" s="T580">v</ta>
            <ta e="T582" id="Seg_11523" s="T581">v</ta>
            <ta e="T583" id="Seg_11524" s="T582">v</ta>
            <ta e="T584" id="Seg_11525" s="T583">v</ta>
            <ta e="T586" id="Seg_11526" s="T585">v</ta>
            <ta e="T587" id="Seg_11527" s="T586">ptcl</ta>
            <ta e="T588" id="Seg_11528" s="T587">v</ta>
            <ta e="T589" id="Seg_11529" s="T588">conj</ta>
            <ta e="T590" id="Seg_11530" s="T589">adv</ta>
            <ta e="T591" id="Seg_11531" s="T590">n</ta>
            <ta e="T592" id="Seg_11532" s="T591">v</ta>
            <ta e="T593" id="Seg_11533" s="T592">pers</ta>
            <ta e="T594" id="Seg_11534" s="T593">ptcl</ta>
            <ta e="T595" id="Seg_11535" s="T594">v</ta>
            <ta e="T596" id="Seg_11536" s="T595">n</ta>
            <ta e="T597" id="Seg_11537" s="T596">n</ta>
            <ta e="T598" id="Seg_11538" s="T597">v</ta>
            <ta e="T599" id="Seg_11539" s="T598">v</ta>
            <ta e="T600" id="Seg_11540" s="T599">dempro</ta>
            <ta e="T602" id="Seg_11541" s="T601">v</ta>
            <ta e="T603" id="Seg_11542" s="T602">dempro</ta>
            <ta e="T604" id="Seg_11543" s="T603">n</ta>
            <ta e="T605" id="Seg_11544" s="T604">n</ta>
            <ta e="T606" id="Seg_11545" s="T605">ptcl</ta>
            <ta e="T607" id="Seg_11546" s="T606">v</ta>
            <ta e="T609" id="Seg_11547" s="T608">num</ta>
            <ta e="T610" id="Seg_11548" s="T609">num</ta>
            <ta e="T611" id="Seg_11549" s="T610">n</ta>
            <ta e="T612" id="Seg_11550" s="T611">pers</ta>
            <ta e="T613" id="Seg_11551" s="T612">n</ta>
            <ta e="T614" id="Seg_11552" s="T613">v</ta>
            <ta e="T615" id="Seg_11553" s="T614">n</ta>
            <ta e="T616" id="Seg_11554" s="T615">adv</ta>
            <ta e="T617" id="Seg_11555" s="T616">n</ta>
            <ta e="T618" id="Seg_11556" s="T617">v</ta>
            <ta e="T619" id="Seg_11557" s="T618">n</ta>
            <ta e="T621" id="Seg_11558" s="T620">v</ta>
            <ta e="T622" id="Seg_11559" s="T621">conj</ta>
            <ta e="T623" id="Seg_11560" s="T622">dempro</ta>
            <ta e="T625" id="Seg_11561" s="T624">que</ta>
            <ta e="T626" id="Seg_11562" s="T625">n</ta>
            <ta e="T627" id="Seg_11563" s="T626">ptcl</ta>
            <ta e="T628" id="Seg_11564" s="T627">v</ta>
            <ta e="T629" id="Seg_11565" s="T628">pers</ta>
            <ta e="T630" id="Seg_11566" s="T629">v</ta>
            <ta e="T631" id="Seg_11567" s="T630">v</ta>
            <ta e="T632" id="Seg_11568" s="T631">dempro</ta>
            <ta e="T633" id="Seg_11569" s="T632">v</ta>
            <ta e="T634" id="Seg_11570" s="T633">conj</ta>
            <ta e="T635" id="Seg_11571" s="T634">v</ta>
            <ta e="T636" id="Seg_11572" s="T635">adv</ta>
            <ta e="T637" id="Seg_11573" s="T636">ptcl</ta>
            <ta e="T638" id="Seg_11574" s="T637">v</ta>
            <ta e="T639" id="Seg_11575" s="T638">n</ta>
            <ta e="T641" id="Seg_11576" s="T640">pers</ta>
            <ta e="T642" id="Seg_11577" s="T641">v</ta>
            <ta e="T643" id="Seg_11578" s="T642">n</ta>
            <ta e="T644" id="Seg_11579" s="T643">pers</ta>
            <ta e="T646" id="Seg_11580" s="T645">n</ta>
            <ta e="T647" id="Seg_11581" s="T646">ptcl</ta>
            <ta e="T648" id="Seg_11582" s="T647">v</ta>
            <ta e="T649" id="Seg_11583" s="T648">v</ta>
            <ta e="T650" id="Seg_11584" s="T649">adv</ta>
            <ta e="T651" id="Seg_11585" s="T650">v</ta>
            <ta e="T652" id="Seg_11586" s="T651">num</ta>
            <ta e="T653" id="Seg_11587" s="T652">n</ta>
            <ta e="T654" id="Seg_11588" s="T653">v</ta>
            <ta e="T655" id="Seg_11589" s="T654">adj</ta>
            <ta e="T656" id="Seg_11590" s="T655">n</ta>
            <ta e="T657" id="Seg_11591" s="T656">v</ta>
            <ta e="T658" id="Seg_11592" s="T657">dempro</ta>
            <ta e="T660" id="Seg_11593" s="T659">v</ta>
            <ta e="T661" id="Seg_11594" s="T660">adv</ta>
            <ta e="T662" id="Seg_11595" s="T661">v</ta>
            <ta e="T663" id="Seg_11596" s="T662">conj</ta>
            <ta e="T664" id="Seg_11597" s="T663">num</ta>
            <ta e="T667" id="Seg_11598" s="T666">n</ta>
            <ta e="T668" id="Seg_11599" s="T667">v</ta>
            <ta e="T669" id="Seg_11600" s="T668">n</ta>
            <ta e="T670" id="Seg_11601" s="T669">v</ta>
            <ta e="T671" id="Seg_11602" s="T670">dempro</ta>
            <ta e="T672" id="Seg_11603" s="T671">v</ta>
            <ta e="T673" id="Seg_11604" s="T672">n</ta>
            <ta e="T674" id="Seg_11605" s="T673">dempro</ta>
            <ta e="T675" id="Seg_11606" s="T674">v</ta>
            <ta e="T676" id="Seg_11607" s="T675">num</ta>
            <ta e="T677" id="Seg_11608" s="T676">num</ta>
            <ta e="T678" id="Seg_11609" s="T677">n</ta>
            <ta e="T679" id="Seg_11610" s="T678">adv</ta>
            <ta e="T680" id="Seg_11611" s="T679">v</ta>
            <ta e="T681" id="Seg_11612" s="T680">n</ta>
            <ta e="T682" id="Seg_11613" s="T681">adj</ta>
            <ta e="T683" id="Seg_11614" s="T682">v</ta>
            <ta e="T685" id="Seg_11615" s="T684">pers</ta>
            <ta e="T686" id="Seg_11616" s="T685">pers</ta>
            <ta e="T687" id="Seg_11617" s="T686">n</ta>
            <ta e="T688" id="Seg_11618" s="T687">n</ta>
            <ta e="T689" id="Seg_11619" s="T688">ptcl</ta>
            <ta e="T690" id="Seg_11620" s="T689">n</ta>
            <ta e="T691" id="Seg_11621" s="T690">v</ta>
            <ta e="T692" id="Seg_11622" s="T691">que</ta>
            <ta e="T693" id="Seg_11623" s="T692">n</ta>
            <ta e="T694" id="Seg_11624" s="T693">adv</ta>
            <ta e="T695" id="Seg_11625" s="T694">v</ta>
            <ta e="T696" id="Seg_11626" s="T695">conj</ta>
            <ta e="T697" id="Seg_11627" s="T696">que</ta>
            <ta e="T698" id="Seg_11628" s="T697">n</ta>
            <ta e="T699" id="Seg_11629" s="T698">v</ta>
            <ta e="T700" id="Seg_11630" s="T699">dempro</ta>
            <ta e="T701" id="Seg_11631" s="T700">que</ta>
            <ta e="T702" id="Seg_11632" s="T701">v</ta>
            <ta e="T703" id="Seg_11633" s="T702">dempro</ta>
            <ta e="T706" id="Seg_11634" s="T705">n</ta>
            <ta e="T707" id="Seg_11635" s="T706">v</ta>
            <ta e="T709" id="Seg_11636" s="T708">adv</ta>
            <ta e="T710" id="Seg_11637" s="T709">adj</ta>
            <ta e="T711" id="Seg_11638" s="T710">n</ta>
            <ta e="T712" id="Seg_11639" s="T711">v</ta>
            <ta e="T713" id="Seg_11640" s="T712">conj</ta>
            <ta e="T714" id="Seg_11641" s="T713">dempro</ta>
            <ta e="T715" id="Seg_11642" s="T714">dempro</ta>
            <ta e="T716" id="Seg_11643" s="T715">n</ta>
            <ta e="T717" id="Seg_11644" s="T716">adv</ta>
            <ta e="T719" id="Seg_11645" s="T718">v</ta>
            <ta e="T720" id="Seg_11646" s="T719">ptcl</ta>
            <ta e="T721" id="Seg_11647" s="T720">adv</ta>
            <ta e="T722" id="Seg_11648" s="T721">dempro</ta>
            <ta e="T723" id="Seg_11649" s="T722">ptcl</ta>
            <ta e="T724" id="Seg_11650" s="T723">v</ta>
            <ta e="T725" id="Seg_11651" s="T724">adv</ta>
            <ta e="T728" id="Seg_11652" s="T727">v</ta>
            <ta e="T729" id="Seg_11653" s="T728">conj</ta>
            <ta e="T730" id="Seg_11654" s="T729">dempro</ta>
            <ta e="T731" id="Seg_11655" s="T730">n</ta>
            <ta e="T732" id="Seg_11656" s="T731">v</ta>
            <ta e="T734" id="Seg_11657" s="T733">adv</ta>
            <ta e="T735" id="Seg_11658" s="T734">dempro</ta>
            <ta e="T736" id="Seg_11659" s="T735">n</ta>
            <ta e="T739" id="Seg_11660" s="T738">v</ta>
            <ta e="T740" id="Seg_11661" s="T739">conj</ta>
            <ta e="T741" id="Seg_11662" s="T740">v</ta>
            <ta e="T742" id="Seg_11663" s="T741">v</ta>
            <ta e="T743" id="Seg_11664" s="T742">adv</ta>
            <ta e="T744" id="Seg_11665" s="T743">conj</ta>
            <ta e="T745" id="Seg_11666" s="T744">ptcl</ta>
            <ta e="T746" id="Seg_11667" s="T745">v</ta>
            <ta e="T748" id="Seg_11668" s="T747">adv</ta>
            <ta e="T749" id="Seg_11669" s="T748">n</ta>
            <ta e="T750" id="Seg_11670" s="T749">quant</ta>
            <ta e="T751" id="Seg_11671" s="T750">v</ta>
            <ta e="T752" id="Seg_11672" s="T751">conj</ta>
            <ta e="T753" id="Seg_11673" s="T752">ptcl</ta>
            <ta e="T754" id="Seg_11674" s="T753">v</ta>
            <ta e="T756" id="Seg_11675" s="T755">adv</ta>
            <ta e="T757" id="Seg_11676" s="T756">n</ta>
            <ta e="T758" id="Seg_11677" s="T757">n</ta>
            <ta e="T759" id="Seg_11678" s="T758">v</ta>
            <ta e="T760" id="Seg_11679" s="T759">n</ta>
            <ta e="T761" id="Seg_11680" s="T760">conj</ta>
            <ta e="T762" id="Seg_11681" s="T761">adv</ta>
            <ta e="T769" id="Seg_11682" s="T768">adv</ta>
            <ta e="T770" id="Seg_11683" s="T769">num</ta>
            <ta e="T771" id="Seg_11684" s="T770">num</ta>
            <ta e="T772" id="Seg_11685" s="T771">ptcl</ta>
            <ta e="T773" id="Seg_11686" s="T772">v</ta>
            <ta e="T775" id="Seg_11687" s="T774">v</ta>
            <ta e="T776" id="Seg_11688" s="T775">adv</ta>
            <ta e="T777" id="Seg_11689" s="T776">num</ta>
            <ta e="T778" id="Seg_11690" s="T777">adj</ta>
            <ta e="T782" id="Seg_11691" s="T781">dempro</ta>
            <ta e="T783" id="Seg_11692" s="T782">ptcl</ta>
            <ta e="T784" id="Seg_11693" s="T783">n</ta>
            <ta e="T785" id="Seg_11694" s="T784">v</ta>
            <ta e="T786" id="Seg_11695" s="T785">dempro</ta>
            <ta e="T787" id="Seg_11696" s="T786">ptcl</ta>
            <ta e="T788" id="Seg_11697" s="T787">adv</ta>
            <ta e="T789" id="Seg_11698" s="T788">n</ta>
            <ta e="T790" id="Seg_11699" s="T789">v</ta>
            <ta e="T791" id="Seg_11700" s="T790">adv</ta>
            <ta e="T792" id="Seg_11701" s="T791">v</ta>
            <ta e="T794" id="Seg_11702" s="T793">adv</ta>
            <ta e="T795" id="Seg_11703" s="T794">num</ta>
            <ta e="T796" id="Seg_11704" s="T795">n</ta>
            <ta e="T798" id="Seg_11705" s="T797">num</ta>
            <ta e="T799" id="Seg_11706" s="T798">n</ta>
            <ta e="T800" id="Seg_11707" s="T799">v</ta>
            <ta e="T801" id="Seg_11708" s="T800">num</ta>
            <ta e="T802" id="Seg_11709" s="T801">num</ta>
            <ta e="T803" id="Seg_11710" s="T802">adv</ta>
            <ta e="T804" id="Seg_11711" s="T803">que</ta>
            <ta e="T805" id="Seg_11712" s="T804">adj</ta>
            <ta e="T806" id="Seg_11713" s="T805">dempro</ta>
            <ta e="T807" id="Seg_11714" s="T806">v</ta>
            <ta e="T808" id="Seg_11715" s="T807">n</ta>
            <ta e="T809" id="Seg_11716" s="T808">adv</ta>
            <ta e="T810" id="Seg_11717" s="T809">dempro</ta>
            <ta e="T811" id="Seg_11718" s="T810">n</ta>
            <ta e="T812" id="Seg_11719" s="T811">v</ta>
            <ta e="T813" id="Seg_11720" s="T812">dempro</ta>
            <ta e="T814" id="Seg_11721" s="T813">v</ta>
            <ta e="T816" id="Seg_11722" s="T815">adv</ta>
            <ta e="T817" id="Seg_11723" s="T816">n</ta>
            <ta e="T818" id="Seg_11724" s="T817">v</ta>
            <ta e="T819" id="Seg_11725" s="T818">n</ta>
            <ta e="T820" id="Seg_11726" s="T819">ptcl</ta>
            <ta e="T821" id="Seg_11727" s="T820">v</ta>
            <ta e="T824" id="Seg_11728" s="T823">n</ta>
            <ta e="T825" id="Seg_11729" s="T824">v</ta>
            <ta e="T826" id="Seg_11730" s="T825">v</ta>
            <ta e="T827" id="Seg_11731" s="T826">que</ta>
            <ta e="T828" id="Seg_11732" s="T827">n</ta>
            <ta e="T829" id="Seg_11733" s="T828">v</ta>
            <ta e="T830" id="Seg_11734" s="T829">adj</ta>
            <ta e="T831" id="Seg_11735" s="T830">n</ta>
            <ta e="T832" id="Seg_11736" s="T831">ptcl</ta>
            <ta e="T833" id="Seg_11737" s="T832">ptcl</ta>
            <ta e="T834" id="Seg_11738" s="T833">adj</ta>
            <ta e="T835" id="Seg_11739" s="T834">ptcl</ta>
            <ta e="T836" id="Seg_11740" s="T835">n</ta>
            <ta e="T837" id="Seg_11741" s="T836">n</ta>
            <ta e="T838" id="Seg_11742" s="T837">v</ta>
            <ta e="T839" id="Seg_11743" s="T838">conj</ta>
            <ta e="T840" id="Seg_11744" s="T839">ptcl</ta>
            <ta e="T841" id="Seg_11745" s="T840">n</ta>
            <ta e="T842" id="Seg_11746" s="T841">n</ta>
            <ta e="T843" id="Seg_11747" s="T842">ptcl</ta>
            <ta e="T844" id="Seg_11748" s="T843">n</ta>
            <ta e="T845" id="Seg_11749" s="T844">n</ta>
            <ta e="T846" id="Seg_11750" s="T845">ptcl</ta>
            <ta e="T847" id="Seg_11751" s="T846">v</ta>
            <ta e="T848" id="Seg_11752" s="T847">conj</ta>
            <ta e="T849" id="Seg_11753" s="T848">adv</ta>
            <ta e="T850" id="Seg_11754" s="T849">adv</ta>
            <ta e="T851" id="Seg_11755" s="T850">v</ta>
            <ta e="T852" id="Seg_11756" s="T851">adv</ta>
            <ta e="T853" id="Seg_11757" s="T852">ptcl</ta>
            <ta e="T854" id="Seg_11758" s="T853">adj</ta>
            <ta e="T855" id="Seg_11759" s="T854">n</ta>
            <ta e="T856" id="Seg_11760" s="T855">v</ta>
            <ta e="T857" id="Seg_11761" s="T856">que</ta>
            <ta e="T858" id="Seg_11762" s="T857">v</ta>
            <ta e="T859" id="Seg_11763" s="T858">adv</ta>
            <ta e="T860" id="Seg_11764" s="T859">adj</ta>
            <ta e="T861" id="Seg_11765" s="T860">n</ta>
            <ta e="T862" id="Seg_11766" s="T861">adj</ta>
            <ta e="T864" id="Seg_11767" s="T863">n</ta>
            <ta e="T865" id="Seg_11768" s="T864">n</ta>
            <ta e="T866" id="Seg_11769" s="T865">adj</ta>
            <ta e="T868" id="Seg_11770" s="T867">v</ta>
            <ta e="T869" id="Seg_11771" s="T868">n</ta>
            <ta e="T870" id="Seg_11772" s="T869">n</ta>
            <ta e="T871" id="Seg_11773" s="T870">v</ta>
            <ta e="T872" id="Seg_11774" s="T871">adv</ta>
            <ta e="T873" id="Seg_11775" s="T872">adj</ta>
            <ta e="T874" id="Seg_11776" s="T873">conj</ta>
            <ta e="T875" id="Seg_11777" s="T874">v</ta>
            <ta e="T876" id="Seg_11778" s="T875">conj</ta>
            <ta e="T877" id="Seg_11779" s="T876">v</ta>
            <ta e="T878" id="Seg_11780" s="T877">adj</ta>
            <ta e="T879" id="Seg_11781" s="T878">adv</ta>
            <ta e="T880" id="Seg_11782" s="T879">conj</ta>
            <ta e="T881" id="Seg_11783" s="T880">adj</ta>
            <ta e="T882" id="Seg_11784" s="T881">n</ta>
            <ta e="T883" id="Seg_11785" s="T882">v</ta>
            <ta e="T884" id="Seg_11786" s="T883">adv</ta>
            <ta e="T885" id="Seg_11787" s="T884">n</ta>
            <ta e="T886" id="Seg_11788" s="T885">adj</ta>
            <ta e="T887" id="Seg_11789" s="T886">n</ta>
            <ta e="T888" id="Seg_11790" s="T887">quant</ta>
            <ta e="T889" id="Seg_11791" s="T888">quant</ta>
            <ta e="T890" id="Seg_11792" s="T889">n</ta>
            <ta e="T891" id="Seg_11793" s="T890">v</ta>
            <ta e="T892" id="Seg_11794" s="T891">quant</ta>
            <ta e="T893" id="Seg_11795" s="T892">que</ta>
            <ta e="T894" id="Seg_11796" s="T893">v</ta>
            <ta e="T895" id="Seg_11797" s="T894">ptcl</ta>
            <ta e="T896" id="Seg_11798" s="T895">v</ta>
            <ta e="T897" id="Seg_11799" s="T896">ptcl</ta>
            <ta e="T898" id="Seg_11800" s="T897">n</ta>
            <ta e="T899" id="Seg_11801" s="T898">v</ta>
            <ta e="T900" id="Seg_11802" s="T899">conj</ta>
            <ta e="T902" id="Seg_11803" s="T901">adv</ta>
            <ta e="T903" id="Seg_11804" s="T902">adv</ta>
            <ta e="T905" id="Seg_11805" s="T904">v</ta>
            <ta e="T906" id="Seg_11806" s="T905">adv</ta>
            <ta e="T907" id="Seg_11807" s="T906">ptcl</ta>
            <ta e="T908" id="Seg_11808" s="T907">n</ta>
            <ta e="T909" id="Seg_11809" s="T908">ptcl</ta>
            <ta e="T910" id="Seg_11810" s="T909">n</ta>
            <ta e="T911" id="Seg_11811" s="T910">ptcl</ta>
            <ta e="T912" id="Seg_11812" s="T911">n</ta>
            <ta e="T913" id="Seg_11813" s="T912">n</ta>
            <ta e="T914" id="Seg_11814" s="T913">n</ta>
            <ta e="T915" id="Seg_11815" s="T914">ptcl</ta>
            <ta e="T916" id="Seg_11816" s="T915">v</ta>
            <ta e="T917" id="Seg_11817" s="T916">adv</ta>
            <ta e="T918" id="Seg_11818" s="T917">ptcl</ta>
            <ta e="T920" id="Seg_11819" s="T919">pers</ta>
            <ta e="T921" id="Seg_11820" s="T920">v</ta>
            <ta e="T922" id="Seg_11821" s="T921">conj</ta>
            <ta e="T923" id="Seg_11822" s="T922">pers</ta>
            <ta e="T924" id="Seg_11823" s="T923">v</ta>
            <ta e="T925" id="Seg_11824" s="T924">ptcl</ta>
            <ta e="T926" id="Seg_11825" s="T925">pers</ta>
            <ta e="T927" id="Seg_11826" s="T926">n</ta>
            <ta e="T928" id="Seg_11827" s="T927">v</ta>
            <ta e="T929" id="Seg_11828" s="T928">v</ta>
            <ta e="T930" id="Seg_11829" s="T929">n</ta>
            <ta e="T931" id="Seg_11830" s="T930">ptcl</ta>
            <ta e="T932" id="Seg_11831" s="T931">ptcl</ta>
            <ta e="T933" id="Seg_11832" s="T932">v</ta>
            <ta e="T934" id="Seg_11833" s="T933">v</ta>
            <ta e="T935" id="Seg_11834" s="T934">n</ta>
            <ta e="T936" id="Seg_11835" s="T935">pers</ta>
            <ta e="T937" id="Seg_11836" s="T936">v</ta>
            <ta e="T938" id="Seg_11837" s="T937">v</ta>
            <ta e="T940" id="Seg_11838" s="T939">aux</ta>
            <ta e="T941" id="Seg_11839" s="T940">v</ta>
            <ta e="T942" id="Seg_11840" s="T941">n</ta>
            <ta e="T943" id="Seg_11841" s="T942">v</ta>
            <ta e="T944" id="Seg_11842" s="T943">adv</ta>
            <ta e="T945" id="Seg_11843" s="T944">adj</ta>
            <ta e="T946" id="Seg_11844" s="T945">v</ta>
            <ta e="T947" id="Seg_11845" s="T946">que</ta>
            <ta e="T948" id="Seg_11846" s="T947">v</ta>
            <ta e="T949" id="Seg_11847" s="T948">que</ta>
            <ta e="T950" id="Seg_11848" s="T949">ptcl</ta>
            <ta e="T951" id="Seg_11849" s="T950">v</ta>
            <ta e="T953" id="Seg_11850" s="T952">n</ta>
            <ta e="T954" id="Seg_11851" s="T953">v</ta>
            <ta e="T955" id="Seg_11852" s="T954">dempro</ta>
            <ta e="T956" id="Seg_11853" s="T955">adj</ta>
            <ta e="T957" id="Seg_11854" s="T956">propr</ta>
            <ta e="T958" id="Seg_11855" s="T957">adj</ta>
            <ta e="T959" id="Seg_11856" s="T958">propr</ta>
            <ta e="T960" id="Seg_11857" s="T959">conj</ta>
            <ta e="T961" id="Seg_11858" s="T960">que</ta>
            <ta e="T962" id="Seg_11859" s="T961">n</ta>
            <ta e="T963" id="Seg_11860" s="T962">v</ta>
            <ta e="T964" id="Seg_11861" s="T963">adv</ta>
            <ta e="T965" id="Seg_11862" s="T964">ptcl</ta>
            <ta e="T966" id="Seg_11863" s="T965">v</ta>
            <ta e="T967" id="Seg_11864" s="T966">ptcl</ta>
            <ta e="T970" id="Seg_11865" s="T969">num</ta>
            <ta e="T971" id="Seg_11866" s="T970">n</ta>
            <ta e="T972" id="Seg_11867" s="T971">ptcl</ta>
            <ta e="T973" id="Seg_11868" s="T972">pers</ta>
            <ta e="T974" id="Seg_11869" s="T973">n</ta>
            <ta e="T975" id="Seg_11870" s="T974">v</ta>
            <ta e="T976" id="Seg_11871" s="T975">dempro</ta>
            <ta e="T977" id="Seg_11872" s="T976">ptcl</ta>
            <ta e="T978" id="Seg_11873" s="T977">adv</ta>
            <ta e="T979" id="Seg_11874" s="T978">v</ta>
            <ta e="T980" id="Seg_11875" s="T979">v</ta>
            <ta e="T981" id="Seg_11876" s="T980">v</ta>
            <ta e="T982" id="Seg_11877" s="T981">v</ta>
            <ta e="T983" id="Seg_11878" s="T982">v</ta>
            <ta e="T984" id="Seg_11879" s="T983">v</ta>
            <ta e="T985" id="Seg_11880" s="T984">ptcl</ta>
            <ta e="T986" id="Seg_11881" s="T985">adj</ta>
            <ta e="T987" id="Seg_11882" s="T986">v</ta>
            <ta e="T990" id="Seg_11883" s="T989">pers</ta>
            <ta e="T991" id="Seg_11884" s="T990">ptcl</ta>
            <ta e="T992" id="Seg_11885" s="T991">v</ta>
            <ta e="T993" id="Seg_11886" s="T992">pers</ta>
            <ta e="T994" id="Seg_11887" s="T993">pers</ta>
            <ta e="T995" id="Seg_11888" s="T994">adv</ta>
            <ta e="T996" id="Seg_11889" s="T995">quant</ta>
            <ta e="T997" id="Seg_11890" s="T996">adv</ta>
            <ta e="T998" id="Seg_11891" s="T997">adv</ta>
            <ta e="T999" id="Seg_11892" s="T998">n</ta>
            <ta e="T1000" id="Seg_11893" s="T999">ptcl</ta>
            <ta e="T1001" id="Seg_11894" s="T1000">v</ta>
            <ta e="T1002" id="Seg_11895" s="T1001">n</ta>
            <ta e="T1003" id="Seg_11896" s="T1002">v</ta>
            <ta e="T1004" id="Seg_11897" s="T1003">n</ta>
            <ta e="T1005" id="Seg_11898" s="T1004">adv</ta>
            <ta e="T1006" id="Seg_11899" s="T1005">pers</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_11900" s="T0">pro.h:A</ta>
            <ta e="T2" id="Seg_11901" s="T1">adv:Time</ta>
            <ta e="T3" id="Seg_11902" s="T2">pro.h:Th</ta>
            <ta e="T6" id="Seg_11903" s="T5">0.2.h:A</ta>
            <ta e="T9" id="Seg_11904" s="T8">pro:Th</ta>
            <ta e="T11" id="Seg_11905" s="T10">0.2.h:A</ta>
            <ta e="T15" id="Seg_11906" s="T14">0.2.h:A</ta>
            <ta e="T17" id="Seg_11907" s="T16">adv:Time</ta>
            <ta e="T18" id="Seg_11908" s="T17">0.3.h:A</ta>
            <ta e="T21" id="Seg_11909" s="T20">0.3.h:A</ta>
            <ta e="T22" id="Seg_11910" s="T21">pro.h:A</ta>
            <ta e="T29" id="Seg_11911" s="T28">0.1.h:A</ta>
            <ta e="T34" id="Seg_11912" s="T33">0.1.h:A</ta>
            <ta e="T35" id="Seg_11913" s="T34">pro.h:A</ta>
            <ta e="T38" id="Seg_11914" s="T37">pro.h:A</ta>
            <ta e="T40" id="Seg_11915" s="T39">np:G</ta>
            <ta e="T41" id="Seg_11916" s="T40">0.2.h:A</ta>
            <ta e="T42" id="Seg_11917" s="T41">0.2.h:A</ta>
            <ta e="T44" id="Seg_11918" s="T43">0.2.h:A</ta>
            <ta e="T48" id="Seg_11919" s="T47">pro.h:A</ta>
            <ta e="T53" id="Seg_11920" s="T52">0.1.h:A</ta>
            <ta e="T57" id="Seg_11921" s="T56">0.2.h:E</ta>
            <ta e="T59" id="Seg_11922" s="T58">0.2.h:P</ta>
            <ta e="T61" id="Seg_11923" s="T60">0.3:P</ta>
            <ta e="T62" id="Seg_11924" s="T61">pro.h:B</ta>
            <ta e="T64" id="Seg_11925" s="T63">0.2.h:Th</ta>
            <ta e="T65" id="Seg_11926" s="T64">adv:Time</ta>
            <ta e="T66" id="Seg_11927" s="T65">pro.h:E</ta>
            <ta e="T69" id="Seg_11928" s="T68">pro.h:Th</ta>
            <ta e="T73" id="Seg_11929" s="T72">adv:Time</ta>
            <ta e="T75" id="Seg_11930" s="T74">pro.h:E</ta>
            <ta e="T81" id="Seg_11931" s="T80">adv:Time</ta>
            <ta e="T83" id="Seg_11932" s="T82">0.3:P</ta>
            <ta e="T84" id="Seg_11933" s="T83">np:Th</ta>
            <ta e="T87" id="Seg_11934" s="T86">0.3:Th</ta>
            <ta e="T88" id="Seg_11935" s="T87">np:E</ta>
            <ta e="T92" id="Seg_11936" s="T91">adv:Time</ta>
            <ta e="T97" id="Seg_11937" s="T96">adv:Time</ta>
            <ta e="T101" id="Seg_11938" s="T100">np:Th</ta>
            <ta e="T104" id="Seg_11939" s="T103">np:P</ta>
            <ta e="T112" id="Seg_11940" s="T111">0.3.h:A</ta>
            <ta e="T113" id="Seg_11941" s="T112">np:L</ta>
            <ta e="T114" id="Seg_11942" s="T113">adv:Time</ta>
            <ta e="T115" id="Seg_11943" s="T114">0.3.h:A</ta>
            <ta e="T116" id="Seg_11944" s="T115">0.1.h:A</ta>
            <ta e="T117" id="Seg_11945" s="T116">pro.h:Poss</ta>
            <ta e="T118" id="Seg_11946" s="T117">np.h:A</ta>
            <ta e="T121" id="Seg_11947" s="T120">np.h:P</ta>
            <ta e="T123" id="Seg_11948" s="T122">np:L</ta>
            <ta e="T125" id="Seg_11949" s="T124">0.3.h:E</ta>
            <ta e="T126" id="Seg_11950" s="T125">0.3.h:A</ta>
            <ta e="T127" id="Seg_11951" s="T126">np.h:Th</ta>
            <ta e="T129" id="Seg_11952" s="T128">pro.h:A</ta>
            <ta e="T132" id="Seg_11953" s="T131">np.h:Th</ta>
            <ta e="T133" id="Seg_11954" s="T132">pro.h:A</ta>
            <ta e="T139" id="Seg_11955" s="T138">0.3.h:A</ta>
            <ta e="T140" id="Seg_11956" s="T139">adv:Time</ta>
            <ta e="T144" id="Seg_11957" s="T143">adv:Time</ta>
            <ta e="T145" id="Seg_11958" s="T144">0.3.h:A</ta>
            <ta e="T146" id="Seg_11959" s="T145">np:Th</ta>
            <ta e="T147" id="Seg_11960" s="T146">0.3.h:A</ta>
            <ta e="T148" id="Seg_11961" s="T147">pro:Th</ta>
            <ta e="T150" id="Seg_11962" s="T149">0.3.h:A</ta>
            <ta e="T151" id="Seg_11963" s="T150">np:G</ta>
            <ta e="T152" id="Seg_11964" s="T151">pro.h:Th</ta>
            <ta e="T156" id="Seg_11965" s="T155">pro.h:Th</ta>
            <ta e="T158" id="Seg_11966" s="T157">0.3.h:A</ta>
            <ta e="T159" id="Seg_11967" s="T158">adv:Time</ta>
            <ta e="T160" id="Seg_11968" s="T159">0.3.h:A</ta>
            <ta e="T161" id="Seg_11969" s="T160">np:G</ta>
            <ta e="T163" id="Seg_11970" s="T162">0.3.h:A</ta>
            <ta e="T166" id="Seg_11971" s="T165">pro.h:Poss</ta>
            <ta e="T167" id="Seg_11972" s="T166">np.h:A</ta>
            <ta e="T168" id="Seg_11973" s="T167">pro.h:Th</ta>
            <ta e="T170" id="Seg_11974" s="T169">pro.h:B</ta>
            <ta e="T173" id="Seg_11975" s="T172">np:Th</ta>
            <ta e="T175" id="Seg_11976" s="T174">adv:Time</ta>
            <ta e="T177" id="Seg_11977" s="T176">np.h:P</ta>
            <ta e="T179" id="Seg_11978" s="T178">0.3.h:P</ta>
            <ta e="T184" id="Seg_11979" s="T183">np:Th</ta>
            <ta e="T186" id="Seg_11980" s="T185"> 0.3.h:Poss np.h:P</ta>
            <ta e="T188" id="Seg_11981" s="T187">pro.h:Th</ta>
            <ta e="T193" id="Seg_11982" s="T192">np.h:P</ta>
            <ta e="T194" id="Seg_11983" s="T193">0.3.h:A</ta>
            <ta e="T195" id="Seg_11984" s="T194">pro.h:P</ta>
            <ta e="T196" id="Seg_11985" s="T195">0.3.h:A</ta>
            <ta e="T200" id="Seg_11986" s="T199">pro.h:P</ta>
            <ta e="T203" id="Seg_11987" s="T202">np:Ins</ta>
            <ta e="T204" id="Seg_11988" s="T203">np:G</ta>
            <ta e="T205" id="Seg_11989" s="T204">0.3.h:A</ta>
            <ta e="T206" id="Seg_11990" s="T205">pro.h:Poss</ta>
            <ta e="T207" id="Seg_11991" s="T206">np.h:P</ta>
            <ta e="T208" id="Seg_11992" s="T207">np:L</ta>
            <ta e="T210" id="Seg_11993" s="T209">adv:Time</ta>
            <ta e="T211" id="Seg_11994" s="T210">np.h:A 0.1.h:Poss</ta>
            <ta e="T213" id="Seg_11995" s="T212">pro.h:Poss</ta>
            <ta e="T214" id="Seg_11996" s="T213">np.h:A</ta>
            <ta e="T216" id="Seg_11997" s="T215">np:Th</ta>
            <ta e="T218" id="Seg_11998" s="T217">np:G</ta>
            <ta e="T219" id="Seg_11999" s="T218">pro.h:E</ta>
            <ta e="T224" id="Seg_12000" s="T223">np:Th</ta>
            <ta e="T225" id="Seg_12001" s="T224">0.3:Th</ta>
            <ta e="T227" id="Seg_12002" s="T226">adv:Time</ta>
            <ta e="T229" id="Seg_12003" s="T228">np:P</ta>
            <ta e="T230" id="Seg_12004" s="T229">0.1.h:A</ta>
            <ta e="T231" id="Seg_12005" s="T230">np:P</ta>
            <ta e="T232" id="Seg_12006" s="T231">0.1.h:A</ta>
            <ta e="T233" id="Seg_12007" s="T232">np:P</ta>
            <ta e="T234" id="Seg_12008" s="T233">0.1.h:A</ta>
            <ta e="T236" id="Seg_12009" s="T235">np:G</ta>
            <ta e="T237" id="Seg_12010" s="T236">0.1.h:A</ta>
            <ta e="T238" id="Seg_12011" s="T237">0.1.h:A</ta>
            <ta e="T239" id="Seg_12012" s="T238">adv:Time</ta>
            <ta e="T240" id="Seg_12013" s="T239">np:P</ta>
            <ta e="T241" id="Seg_12014" s="T240">0.1.h:A</ta>
            <ta e="T242" id="Seg_12015" s="T241">np:P</ta>
            <ta e="T243" id="Seg_12016" s="T242">0.1.h:A</ta>
            <ta e="T244" id="Seg_12017" s="T243">np.h:Th</ta>
            <ta e="T248" id="Seg_12018" s="T247">np.h:E</ta>
            <ta e="T253" id="Seg_12019" s="T252">np:P</ta>
            <ta e="T254" id="Seg_12020" s="T253">0.3.h:A</ta>
            <ta e="T256" id="Seg_12021" s="T255">0.3.h:A</ta>
            <ta e="T257" id="Seg_12022" s="T256">adv:Time</ta>
            <ta e="T258" id="Seg_12023" s="T257">np:L</ta>
            <ta e="T259" id="Seg_12024" s="T258">0.3.h:A</ta>
            <ta e="T264" id="Seg_12025" s="T263">np.h:A</ta>
            <ta e="T266" id="Seg_12026" s="T265">np.h:A</ta>
            <ta e="T271" id="Seg_12027" s="T270">0.3.h:E</ta>
            <ta e="T272" id="Seg_12028" s="T271">adv:Time</ta>
            <ta e="T273" id="Seg_12029" s="T272">0.3.h:A</ta>
            <ta e="T274" id="Seg_12030" s="T273">pro.h:E</ta>
            <ta e="T279" id="Seg_12031" s="T278">pro.h:A</ta>
            <ta e="T281" id="Seg_12032" s="T280">pro.h:E</ta>
            <ta e="T285" id="Seg_12033" s="T284">pro.h:A</ta>
            <ta e="T288" id="Seg_12034" s="T287">pro.h:Th</ta>
            <ta e="T289" id="Seg_12035" s="T288">0.3.h:A</ta>
            <ta e="T294" id="Seg_12036" s="T293">np.h:Th</ta>
            <ta e="T295" id="Seg_12037" s="T294">0.3.h:A</ta>
            <ta e="T299" id="Seg_12038" s="T298">pro.h:Poss</ta>
            <ta e="T300" id="Seg_12039" s="T299">np:Th</ta>
            <ta e="T304" id="Seg_12040" s="T303">adv:L</ta>
            <ta e="T308" id="Seg_12041" s="T307">pro.h:Poss</ta>
            <ta e="T310" id="Seg_12042" s="T309">np.h:Th</ta>
            <ta e="T312" id="Seg_12043" s="T311">0.3.h:A</ta>
            <ta e="T313" id="Seg_12044" s="T312">np:Ins</ta>
            <ta e="T314" id="Seg_12045" s="T313">np.h:Th</ta>
            <ta e="T318" id="Seg_12046" s="T317">np.h:P</ta>
            <ta e="T322" id="Seg_12047" s="T321">np.h:P</ta>
            <ta e="T327" id="Seg_12048" s="T326">np.h:A</ta>
            <ta e="T329" id="Seg_12049" s="T328">pro.h:A</ta>
            <ta e="T332" id="Seg_12050" s="T331">pro.h:E</ta>
            <ta e="T333" id="Seg_12051" s="T332">pro.h:Th</ta>
            <ta e="T338" id="Seg_12052" s="T337">0.2.h:E</ta>
            <ta e="T340" id="Seg_12053" s="T339">np:L</ta>
            <ta e="T341" id="Seg_12054" s="T340">0.3:Th</ta>
            <ta e="T343" id="Seg_12055" s="T342">np:Th</ta>
            <ta e="T347" id="Seg_12056" s="T346">np:G</ta>
            <ta e="T348" id="Seg_12057" s="T347">0.3.h:A</ta>
            <ta e="T350" id="Seg_12058" s="T349">adv:Time</ta>
            <ta e="T351" id="Seg_12059" s="T350">np:Th</ta>
            <ta e="T352" id="Seg_12060" s="T351">0.3.h:A</ta>
            <ta e="T355" id="Seg_12061" s="T354">0.3.h:A</ta>
            <ta e="T356" id="Seg_12062" s="T355">np:P</ta>
            <ta e="T358" id="Seg_12063" s="T357">0.3.h:A</ta>
            <ta e="T359" id="Seg_12064" s="T358">np:P</ta>
            <ta e="T360" id="Seg_12065" s="T359">np:P</ta>
            <ta e="T361" id="Seg_12066" s="T360">pro:Th</ta>
            <ta e="T363" id="Seg_12067" s="T362">np:L</ta>
            <ta e="T364" id="Seg_12068" s="T363">adv:Time</ta>
            <ta e="T366" id="Seg_12069" s="T365">np:G</ta>
            <ta e="T367" id="Seg_12070" s="T366">0.3.h:A</ta>
            <ta e="T368" id="Seg_12071" s="T367">adv:L</ta>
            <ta e="T371" id="Seg_12072" s="T370">0.3.h:A</ta>
            <ta e="T372" id="Seg_12073" s="T371">adv:Time</ta>
            <ta e="T375" id="Seg_12074" s="T374">np:G</ta>
            <ta e="T376" id="Seg_12075" s="T375">adv:Time</ta>
            <ta e="T378" id="Seg_12076" s="T377">np:G</ta>
            <ta e="T383" id="Seg_12077" s="T382">n:Time</ta>
            <ta e="T384" id="Seg_12078" s="T383">0.3.h:A</ta>
            <ta e="T386" id="Seg_12079" s="T385">np:P</ta>
            <ta e="T388" id="Seg_12080" s="T387">adv:Time</ta>
            <ta e="T392" id="Seg_12081" s="T391">np:L</ta>
            <ta e="T395" id="Seg_12082" s="T394">pro.h:A</ta>
            <ta e="T396" id="Seg_12083" s="T395">adv:L</ta>
            <ta e="T399" id="Seg_12084" s="T398">adv:L</ta>
            <ta e="T400" id="Seg_12085" s="T399">np:P</ta>
            <ta e="T401" id="Seg_12086" s="T400">0.3.h:A</ta>
            <ta e="T402" id="Seg_12087" s="T401">pro.h:Poss</ta>
            <ta e="T403" id="Seg_12088" s="T402">np:Th</ta>
            <ta e="T406" id="Seg_12089" s="T405">np:Th</ta>
            <ta e="T407" id="Seg_12090" s="T406">0.3.h:A</ta>
            <ta e="T411" id="Seg_12091" s="T410">pro.h:Poss np:Th</ta>
            <ta e="T413" id="Seg_12092" s="T412">np:Th</ta>
            <ta e="T414" id="Seg_12093" s="T413">0.3.h:A</ta>
            <ta e="T415" id="Seg_12094" s="T414">pro.h:Poss np:Th</ta>
            <ta e="T418" id="Seg_12095" s="T417">np:Th</ta>
            <ta e="T419" id="Seg_12096" s="T418">0.3.h:A</ta>
            <ta e="T421" id="Seg_12097" s="T420">adv:Time</ta>
            <ta e="T423" id="Seg_12098" s="T422">n:Time</ta>
            <ta e="T424" id="Seg_12099" s="T423">0.3.h:A</ta>
            <ta e="T427" id="Seg_12100" s="T426">np:G</ta>
            <ta e="T428" id="Seg_12101" s="T427">0.3.h:A</ta>
            <ta e="T429" id="Seg_12102" s="T428">np.h:R</ta>
            <ta e="T430" id="Seg_12103" s="T429">0.3.h:A</ta>
            <ta e="T431" id="Seg_12104" s="T430">adv:Time</ta>
            <ta e="T436" id="Seg_12105" s="T435">0.3.h:A</ta>
            <ta e="T437" id="Seg_12106" s="T436">0.3.h:A</ta>
            <ta e="T440" id="Seg_12107" s="T439">adv:Time</ta>
            <ta e="T441" id="Seg_12108" s="T440">0.3.h:A</ta>
            <ta e="T442" id="Seg_12109" s="T441">0.3.h:A</ta>
            <ta e="T444" id="Seg_12110" s="T443">0.3.h:A</ta>
            <ta e="T445" id="Seg_12111" s="T444">np:Th</ta>
            <ta e="T446" id="Seg_12112" s="T445">0.3.h:A</ta>
            <ta e="T453" id="Seg_12113" s="T451">np:G</ta>
            <ta e="T455" id="Seg_12114" s="T454">0.3:Th</ta>
            <ta e="T457" id="Seg_12115" s="T456">np:Th</ta>
            <ta e="T458" id="Seg_12116" s="T457">adv:L</ta>
            <ta e="T459" id="Seg_12117" s="T458">np:P</ta>
            <ta e="T460" id="Seg_12118" s="T459">0.3.h:A</ta>
            <ta e="T463" id="Seg_12119" s="T462">np:G</ta>
            <ta e="T464" id="Seg_12120" s="T463">0.3.h:A</ta>
            <ta e="T466" id="Seg_12121" s="T465">n:Time</ta>
            <ta e="T467" id="Seg_12122" s="T1010">0.3.h:A</ta>
            <ta e="T468" id="Seg_12123" s="T467">np:G</ta>
            <ta e="T470" id="Seg_12124" s="T469">adv:L</ta>
            <ta e="T471" id="Seg_12125" s="T470">0.3.h:P</ta>
            <ta e="T472" id="Seg_12126" s="T471">0.3.h:P</ta>
            <ta e="T474" id="Seg_12127" s="T473">pro.h:A</ta>
            <ta e="T477" id="Seg_12128" s="T476">np:So</ta>
            <ta e="T479" id="Seg_12129" s="T478">adv:G</ta>
            <ta e="T481" id="Seg_12130" s="T480">np.h:A</ta>
            <ta e="T482" id="Seg_12131" s="T481">pro:G</ta>
            <ta e="T484" id="Seg_12132" s="T483">0.1.h:A</ta>
            <ta e="T487" id="Seg_12133" s="T486">adv:Time</ta>
            <ta e="T488" id="Seg_12134" s="T487">0.3.h:A</ta>
            <ta e="T489" id="Seg_12135" s="T488">0.3.h:A</ta>
            <ta e="T490" id="Seg_12136" s="T489">0.3.h:E</ta>
            <ta e="T495" id="Seg_12137" s="T494">adv:Time</ta>
            <ta e="T496" id="Seg_12138" s="T495">np:R</ta>
            <ta e="T498" id="Seg_12139" s="T497">0.3.h:A</ta>
            <ta e="T500" id="Seg_12140" s="T499">np:G</ta>
            <ta e="T501" id="Seg_12141" s="T500">0.3.h:A</ta>
            <ta e="T503" id="Seg_12142" s="T502">pro.h:A</ta>
            <ta e="T504" id="Seg_12143" s="T503">np:G</ta>
            <ta e="T506" id="Seg_12144" s="T505">pro.h:P</ta>
            <ta e="T510" id="Seg_12145" s="T509">pro.h:A</ta>
            <ta e="T512" id="Seg_12146" s="T511">np:P</ta>
            <ta e="T519" id="Seg_12147" s="T518">0.3.h:A</ta>
            <ta e="T520" id="Seg_12148" s="T519">0.3.h:A</ta>
            <ta e="T521" id="Seg_12149" s="T520">np:Th</ta>
            <ta e="T522" id="Seg_12150" s="T521">0.3.h:A</ta>
            <ta e="T523" id="Seg_12151" s="T522">adv:L</ta>
            <ta e="T524" id="Seg_12152" s="T523">0.3:Th</ta>
            <ta e="T526" id="Seg_12153" s="T525">adv:L</ta>
            <ta e="T527" id="Seg_12154" s="T526">0.3.h:A</ta>
            <ta e="T530" id="Seg_12155" s="T529">adv:Time</ta>
            <ta e="T532" id="Seg_12156" s="T531">0.3.h:A</ta>
            <ta e="T533" id="Seg_12157" s="T532">0.3.h:A</ta>
            <ta e="T534" id="Seg_12158" s="T533">adv:Time</ta>
            <ta e="T536" id="Seg_12159" s="T535">0.3.h:A</ta>
            <ta e="T538" id="Seg_12160" s="T537">pro.h:A</ta>
            <ta e="T543" id="Seg_12161" s="T542">0.3.h:A</ta>
            <ta e="T544" id="Seg_12162" s="T543">adv:Time</ta>
            <ta e="T545" id="Seg_12163" s="T544">np:Cau</ta>
            <ta e="T547" id="Seg_12164" s="T546">0.3:E</ta>
            <ta e="T549" id="Seg_12165" s="T548">pro:G</ta>
            <ta e="T550" id="Seg_12166" s="T549">np:G</ta>
            <ta e="T552" id="Seg_12167" s="T551">0.3.h:A</ta>
            <ta e="T553" id="Seg_12168" s="T552">adv:Time</ta>
            <ta e="T554" id="Seg_12169" s="T553">pro.h:A</ta>
            <ta e="T560" id="Seg_12170" s="T559">pro.h:Poss</ta>
            <ta e="T561" id="Seg_12171" s="T560">np:E</ta>
            <ta e="T564" id="Seg_12172" s="T563">adv:L</ta>
            <ta e="T565" id="Seg_12173" s="T564">0.3.h:A</ta>
            <ta e="T566" id="Seg_12174" s="T565">pro.h:A</ta>
            <ta e="T567" id="Seg_12175" s="T566">adv:Time</ta>
            <ta e="T569" id="Seg_12176" s="T568">np:Th</ta>
            <ta e="T570" id="Seg_12177" s="T569">0.1.h:A</ta>
            <ta e="T574" id="Seg_12178" s="T573">0.3:Th</ta>
            <ta e="T575" id="Seg_12179" s="T574">pro.h:A</ta>
            <ta e="T576" id="Seg_12180" s="T575">np:G</ta>
            <ta e="T580" id="Seg_12181" s="T579">adv:Time</ta>
            <ta e="T581" id="Seg_12182" s="T580">0.1.h:A</ta>
            <ta e="T582" id="Seg_12183" s="T581">0.1.h:A</ta>
            <ta e="T583" id="Seg_12184" s="T582">0.1.h:E</ta>
            <ta e="T584" id="Seg_12185" s="T583">0.1.h:A</ta>
            <ta e="T586" id="Seg_12186" s="T585">np:E 0.1.h:Poss</ta>
            <ta e="T590" id="Seg_12187" s="T589">adv:Time</ta>
            <ta e="T591" id="Seg_12188" s="T590">np:E</ta>
            <ta e="T593" id="Seg_12189" s="T592">pro.h:A</ta>
            <ta e="T596" id="Seg_12190" s="T595">np:G</ta>
            <ta e="T597" id="Seg_12191" s="T596">np:G</ta>
            <ta e="T598" id="Seg_12192" s="T597">0.1.h:A</ta>
            <ta e="T599" id="Seg_12193" s="T598">0.1.h:A</ta>
            <ta e="T604" id="Seg_12194" s="T603">np:P</ta>
            <ta e="T605" id="Seg_12195" s="T604">np:E</ta>
            <ta e="T611" id="Seg_12196" s="T610">n:Time</ta>
            <ta e="T612" id="Seg_12197" s="T611">pro.h:A</ta>
            <ta e="T613" id="Seg_12198" s="T612">np:P</ta>
            <ta e="T615" id="Seg_12199" s="T614">np:Ins</ta>
            <ta e="T616" id="Seg_12200" s="T615">adv:L</ta>
            <ta e="T617" id="Seg_12201" s="T616">np:Th</ta>
            <ta e="T619" id="Seg_12202" s="T618">np:P</ta>
            <ta e="T623" id="Seg_12203" s="T622">pro.h:A</ta>
            <ta e="T626" id="Seg_12204" s="T625">np:P</ta>
            <ta e="T629" id="Seg_12205" s="T628">pro.h:A</ta>
            <ta e="T631" id="Seg_12206" s="T630">0.2.h:A</ta>
            <ta e="T632" id="Seg_12207" s="T631">pro.h:A</ta>
            <ta e="T635" id="Seg_12208" s="T634">0.3.h:A</ta>
            <ta e="T636" id="Seg_12209" s="T635">adv:Time</ta>
            <ta e="T639" id="Seg_12210" s="T638">np:E</ta>
            <ta e="T641" id="Seg_12211" s="T640">pro.h:Poss</ta>
            <ta e="T643" id="Seg_12212" s="T642">np.h:Th</ta>
            <ta e="T644" id="Seg_12213" s="T643">pro.h:A</ta>
            <ta e="T646" id="Seg_12214" s="T645">np:Ins</ta>
            <ta e="T649" id="Seg_12215" s="T648">0.3.h:A</ta>
            <ta e="T650" id="Seg_12216" s="T649">adv:Time</ta>
            <ta e="T651" id="Seg_12217" s="T650">0.3.h:A</ta>
            <ta e="T653" id="Seg_12218" s="T652">np:Th</ta>
            <ta e="T656" id="Seg_12219" s="T655">np:E</ta>
            <ta e="T658" id="Seg_12220" s="T657">np:P</ta>
            <ta e="T659" id="Seg_12221" s="T658">0.2.h:A</ta>
            <ta e="T660" id="Seg_12222" s="T659">0.2.h:A</ta>
            <ta e="T661" id="Seg_12223" s="T660">adv:Time</ta>
            <ta e="T662" id="Seg_12224" s="T661">0.2.h:A</ta>
            <ta e="T667" id="Seg_12225" s="T666">np:Th</ta>
            <ta e="T670" id="Seg_12226" s="T669">0.3.h:A</ta>
            <ta e="T671" id="Seg_12227" s="T670">pro:Th</ta>
            <ta e="T672" id="Seg_12228" s="T671">0.2.h:A</ta>
            <ta e="T673" id="Seg_12229" s="T672">np:G</ta>
            <ta e="T674" id="Seg_12230" s="T673">pro.h:Th</ta>
            <ta e="T679" id="Seg_12231" s="T678">adv:Time</ta>
            <ta e="T680" id="Seg_12232" s="T679">0.2.h:A</ta>
            <ta e="T681" id="Seg_12233" s="T680">np:P</ta>
            <ta e="T686" id="Seg_12234" s="T685">pro.h:Poss</ta>
            <ta e="T687" id="Seg_12235" s="T686">np.h:A</ta>
            <ta e="T688" id="Seg_12236" s="T687">np:L</ta>
            <ta e="T690" id="Seg_12237" s="T689">np:Ins</ta>
            <ta e="T692" id="Seg_12238" s="T691">pro.h:Poss</ta>
            <ta e="T693" id="Seg_12239" s="T692">np:A</ta>
            <ta e="T694" id="Seg_12240" s="T693">adv:Time</ta>
            <ta e="T697" id="Seg_12241" s="T696">pro.h:Poss</ta>
            <ta e="T698" id="Seg_12242" s="T697">np:Th</ta>
            <ta e="T700" id="Seg_12243" s="T699">pro.h:A</ta>
            <ta e="T701" id="Seg_12244" s="T700">pro.h:Poss np:Th</ta>
            <ta e="T703" id="Seg_12245" s="T702">pro.h:A</ta>
            <ta e="T706" id="Seg_12246" s="T705">np:Th</ta>
            <ta e="T711" id="Seg_12247" s="T710">np:Th</ta>
            <ta e="T712" id="Seg_12248" s="T711">0.3.h:A</ta>
            <ta e="T716" id="Seg_12249" s="T715">np:Th</ta>
            <ta e="T719" id="Seg_12250" s="T718">0.3.h:A</ta>
            <ta e="T721" id="Seg_12251" s="T720">adv:Time</ta>
            <ta e="T722" id="Seg_12252" s="T721">pro:Th</ta>
            <ta e="T725" id="Seg_12253" s="T724">adv:L</ta>
            <ta e="T728" id="Seg_12254" s="T727">0.3:A</ta>
            <ta e="T731" id="Seg_12255" s="T730">np:Th</ta>
            <ta e="T734" id="Seg_12256" s="T733">adv:Time</ta>
            <ta e="T735" id="Seg_12257" s="T734">pro.h:A</ta>
            <ta e="T736" id="Seg_12258" s="T735">np:Th</ta>
            <ta e="T741" id="Seg_12259" s="T740">0.3:A</ta>
            <ta e="T742" id="Seg_12260" s="T741">0.3:A</ta>
            <ta e="T746" id="Seg_12261" s="T745">0.3:Th</ta>
            <ta e="T748" id="Seg_12262" s="T747">adv:Time</ta>
            <ta e="T749" id="Seg_12263" s="T748">np:P</ta>
            <ta e="T751" id="Seg_12264" s="T750">0.3.h:A</ta>
            <ta e="T754" id="Seg_12265" s="T753">0.3.h:A</ta>
            <ta e="T756" id="Seg_12266" s="T755">adv:Time</ta>
            <ta e="T757" id="Seg_12267" s="T756">np.h:A</ta>
            <ta e="T758" id="Seg_12268" s="T757">np:Th</ta>
            <ta e="T760" id="Seg_12269" s="T759">np:L</ta>
            <ta e="T762" id="Seg_12270" s="T761">adv:Time</ta>
            <ta e="T769" id="Seg_12271" s="T768">adv:Time</ta>
            <ta e="T771" id="Seg_12272" s="T770">np:G</ta>
            <ta e="T776" id="Seg_12273" s="T775">adv:Time</ta>
            <ta e="T777" id="Seg_12274" s="T776">np.h:Th</ta>
            <ta e="T782" id="Seg_12275" s="T781">pro.h:Th</ta>
            <ta e="T784" id="Seg_12276" s="T783">np:L</ta>
            <ta e="T785" id="Seg_12277" s="T784">0.3.h:A</ta>
            <ta e="T786" id="Seg_12278" s="T785">pro.h:A</ta>
            <ta e="T788" id="Seg_12279" s="T787">adv:Time</ta>
            <ta e="T789" id="Seg_12280" s="T788">np:Th</ta>
            <ta e="T791" id="Seg_12281" s="T790">adv:Time</ta>
            <ta e="T792" id="Seg_12282" s="T791">0.3.h:A</ta>
            <ta e="T794" id="Seg_12283" s="T793">adv:Time</ta>
            <ta e="T799" id="Seg_12284" s="T798">np.h:A</ta>
            <ta e="T802" id="Seg_12285" s="T801">np:G</ta>
            <ta e="T803" id="Seg_12286" s="T802">adv:Time</ta>
            <ta e="T805" id="Seg_12287" s="T804">np.h:A</ta>
            <ta e="T806" id="Seg_12288" s="T805">pro.h:Th</ta>
            <ta e="T808" id="Seg_12289" s="T807">np:G</ta>
            <ta e="T809" id="Seg_12290" s="T808">adv:Time</ta>
            <ta e="T810" id="Seg_12291" s="T809">pro.h:A</ta>
            <ta e="T811" id="Seg_12292" s="T810">np:Th</ta>
            <ta e="T814" id="Seg_12293" s="T813">0.3.h:A</ta>
            <ta e="T816" id="Seg_12294" s="T815">adv:Time</ta>
            <ta e="T817" id="Seg_12295" s="T816">np:G</ta>
            <ta e="T818" id="Seg_12296" s="T817">0.3.h:E</ta>
            <ta e="T819" id="Seg_12297" s="T818">np:P</ta>
            <ta e="T821" id="Seg_12298" s="T820">0.3.h:E</ta>
            <ta e="T824" id="Seg_12299" s="T823">np:Th</ta>
            <ta e="T826" id="Seg_12300" s="T825">0.2.h:A</ta>
            <ta e="T828" id="Seg_12301" s="T827">np:Pth</ta>
            <ta e="T829" id="Seg_12302" s="T828">0.2.h:A</ta>
            <ta e="T831" id="Seg_12303" s="T830">np:Th</ta>
            <ta e="T837" id="Seg_12304" s="T836">np:Th</ta>
            <ta e="T845" id="Seg_12305" s="T844">np:Th</ta>
            <ta e="T849" id="Seg_12306" s="T848">adv:L</ta>
            <ta e="T851" id="Seg_12307" s="T850">0.2.h:A</ta>
            <ta e="T852" id="Seg_12308" s="T851">adv:L</ta>
            <ta e="T855" id="Seg_12309" s="T854">np:Th</ta>
            <ta e="T857" id="Seg_12310" s="T856">pro:Th</ta>
            <ta e="T861" id="Seg_12311" s="T860">np:Th</ta>
            <ta e="T869" id="Seg_12312" s="T868">np:P</ta>
            <ta e="T870" id="Seg_12313" s="T869">np:Th</ta>
            <ta e="T873" id="Seg_12314" s="T872">0.3:Th</ta>
            <ta e="T875" id="Seg_12315" s="T874">0.2.h:A</ta>
            <ta e="T877" id="Seg_12316" s="T876">0.2.h:E</ta>
            <ta e="T879" id="Seg_12317" s="T878">adv:L</ta>
            <ta e="T882" id="Seg_12318" s="T881">np:G</ta>
            <ta e="T883" id="Seg_12319" s="T882">0.3.h:A</ta>
            <ta e="T885" id="Seg_12320" s="T884">np:Th</ta>
            <ta e="T887" id="Seg_12321" s="T886">np.h:Th</ta>
            <ta e="T889" id="Seg_12322" s="T888">np.h:A</ta>
            <ta e="T890" id="Seg_12323" s="T889">np:P</ta>
            <ta e="T893" id="Seg_12324" s="T892">pro:P</ta>
            <ta e="T894" id="Seg_12325" s="T893">0.3.h:A</ta>
            <ta e="T896" id="Seg_12326" s="T895">0.3.h:A</ta>
            <ta e="T898" id="Seg_12327" s="T897">np:Th</ta>
            <ta e="T899" id="Seg_12328" s="T898">0.3.h:A</ta>
            <ta e="T902" id="Seg_12329" s="T901">adv:L</ta>
            <ta e="T905" id="Seg_12330" s="T904">0.2.h:A</ta>
            <ta e="T906" id="Seg_12331" s="T905">adv:L</ta>
            <ta e="T908" id="Seg_12332" s="T907">np:Th</ta>
            <ta e="T910" id="Seg_12333" s="T909">np:Th</ta>
            <ta e="T912" id="Seg_12334" s="T911">np:Th</ta>
            <ta e="T913" id="Seg_12335" s="T912">np:Th</ta>
            <ta e="T914" id="Seg_12336" s="T913">np.h:P</ta>
            <ta e="T917" id="Seg_12337" s="T916">adv:Time</ta>
            <ta e="T920" id="Seg_12338" s="T919">pro.h:A</ta>
            <ta e="T926" id="Seg_12339" s="T925">pro.h:A</ta>
            <ta e="T927" id="Seg_12340" s="T926">np:P</ta>
            <ta e="T930" id="Seg_12341" s="T929">np:P</ta>
            <ta e="T935" id="Seg_12342" s="T934">np:Th</ta>
            <ta e="T936" id="Seg_12343" s="T935">pro.h:R</ta>
            <ta e="T937" id="Seg_12344" s="T936">0.1.h:A</ta>
            <ta e="T940" id="Seg_12345" s="T939">0.2.h:A</ta>
            <ta e="T943" id="Seg_12346" s="T942">0.2.h:E</ta>
            <ta e="T947" id="Seg_12347" s="T946">pro:G</ta>
            <ta e="T948" id="Seg_12348" s="T947">0.1.h:A</ta>
            <ta e="T949" id="Seg_12349" s="T948">pro.h:R</ta>
            <ta e="T951" id="Seg_12350" s="T950">0.1.h:A</ta>
            <ta e="T953" id="Seg_12351" s="T952">np:Th</ta>
            <ta e="T961" id="Seg_12352" s="T960">pro:L</ta>
            <ta e="T962" id="Seg_12353" s="T961">np:Th</ta>
            <ta e="T963" id="Seg_12354" s="T962">0.3.h:A</ta>
            <ta e="T964" id="Seg_12355" s="T963">adv:L</ta>
            <ta e="T966" id="Seg_12356" s="T965">0.3.h:A</ta>
            <ta e="T971" id="Seg_12357" s="T970">n:Time</ta>
            <ta e="T976" id="Seg_12358" s="T975">pro.h:Th</ta>
            <ta e="T980" id="Seg_12359" s="T979">0.3.h:Th</ta>
            <ta e="T981" id="Seg_12360" s="T980">0.3.h:A</ta>
            <ta e="T982" id="Seg_12361" s="T981">0.3.h:A</ta>
            <ta e="T983" id="Seg_12362" s="T982">0.1.h:A</ta>
            <ta e="T987" id="Seg_12363" s="T986">0.3:P</ta>
            <ta e="T994" id="Seg_12364" s="T993">pro.h:B</ta>
            <ta e="T996" id="Seg_12365" s="T995">np:Th</ta>
            <ta e="T998" id="Seg_12366" s="T997">adv:Time</ta>
            <ta e="T999" id="Seg_12367" s="T998">np:P</ta>
            <ta e="T1002" id="Seg_12368" s="T1001">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_12369" s="T0">pro.h:S</ta>
            <ta e="T3" id="Seg_12370" s="T2">pro.h:O</ta>
            <ta e="T5" id="Seg_12371" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_12372" s="T5">v:pred 0.2.h:S</ta>
            <ta e="T7" id="Seg_12373" s="T6">s:purp</ta>
            <ta e="T10" id="Seg_12374" s="T9">ptcl.neg</ta>
            <ta e="T11" id="Seg_12375" s="T10">v:pred 0.2.h:S</ta>
            <ta e="T14" id="Seg_12376" s="T13">ptcl.neg</ta>
            <ta e="T15" id="Seg_12377" s="T14">v:pred 0.2.h:S</ta>
            <ta e="T18" id="Seg_12378" s="T17">v:pred 0.3.h:S</ta>
            <ta e="T21" id="Seg_12379" s="T20">v:pred 0.3.h:S</ta>
            <ta e="T22" id="Seg_12380" s="T21">pro.h:S</ta>
            <ta e="T23" id="Seg_12381" s="T22">v:pred</ta>
            <ta e="T25" id="Seg_12382" s="T24">ptcl.neg</ta>
            <ta e="T29" id="Seg_12383" s="T28">v:pred 0.1.h:S</ta>
            <ta e="T33" id="Seg_12384" s="T32">ptcl.neg</ta>
            <ta e="T34" id="Seg_12385" s="T33">v:pred 0.1.h:S</ta>
            <ta e="T35" id="Seg_12386" s="T34">pro.h:S</ta>
            <ta e="T36" id="Seg_12387" s="T35">v:pred</ta>
            <ta e="T38" id="Seg_12388" s="T37">pro.h:S</ta>
            <ta e="T39" id="Seg_12389" s="T38">v:pred</ta>
            <ta e="T41" id="Seg_12390" s="T40">v:pred 0.2.h:S</ta>
            <ta e="T42" id="Seg_12391" s="T41">v:pred 0.2.h:S</ta>
            <ta e="T44" id="Seg_12392" s="T43">v:pred 0.2.h:S</ta>
            <ta e="T48" id="Seg_12393" s="T47">pro.h:S</ta>
            <ta e="T49" id="Seg_12394" s="T48">ptcl.neg</ta>
            <ta e="T50" id="Seg_12395" s="T49">v:pred</ta>
            <ta e="T52" id="Seg_12396" s="T51">ptcl.neg</ta>
            <ta e="T53" id="Seg_12397" s="T52">v:pred 0.1.h:S</ta>
            <ta e="T57" id="Seg_12398" s="T56">v:pred 0.2.h:S</ta>
            <ta e="T58" id="Seg_12399" s="T57">ptcl.neg</ta>
            <ta e="T59" id="Seg_12400" s="T58">v:pred 0.2.h:S</ta>
            <ta e="T60" id="Seg_12401" s="T59">adj:pred</ta>
            <ta e="T61" id="Seg_12402" s="T60">cop 0.3:S</ta>
            <ta e="T64" id="Seg_12403" s="T63">v:pred 0.2.h:S</ta>
            <ta e="T66" id="Seg_12404" s="T65">pro.h:S</ta>
            <ta e="T68" id="Seg_12405" s="T67">v:pred</ta>
            <ta e="T69" id="Seg_12406" s="T68">pro.h:O</ta>
            <ta e="T74" id="Seg_12407" s="T73">np:S</ta>
            <ta e="T75" id="Seg_12408" s="T74">pro.h:O</ta>
            <ta e="T78" id="Seg_12409" s="T77">v:pred</ta>
            <ta e="T79" id="Seg_12410" s="T78">v:pred</ta>
            <ta e="T82" id="Seg_12411" s="T81">adj:pred</ta>
            <ta e="T83" id="Seg_12412" s="T82">cop 0.3:S</ta>
            <ta e="T84" id="Seg_12413" s="T83">np:S</ta>
            <ta e="T85" id="Seg_12414" s="T84">v:pred</ta>
            <ta e="T87" id="Seg_12415" s="T86">v:pred 0.3:S</ta>
            <ta e="T88" id="Seg_12416" s="T87">np:S</ta>
            <ta e="T90" id="Seg_12417" s="T89">v:pred</ta>
            <ta e="T101" id="Seg_12418" s="T100">np:S</ta>
            <ta e="T102" id="Seg_12419" s="T101">v:pred</ta>
            <ta e="T104" id="Seg_12420" s="T103">np:S</ta>
            <ta e="T105" id="Seg_12421" s="T104">v:pred</ta>
            <ta e="T106" id="Seg_12422" s="T105">np.h:S</ta>
            <ta e="T108" id="Seg_12423" s="T107">v:pred</ta>
            <ta e="T112" id="Seg_12424" s="T111">v:pred 0.3.h:S</ta>
            <ta e="T115" id="Seg_12425" s="T114">v:pred 0.3.h:S</ta>
            <ta e="T116" id="Seg_12426" s="T115">v:pred 0.1.h:S</ta>
            <ta e="T118" id="Seg_12427" s="T117">np.h:S</ta>
            <ta e="T121" id="Seg_12428" s="T120">np.h:O</ta>
            <ta e="T122" id="Seg_12429" s="T121">v:pred</ta>
            <ta e="T125" id="Seg_12430" s="T124">v:pred 0.3.h:S</ta>
            <ta e="T126" id="Seg_12431" s="T125">v:pred 0.3.h:S</ta>
            <ta e="T127" id="Seg_12432" s="T126">np.h:S</ta>
            <ta e="T128" id="Seg_12433" s="T127">v:pred</ta>
            <ta e="T129" id="Seg_12434" s="T128">pro.h:S</ta>
            <ta e="T130" id="Seg_12435" s="T129">v:pred</ta>
            <ta e="T132" id="Seg_12436" s="T131">np.h:O</ta>
            <ta e="T133" id="Seg_12437" s="T132">pro.h:S</ta>
            <ta e="T137" id="Seg_12438" s="T136">v:pred</ta>
            <ta e="T139" id="Seg_12439" s="T138">v:pred 0.3.h:S</ta>
            <ta e="T145" id="Seg_12440" s="T144">v:pred 0.3.h:S</ta>
            <ta e="T146" id="Seg_12441" s="T145">np:O</ta>
            <ta e="T147" id="Seg_12442" s="T146">v:pred 0.3.h:S</ta>
            <ta e="T148" id="Seg_12443" s="T147">pro:O</ta>
            <ta e="T150" id="Seg_12444" s="T149">v:pred 0.3.h:S</ta>
            <ta e="T152" id="Seg_12445" s="T151">pro.h:S</ta>
            <ta e="T154" id="Seg_12446" s="T153">v:pred</ta>
            <ta e="T156" id="Seg_12447" s="T155">pro.h:O</ta>
            <ta e="T158" id="Seg_12448" s="T157">v:pred 0.3.h:S</ta>
            <ta e="T160" id="Seg_12449" s="T159">v:pred 0.3.h:S</ta>
            <ta e="T163" id="Seg_12450" s="T162">v:pred 0.3.h:S</ta>
            <ta e="T167" id="Seg_12451" s="T166">np.h:S</ta>
            <ta e="T168" id="Seg_12452" s="T167">pro.h:O</ta>
            <ta e="T169" id="Seg_12453" s="T168">v:pred</ta>
            <ta e="T173" id="Seg_12454" s="T172">np:S</ta>
            <ta e="T174" id="Seg_12455" s="T173">v:pred</ta>
            <ta e="T177" id="Seg_12456" s="T176">np.h:S</ta>
            <ta e="T178" id="Seg_12457" s="T177">v:pred</ta>
            <ta e="T179" id="Seg_12458" s="T178">v:pred 0.3.h:S</ta>
            <ta e="T184" id="Seg_12459" s="T183">np:S</ta>
            <ta e="T185" id="Seg_12460" s="T184">v:pred</ta>
            <ta e="T186" id="Seg_12461" s="T185">np.h:S</ta>
            <ta e="T187" id="Seg_12462" s="T186">v:pred</ta>
            <ta e="T188" id="Seg_12463" s="T187">pro.h:S</ta>
            <ta e="T189" id="Seg_12464" s="T188">v:pred</ta>
            <ta e="T193" id="Seg_12465" s="T192">np.h:O</ta>
            <ta e="T194" id="Seg_12466" s="T193">v:pred 0.3.h:S</ta>
            <ta e="T195" id="Seg_12467" s="T194">pro.h:O</ta>
            <ta e="T196" id="Seg_12468" s="T195">v:pred 0.3.h:S</ta>
            <ta e="T200" id="Seg_12469" s="T199">pro.h:S</ta>
            <ta e="T202" id="Seg_12470" s="T201">v:pred</ta>
            <ta e="T205" id="Seg_12471" s="T204">v:pred 0.3.h:S</ta>
            <ta e="T207" id="Seg_12472" s="T206">np.h:S</ta>
            <ta e="T209" id="Seg_12473" s="T208">v:pred</ta>
            <ta e="T211" id="Seg_12474" s="T210">np.h:S</ta>
            <ta e="T214" id="Seg_12475" s="T213">np.h:S</ta>
            <ta e="T217" id="Seg_12476" s="T216">v:pred</ta>
            <ta e="T219" id="Seg_12477" s="T218">pro.h:S</ta>
            <ta e="T222" id="Seg_12478" s="T221">v:pred</ta>
            <ta e="T224" id="Seg_12479" s="T223">n:pred</ta>
            <ta e="T225" id="Seg_12480" s="T224">cop 0.3:S</ta>
            <ta e="T229" id="Seg_12481" s="T228">np:O</ta>
            <ta e="T230" id="Seg_12482" s="T229">v:pred 0.1.h:S</ta>
            <ta e="T231" id="Seg_12483" s="T230">np:O</ta>
            <ta e="T232" id="Seg_12484" s="T231">v:pred 0.1.h:S</ta>
            <ta e="T233" id="Seg_12485" s="T232">np:O</ta>
            <ta e="T234" id="Seg_12486" s="T233">v:pred 0.1.h:S</ta>
            <ta e="T237" id="Seg_12487" s="T236">v:pred 0.1.h:S</ta>
            <ta e="T238" id="Seg_12488" s="T237">v:pred 0.1.h:S</ta>
            <ta e="T240" id="Seg_12489" s="T239">np:O</ta>
            <ta e="T241" id="Seg_12490" s="T240">v:pred 0.1.h:S</ta>
            <ta e="T242" id="Seg_12491" s="T241">np:O</ta>
            <ta e="T243" id="Seg_12492" s="T242">v:pred 0.1.h:S</ta>
            <ta e="T244" id="Seg_12493" s="T243">np.h:S</ta>
            <ta e="T245" id="Seg_12494" s="T244">v:pred</ta>
            <ta e="T248" id="Seg_12495" s="T247">np.h:S</ta>
            <ta e="T249" id="Seg_12496" s="T248">v:pred</ta>
            <ta e="T253" id="Seg_12497" s="T252">np:O</ta>
            <ta e="T254" id="Seg_12498" s="T253">v:pred 0.3.h:S</ta>
            <ta e="T256" id="Seg_12499" s="T255">v:pred 0.3.h:S</ta>
            <ta e="T259" id="Seg_12500" s="T258">v:pred 0.3.h:S</ta>
            <ta e="T263" id="Seg_12501" s="T262">v:pred</ta>
            <ta e="T264" id="Seg_12502" s="T263">np.h:S</ta>
            <ta e="T266" id="Seg_12503" s="T265">np.h:S</ta>
            <ta e="T267" id="Seg_12504" s="T266">v:pred</ta>
            <ta e="T271" id="Seg_12505" s="T270">v:pred 0.3.h:S</ta>
            <ta e="T273" id="Seg_12506" s="T272">v:pred 0.3.h:S</ta>
            <ta e="T274" id="Seg_12507" s="T273">pro.h:S</ta>
            <ta e="T277" id="Seg_12508" s="T276">v:pred</ta>
            <ta e="T279" id="Seg_12509" s="T278">pro.h:S</ta>
            <ta e="T280" id="Seg_12510" s="T279">v:pred</ta>
            <ta e="T281" id="Seg_12511" s="T280">pro.h:S</ta>
            <ta e="T283" id="Seg_12512" s="T282">v:pred</ta>
            <ta e="T285" id="Seg_12513" s="T284">pro.h:S</ta>
            <ta e="T286" id="Seg_12514" s="T285">v:pred</ta>
            <ta e="T288" id="Seg_12515" s="T287">pro.h:O</ta>
            <ta e="T289" id="Seg_12516" s="T288">v:pred 0.3.h:S</ta>
            <ta e="T294" id="Seg_12517" s="T293">np.h:O</ta>
            <ta e="T295" id="Seg_12518" s="T294">v:pred 0.3.h:S</ta>
            <ta e="T300" id="Seg_12519" s="T299">np:S</ta>
            <ta e="T301" id="Seg_12520" s="T300">adj:pred</ta>
            <ta e="T302" id="Seg_12521" s="T301">cop</ta>
            <ta e="T306" id="Seg_12522" s="T305">conv:pred</ta>
            <ta e="T310" id="Seg_12523" s="T309">np.h:S</ta>
            <ta e="T311" id="Seg_12524" s="T310">v:pred</ta>
            <ta e="T312" id="Seg_12525" s="T311">v:pred 0.3.h:S</ta>
            <ta e="T313" id="Seg_12526" s="T312">np.h:O</ta>
            <ta e="T314" id="Seg_12527" s="T313">np.h:S</ta>
            <ta e="T315" id="Seg_12528" s="T314">v:pred</ta>
            <ta e="T318" id="Seg_12529" s="T317">np.h:S</ta>
            <ta e="T319" id="Seg_12530" s="T318">v:pred</ta>
            <ta e="T322" id="Seg_12531" s="T321">np.h:S</ta>
            <ta e="T323" id="Seg_12532" s="T322">v:pred</ta>
            <ta e="T327" id="Seg_12533" s="T326">np.h:S</ta>
            <ta e="T329" id="Seg_12534" s="T328">pro.h:S</ta>
            <ta e="T330" id="Seg_12535" s="T329">v:pred</ta>
            <ta e="T332" id="Seg_12536" s="T331">pro.h:S</ta>
            <ta e="T333" id="Seg_12537" s="T332">pro.h:O</ta>
            <ta e="T334" id="Seg_12538" s="T333">ptcl.neg</ta>
            <ta e="T335" id="Seg_12539" s="T334">v:pred</ta>
            <ta e="T338" id="Seg_12540" s="T337">v:pred 0.2.h:S</ta>
            <ta e="T341" id="Seg_12541" s="T340">cop 0.3:S</ta>
            <ta e="T343" id="Seg_12542" s="T342">n:pred</ta>
            <ta e="T348" id="Seg_12543" s="T347">v:pred 0.3.h:S</ta>
            <ta e="T351" id="Seg_12544" s="T350">np:O</ta>
            <ta e="T352" id="Seg_12545" s="T351">v:pred 0.3.h:S</ta>
            <ta e="T355" id="Seg_12546" s="T354">v:pred 0.3.h:S</ta>
            <ta e="T356" id="Seg_12547" s="T355">np:O</ta>
            <ta e="T358" id="Seg_12548" s="T357">v:pred 0.3.h:S</ta>
            <ta e="T359" id="Seg_12549" s="T358">np:O</ta>
            <ta e="T360" id="Seg_12550" s="T359">np:O</ta>
            <ta e="T361" id="Seg_12551" s="T360">pro:S</ta>
            <ta e="T362" id="Seg_12552" s="T361">v:pred</ta>
            <ta e="T367" id="Seg_12553" s="T366">v:pred 0.3.h:S</ta>
            <ta e="T371" id="Seg_12554" s="T370">v:pred 0.3.h:S</ta>
            <ta e="T384" id="Seg_12555" s="T383">v:pred 0.3.h:S</ta>
            <ta e="T386" id="Seg_12556" s="T385">np:O</ta>
            <ta e="T394" id="Seg_12557" s="T393">v:pred</ta>
            <ta e="T395" id="Seg_12558" s="T394">pro.h:S</ta>
            <ta e="T397" id="Seg_12559" s="T396">v:pred</ta>
            <ta e="T400" id="Seg_12560" s="T399">np:O</ta>
            <ta e="T401" id="Seg_12561" s="T400">v:pred 0.3.h:S</ta>
            <ta e="T403" id="Seg_12562" s="T402">np:S</ta>
            <ta e="T404" id="Seg_12563" s="T403">adj:pred</ta>
            <ta e="T407" id="Seg_12564" s="T406">v:pred 0.3.h:S</ta>
            <ta e="T411" id="Seg_12565" s="T410">np:S</ta>
            <ta e="T412" id="Seg_12566" s="T411">adj:pred</ta>
            <ta e="T413" id="Seg_12567" s="T412">np:O</ta>
            <ta e="T414" id="Seg_12568" s="T413">v:pred 0.3.h:S</ta>
            <ta e="T415" id="Seg_12569" s="T414">np:S</ta>
            <ta e="T417" id="Seg_12570" s="T416">adj:pred</ta>
            <ta e="T418" id="Seg_12571" s="T417">np:O</ta>
            <ta e="T419" id="Seg_12572" s="T418">v:pred 0.3.h:S</ta>
            <ta e="T424" id="Seg_12573" s="T423">v:pred 0.3.h:S</ta>
            <ta e="T428" id="Seg_12574" s="T427">v:pred 0.3.h:S</ta>
            <ta e="T430" id="Seg_12575" s="T429">v:pred 0.3.h:S</ta>
            <ta e="T436" id="Seg_12576" s="T435">v:pred 0.3.h:S</ta>
            <ta e="T437" id="Seg_12577" s="T436">v:pred 0.3.h:S</ta>
            <ta e="T441" id="Seg_12578" s="T440">v:pred 0.3.h:S</ta>
            <ta e="T442" id="Seg_12579" s="T441">v:pred 0.3.h:S</ta>
            <ta e="T444" id="Seg_12580" s="T443">v:pred 0.3.h:S</ta>
            <ta e="T445" id="Seg_12581" s="T444">np:O</ta>
            <ta e="T446" id="Seg_12582" s="T445">v:pred 0.3.h:S</ta>
            <ta e="T451" id="Seg_12583" s="T450">v:pred</ta>
            <ta e="T455" id="Seg_12584" s="T454">v:pred 0.3:S</ta>
            <ta e="T457" id="Seg_12585" s="T456">n:pred</ta>
            <ta e="T459" id="Seg_12586" s="T458">np:O</ta>
            <ta e="T460" id="Seg_12587" s="T459">v:pred 0.3.h:S</ta>
            <ta e="T464" id="Seg_12588" s="T463">v:pred 0.3.h:S</ta>
            <ta e="T1010" id="Seg_12589" s="T466">conv:pred</ta>
            <ta e="T467" id="Seg_12590" s="T1010">v:pred 0.3.h:S</ta>
            <ta e="T471" id="Seg_12591" s="T470">v:pred 0.3.h:S</ta>
            <ta e="T472" id="Seg_12592" s="T471">v:pred 0.3.h:S</ta>
            <ta e="T474" id="Seg_12593" s="T473">pro.h:S</ta>
            <ta e="T478" id="Seg_12594" s="T477">v:pred</ta>
            <ta e="T481" id="Seg_12595" s="T480">np.h:S</ta>
            <ta e="T483" id="Seg_12596" s="T482">v:pred</ta>
            <ta e="T484" id="Seg_12597" s="T483">v:pred 0.1.h:S</ta>
            <ta e="T488" id="Seg_12598" s="T487">v:pred 0.3.h:S</ta>
            <ta e="T489" id="Seg_12599" s="T488">v:pred 0.3.h:S</ta>
            <ta e="T490" id="Seg_12600" s="T489">v:pred 0.3.h:S</ta>
            <ta e="T498" id="Seg_12601" s="T497">v:pred 0.3.h:S</ta>
            <ta e="T501" id="Seg_12602" s="T500">v:pred 0.3.h:S</ta>
            <ta e="T503" id="Seg_12603" s="T502">pro.h:S</ta>
            <ta e="T505" id="Seg_12604" s="T504">v:pred</ta>
            <ta e="T506" id="Seg_12605" s="T505">pro.h:S</ta>
            <ta e="T507" id="Seg_12606" s="T506">n:pred</ta>
            <ta e="T508" id="Seg_12607" s="T507">cop</ta>
            <ta e="T510" id="Seg_12608" s="T509">pro.h:S</ta>
            <ta e="T512" id="Seg_12609" s="T511">np:O</ta>
            <ta e="T514" id="Seg_12610" s="T513">v:pred</ta>
            <ta e="T519" id="Seg_12611" s="T518">v:pred 0.3.h:S</ta>
            <ta e="T520" id="Seg_12612" s="T519">v:pred 0.3.h:S</ta>
            <ta e="T521" id="Seg_12613" s="T520">np:O</ta>
            <ta e="T522" id="Seg_12614" s="T521">v:pred 0.3.h:S</ta>
            <ta e="T524" id="Seg_12615" s="T523">v:pred 0.3:S</ta>
            <ta e="T527" id="Seg_12616" s="T526">v:pred 0.3.h:S</ta>
            <ta e="T532" id="Seg_12617" s="T531">v:pred 0.3.h:S</ta>
            <ta e="T533" id="Seg_12618" s="T532">v:pred 0.3.h:S</ta>
            <ta e="T536" id="Seg_12619" s="T535">v:pred 0.3.h:S</ta>
            <ta e="T538" id="Seg_12620" s="T537">pro.h:S</ta>
            <ta e="T542" id="Seg_12621" s="T541">v:pred</ta>
            <ta e="T543" id="Seg_12622" s="T542">v:pred 0.3.h:S</ta>
            <ta e="T547" id="Seg_12623" s="T546">v:pred 0.3:S</ta>
            <ta e="T552" id="Seg_12624" s="T551">v:pred 0.3.h:S</ta>
            <ta e="T554" id="Seg_12625" s="T553">pro.h:S</ta>
            <ta e="T556" id="Seg_12626" s="T555">v:pred</ta>
            <ta e="T561" id="Seg_12627" s="T560">np:S</ta>
            <ta e="T562" id="Seg_12628" s="T561">v:pred</ta>
            <ta e="T565" id="Seg_12629" s="T564">v:pred 0.3.h:S</ta>
            <ta e="T566" id="Seg_12630" s="T565">pro.h:S</ta>
            <ta e="T568" id="Seg_12631" s="T567">v:pred</ta>
            <ta e="T569" id="Seg_12632" s="T568">np:O</ta>
            <ta e="T570" id="Seg_12633" s="T569">v:pred 0.1.h:S</ta>
            <ta e="T574" id="Seg_12634" s="T573">v:pred 0.3:S</ta>
            <ta e="T575" id="Seg_12635" s="T574">pro.h:S</ta>
            <ta e="T577" id="Seg_12636" s="T576">v:pred</ta>
            <ta e="T581" id="Seg_12637" s="T580">v:pred 0.1.h:S</ta>
            <ta e="T582" id="Seg_12638" s="T581">v:pred 0.1.h:S</ta>
            <ta e="T583" id="Seg_12639" s="T582">v:pred 0.1.h:S</ta>
            <ta e="T584" id="Seg_12640" s="T583">v:pred 0.1.h:S</ta>
            <ta e="T586" id="Seg_12641" s="T585">np:S</ta>
            <ta e="T587" id="Seg_12642" s="T586">ptcl.neg</ta>
            <ta e="T588" id="Seg_12643" s="T587">v:pred</ta>
            <ta e="T591" id="Seg_12644" s="T590">np:S</ta>
            <ta e="T592" id="Seg_12645" s="T591">v:pred</ta>
            <ta e="T593" id="Seg_12646" s="T592">pro.h:S</ta>
            <ta e="T595" id="Seg_12647" s="T594">v:pred</ta>
            <ta e="T598" id="Seg_12648" s="T597">v:pred 0.1.h:S</ta>
            <ta e="T599" id="Seg_12649" s="T598">v:pred 0.1.h:S</ta>
            <ta e="T602" id="Seg_12650" s="T601">v:pred</ta>
            <ta e="T604" id="Seg_12651" s="T603">np:S</ta>
            <ta e="T605" id="Seg_12652" s="T604">np:S</ta>
            <ta e="T606" id="Seg_12653" s="T605">ptcl.neg</ta>
            <ta e="T607" id="Seg_12654" s="T606">v:pred</ta>
            <ta e="T612" id="Seg_12655" s="T611">pro.h:S</ta>
            <ta e="T613" id="Seg_12656" s="T612">np:O</ta>
            <ta e="T614" id="Seg_12657" s="T613">v:pred</ta>
            <ta e="T617" id="Seg_12658" s="T616">np:S</ta>
            <ta e="T618" id="Seg_12659" s="T617">v:pred</ta>
            <ta e="T619" id="Seg_12660" s="T618">np:O</ta>
            <ta e="T621" id="Seg_12661" s="T620">v:pred</ta>
            <ta e="T623" id="Seg_12662" s="T622">pro.h:S</ta>
            <ta e="T626" id="Seg_12663" s="T625">np:O</ta>
            <ta e="T628" id="Seg_12664" s="T627">v:pred</ta>
            <ta e="T629" id="Seg_12665" s="T628">pro.h:S</ta>
            <ta e="T630" id="Seg_12666" s="T629">v:pred</ta>
            <ta e="T631" id="Seg_12667" s="T630">v:pred 0.2.h:S</ta>
            <ta e="T632" id="Seg_12668" s="T631">pro.h:S</ta>
            <ta e="T633" id="Seg_12669" s="T632">v:pred</ta>
            <ta e="T635" id="Seg_12670" s="T634">v:pred 0.3.h:S</ta>
            <ta e="T637" id="Seg_12671" s="T636">ptcl.neg</ta>
            <ta e="T638" id="Seg_12672" s="T637">v:pred</ta>
            <ta e="T639" id="Seg_12673" s="T638">np:S</ta>
            <ta e="T642" id="Seg_12674" s="T641">v:pred</ta>
            <ta e="T643" id="Seg_12675" s="T642">np.h:S</ta>
            <ta e="T644" id="Seg_12676" s="T643">pro.h:S</ta>
            <ta e="T648" id="Seg_12677" s="T647">v:pred</ta>
            <ta e="T649" id="Seg_12678" s="T648">v:pred 0.3.h:S</ta>
            <ta e="T651" id="Seg_12679" s="T650">v:pred 0.3.h:S</ta>
            <ta e="T653" id="Seg_12680" s="T652">np:S</ta>
            <ta e="T654" id="Seg_12681" s="T653">v:pred</ta>
            <ta e="T656" id="Seg_12682" s="T655">np:S</ta>
            <ta e="T657" id="Seg_12683" s="T656">v:pred</ta>
            <ta e="T658" id="Seg_12684" s="T657">pro:O</ta>
            <ta e="T659" id="Seg_12685" s="T658">v:pred 0.2.h:S</ta>
            <ta e="T660" id="Seg_12686" s="T659">v:pred 0.2.h:S</ta>
            <ta e="T662" id="Seg_12687" s="T661">v:pred 0.2.h:S</ta>
            <ta e="T667" id="Seg_12688" s="T666">np:S</ta>
            <ta e="T668" id="Seg_12689" s="T667">v:pred</ta>
            <ta e="T670" id="Seg_12690" s="T669">v:pred 0.3.h:S</ta>
            <ta e="T671" id="Seg_12691" s="T670">pro:O</ta>
            <ta e="T672" id="Seg_12692" s="T671">v:pred 0.2.h:S</ta>
            <ta e="T674" id="Seg_12693" s="T673">pro.h:S</ta>
            <ta e="T675" id="Seg_12694" s="T674">v:pred</ta>
            <ta e="T680" id="Seg_12695" s="T679">v:pred 0.2.h:S</ta>
            <ta e="T681" id="Seg_12696" s="T680">np:S</ta>
            <ta e="T682" id="Seg_12697" s="T681">adj:pred</ta>
            <ta e="T683" id="Seg_12698" s="T682">cop</ta>
            <ta e="T687" id="Seg_12699" s="T686">np.h:S</ta>
            <ta e="T691" id="Seg_12700" s="T690">v:pred</ta>
            <ta e="T693" id="Seg_12701" s="T692">np:S</ta>
            <ta e="T695" id="Seg_12702" s="T694">v:pred</ta>
            <ta e="T698" id="Seg_12703" s="T697">np:S</ta>
            <ta e="T699" id="Seg_12704" s="T698">v:pred</ta>
            <ta e="T700" id="Seg_12705" s="T699">pro.h:S</ta>
            <ta e="T701" id="Seg_12706" s="T700">np:S</ta>
            <ta e="T702" id="Seg_12707" s="T701">v:pred</ta>
            <ta e="T703" id="Seg_12708" s="T702">pro.h:S</ta>
            <ta e="T706" id="Seg_12709" s="T705">np:O</ta>
            <ta e="T707" id="Seg_12710" s="T706">v:pred</ta>
            <ta e="T711" id="Seg_12711" s="T710">np:O</ta>
            <ta e="T712" id="Seg_12712" s="T711">v:pred 0.3.h:S</ta>
            <ta e="T716" id="Seg_12713" s="T715">np:S</ta>
            <ta e="T719" id="Seg_12714" s="T718">v:pred 0.3.h:S</ta>
            <ta e="T722" id="Seg_12715" s="T721">pro:S</ta>
            <ta e="T724" id="Seg_12716" s="T723">v:pred</ta>
            <ta e="T728" id="Seg_12717" s="T727">v:pred 0.3:S</ta>
            <ta e="T731" id="Seg_12718" s="T730">np:S</ta>
            <ta e="T732" id="Seg_12719" s="T731">v:pred</ta>
            <ta e="T735" id="Seg_12720" s="T734">pro.h:S</ta>
            <ta e="T736" id="Seg_12721" s="T735">np:O</ta>
            <ta e="T739" id="Seg_12722" s="T738">v:pred</ta>
            <ta e="T741" id="Seg_12723" s="T740">v:pred 0.3:S</ta>
            <ta e="T742" id="Seg_12724" s="T741">v:pred 0.3:S</ta>
            <ta e="T745" id="Seg_12725" s="T744">ptcl.neg</ta>
            <ta e="T746" id="Seg_12726" s="T745">v:pred 0.3:S</ta>
            <ta e="T749" id="Seg_12727" s="T748">np:O</ta>
            <ta e="T751" id="Seg_12728" s="T750">v:pred 0.3.h:S</ta>
            <ta e="T754" id="Seg_12729" s="T753">v:pred 0.3.h:S</ta>
            <ta e="T757" id="Seg_12730" s="T756">np.h:S</ta>
            <ta e="T758" id="Seg_12731" s="T757">np:O</ta>
            <ta e="T759" id="Seg_12732" s="T758">v:pred</ta>
            <ta e="T770" id="Seg_12733" s="T769">np.h:S</ta>
            <ta e="T775" id="Seg_12734" s="T774">v:pred</ta>
            <ta e="T777" id="Seg_12735" s="T776">np.h:S</ta>
            <ta e="T778" id="Seg_12736" s="T777">adj:pred</ta>
            <ta e="T782" id="Seg_12737" s="T781">pro.h:O</ta>
            <ta e="T785" id="Seg_12738" s="T784">v:pred 0.3.h:S</ta>
            <ta e="T786" id="Seg_12739" s="T785">pro.h:S</ta>
            <ta e="T789" id="Seg_12740" s="T788">np:O</ta>
            <ta e="T790" id="Seg_12741" s="T789">v:pred</ta>
            <ta e="T792" id="Seg_12742" s="T791">v:pred 0.3.h:S</ta>
            <ta e="T796" id="Seg_12743" s="T795">np.h:S</ta>
            <ta e="T800" id="Seg_12744" s="T799">v:pred</ta>
            <ta e="T805" id="Seg_12745" s="T804">np.h:S</ta>
            <ta e="T806" id="Seg_12746" s="T805">pro.h:O</ta>
            <ta e="T807" id="Seg_12747" s="T806">v:pred</ta>
            <ta e="T810" id="Seg_12748" s="T809">pro.h:S</ta>
            <ta e="T811" id="Seg_12749" s="T810">np:O</ta>
            <ta e="T812" id="Seg_12750" s="T811">v:pred</ta>
            <ta e="T814" id="Seg_12751" s="T813">v:pred 0.3.h:S</ta>
            <ta e="T818" id="Seg_12752" s="T817">v:pred 0.3.h:S</ta>
            <ta e="T819" id="Seg_12753" s="T818">np:O</ta>
            <ta e="T821" id="Seg_12754" s="T820">v:pred 0.3.h:S</ta>
            <ta e="T824" id="Seg_12755" s="T823">np:S</ta>
            <ta e="T825" id="Seg_12756" s="T824">v:pred</ta>
            <ta e="T826" id="Seg_12757" s="T825">v:pred 0.2.h:S</ta>
            <ta e="T829" id="Seg_12758" s="T828">v:pred 0.2.h:S</ta>
            <ta e="T831" id="Seg_12759" s="T830">np:S</ta>
            <ta e="T833" id="Seg_12760" s="T832">ptcl.neg</ta>
            <ta e="T834" id="Seg_12761" s="T833">adj:pred</ta>
            <ta e="T836" id="Seg_12762" s="T835">np:S</ta>
            <ta e="T837" id="Seg_12763" s="T836">np:S</ta>
            <ta e="T838" id="Seg_12764" s="T837">v:pred</ta>
            <ta e="T845" id="Seg_12765" s="T844">np:S</ta>
            <ta e="T847" id="Seg_12766" s="T846">v:pred</ta>
            <ta e="T851" id="Seg_12767" s="T850">v:pred 0.2.h:S</ta>
            <ta e="T852" id="Seg_12768" s="T851">pro:S</ta>
            <ta e="T854" id="Seg_12769" s="T853">adj:pred</ta>
            <ta e="T855" id="Seg_12770" s="T854">np:S</ta>
            <ta e="T856" id="Seg_12771" s="T855">v:pred</ta>
            <ta e="T857" id="Seg_12772" s="T856">pro:S</ta>
            <ta e="T858" id="Seg_12773" s="T857">v:pred</ta>
            <ta e="T860" id="Seg_12774" s="T859">adj:pred</ta>
            <ta e="T861" id="Seg_12775" s="T860">np:S</ta>
            <ta e="T862" id="Seg_12776" s="T861">adj:pred</ta>
            <ta e="T868" id="Seg_12777" s="T867">v:pred</ta>
            <ta e="T869" id="Seg_12778" s="T868">np:S</ta>
            <ta e="T870" id="Seg_12779" s="T869">np:S</ta>
            <ta e="T871" id="Seg_12780" s="T870">v:pred</ta>
            <ta e="T873" id="Seg_12781" s="T872">adj:pred 0.3:S</ta>
            <ta e="T875" id="Seg_12782" s="T874">v:pred 0.2.h:S</ta>
            <ta e="T877" id="Seg_12783" s="T876">v:pred 0.2.h:S</ta>
            <ta e="T878" id="Seg_12784" s="T877">adj:pred</ta>
            <ta e="T879" id="Seg_12785" s="T878">pro:S</ta>
            <ta e="T883" id="Seg_12786" s="T882">v:pred 0.3.h:S</ta>
            <ta e="T885" id="Seg_12787" s="T884">np:S</ta>
            <ta e="T886" id="Seg_12788" s="T885">adj:pred</ta>
            <ta e="T887" id="Seg_12789" s="T886">np.h:S</ta>
            <ta e="T888" id="Seg_12790" s="T887">adj:pred</ta>
            <ta e="T889" id="Seg_12791" s="T888">np.h:S</ta>
            <ta e="T890" id="Seg_12792" s="T889">np:O</ta>
            <ta e="T891" id="Seg_12793" s="T890">v:pred</ta>
            <ta e="T893" id="Seg_12794" s="T892">pro:O</ta>
            <ta e="T894" id="Seg_12795" s="T893">v:pred 0.3.h:S</ta>
            <ta e="T896" id="Seg_12796" s="T895">v:pred 0.3.h:S</ta>
            <ta e="T898" id="Seg_12797" s="T897">np:O</ta>
            <ta e="T899" id="Seg_12798" s="T898">v:pred 0.3.h:S</ta>
            <ta e="T905" id="Seg_12799" s="T904">v:pred 0.2.h:S</ta>
            <ta e="T906" id="Seg_12800" s="T905">adj:pred</ta>
            <ta e="T908" id="Seg_12801" s="T907">np:S</ta>
            <ta e="T910" id="Seg_12802" s="T909">np:S</ta>
            <ta e="T912" id="Seg_12803" s="T911">np:S</ta>
            <ta e="T913" id="Seg_12804" s="T912">np:S</ta>
            <ta e="T914" id="Seg_12805" s="T913">np.h:O</ta>
            <ta e="T916" id="Seg_12806" s="T915">v:pred</ta>
            <ta e="T920" id="Seg_12807" s="T919">pro.h:S</ta>
            <ta e="T921" id="Seg_12808" s="T920">v:pred</ta>
            <ta e="T923" id="Seg_12809" s="T922">pro.h:S</ta>
            <ta e="T924" id="Seg_12810" s="T923">v:pred</ta>
            <ta e="T925" id="Seg_12811" s="T924">ptcl:pred</ta>
            <ta e="T927" id="Seg_12812" s="T926">np:O</ta>
            <ta e="T930" id="Seg_12813" s="T929">np:O</ta>
            <ta e="T931" id="Seg_12814" s="T930">ptcl:pred</ta>
            <ta e="T933" id="Seg_12815" s="T932">s:purp</ta>
            <ta e="T934" id="Seg_12816" s="T933">v:pred</ta>
            <ta e="T935" id="Seg_12817" s="T934">np:S</ta>
            <ta e="T937" id="Seg_12818" s="T936">v:pred 0.1.h:S</ta>
            <ta e="T938" id="Seg_12819" s="T937">conv:pred</ta>
            <ta e="T940" id="Seg_12820" s="T939">v:pred 0.2.h:S</ta>
            <ta e="T943" id="Seg_12821" s="T942">v:pred 0.2.h:S</ta>
            <ta e="T948" id="Seg_12822" s="T947">v:pred 0.1.h:S</ta>
            <ta e="T950" id="Seg_12823" s="T949">ptcl.neg</ta>
            <ta e="T951" id="Seg_12824" s="T950">v:pred 0.1.h:S</ta>
            <ta e="T953" id="Seg_12825" s="T952">np:S</ta>
            <ta e="T954" id="Seg_12826" s="T953">v:pred</ta>
            <ta e="T962" id="Seg_12827" s="T961">np:O</ta>
            <ta e="T963" id="Seg_12828" s="T962">v:pred 0.3.h:S</ta>
            <ta e="T966" id="Seg_12829" s="T965">v:pred 0.3.h:S</ta>
            <ta e="T973" id="Seg_12830" s="T972">pro.h:S</ta>
            <ta e="T974" id="Seg_12831" s="T973">np:O</ta>
            <ta e="T976" id="Seg_12832" s="T975">pro.h:S</ta>
            <ta e="T979" id="Seg_12833" s="T978">v:pred</ta>
            <ta e="T980" id="Seg_12834" s="T979">v:pred 0.3.h:S</ta>
            <ta e="T981" id="Seg_12835" s="T980">v:pred 0.3.h:S</ta>
            <ta e="T982" id="Seg_12836" s="T981">v:pred 0.3.h:S</ta>
            <ta e="T983" id="Seg_12837" s="T982">v:pred 0.1.h:S</ta>
            <ta e="T986" id="Seg_12838" s="T985">adj:pred</ta>
            <ta e="T987" id="Seg_12839" s="T986">cop 0.3:S</ta>
            <ta e="T991" id="Seg_12840" s="T990">ptcl:pred</ta>
            <ta e="T996" id="Seg_12841" s="T995">np:S</ta>
            <ta e="T997" id="Seg_12842" s="T996">adj:pred</ta>
            <ta e="T999" id="Seg_12843" s="T998">np:S</ta>
            <ta e="T1001" id="Seg_12844" s="T1000">v:pred</ta>
            <ta e="T1002" id="Seg_12845" s="T1001">np:S</ta>
            <ta e="T1003" id="Seg_12846" s="T1002">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T7" id="Seg_12847" s="T6">%TURK:core</ta>
            <ta e="T8" id="Seg_12848" s="T7">RUS:gram</ta>
            <ta e="T9" id="Seg_12849" s="T8">TURK:gram(INDEF)</ta>
            <ta e="T18" id="Seg_12850" s="T17">TURK:core</ta>
            <ta e="T21" id="Seg_12851" s="T20">%TURK:core</ta>
            <ta e="T24" id="Seg_12852" s="T23">RUS:gram</ta>
            <ta e="T26" id="Seg_12853" s="T25">TURK:disc</ta>
            <ta e="T32" id="Seg_12854" s="T31">TURK:gram(INDEF)</ta>
            <ta e="T35" id="Seg_12855" s="T34">TURK:gram(INDEF)</ta>
            <ta e="T36" id="Seg_12856" s="T35">%TURK:core</ta>
            <ta e="T43" id="Seg_12857" s="T42">%TURK:core</ta>
            <ta e="T46" id="Seg_12858" s="T45">RUS:gram</ta>
            <ta e="T50" id="Seg_12859" s="T49">%TURK:core</ta>
            <ta e="T51" id="Seg_12860" s="T50">RUS:gram</ta>
            <ta e="T54" id="Seg_12861" s="T53">RUS:gram</ta>
            <ta e="T60" id="Seg_12862" s="T59">TURK:core</ta>
            <ta e="T66" id="Seg_12863" s="T65">TURK:gram(INDEF)</ta>
            <ta e="T67" id="Seg_12864" s="T66">TURK:disc</ta>
            <ta e="T70" id="Seg_12865" s="T69">RUS:gram</ta>
            <ta e="T80" id="Seg_12866" s="T79">RUS:gram</ta>
            <ta e="T89" id="Seg_12867" s="T88">TURK:disc</ta>
            <ta e="T91" id="Seg_12868" s="T90">RUS:gram</ta>
            <ta e="T93" id="Seg_12869" s="T92">TURK:disc</ta>
            <ta e="T98" id="Seg_12870" s="T97">TURK:disc</ta>
            <ta e="T103" id="Seg_12871" s="T102">RUS:gram</ta>
            <ta e="T107" id="Seg_12872" s="T106">TURK:disc</ta>
            <ta e="T109" id="Seg_12873" s="T108">RUS:gram</ta>
            <ta e="T110" id="Seg_12874" s="T109">TURK:disc</ta>
            <ta e="T123" id="Seg_12875" s="T122">TAT:cult</ta>
            <ta e="T124" id="Seg_12876" s="T123">TURK:disc</ta>
            <ta e="T134" id="Seg_12877" s="T133">TURK:disc</ta>
            <ta e="T138" id="Seg_12878" s="T137">RUS:gram</ta>
            <ta e="T146" id="Seg_12879" s="T145">RUS:cult</ta>
            <ta e="T149" id="Seg_12880" s="T148">RUS:gram</ta>
            <ta e="T151" id="Seg_12881" s="T150">RUS:cult</ta>
            <ta e="T153" id="Seg_12882" s="T152">TURK:disc</ta>
            <ta e="T155" id="Seg_12883" s="T154">RUS:gram</ta>
            <ta e="T161" id="Seg_12884" s="T160">TURK:cult</ta>
            <ta e="T162" id="Seg_12885" s="T161">RUS:gram</ta>
            <ta e="T165" id="Seg_12886" s="T164">RUS:gram</ta>
            <ta e="T190" id="Seg_12887" s="T189">RUS:gram</ta>
            <ta e="T199" id="Seg_12888" s="T198">RUS:gram</ta>
            <ta e="T201" id="Seg_12889" s="T200">TURK:disc</ta>
            <ta e="T211" id="Seg_12890" s="T210">RUS:core</ta>
            <ta e="T212" id="Seg_12891" s="T211">RUS:gram</ta>
            <ta e="T235" id="Seg_12892" s="T234">RUS:gram</ta>
            <ta e="T251" id="Seg_12893" s="T250">RUS:gram</ta>
            <ta e="T253" id="Seg_12894" s="T252">TURK:cult</ta>
            <ta e="T255" id="Seg_12895" s="T254">TURK:disc</ta>
            <ta e="T258" id="Seg_12896" s="T257">RUS:cult</ta>
            <ta e="T265" id="Seg_12897" s="T264">RUS:gram</ta>
            <ta e="T269" id="Seg_12898" s="T268">RUS:gram</ta>
            <ta e="T275" id="Seg_12899" s="T274">TURK:gram(INDEF)</ta>
            <ta e="T276" id="Seg_12900" s="T275">TURK:disc</ta>
            <ta e="T278" id="Seg_12901" s="T277">RUS:gram</ta>
            <ta e="T282" id="Seg_12902" s="T281">RUS:mod</ta>
            <ta e="T284" id="Seg_12903" s="T283">RUS:mod</ta>
            <ta e="T293" id="Seg_12904" s="T292">RUS:gram</ta>
            <ta e="T300" id="Seg_12905" s="T299">TAT:cult </ta>
            <ta e="T303" id="Seg_12906" s="T302">RUS:gram</ta>
            <ta e="T309" id="Seg_12907" s="T308">RUS:mod</ta>
            <ta e="T310" id="Seg_12908" s="T309">RUS:core</ta>
            <ta e="T317" id="Seg_12909" s="T316">TURK:disc</ta>
            <ta e="T318" id="Seg_12910" s="T317">TURK:disc</ta>
            <ta e="T321" id="Seg_12911" s="T320">TURK:disc</ta>
            <ta e="T327" id="Seg_12912" s="T326">RUS:core</ta>
            <ta e="T331" id="Seg_12913" s="T330">RUS:gram</ta>
            <ta e="T337" id="Seg_12914" s="T336">TURK:disc</ta>
            <ta e="T347" id="Seg_12915" s="T346">TAT:cult</ta>
            <ta e="T349" id="Seg_12916" s="T348">TURK:disc</ta>
            <ta e="T351" id="Seg_12917" s="T350">RUS:cult</ta>
            <ta e="T354" id="Seg_12918" s="T353">RUS:gram</ta>
            <ta e="T356" id="Seg_12919" s="T355">TURK:cult</ta>
            <ta e="T357" id="Seg_12920" s="T356">RUS:gram</ta>
            <ta e="T363" id="Seg_12921" s="T362">RUS:cult</ta>
            <ta e="T365" id="Seg_12922" s="T364">TURK:core</ta>
            <ta e="T366" id="Seg_12923" s="T365">TAT:cult</ta>
            <ta e="T370" id="Seg_12924" s="T369">RUS:disc</ta>
            <ta e="T373" id="Seg_12925" s="T372">RUS:mod</ta>
            <ta e="T375" id="Seg_12926" s="T374">TAT:cult</ta>
            <ta e="T378" id="Seg_12927" s="T377">TAT:cult</ta>
            <ta e="T386" id="Seg_12928" s="T385">TURK:cult</ta>
            <ta e="T387" id="Seg_12929" s="T386">RUS:gram</ta>
            <ta e="T392" id="Seg_12930" s="T1011">TAT:cult</ta>
            <ta e="T398" id="Seg_12931" s="T397">RUS:gram</ta>
            <ta e="T400" id="Seg_12932" s="T399">TURK:cult</ta>
            <ta e="T405" id="Seg_12933" s="T404">RUS:gram</ta>
            <ta e="T410" id="Seg_12934" s="T409">RUS:gram</ta>
            <ta e="T416" id="Seg_12935" s="T415">RUS:mod</ta>
            <ta e="T420" id="Seg_12936" s="T419">RUS:gram</ta>
            <ta e="T427" id="Seg_12937" s="T426">RUS:cult</ta>
            <ta e="T433" id="Seg_12938" s="T432">RUS:gram</ta>
            <ta e="T438" id="Seg_12939" s="T437">TURK:disc</ta>
            <ta e="T443" id="Seg_12940" s="T442">TURK:disc</ta>
            <ta e="T444" id="Seg_12941" s="T443">%TURK:core</ta>
            <ta e="T459" id="Seg_12942" s="T458">TURK:cult</ta>
            <ta e="T461" id="Seg_12943" s="T460">TURK:disc</ta>
            <ta e="T462" id="Seg_12944" s="T461">RUS:core</ta>
            <ta e="T463" id="Seg_12945" s="T462">TAT:cult</ta>
            <ta e="T469" id="Seg_12946" s="T468">RUS:gram</ta>
            <ta e="T491" id="Seg_12947" s="T490">TURK:disc</ta>
            <ta e="T493" id="Seg_12948" s="T492">TURK:disc</ta>
            <ta e="T496" id="Seg_12949" s="T495">TURK:cult</ta>
            <ta e="T502" id="Seg_12950" s="T501">RUS:gram</ta>
            <ta e="T512" id="Seg_12951" s="T511">TURK:cult</ta>
            <ta e="T516" id="Seg_12952" s="T515">RUS:gram</ta>
            <ta e="T521" id="Seg_12953" s="T520">RUS:mod</ta>
            <ta e="T525" id="Seg_12954" s="T524">RUS:gram</ta>
            <ta e="T528" id="Seg_12955" s="T527">TURK:disc</ta>
            <ta e="T531" id="Seg_12956" s="T530">TURK:disc</ta>
            <ta e="T535" id="Seg_12957" s="T534">TURK:disc</ta>
            <ta e="T539" id="Seg_12958" s="T538">TURK:disc</ta>
            <ta e="T545" id="Seg_12959" s="T544">TURK:cult</ta>
            <ta e="T546" id="Seg_12960" s="T545">TURK:disc</ta>
            <ta e="T551" id="Seg_12961" s="T550">TURK:disc</ta>
            <ta e="T563" id="Seg_12962" s="T562">TURK:disc</ta>
            <ta e="T569" id="Seg_12963" s="T568">RUS:cult</ta>
            <ta e="T573" id="Seg_12964" s="T572">RUS:mod</ta>
            <ta e="T589" id="Seg_12965" s="T588">RUS:gram</ta>
            <ta e="T594" id="Seg_12966" s="T593">RUS:mod</ta>
            <ta e="T596" id="Seg_12967" s="T595">RUS:cult</ta>
            <ta e="T604" id="Seg_12968" s="T603">RUS:cult</ta>
            <ta e="T611" id="Seg_12969" s="T610">RUS:core</ta>
            <ta e="T615" id="Seg_12970" s="T614">RUS:cult</ta>
            <ta e="T619" id="Seg_12971" s="T618">RUS:cult</ta>
            <ta e="T622" id="Seg_12972" s="T621">RUS:gram</ta>
            <ta e="T625" id="Seg_12973" s="T624">TURK:gram(INDEF)</ta>
            <ta e="T627" id="Seg_12974" s="T626">TURK:disc</ta>
            <ta e="T634" id="Seg_12975" s="T633">RUS:gram</ta>
            <ta e="T643" id="Seg_12976" s="T642">RUS:cult</ta>
            <ta e="T647" id="Seg_12977" s="T646">TURK:disc</ta>
            <ta e="T656" id="Seg_12978" s="T655">RUS:core</ta>
            <ta e="T663" id="Seg_12979" s="T662">RUS:gram</ta>
            <ta e="T673" id="Seg_12980" s="T672">TURK:cult</ta>
            <ta e="T682" id="Seg_12981" s="T681">TURK:core</ta>
            <ta e="T688" id="Seg_12982" s="T687">RUS:cult</ta>
            <ta e="T689" id="Seg_12983" s="T688">TURK:disc</ta>
            <ta e="T696" id="Seg_12984" s="T695">RUS:gram</ta>
            <ta e="T706" id="Seg_12985" s="T705">TURK:cult</ta>
            <ta e="T710" id="Seg_12986" s="T709">TURK:core</ta>
            <ta e="T713" id="Seg_12987" s="T712">RUS:gram</ta>
            <ta e="T723" id="Seg_12988" s="T722">RUS:mod</ta>
            <ta e="T729" id="Seg_12989" s="T728">RUS:gram</ta>
            <ta e="T740" id="Seg_12990" s="T739">RUS:gram</ta>
            <ta e="T744" id="Seg_12991" s="T743">RUS:gram</ta>
            <ta e="T749" id="Seg_12992" s="T748">TURK:cult</ta>
            <ta e="T752" id="Seg_12993" s="T751">RUS:gram</ta>
            <ta e="T753" id="Seg_12994" s="T752">TURK:disc</ta>
            <ta e="T761" id="Seg_12995" s="T760">RUS:gram</ta>
            <ta e="T772" id="Seg_12996" s="T771">TURK:disc</ta>
            <ta e="T783" id="Seg_12997" s="T782">TURK:disc</ta>
            <ta e="T787" id="Seg_12998" s="T786">TURK:disc</ta>
            <ta e="T789" id="Seg_12999" s="T788">TURK:cult</ta>
            <ta e="T811" id="Seg_13000" s="T810">TURK:cult</ta>
            <ta e="T820" id="Seg_13001" s="T819">TURK:disc</ta>
            <ta e="T824" id="Seg_13002" s="T823">RUS:cult</ta>
            <ta e="T830" id="Seg_13003" s="T829">RUS:core</ta>
            <ta e="T832" id="Seg_13004" s="T831">TURK:disc</ta>
            <ta e="T834" id="Seg_13005" s="T833">TURK:core</ta>
            <ta e="T835" id="Seg_13006" s="T834">TURK:disc</ta>
            <ta e="T836" id="Seg_13007" s="T835">RUS:core</ta>
            <ta e="T839" id="Seg_13008" s="T838">RUS:gram</ta>
            <ta e="T840" id="Seg_13009" s="T839">TURK:disc</ta>
            <ta e="T841" id="Seg_13010" s="T840">RUS:core</ta>
            <ta e="T843" id="Seg_13011" s="T842">TURK:disc</ta>
            <ta e="T845" id="Seg_13012" s="T844">RUS:core</ta>
            <ta e="T846" id="Seg_13013" s="T845">TURK:disc</ta>
            <ta e="T848" id="Seg_13014" s="T847">RUS:gram</ta>
            <ta e="T853" id="Seg_13015" s="T852">TURK:disc</ta>
            <ta e="T854" id="Seg_13016" s="T853">TURK:core</ta>
            <ta e="T857" id="Seg_13017" s="T856">TURK:gram(INDEF)</ta>
            <ta e="T864" id="Seg_13018" s="T863">RUS:core</ta>
            <ta e="T868" id="Seg_13019" s="T867">RUS:core</ta>
            <ta e="T874" id="Seg_13020" s="T873">RUS:gram</ta>
            <ta e="T876" id="Seg_13021" s="T875">RUS:gram</ta>
            <ta e="T878" id="Seg_13022" s="T877">TURK:core</ta>
            <ta e="T880" id="Seg_13023" s="T879">RUS:gram</ta>
            <ta e="T881" id="Seg_13024" s="T880">RUS:core</ta>
            <ta e="T889" id="Seg_13025" s="T888">TURK:disc</ta>
            <ta e="T890" id="Seg_13026" s="T889">TURK:cult</ta>
            <ta e="T892" id="Seg_13027" s="T891">TURK:disc</ta>
            <ta e="T895" id="Seg_13028" s="T894">TURK:disc</ta>
            <ta e="T897" id="Seg_13029" s="T896">TURK:disc</ta>
            <ta e="T900" id="Seg_13030" s="T899">RUS:gram</ta>
            <ta e="T907" id="Seg_13031" s="T906">TURK:disc</ta>
            <ta e="T909" id="Seg_13032" s="T908">TURK:disc</ta>
            <ta e="T911" id="Seg_13033" s="T910">TURK:disc</ta>
            <ta e="T912" id="Seg_13034" s="T911">RUS:core</ta>
            <ta e="T915" id="Seg_13035" s="T914">TURK:disc</ta>
            <ta e="T918" id="Seg_13036" s="T917">TURK:disc</ta>
            <ta e="T922" id="Seg_13037" s="T921">RUS:gram</ta>
            <ta e="T925" id="Seg_13038" s="T924">RUS:mod</ta>
            <ta e="T931" id="Seg_13039" s="T930">RUS:mod</ta>
            <ta e="T932" id="Seg_13040" s="T931">RUS:gram</ta>
            <ta e="T945" id="Seg_13041" s="T944">TURK:core</ta>
            <ta e="T947" id="Seg_13042" s="T946">RUS:gram(INDEF)</ta>
            <ta e="T949" id="Seg_13043" s="T948">TURK:gram(INDEF)</ta>
            <ta e="T960" id="Seg_13044" s="T959">RUS:gram</ta>
            <ta e="T965" id="Seg_13045" s="T964">TURK:disc</ta>
            <ta e="T967" id="Seg_13046" s="T966">TURK:disc</ta>
            <ta e="T972" id="Seg_13047" s="T971">TURK:disc</ta>
            <ta e="T974" id="Seg_13048" s="T973">TURK:cult</ta>
            <ta e="T977" id="Seg_13049" s="T976">TURK:disc</ta>
            <ta e="T981" id="Seg_13050" s="T980">%TURK:core</ta>
            <ta e="T985" id="Seg_13051" s="T984">RUS:gram</ta>
            <ta e="T991" id="Seg_13052" s="T990">RUS:mod</ta>
            <ta e="T1000" id="Seg_13053" s="T999">TURK:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T16" id="Seg_13054" s="T15">RUS:int</ta>
            <ta e="T20" id="Seg_13055" s="T19">RUS:int</ta>
            <ta e="T95" id="Seg_13056" s="T94">RUS:int</ta>
            <ta e="T100" id="Seg_13057" s="T98">RUS:int</ta>
            <ta e="T106" id="Seg_13058" s="T105">RUS:int</ta>
            <ta e="T164" id="Seg_13059" s="T163">RUS:int</ta>
            <ta e="T177" id="Seg_13060" s="T176">RUS:int</ta>
            <ta e="T197" id="Seg_13061" s="T196">RUS:int</ta>
            <ta e="T226" id="Seg_13062" s="T225">RUS:int</ta>
            <ta e="T292" id="Seg_13063" s="T289">RUS:int</ta>
            <ta e="T325" id="Seg_13064" s="T323">RUS:int</ta>
            <ta e="T344" id="Seg_13065" s="T343">RUS:int</ta>
            <ta e="T393" id="Seg_13066" s="T392">RUS:int</ta>
            <ta e="T450" id="Seg_13067" s="T447">RUS:int</ta>
            <ta e="T704" id="Seg_13068" s="T703">RUS:int</ta>
            <ta e="T718" id="Seg_13069" s="T717">RUS:int</ta>
            <ta e="T737" id="Seg_13070" s="T736">RUS:int</ta>
            <ta e="T766" id="Seg_13071" s="T764">RUS:ext</ta>
            <ta e="T823" id="Seg_13072" s="T822">RUS:int</ta>
            <ta e="T863" id="Seg_13073" s="T862">RUS:int</ta>
            <ta e="T867" id="Seg_13074" s="T866">RUS:int</ta>
            <ta e="T989" id="Seg_13075" s="T987">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_13076" s="T0">Сегодня я с вами поссорилась.</ta>
            <ta e="T11" id="Seg_13077" s="T5">Вы пришли разговаривать, а ничего не слушаете.</ta>
            <ta e="T15" id="Seg_13078" s="T11">Вы не говорите.</ta>
            <ta e="T21" id="Seg_13079" s="T15">По радио сегодня была хорошая передача.</ta>
            <ta e="T34" id="Seg_13080" s="T21">Я слушала и не… я все время работаю и не никак не могу слушать.</ta>
            <ta e="T37" id="Seg_13081" s="T34">Один человек разговаривает со снегом.</ta>
            <ta e="T45" id="Seg_13082" s="T37">"Ты ложись на землю, лежи, не говори, не кричи".</ta>
            <ta e="T53" id="Seg_13083" s="T45">"А почему я не буду говорить, не буду кричать?"</ta>
            <ta e="T59" id="Seg_13084" s="T53">"Ты долго будешь жить, не умрешь.</ta>
            <ta e="T62" id="Seg_13085" s="T59">Тебе будет хорошо.</ta>
            <ta e="T64" id="Seg_13086" s="T62">Лежи, лежи".</ta>
            <ta e="T69" id="Seg_13087" s="T64">Потом кто-то услышал его.</ta>
            <ta e="T72" id="Seg_13088" s="T69">И (?) его.</ta>
            <ta e="T79" id="Seg_13089" s="T72">Потом мороз его очень заморозил.</ta>
            <ta e="T85" id="Seg_13090" s="T79">А потом тепло стало, солнце встало.</ta>
            <ta e="T90" id="Seg_13091" s="T85">Поднялось, снег испугался.</ta>
            <ta e="T100" id="Seg_13092" s="T90">И тогда таять начал.</ta>
            <ta e="T102" id="Seg_13093" s="T100">Вода течет.</ta>
            <ta e="T105" id="Seg_13094" s="T102">И снег умер.</ta>
            <ta e="T108" id="Seg_13095" s="T105">Лазарь напился.</ta>
            <ta e="T113" id="Seg_13096" s="T108">И взял в руки (?).</ta>
            <ta e="T116" id="Seg_13097" s="T113">"Сейчас, говорит, я [тебя?] ударю".</ta>
            <ta e="T123" id="Seg_13098" s="T116">Моя мать мальчика родила, в доме.</ta>
            <ta e="T126" id="Seg_13099" s="T123">Он был болен, кричал.</ta>
            <ta e="T128" id="Seg_13100" s="T126">Бабушка была.</ta>
            <ta e="T132" id="Seg_13101" s="T128">Она принесла ребенка.</ta>
            <ta e="T139" id="Seg_13102" s="T132">Она (?), перевязала и помыла [его].</ta>
            <ta e="T142" id="Seg_13103" s="T139">Потом…</ta>
            <ta e="T148" id="Seg_13104" s="T143">Потом нашла тряпки, завернула его.</ta>
            <ta e="T151" id="Seg_13105" s="T148">И положила в печь.</ta>
            <ta e="T154" id="Seg_13106" s="T151">Он лежит.</ta>
            <ta e="T158" id="Seg_13107" s="T154">А они ему дают имя.</ta>
            <ta e="T161" id="Seg_13108" s="T158">Потом отнесли [его] к священнику.</ta>
            <ta e="T164" id="Seg_13109" s="T161">И назвали Максимом.</ta>
            <ta e="T174" id="Seg_13110" s="T164">[Когда] моя мать его родила, мне было двенадцать лет.</ta>
            <ta e="T179" id="Seg_13111" s="T174">Потом этот Максим рос, рос.</ta>
            <ta e="T187" id="Seg_13112" s="T179">Двенадцать лет ему было, мать умерла.</ta>
            <ta e="T189" id="Seg_13113" s="T187">Он остался.</ta>
            <ta e="T198" id="Seg_13114" s="T189">А мать Максима убили, ее крестник убил ее.</ta>
            <ta e="T202" id="Seg_13115" s="T198">И она умерла.</ta>
            <ta e="T205" id="Seg_13116" s="T202">Ее привезли домой на лошади.</ta>
            <ta e="T209" id="Seg_13117" s="T205">Мой отец умер в лесу.</ta>
            <ta e="T218" id="Seg_13118" s="T209">Моя сестра с моим мужем на двух лошадях привезли его к нам домой.</ta>
            <ta e="T222" id="Seg_13119" s="T218">Я очень сильно плакала.</ta>
            <ta e="T226" id="Seg_13120" s="T222">Это был праздник, воскресенье.</ta>
            <ta e="T230" id="Seg_13121" s="T226">Мы выкопали [могилу] в земле.</ta>
            <ta e="T234" id="Seg_13122" s="T230">Гроб(?) сделали, крест сделали.</ta>
            <ta e="T238" id="Seg_13123" s="T234">И положили его в землю, засыпали.</ta>
            <ta e="T243" id="Seg_13124" s="T238">Потом пили воду [=водку?], ели хлеб.</ta>
            <ta e="T245" id="Seg_13125" s="T243">Люди были.</ta>
            <ta e="T246" id="Seg_13126" s="T245">Хватит.</ta>
            <ta e="T252" id="Seg_13127" s="T246">Жили двое камасинцев, женщина и мужчина.</ta>
            <ta e="T256" id="Seg_13128" s="T252">Они пили водку, дрались.</ta>
            <ta e="T263" id="Seg_13129" s="T256">Потом садились на порог, (?).</ta>
            <ta e="T271" id="Seg_13130" s="T263">Женщина и мужчина курили, курили и так засыпали.</ta>
            <ta e="T273" id="Seg_13131" s="T271">Утром они проснулись.</ta>
            <ta e="T277" id="Seg_13132" s="T273">"Я что-то болею".</ta>
            <ta e="T283" id="Seg_13133" s="T277">А [другой] говорит: "Я тоже болею".</ta>
            <ta e="T286" id="Seg_13134" s="T283">Наверное, мы подрались".</ta>
            <ta e="T292" id="Seg_13135" s="T287">Его звали (Тугойин?), Василий Никитич.</ta>
            <ta e="T297" id="Seg_13136" s="T292">А женщину звали Вера Гордеевна.</ta>
            <ta e="T302" id="Seg_13137" s="T298">Их дома были маленькие.</ta>
            <ta e="T306" id="Seg_13138" s="T302">И там стоял в (?).</ta>
            <ta e="T311" id="Seg_13139" s="T307">У него только сестра была.</ta>
            <ta e="T313" id="Seg_13140" s="T311">Ее звали Афанасия.</ta>
            <ta e="T319" id="Seg_13141" s="T313">Детей у них не было, так все умерли.</ta>
            <ta e="T325" id="Seg_13142" s="T320">Все трое умерли.</ta>
            <ta e="T335" id="Seg_13143" s="T326">Его сестра [говорит]: "Что вы дрались, а я вам не верила. [?]</ta>
            <ta e="T338" id="Seg_13144" s="T335">Вы просто так заснули".</ta>
            <ta e="T345" id="Seg_13145" s="T339">У камасинцев был праздник, Михайлов день.</ta>
            <ta e="T349" id="Seg_13146" s="T345">Они собирались в одном доме.</ta>
            <ta e="T353" id="Seg_13147" s="T349">Они накрывают на стол, (?).</ta>
            <ta e="T356" id="Seg_13148" s="T353">И пьют водку.</ta>
            <ta e="T363" id="Seg_13149" s="T356">И едят мясо, хлеб, [все] что есть на столе.</ta>
            <ta e="T367" id="Seg_13150" s="T363">Потом в другой дом пойдут.</ta>
            <ta e="T375" id="Seg_13151" s="T367">Там они так же пьют, потом [идут] еще в один дом.</ta>
            <ta e="T381" id="Seg_13152" s="T375">Потом в четвертый дом, пять (?).</ta>
            <ta e="T389" id="Seg_13153" s="T381">Пять дней они все время пьют водку, и потом хватит.</ta>
            <ta e="T401" id="Seg_13154" s="T391">В Агинском был кабак, они туда шли и покупали там водку.</ta>
            <ta e="T407" id="Seg_13155" s="T401">У кого денег много, они много берут.</ta>
            <ta e="T409" id="Seg_13156" s="T407">Он пять [бутылок] возьмет.</ta>
            <ta e="T414" id="Seg_13157" s="T409">А у кого мало, четыре возьмет.</ta>
            <ta e="T419" id="Seg_13158" s="T414">У кого еще меньше, две возьмет.</ta>
            <ta e="T424" id="Seg_13159" s="T419">И потом на праздник [ее] пили.</ta>
            <ta e="T428" id="Seg_13160" s="T424">В маленькие такие рюмки наливают.</ta>
            <ta e="T439" id="Seg_13161" s="T428">Дают одному, потом так же дают, дают (каждому?).</ta>
            <ta e="T444" id="Seg_13162" s="T439">Потом едят, едят, разговаривают.</ta>
            <ta e="T446" id="Seg_13163" s="T444">Песни поют.</ta>
            <ta e="T453" id="Seg_13164" s="T447">Александр Капитонович Ашпуров поехал в Агинское…</ta>
            <ta e="T457" id="Seg_13165" s="T453">Это был праздник, Николин день.</ta>
            <ta e="T461" id="Seg_13166" s="T457">Они выпили там водки.</ta>
            <ta e="T464" id="Seg_13167" s="T461">Ходили по всем домам.</ta>
            <ta e="T472" id="Seg_13168" s="T464">Он вечером пошел к Анже и там замерз, умер.</ta>
            <ta e="T479" id="Seg_13169" s="T473">Он вышел из дома, на улицу.</ta>
            <ta e="T483" id="Seg_13170" s="T479">Два человека к нему пришли.</ta>
            <ta e="T486" id="Seg_13171" s="T483">"Пойти с мной".</ta>
            <ta e="T489" id="Seg_13172" s="T486">Он пошел.</ta>
            <ta e="T493" id="Seg_13173" s="T489">Видит: (?).</ta>
            <ta e="T498" id="Seg_13174" s="T493">Он помолился.</ta>
            <ta e="T505" id="Seg_13175" s="T498">Он побежал оттуда к людям, (и?) вернулся домой.</ta>
            <ta e="T508" id="Seg_13176" s="T505">Сам седой стал.</ta>
            <ta e="T514" id="Seg_13177" s="T509">Они очень много водки пьют.</ta>
            <ta e="T522" id="Seg_13178" s="T514">Хоть сколько [есть], они пьют и пьют, потом ищут еще.</ta>
            <ta e="T528" id="Seg_13179" s="T522">Если где есть, они туда бегут.</ta>
            <ta e="T532" id="Seg_13180" s="T529">Там поют [песни].</ta>
            <ta e="T533" id="Seg_13181" s="T532">Танцуют.</ta>
            <ta e="T536" id="Seg_13182" s="T533">Потом дерутся.</ta>
            <ta e="T547" id="Seg_13183" s="T537">Кто много пьет, потом (горит?) от водки.</ta>
            <ta e="T552" id="Seg_13184" s="T547">Ему в рот мочатся.</ta>
            <ta e="T556" id="Seg_13185" s="T552">Потом он встанет.</ta>
            <ta e="T565" id="Seg_13186" s="T557">[Один раз] у меня ухо болело, стреляло.</ta>
            <ta e="T574" id="Seg_13187" s="T565">Я тогда нашла тряпку, помочилась на нее, [моча] аж текла с нее.</ta>
            <ta e="T577" id="Seg_13188" s="T574">Я приложила ее к уху.</ta>
            <ta e="T581" id="Seg_13189" s="T577">Потом завязала.</ta>
            <ta e="T583" id="Seg_13190" s="T581">Я легла, заснула.</ta>
            <ta e="T588" id="Seg_13191" s="T583">Встала — ухо не болит.</ta>
            <ta e="T598" id="Seg_13192" s="T588">А [если] у меня голова болит, я тоже помочусь на тряпку и привяжу к голове.</ta>
            <ta e="T607" id="Seg_13193" s="T598">Я лягу, тряпка высохнет, голова не болит.</ta>
            <ta e="T615" id="Seg_13194" s="T608">Один раз мы хлеб молотили машиной.</ta>
            <ta e="T621" id="Seg_13195" s="T615">Там был нож, снопы резать.</ta>
            <ta e="T635" id="Seg_13196" s="T621">Один человек как-то руку порезал, я говорю: "Помочись", он(а) помочился/помочилась и завязал(а).</ta>
            <ta e="T639" id="Seg_13197" s="T635">Потом рука не болела.</ta>
            <ta e="T643" id="Seg_13198" s="T640">У нас не было докторов.</ta>
            <ta e="T648" id="Seg_13199" s="T643">Мы травами лечились.</ta>
            <ta e="T651" id="Seg_13200" s="T648">Их варят, потом пьют.</ta>
            <ta e="T655" id="Seg_13201" s="T651">Есть одна трава, сильно пахнет.</ta>
            <ta e="T662" id="Seg_13202" s="T655">Если желудок болит, ее варишь, пьешь, потом ешь.</ta>
            <ta e="T670" id="Seg_13203" s="T662">Есть одна трава, ее называют (?).</ta>
            <ta e="T683" id="Seg_13204" s="T670">Ее кладешь в водку, она стоит двенадцать дней, потом ее пьешь, и сердцу становится хорошо.</ta>
            <ta e="T691" id="Seg_13205" s="T684">Наши камасинцы на Масленицу [устраивают] скачки на лошадях.</ta>
            <ta e="T699" id="Seg_13206" s="T691">Чья лошадь придет первая, а чья отстанет.</ta>
            <ta e="T707" id="Seg_13207" s="T699">Тот, чья лошадь отстанет, покупает четверть водки.</ta>
            <ta e="T712" id="Seg_13208" s="T708">Потом другую лошадь приведут.</ta>
            <ta e="T720" id="Seg_13209" s="T712">А та лошадь очень бегунец, так говорят.</ta>
            <ta e="T724" id="Seg_13210" s="T720">Она тоже (отстанет?).</ta>
            <ta e="T728" id="Seg_13211" s="T724">Тут побежит.</ta>
            <ta e="T732" id="Seg_13212" s="T728">А та лошадь (отстанет?).</ta>
            <ta e="T739" id="Seg_13213" s="T733">Потом он вытирает этой лошади пот (?)</ta>
            <ta e="T746" id="Seg_13214" s="T739">И ходит, ходит потихоньку, чтобы она не стояла.</ta>
            <ta e="T751" id="Seg_13215" s="T747">Потом они пьют много водки.</ta>
            <ta e="T754" id="Seg_13216" s="T751">И танцуют.</ta>
            <ta e="T760" id="Seg_13217" s="T755">Потом мужчины берут палки в руки.</ta>
            <ta e="T764" id="Seg_13218" s="T760">И потом…</ta>
            <ta e="T766" id="Seg_13219" s="T764">Останови маленько.</ta>
            <ta e="T768" id="Seg_13220" s="T767">(…)</ta>
            <ta e="T775" id="Seg_13221" s="T768">Потом они (?) друг другу.</ta>
            <ta e="T780" id="Seg_13222" s="T775">Один сильный, а другой…</ta>
            <ta e="T785" id="Seg_13223" s="T780">Он ставит его на ноги.</ta>
            <ta e="T790" id="Seg_13224" s="T785">А тот потом ставит водку.</ta>
            <ta e="T792" id="Seg_13225" s="T790">Потом они пьют.</ta>
            <ta e="T802" id="Seg_13226" s="T793">Потом оба камасинца хватаются друг за друга.</ta>
            <ta e="T808" id="Seg_13227" s="T802">Потом тот, кто сильнее, бросит другого на землю.</ta>
            <ta e="T813" id="Seg_13228" s="T808">Тогда он ставит ему водку.</ta>
            <ta e="T814" id="Seg_13229" s="T813">Они пьют.</ta>
            <ta e="T821" id="Seg_13230" s="T815">Потом на землю (бросит?), руку (сломает?).</ta>
            <ta e="T825" id="Seg_13231" s="T822">Вот стоят ворота.</ta>
            <ta e="T829" id="Seg_13232" s="T825">Иди, по какой дороге ты пойдешь?</ta>
            <ta e="T834" id="Seg_13233" s="T829">Правая дорога плохая.</ta>
            <ta e="T836" id="Seg_13234" s="T834">Болота.</ta>
            <ta e="T838" id="Seg_13235" s="T836">Деревья стоят.</ta>
            <ta e="T841" id="Seg_13236" s="T838">И грозы.</ta>
            <ta e="T843" id="Seg_13237" s="T841">Ветер.</ta>
            <ta e="T847" id="Seg_13238" s="T843">Железные колючки цепляются.</ta>
            <ta e="T858" id="Seg_13239" s="T847">А туда ты далеко пойдешь, там хорошо, деревьев нет, ничего нет.</ta>
            <ta e="T860" id="Seg_13240" s="T858">Очень красиво.</ta>
            <ta e="T866" id="Seg_13241" s="T860">Трава красивая, всякие цветы: белые, красные.</ta>
            <ta e="T869" id="Seg_13242" s="T866">Виноград, яблоки растут.</ta>
            <ta e="T879" id="Seg_13243" s="T869">Солнце ходит, очень тепло, хоть ложись да спи, хорошо там.</ta>
            <ta e="T891" id="Seg_13244" s="T879">А налево идти, дорога большая, народу много, все водку пьют.</ta>
            <ta e="T895" id="Seg_13245" s="T891">Всякую [еду] едят.</ta>
            <ta e="T899" id="Seg_13246" s="T895">Танцуют, песни поют.</ta>
            <ta e="T905" id="Seg_13247" s="T899">Там ты далеко уйдешь.</ta>
            <ta e="T912" id="Seg_13248" s="T905">Там огонь, камни, грозы.</ta>
            <ta e="T916" id="Seg_13249" s="T912">Огонь сжигает людей.</ta>
            <ta e="T918" id="Seg_13250" s="T916">Сейчас(?)</ta>
            <ta e="T924" id="Seg_13251" s="T919">Вы идите, а я (?).</ta>
            <ta e="T931" id="Seg_13252" s="T924">Мне надо хлеб поставить, испечь хлеб надо.</ta>
            <ta e="T935" id="Seg_13253" s="T931">А то нет хлеба, чтобы есть.</ta>
            <ta e="T938" id="Seg_13254" s="T935">Вам дам, поедите.</ta>
            <ta e="T946" id="Seg_13255" s="T939">Не выходи замуж, живи, одной жить хорошо.</ta>
            <ta e="T951" id="Seg_13256" s="T946">Я куда-нибудь пойду, никого не буду спрашивать.</ta>
            <ta e="T955" id="Seg_13257" s="T952">Вот вода течет.</ta>
            <ta e="T968" id="Seg_13258" s="T955">Большой Ильбинь, малый Ильбинь, а где воду (берут?), эти места называют (?).</ta>
            <ta e="T982" id="Seg_13259" s="T969">В один год мы (?) рожь, они все сидят, сидят, разговаривают, смеются.</ta>
            <ta e="T992" id="Seg_13260" s="T982">Пойдем (?), а то холодно станет, все равно нам надо (?).</ta>
            <ta e="T997" id="Seg_13261" s="T992">Тебе всегда много надо.</ta>
            <ta e="T1001" id="Seg_13262" s="T997">Потом руки мерзнут.</ta>
            <ta e="T1008" id="Seg_13263" s="T1001">Снег пойдет, мы снова деревья…</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_13264" s="T0">Today I quarelled with you.</ta>
            <ta e="T11" id="Seg_13265" s="T5">You came to speak, but you don't [want to] hear anything.</ta>
            <ta e="T15" id="Seg_13266" s="T11">You aren't speaking.</ta>
            <ta e="T21" id="Seg_13267" s="T15">There was a good radio broadcast yesterday.</ta>
            <ta e="T34" id="Seg_13268" s="T21">I was listening and wasn't… I work all the time and can't listen, at all.</ta>
            <ta e="T37" id="Seg_13269" s="T34">One person speaks with the snow.</ta>
            <ta e="T45" id="Seg_13270" s="T37">"You lie yourself on the ground, lie, don't speak, don't shout."</ta>
            <ta e="T53" id="Seg_13271" s="T45">"Why wouldn't I speak, wouldn't shout?"</ta>
            <ta e="T59" id="Seg_13272" s="T53">"You will live long time, you won't die.</ta>
            <ta e="T62" id="Seg_13273" s="T59">You will be well.</ta>
            <ta e="T64" id="Seg_13274" s="T62">Lie, lie."</ta>
            <ta e="T69" id="Seg_13275" s="T64">Then somebody heard him.</ta>
            <ta e="T72" id="Seg_13276" s="T69">And (?) him.</ta>
            <ta e="T79" id="Seg_13277" s="T72">Then the frost freezed him very much.</ta>
            <ta e="T85" id="Seg_13278" s="T79">And then it got warm, the sun rose.</ta>
            <ta e="T90" id="Seg_13279" s="T85">It rose, the snow was scared.</ta>
            <ta e="T100" id="Seg_13280" s="T90">And then it began to melt.</ta>
            <ta e="T102" id="Seg_13281" s="T100">Water flows.</ta>
            <ta e="T105" id="Seg_13282" s="T102">And the snow died.</ta>
            <ta e="T108" id="Seg_13283" s="T105">Lazar got drunk.</ta>
            <ta e="T113" id="Seg_13284" s="T108">And took (?) in his hand.</ta>
            <ta e="T116" id="Seg_13285" s="T113">"Now I'll beat [you?]", he said.</ta>
            <ta e="T123" id="Seg_13286" s="T116">My mother gave birth to a boy, in the house.</ta>
            <ta e="T126" id="Seg_13287" s="T123">He was ill, he screamed.</ta>
            <ta e="T128" id="Seg_13288" s="T126">There was an old woman.</ta>
            <ta e="T132" id="Seg_13289" s="T128">She brought the child.</ta>
            <ta e="T139" id="Seg_13290" s="T132">She brought [him], tied [the cords?] and washed [him].</ta>
            <ta e="T142" id="Seg_13291" s="T139">Then…</ta>
            <ta e="T148" id="Seg_13292" s="T143">Then she found a rag, wrapped him up.</ta>
            <ta e="T151" id="Seg_13293" s="T148">And put [him] into the oven.</ta>
            <ta e="T154" id="Seg_13294" s="T151">He's lying.</ta>
            <ta e="T158" id="Seg_13295" s="T154">And they are giving him a name.</ta>
            <ta e="T161" id="Seg_13296" s="T158">Then they brought [him] to the priest.</ta>
            <ta e="T164" id="Seg_13297" s="T161">And named him Maksim.</ta>
            <ta e="T174" id="Seg_13298" s="T164">[When] my mother had him, I was twelve years old.</ta>
            <ta e="T179" id="Seg_13299" s="T174">Then this Maksim was growing.</ta>
            <ta e="T187" id="Seg_13300" s="T179">[When] he was twelve years old, his mother died.</ta>
            <ta e="T189" id="Seg_13301" s="T187">He left [alone].</ta>
            <ta e="T198" id="Seg_13302" s="T189">And Maksim's mother was killed, her godson killed her.</ta>
            <ta e="T202" id="Seg_13303" s="T198">And she died.</ta>
            <ta e="T205" id="Seg_13304" s="T202">They brought her home on a horse.</ta>
            <ta e="T209" id="Seg_13305" s="T205">My father died in the forest.</ta>
            <ta e="T218" id="Seg_13306" s="T209">Then my sister and my husband brought him with two horses to our house.</ta>
            <ta e="T222" id="Seg_13307" s="T218">I cried a lot.</ta>
            <ta e="T226" id="Seg_13308" s="T222">It was a holiday, Sunday.</ta>
            <ta e="T230" id="Seg_13309" s="T226">Then we dug the ground.</ta>
            <ta e="T234" id="Seg_13310" s="T230">We made a (coffin?), made the cross.</ta>
            <ta e="T238" id="Seg_13311" s="T234">And put [him] in the ground [and] covered [the grave].</ta>
            <ta e="T243" id="Seg_13312" s="T238">Then we drank water [= vodka?], ate bread.</ta>
            <ta e="T245" id="Seg_13313" s="T243">People was [there].</ta>
            <ta e="T246" id="Seg_13314" s="T245">That's all.</ta>
            <ta e="T252" id="Seg_13315" s="T246">There lived two Kamassians, a woman and a man.</ta>
            <ta e="T256" id="Seg_13316" s="T252">They drank vodka, they fought.</ta>
            <ta e="T263" id="Seg_13317" s="T256">Then they sat down on the threshold, (?).</ta>
            <ta e="T271" id="Seg_13318" s="T263">The woman and the man were smoking (?) and so fell asleep.</ta>
            <ta e="T273" id="Seg_13319" s="T271">In the morning they got up.</ta>
            <ta e="T277" id="Seg_13320" s="T273">"I don't feel well, for some reason."</ta>
            <ta e="T283" id="Seg_13321" s="T277">[The other one] says: "I don't feel well, too.</ta>
            <ta e="T286" id="Seg_13322" s="T283">We should have fought."</ta>
            <ta e="T292" id="Seg_13323" s="T287">His name was (Tugoyin?), Vasili Nikitich.</ta>
            <ta e="T297" id="Seg_13324" s="T292">And the woman's name [was] Vera Gordeevna.</ta>
            <ta e="T302" id="Seg_13325" s="T298">Their houses were small.</ta>
            <ta e="T306" id="Seg_13326" s="T302">It stayed there in (?).</ta>
            <ta e="T311" id="Seg_13327" s="T307">He had only a sister.</ta>
            <ta e="T313" id="Seg_13328" s="T311">Her name was Afanassiya.</ta>
            <ta e="T319" id="Seg_13329" s="T313">[They] had no children, they had all died just so.</ta>
            <ta e="T325" id="Seg_13330" s="T320">All the three had died.</ta>
            <ta e="T335" id="Seg_13331" s="T326">His sister [says]: "Why did you fight, and I didn't believe you. [?]</ta>
            <ta e="T338" id="Seg_13332" s="T335">You went to sleep just so."</ta>
            <ta e="T345" id="Seg_13333" s="T339">The Kamassians had a holiday, St. Michael's day.</ta>
            <ta e="T349" id="Seg_13334" s="T345">They gather in a house.</ta>
            <ta e="T353" id="Seg_13335" s="T349">They lay the table, (?).</ta>
            <ta e="T356" id="Seg_13336" s="T353">And are drinking vodka.</ta>
            <ta e="T363" id="Seg_13337" s="T356">And are eating meat, bread, [and] what there was on the table.</ta>
            <ta e="T367" id="Seg_13338" s="T363">Then they'll go to another house.</ta>
            <ta e="T375" id="Seg_13339" s="T367">There they drink in the same fashion, then [they go] to the next house.</ta>
            <ta e="T381" id="Seg_13340" s="T375">Then to the fourth house, five (?).</ta>
            <ta e="T389" id="Seg_13341" s="T381">During five days they are always drinking vodka, then it'll be enough.</ta>
            <ta e="T401" id="Seg_13342" s="T391">In Aginskoe, there was a tavern, they went there and bought there vodka.</ta>
            <ta e="T407" id="Seg_13343" s="T401">Who has much money, they take much.</ta>
            <ta e="T409" id="Seg_13344" s="T407">He'll take five [bottles].</ta>
            <ta e="T414" id="Seg_13345" s="T409">Who has little money, takes four.</ta>
            <ta e="T419" id="Seg_13346" s="T414">Who has even less, takes two.</ta>
            <ta e="T424" id="Seg_13347" s="T419">And then they drank [it] during the holiday.</ta>
            <ta e="T428" id="Seg_13348" s="T424">They poured [it] in such small goblets.</ta>
            <ta e="T439" id="Seg_13349" s="T428">[They] give [it] to one, then again in the same manner [they] give to each (one?).</ta>
            <ta e="T444" id="Seg_13350" s="T439">Then they're eating, sitting, speaking.</ta>
            <ta e="T446" id="Seg_13351" s="T444">They are singing songs.</ta>
            <ta e="T453" id="Seg_13352" s="T447">Aleksandr Kapitonovich Ashpurov went to Aginskoye…</ta>
            <ta e="T457" id="Seg_13353" s="T453">It was a holiday, St. Nicholas' day.</ta>
            <ta e="T461" id="Seg_13354" s="T457">They drank vodka there.</ta>
            <ta e="T464" id="Seg_13355" s="T461">They went to every house.</ta>
            <ta e="T472" id="Seg_13356" s="T464">That evening he went to Anzha, there he freezed to death.</ta>
            <ta e="T479" id="Seg_13357" s="T473">He went out from his house, outdoor.</ta>
            <ta e="T483" id="Seg_13358" s="T479">Two men came to him.</ta>
            <ta e="T486" id="Seg_13359" s="T483">"Go with me."</ta>
            <ta e="T489" id="Seg_13360" s="T486">Then he went.</ta>
            <ta e="T493" id="Seg_13361" s="T489">He sees: (?).</ta>
            <ta e="T498" id="Seg_13362" s="T493">He prayed to God.</ta>
            <ta e="T505" id="Seg_13363" s="T498">He ran from there to the people, (and?) returned home.</ta>
            <ta e="T508" id="Seg_13364" s="T505">He became white [= седой].</ta>
            <ta e="T514" id="Seg_13365" s="T509">They drink very much vodka.</ta>
            <ta e="T522" id="Seg_13366" s="T514">[If there is] even a bit, they always drink and drink, and [then] look for more.</ta>
            <ta e="T528" id="Seg_13367" s="T522">They run where it is.</ta>
            <ta e="T532" id="Seg_13368" s="T529">Then they sing [songs].</ta>
            <ta e="T533" id="Seg_13369" s="T532">They dance.</ta>
            <ta e="T536" id="Seg_13370" s="T533">Then they fight.</ta>
            <ta e="T547" id="Seg_13371" s="T537">Who drinks a lot, then (burns?) because of the the alcohol.</ta>
            <ta e="T552" id="Seg_13372" s="T547">They urinate into his mouth.</ta>
            <ta e="T556" id="Seg_13373" s="T552">Then he'll stand up.</ta>
            <ta e="T565" id="Seg_13374" s="T557">[Once] my ear ached, I felt pains in it.</ta>
            <ta e="T574" id="Seg_13375" s="T565">Then I found a rag, urinated [on it], [the urine] even flowed from it.</ta>
            <ta e="T577" id="Seg_13376" s="T574">I applied it to my ear.</ta>
            <ta e="T581" id="Seg_13377" s="T577">Then I tied it round.</ta>
            <ta e="T583" id="Seg_13378" s="T581">I lay down, fell asleep.</ta>
            <ta e="T588" id="Seg_13379" s="T583">[When] I was up, my ear didn't ache.</ta>
            <ta e="T598" id="Seg_13380" s="T588">And [if] my head aches, I'll urinate on rags and bind them to my head.</ta>
            <ta e="T607" id="Seg_13381" s="T598">I'll lay down, this rag will dry out, and my head won't ache.</ta>
            <ta e="T615" id="Seg_13382" s="T608">Once we (threshed?) grain with machines.</ta>
            <ta e="T621" id="Seg_13383" s="T615">There was a knife, to cut sheaves.</ta>
            <ta e="T635" id="Seg_13384" s="T621">Somehow s/he cut his/her arm; I say: "Urinate!", s/he urinated and tied [his/her arm] up.</ta>
            <ta e="T639" id="Seg_13385" s="T635">Then his/her arm didn't hurt [anymore].</ta>
            <ta e="T643" id="Seg_13386" s="T640">We had no doctors.</ta>
            <ta e="T648" id="Seg_13387" s="T643">We cured our illnesses with herbs.</ta>
            <ta e="T651" id="Seg_13388" s="T648">[You] boil [it], then drink [it].</ta>
            <ta e="T655" id="Seg_13389" s="T651">There is a grass, with a strong smell.</ta>
            <ta e="T662" id="Seg_13390" s="T655">If your stomach aches, you boil it, drink it, then you eat.</ta>
            <ta e="T670" id="Seg_13391" s="T662">There is a herb, its name is (?).</ta>
            <ta e="T683" id="Seg_13392" s="T670">You put it into the alcohol, it stays for twelve days, they you drink it, and your heart will be well.</ta>
            <ta e="T691" id="Seg_13393" s="T684">Our Kamassians make a horse-race on the Carnival.</ta>
            <ta e="T699" id="Seg_13394" s="T691">Whose horse comes first, and whose horse remains behind.</ta>
            <ta e="T707" id="Seg_13395" s="T699">The one whose horse has left behind buys a quart of vodka.</ta>
            <ta e="T712" id="Seg_13396" s="T708">Then they bring another horse.</ta>
            <ta e="T720" id="Seg_13397" s="T712">And that horse is a good runner, they say so.</ta>
            <ta e="T724" id="Seg_13398" s="T720">Then it will remain (behind?), too.</ta>
            <ta e="T728" id="Seg_13399" s="T724">It'll run here.</ta>
            <ta e="T732" id="Seg_13400" s="T728">And that horse will (stay?).</ta>
            <ta e="T739" id="Seg_13401" s="T733">Then he wipes this horse's sweat. [?]</ta>
            <ta e="T746" id="Seg_13402" s="T739">And walks slowly, so that it doesn't stand.</ta>
            <ta e="T751" id="Seg_13403" s="T747">Then they drink much vodka.</ta>
            <ta e="T754" id="Seg_13404" s="T751">And dance.</ta>
            <ta e="T760" id="Seg_13405" s="T755">Then the men take sticks.</ta>
            <ta e="T764" id="Seg_13406" s="T760">And then…</ta>
            <ta e="T766" id="Seg_13407" s="T764">Stop a little bit.</ta>
            <ta e="T768" id="Seg_13408" s="T767">(…)</ta>
            <ta e="T775" id="Seg_13409" s="T768">Then they (?) to one another.</ta>
            <ta e="T780" id="Seg_13410" s="T775">Then one is powerful, and another…</ta>
            <ta e="T785" id="Seg_13411" s="T780">He gets him on his feet.</ta>
            <ta e="T790" id="Seg_13412" s="T785">Then he serves vodka.</ta>
            <ta e="T792" id="Seg_13413" s="T790">Then they drink.</ta>
            <ta e="T802" id="Seg_13414" s="T793">Then both Kamassians grab one another.</ta>
            <ta e="T808" id="Seg_13415" s="T802">Then the more powerful one puts the other to the ground.</ta>
            <ta e="T813" id="Seg_13416" s="T808">Then he buys him vodka.</ta>
            <ta e="T814" id="Seg_13417" s="T813">They drink.</ta>
            <ta e="T821" id="Seg_13418" s="T815">Then he'll break his legs and arms against the earth. [?]</ta>
            <ta e="T825" id="Seg_13419" s="T822">Here is a gate.</ta>
            <ta e="T829" id="Seg_13420" s="T825">Go, which road will you take?</ta>
            <ta e="T834" id="Seg_13421" s="T829">The right road is bad.</ta>
            <ta e="T836" id="Seg_13422" s="T834">There are swamps.</ta>
            <ta e="T838" id="Seg_13423" s="T836">There are trees.</ta>
            <ta e="T841" id="Seg_13424" s="T838">And thunderstorms.</ta>
            <ta e="T843" id="Seg_13425" s="T841">The wind.</ta>
            <ta e="T847" id="Seg_13426" s="T843">And thorns stick.</ta>
            <ta e="T858" id="Seg_13427" s="T847">And in this direction you'll go far, here it is good, there are no trees, nothing.</ta>
            <ta e="T860" id="Seg_13428" s="T858">It's very beautiful.</ta>
            <ta e="T866" id="Seg_13429" s="T860">The grass is beautiful, all sorts of flowers: white, red.</ta>
            <ta e="T869" id="Seg_13430" s="T866">Vine and apples are growing.</ta>
            <ta e="T879" id="Seg_13431" s="T869">The sun moves [across the sky], it's very warm, you may even lie down and sleep, it's well there.</ta>
            <ta e="T891" id="Seg_13432" s="T879">If you go to the left, the road is very large, there are many people, they all drink vodka. </ta>
            <ta e="T895" id="Seg_13433" s="T891">They eat all sorts of things.</ta>
            <ta e="T899" id="Seg_13434" s="T895">They dance, sing songs.</ta>
            <ta e="T905" id="Seg_13435" s="T899">There you will go far away.</ta>
            <ta e="T912" id="Seg_13436" s="T905">There are fire, stones, thunderstorms.</ta>
            <ta e="T916" id="Seg_13437" s="T912">The fire is burning people.</ta>
            <ta e="T918" id="Seg_13438" s="T916">Now(?)</ta>
            <ta e="T924" id="Seg_13439" s="T919">You all go, and I'll (?).</ta>
            <ta e="T931" id="Seg_13440" s="T924">I have to make dough, to bake bread.</ta>
            <ta e="T935" id="Seg_13441" s="T931">Because there's no bread to eat.</ta>
            <ta e="T938" id="Seg_13442" s="T935">I'll give [it] to you, you'll eat.</ta>
            <ta e="T946" id="Seg_13443" s="T939">Don't marry, live it's good to live alone.</ta>
            <ta e="T951" id="Seg_13444" s="T946">I'll go somewhere, I won't ask anyone.</ta>
            <ta e="T955" id="Seg_13445" s="T952">There is river flowing.</ta>
            <ta e="T968" id="Seg_13446" s="T955">Big Ilbin, Little Ilbin, a where people take (water?), these [places] are called (?).</ta>
            <ta e="T982" id="Seg_13447" s="T969">One year we (?) rye, they are always sitting, sitting, speaking, laughing.</ta>
            <ta e="T992" id="Seg_13448" s="T982">Let's go to (?), otherwise it will be cold, we have to (?) anyway.</ta>
            <ta e="T997" id="Seg_13449" s="T992">You always want much.</ta>
            <ta e="T1001" id="Seg_13450" s="T997">Then the hands freeze.</ta>
            <ta e="T1008" id="Seg_13451" s="T1001">It will snow, we'll again (?) the trees …</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_13452" s="T0">Heute habe ich mich mit euch gestritten.</ta>
            <ta e="T11" id="Seg_13453" s="T5">Ihr seid gekommen, um zu sprechen, aber irh [wollt] nichts hören.</ta>
            <ta e="T15" id="Seg_13454" s="T11">Ihr spricht nicht.</ta>
            <ta e="T21" id="Seg_13455" s="T15">Gestern gab es eine gute Radiosendung.</ta>
            <ta e="T34" id="Seg_13456" s="T21">Ich habe zugehört und war nicht… ich arbeite die ganze Zeit und kann überhaupt nicht zuhören.</ta>
            <ta e="T37" id="Seg_13457" s="T34">Ein Mensch spricht mit dem Schnee.</ta>
            <ta e="T45" id="Seg_13458" s="T37">"Du liegst selber auf dem Boden, liege, sprich nicht, schrei nicht."</ta>
            <ta e="T53" id="Seg_13459" s="T45">"Warum soll ich nicht sprechen, nicht schreien?"</ta>
            <ta e="T59" id="Seg_13460" s="T53">"Du wirst lange leben, du wirst nicht sterben.</ta>
            <ta e="T62" id="Seg_13461" s="T59">Dir wird es gut gehen.</ta>
            <ta e="T64" id="Seg_13462" s="T62">Liege, liege."</ta>
            <ta e="T69" id="Seg_13463" s="T64">Dann hörte ihn jemand.</ta>
            <ta e="T72" id="Seg_13464" s="T69">Und (?) ihn.</ta>
            <ta e="T79" id="Seg_13465" s="T72">Dann ließ ihn der Frost sehr frieren.</ta>
            <ta e="T85" id="Seg_13466" s="T79">Und dann wurde es warm, die Sonne ging auf.</ta>
            <ta e="T90" id="Seg_13467" s="T85">Sie ging auf, der Schnee hatte Angst.</ta>
            <ta e="T100" id="Seg_13468" s="T90">Und dann begann er zu schmelzen.</ta>
            <ta e="T102" id="Seg_13469" s="T100">Wasser fließt.</ta>
            <ta e="T105" id="Seg_13470" s="T102">Und der Schnee starb.</ta>
            <ta e="T108" id="Seg_13471" s="T105">Lazar wurde betrunken.</ta>
            <ta e="T113" id="Seg_13472" s="T108">Und nahm (?) in seine Hand.</ta>
            <ta e="T116" id="Seg_13473" s="T113">"Jetzt schlage ich [dich?]", sagte er.</ta>
            <ta e="T123" id="Seg_13474" s="T116">Meine Mutter gebar einen Jungen, im Haus.</ta>
            <ta e="T126" id="Seg_13475" s="T123">Er war krank, er schrie.</ta>
            <ta e="T128" id="Seg_13476" s="T126">Dort war eine alte Frau.</ta>
            <ta e="T132" id="Seg_13477" s="T128">Sie brachte das Kind.</ta>
            <ta e="T139" id="Seg_13478" s="T132">Sie brachte [ihn], band [die Schnüre?] fest und wusch [ihn].</ta>
            <ta e="T142" id="Seg_13479" s="T139">Dann…</ta>
            <ta e="T148" id="Seg_13480" s="T143">Dann find sie einen Stofffetzen, wickelte ihn ein.</ta>
            <ta e="T151" id="Seg_13481" s="T148">Und steckte [ihn] in den Ofen.</ta>
            <ta e="T154" id="Seg_13482" s="T151">Er liegt. </ta>
            <ta e="T158" id="Seg_13483" s="T154">Und sie geben ihm einen Namen.</ta>
            <ta e="T161" id="Seg_13484" s="T158">Dann brachten sie [ihn] zum Priester.</ta>
            <ta e="T164" id="Seg_13485" s="T161">Und nannten ihn Maxim.</ta>
            <ta e="T174" id="Seg_13486" s="T164">[Als] meine Mutter ihn bekam, war ich zwölf Jahre alt.</ta>
            <ta e="T179" id="Seg_13487" s="T174">Dann wuchs dieser Maxim.</ta>
            <ta e="T187" id="Seg_13488" s="T179">[Als] er zwölf Jahre alt war, starb seine Mutter.</ta>
            <ta e="T189" id="Seg_13489" s="T187">Er blieb [alleine].</ta>
            <ta e="T198" id="Seg_13490" s="T189">Und Maxims Mutter wurde umgebracht, ihr Patensohn brachte sie um.</ta>
            <ta e="T202" id="Seg_13491" s="T198">Und sie starb.</ta>
            <ta e="T205" id="Seg_13492" s="T202">Sie brachten sie auf einem Pferd nach Hause.</ta>
            <ta e="T209" id="Seg_13493" s="T205">Mein Vater starb im Wald.</ta>
            <ta e="T218" id="Seg_13494" s="T209">Dann brachten meine Schwester und mein Mann ihn mit zwei Pferden zu unserem Haus.</ta>
            <ta e="T222" id="Seg_13495" s="T218">Ich weinte viel.</ta>
            <ta e="T226" id="Seg_13496" s="T222">Es war ein Feiertag, Sonntag.</ta>
            <ta e="T230" id="Seg_13497" s="T226">Dann gruben wir die Erde [aus].</ta>
            <ta e="T234" id="Seg_13498" s="T230">Wir machten einen (Sarg?), machten das Kreuz.</ta>
            <ta e="T238" id="Seg_13499" s="T234">Und legten [ihn] in die Erde [und] bedeckten [das Grab].</ta>
            <ta e="T243" id="Seg_13500" s="T238">Dann tranken wir Wasser [= Wodka?], aßen Brot.</ta>
            <ta e="T245" id="Seg_13501" s="T243">Es waren Leute [dort].</ta>
            <ta e="T246" id="Seg_13502" s="T245">Das ist alles.</ta>
            <ta e="T252" id="Seg_13503" s="T246">Es lebten zwei Kamassen, eine Frau und ein Mann.</ta>
            <ta e="T256" id="Seg_13504" s="T252">Sie tranken Wodka, sie kämpften.</ta>
            <ta e="T263" id="Seg_13505" s="T256">Dann setzten sie sich auf die Schwelle.</ta>
            <ta e="T271" id="Seg_13506" s="T263">Die Frau und der Mann rauchten (?) und schliefen ein.</ta>
            <ta e="T273" id="Seg_13507" s="T271">Am Morgen standen sie auf.</ta>
            <ta e="T277" id="Seg_13508" s="T273">"Ich fühle mich irgendwie nicht gut."</ta>
            <ta e="T283" id="Seg_13509" s="T277">[Der/die andere] sagt: "Ich fühle mich auch nicht gut.</ta>
            <ta e="T286" id="Seg_13510" s="T283">Wir hätten kämpfen sollen."</ta>
            <ta e="T292" id="Seg_13511" s="T287">Sein Name war Tugojin, Vasilij Nikitisch.</ta>
            <ta e="T297" id="Seg_13512" s="T292">Und der Name der Frau [war] Vera Gordejevna.</ta>
            <ta e="T302" id="Seg_13513" s="T298">Ihre Häuser waren klein.</ta>
            <ta e="T306" id="Seg_13514" s="T302">Es stand dort in (?).</ta>
            <ta e="T311" id="Seg_13515" s="T307">Er hatte nur eine Schwester.</ta>
            <ta e="T313" id="Seg_13516" s="T311">Ihr Name war Afanasija.</ta>
            <ta e="T319" id="Seg_13517" s="T313">[Sie] hatten keine Kinder, sie starben einfach alle.</ta>
            <ta e="T325" id="Seg_13518" s="T320">Alle drei starben.</ta>
            <ta e="T335" id="Seg_13519" s="T326">Seine Schwester [sagt]: "Warum hast du gekämpft, und ich habe dir nicht geglaubt. [?]</ta>
            <ta e="T338" id="Seg_13520" s="T335">Sie sind einfach schlafen gegangen."</ta>
            <ta e="T345" id="Seg_13521" s="T339">Die Kamassen hatten einen Feiertag, den Tag des St. Michael.</ta>
            <ta e="T349" id="Seg_13522" s="T345">Sie versammeln sich in einem Haus.</ta>
            <ta e="T353" id="Seg_13523" s="T349">Sie decken den Tisch, (?).</ta>
            <ta e="T356" id="Seg_13524" s="T353">Und trinken Wodka.</ta>
            <ta e="T363" id="Seg_13525" s="T356">Und essen Fleisch, Brot, [und] was da auf dem Tisch war.</ta>
            <ta e="T367" id="Seg_13526" s="T363">Dann gehen sie zu einem anderen Haus.</ta>
            <ta e="T375" id="Seg_13527" s="T367">Dort trinken sie genauso, dann [gehen sie] zum nächsten Haus.</ta>
            <ta e="T381" id="Seg_13528" s="T375">Dann zum vierten Haus, fünf (?).</ta>
            <ta e="T389" id="Seg_13529" s="T381">Fünf Tage lang trinken sie immer Wodka, dann ist es genug.</ta>
            <ta e="T401" id="Seg_13530" s="T391">In Aginskoje gab es eine Kneipe, sie gingen dorthin und kauften dort Wodka.</ta>
            <ta e="T407" id="Seg_13531" s="T401">Wer viel Geld hat, kauft viel.</ta>
            <ta e="T409" id="Seg_13532" s="T407">Er kauft fünf [Flaschen].</ta>
            <ta e="T414" id="Seg_13533" s="T409">Wer wenig Geld hat, kauft vier.</ta>
            <ta e="T419" id="Seg_13534" s="T414">Wer noch weniger hat, kauft zwei.</ta>
            <ta e="T424" id="Seg_13535" s="T419">Und dann tranken sie [ihn] am Feiertag.</ta>
            <ta e="T428" id="Seg_13536" s="T424">Sie gossen [ihn] in solche kleinen Schnapsgläser.</ta>
            <ta e="T439" id="Seg_13537" s="T428">[Sie] geben [es] einem, dann wieder geben (jemandem?) ebenso.</ta>
            <ta e="T444" id="Seg_13538" s="T439">Dann essen sie, sitzen, sprechen.</ta>
            <ta e="T446" id="Seg_13539" s="T444">Sie singen Lieder.</ta>
            <ta e="T453" id="Seg_13540" s="T447">Alexandr Kapitonowitsch Aschpurow ging nach Aginskoje…</ta>
            <ta e="T457" id="Seg_13541" s="T453">Es war ein Feiertag, der Tag des St. Nikolaus.</ta>
            <ta e="T461" id="Seg_13542" s="T457">Sie tranken dort Wodka.</ta>
            <ta e="T464" id="Seg_13543" s="T461">Sie gingen zu jedem Haus.</ta>
            <ta e="T472" id="Seg_13544" s="T464">An jenem Abend ging er zum Anzha[-Fluss], dort erfror er.</ta>
            <ta e="T479" id="Seg_13545" s="T473">Er ging aus seinem Haus hinaus, draußen.</ta>
            <ta e="T483" id="Seg_13546" s="T479">Zwei Männer kamen zu ihm.</ta>
            <ta e="T486" id="Seg_13547" s="T483">"Geh mit mir."</ta>
            <ta e="T489" id="Seg_13548" s="T486">Dann ging er.</ta>
            <ta e="T493" id="Seg_13549" s="T489">Er sieht: (?).</ta>
            <ta e="T498" id="Seg_13550" s="T493">Er betete zu Gott.</ta>
            <ta e="T505" id="Seg_13551" s="T498">Er rannte von dort zu den Leuten (und?) kehrte nach Hause zurück.</ta>
            <ta e="T508" id="Seg_13552" s="T505">Er wurde weiß[haarig].</ta>
            <ta e="T514" id="Seg_13553" s="T509">Sie trinken sehr viel Wodka.</ta>
            <ta e="T522" id="Seg_13554" s="T514">[Wenn es] nur ein Bisschen [gibt], trinken sie immer und trinken, und [dann] suchen sie nach mehr.</ta>
            <ta e="T528" id="Seg_13555" s="T522">Sie laufen dorthin, wo es welchen gibt.</ta>
            <ta e="T532" id="Seg_13556" s="T529">Dann singen sie [Lieder].</ta>
            <ta e="T533" id="Seg_13557" s="T532">Sie tanzen.</ta>
            <ta e="T536" id="Seg_13558" s="T533">Dann kämpfen sie.</ta>
            <ta e="T547" id="Seg_13559" s="T537">Wer viel trinkt, dann (brennt?) er wegen des Alkohols.</ta>
            <ta e="T552" id="Seg_13560" s="T547">Sie urinieren in seinen Mund.</ta>
            <ta e="T556" id="Seg_13561" s="T552">Dann steht er auf.</ta>
            <ta e="T565" id="Seg_13562" s="T557">[Einmal] tat mein Ohr weh, ich spürte Schmerzen darin.</ta>
            <ta e="T574" id="Seg_13563" s="T565">Dann fand ich einen Stofffetzen, urinierte [darauf], [der Urin] floss sogar davon runter.</ta>
            <ta e="T577" id="Seg_13564" s="T574">Ich hielt ihn an mein Ohr.</ta>
            <ta e="T581" id="Seg_13565" s="T577">Dann band ich ihn fest.</ta>
            <ta e="T583" id="Seg_13566" s="T581">Ich legte mich hin, schlief ein.</ta>
            <ta e="T588" id="Seg_13567" s="T583">[Als] ich aufwachte, tat mein Ohr nicht weh.</ta>
            <ta e="T598" id="Seg_13568" s="T588">Und [wenn] mein Kopf wehtut, uriniere ich auf Stofffetzen und binde sie mir an den Kopf.</ta>
            <ta e="T607" id="Seg_13569" s="T598">Ich lege mich hin, der Stofffetzen trocknet und mein Kopf tut nicht mehr weh.</ta>
            <ta e="T615" id="Seg_13570" s="T608">Einmal droschen wir Korn mit Maschinen.</ta>
            <ta e="T621" id="Seg_13571" s="T615">Dort war ein Messer, um Garben zu schneiden.</ta>
            <ta e="T635" id="Seg_13572" s="T621">Irgendwie schnitt er/sie seinen/ihren Arm, ich sage: "Uriniere", er/sie urinierte und verband [den Arm].</ta>
            <ta e="T639" id="Seg_13573" s="T635">Dann tat sein/ihr Arm nicht [mehr] weh.</ta>
            <ta e="T643" id="Seg_13574" s="T640">Wir hatten keine Ärzte.</ta>
            <ta e="T648" id="Seg_13575" s="T643">Wir heilten unsere Krankheiten mit Kräutern.</ta>
            <ta e="T651" id="Seg_13576" s="T648">[Du] kochst [sie], dann trinkst du [sie].</ta>
            <ta e="T655" id="Seg_13577" s="T651">Es gibt ein Gras mit starkem Geruch.</ta>
            <ta e="T662" id="Seg_13578" s="T655">Wenn dein Bauch wehtut, dann kochst du es, trinkst es, dann isst du.</ta>
            <ta e="T670" id="Seg_13579" s="T662">Es gibt ein Kraut, sein Name ist (?).</ta>
            <ta e="T683" id="Seg_13580" s="T670">Du tust es in den Alkohol, es bleibt dort für zwölf Tage, dann trinkst du es und deinem Herz geht es gut.</ta>
            <ta e="T691" id="Seg_13581" s="T684">Unsere Kamassen [organisieren] zu Fasching Pferderennen.</ta>
            <ta e="T699" id="Seg_13582" s="T691">Wessen Pferd kommt zuerst und wessen Pferd bleibt dahinter.</ta>
            <ta e="T707" id="Seg_13583" s="T699">Derjenige, dessen Pferd dahinter geblieben ist, kauft einen Viertel Wodka.</ta>
            <ta e="T712" id="Seg_13584" s="T708">Dann bringt man ein anderes Pferd.</ta>
            <ta e="T720" id="Seg_13585" s="T712">Und dieses Pferd ist ein guter Läufer, sagen sie.</ta>
            <ta e="T724" id="Seg_13586" s="T720">Dann bleibt es auch (zurück?).</ta>
            <ta e="T728" id="Seg_13587" s="T724">Es rennt hierher.</ta>
            <ta e="T732" id="Seg_13588" s="T728">Und jenes Pferd wird (bleiben?).</ta>
            <ta e="T739" id="Seg_13589" s="T733">Dann wischt er den Schweiß von diesem Pferd. [?]</ta>
            <ta e="T746" id="Seg_13590" s="T739">Und geht lamgsam, sodass es nicht stehen kann.</ta>
            <ta e="T751" id="Seg_13591" s="T747">Dann trinkt man viel Wodka.</ta>
            <ta e="T754" id="Seg_13592" s="T751">Und tanzt.</ta>
            <ta e="T760" id="Seg_13593" s="T755">Dann nehmen die Männer Stöcker.</ta>
            <ta e="T764" id="Seg_13594" s="T760">Und dann.</ta>
            <ta e="T766" id="Seg_13595" s="T764">Warte ein Bisschen.</ta>
            <ta e="T768" id="Seg_13596" s="T767">(…)</ta>
            <ta e="T775" id="Seg_13597" s="T768">Dann sie (?) zu einander.</ta>
            <ta e="T780" id="Seg_13598" s="T775">Dann ist einer stark und ein anderer…</ta>
            <ta e="T785" id="Seg_13599" s="T780">Er stellt ihn auf seine Füße.</ta>
            <ta e="T790" id="Seg_13600" s="T785">Dann serviert er Wodka.</ta>
            <ta e="T792" id="Seg_13601" s="T790">Dann trinken sie.</ta>
            <ta e="T802" id="Seg_13602" s="T793">Dann nehmen die beiden Kamassen einander.</ta>
            <ta e="T808" id="Seg_13603" s="T802">Dann wirft der Stärkere den anderen auf den Boden.</ta>
            <ta e="T813" id="Seg_13604" s="T808">Dann kauft er ihm Wodka.</ta>
            <ta e="T814" id="Seg_13605" s="T813">Sie trinken.</ta>
            <ta e="T821" id="Seg_13606" s="T815">Dann bricht er seine Beine und Arme gegen die Erde. [?]</ta>
            <ta e="T825" id="Seg_13607" s="T822">Hier ist ein Tor.</ta>
            <ta e="T829" id="Seg_13608" s="T825">Geh, welchen Weg nimmst du?</ta>
            <ta e="T834" id="Seg_13609" s="T829">Der rechte Weg ist schlecht.</ta>
            <ta e="T836" id="Seg_13610" s="T834">Dort sind Sümpfe.</ta>
            <ta e="T838" id="Seg_13611" s="T836">Dort sind Bäume.</ta>
            <ta e="T841" id="Seg_13612" s="T838">Und Gewitter.</ta>
            <ta e="T843" id="Seg_13613" s="T841">Der Wind.</ta>
            <ta e="T847" id="Seg_13614" s="T843">Und Dornen stechen.</ta>
            <ta e="T858" id="Seg_13615" s="T847">Aber in diese Richtung gehst du weit, hier ist es gut, dort sind keine Bäume, nichts.</ta>
            <ta e="T860" id="Seg_13616" s="T858">Es ist sehr schön.</ta>
            <ta e="T866" id="Seg_13617" s="T860">Das Gras ist schön, alle möglichen Blumen: weiße, rote.</ta>
            <ta e="T869" id="Seg_13618" s="T866">Wein und Äpfel wachsen.</ta>
            <ta e="T879" id="Seg_13619" s="T869">Die Sonne bewegt sich [über den Himmel], es ist sehr warm, du kannst dich sogar hinlegen und schlafen, es ist gut dort.</ta>
            <ta e="T891" id="Seg_13620" s="T879">Wenn du nach links gehst, die Straße ist sehr groß, dort sind viele Leute, sie trinken alle Wodka.</ta>
            <ta e="T895" id="Seg_13621" s="T891">Sie essen alles Mögliche.</ta>
            <ta e="T899" id="Seg_13622" s="T895">Sie tanzen, singen Lieder.</ta>
            <ta e="T905" id="Seg_13623" s="T899">Dort wirst du weit gehen.</ta>
            <ta e="T912" id="Seg_13624" s="T905">Dort sind Feuer, Steine, Gewitter.</ta>
            <ta e="T916" id="Seg_13625" s="T912">Das Feuer verbrennt Leute.</ta>
            <ta e="T918" id="Seg_13626" s="T916">Jetzt(?)</ta>
            <ta e="T924" id="Seg_13627" s="T919">Ihr geht alle und ich (?).</ta>
            <ta e="T931" id="Seg_13628" s="T924">Ich muss Teig machen, um Brot zu backen.</ta>
            <ta e="T935" id="Seg_13629" s="T931">Weil kein Brot zum essen da ist.</ta>
            <ta e="T938" id="Seg_13630" s="T935">Ich gebe [es] dir, du isst es.</ta>
            <ta e="T946" id="Seg_13631" s="T939">Heirate nicht, lebe, es ist gut alleine zu leben.</ta>
            <ta e="T951" id="Seg_13632" s="T946">Ich gehe irgendwo hin, ich frage niemanden.</ta>
            <ta e="T955" id="Seg_13633" s="T952">Dort fließt ein Fluss.</ta>
            <ta e="T968" id="Seg_13634" s="T955">Großer Ilbin, Kleiner Ilbin, und wo man Wasser (nimmt?), werden diese [Plätze] genannt (?).</ta>
            <ta e="T982" id="Seg_13635" s="T969">In einem Jahr wir (?) Roggen, sie sitzen immer, sitzen, sprechen, lachen.</ta>
            <ta e="T992" id="Seg_13636" s="T982">Lass uns nach (?) gehen, sonst wird es kalt, wir müssen sowieso (?).</ta>
            <ta e="T997" id="Seg_13637" s="T992">Du willst immer viel.</ta>
            <ta e="T1001" id="Seg_13638" s="T997">Dann frieren die Hände.</ta>
            <ta e="T1008" id="Seg_13639" s="T1001">Es wird schneien, wir (?) wieder die Bäume…</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T11" id="Seg_13640" s="T5">[GVY:] nʼilgölaʔpəʔj- …leʔ</ta>
            <ta e="T69" id="Seg_13641" s="T64">[GVY:] gĭttə?</ta>
            <ta e="T85" id="Seg_13642" s="T79">[GVY:] koja.</ta>
            <ta e="T198" id="Seg_13643" s="T189">[GVY:] Maksimən?</ta>
            <ta e="T263" id="Seg_13644" s="T256">[GVY:] the second part of the sentence is unclear. Ibi may also mean 'took'.</ta>
            <ta e="T271" id="Seg_13645" s="T263">[GVY:] The second form should be also nʼeʔdolaʔpi?</ta>
            <ta e="T313" id="Seg_13646" s="T311">[GVY:] The plural of "Afanassiya" is unclear.</ta>
            <ta e="T319" id="Seg_13647" s="T313">[GVY:] It is not clear, who is referred to now.</ta>
            <ta e="T335" id="Seg_13648" s="T326">[GVY:] This interpretation is very tentative.</ta>
            <ta e="T381" id="Seg_13649" s="T375">[GVY:] sɨləj = целый ("целых пять дней")?</ta>
            <ta e="T407" id="Seg_13650" s="T401">[GVY:] The Russian colloquial construction "если у кого много денег, возьмут много".</ta>
            <ta e="T409" id="Seg_13651" s="T407">[GVY:] igəj = iləj</ta>
            <ta e="T424" id="Seg_13652" s="T419">[GVY:] bĭtleʔbəʔi?</ta>
            <ta e="T464" id="Seg_13653" s="T461">[GVY:] Or "[Then] everyone went home."</ta>
            <ta e="T472" id="Seg_13654" s="T464">[GVY:] Anzha is a river, a left tributary of Kan, which flows near Aginskoye.</ta>
            <ta e="T479" id="Seg_13655" s="T473">[GVY:] Or "He came home" (then the last word is unclear).</ta>
            <ta e="T483" id="Seg_13656" s="T479">[GVY:] koza</ta>
            <ta e="T493" id="Seg_13657" s="T489">[GVY:] Kulʼa as ku-lia 'die-PRES' "He sees that he's dying'? </ta>
            <ta e="T565" id="Seg_13658" s="T557">[GVY:] Or ku mĭnzemnuʔpi from mĭnzer- 'boil'?</ta>
            <ta e="T635" id="Seg_13659" s="T621">[GVY:] or "it" [=the machine] cut my/his/her arm…</ta>
            <ta e="T648" id="Seg_13660" s="T643">[GVY:] nozi = noʔsi?</ta>
            <ta e="T691" id="Seg_13661" s="T684">[GVY:] maslenka 'Carnival' (Масленица)?</ta>
            <ta e="T699" id="Seg_13662" s="T691">[GVY:] or "horse's calf": büzəj 'calf', büžü 'soon'.</ta>
            <ta e="T724" id="Seg_13663" s="T720">[GVY:] dĭm = dĭ?</ta>
            <ta e="T775" id="Seg_13664" s="T768">[GVY:] This whole fragment is about some contest of strengh, but the details are not clear.</ta>
            <ta e="T847" id="Seg_13665" s="T843">[GVY:] Donner (42a) has mɯndəlʼem 'spend a day' and mɯʔl'em 'stick'.</ta>
            <ta e="T905" id="Seg_13666" s="T899">[GVY:] You won't go far (see below)?</ta>
            <ta e="T968" id="Seg_13667" s="T955">[GVY:] Or 'where one takes water'.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T1011" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T1010" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T979" />
            <conversion-tli id="T980" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T985" />
            <conversion-tli id="T986" />
            <conversion-tli id="T987" />
            <conversion-tli id="T988" />
            <conversion-tli id="T989" />
            <conversion-tli id="T990" />
            <conversion-tli id="T991" />
            <conversion-tli id="T992" />
            <conversion-tli id="T993" />
            <conversion-tli id="T994" />
            <conversion-tli id="T995" />
            <conversion-tli id="T996" />
            <conversion-tli id="T997" />
            <conversion-tli id="T998" />
            <conversion-tli id="T999" />
            <conversion-tli id="T1000" />
            <conversion-tli id="T1001" />
            <conversion-tli id="T1002" />
            <conversion-tli id="T1003" />
            <conversion-tli id="T1004" />
            <conversion-tli id="T1005" />
            <conversion-tli id="T1006" />
            <conversion-tli id="T1007" />
            <conversion-tli id="T1008" />
            <conversion-tli id="T1009" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
