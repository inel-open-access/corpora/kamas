<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID3E10E012-BE08-6B53-5930-2E00DB8C7485">
   <head>
      <meta-information>
         <project-name>Kamas</project-name>
         <transcription-name>SAE_196X_SU0232</transcription-name>
         <referenced-file url="SAE_196X_SU0232.wav" />
         <referenced-file url="SAE_196X_SU0232.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\SAE_196X_SU0232\SAE_196X_SU0232.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1190</ud-information>
            <ud-information attribute-name="# HIAT:w">748</ud-information>
            <ud-information attribute-name="# e">760</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">15</ud-information>
            <ud-information attribute-name="# HIAT:u">217</ud-information>
            <ud-information attribute-name="# sc">432</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SAE">
            <abbreviation>SAE</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="5.02" type="appl" />
         <tli id="T2" time="6.2025" type="appl" />
         <tli id="T3" time="7.385" type="appl" />
         <tli id="T4" time="8.567499999999999" type="appl" />
         <tli id="T5" time="9.75" type="appl" />
         <tli id="T6" time="9.86" type="appl" />
         <tli id="T7" time="10.969999999999999" type="appl" />
         <tli id="T8" time="12.08" type="appl" />
         <tli id="T9" time="13.073241635351577" />
         <tli id="T10" time="14.19875" type="appl" />
         <tli id="T11" time="15.0875" type="appl" />
         <tli id="T12" time="15.97625" type="appl" />
         <tli id="T13" time="16.865000000000002" type="appl" />
         <tli id="T14" time="17.75375" type="appl" />
         <tli id="T15" time="18.642500000000002" type="appl" />
         <tli id="T16" time="19.53125" type="appl" />
         <tli id="T17" time="20.42" type="appl" />
         <tli id="T18" time="20.73" type="appl" />
         <tli id="T19" time="21.948" type="appl" />
         <tli id="T20" time="23.166" type="appl" />
         <tli id="T21" time="24.384" type="appl" />
         <tli id="T22" time="25.602" type="appl" />
         <tli id="T23" time="26.82" type="appl" />
         <tli id="T24" time="27.76" type="appl" />
         <tli id="T25" time="28.61" type="appl" />
         <tli id="T26" time="30.74" type="appl" />
         <tli id="T27" time="31.529999999999998" type="appl" />
         <tli id="T28" time="32.32" type="appl" />
         <tli id="T29" time="33.11" type="appl" />
         <tli id="T30" time="33.9" type="appl" />
         <tli id="T31" time="34.66" type="appl" />
         <tli id="T32" time="35.62" type="appl" />
         <tli id="T33" time="36.58" type="appl" />
         <tli id="T34" time="37.15" type="appl" />
         <tli id="T35" time="37.79666666666667" type="appl" />
         <tli id="T36" time="38.443333333333335" type="appl" />
         <tli id="T37" time="39.09" type="appl" />
         <tli id="T38" time="40.09" type="appl" />
         <tli id="T39" time="41.55166666666667" type="appl" />
         <tli id="T40" time="43.013333333333335" type="appl" />
         <tli id="T41" time="44.475" type="appl" />
         <tli id="T42" time="45.93666666666667" type="appl" />
         <tli id="T43" time="47.39833333333333" type="appl" />
         <tli id="T44" time="48.86" type="appl" />
         <tli id="T45" time="49.12632208613247" />
         <tli id="T46" time="50.125" type="appl" />
         <tli id="T47" time="51.0" type="appl" />
         <tli id="T48" time="51.38" type="appl" />
         <tli id="T49" time="51.935" type="appl" />
         <tli id="T50" time="52.49" type="appl" />
         <tli id="T51" time="53.26" type="appl" />
         <tli id="T52" time="54.6225" type="appl" />
         <tli id="T53" time="55.985" type="appl" />
         <tli id="T54" time="57.3475" type="appl" />
         <tli id="T55" time="58.71" type="appl" />
         <tli id="T56" time="59.13291856479779" />
         <tli id="T57" time="59.696666666666665" type="appl" />
         <tli id="T58" time="60.193333333333335" type="appl" />
         <tli id="T59" time="60.69" type="appl" />
         <tli id="T60" time="62.24" type="appl" />
         <tli id="T61" time="63.92333333333334" type="appl" />
         <tli id="T62" time="65.60666666666667" type="appl" />
         <tli id="T63" time="67.29" type="appl" />
         <tli id="T64" time="68.95" type="appl" />
         <tli id="T65" time="70.05125000000001" type="appl" />
         <tli id="T66" time="71.1525" type="appl" />
         <tli id="T67" time="72.25375" type="appl" />
         <tli id="T68" time="73.355" type="appl" />
         <tli id="T69" time="74.45625000000001" type="appl" />
         <tli id="T70" time="75.5575" type="appl" />
         <tli id="T71" time="76.65875" type="appl" />
         <tli id="T72" time="77.76" type="appl" />
         <tli id="T73" time="79.21" type="appl" />
         <tli id="T74" time="81.26666666666667" type="appl" />
         <tli id="T75" time="83.32333333333332" type="appl" />
         <tli id="T76" time="85.38" type="appl" />
         <tli id="T77" time="87.51271950395723" />
         <tli id="T78" time="88.16" type="appl" />
         <tli id="T79" time="88.67" type="appl" />
         <tli id="T80" time="90.7860298777245" />
         <tli id="T81" time="92.18766666666667" type="appl" />
         <tli id="T82" time="93.75533333333333" type="appl" />
         <tli id="T83" time="95.323" type="appl" />
         <tli id="T84" time="95.82599452653929" />
         <tli id="T85" time="97.4415" type="appl" />
         <tli id="T86" time="98.98" type="appl" />
         <tli id="T87" time="100.27575" type="appl" />
         <tli id="T88" time="101.5715" type="appl" />
         <tli id="T89" time="102.86725" type="appl" />
         <tli id="T90" time="104.163" type="appl" />
         <tli id="T91" time="105.42" type="appl" />
         <tli id="T92" time="106.3648" type="appl" />
         <tli id="T93" time="107.3096" type="appl" />
         <tli id="T94" time="108.2544" type="appl" />
         <tli id="T95" time="109.1992" type="appl" />
         <tli id="T96" time="110.144" type="appl" />
         <tli id="T97" time="110.31" type="appl" />
         <tli id="T98" time="112.28" type="appl" />
         <tli id="T99" time="113.85" type="appl" />
         <tli id="T100" time="115.285" type="appl" />
         <tli id="T101" time="116.72" type="appl" />
         <tli id="T102" time="118.15499999999999" type="appl" />
         <tli id="T103" time="119.58999999999999" type="appl" />
         <tli id="T104" time="121.02499999999999" type="appl" />
         <tli id="T105" time="122.46" type="appl" />
         <tli id="T106" time="123.895" type="appl" />
         <tli id="T107" time="125.32999999999998" type="appl" />
         <tli id="T108" time="126.76499999999999" type="appl" />
         <tli id="T109" time="128.2" type="appl" />
         <tli id="T110" time="128.94" type="appl" />
         <tli id="T111" time="129.99666666666667" type="appl" />
         <tli id="T112" time="131.05333333333334" type="appl" />
         <tli id="T113" time="132.11" type="appl" />
         <tli id="T114" time="134.07" type="appl" />
         <tli id="T115" time="134.748" type="appl" />
         <tli id="T116" time="135.426" type="appl" />
         <tli id="T117" time="136.104" type="appl" />
         <tli id="T118" time="136.782" type="appl" />
         <tli id="T119" time="137.46" type="appl" />
         <tli id="T120" time="143.56" type="appl" />
         <tli id="T121" time="144.47" type="appl" />
         <tli id="T122" time="145.0" type="appl" />
         <tli id="T123" time="146.01749999999998" type="appl" />
         <tli id="T124" time="147.035" type="appl" />
         <tli id="T125" time="148.0525" type="appl" />
         <tli id="T126" time="149.07" type="appl" />
         <tli id="T127" time="149.7" type="appl" />
         <tli id="T128" time="150.725" type="appl" />
         <tli id="T129" time="151.75" type="appl" />
         <tli id="T130" time="152.77499999999998" type="appl" />
         <tli id="T131" time="153.79999999999998" type="appl" />
         <tli id="T132" time="154.825" type="appl" />
         <tli id="T133" time="155.85" type="appl" />
         <tli id="T134" time="157.4" type="appl" />
         <tli id="T135" time="158.48950000000002" type="appl" />
         <tli id="T136" time="159.579" type="appl" />
         <tli id="T137" time="160.6685" type="appl" />
         <tli id="T138" time="161.758" type="appl" />
         <tli id="T139" time="162.84750000000003" type="appl" />
         <tli id="T140" time="163.937" type="appl" />
         <tli id="T141" time="164.80551069226226" />
         <tli id="T142" time="165.545" type="appl" />
         <tli id="T143" time="166.57999999999998" type="appl" />
         <tli id="T144" time="167.615" type="appl" />
         <tli id="T145" time="168.65" type="appl" />
         <tli id="T146" time="169.1" type="appl" />
         <tli id="T147" time="169.655" type="appl" />
         <tli id="T148" time="170.21" type="appl" />
         <tli id="T149" time="170.72" type="appl" />
         <tli id="T150" time="171.61" type="appl" />
         <tli id="T151" time="172.62" type="appl" />
         <tli id="T152" time="173.91" type="appl" />
         <tli id="T153" time="175.2" type="appl" />
         <tli id="T154" time="177.16" type="appl" />
         <tli id="T155" time="178.39" type="appl" />
         <tli id="T156" time="179.62" type="appl" />
         <tli id="T157" time="180.85" type="appl" />
         <tli id="T158" time="181.69" type="appl" />
         <tli id="T159" time="182.35399999999998" type="appl" />
         <tli id="T160" time="183.018" type="appl" />
         <tli id="T161" time="183.682" type="appl" />
         <tli id="T162" time="184.346" type="appl" />
         <tli id="T163" time="185.01" type="appl" />
         <tli id="T164" time="185.14" type="appl" />
         <tli id="T165" time="185.82" type="appl" />
         <tli id="T166" time="186.11" type="appl" />
         <tli id="T167" time="186.59666666666666" type="appl" />
         <tli id="T168" time="187.08333333333334" type="appl" />
         <tli id="T169" time="187.57" type="appl" />
         <tli id="T170" time="187.85" type="appl" />
         <tli id="T171" time="188.77333333333334" type="appl" />
         <tli id="T172" time="189.69666666666666" type="appl" />
         <tli id="T173" time="190.62" type="appl" />
         <tli id="T174" time="191.60532271373773" />
         <tli id="T175" time="192.218651745075" />
         <tli id="T176" time="192.75" type="appl" />
         <tli id="T177" time="193.41197670822027" />
         <tli id="T178" time="194.63863477089475" />
         <tli id="T179" time="195.78" type="appl" />
         <tli id="T180" time="196.67" type="appl" />
         <tli id="T181" time="197.56" type="appl" />
         <tli id="T182" time="197.94" type="appl" />
         <tli id="T183" time="198.76" type="appl" />
         <tli id="T184" time="199.58" type="appl" />
         <tli id="T185" time="200.4" type="appl" />
         <tli id="T186" time="200.89" type="appl" />
         <tli id="T187" time="201.65" type="appl" />
         <tli id="T188" time="203.58523868453156" />
         <tli id="T189" time="204.20749999999998" type="appl" />
         <tli id="T190" time="204.755" type="appl" />
         <tli id="T191" time="205.3025" type="appl" />
         <tli id="T192" time="205.85" type="appl" />
         <tli id="T193" time="206.48" type="appl" />
         <tli id="T194" time="207.21187991330837" />
         <tli id="T195" time="207.96520796266825" />
         <tli id="T196" time="208.52499999999998" type="appl" />
         <tli id="T197" time="209.04" type="appl" />
         <tli id="T198" time="209.39" type="appl" />
         <tli id="T199" time="210.0125" type="appl" />
         <tli id="T200" time="210.635" type="appl" />
         <tli id="T201" time="211.2575" type="appl" />
         <tli id="T202" time="211.92666455132542" />
         <tli id="T203" time="213.13000246507147" />
         <tli id="T204" time="213.542" type="appl" />
         <tli id="T205" time="213.874" type="appl" />
         <tli id="T206" time="214.20600000000002" type="appl" />
         <tli id="T207" time="214.538" type="appl" />
         <tli id="T208" time="214.87" type="appl" />
         <tli id="T209" time="214.96" type="appl" />
         <tli id="T210" time="215.7675" type="appl" />
         <tli id="T211" time="216.575" type="appl" />
         <tli id="T212" time="217.3825" type="appl" />
         <tli id="T213" time="218.25000561498237" />
         <tli id="T214" time="219.5699963563386" />
         <tli id="T215" time="220.335" type="appl" />
         <tli id="T216" time="221.08" type="appl" />
         <tli id="T217" time="221.59667484919785" />
         <tli id="T218" time="222.04333333333335" type="appl" />
         <tli id="T219" time="222.47666666666666" type="appl" />
         <tli id="T220" time="223.0584354294892" />
         <tli id="T221" time="223.6833268797257" />
         <tli id="T222" time="224.33" type="appl" />
         <tli id="T223" time="224.95" type="appl" />
         <tli id="T224" time="225.57" type="appl" />
         <tli id="T225" time="226.84" type="appl" />
         <tli id="T226" time="232.08" type="appl" />
         <tli id="T227" time="233.65" type="appl" />
         <tli id="T228" time="234.1125" type="appl" />
         <tli id="T229" time="234.575" type="appl" />
         <tli id="T230" time="235.0375" type="appl" />
         <tli id="T231" time="235.5" type="appl" />
         <tli id="T232" time="236.8" type="appl" />
         <tli id="T233" time="237.5575" type="appl" />
         <tli id="T234" time="238.315" type="appl" />
         <tli id="T235" time="239.07250000000002" type="appl" />
         <tli id="T236" time="239.83" type="appl" />
         <tli id="T237" time="240.58" type="appl" />
         <tli id="T238" time="241.888" type="appl" />
         <tli id="T239" time="243.19600000000003" type="appl" />
         <tli id="T240" time="244.50400000000002" type="appl" />
         <tli id="T241" time="245.812" type="appl" />
         <tli id="T242" time="246.77" type="appl" />
         <tli id="T243" time="247.67666666666668" type="appl" />
         <tli id="T244" time="248.58333333333334" type="appl" />
         <tli id="T245" time="249.49" type="appl" />
         <tli id="T246" time="250.88" type="appl" />
         <tli id="T247" time="252.00333333333333" type="appl" />
         <tli id="T248" time="253.12666666666667" type="appl" />
         <tli id="T249" time="254.25" type="appl" />
         <tli id="T250" time="255.37333333333333" type="appl" />
         <tli id="T251" time="256.49666666666667" type="appl" />
         <tli id="T252" time="257.62" type="appl" />
         <tli id="T253" time="258.26" type="appl" />
         <tli id="T254" time="258.84749999999997" type="appl" />
         <tli id="T255" time="259.435" type="appl" />
         <tli id="T256" time="260.02250000000004" type="appl" />
         <tli id="T257" time="260.61" type="appl" />
         <tli id="T258" time="261.5" type="appl" />
         <tli id="T259" time="262.26666666666665" type="appl" />
         <tli id="T260" time="263.03333333333336" type="appl" />
         <tli id="T261" time="263.8" type="appl" />
         <tli id="T262" time="265.51" type="appl" />
         <tli id="T263" time="266.1342857142857" type="appl" />
         <tli id="T264" time="266.75857142857143" type="appl" />
         <tli id="T265" time="267.3828571428571" type="appl" />
         <tli id="T266" time="268.00714285714287" type="appl" />
         <tli id="T267" time="268.63142857142856" type="appl" />
         <tli id="T268" time="269.2557142857143" type="appl" />
         <tli id="T269" time="269.88" type="appl" />
         <tli id="T270" time="270.51" type="appl" />
         <tli id="T271" time="271.51" type="appl" />
         <tli id="T272" time="272.51" type="appl" />
         <tli id="T273" time="273.51" type="appl" />
         <tli id="T274" time="274.51" type="appl" />
         <tli id="T275" time="275.51666122840516" />
         <tli id="T276" time="276.64439999999996" type="appl" />
         <tli id="T277" time="277.6988" type="appl" />
         <tli id="T278" time="278.7532" type="appl" />
         <tli id="T279" time="279.80760000000004" type="appl" />
         <tli id="T280" time="280.862" type="appl" />
         <tli id="T281" time="281.35" type="appl" />
         <tli id="T282" time="282.11400000000003" type="appl" />
         <tli id="T283" time="282.87800000000004" type="appl" />
         <tli id="T284" time="283.642" type="appl" />
         <tli id="T285" time="284.406" type="appl" />
         <tli id="T286" time="285.17" type="appl" />
         <tli id="T287" time="285.82" type="appl" />
         <tli id="T288" time="286.37025" type="appl" />
         <tli id="T289" time="286.9205" type="appl" />
         <tli id="T290" time="287.47075" type="appl" />
         <tli id="T291" time="288.021" type="appl" />
         <tli id="T292" time="289.091" type="appl" />
         <tli id="T293" time="289.94100000000003" type="appl" />
         <tli id="T294" time="290.791" type="appl" />
         <tli id="T295" time="291.641" type="appl" />
         <tli id="T296" time="292.491" type="appl" />
         <tli id="T297" time="293.341" type="appl" />
         <tli id="T298" time="294.751" type="appl" />
         <tli id="T299" time="295.471" type="appl" />
         <tli id="T300" time="296.65" type="appl" />
         <tli id="T301" time="297.73575" type="appl" />
         <tli id="T302" time="298.8215" type="appl" />
         <tli id="T303" time="299.90725" type="appl" />
         <tli id="T304" time="300.993" type="appl" />
         <tli id="T305" time="301.92" type="appl" />
         <tli id="T306" time="302.58500000000004" type="appl" />
         <tli id="T307" time="303.25" type="appl" />
         <tli id="T308" time="303.91499999999996" type="appl" />
         <tli id="T309" time="304.58" type="appl" />
         <tli id="T310" time="305.07" type="appl" />
         <tli id="T311" time="305.6675" type="appl" />
         <tli id="T312" time="306.265" type="appl" />
         <tli id="T313" time="306.86249999999995" type="appl" />
         <tli id="T314" time="307.46" type="appl" />
         <tli id="T315" time="309.06" type="appl" />
         <tli id="T316" time="310.134" type="appl" />
         <tli id="T317" time="311.208" type="appl" />
         <tli id="T318" time="312.282" type="appl" />
         <tli id="T319" time="313.356" type="appl" />
         <tli id="T320" time="314.43" type="appl" />
         <tli id="T321" time="314.93" type="appl" />
         <tli id="T322" time="315.86985714285714" type="appl" />
         <tli id="T323" time="316.8097142857143" type="appl" />
         <tli id="T324" time="317.7495714285714" type="appl" />
         <tli id="T325" time="318.6894285714286" type="appl" />
         <tli id="T326" time="319.62928571428574" type="appl" />
         <tli id="T327" time="320.5691428571429" type="appl" />
         <tli id="T328" time="321.509" type="appl" />
         <tli id="T329" time="324.3" type="appl" />
         <tli id="T330" time="325.56" type="appl" />
         <tli id="T331" time="326.82" type="appl" />
         <tli id="T332" time="328.08" type="appl" />
         <tli id="T333" time="329.12" type="appl" />
         <tli id="T334" time="329.718" type="appl" />
         <tli id="T335" time="330.31600000000003" type="appl" />
         <tli id="T336" time="330.914" type="appl" />
         <tli id="T337" time="331.512" type="appl" />
         <tli id="T338" time="332.11" type="appl" />
         <tli id="T339" time="332.6" type="appl" />
         <tli id="T340" time="333.18" type="appl" />
         <tli id="T341" time="333.76" type="appl" />
         <tli id="T342" time="334.34000000000003" type="appl" />
         <tli id="T343" time="334.92" type="appl" />
         <tli id="T344" time="337.0043028601008" />
         <tli id="T345" time="339.07" type="appl" />
         <tli id="T346" time="340.88" type="appl" />
         <tli id="T347" time="342.69" type="appl" />
         <tli id="T348" time="344.5" type="appl" />
         <tli id="T349" time="346.31" type="appl" />
         <tli id="T350" time="348.12" type="appl" />
         <tli id="T351" time="349.03" type="appl" />
         <tli id="T352" time="349.7733333333333" type="appl" />
         <tli id="T353" time="350.51666666666665" type="appl" />
         <tli id="T354" time="351.26" type="appl" />
         <tli id="T355" time="352.08" type="appl" />
         <tli id="T356" time="352.755" type="appl" />
         <tli id="T357" time="353.43" type="appl" />
         <tli id="T358" time="354.105" type="appl" />
         <tli id="T359" time="354.78" type="appl" />
         <tli id="T360" time="355.455" type="appl" />
         <tli id="T361" time="356.2375012884482" />
         <tli id="T362" time="356.7" type="appl" />
         <tli id="T363" time="357.92499999999995" type="appl" />
         <tli id="T364" time="359.15" type="appl" />
         <tli id="T365" time="360.92" type="appl" />
         <tli id="T366" time="362.07" type="appl" />
         <tli id="T367" time="364.63077574990024" />
         <tli id="T368" time="364.93538787495015" type="intp" />
         <tli id="T369" time="365.24" type="appl" />
         <tli id="T370" time="365.89" type="appl" />
         <tli id="T371" time="366.87742665816825" />
         <tli id="T372" time="367.28" type="appl" />
         <tli id="T373" time="368.005" type="appl" />
         <tli id="T374" time="368.73" type="appl" />
         <tli id="T375" time="370.35" type="appl" />
         <tli id="T376" time="371.2471428571429" type="appl" />
         <tli id="T377" time="372.14428571428573" type="appl" />
         <tli id="T378" time="373.0414285714286" type="appl" />
         <tli id="T379" time="373.93857142857144" type="appl" />
         <tli id="T380" time="374.8357142857143" type="appl" />
         <tli id="T381" time="375.73285714285714" type="appl" />
         <tli id="T382" time="376.63" type="appl" />
         <tli id="T383" time="377.9" type="appl" />
         <tli id="T384" time="378.42" type="appl" />
         <tli id="T385" time="378.88" type="appl" />
         <tli id="T386" time="380.59" type="appl" />
         <tli id="T387" time="383.517309943144" />
         <tli id="T388" time="384.2009090909091" type="appl" />
         <tli id="T389" time="385.14181818181817" type="appl" />
         <tli id="T390" time="386.08272727272725" type="appl" />
         <tli id="T391" time="387.02363636363634" type="appl" />
         <tli id="T392" time="387.96454545454543" type="appl" />
         <tli id="T393" time="388.9054545454546" type="appl" />
         <tli id="T394" time="389.84636363636366" type="appl" />
         <tli id="T395" time="390.78727272727275" type="appl" />
         <tli id="T396" time="391.72818181818184" type="appl" />
         <tli id="T397" time="392.6690909090909" type="appl" />
         <tli id="T398" time="393.34390768435173" />
         <tli id="T399" time="393.88" type="appl" />
         <tli id="T400" time="394.65999999999997" type="appl" />
         <tli id="T401" time="395.44" type="appl" />
         <tli id="T402" time="396.22" type="appl" />
         <tli id="T403" time="397.0" type="appl" />
         <tli id="T404" time="397.41" type="appl" />
         <tli id="T405" time="398.02" type="appl" />
         <tli id="T406" time="398.63" type="appl" />
         <tli id="T407" time="399.96" type="appl" />
         <tli id="T408" time="401.26666666666665" type="appl" />
         <tli id="T409" time="402.5733333333333" type="appl" />
         <tli id="T410" time="403.88" type="appl" />
         <tli id="T411" time="404.35" type="appl" />
         <tli id="T412" time="404.84833333333336" type="appl" />
         <tli id="T413" time="405.3466666666667" type="appl" />
         <tli id="T414" time="405.845" type="appl" />
         <tli id="T415" time="406.3433333333333" type="appl" />
         <tli id="T416" time="406.84166666666664" type="appl" />
         <tli id="T417" time="407.34" type="appl" />
         <tli id="T418" time="407.66" type="appl" />
         <tli id="T419" time="408.26" type="appl" />
         <tli id="T420" time="408.86" type="appl" />
         <tli id="T421" time="409.46" type="appl" />
         <tli id="T422" time="410.25" type="appl" />
         <tli id="T423" time="410.686" type="appl" />
         <tli id="T424" time="411.122" type="appl" />
         <tli id="T425" time="411.558" type="appl" />
         <tli id="T426" time="411.994" type="appl" />
         <tli id="T427" time="412.61710583213414" />
         <tli id="T428" time="414.13" type="appl" />
         <tli id="T429" time="414.6166666666667" type="appl" />
         <tli id="T430" time="415.1033333333333" type="appl" />
         <tli id="T431" time="415.59" type="appl" />
         <tli id="T432" time="416.52" type="appl" />
         <tli id="T433" time="417.034" type="appl" />
         <tli id="T434" time="417.548" type="appl" />
         <tli id="T435" time="418.06199999999995" type="appl" />
         <tli id="T436" time="418.57599999999996" type="appl" />
         <tli id="T437" time="419.09" type="appl" />
         <tli id="T438" time="419.53" type="appl" />
         <tli id="T439" time="420.53499999999997" type="appl" />
         <tli id="T440" time="421.53999999999996" type="appl" />
         <tli id="T441" time="422.545" type="appl" />
         <tli id="T442" time="423.55" type="appl" />
         <tli id="T443" time="424.14" type="appl" />
         <tli id="T444" time="424.56666666666666" type="appl" />
         <tli id="T445" time="424.99333333333334" type="appl" />
         <tli id="T446" time="425.41999999999996" type="appl" />
         <tli id="T447" time="425.84666666666664" type="appl" />
         <tli id="T448" time="426.2733333333333" type="appl" />
         <tli id="T449" time="426.7" type="appl" />
         <tli id="T450" time="427.62" type="appl" />
         <tli id="T451" time="428.6042857142857" type="appl" />
         <tli id="T452" time="429.5885714285714" type="appl" />
         <tli id="T453" time="430.5728571428571" type="appl" />
         <tli id="T454" time="431.5571428571429" type="appl" />
         <tli id="T455" time="432.5414285714286" type="appl" />
         <tli id="T456" time="433.5257142857143" type="appl" />
         <tli id="T457" time="434.51" type="appl" />
         <tli id="T458" time="434.92" type="appl" />
         <tli id="T459" time="435.74" type="appl" />
         <tli id="T460" time="436.6" type="appl" />
         <tli id="T461" time="438.39" type="appl" />
         <tli id="T462" time="440.79" type="appl" />
         <tli id="T463" time="442.18333333333334" type="appl" />
         <tli id="T464" time="443.5766666666667" type="appl" />
         <tli id="T465" time="444.97" type="appl" />
         <tli id="T466" time="445.82" type="appl" />
         <tli id="T467" time="446.42" type="appl" />
         <tli id="T468" time="446.67" type="appl" />
         <tli id="T469" time="446.9166666666667" type="appl" />
         <tli id="T470" time="447.16333333333336" type="appl" />
         <tli id="T471" time="447.39019526099383" />
         <tli id="T472" time="447.75685935581504" />
         <tli id="T473" time="447.97666666666663" type="appl" />
         <tli id="T474" time="448.42333333333335" type="appl" />
         <tli id="T475" time="448.87" type="appl" />
         <tli id="T476" time="449.51" type="appl" />
         <tli id="T477" time="449.88" type="appl" />
         <tli id="T478" time="450.83" type="appl" />
         <tli id="T479" time="451.55" type="appl" />
         <tli id="T480" time="452.27" type="appl" />
         <tli id="T481" time="452.99" type="appl" />
         <tli id="T482" time="453.56" type="appl" />
         <tli id="T483" time="454.37" type="appl" />
         <tli id="T484" time="455.18" type="appl" />
         <tli id="T485" time="455.99" type="appl" />
         <tli id="T486" time="456.8" type="appl" />
         <tli id="T487" time="457.34" type="appl" />
         <tli id="T488" time="458.2185714285714" type="appl" />
         <tli id="T489" time="459.09714285714284" type="appl" />
         <tli id="T490" time="459.9757142857143" type="appl" />
         <tli id="T491" time="460.8542857142857" type="appl" />
         <tli id="T492" time="461.73285714285714" type="appl" />
         <tli id="T493" time="462.6114285714286" type="appl" />
         <tli id="T494" time="463.49" type="appl" />
         <tli id="T495" time="464.16" type="appl" />
         <tli id="T496" time="465.42" type="appl" />
         <tli id="T497" time="466.3" type="appl" />
         <tli id="T498" time="467.43" type="appl" />
         <tli id="T499" time="468.51" type="appl" />
         <tli id="T500" time="469.375" type="appl" />
         <tli id="T501" time="470.24" type="appl" />
         <tli id="T502" time="471.105" type="appl" />
         <tli id="T503" time="471.96999999999997" type="appl" />
         <tli id="T504" time="472.835" type="appl" />
         <tli id="T505" time="473.7" type="appl" />
         <tli id="T506" time="474.18" type="appl" />
         <tli id="T507" time="474.79571428571427" type="appl" />
         <tli id="T508" time="475.4114285714286" type="appl" />
         <tli id="T509" time="476.02714285714285" type="appl" />
         <tli id="T510" time="476.64285714285717" type="appl" />
         <tli id="T511" time="477.25857142857143" type="appl" />
         <tli id="T512" time="477.87428571428575" type="appl" />
         <tli id="T513" time="478.49" type="appl" />
         <tli id="T514" time="479.59" type="appl" />
         <tli id="T515" time="480.75333333333333" type="appl" />
         <tli id="T516" time="481.91666666666663" type="appl" />
         <tli id="T517" time="483.08" type="appl" />
         <tli id="T518" time="483.9" type="appl" />
         <tli id="T519" time="484.76" type="appl" />
         <tli id="T520" time="485.62" type="appl" />
         <tli id="T521" time="486.47999999999996" type="appl" />
         <tli id="T522" time="487.34" type="appl" />
         <tli id="T523" time="487.6" type="appl" />
         <tli id="T524" time="488.31" type="appl" />
         <tli id="T525" time="489.02000000000004" type="appl" />
         <tli id="T526" time="489.73" type="appl" />
         <tli id="T527" time="490.1" type="appl" />
         <tli id="T528" time="491.75" type="appl" />
         <tli id="T529" time="493.2698734529824" />
         <tli id="T530" time="493.4098724710051" />
         <tli id="T531" time="493.97" type="appl" />
         <tli id="T532" time="495.29" type="appl" />
         <tli id="T533" time="496.35" type="appl" />
         <tli id="T534" time="497.59" type="appl" />
         <tli id="T535" time="498.35112499999997" type="appl" />
         <tli id="T536" time="499.11224999999996" type="appl" />
         <tli id="T537" time="499.87337499999995" type="appl" />
         <tli id="T538" time="500.6345" type="appl" />
         <tli id="T539" time="501.395625" type="appl" />
         <tli id="T540" time="502.15675" type="appl" />
         <tli id="T541" time="502.917875" type="appl" />
         <tli id="T542" time="503.679" type="appl" />
         <tli id="T543" time="504.2297965781828" />
         <tli id="T544" time="504.93142857142857" type="appl" />
         <tli id="T545" time="505.5628571428572" type="appl" />
         <tli id="T546" time="506.19428571428574" type="appl" />
         <tli id="T547" time="506.8257142857143" type="appl" />
         <tli id="T548" time="507.45714285714286" type="appl" />
         <tli id="T549" time="508.08857142857147" type="appl" />
         <tli id="T550" time="508.7533585991351" />
         <tli id="T551" time="509.5133532684008" />
         <tli id="T552" time="510.0466666666667" type="appl" />
         <tli id="T553" time="510.55333333333334" type="appl" />
         <tli id="T554" time="511.13334190551984" />
         <tli id="T555" time="511.91" type="appl" />
         <tli id="T556" time="512.8766630108969" />
         <tli id="T557" time="513.3233265445881" />
         <tli id="T558" time="514.0933333333334" type="appl" />
         <tli id="T559" time="514.8366666666667" type="appl" />
         <tli id="T560" time="515.58" type="appl" />
         <tli id="T561" time="516.5766891416059" />
         <tli id="T562" time="517.12" type="appl" />
         <tli id="T563" time="518.34" type="appl" />
         <tli id="T564" time="519.2" type="appl" />
         <tli id="T565" time="520.31" type="appl" />
         <tli id="T566" time="521.544" type="appl" />
         <tli id="T567" time="522.7779999999999" type="appl" />
         <tli id="T568" time="524.012" type="appl" />
         <tli id="T569" time="524.7066841997454" />
         <tli id="T570" time="526.385" type="appl" />
         <tli id="T571" time="527.99" type="appl" />
         <tli id="T572" time="528.4066582474865" />
         <tli id="T573" time="529.8575000000001" type="appl" />
         <tli id="T574" time="531.235" type="appl" />
         <tli id="T575" time="532.6125" type="appl" />
         <tli id="T576" time="534.0433374608418" />
         <tli id="T577" time="534.81" type="appl" />
         <tli id="T578" time="535.65" type="appl" />
         <tli id="T579" time="536.49" type="appl" />
         <tli id="T580" time="537.33" type="appl" />
         <tli id="T581" time="537.92" type="appl" />
         <tli id="T582" time="538.4875" type="appl" />
         <tli id="T583" time="539.0550000000001" type="appl" />
         <tli id="T584" time="539.6225000000001" type="appl" />
         <tli id="T585" time="540.19" type="appl" />
         <tli id="T586" time="540.23" type="appl" />
         <tli id="T587" time="540.9200000000001" type="appl" />
         <tli id="T588" time="541.61" type="appl" />
         <tli id="T589" time="541.96" type="appl" />
         <tli id="T590" time="543.005" type="appl" />
         <tli id="T591" time="544.05" type="appl" />
         <tli id="T592" time="545.17" type="appl" />
         <tli id="T593" time="546.1733333333333" type="appl" />
         <tli id="T594" time="547.1766666666666" type="appl" />
         <tli id="T595" time="548.18" type="appl" />
         <tli id="T596" time="548.7" type="appl" />
         <tli id="T597" time="549.4100000000001" type="appl" />
         <tli id="T598" time="550.12" type="appl" />
         <tli id="T599" time="550.67" type="appl" />
         <tli id="T600" time="551.47" type="appl" />
         <tli id="T601" time="552.51" type="appl" />
         <tli id="T602" time="553.11" type="appl" />
         <tli id="T603" time="554.54" type="appl" />
         <tli id="T604" time="555.3299999999999" type="appl" />
         <tli id="T605" time="556.12" type="appl" />
         <tli id="T606" time="556.91" type="appl" />
         <tli id="T607" time="557.43" type="appl" />
         <tli id="T608" time="558.3228571428571" type="appl" />
         <tli id="T609" time="559.2157142857143" type="appl" />
         <tli id="T610" time="560.1085714285714" type="appl" />
         <tli id="T611" time="561.0014285714285" type="appl" />
         <tli id="T612" time="561.8942857142856" type="appl" />
         <tli id="T613" time="562.7871428571428" type="appl" />
         <tli id="T614" time="563.68" type="appl" />
         <tli id="T615" time="564.24" type="appl" />
         <tli id="T616" time="565.585" type="appl" />
         <tli id="T617" time="566.9300000000001" type="appl" />
         <tli id="T618" time="568.275" type="appl" />
         <tli id="T619" time="569.62" type="appl" />
         <tli id="T620" time="570.28" type="appl" />
         <tli id="T621" time="571.2016666666666" type="appl" />
         <tli id="T622" time="572.1233333333333" type="appl" />
         <tli id="T623" time="573.045" type="appl" />
         <tli id="T624" time="573.9666666666666" type="appl" />
         <tli id="T625" time="574.8883333333333" type="appl" />
         <tli id="T626" time="575.81" type="appl" />
         <tli id="T627" time="576.48" type="appl" />
         <tli id="T628" time="577.2404285714285" type="appl" />
         <tli id="T629" time="578.0008571428572" type="appl" />
         <tli id="T630" time="578.7612857142857" type="appl" />
         <tli id="T631" time="579.5217142857143" type="appl" />
         <tli id="T632" time="580.2821428571428" type="appl" />
         <tli id="T633" time="581.0425714285715" type="appl" />
         <tli id="T634" time="581.803" type="appl" />
         <tli id="T635" time="582.053" type="appl" />
         <tli id="T636" time="582.6415" type="appl" />
         <tli id="T637" time="583.23" type="appl" />
         <tli id="T638" time="583.803" type="appl" />
         <tli id="T639" time="584.338" type="appl" />
         <tli id="T640" time="584.873" type="appl" />
         <tli id="T641" time="585.563" type="appl" />
         <tli id="T642" time="586.2363333333333" type="appl" />
         <tli id="T643" time="586.9096666666667" type="appl" />
         <tli id="T644" time="587.583" type="appl" />
         <tli id="T645" time="588.683" type="appl" />
         <tli id="T646" time="589.823" type="appl" />
         <tli id="T647" time="590.533" type="appl" />
         <tli id="T648" time="591.1963333333333" type="appl" />
         <tli id="T649" time="591.8596666666667" type="appl" />
         <tli id="T650" time="592.523" type="appl" />
         <tli id="T651" time="592.903" type="appl" />
         <tli id="T652" time="593.6510000000001" type="appl" />
         <tli id="T653" time="594.399" type="appl" />
         <tli id="T654" time="595.147" type="appl" />
         <tli id="T655" time="595.895" type="appl" />
         <tli id="T656" time="596.643" type="appl" />
         <tli id="T657" time="597.293" type="appl" />
         <tli id="T658" time="597.9648888888889" type="appl" />
         <tli id="T659" time="598.6367777777778" type="appl" />
         <tli id="T660" time="599.3086666666667" type="appl" />
         <tli id="T661" time="599.9805555555556" type="appl" />
         <tli id="T662" time="600.6524444444444" type="appl" />
         <tli id="T663" time="601.3243333333334" type="appl" />
         <tli id="T664" time="601.9962222222223" type="appl" />
         <tli id="T665" time="602.6681111111111" type="appl" />
         <tli id="T666" time="603.34" type="appl" />
         <tli id="T667" time="603.893" type="appl" />
         <tli id="T668" time="604.89175" type="appl" />
         <tli id="T669" time="605.8905" type="appl" />
         <tli id="T670" time="606.8892500000001" type="appl" />
         <tli id="T671" time="607.888" type="appl" />
         <tli id="T672" time="608.88675" type="appl" />
         <tli id="T673" time="609.8855000000001" type="appl" />
         <tli id="T674" time="610.8842500000001" type="appl" />
         <tli id="T675" time="611.883" type="appl" />
         <tli id="T676" time="612.61" type="appl" />
         <tli id="T677" time="613.94" type="appl" />
         <tli id="T678" time="615.27" type="appl" />
         <tli id="T679" time="616.6" type="appl" />
         <tli id="T680" time="617.25" type="appl" />
         <tli id="T681" time="617.71" type="appl" />
         <tli id="T682" time="618.17" type="appl" />
         <tli id="T683" time="618.63" type="appl" />
         <tli id="T684" time="619.0899999999999" type="appl" />
         <tli id="T685" time="619.55" type="appl" />
         <tli id="T686" time="620.24" type="appl" />
         <tli id="T687" time="621.2676" type="appl" />
         <tli id="T688" time="622.2952" type="appl" />
         <tli id="T689" time="623.3228" type="appl" />
         <tli id="T690" time="624.3504" type="appl" />
         <tli id="T691" time="625.378" type="appl" />
         <tli id="T692" time="626.31" type="appl" />
         <tli id="T693" time="626.9949999999999" type="appl" />
         <tli id="T694" time="627.68" type="appl" />
         <tli id="T695" time="628.49" type="appl" />
         <tli id="T696" time="629.11" type="appl" />
         <tli id="T697" time="636.58" type="appl" />
         <tli id="T698" time="637.56" type="appl" />
         <tli id="T699" time="638.09" type="appl" />
         <tli id="T700" time="638.65" type="appl" />
         <tli id="T701" time="639.21" type="appl" />
         <tli id="T702" time="639.8499911479754" />
         <tli id="T703" time="640.18" type="appl" />
         <tli id="T704" time="641.395" type="appl" />
         <tli id="T705" time="642.61" type="appl" />
         <tli id="T706" time="643.825" type="appl" />
         <tli id="T707" time="645.04" type="appl" />
         <tli id="T708" time="646.255" type="appl" />
         <tli id="T709" time="647.47" type="appl" />
         <tli id="T710" time="647.9933194459264" />
         <tli id="T711" time="648.96" type="appl" />
         <tli id="T712" time="649.98" type="appl" />
         <tli id="T713" time="651.0" type="appl" />
         <tli id="T714" time="651.69" type="appl" />
         <tli id="T715" time="652.335" type="appl" />
         <tli id="T716" time="652.98" type="appl" />
         <tli id="T717" time="653.37" type="appl" />
         <tli id="T718" time="654.2166666666667" type="appl" />
         <tli id="T719" time="655.0633333333333" type="appl" />
         <tli id="T720" time="655.91" type="appl" />
         <tli id="T721" time="656.32" type="appl" />
         <tli id="T722" time="657.2633333333333" type="appl" />
         <tli id="T723" time="658.2066666666667" type="appl" />
         <tli id="T724" time="659.15" type="appl" />
         <tli id="T725" time="659.57" type="appl" />
         <tli id="T726" time="660.5880000000001" type="appl" />
         <tli id="T727" time="661.606" type="appl" />
         <tli id="T728" time="662.624" type="appl" />
         <tli id="T729" time="663.6419999999999" type="appl" />
         <tli id="T730" time="664.66" type="appl" />
         <tli id="T731" time="664.92" type="appl" />
         <tli id="T732" time="665.4399999999999" type="appl" />
         <tli id="T733" time="665.96" type="appl" />
         <tli id="T734" time="666.48" type="appl" />
         <tli id="T735" time="667.42" type="appl" />
         <tli id="T736" time="668.39" type="appl" />
         <tli id="T737" time="669.36" type="appl" />
         <tli id="T738" time="670.05" type="appl" />
         <tli id="T739" time="671.62" type="appl" />
         <tli id="T740" time="672.29" type="appl" />
         <tli id="T741" time="672.8399999999999" type="appl" />
         <tli id="T742" time="673.39" type="appl" />
         <tli id="T743" time="673.9399999999999" type="appl" />
         <tli id="T744" time="674.49" type="appl" />
         <tli id="T745" time="675.04" type="appl" />
         <tli id="T746" time="675.49" type="appl" />
         <tli id="T747" time="675.975" type="appl" />
         <tli id="T748" time="676.46" type="appl" />
         <tli id="T749" time="676.9449999999999" type="appl" />
         <tli id="T750" time="677.43" type="appl" />
         <tli id="T751" time="678.37" type="appl" />
         <tli id="T752" time="678.985" type="appl" />
         <tli id="T753" time="679.7066907528458" />
         <tli id="T754" time="679.8166899812921" />
         <tli id="T755" time="680.3225" type="appl" />
         <tli id="T756" time="680.775" type="appl" />
         <tli id="T757" time="681.2275" type="appl" />
         <tli id="T758" time="681.68" type="appl" />
         <tli id="T759" time="682.19" type="appl" />
         <tli id="T760" time="682.57" type="appl" />
         <tli id="T761" time="682.95" type="appl" />
         <tli id="T762" time="683.33" type="appl" />
         <tli id="T763" time="683.71" type="appl" />
         <tli id="T764" time="685.48" type="appl" />
         <tli id="T765" time="687.18" type="appl" />
         <tli id="T766" time="687.75" type="appl" />
         <tli id="T767" time="688.4933333333333" type="appl" />
         <tli id="T768" time="689.2366666666667" type="appl" />
         <tli id="T769" time="689.98" type="appl" />
         <tli id="T770" time="690.35" type="appl" />
         <tli id="T771" time="691.0" type="appl" />
         <tli id="T772" time="691.65" type="appl" />
         <tli id="T773" time="693.01" type="appl" />
         <tli id="T774" time="693.92" type="appl" />
         <tli id="T775" time="694.8299999999999" type="appl" />
         <tli id="T776" time="695.74" type="appl" />
         <tli id="T777" time="696.8166749071194" />
         <tli id="T778" time="697.500003447468" />
         <tli id="T779" time="698.11" type="appl" />
         <tli id="T780" time="698.66" type="appl" />
         <tli id="T781" time="699.2099914533159" />
         <tli id="T782" time="699.38" type="appl" />
         <tli id="T783" time="700.225" type="appl" />
         <tli id="T784" time="701.07" type="appl" />
         <tli id="T785" time="701.915" type="appl" />
         <tli id="T786" time="702.76" type="appl" />
         <tli id="T787" time="703.605" type="appl" />
         <tli id="T788" time="704.45" type="appl" />
         <tli id="T789" time="705.0533358837522" />
         <tli id="T790" time="705.6733333333333" type="appl" />
         <tli id="T791" time="706.2266666666667" type="appl" />
         <tli id="T792" time="706.78" type="appl" />
         <tli id="T793" time="707.55" type="appl" />
         <tli id="T794" time="708.21" type="appl" />
         <tli id="T795" time="708.87" type="appl" />
         <tli id="T796" time="710.2400182534635" />
         <tli id="T797" time="710.77" type="appl" />
         <tli id="T798" time="711.54" type="appl" />
         <tli id="T799" time="712.19" type="appl" />
         <tli id="T800" time="712.32" type="appl" />
         <tli id="T801" time="713.4820000000001" type="appl" />
         <tli id="T802" time="714.644" type="appl" />
         <tli id="T803" time="715.806" type="appl" />
         <tli id="T804" time="716.968" type="appl" />
         <tli id="T805" time="718.13" type="appl" />
         <tli id="T806" time="718.87" type="appl" />
         <tli id="T807" time="719.4666666666667" type="appl" />
         <tli id="T808" time="720.0633333333334" type="appl" />
         <tli id="T809" time="720.6600000000001" type="appl" />
         <tli id="T810" time="721.2566666666667" type="appl" />
         <tli id="T811" time="721.8533333333334" type="appl" />
         <tli id="T812" time="722.45" type="appl" />
         <tli id="T813" time="723.57" type="appl" />
         <tli id="T814" time="724.02" type="appl" />
         <tli id="T815" time="725.53" type="appl" />
         <tli id="T816" time="727.0433333333333" type="appl" />
         <tli id="T817" time="728.5566666666667" type="appl" />
         <tli id="T818" time="730.07" type="appl" />
         <tli id="T819" time="731.2266418829829" />
         <tli id="T820" time="732.1" type="appl" />
         <tli id="T821" time="732.1533541328625" />
         <tli id="T822" time="732.8900000000001" type="appl" />
         <tli id="T823" time="733.6200105121474" />
         <tli id="T824" time="734.8566685046807" />
         <tli id="T825" time="735.355" type="appl" />
         <tli id="T826" time="735.8599948005094" />
         <tli id="T827" time="736.3866577730707" />
         <tli id="T828" time="737.2650000000001" type="appl" />
         <tli id="T829" time="738.09" type="appl" />
         <tli id="T830" time="739.09" type="appl" />
         <tli id="T831" time="739.642" type="appl" />
         <tli id="T832" time="740.1940000000001" type="appl" />
         <tli id="T833" time="740.746" type="appl" />
         <tli id="T834" time="741.298" type="appl" />
         <tli id="T835" time="741.9233376878386" />
         <tli id="T836" time="743.4133272367937" />
         <tli id="T837" time="745.3125" type="appl" />
         <tli id="T838" time="747.165" type="appl" />
         <tli id="T839" time="749.0175" type="appl" />
         <tli id="T840" time="750.87" type="appl" />
         <tli id="T841" time="751.0366591820894" />
         <tli id="T842" time="751.85" type="appl" />
         <tli id="T843" time="752.5966482400559" />
         <tli id="T844" time="752.6033148599618" />
         <tli id="T845" time="753.4866666666667" type="appl" />
         <tli id="T846" time="754.3233333333333" type="appl" />
         <tli id="T847" time="755.16" type="appl" />
         <tli id="T848" time="755.64" type="appl" />
         <tli id="T849" time="756.375" type="appl" />
         <tli id="T850" time="757.11" type="appl" />
         <tli id="T851" time="758.05" type="appl" />
         <tli id="T852" time="758.7825" type="appl" />
         <tli id="T853" time="759.515" type="appl" />
         <tli id="T854" time="760.2475" type="appl" />
         <tli id="T855" time="760.98" type="appl" />
         <tli id="T856" time="761.32" type="appl" />
         <tli id="T857" time="762.3233333333334" type="appl" />
         <tli id="T858" time="763.3266666666667" type="appl" />
         <tli id="T859" time="764.33" type="appl" />
         <tli id="T860" time="765.13" type="appl" />
         <tli id="T861" time="765.8333333333334" type="appl" />
         <tli id="T862" time="766.5366666666666" type="appl" />
         <tli id="T863" time="767.24" type="appl" />
         <tli id="T864" time="768.77" type="appl" />
         <tli id="T865" time="770.21" type="appl" />
         <tli id="T866" time="771.65" type="appl" />
         <tli id="T867" time="771.8933358064129" />
         <tli id="T868" time="772.7066666666667" type="appl" />
         <tli id="T869" time="773.4533333333334" type="appl" />
         <tli id="T870" time="774.2" type="appl" />
         <tli id="T871" time="774.9466666666667" type="appl" />
         <tli id="T872" time="775.6933333333334" type="appl" />
         <tli id="T873" time="776.44" type="appl" />
         <tli id="T874" time="777.28" type="appl" />
         <tli id="T875" time="777.9675" type="appl" />
         <tli id="T876" time="778.655" type="appl" />
         <tli id="T877" time="779.3425" type="appl" />
         <tli id="T878" time="780.03" type="appl" />
         <tli id="T879" time="780.27" type="appl" />
         <tli id="T880" time="780.8" type="appl" />
         <tli id="T881" time="781.18" type="appl" />
         <tli id="T882" time="782.1316666666667" type="appl" />
         <tli id="T883" time="783.0833333333333" type="appl" />
         <tli id="T884" time="784.035" type="appl" />
         <tli id="T885" time="784.9866666666667" type="appl" />
         <tli id="T886" time="785.9383333333333" type="appl" />
         <tli id="T887" time="786.89" type="appl" />
         <tli id="T888" time="787.83" type="appl" />
         <tli id="T889" time="788.3900000000001" type="appl" />
         <tli id="T890" time="788.95" type="appl" />
         <tli id="T891" time="789.99" type="appl" />
         <tli id="T892" time="790.73" type="appl" />
         <tli id="T893" time="791.47" type="appl" />
         <tli id="T894" time="792.21" type="appl" />
         <tli id="T895" time="793.63" type="appl" />
         <tli id="T896" time="794.43" type="appl" />
         <tli id="T897" time="795.23" type="appl" />
         <tli id="T898" time="796.1433219628086" />
         <tli id="T899" time="796.41" type="appl" />
         <tli id="T900" time="796.92" type="appl" />
         <tli id="T901" time="797.43" type="appl" />
         <tli id="T902" time="797.94" type="appl" />
         <tli id="T903" time="798.45" type="appl" />
         <tli id="T904" time="799.1933526526983" />
         <tli id="T905" time="799.68" type="appl" />
         <tli id="T906" time="800.3943858951097" />
         <tli id="T907" time="802.21" type="appl" />
         <tli id="T908" time="802.9540000000001" type="appl" />
         <tli id="T909" time="803.698" type="appl" />
         <tli id="T910" time="804.442" type="appl" />
         <tli id="T911" time="805.1859999999999" type="appl" />
         <tli id="T912" time="805.9833571097641" />
         <tli id="T913" time="806.31" type="appl" />
         <tli id="T914" time="807.1333333333333" type="appl" />
         <tli id="T915" time="807.9566666666666" type="appl" />
         <tli id="T916" time="808.78" type="appl" />
         <tli id="T917" time="809.3300003024956" />
         <tli id="T918" time="809.98" type="appl" />
         <tli id="T919" time="810.83" type="appl" />
         <tli id="T920" time="811.6800000000001" type="appl" />
         <tli id="T921" time="812.53" type="appl" />
         <tli id="T922" time="813.38" type="appl" />
         <tli id="T923" time="814.2966842153141" />
         <tli id="T924" time="814.3800169641372" />
         <tli id="T925" time="815.245" type="appl" />
         <tli id="T926" time="816.05" type="appl" />
         <tli id="T927" time="816.855" type="appl" />
         <tli id="T928" time="817.8399926952679" />
         <tli id="T929" time="818.09" type="appl" />
         <tli id="T930" time="818.696" type="appl" />
         <tli id="T931" time="819.302" type="appl" />
         <tli id="T932" time="819.908" type="appl" />
         <tli id="T933" time="820.514" type="appl" />
         <tli id="T934" time="821.12" type="appl" />
         <tli id="T935" time="821.39" type="appl" />
         <tli id="T936" time="821.9575" type="appl" />
         <tli id="T937" time="822.525" type="appl" />
         <tli id="T938" time="823.0925" type="appl" />
         <tli id="T939" time="823.7266701550924" />
         <tli id="T940" time="823.9933349513259" />
         <tli id="T941" time="825.03" type="appl" />
         <tli id="T942" time="825.96" type="appl" />
         <tli id="T943" time="826.8900000000001" type="appl" />
         <tli id="T944" time="827.9333073156772" />
         <tli id="T945" time="828.316690043231" />
         <tli id="T946" time="829.12" type="appl" />
         <tli id="T947" time="829.85" type="appl" />
         <tli id="T948" time="830.2400098860658" />
         <tli id="T949" time="831.1216666666667" type="appl" />
         <tli id="T950" time="831.7033333333333" type="appl" />
         <tli id="T951" time="832.285" type="appl" />
         <tli id="T952" time="832.8666666666667" type="appl" />
         <tli id="T953" time="833.4483333333333" type="appl" />
         <tli id="T954" time="834.0766496418764" />
         <tli id="T955" time="835.0833092476582" />
         <tli id="T956" time="836.2760000000001" type="appl" />
         <tli id="T957" time="837.442" type="appl" />
         <tli id="T958" time="838.6080000000001" type="appl" />
         <tli id="T959" time="839.774" type="appl" />
         <tli id="T960" time="840.94" type="appl" />
         <tli id="T961" time="841.02" type="appl" />
         <tli id="T962" time="841.8025" type="appl" />
         <tli id="T963" time="842.585" type="appl" />
         <tli id="T964" time="843.3675" type="appl" />
         <tli id="T965" time="844.15" type="appl" />
         <tli id="T966" time="844.46" type="appl" />
         <tli id="T967" time="844.9100000000001" type="appl" />
         <tli id="T968" time="845.36" type="appl" />
         <tli id="T969" time="845.81" type="appl" />
         <tli id="T970" time="846.26" type="appl" />
         <tli id="T971" time="846.71" type="appl" />
         <tli id="T972" time="847.16" type="appl" />
         <tli id="T973" time="847.41" type="appl" />
         <tli id="T974" time="848.5666666666666" type="appl" />
         <tli id="T975" time="849.7233333333334" type="appl" />
         <tli id="T976" time="850.88" type="appl" />
         <tli id="T977" time="851.44" type="appl" />
         <tli id="T978" time="852.06" type="appl" />
         <tli id="T0" time="852.44" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SAE"
                      type="t">
         <timeline-fork end="T293" start="T292">
            <tli id="T292.tx.1" />
         </timeline-fork>
         <timeline-fork end="T441" start="T440">
            <tli id="T440.tx.1" />
         </timeline-fork>
         <timeline-fork end="T668" start="T667">
            <tli id="T667.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T5" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Jakše</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">kuza</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">mobi</ts>
                  <nts id="Seg_11" n="HIAT:ip">,</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_14" n="HIAT:w" s="T4">külambi</ts>
                  <nts id="Seg_15" n="HIAT:ip">.</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T8" id="Seg_17" n="sc" s="T6">
               <ts e="T8" id="Seg_19" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_21" n="HIAT:w" s="T6">Jakše</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_24" n="HIAT:w" s="T7">külambi</ts>
                  <nts id="Seg_25" n="HIAT:ip">.</nts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T17" id="Seg_27" n="sc" s="T9">
               <ts e="T17" id="Seg_29" n="HIAT:u" s="T9">
                  <nts id="Seg_30" n="HIAT:ip">(</nts>
                  <ts e="T10" id="Seg_32" n="HIAT:w" s="T9">A</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_35" n="HIAT:w" s="T10">dĭ</ts>
                  <nts id="Seg_36" n="HIAT:ip">)</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_39" n="HIAT:w" s="T11">bile</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_42" n="HIAT:w" s="T12">kuza</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_44" n="HIAT:ip">(</nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">oʔb</ts>
                  <nts id="Seg_47" n="HIAT:ip">)</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_49" n="HIAT:ip">(</nts>
                  <ts e="T15" id="Seg_51" n="HIAT:w" s="T14">ku-</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_54" n="HIAT:w" s="T15">ku-</ts>
                  <nts id="Seg_55" n="HIAT:ip">)</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">külambit</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T23" id="Seg_61" n="sc" s="T18">
               <ts e="T23" id="Seg_63" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">A</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">jakše</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_71" n="HIAT:w" s="T20">kuza</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_74" n="HIAT:w" s="T21">jakše</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_77" n="HIAT:w" s="T22">külambi</ts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T25" id="Seg_80" n="sc" s="T24">
               <ts e="T25" id="Seg_82" n="HIAT:u" s="T24">
                  <nts id="Seg_83" n="HIAT:ip">(</nts>
                  <ts e="T25" id="Seg_85" n="HIAT:w" s="T24">Vot</ts>
                  <nts id="Seg_86" n="HIAT:ip">)</nts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T30" id="Seg_89" n="sc" s="T26">
               <ts e="T30" id="Seg_91" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_93" n="HIAT:w" s="T26">Măn</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_96" n="HIAT:w" s="T27">abam</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_99" n="HIAT:w" s="T28">jakše</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_102" n="HIAT:w" s="T29">amnobi</ts>
                  <nts id="Seg_103" n="HIAT:ip">.</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T33" id="Seg_105" n="sc" s="T31">
               <ts e="T33" id="Seg_107" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_109" n="HIAT:w" s="T31">Dĭttə</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_112" n="HIAT:w" s="T32">bile</ts>
                  <nts id="Seg_113" n="HIAT:ip">.</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T37" id="Seg_115" n="sc" s="T34">
               <ts e="T37" id="Seg_117" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_119" n="HIAT:w" s="T34">Tăŋ</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_122" n="HIAT:w" s="T35">bile</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_125" n="HIAT:w" s="T36">amnobi</ts>
                  <nts id="Seg_126" n="HIAT:ip">.</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T44" id="Seg_128" n="sc" s="T38">
               <ts e="T44" id="Seg_130" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_132" n="HIAT:w" s="T38">Dĭttə</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_134" n="HIAT:ip">(</nts>
                  <ts e="T40" id="Seg_136" n="HIAT:w" s="T39">это</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_139" n="HIAT:w" s="T40">ой=</ts>
                  <nts id="Seg_140" n="HIAT:ip">)</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_143" n="HIAT:w" s="T41">bar</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_146" n="HIAT:w" s="T42">amnobi</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_149" n="HIAT:w" s="T43">jakše</ts>
                  <nts id="Seg_150" n="HIAT:ip">.</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T47" id="Seg_152" n="sc" s="T45">
               <ts e="T47" id="Seg_154" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_156" n="HIAT:w" s="T45">Dĭttə</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_159" n="HIAT:w" s="T46">külambi</ts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T50" id="Seg_162" n="sc" s="T48">
               <ts e="T50" id="Seg_164" n="HIAT:u" s="T48">
                  <nts id="Seg_165" n="HIAT:ip">(</nts>
                  <ts e="T49" id="Seg_167" n="HIAT:w" s="T48">Nu</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_170" n="HIAT:w" s="T49">mĭ=</ts>
                  <nts id="Seg_171" n="HIAT:ip">)</nts>
                  <nts id="Seg_172" n="HIAT:ip">.</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T55" id="Seg_174" n="sc" s="T51">
               <ts e="T55" id="Seg_176" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_178" n="HIAT:w" s="T51">Măn</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_181" n="HIAT:w" s="T52">amnobiam</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_184" n="HIAT:w" s="T53">tăŋ</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_187" n="HIAT:w" s="T54">bile</ts>
                  <nts id="Seg_188" n="HIAT:ip">.</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T59" id="Seg_190" n="sc" s="T56">
               <ts e="T59" id="Seg_192" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_194" n="HIAT:w" s="T56">Tăŋ</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_197" n="HIAT:w" s="T57">bile</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_200" n="HIAT:w" s="T58">amnobiam</ts>
                  <nts id="Seg_201" n="HIAT:ip">.</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T63" id="Seg_203" n="sc" s="T60">
               <ts e="T63" id="Seg_205" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_207" n="HIAT:w" s="T60">Măn</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_210" n="HIAT:w" s="T61">ipekpə</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_213" n="HIAT:w" s="T62">naga</ts>
                  <nts id="Seg_214" n="HIAT:ip">.</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T72" id="Seg_216" n="sc" s="T64">
               <ts e="T72" id="Seg_218" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_220" n="HIAT:w" s="T64">Naga</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_223" n="HIAT:w" s="T65">ipek</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_226" n="HIAT:w" s="T66">măn</ts>
                  <nts id="Seg_227" n="HIAT:ip">,</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_230" n="HIAT:w" s="T67">i</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_233" n="HIAT:w" s="T68">măn</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_236" n="HIAT:w" s="T69">bostə</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_239" n="HIAT:w" s="T70">ĭmbi-ĭmbi</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_242" n="HIAT:w" s="T71">amorbiam</ts>
                  <nts id="Seg_243" n="HIAT:ip">.</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T76" id="Seg_245" n="sc" s="T73">
               <ts e="T76" id="Seg_247" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_249" n="HIAT:w" s="T73">Dĭttəgəš</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_252" n="HIAT:w" s="T74">jakše</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_255" n="HIAT:w" s="T75">amnobiam</ts>
                  <nts id="Seg_256" n="HIAT:ip">.</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T79" id="Seg_258" n="sc" s="T77">
               <ts e="T79" id="Seg_260" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_262" n="HIAT:w" s="T77">Bar</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_265" n="HIAT:w" s="T78">gĭdeʔ</ts>
                  <nts id="Seg_266" n="HIAT:ip">.</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T83" id="Seg_268" n="sc" s="T80">
               <ts e="T83" id="Seg_270" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_272" n="HIAT:w" s="T80">Dĭttəgəš</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_275" n="HIAT:w" s="T81">ari</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_278" n="HIAT:w" s="T82">dʼürbiam</ts>
                  <nts id="Seg_279" n="HIAT:ip">.</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T90" id="Seg_281" n="sc" s="T84">
               <ts e="T86" id="Seg_283" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_285" n="HIAT:w" s="T84">Amnobiam</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_288" n="HIAT:w" s="T85">bile-bile-bile-bile</ts>
                  <nts id="Seg_289" n="HIAT:ip">.</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_292" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_294" n="HIAT:w" s="T86">Koʔbdom</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_296" n="HIAT:ip">(</nts>
                  <ts e="T88" id="Seg_298" n="HIAT:w" s="T87">mĭle-</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_301" n="HIAT:w" s="T88">mĭle-</ts>
                  <nts id="Seg_302" n="HIAT:ip">)</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_305" n="HIAT:w" s="T89">mĭləbiam</ts>
                  <nts id="Seg_306" n="HIAT:ip">.</nts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T96" id="Seg_308" n="sc" s="T91">
               <ts e="T96" id="Seg_310" n="HIAT:u" s="T91">
                  <nts id="Seg_311" n="HIAT:ip">(</nts>
                  <ts e="T92" id="Seg_313" n="HIAT:w" s="T91">I</ts>
                  <nts id="Seg_314" n="HIAT:ip">)</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_317" n="HIAT:w" s="T92">koʔbdozi</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_320" n="HIAT:w" s="T93">tăŋ</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_323" n="HIAT:w" s="T94">bile</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_326" n="HIAT:w" s="T95">amnobiam</ts>
                  <nts id="Seg_327" n="HIAT:ip">.</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T98" id="Seg_329" n="sc" s="T97">
               <ts e="T98" id="Seg_331" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_333" n="HIAT:w" s="T97">Dʼörlam</ts>
                  <nts id="Seg_334" n="HIAT:ip">.</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T109" id="Seg_336" n="sc" s="T99">
               <ts e="T109" id="Seg_338" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_340" n="HIAT:w" s="T99">I</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_342" n="HIAT:ip">(</nts>
                  <ts e="T101" id="Seg_344" n="HIAT:w" s="T100">an-</ts>
                  <nts id="Seg_345" n="HIAT:ip">)</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_348" n="HIAT:w" s="T101">allam</ts>
                  <nts id="Seg_349" n="HIAT:ip">,</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_352" n="HIAT:w" s="T102">i</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_354" n="HIAT:ip">(</nts>
                  <ts e="T104" id="Seg_356" n="HIAT:w" s="T103">šo-</ts>
                  <nts id="Seg_357" n="HIAT:ip">)</nts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_360" n="HIAT:w" s="T104">šolam</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_362" n="HIAT:ip">(</nts>
                  <ts e="T106" id="Seg_364" n="HIAT:w" s="T105">amno-</ts>
                  <nts id="Seg_365" n="HIAT:ip">)</nts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_368" n="HIAT:w" s="T106">amnozittə</ts>
                  <nts id="Seg_369" n="HIAT:ip">,</nts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_372" n="HIAT:w" s="T107">tăŋ</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_375" n="HIAT:w" s="T108">bile</ts>
                  <nts id="Seg_376" n="HIAT:ip">.</nts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T113" id="Seg_378" n="sc" s="T110">
               <ts e="T113" id="Seg_380" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_382" n="HIAT:w" s="T110">Gibər</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_385" n="HIAT:w" s="T111">măna</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_388" n="HIAT:w" s="T112">anzittə</ts>
                  <nts id="Seg_389" n="HIAT:ip">?</nts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T119" id="Seg_391" n="sc" s="T114">
               <ts e="T119" id="Seg_393" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_395" n="HIAT:w" s="T114">Gibərdə</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_398" n="HIAT:w" s="T115">ej</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_401" n="HIAT:w" s="T116">alləm</ts>
                  <nts id="Seg_402" n="HIAT:ip">,</nts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_405" n="HIAT:w" s="T117">lutʼše</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_408" n="HIAT:w" s="T118">külambiam</ts>
                  <nts id="Seg_409" n="HIAT:ip">.</nts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T121" id="Seg_411" n="sc" s="T120">
               <ts e="T121" id="Seg_413" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_415" n="HIAT:w" s="T120">Это</ts>
                  <nts id="Seg_416" n="HIAT:ip">.</nts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T126" id="Seg_418" n="sc" s="T122">
               <ts e="T126" id="Seg_420" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_422" n="HIAT:w" s="T122">Bar</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_425" n="HIAT:w" s="T123">il</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_428" n="HIAT:w" s="T124">jakše</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_431" n="HIAT:w" s="T125">amnoliaʔit</ts>
                  <nts id="Seg_432" n="HIAT:ip">.</nts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T133" id="Seg_434" n="sc" s="T127">
               <ts e="T133" id="Seg_436" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_438" n="HIAT:w" s="T127">A</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_441" n="HIAT:w" s="T128">dĭttə</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_444" n="HIAT:w" s="T129">ari</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_447" n="HIAT:w" s="T130">dʼürgelit</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_450" n="HIAT:w" s="T131">dögəʔ</ts>
                  <nts id="Seg_451" n="HIAT:ip">,</nts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_454" n="HIAT:w" s="T132">gibərdə</ts>
                  <nts id="Seg_455" n="HIAT:ip">.</nts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T140" id="Seg_457" n="sc" s="T134">
               <ts e="T140" id="Seg_459" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_461" n="HIAT:w" s="T134">A</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_464" n="HIAT:w" s="T135">dĭttə</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_467" n="HIAT:w" s="T136">moʔ</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_470" n="HIAT:w" s="T137">döbər</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_473" n="HIAT:w" s="T138">že</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_476" n="HIAT:w" s="T139">šoləj</ts>
                  <nts id="Seg_477" n="HIAT:ip">.</nts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T145" id="Seg_479" n="sc" s="T141">
               <ts e="T145" id="Seg_481" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_483" n="HIAT:w" s="T141">Eh</ts>
                  <nts id="Seg_484" n="HIAT:ip">,</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_487" n="HIAT:w" s="T142">moʔ</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_490" n="HIAT:w" s="T143">dereʔ</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_493" n="HIAT:w" s="T144">mollal</ts>
                  <nts id="Seg_494" n="HIAT:ip">?</nts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T148" id="Seg_496" n="sc" s="T146">
               <ts e="T148" id="Seg_498" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_500" n="HIAT:w" s="T146">I</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_503" n="HIAT:w" s="T147">ĭmbi</ts>
                  <nts id="Seg_504" n="HIAT:ip">?</nts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T150" id="Seg_506" n="sc" s="T149">
               <ts e="T150" id="Seg_508" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_510" n="HIAT:w" s="T149">Dereʔ</ts>
                  <nts id="Seg_511" n="HIAT:ip">.</nts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T153" id="Seg_513" n="sc" s="T151">
               <ts e="T153" id="Seg_515" n="HIAT:u" s="T151">
                  <ts e="T152" id="Seg_517" n="HIAT:w" s="T151">Šoləj</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_520" n="HIAT:w" s="T152">döbər</ts>
                  <nts id="Seg_521" n="HIAT:ip">.</nts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T157" id="Seg_523" n="sc" s="T154">
               <ts e="T157" id="Seg_525" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_527" n="HIAT:w" s="T154">А</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_530" n="HIAT:w" s="T155">döbər</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_533" n="HIAT:w" s="T156">šoləj</ts>
                  <nts id="Seg_534" n="HIAT:ip">.</nts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T163" id="Seg_536" n="sc" s="T158">
               <ts e="T163" id="Seg_538" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_540" n="HIAT:w" s="T158">A</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_543" n="HIAT:w" s="T159">tăn</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_546" n="HIAT:w" s="T160">ĭmbi</ts>
                  <nts id="Seg_547" n="HIAT:ip">,</nts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_550" n="HIAT:w" s="T161">ej</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_553" n="HIAT:w" s="T162">alləl</ts>
                  <nts id="Seg_554" n="HIAT:ip">?</nts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T165" id="Seg_556" n="sc" s="T164">
               <ts e="T165" id="Seg_558" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_560" n="HIAT:w" s="T164">Dʼok</ts>
                  <nts id="Seg_561" n="HIAT:ip">.</nts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T169" id="Seg_563" n="sc" s="T166">
               <ts e="T169" id="Seg_565" n="HIAT:u" s="T166">
                  <ts e="T167" id="Seg_567" n="HIAT:w" s="T166">Măn</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_570" n="HIAT:w" s="T167">ej</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_573" n="HIAT:w" s="T168">alləm</ts>
                  <nts id="Seg_574" n="HIAT:ip">.</nts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T173" id="Seg_576" n="sc" s="T170">
               <ts e="T173" id="Seg_578" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_580" n="HIAT:w" s="T170">Măn</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_583" n="HIAT:w" s="T171">dögən</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_586" n="HIAT:w" s="T172">molam</ts>
                  <nts id="Seg_587" n="HIAT:ip">.</nts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T175" id="Seg_589" n="sc" s="T174">
               <ts e="T175" id="Seg_591" n="HIAT:u" s="T174">
                  <ts e="T175" id="Seg_593" n="HIAT:w" s="T174">Vot</ts>
                  <nts id="Seg_594" n="HIAT:ip">.</nts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T177" id="Seg_596" n="sc" s="T176">
               <ts e="T177" id="Seg_598" n="HIAT:u" s="T176">
                  <nts id="Seg_599" n="HIAT:ip">(</nts>
                  <nts id="Seg_600" n="HIAT:ip">(</nts>
                  <ats e="T177" id="Seg_601" n="HIAT:non-pho" s="T176">BRK</ats>
                  <nts id="Seg_602" n="HIAT:ip">)</nts>
                  <nts id="Seg_603" n="HIAT:ip">)</nts>
                  <nts id="Seg_604" n="HIAT:ip">.</nts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T181" id="Seg_606" n="sc" s="T178">
               <ts e="T181" id="Seg_608" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_610" n="HIAT:w" s="T178">Ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_613" n="HIAT:w" s="T179">mĭnzərzittə</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_616" n="HIAT:w" s="T180">nada</ts>
                  <nts id="Seg_617" n="HIAT:ip">.</nts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T185" id="Seg_619" n="sc" s="T182">
               <ts e="T185" id="Seg_621" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_623" n="HIAT:w" s="T182">Măn</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_626" n="HIAT:w" s="T183">amorzittə</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_629" n="HIAT:w" s="T184">molam</ts>
                  <nts id="Seg_630" n="HIAT:ip">.</nts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T187" id="Seg_632" n="sc" s="T186">
               <ts e="T187" id="Seg_634" n="HIAT:u" s="T186">
                  <nts id="Seg_635" n="HIAT:ip">(</nts>
                  <nts id="Seg_636" n="HIAT:ip">(</nts>
                  <ats e="T187" id="Seg_637" n="HIAT:non-pho" s="T186">BRK</ats>
                  <nts id="Seg_638" n="HIAT:ip">)</nts>
                  <nts id="Seg_639" n="HIAT:ip">)</nts>
                  <nts id="Seg_640" n="HIAT:ip">.</nts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T192" id="Seg_642" n="sc" s="T188">
               <ts e="T192" id="Seg_644" n="HIAT:u" s="T188">
                  <nts id="Seg_645" n="HIAT:ip">(</nts>
                  <ts e="T189" id="Seg_647" n="HIAT:w" s="T188">Ĭmbĭ-</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_650" n="HIAT:w" s="T189">m-</ts>
                  <nts id="Seg_651" n="HIAT:ip">)</nts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_654" n="HIAT:w" s="T190">Ĭmbi</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_657" n="HIAT:w" s="T191">mĭnzerliel</ts>
                  <nts id="Seg_658" n="HIAT:ip">?</nts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T194" id="Seg_660" n="sc" s="T193">
               <ts e="T194" id="Seg_662" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_664" n="HIAT:w" s="T193">Uja</ts>
                  <nts id="Seg_665" n="HIAT:ip">.</nts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T197" id="Seg_667" n="sc" s="T195">
               <ts e="T197" id="Seg_669" n="HIAT:u" s="T195">
                  <ts e="T196" id="Seg_671" n="HIAT:w" s="T195">Mo</ts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_674" n="HIAT:w" s="T196">uja</ts>
                  <nts id="Seg_675" n="HIAT:ip">?</nts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T202" id="Seg_677" n="sc" s="T198">
               <ts e="T202" id="Seg_679" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_681" n="HIAT:w" s="T198">Măn</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_684" n="HIAT:w" s="T199">bɨ</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_686" n="HIAT:ip">(</nts>
                  <ts e="T201" id="Seg_688" n="HIAT:w" s="T200">jejam</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_691" n="HIAT:w" s="T201">amzit</ts>
                  <nts id="Seg_692" n="HIAT:ip">)</nts>
                  <nts id="Seg_693" n="HIAT:ip">.</nts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T208" id="Seg_695" n="sc" s="T203">
               <ts e="T208" id="Seg_697" n="HIAT:u" s="T203">
                  <ts e="T204" id="Seg_699" n="HIAT:w" s="T203">A</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_702" n="HIAT:w" s="T204">măn</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_705" n="HIAT:w" s="T205">bɨ</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_708" n="HIAT:w" s="T206">süt</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_711" n="HIAT:w" s="T207">bɨ</ts>
                  <nts id="Seg_712" n="HIAT:ip">.</nts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T213" id="Seg_714" n="sc" s="T209">
               <ts e="T213" id="Seg_716" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_718" n="HIAT:w" s="T209">Vot</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_721" n="HIAT:w" s="T210">jakše</ts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_724" n="HIAT:w" s="T211">bɨ</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_727" n="HIAT:w" s="T212">amorlam</ts>
                  <nts id="Seg_728" n="HIAT:ip">.</nts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T216" id="Seg_730" n="sc" s="T214">
               <ts e="T216" id="Seg_732" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_734" n="HIAT:w" s="T214">Mo</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_737" n="HIAT:w" s="T215">süt</ts>
                  <nts id="Seg_738" n="HIAT:ip">?</nts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T220" id="Seg_740" n="sc" s="T217">
               <ts e="T220" id="Seg_742" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_744" n="HIAT:w" s="T217">Süt</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_747" n="HIAT:w" s="T218">dak</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_750" n="HIAT:w" s="T219">süt</ts>
                  <nts id="Seg_751" n="HIAT:ip">.</nts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T224" id="Seg_753" n="sc" s="T221">
               <ts e="T224" id="Seg_755" n="HIAT:u" s="T221">
                  <ts e="T222" id="Seg_757" n="HIAT:w" s="T221">A</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_760" n="HIAT:w" s="T222">uja</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_763" n="HIAT:w" s="T223">jakše</ts>
                  <nts id="Seg_764" n="HIAT:ip">.</nts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T226" id="Seg_766" n="sc" s="T225">
               <ts e="T226" id="Seg_768" n="HIAT:u" s="T225">
                  <nts id="Seg_769" n="HIAT:ip">(</nts>
                  <nts id="Seg_770" n="HIAT:ip">(</nts>
                  <ats e="T226" id="Seg_771" n="HIAT:non-pho" s="T225">BRK</ats>
                  <nts id="Seg_772" n="HIAT:ip">)</nts>
                  <nts id="Seg_773" n="HIAT:ip">)</nts>
                  <nts id="Seg_774" n="HIAT:ip">.</nts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T231" id="Seg_776" n="sc" s="T227">
               <ts e="T231" id="Seg_778" n="HIAT:u" s="T227">
                  <ts e="T228" id="Seg_780" n="HIAT:w" s="T227">Mo</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_783" n="HIAT:w" s="T228">măna</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_786" n="HIAT:w" s="T229">ej</ts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_789" n="HIAT:w" s="T230">šoləj</ts>
                  <nts id="Seg_790" n="HIAT:ip">?</nts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T236" id="Seg_792" n="sc" s="T232">
               <ts e="T236" id="Seg_794" n="HIAT:u" s="T232">
                  <ts e="T233" id="Seg_796" n="HIAT:w" s="T232">A</ts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_799" n="HIAT:w" s="T233">măn</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_802" n="HIAT:w" s="T234">iam</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_805" n="HIAT:w" s="T235">bila</ts>
                  <nts id="Seg_806" n="HIAT:ip">.</nts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T241" id="Seg_808" n="sc" s="T237">
               <ts e="T241" id="Seg_810" n="HIAT:u" s="T237">
                  <nts id="Seg_811" n="HIAT:ip">(</nts>
                  <ts e="T238" id="Seg_813" n="HIAT:w" s="T237">Ilajaŋ</ts>
                  <nts id="Seg_814" n="HIAT:ip">)</nts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_817" n="HIAT:w" s="T238">tănan</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_820" n="HIAT:w" s="T239">ej</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_822" n="HIAT:ip">(</nts>
                  <ts e="T241" id="Seg_824" n="HIAT:w" s="T240">öʔliet</ts>
                  <nts id="Seg_825" n="HIAT:ip">)</nts>
                  <nts id="Seg_826" n="HIAT:ip">.</nts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T245" id="Seg_828" n="sc" s="T242">
               <ts e="T245" id="Seg_830" n="HIAT:u" s="T242">
                  <ts e="T243" id="Seg_832" n="HIAT:w" s="T242">Mo</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_835" n="HIAT:w" s="T243">dereʔ</ts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_838" n="HIAT:w" s="T244">ej</ts>
                  <nts id="Seg_839" n="HIAT:ip">?</nts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T252" id="Seg_841" n="sc" s="T246">
               <ts e="T252" id="Seg_843" n="HIAT:u" s="T246">
                  <ts e="T247" id="Seg_845" n="HIAT:w" s="T246">Nu</ts>
                  <nts id="Seg_846" n="HIAT:ip">,</nts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_849" n="HIAT:w" s="T247">măllat</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_852" n="HIAT:w" s="T248">što</ts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_855" n="HIAT:w" s="T249">bila</ts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_858" n="HIAT:w" s="T250">kuzat</ts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_861" n="HIAT:w" s="T251">tăn</ts>
                  <nts id="Seg_862" n="HIAT:ip">.</nts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T257" id="Seg_864" n="sc" s="T253">
               <ts e="T257" id="Seg_866" n="HIAT:u" s="T253">
                  <ts e="T254" id="Seg_868" n="HIAT:w" s="T253">Mo</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_871" n="HIAT:w" s="T254">dere</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_874" n="HIAT:w" s="T255">bila</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_877" n="HIAT:w" s="T256">kuza</ts>
                  <nts id="Seg_878" n="HIAT:ip">?</nts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T261" id="Seg_880" n="sc" s="T258">
               <ts e="T261" id="Seg_882" n="HIAT:u" s="T258">
                  <ts e="T259" id="Seg_884" n="HIAT:w" s="T258">Măn</ts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_887" n="HIAT:w" s="T259">jakše</ts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_890" n="HIAT:w" s="T260">kuza</ts>
                  <nts id="Seg_891" n="HIAT:ip">.</nts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T269" id="Seg_893" n="sc" s="T262">
               <ts e="T269" id="Seg_895" n="HIAT:u" s="T262">
                  <ts e="T263" id="Seg_897" n="HIAT:w" s="T262">Ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_900" n="HIAT:w" s="T263">tănan</ts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_903" n="HIAT:w" s="T264">bar</ts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_906" n="HIAT:w" s="T265">bɨ</ts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_909" n="HIAT:w" s="T266">nörbəlam</ts>
                  <nts id="Seg_910" n="HIAT:ip">,</nts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_913" n="HIAT:w" s="T267">nörbəlam</ts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_916" n="HIAT:w" s="T268">bɨ</ts>
                  <nts id="Seg_917" n="HIAT:ip">.</nts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T274" id="Seg_919" n="sc" s="T270">
               <ts e="T274" id="Seg_921" n="HIAT:u" s="T270">
                  <ts e="T271" id="Seg_923" n="HIAT:w" s="T270">Teʔi</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_925" n="HIAT:ip">(</nts>
                  <ts e="T272" id="Seg_927" n="HIAT:w" s="T271">tăŋ</ts>
                  <nts id="Seg_928" n="HIAT:ip">)</nts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_931" n="HIAT:w" s="T272">jakše</ts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_934" n="HIAT:w" s="T273">moləj</ts>
                  <nts id="Seg_935" n="HIAT:ip">.</nts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T280" id="Seg_937" n="sc" s="T275">
               <ts e="T280" id="Seg_939" n="HIAT:u" s="T275">
                  <ts e="T276" id="Seg_941" n="HIAT:w" s="T275">Oh</ts>
                  <nts id="Seg_942" n="HIAT:ip">,</nts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_945" n="HIAT:w" s="T276">nu</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_948" n="HIAT:w" s="T277">dereʔ</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_951" n="HIAT:w" s="T278">măllat</ts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_954" n="HIAT:w" s="T279">măna</ts>
                  <nts id="Seg_955" n="HIAT:ip">.</nts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T286" id="Seg_957" n="sc" s="T281">
               <ts e="T286" id="Seg_959" n="HIAT:u" s="T281">
                  <ts e="T282" id="Seg_961" n="HIAT:w" s="T281">Măn</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_964" n="HIAT:w" s="T282">dĭm</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_967" n="HIAT:w" s="T283">bar</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_970" n="HIAT:w" s="T284">münörlam</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_973" n="HIAT:w" s="T285">tăŋ</ts>
                  <nts id="Seg_974" n="HIAT:ip">.</nts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T291" id="Seg_976" n="sc" s="T287">
               <ts e="T291" id="Seg_978" n="HIAT:u" s="T287">
                  <ts e="T288" id="Seg_980" n="HIAT:w" s="T287">Dʼok</ts>
                  <nts id="Seg_981" n="HIAT:ip">,</nts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_984" n="HIAT:w" s="T288">münörzittə</ts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_987" n="HIAT:w" s="T289">ne</ts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_990" n="HIAT:w" s="T290">nado</ts>
                  <nts id="Seg_991" n="HIAT:ip">.</nts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T297" id="Seg_993" n="sc" s="T292">
               <ts e="T297" id="Seg_995" n="HIAT:u" s="T292">
                  <nts id="Seg_996" n="HIAT:ip">(</nts>
                  <ts e="T292.tx.1" id="Seg_998" n="HIAT:w" s="T292">A</ts>
                  <nts id="Seg_999" n="HIAT:ip">_</nts>
                  <ts e="T293" id="Seg_1001" n="HIAT:w" s="T292.tx.1">to</ts>
                  <nts id="Seg_1002" n="HIAT:ip">)</nts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1005" n="HIAT:w" s="T293">dĭ</ts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1008" n="HIAT:w" s="T294">măna</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1011" n="HIAT:w" s="T295">münörlil</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1014" n="HIAT:w" s="T296">dittəgaš</ts>
                  <nts id="Seg_1015" n="HIAT:ip">.</nts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T299" id="Seg_1017" n="sc" s="T298">
               <ts e="T299" id="Seg_1019" n="HIAT:u" s="T298">
                  <nts id="Seg_1020" n="HIAT:ip">(</nts>
                  <nts id="Seg_1021" n="HIAT:ip">(</nts>
                  <ats e="T299" id="Seg_1022" n="HIAT:non-pho" s="T298">BRK</ats>
                  <nts id="Seg_1023" n="HIAT:ip">)</nts>
                  <nts id="Seg_1024" n="HIAT:ip">)</nts>
                  <nts id="Seg_1025" n="HIAT:ip">.</nts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T304" id="Seg_1027" n="sc" s="T300">
               <ts e="T304" id="Seg_1029" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_1031" n="HIAT:w" s="T300">Măn</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1034" n="HIAT:w" s="T301">tăŋ</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1037" n="HIAT:w" s="T302">ara</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1040" n="HIAT:w" s="T303">bĭtliem</ts>
                  <nts id="Seg_1041" n="HIAT:ip">.</nts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T309" id="Seg_1043" n="sc" s="T305">
               <ts e="T309" id="Seg_1045" n="HIAT:u" s="T305">
                  <ts e="T306" id="Seg_1047" n="HIAT:w" s="T305">O</ts>
                  <nts id="Seg_1048" n="HIAT:ip">,</nts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1051" n="HIAT:w" s="T306">mo</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1054" n="HIAT:w" s="T307">dereʔ</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1057" n="HIAT:w" s="T308">tăn</ts>
                  <nts id="Seg_1058" n="HIAT:ip">?</nts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T314" id="Seg_1060" n="sc" s="T310">
               <ts e="T314" id="Seg_1062" n="HIAT:u" s="T310">
                  <ts e="T311" id="Seg_1064" n="HIAT:w" s="T310">Nu</ts>
                  <nts id="Seg_1065" n="HIAT:ip">,</nts>
                  <nts id="Seg_1066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1068" n="HIAT:w" s="T311">măn</ts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1071" n="HIAT:w" s="T312">bile</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1074" n="HIAT:w" s="T313">kuza</ts>
                  <nts id="Seg_1075" n="HIAT:ip">.</nts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T320" id="Seg_1077" n="sc" s="T315">
               <ts e="T320" id="Seg_1079" n="HIAT:u" s="T315">
                  <ts e="T316" id="Seg_1081" n="HIAT:w" s="T315">Bar</ts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1084" n="HIAT:w" s="T316">gĭda</ts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1087" n="HIAT:w" s="T317">bar</ts>
                  <nts id="Seg_1088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1089" n="HIAT:ip">(</nts>
                  <ts e="T319" id="Seg_1091" n="HIAT:w" s="T318">iləm</ts>
                  <nts id="Seg_1092" n="HIAT:ip">)</nts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1095" n="HIAT:w" s="T319">ĭmbidə</ts>
                  <nts id="Seg_1096" n="HIAT:ip">.</nts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T328" id="Seg_1098" n="sc" s="T321">
               <ts e="T328" id="Seg_1100" n="HIAT:u" s="T321">
                  <ts e="T322" id="Seg_1102" n="HIAT:w" s="T321">A</ts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1105" n="HIAT:w" s="T322">mo</ts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1107" n="HIAT:ip">(</nts>
                  <ts e="T324" id="Seg_1109" n="HIAT:w" s="T323">tănan</ts>
                  <nts id="Seg_1110" n="HIAT:ip">)</nts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1113" n="HIAT:w" s="T324">iləl</ts>
                  <nts id="Seg_1114" n="HIAT:ip">,</nts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1117" n="HIAT:w" s="T325">dak</ts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1120" n="HIAT:w" s="T326">măn</ts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1123" n="HIAT:w" s="T327">dĭttəgəš</ts>
                  <nts id="Seg_1124" n="HIAT:ip">…</nts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T332" id="Seg_1126" n="sc" s="T329">
               <ts e="T332" id="Seg_1128" n="HIAT:u" s="T329">
                  <ts e="T330" id="Seg_1130" n="HIAT:w" s="T329">Măn</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1133" n="HIAT:w" s="T330">dittəgəš</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1136" n="HIAT:w" s="T331">iləm</ts>
                  <nts id="Seg_1137" n="HIAT:ip">.</nts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T338" id="Seg_1139" n="sc" s="T333">
               <ts e="T338" id="Seg_1141" n="HIAT:u" s="T333">
                  <ts e="T334" id="Seg_1143" n="HIAT:w" s="T333">A</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1146" n="HIAT:w" s="T334">mo</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1149" n="HIAT:w" s="T335">dĭn</ts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1152" n="HIAT:w" s="T336">dere</ts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1155" n="HIAT:w" s="T337">iləl</ts>
                  <nts id="Seg_1156" n="HIAT:ip">?</nts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T343" id="Seg_1158" n="sc" s="T339">
               <ts e="T343" id="Seg_1160" n="HIAT:u" s="T339">
                  <ts e="T340" id="Seg_1162" n="HIAT:w" s="T339">Dak</ts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1165" n="HIAT:w" s="T340">vot</ts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1168" n="HIAT:w" s="T341">iləm</ts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1171" n="HIAT:w" s="T342">măn</ts>
                  <nts id="Seg_1172" n="HIAT:ip">.</nts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T350" id="Seg_1174" n="sc" s="T344">
               <ts e="T350" id="Seg_1176" n="HIAT:u" s="T344">
                  <ts e="T345" id="Seg_1178" n="HIAT:w" s="T344">Măna</ts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1181" n="HIAT:w" s="T345">dĭttəgəš</ts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1184" n="HIAT:w" s="T346">ej</ts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1187" n="HIAT:w" s="T347">kuvas</ts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1190" n="HIAT:w" s="T348">turanə</ts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1193" n="HIAT:w" s="T349">kunnallit</ts>
                  <nts id="Seg_1194" n="HIAT:ip">.</nts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T354" id="Seg_1196" n="sc" s="T351">
               <ts e="T354" id="Seg_1198" n="HIAT:u" s="T351">
                  <ts e="T352" id="Seg_1200" n="HIAT:w" s="T351">Măn</ts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1203" n="HIAT:w" s="T352">dĭgən</ts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1206" n="HIAT:w" s="T353">amnolam</ts>
                  <nts id="Seg_1207" n="HIAT:ip">.</nts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T361" id="Seg_1209" n="sc" s="T355">
               <ts e="T361" id="Seg_1211" n="HIAT:u" s="T355">
                  <ts e="T356" id="Seg_1213" n="HIAT:w" s="T355">A</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1216" n="HIAT:w" s="T356">dĭttə</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1219" n="HIAT:w" s="T357">kak</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1221" n="HIAT:ip">(</nts>
                  <ts e="T359" id="Seg_1223" n="HIAT:w" s="T358">šoləm</ts>
                  <nts id="Seg_1224" n="HIAT:ip">)</nts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1227" n="HIAT:w" s="T359">măna</ts>
                  <nts id="Seg_1228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1230" n="HIAT:w" s="T360">il</ts>
                  <nts id="Seg_1231" n="HIAT:ip">.</nts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T364" id="Seg_1233" n="sc" s="T362">
               <ts e="T364" id="Seg_1235" n="HIAT:u" s="T362">
                  <ts e="T363" id="Seg_1237" n="HIAT:w" s="T362">Šindĭdə</ts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1240" n="HIAT:w" s="T363">ej</ts>
                  <nts id="Seg_1241" n="HIAT:ip">…</nts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T366" id="Seg_1243" n="sc" s="T365">
               <ts e="T366" id="Seg_1245" n="HIAT:u" s="T365">
                  <ts e="T366" id="Seg_1247" n="HIAT:w" s="T365">Ej</ts>
                  <nts id="Seg_1248" n="HIAT:ip">…</nts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T371" id="Seg_1250" n="sc" s="T367">
               <ts e="T371" id="Seg_1252" n="HIAT:u" s="T367">
                  <ts e="T368" id="Seg_1254" n="HIAT:w" s="T367">Вот</ts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1257" n="HIAT:w" s="T368">ведь</ts>
                  <nts id="Seg_1258" n="HIAT:ip">,</nts>
                  <nts id="Seg_1259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1261" n="HIAT:w" s="T369">не</ts>
                  <nts id="Seg_1262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1264" n="HIAT:w" s="T370">выговорить</ts>
                  <nts id="Seg_1265" n="HIAT:ip">.</nts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T374" id="Seg_1267" n="sc" s="T372">
               <ts e="T374" id="Seg_1269" n="HIAT:u" s="T372">
                  <ts e="T373" id="Seg_1271" n="HIAT:w" s="T372">Ej</ts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1274" n="HIAT:w" s="T373">iliet</ts>
                  <nts id="Seg_1275" n="HIAT:ip">.</nts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T382" id="Seg_1277" n="sc" s="T375">
               <ts e="T382" id="Seg_1279" n="HIAT:u" s="T375">
                  <ts e="T376" id="Seg_1281" n="HIAT:w" s="T375">Bar</ts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1284" n="HIAT:w" s="T376">il</ts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1286" n="HIAT:ip">(</nts>
                  <ts e="T378" id="Seg_1288" n="HIAT:w" s="T377">münörlat</ts>
                  <nts id="Seg_1289" n="HIAT:ip">)</nts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1291" n="HIAT:ip">(</nts>
                  <ts e="T379" id="Seg_1293" n="HIAT:w" s="T378">i</ts>
                  <nts id="Seg_1294" n="HIAT:ip">)</nts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1297" n="HIAT:w" s="T379">ej</ts>
                  <nts id="Seg_1298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1300" n="HIAT:w" s="T380">jakše</ts>
                  <nts id="Seg_1301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1303" n="HIAT:w" s="T381">kuza</ts>
                  <nts id="Seg_1304" n="HIAT:ip">.</nts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T384" id="Seg_1306" n="sc" s="T383">
               <ts e="T384" id="Seg_1308" n="HIAT:u" s="T383">
                  <ts e="T384" id="Seg_1310" n="HIAT:w" s="T383">Vot</ts>
                  <nts id="Seg_1311" n="HIAT:ip">.</nts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T386" id="Seg_1313" n="sc" s="T385">
               <ts e="T386" id="Seg_1315" n="HIAT:u" s="T385">
                  <nts id="Seg_1316" n="HIAT:ip">(</nts>
                  <nts id="Seg_1317" n="HIAT:ip">(</nts>
                  <ats e="T386" id="Seg_1318" n="HIAT:non-pho" s="T385">BRK</ats>
                  <nts id="Seg_1319" n="HIAT:ip">)</nts>
                  <nts id="Seg_1320" n="HIAT:ip">)</nts>
                  <nts id="Seg_1321" n="HIAT:ip">.</nts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T398" id="Seg_1323" n="sc" s="T387">
               <ts e="T398" id="Seg_1325" n="HIAT:u" s="T387">
                  <ts e="T388" id="Seg_1327" n="HIAT:w" s="T387">Bile</ts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1330" n="HIAT:w" s="T388">kuza</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1333" n="HIAT:w" s="T389">jakše</ts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1336" n="HIAT:w" s="T390">kuzan</ts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1339" n="HIAT:w" s="T391">ibi</ts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1342" n="HIAT:w" s="T392">kujnek</ts>
                  <nts id="Seg_1343" n="HIAT:ip">,</nts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1346" n="HIAT:w" s="T393">uja</ts>
                  <nts id="Seg_1347" n="HIAT:ip">,</nts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1350" n="HIAT:w" s="T394">i</ts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1353" n="HIAT:w" s="T395">bar</ts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1356" n="HIAT:w" s="T396">ĭmbi</ts>
                  <nts id="Seg_1357" n="HIAT:ip">,</nts>
                  <nts id="Seg_1358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1360" n="HIAT:w" s="T397">bar</ts>
                  <nts id="Seg_1361" n="HIAT:ip">.</nts>
                  <nts id="Seg_1362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T403" id="Seg_1363" n="sc" s="T399">
               <ts e="T403" id="Seg_1365" n="HIAT:u" s="T399">
                  <ts e="T400" id="Seg_1367" n="HIAT:w" s="T399">Aktʼat</ts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1369" n="HIAT:ip">(</nts>
                  <ts e="T401" id="Seg_1371" n="HIAT:w" s="T400">mobi</ts>
                  <nts id="Seg_1372" n="HIAT:ip">)</nts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1375" n="HIAT:w" s="T401">dĭ</ts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1378" n="HIAT:w" s="T402">kuzan</ts>
                  <nts id="Seg_1379" n="HIAT:ip">.</nts>
                  <nts id="Seg_1380" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T406" id="Seg_1381" n="sc" s="T404">
               <ts e="T406" id="Seg_1383" n="HIAT:u" s="T404">
                  <ts e="T405" id="Seg_1385" n="HIAT:w" s="T404">Bar</ts>
                  <nts id="Seg_1386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1388" n="HIAT:w" s="T405">ibi</ts>
                  <nts id="Seg_1389" n="HIAT:ip">.</nts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T410" id="Seg_1391" n="sc" s="T407">
               <ts e="T410" id="Seg_1393" n="HIAT:u" s="T407">
                  <ts e="T408" id="Seg_1395" n="HIAT:w" s="T407">Ĭmbi</ts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1398" n="HIAT:w" s="T408">tăn</ts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1401" n="HIAT:w" s="T409">ibiel</ts>
                  <nts id="Seg_1402" n="HIAT:ip">?</nts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T417" id="Seg_1404" n="sc" s="T411">
               <ts e="T417" id="Seg_1406" n="HIAT:u" s="T411">
                  <ts e="T412" id="Seg_1408" n="HIAT:w" s="T411">Dak</ts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1411" n="HIAT:w" s="T412">măn</ts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1414" n="HIAT:w" s="T413">naga</ts>
                  <nts id="Seg_1415" n="HIAT:ip">,</nts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1418" n="HIAT:w" s="T414">a</ts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1421" n="HIAT:w" s="T415">tăn</ts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1424" n="HIAT:w" s="T416">ige</ts>
                  <nts id="Seg_1425" n="HIAT:ip">.</nts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T421" id="Seg_1427" n="sc" s="T418">
               <ts e="T421" id="Seg_1429" n="HIAT:u" s="T418">
                  <nts id="Seg_1430" n="HIAT:ip">(</nts>
                  <ts e="T419" id="Seg_1432" n="HIAT:w" s="T418">Vo</ts>
                  <nts id="Seg_1433" n="HIAT:ip">)</nts>
                  <nts id="Seg_1434" n="HIAT:ip">,</nts>
                  <nts id="Seg_1435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1437" n="HIAT:w" s="T419">măn</ts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1440" n="HIAT:w" s="T420">ibiem</ts>
                  <nts id="Seg_1441" n="HIAT:ip">.</nts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T427" id="Seg_1443" n="sc" s="T422">
               <ts e="T427" id="Seg_1445" n="HIAT:u" s="T422">
                  <ts e="T423" id="Seg_1447" n="HIAT:w" s="T422">A</ts>
                  <nts id="Seg_1448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1450" n="HIAT:w" s="T423">măn</ts>
                  <nts id="Seg_1451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1453" n="HIAT:w" s="T424">tănan</ts>
                  <nts id="Seg_1454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1456" n="HIAT:w" s="T425">ej</ts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1459" n="HIAT:w" s="T426">iləm</ts>
                  <nts id="Seg_1460" n="HIAT:ip">.</nts>
                  <nts id="Seg_1461" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T431" id="Seg_1462" n="sc" s="T428">
               <ts e="T431" id="Seg_1464" n="HIAT:u" s="T428">
                  <ts e="T429" id="Seg_1466" n="HIAT:w" s="T428">Măn</ts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1469" n="HIAT:w" s="T429">tănan</ts>
                  <nts id="Seg_1470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1472" n="HIAT:w" s="T430">iləm</ts>
                  <nts id="Seg_1473" n="HIAT:ip">.</nts>
                  <nts id="Seg_1474" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T437" id="Seg_1475" n="sc" s="T432">
               <ts e="T437" id="Seg_1477" n="HIAT:u" s="T432">
                  <ts e="T433" id="Seg_1479" n="HIAT:w" s="T432">A</ts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1482" n="HIAT:w" s="T433">moʔ</ts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1484" n="HIAT:ip">(</nts>
                  <ts e="T435" id="Seg_1486" n="HIAT:w" s="T434">nĭn</ts>
                  <nts id="Seg_1487" n="HIAT:ip">)</nts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1490" n="HIAT:w" s="T435">măna</ts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1493" n="HIAT:w" s="T436">iləl</ts>
                  <nts id="Seg_1494" n="HIAT:ip">?</nts>
                  <nts id="Seg_1495" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T442" id="Seg_1496" n="sc" s="T438">
               <ts e="T442" id="Seg_1498" n="HIAT:u" s="T438">
                  <ts e="T439" id="Seg_1500" n="HIAT:w" s="T438">Dak</ts>
                  <nts id="Seg_1501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1503" n="HIAT:w" s="T439">nĭn</ts>
                  <nts id="Seg_1504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440.tx.1" id="Seg_1506" n="HIAT:w" s="T440">na</ts>
                  <nts id="Seg_1507" n="HIAT:ip">_</nts>
                  <ts e="T441" id="Seg_1509" n="HIAT:w" s="T440.tx.1">što</ts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1512" n="HIAT:w" s="T441">ibiel</ts>
                  <nts id="Seg_1513" n="HIAT:ip">?</nts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T449" id="Seg_1515" n="sc" s="T443">
               <ts e="T449" id="Seg_1517" n="HIAT:u" s="T443">
                  <ts e="T444" id="Seg_1519" n="HIAT:w" s="T443">Măn</ts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1522" n="HIAT:w" s="T444">kunnaʔbə</ts>
                  <nts id="Seg_1523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1525" n="HIAT:w" s="T445">i</ts>
                  <nts id="Seg_1526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1528" n="HIAT:w" s="T446">bar</ts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1531" n="HIAT:w" s="T447">ĭmbim</ts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1534" n="HIAT:w" s="T448">bar</ts>
                  <nts id="Seg_1535" n="HIAT:ip">.</nts>
                  <nts id="Seg_1536" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T457" id="Seg_1537" n="sc" s="T450">
               <ts e="T457" id="Seg_1539" n="HIAT:u" s="T450">
                  <ts e="T451" id="Seg_1541" n="HIAT:w" s="T450">A</ts>
                  <nts id="Seg_1542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1544" n="HIAT:w" s="T451">măn</ts>
                  <nts id="Seg_1545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1547" n="HIAT:w" s="T452">bile</ts>
                  <nts id="Seg_1548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1550" n="HIAT:w" s="T453">turanə</ts>
                  <nts id="Seg_1551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1553" n="HIAT:w" s="T454">tănan</ts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1556" n="HIAT:w" s="T455">amnolaʔbə</ts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1558" n="HIAT:ip">(</nts>
                  <nts id="Seg_1559" n="HIAT:ip">(</nts>
                  <ats e="T457" id="Seg_1560" n="HIAT:non-pho" s="T456">…</ats>
                  <nts id="Seg_1561" n="HIAT:ip">)</nts>
                  <nts id="Seg_1562" n="HIAT:ip">)</nts>
                  <nts id="Seg_1563" n="HIAT:ip">.</nts>
                  <nts id="Seg_1564" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T459" id="Seg_1565" n="sc" s="T458">
               <ts e="T459" id="Seg_1567" n="HIAT:u" s="T458">
                  <ts e="T459" id="Seg_1569" n="HIAT:w" s="T458">Vot</ts>
                  <nts id="Seg_1570" n="HIAT:ip">.</nts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T461" id="Seg_1572" n="sc" s="T460">
               <ts e="T461" id="Seg_1574" n="HIAT:u" s="T460">
                  <nts id="Seg_1575" n="HIAT:ip">(</nts>
                  <nts id="Seg_1576" n="HIAT:ip">(</nts>
                  <ats e="T461" id="Seg_1577" n="HIAT:non-pho" s="T460">BRK</ats>
                  <nts id="Seg_1578" n="HIAT:ip">)</nts>
                  <nts id="Seg_1579" n="HIAT:ip">)</nts>
                  <nts id="Seg_1580" n="HIAT:ip">.</nts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T465" id="Seg_1582" n="sc" s="T462">
               <ts e="T465" id="Seg_1584" n="HIAT:u" s="T462">
                  <ts e="T463" id="Seg_1586" n="HIAT:w" s="T462">Amnoʔ</ts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1589" n="HIAT:w" s="T463">mănzi</ts>
                  <nts id="Seg_1590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1592" n="HIAT:w" s="T464">amorzittə</ts>
                  <nts id="Seg_1593" n="HIAT:ip">.</nts>
                  <nts id="Seg_1594" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T467" id="Seg_1595" n="sc" s="T466">
               <ts e="T467" id="Seg_1597" n="HIAT:u" s="T466">
                  <ts e="T467" id="Seg_1599" n="HIAT:w" s="T466">Oj</ts>
                  <nts id="Seg_1600" n="HIAT:ip">.</nts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T471" id="Seg_1602" n="sc" s="T468">
               <ts e="T471" id="Seg_1604" n="HIAT:u" s="T468">
                  <ts e="T469" id="Seg_1606" n="HIAT:w" s="T468">Чего</ts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1609" n="HIAT:w" s="T469">я</ts>
                  <nts id="Seg_1610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1612" n="HIAT:w" s="T470">это</ts>
                  <nts id="Seg_1613" n="HIAT:ip">.</nts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T475" id="Seg_1615" n="sc" s="T472">
               <ts e="T475" id="Seg_1617" n="HIAT:u" s="T472">
                  <ts e="T473" id="Seg_1619" n="HIAT:w" s="T472">Нет</ts>
                  <nts id="Seg_1620" n="HIAT:ip">,</nts>
                  <nts id="Seg_1621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1623" n="HIAT:w" s="T473">не</ts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1626" n="HIAT:w" s="T474">так</ts>
                  <nts id="Seg_1627" n="HIAT:ip">.</nts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T477" id="Seg_1629" n="sc" s="T476">
               <ts e="T477" id="Seg_1631" n="HIAT:u" s="T476">
                  <nts id="Seg_1632" n="HIAT:ip">(</nts>
                  <nts id="Seg_1633" n="HIAT:ip">(</nts>
                  <ats e="T477" id="Seg_1634" n="HIAT:non-pho" s="T476">BRK</ats>
                  <nts id="Seg_1635" n="HIAT:ip">)</nts>
                  <nts id="Seg_1636" n="HIAT:ip">)</nts>
                  <nts id="Seg_1637" n="HIAT:ip">.</nts>
                  <nts id="Seg_1638" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T481" id="Seg_1639" n="sc" s="T478">
               <ts e="T481" id="Seg_1641" n="HIAT:u" s="T478">
                  <ts e="T479" id="Seg_1643" n="HIAT:w" s="T478">Amnoʔ</ts>
                  <nts id="Seg_1644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1646" n="HIAT:w" s="T479">mănzi</ts>
                  <nts id="Seg_1647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1649" n="HIAT:w" s="T480">amorzittə</ts>
                  <nts id="Seg_1650" n="HIAT:ip">.</nts>
                  <nts id="Seg_1651" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T486" id="Seg_1652" n="sc" s="T482">
               <ts e="T486" id="Seg_1654" n="HIAT:u" s="T482">
                  <ts e="T483" id="Seg_1656" n="HIAT:w" s="T482">Da</ts>
                  <nts id="Seg_1657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1659" n="HIAT:w" s="T483">ĭmbi</ts>
                  <nts id="Seg_1660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1662" n="HIAT:w" s="T484">tăn</ts>
                  <nts id="Seg_1663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1665" n="HIAT:w" s="T485">molat</ts>
                  <nts id="Seg_1666" n="HIAT:ip">?</nts>
                  <nts id="Seg_1667" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T494" id="Seg_1668" n="sc" s="T487">
               <ts e="T494" id="Seg_1670" n="HIAT:u" s="T487">
                  <ts e="T488" id="Seg_1672" n="HIAT:w" s="T487">Da</ts>
                  <nts id="Seg_1673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1675" n="HIAT:w" s="T488">măn</ts>
                  <nts id="Seg_1676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1677" n="HIAT:ip">(</nts>
                  <ts e="T490" id="Seg_1679" n="HIAT:w" s="T489">molat</ts>
                  <nts id="Seg_1680" n="HIAT:ip">)</nts>
                  <nts id="Seg_1681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1683" n="HIAT:w" s="T490">ipek</ts>
                  <nts id="Seg_1684" n="HIAT:ip">,</nts>
                  <nts id="Seg_1685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1687" n="HIAT:w" s="T491">uja</ts>
                  <nts id="Seg_1688" n="HIAT:ip">,</nts>
                  <nts id="Seg_1689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1691" n="HIAT:w" s="T492">i</ts>
                  <nts id="Seg_1692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1694" n="HIAT:w" s="T493">süt</ts>
                  <nts id="Seg_1695" n="HIAT:ip">.</nts>
                  <nts id="Seg_1696" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T496" id="Seg_1697" n="sc" s="T495">
               <ts e="T496" id="Seg_1699" n="HIAT:u" s="T495">
                  <ts e="T496" id="Seg_1701" n="HIAT:w" s="T495">Sĭrepne</ts>
                  <nts id="Seg_1702" n="HIAT:ip">.</nts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T498" id="Seg_1704" n="sc" s="T497">
               <ts e="T498" id="Seg_1706" n="HIAT:u" s="T497">
                  <ts e="T498" id="Seg_1708" n="HIAT:w" s="T497">Amdə</ts>
                  <nts id="Seg_1709" n="HIAT:ip">.</nts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T505" id="Seg_1711" n="sc" s="T499">
               <ts e="T505" id="Seg_1713" n="HIAT:u" s="T499">
                  <ts e="T500" id="Seg_1715" n="HIAT:w" s="T499">Dak</ts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1718" n="HIAT:w" s="T500">măn</ts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1721" n="HIAT:w" s="T501">bɨ</ts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1724" n="HIAT:w" s="T502">kajak</ts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1727" n="HIAT:w" s="T503">amorliam</ts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1730" n="HIAT:w" s="T504">bɨ</ts>
                  <nts id="Seg_1731" n="HIAT:ip">.</nts>
                  <nts id="Seg_1732" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T513" id="Seg_1733" n="sc" s="T506">
               <ts e="T513" id="Seg_1735" n="HIAT:u" s="T506">
                  <ts e="T507" id="Seg_1737" n="HIAT:w" s="T506">Nu</ts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1740" n="HIAT:w" s="T507">i</ts>
                  <nts id="Seg_1741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1743" n="HIAT:w" s="T508">kajak</ts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1746" n="HIAT:w" s="T509">măn</ts>
                  <nts id="Seg_1747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1749" n="HIAT:w" s="T510">mĭlem</ts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1752" n="HIAT:w" s="T511">tănan</ts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1755" n="HIAT:w" s="T512">amorzittə</ts>
                  <nts id="Seg_1756" n="HIAT:ip">.</nts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T517" id="Seg_1758" n="sc" s="T514">
               <ts e="T517" id="Seg_1760" n="HIAT:u" s="T514">
                  <ts e="T515" id="Seg_1762" n="HIAT:w" s="T514">Jakše</ts>
                  <nts id="Seg_1763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1765" n="HIAT:w" s="T515">tolʼkă</ts>
                  <nts id="Seg_1766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1768" n="HIAT:w" s="T516">amoraʔ</ts>
                  <nts id="Seg_1769" n="HIAT:ip">.</nts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T522" id="Seg_1771" n="sc" s="T518">
               <ts e="T522" id="Seg_1773" n="HIAT:u" s="T518">
                  <nts id="Seg_1774" n="HIAT:ip">(</nts>
                  <ts e="T519" id="Seg_1776" n="HIAT:w" s="T518">Dö</ts>
                  <nts id="Seg_1777" n="HIAT:ip">)</nts>
                  <nts id="Seg_1778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1780" n="HIAT:w" s="T519">dak</ts>
                  <nts id="Seg_1781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1783" n="HIAT:w" s="T520">măn</ts>
                  <nts id="Seg_1784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1785" n="HIAT:ip">(</nts>
                  <ts e="T522" id="Seg_1787" n="HIAT:w" s="T521">amorlam</ts>
                  <nts id="Seg_1788" n="HIAT:ip">)</nts>
                  <nts id="Seg_1789" n="HIAT:ip">.</nts>
                  <nts id="Seg_1790" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T526" id="Seg_1791" n="sc" s="T523">
               <ts e="T526" id="Seg_1793" n="HIAT:u" s="T523">
                  <nts id="Seg_1794" n="HIAT:ip">(</nts>
                  <ts e="T524" id="Seg_1796" n="HIAT:w" s="T523">Tăŋ=</ts>
                  <nts id="Seg_1797" n="HIAT:ip">)</nts>
                  <nts id="Seg_1798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1800" n="HIAT:w" s="T524">Tăŋ</ts>
                  <nts id="Seg_1801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1803" n="HIAT:w" s="T525">amorlam</ts>
                  <nts id="Seg_1804" n="HIAT:ip">.</nts>
                  <nts id="Seg_1805" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T529" id="Seg_1806" n="sc" s="T527">
               <ts e="T529" id="Seg_1808" n="HIAT:u" s="T527">
                  <ts e="T528" id="Seg_1810" n="HIAT:w" s="T527">Jakše</ts>
                  <nts id="Seg_1811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1813" n="HIAT:w" s="T528">amorliam</ts>
                  <nts id="Seg_1814" n="HIAT:ip">.</nts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T531" id="Seg_1816" n="sc" s="T530">
               <ts e="T531" id="Seg_1818" n="HIAT:u" s="T530">
                  <ts e="T531" id="Seg_1820" n="HIAT:w" s="T530">Vo</ts>
                  <nts id="Seg_1821" n="HIAT:ip">.</nts>
                  <nts id="Seg_1822" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T533" id="Seg_1823" n="sc" s="T532">
               <ts e="T533" id="Seg_1825" n="HIAT:u" s="T532">
                  <nts id="Seg_1826" n="HIAT:ip">(</nts>
                  <nts id="Seg_1827" n="HIAT:ip">(</nts>
                  <ats e="T533" id="Seg_1828" n="HIAT:non-pho" s="T532">BRK</ats>
                  <nts id="Seg_1829" n="HIAT:ip">)</nts>
                  <nts id="Seg_1830" n="HIAT:ip">)</nts>
                  <nts id="Seg_1831" n="HIAT:ip">.</nts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T542" id="Seg_1833" n="sc" s="T534">
               <ts e="T542" id="Seg_1835" n="HIAT:u" s="T534">
                  <nts id="Seg_1836" n="HIAT:ip">(</nts>
                  <ts e="T535" id="Seg_1838" n="HIAT:w" s="T534">Mo</ts>
                  <nts id="Seg_1839" n="HIAT:ip">)</nts>
                  <nts id="Seg_1840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1842" n="HIAT:w" s="T535">tăn</ts>
                  <nts id="Seg_1843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1845" n="HIAT:w" s="T536">măn</ts>
                  <nts id="Seg_1846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1848" n="HIAT:w" s="T537">eʔbdem</ts>
                  <nts id="Seg_1849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1851" n="HIAT:w" s="T538">bar</ts>
                  <nts id="Seg_1852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1854" n="HIAT:w" s="T539">bos</ts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1856" n="HIAT:ip">(</nts>
                  <ts e="T541" id="Seg_1858" n="HIAT:w" s="T540">udanə</ts>
                  <nts id="Seg_1859" n="HIAT:ip">)</nts>
                  <nts id="Seg_1860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_1862" n="HIAT:w" s="T541">ibiel</ts>
                  <nts id="Seg_1863" n="HIAT:ip">?</nts>
                  <nts id="Seg_1864" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T550" id="Seg_1865" n="sc" s="T543">
               <ts e="T550" id="Seg_1867" n="HIAT:u" s="T543">
                  <ts e="T544" id="Seg_1869" n="HIAT:w" s="T543">Vedʼ</ts>
                  <nts id="Seg_1870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_1872" n="HIAT:w" s="T544">tăn</ts>
                  <nts id="Seg_1873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_1875" n="HIAT:w" s="T545">udal</ts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_1878" n="HIAT:w" s="T546">urgo</ts>
                  <nts id="Seg_1879" n="HIAT:ip">,</nts>
                  <nts id="Seg_1880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1882" n="HIAT:w" s="T547">a</ts>
                  <nts id="Seg_1883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_1885" n="HIAT:w" s="T548">măn</ts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_1888" n="HIAT:w" s="T549">idiʼije</ts>
                  <nts id="Seg_1889" n="HIAT:ip">.</nts>
                  <nts id="Seg_1890" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T554" id="Seg_1891" n="sc" s="T551">
               <ts e="T554" id="Seg_1893" n="HIAT:u" s="T551">
                  <ts e="T552" id="Seg_1895" n="HIAT:w" s="T551">Mo</ts>
                  <nts id="Seg_1896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_1898" n="HIAT:w" s="T552">dereʔ</ts>
                  <nts id="Seg_1899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_1901" n="HIAT:w" s="T553">molal</ts>
                  <nts id="Seg_1902" n="HIAT:ip">?</nts>
                  <nts id="Seg_1903" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T556" id="Seg_1904" n="sc" s="T555">
               <ts e="T556" id="Seg_1906" n="HIAT:u" s="T555">
                  <ts e="T556" id="Seg_1908" n="HIAT:w" s="T555">Bile</ts>
                  <nts id="Seg_1909" n="HIAT:ip">.</nts>
                  <nts id="Seg_1910" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T560" id="Seg_1911" n="sc" s="T557">
               <ts e="T560" id="Seg_1913" n="HIAT:u" s="T557">
                  <ts e="T558" id="Seg_1915" n="HIAT:w" s="T557">Ej</ts>
                  <nts id="Seg_1916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_1918" n="HIAT:w" s="T558">jakše</ts>
                  <nts id="Seg_1919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_1921" n="HIAT:w" s="T559">dereʔ</ts>
                  <nts id="Seg_1922" n="HIAT:ip">.</nts>
                  <nts id="Seg_1923" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T562" id="Seg_1924" n="sc" s="T561">
               <ts e="T562" id="Seg_1926" n="HIAT:u" s="T561">
                  <ts e="T562" id="Seg_1928" n="HIAT:w" s="T561">Vot</ts>
                  <nts id="Seg_1929" n="HIAT:ip">.</nts>
                  <nts id="Seg_1930" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T564" id="Seg_1931" n="sc" s="T563">
               <ts e="T564" id="Seg_1933" n="HIAT:u" s="T563">
                  <nts id="Seg_1934" n="HIAT:ip">(</nts>
                  <nts id="Seg_1935" n="HIAT:ip">(</nts>
                  <ats e="T564" id="Seg_1936" n="HIAT:non-pho" s="T563">BRK</ats>
                  <nts id="Seg_1937" n="HIAT:ip">)</nts>
                  <nts id="Seg_1938" n="HIAT:ip">)</nts>
                  <nts id="Seg_1939" n="HIAT:ip">.</nts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T568" id="Seg_1941" n="sc" s="T565">
               <ts e="T568" id="Seg_1943" n="HIAT:u" s="T565">
                  <ts e="T566" id="Seg_1945" n="HIAT:w" s="T565">Măn</ts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_1948" n="HIAT:w" s="T566">tăŋ</ts>
                  <nts id="Seg_1949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1950" n="HIAT:ip">(</nts>
                  <ts e="T568" id="Seg_1952" n="HIAT:w" s="T567">ĭzemneʔbiem</ts>
                  <nts id="Seg_1953" n="HIAT:ip">)</nts>
                  <nts id="Seg_1954" n="HIAT:ip">.</nts>
                  <nts id="Seg_1955" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T571" id="Seg_1956" n="sc" s="T569">
               <ts e="T571" id="Seg_1958" n="HIAT:u" s="T569">
                  <ts e="T570" id="Seg_1960" n="HIAT:w" s="T569">Udazambə</ts>
                  <nts id="Seg_1961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_1963" n="HIAT:w" s="T570">ĭzəmniet</ts>
                  <nts id="Seg_1964" n="HIAT:ip">.</nts>
                  <nts id="Seg_1965" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T576" id="Seg_1966" n="sc" s="T572">
               <ts e="T576" id="Seg_1968" n="HIAT:u" s="T572">
                  <ts e="T573" id="Seg_1970" n="HIAT:w" s="T572">I</ts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_1973" n="HIAT:w" s="T573">ujubə</ts>
                  <nts id="Seg_1974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_1976" n="HIAT:w" s="T574">bar</ts>
                  <nts id="Seg_1977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_1979" n="HIAT:w" s="T575">ĭzemneʔbəliet</ts>
                  <nts id="Seg_1980" n="HIAT:ip">.</nts>
                  <nts id="Seg_1981" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T580" id="Seg_1982" n="sc" s="T577">
               <ts e="T580" id="Seg_1984" n="HIAT:u" s="T577">
                  <ts e="T578" id="Seg_1986" n="HIAT:w" s="T577">Ădnakă</ts>
                  <nts id="Seg_1987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_1989" n="HIAT:w" s="T578">măn</ts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1991" n="HIAT:ip">(</nts>
                  <ts e="T580" id="Seg_1993" n="HIAT:w" s="T579">külambil</ts>
                  <nts id="Seg_1994" n="HIAT:ip">)</nts>
                  <nts id="Seg_1995" n="HIAT:ip">.</nts>
                  <nts id="Seg_1996" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T585" id="Seg_1997" n="sc" s="T581">
               <ts e="T585" id="Seg_1999" n="HIAT:u" s="T581">
                  <ts e="T582" id="Seg_2001" n="HIAT:w" s="T581">Dʼok</ts>
                  <nts id="Seg_2002" n="HIAT:ip">,</nts>
                  <nts id="Seg_2003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2005" n="HIAT:w" s="T582">nʼe</ts>
                  <nts id="Seg_2006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2008" n="HIAT:w" s="T583">nada</ts>
                  <nts id="Seg_2009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2011" n="HIAT:w" s="T584">külamzittə</ts>
                  <nts id="Seg_2012" n="HIAT:ip">.</nts>
                  <nts id="Seg_2013" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T588" id="Seg_2014" n="sc" s="T586">
               <ts e="T588" id="Seg_2016" n="HIAT:u" s="T586">
                  <ts e="T587" id="Seg_2018" n="HIAT:w" s="T586">Nada</ts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2021" n="HIAT:w" s="T587">amnozittə</ts>
                  <nts id="Seg_2022" n="HIAT:ip">.</nts>
                  <nts id="Seg_2023" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T591" id="Seg_2024" n="sc" s="T589">
               <ts e="T591" id="Seg_2026" n="HIAT:u" s="T589">
                  <ts e="T590" id="Seg_2028" n="HIAT:w" s="T589">Amnozittə</ts>
                  <nts id="Seg_2029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2031" n="HIAT:w" s="T590">jakše</ts>
                  <nts id="Seg_2032" n="HIAT:ip">.</nts>
                  <nts id="Seg_2033" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T595" id="Seg_2034" n="sc" s="T592">
               <ts e="T595" id="Seg_2036" n="HIAT:u" s="T592">
                  <ts e="T593" id="Seg_2038" n="HIAT:w" s="T592">A</ts>
                  <nts id="Seg_2039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2040" n="HIAT:ip">(</nts>
                  <ts e="T594" id="Seg_2042" n="HIAT:w" s="T593">külamzittə</ts>
                  <nts id="Seg_2043" n="HIAT:ip">)</nts>
                  <nts id="Seg_2044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2046" n="HIAT:w" s="T594">bile</ts>
                  <nts id="Seg_2047" n="HIAT:ip">.</nts>
                  <nts id="Seg_2048" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T598" id="Seg_2049" n="sc" s="T596">
               <ts e="T598" id="Seg_2051" n="HIAT:u" s="T596">
                  <ts e="T597" id="Seg_2053" n="HIAT:w" s="T596">Tăŋ</ts>
                  <nts id="Seg_2054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2056" n="HIAT:w" s="T597">bile</ts>
                  <nts id="Seg_2057" n="HIAT:ip">.</nts>
                  <nts id="Seg_2058" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T600" id="Seg_2059" n="sc" s="T599">
               <ts e="T600" id="Seg_2061" n="HIAT:u" s="T599">
                  <ts e="T600" id="Seg_2063" n="HIAT:w" s="T599">Vot</ts>
                  <nts id="Seg_2064" n="HIAT:ip">.</nts>
                  <nts id="Seg_2065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T602" id="Seg_2066" n="sc" s="T601">
               <ts e="T602" id="Seg_2068" n="HIAT:u" s="T601">
                  <nts id="Seg_2069" n="HIAT:ip">(</nts>
                  <nts id="Seg_2070" n="HIAT:ip">(</nts>
                  <ats e="T602" id="Seg_2071" n="HIAT:non-pho" s="T601">BRK</ats>
                  <nts id="Seg_2072" n="HIAT:ip">)</nts>
                  <nts id="Seg_2073" n="HIAT:ip">)</nts>
                  <nts id="Seg_2074" n="HIAT:ip">.</nts>
                  <nts id="Seg_2075" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T606" id="Seg_2076" n="sc" s="T603">
               <ts e="T606" id="Seg_2078" n="HIAT:u" s="T603">
                  <ts e="T604" id="Seg_2080" n="HIAT:w" s="T603">Šide</ts>
                  <nts id="Seg_2081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2083" n="HIAT:w" s="T604">nʼi</ts>
                  <nts id="Seg_2084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2086" n="HIAT:w" s="T605">amnobiʔi</ts>
                  <nts id="Seg_2087" n="HIAT:ip">.</nts>
                  <nts id="Seg_2088" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T614" id="Seg_2089" n="sc" s="T607">
               <ts e="T614" id="Seg_2091" n="HIAT:u" s="T607">
                  <ts e="T608" id="Seg_2093" n="HIAT:w" s="T607">Dĭttəgəš</ts>
                  <nts id="Seg_2094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2096" n="HIAT:w" s="T608">dĭ</ts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2099" n="HIAT:w" s="T609">nʼi</ts>
                  <nts id="Seg_2100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2102" n="HIAT:w" s="T610">gibərdə</ts>
                  <nts id="Seg_2103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2105" n="HIAT:w" s="T611">ari</ts>
                  <nts id="Seg_2106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2108" n="HIAT:w" s="T612">dʼürzittə</ts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2111" n="HIAT:w" s="T613">molat</ts>
                  <nts id="Seg_2112" n="HIAT:ip">.</nts>
                  <nts id="Seg_2113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T619" id="Seg_2114" n="sc" s="T615">
               <ts e="T619" id="Seg_2116" n="HIAT:u" s="T615">
                  <ts e="T616" id="Seg_2118" n="HIAT:w" s="T615">A</ts>
                  <nts id="Seg_2119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2121" n="HIAT:w" s="T616">oʔb</ts>
                  <nts id="Seg_2122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2124" n="HIAT:w" s="T617">turagənən</ts>
                  <nts id="Seg_2125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2127" n="HIAT:w" s="T618">amnolaʔpliet</ts>
                  <nts id="Seg_2128" n="HIAT:ip">.</nts>
                  <nts id="Seg_2129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T626" id="Seg_2130" n="sc" s="T620">
               <ts e="T626" id="Seg_2132" n="HIAT:u" s="T620">
                  <ts e="T621" id="Seg_2134" n="HIAT:w" s="T620">Nu</ts>
                  <nts id="Seg_2135" n="HIAT:ip">,</nts>
                  <nts id="Seg_2136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2138" n="HIAT:w" s="T621">mĭlat:</ts>
                  <nts id="Seg_2139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2141" n="HIAT:w" s="T622">tănan</ts>
                  <nts id="Seg_2142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2144" n="HIAT:w" s="T623">ine</ts>
                  <nts id="Seg_2145" n="HIAT:ip">,</nts>
                  <nts id="Seg_2146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2148" n="HIAT:w" s="T624">măna</ts>
                  <nts id="Seg_2149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2151" n="HIAT:w" s="T625">tüžöj</ts>
                  <nts id="Seg_2152" n="HIAT:ip">.</nts>
                  <nts id="Seg_2153" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T634" id="Seg_2154" n="sc" s="T627">
               <ts e="T634" id="Seg_2156" n="HIAT:u" s="T627">
                  <ts e="T628" id="Seg_2158" n="HIAT:w" s="T627">Dʼok</ts>
                  <nts id="Seg_2159" n="HIAT:ip">,</nts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2162" n="HIAT:w" s="T628">tăn</ts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2165" n="HIAT:w" s="T629">it</ts>
                  <nts id="Seg_2166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2168" n="HIAT:w" s="T630">ine</ts>
                  <nts id="Seg_2169" n="HIAT:ip">,</nts>
                  <nts id="Seg_2170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2172" n="HIAT:w" s="T631">a</ts>
                  <nts id="Seg_2173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2175" n="HIAT:w" s="T632">măn</ts>
                  <nts id="Seg_2176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2178" n="HIAT:w" s="T633">tüžöj</ts>
                  <nts id="Seg_2179" n="HIAT:ip">.</nts>
                  <nts id="Seg_2180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T637" id="Seg_2181" n="sc" s="T635">
               <ts e="T637" id="Seg_2183" n="HIAT:u" s="T635">
                  <ts e="T636" id="Seg_2185" n="HIAT:w" s="T635">O</ts>
                  <nts id="Seg_2186" n="HIAT:ip">,</nts>
                  <nts id="Seg_2187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2189" n="HIAT:w" s="T636">enejdənə</ts>
                  <nts id="Seg_2190" n="HIAT:ip">!</nts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T640" id="Seg_2192" n="sc" s="T638">
               <ts e="T640" id="Seg_2194" n="HIAT:u" s="T638">
                  <ts e="T639" id="Seg_2196" n="HIAT:w" s="T638">Mo</ts>
                  <nts id="Seg_2197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2199" n="HIAT:w" s="T639">dereʔ</ts>
                  <nts id="Seg_2200" n="HIAT:ip">?</nts>
                  <nts id="Seg_2201" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T644" id="Seg_2202" n="sc" s="T641">
               <ts e="T644" id="Seg_2204" n="HIAT:u" s="T641">
                  <ts e="T642" id="Seg_2206" n="HIAT:w" s="T641">Nu</ts>
                  <nts id="Seg_2207" n="HIAT:ip">,</nts>
                  <nts id="Seg_2208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2210" n="HIAT:w" s="T642">tăn</ts>
                  <nts id="Seg_2211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2213" n="HIAT:w" s="T643">koško</ts>
                  <nts id="Seg_2214" n="HIAT:ip">!</nts>
                  <nts id="Seg_2215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T646" id="Seg_2216" n="sc" s="T645">
               <ts e="T646" id="Seg_2218" n="HIAT:u" s="T645">
                  <ts e="T646" id="Seg_2220" n="HIAT:w" s="T645">Eneidənəl</ts>
                  <nts id="Seg_2221" n="HIAT:ip">.</nts>
                  <nts id="Seg_2222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T650" id="Seg_2223" n="sc" s="T647">
               <ts e="T650" id="Seg_2225" n="HIAT:u" s="T647">
                  <ts e="T648" id="Seg_2227" n="HIAT:w" s="T647">Mo</ts>
                  <nts id="Seg_2228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2230" n="HIAT:w" s="T648">dereʔ</ts>
                  <nts id="Seg_2231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2233" n="HIAT:w" s="T649">kudonzlial</ts>
                  <nts id="Seg_2234" n="HIAT:ip">?</nts>
                  <nts id="Seg_2235" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T656" id="Seg_2236" n="sc" s="T651">
               <ts e="T656" id="Seg_2238" n="HIAT:u" s="T651">
                  <ts e="T652" id="Seg_2240" n="HIAT:w" s="T651">Dak</ts>
                  <nts id="Seg_2241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2243" n="HIAT:w" s="T652">nada</ts>
                  <nts id="Seg_2244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2246" n="HIAT:w" s="T653">dereʔ</ts>
                  <nts id="Seg_2247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2249" n="HIAT:w" s="T654">kudonzittə</ts>
                  <nts id="Seg_2250" n="HIAT:ip">,</nts>
                  <nts id="Seg_2251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2253" n="HIAT:w" s="T655">bile</ts>
                  <nts id="Seg_2254" n="HIAT:ip">.</nts>
                  <nts id="Seg_2255" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T666" id="Seg_2256" n="sc" s="T657">
               <ts e="T666" id="Seg_2258" n="HIAT:u" s="T657">
                  <ts e="T658" id="Seg_2260" n="HIAT:w" s="T657">A</ts>
                  <nts id="Seg_2261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2263" n="HIAT:w" s="T658">măna</ts>
                  <nts id="Seg_2264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2266" n="HIAT:w" s="T659">vedʼ</ts>
                  <nts id="Seg_2267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2269" n="HIAT:w" s="T660">abam</ts>
                  <nts id="Seg_2270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2272" n="HIAT:w" s="T661">mĭbie</ts>
                  <nts id="Seg_2273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2275" n="HIAT:w" s="T662">ine</ts>
                  <nts id="Seg_2276" n="HIAT:ip">,</nts>
                  <nts id="Seg_2277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2279" n="HIAT:w" s="T663">a</ts>
                  <nts id="Seg_2280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2282" n="HIAT:w" s="T664">tănan</ts>
                  <nts id="Seg_2283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2285" n="HIAT:w" s="T665">tüžöj</ts>
                  <nts id="Seg_2286" n="HIAT:ip">.</nts>
                  <nts id="Seg_2287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T675" id="Seg_2288" n="sc" s="T667">
               <ts e="T675" id="Seg_2290" n="HIAT:u" s="T667">
                  <nts id="Seg_2291" n="HIAT:ip">(</nts>
                  <ts e="T667.tx.1" id="Seg_2293" n="HIAT:w" s="T667">Măbi</ts>
                  <nts id="Seg_2294" n="HIAT:ip">)</nts>
                  <ts e="T668" id="Seg_2296" n="HIAT:w" s="T667.tx.1">:</ts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2299" n="HIAT:w" s="T668">tăn</ts>
                  <nts id="Seg_2300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2302" n="HIAT:w" s="T669">it</ts>
                  <nts id="Seg_2303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2305" n="HIAT:w" s="T670">ine</ts>
                  <nts id="Seg_2306" n="HIAT:ip">,</nts>
                  <nts id="Seg_2307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2309" n="HIAT:w" s="T671">a</ts>
                  <nts id="Seg_2310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2312" n="HIAT:w" s="T672">tăn</ts>
                  <nts id="Seg_2313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2315" n="HIAT:w" s="T673">it</ts>
                  <nts id="Seg_2316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2318" n="HIAT:w" s="T674">tüžöj</ts>
                  <nts id="Seg_2319" n="HIAT:ip">.</nts>
                  <nts id="Seg_2320" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T679" id="Seg_2321" n="sc" s="T676">
               <ts e="T679" id="Seg_2323" n="HIAT:u" s="T676">
                  <ts e="T677" id="Seg_2325" n="HIAT:w" s="T676">A</ts>
                  <nts id="Seg_2326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2328" n="HIAT:w" s="T677">tura</ts>
                  <nts id="Seg_2329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2331" n="HIAT:w" s="T678">tănan</ts>
                  <nts id="Seg_2332" n="HIAT:ip">.</nts>
                  <nts id="Seg_2333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T685" id="Seg_2334" n="sc" s="T680">
               <ts e="T685" id="Seg_2336" n="HIAT:u" s="T680">
                  <ts e="T681" id="Seg_2338" n="HIAT:w" s="T680">I</ts>
                  <nts id="Seg_2339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2341" n="HIAT:w" s="T681">tura</ts>
                  <nts id="Seg_2342" n="HIAT:ip">,</nts>
                  <nts id="Seg_2343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2345" n="HIAT:w" s="T682">i</ts>
                  <nts id="Seg_2346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2348" n="HIAT:w" s="T683">tüžöj</ts>
                  <nts id="Seg_2349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2351" n="HIAT:w" s="T684">tănan</ts>
                  <nts id="Seg_2352" n="HIAT:ip">.</nts>
                  <nts id="Seg_2353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T691" id="Seg_2354" n="sc" s="T686">
               <ts e="T691" id="Seg_2356" n="HIAT:u" s="T686">
                  <ts e="T687" id="Seg_2358" n="HIAT:w" s="T686">A</ts>
                  <nts id="Seg_2359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2361" n="HIAT:w" s="T687">tüj</ts>
                  <nts id="Seg_2362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2364" n="HIAT:w" s="T688">tüžöj</ts>
                  <nts id="Seg_2365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2367" n="HIAT:w" s="T689">ej</ts>
                  <nts id="Seg_2368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2370" n="HIAT:w" s="T690">iliet</ts>
                  <nts id="Seg_2371" n="HIAT:ip">.</nts>
                  <nts id="Seg_2372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T694" id="Seg_2373" n="sc" s="T692">
               <ts e="T694" id="Seg_2375" n="HIAT:u" s="T692">
                  <ts e="T693" id="Seg_2377" n="HIAT:w" s="T692">Ну</ts>
                  <nts id="Seg_2378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2380" n="HIAT:w" s="T693">вот</ts>
                  <nts id="Seg_2381" n="HIAT:ip">.</nts>
                  <nts id="Seg_2382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T696" id="Seg_2383" n="sc" s="T695">
               <ts e="T696" id="Seg_2385" n="HIAT:u" s="T695">
                  <nts id="Seg_2386" n="HIAT:ip">(</nts>
                  <nts id="Seg_2387" n="HIAT:ip">(</nts>
                  <ats e="T696" id="Seg_2388" n="HIAT:non-pho" s="T695">BRK</ats>
                  <nts id="Seg_2389" n="HIAT:ip">)</nts>
                  <nts id="Seg_2390" n="HIAT:ip">)</nts>
                  <nts id="Seg_2391" n="HIAT:ip">.</nts>
                  <nts id="Seg_2392" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T698" id="Seg_2393" n="sc" s="T697">
               <ts e="T698" id="Seg_2395" n="HIAT:u" s="T697">
                  <ts e="T698" id="Seg_2397" n="HIAT:w" s="T697">Ой</ts>
                  <nts id="Seg_2398" n="HIAT:ip">.</nts>
                  <nts id="Seg_2399" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T702" id="Seg_2400" n="sc" s="T699">
               <ts e="T702" id="Seg_2402" n="HIAT:u" s="T699">
                  <ts e="T700" id="Seg_2404" n="HIAT:w" s="T699">Bar</ts>
                  <nts id="Seg_2405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2407" n="HIAT:w" s="T700">gĭda</ts>
                  <nts id="Seg_2408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2410" n="HIAT:w" s="T701">bar</ts>
                  <nts id="Seg_2411" n="HIAT:ip">.</nts>
                  <nts id="Seg_2412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T709" id="Seg_2413" n="sc" s="T703">
               <ts e="T709" id="Seg_2415" n="HIAT:u" s="T703">
                  <ts e="T704" id="Seg_2417" n="HIAT:w" s="T703">Bar</ts>
                  <nts id="Seg_2418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2420" n="HIAT:w" s="T704">il</ts>
                  <nts id="Seg_2421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2422" n="HIAT:ip">(</nts>
                  <ts e="T706" id="Seg_2424" n="HIAT:w" s="T705">ur-</ts>
                  <nts id="Seg_2425" n="HIAT:ip">)</nts>
                  <nts id="Seg_2426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2428" n="HIAT:w" s="T706">urgo</ts>
                  <nts id="Seg_2429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2431" n="HIAT:w" s="T707">il</ts>
                  <nts id="Seg_2432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2433" n="HIAT:ip">(</nts>
                  <ts e="T709" id="Seg_2435" n="HIAT:w" s="T708">ibiʔjə</ts>
                  <nts id="Seg_2436" n="HIAT:ip">)</nts>
                  <nts id="Seg_2437" n="HIAT:ip">.</nts>
                  <nts id="Seg_2438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T713" id="Seg_2439" n="sc" s="T710">
               <ts e="T713" id="Seg_2441" n="HIAT:u" s="T710">
                  <ts e="T711" id="Seg_2443" n="HIAT:w" s="T710">Măn</ts>
                  <nts id="Seg_2444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2446" n="HIAT:w" s="T711">ilbə</ts>
                  <nts id="Seg_2447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2448" n="HIAT:ip">(</nts>
                  <ts e="T713" id="Seg_2450" n="HIAT:w" s="T712">ibiʔjə</ts>
                  <nts id="Seg_2451" n="HIAT:ip">)</nts>
                  <nts id="Seg_2452" n="HIAT:ip">.</nts>
                  <nts id="Seg_2453" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T716" id="Seg_2454" n="sc" s="T714">
               <ts e="T716" id="Seg_2456" n="HIAT:u" s="T714">
                  <ts e="T715" id="Seg_2458" n="HIAT:w" s="T714">I</ts>
                  <nts id="Seg_2459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2461" n="HIAT:w" s="T715">gibər</ts>
                  <nts id="Seg_2462" n="HIAT:ip">?</nts>
                  <nts id="Seg_2463" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T720" id="Seg_2464" n="sc" s="T717">
               <ts e="T720" id="Seg_2466" n="HIAT:u" s="T717">
                  <ts e="T718" id="Seg_2468" n="HIAT:w" s="T717">Эх</ts>
                  <nts id="Seg_2469" n="HIAT:ip">,</nts>
                  <nts id="Seg_2470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2472" n="HIAT:w" s="T718">gibər</ts>
                  <nts id="Seg_2473" n="HIAT:ip">,</nts>
                  <nts id="Seg_2474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2476" n="HIAT:w" s="T719">gibər</ts>
                  <nts id="Seg_2477" n="HIAT:ip">.</nts>
                  <nts id="Seg_2478" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T724" id="Seg_2479" n="sc" s="T721">
               <ts e="T724" id="Seg_2481" n="HIAT:u" s="T721">
                  <ts e="T722" id="Seg_2483" n="HIAT:w" s="T721">Dĭzeŋ</ts>
                  <nts id="Seg_2484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2486" n="HIAT:w" s="T722">dʼörlapliet</ts>
                  <nts id="Seg_2487" n="HIAT:ip">,</nts>
                  <nts id="Seg_2488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2490" n="HIAT:w" s="T723">dʼörlapliet</ts>
                  <nts id="Seg_2491" n="HIAT:ip">.</nts>
                  <nts id="Seg_2492" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T730" id="Seg_2493" n="sc" s="T725">
               <ts e="T730" id="Seg_2495" n="HIAT:u" s="T725">
                  <ts e="T726" id="Seg_2497" n="HIAT:w" s="T725">Bos</ts>
                  <nts id="Seg_2498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2500" n="HIAT:w" s="T726">turagəʔ</ts>
                  <nts id="Seg_2501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2503" n="HIAT:w" s="T727">dĭzem</ts>
                  <nts id="Seg_2504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2506" n="HIAT:w" s="T728">bar</ts>
                  <nts id="Seg_2507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2509" n="HIAT:w" s="T729">ibiʔjə</ts>
                  <nts id="Seg_2510" n="HIAT:ip">.</nts>
                  <nts id="Seg_2511" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T734" id="Seg_2512" n="sc" s="T731">
               <ts e="T734" id="Seg_2514" n="HIAT:u" s="T731">
                  <ts e="T732" id="Seg_2516" n="HIAT:w" s="T731">A</ts>
                  <nts id="Seg_2517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2519" n="HIAT:w" s="T732">ĭmbi</ts>
                  <nts id="Seg_2520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2522" n="HIAT:w" s="T733">ibiʔi</ts>
                  <nts id="Seg_2523" n="HIAT:ip">?</nts>
                  <nts id="Seg_2524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T737" id="Seg_2525" n="sc" s="T735">
               <ts e="T737" id="Seg_2527" n="HIAT:u" s="T735">
                  <ts e="T736" id="Seg_2529" n="HIAT:w" s="T735">Dak</ts>
                  <nts id="Seg_2530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2532" n="HIAT:w" s="T736">ĭmbi</ts>
                  <nts id="Seg_2533" n="HIAT:ip">.</nts>
                  <nts id="Seg_2534" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T739" id="Seg_2535" n="sc" s="T738">
               <ts e="T739" id="Seg_2537" n="HIAT:u" s="T738">
                  <ts e="T739" id="Seg_2539" n="HIAT:w" s="T738">Kudonzəbiʔi</ts>
                  <nts id="Seg_2540" n="HIAT:ip">.</nts>
                  <nts id="Seg_2541" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T745" id="Seg_2542" n="sc" s="T740">
               <ts e="T745" id="Seg_2544" n="HIAT:u" s="T740">
                  <ts e="T741" id="Seg_2546" n="HIAT:w" s="T740">I</ts>
                  <nts id="Seg_2547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2549" n="HIAT:w" s="T741">bar</ts>
                  <nts id="Seg_2550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2552" n="HIAT:w" s="T742">gĭda</ts>
                  <nts id="Seg_2553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2555" n="HIAT:w" s="T743">bar</ts>
                  <nts id="Seg_2556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2558" n="HIAT:w" s="T744">măllaplieʔi</ts>
                  <nts id="Seg_2559" n="HIAT:ip">.</nts>
                  <nts id="Seg_2560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T750" id="Seg_2561" n="sc" s="T746">
               <ts e="T750" id="Seg_2563" n="HIAT:u" s="T746">
                  <ts e="T747" id="Seg_2565" n="HIAT:w" s="T746">Vot</ts>
                  <nts id="Seg_2566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2568" n="HIAT:w" s="T747">dĭzem</ts>
                  <nts id="Seg_2569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2571" n="HIAT:w" s="T748">i</ts>
                  <nts id="Seg_2572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_2574" n="HIAT:w" s="T749">ibiʔi</ts>
                  <nts id="Seg_2575" n="HIAT:ip">.</nts>
                  <nts id="Seg_2576" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T753" id="Seg_2577" n="sc" s="T751">
               <ts e="T753" id="Seg_2579" n="HIAT:u" s="T751">
                  <ts e="T752" id="Seg_2581" n="HIAT:w" s="T751">A</ts>
                  <nts id="Seg_2582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2584" n="HIAT:w" s="T752">gibər</ts>
                  <nts id="Seg_2585" n="HIAT:ip">?</nts>
                  <nts id="Seg_2586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T758" id="Seg_2587" n="sc" s="T754">
               <ts e="T758" id="Seg_2589" n="HIAT:u" s="T754">
                  <ts e="T755" id="Seg_2591" n="HIAT:w" s="T754">Nu</ts>
                  <nts id="Seg_2592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_2594" n="HIAT:w" s="T755">măn</ts>
                  <nts id="Seg_2595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_2597" n="HIAT:w" s="T756">tĭmnem</ts>
                  <nts id="Seg_2598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_2600" n="HIAT:w" s="T757">gibər</ts>
                  <nts id="Seg_2601" n="HIAT:ip">?</nts>
                  <nts id="Seg_2602" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T763" id="Seg_2603" n="sc" s="T759">
               <ts e="T763" id="Seg_2605" n="HIAT:u" s="T759">
                  <ts e="T760" id="Seg_2607" n="HIAT:w" s="T759">Măn</ts>
                  <nts id="Seg_2608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_2610" n="HIAT:w" s="T760">že</ts>
                  <nts id="Seg_2611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_2613" n="HIAT:w" s="T761">ej</ts>
                  <nts id="Seg_2614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_2616" n="HIAT:w" s="T762">tĭmnem</ts>
                  <nts id="Seg_2617" n="HIAT:ip">.</nts>
                  <nts id="Seg_2618" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T765" id="Seg_2619" n="sc" s="T764">
               <ts e="T765" id="Seg_2621" n="HIAT:u" s="T764">
                  <nts id="Seg_2622" n="HIAT:ip">(</nts>
                  <nts id="Seg_2623" n="HIAT:ip">(</nts>
                  <ats e="T765" id="Seg_2624" n="HIAT:non-pho" s="T764">BRK</ats>
                  <nts id="Seg_2625" n="HIAT:ip">)</nts>
                  <nts id="Seg_2626" n="HIAT:ip">)</nts>
                  <nts id="Seg_2627" n="HIAT:ip">.</nts>
                  <nts id="Seg_2628" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T769" id="Seg_2629" n="sc" s="T766">
               <ts e="T769" id="Seg_2631" n="HIAT:u" s="T766">
                  <ts e="T767" id="Seg_2633" n="HIAT:w" s="T766">Kanžəbəj</ts>
                  <nts id="Seg_2634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2635" n="HIAT:ip">(</nts>
                  <ts e="T768" id="Seg_2637" n="HIAT:w" s="T767">mʼeškaʔi</ts>
                  <nts id="Seg_2638" n="HIAT:ip">)</nts>
                  <nts id="Seg_2639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_2641" n="HIAT:w" s="T768">izittə</ts>
                  <nts id="Seg_2642" n="HIAT:ip">.</nts>
                  <nts id="Seg_2643" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T772" id="Seg_2644" n="sc" s="T770">
               <ts e="T772" id="Seg_2646" n="HIAT:u" s="T770">
                  <ts e="T771" id="Seg_2648" n="HIAT:w" s="T770">Mʼeška</ts>
                  <nts id="Seg_2649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_2651" n="HIAT:w" s="T771">kuvas</ts>
                  <nts id="Seg_2652" n="HIAT:ip">.</nts>
                  <nts id="Seg_2653" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T777" id="Seg_2654" n="sc" s="T773">
               <ts e="T777" id="Seg_2656" n="HIAT:u" s="T773">
                  <ts e="T774" id="Seg_2658" n="HIAT:w" s="T773">A</ts>
                  <nts id="Seg_2659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_2661" n="HIAT:w" s="T774">ĭmbi</ts>
                  <nts id="Seg_2662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2663" n="HIAT:ip">(</nts>
                  <ts e="T776" id="Seg_2665" n="HIAT:w" s="T775">molə</ts>
                  <nts id="Seg_2666" n="HIAT:ip">)</nts>
                  <nts id="Seg_2667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_2669" n="HIAT:w" s="T776">dereʔ</ts>
                  <nts id="Seg_2670" n="HIAT:ip">?</nts>
                  <nts id="Seg_2671" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T781" id="Seg_2672" n="sc" s="T778">
               <ts e="T781" id="Seg_2674" n="HIAT:u" s="T778">
                  <ts e="T779" id="Seg_2676" n="HIAT:w" s="T778">O</ts>
                  <nts id="Seg_2677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_2679" n="HIAT:w" s="T779">dak</ts>
                  <nts id="Seg_2680" n="HIAT:ip">,</nts>
                  <nts id="Seg_2681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_2683" n="HIAT:w" s="T780">ĭmbi</ts>
                  <nts id="Seg_2684" n="HIAT:ip">.</nts>
                  <nts id="Seg_2685" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T788" id="Seg_2686" n="sc" s="T782">
               <ts e="T788" id="Seg_2688" n="HIAT:u" s="T782">
                  <ts e="T783" id="Seg_2690" n="HIAT:w" s="T782">A</ts>
                  <nts id="Seg_2691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_2693" n="HIAT:w" s="T783">măn</ts>
                  <nts id="Seg_2694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_2696" n="HIAT:w" s="T784">dĭttə</ts>
                  <nts id="Seg_2697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_2699" n="HIAT:w" s="T785">mĭnzerləm</ts>
                  <nts id="Seg_2700" n="HIAT:ip">,</nts>
                  <nts id="Seg_2701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_2703" n="HIAT:w" s="T786">amorzittə</ts>
                  <nts id="Seg_2704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_2706" n="HIAT:w" s="T787">moləm</ts>
                  <nts id="Seg_2707" n="HIAT:ip">.</nts>
                  <nts id="Seg_2708" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T792" id="Seg_2709" n="sc" s="T789">
               <ts e="T792" id="Seg_2711" n="HIAT:u" s="T789">
                  <ts e="T790" id="Seg_2713" n="HIAT:w" s="T789">I</ts>
                  <nts id="Seg_2714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_2716" n="HIAT:w" s="T790">tăn</ts>
                  <nts id="Seg_2717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_2719" n="HIAT:w" s="T791">amorləl</ts>
                  <nts id="Seg_2720" n="HIAT:ip">.</nts>
                  <nts id="Seg_2721" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T795" id="Seg_2722" n="sc" s="T793">
               <ts e="T795" id="Seg_2724" n="HIAT:u" s="T793">
                  <ts e="T794" id="Seg_2726" n="HIAT:w" s="T793">Jakše</ts>
                  <nts id="Seg_2727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_2729" n="HIAT:w" s="T794">dĭ</ts>
                  <nts id="Seg_2730" n="HIAT:ip">.</nts>
                  <nts id="Seg_2731" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T797" id="Seg_2732" n="sc" s="T796">
               <ts e="T797" id="Seg_2734" n="HIAT:u" s="T796">
                  <ts e="T797" id="Seg_2736" n="HIAT:w" s="T796">Вот</ts>
                  <nts id="Seg_2737" n="HIAT:ip">.</nts>
                  <nts id="Seg_2738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T799" id="Seg_2739" n="sc" s="T798">
               <ts e="T799" id="Seg_2741" n="HIAT:u" s="T798">
                  <nts id="Seg_2742" n="HIAT:ip">(</nts>
                  <nts id="Seg_2743" n="HIAT:ip">(</nts>
                  <ats e="T799" id="Seg_2744" n="HIAT:non-pho" s="T798">BRK</ats>
                  <nts id="Seg_2745" n="HIAT:ip">)</nts>
                  <nts id="Seg_2746" n="HIAT:ip">)</nts>
                  <nts id="Seg_2747" n="HIAT:ip">.</nts>
                  <nts id="Seg_2748" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T805" id="Seg_2749" n="sc" s="T800">
               <ts e="T805" id="Seg_2751" n="HIAT:u" s="T800">
                  <ts e="T801" id="Seg_2753" n="HIAT:w" s="T800">Măn</ts>
                  <nts id="Seg_2754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_2756" n="HIAT:w" s="T801">dĭʔnə</ts>
                  <nts id="Seg_2757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_2759" n="HIAT:w" s="T802">mĭləm</ts>
                  <nts id="Seg_2760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_2762" n="HIAT:w" s="T803">von</ts>
                  <nts id="Seg_2763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_2765" n="HIAT:w" s="T804">gĭdaʔ</ts>
                  <nts id="Seg_2766" n="HIAT:ip">.</nts>
                  <nts id="Seg_2767" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T812" id="Seg_2768" n="sc" s="T806">
               <ts e="T812" id="Seg_2770" n="HIAT:u" s="T806">
                  <ts e="T807" id="Seg_2772" n="HIAT:w" s="T806">A</ts>
                  <nts id="Seg_2773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_2775" n="HIAT:w" s="T807">dĭ</ts>
                  <nts id="Seg_2776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_2778" n="HIAT:w" s="T808">măna</ts>
                  <nts id="Seg_2779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_2781" n="HIAT:w" s="T809">idʼie</ts>
                  <nts id="Seg_2782" n="HIAT:ip">,</nts>
                  <nts id="Seg_2783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_2785" n="HIAT:w" s="T810">idʼie</ts>
                  <nts id="Seg_2786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_2788" n="HIAT:w" s="T811">mĭliet</ts>
                  <nts id="Seg_2789" n="HIAT:ip">.</nts>
                  <nts id="Seg_2790" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T814" id="Seg_2791" n="sc" s="T813">
               <ts e="T814" id="Seg_2793" n="HIAT:u" s="T813">
                  <nts id="Seg_2794" n="HIAT:ip">(</nts>
                  <nts id="Seg_2795" n="HIAT:ip">(</nts>
                  <ats e="T814" id="Seg_2796" n="HIAT:non-pho" s="T813">BRK</ats>
                  <nts id="Seg_2797" n="HIAT:ip">)</nts>
                  <nts id="Seg_2798" n="HIAT:ip">)</nts>
                  <nts id="Seg_2799" n="HIAT:ip">.</nts>
                  <nts id="Seg_2800" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T818" id="Seg_2801" n="sc" s="T815">
               <ts e="T818" id="Seg_2803" n="HIAT:u" s="T815">
                  <ts e="T816" id="Seg_2805" n="HIAT:w" s="T815">Nu</ts>
                  <nts id="Seg_2806" n="HIAT:ip">,</nts>
                  <nts id="Seg_2807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_2809" n="HIAT:w" s="T816">kanžəbəj</ts>
                  <nts id="Seg_2810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_2812" n="HIAT:w" s="T817">mănziʔ</ts>
                  <nts id="Seg_2813" n="HIAT:ip">.</nts>
                  <nts id="Seg_2814" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T820" id="Seg_2815" n="sc" s="T819">
               <ts e="T820" id="Seg_2817" n="HIAT:u" s="T819">
                  <ts e="T820" id="Seg_2819" n="HIAT:w" s="T819">Gibər</ts>
                  <nts id="Seg_2820" n="HIAT:ip">?</nts>
                  <nts id="Seg_2821" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T823" id="Seg_2822" n="sc" s="T821">
               <ts e="T823" id="Seg_2824" n="HIAT:u" s="T821">
                  <ts e="T822" id="Seg_2826" n="HIAT:w" s="T821">Da</ts>
                  <nts id="Seg_2827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_2829" n="HIAT:w" s="T822">dĭbər</ts>
                  <nts id="Seg_2830" n="HIAT:ip">.</nts>
                  <nts id="Seg_2831" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T826" id="Seg_2832" n="sc" s="T824">
               <ts e="T826" id="Seg_2834" n="HIAT:u" s="T824">
                  <ts e="T825" id="Seg_2836" n="HIAT:w" s="T824">A</ts>
                  <nts id="Seg_2837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_2839" n="HIAT:w" s="T825">ial</ts>
                  <nts id="Seg_2840" n="HIAT:ip">?</nts>
                  <nts id="Seg_2841" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T829" id="Seg_2842" n="sc" s="T827">
               <ts e="T829" id="Seg_2844" n="HIAT:u" s="T827">
                  <ts e="T828" id="Seg_2846" n="HIAT:w" s="T827">Ial</ts>
                  <nts id="Seg_2847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2848" n="HIAT:ip">(</nts>
                  <ts e="T829" id="Seg_2850" n="HIAT:w" s="T828">pušaj</ts>
                  <nts id="Seg_2851" n="HIAT:ip">)</nts>
                  <nts id="Seg_2852" n="HIAT:ip">…</nts>
                  <nts id="Seg_2853" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T835" id="Seg_2854" n="sc" s="T830">
               <ts e="T835" id="Seg_2856" n="HIAT:u" s="T830">
                  <nts id="Seg_2857" n="HIAT:ip">(</nts>
                  <ts e="T831" id="Seg_2859" n="HIAT:w" s="T830">Ej</ts>
                  <nts id="Seg_2860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_2862" n="HIAT:w" s="T831">ej-</ts>
                  <nts id="Seg_2863" n="HIAT:ip">)</nts>
                  <nts id="Seg_2864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_2866" n="HIAT:w" s="T832">Ej</ts>
                  <nts id="Seg_2867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_2869" n="HIAT:w" s="T833">iləm</ts>
                  <nts id="Seg_2870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_2872" n="HIAT:w" s="T834">iam</ts>
                  <nts id="Seg_2873" n="HIAT:ip">.</nts>
                  <nts id="Seg_2874" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T840" id="Seg_2875" n="sc" s="T836">
               <ts e="T840" id="Seg_2877" n="HIAT:u" s="T836">
                  <ts e="T837" id="Seg_2879" n="HIAT:w" s="T836">A</ts>
                  <nts id="Seg_2880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2881" n="HIAT:ip">(</nts>
                  <ts e="T838" id="Seg_2883" n="HIAT:w" s="T837">miʔ</ts>
                  <nts id="Seg_2884" n="HIAT:ip">)</nts>
                  <nts id="Seg_2885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_2887" n="HIAT:w" s="T838">šidegöʔ</ts>
                  <nts id="Seg_2888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_2890" n="HIAT:w" s="T839">kanžəbəj</ts>
                  <nts id="Seg_2891" n="HIAT:ip">.</nts>
                  <nts id="Seg_2892" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T843" id="Seg_2893" n="sc" s="T841">
               <ts e="T843" id="Seg_2895" n="HIAT:u" s="T841">
                  <ts e="T842" id="Seg_2897" n="HIAT:w" s="T841">A</ts>
                  <nts id="Seg_2898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_2900" n="HIAT:w" s="T842">gibər</ts>
                  <nts id="Seg_2901" n="HIAT:ip">?</nts>
                  <nts id="Seg_2902" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T847" id="Seg_2903" n="sc" s="T844">
               <ts e="T847" id="Seg_2905" n="HIAT:u" s="T844">
                  <ts e="T845" id="Seg_2907" n="HIAT:w" s="T844">Da</ts>
                  <nts id="Seg_2908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_2910" n="HIAT:w" s="T845">von</ts>
                  <nts id="Seg_2911" n="HIAT:ip">,</nts>
                  <nts id="Seg_2912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_2914" n="HIAT:w" s="T846">dĭbər</ts>
                  <nts id="Seg_2915" n="HIAT:ip">.</nts>
                  <nts id="Seg_2916" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T850" id="Seg_2917" n="sc" s="T848">
               <ts e="T850" id="Seg_2919" n="HIAT:u" s="T848">
                  <ts e="T849" id="Seg_2921" n="HIAT:w" s="T848">Gijen</ts>
                  <nts id="Seg_2922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_2924" n="HIAT:w" s="T849">il</ts>
                  <nts id="Seg_2925" n="HIAT:ip">.</nts>
                  <nts id="Seg_2926" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T855" id="Seg_2927" n="sc" s="T851">
               <ts e="T855" id="Seg_2929" n="HIAT:u" s="T851">
                  <ts e="T852" id="Seg_2931" n="HIAT:w" s="T851">A</ts>
                  <nts id="Seg_2932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_2934" n="HIAT:w" s="T852">ĭmbi</ts>
                  <nts id="Seg_2935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_2937" n="HIAT:w" s="T853">dĭ</ts>
                  <nts id="Seg_2938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_2940" n="HIAT:w" s="T854">dĭgən</ts>
                  <nts id="Seg_2941" n="HIAT:ip">?</nts>
                  <nts id="Seg_2942" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T859" id="Seg_2943" n="sc" s="T856">
               <ts e="T859" id="Seg_2945" n="HIAT:u" s="T856">
                  <nts id="Seg_2946" n="HIAT:ip">(</nts>
                  <ts e="T857" id="Seg_2948" n="HIAT:w" s="T856">Ĭmbidə=</ts>
                  <nts id="Seg_2949" n="HIAT:ip">)</nts>
                  <nts id="Seg_2950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_2952" n="HIAT:w" s="T857">Ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_2953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T859" id="Seg_2955" n="HIAT:w" s="T858">nörbəlaʔi</ts>
                  <nts id="Seg_2956" n="HIAT:ip">.</nts>
                  <nts id="Seg_2957" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T863" id="Seg_2958" n="sc" s="T860">
               <ts e="T863" id="Seg_2960" n="HIAT:u" s="T860">
                  <nts id="Seg_2961" n="HIAT:ip">(</nts>
                  <ts e="T861" id="Seg_2963" n="HIAT:w" s="T860">A</ts>
                  <nts id="Seg_2964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_2966" n="HIAT:w" s="T861">m-</ts>
                  <nts id="Seg_2967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_2969" n="HIAT:w" s="T862">m-</ts>
                  <nts id="Seg_2970" n="HIAT:ip">)</nts>
                  <nts id="Seg_2971" n="HIAT:ip">.</nts>
                  <nts id="Seg_2972" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T866" id="Seg_2973" n="sc" s="T864">
               <ts e="T866" id="Seg_2975" n="HIAT:u" s="T864">
                  <ts e="T865" id="Seg_2977" n="HIAT:w" s="T864">Nörbəlaʔi</ts>
                  <nts id="Seg_2978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_2980" n="HIAT:w" s="T865">ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_2981" n="HIAT:ip">.</nts>
                  <nts id="Seg_2982" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T873" id="Seg_2983" n="sc" s="T867">
               <ts e="T873" id="Seg_2985" n="HIAT:u" s="T867">
                  <ts e="T868" id="Seg_2987" n="HIAT:w" s="T867">A</ts>
                  <nts id="Seg_2988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2989" n="HIAT:ip">(</nts>
                  <ts e="T869" id="Seg_2991" n="HIAT:w" s="T868">miʔ=</ts>
                  <nts id="Seg_2992" n="HIAT:ip">)</nts>
                  <nts id="Seg_2993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_2995" n="HIAT:w" s="T869">măn</ts>
                  <nts id="Seg_2996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_2998" n="HIAT:w" s="T870">ĭmbidə</ts>
                  <nts id="Seg_2999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_3001" n="HIAT:w" s="T871">ej</ts>
                  <nts id="Seg_3002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_3004" n="HIAT:w" s="T872">nörbəlam</ts>
                  <nts id="Seg_3005" n="HIAT:ip">.</nts>
                  <nts id="Seg_3006" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T878" id="Seg_3007" n="sc" s="T874">
               <ts e="T878" id="Seg_3009" n="HIAT:u" s="T874">
                  <ts e="T875" id="Seg_3011" n="HIAT:w" s="T874">A</ts>
                  <nts id="Seg_3012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_3014" n="HIAT:w" s="T875">moʔ</ts>
                  <nts id="Seg_3015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_3017" n="HIAT:w" s="T876">ej</ts>
                  <nts id="Seg_3018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_3020" n="HIAT:w" s="T877">nörbəlal</ts>
                  <nts id="Seg_3021" n="HIAT:ip">?</nts>
                  <nts id="Seg_3022" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T880" id="Seg_3023" n="sc" s="T879">
               <ts e="T880" id="Seg_3025" n="HIAT:u" s="T879">
                  <ts e="T880" id="Seg_3027" n="HIAT:w" s="T879">Dʼok</ts>
                  <nts id="Seg_3028" n="HIAT:ip">.</nts>
                  <nts id="Seg_3029" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T887" id="Seg_3030" n="sc" s="T881">
               <ts e="T887" id="Seg_3032" n="HIAT:u" s="T881">
                  <ts e="T882" id="Seg_3034" n="HIAT:w" s="T881">eželʼi</ts>
                  <nts id="Seg_3035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_3037" n="HIAT:w" s="T882">nörbəzittə</ts>
                  <nts id="Seg_3038" n="HIAT:ip">,</nts>
                  <nts id="Seg_3039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3040" n="HIAT:ip">(</nts>
                  <ts e="T884" id="Seg_3042" n="HIAT:w" s="T883">mĭ-</ts>
                  <nts id="Seg_3043" n="HIAT:ip">)</nts>
                  <nts id="Seg_3044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_3046" n="HIAT:w" s="T884">bile</ts>
                  <nts id="Seg_3047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_3049" n="HIAT:w" s="T885">il</ts>
                  <nts id="Seg_3050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887" id="Seg_3052" n="HIAT:w" s="T886">molam</ts>
                  <nts id="Seg_3053" n="HIAT:ip">.</nts>
                  <nts id="Seg_3054" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T890" id="Seg_3055" n="sc" s="T888">
               <ts e="T890" id="Seg_3057" n="HIAT:u" s="T888">
                  <ts e="T889" id="Seg_3059" n="HIAT:w" s="T888">Mo</ts>
                  <nts id="Seg_3060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_3062" n="HIAT:w" s="T889">dereʔ</ts>
                  <nts id="Seg_3063" n="HIAT:ip">?</nts>
                  <nts id="Seg_3064" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T894" id="Seg_3065" n="sc" s="T891">
               <ts e="T894" id="Seg_3067" n="HIAT:u" s="T891">
                  <ts e="T892" id="Seg_3069" n="HIAT:w" s="T891">Dʼok</ts>
                  <nts id="Seg_3070" n="HIAT:ip">,</nts>
                  <nts id="Seg_3071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_3073" n="HIAT:w" s="T892">jakše</ts>
                  <nts id="Seg_3074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_3076" n="HIAT:w" s="T893">il</ts>
                  <nts id="Seg_3077" n="HIAT:ip">.</nts>
                  <nts id="Seg_3078" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T898" id="Seg_3079" n="sc" s="T895">
               <ts e="T898" id="Seg_3081" n="HIAT:u" s="T895">
                  <ts e="T896" id="Seg_3083" n="HIAT:w" s="T895">Dʼok</ts>
                  <nts id="Seg_3084" n="HIAT:ip">,</nts>
                  <nts id="Seg_3085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_3087" n="HIAT:w" s="T896">bile</ts>
                  <nts id="Seg_3088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_3090" n="HIAT:w" s="T897">il</ts>
                  <nts id="Seg_3091" n="HIAT:ip">.</nts>
                  <nts id="Seg_3092" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T904" id="Seg_3093" n="sc" s="T899">
               <ts e="T904" id="Seg_3095" n="HIAT:u" s="T899">
                  <ts e="T900" id="Seg_3097" n="HIAT:w" s="T899">No</ts>
                  <nts id="Seg_3098" n="HIAT:ip">,</nts>
                  <nts id="Seg_3099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_3101" n="HIAT:w" s="T900">măn</ts>
                  <nts id="Seg_3102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_3104" n="HIAT:w" s="T901">ari</ts>
                  <nts id="Seg_3105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_3107" n="HIAT:w" s="T902">dʼürləm</ts>
                  <nts id="Seg_3108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T904" id="Seg_3110" n="HIAT:w" s="T903">tănan</ts>
                  <nts id="Seg_3111" n="HIAT:ip">.</nts>
                  <nts id="Seg_3112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T906" id="Seg_3113" n="sc" s="T905">
               <ts e="T906" id="Seg_3115" n="HIAT:u" s="T905">
                  <ts e="T906" id="Seg_3117" n="HIAT:w" s="T905">Вот</ts>
                  <nts id="Seg_3118" n="HIAT:ip">.</nts>
                  <nts id="Seg_3119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T912" id="Seg_3120" n="sc" s="T907">
               <ts e="T912" id="Seg_3122" n="HIAT:u" s="T907">
                  <ts e="T908" id="Seg_3124" n="HIAT:w" s="T907">Măn</ts>
                  <nts id="Seg_3125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_3127" n="HIAT:w" s="T908">abam</ts>
                  <nts id="Seg_3128" n="HIAT:ip">,</nts>
                  <nts id="Seg_3129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_3131" n="HIAT:w" s="T909">tăn</ts>
                  <nts id="Seg_3132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_3134" n="HIAT:w" s="T910">abal</ts>
                  <nts id="Seg_3135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T912" id="Seg_3137" n="HIAT:w" s="T911">dʼabrolaʔi</ts>
                  <nts id="Seg_3138" n="HIAT:ip">.</nts>
                  <nts id="Seg_3139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T916" id="Seg_3140" n="sc" s="T913">
               <ts e="T916" id="Seg_3142" n="HIAT:u" s="T913">
                  <ts e="T914" id="Seg_3144" n="HIAT:w" s="T913">Mo</ts>
                  <nts id="Seg_3145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T915" id="Seg_3147" n="HIAT:w" s="T914">dĭ</ts>
                  <nts id="Seg_3148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_3150" n="HIAT:w" s="T915">dʼabrolaʔi</ts>
                  <nts id="Seg_3151" n="HIAT:ip">?</nts>
                  <nts id="Seg_3152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T928" id="Seg_3153" n="sc" s="T917">
               <ts e="T928" id="Seg_3155" n="HIAT:u" s="T917">
                  <ts e="T918" id="Seg_3157" n="HIAT:w" s="T917">Dak</ts>
                  <nts id="Seg_3158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T919" id="Seg_3160" n="HIAT:w" s="T918">dĭ</ts>
                  <nts id="Seg_3161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T920" id="Seg_3163" n="HIAT:w" s="T919">măllat</ts>
                  <nts id="Seg_3164" n="HIAT:ip">,</nts>
                  <nts id="Seg_3165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T921" id="Seg_3167" n="HIAT:w" s="T920">tăn</ts>
                  <nts id="Seg_3168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_3170" n="HIAT:w" s="T921">abal</ts>
                  <nts id="Seg_3171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T924" id="Seg_3173" n="HIAT:w" s="T922">măllat:</ts>
                  <nts id="Seg_3174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3175" n="HIAT:ip">"</nts>
                  <ts e="T925" id="Seg_3177" n="HIAT:w" s="T924">Măn</ts>
                  <nts id="Seg_3178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T926" id="Seg_3180" n="HIAT:w" s="T925">nʼim</ts>
                  <nts id="Seg_3181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T927" id="Seg_3183" n="HIAT:w" s="T926">jakše</ts>
                  <nts id="Seg_3184" n="HIAT:ip">,</nts>
                  <nts id="Seg_3185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T928" id="Seg_3187" n="HIAT:w" s="T927">kuvas</ts>
                  <nts id="Seg_3188" n="HIAT:ip">.</nts>
                  <nts id="Seg_3189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T934" id="Seg_3190" n="sc" s="T929">
               <ts e="T934" id="Seg_3192" n="HIAT:u" s="T929">
                  <ts e="T930" id="Seg_3194" n="HIAT:w" s="T929">A</ts>
                  <nts id="Seg_3195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T931" id="Seg_3197" n="HIAT:w" s="T930">tăn</ts>
                  <nts id="Seg_3198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T932" id="Seg_3200" n="HIAT:w" s="T931">koʔbdo</ts>
                  <nts id="Seg_3201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T933" id="Seg_3203" n="HIAT:w" s="T932">ej</ts>
                  <nts id="Seg_3204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T934" id="Seg_3206" n="HIAT:w" s="T933">jakše</ts>
                  <nts id="Seg_3207" n="HIAT:ip">"</nts>
                  <nts id="Seg_3208" n="HIAT:ip">.</nts>
                  <nts id="Seg_3209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T944" id="Seg_3210" n="sc" s="T935">
               <ts e="T944" id="Seg_3212" n="HIAT:u" s="T935">
                  <ts e="T936" id="Seg_3214" n="HIAT:w" s="T935">A</ts>
                  <nts id="Seg_3215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T937" id="Seg_3217" n="HIAT:w" s="T936">măn</ts>
                  <nts id="Seg_3218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T938" id="Seg_3220" n="HIAT:w" s="T937">abam</ts>
                  <nts id="Seg_3221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T940" id="Seg_3223" n="HIAT:w" s="T938">măllat:</ts>
                  <nts id="Seg_3224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3225" n="HIAT:ip">"</nts>
                  <ts e="T941" id="Seg_3227" n="HIAT:w" s="T940">Eh</ts>
                  <nts id="Seg_3228" n="HIAT:ip">,</nts>
                  <nts id="Seg_3229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T942" id="Seg_3231" n="HIAT:w" s="T941">măn</ts>
                  <nts id="Seg_3232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_3234" n="HIAT:w" s="T942">koʔbdom</ts>
                  <nts id="Seg_3235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T944" id="Seg_3237" n="HIAT:w" s="T943">jakše</ts>
                  <nts id="Seg_3238" n="HIAT:ip">.</nts>
                  <nts id="Seg_3239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T947" id="Seg_3240" n="sc" s="T945">
               <ts e="T947" id="Seg_3242" n="HIAT:u" s="T945">
                  <ts e="T946" id="Seg_3244" n="HIAT:w" s="T945">I</ts>
                  <nts id="Seg_3245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947" id="Seg_3247" n="HIAT:w" s="T946">kuvas</ts>
                  <nts id="Seg_3248" n="HIAT:ip">.</nts>
                  <nts id="Seg_3249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T954" id="Seg_3250" n="sc" s="T948">
               <ts e="T954" id="Seg_3252" n="HIAT:u" s="T948">
                  <ts e="T949" id="Seg_3254" n="HIAT:w" s="T948">I</ts>
                  <nts id="Seg_3255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T950" id="Seg_3257" n="HIAT:w" s="T949">bar</ts>
                  <nts id="Seg_3258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T951" id="Seg_3260" n="HIAT:w" s="T950">ĭmbi</ts>
                  <nts id="Seg_3261" n="HIAT:ip">,</nts>
                  <nts id="Seg_3262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T952" id="Seg_3264" n="HIAT:w" s="T951">bar</ts>
                  <nts id="Seg_3265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T953" id="Seg_3267" n="HIAT:w" s="T952">ĭmbi</ts>
                  <nts id="Seg_3268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T954" id="Seg_3270" n="HIAT:w" s="T953">măllat</ts>
                  <nts id="Seg_3271" n="HIAT:ip">.</nts>
                  <nts id="Seg_3272" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T960" id="Seg_3273" n="sc" s="T955">
               <ts e="T960" id="Seg_3275" n="HIAT:u" s="T955">
                  <ts e="T956" id="Seg_3277" n="HIAT:w" s="T955">I</ts>
                  <nts id="Seg_3278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T957" id="Seg_3280" n="HIAT:w" s="T956">tăn</ts>
                  <nts id="Seg_3281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T958" id="Seg_3283" n="HIAT:w" s="T957">nʼil</ts>
                  <nts id="Seg_3284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T959" id="Seg_3286" n="HIAT:w" s="T958">ej</ts>
                  <nts id="Seg_3287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T960" id="Seg_3289" n="HIAT:w" s="T959">jakše</ts>
                  <nts id="Seg_3290" n="HIAT:ip">.</nts>
                  <nts id="Seg_3291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T965" id="Seg_3292" n="sc" s="T961">
               <ts e="T965" id="Seg_3294" n="HIAT:u" s="T961">
                  <ts e="T962" id="Seg_3296" n="HIAT:w" s="T961">Ĭmbidə</ts>
                  <nts id="Seg_3297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T963" id="Seg_3299" n="HIAT:w" s="T962">ej</ts>
                  <nts id="Seg_3300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964" id="Seg_3302" n="HIAT:w" s="T963">nörbəlat</ts>
                  <nts id="Seg_3303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_3305" n="HIAT:w" s="T964">măna</ts>
                  <nts id="Seg_3306" n="HIAT:ip">.</nts>
                  <nts id="Seg_3307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T972" id="Seg_3308" n="sc" s="T966">
               <ts e="T972" id="Seg_3310" n="HIAT:u" s="T966">
                  <ts e="T967" id="Seg_3312" n="HIAT:w" s="T966">A</ts>
                  <nts id="Seg_3313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T968" id="Seg_3315" n="HIAT:w" s="T967">măn</ts>
                  <nts id="Seg_3316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T969" id="Seg_3318" n="HIAT:w" s="T968">koʔbdom</ts>
                  <nts id="Seg_3319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T970" id="Seg_3321" n="HIAT:w" s="T969">bar</ts>
                  <nts id="Seg_3322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T971" id="Seg_3324" n="HIAT:w" s="T970">ĭmbi</ts>
                  <nts id="Seg_3325" n="HIAT:ip">,</nts>
                  <nts id="Seg_3326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T972" id="Seg_3328" n="HIAT:w" s="T971">bar</ts>
                  <nts id="Seg_3329" n="HIAT:ip">.</nts>
                  <nts id="Seg_3330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T976" id="Seg_3331" n="sc" s="T973">
               <ts e="T976" id="Seg_3333" n="HIAT:u" s="T973">
                  <ts e="T974" id="Seg_3335" n="HIAT:w" s="T973">Tănan</ts>
                  <nts id="Seg_3336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T975" id="Seg_3338" n="HIAT:w" s="T974">üge</ts>
                  <nts id="Seg_3339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T976" id="Seg_3341" n="HIAT:w" s="T975">nörbəlapliet</ts>
                  <nts id="Seg_3342" n="HIAT:ip">.</nts>
                  <nts id="Seg_3343" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T978" id="Seg_3344" n="sc" s="T977">
               <ts e="T978" id="Seg_3346" n="HIAT:u" s="T977">
                  <ts e="T978" id="Seg_3348" n="HIAT:w" s="T977">Vot</ts>
                  <nts id="Seg_3349" n="HIAT:ip">.</nts>
                  <nts id="Seg_3350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T5" id="Seg_3351" n="sc" s="T1">
               <ts e="T2" id="Seg_3353" n="e" s="T1">Jakše </ts>
               <ts e="T3" id="Seg_3355" n="e" s="T2">kuza </ts>
               <ts e="T4" id="Seg_3357" n="e" s="T3">mobi, </ts>
               <ts e="T5" id="Seg_3359" n="e" s="T4">külambi. </ts>
            </ts>
            <ts e="T8" id="Seg_3360" n="sc" s="T6">
               <ts e="T7" id="Seg_3362" n="e" s="T6">Jakše </ts>
               <ts e="T8" id="Seg_3364" n="e" s="T7">külambi. </ts>
            </ts>
            <ts e="T17" id="Seg_3365" n="sc" s="T9">
               <ts e="T10" id="Seg_3367" n="e" s="T9">(A </ts>
               <ts e="T11" id="Seg_3369" n="e" s="T10">dĭ) </ts>
               <ts e="T12" id="Seg_3371" n="e" s="T11">bile </ts>
               <ts e="T13" id="Seg_3373" n="e" s="T12">kuza </ts>
               <ts e="T14" id="Seg_3375" n="e" s="T13">(oʔb) </ts>
               <ts e="T15" id="Seg_3377" n="e" s="T14">(ku- </ts>
               <ts e="T16" id="Seg_3379" n="e" s="T15">ku-) </ts>
               <ts e="T17" id="Seg_3381" n="e" s="T16">külambit. </ts>
            </ts>
            <ts e="T23" id="Seg_3382" n="sc" s="T18">
               <ts e="T19" id="Seg_3384" n="e" s="T18">A </ts>
               <ts e="T20" id="Seg_3386" n="e" s="T19">jakše </ts>
               <ts e="T21" id="Seg_3388" n="e" s="T20">kuza </ts>
               <ts e="T22" id="Seg_3390" n="e" s="T21">jakše </ts>
               <ts e="T23" id="Seg_3392" n="e" s="T22">külambi. </ts>
            </ts>
            <ts e="T25" id="Seg_3393" n="sc" s="T24">
               <ts e="T25" id="Seg_3395" n="e" s="T24">(Vot). </ts>
            </ts>
            <ts e="T30" id="Seg_3396" n="sc" s="T26">
               <ts e="T27" id="Seg_3398" n="e" s="T26">Măn </ts>
               <ts e="T28" id="Seg_3400" n="e" s="T27">abam </ts>
               <ts e="T29" id="Seg_3402" n="e" s="T28">jakše </ts>
               <ts e="T30" id="Seg_3404" n="e" s="T29">amnobi. </ts>
            </ts>
            <ts e="T33" id="Seg_3405" n="sc" s="T31">
               <ts e="T32" id="Seg_3407" n="e" s="T31">Dĭttə </ts>
               <ts e="T33" id="Seg_3409" n="e" s="T32">bile. </ts>
            </ts>
            <ts e="T37" id="Seg_3410" n="sc" s="T34">
               <ts e="T35" id="Seg_3412" n="e" s="T34">Tăŋ </ts>
               <ts e="T36" id="Seg_3414" n="e" s="T35">bile </ts>
               <ts e="T37" id="Seg_3416" n="e" s="T36">amnobi. </ts>
            </ts>
            <ts e="T44" id="Seg_3417" n="sc" s="T38">
               <ts e="T39" id="Seg_3419" n="e" s="T38">Dĭttə </ts>
               <ts e="T40" id="Seg_3421" n="e" s="T39">(это </ts>
               <ts e="T41" id="Seg_3423" n="e" s="T40">ой=) </ts>
               <ts e="T42" id="Seg_3425" n="e" s="T41">bar </ts>
               <ts e="T43" id="Seg_3427" n="e" s="T42">amnobi </ts>
               <ts e="T44" id="Seg_3429" n="e" s="T43">jakše. </ts>
            </ts>
            <ts e="T47" id="Seg_3430" n="sc" s="T45">
               <ts e="T46" id="Seg_3432" n="e" s="T45">Dĭttə </ts>
               <ts e="T47" id="Seg_3434" n="e" s="T46">külambi. </ts>
            </ts>
            <ts e="T50" id="Seg_3435" n="sc" s="T48">
               <ts e="T49" id="Seg_3437" n="e" s="T48">(Nu </ts>
               <ts e="T50" id="Seg_3439" n="e" s="T49">mĭ=). </ts>
            </ts>
            <ts e="T55" id="Seg_3440" n="sc" s="T51">
               <ts e="T52" id="Seg_3442" n="e" s="T51">Măn </ts>
               <ts e="T53" id="Seg_3444" n="e" s="T52">amnobiam </ts>
               <ts e="T54" id="Seg_3446" n="e" s="T53">tăŋ </ts>
               <ts e="T55" id="Seg_3448" n="e" s="T54">bile. </ts>
            </ts>
            <ts e="T59" id="Seg_3449" n="sc" s="T56">
               <ts e="T57" id="Seg_3451" n="e" s="T56">Tăŋ </ts>
               <ts e="T58" id="Seg_3453" n="e" s="T57">bile </ts>
               <ts e="T59" id="Seg_3455" n="e" s="T58">amnobiam. </ts>
            </ts>
            <ts e="T63" id="Seg_3456" n="sc" s="T60">
               <ts e="T61" id="Seg_3458" n="e" s="T60">Măn </ts>
               <ts e="T62" id="Seg_3460" n="e" s="T61">ipekpə </ts>
               <ts e="T63" id="Seg_3462" n="e" s="T62">naga. </ts>
            </ts>
            <ts e="T72" id="Seg_3463" n="sc" s="T64">
               <ts e="T65" id="Seg_3465" n="e" s="T64">Naga </ts>
               <ts e="T66" id="Seg_3467" n="e" s="T65">ipek </ts>
               <ts e="T67" id="Seg_3469" n="e" s="T66">măn, </ts>
               <ts e="T68" id="Seg_3471" n="e" s="T67">i </ts>
               <ts e="T69" id="Seg_3473" n="e" s="T68">măn </ts>
               <ts e="T70" id="Seg_3475" n="e" s="T69">bostə </ts>
               <ts e="T71" id="Seg_3477" n="e" s="T70">ĭmbi-ĭmbi </ts>
               <ts e="T72" id="Seg_3479" n="e" s="T71">amorbiam. </ts>
            </ts>
            <ts e="T76" id="Seg_3480" n="sc" s="T73">
               <ts e="T74" id="Seg_3482" n="e" s="T73">Dĭttəgəš </ts>
               <ts e="T75" id="Seg_3484" n="e" s="T74">jakše </ts>
               <ts e="T76" id="Seg_3486" n="e" s="T75">amnobiam. </ts>
            </ts>
            <ts e="T79" id="Seg_3487" n="sc" s="T77">
               <ts e="T78" id="Seg_3489" n="e" s="T77">Bar </ts>
               <ts e="T79" id="Seg_3491" n="e" s="T78">gĭdeʔ. </ts>
            </ts>
            <ts e="T83" id="Seg_3492" n="sc" s="T80">
               <ts e="T81" id="Seg_3494" n="e" s="T80">Dĭttəgəš </ts>
               <ts e="T82" id="Seg_3496" n="e" s="T81">ari </ts>
               <ts e="T83" id="Seg_3498" n="e" s="T82">dʼürbiam. </ts>
            </ts>
            <ts e="T90" id="Seg_3499" n="sc" s="T84">
               <ts e="T85" id="Seg_3501" n="e" s="T84">Amnobiam </ts>
               <ts e="T86" id="Seg_3503" n="e" s="T85">bile-bile-bile-bile. </ts>
               <ts e="T87" id="Seg_3505" n="e" s="T86">Koʔbdom </ts>
               <ts e="T88" id="Seg_3507" n="e" s="T87">(mĭle- </ts>
               <ts e="T89" id="Seg_3509" n="e" s="T88">mĭle-) </ts>
               <ts e="T90" id="Seg_3511" n="e" s="T89">mĭləbiam. </ts>
            </ts>
            <ts e="T96" id="Seg_3512" n="sc" s="T91">
               <ts e="T92" id="Seg_3514" n="e" s="T91">(I) </ts>
               <ts e="T93" id="Seg_3516" n="e" s="T92">koʔbdozi </ts>
               <ts e="T94" id="Seg_3518" n="e" s="T93">tăŋ </ts>
               <ts e="T95" id="Seg_3520" n="e" s="T94">bile </ts>
               <ts e="T96" id="Seg_3522" n="e" s="T95">amnobiam. </ts>
            </ts>
            <ts e="T98" id="Seg_3523" n="sc" s="T97">
               <ts e="T98" id="Seg_3525" n="e" s="T97">Dʼörlam. </ts>
            </ts>
            <ts e="T109" id="Seg_3526" n="sc" s="T99">
               <ts e="T100" id="Seg_3528" n="e" s="T99">I </ts>
               <ts e="T101" id="Seg_3530" n="e" s="T100">(an-) </ts>
               <ts e="T102" id="Seg_3532" n="e" s="T101">allam, </ts>
               <ts e="T103" id="Seg_3534" n="e" s="T102">i </ts>
               <ts e="T104" id="Seg_3536" n="e" s="T103">(šo-) </ts>
               <ts e="T105" id="Seg_3538" n="e" s="T104">šolam </ts>
               <ts e="T106" id="Seg_3540" n="e" s="T105">(amno-) </ts>
               <ts e="T107" id="Seg_3542" n="e" s="T106">amnozittə, </ts>
               <ts e="T108" id="Seg_3544" n="e" s="T107">tăŋ </ts>
               <ts e="T109" id="Seg_3546" n="e" s="T108">bile. </ts>
            </ts>
            <ts e="T113" id="Seg_3547" n="sc" s="T110">
               <ts e="T111" id="Seg_3549" n="e" s="T110">Gibər </ts>
               <ts e="T112" id="Seg_3551" n="e" s="T111">măna </ts>
               <ts e="T113" id="Seg_3553" n="e" s="T112">anzittə? </ts>
            </ts>
            <ts e="T119" id="Seg_3554" n="sc" s="T114">
               <ts e="T115" id="Seg_3556" n="e" s="T114">Gibərdə </ts>
               <ts e="T116" id="Seg_3558" n="e" s="T115">ej </ts>
               <ts e="T117" id="Seg_3560" n="e" s="T116">alləm, </ts>
               <ts e="T118" id="Seg_3562" n="e" s="T117">lutʼše </ts>
               <ts e="T119" id="Seg_3564" n="e" s="T118">külambiam. </ts>
            </ts>
            <ts e="T121" id="Seg_3565" n="sc" s="T120">
               <ts e="T121" id="Seg_3567" n="e" s="T120">Это. </ts>
            </ts>
            <ts e="T126" id="Seg_3568" n="sc" s="T122">
               <ts e="T123" id="Seg_3570" n="e" s="T122">Bar </ts>
               <ts e="T124" id="Seg_3572" n="e" s="T123">il </ts>
               <ts e="T125" id="Seg_3574" n="e" s="T124">jakše </ts>
               <ts e="T126" id="Seg_3576" n="e" s="T125">amnoliaʔit. </ts>
            </ts>
            <ts e="T133" id="Seg_3577" n="sc" s="T127">
               <ts e="T128" id="Seg_3579" n="e" s="T127">A </ts>
               <ts e="T129" id="Seg_3581" n="e" s="T128">dĭttə </ts>
               <ts e="T130" id="Seg_3583" n="e" s="T129">ari </ts>
               <ts e="T131" id="Seg_3585" n="e" s="T130">dʼürgelit </ts>
               <ts e="T132" id="Seg_3587" n="e" s="T131">dögəʔ, </ts>
               <ts e="T133" id="Seg_3589" n="e" s="T132">gibərdə. </ts>
            </ts>
            <ts e="T140" id="Seg_3590" n="sc" s="T134">
               <ts e="T135" id="Seg_3592" n="e" s="T134">A </ts>
               <ts e="T136" id="Seg_3594" n="e" s="T135">dĭttə </ts>
               <ts e="T137" id="Seg_3596" n="e" s="T136">moʔ </ts>
               <ts e="T138" id="Seg_3598" n="e" s="T137">döbər </ts>
               <ts e="T139" id="Seg_3600" n="e" s="T138">že </ts>
               <ts e="T140" id="Seg_3602" n="e" s="T139">šoləj. </ts>
            </ts>
            <ts e="T145" id="Seg_3603" n="sc" s="T141">
               <ts e="T142" id="Seg_3605" n="e" s="T141">Eh, </ts>
               <ts e="T143" id="Seg_3607" n="e" s="T142">moʔ </ts>
               <ts e="T144" id="Seg_3609" n="e" s="T143">dereʔ </ts>
               <ts e="T145" id="Seg_3611" n="e" s="T144">mollal? </ts>
            </ts>
            <ts e="T148" id="Seg_3612" n="sc" s="T146">
               <ts e="T147" id="Seg_3614" n="e" s="T146">I </ts>
               <ts e="T148" id="Seg_3616" n="e" s="T147">ĭmbi? </ts>
            </ts>
            <ts e="T150" id="Seg_3617" n="sc" s="T149">
               <ts e="T150" id="Seg_3619" n="e" s="T149">Dereʔ. </ts>
            </ts>
            <ts e="T153" id="Seg_3620" n="sc" s="T151">
               <ts e="T152" id="Seg_3622" n="e" s="T151">Šoləj </ts>
               <ts e="T153" id="Seg_3624" n="e" s="T152">döbər. </ts>
            </ts>
            <ts e="T157" id="Seg_3625" n="sc" s="T154">
               <ts e="T155" id="Seg_3627" n="e" s="T154">А </ts>
               <ts e="T156" id="Seg_3629" n="e" s="T155">döbər </ts>
               <ts e="T157" id="Seg_3631" n="e" s="T156">šoləj. </ts>
            </ts>
            <ts e="T163" id="Seg_3632" n="sc" s="T158">
               <ts e="T159" id="Seg_3634" n="e" s="T158">A </ts>
               <ts e="T160" id="Seg_3636" n="e" s="T159">tăn </ts>
               <ts e="T161" id="Seg_3638" n="e" s="T160">ĭmbi, </ts>
               <ts e="T162" id="Seg_3640" n="e" s="T161">ej </ts>
               <ts e="T163" id="Seg_3642" n="e" s="T162">alləl? </ts>
            </ts>
            <ts e="T165" id="Seg_3643" n="sc" s="T164">
               <ts e="T165" id="Seg_3645" n="e" s="T164">Dʼok. </ts>
            </ts>
            <ts e="T169" id="Seg_3646" n="sc" s="T166">
               <ts e="T167" id="Seg_3648" n="e" s="T166">Măn </ts>
               <ts e="T168" id="Seg_3650" n="e" s="T167">ej </ts>
               <ts e="T169" id="Seg_3652" n="e" s="T168">alləm. </ts>
            </ts>
            <ts e="T173" id="Seg_3653" n="sc" s="T170">
               <ts e="T171" id="Seg_3655" n="e" s="T170">Măn </ts>
               <ts e="T172" id="Seg_3657" n="e" s="T171">dögən </ts>
               <ts e="T173" id="Seg_3659" n="e" s="T172">molam. </ts>
            </ts>
            <ts e="T175" id="Seg_3660" n="sc" s="T174">
               <ts e="T175" id="Seg_3662" n="e" s="T174">Vot. </ts>
            </ts>
            <ts e="T177" id="Seg_3663" n="sc" s="T176">
               <ts e="T177" id="Seg_3665" n="e" s="T176">((BRK)). </ts>
            </ts>
            <ts e="T181" id="Seg_3666" n="sc" s="T178">
               <ts e="T179" id="Seg_3668" n="e" s="T178">Ĭmbi-nʼibudʼ </ts>
               <ts e="T180" id="Seg_3670" n="e" s="T179">mĭnzərzittə </ts>
               <ts e="T181" id="Seg_3672" n="e" s="T180">nada. </ts>
            </ts>
            <ts e="T185" id="Seg_3673" n="sc" s="T182">
               <ts e="T183" id="Seg_3675" n="e" s="T182">Măn </ts>
               <ts e="T184" id="Seg_3677" n="e" s="T183">amorzittə </ts>
               <ts e="T185" id="Seg_3679" n="e" s="T184">molam. </ts>
            </ts>
            <ts e="T187" id="Seg_3680" n="sc" s="T186">
               <ts e="T187" id="Seg_3682" n="e" s="T186">((BRK)). </ts>
            </ts>
            <ts e="T192" id="Seg_3683" n="sc" s="T188">
               <ts e="T189" id="Seg_3685" n="e" s="T188">(Ĭmbĭ- </ts>
               <ts e="T190" id="Seg_3687" n="e" s="T189">m-) </ts>
               <ts e="T191" id="Seg_3689" n="e" s="T190">Ĭmbi </ts>
               <ts e="T192" id="Seg_3691" n="e" s="T191">mĭnzerliel? </ts>
            </ts>
            <ts e="T194" id="Seg_3692" n="sc" s="T193">
               <ts e="T194" id="Seg_3694" n="e" s="T193">Uja. </ts>
            </ts>
            <ts e="T197" id="Seg_3695" n="sc" s="T195">
               <ts e="T196" id="Seg_3697" n="e" s="T195">Mo </ts>
               <ts e="T197" id="Seg_3699" n="e" s="T196">uja? </ts>
            </ts>
            <ts e="T202" id="Seg_3700" n="sc" s="T198">
               <ts e="T199" id="Seg_3702" n="e" s="T198">Măn </ts>
               <ts e="T200" id="Seg_3704" n="e" s="T199">bɨ </ts>
               <ts e="T201" id="Seg_3706" n="e" s="T200">(jejam </ts>
               <ts e="T202" id="Seg_3708" n="e" s="T201">amzit). </ts>
            </ts>
            <ts e="T208" id="Seg_3709" n="sc" s="T203">
               <ts e="T204" id="Seg_3711" n="e" s="T203">A </ts>
               <ts e="T205" id="Seg_3713" n="e" s="T204">măn </ts>
               <ts e="T206" id="Seg_3715" n="e" s="T205">bɨ </ts>
               <ts e="T207" id="Seg_3717" n="e" s="T206">süt </ts>
               <ts e="T208" id="Seg_3719" n="e" s="T207">bɨ. </ts>
            </ts>
            <ts e="T213" id="Seg_3720" n="sc" s="T209">
               <ts e="T210" id="Seg_3722" n="e" s="T209">Vot </ts>
               <ts e="T211" id="Seg_3724" n="e" s="T210">jakše </ts>
               <ts e="T212" id="Seg_3726" n="e" s="T211">bɨ </ts>
               <ts e="T213" id="Seg_3728" n="e" s="T212">amorlam. </ts>
            </ts>
            <ts e="T216" id="Seg_3729" n="sc" s="T214">
               <ts e="T215" id="Seg_3731" n="e" s="T214">Mo </ts>
               <ts e="T216" id="Seg_3733" n="e" s="T215">süt? </ts>
            </ts>
            <ts e="T220" id="Seg_3734" n="sc" s="T217">
               <ts e="T218" id="Seg_3736" n="e" s="T217">Süt </ts>
               <ts e="T219" id="Seg_3738" n="e" s="T218">dak </ts>
               <ts e="T220" id="Seg_3740" n="e" s="T219">süt. </ts>
            </ts>
            <ts e="T224" id="Seg_3741" n="sc" s="T221">
               <ts e="T222" id="Seg_3743" n="e" s="T221">A </ts>
               <ts e="T223" id="Seg_3745" n="e" s="T222">uja </ts>
               <ts e="T224" id="Seg_3747" n="e" s="T223">jakše. </ts>
            </ts>
            <ts e="T226" id="Seg_3748" n="sc" s="T225">
               <ts e="T226" id="Seg_3750" n="e" s="T225">((BRK)). </ts>
            </ts>
            <ts e="T231" id="Seg_3751" n="sc" s="T227">
               <ts e="T228" id="Seg_3753" n="e" s="T227">Mo </ts>
               <ts e="T229" id="Seg_3755" n="e" s="T228">măna </ts>
               <ts e="T230" id="Seg_3757" n="e" s="T229">ej </ts>
               <ts e="T231" id="Seg_3759" n="e" s="T230">šoləj? </ts>
            </ts>
            <ts e="T236" id="Seg_3760" n="sc" s="T232">
               <ts e="T233" id="Seg_3762" n="e" s="T232">A </ts>
               <ts e="T234" id="Seg_3764" n="e" s="T233">măn </ts>
               <ts e="T235" id="Seg_3766" n="e" s="T234">iam </ts>
               <ts e="T236" id="Seg_3768" n="e" s="T235">bila. </ts>
            </ts>
            <ts e="T241" id="Seg_3769" n="sc" s="T237">
               <ts e="T238" id="Seg_3771" n="e" s="T237">(Ilajaŋ) </ts>
               <ts e="T239" id="Seg_3773" n="e" s="T238">tănan </ts>
               <ts e="T240" id="Seg_3775" n="e" s="T239">ej </ts>
               <ts e="T241" id="Seg_3777" n="e" s="T240">(öʔliet). </ts>
            </ts>
            <ts e="T245" id="Seg_3778" n="sc" s="T242">
               <ts e="T243" id="Seg_3780" n="e" s="T242">Mo </ts>
               <ts e="T244" id="Seg_3782" n="e" s="T243">dereʔ </ts>
               <ts e="T245" id="Seg_3784" n="e" s="T244">ej? </ts>
            </ts>
            <ts e="T252" id="Seg_3785" n="sc" s="T246">
               <ts e="T247" id="Seg_3787" n="e" s="T246">Nu, </ts>
               <ts e="T248" id="Seg_3789" n="e" s="T247">măllat </ts>
               <ts e="T249" id="Seg_3791" n="e" s="T248">što </ts>
               <ts e="T250" id="Seg_3793" n="e" s="T249">bila </ts>
               <ts e="T251" id="Seg_3795" n="e" s="T250">kuzat </ts>
               <ts e="T252" id="Seg_3797" n="e" s="T251">tăn. </ts>
            </ts>
            <ts e="T257" id="Seg_3798" n="sc" s="T253">
               <ts e="T254" id="Seg_3800" n="e" s="T253">Mo </ts>
               <ts e="T255" id="Seg_3802" n="e" s="T254">dere </ts>
               <ts e="T256" id="Seg_3804" n="e" s="T255">bila </ts>
               <ts e="T257" id="Seg_3806" n="e" s="T256">kuza? </ts>
            </ts>
            <ts e="T261" id="Seg_3807" n="sc" s="T258">
               <ts e="T259" id="Seg_3809" n="e" s="T258">Măn </ts>
               <ts e="T260" id="Seg_3811" n="e" s="T259">jakše </ts>
               <ts e="T261" id="Seg_3813" n="e" s="T260">kuza. </ts>
            </ts>
            <ts e="T269" id="Seg_3814" n="sc" s="T262">
               <ts e="T263" id="Seg_3816" n="e" s="T262">Ĭmbi-nʼibudʼ </ts>
               <ts e="T264" id="Seg_3818" n="e" s="T263">tănan </ts>
               <ts e="T265" id="Seg_3820" n="e" s="T264">bar </ts>
               <ts e="T266" id="Seg_3822" n="e" s="T265">bɨ </ts>
               <ts e="T267" id="Seg_3824" n="e" s="T266">nörbəlam, </ts>
               <ts e="T268" id="Seg_3826" n="e" s="T267">nörbəlam </ts>
               <ts e="T269" id="Seg_3828" n="e" s="T268">bɨ. </ts>
            </ts>
            <ts e="T274" id="Seg_3829" n="sc" s="T270">
               <ts e="T271" id="Seg_3831" n="e" s="T270">Teʔi </ts>
               <ts e="T272" id="Seg_3833" n="e" s="T271">(tăŋ) </ts>
               <ts e="T273" id="Seg_3835" n="e" s="T272">jakše </ts>
               <ts e="T274" id="Seg_3837" n="e" s="T273">moləj. </ts>
            </ts>
            <ts e="T280" id="Seg_3838" n="sc" s="T275">
               <ts e="T276" id="Seg_3840" n="e" s="T275">Oh, </ts>
               <ts e="T277" id="Seg_3842" n="e" s="T276">nu </ts>
               <ts e="T278" id="Seg_3844" n="e" s="T277">dereʔ </ts>
               <ts e="T279" id="Seg_3846" n="e" s="T278">măllat </ts>
               <ts e="T280" id="Seg_3848" n="e" s="T279">măna. </ts>
            </ts>
            <ts e="T286" id="Seg_3849" n="sc" s="T281">
               <ts e="T282" id="Seg_3851" n="e" s="T281">Măn </ts>
               <ts e="T283" id="Seg_3853" n="e" s="T282">dĭm </ts>
               <ts e="T284" id="Seg_3855" n="e" s="T283">bar </ts>
               <ts e="T285" id="Seg_3857" n="e" s="T284">münörlam </ts>
               <ts e="T286" id="Seg_3859" n="e" s="T285">tăŋ. </ts>
            </ts>
            <ts e="T291" id="Seg_3860" n="sc" s="T287">
               <ts e="T288" id="Seg_3862" n="e" s="T287">Dʼok, </ts>
               <ts e="T289" id="Seg_3864" n="e" s="T288">münörzittə </ts>
               <ts e="T290" id="Seg_3866" n="e" s="T289">ne </ts>
               <ts e="T291" id="Seg_3868" n="e" s="T290">nado. </ts>
            </ts>
            <ts e="T297" id="Seg_3869" n="sc" s="T292">
               <ts e="T293" id="Seg_3871" n="e" s="T292">(A_to) </ts>
               <ts e="T294" id="Seg_3873" n="e" s="T293">dĭ </ts>
               <ts e="T295" id="Seg_3875" n="e" s="T294">măna </ts>
               <ts e="T296" id="Seg_3877" n="e" s="T295">münörlil </ts>
               <ts e="T297" id="Seg_3879" n="e" s="T296">dittəgaš. </ts>
            </ts>
            <ts e="T299" id="Seg_3880" n="sc" s="T298">
               <ts e="T299" id="Seg_3882" n="e" s="T298">((BRK)). </ts>
            </ts>
            <ts e="T304" id="Seg_3883" n="sc" s="T300">
               <ts e="T301" id="Seg_3885" n="e" s="T300">Măn </ts>
               <ts e="T302" id="Seg_3887" n="e" s="T301">tăŋ </ts>
               <ts e="T303" id="Seg_3889" n="e" s="T302">ara </ts>
               <ts e="T304" id="Seg_3891" n="e" s="T303">bĭtliem. </ts>
            </ts>
            <ts e="T309" id="Seg_3892" n="sc" s="T305">
               <ts e="T306" id="Seg_3894" n="e" s="T305">O, </ts>
               <ts e="T307" id="Seg_3896" n="e" s="T306">mo </ts>
               <ts e="T308" id="Seg_3898" n="e" s="T307">dereʔ </ts>
               <ts e="T309" id="Seg_3900" n="e" s="T308">tăn? </ts>
            </ts>
            <ts e="T314" id="Seg_3901" n="sc" s="T310">
               <ts e="T311" id="Seg_3903" n="e" s="T310">Nu, </ts>
               <ts e="T312" id="Seg_3905" n="e" s="T311">măn </ts>
               <ts e="T313" id="Seg_3907" n="e" s="T312">bile </ts>
               <ts e="T314" id="Seg_3909" n="e" s="T313">kuza. </ts>
            </ts>
            <ts e="T320" id="Seg_3910" n="sc" s="T315">
               <ts e="T316" id="Seg_3912" n="e" s="T315">Bar </ts>
               <ts e="T317" id="Seg_3914" n="e" s="T316">gĭda </ts>
               <ts e="T318" id="Seg_3916" n="e" s="T317">bar </ts>
               <ts e="T319" id="Seg_3918" n="e" s="T318">(iləm) </ts>
               <ts e="T320" id="Seg_3920" n="e" s="T319">ĭmbidə. </ts>
            </ts>
            <ts e="T328" id="Seg_3921" n="sc" s="T321">
               <ts e="T322" id="Seg_3923" n="e" s="T321">A </ts>
               <ts e="T323" id="Seg_3925" n="e" s="T322">mo </ts>
               <ts e="T324" id="Seg_3927" n="e" s="T323">(tănan) </ts>
               <ts e="T325" id="Seg_3929" n="e" s="T324">iləl, </ts>
               <ts e="T326" id="Seg_3931" n="e" s="T325">dak </ts>
               <ts e="T327" id="Seg_3933" n="e" s="T326">măn </ts>
               <ts e="T328" id="Seg_3935" n="e" s="T327">dĭttəgəš… </ts>
            </ts>
            <ts e="T332" id="Seg_3936" n="sc" s="T329">
               <ts e="T330" id="Seg_3938" n="e" s="T329">Măn </ts>
               <ts e="T331" id="Seg_3940" n="e" s="T330">dittəgəš </ts>
               <ts e="T332" id="Seg_3942" n="e" s="T331">iləm. </ts>
            </ts>
            <ts e="T338" id="Seg_3943" n="sc" s="T333">
               <ts e="T334" id="Seg_3945" n="e" s="T333">A </ts>
               <ts e="T335" id="Seg_3947" n="e" s="T334">mo </ts>
               <ts e="T336" id="Seg_3949" n="e" s="T335">dĭn </ts>
               <ts e="T337" id="Seg_3951" n="e" s="T336">dere </ts>
               <ts e="T338" id="Seg_3953" n="e" s="T337">iləl? </ts>
            </ts>
            <ts e="T343" id="Seg_3954" n="sc" s="T339">
               <ts e="T340" id="Seg_3956" n="e" s="T339">Dak </ts>
               <ts e="T341" id="Seg_3958" n="e" s="T340">vot </ts>
               <ts e="T342" id="Seg_3960" n="e" s="T341">iləm </ts>
               <ts e="T343" id="Seg_3962" n="e" s="T342">măn. </ts>
            </ts>
            <ts e="T350" id="Seg_3963" n="sc" s="T344">
               <ts e="T345" id="Seg_3965" n="e" s="T344">Măna </ts>
               <ts e="T346" id="Seg_3967" n="e" s="T345">dĭttəgəš </ts>
               <ts e="T347" id="Seg_3969" n="e" s="T346">ej </ts>
               <ts e="T348" id="Seg_3971" n="e" s="T347">kuvas </ts>
               <ts e="T349" id="Seg_3973" n="e" s="T348">turanə </ts>
               <ts e="T350" id="Seg_3975" n="e" s="T349">kunnallit. </ts>
            </ts>
            <ts e="T354" id="Seg_3976" n="sc" s="T351">
               <ts e="T352" id="Seg_3978" n="e" s="T351">Măn </ts>
               <ts e="T353" id="Seg_3980" n="e" s="T352">dĭgən </ts>
               <ts e="T354" id="Seg_3982" n="e" s="T353">amnolam. </ts>
            </ts>
            <ts e="T361" id="Seg_3983" n="sc" s="T355">
               <ts e="T356" id="Seg_3985" n="e" s="T355">A </ts>
               <ts e="T357" id="Seg_3987" n="e" s="T356">dĭttə </ts>
               <ts e="T358" id="Seg_3989" n="e" s="T357">kak </ts>
               <ts e="T359" id="Seg_3991" n="e" s="T358">(šoləm) </ts>
               <ts e="T360" id="Seg_3993" n="e" s="T359">măna </ts>
               <ts e="T361" id="Seg_3995" n="e" s="T360">il. </ts>
            </ts>
            <ts e="T364" id="Seg_3996" n="sc" s="T362">
               <ts e="T363" id="Seg_3998" n="e" s="T362">Šindĭdə </ts>
               <ts e="T364" id="Seg_4000" n="e" s="T363">ej… </ts>
            </ts>
            <ts e="T366" id="Seg_4001" n="sc" s="T365">
               <ts e="T366" id="Seg_4003" n="e" s="T365">Ej… </ts>
            </ts>
            <ts e="T371" id="Seg_4004" n="sc" s="T367">
               <ts e="T368" id="Seg_4006" n="e" s="T367">Вот </ts>
               <ts e="T369" id="Seg_4008" n="e" s="T368">ведь, </ts>
               <ts e="T370" id="Seg_4010" n="e" s="T369">не </ts>
               <ts e="T371" id="Seg_4012" n="e" s="T370">выговорить. </ts>
            </ts>
            <ts e="T374" id="Seg_4013" n="sc" s="T372">
               <ts e="T373" id="Seg_4015" n="e" s="T372">Ej </ts>
               <ts e="T374" id="Seg_4017" n="e" s="T373">iliet. </ts>
            </ts>
            <ts e="T382" id="Seg_4018" n="sc" s="T375">
               <ts e="T376" id="Seg_4020" n="e" s="T375">Bar </ts>
               <ts e="T377" id="Seg_4022" n="e" s="T376">il </ts>
               <ts e="T378" id="Seg_4024" n="e" s="T377">(münörlat) </ts>
               <ts e="T379" id="Seg_4026" n="e" s="T378">(i) </ts>
               <ts e="T380" id="Seg_4028" n="e" s="T379">ej </ts>
               <ts e="T381" id="Seg_4030" n="e" s="T380">jakše </ts>
               <ts e="T382" id="Seg_4032" n="e" s="T381">kuza. </ts>
            </ts>
            <ts e="T384" id="Seg_4033" n="sc" s="T383">
               <ts e="T384" id="Seg_4035" n="e" s="T383">Vot. </ts>
            </ts>
            <ts e="T386" id="Seg_4036" n="sc" s="T385">
               <ts e="T386" id="Seg_4038" n="e" s="T385">((BRK)). </ts>
            </ts>
            <ts e="T398" id="Seg_4039" n="sc" s="T387">
               <ts e="T388" id="Seg_4041" n="e" s="T387">Bile </ts>
               <ts e="T389" id="Seg_4043" n="e" s="T388">kuza </ts>
               <ts e="T390" id="Seg_4045" n="e" s="T389">jakše </ts>
               <ts e="T391" id="Seg_4047" n="e" s="T390">kuzan </ts>
               <ts e="T392" id="Seg_4049" n="e" s="T391">ibi </ts>
               <ts e="T393" id="Seg_4051" n="e" s="T392">kujnek, </ts>
               <ts e="T394" id="Seg_4053" n="e" s="T393">uja, </ts>
               <ts e="T395" id="Seg_4055" n="e" s="T394">i </ts>
               <ts e="T396" id="Seg_4057" n="e" s="T395">bar </ts>
               <ts e="T397" id="Seg_4059" n="e" s="T396">ĭmbi, </ts>
               <ts e="T398" id="Seg_4061" n="e" s="T397">bar. </ts>
            </ts>
            <ts e="T403" id="Seg_4062" n="sc" s="T399">
               <ts e="T400" id="Seg_4064" n="e" s="T399">Aktʼat </ts>
               <ts e="T401" id="Seg_4066" n="e" s="T400">(mobi) </ts>
               <ts e="T402" id="Seg_4068" n="e" s="T401">dĭ </ts>
               <ts e="T403" id="Seg_4070" n="e" s="T402">kuzan. </ts>
            </ts>
            <ts e="T406" id="Seg_4071" n="sc" s="T404">
               <ts e="T405" id="Seg_4073" n="e" s="T404">Bar </ts>
               <ts e="T406" id="Seg_4075" n="e" s="T405">ibi. </ts>
            </ts>
            <ts e="T410" id="Seg_4076" n="sc" s="T407">
               <ts e="T408" id="Seg_4078" n="e" s="T407">Ĭmbi </ts>
               <ts e="T409" id="Seg_4080" n="e" s="T408">tăn </ts>
               <ts e="T410" id="Seg_4082" n="e" s="T409">ibiel? </ts>
            </ts>
            <ts e="T417" id="Seg_4083" n="sc" s="T411">
               <ts e="T412" id="Seg_4085" n="e" s="T411">Dak </ts>
               <ts e="T413" id="Seg_4087" n="e" s="T412">măn </ts>
               <ts e="T414" id="Seg_4089" n="e" s="T413">naga, </ts>
               <ts e="T415" id="Seg_4091" n="e" s="T414">a </ts>
               <ts e="T416" id="Seg_4093" n="e" s="T415">tăn </ts>
               <ts e="T417" id="Seg_4095" n="e" s="T416">ige. </ts>
            </ts>
            <ts e="T421" id="Seg_4096" n="sc" s="T418">
               <ts e="T419" id="Seg_4098" n="e" s="T418">(Vo), </ts>
               <ts e="T420" id="Seg_4100" n="e" s="T419">măn </ts>
               <ts e="T421" id="Seg_4102" n="e" s="T420">ibiem. </ts>
            </ts>
            <ts e="T427" id="Seg_4103" n="sc" s="T422">
               <ts e="T423" id="Seg_4105" n="e" s="T422">A </ts>
               <ts e="T424" id="Seg_4107" n="e" s="T423">măn </ts>
               <ts e="T425" id="Seg_4109" n="e" s="T424">tănan </ts>
               <ts e="T426" id="Seg_4111" n="e" s="T425">ej </ts>
               <ts e="T427" id="Seg_4113" n="e" s="T426">iləm. </ts>
            </ts>
            <ts e="T431" id="Seg_4114" n="sc" s="T428">
               <ts e="T429" id="Seg_4116" n="e" s="T428">Măn </ts>
               <ts e="T430" id="Seg_4118" n="e" s="T429">tănan </ts>
               <ts e="T431" id="Seg_4120" n="e" s="T430">iləm. </ts>
            </ts>
            <ts e="T437" id="Seg_4121" n="sc" s="T432">
               <ts e="T433" id="Seg_4123" n="e" s="T432">A </ts>
               <ts e="T434" id="Seg_4125" n="e" s="T433">moʔ </ts>
               <ts e="T435" id="Seg_4127" n="e" s="T434">(nĭn) </ts>
               <ts e="T436" id="Seg_4129" n="e" s="T435">măna </ts>
               <ts e="T437" id="Seg_4131" n="e" s="T436">iləl? </ts>
            </ts>
            <ts e="T442" id="Seg_4132" n="sc" s="T438">
               <ts e="T439" id="Seg_4134" n="e" s="T438">Dak </ts>
               <ts e="T440" id="Seg_4136" n="e" s="T439">nĭn </ts>
               <ts e="T441" id="Seg_4138" n="e" s="T440">na_što </ts>
               <ts e="T442" id="Seg_4140" n="e" s="T441">ibiel? </ts>
            </ts>
            <ts e="T449" id="Seg_4141" n="sc" s="T443">
               <ts e="T444" id="Seg_4143" n="e" s="T443">Măn </ts>
               <ts e="T445" id="Seg_4145" n="e" s="T444">kunnaʔbə </ts>
               <ts e="T446" id="Seg_4147" n="e" s="T445">i </ts>
               <ts e="T447" id="Seg_4149" n="e" s="T446">bar </ts>
               <ts e="T448" id="Seg_4151" n="e" s="T447">ĭmbim </ts>
               <ts e="T449" id="Seg_4153" n="e" s="T448">bar. </ts>
            </ts>
            <ts e="T457" id="Seg_4154" n="sc" s="T450">
               <ts e="T451" id="Seg_4156" n="e" s="T450">A </ts>
               <ts e="T452" id="Seg_4158" n="e" s="T451">măn </ts>
               <ts e="T453" id="Seg_4160" n="e" s="T452">bile </ts>
               <ts e="T454" id="Seg_4162" n="e" s="T453">turanə </ts>
               <ts e="T455" id="Seg_4164" n="e" s="T454">tănan </ts>
               <ts e="T456" id="Seg_4166" n="e" s="T455">amnolaʔbə </ts>
               <ts e="T457" id="Seg_4168" n="e" s="T456">((…)). </ts>
            </ts>
            <ts e="T459" id="Seg_4169" n="sc" s="T458">
               <ts e="T459" id="Seg_4171" n="e" s="T458">Vot. </ts>
            </ts>
            <ts e="T461" id="Seg_4172" n="sc" s="T460">
               <ts e="T461" id="Seg_4174" n="e" s="T460">((BRK)). </ts>
            </ts>
            <ts e="T465" id="Seg_4175" n="sc" s="T462">
               <ts e="T463" id="Seg_4177" n="e" s="T462">Amnoʔ </ts>
               <ts e="T464" id="Seg_4179" n="e" s="T463">mănzi </ts>
               <ts e="T465" id="Seg_4181" n="e" s="T464">amorzittə. </ts>
            </ts>
            <ts e="T467" id="Seg_4182" n="sc" s="T466">
               <ts e="T467" id="Seg_4184" n="e" s="T466">Oj. </ts>
            </ts>
            <ts e="T471" id="Seg_4185" n="sc" s="T468">
               <ts e="T469" id="Seg_4187" n="e" s="T468">Чего </ts>
               <ts e="T470" id="Seg_4189" n="e" s="T469">я </ts>
               <ts e="T471" id="Seg_4191" n="e" s="T470">это. </ts>
            </ts>
            <ts e="T475" id="Seg_4192" n="sc" s="T472">
               <ts e="T473" id="Seg_4194" n="e" s="T472">Нет, </ts>
               <ts e="T474" id="Seg_4196" n="e" s="T473">не </ts>
               <ts e="T475" id="Seg_4198" n="e" s="T474">так. </ts>
            </ts>
            <ts e="T477" id="Seg_4199" n="sc" s="T476">
               <ts e="T477" id="Seg_4201" n="e" s="T476">((BRK)). </ts>
            </ts>
            <ts e="T481" id="Seg_4202" n="sc" s="T478">
               <ts e="T479" id="Seg_4204" n="e" s="T478">Amnoʔ </ts>
               <ts e="T480" id="Seg_4206" n="e" s="T479">mănzi </ts>
               <ts e="T481" id="Seg_4208" n="e" s="T480">amorzittə. </ts>
            </ts>
            <ts e="T486" id="Seg_4209" n="sc" s="T482">
               <ts e="T483" id="Seg_4211" n="e" s="T482">Da </ts>
               <ts e="T484" id="Seg_4213" n="e" s="T483">ĭmbi </ts>
               <ts e="T485" id="Seg_4215" n="e" s="T484">tăn </ts>
               <ts e="T486" id="Seg_4217" n="e" s="T485">molat? </ts>
            </ts>
            <ts e="T494" id="Seg_4218" n="sc" s="T487">
               <ts e="T488" id="Seg_4220" n="e" s="T487">Da </ts>
               <ts e="T489" id="Seg_4222" n="e" s="T488">măn </ts>
               <ts e="T490" id="Seg_4224" n="e" s="T489">(molat) </ts>
               <ts e="T491" id="Seg_4226" n="e" s="T490">ipek, </ts>
               <ts e="T492" id="Seg_4228" n="e" s="T491">uja, </ts>
               <ts e="T493" id="Seg_4230" n="e" s="T492">i </ts>
               <ts e="T494" id="Seg_4232" n="e" s="T493">süt. </ts>
            </ts>
            <ts e="T496" id="Seg_4233" n="sc" s="T495">
               <ts e="T496" id="Seg_4235" n="e" s="T495">Sĭrepne. </ts>
            </ts>
            <ts e="T498" id="Seg_4236" n="sc" s="T497">
               <ts e="T498" id="Seg_4238" n="e" s="T497">Amdə. </ts>
            </ts>
            <ts e="T505" id="Seg_4239" n="sc" s="T499">
               <ts e="T500" id="Seg_4241" n="e" s="T499">Dak </ts>
               <ts e="T501" id="Seg_4243" n="e" s="T500">măn </ts>
               <ts e="T502" id="Seg_4245" n="e" s="T501">bɨ </ts>
               <ts e="T503" id="Seg_4247" n="e" s="T502">kajak </ts>
               <ts e="T504" id="Seg_4249" n="e" s="T503">amorliam </ts>
               <ts e="T505" id="Seg_4251" n="e" s="T504">bɨ. </ts>
            </ts>
            <ts e="T513" id="Seg_4252" n="sc" s="T506">
               <ts e="T507" id="Seg_4254" n="e" s="T506">Nu </ts>
               <ts e="T508" id="Seg_4256" n="e" s="T507">i </ts>
               <ts e="T509" id="Seg_4258" n="e" s="T508">kajak </ts>
               <ts e="T510" id="Seg_4260" n="e" s="T509">măn </ts>
               <ts e="T511" id="Seg_4262" n="e" s="T510">mĭlem </ts>
               <ts e="T512" id="Seg_4264" n="e" s="T511">tănan </ts>
               <ts e="T513" id="Seg_4266" n="e" s="T512">amorzittə. </ts>
            </ts>
            <ts e="T517" id="Seg_4267" n="sc" s="T514">
               <ts e="T515" id="Seg_4269" n="e" s="T514">Jakše </ts>
               <ts e="T516" id="Seg_4271" n="e" s="T515">tolʼkă </ts>
               <ts e="T517" id="Seg_4273" n="e" s="T516">amoraʔ. </ts>
            </ts>
            <ts e="T522" id="Seg_4274" n="sc" s="T518">
               <ts e="T519" id="Seg_4276" n="e" s="T518">(Dö) </ts>
               <ts e="T520" id="Seg_4278" n="e" s="T519">dak </ts>
               <ts e="T521" id="Seg_4280" n="e" s="T520">măn </ts>
               <ts e="T522" id="Seg_4282" n="e" s="T521">(amorlam). </ts>
            </ts>
            <ts e="T526" id="Seg_4283" n="sc" s="T523">
               <ts e="T524" id="Seg_4285" n="e" s="T523">(Tăŋ=) </ts>
               <ts e="T525" id="Seg_4287" n="e" s="T524">Tăŋ </ts>
               <ts e="T526" id="Seg_4289" n="e" s="T525">amorlam. </ts>
            </ts>
            <ts e="T529" id="Seg_4290" n="sc" s="T527">
               <ts e="T528" id="Seg_4292" n="e" s="T527">Jakše </ts>
               <ts e="T529" id="Seg_4294" n="e" s="T528">amorliam. </ts>
            </ts>
            <ts e="T531" id="Seg_4295" n="sc" s="T530">
               <ts e="T531" id="Seg_4297" n="e" s="T530">Vo. </ts>
            </ts>
            <ts e="T533" id="Seg_4298" n="sc" s="T532">
               <ts e="T533" id="Seg_4300" n="e" s="T532">((BRK)). </ts>
            </ts>
            <ts e="T542" id="Seg_4301" n="sc" s="T534">
               <ts e="T535" id="Seg_4303" n="e" s="T534">(Mo) </ts>
               <ts e="T536" id="Seg_4305" n="e" s="T535">tăn </ts>
               <ts e="T537" id="Seg_4307" n="e" s="T536">măn </ts>
               <ts e="T538" id="Seg_4309" n="e" s="T537">eʔbdem </ts>
               <ts e="T539" id="Seg_4311" n="e" s="T538">bar </ts>
               <ts e="T540" id="Seg_4313" n="e" s="T539">bos </ts>
               <ts e="T541" id="Seg_4315" n="e" s="T540">(udanə) </ts>
               <ts e="T542" id="Seg_4317" n="e" s="T541">ibiel? </ts>
            </ts>
            <ts e="T550" id="Seg_4318" n="sc" s="T543">
               <ts e="T544" id="Seg_4320" n="e" s="T543">Vedʼ </ts>
               <ts e="T545" id="Seg_4322" n="e" s="T544">tăn </ts>
               <ts e="T546" id="Seg_4324" n="e" s="T545">udal </ts>
               <ts e="T547" id="Seg_4326" n="e" s="T546">urgo, </ts>
               <ts e="T548" id="Seg_4328" n="e" s="T547">a </ts>
               <ts e="T549" id="Seg_4330" n="e" s="T548">măn </ts>
               <ts e="T550" id="Seg_4332" n="e" s="T549">idiʼije. </ts>
            </ts>
            <ts e="T554" id="Seg_4333" n="sc" s="T551">
               <ts e="T552" id="Seg_4335" n="e" s="T551">Mo </ts>
               <ts e="T553" id="Seg_4337" n="e" s="T552">dereʔ </ts>
               <ts e="T554" id="Seg_4339" n="e" s="T553">molal? </ts>
            </ts>
            <ts e="T556" id="Seg_4340" n="sc" s="T555">
               <ts e="T556" id="Seg_4342" n="e" s="T555">Bile. </ts>
            </ts>
            <ts e="T560" id="Seg_4343" n="sc" s="T557">
               <ts e="T558" id="Seg_4345" n="e" s="T557">Ej </ts>
               <ts e="T559" id="Seg_4347" n="e" s="T558">jakše </ts>
               <ts e="T560" id="Seg_4349" n="e" s="T559">dereʔ. </ts>
            </ts>
            <ts e="T562" id="Seg_4350" n="sc" s="T561">
               <ts e="T562" id="Seg_4352" n="e" s="T561">Vot. </ts>
            </ts>
            <ts e="T564" id="Seg_4353" n="sc" s="T563">
               <ts e="T564" id="Seg_4355" n="e" s="T563">((BRK)). </ts>
            </ts>
            <ts e="T568" id="Seg_4356" n="sc" s="T565">
               <ts e="T566" id="Seg_4358" n="e" s="T565">Măn </ts>
               <ts e="T567" id="Seg_4360" n="e" s="T566">tăŋ </ts>
               <ts e="T568" id="Seg_4362" n="e" s="T567">(ĭzemneʔbiem). </ts>
            </ts>
            <ts e="T571" id="Seg_4363" n="sc" s="T569">
               <ts e="T570" id="Seg_4365" n="e" s="T569">Udazambə </ts>
               <ts e="T571" id="Seg_4367" n="e" s="T570">ĭzəmniet. </ts>
            </ts>
            <ts e="T576" id="Seg_4368" n="sc" s="T572">
               <ts e="T573" id="Seg_4370" n="e" s="T572">I </ts>
               <ts e="T574" id="Seg_4372" n="e" s="T573">ujubə </ts>
               <ts e="T575" id="Seg_4374" n="e" s="T574">bar </ts>
               <ts e="T576" id="Seg_4376" n="e" s="T575">ĭzemneʔbəliet. </ts>
            </ts>
            <ts e="T580" id="Seg_4377" n="sc" s="T577">
               <ts e="T578" id="Seg_4379" n="e" s="T577">Ădnakă </ts>
               <ts e="T579" id="Seg_4381" n="e" s="T578">măn </ts>
               <ts e="T580" id="Seg_4383" n="e" s="T579">(külambil). </ts>
            </ts>
            <ts e="T585" id="Seg_4384" n="sc" s="T581">
               <ts e="T582" id="Seg_4386" n="e" s="T581">Dʼok, </ts>
               <ts e="T583" id="Seg_4388" n="e" s="T582">nʼe </ts>
               <ts e="T584" id="Seg_4390" n="e" s="T583">nada </ts>
               <ts e="T585" id="Seg_4392" n="e" s="T584">külamzittə. </ts>
            </ts>
            <ts e="T588" id="Seg_4393" n="sc" s="T586">
               <ts e="T587" id="Seg_4395" n="e" s="T586">Nada </ts>
               <ts e="T588" id="Seg_4397" n="e" s="T587">amnozittə. </ts>
            </ts>
            <ts e="T591" id="Seg_4398" n="sc" s="T589">
               <ts e="T590" id="Seg_4400" n="e" s="T589">Amnozittə </ts>
               <ts e="T591" id="Seg_4402" n="e" s="T590">jakše. </ts>
            </ts>
            <ts e="T595" id="Seg_4403" n="sc" s="T592">
               <ts e="T593" id="Seg_4405" n="e" s="T592">A </ts>
               <ts e="T594" id="Seg_4407" n="e" s="T593">(külamzittə) </ts>
               <ts e="T595" id="Seg_4409" n="e" s="T594">bile. </ts>
            </ts>
            <ts e="T598" id="Seg_4410" n="sc" s="T596">
               <ts e="T597" id="Seg_4412" n="e" s="T596">Tăŋ </ts>
               <ts e="T598" id="Seg_4414" n="e" s="T597">bile. </ts>
            </ts>
            <ts e="T600" id="Seg_4415" n="sc" s="T599">
               <ts e="T600" id="Seg_4417" n="e" s="T599">Vot. </ts>
            </ts>
            <ts e="T602" id="Seg_4418" n="sc" s="T601">
               <ts e="T602" id="Seg_4420" n="e" s="T601">((BRK)). </ts>
            </ts>
            <ts e="T606" id="Seg_4421" n="sc" s="T603">
               <ts e="T604" id="Seg_4423" n="e" s="T603">Šide </ts>
               <ts e="T605" id="Seg_4425" n="e" s="T604">nʼi </ts>
               <ts e="T606" id="Seg_4427" n="e" s="T605">amnobiʔi. </ts>
            </ts>
            <ts e="T614" id="Seg_4428" n="sc" s="T607">
               <ts e="T608" id="Seg_4430" n="e" s="T607">Dĭttəgəš </ts>
               <ts e="T609" id="Seg_4432" n="e" s="T608">dĭ </ts>
               <ts e="T610" id="Seg_4434" n="e" s="T609">nʼi </ts>
               <ts e="T611" id="Seg_4436" n="e" s="T610">gibərdə </ts>
               <ts e="T612" id="Seg_4438" n="e" s="T611">ari </ts>
               <ts e="T613" id="Seg_4440" n="e" s="T612">dʼürzittə </ts>
               <ts e="T614" id="Seg_4442" n="e" s="T613">molat. </ts>
            </ts>
            <ts e="T619" id="Seg_4443" n="sc" s="T615">
               <ts e="T616" id="Seg_4445" n="e" s="T615">A </ts>
               <ts e="T617" id="Seg_4447" n="e" s="T616">oʔb </ts>
               <ts e="T618" id="Seg_4449" n="e" s="T617">turagənən </ts>
               <ts e="T619" id="Seg_4451" n="e" s="T618">amnolaʔpliet. </ts>
            </ts>
            <ts e="T626" id="Seg_4452" n="sc" s="T620">
               <ts e="T621" id="Seg_4454" n="e" s="T620">Nu, </ts>
               <ts e="T622" id="Seg_4456" n="e" s="T621">mĭlat: </ts>
               <ts e="T623" id="Seg_4458" n="e" s="T622">tănan </ts>
               <ts e="T624" id="Seg_4460" n="e" s="T623">ine, </ts>
               <ts e="T625" id="Seg_4462" n="e" s="T624">măna </ts>
               <ts e="T626" id="Seg_4464" n="e" s="T625">tüžöj. </ts>
            </ts>
            <ts e="T634" id="Seg_4465" n="sc" s="T627">
               <ts e="T628" id="Seg_4467" n="e" s="T627">Dʼok, </ts>
               <ts e="T629" id="Seg_4469" n="e" s="T628">tăn </ts>
               <ts e="T630" id="Seg_4471" n="e" s="T629">it </ts>
               <ts e="T631" id="Seg_4473" n="e" s="T630">ine, </ts>
               <ts e="T632" id="Seg_4475" n="e" s="T631">a </ts>
               <ts e="T633" id="Seg_4477" n="e" s="T632">măn </ts>
               <ts e="T634" id="Seg_4479" n="e" s="T633">tüžöj. </ts>
            </ts>
            <ts e="T637" id="Seg_4480" n="sc" s="T635">
               <ts e="T636" id="Seg_4482" n="e" s="T635">O, </ts>
               <ts e="T637" id="Seg_4484" n="e" s="T636">enejdənə! </ts>
            </ts>
            <ts e="T640" id="Seg_4485" n="sc" s="T638">
               <ts e="T639" id="Seg_4487" n="e" s="T638">Mo </ts>
               <ts e="T640" id="Seg_4489" n="e" s="T639">dereʔ? </ts>
            </ts>
            <ts e="T644" id="Seg_4490" n="sc" s="T641">
               <ts e="T642" id="Seg_4492" n="e" s="T641">Nu, </ts>
               <ts e="T643" id="Seg_4494" n="e" s="T642">tăn </ts>
               <ts e="T644" id="Seg_4496" n="e" s="T643">koško! </ts>
            </ts>
            <ts e="T646" id="Seg_4497" n="sc" s="T645">
               <ts e="T646" id="Seg_4499" n="e" s="T645">Eneidənəl. </ts>
            </ts>
            <ts e="T650" id="Seg_4500" n="sc" s="T647">
               <ts e="T648" id="Seg_4502" n="e" s="T647">Mo </ts>
               <ts e="T649" id="Seg_4504" n="e" s="T648">dereʔ </ts>
               <ts e="T650" id="Seg_4506" n="e" s="T649">kudonzlial? </ts>
            </ts>
            <ts e="T656" id="Seg_4507" n="sc" s="T651">
               <ts e="T652" id="Seg_4509" n="e" s="T651">Dak </ts>
               <ts e="T653" id="Seg_4511" n="e" s="T652">nada </ts>
               <ts e="T654" id="Seg_4513" n="e" s="T653">dereʔ </ts>
               <ts e="T655" id="Seg_4515" n="e" s="T654">kudonzittə, </ts>
               <ts e="T656" id="Seg_4517" n="e" s="T655">bile. </ts>
            </ts>
            <ts e="T666" id="Seg_4518" n="sc" s="T657">
               <ts e="T658" id="Seg_4520" n="e" s="T657">A </ts>
               <ts e="T659" id="Seg_4522" n="e" s="T658">măna </ts>
               <ts e="T660" id="Seg_4524" n="e" s="T659">vedʼ </ts>
               <ts e="T661" id="Seg_4526" n="e" s="T660">abam </ts>
               <ts e="T662" id="Seg_4528" n="e" s="T661">mĭbie </ts>
               <ts e="T663" id="Seg_4530" n="e" s="T662">ine, </ts>
               <ts e="T664" id="Seg_4532" n="e" s="T663">a </ts>
               <ts e="T665" id="Seg_4534" n="e" s="T664">tănan </ts>
               <ts e="T666" id="Seg_4536" n="e" s="T665">tüžöj. </ts>
            </ts>
            <ts e="T675" id="Seg_4537" n="sc" s="T667">
               <ts e="T668" id="Seg_4539" n="e" s="T667">(Măbi): </ts>
               <ts e="T669" id="Seg_4541" n="e" s="T668">tăn </ts>
               <ts e="T670" id="Seg_4543" n="e" s="T669">it </ts>
               <ts e="T671" id="Seg_4545" n="e" s="T670">ine, </ts>
               <ts e="T672" id="Seg_4547" n="e" s="T671">a </ts>
               <ts e="T673" id="Seg_4549" n="e" s="T672">tăn </ts>
               <ts e="T674" id="Seg_4551" n="e" s="T673">it </ts>
               <ts e="T675" id="Seg_4553" n="e" s="T674">tüžöj. </ts>
            </ts>
            <ts e="T679" id="Seg_4554" n="sc" s="T676">
               <ts e="T677" id="Seg_4556" n="e" s="T676">A </ts>
               <ts e="T678" id="Seg_4558" n="e" s="T677">tura </ts>
               <ts e="T679" id="Seg_4560" n="e" s="T678">tănan. </ts>
            </ts>
            <ts e="T685" id="Seg_4561" n="sc" s="T680">
               <ts e="T681" id="Seg_4563" n="e" s="T680">I </ts>
               <ts e="T682" id="Seg_4565" n="e" s="T681">tura, </ts>
               <ts e="T683" id="Seg_4567" n="e" s="T682">i </ts>
               <ts e="T684" id="Seg_4569" n="e" s="T683">tüžöj </ts>
               <ts e="T685" id="Seg_4571" n="e" s="T684">tănan. </ts>
            </ts>
            <ts e="T691" id="Seg_4572" n="sc" s="T686">
               <ts e="T687" id="Seg_4574" n="e" s="T686">A </ts>
               <ts e="T688" id="Seg_4576" n="e" s="T687">tüj </ts>
               <ts e="T689" id="Seg_4578" n="e" s="T688">tüžöj </ts>
               <ts e="T690" id="Seg_4580" n="e" s="T689">ej </ts>
               <ts e="T691" id="Seg_4582" n="e" s="T690">iliet. </ts>
            </ts>
            <ts e="T694" id="Seg_4583" n="sc" s="T692">
               <ts e="T693" id="Seg_4585" n="e" s="T692">Ну </ts>
               <ts e="T694" id="Seg_4587" n="e" s="T693">вот. </ts>
            </ts>
            <ts e="T696" id="Seg_4588" n="sc" s="T695">
               <ts e="T696" id="Seg_4590" n="e" s="T695">((BRK)). </ts>
            </ts>
            <ts e="T698" id="Seg_4591" n="sc" s="T697">
               <ts e="T698" id="Seg_4593" n="e" s="T697">Ой. </ts>
            </ts>
            <ts e="T702" id="Seg_4594" n="sc" s="T699">
               <ts e="T700" id="Seg_4596" n="e" s="T699">Bar </ts>
               <ts e="T701" id="Seg_4598" n="e" s="T700">gĭda </ts>
               <ts e="T702" id="Seg_4600" n="e" s="T701">bar. </ts>
            </ts>
            <ts e="T709" id="Seg_4601" n="sc" s="T703">
               <ts e="T704" id="Seg_4603" n="e" s="T703">Bar </ts>
               <ts e="T705" id="Seg_4605" n="e" s="T704">il </ts>
               <ts e="T706" id="Seg_4607" n="e" s="T705">(ur-) </ts>
               <ts e="T707" id="Seg_4609" n="e" s="T706">urgo </ts>
               <ts e="T708" id="Seg_4611" n="e" s="T707">il </ts>
               <ts e="T709" id="Seg_4613" n="e" s="T708">(ibiʔjə). </ts>
            </ts>
            <ts e="T713" id="Seg_4614" n="sc" s="T710">
               <ts e="T711" id="Seg_4616" n="e" s="T710">Măn </ts>
               <ts e="T712" id="Seg_4618" n="e" s="T711">ilbə </ts>
               <ts e="T713" id="Seg_4620" n="e" s="T712">(ibiʔjə). </ts>
            </ts>
            <ts e="T716" id="Seg_4621" n="sc" s="T714">
               <ts e="T715" id="Seg_4623" n="e" s="T714">I </ts>
               <ts e="T716" id="Seg_4625" n="e" s="T715">gibər? </ts>
            </ts>
            <ts e="T720" id="Seg_4626" n="sc" s="T717">
               <ts e="T718" id="Seg_4628" n="e" s="T717">Эх, </ts>
               <ts e="T719" id="Seg_4630" n="e" s="T718">gibər, </ts>
               <ts e="T720" id="Seg_4632" n="e" s="T719">gibər. </ts>
            </ts>
            <ts e="T724" id="Seg_4633" n="sc" s="T721">
               <ts e="T722" id="Seg_4635" n="e" s="T721">Dĭzeŋ </ts>
               <ts e="T723" id="Seg_4637" n="e" s="T722">dʼörlapliet, </ts>
               <ts e="T724" id="Seg_4639" n="e" s="T723">dʼörlapliet. </ts>
            </ts>
            <ts e="T730" id="Seg_4640" n="sc" s="T725">
               <ts e="T726" id="Seg_4642" n="e" s="T725">Bos </ts>
               <ts e="T727" id="Seg_4644" n="e" s="T726">turagəʔ </ts>
               <ts e="T728" id="Seg_4646" n="e" s="T727">dĭzem </ts>
               <ts e="T729" id="Seg_4648" n="e" s="T728">bar </ts>
               <ts e="T730" id="Seg_4650" n="e" s="T729">ibiʔjə. </ts>
            </ts>
            <ts e="T734" id="Seg_4651" n="sc" s="T731">
               <ts e="T732" id="Seg_4653" n="e" s="T731">A </ts>
               <ts e="T733" id="Seg_4655" n="e" s="T732">ĭmbi </ts>
               <ts e="T734" id="Seg_4657" n="e" s="T733">ibiʔi? </ts>
            </ts>
            <ts e="T737" id="Seg_4658" n="sc" s="T735">
               <ts e="T736" id="Seg_4660" n="e" s="T735">Dak </ts>
               <ts e="T737" id="Seg_4662" n="e" s="T736">ĭmbi. </ts>
            </ts>
            <ts e="T739" id="Seg_4663" n="sc" s="T738">
               <ts e="T739" id="Seg_4665" n="e" s="T738">Kudonzəbiʔi. </ts>
            </ts>
            <ts e="T745" id="Seg_4666" n="sc" s="T740">
               <ts e="T741" id="Seg_4668" n="e" s="T740">I </ts>
               <ts e="T742" id="Seg_4670" n="e" s="T741">bar </ts>
               <ts e="T743" id="Seg_4672" n="e" s="T742">gĭda </ts>
               <ts e="T744" id="Seg_4674" n="e" s="T743">bar </ts>
               <ts e="T745" id="Seg_4676" n="e" s="T744">măllaplieʔi. </ts>
            </ts>
            <ts e="T750" id="Seg_4677" n="sc" s="T746">
               <ts e="T747" id="Seg_4679" n="e" s="T746">Vot </ts>
               <ts e="T748" id="Seg_4681" n="e" s="T747">dĭzem </ts>
               <ts e="T749" id="Seg_4683" n="e" s="T748">i </ts>
               <ts e="T750" id="Seg_4685" n="e" s="T749">ibiʔi. </ts>
            </ts>
            <ts e="T753" id="Seg_4686" n="sc" s="T751">
               <ts e="T752" id="Seg_4688" n="e" s="T751">A </ts>
               <ts e="T753" id="Seg_4690" n="e" s="T752">gibər? </ts>
            </ts>
            <ts e="T758" id="Seg_4691" n="sc" s="T754">
               <ts e="T755" id="Seg_4693" n="e" s="T754">Nu </ts>
               <ts e="T756" id="Seg_4695" n="e" s="T755">măn </ts>
               <ts e="T757" id="Seg_4697" n="e" s="T756">tĭmnem </ts>
               <ts e="T758" id="Seg_4699" n="e" s="T757">gibər? </ts>
            </ts>
            <ts e="T763" id="Seg_4700" n="sc" s="T759">
               <ts e="T760" id="Seg_4702" n="e" s="T759">Măn </ts>
               <ts e="T761" id="Seg_4704" n="e" s="T760">že </ts>
               <ts e="T762" id="Seg_4706" n="e" s="T761">ej </ts>
               <ts e="T763" id="Seg_4708" n="e" s="T762">tĭmnem. </ts>
            </ts>
            <ts e="T765" id="Seg_4709" n="sc" s="T764">
               <ts e="T765" id="Seg_4711" n="e" s="T764">((BRK)). </ts>
            </ts>
            <ts e="T769" id="Seg_4712" n="sc" s="T766">
               <ts e="T767" id="Seg_4714" n="e" s="T766">Kanžəbəj </ts>
               <ts e="T768" id="Seg_4716" n="e" s="T767">(mʼeškaʔi) </ts>
               <ts e="T769" id="Seg_4718" n="e" s="T768">izittə. </ts>
            </ts>
            <ts e="T772" id="Seg_4719" n="sc" s="T770">
               <ts e="T771" id="Seg_4721" n="e" s="T770">Mʼeška </ts>
               <ts e="T772" id="Seg_4723" n="e" s="T771">kuvas. </ts>
            </ts>
            <ts e="T777" id="Seg_4724" n="sc" s="T773">
               <ts e="T774" id="Seg_4726" n="e" s="T773">A </ts>
               <ts e="T775" id="Seg_4728" n="e" s="T774">ĭmbi </ts>
               <ts e="T776" id="Seg_4730" n="e" s="T775">(molə) </ts>
               <ts e="T777" id="Seg_4732" n="e" s="T776">dereʔ? </ts>
            </ts>
            <ts e="T781" id="Seg_4733" n="sc" s="T778">
               <ts e="T779" id="Seg_4735" n="e" s="T778">O </ts>
               <ts e="T780" id="Seg_4737" n="e" s="T779">dak, </ts>
               <ts e="T781" id="Seg_4739" n="e" s="T780">ĭmbi. </ts>
            </ts>
            <ts e="T788" id="Seg_4740" n="sc" s="T782">
               <ts e="T783" id="Seg_4742" n="e" s="T782">A </ts>
               <ts e="T784" id="Seg_4744" n="e" s="T783">măn </ts>
               <ts e="T785" id="Seg_4746" n="e" s="T784">dĭttə </ts>
               <ts e="T786" id="Seg_4748" n="e" s="T785">mĭnzerləm, </ts>
               <ts e="T787" id="Seg_4750" n="e" s="T786">amorzittə </ts>
               <ts e="T788" id="Seg_4752" n="e" s="T787">moləm. </ts>
            </ts>
            <ts e="T792" id="Seg_4753" n="sc" s="T789">
               <ts e="T790" id="Seg_4755" n="e" s="T789">I </ts>
               <ts e="T791" id="Seg_4757" n="e" s="T790">tăn </ts>
               <ts e="T792" id="Seg_4759" n="e" s="T791">amorləl. </ts>
            </ts>
            <ts e="T795" id="Seg_4760" n="sc" s="T793">
               <ts e="T794" id="Seg_4762" n="e" s="T793">Jakše </ts>
               <ts e="T795" id="Seg_4764" n="e" s="T794">dĭ. </ts>
            </ts>
            <ts e="T797" id="Seg_4765" n="sc" s="T796">
               <ts e="T797" id="Seg_4767" n="e" s="T796">Вот. </ts>
            </ts>
            <ts e="T799" id="Seg_4768" n="sc" s="T798">
               <ts e="T799" id="Seg_4770" n="e" s="T798">((BRK)). </ts>
            </ts>
            <ts e="T805" id="Seg_4771" n="sc" s="T800">
               <ts e="T801" id="Seg_4773" n="e" s="T800">Măn </ts>
               <ts e="T802" id="Seg_4775" n="e" s="T801">dĭʔnə </ts>
               <ts e="T803" id="Seg_4777" n="e" s="T802">mĭləm </ts>
               <ts e="T804" id="Seg_4779" n="e" s="T803">von </ts>
               <ts e="T805" id="Seg_4781" n="e" s="T804">gĭdaʔ. </ts>
            </ts>
            <ts e="T812" id="Seg_4782" n="sc" s="T806">
               <ts e="T807" id="Seg_4784" n="e" s="T806">A </ts>
               <ts e="T808" id="Seg_4786" n="e" s="T807">dĭ </ts>
               <ts e="T809" id="Seg_4788" n="e" s="T808">măna </ts>
               <ts e="T810" id="Seg_4790" n="e" s="T809">idʼie, </ts>
               <ts e="T811" id="Seg_4792" n="e" s="T810">idʼie </ts>
               <ts e="T812" id="Seg_4794" n="e" s="T811">mĭliet. </ts>
            </ts>
            <ts e="T814" id="Seg_4795" n="sc" s="T813">
               <ts e="T814" id="Seg_4797" n="e" s="T813">((BRK)). </ts>
            </ts>
            <ts e="T818" id="Seg_4798" n="sc" s="T815">
               <ts e="T816" id="Seg_4800" n="e" s="T815">Nu, </ts>
               <ts e="T817" id="Seg_4802" n="e" s="T816">kanžəbəj </ts>
               <ts e="T818" id="Seg_4804" n="e" s="T817">mănziʔ. </ts>
            </ts>
            <ts e="T820" id="Seg_4805" n="sc" s="T819">
               <ts e="T820" id="Seg_4807" n="e" s="T819">Gibər? </ts>
            </ts>
            <ts e="T823" id="Seg_4808" n="sc" s="T821">
               <ts e="T822" id="Seg_4810" n="e" s="T821">Da </ts>
               <ts e="T823" id="Seg_4812" n="e" s="T822">dĭbər. </ts>
            </ts>
            <ts e="T826" id="Seg_4813" n="sc" s="T824">
               <ts e="T825" id="Seg_4815" n="e" s="T824">A </ts>
               <ts e="T826" id="Seg_4817" n="e" s="T825">ial? </ts>
            </ts>
            <ts e="T829" id="Seg_4818" n="sc" s="T827">
               <ts e="T828" id="Seg_4820" n="e" s="T827">Ial </ts>
               <ts e="T829" id="Seg_4822" n="e" s="T828">(pušaj)… </ts>
            </ts>
            <ts e="T835" id="Seg_4823" n="sc" s="T830">
               <ts e="T831" id="Seg_4825" n="e" s="T830">(Ej </ts>
               <ts e="T832" id="Seg_4827" n="e" s="T831">ej-) </ts>
               <ts e="T833" id="Seg_4829" n="e" s="T832">Ej </ts>
               <ts e="T834" id="Seg_4831" n="e" s="T833">iləm </ts>
               <ts e="T835" id="Seg_4833" n="e" s="T834">iam. </ts>
            </ts>
            <ts e="T840" id="Seg_4834" n="sc" s="T836">
               <ts e="T837" id="Seg_4836" n="e" s="T836">A </ts>
               <ts e="T838" id="Seg_4838" n="e" s="T837">(miʔ) </ts>
               <ts e="T839" id="Seg_4840" n="e" s="T838">šidegöʔ </ts>
               <ts e="T840" id="Seg_4842" n="e" s="T839">kanžəbəj. </ts>
            </ts>
            <ts e="T843" id="Seg_4843" n="sc" s="T841">
               <ts e="T842" id="Seg_4845" n="e" s="T841">A </ts>
               <ts e="T843" id="Seg_4847" n="e" s="T842">gibər? </ts>
            </ts>
            <ts e="T847" id="Seg_4848" n="sc" s="T844">
               <ts e="T845" id="Seg_4850" n="e" s="T844">Da </ts>
               <ts e="T846" id="Seg_4852" n="e" s="T845">von, </ts>
               <ts e="T847" id="Seg_4854" n="e" s="T846">dĭbər. </ts>
            </ts>
            <ts e="T850" id="Seg_4855" n="sc" s="T848">
               <ts e="T849" id="Seg_4857" n="e" s="T848">Gijen </ts>
               <ts e="T850" id="Seg_4859" n="e" s="T849">il. </ts>
            </ts>
            <ts e="T855" id="Seg_4860" n="sc" s="T851">
               <ts e="T852" id="Seg_4862" n="e" s="T851">A </ts>
               <ts e="T853" id="Seg_4864" n="e" s="T852">ĭmbi </ts>
               <ts e="T854" id="Seg_4866" n="e" s="T853">dĭ </ts>
               <ts e="T855" id="Seg_4868" n="e" s="T854">dĭgən? </ts>
            </ts>
            <ts e="T859" id="Seg_4869" n="sc" s="T856">
               <ts e="T857" id="Seg_4871" n="e" s="T856">(Ĭmbidə=) </ts>
               <ts e="T858" id="Seg_4873" n="e" s="T857">Ĭmbi-nʼibudʼ </ts>
               <ts e="T859" id="Seg_4875" n="e" s="T858">nörbəlaʔi. </ts>
            </ts>
            <ts e="T863" id="Seg_4876" n="sc" s="T860">
               <ts e="T861" id="Seg_4878" n="e" s="T860">(A </ts>
               <ts e="T862" id="Seg_4880" n="e" s="T861">m- </ts>
               <ts e="T863" id="Seg_4882" n="e" s="T862">m-). </ts>
            </ts>
            <ts e="T866" id="Seg_4883" n="sc" s="T864">
               <ts e="T865" id="Seg_4885" n="e" s="T864">Nörbəlaʔi </ts>
               <ts e="T866" id="Seg_4887" n="e" s="T865">ĭmbi-nʼibudʼ. </ts>
            </ts>
            <ts e="T873" id="Seg_4888" n="sc" s="T867">
               <ts e="T868" id="Seg_4890" n="e" s="T867">A </ts>
               <ts e="T869" id="Seg_4892" n="e" s="T868">(miʔ=) </ts>
               <ts e="T870" id="Seg_4894" n="e" s="T869">măn </ts>
               <ts e="T871" id="Seg_4896" n="e" s="T870">ĭmbidə </ts>
               <ts e="T872" id="Seg_4898" n="e" s="T871">ej </ts>
               <ts e="T873" id="Seg_4900" n="e" s="T872">nörbəlam. </ts>
            </ts>
            <ts e="T878" id="Seg_4901" n="sc" s="T874">
               <ts e="T875" id="Seg_4903" n="e" s="T874">A </ts>
               <ts e="T876" id="Seg_4905" n="e" s="T875">moʔ </ts>
               <ts e="T877" id="Seg_4907" n="e" s="T876">ej </ts>
               <ts e="T878" id="Seg_4909" n="e" s="T877">nörbəlal? </ts>
            </ts>
            <ts e="T880" id="Seg_4910" n="sc" s="T879">
               <ts e="T880" id="Seg_4912" n="e" s="T879">Dʼok. </ts>
            </ts>
            <ts e="T887" id="Seg_4913" n="sc" s="T881">
               <ts e="T882" id="Seg_4915" n="e" s="T881">eželʼi </ts>
               <ts e="T883" id="Seg_4917" n="e" s="T882">nörbəzittə, </ts>
               <ts e="T884" id="Seg_4919" n="e" s="T883">(mĭ-) </ts>
               <ts e="T885" id="Seg_4921" n="e" s="T884">bile </ts>
               <ts e="T886" id="Seg_4923" n="e" s="T885">il </ts>
               <ts e="T887" id="Seg_4925" n="e" s="T886">molam. </ts>
            </ts>
            <ts e="T890" id="Seg_4926" n="sc" s="T888">
               <ts e="T889" id="Seg_4928" n="e" s="T888">Mo </ts>
               <ts e="T890" id="Seg_4930" n="e" s="T889">dereʔ? </ts>
            </ts>
            <ts e="T894" id="Seg_4931" n="sc" s="T891">
               <ts e="T892" id="Seg_4933" n="e" s="T891">Dʼok, </ts>
               <ts e="T893" id="Seg_4935" n="e" s="T892">jakše </ts>
               <ts e="T894" id="Seg_4937" n="e" s="T893">il. </ts>
            </ts>
            <ts e="T898" id="Seg_4938" n="sc" s="T895">
               <ts e="T896" id="Seg_4940" n="e" s="T895">Dʼok, </ts>
               <ts e="T897" id="Seg_4942" n="e" s="T896">bile </ts>
               <ts e="T898" id="Seg_4944" n="e" s="T897">il. </ts>
            </ts>
            <ts e="T904" id="Seg_4945" n="sc" s="T899">
               <ts e="T900" id="Seg_4947" n="e" s="T899">No, </ts>
               <ts e="T901" id="Seg_4949" n="e" s="T900">măn </ts>
               <ts e="T902" id="Seg_4951" n="e" s="T901">ari </ts>
               <ts e="T903" id="Seg_4953" n="e" s="T902">dʼürləm </ts>
               <ts e="T904" id="Seg_4955" n="e" s="T903">tănan. </ts>
            </ts>
            <ts e="T906" id="Seg_4956" n="sc" s="T905">
               <ts e="T906" id="Seg_4958" n="e" s="T905">Вот. </ts>
            </ts>
            <ts e="T912" id="Seg_4959" n="sc" s="T907">
               <ts e="T908" id="Seg_4961" n="e" s="T907">Măn </ts>
               <ts e="T909" id="Seg_4963" n="e" s="T908">abam, </ts>
               <ts e="T910" id="Seg_4965" n="e" s="T909">tăn </ts>
               <ts e="T911" id="Seg_4967" n="e" s="T910">abal </ts>
               <ts e="T912" id="Seg_4969" n="e" s="T911">dʼabrolaʔi. </ts>
            </ts>
            <ts e="T916" id="Seg_4970" n="sc" s="T913">
               <ts e="T914" id="Seg_4972" n="e" s="T913">Mo </ts>
               <ts e="T915" id="Seg_4974" n="e" s="T914">dĭ </ts>
               <ts e="T916" id="Seg_4976" n="e" s="T915">dʼabrolaʔi? </ts>
            </ts>
            <ts e="T928" id="Seg_4977" n="sc" s="T917">
               <ts e="T918" id="Seg_4979" n="e" s="T917">Dak </ts>
               <ts e="T919" id="Seg_4981" n="e" s="T918">dĭ </ts>
               <ts e="T920" id="Seg_4983" n="e" s="T919">măllat, </ts>
               <ts e="T921" id="Seg_4985" n="e" s="T920">tăn </ts>
               <ts e="T922" id="Seg_4987" n="e" s="T921">abal </ts>
               <ts e="T924" id="Seg_4989" n="e" s="T922">măllat: </ts>
               <ts e="T925" id="Seg_4991" n="e" s="T924">"Măn </ts>
               <ts e="T926" id="Seg_4993" n="e" s="T925">nʼim </ts>
               <ts e="T927" id="Seg_4995" n="e" s="T926">jakše, </ts>
               <ts e="T928" id="Seg_4997" n="e" s="T927">kuvas. </ts>
            </ts>
            <ts e="T934" id="Seg_4998" n="sc" s="T929">
               <ts e="T930" id="Seg_5000" n="e" s="T929">A </ts>
               <ts e="T931" id="Seg_5002" n="e" s="T930">tăn </ts>
               <ts e="T932" id="Seg_5004" n="e" s="T931">koʔbdo </ts>
               <ts e="T933" id="Seg_5006" n="e" s="T932">ej </ts>
               <ts e="T934" id="Seg_5008" n="e" s="T933">jakše". </ts>
            </ts>
            <ts e="T944" id="Seg_5009" n="sc" s="T935">
               <ts e="T936" id="Seg_5011" n="e" s="T935">A </ts>
               <ts e="T937" id="Seg_5013" n="e" s="T936">măn </ts>
               <ts e="T938" id="Seg_5015" n="e" s="T937">abam </ts>
               <ts e="T940" id="Seg_5017" n="e" s="T938">măllat: </ts>
               <ts e="T941" id="Seg_5019" n="e" s="T940">"Eh, </ts>
               <ts e="T942" id="Seg_5021" n="e" s="T941">măn </ts>
               <ts e="T943" id="Seg_5023" n="e" s="T942">koʔbdom </ts>
               <ts e="T944" id="Seg_5025" n="e" s="T943">jakše. </ts>
            </ts>
            <ts e="T947" id="Seg_5026" n="sc" s="T945">
               <ts e="T946" id="Seg_5028" n="e" s="T945">I </ts>
               <ts e="T947" id="Seg_5030" n="e" s="T946">kuvas. </ts>
            </ts>
            <ts e="T954" id="Seg_5031" n="sc" s="T948">
               <ts e="T949" id="Seg_5033" n="e" s="T948">I </ts>
               <ts e="T950" id="Seg_5035" n="e" s="T949">bar </ts>
               <ts e="T951" id="Seg_5037" n="e" s="T950">ĭmbi, </ts>
               <ts e="T952" id="Seg_5039" n="e" s="T951">bar </ts>
               <ts e="T953" id="Seg_5041" n="e" s="T952">ĭmbi </ts>
               <ts e="T954" id="Seg_5043" n="e" s="T953">măllat. </ts>
            </ts>
            <ts e="T960" id="Seg_5044" n="sc" s="T955">
               <ts e="T956" id="Seg_5046" n="e" s="T955">I </ts>
               <ts e="T957" id="Seg_5048" n="e" s="T956">tăn </ts>
               <ts e="T958" id="Seg_5050" n="e" s="T957">nʼil </ts>
               <ts e="T959" id="Seg_5052" n="e" s="T958">ej </ts>
               <ts e="T960" id="Seg_5054" n="e" s="T959">jakše. </ts>
            </ts>
            <ts e="T965" id="Seg_5055" n="sc" s="T961">
               <ts e="T962" id="Seg_5057" n="e" s="T961">Ĭmbidə </ts>
               <ts e="T963" id="Seg_5059" n="e" s="T962">ej </ts>
               <ts e="T964" id="Seg_5061" n="e" s="T963">nörbəlat </ts>
               <ts e="T965" id="Seg_5063" n="e" s="T964">măna. </ts>
            </ts>
            <ts e="T972" id="Seg_5064" n="sc" s="T966">
               <ts e="T967" id="Seg_5066" n="e" s="T966">A </ts>
               <ts e="T968" id="Seg_5068" n="e" s="T967">măn </ts>
               <ts e="T969" id="Seg_5070" n="e" s="T968">koʔbdom </ts>
               <ts e="T970" id="Seg_5072" n="e" s="T969">bar </ts>
               <ts e="T971" id="Seg_5074" n="e" s="T970">ĭmbi, </ts>
               <ts e="T972" id="Seg_5076" n="e" s="T971">bar. </ts>
            </ts>
            <ts e="T976" id="Seg_5077" n="sc" s="T973">
               <ts e="T974" id="Seg_5079" n="e" s="T973">Tănan </ts>
               <ts e="T975" id="Seg_5081" n="e" s="T974">üge </ts>
               <ts e="T976" id="Seg_5083" n="e" s="T975">nörbəlapliet. </ts>
            </ts>
            <ts e="T978" id="Seg_5084" n="sc" s="T977">
               <ts e="T978" id="Seg_5086" n="e" s="T977">Vot. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_5087" s="T1">SAE_196X_SU0232.001 (001)</ta>
            <ta e="T8" id="Seg_5088" s="T6">SAE_196X_SU0232.002 (002)</ta>
            <ta e="T17" id="Seg_5089" s="T9">SAE_196X_SU0232.003 (003)</ta>
            <ta e="T23" id="Seg_5090" s="T18">SAE_196X_SU0232.004 (004)</ta>
            <ta e="T25" id="Seg_5091" s="T24">SAE_196X_SU0232.005 (005)</ta>
            <ta e="T30" id="Seg_5092" s="T26">SAE_196X_SU0232.006 (006)</ta>
            <ta e="T33" id="Seg_5093" s="T31">SAE_196X_SU0232.007 (007)</ta>
            <ta e="T37" id="Seg_5094" s="T34">SAE_196X_SU0232.008 (008)</ta>
            <ta e="T44" id="Seg_5095" s="T38">SAE_196X_SU0232.009 (009)</ta>
            <ta e="T47" id="Seg_5096" s="T45">SAE_196X_SU0232.010 (010)</ta>
            <ta e="T50" id="Seg_5097" s="T48">SAE_196X_SU0232.011 (011)</ta>
            <ta e="T55" id="Seg_5098" s="T51">SAE_196X_SU0232.012 (012)</ta>
            <ta e="T59" id="Seg_5099" s="T56">SAE_196X_SU0232.013 (013)</ta>
            <ta e="T63" id="Seg_5100" s="T60">SAE_196X_SU0232.014 (014)</ta>
            <ta e="T72" id="Seg_5101" s="T64">SAE_196X_SU0232.015 (015)</ta>
            <ta e="T76" id="Seg_5102" s="T73">SAE_196X_SU0232.016 (016)</ta>
            <ta e="T79" id="Seg_5103" s="T77">SAE_196X_SU0232.017 (017)</ta>
            <ta e="T83" id="Seg_5104" s="T80">SAE_196X_SU0232.018 (018)</ta>
            <ta e="T86" id="Seg_5105" s="T84">SAE_196X_SU0232.019 (019)</ta>
            <ta e="T90" id="Seg_5106" s="T86">SAE_196X_SU0232.020 (020)</ta>
            <ta e="T96" id="Seg_5107" s="T91">SAE_196X_SU0232.021 (021)</ta>
            <ta e="T98" id="Seg_5108" s="T97">SAE_196X_SU0232.022 (022)</ta>
            <ta e="T109" id="Seg_5109" s="T99">SAE_196X_SU0232.023 (023)</ta>
            <ta e="T113" id="Seg_5110" s="T110">SAE_196X_SU0232.024 (024)</ta>
            <ta e="T119" id="Seg_5111" s="T114">SAE_196X_SU0232.025 (025)</ta>
            <ta e="T121" id="Seg_5112" s="T120">SAE_196X_SU0232.026 (026)</ta>
            <ta e="T126" id="Seg_5113" s="T122">SAE_196X_SU0232.027 (027)</ta>
            <ta e="T133" id="Seg_5114" s="T127">SAE_196X_SU0232.028 (028)</ta>
            <ta e="T140" id="Seg_5115" s="T134">SAE_196X_SU0232.029 (029)</ta>
            <ta e="T145" id="Seg_5116" s="T141">SAE_196X_SU0232.030 (030)</ta>
            <ta e="T148" id="Seg_5117" s="T146">SAE_196X_SU0232.031 (031)</ta>
            <ta e="T150" id="Seg_5118" s="T149">SAE_196X_SU0232.032 (032)</ta>
            <ta e="T153" id="Seg_5119" s="T151">SAE_196X_SU0232.033 (033)</ta>
            <ta e="T157" id="Seg_5120" s="T154">SAE_196X_SU0232.034 (034)</ta>
            <ta e="T163" id="Seg_5121" s="T158">SAE_196X_SU0232.035 (035)</ta>
            <ta e="T165" id="Seg_5122" s="T164">SAE_196X_SU0232.036 (036)</ta>
            <ta e="T169" id="Seg_5123" s="T166">SAE_196X_SU0232.037 (037)</ta>
            <ta e="T173" id="Seg_5124" s="T170">SAE_196X_SU0232.038 (038)</ta>
            <ta e="T175" id="Seg_5125" s="T174">SAE_196X_SU0232.039 (039)</ta>
            <ta e="T177" id="Seg_5126" s="T176">SAE_196X_SU0232.040 (040)</ta>
            <ta e="T181" id="Seg_5127" s="T178">SAE_196X_SU0232.041 (041)</ta>
            <ta e="T185" id="Seg_5128" s="T182">SAE_196X_SU0232.042 (042)</ta>
            <ta e="T187" id="Seg_5129" s="T186">SAE_196X_SU0232.043 (043)</ta>
            <ta e="T192" id="Seg_5130" s="T188">SAE_196X_SU0232.044 (044)</ta>
            <ta e="T194" id="Seg_5131" s="T193">SAE_196X_SU0232.045 (045)</ta>
            <ta e="T197" id="Seg_5132" s="T195">SAE_196X_SU0232.046 (046)</ta>
            <ta e="T202" id="Seg_5133" s="T198">SAE_196X_SU0232.047 (047)</ta>
            <ta e="T208" id="Seg_5134" s="T203">SAE_196X_SU0232.048 (048)</ta>
            <ta e="T213" id="Seg_5135" s="T209">SAE_196X_SU0232.049 (049)</ta>
            <ta e="T216" id="Seg_5136" s="T214">SAE_196X_SU0232.050 (050)</ta>
            <ta e="T220" id="Seg_5137" s="T217">SAE_196X_SU0232.051 (051)</ta>
            <ta e="T224" id="Seg_5138" s="T221">SAE_196X_SU0232.052 (052)</ta>
            <ta e="T226" id="Seg_5139" s="T225">SAE_196X_SU0232.053 (053)</ta>
            <ta e="T231" id="Seg_5140" s="T227">SAE_196X_SU0232.054 (054)</ta>
            <ta e="T236" id="Seg_5141" s="T232">SAE_196X_SU0232.055 (055)</ta>
            <ta e="T241" id="Seg_5142" s="T237">SAE_196X_SU0232.056 (056)</ta>
            <ta e="T245" id="Seg_5143" s="T242">SAE_196X_SU0232.057 (057)</ta>
            <ta e="T252" id="Seg_5144" s="T246">SAE_196X_SU0232.058 (058)</ta>
            <ta e="T257" id="Seg_5145" s="T253">SAE_196X_SU0232.059 (059)</ta>
            <ta e="T261" id="Seg_5146" s="T258">SAE_196X_SU0232.060 (060)</ta>
            <ta e="T269" id="Seg_5147" s="T262">SAE_196X_SU0232.061 (061)</ta>
            <ta e="T274" id="Seg_5148" s="T270">SAE_196X_SU0232.062 (062)</ta>
            <ta e="T280" id="Seg_5149" s="T275">SAE_196X_SU0232.063 (063)</ta>
            <ta e="T286" id="Seg_5150" s="T281">SAE_196X_SU0232.064 (064)</ta>
            <ta e="T291" id="Seg_5151" s="T287">SAE_196X_SU0232.065 (065)</ta>
            <ta e="T297" id="Seg_5152" s="T292">SAE_196X_SU0232.066 (066)</ta>
            <ta e="T299" id="Seg_5153" s="T298">SAE_196X_SU0232.067 (067)</ta>
            <ta e="T304" id="Seg_5154" s="T300">SAE_196X_SU0232.068 (068)</ta>
            <ta e="T309" id="Seg_5155" s="T305">SAE_196X_SU0232.069 (069)</ta>
            <ta e="T314" id="Seg_5156" s="T310">SAE_196X_SU0232.070 (070)</ta>
            <ta e="T320" id="Seg_5157" s="T315">SAE_196X_SU0232.071 (071)</ta>
            <ta e="T328" id="Seg_5158" s="T321">SAE_196X_SU0232.072 (072)</ta>
            <ta e="T332" id="Seg_5159" s="T329">SAE_196X_SU0232.073 (073)</ta>
            <ta e="T338" id="Seg_5160" s="T333">SAE_196X_SU0232.074 (074)</ta>
            <ta e="T343" id="Seg_5161" s="T339">SAE_196X_SU0232.075 (075)</ta>
            <ta e="T350" id="Seg_5162" s="T344">SAE_196X_SU0232.076 (076)</ta>
            <ta e="T354" id="Seg_5163" s="T351">SAE_196X_SU0232.077 (077)</ta>
            <ta e="T361" id="Seg_5164" s="T355">SAE_196X_SU0232.078 (078)</ta>
            <ta e="T364" id="Seg_5165" s="T362">SAE_196X_SU0232.079 (079)</ta>
            <ta e="T366" id="Seg_5166" s="T365">SAE_196X_SU0232.080 (080)</ta>
            <ta e="T371" id="Seg_5167" s="T367">SAE_196X_SU0232.081 (081)</ta>
            <ta e="T374" id="Seg_5168" s="T372">SAE_196X_SU0232.082 (082)</ta>
            <ta e="T382" id="Seg_5169" s="T375">SAE_196X_SU0232.083 (083)</ta>
            <ta e="T384" id="Seg_5170" s="T383">SAE_196X_SU0232.084 (084)</ta>
            <ta e="T386" id="Seg_5171" s="T385">SAE_196X_SU0232.085 (085)</ta>
            <ta e="T398" id="Seg_5172" s="T387">SAE_196X_SU0232.086 (086)</ta>
            <ta e="T403" id="Seg_5173" s="T399">SAE_196X_SU0232.087 (087)</ta>
            <ta e="T406" id="Seg_5174" s="T404">SAE_196X_SU0232.088 (088)</ta>
            <ta e="T410" id="Seg_5175" s="T407">SAE_196X_SU0232.089 (089)</ta>
            <ta e="T417" id="Seg_5176" s="T411">SAE_196X_SU0232.090 (090)</ta>
            <ta e="T421" id="Seg_5177" s="T418">SAE_196X_SU0232.091 (091)</ta>
            <ta e="T427" id="Seg_5178" s="T422">SAE_196X_SU0232.092 (092)</ta>
            <ta e="T431" id="Seg_5179" s="T428">SAE_196X_SU0232.093 (093)</ta>
            <ta e="T437" id="Seg_5180" s="T432">SAE_196X_SU0232.094 (094)</ta>
            <ta e="T442" id="Seg_5181" s="T438">SAE_196X_SU0232.095 (095)</ta>
            <ta e="T449" id="Seg_5182" s="T443">SAE_196X_SU0232.096 (096)</ta>
            <ta e="T457" id="Seg_5183" s="T450">SAE_196X_SU0232.097 (097)</ta>
            <ta e="T459" id="Seg_5184" s="T458">SAE_196X_SU0232.098 (098)</ta>
            <ta e="T461" id="Seg_5185" s="T460">SAE_196X_SU0232.099 (099)</ta>
            <ta e="T465" id="Seg_5186" s="T462">SAE_196X_SU0232.100 (100)</ta>
            <ta e="T467" id="Seg_5187" s="T466">SAE_196X_SU0232.101 (101)</ta>
            <ta e="T471" id="Seg_5188" s="T468">SAE_196X_SU0232.102 (102)</ta>
            <ta e="T475" id="Seg_5189" s="T472">SAE_196X_SU0232.103 (103)</ta>
            <ta e="T477" id="Seg_5190" s="T476">SAE_196X_SU0232.104 (104)</ta>
            <ta e="T481" id="Seg_5191" s="T478">SAE_196X_SU0232.105 (105)</ta>
            <ta e="T486" id="Seg_5192" s="T482">SAE_196X_SU0232.106 (106)</ta>
            <ta e="T494" id="Seg_5193" s="T487">SAE_196X_SU0232.107 (107)</ta>
            <ta e="T496" id="Seg_5194" s="T495">SAE_196X_SU0232.108 (108)</ta>
            <ta e="T498" id="Seg_5195" s="T497">SAE_196X_SU0232.109 (109)</ta>
            <ta e="T505" id="Seg_5196" s="T499">SAE_196X_SU0232.110 (110)</ta>
            <ta e="T513" id="Seg_5197" s="T506">SAE_196X_SU0232.111 (111)</ta>
            <ta e="T517" id="Seg_5198" s="T514">SAE_196X_SU0232.112 (112)</ta>
            <ta e="T522" id="Seg_5199" s="T518">SAE_196X_SU0232.113 (113)</ta>
            <ta e="T526" id="Seg_5200" s="T523">SAE_196X_SU0232.114 (114)</ta>
            <ta e="T529" id="Seg_5201" s="T527">SAE_196X_SU0232.115 (115)</ta>
            <ta e="T531" id="Seg_5202" s="T530">SAE_196X_SU0232.116 (116)</ta>
            <ta e="T533" id="Seg_5203" s="T532">SAE_196X_SU0232.117 (117)</ta>
            <ta e="T542" id="Seg_5204" s="T534">SAE_196X_SU0232.118 (118)</ta>
            <ta e="T550" id="Seg_5205" s="T543">SAE_196X_SU0232.119 (119)</ta>
            <ta e="T554" id="Seg_5206" s="T551">SAE_196X_SU0232.120 (120)</ta>
            <ta e="T556" id="Seg_5207" s="T555">SAE_196X_SU0232.121 (121)</ta>
            <ta e="T560" id="Seg_5208" s="T557">SAE_196X_SU0232.122 (122)</ta>
            <ta e="T562" id="Seg_5209" s="T561">SAE_196X_SU0232.123 (123)</ta>
            <ta e="T564" id="Seg_5210" s="T563">SAE_196X_SU0232.124 (124)</ta>
            <ta e="T568" id="Seg_5211" s="T565">SAE_196X_SU0232.125 (125)</ta>
            <ta e="T571" id="Seg_5212" s="T569">SAE_196X_SU0232.126 (126)</ta>
            <ta e="T576" id="Seg_5213" s="T572">SAE_196X_SU0232.127 (127)</ta>
            <ta e="T580" id="Seg_5214" s="T577">SAE_196X_SU0232.128 (128)</ta>
            <ta e="T585" id="Seg_5215" s="T581">SAE_196X_SU0232.129 (129)</ta>
            <ta e="T588" id="Seg_5216" s="T586">SAE_196X_SU0232.130 (130)</ta>
            <ta e="T591" id="Seg_5217" s="T589">SAE_196X_SU0232.131 (131)</ta>
            <ta e="T595" id="Seg_5218" s="T592">SAE_196X_SU0232.132 (132)</ta>
            <ta e="T598" id="Seg_5219" s="T596">SAE_196X_SU0232.133 (133)</ta>
            <ta e="T600" id="Seg_5220" s="T599">SAE_196X_SU0232.134 (134)</ta>
            <ta e="T602" id="Seg_5221" s="T601">SAE_196X_SU0232.135 (135)</ta>
            <ta e="T606" id="Seg_5222" s="T603">SAE_196X_SU0232.136 (136)</ta>
            <ta e="T614" id="Seg_5223" s="T607">SAE_196X_SU0232.137 (137)</ta>
            <ta e="T619" id="Seg_5224" s="T615">SAE_196X_SU0232.138 (138)</ta>
            <ta e="T626" id="Seg_5225" s="T620">SAE_196X_SU0232.139 (139)</ta>
            <ta e="T634" id="Seg_5226" s="T627">SAE_196X_SU0232.140 (140)</ta>
            <ta e="T637" id="Seg_5227" s="T635">SAE_196X_SU0232.141 (141)</ta>
            <ta e="T640" id="Seg_5228" s="T638">SAE_196X_SU0232.142 (142)</ta>
            <ta e="T644" id="Seg_5229" s="T641">SAE_196X_SU0232.143 (143)</ta>
            <ta e="T646" id="Seg_5230" s="T645">SAE_196X_SU0232.144 (144)</ta>
            <ta e="T650" id="Seg_5231" s="T647">SAE_196X_SU0232.145 (145)</ta>
            <ta e="T656" id="Seg_5232" s="T651">SAE_196X_SU0232.146 (146)</ta>
            <ta e="T666" id="Seg_5233" s="T657">SAE_196X_SU0232.147 (147)</ta>
            <ta e="T675" id="Seg_5234" s="T667">SAE_196X_SU0232.148 (148)</ta>
            <ta e="T679" id="Seg_5235" s="T676">SAE_196X_SU0232.149 (149)</ta>
            <ta e="T685" id="Seg_5236" s="T680">SAE_196X_SU0232.150 (150)</ta>
            <ta e="T691" id="Seg_5237" s="T686">SAE_196X_SU0232.151 (151)</ta>
            <ta e="T694" id="Seg_5238" s="T692">SAE_196X_SU0232.152 (152)</ta>
            <ta e="T696" id="Seg_5239" s="T695">SAE_196X_SU0232.153 (153)</ta>
            <ta e="T698" id="Seg_5240" s="T697">SAE_196X_SU0232.154 (154)</ta>
            <ta e="T702" id="Seg_5241" s="T699">SAE_196X_SU0232.155 (155)</ta>
            <ta e="T709" id="Seg_5242" s="T703">SAE_196X_SU0232.156 (156)</ta>
            <ta e="T713" id="Seg_5243" s="T710">SAE_196X_SU0232.157 (157)</ta>
            <ta e="T716" id="Seg_5244" s="T714">SAE_196X_SU0232.158 (158)</ta>
            <ta e="T720" id="Seg_5245" s="T717">SAE_196X_SU0232.159 (159)</ta>
            <ta e="T724" id="Seg_5246" s="T721">SAE_196X_SU0232.160 (160)</ta>
            <ta e="T730" id="Seg_5247" s="T725">SAE_196X_SU0232.161 (161)</ta>
            <ta e="T734" id="Seg_5248" s="T731">SAE_196X_SU0232.162 (162)</ta>
            <ta e="T737" id="Seg_5249" s="T735">SAE_196X_SU0232.163 (163)</ta>
            <ta e="T739" id="Seg_5250" s="T738">SAE_196X_SU0232.164 (164)</ta>
            <ta e="T745" id="Seg_5251" s="T740">SAE_196X_SU0232.165 (165)</ta>
            <ta e="T750" id="Seg_5252" s="T746">SAE_196X_SU0232.166 (166)</ta>
            <ta e="T753" id="Seg_5253" s="T751">SAE_196X_SU0232.167 (167)</ta>
            <ta e="T758" id="Seg_5254" s="T754">SAE_196X_SU0232.168 (168)</ta>
            <ta e="T763" id="Seg_5255" s="T759">SAE_196X_SU0232.169 (169)</ta>
            <ta e="T765" id="Seg_5256" s="T764">SAE_196X_SU0232.170 (170)</ta>
            <ta e="T769" id="Seg_5257" s="T766">SAE_196X_SU0232.171 (171)</ta>
            <ta e="T772" id="Seg_5258" s="T770">SAE_196X_SU0232.172 (172)</ta>
            <ta e="T777" id="Seg_5259" s="T773">SAE_196X_SU0232.173 (173)</ta>
            <ta e="T781" id="Seg_5260" s="T778">SAE_196X_SU0232.174 (174)</ta>
            <ta e="T788" id="Seg_5261" s="T782">SAE_196X_SU0232.175 (175)</ta>
            <ta e="T792" id="Seg_5262" s="T789">SAE_196X_SU0232.176 (176)</ta>
            <ta e="T795" id="Seg_5263" s="T793">SAE_196X_SU0232.177 (177)</ta>
            <ta e="T797" id="Seg_5264" s="T796">SAE_196X_SU0232.178 (178)</ta>
            <ta e="T799" id="Seg_5265" s="T798">SAE_196X_SU0232.179 (179)</ta>
            <ta e="T805" id="Seg_5266" s="T800">SAE_196X_SU0232.180 (180)</ta>
            <ta e="T812" id="Seg_5267" s="T806">SAE_196X_SU0232.181 (181)</ta>
            <ta e="T814" id="Seg_5268" s="T813">SAE_196X_SU0232.182 (182)</ta>
            <ta e="T818" id="Seg_5269" s="T815">SAE_196X_SU0232.183 (183)</ta>
            <ta e="T820" id="Seg_5270" s="T819">SAE_196X_SU0232.184 (184)</ta>
            <ta e="T823" id="Seg_5271" s="T821">SAE_196X_SU0232.185 (185)</ta>
            <ta e="T826" id="Seg_5272" s="T824">SAE_196X_SU0232.186 (186)</ta>
            <ta e="T829" id="Seg_5273" s="T827">SAE_196X_SU0232.187 (187)</ta>
            <ta e="T835" id="Seg_5274" s="T830">SAE_196X_SU0232.188 (188)</ta>
            <ta e="T840" id="Seg_5275" s="T836">SAE_196X_SU0232.189 (189)</ta>
            <ta e="T843" id="Seg_5276" s="T841">SAE_196X_SU0232.190 (190)</ta>
            <ta e="T847" id="Seg_5277" s="T844">SAE_196X_SU0232.191 (191)</ta>
            <ta e="T850" id="Seg_5278" s="T848">SAE_196X_SU0232.192 (192)</ta>
            <ta e="T855" id="Seg_5279" s="T851">SAE_196X_SU0232.193 (193)</ta>
            <ta e="T859" id="Seg_5280" s="T856">SAE_196X_SU0232.194 (194)</ta>
            <ta e="T863" id="Seg_5281" s="T860">SAE_196X_SU0232.195 (195)</ta>
            <ta e="T866" id="Seg_5282" s="T864">SAE_196X_SU0232.196 (196)</ta>
            <ta e="T873" id="Seg_5283" s="T867">SAE_196X_SU0232.197 (197)</ta>
            <ta e="T878" id="Seg_5284" s="T874">SAE_196X_SU0232.198 (198)</ta>
            <ta e="T880" id="Seg_5285" s="T879">SAE_196X_SU0232.199 (199)</ta>
            <ta e="T887" id="Seg_5286" s="T881">SAE_196X_SU0232.200 (200)</ta>
            <ta e="T890" id="Seg_5287" s="T888">SAE_196X_SU0232.201 (201)</ta>
            <ta e="T894" id="Seg_5288" s="T891">SAE_196X_SU0232.202 (202)</ta>
            <ta e="T898" id="Seg_5289" s="T895">SAE_196X_SU0232.203 (203)</ta>
            <ta e="T904" id="Seg_5290" s="T899">SAE_196X_SU0232.204 (204)</ta>
            <ta e="T906" id="Seg_5291" s="T905">SAE_196X_SU0232.205 (205)</ta>
            <ta e="T912" id="Seg_5292" s="T907">SAE_196X_SU0232.206 (206)</ta>
            <ta e="T916" id="Seg_5293" s="T913">SAE_196X_SU0232.207 (207)</ta>
            <ta e="T928" id="Seg_5294" s="T917">SAE_196X_SU0232.208 (208) </ta>
            <ta e="T934" id="Seg_5295" s="T929">SAE_196X_SU0232.209 (210)</ta>
            <ta e="T944" id="Seg_5296" s="T935">SAE_196X_SU0232.210 (211) </ta>
            <ta e="T947" id="Seg_5297" s="T945">SAE_196X_SU0232.211 (213)</ta>
            <ta e="T954" id="Seg_5298" s="T948">SAE_196X_SU0232.212 (214)</ta>
            <ta e="T960" id="Seg_5299" s="T955">SAE_196X_SU0232.213 (215)</ta>
            <ta e="T965" id="Seg_5300" s="T961">SAE_196X_SU0232.214 (216)</ta>
            <ta e="T972" id="Seg_5301" s="T966">SAE_196X_SU0232.215 (217)</ta>
            <ta e="T976" id="Seg_5302" s="T973">SAE_196X_SU0232.216 (218)</ta>
            <ta e="T978" id="Seg_5303" s="T977">SAE_196X_SU0232.217 (219)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_5304" s="T1">Jakše kuza mobi, külambi. </ta>
            <ta e="T8" id="Seg_5305" s="T6">Jakše külambi. </ta>
            <ta e="T17" id="Seg_5306" s="T9">(A dĭ) bile kuza (oʔb) (ku- ku-) külambit. </ta>
            <ta e="T23" id="Seg_5307" s="T18">A jakše kuza jakše külambi. </ta>
            <ta e="T25" id="Seg_5308" s="T24">(Vot). </ta>
            <ta e="T30" id="Seg_5309" s="T26">Măn abam jakše amnobi. </ta>
            <ta e="T33" id="Seg_5310" s="T31">Dĭttə bile. </ta>
            <ta e="T37" id="Seg_5311" s="T34">Tăŋ bile amnobi. </ta>
            <ta e="T44" id="Seg_5312" s="T38">Dĭttə (это ой=) bar amnobi jakše. </ta>
            <ta e="T47" id="Seg_5313" s="T45">Dĭttə külambi. </ta>
            <ta e="T50" id="Seg_5314" s="T48">(Nu mĭ=). </ta>
            <ta e="T55" id="Seg_5315" s="T51">Măn amnobiam tăŋ bile. </ta>
            <ta e="T59" id="Seg_5316" s="T56">Tăŋ bile amnobiam. </ta>
            <ta e="T63" id="Seg_5317" s="T60">Măn ipekpə naga. </ta>
            <ta e="T72" id="Seg_5318" s="T64">Naga ipek măn, i măn bostə ĭmbi-ĭmbi amorbiam. </ta>
            <ta e="T76" id="Seg_5319" s="T73">Dĭttəgəš jakše amnobiam. </ta>
            <ta e="T79" id="Seg_5320" s="T77">Bar gĭdeʔ. </ta>
            <ta e="T83" id="Seg_5321" s="T80">Dĭttəgəš ari dʼürbiam. </ta>
            <ta e="T86" id="Seg_5322" s="T84">Amnobiam bile-bile-bile-bile. </ta>
            <ta e="T90" id="Seg_5323" s="T86">Koʔbdom (mĭle- mĭle-) mĭləbiam. </ta>
            <ta e="T96" id="Seg_5324" s="T91">(I) koʔbdozi tăŋ bile amnobiam. </ta>
            <ta e="T98" id="Seg_5325" s="T97">Dʼörlam. </ta>
            <ta e="T109" id="Seg_5326" s="T99">I (an-) allam, i (šo-) šolam (amno-) amnozittə, tăŋ bile. </ta>
            <ta e="T113" id="Seg_5327" s="T110">Gibər măna anzittə? </ta>
            <ta e="T119" id="Seg_5328" s="T114">Gibərdə ej alləm, lutʼše külambiam. </ta>
            <ta e="T121" id="Seg_5329" s="T120">Это. </ta>
            <ta e="T126" id="Seg_5330" s="T122">Bar il jakše amnoliaʔit. </ta>
            <ta e="T133" id="Seg_5331" s="T127">A dĭttə ari dʼürgelit dögəʔ, gibərdə. </ta>
            <ta e="T140" id="Seg_5332" s="T134">A dĭttə moʔ döbər že šoləj. </ta>
            <ta e="T145" id="Seg_5333" s="T141">Eh, moʔ dereʔ mollal? </ta>
            <ta e="T148" id="Seg_5334" s="T146">I ĭmbi? </ta>
            <ta e="T150" id="Seg_5335" s="T149">Dereʔ. </ta>
            <ta e="T153" id="Seg_5336" s="T151">Šoləj döbər. </ta>
            <ta e="T157" id="Seg_5337" s="T154">А ((PAUSE)) döbər šoləj. </ta>
            <ta e="T163" id="Seg_5338" s="T158">A tăn ĭmbi, ej alləl? </ta>
            <ta e="T165" id="Seg_5339" s="T164">Dʼok. </ta>
            <ta e="T169" id="Seg_5340" s="T166">Măn ej alləm. </ta>
            <ta e="T173" id="Seg_5341" s="T170">Măn dögən molam. </ta>
            <ta e="T175" id="Seg_5342" s="T174">Vot. </ta>
            <ta e="T177" id="Seg_5343" s="T176">((BRK)). </ta>
            <ta e="T181" id="Seg_5344" s="T178">Ĭmbi-nʼibudʼ mĭnzərzittə nada. </ta>
            <ta e="T185" id="Seg_5345" s="T182">Măn amorzittə molam. </ta>
            <ta e="T187" id="Seg_5346" s="T186">((BRK)). </ta>
            <ta e="T192" id="Seg_5347" s="T188">(Ĭmbĭ- m-) Ĭmbi mĭnzerliel? </ta>
            <ta e="T194" id="Seg_5348" s="T193">Uja. </ta>
            <ta e="T197" id="Seg_5349" s="T195">Mo uja? </ta>
            <ta e="T202" id="Seg_5350" s="T198">Măn bɨ (jejam amzit). </ta>
            <ta e="T208" id="Seg_5351" s="T203">A măn bɨ süt bɨ. </ta>
            <ta e="T213" id="Seg_5352" s="T209">Vot jakše bɨ amorlam. </ta>
            <ta e="T216" id="Seg_5353" s="T214">Mo süt? </ta>
            <ta e="T220" id="Seg_5354" s="T217">Süt dak süt. </ta>
            <ta e="T224" id="Seg_5355" s="T221">A uja jakše. </ta>
            <ta e="T226" id="Seg_5356" s="T225">((BRK)). </ta>
            <ta e="T231" id="Seg_5357" s="T227">Mo măna ej šoləj? </ta>
            <ta e="T236" id="Seg_5358" s="T232">A măn iam bila. </ta>
            <ta e="T241" id="Seg_5359" s="T237">(Ilajaŋ) tănan ej (öʔliet). </ta>
            <ta e="T245" id="Seg_5360" s="T242">Mo dereʔ ej? </ta>
            <ta e="T252" id="Seg_5361" s="T246">Nu, măllat što bila kuzat tăn. </ta>
            <ta e="T257" id="Seg_5362" s="T253">Mo dere bila kuza? </ta>
            <ta e="T261" id="Seg_5363" s="T258">Măn jakše kuza. </ta>
            <ta e="T269" id="Seg_5364" s="T262">Ĭmbi-nʼibudʼ tănan bar bɨ nörbəlam, nörbəlam bɨ. </ta>
            <ta e="T274" id="Seg_5365" s="T270">Teʔi (tăŋ) jakše moləj. </ta>
            <ta e="T280" id="Seg_5366" s="T275">Oh, nu dereʔ măllat măna. </ta>
            <ta e="T286" id="Seg_5367" s="T281">Măn dĭm bar münörlam tăŋ. </ta>
            <ta e="T291" id="Seg_5368" s="T287">Dʼok, münörzittə ne nado. </ta>
            <ta e="T297" id="Seg_5369" s="T292">(A to) dĭ măna münörlil dittəgaš. </ta>
            <ta e="T299" id="Seg_5370" s="T298">((BRK)). </ta>
            <ta e="T304" id="Seg_5371" s="T300">Măn tăŋ ara bĭtliem. </ta>
            <ta e="T309" id="Seg_5372" s="T305">O, mo dereʔ tăn? </ta>
            <ta e="T314" id="Seg_5373" s="T310">Nu, măn bile kuza. </ta>
            <ta e="T320" id="Seg_5374" s="T315">Bar gĭda bar (iləm) ĭmbidə. </ta>
            <ta e="T328" id="Seg_5375" s="T321">A mo (tănan) iləl, dak măn dĭttəgəš… </ta>
            <ta e="T332" id="Seg_5376" s="T329">Măn dittəgəš iləm. </ta>
            <ta e="T338" id="Seg_5377" s="T333">A mo dĭn dere iləl? </ta>
            <ta e="T343" id="Seg_5378" s="T339">Dak vot iləm măn. </ta>
            <ta e="T350" id="Seg_5379" s="T344">Măna dĭttəgəš ej kuvas turanə kunnallit. </ta>
            <ta e="T354" id="Seg_5380" s="T351">Măn dĭgən amnolam. </ta>
            <ta e="T361" id="Seg_5381" s="T355">A dĭttə kak (šoləm) măna il. </ta>
            <ta e="T364" id="Seg_5382" s="T362">Šindĭdə ej… </ta>
            <ta e="T366" id="Seg_5383" s="T365">Ej… </ta>
            <ta e="T371" id="Seg_5384" s="T367">Вот ведь, не выговорить. </ta>
            <ta e="T374" id="Seg_5385" s="T372">Ej iliet. </ta>
            <ta e="T382" id="Seg_5386" s="T375">Bar il (münörlat) (i) ej jakše kuza. </ta>
            <ta e="T384" id="Seg_5387" s="T383">Vot. </ta>
            <ta e="T386" id="Seg_5388" s="T385">((BRK)). </ta>
            <ta e="T398" id="Seg_5389" s="T387">Bile kuza jakše kuzan ibi kujnek, uja, i bar ĭmbi, bar. </ta>
            <ta e="T403" id="Seg_5390" s="T399">Aktʼat (mobi) dĭ kuzan. </ta>
            <ta e="T406" id="Seg_5391" s="T404">Bar ibi. </ta>
            <ta e="T410" id="Seg_5392" s="T407">Ĭmbi tăn ibiel? </ta>
            <ta e="T417" id="Seg_5393" s="T411">Dak măn naga, a tăn ige. </ta>
            <ta e="T421" id="Seg_5394" s="T418">(Vo), măn ibiem. </ta>
            <ta e="T427" id="Seg_5395" s="T422">A măn tănan ej iləm. </ta>
            <ta e="T431" id="Seg_5396" s="T428">Măn tănan iləm. </ta>
            <ta e="T437" id="Seg_5397" s="T432">A moʔ (nĭn) măna iləl? </ta>
            <ta e="T442" id="Seg_5398" s="T438">Dak nĭn na što ibiel? </ta>
            <ta e="T449" id="Seg_5399" s="T443">Măn kunnaʔbə i bar ĭmbim bar. </ta>
            <ta e="T457" id="Seg_5400" s="T450">A măn bile turanə tănan amnolaʔbə ((…)). </ta>
            <ta e="T459" id="Seg_5401" s="T458">Vot. </ta>
            <ta e="T461" id="Seg_5402" s="T460">((BRK)). </ta>
            <ta e="T465" id="Seg_5403" s="T462">Amnoʔ mănzi amorzittə. </ta>
            <ta e="T467" id="Seg_5404" s="T466">Oj. </ta>
            <ta e="T471" id="Seg_5405" s="T468">Чего я это. </ta>
            <ta e="T475" id="Seg_5406" s="T472">Нет, не так. </ta>
            <ta e="T477" id="Seg_5407" s="T476">((BRK)). </ta>
            <ta e="T481" id="Seg_5408" s="T478">Amnoʔ mănzi amorzittə. </ta>
            <ta e="T486" id="Seg_5409" s="T482">Da ĭmbi tăn molat? </ta>
            <ta e="T494" id="Seg_5410" s="T487">Da măn (molat) ipek, uja, i süt. </ta>
            <ta e="T496" id="Seg_5411" s="T495">Sĭrepne. </ta>
            <ta e="T498" id="Seg_5412" s="T497">Amdə. </ta>
            <ta e="T505" id="Seg_5413" s="T499">Dak măn bɨ kajak amorliam bɨ. </ta>
            <ta e="T513" id="Seg_5414" s="T506">Nu i kajak măn mĭlem tănan amorzittə. </ta>
            <ta e="T517" id="Seg_5415" s="T514">Jakše tolʼkă amoraʔ. </ta>
            <ta e="T522" id="Seg_5416" s="T518">(Dö) dak măn (amorlam). </ta>
            <ta e="T526" id="Seg_5417" s="T523">(Tăŋ=) Tăŋ amorlam. </ta>
            <ta e="T529" id="Seg_5418" s="T527">Jakše amorliam. </ta>
            <ta e="T531" id="Seg_5419" s="T530">Vo. </ta>
            <ta e="T533" id="Seg_5420" s="T532">((BRK)). </ta>
            <ta e="T542" id="Seg_5421" s="T534">(Mo) tăn măn eʔbdem bar bos (udanə) ibiel? </ta>
            <ta e="T550" id="Seg_5422" s="T543">Vedʼ tăn udal urgo, a măn idiʼije. </ta>
            <ta e="T554" id="Seg_5423" s="T551">Mo dereʔ molal? </ta>
            <ta e="T556" id="Seg_5424" s="T555">Bile. </ta>
            <ta e="T560" id="Seg_5425" s="T557">Ej jakše dereʔ. </ta>
            <ta e="T562" id="Seg_5426" s="T561">Vot. </ta>
            <ta e="T564" id="Seg_5427" s="T563">((BRK)). </ta>
            <ta e="T568" id="Seg_5428" s="T565">Măn tăŋ (ĭzemneʔbiem). </ta>
            <ta e="T571" id="Seg_5429" s="T569">Udazambə ĭzəmniet. </ta>
            <ta e="T576" id="Seg_5430" s="T572">I ujubə bar ĭzemneʔbəliet. </ta>
            <ta e="T580" id="Seg_5431" s="T577">Ădnakă măn (külambil). </ta>
            <ta e="T585" id="Seg_5432" s="T581">Dʼok, nʼe nada külamzittə. </ta>
            <ta e="T588" id="Seg_5433" s="T586">Nada amnozittə. </ta>
            <ta e="T591" id="Seg_5434" s="T589">Amnozittə jakše. </ta>
            <ta e="T595" id="Seg_5435" s="T592">A (külamzittə) bile. </ta>
            <ta e="T598" id="Seg_5436" s="T596">Tăŋ bile. </ta>
            <ta e="T600" id="Seg_5437" s="T599">Vot. </ta>
            <ta e="T602" id="Seg_5438" s="T601">((BRK)). </ta>
            <ta e="T606" id="Seg_5439" s="T603">Šide nʼi amnobiʔi. </ta>
            <ta e="T614" id="Seg_5440" s="T607">Dĭttəgəš dĭ nʼi gibərdə ari dʼürzittə molat. </ta>
            <ta e="T619" id="Seg_5441" s="T615">A oʔb turagənən amnolaʔpliet. </ta>
            <ta e="T626" id="Seg_5442" s="T620">Nu, mĭlat: tănan ine, măna tüžöj. </ta>
            <ta e="T634" id="Seg_5443" s="T627">Dʼok, tăn it ine, a măn tüžöj. </ta>
            <ta e="T637" id="Seg_5444" s="T635">O, enejdənə! </ta>
            <ta e="T640" id="Seg_5445" s="T638">Mo dereʔ? </ta>
            <ta e="T644" id="Seg_5446" s="T641">Nu, tăn koško! </ta>
            <ta e="T646" id="Seg_5447" s="T645">Eneidənəl. </ta>
            <ta e="T650" id="Seg_5448" s="T647">Mo dereʔ kudonzlial? </ta>
            <ta e="T656" id="Seg_5449" s="T651">Dak nada dereʔ kudonzittə, bile. </ta>
            <ta e="T666" id="Seg_5450" s="T657">A măna vedʼ abam mĭbie ine, a tănan tüžöj. </ta>
            <ta e="T675" id="Seg_5451" s="T667">(Măbi): tăn it ine, a tăn it tüžöj. </ta>
            <ta e="T679" id="Seg_5452" s="T676">A tura tănan. </ta>
            <ta e="T685" id="Seg_5453" s="T680">I tura, i tüžöj tănan. </ta>
            <ta e="T691" id="Seg_5454" s="T686">A tüj tüžöj ej iliet. </ta>
            <ta e="T694" id="Seg_5455" s="T692">Ну вот. </ta>
            <ta e="T696" id="Seg_5456" s="T695">((BRK)). </ta>
            <ta e="T698" id="Seg_5457" s="T697">Ой. </ta>
            <ta e="T702" id="Seg_5458" s="T699">Bar gĭda bar. </ta>
            <ta e="T709" id="Seg_5459" s="T703">Bar il (ur-) urgo il (ibiʔjə). </ta>
            <ta e="T713" id="Seg_5460" s="T710">Măn ilbə (ibiʔjə). </ta>
            <ta e="T716" id="Seg_5461" s="T714">I gibər? </ta>
            <ta e="T720" id="Seg_5462" s="T717">Эх, gibər, gibər. </ta>
            <ta e="T724" id="Seg_5463" s="T721">Dĭzeŋ dʼörlapliet, dʼörlapliet. </ta>
            <ta e="T730" id="Seg_5464" s="T725">Bos turagəʔ dĭzem bar ibiʔjə. </ta>
            <ta e="T734" id="Seg_5465" s="T731">A ĭmbi ibiʔi? </ta>
            <ta e="T737" id="Seg_5466" s="T735">Dak ĭmbi. </ta>
            <ta e="T739" id="Seg_5467" s="T738">Kudonzəbiʔi. </ta>
            <ta e="T745" id="Seg_5468" s="T740">I bar gĭda bar măllaplieʔi. </ta>
            <ta e="T750" id="Seg_5469" s="T746">Vot dĭzem i ibiʔi. </ta>
            <ta e="T753" id="Seg_5470" s="T751">A gibər? </ta>
            <ta e="T758" id="Seg_5471" s="T754">Nu măn tĭmnem gibər? </ta>
            <ta e="T763" id="Seg_5472" s="T759">Măn že ej tĭmnem. </ta>
            <ta e="T765" id="Seg_5473" s="T764">((BRK)). </ta>
            <ta e="T769" id="Seg_5474" s="T766">Kanžəbəj (mʼeškaʔi) izittə. </ta>
            <ta e="T772" id="Seg_5475" s="T770">Mʼeška kuvas. </ta>
            <ta e="T777" id="Seg_5476" s="T773">A ĭmbi (molə) dereʔ? </ta>
            <ta e="T781" id="Seg_5477" s="T778">O dak, ĭmbi. </ta>
            <ta e="T788" id="Seg_5478" s="T782">A măn dĭttə mĭnzerləm, amorzittə moləm. </ta>
            <ta e="T792" id="Seg_5479" s="T789">I tăn amorləl. </ta>
            <ta e="T795" id="Seg_5480" s="T793">Jakše dĭ. </ta>
            <ta e="T797" id="Seg_5481" s="T796">Вот. </ta>
            <ta e="T799" id="Seg_5482" s="T798">((BRK)). </ta>
            <ta e="T805" id="Seg_5483" s="T800">Măn dĭʔnə mĭləm von gĭdaʔ. </ta>
            <ta e="T812" id="Seg_5484" s="T806">A dĭ măna idʼie, idʼie mĭliet. </ta>
            <ta e="T814" id="Seg_5485" s="T813">((BRK)). </ta>
            <ta e="T818" id="Seg_5486" s="T815">Nu, kanžəbəj mănziʔ. </ta>
            <ta e="T820" id="Seg_5487" s="T819">Gibər? </ta>
            <ta e="T823" id="Seg_5488" s="T821">Da dĭbər. </ta>
            <ta e="T826" id="Seg_5489" s="T824">A ial? </ta>
            <ta e="T829" id="Seg_5490" s="T827">Ial (pušaj)… </ta>
            <ta e="T835" id="Seg_5491" s="T830">(Ej ej-) Ej iləm iam. </ta>
            <ta e="T840" id="Seg_5492" s="T836">A (miʔ) šidegöʔ kanžəbəj. </ta>
            <ta e="T843" id="Seg_5493" s="T841">A ((PAUSE)) gibər? </ta>
            <ta e="T847" id="Seg_5494" s="T844">Da von, dĭbər. </ta>
            <ta e="T850" id="Seg_5495" s="T848">Gijen il. </ta>
            <ta e="T855" id="Seg_5496" s="T851">A ĭmbi dĭ dĭgən? </ta>
            <ta e="T859" id="Seg_5497" s="T856">(Ĭmbidə=) Ĭmbi-nʼibudʼ nörbəlaʔi. </ta>
            <ta e="T863" id="Seg_5498" s="T860">(A m- m-). </ta>
            <ta e="T866" id="Seg_5499" s="T864">Nörbəlaʔi ĭmbi-nʼibudʼ. </ta>
            <ta e="T873" id="Seg_5500" s="T867">A (miʔ=) măn ĭmbidə ej nörbəlam. </ta>
            <ta e="T878" id="Seg_5501" s="T874">A moʔ ej nörbəlal? </ta>
            <ta e="T880" id="Seg_5502" s="T879">Dʼok. </ta>
            <ta e="T887" id="Seg_5503" s="T881">eželʼi nörbəzittə, (mĭ-) bile il molam. </ta>
            <ta e="T890" id="Seg_5504" s="T888">Mo dereʔ? </ta>
            <ta e="T894" id="Seg_5505" s="T891">Dʼok, jakše il. </ta>
            <ta e="T898" id="Seg_5506" s="T895">Dʼok, bile il. </ta>
            <ta e="T904" id="Seg_5507" s="T899">No, măn ari dʼürləm tănan. </ta>
            <ta e="T906" id="Seg_5508" s="T905">Вот. </ta>
            <ta e="T912" id="Seg_5509" s="T907">Măn abam, tăn abal dʼabrolaʔi. </ta>
            <ta e="T916" id="Seg_5510" s="T913">Mo dĭ dʼabrolaʔi? </ta>
            <ta e="T928" id="Seg_5511" s="T917">Dak dĭ măllat, tăn abal măllat: "Măn nʼim jakše, kuvas. </ta>
            <ta e="T934" id="Seg_5512" s="T929">A tăn koʔbdo ej jakše". </ta>
            <ta e="T944" id="Seg_5513" s="T935">A măn abam măllat: "Eh, măn koʔbdom jakše. </ta>
            <ta e="T947" id="Seg_5514" s="T945">I kuvas. </ta>
            <ta e="T954" id="Seg_5515" s="T948">I bar ĭmbi, bar ĭmbi măllat. </ta>
            <ta e="T960" id="Seg_5516" s="T955">I tăn nʼil ej jakše. </ta>
            <ta e="T965" id="Seg_5517" s="T961">Ĭmbidə ej nörbəlat măna. </ta>
            <ta e="T972" id="Seg_5518" s="T966">A măn koʔbdom bar ĭmbi, bar. </ta>
            <ta e="T976" id="Seg_5519" s="T973">Tănan üge nörbəlapliet. </ta>
            <ta e="T978" id="Seg_5520" s="T977">Vot. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_5521" s="T1">jakše</ta>
            <ta e="T3" id="Seg_5522" s="T2">kuza</ta>
            <ta e="T4" id="Seg_5523" s="T3">mo-bi</ta>
            <ta e="T5" id="Seg_5524" s="T4">kü-lam-bi</ta>
            <ta e="T7" id="Seg_5525" s="T6">jakše</ta>
            <ta e="T8" id="Seg_5526" s="T7">kü-lam-bi</ta>
            <ta e="T10" id="Seg_5527" s="T9">a</ta>
            <ta e="T11" id="Seg_5528" s="T10">dĭ</ta>
            <ta e="T12" id="Seg_5529" s="T11">bile</ta>
            <ta e="T13" id="Seg_5530" s="T12">kuza</ta>
            <ta e="T14" id="Seg_5531" s="T13">oʔb</ta>
            <ta e="T17" id="Seg_5532" s="T16">kü-lam-bi-t</ta>
            <ta e="T19" id="Seg_5533" s="T18">a</ta>
            <ta e="T20" id="Seg_5534" s="T19">jakše</ta>
            <ta e="T21" id="Seg_5535" s="T20">kuza</ta>
            <ta e="T22" id="Seg_5536" s="T21">jakše</ta>
            <ta e="T23" id="Seg_5537" s="T22">kü-lam-bi</ta>
            <ta e="T25" id="Seg_5538" s="T24">vot</ta>
            <ta e="T27" id="Seg_5539" s="T26">măn</ta>
            <ta e="T28" id="Seg_5540" s="T27">aba-m</ta>
            <ta e="T29" id="Seg_5541" s="T28">jakše</ta>
            <ta e="T30" id="Seg_5542" s="T29">amno-bi</ta>
            <ta e="T32" id="Seg_5543" s="T31">dĭttə</ta>
            <ta e="T33" id="Seg_5544" s="T32">bile</ta>
            <ta e="T35" id="Seg_5545" s="T34">tăŋ</ta>
            <ta e="T36" id="Seg_5546" s="T35">bile</ta>
            <ta e="T37" id="Seg_5547" s="T36">amno-bi</ta>
            <ta e="T39" id="Seg_5548" s="T38">dĭttə</ta>
            <ta e="T42" id="Seg_5549" s="T41">bar</ta>
            <ta e="T43" id="Seg_5550" s="T42">amno-bi</ta>
            <ta e="T44" id="Seg_5551" s="T43">jakše</ta>
            <ta e="T46" id="Seg_5552" s="T45">dĭttə</ta>
            <ta e="T47" id="Seg_5553" s="T46">kü-lam-bi</ta>
            <ta e="T49" id="Seg_5554" s="T48">nu</ta>
            <ta e="T50" id="Seg_5555" s="T49">mĭ</ta>
            <ta e="T52" id="Seg_5556" s="T51">măn</ta>
            <ta e="T53" id="Seg_5557" s="T52">amno-bia-m</ta>
            <ta e="T54" id="Seg_5558" s="T53">tăŋ</ta>
            <ta e="T55" id="Seg_5559" s="T54">bile</ta>
            <ta e="T57" id="Seg_5560" s="T56">tăŋ</ta>
            <ta e="T58" id="Seg_5561" s="T57">bile</ta>
            <ta e="T59" id="Seg_5562" s="T58">amno-bia-m</ta>
            <ta e="T61" id="Seg_5563" s="T60">măn</ta>
            <ta e="T62" id="Seg_5564" s="T61">ipek-pə</ta>
            <ta e="T63" id="Seg_5565" s="T62">naga</ta>
            <ta e="T65" id="Seg_5566" s="T64">naga</ta>
            <ta e="T66" id="Seg_5567" s="T65">ipek</ta>
            <ta e="T67" id="Seg_5568" s="T66">măn</ta>
            <ta e="T68" id="Seg_5569" s="T67">i</ta>
            <ta e="T69" id="Seg_5570" s="T68">măn</ta>
            <ta e="T70" id="Seg_5571" s="T69">bos-tə</ta>
            <ta e="T71" id="Seg_5572" s="T70">ĭmbi-ĭmbi</ta>
            <ta e="T72" id="Seg_5573" s="T71">amor-bia-m</ta>
            <ta e="T74" id="Seg_5574" s="T73">dĭttəgə=š</ta>
            <ta e="T75" id="Seg_5575" s="T74">jakše</ta>
            <ta e="T76" id="Seg_5576" s="T75">amno-bia-m</ta>
            <ta e="T78" id="Seg_5577" s="T77">bar</ta>
            <ta e="T81" id="Seg_5578" s="T80">dĭttəgə-š</ta>
            <ta e="T82" id="Seg_5579" s="T81">ari</ta>
            <ta e="T83" id="Seg_5580" s="T82">dʼür-bia-m</ta>
            <ta e="T85" id="Seg_5581" s="T84">amno-bia-m</ta>
            <ta e="T86" id="Seg_5582" s="T85">bile-bile-bile-bile</ta>
            <ta e="T87" id="Seg_5583" s="T86">koʔbdo-m</ta>
            <ta e="T88" id="Seg_5584" s="T87">mĭ-le</ta>
            <ta e="T89" id="Seg_5585" s="T88">mĭ-le</ta>
            <ta e="T90" id="Seg_5586" s="T89">mĭ-lə-bia-m</ta>
            <ta e="T92" id="Seg_5587" s="T91">i</ta>
            <ta e="T93" id="Seg_5588" s="T92">koʔbdo-zi</ta>
            <ta e="T94" id="Seg_5589" s="T93">tăŋ</ta>
            <ta e="T95" id="Seg_5590" s="T94">bile</ta>
            <ta e="T96" id="Seg_5591" s="T95">amno-bia-m</ta>
            <ta e="T98" id="Seg_5592" s="T97">dʼör-la-m</ta>
            <ta e="T100" id="Seg_5593" s="T99">i</ta>
            <ta e="T102" id="Seg_5594" s="T101">al-la-m</ta>
            <ta e="T103" id="Seg_5595" s="T102">i</ta>
            <ta e="T104" id="Seg_5596" s="T103">šo</ta>
            <ta e="T105" id="Seg_5597" s="T104">šo-la-m</ta>
            <ta e="T107" id="Seg_5598" s="T106">amno-zittə</ta>
            <ta e="T108" id="Seg_5599" s="T107">tăŋ</ta>
            <ta e="T109" id="Seg_5600" s="T108">bile</ta>
            <ta e="T111" id="Seg_5601" s="T110">gibər</ta>
            <ta e="T112" id="Seg_5602" s="T111">măna</ta>
            <ta e="T113" id="Seg_5603" s="T112">an-zittə</ta>
            <ta e="T115" id="Seg_5604" s="T114">gibər=də</ta>
            <ta e="T116" id="Seg_5605" s="T115">ej</ta>
            <ta e="T117" id="Seg_5606" s="T116">al-lə-m</ta>
            <ta e="T118" id="Seg_5607" s="T117">lutʼše</ta>
            <ta e="T119" id="Seg_5608" s="T118">kü-lam-bia-m</ta>
            <ta e="T123" id="Seg_5609" s="T122">bar</ta>
            <ta e="T124" id="Seg_5610" s="T123">il</ta>
            <ta e="T125" id="Seg_5611" s="T124">jakše</ta>
            <ta e="T126" id="Seg_5612" s="T125">amno-lia-ʔit</ta>
            <ta e="T128" id="Seg_5613" s="T127">a</ta>
            <ta e="T129" id="Seg_5614" s="T128">dĭttə</ta>
            <ta e="T130" id="Seg_5615" s="T129">ari</ta>
            <ta e="T131" id="Seg_5616" s="T130">dʼür-gelit</ta>
            <ta e="T132" id="Seg_5617" s="T131">dögəʔ</ta>
            <ta e="T133" id="Seg_5618" s="T132">gibər=də</ta>
            <ta e="T135" id="Seg_5619" s="T134">a</ta>
            <ta e="T136" id="Seg_5620" s="T135">dĭttə</ta>
            <ta e="T137" id="Seg_5621" s="T136">moʔ</ta>
            <ta e="T138" id="Seg_5622" s="T137">döbər</ta>
            <ta e="T139" id="Seg_5623" s="T138">že</ta>
            <ta e="T140" id="Seg_5624" s="T139">šo-lə-j</ta>
            <ta e="T142" id="Seg_5625" s="T141">Eh</ta>
            <ta e="T143" id="Seg_5626" s="T142">moʔ</ta>
            <ta e="T144" id="Seg_5627" s="T143">dereʔ</ta>
            <ta e="T145" id="Seg_5628" s="T144">mo-l-la-l</ta>
            <ta e="T147" id="Seg_5629" s="T146">i</ta>
            <ta e="T148" id="Seg_5630" s="T147">ĭmbi</ta>
            <ta e="T150" id="Seg_5631" s="T149">dereʔ</ta>
            <ta e="T152" id="Seg_5632" s="T151">šo-lə-j</ta>
            <ta e="T153" id="Seg_5633" s="T152">döbər</ta>
            <ta e="T155" id="Seg_5634" s="T154">а</ta>
            <ta e="T156" id="Seg_5635" s="T155">döbər</ta>
            <ta e="T157" id="Seg_5636" s="T156">šo-lə-j</ta>
            <ta e="T159" id="Seg_5637" s="T158">a</ta>
            <ta e="T160" id="Seg_5638" s="T159">tăn</ta>
            <ta e="T161" id="Seg_5639" s="T160">ĭmbi</ta>
            <ta e="T162" id="Seg_5640" s="T161">ej</ta>
            <ta e="T163" id="Seg_5641" s="T162">al-lə-l</ta>
            <ta e="T165" id="Seg_5642" s="T164">dʼok</ta>
            <ta e="T167" id="Seg_5643" s="T166">măn</ta>
            <ta e="T168" id="Seg_5644" s="T167">ej</ta>
            <ta e="T169" id="Seg_5645" s="T168">al-lə-m</ta>
            <ta e="T171" id="Seg_5646" s="T170">măn</ta>
            <ta e="T172" id="Seg_5647" s="T171">dö-gən</ta>
            <ta e="T173" id="Seg_5648" s="T172">mo-la-m</ta>
            <ta e="T175" id="Seg_5649" s="T174">vot</ta>
            <ta e="T177" id="Seg_5650" s="T176">BRK</ta>
            <ta e="T179" id="Seg_5651" s="T178">ĭmbi=nʼibudʼ</ta>
            <ta e="T180" id="Seg_5652" s="T179">mĭnzər-zittə</ta>
            <ta e="T181" id="Seg_5653" s="T180">nada</ta>
            <ta e="T183" id="Seg_5654" s="T182">măn</ta>
            <ta e="T184" id="Seg_5655" s="T183">amor-zittə</ta>
            <ta e="T185" id="Seg_5656" s="T184">mo-la-m</ta>
            <ta e="T187" id="Seg_5657" s="T186">BRK</ta>
            <ta e="T189" id="Seg_5658" s="T188">Ĭmbĭ-</ta>
            <ta e="T191" id="Seg_5659" s="T190">ĭmbi</ta>
            <ta e="T192" id="Seg_5660" s="T191">mĭnzer-lie-l</ta>
            <ta e="T194" id="Seg_5661" s="T193">uja</ta>
            <ta e="T196" id="Seg_5662" s="T195">mo</ta>
            <ta e="T197" id="Seg_5663" s="T196">uja</ta>
            <ta e="T199" id="Seg_5664" s="T198">măn</ta>
            <ta e="T200" id="Seg_5665" s="T199">bɨ</ta>
            <ta e="T201" id="Seg_5666" s="T200">jeja-m</ta>
            <ta e="T202" id="Seg_5667" s="T201">am-zit</ta>
            <ta e="T204" id="Seg_5668" s="T203">a</ta>
            <ta e="T205" id="Seg_5669" s="T204">măn</ta>
            <ta e="T206" id="Seg_5670" s="T205">bɨ</ta>
            <ta e="T207" id="Seg_5671" s="T206">süt</ta>
            <ta e="T208" id="Seg_5672" s="T207">bɨ</ta>
            <ta e="T210" id="Seg_5673" s="T209">vot</ta>
            <ta e="T211" id="Seg_5674" s="T210">jakše</ta>
            <ta e="T212" id="Seg_5675" s="T211">bɨ</ta>
            <ta e="T213" id="Seg_5676" s="T212">amor-la-m</ta>
            <ta e="T215" id="Seg_5677" s="T214">mo</ta>
            <ta e="T216" id="Seg_5678" s="T215">süt</ta>
            <ta e="T218" id="Seg_5679" s="T217">süt</ta>
            <ta e="T219" id="Seg_5680" s="T218">dak</ta>
            <ta e="T220" id="Seg_5681" s="T219">süt</ta>
            <ta e="T222" id="Seg_5682" s="T221">a</ta>
            <ta e="T223" id="Seg_5683" s="T222">uja</ta>
            <ta e="T224" id="Seg_5684" s="T223">jakše</ta>
            <ta e="T226" id="Seg_5685" s="T225">BRK</ta>
            <ta e="T228" id="Seg_5686" s="T227">mo</ta>
            <ta e="T229" id="Seg_5687" s="T228">măna</ta>
            <ta e="T230" id="Seg_5688" s="T229">ej</ta>
            <ta e="T231" id="Seg_5689" s="T230">šo-lə-j</ta>
            <ta e="T233" id="Seg_5690" s="T232">a</ta>
            <ta e="T234" id="Seg_5691" s="T233">măn</ta>
            <ta e="T235" id="Seg_5692" s="T234">ia-m</ta>
            <ta e="T236" id="Seg_5693" s="T235">bila</ta>
            <ta e="T239" id="Seg_5694" s="T238">tănan</ta>
            <ta e="T240" id="Seg_5695" s="T239">ej</ta>
            <ta e="T241" id="Seg_5696" s="T240">öʔ-lie-t</ta>
            <ta e="T243" id="Seg_5697" s="T242">mo</ta>
            <ta e="T244" id="Seg_5698" s="T243">dereʔ</ta>
            <ta e="T245" id="Seg_5699" s="T244">ej</ta>
            <ta e="T247" id="Seg_5700" s="T246">nu</ta>
            <ta e="T248" id="Seg_5701" s="T247">măl-la-t</ta>
            <ta e="T249" id="Seg_5702" s="T248">što</ta>
            <ta e="T250" id="Seg_5703" s="T249">bila</ta>
            <ta e="T251" id="Seg_5704" s="T250">kuza-t</ta>
            <ta e="T252" id="Seg_5705" s="T251">tăn</ta>
            <ta e="T254" id="Seg_5706" s="T253">mo</ta>
            <ta e="T255" id="Seg_5707" s="T254">dere</ta>
            <ta e="T256" id="Seg_5708" s="T255">bila</ta>
            <ta e="T257" id="Seg_5709" s="T256">kuza</ta>
            <ta e="T259" id="Seg_5710" s="T258">măn</ta>
            <ta e="T260" id="Seg_5711" s="T259">jakše</ta>
            <ta e="T261" id="Seg_5712" s="T260">kuza</ta>
            <ta e="T263" id="Seg_5713" s="T262">ĭmbi=nʼibudʼ</ta>
            <ta e="T264" id="Seg_5714" s="T263">tănan</ta>
            <ta e="T265" id="Seg_5715" s="T264">bar</ta>
            <ta e="T266" id="Seg_5716" s="T265">bɨ</ta>
            <ta e="T267" id="Seg_5717" s="T266">nörbə-la-m</ta>
            <ta e="T268" id="Seg_5718" s="T267">nörbə-la-m</ta>
            <ta e="T269" id="Seg_5719" s="T268">bɨ</ta>
            <ta e="T271" id="Seg_5720" s="T270">teʔi</ta>
            <ta e="T272" id="Seg_5721" s="T271">tăŋ</ta>
            <ta e="T273" id="Seg_5722" s="T272">jakše</ta>
            <ta e="T274" id="Seg_5723" s="T273">mo-lə-j</ta>
            <ta e="T276" id="Seg_5724" s="T275">oh</ta>
            <ta e="T277" id="Seg_5725" s="T276">nu</ta>
            <ta e="T278" id="Seg_5726" s="T277">dereʔ</ta>
            <ta e="T279" id="Seg_5727" s="T278">măl-la-t</ta>
            <ta e="T280" id="Seg_5728" s="T279">măna</ta>
            <ta e="T282" id="Seg_5729" s="T281">măn</ta>
            <ta e="T283" id="Seg_5730" s="T282">dĭ-m</ta>
            <ta e="T284" id="Seg_5731" s="T283">bar</ta>
            <ta e="T285" id="Seg_5732" s="T284">münör-la-m</ta>
            <ta e="T286" id="Seg_5733" s="T285">tăŋ</ta>
            <ta e="T288" id="Seg_5734" s="T287">dʼok</ta>
            <ta e="T289" id="Seg_5735" s="T288">münör-zittə</ta>
            <ta e="T290" id="Seg_5736" s="T289">ne</ta>
            <ta e="T293" id="Seg_5737" s="T292">ato</ta>
            <ta e="T294" id="Seg_5738" s="T293">dĭ</ta>
            <ta e="T295" id="Seg_5739" s="T294">măna</ta>
            <ta e="T296" id="Seg_5740" s="T295">münör-li-l</ta>
            <ta e="T297" id="Seg_5741" s="T296">dittəga-š</ta>
            <ta e="T299" id="Seg_5742" s="T298">BRK</ta>
            <ta e="T301" id="Seg_5743" s="T300">măn</ta>
            <ta e="T302" id="Seg_5744" s="T301">tăŋ</ta>
            <ta e="T303" id="Seg_5745" s="T302">ara</ta>
            <ta e="T304" id="Seg_5746" s="T303">bĭt-lie-m</ta>
            <ta e="T306" id="Seg_5747" s="T305">o</ta>
            <ta e="T307" id="Seg_5748" s="T306">mo</ta>
            <ta e="T308" id="Seg_5749" s="T307">dereʔ</ta>
            <ta e="T309" id="Seg_5750" s="T308">tăn</ta>
            <ta e="T311" id="Seg_5751" s="T310">nu</ta>
            <ta e="T312" id="Seg_5752" s="T311">măn</ta>
            <ta e="T313" id="Seg_5753" s="T312">bile</ta>
            <ta e="T314" id="Seg_5754" s="T313">kuza</ta>
            <ta e="T316" id="Seg_5755" s="T315">bar</ta>
            <ta e="T317" id="Seg_5756" s="T316">gĭda</ta>
            <ta e="T318" id="Seg_5757" s="T317">bar</ta>
            <ta e="T319" id="Seg_5758" s="T318">i-lə-m</ta>
            <ta e="T320" id="Seg_5759" s="T319">ĭmbi-də</ta>
            <ta e="T322" id="Seg_5760" s="T321">a</ta>
            <ta e="T323" id="Seg_5761" s="T322">mo</ta>
            <ta e="T324" id="Seg_5762" s="T323">tănan</ta>
            <ta e="T325" id="Seg_5763" s="T324">i-lə-l</ta>
            <ta e="T326" id="Seg_5764" s="T325">dak</ta>
            <ta e="T327" id="Seg_5765" s="T326">măn</ta>
            <ta e="T328" id="Seg_5766" s="T327">dĭttəgə=š</ta>
            <ta e="T330" id="Seg_5767" s="T329">măn</ta>
            <ta e="T331" id="Seg_5768" s="T330">dittəgə-š</ta>
            <ta e="T332" id="Seg_5769" s="T331">i-lə-m</ta>
            <ta e="T334" id="Seg_5770" s="T333">a</ta>
            <ta e="T335" id="Seg_5771" s="T334">mo</ta>
            <ta e="T336" id="Seg_5772" s="T335">dĭn</ta>
            <ta e="T337" id="Seg_5773" s="T336">dere</ta>
            <ta e="T338" id="Seg_5774" s="T337">i-lə-l</ta>
            <ta e="T340" id="Seg_5775" s="T339">dak</ta>
            <ta e="T341" id="Seg_5776" s="T340">vot</ta>
            <ta e="T342" id="Seg_5777" s="T341">i-lə-m</ta>
            <ta e="T343" id="Seg_5778" s="T342">măn</ta>
            <ta e="T345" id="Seg_5779" s="T344">măna</ta>
            <ta e="T346" id="Seg_5780" s="T345">dĭttəgə-š</ta>
            <ta e="T347" id="Seg_5781" s="T346">ej</ta>
            <ta e="T348" id="Seg_5782" s="T347">kuvas</ta>
            <ta e="T349" id="Seg_5783" s="T348">tura-nə</ta>
            <ta e="T352" id="Seg_5784" s="T351">măn</ta>
            <ta e="T353" id="Seg_5785" s="T352">dĭgən</ta>
            <ta e="T354" id="Seg_5786" s="T353">amno-la-m</ta>
            <ta e="T356" id="Seg_5787" s="T355">a</ta>
            <ta e="T357" id="Seg_5788" s="T356">dĭttə</ta>
            <ta e="T358" id="Seg_5789" s="T357">kak</ta>
            <ta e="T359" id="Seg_5790" s="T358">šo-lə-m</ta>
            <ta e="T360" id="Seg_5791" s="T359">măna</ta>
            <ta e="T361" id="Seg_5792" s="T360">il</ta>
            <ta e="T363" id="Seg_5793" s="T362">šindĭ-də</ta>
            <ta e="T364" id="Seg_5794" s="T363">ej</ta>
            <ta e="T366" id="Seg_5795" s="T365">ej</ta>
            <ta e="T373" id="Seg_5796" s="T372">ej</ta>
            <ta e="T374" id="Seg_5797" s="T373">i-lie-t</ta>
            <ta e="T376" id="Seg_5798" s="T375">bar</ta>
            <ta e="T377" id="Seg_5799" s="T376">il</ta>
            <ta e="T379" id="Seg_5800" s="T378">i</ta>
            <ta e="T380" id="Seg_5801" s="T379">ej</ta>
            <ta e="T381" id="Seg_5802" s="T380">jakše</ta>
            <ta e="T382" id="Seg_5803" s="T381">kuza</ta>
            <ta e="T384" id="Seg_5804" s="T383">vot</ta>
            <ta e="T386" id="Seg_5805" s="T385">BRK</ta>
            <ta e="T388" id="Seg_5806" s="T387">bile</ta>
            <ta e="T389" id="Seg_5807" s="T388">kuza</ta>
            <ta e="T390" id="Seg_5808" s="T389">jakše</ta>
            <ta e="T391" id="Seg_5809" s="T390">kuza-n</ta>
            <ta e="T392" id="Seg_5810" s="T391">i-bi</ta>
            <ta e="T393" id="Seg_5811" s="T392">kujnek</ta>
            <ta e="T394" id="Seg_5812" s="T393">uja</ta>
            <ta e="T395" id="Seg_5813" s="T394">i</ta>
            <ta e="T396" id="Seg_5814" s="T395">bar</ta>
            <ta e="T397" id="Seg_5815" s="T396">ĭmbi</ta>
            <ta e="T398" id="Seg_5816" s="T397">bar</ta>
            <ta e="T400" id="Seg_5817" s="T399">aktʼa-t</ta>
            <ta e="T401" id="Seg_5818" s="T400">mo-bi</ta>
            <ta e="T402" id="Seg_5819" s="T401">dĭ</ta>
            <ta e="T403" id="Seg_5820" s="T402">kuza-n</ta>
            <ta e="T405" id="Seg_5821" s="T404">bar</ta>
            <ta e="T406" id="Seg_5822" s="T405">i-bi</ta>
            <ta e="T408" id="Seg_5823" s="T407">ĭmbi</ta>
            <ta e="T409" id="Seg_5824" s="T408">tăn</ta>
            <ta e="T410" id="Seg_5825" s="T409">i-bie-l</ta>
            <ta e="T412" id="Seg_5826" s="T411">dak</ta>
            <ta e="T413" id="Seg_5827" s="T412">măn</ta>
            <ta e="T414" id="Seg_5828" s="T413">naga</ta>
            <ta e="T415" id="Seg_5829" s="T414">a</ta>
            <ta e="T416" id="Seg_5830" s="T415">tăn</ta>
            <ta e="T417" id="Seg_5831" s="T416">i-ge</ta>
            <ta e="T420" id="Seg_5832" s="T419">măn</ta>
            <ta e="T421" id="Seg_5833" s="T420">i-bie-m</ta>
            <ta e="T423" id="Seg_5834" s="T422">a</ta>
            <ta e="T424" id="Seg_5835" s="T423">măn</ta>
            <ta e="T425" id="Seg_5836" s="T424">tănan</ta>
            <ta e="T426" id="Seg_5837" s="T425">ej</ta>
            <ta e="T427" id="Seg_5838" s="T426">i-lə-m</ta>
            <ta e="T429" id="Seg_5839" s="T428">măn</ta>
            <ta e="T430" id="Seg_5840" s="T429">tănan</ta>
            <ta e="T431" id="Seg_5841" s="T430">i-lə-m</ta>
            <ta e="T433" id="Seg_5842" s="T432">a</ta>
            <ta e="T434" id="Seg_5843" s="T433">moʔ</ta>
            <ta e="T435" id="Seg_5844" s="T434">nĭn</ta>
            <ta e="T436" id="Seg_5845" s="T435">măna</ta>
            <ta e="T437" id="Seg_5846" s="T436">i-lə-l</ta>
            <ta e="T439" id="Seg_5847" s="T438">dak</ta>
            <ta e="T440" id="Seg_5848" s="T439">nĭn</ta>
            <ta e="T441" id="Seg_5849" s="T440">našto</ta>
            <ta e="T442" id="Seg_5850" s="T441">i-bie-l</ta>
            <ta e="T444" id="Seg_5851" s="T443">măn</ta>
            <ta e="T445" id="Seg_5852" s="T444">kun-naʔbə</ta>
            <ta e="T446" id="Seg_5853" s="T445">i</ta>
            <ta e="T447" id="Seg_5854" s="T446">bar</ta>
            <ta e="T448" id="Seg_5855" s="T447">ĭmbi-m</ta>
            <ta e="T449" id="Seg_5856" s="T448">bar</ta>
            <ta e="T451" id="Seg_5857" s="T450">a</ta>
            <ta e="T452" id="Seg_5858" s="T451">măn</ta>
            <ta e="T453" id="Seg_5859" s="T452">bile</ta>
            <ta e="T454" id="Seg_5860" s="T453">tura-nə</ta>
            <ta e="T455" id="Seg_5861" s="T454">tănan</ta>
            <ta e="T456" id="Seg_5862" s="T455">amno-laʔbə</ta>
            <ta e="T459" id="Seg_5863" s="T458">vot</ta>
            <ta e="T461" id="Seg_5864" s="T460">BRK</ta>
            <ta e="T463" id="Seg_5865" s="T462">amno-ʔ</ta>
            <ta e="T464" id="Seg_5866" s="T463">măn-zi</ta>
            <ta e="T465" id="Seg_5867" s="T464">amor-zittə</ta>
            <ta e="T467" id="Seg_5868" s="T466">oj</ta>
            <ta e="T477" id="Seg_5869" s="T476">BRK</ta>
            <ta e="T479" id="Seg_5870" s="T478">amno-ʔ</ta>
            <ta e="T480" id="Seg_5871" s="T479">măn-zi</ta>
            <ta e="T481" id="Seg_5872" s="T480">amor-zittə</ta>
            <ta e="T483" id="Seg_5873" s="T482">da</ta>
            <ta e="T484" id="Seg_5874" s="T483">ĭmbi</ta>
            <ta e="T485" id="Seg_5875" s="T484">tăn</ta>
            <ta e="T486" id="Seg_5876" s="T485">mo-la-t</ta>
            <ta e="T488" id="Seg_5877" s="T487">da</ta>
            <ta e="T489" id="Seg_5878" s="T488">măn</ta>
            <ta e="T490" id="Seg_5879" s="T489">mo-la-t</ta>
            <ta e="T491" id="Seg_5880" s="T490">ipek</ta>
            <ta e="T492" id="Seg_5881" s="T491">uja</ta>
            <ta e="T493" id="Seg_5882" s="T492">i</ta>
            <ta e="T494" id="Seg_5883" s="T493">süt</ta>
            <ta e="T496" id="Seg_5884" s="T495">sĭrep-ne</ta>
            <ta e="T498" id="Seg_5885" s="T497">am-də</ta>
            <ta e="T500" id="Seg_5886" s="T499">dak</ta>
            <ta e="T501" id="Seg_5887" s="T500">măn</ta>
            <ta e="T502" id="Seg_5888" s="T501">bɨ</ta>
            <ta e="T503" id="Seg_5889" s="T502">kajak</ta>
            <ta e="T504" id="Seg_5890" s="T503">amor-lia-m</ta>
            <ta e="T505" id="Seg_5891" s="T504">bɨ</ta>
            <ta e="T507" id="Seg_5892" s="T506">nu</ta>
            <ta e="T508" id="Seg_5893" s="T507">i</ta>
            <ta e="T509" id="Seg_5894" s="T508">kajak</ta>
            <ta e="T510" id="Seg_5895" s="T509">măn</ta>
            <ta e="T511" id="Seg_5896" s="T510">mĭ-le-m</ta>
            <ta e="T512" id="Seg_5897" s="T511">tănan</ta>
            <ta e="T513" id="Seg_5898" s="T512">amor-zittə</ta>
            <ta e="T515" id="Seg_5899" s="T514">jakše</ta>
            <ta e="T516" id="Seg_5900" s="T515">tolʼkă</ta>
            <ta e="T517" id="Seg_5901" s="T516">amor-a-ʔ</ta>
            <ta e="T519" id="Seg_5902" s="T518">dö</ta>
            <ta e="T520" id="Seg_5903" s="T519">dak</ta>
            <ta e="T521" id="Seg_5904" s="T520">măn</ta>
            <ta e="T522" id="Seg_5905" s="T521">amor-la-m</ta>
            <ta e="T524" id="Seg_5906" s="T523">tăŋ</ta>
            <ta e="T525" id="Seg_5907" s="T524">tăŋ</ta>
            <ta e="T526" id="Seg_5908" s="T525">amor-la-m</ta>
            <ta e="T528" id="Seg_5909" s="T527">jakše</ta>
            <ta e="T529" id="Seg_5910" s="T528">amor-lia-m</ta>
            <ta e="T533" id="Seg_5911" s="T532">BRK</ta>
            <ta e="T535" id="Seg_5912" s="T534">mo</ta>
            <ta e="T536" id="Seg_5913" s="T535">tăn</ta>
            <ta e="T537" id="Seg_5914" s="T536">măn</ta>
            <ta e="T538" id="Seg_5915" s="T537">eʔbde-m</ta>
            <ta e="T539" id="Seg_5916" s="T538">bar</ta>
            <ta e="T540" id="Seg_5917" s="T539">bos</ta>
            <ta e="T541" id="Seg_5918" s="T540">uda-nə</ta>
            <ta e="T542" id="Seg_5919" s="T541">i-bie-l</ta>
            <ta e="T544" id="Seg_5920" s="T543">vedʼ</ta>
            <ta e="T545" id="Seg_5921" s="T544">tăn</ta>
            <ta e="T546" id="Seg_5922" s="T545">uda-l</ta>
            <ta e="T547" id="Seg_5923" s="T546">urgo</ta>
            <ta e="T548" id="Seg_5924" s="T547">a</ta>
            <ta e="T549" id="Seg_5925" s="T548">măn</ta>
            <ta e="T550" id="Seg_5926" s="T549">idiʼije</ta>
            <ta e="T552" id="Seg_5927" s="T551">mo</ta>
            <ta e="T553" id="Seg_5928" s="T552">dereʔ</ta>
            <ta e="T554" id="Seg_5929" s="T553">mo-la-l</ta>
            <ta e="T556" id="Seg_5930" s="T555">bile</ta>
            <ta e="T558" id="Seg_5931" s="T557">ej</ta>
            <ta e="T559" id="Seg_5932" s="T558">jakše</ta>
            <ta e="T560" id="Seg_5933" s="T559">dereʔ</ta>
            <ta e="T562" id="Seg_5934" s="T561">vot</ta>
            <ta e="T564" id="Seg_5935" s="T563">BRK</ta>
            <ta e="T566" id="Seg_5936" s="T565">măn</ta>
            <ta e="T567" id="Seg_5937" s="T566">tăŋ</ta>
            <ta e="T568" id="Seg_5938" s="T567">ĭzem-neʔ-bie-m</ta>
            <ta e="T570" id="Seg_5939" s="T569">uda-za-mbə</ta>
            <ta e="T571" id="Seg_5940" s="T570">ĭzəm-nie-t</ta>
            <ta e="T573" id="Seg_5941" s="T572">i</ta>
            <ta e="T574" id="Seg_5942" s="T573">uju-bə</ta>
            <ta e="T575" id="Seg_5943" s="T574">bar</ta>
            <ta e="T576" id="Seg_5944" s="T575">ĭzem-neʔbə-lie-t</ta>
            <ta e="T578" id="Seg_5945" s="T577">ădnakă</ta>
            <ta e="T579" id="Seg_5946" s="T578">măn</ta>
            <ta e="T580" id="Seg_5947" s="T579">kü-la-m-bil</ta>
            <ta e="T582" id="Seg_5948" s="T581">dʼok</ta>
            <ta e="T583" id="Seg_5949" s="T582">nʼe</ta>
            <ta e="T584" id="Seg_5950" s="T583">nada</ta>
            <ta e="T585" id="Seg_5951" s="T584">kü-la-m-zittə</ta>
            <ta e="T587" id="Seg_5952" s="T586">nada</ta>
            <ta e="T588" id="Seg_5953" s="T587">amno-zittə</ta>
            <ta e="T590" id="Seg_5954" s="T589">amno-zittə</ta>
            <ta e="T591" id="Seg_5955" s="T590">jakše</ta>
            <ta e="T593" id="Seg_5956" s="T592">a</ta>
            <ta e="T594" id="Seg_5957" s="T593">kü-la-m-zittə</ta>
            <ta e="T595" id="Seg_5958" s="T594">bile</ta>
            <ta e="T597" id="Seg_5959" s="T596">tăŋ</ta>
            <ta e="T598" id="Seg_5960" s="T597">bile</ta>
            <ta e="T600" id="Seg_5961" s="T599">vot</ta>
            <ta e="T602" id="Seg_5962" s="T601">BRK</ta>
            <ta e="T604" id="Seg_5963" s="T603">šide</ta>
            <ta e="T605" id="Seg_5964" s="T604">nʼi</ta>
            <ta e="T606" id="Seg_5965" s="T605">amno-bi-ʔi</ta>
            <ta e="T608" id="Seg_5966" s="T607">dĭttəgə=š</ta>
            <ta e="T609" id="Seg_5967" s="T608">dĭ</ta>
            <ta e="T610" id="Seg_5968" s="T609">nʼi</ta>
            <ta e="T611" id="Seg_5969" s="T610">gibər=də</ta>
            <ta e="T612" id="Seg_5970" s="T611">ari</ta>
            <ta e="T613" id="Seg_5971" s="T612">dʼür-zittə</ta>
            <ta e="T614" id="Seg_5972" s="T613">mo-la-t</ta>
            <ta e="T616" id="Seg_5973" s="T615">a</ta>
            <ta e="T617" id="Seg_5974" s="T616">oʔb</ta>
            <ta e="T618" id="Seg_5975" s="T617">tura-gən-ə-n</ta>
            <ta e="T619" id="Seg_5976" s="T618">amno-laʔp-lie-t</ta>
            <ta e="T621" id="Seg_5977" s="T620">nu</ta>
            <ta e="T622" id="Seg_5978" s="T621">mĭ-la-t</ta>
            <ta e="T623" id="Seg_5979" s="T622">tănan</ta>
            <ta e="T624" id="Seg_5980" s="T623">ine</ta>
            <ta e="T625" id="Seg_5981" s="T624">măna</ta>
            <ta e="T626" id="Seg_5982" s="T625">tüžöj</ta>
            <ta e="T628" id="Seg_5983" s="T627">dʼok</ta>
            <ta e="T629" id="Seg_5984" s="T628">tăn</ta>
            <ta e="T630" id="Seg_5985" s="T629">i-t</ta>
            <ta e="T631" id="Seg_5986" s="T630">ine</ta>
            <ta e="T632" id="Seg_5987" s="T631">a</ta>
            <ta e="T633" id="Seg_5988" s="T632">măn</ta>
            <ta e="T634" id="Seg_5989" s="T633">tüžöj</ta>
            <ta e="T636" id="Seg_5990" s="T635">o</ta>
            <ta e="T637" id="Seg_5991" s="T636">enejdənə</ta>
            <ta e="T639" id="Seg_5992" s="T638">mo</ta>
            <ta e="T640" id="Seg_5993" s="T639">dereʔ</ta>
            <ta e="T642" id="Seg_5994" s="T641">nu</ta>
            <ta e="T643" id="Seg_5995" s="T642">tăn</ta>
            <ta e="T644" id="Seg_5996" s="T643">koško</ta>
            <ta e="T646" id="Seg_5997" s="T645">eneidənə-l</ta>
            <ta e="T648" id="Seg_5998" s="T647">mo</ta>
            <ta e="T649" id="Seg_5999" s="T648">dereʔ</ta>
            <ta e="T650" id="Seg_6000" s="T649">kudo-nz-lia-l</ta>
            <ta e="T652" id="Seg_6001" s="T651">dak</ta>
            <ta e="T653" id="Seg_6002" s="T652">nada</ta>
            <ta e="T654" id="Seg_6003" s="T653">dereʔ</ta>
            <ta e="T655" id="Seg_6004" s="T654">kudon-zittə</ta>
            <ta e="T656" id="Seg_6005" s="T655">bile</ta>
            <ta e="T658" id="Seg_6006" s="T657">a</ta>
            <ta e="T659" id="Seg_6007" s="T658">măna</ta>
            <ta e="T660" id="Seg_6008" s="T659">vedʼ</ta>
            <ta e="T661" id="Seg_6009" s="T660">aba-m</ta>
            <ta e="T662" id="Seg_6010" s="T661">mĭ-bie</ta>
            <ta e="T663" id="Seg_6011" s="T662">ine</ta>
            <ta e="T664" id="Seg_6012" s="T663">a</ta>
            <ta e="T665" id="Seg_6013" s="T664">tănan</ta>
            <ta e="T666" id="Seg_6014" s="T665">tüžöj</ta>
            <ta e="T668" id="Seg_6015" s="T667">mă-bi</ta>
            <ta e="T669" id="Seg_6016" s="T668">tăn</ta>
            <ta e="T670" id="Seg_6017" s="T669">i-t</ta>
            <ta e="T671" id="Seg_6018" s="T670">ine</ta>
            <ta e="T672" id="Seg_6019" s="T671">a</ta>
            <ta e="T673" id="Seg_6020" s="T672">tăn</ta>
            <ta e="T674" id="Seg_6021" s="T673">i-t</ta>
            <ta e="T675" id="Seg_6022" s="T674">tüžöj</ta>
            <ta e="T677" id="Seg_6023" s="T676">a</ta>
            <ta e="T678" id="Seg_6024" s="T677">tura</ta>
            <ta e="T679" id="Seg_6025" s="T678">tănan</ta>
            <ta e="T681" id="Seg_6026" s="T680">i</ta>
            <ta e="T682" id="Seg_6027" s="T681">tura</ta>
            <ta e="T683" id="Seg_6028" s="T682">i</ta>
            <ta e="T684" id="Seg_6029" s="T683">tüžöj</ta>
            <ta e="T685" id="Seg_6030" s="T684">tănan</ta>
            <ta e="T687" id="Seg_6031" s="T686">a</ta>
            <ta e="T688" id="Seg_6032" s="T687">tüj</ta>
            <ta e="T689" id="Seg_6033" s="T688">tüžöj</ta>
            <ta e="T690" id="Seg_6034" s="T689">ej</ta>
            <ta e="T691" id="Seg_6035" s="T690">i-lie-t</ta>
            <ta e="T696" id="Seg_6036" s="T695">BRK</ta>
            <ta e="T700" id="Seg_6037" s="T699">bar</ta>
            <ta e="T701" id="Seg_6038" s="T700">gĭda</ta>
            <ta e="T702" id="Seg_6039" s="T701">bar</ta>
            <ta e="T704" id="Seg_6040" s="T703">bar</ta>
            <ta e="T705" id="Seg_6041" s="T704">il</ta>
            <ta e="T707" id="Seg_6042" s="T706">urgo</ta>
            <ta e="T708" id="Seg_6043" s="T707">il</ta>
            <ta e="T709" id="Seg_6044" s="T708">i-bi-ʔjə</ta>
            <ta e="T711" id="Seg_6045" s="T710">măn</ta>
            <ta e="T712" id="Seg_6046" s="T711">il-bə</ta>
            <ta e="T713" id="Seg_6047" s="T712">i-bi-ʔjə</ta>
            <ta e="T715" id="Seg_6048" s="T714">i</ta>
            <ta e="T716" id="Seg_6049" s="T715">gibər</ta>
            <ta e="T719" id="Seg_6050" s="T718">gibər</ta>
            <ta e="T720" id="Seg_6051" s="T719">gibər</ta>
            <ta e="T722" id="Seg_6052" s="T721">dĭ-zeŋ</ta>
            <ta e="T723" id="Seg_6053" s="T722">dʼör-lap-lie-t</ta>
            <ta e="T724" id="Seg_6054" s="T723">dʼör-lap-lie-t</ta>
            <ta e="T726" id="Seg_6055" s="T725">bos</ta>
            <ta e="T727" id="Seg_6056" s="T726">tura-gəʔ</ta>
            <ta e="T728" id="Seg_6057" s="T727">dĭ-zem</ta>
            <ta e="T729" id="Seg_6058" s="T728">bar</ta>
            <ta e="T730" id="Seg_6059" s="T729">i-bi-ʔjə</ta>
            <ta e="T732" id="Seg_6060" s="T731">a</ta>
            <ta e="T733" id="Seg_6061" s="T732">ĭmbi</ta>
            <ta e="T734" id="Seg_6062" s="T733">i-bi-ʔi</ta>
            <ta e="T736" id="Seg_6063" s="T735">dak</ta>
            <ta e="T737" id="Seg_6064" s="T736">ĭmbi</ta>
            <ta e="T739" id="Seg_6065" s="T738">kudo-nzə-bi-ʔi</ta>
            <ta e="T741" id="Seg_6066" s="T740">i</ta>
            <ta e="T742" id="Seg_6067" s="T741">bar</ta>
            <ta e="T743" id="Seg_6068" s="T742">gĭda</ta>
            <ta e="T744" id="Seg_6069" s="T743">bar</ta>
            <ta e="T745" id="Seg_6070" s="T744">măl-lap-lie-ʔi</ta>
            <ta e="T747" id="Seg_6071" s="T746">vot</ta>
            <ta e="T748" id="Seg_6072" s="T747">dĭ-zem</ta>
            <ta e="T749" id="Seg_6073" s="T748">i</ta>
            <ta e="T750" id="Seg_6074" s="T749">i-bi-ʔi</ta>
            <ta e="T752" id="Seg_6075" s="T751">a</ta>
            <ta e="T753" id="Seg_6076" s="T752">gibər</ta>
            <ta e="T755" id="Seg_6077" s="T754">nu</ta>
            <ta e="T756" id="Seg_6078" s="T755">măn</ta>
            <ta e="T757" id="Seg_6079" s="T756">tĭmne-m</ta>
            <ta e="T758" id="Seg_6080" s="T757">gibər</ta>
            <ta e="T760" id="Seg_6081" s="T759">măn</ta>
            <ta e="T761" id="Seg_6082" s="T760">že</ta>
            <ta e="T762" id="Seg_6083" s="T761">ej</ta>
            <ta e="T763" id="Seg_6084" s="T762">tĭmne-m</ta>
            <ta e="T765" id="Seg_6085" s="T764">BRK</ta>
            <ta e="T767" id="Seg_6086" s="T766">kan-žə-bəj</ta>
            <ta e="T768" id="Seg_6087" s="T767">mʼeška-ʔi</ta>
            <ta e="T769" id="Seg_6088" s="T768">i-zittə</ta>
            <ta e="T771" id="Seg_6089" s="T770">mʼeška</ta>
            <ta e="T772" id="Seg_6090" s="T771">kuvas</ta>
            <ta e="T774" id="Seg_6091" s="T773">a</ta>
            <ta e="T775" id="Seg_6092" s="T774">ĭmbi</ta>
            <ta e="T777" id="Seg_6093" s="T776">dereʔ</ta>
            <ta e="T779" id="Seg_6094" s="T778">o</ta>
            <ta e="T780" id="Seg_6095" s="T779">dak</ta>
            <ta e="T781" id="Seg_6096" s="T780">ĭmbi</ta>
            <ta e="T783" id="Seg_6097" s="T782">a</ta>
            <ta e="T784" id="Seg_6098" s="T783">măn</ta>
            <ta e="T785" id="Seg_6099" s="T784">dĭttə</ta>
            <ta e="T786" id="Seg_6100" s="T785">mĭnzer-lə-m</ta>
            <ta e="T787" id="Seg_6101" s="T786">amor-zittə</ta>
            <ta e="T788" id="Seg_6102" s="T787">mo-lə-m</ta>
            <ta e="T790" id="Seg_6103" s="T789">i</ta>
            <ta e="T791" id="Seg_6104" s="T790">tăn</ta>
            <ta e="T792" id="Seg_6105" s="T791">amor-lə-l</ta>
            <ta e="T794" id="Seg_6106" s="T793">jakše</ta>
            <ta e="T795" id="Seg_6107" s="T794">dĭ</ta>
            <ta e="T799" id="Seg_6108" s="T798">BRK</ta>
            <ta e="T801" id="Seg_6109" s="T800">măn</ta>
            <ta e="T802" id="Seg_6110" s="T801">dĭʔ-nə</ta>
            <ta e="T803" id="Seg_6111" s="T802">mĭ-lə-m</ta>
            <ta e="T804" id="Seg_6112" s="T803">von</ta>
            <ta e="T805" id="Seg_6113" s="T804">gĭda-ʔ</ta>
            <ta e="T807" id="Seg_6114" s="T806">a</ta>
            <ta e="T808" id="Seg_6115" s="T807">dĭ</ta>
            <ta e="T809" id="Seg_6116" s="T808">măna</ta>
            <ta e="T810" id="Seg_6117" s="T809">idʼie</ta>
            <ta e="T811" id="Seg_6118" s="T810">idʼie</ta>
            <ta e="T812" id="Seg_6119" s="T811">mĭ-lie-t</ta>
            <ta e="T814" id="Seg_6120" s="T813">BRK</ta>
            <ta e="T816" id="Seg_6121" s="T815">nu</ta>
            <ta e="T817" id="Seg_6122" s="T816">kan-žə-bəj</ta>
            <ta e="T818" id="Seg_6123" s="T817">măn-ziʔ</ta>
            <ta e="T820" id="Seg_6124" s="T819">gibər</ta>
            <ta e="T822" id="Seg_6125" s="T821">da</ta>
            <ta e="T823" id="Seg_6126" s="T822">dĭbər</ta>
            <ta e="T825" id="Seg_6127" s="T824">a</ta>
            <ta e="T826" id="Seg_6128" s="T825">ia-l</ta>
            <ta e="T828" id="Seg_6129" s="T827">ia-l</ta>
            <ta e="T829" id="Seg_6130" s="T828">pušaj</ta>
            <ta e="T831" id="Seg_6131" s="T830">ej</ta>
            <ta e="T833" id="Seg_6132" s="T832">ej</ta>
            <ta e="T834" id="Seg_6133" s="T833">i-l-m</ta>
            <ta e="T835" id="Seg_6134" s="T834">ia-m</ta>
            <ta e="T837" id="Seg_6135" s="T836">a</ta>
            <ta e="T838" id="Seg_6136" s="T837">miʔ</ta>
            <ta e="T839" id="Seg_6137" s="T838">šide-göʔ</ta>
            <ta e="T840" id="Seg_6138" s="T839">kan-žə-bəj</ta>
            <ta e="T842" id="Seg_6139" s="T841">a</ta>
            <ta e="T843" id="Seg_6140" s="T842">gibər</ta>
            <ta e="T845" id="Seg_6141" s="T844">da</ta>
            <ta e="T846" id="Seg_6142" s="T845">von</ta>
            <ta e="T847" id="Seg_6143" s="T846">dĭbər</ta>
            <ta e="T849" id="Seg_6144" s="T848">gijen</ta>
            <ta e="T850" id="Seg_6145" s="T849">il</ta>
            <ta e="T852" id="Seg_6146" s="T851">a</ta>
            <ta e="T853" id="Seg_6147" s="T852">ĭmbi</ta>
            <ta e="T854" id="Seg_6148" s="T853">dĭ</ta>
            <ta e="T855" id="Seg_6149" s="T854">dĭgən</ta>
            <ta e="T857" id="Seg_6150" s="T856">ĭmbi=də</ta>
            <ta e="T858" id="Seg_6151" s="T857">ĭmbi=nʼibudʼ</ta>
            <ta e="T859" id="Seg_6152" s="T858">nörbə-la-ʔi</ta>
            <ta e="T861" id="Seg_6153" s="T860">a</ta>
            <ta e="T865" id="Seg_6154" s="T864">nörbə-la-ʔi</ta>
            <ta e="T866" id="Seg_6155" s="T865">ĭmbi=nʼibudʼ</ta>
            <ta e="T868" id="Seg_6156" s="T867">a</ta>
            <ta e="T869" id="Seg_6157" s="T868">miʔ</ta>
            <ta e="T870" id="Seg_6158" s="T869">măn</ta>
            <ta e="T871" id="Seg_6159" s="T870">ĭmbi=də</ta>
            <ta e="T872" id="Seg_6160" s="T871">ej</ta>
            <ta e="T873" id="Seg_6161" s="T872">nörbə-la-m</ta>
            <ta e="T875" id="Seg_6162" s="T874">a</ta>
            <ta e="T876" id="Seg_6163" s="T875">moʔ</ta>
            <ta e="T877" id="Seg_6164" s="T876">ej</ta>
            <ta e="T878" id="Seg_6165" s="T877">nörbə-la-l</ta>
            <ta e="T880" id="Seg_6166" s="T879">dʼok</ta>
            <ta e="T882" id="Seg_6167" s="T881">eželʼi</ta>
            <ta e="T883" id="Seg_6168" s="T882">nörbə-zittə</ta>
            <ta e="T885" id="Seg_6169" s="T884">bile</ta>
            <ta e="T886" id="Seg_6170" s="T885">il</ta>
            <ta e="T887" id="Seg_6171" s="T886">mo-la-m</ta>
            <ta e="T889" id="Seg_6172" s="T888">mo</ta>
            <ta e="T890" id="Seg_6173" s="T889">dereʔ</ta>
            <ta e="T892" id="Seg_6174" s="T891">dʼok</ta>
            <ta e="T893" id="Seg_6175" s="T892">jakše</ta>
            <ta e="T894" id="Seg_6176" s="T893">il</ta>
            <ta e="T896" id="Seg_6177" s="T895">dʼok</ta>
            <ta e="T897" id="Seg_6178" s="T896">bile</ta>
            <ta e="T898" id="Seg_6179" s="T897">il</ta>
            <ta e="T900" id="Seg_6180" s="T899">no</ta>
            <ta e="T901" id="Seg_6181" s="T900">măn</ta>
            <ta e="T902" id="Seg_6182" s="T901">ari</ta>
            <ta e="T903" id="Seg_6183" s="T902">dʼür-lə-m</ta>
            <ta e="T904" id="Seg_6184" s="T903">tănan</ta>
            <ta e="T908" id="Seg_6185" s="T907">măn</ta>
            <ta e="T909" id="Seg_6186" s="T908">aba-m</ta>
            <ta e="T910" id="Seg_6187" s="T909">tăn</ta>
            <ta e="T911" id="Seg_6188" s="T910">aba-l</ta>
            <ta e="T912" id="Seg_6189" s="T911">dʼabro-la-ʔi</ta>
            <ta e="T914" id="Seg_6190" s="T913">mo</ta>
            <ta e="T915" id="Seg_6191" s="T914">dĭ</ta>
            <ta e="T916" id="Seg_6192" s="T915">dʼabro-la-ʔi</ta>
            <ta e="T918" id="Seg_6193" s="T917">dak</ta>
            <ta e="T919" id="Seg_6194" s="T918">dĭ</ta>
            <ta e="T920" id="Seg_6195" s="T919">măl-la-t</ta>
            <ta e="T921" id="Seg_6196" s="T920">tăn</ta>
            <ta e="T922" id="Seg_6197" s="T921">aba-l</ta>
            <ta e="T924" id="Seg_6198" s="T922">măl-la-t</ta>
            <ta e="T925" id="Seg_6199" s="T924">măn</ta>
            <ta e="T926" id="Seg_6200" s="T925">nʼi-m</ta>
            <ta e="T927" id="Seg_6201" s="T926">jakše</ta>
            <ta e="T928" id="Seg_6202" s="T927">kuvas</ta>
            <ta e="T930" id="Seg_6203" s="T929">a</ta>
            <ta e="T931" id="Seg_6204" s="T930">tăn</ta>
            <ta e="T932" id="Seg_6205" s="T931">koʔbdo</ta>
            <ta e="T933" id="Seg_6206" s="T932">ej</ta>
            <ta e="T934" id="Seg_6207" s="T933">jakše</ta>
            <ta e="T936" id="Seg_6208" s="T935">a</ta>
            <ta e="T937" id="Seg_6209" s="T936">măn</ta>
            <ta e="T938" id="Seg_6210" s="T937">aba-m</ta>
            <ta e="T940" id="Seg_6211" s="T938">măl-la-t</ta>
            <ta e="T941" id="Seg_6212" s="T940">Eh</ta>
            <ta e="T942" id="Seg_6213" s="T941">măn</ta>
            <ta e="T943" id="Seg_6214" s="T942">koʔbdo-m</ta>
            <ta e="T944" id="Seg_6215" s="T943">jakše</ta>
            <ta e="T946" id="Seg_6216" s="T945">i</ta>
            <ta e="T947" id="Seg_6217" s="T946">kuvas</ta>
            <ta e="T949" id="Seg_6218" s="T948">i</ta>
            <ta e="T950" id="Seg_6219" s="T949">bar</ta>
            <ta e="T951" id="Seg_6220" s="T950">ĭmbi</ta>
            <ta e="T952" id="Seg_6221" s="T951">bar</ta>
            <ta e="T953" id="Seg_6222" s="T952">ĭmbi</ta>
            <ta e="T954" id="Seg_6223" s="T953">măl-la-t</ta>
            <ta e="T956" id="Seg_6224" s="T955">i</ta>
            <ta e="T957" id="Seg_6225" s="T956">tăn</ta>
            <ta e="T958" id="Seg_6226" s="T957">nʼi-l</ta>
            <ta e="T959" id="Seg_6227" s="T958">ej</ta>
            <ta e="T960" id="Seg_6228" s="T959">jakše</ta>
            <ta e="T962" id="Seg_6229" s="T961">ĭmbi=də</ta>
            <ta e="T963" id="Seg_6230" s="T962">ej</ta>
            <ta e="T964" id="Seg_6231" s="T963">nörbə-la-t</ta>
            <ta e="T965" id="Seg_6232" s="T964">măna</ta>
            <ta e="T967" id="Seg_6233" s="T966">a</ta>
            <ta e="T968" id="Seg_6234" s="T967">măn</ta>
            <ta e="T969" id="Seg_6235" s="T968">koʔbdo-m</ta>
            <ta e="T970" id="Seg_6236" s="T969">bar</ta>
            <ta e="T971" id="Seg_6237" s="T970">ĭmbi</ta>
            <ta e="T972" id="Seg_6238" s="T971">bar</ta>
            <ta e="T974" id="Seg_6239" s="T973">tănan</ta>
            <ta e="T975" id="Seg_6240" s="T974">üge</ta>
            <ta e="T976" id="Seg_6241" s="T975">nörbə-lap-lie-t</ta>
            <ta e="T978" id="Seg_6242" s="T977">vot</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_6243" s="T1">jakšə</ta>
            <ta e="T3" id="Seg_6244" s="T2">kuza</ta>
            <ta e="T4" id="Seg_6245" s="T3">mo-bi</ta>
            <ta e="T5" id="Seg_6246" s="T4">kü-laːm-bi</ta>
            <ta e="T7" id="Seg_6247" s="T6">jakšə</ta>
            <ta e="T8" id="Seg_6248" s="T7">kü-laːm-bi</ta>
            <ta e="T10" id="Seg_6249" s="T9">a</ta>
            <ta e="T11" id="Seg_6250" s="T10">dĭ</ta>
            <ta e="T12" id="Seg_6251" s="T11">bile</ta>
            <ta e="T13" id="Seg_6252" s="T12">kuza</ta>
            <ta e="T14" id="Seg_6253" s="T13">oʔb</ta>
            <ta e="T17" id="Seg_6254" s="T16">kü-laːm-bi-t</ta>
            <ta e="T19" id="Seg_6255" s="T18">a</ta>
            <ta e="T20" id="Seg_6256" s="T19">jakšə</ta>
            <ta e="T21" id="Seg_6257" s="T20">kuza</ta>
            <ta e="T22" id="Seg_6258" s="T21">jakšə</ta>
            <ta e="T23" id="Seg_6259" s="T22">kü-laːm-bi</ta>
            <ta e="T25" id="Seg_6260" s="T24">vot</ta>
            <ta e="T27" id="Seg_6261" s="T26">măn</ta>
            <ta e="T28" id="Seg_6262" s="T27">aba-m</ta>
            <ta e="T29" id="Seg_6263" s="T28">jakšə</ta>
            <ta e="T30" id="Seg_6264" s="T29">amno-bi</ta>
            <ta e="T32" id="Seg_6265" s="T31">dĭgəttə</ta>
            <ta e="T33" id="Seg_6266" s="T32">bile</ta>
            <ta e="T35" id="Seg_6267" s="T34">tăŋ</ta>
            <ta e="T36" id="Seg_6268" s="T35">bile</ta>
            <ta e="T37" id="Seg_6269" s="T36">amno-bi</ta>
            <ta e="T39" id="Seg_6270" s="T38">dĭgəttə</ta>
            <ta e="T42" id="Seg_6271" s="T41">bar</ta>
            <ta e="T43" id="Seg_6272" s="T42">amno-bi</ta>
            <ta e="T44" id="Seg_6273" s="T43">jakšə</ta>
            <ta e="T46" id="Seg_6274" s="T45">dĭgəttə</ta>
            <ta e="T47" id="Seg_6275" s="T46">kü-laːm-bi</ta>
            <ta e="T49" id="Seg_6276" s="T48">nu</ta>
            <ta e="T52" id="Seg_6277" s="T51">măn</ta>
            <ta e="T53" id="Seg_6278" s="T52">amnə-bi-m</ta>
            <ta e="T54" id="Seg_6279" s="T53">tăŋ</ta>
            <ta e="T55" id="Seg_6280" s="T54">bile</ta>
            <ta e="T57" id="Seg_6281" s="T56">tăŋ</ta>
            <ta e="T58" id="Seg_6282" s="T57">bile</ta>
            <ta e="T59" id="Seg_6283" s="T58">amnə-bi-m</ta>
            <ta e="T61" id="Seg_6284" s="T60">măn</ta>
            <ta e="T62" id="Seg_6285" s="T61">ipek-m</ta>
            <ta e="T63" id="Seg_6286" s="T62">naga</ta>
            <ta e="T65" id="Seg_6287" s="T64">naga</ta>
            <ta e="T66" id="Seg_6288" s="T65">ipek</ta>
            <ta e="T67" id="Seg_6289" s="T66">măn</ta>
            <ta e="T68" id="Seg_6290" s="T67">i</ta>
            <ta e="T69" id="Seg_6291" s="T68">măn</ta>
            <ta e="T70" id="Seg_6292" s="T69">bos-də</ta>
            <ta e="T71" id="Seg_6293" s="T70">ĭmbi-ĭmbi</ta>
            <ta e="T72" id="Seg_6294" s="T71">amor-bi-m</ta>
            <ta e="T74" id="Seg_6295" s="T73">dĭgəttə=š</ta>
            <ta e="T75" id="Seg_6296" s="T74">jakšə</ta>
            <ta e="T76" id="Seg_6297" s="T75">amnə-bi-m</ta>
            <ta e="T78" id="Seg_6298" s="T77">bar</ta>
            <ta e="T81" id="Seg_6299" s="T80">dĭgəttə-š</ta>
            <ta e="T82" id="Seg_6300" s="T81">ari</ta>
            <ta e="T83" id="Seg_6301" s="T82">tʼür-bi-m</ta>
            <ta e="T85" id="Seg_6302" s="T84">amnə-bi-m</ta>
            <ta e="T86" id="Seg_6303" s="T85">bile-bile-bile-bile</ta>
            <ta e="T87" id="Seg_6304" s="T86">koʔbdo-m</ta>
            <ta e="T88" id="Seg_6305" s="T87">mĭ-lei</ta>
            <ta e="T89" id="Seg_6306" s="T88">mĭ-lei</ta>
            <ta e="T90" id="Seg_6307" s="T89">mĭ-lei-bi-m</ta>
            <ta e="T92" id="Seg_6308" s="T91">i</ta>
            <ta e="T93" id="Seg_6309" s="T92">koʔbdo-ziʔ</ta>
            <ta e="T94" id="Seg_6310" s="T93">tăŋ</ta>
            <ta e="T95" id="Seg_6311" s="T94">bile</ta>
            <ta e="T96" id="Seg_6312" s="T95">amnə-bi-m</ta>
            <ta e="T98" id="Seg_6313" s="T97">tʼor-labaʔ-m</ta>
            <ta e="T100" id="Seg_6314" s="T99">i</ta>
            <ta e="T102" id="Seg_6315" s="T101">an-lV-m</ta>
            <ta e="T103" id="Seg_6316" s="T102">i</ta>
            <ta e="T104" id="Seg_6317" s="T103">šo</ta>
            <ta e="T105" id="Seg_6318" s="T104">šo-labaʔ-m</ta>
            <ta e="T107" id="Seg_6319" s="T106">amno-zittə</ta>
            <ta e="T108" id="Seg_6320" s="T107">tăŋ</ta>
            <ta e="T109" id="Seg_6321" s="T108">bile</ta>
            <ta e="T111" id="Seg_6322" s="T110">gibər</ta>
            <ta e="T112" id="Seg_6323" s="T111">măna</ta>
            <ta e="T113" id="Seg_6324" s="T112">an-zittə</ta>
            <ta e="T115" id="Seg_6325" s="T114">gibər=də</ta>
            <ta e="T116" id="Seg_6326" s="T115">ej</ta>
            <ta e="T117" id="Seg_6327" s="T116">an-lV-m</ta>
            <ta e="T118" id="Seg_6328" s="T117">lutʼše</ta>
            <ta e="T119" id="Seg_6329" s="T118">kü-laːm-bi-m</ta>
            <ta e="T123" id="Seg_6330" s="T122">bar</ta>
            <ta e="T124" id="Seg_6331" s="T123">il</ta>
            <ta e="T125" id="Seg_6332" s="T124">jakšə</ta>
            <ta e="T126" id="Seg_6333" s="T125">amno-liA-jəʔ</ta>
            <ta e="T128" id="Seg_6334" s="T127">a</ta>
            <ta e="T129" id="Seg_6335" s="T128">dĭgəttə</ta>
            <ta e="T130" id="Seg_6336" s="T129">ari</ta>
            <ta e="T131" id="Seg_6337" s="T130">tʼür-gA</ta>
            <ta e="T133" id="Seg_6338" s="T132">gibər=də</ta>
            <ta e="T135" id="Seg_6339" s="T134">a</ta>
            <ta e="T136" id="Seg_6340" s="T135">dĭgəttə</ta>
            <ta e="T137" id="Seg_6341" s="T136">moʔ</ta>
            <ta e="T138" id="Seg_6342" s="T137">döbər</ta>
            <ta e="T139" id="Seg_6343" s="T138">že</ta>
            <ta e="T140" id="Seg_6344" s="T139">šo-lV-j</ta>
            <ta e="T142" id="Seg_6345" s="T141">o</ta>
            <ta e="T143" id="Seg_6346" s="T142">moʔ</ta>
            <ta e="T144" id="Seg_6347" s="T143">dărəʔ</ta>
            <ta e="T145" id="Seg_6348" s="T144">mo-l-lV-l</ta>
            <ta e="T147" id="Seg_6349" s="T146">i</ta>
            <ta e="T148" id="Seg_6350" s="T147">ĭmbi</ta>
            <ta e="T150" id="Seg_6351" s="T149">dărəʔ</ta>
            <ta e="T152" id="Seg_6352" s="T151">šo-lV-j</ta>
            <ta e="T153" id="Seg_6353" s="T152">döbər</ta>
            <ta e="T155" id="Seg_6354" s="T154">a</ta>
            <ta e="T156" id="Seg_6355" s="T155">döbər</ta>
            <ta e="T157" id="Seg_6356" s="T156">šo-lV-j</ta>
            <ta e="T159" id="Seg_6357" s="T158">a</ta>
            <ta e="T160" id="Seg_6358" s="T159">tăn</ta>
            <ta e="T161" id="Seg_6359" s="T160">ĭmbi</ta>
            <ta e="T162" id="Seg_6360" s="T161">ej</ta>
            <ta e="T163" id="Seg_6361" s="T162">an-lV-l</ta>
            <ta e="T165" id="Seg_6362" s="T164">dʼok</ta>
            <ta e="T167" id="Seg_6363" s="T166">măn</ta>
            <ta e="T168" id="Seg_6364" s="T167">ej</ta>
            <ta e="T169" id="Seg_6365" s="T168">an-lV-m</ta>
            <ta e="T171" id="Seg_6366" s="T170">măn</ta>
            <ta e="T172" id="Seg_6367" s="T171">dö-Kən</ta>
            <ta e="T173" id="Seg_6368" s="T172">mo-lV-m</ta>
            <ta e="T175" id="Seg_6369" s="T174">vot</ta>
            <ta e="T179" id="Seg_6370" s="T178">ĭmbi=nʼibudʼ</ta>
            <ta e="T180" id="Seg_6371" s="T179">mĭnzər-zittə</ta>
            <ta e="T181" id="Seg_6372" s="T180">nadə</ta>
            <ta e="T183" id="Seg_6373" s="T182">măn</ta>
            <ta e="T184" id="Seg_6374" s="T183">amor-zittə</ta>
            <ta e="T185" id="Seg_6375" s="T184">mo-lV-m</ta>
            <ta e="T191" id="Seg_6376" s="T190">ĭmbi</ta>
            <ta e="T192" id="Seg_6377" s="T191">mĭnzər-liA-l</ta>
            <ta e="T194" id="Seg_6378" s="T193">uja</ta>
            <ta e="T196" id="Seg_6379" s="T195">moʔ</ta>
            <ta e="T197" id="Seg_6380" s="T196">uja</ta>
            <ta e="T199" id="Seg_6381" s="T198">măn</ta>
            <ta e="T200" id="Seg_6382" s="T199">bɨ</ta>
            <ta e="T201" id="Seg_6383" s="T200">jeja-m</ta>
            <ta e="T202" id="Seg_6384" s="T201">am-zit</ta>
            <ta e="T204" id="Seg_6385" s="T203">a</ta>
            <ta e="T205" id="Seg_6386" s="T204">măn</ta>
            <ta e="T206" id="Seg_6387" s="T205">bɨ</ta>
            <ta e="T207" id="Seg_6388" s="T206">süt</ta>
            <ta e="T208" id="Seg_6389" s="T207">bɨ</ta>
            <ta e="T210" id="Seg_6390" s="T209">vot</ta>
            <ta e="T211" id="Seg_6391" s="T210">jakšə</ta>
            <ta e="T212" id="Seg_6392" s="T211">bɨ</ta>
            <ta e="T213" id="Seg_6393" s="T212">amor-lV-m</ta>
            <ta e="T215" id="Seg_6394" s="T214">moʔ</ta>
            <ta e="T216" id="Seg_6395" s="T215">süt</ta>
            <ta e="T218" id="Seg_6396" s="T217">süt</ta>
            <ta e="T219" id="Seg_6397" s="T218">tak</ta>
            <ta e="T220" id="Seg_6398" s="T219">süt</ta>
            <ta e="T222" id="Seg_6399" s="T221">a</ta>
            <ta e="T223" id="Seg_6400" s="T222">uja</ta>
            <ta e="T224" id="Seg_6401" s="T223">jakšə</ta>
            <ta e="T228" id="Seg_6402" s="T227">moʔ</ta>
            <ta e="T229" id="Seg_6403" s="T228">măna</ta>
            <ta e="T230" id="Seg_6404" s="T229">ej</ta>
            <ta e="T231" id="Seg_6405" s="T230">šo-lV-j</ta>
            <ta e="T233" id="Seg_6406" s="T232">a</ta>
            <ta e="T234" id="Seg_6407" s="T233">măn</ta>
            <ta e="T235" id="Seg_6408" s="T234">ija-m</ta>
            <ta e="T236" id="Seg_6409" s="T235">bile</ta>
            <ta e="T239" id="Seg_6410" s="T238">tănan</ta>
            <ta e="T240" id="Seg_6411" s="T239">ej</ta>
            <ta e="T241" id="Seg_6412" s="T240">öʔ-liA-t</ta>
            <ta e="T243" id="Seg_6413" s="T242">moʔ</ta>
            <ta e="T244" id="Seg_6414" s="T243">dărəʔ</ta>
            <ta e="T245" id="Seg_6415" s="T244">ej</ta>
            <ta e="T247" id="Seg_6416" s="T246">nu</ta>
            <ta e="T248" id="Seg_6417" s="T247">măn-labaʔ-t</ta>
            <ta e="T249" id="Seg_6418" s="T248">što</ta>
            <ta e="T250" id="Seg_6419" s="T249">bile</ta>
            <ta e="T251" id="Seg_6420" s="T250">kuza-t</ta>
            <ta e="T252" id="Seg_6421" s="T251">tăn</ta>
            <ta e="T254" id="Seg_6422" s="T253">moʔ</ta>
            <ta e="T255" id="Seg_6423" s="T254">dărəʔ</ta>
            <ta e="T256" id="Seg_6424" s="T255">bile</ta>
            <ta e="T257" id="Seg_6425" s="T256">kuza</ta>
            <ta e="T259" id="Seg_6426" s="T258">măn</ta>
            <ta e="T260" id="Seg_6427" s="T259">jakšə</ta>
            <ta e="T261" id="Seg_6428" s="T260">kuza</ta>
            <ta e="T263" id="Seg_6429" s="T262">ĭmbi=nʼibudʼ</ta>
            <ta e="T264" id="Seg_6430" s="T263">tănan</ta>
            <ta e="T265" id="Seg_6431" s="T264">bar</ta>
            <ta e="T266" id="Seg_6432" s="T265">bɨ</ta>
            <ta e="T267" id="Seg_6433" s="T266">nörbə-lV-m</ta>
            <ta e="T268" id="Seg_6434" s="T267">nörbə-lV-m</ta>
            <ta e="T269" id="Seg_6435" s="T268">bɨ</ta>
            <ta e="T272" id="Seg_6436" s="T271">tăŋ</ta>
            <ta e="T273" id="Seg_6437" s="T272">jakšə</ta>
            <ta e="T274" id="Seg_6438" s="T273">mo-lV-j</ta>
            <ta e="T276" id="Seg_6439" s="T275">oh</ta>
            <ta e="T277" id="Seg_6440" s="T276">nu</ta>
            <ta e="T278" id="Seg_6441" s="T277">dărəʔ</ta>
            <ta e="T279" id="Seg_6442" s="T278">măn-labaʔ-t</ta>
            <ta e="T280" id="Seg_6443" s="T279">măna</ta>
            <ta e="T282" id="Seg_6444" s="T281">măn</ta>
            <ta e="T283" id="Seg_6445" s="T282">dĭ-m</ta>
            <ta e="T284" id="Seg_6446" s="T283">bar</ta>
            <ta e="T285" id="Seg_6447" s="T284">münör-lV-m</ta>
            <ta e="T286" id="Seg_6448" s="T285">tăŋ</ta>
            <ta e="T288" id="Seg_6449" s="T287">dʼok</ta>
            <ta e="T289" id="Seg_6450" s="T288">münör-zittə</ta>
            <ta e="T293" id="Seg_6451" s="T292">ato</ta>
            <ta e="T294" id="Seg_6452" s="T293">dĭ</ta>
            <ta e="T295" id="Seg_6453" s="T294">măna</ta>
            <ta e="T296" id="Seg_6454" s="T295">münör-lV-l</ta>
            <ta e="T297" id="Seg_6455" s="T296">dĭgəttə-š</ta>
            <ta e="T301" id="Seg_6456" s="T300">măn</ta>
            <ta e="T302" id="Seg_6457" s="T301">tăŋ</ta>
            <ta e="T303" id="Seg_6458" s="T302">ara</ta>
            <ta e="T304" id="Seg_6459" s="T303">bĭs-liA-m</ta>
            <ta e="T306" id="Seg_6460" s="T305">o</ta>
            <ta e="T307" id="Seg_6461" s="T306">moʔ</ta>
            <ta e="T308" id="Seg_6462" s="T307">dărəʔ</ta>
            <ta e="T309" id="Seg_6463" s="T308">tăn</ta>
            <ta e="T311" id="Seg_6464" s="T310">nu</ta>
            <ta e="T312" id="Seg_6465" s="T311">măn</ta>
            <ta e="T313" id="Seg_6466" s="T312">bile</ta>
            <ta e="T314" id="Seg_6467" s="T313">kuza</ta>
            <ta e="T316" id="Seg_6468" s="T315">bar</ta>
            <ta e="T317" id="Seg_6469" s="T316">gĭda</ta>
            <ta e="T318" id="Seg_6470" s="T317">bar</ta>
            <ta e="T319" id="Seg_6471" s="T318">i-lV-m</ta>
            <ta e="T320" id="Seg_6472" s="T319">ĭmbi-də</ta>
            <ta e="T322" id="Seg_6473" s="T321">a</ta>
            <ta e="T323" id="Seg_6474" s="T322">moʔ</ta>
            <ta e="T324" id="Seg_6475" s="T323">tănan</ta>
            <ta e="T325" id="Seg_6476" s="T324">i-lV-l</ta>
            <ta e="T326" id="Seg_6477" s="T325">tak</ta>
            <ta e="T327" id="Seg_6478" s="T326">măn</ta>
            <ta e="T328" id="Seg_6479" s="T327">dĭgəttə=š</ta>
            <ta e="T330" id="Seg_6480" s="T329">măn</ta>
            <ta e="T331" id="Seg_6481" s="T330">dĭgəttə-š</ta>
            <ta e="T332" id="Seg_6482" s="T331">i-lV-m</ta>
            <ta e="T334" id="Seg_6483" s="T333">a</ta>
            <ta e="T335" id="Seg_6484" s="T334">moʔ</ta>
            <ta e="T336" id="Seg_6485" s="T335">dĭn</ta>
            <ta e="T337" id="Seg_6486" s="T336">dărəʔ</ta>
            <ta e="T338" id="Seg_6487" s="T337">i-lV-l</ta>
            <ta e="T340" id="Seg_6488" s="T339">tak</ta>
            <ta e="T341" id="Seg_6489" s="T340">vot</ta>
            <ta e="T342" id="Seg_6490" s="T341">i-lV-m</ta>
            <ta e="T343" id="Seg_6491" s="T342">măn</ta>
            <ta e="T345" id="Seg_6492" s="T344">măna</ta>
            <ta e="T346" id="Seg_6493" s="T345">dĭgəttə-š</ta>
            <ta e="T347" id="Seg_6494" s="T346">ej</ta>
            <ta e="T348" id="Seg_6495" s="T347">kuvas</ta>
            <ta e="T349" id="Seg_6496" s="T348">tura-Tə</ta>
            <ta e="T352" id="Seg_6497" s="T351">măn</ta>
            <ta e="T353" id="Seg_6498" s="T352">dĭgən</ta>
            <ta e="T354" id="Seg_6499" s="T353">amno-lV-m</ta>
            <ta e="T356" id="Seg_6500" s="T355">a</ta>
            <ta e="T357" id="Seg_6501" s="T356">dĭgəttə</ta>
            <ta e="T358" id="Seg_6502" s="T357">kak</ta>
            <ta e="T359" id="Seg_6503" s="T358">šo-lV-m</ta>
            <ta e="T360" id="Seg_6504" s="T359">măna</ta>
            <ta e="T361" id="Seg_6505" s="T360">il</ta>
            <ta e="T363" id="Seg_6506" s="T362">šində-də</ta>
            <ta e="T364" id="Seg_6507" s="T363">ej</ta>
            <ta e="T366" id="Seg_6508" s="T365">ej</ta>
            <ta e="T373" id="Seg_6509" s="T372">ej</ta>
            <ta e="T374" id="Seg_6510" s="T373">i-liA-t</ta>
            <ta e="T376" id="Seg_6511" s="T375">bar</ta>
            <ta e="T377" id="Seg_6512" s="T376">il</ta>
            <ta e="T379" id="Seg_6513" s="T378">i</ta>
            <ta e="T380" id="Seg_6514" s="T379">ej</ta>
            <ta e="T381" id="Seg_6515" s="T380">jakšə</ta>
            <ta e="T382" id="Seg_6516" s="T381">kuza</ta>
            <ta e="T384" id="Seg_6517" s="T383">vot</ta>
            <ta e="T388" id="Seg_6518" s="T387">bile</ta>
            <ta e="T389" id="Seg_6519" s="T388">kuza</ta>
            <ta e="T390" id="Seg_6520" s="T389">jakšə</ta>
            <ta e="T391" id="Seg_6521" s="T390">kuza-n</ta>
            <ta e="T392" id="Seg_6522" s="T391">i-bi</ta>
            <ta e="T393" id="Seg_6523" s="T392">kujnek</ta>
            <ta e="T394" id="Seg_6524" s="T393">uja</ta>
            <ta e="T395" id="Seg_6525" s="T394">i</ta>
            <ta e="T396" id="Seg_6526" s="T395">bar</ta>
            <ta e="T397" id="Seg_6527" s="T396">ĭmbi</ta>
            <ta e="T398" id="Seg_6528" s="T397">bar</ta>
            <ta e="T400" id="Seg_6529" s="T399">aktʼa-t</ta>
            <ta e="T401" id="Seg_6530" s="T400">mo-bi</ta>
            <ta e="T402" id="Seg_6531" s="T401">dĭ</ta>
            <ta e="T403" id="Seg_6532" s="T402">kuza-n</ta>
            <ta e="T405" id="Seg_6533" s="T404">bar</ta>
            <ta e="T406" id="Seg_6534" s="T405">i-bi</ta>
            <ta e="T408" id="Seg_6535" s="T407">ĭmbi</ta>
            <ta e="T409" id="Seg_6536" s="T408">tăn</ta>
            <ta e="T410" id="Seg_6537" s="T409">i-bi-l</ta>
            <ta e="T412" id="Seg_6538" s="T411">tak</ta>
            <ta e="T413" id="Seg_6539" s="T412">măn</ta>
            <ta e="T414" id="Seg_6540" s="T413">naga</ta>
            <ta e="T415" id="Seg_6541" s="T414">a</ta>
            <ta e="T416" id="Seg_6542" s="T415">tăn</ta>
            <ta e="T417" id="Seg_6543" s="T416">i-gA</ta>
            <ta e="T420" id="Seg_6544" s="T419">măn</ta>
            <ta e="T421" id="Seg_6545" s="T420">i-bi-m</ta>
            <ta e="T423" id="Seg_6546" s="T422">a</ta>
            <ta e="T424" id="Seg_6547" s="T423">măn</ta>
            <ta e="T425" id="Seg_6548" s="T424">tănan</ta>
            <ta e="T426" id="Seg_6549" s="T425">ej</ta>
            <ta e="T427" id="Seg_6550" s="T426">i-lV-m</ta>
            <ta e="T429" id="Seg_6551" s="T428">măn</ta>
            <ta e="T430" id="Seg_6552" s="T429">tănan</ta>
            <ta e="T431" id="Seg_6553" s="T430">i-lV-m</ta>
            <ta e="T433" id="Seg_6554" s="T432">a</ta>
            <ta e="T434" id="Seg_6555" s="T433">moʔ</ta>
            <ta e="T435" id="Seg_6556" s="T434">nĭn</ta>
            <ta e="T436" id="Seg_6557" s="T435">măna</ta>
            <ta e="T437" id="Seg_6558" s="T436">i-lV-l</ta>
            <ta e="T439" id="Seg_6559" s="T438">tak</ta>
            <ta e="T440" id="Seg_6560" s="T439">nĭn</ta>
            <ta e="T441" id="Seg_6561" s="T440">našto</ta>
            <ta e="T442" id="Seg_6562" s="T441">i-bi-l</ta>
            <ta e="T444" id="Seg_6563" s="T443">măn</ta>
            <ta e="T445" id="Seg_6564" s="T444">kun-laʔbə</ta>
            <ta e="T446" id="Seg_6565" s="T445">i</ta>
            <ta e="T447" id="Seg_6566" s="T446">bar</ta>
            <ta e="T448" id="Seg_6567" s="T447">ĭmbi-m</ta>
            <ta e="T449" id="Seg_6568" s="T448">bar</ta>
            <ta e="T451" id="Seg_6569" s="T450">a</ta>
            <ta e="T452" id="Seg_6570" s="T451">măn</ta>
            <ta e="T453" id="Seg_6571" s="T452">bile</ta>
            <ta e="T454" id="Seg_6572" s="T453">tura-Tə</ta>
            <ta e="T455" id="Seg_6573" s="T454">tănan</ta>
            <ta e="T456" id="Seg_6574" s="T455">amno-laʔbə</ta>
            <ta e="T459" id="Seg_6575" s="T458">vot</ta>
            <ta e="T463" id="Seg_6576" s="T462">amnə-ʔ</ta>
            <ta e="T464" id="Seg_6577" s="T463">măn-ziʔ</ta>
            <ta e="T465" id="Seg_6578" s="T464">amor-zittə</ta>
            <ta e="T467" id="Seg_6579" s="T466">oi</ta>
            <ta e="T479" id="Seg_6580" s="T478">amnə-ʔ</ta>
            <ta e="T480" id="Seg_6581" s="T479">măn-ziʔ</ta>
            <ta e="T481" id="Seg_6582" s="T480">amor-zittə</ta>
            <ta e="T483" id="Seg_6583" s="T482">da</ta>
            <ta e="T484" id="Seg_6584" s="T483">ĭmbi</ta>
            <ta e="T485" id="Seg_6585" s="T484">tăn</ta>
            <ta e="T486" id="Seg_6586" s="T485">mo-lV-t</ta>
            <ta e="T488" id="Seg_6587" s="T487">da</ta>
            <ta e="T489" id="Seg_6588" s="T488">măn</ta>
            <ta e="T490" id="Seg_6589" s="T489">mo-lV-t</ta>
            <ta e="T491" id="Seg_6590" s="T490">ipek</ta>
            <ta e="T492" id="Seg_6591" s="T491">uja</ta>
            <ta e="T493" id="Seg_6592" s="T492">i</ta>
            <ta e="T494" id="Seg_6593" s="T493">süt</ta>
            <ta e="T496" id="Seg_6594" s="T495">sĭreʔp-ne</ta>
            <ta e="T498" id="Seg_6595" s="T497">am-t</ta>
            <ta e="T500" id="Seg_6596" s="T499">tak</ta>
            <ta e="T501" id="Seg_6597" s="T500">măn</ta>
            <ta e="T502" id="Seg_6598" s="T501">bɨ</ta>
            <ta e="T503" id="Seg_6599" s="T502">kajaʔ</ta>
            <ta e="T504" id="Seg_6600" s="T503">amor-liA-m</ta>
            <ta e="T505" id="Seg_6601" s="T504">bɨ</ta>
            <ta e="T507" id="Seg_6602" s="T506">nu</ta>
            <ta e="T508" id="Seg_6603" s="T507">i</ta>
            <ta e="T509" id="Seg_6604" s="T508">kajaʔ</ta>
            <ta e="T510" id="Seg_6605" s="T509">măn</ta>
            <ta e="T511" id="Seg_6606" s="T510">mĭ-lV-m</ta>
            <ta e="T512" id="Seg_6607" s="T511">tănan</ta>
            <ta e="T513" id="Seg_6608" s="T512">amor-zittə</ta>
            <ta e="T515" id="Seg_6609" s="T514">jakšə</ta>
            <ta e="T516" id="Seg_6610" s="T515">tolʼko</ta>
            <ta e="T517" id="Seg_6611" s="T516">amor-ə-ʔ</ta>
            <ta e="T519" id="Seg_6612" s="T518">dö</ta>
            <ta e="T520" id="Seg_6613" s="T519">tak</ta>
            <ta e="T521" id="Seg_6614" s="T520">măn</ta>
            <ta e="T522" id="Seg_6615" s="T521">amor-lV-m</ta>
            <ta e="T524" id="Seg_6616" s="T523">tăŋ</ta>
            <ta e="T525" id="Seg_6617" s="T524">tăŋ</ta>
            <ta e="T526" id="Seg_6618" s="T525">amor-lV-m</ta>
            <ta e="T528" id="Seg_6619" s="T527">jakšə</ta>
            <ta e="T529" id="Seg_6620" s="T528">amor-liA-m</ta>
            <ta e="T535" id="Seg_6621" s="T534">moʔ</ta>
            <ta e="T536" id="Seg_6622" s="T535">tăn</ta>
            <ta e="T537" id="Seg_6623" s="T536">măn</ta>
            <ta e="T538" id="Seg_6624" s="T537">eʔbdə-m</ta>
            <ta e="T539" id="Seg_6625" s="T538">bar</ta>
            <ta e="T540" id="Seg_6626" s="T539">bos</ta>
            <ta e="T541" id="Seg_6627" s="T540">uda-Tə</ta>
            <ta e="T542" id="Seg_6628" s="T541">i-bi-l</ta>
            <ta e="T544" id="Seg_6629" s="T543">vedʼ</ta>
            <ta e="T545" id="Seg_6630" s="T544">tăn</ta>
            <ta e="T546" id="Seg_6631" s="T545">uda-l</ta>
            <ta e="T547" id="Seg_6632" s="T546">urgo</ta>
            <ta e="T548" id="Seg_6633" s="T547">a</ta>
            <ta e="T549" id="Seg_6634" s="T548">măn</ta>
            <ta e="T550" id="Seg_6635" s="T549">idʼiʔeʔe</ta>
            <ta e="T552" id="Seg_6636" s="T551">moʔ</ta>
            <ta e="T553" id="Seg_6637" s="T552">dărəʔ</ta>
            <ta e="T554" id="Seg_6638" s="T553">mo-lV-l</ta>
            <ta e="T556" id="Seg_6639" s="T555">bile</ta>
            <ta e="T558" id="Seg_6640" s="T557">ej</ta>
            <ta e="T559" id="Seg_6641" s="T558">jakšə</ta>
            <ta e="T560" id="Seg_6642" s="T559">dărəʔ</ta>
            <ta e="T562" id="Seg_6643" s="T561">vot</ta>
            <ta e="T566" id="Seg_6644" s="T565">măn</ta>
            <ta e="T567" id="Seg_6645" s="T566">tăŋ</ta>
            <ta e="T568" id="Seg_6646" s="T567">ĭzem-luʔbdə-bi-m</ta>
            <ta e="T570" id="Seg_6647" s="T569">uda-zAŋ-m</ta>
            <ta e="T571" id="Seg_6648" s="T570">ĭzem-liA-t</ta>
            <ta e="T573" id="Seg_6649" s="T572">i</ta>
            <ta e="T574" id="Seg_6650" s="T573">üjü-bə</ta>
            <ta e="T575" id="Seg_6651" s="T574">bar</ta>
            <ta e="T576" id="Seg_6652" s="T575">ĭzem-laʔbə-liA-t</ta>
            <ta e="T578" id="Seg_6653" s="T577">adnaka</ta>
            <ta e="T579" id="Seg_6654" s="T578">măn</ta>
            <ta e="T580" id="Seg_6655" s="T579">kü-labaʔ-m-bil</ta>
            <ta e="T582" id="Seg_6656" s="T581">dʼok</ta>
            <ta e="T583" id="Seg_6657" s="T582">nʼe</ta>
            <ta e="T584" id="Seg_6658" s="T583">nadə</ta>
            <ta e="T585" id="Seg_6659" s="T584">kü-labaʔ-m-zittə</ta>
            <ta e="T587" id="Seg_6660" s="T586">nadə</ta>
            <ta e="T588" id="Seg_6661" s="T587">amno-zittə</ta>
            <ta e="T590" id="Seg_6662" s="T589">amno-zittə</ta>
            <ta e="T591" id="Seg_6663" s="T590">jakšə</ta>
            <ta e="T593" id="Seg_6664" s="T592">a</ta>
            <ta e="T594" id="Seg_6665" s="T593">kü-labaʔ-m-zittə</ta>
            <ta e="T595" id="Seg_6666" s="T594">bile</ta>
            <ta e="T597" id="Seg_6667" s="T596">tăŋ</ta>
            <ta e="T598" id="Seg_6668" s="T597">bile</ta>
            <ta e="T600" id="Seg_6669" s="T599">vot</ta>
            <ta e="T604" id="Seg_6670" s="T603">šide</ta>
            <ta e="T605" id="Seg_6671" s="T604">nʼi</ta>
            <ta e="T606" id="Seg_6672" s="T605">amno-bi-jəʔ</ta>
            <ta e="T608" id="Seg_6673" s="T607">dĭgəttə=š</ta>
            <ta e="T609" id="Seg_6674" s="T608">dĭ</ta>
            <ta e="T610" id="Seg_6675" s="T609">nʼi</ta>
            <ta e="T611" id="Seg_6676" s="T610">gibər=də</ta>
            <ta e="T612" id="Seg_6677" s="T611">ari</ta>
            <ta e="T613" id="Seg_6678" s="T612">tʼür-zittə</ta>
            <ta e="T614" id="Seg_6679" s="T613">mo-lV-t</ta>
            <ta e="T616" id="Seg_6680" s="T615">a</ta>
            <ta e="T617" id="Seg_6681" s="T616">oʔb</ta>
            <ta e="T618" id="Seg_6682" s="T617">tura-Kən-ə-ŋ</ta>
            <ta e="T619" id="Seg_6683" s="T618">amno-laʔbə-liA-t</ta>
            <ta e="T621" id="Seg_6684" s="T620">nu</ta>
            <ta e="T622" id="Seg_6685" s="T621">mĭ-lV-t</ta>
            <ta e="T623" id="Seg_6686" s="T622">tănan</ta>
            <ta e="T624" id="Seg_6687" s="T623">ine</ta>
            <ta e="T625" id="Seg_6688" s="T624">măna</ta>
            <ta e="T626" id="Seg_6689" s="T625">tüžöj</ta>
            <ta e="T628" id="Seg_6690" s="T627">dʼok</ta>
            <ta e="T629" id="Seg_6691" s="T628">tăn</ta>
            <ta e="T630" id="Seg_6692" s="T629">i-t</ta>
            <ta e="T631" id="Seg_6693" s="T630">ine</ta>
            <ta e="T632" id="Seg_6694" s="T631">a</ta>
            <ta e="T633" id="Seg_6695" s="T632">măn</ta>
            <ta e="T634" id="Seg_6696" s="T633">tüžöj</ta>
            <ta e="T636" id="Seg_6697" s="T635">o</ta>
            <ta e="T637" id="Seg_6698" s="T636">eneidəne</ta>
            <ta e="T639" id="Seg_6699" s="T638">moʔ</ta>
            <ta e="T640" id="Seg_6700" s="T639">dărəʔ</ta>
            <ta e="T642" id="Seg_6701" s="T641">nu</ta>
            <ta e="T643" id="Seg_6702" s="T642">tăn</ta>
            <ta e="T644" id="Seg_6703" s="T643">koško</ta>
            <ta e="T646" id="Seg_6704" s="T645">eneidəne-l</ta>
            <ta e="T648" id="Seg_6705" s="T647">moʔ</ta>
            <ta e="T649" id="Seg_6706" s="T648">dărəʔ</ta>
            <ta e="T650" id="Seg_6707" s="T649">kudo-nzə-liA-l</ta>
            <ta e="T652" id="Seg_6708" s="T651">tak</ta>
            <ta e="T653" id="Seg_6709" s="T652">nadə</ta>
            <ta e="T654" id="Seg_6710" s="T653">dărəʔ</ta>
            <ta e="T655" id="Seg_6711" s="T654">kudonz-zittə</ta>
            <ta e="T656" id="Seg_6712" s="T655">bile</ta>
            <ta e="T658" id="Seg_6713" s="T657">a</ta>
            <ta e="T659" id="Seg_6714" s="T658">măna</ta>
            <ta e="T660" id="Seg_6715" s="T659">vedʼ</ta>
            <ta e="T661" id="Seg_6716" s="T660">aba-m</ta>
            <ta e="T662" id="Seg_6717" s="T661">mĭ-bi</ta>
            <ta e="T663" id="Seg_6718" s="T662">ine</ta>
            <ta e="T664" id="Seg_6719" s="T663">a</ta>
            <ta e="T665" id="Seg_6720" s="T664">tănan</ta>
            <ta e="T666" id="Seg_6721" s="T665">tüžöj</ta>
            <ta e="T668" id="Seg_6722" s="T667">măn-bi</ta>
            <ta e="T669" id="Seg_6723" s="T668">tăn</ta>
            <ta e="T670" id="Seg_6724" s="T669">i-t</ta>
            <ta e="T671" id="Seg_6725" s="T670">ine</ta>
            <ta e="T672" id="Seg_6726" s="T671">a</ta>
            <ta e="T673" id="Seg_6727" s="T672">tăn</ta>
            <ta e="T674" id="Seg_6728" s="T673">i-t</ta>
            <ta e="T675" id="Seg_6729" s="T674">tüžöj</ta>
            <ta e="T677" id="Seg_6730" s="T676">a</ta>
            <ta e="T678" id="Seg_6731" s="T677">tura</ta>
            <ta e="T679" id="Seg_6732" s="T678">tănan</ta>
            <ta e="T681" id="Seg_6733" s="T680">i</ta>
            <ta e="T682" id="Seg_6734" s="T681">tura</ta>
            <ta e="T683" id="Seg_6735" s="T682">i</ta>
            <ta e="T684" id="Seg_6736" s="T683">tüžöj</ta>
            <ta e="T685" id="Seg_6737" s="T684">tănan</ta>
            <ta e="T687" id="Seg_6738" s="T686">a</ta>
            <ta e="T688" id="Seg_6739" s="T687">tüj</ta>
            <ta e="T689" id="Seg_6740" s="T688">tüžöj</ta>
            <ta e="T690" id="Seg_6741" s="T689">ej</ta>
            <ta e="T691" id="Seg_6742" s="T690">i-liA-t</ta>
            <ta e="T700" id="Seg_6743" s="T699">bar</ta>
            <ta e="T701" id="Seg_6744" s="T700">gĭda</ta>
            <ta e="T702" id="Seg_6745" s="T701">bar</ta>
            <ta e="T704" id="Seg_6746" s="T703">bar</ta>
            <ta e="T705" id="Seg_6747" s="T704">il</ta>
            <ta e="T707" id="Seg_6748" s="T706">urgo</ta>
            <ta e="T708" id="Seg_6749" s="T707">il</ta>
            <ta e="T709" id="Seg_6750" s="T708">i-bi-jəʔ</ta>
            <ta e="T711" id="Seg_6751" s="T710">măn</ta>
            <ta e="T712" id="Seg_6752" s="T711">il-m</ta>
            <ta e="T713" id="Seg_6753" s="T712">i-bi-jəʔ</ta>
            <ta e="T715" id="Seg_6754" s="T714">i</ta>
            <ta e="T716" id="Seg_6755" s="T715">gibər</ta>
            <ta e="T719" id="Seg_6756" s="T718">gibər</ta>
            <ta e="T720" id="Seg_6757" s="T719">gibər</ta>
            <ta e="T722" id="Seg_6758" s="T721">dĭ-zAŋ</ta>
            <ta e="T723" id="Seg_6759" s="T722">tʼor-labaʔ-liA-jəʔ</ta>
            <ta e="T724" id="Seg_6760" s="T723">tʼor-labaʔ-liA-jəʔ</ta>
            <ta e="T726" id="Seg_6761" s="T725">bos</ta>
            <ta e="T727" id="Seg_6762" s="T726">tura-gəʔ</ta>
            <ta e="T728" id="Seg_6763" s="T727">dĭ-zem</ta>
            <ta e="T729" id="Seg_6764" s="T728">bar</ta>
            <ta e="T730" id="Seg_6765" s="T729">i-bi-jəʔ</ta>
            <ta e="T732" id="Seg_6766" s="T731">a</ta>
            <ta e="T733" id="Seg_6767" s="T732">ĭmbi</ta>
            <ta e="T734" id="Seg_6768" s="T733">i-bi-jəʔ</ta>
            <ta e="T736" id="Seg_6769" s="T735">tak</ta>
            <ta e="T737" id="Seg_6770" s="T736">ĭmbi</ta>
            <ta e="T739" id="Seg_6771" s="T738">kudo-nzə-bi-jəʔ</ta>
            <ta e="T741" id="Seg_6772" s="T740">i</ta>
            <ta e="T742" id="Seg_6773" s="T741">bar</ta>
            <ta e="T743" id="Seg_6774" s="T742">gĭda</ta>
            <ta e="T744" id="Seg_6775" s="T743">bar</ta>
            <ta e="T745" id="Seg_6776" s="T744">măn-labaʔ-liA-jəʔ</ta>
            <ta e="T747" id="Seg_6777" s="T746">vot</ta>
            <ta e="T748" id="Seg_6778" s="T747">dĭ-zem</ta>
            <ta e="T749" id="Seg_6779" s="T748">i</ta>
            <ta e="T750" id="Seg_6780" s="T749">i-bi-jəʔ</ta>
            <ta e="T752" id="Seg_6781" s="T751">a</ta>
            <ta e="T753" id="Seg_6782" s="T752">gibər</ta>
            <ta e="T755" id="Seg_6783" s="T754">nu</ta>
            <ta e="T756" id="Seg_6784" s="T755">măn</ta>
            <ta e="T757" id="Seg_6785" s="T756">tĭmne-m</ta>
            <ta e="T758" id="Seg_6786" s="T757">gibər</ta>
            <ta e="T760" id="Seg_6787" s="T759">măn</ta>
            <ta e="T761" id="Seg_6788" s="T760">že</ta>
            <ta e="T762" id="Seg_6789" s="T761">ej</ta>
            <ta e="T763" id="Seg_6790" s="T762">tĭmne-m</ta>
            <ta e="T767" id="Seg_6791" s="T766">kan-žə-bəj</ta>
            <ta e="T768" id="Seg_6792" s="T767">beške-jəʔ</ta>
            <ta e="T769" id="Seg_6793" s="T768">i-zittə</ta>
            <ta e="T771" id="Seg_6794" s="T770">beške</ta>
            <ta e="T772" id="Seg_6795" s="T771">kuvas</ta>
            <ta e="T774" id="Seg_6796" s="T773">a</ta>
            <ta e="T775" id="Seg_6797" s="T774">ĭmbi</ta>
            <ta e="T777" id="Seg_6798" s="T776">dărəʔ</ta>
            <ta e="T779" id="Seg_6799" s="T778">o</ta>
            <ta e="T780" id="Seg_6800" s="T779">tak</ta>
            <ta e="T781" id="Seg_6801" s="T780">ĭmbi</ta>
            <ta e="T783" id="Seg_6802" s="T782">a</ta>
            <ta e="T784" id="Seg_6803" s="T783">măn</ta>
            <ta e="T785" id="Seg_6804" s="T784">dĭgəttə</ta>
            <ta e="T786" id="Seg_6805" s="T785">mĭnzər-lV-m</ta>
            <ta e="T787" id="Seg_6806" s="T786">amor-zittə</ta>
            <ta e="T788" id="Seg_6807" s="T787">mo-lV-m</ta>
            <ta e="T790" id="Seg_6808" s="T789">i</ta>
            <ta e="T791" id="Seg_6809" s="T790">tăn</ta>
            <ta e="T792" id="Seg_6810" s="T791">amor-lV-l</ta>
            <ta e="T794" id="Seg_6811" s="T793">jakšə</ta>
            <ta e="T795" id="Seg_6812" s="T794">dĭ</ta>
            <ta e="T801" id="Seg_6813" s="T800">măn</ta>
            <ta e="T802" id="Seg_6814" s="T801">dĭ-Tə</ta>
            <ta e="T803" id="Seg_6815" s="T802">mĭ-lV-m</ta>
            <ta e="T804" id="Seg_6816" s="T803">von</ta>
            <ta e="T805" id="Seg_6817" s="T804">gĭda-jəʔ</ta>
            <ta e="T807" id="Seg_6818" s="T806">a</ta>
            <ta e="T808" id="Seg_6819" s="T807">dĭ</ta>
            <ta e="T809" id="Seg_6820" s="T808">măna</ta>
            <ta e="T810" id="Seg_6821" s="T809">idʼiʔeʔe</ta>
            <ta e="T811" id="Seg_6822" s="T810">idʼiʔeʔe</ta>
            <ta e="T812" id="Seg_6823" s="T811">mĭ-liA-t</ta>
            <ta e="T816" id="Seg_6824" s="T815">nu</ta>
            <ta e="T817" id="Seg_6825" s="T816">kan-žə-bəj</ta>
            <ta e="T818" id="Seg_6826" s="T817">măn-ziʔ</ta>
            <ta e="T820" id="Seg_6827" s="T819">gibər</ta>
            <ta e="T822" id="Seg_6828" s="T821">da</ta>
            <ta e="T823" id="Seg_6829" s="T822">dĭbər</ta>
            <ta e="T825" id="Seg_6830" s="T824">a</ta>
            <ta e="T826" id="Seg_6831" s="T825">ija-l</ta>
            <ta e="T828" id="Seg_6832" s="T827">ija-l</ta>
            <ta e="T829" id="Seg_6833" s="T828">pušaj</ta>
            <ta e="T831" id="Seg_6834" s="T830">ej</ta>
            <ta e="T833" id="Seg_6835" s="T832">ej</ta>
            <ta e="T834" id="Seg_6836" s="T833">i-l-m</ta>
            <ta e="T835" id="Seg_6837" s="T834">ija-m</ta>
            <ta e="T837" id="Seg_6838" s="T836">a</ta>
            <ta e="T838" id="Seg_6839" s="T837">miʔ</ta>
            <ta e="T839" id="Seg_6840" s="T838">šide-göʔ</ta>
            <ta e="T840" id="Seg_6841" s="T839">kan-žə-bəj</ta>
            <ta e="T842" id="Seg_6842" s="T841">a</ta>
            <ta e="T843" id="Seg_6843" s="T842">gibər</ta>
            <ta e="T845" id="Seg_6844" s="T844">da</ta>
            <ta e="T846" id="Seg_6845" s="T845">von</ta>
            <ta e="T847" id="Seg_6846" s="T846">dĭbər</ta>
            <ta e="T849" id="Seg_6847" s="T848">gijen</ta>
            <ta e="T850" id="Seg_6848" s="T849">il</ta>
            <ta e="T852" id="Seg_6849" s="T851">a</ta>
            <ta e="T853" id="Seg_6850" s="T852">ĭmbi</ta>
            <ta e="T854" id="Seg_6851" s="T853">dĭ</ta>
            <ta e="T855" id="Seg_6852" s="T854">dĭgən</ta>
            <ta e="T857" id="Seg_6853" s="T856">ĭmbi=də</ta>
            <ta e="T858" id="Seg_6854" s="T857">ĭmbi=nʼibudʼ</ta>
            <ta e="T859" id="Seg_6855" s="T858">nörbə-lV-jəʔ</ta>
            <ta e="T861" id="Seg_6856" s="T860">a</ta>
            <ta e="T865" id="Seg_6857" s="T864">nörbə-lV-jəʔ</ta>
            <ta e="T866" id="Seg_6858" s="T865">ĭmbi=nʼibudʼ</ta>
            <ta e="T868" id="Seg_6859" s="T867">a</ta>
            <ta e="T869" id="Seg_6860" s="T868">miʔ</ta>
            <ta e="T870" id="Seg_6861" s="T869">măn</ta>
            <ta e="T871" id="Seg_6862" s="T870">ĭmbi=də</ta>
            <ta e="T872" id="Seg_6863" s="T871">ej</ta>
            <ta e="T873" id="Seg_6864" s="T872">nörbə-lV-m</ta>
            <ta e="T875" id="Seg_6865" s="T874">a</ta>
            <ta e="T876" id="Seg_6866" s="T875">moʔ</ta>
            <ta e="T877" id="Seg_6867" s="T876">ej</ta>
            <ta e="T878" id="Seg_6868" s="T877">nörbə-lV-l</ta>
            <ta e="T880" id="Seg_6869" s="T879">dʼok</ta>
            <ta e="T882" id="Seg_6870" s="T881">eželʼi</ta>
            <ta e="T883" id="Seg_6871" s="T882">nörbə-zittə</ta>
            <ta e="T885" id="Seg_6872" s="T884">bile</ta>
            <ta e="T886" id="Seg_6873" s="T885">il</ta>
            <ta e="T887" id="Seg_6874" s="T886">mo-lV-m</ta>
            <ta e="T889" id="Seg_6875" s="T888">moʔ</ta>
            <ta e="T890" id="Seg_6876" s="T889">dărəʔ</ta>
            <ta e="T892" id="Seg_6877" s="T891">dʼok</ta>
            <ta e="T893" id="Seg_6878" s="T892">jakšə</ta>
            <ta e="T894" id="Seg_6879" s="T893">il</ta>
            <ta e="T896" id="Seg_6880" s="T895">dʼok</ta>
            <ta e="T897" id="Seg_6881" s="T896">bile</ta>
            <ta e="T898" id="Seg_6882" s="T897">il</ta>
            <ta e="T900" id="Seg_6883" s="T899">no</ta>
            <ta e="T901" id="Seg_6884" s="T900">măn</ta>
            <ta e="T902" id="Seg_6885" s="T901">ari</ta>
            <ta e="T903" id="Seg_6886" s="T902">tʼür-lV-m</ta>
            <ta e="T904" id="Seg_6887" s="T903">tănan</ta>
            <ta e="T908" id="Seg_6888" s="T907">măn</ta>
            <ta e="T909" id="Seg_6889" s="T908">aba-m</ta>
            <ta e="T910" id="Seg_6890" s="T909">tăn</ta>
            <ta e="T911" id="Seg_6891" s="T910">aba-l</ta>
            <ta e="T912" id="Seg_6892" s="T911">tʼabəro-labaʔ-jəʔ</ta>
            <ta e="T914" id="Seg_6893" s="T913">moʔ</ta>
            <ta e="T915" id="Seg_6894" s="T914">dĭ</ta>
            <ta e="T916" id="Seg_6895" s="T915">tʼabəro-labaʔ-jəʔ</ta>
            <ta e="T918" id="Seg_6896" s="T917">tak</ta>
            <ta e="T919" id="Seg_6897" s="T918">dĭ</ta>
            <ta e="T920" id="Seg_6898" s="T919">măn-labaʔ-t</ta>
            <ta e="T921" id="Seg_6899" s="T920">tăn</ta>
            <ta e="T922" id="Seg_6900" s="T921">aba-l</ta>
            <ta e="T924" id="Seg_6901" s="T922">măn-labaʔ-t</ta>
            <ta e="T925" id="Seg_6902" s="T924">măn</ta>
            <ta e="T926" id="Seg_6903" s="T925">nʼi-m</ta>
            <ta e="T927" id="Seg_6904" s="T926">jakšə</ta>
            <ta e="T928" id="Seg_6905" s="T927">kuvas</ta>
            <ta e="T930" id="Seg_6906" s="T929">a</ta>
            <ta e="T931" id="Seg_6907" s="T930">tăn</ta>
            <ta e="T932" id="Seg_6908" s="T931">koʔbdo</ta>
            <ta e="T933" id="Seg_6909" s="T932">ej</ta>
            <ta e="T934" id="Seg_6910" s="T933">jakšə</ta>
            <ta e="T936" id="Seg_6911" s="T935">a</ta>
            <ta e="T937" id="Seg_6912" s="T936">măn</ta>
            <ta e="T938" id="Seg_6913" s="T937">aba-m</ta>
            <ta e="T940" id="Seg_6914" s="T938">măn-labaʔ-t</ta>
            <ta e="T941" id="Seg_6915" s="T940">o</ta>
            <ta e="T942" id="Seg_6916" s="T941">măn</ta>
            <ta e="T943" id="Seg_6917" s="T942">koʔbdo-m</ta>
            <ta e="T944" id="Seg_6918" s="T943">jakšə</ta>
            <ta e="T946" id="Seg_6919" s="T945">i</ta>
            <ta e="T947" id="Seg_6920" s="T946">kuvas</ta>
            <ta e="T949" id="Seg_6921" s="T948">i</ta>
            <ta e="T950" id="Seg_6922" s="T949">bar</ta>
            <ta e="T951" id="Seg_6923" s="T950">ĭmbi</ta>
            <ta e="T952" id="Seg_6924" s="T951">bar</ta>
            <ta e="T953" id="Seg_6925" s="T952">ĭmbi</ta>
            <ta e="T954" id="Seg_6926" s="T953">măn-labaʔ-t</ta>
            <ta e="T956" id="Seg_6927" s="T955">i</ta>
            <ta e="T957" id="Seg_6928" s="T956">tăn</ta>
            <ta e="T958" id="Seg_6929" s="T957">nʼi-l</ta>
            <ta e="T959" id="Seg_6930" s="T958">ej</ta>
            <ta e="T960" id="Seg_6931" s="T959">jakšə</ta>
            <ta e="T962" id="Seg_6932" s="T961">ĭmbi=də</ta>
            <ta e="T963" id="Seg_6933" s="T962">ej</ta>
            <ta e="T964" id="Seg_6934" s="T963">nörbə-labaʔ-t</ta>
            <ta e="T965" id="Seg_6935" s="T964">măna</ta>
            <ta e="T967" id="Seg_6936" s="T966">a</ta>
            <ta e="T968" id="Seg_6937" s="T967">măn</ta>
            <ta e="T969" id="Seg_6938" s="T968">koʔbdo-m</ta>
            <ta e="T970" id="Seg_6939" s="T969">bar</ta>
            <ta e="T971" id="Seg_6940" s="T970">ĭmbi</ta>
            <ta e="T972" id="Seg_6941" s="T971">bar</ta>
            <ta e="T974" id="Seg_6942" s="T973">tănan</ta>
            <ta e="T975" id="Seg_6943" s="T974">üge</ta>
            <ta e="T976" id="Seg_6944" s="T975">nörbə-labaʔ-liA-t</ta>
            <ta e="T978" id="Seg_6945" s="T977">vot</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_6946" s="T1">good</ta>
            <ta e="T3" id="Seg_6947" s="T2">human.[NOM.SG]</ta>
            <ta e="T4" id="Seg_6948" s="T3">can-PST.[3SG]</ta>
            <ta e="T5" id="Seg_6949" s="T4">die-RES-PST.[3SG]</ta>
            <ta e="T7" id="Seg_6950" s="T6">good</ta>
            <ta e="T8" id="Seg_6951" s="T7">die-RES-PST.[3SG]</ta>
            <ta e="T10" id="Seg_6952" s="T9">and</ta>
            <ta e="T11" id="Seg_6953" s="T10">this.[NOM.SG]</ta>
            <ta e="T12" id="Seg_6954" s="T11">bad.[NOM.SG]</ta>
            <ta e="T13" id="Seg_6955" s="T12">human.[NOM.SG]</ta>
            <ta e="T14" id="Seg_6956" s="T13">one.[NOM.SG]</ta>
            <ta e="T17" id="Seg_6957" s="T16">die-RES-PST-3SG.O</ta>
            <ta e="T19" id="Seg_6958" s="T18">and</ta>
            <ta e="T20" id="Seg_6959" s="T19">good</ta>
            <ta e="T21" id="Seg_6960" s="T20">human.[NOM.SG]</ta>
            <ta e="T22" id="Seg_6961" s="T21">good</ta>
            <ta e="T23" id="Seg_6962" s="T22">die-RES-PST.[3SG]</ta>
            <ta e="T25" id="Seg_6963" s="T24">look</ta>
            <ta e="T27" id="Seg_6964" s="T26">I.GEN</ta>
            <ta e="T28" id="Seg_6965" s="T27">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T29" id="Seg_6966" s="T28">good</ta>
            <ta e="T30" id="Seg_6967" s="T29">live-PST.[3SG]</ta>
            <ta e="T32" id="Seg_6968" s="T31">then</ta>
            <ta e="T33" id="Seg_6969" s="T32">bad.[NOM.SG]</ta>
            <ta e="T35" id="Seg_6970" s="T34">strongly</ta>
            <ta e="T36" id="Seg_6971" s="T35">bad.[NOM.SG]</ta>
            <ta e="T37" id="Seg_6972" s="T36">live-PST.[3SG]</ta>
            <ta e="T39" id="Seg_6973" s="T38">then</ta>
            <ta e="T42" id="Seg_6974" s="T41">PTCL</ta>
            <ta e="T43" id="Seg_6975" s="T42">live-PST.[3SG]</ta>
            <ta e="T44" id="Seg_6976" s="T43">good</ta>
            <ta e="T46" id="Seg_6977" s="T45">then</ta>
            <ta e="T47" id="Seg_6978" s="T46">die-RES-PST.[3SG]</ta>
            <ta e="T49" id="Seg_6979" s="T48">well</ta>
            <ta e="T52" id="Seg_6980" s="T51">I.NOM</ta>
            <ta e="T53" id="Seg_6981" s="T52">live-PST-1SG</ta>
            <ta e="T54" id="Seg_6982" s="T53">strongly</ta>
            <ta e="T55" id="Seg_6983" s="T54">bad.[NOM.SG]</ta>
            <ta e="T57" id="Seg_6984" s="T56">strongly</ta>
            <ta e="T58" id="Seg_6985" s="T57">bad.[NOM.SG]</ta>
            <ta e="T59" id="Seg_6986" s="T58">live-PST-1SG</ta>
            <ta e="T61" id="Seg_6987" s="T60">I.NOM</ta>
            <ta e="T62" id="Seg_6988" s="T61">bread-NOM/GEN/ACC.1SG</ta>
            <ta e="T63" id="Seg_6989" s="T62">NEG.EX.[3SG]</ta>
            <ta e="T65" id="Seg_6990" s="T64">NEG.EX.[3SG]</ta>
            <ta e="T66" id="Seg_6991" s="T65">bread.[NOM.SG]</ta>
            <ta e="T67" id="Seg_6992" s="T66">I.NOM</ta>
            <ta e="T68" id="Seg_6993" s="T67">and</ta>
            <ta e="T69" id="Seg_6994" s="T68">I.NOM</ta>
            <ta e="T70" id="Seg_6995" s="T69">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T71" id="Seg_6996" s="T70">what-what</ta>
            <ta e="T72" id="Seg_6997" s="T71">eat-PST-1SG</ta>
            <ta e="T74" id="Seg_6998" s="T73">then=%%</ta>
            <ta e="T75" id="Seg_6999" s="T74">good</ta>
            <ta e="T76" id="Seg_7000" s="T75">live-PST-1SG</ta>
            <ta e="T78" id="Seg_7001" s="T77">PTCL</ta>
            <ta e="T81" id="Seg_7002" s="T80">then-%%</ta>
            <ta e="T82" id="Seg_7003" s="T81">little</ta>
            <ta e="T83" id="Seg_7004" s="T82">disappear-PST-1SG</ta>
            <ta e="T85" id="Seg_7005" s="T84">live-PST-1SG</ta>
            <ta e="T86" id="Seg_7006" s="T85">poor-poor-poor-poor</ta>
            <ta e="T87" id="Seg_7007" s="T86">daughter-NOM/GEN/ACC.1SG</ta>
            <ta e="T88" id="Seg_7008" s="T87">give-HAB</ta>
            <ta e="T89" id="Seg_7009" s="T88">give-HAB</ta>
            <ta e="T90" id="Seg_7010" s="T89">give-HAB-PST-1SG</ta>
            <ta e="T92" id="Seg_7011" s="T91">and</ta>
            <ta e="T93" id="Seg_7012" s="T92">daughter-COM</ta>
            <ta e="T94" id="Seg_7013" s="T93">strongly</ta>
            <ta e="T95" id="Seg_7014" s="T94">bad.[NOM.SG]</ta>
            <ta e="T96" id="Seg_7015" s="T95">live-PST-1SG</ta>
            <ta e="T98" id="Seg_7016" s="T97">cry-RES-1SG</ta>
            <ta e="T100" id="Seg_7017" s="T99">and</ta>
            <ta e="T102" id="Seg_7018" s="T101">go-FUT-1SG</ta>
            <ta e="T103" id="Seg_7019" s="T102">and</ta>
            <ta e="T104" id="Seg_7020" s="T103">come</ta>
            <ta e="T105" id="Seg_7021" s="T104">come-RES-1SG</ta>
            <ta e="T107" id="Seg_7022" s="T106">live-INF.LAT</ta>
            <ta e="T108" id="Seg_7023" s="T107">strongly</ta>
            <ta e="T109" id="Seg_7024" s="T108">bad.[NOM.SG]</ta>
            <ta e="T111" id="Seg_7025" s="T110">where.to</ta>
            <ta e="T112" id="Seg_7026" s="T111">I.LAT</ta>
            <ta e="T113" id="Seg_7027" s="T112">go-INF.LAT</ta>
            <ta e="T115" id="Seg_7028" s="T114">where.to=INDEF</ta>
            <ta e="T116" id="Seg_7029" s="T115">NEG</ta>
            <ta e="T117" id="Seg_7030" s="T116">go-FUT-1SG</ta>
            <ta e="T118" id="Seg_7031" s="T117">better</ta>
            <ta e="T119" id="Seg_7032" s="T118">die-RES-PST-1SG</ta>
            <ta e="T123" id="Seg_7033" s="T122">PTCL</ta>
            <ta e="T124" id="Seg_7034" s="T123">people.[NOM.SG]</ta>
            <ta e="T125" id="Seg_7035" s="T124">good</ta>
            <ta e="T126" id="Seg_7036" s="T125">live-PRS-3PL</ta>
            <ta e="T128" id="Seg_7037" s="T127">and</ta>
            <ta e="T129" id="Seg_7038" s="T128">then</ta>
            <ta e="T130" id="Seg_7039" s="T129">little</ta>
            <ta e="T131" id="Seg_7040" s="T130">disappear-PRS</ta>
            <ta e="T133" id="Seg_7041" s="T132">where.to=INDEF</ta>
            <ta e="T135" id="Seg_7042" s="T134">and</ta>
            <ta e="T136" id="Seg_7043" s="T135">then</ta>
            <ta e="T137" id="Seg_7044" s="T136">why</ta>
            <ta e="T138" id="Seg_7045" s="T137">here</ta>
            <ta e="T139" id="Seg_7046" s="T138">PTCL</ta>
            <ta e="T140" id="Seg_7047" s="T139">come-FUT-3SG</ta>
            <ta e="T142" id="Seg_7048" s="T141">oh</ta>
            <ta e="T143" id="Seg_7049" s="T142">why</ta>
            <ta e="T144" id="Seg_7050" s="T143">so</ta>
            <ta e="T145" id="Seg_7051" s="T144">can-FRQ-FUT-2SG</ta>
            <ta e="T147" id="Seg_7052" s="T146">and</ta>
            <ta e="T148" id="Seg_7053" s="T147">what.[NOM.SG]</ta>
            <ta e="T150" id="Seg_7054" s="T149">so</ta>
            <ta e="T152" id="Seg_7055" s="T151">come-FUT-3SG</ta>
            <ta e="T153" id="Seg_7056" s="T152">here</ta>
            <ta e="T155" id="Seg_7057" s="T154">and</ta>
            <ta e="T156" id="Seg_7058" s="T155">here</ta>
            <ta e="T157" id="Seg_7059" s="T156">come-FUT-3SG</ta>
            <ta e="T159" id="Seg_7060" s="T158">and</ta>
            <ta e="T160" id="Seg_7061" s="T159">you.NOM</ta>
            <ta e="T161" id="Seg_7062" s="T160">what.[NOM.SG]</ta>
            <ta e="T162" id="Seg_7063" s="T161">NEG</ta>
            <ta e="T163" id="Seg_7064" s="T162">go-FUT-2SG</ta>
            <ta e="T165" id="Seg_7065" s="T164">no</ta>
            <ta e="T167" id="Seg_7066" s="T166">I.NOM</ta>
            <ta e="T168" id="Seg_7067" s="T167">NEG</ta>
            <ta e="T169" id="Seg_7068" s="T168">go-FUT-1SG</ta>
            <ta e="T171" id="Seg_7069" s="T170">I.NOM</ta>
            <ta e="T172" id="Seg_7070" s="T171">that-LOC</ta>
            <ta e="T173" id="Seg_7071" s="T172">can-FUT-1SG</ta>
            <ta e="T175" id="Seg_7072" s="T174">look</ta>
            <ta e="T179" id="Seg_7073" s="T178">what=INDEF</ta>
            <ta e="T180" id="Seg_7074" s="T179">boil-INF.LAT</ta>
            <ta e="T181" id="Seg_7075" s="T180">one.should</ta>
            <ta e="T183" id="Seg_7076" s="T182">I.NOM</ta>
            <ta e="T184" id="Seg_7077" s="T183">eat-INF.LAT</ta>
            <ta e="T185" id="Seg_7078" s="T184">want-FUT-1SG</ta>
            <ta e="T191" id="Seg_7079" s="T190">what.[NOM.SG]</ta>
            <ta e="T192" id="Seg_7080" s="T191">boil-PRS-2SG</ta>
            <ta e="T194" id="Seg_7081" s="T193">meat.[NOM.SG]</ta>
            <ta e="T196" id="Seg_7082" s="T195">why</ta>
            <ta e="T197" id="Seg_7083" s="T196">meat.[NOM.SG]</ta>
            <ta e="T199" id="Seg_7084" s="T198">I.NOM</ta>
            <ta e="T200" id="Seg_7085" s="T199">IRREAL</ta>
            <ta e="T201" id="Seg_7086" s="T200">%%-1SG</ta>
            <ta e="T202" id="Seg_7087" s="T201">eat-INF</ta>
            <ta e="T204" id="Seg_7088" s="T203">and</ta>
            <ta e="T205" id="Seg_7089" s="T204">I.NOM</ta>
            <ta e="T206" id="Seg_7090" s="T205">IRREAL</ta>
            <ta e="T207" id="Seg_7091" s="T206">milk.[NOM.SG]</ta>
            <ta e="T208" id="Seg_7092" s="T207">IRREAL</ta>
            <ta e="T210" id="Seg_7093" s="T209">look</ta>
            <ta e="T211" id="Seg_7094" s="T210">good</ta>
            <ta e="T212" id="Seg_7095" s="T211">IRREAL</ta>
            <ta e="T213" id="Seg_7096" s="T212">eat-FUT-1SG</ta>
            <ta e="T215" id="Seg_7097" s="T214">why</ta>
            <ta e="T216" id="Seg_7098" s="T215">milk.[NOM.SG]</ta>
            <ta e="T218" id="Seg_7099" s="T217">milk.[NOM.SG]</ta>
            <ta e="T219" id="Seg_7100" s="T218">so</ta>
            <ta e="T220" id="Seg_7101" s="T219">milk.[NOM.SG]</ta>
            <ta e="T222" id="Seg_7102" s="T221">and</ta>
            <ta e="T223" id="Seg_7103" s="T222">meat.[NOM.SG]</ta>
            <ta e="T224" id="Seg_7104" s="T223">good</ta>
            <ta e="T228" id="Seg_7105" s="T227">why</ta>
            <ta e="T229" id="Seg_7106" s="T228">I.LAT</ta>
            <ta e="T230" id="Seg_7107" s="T229">NEG</ta>
            <ta e="T231" id="Seg_7108" s="T230">come-FUT-3SG</ta>
            <ta e="T233" id="Seg_7109" s="T232">and</ta>
            <ta e="T234" id="Seg_7110" s="T233">I.NOM</ta>
            <ta e="T235" id="Seg_7111" s="T234">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T236" id="Seg_7112" s="T235">bad</ta>
            <ta e="T239" id="Seg_7113" s="T238">you.DAT</ta>
            <ta e="T240" id="Seg_7114" s="T239">NEG</ta>
            <ta e="T241" id="Seg_7115" s="T240">send-PRS-3SG.O</ta>
            <ta e="T243" id="Seg_7116" s="T242">why</ta>
            <ta e="T244" id="Seg_7117" s="T243">so</ta>
            <ta e="T245" id="Seg_7118" s="T244">NEG</ta>
            <ta e="T247" id="Seg_7119" s="T246">well</ta>
            <ta e="T248" id="Seg_7120" s="T247">say-RES-3SG.O</ta>
            <ta e="T249" id="Seg_7121" s="T248">that</ta>
            <ta e="T250" id="Seg_7122" s="T249">bad</ta>
            <ta e="T251" id="Seg_7123" s="T250">man-NOM/GEN.3SG</ta>
            <ta e="T252" id="Seg_7124" s="T251">you.NOM</ta>
            <ta e="T254" id="Seg_7125" s="T253">why</ta>
            <ta e="T255" id="Seg_7126" s="T254">so</ta>
            <ta e="T256" id="Seg_7127" s="T255">bad</ta>
            <ta e="T257" id="Seg_7128" s="T256">man.[NOM.SG]</ta>
            <ta e="T259" id="Seg_7129" s="T258">I.NOM</ta>
            <ta e="T260" id="Seg_7130" s="T259">good</ta>
            <ta e="T261" id="Seg_7131" s="T260">man.[NOM.SG]</ta>
            <ta e="T263" id="Seg_7132" s="T262">what=INDEF</ta>
            <ta e="T264" id="Seg_7133" s="T263">you.DAT</ta>
            <ta e="T265" id="Seg_7134" s="T264">PTCL</ta>
            <ta e="T266" id="Seg_7135" s="T265">IRREAL</ta>
            <ta e="T267" id="Seg_7136" s="T266">tell-FUT-1SG</ta>
            <ta e="T268" id="Seg_7137" s="T267">tell-FUT-1SG</ta>
            <ta e="T269" id="Seg_7138" s="T268">IRREAL</ta>
            <ta e="T272" id="Seg_7139" s="T271">strongly</ta>
            <ta e="T273" id="Seg_7140" s="T272">good</ta>
            <ta e="T274" id="Seg_7141" s="T273">become-FUT-3SG</ta>
            <ta e="T276" id="Seg_7142" s="T275">oh</ta>
            <ta e="T277" id="Seg_7143" s="T276">well</ta>
            <ta e="T278" id="Seg_7144" s="T277">so</ta>
            <ta e="T279" id="Seg_7145" s="T278">say-RES-3SG.O</ta>
            <ta e="T280" id="Seg_7146" s="T279">I.LAT</ta>
            <ta e="T282" id="Seg_7147" s="T281">I.NOM</ta>
            <ta e="T283" id="Seg_7148" s="T282">this-ACC</ta>
            <ta e="T284" id="Seg_7149" s="T283">PTCL</ta>
            <ta e="T285" id="Seg_7150" s="T284">beat-FUT-1SG</ta>
            <ta e="T286" id="Seg_7151" s="T285">strongly</ta>
            <ta e="T288" id="Seg_7152" s="T287">no</ta>
            <ta e="T289" id="Seg_7153" s="T288">beat-INF.LAT</ta>
            <ta e="T293" id="Seg_7154" s="T292">otherwise</ta>
            <ta e="T294" id="Seg_7155" s="T293">this.[NOM.SG]</ta>
            <ta e="T295" id="Seg_7156" s="T294">I.LAT</ta>
            <ta e="T296" id="Seg_7157" s="T295">beat-FUT-2SG</ta>
            <ta e="T297" id="Seg_7158" s="T296">then-%%</ta>
            <ta e="T301" id="Seg_7159" s="T300">I.NOM</ta>
            <ta e="T302" id="Seg_7160" s="T301">strongly</ta>
            <ta e="T303" id="Seg_7161" s="T302">vodka.[NOM.SG]</ta>
            <ta e="T304" id="Seg_7162" s="T303">drink-PRS-1SG</ta>
            <ta e="T306" id="Seg_7163" s="T305">oh</ta>
            <ta e="T307" id="Seg_7164" s="T306">why</ta>
            <ta e="T308" id="Seg_7165" s="T307">so</ta>
            <ta e="T309" id="Seg_7166" s="T308">you.NOM</ta>
            <ta e="T311" id="Seg_7167" s="T310">well</ta>
            <ta e="T312" id="Seg_7168" s="T311">I.NOM</ta>
            <ta e="T313" id="Seg_7169" s="T312">bad.[NOM.SG]</ta>
            <ta e="T314" id="Seg_7170" s="T313">man.[NOM.SG]</ta>
            <ta e="T316" id="Seg_7171" s="T315">PTCL</ta>
            <ta e="T317" id="Seg_7172" s="T316">%%</ta>
            <ta e="T318" id="Seg_7173" s="T317">PTCL</ta>
            <ta e="T319" id="Seg_7174" s="T318">take-FUT-1SG</ta>
            <ta e="T320" id="Seg_7175" s="T319">what-NOM/GEN/ACC.3SG</ta>
            <ta e="T322" id="Seg_7176" s="T321">and</ta>
            <ta e="T323" id="Seg_7177" s="T322">why</ta>
            <ta e="T324" id="Seg_7178" s="T323">you.DAT</ta>
            <ta e="T325" id="Seg_7179" s="T324">take-FUT-2SG</ta>
            <ta e="T326" id="Seg_7180" s="T325">so</ta>
            <ta e="T327" id="Seg_7181" s="T326">I.NOM</ta>
            <ta e="T328" id="Seg_7182" s="T327">then=%%</ta>
            <ta e="T330" id="Seg_7183" s="T329">I.NOM</ta>
            <ta e="T331" id="Seg_7184" s="T330">then-%%</ta>
            <ta e="T332" id="Seg_7185" s="T331">take-FUT-1SG</ta>
            <ta e="T334" id="Seg_7186" s="T333">and</ta>
            <ta e="T335" id="Seg_7187" s="T334">why</ta>
            <ta e="T336" id="Seg_7188" s="T335">there</ta>
            <ta e="T337" id="Seg_7189" s="T336">so</ta>
            <ta e="T338" id="Seg_7190" s="T337">take-FUT-2SG</ta>
            <ta e="T340" id="Seg_7191" s="T339">so</ta>
            <ta e="T341" id="Seg_7192" s="T340">look</ta>
            <ta e="T342" id="Seg_7193" s="T341">take-FUT-1SG</ta>
            <ta e="T343" id="Seg_7194" s="T342">I.NOM</ta>
            <ta e="T345" id="Seg_7195" s="T344">I.LAT</ta>
            <ta e="T346" id="Seg_7196" s="T345">then-%%</ta>
            <ta e="T347" id="Seg_7197" s="T346">NEG</ta>
            <ta e="T348" id="Seg_7198" s="T347">beautiful.[NOM.SG]</ta>
            <ta e="T349" id="Seg_7199" s="T348">house-LAT</ta>
            <ta e="T352" id="Seg_7200" s="T351">I.NOM</ta>
            <ta e="T353" id="Seg_7201" s="T352">there</ta>
            <ta e="T354" id="Seg_7202" s="T353">live-FUT-1SG</ta>
            <ta e="T356" id="Seg_7203" s="T355">and</ta>
            <ta e="T357" id="Seg_7204" s="T356">then</ta>
            <ta e="T358" id="Seg_7205" s="T357">like</ta>
            <ta e="T359" id="Seg_7206" s="T358">come-FUT-1SG</ta>
            <ta e="T360" id="Seg_7207" s="T359">I.LAT</ta>
            <ta e="T361" id="Seg_7208" s="T360">people.[NOM.SG]</ta>
            <ta e="T363" id="Seg_7209" s="T362">who-NOM/GEN/ACC.3SG</ta>
            <ta e="T364" id="Seg_7210" s="T363">NEG</ta>
            <ta e="T366" id="Seg_7211" s="T365">NEG</ta>
            <ta e="T373" id="Seg_7212" s="T372">NEG</ta>
            <ta e="T374" id="Seg_7213" s="T373">take-PRS-3SG.O</ta>
            <ta e="T376" id="Seg_7214" s="T375">PTCL</ta>
            <ta e="T377" id="Seg_7215" s="T376">people.[NOM.SG]</ta>
            <ta e="T379" id="Seg_7216" s="T378">and</ta>
            <ta e="T380" id="Seg_7217" s="T379">NEG</ta>
            <ta e="T381" id="Seg_7218" s="T380">good</ta>
            <ta e="T382" id="Seg_7219" s="T381">man.[NOM.SG]</ta>
            <ta e="T384" id="Seg_7220" s="T383">look</ta>
            <ta e="T388" id="Seg_7221" s="T387">bad.[NOM.SG]</ta>
            <ta e="T389" id="Seg_7222" s="T388">man.[NOM.SG]</ta>
            <ta e="T390" id="Seg_7223" s="T389">good</ta>
            <ta e="T391" id="Seg_7224" s="T390">man-GEN</ta>
            <ta e="T392" id="Seg_7225" s="T391">take-PST.[3SG]</ta>
            <ta e="T393" id="Seg_7226" s="T392">shirt.[NOM.SG]</ta>
            <ta e="T394" id="Seg_7227" s="T393">meat.[NOM.SG]</ta>
            <ta e="T395" id="Seg_7228" s="T394">and</ta>
            <ta e="T396" id="Seg_7229" s="T395">PTCL</ta>
            <ta e="T397" id="Seg_7230" s="T396">what.[NOM.SG]</ta>
            <ta e="T398" id="Seg_7231" s="T397">PTCL</ta>
            <ta e="T400" id="Seg_7232" s="T399">money-NOM/GEN.3SG</ta>
            <ta e="T401" id="Seg_7233" s="T400">can-PST.[3SG]</ta>
            <ta e="T402" id="Seg_7234" s="T401">this.[NOM.SG]</ta>
            <ta e="T403" id="Seg_7235" s="T402">man-GEN</ta>
            <ta e="T405" id="Seg_7236" s="T404">PTCL</ta>
            <ta e="T406" id="Seg_7237" s="T405">take-PST.[3SG]</ta>
            <ta e="T408" id="Seg_7238" s="T407">what.[NOM.SG]</ta>
            <ta e="T409" id="Seg_7239" s="T408">you.NOM</ta>
            <ta e="T410" id="Seg_7240" s="T409">take-PST-2SG</ta>
            <ta e="T412" id="Seg_7241" s="T411">so</ta>
            <ta e="T413" id="Seg_7242" s="T412">I.NOM</ta>
            <ta e="T414" id="Seg_7243" s="T413">NEG.EX.[3SG]</ta>
            <ta e="T415" id="Seg_7244" s="T414">and</ta>
            <ta e="T416" id="Seg_7245" s="T415">you.NOM</ta>
            <ta e="T417" id="Seg_7246" s="T416">be-PRS.[3SG]</ta>
            <ta e="T420" id="Seg_7247" s="T419">I.NOM</ta>
            <ta e="T421" id="Seg_7248" s="T420">take-PST-1SG</ta>
            <ta e="T423" id="Seg_7249" s="T422">and</ta>
            <ta e="T424" id="Seg_7250" s="T423">I.NOM</ta>
            <ta e="T425" id="Seg_7251" s="T424">you.DAT</ta>
            <ta e="T426" id="Seg_7252" s="T425">NEG</ta>
            <ta e="T427" id="Seg_7253" s="T426">take-FUT-1SG</ta>
            <ta e="T429" id="Seg_7254" s="T428">I.NOM</ta>
            <ta e="T430" id="Seg_7255" s="T429">you.DAT</ta>
            <ta e="T431" id="Seg_7256" s="T430">take-FUT-1SG</ta>
            <ta e="T433" id="Seg_7257" s="T432">and</ta>
            <ta e="T434" id="Seg_7258" s="T433">why</ta>
            <ta e="T435" id="Seg_7259" s="T434">%%</ta>
            <ta e="T436" id="Seg_7260" s="T435">I.LAT</ta>
            <ta e="T437" id="Seg_7261" s="T436">take-FUT-2SG</ta>
            <ta e="T439" id="Seg_7262" s="T438">so</ta>
            <ta e="T440" id="Seg_7263" s="T439">%%</ta>
            <ta e="T441" id="Seg_7264" s="T440">what.for</ta>
            <ta e="T442" id="Seg_7265" s="T441">take-PST-2SG</ta>
            <ta e="T444" id="Seg_7266" s="T443">I.NOM</ta>
            <ta e="T445" id="Seg_7267" s="T444">bring-DUR.[3SG]</ta>
            <ta e="T446" id="Seg_7268" s="T445">and</ta>
            <ta e="T447" id="Seg_7269" s="T446">PTCL</ta>
            <ta e="T448" id="Seg_7270" s="T447">what-ACC</ta>
            <ta e="T449" id="Seg_7271" s="T448">PTCL</ta>
            <ta e="T451" id="Seg_7272" s="T450">and</ta>
            <ta e="T452" id="Seg_7273" s="T451">I.NOM</ta>
            <ta e="T453" id="Seg_7274" s="T452">bad.[NOM.SG]</ta>
            <ta e="T454" id="Seg_7275" s="T453">house-LAT</ta>
            <ta e="T455" id="Seg_7276" s="T454">you.DAT</ta>
            <ta e="T456" id="Seg_7277" s="T455">sit-DUR.[3SG]</ta>
            <ta e="T459" id="Seg_7278" s="T458">look</ta>
            <ta e="T463" id="Seg_7279" s="T462">sit.down-IMP.2SG</ta>
            <ta e="T464" id="Seg_7280" s="T463">I.NOM-COM</ta>
            <ta e="T465" id="Seg_7281" s="T464">eat-INF.LAT</ta>
            <ta e="T467" id="Seg_7282" s="T466">oh</ta>
            <ta e="T479" id="Seg_7283" s="T478">sit.down-IMP.2SG</ta>
            <ta e="T480" id="Seg_7284" s="T479">I.NOM-COM</ta>
            <ta e="T481" id="Seg_7285" s="T480">eat-INF.LAT</ta>
            <ta e="T483" id="Seg_7286" s="T482">and</ta>
            <ta e="T484" id="Seg_7287" s="T483">what.[NOM.SG]</ta>
            <ta e="T485" id="Seg_7288" s="T484">you.NOM</ta>
            <ta e="T486" id="Seg_7289" s="T485">can-FUT-3SG.O</ta>
            <ta e="T488" id="Seg_7290" s="T487">and</ta>
            <ta e="T489" id="Seg_7291" s="T488">I.NOM</ta>
            <ta e="T490" id="Seg_7292" s="T489">can-FUT-3SG.O</ta>
            <ta e="T491" id="Seg_7293" s="T490">bread.[NOM.SG]</ta>
            <ta e="T492" id="Seg_7294" s="T491">meat.[NOM.SG]</ta>
            <ta e="T493" id="Seg_7295" s="T492">and</ta>
            <ta e="T494" id="Seg_7296" s="T493">milk.[NOM.SG]</ta>
            <ta e="T496" id="Seg_7297" s="T495">sugar-%%</ta>
            <ta e="T498" id="Seg_7298" s="T497">eat-IMP.2SG.O</ta>
            <ta e="T500" id="Seg_7299" s="T499">so</ta>
            <ta e="T501" id="Seg_7300" s="T500">I.NOM</ta>
            <ta e="T502" id="Seg_7301" s="T501">IRREAL</ta>
            <ta e="T503" id="Seg_7302" s="T502">butter.[NOM.SG]</ta>
            <ta e="T504" id="Seg_7303" s="T503">eat-PRS-1SG</ta>
            <ta e="T505" id="Seg_7304" s="T504">IRREAL</ta>
            <ta e="T507" id="Seg_7305" s="T506">well</ta>
            <ta e="T508" id="Seg_7306" s="T507">and</ta>
            <ta e="T509" id="Seg_7307" s="T508">butter.[NOM.SG]</ta>
            <ta e="T510" id="Seg_7308" s="T509">I.NOM</ta>
            <ta e="T511" id="Seg_7309" s="T510">give-FUT-1SG</ta>
            <ta e="T512" id="Seg_7310" s="T511">you.DAT</ta>
            <ta e="T513" id="Seg_7311" s="T512">eat-INF.LAT</ta>
            <ta e="T515" id="Seg_7312" s="T514">good</ta>
            <ta e="T516" id="Seg_7313" s="T515">only</ta>
            <ta e="T517" id="Seg_7314" s="T516">eat-EP-IMP.2SG</ta>
            <ta e="T519" id="Seg_7315" s="T518">that.[NOM.SG]</ta>
            <ta e="T520" id="Seg_7316" s="T519">so</ta>
            <ta e="T521" id="Seg_7317" s="T520">I.NOM</ta>
            <ta e="T522" id="Seg_7318" s="T521">eat-FUT-1SG</ta>
            <ta e="T524" id="Seg_7319" s="T523">strongly</ta>
            <ta e="T525" id="Seg_7320" s="T524">strongly</ta>
            <ta e="T526" id="Seg_7321" s="T525">eat-FUT-1SG</ta>
            <ta e="T528" id="Seg_7322" s="T527">good</ta>
            <ta e="T529" id="Seg_7323" s="T528">eat-PRS-1SG</ta>
            <ta e="T535" id="Seg_7324" s="T534">why</ta>
            <ta e="T536" id="Seg_7325" s="T535">you.NOM</ta>
            <ta e="T537" id="Seg_7326" s="T536">I.NOM</ta>
            <ta e="T538" id="Seg_7327" s="T537">hair-NOM/GEN/ACC.1SG</ta>
            <ta e="T539" id="Seg_7328" s="T538">PTCL</ta>
            <ta e="T540" id="Seg_7329" s="T539">self</ta>
            <ta e="T541" id="Seg_7330" s="T540">hand-LAT</ta>
            <ta e="T542" id="Seg_7331" s="T541">take-PST-2SG</ta>
            <ta e="T544" id="Seg_7332" s="T543">you.know</ta>
            <ta e="T545" id="Seg_7333" s="T544">you.NOM</ta>
            <ta e="T546" id="Seg_7334" s="T545">hand-NOM/GEN/ACC.2SG</ta>
            <ta e="T547" id="Seg_7335" s="T546">big.[NOM.SG]</ta>
            <ta e="T548" id="Seg_7336" s="T547">and</ta>
            <ta e="T549" id="Seg_7337" s="T548">I.GEN</ta>
            <ta e="T550" id="Seg_7338" s="T549">a.few</ta>
            <ta e="T552" id="Seg_7339" s="T551">why</ta>
            <ta e="T553" id="Seg_7340" s="T552">so</ta>
            <ta e="T554" id="Seg_7341" s="T553">become-FUT-2SG</ta>
            <ta e="T556" id="Seg_7342" s="T555">bad.[NOM.SG]</ta>
            <ta e="T558" id="Seg_7343" s="T557">NEG</ta>
            <ta e="T559" id="Seg_7344" s="T558">good</ta>
            <ta e="T560" id="Seg_7345" s="T559">so</ta>
            <ta e="T562" id="Seg_7346" s="T561">look</ta>
            <ta e="T566" id="Seg_7347" s="T565">I.NOM</ta>
            <ta e="T567" id="Seg_7348" s="T566">strongly</ta>
            <ta e="T568" id="Seg_7349" s="T567">hurt-MOM-PST-1SG</ta>
            <ta e="T570" id="Seg_7350" s="T569">hand-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T571" id="Seg_7351" s="T570">hurt-PRS-3SG.O</ta>
            <ta e="T573" id="Seg_7352" s="T572">and</ta>
            <ta e="T574" id="Seg_7353" s="T573">foot-ACC.3SG</ta>
            <ta e="T575" id="Seg_7354" s="T574">PTCL</ta>
            <ta e="T576" id="Seg_7355" s="T575">hurt-DUR-PRS-3SG.O</ta>
            <ta e="T578" id="Seg_7356" s="T577">however</ta>
            <ta e="T579" id="Seg_7357" s="T578">I.NOM</ta>
            <ta e="T580" id="Seg_7358" s="T579">die-RES-FACT-%%</ta>
            <ta e="T582" id="Seg_7359" s="T581">no</ta>
            <ta e="T583" id="Seg_7360" s="T582">not</ta>
            <ta e="T584" id="Seg_7361" s="T583">one.should</ta>
            <ta e="T585" id="Seg_7362" s="T584">die-RES-FACT-INF.LAT</ta>
            <ta e="T587" id="Seg_7363" s="T586">one.should</ta>
            <ta e="T588" id="Seg_7364" s="T587">live-INF.LAT</ta>
            <ta e="T590" id="Seg_7365" s="T589">live-INF.LAT</ta>
            <ta e="T591" id="Seg_7366" s="T590">good</ta>
            <ta e="T593" id="Seg_7367" s="T592">and</ta>
            <ta e="T594" id="Seg_7368" s="T593">die-RES-FACT-INF.LAT</ta>
            <ta e="T595" id="Seg_7369" s="T594">bad.[NOM.SG]</ta>
            <ta e="T597" id="Seg_7370" s="T596">strongly</ta>
            <ta e="T598" id="Seg_7371" s="T597">bad.[NOM.SG]</ta>
            <ta e="T600" id="Seg_7372" s="T599">look</ta>
            <ta e="T604" id="Seg_7373" s="T603">two.[NOM.SG]</ta>
            <ta e="T605" id="Seg_7374" s="T604">boy.[NOM.SG]</ta>
            <ta e="T606" id="Seg_7375" s="T605">live-PST-3PL</ta>
            <ta e="T608" id="Seg_7376" s="T607">then=%%</ta>
            <ta e="T609" id="Seg_7377" s="T608">this.[NOM.SG]</ta>
            <ta e="T610" id="Seg_7378" s="T609">boy.[NOM.SG]</ta>
            <ta e="T611" id="Seg_7379" s="T610">where.to=INDEF</ta>
            <ta e="T612" id="Seg_7380" s="T611">little</ta>
            <ta e="T613" id="Seg_7381" s="T612">disappear-INF.LAT</ta>
            <ta e="T614" id="Seg_7382" s="T613">want-FUT-3SG.O</ta>
            <ta e="T616" id="Seg_7383" s="T615">and</ta>
            <ta e="T617" id="Seg_7384" s="T616">one.[NOM.SG]</ta>
            <ta e="T618" id="Seg_7385" s="T617">house-LOC-EP-NOM/GEN/ACC.1SG</ta>
            <ta e="T619" id="Seg_7386" s="T618">live-DUR-PRS-3SG.O</ta>
            <ta e="T621" id="Seg_7387" s="T620">well</ta>
            <ta e="T622" id="Seg_7388" s="T621">give-FUT-3SG.O</ta>
            <ta e="T623" id="Seg_7389" s="T622">you.DAT</ta>
            <ta e="T624" id="Seg_7390" s="T623">horse.[NOM.SG]</ta>
            <ta e="T625" id="Seg_7391" s="T624">I.LAT</ta>
            <ta e="T626" id="Seg_7392" s="T625">cow.[NOM.SG]</ta>
            <ta e="T628" id="Seg_7393" s="T627">no</ta>
            <ta e="T629" id="Seg_7394" s="T628">you.NOM</ta>
            <ta e="T630" id="Seg_7395" s="T629">take-IMP.2SG.O</ta>
            <ta e="T631" id="Seg_7396" s="T630">horse.[NOM.SG]</ta>
            <ta e="T632" id="Seg_7397" s="T631">and</ta>
            <ta e="T633" id="Seg_7398" s="T632">I.NOM</ta>
            <ta e="T634" id="Seg_7399" s="T633">cow.[NOM.SG]</ta>
            <ta e="T636" id="Seg_7400" s="T635">oh</ta>
            <ta e="T637" id="Seg_7401" s="T636">devil.[NOM.SG]</ta>
            <ta e="T639" id="Seg_7402" s="T638">why</ta>
            <ta e="T640" id="Seg_7403" s="T639">so</ta>
            <ta e="T642" id="Seg_7404" s="T641">well</ta>
            <ta e="T643" id="Seg_7405" s="T642">you.NOM</ta>
            <ta e="T644" id="Seg_7406" s="T643">vagabond.[NOM.SG]</ta>
            <ta e="T646" id="Seg_7407" s="T645">devil-NOM/GEN/ACC.2SG</ta>
            <ta e="T648" id="Seg_7408" s="T647">why</ta>
            <ta e="T649" id="Seg_7409" s="T648">so</ta>
            <ta e="T650" id="Seg_7410" s="T649">scold-DES-PRS-2SG</ta>
            <ta e="T652" id="Seg_7411" s="T651">so</ta>
            <ta e="T653" id="Seg_7412" s="T652">one.should</ta>
            <ta e="T654" id="Seg_7413" s="T653">so</ta>
            <ta e="T655" id="Seg_7414" s="T654">scold-INF.LAT</ta>
            <ta e="T656" id="Seg_7415" s="T655">bad.[NOM.SG]</ta>
            <ta e="T658" id="Seg_7416" s="T657">and</ta>
            <ta e="T659" id="Seg_7417" s="T658">I.LAT</ta>
            <ta e="T660" id="Seg_7418" s="T659">you.know</ta>
            <ta e="T661" id="Seg_7419" s="T660">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T662" id="Seg_7420" s="T661">give-PST.[3SG]</ta>
            <ta e="T663" id="Seg_7421" s="T662">horse.[NOM.SG]</ta>
            <ta e="T664" id="Seg_7422" s="T663">and</ta>
            <ta e="T665" id="Seg_7423" s="T664">you.DAT</ta>
            <ta e="T666" id="Seg_7424" s="T665">cow.[NOM.SG]</ta>
            <ta e="T668" id="Seg_7425" s="T667">say-PST.[3SG]</ta>
            <ta e="T669" id="Seg_7426" s="T668">you.NOM</ta>
            <ta e="T670" id="Seg_7427" s="T669">take-IMP.2SG.O</ta>
            <ta e="T671" id="Seg_7428" s="T670">horse.[NOM.SG]</ta>
            <ta e="T672" id="Seg_7429" s="T671">and</ta>
            <ta e="T673" id="Seg_7430" s="T672">you.NOM</ta>
            <ta e="T674" id="Seg_7431" s="T673">take-IMP.2SG.O</ta>
            <ta e="T675" id="Seg_7432" s="T674">cow.[NOM.SG]</ta>
            <ta e="T677" id="Seg_7433" s="T676">and</ta>
            <ta e="T678" id="Seg_7434" s="T677">house.[NOM.SG]</ta>
            <ta e="T679" id="Seg_7435" s="T678">you.DAT</ta>
            <ta e="T681" id="Seg_7436" s="T680">and</ta>
            <ta e="T682" id="Seg_7437" s="T681">house.[NOM.SG]</ta>
            <ta e="T683" id="Seg_7438" s="T682">and</ta>
            <ta e="T684" id="Seg_7439" s="T683">cow.[NOM.SG]</ta>
            <ta e="T685" id="Seg_7440" s="T684">you.DAT</ta>
            <ta e="T687" id="Seg_7441" s="T686">and</ta>
            <ta e="T688" id="Seg_7442" s="T687">now</ta>
            <ta e="T689" id="Seg_7443" s="T688">cow.[NOM.SG]</ta>
            <ta e="T690" id="Seg_7444" s="T689">NEG</ta>
            <ta e="T691" id="Seg_7445" s="T690">take-PRS-3SG.O</ta>
            <ta e="T700" id="Seg_7446" s="T699">PTCL</ta>
            <ta e="T701" id="Seg_7447" s="T700">%%</ta>
            <ta e="T702" id="Seg_7448" s="T701">PTCL</ta>
            <ta e="T704" id="Seg_7449" s="T703">PTCL</ta>
            <ta e="T705" id="Seg_7450" s="T704">people.[NOM.SG]</ta>
            <ta e="T707" id="Seg_7451" s="T706">big.[NOM.SG]</ta>
            <ta e="T708" id="Seg_7452" s="T707">people.[NOM.SG]</ta>
            <ta e="T709" id="Seg_7453" s="T708">be-PST-3PL</ta>
            <ta e="T711" id="Seg_7454" s="T710">I.GEN</ta>
            <ta e="T712" id="Seg_7455" s="T711">people-NOM/GEN/ACC.1SG</ta>
            <ta e="T713" id="Seg_7456" s="T712">be-PST-3PL</ta>
            <ta e="T715" id="Seg_7457" s="T714">and</ta>
            <ta e="T716" id="Seg_7458" s="T715">where.to</ta>
            <ta e="T719" id="Seg_7459" s="T718">where.to</ta>
            <ta e="T720" id="Seg_7460" s="T719">where.to</ta>
            <ta e="T722" id="Seg_7461" s="T721">this-PL</ta>
            <ta e="T723" id="Seg_7462" s="T722">cry-RES-PRS-3PL</ta>
            <ta e="T724" id="Seg_7463" s="T723">cry-RES-PRS-3PL</ta>
            <ta e="T726" id="Seg_7464" s="T725">self</ta>
            <ta e="T727" id="Seg_7465" s="T726">house-ABL</ta>
            <ta e="T728" id="Seg_7466" s="T727">this-ACC.PL</ta>
            <ta e="T729" id="Seg_7467" s="T728">PTCL</ta>
            <ta e="T730" id="Seg_7468" s="T729">take-PST-3PL</ta>
            <ta e="T732" id="Seg_7469" s="T731">and</ta>
            <ta e="T733" id="Seg_7470" s="T732">what.[NOM.SG]</ta>
            <ta e="T734" id="Seg_7471" s="T733">take-PST-3PL</ta>
            <ta e="T736" id="Seg_7472" s="T735">so</ta>
            <ta e="T737" id="Seg_7473" s="T736">what.[NOM.SG]</ta>
            <ta e="T739" id="Seg_7474" s="T738">scold-DES-PST-3PL</ta>
            <ta e="T741" id="Seg_7475" s="T740">and</ta>
            <ta e="T742" id="Seg_7476" s="T741">PTCL</ta>
            <ta e="T743" id="Seg_7477" s="T742">%%</ta>
            <ta e="T744" id="Seg_7478" s="T743">PTCL</ta>
            <ta e="T745" id="Seg_7479" s="T744">say-RES-PRS-3PL</ta>
            <ta e="T747" id="Seg_7480" s="T746">look</ta>
            <ta e="T748" id="Seg_7481" s="T747">this-ACC.PL</ta>
            <ta e="T749" id="Seg_7482" s="T748">and</ta>
            <ta e="T750" id="Seg_7483" s="T749">take-PST-3PL</ta>
            <ta e="T752" id="Seg_7484" s="T751">and</ta>
            <ta e="T753" id="Seg_7485" s="T752">where.to</ta>
            <ta e="T755" id="Seg_7486" s="T754">well</ta>
            <ta e="T756" id="Seg_7487" s="T755">I.NOM</ta>
            <ta e="T757" id="Seg_7488" s="T756">know-1SG</ta>
            <ta e="T758" id="Seg_7489" s="T757">where.to</ta>
            <ta e="T760" id="Seg_7490" s="T759">I.NOM</ta>
            <ta e="T761" id="Seg_7491" s="T760">PTCL</ta>
            <ta e="T762" id="Seg_7492" s="T761">NEG</ta>
            <ta e="T763" id="Seg_7493" s="T762">know-1SG</ta>
            <ta e="T767" id="Seg_7494" s="T766">go-OPT.DU/PL-1DU</ta>
            <ta e="T768" id="Seg_7495" s="T767">mushroom-PL</ta>
            <ta e="T769" id="Seg_7496" s="T768">take-INF.LAT</ta>
            <ta e="T771" id="Seg_7497" s="T770">mushroom.[NOM.SG]</ta>
            <ta e="T772" id="Seg_7498" s="T771">beautiful.[NOM.SG]</ta>
            <ta e="T774" id="Seg_7499" s="T773">and</ta>
            <ta e="T775" id="Seg_7500" s="T774">what.[NOM.SG]</ta>
            <ta e="T777" id="Seg_7501" s="T776">so</ta>
            <ta e="T779" id="Seg_7502" s="T778">oh</ta>
            <ta e="T780" id="Seg_7503" s="T779">so</ta>
            <ta e="T781" id="Seg_7504" s="T780">what.[NOM.SG]</ta>
            <ta e="T783" id="Seg_7505" s="T782">and</ta>
            <ta e="T784" id="Seg_7506" s="T783">I.NOM</ta>
            <ta e="T785" id="Seg_7507" s="T784">then</ta>
            <ta e="T786" id="Seg_7508" s="T785">boil-FUT-1SG</ta>
            <ta e="T787" id="Seg_7509" s="T786">eat-INF.LAT</ta>
            <ta e="T788" id="Seg_7510" s="T787">want-FUT-1SG</ta>
            <ta e="T790" id="Seg_7511" s="T789">and</ta>
            <ta e="T791" id="Seg_7512" s="T790">you.NOM</ta>
            <ta e="T792" id="Seg_7513" s="T791">eat-FUT-2SG</ta>
            <ta e="T794" id="Seg_7514" s="T793">good</ta>
            <ta e="T795" id="Seg_7515" s="T794">this.[NOM.SG]</ta>
            <ta e="T801" id="Seg_7516" s="T800">I.NOM</ta>
            <ta e="T802" id="Seg_7517" s="T801">this-LAT</ta>
            <ta e="T803" id="Seg_7518" s="T802">give-FUT-1SG</ta>
            <ta e="T804" id="Seg_7519" s="T803">there</ta>
            <ta e="T805" id="Seg_7520" s="T804">%%-PL</ta>
            <ta e="T807" id="Seg_7521" s="T806">and</ta>
            <ta e="T808" id="Seg_7522" s="T807">this.[NOM.SG]</ta>
            <ta e="T809" id="Seg_7523" s="T808">I.LAT</ta>
            <ta e="T810" id="Seg_7524" s="T809">a.few</ta>
            <ta e="T811" id="Seg_7525" s="T810">a.few</ta>
            <ta e="T812" id="Seg_7526" s="T811">give-PRS-3SG.O</ta>
            <ta e="T816" id="Seg_7527" s="T815">well</ta>
            <ta e="T817" id="Seg_7528" s="T816">go-OPT.DU/PL-1DU</ta>
            <ta e="T818" id="Seg_7529" s="T817">I-INS</ta>
            <ta e="T820" id="Seg_7530" s="T819">where.to</ta>
            <ta e="T822" id="Seg_7531" s="T821">and</ta>
            <ta e="T823" id="Seg_7532" s="T822">there</ta>
            <ta e="T825" id="Seg_7533" s="T824">and</ta>
            <ta e="T826" id="Seg_7534" s="T825">mother-NOM/GEN/ACC.2SG</ta>
            <ta e="T828" id="Seg_7535" s="T827">mother-NOM/GEN/ACC.2SG</ta>
            <ta e="T829" id="Seg_7536" s="T828">JUSS</ta>
            <ta e="T831" id="Seg_7537" s="T830">NEG</ta>
            <ta e="T833" id="Seg_7538" s="T832">NEG</ta>
            <ta e="T834" id="Seg_7539" s="T833">take-FUT-1SG</ta>
            <ta e="T835" id="Seg_7540" s="T834">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T837" id="Seg_7541" s="T836">and</ta>
            <ta e="T838" id="Seg_7542" s="T837">we.NOM</ta>
            <ta e="T839" id="Seg_7543" s="T838">two-COLL</ta>
            <ta e="T840" id="Seg_7544" s="T839">go-OPT.DU/PL-1DU</ta>
            <ta e="T842" id="Seg_7545" s="T841">and</ta>
            <ta e="T843" id="Seg_7546" s="T842">where.to</ta>
            <ta e="T845" id="Seg_7547" s="T844">and</ta>
            <ta e="T846" id="Seg_7548" s="T845">there</ta>
            <ta e="T847" id="Seg_7549" s="T846">there</ta>
            <ta e="T849" id="Seg_7550" s="T848">where</ta>
            <ta e="T850" id="Seg_7551" s="T849">people.[NOM.SG]</ta>
            <ta e="T852" id="Seg_7552" s="T851">and</ta>
            <ta e="T853" id="Seg_7553" s="T852">what.[NOM.SG]</ta>
            <ta e="T854" id="Seg_7554" s="T853">this.[NOM.SG]</ta>
            <ta e="T855" id="Seg_7555" s="T854">there</ta>
            <ta e="T857" id="Seg_7556" s="T856">what.[NOM.SG]=INDEF</ta>
            <ta e="T858" id="Seg_7557" s="T857">what=INDEF</ta>
            <ta e="T859" id="Seg_7558" s="T858">tell-FUT-3PL</ta>
            <ta e="T861" id="Seg_7559" s="T860">and</ta>
            <ta e="T865" id="Seg_7560" s="T864">tell-FUT-3PL</ta>
            <ta e="T866" id="Seg_7561" s="T865">what=INDEF</ta>
            <ta e="T868" id="Seg_7562" s="T867">and</ta>
            <ta e="T869" id="Seg_7563" s="T868">we.NOM</ta>
            <ta e="T870" id="Seg_7564" s="T869">I.NOM</ta>
            <ta e="T871" id="Seg_7565" s="T870">what.[NOM.SG]=INDEF</ta>
            <ta e="T872" id="Seg_7566" s="T871">NEG</ta>
            <ta e="T873" id="Seg_7567" s="T872">tell-FUT-1SG</ta>
            <ta e="T875" id="Seg_7568" s="T874">and</ta>
            <ta e="T876" id="Seg_7569" s="T875">why</ta>
            <ta e="T877" id="Seg_7570" s="T876">NEG</ta>
            <ta e="T878" id="Seg_7571" s="T877">tell-FUT-2SG</ta>
            <ta e="T880" id="Seg_7572" s="T879">no</ta>
            <ta e="T882" id="Seg_7573" s="T881">if</ta>
            <ta e="T883" id="Seg_7574" s="T882">tell-INF.LAT</ta>
            <ta e="T885" id="Seg_7575" s="T884">bad.[NOM.SG]</ta>
            <ta e="T886" id="Seg_7576" s="T885">people.[NOM.SG]</ta>
            <ta e="T887" id="Seg_7577" s="T886">can-FUT-1SG</ta>
            <ta e="T889" id="Seg_7578" s="T888">why</ta>
            <ta e="T890" id="Seg_7579" s="T889">so</ta>
            <ta e="T892" id="Seg_7580" s="T891">no</ta>
            <ta e="T893" id="Seg_7581" s="T892">good</ta>
            <ta e="T894" id="Seg_7582" s="T893">people.[NOM.SG]</ta>
            <ta e="T896" id="Seg_7583" s="T895">no</ta>
            <ta e="T897" id="Seg_7584" s="T896">bad.[NOM.SG]</ta>
            <ta e="T898" id="Seg_7585" s="T897">people.[NOM.SG]</ta>
            <ta e="T900" id="Seg_7586" s="T899">well</ta>
            <ta e="T901" id="Seg_7587" s="T900">I.NOM</ta>
            <ta e="T902" id="Seg_7588" s="T901">little</ta>
            <ta e="T903" id="Seg_7589" s="T902">disappear-FUT-1SG</ta>
            <ta e="T904" id="Seg_7590" s="T903">you.DAT</ta>
            <ta e="T908" id="Seg_7591" s="T907">I.GEN</ta>
            <ta e="T909" id="Seg_7592" s="T908">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T910" id="Seg_7593" s="T909">you.GEN</ta>
            <ta e="T911" id="Seg_7594" s="T910">father-NOM/GEN/ACC.2SG</ta>
            <ta e="T912" id="Seg_7595" s="T911">fight-RES-3PL</ta>
            <ta e="T914" id="Seg_7596" s="T913">why</ta>
            <ta e="T915" id="Seg_7597" s="T914">this.[NOM.SG]</ta>
            <ta e="T916" id="Seg_7598" s="T915">fight-RES-3PL</ta>
            <ta e="T918" id="Seg_7599" s="T917">so</ta>
            <ta e="T919" id="Seg_7600" s="T918">this.[NOM.SG]</ta>
            <ta e="T920" id="Seg_7601" s="T919">say-RES-3SG.O</ta>
            <ta e="T921" id="Seg_7602" s="T920">you.GEN</ta>
            <ta e="T922" id="Seg_7603" s="T921">father-NOM/GEN/ACC.2SG</ta>
            <ta e="T924" id="Seg_7604" s="T922">say-RES-3SG.O</ta>
            <ta e="T925" id="Seg_7605" s="T924">I.GEN</ta>
            <ta e="T926" id="Seg_7606" s="T925">boy-NOM/GEN/ACC.1SG</ta>
            <ta e="T927" id="Seg_7607" s="T926">good</ta>
            <ta e="T928" id="Seg_7608" s="T927">beautiful.[NOM.SG]</ta>
            <ta e="T930" id="Seg_7609" s="T929">and</ta>
            <ta e="T931" id="Seg_7610" s="T930">you.GEN</ta>
            <ta e="T932" id="Seg_7611" s="T931">girl.[NOM.SG]</ta>
            <ta e="T933" id="Seg_7612" s="T932">NEG</ta>
            <ta e="T934" id="Seg_7613" s="T933">good</ta>
            <ta e="T936" id="Seg_7614" s="T935">and</ta>
            <ta e="T937" id="Seg_7615" s="T936">I.GEN</ta>
            <ta e="T938" id="Seg_7616" s="T937">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T940" id="Seg_7617" s="T938">say-RES-3SG.O</ta>
            <ta e="T941" id="Seg_7618" s="T940">oh</ta>
            <ta e="T942" id="Seg_7619" s="T941">I.GEN</ta>
            <ta e="T943" id="Seg_7620" s="T942">daughter-NOM/GEN/ACC.1SG</ta>
            <ta e="T944" id="Seg_7621" s="T943">good</ta>
            <ta e="T946" id="Seg_7622" s="T945">and</ta>
            <ta e="T947" id="Seg_7623" s="T946">beautiful.[NOM.SG]</ta>
            <ta e="T949" id="Seg_7624" s="T948">and</ta>
            <ta e="T950" id="Seg_7625" s="T949">PTCL</ta>
            <ta e="T951" id="Seg_7626" s="T950">what.[NOM.SG]</ta>
            <ta e="T952" id="Seg_7627" s="T951">PTCL</ta>
            <ta e="T953" id="Seg_7628" s="T952">what.[NOM.SG]</ta>
            <ta e="T954" id="Seg_7629" s="T953">say-RES-3SG.O</ta>
            <ta e="T956" id="Seg_7630" s="T955">and</ta>
            <ta e="T957" id="Seg_7631" s="T956">you.GEN</ta>
            <ta e="T958" id="Seg_7632" s="T957">son-NOM/GEN/ACC.2SG</ta>
            <ta e="T959" id="Seg_7633" s="T958">NEG</ta>
            <ta e="T960" id="Seg_7634" s="T959">good.[NOM.SG]</ta>
            <ta e="T962" id="Seg_7635" s="T961">what.[NOM.SG]=INDEF</ta>
            <ta e="T963" id="Seg_7636" s="T962">NEG</ta>
            <ta e="T964" id="Seg_7637" s="T963">tell-RES-3SG.O</ta>
            <ta e="T965" id="Seg_7638" s="T964">I.LAT</ta>
            <ta e="T967" id="Seg_7639" s="T966">and</ta>
            <ta e="T968" id="Seg_7640" s="T967">I.GEN</ta>
            <ta e="T969" id="Seg_7641" s="T968">daughter-NOM/GEN/ACC.1SG</ta>
            <ta e="T970" id="Seg_7642" s="T969">PTCL</ta>
            <ta e="T971" id="Seg_7643" s="T970">what.[NOM.SG]</ta>
            <ta e="T972" id="Seg_7644" s="T971">PTCL</ta>
            <ta e="T974" id="Seg_7645" s="T973">you.DAT</ta>
            <ta e="T975" id="Seg_7646" s="T974">always</ta>
            <ta e="T976" id="Seg_7647" s="T975">tell-RES-PRS-3SG.O</ta>
            <ta e="T978" id="Seg_7648" s="T977">look</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_7649" s="T1">хороший</ta>
            <ta e="T3" id="Seg_7650" s="T2">человек.[NOM.SG]</ta>
            <ta e="T4" id="Seg_7651" s="T3">мочь-PST.[3SG]</ta>
            <ta e="T5" id="Seg_7652" s="T4">умереть-RES-PST.[3SG]</ta>
            <ta e="T7" id="Seg_7653" s="T6">хороший</ta>
            <ta e="T8" id="Seg_7654" s="T7">умереть-RES-PST.[3SG]</ta>
            <ta e="T10" id="Seg_7655" s="T9">а</ta>
            <ta e="T11" id="Seg_7656" s="T10">этот.[NOM.SG]</ta>
            <ta e="T12" id="Seg_7657" s="T11">плохой.[NOM.SG]</ta>
            <ta e="T13" id="Seg_7658" s="T12">человек.[NOM.SG]</ta>
            <ta e="T14" id="Seg_7659" s="T13">один.[NOM.SG]</ta>
            <ta e="T17" id="Seg_7660" s="T16">умереть-RES-PST-3SG.O</ta>
            <ta e="T19" id="Seg_7661" s="T18">а</ta>
            <ta e="T20" id="Seg_7662" s="T19">хороший</ta>
            <ta e="T21" id="Seg_7663" s="T20">человек.[NOM.SG]</ta>
            <ta e="T22" id="Seg_7664" s="T21">хороший</ta>
            <ta e="T23" id="Seg_7665" s="T22">умереть-RES-PST.[3SG]</ta>
            <ta e="T25" id="Seg_7666" s="T24">вот</ta>
            <ta e="T27" id="Seg_7667" s="T26">я.GEN</ta>
            <ta e="T28" id="Seg_7668" s="T27">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T29" id="Seg_7669" s="T28">хороший</ta>
            <ta e="T30" id="Seg_7670" s="T29">жить-PST.[3SG]</ta>
            <ta e="T32" id="Seg_7671" s="T31">тогда</ta>
            <ta e="T33" id="Seg_7672" s="T32">плохой.[NOM.SG]</ta>
            <ta e="T35" id="Seg_7673" s="T34">сильно</ta>
            <ta e="T36" id="Seg_7674" s="T35">плохой.[NOM.SG]</ta>
            <ta e="T37" id="Seg_7675" s="T36">жить-PST.[3SG]</ta>
            <ta e="T39" id="Seg_7676" s="T38">тогда</ta>
            <ta e="T42" id="Seg_7677" s="T41">PTCL</ta>
            <ta e="T43" id="Seg_7678" s="T42">жить-PST.[3SG]</ta>
            <ta e="T44" id="Seg_7679" s="T43">хороший</ta>
            <ta e="T46" id="Seg_7680" s="T45">тогда</ta>
            <ta e="T47" id="Seg_7681" s="T46">умереть-RES-PST.[3SG]</ta>
            <ta e="T49" id="Seg_7682" s="T48">ну</ta>
            <ta e="T52" id="Seg_7683" s="T51">я.NOM</ta>
            <ta e="T53" id="Seg_7684" s="T52">жить-PST-1SG</ta>
            <ta e="T54" id="Seg_7685" s="T53">сильно</ta>
            <ta e="T55" id="Seg_7686" s="T54">плохой.[NOM.SG]</ta>
            <ta e="T57" id="Seg_7687" s="T56">сильно</ta>
            <ta e="T58" id="Seg_7688" s="T57">плохой.[NOM.SG]</ta>
            <ta e="T59" id="Seg_7689" s="T58">жить-PST-1SG</ta>
            <ta e="T61" id="Seg_7690" s="T60">я.NOM</ta>
            <ta e="T62" id="Seg_7691" s="T61">хлеб-NOM/GEN/ACC.1SG</ta>
            <ta e="T63" id="Seg_7692" s="T62">NEG.EX.[3SG]</ta>
            <ta e="T65" id="Seg_7693" s="T64">NEG.EX.[3SG]</ta>
            <ta e="T66" id="Seg_7694" s="T65">хлеб.[NOM.SG]</ta>
            <ta e="T67" id="Seg_7695" s="T66">я.NOM</ta>
            <ta e="T68" id="Seg_7696" s="T67">и</ta>
            <ta e="T69" id="Seg_7697" s="T68">я.NOM</ta>
            <ta e="T70" id="Seg_7698" s="T69">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T71" id="Seg_7699" s="T70">что-что</ta>
            <ta e="T72" id="Seg_7700" s="T71">есть-PST-1SG</ta>
            <ta e="T74" id="Seg_7701" s="T73">тогда=%%</ta>
            <ta e="T75" id="Seg_7702" s="T74">хороший</ta>
            <ta e="T76" id="Seg_7703" s="T75">жить-PST-1SG</ta>
            <ta e="T78" id="Seg_7704" s="T77">PTCL</ta>
            <ta e="T81" id="Seg_7705" s="T80">тогда-%%</ta>
            <ta e="T82" id="Seg_7706" s="T81">немного</ta>
            <ta e="T83" id="Seg_7707" s="T82">исчезнуть-PST-1SG</ta>
            <ta e="T85" id="Seg_7708" s="T84">жить-PST-1SG</ta>
            <ta e="T86" id="Seg_7709" s="T85">бедный-бедный-бедный-бедный</ta>
            <ta e="T87" id="Seg_7710" s="T86">дочь-NOM/GEN/ACC.1SG</ta>
            <ta e="T88" id="Seg_7711" s="T87">дать-HAB</ta>
            <ta e="T89" id="Seg_7712" s="T88">дать-HAB</ta>
            <ta e="T90" id="Seg_7713" s="T89">дать-HAB-PST-1SG</ta>
            <ta e="T92" id="Seg_7714" s="T91">и</ta>
            <ta e="T93" id="Seg_7715" s="T92">дочь-COM</ta>
            <ta e="T94" id="Seg_7716" s="T93">сильно</ta>
            <ta e="T95" id="Seg_7717" s="T94">плохой.[NOM.SG]</ta>
            <ta e="T96" id="Seg_7718" s="T95">жить-PST-1SG</ta>
            <ta e="T98" id="Seg_7719" s="T97">плакать-RES-1SG</ta>
            <ta e="T100" id="Seg_7720" s="T99">и</ta>
            <ta e="T102" id="Seg_7721" s="T101">идти-FUT-1SG</ta>
            <ta e="T103" id="Seg_7722" s="T102">и</ta>
            <ta e="T104" id="Seg_7723" s="T103">прийти</ta>
            <ta e="T105" id="Seg_7724" s="T104">прийти-RES-1SG</ta>
            <ta e="T107" id="Seg_7725" s="T106">жить-INF.LAT</ta>
            <ta e="T108" id="Seg_7726" s="T107">сильно</ta>
            <ta e="T109" id="Seg_7727" s="T108">плохой.[NOM.SG]</ta>
            <ta e="T111" id="Seg_7728" s="T110">куда</ta>
            <ta e="T112" id="Seg_7729" s="T111">я.LAT</ta>
            <ta e="T113" id="Seg_7730" s="T112">идти-INF.LAT</ta>
            <ta e="T115" id="Seg_7731" s="T114">куда=INDEF</ta>
            <ta e="T116" id="Seg_7732" s="T115">NEG</ta>
            <ta e="T117" id="Seg_7733" s="T116">идти-FUT-1SG</ta>
            <ta e="T118" id="Seg_7734" s="T117">лучше</ta>
            <ta e="T119" id="Seg_7735" s="T118">умереть-RES-PST-1SG</ta>
            <ta e="T123" id="Seg_7736" s="T122">PTCL</ta>
            <ta e="T124" id="Seg_7737" s="T123">люди.[NOM.SG]</ta>
            <ta e="T125" id="Seg_7738" s="T124">хороший</ta>
            <ta e="T126" id="Seg_7739" s="T125">жить-PRS-3PL</ta>
            <ta e="T128" id="Seg_7740" s="T127">а</ta>
            <ta e="T129" id="Seg_7741" s="T128">тогда</ta>
            <ta e="T130" id="Seg_7742" s="T129">немного</ta>
            <ta e="T131" id="Seg_7743" s="T130">исчезнуть-PRS</ta>
            <ta e="T133" id="Seg_7744" s="T132">куда=INDEF</ta>
            <ta e="T135" id="Seg_7745" s="T134">а</ta>
            <ta e="T136" id="Seg_7746" s="T135">тогда</ta>
            <ta e="T137" id="Seg_7747" s="T136">почему</ta>
            <ta e="T138" id="Seg_7748" s="T137">здесь</ta>
            <ta e="T139" id="Seg_7749" s="T138">же</ta>
            <ta e="T140" id="Seg_7750" s="T139">прийти-FUT-3SG</ta>
            <ta e="T142" id="Seg_7751" s="T141">о</ta>
            <ta e="T143" id="Seg_7752" s="T142">почему</ta>
            <ta e="T144" id="Seg_7753" s="T143">так</ta>
            <ta e="T145" id="Seg_7754" s="T144">мочь-FRQ-FUT-2SG</ta>
            <ta e="T147" id="Seg_7755" s="T146">и</ta>
            <ta e="T148" id="Seg_7756" s="T147">что.[NOM.SG]</ta>
            <ta e="T150" id="Seg_7757" s="T149">так</ta>
            <ta e="T152" id="Seg_7758" s="T151">прийти-FUT-3SG</ta>
            <ta e="T153" id="Seg_7759" s="T152">здесь</ta>
            <ta e="T155" id="Seg_7760" s="T154">а</ta>
            <ta e="T156" id="Seg_7761" s="T155">здесь</ta>
            <ta e="T157" id="Seg_7762" s="T156">прийти-FUT-3SG</ta>
            <ta e="T159" id="Seg_7763" s="T158">а</ta>
            <ta e="T160" id="Seg_7764" s="T159">ты.NOM</ta>
            <ta e="T161" id="Seg_7765" s="T160">что.[NOM.SG]</ta>
            <ta e="T162" id="Seg_7766" s="T161">NEG</ta>
            <ta e="T163" id="Seg_7767" s="T162">идти-FUT-2SG</ta>
            <ta e="T165" id="Seg_7768" s="T164">нет</ta>
            <ta e="T167" id="Seg_7769" s="T166">я.NOM</ta>
            <ta e="T168" id="Seg_7770" s="T167">NEG</ta>
            <ta e="T169" id="Seg_7771" s="T168">идти-FUT-1SG</ta>
            <ta e="T171" id="Seg_7772" s="T170">я.NOM</ta>
            <ta e="T172" id="Seg_7773" s="T171">тот-LOC</ta>
            <ta e="T173" id="Seg_7774" s="T172">мочь-FUT-1SG</ta>
            <ta e="T175" id="Seg_7775" s="T174">вот</ta>
            <ta e="T179" id="Seg_7776" s="T178">что=INDEF</ta>
            <ta e="T180" id="Seg_7777" s="T179">кипятить-INF.LAT</ta>
            <ta e="T181" id="Seg_7778" s="T180">надо</ta>
            <ta e="T183" id="Seg_7779" s="T182">я.NOM</ta>
            <ta e="T184" id="Seg_7780" s="T183">есть-INF.LAT</ta>
            <ta e="T185" id="Seg_7781" s="T184">мочь-FUT-1SG</ta>
            <ta e="T191" id="Seg_7782" s="T190">что.[NOM.SG]</ta>
            <ta e="T192" id="Seg_7783" s="T191">кипятить-PRS-2SG</ta>
            <ta e="T194" id="Seg_7784" s="T193">мясо.[NOM.SG]</ta>
            <ta e="T196" id="Seg_7785" s="T195">почему</ta>
            <ta e="T197" id="Seg_7786" s="T196">мясо.[NOM.SG]</ta>
            <ta e="T199" id="Seg_7787" s="T198">я.NOM</ta>
            <ta e="T200" id="Seg_7788" s="T199">IRREAL</ta>
            <ta e="T201" id="Seg_7789" s="T200">%%-1SG</ta>
            <ta e="T202" id="Seg_7790" s="T201">съесть-INF</ta>
            <ta e="T204" id="Seg_7791" s="T203">а</ta>
            <ta e="T205" id="Seg_7792" s="T204">я.NOM</ta>
            <ta e="T206" id="Seg_7793" s="T205">IRREAL</ta>
            <ta e="T207" id="Seg_7794" s="T206">молоко.[NOM.SG]</ta>
            <ta e="T208" id="Seg_7795" s="T207">IRREAL</ta>
            <ta e="T210" id="Seg_7796" s="T209">вот</ta>
            <ta e="T211" id="Seg_7797" s="T210">хороший</ta>
            <ta e="T212" id="Seg_7798" s="T211">IRREAL</ta>
            <ta e="T213" id="Seg_7799" s="T212">есть-FUT-1SG</ta>
            <ta e="T215" id="Seg_7800" s="T214">почему</ta>
            <ta e="T216" id="Seg_7801" s="T215">молоко.[NOM.SG]</ta>
            <ta e="T218" id="Seg_7802" s="T217">молоко.[NOM.SG]</ta>
            <ta e="T219" id="Seg_7803" s="T218">так</ta>
            <ta e="T220" id="Seg_7804" s="T219">молоко.[NOM.SG]</ta>
            <ta e="T222" id="Seg_7805" s="T221">а</ta>
            <ta e="T223" id="Seg_7806" s="T222">мясо.[NOM.SG]</ta>
            <ta e="T224" id="Seg_7807" s="T223">хороший</ta>
            <ta e="T228" id="Seg_7808" s="T227">почему</ta>
            <ta e="T229" id="Seg_7809" s="T228">я.LAT</ta>
            <ta e="T230" id="Seg_7810" s="T229">NEG</ta>
            <ta e="T231" id="Seg_7811" s="T230">прийти-FUT-3SG</ta>
            <ta e="T233" id="Seg_7812" s="T232">а</ta>
            <ta e="T234" id="Seg_7813" s="T233">я.NOM</ta>
            <ta e="T235" id="Seg_7814" s="T234">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T236" id="Seg_7815" s="T235">плохой</ta>
            <ta e="T239" id="Seg_7816" s="T238">ты.DAT</ta>
            <ta e="T240" id="Seg_7817" s="T239">NEG</ta>
            <ta e="T241" id="Seg_7818" s="T240">послать-PRS-3SG.O</ta>
            <ta e="T243" id="Seg_7819" s="T242">почему</ta>
            <ta e="T244" id="Seg_7820" s="T243">так</ta>
            <ta e="T245" id="Seg_7821" s="T244">NEG</ta>
            <ta e="T247" id="Seg_7822" s="T246">ну</ta>
            <ta e="T248" id="Seg_7823" s="T247">сказать-RES-3SG.O</ta>
            <ta e="T249" id="Seg_7824" s="T248">что</ta>
            <ta e="T250" id="Seg_7825" s="T249">плохой</ta>
            <ta e="T251" id="Seg_7826" s="T250">мужчина-NOM/GEN.3SG</ta>
            <ta e="T252" id="Seg_7827" s="T251">ты.NOM</ta>
            <ta e="T254" id="Seg_7828" s="T253">почему</ta>
            <ta e="T255" id="Seg_7829" s="T254">так</ta>
            <ta e="T256" id="Seg_7830" s="T255">плохой</ta>
            <ta e="T257" id="Seg_7831" s="T256">мужчина.[NOM.SG]</ta>
            <ta e="T259" id="Seg_7832" s="T258">я.NOM</ta>
            <ta e="T260" id="Seg_7833" s="T259">хороший</ta>
            <ta e="T261" id="Seg_7834" s="T260">мужчина.[NOM.SG]</ta>
            <ta e="T263" id="Seg_7835" s="T262">что=INDEF</ta>
            <ta e="T264" id="Seg_7836" s="T263">ты.DAT</ta>
            <ta e="T265" id="Seg_7837" s="T264">PTCL</ta>
            <ta e="T266" id="Seg_7838" s="T265">IRREAL</ta>
            <ta e="T267" id="Seg_7839" s="T266">сказать-FUT-1SG</ta>
            <ta e="T268" id="Seg_7840" s="T267">сказать-FUT-1SG</ta>
            <ta e="T269" id="Seg_7841" s="T268">IRREAL</ta>
            <ta e="T272" id="Seg_7842" s="T271">сильно</ta>
            <ta e="T273" id="Seg_7843" s="T272">хороший</ta>
            <ta e="T274" id="Seg_7844" s="T273">стать-FUT-3SG</ta>
            <ta e="T276" id="Seg_7845" s="T275">ох</ta>
            <ta e="T277" id="Seg_7846" s="T276">ну</ta>
            <ta e="T278" id="Seg_7847" s="T277">так</ta>
            <ta e="T279" id="Seg_7848" s="T278">сказать-RES-3SG.O</ta>
            <ta e="T280" id="Seg_7849" s="T279">я.LAT</ta>
            <ta e="T282" id="Seg_7850" s="T281">я.NOM</ta>
            <ta e="T283" id="Seg_7851" s="T282">этот-ACC</ta>
            <ta e="T284" id="Seg_7852" s="T283">PTCL</ta>
            <ta e="T285" id="Seg_7853" s="T284">бить-FUT-1SG</ta>
            <ta e="T286" id="Seg_7854" s="T285">сильно</ta>
            <ta e="T288" id="Seg_7855" s="T287">нет</ta>
            <ta e="T289" id="Seg_7856" s="T288">бить-INF.LAT</ta>
            <ta e="T293" id="Seg_7857" s="T292">а.то</ta>
            <ta e="T294" id="Seg_7858" s="T293">этот.[NOM.SG]</ta>
            <ta e="T295" id="Seg_7859" s="T294">я.LAT</ta>
            <ta e="T296" id="Seg_7860" s="T295">бить-FUT-2SG</ta>
            <ta e="T297" id="Seg_7861" s="T296">тогда-%%</ta>
            <ta e="T301" id="Seg_7862" s="T300">я.NOM</ta>
            <ta e="T302" id="Seg_7863" s="T301">сильно</ta>
            <ta e="T303" id="Seg_7864" s="T302">водка.[NOM.SG]</ta>
            <ta e="T304" id="Seg_7865" s="T303">пить-PRS-1SG</ta>
            <ta e="T306" id="Seg_7866" s="T305">о</ta>
            <ta e="T307" id="Seg_7867" s="T306">почему</ta>
            <ta e="T308" id="Seg_7868" s="T307">так</ta>
            <ta e="T309" id="Seg_7869" s="T308">ты.NOM</ta>
            <ta e="T311" id="Seg_7870" s="T310">ну</ta>
            <ta e="T312" id="Seg_7871" s="T311">я.NOM</ta>
            <ta e="T313" id="Seg_7872" s="T312">плохой.[NOM.SG]</ta>
            <ta e="T314" id="Seg_7873" s="T313">мужчина.[NOM.SG]</ta>
            <ta e="T316" id="Seg_7874" s="T315">PTCL</ta>
            <ta e="T317" id="Seg_7875" s="T316">%%</ta>
            <ta e="T318" id="Seg_7876" s="T317">PTCL</ta>
            <ta e="T319" id="Seg_7877" s="T318">взять-FUT-1SG</ta>
            <ta e="T320" id="Seg_7878" s="T319">что-NOM/GEN/ACC.3SG</ta>
            <ta e="T322" id="Seg_7879" s="T321">а</ta>
            <ta e="T323" id="Seg_7880" s="T322">почему</ta>
            <ta e="T324" id="Seg_7881" s="T323">ты.DAT</ta>
            <ta e="T325" id="Seg_7882" s="T324">взять-FUT-2SG</ta>
            <ta e="T326" id="Seg_7883" s="T325">так</ta>
            <ta e="T327" id="Seg_7884" s="T326">я.NOM</ta>
            <ta e="T328" id="Seg_7885" s="T327">тогда=%%</ta>
            <ta e="T330" id="Seg_7886" s="T329">я.NOM</ta>
            <ta e="T331" id="Seg_7887" s="T330">тогда-%%</ta>
            <ta e="T332" id="Seg_7888" s="T331">взять-FUT-1SG</ta>
            <ta e="T334" id="Seg_7889" s="T333">а</ta>
            <ta e="T335" id="Seg_7890" s="T334">почему</ta>
            <ta e="T336" id="Seg_7891" s="T335">там</ta>
            <ta e="T337" id="Seg_7892" s="T336">так</ta>
            <ta e="T338" id="Seg_7893" s="T337">взять-FUT-2SG</ta>
            <ta e="T340" id="Seg_7894" s="T339">так</ta>
            <ta e="T341" id="Seg_7895" s="T340">вот</ta>
            <ta e="T342" id="Seg_7896" s="T341">взять-FUT-1SG</ta>
            <ta e="T343" id="Seg_7897" s="T342">я.NOM</ta>
            <ta e="T345" id="Seg_7898" s="T344">я.LAT</ta>
            <ta e="T346" id="Seg_7899" s="T345">тогда-%%</ta>
            <ta e="T347" id="Seg_7900" s="T346">NEG</ta>
            <ta e="T348" id="Seg_7901" s="T347">красивый.[NOM.SG]</ta>
            <ta e="T349" id="Seg_7902" s="T348">дом-LAT</ta>
            <ta e="T352" id="Seg_7903" s="T351">я.NOM</ta>
            <ta e="T353" id="Seg_7904" s="T352">там</ta>
            <ta e="T354" id="Seg_7905" s="T353">жить-FUT-1SG</ta>
            <ta e="T356" id="Seg_7906" s="T355">а</ta>
            <ta e="T357" id="Seg_7907" s="T356">тогда</ta>
            <ta e="T358" id="Seg_7908" s="T357">как</ta>
            <ta e="T359" id="Seg_7909" s="T358">прийти-FUT-1SG</ta>
            <ta e="T360" id="Seg_7910" s="T359">я.LAT</ta>
            <ta e="T361" id="Seg_7911" s="T360">люди.[NOM.SG]</ta>
            <ta e="T363" id="Seg_7912" s="T362">кто-NOM/GEN/ACC.3SG</ta>
            <ta e="T364" id="Seg_7913" s="T363">NEG</ta>
            <ta e="T366" id="Seg_7914" s="T365">NEG</ta>
            <ta e="T373" id="Seg_7915" s="T372">NEG</ta>
            <ta e="T374" id="Seg_7916" s="T373">взять-PRS-3SG.O</ta>
            <ta e="T376" id="Seg_7917" s="T375">PTCL</ta>
            <ta e="T377" id="Seg_7918" s="T376">люди.[NOM.SG]</ta>
            <ta e="T379" id="Seg_7919" s="T378">и</ta>
            <ta e="T380" id="Seg_7920" s="T379">NEG</ta>
            <ta e="T381" id="Seg_7921" s="T380">хороший</ta>
            <ta e="T382" id="Seg_7922" s="T381">мужчина.[NOM.SG]</ta>
            <ta e="T384" id="Seg_7923" s="T383">вот</ta>
            <ta e="T388" id="Seg_7924" s="T387">плохой.[NOM.SG]</ta>
            <ta e="T389" id="Seg_7925" s="T388">мужчина.[NOM.SG]</ta>
            <ta e="T390" id="Seg_7926" s="T389">хороший</ta>
            <ta e="T391" id="Seg_7927" s="T390">мужчина-GEN</ta>
            <ta e="T392" id="Seg_7928" s="T391">взять-PST.[3SG]</ta>
            <ta e="T393" id="Seg_7929" s="T392">рубашка.[NOM.SG]</ta>
            <ta e="T394" id="Seg_7930" s="T393">мясо.[NOM.SG]</ta>
            <ta e="T395" id="Seg_7931" s="T394">и</ta>
            <ta e="T396" id="Seg_7932" s="T395">PTCL</ta>
            <ta e="T397" id="Seg_7933" s="T396">что.[NOM.SG]</ta>
            <ta e="T398" id="Seg_7934" s="T397">PTCL</ta>
            <ta e="T400" id="Seg_7935" s="T399">деньги-NOM/GEN.3SG</ta>
            <ta e="T401" id="Seg_7936" s="T400">мочь-PST.[3SG]</ta>
            <ta e="T402" id="Seg_7937" s="T401">этот.[NOM.SG]</ta>
            <ta e="T403" id="Seg_7938" s="T402">мужчина-GEN</ta>
            <ta e="T405" id="Seg_7939" s="T404">PTCL</ta>
            <ta e="T406" id="Seg_7940" s="T405">взять-PST.[3SG]</ta>
            <ta e="T408" id="Seg_7941" s="T407">что.[NOM.SG]</ta>
            <ta e="T409" id="Seg_7942" s="T408">ты.NOM</ta>
            <ta e="T410" id="Seg_7943" s="T409">взять-PST-2SG</ta>
            <ta e="T412" id="Seg_7944" s="T411">так</ta>
            <ta e="T413" id="Seg_7945" s="T412">я.NOM</ta>
            <ta e="T414" id="Seg_7946" s="T413">NEG.EX.[3SG]</ta>
            <ta e="T415" id="Seg_7947" s="T414">а</ta>
            <ta e="T416" id="Seg_7948" s="T415">ты.NOM</ta>
            <ta e="T417" id="Seg_7949" s="T416">быть-PRS.[3SG]</ta>
            <ta e="T420" id="Seg_7950" s="T419">я.NOM</ta>
            <ta e="T421" id="Seg_7951" s="T420">взять-PST-1SG</ta>
            <ta e="T423" id="Seg_7952" s="T422">а</ta>
            <ta e="T424" id="Seg_7953" s="T423">я.NOM</ta>
            <ta e="T425" id="Seg_7954" s="T424">ты.DAT</ta>
            <ta e="T426" id="Seg_7955" s="T425">NEG</ta>
            <ta e="T427" id="Seg_7956" s="T426">взять-FUT-1SG</ta>
            <ta e="T429" id="Seg_7957" s="T428">я.NOM</ta>
            <ta e="T430" id="Seg_7958" s="T429">ты.DAT</ta>
            <ta e="T431" id="Seg_7959" s="T430">взять-FUT-1SG</ta>
            <ta e="T433" id="Seg_7960" s="T432">а</ta>
            <ta e="T434" id="Seg_7961" s="T433">почему</ta>
            <ta e="T435" id="Seg_7962" s="T434">%%</ta>
            <ta e="T436" id="Seg_7963" s="T435">я.LAT</ta>
            <ta e="T437" id="Seg_7964" s="T436">взять-FUT-2SG</ta>
            <ta e="T439" id="Seg_7965" s="T438">так</ta>
            <ta e="T440" id="Seg_7966" s="T439">%%</ta>
            <ta e="T441" id="Seg_7967" s="T440">зачем</ta>
            <ta e="T442" id="Seg_7968" s="T441">взять-PST-2SG</ta>
            <ta e="T444" id="Seg_7969" s="T443">я.NOM</ta>
            <ta e="T445" id="Seg_7970" s="T444">нести-DUR.[3SG]</ta>
            <ta e="T446" id="Seg_7971" s="T445">и</ta>
            <ta e="T447" id="Seg_7972" s="T446">PTCL</ta>
            <ta e="T448" id="Seg_7973" s="T447">что-ACC</ta>
            <ta e="T449" id="Seg_7974" s="T448">PTCL</ta>
            <ta e="T451" id="Seg_7975" s="T450">а</ta>
            <ta e="T452" id="Seg_7976" s="T451">я.NOM</ta>
            <ta e="T453" id="Seg_7977" s="T452">плохой.[NOM.SG]</ta>
            <ta e="T454" id="Seg_7978" s="T453">дом-LAT</ta>
            <ta e="T455" id="Seg_7979" s="T454">ты.DAT</ta>
            <ta e="T456" id="Seg_7980" s="T455">сидеть-DUR.[3SG]</ta>
            <ta e="T459" id="Seg_7981" s="T458">вот</ta>
            <ta e="T463" id="Seg_7982" s="T462">сесть-IMP.2SG</ta>
            <ta e="T464" id="Seg_7983" s="T463">я.NOM-COM</ta>
            <ta e="T465" id="Seg_7984" s="T464">есть-INF.LAT</ta>
            <ta e="T467" id="Seg_7985" s="T466">о</ta>
            <ta e="T479" id="Seg_7986" s="T478">сесть-IMP.2SG</ta>
            <ta e="T480" id="Seg_7987" s="T479">я.NOM-COM</ta>
            <ta e="T481" id="Seg_7988" s="T480">есть-INF.LAT</ta>
            <ta e="T483" id="Seg_7989" s="T482">и</ta>
            <ta e="T484" id="Seg_7990" s="T483">что.[NOM.SG]</ta>
            <ta e="T485" id="Seg_7991" s="T484">ты.NOM</ta>
            <ta e="T486" id="Seg_7992" s="T485">мочь-FUT-3SG.O</ta>
            <ta e="T488" id="Seg_7993" s="T487">и</ta>
            <ta e="T489" id="Seg_7994" s="T488">я.NOM</ta>
            <ta e="T490" id="Seg_7995" s="T489">мочь-FUT-3SG.O</ta>
            <ta e="T491" id="Seg_7996" s="T490">хлеб.[NOM.SG]</ta>
            <ta e="T492" id="Seg_7997" s="T491">мясо.[NOM.SG]</ta>
            <ta e="T493" id="Seg_7998" s="T492">и</ta>
            <ta e="T494" id="Seg_7999" s="T493">молоко.[NOM.SG]</ta>
            <ta e="T496" id="Seg_8000" s="T495">сахар-%%</ta>
            <ta e="T498" id="Seg_8001" s="T497">съесть-IMP.2SG.O</ta>
            <ta e="T500" id="Seg_8002" s="T499">так</ta>
            <ta e="T501" id="Seg_8003" s="T500">я.NOM</ta>
            <ta e="T502" id="Seg_8004" s="T501">IRREAL</ta>
            <ta e="T503" id="Seg_8005" s="T502">масло.[NOM.SG]</ta>
            <ta e="T504" id="Seg_8006" s="T503">есть-PRS-1SG</ta>
            <ta e="T505" id="Seg_8007" s="T504">IRREAL</ta>
            <ta e="T507" id="Seg_8008" s="T506">ну</ta>
            <ta e="T508" id="Seg_8009" s="T507">и</ta>
            <ta e="T509" id="Seg_8010" s="T508">масло.[NOM.SG]</ta>
            <ta e="T510" id="Seg_8011" s="T509">я.NOM</ta>
            <ta e="T511" id="Seg_8012" s="T510">дать-FUT-1SG</ta>
            <ta e="T512" id="Seg_8013" s="T511">ты.DAT</ta>
            <ta e="T513" id="Seg_8014" s="T512">есть-INF.LAT</ta>
            <ta e="T515" id="Seg_8015" s="T514">хороший</ta>
            <ta e="T516" id="Seg_8016" s="T515">только</ta>
            <ta e="T517" id="Seg_8017" s="T516">есть-EP-IMP.2SG</ta>
            <ta e="T519" id="Seg_8018" s="T518">тот.[NOM.SG]</ta>
            <ta e="T520" id="Seg_8019" s="T519">так</ta>
            <ta e="T521" id="Seg_8020" s="T520">я.NOM</ta>
            <ta e="T522" id="Seg_8021" s="T521">есть-FUT-1SG</ta>
            <ta e="T524" id="Seg_8022" s="T523">сильно</ta>
            <ta e="T525" id="Seg_8023" s="T524">сильно</ta>
            <ta e="T526" id="Seg_8024" s="T525">есть-FUT-1SG</ta>
            <ta e="T528" id="Seg_8025" s="T527">хороший</ta>
            <ta e="T529" id="Seg_8026" s="T528">есть-PRS-1SG</ta>
            <ta e="T535" id="Seg_8027" s="T534">почему</ta>
            <ta e="T536" id="Seg_8028" s="T535">ты.NOM</ta>
            <ta e="T537" id="Seg_8029" s="T536">я.NOM</ta>
            <ta e="T538" id="Seg_8030" s="T537">волосы-NOM/GEN/ACC.1SG</ta>
            <ta e="T539" id="Seg_8031" s="T538">PTCL</ta>
            <ta e="T540" id="Seg_8032" s="T539">сам</ta>
            <ta e="T541" id="Seg_8033" s="T540">рука-LAT</ta>
            <ta e="T542" id="Seg_8034" s="T541">взять-PST-2SG</ta>
            <ta e="T544" id="Seg_8035" s="T543">ведь</ta>
            <ta e="T545" id="Seg_8036" s="T544">ты.NOM</ta>
            <ta e="T546" id="Seg_8037" s="T545">рука-NOM/GEN/ACC.2SG</ta>
            <ta e="T547" id="Seg_8038" s="T546">большой.[NOM.SG]</ta>
            <ta e="T548" id="Seg_8039" s="T547">а</ta>
            <ta e="T549" id="Seg_8040" s="T548">я.GEN</ta>
            <ta e="T550" id="Seg_8041" s="T549">немного</ta>
            <ta e="T552" id="Seg_8042" s="T551">почему</ta>
            <ta e="T553" id="Seg_8043" s="T552">так</ta>
            <ta e="T554" id="Seg_8044" s="T553">стать-FUT-2SG</ta>
            <ta e="T556" id="Seg_8045" s="T555">плохой.[NOM.SG]</ta>
            <ta e="T558" id="Seg_8046" s="T557">NEG</ta>
            <ta e="T559" id="Seg_8047" s="T558">хороший</ta>
            <ta e="T560" id="Seg_8048" s="T559">так</ta>
            <ta e="T562" id="Seg_8049" s="T561">вот</ta>
            <ta e="T566" id="Seg_8050" s="T565">я.NOM</ta>
            <ta e="T567" id="Seg_8051" s="T566">сильно</ta>
            <ta e="T568" id="Seg_8052" s="T567">болеть-MOM-PST-1SG</ta>
            <ta e="T570" id="Seg_8053" s="T569">рука-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T571" id="Seg_8054" s="T570">болеть-PRS-3SG.O</ta>
            <ta e="T573" id="Seg_8055" s="T572">и</ta>
            <ta e="T574" id="Seg_8056" s="T573">нога-ACC.3SG</ta>
            <ta e="T575" id="Seg_8057" s="T574">PTCL</ta>
            <ta e="T576" id="Seg_8058" s="T575">болеть-DUR-PRS-3SG.O</ta>
            <ta e="T578" id="Seg_8059" s="T577">однако</ta>
            <ta e="T579" id="Seg_8060" s="T578">я.NOM</ta>
            <ta e="T580" id="Seg_8061" s="T579">умереть-RES-FACT-%%</ta>
            <ta e="T582" id="Seg_8062" s="T581">нет</ta>
            <ta e="T583" id="Seg_8063" s="T582">не</ta>
            <ta e="T584" id="Seg_8064" s="T583">надо</ta>
            <ta e="T585" id="Seg_8065" s="T584">умереть-RES-FACT-INF.LAT</ta>
            <ta e="T587" id="Seg_8066" s="T586">надо</ta>
            <ta e="T588" id="Seg_8067" s="T587">жить-INF.LAT</ta>
            <ta e="T590" id="Seg_8068" s="T589">жить-INF.LAT</ta>
            <ta e="T591" id="Seg_8069" s="T590">хороший</ta>
            <ta e="T593" id="Seg_8070" s="T592">а</ta>
            <ta e="T594" id="Seg_8071" s="T593">умереть-RES-FACT-INF.LAT</ta>
            <ta e="T595" id="Seg_8072" s="T594">плохой.[NOM.SG]</ta>
            <ta e="T597" id="Seg_8073" s="T596">сильно</ta>
            <ta e="T598" id="Seg_8074" s="T597">плохой.[NOM.SG]</ta>
            <ta e="T600" id="Seg_8075" s="T599">вот</ta>
            <ta e="T604" id="Seg_8076" s="T603">два.[NOM.SG]</ta>
            <ta e="T605" id="Seg_8077" s="T604">мальчик.[NOM.SG]</ta>
            <ta e="T606" id="Seg_8078" s="T605">жить-PST-3PL</ta>
            <ta e="T608" id="Seg_8079" s="T607">тогда=%%</ta>
            <ta e="T609" id="Seg_8080" s="T608">этот.[NOM.SG]</ta>
            <ta e="T610" id="Seg_8081" s="T609">мальчик.[NOM.SG]</ta>
            <ta e="T611" id="Seg_8082" s="T610">куда=INDEF</ta>
            <ta e="T612" id="Seg_8083" s="T611">немного</ta>
            <ta e="T613" id="Seg_8084" s="T612">исчезнуть-INF.LAT</ta>
            <ta e="T614" id="Seg_8085" s="T613">хотеть-FUT-3SG.O</ta>
            <ta e="T616" id="Seg_8086" s="T615">а</ta>
            <ta e="T617" id="Seg_8087" s="T616">один.[NOM.SG]</ta>
            <ta e="T618" id="Seg_8088" s="T617">дом-LOC-EP-NOM/GEN/ACC.1SG</ta>
            <ta e="T619" id="Seg_8089" s="T618">жить-DUR-PRS-3SG.O</ta>
            <ta e="T621" id="Seg_8090" s="T620">ну</ta>
            <ta e="T622" id="Seg_8091" s="T621">дать-FUT-3SG.O</ta>
            <ta e="T623" id="Seg_8092" s="T622">ты.DAT</ta>
            <ta e="T624" id="Seg_8093" s="T623">лошадь.[NOM.SG]</ta>
            <ta e="T625" id="Seg_8094" s="T624">я.LAT</ta>
            <ta e="T626" id="Seg_8095" s="T625">корова.[NOM.SG]</ta>
            <ta e="T628" id="Seg_8096" s="T627">нет</ta>
            <ta e="T629" id="Seg_8097" s="T628">ты.NOM</ta>
            <ta e="T630" id="Seg_8098" s="T629">взять-IMP.2SG.O</ta>
            <ta e="T631" id="Seg_8099" s="T630">лошадь.[NOM.SG]</ta>
            <ta e="T632" id="Seg_8100" s="T631">а</ta>
            <ta e="T633" id="Seg_8101" s="T632">я.NOM</ta>
            <ta e="T634" id="Seg_8102" s="T633">корова.[NOM.SG]</ta>
            <ta e="T636" id="Seg_8103" s="T635">о</ta>
            <ta e="T637" id="Seg_8104" s="T636">черт.[NOM.SG]</ta>
            <ta e="T639" id="Seg_8105" s="T638">почему</ta>
            <ta e="T640" id="Seg_8106" s="T639">так</ta>
            <ta e="T642" id="Seg_8107" s="T641">ну</ta>
            <ta e="T643" id="Seg_8108" s="T642">ты.NOM</ta>
            <ta e="T644" id="Seg_8109" s="T643">бродяга.[NOM.SG]</ta>
            <ta e="T646" id="Seg_8110" s="T645">черт-NOM/GEN/ACC.2SG</ta>
            <ta e="T648" id="Seg_8111" s="T647">почему</ta>
            <ta e="T649" id="Seg_8112" s="T648">так</ta>
            <ta e="T650" id="Seg_8113" s="T649">ругать-DES-PRS-2SG</ta>
            <ta e="T652" id="Seg_8114" s="T651">так</ta>
            <ta e="T653" id="Seg_8115" s="T652">надо</ta>
            <ta e="T654" id="Seg_8116" s="T653">так</ta>
            <ta e="T655" id="Seg_8117" s="T654">ругать-INF.LAT</ta>
            <ta e="T656" id="Seg_8118" s="T655">плохой.[NOM.SG]</ta>
            <ta e="T658" id="Seg_8119" s="T657">а</ta>
            <ta e="T659" id="Seg_8120" s="T658">я.LAT</ta>
            <ta e="T660" id="Seg_8121" s="T659">ведь</ta>
            <ta e="T661" id="Seg_8122" s="T660">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T662" id="Seg_8123" s="T661">дать-PST.[3SG]</ta>
            <ta e="T663" id="Seg_8124" s="T662">лошадь.[NOM.SG]</ta>
            <ta e="T664" id="Seg_8125" s="T663">а</ta>
            <ta e="T665" id="Seg_8126" s="T664">ты.DAT</ta>
            <ta e="T666" id="Seg_8127" s="T665">корова.[NOM.SG]</ta>
            <ta e="T668" id="Seg_8128" s="T667">сказать-PST.[3SG]</ta>
            <ta e="T669" id="Seg_8129" s="T668">ты.NOM</ta>
            <ta e="T670" id="Seg_8130" s="T669">взять-IMP.2SG.O</ta>
            <ta e="T671" id="Seg_8131" s="T670">лошадь.[NOM.SG]</ta>
            <ta e="T672" id="Seg_8132" s="T671">а</ta>
            <ta e="T673" id="Seg_8133" s="T672">ты.NOM</ta>
            <ta e="T674" id="Seg_8134" s="T673">взять-IMP.2SG.O</ta>
            <ta e="T675" id="Seg_8135" s="T674">корова.[NOM.SG]</ta>
            <ta e="T677" id="Seg_8136" s="T676">а</ta>
            <ta e="T678" id="Seg_8137" s="T677">дом.[NOM.SG]</ta>
            <ta e="T679" id="Seg_8138" s="T678">ты.DAT</ta>
            <ta e="T681" id="Seg_8139" s="T680">и</ta>
            <ta e="T682" id="Seg_8140" s="T681">дом.[NOM.SG]</ta>
            <ta e="T683" id="Seg_8141" s="T682">и</ta>
            <ta e="T684" id="Seg_8142" s="T683">корова.[NOM.SG]</ta>
            <ta e="T685" id="Seg_8143" s="T684">ты.DAT</ta>
            <ta e="T687" id="Seg_8144" s="T686">а</ta>
            <ta e="T688" id="Seg_8145" s="T687">сейчас</ta>
            <ta e="T689" id="Seg_8146" s="T688">корова.[NOM.SG]</ta>
            <ta e="T690" id="Seg_8147" s="T689">NEG</ta>
            <ta e="T691" id="Seg_8148" s="T690">взять-PRS-3SG.O</ta>
            <ta e="T700" id="Seg_8149" s="T699">PTCL</ta>
            <ta e="T701" id="Seg_8150" s="T700">%%</ta>
            <ta e="T702" id="Seg_8151" s="T701">PTCL</ta>
            <ta e="T704" id="Seg_8152" s="T703">PTCL</ta>
            <ta e="T705" id="Seg_8153" s="T704">люди.[NOM.SG]</ta>
            <ta e="T707" id="Seg_8154" s="T706">большой.[NOM.SG]</ta>
            <ta e="T708" id="Seg_8155" s="T707">люди.[NOM.SG]</ta>
            <ta e="T709" id="Seg_8156" s="T708">быть-PST-3PL</ta>
            <ta e="T711" id="Seg_8157" s="T710">я.GEN</ta>
            <ta e="T712" id="Seg_8158" s="T711">люди-NOM/GEN/ACC.1SG</ta>
            <ta e="T713" id="Seg_8159" s="T712">быть-PST-3PL</ta>
            <ta e="T715" id="Seg_8160" s="T714">и</ta>
            <ta e="T716" id="Seg_8161" s="T715">куда</ta>
            <ta e="T719" id="Seg_8162" s="T718">куда</ta>
            <ta e="T720" id="Seg_8163" s="T719">куда</ta>
            <ta e="T722" id="Seg_8164" s="T721">этот-PL</ta>
            <ta e="T723" id="Seg_8165" s="T722">плакать-RES-PRS-3PL</ta>
            <ta e="T724" id="Seg_8166" s="T723">плакать-RES-PRS-3PL</ta>
            <ta e="T726" id="Seg_8167" s="T725">сам</ta>
            <ta e="T727" id="Seg_8168" s="T726">дом-ABL</ta>
            <ta e="T728" id="Seg_8169" s="T727">этот-ACC.PL</ta>
            <ta e="T729" id="Seg_8170" s="T728">PTCL</ta>
            <ta e="T730" id="Seg_8171" s="T729">взять-PST-3PL</ta>
            <ta e="T732" id="Seg_8172" s="T731">а</ta>
            <ta e="T733" id="Seg_8173" s="T732">что.[NOM.SG]</ta>
            <ta e="T734" id="Seg_8174" s="T733">взять-PST-3PL</ta>
            <ta e="T736" id="Seg_8175" s="T735">так</ta>
            <ta e="T737" id="Seg_8176" s="T736">что.[NOM.SG]</ta>
            <ta e="T739" id="Seg_8177" s="T738">ругать-DES-PST-3PL</ta>
            <ta e="T741" id="Seg_8178" s="T740">и</ta>
            <ta e="T742" id="Seg_8179" s="T741">PTCL</ta>
            <ta e="T743" id="Seg_8180" s="T742">%%</ta>
            <ta e="T744" id="Seg_8181" s="T743">PTCL</ta>
            <ta e="T745" id="Seg_8182" s="T744">сказать-RES-PRS-3PL</ta>
            <ta e="T747" id="Seg_8183" s="T746">вот</ta>
            <ta e="T748" id="Seg_8184" s="T747">этот-ACC.PL</ta>
            <ta e="T749" id="Seg_8185" s="T748">и</ta>
            <ta e="T750" id="Seg_8186" s="T749">взять-PST-3PL</ta>
            <ta e="T752" id="Seg_8187" s="T751">а</ta>
            <ta e="T753" id="Seg_8188" s="T752">куда</ta>
            <ta e="T755" id="Seg_8189" s="T754">ну</ta>
            <ta e="T756" id="Seg_8190" s="T755">я.NOM</ta>
            <ta e="T757" id="Seg_8191" s="T756">знать-1SG</ta>
            <ta e="T758" id="Seg_8192" s="T757">куда</ta>
            <ta e="T760" id="Seg_8193" s="T759">я.NOM</ta>
            <ta e="T761" id="Seg_8194" s="T760">же</ta>
            <ta e="T762" id="Seg_8195" s="T761">NEG</ta>
            <ta e="T763" id="Seg_8196" s="T762">знать-1SG</ta>
            <ta e="T767" id="Seg_8197" s="T766">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T768" id="Seg_8198" s="T767">гриб-PL</ta>
            <ta e="T769" id="Seg_8199" s="T768">взять-INF.LAT</ta>
            <ta e="T771" id="Seg_8200" s="T770">гриб.[NOM.SG]</ta>
            <ta e="T772" id="Seg_8201" s="T771">красивый.[NOM.SG]</ta>
            <ta e="T774" id="Seg_8202" s="T773">а</ta>
            <ta e="T775" id="Seg_8203" s="T774">что.[NOM.SG]</ta>
            <ta e="T777" id="Seg_8204" s="T776">так</ta>
            <ta e="T779" id="Seg_8205" s="T778">о</ta>
            <ta e="T780" id="Seg_8206" s="T779">так</ta>
            <ta e="T781" id="Seg_8207" s="T780">что.[NOM.SG]</ta>
            <ta e="T783" id="Seg_8208" s="T782">а</ta>
            <ta e="T784" id="Seg_8209" s="T783">я.NOM</ta>
            <ta e="T785" id="Seg_8210" s="T784">тогда</ta>
            <ta e="T786" id="Seg_8211" s="T785">кипятить-FUT-1SG</ta>
            <ta e="T787" id="Seg_8212" s="T786">есть-INF.LAT</ta>
            <ta e="T788" id="Seg_8213" s="T787">хотеть-FUT-1SG</ta>
            <ta e="T790" id="Seg_8214" s="T789">и</ta>
            <ta e="T791" id="Seg_8215" s="T790">ты.NOM</ta>
            <ta e="T792" id="Seg_8216" s="T791">есть-FUT-2SG</ta>
            <ta e="T794" id="Seg_8217" s="T793">хороший</ta>
            <ta e="T795" id="Seg_8218" s="T794">этот.[NOM.SG]</ta>
            <ta e="T801" id="Seg_8219" s="T800">я.NOM</ta>
            <ta e="T802" id="Seg_8220" s="T801">этот-LAT</ta>
            <ta e="T803" id="Seg_8221" s="T802">дать-FUT-1SG</ta>
            <ta e="T804" id="Seg_8222" s="T803">вон</ta>
            <ta e="T805" id="Seg_8223" s="T804">%%-PL</ta>
            <ta e="T807" id="Seg_8224" s="T806">а</ta>
            <ta e="T808" id="Seg_8225" s="T807">этот.[NOM.SG]</ta>
            <ta e="T809" id="Seg_8226" s="T808">я.LAT</ta>
            <ta e="T810" id="Seg_8227" s="T809">немного</ta>
            <ta e="T811" id="Seg_8228" s="T810">немного</ta>
            <ta e="T812" id="Seg_8229" s="T811">дать-PRS-3SG.O</ta>
            <ta e="T816" id="Seg_8230" s="T815">ну</ta>
            <ta e="T817" id="Seg_8231" s="T816">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T818" id="Seg_8232" s="T817">я-INS</ta>
            <ta e="T820" id="Seg_8233" s="T819">куда</ta>
            <ta e="T822" id="Seg_8234" s="T821">и</ta>
            <ta e="T823" id="Seg_8235" s="T822">там</ta>
            <ta e="T825" id="Seg_8236" s="T824">а</ta>
            <ta e="T826" id="Seg_8237" s="T825">мать-NOM/GEN/ACC.2SG</ta>
            <ta e="T828" id="Seg_8238" s="T827">мать-NOM/GEN/ACC.2SG</ta>
            <ta e="T829" id="Seg_8239" s="T828">JUSS</ta>
            <ta e="T831" id="Seg_8240" s="T830">NEG</ta>
            <ta e="T833" id="Seg_8241" s="T832">NEG</ta>
            <ta e="T834" id="Seg_8242" s="T833">взять-FUT-1SG</ta>
            <ta e="T835" id="Seg_8243" s="T834">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T837" id="Seg_8244" s="T836">а</ta>
            <ta e="T838" id="Seg_8245" s="T837">мы.NOM</ta>
            <ta e="T839" id="Seg_8246" s="T838">два-COLL</ta>
            <ta e="T840" id="Seg_8247" s="T839">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T842" id="Seg_8248" s="T841">а</ta>
            <ta e="T843" id="Seg_8249" s="T842">куда</ta>
            <ta e="T845" id="Seg_8250" s="T844">и</ta>
            <ta e="T846" id="Seg_8251" s="T845">вон</ta>
            <ta e="T847" id="Seg_8252" s="T846">там</ta>
            <ta e="T849" id="Seg_8253" s="T848">где</ta>
            <ta e="T850" id="Seg_8254" s="T849">люди.[NOM.SG]</ta>
            <ta e="T852" id="Seg_8255" s="T851">а</ta>
            <ta e="T853" id="Seg_8256" s="T852">что.[NOM.SG]</ta>
            <ta e="T854" id="Seg_8257" s="T853">этот.[NOM.SG]</ta>
            <ta e="T855" id="Seg_8258" s="T854">там</ta>
            <ta e="T857" id="Seg_8259" s="T856">что.[NOM.SG]=INDEF</ta>
            <ta e="T858" id="Seg_8260" s="T857">что=INDEF</ta>
            <ta e="T859" id="Seg_8261" s="T858">сказать-FUT-3PL</ta>
            <ta e="T861" id="Seg_8262" s="T860">а</ta>
            <ta e="T865" id="Seg_8263" s="T864">сказать-FUT-3PL</ta>
            <ta e="T866" id="Seg_8264" s="T865">что=INDEF</ta>
            <ta e="T868" id="Seg_8265" s="T867">а</ta>
            <ta e="T869" id="Seg_8266" s="T868">мы.NOM</ta>
            <ta e="T870" id="Seg_8267" s="T869">я.NOM</ta>
            <ta e="T871" id="Seg_8268" s="T870">что.[NOM.SG]=INDEF</ta>
            <ta e="T872" id="Seg_8269" s="T871">NEG</ta>
            <ta e="T873" id="Seg_8270" s="T872">сказать-FUT-1SG</ta>
            <ta e="T875" id="Seg_8271" s="T874">а</ta>
            <ta e="T876" id="Seg_8272" s="T875">почему</ta>
            <ta e="T877" id="Seg_8273" s="T876">NEG</ta>
            <ta e="T878" id="Seg_8274" s="T877">сказать-FUT-2SG</ta>
            <ta e="T880" id="Seg_8275" s="T879">нет</ta>
            <ta e="T882" id="Seg_8276" s="T881">если</ta>
            <ta e="T883" id="Seg_8277" s="T882">сказать-INF.LAT</ta>
            <ta e="T885" id="Seg_8278" s="T884">плохой.[NOM.SG]</ta>
            <ta e="T886" id="Seg_8279" s="T885">люди.[NOM.SG]</ta>
            <ta e="T887" id="Seg_8280" s="T886">мочь-FUT-1SG</ta>
            <ta e="T889" id="Seg_8281" s="T888">почему</ta>
            <ta e="T890" id="Seg_8282" s="T889">так</ta>
            <ta e="T892" id="Seg_8283" s="T891">нет</ta>
            <ta e="T893" id="Seg_8284" s="T892">хороший</ta>
            <ta e="T894" id="Seg_8285" s="T893">люди.[NOM.SG]</ta>
            <ta e="T896" id="Seg_8286" s="T895">нет</ta>
            <ta e="T897" id="Seg_8287" s="T896">плохой.[NOM.SG]</ta>
            <ta e="T898" id="Seg_8288" s="T897">люди.[NOM.SG]</ta>
            <ta e="T900" id="Seg_8289" s="T899">ну</ta>
            <ta e="T901" id="Seg_8290" s="T900">я.NOM</ta>
            <ta e="T902" id="Seg_8291" s="T901">немного</ta>
            <ta e="T903" id="Seg_8292" s="T902">исчезнуть-FUT-1SG</ta>
            <ta e="T904" id="Seg_8293" s="T903">ты.DAT</ta>
            <ta e="T908" id="Seg_8294" s="T907">я.GEN</ta>
            <ta e="T909" id="Seg_8295" s="T908">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T910" id="Seg_8296" s="T909">ты.GEN</ta>
            <ta e="T911" id="Seg_8297" s="T910">отец-NOM/GEN/ACC.2SG</ta>
            <ta e="T912" id="Seg_8298" s="T911">бороться-RES-3PL</ta>
            <ta e="T914" id="Seg_8299" s="T913">почему</ta>
            <ta e="T915" id="Seg_8300" s="T914">этот.[NOM.SG]</ta>
            <ta e="T916" id="Seg_8301" s="T915">бороться-RES-3PL</ta>
            <ta e="T918" id="Seg_8302" s="T917">так</ta>
            <ta e="T919" id="Seg_8303" s="T918">этот.[NOM.SG]</ta>
            <ta e="T920" id="Seg_8304" s="T919">сказать-RES-3SG.O</ta>
            <ta e="T921" id="Seg_8305" s="T920">ты.GEN</ta>
            <ta e="T922" id="Seg_8306" s="T921">отец-NOM/GEN/ACC.2SG</ta>
            <ta e="T924" id="Seg_8307" s="T922">сказать-RES-3SG.O</ta>
            <ta e="T925" id="Seg_8308" s="T924">я.GEN</ta>
            <ta e="T926" id="Seg_8309" s="T925">мальчик-NOM/GEN/ACC.1SG</ta>
            <ta e="T927" id="Seg_8310" s="T926">хороший</ta>
            <ta e="T928" id="Seg_8311" s="T927">красивый.[NOM.SG]</ta>
            <ta e="T930" id="Seg_8312" s="T929">а</ta>
            <ta e="T931" id="Seg_8313" s="T930">ты.GEN</ta>
            <ta e="T932" id="Seg_8314" s="T931">девушка.[NOM.SG]</ta>
            <ta e="T933" id="Seg_8315" s="T932">NEG</ta>
            <ta e="T934" id="Seg_8316" s="T933">хороший</ta>
            <ta e="T936" id="Seg_8317" s="T935">а</ta>
            <ta e="T937" id="Seg_8318" s="T936">я.GEN</ta>
            <ta e="T938" id="Seg_8319" s="T937">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T940" id="Seg_8320" s="T938">сказать-RES-3SG.O</ta>
            <ta e="T941" id="Seg_8321" s="T940">о</ta>
            <ta e="T942" id="Seg_8322" s="T941">я.GEN</ta>
            <ta e="T943" id="Seg_8323" s="T942">дочь-NOM/GEN/ACC.1SG</ta>
            <ta e="T944" id="Seg_8324" s="T943">хороший</ta>
            <ta e="T946" id="Seg_8325" s="T945">и</ta>
            <ta e="T947" id="Seg_8326" s="T946">красивый.[NOM.SG]</ta>
            <ta e="T949" id="Seg_8327" s="T948">и</ta>
            <ta e="T950" id="Seg_8328" s="T949">PTCL</ta>
            <ta e="T951" id="Seg_8329" s="T950">что.[NOM.SG]</ta>
            <ta e="T952" id="Seg_8330" s="T951">PTCL</ta>
            <ta e="T953" id="Seg_8331" s="T952">что.[NOM.SG]</ta>
            <ta e="T954" id="Seg_8332" s="T953">сказать-RES-3SG.O</ta>
            <ta e="T956" id="Seg_8333" s="T955">и</ta>
            <ta e="T957" id="Seg_8334" s="T956">ты.GEN</ta>
            <ta e="T958" id="Seg_8335" s="T957">сын-NOM/GEN/ACC.2SG</ta>
            <ta e="T959" id="Seg_8336" s="T958">NEG</ta>
            <ta e="T960" id="Seg_8337" s="T959">хороший.[NOM.SG]</ta>
            <ta e="T962" id="Seg_8338" s="T961">что.[NOM.SG]=INDEF</ta>
            <ta e="T963" id="Seg_8339" s="T962">NEG</ta>
            <ta e="T964" id="Seg_8340" s="T963">сказать-RES-3SG.O</ta>
            <ta e="T965" id="Seg_8341" s="T964">я.LAT</ta>
            <ta e="T967" id="Seg_8342" s="T966">а</ta>
            <ta e="T968" id="Seg_8343" s="T967">я.GEN</ta>
            <ta e="T969" id="Seg_8344" s="T968">дочь-NOM/GEN/ACC.1SG</ta>
            <ta e="T970" id="Seg_8345" s="T969">PTCL</ta>
            <ta e="T971" id="Seg_8346" s="T970">что.[NOM.SG]</ta>
            <ta e="T972" id="Seg_8347" s="T971">PTCL</ta>
            <ta e="T974" id="Seg_8348" s="T973">ты.DAT</ta>
            <ta e="T975" id="Seg_8349" s="T974">всегда</ta>
            <ta e="T976" id="Seg_8350" s="T975">сказать-RES-PRS-3SG.O</ta>
            <ta e="T978" id="Seg_8351" s="T977">вот</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_8352" s="T1">adj</ta>
            <ta e="T3" id="Seg_8353" s="T2">n.[n:case]</ta>
            <ta e="T4" id="Seg_8354" s="T3">v-v:tense.[v:pn]</ta>
            <ta e="T5" id="Seg_8355" s="T4">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T7" id="Seg_8356" s="T6">adj</ta>
            <ta e="T8" id="Seg_8357" s="T7">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T10" id="Seg_8358" s="T9">conj</ta>
            <ta e="T11" id="Seg_8359" s="T10">dempro.[n:case]</ta>
            <ta e="T12" id="Seg_8360" s="T11">adj.[n:case]</ta>
            <ta e="T13" id="Seg_8361" s="T12">n.[n:case]</ta>
            <ta e="T14" id="Seg_8362" s="T13">num.[n:case]</ta>
            <ta e="T17" id="Seg_8363" s="T16">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_8364" s="T18">conj</ta>
            <ta e="T20" id="Seg_8365" s="T19">adj</ta>
            <ta e="T21" id="Seg_8366" s="T20">n.[n:case]</ta>
            <ta e="T22" id="Seg_8367" s="T21">adj</ta>
            <ta e="T23" id="Seg_8368" s="T22">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T25" id="Seg_8369" s="T24">ptcl</ta>
            <ta e="T27" id="Seg_8370" s="T26">pers</ta>
            <ta e="T28" id="Seg_8371" s="T27">n-n:case.poss</ta>
            <ta e="T29" id="Seg_8372" s="T28">adj</ta>
            <ta e="T30" id="Seg_8373" s="T29">v-v:tense.[v:pn]</ta>
            <ta e="T32" id="Seg_8374" s="T31">adv</ta>
            <ta e="T33" id="Seg_8375" s="T32">adj.[n:case]</ta>
            <ta e="T35" id="Seg_8376" s="T34">adv</ta>
            <ta e="T36" id="Seg_8377" s="T35">adj.[n:case]</ta>
            <ta e="T37" id="Seg_8378" s="T36">v-v:tense.[v:pn]</ta>
            <ta e="T39" id="Seg_8379" s="T38">adv</ta>
            <ta e="T42" id="Seg_8380" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_8381" s="T42">v-v:tense.[v:pn]</ta>
            <ta e="T44" id="Seg_8382" s="T43">adj</ta>
            <ta e="T46" id="Seg_8383" s="T45">adv</ta>
            <ta e="T47" id="Seg_8384" s="T46">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T49" id="Seg_8385" s="T48">ptcl</ta>
            <ta e="T52" id="Seg_8386" s="T51">pers</ta>
            <ta e="T53" id="Seg_8387" s="T52">v-v:tense-v:pn</ta>
            <ta e="T54" id="Seg_8388" s="T53">adv</ta>
            <ta e="T55" id="Seg_8389" s="T54">adj.[n:case]</ta>
            <ta e="T57" id="Seg_8390" s="T56">adv</ta>
            <ta e="T58" id="Seg_8391" s="T57">adj.[n:case]</ta>
            <ta e="T59" id="Seg_8392" s="T58">v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_8393" s="T60">pers</ta>
            <ta e="T62" id="Seg_8394" s="T61">n-n:case.poss</ta>
            <ta e="T63" id="Seg_8395" s="T62">v.[v:pn]</ta>
            <ta e="T65" id="Seg_8396" s="T64">v.[v:pn]</ta>
            <ta e="T66" id="Seg_8397" s="T65">n.[n:case]</ta>
            <ta e="T67" id="Seg_8398" s="T66">pers</ta>
            <ta e="T68" id="Seg_8399" s="T67">conj</ta>
            <ta e="T69" id="Seg_8400" s="T68">pers</ta>
            <ta e="T70" id="Seg_8401" s="T69">refl-n:case.poss</ta>
            <ta e="T71" id="Seg_8402" s="T70">que-que</ta>
            <ta e="T72" id="Seg_8403" s="T71">v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_8404" s="T73">adv=%%</ta>
            <ta e="T75" id="Seg_8405" s="T74">adj</ta>
            <ta e="T76" id="Seg_8406" s="T75">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_8407" s="T77">ptcl</ta>
            <ta e="T81" id="Seg_8408" s="T80">adv-%%</ta>
            <ta e="T82" id="Seg_8409" s="T81">ptcl</ta>
            <ta e="T83" id="Seg_8410" s="T82">v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_8411" s="T84">v-v:tense-v:pn</ta>
            <ta e="T86" id="Seg_8412" s="T85">adj-adj-adj-adj</ta>
            <ta e="T87" id="Seg_8413" s="T86">n-n:case.poss</ta>
            <ta e="T88" id="Seg_8414" s="T87">v-v&gt;v</ta>
            <ta e="T89" id="Seg_8415" s="T88">v-v&gt;v</ta>
            <ta e="T90" id="Seg_8416" s="T89">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T92" id="Seg_8417" s="T91">conj</ta>
            <ta e="T93" id="Seg_8418" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_8419" s="T93">adv</ta>
            <ta e="T95" id="Seg_8420" s="T94">adj.[n:case]</ta>
            <ta e="T96" id="Seg_8421" s="T95">v-v:tense-v:pn</ta>
            <ta e="T98" id="Seg_8422" s="T97">v-v&gt;v-v:pn</ta>
            <ta e="T100" id="Seg_8423" s="T99">conj</ta>
            <ta e="T102" id="Seg_8424" s="T101">v-v:tense-v:pn</ta>
            <ta e="T103" id="Seg_8425" s="T102">conj</ta>
            <ta e="T104" id="Seg_8426" s="T103">v</ta>
            <ta e="T105" id="Seg_8427" s="T104">v-v&gt;v-v:pn</ta>
            <ta e="T107" id="Seg_8428" s="T106">v-v:n.fin</ta>
            <ta e="T108" id="Seg_8429" s="T107">adv</ta>
            <ta e="T109" id="Seg_8430" s="T108">adj.[n:case]</ta>
            <ta e="T111" id="Seg_8431" s="T110">que</ta>
            <ta e="T112" id="Seg_8432" s="T111">pers</ta>
            <ta e="T113" id="Seg_8433" s="T112">v-v:n.fin</ta>
            <ta e="T115" id="Seg_8434" s="T114">que=ptcl</ta>
            <ta e="T116" id="Seg_8435" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_8436" s="T116">v-v:tense-v:pn</ta>
            <ta e="T118" id="Seg_8437" s="T117">adv</ta>
            <ta e="T119" id="Seg_8438" s="T118">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_8439" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_8440" s="T123">n.[n:case]</ta>
            <ta e="T125" id="Seg_8441" s="T124">adj</ta>
            <ta e="T126" id="Seg_8442" s="T125">v-v:tense-v:pn</ta>
            <ta e="T128" id="Seg_8443" s="T127">conj</ta>
            <ta e="T129" id="Seg_8444" s="T128">adv</ta>
            <ta e="T130" id="Seg_8445" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_8446" s="T130">v-v:tense</ta>
            <ta e="T133" id="Seg_8447" s="T132">que=ptcl</ta>
            <ta e="T135" id="Seg_8448" s="T134">conj</ta>
            <ta e="T136" id="Seg_8449" s="T135">adv</ta>
            <ta e="T137" id="Seg_8450" s="T136">que</ta>
            <ta e="T138" id="Seg_8451" s="T137">adv</ta>
            <ta e="T139" id="Seg_8452" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_8453" s="T139">v-v:tense-v:pn</ta>
            <ta e="T142" id="Seg_8454" s="T141">interj</ta>
            <ta e="T143" id="Seg_8455" s="T142">que</ta>
            <ta e="T144" id="Seg_8456" s="T143">ptcl</ta>
            <ta e="T145" id="Seg_8457" s="T144">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T147" id="Seg_8458" s="T146">conj</ta>
            <ta e="T148" id="Seg_8459" s="T147">que.[n:case]</ta>
            <ta e="T150" id="Seg_8460" s="T149">ptcl</ta>
            <ta e="T152" id="Seg_8461" s="T151">v-v:tense-v:pn</ta>
            <ta e="T153" id="Seg_8462" s="T152">adv</ta>
            <ta e="T155" id="Seg_8463" s="T154">conj</ta>
            <ta e="T156" id="Seg_8464" s="T155">adv</ta>
            <ta e="T157" id="Seg_8465" s="T156">v-v:tense-v:pn</ta>
            <ta e="T159" id="Seg_8466" s="T158">conj</ta>
            <ta e="T160" id="Seg_8467" s="T159">pers</ta>
            <ta e="T161" id="Seg_8468" s="T160">que.[n:case]</ta>
            <ta e="T162" id="Seg_8469" s="T161">ptcl</ta>
            <ta e="T163" id="Seg_8470" s="T162">v-v:tense-v:pn</ta>
            <ta e="T165" id="Seg_8471" s="T164">ptcl</ta>
            <ta e="T167" id="Seg_8472" s="T166">pers</ta>
            <ta e="T168" id="Seg_8473" s="T167">ptcl</ta>
            <ta e="T169" id="Seg_8474" s="T168">v-v:tense-v:pn</ta>
            <ta e="T171" id="Seg_8475" s="T170">pers</ta>
            <ta e="T172" id="Seg_8476" s="T171">dempro-n:case</ta>
            <ta e="T173" id="Seg_8477" s="T172">v-v:tense-v:pn</ta>
            <ta e="T175" id="Seg_8478" s="T174">ptcl</ta>
            <ta e="T177" id="Seg_8479" s="T176">%%</ta>
            <ta e="T179" id="Seg_8480" s="T178">que=ptcl</ta>
            <ta e="T180" id="Seg_8481" s="T179">v-v:n.fin</ta>
            <ta e="T181" id="Seg_8482" s="T180">ptcl</ta>
            <ta e="T183" id="Seg_8483" s="T182">pers</ta>
            <ta e="T184" id="Seg_8484" s="T183">v-v:n.fin</ta>
            <ta e="T185" id="Seg_8485" s="T184">v-v:tense-v:pn</ta>
            <ta e="T187" id="Seg_8486" s="T186">%%</ta>
            <ta e="T191" id="Seg_8487" s="T190">que.[n:case]</ta>
            <ta e="T192" id="Seg_8488" s="T191">v-v:tense-v:pn</ta>
            <ta e="T194" id="Seg_8489" s="T193">n.[n:case]</ta>
            <ta e="T196" id="Seg_8490" s="T195">que</ta>
            <ta e="T197" id="Seg_8491" s="T196">n.[n:case]</ta>
            <ta e="T199" id="Seg_8492" s="T198">pers</ta>
            <ta e="T200" id="Seg_8493" s="T199">ptcl</ta>
            <ta e="T201" id="Seg_8494" s="T200">v-v:pn</ta>
            <ta e="T202" id="Seg_8495" s="T201">v-v:n.fin</ta>
            <ta e="T204" id="Seg_8496" s="T203">conj</ta>
            <ta e="T205" id="Seg_8497" s="T204">pers</ta>
            <ta e="T206" id="Seg_8498" s="T205">ptcl</ta>
            <ta e="T207" id="Seg_8499" s="T206">n.[n:case]</ta>
            <ta e="T208" id="Seg_8500" s="T207">ptcl</ta>
            <ta e="T210" id="Seg_8501" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_8502" s="T210">adj</ta>
            <ta e="T212" id="Seg_8503" s="T211">ptcl</ta>
            <ta e="T213" id="Seg_8504" s="T212">v-v:tense-v:pn</ta>
            <ta e="T215" id="Seg_8505" s="T214">que</ta>
            <ta e="T216" id="Seg_8506" s="T215">n.[n:case]</ta>
            <ta e="T218" id="Seg_8507" s="T217">n.[n:case]</ta>
            <ta e="T219" id="Seg_8508" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_8509" s="T219">n.[n:case]</ta>
            <ta e="T222" id="Seg_8510" s="T221">conj</ta>
            <ta e="T223" id="Seg_8511" s="T222">n.[n:case]</ta>
            <ta e="T224" id="Seg_8512" s="T223">adj</ta>
            <ta e="T226" id="Seg_8513" s="T225">%%</ta>
            <ta e="T228" id="Seg_8514" s="T227">que</ta>
            <ta e="T229" id="Seg_8515" s="T228">pers</ta>
            <ta e="T230" id="Seg_8516" s="T229">ptcl</ta>
            <ta e="T231" id="Seg_8517" s="T230">v-v:tense-v:pn</ta>
            <ta e="T233" id="Seg_8518" s="T232">conj</ta>
            <ta e="T234" id="Seg_8519" s="T233">pers</ta>
            <ta e="T235" id="Seg_8520" s="T234">n-n:case.poss</ta>
            <ta e="T236" id="Seg_8521" s="T235">adj</ta>
            <ta e="T239" id="Seg_8522" s="T238">pers</ta>
            <ta e="T240" id="Seg_8523" s="T239">ptcl</ta>
            <ta e="T241" id="Seg_8524" s="T240">v-v:tense-v:pn</ta>
            <ta e="T243" id="Seg_8525" s="T242">que</ta>
            <ta e="T244" id="Seg_8526" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_8527" s="T244">ptcl</ta>
            <ta e="T247" id="Seg_8528" s="T246">ptcl</ta>
            <ta e="T248" id="Seg_8529" s="T247">v-v&gt;v-v:pn</ta>
            <ta e="T249" id="Seg_8530" s="T248">conj</ta>
            <ta e="T250" id="Seg_8531" s="T249">adj</ta>
            <ta e="T251" id="Seg_8532" s="T250">n-n:case.poss</ta>
            <ta e="T252" id="Seg_8533" s="T251">pers</ta>
            <ta e="T254" id="Seg_8534" s="T253">que</ta>
            <ta e="T255" id="Seg_8535" s="T254">ptcl</ta>
            <ta e="T256" id="Seg_8536" s="T255">adj</ta>
            <ta e="T257" id="Seg_8537" s="T256">n.[n:case]</ta>
            <ta e="T259" id="Seg_8538" s="T258">pers</ta>
            <ta e="T260" id="Seg_8539" s="T259">adj</ta>
            <ta e="T261" id="Seg_8540" s="T260">n.[n:case]</ta>
            <ta e="T263" id="Seg_8541" s="T262">que=ptcl</ta>
            <ta e="T264" id="Seg_8542" s="T263">pers</ta>
            <ta e="T265" id="Seg_8543" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_8544" s="T265">ptcl</ta>
            <ta e="T267" id="Seg_8545" s="T266">v-v:tense-v:pn</ta>
            <ta e="T268" id="Seg_8546" s="T267">v-v:tense-v:pn</ta>
            <ta e="T269" id="Seg_8547" s="T268">ptcl</ta>
            <ta e="T272" id="Seg_8548" s="T271">adv</ta>
            <ta e="T273" id="Seg_8549" s="T272">adj</ta>
            <ta e="T274" id="Seg_8550" s="T273">v-v:tense-v:pn</ta>
            <ta e="T276" id="Seg_8551" s="T275">interj</ta>
            <ta e="T277" id="Seg_8552" s="T276">ptcl</ta>
            <ta e="T278" id="Seg_8553" s="T277">ptcl</ta>
            <ta e="T279" id="Seg_8554" s="T278">v-v&gt;v-v:pn</ta>
            <ta e="T280" id="Seg_8555" s="T279">pers</ta>
            <ta e="T282" id="Seg_8556" s="T281">pers</ta>
            <ta e="T283" id="Seg_8557" s="T282">dempro-n:case</ta>
            <ta e="T284" id="Seg_8558" s="T283">ptcl</ta>
            <ta e="T285" id="Seg_8559" s="T284">v-v:tense-v:pn</ta>
            <ta e="T286" id="Seg_8560" s="T285">adv</ta>
            <ta e="T288" id="Seg_8561" s="T287">ptcl</ta>
            <ta e="T289" id="Seg_8562" s="T288">v-v:n.fin</ta>
            <ta e="T293" id="Seg_8563" s="T292">ptcl</ta>
            <ta e="T294" id="Seg_8564" s="T293">dempro.[n:case]</ta>
            <ta e="T295" id="Seg_8565" s="T294">pers</ta>
            <ta e="T296" id="Seg_8566" s="T295">v-v:tense-v:pn</ta>
            <ta e="T297" id="Seg_8567" s="T296">adv-%%</ta>
            <ta e="T299" id="Seg_8568" s="T298">%%</ta>
            <ta e="T301" id="Seg_8569" s="T300">pers</ta>
            <ta e="T302" id="Seg_8570" s="T301">adv</ta>
            <ta e="T303" id="Seg_8571" s="T302">n.[n:case]</ta>
            <ta e="T304" id="Seg_8572" s="T303">v-v:tense-v:pn</ta>
            <ta e="T306" id="Seg_8573" s="T305">interj</ta>
            <ta e="T307" id="Seg_8574" s="T306">que</ta>
            <ta e="T308" id="Seg_8575" s="T307">ptcl</ta>
            <ta e="T309" id="Seg_8576" s="T308">pers</ta>
            <ta e="T311" id="Seg_8577" s="T310">ptcl</ta>
            <ta e="T312" id="Seg_8578" s="T311">pers</ta>
            <ta e="T313" id="Seg_8579" s="T312">adj.[n:case]</ta>
            <ta e="T314" id="Seg_8580" s="T313">n.[n:case]</ta>
            <ta e="T316" id="Seg_8581" s="T315">ptcl</ta>
            <ta e="T317" id="Seg_8582" s="T316">%%</ta>
            <ta e="T318" id="Seg_8583" s="T317">ptcl</ta>
            <ta e="T319" id="Seg_8584" s="T318">v-v:tense-v:pn</ta>
            <ta e="T320" id="Seg_8585" s="T319">que-n:case.poss</ta>
            <ta e="T322" id="Seg_8586" s="T321">conj</ta>
            <ta e="T323" id="Seg_8587" s="T322">que</ta>
            <ta e="T324" id="Seg_8588" s="T323">pers</ta>
            <ta e="T325" id="Seg_8589" s="T324">v-v:tense-v:pn</ta>
            <ta e="T326" id="Seg_8590" s="T325">ptcl</ta>
            <ta e="T327" id="Seg_8591" s="T326">pers</ta>
            <ta e="T328" id="Seg_8592" s="T327">adv=%%</ta>
            <ta e="T330" id="Seg_8593" s="T329">pers</ta>
            <ta e="T331" id="Seg_8594" s="T330">adv-%%</ta>
            <ta e="T332" id="Seg_8595" s="T331">v-v:tense-v:pn</ta>
            <ta e="T334" id="Seg_8596" s="T333">conj</ta>
            <ta e="T335" id="Seg_8597" s="T334">que</ta>
            <ta e="T336" id="Seg_8598" s="T335">adv</ta>
            <ta e="T337" id="Seg_8599" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_8600" s="T337">v-v:tense-v:pn</ta>
            <ta e="T340" id="Seg_8601" s="T339">ptcl</ta>
            <ta e="T341" id="Seg_8602" s="T340">ptcl</ta>
            <ta e="T342" id="Seg_8603" s="T341">v-v:tense-v:pn</ta>
            <ta e="T343" id="Seg_8604" s="T342">pers</ta>
            <ta e="T345" id="Seg_8605" s="T344">pers</ta>
            <ta e="T346" id="Seg_8606" s="T345">adv-%%</ta>
            <ta e="T347" id="Seg_8607" s="T346">ptcl</ta>
            <ta e="T348" id="Seg_8608" s="T347">adj.[n:case]</ta>
            <ta e="T349" id="Seg_8609" s="T348">n-n:case</ta>
            <ta e="T352" id="Seg_8610" s="T351">pers</ta>
            <ta e="T353" id="Seg_8611" s="T352">adv</ta>
            <ta e="T354" id="Seg_8612" s="T353">v-v:tense-v:pn</ta>
            <ta e="T356" id="Seg_8613" s="T355">conj</ta>
            <ta e="T357" id="Seg_8614" s="T356">adv</ta>
            <ta e="T358" id="Seg_8615" s="T357">ptcl</ta>
            <ta e="T359" id="Seg_8616" s="T358">v-v:tense-v:pn</ta>
            <ta e="T360" id="Seg_8617" s="T359">pers</ta>
            <ta e="T361" id="Seg_8618" s="T360">n.[n:case]</ta>
            <ta e="T363" id="Seg_8619" s="T362">que-n:case.poss</ta>
            <ta e="T364" id="Seg_8620" s="T363">ptcl</ta>
            <ta e="T366" id="Seg_8621" s="T365">ptcl</ta>
            <ta e="T373" id="Seg_8622" s="T372">ptcl</ta>
            <ta e="T374" id="Seg_8623" s="T373">v-v:tense-v:pn</ta>
            <ta e="T376" id="Seg_8624" s="T375">ptcl</ta>
            <ta e="T377" id="Seg_8625" s="T376">n.[n:case]</ta>
            <ta e="T379" id="Seg_8626" s="T378">conj</ta>
            <ta e="T380" id="Seg_8627" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_8628" s="T380">adj</ta>
            <ta e="T382" id="Seg_8629" s="T381">n.[n:case]</ta>
            <ta e="T384" id="Seg_8630" s="T383">ptcl</ta>
            <ta e="T386" id="Seg_8631" s="T385">%%</ta>
            <ta e="T388" id="Seg_8632" s="T387">adj.[n:case]</ta>
            <ta e="T389" id="Seg_8633" s="T388">n.[n:case]</ta>
            <ta e="T390" id="Seg_8634" s="T389">adj</ta>
            <ta e="T391" id="Seg_8635" s="T390">n-n:case</ta>
            <ta e="T392" id="Seg_8636" s="T391">v-v:tense.[v:pn]</ta>
            <ta e="T393" id="Seg_8637" s="T392">n.[n:case]</ta>
            <ta e="T394" id="Seg_8638" s="T393">n.[n:case]</ta>
            <ta e="T395" id="Seg_8639" s="T394">conj</ta>
            <ta e="T396" id="Seg_8640" s="T395">ptcl</ta>
            <ta e="T397" id="Seg_8641" s="T396">que.[n:case]</ta>
            <ta e="T398" id="Seg_8642" s="T397">ptcl</ta>
            <ta e="T400" id="Seg_8643" s="T399">n-n:case.poss</ta>
            <ta e="T401" id="Seg_8644" s="T400">v-v:tense.[v:pn]</ta>
            <ta e="T402" id="Seg_8645" s="T401">dempro.[n:case]</ta>
            <ta e="T403" id="Seg_8646" s="T402">n-n:case</ta>
            <ta e="T405" id="Seg_8647" s="T404">ptcl</ta>
            <ta e="T406" id="Seg_8648" s="T405">v-v:tense.[v:pn]</ta>
            <ta e="T408" id="Seg_8649" s="T407">que.[n:case]</ta>
            <ta e="T409" id="Seg_8650" s="T408">pers</ta>
            <ta e="T410" id="Seg_8651" s="T409">v-v:tense-v:pn</ta>
            <ta e="T412" id="Seg_8652" s="T411">ptcl</ta>
            <ta e="T413" id="Seg_8653" s="T412">pers</ta>
            <ta e="T414" id="Seg_8654" s="T413">v.[v:pn]</ta>
            <ta e="T415" id="Seg_8655" s="T414">conj</ta>
            <ta e="T416" id="Seg_8656" s="T415">pers</ta>
            <ta e="T417" id="Seg_8657" s="T416">v-v:tense.[v:pn]</ta>
            <ta e="T420" id="Seg_8658" s="T419">pers</ta>
            <ta e="T421" id="Seg_8659" s="T420">v-v:tense-v:pn</ta>
            <ta e="T423" id="Seg_8660" s="T422">conj</ta>
            <ta e="T424" id="Seg_8661" s="T423">pers</ta>
            <ta e="T425" id="Seg_8662" s="T424">pers</ta>
            <ta e="T426" id="Seg_8663" s="T425">ptcl</ta>
            <ta e="T427" id="Seg_8664" s="T426">v-v:tense-v:pn</ta>
            <ta e="T429" id="Seg_8665" s="T428">pers</ta>
            <ta e="T430" id="Seg_8666" s="T429">pers</ta>
            <ta e="T431" id="Seg_8667" s="T430">v-v:tense-v:pn</ta>
            <ta e="T433" id="Seg_8668" s="T432">conj</ta>
            <ta e="T434" id="Seg_8669" s="T433">que</ta>
            <ta e="T435" id="Seg_8670" s="T434">%%</ta>
            <ta e="T436" id="Seg_8671" s="T435">pers</ta>
            <ta e="T437" id="Seg_8672" s="T436">v-v:tense-v:pn</ta>
            <ta e="T439" id="Seg_8673" s="T438">ptcl</ta>
            <ta e="T440" id="Seg_8674" s="T439">%%</ta>
            <ta e="T441" id="Seg_8675" s="T440">adv</ta>
            <ta e="T442" id="Seg_8676" s="T441">v-v:tense-v:pn</ta>
            <ta e="T444" id="Seg_8677" s="T443">pers</ta>
            <ta e="T445" id="Seg_8678" s="T444">v-v&gt;v.[v:pn]</ta>
            <ta e="T446" id="Seg_8679" s="T445">conj</ta>
            <ta e="T447" id="Seg_8680" s="T446">ptcl</ta>
            <ta e="T448" id="Seg_8681" s="T447">que-n:case</ta>
            <ta e="T449" id="Seg_8682" s="T448">ptcl</ta>
            <ta e="T451" id="Seg_8683" s="T450">conj</ta>
            <ta e="T452" id="Seg_8684" s="T451">pers</ta>
            <ta e="T453" id="Seg_8685" s="T452">adj.[n:case]</ta>
            <ta e="T454" id="Seg_8686" s="T453">n-n:case</ta>
            <ta e="T455" id="Seg_8687" s="T454">pers</ta>
            <ta e="T456" id="Seg_8688" s="T455">v-v&gt;v.[v:pn]</ta>
            <ta e="T459" id="Seg_8689" s="T458">ptcl</ta>
            <ta e="T461" id="Seg_8690" s="T460">%%</ta>
            <ta e="T463" id="Seg_8691" s="T462">v-v:mood.pn</ta>
            <ta e="T464" id="Seg_8692" s="T463">pers-n:case</ta>
            <ta e="T465" id="Seg_8693" s="T464">v-v:n.fin</ta>
            <ta e="T467" id="Seg_8694" s="T466">interj</ta>
            <ta e="T477" id="Seg_8695" s="T476">%%</ta>
            <ta e="T479" id="Seg_8696" s="T478">v-v:mood.pn</ta>
            <ta e="T480" id="Seg_8697" s="T479">pers-n:case</ta>
            <ta e="T481" id="Seg_8698" s="T480">v-v:n.fin</ta>
            <ta e="T483" id="Seg_8699" s="T482">conj</ta>
            <ta e="T484" id="Seg_8700" s="T483">que.[n:case]</ta>
            <ta e="T485" id="Seg_8701" s="T484">pers</ta>
            <ta e="T486" id="Seg_8702" s="T485">v-v:tense-v:pn</ta>
            <ta e="T488" id="Seg_8703" s="T487">conj</ta>
            <ta e="T489" id="Seg_8704" s="T488">pers</ta>
            <ta e="T490" id="Seg_8705" s="T489">v-v:tense-v:pn</ta>
            <ta e="T491" id="Seg_8706" s="T490">n.[n:case]</ta>
            <ta e="T492" id="Seg_8707" s="T491">n.[n:case]</ta>
            <ta e="T493" id="Seg_8708" s="T492">conj</ta>
            <ta e="T494" id="Seg_8709" s="T493">n.[n:case]</ta>
            <ta e="T496" id="Seg_8710" s="T495">n-%%</ta>
            <ta e="T498" id="Seg_8711" s="T497">v-v:mood.pn</ta>
            <ta e="T500" id="Seg_8712" s="T499">ptcl</ta>
            <ta e="T501" id="Seg_8713" s="T500">pers</ta>
            <ta e="T502" id="Seg_8714" s="T501">ptcl</ta>
            <ta e="T503" id="Seg_8715" s="T502">n.[n:case]</ta>
            <ta e="T504" id="Seg_8716" s="T503">v-v:tense-v:pn</ta>
            <ta e="T505" id="Seg_8717" s="T504">ptcl</ta>
            <ta e="T507" id="Seg_8718" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_8719" s="T507">conj</ta>
            <ta e="T509" id="Seg_8720" s="T508">n.[n:case]</ta>
            <ta e="T510" id="Seg_8721" s="T509">pers</ta>
            <ta e="T511" id="Seg_8722" s="T510">v-v:tense-v:pn</ta>
            <ta e="T512" id="Seg_8723" s="T511">pers</ta>
            <ta e="T513" id="Seg_8724" s="T512">v-v:n.fin</ta>
            <ta e="T515" id="Seg_8725" s="T514">adj</ta>
            <ta e="T516" id="Seg_8726" s="T515">adv</ta>
            <ta e="T517" id="Seg_8727" s="T516">v-v:ins-v:mood.pn</ta>
            <ta e="T519" id="Seg_8728" s="T518">dempro.[n:case]</ta>
            <ta e="T520" id="Seg_8729" s="T519">ptcl</ta>
            <ta e="T521" id="Seg_8730" s="T520">pers</ta>
            <ta e="T522" id="Seg_8731" s="T521">v-v:tense-v:pn</ta>
            <ta e="T524" id="Seg_8732" s="T523">adv</ta>
            <ta e="T525" id="Seg_8733" s="T524">adv</ta>
            <ta e="T526" id="Seg_8734" s="T525">v-v:tense-v:pn</ta>
            <ta e="T528" id="Seg_8735" s="T527">adj</ta>
            <ta e="T529" id="Seg_8736" s="T528">v-v:tense-v:pn</ta>
            <ta e="T533" id="Seg_8737" s="T532">%%</ta>
            <ta e="T535" id="Seg_8738" s="T534">que</ta>
            <ta e="T536" id="Seg_8739" s="T535">pers</ta>
            <ta e="T537" id="Seg_8740" s="T536">pers</ta>
            <ta e="T538" id="Seg_8741" s="T537">n-n:case.poss</ta>
            <ta e="T539" id="Seg_8742" s="T538">ptcl</ta>
            <ta e="T540" id="Seg_8743" s="T539">refl</ta>
            <ta e="T541" id="Seg_8744" s="T540">n-n:case</ta>
            <ta e="T542" id="Seg_8745" s="T541">v-v:tense-v:pn</ta>
            <ta e="T544" id="Seg_8746" s="T543">ptcl</ta>
            <ta e="T545" id="Seg_8747" s="T544">pers</ta>
            <ta e="T546" id="Seg_8748" s="T545">n-n:case.poss</ta>
            <ta e="T547" id="Seg_8749" s="T546">adj.[n:case]</ta>
            <ta e="T548" id="Seg_8750" s="T547">conj</ta>
            <ta e="T549" id="Seg_8751" s="T548">pers</ta>
            <ta e="T550" id="Seg_8752" s="T549">adv</ta>
            <ta e="T552" id="Seg_8753" s="T551">que</ta>
            <ta e="T553" id="Seg_8754" s="T552">ptcl</ta>
            <ta e="T554" id="Seg_8755" s="T553">v-v:tense-v:pn</ta>
            <ta e="T556" id="Seg_8756" s="T555">adj.[n:case]</ta>
            <ta e="T558" id="Seg_8757" s="T557">ptcl</ta>
            <ta e="T559" id="Seg_8758" s="T558">adj</ta>
            <ta e="T560" id="Seg_8759" s="T559">ptcl</ta>
            <ta e="T562" id="Seg_8760" s="T561">ptcl</ta>
            <ta e="T564" id="Seg_8761" s="T563">%%</ta>
            <ta e="T566" id="Seg_8762" s="T565">pers</ta>
            <ta e="T567" id="Seg_8763" s="T566">adv</ta>
            <ta e="T568" id="Seg_8764" s="T567">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T570" id="Seg_8765" s="T569">n-n:num-n:case.poss</ta>
            <ta e="T571" id="Seg_8766" s="T570">v-v:tense-v:pn</ta>
            <ta e="T573" id="Seg_8767" s="T572">conj</ta>
            <ta e="T574" id="Seg_8768" s="T573">n-n:case.poss</ta>
            <ta e="T575" id="Seg_8769" s="T574">ptcl</ta>
            <ta e="T576" id="Seg_8770" s="T575">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T578" id="Seg_8771" s="T577">adv</ta>
            <ta e="T579" id="Seg_8772" s="T578">pers</ta>
            <ta e="T580" id="Seg_8773" s="T579">v-v&gt;v-v&gt;v-%%</ta>
            <ta e="T582" id="Seg_8774" s="T581">ptcl</ta>
            <ta e="T583" id="Seg_8775" s="T582">ptcl</ta>
            <ta e="T584" id="Seg_8776" s="T583">ptcl</ta>
            <ta e="T585" id="Seg_8777" s="T584">v-v&gt;v-v&gt;v-v:n.fin</ta>
            <ta e="T587" id="Seg_8778" s="T586">ptcl</ta>
            <ta e="T588" id="Seg_8779" s="T587">v-v:n.fin</ta>
            <ta e="T590" id="Seg_8780" s="T589">v-v:n.fin</ta>
            <ta e="T591" id="Seg_8781" s="T590">adj</ta>
            <ta e="T593" id="Seg_8782" s="T592">conj</ta>
            <ta e="T594" id="Seg_8783" s="T593">v-v&gt;v-v&gt;v-v:n.fin</ta>
            <ta e="T595" id="Seg_8784" s="T594">adj.[n:case]</ta>
            <ta e="T597" id="Seg_8785" s="T596">adv</ta>
            <ta e="T598" id="Seg_8786" s="T597">adj.[n:case]</ta>
            <ta e="T600" id="Seg_8787" s="T599">ptcl</ta>
            <ta e="T602" id="Seg_8788" s="T601">%%</ta>
            <ta e="T604" id="Seg_8789" s="T603">num.[n:case]</ta>
            <ta e="T605" id="Seg_8790" s="T604">n.[n:case]</ta>
            <ta e="T606" id="Seg_8791" s="T605">v-v:tense-v:pn</ta>
            <ta e="T608" id="Seg_8792" s="T607">adv=%%</ta>
            <ta e="T609" id="Seg_8793" s="T608">dempro.[n:case]</ta>
            <ta e="T610" id="Seg_8794" s="T609">n.[n:case]</ta>
            <ta e="T611" id="Seg_8795" s="T610">que=ptcl</ta>
            <ta e="T612" id="Seg_8796" s="T611">ptcl</ta>
            <ta e="T613" id="Seg_8797" s="T612">v-v:n.fin</ta>
            <ta e="T614" id="Seg_8798" s="T613">v-v:tense-v:pn</ta>
            <ta e="T616" id="Seg_8799" s="T615">conj</ta>
            <ta e="T617" id="Seg_8800" s="T616">num.[n:case]</ta>
            <ta e="T618" id="Seg_8801" s="T617">n-n:case-n:ins-n:case.poss</ta>
            <ta e="T619" id="Seg_8802" s="T618">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T621" id="Seg_8803" s="T620">ptcl</ta>
            <ta e="T622" id="Seg_8804" s="T621">v-v:tense-v:pn</ta>
            <ta e="T623" id="Seg_8805" s="T622">pers</ta>
            <ta e="T624" id="Seg_8806" s="T623">n.[n:case]</ta>
            <ta e="T625" id="Seg_8807" s="T624">pers</ta>
            <ta e="T626" id="Seg_8808" s="T625">n.[n:case]</ta>
            <ta e="T628" id="Seg_8809" s="T627">ptcl</ta>
            <ta e="T629" id="Seg_8810" s="T628">pers</ta>
            <ta e="T630" id="Seg_8811" s="T629">v-v:mood.pn</ta>
            <ta e="T631" id="Seg_8812" s="T630">n.[n:case]</ta>
            <ta e="T632" id="Seg_8813" s="T631">conj</ta>
            <ta e="T633" id="Seg_8814" s="T632">pers</ta>
            <ta e="T634" id="Seg_8815" s="T633">n.[n:case]</ta>
            <ta e="T636" id="Seg_8816" s="T635">interj</ta>
            <ta e="T637" id="Seg_8817" s="T636">n.[n:case]</ta>
            <ta e="T639" id="Seg_8818" s="T638">que</ta>
            <ta e="T640" id="Seg_8819" s="T639">ptcl</ta>
            <ta e="T642" id="Seg_8820" s="T641">ptcl</ta>
            <ta e="T643" id="Seg_8821" s="T642">pers</ta>
            <ta e="T644" id="Seg_8822" s="T643">n.[n:case]</ta>
            <ta e="T646" id="Seg_8823" s="T645">n-n:case.poss</ta>
            <ta e="T648" id="Seg_8824" s="T647">que</ta>
            <ta e="T649" id="Seg_8825" s="T648">ptcl</ta>
            <ta e="T650" id="Seg_8826" s="T649">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T652" id="Seg_8827" s="T651">ptcl</ta>
            <ta e="T653" id="Seg_8828" s="T652">ptcl</ta>
            <ta e="T654" id="Seg_8829" s="T653">ptcl</ta>
            <ta e="T655" id="Seg_8830" s="T654">v-v:n.fin</ta>
            <ta e="T656" id="Seg_8831" s="T655">adj.[n:case]</ta>
            <ta e="T658" id="Seg_8832" s="T657">conj</ta>
            <ta e="T659" id="Seg_8833" s="T658">pers</ta>
            <ta e="T660" id="Seg_8834" s="T659">ptcl</ta>
            <ta e="T661" id="Seg_8835" s="T660">n-n:case.poss</ta>
            <ta e="T662" id="Seg_8836" s="T661">v-v:tense.[v:pn]</ta>
            <ta e="T663" id="Seg_8837" s="T662">n.[n:case]</ta>
            <ta e="T664" id="Seg_8838" s="T663">conj</ta>
            <ta e="T665" id="Seg_8839" s="T664">pers</ta>
            <ta e="T666" id="Seg_8840" s="T665">n.[n:case]</ta>
            <ta e="T668" id="Seg_8841" s="T667">v-v:tense.[v:pn]</ta>
            <ta e="T669" id="Seg_8842" s="T668">pers</ta>
            <ta e="T670" id="Seg_8843" s="T669">v-v:mood.pn</ta>
            <ta e="T671" id="Seg_8844" s="T670">n.[n:case]</ta>
            <ta e="T672" id="Seg_8845" s="T671">conj</ta>
            <ta e="T673" id="Seg_8846" s="T672">pers</ta>
            <ta e="T674" id="Seg_8847" s="T673">v-v:mood.pn</ta>
            <ta e="T675" id="Seg_8848" s="T674">n.[n:case]</ta>
            <ta e="T677" id="Seg_8849" s="T676">conj</ta>
            <ta e="T678" id="Seg_8850" s="T677">n.[n:case]</ta>
            <ta e="T679" id="Seg_8851" s="T678">pers</ta>
            <ta e="T681" id="Seg_8852" s="T680">conj</ta>
            <ta e="T682" id="Seg_8853" s="T681">n.[n:case]</ta>
            <ta e="T683" id="Seg_8854" s="T682">conj</ta>
            <ta e="T684" id="Seg_8855" s="T683">n.[n:case]</ta>
            <ta e="T685" id="Seg_8856" s="T684">pers</ta>
            <ta e="T687" id="Seg_8857" s="T686">conj</ta>
            <ta e="T688" id="Seg_8858" s="T687">adv</ta>
            <ta e="T689" id="Seg_8859" s="T688">n.[n:case]</ta>
            <ta e="T690" id="Seg_8860" s="T689">ptcl</ta>
            <ta e="T691" id="Seg_8861" s="T690">v-v:tense-v:pn</ta>
            <ta e="T696" id="Seg_8862" s="T695">%%</ta>
            <ta e="T700" id="Seg_8863" s="T699">ptcl</ta>
            <ta e="T701" id="Seg_8864" s="T700">%%</ta>
            <ta e="T702" id="Seg_8865" s="T701">ptcl</ta>
            <ta e="T704" id="Seg_8866" s="T703">ptcl</ta>
            <ta e="T705" id="Seg_8867" s="T704">n.[n:case]</ta>
            <ta e="T707" id="Seg_8868" s="T706">adj.[n:case]</ta>
            <ta e="T708" id="Seg_8869" s="T707">n.[n:case]</ta>
            <ta e="T709" id="Seg_8870" s="T708">v-v:tense-v:pn</ta>
            <ta e="T711" id="Seg_8871" s="T710">pers</ta>
            <ta e="T712" id="Seg_8872" s="T711">n-n:case.poss</ta>
            <ta e="T713" id="Seg_8873" s="T712">v-v:tense-v:pn</ta>
            <ta e="T715" id="Seg_8874" s="T714">conj</ta>
            <ta e="T716" id="Seg_8875" s="T715">que</ta>
            <ta e="T719" id="Seg_8876" s="T718">que</ta>
            <ta e="T720" id="Seg_8877" s="T719">que</ta>
            <ta e="T722" id="Seg_8878" s="T721">dempro-n:num</ta>
            <ta e="T723" id="Seg_8879" s="T722">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T724" id="Seg_8880" s="T723">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T726" id="Seg_8881" s="T725">refl</ta>
            <ta e="T727" id="Seg_8882" s="T726">n-n:case</ta>
            <ta e="T728" id="Seg_8883" s="T727">dempro-n:case</ta>
            <ta e="T729" id="Seg_8884" s="T728">ptcl</ta>
            <ta e="T730" id="Seg_8885" s="T729">v-v:tense-v:pn</ta>
            <ta e="T732" id="Seg_8886" s="T731">conj</ta>
            <ta e="T733" id="Seg_8887" s="T732">que.[n:case]</ta>
            <ta e="T734" id="Seg_8888" s="T733">v-v:tense-v:pn</ta>
            <ta e="T736" id="Seg_8889" s="T735">ptcl</ta>
            <ta e="T737" id="Seg_8890" s="T736">que.[n:case]</ta>
            <ta e="T739" id="Seg_8891" s="T738">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T741" id="Seg_8892" s="T740">conj</ta>
            <ta e="T742" id="Seg_8893" s="T741">ptcl</ta>
            <ta e="T743" id="Seg_8894" s="T742">%%</ta>
            <ta e="T744" id="Seg_8895" s="T743">ptcl</ta>
            <ta e="T745" id="Seg_8896" s="T744">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T747" id="Seg_8897" s="T746">ptcl</ta>
            <ta e="T748" id="Seg_8898" s="T747">dempro-n:case</ta>
            <ta e="T749" id="Seg_8899" s="T748">conj</ta>
            <ta e="T750" id="Seg_8900" s="T749">v-v:tense-v:pn</ta>
            <ta e="T752" id="Seg_8901" s="T751">conj</ta>
            <ta e="T753" id="Seg_8902" s="T752">que</ta>
            <ta e="T755" id="Seg_8903" s="T754">ptcl</ta>
            <ta e="T756" id="Seg_8904" s="T755">pers</ta>
            <ta e="T757" id="Seg_8905" s="T756">v-v:pn</ta>
            <ta e="T758" id="Seg_8906" s="T757">que</ta>
            <ta e="T760" id="Seg_8907" s="T759">pers</ta>
            <ta e="T761" id="Seg_8908" s="T760">ptcl</ta>
            <ta e="T762" id="Seg_8909" s="T761">ptcl</ta>
            <ta e="T763" id="Seg_8910" s="T762">v-v:pn</ta>
            <ta e="T765" id="Seg_8911" s="T764">%%</ta>
            <ta e="T767" id="Seg_8912" s="T766">v-v:mood-v:pn</ta>
            <ta e="T768" id="Seg_8913" s="T767">n-n:num</ta>
            <ta e="T769" id="Seg_8914" s="T768">v-v:n.fin</ta>
            <ta e="T771" id="Seg_8915" s="T770">n.[n:case]</ta>
            <ta e="T772" id="Seg_8916" s="T771">adj.[n:case]</ta>
            <ta e="T774" id="Seg_8917" s="T773">conj</ta>
            <ta e="T775" id="Seg_8918" s="T774">que.[n:case]</ta>
            <ta e="T777" id="Seg_8919" s="T776">ptcl</ta>
            <ta e="T779" id="Seg_8920" s="T778">interj</ta>
            <ta e="T780" id="Seg_8921" s="T779">ptcl</ta>
            <ta e="T781" id="Seg_8922" s="T780">que.[n:case]</ta>
            <ta e="T783" id="Seg_8923" s="T782">conj</ta>
            <ta e="T784" id="Seg_8924" s="T783">pers</ta>
            <ta e="T785" id="Seg_8925" s="T784">adv</ta>
            <ta e="T786" id="Seg_8926" s="T785">v-v:tense-v:pn</ta>
            <ta e="T787" id="Seg_8927" s="T786">v-v:n.fin</ta>
            <ta e="T788" id="Seg_8928" s="T787">v-v:tense-v:pn</ta>
            <ta e="T790" id="Seg_8929" s="T789">conj</ta>
            <ta e="T791" id="Seg_8930" s="T790">pers</ta>
            <ta e="T792" id="Seg_8931" s="T791">v-v:tense-v:pn</ta>
            <ta e="T794" id="Seg_8932" s="T793">adj</ta>
            <ta e="T795" id="Seg_8933" s="T794">dempro.[n:case]</ta>
            <ta e="T799" id="Seg_8934" s="T798">%%</ta>
            <ta e="T801" id="Seg_8935" s="T800">pers</ta>
            <ta e="T802" id="Seg_8936" s="T801">dempro-n:case</ta>
            <ta e="T803" id="Seg_8937" s="T802">v-v:tense-v:pn</ta>
            <ta e="T804" id="Seg_8938" s="T803">adv</ta>
            <ta e="T805" id="Seg_8939" s="T804">%%-n:num</ta>
            <ta e="T807" id="Seg_8940" s="T806">conj</ta>
            <ta e="T808" id="Seg_8941" s="T807">dempro.[n:case]</ta>
            <ta e="T809" id="Seg_8942" s="T808">pers</ta>
            <ta e="T810" id="Seg_8943" s="T809">adv</ta>
            <ta e="T811" id="Seg_8944" s="T810">adv</ta>
            <ta e="T812" id="Seg_8945" s="T811">v-v:tense-v:pn</ta>
            <ta e="T814" id="Seg_8946" s="T813">%%</ta>
            <ta e="T816" id="Seg_8947" s="T815">ptcl</ta>
            <ta e="T817" id="Seg_8948" s="T816">v-v:mood-v:pn</ta>
            <ta e="T818" id="Seg_8949" s="T817">pers-n:case</ta>
            <ta e="T820" id="Seg_8950" s="T819">que</ta>
            <ta e="T822" id="Seg_8951" s="T821">conj</ta>
            <ta e="T823" id="Seg_8952" s="T822">adv</ta>
            <ta e="T825" id="Seg_8953" s="T824">conj</ta>
            <ta e="T826" id="Seg_8954" s="T825">n-n:case.poss</ta>
            <ta e="T828" id="Seg_8955" s="T827">n-n:case.poss</ta>
            <ta e="T829" id="Seg_8956" s="T828">ptcl</ta>
            <ta e="T831" id="Seg_8957" s="T830">ptcl</ta>
            <ta e="T833" id="Seg_8958" s="T832">ptcl</ta>
            <ta e="T834" id="Seg_8959" s="T833">v-v:tense-v:pn</ta>
            <ta e="T835" id="Seg_8960" s="T834">n-n:case.poss</ta>
            <ta e="T837" id="Seg_8961" s="T836">conj</ta>
            <ta e="T838" id="Seg_8962" s="T837">pers</ta>
            <ta e="T839" id="Seg_8963" s="T838">num-num&gt;num</ta>
            <ta e="T840" id="Seg_8964" s="T839">v-v:mood-v:pn</ta>
            <ta e="T842" id="Seg_8965" s="T841">conj</ta>
            <ta e="T843" id="Seg_8966" s="T842">que</ta>
            <ta e="T845" id="Seg_8967" s="T844">conj</ta>
            <ta e="T846" id="Seg_8968" s="T845">adv</ta>
            <ta e="T847" id="Seg_8969" s="T846">adv</ta>
            <ta e="T849" id="Seg_8970" s="T848">que</ta>
            <ta e="T850" id="Seg_8971" s="T849">n.[n:case]</ta>
            <ta e="T852" id="Seg_8972" s="T851">conj</ta>
            <ta e="T853" id="Seg_8973" s="T852">que.[n:case]</ta>
            <ta e="T854" id="Seg_8974" s="T853">dempro.[n:case]</ta>
            <ta e="T855" id="Seg_8975" s="T854">adv</ta>
            <ta e="T857" id="Seg_8976" s="T856">que.[n:case]=ptcl</ta>
            <ta e="T858" id="Seg_8977" s="T857">que=ptcl</ta>
            <ta e="T859" id="Seg_8978" s="T858">v-v:tense-v:pn</ta>
            <ta e="T861" id="Seg_8979" s="T860">conj</ta>
            <ta e="T865" id="Seg_8980" s="T864">v-v:tense-v:pn</ta>
            <ta e="T866" id="Seg_8981" s="T865">que=ptcl</ta>
            <ta e="T868" id="Seg_8982" s="T867">conj</ta>
            <ta e="T869" id="Seg_8983" s="T868">pers</ta>
            <ta e="T870" id="Seg_8984" s="T869">pers</ta>
            <ta e="T871" id="Seg_8985" s="T870">que.[n:case]=ptcl</ta>
            <ta e="T872" id="Seg_8986" s="T871">ptcl</ta>
            <ta e="T873" id="Seg_8987" s="T872">v-v:tense-v:pn</ta>
            <ta e="T875" id="Seg_8988" s="T874">conj</ta>
            <ta e="T876" id="Seg_8989" s="T875">que</ta>
            <ta e="T877" id="Seg_8990" s="T876">ptcl</ta>
            <ta e="T878" id="Seg_8991" s="T877">v-v:tense-v:pn</ta>
            <ta e="T880" id="Seg_8992" s="T879">ptcl</ta>
            <ta e="T882" id="Seg_8993" s="T881">conj</ta>
            <ta e="T883" id="Seg_8994" s="T882">v-v:n.fin</ta>
            <ta e="T885" id="Seg_8995" s="T884">adj.[n:case]</ta>
            <ta e="T886" id="Seg_8996" s="T885">n.[n:case]</ta>
            <ta e="T887" id="Seg_8997" s="T886">v-v:tense-v:pn</ta>
            <ta e="T889" id="Seg_8998" s="T888">que</ta>
            <ta e="T890" id="Seg_8999" s="T889">ptcl</ta>
            <ta e="T892" id="Seg_9000" s="T891">ptcl</ta>
            <ta e="T893" id="Seg_9001" s="T892">adj</ta>
            <ta e="T894" id="Seg_9002" s="T893">n.[n:case]</ta>
            <ta e="T896" id="Seg_9003" s="T895">ptcl</ta>
            <ta e="T897" id="Seg_9004" s="T896">adj.[n:case]</ta>
            <ta e="T898" id="Seg_9005" s="T897">n.[n:case]</ta>
            <ta e="T900" id="Seg_9006" s="T899">ptcl</ta>
            <ta e="T901" id="Seg_9007" s="T900">pers</ta>
            <ta e="T902" id="Seg_9008" s="T901">ptcl</ta>
            <ta e="T903" id="Seg_9009" s="T902">v-v:tense-v:pn</ta>
            <ta e="T904" id="Seg_9010" s="T903">pers</ta>
            <ta e="T908" id="Seg_9011" s="T907">pers</ta>
            <ta e="T909" id="Seg_9012" s="T908">n-n:case.poss</ta>
            <ta e="T910" id="Seg_9013" s="T909">pers</ta>
            <ta e="T911" id="Seg_9014" s="T910">n-n:case.poss</ta>
            <ta e="T912" id="Seg_9015" s="T911">v-v&gt;v-v:pn</ta>
            <ta e="T914" id="Seg_9016" s="T913">que</ta>
            <ta e="T915" id="Seg_9017" s="T914">dempro.[n:case]</ta>
            <ta e="T916" id="Seg_9018" s="T915">v-v&gt;v-v:pn</ta>
            <ta e="T918" id="Seg_9019" s="T917">ptcl</ta>
            <ta e="T919" id="Seg_9020" s="T918">dempro.[n:case]</ta>
            <ta e="T920" id="Seg_9021" s="T919">v-v&gt;v-v:pn</ta>
            <ta e="T921" id="Seg_9022" s="T920">pers</ta>
            <ta e="T922" id="Seg_9023" s="T921">n-n:case.poss</ta>
            <ta e="T924" id="Seg_9024" s="T922">v-v&gt;v-v:pn</ta>
            <ta e="T925" id="Seg_9025" s="T924">pers</ta>
            <ta e="T926" id="Seg_9026" s="T925">n-n:case.poss</ta>
            <ta e="T927" id="Seg_9027" s="T926">adj</ta>
            <ta e="T928" id="Seg_9028" s="T927">adj.[n:case]</ta>
            <ta e="T930" id="Seg_9029" s="T929">conj</ta>
            <ta e="T931" id="Seg_9030" s="T930">pers</ta>
            <ta e="T932" id="Seg_9031" s="T931">n.[n:case]</ta>
            <ta e="T933" id="Seg_9032" s="T932">ptcl</ta>
            <ta e="T934" id="Seg_9033" s="T933">adj</ta>
            <ta e="T936" id="Seg_9034" s="T935">conj</ta>
            <ta e="T937" id="Seg_9035" s="T936">pers</ta>
            <ta e="T938" id="Seg_9036" s="T937">n-n:case.poss</ta>
            <ta e="T940" id="Seg_9037" s="T938">v-v&gt;v-v:pn</ta>
            <ta e="T941" id="Seg_9038" s="T940">interj</ta>
            <ta e="T942" id="Seg_9039" s="T941">pers</ta>
            <ta e="T943" id="Seg_9040" s="T942">n-n:case.poss</ta>
            <ta e="T944" id="Seg_9041" s="T943">adj</ta>
            <ta e="T946" id="Seg_9042" s="T945">conj</ta>
            <ta e="T947" id="Seg_9043" s="T946">adj.[n:case]</ta>
            <ta e="T949" id="Seg_9044" s="T948">conj</ta>
            <ta e="T950" id="Seg_9045" s="T949">ptcl</ta>
            <ta e="T951" id="Seg_9046" s="T950">que.[n:case]</ta>
            <ta e="T952" id="Seg_9047" s="T951">ptcl</ta>
            <ta e="T953" id="Seg_9048" s="T952">que.[n:case]</ta>
            <ta e="T954" id="Seg_9049" s="T953">v-v&gt;v-v:pn</ta>
            <ta e="T956" id="Seg_9050" s="T955">conj</ta>
            <ta e="T957" id="Seg_9051" s="T956">pers</ta>
            <ta e="T958" id="Seg_9052" s="T957">n-n:case.poss</ta>
            <ta e="T959" id="Seg_9053" s="T958">ptcl</ta>
            <ta e="T960" id="Seg_9054" s="T959">adj.[n:case]</ta>
            <ta e="T962" id="Seg_9055" s="T961">que.[n:case]=ptcl</ta>
            <ta e="T963" id="Seg_9056" s="T962">ptcl</ta>
            <ta e="T964" id="Seg_9057" s="T963">v-v&gt;v-v:pn</ta>
            <ta e="T965" id="Seg_9058" s="T964">pers</ta>
            <ta e="T967" id="Seg_9059" s="T966">conj</ta>
            <ta e="T968" id="Seg_9060" s="T967">pers</ta>
            <ta e="T969" id="Seg_9061" s="T968">n-n:case.poss</ta>
            <ta e="T970" id="Seg_9062" s="T969">ptcl</ta>
            <ta e="T971" id="Seg_9063" s="T970">que.[n:case]</ta>
            <ta e="T972" id="Seg_9064" s="T971">ptcl</ta>
            <ta e="T974" id="Seg_9065" s="T973">pers</ta>
            <ta e="T975" id="Seg_9066" s="T974">adv</ta>
            <ta e="T976" id="Seg_9067" s="T975">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T978" id="Seg_9068" s="T977">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_9069" s="T1">adj</ta>
            <ta e="T3" id="Seg_9070" s="T2">n</ta>
            <ta e="T4" id="Seg_9071" s="T3">v</ta>
            <ta e="T5" id="Seg_9072" s="T4">v</ta>
            <ta e="T7" id="Seg_9073" s="T6">adj</ta>
            <ta e="T8" id="Seg_9074" s="T7">v</ta>
            <ta e="T10" id="Seg_9075" s="T9">conj</ta>
            <ta e="T11" id="Seg_9076" s="T10">dempro</ta>
            <ta e="T12" id="Seg_9077" s="T11">adj</ta>
            <ta e="T13" id="Seg_9078" s="T12">n</ta>
            <ta e="T14" id="Seg_9079" s="T13">num</ta>
            <ta e="T17" id="Seg_9080" s="T16">v</ta>
            <ta e="T19" id="Seg_9081" s="T18">conj</ta>
            <ta e="T20" id="Seg_9082" s="T19">adj</ta>
            <ta e="T21" id="Seg_9083" s="T20">n</ta>
            <ta e="T22" id="Seg_9084" s="T21">adj</ta>
            <ta e="T23" id="Seg_9085" s="T22">v</ta>
            <ta e="T25" id="Seg_9086" s="T24">ptcl</ta>
            <ta e="T27" id="Seg_9087" s="T26">pers</ta>
            <ta e="T28" id="Seg_9088" s="T27">n</ta>
            <ta e="T29" id="Seg_9089" s="T28">adj</ta>
            <ta e="T30" id="Seg_9090" s="T29">v</ta>
            <ta e="T32" id="Seg_9091" s="T31">adv</ta>
            <ta e="T33" id="Seg_9092" s="T32">adj</ta>
            <ta e="T35" id="Seg_9093" s="T34">adv</ta>
            <ta e="T36" id="Seg_9094" s="T35">adj</ta>
            <ta e="T37" id="Seg_9095" s="T36">v</ta>
            <ta e="T39" id="Seg_9096" s="T38">adv</ta>
            <ta e="T42" id="Seg_9097" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_9098" s="T42">v</ta>
            <ta e="T44" id="Seg_9099" s="T43">adj</ta>
            <ta e="T46" id="Seg_9100" s="T45">adv</ta>
            <ta e="T47" id="Seg_9101" s="T46">v</ta>
            <ta e="T49" id="Seg_9102" s="T48">ptcl</ta>
            <ta e="T52" id="Seg_9103" s="T51">pers</ta>
            <ta e="T53" id="Seg_9104" s="T52">v</ta>
            <ta e="T54" id="Seg_9105" s="T53">adv</ta>
            <ta e="T55" id="Seg_9106" s="T54">adj</ta>
            <ta e="T57" id="Seg_9107" s="T56">adv</ta>
            <ta e="T58" id="Seg_9108" s="T57">adj</ta>
            <ta e="T59" id="Seg_9109" s="T58">v</ta>
            <ta e="T61" id="Seg_9110" s="T60">pers</ta>
            <ta e="T62" id="Seg_9111" s="T61">n</ta>
            <ta e="T63" id="Seg_9112" s="T62">v</ta>
            <ta e="T65" id="Seg_9113" s="T64">v</ta>
            <ta e="T66" id="Seg_9114" s="T65">n</ta>
            <ta e="T67" id="Seg_9115" s="T66">pers</ta>
            <ta e="T68" id="Seg_9116" s="T67">conj</ta>
            <ta e="T69" id="Seg_9117" s="T68">pers</ta>
            <ta e="T70" id="Seg_9118" s="T69">refl</ta>
            <ta e="T71" id="Seg_9119" s="T70">que</ta>
            <ta e="T72" id="Seg_9120" s="T71">v</ta>
            <ta e="T74" id="Seg_9121" s="T73">adv</ta>
            <ta e="T75" id="Seg_9122" s="T74">adj</ta>
            <ta e="T76" id="Seg_9123" s="T75">v</ta>
            <ta e="T78" id="Seg_9124" s="T77">ptcl</ta>
            <ta e="T81" id="Seg_9125" s="T80">adv</ta>
            <ta e="T82" id="Seg_9126" s="T81">ptcl</ta>
            <ta e="T83" id="Seg_9127" s="T82">v</ta>
            <ta e="T85" id="Seg_9128" s="T84">v</ta>
            <ta e="T86" id="Seg_9129" s="T85">adj</ta>
            <ta e="T87" id="Seg_9130" s="T86">n</ta>
            <ta e="T88" id="Seg_9131" s="T87">v</ta>
            <ta e="T89" id="Seg_9132" s="T88">v</ta>
            <ta e="T90" id="Seg_9133" s="T89">v</ta>
            <ta e="T92" id="Seg_9134" s="T91">conj</ta>
            <ta e="T93" id="Seg_9135" s="T92">n</ta>
            <ta e="T94" id="Seg_9136" s="T93">adv</ta>
            <ta e="T95" id="Seg_9137" s="T94">adj</ta>
            <ta e="T96" id="Seg_9138" s="T95">v</ta>
            <ta e="T98" id="Seg_9139" s="T97">v</ta>
            <ta e="T100" id="Seg_9140" s="T99">conj</ta>
            <ta e="T102" id="Seg_9141" s="T101">v</ta>
            <ta e="T103" id="Seg_9142" s="T102">conj</ta>
            <ta e="T104" id="Seg_9143" s="T103">v</ta>
            <ta e="T105" id="Seg_9144" s="T104">v</ta>
            <ta e="T107" id="Seg_9145" s="T106">v</ta>
            <ta e="T108" id="Seg_9146" s="T107">adv</ta>
            <ta e="T109" id="Seg_9147" s="T108">adj</ta>
            <ta e="T111" id="Seg_9148" s="T110">que</ta>
            <ta e="T112" id="Seg_9149" s="T111">pers</ta>
            <ta e="T113" id="Seg_9150" s="T112">v</ta>
            <ta e="T115" id="Seg_9151" s="T114">que</ta>
            <ta e="T116" id="Seg_9152" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_9153" s="T116">v</ta>
            <ta e="T118" id="Seg_9154" s="T117">adv</ta>
            <ta e="T119" id="Seg_9155" s="T118">v</ta>
            <ta e="T123" id="Seg_9156" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_9157" s="T123">n</ta>
            <ta e="T125" id="Seg_9158" s="T124">adj</ta>
            <ta e="T126" id="Seg_9159" s="T125">v</ta>
            <ta e="T128" id="Seg_9160" s="T127">conj</ta>
            <ta e="T129" id="Seg_9161" s="T128">adv</ta>
            <ta e="T130" id="Seg_9162" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_9163" s="T130">v</ta>
            <ta e="T133" id="Seg_9164" s="T132">que</ta>
            <ta e="T135" id="Seg_9165" s="T134">conj</ta>
            <ta e="T136" id="Seg_9166" s="T135">adv</ta>
            <ta e="T137" id="Seg_9167" s="T136">que</ta>
            <ta e="T138" id="Seg_9168" s="T137">adv</ta>
            <ta e="T139" id="Seg_9169" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_9170" s="T139">v</ta>
            <ta e="T142" id="Seg_9171" s="T141">interj</ta>
            <ta e="T143" id="Seg_9172" s="T142">que</ta>
            <ta e="T144" id="Seg_9173" s="T143">ptcl</ta>
            <ta e="T145" id="Seg_9174" s="T144">v</ta>
            <ta e="T147" id="Seg_9175" s="T146">conj</ta>
            <ta e="T148" id="Seg_9176" s="T147">que</ta>
            <ta e="T150" id="Seg_9177" s="T149">ptcl</ta>
            <ta e="T152" id="Seg_9178" s="T151">v</ta>
            <ta e="T153" id="Seg_9179" s="T152">adv</ta>
            <ta e="T155" id="Seg_9180" s="T154">conj</ta>
            <ta e="T156" id="Seg_9181" s="T155">adv</ta>
            <ta e="T157" id="Seg_9182" s="T156">v</ta>
            <ta e="T159" id="Seg_9183" s="T158">conj</ta>
            <ta e="T160" id="Seg_9184" s="T159">pers</ta>
            <ta e="T161" id="Seg_9185" s="T160">que</ta>
            <ta e="T162" id="Seg_9186" s="T161">ptcl</ta>
            <ta e="T163" id="Seg_9187" s="T162">v</ta>
            <ta e="T165" id="Seg_9188" s="T164">ptcl</ta>
            <ta e="T167" id="Seg_9189" s="T166">pers</ta>
            <ta e="T168" id="Seg_9190" s="T167">ptcl</ta>
            <ta e="T169" id="Seg_9191" s="T168">v</ta>
            <ta e="T171" id="Seg_9192" s="T170">pers</ta>
            <ta e="T172" id="Seg_9193" s="T171">adv</ta>
            <ta e="T173" id="Seg_9194" s="T172">v</ta>
            <ta e="T175" id="Seg_9195" s="T174">ptcl</ta>
            <ta e="T179" id="Seg_9196" s="T178">que</ta>
            <ta e="T180" id="Seg_9197" s="T179">v</ta>
            <ta e="T181" id="Seg_9198" s="T180">ptcl</ta>
            <ta e="T183" id="Seg_9199" s="T182">pers</ta>
            <ta e="T184" id="Seg_9200" s="T183">v</ta>
            <ta e="T185" id="Seg_9201" s="T184">v</ta>
            <ta e="T191" id="Seg_9202" s="T190">que</ta>
            <ta e="T192" id="Seg_9203" s="T191">v</ta>
            <ta e="T194" id="Seg_9204" s="T193">n</ta>
            <ta e="T196" id="Seg_9205" s="T195">que</ta>
            <ta e="T197" id="Seg_9206" s="T196">n</ta>
            <ta e="T199" id="Seg_9207" s="T198">pers</ta>
            <ta e="T200" id="Seg_9208" s="T199">ptcl</ta>
            <ta e="T201" id="Seg_9209" s="T200">v</ta>
            <ta e="T202" id="Seg_9210" s="T201">v</ta>
            <ta e="T204" id="Seg_9211" s="T203">conj</ta>
            <ta e="T205" id="Seg_9212" s="T204">pers</ta>
            <ta e="T206" id="Seg_9213" s="T205">ptcl</ta>
            <ta e="T207" id="Seg_9214" s="T206">n</ta>
            <ta e="T208" id="Seg_9215" s="T207">ptcl</ta>
            <ta e="T210" id="Seg_9216" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_9217" s="T210">adj</ta>
            <ta e="T212" id="Seg_9218" s="T211">ptcl</ta>
            <ta e="T213" id="Seg_9219" s="T212">v</ta>
            <ta e="T215" id="Seg_9220" s="T214">que</ta>
            <ta e="T216" id="Seg_9221" s="T215">n</ta>
            <ta e="T218" id="Seg_9222" s="T217">n</ta>
            <ta e="T219" id="Seg_9223" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_9224" s="T219">n</ta>
            <ta e="T222" id="Seg_9225" s="T221">conj</ta>
            <ta e="T223" id="Seg_9226" s="T222">n</ta>
            <ta e="T224" id="Seg_9227" s="T223">adj</ta>
            <ta e="T228" id="Seg_9228" s="T227">que</ta>
            <ta e="T229" id="Seg_9229" s="T228">pers</ta>
            <ta e="T230" id="Seg_9230" s="T229">ptcl</ta>
            <ta e="T231" id="Seg_9231" s="T230">v</ta>
            <ta e="T233" id="Seg_9232" s="T232">conj</ta>
            <ta e="T234" id="Seg_9233" s="T233">pers</ta>
            <ta e="T235" id="Seg_9234" s="T234">n</ta>
            <ta e="T236" id="Seg_9235" s="T235">adj</ta>
            <ta e="T239" id="Seg_9236" s="T238">pers</ta>
            <ta e="T240" id="Seg_9237" s="T239">ptcl</ta>
            <ta e="T241" id="Seg_9238" s="T240">v</ta>
            <ta e="T243" id="Seg_9239" s="T242">que</ta>
            <ta e="T244" id="Seg_9240" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_9241" s="T244">ptcl</ta>
            <ta e="T247" id="Seg_9242" s="T246">ptcl</ta>
            <ta e="T248" id="Seg_9243" s="T247">v</ta>
            <ta e="T249" id="Seg_9244" s="T248">conj</ta>
            <ta e="T250" id="Seg_9245" s="T249">adj</ta>
            <ta e="T251" id="Seg_9246" s="T250">n</ta>
            <ta e="T252" id="Seg_9247" s="T251">pers</ta>
            <ta e="T254" id="Seg_9248" s="T253">que</ta>
            <ta e="T255" id="Seg_9249" s="T254">ptcl</ta>
            <ta e="T256" id="Seg_9250" s="T255">adj</ta>
            <ta e="T257" id="Seg_9251" s="T256">n</ta>
            <ta e="T259" id="Seg_9252" s="T258">pers</ta>
            <ta e="T260" id="Seg_9253" s="T259">adj</ta>
            <ta e="T261" id="Seg_9254" s="T260">n</ta>
            <ta e="T263" id="Seg_9255" s="T262">que</ta>
            <ta e="T264" id="Seg_9256" s="T263">pers</ta>
            <ta e="T265" id="Seg_9257" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_9258" s="T265">ptcl</ta>
            <ta e="T267" id="Seg_9259" s="T266">v</ta>
            <ta e="T268" id="Seg_9260" s="T267">v</ta>
            <ta e="T269" id="Seg_9261" s="T268">ptcl</ta>
            <ta e="T272" id="Seg_9262" s="T271">adv</ta>
            <ta e="T273" id="Seg_9263" s="T272">adj</ta>
            <ta e="T274" id="Seg_9264" s="T273">v</ta>
            <ta e="T276" id="Seg_9265" s="T275">interj</ta>
            <ta e="T277" id="Seg_9266" s="T276">ptcl</ta>
            <ta e="T278" id="Seg_9267" s="T277">ptcl</ta>
            <ta e="T279" id="Seg_9268" s="T278">v</ta>
            <ta e="T280" id="Seg_9269" s="T279">pers</ta>
            <ta e="T282" id="Seg_9270" s="T281">pers</ta>
            <ta e="T283" id="Seg_9271" s="T282">dempro</ta>
            <ta e="T284" id="Seg_9272" s="T283">ptcl</ta>
            <ta e="T285" id="Seg_9273" s="T284">v</ta>
            <ta e="T286" id="Seg_9274" s="T285">adv</ta>
            <ta e="T288" id="Seg_9275" s="T287">ptcl</ta>
            <ta e="T289" id="Seg_9276" s="T288">v</ta>
            <ta e="T293" id="Seg_9277" s="T292">ptcl</ta>
            <ta e="T294" id="Seg_9278" s="T293">dempro</ta>
            <ta e="T295" id="Seg_9279" s="T294">pers</ta>
            <ta e="T296" id="Seg_9280" s="T295">v</ta>
            <ta e="T297" id="Seg_9281" s="T296">adv</ta>
            <ta e="T301" id="Seg_9282" s="T300">pers</ta>
            <ta e="T302" id="Seg_9283" s="T301">adv</ta>
            <ta e="T303" id="Seg_9284" s="T302">n</ta>
            <ta e="T304" id="Seg_9285" s="T303">v</ta>
            <ta e="T306" id="Seg_9286" s="T305">interj</ta>
            <ta e="T307" id="Seg_9287" s="T306">que</ta>
            <ta e="T308" id="Seg_9288" s="T307">ptcl</ta>
            <ta e="T309" id="Seg_9289" s="T308">pers</ta>
            <ta e="T311" id="Seg_9290" s="T310">ptcl</ta>
            <ta e="T312" id="Seg_9291" s="T311">pers</ta>
            <ta e="T313" id="Seg_9292" s="T312">adj</ta>
            <ta e="T314" id="Seg_9293" s="T313">n</ta>
            <ta e="T316" id="Seg_9294" s="T315">ptcl</ta>
            <ta e="T318" id="Seg_9295" s="T317">ptcl</ta>
            <ta e="T319" id="Seg_9296" s="T318">n</ta>
            <ta e="T320" id="Seg_9297" s="T319">que</ta>
            <ta e="T322" id="Seg_9298" s="T321">conj</ta>
            <ta e="T323" id="Seg_9299" s="T322">que</ta>
            <ta e="T324" id="Seg_9300" s="T323">pers</ta>
            <ta e="T325" id="Seg_9301" s="T324">v</ta>
            <ta e="T326" id="Seg_9302" s="T325">ptcl</ta>
            <ta e="T327" id="Seg_9303" s="T326">pers</ta>
            <ta e="T328" id="Seg_9304" s="T327">adv</ta>
            <ta e="T330" id="Seg_9305" s="T329">pers</ta>
            <ta e="T331" id="Seg_9306" s="T330">adv</ta>
            <ta e="T332" id="Seg_9307" s="T331">v</ta>
            <ta e="T334" id="Seg_9308" s="T333">conj</ta>
            <ta e="T335" id="Seg_9309" s="T334">que</ta>
            <ta e="T336" id="Seg_9310" s="T335">adv</ta>
            <ta e="T337" id="Seg_9311" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_9312" s="T337">v</ta>
            <ta e="T340" id="Seg_9313" s="T339">ptcl</ta>
            <ta e="T341" id="Seg_9314" s="T340">ptcl</ta>
            <ta e="T342" id="Seg_9315" s="T341">v</ta>
            <ta e="T343" id="Seg_9316" s="T342">pers</ta>
            <ta e="T345" id="Seg_9317" s="T344">pers</ta>
            <ta e="T346" id="Seg_9318" s="T345">adv</ta>
            <ta e="T347" id="Seg_9319" s="T346">ptcl</ta>
            <ta e="T348" id="Seg_9320" s="T347">adj</ta>
            <ta e="T349" id="Seg_9321" s="T348">n</ta>
            <ta e="T352" id="Seg_9322" s="T351">pers</ta>
            <ta e="T353" id="Seg_9323" s="T352">adv</ta>
            <ta e="T354" id="Seg_9324" s="T353">v</ta>
            <ta e="T356" id="Seg_9325" s="T355">conj</ta>
            <ta e="T357" id="Seg_9326" s="T356">adv</ta>
            <ta e="T358" id="Seg_9327" s="T357">ptcl</ta>
            <ta e="T359" id="Seg_9328" s="T358">v</ta>
            <ta e="T360" id="Seg_9329" s="T359">pers</ta>
            <ta e="T361" id="Seg_9330" s="T360">n</ta>
            <ta e="T363" id="Seg_9331" s="T362">que</ta>
            <ta e="T364" id="Seg_9332" s="T363">ptcl</ta>
            <ta e="T366" id="Seg_9333" s="T365">ptcl</ta>
            <ta e="T373" id="Seg_9334" s="T372">ptcl</ta>
            <ta e="T374" id="Seg_9335" s="T373">v</ta>
            <ta e="T376" id="Seg_9336" s="T375">ptcl</ta>
            <ta e="T377" id="Seg_9337" s="T376">n</ta>
            <ta e="T379" id="Seg_9338" s="T378">conj</ta>
            <ta e="T380" id="Seg_9339" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_9340" s="T380">adj</ta>
            <ta e="T382" id="Seg_9341" s="T381">n</ta>
            <ta e="T384" id="Seg_9342" s="T383">ptcl</ta>
            <ta e="T388" id="Seg_9343" s="T387">adj</ta>
            <ta e="T389" id="Seg_9344" s="T388">n</ta>
            <ta e="T390" id="Seg_9345" s="T389">adj</ta>
            <ta e="T391" id="Seg_9346" s="T390">n</ta>
            <ta e="T392" id="Seg_9347" s="T391">v</ta>
            <ta e="T393" id="Seg_9348" s="T392">n</ta>
            <ta e="T394" id="Seg_9349" s="T393">n</ta>
            <ta e="T395" id="Seg_9350" s="T394">conj</ta>
            <ta e="T396" id="Seg_9351" s="T395">ptcl</ta>
            <ta e="T397" id="Seg_9352" s="T396">que</ta>
            <ta e="T398" id="Seg_9353" s="T397">ptcl</ta>
            <ta e="T400" id="Seg_9354" s="T399">n</ta>
            <ta e="T401" id="Seg_9355" s="T400">v</ta>
            <ta e="T402" id="Seg_9356" s="T401">dempro</ta>
            <ta e="T403" id="Seg_9357" s="T402">n</ta>
            <ta e="T405" id="Seg_9358" s="T404">ptcl</ta>
            <ta e="T406" id="Seg_9359" s="T405">v</ta>
            <ta e="T408" id="Seg_9360" s="T407">que</ta>
            <ta e="T409" id="Seg_9361" s="T408">pers</ta>
            <ta e="T410" id="Seg_9362" s="T409">v</ta>
            <ta e="T412" id="Seg_9363" s="T411">ptcl</ta>
            <ta e="T413" id="Seg_9364" s="T412">pers</ta>
            <ta e="T414" id="Seg_9365" s="T413">v</ta>
            <ta e="T415" id="Seg_9366" s="T414">conj</ta>
            <ta e="T416" id="Seg_9367" s="T415">pers</ta>
            <ta e="T417" id="Seg_9368" s="T416">v</ta>
            <ta e="T420" id="Seg_9369" s="T419">pers</ta>
            <ta e="T421" id="Seg_9370" s="T420">v</ta>
            <ta e="T423" id="Seg_9371" s="T422">conj</ta>
            <ta e="T424" id="Seg_9372" s="T423">pers</ta>
            <ta e="T425" id="Seg_9373" s="T424">pers</ta>
            <ta e="T426" id="Seg_9374" s="T425">ptcl</ta>
            <ta e="T427" id="Seg_9375" s="T426">n</ta>
            <ta e="T429" id="Seg_9376" s="T428">pers</ta>
            <ta e="T430" id="Seg_9377" s="T429">pers</ta>
            <ta e="T431" id="Seg_9378" s="T430">n</ta>
            <ta e="T433" id="Seg_9379" s="T432">conj</ta>
            <ta e="T434" id="Seg_9380" s="T433">que</ta>
            <ta e="T436" id="Seg_9381" s="T435">pers</ta>
            <ta e="T437" id="Seg_9382" s="T436">v</ta>
            <ta e="T439" id="Seg_9383" s="T438">ptcl</ta>
            <ta e="T441" id="Seg_9384" s="T440">adv</ta>
            <ta e="T442" id="Seg_9385" s="T441">v</ta>
            <ta e="T444" id="Seg_9386" s="T443">pers</ta>
            <ta e="T445" id="Seg_9387" s="T444">v</ta>
            <ta e="T446" id="Seg_9388" s="T445">conj</ta>
            <ta e="T447" id="Seg_9389" s="T446">ptcl</ta>
            <ta e="T448" id="Seg_9390" s="T447">que</ta>
            <ta e="T449" id="Seg_9391" s="T448">ptcl</ta>
            <ta e="T451" id="Seg_9392" s="T450">conj</ta>
            <ta e="T452" id="Seg_9393" s="T451">pers</ta>
            <ta e="T453" id="Seg_9394" s="T452">adj</ta>
            <ta e="T454" id="Seg_9395" s="T453">n</ta>
            <ta e="T455" id="Seg_9396" s="T454">pers</ta>
            <ta e="T456" id="Seg_9397" s="T455">v</ta>
            <ta e="T459" id="Seg_9398" s="T458">ptcl</ta>
            <ta e="T463" id="Seg_9399" s="T462">v</ta>
            <ta e="T464" id="Seg_9400" s="T463">pers</ta>
            <ta e="T465" id="Seg_9401" s="T464">v</ta>
            <ta e="T467" id="Seg_9402" s="T466">interj</ta>
            <ta e="T479" id="Seg_9403" s="T478">v</ta>
            <ta e="T480" id="Seg_9404" s="T479">pers</ta>
            <ta e="T481" id="Seg_9405" s="T480">v</ta>
            <ta e="T483" id="Seg_9406" s="T482">conj</ta>
            <ta e="T484" id="Seg_9407" s="T483">que</ta>
            <ta e="T485" id="Seg_9408" s="T484">pers</ta>
            <ta e="T486" id="Seg_9409" s="T485">v</ta>
            <ta e="T488" id="Seg_9410" s="T487">conj</ta>
            <ta e="T489" id="Seg_9411" s="T488">pers</ta>
            <ta e="T490" id="Seg_9412" s="T489">v</ta>
            <ta e="T491" id="Seg_9413" s="T490">n</ta>
            <ta e="T492" id="Seg_9414" s="T491">n</ta>
            <ta e="T493" id="Seg_9415" s="T492">conj</ta>
            <ta e="T494" id="Seg_9416" s="T493">n</ta>
            <ta e="T496" id="Seg_9417" s="T495">n</ta>
            <ta e="T498" id="Seg_9418" s="T497">v</ta>
            <ta e="T500" id="Seg_9419" s="T499">ptcl</ta>
            <ta e="T501" id="Seg_9420" s="T500">pers</ta>
            <ta e="T502" id="Seg_9421" s="T501">ptcl</ta>
            <ta e="T503" id="Seg_9422" s="T502">n</ta>
            <ta e="T504" id="Seg_9423" s="T503">v</ta>
            <ta e="T505" id="Seg_9424" s="T504">ptcl</ta>
            <ta e="T507" id="Seg_9425" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_9426" s="T507">conj</ta>
            <ta e="T509" id="Seg_9427" s="T508">n</ta>
            <ta e="T510" id="Seg_9428" s="T509">pers</ta>
            <ta e="T511" id="Seg_9429" s="T510">v</ta>
            <ta e="T512" id="Seg_9430" s="T511">pers</ta>
            <ta e="T513" id="Seg_9431" s="T512">v</ta>
            <ta e="T515" id="Seg_9432" s="T514">adj</ta>
            <ta e="T516" id="Seg_9433" s="T515">adv</ta>
            <ta e="T517" id="Seg_9434" s="T516">v</ta>
            <ta e="T519" id="Seg_9435" s="T518">dempro</ta>
            <ta e="T520" id="Seg_9436" s="T519">ptcl</ta>
            <ta e="T521" id="Seg_9437" s="T520">pers</ta>
            <ta e="T522" id="Seg_9438" s="T521">v</ta>
            <ta e="T524" id="Seg_9439" s="T523">adv</ta>
            <ta e="T525" id="Seg_9440" s="T524">adv</ta>
            <ta e="T526" id="Seg_9441" s="T525">v</ta>
            <ta e="T528" id="Seg_9442" s="T527">adj</ta>
            <ta e="T529" id="Seg_9443" s="T528">v</ta>
            <ta e="T535" id="Seg_9444" s="T534">que</ta>
            <ta e="T536" id="Seg_9445" s="T535">pers</ta>
            <ta e="T537" id="Seg_9446" s="T536">pers</ta>
            <ta e="T538" id="Seg_9447" s="T537">n</ta>
            <ta e="T539" id="Seg_9448" s="T538">ptcl</ta>
            <ta e="T540" id="Seg_9449" s="T539">refl</ta>
            <ta e="T541" id="Seg_9450" s="T540">n</ta>
            <ta e="T542" id="Seg_9451" s="T541">v</ta>
            <ta e="T544" id="Seg_9452" s="T543">ptcl</ta>
            <ta e="T545" id="Seg_9453" s="T544">pers</ta>
            <ta e="T546" id="Seg_9454" s="T545">n</ta>
            <ta e="T547" id="Seg_9455" s="T546">adj</ta>
            <ta e="T548" id="Seg_9456" s="T547">conj</ta>
            <ta e="T549" id="Seg_9457" s="T548">pers</ta>
            <ta e="T550" id="Seg_9458" s="T549">adv</ta>
            <ta e="T552" id="Seg_9459" s="T551">que</ta>
            <ta e="T553" id="Seg_9460" s="T552">ptcl</ta>
            <ta e="T554" id="Seg_9461" s="T553">v</ta>
            <ta e="T556" id="Seg_9462" s="T555">adj</ta>
            <ta e="T558" id="Seg_9463" s="T557">ptcl</ta>
            <ta e="T559" id="Seg_9464" s="T558">adj</ta>
            <ta e="T560" id="Seg_9465" s="T559">ptcl</ta>
            <ta e="T562" id="Seg_9466" s="T561">ptcl</ta>
            <ta e="T566" id="Seg_9467" s="T565">pers</ta>
            <ta e="T567" id="Seg_9468" s="T566">adv</ta>
            <ta e="T568" id="Seg_9469" s="T567">v</ta>
            <ta e="T570" id="Seg_9470" s="T569">n</ta>
            <ta e="T571" id="Seg_9471" s="T570">v</ta>
            <ta e="T573" id="Seg_9472" s="T572">conj</ta>
            <ta e="T574" id="Seg_9473" s="T573">n</ta>
            <ta e="T575" id="Seg_9474" s="T574">ptcl</ta>
            <ta e="T576" id="Seg_9475" s="T575">v</ta>
            <ta e="T578" id="Seg_9476" s="T577">adv</ta>
            <ta e="T579" id="Seg_9477" s="T578">pers</ta>
            <ta e="T580" id="Seg_9478" s="T579">v</ta>
            <ta e="T582" id="Seg_9479" s="T581">ptcl</ta>
            <ta e="T583" id="Seg_9480" s="T582">ptcl</ta>
            <ta e="T584" id="Seg_9481" s="T583">ptcl</ta>
            <ta e="T585" id="Seg_9482" s="T584">v</ta>
            <ta e="T587" id="Seg_9483" s="T586">ptcl</ta>
            <ta e="T588" id="Seg_9484" s="T587">v</ta>
            <ta e="T590" id="Seg_9485" s="T589">v</ta>
            <ta e="T591" id="Seg_9486" s="T590">adj</ta>
            <ta e="T593" id="Seg_9487" s="T592">conj</ta>
            <ta e="T594" id="Seg_9488" s="T593">v</ta>
            <ta e="T595" id="Seg_9489" s="T594">adj</ta>
            <ta e="T597" id="Seg_9490" s="T596">adv</ta>
            <ta e="T598" id="Seg_9491" s="T597">adj</ta>
            <ta e="T600" id="Seg_9492" s="T599">ptcl</ta>
            <ta e="T604" id="Seg_9493" s="T603">num</ta>
            <ta e="T605" id="Seg_9494" s="T604">n</ta>
            <ta e="T606" id="Seg_9495" s="T605">v</ta>
            <ta e="T608" id="Seg_9496" s="T607">adv</ta>
            <ta e="T609" id="Seg_9497" s="T608">dempro</ta>
            <ta e="T610" id="Seg_9498" s="T609">n</ta>
            <ta e="T611" id="Seg_9499" s="T610">que</ta>
            <ta e="T612" id="Seg_9500" s="T611">ptcl</ta>
            <ta e="T613" id="Seg_9501" s="T612">v</ta>
            <ta e="T614" id="Seg_9502" s="T613">v</ta>
            <ta e="T616" id="Seg_9503" s="T615">conj</ta>
            <ta e="T617" id="Seg_9504" s="T616">num</ta>
            <ta e="T618" id="Seg_9505" s="T617">n</ta>
            <ta e="T619" id="Seg_9506" s="T618">v</ta>
            <ta e="T621" id="Seg_9507" s="T620">ptcl</ta>
            <ta e="T622" id="Seg_9508" s="T621">v</ta>
            <ta e="T623" id="Seg_9509" s="T622">pers</ta>
            <ta e="T624" id="Seg_9510" s="T623">n</ta>
            <ta e="T625" id="Seg_9511" s="T624">pers</ta>
            <ta e="T626" id="Seg_9512" s="T625">n</ta>
            <ta e="T628" id="Seg_9513" s="T627">ptcl</ta>
            <ta e="T629" id="Seg_9514" s="T628">pers</ta>
            <ta e="T630" id="Seg_9515" s="T629">v</ta>
            <ta e="T631" id="Seg_9516" s="T630">n</ta>
            <ta e="T632" id="Seg_9517" s="T631">conj</ta>
            <ta e="T633" id="Seg_9518" s="T632">pers</ta>
            <ta e="T634" id="Seg_9519" s="T633">n</ta>
            <ta e="T636" id="Seg_9520" s="T635">interj</ta>
            <ta e="T637" id="Seg_9521" s="T636">n</ta>
            <ta e="T639" id="Seg_9522" s="T638">que</ta>
            <ta e="T640" id="Seg_9523" s="T639">ptcl</ta>
            <ta e="T642" id="Seg_9524" s="T641">ptcl</ta>
            <ta e="T643" id="Seg_9525" s="T642">pers</ta>
            <ta e="T644" id="Seg_9526" s="T643">n</ta>
            <ta e="T646" id="Seg_9527" s="T645">n</ta>
            <ta e="T648" id="Seg_9528" s="T647">que</ta>
            <ta e="T649" id="Seg_9529" s="T648">ptcl</ta>
            <ta e="T650" id="Seg_9530" s="T649">v</ta>
            <ta e="T652" id="Seg_9531" s="T651">ptcl</ta>
            <ta e="T653" id="Seg_9532" s="T652">ptcl</ta>
            <ta e="T654" id="Seg_9533" s="T653">ptcl</ta>
            <ta e="T655" id="Seg_9534" s="T654">v</ta>
            <ta e="T656" id="Seg_9535" s="T655">adj</ta>
            <ta e="T658" id="Seg_9536" s="T657">conj</ta>
            <ta e="T659" id="Seg_9537" s="T658">pers</ta>
            <ta e="T660" id="Seg_9538" s="T659">ptcl</ta>
            <ta e="T661" id="Seg_9539" s="T660">n</ta>
            <ta e="T662" id="Seg_9540" s="T661">v</ta>
            <ta e="T663" id="Seg_9541" s="T662">n</ta>
            <ta e="T664" id="Seg_9542" s="T663">conj</ta>
            <ta e="T665" id="Seg_9543" s="T664">pers</ta>
            <ta e="T666" id="Seg_9544" s="T665">n</ta>
            <ta e="T668" id="Seg_9545" s="T667">v</ta>
            <ta e="T669" id="Seg_9546" s="T668">pers</ta>
            <ta e="T670" id="Seg_9547" s="T669">v</ta>
            <ta e="T671" id="Seg_9548" s="T670">n</ta>
            <ta e="T672" id="Seg_9549" s="T671">conj</ta>
            <ta e="T673" id="Seg_9550" s="T672">pers</ta>
            <ta e="T674" id="Seg_9551" s="T673">v</ta>
            <ta e="T675" id="Seg_9552" s="T674">n</ta>
            <ta e="T677" id="Seg_9553" s="T676">conj</ta>
            <ta e="T678" id="Seg_9554" s="T677">n</ta>
            <ta e="T679" id="Seg_9555" s="T678">pers</ta>
            <ta e="T681" id="Seg_9556" s="T680">conj</ta>
            <ta e="T682" id="Seg_9557" s="T681">n</ta>
            <ta e="T683" id="Seg_9558" s="T682">conj</ta>
            <ta e="T684" id="Seg_9559" s="T683">n</ta>
            <ta e="T685" id="Seg_9560" s="T684">pers</ta>
            <ta e="T687" id="Seg_9561" s="T686">conj</ta>
            <ta e="T688" id="Seg_9562" s="T687">adv</ta>
            <ta e="T689" id="Seg_9563" s="T688">n</ta>
            <ta e="T690" id="Seg_9564" s="T689">ptcl</ta>
            <ta e="T691" id="Seg_9565" s="T690">v</ta>
            <ta e="T700" id="Seg_9566" s="T699">ptcl</ta>
            <ta e="T702" id="Seg_9567" s="T701">ptcl</ta>
            <ta e="T704" id="Seg_9568" s="T703">ptcl</ta>
            <ta e="T705" id="Seg_9569" s="T704">n</ta>
            <ta e="T707" id="Seg_9570" s="T706">adj</ta>
            <ta e="T708" id="Seg_9571" s="T707">n</ta>
            <ta e="T709" id="Seg_9572" s="T708">v</ta>
            <ta e="T711" id="Seg_9573" s="T710">pers</ta>
            <ta e="T712" id="Seg_9574" s="T711">n</ta>
            <ta e="T713" id="Seg_9575" s="T712">v</ta>
            <ta e="T715" id="Seg_9576" s="T714">conj</ta>
            <ta e="T716" id="Seg_9577" s="T715">que</ta>
            <ta e="T719" id="Seg_9578" s="T718">que</ta>
            <ta e="T720" id="Seg_9579" s="T719">que</ta>
            <ta e="T722" id="Seg_9580" s="T721">dempro</ta>
            <ta e="T723" id="Seg_9581" s="T722">v</ta>
            <ta e="T724" id="Seg_9582" s="T723">v</ta>
            <ta e="T726" id="Seg_9583" s="T725">refl</ta>
            <ta e="T727" id="Seg_9584" s="T726">n</ta>
            <ta e="T728" id="Seg_9585" s="T727">dempro</ta>
            <ta e="T729" id="Seg_9586" s="T728">ptcl</ta>
            <ta e="T730" id="Seg_9587" s="T729">v</ta>
            <ta e="T732" id="Seg_9588" s="T731">conj</ta>
            <ta e="T733" id="Seg_9589" s="T732">que</ta>
            <ta e="T734" id="Seg_9590" s="T733">v</ta>
            <ta e="T736" id="Seg_9591" s="T735">ptcl</ta>
            <ta e="T737" id="Seg_9592" s="T736">que</ta>
            <ta e="T739" id="Seg_9593" s="T738">v</ta>
            <ta e="T741" id="Seg_9594" s="T740">conj</ta>
            <ta e="T742" id="Seg_9595" s="T741">ptcl</ta>
            <ta e="T744" id="Seg_9596" s="T743">ptcl</ta>
            <ta e="T745" id="Seg_9597" s="T744">v</ta>
            <ta e="T747" id="Seg_9598" s="T746">ptcl</ta>
            <ta e="T748" id="Seg_9599" s="T747">dempro</ta>
            <ta e="T749" id="Seg_9600" s="T748">conj</ta>
            <ta e="T750" id="Seg_9601" s="T749">v</ta>
            <ta e="T752" id="Seg_9602" s="T751">conj</ta>
            <ta e="T753" id="Seg_9603" s="T752">que</ta>
            <ta e="T755" id="Seg_9604" s="T754">ptcl</ta>
            <ta e="T756" id="Seg_9605" s="T755">pers</ta>
            <ta e="T757" id="Seg_9606" s="T756">v</ta>
            <ta e="T758" id="Seg_9607" s="T757">que</ta>
            <ta e="T760" id="Seg_9608" s="T759">pers</ta>
            <ta e="T761" id="Seg_9609" s="T760">ptcl</ta>
            <ta e="T762" id="Seg_9610" s="T761">ptcl</ta>
            <ta e="T763" id="Seg_9611" s="T762">v</ta>
            <ta e="T767" id="Seg_9612" s="T766">v</ta>
            <ta e="T768" id="Seg_9613" s="T767">n</ta>
            <ta e="T769" id="Seg_9614" s="T768">v</ta>
            <ta e="T771" id="Seg_9615" s="T770">n</ta>
            <ta e="T772" id="Seg_9616" s="T771">adj</ta>
            <ta e="T774" id="Seg_9617" s="T773">conj</ta>
            <ta e="T775" id="Seg_9618" s="T774">que</ta>
            <ta e="T777" id="Seg_9619" s="T776">ptcl</ta>
            <ta e="T779" id="Seg_9620" s="T778">interj</ta>
            <ta e="T780" id="Seg_9621" s="T779">ptcl</ta>
            <ta e="T781" id="Seg_9622" s="T780">que</ta>
            <ta e="T783" id="Seg_9623" s="T782">conj</ta>
            <ta e="T784" id="Seg_9624" s="T783">pers</ta>
            <ta e="T785" id="Seg_9625" s="T784">adv</ta>
            <ta e="T786" id="Seg_9626" s="T785">v</ta>
            <ta e="T787" id="Seg_9627" s="T786">v</ta>
            <ta e="T788" id="Seg_9628" s="T787">v</ta>
            <ta e="T790" id="Seg_9629" s="T789">conj</ta>
            <ta e="T791" id="Seg_9630" s="T790">pers</ta>
            <ta e="T792" id="Seg_9631" s="T791">v</ta>
            <ta e="T794" id="Seg_9632" s="T793">adj</ta>
            <ta e="T795" id="Seg_9633" s="T794">dempro</ta>
            <ta e="T801" id="Seg_9634" s="T800">pers</ta>
            <ta e="T802" id="Seg_9635" s="T801">dempro</ta>
            <ta e="T803" id="Seg_9636" s="T802">v</ta>
            <ta e="T804" id="Seg_9637" s="T803">adv</ta>
            <ta e="T807" id="Seg_9638" s="T806">conj</ta>
            <ta e="T808" id="Seg_9639" s="T807">dempro</ta>
            <ta e="T809" id="Seg_9640" s="T808">pers</ta>
            <ta e="T810" id="Seg_9641" s="T809">adv</ta>
            <ta e="T811" id="Seg_9642" s="T810">adv</ta>
            <ta e="T812" id="Seg_9643" s="T811">v</ta>
            <ta e="T816" id="Seg_9644" s="T815">ptcl</ta>
            <ta e="T817" id="Seg_9645" s="T816">v</ta>
            <ta e="T818" id="Seg_9646" s="T817">pers</ta>
            <ta e="T820" id="Seg_9647" s="T819">que</ta>
            <ta e="T822" id="Seg_9648" s="T821">conj</ta>
            <ta e="T823" id="Seg_9649" s="T822">adv</ta>
            <ta e="T825" id="Seg_9650" s="T824">conj</ta>
            <ta e="T826" id="Seg_9651" s="T825">n</ta>
            <ta e="T828" id="Seg_9652" s="T827">n</ta>
            <ta e="T829" id="Seg_9653" s="T828">ptcl</ta>
            <ta e="T831" id="Seg_9654" s="T830">ptcl</ta>
            <ta e="T833" id="Seg_9655" s="T832">ptcl</ta>
            <ta e="T834" id="Seg_9656" s="T833">n</ta>
            <ta e="T835" id="Seg_9657" s="T834">n</ta>
            <ta e="T837" id="Seg_9658" s="T836">conj</ta>
            <ta e="T838" id="Seg_9659" s="T837">pers</ta>
            <ta e="T839" id="Seg_9660" s="T838">num</ta>
            <ta e="T840" id="Seg_9661" s="T839">v</ta>
            <ta e="T842" id="Seg_9662" s="T841">conj</ta>
            <ta e="T843" id="Seg_9663" s="T842">que</ta>
            <ta e="T845" id="Seg_9664" s="T844">conj</ta>
            <ta e="T846" id="Seg_9665" s="T845">adv</ta>
            <ta e="T847" id="Seg_9666" s="T846">adv</ta>
            <ta e="T849" id="Seg_9667" s="T848">que</ta>
            <ta e="T850" id="Seg_9668" s="T849">n</ta>
            <ta e="T852" id="Seg_9669" s="T851">conj</ta>
            <ta e="T853" id="Seg_9670" s="T852">que</ta>
            <ta e="T854" id="Seg_9671" s="T853">dempro</ta>
            <ta e="T855" id="Seg_9672" s="T854">adv</ta>
            <ta e="T857" id="Seg_9673" s="T856">que</ta>
            <ta e="T858" id="Seg_9674" s="T857">que</ta>
            <ta e="T859" id="Seg_9675" s="T858">v</ta>
            <ta e="T861" id="Seg_9676" s="T860">conj</ta>
            <ta e="T865" id="Seg_9677" s="T864">v</ta>
            <ta e="T866" id="Seg_9678" s="T865">que</ta>
            <ta e="T868" id="Seg_9679" s="T867">conj</ta>
            <ta e="T869" id="Seg_9680" s="T868">pers</ta>
            <ta e="T870" id="Seg_9681" s="T869">pers</ta>
            <ta e="T871" id="Seg_9682" s="T870">que</ta>
            <ta e="T872" id="Seg_9683" s="T871">ptcl</ta>
            <ta e="T873" id="Seg_9684" s="T872">v</ta>
            <ta e="T875" id="Seg_9685" s="T874">conj</ta>
            <ta e="T876" id="Seg_9686" s="T875">que</ta>
            <ta e="T877" id="Seg_9687" s="T876">ptcl</ta>
            <ta e="T878" id="Seg_9688" s="T877">v</ta>
            <ta e="T880" id="Seg_9689" s="T879">ptcl</ta>
            <ta e="T882" id="Seg_9690" s="T881">conj</ta>
            <ta e="T883" id="Seg_9691" s="T882">v</ta>
            <ta e="T885" id="Seg_9692" s="T884">adj</ta>
            <ta e="T886" id="Seg_9693" s="T885">n</ta>
            <ta e="T887" id="Seg_9694" s="T886">v</ta>
            <ta e="T889" id="Seg_9695" s="T888">que</ta>
            <ta e="T890" id="Seg_9696" s="T889">ptcl</ta>
            <ta e="T892" id="Seg_9697" s="T891">ptcl</ta>
            <ta e="T893" id="Seg_9698" s="T892">adj</ta>
            <ta e="T894" id="Seg_9699" s="T893">n</ta>
            <ta e="T896" id="Seg_9700" s="T895">ptcl</ta>
            <ta e="T897" id="Seg_9701" s="T896">adj</ta>
            <ta e="T898" id="Seg_9702" s="T897">n</ta>
            <ta e="T900" id="Seg_9703" s="T899">ptcl</ta>
            <ta e="T901" id="Seg_9704" s="T900">pers</ta>
            <ta e="T902" id="Seg_9705" s="T901">ptcl</ta>
            <ta e="T903" id="Seg_9706" s="T902">v</ta>
            <ta e="T904" id="Seg_9707" s="T903">pers</ta>
            <ta e="T908" id="Seg_9708" s="T907">pers</ta>
            <ta e="T909" id="Seg_9709" s="T908">n</ta>
            <ta e="T910" id="Seg_9710" s="T909">pers</ta>
            <ta e="T911" id="Seg_9711" s="T910">n</ta>
            <ta e="T912" id="Seg_9712" s="T911">v</ta>
            <ta e="T914" id="Seg_9713" s="T913">que</ta>
            <ta e="T915" id="Seg_9714" s="T914">dempro</ta>
            <ta e="T916" id="Seg_9715" s="T915">v</ta>
            <ta e="T918" id="Seg_9716" s="T917">ptcl</ta>
            <ta e="T919" id="Seg_9717" s="T918">dempro</ta>
            <ta e="T920" id="Seg_9718" s="T919">v</ta>
            <ta e="T921" id="Seg_9719" s="T920">pers</ta>
            <ta e="T922" id="Seg_9720" s="T921">n</ta>
            <ta e="T924" id="Seg_9721" s="T922">v</ta>
            <ta e="T925" id="Seg_9722" s="T924">pers</ta>
            <ta e="T926" id="Seg_9723" s="T925">n</ta>
            <ta e="T927" id="Seg_9724" s="T926">adj</ta>
            <ta e="T928" id="Seg_9725" s="T927">adj</ta>
            <ta e="T930" id="Seg_9726" s="T929">conj</ta>
            <ta e="T931" id="Seg_9727" s="T930">pers</ta>
            <ta e="T932" id="Seg_9728" s="T931">n</ta>
            <ta e="T933" id="Seg_9729" s="T932">ptcl</ta>
            <ta e="T934" id="Seg_9730" s="T933">adj</ta>
            <ta e="T936" id="Seg_9731" s="T935">conj</ta>
            <ta e="T937" id="Seg_9732" s="T936">pers</ta>
            <ta e="T938" id="Seg_9733" s="T937">n</ta>
            <ta e="T940" id="Seg_9734" s="T938">v</ta>
            <ta e="T941" id="Seg_9735" s="T940">interj</ta>
            <ta e="T942" id="Seg_9736" s="T941">pers</ta>
            <ta e="T943" id="Seg_9737" s="T942">n</ta>
            <ta e="T944" id="Seg_9738" s="T943">adj</ta>
            <ta e="T946" id="Seg_9739" s="T945">conj</ta>
            <ta e="T947" id="Seg_9740" s="T946">adj</ta>
            <ta e="T949" id="Seg_9741" s="T948">conj</ta>
            <ta e="T950" id="Seg_9742" s="T949">ptcl</ta>
            <ta e="T951" id="Seg_9743" s="T950">que</ta>
            <ta e="T952" id="Seg_9744" s="T951">ptcl</ta>
            <ta e="T953" id="Seg_9745" s="T952">que</ta>
            <ta e="T954" id="Seg_9746" s="T953">v</ta>
            <ta e="T956" id="Seg_9747" s="T955">conj</ta>
            <ta e="T957" id="Seg_9748" s="T956">pers</ta>
            <ta e="T958" id="Seg_9749" s="T957">n</ta>
            <ta e="T959" id="Seg_9750" s="T958">ptcl</ta>
            <ta e="T960" id="Seg_9751" s="T959">adj</ta>
            <ta e="T962" id="Seg_9752" s="T961">que</ta>
            <ta e="T963" id="Seg_9753" s="T962">ptcl</ta>
            <ta e="T964" id="Seg_9754" s="T963">v</ta>
            <ta e="T965" id="Seg_9755" s="T964">pers</ta>
            <ta e="T967" id="Seg_9756" s="T966">conj</ta>
            <ta e="T968" id="Seg_9757" s="T967">pers</ta>
            <ta e="T969" id="Seg_9758" s="T968">n</ta>
            <ta e="T970" id="Seg_9759" s="T969">ptcl</ta>
            <ta e="T971" id="Seg_9760" s="T970">que</ta>
            <ta e="T972" id="Seg_9761" s="T971">ptcl</ta>
            <ta e="T974" id="Seg_9762" s="T973">pers</ta>
            <ta e="T975" id="Seg_9763" s="T974">adv</ta>
            <ta e="T976" id="Seg_9764" s="T975">v</ta>
            <ta e="T978" id="Seg_9765" s="T977">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_9766" s="T2">np.h:Th</ta>
            <ta e="T4" id="Seg_9767" s="T3">0.3.h:Th</ta>
            <ta e="T5" id="Seg_9768" s="T4">0.3.h:P</ta>
            <ta e="T8" id="Seg_9769" s="T7">0.3.h:P</ta>
            <ta e="T13" id="Seg_9770" s="T12">np.h:P</ta>
            <ta e="T21" id="Seg_9771" s="T20">np.h:P</ta>
            <ta e="T27" id="Seg_9772" s="T26">pro.h:Poss</ta>
            <ta e="T28" id="Seg_9773" s="T27">np.h:E</ta>
            <ta e="T32" id="Seg_9774" s="T31">adv:Time</ta>
            <ta e="T37" id="Seg_9775" s="T36">0.3.h:E</ta>
            <ta e="T39" id="Seg_9776" s="T38">adv:Time</ta>
            <ta e="T43" id="Seg_9777" s="T42">0.3.h:E</ta>
            <ta e="T46" id="Seg_9778" s="T45">adv:Time</ta>
            <ta e="T47" id="Seg_9779" s="T46">0.3.h:P</ta>
            <ta e="T52" id="Seg_9780" s="T51">pro.h:E</ta>
            <ta e="T59" id="Seg_9781" s="T58">0.1.h:E</ta>
            <ta e="T61" id="Seg_9782" s="T60">pro.h:Poss</ta>
            <ta e="T62" id="Seg_9783" s="T61">np:Th</ta>
            <ta e="T66" id="Seg_9784" s="T65">np:Th</ta>
            <ta e="T67" id="Seg_9785" s="T66">pro.h:Poss</ta>
            <ta e="T69" id="Seg_9786" s="T68">pro.h:A</ta>
            <ta e="T70" id="Seg_9787" s="T69">pro.h:Poss</ta>
            <ta e="T71" id="Seg_9788" s="T70">pro:P</ta>
            <ta e="T74" id="Seg_9789" s="T73">adv:Time</ta>
            <ta e="T76" id="Seg_9790" s="T75">0.1.h:E</ta>
            <ta e="T81" id="Seg_9791" s="T80">adv:Time</ta>
            <ta e="T83" id="Seg_9792" s="T82">0.1.h:A</ta>
            <ta e="T85" id="Seg_9793" s="T84">0.1.h:E</ta>
            <ta e="T87" id="Seg_9794" s="T86">np.h:P</ta>
            <ta e="T90" id="Seg_9795" s="T89">0.1.h:A</ta>
            <ta e="T93" id="Seg_9796" s="T92">np.h:Com</ta>
            <ta e="T96" id="Seg_9797" s="T95">0.1.h:E</ta>
            <ta e="T98" id="Seg_9798" s="T97">0.1.h:E</ta>
            <ta e="T102" id="Seg_9799" s="T101">0.1.h:A</ta>
            <ta e="T105" id="Seg_9800" s="T104">0.1.h:A</ta>
            <ta e="T111" id="Seg_9801" s="T110">pro:G</ta>
            <ta e="T112" id="Seg_9802" s="T111">pro.h:A</ta>
            <ta e="T115" id="Seg_9803" s="T114">pro:G</ta>
            <ta e="T117" id="Seg_9804" s="T116">0.1.h:A</ta>
            <ta e="T119" id="Seg_9805" s="T118">0.1.h:P</ta>
            <ta e="T124" id="Seg_9806" s="T123">np.h:E</ta>
            <ta e="T129" id="Seg_9807" s="T128">adv:Time</ta>
            <ta e="T133" id="Seg_9808" s="T132">pro:G</ta>
            <ta e="T136" id="Seg_9809" s="T135">adv:Time</ta>
            <ta e="T138" id="Seg_9810" s="T137">adv:L</ta>
            <ta e="T140" id="Seg_9811" s="T139">0.3.h:A</ta>
            <ta e="T145" id="Seg_9812" s="T144">0.2.h:A</ta>
            <ta e="T152" id="Seg_9813" s="T151">0.3.h:A</ta>
            <ta e="T153" id="Seg_9814" s="T152">adv:L</ta>
            <ta e="T156" id="Seg_9815" s="T155">adv:L</ta>
            <ta e="T157" id="Seg_9816" s="T156">0.3.h:A</ta>
            <ta e="T160" id="Seg_9817" s="T159">pro.h:A</ta>
            <ta e="T167" id="Seg_9818" s="T166">pro.h:A</ta>
            <ta e="T171" id="Seg_9819" s="T170">pro.h:A</ta>
            <ta e="T172" id="Seg_9820" s="T171">pro:G</ta>
            <ta e="T179" id="Seg_9821" s="T178">pro:P</ta>
            <ta e="T183" id="Seg_9822" s="T182">pro.h:A</ta>
            <ta e="T191" id="Seg_9823" s="T190">pro:P</ta>
            <ta e="T192" id="Seg_9824" s="T191">0.2.h:A</ta>
            <ta e="T199" id="Seg_9825" s="T198">0.2.h:A</ta>
            <ta e="T205" id="Seg_9826" s="T204">pro.h:A</ta>
            <ta e="T213" id="Seg_9827" s="T212">0.1.h:A</ta>
            <ta e="T218" id="Seg_9828" s="T217">np:Th</ta>
            <ta e="T220" id="Seg_9829" s="T219">np:Th</ta>
            <ta e="T223" id="Seg_9830" s="T222">np:Th</ta>
            <ta e="T229" id="Seg_9831" s="T228">pro:G</ta>
            <ta e="T231" id="Seg_9832" s="T230">0.3.h:A</ta>
            <ta e="T234" id="Seg_9833" s="T233">pro.h:Poss</ta>
            <ta e="T235" id="Seg_9834" s="T234">np.h:Th</ta>
            <ta e="T239" id="Seg_9835" s="T238">pro.h:Th</ta>
            <ta e="T241" id="Seg_9836" s="T240">0.3.h:A</ta>
            <ta e="T248" id="Seg_9837" s="T247">0.3.h:A</ta>
            <ta e="T251" id="Seg_9838" s="T250">np.h:Th</ta>
            <ta e="T252" id="Seg_9839" s="T251">pro.h:Th</ta>
            <ta e="T259" id="Seg_9840" s="T258">pro.h:Th</ta>
            <ta e="T261" id="Seg_9841" s="T260">np.h:Th</ta>
            <ta e="T263" id="Seg_9842" s="T262">pro:Th</ta>
            <ta e="T264" id="Seg_9843" s="T263">pro.h:R</ta>
            <ta e="T267" id="Seg_9844" s="T266">0.1.h:A</ta>
            <ta e="T268" id="Seg_9845" s="T267">0.1.h:A</ta>
            <ta e="T274" id="Seg_9846" s="T273">0.3:P</ta>
            <ta e="T279" id="Seg_9847" s="T278">0.3.h:A</ta>
            <ta e="T280" id="Seg_9848" s="T279">pro.h:R</ta>
            <ta e="T282" id="Seg_9849" s="T281">pro.h:A</ta>
            <ta e="T283" id="Seg_9850" s="T282">pro.h:E</ta>
            <ta e="T294" id="Seg_9851" s="T293">pro.h:A</ta>
            <ta e="T295" id="Seg_9852" s="T294">pro.h:E</ta>
            <ta e="T297" id="Seg_9853" s="T296">adv:Time</ta>
            <ta e="T301" id="Seg_9854" s="T300">pro.h:A</ta>
            <ta e="T303" id="Seg_9855" s="T302">np:P</ta>
            <ta e="T312" id="Seg_9856" s="T311">pro.h:Th</ta>
            <ta e="T314" id="Seg_9857" s="T313">np.h:Th</ta>
            <ta e="T319" id="Seg_9858" s="T318">0.1.h:A</ta>
            <ta e="T320" id="Seg_9859" s="T319">pro:Th</ta>
            <ta e="T324" id="Seg_9860" s="T323">pro.h:B</ta>
            <ta e="T325" id="Seg_9861" s="T324">0.2.h:A</ta>
            <ta e="T328" id="Seg_9862" s="T327">adv:Time</ta>
            <ta e="T330" id="Seg_9863" s="T329">pro.h:A</ta>
            <ta e="T331" id="Seg_9864" s="T330">adv:Time</ta>
            <ta e="T336" id="Seg_9865" s="T335">adv:L</ta>
            <ta e="T338" id="Seg_9866" s="T337">0.2.h:A</ta>
            <ta e="T343" id="Seg_9867" s="T342">pro.h:A</ta>
            <ta e="T345" id="Seg_9868" s="T344">pro.h:Th</ta>
            <ta e="T346" id="Seg_9869" s="T345">adv:Time</ta>
            <ta e="T349" id="Seg_9870" s="T348">np:G</ta>
            <ta e="T352" id="Seg_9871" s="T351">pro.h:E</ta>
            <ta e="T353" id="Seg_9872" s="T352">adv:L</ta>
            <ta e="T357" id="Seg_9873" s="T356">adv:Time</ta>
            <ta e="T360" id="Seg_9874" s="T359">pro:G</ta>
            <ta e="T361" id="Seg_9875" s="T360">np.h:A</ta>
            <ta e="T374" id="Seg_9876" s="T373">0.3.h:A</ta>
            <ta e="T377" id="Seg_9877" s="T376">np.h:A</ta>
            <ta e="T382" id="Seg_9878" s="T381">np.h:E</ta>
            <ta e="T389" id="Seg_9879" s="T388">np.h:A</ta>
            <ta e="T391" id="Seg_9880" s="T390">np.h:Poss</ta>
            <ta e="T393" id="Seg_9881" s="T392">np:Th</ta>
            <ta e="T394" id="Seg_9882" s="T393">np:Th</ta>
            <ta e="T397" id="Seg_9883" s="T396">pro:Th</ta>
            <ta e="T400" id="Seg_9884" s="T399">np:Th</ta>
            <ta e="T403" id="Seg_9885" s="T402">np.h:Poss</ta>
            <ta e="T406" id="Seg_9886" s="T405">0.3.h:A</ta>
            <ta e="T408" id="Seg_9887" s="T407">pro:Th</ta>
            <ta e="T409" id="Seg_9888" s="T408">pro.h:A</ta>
            <ta e="T413" id="Seg_9889" s="T412">np:Th pro.h:Poss</ta>
            <ta e="T416" id="Seg_9890" s="T415">np:Th pro.h:Poss</ta>
            <ta e="T420" id="Seg_9891" s="T419">pro.h:A</ta>
            <ta e="T424" id="Seg_9892" s="T423">pro.h:A</ta>
            <ta e="T425" id="Seg_9893" s="T424">np:Th pro.h:Poss</ta>
            <ta e="T429" id="Seg_9894" s="T428">pro.h:A</ta>
            <ta e="T430" id="Seg_9895" s="T429">np:Th pro.h:Poss</ta>
            <ta e="T436" id="Seg_9896" s="T435">np:Th pro.h:Poss</ta>
            <ta e="T437" id="Seg_9897" s="T436">0.2.h:A</ta>
            <ta e="T442" id="Seg_9898" s="T441">0.2.h:A</ta>
            <ta e="T463" id="Seg_9899" s="T462">0.2.h:A</ta>
            <ta e="T464" id="Seg_9900" s="T463">pro.h:Com</ta>
            <ta e="T479" id="Seg_9901" s="T478">0.2.h:A</ta>
            <ta e="T480" id="Seg_9902" s="T479">pro.h:Com</ta>
            <ta e="T484" id="Seg_9903" s="T483">pro:Th</ta>
            <ta e="T485" id="Seg_9904" s="T484">pro.h:Poss</ta>
            <ta e="T489" id="Seg_9905" s="T488">pro.h:Poss</ta>
            <ta e="T491" id="Seg_9906" s="T490">np:Th</ta>
            <ta e="T492" id="Seg_9907" s="T491">np:Th</ta>
            <ta e="T494" id="Seg_9908" s="T493">np:Th</ta>
            <ta e="T498" id="Seg_9909" s="T497">0.2.h:A</ta>
            <ta e="T501" id="Seg_9910" s="T500">pro.h:A</ta>
            <ta e="T503" id="Seg_9911" s="T502">np:P</ta>
            <ta e="T509" id="Seg_9912" s="T508">np:Th</ta>
            <ta e="T510" id="Seg_9913" s="T509">pro.h:A</ta>
            <ta e="T512" id="Seg_9914" s="T511">pro.h:R</ta>
            <ta e="T517" id="Seg_9915" s="T516">0.2.h:A</ta>
            <ta e="T521" id="Seg_9916" s="T520">pro.h:A</ta>
            <ta e="T526" id="Seg_9917" s="T525">0.1.h:A</ta>
            <ta e="T529" id="Seg_9918" s="T528">0.1.h:A</ta>
            <ta e="T536" id="Seg_9919" s="T535">pro.h:A</ta>
            <ta e="T537" id="Seg_9920" s="T536">pro.h:Poss</ta>
            <ta e="T538" id="Seg_9921" s="T537">np:Th</ta>
            <ta e="T541" id="Seg_9922" s="T540">np:Ins</ta>
            <ta e="T545" id="Seg_9923" s="T544">pro.h:Poss</ta>
            <ta e="T546" id="Seg_9924" s="T545">np:Th</ta>
            <ta e="T549" id="Seg_9925" s="T548">np:Th pro.h:Poss</ta>
            <ta e="T554" id="Seg_9926" s="T553">0.2.h:Th</ta>
            <ta e="T566" id="Seg_9927" s="T565">pro.h:E</ta>
            <ta e="T570" id="Seg_9928" s="T569">np:E</ta>
            <ta e="T574" id="Seg_9929" s="T573">np:E</ta>
            <ta e="T579" id="Seg_9930" s="T578">pro.h:P</ta>
            <ta e="T605" id="Seg_9931" s="T604">np.h:E</ta>
            <ta e="T608" id="Seg_9932" s="T607">adv:Time</ta>
            <ta e="T610" id="Seg_9933" s="T609">np.h:A</ta>
            <ta e="T611" id="Seg_9934" s="T610">pro:G</ta>
            <ta e="T617" id="Seg_9935" s="T616">np.h:E</ta>
            <ta e="T618" id="Seg_9936" s="T617">np:L</ta>
            <ta e="T622" id="Seg_9937" s="T621">0.3.h:A</ta>
            <ta e="T623" id="Seg_9938" s="T622">pro.h:B</ta>
            <ta e="T624" id="Seg_9939" s="T623">np:Th</ta>
            <ta e="T625" id="Seg_9940" s="T624">pro.h:B</ta>
            <ta e="T626" id="Seg_9941" s="T625">np:Th</ta>
            <ta e="T629" id="Seg_9942" s="T628">pro.h:A</ta>
            <ta e="T631" id="Seg_9943" s="T630">np:Th</ta>
            <ta e="T633" id="Seg_9944" s="T632">pro.h:A</ta>
            <ta e="T634" id="Seg_9945" s="T633">np:Th</ta>
            <ta e="T650" id="Seg_9946" s="T649">0.2.h:A</ta>
            <ta e="T659" id="Seg_9947" s="T658">pro.h:Poss</ta>
            <ta e="T661" id="Seg_9948" s="T660">np.h:A</ta>
            <ta e="T663" id="Seg_9949" s="T662">np:Th</ta>
            <ta e="T665" id="Seg_9950" s="T664">pro.h:R</ta>
            <ta e="T666" id="Seg_9951" s="T665">np:Th</ta>
            <ta e="T668" id="Seg_9952" s="T667">0.3.h:A</ta>
            <ta e="T669" id="Seg_9953" s="T668">pro.h:A</ta>
            <ta e="T671" id="Seg_9954" s="T670">np:Th</ta>
            <ta e="T673" id="Seg_9955" s="T672">pro.h:A</ta>
            <ta e="T675" id="Seg_9956" s="T674">np:Th</ta>
            <ta e="T678" id="Seg_9957" s="T677">np:Th</ta>
            <ta e="T679" id="Seg_9958" s="T678">pro.h:Poss</ta>
            <ta e="T682" id="Seg_9959" s="T681">np:Th</ta>
            <ta e="T684" id="Seg_9960" s="T683">np:Th</ta>
            <ta e="T685" id="Seg_9961" s="T684">pro.h:Poss</ta>
            <ta e="T688" id="Seg_9962" s="T687">adv:Time</ta>
            <ta e="T689" id="Seg_9963" s="T688">np:Th</ta>
            <ta e="T691" id="Seg_9964" s="T690">0.3.h:A</ta>
            <ta e="T708" id="Seg_9965" s="T707">np.h:Th</ta>
            <ta e="T711" id="Seg_9966" s="T710">pro.h:Poss</ta>
            <ta e="T712" id="Seg_9967" s="T711">np.h:Th</ta>
            <ta e="T716" id="Seg_9968" s="T715">pro:G</ta>
            <ta e="T719" id="Seg_9969" s="T718">pro:G</ta>
            <ta e="T720" id="Seg_9970" s="T719">pro:G</ta>
            <ta e="T722" id="Seg_9971" s="T721">pro.h:E</ta>
            <ta e="T724" id="Seg_9972" s="T723">0.3.h:E</ta>
            <ta e="T727" id="Seg_9973" s="T726">np:So</ta>
            <ta e="T728" id="Seg_9974" s="T727">pro.h:Th</ta>
            <ta e="T730" id="Seg_9975" s="T729">0.3.h:A</ta>
            <ta e="T734" id="Seg_9976" s="T733">0.3.h:A</ta>
            <ta e="T739" id="Seg_9977" s="T738">0.3.h:A</ta>
            <ta e="T745" id="Seg_9978" s="T744">0.3.h:A</ta>
            <ta e="T748" id="Seg_9979" s="T747">pro.h:Th</ta>
            <ta e="T750" id="Seg_9980" s="T749">0.3.h:A</ta>
            <ta e="T753" id="Seg_9981" s="T752">pro:G</ta>
            <ta e="T756" id="Seg_9982" s="T755">pro.h:E</ta>
            <ta e="T758" id="Seg_9983" s="T757">pro:G</ta>
            <ta e="T760" id="Seg_9984" s="T759">pro.h:E</ta>
            <ta e="T767" id="Seg_9985" s="T766">0.1.h:A</ta>
            <ta e="T768" id="Seg_9986" s="T767">np:Th</ta>
            <ta e="T771" id="Seg_9987" s="T770">np:Th</ta>
            <ta e="T784" id="Seg_9988" s="T783">pro.h:A</ta>
            <ta e="T785" id="Seg_9989" s="T784">adv:Time</ta>
            <ta e="T788" id="Seg_9990" s="T787">0.1.h:A</ta>
            <ta e="T791" id="Seg_9991" s="T790">pro.h:A</ta>
            <ta e="T795" id="Seg_9992" s="T794">pro:Th</ta>
            <ta e="T801" id="Seg_9993" s="T800">pro.h:A</ta>
            <ta e="T802" id="Seg_9994" s="T801">pro.h:R</ta>
            <ta e="T804" id="Seg_9995" s="T803">adv:L</ta>
            <ta e="T808" id="Seg_9996" s="T807">pro.h:A</ta>
            <ta e="T809" id="Seg_9997" s="T808">pro.h:R</ta>
            <ta e="T810" id="Seg_9998" s="T809">np:Th</ta>
            <ta e="T811" id="Seg_9999" s="T810">np:Th</ta>
            <ta e="T817" id="Seg_10000" s="T816">0.1.h:A</ta>
            <ta e="T818" id="Seg_10001" s="T817">pro.h:Com</ta>
            <ta e="T820" id="Seg_10002" s="T819">pro:G</ta>
            <ta e="T823" id="Seg_10003" s="T822">adv:L</ta>
            <ta e="T826" id="Seg_10004" s="T825">0.2.h:Poss</ta>
            <ta e="T828" id="Seg_10005" s="T827">0.2.h:Poss</ta>
            <ta e="T834" id="Seg_10006" s="T833">0.1.h:A</ta>
            <ta e="T835" id="Seg_10007" s="T834">np.h:Th 0.1.h:Poss</ta>
            <ta e="T840" id="Seg_10008" s="T839">0.1.h:A</ta>
            <ta e="T843" id="Seg_10009" s="T842">pro:G</ta>
            <ta e="T846" id="Seg_10010" s="T845">adv:L</ta>
            <ta e="T847" id="Seg_10011" s="T846">adv:L</ta>
            <ta e="T849" id="Seg_10012" s="T848">pro:L</ta>
            <ta e="T850" id="Seg_10013" s="T849">np.h:Th</ta>
            <ta e="T853" id="Seg_10014" s="T852">pro:Th</ta>
            <ta e="T854" id="Seg_10015" s="T853">pro:Th</ta>
            <ta e="T855" id="Seg_10016" s="T854">adv:L</ta>
            <ta e="T858" id="Seg_10017" s="T857">pro:Th</ta>
            <ta e="T859" id="Seg_10018" s="T858">0.3.h:A</ta>
            <ta e="T865" id="Seg_10019" s="T864">0.3.h:A</ta>
            <ta e="T866" id="Seg_10020" s="T865">pro:Th</ta>
            <ta e="T870" id="Seg_10021" s="T869">pro.h:A</ta>
            <ta e="T871" id="Seg_10022" s="T870">pro:Th</ta>
            <ta e="T878" id="Seg_10023" s="T877">0.2.h:A</ta>
            <ta e="T886" id="Seg_10024" s="T885">np.h:Th</ta>
            <ta e="T887" id="Seg_10025" s="T886">0.1.h:Th</ta>
            <ta e="T894" id="Seg_10026" s="T893">np.h:Th</ta>
            <ta e="T898" id="Seg_10027" s="T897">np.h:Th</ta>
            <ta e="T901" id="Seg_10028" s="T900">pro.h:A</ta>
            <ta e="T904" id="Seg_10029" s="T903">pro:G</ta>
            <ta e="T908" id="Seg_10030" s="T907">pro.h:Poss</ta>
            <ta e="T909" id="Seg_10031" s="T908">np.h:A</ta>
            <ta e="T910" id="Seg_10032" s="T909">pro.h:Poss</ta>
            <ta e="T911" id="Seg_10033" s="T910">np.h:Th</ta>
            <ta e="T915" id="Seg_10034" s="T914">pro.h:A</ta>
            <ta e="T919" id="Seg_10035" s="T918">pro.h:A</ta>
            <ta e="T921" id="Seg_10036" s="T920">pro.h:Poss</ta>
            <ta e="T922" id="Seg_10037" s="T921">np.h:A</ta>
            <ta e="T925" id="Seg_10038" s="T924">pro.h:Poss</ta>
            <ta e="T926" id="Seg_10039" s="T925">np.h:Th</ta>
            <ta e="T931" id="Seg_10040" s="T930">pro.h:Poss</ta>
            <ta e="T932" id="Seg_10041" s="T931">np.h:Th</ta>
            <ta e="T937" id="Seg_10042" s="T936">pro.h:Poss</ta>
            <ta e="T938" id="Seg_10043" s="T937">np.h:A</ta>
            <ta e="T942" id="Seg_10044" s="T941">pro.h:Poss</ta>
            <ta e="T943" id="Seg_10045" s="T942">np.h:Th</ta>
            <ta e="T953" id="Seg_10046" s="T952">pro:Th</ta>
            <ta e="T954" id="Seg_10047" s="T953">0.3.h:A</ta>
            <ta e="T957" id="Seg_10048" s="T956">pro.h:Poss</ta>
            <ta e="T958" id="Seg_10049" s="T957">np.h:Th</ta>
            <ta e="T962" id="Seg_10050" s="T961">pro:Th</ta>
            <ta e="T964" id="Seg_10051" s="T963">0.3.h:A</ta>
            <ta e="T965" id="Seg_10052" s="T964">pro.h:R</ta>
            <ta e="T968" id="Seg_10053" s="T967">pro.h:Poss</ta>
            <ta e="T969" id="Seg_10054" s="T968">np.h:A</ta>
            <ta e="T971" id="Seg_10055" s="T970">pro:Th</ta>
            <ta e="T974" id="Seg_10056" s="T973">pro.h:R</ta>
            <ta e="T976" id="Seg_10057" s="T975">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_10058" s="T2">n:pred</ta>
            <ta e="T4" id="Seg_10059" s="T3">cop 0.3.h:S</ta>
            <ta e="T5" id="Seg_10060" s="T4">v:pred 0.3.h:S</ta>
            <ta e="T8" id="Seg_10061" s="T7">v:pred 0.3.h:S</ta>
            <ta e="T13" id="Seg_10062" s="T12">np.h:S</ta>
            <ta e="T17" id="Seg_10063" s="T16">v:pred</ta>
            <ta e="T21" id="Seg_10064" s="T20">np.h:S</ta>
            <ta e="T23" id="Seg_10065" s="T22">v:pred</ta>
            <ta e="T25" id="Seg_10066" s="T24">ptcl:pred</ta>
            <ta e="T28" id="Seg_10067" s="T27">np.h:S</ta>
            <ta e="T30" id="Seg_10068" s="T29">v:pred</ta>
            <ta e="T37" id="Seg_10069" s="T36">v:pred 0.3.h:S</ta>
            <ta e="T43" id="Seg_10070" s="T42">v:pred 0.3.h:S</ta>
            <ta e="T47" id="Seg_10071" s="T46">v:pred 0.3.h:S</ta>
            <ta e="T52" id="Seg_10072" s="T51">pro.h:S</ta>
            <ta e="T53" id="Seg_10073" s="T52">v:pred</ta>
            <ta e="T59" id="Seg_10074" s="T58">v:pred 0.1.h:S</ta>
            <ta e="T62" id="Seg_10075" s="T61">np:S</ta>
            <ta e="T63" id="Seg_10076" s="T62">v:pred</ta>
            <ta e="T65" id="Seg_10077" s="T64">v:pred</ta>
            <ta e="T66" id="Seg_10078" s="T65">np:S</ta>
            <ta e="T69" id="Seg_10079" s="T68">pro.h:S</ta>
            <ta e="T71" id="Seg_10080" s="T70">pro:O</ta>
            <ta e="T72" id="Seg_10081" s="T71">v:pred</ta>
            <ta e="T76" id="Seg_10082" s="T75">v:pred 0.1.h:S</ta>
            <ta e="T83" id="Seg_10083" s="T82">v:pred 0.1.h:S</ta>
            <ta e="T85" id="Seg_10084" s="T84">v:pred 0.1.h:S</ta>
            <ta e="T87" id="Seg_10085" s="T86">np.h:O</ta>
            <ta e="T90" id="Seg_10086" s="T89">v:pred 0.1.h:S</ta>
            <ta e="T96" id="Seg_10087" s="T95">v:pred 0.1.h:S</ta>
            <ta e="T98" id="Seg_10088" s="T97">v:pred 0.1.h:S</ta>
            <ta e="T102" id="Seg_10089" s="T101">v:pred 0.1.h:S</ta>
            <ta e="T105" id="Seg_10090" s="T104">v:pred 0.1.h:S</ta>
            <ta e="T112" id="Seg_10091" s="T111">pro.h:S</ta>
            <ta e="T116" id="Seg_10092" s="T115">ptcl.neg</ta>
            <ta e="T117" id="Seg_10093" s="T116">v:pred 0.1.h:S</ta>
            <ta e="T119" id="Seg_10094" s="T118">v:pred 0.1.h:S</ta>
            <ta e="T124" id="Seg_10095" s="T123">np.h:S</ta>
            <ta e="T126" id="Seg_10096" s="T125">v:pred</ta>
            <ta e="T131" id="Seg_10097" s="T130">v:pred</ta>
            <ta e="T140" id="Seg_10098" s="T139">v:pred 0.3.h:S</ta>
            <ta e="T145" id="Seg_10099" s="T144">v:pred 0.2.h:S</ta>
            <ta e="T152" id="Seg_10100" s="T151">v:pred 0.3.h:S</ta>
            <ta e="T157" id="Seg_10101" s="T156">v:pred 0.3.h:S</ta>
            <ta e="T160" id="Seg_10102" s="T159">pro.h:S</ta>
            <ta e="T162" id="Seg_10103" s="T161">ptcl.neg</ta>
            <ta e="T163" id="Seg_10104" s="T162">v:pred</ta>
            <ta e="T167" id="Seg_10105" s="T166">pro.h:S</ta>
            <ta e="T168" id="Seg_10106" s="T167">ptcl.neg</ta>
            <ta e="T169" id="Seg_10107" s="T168">v:pred</ta>
            <ta e="T171" id="Seg_10108" s="T170">pro.h:S</ta>
            <ta e="T173" id="Seg_10109" s="T172">v:pred</ta>
            <ta e="T175" id="Seg_10110" s="T174">ptcl:pred</ta>
            <ta e="T179" id="Seg_10111" s="T178">pro:O</ta>
            <ta e="T181" id="Seg_10112" s="T180">ptcl:pred</ta>
            <ta e="T183" id="Seg_10113" s="T182">pro.h:S</ta>
            <ta e="T185" id="Seg_10114" s="T184">v:pred</ta>
            <ta e="T191" id="Seg_10115" s="T190">pro:O</ta>
            <ta e="T192" id="Seg_10116" s="T191">v:pred 0.2.h:S</ta>
            <ta e="T199" id="Seg_10117" s="T198">0.2.h:S</ta>
            <ta e="T200" id="Seg_10118" s="T199">ptcl:pred</ta>
            <ta e="T205" id="Seg_10119" s="T204">pro.h:S</ta>
            <ta e="T206" id="Seg_10120" s="T205">ptcl:pred</ta>
            <ta e="T210" id="Seg_10121" s="T209">ptcl:pred</ta>
            <ta e="T213" id="Seg_10122" s="T212">v:pred 0.1.h:S</ta>
            <ta e="T218" id="Seg_10123" s="T217">np:S</ta>
            <ta e="T220" id="Seg_10124" s="T219">n:pred</ta>
            <ta e="T223" id="Seg_10125" s="T222">np:S</ta>
            <ta e="T224" id="Seg_10126" s="T223">adj:pred</ta>
            <ta e="T230" id="Seg_10127" s="T229">ptcl.neg</ta>
            <ta e="T231" id="Seg_10128" s="T230">v:pred 0.3.h:S</ta>
            <ta e="T235" id="Seg_10129" s="T234">np.h:S</ta>
            <ta e="T236" id="Seg_10130" s="T235">adj:pred</ta>
            <ta e="T239" id="Seg_10131" s="T238">pro.h:O</ta>
            <ta e="T240" id="Seg_10132" s="T239">ptcl.neg</ta>
            <ta e="T241" id="Seg_10133" s="T240">v:pred 0.3.h:S</ta>
            <ta e="T245" id="Seg_10134" s="T244">ptcl.neg</ta>
            <ta e="T248" id="Seg_10135" s="T247">v:pred 0.3.h:S</ta>
            <ta e="T251" id="Seg_10136" s="T250">n:pred</ta>
            <ta e="T252" id="Seg_10137" s="T251">pro.h:S</ta>
            <ta e="T259" id="Seg_10138" s="T258">pro.h:S</ta>
            <ta e="T261" id="Seg_10139" s="T260">n:pred</ta>
            <ta e="T263" id="Seg_10140" s="T262">pro:O</ta>
            <ta e="T267" id="Seg_10141" s="T266">v:pred 0.1.h:S</ta>
            <ta e="T268" id="Seg_10142" s="T267">v:pred 0.1.h:S</ta>
            <ta e="T273" id="Seg_10143" s="T272">adj:pred</ta>
            <ta e="T274" id="Seg_10144" s="T273">cop 0.3:S</ta>
            <ta e="T279" id="Seg_10145" s="T278">v:pred 0.3.h:S</ta>
            <ta e="T282" id="Seg_10146" s="T281">pro.h:S</ta>
            <ta e="T283" id="Seg_10147" s="T282">pro.h:O</ta>
            <ta e="T285" id="Seg_10148" s="T284">v:pred</ta>
            <ta e="T294" id="Seg_10149" s="T293">pro.h:S</ta>
            <ta e="T296" id="Seg_10150" s="T295">v:pred</ta>
            <ta e="T301" id="Seg_10151" s="T300">pro.h:S</ta>
            <ta e="T303" id="Seg_10152" s="T302">np:O</ta>
            <ta e="T304" id="Seg_10153" s="T303">v:pred</ta>
            <ta e="T312" id="Seg_10154" s="T311">pro.h:S</ta>
            <ta e="T314" id="Seg_10155" s="T313">n:pred</ta>
            <ta e="T319" id="Seg_10156" s="T318">v:pred 0.1.h:S</ta>
            <ta e="T320" id="Seg_10157" s="T319">pro:O</ta>
            <ta e="T325" id="Seg_10158" s="T324">v:pred 0.2.h:S</ta>
            <ta e="T330" id="Seg_10159" s="T329">pro.h:S</ta>
            <ta e="T332" id="Seg_10160" s="T331">v:pred</ta>
            <ta e="T338" id="Seg_10161" s="T337">v:pred 0.2.h:S</ta>
            <ta e="T342" id="Seg_10162" s="T341">v:pred</ta>
            <ta e="T343" id="Seg_10163" s="T342">pro.h:S</ta>
            <ta e="T347" id="Seg_10164" s="T346">ptcl.neg</ta>
            <ta e="T352" id="Seg_10165" s="T351">pro.h:S</ta>
            <ta e="T354" id="Seg_10166" s="T353">v:pred</ta>
            <ta e="T359" id="Seg_10167" s="T358">v:pred</ta>
            <ta e="T361" id="Seg_10168" s="T360">np.h:S</ta>
            <ta e="T364" id="Seg_10169" s="T363">ptcl.neg</ta>
            <ta e="T366" id="Seg_10170" s="T365">ptcl.neg</ta>
            <ta e="T373" id="Seg_10171" s="T372">ptcl.neg</ta>
            <ta e="T374" id="Seg_10172" s="T373">v:pred 0.3.h:S</ta>
            <ta e="T377" id="Seg_10173" s="T376">np.h:S</ta>
            <ta e="T380" id="Seg_10174" s="T379">ptcl.neg</ta>
            <ta e="T382" id="Seg_10175" s="T381">np.h:O</ta>
            <ta e="T384" id="Seg_10176" s="T383">ptcl:pred</ta>
            <ta e="T389" id="Seg_10177" s="T388">np.h:S</ta>
            <ta e="T392" id="Seg_10178" s="T391">v:pred</ta>
            <ta e="T393" id="Seg_10179" s="T392">np:O</ta>
            <ta e="T394" id="Seg_10180" s="T393">np:O</ta>
            <ta e="T397" id="Seg_10181" s="T396">pro:O</ta>
            <ta e="T400" id="Seg_10182" s="T399">np:S</ta>
            <ta e="T401" id="Seg_10183" s="T400">v:pred</ta>
            <ta e="T406" id="Seg_10184" s="T405">v:pred 0.3.h:S</ta>
            <ta e="T408" id="Seg_10185" s="T407">pro:O</ta>
            <ta e="T409" id="Seg_10186" s="T408">pro.h:S</ta>
            <ta e="T410" id="Seg_10187" s="T409">v:pred</ta>
            <ta e="T413" id="Seg_10188" s="T412">np:S</ta>
            <ta e="T414" id="Seg_10189" s="T413">v:pred</ta>
            <ta e="T416" id="Seg_10190" s="T415">np:S</ta>
            <ta e="T417" id="Seg_10191" s="T416">v:pred</ta>
            <ta e="T420" id="Seg_10192" s="T419">pro.h:S</ta>
            <ta e="T421" id="Seg_10193" s="T420">v:pred</ta>
            <ta e="T424" id="Seg_10194" s="T423">pro.h:S</ta>
            <ta e="T425" id="Seg_10195" s="T424">np:O</ta>
            <ta e="T426" id="Seg_10196" s="T425">ptcl.neg</ta>
            <ta e="T427" id="Seg_10197" s="T426">v:pred</ta>
            <ta e="T429" id="Seg_10198" s="T428">pro.h:S</ta>
            <ta e="T430" id="Seg_10199" s="T429">np:O</ta>
            <ta e="T431" id="Seg_10200" s="T430">v:pred</ta>
            <ta e="T436" id="Seg_10201" s="T435">np:O</ta>
            <ta e="T437" id="Seg_10202" s="T436">v:pred 0.2.h:S</ta>
            <ta e="T442" id="Seg_10203" s="T441">v:pred 0.2.h:S</ta>
            <ta e="T459" id="Seg_10204" s="T458">ptcl:pred</ta>
            <ta e="T463" id="Seg_10205" s="T462">v:pred 0.2.h:S</ta>
            <ta e="T465" id="Seg_10206" s="T464">s:purp</ta>
            <ta e="T479" id="Seg_10207" s="T478">v:pred 0.2.h:S</ta>
            <ta e="T481" id="Seg_10208" s="T480">s:purp</ta>
            <ta e="T484" id="Seg_10209" s="T483">pro:S</ta>
            <ta e="T485" id="Seg_10210" s="T484">n:pred</ta>
            <ta e="T486" id="Seg_10211" s="T485">cop</ta>
            <ta e="T489" id="Seg_10212" s="T488">n:pred</ta>
            <ta e="T490" id="Seg_10213" s="T489">cop</ta>
            <ta e="T491" id="Seg_10214" s="T490">np:S</ta>
            <ta e="T492" id="Seg_10215" s="T491">np:S</ta>
            <ta e="T494" id="Seg_10216" s="T493">np:S</ta>
            <ta e="T498" id="Seg_10217" s="T497">v:pred 0.2.h:S</ta>
            <ta e="T501" id="Seg_10218" s="T500">pro.h:S</ta>
            <ta e="T503" id="Seg_10219" s="T502">np:O</ta>
            <ta e="T504" id="Seg_10220" s="T503">v:pred</ta>
            <ta e="T509" id="Seg_10221" s="T508">np:O</ta>
            <ta e="T510" id="Seg_10222" s="T509">pro.h:S</ta>
            <ta e="T511" id="Seg_10223" s="T510">v:pred</ta>
            <ta e="T513" id="Seg_10224" s="T512">s:purp</ta>
            <ta e="T517" id="Seg_10225" s="T516">v:pred 0.2.h:S</ta>
            <ta e="T521" id="Seg_10226" s="T520">pro.h:S</ta>
            <ta e="T522" id="Seg_10227" s="T521">v:pred</ta>
            <ta e="T526" id="Seg_10228" s="T525">v:pred 0.1.h:S</ta>
            <ta e="T529" id="Seg_10229" s="T528">v:pred 0.1.h:S</ta>
            <ta e="T536" id="Seg_10230" s="T535">pro.h:S</ta>
            <ta e="T538" id="Seg_10231" s="T537">np:O</ta>
            <ta e="T542" id="Seg_10232" s="T541">v:pred</ta>
            <ta e="T544" id="Seg_10233" s="T543">ptcl:pred</ta>
            <ta e="T546" id="Seg_10234" s="T545">np:S</ta>
            <ta e="T547" id="Seg_10235" s="T546">adj:pred</ta>
            <ta e="T549" id="Seg_10236" s="T548">np:S</ta>
            <ta e="T550" id="Seg_10237" s="T549">adj:pred</ta>
            <ta e="T553" id="Seg_10238" s="T552">ptcl:pred</ta>
            <ta e="T554" id="Seg_10239" s="T553">cop 0.2.h:S</ta>
            <ta e="T556" id="Seg_10240" s="T555">adj:pred</ta>
            <ta e="T558" id="Seg_10241" s="T557">ptcl.neg</ta>
            <ta e="T559" id="Seg_10242" s="T558">adj:pred</ta>
            <ta e="T562" id="Seg_10243" s="T561">ptcl:pred</ta>
            <ta e="T566" id="Seg_10244" s="T565">pro.h:S</ta>
            <ta e="T568" id="Seg_10245" s="T567">v:pred</ta>
            <ta e="T570" id="Seg_10246" s="T569">np:S</ta>
            <ta e="T571" id="Seg_10247" s="T570">v:pred</ta>
            <ta e="T574" id="Seg_10248" s="T573">np:S</ta>
            <ta e="T576" id="Seg_10249" s="T575">v:pred</ta>
            <ta e="T579" id="Seg_10250" s="T578">pro.h:S</ta>
            <ta e="T580" id="Seg_10251" s="T579">v:pred</ta>
            <ta e="T582" id="Seg_10252" s="T581">ptcl.neg</ta>
            <ta e="T583" id="Seg_10253" s="T582">ptcl.neg</ta>
            <ta e="T584" id="Seg_10254" s="T583">ptcl:pred</ta>
            <ta e="T587" id="Seg_10255" s="T586">ptcl:pred</ta>
            <ta e="T591" id="Seg_10256" s="T590">adj:pred</ta>
            <ta e="T595" id="Seg_10257" s="T594">adj:pred</ta>
            <ta e="T598" id="Seg_10258" s="T597">adj:pred</ta>
            <ta e="T600" id="Seg_10259" s="T599">ptcl:pred</ta>
            <ta e="T605" id="Seg_10260" s="T604">np.h:S</ta>
            <ta e="T606" id="Seg_10261" s="T605">v:pred</ta>
            <ta e="T610" id="Seg_10262" s="T609">np.h:S</ta>
            <ta e="T614" id="Seg_10263" s="T613">v:pred</ta>
            <ta e="T617" id="Seg_10264" s="T616">np.h:S</ta>
            <ta e="T619" id="Seg_10265" s="T618">v:pred</ta>
            <ta e="T622" id="Seg_10266" s="T621">v:pred 0.3.h:S</ta>
            <ta e="T623" id="Seg_10267" s="T622">n:pred</ta>
            <ta e="T624" id="Seg_10268" s="T623">np:S</ta>
            <ta e="T625" id="Seg_10269" s="T624">n:pred</ta>
            <ta e="T626" id="Seg_10270" s="T625">np:S</ta>
            <ta e="T629" id="Seg_10271" s="T628">pro.h:S</ta>
            <ta e="T630" id="Seg_10272" s="T629">v:pred</ta>
            <ta e="T631" id="Seg_10273" s="T630">np:O</ta>
            <ta e="T633" id="Seg_10274" s="T632">pro.h:S</ta>
            <ta e="T634" id="Seg_10275" s="T633">np:O</ta>
            <ta e="T650" id="Seg_10276" s="T649">v:pred 0.2.h:S</ta>
            <ta e="T653" id="Seg_10277" s="T652">ptcl:pred</ta>
            <ta e="T656" id="Seg_10278" s="T655">adj:pred</ta>
            <ta e="T660" id="Seg_10279" s="T659">ptcl:pred</ta>
            <ta e="T661" id="Seg_10280" s="T660">np.h:S</ta>
            <ta e="T662" id="Seg_10281" s="T661">v:pred</ta>
            <ta e="T663" id="Seg_10282" s="T662">np:O</ta>
            <ta e="T666" id="Seg_10283" s="T665">np:O</ta>
            <ta e="T668" id="Seg_10284" s="T667">v:pred 0.3.h:S</ta>
            <ta e="T669" id="Seg_10285" s="T668">pro.h:S</ta>
            <ta e="T670" id="Seg_10286" s="T669">v:pred</ta>
            <ta e="T671" id="Seg_10287" s="T670">np:O</ta>
            <ta e="T673" id="Seg_10288" s="T672">pro.h:S</ta>
            <ta e="T674" id="Seg_10289" s="T673">v:pred</ta>
            <ta e="T675" id="Seg_10290" s="T674">np:O</ta>
            <ta e="T678" id="Seg_10291" s="T677">np:S</ta>
            <ta e="T679" id="Seg_10292" s="T678">n:pred</ta>
            <ta e="T682" id="Seg_10293" s="T681">np:S</ta>
            <ta e="T684" id="Seg_10294" s="T683">np:S</ta>
            <ta e="T685" id="Seg_10295" s="T684">n:pred</ta>
            <ta e="T689" id="Seg_10296" s="T688">np:O</ta>
            <ta e="T690" id="Seg_10297" s="T689">ptcl.neg</ta>
            <ta e="T691" id="Seg_10298" s="T690">v:pred 0.3.h:S</ta>
            <ta e="T707" id="Seg_10299" s="T706">adj:pred</ta>
            <ta e="T708" id="Seg_10300" s="T707">np.h:S</ta>
            <ta e="T709" id="Seg_10301" s="T708">cop</ta>
            <ta e="T712" id="Seg_10302" s="T711">np.h:S</ta>
            <ta e="T713" id="Seg_10303" s="T712">v:pred</ta>
            <ta e="T722" id="Seg_10304" s="T721">pro.h:S</ta>
            <ta e="T723" id="Seg_10305" s="T722">v:pred</ta>
            <ta e="T724" id="Seg_10306" s="T723">v:pred 0.3.h:S</ta>
            <ta e="T728" id="Seg_10307" s="T727">pro.h:O</ta>
            <ta e="T730" id="Seg_10308" s="T729">v:pred 0.3.h:S</ta>
            <ta e="T734" id="Seg_10309" s="T733">v:pred 0.3.h:S</ta>
            <ta e="T739" id="Seg_10310" s="T738">v:pred 0.3.h:S</ta>
            <ta e="T745" id="Seg_10311" s="T744">v:pred 0.3.h:S</ta>
            <ta e="T748" id="Seg_10312" s="T747">pro.h:O</ta>
            <ta e="T750" id="Seg_10313" s="T749">v:pred 0.3.h:S</ta>
            <ta e="T756" id="Seg_10314" s="T755">pro.h:S</ta>
            <ta e="T757" id="Seg_10315" s="T756">v:pred</ta>
            <ta e="T760" id="Seg_10316" s="T759">pro.h:S</ta>
            <ta e="T762" id="Seg_10317" s="T761">ptcl.neg</ta>
            <ta e="T763" id="Seg_10318" s="T762">v:pred</ta>
            <ta e="T767" id="Seg_10319" s="T766">v:pred 0.1.h:S</ta>
            <ta e="T768" id="Seg_10320" s="T767">np:O</ta>
            <ta e="T769" id="Seg_10321" s="T768">s:purp</ta>
            <ta e="T771" id="Seg_10322" s="T770">np:S</ta>
            <ta e="T772" id="Seg_10323" s="T771">adj:pred</ta>
            <ta e="T784" id="Seg_10324" s="T783">pro.h:S</ta>
            <ta e="T786" id="Seg_10325" s="T785">v:pred</ta>
            <ta e="T788" id="Seg_10326" s="T787">v:pred 0.1.h:S</ta>
            <ta e="T791" id="Seg_10327" s="T790">pro.h:S</ta>
            <ta e="T792" id="Seg_10328" s="T791">v:pred</ta>
            <ta e="T794" id="Seg_10329" s="T793">adj:pred</ta>
            <ta e="T795" id="Seg_10330" s="T794">pro:S</ta>
            <ta e="T801" id="Seg_10331" s="T800">pro.h:S</ta>
            <ta e="T803" id="Seg_10332" s="T802">v:pred</ta>
            <ta e="T808" id="Seg_10333" s="T807">pro.h:S</ta>
            <ta e="T810" id="Seg_10334" s="T809">np:O</ta>
            <ta e="T811" id="Seg_10335" s="T810">np:O</ta>
            <ta e="T812" id="Seg_10336" s="T811">v:pred</ta>
            <ta e="T817" id="Seg_10337" s="T816">v:pred 0.1.h:S</ta>
            <ta e="T829" id="Seg_10338" s="T828">ptcl:pred</ta>
            <ta e="T831" id="Seg_10339" s="T830">ptcl.neg</ta>
            <ta e="T833" id="Seg_10340" s="T832">ptcl.neg</ta>
            <ta e="T834" id="Seg_10341" s="T833">v:pred 0.1.h:S</ta>
            <ta e="T835" id="Seg_10342" s="T834">np.h:O</ta>
            <ta e="T840" id="Seg_10343" s="T839">v:pred 0.1.h:S</ta>
            <ta e="T850" id="Seg_10344" s="T849">np.h:S</ta>
            <ta e="T853" id="Seg_10345" s="T852">pro:S</ta>
            <ta e="T854" id="Seg_10346" s="T853">n:pred</ta>
            <ta e="T858" id="Seg_10347" s="T857">pro:O</ta>
            <ta e="T859" id="Seg_10348" s="T858">v:pred 0.3.h:S</ta>
            <ta e="T865" id="Seg_10349" s="T864">v:pred 0.3.h:S</ta>
            <ta e="T866" id="Seg_10350" s="T865">pro:O</ta>
            <ta e="T870" id="Seg_10351" s="T869">pro.h:S</ta>
            <ta e="T871" id="Seg_10352" s="T870">pro:O</ta>
            <ta e="T872" id="Seg_10353" s="T871">ptcl.neg</ta>
            <ta e="T873" id="Seg_10354" s="T872">v:pred</ta>
            <ta e="T877" id="Seg_10355" s="T876">ptcl.neg</ta>
            <ta e="T878" id="Seg_10356" s="T877">v:pred 0.2.h:S</ta>
            <ta e="T886" id="Seg_10357" s="T885">n:pred</ta>
            <ta e="T887" id="Seg_10358" s="T886">cop 0.1.h:S</ta>
            <ta e="T894" id="Seg_10359" s="T893">n:pred</ta>
            <ta e="T898" id="Seg_10360" s="T897">n:pred</ta>
            <ta e="T901" id="Seg_10361" s="T900">pro.h:S</ta>
            <ta e="T903" id="Seg_10362" s="T902">v:pred</ta>
            <ta e="T909" id="Seg_10363" s="T908">np.h:S</ta>
            <ta e="T911" id="Seg_10364" s="T910">np.h:O</ta>
            <ta e="T912" id="Seg_10365" s="T911">v:pred</ta>
            <ta e="T915" id="Seg_10366" s="T914">pro.h:S</ta>
            <ta e="T916" id="Seg_10367" s="T915">v:pred</ta>
            <ta e="T919" id="Seg_10368" s="T918">pro.h:S</ta>
            <ta e="T920" id="Seg_10369" s="T919">v:pred</ta>
            <ta e="T922" id="Seg_10370" s="T921">np.h:S</ta>
            <ta e="T924" id="Seg_10371" s="T922">v:pred</ta>
            <ta e="T926" id="Seg_10372" s="T925">np.h:S</ta>
            <ta e="T927" id="Seg_10373" s="T926">adj:pred</ta>
            <ta e="T928" id="Seg_10374" s="T927">adj:pred</ta>
            <ta e="T932" id="Seg_10375" s="T931">np.h:S</ta>
            <ta e="T933" id="Seg_10376" s="T932">ptcl.neg</ta>
            <ta e="T934" id="Seg_10377" s="T933">adj:pred</ta>
            <ta e="T938" id="Seg_10378" s="T937">np.h:S</ta>
            <ta e="T940" id="Seg_10379" s="T938">v:pred</ta>
            <ta e="T943" id="Seg_10380" s="T942">np.h:S</ta>
            <ta e="T944" id="Seg_10381" s="T943">adj:pred</ta>
            <ta e="T947" id="Seg_10382" s="T946">adj:pred</ta>
            <ta e="T953" id="Seg_10383" s="T952">pro:O</ta>
            <ta e="T954" id="Seg_10384" s="T953">v:pred 0.3.h:S</ta>
            <ta e="T958" id="Seg_10385" s="T957">np.h:S</ta>
            <ta e="T959" id="Seg_10386" s="T958">ptcl.neg</ta>
            <ta e="T960" id="Seg_10387" s="T959">adj:pred</ta>
            <ta e="T962" id="Seg_10388" s="T961">pro:O</ta>
            <ta e="T963" id="Seg_10389" s="T962">ptcl.neg</ta>
            <ta e="T964" id="Seg_10390" s="T963">v:pred 0.3.h:S</ta>
            <ta e="T969" id="Seg_10391" s="T968">np.h:S</ta>
            <ta e="T971" id="Seg_10392" s="T970">pro:O</ta>
            <ta e="T976" id="Seg_10393" s="T975">v:pred 0.3.h:S</ta>
            <ta e="T978" id="Seg_10394" s="T977">ptcl:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_10395" s="T1">TURK:core</ta>
            <ta e="T7" id="Seg_10396" s="T6">TURK:core</ta>
            <ta e="T10" id="Seg_10397" s="T9">RUS:gram</ta>
            <ta e="T19" id="Seg_10398" s="T18">RUS:gram</ta>
            <ta e="T20" id="Seg_10399" s="T19">TURK:core</ta>
            <ta e="T22" id="Seg_10400" s="T21">TURK:core</ta>
            <ta e="T25" id="Seg_10401" s="T24">RUS:mod</ta>
            <ta e="T28" id="Seg_10402" s="T27">TURK:core</ta>
            <ta e="T29" id="Seg_10403" s="T28">TURK:core</ta>
            <ta e="T42" id="Seg_10404" s="T41">TURK:disc</ta>
            <ta e="T44" id="Seg_10405" s="T43">TURK:core</ta>
            <ta e="T49" id="Seg_10406" s="T48">RUS:disc</ta>
            <ta e="T62" id="Seg_10407" s="T61">TURK:cult</ta>
            <ta e="T66" id="Seg_10408" s="T65">TURK:cult</ta>
            <ta e="T68" id="Seg_10409" s="T67">RUS:gram</ta>
            <ta e="T75" id="Seg_10410" s="T74">TURK:core</ta>
            <ta e="T78" id="Seg_10411" s="T77">TURK:disc</ta>
            <ta e="T82" id="Seg_10412" s="T81">%TURK:core</ta>
            <ta e="T92" id="Seg_10413" s="T91">RUS:gram</ta>
            <ta e="T100" id="Seg_10414" s="T99">RUS:gram</ta>
            <ta e="T103" id="Seg_10415" s="T102">RUS:gram</ta>
            <ta e="T115" id="Seg_10416" s="T114">TURK:gram(INDEF)</ta>
            <ta e="T118" id="Seg_10417" s="T117">RUS:core</ta>
            <ta e="T123" id="Seg_10418" s="T122">TURK:disc</ta>
            <ta e="T125" id="Seg_10419" s="T124">TURK:core</ta>
            <ta e="T128" id="Seg_10420" s="T127">RUS:gram</ta>
            <ta e="T130" id="Seg_10421" s="T129">%TURK:core</ta>
            <ta e="T133" id="Seg_10422" s="T132">TURK:gram(INDEF)</ta>
            <ta e="T135" id="Seg_10423" s="T134">RUS:gram</ta>
            <ta e="T139" id="Seg_10424" s="T138">RUS:disc</ta>
            <ta e="T142" id="Seg_10425" s="T141">RUS:mod</ta>
            <ta e="T147" id="Seg_10426" s="T146">RUS:gram</ta>
            <ta e="T155" id="Seg_10427" s="T154">RUS:gram</ta>
            <ta e="T159" id="Seg_10428" s="T158">RUS:gram</ta>
            <ta e="T165" id="Seg_10429" s="T164">TURK:disc</ta>
            <ta e="T175" id="Seg_10430" s="T174">RUS:mod</ta>
            <ta e="T179" id="Seg_10431" s="T178">RUS:gram(INDEF)</ta>
            <ta e="T181" id="Seg_10432" s="T180">RUS:mod</ta>
            <ta e="T200" id="Seg_10433" s="T199">RUS:gram</ta>
            <ta e="T204" id="Seg_10434" s="T203">RUS:gram</ta>
            <ta e="T206" id="Seg_10435" s="T205">RUS:gram</ta>
            <ta e="T207" id="Seg_10436" s="T206">TURK:cult</ta>
            <ta e="T208" id="Seg_10437" s="T207">RUS:gram</ta>
            <ta e="T210" id="Seg_10438" s="T209">RUS:mod</ta>
            <ta e="T211" id="Seg_10439" s="T210">TURK:core</ta>
            <ta e="T212" id="Seg_10440" s="T211">RUS:gram</ta>
            <ta e="T216" id="Seg_10441" s="T215">TURK:cult</ta>
            <ta e="T218" id="Seg_10442" s="T217">TURK:cult</ta>
            <ta e="T219" id="Seg_10443" s="T218">RUS:gram</ta>
            <ta e="T220" id="Seg_10444" s="T219">TURK:cult</ta>
            <ta e="T222" id="Seg_10445" s="T221">RUS:gram</ta>
            <ta e="T224" id="Seg_10446" s="T223">TURK:core</ta>
            <ta e="T233" id="Seg_10447" s="T232">RUS:gram</ta>
            <ta e="T247" id="Seg_10448" s="T246">RUS:disc</ta>
            <ta e="T249" id="Seg_10449" s="T248">RUS:gram</ta>
            <ta e="T260" id="Seg_10450" s="T259">TURK:core</ta>
            <ta e="T263" id="Seg_10451" s="T262">RUS:gram(INDEF)</ta>
            <ta e="T265" id="Seg_10452" s="T264">TURK:disc</ta>
            <ta e="T266" id="Seg_10453" s="T265">RUS:gram</ta>
            <ta e="T269" id="Seg_10454" s="T268">RUS:gram</ta>
            <ta e="T273" id="Seg_10455" s="T272">TURK:core</ta>
            <ta e="T277" id="Seg_10456" s="T276">RUS:disc</ta>
            <ta e="T284" id="Seg_10457" s="T283">TURK:disc</ta>
            <ta e="T288" id="Seg_10458" s="T287">TURK:disc</ta>
            <ta e="T293" id="Seg_10459" s="T292">RUS:gram</ta>
            <ta e="T303" id="Seg_10460" s="T302">TURK:cult</ta>
            <ta e="T306" id="Seg_10461" s="T305">RUS:mod</ta>
            <ta e="T311" id="Seg_10462" s="T310">RUS:disc</ta>
            <ta e="T316" id="Seg_10463" s="T315">TURK:disc</ta>
            <ta e="T318" id="Seg_10464" s="T317">TURK:disc</ta>
            <ta e="T322" id="Seg_10465" s="T321">RUS:gram</ta>
            <ta e="T326" id="Seg_10466" s="T325">RUS:gram</ta>
            <ta e="T334" id="Seg_10467" s="T333">RUS:gram</ta>
            <ta e="T340" id="Seg_10468" s="T339">RUS:gram</ta>
            <ta e="T341" id="Seg_10469" s="T340">RUS:mod</ta>
            <ta e="T349" id="Seg_10470" s="T348">TAT:cult</ta>
            <ta e="T356" id="Seg_10471" s="T355">RUS:gram</ta>
            <ta e="T358" id="Seg_10472" s="T357">RUS:gram</ta>
            <ta e="T376" id="Seg_10473" s="T375">TURK:disc</ta>
            <ta e="T379" id="Seg_10474" s="T378">RUS:gram</ta>
            <ta e="T381" id="Seg_10475" s="T380">TURK:core</ta>
            <ta e="T384" id="Seg_10476" s="T383">RUS:mod</ta>
            <ta e="T390" id="Seg_10477" s="T389">TURK:core</ta>
            <ta e="T395" id="Seg_10478" s="T394">RUS:gram</ta>
            <ta e="T396" id="Seg_10479" s="T395">TURK:disc</ta>
            <ta e="T398" id="Seg_10480" s="T397">TURK:disc</ta>
            <ta e="T400" id="Seg_10481" s="T399">TAT:cult</ta>
            <ta e="T405" id="Seg_10482" s="T404">TURK:disc</ta>
            <ta e="T412" id="Seg_10483" s="T411">RUS:gram</ta>
            <ta e="T415" id="Seg_10484" s="T414">RUS:gram</ta>
            <ta e="T423" id="Seg_10485" s="T422">RUS:gram</ta>
            <ta e="T433" id="Seg_10486" s="T432">RUS:gram</ta>
            <ta e="T439" id="Seg_10487" s="T438">RUS:gram</ta>
            <ta e="T441" id="Seg_10488" s="T440">RUS:mod</ta>
            <ta e="T446" id="Seg_10489" s="T445">RUS:gram</ta>
            <ta e="T447" id="Seg_10490" s="T446">TURK:disc</ta>
            <ta e="T449" id="Seg_10491" s="T448">TURK:disc</ta>
            <ta e="T451" id="Seg_10492" s="T450">RUS:gram</ta>
            <ta e="T454" id="Seg_10493" s="T453">TAT:cult</ta>
            <ta e="T459" id="Seg_10494" s="T458">RUS:mod</ta>
            <ta e="T467" id="Seg_10495" s="T466">RUS:disc</ta>
            <ta e="T483" id="Seg_10496" s="T482">RUS:gram</ta>
            <ta e="T488" id="Seg_10497" s="T487">RUS:gram</ta>
            <ta e="T491" id="Seg_10498" s="T490">TURK:cult</ta>
            <ta e="T493" id="Seg_10499" s="T492">RUS:gram</ta>
            <ta e="T494" id="Seg_10500" s="T493">TURK:cult</ta>
            <ta e="T500" id="Seg_10501" s="T499">RUS:gram</ta>
            <ta e="T502" id="Seg_10502" s="T501">RUS:gram</ta>
            <ta e="T505" id="Seg_10503" s="T504">RUS:gram</ta>
            <ta e="T507" id="Seg_10504" s="T506">RUS:disc</ta>
            <ta e="T508" id="Seg_10505" s="T507">RUS:gram</ta>
            <ta e="T515" id="Seg_10506" s="T514">TURK:core</ta>
            <ta e="T516" id="Seg_10507" s="T515">RUS:mod</ta>
            <ta e="T520" id="Seg_10508" s="T519">RUS:gram</ta>
            <ta e="T528" id="Seg_10509" s="T527">TURK:core</ta>
            <ta e="T539" id="Seg_10510" s="T538">TURK:disc</ta>
            <ta e="T544" id="Seg_10511" s="T543">RUS:mod</ta>
            <ta e="T548" id="Seg_10512" s="T547">RUS:gram</ta>
            <ta e="T559" id="Seg_10513" s="T558">TURK:core</ta>
            <ta e="T562" id="Seg_10514" s="T561">RUS:mod</ta>
            <ta e="T573" id="Seg_10515" s="T572">RUS:gram</ta>
            <ta e="T575" id="Seg_10516" s="T574">TURK:disc</ta>
            <ta e="T578" id="Seg_10517" s="T577">RUS:mod</ta>
            <ta e="T582" id="Seg_10518" s="T581">TURK:disc</ta>
            <ta e="T583" id="Seg_10519" s="T582">RUS:gram</ta>
            <ta e="T584" id="Seg_10520" s="T583">RUS:mod</ta>
            <ta e="T587" id="Seg_10521" s="T586">RUS:mod</ta>
            <ta e="T591" id="Seg_10522" s="T590">TURK:core</ta>
            <ta e="T593" id="Seg_10523" s="T592">RUS:gram</ta>
            <ta e="T600" id="Seg_10524" s="T599">RUS:mod</ta>
            <ta e="T611" id="Seg_10525" s="T610">TURK:gram(INDEF)</ta>
            <ta e="T612" id="Seg_10526" s="T611">%TURK:core</ta>
            <ta e="T616" id="Seg_10527" s="T615">RUS:gram</ta>
            <ta e="T618" id="Seg_10528" s="T617">TAT:cult</ta>
            <ta e="T621" id="Seg_10529" s="T620">RUS:disc</ta>
            <ta e="T626" id="Seg_10530" s="T625">TURK:cult</ta>
            <ta e="T628" id="Seg_10531" s="T627">TURK:disc</ta>
            <ta e="T632" id="Seg_10532" s="T631">RUS:gram</ta>
            <ta e="T634" id="Seg_10533" s="T633">TURK:cult</ta>
            <ta e="T636" id="Seg_10534" s="T635">RUS:mod</ta>
            <ta e="T642" id="Seg_10535" s="T641">RUS:disc</ta>
            <ta e="T644" id="Seg_10536" s="T643">%TAT:cult</ta>
            <ta e="T652" id="Seg_10537" s="T651">RUS:gram</ta>
            <ta e="T653" id="Seg_10538" s="T652">RUS:mod</ta>
            <ta e="T658" id="Seg_10539" s="T657">RUS:gram</ta>
            <ta e="T660" id="Seg_10540" s="T659">RUS:mod</ta>
            <ta e="T661" id="Seg_10541" s="T660">TURK:core</ta>
            <ta e="T664" id="Seg_10542" s="T663">RUS:gram</ta>
            <ta e="T666" id="Seg_10543" s="T665">TURK:cult</ta>
            <ta e="T672" id="Seg_10544" s="T671">RUS:gram</ta>
            <ta e="T675" id="Seg_10545" s="T674">TURK:cult</ta>
            <ta e="T677" id="Seg_10546" s="T676">RUS:gram</ta>
            <ta e="T678" id="Seg_10547" s="T677">TAT:cult</ta>
            <ta e="T681" id="Seg_10548" s="T680">RUS:gram</ta>
            <ta e="T682" id="Seg_10549" s="T681">TAT:cult</ta>
            <ta e="T683" id="Seg_10550" s="T682">RUS:gram</ta>
            <ta e="T684" id="Seg_10551" s="T683">TURK:cult</ta>
            <ta e="T687" id="Seg_10552" s="T686">RUS:gram</ta>
            <ta e="T689" id="Seg_10553" s="T688">TURK:cult</ta>
            <ta e="T700" id="Seg_10554" s="T699">TURK:disc</ta>
            <ta e="T702" id="Seg_10555" s="T701">TURK:disc</ta>
            <ta e="T704" id="Seg_10556" s="T703">TURK:disc</ta>
            <ta e="T715" id="Seg_10557" s="T714">RUS:gram</ta>
            <ta e="T727" id="Seg_10558" s="T726">TAT:cult</ta>
            <ta e="T729" id="Seg_10559" s="T728">TURK:disc</ta>
            <ta e="T732" id="Seg_10560" s="T731">RUS:gram</ta>
            <ta e="T736" id="Seg_10561" s="T735">RUS:gram</ta>
            <ta e="T741" id="Seg_10562" s="T740">RUS:gram</ta>
            <ta e="T742" id="Seg_10563" s="T741">TURK:disc</ta>
            <ta e="T744" id="Seg_10564" s="T743">TURK:disc</ta>
            <ta e="T747" id="Seg_10565" s="T746">RUS:mod</ta>
            <ta e="T749" id="Seg_10566" s="T748">RUS:gram</ta>
            <ta e="T752" id="Seg_10567" s="T751">RUS:gram</ta>
            <ta e="T755" id="Seg_10568" s="T754">RUS:disc</ta>
            <ta e="T761" id="Seg_10569" s="T760">RUS:disc</ta>
            <ta e="T774" id="Seg_10570" s="T773">RUS:gram</ta>
            <ta e="T779" id="Seg_10571" s="T778">RUS:mod</ta>
            <ta e="T780" id="Seg_10572" s="T779">RUS:gram</ta>
            <ta e="T783" id="Seg_10573" s="T782">RUS:gram</ta>
            <ta e="T790" id="Seg_10574" s="T789">RUS:gram</ta>
            <ta e="T794" id="Seg_10575" s="T793">TURK:core</ta>
            <ta e="T804" id="Seg_10576" s="T803">RUS:core</ta>
            <ta e="T807" id="Seg_10577" s="T806">RUS:gram</ta>
            <ta e="T816" id="Seg_10578" s="T815">RUS:disc</ta>
            <ta e="T822" id="Seg_10579" s="T821">RUS:gram</ta>
            <ta e="T825" id="Seg_10580" s="T824">RUS:gram</ta>
            <ta e="T829" id="Seg_10581" s="T828">RUS:gram</ta>
            <ta e="T837" id="Seg_10582" s="T836">RUS:gram</ta>
            <ta e="T842" id="Seg_10583" s="T841">RUS:gram</ta>
            <ta e="T845" id="Seg_10584" s="T844">RUS:gram</ta>
            <ta e="T846" id="Seg_10585" s="T845">RUS:core</ta>
            <ta e="T852" id="Seg_10586" s="T851">RUS:gram</ta>
            <ta e="T857" id="Seg_10587" s="T856">TURK:gram(INDEF)</ta>
            <ta e="T858" id="Seg_10588" s="T857">RUS:gram(INDEF)</ta>
            <ta e="T861" id="Seg_10589" s="T860">RUS:gram</ta>
            <ta e="T866" id="Seg_10590" s="T865">RUS:gram(INDEF)</ta>
            <ta e="T868" id="Seg_10591" s="T867">RUS:gram</ta>
            <ta e="T871" id="Seg_10592" s="T870">TURK:gram(INDEF)</ta>
            <ta e="T875" id="Seg_10593" s="T874">RUS:gram</ta>
            <ta e="T880" id="Seg_10594" s="T879">TURK:disc</ta>
            <ta e="T882" id="Seg_10595" s="T881">RUS:gram</ta>
            <ta e="T892" id="Seg_10596" s="T891">TURK:disc</ta>
            <ta e="T893" id="Seg_10597" s="T892">TURK:core</ta>
            <ta e="T896" id="Seg_10598" s="T895">TURK:disc</ta>
            <ta e="T900" id="Seg_10599" s="T899">RUS:disc</ta>
            <ta e="T902" id="Seg_10600" s="T901">%TURK:core</ta>
            <ta e="T909" id="Seg_10601" s="T908">TURK:core</ta>
            <ta e="T911" id="Seg_10602" s="T910">TURK:core</ta>
            <ta e="T918" id="Seg_10603" s="T917">RUS:gram</ta>
            <ta e="T922" id="Seg_10604" s="T921">TURK:core</ta>
            <ta e="T927" id="Seg_10605" s="T926">TURK:core</ta>
            <ta e="T930" id="Seg_10606" s="T929">RUS:gram</ta>
            <ta e="T934" id="Seg_10607" s="T933">TURK:core</ta>
            <ta e="T936" id="Seg_10608" s="T935">RUS:gram</ta>
            <ta e="T938" id="Seg_10609" s="T937">TURK:core</ta>
            <ta e="T941" id="Seg_10610" s="T940">RUS:mod</ta>
            <ta e="T944" id="Seg_10611" s="T943">TURK:core</ta>
            <ta e="T946" id="Seg_10612" s="T945">RUS:gram</ta>
            <ta e="T949" id="Seg_10613" s="T948">RUS:gram</ta>
            <ta e="T950" id="Seg_10614" s="T949">TURK:disc</ta>
            <ta e="T952" id="Seg_10615" s="T951">TURK:disc</ta>
            <ta e="T956" id="Seg_10616" s="T955">RUS:gram</ta>
            <ta e="T960" id="Seg_10617" s="T959">TURK:core</ta>
            <ta e="T962" id="Seg_10618" s="T961">TURK:gram(INDEF)</ta>
            <ta e="T967" id="Seg_10619" s="T966">RUS:gram</ta>
            <ta e="T970" id="Seg_10620" s="T969">TURK:disc</ta>
            <ta e="T972" id="Seg_10621" s="T971">TURK:disc</ta>
            <ta e="T975" id="Seg_10622" s="T974">TURK:core</ta>
            <ta e="T978" id="Seg_10623" s="T977">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T41" id="Seg_10624" s="T39">RUS:int</ta>
            <ta e="T121" id="Seg_10625" s="T120">RUS:int</ta>
            <ta e="T371" id="Seg_10626" s="T367">RUS:ext</ta>
            <ta e="T471" id="Seg_10627" s="T468">RUS:ext</ta>
            <ta e="T475" id="Seg_10628" s="T472">RUS:ext</ta>
            <ta e="T694" id="Seg_10629" s="T692">RUS:ext</ta>
            <ta e="T698" id="Seg_10630" s="T697">RUS:ext</ta>
            <ta e="T718" id="Seg_10631" s="T717">RUS:int</ta>
            <ta e="T797" id="Seg_10632" s="T796">RUS:ext</ta>
            <ta e="T906" id="Seg_10633" s="T905">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_10634" s="T1">Хороший человек был, умер.</ta>
            <ta e="T8" id="Seg_10635" s="T6">Хорошо умер.</ta>
            <ta e="T17" id="Seg_10636" s="T9">А тот плохой человек (один?) умер.</ta>
            <ta e="T23" id="Seg_10637" s="T18">А хороший человек хорошо умер.</ta>
            <ta e="T25" id="Seg_10638" s="T24">Вот.</ta>
            <ta e="T30" id="Seg_10639" s="T26">Мой отец хорошо жил.</ta>
            <ta e="T33" id="Seg_10640" s="T31">Потом плохо.</ta>
            <ta e="T37" id="Seg_10641" s="T34">Очень плохо жил.</ta>
            <ta e="T44" id="Seg_10642" s="T38">Потом жил хорошо. [?]</ta>
            <ta e="T47" id="Seg_10643" s="T45">Потом умер.</ta>
            <ta e="T55" id="Seg_10644" s="T51">Я жила очень плохо.</ta>
            <ta e="T59" id="Seg_10645" s="T56">Я очень плохо жила.</ta>
            <ta e="T63" id="Seg_10646" s="T60">У меня не (было?) хлеба.</ta>
            <ta e="T72" id="Seg_10647" s="T64">Не было хлеба у меня, и я сама ела (что-попало?).</ta>
            <ta e="T76" id="Seg_10648" s="T73">Потом я жила хорошо.</ta>
            <ta e="T79" id="Seg_10649" s="T77">(По-всякому?)</ta>
            <ta e="T83" id="Seg_10650" s="T80">Потом я ушла.</ta>
            <ta e="T86" id="Seg_10651" s="T84">Жила плохо-плохо.</ta>
            <ta e="T90" id="Seg_10652" s="T86">Дочь (родила?)</ta>
            <ta e="T96" id="Seg_10653" s="T91">И с дочерью очень плохо жила.</ta>
            <ta e="T98" id="Seg_10654" s="T97">Я плакала.</ta>
            <ta e="T109" id="Seg_10655" s="T99">И ушла, и пришла жить, очень плохо.</ta>
            <ta e="T113" id="Seg_10656" s="T110">Куда мне идти?</ta>
            <ta e="T119" id="Seg_10657" s="T114">Никуда не пойду, лучше умру.</ta>
            <ta e="T121" id="Seg_10658" s="T120">Это.</ta>
            <ta e="T126" id="Seg_10659" s="T122">Все люди хорошо живут.</ta>
            <ta e="T133" id="Seg_10660" s="T127">А потом уходят отсюда, куда-то.</ta>
            <ta e="T140" id="Seg_10661" s="T134">А потом почему сюда же приходят?</ta>
            <ta e="T145" id="Seg_10662" s="T141">Эх, почему ты так будешь [делать]?</ta>
            <ta e="T148" id="Seg_10663" s="T146">А что?</ta>
            <ta e="T150" id="Seg_10664" s="T149">Да так.</ta>
            <ta e="T153" id="Seg_10665" s="T151">Придет сюда.</ta>
            <ta e="T157" id="Seg_10666" s="T154">А… сюда придет.</ta>
            <ta e="T163" id="Seg_10667" s="T158">А ты что, не пойдешь?</ta>
            <ta e="T165" id="Seg_10668" s="T164">Нет.</ta>
            <ta e="T169" id="Seg_10669" s="T166">Я не пойду.</ta>
            <ta e="T173" id="Seg_10670" s="T170">Я здесь буду.</ta>
            <ta e="T181" id="Seg_10671" s="T178">Что-нибудь приготовить надо.</ta>
            <ta e="T185" id="Seg_10672" s="T182">Я есть буду.</ta>
            <ta e="T192" id="Seg_10673" s="T188">Что ты готовишь?</ta>
            <ta e="T194" id="Seg_10674" s="T193">Мясо.</ta>
            <ta e="T197" id="Seg_10675" s="T195">Почему мясо?</ta>
            <ta e="T202" id="Seg_10676" s="T198">Я бы (?).</ta>
            <ta e="T208" id="Seg_10677" s="T203">А мне бы молоко.</ta>
            <ta e="T213" id="Seg_10678" s="T209">Вот я бы хорошо поела.</ta>
            <ta e="T216" id="Seg_10679" s="T214">Почему молоко?</ta>
            <ta e="T220" id="Seg_10680" s="T217">Молоко так молоко.</ta>
            <ta e="T224" id="Seg_10681" s="T221">А мясо хорошо.</ta>
            <ta e="T231" id="Seg_10682" s="T227">Почему он(а) ко мне не приходит?</ta>
            <ta e="T236" id="Seg_10683" s="T232">А у меня мать плохая.</ta>
            <ta e="T241" id="Seg_10684" s="T237">(…) тебя не пускает.</ta>
            <ta e="T245" id="Seg_10685" s="T242">Почему?</ta>
            <ta e="T252" id="Seg_10686" s="T246">Ну, скажет, что ты плохой человек.</ta>
            <ta e="T257" id="Seg_10687" s="T253">Почему плохой человек?</ta>
            <ta e="T261" id="Seg_10688" s="T258">Я хороший человек.</ta>
            <ta e="T269" id="Seg_10689" s="T262">Что-нибудь тебе рассказывал, рассказывал бы.</ta>
            <ta e="T274" id="Seg_10690" s="T270">Сразу станет очень хорошо. [?]</ta>
            <ta e="T280" id="Seg_10691" s="T275">Ох, ну что он мне скажет. [?]</ta>
            <ta e="T286" id="Seg_10692" s="T281">Я его побью сильно.</ta>
            <ta e="T291" id="Seg_10693" s="T287">Нет, бить не надо.</ta>
            <ta e="T297" id="Seg_10694" s="T292">А то он меня побьет потом.</ta>
            <ta e="T304" id="Seg_10695" s="T300">Я пью много водки.</ta>
            <ta e="T309" id="Seg_10696" s="T305">О, почему ты?</ta>
            <ta e="T314" id="Seg_10697" s="T310">Ну, я плохой человек.</ta>
            <ta e="T320" id="Seg_10698" s="T315">Я все беру. [?]</ta>
            <ta e="T328" id="Seg_10699" s="T321">А что (ты себе?) берешь, я потом…</ta>
            <ta e="T332" id="Seg_10700" s="T329">Я потом беру.</ta>
            <ta e="T338" id="Seg_10701" s="T333">А почему ты так берешь. [?]</ta>
            <ta e="T343" id="Seg_10702" s="T339">Да вот я беру.</ta>
            <ta e="T350" id="Seg_10703" s="T344">Меня потом в некрасивый дом перевели. [?]</ta>
            <ta e="T354" id="Seg_10704" s="T351">Я там живу.</ta>
            <ta e="T361" id="Seg_10705" s="T355">А потом как придут ко мне люди.</ta>
            <ta e="T364" id="Seg_10706" s="T362">Никто не…</ta>
            <ta e="T366" id="Seg_10707" s="T365">Не…</ta>
            <ta e="T374" id="Seg_10708" s="T372">Не берет.</ta>
            <ta e="T382" id="Seg_10709" s="T375">Все (бьют?) нехорошего человека. [?]</ta>
            <ta e="T398" id="Seg_10710" s="T387">Плохой человек у хорошего человека взял рубашку, мясо, и всё.</ta>
            <ta e="T403" id="Seg_10711" s="T399">Деньги были у этого человека.</ta>
            <ta e="T406" id="Seg_10712" s="T404">Все взял.</ta>
            <ta e="T410" id="Seg_10713" s="T407">Почему ты взял?</ta>
            <ta e="T417" id="Seg_10714" s="T411">Так у меня нет, а у тебя есть.</ta>
            <ta e="T421" id="Seg_10715" s="T418">(Вот?) я и взял.</ta>
            <ta e="T427" id="Seg_10716" s="T422">А я у тебя не возьму.</ta>
            <ta e="T431" id="Seg_10717" s="T428">Я у тебя возьму.</ta>
            <ta e="T437" id="Seg_10718" s="T432">А почему ты (?) у меня взял?</ta>
            <ta e="T442" id="Seg_10719" s="T438">Так на что тебе (?)?</ta>
            <ta e="T449" id="Seg_10720" s="T443">Я ношу всё. [?]</ta>
            <ta e="T457" id="Seg_10721" s="T450">А я тебя в плохой дом посажу. [?]</ta>
            <ta e="T465" id="Seg_10722" s="T462">Садись со мной есть.</ta>
            <ta e="T481" id="Seg_10723" s="T478">Садись со мной есть.</ta>
            <ta e="T486" id="Seg_10724" s="T482">А что у тебя есть?</ta>
            <ta e="T494" id="Seg_10725" s="T487">У меня есть хлеб, мясо, молоко.</ta>
            <ta e="T496" id="Seg_10726" s="T495">Сахар.</ta>
            <ta e="T498" id="Seg_10727" s="T497">Ешь.</ta>
            <ta e="T505" id="Seg_10728" s="T499">Так я бы масла поел.</ta>
            <ta e="T513" id="Seg_10729" s="T506">Ну и масло я тебе дам поесть.</ta>
            <ta e="T517" id="Seg_10730" s="T514">Только хорошо ешь.</ta>
            <ta e="T522" id="Seg_10731" s="T518">Тогда я поем. [?]</ta>
            <ta e="T526" id="Seg_10732" s="T523">Много буду есть.</ta>
            <ta e="T529" id="Seg_10733" s="T527">Я хорошо ем.</ta>
            <ta e="T542" id="Seg_10734" s="T534">Почему ты взял мои волосы своей рукой?</ta>
            <ta e="T550" id="Seg_10735" s="T543">Ведь твоя рука большая, а моя маленькая.</ta>
            <ta e="T554" id="Seg_10736" s="T551">Почему ты такой?</ta>
            <ta e="T556" id="Seg_10737" s="T555">Плохой.</ta>
            <ta e="T560" id="Seg_10738" s="T557">Нехорошо так.</ta>
            <ta e="T562" id="Seg_10739" s="T561">Вот.</ta>
            <ta e="T568" id="Seg_10740" s="T565">Я очень болею.</ta>
            <ta e="T571" id="Seg_10741" s="T569">Руки у меня болели.</ta>
            <ta e="T576" id="Seg_10742" s="T572">И голова у меня болит.</ta>
            <ta e="T580" id="Seg_10743" s="T577">Однако я умру.</ta>
            <ta e="T585" id="Seg_10744" s="T581">Нет, не надо умирать.</ta>
            <ta e="T588" id="Seg_10745" s="T586">Надо жить.</ta>
            <ta e="T591" id="Seg_10746" s="T589">Жить хорошо.</ta>
            <ta e="T595" id="Seg_10747" s="T592">А умирать плохо.</ta>
            <ta e="T598" id="Seg_10748" s="T596">Очень плохо.</ta>
            <ta e="T606" id="Seg_10749" s="T603">Жили два парня.</ta>
            <ta e="T614" id="Seg_10750" s="T607">Потом один парень []решил] куда-то уйти.</ta>
            <ta e="T619" id="Seg_10751" s="T615">А один дома живет.</ta>
            <ta e="T626" id="Seg_10752" s="T620">Ну, (дает?): тебе лошадь, мне корову.</ta>
            <ta e="T634" id="Seg_10753" s="T627">Нет, ты возьми лошадь, а я -- корову.</ta>
            <ta e="T637" id="Seg_10754" s="T635">О, черт!</ta>
            <ta e="T640" id="Seg_10755" s="T638">Почему?</ta>
            <ta e="T644" id="Seg_10756" s="T641">Ну, ты бродяга!</ta>
            <ta e="T646" id="Seg_10757" s="T645">Ты черт.</ta>
            <ta e="T650" id="Seg_10758" s="T647">Почему ты ругаешься?</ta>
            <ta e="T656" id="Seg_10759" s="T651">Так надо так ругаться, плохо!</ta>
            <ta e="T666" id="Seg_10760" s="T657">А ведь отец мне дал лошадь, а тебе корову.</ta>
            <ta e="T675" id="Seg_10761" s="T667">Сказал: ты возьми лошадь, а ты возьми корову.</ta>
            <ta e="T679" id="Seg_10762" s="T676">А дом тебе.</ta>
            <ta e="T685" id="Seg_10763" s="T680">И дом, и корова тебе.</ta>
            <ta e="T691" id="Seg_10764" s="T686">А теперь [он] корову не берет.</ta>
            <ta e="T694" id="Seg_10765" s="T692">Ну, вот!</ta>
            <ta e="T709" id="Seg_10766" s="T703">Весь народ, большой народ был.</ta>
            <ta e="T713" id="Seg_10767" s="T710">Мой народ был.</ta>
            <ta e="T716" id="Seg_10768" s="T714">И где [он]?</ta>
            <ta e="T720" id="Seg_10769" s="T717">Эх, где, где…</ta>
            <ta e="T724" id="Seg_10770" s="T721">Они плачут, плачут.</ta>
            <ta e="T730" id="Seg_10771" s="T725">Из своих домов их забрали.</ta>
            <ta e="T734" id="Seg_10772" s="T731">А почему забрали?</ta>
            <ta e="T737" id="Seg_10773" s="T735">Так почему.</ta>
            <ta e="T739" id="Seg_10774" s="T738">Ругались.</ta>
            <ta e="T745" id="Seg_10775" s="T740">И всякое говорили.</ta>
            <ta e="T750" id="Seg_10776" s="T746">Вот их и забрали.</ta>
            <ta e="T753" id="Seg_10777" s="T751">А куда?</ta>
            <ta e="T758" id="Seg_10778" s="T754">Ну я знаю куда?</ta>
            <ta e="T763" id="Seg_10779" s="T759">Я же не знаю.</ta>
            <ta e="T769" id="Seg_10780" s="T766">Пойдем грибы собирать.</ta>
            <ta e="T772" id="Seg_10781" s="T770">Грибы хорошие.</ta>
            <ta e="T777" id="Seg_10782" s="T773">А что будет [с ними]?</ta>
            <ta e="T781" id="Seg_10783" s="T778">Ну, что.</ta>
            <ta e="T788" id="Seg_10784" s="T782">А я потом сварю, есть стану.</ta>
            <ta e="T792" id="Seg_10785" s="T789">И ты поешь.</ta>
            <ta e="T795" id="Seg_10786" s="T793">Это хорошо.</ta>
            <ta e="T797" id="Seg_10787" s="T796">Вот.</ta>
            <ta e="T805" id="Seg_10788" s="T800">Я ему даю вон сколько.</ta>
            <ta e="T812" id="Seg_10789" s="T806">А он мне мало-мало дает.</ta>
            <ta e="T818" id="Seg_10790" s="T815">Ну, пошли со мной.</ta>
            <ta e="T820" id="Seg_10791" s="T819">Куда?</ta>
            <ta e="T823" id="Seg_10792" s="T821">Да туда.</ta>
            <ta e="T826" id="Seg_10793" s="T824">А твоя мать?</ta>
            <ta e="T829" id="Seg_10794" s="T827">Мать, пускай…</ta>
            <ta e="T835" id="Seg_10795" s="T830">Не возьму мать.</ta>
            <ta e="T840" id="Seg_10796" s="T836">Давай вдвоем пойдем.</ta>
            <ta e="T843" id="Seg_10797" s="T841">А куда?</ta>
            <ta e="T847" id="Seg_10798" s="T844">Да вон туда.</ta>
            <ta e="T850" id="Seg_10799" s="T848">Где люди.</ta>
            <ta e="T855" id="Seg_10800" s="T851">А что это там?</ta>
            <ta e="T859" id="Seg_10801" s="T856">[О] чем-то говорят.</ta>
            <ta e="T866" id="Seg_10802" s="T864">Они что-то говорят.</ta>
            <ta e="T873" id="Seg_10803" s="T867">А я ничего не буду говорить.</ta>
            <ta e="T878" id="Seg_10804" s="T874">А почему не будешь говорить?</ta>
            <ta e="T880" id="Seg_10805" s="T879">Нет.</ta>
            <ta e="T887" id="Seg_10806" s="T881">Если говорить, я плохим человеком стану.</ta>
            <ta e="T890" id="Seg_10807" s="T888">Почему?</ta>
            <ta e="T894" id="Seg_10808" s="T891">Нет, хорошим человеком.</ta>
            <ta e="T898" id="Seg_10809" s="T895">Нет, плохим человеком.</ta>
            <ta e="T904" id="Seg_10810" s="T899">Ну, я пойду (с?) тобой.</ta>
            <ta e="T912" id="Seg_10811" s="T907">Мой отец с твоим отцом ругаются.</ta>
            <ta e="T916" id="Seg_10812" s="T913">Почему они ругаются?</ta>
            <ta e="T928" id="Seg_10813" s="T917">Да он говорит, твой отец говорит: "Мой сын хороший, красивый.</ta>
            <ta e="T934" id="Seg_10814" s="T929">А твоя дочь не хорошая".</ta>
            <ta e="T944" id="Seg_10815" s="T935">А мой отец говорит: "Эх, моя дочь хорошая.</ta>
            <ta e="T947" id="Seg_10816" s="T945">И красивая.</ta>
            <ta e="T954" id="Seg_10817" s="T948">И всё-всё говорит.</ta>
            <ta e="T960" id="Seg_10818" s="T955">А твой сын не хорош.</ta>
            <ta e="T965" id="Seg_10819" s="T961">Ничего мне не говорит.</ta>
            <ta e="T972" id="Seg_10820" s="T966">А моя дочь всё-всё [говорит].</ta>
            <ta e="T976" id="Seg_10821" s="T973">Тебе всегда говорит.</ta>
            <ta e="T978" id="Seg_10822" s="T977">Вот.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_10823" s="T1">[It] was a good person, he died.</ta>
            <ta e="T8" id="Seg_10824" s="T6">He died well.</ta>
            <ta e="T17" id="Seg_10825" s="T9">And that bad person died (alone?).</ta>
            <ta e="T23" id="Seg_10826" s="T18">And the good person died well.</ta>
            <ta e="T30" id="Seg_10827" s="T26">My father lived well.</ta>
            <ta e="T33" id="Seg_10828" s="T31">Then badly.</ta>
            <ta e="T37" id="Seg_10829" s="T34">He lived very bad.</ta>
            <ta e="T44" id="Seg_10830" s="T38">Then (he?) lived very well. [?]</ta>
            <ta e="T47" id="Seg_10831" s="T45">Then he died.</ta>
            <ta e="T55" id="Seg_10832" s="T51">I lived very poorly.</ta>
            <ta e="T59" id="Seg_10833" s="T56">I lived very poorly.</ta>
            <ta e="T63" id="Seg_10834" s="T60">I (had?) no bread.</ta>
            <ta e="T72" id="Seg_10835" s="T64">I have no bread, and I ate myself (whatever I had?).</ta>
            <ta e="T76" id="Seg_10836" s="T73">Then I lived well.</ta>
            <ta e="T79" id="Seg_10837" s="T77">(In different ways?)</ta>
            <ta e="T83" id="Seg_10838" s="T80">Then I went away.</ta>
            <ta e="T86" id="Seg_10839" s="T84">I lived very badly.</ta>
            <ta e="T90" id="Seg_10840" s="T86">I (gave birth?) to a daughter.</ta>
            <ta e="T96" id="Seg_10841" s="T91">And with the daughter, too, I lived very badly.</ta>
            <ta e="T98" id="Seg_10842" s="T97">I cried.</ta>
            <ta e="T109" id="Seg_10843" s="T99">And went away, and came to live, very badly.</ta>
            <ta e="T113" id="Seg_10844" s="T110">Where can I go?</ta>
            <ta e="T119" id="Seg_10845" s="T114">I won't go anywhere, I'd rather die.</ta>
            <ta e="T121" id="Seg_10846" s="T120">This.</ta>
            <ta e="T126" id="Seg_10847" s="T122">All people live well.</ta>
            <ta e="T133" id="Seg_10848" s="T127">And then they go away from here, somewhere.</ta>
            <ta e="T140" id="Seg_10849" s="T134">And then (why?) do (they?) come here again?</ta>
            <ta e="T145" id="Seg_10850" s="T141">Eh, why will you [do] so?</ta>
            <ta e="T148" id="Seg_10851" s="T146">Why?</ta>
            <ta e="T150" id="Seg_10852" s="T149">Just so.</ta>
            <ta e="T153" id="Seg_10853" s="T151">He'll come here.</ta>
            <ta e="T157" id="Seg_10854" s="T154">And… he'll come here.</ta>
            <ta e="T163" id="Seg_10855" s="T158">What, you aren't going?</ta>
            <ta e="T165" id="Seg_10856" s="T164">No</ta>
            <ta e="T169" id="Seg_10857" s="T166">I won't go.</ta>
            <ta e="T173" id="Seg_10858" s="T170">I'll be here.</ta>
            <ta e="T181" id="Seg_10859" s="T178">[I] should cook something.</ta>
            <ta e="T185" id="Seg_10860" s="T182">I'll eat.</ta>
            <ta e="T192" id="Seg_10861" s="T188">What are you cooking?</ta>
            <ta e="T194" id="Seg_10862" s="T193">Meat.</ta>
            <ta e="T197" id="Seg_10863" s="T195">Why meat?</ta>
            <ta e="T202" id="Seg_10864" s="T198">I'd rather (?).</ta>
            <ta e="T208" id="Seg_10865" s="T203">And I'd rather have milk.</ta>
            <ta e="T213" id="Seg_10866" s="T209">Then I would eat well.</ta>
            <ta e="T216" id="Seg_10867" s="T214">Why milk?</ta>
            <ta e="T220" id="Seg_10868" s="T217">Milk is OK.</ta>
            <ta e="T224" id="Seg_10869" s="T221">Meat is good.</ta>
            <ta e="T231" id="Seg_10870" s="T227">Why doesn't s/he come to me?</ta>
            <ta e="T236" id="Seg_10871" s="T232">Because my mother is bad.</ta>
            <ta e="T241" id="Seg_10872" s="T237">(…) doesn't let you [go].</ta>
            <ta e="T245" id="Seg_10873" s="T242">Why not?</ta>
            <ta e="T252" id="Seg_10874" s="T246">Well, she will say that you are a bad person.</ta>
            <ta e="T257" id="Seg_10875" s="T253">Why a bad person?</ta>
            <ta e="T261" id="Seg_10876" s="T258">I'm a good person.</ta>
            <ta e="T269" id="Seg_10877" s="T262">I would tell you something.</ta>
            <ta e="T274" id="Seg_10878" s="T270">It will be very good right away. [?]</ta>
            <ta e="T280" id="Seg_10879" s="T275">Oh, what will he tell me. [?]</ta>
            <ta e="T286" id="Seg_10880" s="T281">I'll beat him hardly.</ta>
            <ta e="T291" id="Seg_10881" s="T287">No, don't beat.</ta>
            <ta e="T297" id="Seg_10882" s="T292">Else he'll beat me after.</ta>
            <ta e="T304" id="Seg_10883" s="T300">I drink much vodka.</ta>
            <ta e="T309" id="Seg_10884" s="T305">Oh, why you?</ta>
            <ta e="T314" id="Seg_10885" s="T310">Well, I am a bad person.</ta>
            <ta e="T320" id="Seg_10886" s="T315">I take everything. [?]</ta>
            <ta e="T328" id="Seg_10887" s="T321">What you take (for yourself?), then I will…</ta>
            <ta e="T332" id="Seg_10888" s="T329">I take then.</ta>
            <ta e="T338" id="Seg_10889" s="T333">Why do you take so. [?]</ta>
            <ta e="T343" id="Seg_10890" s="T339">So I take.</ta>
            <ta e="T350" id="Seg_10891" s="T344">They then took me to a not beautiful house. [?]</ta>
            <ta e="T354" id="Seg_10892" s="T351">I'm living there.</ta>
            <ta e="T361" id="Seg_10893" s="T355">Then people come to me.</ta>
            <ta e="T364" id="Seg_10894" s="T362">Nobody…</ta>
            <ta e="T366" id="Seg_10895" s="T365">Don't…</ta>
            <ta e="T374" id="Seg_10896" s="T372">He don't take.</ta>
            <ta e="T382" id="Seg_10897" s="T375">Everybody (beats?) the bad person. [?]</ta>
            <ta e="T398" id="Seg_10898" s="T387">A bad person took a good person's shirt, meat and everything.</ta>
            <ta e="T403" id="Seg_10899" s="T399">This person had money.</ta>
            <ta e="T406" id="Seg_10900" s="T404">He took everything.</ta>
            <ta e="T410" id="Seg_10901" s="T407">Why did you take?</ta>
            <ta e="T417" id="Seg_10902" s="T411">Because I have not, and you have.</ta>
            <ta e="T421" id="Seg_10903" s="T418">So I took.</ta>
            <ta e="T427" id="Seg_10904" s="T422">And I won't take yours.</ta>
            <ta e="T431" id="Seg_10905" s="T428">I'll take yours.</ta>
            <ta e="T437" id="Seg_10906" s="T432">Why did you take my (?)?</ta>
            <ta e="T442" id="Seg_10907" s="T438">What do you need (?) for?</ta>
            <ta e="T449" id="Seg_10908" s="T443">I carry everything. [?]</ta>
            <ta e="T457" id="Seg_10909" s="T450">I will put you in a bad house. [?]</ta>
            <ta e="T465" id="Seg_10910" s="T462">Sit down and eat with me.</ta>
            <ta e="T481" id="Seg_10911" s="T478">Sit down to eat with me.</ta>
            <ta e="T486" id="Seg_10912" s="T482">What do you have?</ta>
            <ta e="T494" id="Seg_10913" s="T487">I have bread, meat, milk.</ta>
            <ta e="T496" id="Seg_10914" s="T495">Sugar.</ta>
            <ta e="T498" id="Seg_10915" s="T497">Eat.</ta>
            <ta e="T505" id="Seg_10916" s="T499">I'd like to eat butter.</ta>
            <ta e="T513" id="Seg_10917" s="T506">Well, I'll give you butter, too.</ta>
            <ta e="T517" id="Seg_10918" s="T514">Only eat well.</ta>
            <ta e="T522" id="Seg_10919" s="T518">Then I'll eat. [?]</ta>
            <ta e="T526" id="Seg_10920" s="T523">I'll eat much.</ta>
            <ta e="T529" id="Seg_10921" s="T527">I eat well.</ta>
            <ta e="T542" id="Seg_10922" s="T534">Why did you take my hair with your hand.</ta>
            <ta e="T550" id="Seg_10923" s="T543">Your hand is big, and mine is small.</ta>
            <ta e="T554" id="Seg_10924" s="T551">Why are you like this?</ta>
            <ta e="T556" id="Seg_10925" s="T555">Bad.</ta>
            <ta e="T560" id="Seg_10926" s="T557">It's not good to [do] so.</ta>
            <ta e="T568" id="Seg_10927" s="T565">I am very ill.</ta>
            <ta e="T571" id="Seg_10928" s="T569">My hands hurt.</ta>
            <ta e="T576" id="Seg_10929" s="T572">And my head is aching.</ta>
            <ta e="T580" id="Seg_10930" s="T577">I will surely die.</ta>
            <ta e="T585" id="Seg_10931" s="T581">No, don't die.</ta>
            <ta e="T588" id="Seg_10932" s="T586">You should live.</ta>
            <ta e="T591" id="Seg_10933" s="T589">Living is good.</ta>
            <ta e="T595" id="Seg_10934" s="T592">And dying is bad.</ta>
            <ta e="T598" id="Seg_10935" s="T596">Very bad.</ta>
            <ta e="T606" id="Seg_10936" s="T603">There lived two boys.</ta>
            <ta e="T614" id="Seg_10937" s="T607">Then that boy [decided] to go somewhere.</ta>
            <ta e="T619" id="Seg_10938" s="T615">And one is living home.</ta>
            <ta e="T626" id="Seg_10939" s="T620">Well (he gives): the horse is for you, the cow is for me. </ta>
            <ta e="T634" id="Seg_10940" s="T627">No, you take the horse, and I [take] the cow.</ta>
            <ta e="T637" id="Seg_10941" s="T635">O, [you] a devil!</ta>
            <ta e="T640" id="Seg_10942" s="T638">Why?</ta>
            <ta e="T644" id="Seg_10943" s="T641">Well, you tramp!</ta>
            <ta e="T646" id="Seg_10944" s="T645">You a devil!</ta>
            <ta e="T650" id="Seg_10945" s="T647">Why are you cursing?</ta>
            <ta e="T656" id="Seg_10946" s="T651">Because I have to curse like this, it's bad.</ta>
            <ta e="T666" id="Seg_10947" s="T657">You know, our father gave the horse to me, and the cow to you.</ta>
            <ta e="T675" id="Seg_10948" s="T667">He said: you take the horse, and you take the cow.</ta>
            <ta e="T679" id="Seg_10949" s="T676">And the house is yours.</ta>
            <ta e="T685" id="Seg_10950" s="T680">Both the house and the cow are yours.</ta>
            <ta e="T691" id="Seg_10951" s="T686">And now he doesn't want to take the cow.</ta>
            <ta e="T694" id="Seg_10952" s="T692">Well, here!</ta>
            <ta e="T709" id="Seg_10953" s="T703">There was a whole people, a large people.</ta>
            <ta e="T713" id="Seg_10954" s="T710">There was my people.</ta>
            <ta e="T716" id="Seg_10955" s="T714">Where [are they]?</ta>
            <ta e="T720" id="Seg_10956" s="T717">Eh, where, where.</ta>
            <ta e="T724" id="Seg_10957" s="T721">They cry and cry.</ta>
            <ta e="T730" id="Seg_10958" s="T725">They were taken from their houses.</ta>
            <ta e="T734" id="Seg_10959" s="T731">And what they were taken for?</ta>
            <ta e="T737" id="Seg_10960" s="T735">So why.</ta>
            <ta e="T739" id="Seg_10961" s="T738">They cursed.</ta>
            <ta e="T745" id="Seg_10962" s="T740">And were saying all sorts of things.</ta>
            <ta e="T750" id="Seg_10963" s="T746">So they were taken away.</ta>
            <ta e="T753" id="Seg_10964" s="T751">Where to?</ta>
            <ta e="T758" id="Seg_10965" s="T754">Do I know?</ta>
            <ta e="T763" id="Seg_10966" s="T759">I don't know.</ta>
            <ta e="T769" id="Seg_10967" s="T766">Let's go and gather mushrooms.</ta>
            <ta e="T772" id="Seg_10968" s="T770">Mushroom[s] are good.</ta>
            <ta e="T777" id="Seg_10969" s="T773">What will be [with them]?</ta>
            <ta e="T781" id="Seg_10970" s="T778">Well, what…</ta>
            <ta e="T788" id="Seg_10971" s="T782">I'll boil them and eat.</ta>
            <ta e="T792" id="Seg_10972" s="T789">And you'll eat, too.</ta>
            <ta e="T795" id="Seg_10973" s="T793">This is good.</ta>
            <ta e="T805" id="Seg_10974" s="T800">I give him so much.</ta>
            <ta e="T812" id="Seg_10975" s="T806">And he gives me only a little.</ta>
            <ta e="T818" id="Seg_10976" s="T815">Well, come with me.</ta>
            <ta e="T820" id="Seg_10977" s="T819">Where to?</ta>
            <ta e="T823" id="Seg_10978" s="T821">There.</ta>
            <ta e="T826" id="Seg_10979" s="T824">And your mother?</ta>
            <ta e="T829" id="Seg_10980" s="T827">Mother, let her…</ta>
            <ta e="T835" id="Seg_10981" s="T830">I won't take mother.</ta>
            <ta e="T840" id="Seg_10982" s="T836">Let's go we two.</ta>
            <ta e="T843" id="Seg_10983" s="T841">Where to?</ta>
            <ta e="T847" id="Seg_10984" s="T844">There.</ta>
            <ta e="T850" id="Seg_10985" s="T848">Where are people.</ta>
            <ta e="T855" id="Seg_10986" s="T851">What is there?</ta>
            <ta e="T859" id="Seg_10987" s="T856">They are telling something.</ta>
            <ta e="T866" id="Seg_10988" s="T864">They are telling something.</ta>
            <ta e="T873" id="Seg_10989" s="T867">I won't say anything.</ta>
            <ta e="T878" id="Seg_10990" s="T874">Why won't you say?</ta>
            <ta e="T880" id="Seg_10991" s="T879">No.</ta>
            <ta e="T887" id="Seg_10992" s="T881">If I speak, I'll be a bad person.</ta>
            <ta e="T890" id="Seg_10993" s="T888">Why?</ta>
            <ta e="T894" id="Seg_10994" s="T891">[You'll be] a good person.</ta>
            <ta e="T898" id="Seg_10995" s="T895">No, a bad person.</ta>
            <ta e="T904" id="Seg_10996" s="T899">Well, I'll go (with?) you.</ta>
            <ta e="T912" id="Seg_10997" s="T907">My father is quarelling with your father.</ta>
            <ta e="T916" id="Seg_10998" s="T913">Why are they quarelling?</ta>
            <ta e="T928" id="Seg_10999" s="T917">Because your father says: "My son is good, handsome.</ta>
            <ta e="T934" id="Seg_11000" s="T929">Your daughter isn't good.</ta>
            <ta e="T944" id="Seg_11001" s="T935">And my father says: "My daughter is good.</ta>
            <ta e="T947" id="Seg_11002" s="T945">And pretty.</ta>
            <ta e="T954" id="Seg_11003" s="T948">And she says all sorts of things.</ta>
            <ta e="T960" id="Seg_11004" s="T955">And your son isn't good.</ta>
            <ta e="T965" id="Seg_11005" s="T961">He doesn't tell me anything.</ta>
            <ta e="T972" id="Seg_11006" s="T966">And my daughter [tells] everything.</ta>
            <ta e="T976" id="Seg_11007" s="T973">She speaks always to you.</ta>
            <ta e="T978" id="Seg_11008" s="T977">That's it.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_11009" s="T1">[Er] war eine gute Person, er starb.</ta>
            <ta e="T8" id="Seg_11010" s="T6">Er starb gut.</ta>
            <ta e="T17" id="Seg_11011" s="T9">Und diese schlechte Person starb (allein?).</ta>
            <ta e="T23" id="Seg_11012" s="T18">Und die gute Person starb gut.</ta>
            <ta e="T30" id="Seg_11013" s="T26">Mein Vater lebte gut.</ta>
            <ta e="T33" id="Seg_11014" s="T31">Dann schlecht.</ta>
            <ta e="T37" id="Seg_11015" s="T34">Er lebte sehr schlecht.</ta>
            <ta e="T44" id="Seg_11016" s="T38">Dann lebte (er?) sehr gut. [?]</ta>
            <ta e="T47" id="Seg_11017" s="T45">Dann starb er.</ta>
            <ta e="T55" id="Seg_11018" s="T51">Ich lebte sehr ärmlich.</ta>
            <ta e="T59" id="Seg_11019" s="T56">Ich lebte sehr ärmlich.</ta>
            <ta e="T63" id="Seg_11020" s="T60">Ich (hatte?) kein Brot.</ta>
            <ta e="T72" id="Seg_11021" s="T64">Ich habe kein Brot, und ich aß mich selbst (was auch immer ich hatte?).</ta>
            <ta e="T76" id="Seg_11022" s="T73">Dann lebte ich gut.</ta>
            <ta e="T79" id="Seg_11023" s="T77">(Auf unterschiedliche Weise?)</ta>
            <ta e="T83" id="Seg_11024" s="T80">Dann ging ich fort.</ta>
            <ta e="T86" id="Seg_11025" s="T84">Ich lebte sehr schlecht.</ta>
            <ta e="T90" id="Seg_11026" s="T86">Ich (gebar) eine Tochter. </ta>
            <ta e="T96" id="Seg_11027" s="T91">Und auch mit der Tochter, lebte ich sehr schlecht.</ta>
            <ta e="T98" id="Seg_11028" s="T97">Ich weinte.</ta>
            <ta e="T109" id="Seg_11029" s="T99">Und ging fort, und lebte sehr schlecht.</ta>
            <ta e="T113" id="Seg_11030" s="T110">Wo kann ich hingehen?</ta>
            <ta e="T119" id="Seg_11031" s="T114">Ich werde nirgendwo hingehen, ich würde lieber sterben.</ta>
            <ta e="T121" id="Seg_11032" s="T120">Dieses.</ta>
            <ta e="T126" id="Seg_11033" s="T122">Alle Leute leben gut.</ta>
            <ta e="T133" id="Seg_11034" s="T127">Und dann gehen sie fort von hier, irgendwohin.</ta>
            <ta e="T140" id="Seg_11035" s="T134">Und dann (warum?) kommen (sie) wieder hier her?</ta>
            <ta e="T145" id="Seg_11036" s="T141">Eh, warum wirst du das tun? </ta>
            <ta e="T148" id="Seg_11037" s="T146">Warum?</ta>
            <ta e="T150" id="Seg_11038" s="T149">Genau so.</ta>
            <ta e="T153" id="Seg_11039" s="T151">Er wird hier her kommen.</ta>
            <ta e="T157" id="Seg_11040" s="T154">Und... er wird hier her kommen.</ta>
            <ta e="T163" id="Seg_11041" s="T158">Was, du gehst nicht?</ta>
            <ta e="T165" id="Seg_11042" s="T164">Nein.</ta>
            <ta e="T169" id="Seg_11043" s="T166">Ich werde nicht gehen.</ta>
            <ta e="T173" id="Seg_11044" s="T170">Ich werde hier sein.</ta>
            <ta e="T181" id="Seg_11045" s="T178">[Ich] sollte etwas kochen.</ta>
            <ta e="T185" id="Seg_11046" s="T182">Ich will (/werde) essen.</ta>
            <ta e="T192" id="Seg_11047" s="T188">Was kochst du?</ta>
            <ta e="T194" id="Seg_11048" s="T193">Fleisch.</ta>
            <ta e="T197" id="Seg_11049" s="T195">Warum Fleisch?</ta>
            <ta e="T202" id="Seg_11050" s="T198">Ich würde lieber (?).</ta>
            <ta e="T208" id="Seg_11051" s="T203">Und ich würde lieber Milch haben.</ta>
            <ta e="T213" id="Seg_11052" s="T209">Dann würde ich gut essen.</ta>
            <ta e="T216" id="Seg_11053" s="T214">Warum Milch?</ta>
            <ta e="T220" id="Seg_11054" s="T217">Milch ist OK.</ta>
            <ta e="T224" id="Seg_11055" s="T221">Fleisch ist gut.</ta>
            <ta e="T231" id="Seg_11056" s="T227">Warum kommt sie/er nicht zu mir?</ta>
            <ta e="T236" id="Seg_11057" s="T232">Weil meine Mutter böse ist.</ta>
            <ta e="T241" id="Seg_11058" s="T237">(...) lässst dich nicht [gehen].</ta>
            <ta e="T245" id="Seg_11059" s="T242">Warum nicht?</ta>
            <ta e="T252" id="Seg_11060" s="T246">Nun, sie wird sagen, dass du eine schlechte Person bist.</ta>
            <ta e="T257" id="Seg_11061" s="T253">Warum eine schlechte Person?</ta>
            <ta e="T261" id="Seg_11062" s="T258">Ich bin eine gute Person.</ta>
            <ta e="T269" id="Seg_11063" s="T262">Ich würde dir etwas erzählen.</ta>
            <ta e="T274" id="Seg_11064" s="T270">Es wird sofort sehr gut sein. [?]</ta>
            <ta e="T280" id="Seg_11065" s="T275">Oh, was wird er mir erzählen. [?]</ta>
            <ta e="T286" id="Seg_11066" s="T281">Ich werde ihn fest schlagen.</ta>
            <ta e="T291" id="Seg_11067" s="T287">Nein, nicht schlagen.</ta>
            <ta e="T297" id="Seg_11068" s="T292">Sonst wird er mich danach schlagen.</ta>
            <ta e="T304" id="Seg_11069" s="T300">Ich trinke viel Wodka.</ta>
            <ta e="T309" id="Seg_11070" s="T305">Oh, warum du?</ta>
            <ta e="T314" id="Seg_11071" s="T310">Nun, ich bin eine schlechte Person.</ta>
            <ta e="T320" id="Seg_11072" s="T315">Ich nehme alles.</ta>
            <ta e="T328" id="Seg_11073" s="T321">Was du (für dich selbst?) nimmst, dann werde ich...</ta>
            <ta e="T332" id="Seg_11074" s="T329">Ich nehme dann.</ta>
            <ta e="T338" id="Seg_11075" s="T333">Warum nimmst du so. [?]</ta>
            <ta e="T343" id="Seg_11076" s="T339">So nehme ich.</ta>
            <ta e="T350" id="Seg_11077" s="T344">Sie brachte mich dann zu einem nicht schönen Haus. [?]</ta>
            <ta e="T354" id="Seg_11078" s="T351">Ich lebe dort.</ta>
            <ta e="T361" id="Seg_11079" s="T355">Dann kommen Leute zu mir.</ta>
            <ta e="T364" id="Seg_11080" s="T362">Niemand...</ta>
            <ta e="T366" id="Seg_11081" s="T365">Nicht...</ta>
            <ta e="T374" id="Seg_11082" s="T372">Er nimmt nicht.</ta>
            <ta e="T382" id="Seg_11083" s="T375">Jeder (schlägt) die schlechte Person.</ta>
            <ta e="T398" id="Seg_11084" s="T387">Eine schlechte Person nahm das Hemd einer guten Person, Fleisch und allles.</ta>
            <ta e="T403" id="Seg_11085" s="T399">Diese Person hatte Geld.</ta>
            <ta e="T406" id="Seg_11086" s="T404">Er nahm alles.</ta>
            <ta e="T410" id="Seg_11087" s="T407">Warum nahmst du?</ta>
            <ta e="T417" id="Seg_11088" s="T411">Weil ich nicht habe, und du hast.</ta>
            <ta e="T421" id="Seg_11089" s="T418">Also nahm ich.</ta>
            <ta e="T427" id="Seg_11090" s="T422">Und ich werde nicht deins nehmen.</ta>
            <ta e="T431" id="Seg_11091" s="T428">Ich werde deins nehmen.</ta>
            <ta e="T437" id="Seg_11092" s="T432">Warum nahmst du mein (?)?</ta>
            <ta e="T442" id="Seg_11093" s="T438">Wofür brauchst du (?)?</ta>
            <ta e="T449" id="Seg_11094" s="T443">Ich trage alles. (?)</ta>
            <ta e="T457" id="Seg_11095" s="T450">Ich werde dich in ein schlechte Haus setzen.</ta>
            <ta e="T465" id="Seg_11096" s="T462">Setze dich und esse mit mir.</ta>
            <ta e="T481" id="Seg_11097" s="T478">Setze dich um mit mir zu essen.</ta>
            <ta e="T486" id="Seg_11098" s="T482">Was hast du?</ta>
            <ta e="T494" id="Seg_11099" s="T487">Ich habe Brot, Fleisch, Milch.</ta>
            <ta e="T496" id="Seg_11100" s="T495">Zucker.</ta>
            <ta e="T498" id="Seg_11101" s="T497">Iss.</ta>
            <ta e="T505" id="Seg_11102" s="T499">Ich würde gerne Butter essen.</ta>
            <ta e="T513" id="Seg_11103" s="T506">Also, ich werde dir auch Butter geben.</ta>
            <ta e="T517" id="Seg_11104" s="T514">Iss nur gut.</ta>
            <ta e="T522" id="Seg_11105" s="T518">Dann werde ich essen. [?]</ta>
            <ta e="T526" id="Seg_11106" s="T523">Ich werde viel essen.</ta>
            <ta e="T529" id="Seg_11107" s="T527">Ich esse gut.</ta>
            <ta e="T542" id="Seg_11108" s="T534">Warum hast du meine Haare mit deiner Hand genommen?</ta>
            <ta e="T550" id="Seg_11109" s="T543">Deine Hand ist groß, und deine ist klein.</ta>
            <ta e="T554" id="Seg_11110" s="T551">Warum bist du so?</ta>
            <ta e="T556" id="Seg_11111" s="T555">Schlecht.</ta>
            <ta e="T560" id="Seg_11112" s="T557">Es ist nicht gut das zu [tun].</ta>
            <ta e="T568" id="Seg_11113" s="T565">Ich bin sehr krank.</ta>
            <ta e="T571" id="Seg_11114" s="T569">Meine Hände schmerzen.</ta>
            <ta e="T576" id="Seg_11115" s="T572">Und mein Kopf tut weh.</ta>
            <ta e="T580" id="Seg_11116" s="T577">Ich werde sicher sterben.</ta>
            <ta e="T585" id="Seg_11117" s="T581">Nein, stirb nicht.</ta>
            <ta e="T588" id="Seg_11118" s="T586">Du sollst leben.</ta>
            <ta e="T591" id="Seg_11119" s="T589">Leben ist gut.</ta>
            <ta e="T595" id="Seg_11120" s="T592">Und sterben ist schlecht.</ta>
            <ta e="T598" id="Seg_11121" s="T596">Sehr schlecht.</ta>
            <ta e="T606" id="Seg_11122" s="T603">Dort lebten zwei Jungen.</ta>
            <ta e="T614" id="Seg_11123" s="T607">Dann [entschied] der Junge irgendwohin zu gehen.</ta>
            <ta e="T619" id="Seg_11124" s="T615">Und einer lebt zu Hause.</ta>
            <ta e="T626" id="Seg_11125" s="T620">Also (gibt er): das Pferd ist für dich, the Kuh ist für mich.</ta>
            <ta e="T634" id="Seg_11126" s="T627">Nein, du nimmst das Pferd, und ich nehme die Kuh.</ta>
            <ta e="T637" id="Seg_11127" s="T635">O, du Teufel!</ta>
            <ta e="T640" id="Seg_11128" s="T638">Warum?</ta>
            <ta e="T644" id="Seg_11129" s="T641">Also, du Landstreicher!</ta>
            <ta e="T646" id="Seg_11130" s="T645">Du Teufel!</ta>
            <ta e="T650" id="Seg_11131" s="T647">Warum fluchst du?</ta>
            <ta e="T656" id="Seg_11132" s="T651">Weil ich so fluchen muss, es ist schlecht.</ta>
            <ta e="T666" id="Seg_11133" s="T657">Weißst du, unser Vater gab mir das Pferd, und dir die Kuh.</ta>
            <ta e="T675" id="Seg_11134" s="T667">Er sagte: du nimmst das Pferd, und du nimmst die Kuh.</ta>
            <ta e="T679" id="Seg_11135" s="T676">Und das Haus ist deins.</ta>
            <ta e="T685" id="Seg_11136" s="T680">Sowohl das Haus als auch die Kuh sind deins.</ta>
            <ta e="T691" id="Seg_11137" s="T686">Und jetzt will er nicht die Kuh nehmen.</ta>
            <ta e="T694" id="Seg_11138" s="T692">Nun!</ta>
            <ta e="T709" id="Seg_11139" s="T703">Da war eine ganze Person, eine große Person.</ta>
            <ta e="T713" id="Seg_11140" s="T710">Dort waren meine Leute.</ta>
            <ta e="T716" id="Seg_11141" s="T714">Wo [sind sie]?</ta>
            <ta e="T720" id="Seg_11142" s="T717">Eh, wo, wo.</ta>
            <ta e="T724" id="Seg_11143" s="T721">Sie weinen und weinen.</ta>
            <ta e="T730" id="Seg_11144" s="T725">Sie sind wurden aus ihrem Haus geholt.</ta>
            <ta e="T734" id="Seg_11145" s="T731">Und warum wurde sie geholt?</ta>
            <ta e="T737" id="Seg_11146" s="T735">Also warum.</ta>
            <ta e="T739" id="Seg_11147" s="T738">Sie fluchten.</ta>
            <ta e="T745" id="Seg_11148" s="T740">Und sagten alle möglichen Dinge.</ta>
            <ta e="T750" id="Seg_11149" s="T746">Also wurden sie weg geholt.</ta>
            <ta e="T753" id="Seg_11150" s="T751">Wohin?</ta>
            <ta e="T758" id="Seg_11151" s="T754">Weiß ich das?</ta>
            <ta e="T763" id="Seg_11152" s="T759">Weiß ich nicht.</ta>
            <ta e="T769" id="Seg_11153" s="T766">Lass und gehen und Pilze sammeln.</ta>
            <ta e="T772" id="Seg_11154" s="T770">Pilze sind gut.</ta>
            <ta e="T777" id="Seg_11155" s="T773">Was wird [mit ihnen] sein? </ta>
            <ta e="T781" id="Seg_11156" s="T778">Also, was...</ta>
            <ta e="T788" id="Seg_11157" s="T782">Ich werde sie kochen und essen.</ta>
            <ta e="T792" id="Seg_11158" s="T789">Und du wirst auch essen.</ta>
            <ta e="T795" id="Seg_11159" s="T793">Das ist gut.</ta>
            <ta e="T805" id="Seg_11160" s="T800">Ich gebe ihm so viel.</ta>
            <ta e="T812" id="Seg_11161" s="T806">Und er gibt mir nur ein wenig.</ta>
            <ta e="T818" id="Seg_11162" s="T815">Also, komm mit mir.</ta>
            <ta e="T820" id="Seg_11163" s="T819">Wohin?</ta>
            <ta e="T823" id="Seg_11164" s="T821">Dort.</ta>
            <ta e="T826" id="Seg_11165" s="T824">Und deine Mutter?</ta>
            <ta e="T829" id="Seg_11166" s="T827">Mutter, lass sie...</ta>
            <ta e="T835" id="Seg_11167" s="T830">Ich werde meine Mutter nicht mitnehmen.</ta>
            <ta e="T840" id="Seg_11168" s="T836">Lass uns gehen, wir beide.</ta>
            <ta e="T843" id="Seg_11169" s="T841">Wohin?</ta>
            <ta e="T847" id="Seg_11170" s="T844">Dort.</ta>
            <ta e="T850" id="Seg_11171" s="T848">Wo Leute sind.</ta>
            <ta e="T855" id="Seg_11172" s="T851">Was ist dort?</ta>
            <ta e="T859" id="Seg_11173" s="T856">Sie erzählen etwas.</ta>
            <ta e="T866" id="Seg_11174" s="T864">Sie erzählen etwas.</ta>
            <ta e="T873" id="Seg_11175" s="T867">Ich werde nichts sagen.</ta>
            <ta e="T878" id="Seg_11176" s="T874">Warum wirst du nicht sagen?</ta>
            <ta e="T880" id="Seg_11177" s="T879">Nein.</ta>
            <ta e="T887" id="Seg_11178" s="T881">Wenn ich spreche, werde ich zu einer schlechte Person.</ta>
            <ta e="T890" id="Seg_11179" s="T888">Warum?</ta>
            <ta e="T894" id="Seg_11180" s="T891">Nein, [du wirst] eine gute Person.</ta>
            <ta e="T898" id="Seg_11181" s="T895">Nein, eine schlechte Person.</ta>
            <ta e="T904" id="Seg_11182" s="T899">Also, ich werde mit dir gehen.</ta>
            <ta e="T912" id="Seg_11183" s="T907">Mein Vater streitet mit deinem Vater.</ta>
            <ta e="T916" id="Seg_11184" s="T913">Warum streiten sie?</ta>
            <ta e="T928" id="Seg_11185" s="T917">Weil dein Vater sagt: 'Mein Sohn ist gut, schön.</ta>
            <ta e="T934" id="Seg_11186" s="T929">Deine Tochter ist nicht gut.</ta>
            <ta e="T944" id="Seg_11187" s="T935">Und mein Vater sagt: 'Meine Tochter ist gut.</ta>
            <ta e="T947" id="Seg_11188" s="T945">Und hübsch.</ta>
            <ta e="T954" id="Seg_11189" s="T948">Und sie sagt alle möglichen Dinge.</ta>
            <ta e="T960" id="Seg_11190" s="T955">Und dein Sohn ist nicht gut.</ta>
            <ta e="T965" id="Seg_11191" s="T961">Er erzählt mir nichts.</ta>
            <ta e="T972" id="Seg_11192" s="T966">Und meine Tochter erzählt alles.</ta>
            <ta e="T976" id="Seg_11193" s="T973">Sie sprecht immer mit dir.</ta>
            <ta e="T978" id="Seg_11194" s="T977">Das ist alles.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T50" id="Seg_11195" s="T48">[GVY:] Maybe just a false start.</ta>
            <ta e="T98" id="Seg_11196" s="T97">[GVY:] Presumably this sentence must be in the past tense.</ta>
            <ta e="T185" id="Seg_11197" s="T182">[GVY:] This may also mean "I want to eat" (= 'I am hungry')</ta>
            <ta e="T216" id="Seg_11198" s="T214">[GVY:] Moʔ?</ta>
            <ta e="T274" id="Seg_11199" s="T270">[GVY:] The interpretation is tentative. Tăn 'you'?</ta>
            <ta e="T320" id="Seg_11200" s="T315">[GVY:] iləŋ? This whole fragment is unclear.</ta>
            <ta e="T382" id="Seg_11201" s="T375">[GVY:] münörlaʔi?</ta>
            <ta e="T449" id="Seg_11202" s="T443">[GVY:] This and the next sentence are fully incomprehensible.</ta>
            <ta e="T486" id="Seg_11203" s="T482">[GVY:] Or mĭlat 'give'? [WNB] The analysis of molat is tentative.</ta>
            <ta e="T505" id="Seg_11204" s="T499">[GVY:] Or amorbiam.</ta>
            <ta e="T522" id="Seg_11205" s="T518">[GVY:] Unclear.</ta>
            <ta e="T550" id="Seg_11206" s="T543">[GVY:] Or 'I am small'.</ta>
            <ta e="T571" id="Seg_11207" s="T569">[GVY:] ĭzemmiet.</ta>
            <ta e="T580" id="Seg_11208" s="T577">[GVY:] the verbal form is inclear.</ta>
            <ta e="T595" id="Seg_11209" s="T592">[GVY:] Or rather "A kulam vedʼ bile"</ta>
            <ta e="T606" id="Seg_11210" s="T603">[GVY:] Amnobiiʔ.</ta>
            <ta e="T619" id="Seg_11211" s="T615">[GVY:] amnolapliet.</ta>
            <ta e="T626" id="Seg_11212" s="T620">[GVY:] Or măllat 'he says'?</ta>
            <ta e="T644" id="Seg_11213" s="T641">[GVY:] It is a curse.</ta>
            <ta e="T691" id="Seg_11214" s="T686">[GVY:] An interpretation "you don't want…" (literally "you don't take") is also possible.</ta>
            <ta e="T702" id="Seg_11215" s="T699">[GVY:] Unclear.</ta>
            <ta e="T709" id="Seg_11216" s="T703">[GVY:] ivijeʔ</ta>
            <ta e="T713" id="Seg_11217" s="T710">[GVY:] ilbəm?</ta>
            <ta e="T777" id="Seg_11218" s="T773">[GVY:] The translation is tentative.</ta>
            <ta e="T840" id="Seg_11219" s="T836">[GVY:] Maybe there is Russian мы instead of miʔ.</ta>
            <ta e="T904" id="Seg_11220" s="T899">[GVY:] or "to you", or "to your place"?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
