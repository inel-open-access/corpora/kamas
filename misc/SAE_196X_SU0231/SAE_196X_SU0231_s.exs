<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDF3FE8797-AD8E-2DC7-1444-06B18097B838">
   <head>
      <meta-information>
         <project-name>Kamas</project-name>
         <transcription-name>SAE_196X_SU0231</transcription-name>
         <referenced-file url="SAE_196X_SU0231.wav" />
         <referenced-file url="SAE_196X_SU0231.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\SAE_196X_SU0231\SAE_196X_SU0231.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">908</ud-information>
            <ud-information attribute-name="# HIAT:w">535</ud-information>
            <ud-information attribute-name="# e">552</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">17</ud-information>
            <ud-information attribute-name="# HIAT:u">188</ud-information>
            <ud-information attribute-name="# sc">368</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SAE">
            <abbreviation>SAE</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="MAK">
            <abbreviation>MAK</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="3.3" type="appl" />
         <tli id="T2" time="3.94" type="appl" />
         <tli id="T3" time="6.46" type="appl" />
         <tli id="T4" time="7.26" type="appl" />
         <tli id="T5" time="8.156666269995739" />
         <tli id="T6" time="8.89" type="appl" />
         <tli id="T7" time="9.02" type="appl" />
         <tli id="T8" time="9.37" type="appl" />
         <tli id="T9" time="11.8" type="appl" />
         <tli id="T10" time="12.835" type="appl" />
         <tli id="T11" time="13.870000000000001" type="appl" />
         <tli id="T12" time="14.905" type="appl" />
         <tli id="T13" time="15.94" type="appl" />
         <tli id="T14" time="16.58" type="appl" />
         <tli id="T15" time="17.225" type="appl" />
         <tli id="T16" time="17.87" type="appl" />
         <tli id="T17" time="20.21" type="appl" />
         <tli id="T18" time="20.986666666666668" type="appl" />
         <tli id="T19" time="21.763333333333332" type="appl" />
         <tli id="T20" time="22.54" type="appl" />
         <tli id="T21" time="23.22" type="appl" />
         <tli id="T22" time="24.18" type="appl" />
         <tli id="T23" time="25.14" type="appl" />
         <tli id="T24" time="26.1" type="appl" />
         <tli id="T25" time="27.06" type="appl" />
         <tli id="T26" time="28.02" type="appl" />
         <tli id="T27" time="28.94" type="appl" />
         <tli id="T28" time="29.886000000000003" type="appl" />
         <tli id="T29" time="30.832" type="appl" />
         <tli id="T30" time="31.778000000000002" type="appl" />
         <tli id="T31" time="32.724000000000004" type="appl" />
         <tli id="T32" time="33.67" type="appl" />
         <tli id="T33" time="34.66" type="appl" />
         <tli id="T34" time="36.18" type="appl" />
         <tli id="T35" time="37.699999999999996" type="appl" />
         <tli id="T36" time="39.22" type="appl" />
         <tli id="T37" time="39.7" type="appl" />
         <tli id="T38" time="40.2075" type="appl" />
         <tli id="T39" time="40.715" type="appl" />
         <tli id="T40" time="41.2225" type="appl" />
         <tli id="T41" time="41.73" type="appl" />
         <tli id="T42" time="42.67" type="appl" />
         <tli id="T43" time="43.913333333333334" type="appl" />
         <tli id="T44" time="45.156666666666666" type="appl" />
         <tli id="T45" time="46.4" type="appl" />
         <tli id="T46" time="46.78" type="appl" />
         <tli id="T47" time="48.126666666666665" type="appl" />
         <tli id="T48" time="49.473333333333336" type="appl" />
         <tli id="T49" time="50.82" type="appl" />
         <tli id="T50" time="53.93" type="appl" />
         <tli id="T51" time="54.55" type="appl" />
         <tli id="T52" time="55.17" type="appl" />
         <tli id="T53" time="56.25" type="appl" />
         <tli id="T54" time="58.98" type="appl" />
         <tli id="T55" time="59.407999999999994" type="appl" />
         <tli id="T56" time="59.836" type="appl" />
         <tli id="T57" time="60.263999999999996" type="appl" />
         <tli id="T58" time="60.63" type="appl" />
         <tli id="T59" time="60.692" type="appl" />
         <tli id="T60" time="61.12" type="appl" />
         <tli id="T61" time="61.43" type="appl" />
         <tli id="T62" time="63.49" type="appl" />
         <tli id="T63" time="64.736" type="appl" />
         <tli id="T64" time="65.982" type="appl" />
         <tli id="T65" time="67.228" type="appl" />
         <tli id="T66" time="68.474" type="appl" />
         <tli id="T67" time="69.72" type="appl" />
         <tli id="T68" time="71.16" type="appl" />
         <tli id="T69" time="72.215" type="appl" />
         <tli id="T70" time="73.27" type="appl" />
         <tli id="T71" time="73.94" type="appl" />
         <tli id="T72" time="74.5375" type="appl" />
         <tli id="T73" time="75.13499999999999" type="appl" />
         <tli id="T74" time="75.7325" type="appl" />
         <tli id="T75" time="76.33" type="appl" />
         <tli id="T76" time="77.34" type="appl" />
         <tli id="T77" time="78.82666666666667" type="appl" />
         <tli id="T78" time="80.31333333333333" type="appl" />
         <tli id="T79" time="81.8" type="appl" />
         <tli id="T80" time="84.07" type="appl" />
         <tli id="T81" time="85.35" type="appl" />
         <tli id="T82" time="88.17" type="appl" />
         <tli id="T83" time="89.15" type="appl" />
         <tli id="T84" time="92.04" type="appl" />
         <tli id="T85" time="93.305" type="appl" />
         <tli id="T86" time="94.57" type="appl" />
         <tli id="T87" time="95.835" type="appl" />
         <tli id="T88" time="97.1" type="appl" />
         <tli id="T89" time="98.32" type="appl" />
         <tli id="T90" time="98.96" type="appl" />
         <tli id="T91" time="99.6" type="appl" />
         <tli id="T92" time="100.24" type="appl" />
         <tli id="T93" time="100.55" type="appl" />
         <tli id="T94" time="101.45" type="appl" />
         <tli id="T95" time="102.09" type="appl" />
         <tli id="T96" time="102.96333333333334" type="appl" />
         <tli id="T97" time="103.83666666666666" type="appl" />
         <tli id="T98" time="104.71" type="appl" />
         <tli id="T99" time="105.38" type="appl" />
         <tli id="T100" time="106.768" type="appl" />
         <tli id="T101" time="108.15599999999999" type="appl" />
         <tli id="T102" time="109.544" type="appl" />
         <tli id="T103" time="110.63" type="appl" />
         <tli id="T104" time="111.84" type="appl" />
         <tli id="T105" time="113.05" type="appl" />
         <tli id="T106" time="114.26" type="appl" />
         <tli id="T107" time="115.47" type="appl" />
         <tli id="T108" time="116.68" type="appl" />
         <tli id="T109" time="117.65" type="appl" />
         <tli id="T110" time="118.67016666666667" type="appl" />
         <tli id="T111" time="119.69033333333334" type="appl" />
         <tli id="T112" time="120.7105" type="appl" />
         <tli id="T113" time="121.73066666666666" type="appl" />
         <tli id="T114" time="122.75083333333333" type="appl" />
         <tli id="T115" time="123.771" type="appl" />
         <tli id="T116" time="126.57" type="appl" />
         <tli id="T117" time="127.18" type="appl" />
         <tli id="T118" time="127.79" type="appl" />
         <tli id="T119" time="127.96" type="appl" />
         <tli id="T120" time="128.65" type="appl" />
         <tli id="T121" time="129.24" type="appl" />
         <tli id="T122" time="129.83" type="appl" />
         <tli id="T123" time="130.03" type="appl" />
         <tli id="T124" time="130.72" type="appl" />
         <tli id="T125" time="131.41" type="appl" />
         <tli id="T126" time="132.1" type="appl" />
         <tli id="T127" time="133.04" type="appl" />
         <tli id="T128" time="133.64499999999998" type="appl" />
         <tli id="T129" time="134.25" type="appl" />
         <tli id="T130" time="134.34" type="appl" />
         <tli id="T131" time="134.80166666666668" type="appl" />
         <tli id="T132" time="135.26333333333335" type="appl" />
         <tli id="T133" time="135.72500000000002" type="appl" />
         <tli id="T134" time="136.18666666666667" type="appl" />
         <tli id="T135" time="136.64833333333334" type="appl" />
         <tli id="T136" time="137.11" type="appl" />
         <tli id="T137" time="138.58" type="appl" />
         <tli id="T138" time="139.08" type="appl" />
         <tli id="T139" time="139.82" type="appl" />
         <tli id="T140" time="140.52" type="appl" />
         <tli id="T141" time="141.07" type="appl" />
         <tli id="T142" time="141.85" type="appl" />
         <tli id="T143" time="144.73" type="appl" />
         <tli id="T144" time="145.7625" type="appl" />
         <tli id="T145" time="146.79500000000002" type="appl" />
         <tli id="T146" time="147.82750000000001" type="appl" />
         <tli id="T147" time="148.86" type="appl" />
         <tli id="T148" time="149.14" type="appl" />
         <tli id="T149" time="149.67666666666665" type="appl" />
         <tli id="T150" time="150.21333333333334" type="appl" />
         <tli id="T151" time="150.75" type="appl" />
         <tli id="T152" time="151.15" type="appl" />
         <tli id="T153" time="152.11" type="appl" />
         <tli id="T154" time="152.85" type="appl" />
         <tli id="T155" time="153.4875" type="appl" />
         <tli id="T156" time="154.125" type="appl" />
         <tli id="T157" time="154.7625" type="appl" />
         <tli id="T158" time="155.4" type="appl" />
         <tli id="T159" time="156.49" type="appl" />
         <tli id="T160" time="157.78" type="appl" />
         <tli id="T161" time="159.07000000000002" type="appl" />
         <tli id="T162" time="160.36" type="appl" />
         <tli id="T163" time="160.79" type="appl" />
         <tli id="T164" time="162.04" type="appl" />
         <tli id="T165" time="163.38" type="appl" />
         <tli id="T166" time="163.91666666666666" type="appl" />
         <tli id="T167" time="164.45333333333335" type="appl" />
         <tli id="T168" time="164.99" type="appl" />
         <tli id="T169" time="167.16" type="appl" />
         <tli id="T170" time="167.96666666666667" type="appl" />
         <tli id="T171" time="168.77333333333334" type="appl" />
         <tli id="T172" time="169.58" type="appl" />
         <tli id="T173" time="170.92" type="appl" />
         <tli id="T174" time="171.7" type="appl" />
         <tli id="T175" time="172.67" type="appl" />
         <tli id="T176" time="173.37" type="appl" />
         <tli id="T177" time="174.88" type="appl" />
         <tli id="T178" time="175.535" type="appl" />
         <tli id="T179" time="176.19" type="appl" />
         <tli id="T180" time="176.845" type="appl" />
         <tli id="T181" time="177.5" type="appl" />
         <tli id="T182" time="178.155" type="appl" />
         <tli id="T183" time="178.81" type="appl" />
         <tli id="T184" time="179.16" type="appl" />
         <tli id="T185" time="179.63" type="appl" />
         <tli id="T186" time="180.1" type="appl" />
         <tli id="T187" time="180.57" type="appl" />
         <tli id="T188" time="180.69" type="appl" />
         <tli id="T189" time="181.27666666666667" type="appl" />
         <tli id="T190" time="181.86333333333332" type="appl" />
         <tli id="T191" time="182.45" type="appl" />
         <tli id="T192" time="183.02499999999998" type="appl" />
         <tli id="T193" time="183.6" type="appl" />
         <tli id="T194" time="184.24" type="appl" />
         <tli id="T195" time="185.044" type="appl" />
         <tli id="T196" time="185.848" type="appl" />
         <tli id="T197" time="186.652" type="appl" />
         <tli id="T198" time="187.456" type="appl" />
         <tli id="T199" time="188.26" type="appl" />
         <tli id="T200" time="189.03" type="appl" />
         <tli id="T201" time="190.19" type="appl" />
         <tli id="T202" time="190.9" type="appl" />
         <tli id="T203" time="191.905" type="appl" />
         <tli id="T204" time="192.91" type="appl" />
         <tli id="T205" time="194.01" type="appl" />
         <tli id="T206" time="194.7575" type="appl" />
         <tli id="T207" time="195.505" type="appl" />
         <tli id="T208" time="196.2525" type="appl" />
         <tli id="T209" time="197.0" type="appl" />
         <tli id="T210" time="198.44" type="appl" />
         <tli id="T211" time="199.68" type="appl" />
         <tli id="T212" time="202.34" type="appl" />
         <tli id="T213" time="202.78" type="appl" />
         <tli id="T214" time="204.67" type="appl" />
         <tli id="T215" time="206.14" type="appl" />
         <tli id="T216" time="207.60999999999999" type="appl" />
         <tli id="T217" time="209.07999999999998" type="appl" />
         <tli id="T218" time="210.55" type="appl" />
         <tli id="T219" time="212.02" type="appl" />
         <tli id="T220" time="213.49" type="appl" />
         <tli id="T221" time="214.25" type="appl" />
         <tli id="T222" time="215.24" type="appl" />
         <tli id="T223" time="216.8" type="appl" />
         <tli id="T224" time="217.47" type="appl" />
         <tli id="T225" time="217.5" type="appl" />
         <tli id="T226" time="218.84" type="appl" />
         <tli id="T227" time="220.18" type="appl" />
         <tli id="T228" time="221.51999999999998" type="appl" />
         <tli id="T229" time="222.85999999999999" type="appl" />
         <tli id="T230" time="224.2" type="appl" />
         <tli id="T231" time="225.54" type="appl" />
         <tli id="T232" time="225.78" type="appl" />
         <tli id="T233" time="226.446" type="appl" />
         <tli id="T234" time="227.112" type="appl" />
         <tli id="T235" time="227.778" type="appl" />
         <tli id="T236" time="228.47" type="appl" />
         <tli id="T237" time="229.815" type="appl" />
         <tli id="T238" time="231.16" type="appl" />
         <tli id="T239" time="235.38" type="appl" />
         <tli id="T240" time="235.976" type="appl" />
         <tli id="T241" time="236.572" type="appl" />
         <tli id="T242" time="237.168" type="appl" />
         <tli id="T243" time="237.764" type="appl" />
         <tli id="T244" time="238.36" type="appl" />
         <tli id="T245" time="239.38" type="appl" />
         <tli id="T246" time="239.84666666666666" type="appl" />
         <tli id="T247" time="240.31333333333333" type="appl" />
         <tli id="T248" time="240.78" type="appl" />
         <tli id="T249" time="241.56" type="appl" />
         <tli id="T250" time="242.46333333333334" type="appl" />
         <tli id="T251" time="243.36666666666667" type="appl" />
         <tli id="T252" time="244.27" type="appl" />
         <tli id="T253" time="245.08" type="appl" />
         <tli id="T254" time="245.73666666666668" type="appl" />
         <tli id="T255" time="246.39333333333335" type="appl" />
         <tli id="T256" time="247.05" type="appl" />
         <tli id="T257" time="248.11" type="appl" />
         <tli id="T258" time="248.774" type="appl" />
         <tli id="T259" time="249.43800000000002" type="appl" />
         <tli id="T260" time="250.102" type="appl" />
         <tli id="T261" time="250.76600000000002" type="appl" />
         <tli id="T262" time="251.43" type="appl" />
         <tli id="T263" time="252.02" type="appl" />
         <tli id="T264" time="254.88333333333335" type="appl" />
         <tli id="T265" time="257.74666666666667" type="appl" />
         <tli id="T266" time="260.61" type="appl" />
         <tli id="T267" time="262.93" type="appl" />
         <tli id="T268" time="263.5478" type="appl" />
         <tli id="T269" time="264.1656" type="appl" />
         <tli id="T270" time="264.78340000000003" type="appl" />
         <tli id="T271" time="265.4012" type="appl" />
         <tli id="T272" time="266.019" type="appl" />
         <tli id="T273" time="266.609" type="appl" />
         <tli id="T274" time="266.9715" type="appl" />
         <tli id="T275" time="267.334" type="appl" />
         <tli id="T276" time="267.6965" type="appl" />
         <tli id="T277" time="268.059" type="appl" />
         <tli id="T278" time="268.219" type="appl" />
         <tli id="T279" time="268.689" type="appl" />
         <tli id="T280" time="270.139" type="appl" />
         <tli id="T281" time="271.384" type="appl" />
         <tli id="T282" time="272.629" type="appl" />
         <tli id="T283" time="273.874" type="appl" />
         <tli id="T284" time="275.119" type="appl" />
         <tli id="T285" time="277.04" type="appl" />
         <tli id="T286" time="277.928" type="appl" />
         <tli id="T287" time="278.81600000000003" type="appl" />
         <tli id="T288" time="279.704" type="appl" />
         <tli id="T289" time="280.592" type="appl" />
         <tli id="T290" time="281.58" type="appl" />
         <tli id="T291" time="282.26" type="appl" />
         <tli id="T292" time="282.94" type="appl" />
         <tli id="T293" time="283.72" type="appl" />
         <tli id="T294" time="284.475" type="appl" />
         <tli id="T295" time="285.23" type="appl" />
         <tli id="T296" time="286.43" type="appl" />
         <tli id="T297" time="287.28499999999997" type="appl" />
         <tli id="T298" time="288.14" type="appl" />
         <tli id="T299" time="288.96" type="appl" />
         <tli id="T300" time="290.48" type="appl" />
         <tli id="T301" time="291.97" type="appl" />
         <tli id="T302" time="293.9725" type="appl" />
         <tli id="T303" time="295.975" type="appl" />
         <tli id="T304" time="297.9775" type="appl" />
         <tli id="T305" time="299.98" type="appl" />
         <tli id="T306" time="300.8" type="appl" />
         <tli id="T307" time="301.24333333333334" type="appl" />
         <tli id="T308" time="301.68666666666667" type="appl" />
         <tli id="T309" time="302.13" type="appl" />
         <tli id="T310" time="302.89" type="appl" />
         <tli id="T311" time="304.13666666666666" type="appl" />
         <tli id="T312" time="305.3833333333333" type="appl" />
         <tli id="T313" time="306.63" type="appl" />
         <tli id="T314" time="308.74" type="appl" />
         <tli id="T315" time="310.11" type="appl" />
         <tli id="T316" time="311.48" type="appl" />
         <tli id="T317" time="312.39" type="appl" />
         <tli id="T318" time="312.95" type="appl" />
         <tli id="T319" time="313.51" type="appl" />
         <tli id="T320" time="314.4" type="appl" />
         <tli id="T321" time="315.6433333333333" type="appl" />
         <tli id="T322" time="316.88666666666666" type="appl" />
         <tli id="T323" time="318.13" type="appl" />
         <tli id="T324" time="320.74" type="appl" />
         <tli id="T325" time="321.32500000000005" type="appl" />
         <tli id="T326" time="321.91" type="appl" />
         <tli id="T327" time="322.54" type="appl" />
         <tli id="T328" time="323.31" type="appl" />
         <tli id="T329" time="325.2" type="appl" />
         <tli id="T330" time="325.9033333333333" type="appl" />
         <tli id="T331" time="326.6066666666667" type="appl" />
         <tli id="T332" time="327.31" type="appl" />
         <tli id="T333" time="328.38" type="appl" />
         <tli id="T334" time="328.85" type="appl" />
         <tli id="T335" time="329.32" type="appl" />
         <tli id="T336" time="330.37" type="appl" />
         <tli id="T337" time="331.11" type="appl" />
         <tli id="T338" time="331.85" type="appl" />
         <tli id="T339" time="332.16" type="appl" />
         <tli id="T340" time="333.00800000000004" type="appl" />
         <tli id="T341" time="333.856" type="appl" />
         <tli id="T342" time="334.704" type="appl" />
         <tli id="T343" time="335.55199999999996" type="appl" />
         <tli id="T344" time="336.4" type="appl" />
         <tli id="T345" time="337.01" type="appl" />
         <tli id="T346" time="337.568" type="appl" />
         <tli id="T347" time="338.126" type="appl" />
         <tli id="T348" time="338.684" type="appl" />
         <tli id="T349" time="339.242" type="appl" />
         <tli id="T350" time="339.8" type="appl" />
         <tli id="T351" time="340.2" type="appl" />
         <tli id="T352" time="341.2675" type="appl" />
         <tli id="T353" time="342.33500000000004" type="appl" />
         <tli id="T354" time="343.40250000000003" type="appl" />
         <tli id="T355" time="344.47" type="appl" />
         <tli id="T356" time="344.49" type="appl" />
         <tli id="T357" time="345.37666666666667" type="appl" />
         <tli id="T358" time="346.2633333333333" type="appl" />
         <tli id="T359" time="347.15" type="appl" />
         <tli id="T360" time="348.52" type="appl" />
         <tli id="T361" time="349.43666666666667" type="appl" />
         <tli id="T362" time="350.3533333333333" type="appl" />
         <tli id="T363" time="351.27" type="appl" />
         <tli id="T364" time="352.78" type="appl" />
         <tli id="T365" time="353.1133333333333" type="appl" />
         <tli id="T366" time="353.44666666666666" type="appl" />
         <tli id="T367" time="353.78" type="appl" />
         <tli id="T368" time="354.91999999999996" type="appl" />
         <tli id="T369" time="356.06" type="appl" />
         <tli id="T370" time="357.2" type="appl" />
         <tli id="T371" time="358.62" type="appl" />
         <tli id="T372" time="359.56333333333333" type="appl" />
         <tli id="T373" time="360.50666666666666" type="appl" />
         <tli id="T374" time="361.45" type="appl" />
         <tli id="T375" time="362.28" type="appl" />
         <tli id="T376" time="362.91999999999996" type="appl" />
         <tli id="T377" time="363.56" type="appl" />
         <tli id="T378" time="365.05" type="appl" />
         <tli id="T379" time="365.55333333333334" type="appl" />
         <tli id="T380" time="366.0566666666667" type="appl" />
         <tli id="T381" time="366.56" type="appl" />
         <tli id="T382" time="367.57" type="appl" />
         <tli id="T383" time="368.33" type="appl" />
         <tli id="T384" time="369.49" type="appl" />
         <tli id="T385" time="371.04333333333335" type="appl" />
         <tli id="T386" time="372.59666666666664" type="appl" />
         <tli id="T387" time="374.15" type="appl" />
         <tli id="T388" time="375.49" type="appl" />
         <tli id="T389" time="376.55" type="appl" />
         <tli id="T390" time="377.42" type="appl" />
         <tli id="T391" time="378.3466666666667" type="appl" />
         <tli id="T392" time="379.27333333333337" type="appl" />
         <tli id="T393" time="380.20000000000005" type="appl" />
         <tli id="T394" time="381.12666666666667" type="appl" />
         <tli id="T395" time="382.05333333333334" type="appl" />
         <tli id="T396" time="382.98" type="appl" />
         <tli id="T397" time="383.49" type="appl" />
         <tli id="T398" time="384.75" type="appl" />
         <tli id="T399" time="385.07" type="appl" />
         <tli id="T400" time="385.38" type="appl" />
         <tli id="T401" time="387.58" type="appl" />
         <tli id="T402" time="388.69" type="appl" />
         <tli id="T403" time="389.8" type="appl" />
         <tli id="T404" time="390.91" type="appl" />
         <tli id="T405" time="391.46" type="appl" />
         <tli id="T406" time="392.155" type="appl" />
         <tli id="T407" time="392.85" type="appl" />
         <tli id="T408" time="394.01" type="appl" />
         <tli id="T409" time="394.64666666666665" type="appl" />
         <tli id="T410" time="395.28333333333336" type="appl" />
         <tli id="T411" time="395.92" type="appl" />
         <tli id="T412" time="397.34" type="appl" />
         <tli id="T413" time="398.0233333333333" type="appl" />
         <tli id="T414" time="398.70666666666665" type="appl" />
         <tli id="T415" time="399.39" type="appl" />
         <tli id="T416" time="399.68" type="appl" />
         <tli id="T417" time="400.79" type="appl" />
         <tli id="T418" time="404.52" type="appl" />
         <tli id="T419" time="405.2133333333333" type="appl" />
         <tli id="T420" time="405.9066666666667" type="appl" />
         <tli id="T421" time="406.6" type="appl" />
         <tli id="T422" time="406.87" type="appl" />
         <tli id="T423" time="407.515" type="appl" />
         <tli id="T424" time="408.16" type="appl" />
         <tli id="T425" time="408.31" type="appl" />
         <tli id="T426" time="408.74666666666667" type="appl" />
         <tli id="T427" time="409.18333333333334" type="appl" />
         <tli id="T428" time="409.62" type="appl" />
         <tli id="T429" time="409.86" type="appl" />
         <tli id="T430" time="410.4125" type="appl" />
         <tli id="T431" time="410.96500000000003" type="appl" />
         <tli id="T432" time="411.5175" type="appl" />
         <tli id="T433" time="412.07" type="appl" />
         <tli id="T434" time="412.63" type="appl" />
         <tli id="T435" time="413.7575" type="appl" />
         <tli id="T436" time="414.885" type="appl" />
         <tli id="T437" time="416.0125" type="appl" />
         <tli id="T438" time="417.14" type="appl" />
         <tli id="T439" time="420.32" type="appl" />
         <tli id="T440" time="421.2675" type="appl" />
         <tli id="T441" time="422.21500000000003" type="appl" />
         <tli id="T442" time="423.1625" type="appl" />
         <tli id="T443" time="424.11" type="appl" />
         <tli id="T444" time="424.86" type="appl" />
         <tli id="T445" time="426.57500000000005" type="appl" />
         <tli id="T446" time="428.29" type="appl" />
         <tli id="T447" time="430.34" type="appl" />
         <tli id="T448" time="430.97" type="appl" />
         <tli id="T449" time="431.54" type="appl" />
         <tli id="T450" time="433.46" type="appl" />
         <tli id="T451" time="434.13" type="appl" />
         <tli id="T452" time="434.78" type="appl" />
         <tli id="T453" time="435.43" type="appl" />
         <tli id="T454" time="436.08" type="appl" />
         <tli id="T455" time="437.6" type="appl" />
         <tli id="T456" time="438.285" type="appl" />
         <tli id="T457" time="438.97" type="appl" />
         <tli id="T458" time="439.655" type="appl" />
         <tli id="T459" time="440.34" type="appl" />
         <tli id="T460" time="441.57" type="appl" />
         <tli id="T461" time="442.13" type="appl" />
         <tli id="T462" time="444.26" type="appl" />
         <tli id="T463" time="444.89" type="appl" />
         <tli id="T464" time="445.52" type="appl" />
         <tli id="T465" time="446.72" type="appl" />
         <tli id="T466" time="447.56" type="appl" />
         <tli id="T467" time="448.89" type="appl" />
         <tli id="T468" time="449.525" type="appl" />
         <tli id="T469" time="450.16" type="appl" />
         <tli id="T470" time="450.94" type="appl" />
         <tli id="T471" time="451.635" type="appl" />
         <tli id="T472" time="452.33" type="appl" />
         <tli id="T473" time="453.0" type="appl" />
         <tli id="T474" time="453.54" type="appl" />
         <tli id="T475" time="454.51" type="appl" />
         <tli id="T476" time="455.42" type="appl" />
         <tli id="T477" time="456.36" type="appl" />
         <tli id="T478" time="456.80333333333334" type="appl" />
         <tli id="T479" time="457.24666666666667" type="appl" />
         <tli id="T480" time="457.69" type="appl" />
         <tli id="T481" time="458.48" type="appl" />
         <tli id="T482" time="459.305" type="appl" />
         <tli id="T483" time="460.13" type="appl" />
         <tli id="T484" time="461.82" type="appl" />
         <tli id="T485" time="462.83" type="appl" />
         <tli id="T486" time="463.86" type="appl" />
         <tli id="T487" time="464.91" type="appl" />
         <tli id="T488" time="467.96" type="appl" />
         <tli id="T489" time="468.63" type="appl" />
         <tli id="T490" time="469.3" type="appl" />
         <tli id="T491" time="469.96999999999997" type="appl" />
         <tli id="T492" time="470.64" type="appl" />
         <tli id="T493" time="471.31" type="appl" />
         <tli id="T494" time="471.74" type="appl" />
         <tli id="T495" time="472.24666666666667" type="appl" />
         <tli id="T496" time="472.75333333333333" type="appl" />
         <tli id="T497" time="473.26" type="appl" />
         <tli id="T498" time="473.76666666666665" type="appl" />
         <tli id="T499" time="474.2733333333333" type="appl" />
         <tli id="T500" time="474.78" type="appl" />
         <tli id="T501" time="478.23" type="appl" />
         <tli id="T502" time="478.68" type="appl" />
         <tli id="T503" time="479.5" type="appl" />
         <tli id="T504" time="480.65250000000003" type="appl" />
         <tli id="T505" time="481.805" type="appl" />
         <tli id="T506" time="482.9575" type="appl" />
         <tli id="T507" time="484.11" type="appl" />
         <tli id="T508" time="485.42" type="appl" />
         <tli id="T509" time="486.476" type="appl" />
         <tli id="T510" time="487.53200000000004" type="appl" />
         <tli id="T511" time="488.588" type="appl" />
         <tli id="T512" time="489.644" type="appl" />
         <tli id="T513" time="492.92" type="appl" />
         <tli id="T514" time="493.45" type="appl" />
         <tli id="T515" time="493.98" type="appl" />
         <tli id="T516" time="494.51" type="appl" />
         <tli id="T517" time="495.35" type="appl" />
         <tli id="T518" time="496.46" type="appl" />
         <tli id="T519" time="497.07" type="appl" />
         <tli id="T520" time="497.7633333333333" type="appl" />
         <tli id="T521" time="498.45666666666665" type="appl" />
         <tli id="T522" time="499.15" type="appl" />
         <tli id="T523" time="499.92" type="appl" />
         <tli id="T524" time="500.90333333333336" type="appl" />
         <tli id="T525" time="501.88666666666666" type="appl" />
         <tli id="T526" time="502.87" type="appl" />
         <tli id="T527" time="503.85333333333335" type="appl" />
         <tli id="T528" time="504.83666666666664" type="appl" />
         <tli id="T529" time="505.82" type="appl" />
         <tli id="T530" time="507.14" type="appl" />
         <tli id="T531" time="508.01" type="appl" />
         <tli id="T532" time="510.05" type="appl" />
         <tli id="T533" time="510.6" type="appl" />
         <tli id="T534" time="512.03" type="appl" />
         <tli id="T535" time="512.78" type="appl" />
         <tli id="T536" time="513.53" type="appl" />
         <tli id="T537" time="514.28" type="appl" />
         <tli id="T538" time="515.03" type="appl" />
         <tli id="T539" time="515.78" type="appl" />
         <tli id="T540" time="516.53" type="appl" />
         <tli id="T541" time="517.28" type="appl" />
         <tli id="T542" time="518.03" type="appl" />
         <tli id="T543" time="518.78" type="appl" />
         <tli id="T544" time="519.53" type="appl" />
         <tli id="T545" time="520.28" type="appl" />
         <tli id="T546" time="521.03" type="appl" />
         <tli id="T547" time="521.78" type="appl" />
         <tli id="T548" time="522.53" type="appl" />
         <tli id="T549" time="523.28" type="appl" />
         <tli id="T550" time="524.03" type="appl" />
         <tli id="T551" time="524.9" type="appl" />
         <tli id="T552" time="525.2733333333333" type="appl" />
         <tli id="T553" time="525.6466666666666" type="appl" />
         <tli id="T554" time="526.02" type="appl" />
         <tli id="T555" time="528.36" type="appl" />
         <tli id="T556" time="528.9466666666667" type="appl" />
         <tli id="T557" time="529.5333333333333" type="appl" />
         <tli id="T558" time="530.12" type="appl" />
         <tli id="T559" time="531.74" type="appl" />
         <tli id="T560" time="532.56" type="appl" />
         <tli id="T561" time="532.66" type="appl" />
         <tli id="T562" time="533.208" type="appl" />
         <tli id="T563" time="533.756" type="appl" />
         <tli id="T564" time="534.304" type="appl" />
         <tli id="T565" time="534.852" type="appl" />
         <tli id="T566" time="535.4" type="appl" />
         <tli id="T567" time="536.7" type="appl" />
         <tli id="T568" time="537.68" type="appl" />
         <tli id="T569" time="538.25" type="appl" />
         <tli id="T570" time="538.885" type="appl" />
         <tli id="T571" time="539.52" type="appl" />
         <tli id="T572" time="540.155" type="appl" />
         <tli id="T573" time="540.79" type="appl" />
         <tli id="T574" time="541.51" type="appl" />
         <tli id="T575" time="542.8166666666666" type="appl" />
         <tli id="T576" time="544.1233333333333" type="appl" />
         <tli id="T577" time="545.43" type="appl" />
         <tli id="T578" time="546.94" type="appl" />
         <tli id="T579" time="548.2583333333334" type="appl" />
         <tli id="T580" time="549.5766666666667" type="appl" />
         <tli id="T581" time="550.895" type="appl" />
         <tli id="T582" time="552.2133333333334" type="appl" />
         <tli id="T583" time="553.5316666666668" type="appl" />
         <tli id="T584" time="554.85" type="appl" />
         <tli id="T585" time="555.12" type="appl" />
         <tli id="T586" time="556.96" type="appl" />
         <tli id="T587" time="558.8" type="appl" />
         <tli id="T588" time="560.08" type="appl" />
         <tli id="T589" time="561.61" type="appl" />
         <tli id="T590" time="563.14" type="appl" />
         <tli id="T591" time="564.67" type="appl" />
         <tli id="T592" time="565.12" type="appl" />
         <tli id="T593" time="565.618" type="appl" />
         <tli id="T594" time="566.116" type="appl" />
         <tli id="T595" time="566.614" type="appl" />
         <tli id="T596" time="567.112" type="appl" />
         <tli id="T597" time="567.61" type="appl" />
         <tli id="T598" time="568.19" type="appl" />
         <tli id="T599" time="568.6700000000001" type="appl" />
         <tli id="T600" time="569.1500000000001" type="appl" />
         <tli id="T601" time="569.63" type="appl" />
         <tli id="T602" time="570.11" type="appl" />
         <tli id="T603" time="570.91" type="appl" />
         <tli id="T604" time="571.8375" type="appl" />
         <tli id="T605" time="572.765" type="appl" />
         <tli id="T606" time="573.6925" type="appl" />
         <tli id="T607" time="574.62" type="appl" />
         <tli id="T608" time="575.48" type="appl" />
         <tli id="T609" time="576.52" type="appl" />
         <tli id="T610" time="577.56" type="appl" />
         <tli id="T611" time="578.6" type="appl" />
         <tli id="T612" time="579.64" type="appl" />
         <tli id="T613" time="580.86" type="appl" />
         <tli id="T614" time="581.5924" type="appl" />
         <tli id="T615" time="582.3248" type="appl" />
         <tli id="T616" time="583.0572000000001" type="appl" />
         <tli id="T617" time="583.7896000000001" type="appl" />
         <tli id="T618" time="584.522" type="appl" />
         <tli id="T619" time="584.9" type="appl" />
         <tli id="T620" time="585.81" type="appl" />
         <tli id="T621" time="586.72" type="appl" />
         <tli id="T622" time="588.67" type="appl" />
         <tli id="T623" time="589.57" type="appl" />
         <tli id="T624" time="590.65" type="appl" />
         <tli id="T625" time="591.77" type="appl" />
         <tli id="T626" time="592.89" type="appl" />
         <tli id="T627" time="594.01" type="appl" />
         <tli id="T628" time="594.89" type="appl" />
         <tli id="T629" time="595.6633333333333" type="appl" />
         <tli id="T630" time="596.4366666666667" type="appl" />
         <tli id="T631" time="597.21" type="appl" />
         <tli id="T632" time="597.48" type="appl" />
         <tli id="T633" time="598.37" type="appl" />
         <tli id="T634" time="599.26" type="appl" />
         <tli id="T635" time="599.69" type="appl" />
         <tli id="T636" time="600.17" type="appl" />
         <tli id="T637" time="600.91" type="appl" />
         <tli id="T638" time="601.37" type="appl" />
         <tli id="T639" time="603.79" type="appl" />
         <tli id="T640" time="604.685" type="appl" />
         <tli id="T641" time="605.5799999999999" type="appl" />
         <tli id="T642" time="606.475" type="appl" />
         <tli id="T643" time="607.37" type="appl" />
         <tli id="T644" time="607.96" type="appl" />
         <tli id="T645" time="608.5833333333334" type="appl" />
         <tli id="T646" time="609.2066666666667" type="appl" />
         <tli id="T647" time="609.83" type="appl" />
         <tli id="T648" time="610.64" type="appl" />
         <tli id="T649" time="611.2775" type="appl" />
         <tli id="T650" time="611.915" type="appl" />
         <tli id="T651" time="612.5525" type="appl" />
         <tli id="T652" time="613.19" type="appl" />
         <tli id="T653" time="613.31" type="appl" />
         <tli id="T654" time="613.9233333333333" type="appl" />
         <tli id="T655" time="614.5366666666666" type="appl" />
         <tli id="T656" time="615.15" type="appl" />
         <tli id="T657" time="615.48" type="appl" />
         <tli id="T658" time="616.0250000000001" type="appl" />
         <tli id="T659" time="616.57" type="appl" />
         <tli id="T660" time="617.29" type="appl" />
         <tli id="T661" time="618.2166666666667" type="appl" />
         <tli id="T662" time="619.1433333333333" type="appl" />
         <tli id="T663" time="620.07" type="appl" />
         <tli id="T664" time="620.3" type="appl" />
         <tli id="T665" time="621.1624999999999" type="appl" />
         <tli id="T666" time="622.025" type="appl" />
         <tli id="T667" time="622.8875" type="appl" />
         <tli id="T668" time="623.75" type="appl" />
         <tli id="T669" time="624.73" type="appl" />
         <tli id="T670" time="625.09" type="appl" />
         <tli id="T671" time="626.7" type="appl" />
         <tli id="T672" time="627.3375000000001" type="appl" />
         <tli id="T673" time="627.975" type="appl" />
         <tli id="T674" time="628.6125" type="appl" />
         <tli id="T675" time="629.25" type="appl" />
         <tli id="T676" time="629.99" type="appl" />
         <tli id="T677" time="630.702" type="appl" />
         <tli id="T678" time="631.414" type="appl" />
         <tli id="T679" time="632.126" type="appl" />
         <tli id="T680" time="632.838" type="appl" />
         <tli id="T681" time="633.55" type="appl" />
         <tli id="T682" time="633.98" type="appl" />
         <tli id="T683" time="634.47125" type="appl" />
         <tli id="T684" time="634.9625" type="appl" />
         <tli id="T685" time="635.45375" type="appl" />
         <tli id="T686" time="635.9449999999999" type="appl" />
         <tli id="T687" time="636.43625" type="appl" />
         <tli id="T688" time="636.9275" type="appl" />
         <tli id="T689" time="637.4187499999999" type="appl" />
         <tli id="T690" time="637.91" type="appl" />
         <tli id="T691" time="638.38" type="appl" />
         <tli id="T692" time="638.8675000000001" type="appl" />
         <tli id="T693" time="639.355" type="appl" />
         <tli id="T694" time="639.8425" type="appl" />
         <tli id="T695" time="640.33" type="appl" />
         <tli id="T696" time="641.25" type="appl" />
         <tli id="T697" time="641.9449999999999" type="appl" />
         <tli id="T698" time="642.64" type="appl" />
         <tli id="T699" time="643.335" type="appl" />
         <tli id="T700" time="644.03" type="appl" />
         <tli id="T701" time="644.53" type="appl" />
         <tli id="T702" time="645.2028571428572" type="appl" />
         <tli id="T703" time="645.8757142857143" type="appl" />
         <tli id="T704" time="646.5485714285714" type="appl" />
         <tli id="T705" time="647.2214285714285" type="appl" />
         <tli id="T706" time="647.8942857142857" type="appl" />
         <tli id="T707" time="648.5671428571428" type="appl" />
         <tli id="T708" time="649.24" type="appl" />
         <tli id="T709" time="650.65" type="appl" />
         <tli id="T710" time="651.85075" type="appl" />
         <tli id="T711" time="653.0515" type="appl" />
         <tli id="T712" time="654.25225" type="appl" />
         <tli id="T713" time="655.453" type="appl" />
         <tli id="T714" time="656.77" type="appl" />
         <tli id="T715" time="657.672" type="appl" />
         <tli id="T716" time="658.574" type="appl" />
         <tli id="T717" time="659.476" type="appl" />
         <tli id="T718" time="660.3779999999999" type="appl" />
         <tli id="T719" time="661.28" type="appl" />
         <tli id="T720" time="661.4" type="appl" />
         <tli id="T721" time="662.174" type="appl" />
         <tli id="T722" time="662.948" type="appl" />
         <tli id="T723" time="663.722" type="appl" />
         <tli id="T724" time="664.496" type="appl" />
         <tli id="T725" time="665.27" type="appl" />
         <tli id="T726" time="665.59" type="appl" />
         <tli id="T727" time="666.4000000000001" type="appl" />
         <tli id="T728" time="667.21" type="appl" />
         <tli id="T729" time="667.47" type="appl" />
         <tli id="T730" time="668.096" type="appl" />
         <tli id="T731" time="668.722" type="appl" />
         <tli id="T732" time="669.3480000000001" type="appl" />
         <tli id="T733" time="669.974" type="appl" />
         <tli id="T734" time="670.6" type="appl" />
         <tli id="T735" time="672.32" type="appl" />
         <tli id="T736" time="672.99" type="appl" />
         <tli id="T0" time="673.307" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-SAE"
                      id="tx-SAE"
                      speaker="SAE"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-SAE">
            <ts e="T2" id="Seg_0" n="sc" s="T1">
               <ts e="T2" id="Seg_2" n="HIAT:u" s="T1">
                  <nts id="Seg_3" n="HIAT:ip">(</nts>
                  <nts id="Seg_4" n="HIAT:ip">(</nts>
                  <ats e="T2" id="Seg_5" n="HIAT:non-pho" s="T1">BRK</ats>
                  <nts id="Seg_6" n="HIAT:ip">)</nts>
                  <nts id="Seg_7" n="HIAT:ip">)</nts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T7" id="Seg_10" n="sc" s="T5">
               <ts e="T7" id="Seg_12" n="HIAT:u" s="T5">
                  <ts e="T7" id="Seg_14" n="HIAT:w" s="T5">Говорить</ts>
                  <nts id="Seg_15" n="HIAT:ip">?</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T13" id="Seg_17" n="sc" s="T9">
               <ts e="T13" id="Seg_19" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_21" n="HIAT:w" s="T9">Abam</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_24" n="HIAT:w" s="T10">mo</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_27" n="HIAT:w" s="T11">dereʔ</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_30" n="HIAT:w" s="T12">nörbəlat</ts>
                  <nts id="Seg_31" n="HIAT:ip">.</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T16" id="Seg_33" n="sc" s="T14">
               <ts e="T16" id="Seg_35" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_37" n="HIAT:w" s="T14">Šen</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_40" n="HIAT:w" s="T15">bile</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T20" id="Seg_43" n="sc" s="T17">
               <ts e="T20" id="Seg_45" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_47" n="HIAT:w" s="T17">Šen</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_50" n="HIAT:w" s="T18">bile</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_53" n="HIAT:w" s="T19">nörbəlat</ts>
                  <nts id="Seg_54" n="HIAT:ip">.</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T26" id="Seg_56" n="sc" s="T21">
               <ts e="T26" id="Seg_58" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_60" n="HIAT:w" s="T21">Mo</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_63" n="HIAT:w" s="T22">dereʔ</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_65" n="HIAT:ip">(</nts>
                  <ts e="T24" id="Seg_67" n="HIAT:w" s="T23">ne-</ts>
                  <nts id="Seg_68" n="HIAT:ip">)</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_71" n="HIAT:w" s="T24">tăn</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_74" n="HIAT:w" s="T25">nörblat</ts>
                  <nts id="Seg_75" n="HIAT:ip">?</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T32" id="Seg_77" n="sc" s="T27">
               <ts e="T32" id="Seg_79" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_81" n="HIAT:w" s="T27">Dʼok</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_84" n="HIAT:w" s="T28">măn</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_87" n="HIAT:w" s="T29">jakše</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_90" n="HIAT:w" s="T30">nörblam</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_93" n="HIAT:w" s="T31">tănan</ts>
                  <nts id="Seg_94" n="HIAT:ip">.</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T36" id="Seg_96" n="sc" s="T33">
               <ts e="T36" id="Seg_98" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_100" n="HIAT:w" s="T33">Nen</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_103" n="HIAT:w" s="T34">anaʔ</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_106" n="HIAT:w" s="T35">dĭbər-tə</ts>
                  <nts id="Seg_107" n="HIAT:ip">.</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T41" id="Seg_109" n="sc" s="T37">
               <ts e="T41" id="Seg_111" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_113" n="HIAT:w" s="T37">Nu</ts>
                  <nts id="Seg_114" n="HIAT:ip">,</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_117" n="HIAT:w" s="T38">măn</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_120" n="HIAT:w" s="T39">ej</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_123" n="HIAT:w" s="T40">allam</ts>
                  <nts id="Seg_124" n="HIAT:ip">.</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T45" id="Seg_126" n="sc" s="T42">
               <ts e="T45" id="Seg_128" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_130" n="HIAT:w" s="T42">Moʔ</ts>
                  <nts id="Seg_131" n="HIAT:ip">,</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_134" n="HIAT:w" s="T43">dak</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_137" n="HIAT:w" s="T44">moʔ</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T49" id="Seg_140" n="sc" s="T46">
               <ts e="T49" id="Seg_142" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_144" n="HIAT:w" s="T46">Măn</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_147" n="HIAT:w" s="T47">udam</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_150" n="HIAT:w" s="T48">naga</ts>
                  <nts id="Seg_151" n="HIAT:ip">.</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T53" id="Seg_153" n="sc" s="T50">
               <ts e="T52" id="Seg_155" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_157" n="HIAT:w" s="T50">Ну</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_160" n="HIAT:w" s="T51">вот</ts>
                  <nts id="Seg_161" n="HIAT:ip">.</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_164" n="HIAT:u" s="T52">
                  <nts id="Seg_165" n="HIAT:ip">(</nts>
                  <nts id="Seg_166" n="HIAT:ip">(</nts>
                  <ats e="T53" id="Seg_167" n="HIAT:non-pho" s="T52">BRK</ats>
                  <nts id="Seg_168" n="HIAT:ip">)</nts>
                  <nts id="Seg_169" n="HIAT:ip">)</nts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T60" id="Seg_172" n="sc" s="T54">
               <ts e="T60" id="Seg_174" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_176" n="HIAT:w" s="T54">Ой</ts>
                  <nts id="Seg_177" n="HIAT:ip">,</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_180" n="HIAT:w" s="T55">я</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_183" n="HIAT:w" s="T56">беру</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_186" n="HIAT:w" s="T57">в</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_189" n="HIAT:w" s="T59">руки</ts>
                  <nts id="Seg_190" n="HIAT:ip">…</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T67" id="Seg_192" n="sc" s="T62">
               <ts e="T67" id="Seg_194" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_196" n="HIAT:w" s="T62">Aba</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_199" n="HIAT:w" s="T63">koško</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_202" n="HIAT:w" s="T64">moʔ</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_205" n="HIAT:w" s="T65">dereʔ</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_208" n="HIAT:w" s="T66">tăn</ts>
                  <nts id="Seg_209" n="HIAT:ip">?</nts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T70" id="Seg_211" n="sc" s="T68">
               <ts e="T70" id="Seg_213" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_215" n="HIAT:w" s="T68">Anaʔ</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_218" n="HIAT:w" s="T69">dĭbər</ts>
                  <nts id="Seg_219" n="HIAT:ip">.</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T75" id="Seg_221" n="sc" s="T71">
               <ts e="T75" id="Seg_223" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_225" n="HIAT:w" s="T71">Dʼok</ts>
                  <nts id="Seg_226" n="HIAT:ip">,</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_229" n="HIAT:w" s="T72">măn</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_232" n="HIAT:w" s="T73">ej</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_235" n="HIAT:w" s="T74">allam</ts>
                  <nts id="Seg_236" n="HIAT:ip">.</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T79" id="Seg_238" n="sc" s="T76">
               <ts e="T79" id="Seg_240" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_242" n="HIAT:w" s="T76">Măn</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_245" n="HIAT:w" s="T77">udam</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_248" n="HIAT:w" s="T78">bile</ts>
                  <nts id="Seg_249" n="HIAT:ip">.</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T81" id="Seg_251" n="sc" s="T80">
               <ts e="T81" id="Seg_253" n="HIAT:u" s="T80">
                  <nts id="Seg_254" n="HIAT:ip">(</nts>
                  <nts id="Seg_255" n="HIAT:ip">(</nts>
                  <ats e="T81" id="Seg_256" n="HIAT:non-pho" s="T80">BRK</ats>
                  <nts id="Seg_257" n="HIAT:ip">)</nts>
                  <nts id="Seg_258" n="HIAT:ip">)</nts>
                  <nts id="Seg_259" n="HIAT:ip">.</nts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T83" id="Seg_261" n="sc" s="T82">
               <ts e="T83" id="Seg_263" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_265" n="HIAT:w" s="T82">Можно</ts>
                  <nts id="Seg_266" n="HIAT:ip">?</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T88" id="Seg_268" n="sc" s="T84">
               <ts e="T88" id="Seg_270" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_272" n="HIAT:w" s="T84">Kanžəbəj</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_275" n="HIAT:w" s="T85">măna</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_278" n="HIAT:w" s="T86">ara</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_281" n="HIAT:w" s="T87">bĭʔsittə</ts>
                  <nts id="Seg_282" n="HIAT:ip">.</nts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T92" id="Seg_284" n="sc" s="T89">
               <ts e="T92" id="Seg_286" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_288" n="HIAT:w" s="T89">Dʼok</ts>
                  <nts id="Seg_289" n="HIAT:ip">,</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_292" n="HIAT:w" s="T90">ej</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_295" n="HIAT:w" s="T91">allam</ts>
                  <nts id="Seg_296" n="HIAT:ip">.</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T94" id="Seg_298" n="sc" s="T93">
               <ts e="T94" id="Seg_300" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_302" n="HIAT:w" s="T93">Moʔ</ts>
                  <nts id="Seg_303" n="HIAT:ip">?</nts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T98" id="Seg_305" n="sc" s="T95">
               <ts e="T98" id="Seg_307" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_309" n="HIAT:w" s="T95">Kanžəbəj</ts>
                  <nts id="Seg_310" n="HIAT:ip">,</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_313" n="HIAT:w" s="T96">ara</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_316" n="HIAT:w" s="T97">kuvas</ts>
                  <nts id="Seg_317" n="HIAT:ip">.</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T102" id="Seg_319" n="sc" s="T99">
               <ts e="T102" id="Seg_321" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_323" n="HIAT:w" s="T99">Kuvas</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_325" n="HIAT:ip">(</nts>
                  <ts e="T101" id="Seg_327" n="HIAT:w" s="T100">bĭs-</ts>
                  <nts id="Seg_328" n="HIAT:ip">)</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_331" n="HIAT:w" s="T101">bĭsittə</ts>
                  <nts id="Seg_332" n="HIAT:ip">.</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T108" id="Seg_334" n="sc" s="T103">
               <ts e="T108" id="Seg_336" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_338" n="HIAT:w" s="T103">Dʼok</ts>
                  <nts id="Seg_339" n="HIAT:ip">,</nts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_342" n="HIAT:w" s="T104">măna</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_344" n="HIAT:ip">(</nts>
                  <ts e="T106" id="Seg_346" n="HIAT:w" s="T105">dĭ-</ts>
                  <nts id="Seg_347" n="HIAT:ip">)</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_350" n="HIAT:w" s="T106">dĭgən</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_353" n="HIAT:w" s="T107">münörlit</ts>
                  <nts id="Seg_354" n="HIAT:ip">.</nts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T115" id="Seg_356" n="sc" s="T109">
               <ts e="T115" id="Seg_358" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_360" n="HIAT:w" s="T109">Dʼok</ts>
                  <nts id="Seg_361" n="HIAT:ip">,</nts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_364" n="HIAT:w" s="T110">măn</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_367" n="HIAT:w" s="T111">ej</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_370" n="HIAT:w" s="T112">mĭləm</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_373" n="HIAT:w" s="T113">tănan</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_376" n="HIAT:w" s="T114">münörzittə</ts>
                  <nts id="Seg_377" n="HIAT:ip">.</nts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T118" id="Seg_379" n="sc" s="T116">
               <ts e="T118" id="Seg_381" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_383" n="HIAT:w" s="T116">Nu</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_386" n="HIAT:w" s="T117">alləm</ts>
                  <nts id="Seg_387" n="HIAT:ip">.</nts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T122" id="Seg_389" n="sc" s="T119">
               <ts e="T120" id="Seg_391" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_393" n="HIAT:w" s="T119">Alləm</ts>
                  <nts id="Seg_394" n="HIAT:ip">.</nts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_397" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_399" n="HIAT:w" s="T120">Ej</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_402" n="HIAT:w" s="T121">mĭləl</ts>
                  <nts id="Seg_403" n="HIAT:ip">?</nts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T126" id="Seg_405" n="sc" s="T123">
               <ts e="T126" id="Seg_407" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_409" n="HIAT:w" s="T123">Dʼok</ts>
                  <nts id="Seg_410" n="HIAT:ip">,</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_413" n="HIAT:w" s="T124">ej</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_416" n="HIAT:w" s="T125">mĭləm</ts>
                  <nts id="Seg_417" n="HIAT:ip">.</nts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T129" id="Seg_419" n="sc" s="T127">
               <ts e="T129" id="Seg_421" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_423" n="HIAT:w" s="T127">Nu</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_426" n="HIAT:w" s="T128">jakše</ts>
                  <nts id="Seg_427" n="HIAT:ip">.</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T136" id="Seg_429" n="sc" s="T130">
               <ts e="T136" id="Seg_431" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_433" n="HIAT:w" s="T130">Jakše</ts>
                  <nts id="Seg_434" n="HIAT:ip">,</nts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_437" n="HIAT:w" s="T131">ej</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_440" n="HIAT:w" s="T132">mĭləl</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_443" n="HIAT:w" s="T133">dak</ts>
                  <nts id="Seg_444" n="HIAT:ip">,</nts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_447" n="HIAT:w" s="T134">măn</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_450" n="HIAT:w" s="T135">alləm</ts>
                  <nts id="Seg_451" n="HIAT:ip">.</nts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T138" id="Seg_453" n="sc" s="T137">
               <ts e="T138" id="Seg_455" n="HIAT:u" s="T137">
                  <nts id="Seg_456" n="HIAT:ip">(</nts>
                  <nts id="Seg_457" n="HIAT:ip">(</nts>
                  <ats e="T138" id="Seg_458" n="HIAT:non-pho" s="T137">BRK</ats>
                  <nts id="Seg_459" n="HIAT:ip">)</nts>
                  <nts id="Seg_460" n="HIAT:ip">)</nts>
                  <nts id="Seg_461" n="HIAT:ip">.</nts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T142" id="Seg_463" n="sc" s="T141">
               <ts e="T142" id="Seg_465" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_467" n="HIAT:w" s="T141">Сейчас</ts>
                  <nts id="Seg_468" n="HIAT:ip">.</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T147" id="Seg_470" n="sc" s="T143">
               <ts e="T147" id="Seg_472" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_474" n="HIAT:w" s="T143">Măn</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_477" n="HIAT:w" s="T144">iamzi</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_480" n="HIAT:w" s="T145">ej</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_483" n="HIAT:w" s="T146">molam</ts>
                  <nts id="Seg_484" n="HIAT:ip">.</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T151" id="Seg_486" n="sc" s="T148">
               <ts e="T151" id="Seg_488" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_490" n="HIAT:w" s="T148">Măn</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_493" n="HIAT:w" s="T149">ari</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_496" n="HIAT:w" s="T150">dʼürləm</ts>
                  <nts id="Seg_497" n="HIAT:ip">.</nts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T153" id="Seg_499" n="sc" s="T152">
               <ts e="T153" id="Seg_501" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_503" n="HIAT:w" s="T152">Moʔ</ts>
                  <nts id="Seg_504" n="HIAT:ip">?</nts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T158" id="Seg_506" n="sc" s="T154">
               <ts e="T158" id="Seg_508" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_510" n="HIAT:w" s="T154">Da</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_513" n="HIAT:w" s="T155">dĭ</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_516" n="HIAT:w" s="T156">kudonzla</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_519" n="HIAT:w" s="T157">tăŋ</ts>
                  <nts id="Seg_520" n="HIAT:ip">.</nts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T162" id="Seg_522" n="sc" s="T159">
               <ts e="T162" id="Seg_524" n="HIAT:u" s="T159">
                  <ts e="T160" id="Seg_526" n="HIAT:w" s="T159">Nu</ts>
                  <nts id="Seg_527" n="HIAT:ip">,</nts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_530" n="HIAT:w" s="T160">amnoʔ</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_533" n="HIAT:w" s="T161">iazil</ts>
                  <nts id="Seg_534" n="HIAT:ip">.</nts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T164" id="Seg_536" n="sc" s="T163">
               <ts e="T164" id="Seg_538" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_540" n="HIAT:w" s="T163">Amnoʔ</ts>
                  <nts id="Seg_541" n="HIAT:ip">.</nts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T168" id="Seg_543" n="sc" s="T165">
               <ts e="T168" id="Seg_545" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_547" n="HIAT:w" s="T165">Moʔ</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_550" n="HIAT:w" s="T166">dere</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_553" n="HIAT:w" s="T167">molal</ts>
                  <nts id="Seg_554" n="HIAT:ip">?</nts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T172" id="Seg_556" n="sc" s="T169">
               <ts e="T172" id="Seg_558" n="HIAT:u" s="T169">
                  <ts e="T170" id="Seg_560" n="HIAT:w" s="T169">Tăn</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_563" n="HIAT:w" s="T170">bila</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_566" n="HIAT:w" s="T171">kuza</ts>
                  <nts id="Seg_567" n="HIAT:ip">.</nts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T174" id="Seg_569" n="sc" s="T173">
               <ts e="T174" id="Seg_571" n="HIAT:u" s="T173">
                  <ts e="T174" id="Seg_573" n="HIAT:w" s="T173">Вот</ts>
                  <nts id="Seg_574" n="HIAT:ip">.</nts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T176" id="Seg_576" n="sc" s="T175">
               <ts e="T176" id="Seg_578" n="HIAT:u" s="T175">
                  <nts id="Seg_579" n="HIAT:ip">(</nts>
                  <nts id="Seg_580" n="HIAT:ip">(</nts>
                  <ats e="T176" id="Seg_581" n="HIAT:non-pho" s="T175">BRK</ats>
                  <nts id="Seg_582" n="HIAT:ip">)</nts>
                  <nts id="Seg_583" n="HIAT:ip">)</nts>
                  <nts id="Seg_584" n="HIAT:ip">.</nts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T183" id="Seg_586" n="sc" s="T177">
               <ts e="T183" id="Seg_588" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_590" n="HIAT:w" s="T177">Tăn</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_593" n="HIAT:w" s="T178">abal</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_596" n="HIAT:w" s="T179">iam</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_599" n="HIAT:w" s="T180">šen</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_602" n="HIAT:w" s="T181">bila</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_605" n="HIAT:w" s="T182">kuza</ts>
                  <nts id="Seg_606" n="HIAT:ip">.</nts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T187" id="Seg_608" n="sc" s="T184">
               <ts e="T187" id="Seg_610" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_612" n="HIAT:w" s="T184">A</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_615" n="HIAT:w" s="T185">măn</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_618" n="HIAT:w" s="T186">jakše</ts>
                  <nts id="Seg_619" n="HIAT:ip">.</nts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T193" id="Seg_621" n="sc" s="T188">
               <ts e="T191" id="Seg_623" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_625" n="HIAT:w" s="T188">Dʼok</ts>
                  <nts id="Seg_626" n="HIAT:ip">,</nts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_629" n="HIAT:w" s="T189">măn</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_632" n="HIAT:w" s="T190">jakše</ts>
                  <nts id="Seg_633" n="HIAT:ip">.</nts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_636" n="HIAT:u" s="T191">
                  <ts e="T192" id="Seg_638" n="HIAT:w" s="T191">Tăn</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_641" n="HIAT:w" s="T192">bila</ts>
                  <nts id="Seg_642" n="HIAT:ip">.</nts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T199" id="Seg_644" n="sc" s="T194">
               <ts e="T199" id="Seg_646" n="HIAT:u" s="T194">
                  <ts e="T195" id="Seg_648" n="HIAT:w" s="T194">Oh</ts>
                  <nts id="Seg_649" n="HIAT:ip">,</nts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_652" n="HIAT:w" s="T195">mo</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_655" n="HIAT:w" s="T196">dereʔ</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_657" n="HIAT:ip">(</nts>
                  <ts e="T198" id="Seg_659" n="HIAT:w" s="T197">m-</ts>
                  <nts id="Seg_660" n="HIAT:ip">)</nts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_663" n="HIAT:w" s="T198">măllal</ts>
                  <nts id="Seg_664" n="HIAT:ip">?</nts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T201" id="Seg_666" n="sc" s="T200">
               <ts e="T201" id="Seg_668" n="HIAT:u" s="T200">
                  <ts e="T201" id="Seg_670" n="HIAT:w" s="T200">Eneidənə</ts>
                  <nts id="Seg_671" n="HIAT:ip">.</nts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T204" id="Seg_673" n="sc" s="T202">
               <ts e="T204" id="Seg_675" n="HIAT:u" s="T202">
                  <ts e="T203" id="Seg_677" n="HIAT:w" s="T202">Mănuʔ</ts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_680" n="HIAT:w" s="T203">jakše</ts>
                  <nts id="Seg_681" n="HIAT:ip">.</nts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T209" id="Seg_683" n="sc" s="T205">
               <ts e="T209" id="Seg_685" n="HIAT:u" s="T205">
                  <ts e="T206" id="Seg_687" n="HIAT:w" s="T205">Oh</ts>
                  <nts id="Seg_688" n="HIAT:ip">,</nts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_691" n="HIAT:w" s="T206">tăŋ</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_694" n="HIAT:w" s="T207">bila</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_697" n="HIAT:w" s="T208">kuza</ts>
                  <nts id="Seg_698" n="HIAT:ip">.</nts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T211" id="Seg_700" n="sc" s="T210">
               <ts e="T211" id="Seg_702" n="HIAT:u" s="T210">
                  <ts e="T211" id="Seg_704" n="HIAT:w" s="T210">Eneidənə</ts>
                  <nts id="Seg_705" n="HIAT:ip">.</nts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T213" id="Seg_707" n="sc" s="T212">
               <ts e="T213" id="Seg_709" n="HIAT:u" s="T212">
                  <nts id="Seg_710" n="HIAT:ip">(</nts>
                  <nts id="Seg_711" n="HIAT:ip">(</nts>
                  <ats e="T213" id="Seg_712" n="HIAT:non-pho" s="T212">BRK</ats>
                  <nts id="Seg_713" n="HIAT:ip">)</nts>
                  <nts id="Seg_714" n="HIAT:ip">)</nts>
                  <nts id="Seg_715" n="HIAT:ip">.</nts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T220" id="Seg_717" n="sc" s="T214">
               <ts e="T220" id="Seg_719" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_721" n="HIAT:w" s="T214">Măn</ts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_723" n="HIAT:ip">(</nts>
                  <ts e="T216" id="Seg_725" n="HIAT:w" s="T215">iam</ts>
                  <nts id="Seg_726" n="HIAT:ip">)</nts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_728" n="HIAT:ip">(</nts>
                  <ts e="T217" id="Seg_730" n="HIAT:w" s="T216">sumna=</ts>
                  <nts id="Seg_731" n="HIAT:ip">)</nts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_734" n="HIAT:w" s="T217">sumna</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_737" n="HIAT:w" s="T218">nʼi</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_740" n="HIAT:w" s="T219">dĭn</ts>
                  <nts id="Seg_741" n="HIAT:ip">.</nts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T222" id="Seg_743" n="sc" s="T221">
               <ts e="T222" id="Seg_745" n="HIAT:u" s="T221">
                  <ts e="T222" id="Seg_747" n="HIAT:w" s="T221">Ой</ts>
                  <nts id="Seg_748" n="HIAT:ip">…</nts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T224" id="Seg_750" n="sc" s="T223">
               <ts e="T224" id="Seg_752" n="HIAT:u" s="T223">
                  <nts id="Seg_753" n="HIAT:ip">(</nts>
                  <nts id="Seg_754" n="HIAT:ip">(</nts>
                  <ats e="T224" id="Seg_755" n="HIAT:non-pho" s="T223">BRK</ats>
                  <nts id="Seg_756" n="HIAT:ip">)</nts>
                  <nts id="Seg_757" n="HIAT:ip">)</nts>
                  <nts id="Seg_758" n="HIAT:ip">.</nts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T231" id="Seg_760" n="sc" s="T225">
               <ts e="T231" id="Seg_762" n="HIAT:u" s="T225">
                  <ts e="T226" id="Seg_764" n="HIAT:w" s="T225">Dĭn</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_767" n="HIAT:w" s="T226">mobi</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_770" n="HIAT:w" s="T227">nagur</ts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_773" n="HIAT:w" s="T228">koʔbdo</ts>
                  <nts id="Seg_774" n="HIAT:ip">,</nts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_777" n="HIAT:w" s="T229">šide</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_780" n="HIAT:w" s="T230">nʼi</ts>
                  <nts id="Seg_781" n="HIAT:ip">.</nts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T235" id="Seg_783" n="sc" s="T232">
               <ts e="T235" id="Seg_785" n="HIAT:u" s="T232">
                  <ts e="T233" id="Seg_787" n="HIAT:w" s="T232">Oʔb</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_790" n="HIAT:w" s="T233">nʼi</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_793" n="HIAT:w" s="T234">külambi</ts>
                  <nts id="Seg_794" n="HIAT:ip">.</nts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T238" id="Seg_796" n="sc" s="T236">
               <ts e="T238" id="Seg_798" n="HIAT:u" s="T236">
                  <ts e="T237" id="Seg_800" n="HIAT:w" s="T236">I</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_803" n="HIAT:w" s="T237">koʔbdo</ts>
                  <nts id="Seg_804" n="HIAT:ip">…</nts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T244" id="Seg_806" n="sc" s="T239">
               <ts e="T244" id="Seg_808" n="HIAT:u" s="T239">
                  <ts e="T240" id="Seg_810" n="HIAT:w" s="T239">Вот</ts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_813" n="HIAT:w" s="T240">память</ts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_816" n="HIAT:w" s="T241">вишь</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_819" n="HIAT:w" s="T242">какая</ts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_822" n="HIAT:w" s="T243">дурная</ts>
                  <nts id="Seg_823" n="HIAT:ip">.</nts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T248" id="Seg_825" n="sc" s="T245">
               <ts e="T248" id="Seg_827" n="HIAT:u" s="T245">
                  <ts e="T246" id="Seg_829" n="HIAT:w" s="T245">Не</ts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_831" n="HIAT:ip">(</nts>
                  <ts e="T247" id="Seg_833" n="HIAT:w" s="T246">гово-</ts>
                  <nts id="Seg_834" n="HIAT:ip">)</nts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_836" n="HIAT:ip">(</nts>
                  <nts id="Seg_837" n="HIAT:ip">(</nts>
                  <ats e="T248" id="Seg_838" n="HIAT:non-pho" s="T247">BRK</ats>
                  <nts id="Seg_839" n="HIAT:ip">)</nts>
                  <nts id="Seg_840" n="HIAT:ip">)</nts>
                  <nts id="Seg_841" n="HIAT:ip">.</nts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T252" id="Seg_843" n="sc" s="T249">
               <ts e="T252" id="Seg_845" n="HIAT:u" s="T249">
                  <ts e="T250" id="Seg_847" n="HIAT:w" s="T249">Šide</ts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_850" n="HIAT:w" s="T250">koʔbdo</ts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_853" n="HIAT:w" s="T251">külambi</ts>
                  <nts id="Seg_854" n="HIAT:ip">.</nts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T256" id="Seg_856" n="sc" s="T253">
               <ts e="T256" id="Seg_858" n="HIAT:u" s="T253">
                  <ts e="T254" id="Seg_860" n="HIAT:w" s="T253">A</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_863" n="HIAT:w" s="T254">oʔb</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_866" n="HIAT:w" s="T255">dʼok</ts>
                  <nts id="Seg_867" n="HIAT:ip">.</nts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T262" id="Seg_869" n="sc" s="T257">
               <ts e="T262" id="Seg_871" n="HIAT:u" s="T257">
                  <ts e="T258" id="Seg_873" n="HIAT:w" s="T257">Tei</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_876" n="HIAT:w" s="T258">oʔb</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_879" n="HIAT:w" s="T259">nʼi</ts>
                  <nts id="Seg_880" n="HIAT:ip">,</nts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_883" n="HIAT:w" s="T260">oʔb</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_886" n="HIAT:w" s="T261">koʔbdo</ts>
                  <nts id="Seg_887" n="HIAT:ip">.</nts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T266" id="Seg_889" n="sc" s="T263">
               <ts e="T266" id="Seg_891" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_893" n="HIAT:w" s="T263">O</ts>
                  <nts id="Seg_894" n="HIAT:ip">,</nts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_897" n="HIAT:w" s="T264">iam</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_900" n="HIAT:w" s="T265">tăŋ</ts>
                  <nts id="Seg_901" n="HIAT:ip">…</nts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T272" id="Seg_903" n="sc" s="T267">
               <ts e="T272" id="Seg_905" n="HIAT:u" s="T267">
                  <nts id="Seg_906" n="HIAT:ip">"</nts>
                  <ts e="T268" id="Seg_908" n="HIAT:w" s="T267">Плакала</ts>
                  <nts id="Seg_909" n="HIAT:ip">"</nts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_912" n="HIAT:w" s="T268">не</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_915" n="HIAT:w" s="T269">могу</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_918" n="HIAT:w" s="T270">выговорить</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_921" n="HIAT:w" s="T271">никак</ts>
                  <nts id="Seg_922" n="HIAT:ip">.</nts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T277" id="Seg_924" n="sc" s="T273">
               <ts e="T277" id="Seg_926" n="HIAT:u" s="T273">
                  <ts e="T274" id="Seg_928" n="HIAT:w" s="T273">Что</ts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_931" n="HIAT:w" s="T274">об</ts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_934" n="HIAT:w" s="T275">дитях</ts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_937" n="HIAT:w" s="T276">плакала</ts>
                  <nts id="Seg_938" n="HIAT:ip">.</nts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T279" id="Seg_940" n="sc" s="T278">
               <ts e="T279" id="Seg_942" n="HIAT:u" s="T278">
                  <nts id="Seg_943" n="HIAT:ip">(</nts>
                  <nts id="Seg_944" n="HIAT:ip">(</nts>
                  <ats e="T279" id="Seg_945" n="HIAT:non-pho" s="T278">BRK</ats>
                  <nts id="Seg_946" n="HIAT:ip">)</nts>
                  <nts id="Seg_947" n="HIAT:ip">)</nts>
                  <nts id="Seg_948" n="HIAT:ip">.</nts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T284" id="Seg_950" n="sc" s="T280">
               <ts e="T284" id="Seg_952" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_954" n="HIAT:w" s="T280">Amnoʔ</ts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_957" n="HIAT:w" s="T281">sarɨ</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_960" n="HIAT:w" s="T282">su</ts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_963" n="HIAT:w" s="T283">amzittə</ts>
                  <nts id="Seg_964" n="HIAT:ip">.</nts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T289" id="Seg_966" n="sc" s="T285">
               <ts e="T289" id="Seg_968" n="HIAT:u" s="T285">
                  <ts e="T286" id="Seg_970" n="HIAT:w" s="T285">Nu</ts>
                  <nts id="Seg_971" n="HIAT:ip">,</nts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_974" n="HIAT:w" s="T286">măn</ts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_977" n="HIAT:w" s="T287">только-только</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_980" n="HIAT:w" s="T288">amorbiam</ts>
                  <nts id="Seg_981" n="HIAT:ip">.</nts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T292" id="Seg_983" n="sc" s="T290">
               <ts e="T292" id="Seg_985" n="HIAT:u" s="T290">
                  <ts e="T291" id="Seg_987" n="HIAT:w" s="T290">Nu</ts>
                  <nts id="Seg_988" n="HIAT:ip">,</nts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_991" n="HIAT:w" s="T291">uja</ts>
                  <nts id="Seg_992" n="HIAT:ip">.</nts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T295" id="Seg_994" n="sc" s="T293">
               <ts e="T295" id="Seg_996" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_998" n="HIAT:w" s="T293">Uja</ts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1001" n="HIAT:w" s="T294">jakše</ts>
                  <nts id="Seg_1002" n="HIAT:ip">.</nts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T298" id="Seg_1004" n="sc" s="T296">
               <ts e="T298" id="Seg_1006" n="HIAT:u" s="T296">
                  <ts e="T297" id="Seg_1008" n="HIAT:w" s="T296">Uja</ts>
                  <nts id="Seg_1009" n="HIAT:ip">,</nts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1012" n="HIAT:w" s="T297">ipek</ts>
                  <nts id="Seg_1013" n="HIAT:ip">.</nts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T300" id="Seg_1015" n="sc" s="T299">
               <ts e="T300" id="Seg_1017" n="HIAT:u" s="T299">
                  <ts e="T300" id="Seg_1019" n="HIAT:w" s="T299">Amzittə</ts>
                  <nts id="Seg_1020" n="HIAT:ip">.</nts>
                  <nts id="Seg_1021" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T305" id="Seg_1022" n="sc" s="T301">
               <ts e="T305" id="Seg_1024" n="HIAT:u" s="T301">
                  <ts e="T302" id="Seg_1026" n="HIAT:w" s="T301">Nu</ts>
                  <nts id="Seg_1027" n="HIAT:ip">,</nts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1030" n="HIAT:w" s="T302">măn</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1033" n="HIAT:w" s="T303">tolʼkă</ts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1036" n="HIAT:w" s="T304">amorbiam</ts>
                  <nts id="Seg_1037" n="HIAT:ip">.</nts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T309" id="Seg_1039" n="sc" s="T306">
               <ts e="T309" id="Seg_1041" n="HIAT:u" s="T306">
                  <ts e="T307" id="Seg_1043" n="HIAT:w" s="T306">Ну</ts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1046" n="HIAT:w" s="T307">дак</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1049" n="HIAT:w" s="T308">что</ts>
                  <nts id="Seg_1050" n="HIAT:ip">.</nts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T313" id="Seg_1052" n="sc" s="T310">
               <ts e="T313" id="Seg_1054" n="HIAT:u" s="T310">
                  <ts e="T311" id="Seg_1056" n="HIAT:w" s="T310">Amorbiam</ts>
                  <nts id="Seg_1057" n="HIAT:ip">,</nts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1060" n="HIAT:w" s="T311">amorbəʔ</ts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1063" n="HIAT:w" s="T312">mănziʔ</ts>
                  <nts id="Seg_1064" n="HIAT:ip">.</nts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T316" id="Seg_1066" n="sc" s="T314">
               <ts e="T316" id="Seg_1068" n="HIAT:u" s="T314">
                  <ts e="T315" id="Seg_1070" n="HIAT:w" s="T314">Dö</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1073" n="HIAT:w" s="T315">ipek</ts>
                  <nts id="Seg_1074" n="HIAT:ip">.</nts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T319" id="Seg_1076" n="sc" s="T317">
               <ts e="T319" id="Seg_1078" n="HIAT:u" s="T317">
                  <ts e="T318" id="Seg_1080" n="HIAT:w" s="T317">Dö</ts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1083" n="HIAT:w" s="T318">uja</ts>
                  <nts id="Seg_1084" n="HIAT:ip">.</nts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T323" id="Seg_1086" n="sc" s="T320">
               <ts e="T323" id="Seg_1088" n="HIAT:u" s="T320">
                  <nts id="Seg_1089" n="HIAT:ip">(</nts>
                  <ts e="T321" id="Seg_1091" n="HIAT:w" s="T320">Am-</ts>
                  <nts id="Seg_1092" n="HIAT:ip">)</nts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1095" n="HIAT:w" s="T321">Amorzittə</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1098" n="HIAT:w" s="T322">možnă</ts>
                  <nts id="Seg_1099" n="HIAT:ip">.</nts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T326" id="Seg_1101" n="sc" s="T324">
               <ts e="T326" id="Seg_1103" n="HIAT:u" s="T324">
                  <ts e="T325" id="Seg_1105" n="HIAT:w" s="T324">Ну</ts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1108" n="HIAT:w" s="T325">вот</ts>
                  <nts id="Seg_1109" n="HIAT:ip">.</nts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T328" id="Seg_1111" n="sc" s="T327">
               <ts e="T328" id="Seg_1113" n="HIAT:u" s="T327">
                  <nts id="Seg_1114" n="HIAT:ip">(</nts>
                  <nts id="Seg_1115" n="HIAT:ip">(</nts>
                  <ats e="T328" id="Seg_1116" n="HIAT:non-pho" s="T327">BRK</ats>
                  <nts id="Seg_1117" n="HIAT:ip">)</nts>
                  <nts id="Seg_1118" n="HIAT:ip">)</nts>
                  <nts id="Seg_1119" n="HIAT:ip">.</nts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T332" id="Seg_1121" n="sc" s="T329">
               <ts e="T332" id="Seg_1123" n="HIAT:u" s="T329">
                  <ts e="T330" id="Seg_1125" n="HIAT:w" s="T329">Šoʔ</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1128" n="HIAT:w" s="T330">döbər</ts>
                  <nts id="Seg_1129" n="HIAT:ip">,</nts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1132" n="HIAT:w" s="T331">măna</ts>
                  <nts id="Seg_1133" n="HIAT:ip">.</nts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T335" id="Seg_1135" n="sc" s="T333">
               <ts e="T335" id="Seg_1137" n="HIAT:u" s="T333">
                  <ts e="T334" id="Seg_1139" n="HIAT:w" s="T333">A</ts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1142" n="HIAT:w" s="T334">ĭmbi</ts>
                  <nts id="Seg_1143" n="HIAT:ip">?</nts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T338" id="Seg_1145" n="sc" s="T336">
               <ts e="T338" id="Seg_1147" n="HIAT:u" s="T336">
                  <ts e="T337" id="Seg_1149" n="HIAT:w" s="T336">Ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1152" n="HIAT:w" s="T337">nörbəlal</ts>
                  <nts id="Seg_1153" n="HIAT:ip">.</nts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T344" id="Seg_1155" n="sc" s="T339">
               <ts e="T344" id="Seg_1157" n="HIAT:u" s="T339">
                  <ts e="T340" id="Seg_1159" n="HIAT:w" s="T339">Dʼok</ts>
                  <nts id="Seg_1160" n="HIAT:ip">,</nts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1162" n="HIAT:ip">(</nts>
                  <ts e="T341" id="Seg_1164" n="HIAT:w" s="T340">da</ts>
                  <nts id="Seg_1165" n="HIAT:ip">)</nts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1168" n="HIAT:w" s="T341">tăn</ts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1171" n="HIAT:w" s="T342">nörbaʔ</ts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1174" n="HIAT:w" s="T343">măna</ts>
                  <nts id="Seg_1175" n="HIAT:ip">.</nts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T350" id="Seg_1177" n="sc" s="T345">
               <ts e="T350" id="Seg_1179" n="HIAT:u" s="T345">
                  <ts e="T346" id="Seg_1181" n="HIAT:w" s="T345">A</ts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1184" n="HIAT:w" s="T346">măn</ts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1187" n="HIAT:w" s="T347">ĭmbidə</ts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1190" n="HIAT:w" s="T348">ej</ts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1193" n="HIAT:w" s="T349">tĭmnem</ts>
                  <nts id="Seg_1194" n="HIAT:ip">.</nts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T355" id="Seg_1196" n="sc" s="T351">
               <ts e="T355" id="Seg_1198" n="HIAT:u" s="T351">
                  <ts e="T352" id="Seg_1200" n="HIAT:w" s="T351">Moʔ</ts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1203" n="HIAT:w" s="T352">dere</ts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1206" n="HIAT:w" s="T353">ĭmbidə</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1209" n="HIAT:w" s="T354">ej</ts>
                  <nts id="Seg_1210" n="HIAT:ip">…</nts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T359" id="Seg_1212" n="sc" s="T356">
               <ts e="T359" id="Seg_1214" n="HIAT:u" s="T356">
                  <ts e="T357" id="Seg_1216" n="HIAT:w" s="T356">A</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1219" n="HIAT:w" s="T357">măn</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1222" n="HIAT:w" s="T358">nörbəlam</ts>
                  <nts id="Seg_1223" n="HIAT:ip">.</nts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T363" id="Seg_1225" n="sc" s="T360">
               <ts e="T363" id="Seg_1227" n="HIAT:u" s="T360">
                  <ts e="T361" id="Seg_1229" n="HIAT:w" s="T360">Tăn</ts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1232" n="HIAT:w" s="T361">jakše</ts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1235" n="HIAT:w" s="T362">kuza</ts>
                  <nts id="Seg_1236" n="HIAT:ip">.</nts>
                  <nts id="Seg_1237" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T370" id="Seg_1238" n="sc" s="T364">
               <ts e="T367" id="Seg_1240" n="HIAT:u" s="T364">
                  <ts e="T365" id="Seg_1242" n="HIAT:w" s="T364">A</ts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1245" n="HIAT:w" s="T365">măn</ts>
                  <nts id="Seg_1246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1248" n="HIAT:w" s="T366">bile</ts>
                  <nts id="Seg_1249" n="HIAT:ip">.</nts>
                  <nts id="Seg_1250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T370" id="Seg_1252" n="HIAT:u" s="T367">
                  <ts e="T368" id="Seg_1254" n="HIAT:w" s="T367">Dʼok</ts>
                  <nts id="Seg_1255" n="HIAT:ip">,</nts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1258" n="HIAT:w" s="T368">tăn</ts>
                  <nts id="Seg_1259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1261" n="HIAT:w" s="T369">jakše</ts>
                  <nts id="Seg_1262" n="HIAT:ip">.</nts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T374" id="Seg_1264" n="sc" s="T371">
               <ts e="T374" id="Seg_1266" n="HIAT:u" s="T371">
                  <ts e="T372" id="Seg_1268" n="HIAT:w" s="T371">Moʔ</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1271" n="HIAT:w" s="T372">dereʔ</ts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1274" n="HIAT:w" s="T373">nörbəlal</ts>
                  <nts id="Seg_1275" n="HIAT:ip">?</nts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T377" id="Seg_1277" n="sc" s="T375">
               <ts e="T377" id="Seg_1279" n="HIAT:u" s="T375">
                  <ts e="T376" id="Seg_1281" n="HIAT:w" s="T375">Bile</ts>
                  <nts id="Seg_1282" n="HIAT:ip">,</nts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1285" n="HIAT:w" s="T376">bile</ts>
                  <nts id="Seg_1286" n="HIAT:ip">.</nts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T381" id="Seg_1288" n="sc" s="T378">
               <ts e="T381" id="Seg_1290" n="HIAT:u" s="T378">
                  <ts e="T379" id="Seg_1292" n="HIAT:w" s="T378">Dere</ts>
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1295" n="HIAT:w" s="T379">ej</ts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1298" n="HIAT:w" s="T380">nörbaʔ</ts>
                  <nts id="Seg_1299" n="HIAT:ip">.</nts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T383" id="Seg_1301" n="sc" s="T382">
               <ts e="T383" id="Seg_1303" n="HIAT:u" s="T382">
                  <ts e="T383" id="Seg_1305" n="HIAT:w" s="T382">Bile</ts>
                  <nts id="Seg_1306" n="HIAT:ip">.</nts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T387" id="Seg_1308" n="sc" s="T384">
               <ts e="T387" id="Seg_1310" n="HIAT:u" s="T384">
                  <ts e="T385" id="Seg_1312" n="HIAT:w" s="T384">Nadă</ts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1315" n="HIAT:w" s="T385">nörbəzittə</ts>
                  <nts id="Seg_1316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1318" n="HIAT:w" s="T386">jakše</ts>
                  <nts id="Seg_1319" n="HIAT:ip">.</nts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T389" id="Seg_1321" n="sc" s="T388">
               <ts e="T389" id="Seg_1323" n="HIAT:u" s="T388">
                  <ts e="T389" id="Seg_1325" n="HIAT:w" s="T388">Kuza</ts>
                  <nts id="Seg_1326" n="HIAT:ip">.</nts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T396" id="Seg_1328" n="sc" s="T390">
               <ts e="T396" id="Seg_1330" n="HIAT:u" s="T390">
                  <ts e="T391" id="Seg_1332" n="HIAT:w" s="T390">A</ts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1335" n="HIAT:w" s="T391">tăn</ts>
                  <nts id="Seg_1336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1338" n="HIAT:w" s="T392">üge</ts>
                  <nts id="Seg_1339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1341" n="HIAT:w" s="T393">ĭmbidə</ts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1344" n="HIAT:w" s="T394">ej</ts>
                  <nts id="Seg_1345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1347" n="HIAT:w" s="T395">nörbəlal</ts>
                  <nts id="Seg_1348" n="HIAT:ip">.</nts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T398" id="Seg_1350" n="sc" s="T397">
               <ts e="T398" id="Seg_1352" n="HIAT:u" s="T397">
                  <ts e="T398" id="Seg_1354" n="HIAT:w" s="T397">Вот</ts>
                  <nts id="Seg_1355" n="HIAT:ip">.</nts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T400" id="Seg_1357" n="sc" s="T399">
               <ts e="T400" id="Seg_1359" n="HIAT:u" s="T399">
                  <nts id="Seg_1360" n="HIAT:ip">(</nts>
                  <nts id="Seg_1361" n="HIAT:ip">(</nts>
                  <ats e="T400" id="Seg_1362" n="HIAT:non-pho" s="T399">BRK</ats>
                  <nts id="Seg_1363" n="HIAT:ip">)</nts>
                  <nts id="Seg_1364" n="HIAT:ip">)</nts>
                  <nts id="Seg_1365" n="HIAT:ip">.</nts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T404" id="Seg_1367" n="sc" s="T401">
               <ts e="T404" id="Seg_1369" n="HIAT:u" s="T401">
                  <ts e="T402" id="Seg_1371" n="HIAT:w" s="T401">Kajdɨ</ts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1374" n="HIAT:w" s="T402">dʼestek</ts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1377" n="HIAT:w" s="T403">agɨlgazɨŋ</ts>
                  <nts id="Seg_1378" n="HIAT:ip">?</nts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T407" id="Seg_1380" n="sc" s="T405">
               <ts e="T407" id="Seg_1382" n="HIAT:u" s="T405">
                  <ts e="T406" id="Seg_1384" n="HIAT:w" s="T405">Kara</ts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1387" n="HIAT:w" s="T406">dʼestek</ts>
                  <nts id="Seg_1388" n="HIAT:ip">.</nts>
                  <nts id="Seg_1389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T411" id="Seg_1390" n="sc" s="T408">
               <ts e="T411" id="Seg_1392" n="HIAT:u" s="T408">
                  <ts e="T409" id="Seg_1394" n="HIAT:w" s="T408">A</ts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1397" n="HIAT:w" s="T409">kɨzɨ</ts>
                  <nts id="Seg_1398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1400" n="HIAT:w" s="T410">dʼestek</ts>
                  <nts id="Seg_1401" n="HIAT:ip">?</nts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T415" id="Seg_1403" n="sc" s="T412">
               <ts e="T415" id="Seg_1405" n="HIAT:u" s="T412">
                  <ts e="T413" id="Seg_1407" n="HIAT:w" s="T412">Agɨlgam</ts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1410" n="HIAT:w" s="T413">kɨzɨ</ts>
                  <nts id="Seg_1411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1413" n="HIAT:w" s="T414">dʼestek</ts>
                  <nts id="Seg_1414" n="HIAT:ip">.</nts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T417" id="Seg_1416" n="sc" s="T416">
               <ts e="T417" id="Seg_1418" n="HIAT:u" s="T416">
                  <ts e="T417" id="Seg_1420" n="HIAT:w" s="T416">Agɨlgam</ts>
                  <nts id="Seg_1421" n="HIAT:ip">.</nts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T421" id="Seg_1423" n="sc" s="T418">
               <ts e="T421" id="Seg_1425" n="HIAT:u" s="T418">
                  <ts e="T419" id="Seg_1427" n="HIAT:w" s="T418">Nada</ts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1430" n="HIAT:w" s="T419">ipek</ts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1433" n="HIAT:w" s="T420">vɨžɨrarga</ts>
                  <nts id="Seg_1434" n="HIAT:ip">.</nts>
                  <nts id="Seg_1435" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T424" id="Seg_1436" n="sc" s="T422">
               <ts e="T424" id="Seg_1438" n="HIAT:u" s="T422">
                  <ts e="T423" id="Seg_1440" n="HIAT:w" s="T422">Kajdɨ</ts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1443" n="HIAT:w" s="T423">ipek</ts>
                  <nts id="Seg_1444" n="HIAT:ip">?</nts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T428" id="Seg_1446" n="sc" s="T425">
               <ts e="T428" id="Seg_1448" n="HIAT:u" s="T425">
                  <ts e="T426" id="Seg_1450" n="HIAT:w" s="T425">Da</ts>
                  <nts id="Seg_1451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1453" n="HIAT:w" s="T426">kara</ts>
                  <nts id="Seg_1454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1456" n="HIAT:w" s="T427">ipek</ts>
                  <nts id="Seg_1457" n="HIAT:ip">.</nts>
                  <nts id="Seg_1458" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T433" id="Seg_1459" n="sc" s="T429">
               <ts e="T433" id="Seg_1461" n="HIAT:u" s="T429">
                  <ts e="T430" id="Seg_1463" n="HIAT:w" s="T429">Dʼok</ts>
                  <nts id="Seg_1464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1466" n="HIAT:w" s="T430">kara</ts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1469" n="HIAT:w" s="T431">ipek</ts>
                  <nts id="Seg_1470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1472" n="HIAT:w" s="T432">dʼabal</ts>
                  <nts id="Seg_1473" n="HIAT:ip">.</nts>
                  <nts id="Seg_1474" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T438" id="Seg_1475" n="sc" s="T434">
               <ts e="T438" id="Seg_1477" n="HIAT:u" s="T434">
                  <ts e="T435" id="Seg_1479" n="HIAT:w" s="T434">Nada</ts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1482" n="HIAT:w" s="T435">jakše</ts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1485" n="HIAT:w" s="T436">ipek</ts>
                  <nts id="Seg_1486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1488" n="HIAT:w" s="T437">bɨžɨrarga</ts>
                  <nts id="Seg_1489" n="HIAT:ip">.</nts>
                  <nts id="Seg_1490" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T443" id="Seg_1491" n="sc" s="T439">
               <ts e="T443" id="Seg_1493" n="HIAT:u" s="T439">
                  <nts id="Seg_1494" n="HIAT:ip">(</nts>
                  <ts e="T440" id="Seg_1496" n="HIAT:w" s="T439">Turgus</ts>
                  <nts id="Seg_1497" n="HIAT:ip">)</nts>
                  <nts id="Seg_1498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1500" n="HIAT:w" s="T440">sarɨ</ts>
                  <nts id="Seg_1501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1503" n="HIAT:w" s="T441">su</ts>
                  <nts id="Seg_1504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1506" n="HIAT:w" s="T442">ičerbɨs</ts>
                  <nts id="Seg_1507" n="HIAT:ip">.</nts>
                  <nts id="Seg_1508" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T446" id="Seg_1509" n="sc" s="T444">
               <ts e="T446" id="Seg_1511" n="HIAT:u" s="T444">
                  <ts e="T445" id="Seg_1513" n="HIAT:w" s="T444">Măn</ts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1516" n="HIAT:w" s="T445">ičerbɨm</ts>
                  <nts id="Seg_1517" n="HIAT:ip">.</nts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T448" id="Seg_1519" n="sc" s="T447">
               <ts e="T448" id="Seg_1521" n="HIAT:u" s="T447">
                  <ts e="T448" id="Seg_1523" n="HIAT:w" s="T447">Вот</ts>
                  <nts id="Seg_1524" n="HIAT:ip">.</nts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T450" id="Seg_1526" n="sc" s="T449">
               <ts e="T450" id="Seg_1528" n="HIAT:u" s="T449">
                  <nts id="Seg_1529" n="HIAT:ip">(</nts>
                  <nts id="Seg_1530" n="HIAT:ip">(</nts>
                  <ats e="T450" id="Seg_1531" n="HIAT:non-pho" s="T449">BRK</ats>
                  <nts id="Seg_1532" n="HIAT:ip">)</nts>
                  <nts id="Seg_1533" n="HIAT:ip">)</nts>
                  <nts id="Seg_1534" n="HIAT:ip">.</nts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T454" id="Seg_1536" n="sc" s="T451">
               <ts e="T454" id="Seg_1538" n="HIAT:u" s="T451">
                  <ts e="T452" id="Seg_1540" n="HIAT:w" s="T451">Măn</ts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1543" n="HIAT:w" s="T452">ulum</ts>
                  <nts id="Seg_1544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1546" n="HIAT:w" s="T453">ĭzemniet</ts>
                  <nts id="Seg_1547" n="HIAT:ip">.</nts>
                  <nts id="Seg_1548" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T459" id="Seg_1549" n="sc" s="T455">
               <ts e="T459" id="Seg_1551" n="HIAT:u" s="T455">
                  <ts e="T456" id="Seg_1553" n="HIAT:w" s="T455">Tej</ts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1556" n="HIAT:w" s="T456">măn</ts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1559" n="HIAT:w" s="T457">ulum</ts>
                  <nts id="Seg_1560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1562" n="HIAT:w" s="T458">jakše</ts>
                  <nts id="Seg_1563" n="HIAT:ip">.</nts>
                  <nts id="Seg_1564" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T461" id="Seg_1565" n="sc" s="T460">
               <ts e="T461" id="Seg_1567" n="HIAT:u" s="T460">
                  <nts id="Seg_1568" n="HIAT:ip">(</nts>
                  <nts id="Seg_1569" n="HIAT:ip">(</nts>
                  <ats e="T461" id="Seg_1570" n="HIAT:non-pho" s="T460">BRK</ats>
                  <nts id="Seg_1571" n="HIAT:ip">)</nts>
                  <nts id="Seg_1572" n="HIAT:ip">)</nts>
                  <nts id="Seg_1573" n="HIAT:ip">.</nts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T464" id="Seg_1575" n="sc" s="T462">
               <ts e="T464" id="Seg_1577" n="HIAT:u" s="T462">
                  <ts e="T463" id="Seg_1579" n="HIAT:w" s="T462">Gibər</ts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1581" n="HIAT:ip">(</nts>
                  <ts e="T464" id="Seg_1583" n="HIAT:w" s="T463">alləl</ts>
                  <nts id="Seg_1584" n="HIAT:ip">)</nts>
                  <nts id="Seg_1585" n="HIAT:ip">?</nts>
                  <nts id="Seg_1586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T466" id="Seg_1587" n="sc" s="T465">
               <ts e="T466" id="Seg_1589" n="HIAT:u" s="T465">
                  <ts e="T466" id="Seg_1591" n="HIAT:w" s="T465">Šoləm</ts>
                  <nts id="Seg_1592" n="HIAT:ip">.</nts>
                  <nts id="Seg_1593" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T469" id="Seg_1594" n="sc" s="T467">
               <ts e="T469" id="Seg_1596" n="HIAT:u" s="T467">
                  <ts e="T468" id="Seg_1598" n="HIAT:w" s="T467">Mo</ts>
                  <nts id="Seg_1599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1601" n="HIAT:w" s="T468">dereʔ</ts>
                  <nts id="Seg_1602" n="HIAT:ip">?</nts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T472" id="Seg_1604" n="sc" s="T470">
               <ts e="T472" id="Seg_1606" n="HIAT:u" s="T470">
                  <ts e="T471" id="Seg_1608" n="HIAT:w" s="T470">Dak</ts>
                  <nts id="Seg_1609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1611" n="HIAT:w" s="T471">ĭmbi</ts>
                  <nts id="Seg_1612" n="HIAT:ip">?</nts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T474" id="Seg_1614" n="sc" s="T473">
               <ts e="T474" id="Seg_1616" n="HIAT:u" s="T473">
                  <ts e="T474" id="Seg_1618" n="HIAT:w" s="T473">Šoʔ</ts>
                  <nts id="Seg_1619" n="HIAT:ip">.</nts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T476" id="Seg_1621" n="sc" s="T475">
               <ts e="T476" id="Seg_1623" n="HIAT:u" s="T475">
                  <ts e="T476" id="Seg_1625" n="HIAT:w" s="T475">Dʼok</ts>
                  <nts id="Seg_1626" n="HIAT:ip">!</nts>
                  <nts id="Seg_1627" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T480" id="Seg_1628" n="sc" s="T477">
               <ts e="T480" id="Seg_1630" n="HIAT:u" s="T477">
                  <ts e="T478" id="Seg_1632" n="HIAT:w" s="T477">Măn</ts>
                  <nts id="Seg_1633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1635" n="HIAT:w" s="T478">ari</ts>
                  <nts id="Seg_1636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1638" n="HIAT:w" s="T479">dʼürləm</ts>
                  <nts id="Seg_1639" n="HIAT:ip">.</nts>
                  <nts id="Seg_1640" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T483" id="Seg_1641" n="sc" s="T481">
               <ts e="T483" id="Seg_1643" n="HIAT:u" s="T481">
                  <ts e="T482" id="Seg_1645" n="HIAT:w" s="T481">Ari</ts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1648" n="HIAT:w" s="T482">dʼürləŋ</ts>
                  <nts id="Seg_1649" n="HIAT:ip">!</nts>
                  <nts id="Seg_1650" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T485" id="Seg_1651" n="sc" s="T484">
               <ts e="T485" id="Seg_1653" n="HIAT:u" s="T484">
                  <ts e="T485" id="Seg_1655" n="HIAT:w" s="T484">Tăn</ts>
                  <nts id="Seg_1656" n="HIAT:ip">…</nts>
                  <nts id="Seg_1657" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T487" id="Seg_1658" n="sc" s="T486">
               <ts e="T487" id="Seg_1660" n="HIAT:u" s="T486">
                  <ts e="T487" id="Seg_1662" n="HIAT:w" s="T486">A</ts>
                  <nts id="Seg_1663" n="HIAT:ip">?</nts>
                  <nts id="Seg_1664" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T493" id="Seg_1665" n="sc" s="T488">
               <ts e="T493" id="Seg_1667" n="HIAT:u" s="T488">
                  <ts e="T489" id="Seg_1669" n="HIAT:w" s="T488">Tăn</ts>
                  <nts id="Seg_1670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1672" n="HIAT:w" s="T489">bar</ts>
                  <nts id="Seg_1673" n="HIAT:ip">,</nts>
                  <nts id="Seg_1674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1675" n="HIAT:ip">(</nts>
                  <ts e="T491" id="Seg_1677" n="HIAT:w" s="T490">gĭda</ts>
                  <nts id="Seg_1678" n="HIAT:ip">)</nts>
                  <nts id="Seg_1679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1681" n="HIAT:w" s="T491">bar</ts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1684" n="HIAT:w" s="T492">moləl</ts>
                  <nts id="Seg_1685" n="HIAT:ip">.</nts>
                  <nts id="Seg_1686" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T500" id="Seg_1687" n="sc" s="T494">
               <ts e="T500" id="Seg_1689" n="HIAT:u" s="T494">
                  <ts e="T495" id="Seg_1691" n="HIAT:w" s="T494">Nu</ts>
                  <nts id="Seg_1692" n="HIAT:ip">,</nts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1695" n="HIAT:w" s="T495">a</ts>
                  <nts id="Seg_1696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1698" n="HIAT:w" s="T496">tăn</ts>
                  <nts id="Seg_1699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1701" n="HIAT:w" s="T497">dak</ts>
                  <nts id="Seg_1702" n="HIAT:ip">,</nts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1705" n="HIAT:w" s="T498">ej</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1708" n="HIAT:w" s="T499">molal</ts>
                  <nts id="Seg_1709" n="HIAT:ip">.</nts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T502" id="Seg_1711" n="sc" s="T501">
               <ts e="T502" id="Seg_1713" n="HIAT:u" s="T501">
                  <nts id="Seg_1714" n="HIAT:ip">(</nts>
                  <nts id="Seg_1715" n="HIAT:ip">(</nts>
                  <ats e="T502" id="Seg_1716" n="HIAT:non-pho" s="T501">BRK</ats>
                  <nts id="Seg_1717" n="HIAT:ip">)</nts>
                  <nts id="Seg_1718" n="HIAT:ip">)</nts>
                  <nts id="Seg_1719" n="HIAT:ip">.</nts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T507" id="Seg_1721" n="sc" s="T503">
               <ts e="T507" id="Seg_1723" n="HIAT:u" s="T503">
                  <ts e="T504" id="Seg_1725" n="HIAT:w" s="T503">Măn</ts>
                  <nts id="Seg_1726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1727" n="HIAT:ip">(</nts>
                  <ts e="T505" id="Seg_1729" n="HIAT:w" s="T504">tüžöj</ts>
                  <nts id="Seg_1730" n="HIAT:ip">)</nts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1733" n="HIAT:w" s="T505">jakše</ts>
                  <nts id="Seg_1734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1736" n="HIAT:w" s="T506">tüžöj</ts>
                  <nts id="Seg_1737" n="HIAT:ip">.</nts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T512" id="Seg_1739" n="sc" s="T508">
               <ts e="T512" id="Seg_1741" n="HIAT:u" s="T508">
                  <ts e="T509" id="Seg_1743" n="HIAT:w" s="T508">I</ts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1746" n="HIAT:w" s="T509">süt</ts>
                  <nts id="Seg_1747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1749" n="HIAT:w" s="T510">măna</ts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1752" n="HIAT:w" s="T511">mĭliet</ts>
                  <nts id="Seg_1753" n="HIAT:ip">.</nts>
                  <nts id="Seg_1754" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T516" id="Seg_1755" n="sc" s="T513">
               <ts e="T516" id="Seg_1757" n="HIAT:u" s="T513">
                  <ts e="T514" id="Seg_1759" n="HIAT:w" s="T513">Süt</ts>
                  <nts id="Seg_1760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1762" n="HIAT:w" s="T514">măna</ts>
                  <nts id="Seg_1763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1765" n="HIAT:w" s="T515">mĭliet</ts>
                  <nts id="Seg_1766" n="HIAT:ip">.</nts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T518" id="Seg_1768" n="sc" s="T517">
               <ts e="T518" id="Seg_1770" n="HIAT:u" s="T517">
                  <ts e="T518" id="Seg_1772" n="HIAT:w" s="T517">Jakše</ts>
                  <nts id="Seg_1773" n="HIAT:ip">.</nts>
                  <nts id="Seg_1774" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T522" id="Seg_1775" n="sc" s="T519">
               <ts e="T522" id="Seg_1777" n="HIAT:u" s="T519">
                  <ts e="T520" id="Seg_1779" n="HIAT:w" s="T519">Gibər</ts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1782" n="HIAT:w" s="T520">dĭ</ts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1785" n="HIAT:w" s="T521">süt</ts>
                  <nts id="Seg_1786" n="HIAT:ip">?</nts>
                  <nts id="Seg_1787" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T529" id="Seg_1788" n="sc" s="T523">
               <ts e="T529" id="Seg_1790" n="HIAT:u" s="T523">
                  <ts e="T524" id="Seg_1792" n="HIAT:w" s="T523">Da</ts>
                  <nts id="Seg_1793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1795" n="HIAT:w" s="T524">gibər</ts>
                  <nts id="Seg_1796" n="HIAT:ip">,</nts>
                  <nts id="Seg_1797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1799" n="HIAT:w" s="T525">măn</ts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1802" n="HIAT:w" s="T526">amorlam</ts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1805" n="HIAT:w" s="T527">dö</ts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1808" n="HIAT:w" s="T528">süt</ts>
                  <nts id="Seg_1809" n="HIAT:ip">.</nts>
                  <nts id="Seg_1810" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T531" id="Seg_1811" n="sc" s="T530">
               <ts e="T531" id="Seg_1813" n="HIAT:u" s="T530">
                  <ts e="T531" id="Seg_1815" n="HIAT:w" s="T530">Вот</ts>
                  <nts id="Seg_1816" n="HIAT:ip">.</nts>
                  <nts id="Seg_1817" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T533" id="Seg_1818" n="sc" s="T532">
               <ts e="T533" id="Seg_1820" n="HIAT:u" s="T532">
                  <nts id="Seg_1821" n="HIAT:ip">(</nts>
                  <nts id="Seg_1822" n="HIAT:ip">(</nts>
                  <ats e="T533" id="Seg_1823" n="HIAT:non-pho" s="T532">BRK</ats>
                  <nts id="Seg_1824" n="HIAT:ip">)</nts>
                  <nts id="Seg_1825" n="HIAT:ip">)</nts>
                  <nts id="Seg_1826" n="HIAT:ip">.</nts>
                  <nts id="Seg_1827" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T550" id="Seg_1828" n="sc" s="T534">
               <ts e="T550" id="Seg_1830" n="HIAT:u" s="T534">
                  <ts e="T535" id="Seg_1832" n="HIAT:w" s="T534">On</ts>
                  <nts id="Seg_1833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1835" n="HIAT:w" s="T535">bɨr</ts>
                  <nts id="Seg_1836" n="HIAT:ip">,</nts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1839" n="HIAT:w" s="T536">on</ts>
                  <nts id="Seg_1840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1842" n="HIAT:w" s="T537">ikɨ</ts>
                  <nts id="Seg_1843" n="HIAT:ip">,</nts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1846" n="HIAT:w" s="T538">on</ts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1849" n="HIAT:w" s="T539">uš</ts>
                  <nts id="Seg_1850" n="HIAT:ip">,</nts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1853" n="HIAT:w" s="T540">on</ts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_1856" n="HIAT:w" s="T541">tort</ts>
                  <nts id="Seg_1857" n="HIAT:ip">,</nts>
                  <nts id="Seg_1858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1860" n="HIAT:w" s="T542">on</ts>
                  <nts id="Seg_1861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_1863" n="HIAT:w" s="T543">bʼeš</ts>
                  <nts id="Seg_1864" n="HIAT:ip">,</nts>
                  <nts id="Seg_1865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_1867" n="HIAT:w" s="T544">on</ts>
                  <nts id="Seg_1868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_1870" n="HIAT:w" s="T545">altɨ</ts>
                  <nts id="Seg_1871" n="HIAT:ip">,</nts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_1874" n="HIAT:w" s="T546">on</ts>
                  <nts id="Seg_1875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1877" n="HIAT:w" s="T547">sʼegɨs</ts>
                  <nts id="Seg_1878" n="HIAT:ip">,</nts>
                  <nts id="Seg_1879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_1881" n="HIAT:w" s="T548">on</ts>
                  <nts id="Seg_1882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_1884" n="HIAT:w" s="T549">togus</ts>
                  <nts id="Seg_1885" n="HIAT:ip">.</nts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T554" id="Seg_1887" n="sc" s="T551">
               <ts e="T554" id="Seg_1889" n="HIAT:u" s="T551">
                  <ts e="T552" id="Seg_1891" n="HIAT:w" s="T551">Вот</ts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_1894" n="HIAT:w" s="T552">и</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_1897" n="HIAT:w" s="T553">все</ts>
                  <nts id="Seg_1898" n="HIAT:ip">.</nts>
                  <nts id="Seg_1899" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T558" id="Seg_1900" n="sc" s="T555">
               <ts e="T558" id="Seg_1902" n="HIAT:u" s="T555">
                  <ts e="T556" id="Seg_1904" n="HIAT:w" s="T555">Măn</ts>
                  <nts id="Seg_1905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_1907" n="HIAT:w" s="T556">ari</ts>
                  <nts id="Seg_1908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1909" n="HIAT:ip">(</nts>
                  <ts e="T558" id="Seg_1911" n="HIAT:w" s="T557">dʼürləm</ts>
                  <nts id="Seg_1912" n="HIAT:ip">)</nts>
                  <nts id="Seg_1913" n="HIAT:ip">.</nts>
                  <nts id="Seg_1914" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T560" id="Seg_1915" n="sc" s="T559">
               <ts e="T560" id="Seg_1917" n="HIAT:u" s="T559">
                  <ts e="T560" id="Seg_1919" n="HIAT:w" s="T559">Gibər</ts>
                  <nts id="Seg_1920" n="HIAT:ip">?</nts>
                  <nts id="Seg_1921" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T566" id="Seg_1922" n="sc" s="T561">
               <ts e="T566" id="Seg_1924" n="HIAT:u" s="T561">
                  <ts e="T562" id="Seg_1926" n="HIAT:w" s="T561">O</ts>
                  <nts id="Seg_1927" n="HIAT:ip">,</nts>
                  <nts id="Seg_1928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_1930" n="HIAT:w" s="T562">gibər</ts>
                  <nts id="Seg_1931" n="HIAT:ip">,</nts>
                  <nts id="Seg_1932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_1934" n="HIAT:w" s="T563">gibər</ts>
                  <nts id="Seg_1935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1936" n="HIAT:ip">(</nts>
                  <ts e="T565" id="Seg_1938" n="HIAT:w" s="T564">a-</ts>
                  <nts id="Seg_1939" n="HIAT:ip">)</nts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_1942" n="HIAT:w" s="T565">alləm</ts>
                  <nts id="Seg_1943" n="HIAT:ip">.</nts>
                  <nts id="Seg_1944" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T568" id="Seg_1945" n="sc" s="T567">
               <ts e="T568" id="Seg_1947" n="HIAT:u" s="T567">
                  <ts e="T568" id="Seg_1949" n="HIAT:w" s="T567">Ĭmbi</ts>
                  <nts id="Seg_1950" n="HIAT:ip">?</nts>
                  <nts id="Seg_1951" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T573" id="Seg_1952" n="sc" s="T569">
               <ts e="T573" id="Seg_1954" n="HIAT:u" s="T569">
                  <ts e="T570" id="Seg_1956" n="HIAT:w" s="T569">Da</ts>
                  <nts id="Seg_1957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_1959" n="HIAT:w" s="T570">măn</ts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_1962" n="HIAT:w" s="T571">ipek</ts>
                  <nts id="Seg_1963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_1965" n="HIAT:w" s="T572">naga</ts>
                  <nts id="Seg_1966" n="HIAT:ip">.</nts>
                  <nts id="Seg_1967" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T577" id="Seg_1968" n="sc" s="T574">
               <ts e="T577" id="Seg_1970" n="HIAT:u" s="T574">
                  <ts e="T575" id="Seg_1972" n="HIAT:w" s="T574">Можеть</ts>
                  <nts id="Seg_1973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_1975" n="HIAT:w" s="T575">ipek</ts>
                  <nts id="Seg_1976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_1978" n="HIAT:w" s="T576">iləm</ts>
                  <nts id="Seg_1979" n="HIAT:ip">.</nts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T584" id="Seg_1981" n="sc" s="T578">
               <ts e="T584" id="Seg_1983" n="HIAT:u" s="T578">
                  <ts e="T579" id="Seg_1985" n="HIAT:w" s="T578">A</ts>
                  <nts id="Seg_1986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1987" n="HIAT:ip">(</nts>
                  <ts e="T580" id="Seg_1989" n="HIAT:w" s="T579">dettə</ts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_1992" n="HIAT:w" s="T580">guda=</ts>
                  <nts id="Seg_1993" n="HIAT:ip">)</nts>
                  <nts id="Seg_1994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_1996" n="HIAT:w" s="T581">dettə</ts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_1999" n="HIAT:w" s="T582">gibər</ts>
                  <nts id="Seg_2000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2002" n="HIAT:w" s="T583">alləl</ts>
                  <nts id="Seg_2003" n="HIAT:ip">?</nts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T587" id="Seg_2005" n="sc" s="T585">
               <ts e="T587" id="Seg_2007" n="HIAT:u" s="T585">
                  <ts e="T586" id="Seg_2009" n="HIAT:w" s="T585">Alləm</ts>
                  <nts id="Seg_2010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2012" n="HIAT:w" s="T586">gibər-nʼibudʼ</ts>
                  <nts id="Seg_2013" n="HIAT:ip">.</nts>
                  <nts id="Seg_2014" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T591" id="Seg_2015" n="sc" s="T588">
               <ts e="T591" id="Seg_2017" n="HIAT:u" s="T588">
                  <ts e="T589" id="Seg_2019" n="HIAT:w" s="T588">Amnoləŋ</ts>
                  <nts id="Seg_2020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2022" n="HIAT:w" s="T589">i</ts>
                  <nts id="Seg_2023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2025" n="HIAT:w" s="T590">iləm</ts>
                  <nts id="Seg_2026" n="HIAT:ip">.</nts>
                  <nts id="Seg_2027" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T597" id="Seg_2028" n="sc" s="T592">
               <ts e="T597" id="Seg_2030" n="HIAT:u" s="T592">
                  <nts id="Seg_2031" n="HIAT:ip">(</nts>
                  <ts e="T593" id="Seg_2033" n="HIAT:w" s="T592">Bar</ts>
                  <nts id="Seg_2034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2036" n="HIAT:w" s="T593">ĭmbi=</ts>
                  <nts id="Seg_2037" n="HIAT:ip">)</nts>
                  <nts id="Seg_2038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2040" n="HIAT:w" s="T594">bar</ts>
                  <nts id="Seg_2041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2042" n="HIAT:ip">(</nts>
                  <ts e="T596" id="Seg_2044" n="HIAT:w" s="T595">i-</ts>
                  <nts id="Seg_2045" n="HIAT:ip">)</nts>
                  <nts id="Seg_2046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2048" n="HIAT:w" s="T596">iləm</ts>
                  <nts id="Seg_2049" n="HIAT:ip">.</nts>
                  <nts id="Seg_2050" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T602" id="Seg_2051" n="sc" s="T598">
               <ts e="T602" id="Seg_2053" n="HIAT:u" s="T598">
                  <ts e="T599" id="Seg_2055" n="HIAT:w" s="T598">A</ts>
                  <nts id="Seg_2056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2058" n="HIAT:w" s="T599">moʔ</ts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2061" n="HIAT:w" s="T600">dere</ts>
                  <nts id="Seg_2062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2064" n="HIAT:w" s="T601">moləl</ts>
                  <nts id="Seg_2065" n="HIAT:ip">?</nts>
                  <nts id="Seg_2066" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T607" id="Seg_2067" n="sc" s="T603">
               <ts e="T607" id="Seg_2069" n="HIAT:u" s="T603">
                  <ts e="T604" id="Seg_2071" n="HIAT:w" s="T603">Dak</ts>
                  <nts id="Seg_2072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2074" n="HIAT:w" s="T604">măna</ts>
                  <nts id="Seg_2075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2077" n="HIAT:w" s="T605">bile</ts>
                  <nts id="Seg_2078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2080" n="HIAT:w" s="T606">amnozittə</ts>
                  <nts id="Seg_2081" n="HIAT:ip">.</nts>
                  <nts id="Seg_2082" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T612" id="Seg_2083" n="sc" s="T608">
               <ts e="T612" id="Seg_2085" n="HIAT:u" s="T608">
                  <ts e="T609" id="Seg_2087" n="HIAT:w" s="T608">Nada</ts>
                  <nts id="Seg_2088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2090" n="HIAT:w" s="T609">anzittə</ts>
                  <nts id="Seg_2091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2093" n="HIAT:w" s="T610">bostə</ts>
                  <nts id="Seg_2094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2096" n="HIAT:w" s="T611">turanə</ts>
                  <nts id="Seg_2097" n="HIAT:ip">.</nts>
                  <nts id="Seg_2098" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T618" id="Seg_2099" n="sc" s="T613">
               <ts e="T618" id="Seg_2101" n="HIAT:u" s="T613">
                  <ts e="T614" id="Seg_2103" n="HIAT:w" s="T613">A</ts>
                  <nts id="Seg_2104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2106" n="HIAT:w" s="T614">ĭmbi</ts>
                  <nts id="Seg_2107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2109" n="HIAT:w" s="T615">tura</ts>
                  <nts id="Seg_2110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2111" n="HIAT:ip">(</nts>
                  <ts e="T617" id="Seg_2113" n="HIAT:w" s="T616">mĭləl</ts>
                  <nts id="Seg_2114" n="HIAT:ip">)</nts>
                  <nts id="Seg_2115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2117" n="HIAT:w" s="T617">tănan</ts>
                  <nts id="Seg_2118" n="HIAT:ip">?</nts>
                  <nts id="Seg_2119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T621" id="Seg_2120" n="sc" s="T619">
               <ts e="T621" id="Seg_2122" n="HIAT:u" s="T619">
                  <ts e="T620" id="Seg_2124" n="HIAT:w" s="T619">Ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_2125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2127" n="HIAT:w" s="T620">mĭləl</ts>
                  <nts id="Seg_2128" n="HIAT:ip">.</nts>
                  <nts id="Seg_2129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T623" id="Seg_2130" n="sc" s="T622">
               <ts e="T623" id="Seg_2132" n="HIAT:u" s="T622">
                  <ts e="T623" id="Seg_2134" n="HIAT:w" s="T622">Gibər</ts>
                  <nts id="Seg_2135" n="HIAT:ip">?</nts>
                  <nts id="Seg_2136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T627" id="Seg_2137" n="sc" s="T624">
               <ts e="T627" id="Seg_2139" n="HIAT:u" s="T624">
                  <ts e="T625" id="Seg_2141" n="HIAT:w" s="T624">A</ts>
                  <nts id="Seg_2142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2144" n="HIAT:w" s="T625">dettə</ts>
                  <nts id="Seg_2145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2147" n="HIAT:w" s="T626">gibər</ts>
                  <nts id="Seg_2148" n="HIAT:ip">?</nts>
                  <nts id="Seg_2149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T631" id="Seg_2150" n="sc" s="T628">
               <ts e="T631" id="Seg_2152" n="HIAT:u" s="T628">
                  <ts e="T629" id="Seg_2154" n="HIAT:w" s="T628">Nu</ts>
                  <nts id="Seg_2155" n="HIAT:ip">,</nts>
                  <nts id="Seg_2156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2158" n="HIAT:w" s="T629">bos</ts>
                  <nts id="Seg_2159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2161" n="HIAT:w" s="T630">turanə</ts>
                  <nts id="Seg_2162" n="HIAT:ip">.</nts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T634" id="Seg_2164" n="sc" s="T632">
               <ts e="T634" id="Seg_2166" n="HIAT:u" s="T632">
                  <ts e="T633" id="Seg_2168" n="HIAT:w" s="T632">Šoləm</ts>
                  <nts id="Seg_2169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2171" n="HIAT:w" s="T633">amnolapliem</ts>
                  <nts id="Seg_2172" n="HIAT:ip">.</nts>
                  <nts id="Seg_2173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T636" id="Seg_2174" n="sc" s="T635">
               <ts e="T636" id="Seg_2176" n="HIAT:u" s="T635">
                  <ts e="T636" id="Seg_2178" n="HIAT:w" s="T635">Вот</ts>
                  <nts id="Seg_2179" n="HIAT:ip">.</nts>
                  <nts id="Seg_2180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T638" id="Seg_2181" n="sc" s="T637">
               <ts e="T638" id="Seg_2183" n="HIAT:u" s="T637">
                  <nts id="Seg_2184" n="HIAT:ip">(</nts>
                  <nts id="Seg_2185" n="HIAT:ip">(</nts>
                  <ats e="T638" id="Seg_2186" n="HIAT:non-pho" s="T637">BRK</ats>
                  <nts id="Seg_2187" n="HIAT:ip">)</nts>
                  <nts id="Seg_2188" n="HIAT:ip">)</nts>
                  <nts id="Seg_2189" n="HIAT:ip">.</nts>
                  <nts id="Seg_2190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T643" id="Seg_2191" n="sc" s="T639">
               <ts e="T643" id="Seg_2193" n="HIAT:u" s="T639">
                  <ts e="T640" id="Seg_2195" n="HIAT:w" s="T639">Dettə</ts>
                  <nts id="Seg_2196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2198" n="HIAT:w" s="T640">măna</ts>
                  <nts id="Seg_2199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2201" n="HIAT:w" s="T641">tamgu</ts>
                  <nts id="Seg_2202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2204" n="HIAT:w" s="T642">nʼeʔsittə</ts>
                  <nts id="Seg_2205" n="HIAT:ip">.</nts>
                  <nts id="Seg_2206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T647" id="Seg_2207" n="sc" s="T644">
               <ts e="T647" id="Seg_2209" n="HIAT:u" s="T644">
                  <ts e="T645" id="Seg_2211" n="HIAT:w" s="T644">Măn</ts>
                  <nts id="Seg_2212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2214" n="HIAT:w" s="T645">naga</ts>
                  <nts id="Seg_2215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2217" n="HIAT:w" s="T646">tamgu</ts>
                  <nts id="Seg_2218" n="HIAT:ip">.</nts>
                  <nts id="Seg_2219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T652" id="Seg_2220" n="sc" s="T648">
               <ts e="T652" id="Seg_2222" n="HIAT:u" s="T648">
                  <ts e="T649" id="Seg_2224" n="HIAT:w" s="T648">Ox</ts>
                  <nts id="Seg_2225" n="HIAT:ip">,</nts>
                  <nts id="Seg_2226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2228" n="HIAT:w" s="T649">i</ts>
                  <nts id="Seg_2229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2231" n="HIAT:w" s="T650">măn</ts>
                  <nts id="Seg_2232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2234" n="HIAT:w" s="T651">naga</ts>
                  <nts id="Seg_2235" n="HIAT:ip">.</nts>
                  <nts id="Seg_2236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T656" id="Seg_2237" n="sc" s="T653">
               <ts e="T656" id="Seg_2239" n="HIAT:u" s="T653">
                  <ts e="T654" id="Seg_2241" n="HIAT:w" s="T653">Dʼok</ts>
                  <nts id="Seg_2242" n="HIAT:ip">,</nts>
                  <nts id="Seg_2243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2245" n="HIAT:w" s="T654">ige</ts>
                  <nts id="Seg_2246" n="HIAT:ip">,</nts>
                  <nts id="Seg_2247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2249" n="HIAT:w" s="T655">ige</ts>
                  <nts id="Seg_2250" n="HIAT:ip">.</nts>
                  <nts id="Seg_2251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T659" id="Seg_2252" n="sc" s="T657">
               <ts e="T659" id="Seg_2254" n="HIAT:u" s="T657">
                  <ts e="T658" id="Seg_2256" n="HIAT:w" s="T657">Dettə</ts>
                  <nts id="Seg_2257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2259" n="HIAT:w" s="T658">măna</ts>
                  <nts id="Seg_2260" n="HIAT:ip">.</nts>
                  <nts id="Seg_2261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T663" id="Seg_2262" n="sc" s="T660">
               <ts e="T663" id="Seg_2264" n="HIAT:u" s="T660">
                  <ts e="T661" id="Seg_2266" n="HIAT:w" s="T660">Măn</ts>
                  <nts id="Seg_2267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2269" n="HIAT:w" s="T661">naga</ts>
                  <nts id="Seg_2270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2272" n="HIAT:w" s="T662">tamgu</ts>
                  <nts id="Seg_2273" n="HIAT:ip">.</nts>
                  <nts id="Seg_2274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T668" id="Seg_2275" n="sc" s="T664">
               <ts e="T668" id="Seg_2277" n="HIAT:u" s="T664">
                  <ts e="T665" id="Seg_2279" n="HIAT:w" s="T664">A</ts>
                  <nts id="Seg_2280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2282" n="HIAT:w" s="T665">măn</ts>
                  <nts id="Seg_2283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2285" n="HIAT:w" s="T666">nʼeʔsittə</ts>
                  <nts id="Seg_2286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2288" n="HIAT:w" s="T667">molam</ts>
                  <nts id="Seg_2289" n="HIAT:ip">.</nts>
                  <nts id="Seg_2290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T670" id="Seg_2291" n="sc" s="T669">
               <ts e="T670" id="Seg_2293" n="HIAT:u" s="T669">
                  <nts id="Seg_2294" n="HIAT:ip">(</nts>
                  <nts id="Seg_2295" n="HIAT:ip">(</nts>
                  <ats e="T670" id="Seg_2296" n="HIAT:non-pho" s="T669">BRK</ats>
                  <nts id="Seg_2297" n="HIAT:ip">)</nts>
                  <nts id="Seg_2298" n="HIAT:ip">)</nts>
                  <nts id="Seg_2299" n="HIAT:ip">.</nts>
                  <nts id="Seg_2300" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T675" id="Seg_2301" n="sc" s="T671">
               <ts e="T675" id="Seg_2303" n="HIAT:u" s="T671">
                  <ts e="T672" id="Seg_2305" n="HIAT:w" s="T671">Šoʔ</ts>
                  <nts id="Seg_2306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2308" n="HIAT:w" s="T672">măna</ts>
                  <nts id="Seg_2309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2311" n="HIAT:w" s="T673">ara</ts>
                  <nts id="Seg_2312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2314" n="HIAT:w" s="T674">bĭʔsittə</ts>
                  <nts id="Seg_2315" n="HIAT:ip">.</nts>
                  <nts id="Seg_2316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T681" id="Seg_2317" n="sc" s="T676">
               <ts e="T681" id="Seg_2319" n="HIAT:u" s="T676">
                  <ts e="T677" id="Seg_2321" n="HIAT:w" s="T676">Da</ts>
                  <nts id="Seg_2322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2324" n="HIAT:w" s="T677">ara</ts>
                  <nts id="Seg_2325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2326" n="HIAT:ip">(</nts>
                  <ts e="T679" id="Seg_2328" n="HIAT:w" s="T678">bĭ-</ts>
                  <nts id="Seg_2329" n="HIAT:ip">)</nts>
                  <nts id="Seg_2330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2332" n="HIAT:w" s="T679">bĭʔsittə</ts>
                  <nts id="Seg_2333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2335" n="HIAT:w" s="T680">nada</ts>
                  <nts id="Seg_2336" n="HIAT:ip">.</nts>
                  <nts id="Seg_2337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T690" id="Seg_2338" n="sc" s="T682">
               <ts e="T690" id="Seg_2340" n="HIAT:u" s="T682">
                  <ts e="T683" id="Seg_2342" n="HIAT:w" s="T682">Nu</ts>
                  <nts id="Seg_2343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2345" n="HIAT:w" s="T683">nada</ts>
                  <nts id="Seg_2346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2347" n="HIAT:ip">(</nts>
                  <ts e="T685" id="Seg_2349" n="HIAT:w" s="T684">g-</ts>
                  <nts id="Seg_2350" n="HIAT:ip">)</nts>
                  <nts id="Seg_2351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2353" n="HIAT:w" s="T685">ara</ts>
                  <nts id="Seg_2354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2356" n="HIAT:w" s="T686">bĭʔsʼittə</ts>
                  <nts id="Seg_2357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2359" n="HIAT:w" s="T687">dak</ts>
                  <nts id="Seg_2360" n="HIAT:ip">,</nts>
                  <nts id="Seg_2361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2363" n="HIAT:w" s="T688">ej</ts>
                  <nts id="Seg_2364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2366" n="HIAT:w" s="T689">dʼabroʔ</ts>
                  <nts id="Seg_2367" n="HIAT:ip">.</nts>
                  <nts id="Seg_2368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T695" id="Seg_2369" n="sc" s="T691">
               <ts e="T695" id="Seg_2371" n="HIAT:u" s="T691">
                  <ts e="T692" id="Seg_2373" n="HIAT:w" s="T691">Tăn</ts>
                  <nts id="Seg_2374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2376" n="HIAT:w" s="T692">vedʼ</ts>
                  <nts id="Seg_2377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2379" n="HIAT:w" s="T693">bile</ts>
                  <nts id="Seg_2380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2382" n="HIAT:w" s="T694">kuza</ts>
                  <nts id="Seg_2383" n="HIAT:ip">!</nts>
                  <nts id="Seg_2384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T700" id="Seg_2385" n="sc" s="T696">
               <ts e="T700" id="Seg_2387" n="HIAT:u" s="T696">
                  <ts e="T697" id="Seg_2389" n="HIAT:w" s="T696">A</ts>
                  <nts id="Seg_2390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2392" n="HIAT:w" s="T697">măn</ts>
                  <nts id="Seg_2393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2395" n="HIAT:w" s="T698">jakše</ts>
                  <nts id="Seg_2396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2398" n="HIAT:w" s="T699">kuza</ts>
                  <nts id="Seg_2399" n="HIAT:ip">.</nts>
                  <nts id="Seg_2400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T708" id="Seg_2401" n="sc" s="T701">
               <ts e="T708" id="Seg_2403" n="HIAT:u" s="T701">
                  <ts e="T702" id="Seg_2405" n="HIAT:w" s="T701">Măn</ts>
                  <nts id="Seg_2406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2408" n="HIAT:w" s="T702">ara</ts>
                  <nts id="Seg_2409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2410" n="HIAT:ip">(</nts>
                  <ts e="T704" id="Seg_2412" n="HIAT:w" s="T703">bĭ-</ts>
                  <nts id="Seg_2413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2415" n="HIAT:w" s="T704">bĭ-</ts>
                  <nts id="Seg_2416" n="HIAT:ip">)</nts>
                  <nts id="Seg_2417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2419" n="HIAT:w" s="T705">bĭtliem</ts>
                  <nts id="Seg_2420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2421" n="HIAT:ip">—</nts>
                  <nts id="Seg_2422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2424" n="HIAT:w" s="T706">ej</ts>
                  <nts id="Seg_2425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2427" n="HIAT:w" s="T707">dʼabrolam</ts>
                  <nts id="Seg_2428" n="HIAT:ip">.</nts>
                  <nts id="Seg_2429" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T713" id="Seg_2430" n="sc" s="T709">
               <ts e="T713" id="Seg_2432" n="HIAT:u" s="T709">
                  <ts e="T710" id="Seg_2434" n="HIAT:w" s="T709">A</ts>
                  <nts id="Seg_2435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2437" n="HIAT:w" s="T710">tăn</ts>
                  <nts id="Seg_2438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2440" n="HIAT:w" s="T711">üge</ts>
                  <nts id="Seg_2441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2442" n="HIAT:ip">(</nts>
                  <ts e="T713" id="Seg_2444" n="HIAT:w" s="T712">dʼabroliel</ts>
                  <nts id="Seg_2445" n="HIAT:ip">)</nts>
                  <nts id="Seg_2446" n="HIAT:ip">.</nts>
                  <nts id="Seg_2447" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T719" id="Seg_2448" n="sc" s="T714">
               <ts e="T719" id="Seg_2450" n="HIAT:u" s="T714">
                  <nts id="Seg_2451" n="HIAT:ip">(</nts>
                  <ts e="T715" id="Seg_2453" n="HIAT:w" s="T714">Šen</ts>
                  <nts id="Seg_2454" n="HIAT:ip">)</nts>
                  <nts id="Seg_2455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2457" n="HIAT:w" s="T715">ej</ts>
                  <nts id="Seg_2458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2460" n="HIAT:w" s="T716">jakše</ts>
                  <nts id="Seg_2461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2463" n="HIAT:w" s="T717">dărə</ts>
                  <nts id="Seg_2464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2466" n="HIAT:w" s="T718">bĭʔsittə</ts>
                  <nts id="Seg_2467" n="HIAT:ip">.</nts>
                  <nts id="Seg_2468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T725" id="Seg_2469" n="sc" s="T720">
               <ts e="T725" id="Seg_2471" n="HIAT:u" s="T720">
                  <ts e="T721" id="Seg_2473" n="HIAT:w" s="T720">Bĭʔsittə</ts>
                  <nts id="Seg_2474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2476" n="HIAT:w" s="T721">nada</ts>
                  <nts id="Seg_2477" n="HIAT:ip">,</nts>
                  <nts id="Seg_2478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2480" n="HIAT:w" s="T722">nʼe</ts>
                  <nts id="Seg_2481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2483" n="HIAT:w" s="T723">nada</ts>
                  <nts id="Seg_2484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2486" n="HIAT:w" s="T724">dʼabrozittə</ts>
                  <nts id="Seg_2487" n="HIAT:ip">.</nts>
                  <nts id="Seg_2488" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T728" id="Seg_2489" n="sc" s="T726">
               <ts e="T728" id="Seg_2491" n="HIAT:u" s="T726">
                  <ts e="T727" id="Seg_2493" n="HIAT:w" s="T726">Nada</ts>
                  <nts id="Seg_2494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2496" n="HIAT:w" s="T727">jakše</ts>
                  <nts id="Seg_2497" n="HIAT:ip">.</nts>
                  <nts id="Seg_2498" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T734" id="Seg_2499" n="sc" s="T729">
               <ts e="T734" id="Seg_2501" n="HIAT:u" s="T729">
                  <ts e="T730" id="Seg_2503" n="HIAT:w" s="T729">Măn</ts>
                  <nts id="Seg_2504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2506" n="HIAT:w" s="T730">iš</ts>
                  <nts id="Seg_2507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2508" n="HIAT:ip">(</nts>
                  <ts e="T732" id="Seg_2510" n="HIAT:w" s="T731">gĭrgit</ts>
                  <nts id="Seg_2511" n="HIAT:ip">)</nts>
                  <nts id="Seg_2512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2514" n="HIAT:w" s="T732">kuvas</ts>
                  <nts id="Seg_2515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2517" n="HIAT:w" s="T733">kuza</ts>
                  <nts id="Seg_2518" n="HIAT:ip">.</nts>
                  <nts id="Seg_2519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T736" id="Seg_2520" n="sc" s="T735">
               <ts e="T736" id="Seg_2522" n="HIAT:u" s="T735">
                  <ts e="T736" id="Seg_2524" n="HIAT:w" s="T735">Вот</ts>
                  <nts id="Seg_2525" n="HIAT:ip">.</nts>
                  <nts id="Seg_2526" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-SAE">
            <ts e="T2" id="Seg_2527" n="sc" s="T1">
               <ts e="T2" id="Seg_2529" n="e" s="T1">((BRK)). </ts>
            </ts>
            <ts e="T7" id="Seg_2530" n="sc" s="T5">
               <ts e="T7" id="Seg_2532" n="e" s="T5">Говорить? </ts>
            </ts>
            <ts e="T13" id="Seg_2533" n="sc" s="T9">
               <ts e="T10" id="Seg_2535" n="e" s="T9">Abam </ts>
               <ts e="T11" id="Seg_2537" n="e" s="T10">mo </ts>
               <ts e="T12" id="Seg_2539" n="e" s="T11">dereʔ </ts>
               <ts e="T13" id="Seg_2541" n="e" s="T12">nörbəlat. </ts>
            </ts>
            <ts e="T16" id="Seg_2542" n="sc" s="T14">
               <ts e="T15" id="Seg_2544" n="e" s="T14">Šen </ts>
               <ts e="T16" id="Seg_2546" n="e" s="T15">bile. </ts>
            </ts>
            <ts e="T20" id="Seg_2547" n="sc" s="T17">
               <ts e="T18" id="Seg_2549" n="e" s="T17">Šen </ts>
               <ts e="T19" id="Seg_2551" n="e" s="T18">bile </ts>
               <ts e="T20" id="Seg_2553" n="e" s="T19">nörbəlat. </ts>
            </ts>
            <ts e="T26" id="Seg_2554" n="sc" s="T21">
               <ts e="T22" id="Seg_2556" n="e" s="T21">Mo </ts>
               <ts e="T23" id="Seg_2558" n="e" s="T22">dereʔ </ts>
               <ts e="T24" id="Seg_2560" n="e" s="T23">(ne-) </ts>
               <ts e="T25" id="Seg_2562" n="e" s="T24">tăn </ts>
               <ts e="T26" id="Seg_2564" n="e" s="T25">nörblat? </ts>
            </ts>
            <ts e="T32" id="Seg_2565" n="sc" s="T27">
               <ts e="T28" id="Seg_2567" n="e" s="T27">Dʼok </ts>
               <ts e="T29" id="Seg_2569" n="e" s="T28">măn </ts>
               <ts e="T30" id="Seg_2571" n="e" s="T29">jakše </ts>
               <ts e="T31" id="Seg_2573" n="e" s="T30">nörblam </ts>
               <ts e="T32" id="Seg_2575" n="e" s="T31">tănan. </ts>
            </ts>
            <ts e="T36" id="Seg_2576" n="sc" s="T33">
               <ts e="T34" id="Seg_2578" n="e" s="T33">Nen </ts>
               <ts e="T35" id="Seg_2580" n="e" s="T34">anaʔ </ts>
               <ts e="T36" id="Seg_2582" n="e" s="T35">dĭbər-tə. </ts>
            </ts>
            <ts e="T41" id="Seg_2583" n="sc" s="T37">
               <ts e="T38" id="Seg_2585" n="e" s="T37">Nu, </ts>
               <ts e="T39" id="Seg_2587" n="e" s="T38">măn </ts>
               <ts e="T40" id="Seg_2589" n="e" s="T39">ej </ts>
               <ts e="T41" id="Seg_2591" n="e" s="T40">allam. </ts>
            </ts>
            <ts e="T45" id="Seg_2592" n="sc" s="T42">
               <ts e="T43" id="Seg_2594" n="e" s="T42">Moʔ, </ts>
               <ts e="T44" id="Seg_2596" n="e" s="T43">dak </ts>
               <ts e="T45" id="Seg_2598" n="e" s="T44">moʔ. </ts>
            </ts>
            <ts e="T49" id="Seg_2599" n="sc" s="T46">
               <ts e="T47" id="Seg_2601" n="e" s="T46">Măn </ts>
               <ts e="T48" id="Seg_2603" n="e" s="T47">udam </ts>
               <ts e="T49" id="Seg_2605" n="e" s="T48">naga. </ts>
            </ts>
            <ts e="T53" id="Seg_2606" n="sc" s="T50">
               <ts e="T51" id="Seg_2608" n="e" s="T50">Ну </ts>
               <ts e="T52" id="Seg_2610" n="e" s="T51">вот. </ts>
               <ts e="T53" id="Seg_2612" n="e" s="T52">((BRK)). </ts>
            </ts>
            <ts e="T60" id="Seg_2613" n="sc" s="T54">
               <ts e="T55" id="Seg_2615" n="e" s="T54">Ой, </ts>
               <ts e="T56" id="Seg_2617" n="e" s="T55">я </ts>
               <ts e="T57" id="Seg_2619" n="e" s="T56">беру </ts>
               <ts e="T59" id="Seg_2621" n="e" s="T57">в </ts>
               <ts e="T60" id="Seg_2623" n="e" s="T59">руки… </ts>
            </ts>
            <ts e="T67" id="Seg_2624" n="sc" s="T62">
               <ts e="T63" id="Seg_2626" n="e" s="T62">Aba </ts>
               <ts e="T64" id="Seg_2628" n="e" s="T63">koško </ts>
               <ts e="T65" id="Seg_2630" n="e" s="T64">moʔ </ts>
               <ts e="T66" id="Seg_2632" n="e" s="T65">dereʔ </ts>
               <ts e="T67" id="Seg_2634" n="e" s="T66">tăn? </ts>
            </ts>
            <ts e="T70" id="Seg_2635" n="sc" s="T68">
               <ts e="T69" id="Seg_2637" n="e" s="T68">Anaʔ </ts>
               <ts e="T70" id="Seg_2639" n="e" s="T69">dĭbər. </ts>
            </ts>
            <ts e="T75" id="Seg_2640" n="sc" s="T71">
               <ts e="T72" id="Seg_2642" n="e" s="T71">Dʼok, </ts>
               <ts e="T73" id="Seg_2644" n="e" s="T72">măn </ts>
               <ts e="T74" id="Seg_2646" n="e" s="T73">ej </ts>
               <ts e="T75" id="Seg_2648" n="e" s="T74">allam. </ts>
            </ts>
            <ts e="T79" id="Seg_2649" n="sc" s="T76">
               <ts e="T77" id="Seg_2651" n="e" s="T76">Măn </ts>
               <ts e="T78" id="Seg_2653" n="e" s="T77">udam </ts>
               <ts e="T79" id="Seg_2655" n="e" s="T78">bile. </ts>
            </ts>
            <ts e="T81" id="Seg_2656" n="sc" s="T80">
               <ts e="T81" id="Seg_2658" n="e" s="T80">((BRK)). </ts>
            </ts>
            <ts e="T83" id="Seg_2659" n="sc" s="T82">
               <ts e="T83" id="Seg_2661" n="e" s="T82">Можно? </ts>
            </ts>
            <ts e="T88" id="Seg_2662" n="sc" s="T84">
               <ts e="T85" id="Seg_2664" n="e" s="T84">Kanžəbəj </ts>
               <ts e="T86" id="Seg_2666" n="e" s="T85">măna </ts>
               <ts e="T87" id="Seg_2668" n="e" s="T86">ara </ts>
               <ts e="T88" id="Seg_2670" n="e" s="T87">bĭʔsittə. </ts>
            </ts>
            <ts e="T92" id="Seg_2671" n="sc" s="T89">
               <ts e="T90" id="Seg_2673" n="e" s="T89">Dʼok, </ts>
               <ts e="T91" id="Seg_2675" n="e" s="T90">ej </ts>
               <ts e="T92" id="Seg_2677" n="e" s="T91">allam. </ts>
            </ts>
            <ts e="T94" id="Seg_2678" n="sc" s="T93">
               <ts e="T94" id="Seg_2680" n="e" s="T93">Moʔ? </ts>
            </ts>
            <ts e="T98" id="Seg_2681" n="sc" s="T95">
               <ts e="T96" id="Seg_2683" n="e" s="T95">Kanžəbəj, </ts>
               <ts e="T97" id="Seg_2685" n="e" s="T96">ara </ts>
               <ts e="T98" id="Seg_2687" n="e" s="T97">kuvas. </ts>
            </ts>
            <ts e="T102" id="Seg_2688" n="sc" s="T99">
               <ts e="T100" id="Seg_2690" n="e" s="T99">Kuvas </ts>
               <ts e="T101" id="Seg_2692" n="e" s="T100">(bĭs-) </ts>
               <ts e="T102" id="Seg_2694" n="e" s="T101">bĭsittə. </ts>
            </ts>
            <ts e="T108" id="Seg_2695" n="sc" s="T103">
               <ts e="T104" id="Seg_2697" n="e" s="T103">Dʼok, </ts>
               <ts e="T105" id="Seg_2699" n="e" s="T104">măna </ts>
               <ts e="T106" id="Seg_2701" n="e" s="T105">(dĭ-) </ts>
               <ts e="T107" id="Seg_2703" n="e" s="T106">dĭgən </ts>
               <ts e="T108" id="Seg_2705" n="e" s="T107">münörlit. </ts>
            </ts>
            <ts e="T115" id="Seg_2706" n="sc" s="T109">
               <ts e="T110" id="Seg_2708" n="e" s="T109">Dʼok, </ts>
               <ts e="T111" id="Seg_2710" n="e" s="T110">măn </ts>
               <ts e="T112" id="Seg_2712" n="e" s="T111">ej </ts>
               <ts e="T113" id="Seg_2714" n="e" s="T112">mĭləm </ts>
               <ts e="T114" id="Seg_2716" n="e" s="T113">tănan </ts>
               <ts e="T115" id="Seg_2718" n="e" s="T114">münörzittə. </ts>
            </ts>
            <ts e="T118" id="Seg_2719" n="sc" s="T116">
               <ts e="T117" id="Seg_2721" n="e" s="T116">Nu </ts>
               <ts e="T118" id="Seg_2723" n="e" s="T117">alləm. </ts>
            </ts>
            <ts e="T122" id="Seg_2724" n="sc" s="T119">
               <ts e="T120" id="Seg_2726" n="e" s="T119">Alləm. </ts>
               <ts e="T121" id="Seg_2728" n="e" s="T120">Ej </ts>
               <ts e="T122" id="Seg_2730" n="e" s="T121">mĭləl? </ts>
            </ts>
            <ts e="T126" id="Seg_2731" n="sc" s="T123">
               <ts e="T124" id="Seg_2733" n="e" s="T123">Dʼok, </ts>
               <ts e="T125" id="Seg_2735" n="e" s="T124">ej </ts>
               <ts e="T126" id="Seg_2737" n="e" s="T125">mĭləm. </ts>
            </ts>
            <ts e="T129" id="Seg_2738" n="sc" s="T127">
               <ts e="T128" id="Seg_2740" n="e" s="T127">Nu </ts>
               <ts e="T129" id="Seg_2742" n="e" s="T128">jakše. </ts>
            </ts>
            <ts e="T136" id="Seg_2743" n="sc" s="T130">
               <ts e="T131" id="Seg_2745" n="e" s="T130">Jakše, </ts>
               <ts e="T132" id="Seg_2747" n="e" s="T131">ej </ts>
               <ts e="T133" id="Seg_2749" n="e" s="T132">mĭləl </ts>
               <ts e="T134" id="Seg_2751" n="e" s="T133">dak, </ts>
               <ts e="T135" id="Seg_2753" n="e" s="T134">măn </ts>
               <ts e="T136" id="Seg_2755" n="e" s="T135">alləm. </ts>
            </ts>
            <ts e="T138" id="Seg_2756" n="sc" s="T137">
               <ts e="T138" id="Seg_2758" n="e" s="T137">((BRK)). </ts>
            </ts>
            <ts e="T142" id="Seg_2759" n="sc" s="T141">
               <ts e="T142" id="Seg_2761" n="e" s="T141">Сейчас. </ts>
            </ts>
            <ts e="T147" id="Seg_2762" n="sc" s="T143">
               <ts e="T144" id="Seg_2764" n="e" s="T143">Măn </ts>
               <ts e="T145" id="Seg_2766" n="e" s="T144">iamzi </ts>
               <ts e="T146" id="Seg_2768" n="e" s="T145">ej </ts>
               <ts e="T147" id="Seg_2770" n="e" s="T146">molam. </ts>
            </ts>
            <ts e="T151" id="Seg_2771" n="sc" s="T148">
               <ts e="T149" id="Seg_2773" n="e" s="T148">Măn </ts>
               <ts e="T150" id="Seg_2775" n="e" s="T149">ari </ts>
               <ts e="T151" id="Seg_2777" n="e" s="T150">dʼürləm. </ts>
            </ts>
            <ts e="T153" id="Seg_2778" n="sc" s="T152">
               <ts e="T153" id="Seg_2780" n="e" s="T152">Moʔ? </ts>
            </ts>
            <ts e="T158" id="Seg_2781" n="sc" s="T154">
               <ts e="T155" id="Seg_2783" n="e" s="T154">Da </ts>
               <ts e="T156" id="Seg_2785" n="e" s="T155">dĭ </ts>
               <ts e="T157" id="Seg_2787" n="e" s="T156">kudonzla </ts>
               <ts e="T158" id="Seg_2789" n="e" s="T157">tăŋ. </ts>
            </ts>
            <ts e="T162" id="Seg_2790" n="sc" s="T159">
               <ts e="T160" id="Seg_2792" n="e" s="T159">Nu, </ts>
               <ts e="T161" id="Seg_2794" n="e" s="T160">amnoʔ </ts>
               <ts e="T162" id="Seg_2796" n="e" s="T161">iazil. </ts>
            </ts>
            <ts e="T164" id="Seg_2797" n="sc" s="T163">
               <ts e="T164" id="Seg_2799" n="e" s="T163">Amnoʔ. </ts>
            </ts>
            <ts e="T168" id="Seg_2800" n="sc" s="T165">
               <ts e="T166" id="Seg_2802" n="e" s="T165">Moʔ </ts>
               <ts e="T167" id="Seg_2804" n="e" s="T166">dere </ts>
               <ts e="T168" id="Seg_2806" n="e" s="T167">molal? </ts>
            </ts>
            <ts e="T172" id="Seg_2807" n="sc" s="T169">
               <ts e="T170" id="Seg_2809" n="e" s="T169">Tăn </ts>
               <ts e="T171" id="Seg_2811" n="e" s="T170">bila </ts>
               <ts e="T172" id="Seg_2813" n="e" s="T171">kuza. </ts>
            </ts>
            <ts e="T174" id="Seg_2814" n="sc" s="T173">
               <ts e="T174" id="Seg_2816" n="e" s="T173">Вот. </ts>
            </ts>
            <ts e="T176" id="Seg_2817" n="sc" s="T175">
               <ts e="T176" id="Seg_2819" n="e" s="T175">((BRK)). </ts>
            </ts>
            <ts e="T183" id="Seg_2820" n="sc" s="T177">
               <ts e="T178" id="Seg_2822" n="e" s="T177">Tăn </ts>
               <ts e="T179" id="Seg_2824" n="e" s="T178">abal </ts>
               <ts e="T180" id="Seg_2826" n="e" s="T179">iam </ts>
               <ts e="T181" id="Seg_2828" n="e" s="T180">šen </ts>
               <ts e="T182" id="Seg_2830" n="e" s="T181">bila </ts>
               <ts e="T183" id="Seg_2832" n="e" s="T182">kuza. </ts>
            </ts>
            <ts e="T187" id="Seg_2833" n="sc" s="T184">
               <ts e="T185" id="Seg_2835" n="e" s="T184">A </ts>
               <ts e="T186" id="Seg_2837" n="e" s="T185">măn </ts>
               <ts e="T187" id="Seg_2839" n="e" s="T186">jakše. </ts>
            </ts>
            <ts e="T193" id="Seg_2840" n="sc" s="T188">
               <ts e="T189" id="Seg_2842" n="e" s="T188">Dʼok, </ts>
               <ts e="T190" id="Seg_2844" n="e" s="T189">măn </ts>
               <ts e="T191" id="Seg_2846" n="e" s="T190">jakše. </ts>
               <ts e="T192" id="Seg_2848" n="e" s="T191">Tăn </ts>
               <ts e="T193" id="Seg_2850" n="e" s="T192">bila. </ts>
            </ts>
            <ts e="T199" id="Seg_2851" n="sc" s="T194">
               <ts e="T195" id="Seg_2853" n="e" s="T194">Oh, </ts>
               <ts e="T196" id="Seg_2855" n="e" s="T195">mo </ts>
               <ts e="T197" id="Seg_2857" n="e" s="T196">dereʔ </ts>
               <ts e="T198" id="Seg_2859" n="e" s="T197">(m-) </ts>
               <ts e="T199" id="Seg_2861" n="e" s="T198">măllal? </ts>
            </ts>
            <ts e="T201" id="Seg_2862" n="sc" s="T200">
               <ts e="T201" id="Seg_2864" n="e" s="T200">Eneidənə. </ts>
            </ts>
            <ts e="T204" id="Seg_2865" n="sc" s="T202">
               <ts e="T203" id="Seg_2867" n="e" s="T202">Mănuʔ </ts>
               <ts e="T204" id="Seg_2869" n="e" s="T203">jakše. </ts>
            </ts>
            <ts e="T209" id="Seg_2870" n="sc" s="T205">
               <ts e="T206" id="Seg_2872" n="e" s="T205">Oh, </ts>
               <ts e="T207" id="Seg_2874" n="e" s="T206">tăŋ </ts>
               <ts e="T208" id="Seg_2876" n="e" s="T207">bila </ts>
               <ts e="T209" id="Seg_2878" n="e" s="T208">kuza. </ts>
            </ts>
            <ts e="T211" id="Seg_2879" n="sc" s="T210">
               <ts e="T211" id="Seg_2881" n="e" s="T210">Eneidənə. </ts>
            </ts>
            <ts e="T213" id="Seg_2882" n="sc" s="T212">
               <ts e="T213" id="Seg_2884" n="e" s="T212">((BRK)). </ts>
            </ts>
            <ts e="T220" id="Seg_2885" n="sc" s="T214">
               <ts e="T215" id="Seg_2887" n="e" s="T214">Măn </ts>
               <ts e="T216" id="Seg_2889" n="e" s="T215">(iam) </ts>
               <ts e="T217" id="Seg_2891" n="e" s="T216">(sumna=) </ts>
               <ts e="T218" id="Seg_2893" n="e" s="T217">sumna </ts>
               <ts e="T219" id="Seg_2895" n="e" s="T218">nʼi </ts>
               <ts e="T220" id="Seg_2897" n="e" s="T219">dĭn. </ts>
            </ts>
            <ts e="T222" id="Seg_2898" n="sc" s="T221">
               <ts e="T222" id="Seg_2900" n="e" s="T221">Ой… </ts>
            </ts>
            <ts e="T224" id="Seg_2901" n="sc" s="T223">
               <ts e="T224" id="Seg_2903" n="e" s="T223">((BRK)). </ts>
            </ts>
            <ts e="T231" id="Seg_2904" n="sc" s="T225">
               <ts e="T226" id="Seg_2906" n="e" s="T225">Dĭn </ts>
               <ts e="T227" id="Seg_2908" n="e" s="T226">mobi </ts>
               <ts e="T228" id="Seg_2910" n="e" s="T227">nagur </ts>
               <ts e="T229" id="Seg_2912" n="e" s="T228">koʔbdo, </ts>
               <ts e="T230" id="Seg_2914" n="e" s="T229">šide </ts>
               <ts e="T231" id="Seg_2916" n="e" s="T230">nʼi. </ts>
            </ts>
            <ts e="T235" id="Seg_2917" n="sc" s="T232">
               <ts e="T233" id="Seg_2919" n="e" s="T232">Oʔb </ts>
               <ts e="T234" id="Seg_2921" n="e" s="T233">nʼi </ts>
               <ts e="T235" id="Seg_2923" n="e" s="T234">külambi. </ts>
            </ts>
            <ts e="T238" id="Seg_2924" n="sc" s="T236">
               <ts e="T237" id="Seg_2926" n="e" s="T236">I </ts>
               <ts e="T238" id="Seg_2928" n="e" s="T237">koʔbdo… </ts>
            </ts>
            <ts e="T244" id="Seg_2929" n="sc" s="T239">
               <ts e="T240" id="Seg_2931" n="e" s="T239">Вот </ts>
               <ts e="T241" id="Seg_2933" n="e" s="T240">память </ts>
               <ts e="T242" id="Seg_2935" n="e" s="T241">вишь </ts>
               <ts e="T243" id="Seg_2937" n="e" s="T242">какая </ts>
               <ts e="T244" id="Seg_2939" n="e" s="T243">дурная. </ts>
            </ts>
            <ts e="T248" id="Seg_2940" n="sc" s="T245">
               <ts e="T246" id="Seg_2942" n="e" s="T245">Не </ts>
               <ts e="T247" id="Seg_2944" n="e" s="T246">(гово-) </ts>
               <ts e="T248" id="Seg_2946" n="e" s="T247">((BRK)). </ts>
            </ts>
            <ts e="T252" id="Seg_2947" n="sc" s="T249">
               <ts e="T250" id="Seg_2949" n="e" s="T249">Šide </ts>
               <ts e="T251" id="Seg_2951" n="e" s="T250">koʔbdo </ts>
               <ts e="T252" id="Seg_2953" n="e" s="T251">külambi. </ts>
            </ts>
            <ts e="T256" id="Seg_2954" n="sc" s="T253">
               <ts e="T254" id="Seg_2956" n="e" s="T253">A </ts>
               <ts e="T255" id="Seg_2958" n="e" s="T254">oʔb </ts>
               <ts e="T256" id="Seg_2960" n="e" s="T255">dʼok. </ts>
            </ts>
            <ts e="T262" id="Seg_2961" n="sc" s="T257">
               <ts e="T258" id="Seg_2963" n="e" s="T257">Tei </ts>
               <ts e="T259" id="Seg_2965" n="e" s="T258">oʔb </ts>
               <ts e="T260" id="Seg_2967" n="e" s="T259">nʼi, </ts>
               <ts e="T261" id="Seg_2969" n="e" s="T260">oʔb </ts>
               <ts e="T262" id="Seg_2971" n="e" s="T261">koʔbdo. </ts>
            </ts>
            <ts e="T266" id="Seg_2972" n="sc" s="T263">
               <ts e="T264" id="Seg_2974" n="e" s="T263">O, </ts>
               <ts e="T265" id="Seg_2976" n="e" s="T264">iam </ts>
               <ts e="T266" id="Seg_2978" n="e" s="T265">tăŋ… </ts>
            </ts>
            <ts e="T272" id="Seg_2979" n="sc" s="T267">
               <ts e="T268" id="Seg_2981" n="e" s="T267">"Плакала" </ts>
               <ts e="T269" id="Seg_2983" n="e" s="T268">не </ts>
               <ts e="T270" id="Seg_2985" n="e" s="T269">могу </ts>
               <ts e="T271" id="Seg_2987" n="e" s="T270">выговорить </ts>
               <ts e="T272" id="Seg_2989" n="e" s="T271">никак. </ts>
            </ts>
            <ts e="T277" id="Seg_2990" n="sc" s="T273">
               <ts e="T274" id="Seg_2992" n="e" s="T273">Что </ts>
               <ts e="T275" id="Seg_2994" n="e" s="T274">об </ts>
               <ts e="T276" id="Seg_2996" n="e" s="T275">дитях </ts>
               <ts e="T277" id="Seg_2998" n="e" s="T276">плакала. </ts>
            </ts>
            <ts e="T279" id="Seg_2999" n="sc" s="T278">
               <ts e="T279" id="Seg_3001" n="e" s="T278">((BRK)). </ts>
            </ts>
            <ts e="T284" id="Seg_3002" n="sc" s="T280">
               <ts e="T281" id="Seg_3004" n="e" s="T280">Amnoʔ </ts>
               <ts e="T282" id="Seg_3006" n="e" s="T281">sarɨ </ts>
               <ts e="T283" id="Seg_3008" n="e" s="T282">su </ts>
               <ts e="T284" id="Seg_3010" n="e" s="T283">amzittə. </ts>
            </ts>
            <ts e="T289" id="Seg_3011" n="sc" s="T285">
               <ts e="T286" id="Seg_3013" n="e" s="T285">Nu, </ts>
               <ts e="T287" id="Seg_3015" n="e" s="T286">măn </ts>
               <ts e="T288" id="Seg_3017" n="e" s="T287">только-только </ts>
               <ts e="T289" id="Seg_3019" n="e" s="T288">amorbiam. </ts>
            </ts>
            <ts e="T292" id="Seg_3020" n="sc" s="T290">
               <ts e="T291" id="Seg_3022" n="e" s="T290">Nu, </ts>
               <ts e="T292" id="Seg_3024" n="e" s="T291">uja. </ts>
            </ts>
            <ts e="T295" id="Seg_3025" n="sc" s="T293">
               <ts e="T294" id="Seg_3027" n="e" s="T293">Uja </ts>
               <ts e="T295" id="Seg_3029" n="e" s="T294">jakše. </ts>
            </ts>
            <ts e="T298" id="Seg_3030" n="sc" s="T296">
               <ts e="T297" id="Seg_3032" n="e" s="T296">Uja, </ts>
               <ts e="T298" id="Seg_3034" n="e" s="T297">ipek. </ts>
            </ts>
            <ts e="T300" id="Seg_3035" n="sc" s="T299">
               <ts e="T300" id="Seg_3037" n="e" s="T299">Amzittə. </ts>
            </ts>
            <ts e="T305" id="Seg_3038" n="sc" s="T301">
               <ts e="T302" id="Seg_3040" n="e" s="T301">Nu, </ts>
               <ts e="T303" id="Seg_3042" n="e" s="T302">măn </ts>
               <ts e="T304" id="Seg_3044" n="e" s="T303">tolʼkă </ts>
               <ts e="T305" id="Seg_3046" n="e" s="T304">amorbiam. </ts>
            </ts>
            <ts e="T309" id="Seg_3047" n="sc" s="T306">
               <ts e="T307" id="Seg_3049" n="e" s="T306">Ну </ts>
               <ts e="T308" id="Seg_3051" n="e" s="T307">дак </ts>
               <ts e="T309" id="Seg_3053" n="e" s="T308">что. </ts>
            </ts>
            <ts e="T313" id="Seg_3054" n="sc" s="T310">
               <ts e="T311" id="Seg_3056" n="e" s="T310">Amorbiam, </ts>
               <ts e="T312" id="Seg_3058" n="e" s="T311">amorbəʔ </ts>
               <ts e="T313" id="Seg_3060" n="e" s="T312">mănziʔ. </ts>
            </ts>
            <ts e="T316" id="Seg_3061" n="sc" s="T314">
               <ts e="T315" id="Seg_3063" n="e" s="T314">Dö </ts>
               <ts e="T316" id="Seg_3065" n="e" s="T315">ipek. </ts>
            </ts>
            <ts e="T319" id="Seg_3066" n="sc" s="T317">
               <ts e="T318" id="Seg_3068" n="e" s="T317">Dö </ts>
               <ts e="T319" id="Seg_3070" n="e" s="T318">uja. </ts>
            </ts>
            <ts e="T323" id="Seg_3071" n="sc" s="T320">
               <ts e="T321" id="Seg_3073" n="e" s="T320">(Am-) </ts>
               <ts e="T322" id="Seg_3075" n="e" s="T321">Amorzittə </ts>
               <ts e="T323" id="Seg_3077" n="e" s="T322">možnă. </ts>
            </ts>
            <ts e="T326" id="Seg_3078" n="sc" s="T324">
               <ts e="T325" id="Seg_3080" n="e" s="T324">Ну </ts>
               <ts e="T326" id="Seg_3082" n="e" s="T325">вот. </ts>
            </ts>
            <ts e="T328" id="Seg_3083" n="sc" s="T327">
               <ts e="T328" id="Seg_3085" n="e" s="T327">((BRK)). </ts>
            </ts>
            <ts e="T332" id="Seg_3086" n="sc" s="T329">
               <ts e="T330" id="Seg_3088" n="e" s="T329">Šoʔ </ts>
               <ts e="T331" id="Seg_3090" n="e" s="T330">döbər, </ts>
               <ts e="T332" id="Seg_3092" n="e" s="T331">măna. </ts>
            </ts>
            <ts e="T335" id="Seg_3093" n="sc" s="T333">
               <ts e="T334" id="Seg_3095" n="e" s="T333">A </ts>
               <ts e="T335" id="Seg_3097" n="e" s="T334">ĭmbi? </ts>
            </ts>
            <ts e="T338" id="Seg_3098" n="sc" s="T336">
               <ts e="T337" id="Seg_3100" n="e" s="T336">Ĭmbi-nʼibudʼ </ts>
               <ts e="T338" id="Seg_3102" n="e" s="T337">nörbəlal. </ts>
            </ts>
            <ts e="T344" id="Seg_3103" n="sc" s="T339">
               <ts e="T340" id="Seg_3105" n="e" s="T339">Dʼok, </ts>
               <ts e="T341" id="Seg_3107" n="e" s="T340">(da) </ts>
               <ts e="T342" id="Seg_3109" n="e" s="T341">tăn </ts>
               <ts e="T343" id="Seg_3111" n="e" s="T342">nörbaʔ </ts>
               <ts e="T344" id="Seg_3113" n="e" s="T343">măna. </ts>
            </ts>
            <ts e="T350" id="Seg_3114" n="sc" s="T345">
               <ts e="T346" id="Seg_3116" n="e" s="T345">A </ts>
               <ts e="T347" id="Seg_3118" n="e" s="T346">măn </ts>
               <ts e="T348" id="Seg_3120" n="e" s="T347">ĭmbidə </ts>
               <ts e="T349" id="Seg_3122" n="e" s="T348">ej </ts>
               <ts e="T350" id="Seg_3124" n="e" s="T349">tĭmnem. </ts>
            </ts>
            <ts e="T355" id="Seg_3125" n="sc" s="T351">
               <ts e="T352" id="Seg_3127" n="e" s="T351">Moʔ </ts>
               <ts e="T353" id="Seg_3129" n="e" s="T352">dere </ts>
               <ts e="T354" id="Seg_3131" n="e" s="T353">ĭmbidə </ts>
               <ts e="T355" id="Seg_3133" n="e" s="T354">ej… </ts>
            </ts>
            <ts e="T359" id="Seg_3134" n="sc" s="T356">
               <ts e="T357" id="Seg_3136" n="e" s="T356">A </ts>
               <ts e="T358" id="Seg_3138" n="e" s="T357">măn </ts>
               <ts e="T359" id="Seg_3140" n="e" s="T358">nörbəlam. </ts>
            </ts>
            <ts e="T363" id="Seg_3141" n="sc" s="T360">
               <ts e="T361" id="Seg_3143" n="e" s="T360">Tăn </ts>
               <ts e="T362" id="Seg_3145" n="e" s="T361">jakše </ts>
               <ts e="T363" id="Seg_3147" n="e" s="T362">kuza. </ts>
            </ts>
            <ts e="T370" id="Seg_3148" n="sc" s="T364">
               <ts e="T365" id="Seg_3150" n="e" s="T364">A </ts>
               <ts e="T366" id="Seg_3152" n="e" s="T365">măn </ts>
               <ts e="T367" id="Seg_3154" n="e" s="T366">bile. </ts>
               <ts e="T368" id="Seg_3156" n="e" s="T367">Dʼok, </ts>
               <ts e="T369" id="Seg_3158" n="e" s="T368">tăn </ts>
               <ts e="T370" id="Seg_3160" n="e" s="T369">jakše. </ts>
            </ts>
            <ts e="T374" id="Seg_3161" n="sc" s="T371">
               <ts e="T372" id="Seg_3163" n="e" s="T371">Moʔ </ts>
               <ts e="T373" id="Seg_3165" n="e" s="T372">dereʔ </ts>
               <ts e="T374" id="Seg_3167" n="e" s="T373">nörbəlal? </ts>
            </ts>
            <ts e="T377" id="Seg_3168" n="sc" s="T375">
               <ts e="T376" id="Seg_3170" n="e" s="T375">Bile, </ts>
               <ts e="T377" id="Seg_3172" n="e" s="T376">bile. </ts>
            </ts>
            <ts e="T381" id="Seg_3173" n="sc" s="T378">
               <ts e="T379" id="Seg_3175" n="e" s="T378">Dere </ts>
               <ts e="T380" id="Seg_3177" n="e" s="T379">ej </ts>
               <ts e="T381" id="Seg_3179" n="e" s="T380">nörbaʔ. </ts>
            </ts>
            <ts e="T383" id="Seg_3180" n="sc" s="T382">
               <ts e="T383" id="Seg_3182" n="e" s="T382">Bile. </ts>
            </ts>
            <ts e="T387" id="Seg_3183" n="sc" s="T384">
               <ts e="T385" id="Seg_3185" n="e" s="T384">Nadă </ts>
               <ts e="T386" id="Seg_3187" n="e" s="T385">nörbəzittə </ts>
               <ts e="T387" id="Seg_3189" n="e" s="T386">jakše. </ts>
            </ts>
            <ts e="T389" id="Seg_3190" n="sc" s="T388">
               <ts e="T389" id="Seg_3192" n="e" s="T388">Kuza. </ts>
            </ts>
            <ts e="T396" id="Seg_3193" n="sc" s="T390">
               <ts e="T391" id="Seg_3195" n="e" s="T390">A </ts>
               <ts e="T392" id="Seg_3197" n="e" s="T391">tăn </ts>
               <ts e="T393" id="Seg_3199" n="e" s="T392">üge </ts>
               <ts e="T394" id="Seg_3201" n="e" s="T393">ĭmbidə </ts>
               <ts e="T395" id="Seg_3203" n="e" s="T394">ej </ts>
               <ts e="T396" id="Seg_3205" n="e" s="T395">nörbəlal. </ts>
            </ts>
            <ts e="T398" id="Seg_3206" n="sc" s="T397">
               <ts e="T398" id="Seg_3208" n="e" s="T397">Вот. </ts>
            </ts>
            <ts e="T400" id="Seg_3209" n="sc" s="T399">
               <ts e="T400" id="Seg_3211" n="e" s="T399">((BRK)). </ts>
            </ts>
            <ts e="T404" id="Seg_3212" n="sc" s="T401">
               <ts e="T402" id="Seg_3214" n="e" s="T401">Kajdɨ </ts>
               <ts e="T403" id="Seg_3216" n="e" s="T402">dʼestek </ts>
               <ts e="T404" id="Seg_3218" n="e" s="T403">agɨlgazɨŋ? </ts>
            </ts>
            <ts e="T407" id="Seg_3219" n="sc" s="T405">
               <ts e="T406" id="Seg_3221" n="e" s="T405">Kara </ts>
               <ts e="T407" id="Seg_3223" n="e" s="T406">dʼestek. </ts>
            </ts>
            <ts e="T411" id="Seg_3224" n="sc" s="T408">
               <ts e="T409" id="Seg_3226" n="e" s="T408">A </ts>
               <ts e="T410" id="Seg_3228" n="e" s="T409">kɨzɨ </ts>
               <ts e="T411" id="Seg_3230" n="e" s="T410">dʼestek? </ts>
            </ts>
            <ts e="T415" id="Seg_3231" n="sc" s="T412">
               <ts e="T413" id="Seg_3233" n="e" s="T412">Agɨlgam </ts>
               <ts e="T414" id="Seg_3235" n="e" s="T413">kɨzɨ </ts>
               <ts e="T415" id="Seg_3237" n="e" s="T414">dʼestek. </ts>
            </ts>
            <ts e="T417" id="Seg_3238" n="sc" s="T416">
               <ts e="T417" id="Seg_3240" n="e" s="T416">Agɨlgam. </ts>
            </ts>
            <ts e="T421" id="Seg_3241" n="sc" s="T418">
               <ts e="T419" id="Seg_3243" n="e" s="T418">Nada </ts>
               <ts e="T420" id="Seg_3245" n="e" s="T419">ipek </ts>
               <ts e="T421" id="Seg_3247" n="e" s="T420">vɨžɨrarga. </ts>
            </ts>
            <ts e="T424" id="Seg_3248" n="sc" s="T422">
               <ts e="T423" id="Seg_3250" n="e" s="T422">Kajdɨ </ts>
               <ts e="T424" id="Seg_3252" n="e" s="T423">ipek? </ts>
            </ts>
            <ts e="T428" id="Seg_3253" n="sc" s="T425">
               <ts e="T426" id="Seg_3255" n="e" s="T425">Da </ts>
               <ts e="T427" id="Seg_3257" n="e" s="T426">kara </ts>
               <ts e="T428" id="Seg_3259" n="e" s="T427">ipek. </ts>
            </ts>
            <ts e="T433" id="Seg_3260" n="sc" s="T429">
               <ts e="T430" id="Seg_3262" n="e" s="T429">Dʼok </ts>
               <ts e="T431" id="Seg_3264" n="e" s="T430">kara </ts>
               <ts e="T432" id="Seg_3266" n="e" s="T431">ipek </ts>
               <ts e="T433" id="Seg_3268" n="e" s="T432">dʼabal. </ts>
            </ts>
            <ts e="T438" id="Seg_3269" n="sc" s="T434">
               <ts e="T435" id="Seg_3271" n="e" s="T434">Nada </ts>
               <ts e="T436" id="Seg_3273" n="e" s="T435">jakše </ts>
               <ts e="T437" id="Seg_3275" n="e" s="T436">ipek </ts>
               <ts e="T438" id="Seg_3277" n="e" s="T437">bɨžɨrarga. </ts>
            </ts>
            <ts e="T443" id="Seg_3278" n="sc" s="T439">
               <ts e="T440" id="Seg_3280" n="e" s="T439">(Turgus) </ts>
               <ts e="T441" id="Seg_3282" n="e" s="T440">sarɨ </ts>
               <ts e="T442" id="Seg_3284" n="e" s="T441">su </ts>
               <ts e="T443" id="Seg_3286" n="e" s="T442">ičerbɨs. </ts>
            </ts>
            <ts e="T446" id="Seg_3287" n="sc" s="T444">
               <ts e="T445" id="Seg_3289" n="e" s="T444">Măn </ts>
               <ts e="T446" id="Seg_3291" n="e" s="T445">ičerbɨm. </ts>
            </ts>
            <ts e="T448" id="Seg_3292" n="sc" s="T447">
               <ts e="T448" id="Seg_3294" n="e" s="T447">Вот. </ts>
            </ts>
            <ts e="T450" id="Seg_3295" n="sc" s="T449">
               <ts e="T450" id="Seg_3297" n="e" s="T449">((BRK)). </ts>
            </ts>
            <ts e="T454" id="Seg_3298" n="sc" s="T451">
               <ts e="T452" id="Seg_3300" n="e" s="T451">Măn </ts>
               <ts e="T453" id="Seg_3302" n="e" s="T452">ulum </ts>
               <ts e="T454" id="Seg_3304" n="e" s="T453">ĭzemniet. </ts>
            </ts>
            <ts e="T459" id="Seg_3305" n="sc" s="T455">
               <ts e="T456" id="Seg_3307" n="e" s="T455">Tej </ts>
               <ts e="T457" id="Seg_3309" n="e" s="T456">măn </ts>
               <ts e="T458" id="Seg_3311" n="e" s="T457">ulum </ts>
               <ts e="T459" id="Seg_3313" n="e" s="T458">jakše. </ts>
            </ts>
            <ts e="T461" id="Seg_3314" n="sc" s="T460">
               <ts e="T461" id="Seg_3316" n="e" s="T460">((BRK)). </ts>
            </ts>
            <ts e="T464" id="Seg_3317" n="sc" s="T462">
               <ts e="T463" id="Seg_3319" n="e" s="T462">Gibər </ts>
               <ts e="T464" id="Seg_3321" n="e" s="T463">(alləl)? </ts>
            </ts>
            <ts e="T466" id="Seg_3322" n="sc" s="T465">
               <ts e="T466" id="Seg_3324" n="e" s="T465">Šoləm. </ts>
            </ts>
            <ts e="T469" id="Seg_3325" n="sc" s="T467">
               <ts e="T468" id="Seg_3327" n="e" s="T467">Mo </ts>
               <ts e="T469" id="Seg_3329" n="e" s="T468">dereʔ? </ts>
            </ts>
            <ts e="T472" id="Seg_3330" n="sc" s="T470">
               <ts e="T471" id="Seg_3332" n="e" s="T470">Dak </ts>
               <ts e="T472" id="Seg_3334" n="e" s="T471">ĭmbi? </ts>
            </ts>
            <ts e="T474" id="Seg_3335" n="sc" s="T473">
               <ts e="T474" id="Seg_3337" n="e" s="T473">Šoʔ. </ts>
            </ts>
            <ts e="T476" id="Seg_3338" n="sc" s="T475">
               <ts e="T476" id="Seg_3340" n="e" s="T475">Dʼok! </ts>
            </ts>
            <ts e="T480" id="Seg_3341" n="sc" s="T477">
               <ts e="T478" id="Seg_3343" n="e" s="T477">Măn </ts>
               <ts e="T479" id="Seg_3345" n="e" s="T478">ari </ts>
               <ts e="T480" id="Seg_3347" n="e" s="T479">dʼürləm. </ts>
            </ts>
            <ts e="T483" id="Seg_3348" n="sc" s="T481">
               <ts e="T482" id="Seg_3350" n="e" s="T481">Ari </ts>
               <ts e="T483" id="Seg_3352" n="e" s="T482">dʼürləŋ! </ts>
            </ts>
            <ts e="T485" id="Seg_3353" n="sc" s="T484">
               <ts e="T485" id="Seg_3355" n="e" s="T484">Tăn… </ts>
            </ts>
            <ts e="T487" id="Seg_3356" n="sc" s="T486">
               <ts e="T487" id="Seg_3358" n="e" s="T486">A? </ts>
            </ts>
            <ts e="T493" id="Seg_3359" n="sc" s="T488">
               <ts e="T489" id="Seg_3361" n="e" s="T488">Tăn </ts>
               <ts e="T490" id="Seg_3363" n="e" s="T489">bar, </ts>
               <ts e="T491" id="Seg_3365" n="e" s="T490">(gĭda) </ts>
               <ts e="T492" id="Seg_3367" n="e" s="T491">bar </ts>
               <ts e="T493" id="Seg_3369" n="e" s="T492">moləl. </ts>
            </ts>
            <ts e="T500" id="Seg_3370" n="sc" s="T494">
               <ts e="T495" id="Seg_3372" n="e" s="T494">Nu, </ts>
               <ts e="T496" id="Seg_3374" n="e" s="T495">a </ts>
               <ts e="T497" id="Seg_3376" n="e" s="T496">tăn </ts>
               <ts e="T498" id="Seg_3378" n="e" s="T497">dak, </ts>
               <ts e="T499" id="Seg_3380" n="e" s="T498">ej </ts>
               <ts e="T500" id="Seg_3382" n="e" s="T499">molal. </ts>
            </ts>
            <ts e="T502" id="Seg_3383" n="sc" s="T501">
               <ts e="T502" id="Seg_3385" n="e" s="T501">((BRK)). </ts>
            </ts>
            <ts e="T507" id="Seg_3386" n="sc" s="T503">
               <ts e="T504" id="Seg_3388" n="e" s="T503">Măn </ts>
               <ts e="T505" id="Seg_3390" n="e" s="T504">(tüžöj) </ts>
               <ts e="T506" id="Seg_3392" n="e" s="T505">jakše </ts>
               <ts e="T507" id="Seg_3394" n="e" s="T506">tüžöj. </ts>
            </ts>
            <ts e="T512" id="Seg_3395" n="sc" s="T508">
               <ts e="T509" id="Seg_3397" n="e" s="T508">I </ts>
               <ts e="T510" id="Seg_3399" n="e" s="T509">süt </ts>
               <ts e="T511" id="Seg_3401" n="e" s="T510">măna </ts>
               <ts e="T512" id="Seg_3403" n="e" s="T511">mĭliet. </ts>
            </ts>
            <ts e="T516" id="Seg_3404" n="sc" s="T513">
               <ts e="T514" id="Seg_3406" n="e" s="T513">Süt </ts>
               <ts e="T515" id="Seg_3408" n="e" s="T514">măna </ts>
               <ts e="T516" id="Seg_3410" n="e" s="T515">mĭliet. </ts>
            </ts>
            <ts e="T518" id="Seg_3411" n="sc" s="T517">
               <ts e="T518" id="Seg_3413" n="e" s="T517">Jakše. </ts>
            </ts>
            <ts e="T522" id="Seg_3414" n="sc" s="T519">
               <ts e="T520" id="Seg_3416" n="e" s="T519">Gibər </ts>
               <ts e="T521" id="Seg_3418" n="e" s="T520">dĭ </ts>
               <ts e="T522" id="Seg_3420" n="e" s="T521">süt? </ts>
            </ts>
            <ts e="T529" id="Seg_3421" n="sc" s="T523">
               <ts e="T524" id="Seg_3423" n="e" s="T523">Da </ts>
               <ts e="T525" id="Seg_3425" n="e" s="T524">gibər, </ts>
               <ts e="T526" id="Seg_3427" n="e" s="T525">măn </ts>
               <ts e="T527" id="Seg_3429" n="e" s="T526">amorlam </ts>
               <ts e="T528" id="Seg_3431" n="e" s="T527">dö </ts>
               <ts e="T529" id="Seg_3433" n="e" s="T528">süt. </ts>
            </ts>
            <ts e="T531" id="Seg_3434" n="sc" s="T530">
               <ts e="T531" id="Seg_3436" n="e" s="T530">Вот. </ts>
            </ts>
            <ts e="T533" id="Seg_3437" n="sc" s="T532">
               <ts e="T533" id="Seg_3439" n="e" s="T532">((BRK)). </ts>
            </ts>
            <ts e="T550" id="Seg_3440" n="sc" s="T534">
               <ts e="T535" id="Seg_3442" n="e" s="T534">On </ts>
               <ts e="T536" id="Seg_3444" n="e" s="T535">bɨr, </ts>
               <ts e="T537" id="Seg_3446" n="e" s="T536">on </ts>
               <ts e="T538" id="Seg_3448" n="e" s="T537">ikɨ, </ts>
               <ts e="T539" id="Seg_3450" n="e" s="T538">on </ts>
               <ts e="T540" id="Seg_3452" n="e" s="T539">uš, </ts>
               <ts e="T541" id="Seg_3454" n="e" s="T540">on </ts>
               <ts e="T542" id="Seg_3456" n="e" s="T541">tort, </ts>
               <ts e="T543" id="Seg_3458" n="e" s="T542">on </ts>
               <ts e="T544" id="Seg_3460" n="e" s="T543">bʼeš, </ts>
               <ts e="T545" id="Seg_3462" n="e" s="T544">on </ts>
               <ts e="T546" id="Seg_3464" n="e" s="T545">altɨ, </ts>
               <ts e="T547" id="Seg_3466" n="e" s="T546">on </ts>
               <ts e="T548" id="Seg_3468" n="e" s="T547">sʼegɨs, </ts>
               <ts e="T549" id="Seg_3470" n="e" s="T548">on </ts>
               <ts e="T550" id="Seg_3472" n="e" s="T549">togus. </ts>
            </ts>
            <ts e="T554" id="Seg_3473" n="sc" s="T551">
               <ts e="T552" id="Seg_3475" n="e" s="T551">Вот </ts>
               <ts e="T553" id="Seg_3477" n="e" s="T552">и </ts>
               <ts e="T554" id="Seg_3479" n="e" s="T553">все. </ts>
            </ts>
            <ts e="T558" id="Seg_3480" n="sc" s="T555">
               <ts e="T556" id="Seg_3482" n="e" s="T555">Măn </ts>
               <ts e="T557" id="Seg_3484" n="e" s="T556">ari </ts>
               <ts e="T558" id="Seg_3486" n="e" s="T557">(dʼürləm). </ts>
            </ts>
            <ts e="T560" id="Seg_3487" n="sc" s="T559">
               <ts e="T560" id="Seg_3489" n="e" s="T559">Gibər? </ts>
            </ts>
            <ts e="T566" id="Seg_3490" n="sc" s="T561">
               <ts e="T562" id="Seg_3492" n="e" s="T561">O, </ts>
               <ts e="T563" id="Seg_3494" n="e" s="T562">gibər, </ts>
               <ts e="T564" id="Seg_3496" n="e" s="T563">gibər </ts>
               <ts e="T565" id="Seg_3498" n="e" s="T564">(a-) </ts>
               <ts e="T566" id="Seg_3500" n="e" s="T565">alləm. </ts>
            </ts>
            <ts e="T568" id="Seg_3501" n="sc" s="T567">
               <ts e="T568" id="Seg_3503" n="e" s="T567">Ĭmbi? </ts>
            </ts>
            <ts e="T573" id="Seg_3504" n="sc" s="T569">
               <ts e="T570" id="Seg_3506" n="e" s="T569">Da </ts>
               <ts e="T571" id="Seg_3508" n="e" s="T570">măn </ts>
               <ts e="T572" id="Seg_3510" n="e" s="T571">ipek </ts>
               <ts e="T573" id="Seg_3512" n="e" s="T572">naga. </ts>
            </ts>
            <ts e="T577" id="Seg_3513" n="sc" s="T574">
               <ts e="T575" id="Seg_3515" n="e" s="T574">Можеть </ts>
               <ts e="T576" id="Seg_3517" n="e" s="T575">ipek </ts>
               <ts e="T577" id="Seg_3519" n="e" s="T576">iləm. </ts>
            </ts>
            <ts e="T584" id="Seg_3520" n="sc" s="T578">
               <ts e="T579" id="Seg_3522" n="e" s="T578">A </ts>
               <ts e="T580" id="Seg_3524" n="e" s="T579">(dettə </ts>
               <ts e="T581" id="Seg_3526" n="e" s="T580">guda=) </ts>
               <ts e="T582" id="Seg_3528" n="e" s="T581">dettə </ts>
               <ts e="T583" id="Seg_3530" n="e" s="T582">gibər </ts>
               <ts e="T584" id="Seg_3532" n="e" s="T583">alləl? </ts>
            </ts>
            <ts e="T587" id="Seg_3533" n="sc" s="T585">
               <ts e="T586" id="Seg_3535" n="e" s="T585">Alləm </ts>
               <ts e="T587" id="Seg_3537" n="e" s="T586">gibər-nʼibudʼ. </ts>
            </ts>
            <ts e="T591" id="Seg_3538" n="sc" s="T588">
               <ts e="T589" id="Seg_3540" n="e" s="T588">Amnoləŋ </ts>
               <ts e="T590" id="Seg_3542" n="e" s="T589">i </ts>
               <ts e="T591" id="Seg_3544" n="e" s="T590">iləm. </ts>
            </ts>
            <ts e="T597" id="Seg_3545" n="sc" s="T592">
               <ts e="T593" id="Seg_3547" n="e" s="T592">(Bar </ts>
               <ts e="T594" id="Seg_3549" n="e" s="T593">ĭmbi=) </ts>
               <ts e="T595" id="Seg_3551" n="e" s="T594">bar </ts>
               <ts e="T596" id="Seg_3553" n="e" s="T595">(i-) </ts>
               <ts e="T597" id="Seg_3555" n="e" s="T596">iləm. </ts>
            </ts>
            <ts e="T602" id="Seg_3556" n="sc" s="T598">
               <ts e="T599" id="Seg_3558" n="e" s="T598">A </ts>
               <ts e="T600" id="Seg_3560" n="e" s="T599">moʔ </ts>
               <ts e="T601" id="Seg_3562" n="e" s="T600">dere </ts>
               <ts e="T602" id="Seg_3564" n="e" s="T601">moləl? </ts>
            </ts>
            <ts e="T607" id="Seg_3565" n="sc" s="T603">
               <ts e="T604" id="Seg_3567" n="e" s="T603">Dak </ts>
               <ts e="T605" id="Seg_3569" n="e" s="T604">măna </ts>
               <ts e="T606" id="Seg_3571" n="e" s="T605">bile </ts>
               <ts e="T607" id="Seg_3573" n="e" s="T606">amnozittə. </ts>
            </ts>
            <ts e="T612" id="Seg_3574" n="sc" s="T608">
               <ts e="T609" id="Seg_3576" n="e" s="T608">Nada </ts>
               <ts e="T610" id="Seg_3578" n="e" s="T609">anzittə </ts>
               <ts e="T611" id="Seg_3580" n="e" s="T610">bostə </ts>
               <ts e="T612" id="Seg_3582" n="e" s="T611">turanə. </ts>
            </ts>
            <ts e="T618" id="Seg_3583" n="sc" s="T613">
               <ts e="T614" id="Seg_3585" n="e" s="T613">A </ts>
               <ts e="T615" id="Seg_3587" n="e" s="T614">ĭmbi </ts>
               <ts e="T616" id="Seg_3589" n="e" s="T615">tura </ts>
               <ts e="T617" id="Seg_3591" n="e" s="T616">(mĭləl) </ts>
               <ts e="T618" id="Seg_3593" n="e" s="T617">tănan? </ts>
            </ts>
            <ts e="T621" id="Seg_3594" n="sc" s="T619">
               <ts e="T620" id="Seg_3596" n="e" s="T619">Ĭmbi-nʼibudʼ </ts>
               <ts e="T621" id="Seg_3598" n="e" s="T620">mĭləl. </ts>
            </ts>
            <ts e="T623" id="Seg_3599" n="sc" s="T622">
               <ts e="T623" id="Seg_3601" n="e" s="T622">Gibər? </ts>
            </ts>
            <ts e="T627" id="Seg_3602" n="sc" s="T624">
               <ts e="T625" id="Seg_3604" n="e" s="T624">A </ts>
               <ts e="T626" id="Seg_3606" n="e" s="T625">dettə </ts>
               <ts e="T627" id="Seg_3608" n="e" s="T626">gibər? </ts>
            </ts>
            <ts e="T631" id="Seg_3609" n="sc" s="T628">
               <ts e="T629" id="Seg_3611" n="e" s="T628">Nu, </ts>
               <ts e="T630" id="Seg_3613" n="e" s="T629">bos </ts>
               <ts e="T631" id="Seg_3615" n="e" s="T630">turanə. </ts>
            </ts>
            <ts e="T634" id="Seg_3616" n="sc" s="T632">
               <ts e="T633" id="Seg_3618" n="e" s="T632">Šoləm </ts>
               <ts e="T634" id="Seg_3620" n="e" s="T633">amnolapliem. </ts>
            </ts>
            <ts e="T636" id="Seg_3621" n="sc" s="T635">
               <ts e="T636" id="Seg_3623" n="e" s="T635">Вот. </ts>
            </ts>
            <ts e="T638" id="Seg_3624" n="sc" s="T637">
               <ts e="T638" id="Seg_3626" n="e" s="T637">((BRK)). </ts>
            </ts>
            <ts e="T643" id="Seg_3627" n="sc" s="T639">
               <ts e="T640" id="Seg_3629" n="e" s="T639">Dettə </ts>
               <ts e="T641" id="Seg_3631" n="e" s="T640">măna </ts>
               <ts e="T642" id="Seg_3633" n="e" s="T641">tamgu </ts>
               <ts e="T643" id="Seg_3635" n="e" s="T642">nʼeʔsittə. </ts>
            </ts>
            <ts e="T647" id="Seg_3636" n="sc" s="T644">
               <ts e="T645" id="Seg_3638" n="e" s="T644">Măn </ts>
               <ts e="T646" id="Seg_3640" n="e" s="T645">naga </ts>
               <ts e="T647" id="Seg_3642" n="e" s="T646">tamgu. </ts>
            </ts>
            <ts e="T652" id="Seg_3643" n="sc" s="T648">
               <ts e="T649" id="Seg_3645" n="e" s="T648">Ox, </ts>
               <ts e="T650" id="Seg_3647" n="e" s="T649">i </ts>
               <ts e="T651" id="Seg_3649" n="e" s="T650">măn </ts>
               <ts e="T652" id="Seg_3651" n="e" s="T651">naga. </ts>
            </ts>
            <ts e="T656" id="Seg_3652" n="sc" s="T653">
               <ts e="T654" id="Seg_3654" n="e" s="T653">Dʼok, </ts>
               <ts e="T655" id="Seg_3656" n="e" s="T654">ige, </ts>
               <ts e="T656" id="Seg_3658" n="e" s="T655">ige. </ts>
            </ts>
            <ts e="T659" id="Seg_3659" n="sc" s="T657">
               <ts e="T658" id="Seg_3661" n="e" s="T657">Dettə </ts>
               <ts e="T659" id="Seg_3663" n="e" s="T658">măna. </ts>
            </ts>
            <ts e="T663" id="Seg_3664" n="sc" s="T660">
               <ts e="T661" id="Seg_3666" n="e" s="T660">Măn </ts>
               <ts e="T662" id="Seg_3668" n="e" s="T661">naga </ts>
               <ts e="T663" id="Seg_3670" n="e" s="T662">tamgu. </ts>
            </ts>
            <ts e="T668" id="Seg_3671" n="sc" s="T664">
               <ts e="T665" id="Seg_3673" n="e" s="T664">A </ts>
               <ts e="T666" id="Seg_3675" n="e" s="T665">măn </ts>
               <ts e="T667" id="Seg_3677" n="e" s="T666">nʼeʔsittə </ts>
               <ts e="T668" id="Seg_3679" n="e" s="T667">molam. </ts>
            </ts>
            <ts e="T670" id="Seg_3680" n="sc" s="T669">
               <ts e="T670" id="Seg_3682" n="e" s="T669">((BRK)). </ts>
            </ts>
            <ts e="T675" id="Seg_3683" n="sc" s="T671">
               <ts e="T672" id="Seg_3685" n="e" s="T671">Šoʔ </ts>
               <ts e="T673" id="Seg_3687" n="e" s="T672">măna </ts>
               <ts e="T674" id="Seg_3689" n="e" s="T673">ara </ts>
               <ts e="T675" id="Seg_3691" n="e" s="T674">bĭʔsittə. </ts>
            </ts>
            <ts e="T681" id="Seg_3692" n="sc" s="T676">
               <ts e="T677" id="Seg_3694" n="e" s="T676">Da </ts>
               <ts e="T678" id="Seg_3696" n="e" s="T677">ara </ts>
               <ts e="T679" id="Seg_3698" n="e" s="T678">(bĭ-) </ts>
               <ts e="T680" id="Seg_3700" n="e" s="T679">bĭʔsittə </ts>
               <ts e="T681" id="Seg_3702" n="e" s="T680">nada. </ts>
            </ts>
            <ts e="T690" id="Seg_3703" n="sc" s="T682">
               <ts e="T683" id="Seg_3705" n="e" s="T682">Nu </ts>
               <ts e="T684" id="Seg_3707" n="e" s="T683">nada </ts>
               <ts e="T685" id="Seg_3709" n="e" s="T684">(g-) </ts>
               <ts e="T686" id="Seg_3711" n="e" s="T685">ara </ts>
               <ts e="T687" id="Seg_3713" n="e" s="T686">bĭʔsʼittə </ts>
               <ts e="T688" id="Seg_3715" n="e" s="T687">dak, </ts>
               <ts e="T689" id="Seg_3717" n="e" s="T688">ej </ts>
               <ts e="T690" id="Seg_3719" n="e" s="T689">dʼabroʔ. </ts>
            </ts>
            <ts e="T695" id="Seg_3720" n="sc" s="T691">
               <ts e="T692" id="Seg_3722" n="e" s="T691">Tăn </ts>
               <ts e="T693" id="Seg_3724" n="e" s="T692">vedʼ </ts>
               <ts e="T694" id="Seg_3726" n="e" s="T693">bile </ts>
               <ts e="T695" id="Seg_3728" n="e" s="T694">kuza! </ts>
            </ts>
            <ts e="T700" id="Seg_3729" n="sc" s="T696">
               <ts e="T697" id="Seg_3731" n="e" s="T696">A </ts>
               <ts e="T698" id="Seg_3733" n="e" s="T697">măn </ts>
               <ts e="T699" id="Seg_3735" n="e" s="T698">jakše </ts>
               <ts e="T700" id="Seg_3737" n="e" s="T699">kuza. </ts>
            </ts>
            <ts e="T708" id="Seg_3738" n="sc" s="T701">
               <ts e="T702" id="Seg_3740" n="e" s="T701">Măn </ts>
               <ts e="T703" id="Seg_3742" n="e" s="T702">ara </ts>
               <ts e="T704" id="Seg_3744" n="e" s="T703">(bĭ- </ts>
               <ts e="T705" id="Seg_3746" n="e" s="T704">bĭ-) </ts>
               <ts e="T706" id="Seg_3748" n="e" s="T705">bĭtliem — </ts>
               <ts e="T707" id="Seg_3750" n="e" s="T706">ej </ts>
               <ts e="T708" id="Seg_3752" n="e" s="T707">dʼabrolam. </ts>
            </ts>
            <ts e="T713" id="Seg_3753" n="sc" s="T709">
               <ts e="T710" id="Seg_3755" n="e" s="T709">A </ts>
               <ts e="T711" id="Seg_3757" n="e" s="T710">tăn </ts>
               <ts e="T712" id="Seg_3759" n="e" s="T711">üge </ts>
               <ts e="T713" id="Seg_3761" n="e" s="T712">(dʼabroliel). </ts>
            </ts>
            <ts e="T719" id="Seg_3762" n="sc" s="T714">
               <ts e="T715" id="Seg_3764" n="e" s="T714">(Šen) </ts>
               <ts e="T716" id="Seg_3766" n="e" s="T715">ej </ts>
               <ts e="T717" id="Seg_3768" n="e" s="T716">jakše </ts>
               <ts e="T718" id="Seg_3770" n="e" s="T717">dărə </ts>
               <ts e="T719" id="Seg_3772" n="e" s="T718">bĭʔsittə. </ts>
            </ts>
            <ts e="T725" id="Seg_3773" n="sc" s="T720">
               <ts e="T721" id="Seg_3775" n="e" s="T720">Bĭʔsittə </ts>
               <ts e="T722" id="Seg_3777" n="e" s="T721">nada, </ts>
               <ts e="T723" id="Seg_3779" n="e" s="T722">nʼe </ts>
               <ts e="T724" id="Seg_3781" n="e" s="T723">nada </ts>
               <ts e="T725" id="Seg_3783" n="e" s="T724">dʼabrozittə. </ts>
            </ts>
            <ts e="T728" id="Seg_3784" n="sc" s="T726">
               <ts e="T727" id="Seg_3786" n="e" s="T726">Nada </ts>
               <ts e="T728" id="Seg_3788" n="e" s="T727">jakše. </ts>
            </ts>
            <ts e="T734" id="Seg_3789" n="sc" s="T729">
               <ts e="T730" id="Seg_3791" n="e" s="T729">Măn </ts>
               <ts e="T731" id="Seg_3793" n="e" s="T730">iš </ts>
               <ts e="T732" id="Seg_3795" n="e" s="T731">(gĭrgit) </ts>
               <ts e="T733" id="Seg_3797" n="e" s="T732">kuvas </ts>
               <ts e="T734" id="Seg_3799" n="e" s="T733">kuza. </ts>
            </ts>
            <ts e="T736" id="Seg_3800" n="sc" s="T735">
               <ts e="T736" id="Seg_3802" n="e" s="T735">Вот. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-SAE">
            <ta e="T2" id="Seg_3803" s="T1">SAE_196X_SU0231.SAE.001 (001)</ta>
            <ta e="T7" id="Seg_3804" s="T5">SAE_196X_SU0231.SAE.002 (003)</ta>
            <ta e="T13" id="Seg_3805" s="T9">SAE_196X_SU0231.SAE.003 (005)</ta>
            <ta e="T16" id="Seg_3806" s="T14">SAE_196X_SU0231.SAE.004 (006)</ta>
            <ta e="T20" id="Seg_3807" s="T17">SAE_196X_SU0231.SAE.005 (007)</ta>
            <ta e="T26" id="Seg_3808" s="T21">SAE_196X_SU0231.SAE.006 (008)</ta>
            <ta e="T32" id="Seg_3809" s="T27">SAE_196X_SU0231.SAE.007 (009)</ta>
            <ta e="T36" id="Seg_3810" s="T33">SAE_196X_SU0231.SAE.008 (010)</ta>
            <ta e="T41" id="Seg_3811" s="T37">SAE_196X_SU0231.SAE.009 (011)</ta>
            <ta e="T45" id="Seg_3812" s="T42">SAE_196X_SU0231.SAE.010 (012)</ta>
            <ta e="T49" id="Seg_3813" s="T46">SAE_196X_SU0231.SAE.011 (013)</ta>
            <ta e="T52" id="Seg_3814" s="T50">SAE_196X_SU0231.SAE.012 (014)</ta>
            <ta e="T53" id="Seg_3815" s="T52">SAE_196X_SU0231.SAE.013 (015)</ta>
            <ta e="T60" id="Seg_3816" s="T54">SAE_196X_SU0231.SAE.014 (016)</ta>
            <ta e="T67" id="Seg_3817" s="T62">SAE_196X_SU0231.SAE.015 (018)</ta>
            <ta e="T70" id="Seg_3818" s="T68">SAE_196X_SU0231.SAE.016 (019)</ta>
            <ta e="T75" id="Seg_3819" s="T71">SAE_196X_SU0231.SAE.017 (020)</ta>
            <ta e="T79" id="Seg_3820" s="T76">SAE_196X_SU0231.SAE.018 (021)</ta>
            <ta e="T81" id="Seg_3821" s="T80">SAE_196X_SU0231.SAE.019 (022)</ta>
            <ta e="T83" id="Seg_3822" s="T82">SAE_196X_SU0231.SAE.020 (023)</ta>
            <ta e="T88" id="Seg_3823" s="T84">SAE_196X_SU0231.SAE.021 (024)</ta>
            <ta e="T92" id="Seg_3824" s="T89">SAE_196X_SU0231.SAE.022 (025)</ta>
            <ta e="T94" id="Seg_3825" s="T93">SAE_196X_SU0231.SAE.023 (026)</ta>
            <ta e="T98" id="Seg_3826" s="T95">SAE_196X_SU0231.SAE.024 (027)</ta>
            <ta e="T102" id="Seg_3827" s="T99">SAE_196X_SU0231.SAE.025 (028)</ta>
            <ta e="T108" id="Seg_3828" s="T103">SAE_196X_SU0231.SAE.026 (029)</ta>
            <ta e="T115" id="Seg_3829" s="T109">SAE_196X_SU0231.SAE.027 (030)</ta>
            <ta e="T118" id="Seg_3830" s="T116">SAE_196X_SU0231.SAE.028 (031)</ta>
            <ta e="T120" id="Seg_3831" s="T119">SAE_196X_SU0231.SAE.029 (032)</ta>
            <ta e="T122" id="Seg_3832" s="T120">SAE_196X_SU0231.SAE.030 (033)</ta>
            <ta e="T126" id="Seg_3833" s="T123">SAE_196X_SU0231.SAE.031 (034)</ta>
            <ta e="T129" id="Seg_3834" s="T127">SAE_196X_SU0231.SAE.032 (035)</ta>
            <ta e="T136" id="Seg_3835" s="T130">SAE_196X_SU0231.SAE.033 (036)</ta>
            <ta e="T138" id="Seg_3836" s="T137">SAE_196X_SU0231.SAE.034 (037)</ta>
            <ta e="T142" id="Seg_3837" s="T141">SAE_196X_SU0231.SAE.035 (039)</ta>
            <ta e="T147" id="Seg_3838" s="T143">SAE_196X_SU0231.SAE.036 (040)</ta>
            <ta e="T151" id="Seg_3839" s="T148">SAE_196X_SU0231.SAE.037 (041)</ta>
            <ta e="T153" id="Seg_3840" s="T152">SAE_196X_SU0231.SAE.038 (042)</ta>
            <ta e="T158" id="Seg_3841" s="T154">SAE_196X_SU0231.SAE.039 (043)</ta>
            <ta e="T162" id="Seg_3842" s="T159">SAE_196X_SU0231.SAE.040 (044)</ta>
            <ta e="T164" id="Seg_3843" s="T163">SAE_196X_SU0231.SAE.041 (045)</ta>
            <ta e="T168" id="Seg_3844" s="T165">SAE_196X_SU0231.SAE.042 (046)</ta>
            <ta e="T172" id="Seg_3845" s="T169">SAE_196X_SU0231.SAE.043 (047)</ta>
            <ta e="T174" id="Seg_3846" s="T173">SAE_196X_SU0231.SAE.044 (048)</ta>
            <ta e="T176" id="Seg_3847" s="T175">SAE_196X_SU0231.SAE.045 (049)</ta>
            <ta e="T183" id="Seg_3848" s="T177">SAE_196X_SU0231.SAE.046 (050)</ta>
            <ta e="T187" id="Seg_3849" s="T184">SAE_196X_SU0231.SAE.047 (051)</ta>
            <ta e="T191" id="Seg_3850" s="T188">SAE_196X_SU0231.SAE.048 (052)</ta>
            <ta e="T193" id="Seg_3851" s="T191">SAE_196X_SU0231.SAE.049 (053)</ta>
            <ta e="T199" id="Seg_3852" s="T194">SAE_196X_SU0231.SAE.050 (054)</ta>
            <ta e="T201" id="Seg_3853" s="T200">SAE_196X_SU0231.SAE.051 (055)</ta>
            <ta e="T204" id="Seg_3854" s="T202">SAE_196X_SU0231.SAE.052 (056)</ta>
            <ta e="T209" id="Seg_3855" s="T205">SAE_196X_SU0231.SAE.053 (057)</ta>
            <ta e="T211" id="Seg_3856" s="T210">SAE_196X_SU0231.SAE.054 (058)</ta>
            <ta e="T213" id="Seg_3857" s="T212">SAE_196X_SU0231.SAE.055 (059)</ta>
            <ta e="T220" id="Seg_3858" s="T214">SAE_196X_SU0231.SAE.056 (060)</ta>
            <ta e="T222" id="Seg_3859" s="T221">SAE_196X_SU0231.SAE.057 (061)</ta>
            <ta e="T224" id="Seg_3860" s="T223">SAE_196X_SU0231.SAE.058 (062)</ta>
            <ta e="T231" id="Seg_3861" s="T225">SAE_196X_SU0231.SAE.059 (063)</ta>
            <ta e="T235" id="Seg_3862" s="T232">SAE_196X_SU0231.SAE.060 (064)</ta>
            <ta e="T238" id="Seg_3863" s="T236">SAE_196X_SU0231.SAE.061 (065)</ta>
            <ta e="T244" id="Seg_3864" s="T239">SAE_196X_SU0231.SAE.062 (066)</ta>
            <ta e="T248" id="Seg_3865" s="T245">SAE_196X_SU0231.SAE.063 (067)</ta>
            <ta e="T252" id="Seg_3866" s="T249">SAE_196X_SU0231.SAE.064 (068)</ta>
            <ta e="T256" id="Seg_3867" s="T253">SAE_196X_SU0231.SAE.065 (069)</ta>
            <ta e="T262" id="Seg_3868" s="T257">SAE_196X_SU0231.SAE.066 (070)</ta>
            <ta e="T266" id="Seg_3869" s="T263">SAE_196X_SU0231.SAE.067 (071)</ta>
            <ta e="T272" id="Seg_3870" s="T267">SAE_196X_SU0231.SAE.068 (072)</ta>
            <ta e="T277" id="Seg_3871" s="T273">SAE_196X_SU0231.SAE.069 (073)</ta>
            <ta e="T279" id="Seg_3872" s="T278">SAE_196X_SU0231.SAE.070 (074)</ta>
            <ta e="T284" id="Seg_3873" s="T280">SAE_196X_SU0231.SAE.071 (075)</ta>
            <ta e="T289" id="Seg_3874" s="T285">SAE_196X_SU0231.SAE.072 (076)</ta>
            <ta e="T292" id="Seg_3875" s="T290">SAE_196X_SU0231.SAE.073 (077)</ta>
            <ta e="T295" id="Seg_3876" s="T293">SAE_196X_SU0231.SAE.074 (078)</ta>
            <ta e="T298" id="Seg_3877" s="T296">SAE_196X_SU0231.SAE.075 (079)</ta>
            <ta e="T300" id="Seg_3878" s="T299">SAE_196X_SU0231.SAE.076 (080)</ta>
            <ta e="T305" id="Seg_3879" s="T301">SAE_196X_SU0231.SAE.077 (081)</ta>
            <ta e="T309" id="Seg_3880" s="T306">SAE_196X_SU0231.SAE.078 (082)</ta>
            <ta e="T313" id="Seg_3881" s="T310">SAE_196X_SU0231.SAE.079 (083)</ta>
            <ta e="T316" id="Seg_3882" s="T314">SAE_196X_SU0231.SAE.080 (084)</ta>
            <ta e="T319" id="Seg_3883" s="T317">SAE_196X_SU0231.SAE.081 (085)</ta>
            <ta e="T323" id="Seg_3884" s="T320">SAE_196X_SU0231.SAE.082 (086)</ta>
            <ta e="T326" id="Seg_3885" s="T324">SAE_196X_SU0231.SAE.083 (087)</ta>
            <ta e="T328" id="Seg_3886" s="T327">SAE_196X_SU0231.SAE.084 (088)</ta>
            <ta e="T332" id="Seg_3887" s="T329">SAE_196X_SU0231.SAE.085 (089)</ta>
            <ta e="T335" id="Seg_3888" s="T333">SAE_196X_SU0231.SAE.086 (090)</ta>
            <ta e="T338" id="Seg_3889" s="T336">SAE_196X_SU0231.SAE.087 (091)</ta>
            <ta e="T344" id="Seg_3890" s="T339">SAE_196X_SU0231.SAE.088 (092)</ta>
            <ta e="T350" id="Seg_3891" s="T345">SAE_196X_SU0231.SAE.089 (093)</ta>
            <ta e="T355" id="Seg_3892" s="T351">SAE_196X_SU0231.SAE.090 (094)</ta>
            <ta e="T359" id="Seg_3893" s="T356">SAE_196X_SU0231.SAE.091 (095)</ta>
            <ta e="T363" id="Seg_3894" s="T360">SAE_196X_SU0231.SAE.092 (096)</ta>
            <ta e="T367" id="Seg_3895" s="T364">SAE_196X_SU0231.SAE.093 (097)</ta>
            <ta e="T370" id="Seg_3896" s="T367">SAE_196X_SU0231.SAE.094 (098)</ta>
            <ta e="T374" id="Seg_3897" s="T371">SAE_196X_SU0231.SAE.095 (099)</ta>
            <ta e="T377" id="Seg_3898" s="T375">SAE_196X_SU0231.SAE.096 (100)</ta>
            <ta e="T381" id="Seg_3899" s="T378">SAE_196X_SU0231.SAE.097 (101)</ta>
            <ta e="T383" id="Seg_3900" s="T382">SAE_196X_SU0231.SAE.098 (102)</ta>
            <ta e="T387" id="Seg_3901" s="T384">SAE_196X_SU0231.SAE.099 (103)</ta>
            <ta e="T389" id="Seg_3902" s="T388">SAE_196X_SU0231.SAE.100 (104)</ta>
            <ta e="T396" id="Seg_3903" s="T390">SAE_196X_SU0231.SAE.101 (105)</ta>
            <ta e="T398" id="Seg_3904" s="T397">SAE_196X_SU0231.SAE.102 (106)</ta>
            <ta e="T400" id="Seg_3905" s="T399">SAE_196X_SU0231.SAE.103 (107)</ta>
            <ta e="T404" id="Seg_3906" s="T401">SAE_196X_SU0231.SAE.104 (108)</ta>
            <ta e="T407" id="Seg_3907" s="T405">SAE_196X_SU0231.SAE.105 (109)</ta>
            <ta e="T411" id="Seg_3908" s="T408">SAE_196X_SU0231.SAE.106 (110)</ta>
            <ta e="T415" id="Seg_3909" s="T412">SAE_196X_SU0231.SAE.107 (111)</ta>
            <ta e="T417" id="Seg_3910" s="T416">SAE_196X_SU0231.SAE.108 (112)</ta>
            <ta e="T421" id="Seg_3911" s="T418">SAE_196X_SU0231.SAE.109 (113)</ta>
            <ta e="T424" id="Seg_3912" s="T422">SAE_196X_SU0231.SAE.110 (114)</ta>
            <ta e="T428" id="Seg_3913" s="T425">SAE_196X_SU0231.SAE.111 (115)</ta>
            <ta e="T433" id="Seg_3914" s="T429">SAE_196X_SU0231.SAE.112 (116)</ta>
            <ta e="T438" id="Seg_3915" s="T434">SAE_196X_SU0231.SAE.113 (117)</ta>
            <ta e="T443" id="Seg_3916" s="T439">SAE_196X_SU0231.SAE.114 (118)</ta>
            <ta e="T446" id="Seg_3917" s="T444">SAE_196X_SU0231.SAE.115 (119)</ta>
            <ta e="T448" id="Seg_3918" s="T447">SAE_196X_SU0231.SAE.116 (120)</ta>
            <ta e="T450" id="Seg_3919" s="T449">SAE_196X_SU0231.SAE.117 (121)</ta>
            <ta e="T454" id="Seg_3920" s="T451">SAE_196X_SU0231.SAE.118 (122)</ta>
            <ta e="T459" id="Seg_3921" s="T455">SAE_196X_SU0231.SAE.119 (123)</ta>
            <ta e="T461" id="Seg_3922" s="T460">SAE_196X_SU0231.SAE.120 (124)</ta>
            <ta e="T464" id="Seg_3923" s="T462">SAE_196X_SU0231.SAE.121 (125)</ta>
            <ta e="T466" id="Seg_3924" s="T465">SAE_196X_SU0231.SAE.122 (126)</ta>
            <ta e="T469" id="Seg_3925" s="T467">SAE_196X_SU0231.SAE.123 (127)</ta>
            <ta e="T472" id="Seg_3926" s="T470">SAE_196X_SU0231.SAE.124 (128)</ta>
            <ta e="T474" id="Seg_3927" s="T473">SAE_196X_SU0231.SAE.125 (129)</ta>
            <ta e="T476" id="Seg_3928" s="T475">SAE_196X_SU0231.SAE.126 (130)</ta>
            <ta e="T480" id="Seg_3929" s="T477">SAE_196X_SU0231.SAE.127 (131)</ta>
            <ta e="T483" id="Seg_3930" s="T481">SAE_196X_SU0231.SAE.128 (132)</ta>
            <ta e="T485" id="Seg_3931" s="T484">SAE_196X_SU0231.SAE.129 (133)</ta>
            <ta e="T487" id="Seg_3932" s="T486">SAE_196X_SU0231.SAE.130 (134)</ta>
            <ta e="T493" id="Seg_3933" s="T488">SAE_196X_SU0231.SAE.131 (135)</ta>
            <ta e="T500" id="Seg_3934" s="T494">SAE_196X_SU0231.SAE.132 (136)</ta>
            <ta e="T502" id="Seg_3935" s="T501">SAE_196X_SU0231.SAE.133 (137)</ta>
            <ta e="T507" id="Seg_3936" s="T503">SAE_196X_SU0231.SAE.134 (138)</ta>
            <ta e="T512" id="Seg_3937" s="T508">SAE_196X_SU0231.SAE.135 (139)</ta>
            <ta e="T516" id="Seg_3938" s="T513">SAE_196X_SU0231.SAE.136 (140)</ta>
            <ta e="T518" id="Seg_3939" s="T517">SAE_196X_SU0231.SAE.137 (141)</ta>
            <ta e="T522" id="Seg_3940" s="T519">SAE_196X_SU0231.SAE.138 (142)</ta>
            <ta e="T529" id="Seg_3941" s="T523">SAE_196X_SU0231.SAE.139 (143)</ta>
            <ta e="T531" id="Seg_3942" s="T530">SAE_196X_SU0231.SAE.140 (144)</ta>
            <ta e="T533" id="Seg_3943" s="T532">SAE_196X_SU0231.SAE.141 (145)</ta>
            <ta e="T550" id="Seg_3944" s="T534">SAE_196X_SU0231.SAE.142 (146)</ta>
            <ta e="T554" id="Seg_3945" s="T551">SAE_196X_SU0231.SAE.143 (147)</ta>
            <ta e="T558" id="Seg_3946" s="T555">SAE_196X_SU0231.SAE.144 (148)</ta>
            <ta e="T560" id="Seg_3947" s="T559">SAE_196X_SU0231.SAE.145 (149)</ta>
            <ta e="T566" id="Seg_3948" s="T561">SAE_196X_SU0231.SAE.146 (150)</ta>
            <ta e="T568" id="Seg_3949" s="T567">SAE_196X_SU0231.SAE.147 (151)</ta>
            <ta e="T573" id="Seg_3950" s="T569">SAE_196X_SU0231.SAE.148 (152)</ta>
            <ta e="T577" id="Seg_3951" s="T574">SAE_196X_SU0231.SAE.149 (153)</ta>
            <ta e="T584" id="Seg_3952" s="T578">SAE_196X_SU0231.SAE.150 (154)</ta>
            <ta e="T587" id="Seg_3953" s="T585">SAE_196X_SU0231.SAE.151 (155)</ta>
            <ta e="T591" id="Seg_3954" s="T588">SAE_196X_SU0231.SAE.152 (156)</ta>
            <ta e="T597" id="Seg_3955" s="T592">SAE_196X_SU0231.SAE.153 (157)</ta>
            <ta e="T602" id="Seg_3956" s="T598">SAE_196X_SU0231.SAE.154 (158)</ta>
            <ta e="T607" id="Seg_3957" s="T603">SAE_196X_SU0231.SAE.155 (159)</ta>
            <ta e="T612" id="Seg_3958" s="T608">SAE_196X_SU0231.SAE.156 (160)</ta>
            <ta e="T618" id="Seg_3959" s="T613">SAE_196X_SU0231.SAE.157 (161)</ta>
            <ta e="T621" id="Seg_3960" s="T619">SAE_196X_SU0231.SAE.158 (162)</ta>
            <ta e="T623" id="Seg_3961" s="T622">SAE_196X_SU0231.SAE.159 (163)</ta>
            <ta e="T627" id="Seg_3962" s="T624">SAE_196X_SU0231.SAE.160 (164)</ta>
            <ta e="T631" id="Seg_3963" s="T628">SAE_196X_SU0231.SAE.161 (165)</ta>
            <ta e="T634" id="Seg_3964" s="T632">SAE_196X_SU0231.SAE.162 (166)</ta>
            <ta e="T636" id="Seg_3965" s="T635">SAE_196X_SU0231.SAE.163 (167)</ta>
            <ta e="T638" id="Seg_3966" s="T637">SAE_196X_SU0231.SAE.164 (168)</ta>
            <ta e="T643" id="Seg_3967" s="T639">SAE_196X_SU0231.SAE.165 (169)</ta>
            <ta e="T647" id="Seg_3968" s="T644">SAE_196X_SU0231.SAE.166 (170)</ta>
            <ta e="T652" id="Seg_3969" s="T648">SAE_196X_SU0231.SAE.167 (171)</ta>
            <ta e="T656" id="Seg_3970" s="T653">SAE_196X_SU0231.SAE.168 (172)</ta>
            <ta e="T659" id="Seg_3971" s="T657">SAE_196X_SU0231.SAE.169 (173)</ta>
            <ta e="T663" id="Seg_3972" s="T660">SAE_196X_SU0231.SAE.170 (174)</ta>
            <ta e="T668" id="Seg_3973" s="T664">SAE_196X_SU0231.SAE.171 (175)</ta>
            <ta e="T670" id="Seg_3974" s="T669">SAE_196X_SU0231.SAE.172 (176)</ta>
            <ta e="T675" id="Seg_3975" s="T671">SAE_196X_SU0231.SAE.173 (177)</ta>
            <ta e="T681" id="Seg_3976" s="T676">SAE_196X_SU0231.SAE.174 (178)</ta>
            <ta e="T690" id="Seg_3977" s="T682">SAE_196X_SU0231.SAE.175 (179)</ta>
            <ta e="T695" id="Seg_3978" s="T691">SAE_196X_SU0231.SAE.176 (180)</ta>
            <ta e="T700" id="Seg_3979" s="T696">SAE_196X_SU0231.SAE.177 (181)</ta>
            <ta e="T708" id="Seg_3980" s="T701">SAE_196X_SU0231.SAE.178 (182)</ta>
            <ta e="T713" id="Seg_3981" s="T709">SAE_196X_SU0231.SAE.179 (183)</ta>
            <ta e="T719" id="Seg_3982" s="T714">SAE_196X_SU0231.SAE.180 (184)</ta>
            <ta e="T725" id="Seg_3983" s="T720">SAE_196X_SU0231.SAE.181 (185)</ta>
            <ta e="T728" id="Seg_3984" s="T726">SAE_196X_SU0231.SAE.182 (186)</ta>
            <ta e="T734" id="Seg_3985" s="T729">SAE_196X_SU0231.SAE.183 (187)</ta>
            <ta e="T736" id="Seg_3986" s="T735">SAE_196X_SU0231.SAE.184 (188)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-SAE">
            <ta e="T2" id="Seg_3987" s="T1">((BRK)). </ta>
            <ta e="T7" id="Seg_3988" s="T5">Говорить? </ta>
            <ta e="T13" id="Seg_3989" s="T9">Abam mo dereʔ nörbəlat. </ta>
            <ta e="T16" id="Seg_3990" s="T14">Šen bile. </ta>
            <ta e="T20" id="Seg_3991" s="T17">Šen bile nörbəlat. </ta>
            <ta e="T26" id="Seg_3992" s="T21">Mo dereʔ (ne-) tăn nörblat? </ta>
            <ta e="T32" id="Seg_3993" s="T27">Dʼok măn jakše nörblam tănan. </ta>
            <ta e="T36" id="Seg_3994" s="T33">Nen anaʔ dĭbər-tə. </ta>
            <ta e="T41" id="Seg_3995" s="T37">Nu, măn ej allam. </ta>
            <ta e="T45" id="Seg_3996" s="T42">Moʔ, dak moʔ. </ta>
            <ta e="T49" id="Seg_3997" s="T46">Măn udam naga. </ta>
            <ta e="T52" id="Seg_3998" s="T50">Ну вот. </ta>
            <ta e="T53" id="Seg_3999" s="T52">((BRK)). </ta>
            <ta e="T60" id="Seg_4000" s="T54">Ой, я беру в руки… </ta>
            <ta e="T67" id="Seg_4001" s="T62">Aba koško moʔ dereʔ tăn? </ta>
            <ta e="T70" id="Seg_4002" s="T68">Anaʔ dĭbər. </ta>
            <ta e="T75" id="Seg_4003" s="T71">Dʼok, măn ej allam. </ta>
            <ta e="T79" id="Seg_4004" s="T76">Măn udam bile. </ta>
            <ta e="T81" id="Seg_4005" s="T80">((BRK)). </ta>
            <ta e="T83" id="Seg_4006" s="T82">Можно? </ta>
            <ta e="T88" id="Seg_4007" s="T84">Kanžəbəj măna ara bĭʔsittə. </ta>
            <ta e="T92" id="Seg_4008" s="T89">Dʼok, ej allam. </ta>
            <ta e="T94" id="Seg_4009" s="T93">Moʔ? </ta>
            <ta e="T98" id="Seg_4010" s="T95">Kanžəbəj, ara kuvas. </ta>
            <ta e="T102" id="Seg_4011" s="T99">Kuvas (bĭs-) bĭsittə. </ta>
            <ta e="T108" id="Seg_4012" s="T103">Dʼok, măna (dĭ-) dĭgən münörlit. </ta>
            <ta e="T115" id="Seg_4013" s="T109">Dʼok, măn ej mĭləm tănan münörzittə. </ta>
            <ta e="T118" id="Seg_4014" s="T116">Nu alləm. </ta>
            <ta e="T120" id="Seg_4015" s="T119">Alləm. </ta>
            <ta e="T122" id="Seg_4016" s="T120">Ej mĭləl? </ta>
            <ta e="T126" id="Seg_4017" s="T123">Dʼok, ej mĭləm. </ta>
            <ta e="T129" id="Seg_4018" s="T127">Nu jakše. </ta>
            <ta e="T136" id="Seg_4019" s="T130">Jakše, ej mĭləl dak, măn alləm. </ta>
            <ta e="T138" id="Seg_4020" s="T137">((BRK)). </ta>
            <ta e="T142" id="Seg_4021" s="T141">Сейчас. </ta>
            <ta e="T147" id="Seg_4022" s="T143">Măn iamzi ej molam. </ta>
            <ta e="T151" id="Seg_4023" s="T148">Măn ari dʼürləm. </ta>
            <ta e="T153" id="Seg_4024" s="T152">Moʔ? </ta>
            <ta e="T158" id="Seg_4025" s="T154">Da dĭ kudonzla tăŋ. </ta>
            <ta e="T162" id="Seg_4026" s="T159">Nu, amnoʔ iazil. </ta>
            <ta e="T164" id="Seg_4027" s="T163">Amnoʔ. </ta>
            <ta e="T168" id="Seg_4028" s="T165">Moʔ dere molal? </ta>
            <ta e="T172" id="Seg_4029" s="T169">Tăn bila kuza. </ta>
            <ta e="T174" id="Seg_4030" s="T173">Вот. </ta>
            <ta e="T176" id="Seg_4031" s="T175">((BRK)). </ta>
            <ta e="T183" id="Seg_4032" s="T177">Tăn abal iam šen bila kuza. </ta>
            <ta e="T187" id="Seg_4033" s="T184">A măn jakše. </ta>
            <ta e="T191" id="Seg_4034" s="T188">Dʼok, măn jakše. </ta>
            <ta e="T193" id="Seg_4035" s="T191">Tăn bila. </ta>
            <ta e="T199" id="Seg_4036" s="T194">Oh, mo dereʔ (m-) măllal? </ta>
            <ta e="T201" id="Seg_4037" s="T200">Eneidənə. </ta>
            <ta e="T204" id="Seg_4038" s="T202">Mănuʔ jakše. </ta>
            <ta e="T209" id="Seg_4039" s="T205">Oh, tăŋ bila kuza. </ta>
            <ta e="T211" id="Seg_4040" s="T210">Eneidənə. </ta>
            <ta e="T213" id="Seg_4041" s="T212">((BRK)). </ta>
            <ta e="T220" id="Seg_4042" s="T214">Măn (iam) (sumna=) sumna nʼi dĭn. </ta>
            <ta e="T222" id="Seg_4043" s="T221">Ой… </ta>
            <ta e="T224" id="Seg_4044" s="T223">((BRK)). </ta>
            <ta e="T231" id="Seg_4045" s="T225">Dĭn mobi nagur koʔbdo, šide nʼi. </ta>
            <ta e="T235" id="Seg_4046" s="T232">Oʔb nʼi külambi. </ta>
            <ta e="T238" id="Seg_4047" s="T236">I koʔbdo… </ta>
            <ta e="T244" id="Seg_4048" s="T239">Вот память вишь какая дурная. </ta>
            <ta e="T248" id="Seg_4049" s="T245">Не (гово-) ((BRK)). </ta>
            <ta e="T252" id="Seg_4050" s="T249">Šide koʔbdo külambi. </ta>
            <ta e="T256" id="Seg_4051" s="T253">A oʔb dʼok. </ta>
            <ta e="T262" id="Seg_4052" s="T257">Tei oʔb nʼi, oʔb koʔbdo. </ta>
            <ta e="T266" id="Seg_4053" s="T263">O, iam tăŋ… </ta>
            <ta e="T272" id="Seg_4054" s="T267">"Плакала" не могу выговорить никак. </ta>
            <ta e="T277" id="Seg_4055" s="T273">Что об дитях плакала. </ta>
            <ta e="T279" id="Seg_4056" s="T278">((BRK)). </ta>
            <ta e="T284" id="Seg_4057" s="T280">Amnoʔ sarɨ su amzittə. </ta>
            <ta e="T289" id="Seg_4058" s="T285">Nu, măn только-только amorbiam. </ta>
            <ta e="T292" id="Seg_4059" s="T290">Nu, uja. </ta>
            <ta e="T295" id="Seg_4060" s="T293">Uja jakše. </ta>
            <ta e="T298" id="Seg_4061" s="T296">Uja, ipek. </ta>
            <ta e="T300" id="Seg_4062" s="T299">Amzittə. </ta>
            <ta e="T305" id="Seg_4063" s="T301">Nu, măn tolʼkă amorbiam. </ta>
            <ta e="T309" id="Seg_4064" s="T306">Ну дак что. </ta>
            <ta e="T313" id="Seg_4065" s="T310">Amorbiam, amorbəʔ mănziʔ. </ta>
            <ta e="T316" id="Seg_4066" s="T314">Dö ipek. </ta>
            <ta e="T319" id="Seg_4067" s="T317">Dö uja. </ta>
            <ta e="T323" id="Seg_4068" s="T320">(Am-) Amorzittə možnă. </ta>
            <ta e="T326" id="Seg_4069" s="T324">Ну вот. </ta>
            <ta e="T328" id="Seg_4070" s="T327">((BRK)). </ta>
            <ta e="T332" id="Seg_4071" s="T329">Šoʔ döbər, măna. </ta>
            <ta e="T335" id="Seg_4072" s="T333">A ĭmbi? </ta>
            <ta e="T338" id="Seg_4073" s="T336">Ĭmbi-nʼibudʼ nörbəlal. </ta>
            <ta e="T344" id="Seg_4074" s="T339">Dʼok, (da) tăn nörbaʔ măna. </ta>
            <ta e="T350" id="Seg_4075" s="T345">A măn ĭmbidə ej tĭmnem. </ta>
            <ta e="T355" id="Seg_4076" s="T351">Moʔ dere ĭmbidə ej… </ta>
            <ta e="T359" id="Seg_4077" s="T356">A măn nörbəlam. </ta>
            <ta e="T363" id="Seg_4078" s="T360">Tăn jakše kuza. </ta>
            <ta e="T367" id="Seg_4079" s="T364">A măn bile. </ta>
            <ta e="T370" id="Seg_4080" s="T367">Dʼok, tăn jakše. </ta>
            <ta e="T374" id="Seg_4081" s="T371">Moʔ dereʔ nörbəlal? </ta>
            <ta e="T377" id="Seg_4082" s="T375">Bile, bile. </ta>
            <ta e="T381" id="Seg_4083" s="T378">Dere ej nörbaʔ. </ta>
            <ta e="T383" id="Seg_4084" s="T382">Bile. </ta>
            <ta e="T387" id="Seg_4085" s="T384">Nadă nörbəzittə jakše. </ta>
            <ta e="T389" id="Seg_4086" s="T388">Kuza. </ta>
            <ta e="T396" id="Seg_4087" s="T390">A tăn üge ĭmbidə ej nörbəlal. </ta>
            <ta e="T398" id="Seg_4088" s="T397">Вот. </ta>
            <ta e="T400" id="Seg_4089" s="T399">((BRK)). </ta>
            <ta e="T404" id="Seg_4090" s="T401">Kajdɨ dʼestek agɨlgazɨŋ? </ta>
            <ta e="T407" id="Seg_4091" s="T405">Kara dʼestek. </ta>
            <ta e="T411" id="Seg_4092" s="T408">A kɨzɨ dʼestek? </ta>
            <ta e="T415" id="Seg_4093" s="T412">Agɨlgam kɨzɨ dʼestek. </ta>
            <ta e="T417" id="Seg_4094" s="T416">Agɨlgam. </ta>
            <ta e="T421" id="Seg_4095" s="T418">Nada ipek vɨžɨrarga. </ta>
            <ta e="T424" id="Seg_4096" s="T422">Kajdɨ ipek? </ta>
            <ta e="T428" id="Seg_4097" s="T425">Da kara ipek. </ta>
            <ta e="T433" id="Seg_4098" s="T429">Dʼok kara ipek dʼabal. </ta>
            <ta e="T438" id="Seg_4099" s="T434">Nada jakše ipek bɨžɨrarga. </ta>
            <ta e="T443" id="Seg_4100" s="T439">(Turgus) sarɨ su ičerbɨs. </ta>
            <ta e="T446" id="Seg_4101" s="T444">Măn ičerbɨm. </ta>
            <ta e="T448" id="Seg_4102" s="T447">Вот. </ta>
            <ta e="T450" id="Seg_4103" s="T449">((BRK)). </ta>
            <ta e="T454" id="Seg_4104" s="T451">Măn ulum ĭzemniet. </ta>
            <ta e="T459" id="Seg_4105" s="T455">Tej măn ulum jakše. </ta>
            <ta e="T461" id="Seg_4106" s="T460">((BRK)). </ta>
            <ta e="T464" id="Seg_4107" s="T462">Gibər (alləl)? </ta>
            <ta e="T466" id="Seg_4108" s="T465">Šoləm. </ta>
            <ta e="T469" id="Seg_4109" s="T467">Mo dereʔ? </ta>
            <ta e="T472" id="Seg_4110" s="T470">Dak ĭmbi? </ta>
            <ta e="T474" id="Seg_4111" s="T473">Šoʔ. </ta>
            <ta e="T476" id="Seg_4112" s="T475">Dʼok! </ta>
            <ta e="T480" id="Seg_4113" s="T477">Măn ari dʼürləm. </ta>
            <ta e="T483" id="Seg_4114" s="T481">Ari dʼürləŋ! </ta>
            <ta e="T485" id="Seg_4115" s="T484">Tăn… </ta>
            <ta e="T487" id="Seg_4116" s="T486">A? </ta>
            <ta e="T493" id="Seg_4117" s="T488">Tăn bar, (gĭda) bar moləl. </ta>
            <ta e="T500" id="Seg_4118" s="T494">Nu, a tăn dak, ej molal. </ta>
            <ta e="T502" id="Seg_4119" s="T501">((BRK)). </ta>
            <ta e="T507" id="Seg_4120" s="T503">Măn (tüžöj) jakše tüžöj. </ta>
            <ta e="T512" id="Seg_4121" s="T508">I süt măna mĭliet. </ta>
            <ta e="T516" id="Seg_4122" s="T513">Süt măna mĭliet. </ta>
            <ta e="T518" id="Seg_4123" s="T517">Jakše. </ta>
            <ta e="T522" id="Seg_4124" s="T519">Gibər dĭ süt? </ta>
            <ta e="T529" id="Seg_4125" s="T523">Da gibər, măn amorlam dö süt. </ta>
            <ta e="T531" id="Seg_4126" s="T530">Вот. </ta>
            <ta e="T533" id="Seg_4127" s="T532">((BRK)). </ta>
            <ta e="T550" id="Seg_4128" s="T534">On bɨr, on ikɨ, on uš, on tort, on bʼeš, on altɨ, on sʼegɨs, on togus. </ta>
            <ta e="T554" id="Seg_4129" s="T551">Вот и все. </ta>
            <ta e="T558" id="Seg_4130" s="T555">Măn ari (dʼürləm). </ta>
            <ta e="T560" id="Seg_4131" s="T559">Gibər? </ta>
            <ta e="T566" id="Seg_4132" s="T561">O, gibər, gibər (a-) alləm. </ta>
            <ta e="T568" id="Seg_4133" s="T567">Ĭmbi? </ta>
            <ta e="T573" id="Seg_4134" s="T569">Da măn ipek naga. </ta>
            <ta e="T577" id="Seg_4135" s="T574">Можеть ipek iləm. </ta>
            <ta e="T584" id="Seg_4136" s="T578">A (dettə guda=) dettə gibər alləl? </ta>
            <ta e="T587" id="Seg_4137" s="T585">Alləm gibər-nʼibudʼ. </ta>
            <ta e="T591" id="Seg_4138" s="T588">Amnoləŋ i iləm. </ta>
            <ta e="T597" id="Seg_4139" s="T592">(Bar ĭmbi=) bar (i-) iləm. </ta>
            <ta e="T602" id="Seg_4140" s="T598">A moʔ dere moləl? </ta>
            <ta e="T607" id="Seg_4141" s="T603">Dak măna bile amnozittə. </ta>
            <ta e="T612" id="Seg_4142" s="T608">Nada anzittə bostə turanə. </ta>
            <ta e="T618" id="Seg_4143" s="T613">A ĭmbi tura (mĭləl) tănan? </ta>
            <ta e="T621" id="Seg_4144" s="T619">Ĭmbi-nʼibudʼ mĭləl. </ta>
            <ta e="T623" id="Seg_4145" s="T622">Gibər? </ta>
            <ta e="T627" id="Seg_4146" s="T624">A dettə gibər? </ta>
            <ta e="T631" id="Seg_4147" s="T628">Nu, bos turanə. </ta>
            <ta e="T634" id="Seg_4148" s="T632">Šoləm amnolapliem. </ta>
            <ta e="T636" id="Seg_4149" s="T635">Вот. </ta>
            <ta e="T638" id="Seg_4150" s="T637">((BRK)). </ta>
            <ta e="T643" id="Seg_4151" s="T639">Dettə măna tamgu nʼeʔsittə. </ta>
            <ta e="T647" id="Seg_4152" s="T644">Măn naga tamgu. </ta>
            <ta e="T652" id="Seg_4153" s="T648">Ox, i măn naga. </ta>
            <ta e="T656" id="Seg_4154" s="T653">Dʼok, ige, ige. </ta>
            <ta e="T659" id="Seg_4155" s="T657">Dettə măna. </ta>
            <ta e="T663" id="Seg_4156" s="T660">Măn naga tamgu. </ta>
            <ta e="T668" id="Seg_4157" s="T664">A măn nʼeʔsittə molam. </ta>
            <ta e="T670" id="Seg_4158" s="T669">((BRK)). </ta>
            <ta e="T675" id="Seg_4159" s="T671">Šoʔ măna ara bĭʔsittə. </ta>
            <ta e="T681" id="Seg_4160" s="T676">Da ara (bĭ-) bĭʔsittə nada. </ta>
            <ta e="T690" id="Seg_4161" s="T682">Nu nada (g-) ara bĭʔsʼittə dak, ej dʼabroʔ. </ta>
            <ta e="T695" id="Seg_4162" s="T691">Tăn vedʼ bile kuza! </ta>
            <ta e="T700" id="Seg_4163" s="T696">A măn jakše kuza. </ta>
            <ta e="T708" id="Seg_4164" s="T701">Măn ara (bĭ- bĭ-) bĭtliem — ej dʼabrolam. </ta>
            <ta e="T713" id="Seg_4165" s="T709">A tăn üge (dʼabroliel). </ta>
            <ta e="T719" id="Seg_4166" s="T714">(Šen) ej jakše dărə bĭʔsittə. </ta>
            <ta e="T725" id="Seg_4167" s="T720">Bĭʔsittə nada, nʼe nada dʼabrozittə. </ta>
            <ta e="T728" id="Seg_4168" s="T726">Nada jakše. </ta>
            <ta e="T734" id="Seg_4169" s="T729">Măn iš (gĭrgit) kuvas kuza. </ta>
            <ta e="T736" id="Seg_4170" s="T735">Вот. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-SAE">
            <ta e="T10" id="Seg_4171" s="T9">aba-m</ta>
            <ta e="T11" id="Seg_4172" s="T10">mo</ta>
            <ta e="T12" id="Seg_4173" s="T11">dereʔ</ta>
            <ta e="T13" id="Seg_4174" s="T12">nör-bəl-a-t</ta>
            <ta e="T15" id="Seg_4175" s="T14">šen</ta>
            <ta e="T16" id="Seg_4176" s="T15">bile</ta>
            <ta e="T18" id="Seg_4177" s="T17">šen</ta>
            <ta e="T19" id="Seg_4178" s="T18">bile</ta>
            <ta e="T20" id="Seg_4179" s="T19">nör-bəl-a-t</ta>
            <ta e="T22" id="Seg_4180" s="T21">mo</ta>
            <ta e="T23" id="Seg_4181" s="T22">dereʔ</ta>
            <ta e="T25" id="Seg_4182" s="T24">tăn</ta>
            <ta e="T26" id="Seg_4183" s="T25">nör-bl-a-t</ta>
            <ta e="T28" id="Seg_4184" s="T27">dʼok</ta>
            <ta e="T29" id="Seg_4185" s="T28">măn</ta>
            <ta e="T30" id="Seg_4186" s="T29">jakše</ta>
            <ta e="T31" id="Seg_4187" s="T30">nör-bl-a-m</ta>
            <ta e="T32" id="Seg_4188" s="T31">tănan</ta>
            <ta e="T34" id="Seg_4189" s="T33">Nen</ta>
            <ta e="T35" id="Seg_4190" s="T34">an-a-ʔ</ta>
            <ta e="T36" id="Seg_4191" s="T35">dĭbər-tə</ta>
            <ta e="T38" id="Seg_4192" s="T37">nu</ta>
            <ta e="T39" id="Seg_4193" s="T38">măn</ta>
            <ta e="T40" id="Seg_4194" s="T39">ej</ta>
            <ta e="T41" id="Seg_4195" s="T40">al-la-m</ta>
            <ta e="T43" id="Seg_4196" s="T42">moʔ</ta>
            <ta e="T44" id="Seg_4197" s="T43">dak</ta>
            <ta e="T45" id="Seg_4198" s="T44">moʔ</ta>
            <ta e="T47" id="Seg_4199" s="T46">măn</ta>
            <ta e="T48" id="Seg_4200" s="T47">uda-m</ta>
            <ta e="T49" id="Seg_4201" s="T48">naga</ta>
            <ta e="T63" id="Seg_4202" s="T62">aba</ta>
            <ta e="T65" id="Seg_4203" s="T64">moʔ</ta>
            <ta e="T66" id="Seg_4204" s="T65">dereʔ</ta>
            <ta e="T67" id="Seg_4205" s="T66">tăn</ta>
            <ta e="T69" id="Seg_4206" s="T68">an-a-ʔ</ta>
            <ta e="T70" id="Seg_4207" s="T69">dĭbər</ta>
            <ta e="T72" id="Seg_4208" s="T71">dʼok</ta>
            <ta e="T73" id="Seg_4209" s="T72">măn</ta>
            <ta e="T74" id="Seg_4210" s="T73">ej</ta>
            <ta e="T75" id="Seg_4211" s="T74">al-la-m</ta>
            <ta e="T77" id="Seg_4212" s="T76">măn</ta>
            <ta e="T78" id="Seg_4213" s="T77">uda-m</ta>
            <ta e="T79" id="Seg_4214" s="T78">bile</ta>
            <ta e="T85" id="Seg_4215" s="T84">kan-žə-bəj</ta>
            <ta e="T86" id="Seg_4216" s="T85">măna</ta>
            <ta e="T87" id="Seg_4217" s="T86">ara</ta>
            <ta e="T88" id="Seg_4218" s="T87">bĭʔ-sittə</ta>
            <ta e="T90" id="Seg_4219" s="T89">dʼok</ta>
            <ta e="T91" id="Seg_4220" s="T90">ej</ta>
            <ta e="T92" id="Seg_4221" s="T91">al-la-m</ta>
            <ta e="T94" id="Seg_4222" s="T93">moʔ</ta>
            <ta e="T96" id="Seg_4223" s="T95">kan-žə-bəj</ta>
            <ta e="T97" id="Seg_4224" s="T96">ara</ta>
            <ta e="T98" id="Seg_4225" s="T97">kuvas</ta>
            <ta e="T100" id="Seg_4226" s="T99">kuvas</ta>
            <ta e="T101" id="Seg_4227" s="T100">bĭs-</ta>
            <ta e="T102" id="Seg_4228" s="T101">bĭ-sittə</ta>
            <ta e="T104" id="Seg_4229" s="T103">dʼok</ta>
            <ta e="T105" id="Seg_4230" s="T104">măna</ta>
            <ta e="T107" id="Seg_4231" s="T106">dĭgən</ta>
            <ta e="T108" id="Seg_4232" s="T107">münör-li-t</ta>
            <ta e="T110" id="Seg_4233" s="T109">dʼok</ta>
            <ta e="T111" id="Seg_4234" s="T110">măn</ta>
            <ta e="T112" id="Seg_4235" s="T111">ej</ta>
            <ta e="T113" id="Seg_4236" s="T112">mĭ-lə-m</ta>
            <ta e="T114" id="Seg_4237" s="T113">tănan</ta>
            <ta e="T115" id="Seg_4238" s="T114">münör-zittə</ta>
            <ta e="T117" id="Seg_4239" s="T116">nu</ta>
            <ta e="T118" id="Seg_4240" s="T117">al-lə-m</ta>
            <ta e="T120" id="Seg_4241" s="T119">al-lə-m</ta>
            <ta e="T121" id="Seg_4242" s="T120">ej</ta>
            <ta e="T122" id="Seg_4243" s="T121">mĭ-lə-l</ta>
            <ta e="T124" id="Seg_4244" s="T123">dʼok</ta>
            <ta e="T125" id="Seg_4245" s="T124">ej</ta>
            <ta e="T126" id="Seg_4246" s="T125">mĭ-lə-m</ta>
            <ta e="T128" id="Seg_4247" s="T127">nu</ta>
            <ta e="T129" id="Seg_4248" s="T128">jakše</ta>
            <ta e="T131" id="Seg_4249" s="T130">jakše</ta>
            <ta e="T132" id="Seg_4250" s="T131">ej</ta>
            <ta e="T133" id="Seg_4251" s="T132">mĭ-lə-l</ta>
            <ta e="T134" id="Seg_4252" s="T133">dak</ta>
            <ta e="T135" id="Seg_4253" s="T134">măn</ta>
            <ta e="T136" id="Seg_4254" s="T135">al-lə-m</ta>
            <ta e="T144" id="Seg_4255" s="T143">măn</ta>
            <ta e="T145" id="Seg_4256" s="T144">ia-m-zi</ta>
            <ta e="T146" id="Seg_4257" s="T145">ej</ta>
            <ta e="T147" id="Seg_4258" s="T146">mo-la-m</ta>
            <ta e="T149" id="Seg_4259" s="T148">măn</ta>
            <ta e="T151" id="Seg_4260" s="T150">dʼür-lə-m</ta>
            <ta e="T153" id="Seg_4261" s="T152">moʔ</ta>
            <ta e="T155" id="Seg_4262" s="T154">da</ta>
            <ta e="T156" id="Seg_4263" s="T155">dĭ</ta>
            <ta e="T157" id="Seg_4264" s="T156">kudonz-la</ta>
            <ta e="T158" id="Seg_4265" s="T157">tăŋ</ta>
            <ta e="T160" id="Seg_4266" s="T159">nu</ta>
            <ta e="T161" id="Seg_4267" s="T160">amno-ʔ</ta>
            <ta e="T162" id="Seg_4268" s="T161">ia-zi-l</ta>
            <ta e="T164" id="Seg_4269" s="T163">amno-ʔ</ta>
            <ta e="T166" id="Seg_4270" s="T165">moʔ</ta>
            <ta e="T167" id="Seg_4271" s="T166">dere</ta>
            <ta e="T168" id="Seg_4272" s="T167">mo-la-l</ta>
            <ta e="T170" id="Seg_4273" s="T169">tăn</ta>
            <ta e="T171" id="Seg_4274" s="T170">bila</ta>
            <ta e="T172" id="Seg_4275" s="T171">kuza</ta>
            <ta e="T178" id="Seg_4276" s="T177">tăn</ta>
            <ta e="T179" id="Seg_4277" s="T178">aba-l</ta>
            <ta e="T180" id="Seg_4278" s="T179">ia-m</ta>
            <ta e="T181" id="Seg_4279" s="T180">šen</ta>
            <ta e="T182" id="Seg_4280" s="T181">bila</ta>
            <ta e="T183" id="Seg_4281" s="T182">kuza</ta>
            <ta e="T185" id="Seg_4282" s="T184">a</ta>
            <ta e="T186" id="Seg_4283" s="T185">măn</ta>
            <ta e="T187" id="Seg_4284" s="T186">jakše</ta>
            <ta e="T189" id="Seg_4285" s="T188">dʼok</ta>
            <ta e="T190" id="Seg_4286" s="T189">măn</ta>
            <ta e="T191" id="Seg_4287" s="T190">jakše</ta>
            <ta e="T192" id="Seg_4288" s="T191">tăn</ta>
            <ta e="T193" id="Seg_4289" s="T192">bila</ta>
            <ta e="T196" id="Seg_4290" s="T195">mo</ta>
            <ta e="T197" id="Seg_4291" s="T196">dereʔ</ta>
            <ta e="T199" id="Seg_4292" s="T198">măl-la-l</ta>
            <ta e="T201" id="Seg_4293" s="T200">eneidənə</ta>
            <ta e="T203" id="Seg_4294" s="T202">măn-u-ʔ</ta>
            <ta e="T204" id="Seg_4295" s="T203">jakše</ta>
            <ta e="T207" id="Seg_4296" s="T206">tăŋ</ta>
            <ta e="T208" id="Seg_4297" s="T207">bila</ta>
            <ta e="T209" id="Seg_4298" s="T208">kuza</ta>
            <ta e="T211" id="Seg_4299" s="T210">eneidənə</ta>
            <ta e="T215" id="Seg_4300" s="T214">măn</ta>
            <ta e="T216" id="Seg_4301" s="T215">ia-m</ta>
            <ta e="T217" id="Seg_4302" s="T216">sumna</ta>
            <ta e="T218" id="Seg_4303" s="T217">sumna</ta>
            <ta e="T219" id="Seg_4304" s="T218">nʼi</ta>
            <ta e="T220" id="Seg_4305" s="T219">dĭn</ta>
            <ta e="T226" id="Seg_4306" s="T225">dĭn</ta>
            <ta e="T227" id="Seg_4307" s="T226">mo-bi</ta>
            <ta e="T228" id="Seg_4308" s="T227">nagur</ta>
            <ta e="T229" id="Seg_4309" s="T228">koʔbdo</ta>
            <ta e="T230" id="Seg_4310" s="T229">šide</ta>
            <ta e="T231" id="Seg_4311" s="T230">nʼi</ta>
            <ta e="T233" id="Seg_4312" s="T232">oʔb</ta>
            <ta e="T234" id="Seg_4313" s="T233">nʼi</ta>
            <ta e="T235" id="Seg_4314" s="T234">kü-lam-bi</ta>
            <ta e="T237" id="Seg_4315" s="T236">i</ta>
            <ta e="T238" id="Seg_4316" s="T237">koʔbdo</ta>
            <ta e="T250" id="Seg_4317" s="T249">šide</ta>
            <ta e="T251" id="Seg_4318" s="T250">koʔbdo</ta>
            <ta e="T252" id="Seg_4319" s="T251">kü-lam-bi</ta>
            <ta e="T254" id="Seg_4320" s="T253">a</ta>
            <ta e="T255" id="Seg_4321" s="T254">oʔb</ta>
            <ta e="T256" id="Seg_4322" s="T255">dʼok</ta>
            <ta e="T258" id="Seg_4323" s="T257">tei</ta>
            <ta e="T259" id="Seg_4324" s="T258">oʔb</ta>
            <ta e="T260" id="Seg_4325" s="T259">nʼi</ta>
            <ta e="T261" id="Seg_4326" s="T260">oʔb</ta>
            <ta e="T262" id="Seg_4327" s="T261">koʔbdo</ta>
            <ta e="T264" id="Seg_4328" s="T263">o</ta>
            <ta e="T265" id="Seg_4329" s="T264">ia-m</ta>
            <ta e="T266" id="Seg_4330" s="T265">tăŋ</ta>
            <ta e="T281" id="Seg_4331" s="T280">amno-ʔ</ta>
            <ta e="T282" id="Seg_4332" s="T281">sarɨ</ta>
            <ta e="T283" id="Seg_4333" s="T282">su</ta>
            <ta e="T284" id="Seg_4334" s="T283">am-zittə</ta>
            <ta e="T286" id="Seg_4335" s="T285">nu</ta>
            <ta e="T287" id="Seg_4336" s="T286">măn</ta>
            <ta e="T289" id="Seg_4337" s="T288">amor-bia-m</ta>
            <ta e="T291" id="Seg_4338" s="T290">nu</ta>
            <ta e="T292" id="Seg_4339" s="T291">uja</ta>
            <ta e="T294" id="Seg_4340" s="T293">uja</ta>
            <ta e="T295" id="Seg_4341" s="T294">jakše</ta>
            <ta e="T297" id="Seg_4342" s="T296">uja</ta>
            <ta e="T298" id="Seg_4343" s="T297">ipek</ta>
            <ta e="T300" id="Seg_4344" s="T299">am-zittə</ta>
            <ta e="T302" id="Seg_4345" s="T301">nu</ta>
            <ta e="T303" id="Seg_4346" s="T302">măn</ta>
            <ta e="T304" id="Seg_4347" s="T303">tolʼkă</ta>
            <ta e="T305" id="Seg_4348" s="T304">amor-bia-m</ta>
            <ta e="T311" id="Seg_4349" s="T310">amor-bia-m</ta>
            <ta e="T312" id="Seg_4350" s="T311">amor-bə-ʔ</ta>
            <ta e="T313" id="Seg_4351" s="T312">măn-ziʔ</ta>
            <ta e="T315" id="Seg_4352" s="T314">dö</ta>
            <ta e="T316" id="Seg_4353" s="T315">ipek</ta>
            <ta e="T318" id="Seg_4354" s="T317">dö</ta>
            <ta e="T319" id="Seg_4355" s="T318">uja</ta>
            <ta e="T321" id="Seg_4356" s="T320">am-</ta>
            <ta e="T322" id="Seg_4357" s="T321">amor-zittə</ta>
            <ta e="T323" id="Seg_4358" s="T322">možnă</ta>
            <ta e="T330" id="Seg_4359" s="T329">šo-ʔ</ta>
            <ta e="T331" id="Seg_4360" s="T330">döbər</ta>
            <ta e="T332" id="Seg_4361" s="T331">măna</ta>
            <ta e="T334" id="Seg_4362" s="T333">a</ta>
            <ta e="T335" id="Seg_4363" s="T334">ĭmbi</ta>
            <ta e="T337" id="Seg_4364" s="T336">ĭmbi=nʼibudʼ</ta>
            <ta e="T338" id="Seg_4365" s="T337">nörbə-la-l</ta>
            <ta e="T340" id="Seg_4366" s="T339">dʼok</ta>
            <ta e="T341" id="Seg_4367" s="T340">da</ta>
            <ta e="T342" id="Seg_4368" s="T341">tăn</ta>
            <ta e="T343" id="Seg_4369" s="T342">nörba-ʔ</ta>
            <ta e="T344" id="Seg_4370" s="T343">măna</ta>
            <ta e="T346" id="Seg_4371" s="T345">a</ta>
            <ta e="T347" id="Seg_4372" s="T346">măn</ta>
            <ta e="T348" id="Seg_4373" s="T347">ĭmbi=də</ta>
            <ta e="T349" id="Seg_4374" s="T348">ej</ta>
            <ta e="T350" id="Seg_4375" s="T349">tĭmne-m</ta>
            <ta e="T352" id="Seg_4376" s="T351">moʔ</ta>
            <ta e="T353" id="Seg_4377" s="T352">dere</ta>
            <ta e="T354" id="Seg_4378" s="T353">ĭmbi=də</ta>
            <ta e="T355" id="Seg_4379" s="T354">ej</ta>
            <ta e="T357" id="Seg_4380" s="T356">a</ta>
            <ta e="T358" id="Seg_4381" s="T357">măn</ta>
            <ta e="T359" id="Seg_4382" s="T358">nörbə-la-m</ta>
            <ta e="T361" id="Seg_4383" s="T360">tăn</ta>
            <ta e="T362" id="Seg_4384" s="T361">jakše</ta>
            <ta e="T363" id="Seg_4385" s="T362">kuza</ta>
            <ta e="T365" id="Seg_4386" s="T364">a</ta>
            <ta e="T366" id="Seg_4387" s="T365">măn</ta>
            <ta e="T367" id="Seg_4388" s="T366">bile</ta>
            <ta e="T368" id="Seg_4389" s="T367">dʼok</ta>
            <ta e="T369" id="Seg_4390" s="T368">tăn</ta>
            <ta e="T370" id="Seg_4391" s="T369">jakše</ta>
            <ta e="T372" id="Seg_4392" s="T371">moʔ</ta>
            <ta e="T373" id="Seg_4393" s="T372">dereʔ</ta>
            <ta e="T374" id="Seg_4394" s="T373">nörbə-la-l</ta>
            <ta e="T376" id="Seg_4395" s="T375">bile</ta>
            <ta e="T377" id="Seg_4396" s="T376">bile</ta>
            <ta e="T379" id="Seg_4397" s="T378">dere</ta>
            <ta e="T380" id="Seg_4398" s="T379">ej</ta>
            <ta e="T381" id="Seg_4399" s="T380">nörba-ʔ</ta>
            <ta e="T383" id="Seg_4400" s="T382">bile</ta>
            <ta e="T385" id="Seg_4401" s="T384">nadă</ta>
            <ta e="T386" id="Seg_4402" s="T385">nörbə-zittə</ta>
            <ta e="T387" id="Seg_4403" s="T386">jakše</ta>
            <ta e="T389" id="Seg_4404" s="T388">kuza</ta>
            <ta e="T391" id="Seg_4405" s="T390">a</ta>
            <ta e="T392" id="Seg_4406" s="T391">tăn</ta>
            <ta e="T393" id="Seg_4407" s="T392">üge</ta>
            <ta e="T394" id="Seg_4408" s="T393">ĭmbi=də</ta>
            <ta e="T395" id="Seg_4409" s="T394">ej</ta>
            <ta e="T396" id="Seg_4410" s="T395">nörbə-la-l</ta>
            <ta e="T402" id="Seg_4411" s="T401">kajdɨ</ta>
            <ta e="T403" id="Seg_4412" s="T402">dʼestek</ta>
            <ta e="T404" id="Seg_4413" s="T403">agɨlgazɨŋ</ta>
            <ta e="T406" id="Seg_4414" s="T405">kara</ta>
            <ta e="T407" id="Seg_4415" s="T406">dʼestek</ta>
            <ta e="T409" id="Seg_4416" s="T408">a</ta>
            <ta e="T411" id="Seg_4417" s="T410">dʼestek</ta>
            <ta e="T413" id="Seg_4418" s="T412">agɨl-ga-m</ta>
            <ta e="T415" id="Seg_4419" s="T414">dʼestek</ta>
            <ta e="T417" id="Seg_4420" s="T416">agɨl-ga-m</ta>
            <ta e="T419" id="Seg_4421" s="T418">nada</ta>
            <ta e="T420" id="Seg_4422" s="T419">ipek</ta>
            <ta e="T423" id="Seg_4423" s="T422">kajdɨ</ta>
            <ta e="T424" id="Seg_4424" s="T423">ipek</ta>
            <ta e="T426" id="Seg_4425" s="T425">da</ta>
            <ta e="T427" id="Seg_4426" s="T426">kara</ta>
            <ta e="T428" id="Seg_4427" s="T427">ipek</ta>
            <ta e="T430" id="Seg_4428" s="T429">dʼok</ta>
            <ta e="T431" id="Seg_4429" s="T430">kara</ta>
            <ta e="T432" id="Seg_4430" s="T431">ipek</ta>
            <ta e="T435" id="Seg_4431" s="T434">nada</ta>
            <ta e="T436" id="Seg_4432" s="T435">jakše</ta>
            <ta e="T437" id="Seg_4433" s="T436">ipek</ta>
            <ta e="T452" id="Seg_4434" s="T451">măn</ta>
            <ta e="T453" id="Seg_4435" s="T452">ulu-m</ta>
            <ta e="T454" id="Seg_4436" s="T453">ĭzem-nie-t</ta>
            <ta e="T456" id="Seg_4437" s="T455">tej</ta>
            <ta e="T457" id="Seg_4438" s="T456">măn</ta>
            <ta e="T458" id="Seg_4439" s="T457">ulu-m</ta>
            <ta e="T459" id="Seg_4440" s="T458">jakše</ta>
            <ta e="T463" id="Seg_4441" s="T462">gibər</ta>
            <ta e="T464" id="Seg_4442" s="T463">al-lə-l</ta>
            <ta e="T466" id="Seg_4443" s="T465">šo-lə-m</ta>
            <ta e="T468" id="Seg_4444" s="T467">mo</ta>
            <ta e="T469" id="Seg_4445" s="T468">dereʔ</ta>
            <ta e="T471" id="Seg_4446" s="T470">dak</ta>
            <ta e="T472" id="Seg_4447" s="T471">ĭmbi</ta>
            <ta e="T474" id="Seg_4448" s="T473">šo-ʔ</ta>
            <ta e="T476" id="Seg_4449" s="T475">dʼok</ta>
            <ta e="T478" id="Seg_4450" s="T477">măn</ta>
            <ta e="T480" id="Seg_4451" s="T479">dʼür-lə-m</ta>
            <ta e="T483" id="Seg_4452" s="T482">dʼür-ləŋ</ta>
            <ta e="T485" id="Seg_4453" s="T484">tăn</ta>
            <ta e="T487" id="Seg_4454" s="T486">a</ta>
            <ta e="T489" id="Seg_4455" s="T488">tăn</ta>
            <ta e="T490" id="Seg_4456" s="T489">bar</ta>
            <ta e="T492" id="Seg_4457" s="T491">bar</ta>
            <ta e="T493" id="Seg_4458" s="T492">mo-lə-l</ta>
            <ta e="T495" id="Seg_4459" s="T494">nu</ta>
            <ta e="T496" id="Seg_4460" s="T495">a</ta>
            <ta e="T497" id="Seg_4461" s="T496">tăn</ta>
            <ta e="T498" id="Seg_4462" s="T497">dak</ta>
            <ta e="T499" id="Seg_4463" s="T498">ej</ta>
            <ta e="T500" id="Seg_4464" s="T499">mo-la-l</ta>
            <ta e="T504" id="Seg_4465" s="T503">măn</ta>
            <ta e="T505" id="Seg_4466" s="T504">tüžöj</ta>
            <ta e="T506" id="Seg_4467" s="T505">jakše</ta>
            <ta e="T507" id="Seg_4468" s="T506">tüžöj</ta>
            <ta e="T509" id="Seg_4469" s="T508">i</ta>
            <ta e="T510" id="Seg_4470" s="T509">süt</ta>
            <ta e="T511" id="Seg_4471" s="T510">măna</ta>
            <ta e="T512" id="Seg_4472" s="T511">mĭ-lie-t</ta>
            <ta e="T514" id="Seg_4473" s="T513">süt</ta>
            <ta e="T515" id="Seg_4474" s="T514">măna</ta>
            <ta e="T516" id="Seg_4475" s="T515">mĭ-lie-t</ta>
            <ta e="T518" id="Seg_4476" s="T517">jakše</ta>
            <ta e="T520" id="Seg_4477" s="T519">gibər</ta>
            <ta e="T521" id="Seg_4478" s="T520">dĭ</ta>
            <ta e="T522" id="Seg_4479" s="T521">süt</ta>
            <ta e="T524" id="Seg_4480" s="T523">da</ta>
            <ta e="T525" id="Seg_4481" s="T524">gibər</ta>
            <ta e="T526" id="Seg_4482" s="T525">măn</ta>
            <ta e="T527" id="Seg_4483" s="T526">amor-la-m</ta>
            <ta e="T528" id="Seg_4484" s="T527">dö</ta>
            <ta e="T529" id="Seg_4485" s="T528">süt</ta>
            <ta e="T556" id="Seg_4486" s="T555">măn</ta>
            <ta e="T558" id="Seg_4487" s="T557">dʼür-lə-m</ta>
            <ta e="T560" id="Seg_4488" s="T559">gibər</ta>
            <ta e="T562" id="Seg_4489" s="T561">o</ta>
            <ta e="T563" id="Seg_4490" s="T562">gibər</ta>
            <ta e="T564" id="Seg_4491" s="T563">gibər</ta>
            <ta e="T565" id="Seg_4492" s="T564">a</ta>
            <ta e="T566" id="Seg_4493" s="T565">al-lə-m</ta>
            <ta e="T568" id="Seg_4494" s="T567">ĭmbi</ta>
            <ta e="T570" id="Seg_4495" s="T569">da</ta>
            <ta e="T571" id="Seg_4496" s="T570">măn</ta>
            <ta e="T572" id="Seg_4497" s="T571">ipek</ta>
            <ta e="T573" id="Seg_4498" s="T572">naga</ta>
            <ta e="T576" id="Seg_4499" s="T575">ipek</ta>
            <ta e="T577" id="Seg_4500" s="T576">i-lə-m</ta>
            <ta e="T579" id="Seg_4501" s="T578">a</ta>
            <ta e="T580" id="Seg_4502" s="T579">det-tə</ta>
            <ta e="T582" id="Seg_4503" s="T581">det-tə</ta>
            <ta e="T583" id="Seg_4504" s="T582">gibər</ta>
            <ta e="T584" id="Seg_4505" s="T583">al-lə-l</ta>
            <ta e="T586" id="Seg_4506" s="T585">al-lə-m</ta>
            <ta e="T587" id="Seg_4507" s="T586">gibər=nʼibudʼ</ta>
            <ta e="T589" id="Seg_4508" s="T588">amno-lə-ŋ</ta>
            <ta e="T590" id="Seg_4509" s="T589">i</ta>
            <ta e="T591" id="Seg_4510" s="T590">i-lə-m</ta>
            <ta e="T593" id="Seg_4511" s="T592">bar</ta>
            <ta e="T594" id="Seg_4512" s="T593">ĭmbi</ta>
            <ta e="T595" id="Seg_4513" s="T594">bar</ta>
            <ta e="T597" id="Seg_4514" s="T596">i-lə-m</ta>
            <ta e="T599" id="Seg_4515" s="T598">a</ta>
            <ta e="T600" id="Seg_4516" s="T599">moʔ</ta>
            <ta e="T601" id="Seg_4517" s="T600">dere</ta>
            <ta e="T602" id="Seg_4518" s="T601">mo-lə-l</ta>
            <ta e="T604" id="Seg_4519" s="T603">dak</ta>
            <ta e="T605" id="Seg_4520" s="T604">măna</ta>
            <ta e="T606" id="Seg_4521" s="T605">bile</ta>
            <ta e="T607" id="Seg_4522" s="T606">amno-zittə</ta>
            <ta e="T609" id="Seg_4523" s="T608">nada</ta>
            <ta e="T610" id="Seg_4524" s="T609">an-zittə</ta>
            <ta e="T611" id="Seg_4525" s="T610">bos-tə</ta>
            <ta e="T612" id="Seg_4526" s="T611">tura-nə</ta>
            <ta e="T614" id="Seg_4527" s="T613">a</ta>
            <ta e="T615" id="Seg_4528" s="T614">ĭmbi</ta>
            <ta e="T616" id="Seg_4529" s="T615">tura</ta>
            <ta e="T617" id="Seg_4530" s="T616">mĭ-lə-l</ta>
            <ta e="T618" id="Seg_4531" s="T617">tănan</ta>
            <ta e="T620" id="Seg_4532" s="T619">ĭmbi=nʼibudʼ</ta>
            <ta e="T621" id="Seg_4533" s="T620">mĭ-lə-l</ta>
            <ta e="T623" id="Seg_4534" s="T622">gibər</ta>
            <ta e="T625" id="Seg_4535" s="T624">a</ta>
            <ta e="T626" id="Seg_4536" s="T625">det-tə</ta>
            <ta e="T627" id="Seg_4537" s="T626">gibər</ta>
            <ta e="T629" id="Seg_4538" s="T628">nu</ta>
            <ta e="T630" id="Seg_4539" s="T629">bos</ta>
            <ta e="T631" id="Seg_4540" s="T630">tura-nə</ta>
            <ta e="T633" id="Seg_4541" s="T632">šo-lə-m</ta>
            <ta e="T634" id="Seg_4542" s="T633">amno-lap-lie-m</ta>
            <ta e="T640" id="Seg_4543" s="T639">det-tə</ta>
            <ta e="T641" id="Seg_4544" s="T640">măna</ta>
            <ta e="T642" id="Seg_4545" s="T641">tamgu</ta>
            <ta e="T643" id="Seg_4546" s="T642">nʼeʔ-sittə</ta>
            <ta e="T645" id="Seg_4547" s="T644">măn</ta>
            <ta e="T646" id="Seg_4548" s="T645">naga</ta>
            <ta e="T647" id="Seg_4549" s="T646">tamgu</ta>
            <ta e="T650" id="Seg_4550" s="T649">i</ta>
            <ta e="T651" id="Seg_4551" s="T650">măn</ta>
            <ta e="T652" id="Seg_4552" s="T651">naga</ta>
            <ta e="T654" id="Seg_4553" s="T653">dʼok</ta>
            <ta e="T655" id="Seg_4554" s="T654">i-ge</ta>
            <ta e="T656" id="Seg_4555" s="T655">i-ge</ta>
            <ta e="T658" id="Seg_4556" s="T657">det-tə</ta>
            <ta e="T659" id="Seg_4557" s="T658">măna</ta>
            <ta e="T661" id="Seg_4558" s="T660">măn</ta>
            <ta e="T662" id="Seg_4559" s="T661">naga</ta>
            <ta e="T663" id="Seg_4560" s="T662">tamgu</ta>
            <ta e="T665" id="Seg_4561" s="T664">a</ta>
            <ta e="T666" id="Seg_4562" s="T665">măn</ta>
            <ta e="T667" id="Seg_4563" s="T666">nʼeʔ-sittə</ta>
            <ta e="T668" id="Seg_4564" s="T667">mo-la-m</ta>
            <ta e="T672" id="Seg_4565" s="T671">šo-ʔ</ta>
            <ta e="T673" id="Seg_4566" s="T672">măna</ta>
            <ta e="T674" id="Seg_4567" s="T673">ara</ta>
            <ta e="T675" id="Seg_4568" s="T674">bĭʔ-sittə</ta>
            <ta e="T677" id="Seg_4569" s="T676">da</ta>
            <ta e="T678" id="Seg_4570" s="T677">ara</ta>
            <ta e="T680" id="Seg_4571" s="T679">bĭʔ-sittə</ta>
            <ta e="T681" id="Seg_4572" s="T680">nada</ta>
            <ta e="T683" id="Seg_4573" s="T682">nu</ta>
            <ta e="T684" id="Seg_4574" s="T683">nada</ta>
            <ta e="T686" id="Seg_4575" s="T685">ara</ta>
            <ta e="T687" id="Seg_4576" s="T686">bĭʔ-sʼittə</ta>
            <ta e="T688" id="Seg_4577" s="T687">dak</ta>
            <ta e="T689" id="Seg_4578" s="T688">ej</ta>
            <ta e="T690" id="Seg_4579" s="T689">dʼabro-ʔ</ta>
            <ta e="T692" id="Seg_4580" s="T691">tăn</ta>
            <ta e="T693" id="Seg_4581" s="T692">vedʼ</ta>
            <ta e="T694" id="Seg_4582" s="T693">bile</ta>
            <ta e="T695" id="Seg_4583" s="T694">kuza</ta>
            <ta e="T697" id="Seg_4584" s="T696">a</ta>
            <ta e="T698" id="Seg_4585" s="T697">măn</ta>
            <ta e="T699" id="Seg_4586" s="T698">jakše</ta>
            <ta e="T700" id="Seg_4587" s="T699">kuza</ta>
            <ta e="T702" id="Seg_4588" s="T701">măn</ta>
            <ta e="T703" id="Seg_4589" s="T702">ara</ta>
            <ta e="T706" id="Seg_4590" s="T705">bĭt-lie-m</ta>
            <ta e="T707" id="Seg_4591" s="T706">ej</ta>
            <ta e="T708" id="Seg_4592" s="T707">dʼabro-la-m</ta>
            <ta e="T710" id="Seg_4593" s="T709">a</ta>
            <ta e="T711" id="Seg_4594" s="T710">tăn</ta>
            <ta e="T712" id="Seg_4595" s="T711">üge</ta>
            <ta e="T713" id="Seg_4596" s="T712">dʼabro-lie-l</ta>
            <ta e="T715" id="Seg_4597" s="T714">šen</ta>
            <ta e="T716" id="Seg_4598" s="T715">ej</ta>
            <ta e="T717" id="Seg_4599" s="T716">jakše</ta>
            <ta e="T718" id="Seg_4600" s="T717">dărə</ta>
            <ta e="T719" id="Seg_4601" s="T718">bĭʔ-sittə</ta>
            <ta e="T721" id="Seg_4602" s="T720">bĭʔ-sittə</ta>
            <ta e="T722" id="Seg_4603" s="T721">nada</ta>
            <ta e="T723" id="Seg_4604" s="T722">nʼe</ta>
            <ta e="T724" id="Seg_4605" s="T723">nada</ta>
            <ta e="T725" id="Seg_4606" s="T724">dʼabro-zittə</ta>
            <ta e="T727" id="Seg_4607" s="T726">nada</ta>
            <ta e="T728" id="Seg_4608" s="T727">jakše</ta>
            <ta e="T730" id="Seg_4609" s="T729">măn</ta>
            <ta e="T733" id="Seg_4610" s="T732">kuvas</ta>
            <ta e="T734" id="Seg_4611" s="T733">kuza</ta>
         </annotation>
         <annotation name="mp" tierref="mp-SAE">
            <ta e="T10" id="Seg_4612" s="T9">aba-m</ta>
            <ta e="T11" id="Seg_4613" s="T10">moʔ</ta>
            <ta e="T12" id="Seg_4614" s="T11">dărəʔ</ta>
            <ta e="T13" id="Seg_4615" s="T12">nörbə-bəl-ə-t</ta>
            <ta e="T15" id="Seg_4616" s="T14">šen</ta>
            <ta e="T16" id="Seg_4617" s="T15">bile</ta>
            <ta e="T18" id="Seg_4618" s="T17">šen</ta>
            <ta e="T19" id="Seg_4619" s="T18">bile</ta>
            <ta e="T20" id="Seg_4620" s="T19">nörbə-bəl-ə-t</ta>
            <ta e="T22" id="Seg_4621" s="T21">moʔ</ta>
            <ta e="T23" id="Seg_4622" s="T22">dărəʔ</ta>
            <ta e="T25" id="Seg_4623" s="T24">tăn</ta>
            <ta e="T26" id="Seg_4624" s="T25">nörbə-bəl-ə-t</ta>
            <ta e="T28" id="Seg_4625" s="T27">dʼok</ta>
            <ta e="T29" id="Seg_4626" s="T28">măn</ta>
            <ta e="T30" id="Seg_4627" s="T29">jakšə</ta>
            <ta e="T31" id="Seg_4628" s="T30">nörbə-bəl-ə-m</ta>
            <ta e="T32" id="Seg_4629" s="T31">tănan</ta>
            <ta e="T35" id="Seg_4630" s="T34">an-ə-ʔ</ta>
            <ta e="T36" id="Seg_4631" s="T35">dĭbər-Tə</ta>
            <ta e="T38" id="Seg_4632" s="T37">nu</ta>
            <ta e="T39" id="Seg_4633" s="T38">măn</ta>
            <ta e="T40" id="Seg_4634" s="T39">ej</ta>
            <ta e="T41" id="Seg_4635" s="T40">an-lV-m</ta>
            <ta e="T43" id="Seg_4636" s="T42">moʔ</ta>
            <ta e="T44" id="Seg_4637" s="T43">tak</ta>
            <ta e="T45" id="Seg_4638" s="T44">moʔ</ta>
            <ta e="T47" id="Seg_4639" s="T46">măn</ta>
            <ta e="T48" id="Seg_4640" s="T47">uda-m</ta>
            <ta e="T49" id="Seg_4641" s="T48">naga</ta>
            <ta e="T63" id="Seg_4642" s="T62">aba</ta>
            <ta e="T65" id="Seg_4643" s="T64">moʔ</ta>
            <ta e="T66" id="Seg_4644" s="T65">dărəʔ</ta>
            <ta e="T67" id="Seg_4645" s="T66">tăn</ta>
            <ta e="T69" id="Seg_4646" s="T68">an-ə-ʔ</ta>
            <ta e="T70" id="Seg_4647" s="T69">dĭbər</ta>
            <ta e="T72" id="Seg_4648" s="T71">dʼok</ta>
            <ta e="T73" id="Seg_4649" s="T72">măn</ta>
            <ta e="T74" id="Seg_4650" s="T73">ej</ta>
            <ta e="T75" id="Seg_4651" s="T74">an-lV-m</ta>
            <ta e="T77" id="Seg_4652" s="T76">măn</ta>
            <ta e="T78" id="Seg_4653" s="T77">uda-m</ta>
            <ta e="T79" id="Seg_4654" s="T78">bile</ta>
            <ta e="T85" id="Seg_4655" s="T84">kan-žə-bəj</ta>
            <ta e="T86" id="Seg_4656" s="T85">măna</ta>
            <ta e="T87" id="Seg_4657" s="T86">ara</ta>
            <ta e="T88" id="Seg_4658" s="T87">bĭs-zittə</ta>
            <ta e="T90" id="Seg_4659" s="T89">dʼok</ta>
            <ta e="T91" id="Seg_4660" s="T90">ej</ta>
            <ta e="T92" id="Seg_4661" s="T91">an-lV-m</ta>
            <ta e="T94" id="Seg_4662" s="T93">moʔ</ta>
            <ta e="T96" id="Seg_4663" s="T95">kan-žə-bəj</ta>
            <ta e="T97" id="Seg_4664" s="T96">ara</ta>
            <ta e="T98" id="Seg_4665" s="T97">kuvas</ta>
            <ta e="T100" id="Seg_4666" s="T99">kuvas</ta>
            <ta e="T101" id="Seg_4667" s="T100">bĭs-</ta>
            <ta e="T102" id="Seg_4668" s="T101">bĭs-zittə</ta>
            <ta e="T104" id="Seg_4669" s="T103">dʼok</ta>
            <ta e="T105" id="Seg_4670" s="T104">măna</ta>
            <ta e="T107" id="Seg_4671" s="T106">dĭgən</ta>
            <ta e="T108" id="Seg_4672" s="T107">münör-lV-t</ta>
            <ta e="T110" id="Seg_4673" s="T109">dʼok</ta>
            <ta e="T111" id="Seg_4674" s="T110">măn</ta>
            <ta e="T112" id="Seg_4675" s="T111">ej</ta>
            <ta e="T113" id="Seg_4676" s="T112">mĭ-lV-m</ta>
            <ta e="T114" id="Seg_4677" s="T113">tănan</ta>
            <ta e="T115" id="Seg_4678" s="T114">münör-zittə</ta>
            <ta e="T117" id="Seg_4679" s="T116">nu</ta>
            <ta e="T118" id="Seg_4680" s="T117">an-lV-m</ta>
            <ta e="T120" id="Seg_4681" s="T119">an-lV-m</ta>
            <ta e="T121" id="Seg_4682" s="T120">ej</ta>
            <ta e="T122" id="Seg_4683" s="T121">mĭ-lV-l</ta>
            <ta e="T124" id="Seg_4684" s="T123">dʼok</ta>
            <ta e="T125" id="Seg_4685" s="T124">ej</ta>
            <ta e="T126" id="Seg_4686" s="T125">mĭ-lV-m</ta>
            <ta e="T128" id="Seg_4687" s="T127">nu</ta>
            <ta e="T129" id="Seg_4688" s="T128">jakšə</ta>
            <ta e="T131" id="Seg_4689" s="T130">jakšə</ta>
            <ta e="T132" id="Seg_4690" s="T131">ej</ta>
            <ta e="T133" id="Seg_4691" s="T132">mĭ-lV-l</ta>
            <ta e="T134" id="Seg_4692" s="T133">tak</ta>
            <ta e="T135" id="Seg_4693" s="T134">măn</ta>
            <ta e="T136" id="Seg_4694" s="T135">an-lV-m</ta>
            <ta e="T144" id="Seg_4695" s="T143">măn</ta>
            <ta e="T145" id="Seg_4696" s="T144">ija-m-ziʔ</ta>
            <ta e="T146" id="Seg_4697" s="T145">ej</ta>
            <ta e="T147" id="Seg_4698" s="T146">mo-lV-m</ta>
            <ta e="T149" id="Seg_4699" s="T148">măn</ta>
            <ta e="T151" id="Seg_4700" s="T150">tʼür-lV-m</ta>
            <ta e="T153" id="Seg_4701" s="T152">moʔ</ta>
            <ta e="T155" id="Seg_4702" s="T154">da</ta>
            <ta e="T156" id="Seg_4703" s="T155">dĭ</ta>
            <ta e="T157" id="Seg_4704" s="T156">kudonz-lV</ta>
            <ta e="T158" id="Seg_4705" s="T157">tăŋ</ta>
            <ta e="T160" id="Seg_4706" s="T159">nu</ta>
            <ta e="T161" id="Seg_4707" s="T160">amno-ʔ</ta>
            <ta e="T162" id="Seg_4708" s="T161">ija-ziʔ-l</ta>
            <ta e="T164" id="Seg_4709" s="T163">amno-ʔ</ta>
            <ta e="T166" id="Seg_4710" s="T165">moʔ</ta>
            <ta e="T167" id="Seg_4711" s="T166">dărəʔ</ta>
            <ta e="T168" id="Seg_4712" s="T167">mo-lV-l</ta>
            <ta e="T170" id="Seg_4713" s="T169">tăn</ta>
            <ta e="T171" id="Seg_4714" s="T170">bile</ta>
            <ta e="T172" id="Seg_4715" s="T171">kuza</ta>
            <ta e="T178" id="Seg_4716" s="T177">tăn</ta>
            <ta e="T179" id="Seg_4717" s="T178">aba-l</ta>
            <ta e="T180" id="Seg_4718" s="T179">ija-m</ta>
            <ta e="T181" id="Seg_4719" s="T180">šen</ta>
            <ta e="T182" id="Seg_4720" s="T181">bile</ta>
            <ta e="T183" id="Seg_4721" s="T182">kuza</ta>
            <ta e="T185" id="Seg_4722" s="T184">a</ta>
            <ta e="T186" id="Seg_4723" s="T185">măn</ta>
            <ta e="T187" id="Seg_4724" s="T186">jakšə</ta>
            <ta e="T189" id="Seg_4725" s="T188">dʼok</ta>
            <ta e="T190" id="Seg_4726" s="T189">măn</ta>
            <ta e="T191" id="Seg_4727" s="T190">jakšə</ta>
            <ta e="T192" id="Seg_4728" s="T191">tăn</ta>
            <ta e="T193" id="Seg_4729" s="T192">bile</ta>
            <ta e="T196" id="Seg_4730" s="T195">moʔ</ta>
            <ta e="T197" id="Seg_4731" s="T196">dărəʔ</ta>
            <ta e="T199" id="Seg_4732" s="T198">măn-labaʔ-l</ta>
            <ta e="T201" id="Seg_4733" s="T200">eneidəne</ta>
            <ta e="T203" id="Seg_4734" s="T202">măn-ə-ʔ</ta>
            <ta e="T204" id="Seg_4735" s="T203">jakšə</ta>
            <ta e="T207" id="Seg_4736" s="T206">tăŋ</ta>
            <ta e="T208" id="Seg_4737" s="T207">bile</ta>
            <ta e="T209" id="Seg_4738" s="T208">kuza</ta>
            <ta e="T211" id="Seg_4739" s="T210">eneidəne</ta>
            <ta e="T215" id="Seg_4740" s="T214">măn</ta>
            <ta e="T216" id="Seg_4741" s="T215">ija-m</ta>
            <ta e="T217" id="Seg_4742" s="T216">sumna</ta>
            <ta e="T218" id="Seg_4743" s="T217">sumna</ta>
            <ta e="T219" id="Seg_4744" s="T218">nʼi</ta>
            <ta e="T220" id="Seg_4745" s="T219">dĭn</ta>
            <ta e="T226" id="Seg_4746" s="T225">dĭn</ta>
            <ta e="T227" id="Seg_4747" s="T226">mo-bi</ta>
            <ta e="T228" id="Seg_4748" s="T227">nagur</ta>
            <ta e="T229" id="Seg_4749" s="T228">koʔbdo</ta>
            <ta e="T230" id="Seg_4750" s="T229">šide</ta>
            <ta e="T231" id="Seg_4751" s="T230">nʼi</ta>
            <ta e="T233" id="Seg_4752" s="T232">oʔb</ta>
            <ta e="T234" id="Seg_4753" s="T233">nʼi</ta>
            <ta e="T235" id="Seg_4754" s="T234">kü-laːm-bi</ta>
            <ta e="T237" id="Seg_4755" s="T236">i</ta>
            <ta e="T238" id="Seg_4756" s="T237">koʔbdo</ta>
            <ta e="T250" id="Seg_4757" s="T249">šide</ta>
            <ta e="T251" id="Seg_4758" s="T250">koʔbdo</ta>
            <ta e="T252" id="Seg_4759" s="T251">kü-laːm-bi</ta>
            <ta e="T254" id="Seg_4760" s="T253">a</ta>
            <ta e="T255" id="Seg_4761" s="T254">oʔb</ta>
            <ta e="T256" id="Seg_4762" s="T255">dʼok</ta>
            <ta e="T258" id="Seg_4763" s="T257">tüj</ta>
            <ta e="T259" id="Seg_4764" s="T258">oʔb</ta>
            <ta e="T260" id="Seg_4765" s="T259">nʼi</ta>
            <ta e="T261" id="Seg_4766" s="T260">oʔb</ta>
            <ta e="T262" id="Seg_4767" s="T261">koʔbdo</ta>
            <ta e="T264" id="Seg_4768" s="T263">o</ta>
            <ta e="T265" id="Seg_4769" s="T264">ija-m</ta>
            <ta e="T266" id="Seg_4770" s="T265">tăŋ</ta>
            <ta e="T281" id="Seg_4771" s="T280">amno-ʔ</ta>
            <ta e="T282" id="Seg_4772" s="T281">sarɨ</ta>
            <ta e="T283" id="Seg_4773" s="T282">su</ta>
            <ta e="T284" id="Seg_4774" s="T283">am-zittə</ta>
            <ta e="T286" id="Seg_4775" s="T285">nu</ta>
            <ta e="T287" id="Seg_4776" s="T286">măn</ta>
            <ta e="T289" id="Seg_4777" s="T288">amor-bi-m</ta>
            <ta e="T291" id="Seg_4778" s="T290">nu</ta>
            <ta e="T292" id="Seg_4779" s="T291">uja</ta>
            <ta e="T294" id="Seg_4780" s="T293">uja</ta>
            <ta e="T295" id="Seg_4781" s="T294">jakšə</ta>
            <ta e="T297" id="Seg_4782" s="T296">uja</ta>
            <ta e="T298" id="Seg_4783" s="T297">ipek</ta>
            <ta e="T300" id="Seg_4784" s="T299">am-zittə</ta>
            <ta e="T302" id="Seg_4785" s="T301">nu</ta>
            <ta e="T303" id="Seg_4786" s="T302">măn</ta>
            <ta e="T304" id="Seg_4787" s="T303">tolʼko</ta>
            <ta e="T305" id="Seg_4788" s="T304">amor-bi-m</ta>
            <ta e="T311" id="Seg_4789" s="T310">amor-bi-m</ta>
            <ta e="T312" id="Seg_4790" s="T311">amor-bə-ʔ</ta>
            <ta e="T313" id="Seg_4791" s="T312">măn-ziʔ</ta>
            <ta e="T315" id="Seg_4792" s="T314">dö</ta>
            <ta e="T316" id="Seg_4793" s="T315">ipek</ta>
            <ta e="T318" id="Seg_4794" s="T317">dö</ta>
            <ta e="T319" id="Seg_4795" s="T318">uja</ta>
            <ta e="T321" id="Seg_4796" s="T320">am-</ta>
            <ta e="T322" id="Seg_4797" s="T321">amor-zittə</ta>
            <ta e="T323" id="Seg_4798" s="T322">možna</ta>
            <ta e="T330" id="Seg_4799" s="T329">šo-ʔ</ta>
            <ta e="T331" id="Seg_4800" s="T330">döbər</ta>
            <ta e="T332" id="Seg_4801" s="T331">măna</ta>
            <ta e="T334" id="Seg_4802" s="T333">a</ta>
            <ta e="T335" id="Seg_4803" s="T334">ĭmbi</ta>
            <ta e="T337" id="Seg_4804" s="T336">ĭmbi=nʼibudʼ</ta>
            <ta e="T338" id="Seg_4805" s="T337">nörbə-lV-l</ta>
            <ta e="T340" id="Seg_4806" s="T339">dʼok</ta>
            <ta e="T341" id="Seg_4807" s="T340">da</ta>
            <ta e="T342" id="Seg_4808" s="T341">tăn</ta>
            <ta e="T343" id="Seg_4809" s="T342">nörbə-ʔ</ta>
            <ta e="T344" id="Seg_4810" s="T343">măna</ta>
            <ta e="T346" id="Seg_4811" s="T345">a</ta>
            <ta e="T347" id="Seg_4812" s="T346">măn</ta>
            <ta e="T348" id="Seg_4813" s="T347">ĭmbi=də</ta>
            <ta e="T349" id="Seg_4814" s="T348">ej</ta>
            <ta e="T350" id="Seg_4815" s="T349">tĭmne-m</ta>
            <ta e="T352" id="Seg_4816" s="T351">moʔ</ta>
            <ta e="T353" id="Seg_4817" s="T352">dărəʔ</ta>
            <ta e="T354" id="Seg_4818" s="T353">ĭmbi=də</ta>
            <ta e="T355" id="Seg_4819" s="T354">ej</ta>
            <ta e="T357" id="Seg_4820" s="T356">a</ta>
            <ta e="T358" id="Seg_4821" s="T357">măn</ta>
            <ta e="T359" id="Seg_4822" s="T358">nörbə-lV-m</ta>
            <ta e="T361" id="Seg_4823" s="T360">tăn</ta>
            <ta e="T362" id="Seg_4824" s="T361">jakšə</ta>
            <ta e="T363" id="Seg_4825" s="T362">kuza</ta>
            <ta e="T365" id="Seg_4826" s="T364">a</ta>
            <ta e="T366" id="Seg_4827" s="T365">măn</ta>
            <ta e="T367" id="Seg_4828" s="T366">bile</ta>
            <ta e="T368" id="Seg_4829" s="T367">dʼok</ta>
            <ta e="T369" id="Seg_4830" s="T368">tăn</ta>
            <ta e="T370" id="Seg_4831" s="T369">jakšə</ta>
            <ta e="T372" id="Seg_4832" s="T371">moʔ</ta>
            <ta e="T373" id="Seg_4833" s="T372">dărəʔ</ta>
            <ta e="T374" id="Seg_4834" s="T373">nörbə-labaʔ-l</ta>
            <ta e="T376" id="Seg_4835" s="T375">bile</ta>
            <ta e="T377" id="Seg_4836" s="T376">bile</ta>
            <ta e="T379" id="Seg_4837" s="T378">dărəʔ</ta>
            <ta e="T380" id="Seg_4838" s="T379">ej</ta>
            <ta e="T381" id="Seg_4839" s="T380">nörbə-ʔ</ta>
            <ta e="T383" id="Seg_4840" s="T382">bile</ta>
            <ta e="T385" id="Seg_4841" s="T384">nadə</ta>
            <ta e="T386" id="Seg_4842" s="T385">nörbə-zittə</ta>
            <ta e="T387" id="Seg_4843" s="T386">jakšə</ta>
            <ta e="T389" id="Seg_4844" s="T388">kuza</ta>
            <ta e="T391" id="Seg_4845" s="T390">a</ta>
            <ta e="T392" id="Seg_4846" s="T391">tăn</ta>
            <ta e="T393" id="Seg_4847" s="T392">üge</ta>
            <ta e="T394" id="Seg_4848" s="T393">ĭmbi=də</ta>
            <ta e="T395" id="Seg_4849" s="T394">ej</ta>
            <ta e="T396" id="Seg_4850" s="T395">nörbə-labaʔ-l</ta>
            <ta e="T403" id="Seg_4851" s="T402">dʼestek</ta>
            <ta e="T406" id="Seg_4852" s="T405">kara</ta>
            <ta e="T407" id="Seg_4853" s="T406">dʼestek</ta>
            <ta e="T409" id="Seg_4854" s="T408">a</ta>
            <ta e="T411" id="Seg_4855" s="T410">dʼestek</ta>
            <ta e="T413" id="Seg_4856" s="T412">%%-gA-m</ta>
            <ta e="T415" id="Seg_4857" s="T414">dʼestek</ta>
            <ta e="T417" id="Seg_4858" s="T416">%%-gA-m</ta>
            <ta e="T419" id="Seg_4859" s="T418">nadə</ta>
            <ta e="T420" id="Seg_4860" s="T419">ipek</ta>
            <ta e="T424" id="Seg_4861" s="T423">ipek</ta>
            <ta e="T426" id="Seg_4862" s="T425">da</ta>
            <ta e="T427" id="Seg_4863" s="T426">kara</ta>
            <ta e="T428" id="Seg_4864" s="T427">ipek</ta>
            <ta e="T430" id="Seg_4865" s="T429">dʼok</ta>
            <ta e="T431" id="Seg_4866" s="T430">kara</ta>
            <ta e="T432" id="Seg_4867" s="T431">ipek</ta>
            <ta e="T435" id="Seg_4868" s="T434">nadə</ta>
            <ta e="T436" id="Seg_4869" s="T435">jakšə</ta>
            <ta e="T437" id="Seg_4870" s="T436">ipek</ta>
            <ta e="T452" id="Seg_4871" s="T451">măn</ta>
            <ta e="T453" id="Seg_4872" s="T452">ulu-m</ta>
            <ta e="T454" id="Seg_4873" s="T453">ĭzem-liA-t</ta>
            <ta e="T456" id="Seg_4874" s="T455">tüj</ta>
            <ta e="T457" id="Seg_4875" s="T456">măn</ta>
            <ta e="T458" id="Seg_4876" s="T457">ulu-m</ta>
            <ta e="T459" id="Seg_4877" s="T458">jakšə</ta>
            <ta e="T463" id="Seg_4878" s="T462">gibər</ta>
            <ta e="T464" id="Seg_4879" s="T463">an-lV-l</ta>
            <ta e="T466" id="Seg_4880" s="T465">šo-lV-m</ta>
            <ta e="T468" id="Seg_4881" s="T467">moʔ</ta>
            <ta e="T469" id="Seg_4882" s="T468">dărəʔ</ta>
            <ta e="T471" id="Seg_4883" s="T470">tak</ta>
            <ta e="T472" id="Seg_4884" s="T471">ĭmbi</ta>
            <ta e="T474" id="Seg_4885" s="T473">šo-ʔ</ta>
            <ta e="T476" id="Seg_4886" s="T475">dʼok</ta>
            <ta e="T478" id="Seg_4887" s="T477">măn</ta>
            <ta e="T480" id="Seg_4888" s="T479">tʼür-lV-m</ta>
            <ta e="T483" id="Seg_4889" s="T482">tʼür-lV</ta>
            <ta e="T485" id="Seg_4890" s="T484">tăn</ta>
            <ta e="T487" id="Seg_4891" s="T486">a</ta>
            <ta e="T489" id="Seg_4892" s="T488">tăn</ta>
            <ta e="T490" id="Seg_4893" s="T489">bar</ta>
            <ta e="T492" id="Seg_4894" s="T491">bar</ta>
            <ta e="T493" id="Seg_4895" s="T492">mo-lV-l</ta>
            <ta e="T495" id="Seg_4896" s="T494">nu</ta>
            <ta e="T496" id="Seg_4897" s="T495">a</ta>
            <ta e="T497" id="Seg_4898" s="T496">tăn</ta>
            <ta e="T498" id="Seg_4899" s="T497">tak</ta>
            <ta e="T499" id="Seg_4900" s="T498">ej</ta>
            <ta e="T500" id="Seg_4901" s="T499">mo-lV-l</ta>
            <ta e="T504" id="Seg_4902" s="T503">măn</ta>
            <ta e="T505" id="Seg_4903" s="T504">tüžöj</ta>
            <ta e="T506" id="Seg_4904" s="T505">jakšə</ta>
            <ta e="T507" id="Seg_4905" s="T506">tüžöj</ta>
            <ta e="T509" id="Seg_4906" s="T508">i</ta>
            <ta e="T510" id="Seg_4907" s="T509">süt</ta>
            <ta e="T511" id="Seg_4908" s="T510">măna</ta>
            <ta e="T512" id="Seg_4909" s="T511">mĭ-liA-t</ta>
            <ta e="T514" id="Seg_4910" s="T513">süt</ta>
            <ta e="T515" id="Seg_4911" s="T514">măna</ta>
            <ta e="T516" id="Seg_4912" s="T515">mĭ-liA-t</ta>
            <ta e="T518" id="Seg_4913" s="T517">jakšə</ta>
            <ta e="T520" id="Seg_4914" s="T519">gibər</ta>
            <ta e="T521" id="Seg_4915" s="T520">dĭ</ta>
            <ta e="T522" id="Seg_4916" s="T521">süt</ta>
            <ta e="T524" id="Seg_4917" s="T523">da</ta>
            <ta e="T525" id="Seg_4918" s="T524">gibər</ta>
            <ta e="T526" id="Seg_4919" s="T525">măn</ta>
            <ta e="T527" id="Seg_4920" s="T526">amor-lV-m</ta>
            <ta e="T528" id="Seg_4921" s="T527">dö</ta>
            <ta e="T529" id="Seg_4922" s="T528">süt</ta>
            <ta e="T556" id="Seg_4923" s="T555">măn</ta>
            <ta e="T558" id="Seg_4924" s="T557">tʼür-lV-m</ta>
            <ta e="T560" id="Seg_4925" s="T559">gibər</ta>
            <ta e="T562" id="Seg_4926" s="T561">o</ta>
            <ta e="T563" id="Seg_4927" s="T562">gibər</ta>
            <ta e="T564" id="Seg_4928" s="T563">gibər</ta>
            <ta e="T566" id="Seg_4929" s="T565">an-lV-m</ta>
            <ta e="T568" id="Seg_4930" s="T567">ĭmbi</ta>
            <ta e="T570" id="Seg_4931" s="T569">da</ta>
            <ta e="T571" id="Seg_4932" s="T570">măn</ta>
            <ta e="T572" id="Seg_4933" s="T571">ipek</ta>
            <ta e="T573" id="Seg_4934" s="T572">naga</ta>
            <ta e="T576" id="Seg_4935" s="T575">ipek</ta>
            <ta e="T577" id="Seg_4936" s="T576">i-lV-m</ta>
            <ta e="T579" id="Seg_4937" s="T578">a</ta>
            <ta e="T580" id="Seg_4938" s="T579">det-t</ta>
            <ta e="T582" id="Seg_4939" s="T581">det-t</ta>
            <ta e="T583" id="Seg_4940" s="T582">gibər</ta>
            <ta e="T584" id="Seg_4941" s="T583">an-lV-l</ta>
            <ta e="T586" id="Seg_4942" s="T585">an-lV-m</ta>
            <ta e="T587" id="Seg_4943" s="T586">gibər=nʼibudʼ</ta>
            <ta e="T589" id="Seg_4944" s="T588">amno-lV-ŋ</ta>
            <ta e="T590" id="Seg_4945" s="T589">i</ta>
            <ta e="T591" id="Seg_4946" s="T590">i-lV-m</ta>
            <ta e="T593" id="Seg_4947" s="T592">bar</ta>
            <ta e="T594" id="Seg_4948" s="T593">ĭmbi</ta>
            <ta e="T595" id="Seg_4949" s="T594">bar</ta>
            <ta e="T597" id="Seg_4950" s="T596">i-lV-m</ta>
            <ta e="T599" id="Seg_4951" s="T598">a</ta>
            <ta e="T600" id="Seg_4952" s="T599">moʔ</ta>
            <ta e="T601" id="Seg_4953" s="T600">dărəʔ</ta>
            <ta e="T602" id="Seg_4954" s="T601">mo-lV-l</ta>
            <ta e="T604" id="Seg_4955" s="T603">tak</ta>
            <ta e="T605" id="Seg_4956" s="T604">măna</ta>
            <ta e="T606" id="Seg_4957" s="T605">bile</ta>
            <ta e="T607" id="Seg_4958" s="T606">amno-zittə</ta>
            <ta e="T609" id="Seg_4959" s="T608">nadə</ta>
            <ta e="T610" id="Seg_4960" s="T609">an-zittə</ta>
            <ta e="T611" id="Seg_4961" s="T610">bos-də</ta>
            <ta e="T612" id="Seg_4962" s="T611">tura-Tə</ta>
            <ta e="T614" id="Seg_4963" s="T613">a</ta>
            <ta e="T615" id="Seg_4964" s="T614">ĭmbi</ta>
            <ta e="T616" id="Seg_4965" s="T615">tura</ta>
            <ta e="T617" id="Seg_4966" s="T616">mĭ-lV-l</ta>
            <ta e="T618" id="Seg_4967" s="T617">tănan</ta>
            <ta e="T620" id="Seg_4968" s="T619">ĭmbi=nʼibudʼ</ta>
            <ta e="T621" id="Seg_4969" s="T620">mĭ-lV-l</ta>
            <ta e="T623" id="Seg_4970" s="T622">gibər</ta>
            <ta e="T625" id="Seg_4971" s="T624">a</ta>
            <ta e="T626" id="Seg_4972" s="T625">det-t</ta>
            <ta e="T627" id="Seg_4973" s="T626">gibər</ta>
            <ta e="T629" id="Seg_4974" s="T628">nu</ta>
            <ta e="T630" id="Seg_4975" s="T629">bos</ta>
            <ta e="T631" id="Seg_4976" s="T630">tura-Tə</ta>
            <ta e="T633" id="Seg_4977" s="T632">šo-lV-m</ta>
            <ta e="T634" id="Seg_4978" s="T633">amno-labaʔ-liA-m</ta>
            <ta e="T640" id="Seg_4979" s="T639">det-t</ta>
            <ta e="T641" id="Seg_4980" s="T640">măna</ta>
            <ta e="T642" id="Seg_4981" s="T641">taŋgu</ta>
            <ta e="T643" id="Seg_4982" s="T642">nʼeʔbdə-zittə</ta>
            <ta e="T645" id="Seg_4983" s="T644">măn</ta>
            <ta e="T646" id="Seg_4984" s="T645">naga</ta>
            <ta e="T647" id="Seg_4985" s="T646">taŋgu</ta>
            <ta e="T650" id="Seg_4986" s="T649">i</ta>
            <ta e="T651" id="Seg_4987" s="T650">măn</ta>
            <ta e="T652" id="Seg_4988" s="T651">naga</ta>
            <ta e="T654" id="Seg_4989" s="T653">dʼok</ta>
            <ta e="T655" id="Seg_4990" s="T654">i-gA</ta>
            <ta e="T656" id="Seg_4991" s="T655">i-gA</ta>
            <ta e="T658" id="Seg_4992" s="T657">det-t</ta>
            <ta e="T659" id="Seg_4993" s="T658">măna</ta>
            <ta e="T661" id="Seg_4994" s="T660">măn</ta>
            <ta e="T662" id="Seg_4995" s="T661">naga</ta>
            <ta e="T663" id="Seg_4996" s="T662">taŋgu</ta>
            <ta e="T665" id="Seg_4997" s="T664">a</ta>
            <ta e="T666" id="Seg_4998" s="T665">măn</ta>
            <ta e="T667" id="Seg_4999" s="T666">nʼeʔbdə-zittə</ta>
            <ta e="T668" id="Seg_5000" s="T667">mo-lV-m</ta>
            <ta e="T672" id="Seg_5001" s="T671">šo-ʔ</ta>
            <ta e="T673" id="Seg_5002" s="T672">măna</ta>
            <ta e="T674" id="Seg_5003" s="T673">ara</ta>
            <ta e="T675" id="Seg_5004" s="T674">bĭs-zittə</ta>
            <ta e="T677" id="Seg_5005" s="T676">da</ta>
            <ta e="T678" id="Seg_5006" s="T677">ara</ta>
            <ta e="T680" id="Seg_5007" s="T679">bĭs-zittə</ta>
            <ta e="T681" id="Seg_5008" s="T680">nadə</ta>
            <ta e="T683" id="Seg_5009" s="T682">nu</ta>
            <ta e="T684" id="Seg_5010" s="T683">nadə</ta>
            <ta e="T686" id="Seg_5011" s="T685">ara</ta>
            <ta e="T687" id="Seg_5012" s="T686">bĭs-zittə</ta>
            <ta e="T688" id="Seg_5013" s="T687">tak</ta>
            <ta e="T689" id="Seg_5014" s="T688">ej</ta>
            <ta e="T690" id="Seg_5015" s="T689">tʼabəro-ʔ</ta>
            <ta e="T692" id="Seg_5016" s="T691">tăn</ta>
            <ta e="T693" id="Seg_5017" s="T692">vedʼ</ta>
            <ta e="T694" id="Seg_5018" s="T693">bile</ta>
            <ta e="T695" id="Seg_5019" s="T694">kuza</ta>
            <ta e="T697" id="Seg_5020" s="T696">a</ta>
            <ta e="T698" id="Seg_5021" s="T697">măn</ta>
            <ta e="T699" id="Seg_5022" s="T698">jakšə</ta>
            <ta e="T700" id="Seg_5023" s="T699">kuza</ta>
            <ta e="T702" id="Seg_5024" s="T701">măn</ta>
            <ta e="T703" id="Seg_5025" s="T702">ara</ta>
            <ta e="T706" id="Seg_5026" s="T705">bĭs-liA-m</ta>
            <ta e="T707" id="Seg_5027" s="T706">ej</ta>
            <ta e="T708" id="Seg_5028" s="T707">tʼabəro-lV-m</ta>
            <ta e="T710" id="Seg_5029" s="T709">a</ta>
            <ta e="T711" id="Seg_5030" s="T710">tăn</ta>
            <ta e="T712" id="Seg_5031" s="T711">üge</ta>
            <ta e="T713" id="Seg_5032" s="T712">tʼabəro-liA-l</ta>
            <ta e="T715" id="Seg_5033" s="T714">šen</ta>
            <ta e="T716" id="Seg_5034" s="T715">ej</ta>
            <ta e="T717" id="Seg_5035" s="T716">jakšə</ta>
            <ta e="T718" id="Seg_5036" s="T717">dărəʔ</ta>
            <ta e="T719" id="Seg_5037" s="T718">bĭs-zittə</ta>
            <ta e="T721" id="Seg_5038" s="T720">bĭs-zittə</ta>
            <ta e="T722" id="Seg_5039" s="T721">nadə</ta>
            <ta e="T723" id="Seg_5040" s="T722">nʼe</ta>
            <ta e="T724" id="Seg_5041" s="T723">nadə</ta>
            <ta e="T725" id="Seg_5042" s="T724">tʼabəro-zittə</ta>
            <ta e="T727" id="Seg_5043" s="T726">nadə</ta>
            <ta e="T728" id="Seg_5044" s="T727">jakšə</ta>
            <ta e="T730" id="Seg_5045" s="T729">măn</ta>
            <ta e="T733" id="Seg_5046" s="T732">kuvas</ta>
            <ta e="T734" id="Seg_5047" s="T733">kuza</ta>
         </annotation>
         <annotation name="ge" tierref="ge-SAE">
            <ta e="T10" id="Seg_5048" s="T9">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T11" id="Seg_5049" s="T10">why</ta>
            <ta e="T12" id="Seg_5050" s="T11">so</ta>
            <ta e="T13" id="Seg_5051" s="T12">tell-%%-EP-3SG.O</ta>
            <ta e="T15" id="Seg_5052" s="T14">really</ta>
            <ta e="T16" id="Seg_5053" s="T15">bad.[NOM.SG]</ta>
            <ta e="T18" id="Seg_5054" s="T17">really</ta>
            <ta e="T19" id="Seg_5055" s="T18">bad.[NOM.SG]</ta>
            <ta e="T20" id="Seg_5056" s="T19">tell-%%-EP-3SG.O</ta>
            <ta e="T22" id="Seg_5057" s="T21">why</ta>
            <ta e="T23" id="Seg_5058" s="T22">so</ta>
            <ta e="T25" id="Seg_5059" s="T24">you.NOM</ta>
            <ta e="T26" id="Seg_5060" s="T25">tell-%%-EP-3SG.O</ta>
            <ta e="T28" id="Seg_5061" s="T27">no</ta>
            <ta e="T29" id="Seg_5062" s="T28">I.NOM</ta>
            <ta e="T30" id="Seg_5063" s="T29">good</ta>
            <ta e="T31" id="Seg_5064" s="T30">tell-%%-EP-1SG</ta>
            <ta e="T32" id="Seg_5065" s="T31">you.DAT</ta>
            <ta e="T35" id="Seg_5066" s="T34">go-EP-IMP.2SG</ta>
            <ta e="T36" id="Seg_5067" s="T35">there-LAT</ta>
            <ta e="T38" id="Seg_5068" s="T37">well</ta>
            <ta e="T39" id="Seg_5069" s="T38">I.NOM</ta>
            <ta e="T40" id="Seg_5070" s="T39">NEG</ta>
            <ta e="T41" id="Seg_5071" s="T40">go-FUT-1SG</ta>
            <ta e="T43" id="Seg_5072" s="T42">why</ta>
            <ta e="T44" id="Seg_5073" s="T43">so</ta>
            <ta e="T45" id="Seg_5074" s="T44">why</ta>
            <ta e="T47" id="Seg_5075" s="T46">I.NOM</ta>
            <ta e="T48" id="Seg_5076" s="T47">hand-NOM/GEN/ACC.1SG</ta>
            <ta e="T49" id="Seg_5077" s="T48">NEG.EX.[3SG]</ta>
            <ta e="T63" id="Seg_5078" s="T62">father</ta>
            <ta e="T65" id="Seg_5079" s="T64">why</ta>
            <ta e="T66" id="Seg_5080" s="T65">so</ta>
            <ta e="T67" id="Seg_5081" s="T66">you.NOM</ta>
            <ta e="T69" id="Seg_5082" s="T68">go-EP-IMP.2SG</ta>
            <ta e="T70" id="Seg_5083" s="T69">there</ta>
            <ta e="T72" id="Seg_5084" s="T71">no</ta>
            <ta e="T73" id="Seg_5085" s="T72">I.NOM</ta>
            <ta e="T74" id="Seg_5086" s="T73">NEG</ta>
            <ta e="T75" id="Seg_5087" s="T74">go-FUT-1SG</ta>
            <ta e="T77" id="Seg_5088" s="T76">I.NOM</ta>
            <ta e="T78" id="Seg_5089" s="T77">hand-NOM/GEN/ACC.1SG</ta>
            <ta e="T79" id="Seg_5090" s="T78">bad.[NOM.SG]</ta>
            <ta e="T85" id="Seg_5091" s="T84">go-OPT.DU/PL-1DU</ta>
            <ta e="T86" id="Seg_5092" s="T85">I.LAT</ta>
            <ta e="T87" id="Seg_5093" s="T86">vodka.[NOM.SG]</ta>
            <ta e="T88" id="Seg_5094" s="T87">drink-INF.LAT</ta>
            <ta e="T90" id="Seg_5095" s="T89">no</ta>
            <ta e="T91" id="Seg_5096" s="T90">NEG</ta>
            <ta e="T92" id="Seg_5097" s="T91">go-FUT-1SG</ta>
            <ta e="T94" id="Seg_5098" s="T93">why</ta>
            <ta e="T96" id="Seg_5099" s="T95">go-OPT.DU/PL-1DU</ta>
            <ta e="T97" id="Seg_5100" s="T96">vodka.[NOM.SG]</ta>
            <ta e="T98" id="Seg_5101" s="T97">beautiful.[NOM.SG]</ta>
            <ta e="T100" id="Seg_5102" s="T99">beautiful.[NOM.SG]</ta>
            <ta e="T101" id="Seg_5103" s="T100">drink-</ta>
            <ta e="T102" id="Seg_5104" s="T101">drink-INF.LAT</ta>
            <ta e="T104" id="Seg_5105" s="T103">no</ta>
            <ta e="T105" id="Seg_5106" s="T104">I.LAT</ta>
            <ta e="T107" id="Seg_5107" s="T106">there</ta>
            <ta e="T108" id="Seg_5108" s="T107">beat-FUT-3SG.O</ta>
            <ta e="T110" id="Seg_5109" s="T109">no</ta>
            <ta e="T111" id="Seg_5110" s="T110">I.NOM</ta>
            <ta e="T112" id="Seg_5111" s="T111">NEG</ta>
            <ta e="T113" id="Seg_5112" s="T112">give-FUT-1SG</ta>
            <ta e="T114" id="Seg_5113" s="T113">you.DAT</ta>
            <ta e="T115" id="Seg_5114" s="T114">beat-INF.LAT</ta>
            <ta e="T117" id="Seg_5115" s="T116">well</ta>
            <ta e="T118" id="Seg_5116" s="T117">go-FUT-1SG</ta>
            <ta e="T120" id="Seg_5117" s="T119">go-FUT-1SG</ta>
            <ta e="T121" id="Seg_5118" s="T120">NEG</ta>
            <ta e="T122" id="Seg_5119" s="T121">give-FUT-2SG</ta>
            <ta e="T124" id="Seg_5120" s="T123">no</ta>
            <ta e="T125" id="Seg_5121" s="T124">NEG</ta>
            <ta e="T126" id="Seg_5122" s="T125">give-FUT-1SG</ta>
            <ta e="T128" id="Seg_5123" s="T127">well</ta>
            <ta e="T129" id="Seg_5124" s="T128">good</ta>
            <ta e="T131" id="Seg_5125" s="T130">good</ta>
            <ta e="T132" id="Seg_5126" s="T131">NEG</ta>
            <ta e="T133" id="Seg_5127" s="T132">give-FUT-2SG</ta>
            <ta e="T134" id="Seg_5128" s="T133">so</ta>
            <ta e="T135" id="Seg_5129" s="T134">I.NOM</ta>
            <ta e="T136" id="Seg_5130" s="T135">go-FUT-1SG</ta>
            <ta e="T144" id="Seg_5131" s="T143">I.NOM</ta>
            <ta e="T145" id="Seg_5132" s="T144">mother-1SG-COM</ta>
            <ta e="T146" id="Seg_5133" s="T145">NEG</ta>
            <ta e="T147" id="Seg_5134" s="T146">can-FUT-1SG</ta>
            <ta e="T149" id="Seg_5135" s="T148">I.NOM</ta>
            <ta e="T151" id="Seg_5136" s="T150">disappear-FUT-1SG</ta>
            <ta e="T153" id="Seg_5137" s="T152">why</ta>
            <ta e="T155" id="Seg_5138" s="T154">and</ta>
            <ta e="T156" id="Seg_5139" s="T155">this.[NOM.SG]</ta>
            <ta e="T157" id="Seg_5140" s="T156">scold-FUT.[3SG]</ta>
            <ta e="T158" id="Seg_5141" s="T157">strongly</ta>
            <ta e="T160" id="Seg_5142" s="T159">well</ta>
            <ta e="T161" id="Seg_5143" s="T160">live-IMP.2SG</ta>
            <ta e="T162" id="Seg_5144" s="T161">mother-COM-2SG</ta>
            <ta e="T164" id="Seg_5145" s="T163">live-IMP.2SG</ta>
            <ta e="T166" id="Seg_5146" s="T165">why</ta>
            <ta e="T167" id="Seg_5147" s="T166">so</ta>
            <ta e="T168" id="Seg_5148" s="T167">become-FUT-2SG</ta>
            <ta e="T170" id="Seg_5149" s="T169">you.NOM</ta>
            <ta e="T171" id="Seg_5150" s="T170">bad</ta>
            <ta e="T172" id="Seg_5151" s="T171">man.[NOM.SG]</ta>
            <ta e="T178" id="Seg_5152" s="T177">you.NOM</ta>
            <ta e="T179" id="Seg_5153" s="T178">father-NOM/GEN/ACC.2SG</ta>
            <ta e="T180" id="Seg_5154" s="T179">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T181" id="Seg_5155" s="T180">really</ta>
            <ta e="T182" id="Seg_5156" s="T181">bad</ta>
            <ta e="T183" id="Seg_5157" s="T182">man.[NOM.SG]</ta>
            <ta e="T185" id="Seg_5158" s="T184">and</ta>
            <ta e="T186" id="Seg_5159" s="T185">I.NOM</ta>
            <ta e="T187" id="Seg_5160" s="T186">good</ta>
            <ta e="T189" id="Seg_5161" s="T188">no</ta>
            <ta e="T190" id="Seg_5162" s="T189">I.GEN</ta>
            <ta e="T191" id="Seg_5163" s="T190">good</ta>
            <ta e="T192" id="Seg_5164" s="T191">you.GEN</ta>
            <ta e="T193" id="Seg_5165" s="T192">bad</ta>
            <ta e="T196" id="Seg_5166" s="T195">why</ta>
            <ta e="T197" id="Seg_5167" s="T196">so</ta>
            <ta e="T199" id="Seg_5168" s="T198">say-RES-2SG</ta>
            <ta e="T201" id="Seg_5169" s="T200">devil.[NOM.SG]</ta>
            <ta e="T203" id="Seg_5170" s="T202">say-EP-IMP.2SG</ta>
            <ta e="T204" id="Seg_5171" s="T203">good</ta>
            <ta e="T207" id="Seg_5172" s="T206">strongly</ta>
            <ta e="T208" id="Seg_5173" s="T207">bad</ta>
            <ta e="T209" id="Seg_5174" s="T208">man.[NOM.SG]</ta>
            <ta e="T211" id="Seg_5175" s="T210">devil.[NOM.SG]</ta>
            <ta e="T215" id="Seg_5176" s="T214">I.NOM</ta>
            <ta e="T216" id="Seg_5177" s="T215">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T217" id="Seg_5178" s="T216">five.[NOM.SG]</ta>
            <ta e="T218" id="Seg_5179" s="T217">five.[NOM.SG]</ta>
            <ta e="T219" id="Seg_5180" s="T218">boy.[NOM.SG]</ta>
            <ta e="T220" id="Seg_5181" s="T219">there</ta>
            <ta e="T226" id="Seg_5182" s="T225">there</ta>
            <ta e="T227" id="Seg_5183" s="T226">become-PST.[3SG]</ta>
            <ta e="T228" id="Seg_5184" s="T227">three.[NOM.SG]</ta>
            <ta e="T229" id="Seg_5185" s="T228">girl.[NOM.SG]</ta>
            <ta e="T230" id="Seg_5186" s="T229">two.[NOM.SG]</ta>
            <ta e="T231" id="Seg_5187" s="T230">boy.[NOM.SG]</ta>
            <ta e="T233" id="Seg_5188" s="T232">one.[NOM.SG]</ta>
            <ta e="T234" id="Seg_5189" s="T233">boy.[NOM.SG]</ta>
            <ta e="T235" id="Seg_5190" s="T234">die-RES-PST.[3SG]</ta>
            <ta e="T237" id="Seg_5191" s="T236">and</ta>
            <ta e="T238" id="Seg_5192" s="T237">girl.[NOM.SG]</ta>
            <ta e="T250" id="Seg_5193" s="T249">two.[NOM.SG]</ta>
            <ta e="T251" id="Seg_5194" s="T250">girl.[NOM.SG]</ta>
            <ta e="T252" id="Seg_5195" s="T251">die-RES-PST.[3SG]</ta>
            <ta e="T254" id="Seg_5196" s="T253">and</ta>
            <ta e="T255" id="Seg_5197" s="T254">one.[NOM.SG]</ta>
            <ta e="T256" id="Seg_5198" s="T255">no</ta>
            <ta e="T258" id="Seg_5199" s="T257">now</ta>
            <ta e="T259" id="Seg_5200" s="T258">one.[NOM.SG]</ta>
            <ta e="T260" id="Seg_5201" s="T259">boy.[NOM.SG]</ta>
            <ta e="T261" id="Seg_5202" s="T260">one.[NOM.SG]</ta>
            <ta e="T262" id="Seg_5203" s="T261">girl.[NOM.SG]</ta>
            <ta e="T264" id="Seg_5204" s="T263">oh</ta>
            <ta e="T265" id="Seg_5205" s="T264">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T266" id="Seg_5206" s="T265">strongly</ta>
            <ta e="T281" id="Seg_5207" s="T280">sit-IMP.2SG</ta>
            <ta e="T282" id="Seg_5208" s="T281">yellow</ta>
            <ta e="T283" id="Seg_5209" s="T282">water.[NOM.SG]</ta>
            <ta e="T284" id="Seg_5210" s="T283">eat-INF.LAT</ta>
            <ta e="T286" id="Seg_5211" s="T285">well</ta>
            <ta e="T287" id="Seg_5212" s="T286">I.NOM</ta>
            <ta e="T289" id="Seg_5213" s="T288">eat-PST-1SG</ta>
            <ta e="T291" id="Seg_5214" s="T290">well</ta>
            <ta e="T292" id="Seg_5215" s="T291">meat.[NOM.SG]</ta>
            <ta e="T294" id="Seg_5216" s="T293">meat.[NOM.SG]</ta>
            <ta e="T295" id="Seg_5217" s="T294">good</ta>
            <ta e="T297" id="Seg_5218" s="T296">meat.[NOM.SG]</ta>
            <ta e="T298" id="Seg_5219" s="T297">bread.[NOM.SG]</ta>
            <ta e="T300" id="Seg_5220" s="T299">eat-INF.LAT</ta>
            <ta e="T302" id="Seg_5221" s="T301">well</ta>
            <ta e="T303" id="Seg_5222" s="T302">I.NOM</ta>
            <ta e="T304" id="Seg_5223" s="T303">only</ta>
            <ta e="T305" id="Seg_5224" s="T304">eat-PST-1SG</ta>
            <ta e="T311" id="Seg_5225" s="T310">eat-PST-1SG</ta>
            <ta e="T312" id="Seg_5226" s="T311">eat-%%-IMP.2SG</ta>
            <ta e="T313" id="Seg_5227" s="T312">I.NOM-COM</ta>
            <ta e="T315" id="Seg_5228" s="T314">that.[NOM.SG]</ta>
            <ta e="T316" id="Seg_5229" s="T315">bread.[NOM.SG]</ta>
            <ta e="T318" id="Seg_5230" s="T317">that.[NOM.SG]</ta>
            <ta e="T319" id="Seg_5231" s="T318">meat.[NOM.SG]</ta>
            <ta e="T321" id="Seg_5232" s="T320">sit-</ta>
            <ta e="T322" id="Seg_5233" s="T321">eat-INF.LAT</ta>
            <ta e="T323" id="Seg_5234" s="T322">one.can</ta>
            <ta e="T330" id="Seg_5235" s="T329">come-IMP.2SG</ta>
            <ta e="T331" id="Seg_5236" s="T330">here</ta>
            <ta e="T332" id="Seg_5237" s="T331">I.LAT</ta>
            <ta e="T334" id="Seg_5238" s="T333">and</ta>
            <ta e="T335" id="Seg_5239" s="T334">what.[NOM.SG]</ta>
            <ta e="T337" id="Seg_5240" s="T336">what=INDEF</ta>
            <ta e="T338" id="Seg_5241" s="T337">tell-FUT-2SG</ta>
            <ta e="T340" id="Seg_5242" s="T339">no</ta>
            <ta e="T341" id="Seg_5243" s="T340">and</ta>
            <ta e="T342" id="Seg_5244" s="T341">you.NOM</ta>
            <ta e="T343" id="Seg_5245" s="T342">tell-IMP.2SG</ta>
            <ta e="T344" id="Seg_5246" s="T343">I.LAT</ta>
            <ta e="T346" id="Seg_5247" s="T345">and</ta>
            <ta e="T347" id="Seg_5248" s="T346">I.NOM</ta>
            <ta e="T348" id="Seg_5249" s="T347">what.[NOM.SG]=INDEF</ta>
            <ta e="T349" id="Seg_5250" s="T348">NEG</ta>
            <ta e="T350" id="Seg_5251" s="T349">know-1SG</ta>
            <ta e="T352" id="Seg_5252" s="T351">why</ta>
            <ta e="T353" id="Seg_5253" s="T352">so</ta>
            <ta e="T354" id="Seg_5254" s="T353">what.[NOM.SG]=INDEF</ta>
            <ta e="T355" id="Seg_5255" s="T354">NEG</ta>
            <ta e="T357" id="Seg_5256" s="T356">and</ta>
            <ta e="T358" id="Seg_5257" s="T357">I.NOM</ta>
            <ta e="T359" id="Seg_5258" s="T358">tell-FUT-1SG</ta>
            <ta e="T361" id="Seg_5259" s="T360">you.NOM</ta>
            <ta e="T362" id="Seg_5260" s="T361">good</ta>
            <ta e="T363" id="Seg_5261" s="T362">man.[NOM.SG]</ta>
            <ta e="T365" id="Seg_5262" s="T364">and</ta>
            <ta e="T366" id="Seg_5263" s="T365">I.NOM</ta>
            <ta e="T367" id="Seg_5264" s="T366">bad.[NOM.SG]</ta>
            <ta e="T368" id="Seg_5265" s="T367">no</ta>
            <ta e="T369" id="Seg_5266" s="T368">you.NOM</ta>
            <ta e="T370" id="Seg_5267" s="T369">good</ta>
            <ta e="T372" id="Seg_5268" s="T371">why</ta>
            <ta e="T373" id="Seg_5269" s="T372">so</ta>
            <ta e="T374" id="Seg_5270" s="T373">tell-RES-2SG</ta>
            <ta e="T376" id="Seg_5271" s="T375">bad.[NOM.SG]</ta>
            <ta e="T377" id="Seg_5272" s="T376">bad.[NOM.SG]</ta>
            <ta e="T379" id="Seg_5273" s="T378">so</ta>
            <ta e="T380" id="Seg_5274" s="T379">NEG</ta>
            <ta e="T381" id="Seg_5275" s="T380">tell-IMP.2SG</ta>
            <ta e="T383" id="Seg_5276" s="T382">bad.[NOM.SG]</ta>
            <ta e="T385" id="Seg_5277" s="T384">one.should</ta>
            <ta e="T386" id="Seg_5278" s="T385">tell-INF.LAT</ta>
            <ta e="T387" id="Seg_5279" s="T386">good</ta>
            <ta e="T389" id="Seg_5280" s="T388">man.[NOM.SG]</ta>
            <ta e="T391" id="Seg_5281" s="T390">and</ta>
            <ta e="T392" id="Seg_5282" s="T391">you.NOM</ta>
            <ta e="T393" id="Seg_5283" s="T392">always</ta>
            <ta e="T394" id="Seg_5284" s="T393">what.[NOM.SG]=INDEF</ta>
            <ta e="T395" id="Seg_5285" s="T394">NEG</ta>
            <ta e="T396" id="Seg_5286" s="T395">tell-RES-2SG</ta>
            <ta e="T403" id="Seg_5287" s="T402">berry.[NOM.SG]</ta>
            <ta e="T406" id="Seg_5288" s="T405">black</ta>
            <ta e="T407" id="Seg_5289" s="T406">berry.[NOM.SG]</ta>
            <ta e="T409" id="Seg_5290" s="T408">and</ta>
            <ta e="T411" id="Seg_5291" s="T410">berry.[NOM.SG]</ta>
            <ta e="T413" id="Seg_5292" s="T412">%%-PRS-1SG</ta>
            <ta e="T415" id="Seg_5293" s="T414">berry.[NOM.SG]</ta>
            <ta e="T417" id="Seg_5294" s="T416">%%-PRS-1SG</ta>
            <ta e="T419" id="Seg_5295" s="T418">one.should</ta>
            <ta e="T420" id="Seg_5296" s="T419">bread.[NOM.SG]</ta>
            <ta e="T424" id="Seg_5297" s="T423">bread.[NOM.SG]</ta>
            <ta e="T426" id="Seg_5298" s="T425">and</ta>
            <ta e="T427" id="Seg_5299" s="T426">black</ta>
            <ta e="T428" id="Seg_5300" s="T427">bread.[NOM.SG]</ta>
            <ta e="T430" id="Seg_5301" s="T429">no</ta>
            <ta e="T431" id="Seg_5302" s="T430">black</ta>
            <ta e="T432" id="Seg_5303" s="T431">bread.[NOM.SG]</ta>
            <ta e="T435" id="Seg_5304" s="T434">one.should</ta>
            <ta e="T436" id="Seg_5305" s="T435">good</ta>
            <ta e="T437" id="Seg_5306" s="T436">bread.[NOM.SG]</ta>
            <ta e="T452" id="Seg_5307" s="T451">I.NOM</ta>
            <ta e="T453" id="Seg_5308" s="T452">head-NOM/GEN/ACC.1SG</ta>
            <ta e="T454" id="Seg_5309" s="T453">hurt-PRS-3SG.O</ta>
            <ta e="T456" id="Seg_5310" s="T455">now</ta>
            <ta e="T457" id="Seg_5311" s="T456">I.NOM</ta>
            <ta e="T458" id="Seg_5312" s="T457">head-NOM/GEN/ACC.1SG</ta>
            <ta e="T459" id="Seg_5313" s="T458">good</ta>
            <ta e="T463" id="Seg_5314" s="T462">where.to</ta>
            <ta e="T464" id="Seg_5315" s="T463">go-FUT-2SG</ta>
            <ta e="T466" id="Seg_5316" s="T465">come-FUT-1SG</ta>
            <ta e="T468" id="Seg_5317" s="T467">why</ta>
            <ta e="T469" id="Seg_5318" s="T468">so</ta>
            <ta e="T471" id="Seg_5319" s="T470">so</ta>
            <ta e="T472" id="Seg_5320" s="T471">what.[NOM.SG]</ta>
            <ta e="T474" id="Seg_5321" s="T473">come-IMP.2SG</ta>
            <ta e="T476" id="Seg_5322" s="T475">no</ta>
            <ta e="T478" id="Seg_5323" s="T477">I.NOM</ta>
            <ta e="T480" id="Seg_5324" s="T479">disappear-FUT-1SG</ta>
            <ta e="T483" id="Seg_5325" s="T482">disappear-FUT</ta>
            <ta e="T485" id="Seg_5326" s="T484">you.NOM</ta>
            <ta e="T487" id="Seg_5327" s="T486">ah</ta>
            <ta e="T489" id="Seg_5328" s="T488">you.NOM</ta>
            <ta e="T490" id="Seg_5329" s="T489">PTCL</ta>
            <ta e="T492" id="Seg_5330" s="T491">PTCL</ta>
            <ta e="T493" id="Seg_5331" s="T492">can-FUT-2SG</ta>
            <ta e="T495" id="Seg_5332" s="T494">well</ta>
            <ta e="T496" id="Seg_5333" s="T495">and</ta>
            <ta e="T497" id="Seg_5334" s="T496">you.NOM</ta>
            <ta e="T498" id="Seg_5335" s="T497">so</ta>
            <ta e="T499" id="Seg_5336" s="T498">NEG</ta>
            <ta e="T500" id="Seg_5337" s="T499">become-FUT-2SG</ta>
            <ta e="T504" id="Seg_5338" s="T503">I.GEN</ta>
            <ta e="T505" id="Seg_5339" s="T504">cow.[NOM.SG]</ta>
            <ta e="T506" id="Seg_5340" s="T505">good</ta>
            <ta e="T507" id="Seg_5341" s="T506">cow.[NOM.SG]</ta>
            <ta e="T509" id="Seg_5342" s="T508">and</ta>
            <ta e="T510" id="Seg_5343" s="T509">milk.[NOM.SG]</ta>
            <ta e="T511" id="Seg_5344" s="T510">I.LAT</ta>
            <ta e="T512" id="Seg_5345" s="T511">give-PRS-3SG.O</ta>
            <ta e="T514" id="Seg_5346" s="T513">milk.[NOM.SG]</ta>
            <ta e="T515" id="Seg_5347" s="T514">I.LAT</ta>
            <ta e="T516" id="Seg_5348" s="T515">give-PRS-3SG.O</ta>
            <ta e="T518" id="Seg_5349" s="T517">good</ta>
            <ta e="T520" id="Seg_5350" s="T519">where.to</ta>
            <ta e="T521" id="Seg_5351" s="T520">this.[NOM.SG]</ta>
            <ta e="T522" id="Seg_5352" s="T521">milk.[NOM.SG]</ta>
            <ta e="T524" id="Seg_5353" s="T523">and</ta>
            <ta e="T525" id="Seg_5354" s="T524">where.to</ta>
            <ta e="T526" id="Seg_5355" s="T525">I.NOM</ta>
            <ta e="T527" id="Seg_5356" s="T526">eat-FUT-1SG</ta>
            <ta e="T528" id="Seg_5357" s="T527">that.[NOM.SG]</ta>
            <ta e="T529" id="Seg_5358" s="T528">milk.[NOM.SG]</ta>
            <ta e="T556" id="Seg_5359" s="T555">I.NOM</ta>
            <ta e="T558" id="Seg_5360" s="T557">disappear-FUT-1SG</ta>
            <ta e="T560" id="Seg_5361" s="T559">where.to</ta>
            <ta e="T562" id="Seg_5362" s="T561">oh</ta>
            <ta e="T563" id="Seg_5363" s="T562">where.to</ta>
            <ta e="T564" id="Seg_5364" s="T563">where.to</ta>
            <ta e="T566" id="Seg_5365" s="T565">go-FUT-1SG</ta>
            <ta e="T568" id="Seg_5366" s="T567">what.[NOM.SG]</ta>
            <ta e="T570" id="Seg_5367" s="T569">and</ta>
            <ta e="T571" id="Seg_5368" s="T570">I.NOM</ta>
            <ta e="T572" id="Seg_5369" s="T571">bread.[NOM.SG]</ta>
            <ta e="T573" id="Seg_5370" s="T572">NEG.EX.[3SG]</ta>
            <ta e="T576" id="Seg_5371" s="T575">bread.[NOM.SG]</ta>
            <ta e="T577" id="Seg_5372" s="T576">take-FUT-1SG</ta>
            <ta e="T579" id="Seg_5373" s="T578">and</ta>
            <ta e="T580" id="Seg_5374" s="T579">bring-IMP.2SG.O</ta>
            <ta e="T582" id="Seg_5375" s="T581">bring-IMP.2SG.O</ta>
            <ta e="T583" id="Seg_5376" s="T582">where.to</ta>
            <ta e="T584" id="Seg_5377" s="T583">go-FUT-2SG</ta>
            <ta e="T586" id="Seg_5378" s="T585">go-FUT-1SG</ta>
            <ta e="T587" id="Seg_5379" s="T586">where.to=INDEF</ta>
            <ta e="T589" id="Seg_5380" s="T588">sit-FUT-1SG</ta>
            <ta e="T590" id="Seg_5381" s="T589">and</ta>
            <ta e="T591" id="Seg_5382" s="T590">take-FUT-1SG</ta>
            <ta e="T593" id="Seg_5383" s="T592">all</ta>
            <ta e="T594" id="Seg_5384" s="T593">what.[NOM.SG]</ta>
            <ta e="T595" id="Seg_5385" s="T594">PTCL</ta>
            <ta e="T597" id="Seg_5386" s="T596">take-FUT-1SG</ta>
            <ta e="T599" id="Seg_5387" s="T598">and</ta>
            <ta e="T600" id="Seg_5388" s="T599">why</ta>
            <ta e="T601" id="Seg_5389" s="T600">so</ta>
            <ta e="T602" id="Seg_5390" s="T601">can-FUT-2SG</ta>
            <ta e="T604" id="Seg_5391" s="T603">so</ta>
            <ta e="T605" id="Seg_5392" s="T604">I.LAT</ta>
            <ta e="T606" id="Seg_5393" s="T605">bad.[NOM.SG]</ta>
            <ta e="T607" id="Seg_5394" s="T606">live-INF.LAT</ta>
            <ta e="T609" id="Seg_5395" s="T608">one.should</ta>
            <ta e="T610" id="Seg_5396" s="T609">go-INF.LAT</ta>
            <ta e="T611" id="Seg_5397" s="T610">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T612" id="Seg_5398" s="T611">house-LAT</ta>
            <ta e="T614" id="Seg_5399" s="T613">and</ta>
            <ta e="T615" id="Seg_5400" s="T614">what.[NOM.SG]</ta>
            <ta e="T616" id="Seg_5401" s="T615">house.[NOM.SG]</ta>
            <ta e="T617" id="Seg_5402" s="T616">give-FUT-2SG</ta>
            <ta e="T618" id="Seg_5403" s="T617">you.DAT</ta>
            <ta e="T620" id="Seg_5404" s="T619">what=INDEF</ta>
            <ta e="T621" id="Seg_5405" s="T620">give-FUT-2SG</ta>
            <ta e="T623" id="Seg_5406" s="T622">where.to</ta>
            <ta e="T625" id="Seg_5407" s="T624">and</ta>
            <ta e="T626" id="Seg_5408" s="T625">bring-IMP.2SG.O</ta>
            <ta e="T627" id="Seg_5409" s="T626">where.to</ta>
            <ta e="T629" id="Seg_5410" s="T628">well</ta>
            <ta e="T630" id="Seg_5411" s="T629">self</ta>
            <ta e="T631" id="Seg_5412" s="T630">house-LAT</ta>
            <ta e="T633" id="Seg_5413" s="T632">come-FUT-1SG</ta>
            <ta e="T634" id="Seg_5414" s="T633">live-RES-PRS-1SG</ta>
            <ta e="T640" id="Seg_5415" s="T639">bring-IMP.2SG.O</ta>
            <ta e="T641" id="Seg_5416" s="T640">I.LAT</ta>
            <ta e="T642" id="Seg_5417" s="T641">tobacco.[NOM.SG]</ta>
            <ta e="T643" id="Seg_5418" s="T642">smoke-INF.LAT</ta>
            <ta e="T645" id="Seg_5419" s="T644">I.NOM</ta>
            <ta e="T646" id="Seg_5420" s="T645">NEG.EX.[3SG]</ta>
            <ta e="T647" id="Seg_5421" s="T646">tobacco.[NOM.SG]</ta>
            <ta e="T650" id="Seg_5422" s="T649">and</ta>
            <ta e="T651" id="Seg_5423" s="T650">I.NOM</ta>
            <ta e="T652" id="Seg_5424" s="T651">NEG.EX.[3SG]</ta>
            <ta e="T654" id="Seg_5425" s="T653">no</ta>
            <ta e="T655" id="Seg_5426" s="T654">be-PRS.[3SG]</ta>
            <ta e="T656" id="Seg_5427" s="T655">be-PRS.[3SG]</ta>
            <ta e="T658" id="Seg_5428" s="T657">bring-IMP.2SG.O</ta>
            <ta e="T659" id="Seg_5429" s="T658">I.LAT</ta>
            <ta e="T661" id="Seg_5430" s="T660">I.NOM</ta>
            <ta e="T662" id="Seg_5431" s="T661">NEG.EX.[3SG]</ta>
            <ta e="T663" id="Seg_5432" s="T662">tobacco.[NOM.SG]</ta>
            <ta e="T665" id="Seg_5433" s="T664">and</ta>
            <ta e="T666" id="Seg_5434" s="T665">I.NOM</ta>
            <ta e="T667" id="Seg_5435" s="T666">smoke-INF.LAT</ta>
            <ta e="T668" id="Seg_5436" s="T667">want-FUT-1SG</ta>
            <ta e="T672" id="Seg_5437" s="T671">come-IMP.2SG</ta>
            <ta e="T673" id="Seg_5438" s="T672">I.LAT</ta>
            <ta e="T674" id="Seg_5439" s="T673">vodka.[NOM.SG]</ta>
            <ta e="T675" id="Seg_5440" s="T674">drink-INF.LAT</ta>
            <ta e="T677" id="Seg_5441" s="T676">and</ta>
            <ta e="T678" id="Seg_5442" s="T677">vodka.[NOM.SG]</ta>
            <ta e="T680" id="Seg_5443" s="T679">drink-INF.LAT</ta>
            <ta e="T681" id="Seg_5444" s="T680">one.should</ta>
            <ta e="T683" id="Seg_5445" s="T682">well</ta>
            <ta e="T684" id="Seg_5446" s="T683">one.should</ta>
            <ta e="T686" id="Seg_5447" s="T685">vodka.[NOM.SG]</ta>
            <ta e="T687" id="Seg_5448" s="T686">drink-INF.LAT</ta>
            <ta e="T688" id="Seg_5449" s="T687">so</ta>
            <ta e="T689" id="Seg_5450" s="T688">NEG</ta>
            <ta e="T690" id="Seg_5451" s="T689">fight-IMP.2SG</ta>
            <ta e="T692" id="Seg_5452" s="T691">you.NOM</ta>
            <ta e="T693" id="Seg_5453" s="T692">you.know</ta>
            <ta e="T694" id="Seg_5454" s="T693">bad.[NOM.SG]</ta>
            <ta e="T695" id="Seg_5455" s="T694">human.[NOM.SG]</ta>
            <ta e="T697" id="Seg_5456" s="T696">and</ta>
            <ta e="T698" id="Seg_5457" s="T697">I.NOM</ta>
            <ta e="T699" id="Seg_5458" s="T698">good</ta>
            <ta e="T700" id="Seg_5459" s="T699">human.[NOM.SG]</ta>
            <ta e="T702" id="Seg_5460" s="T701">I.NOM</ta>
            <ta e="T703" id="Seg_5461" s="T702">vodka.[NOM.SG]</ta>
            <ta e="T706" id="Seg_5462" s="T705">drink-PRS-1SG</ta>
            <ta e="T707" id="Seg_5463" s="T706">NEG</ta>
            <ta e="T708" id="Seg_5464" s="T707">fight-FUT-1SG</ta>
            <ta e="T710" id="Seg_5465" s="T709">and</ta>
            <ta e="T711" id="Seg_5466" s="T710">you.NOM</ta>
            <ta e="T712" id="Seg_5467" s="T711">always</ta>
            <ta e="T713" id="Seg_5468" s="T712">fight-PRS-2SG</ta>
            <ta e="T715" id="Seg_5469" s="T714">really</ta>
            <ta e="T716" id="Seg_5470" s="T715">NEG</ta>
            <ta e="T717" id="Seg_5471" s="T716">good</ta>
            <ta e="T718" id="Seg_5472" s="T717">so</ta>
            <ta e="T719" id="Seg_5473" s="T718">drink-INF.LAT</ta>
            <ta e="T721" id="Seg_5474" s="T720">drink-INF.LAT</ta>
            <ta e="T722" id="Seg_5475" s="T721">one.should</ta>
            <ta e="T723" id="Seg_5476" s="T722">not</ta>
            <ta e="T724" id="Seg_5477" s="T723">one.should</ta>
            <ta e="T725" id="Seg_5478" s="T724">fight-INF.LAT</ta>
            <ta e="T727" id="Seg_5479" s="T726">one.should</ta>
            <ta e="T728" id="Seg_5480" s="T727">good</ta>
            <ta e="T730" id="Seg_5481" s="T729">I.NOM</ta>
            <ta e="T733" id="Seg_5482" s="T732">beautiful.[NOM.SG]</ta>
            <ta e="T734" id="Seg_5483" s="T733">man.[NOM.SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-SAE">
            <ta e="T10" id="Seg_5484" s="T9">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T11" id="Seg_5485" s="T10">почему</ta>
            <ta e="T12" id="Seg_5486" s="T11">так</ta>
            <ta e="T13" id="Seg_5487" s="T12">сказать-%%-EP-3SG.O</ta>
            <ta e="T15" id="Seg_5488" s="T14">действительно</ta>
            <ta e="T16" id="Seg_5489" s="T15">плохой.[NOM.SG]</ta>
            <ta e="T18" id="Seg_5490" s="T17">действительно</ta>
            <ta e="T19" id="Seg_5491" s="T18">плохой.[NOM.SG]</ta>
            <ta e="T20" id="Seg_5492" s="T19">сказать-%%-EP-3SG.O</ta>
            <ta e="T22" id="Seg_5493" s="T21">почему</ta>
            <ta e="T23" id="Seg_5494" s="T22">так</ta>
            <ta e="T25" id="Seg_5495" s="T24">ты.NOM</ta>
            <ta e="T26" id="Seg_5496" s="T25">сказать-%%-EP-3SG.O</ta>
            <ta e="T28" id="Seg_5497" s="T27">нет</ta>
            <ta e="T29" id="Seg_5498" s="T28">я.NOM</ta>
            <ta e="T30" id="Seg_5499" s="T29">хороший</ta>
            <ta e="T31" id="Seg_5500" s="T30">сказать-%%-EP-1SG</ta>
            <ta e="T32" id="Seg_5501" s="T31">ты.DAT</ta>
            <ta e="T35" id="Seg_5502" s="T34">идти-EP-IMP.2SG</ta>
            <ta e="T36" id="Seg_5503" s="T35">там-LAT</ta>
            <ta e="T38" id="Seg_5504" s="T37">ну</ta>
            <ta e="T39" id="Seg_5505" s="T38">я.NOM</ta>
            <ta e="T40" id="Seg_5506" s="T39">NEG</ta>
            <ta e="T41" id="Seg_5507" s="T40">идти-FUT-1SG</ta>
            <ta e="T43" id="Seg_5508" s="T42">почему</ta>
            <ta e="T44" id="Seg_5509" s="T43">так</ta>
            <ta e="T45" id="Seg_5510" s="T44">почему</ta>
            <ta e="T47" id="Seg_5511" s="T46">я.NOM</ta>
            <ta e="T48" id="Seg_5512" s="T47">рука-NOM/GEN/ACC.1SG</ta>
            <ta e="T49" id="Seg_5513" s="T48">NEG.EX.[3SG]</ta>
            <ta e="T63" id="Seg_5514" s="T62">отец</ta>
            <ta e="T65" id="Seg_5515" s="T64">почему</ta>
            <ta e="T66" id="Seg_5516" s="T65">так</ta>
            <ta e="T67" id="Seg_5517" s="T66">ты.NOM</ta>
            <ta e="T69" id="Seg_5518" s="T68">идти-EP-IMP.2SG</ta>
            <ta e="T70" id="Seg_5519" s="T69">там</ta>
            <ta e="T72" id="Seg_5520" s="T71">нет</ta>
            <ta e="T73" id="Seg_5521" s="T72">я.NOM</ta>
            <ta e="T74" id="Seg_5522" s="T73">NEG</ta>
            <ta e="T75" id="Seg_5523" s="T74">идти-FUT-1SG</ta>
            <ta e="T77" id="Seg_5524" s="T76">я.NOM</ta>
            <ta e="T78" id="Seg_5525" s="T77">рука-NOM/GEN/ACC.1SG</ta>
            <ta e="T79" id="Seg_5526" s="T78">плохой.[NOM.SG]</ta>
            <ta e="T85" id="Seg_5527" s="T84">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T86" id="Seg_5528" s="T85">я.LAT</ta>
            <ta e="T87" id="Seg_5529" s="T86">водка.[NOM.SG]</ta>
            <ta e="T88" id="Seg_5530" s="T87">пить-INF.LAT</ta>
            <ta e="T90" id="Seg_5531" s="T89">нет</ta>
            <ta e="T91" id="Seg_5532" s="T90">NEG</ta>
            <ta e="T92" id="Seg_5533" s="T91">идти-FUT-1SG</ta>
            <ta e="T94" id="Seg_5534" s="T93">почему</ta>
            <ta e="T96" id="Seg_5535" s="T95">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T97" id="Seg_5536" s="T96">водка.[NOM.SG]</ta>
            <ta e="T98" id="Seg_5537" s="T97">красивый.[NOM.SG]</ta>
            <ta e="T100" id="Seg_5538" s="T99">красивый.[NOM.SG]</ta>
            <ta e="T101" id="Seg_5539" s="T100">пить-</ta>
            <ta e="T102" id="Seg_5540" s="T101">пить-INF.LAT</ta>
            <ta e="T104" id="Seg_5541" s="T103">нет</ta>
            <ta e="T105" id="Seg_5542" s="T104">я.LAT</ta>
            <ta e="T107" id="Seg_5543" s="T106">там</ta>
            <ta e="T108" id="Seg_5544" s="T107">бить-FUT-3SG.O</ta>
            <ta e="T110" id="Seg_5545" s="T109">нет</ta>
            <ta e="T111" id="Seg_5546" s="T110">я.NOM</ta>
            <ta e="T112" id="Seg_5547" s="T111">NEG</ta>
            <ta e="T113" id="Seg_5548" s="T112">дать-FUT-1SG</ta>
            <ta e="T114" id="Seg_5549" s="T113">ты.DAT</ta>
            <ta e="T115" id="Seg_5550" s="T114">бить-INF.LAT</ta>
            <ta e="T117" id="Seg_5551" s="T116">ну</ta>
            <ta e="T118" id="Seg_5552" s="T117">идти-FUT-1SG</ta>
            <ta e="T120" id="Seg_5553" s="T119">идти-FUT-1SG</ta>
            <ta e="T121" id="Seg_5554" s="T120">NEG</ta>
            <ta e="T122" id="Seg_5555" s="T121">дать-FUT-2SG</ta>
            <ta e="T124" id="Seg_5556" s="T123">нет</ta>
            <ta e="T125" id="Seg_5557" s="T124">NEG</ta>
            <ta e="T126" id="Seg_5558" s="T125">дать-FUT-1SG</ta>
            <ta e="T128" id="Seg_5559" s="T127">ну</ta>
            <ta e="T129" id="Seg_5560" s="T128">хороший</ta>
            <ta e="T131" id="Seg_5561" s="T130">хороший</ta>
            <ta e="T132" id="Seg_5562" s="T131">NEG</ta>
            <ta e="T133" id="Seg_5563" s="T132">дать-FUT-2SG</ta>
            <ta e="T134" id="Seg_5564" s="T133">так</ta>
            <ta e="T135" id="Seg_5565" s="T134">я.NOM</ta>
            <ta e="T136" id="Seg_5566" s="T135">идти-FUT-1SG</ta>
            <ta e="T144" id="Seg_5567" s="T143">я.NOM</ta>
            <ta e="T145" id="Seg_5568" s="T144">мать-1SG-COM</ta>
            <ta e="T146" id="Seg_5569" s="T145">NEG</ta>
            <ta e="T147" id="Seg_5570" s="T146">мочь-FUT-1SG</ta>
            <ta e="T149" id="Seg_5571" s="T148">я.NOM</ta>
            <ta e="T151" id="Seg_5572" s="T150">исчезнуть-FUT-1SG</ta>
            <ta e="T153" id="Seg_5573" s="T152">почему</ta>
            <ta e="T155" id="Seg_5574" s="T154">и</ta>
            <ta e="T156" id="Seg_5575" s="T155">этот.[NOM.SG]</ta>
            <ta e="T157" id="Seg_5576" s="T156">ругать-FUT.[3SG]</ta>
            <ta e="T158" id="Seg_5577" s="T157">сильно</ta>
            <ta e="T160" id="Seg_5578" s="T159">ну</ta>
            <ta e="T161" id="Seg_5579" s="T160">жить-IMP.2SG</ta>
            <ta e="T162" id="Seg_5580" s="T161">мать-COM-2SG</ta>
            <ta e="T164" id="Seg_5581" s="T163">жить-IMP.2SG</ta>
            <ta e="T166" id="Seg_5582" s="T165">почему</ta>
            <ta e="T167" id="Seg_5583" s="T166">так</ta>
            <ta e="T168" id="Seg_5584" s="T167">стать-FUT-2SG</ta>
            <ta e="T170" id="Seg_5585" s="T169">ты.NOM</ta>
            <ta e="T171" id="Seg_5586" s="T170">плохой</ta>
            <ta e="T172" id="Seg_5587" s="T171">мужчина.[NOM.SG]</ta>
            <ta e="T178" id="Seg_5588" s="T177">ты.NOM</ta>
            <ta e="T179" id="Seg_5589" s="T178">отец-NOM/GEN/ACC.2SG</ta>
            <ta e="T180" id="Seg_5590" s="T179">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T181" id="Seg_5591" s="T180">действительно</ta>
            <ta e="T182" id="Seg_5592" s="T181">плохой</ta>
            <ta e="T183" id="Seg_5593" s="T182">мужчина.[NOM.SG]</ta>
            <ta e="T185" id="Seg_5594" s="T184">а</ta>
            <ta e="T186" id="Seg_5595" s="T185">я.NOM</ta>
            <ta e="T187" id="Seg_5596" s="T186">хороший</ta>
            <ta e="T189" id="Seg_5597" s="T188">нет</ta>
            <ta e="T190" id="Seg_5598" s="T189">я.GEN</ta>
            <ta e="T191" id="Seg_5599" s="T190">хороший</ta>
            <ta e="T192" id="Seg_5600" s="T191">ты.GEN</ta>
            <ta e="T193" id="Seg_5601" s="T192">плохой</ta>
            <ta e="T196" id="Seg_5602" s="T195">почему</ta>
            <ta e="T197" id="Seg_5603" s="T196">так</ta>
            <ta e="T199" id="Seg_5604" s="T198">сказать-RES-2SG</ta>
            <ta e="T201" id="Seg_5605" s="T200">черт.[NOM.SG]</ta>
            <ta e="T203" id="Seg_5606" s="T202">сказать-EP-IMP.2SG</ta>
            <ta e="T204" id="Seg_5607" s="T203">хороший</ta>
            <ta e="T207" id="Seg_5608" s="T206">сильно</ta>
            <ta e="T208" id="Seg_5609" s="T207">плохой</ta>
            <ta e="T209" id="Seg_5610" s="T208">мужчина.[NOM.SG]</ta>
            <ta e="T211" id="Seg_5611" s="T210">черт.[NOM.SG]</ta>
            <ta e="T215" id="Seg_5612" s="T214">я.NOM</ta>
            <ta e="T216" id="Seg_5613" s="T215">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T217" id="Seg_5614" s="T216">пять.[NOM.SG]</ta>
            <ta e="T218" id="Seg_5615" s="T217">пять.[NOM.SG]</ta>
            <ta e="T219" id="Seg_5616" s="T218">мальчик.[NOM.SG]</ta>
            <ta e="T220" id="Seg_5617" s="T219">там</ta>
            <ta e="T226" id="Seg_5618" s="T225">там</ta>
            <ta e="T227" id="Seg_5619" s="T226">мочь-PST.[3SG]</ta>
            <ta e="T228" id="Seg_5620" s="T227">три.[NOM.SG]</ta>
            <ta e="T229" id="Seg_5621" s="T228">девушка.[NOM.SG]</ta>
            <ta e="T230" id="Seg_5622" s="T229">два.[NOM.SG]</ta>
            <ta e="T231" id="Seg_5623" s="T230">мальчик.[NOM.SG]</ta>
            <ta e="T233" id="Seg_5624" s="T232">один.[NOM.SG]</ta>
            <ta e="T234" id="Seg_5625" s="T233">мальчик.[NOM.SG]</ta>
            <ta e="T235" id="Seg_5626" s="T234">умереть-RES-PST.[3SG]</ta>
            <ta e="T237" id="Seg_5627" s="T236">и</ta>
            <ta e="T238" id="Seg_5628" s="T237">девушка.[NOM.SG]</ta>
            <ta e="T250" id="Seg_5629" s="T249">два.[NOM.SG]</ta>
            <ta e="T251" id="Seg_5630" s="T250">девушка.[NOM.SG]</ta>
            <ta e="T252" id="Seg_5631" s="T251">умереть-RES-PST.[3SG]</ta>
            <ta e="T254" id="Seg_5632" s="T253">а</ta>
            <ta e="T255" id="Seg_5633" s="T254">один.[NOM.SG]</ta>
            <ta e="T256" id="Seg_5634" s="T255">нет</ta>
            <ta e="T258" id="Seg_5635" s="T257">сейчас</ta>
            <ta e="T259" id="Seg_5636" s="T258">один.[NOM.SG]</ta>
            <ta e="T260" id="Seg_5637" s="T259">мальчик.[NOM.SG]</ta>
            <ta e="T261" id="Seg_5638" s="T260">один.[NOM.SG]</ta>
            <ta e="T262" id="Seg_5639" s="T261">девушка.[NOM.SG]</ta>
            <ta e="T264" id="Seg_5640" s="T263">о</ta>
            <ta e="T265" id="Seg_5641" s="T264">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T266" id="Seg_5642" s="T265">сильно</ta>
            <ta e="T281" id="Seg_5643" s="T280">сидеть-IMP.2SG</ta>
            <ta e="T282" id="Seg_5644" s="T281">желтый</ta>
            <ta e="T283" id="Seg_5645" s="T282">вода.[NOM.SG]</ta>
            <ta e="T284" id="Seg_5646" s="T283">съесть-INF.LAT</ta>
            <ta e="T286" id="Seg_5647" s="T285">ну</ta>
            <ta e="T287" id="Seg_5648" s="T286">я.NOM</ta>
            <ta e="T289" id="Seg_5649" s="T288">есть-PST-1SG</ta>
            <ta e="T291" id="Seg_5650" s="T290">ну</ta>
            <ta e="T292" id="Seg_5651" s="T291">мясо.[NOM.SG]</ta>
            <ta e="T294" id="Seg_5652" s="T293">мясо.[NOM.SG]</ta>
            <ta e="T295" id="Seg_5653" s="T294">хороший</ta>
            <ta e="T297" id="Seg_5654" s="T296">мясо.[NOM.SG]</ta>
            <ta e="T298" id="Seg_5655" s="T297">хлеб.[NOM.SG]</ta>
            <ta e="T300" id="Seg_5656" s="T299">съесть-INF.LAT</ta>
            <ta e="T302" id="Seg_5657" s="T301">ну</ta>
            <ta e="T303" id="Seg_5658" s="T302">я.NOM</ta>
            <ta e="T304" id="Seg_5659" s="T303">только</ta>
            <ta e="T305" id="Seg_5660" s="T304">есть-PST-1SG</ta>
            <ta e="T311" id="Seg_5661" s="T310">есть-PST-1SG</ta>
            <ta e="T312" id="Seg_5662" s="T311">есть-%%-IMP.2SG</ta>
            <ta e="T313" id="Seg_5663" s="T312">я.NOM-COM</ta>
            <ta e="T315" id="Seg_5664" s="T314">тот.[NOM.SG]</ta>
            <ta e="T316" id="Seg_5665" s="T315">хлеб.[NOM.SG]</ta>
            <ta e="T318" id="Seg_5666" s="T317">тот.[NOM.SG]</ta>
            <ta e="T319" id="Seg_5667" s="T318">мясо.[NOM.SG]</ta>
            <ta e="T321" id="Seg_5668" s="T320">сидеть-</ta>
            <ta e="T322" id="Seg_5669" s="T321">есть-INF.LAT</ta>
            <ta e="T323" id="Seg_5670" s="T322">можно</ta>
            <ta e="T330" id="Seg_5671" s="T329">прийти-IMP.2SG</ta>
            <ta e="T331" id="Seg_5672" s="T330">здесь</ta>
            <ta e="T332" id="Seg_5673" s="T331">я.LAT</ta>
            <ta e="T334" id="Seg_5674" s="T333">а</ta>
            <ta e="T335" id="Seg_5675" s="T334">что.[NOM.SG]</ta>
            <ta e="T337" id="Seg_5676" s="T336">что=INDEF</ta>
            <ta e="T338" id="Seg_5677" s="T337">сказать-FUT-2SG</ta>
            <ta e="T340" id="Seg_5678" s="T339">нет</ta>
            <ta e="T341" id="Seg_5679" s="T340">и</ta>
            <ta e="T342" id="Seg_5680" s="T341">ты.NOM</ta>
            <ta e="T343" id="Seg_5681" s="T342">сказать-IMP.2SG</ta>
            <ta e="T344" id="Seg_5682" s="T343">я.LAT</ta>
            <ta e="T346" id="Seg_5683" s="T345">а</ta>
            <ta e="T347" id="Seg_5684" s="T346">я.NOM</ta>
            <ta e="T348" id="Seg_5685" s="T347">что.[NOM.SG]=INDEF</ta>
            <ta e="T349" id="Seg_5686" s="T348">NEG</ta>
            <ta e="T350" id="Seg_5687" s="T349">знать-1SG</ta>
            <ta e="T352" id="Seg_5688" s="T351">почему</ta>
            <ta e="T353" id="Seg_5689" s="T352">так</ta>
            <ta e="T354" id="Seg_5690" s="T353">что.[NOM.SG]=INDEF</ta>
            <ta e="T355" id="Seg_5691" s="T354">NEG</ta>
            <ta e="T357" id="Seg_5692" s="T356">а</ta>
            <ta e="T358" id="Seg_5693" s="T357">я.NOM</ta>
            <ta e="T359" id="Seg_5694" s="T358">сказать-FUT-1SG</ta>
            <ta e="T361" id="Seg_5695" s="T360">ты.NOM</ta>
            <ta e="T362" id="Seg_5696" s="T361">хороший</ta>
            <ta e="T363" id="Seg_5697" s="T362">мужчина.[NOM.SG]</ta>
            <ta e="T365" id="Seg_5698" s="T364">а</ta>
            <ta e="T366" id="Seg_5699" s="T365">я.NOM</ta>
            <ta e="T367" id="Seg_5700" s="T366">плохой.[NOM.SG]</ta>
            <ta e="T368" id="Seg_5701" s="T367">нет</ta>
            <ta e="T369" id="Seg_5702" s="T368">ты.NOM</ta>
            <ta e="T370" id="Seg_5703" s="T369">хороший</ta>
            <ta e="T372" id="Seg_5704" s="T371">почему</ta>
            <ta e="T373" id="Seg_5705" s="T372">так</ta>
            <ta e="T374" id="Seg_5706" s="T373">сказать-RES-2SG</ta>
            <ta e="T376" id="Seg_5707" s="T375">плохой.[NOM.SG]</ta>
            <ta e="T377" id="Seg_5708" s="T376">плохой.[NOM.SG]</ta>
            <ta e="T379" id="Seg_5709" s="T378">так</ta>
            <ta e="T380" id="Seg_5710" s="T379">NEG</ta>
            <ta e="T381" id="Seg_5711" s="T380">сказать-IMP.2SG</ta>
            <ta e="T383" id="Seg_5712" s="T382">плохой.[NOM.SG]</ta>
            <ta e="T385" id="Seg_5713" s="T384">надо</ta>
            <ta e="T386" id="Seg_5714" s="T385">сказать-INF.LAT</ta>
            <ta e="T387" id="Seg_5715" s="T386">хороший</ta>
            <ta e="T389" id="Seg_5716" s="T388">мужчина.[NOM.SG]</ta>
            <ta e="T391" id="Seg_5717" s="T390">а</ta>
            <ta e="T392" id="Seg_5718" s="T391">ты.NOM</ta>
            <ta e="T393" id="Seg_5719" s="T392">всегда</ta>
            <ta e="T394" id="Seg_5720" s="T393">что.[NOM.SG]=INDEF</ta>
            <ta e="T395" id="Seg_5721" s="T394">NEG</ta>
            <ta e="T396" id="Seg_5722" s="T395">сказать-RES-2SG</ta>
            <ta e="T403" id="Seg_5723" s="T402">ягода.[NOM.SG]</ta>
            <ta e="T406" id="Seg_5724" s="T405">черный</ta>
            <ta e="T407" id="Seg_5725" s="T406">ягода.[NOM.SG]</ta>
            <ta e="T409" id="Seg_5726" s="T408">а</ta>
            <ta e="T411" id="Seg_5727" s="T410">ягода.[NOM.SG]</ta>
            <ta e="T413" id="Seg_5728" s="T412">%%-PRS-1SG</ta>
            <ta e="T415" id="Seg_5729" s="T414">ягода.[NOM.SG]</ta>
            <ta e="T417" id="Seg_5730" s="T416">%%-PRS-1SG</ta>
            <ta e="T419" id="Seg_5731" s="T418">надо</ta>
            <ta e="T420" id="Seg_5732" s="T419">хлеб.[NOM.SG]</ta>
            <ta e="T424" id="Seg_5733" s="T423">хлеб.[NOM.SG]</ta>
            <ta e="T426" id="Seg_5734" s="T425">и</ta>
            <ta e="T427" id="Seg_5735" s="T426">черный</ta>
            <ta e="T428" id="Seg_5736" s="T427">хлеб.[NOM.SG]</ta>
            <ta e="T430" id="Seg_5737" s="T429">нет</ta>
            <ta e="T431" id="Seg_5738" s="T430">черный</ta>
            <ta e="T432" id="Seg_5739" s="T431">хлеб.[NOM.SG]</ta>
            <ta e="T435" id="Seg_5740" s="T434">надо</ta>
            <ta e="T436" id="Seg_5741" s="T435">хороший</ta>
            <ta e="T437" id="Seg_5742" s="T436">хлеб.[NOM.SG]</ta>
            <ta e="T452" id="Seg_5743" s="T451">я.NOM</ta>
            <ta e="T453" id="Seg_5744" s="T452">голова-NOM/GEN/ACC.1SG</ta>
            <ta e="T454" id="Seg_5745" s="T453">болеть-PRS-3SG.O</ta>
            <ta e="T456" id="Seg_5746" s="T455">сейчас</ta>
            <ta e="T457" id="Seg_5747" s="T456">я.NOM</ta>
            <ta e="T458" id="Seg_5748" s="T457">голова-NOM/GEN/ACC.1SG</ta>
            <ta e="T459" id="Seg_5749" s="T458">хороший</ta>
            <ta e="T463" id="Seg_5750" s="T462">куда</ta>
            <ta e="T464" id="Seg_5751" s="T463">идти-FUT-2SG</ta>
            <ta e="T466" id="Seg_5752" s="T465">прийти-FUT-1SG</ta>
            <ta e="T468" id="Seg_5753" s="T467">почему</ta>
            <ta e="T469" id="Seg_5754" s="T468">так</ta>
            <ta e="T471" id="Seg_5755" s="T470">так</ta>
            <ta e="T472" id="Seg_5756" s="T471">что.[NOM.SG]</ta>
            <ta e="T474" id="Seg_5757" s="T473">прийти-IMP.2SG</ta>
            <ta e="T476" id="Seg_5758" s="T475">нет</ta>
            <ta e="T478" id="Seg_5759" s="T477">я.NOM</ta>
            <ta e="T480" id="Seg_5760" s="T479">исчезнуть-FUT-1SG</ta>
            <ta e="T483" id="Seg_5761" s="T482">исчезнуть-FUT</ta>
            <ta e="T485" id="Seg_5762" s="T484">ты.NOM</ta>
            <ta e="T487" id="Seg_5763" s="T486">а</ta>
            <ta e="T489" id="Seg_5764" s="T488">ты.NOM</ta>
            <ta e="T490" id="Seg_5765" s="T489">PTCL</ta>
            <ta e="T492" id="Seg_5766" s="T491">PTCL</ta>
            <ta e="T493" id="Seg_5767" s="T492">мочь-FUT-2SG</ta>
            <ta e="T495" id="Seg_5768" s="T494">ну</ta>
            <ta e="T496" id="Seg_5769" s="T495">а</ta>
            <ta e="T497" id="Seg_5770" s="T496">ты.NOM</ta>
            <ta e="T498" id="Seg_5771" s="T497">так</ta>
            <ta e="T499" id="Seg_5772" s="T498">NEG</ta>
            <ta e="T500" id="Seg_5773" s="T499">стать-FUT-2SG</ta>
            <ta e="T504" id="Seg_5774" s="T503">я.GEN</ta>
            <ta e="T505" id="Seg_5775" s="T504">корова.[NOM.SG]</ta>
            <ta e="T506" id="Seg_5776" s="T505">хороший</ta>
            <ta e="T507" id="Seg_5777" s="T506">корова.[NOM.SG]</ta>
            <ta e="T509" id="Seg_5778" s="T508">и</ta>
            <ta e="T510" id="Seg_5779" s="T509">молоко.[NOM.SG]</ta>
            <ta e="T511" id="Seg_5780" s="T510">я.LAT</ta>
            <ta e="T512" id="Seg_5781" s="T511">дать-PRS-3SG.O</ta>
            <ta e="T514" id="Seg_5782" s="T513">молоко.[NOM.SG]</ta>
            <ta e="T515" id="Seg_5783" s="T514">я.LAT</ta>
            <ta e="T516" id="Seg_5784" s="T515">дать-PRS-3SG.O</ta>
            <ta e="T518" id="Seg_5785" s="T517">хороший</ta>
            <ta e="T520" id="Seg_5786" s="T519">куда</ta>
            <ta e="T521" id="Seg_5787" s="T520">этот.[NOM.SG]</ta>
            <ta e="T522" id="Seg_5788" s="T521">молоко.[NOM.SG]</ta>
            <ta e="T524" id="Seg_5789" s="T523">и</ta>
            <ta e="T525" id="Seg_5790" s="T524">куда</ta>
            <ta e="T526" id="Seg_5791" s="T525">я.NOM</ta>
            <ta e="T527" id="Seg_5792" s="T526">есть-FUT-1SG</ta>
            <ta e="T528" id="Seg_5793" s="T527">тот.[NOM.SG]</ta>
            <ta e="T529" id="Seg_5794" s="T528">молоко.[NOM.SG]</ta>
            <ta e="T556" id="Seg_5795" s="T555">я.NOM</ta>
            <ta e="T558" id="Seg_5796" s="T557">исчезнуть-FUT-1SG</ta>
            <ta e="T560" id="Seg_5797" s="T559">куда</ta>
            <ta e="T562" id="Seg_5798" s="T561">о</ta>
            <ta e="T563" id="Seg_5799" s="T562">куда</ta>
            <ta e="T564" id="Seg_5800" s="T563">куда</ta>
            <ta e="T566" id="Seg_5801" s="T565">идти-FUT-1SG</ta>
            <ta e="T568" id="Seg_5802" s="T567">что.[NOM.SG]</ta>
            <ta e="T570" id="Seg_5803" s="T569">и</ta>
            <ta e="T571" id="Seg_5804" s="T570">я.NOM</ta>
            <ta e="T572" id="Seg_5805" s="T571">хлеб.[NOM.SG]</ta>
            <ta e="T573" id="Seg_5806" s="T572">NEG.EX.[3SG]</ta>
            <ta e="T576" id="Seg_5807" s="T575">хлеб.[NOM.SG]</ta>
            <ta e="T577" id="Seg_5808" s="T576">взять-FUT-1SG</ta>
            <ta e="T579" id="Seg_5809" s="T578">а</ta>
            <ta e="T580" id="Seg_5810" s="T579">принести-IMP.2SG.O</ta>
            <ta e="T582" id="Seg_5811" s="T581">принести-IMP.2SG.O</ta>
            <ta e="T583" id="Seg_5812" s="T582">куда</ta>
            <ta e="T584" id="Seg_5813" s="T583">идти-FUT-2SG</ta>
            <ta e="T586" id="Seg_5814" s="T585">идти-FUT-1SG</ta>
            <ta e="T587" id="Seg_5815" s="T586">куда=INDEF</ta>
            <ta e="T589" id="Seg_5816" s="T588">сидеть-FUT-1SG</ta>
            <ta e="T590" id="Seg_5817" s="T589">и</ta>
            <ta e="T591" id="Seg_5818" s="T590">взять-FUT-1SG</ta>
            <ta e="T593" id="Seg_5819" s="T592">весь</ta>
            <ta e="T594" id="Seg_5820" s="T593">что.[NOM.SG]</ta>
            <ta e="T595" id="Seg_5821" s="T594">PTCL</ta>
            <ta e="T597" id="Seg_5822" s="T596">взять-FUT-1SG</ta>
            <ta e="T599" id="Seg_5823" s="T598">а</ta>
            <ta e="T600" id="Seg_5824" s="T599">почему</ta>
            <ta e="T601" id="Seg_5825" s="T600">так</ta>
            <ta e="T602" id="Seg_5826" s="T601">мочь-FUT-2SG</ta>
            <ta e="T604" id="Seg_5827" s="T603">так</ta>
            <ta e="T605" id="Seg_5828" s="T604">я.LAT</ta>
            <ta e="T606" id="Seg_5829" s="T605">плохой.[NOM.SG]</ta>
            <ta e="T607" id="Seg_5830" s="T606">жить-INF.LAT</ta>
            <ta e="T609" id="Seg_5831" s="T608">надо</ta>
            <ta e="T610" id="Seg_5832" s="T609">идти-INF.LAT</ta>
            <ta e="T611" id="Seg_5833" s="T610">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T612" id="Seg_5834" s="T611">дом-LAT</ta>
            <ta e="T614" id="Seg_5835" s="T613">а</ta>
            <ta e="T615" id="Seg_5836" s="T614">что.[NOM.SG]</ta>
            <ta e="T616" id="Seg_5837" s="T615">дом.[NOM.SG]</ta>
            <ta e="T617" id="Seg_5838" s="T616">дать-FUT-2SG</ta>
            <ta e="T618" id="Seg_5839" s="T617">ты.DAT</ta>
            <ta e="T620" id="Seg_5840" s="T619">что=INDEF</ta>
            <ta e="T621" id="Seg_5841" s="T620">дать-FUT-2SG</ta>
            <ta e="T623" id="Seg_5842" s="T622">куда</ta>
            <ta e="T625" id="Seg_5843" s="T624">а</ta>
            <ta e="T626" id="Seg_5844" s="T625">принести-IMP.2SG.O</ta>
            <ta e="T627" id="Seg_5845" s="T626">куда</ta>
            <ta e="T629" id="Seg_5846" s="T628">ну</ta>
            <ta e="T630" id="Seg_5847" s="T629">сам</ta>
            <ta e="T631" id="Seg_5848" s="T630">дом-LAT</ta>
            <ta e="T633" id="Seg_5849" s="T632">прийти-FUT-1SG</ta>
            <ta e="T634" id="Seg_5850" s="T633">жить-RES-PRS-1SG</ta>
            <ta e="T640" id="Seg_5851" s="T639">принести-IMP.2SG.O</ta>
            <ta e="T641" id="Seg_5852" s="T640">я.LAT</ta>
            <ta e="T642" id="Seg_5853" s="T641">табак.[NOM.SG]</ta>
            <ta e="T643" id="Seg_5854" s="T642">курить-INF.LAT</ta>
            <ta e="T645" id="Seg_5855" s="T644">я.NOM</ta>
            <ta e="T646" id="Seg_5856" s="T645">NEG.EX.[3SG]</ta>
            <ta e="T647" id="Seg_5857" s="T646">табак.[NOM.SG]</ta>
            <ta e="T650" id="Seg_5858" s="T649">и</ta>
            <ta e="T651" id="Seg_5859" s="T650">я.NOM</ta>
            <ta e="T652" id="Seg_5860" s="T651">NEG.EX.[3SG]</ta>
            <ta e="T654" id="Seg_5861" s="T653">нет</ta>
            <ta e="T655" id="Seg_5862" s="T654">быть-PRS.[3SG]</ta>
            <ta e="T656" id="Seg_5863" s="T655">быть-PRS.[3SG]</ta>
            <ta e="T658" id="Seg_5864" s="T657">принести-IMP.2SG.O</ta>
            <ta e="T659" id="Seg_5865" s="T658">я.LAT</ta>
            <ta e="T661" id="Seg_5866" s="T660">я.NOM</ta>
            <ta e="T662" id="Seg_5867" s="T661">NEG.EX.[3SG]</ta>
            <ta e="T663" id="Seg_5868" s="T662">табак.[NOM.SG]</ta>
            <ta e="T665" id="Seg_5869" s="T664">а</ta>
            <ta e="T666" id="Seg_5870" s="T665">я.NOM</ta>
            <ta e="T667" id="Seg_5871" s="T666">курить-INF.LAT</ta>
            <ta e="T668" id="Seg_5872" s="T667">мочь-FUT-1SG</ta>
            <ta e="T672" id="Seg_5873" s="T671">прийти-IMP.2SG</ta>
            <ta e="T673" id="Seg_5874" s="T672">я.LAT</ta>
            <ta e="T674" id="Seg_5875" s="T673">водка.[NOM.SG]</ta>
            <ta e="T675" id="Seg_5876" s="T674">пить-INF.LAT</ta>
            <ta e="T677" id="Seg_5877" s="T676">и</ta>
            <ta e="T678" id="Seg_5878" s="T677">водка.[NOM.SG]</ta>
            <ta e="T680" id="Seg_5879" s="T679">пить-INF.LAT</ta>
            <ta e="T681" id="Seg_5880" s="T680">надо</ta>
            <ta e="T683" id="Seg_5881" s="T682">ну</ta>
            <ta e="T684" id="Seg_5882" s="T683">надо</ta>
            <ta e="T686" id="Seg_5883" s="T685">водка.[NOM.SG]</ta>
            <ta e="T687" id="Seg_5884" s="T686">пить-INF.LAT</ta>
            <ta e="T688" id="Seg_5885" s="T687">так</ta>
            <ta e="T689" id="Seg_5886" s="T688">NEG</ta>
            <ta e="T690" id="Seg_5887" s="T689">бороться-IMP.2SG</ta>
            <ta e="T692" id="Seg_5888" s="T691">ты.NOM</ta>
            <ta e="T693" id="Seg_5889" s="T692">ведь</ta>
            <ta e="T694" id="Seg_5890" s="T693">плохой.[NOM.SG]</ta>
            <ta e="T695" id="Seg_5891" s="T694">человек.[NOM.SG]</ta>
            <ta e="T697" id="Seg_5892" s="T696">а</ta>
            <ta e="T698" id="Seg_5893" s="T697">я.NOM</ta>
            <ta e="T699" id="Seg_5894" s="T698">хороший</ta>
            <ta e="T700" id="Seg_5895" s="T699">человек.[NOM.SG]</ta>
            <ta e="T702" id="Seg_5896" s="T701">я.NOM</ta>
            <ta e="T703" id="Seg_5897" s="T702">водка.[NOM.SG]</ta>
            <ta e="T706" id="Seg_5898" s="T705">пить-PRS-1SG</ta>
            <ta e="T707" id="Seg_5899" s="T706">NEG</ta>
            <ta e="T708" id="Seg_5900" s="T707">бороться-FUT-1SG</ta>
            <ta e="T710" id="Seg_5901" s="T709">а</ta>
            <ta e="T711" id="Seg_5902" s="T710">ты.NOM</ta>
            <ta e="T712" id="Seg_5903" s="T711">всегда</ta>
            <ta e="T713" id="Seg_5904" s="T712">бороться-PRS-2SG</ta>
            <ta e="T715" id="Seg_5905" s="T714">действительно</ta>
            <ta e="T716" id="Seg_5906" s="T715">NEG</ta>
            <ta e="T717" id="Seg_5907" s="T716">хороший</ta>
            <ta e="T718" id="Seg_5908" s="T717">так</ta>
            <ta e="T719" id="Seg_5909" s="T718">пить-INF.LAT</ta>
            <ta e="T721" id="Seg_5910" s="T720">пить-INF.LAT</ta>
            <ta e="T722" id="Seg_5911" s="T721">надо</ta>
            <ta e="T723" id="Seg_5912" s="T722">не</ta>
            <ta e="T724" id="Seg_5913" s="T723">надо</ta>
            <ta e="T725" id="Seg_5914" s="T724">бороться-INF.LAT</ta>
            <ta e="T727" id="Seg_5915" s="T726">надо</ta>
            <ta e="T728" id="Seg_5916" s="T727">хороший</ta>
            <ta e="T730" id="Seg_5917" s="T729">я.NOM</ta>
            <ta e="T733" id="Seg_5918" s="T732">красивый.[NOM.SG]</ta>
            <ta e="T734" id="Seg_5919" s="T733">мужчина.[NOM.SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-SAE">
            <ta e="T10" id="Seg_5920" s="T9">n-n:case.poss</ta>
            <ta e="T11" id="Seg_5921" s="T10">que</ta>
            <ta e="T12" id="Seg_5922" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_5923" s="T12">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T15" id="Seg_5924" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_5925" s="T15">adj.[n:case]</ta>
            <ta e="T18" id="Seg_5926" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_5927" s="T18">adj.[n:case]</ta>
            <ta e="T20" id="Seg_5928" s="T19">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T22" id="Seg_5929" s="T21">que</ta>
            <ta e="T23" id="Seg_5930" s="T22">ptcl</ta>
            <ta e="T25" id="Seg_5931" s="T24">pers</ta>
            <ta e="T26" id="Seg_5932" s="T25">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T28" id="Seg_5933" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_5934" s="T28">pers</ta>
            <ta e="T30" id="Seg_5935" s="T29">adj</ta>
            <ta e="T31" id="Seg_5936" s="T30">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T32" id="Seg_5937" s="T31">pers</ta>
            <ta e="T35" id="Seg_5938" s="T34">v-v:ins-v:mood.pn</ta>
            <ta e="T36" id="Seg_5939" s="T35">adv-n:case</ta>
            <ta e="T38" id="Seg_5940" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_5941" s="T38">pers</ta>
            <ta e="T40" id="Seg_5942" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_5943" s="T40">v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_5944" s="T42">que</ta>
            <ta e="T44" id="Seg_5945" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_5946" s="T44">que</ta>
            <ta e="T47" id="Seg_5947" s="T46">pers</ta>
            <ta e="T48" id="Seg_5948" s="T47">n-n:case.poss</ta>
            <ta e="T49" id="Seg_5949" s="T48">v.[v:pn]</ta>
            <ta e="T63" id="Seg_5950" s="T62">n</ta>
            <ta e="T65" id="Seg_5951" s="T64">que</ta>
            <ta e="T66" id="Seg_5952" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_5953" s="T66">pers</ta>
            <ta e="T69" id="Seg_5954" s="T68">v-v:ins-v:mood.pn</ta>
            <ta e="T70" id="Seg_5955" s="T69">adv</ta>
            <ta e="T72" id="Seg_5956" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_5957" s="T72">pers</ta>
            <ta e="T74" id="Seg_5958" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_5959" s="T74">v-v:tense-v:pn</ta>
            <ta e="T77" id="Seg_5960" s="T76">pers</ta>
            <ta e="T78" id="Seg_5961" s="T77">n-n:case.poss</ta>
            <ta e="T79" id="Seg_5962" s="T78">adj.[n:case]</ta>
            <ta e="T85" id="Seg_5963" s="T84">v-v:mood-v:pn</ta>
            <ta e="T86" id="Seg_5964" s="T85">pers</ta>
            <ta e="T87" id="Seg_5965" s="T86">n.[n:case]</ta>
            <ta e="T88" id="Seg_5966" s="T87">v-v:n.fin</ta>
            <ta e="T90" id="Seg_5967" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_5968" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_5969" s="T91">v-v:tense-v:pn</ta>
            <ta e="T94" id="Seg_5970" s="T93">que</ta>
            <ta e="T96" id="Seg_5971" s="T95">v-v:mood-v:pn</ta>
            <ta e="T97" id="Seg_5972" s="T96">n.[n:case]</ta>
            <ta e="T98" id="Seg_5973" s="T97">adj.[n:case]</ta>
            <ta e="T100" id="Seg_5974" s="T99">adj.[n:case]</ta>
            <ta e="T101" id="Seg_5975" s="T100">v</ta>
            <ta e="T102" id="Seg_5976" s="T101">v-v:n.fin</ta>
            <ta e="T104" id="Seg_5977" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_5978" s="T104">pers</ta>
            <ta e="T107" id="Seg_5979" s="T106">adv</ta>
            <ta e="T108" id="Seg_5980" s="T107">v-v:tense-v:pn</ta>
            <ta e="T110" id="Seg_5981" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_5982" s="T110">pers</ta>
            <ta e="T112" id="Seg_5983" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_5984" s="T112">v-v:tense-v:pn</ta>
            <ta e="T114" id="Seg_5985" s="T113">pers</ta>
            <ta e="T115" id="Seg_5986" s="T114">v-v:n.fin</ta>
            <ta e="T117" id="Seg_5987" s="T116">ptcl</ta>
            <ta e="T118" id="Seg_5988" s="T117">v-v:tense-v:pn</ta>
            <ta e="T120" id="Seg_5989" s="T119">v-v:tense-v:pn</ta>
            <ta e="T121" id="Seg_5990" s="T120">ptcl</ta>
            <ta e="T122" id="Seg_5991" s="T121">v-v:tense-v:pn</ta>
            <ta e="T124" id="Seg_5992" s="T123">ptcl</ta>
            <ta e="T125" id="Seg_5993" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_5994" s="T125">v-v:tense-v:pn</ta>
            <ta e="T128" id="Seg_5995" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_5996" s="T128">adj</ta>
            <ta e="T131" id="Seg_5997" s="T130">adj</ta>
            <ta e="T132" id="Seg_5998" s="T131">ptcl</ta>
            <ta e="T133" id="Seg_5999" s="T132">v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_6000" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_6001" s="T134">pers</ta>
            <ta e="T136" id="Seg_6002" s="T135">v-v:tense-v:pn</ta>
            <ta e="T144" id="Seg_6003" s="T143">pers</ta>
            <ta e="T145" id="Seg_6004" s="T144">n-n:case.poss-n:case</ta>
            <ta e="T146" id="Seg_6005" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_6006" s="T146">v-v:tense-v:pn</ta>
            <ta e="T149" id="Seg_6007" s="T148">pers</ta>
            <ta e="T151" id="Seg_6008" s="T150">v-v:tense-v:pn</ta>
            <ta e="T153" id="Seg_6009" s="T152">que</ta>
            <ta e="T155" id="Seg_6010" s="T154">conj</ta>
            <ta e="T156" id="Seg_6011" s="T155">dempro.[n:case]</ta>
            <ta e="T157" id="Seg_6012" s="T156">v-v:tense.[v:pn]</ta>
            <ta e="T158" id="Seg_6013" s="T157">adv</ta>
            <ta e="T160" id="Seg_6014" s="T159">ptcl</ta>
            <ta e="T161" id="Seg_6015" s="T160">v-v:mood.pn</ta>
            <ta e="T162" id="Seg_6016" s="T161">n-n:case-n:case.poss</ta>
            <ta e="T164" id="Seg_6017" s="T163">v-v:mood.pn</ta>
            <ta e="T166" id="Seg_6018" s="T165">que</ta>
            <ta e="T167" id="Seg_6019" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_6020" s="T167">v-v:tense-v:pn</ta>
            <ta e="T170" id="Seg_6021" s="T169">pers</ta>
            <ta e="T171" id="Seg_6022" s="T170">adj</ta>
            <ta e="T172" id="Seg_6023" s="T171">n.[n:case]</ta>
            <ta e="T178" id="Seg_6024" s="T177">pers</ta>
            <ta e="T179" id="Seg_6025" s="T178">n-n:case.poss</ta>
            <ta e="T180" id="Seg_6026" s="T179">n-n:case.poss</ta>
            <ta e="T181" id="Seg_6027" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_6028" s="T181">adj</ta>
            <ta e="T183" id="Seg_6029" s="T182">n.[n:case]</ta>
            <ta e="T185" id="Seg_6030" s="T184">conj</ta>
            <ta e="T186" id="Seg_6031" s="T185">pers</ta>
            <ta e="T187" id="Seg_6032" s="T186">adj</ta>
            <ta e="T189" id="Seg_6033" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_6034" s="T189">pers</ta>
            <ta e="T191" id="Seg_6035" s="T190">adj</ta>
            <ta e="T192" id="Seg_6036" s="T191">pers</ta>
            <ta e="T193" id="Seg_6037" s="T192">adj</ta>
            <ta e="T196" id="Seg_6038" s="T195">que</ta>
            <ta e="T197" id="Seg_6039" s="T196">ptcl</ta>
            <ta e="T199" id="Seg_6040" s="T198">v-v&gt;v-v:pn</ta>
            <ta e="T201" id="Seg_6041" s="T200">n.[n:case]</ta>
            <ta e="T203" id="Seg_6042" s="T202">v-v:ins-v:mood.pn</ta>
            <ta e="T204" id="Seg_6043" s="T203">adj</ta>
            <ta e="T207" id="Seg_6044" s="T206">adv</ta>
            <ta e="T208" id="Seg_6045" s="T207">adj</ta>
            <ta e="T209" id="Seg_6046" s="T208">n.[n:case]</ta>
            <ta e="T211" id="Seg_6047" s="T210">n.[n:case]</ta>
            <ta e="T215" id="Seg_6048" s="T214">pers</ta>
            <ta e="T216" id="Seg_6049" s="T215">n-n:case.poss</ta>
            <ta e="T217" id="Seg_6050" s="T216">num.[n:case]</ta>
            <ta e="T218" id="Seg_6051" s="T217">num.[n:case]</ta>
            <ta e="T219" id="Seg_6052" s="T218">n.[n:case]</ta>
            <ta e="T220" id="Seg_6053" s="T219">adv</ta>
            <ta e="T226" id="Seg_6054" s="T225">adv</ta>
            <ta e="T227" id="Seg_6055" s="T226">v-v:tense.[v:pn]</ta>
            <ta e="T228" id="Seg_6056" s="T227">num.[n:case]</ta>
            <ta e="T229" id="Seg_6057" s="T228">n.[n:case]</ta>
            <ta e="T230" id="Seg_6058" s="T229">num.[n:case]</ta>
            <ta e="T231" id="Seg_6059" s="T230">n.[n:case]</ta>
            <ta e="T233" id="Seg_6060" s="T232">num.[n:case]</ta>
            <ta e="T234" id="Seg_6061" s="T233">n.[n:case]</ta>
            <ta e="T235" id="Seg_6062" s="T234">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T237" id="Seg_6063" s="T236">conj</ta>
            <ta e="T238" id="Seg_6064" s="T237">n.[n:case]</ta>
            <ta e="T250" id="Seg_6065" s="T249">num.[n:case]</ta>
            <ta e="T251" id="Seg_6066" s="T250">n.[n:case]</ta>
            <ta e="T252" id="Seg_6067" s="T251">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T254" id="Seg_6068" s="T253">conj</ta>
            <ta e="T255" id="Seg_6069" s="T254">num.[n:case]</ta>
            <ta e="T256" id="Seg_6070" s="T255">ptcl</ta>
            <ta e="T258" id="Seg_6071" s="T257">adv</ta>
            <ta e="T259" id="Seg_6072" s="T258">num.[n:case]</ta>
            <ta e="T260" id="Seg_6073" s="T259">n.[n:case]</ta>
            <ta e="T261" id="Seg_6074" s="T260">num.[n:case]</ta>
            <ta e="T262" id="Seg_6075" s="T261">n.[n:case]</ta>
            <ta e="T264" id="Seg_6076" s="T263">interj</ta>
            <ta e="T265" id="Seg_6077" s="T264">n-n:case.poss</ta>
            <ta e="T266" id="Seg_6078" s="T265">adv</ta>
            <ta e="T281" id="Seg_6079" s="T280">v-v:mood.pn</ta>
            <ta e="T282" id="Seg_6080" s="T281">adj</ta>
            <ta e="T283" id="Seg_6081" s="T282">n.[n:case]</ta>
            <ta e="T284" id="Seg_6082" s="T283">v-v:n.fin</ta>
            <ta e="T286" id="Seg_6083" s="T285">ptcl</ta>
            <ta e="T287" id="Seg_6084" s="T286">pers</ta>
            <ta e="T289" id="Seg_6085" s="T288">v-v:tense-v:pn</ta>
            <ta e="T291" id="Seg_6086" s="T290">ptcl</ta>
            <ta e="T292" id="Seg_6087" s="T291">n.[n:case]</ta>
            <ta e="T294" id="Seg_6088" s="T293">n.[n:case]</ta>
            <ta e="T295" id="Seg_6089" s="T294">adj</ta>
            <ta e="T297" id="Seg_6090" s="T296">n.[n:case]</ta>
            <ta e="T298" id="Seg_6091" s="T297">n.[n:case]</ta>
            <ta e="T300" id="Seg_6092" s="T299">v-v:n.fin</ta>
            <ta e="T302" id="Seg_6093" s="T301">ptcl</ta>
            <ta e="T303" id="Seg_6094" s="T302">pers</ta>
            <ta e="T304" id="Seg_6095" s="T303">adv</ta>
            <ta e="T305" id="Seg_6096" s="T304">v-v:tense-v:pn</ta>
            <ta e="T311" id="Seg_6097" s="T310">v-v:tense-v:pn</ta>
            <ta e="T312" id="Seg_6098" s="T311">v-v&gt;v-v:mood.pn</ta>
            <ta e="T313" id="Seg_6099" s="T312">pers-n:case</ta>
            <ta e="T315" id="Seg_6100" s="T314">dempro.[n:case]</ta>
            <ta e="T316" id="Seg_6101" s="T315">n.[n:case]</ta>
            <ta e="T318" id="Seg_6102" s="T317">dempro.[n:case]</ta>
            <ta e="T319" id="Seg_6103" s="T318">n.[n:case]</ta>
            <ta e="T321" id="Seg_6104" s="T320">v</ta>
            <ta e="T322" id="Seg_6105" s="T321">v-v:n.fin</ta>
            <ta e="T323" id="Seg_6106" s="T322">ptcl</ta>
            <ta e="T330" id="Seg_6107" s="T329">v-v:mood.pn</ta>
            <ta e="T331" id="Seg_6108" s="T330">adv</ta>
            <ta e="T332" id="Seg_6109" s="T331">pers</ta>
            <ta e="T334" id="Seg_6110" s="T333">conj</ta>
            <ta e="T335" id="Seg_6111" s="T334">que.[n:case]</ta>
            <ta e="T337" id="Seg_6112" s="T336">que=ptcl</ta>
            <ta e="T338" id="Seg_6113" s="T337">v-v:tense-v:pn</ta>
            <ta e="T340" id="Seg_6114" s="T339">ptcl</ta>
            <ta e="T341" id="Seg_6115" s="T340">conj</ta>
            <ta e="T342" id="Seg_6116" s="T341">pers</ta>
            <ta e="T343" id="Seg_6117" s="T342">v-v:mood.pn</ta>
            <ta e="T344" id="Seg_6118" s="T343">pers</ta>
            <ta e="T346" id="Seg_6119" s="T345">conj</ta>
            <ta e="T347" id="Seg_6120" s="T346">pers</ta>
            <ta e="T348" id="Seg_6121" s="T347">que.[n:case]=ptcl</ta>
            <ta e="T349" id="Seg_6122" s="T348">ptcl</ta>
            <ta e="T350" id="Seg_6123" s="T349">v-v:pn</ta>
            <ta e="T352" id="Seg_6124" s="T351">que</ta>
            <ta e="T353" id="Seg_6125" s="T352">ptcl</ta>
            <ta e="T354" id="Seg_6126" s="T353">que.[n:case]=ptcl</ta>
            <ta e="T355" id="Seg_6127" s="T354">ptcl</ta>
            <ta e="T357" id="Seg_6128" s="T356">conj</ta>
            <ta e="T358" id="Seg_6129" s="T357">pers</ta>
            <ta e="T359" id="Seg_6130" s="T358">v-v:tense-v:pn</ta>
            <ta e="T361" id="Seg_6131" s="T360">pers</ta>
            <ta e="T362" id="Seg_6132" s="T361">adj</ta>
            <ta e="T363" id="Seg_6133" s="T362">n.[n:case]</ta>
            <ta e="T365" id="Seg_6134" s="T364">conj</ta>
            <ta e="T366" id="Seg_6135" s="T365">pers</ta>
            <ta e="T367" id="Seg_6136" s="T366">adj.[n:case]</ta>
            <ta e="T368" id="Seg_6137" s="T367">ptcl</ta>
            <ta e="T369" id="Seg_6138" s="T368">pers</ta>
            <ta e="T370" id="Seg_6139" s="T369">adj</ta>
            <ta e="T372" id="Seg_6140" s="T371">que</ta>
            <ta e="T373" id="Seg_6141" s="T372">ptcl</ta>
            <ta e="T374" id="Seg_6142" s="T373">v-v&gt;v-v:pn</ta>
            <ta e="T376" id="Seg_6143" s="T375">adj.[n:case]</ta>
            <ta e="T377" id="Seg_6144" s="T376">adj.[n:case]</ta>
            <ta e="T379" id="Seg_6145" s="T378">ptcl</ta>
            <ta e="T380" id="Seg_6146" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_6147" s="T380">v-v:mood.pn</ta>
            <ta e="T383" id="Seg_6148" s="T382">adj.[n:case]</ta>
            <ta e="T385" id="Seg_6149" s="T384">ptcl</ta>
            <ta e="T386" id="Seg_6150" s="T385">v-v:n.fin</ta>
            <ta e="T387" id="Seg_6151" s="T386">adj</ta>
            <ta e="T389" id="Seg_6152" s="T388">n.[n:case]</ta>
            <ta e="T391" id="Seg_6153" s="T390">conj</ta>
            <ta e="T392" id="Seg_6154" s="T391">pers</ta>
            <ta e="T393" id="Seg_6155" s="T392">adv</ta>
            <ta e="T394" id="Seg_6156" s="T393">que.[n:case]=ptcl</ta>
            <ta e="T395" id="Seg_6157" s="T394">ptcl</ta>
            <ta e="T396" id="Seg_6158" s="T395">v-v&gt;v-v:pn</ta>
            <ta e="T403" id="Seg_6159" s="T402">n.[n:case]</ta>
            <ta e="T406" id="Seg_6160" s="T405">adj</ta>
            <ta e="T407" id="Seg_6161" s="T406">n.[n:case]</ta>
            <ta e="T409" id="Seg_6162" s="T408">conj</ta>
            <ta e="T411" id="Seg_6163" s="T410">n.[n:case]</ta>
            <ta e="T413" id="Seg_6164" s="T412">%%-v:tense-v:pn</ta>
            <ta e="T415" id="Seg_6165" s="T414">n.[n:case]</ta>
            <ta e="T417" id="Seg_6166" s="T416">%%-v:tense-v:pn</ta>
            <ta e="T419" id="Seg_6167" s="T418">ptcl</ta>
            <ta e="T420" id="Seg_6168" s="T419">n.[n:case]</ta>
            <ta e="T424" id="Seg_6169" s="T423">n.[n:case]</ta>
            <ta e="T426" id="Seg_6170" s="T425">conj</ta>
            <ta e="T427" id="Seg_6171" s="T426">adj</ta>
            <ta e="T428" id="Seg_6172" s="T427">n.[n:case]</ta>
            <ta e="T430" id="Seg_6173" s="T429">ptcl</ta>
            <ta e="T431" id="Seg_6174" s="T430">adj</ta>
            <ta e="T432" id="Seg_6175" s="T431">n.[n:case]</ta>
            <ta e="T435" id="Seg_6176" s="T434">ptcl</ta>
            <ta e="T436" id="Seg_6177" s="T435">adj</ta>
            <ta e="T437" id="Seg_6178" s="T436">n.[n:case]</ta>
            <ta e="T452" id="Seg_6179" s="T451">pers</ta>
            <ta e="T453" id="Seg_6180" s="T452">n-n:case.poss</ta>
            <ta e="T454" id="Seg_6181" s="T453">v-v:tense-v:pn</ta>
            <ta e="T456" id="Seg_6182" s="T455">adv</ta>
            <ta e="T457" id="Seg_6183" s="T456">pers</ta>
            <ta e="T458" id="Seg_6184" s="T457">n-n:case.poss</ta>
            <ta e="T459" id="Seg_6185" s="T458">adj</ta>
            <ta e="T463" id="Seg_6186" s="T462">que</ta>
            <ta e="T464" id="Seg_6187" s="T463">v-v:tense-v:pn</ta>
            <ta e="T466" id="Seg_6188" s="T465">v-v:tense-v:pn</ta>
            <ta e="T468" id="Seg_6189" s="T467">que</ta>
            <ta e="T469" id="Seg_6190" s="T468">ptcl</ta>
            <ta e="T471" id="Seg_6191" s="T470">ptcl</ta>
            <ta e="T472" id="Seg_6192" s="T471">que.[n:case]</ta>
            <ta e="T474" id="Seg_6193" s="T473">v-v:mood.pn</ta>
            <ta e="T476" id="Seg_6194" s="T475">ptcl</ta>
            <ta e="T478" id="Seg_6195" s="T477">pers</ta>
            <ta e="T480" id="Seg_6196" s="T479">v-v:tense-v:pn</ta>
            <ta e="T483" id="Seg_6197" s="T482">v-v:tense</ta>
            <ta e="T485" id="Seg_6198" s="T484">pers</ta>
            <ta e="T487" id="Seg_6199" s="T486">interj</ta>
            <ta e="T489" id="Seg_6200" s="T488">pers</ta>
            <ta e="T490" id="Seg_6201" s="T489">ptcl</ta>
            <ta e="T492" id="Seg_6202" s="T491">ptcl</ta>
            <ta e="T493" id="Seg_6203" s="T492">v-v:tense-v:pn</ta>
            <ta e="T495" id="Seg_6204" s="T494">ptcl</ta>
            <ta e="T496" id="Seg_6205" s="T495">conj</ta>
            <ta e="T497" id="Seg_6206" s="T496">pers</ta>
            <ta e="T498" id="Seg_6207" s="T497">ptcl</ta>
            <ta e="T499" id="Seg_6208" s="T498">ptcl</ta>
            <ta e="T500" id="Seg_6209" s="T499">v-v:tense-v:pn</ta>
            <ta e="T504" id="Seg_6210" s="T503">pers</ta>
            <ta e="T505" id="Seg_6211" s="T504">n.[n:case]</ta>
            <ta e="T506" id="Seg_6212" s="T505">adj</ta>
            <ta e="T507" id="Seg_6213" s="T506">n.[n:case]</ta>
            <ta e="T509" id="Seg_6214" s="T508">conj</ta>
            <ta e="T510" id="Seg_6215" s="T509">n.[n:case]</ta>
            <ta e="T511" id="Seg_6216" s="T510">pers</ta>
            <ta e="T512" id="Seg_6217" s="T511">v-v:tense-v:pn</ta>
            <ta e="T514" id="Seg_6218" s="T513">n.[n:case]</ta>
            <ta e="T515" id="Seg_6219" s="T514">pers</ta>
            <ta e="T516" id="Seg_6220" s="T515">v-v:tense-v:pn</ta>
            <ta e="T518" id="Seg_6221" s="T517">adj</ta>
            <ta e="T520" id="Seg_6222" s="T519">que</ta>
            <ta e="T521" id="Seg_6223" s="T520">dempro.[n:case]</ta>
            <ta e="T522" id="Seg_6224" s="T521">n.[n:case]</ta>
            <ta e="T524" id="Seg_6225" s="T523">conj</ta>
            <ta e="T525" id="Seg_6226" s="T524">que</ta>
            <ta e="T526" id="Seg_6227" s="T525">pers</ta>
            <ta e="T527" id="Seg_6228" s="T526">v-v:tense-v:pn</ta>
            <ta e="T528" id="Seg_6229" s="T527">dempro.[n:case]</ta>
            <ta e="T529" id="Seg_6230" s="T528">n.[n:case]</ta>
            <ta e="T556" id="Seg_6231" s="T555">pers</ta>
            <ta e="T558" id="Seg_6232" s="T557">v-v:tense-v:pn</ta>
            <ta e="T560" id="Seg_6233" s="T559">que</ta>
            <ta e="T562" id="Seg_6234" s="T561">interj</ta>
            <ta e="T563" id="Seg_6235" s="T562">que</ta>
            <ta e="T564" id="Seg_6236" s="T563">que</ta>
            <ta e="T566" id="Seg_6237" s="T565">v-v:tense-v:pn</ta>
            <ta e="T568" id="Seg_6238" s="T567">que.[n:case]</ta>
            <ta e="T570" id="Seg_6239" s="T569">conj</ta>
            <ta e="T571" id="Seg_6240" s="T570">pers</ta>
            <ta e="T572" id="Seg_6241" s="T571">n.[n:case]</ta>
            <ta e="T573" id="Seg_6242" s="T572">v.[v:pn]</ta>
            <ta e="T576" id="Seg_6243" s="T575">n.[n:case]</ta>
            <ta e="T577" id="Seg_6244" s="T576">v-v:tense-v:pn</ta>
            <ta e="T579" id="Seg_6245" s="T578">conj</ta>
            <ta e="T580" id="Seg_6246" s="T579">v-v:mood.pn</ta>
            <ta e="T582" id="Seg_6247" s="T581">v-v:mood.pn</ta>
            <ta e="T583" id="Seg_6248" s="T582">que</ta>
            <ta e="T584" id="Seg_6249" s="T583">v-v:tense-v:pn</ta>
            <ta e="T586" id="Seg_6250" s="T585">v-v:tense-v:pn</ta>
            <ta e="T587" id="Seg_6251" s="T586">que=ptcl</ta>
            <ta e="T589" id="Seg_6252" s="T588">v-v:tense-v:pn</ta>
            <ta e="T590" id="Seg_6253" s="T589">conj</ta>
            <ta e="T591" id="Seg_6254" s="T590">v-v:tense-v:pn</ta>
            <ta e="T593" id="Seg_6255" s="T592">quant</ta>
            <ta e="T594" id="Seg_6256" s="T593">que.[n:case]</ta>
            <ta e="T595" id="Seg_6257" s="T594">ptcl</ta>
            <ta e="T597" id="Seg_6258" s="T596">v-v:tense-v:pn</ta>
            <ta e="T599" id="Seg_6259" s="T598">conj</ta>
            <ta e="T600" id="Seg_6260" s="T599">que</ta>
            <ta e="T601" id="Seg_6261" s="T600">ptcl</ta>
            <ta e="T602" id="Seg_6262" s="T601">v-v:tense-v:pn</ta>
            <ta e="T604" id="Seg_6263" s="T603">ptcl</ta>
            <ta e="T605" id="Seg_6264" s="T604">pers</ta>
            <ta e="T606" id="Seg_6265" s="T605">adj.[n:case]</ta>
            <ta e="T607" id="Seg_6266" s="T606">v-v:n.fin</ta>
            <ta e="T609" id="Seg_6267" s="T608">ptcl</ta>
            <ta e="T610" id="Seg_6268" s="T609">v-v:n.fin</ta>
            <ta e="T611" id="Seg_6269" s="T610">refl-n:case.poss</ta>
            <ta e="T612" id="Seg_6270" s="T611">n-n:case</ta>
            <ta e="T614" id="Seg_6271" s="T613">conj</ta>
            <ta e="T615" id="Seg_6272" s="T614">que.[n:case]</ta>
            <ta e="T616" id="Seg_6273" s="T615">n.[n:case]</ta>
            <ta e="T617" id="Seg_6274" s="T616">v-v:tense-v:pn</ta>
            <ta e="T618" id="Seg_6275" s="T617">pers</ta>
            <ta e="T620" id="Seg_6276" s="T619">que=ptcl</ta>
            <ta e="T621" id="Seg_6277" s="T620">v-v:tense-v:pn</ta>
            <ta e="T623" id="Seg_6278" s="T622">que</ta>
            <ta e="T625" id="Seg_6279" s="T624">conj</ta>
            <ta e="T626" id="Seg_6280" s="T625">v-v:mood.pn</ta>
            <ta e="T627" id="Seg_6281" s="T626">que</ta>
            <ta e="T629" id="Seg_6282" s="T628">ptcl</ta>
            <ta e="T630" id="Seg_6283" s="T629">refl</ta>
            <ta e="T631" id="Seg_6284" s="T630">n-n:case</ta>
            <ta e="T633" id="Seg_6285" s="T632">v-v:tense-v:pn</ta>
            <ta e="T634" id="Seg_6286" s="T633">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T640" id="Seg_6287" s="T639">v-v:mood.pn</ta>
            <ta e="T641" id="Seg_6288" s="T640">pers</ta>
            <ta e="T642" id="Seg_6289" s="T641">n.[n:case]</ta>
            <ta e="T643" id="Seg_6290" s="T642">v-v:n.fin</ta>
            <ta e="T645" id="Seg_6291" s="T644">pers</ta>
            <ta e="T646" id="Seg_6292" s="T645">v.[v:pn]</ta>
            <ta e="T647" id="Seg_6293" s="T646">n.[n:case]</ta>
            <ta e="T650" id="Seg_6294" s="T649">conj</ta>
            <ta e="T651" id="Seg_6295" s="T650">pers</ta>
            <ta e="T652" id="Seg_6296" s="T651">v.[v:pn]</ta>
            <ta e="T654" id="Seg_6297" s="T653">ptcl</ta>
            <ta e="T655" id="Seg_6298" s="T654">v-v:tense.[v:pn]</ta>
            <ta e="T656" id="Seg_6299" s="T655">v-v:tense.[v:pn]</ta>
            <ta e="T658" id="Seg_6300" s="T657">v-v:mood.pn</ta>
            <ta e="T659" id="Seg_6301" s="T658">pers</ta>
            <ta e="T661" id="Seg_6302" s="T660">pers</ta>
            <ta e="T662" id="Seg_6303" s="T661">v.[v:pn]</ta>
            <ta e="T663" id="Seg_6304" s="T662">n.[n:case]</ta>
            <ta e="T665" id="Seg_6305" s="T664">conj</ta>
            <ta e="T666" id="Seg_6306" s="T665">pers</ta>
            <ta e="T667" id="Seg_6307" s="T666">v-v:n.fin</ta>
            <ta e="T668" id="Seg_6308" s="T667">v-v:tense-v:pn</ta>
            <ta e="T672" id="Seg_6309" s="T671">v-v:mood.pn</ta>
            <ta e="T673" id="Seg_6310" s="T672">pers</ta>
            <ta e="T674" id="Seg_6311" s="T673">n.[n:case]</ta>
            <ta e="T675" id="Seg_6312" s="T674">v-v:n.fin</ta>
            <ta e="T677" id="Seg_6313" s="T676">conj</ta>
            <ta e="T678" id="Seg_6314" s="T677">n.[n:case]</ta>
            <ta e="T680" id="Seg_6315" s="T679">v-v:n.fin</ta>
            <ta e="T681" id="Seg_6316" s="T680">ptcl</ta>
            <ta e="T683" id="Seg_6317" s="T682">ptcl</ta>
            <ta e="T684" id="Seg_6318" s="T683">ptcl</ta>
            <ta e="T686" id="Seg_6319" s="T685">n.[n:case]</ta>
            <ta e="T687" id="Seg_6320" s="T686">v-v:n.fin</ta>
            <ta e="T688" id="Seg_6321" s="T687">ptcl</ta>
            <ta e="T689" id="Seg_6322" s="T688">ptcl</ta>
            <ta e="T690" id="Seg_6323" s="T689">v-v:mood.pn</ta>
            <ta e="T692" id="Seg_6324" s="T691">pers</ta>
            <ta e="T693" id="Seg_6325" s="T692">ptcl</ta>
            <ta e="T694" id="Seg_6326" s="T693">adj.[n:case]</ta>
            <ta e="T695" id="Seg_6327" s="T694">n.[n:case]</ta>
            <ta e="T697" id="Seg_6328" s="T696">conj</ta>
            <ta e="T698" id="Seg_6329" s="T697">pers</ta>
            <ta e="T699" id="Seg_6330" s="T698">adj</ta>
            <ta e="T700" id="Seg_6331" s="T699">n.[n:case]</ta>
            <ta e="T702" id="Seg_6332" s="T701">pers</ta>
            <ta e="T703" id="Seg_6333" s="T702">n.[n:case]</ta>
            <ta e="T706" id="Seg_6334" s="T705">v-v:tense-v:pn</ta>
            <ta e="T707" id="Seg_6335" s="T706">ptcl</ta>
            <ta e="T708" id="Seg_6336" s="T707">v-v:tense-v:pn</ta>
            <ta e="T710" id="Seg_6337" s="T709">conj</ta>
            <ta e="T711" id="Seg_6338" s="T710">pers</ta>
            <ta e="T712" id="Seg_6339" s="T711">adv</ta>
            <ta e="T713" id="Seg_6340" s="T712">v-v:tense-v:pn</ta>
            <ta e="T715" id="Seg_6341" s="T714">ptcl</ta>
            <ta e="T716" id="Seg_6342" s="T715">ptcl</ta>
            <ta e="T717" id="Seg_6343" s="T716">adj</ta>
            <ta e="T718" id="Seg_6344" s="T717">ptcl</ta>
            <ta e="T719" id="Seg_6345" s="T718">v-v:n.fin</ta>
            <ta e="T721" id="Seg_6346" s="T720">v-v:n.fin</ta>
            <ta e="T722" id="Seg_6347" s="T721">ptcl</ta>
            <ta e="T723" id="Seg_6348" s="T722">ptcl</ta>
            <ta e="T724" id="Seg_6349" s="T723">ptcl</ta>
            <ta e="T725" id="Seg_6350" s="T724">v-v:n.fin</ta>
            <ta e="T727" id="Seg_6351" s="T726">ptcl</ta>
            <ta e="T728" id="Seg_6352" s="T727">adj</ta>
            <ta e="T730" id="Seg_6353" s="T729">pers</ta>
            <ta e="T733" id="Seg_6354" s="T732">adj.[n:case]</ta>
            <ta e="T734" id="Seg_6355" s="T733">n.[n:case]</ta>
         </annotation>
         <annotation name="ps" tierref="ps-SAE">
            <ta e="T10" id="Seg_6356" s="T9">n</ta>
            <ta e="T11" id="Seg_6357" s="T10">que</ta>
            <ta e="T12" id="Seg_6358" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_6359" s="T12">v</ta>
            <ta e="T15" id="Seg_6360" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_6361" s="T15">adj</ta>
            <ta e="T18" id="Seg_6362" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_6363" s="T18">adj</ta>
            <ta e="T20" id="Seg_6364" s="T19">v</ta>
            <ta e="T22" id="Seg_6365" s="T21">que</ta>
            <ta e="T23" id="Seg_6366" s="T22">ptcl</ta>
            <ta e="T25" id="Seg_6367" s="T24">pers</ta>
            <ta e="T26" id="Seg_6368" s="T25">v</ta>
            <ta e="T28" id="Seg_6369" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_6370" s="T28">pers</ta>
            <ta e="T30" id="Seg_6371" s="T29">adj</ta>
            <ta e="T31" id="Seg_6372" s="T30">v</ta>
            <ta e="T32" id="Seg_6373" s="T31">pers</ta>
            <ta e="T35" id="Seg_6374" s="T34">v</ta>
            <ta e="T36" id="Seg_6375" s="T35">adv</ta>
            <ta e="T38" id="Seg_6376" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_6377" s="T38">pers</ta>
            <ta e="T40" id="Seg_6378" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_6379" s="T40">v</ta>
            <ta e="T43" id="Seg_6380" s="T42">que</ta>
            <ta e="T44" id="Seg_6381" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_6382" s="T44">que</ta>
            <ta e="T47" id="Seg_6383" s="T46">pers</ta>
            <ta e="T48" id="Seg_6384" s="T47">n</ta>
            <ta e="T49" id="Seg_6385" s="T48">v</ta>
            <ta e="T63" id="Seg_6386" s="T62">n</ta>
            <ta e="T65" id="Seg_6387" s="T64">que</ta>
            <ta e="T66" id="Seg_6388" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_6389" s="T66">pers</ta>
            <ta e="T69" id="Seg_6390" s="T68">v</ta>
            <ta e="T70" id="Seg_6391" s="T69">adv</ta>
            <ta e="T72" id="Seg_6392" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_6393" s="T72">pers</ta>
            <ta e="T74" id="Seg_6394" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_6395" s="T74">v</ta>
            <ta e="T77" id="Seg_6396" s="T76">pers</ta>
            <ta e="T78" id="Seg_6397" s="T77">n</ta>
            <ta e="T79" id="Seg_6398" s="T78">adj</ta>
            <ta e="T85" id="Seg_6399" s="T84">v</ta>
            <ta e="T86" id="Seg_6400" s="T85">pers</ta>
            <ta e="T87" id="Seg_6401" s="T86">n</ta>
            <ta e="T88" id="Seg_6402" s="T87">v</ta>
            <ta e="T90" id="Seg_6403" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_6404" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_6405" s="T91">v</ta>
            <ta e="T94" id="Seg_6406" s="T93">que</ta>
            <ta e="T96" id="Seg_6407" s="T95">v</ta>
            <ta e="T97" id="Seg_6408" s="T96">n</ta>
            <ta e="T98" id="Seg_6409" s="T97">adj</ta>
            <ta e="T100" id="Seg_6410" s="T99">adj</ta>
            <ta e="T101" id="Seg_6411" s="T100">v</ta>
            <ta e="T102" id="Seg_6412" s="T101">v</ta>
            <ta e="T104" id="Seg_6413" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_6414" s="T104">pers</ta>
            <ta e="T107" id="Seg_6415" s="T106">adv</ta>
            <ta e="T108" id="Seg_6416" s="T107">v</ta>
            <ta e="T110" id="Seg_6417" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_6418" s="T110">pers</ta>
            <ta e="T112" id="Seg_6419" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_6420" s="T112">v</ta>
            <ta e="T114" id="Seg_6421" s="T113">pers</ta>
            <ta e="T115" id="Seg_6422" s="T114">v</ta>
            <ta e="T117" id="Seg_6423" s="T116">ptcl</ta>
            <ta e="T118" id="Seg_6424" s="T117">v</ta>
            <ta e="T120" id="Seg_6425" s="T119">v</ta>
            <ta e="T121" id="Seg_6426" s="T120">ptcl</ta>
            <ta e="T122" id="Seg_6427" s="T121">v</ta>
            <ta e="T124" id="Seg_6428" s="T123">ptcl</ta>
            <ta e="T125" id="Seg_6429" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_6430" s="T125">v</ta>
            <ta e="T128" id="Seg_6431" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_6432" s="T128">adj</ta>
            <ta e="T131" id="Seg_6433" s="T130">adj</ta>
            <ta e="T132" id="Seg_6434" s="T131">ptcl</ta>
            <ta e="T133" id="Seg_6435" s="T132">v</ta>
            <ta e="T134" id="Seg_6436" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_6437" s="T134">pers</ta>
            <ta e="T136" id="Seg_6438" s="T135">v</ta>
            <ta e="T144" id="Seg_6439" s="T143">pers</ta>
            <ta e="T145" id="Seg_6440" s="T144">n</ta>
            <ta e="T146" id="Seg_6441" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_6442" s="T146">v</ta>
            <ta e="T149" id="Seg_6443" s="T148">pers</ta>
            <ta e="T151" id="Seg_6444" s="T150">v</ta>
            <ta e="T153" id="Seg_6445" s="T152">que</ta>
            <ta e="T155" id="Seg_6446" s="T154">conj</ta>
            <ta e="T156" id="Seg_6447" s="T155">dempro</ta>
            <ta e="T157" id="Seg_6448" s="T156">v</ta>
            <ta e="T158" id="Seg_6449" s="T157">adv</ta>
            <ta e="T160" id="Seg_6450" s="T159">ptcl</ta>
            <ta e="T161" id="Seg_6451" s="T160">v</ta>
            <ta e="T162" id="Seg_6452" s="T161">n</ta>
            <ta e="T164" id="Seg_6453" s="T163">v</ta>
            <ta e="T166" id="Seg_6454" s="T165">que</ta>
            <ta e="T167" id="Seg_6455" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_6456" s="T167">v</ta>
            <ta e="T170" id="Seg_6457" s="T169">pers</ta>
            <ta e="T171" id="Seg_6458" s="T170">adj</ta>
            <ta e="T172" id="Seg_6459" s="T171">n</ta>
            <ta e="T178" id="Seg_6460" s="T177">pers</ta>
            <ta e="T179" id="Seg_6461" s="T178">n</ta>
            <ta e="T180" id="Seg_6462" s="T179">n</ta>
            <ta e="T181" id="Seg_6463" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_6464" s="T181">adj</ta>
            <ta e="T183" id="Seg_6465" s="T182">n</ta>
            <ta e="T185" id="Seg_6466" s="T184">conj</ta>
            <ta e="T186" id="Seg_6467" s="T185">pers</ta>
            <ta e="T187" id="Seg_6468" s="T186">adj</ta>
            <ta e="T189" id="Seg_6469" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_6470" s="T189">pers</ta>
            <ta e="T191" id="Seg_6471" s="T190">adj</ta>
            <ta e="T192" id="Seg_6472" s="T191">pers</ta>
            <ta e="T193" id="Seg_6473" s="T192">adj</ta>
            <ta e="T196" id="Seg_6474" s="T195">que</ta>
            <ta e="T197" id="Seg_6475" s="T196">ptcl</ta>
            <ta e="T199" id="Seg_6476" s="T198">v</ta>
            <ta e="T201" id="Seg_6477" s="T200">n</ta>
            <ta e="T203" id="Seg_6478" s="T202">v</ta>
            <ta e="T204" id="Seg_6479" s="T203">adj</ta>
            <ta e="T207" id="Seg_6480" s="T206">adv</ta>
            <ta e="T208" id="Seg_6481" s="T207">adj</ta>
            <ta e="T209" id="Seg_6482" s="T208">n</ta>
            <ta e="T211" id="Seg_6483" s="T210">n</ta>
            <ta e="T215" id="Seg_6484" s="T214">pers</ta>
            <ta e="T216" id="Seg_6485" s="T215">n</ta>
            <ta e="T217" id="Seg_6486" s="T216">num</ta>
            <ta e="T218" id="Seg_6487" s="T217">num</ta>
            <ta e="T219" id="Seg_6488" s="T218">n</ta>
            <ta e="T220" id="Seg_6489" s="T219">adv</ta>
            <ta e="T226" id="Seg_6490" s="T225">adv</ta>
            <ta e="T227" id="Seg_6491" s="T226">v</ta>
            <ta e="T228" id="Seg_6492" s="T227">num</ta>
            <ta e="T229" id="Seg_6493" s="T228">n</ta>
            <ta e="T230" id="Seg_6494" s="T229">num</ta>
            <ta e="T231" id="Seg_6495" s="T230">n</ta>
            <ta e="T233" id="Seg_6496" s="T232">num</ta>
            <ta e="T234" id="Seg_6497" s="T233">n</ta>
            <ta e="T235" id="Seg_6498" s="T234">v</ta>
            <ta e="T237" id="Seg_6499" s="T236">conj</ta>
            <ta e="T238" id="Seg_6500" s="T237">n</ta>
            <ta e="T250" id="Seg_6501" s="T249">num</ta>
            <ta e="T251" id="Seg_6502" s="T250">n</ta>
            <ta e="T252" id="Seg_6503" s="T251">v</ta>
            <ta e="T254" id="Seg_6504" s="T253">conj</ta>
            <ta e="T255" id="Seg_6505" s="T254">num</ta>
            <ta e="T256" id="Seg_6506" s="T255">ptcl</ta>
            <ta e="T258" id="Seg_6507" s="T257">adv</ta>
            <ta e="T259" id="Seg_6508" s="T258">num</ta>
            <ta e="T260" id="Seg_6509" s="T259">n</ta>
            <ta e="T261" id="Seg_6510" s="T260">num</ta>
            <ta e="T262" id="Seg_6511" s="T261">n</ta>
            <ta e="T264" id="Seg_6512" s="T263">interj</ta>
            <ta e="T265" id="Seg_6513" s="T264">n</ta>
            <ta e="T266" id="Seg_6514" s="T265">adv</ta>
            <ta e="T281" id="Seg_6515" s="T280">v</ta>
            <ta e="T282" id="Seg_6516" s="T281">adj</ta>
            <ta e="T283" id="Seg_6517" s="T282">n</ta>
            <ta e="T284" id="Seg_6518" s="T283">v</ta>
            <ta e="T286" id="Seg_6519" s="T285">ptcl</ta>
            <ta e="T287" id="Seg_6520" s="T286">pers</ta>
            <ta e="T289" id="Seg_6521" s="T288">v</ta>
            <ta e="T291" id="Seg_6522" s="T290">ptcl</ta>
            <ta e="T292" id="Seg_6523" s="T291">n</ta>
            <ta e="T294" id="Seg_6524" s="T293">n</ta>
            <ta e="T295" id="Seg_6525" s="T294">adj</ta>
            <ta e="T297" id="Seg_6526" s="T296">n</ta>
            <ta e="T298" id="Seg_6527" s="T297">n</ta>
            <ta e="T300" id="Seg_6528" s="T299">v</ta>
            <ta e="T302" id="Seg_6529" s="T301">ptcl</ta>
            <ta e="T303" id="Seg_6530" s="T302">pers</ta>
            <ta e="T304" id="Seg_6531" s="T303">adv</ta>
            <ta e="T305" id="Seg_6532" s="T304">v</ta>
            <ta e="T311" id="Seg_6533" s="T310">v</ta>
            <ta e="T312" id="Seg_6534" s="T311">v</ta>
            <ta e="T313" id="Seg_6535" s="T312">pers</ta>
            <ta e="T315" id="Seg_6536" s="T314">dempro</ta>
            <ta e="T316" id="Seg_6537" s="T315">n</ta>
            <ta e="T318" id="Seg_6538" s="T317">dempro</ta>
            <ta e="T319" id="Seg_6539" s="T318">n</ta>
            <ta e="T321" id="Seg_6540" s="T320">v</ta>
            <ta e="T322" id="Seg_6541" s="T321">v</ta>
            <ta e="T323" id="Seg_6542" s="T322">ptcl</ta>
            <ta e="T330" id="Seg_6543" s="T329">v</ta>
            <ta e="T331" id="Seg_6544" s="T330">adv</ta>
            <ta e="T332" id="Seg_6545" s="T331">pers</ta>
            <ta e="T334" id="Seg_6546" s="T333">conj</ta>
            <ta e="T335" id="Seg_6547" s="T334">que</ta>
            <ta e="T337" id="Seg_6548" s="T336">que</ta>
            <ta e="T338" id="Seg_6549" s="T337">v</ta>
            <ta e="T340" id="Seg_6550" s="T339">ptcl</ta>
            <ta e="T341" id="Seg_6551" s="T340">conj</ta>
            <ta e="T342" id="Seg_6552" s="T341">pers</ta>
            <ta e="T343" id="Seg_6553" s="T342">v</ta>
            <ta e="T344" id="Seg_6554" s="T343">pers</ta>
            <ta e="T346" id="Seg_6555" s="T345">conj</ta>
            <ta e="T347" id="Seg_6556" s="T346">pers</ta>
            <ta e="T348" id="Seg_6557" s="T347">que</ta>
            <ta e="T349" id="Seg_6558" s="T348">ptcl</ta>
            <ta e="T350" id="Seg_6559" s="T349">v</ta>
            <ta e="T352" id="Seg_6560" s="T351">que</ta>
            <ta e="T353" id="Seg_6561" s="T352">ptcl</ta>
            <ta e="T354" id="Seg_6562" s="T353">que</ta>
            <ta e="T355" id="Seg_6563" s="T354">ptcl</ta>
            <ta e="T357" id="Seg_6564" s="T356">conj</ta>
            <ta e="T358" id="Seg_6565" s="T357">pers</ta>
            <ta e="T359" id="Seg_6566" s="T358">v</ta>
            <ta e="T361" id="Seg_6567" s="T360">pers</ta>
            <ta e="T362" id="Seg_6568" s="T361">adj</ta>
            <ta e="T363" id="Seg_6569" s="T362">n</ta>
            <ta e="T365" id="Seg_6570" s="T364">conj</ta>
            <ta e="T366" id="Seg_6571" s="T365">pers</ta>
            <ta e="T367" id="Seg_6572" s="T366">adj</ta>
            <ta e="T368" id="Seg_6573" s="T367">ptcl</ta>
            <ta e="T369" id="Seg_6574" s="T368">pers</ta>
            <ta e="T370" id="Seg_6575" s="T369">adj</ta>
            <ta e="T372" id="Seg_6576" s="T371">que</ta>
            <ta e="T373" id="Seg_6577" s="T372">ptcl</ta>
            <ta e="T374" id="Seg_6578" s="T373">v</ta>
            <ta e="T376" id="Seg_6579" s="T375">adj</ta>
            <ta e="T377" id="Seg_6580" s="T376">adj</ta>
            <ta e="T379" id="Seg_6581" s="T378">ptcl</ta>
            <ta e="T380" id="Seg_6582" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_6583" s="T380">v</ta>
            <ta e="T383" id="Seg_6584" s="T382">adj</ta>
            <ta e="T385" id="Seg_6585" s="T384">ptcl</ta>
            <ta e="T386" id="Seg_6586" s="T385">v</ta>
            <ta e="T387" id="Seg_6587" s="T386">adj</ta>
            <ta e="T389" id="Seg_6588" s="T388">n</ta>
            <ta e="T391" id="Seg_6589" s="T390">conj</ta>
            <ta e="T392" id="Seg_6590" s="T391">pers</ta>
            <ta e="T393" id="Seg_6591" s="T392">adv</ta>
            <ta e="T394" id="Seg_6592" s="T393">que</ta>
            <ta e="T395" id="Seg_6593" s="T394">ptcl</ta>
            <ta e="T396" id="Seg_6594" s="T395">v</ta>
            <ta e="T403" id="Seg_6595" s="T402">n</ta>
            <ta e="T406" id="Seg_6596" s="T405">adj</ta>
            <ta e="T407" id="Seg_6597" s="T406">n</ta>
            <ta e="T409" id="Seg_6598" s="T408">conj</ta>
            <ta e="T411" id="Seg_6599" s="T410">n</ta>
            <ta e="T415" id="Seg_6600" s="T414">n</ta>
            <ta e="T417" id="Seg_6601" s="T416">v</ta>
            <ta e="T419" id="Seg_6602" s="T418">ptcl</ta>
            <ta e="T420" id="Seg_6603" s="T419">n</ta>
            <ta e="T424" id="Seg_6604" s="T423">n</ta>
            <ta e="T426" id="Seg_6605" s="T425">conj</ta>
            <ta e="T427" id="Seg_6606" s="T426">adj</ta>
            <ta e="T428" id="Seg_6607" s="T427">n</ta>
            <ta e="T430" id="Seg_6608" s="T429">ptcl</ta>
            <ta e="T431" id="Seg_6609" s="T430">adj</ta>
            <ta e="T432" id="Seg_6610" s="T431">n</ta>
            <ta e="T435" id="Seg_6611" s="T434">ptcl</ta>
            <ta e="T436" id="Seg_6612" s="T435">adj</ta>
            <ta e="T437" id="Seg_6613" s="T436">n</ta>
            <ta e="T452" id="Seg_6614" s="T451">pers</ta>
            <ta e="T453" id="Seg_6615" s="T452">n</ta>
            <ta e="T454" id="Seg_6616" s="T453">v</ta>
            <ta e="T456" id="Seg_6617" s="T455">adv</ta>
            <ta e="T457" id="Seg_6618" s="T456">pers</ta>
            <ta e="T458" id="Seg_6619" s="T457">n</ta>
            <ta e="T459" id="Seg_6620" s="T458">adj</ta>
            <ta e="T463" id="Seg_6621" s="T462">que</ta>
            <ta e="T464" id="Seg_6622" s="T463">v</ta>
            <ta e="T466" id="Seg_6623" s="T465">v</ta>
            <ta e="T468" id="Seg_6624" s="T467">que</ta>
            <ta e="T469" id="Seg_6625" s="T468">ptcl</ta>
            <ta e="T471" id="Seg_6626" s="T470">ptcl</ta>
            <ta e="T472" id="Seg_6627" s="T471">que</ta>
            <ta e="T474" id="Seg_6628" s="T473">v</ta>
            <ta e="T476" id="Seg_6629" s="T475">ptcl</ta>
            <ta e="T478" id="Seg_6630" s="T477">pers</ta>
            <ta e="T480" id="Seg_6631" s="T479">v</ta>
            <ta e="T483" id="Seg_6632" s="T482">v</ta>
            <ta e="T485" id="Seg_6633" s="T484">pers</ta>
            <ta e="T487" id="Seg_6634" s="T486">interj</ta>
            <ta e="T489" id="Seg_6635" s="T488">pers</ta>
            <ta e="T490" id="Seg_6636" s="T489">ptcl</ta>
            <ta e="T492" id="Seg_6637" s="T491">ptcl</ta>
            <ta e="T493" id="Seg_6638" s="T492">v</ta>
            <ta e="T495" id="Seg_6639" s="T494">ptcl</ta>
            <ta e="T496" id="Seg_6640" s="T495">conj</ta>
            <ta e="T497" id="Seg_6641" s="T496">pers</ta>
            <ta e="T498" id="Seg_6642" s="T497">ptcl</ta>
            <ta e="T499" id="Seg_6643" s="T498">ptcl</ta>
            <ta e="T500" id="Seg_6644" s="T499">v</ta>
            <ta e="T504" id="Seg_6645" s="T503">pers</ta>
            <ta e="T505" id="Seg_6646" s="T504">n</ta>
            <ta e="T506" id="Seg_6647" s="T505">adj</ta>
            <ta e="T507" id="Seg_6648" s="T506">n</ta>
            <ta e="T509" id="Seg_6649" s="T508">conj</ta>
            <ta e="T510" id="Seg_6650" s="T509">n</ta>
            <ta e="T511" id="Seg_6651" s="T510">pers</ta>
            <ta e="T512" id="Seg_6652" s="T511">v</ta>
            <ta e="T514" id="Seg_6653" s="T513">n</ta>
            <ta e="T515" id="Seg_6654" s="T514">pers</ta>
            <ta e="T516" id="Seg_6655" s="T515">v</ta>
            <ta e="T518" id="Seg_6656" s="T517">adj</ta>
            <ta e="T520" id="Seg_6657" s="T519">que</ta>
            <ta e="T521" id="Seg_6658" s="T520">dempro</ta>
            <ta e="T522" id="Seg_6659" s="T521">n</ta>
            <ta e="T524" id="Seg_6660" s="T523">conj</ta>
            <ta e="T525" id="Seg_6661" s="T524">que</ta>
            <ta e="T526" id="Seg_6662" s="T525">pers</ta>
            <ta e="T527" id="Seg_6663" s="T526">v</ta>
            <ta e="T528" id="Seg_6664" s="T527">dempro</ta>
            <ta e="T529" id="Seg_6665" s="T528">n</ta>
            <ta e="T556" id="Seg_6666" s="T555">pers</ta>
            <ta e="T558" id="Seg_6667" s="T557">v</ta>
            <ta e="T560" id="Seg_6668" s="T559">que</ta>
            <ta e="T562" id="Seg_6669" s="T561">interj</ta>
            <ta e="T563" id="Seg_6670" s="T562">que</ta>
            <ta e="T564" id="Seg_6671" s="T563">que</ta>
            <ta e="T566" id="Seg_6672" s="T565">v</ta>
            <ta e="T568" id="Seg_6673" s="T567">que</ta>
            <ta e="T570" id="Seg_6674" s="T569">conj</ta>
            <ta e="T571" id="Seg_6675" s="T570">pers</ta>
            <ta e="T572" id="Seg_6676" s="T571">n</ta>
            <ta e="T573" id="Seg_6677" s="T572">v</ta>
            <ta e="T576" id="Seg_6678" s="T575">n</ta>
            <ta e="T577" id="Seg_6679" s="T576">v</ta>
            <ta e="T579" id="Seg_6680" s="T578">conj</ta>
            <ta e="T580" id="Seg_6681" s="T579">v</ta>
            <ta e="T582" id="Seg_6682" s="T581">v</ta>
            <ta e="T583" id="Seg_6683" s="T582">que</ta>
            <ta e="T584" id="Seg_6684" s="T583">v</ta>
            <ta e="T586" id="Seg_6685" s="T585">v</ta>
            <ta e="T587" id="Seg_6686" s="T586">que</ta>
            <ta e="T589" id="Seg_6687" s="T588">v</ta>
            <ta e="T590" id="Seg_6688" s="T589">conj</ta>
            <ta e="T591" id="Seg_6689" s="T590">v</ta>
            <ta e="T593" id="Seg_6690" s="T592">quant</ta>
            <ta e="T594" id="Seg_6691" s="T593">que</ta>
            <ta e="T595" id="Seg_6692" s="T594">ptcl</ta>
            <ta e="T597" id="Seg_6693" s="T596">v</ta>
            <ta e="T599" id="Seg_6694" s="T598">conj</ta>
            <ta e="T600" id="Seg_6695" s="T599">que</ta>
            <ta e="T601" id="Seg_6696" s="T600">ptcl</ta>
            <ta e="T602" id="Seg_6697" s="T601">v</ta>
            <ta e="T604" id="Seg_6698" s="T603">ptcl</ta>
            <ta e="T605" id="Seg_6699" s="T604">pers</ta>
            <ta e="T606" id="Seg_6700" s="T605">adj</ta>
            <ta e="T607" id="Seg_6701" s="T606">v</ta>
            <ta e="T609" id="Seg_6702" s="T608">ptcl</ta>
            <ta e="T610" id="Seg_6703" s="T609">v</ta>
            <ta e="T611" id="Seg_6704" s="T610">refl</ta>
            <ta e="T612" id="Seg_6705" s="T611">n</ta>
            <ta e="T614" id="Seg_6706" s="T613">conj</ta>
            <ta e="T615" id="Seg_6707" s="T614">que</ta>
            <ta e="T616" id="Seg_6708" s="T615">n</ta>
            <ta e="T617" id="Seg_6709" s="T616">v</ta>
            <ta e="T618" id="Seg_6710" s="T617">pers</ta>
            <ta e="T620" id="Seg_6711" s="T619">que</ta>
            <ta e="T621" id="Seg_6712" s="T620">v</ta>
            <ta e="T623" id="Seg_6713" s="T622">que</ta>
            <ta e="T625" id="Seg_6714" s="T624">conj</ta>
            <ta e="T626" id="Seg_6715" s="T625">v</ta>
            <ta e="T627" id="Seg_6716" s="T626">que</ta>
            <ta e="T629" id="Seg_6717" s="T628">ptcl</ta>
            <ta e="T630" id="Seg_6718" s="T629">refl</ta>
            <ta e="T631" id="Seg_6719" s="T630">n</ta>
            <ta e="T633" id="Seg_6720" s="T632">v</ta>
            <ta e="T634" id="Seg_6721" s="T633">v</ta>
            <ta e="T640" id="Seg_6722" s="T639">v</ta>
            <ta e="T641" id="Seg_6723" s="T640">pers</ta>
            <ta e="T642" id="Seg_6724" s="T641">n</ta>
            <ta e="T643" id="Seg_6725" s="T642">v</ta>
            <ta e="T645" id="Seg_6726" s="T644">pers</ta>
            <ta e="T646" id="Seg_6727" s="T645">v</ta>
            <ta e="T647" id="Seg_6728" s="T646">n</ta>
            <ta e="T650" id="Seg_6729" s="T649">conj</ta>
            <ta e="T651" id="Seg_6730" s="T650">pers</ta>
            <ta e="T652" id="Seg_6731" s="T651">v</ta>
            <ta e="T654" id="Seg_6732" s="T653">ptcl</ta>
            <ta e="T655" id="Seg_6733" s="T654">v</ta>
            <ta e="T656" id="Seg_6734" s="T655">v</ta>
            <ta e="T658" id="Seg_6735" s="T657">v</ta>
            <ta e="T659" id="Seg_6736" s="T658">pers</ta>
            <ta e="T661" id="Seg_6737" s="T660">pers</ta>
            <ta e="T662" id="Seg_6738" s="T661">v</ta>
            <ta e="T663" id="Seg_6739" s="T662">n</ta>
            <ta e="T665" id="Seg_6740" s="T664">conj</ta>
            <ta e="T666" id="Seg_6741" s="T665">pers</ta>
            <ta e="T667" id="Seg_6742" s="T666">v</ta>
            <ta e="T668" id="Seg_6743" s="T667">v</ta>
            <ta e="T672" id="Seg_6744" s="T671">v</ta>
            <ta e="T673" id="Seg_6745" s="T672">pers</ta>
            <ta e="T674" id="Seg_6746" s="T673">n</ta>
            <ta e="T675" id="Seg_6747" s="T674">v</ta>
            <ta e="T677" id="Seg_6748" s="T676">conj</ta>
            <ta e="T678" id="Seg_6749" s="T677">n</ta>
            <ta e="T680" id="Seg_6750" s="T679">v</ta>
            <ta e="T681" id="Seg_6751" s="T680">ptcl</ta>
            <ta e="T683" id="Seg_6752" s="T682">ptcl</ta>
            <ta e="T684" id="Seg_6753" s="T683">ptcl</ta>
            <ta e="T686" id="Seg_6754" s="T685">n</ta>
            <ta e="T687" id="Seg_6755" s="T686">v</ta>
            <ta e="T688" id="Seg_6756" s="T687">ptcl</ta>
            <ta e="T689" id="Seg_6757" s="T688">ptcl</ta>
            <ta e="T690" id="Seg_6758" s="T689">v</ta>
            <ta e="T692" id="Seg_6759" s="T691">pers</ta>
            <ta e="T693" id="Seg_6760" s="T692">ptcl</ta>
            <ta e="T694" id="Seg_6761" s="T693">adj</ta>
            <ta e="T695" id="Seg_6762" s="T694">n</ta>
            <ta e="T697" id="Seg_6763" s="T696">conj</ta>
            <ta e="T698" id="Seg_6764" s="T697">pers</ta>
            <ta e="T699" id="Seg_6765" s="T698">adj</ta>
            <ta e="T700" id="Seg_6766" s="T699">n</ta>
            <ta e="T702" id="Seg_6767" s="T701">pers</ta>
            <ta e="T703" id="Seg_6768" s="T702">n</ta>
            <ta e="T706" id="Seg_6769" s="T705">v</ta>
            <ta e="T707" id="Seg_6770" s="T706">ptcl</ta>
            <ta e="T708" id="Seg_6771" s="T707">v</ta>
            <ta e="T710" id="Seg_6772" s="T709">conj</ta>
            <ta e="T711" id="Seg_6773" s="T710">pers</ta>
            <ta e="T712" id="Seg_6774" s="T711">adv</ta>
            <ta e="T715" id="Seg_6775" s="T714">ptcl</ta>
            <ta e="T716" id="Seg_6776" s="T715">ptcl</ta>
            <ta e="T717" id="Seg_6777" s="T716">adj</ta>
            <ta e="T718" id="Seg_6778" s="T717">ptcl</ta>
            <ta e="T719" id="Seg_6779" s="T718">v</ta>
            <ta e="T721" id="Seg_6780" s="T720">v</ta>
            <ta e="T722" id="Seg_6781" s="T721">ptcl</ta>
            <ta e="T723" id="Seg_6782" s="T722">ptcl</ta>
            <ta e="T724" id="Seg_6783" s="T723">ptcl</ta>
            <ta e="T725" id="Seg_6784" s="T724">v</ta>
            <ta e="T727" id="Seg_6785" s="T726">ptcl</ta>
            <ta e="T728" id="Seg_6786" s="T727">adj</ta>
            <ta e="T730" id="Seg_6787" s="T729">pers</ta>
            <ta e="T733" id="Seg_6788" s="T732">adj</ta>
            <ta e="T734" id="Seg_6789" s="T733">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-SAE">
            <ta e="T10" id="Seg_6790" s="T9">np.h:A</ta>
            <ta e="T20" id="Seg_6791" s="T19">0.3.h:A</ta>
            <ta e="T25" id="Seg_6792" s="T24">pro.h:A</ta>
            <ta e="T29" id="Seg_6793" s="T28">pro.h:A</ta>
            <ta e="T32" id="Seg_6794" s="T31">pro.h:R</ta>
            <ta e="T35" id="Seg_6795" s="T34">0.2.h:A</ta>
            <ta e="T36" id="Seg_6796" s="T35">adv:G</ta>
            <ta e="T39" id="Seg_6797" s="T38">pro.h:A</ta>
            <ta e="T47" id="Seg_6798" s="T46">pro.h:Poss</ta>
            <ta e="T48" id="Seg_6799" s="T47">np:Th</ta>
            <ta e="T67" id="Seg_6800" s="T66">pro.h:Th</ta>
            <ta e="T69" id="Seg_6801" s="T68">0.2.h:A</ta>
            <ta e="T70" id="Seg_6802" s="T69">adv:L</ta>
            <ta e="T73" id="Seg_6803" s="T72">pro.h:A</ta>
            <ta e="T77" id="Seg_6804" s="T76">pro.h:Poss</ta>
            <ta e="T78" id="Seg_6805" s="T77">np:E</ta>
            <ta e="T85" id="Seg_6806" s="T84">0.1.h:A</ta>
            <ta e="T86" id="Seg_6807" s="T85">pro:G</ta>
            <ta e="T87" id="Seg_6808" s="T86">np:P</ta>
            <ta e="T92" id="Seg_6809" s="T91">0.1.h:A</ta>
            <ta e="T96" id="Seg_6810" s="T95">0.1.h:A</ta>
            <ta e="T97" id="Seg_6811" s="T96">np:Th</ta>
            <ta e="T105" id="Seg_6812" s="T104">pro.h:E</ta>
            <ta e="T107" id="Seg_6813" s="T106">adv:L</ta>
            <ta e="T108" id="Seg_6814" s="T107">0.3.h:A</ta>
            <ta e="T111" id="Seg_6815" s="T110">pro.h:A</ta>
            <ta e="T114" id="Seg_6816" s="T113">pro.h:E</ta>
            <ta e="T118" id="Seg_6817" s="T117">0.1.h:A</ta>
            <ta e="T120" id="Seg_6818" s="T119">0.1.h:A</ta>
            <ta e="T122" id="Seg_6819" s="T121">0.2.h:A</ta>
            <ta e="T126" id="Seg_6820" s="T125">0.1.h:A</ta>
            <ta e="T133" id="Seg_6821" s="T132">0.2.h:A</ta>
            <ta e="T135" id="Seg_6822" s="T134">pro.h:A</ta>
            <ta e="T144" id="Seg_6823" s="T143">pro.h:A</ta>
            <ta e="T145" id="Seg_6824" s="T144">np.h:Com</ta>
            <ta e="T149" id="Seg_6825" s="T148">pro.h:A</ta>
            <ta e="T156" id="Seg_6826" s="T155">pro.h:A</ta>
            <ta e="T161" id="Seg_6827" s="T160">0.2.h:E</ta>
            <ta e="T162" id="Seg_6828" s="T161">np.h:Com</ta>
            <ta e="T164" id="Seg_6829" s="T163">0.2.h:E</ta>
            <ta e="T168" id="Seg_6830" s="T167">0.2.h:P</ta>
            <ta e="T170" id="Seg_6831" s="T169">pro.h:Th</ta>
            <ta e="T172" id="Seg_6832" s="T171">np.h:Th</ta>
            <ta e="T178" id="Seg_6833" s="T177">pro.h:Poss</ta>
            <ta e="T179" id="Seg_6834" s="T178">np.h:Th</ta>
            <ta e="T180" id="Seg_6835" s="T179">np.h:Th</ta>
            <ta e="T183" id="Seg_6836" s="T182">np.h:Th</ta>
            <ta e="T186" id="Seg_6837" s="T185">pro.h:Poss np.h:Th</ta>
            <ta e="T190" id="Seg_6838" s="T189">np.h:Th pro.h:Poss</ta>
            <ta e="T192" id="Seg_6839" s="T191">pro.h:Poss np.h:Th</ta>
            <ta e="T199" id="Seg_6840" s="T198">0.2.h:A</ta>
            <ta e="T203" id="Seg_6841" s="T202">0.2.h:A</ta>
            <ta e="T215" id="Seg_6842" s="T214">pro.h:Poss</ta>
            <ta e="T216" id="Seg_6843" s="T215">np.h:Poss</ta>
            <ta e="T219" id="Seg_6844" s="T218">np.h:Th</ta>
            <ta e="T220" id="Seg_6845" s="T219">adv:L</ta>
            <ta e="T226" id="Seg_6846" s="T225">adv:L</ta>
            <ta e="T229" id="Seg_6847" s="T228">np.h:Th</ta>
            <ta e="T231" id="Seg_6848" s="T230">np.h:Th</ta>
            <ta e="T234" id="Seg_6849" s="T233">np.h:P</ta>
            <ta e="T251" id="Seg_6850" s="T250">np.h:P</ta>
            <ta e="T258" id="Seg_6851" s="T257">adv:Time</ta>
            <ta e="T265" id="Seg_6852" s="T264">0.1.h:Poss</ta>
            <ta e="T281" id="Seg_6853" s="T280">0.2.h:Th</ta>
            <ta e="T282" id="Seg_6854" s="T281">np:P</ta>
            <ta e="T287" id="Seg_6855" s="T286">pro.h:A</ta>
            <ta e="T294" id="Seg_6856" s="T293">np:Th</ta>
            <ta e="T303" id="Seg_6857" s="T302">pro.h:A</ta>
            <ta e="T311" id="Seg_6858" s="T310">0.1.h:A</ta>
            <ta e="T312" id="Seg_6859" s="T311">0.2.h:A</ta>
            <ta e="T313" id="Seg_6860" s="T312">pro.h:Com</ta>
            <ta e="T315" id="Seg_6861" s="T314">pro:Th</ta>
            <ta e="T316" id="Seg_6862" s="T315">np:Th</ta>
            <ta e="T318" id="Seg_6863" s="T317">pro:Th</ta>
            <ta e="T319" id="Seg_6864" s="T318">np:Th</ta>
            <ta e="T330" id="Seg_6865" s="T329">0.2.h:A</ta>
            <ta e="T331" id="Seg_6866" s="T330">adv:L</ta>
            <ta e="T332" id="Seg_6867" s="T331">pro:G</ta>
            <ta e="T337" id="Seg_6868" s="T336">pro:Th</ta>
            <ta e="T338" id="Seg_6869" s="T337">0.2.h:A</ta>
            <ta e="T342" id="Seg_6870" s="T341">pro.h:A</ta>
            <ta e="T344" id="Seg_6871" s="T343">pro.h:R</ta>
            <ta e="T347" id="Seg_6872" s="T346">pro.h:E</ta>
            <ta e="T348" id="Seg_6873" s="T347">pro:Th</ta>
            <ta e="T358" id="Seg_6874" s="T357">pro.h:A</ta>
            <ta e="T361" id="Seg_6875" s="T360">pro.h:Th</ta>
            <ta e="T363" id="Seg_6876" s="T362">np.h:Th</ta>
            <ta e="T366" id="Seg_6877" s="T365">pro.h:Th</ta>
            <ta e="T369" id="Seg_6878" s="T368">pro.h:Th</ta>
            <ta e="T374" id="Seg_6879" s="T373">0.2.h:A</ta>
            <ta e="T381" id="Seg_6880" s="T380">0.2.h:A</ta>
            <ta e="T392" id="Seg_6881" s="T391">pro.h:A</ta>
            <ta e="T394" id="Seg_6882" s="T393">pro:Th</ta>
            <ta e="T413" id="Seg_6883" s="T412">0.1.h:A</ta>
            <ta e="T415" id="Seg_6884" s="T414">np:Th</ta>
            <ta e="T417" id="Seg_6885" s="T416">0.1.h:A</ta>
            <ta e="T420" id="Seg_6886" s="T419">np:P</ta>
            <ta e="T432" id="Seg_6887" s="T431">np:Th</ta>
            <ta e="T437" id="Seg_6888" s="T436">np:P</ta>
            <ta e="T452" id="Seg_6889" s="T451">pro.h:Poss</ta>
            <ta e="T453" id="Seg_6890" s="T452">np:E</ta>
            <ta e="T456" id="Seg_6891" s="T455">adv:Time</ta>
            <ta e="T457" id="Seg_6892" s="T456">pro.h:Poss</ta>
            <ta e="T458" id="Seg_6893" s="T457">np:E</ta>
            <ta e="T464" id="Seg_6894" s="T463">0.2.h:A</ta>
            <ta e="T466" id="Seg_6895" s="T465">0.1.h:A</ta>
            <ta e="T474" id="Seg_6896" s="T473">0.2.h:A</ta>
            <ta e="T478" id="Seg_6897" s="T477">pro.h:A</ta>
            <ta e="T504" id="Seg_6898" s="T503">pro.h:Poss</ta>
            <ta e="T505" id="Seg_6899" s="T504">np:Th</ta>
            <ta e="T507" id="Seg_6900" s="T506">np:Th</ta>
            <ta e="T510" id="Seg_6901" s="T509">np:Th</ta>
            <ta e="T511" id="Seg_6902" s="T510">pro.h:R</ta>
            <ta e="T512" id="Seg_6903" s="T511">0.3:A</ta>
            <ta e="T514" id="Seg_6904" s="T513">np:Th</ta>
            <ta e="T515" id="Seg_6905" s="T514">pro.h:R</ta>
            <ta e="T516" id="Seg_6906" s="T515">0.3:A</ta>
            <ta e="T520" id="Seg_6907" s="T519">pro:L</ta>
            <ta e="T522" id="Seg_6908" s="T521">np:Th</ta>
            <ta e="T526" id="Seg_6909" s="T525">pro.h:A</ta>
            <ta e="T529" id="Seg_6910" s="T528">np:P</ta>
            <ta e="T556" id="Seg_6911" s="T555">pro.h:A</ta>
            <ta e="T560" id="Seg_6912" s="T559">pro:G</ta>
            <ta e="T563" id="Seg_6913" s="T562">pro:G</ta>
            <ta e="T564" id="Seg_6914" s="T563">pro:G</ta>
            <ta e="T566" id="Seg_6915" s="T565">0.1.h:A</ta>
            <ta e="T571" id="Seg_6916" s="T570">pro.h:Poss</ta>
            <ta e="T572" id="Seg_6917" s="T571">np:Th</ta>
            <ta e="T576" id="Seg_6918" s="T575">np:Th</ta>
            <ta e="T577" id="Seg_6919" s="T576">0.1.h:A</ta>
            <ta e="T582" id="Seg_6920" s="T581">0.2.h:A</ta>
            <ta e="T583" id="Seg_6921" s="T582">pro:G</ta>
            <ta e="T584" id="Seg_6922" s="T583">0.2.h:A</ta>
            <ta e="T586" id="Seg_6923" s="T585">0.1.h:A</ta>
            <ta e="T587" id="Seg_6924" s="T586">pro:G</ta>
            <ta e="T589" id="Seg_6925" s="T588">0.1.h:Th</ta>
            <ta e="T591" id="Seg_6926" s="T590">0.1.h:A</ta>
            <ta e="T597" id="Seg_6927" s="T596">0.1.h:A</ta>
            <ta e="T602" id="Seg_6928" s="T601">0.2.h:A</ta>
            <ta e="T605" id="Seg_6929" s="T604">pro.h:E</ta>
            <ta e="T611" id="Seg_6930" s="T610">pro.h:Poss</ta>
            <ta e="T612" id="Seg_6931" s="T611">np:G</ta>
            <ta e="T618" id="Seg_6932" s="T617">pro.h:Poss</ta>
            <ta e="T620" id="Seg_6933" s="T619">pro:Th</ta>
            <ta e="T621" id="Seg_6934" s="T620">0.2.h:A</ta>
            <ta e="T623" id="Seg_6935" s="T622">pro:G</ta>
            <ta e="T626" id="Seg_6936" s="T625">0.2.h:A</ta>
            <ta e="T627" id="Seg_6937" s="T626">pro:G</ta>
            <ta e="T631" id="Seg_6938" s="T630">np:G</ta>
            <ta e="T633" id="Seg_6939" s="T632">0.1.h:A</ta>
            <ta e="T634" id="Seg_6940" s="T633">0.1.h:E</ta>
            <ta e="T640" id="Seg_6941" s="T639">0.2.h:A</ta>
            <ta e="T641" id="Seg_6942" s="T640">pro.h:R</ta>
            <ta e="T642" id="Seg_6943" s="T641">np:Th</ta>
            <ta e="T645" id="Seg_6944" s="T644">pro.h:Poss</ta>
            <ta e="T647" id="Seg_6945" s="T646">np:Th</ta>
            <ta e="T651" id="Seg_6946" s="T650">np:Th pro.h:Poss</ta>
            <ta e="T655" id="Seg_6947" s="T654">0.3:Th</ta>
            <ta e="T656" id="Seg_6948" s="T655">0.3:Th</ta>
            <ta e="T658" id="Seg_6949" s="T657">0.2.h:A</ta>
            <ta e="T659" id="Seg_6950" s="T658">pro.h:R</ta>
            <ta e="T661" id="Seg_6951" s="T660">pro.h:Poss</ta>
            <ta e="T663" id="Seg_6952" s="T662">np:Th</ta>
            <ta e="T666" id="Seg_6953" s="T665">pro.h:A</ta>
            <ta e="T672" id="Seg_6954" s="T671">0.2.h:A</ta>
            <ta e="T673" id="Seg_6955" s="T672">pro:G</ta>
            <ta e="T674" id="Seg_6956" s="T673">np:P</ta>
            <ta e="T678" id="Seg_6957" s="T677">np:P</ta>
            <ta e="T686" id="Seg_6958" s="T685">np:P</ta>
            <ta e="T690" id="Seg_6959" s="T689">0.2.h:A</ta>
            <ta e="T692" id="Seg_6960" s="T691">pro.h:Th</ta>
            <ta e="T695" id="Seg_6961" s="T694">np.h:Th</ta>
            <ta e="T698" id="Seg_6962" s="T697">pro.h:Th</ta>
            <ta e="T700" id="Seg_6963" s="T699">np.h:Th</ta>
            <ta e="T702" id="Seg_6964" s="T701">pro.h:A</ta>
            <ta e="T703" id="Seg_6965" s="T702">np:P</ta>
            <ta e="T708" id="Seg_6966" s="T707">0.1.h:A</ta>
            <ta e="T711" id="Seg_6967" s="T710">pro.h:A</ta>
            <ta e="T730" id="Seg_6968" s="T729">pro.h:Th</ta>
            <ta e="T734" id="Seg_6969" s="T733">np.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-SAE">
            <ta e="T10" id="Seg_6970" s="T9">np.h:S</ta>
            <ta e="T13" id="Seg_6971" s="T12">v:pred</ta>
            <ta e="T16" id="Seg_6972" s="T15">adj:pred</ta>
            <ta e="T20" id="Seg_6973" s="T19">v:pred 0.3.h:S</ta>
            <ta e="T25" id="Seg_6974" s="T24">pro.h:S</ta>
            <ta e="T26" id="Seg_6975" s="T25">v:pred</ta>
            <ta e="T29" id="Seg_6976" s="T28">pro.h:S</ta>
            <ta e="T31" id="Seg_6977" s="T30">v:pred</ta>
            <ta e="T35" id="Seg_6978" s="T34">v:pred 0.2.h:S</ta>
            <ta e="T39" id="Seg_6979" s="T38">pro.h:S</ta>
            <ta e="T40" id="Seg_6980" s="T39">ptcl.neg</ta>
            <ta e="T41" id="Seg_6981" s="T40">v:pred</ta>
            <ta e="T48" id="Seg_6982" s="T47">np:S</ta>
            <ta e="T49" id="Seg_6983" s="T48">v:pred</ta>
            <ta e="T66" id="Seg_6984" s="T65">ptcl:pred</ta>
            <ta e="T67" id="Seg_6985" s="T66">pro.h:S</ta>
            <ta e="T69" id="Seg_6986" s="T68">v:pred 0.2.h:S</ta>
            <ta e="T73" id="Seg_6987" s="T72">pro.h:S</ta>
            <ta e="T74" id="Seg_6988" s="T73">ptcl.neg</ta>
            <ta e="T75" id="Seg_6989" s="T74">v:pred</ta>
            <ta e="T78" id="Seg_6990" s="T77">np:S</ta>
            <ta e="T79" id="Seg_6991" s="T78">adj:pred</ta>
            <ta e="T85" id="Seg_6992" s="T84">v:pred 0.1.h:S</ta>
            <ta e="T87" id="Seg_6993" s="T86">np:O</ta>
            <ta e="T88" id="Seg_6994" s="T87">s:purp</ta>
            <ta e="T91" id="Seg_6995" s="T90">ptcl.neg</ta>
            <ta e="T92" id="Seg_6996" s="T91">v:pred 0.1.h:S</ta>
            <ta e="T96" id="Seg_6997" s="T95">v:pred 0.1.h:S</ta>
            <ta e="T97" id="Seg_6998" s="T96">np:S</ta>
            <ta e="T98" id="Seg_6999" s="T97">adj:pred</ta>
            <ta e="T100" id="Seg_7000" s="T99">adj:pred</ta>
            <ta e="T108" id="Seg_7001" s="T107">v:pred 0.3.h:S</ta>
            <ta e="T111" id="Seg_7002" s="T110">pro.h:S</ta>
            <ta e="T112" id="Seg_7003" s="T111">ptcl.neg</ta>
            <ta e="T113" id="Seg_7004" s="T112">v:pred</ta>
            <ta e="T115" id="Seg_7005" s="T114">s:purp</ta>
            <ta e="T118" id="Seg_7006" s="T117">v:pred 0.1.h:S</ta>
            <ta e="T120" id="Seg_7007" s="T119">v:pred 0.1.h:S</ta>
            <ta e="T121" id="Seg_7008" s="T120">ptcl.neg</ta>
            <ta e="T122" id="Seg_7009" s="T121">v:pred 0.2.h:S</ta>
            <ta e="T125" id="Seg_7010" s="T124">ptcl.neg</ta>
            <ta e="T126" id="Seg_7011" s="T125">v:pred 0.1.h:S</ta>
            <ta e="T132" id="Seg_7012" s="T131">ptcl.neg</ta>
            <ta e="T133" id="Seg_7013" s="T132">v:pred 0.2.h:S</ta>
            <ta e="T135" id="Seg_7014" s="T134">pro.h:S</ta>
            <ta e="T136" id="Seg_7015" s="T135">v:pred</ta>
            <ta e="T144" id="Seg_7016" s="T143">pro.h:S</ta>
            <ta e="T146" id="Seg_7017" s="T145">ptcl.neg</ta>
            <ta e="T147" id="Seg_7018" s="T146">v:pred</ta>
            <ta e="T149" id="Seg_7019" s="T148">pro.h:S</ta>
            <ta e="T151" id="Seg_7020" s="T150">v:pred</ta>
            <ta e="T156" id="Seg_7021" s="T155">pro.h:S</ta>
            <ta e="T157" id="Seg_7022" s="T156">v:pred</ta>
            <ta e="T161" id="Seg_7023" s="T160">v:pred 0.2.h:S</ta>
            <ta e="T164" id="Seg_7024" s="T163">v:pred 0.2.h:S</ta>
            <ta e="T167" id="Seg_7025" s="T166">ptcl:pred</ta>
            <ta e="T168" id="Seg_7026" s="T167">cop 0.2.h:S</ta>
            <ta e="T170" id="Seg_7027" s="T169">pro.h:S</ta>
            <ta e="T172" id="Seg_7028" s="T171">n:pred</ta>
            <ta e="T179" id="Seg_7029" s="T178">np.h:S</ta>
            <ta e="T180" id="Seg_7030" s="T179">np.h:S</ta>
            <ta e="T183" id="Seg_7031" s="T182">n:pred</ta>
            <ta e="T186" id="Seg_7032" s="T185">np.h:S</ta>
            <ta e="T187" id="Seg_7033" s="T186">adj:pred</ta>
            <ta e="T190" id="Seg_7034" s="T189">np.h:S</ta>
            <ta e="T191" id="Seg_7035" s="T190">adj:pred</ta>
            <ta e="T192" id="Seg_7036" s="T191">np.h:S</ta>
            <ta e="T193" id="Seg_7037" s="T192">adj:pred</ta>
            <ta e="T199" id="Seg_7038" s="T198">v:pred 0.2.h:S</ta>
            <ta e="T203" id="Seg_7039" s="T202">v:pred 0.2.h:S</ta>
            <ta e="T219" id="Seg_7040" s="T218">np.h:S</ta>
            <ta e="T227" id="Seg_7041" s="T226">v:pred </ta>
            <ta e="T229" id="Seg_7042" s="T228">np.h:S</ta>
            <ta e="T231" id="Seg_7043" s="T230">np.h:S</ta>
            <ta e="T234" id="Seg_7044" s="T233">np.h:S</ta>
            <ta e="T235" id="Seg_7045" s="T234">v:pred</ta>
            <ta e="T251" id="Seg_7046" s="T250">np.h:S</ta>
            <ta e="T252" id="Seg_7047" s="T251">v:pred</ta>
            <ta e="T281" id="Seg_7048" s="T280">v:pred 0.2.h:S</ta>
            <ta e="T282" id="Seg_7049" s="T281">np:O</ta>
            <ta e="T284" id="Seg_7050" s="T283">s:purp</ta>
            <ta e="T287" id="Seg_7051" s="T286">pro.h:S</ta>
            <ta e="T289" id="Seg_7052" s="T288">v:pred</ta>
            <ta e="T294" id="Seg_7053" s="T293">np:S</ta>
            <ta e="T295" id="Seg_7054" s="T294">adj:pred</ta>
            <ta e="T303" id="Seg_7055" s="T302">pro.h:S</ta>
            <ta e="T305" id="Seg_7056" s="T304">v:pred</ta>
            <ta e="T311" id="Seg_7057" s="T310">v:pred 0.1.h:S</ta>
            <ta e="T312" id="Seg_7058" s="T311">v:pred 0.2.h:S</ta>
            <ta e="T315" id="Seg_7059" s="T314">pro:S</ta>
            <ta e="T316" id="Seg_7060" s="T315">n:pred</ta>
            <ta e="T318" id="Seg_7061" s="T317">pro:S</ta>
            <ta e="T319" id="Seg_7062" s="T318">n:pred</ta>
            <ta e="T323" id="Seg_7063" s="T322">ptcl:pred</ta>
            <ta e="T330" id="Seg_7064" s="T329">v:pred 0.2.h:S</ta>
            <ta e="T337" id="Seg_7065" s="T336">pro:O</ta>
            <ta e="T338" id="Seg_7066" s="T337">v:pred 0.2.h:S</ta>
            <ta e="T342" id="Seg_7067" s="T341">pro.h:S</ta>
            <ta e="T343" id="Seg_7068" s="T342">v:pred</ta>
            <ta e="T347" id="Seg_7069" s="T346">pro.h:S</ta>
            <ta e="T348" id="Seg_7070" s="T347">pro:O</ta>
            <ta e="T349" id="Seg_7071" s="T348">ptcl.neg</ta>
            <ta e="T350" id="Seg_7072" s="T349">v:pred</ta>
            <ta e="T355" id="Seg_7073" s="T354">ptcl.neg</ta>
            <ta e="T358" id="Seg_7074" s="T357">pro.h:S</ta>
            <ta e="T359" id="Seg_7075" s="T358">v:pred</ta>
            <ta e="T361" id="Seg_7076" s="T360">pro.h:S</ta>
            <ta e="T363" id="Seg_7077" s="T362">n:pred</ta>
            <ta e="T366" id="Seg_7078" s="T365">pro.h:S</ta>
            <ta e="T367" id="Seg_7079" s="T366">adj:pred</ta>
            <ta e="T369" id="Seg_7080" s="T368">pro.h:S</ta>
            <ta e="T370" id="Seg_7081" s="T369">adj:pred</ta>
            <ta e="T374" id="Seg_7082" s="T373">v:pred 0.2.h:S</ta>
            <ta e="T380" id="Seg_7083" s="T379">ptcl.neg</ta>
            <ta e="T381" id="Seg_7084" s="T380">v:pred 0.2.h:S</ta>
            <ta e="T385" id="Seg_7085" s="T384">ptcl:pred</ta>
            <ta e="T392" id="Seg_7086" s="T391">pro.h:S</ta>
            <ta e="T394" id="Seg_7087" s="T393">pro:O</ta>
            <ta e="T395" id="Seg_7088" s="T394">ptcl.neg</ta>
            <ta e="T396" id="Seg_7089" s="T395">v:pred</ta>
            <ta e="T413" id="Seg_7090" s="T412">v:pred 0.1.h:S</ta>
            <ta e="T415" id="Seg_7091" s="T414">np:O</ta>
            <ta e="T417" id="Seg_7092" s="T416">v:pred 0.1.h:S</ta>
            <ta e="T419" id="Seg_7093" s="T418">ptcl:pred</ta>
            <ta e="T420" id="Seg_7094" s="T419">np:O</ta>
            <ta e="T432" id="Seg_7095" s="T431">np:S</ta>
            <ta e="T435" id="Seg_7096" s="T434">ptcl:pred</ta>
            <ta e="T437" id="Seg_7097" s="T436">np:O</ta>
            <ta e="T453" id="Seg_7098" s="T452">np:S</ta>
            <ta e="T454" id="Seg_7099" s="T453">v:pred</ta>
            <ta e="T458" id="Seg_7100" s="T457">np:S</ta>
            <ta e="T459" id="Seg_7101" s="T458">adj:pred</ta>
            <ta e="T464" id="Seg_7102" s="T463">v:pred 0.2.h:S</ta>
            <ta e="T466" id="Seg_7103" s="T465">v:pred 0.1.h:S</ta>
            <ta e="T474" id="Seg_7104" s="T473">v:pred 0.2.h:S</ta>
            <ta e="T478" id="Seg_7105" s="T477">pro.h:S</ta>
            <ta e="T480" id="Seg_7106" s="T479">v:pred</ta>
            <ta e="T483" id="Seg_7107" s="T482">v:pred</ta>
            <ta e="T505" id="Seg_7108" s="T504">np:S</ta>
            <ta e="T507" id="Seg_7109" s="T506">n:pred</ta>
            <ta e="T510" id="Seg_7110" s="T509">np:O</ta>
            <ta e="T512" id="Seg_7111" s="T511">v:pred 0.3:S</ta>
            <ta e="T514" id="Seg_7112" s="T513">np:O</ta>
            <ta e="T516" id="Seg_7113" s="T515">v:pred 0.3:S</ta>
            <ta e="T522" id="Seg_7114" s="T521">np:S</ta>
            <ta e="T526" id="Seg_7115" s="T525">pro.h:S</ta>
            <ta e="T527" id="Seg_7116" s="T526">v:pred</ta>
            <ta e="T529" id="Seg_7117" s="T528">np:O</ta>
            <ta e="T556" id="Seg_7118" s="T555">pro.h:S</ta>
            <ta e="T558" id="Seg_7119" s="T557">v:pred</ta>
            <ta e="T566" id="Seg_7120" s="T565">v:pred 0.1.h:S</ta>
            <ta e="T572" id="Seg_7121" s="T571">np:S</ta>
            <ta e="T573" id="Seg_7122" s="T572">v:pred</ta>
            <ta e="T576" id="Seg_7123" s="T575">np:O</ta>
            <ta e="T577" id="Seg_7124" s="T576">v:pred 0.1.h:S</ta>
            <ta e="T582" id="Seg_7125" s="T581">v:pred 0.2.h:S</ta>
            <ta e="T584" id="Seg_7126" s="T583">v:pred 0.2.h:S</ta>
            <ta e="T586" id="Seg_7127" s="T585">v:pred 0.1.h:S</ta>
            <ta e="T589" id="Seg_7128" s="T588">v:pred 0.1.h:S</ta>
            <ta e="T591" id="Seg_7129" s="T590">v:pred 0.1.h:S</ta>
            <ta e="T597" id="Seg_7130" s="T596">v:pred 0.1.h:S</ta>
            <ta e="T602" id="Seg_7131" s="T601">v:pred 0.2.h:S</ta>
            <ta e="T605" id="Seg_7132" s="T604">pro.h:S</ta>
            <ta e="T607" id="Seg_7133" s="T606">v:pred</ta>
            <ta e="T609" id="Seg_7134" s="T608">ptcl:pred</ta>
            <ta e="T620" id="Seg_7135" s="T619">pro:O</ta>
            <ta e="T621" id="Seg_7136" s="T620">v:pred 0.2.h:S</ta>
            <ta e="T626" id="Seg_7137" s="T625">v:pred 0.2.h:S</ta>
            <ta e="T633" id="Seg_7138" s="T632">v:pred 0.1.h:S</ta>
            <ta e="T634" id="Seg_7139" s="T633">v:pred 0.1.h:S</ta>
            <ta e="T640" id="Seg_7140" s="T639">v:pred 0.2.h:S</ta>
            <ta e="T642" id="Seg_7141" s="T641">np:O</ta>
            <ta e="T643" id="Seg_7142" s="T642">s:purp</ta>
            <ta e="T646" id="Seg_7143" s="T645">v:pred</ta>
            <ta e="T647" id="Seg_7144" s="T646">np:S</ta>
            <ta e="T651" id="Seg_7145" s="T650">np:S</ta>
            <ta e="T652" id="Seg_7146" s="T651">v:pred</ta>
            <ta e="T655" id="Seg_7147" s="T654">v:pred 0.3:S</ta>
            <ta e="T656" id="Seg_7148" s="T655">v:pred 0.3:S</ta>
            <ta e="T658" id="Seg_7149" s="T657">v:pred 0.2.h:S</ta>
            <ta e="T662" id="Seg_7150" s="T661">v:pred</ta>
            <ta e="T663" id="Seg_7151" s="T662">np:S</ta>
            <ta e="T666" id="Seg_7152" s="T665">pro.h:S</ta>
            <ta e="T668" id="Seg_7153" s="T667">v:pred</ta>
            <ta e="T672" id="Seg_7154" s="T671">v:pred 0.2.h:S</ta>
            <ta e="T674" id="Seg_7155" s="T673">np:O</ta>
            <ta e="T675" id="Seg_7156" s="T674">s:purp</ta>
            <ta e="T678" id="Seg_7157" s="T677">np:O</ta>
            <ta e="T681" id="Seg_7158" s="T680">ptcl:pred</ta>
            <ta e="T684" id="Seg_7159" s="T683">ptcl:pred</ta>
            <ta e="T686" id="Seg_7160" s="T685">np:O</ta>
            <ta e="T689" id="Seg_7161" s="T688">ptcl.neg</ta>
            <ta e="T690" id="Seg_7162" s="T689">v:pred 0.2.h:S</ta>
            <ta e="T692" id="Seg_7163" s="T691">pro.h:S</ta>
            <ta e="T695" id="Seg_7164" s="T694">n:pred</ta>
            <ta e="T698" id="Seg_7165" s="T697">pro.h:S</ta>
            <ta e="T700" id="Seg_7166" s="T699">n:pred</ta>
            <ta e="T702" id="Seg_7167" s="T701">pro.h:S</ta>
            <ta e="T703" id="Seg_7168" s="T702">np:O</ta>
            <ta e="T706" id="Seg_7169" s="T705">v:pred</ta>
            <ta e="T707" id="Seg_7170" s="T706">ptcl.neg</ta>
            <ta e="T708" id="Seg_7171" s="T707">v:pred 0.1.h:S</ta>
            <ta e="T711" id="Seg_7172" s="T710">pro.h:S</ta>
            <ta e="T713" id="Seg_7173" s="T712">v:pred</ta>
            <ta e="T716" id="Seg_7174" s="T715">ptcl.neg</ta>
            <ta e="T717" id="Seg_7175" s="T716">adj:pred</ta>
            <ta e="T722" id="Seg_7176" s="T721">ptcl:pred</ta>
            <ta e="T723" id="Seg_7177" s="T722">ptcl.neg</ta>
            <ta e="T724" id="Seg_7178" s="T723">ptcl:pred</ta>
            <ta e="T727" id="Seg_7179" s="T726">ptcl:pred</ta>
            <ta e="T730" id="Seg_7180" s="T729">pro.h:S</ta>
            <ta e="T734" id="Seg_7181" s="T733">n:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST-SAE" />
         <annotation name="BOR" tierref="BOR-SAE">
            <ta e="T10" id="Seg_7182" s="T9">TURK:core</ta>
            <ta e="T28" id="Seg_7183" s="T27">TURK:disc</ta>
            <ta e="T30" id="Seg_7184" s="T29">TURK:core</ta>
            <ta e="T38" id="Seg_7185" s="T37">RUS:disc</ta>
            <ta e="T44" id="Seg_7186" s="T43">RUS:gram</ta>
            <ta e="T63" id="Seg_7187" s="T62">TURK:core</ta>
            <ta e="T72" id="Seg_7188" s="T71">TURK:disc</ta>
            <ta e="T87" id="Seg_7189" s="T86">TURK:cult</ta>
            <ta e="T90" id="Seg_7190" s="T89">TURK:disc</ta>
            <ta e="T97" id="Seg_7191" s="T96">TURK:cult</ta>
            <ta e="T104" id="Seg_7192" s="T103">TURK:disc</ta>
            <ta e="T110" id="Seg_7193" s="T109">TURK:disc</ta>
            <ta e="T117" id="Seg_7194" s="T116">RUS:disc</ta>
            <ta e="T124" id="Seg_7195" s="T123">TURK:disc</ta>
            <ta e="T128" id="Seg_7196" s="T127">RUS:disc</ta>
            <ta e="T129" id="Seg_7197" s="T128">TURK:core</ta>
            <ta e="T131" id="Seg_7198" s="T130">TURK:core</ta>
            <ta e="T134" id="Seg_7199" s="T133">RUS:gram</ta>
            <ta e="T155" id="Seg_7200" s="T154">RUS:gram</ta>
            <ta e="T160" id="Seg_7201" s="T159">RUS:disc</ta>
            <ta e="T179" id="Seg_7202" s="T178">TURK:core</ta>
            <ta e="T185" id="Seg_7203" s="T184">RUS:gram</ta>
            <ta e="T187" id="Seg_7204" s="T186">TURK:core</ta>
            <ta e="T189" id="Seg_7205" s="T188">TURK:disc</ta>
            <ta e="T191" id="Seg_7206" s="T190">TURK:core</ta>
            <ta e="T204" id="Seg_7207" s="T203">TURK:core</ta>
            <ta e="T237" id="Seg_7208" s="T236">RUS:gram</ta>
            <ta e="T254" id="Seg_7209" s="T253">RUS:gram</ta>
            <ta e="T256" id="Seg_7210" s="T255">TURK:disc</ta>
            <ta e="T264" id="Seg_7211" s="T263">RUS:mod</ta>
            <ta e="T282" id="Seg_7212" s="T281">KHAK:cult</ta>
            <ta e="T283" id="Seg_7213" s="T282">KHAK:cult</ta>
            <ta e="T286" id="Seg_7214" s="T285">RUS:disc</ta>
            <ta e="T291" id="Seg_7215" s="T290">RUS:disc</ta>
            <ta e="T295" id="Seg_7216" s="T294">TURK:core</ta>
            <ta e="T298" id="Seg_7217" s="T297">TURK:cult</ta>
            <ta e="T302" id="Seg_7218" s="T301">RUS:disc</ta>
            <ta e="T304" id="Seg_7219" s="T303">RUS:mod</ta>
            <ta e="T316" id="Seg_7220" s="T315">TURK:cult</ta>
            <ta e="T323" id="Seg_7221" s="T322">RUS:mod</ta>
            <ta e="T334" id="Seg_7222" s="T333">RUS:gram</ta>
            <ta e="T337" id="Seg_7223" s="T336">RUS:gram(INDEF)</ta>
            <ta e="T340" id="Seg_7224" s="T339">TURK:disc</ta>
            <ta e="T341" id="Seg_7225" s="T340">RUS:gram</ta>
            <ta e="T346" id="Seg_7226" s="T345">RUS:gram</ta>
            <ta e="T348" id="Seg_7227" s="T347">TURK:gram(INDEF)</ta>
            <ta e="T354" id="Seg_7228" s="T353">TURK:gram(INDEF)</ta>
            <ta e="T357" id="Seg_7229" s="T356">RUS:gram</ta>
            <ta e="T362" id="Seg_7230" s="T361">TURK:core</ta>
            <ta e="T365" id="Seg_7231" s="T364">RUS:gram</ta>
            <ta e="T368" id="Seg_7232" s="T367">TURK:disc</ta>
            <ta e="T370" id="Seg_7233" s="T369">TURK:core</ta>
            <ta e="T385" id="Seg_7234" s="T384">RUS:mod</ta>
            <ta e="T387" id="Seg_7235" s="T386">TURK:core</ta>
            <ta e="T391" id="Seg_7236" s="T390">RUS:gram</ta>
            <ta e="T393" id="Seg_7237" s="T392">TURK:core</ta>
            <ta e="T394" id="Seg_7238" s="T393">TURK:gram(INDEF)</ta>
            <ta e="T409" id="Seg_7239" s="T408">RUS:gram</ta>
            <ta e="T419" id="Seg_7240" s="T418">RUS:mod</ta>
            <ta e="T420" id="Seg_7241" s="T419">TURK:cult</ta>
            <ta e="T424" id="Seg_7242" s="T423">TURK:cult</ta>
            <ta e="T426" id="Seg_7243" s="T425">RUS:gram</ta>
            <ta e="T428" id="Seg_7244" s="T427">TURK:cult</ta>
            <ta e="T430" id="Seg_7245" s="T429">TURK:disc</ta>
            <ta e="T432" id="Seg_7246" s="T431">TURK:cult</ta>
            <ta e="T435" id="Seg_7247" s="T434">RUS:mod</ta>
            <ta e="T436" id="Seg_7248" s="T435">TURK:core</ta>
            <ta e="T437" id="Seg_7249" s="T436">TURK:cult</ta>
            <ta e="T459" id="Seg_7250" s="T458">TURK:core</ta>
            <ta e="T471" id="Seg_7251" s="T470">RUS:gram</ta>
            <ta e="T476" id="Seg_7252" s="T475">TURK:disc</ta>
            <ta e="T487" id="Seg_7253" s="T486">RUS:disc</ta>
            <ta e="T490" id="Seg_7254" s="T489">TURK:disc</ta>
            <ta e="T492" id="Seg_7255" s="T491">TURK:disc</ta>
            <ta e="T495" id="Seg_7256" s="T494">RUS:disc</ta>
            <ta e="T496" id="Seg_7257" s="T495">RUS:gram</ta>
            <ta e="T498" id="Seg_7258" s="T497">RUS:gram</ta>
            <ta e="T505" id="Seg_7259" s="T504">TURK:cult</ta>
            <ta e="T506" id="Seg_7260" s="T505">TURK:core</ta>
            <ta e="T507" id="Seg_7261" s="T506">TURK:cult</ta>
            <ta e="T509" id="Seg_7262" s="T508">RUS:gram</ta>
            <ta e="T510" id="Seg_7263" s="T509">TURK:cult</ta>
            <ta e="T514" id="Seg_7264" s="T513">TURK:cult</ta>
            <ta e="T518" id="Seg_7265" s="T517">TURK:core</ta>
            <ta e="T522" id="Seg_7266" s="T521">TURK:cult</ta>
            <ta e="T524" id="Seg_7267" s="T523">RUS:gram</ta>
            <ta e="T529" id="Seg_7268" s="T528">TURK:cult</ta>
            <ta e="T562" id="Seg_7269" s="T561">RUS:mod</ta>
            <ta e="T570" id="Seg_7270" s="T569">RUS:gram</ta>
            <ta e="T572" id="Seg_7271" s="T571">TURK:cult</ta>
            <ta e="T576" id="Seg_7272" s="T575">TURK:cult</ta>
            <ta e="T579" id="Seg_7273" s="T578">RUS:gram</ta>
            <ta e="T587" id="Seg_7274" s="T586">RUS:gram(INDEF)</ta>
            <ta e="T590" id="Seg_7275" s="T589">RUS:gram</ta>
            <ta e="T593" id="Seg_7276" s="T592">TURK:disc</ta>
            <ta e="T595" id="Seg_7277" s="T594">TURK:disc</ta>
            <ta e="T599" id="Seg_7278" s="T598">RUS:gram</ta>
            <ta e="T604" id="Seg_7279" s="T603">RUS:gram</ta>
            <ta e="T609" id="Seg_7280" s="T608">RUS:mod</ta>
            <ta e="T612" id="Seg_7281" s="T611">TAT:cult</ta>
            <ta e="T614" id="Seg_7282" s="T613">RUS:gram</ta>
            <ta e="T616" id="Seg_7283" s="T615">TAT:cult</ta>
            <ta e="T620" id="Seg_7284" s="T619">RUS:gram(INDEF)</ta>
            <ta e="T625" id="Seg_7285" s="T624">RUS:gram</ta>
            <ta e="T629" id="Seg_7286" s="T628">RUS:disc</ta>
            <ta e="T631" id="Seg_7287" s="T630">TAT:cult</ta>
            <ta e="T642" id="Seg_7288" s="T641">TURK:cult</ta>
            <ta e="T647" id="Seg_7289" s="T646">TURK:cult</ta>
            <ta e="T650" id="Seg_7290" s="T649">RUS:gram</ta>
            <ta e="T654" id="Seg_7291" s="T653">TURK:disc</ta>
            <ta e="T663" id="Seg_7292" s="T662">TURK:cult</ta>
            <ta e="T665" id="Seg_7293" s="T664">RUS:gram</ta>
            <ta e="T674" id="Seg_7294" s="T673">TURK:cult</ta>
            <ta e="T677" id="Seg_7295" s="T676">RUS:gram</ta>
            <ta e="T678" id="Seg_7296" s="T677">TURK:cult</ta>
            <ta e="T681" id="Seg_7297" s="T680">RUS:mod</ta>
            <ta e="T683" id="Seg_7298" s="T682">RUS:disc</ta>
            <ta e="T684" id="Seg_7299" s="T683">RUS:mod</ta>
            <ta e="T686" id="Seg_7300" s="T685">TURK:cult</ta>
            <ta e="T688" id="Seg_7301" s="T687">RUS:gram</ta>
            <ta e="T693" id="Seg_7302" s="T692">RUS:mod</ta>
            <ta e="T697" id="Seg_7303" s="T696">RUS:gram</ta>
            <ta e="T699" id="Seg_7304" s="T698">TURK:core</ta>
            <ta e="T703" id="Seg_7305" s="T702">TURK:cult</ta>
            <ta e="T710" id="Seg_7306" s="T709">RUS:gram</ta>
            <ta e="T712" id="Seg_7307" s="T711">TURK:core</ta>
            <ta e="T717" id="Seg_7308" s="T716">TURK:core</ta>
            <ta e="T722" id="Seg_7309" s="T721">RUS:mod</ta>
            <ta e="T723" id="Seg_7310" s="T722">RUS:gram</ta>
            <ta e="T724" id="Seg_7311" s="T723">RUS:mod</ta>
            <ta e="T727" id="Seg_7312" s="T726">RUS:mod</ta>
            <ta e="T728" id="Seg_7313" s="T727">TURK:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-SAE" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-SAE" />
         <annotation name="CS" tierref="CS-SAE">
            <ta e="T7" id="Seg_7314" s="T5">RUS:ext</ta>
            <ta e="T52" id="Seg_7315" s="T50">RUS:ext</ta>
            <ta e="T60" id="Seg_7316" s="T54">RUS:ext</ta>
            <ta e="T83" id="Seg_7317" s="T82">RUS:ext</ta>
            <ta e="T142" id="Seg_7318" s="T141">RUS:ext</ta>
            <ta e="T174" id="Seg_7319" s="T173">RUS:ext</ta>
            <ta e="T244" id="Seg_7320" s="T239">RUS:ext</ta>
            <ta e="T248" id="Seg_7321" s="T245">RUS:ext</ta>
            <ta e="T272" id="Seg_7322" s="T267">RUS:ext</ta>
            <ta e="T277" id="Seg_7323" s="T273">RUS:ext</ta>
            <ta e="T288" id="Seg_7324" s="T287">RUS:int</ta>
            <ta e="T309" id="Seg_7325" s="T306">RUS:ext</ta>
            <ta e="T326" id="Seg_7326" s="T324">RUS:ext</ta>
            <ta e="T398" id="Seg_7327" s="T397">RUS:ext</ta>
            <ta e="T404" id="Seg_7328" s="T401">KHAK:ext</ta>
            <ta e="T407" id="Seg_7329" s="T405">KHAK:ext</ta>
            <ta e="T411" id="Seg_7330" s="T408">KHAK:ext</ta>
            <ta e="T415" id="Seg_7331" s="T412">KHAK:ext</ta>
            <ta e="T417" id="Seg_7332" s="T416">KHAK:ext</ta>
            <ta e="T421" id="Seg_7333" s="T418">KHAK:ext</ta>
            <ta e="T424" id="Seg_7334" s="T422">KHAK:ext</ta>
            <ta e="T428" id="Seg_7335" s="T425">KHAK:ext</ta>
            <ta e="T433" id="Seg_7336" s="T429">KHAK:ext</ta>
            <ta e="T438" id="Seg_7337" s="T434">KHAK:ext</ta>
            <ta e="T443" id="Seg_7338" s="T439">KHAK:ext</ta>
            <ta e="T446" id="Seg_7339" s="T444">KHAK:ext</ta>
            <ta e="T448" id="Seg_7340" s="T447">RUS:ext</ta>
            <ta e="T531" id="Seg_7341" s="T530">RUS:ext</ta>
            <ta e="T554" id="Seg_7342" s="T551">RUS:ext</ta>
            <ta e="T575" id="Seg_7343" s="T574">RUS:int</ta>
            <ta e="T636" id="Seg_7344" s="T635">RUS:ext</ta>
            <ta e="T649" id="Seg_7345" s="T648">RUS:int</ta>
            <ta e="T736" id="Seg_7346" s="T735">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-SAE">
            <ta e="T7" id="Seg_7347" s="T5">Говорить? </ta>
            <ta e="T13" id="Seg_7348" s="T9">Отец почему так говорит?</ta>
            <ta e="T16" id="Seg_7349" s="T14">Он (очень?) плохой.</ta>
            <ta e="T20" id="Seg_7350" s="T17">Он говорит (очень?) плохо.</ta>
            <ta e="T26" id="Seg_7351" s="T21">Почему ты так говоришь?</ta>
            <ta e="T32" id="Seg_7352" s="T27">Нет, я хорошо с тобой говорю.</ta>
            <ta e="T36" id="Seg_7353" s="T33">(…) иди куда-нибудь. [?]</ta>
            <ta e="T41" id="Seg_7354" s="T37">Ну, я не пойду.</ta>
            <ta e="T45" id="Seg_7355" s="T42">Почему. [?]</ta>
            <ta e="T49" id="Seg_7356" s="T46">У меня руки нет.</ta>
            <ta e="T52" id="Seg_7357" s="T50">Ну вот. </ta>
            <ta e="T60" id="Seg_7358" s="T54">Ой, я беру в руки… </ta>
            <ta e="T67" id="Seg_7359" s="T62">[Как] медведь, почему ты такой?</ta>
            <ta e="T70" id="Seg_7360" s="T68">Иди сюда.</ta>
            <ta e="T75" id="Seg_7361" s="T71">Нет, я не пойду.</ta>
            <ta e="T79" id="Seg_7362" s="T76">У меня рука болит.</ta>
            <ta e="T83" id="Seg_7363" s="T82">Можно? </ta>
            <ta e="T88" id="Seg_7364" s="T84">Пойдем ко мне пить водку.</ta>
            <ta e="T92" id="Seg_7365" s="T89">Нет, не пойду.</ta>
            <ta e="T94" id="Seg_7366" s="T93">Почему?</ta>
            <ta e="T98" id="Seg_7367" s="T95">Пойдем, водка хорошая.</ta>
            <ta e="T102" id="Seg_7368" s="T99">Хорошая для питья.</ta>
            <ta e="T108" id="Seg_7369" s="T103">Нет, меня там побьют.</ta>
            <ta e="T115" id="Seg_7370" s="T109">Нет, я не дам тебя бить.</ta>
            <ta e="T118" id="Seg_7371" s="T116">Ну пойду.</ta>
            <ta e="T120" id="Seg_7372" s="T119">Пойду.</ta>
            <ta e="T122" id="Seg_7373" s="T120">Не дашь?</ta>
            <ta e="T126" id="Seg_7374" s="T123">Нет, не дам!</ta>
            <ta e="T129" id="Seg_7375" s="T127">Ну хорошо.</ta>
            <ta e="T136" id="Seg_7376" s="T130">Хорошо, если не дашь, я пойду.</ta>
            <ta e="T142" id="Seg_7377" s="T141">Сейчас. </ta>
            <ta e="T147" id="Seg_7378" s="T143">Я с матерью не буду.</ta>
            <ta e="T151" id="Seg_7379" s="T148">Я уйду.</ta>
            <ta e="T153" id="Seg_7380" s="T152">Почему?</ta>
            <ta e="T158" id="Seg_7381" s="T154">Да она сильно ругается.</ta>
            <ta e="T162" id="Seg_7382" s="T159">Ну, живи с матерью.</ta>
            <ta e="T164" id="Seg_7383" s="T163">Живи.</ta>
            <ta e="T168" id="Seg_7384" s="T165">Почему ты такой?</ta>
            <ta e="T172" id="Seg_7385" s="T169">Ты плохой человек.</ta>
            <ta e="T174" id="Seg_7386" s="T173">Вот. </ta>
            <ta e="T183" id="Seg_7387" s="T177">Твои родители (очень?) плохие люди.</ta>
            <ta e="T187" id="Seg_7388" s="T184">А мои хорошие.</ta>
            <ta e="T191" id="Seg_7389" s="T188">Нет, мои хорошие.</ta>
            <ta e="T193" id="Seg_7390" s="T191">Твои плохие.</ta>
            <ta e="T199" id="Seg_7391" s="T194">Ох, почему так говоришь?</ta>
            <ta e="T201" id="Seg_7392" s="T200">Черт.</ta>
            <ta e="T204" id="Seg_7393" s="T202">Говори хорошо.</ta>
            <ta e="T209" id="Seg_7394" s="T205">Ох, очень плохой человек.</ta>
            <ta e="T211" id="Seg_7395" s="T210">Черт.</ta>
            <ta e="T220" id="Seg_7396" s="T214">У моей матери пятеро детей.</ta>
            <ta e="T222" id="Seg_7397" s="T221">Ой… </ta>
            <ta e="T231" id="Seg_7398" s="T225">У нее было три дочери, два сына.</ta>
            <ta e="T235" id="Seg_7399" s="T232">Один сын умер.</ta>
            <ta e="T238" id="Seg_7400" s="T236">И дочь…</ta>
            <ta e="T244" id="Seg_7401" s="T239">Вот память вишь какая дурная. </ta>
            <ta e="T252" id="Seg_7402" s="T249">Две дочери умерли.</ta>
            <ta e="T256" id="Seg_7403" s="T253">А одна - нет.</ta>
            <ta e="T262" id="Seg_7404" s="T257">Сейчас один сын, одна дочь.</ta>
            <ta e="T266" id="Seg_7405" s="T263">О, мать очень…</ta>
            <ta e="T272" id="Seg_7406" s="T267">"Плакала" не могу выговорить никак. </ta>
            <ta e="T277" id="Seg_7407" s="T273">Что об дитях плакала. </ta>
            <ta e="T284" id="Seg_7408" s="T280">Садись чай пить.</ta>
            <ta e="T289" id="Seg_7409" s="T285">Ну, я только-только попил.</ta>
            <ta e="T292" id="Seg_7410" s="T290">Ну, мясо [ешь].</ta>
            <ta e="T295" id="Seg_7411" s="T293">Мясо хорошее.</ta>
            <ta e="T298" id="Seg_7412" s="T296">Мясо, хлеб.</ta>
            <ta e="T300" id="Seg_7413" s="T299">Есть.</ta>
            <ta e="T305" id="Seg_7414" s="T301">Ну, я только поел.</ta>
            <ta e="T309" id="Seg_7415" s="T306">Ну дак что. </ta>
            <ta e="T313" id="Seg_7416" s="T310">Поел, поешь со мной.</ta>
            <ta e="T316" id="Seg_7417" s="T314">Это хлеб.</ta>
            <ta e="T319" id="Seg_7418" s="T317">Это мясо.</ta>
            <ta e="T323" id="Seg_7419" s="T320">Есть можно.</ta>
            <ta e="T326" id="Seg_7420" s="T324">Ну вот. </ta>
            <ta e="T332" id="Seg_7421" s="T329">Иди сюда, ко мне.</ta>
            <ta e="T335" id="Seg_7422" s="T333">А что?</ta>
            <ta e="T338" id="Seg_7423" s="T336">Что-нибудь расскажешь.</ta>
            <ta e="T344" id="Seg_7424" s="T339">Нет, ты мне рассказывай.</ta>
            <ta e="T350" id="Seg_7425" s="T345">А я ничего не знаю.</ta>
            <ta e="T355" id="Seg_7426" s="T351">Почему ничего не…</ta>
            <ta e="T359" id="Seg_7427" s="T356">А я расскажу.</ta>
            <ta e="T363" id="Seg_7428" s="T360">Ты хороший человек.</ta>
            <ta e="T367" id="Seg_7429" s="T364">А я плохой.</ta>
            <ta e="T370" id="Seg_7430" s="T367">Нет, ты хороший.</ta>
            <ta e="T374" id="Seg_7431" s="T371">Почему ты так говоришь?</ta>
            <ta e="T377" id="Seg_7432" s="T375">Плохой, плохой.</ta>
            <ta e="T381" id="Seg_7433" s="T378">Не говори так.</ta>
            <ta e="T383" id="Seg_7434" s="T382">Плохой.</ta>
            <ta e="T387" id="Seg_7435" s="T384">Надо говорить хорошо.</ta>
            <ta e="T389" id="Seg_7436" s="T388">Человек.</ta>
            <ta e="T396" id="Seg_7437" s="T390">А ты никогда ничего не говоришь.</ta>
            <ta e="T398" id="Seg_7438" s="T397">Вот. </ta>
            <ta e="T404" id="Seg_7439" s="T401">Какую ягоду вы принесли?</ta>
            <ta e="T407" id="Seg_7440" s="T405">Черную ягоду.</ta>
            <ta e="T411" id="Seg_7441" s="T408">А красную ягоду?</ta>
            <ta e="T415" id="Seg_7442" s="T412">Я принес красную ягоду.</ta>
            <ta e="T417" id="Seg_7443" s="T416">Я принес.</ta>
            <ta e="T421" id="Seg_7444" s="T418">Надо хлеб печь.</ta>
            <ta e="T424" id="Seg_7445" s="T422">Какой хлеб?</ta>
            <ta e="T428" id="Seg_7446" s="T425">Черный хлеб.</ta>
            <ta e="T433" id="Seg_7447" s="T429">Нет, черный хлеб плохой.</ta>
            <ta e="T438" id="Seg_7448" s="T434">Надо хороший хлеб печь.</ta>
            <ta e="T443" id="Seg_7449" s="T439">Мы пьем (?) чай.</ta>
            <ta e="T446" id="Seg_7450" s="T444">Я пью.</ta>
            <ta e="T448" id="Seg_7451" s="T447">Вот. </ta>
            <ta e="T454" id="Seg_7452" s="T451">У меня голова болит.</ta>
            <ta e="T459" id="Seg_7453" s="T455">Теперь моя голова хорошо.</ta>
            <ta e="T464" id="Seg_7454" s="T462">Куда ты идешь?</ta>
            <ta e="T466" id="Seg_7455" s="T465">Я приду.</ta>
            <ta e="T469" id="Seg_7456" s="T467">Почему. [?]</ta>
            <ta e="T472" id="Seg_7457" s="T470">Так что?</ta>
            <ta e="T474" id="Seg_7458" s="T473">Приходи.</ta>
            <ta e="T476" id="Seg_7459" s="T475">Нет!</ta>
            <ta e="T480" id="Seg_7460" s="T477">Я ухожу.</ta>
            <ta e="T483" id="Seg_7461" s="T481">Ты уходишь!</ta>
            <ta e="T485" id="Seg_7462" s="T484">Ты…</ta>
            <ta e="T487" id="Seg_7463" s="T486">A? </ta>
            <ta e="T493" id="Seg_7464" s="T488">(%)</ta>
            <ta e="T500" id="Seg_7465" s="T494">(%)</ta>
            <ta e="T507" id="Seg_7466" s="T503">Моя корова - хорошая корова.</ta>
            <ta e="T512" id="Seg_7467" s="T508">И молоко мне дает.</ta>
            <ta e="T516" id="Seg_7468" s="T513">Молоко мне дает.</ta>
            <ta e="T518" id="Seg_7469" s="T517">Хорошее.</ta>
            <ta e="T522" id="Seg_7470" s="T519">Где это молоко?</ta>
            <ta e="T529" id="Seg_7471" s="T523">Да где, я выпила это молоко.</ta>
            <ta e="T531" id="Seg_7472" s="T530">Вот. </ta>
            <ta e="T550" id="Seg_7473" s="T534">11, 12, 13, 14, 15, 16, 18, 19</ta>
            <ta e="T554" id="Seg_7474" s="T551">Вот и все. </ta>
            <ta e="T558" id="Seg_7475" s="T555">Я ухожу.</ta>
            <ta e="T560" id="Seg_7476" s="T559">Куда?</ta>
            <ta e="T566" id="Seg_7477" s="T561">О, куда я иду.</ta>
            <ta e="T568" id="Seg_7478" s="T567">Зачем?</ta>
            <ta e="T573" id="Seg_7479" s="T569">Да у меня хлеба нет.</ta>
            <ta e="T577" id="Seg_7480" s="T574">Может хлеба куплю.</ta>
            <ta e="T584" id="Seg_7481" s="T578">А потом куда ты пойдешь?</ta>
            <ta e="T587" id="Seg_7482" s="T585">Пойду куда-нибудь.</ta>
            <ta e="T591" id="Seg_7483" s="T588">Сяду и возьму (?)</ta>
            <ta e="T597" id="Seg_7484" s="T592">(Всё?) возьму.</ta>
            <ta e="T602" id="Seg_7485" s="T598">А почему ты так будешь [делать]?</ta>
            <ta e="T607" id="Seg_7486" s="T603">Так мне плохо живется.</ta>
            <ta e="T612" id="Seg_7487" s="T608">Надо идти к себе домой.</ta>
            <ta e="T618" id="Seg_7488" s="T613">А какой дом у тебя есть. [?]</ta>
            <ta e="T621" id="Seg_7489" s="T619">Дай мне что-нибудь!</ta>
            <ta e="T623" id="Seg_7490" s="T622">Куда?</ta>
            <ta e="T627" id="Seg_7491" s="T624">А потом куда?</ta>
            <ta e="T631" id="Seg_7492" s="T628">Ну, к себе домой.</ta>
            <ta e="T634" id="Seg_7493" s="T632">Приду, буду жить.</ta>
            <ta e="T636" id="Seg_7494" s="T635">Вот. </ta>
            <ta e="T643" id="Seg_7495" s="T639">Дай мне табак покурить.</ta>
            <ta e="T647" id="Seg_7496" s="T644">У меня нет табака.</ta>
            <ta e="T652" id="Seg_7497" s="T648">Ох, и у меня нет.</ta>
            <ta e="T656" id="Seg_7498" s="T653">Нет, есть, есть.</ta>
            <ta e="T659" id="Seg_7499" s="T657">Дай мне.</ta>
            <ta e="T663" id="Seg_7500" s="T660">У меня нет табака.</ta>
            <ta e="T668" id="Seg_7501" s="T664">А я курить хочу.</ta>
            <ta e="T675" id="Seg_7502" s="T671">Приходи ко мне пить водку.</ta>
            <ta e="T681" id="Seg_7503" s="T676">Да водку пить надо.</ta>
            <ta e="T690" id="Seg_7504" s="T682">Ну если надо водку пить, то не дерись.</ta>
            <ta e="T695" id="Seg_7505" s="T691">Ты ведь плохой человек!</ta>
            <ta e="T700" id="Seg_7506" s="T696">А я хороший человек.</ta>
            <ta e="T708" id="Seg_7507" s="T701">Я водки выпью — не дерусь.</ta>
            <ta e="T713" id="Seg_7508" s="T709">А ты всегда дерешься.</ta>
            <ta e="T719" id="Seg_7509" s="T714">Очень нехорошо так пить.</ta>
            <ta e="T725" id="Seg_7510" s="T720">Пить надо, не надо драться.</ta>
            <ta e="T728" id="Seg_7511" s="T726">Надо хорошо.</ta>
            <ta e="T734" id="Seg_7512" s="T729">Я видишь какой хороший человек.</ta>
            <ta e="T736" id="Seg_7513" s="T735">Вот. </ta>
         </annotation>
         <annotation name="fe" tierref="fe-SAE">
            <ta e="T7" id="Seg_7514" s="T5">Shall I speak?</ta>
            <ta e="T13" id="Seg_7515" s="T9">Why is the father speaking like this?</ta>
            <ta e="T16" id="Seg_7516" s="T14">He is (very?) bad.</ta>
            <ta e="T20" id="Seg_7517" s="T17">He speaks (very?) badly.</ta>
            <ta e="T26" id="Seg_7518" s="T21">Why are you speaking so?</ta>
            <ta e="T32" id="Seg_7519" s="T27">No, I'm speaking well to you.</ta>
            <ta e="T36" id="Seg_7520" s="T33">(…) go somewhere. [?]</ta>
            <ta e="T41" id="Seg_7521" s="T37">No, I won't go.</ta>
            <ta e="T45" id="Seg_7522" s="T42">Why. [?]</ta>
            <ta e="T49" id="Seg_7523" s="T46">I have no hand.</ta>
            <ta e="T52" id="Seg_7524" s="T50">Well.</ta>
            <ta e="T60" id="Seg_7525" s="T54">Oh, I'm taking it in my hands… </ta>
            <ta e="T67" id="Seg_7526" s="T62">[Like a bear], why are you so?</ta>
            <ta e="T70" id="Seg_7527" s="T68">Come here.</ta>
            <ta e="T75" id="Seg_7528" s="T71">No, I won't go.</ta>
            <ta e="T79" id="Seg_7529" s="T76">My hand hurts.</ta>
            <ta e="T83" id="Seg_7530" s="T82">May I?</ta>
            <ta e="T88" id="Seg_7531" s="T84">Let's go to drink vodka to me.</ta>
            <ta e="T92" id="Seg_7532" s="T89">No, I won't go.</ta>
            <ta e="T94" id="Seg_7533" s="T93">Why?</ta>
            <ta e="T98" id="Seg_7534" s="T95">Come, the vodka is good.</ta>
            <ta e="T102" id="Seg_7535" s="T99">Good to drink.</ta>
            <ta e="T108" id="Seg_7536" s="T103">No, they'll beat me there.</ta>
            <ta e="T115" id="Seg_7537" s="T109">No, I won't let [them] beat you.</ta>
            <ta e="T118" id="Seg_7538" s="T116">Well, I'll go.</ta>
            <ta e="T120" id="Seg_7539" s="T119">I'll go.</ta>
            <ta e="T122" id="Seg_7540" s="T120">You won't let [them]?</ta>
            <ta e="T126" id="Seg_7541" s="T123">No, I won't.</ta>
            <ta e="T129" id="Seg_7542" s="T127">Well then.</ta>
            <ta e="T136" id="Seg_7543" s="T130">Good, if you don't let [them beat me], I'll go.</ta>
            <ta e="T142" id="Seg_7544" s="T141">Moment.</ta>
            <ta e="T147" id="Seg_7545" s="T143">I won't be with [my] mother.</ta>
            <ta e="T151" id="Seg_7546" s="T148">I'll go away.</ta>
            <ta e="T153" id="Seg_7547" s="T152">Why?</ta>
            <ta e="T158" id="Seg_7548" s="T154">Because she scolds much.</ta>
            <ta e="T162" id="Seg_7549" s="T159">Well. live with your mother.</ta>
            <ta e="T164" id="Seg_7550" s="T163">Live.</ta>
            <ta e="T168" id="Seg_7551" s="T165">Why are you so?</ta>
            <ta e="T172" id="Seg_7552" s="T169">You are a bad person.</ta>
            <ta e="T174" id="Seg_7553" s="T173">That's it.</ta>
            <ta e="T183" id="Seg_7554" s="T177">Your parents are (very?) bad persons.</ta>
            <ta e="T187" id="Seg_7555" s="T184">Mine are good.</ta>
            <ta e="T191" id="Seg_7556" s="T188">No, mine are good.</ta>
            <ta e="T193" id="Seg_7557" s="T191">Yours are bad.</ta>
            <ta e="T199" id="Seg_7558" s="T194">Oh, why do you say so?</ta>
            <ta e="T201" id="Seg_7559" s="T200">A devil.</ta>
            <ta e="T204" id="Seg_7560" s="T202">Speak well.</ta>
            <ta e="T209" id="Seg_7561" s="T205">Oh, a very bad person.</ta>
            <ta e="T211" id="Seg_7562" s="T210">A devil.</ta>
            <ta e="T220" id="Seg_7563" s="T214">My (mother?) has five children.</ta>
            <ta e="T222" id="Seg_7564" s="T221">Oh…</ta>
            <ta e="T231" id="Seg_7565" s="T225">She had three daughters, two sons.</ta>
            <ta e="T235" id="Seg_7566" s="T232">One son died.</ta>
            <ta e="T238" id="Seg_7567" s="T236">And [a] daughter…</ta>
            <ta e="T244" id="Seg_7568" s="T239">You see, my memory is so bad.</ta>
            <ta e="T252" id="Seg_7569" s="T249">Two daughters died.</ta>
            <ta e="T256" id="Seg_7570" s="T253">One didn't.</ta>
            <ta e="T262" id="Seg_7571" s="T257">Now there are one son, one daughter.</ta>
            <ta e="T266" id="Seg_7572" s="T263">Oh, my mother [is] very…</ta>
            <ta e="T272" id="Seg_7573" s="T267">I can't pronounce "She cried".</ta>
            <ta e="T277" id="Seg_7574" s="T273">That she cried for her children.</ta>
            <ta e="T284" id="Seg_7575" s="T280">Sit down to drink tea.</ta>
            <ta e="T289" id="Seg_7576" s="T285">Well, I've just drunk.</ta>
            <ta e="T292" id="Seg_7577" s="T290">Then [eat] meat.</ta>
            <ta e="T295" id="Seg_7578" s="T293">The meat is good.</ta>
            <ta e="T298" id="Seg_7579" s="T296">Meat, bread.</ta>
            <ta e="T300" id="Seg_7580" s="T299">To eat.</ta>
            <ta e="T305" id="Seg_7581" s="T301">Well, I've just eaten.</ta>
            <ta e="T309" id="Seg_7582" s="T306">So what?</ta>
            <ta e="T313" id="Seg_7583" s="T310">[You] ate, eat with me.</ta>
            <ta e="T316" id="Seg_7584" s="T314">This is bread.</ta>
            <ta e="T319" id="Seg_7585" s="T317">This is meat.</ta>
            <ta e="T323" id="Seg_7586" s="T320">[One] can eat.</ta>
            <ta e="T326" id="Seg_7587" s="T324">Well than.</ta>
            <ta e="T332" id="Seg_7588" s="T329">Come here, to me.</ta>
            <ta e="T335" id="Seg_7589" s="T333">Why?</ta>
            <ta e="T338" id="Seg_7590" s="T336">You'll tell something.</ta>
            <ta e="T344" id="Seg_7591" s="T339">No, you tell me.</ta>
            <ta e="T350" id="Seg_7592" s="T345">I don't know anything.</ta>
            <ta e="T355" id="Seg_7593" s="T351">Why anything…</ta>
            <ta e="T359" id="Seg_7594" s="T356">I will tell.</ta>
            <ta e="T363" id="Seg_7595" s="T360">You're a good person.</ta>
            <ta e="T367" id="Seg_7596" s="T364">I am bad.</ta>
            <ta e="T370" id="Seg_7597" s="T367">No, you are good.</ta>
            <ta e="T374" id="Seg_7598" s="T371">Why do you say so?</ta>
            <ta e="T377" id="Seg_7599" s="T375">Bad, bad.</ta>
            <ta e="T381" id="Seg_7600" s="T378">Don't say so.</ta>
            <ta e="T383" id="Seg_7601" s="T382">Bad.</ta>
            <ta e="T387" id="Seg_7602" s="T384">You should speak well.</ta>
            <ta e="T389" id="Seg_7603" s="T388">Person</ta>
            <ta e="T396" id="Seg_7604" s="T390">And you never say anything.</ta>
            <ta e="T398" id="Seg_7605" s="T397">That's it.</ta>
            <ta e="T404" id="Seg_7606" s="T401">Which berries have you brought?</ta>
            <ta e="T407" id="Seg_7607" s="T405">Black berries.</ta>
            <ta e="T411" id="Seg_7608" s="T408">What about the red berries?</ta>
            <ta e="T415" id="Seg_7609" s="T412">I brought red berries.</ta>
            <ta e="T417" id="Seg_7610" s="T416">I brought.</ta>
            <ta e="T421" id="Seg_7611" s="T418">[We] should bake bread.</ta>
            <ta e="T424" id="Seg_7612" s="T422">What kind of bread?</ta>
            <ta e="T428" id="Seg_7613" s="T425">Black bread.</ta>
            <ta e="T433" id="Seg_7614" s="T429">No, the black bread is bad.</ta>
            <ta e="T438" id="Seg_7615" s="T434">[We] should bake good bread.</ta>
            <ta e="T443" id="Seg_7616" s="T439">We drink (?) tea.</ta>
            <ta e="T446" id="Seg_7617" s="T444">I drink.</ta>
            <ta e="T448" id="Seg_7618" s="T447">That's it.</ta>
            <ta e="T454" id="Seg_7619" s="T451">My head is aching.</ta>
            <ta e="T459" id="Seg_7620" s="T455">Now my head is good.</ta>
            <ta e="T464" id="Seg_7621" s="T462">Where are you going?</ta>
            <ta e="T466" id="Seg_7622" s="T465">I'll come.</ta>
            <ta e="T469" id="Seg_7623" s="T467">Why. [?]</ta>
            <ta e="T472" id="Seg_7624" s="T470">So what?</ta>
            <ta e="T474" id="Seg_7625" s="T473">Come.</ta>
            <ta e="T476" id="Seg_7626" s="T475">No.</ta>
            <ta e="T480" id="Seg_7627" s="T477">I'll go away.</ta>
            <ta e="T483" id="Seg_7628" s="T481">(You?)'ll go away!</ta>
            <ta e="T485" id="Seg_7629" s="T484">You…</ta>
            <ta e="T487" id="Seg_7630" s="T486">What?</ta>
            <ta e="T493" id="Seg_7631" s="T488">(%)</ta>
            <ta e="T500" id="Seg_7632" s="T494">(%)</ta>
            <ta e="T507" id="Seg_7633" s="T503">My cow is a good cow.</ta>
            <ta e="T512" id="Seg_7634" s="T508">And it gives me milk.</ta>
            <ta e="T516" id="Seg_7635" s="T513">It gives me milk.</ta>
            <ta e="T518" id="Seg_7636" s="T517">Good.</ta>
            <ta e="T522" id="Seg_7637" s="T519">Where is this milk?</ta>
            <ta e="T529" id="Seg_7638" s="T523">Where, I've drunk this milk.</ta>
            <ta e="T531" id="Seg_7639" s="T530">That's it.</ta>
            <ta e="T550" id="Seg_7640" s="T534">11, 12, 13, 14, 15, 16, 18, 19</ta>
            <ta e="T554" id="Seg_7641" s="T551">That's all.</ta>
            <ta e="T558" id="Seg_7642" s="T555">I'll go away.</ta>
            <ta e="T560" id="Seg_7643" s="T559">Where to?</ta>
            <ta e="T566" id="Seg_7644" s="T561">Oh, where am I going.</ta>
            <ta e="T568" id="Seg_7645" s="T567">What for?</ta>
            <ta e="T573" id="Seg_7646" s="T569">Because I have no bread.</ta>
            <ta e="T577" id="Seg_7647" s="T574">Maybe I'll buy bread.</ta>
            <ta e="T584" id="Seg_7648" s="T578">Then where will you go to?</ta>
            <ta e="T587" id="Seg_7649" s="T585">I'll go somewhere.</ta>
            <ta e="T591" id="Seg_7650" s="T588">[I?]'ll sit and take. [?]</ta>
            <ta e="T597" id="Seg_7651" s="T592">I'll take (everything?)</ta>
            <ta e="T602" id="Seg_7652" s="T598">Why will you [do] so?</ta>
            <ta e="T607" id="Seg_7653" s="T603">Because I'm living poorly.</ta>
            <ta e="T612" id="Seg_7654" s="T608">I must go to my place.</ta>
            <ta e="T618" id="Seg_7655" s="T613">What kind of house do you have (?)</ta>
            <ta e="T621" id="Seg_7656" s="T619">Give me anything!</ta>
            <ta e="T623" id="Seg_7657" s="T622">Where to?</ta>
            <ta e="T627" id="Seg_7658" s="T624">And then where?</ta>
            <ta e="T631" id="Seg_7659" s="T628">Well, to my place.</ta>
            <ta e="T634" id="Seg_7660" s="T632">I'll come and will live.</ta>
            <ta e="T636" id="Seg_7661" s="T635">That's it.</ta>
            <ta e="T643" id="Seg_7662" s="T639">Give me tobacco to smoke.</ta>
            <ta e="T647" id="Seg_7663" s="T644">I have no tobacco.</ta>
            <ta e="T652" id="Seg_7664" s="T648">Oh, I haven't either.</ta>
            <ta e="T656" id="Seg_7665" s="T653">Yes, [you] have.</ta>
            <ta e="T659" id="Seg_7666" s="T657">Give me.</ta>
            <ta e="T663" id="Seg_7667" s="T660">I don't have any tobacco.</ta>
            <ta e="T668" id="Seg_7668" s="T664">And I want to smoke.</ta>
            <ta e="T675" id="Seg_7669" s="T671">Come to drink vodka with me.</ta>
            <ta e="T681" id="Seg_7670" s="T676">Yes, [we] should drink vodka.</ta>
            <ta e="T690" id="Seg_7671" s="T682">Well, if you have to drink vodka, don't fight.</ta>
            <ta e="T695" id="Seg_7672" s="T691">Because you're a bad person!</ta>
            <ta e="T700" id="Seg_7673" s="T696">I am a good person.</ta>
            <ta e="T708" id="Seg_7674" s="T701">[When] I drink vodka, I don't fight.</ta>
            <ta e="T713" id="Seg_7675" s="T709">And you always fight.</ta>
            <ta e="T719" id="Seg_7676" s="T714">It's very bad to drink like this.</ta>
            <ta e="T725" id="Seg_7677" s="T720">You should drink, you should not fight.</ta>
            <ta e="T728" id="Seg_7678" s="T726">You should do it well.</ta>
            <ta e="T734" id="Seg_7679" s="T729">Look what a good person I am.</ta>
            <ta e="T736" id="Seg_7680" s="T735">That's it.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-SAE">
            <ta e="T7" id="Seg_7681" s="T5">Sprechen?</ta>
            <ta e="T13" id="Seg_7682" s="T9">Warum spricht der Vater so?</ta>
            <ta e="T16" id="Seg_7683" s="T14">Er ist (sehr?) schlecht.</ta>
            <ta e="T20" id="Seg_7684" s="T17">Er spricht (sehr?) schlecht.</ta>
            <ta e="T26" id="Seg_7685" s="T21">Warum sprichst du so?</ta>
            <ta e="T32" id="Seg_7686" s="T27">Nein, ich spreche gut zu dir.</ta>
            <ta e="T36" id="Seg_7687" s="T33">(…) geh' irgendwo hin. [?]</ta>
            <ta e="T41" id="Seg_7688" s="T37">Nein, ich werde nicht gehen.</ta>
            <ta e="T45" id="Seg_7689" s="T42">Warum. [?] </ta>
            <ta e="T49" id="Seg_7690" s="T46">Ich habe keine Hand.</ta>
            <ta e="T52" id="Seg_7691" s="T50">Nun dann.</ta>
            <ta e="T60" id="Seg_7692" s="T54">Oh, ich nehme es in die Hand…</ta>
            <ta e="T67" id="Seg_7693" s="T62">[Wie ein Bär], warum bist du so?</ta>
            <ta e="T70" id="Seg_7694" s="T68">Komm her.</ta>
            <ta e="T75" id="Seg_7695" s="T71">Nein, ich werde nicht gehen.</ta>
            <ta e="T79" id="Seg_7696" s="T76">Meine Hand tut weh.</ta>
            <ta e="T83" id="Seg_7697" s="T82">Darf ich?</ta>
            <ta e="T88" id="Seg_7698" s="T84">Lass uns zu mir gehen und Wodka trinken.</ta>
            <ta e="T92" id="Seg_7699" s="T89">Nein, ich werde nicht gehen.</ta>
            <ta e="T94" id="Seg_7700" s="T93">Warum?</ta>
            <ta e="T98" id="Seg_7701" s="T95">Komm, der Vodka ist gut.</ta>
            <ta e="T102" id="Seg_7702" s="T99">Gut zu trinken.</ta>
            <ta e="T108" id="Seg_7703" s="T103">Nein, sie werden mich dort schlagen.</ta>
            <ta e="T115" id="Seg_7704" s="T109">Nein, ich werde [sie] dich nicht schlagen lassen.</ta>
            <ta e="T118" id="Seg_7705" s="T116">Nun, ich werde gehen.</ta>
            <ta e="T120" id="Seg_7706" s="T119">Ich werde gehen.</ta>
            <ta e="T122" id="Seg_7707" s="T120">Du wirst [sie] nicht lassen?</ta>
            <ta e="T126" id="Seg_7708" s="T123">Nein, werde ich nicht.</ta>
            <ta e="T129" id="Seg_7709" s="T127">Nun dann.</ta>
            <ta e="T136" id="Seg_7710" s="T130">Gut, wenn du sie mich nicht schlagen lässt, werde ich gehen.</ta>
            <ta e="T142" id="Seg_7711" s="T141">Jetzt.</ta>
            <ta e="T147" id="Seg_7712" s="T143">Ich werde nicht mit meiner Mutter sein.</ta>
            <ta e="T151" id="Seg_7713" s="T148">Ich werde weggehen.</ta>
            <ta e="T153" id="Seg_7714" s="T152">Warum?</ta>
            <ta e="T158" id="Seg_7715" s="T154">Weil sie so viel flucht.</ta>
            <ta e="T162" id="Seg_7716" s="T159">Nun, lebe mit deiner Mutter.</ta>
            <ta e="T164" id="Seg_7717" s="T163">Lebe.</ta>
            <ta e="T168" id="Seg_7718" s="T165">Warum bist du so?</ta>
            <ta e="T172" id="Seg_7719" s="T169">Du bist eine schlechte Person.</ta>
            <ta e="T174" id="Seg_7720" s="T173">Nun.</ta>
            <ta e="T183" id="Seg_7721" s="T177">Deine Eltern sind (sehr?) schlechte Personen.</ta>
            <ta e="T187" id="Seg_7722" s="T184">Meine sind gut.</ta>
            <ta e="T191" id="Seg_7723" s="T188">Nein, meine sind gut.</ta>
            <ta e="T193" id="Seg_7724" s="T191">Deine sind schlecht.</ta>
            <ta e="T199" id="Seg_7725" s="T194">Oh, warum sagst du sowas?</ta>
            <ta e="T201" id="Seg_7726" s="T200">Ein Teufel.</ta>
            <ta e="T204" id="Seg_7727" s="T202">Sprich gut.</ta>
            <ta e="T209" id="Seg_7728" s="T205">Oh, eine sehr schlechte Person.</ta>
            <ta e="T211" id="Seg_7729" s="T210">Ein Teufel.</ta>
            <ta e="T220" id="Seg_7730" s="T214">Meine Mutter hat fünf Kinder.</ta>
            <ta e="T222" id="Seg_7731" s="T221">Oh…</ta>
            <ta e="T231" id="Seg_7732" s="T225">Sie hatte drei Töchter, zwei Söhne.</ta>
            <ta e="T235" id="Seg_7733" s="T232">Ein Sohn starb.</ta>
            <ta e="T238" id="Seg_7734" s="T236">Und eine Tochter…</ta>
            <ta e="T244" id="Seg_7735" s="T239">Siehst du, mein Gedächtnis ist so schlecht.</ta>
            <ta e="T252" id="Seg_7736" s="T249">Zwei Töchter starben.</ta>
            <ta e="T256" id="Seg_7737" s="T253">Eine nicht.</ta>
            <ta e="T262" id="Seg_7738" s="T257">Nun gibt es einen Sohn, eine Tochter.</ta>
            <ta e="T266" id="Seg_7739" s="T263">Oh, meine Mutter ist sehr…</ta>
            <ta e="T272" id="Seg_7740" s="T267">"Sie weinte" kann ich nicht aussprechen.</ta>
            <ta e="T277" id="Seg_7741" s="T273">Dass sie über ihre Kinder weinte.</ta>
            <ta e="T284" id="Seg_7742" s="T280">Setz dich hin und trink Tee.</ta>
            <ta e="T289" id="Seg_7743" s="T285">Nun, ich habe gerade getrunken.</ta>
            <ta e="T292" id="Seg_7744" s="T290">Dann [iss] Fleisch.</ta>
            <ta e="T295" id="Seg_7745" s="T293">Das Fleisch ist gut.</ta>
            <ta e="T298" id="Seg_7746" s="T296">Fleisch, Brot.</ta>
            <ta e="T300" id="Seg_7747" s="T299">Essen.</ta>
            <ta e="T305" id="Seg_7748" s="T301">Nun, ich habe gerade gegessen.</ta>
            <ta e="T309" id="Seg_7749" s="T306">Na und?</ta>
            <ta e="T313" id="Seg_7750" s="T310">Du hast gegessen, iss mit mir.</ta>
            <ta e="T316" id="Seg_7751" s="T314">Das ist Brot.</ta>
            <ta e="T319" id="Seg_7752" s="T317">Das ist Fleisch.</ta>
            <ta e="T323" id="Seg_7753" s="T320">Man kann es essen.</ta>
            <ta e="T326" id="Seg_7754" s="T324">Nun denn.</ta>
            <ta e="T332" id="Seg_7755" s="T329">Komm her zu mir.</ta>
            <ta e="T335" id="Seg_7756" s="T333">Warum?</ta>
            <ta e="T338" id="Seg_7757" s="T336">Du wirst etwas erzählen.</ta>
            <ta e="T344" id="Seg_7758" s="T339">Nein, du erzählst mir. </ta>
            <ta e="T350" id="Seg_7759" s="T345">Ich weiß nichts…</ta>
            <ta e="T355" id="Seg_7760" s="T351">Warum nichts…</ta>
            <ta e="T359" id="Seg_7761" s="T356">Ich werde erzählen.</ta>
            <ta e="T363" id="Seg_7762" s="T360">Du bist eine gute Person.</ta>
            <ta e="T367" id="Seg_7763" s="T364">Ich bin schlecht.</ta>
            <ta e="T370" id="Seg_7764" s="T367">Nein, du bist gut.</ta>
            <ta e="T374" id="Seg_7765" s="T371">Warum sagst du sowas?</ta>
            <ta e="T377" id="Seg_7766" s="T375">Schlecht, schlecht.</ta>
            <ta e="T381" id="Seg_7767" s="T378">Dann sprich nicht so.</ta>
            <ta e="T383" id="Seg_7768" s="T382">Schlecht.</ta>
            <ta e="T387" id="Seg_7769" s="T384">Du solltest gut sprechen.</ta>
            <ta e="T389" id="Seg_7770" s="T388">Person.</ta>
            <ta e="T396" id="Seg_7771" s="T390">Und du sagst niemals etwas.</ta>
            <ta e="T398" id="Seg_7772" s="T397">Nun.</ta>
            <ta e="T404" id="Seg_7773" s="T401">Welche Beeren hast du gekauft?</ta>
            <ta e="T407" id="Seg_7774" s="T405">Schwarze Beeren.</ta>
            <ta e="T411" id="Seg_7775" s="T408">Was ist mit roten Beeren?</ta>
            <ta e="T415" id="Seg_7776" s="T412">Ich kaufte rote Beeren.</ta>
            <ta e="T417" id="Seg_7777" s="T416">Ich kaufte.</ta>
            <ta e="T421" id="Seg_7778" s="T418">Wir sollten Brot backen.</ta>
            <ta e="T424" id="Seg_7779" s="T422">Welche Art Brot?</ta>
            <ta e="T428" id="Seg_7780" s="T425">Schwarzes Brot.</ta>
            <ta e="T433" id="Seg_7781" s="T429">Nein, das schwarze Brot ist schlecht.</ta>
            <ta e="T438" id="Seg_7782" s="T434">Wir sollten ein gutes Brot backen.</ta>
            <ta e="T443" id="Seg_7783" s="T439">Wir trinken (?) Tee.</ta>
            <ta e="T446" id="Seg_7784" s="T444">Ich trinke.</ta>
            <ta e="T448" id="Seg_7785" s="T447">Nun.</ta>
            <ta e="T454" id="Seg_7786" s="T451">Mein Kopf tut weh.</ta>
            <ta e="T459" id="Seg_7787" s="T455">Meinem Kopf geht es nun gut.</ta>
            <ta e="T464" id="Seg_7788" s="T462">Wohin gehst du?</ta>
            <ta e="T466" id="Seg_7789" s="T465">Ich werde kommen.</ta>
            <ta e="T469" id="Seg_7790" s="T467">Warum?</ta>
            <ta e="T472" id="Seg_7791" s="T470">Na und?</ta>
            <ta e="T474" id="Seg_7792" s="T473">Komm.</ta>
            <ta e="T476" id="Seg_7793" s="T475">Nein.</ta>
            <ta e="T480" id="Seg_7794" s="T477">Ich werde gehen.</ta>
            <ta e="T483" id="Seg_7795" s="T481">Du wirst gehen!</ta>
            <ta e="T485" id="Seg_7796" s="T484">Du…</ta>
            <ta e="T487" id="Seg_7797" s="T486">Was?</ta>
            <ta e="T493" id="Seg_7798" s="T488">(%)</ta>
            <ta e="T500" id="Seg_7799" s="T494">(%)</ta>
            <ta e="T507" id="Seg_7800" s="T503">Meine Kuh ist eine gute Kuh.</ta>
            <ta e="T512" id="Seg_7801" s="T508">Und sie gibt Milch.</ta>
            <ta e="T516" id="Seg_7802" s="T513">Sie gibt mir Milch.</ta>
            <ta e="T518" id="Seg_7803" s="T517">Gut.</ta>
            <ta e="T522" id="Seg_7804" s="T519">Wo ist die Milch?</ta>
            <ta e="T529" id="Seg_7805" s="T523">Wo, ich habe diese Milch getrunken.</ta>
            <ta e="T531" id="Seg_7806" s="T530">Nun.</ta>
            <ta e="T550" id="Seg_7807" s="T534">11, 12, 13, 14, 15, 16, 18, 19</ta>
            <ta e="T554" id="Seg_7808" s="T551">Das ist alles.</ta>
            <ta e="T558" id="Seg_7809" s="T555">Ich werde weggehen.</ta>
            <ta e="T560" id="Seg_7810" s="T559">Wohin?</ta>
            <ta e="T566" id="Seg_7811" s="T561">Oh, wohin ich gehe.</ta>
            <ta e="T568" id="Seg_7812" s="T567">Wofür?</ta>
            <ta e="T573" id="Seg_7813" s="T569">Weil ich kein Brot habe.</ta>
            <ta e="T577" id="Seg_7814" s="T574">Vielleicht werde ich Brot kaufen.</ta>
            <ta e="T584" id="Seg_7815" s="T578">Wohin wirst du dann gehen?</ta>
            <ta e="T587" id="Seg_7816" s="T585">Ich werde irgendwohin gehen.</ta>
            <ta e="T591" id="Seg_7817" s="T588">Ich werde sitzen und nehmen. [?] </ta>
            <ta e="T597" id="Seg_7818" s="T592">Ich werde (alles?) nehmen.</ta>
            <ta e="T602" id="Seg_7819" s="T598">Warum wirst du das so machen?</ta>
            <ta e="T607" id="Seg_7820" s="T603">Weil ich arm lebe.</ta>
            <ta e="T612" id="Seg_7821" s="T608">Ich muss nach Hause gehen.</ta>
            <ta e="T618" id="Seg_7822" s="T613">Welche Art Haus hast du?</ta>
            <ta e="T621" id="Seg_7823" s="T619">Gib mir alles.</ta>
            <ta e="T623" id="Seg_7824" s="T622">Wohin?</ta>
            <ta e="T627" id="Seg_7825" s="T624">Und dann wohin?</ta>
            <ta e="T631" id="Seg_7826" s="T628">Nun, zu mir nach Hause.</ta>
            <ta e="T634" id="Seg_7827" s="T632">Ich werde kommen, ich werde leben.</ta>
            <ta e="T636" id="Seg_7828" s="T635">Nun.</ta>
            <ta e="T643" id="Seg_7829" s="T639">Gib mir Tabak zum Rauchen.</ta>
            <ta e="T647" id="Seg_7830" s="T644">Ich habe keinen Tabak.</ta>
            <ta e="T652" id="Seg_7831" s="T648">Oh, ich habe auch keinen.</ta>
            <ta e="T656" id="Seg_7832" s="T653">Doch, hast du.</ta>
            <ta e="T659" id="Seg_7833" s="T657">Gib mir.</ta>
            <ta e="T663" id="Seg_7834" s="T660">Ich habe keinen Tabak.</ta>
            <ta e="T668" id="Seg_7835" s="T664">Und ich will rauchen.</ta>
            <ta e="T675" id="Seg_7836" s="T671">Komm Wodka mit mir trinken.</ta>
            <ta e="T681" id="Seg_7837" s="T676">Ja, wir sollten Wodka trinken.</ta>
            <ta e="T690" id="Seg_7838" s="T682">Nun, wenn du Wodka trinken musst, streite dich nicht.</ta>
            <ta e="T695" id="Seg_7839" s="T691">Weil du eine schlechte Person bist.</ta>
            <ta e="T700" id="Seg_7840" s="T696">Ich bin eine gute Person.</ta>
            <ta e="T708" id="Seg_7841" s="T701">Wenn ich Wodka trinke, streite ich mich nicht.</ta>
            <ta e="T713" id="Seg_7842" s="T709">Und du streitest dich immer.</ta>
            <ta e="T719" id="Seg_7843" s="T714">Es ist sehr schlecht so zu trinken.</ta>
            <ta e="T725" id="Seg_7844" s="T720">Du solltest trinken, du solltest dich nicht streiten.</ta>
            <ta e="T728" id="Seg_7845" s="T726">Du solltest es gut machen.</ta>
            <ta e="T734" id="Seg_7846" s="T729">Schau was für eine gute Person ich bin.</ta>
            <ta e="T736" id="Seg_7847" s="T735">Nun.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-SAE">
            <ta e="T220" id="Seg_7848" s="T214">[GVY:] jeŋ?</ta>
            <ta e="T284" id="Seg_7849" s="T280">[GVY:] Khakas sarɨɣ suɣ 'yellow water'</ta>
            <ta e="T389" id="Seg_7850" s="T388">[GVY:] This may be a part of the preceding sentence.</ta>
            <ta e="T404" id="Seg_7851" s="T401">[GVY:] This entire fragment is in Khakas.</ta>
            <ta e="T464" id="Seg_7852" s="T462">[GVY:] The verb endings are not clear; they resemble -ŋ.</ta>
            <ta e="T493" id="Seg_7853" s="T488">[GVY:] This and the next sentences are unclear.</ta>
            <ta e="T507" id="Seg_7854" s="T503">[GVY:] [tuzužoj]</ta>
            <ta e="T550" id="Seg_7855" s="T534">[GVY:] In Khakas.</ta>
            <ta e="T558" id="Seg_7856" s="T555">[GVY:] dʼürləl?</ta>
            <ta e="T584" id="Seg_7857" s="T578">[GVY:] guda = куда?</ta>
            <ta e="T591" id="Seg_7858" s="T588">[GVY:] Unclear.</ta>
            <ta e="T597" id="Seg_7859" s="T592">[GVY:] Or "buy"?</ta>
            <ta e="T618" id="Seg_7860" s="T613">[GVY:] This fragment is unclear.</ta>
            <ta e="T713" id="Seg_7861" s="T709">[GVY:] dʼabrolaps- liel.</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-MAK"
                      id="tx-MAK"
                      speaker="MAK"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-MAK">
            <ts e="T4" id="Seg_7862" n="sc" s="T3">
               <ts e="T4" id="Seg_7864" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_7866" n="HIAT:w" s="T3">Можно</ts>
                  <nts id="Seg_7867" n="HIAT:ip">.</nts>
                  <nts id="Seg_7868" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T8" id="Seg_7869" n="sc" s="T6">
               <ts e="T8" id="Seg_7871" n="HIAT:u" s="T6">
                  <ts e="T8" id="Seg_7873" n="HIAT:w" s="T6">Ага</ts>
                  <nts id="Seg_7874" n="HIAT:ip">.</nts>
                  <nts id="Seg_7875" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T61" id="Seg_7876" n="sc" s="T58">
               <ts e="T61" id="Seg_7878" n="HIAT:u" s="T58">
                  <ts e="T61" id="Seg_7880" n="HIAT:w" s="T58">Ничего</ts>
                  <nts id="Seg_7881" n="HIAT:ip">!</nts>
                  <nts id="Seg_7882" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T140" id="Seg_7883" n="sc" s="T139">
               <ts e="T140" id="Seg_7885" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_7887" n="HIAT:w" s="T139">Можно</ts>
                  <nts id="Seg_7888" n="HIAT:ip">.</nts>
                  <nts id="Seg_7889" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-MAK">
            <ts e="T4" id="Seg_7890" n="sc" s="T3">
               <ts e="T4" id="Seg_7892" n="e" s="T3">Можно. </ts>
            </ts>
            <ts e="T8" id="Seg_7893" n="sc" s="T6">
               <ts e="T8" id="Seg_7895" n="e" s="T6">Ага. </ts>
            </ts>
            <ts e="T61" id="Seg_7896" n="sc" s="T58">
               <ts e="T61" id="Seg_7898" n="e" s="T58">Ничего! </ts>
            </ts>
            <ts e="T140" id="Seg_7899" n="sc" s="T139">
               <ts e="T140" id="Seg_7901" n="e" s="T139">Можно. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-MAK">
            <ta e="T4" id="Seg_7902" s="T3">SAE_196X_SU0231.MAK.001 (002)</ta>
            <ta e="T8" id="Seg_7903" s="T6">SAE_196X_SU0231.MAK.002 (004)</ta>
            <ta e="T61" id="Seg_7904" s="T58">SAE_196X_SU0231.MAK.003 (017)</ta>
            <ta e="T140" id="Seg_7905" s="T139">SAE_196X_SU0231.MAK.004 (038)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-MAK">
            <ta e="T4" id="Seg_7906" s="T3">Можно. </ta>
            <ta e="T8" id="Seg_7907" s="T6">Ага. </ta>
            <ta e="T61" id="Seg_7908" s="T58">Ничего! </ta>
            <ta e="T140" id="Seg_7909" s="T139">Можно. </ta>
         </annotation>
         <annotation name="CS" tierref="CS-MAK">
            <ta e="T4" id="Seg_7910" s="T3">RUS:ext</ta>
            <ta e="T8" id="Seg_7911" s="T6">RUS:ext</ta>
            <ta e="T61" id="Seg_7912" s="T58">RUS:ext</ta>
            <ta e="T140" id="Seg_7913" s="T139">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-MAK" />
         <annotation name="fe" tierref="fe-MAK" />
         <annotation name="fg" tierref="fg-MAK">
            <ta e="T4" id="Seg_7914" s="T3">Ich kann.</ta>
            <ta e="T8" id="Seg_7915" s="T6">Aha,</ta>
            <ta e="T61" id="Seg_7916" s="T58">Schon gut!</ta>
            <ta e="T140" id="Seg_7917" s="T139">Ich kann.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-MAK" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-SAE"
                          name="ref"
                          segmented-tier-id="tx-SAE"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-SAE"
                          name="ts"
                          segmented-tier-id="tx-SAE"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-SAE"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-SAE"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-SAE"
                          name="mb"
                          segmented-tier-id="tx-SAE"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-SAE"
                          name="mp"
                          segmented-tier-id="tx-SAE"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-SAE"
                          name="ge"
                          segmented-tier-id="tx-SAE"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-SAE"
                          name="gr"
                          segmented-tier-id="tx-SAE"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-SAE"
                          name="mc"
                          segmented-tier-id="tx-SAE"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-SAE"
                          name="ps"
                          segmented-tier-id="tx-SAE"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-SAE"
                          name="SeR"
                          segmented-tier-id="tx-SAE"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-SAE"
                          name="SyF"
                          segmented-tier-id="tx-SAE"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-SAE"
                          name="IST"
                          segmented-tier-id="tx-SAE"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-SAE"
                          name="BOR"
                          segmented-tier-id="tx-SAE"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-SAE"
                          name="BOR-Phon"
                          segmented-tier-id="tx-SAE"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-SAE"
                          name="BOR-Morph"
                          segmented-tier-id="tx-SAE"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-SAE"
                          name="CS"
                          segmented-tier-id="tx-SAE"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-SAE"
                          name="fr"
                          segmented-tier-id="tx-SAE"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-SAE"
                          name="fe"
                          segmented-tier-id="tx-SAE"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-SAE"
                          name="fg"
                          segmented-tier-id="tx-SAE"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-SAE"
                          name="nt"
                          segmented-tier-id="tx-SAE"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-MAK"
                          name="ref"
                          segmented-tier-id="tx-MAK"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-MAK"
                          name="ts"
                          segmented-tier-id="tx-MAK"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-MAK"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-MAK"
                          type="t" />
         <conversion-tier category="CS"
                          display-name="CS-MAK"
                          name="CS"
                          segmented-tier-id="tx-MAK"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-MAK"
                          name="fr"
                          segmented-tier-id="tx-MAK"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-MAK"
                          name="fe"
                          segmented-tier-id="tx-MAK"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-MAK"
                          name="fg"
                          segmented-tier-id="tx-MAK"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-MAK"
                          name="nt"
                          segmented-tier-id="tx-MAK"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
