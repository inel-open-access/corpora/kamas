<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID2F1817C0-11F5-1F1C-BD58-AA2A382D3A96">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_1964_SU0205.wav" />
         <referenced-file url="PKZ_1964_SU0205.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_1964_SU0205\PKZ_1964_SU0205.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1475</ud-information>
            <ud-information attribute-name="# HIAT:w">966</ud-information>
            <ud-information attribute-name="# e">943</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">10</ud-information>
            <ud-information attribute-name="# HIAT:u">287</ud-information>
            <ud-information attribute-name="# sc">60</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KA">
            <abbreviation>KA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.135" type="appl" />
         <tli id="T1" time="0.887" type="appl" />
         <tli id="T2" time="1.638" type="appl" />
         <tli id="T3" time="2.39" type="appl" />
         <tli id="T4" time="3.8066414527163053" />
         <tli id="T5" time="4.452" type="appl" />
         <tli id="T6" time="4.794" type="appl" />
         <tli id="T7" time="5.137" type="appl" />
         <tli id="T8" time="5.479" type="appl" />
         <tli id="T9" time="5.821" type="appl" />
         <tli id="T10" time="6.163" type="appl" />
         <tli id="T11" time="6.506" type="appl" />
         <tli id="T12" time="6.848" type="appl" />
         <tli id="T13" time="6.853287939391177" />
         <tli id="T14" time="7.6799491305239656" />
         <tli id="T15" time="8.39" type="appl" />
         <tli id="T16" time="9.189" type="appl" />
         <tli id="T17" time="9.988" type="appl" />
         <tli id="T18" time="10.788" type="appl" />
         <tli id="T19" time="11.588" type="appl" />
         <tli id="T20" time="12.387" type="appl" />
         <tli id="T21" time="12.99" type="appl" />
         <tli id="T22" time="13.593" type="appl" />
         <tli id="T23" time="14.197" type="appl" />
         <tli id="T24" time="14.8" type="appl" />
         <tli id="T25" time="15.403" type="appl" />
         <tli id="T26" time="16.006" type="appl" />
         <tli id="T27" time="16.609" type="appl" />
         <tli id="T28" time="17.213" type="appl" />
         <tli id="T29" time="17.816" type="appl" />
         <tli id="T30" time="18.50654408535983" />
         <tli id="T31" time="19.118" type="appl" />
         <tli id="T32" time="19.778" type="appl" />
         <tli id="T33" time="20.438" type="appl" />
         <tli id="T34" time="21.235" type="appl" />
         <tli id="T35" time="22.033" type="appl" />
         <tli id="T36" time="22.83" type="appl" />
         <tli id="T37" time="23.628" type="appl" />
         <tli id="T38" time="24.425" type="appl" />
         <tli id="T39" time="25.767" type="appl" />
         <tli id="T40" time="26.332" type="appl" />
         <tli id="T41" time="26.896" type="appl" />
         <tli id="T42" time="26.993154539489176" />
         <tli id="T43" time="27.27157726974459" type="intp" />
         <tli id="T44" time="27.55" type="appl" />
         <tli id="T45" time="27.875" type="appl" />
         <tli id="T46" time="28.106480498514784" />
         <tli id="T47" time="28.286479306261445" />
         <tli id="T48" time="28.645652870840962" type="intp" />
         <tli id="T49" time="29.004826435420483" type="intp" />
         <tli id="T50" time="29.364" type="appl" />
         <tli id="T51" time="29.988" type="appl" />
         <tli id="T52" time="30.683" type="appl" />
         <tli id="T53" time="31.31" type="appl" />
         <tli id="T54" time="31.926455196249364" />
         <tli id="T55" time="32.72" type="appl" />
         <tli id="T56" time="33.502" type="appl" />
         <tli id="T57" time="34.782" type="appl" />
         <tli id="T58" time="35.765" type="appl" />
         <tli id="T59" time="36.747" type="appl" />
         <tli id="T60" time="38.268" type="appl" />
         <tli id="T61" time="39.619" type="appl" />
         <tli id="T62" time="41.619724324532214" />
         <tli id="T63" time="42.841" type="appl" />
         <tli id="T64" time="43.395" type="appl" />
         <tli id="T65" time="43.948" type="appl" />
         <tli id="T66" time="44.562" type="appl" />
         <tli id="T67" time="45.175" type="appl" />
         <tli id="T68" time="45.789" type="appl" />
         <tli id="T69" time="46.49" type="appl" />
         <tli id="T70" time="47.192" type="appl" />
         <tli id="T71" time="47.893" type="appl" />
         <tli id="T72" time="48.681" type="appl" />
         <tli id="T73" time="49.47" type="appl" />
         <tli id="T74" time="50.258" type="appl" />
         <tli id="T75" time="50.919" type="appl" />
         <tli id="T76" time="51.23" type="appl" />
         <tli id="T77" time="51.54" type="appl" />
         <tli id="T78" time="51.851" type="appl" />
         <tli id="T79" time="52.162" type="appl" />
         <tli id="T80" time="52.473" type="appl" />
         <tli id="T81" time="52.784" type="appl" />
         <tli id="T82" time="53.095" type="appl" />
         <tli id="T83" time="53.405" type="appl" />
         <tli id="T84" time="53.716" type="appl" />
         <tli id="T85" time="53.746310668649485" />
         <tli id="T86" time="54.94766989873811" />
         <tli id="T87" time="55.482" type="appl" />
         <tli id="T88" time="62.34625370543413" />
         <tli id="T89" time="62.892" type="appl" />
         <tli id="T90" time="63.417" type="appl" />
         <tli id="T91" time="64.697" type="appl" />
         <tli id="T92" time="65.698" type="appl" />
         <tli id="T93" time="66.7" type="appl" />
         <tli id="T94" time="67.8595505204891" />
         <tli id="T95" time="69.177" type="appl" />
         <tli id="T96" time="71.62619223815058" />
         <tli id="T97" time="72.619" type="appl" />
         <tli id="T98" time="73.246" type="appl" />
         <tli id="T99" time="73.872" type="appl" />
         <tli id="T100" time="74.499" type="appl" />
         <tli id="T101" time="75.4328336908669" />
         <tli id="T102" time="76.169" type="appl" />
         <tli id="T103" time="76.856" type="appl" />
         <tli id="T104" time="77.543" type="appl" />
         <tli id="T105" time="78.116" type="appl" />
         <tli id="T106" time="78.688" type="appl" />
         <tli id="T107" time="79.666" type="appl" />
         <tli id="T108" time="80.333" type="appl" />
         <tli id="T109" time="81.001" type="appl" />
         <tli id="T110" time="81.669" type="appl" />
         <tli id="T111" time="82.336" type="appl" />
         <tli id="T112" time="83.004" type="appl" />
         <tli id="T113" time="83.672" type="appl" />
         <tli id="T114" time="84.339" type="appl" />
         <tli id="T115" time="88.41941433866262" />
         <tli id="T116" time="89.586" type="appl" />
         <tli id="T117" time="90.389" type="appl" />
         <tli id="T118" time="91.221" type="appl" />
         <tli id="T119" time="92.53272042679916" />
         <tli id="T120" time="93.314" type="appl" />
         <tli id="T121" time="94.036" type="appl" />
         <tli id="T122" time="94.758" type="appl" />
         <tli id="T123" time="95.282" type="appl" />
         <tli id="T124" time="95.93936452896735" />
         <tli id="T125" time="96.678" type="appl" />
         <tli id="T126" time="101.98599114475313" />
         <tli id="T127" time="102.706" type="appl" />
         <tli id="T128" time="103.56598067941822" />
         <tli id="T129" time="104.564" type="appl" />
         <tli id="T130" time="105.527" type="appl" />
         <tli id="T131" time="106.69262663620272" />
         <tli id="T132" time="107.417" type="appl" />
         <tli id="T133" time="108.108" type="appl" />
         <tli id="T134" time="108.798" type="appl" />
         <tli id="T135" time="109.81260597047806" />
         <tli id="T136" time="111.239" type="appl" />
         <tli id="T137" time="112.63925391435147" />
         <tli id="T138" time="113.42" type="appl" />
         <tli id="T139" time="114.059" type="appl" />
         <tli id="T140" time="114.698" type="appl" />
         <tli id="T141" time="115.337" type="appl" />
         <tli id="T142" time="115.813" type="appl" />
         <tli id="T143" time="116.289" type="appl" />
         <tli id="T144" time="116.765" type="appl" />
         <tli id="T145" time="117.241" type="appl" />
         <tli id="T146" time="117.96" type="appl" />
         <tli id="T147" time="118.648" type="appl" />
         <tli id="T148" time="119.33" type="appl" />
         <tli id="T149" time="119.985" type="appl" />
         <tli id="T150" time="120.64" type="appl" />
         <tli id="T151" time="121.296" type="appl" />
         <tli id="T152" time="121.981" type="appl" />
         <tli id="T153" time="122.667" type="appl" />
         <tli id="T154" time="125.27917019167218" />
         <tli id="T155" time="126.258" type="appl" />
         <tli id="T156" time="126.887" type="appl" />
         <tli id="T157" time="127.515" type="appl" />
         <tli id="T158" time="128.143" type="appl" />
         <tli id="T159" time="128.771" type="appl" />
         <tli id="T160" time="129.4" type="appl" />
         <tli id="T161" time="130.51246886134345" />
         <tli id="T162" time="131.322" type="appl" />
         <tli id="T163" time="131.964" type="appl" />
         <tli id="T164" time="132.606" type="appl" />
         <tli id="T165" time="133.248" type="appl" />
         <tli id="T166" time="133.876" type="appl" />
         <tli id="T167" time="134.505" type="appl" />
         <tli id="T168" time="135.133" type="appl" />
         <tli id="T169" time="135.761" type="appl" />
         <tli id="T170" time="136.39" type="appl" />
         <tli id="T171" time="137.018" type="appl" />
         <tli id="T172" time="137.668" type="appl" />
         <tli id="T173" time="138.198" type="appl" />
         <tli id="T174" time="138.727" type="appl" />
         <tli id="T175" time="139.256" type="appl" />
         <tli id="T176" time="139.786" type="appl" />
         <tli id="T177" time="140.315" type="appl" />
         <tli id="T178" time="140.844" type="appl" />
         <tli id="T179" time="141.374" type="appl" />
         <tli id="T180" time="141.903" type="appl" />
         <tli id="T181" time="142.658" type="appl" />
         <tli id="T182" time="143.203" type="appl" />
         <tli id="T183" time="143.748" type="appl" />
         <tli id="T184" time="145.20570487147785" />
         <tli id="T185" time="146.102" type="appl" />
         <tli id="T186" time="146.734" type="appl" />
         <tli id="T187" time="147.365" type="appl" />
         <tli id="T188" time="147.997" type="appl" />
         <tli id="T189" time="148.56800030906376" />
         <tli id="T190" time="149.102" type="appl" />
         <tli id="T191" time="149.5949987148949" />
         <tli id="T192" time="150.306" type="appl" />
         <tli id="T193" time="151.2256649972271" />
         <tli id="T194" time="152.067" type="appl" />
         <tli id="T195" time="152.9389869820749" />
         <tli id="T196" time="154.82564115216022" />
         <tli id="T197" time="155.721" type="appl" />
         <tli id="T198" time="156.541" type="appl" />
         <tli id="T199" time="157.88562088385336" />
         <tli id="T200" time="158.656" type="appl" />
         <tli id="T201" time="159.173" type="appl" />
         <tli id="T202" time="159.69" type="appl" />
         <tli id="T203" time="160.208" type="appl" />
         <tli id="T204" time="160.725" type="appl" />
         <tli id="T205" time="161.242" type="appl" />
         <tli id="T206" time="161.759" type="appl" />
         <tli id="T207" time="162.276" type="appl" />
         <tli id="T208" time="162.964" type="appl" />
         <tli id="T209" time="163.652" type="appl" />
         <tli id="T210" time="164.339" type="appl" />
         <tli id="T211" time="165.027" type="appl" />
         <tli id="T212" time="166.152" type="appl" />
         <tli id="T213" time="166.954" type="appl" />
         <tli id="T214" time="167.379" type="appl" />
         <tli id="T215" time="167.805" type="appl" />
         <tli id="T216" time="168.23" type="appl" />
         <tli id="T217" time="168.656" type="appl" />
         <tli id="T218" time="169.081" type="appl" />
         <tli id="T219" time="170.41466549971838" />
         <tli id="T220" time="171.201" type="appl" />
         <tli id="T221" time="172.014" type="appl" />
         <tli id="T222" time="172.8721882843897" />
         <tli id="T223" time="177.05882722012672" />
         <tli id="T224" time="177.851" type="appl" />
         <tli id="T225" time="178.53" type="appl" />
         <tli id="T226" time="179.45881132341546" />
         <tli id="T227" time="180.221" type="appl" />
         <tli id="T228" time="180.861" type="appl" />
         <tli id="T229" time="181.62546363888444" />
         <tli id="T230" time="182.726" type="appl" />
         <tli id="T231" time="183.068" type="appl" />
         <tli id="T232" time="184.30544588755689" />
         <tli id="T233" time="185.31" type="appl" />
         <tli id="T234" time="186.18" type="appl" />
         <tli id="T235" time="187.05" type="appl" />
         <tli id="T236" time="187.92" type="appl" />
         <tli id="T237" time="189.139" type="appl" />
         <tli id="T238" time="190.359" type="appl" />
         <tli id="T239" time="191.578" type="appl" />
         <tli id="T240" time="192.304" type="appl" />
         <tli id="T241" time="193.012" type="appl" />
         <tli id="T242" time="193.72566994940115" />
         <tli id="T243" time="194.932" type="appl" />
         <tli id="T244" time="195.766" type="appl" />
         <tli id="T245" time="196.6" type="appl" />
         <tli id="T246" time="197.435" type="appl" />
         <tli id="T247" time="198.269" type="appl" />
         <tli id="T248" time="199.103" type="appl" />
         <tli id="T249" time="199.8320097113332" />
         <tli id="T250" time="201.719" type="appl" />
         <tli id="T251" time="206.284" type="appl" />
         <tli id="T252" time="206.672" type="appl" />
         <tli id="T253" time="207.06" type="appl" />
         <tli id="T254" time="207.449" type="appl" />
         <tli id="T255" time="207.837" type="appl" />
         <tli id="T256" time="208.7986169861203" />
         <tli id="T257" time="209.748" type="appl" />
         <tli id="T258" time="210.414" type="appl" />
         <tli id="T259" time="211.08" type="appl" />
         <tli id="T260" time="211.8919298303591" />
         <tli id="T261" time="212.562" type="appl" />
         <tli id="T262" time="213.377" type="appl" />
         <tli id="T263" time="214.191" type="appl" />
         <tli id="T264" time="215.006" type="appl" />
         <tli id="T265" time="215.651" type="appl" />
         <tli id="T266" time="216.2252344612971" />
         <tli id="T267" time="216.901" type="appl" />
         <tli id="T268" time="217.538" type="appl" />
         <tli id="T269" time="218.521" type="appl" />
         <tli id="T270" time="219.166" type="appl" />
         <tli id="T271" time="220.048" type="appl" />
         <tli id="T272" time="220.778" type="appl" />
         <tli id="T273" time="221.509" type="appl" />
         <tli id="T274" time="222.41186014977475" />
         <tli id="T275" time="223.358" type="appl" />
         <tli id="T276" time="224.40518028000625" />
         <tli id="T277" time="225.414" type="appl" />
         <tli id="T278" time="226.395" type="appl" />
         <tli id="T279" time="227.134" type="appl" />
         <tli id="T280" time="227.874" type="appl" />
         <tli id="T281" time="228.614" type="appl" />
         <tli id="T282" time="232.11179590056673" />
         <tli id="T283" time="232.963" type="appl" />
         <tli id="T284" time="233.598" type="appl" />
         <tli id="T285" time="234.232" type="appl" />
         <tli id="T286" time="234.866" type="appl" />
         <tli id="T287" time="235.5" type="appl" />
         <tli id="T288" time="236.135" type="appl" />
         <tli id="T289" time="236.78900450359708" />
         <tli id="T290" time="237.644" type="appl" />
         <tli id="T291" time="238.519" type="appl" />
         <tli id="T292" time="239.699" type="appl" />
         <tli id="T293" time="240.729" type="appl" />
         <tli id="T294" time="241.642" type="appl" />
         <tli id="T295" time="242.556" type="appl" />
         <tli id="T296" time="243.469" type="appl" />
         <tli id="T297" time="244.092" type="appl" />
         <tli id="T298" time="244.683" type="appl" />
         <tli id="T299" time="245.275" type="appl" />
         <tli id="T300" time="247.04503032103" />
         <tli id="T301" time="248.271" type="appl" />
         <tli id="T302" time="248.875" type="appl" />
         <tli id="T303" time="249.478" type="appl" />
         <tli id="T304" time="250.081" type="appl" />
         <tli id="T305" time="250.685" type="appl" />
         <tli id="T306" time="251.288" type="appl" />
         <tli id="T307" time="251.873" type="appl" />
         <tli id="T308" time="252.455" type="appl" />
         <tli id="T309" time="253.036" type="appl" />
         <tli id="T310" time="253.618" type="appl" />
         <tli id="T311" time="254.199" type="appl" />
         <tli id="T312" time="255.071" type="appl" />
         <tli id="T313" time="255.944" type="appl" />
         <tli id="T314" time="256.816" type="appl" />
         <tli id="T315" time="257.689" type="appl" />
         <tli id="T316" time="258.6449534869256" />
         <tli id="T317" time="259.367" type="appl" />
         <tli id="T318" time="259.993" type="appl" />
         <tli id="T319" time="260.618" type="appl" />
         <tli id="T320" time="261.532" type="appl" />
         <tli id="T321" time="264.09825069939836" />
         <tli id="T322" time="264.744" type="appl" />
         <tli id="T323" time="265.29165425301613" />
         <tli id="T324" time="266.109" type="appl" />
         <tli id="T325" time="266.872" type="appl" />
         <tli id="T326" time="268.9315520185215" />
         <tli id="T327" time="269.8" type="appl" />
         <tli id="T328" time="270.446" type="appl" />
         <tli id="T329" time="271.091" type="appl" />
         <tli id="T330" time="272.504861683418" />
         <tli id="T331" time="273.447" type="appl" />
         <tli id="T332" time="274.268" type="appl" />
         <tli id="T333" time="275.088" type="appl" />
         <tli id="T334" time="275.909" type="appl" />
         <tli id="T335" time="277.26483015494074" />
         <tli id="T336" time="278.384" type="appl" />
         <tli id="T337" time="279.317342601435" />
         <tli id="T338" time="279.72" type="appl" />
         <tli id="T339" time="280.155" type="appl" />
         <tli id="T340" time="280.591" type="appl" />
         <tli id="T341" time="281.15813770027574" />
         <tli id="T342" time="281.938" type="appl" />
         <tli id="T343" time="282.85145981759615" />
         <tli id="T344" time="283.74" type="appl" />
         <tli id="T345" time="287.471429216427" />
         <tli id="T346" time="288.562" type="appl" />
         <tli id="T347" time="289.342" type="appl" />
         <tli id="T348" time="290.31141040531867" />
         <tli id="T349" time="291.049" type="appl" />
         <tli id="T350" time="291.691" type="appl" />
         <tli id="T351" time="292.332" type="appl" />
         <tli id="T352" time="293.184724706756" />
         <tli id="T353" time="293.959" type="appl" />
         <tli id="T354" time="294.592" type="appl" />
         <tli id="T355" time="295.224" type="appl" />
         <tli id="T356" time="296.172" type="appl" />
         <tli id="T357" time="297.0980321196185" />
         <tli id="T358" time="299.2046848325052" />
         <tli id="T359" time="299.869" type="appl" />
         <tli id="T360" time="300.57" type="appl" />
         <tli id="T361" time="301.271" type="appl" />
         <tli id="T362" time="302.21799820663443" />
         <tli id="T363" time="303.178" type="appl" />
         <tli id="T364" time="304.55131608483185" />
         <tli id="T365" time="305.255" type="appl" />
         <tli id="T366" time="305.961" type="appl" />
         <tli id="T367" time="306.666" type="appl" />
         <tli id="T368" time="307.372" type="appl" />
         <tli id="T369" time="308.078" type="appl" />
         <tli id="T370" time="309.03795303347994" />
         <tli id="T371" time="309.852" type="appl" />
         <tli id="T372" time="310.525" type="appl" />
         <tli id="T373" time="311.197" type="appl" />
         <tli id="T374" time="311.87" type="appl" />
         <tli id="T375" time="313.50459011460066" />
         <tli id="T376" time="314.193" type="appl" />
         <tli id="T377" time="314.706" type="appl" />
         <tli id="T378" time="315.218" type="appl" />
         <tli id="T379" time="315.731" type="appl" />
         <tli id="T380" time="316.243" type="appl" />
         <tli id="T381" time="316.756" type="appl" />
         <tli id="T382" time="317.4645638850271" />
         <tli id="T383" time="318.15" type="appl" />
         <tli id="T384" time="320.77120864955816" />
         <tli id="T385" time="321.412" type="appl" />
         <tli id="T386" time="321.934" type="appl" />
         <tli id="T387" time="322.456" type="appl" />
         <tli id="T388" time="322.977" type="appl" />
         <tli id="T389" time="323.499" type="appl" />
         <tli id="T390" time="324.021" type="appl" />
         <tli id="T391" time="324.543" type="appl" />
         <tli id="T392" time="324.97834224121203" />
         <tli id="T393" time="325.752" type="appl" />
         <tli id="T394" time="326.44" type="appl" />
         <tli id="T395" time="327.47783089374843" />
         <tli id="T396" time="328.05" type="appl" />
         <tli id="T397" time="328.567" type="appl" />
         <tli id="T398" time="329.084" type="appl" />
         <tli id="T399" time="329.601" type="appl" />
         <tli id="T400" time="330.118" type="appl" />
         <tli id="T401" time="330.635" type="appl" />
         <tli id="T402" time="333.96445459513717" />
         <tli id="T403" time="334.665" type="appl" />
         <tli id="T404" time="335.091" type="appl" />
         <tli id="T405" time="335.516" type="appl" />
         <tli id="T406" time="335.941" type="appl" />
         <tli id="T407" time="336.367" type="appl" />
         <tli id="T408" time="338.3177590936026" />
         <tli id="T409" time="339.148" type="appl" />
         <tli id="T410" time="339.753" type="appl" />
         <tli id="T411" time="340.991" type="appl" />
         <tli id="T412" time="341.327" type="appl" />
         <tli id="T413" time="341.662" type="appl" />
         <tli id="T414" time="342.118" type="appl" />
         <tli id="T415" time="342.574" type="appl" />
         <tli id="T416" time="343.352" type="appl" />
         <tli id="T417" time="343.948" type="appl" />
         <tli id="T418" time="344.544" type="appl" />
         <tli id="T419" time="345.139" type="appl" />
         <tli id="T420" time="345.779" type="appl" />
         <tli id="T421" time="347.87102914919353" />
         <tli id="T422" time="348.415" type="appl" />
         <tli id="T423" time="348.892" type="appl" />
         <tli id="T424" time="349.368" type="appl" />
         <tli id="T425" time="349.844" type="appl" />
         <tli id="T426" time="350.321" type="appl" />
         <tli id="T427" time="350.797" type="appl" />
         <tli id="T428" time="351.398" type="appl" />
         <tli id="T429" time="352.0" type="appl" />
         <tli id="T430" time="352.67766397828024" />
         <tli id="T431" time="353.578" type="appl" />
         <tli id="T432" time="354.6043178834203" />
         <tli id="T433" time="355.643" type="appl" />
         <tli id="T434" time="356.585" type="appl" />
         <tli id="T435" time="357.528" type="appl" />
         <tli id="T436" time="358.47" type="appl" />
         <tli id="T437" time="359.43761920254343" />
         <tli id="T438" time="360.244" type="appl" />
         <tli id="T439" time="360.946" type="appl" />
         <tli id="T440" time="361.647" type="appl" />
         <tli id="T441" time="362.349" type="appl" />
         <tli id="T442" time="363.051" type="appl" />
         <tli id="T443" time="363.753" type="appl" />
         <tli id="T444" time="364.455" type="appl" />
         <tli id="T445" time="365.156" type="appl" />
         <tli id="T446" time="365.858" type="appl" />
         <tli id="T447" time="367.63756488878" />
         <tli id="T448" time="368.46" type="appl" />
         <tli id="T449" time="369.183" type="appl" />
         <tli id="T450" time="369.907" type="appl" />
         <tli id="T451" time="370.63" type="appl" />
         <tli id="T452" time="371.353" type="appl" />
         <tli id="T453" time="372.076" type="appl" />
         <tli id="T454" time="372.8" type="appl" />
         <tli id="T455" time="373.523" type="appl" />
         <tli id="T456" time="376.550839183494" />
         <tli id="T457" time="377.198" type="appl" />
         <tli id="T458" time="377.739" type="appl" />
         <tli id="T459" time="379.02" type="appl" />
         <tli id="T460" time="380.009" type="appl" />
         <tli id="T461" time="380.997" type="appl" />
         <tli id="T462" time="382.038" type="appl" />
         <tli id="T463" time="383.078" type="appl" />
         <tli id="T464" time="384.118" type="appl" />
         <tli id="T465" time="385.159" type="appl" />
         <tli id="T466" time="385.964" type="appl" />
         <tli id="T467" time="386.768" type="appl" />
         <tli id="T468" time="387.573" type="appl" />
         <tli id="T469" time="388.039" type="appl" />
         <tli id="T470" time="388.2" type="appl" />
         <tli id="T471" time="388.36" type="appl" />
         <tli id="T472" time="388.521" type="appl" />
         <tli id="T473" time="388.682" type="appl" />
         <tli id="T474" time="388.843" type="appl" />
         <tli id="T475" time="389.004" type="appl" />
         <tli id="T476" time="389.164" type="appl" />
         <tli id="T477" time="389.325" type="appl" />
         <tli id="T478" time="398.23217747270485" />
         <tli id="T479" time="399.081" type="appl" />
         <tli id="T480" time="399.966" type="appl" />
         <tli id="T481" time="400.851" type="appl" />
         <tli id="T482" time="401.736" type="appl" />
         <tli id="T483" time="402.621" type="appl" />
         <tli id="T484" time="403.365" type="appl" />
         <tli id="T485" time="404.11" type="appl" />
         <tli id="T486" time="405.6906461709248" />
         <tli id="T487" time="406.283" type="appl" />
         <tli id="T488" time="407.3973015332635" />
         <tli id="T489" time="408.228" type="appl" />
         <tli id="T490" time="408.792" type="appl" />
         <tli id="T491" time="409.356" type="appl" />
         <tli id="T492" time="409.919" type="appl" />
         <tli id="T493" time="410.483" type="appl" />
         <tli id="T494" time="411.047" type="appl" />
         <tli id="T495" time="411.611" type="appl" />
         <tli id="T496" time="412.175" type="appl" />
         <tli id="T497" time="413.158" type="appl" />
         <tli id="T498" time="414.23058960512725" />
         <tli id="T499" time="415.703" type="appl" />
         <tli id="T500" time="418.5905607261018" />
         <tli id="T501" time="419.774" type="appl" />
         <tli id="T502" time="420.958" type="appl" />
         <tli id="T503" time="422.6838669467109" />
         <tli id="T504" time="423.984" type="appl" />
         <tli id="T505" time="429.48382190602894" />
         <tli id="T506" time="430.548" type="appl" />
         <tli id="T507" time="431.519" type="appl" />
         <tli id="T508" time="432.146" type="appl" />
         <tli id="T509" time="432.718" type="appl" />
         <tli id="T510" time="433.29" type="appl" />
         <tli id="T511" time="433.862" type="appl" />
         <tli id="T512" time="434.434" type="appl" />
         <tli id="T513" time="435.29711673399504" />
         <tli id="T514" time="437.1371045465164" />
         <tli id="T515" time="437.517" type="appl" />
         <tli id="T516" time="438.12" type="appl" />
         <tli id="T517" time="438.723" type="appl" />
         <tli id="T518" time="439.326" type="appl" />
         <tli id="T519" time="439.929" type="appl" />
         <tli id="T520" time="440.8637465291231" />
         <tli id="T521" time="442.001" type="appl" />
         <tli id="T522" time="443.23039751986613" />
         <tli id="T523" time="444.03" type="appl" />
         <tli id="T524" time="444.832" type="appl" />
         <tli id="T525" time="445.971" type="appl" />
         <tli id="T526" time="446.941" type="appl" />
         <tli id="T527" time="448.8970266526312" />
         <tli id="T528" time="449.695" type="appl" />
         <tli id="T529" time="450.376" type="appl" />
         <tli id="T530" time="451.056" type="appl" />
         <tli id="T531" time="451.981" type="appl" />
         <tli id="T532" time="452.709" type="appl" />
         <tli id="T533" time="453.5903288990625" />
         <tli id="T534" time="454.736" type="appl" />
         <tli id="T535" time="455.93031339976903" />
         <tli id="T536" time="456.553" type="appl" />
         <tli id="T537" time="457.09" type="appl" />
         <tli id="T538" time="457.627" type="appl" />
         <tli id="T539" time="458.164" type="appl" />
         <tli id="T540" time="458.701" type="appl" />
         <tli id="T541" time="459.4836231971382" />
         <tli id="T542" time="459.905" type="appl" />
         <tli id="T543" time="460.410283725908" />
         <tli id="T544" time="461.214" type="appl" />
         <tli id="T545" time="461.927" type="appl" />
         <tli id="T546" time="462.64" type="appl" />
         <tli id="T547" time="463.57026279523825" />
         <tli id="T548" time="464.467" type="appl" />
         <tli id="T549" time="465.152" type="appl" />
         <tli id="T550" time="465.838" type="appl" />
         <tli id="T551" time="466.523" type="appl" />
         <tli id="T552" time="467.209" type="appl" />
         <tli id="T553" time="467.894" type="appl" />
         <tli id="T554" time="470.3835509995746" />
         <tli id="T555" time="471.389" type="appl" />
         <tli id="T556" time="472.071" type="appl" />
         <tli id="T557" time="474.0635266246173" />
         <tli id="T558" time="474.524" type="appl" />
         <tli id="T559" time="475.1835192061521" />
         <tli id="T560" time="476.298" type="appl" />
         <tli id="T561" time="476.686" type="appl" />
         <tli id="T562" time="477.075" type="appl" />
         <tli id="T563" time="477.463" type="appl" />
         <tli id="T564" time="477.852" type="appl" />
         <tli id="T565" time="478.276" type="appl" />
         <tli id="T566" time="479.1101598640328" />
         <tli id="T567" time="480.13" type="appl" />
         <tli id="T568" time="481.2568123119744" />
         <tli id="T569" time="483.27" type="appl" />
         <tli id="T570" time="484.989" type="appl" />
         <tli id="T571" time="485.62" type="appl" />
         <tli id="T572" time="486.312" type="appl" />
         <tli id="T573" time="487.6634365432534" />
         <tli id="T574" time="488.836" type="appl" />
         <tli id="T575" time="490.103420381597" />
         <tli id="T576" time="491.088" type="appl" />
         <tli id="T577" time="491.533" type="appl" />
         <tli id="T578" time="492.249" type="appl" />
         <tli id="T579" time="494.383392032462" />
         <tli id="T580" time="494.952" type="appl" />
         <tli id="T581" time="495.366" type="appl" />
         <tli id="T582" time="495.49671799148757" />
         <tli id="T583" time="496.09" type="appl" />
         <tli id="T584" time="496.921" type="appl" />
         <tli id="T585" time="497.814" type="appl" />
         <tli id="T586" time="500.0100214301722" />
         <tli id="T587" time="501.0" type="appl" />
         <tli id="T588" time="501.954" type="appl" />
         <tli id="T984" time="502.361" type="intp" />
         <tli id="T589" time="502.768" type="appl" />
         <tli id="T590" time="507.98330195109816" />
         <tli id="T591" time="508.708" type="appl" />
         <tli id="T592" time="509.139" type="appl" />
         <tli id="T593" time="510.056" type="appl" />
         <tli id="T594" time="511.076614795337" />
         <tli id="T595" time="511.812" type="appl" />
         <tli id="T596" time="513.7299305539728" />
         <tli id="T597" time="514.808" type="appl" />
         <tli id="T598" time="516.6099114779194" />
         <tli id="T599" time="517.838" type="appl" />
         <tli id="T600" time="518.826" type="appl" />
         <tli id="T601" time="520.048" type="appl" />
         <tli id="T602" time="521.257" type="appl" />
         <tli id="T603" time="522.414" type="appl" />
         <tli id="T604" time="523.338" type="appl" />
         <tli id="T605" time="526.0298490833277" />
         <tli id="T606" time="527.5431723929014" />
         <tli id="T607" time="529.108" type="appl" />
         <tli id="T608" time="530.454" type="appl" />
         <tli id="T985" time="530.673" />
         <tli id="T609" time="531.8" type="appl" />
         <tli id="T610" time="532.992" type="intp" />
         <tli id="T611" time="534.184" type="appl" />
         <tli id="T612" time="534.8031243053498" />
         <tli id="T613" time="536.035" type="appl" />
         <tli id="T614" time="537.4297735739491" />
         <tli id="T615" time="538.109" type="appl" />
         <tli id="T616" time="538.572" type="appl" />
         <tli id="T617" time="539.036" type="appl" />
         <tli id="T618" time="539.7164250945826" />
         <tli id="T619" time="540.606" type="appl" />
         <tli id="T620" time="541.323" type="appl" />
         <tli id="T621" time="542.0964093303439" />
         <tli id="T622" time="542.917" type="appl" />
         <tli id="T623" time="543.79" type="appl" />
         <tli id="T624" time="544.404" type="appl" />
         <tli id="T625" time="545.015" type="appl" />
         <tli id="T626" time="547.1497091922686" />
         <tli id="T627" time="547.833" type="appl" />
         <tli id="T628" time="548.372" type="appl" />
         <tli id="T629" time="548.912" type="appl" />
         <tli id="T630" time="549.451" type="appl" />
         <tli id="T631" time="549.991" type="appl" />
         <tli id="T632" time="550.53" type="appl" />
         <tli id="T633" time="552.9163376626707" />
         <tli id="T634" time="553.665" type="appl" />
         <tli id="T635" time="554.393" type="appl" />
         <tli id="T636" time="555.121" type="appl" />
         <tli id="T637" time="555.849" type="appl" />
         <tli id="T638" time="556.577" type="appl" />
         <tli id="T639" time="557.489" type="appl" />
         <tli id="T640" time="558.257" type="appl" />
         <tli id="T641" time="559.026" type="appl" />
         <tli id="T642" time="562.6096067909535" />
         <tli id="T643" time="563.322" type="appl" />
         <tli id="T644" time="563.999" type="appl" />
         <tli id="T645" time="564.7495926163859" />
         <tli id="T646" time="565.342922019699" />
         <tli id="T647" time="565.818" type="appl" />
         <tli id="T648" time="566.819" type="appl" />
         <tli id="T649" time="567.819" type="appl" />
         <tli id="T650" time="568.8962318170682" />
         <tli id="T651" time="569.345" type="appl" />
         <tli id="T652" time="569.87" type="appl" />
         <tli id="T653" time="570.395" type="appl" />
         <tli id="T654" time="573.7761994937554" />
         <tli id="T655" time="574.621" type="appl" />
         <tli id="T656" time="575.496" type="appl" />
         <tli id="T657" time="576.37" type="appl" />
         <tli id="T658" time="577.9028388269102" />
         <tli id="T659" time="578.744" type="appl" />
         <tli id="T660" time="579.376" type="appl" />
         <tli id="T661" time="580.008" type="appl" />
         <tli id="T662" time="580.6266749517755" />
         <tli id="T663" time="581.447" type="appl" />
         <tli id="T664" time="582.254" type="appl" />
         <tli id="T665" time="582.945" type="appl" />
         <tli id="T666" time="583.636" type="appl" />
         <tli id="T667" time="584.5227949784814" />
         <tli id="T668" time="585.159" type="appl" />
         <tli id="T669" time="585.682" type="appl" />
         <tli id="T670" time="586.1773152694792" />
         <tli id="T671" time="587.433" type="appl" />
         <tli id="T672" time="588.662" type="appl" />
         <tli id="T673" time="589.383" type="appl" />
         <tli id="T674" time="590.2360904688105" />
         <tli id="T675" time="591.046" type="appl" />
         <tli id="T676" time="591.692" type="appl" />
         <tli id="T677" time="594.7293940399678" />
         <tli id="T678" time="595.734" type="appl" />
         <tli id="T679" time="596.317" type="appl" />
         <tli id="T680" time="596.899" type="appl" />
         <tli id="T681" time="597.482" type="appl" />
         <tli id="T682" time="598.064" type="appl" />
         <tli id="T683" time="598.716" type="appl" />
         <tli id="T684" time="599.359" type="appl" />
         <tli id="T685" time="600.0286818558818" />
         <tli id="T686" time="600.709" type="appl" />
         <tli id="T687" time="601.417" type="appl" />
         <tli id="T688" time="602.5093425081288" />
         <tli id="T689" time="603.558" type="appl" />
         <tli id="T690" time="604.261" type="appl" />
         <tli id="T691" time="605.2093246243285" />
         <tli id="T692" time="606.199" type="appl" />
         <tli id="T693" time="606.976" type="appl" />
         <tli id="T694" time="607.753" type="appl" />
         <tli id="T695" time="608.632" type="appl" />
         <tli id="T696" time="609.51" type="appl" />
         <tli id="T697" time="610.44" type="appl" />
         <tli id="T698" time="611.359" type="appl" />
         <tli id="T699" time="614.675928587301" />
         <tli id="T700" time="615.613" type="appl" />
         <tli id="T701" time="616.348" type="appl" />
         <tli id="T702" time="617.083" type="appl" />
         <tli id="T703" time="618.4159038149259" />
         <tli id="T704" time="619.148" type="appl" />
         <tli id="T705" time="619.74" type="appl" />
         <tli id="T706" time="620.333" type="appl" />
         <tli id="T707" time="620.892" type="appl" />
         <tli id="T708" time="621.451" type="appl" />
         <tli id="T709" time="622.01" type="appl" />
         <tli id="T710" time="623.578" type="appl" />
         <tli id="T711" time="624.171" type="appl" />
         <tli id="T712" time="624.765" type="appl" />
         <tli id="T713" time="625.358" type="appl" />
         <tli id="T714" time="625.951" type="appl" />
         <tli id="T715" time="626.544" type="appl" />
         <tli id="T716" time="627.138" type="appl" />
         <tli id="T717" time="627.731" type="appl" />
         <tli id="T718" time="630.0224936033305" />
         <tli id="T719" time="630.786" type="appl" />
         <tli id="T720" time="631.54" type="appl" />
         <tli id="T721" time="632.294" type="appl" />
         <tli id="T722" time="635.169126180383" />
         <tli id="T723" time="636.142" type="appl" />
         <tli id="T724" time="636.963" type="appl" />
         <tli id="T725" time="637.785" type="appl" />
         <tli id="T726" time="638.598" type="appl" />
         <tli id="T727" time="640.0824269696158" />
         <tli id="T728" time="641.14" type="appl" />
         <tli id="T729" time="642.094" type="appl" />
         <tli id="T730" time="642.776" type="appl" />
         <tli id="T731" time="645.3557253743419" />
         <tli id="T732" time="646.328" type="appl" />
         <tli id="T733" time="647.178" type="appl" />
         <tli id="T734" time="648.085" type="appl" />
         <tli id="T735" time="648.993" type="appl" />
         <tli id="T736" time="651.7356831155844" />
         <tli id="T737" time="652.956" type="appl" />
         <tli id="T738" time="654.024" type="appl" />
         <tli id="T739" time="655.091" type="appl" />
         <tli id="T740" time="656.7089835073995" />
         <tli id="T741" time="657.798" type="appl" />
         <tli id="T742" time="658.8823024453777" />
         <tli id="T743" time="659.612" type="appl" />
         <tli id="T744" time="660.201" type="appl" />
         <tli id="T745" time="660.791" type="appl" />
         <tli id="T746" time="661.38" type="appl" />
         <tli id="T747" time="662.4489454877651" />
         <tli id="T748" time="663.7489368770465" />
         <tli id="T749" time="664.295" type="appl" />
         <tli id="T750" time="665.046" type="appl" />
         <tli id="T751" time="665.798" type="appl" />
         <tli id="T752" time="666.598" type="appl" />
         <tli id="T753" time="667.391" type="appl" />
         <tli id="T754" time="668.183" type="appl" />
         <tli id="T755" time="668.975" type="appl" />
         <tli id="T756" time="669.768" type="appl" />
         <tli id="T757" time="670.5866520029201" />
         <tli id="T758" time="671.227" type="appl" />
         <tli id="T759" time="671.893" type="appl" />
         <tli id="T760" time="672.56" type="appl" />
         <tli id="T761" time="673.407" type="appl" />
         <tli id="T762" time="674.253" type="appl" />
         <tli id="T763" time="675.3421934204329" />
         <tli id="T764" time="676.293" type="appl" />
         <tli id="T765" time="677.305" type="appl" />
         <tli id="T766" time="678.317" type="appl" />
         <tli id="T767" time="680.281" type="appl" />
         <tli id="T768" time="680.488" type="appl" />
         <tli id="T769" time="680.695" type="appl" />
         <tli id="T770" time="681.5488189764379" />
         <tli id="T771" time="682.1732592847936" />
         <tli id="T772" time="682.7976995931492" />
         <tli id="T773" time="683.4221399015049" />
         <tli id="T774" time="684.374" type="appl" />
         <tli id="T775" time="687.4287800294953" />
         <tli id="T776" time="688.286" type="appl" />
         <tli id="T777" time="688.966" type="appl" />
         <tli id="T778" time="689.645" type="appl" />
         <tli id="T779" time="690.325" type="appl" />
         <tli id="T780" time="690.92" type="appl" />
         <tli id="T781" time="691.6487520777781" />
         <tli id="T782" time="692.484" type="appl" />
         <tli id="T783" time="693.368740685135" />
         <tli id="T784" time="693.855" type="appl" />
         <tli id="T785" time="694.43" type="appl" />
         <tli id="T786" time="695.049" type="appl" />
         <tli id="T787" time="695.668" type="appl" />
         <tli id="T788" time="696.287" type="appl" />
         <tli id="T789" time="698.7220385599707" />
         <tli id="T790" time="699.488" type="appl" />
         <tli id="T791" time="700.231" type="appl" />
         <tli id="T792" time="701.1020227957321" />
         <tli id="T793" time="701.784" type="appl" />
         <tli id="T794" time="702.409" type="appl" />
         <tli id="T795" time="703.034" type="appl" />
         <tli id="T796" time="703.659" type="appl" />
         <tli id="T797" time="704.283" type="appl" />
         <tli id="T798" time="704.908" type="appl" />
         <tli id="T799" time="705.533" type="appl" />
         <tli id="T800" time="706.158" type="appl" />
         <tli id="T801" time="707.308648351737" />
         <tli id="T802" time="708.41" type="appl" />
         <tli id="T803" time="711.0486235793619" />
         <tli id="T804" time="711.803" type="appl" />
         <tli id="T805" time="712.486" type="appl" />
         <tli id="T806" time="713.17" type="appl" />
         <tli id="T807" time="713.854" type="appl" />
         <tli id="T808" time="714.453" type="appl" />
         <tli id="T809" time="715.052" type="appl" />
         <tli id="T810" time="715.651" type="appl" />
         <tli id="T811" time="716.8219186722733" />
         <tli id="T812" time="717.301" type="appl" />
         <tli id="T813" time="717.866" type="appl" />
         <tli id="T814" time="719.6618998611649" />
         <tli id="T815" time="720.676" type="appl" />
         <tli id="T816" time="721.484" type="appl" />
         <tli id="T817" time="722.2" type="appl" />
         <tli id="T818" time="722.91" type="appl" />
         <tli id="T819" time="723.8285389293745" />
         <tli id="T820" time="724.478" type="appl" />
         <tli id="T821" time="725.051" type="appl" />
         <tli id="T822" time="725.624" type="appl" />
         <tli id="T823" time="726.198" type="appl" />
         <tli id="T824" time="726.771" type="appl" />
         <tli id="T825" time="727.7085132296913" />
         <tli id="T826" time="728.758" type="appl" />
         <tli id="T827" time="730.7884928289119" />
         <tli id="T828" time="731.736" type="appl" />
         <tli id="T829" time="733.7151401104222" />
         <tli id="T830" time="735.5017949428707" />
         <tli id="T831" time="736.277" type="appl" />
         <tli id="T832" time="736.948" type="appl" />
         <tli id="T833" time="737.619" type="appl" />
         <tli id="T834" time="738.2700057738124" />
         <tli id="T835" time="738.904" type="appl" />
         <tli id="T836" time="739.518" type="appl" />
         <tli id="T837" time="740.132" type="appl" />
         <tli id="T838" time="740.8817593077429" />
         <tli id="T839" time="741.598" type="appl" />
         <tli id="T840" time="742.225" type="appl" />
         <tli id="T841" time="742.853" type="appl" />
         <tli id="T842" time="743.48" type="appl" />
         <tli id="T843" time="744.108" type="appl" />
         <tli id="T844" time="744.735" type="appl" />
         <tli id="T845" time="745.363" type="appl" />
         <tli id="T846" time="746.243" type="appl" />
         <tli id="T847" time="747.008" type="appl" />
         <tli id="T848" time="748.4950422131755" />
         <tli id="T849" time="749.107" type="appl" />
         <tli id="T850" time="749.669" type="appl" />
         <tli id="T851" time="750.23" type="appl" />
         <tli id="T852" time="750.792" type="appl" />
         <tli id="T853" time="751.354" type="appl" />
         <tli id="T854" time="752.508348963675" />
         <tli id="T855" time="753.912" type="appl" />
         <tli id="T856" time="754.655" type="appl" />
         <tli id="T857" time="755.398" type="appl" />
         <tli id="T858" time="756.14" type="appl" />
         <tli id="T859" time="757.164" type="appl" />
         <tli id="T860" time="759.4483029956849" />
         <tli id="T861" time="760.19" type="appl" />
         <tli id="T862" time="760.705" type="appl" />
         <tli id="T863" time="761.219" type="appl" />
         <tli id="T864" time="761.734" type="appl" />
         <tli id="T865" time="762.248" type="appl" />
         <tli id="T866" time="762.763" type="appl" />
         <tli id="T867" time="763.4016101436022" />
         <tli id="T868" time="764.135" type="appl" />
         <tli id="T869" time="764.756" type="appl" />
         <tli id="T870" time="765.376" type="appl" />
         <tli id="T871" time="769.2415714616047" />
         <tli id="T872" time="770.9015604663795" />
         <tli id="T873" time="771.663" type="appl" />
         <tli id="T874" time="772.372" type="appl" />
         <tli id="T875" time="773.081" type="appl" />
         <tli id="T876" time="773.79" type="appl" />
         <tli id="T877" time="774.414" type="appl" />
         <tli id="T878" time="775.037" type="appl" />
         <tli id="T879" time="775.661" type="appl" />
         <tli id="T880" time="776.4548570164893" />
         <tli id="T881" time="777.317" type="appl" />
         <tli id="T882" time="778.101" type="appl" />
         <tli id="T883" time="785.2481321060388" />
         <tli id="T884" time="786.026" type="appl" />
         <tli id="T885" time="787.3681180639439" />
         <tli id="T886" time="788.517" type="appl" />
         <tli id="T887" time="789.434" type="appl" />
         <tli id="T888" time="790.352" type="appl" />
         <tli id="T889" time="791.108" type="appl" />
         <tli id="T890" time="791.864" type="appl" />
         <tli id="T891" time="792.7614156738343" />
         <tli id="T892" time="793.563" type="appl" />
         <tli id="T893" time="794.177" type="appl" />
         <tli id="T894" time="795.2213993797054" />
         <tli id="T895" time="796.264" type="appl" />
         <tli id="T896" time="796.867" type="appl" />
         <tli id="T897" time="797.594" type="appl" />
         <tli id="T898" time="798.7480426870379" />
         <tli id="T899" time="799.741" type="appl" />
         <tli id="T900" time="800.371" type="appl" />
         <tli id="T901" time="801.001" type="appl" />
         <tli id="T902" time="801.631" type="appl" />
         <tli id="T903" time="802.486" type="appl" />
         <tli id="T904" time="803.278" type="appl" />
         <tli id="T905" time="804.014" type="appl" />
         <tli id="T906" time="804.8680021504243" />
         <tli id="T907" time="805.836" type="appl" />
         <tli id="T908" time="807.0546543334207" />
         <tli id="T909" time="809.0613077086704" />
         <tli id="T910" time="809.839" type="appl" />
         <tli id="T911" time="810.506" type="appl" />
         <tli id="T912" time="811.174" type="appl" />
         <tli id="T913" time="812.7612832012405" />
         <tli id="T914" time="813.686" type="appl" />
         <tli id="T915" time="814.234" type="appl" />
         <tli id="T916" time="814.783" type="appl" />
         <tli id="T917" time="815.331" type="appl" />
         <tli id="T918" time="815.88" type="appl" />
         <tli id="T919" time="816.428" type="appl" />
         <tli id="T920" time="816.9836510669875" />
         <tli id="T921" time="817.776" type="appl" />
         <tli id="T922" time="818.5426511573465" />
         <tli id="T923" time="819.003" type="appl" />
         <tli id="T924" time="819.429" type="appl" />
         <tli id="T925" time="819.856" type="appl" />
         <tli id="T926" time="820.282" type="appl" />
         <tli id="T927" time="820.7945633247487" />
         <tli id="T928" time="821.541" type="appl" />
         <tli id="T929" time="822.148" type="appl" />
         <tli id="T930" time="822.756" type="appl" />
         <tli id="T931" time="823.5945447785856" />
         <tli id="T932" time="824.249" type="appl" />
         <tli id="T933" time="825.045" type="appl" />
         <tli id="T934" time="829.0145088785126" />
         <tli id="T935" time="829.872" type="appl" />
         <tli id="T936" time="830.553" type="appl" />
         <tli id="T937" time="831.234" type="appl" />
         <tli id="T938" time="834.4078064884033" />
         <tli id="T939" time="835.483" type="appl" />
         <tli id="T940" time="836.39" type="appl" />
         <tli id="T941" time="838.141115093519" />
         <tli id="T942" time="839.136" type="appl" />
         <tli id="T943" time="839.7744376082571" />
         <tli id="T944" time="840.36" type="appl" />
         <tli id="T945" time="840.881" type="appl" />
         <tli id="T946" time="841.401" type="appl" />
         <tli id="T947" time="841.921" type="appl" />
         <tli id="T948" time="842.442" type="appl" />
         <tli id="T949" time="842.962" type="appl" />
         <tli id="T950" time="843.876" type="appl" />
         <tli id="T951" time="845.0010696554193" />
         <tli id="T952" time="846.358" type="appl" />
         <tli id="T953" time="848.9943765383914" />
         <tli id="T954" time="850.235" type="appl" />
         <tli id="T955" time="851.099" type="appl" />
         <tli id="T956" time="852.0676895151028" />
         <tli id="T957" time="852.99" type="appl" />
         <tli id="T958" time="853.777" type="appl" />
         <tli id="T959" time="854.564" type="appl" />
         <tli id="T960" time="855.35" type="appl" />
         <tli id="T961" time="856.136" type="appl" />
         <tli id="T962" time="856.923" type="appl" />
         <tli id="T963" time="857.709" type="appl" />
         <tli id="T964" time="858.496" type="appl" />
         <tli id="T965" time="859.282" type="appl" />
         <tli id="T966" time="861.0276301673808" />
         <tli id="T967" time="862.059" type="appl" />
         <tli id="T968" time="862.649" type="appl" />
         <tli id="T969" time="864.4476075145674" />
         <tli id="T970" time="865.328" type="appl" />
         <tli id="T971" time="865.949" type="appl" />
         <tli id="T972" time="866.57" type="appl" />
         <tli id="T973" time="867.192" type="appl" />
         <tli id="T974" time="867.814" type="appl" />
         <tli id="T975" time="869.0675769133979" />
         <tli id="T976" time="870.314" type="appl" />
         <tli id="T977" time="871.466" type="appl" />
         <tli id="T978" time="872.618" type="appl" />
         <tli id="T979" time="873.769" type="appl" />
         <tli id="T980" time="874.921" type="appl" />
         <tli id="T981" time="876.073" type="appl" />
         <tli id="T982" time="877.225" type="appl" />
         <tli id="T983" time="877.534" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-KA"
                      id="tx-KA"
                      speaker="KA"
                      type="t">
         <timeline-fork end="T4" start="T0">
            <tli id="T0.tx-KA.1" />
            <tli id="T0.tx-KA.2" />
            <tli id="T0.tx-KA.3" />
         </timeline-fork>
         <timeline-fork end="T90" start="T88">
            <tli id="T88.tx-KA.1" />
         </timeline-fork>
         <timeline-fork end="T408" start="T402">
            <tli id="T402.tx-KA.1" />
            <tli id="T402.tx-KA.2" />
            <tli id="T402.tx-KA.3" />
            <tli id="T402.tx-KA.4" />
         </timeline-fork>
         <timeline-fork end="T573" start="T571">
            <tli id="T571.tx-KA.1" />
         </timeline-fork>
         <timeline-fork end="T577" start="T575">
            <tli id="T575.tx-KA.1" />
         </timeline-fork>
         <timeline-fork end="T582" start="T579">
            <tli id="T579.tx-KA.1" />
            <tli id="T579.tx-KA.2" />
         </timeline-fork>
         <timeline-fork end="T586" start="T584">
            <tli id="T584.tx-KA.1" />
         </timeline-fork>
         <timeline-fork end="T592" start="T590">
            <tli id="T590.tx-KA.1" />
         </timeline-fork>
         <timeline-fork end="T612" start="T610">
            <tli id="T610.tx-KA.1" />
         </timeline-fork>
         <timeline-fork end="T646" start="T642">
            <tli id="T642.tx-KA.1" />
            <tli id="T642.tx-KA.2" />
            <tli id="T642.tx-KA.3" />
         </timeline-fork>
         <timeline-fork end="T770" start="T766">
            <tli id="T766.tx-KA.1" />
            <tli id="T766.tx-KA.2" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KA">
            <ts e="T4" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T0.tx-KA.1" id="Seg_4" n="HIAT:w" s="T0">Klaudia</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-KA.2" id="Seg_7" n="HIAT:w" s="T0.tx-KA.1">Plotnikova</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-KA.3" id="Seg_11" n="HIAT:w" s="T0.tx-KA.2">teine</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T0.tx-KA.3">lint</ts>
                  <nts id="Seg_15" n="HIAT:ip">.</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T14" id="Seg_17" n="sc" s="T13">
               <ts e="T14" id="Seg_19" n="HIAT:u" s="T13">
                  <nts id="Seg_20" n="HIAT:ip">(</nts>
                  <nts id="Seg_21" n="HIAT:ip">(</nts>
                  <ats e="T14" id="Seg_22" n="HIAT:non-pho" s="T13">DMG</ats>
                  <nts id="Seg_23" n="HIAT:ip">)</nts>
                  <nts id="Seg_24" n="HIAT:ip">)</nts>
                  <nts id="Seg_25" n="HIAT:ip">.</nts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T48" id="Seg_27" n="sc" s="T42">
               <ts e="T48" id="Seg_29" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_31" n="HIAT:w" s="T42">Ну</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_34" n="HIAT:w" s="T43">что</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_37" n="HIAT:w" s="T44">ж</ts>
                  <nts id="Seg_38" n="HIAT:ip">,</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_41" n="HIAT:w" s="T45">можешь</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_44" n="HIAT:w" s="T46">успокоиться</ts>
                  <nts id="Seg_45" n="HIAT:ip">,</nts>
                  <nts id="Seg_46" n="HIAT:ip">…</nts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T85" id="Seg_48" n="sc" s="T74">
               <ts e="T85" id="Seg_50" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_52" n="HIAT:w" s="T74">Ну</ts>
                  <nts id="Seg_53" n="HIAT:ip">,</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_56" n="HIAT:w" s="T75">там</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_59" n="HIAT:w" s="T76">коня</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_62" n="HIAT:w" s="T77">еще</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_65" n="HIAT:w" s="T78">запрячь</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_68" n="HIAT:w" s="T79">и</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_71" n="HIAT:w" s="T80">на</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_74" n="HIAT:w" s="T81">коне</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_77" n="HIAT:w" s="T82">поехать</ts>
                  <nts id="Seg_78" n="HIAT:ip">,</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_81" n="HIAT:w" s="T83">как</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_84" n="HIAT:w" s="T84">это</ts>
                  <nts id="Seg_85" n="HIAT:ip">?</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T90" id="Seg_87" n="sc" s="T86">
               <ts e="T88" id="Seg_89" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_91" n="HIAT:w" s="T86">Ага</ts>
                  <nts id="Seg_92" n="HIAT:ip">,</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_95" n="HIAT:w" s="T87">ага</ts>
                  <nts id="Seg_96" n="HIAT:ip">.</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_99" n="HIAT:u" s="T88">
                  <nts id="Seg_100" n="HIAT:ip">(</nts>
                  <nts id="Seg_101" n="HIAT:ip">(</nts>
                  <ats e="T88.tx-KA.1" id="Seg_102" n="HIAT:non-pho" s="T88">DMG</ats>
                  <nts id="Seg_103" n="HIAT:ip">)</nts>
                  <nts id="Seg_104" n="HIAT:ip">)</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_107" n="HIAT:w" s="T88.tx-KA.1">сейчас</ts>
                  <nts id="Seg_108" n="HIAT:ip">.</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T408" id="Seg_110" n="sc" s="T402">
               <ts e="T408" id="Seg_112" n="HIAT:u" s="T402">
                  <ts e="T402.tx-KA.1" id="Seg_114" n="HIAT:w" s="T402">Вот</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402.tx-KA.2" id="Seg_117" n="HIAT:w" s="T402.tx-KA.1">еще</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402.tx-KA.3" id="Seg_120" n="HIAT:w" s="T402.tx-KA.2">сахар</ts>
                  <nts id="Seg_121" n="HIAT:ip">,</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402.tx-KA.4" id="Seg_124" n="HIAT:w" s="T402.tx-KA.3">кажется</ts>
                  <nts id="Seg_125" n="HIAT:ip">,</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_128" n="HIAT:w" s="T402.tx-KA.4">будешь</ts>
                  <nts id="Seg_129" n="HIAT:ip">…</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T573" id="Seg_131" n="sc" s="T571">
               <ts e="T573" id="Seg_133" n="HIAT:u" s="T571">
                  <ts e="T571.tx-KA.1" id="Seg_135" n="HIAT:w" s="T571">Не</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_138" n="HIAT:w" s="T571.tx-KA.1">разговаривай</ts>
                  <nts id="Seg_139" n="HIAT:ip">.</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T577" id="Seg_141" n="sc" s="T575">
               <ts e="T577" id="Seg_143" n="HIAT:u" s="T575">
                  <ts e="T575.tx-KA.1" id="Seg_145" n="HIAT:w" s="T575">Не</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_148" n="HIAT:w" s="T575.tx-KA.1">ругайся</ts>
                  <nts id="Seg_149" n="HIAT:ip">.</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T582" id="Seg_151" n="sc" s="T579">
               <ts e="T582" id="Seg_153" n="HIAT:u" s="T579">
                  <ts e="T579.tx-KA.1" id="Seg_155" n="HIAT:w" s="T579">Не</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579.tx-KA.2" id="Seg_158" n="HIAT:w" s="T579.tx-KA.1">кричи</ts>
                  <nts id="Seg_159" n="HIAT:ip">,</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_162" n="HIAT:w" s="T579.tx-KA.2">кажется</ts>
                  <nts id="Seg_163" n="HIAT:ip">.</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T586" id="Seg_165" n="sc" s="T584">
               <ts e="T586" id="Seg_167" n="HIAT:u" s="T584">
                  <ts e="T584.tx-KA.1" id="Seg_169" n="HIAT:w" s="T584">Сопли</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_172" n="HIAT:w" s="T584.tx-KA.1">плывут</ts>
                  <nts id="Seg_173" n="HIAT:ip">.</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T592" id="Seg_175" n="sc" s="T590">
               <ts e="T592" id="Seg_177" n="HIAT:u" s="T590">
                  <ts e="T590.tx-KA.1" id="Seg_179" n="HIAT:w" s="T590">Не</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_182" n="HIAT:w" s="T590.tx-KA.1">беги</ts>
                  <nts id="Seg_183" n="HIAT:ip">.</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T606" id="Seg_185" n="sc" s="T605">
               <ts e="T606" id="Seg_187" n="HIAT:u" s="T605">
                  <ts e="T606" id="Seg_189" n="HIAT:w" s="T605">Большой</ts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T612" id="Seg_192" n="sc" s="T609">
               <ts e="T610" id="Seg_194" n="HIAT:u" s="T609">
                  <ts e="T610" id="Seg_196" n="HIAT:w" s="T609">Ага</ts>
                  <nts id="Seg_197" n="HIAT:ip">.</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T612" id="Seg_200" n="HIAT:u" s="T610">
                  <ts e="T610.tx-KA.1" id="Seg_202" n="HIAT:w" s="T610">Маленький</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_205" n="HIAT:w" s="T610.tx-KA.1">еще</ts>
                  <nts id="Seg_206" n="HIAT:ip">.</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T646" id="Seg_208" n="sc" s="T642">
               <ts e="T646" id="Seg_210" n="HIAT:u" s="T642">
                  <ts e="T642.tx-KA.1" id="Seg_212" n="HIAT:w" s="T642">А</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642.tx-KA.2" id="Seg_215" n="HIAT:w" s="T642.tx-KA.1">зачем</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642.tx-KA.3" id="Seg_218" n="HIAT:w" s="T642.tx-KA.2">ножом</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_221" n="HIAT:w" s="T642.tx-KA.3">баловался</ts>
                  <nts id="Seg_222" n="HIAT:ip">?</nts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T770" id="Seg_224" n="sc" s="T766">
               <ts e="T770" id="Seg_226" n="HIAT:u" s="T766">
                  <ts e="T766.tx-KA.1" id="Seg_228" n="HIAT:w" s="T766">Маленькая</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766.tx-KA.2" id="Seg_231" n="HIAT:w" s="T766.tx-KA.1">чашка</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_234" n="HIAT:w" s="T766.tx-KA.2">чая</ts>
                  <nts id="Seg_235" n="HIAT:ip">…</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KA">
            <ts e="T4" id="Seg_237" n="sc" s="T0">
               <ts e="T4" id="Seg_239" n="e" s="T0">Klaudia Plotnikova, teine lint. </ts>
            </ts>
            <ts e="T14" id="Seg_240" n="sc" s="T13">
               <ts e="T14" id="Seg_242" n="e" s="T13">((DMG)). </ts>
            </ts>
            <ts e="T48" id="Seg_243" n="sc" s="T42">
               <ts e="T43" id="Seg_245" n="e" s="T42">Ну </ts>
               <ts e="T44" id="Seg_247" n="e" s="T43">что </ts>
               <ts e="T45" id="Seg_249" n="e" s="T44">ж, </ts>
               <ts e="T46" id="Seg_251" n="e" s="T45">можешь </ts>
               <ts e="T48" id="Seg_253" n="e" s="T46">успокоиться,… </ts>
            </ts>
            <ts e="T85" id="Seg_254" n="sc" s="T74">
               <ts e="T75" id="Seg_256" n="e" s="T74">Ну, </ts>
               <ts e="T76" id="Seg_258" n="e" s="T75">там </ts>
               <ts e="T77" id="Seg_260" n="e" s="T76">коня </ts>
               <ts e="T78" id="Seg_262" n="e" s="T77">еще </ts>
               <ts e="T79" id="Seg_264" n="e" s="T78">запрячь </ts>
               <ts e="T80" id="Seg_266" n="e" s="T79">и </ts>
               <ts e="T81" id="Seg_268" n="e" s="T80">на </ts>
               <ts e="T82" id="Seg_270" n="e" s="T81">коне </ts>
               <ts e="T83" id="Seg_272" n="e" s="T82">поехать, </ts>
               <ts e="T84" id="Seg_274" n="e" s="T83">как </ts>
               <ts e="T85" id="Seg_276" n="e" s="T84">это? </ts>
            </ts>
            <ts e="T90" id="Seg_277" n="sc" s="T86">
               <ts e="T87" id="Seg_279" n="e" s="T86">Ага, </ts>
               <ts e="T88" id="Seg_281" n="e" s="T87">ага. </ts>
               <ts e="T90" id="Seg_283" n="e" s="T88">((DMG)) сейчас. </ts>
            </ts>
            <ts e="T408" id="Seg_284" n="sc" s="T402">
               <ts e="T408" id="Seg_286" n="e" s="T402">Вот еще сахар, кажется, будешь… </ts>
            </ts>
            <ts e="T573" id="Seg_287" n="sc" s="T571">
               <ts e="T573" id="Seg_289" n="e" s="T571">Не разговаривай. </ts>
            </ts>
            <ts e="T577" id="Seg_290" n="sc" s="T575">
               <ts e="T577" id="Seg_292" n="e" s="T575">Не ругайся. </ts>
            </ts>
            <ts e="T582" id="Seg_293" n="sc" s="T579">
               <ts e="T582" id="Seg_295" n="e" s="T579">Не кричи, кажется. </ts>
            </ts>
            <ts e="T586" id="Seg_296" n="sc" s="T584">
               <ts e="T586" id="Seg_298" n="e" s="T584">Сопли плывут. </ts>
            </ts>
            <ts e="T592" id="Seg_299" n="sc" s="T590">
               <ts e="T592" id="Seg_301" n="e" s="T590">Не беги. </ts>
            </ts>
            <ts e="T606" id="Seg_302" n="sc" s="T605">
               <ts e="T606" id="Seg_304" n="e" s="T605">Большой. </ts>
            </ts>
            <ts e="T612" id="Seg_305" n="sc" s="T609">
               <ts e="T610" id="Seg_307" n="e" s="T609">Ага. </ts>
               <ts e="T612" id="Seg_309" n="e" s="T610">Маленький еще. </ts>
            </ts>
            <ts e="T646" id="Seg_310" n="sc" s="T642">
               <ts e="T646" id="Seg_312" n="e" s="T642">А зачем ножом баловался? </ts>
            </ts>
            <ts e="T770" id="Seg_313" n="sc" s="T766">
               <ts e="T770" id="Seg_315" n="e" s="T766">Маленькая чашка чая… </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KA">
            <ta e="T4" id="Seg_316" s="T0">PKZ_1964_SU0205.KA.001 (001)</ta>
            <ta e="T14" id="Seg_317" s="T13">PKZ_1964_SU0205.KA.002 (003)</ta>
            <ta e="T48" id="Seg_318" s="T42">PKZ_1964_SU0205.KA.003 (006)</ta>
            <ta e="T85" id="Seg_319" s="T74">PKZ_1964_SU0205.KA.004 (008)</ta>
            <ta e="T88" id="Seg_320" s="T86">PKZ_1964_SU0205.KA.005 (010)</ta>
            <ta e="T90" id="Seg_321" s="T88">PKZ_1964_SU0205.KA.006 (011)</ta>
            <ta e="T408" id="Seg_322" s="T402">PKZ_1964_SU0205.KA.007 (022)</ta>
            <ta e="T573" id="Seg_323" s="T571">PKZ_1964_SU0205.KA.008 (033.001)</ta>
            <ta e="T577" id="Seg_324" s="T575">PKZ_1964_SU0205.KA.009 (034.001)</ta>
            <ta e="T582" id="Seg_325" s="T579">PKZ_1964_SU0205.KA.010 (035.001)</ta>
            <ta e="T586" id="Seg_326" s="T584">PKZ_1964_SU0205.KA.011 (036.001)</ta>
            <ta e="T592" id="Seg_327" s="T590">PKZ_1964_SU0205.KA.012 (037.001)</ta>
            <ta e="T606" id="Seg_328" s="T605">PKZ_1964_SU0205.KA.013 (039.001)</ta>
            <ta e="T610" id="Seg_329" s="T609">PKZ_1964_SU0205.KA.014 (039.003)</ta>
            <ta e="T612" id="Seg_330" s="T610">PKZ_1964_SU0205.KA.015 (039.004)</ta>
            <ta e="T646" id="Seg_331" s="T642">PKZ_1964_SU0205.KA.016 (041.003)</ta>
            <ta e="T770" id="Seg_332" s="T766">PKZ_1964_SU0205.KA.017 (047.006)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KA">
            <ta e="T4" id="Seg_333" s="T0">Klaudia Plotnikova, teine lint. </ta>
            <ta e="T14" id="Seg_334" s="T13">DMG. </ta>
            <ta e="T48" id="Seg_335" s="T42">Нучто ж, можешь успокоиться, …. </ta>
            <ta e="T85" id="Seg_336" s="T74">Ну, там коня еще запрячь и на коне поехать, как это? </ta>
            <ta e="T88" id="Seg_337" s="T86">Ага, ага. </ta>
            <ta e="T90" id="Seg_338" s="T88">((DMG)) сейчас. </ta>
            <ta e="T408" id="Seg_339" s="T402">Вот еще сахар, кажется, будешь … </ta>
            <ta e="T573" id="Seg_340" s="T571">Не разговаривай. </ta>
            <ta e="T577" id="Seg_341" s="T575">Не ругайся. </ta>
            <ta e="T582" id="Seg_342" s="T579">Не кричи, кажется. </ta>
            <ta e="T586" id="Seg_343" s="T584">Сопли плывут. </ta>
            <ta e="T592" id="Seg_344" s="T590">Не беги. </ta>
            <ta e="T606" id="Seg_345" s="T605">Большой. </ta>
            <ta e="T610" id="Seg_346" s="T609">Ага. </ta>
            <ta e="T612" id="Seg_347" s="T610">Маленький еще. </ta>
            <ta e="T646" id="Seg_348" s="T642">А зачем ножом баловался? </ta>
            <ta e="T770" id="Seg_349" s="T766">Маленькая чашка чая … </ta>
         </annotation>
         <annotation name="CS" tierref="CS-KA">
            <ta e="T4" id="Seg_350" s="T0">EST:ext</ta>
            <ta e="T48" id="Seg_351" s="T42">RUS:ext</ta>
            <ta e="T85" id="Seg_352" s="T74">RUS:ext</ta>
            <ta e="T88" id="Seg_353" s="T86">RUS:ext</ta>
            <ta e="T90" id="Seg_354" s="T88">RUS:ext</ta>
            <ta e="T408" id="Seg_355" s="T402">RUS:ext</ta>
            <ta e="T573" id="Seg_356" s="T571">RUS:ext</ta>
            <ta e="T577" id="Seg_357" s="T575">RUS:ext</ta>
            <ta e="T582" id="Seg_358" s="T579">RUS:ext</ta>
            <ta e="T586" id="Seg_359" s="T584">RUS:ext</ta>
            <ta e="T592" id="Seg_360" s="T590">RUS:ext</ta>
            <ta e="T606" id="Seg_361" s="T605">RUS:ext</ta>
            <ta e="T610" id="Seg_362" s="T609">RUS:ext</ta>
            <ta e="T612" id="Seg_363" s="T610">RUS:ext</ta>
            <ta e="T646" id="Seg_364" s="T642">RUS:ext</ta>
            <ta e="T770" id="Seg_365" s="T766">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KA">
            <ta e="T4" id="Seg_366" s="T0">Клавдия Плотникова, вторая плёнка.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-KA">
            <ta e="T4" id="Seg_367" s="T0">Klavdiya Plotnikova, second tape.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KA" />
         <annotation name="nt" tierref="nt-KA">
            <ta e="T586" id="Seg_368" s="T584">[AAV] Intended: текут?</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-PKZ"
                      id="tx-PKZ"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T42" start="T38">
            <tli id="T38.tx-PKZ.1" />
            <tli id="T38.tx-PKZ.2" />
         </timeline-fork>
         <timeline-fork end="T120" start="T119">
            <tli id="T119.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T376" start="T375">
            <tli id="T375.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T548" start="T547">
            <tli id="T547.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T622" start="T621">
            <tli id="T621.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T630" start="T629">
            <tli id="T629.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T647" start="T645">
            <tli id="T645.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T806" start="T805">
            <tli id="T805.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T885" start="T883">
            <tli id="T883.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T969" start="T966">
            <tli id="T966.tx-PKZ.1" />
            <tli id="T966.tx-PKZ.2" />
         </timeline-fork>
         <timeline-fork end="T979" start="T978">
            <tli id="T978.tx-PKZ.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PKZ">
            <ts e="T13" id="Seg_369" n="sc" s="T4">
               <ts e="T13" id="Seg_371" n="HIAT:u" s="T4">
                  <nts id="Seg_372" n="HIAT:ip">(</nts>
                  <nts id="Seg_373" n="HIAT:ip">(</nts>
                  <ats e="T5" id="Seg_374" n="HIAT:non-pho" s="T4">DMG</ats>
                  <nts id="Seg_375" n="HIAT:ip">)</nts>
                  <nts id="Seg_376" n="HIAT:ip">)</nts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_379" n="HIAT:w" s="T5">Я</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_382" n="HIAT:w" s="T6">сперва</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_385" n="HIAT:w" s="T7">расскажу</ts>
                  <nts id="Seg_386" n="HIAT:ip">,</nts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_389" n="HIAT:w" s="T8">погоди</ts>
                  <nts id="Seg_390" n="HIAT:ip">,</nts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_393" n="HIAT:w" s="T9">не</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_396" n="HIAT:w" s="T10">сейчас</ts>
                  <nts id="Seg_397" n="HIAT:ip">…</nts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T42" id="Seg_399" n="sc" s="T14">
               <ts e="T20" id="Seg_401" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_403" n="HIAT:w" s="T14">Dʼijenə</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_406" n="HIAT:w" s="T15">teinen</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_409" n="HIAT:w" s="T16">kallam</ts>
                  <nts id="Seg_410" n="HIAT:ip">,</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_412" n="HIAT:ip">(</nts>
                  <ts e="T18" id="Seg_414" n="HIAT:w" s="T17">in</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_417" n="HIAT:w" s="T18">-</ts>
                  <nts id="Seg_418" n="HIAT:ip">)</nts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_421" n="HIAT:w" s="T19">ineziʔ</ts>
                  <nts id="Seg_422" n="HIAT:ip">.</nts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_425" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_427" n="HIAT:w" s="T20">Gijen</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_430" n="HIAT:w" s="T21">băra</ts>
                  <nts id="Seg_431" n="HIAT:ip">,</nts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_434" n="HIAT:w" s="T22">nada</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_437" n="HIAT:w" s="T23">ipek</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_440" n="HIAT:w" s="T24">enzittə</ts>
                  <nts id="Seg_441" n="HIAT:ip">,</nts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_444" n="HIAT:w" s="T25">tus</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_447" n="HIAT:w" s="T26">enzittə</ts>
                  <nts id="Seg_448" n="HIAT:ip">,</nts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_451" n="HIAT:w" s="T27">šamnak</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_454" n="HIAT:w" s="T28">nada</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_457" n="HIAT:w" s="T29">enzittə</ts>
                  <nts id="Seg_458" n="HIAT:ip">.</nts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_461" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_463" n="HIAT:w" s="T30">Nada</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_466" n="HIAT:w" s="T31">uja</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_469" n="HIAT:w" s="T32">enzittə</ts>
                  <nts id="Seg_470" n="HIAT:ip">.</nts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_473" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_475" n="HIAT:w" s="T33">Nada</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_478" n="HIAT:w" s="T34">ipek</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_481" n="HIAT:w" s="T35">enzittə</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_483" n="HIAT:ip">(</nts>
                  <ts e="T37" id="Seg_485" n="HIAT:w" s="T36">naʔ</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_488" n="HIAT:w" s="T37">-</ts>
                  <nts id="Seg_489" n="HIAT:ip">)</nts>
                  <nts id="Seg_490" n="HIAT:ip">.</nts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_493" n="HIAT:u" s="T38">
                  <ts e="T38.tx-PKZ.1" id="Seg_495" n="HIAT:w" s="T38">Говорю</ts>
                  <nts id="Seg_496" n="HIAT:ip">,</nts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38.tx-PKZ.2" id="Seg_499" n="HIAT:w" s="T38.tx-PKZ.1">господи</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_502" n="HIAT:w" s="T38.tx-PKZ.2">прости</ts>
                  <nts id="Seg_503" n="HIAT:ip">!</nts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T74" id="Seg_505" n="sc" s="T46">
               <ts e="T51" id="Seg_507" n="HIAT:u" s="T46">
                  <ts e="T49" id="Seg_509" n="HIAT:w" s="T46">Ipek</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_512" n="HIAT:w" s="T49">enzittə</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_515" n="HIAT:w" s="T50">nada</ts>
                  <nts id="Seg_516" n="HIAT:ip">.</nts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_519" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_521" n="HIAT:w" s="T51">Kajak</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_524" n="HIAT:w" s="T52">enzittə</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_527" n="HIAT:w" s="T53">nada</ts>
                  <nts id="Seg_528" n="HIAT:ip">.</nts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_531" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_533" n="HIAT:w" s="T54">Munujʔ</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_536" n="HIAT:w" s="T55">enzittə</ts>
                  <nts id="Seg_537" n="HIAT:ip">.</nts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_540" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_542" n="HIAT:w" s="T56">Šamnak</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_545" n="HIAT:w" s="T57">nada</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_548" n="HIAT:w" s="T58">enzittə</ts>
                  <nts id="Seg_549" n="HIAT:ip">.</nts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_552" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_554" n="HIAT:w" s="T59">Nada</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_557" n="HIAT:w" s="T60">aspaʔ</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_560" n="HIAT:w" s="T61">enzittə</ts>
                  <nts id="Seg_561" n="HIAT:ip">.</nts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_564" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_566" n="HIAT:w" s="T62">Men</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_569" n="HIAT:w" s="T63">izittə</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_572" n="HIAT:w" s="T64">nada</ts>
                  <nts id="Seg_573" n="HIAT:ip">.</nts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_576" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_578" n="HIAT:w" s="T65">Multuk</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_581" n="HIAT:w" s="T66">izittə</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_584" n="HIAT:w" s="T67">nada</ts>
                  <nts id="Seg_585" n="HIAT:ip">.</nts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_588" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_590" n="HIAT:w" s="T68">Nada</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_593" n="HIAT:w" s="T69">baltu</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_596" n="HIAT:w" s="T70">izittə</ts>
                  <nts id="Seg_597" n="HIAT:ip">.</nts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_600" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_602" n="HIAT:w" s="T71">Dagaj</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_605" n="HIAT:w" s="T72">izittə</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_608" n="HIAT:w" s="T73">nada</ts>
                  <nts id="Seg_609" n="HIAT:ip">.</nts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T86" id="Seg_611" n="sc" s="T85">
               <ts e="T86" id="Seg_613" n="HIAT:u" s="T85">
                  <nts id="Seg_614" n="HIAT:ip">(</nts>
                  <nts id="Seg_615" n="HIAT:ip">(</nts>
                  <ats e="T86" id="Seg_616" n="HIAT:non-pho" s="T85">kukganʼanʼi</ats>
                  <nts id="Seg_617" n="HIAT:ip">)</nts>
                  <nts id="Seg_618" n="HIAT:ip">)</nts>
                  <nts id="Seg_619" n="HIAT:ip">.</nts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T402" id="Seg_621" n="sc" s="T90">
               <ts e="T94" id="Seg_623" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_625" n="HIAT:w" s="T90">Nu</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_628" n="HIAT:w" s="T91">kuzasogə</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_631" n="HIAT:w" s="T92">šonəgaʔi</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_634" n="HIAT:w" s="T93">dʼăbaktərzittə</ts>
                  <nts id="Seg_635" n="HIAT:ip">.</nts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_638" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_640" n="HIAT:w" s="T94">Amnaʔ</ts>
                  <nts id="Seg_641" n="HIAT:ip">!</nts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_644" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_646" n="HIAT:w" s="T95">Dʼăbaktərləbaʔ</ts>
                  <nts id="Seg_647" n="HIAT:ip">!</nts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_650" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_652" n="HIAT:w" s="T96">Amorzittə</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_655" n="HIAT:w" s="T97">padʼi</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_658" n="HIAT:w" s="T98">axota</ts>
                  <nts id="Seg_659" n="HIAT:ip">,</nts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_662" n="HIAT:w" s="T99">amnaʔ</ts>
                  <nts id="Seg_663" n="HIAT:ip">,</nts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_666" n="HIAT:w" s="T100">amoraʔ</ts>
                  <nts id="Seg_667" n="HIAT:ip">!</nts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_670" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_672" n="HIAT:w" s="T101">Dĭn</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_675" n="HIAT:w" s="T102">ipek</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_678" n="HIAT:w" s="T103">iʔbölaʔbə</ts>
                  <nts id="Seg_679" n="HIAT:ip">.</nts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_682" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_684" n="HIAT:w" s="T104">Kajak</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_687" n="HIAT:w" s="T105">nuga</ts>
                  <nts id="Seg_688" n="HIAT:ip">.</nts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_691" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_693" n="HIAT:w" s="T106">Šamnak</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_696" n="HIAT:w" s="T107">ige</ts>
                  <nts id="Seg_697" n="HIAT:ip">,</nts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_700" n="HIAT:w" s="T108">tus</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_703" n="HIAT:w" s="T109">ige</ts>
                  <nts id="Seg_704" n="HIAT:ip">,</nts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_707" n="HIAT:w" s="T110">munujʔ</ts>
                  <nts id="Seg_708" n="HIAT:ip">,</nts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_711" n="HIAT:w" s="T111">uja</ts>
                  <nts id="Seg_712" n="HIAT:ip">,</nts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_715" n="HIAT:w" s="T112">kola</ts>
                  <nts id="Seg_716" n="HIAT:ip">,</nts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_719" n="HIAT:w" s="T113">amaʔ</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_722" n="HIAT:w" s="T114">bar</ts>
                  <nts id="Seg_723" n="HIAT:ip">!</nts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_726" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_728" n="HIAT:w" s="T115">Iʔ</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_731" n="HIAT:w" s="T116">alomaʔ</ts>
                  <nts id="Seg_732" n="HIAT:ip">!</nts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_735" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_737" n="HIAT:w" s="T117">Iʔ</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_740" n="HIAT:w" s="T118">kirgaːraʔ</ts>
                  <nts id="Seg_741" n="HIAT:ip">!</nts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_744" n="HIAT:u" s="T119">
                  <ts e="T119.tx-PKZ.1" id="Seg_746" n="HIAT:w" s="T119">A</ts>
                  <nts id="Seg_747" n="HIAT:ip">_</nts>
                  <ts e="T120" id="Seg_749" n="HIAT:w" s="T119.tx-PKZ.1">to</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_752" n="HIAT:w" s="T120">münörlem</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_755" n="HIAT:w" s="T121">tănan</ts>
                  <nts id="Seg_756" n="HIAT:ip">!</nts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_759" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_761" n="HIAT:w" s="T122">Iʔ</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_764" n="HIAT:w" s="T123">dʼoraʔ</ts>
                  <nts id="Seg_765" n="HIAT:ip">!</nts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_768" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_770" n="HIAT:w" s="T124">Iʔ</ts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_773" n="HIAT:w" s="T125">kuroːmaʔ</ts>
                  <nts id="Seg_774" n="HIAT:ip">!</nts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_777" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_779" n="HIAT:w" s="T126">Iʔ</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_782" n="HIAT:w" s="T127">šaːmaʔ</ts>
                  <nts id="Seg_783" n="HIAT:ip">!</nts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_786" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_788" n="HIAT:w" s="T128">Amnoːlaʔbəl</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_791" n="HIAT:w" s="T129">bar</ts>
                  <nts id="Seg_792" n="HIAT:ip">,</nts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_795" n="HIAT:w" s="T130">šaːmnaʔbəl</ts>
                  <nts id="Seg_796" n="HIAT:ip">!</nts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_799" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_801" n="HIAT:w" s="T131">Tăn</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_804" n="HIAT:w" s="T132">ugaːndə</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_807" n="HIAT:w" s="T133">iʔgö</ts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_810" n="HIAT:w" s="T134">šaːmnaʔbəl</ts>
                  <nts id="Seg_811" n="HIAT:ip">.</nts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_814" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_816" n="HIAT:w" s="T135">Все</ts>
                  <nts id="Seg_817" n="HIAT:ip">,</nts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_820" n="HIAT:w" s="T136">наверное</ts>
                  <nts id="Seg_821" n="HIAT:ip">.</nts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_824" n="HIAT:u" s="T137">
                  <ts e="T138" id="Seg_826" n="HIAT:w" s="T137">Măn</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_829" n="HIAT:w" s="T138">tüjö</ts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_832" n="HIAT:w" s="T139">kallam</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_835" n="HIAT:w" s="T140">šumuranə</ts>
                  <nts id="Seg_836" n="HIAT:ip">.</nts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_839" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_841" n="HIAT:w" s="T141">A</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_844" n="HIAT:w" s="T142">tăn</ts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_847" n="HIAT:w" s="T143">maʔnəl</ts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_850" n="HIAT:w" s="T144">amnoʔ</ts>
                  <nts id="Seg_851" n="HIAT:ip">!</nts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_854" n="HIAT:u" s="T145">
                  <ts e="T146" id="Seg_856" n="HIAT:w" s="T145">Iʔ</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_859" n="HIAT:w" s="T146">alomaʔ</ts>
                  <nts id="Seg_860" n="HIAT:ip">!</nts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_863" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_865" n="HIAT:w" s="T147">Măn</ts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_868" n="HIAT:w" s="T148">ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_871" n="HIAT:w" s="T149">tănan</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_874" n="HIAT:w" s="T150">detlem</ts>
                  <nts id="Seg_875" n="HIAT:ip">.</nts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_878" n="HIAT:u" s="T151">
                  <ts e="T152" id="Seg_880" n="HIAT:w" s="T151">Dĭgəttə</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_883" n="HIAT:w" s="T152">mĭlem</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_886" n="HIAT:w" s="T153">tănan</ts>
                  <nts id="Seg_887" n="HIAT:ip">.</nts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_890" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_892" n="HIAT:w" s="T154">Taːldʼen</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_894" n="HIAT:ip">(</nts>
                  <ts e="T156" id="Seg_896" n="HIAT:w" s="T155">di</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_899" n="HIAT:w" s="T156">-</ts>
                  <nts id="Seg_900" n="HIAT:ip">)</nts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_903" n="HIAT:w" s="T157">taːldʼen</ts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_906" n="HIAT:w" s="T158">šobi</ts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_909" n="HIAT:w" s="T159">măn</ts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_912" n="HIAT:w" s="T160">tuganbə</ts>
                  <nts id="Seg_913" n="HIAT:ip">.</nts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_916" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_918" n="HIAT:w" s="T161">Măn</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_921" n="HIAT:w" s="T162">mămbiam:</ts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_924" n="HIAT:w" s="T163">iʔbeʔ</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_927" n="HIAT:w" s="T164">kunoːlzittə</ts>
                  <nts id="Seg_928" n="HIAT:ip">!</nts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_931" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_933" n="HIAT:w" s="T165">Dĭgəttə</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_936" n="HIAT:w" s="T166">mămbiam:</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_939" n="HIAT:w" s="T167">amoraʔ</ts>
                  <nts id="Seg_940" n="HIAT:ip">,</nts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_943" n="HIAT:w" s="T168">padʼi</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_946" n="HIAT:w" s="T169">amorzittə</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_949" n="HIAT:w" s="T170">axota</ts>
                  <nts id="Seg_950" n="HIAT:ip">.</nts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T180" id="Seg_953" n="HIAT:u" s="T171">
                  <ts e="T172" id="Seg_955" n="HIAT:w" s="T171">Dĭ</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_958" n="HIAT:w" s="T172">mămbi:</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_960" n="HIAT:ip">(</nts>
                  <ts e="T174" id="Seg_962" n="HIAT:w" s="T173">măn</ts>
                  <nts id="Seg_963" n="HIAT:ip">)</nts>
                  <nts id="Seg_964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_966" n="HIAT:w" s="T174">măn</ts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_969" n="HIAT:w" s="T175">ej</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_971" n="HIAT:ip">(</nts>
                  <ts e="T177" id="Seg_973" n="HIAT:w" s="T176">amori</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_976" n="HIAT:w" s="T177">-</ts>
                  <nts id="Seg_977" n="HIAT:ip">)</nts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_980" n="HIAT:w" s="T178">amorzittə</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_982" n="HIAT:ip">(</nts>
                  <nts id="Seg_983" n="HIAT:ip">(</nts>
                  <ats e="T180" id="Seg_984" n="HIAT:non-pho" s="T179">n</ats>
                  <nts id="Seg_985" n="HIAT:ip">)</nts>
                  <nts id="Seg_986" n="HIAT:ip">)</nts>
                  <nts id="Seg_987" n="HIAT:ip">.</nts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_990" n="HIAT:u" s="T180">
                  <ts e="T181" id="Seg_992" n="HIAT:w" s="T180">Măn</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_995" n="HIAT:w" s="T181">turagən</ts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_998" n="HIAT:w" s="T182">ambiam</ts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_1001" n="HIAT:w" s="T183">dĭn</ts>
                  <nts id="Seg_1002" n="HIAT:ip">.</nts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T189" id="Seg_1005" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_1007" n="HIAT:w" s="T184">Dĭgəttə</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1009" n="HIAT:ip">(</nts>
                  <ts e="T186" id="Seg_1011" n="HIAT:w" s="T185">iʔbə</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_1014" n="HIAT:w" s="T186">-</ts>
                  <nts id="Seg_1015" n="HIAT:ip">)</nts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_1018" n="HIAT:w" s="T187">iʔbəbi</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_1021" n="HIAT:w" s="T188">kunoːlzittə</ts>
                  <nts id="Seg_1022" n="HIAT:ip">.</nts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T191" id="Seg_1025" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_1027" n="HIAT:w" s="T189">Măn</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_1030" n="HIAT:w" s="T190">iʔbəbiem</ts>
                  <nts id="Seg_1031" n="HIAT:ip">.</nts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_1034" n="HIAT:u" s="T191">
                  <ts e="T192" id="Seg_1036" n="HIAT:w" s="T191">Erten</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_1039" n="HIAT:w" s="T192">uʔbdəbiam</ts>
                  <nts id="Seg_1040" n="HIAT:ip">.</nts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T195" id="Seg_1043" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_1045" n="HIAT:w" s="T193">Šumuranə</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_1048" n="HIAT:w" s="T194">mĭmbiem</ts>
                  <nts id="Seg_1049" n="HIAT:ip">.</nts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_1052" n="HIAT:u" s="T195">
                  <ts e="T196" id="Seg_1054" n="HIAT:w" s="T195">Šobiam</ts>
                  <nts id="Seg_1055" n="HIAT:ip">.</nts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T199" id="Seg_1058" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_1060" n="HIAT:w" s="T196">Dĭʔnə</ts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_1063" n="HIAT:w" s="T197">mămbiam:</ts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_1066" n="HIAT:w" s="T198">uʔbdaʔ</ts>
                  <nts id="Seg_1067" n="HIAT:ip">!</nts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_1070" n="HIAT:u" s="T199">
                  <ts e="T200" id="Seg_1072" n="HIAT:w" s="T199">Amoraʔ</ts>
                  <nts id="Seg_1073" n="HIAT:ip">,</nts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_1076" n="HIAT:w" s="T200">dĭn</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_1079" n="HIAT:w" s="T201">namzəga</ts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1081" n="HIAT:ip">(</nts>
                  <ts e="T203" id="Seg_1083" n="HIAT:w" s="T202">su</ts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_1086" n="HIAT:w" s="T203">ig</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_1089" n="HIAT:w" s="T204">-</ts>
                  <nts id="Seg_1090" n="HIAT:ip">)</nts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_1093" n="HIAT:w" s="T205">süt</ts>
                  <nts id="Seg_1094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_1096" n="HIAT:w" s="T206">ige</ts>
                  <nts id="Seg_1097" n="HIAT:ip">.</nts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T211" id="Seg_1100" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_1102" n="HIAT:w" s="T207">Keʔbde</ts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_1105" n="HIAT:w" s="T208">ige</ts>
                  <nts id="Seg_1106" n="HIAT:ip">,</nts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_1109" n="HIAT:w" s="T209">munujʔ</ts>
                  <nts id="Seg_1110" n="HIAT:ip">,</nts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_1113" n="HIAT:w" s="T210">ipek</ts>
                  <nts id="Seg_1114" n="HIAT:ip">.</nts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T213" id="Seg_1117" n="HIAT:u" s="T211">
                  <ts e="T212" id="Seg_1119" n="HIAT:w" s="T211">Amoraʔ</ts>
                  <nts id="Seg_1120" n="HIAT:ip">,</nts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_1123" n="HIAT:w" s="T212">amnaʔ</ts>
                  <nts id="Seg_1124" n="HIAT:ip">.</nts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_1127" n="HIAT:u" s="T213">
                  <nts id="Seg_1128" n="HIAT:ip">(</nts>
                  <ts e="T214" id="Seg_1130" n="HIAT:w" s="T213">Dĭ</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_1133" n="HIAT:w" s="T214">ba</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_1136" n="HIAT:w" s="T215">-</ts>
                  <nts id="Seg_1137" n="HIAT:ip">)</nts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_1140" n="HIAT:w" s="T216">dĭ</ts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_1143" n="HIAT:w" s="T217">uʔpi</ts>
                  <nts id="Seg_1144" n="HIAT:ip">.</nts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T219" id="Seg_1147" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_1149" n="HIAT:w" s="T218">Bozəjzəbi</ts>
                  <nts id="Seg_1150" n="HIAT:ip">.</nts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T222" id="Seg_1153" n="HIAT:u" s="T219">
                  <ts e="T220" id="Seg_1155" n="HIAT:w" s="T219">Dĭgəttə</ts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_1158" n="HIAT:w" s="T220">amnobi</ts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_1161" n="HIAT:w" s="T221">amorzittə</ts>
                  <nts id="Seg_1162" n="HIAT:ip">.</nts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_1165" n="HIAT:u" s="T222">
                  <ts e="T223" id="Seg_1167" n="HIAT:w" s="T222">Всё</ts>
                  <nts id="Seg_1168" n="HIAT:ip">.</nts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_1171" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_1173" n="HIAT:w" s="T223">Dĭgəttə</ts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_1176" n="HIAT:w" s="T224">kala</ts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_1179" n="HIAT:w" s="T225">dʼürbi</ts>
                  <nts id="Seg_1180" n="HIAT:ip">.</nts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_1183" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_1185" n="HIAT:w" s="T226">A</ts>
                  <nts id="Seg_1186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_1188" n="HIAT:w" s="T227">măn</ts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_1191" n="HIAT:w" s="T228">bozəjbiam</ts>
                  <nts id="Seg_1192" n="HIAT:ip">.</nts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T232" id="Seg_1195" n="HIAT:u" s="T229">
                  <ts e="T230" id="Seg_1197" n="HIAT:w" s="T229">Ну</ts>
                  <nts id="Seg_1198" n="HIAT:ip">,</nts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_1201" n="HIAT:w" s="T230">все</ts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1203" n="HIAT:ip">(</nts>
                  <nts id="Seg_1204" n="HIAT:ip">(</nts>
                  <ats e="T232" id="Seg_1205" n="HIAT:non-pho" s="T231">…</ats>
                  <nts id="Seg_1206" n="HIAT:ip">)</nts>
                  <nts id="Seg_1207" n="HIAT:ip">)</nts>
                  <nts id="Seg_1208" n="HIAT:ip">.</nts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_1211" n="HIAT:u" s="T232">
                  <ts e="T233" id="Seg_1213" n="HIAT:w" s="T232">Taːldʼen</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_1216" n="HIAT:w" s="T233">măn</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_1219" n="HIAT:w" s="T234">miʔ</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_1222" n="HIAT:w" s="T235">tuʔluʔ</ts>
                  <nts id="Seg_1223" n="HIAT:ip">.</nts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T239" id="Seg_1226" n="HIAT:u" s="T236">
                  <nts id="Seg_1227" n="HIAT:ip">(</nts>
                  <ts e="T237" id="Seg_1229" n="HIAT:w" s="T236">Tu</ts>
                  <nts id="Seg_1230" n="HIAT:ip">)</nts>
                  <nts id="Seg_1231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_1233" n="HIAT:w" s="T237">tažerbibaʔ</ts>
                  <nts id="Seg_1234" n="HIAT:ip">,</nts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_1237" n="HIAT:w" s="T238">ineziʔ</ts>
                  <nts id="Seg_1238" n="HIAT:ip">.</nts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_1241" n="HIAT:u" s="T239">
                  <ts e="T240" id="Seg_1243" n="HIAT:w" s="T239">Šide</ts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_1246" n="HIAT:w" s="T240">koʔbdo</ts>
                  <nts id="Seg_1247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_1249" n="HIAT:w" s="T241">embiʔi</ts>
                  <nts id="Seg_1250" n="HIAT:ip">.</nts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T249" id="Seg_1253" n="HIAT:u" s="T242">
                  <ts e="T243" id="Seg_1255" n="HIAT:w" s="T242">Onʼiʔ</ts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_1258" n="HIAT:w" s="T243">tibi</ts>
                  <nts id="Seg_1259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_1261" n="HIAT:w" s="T244">măn</ts>
                  <nts id="Seg_1262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1263" n="HIAT:ip">(</nts>
                  <ts e="T246" id="Seg_1265" n="HIAT:w" s="T245">bospəm</ts>
                  <nts id="Seg_1266" n="HIAT:ip">)</nts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1268" n="HIAT:ip">(</nts>
                  <ts e="T247" id="Seg_1270" n="HIAT:w" s="T246">u</ts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_1273" n="HIAT:w" s="T247">-</ts>
                  <nts id="Seg_1274" n="HIAT:ip">)</nts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_1277" n="HIAT:w" s="T248">embiem</ts>
                  <nts id="Seg_1278" n="HIAT:ip">.</nts>
                  <nts id="Seg_1279" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T250" id="Seg_1281" n="HIAT:u" s="T249">
                  <ts e="T250" id="Seg_1283" n="HIAT:w" s="T249">Tažerbibaʔ</ts>
                  <nts id="Seg_1284" n="HIAT:ip">.</nts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_1287" n="HIAT:u" s="T250">
                  <ts e="T251" id="Seg_1289" n="HIAT:w" s="T250">Погоди</ts>
                  <nts id="Seg_1290" n="HIAT:ip">,</nts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_1293" n="HIAT:w" s="T251">как</ts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_1296" n="HIAT:w" s="T253">я</ts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_1299" n="HIAT:w" s="T254">это</ts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_1302" n="HIAT:w" s="T255">забыла</ts>
                  <nts id="Seg_1303" n="HIAT:ip">.</nts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_1306" n="HIAT:u" s="T256">
                  <ts e="T257" id="Seg_1308" n="HIAT:w" s="T256">Măn</ts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_1311" n="HIAT:w" s="T257">karəldʼaːn</ts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_1314" n="HIAT:w" s="T258">multʼanə</ts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_1317" n="HIAT:w" s="T259">mĭmbiem</ts>
                  <nts id="Seg_1318" n="HIAT:ip">.</nts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T264" id="Seg_1321" n="HIAT:u" s="T260">
                  <ts e="T261" id="Seg_1323" n="HIAT:w" s="T260">Bozəjbiam</ts>
                  <nts id="Seg_1324" n="HIAT:ip">,</nts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1327" n="HIAT:w" s="T261">šišəge</ts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1330" n="HIAT:w" s="T262">bü</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1333" n="HIAT:w" s="T263">ibi</ts>
                  <nts id="Seg_1334" n="HIAT:ip">.</nts>
                  <nts id="Seg_1335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T266" id="Seg_1337" n="HIAT:u" s="T264">
                  <ts e="T265" id="Seg_1339" n="HIAT:w" s="T264">Dʼibige</ts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1342" n="HIAT:w" s="T265">bü</ts>
                  <nts id="Seg_1343" n="HIAT:ip">.</nts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T268" id="Seg_1346" n="HIAT:u" s="T266">
                  <ts e="T267" id="Seg_1348" n="HIAT:w" s="T266">Saːbən</ts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1351" n="HIAT:w" s="T267">ibiem</ts>
                  <nts id="Seg_1352" n="HIAT:ip">.</nts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T270" id="Seg_1355" n="HIAT:u" s="T268">
                  <ts e="T269" id="Seg_1357" n="HIAT:w" s="T268">Kujnek</ts>
                  <nts id="Seg_1358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1360" n="HIAT:w" s="T269">ibiem</ts>
                  <nts id="Seg_1361" n="HIAT:ip">.</nts>
                  <nts id="Seg_1362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T274" id="Seg_1364" n="HIAT:u" s="T270">
                  <ts e="T271" id="Seg_1366" n="HIAT:w" s="T270">Multʼagəʔ</ts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1369" n="HIAT:w" s="T271">šobiam</ts>
                  <nts id="Seg_1370" n="HIAT:ip">,</nts>
                  <nts id="Seg_1371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1373" n="HIAT:w" s="T272">tüžöj</ts>
                  <nts id="Seg_1374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1376" n="HIAT:w" s="T273">šobi</ts>
                  <nts id="Seg_1377" n="HIAT:ip">.</nts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T276" id="Seg_1380" n="HIAT:u" s="T274">
                  <ts e="T275" id="Seg_1382" n="HIAT:w" s="T274">Măn</ts>
                  <nts id="Seg_1383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1385" n="HIAT:w" s="T275">surdobiam</ts>
                  <nts id="Seg_1386" n="HIAT:ip">.</nts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T278" id="Seg_1389" n="HIAT:u" s="T276">
                  <ts e="T277" id="Seg_1391" n="HIAT:w" s="T276">Ugaːndə</ts>
                  <nts id="Seg_1392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1394" n="HIAT:w" s="T277">taraːruʔbiem</ts>
                  <nts id="Seg_1395" n="HIAT:ip">.</nts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_1398" n="HIAT:u" s="T278">
                  <ts e="T279" id="Seg_1400" n="HIAT:w" s="T278">Saʔməluʔpam</ts>
                  <nts id="Seg_1401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1402" n="HIAT:ip">(</nts>
                  <ts e="T280" id="Seg_1404" n="HIAT:w" s="T279">i</ts>
                  <nts id="Seg_1405" n="HIAT:ip">)</nts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1408" n="HIAT:w" s="T280">i</ts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1411" n="HIAT:w" s="T281">kunoːlbiam</ts>
                  <nts id="Seg_1412" n="HIAT:ip">.</nts>
                  <nts id="Seg_1413" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T289" id="Seg_1415" n="HIAT:u" s="T282">
                  <ts e="T283" id="Seg_1417" n="HIAT:w" s="T282">Teinen</ts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1420" n="HIAT:w" s="T283">noʔ</ts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1423" n="HIAT:w" s="T284">măn</ts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1426" n="HIAT:w" s="T285">kallam</ts>
                  <nts id="Seg_1427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1429" n="HIAT:w" s="T286">beškejleʔ</ts>
                  <nts id="Seg_1430" n="HIAT:ip">,</nts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1433" n="HIAT:w" s="T287">beške</ts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1436" n="HIAT:w" s="T288">detlem</ts>
                  <nts id="Seg_1437" n="HIAT:ip">.</nts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T291" id="Seg_1440" n="HIAT:u" s="T289">
                  <ts e="T290" id="Seg_1442" n="HIAT:w" s="T289">Dĭgəttə</ts>
                  <nts id="Seg_1443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1445" n="HIAT:w" s="T290">bozlam</ts>
                  <nts id="Seg_1446" n="HIAT:ip">.</nts>
                  <nts id="Seg_1447" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T293" id="Seg_1449" n="HIAT:u" s="T291">
                  <ts e="T292" id="Seg_1451" n="HIAT:w" s="T291">Tustʼarlim</ts>
                  <nts id="Seg_1452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1454" n="HIAT:w" s="T292">bar</ts>
                  <nts id="Seg_1455" n="HIAT:ip">.</nts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T296" id="Seg_1458" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_1460" n="HIAT:w" s="T293">Dĭgəttə</ts>
                  <nts id="Seg_1461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1463" n="HIAT:w" s="T294">tustʼarləj</ts>
                  <nts id="Seg_1464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1466" n="HIAT:w" s="T295">bar</ts>
                  <nts id="Seg_1467" n="HIAT:ip">.</nts>
                  <nts id="Seg_1468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_1470" n="HIAT:u" s="T296">
                  <ts e="T297" id="Seg_1472" n="HIAT:w" s="T296">Dĭgəttə</ts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1475" n="HIAT:w" s="T297">amzittə</ts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1478" n="HIAT:w" s="T298">i</ts>
                  <nts id="Seg_1479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1481" n="HIAT:w" s="T299">možna</ts>
                  <nts id="Seg_1482" n="HIAT:ip">.</nts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T306" id="Seg_1485" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_1487" n="HIAT:w" s="T300">Koŋ</ts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1490" n="HIAT:w" s="T301">bar</ts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1493" n="HIAT:w" s="T302">amnoːlaʔpi</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1496" n="HIAT:w" s="T303">bar</ts>
                  <nts id="Seg_1497" n="HIAT:ip">,</nts>
                  <nts id="Seg_1498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1500" n="HIAT:w" s="T304">ugaːndə</ts>
                  <nts id="Seg_1501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1503" n="HIAT:w" s="T305">jezerik</ts>
                  <nts id="Seg_1504" n="HIAT:ip">.</nts>
                  <nts id="Seg_1505" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T311" id="Seg_1507" n="HIAT:u" s="T306">
                  <ts e="T307" id="Seg_1509" n="HIAT:w" s="T306">Ara</ts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1512" n="HIAT:w" s="T307">bĭʔpi</ts>
                  <nts id="Seg_1513" n="HIAT:ip">,</nts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1516" n="HIAT:w" s="T308">ugaːndə</ts>
                  <nts id="Seg_1517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1519" n="HIAT:w" s="T309">iʔgö</ts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1522" n="HIAT:w" s="T310">bĭʔpi</ts>
                  <nts id="Seg_1523" n="HIAT:ip">.</nts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T316" id="Seg_1526" n="HIAT:u" s="T311">
                  <ts e="T312" id="Seg_1528" n="HIAT:w" s="T311">Saʔməluʔpi</ts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1531" n="HIAT:w" s="T312">bar</ts>
                  <nts id="Seg_1532" n="HIAT:ip">,</nts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1535" n="HIAT:w" s="T313">mut-mat</ts>
                  <nts id="Seg_1536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1537" n="HIAT:ip">(</nts>
                  <ts e="T315" id="Seg_1539" n="HIAT:w" s="T314">kudo-</ts>
                  <nts id="Seg_1540" n="HIAT:ip">)</nts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1543" n="HIAT:w" s="T315">kudonzlaʔbə</ts>
                  <nts id="Seg_1544" n="HIAT:ip">.</nts>
                  <nts id="Seg_1545" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_1547" n="HIAT:u" s="T316">
                  <ts e="T317" id="Seg_1549" n="HIAT:w" s="T316">Măn</ts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1552" n="HIAT:w" s="T317">dĭm</ts>
                  <nts id="Seg_1553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1555" n="HIAT:w" s="T318">amnəːlbiam</ts>
                  <nts id="Seg_1556" n="HIAT:ip">.</nts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T321" id="Seg_1559" n="HIAT:u" s="T319">
                  <ts e="T320" id="Seg_1561" n="HIAT:w" s="T319">Amnoʔ</ts>
                  <nts id="Seg_1562" n="HIAT:ip">,</nts>
                  <nts id="Seg_1563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1565" n="HIAT:w" s="T320">mămbiam</ts>
                  <nts id="Seg_1566" n="HIAT:ip">.</nts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T326" id="Seg_1569" n="HIAT:u" s="T321">
                  <ts e="T322" id="Seg_1571" n="HIAT:w" s="T321">Măn</ts>
                  <nts id="Seg_1572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1574" n="HIAT:w" s="T322">mămbiam:</ts>
                  <nts id="Seg_1575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1577" n="HIAT:w" s="T323">Iʔ</ts>
                  <nts id="Seg_1578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1580" n="HIAT:w" s="T324">kudonzaʔ</ts>
                  <nts id="Seg_1581" n="HIAT:ip">,</nts>
                  <nts id="Seg_1582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1584" n="HIAT:w" s="T325">amnoʔ</ts>
                  <nts id="Seg_1585" n="HIAT:ip">!</nts>
                  <nts id="Seg_1586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T330" id="Seg_1588" n="HIAT:u" s="T326">
                  <ts e="T327" id="Seg_1590" n="HIAT:w" s="T326">Amnoʔ</ts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1593" n="HIAT:w" s="T327">segi</ts>
                  <nts id="Seg_1594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1596" n="HIAT:w" s="T328">bü</ts>
                  <nts id="Seg_1597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1599" n="HIAT:w" s="T329">bĭssittə</ts>
                  <nts id="Seg_1600" n="HIAT:ip">.</nts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T335" id="Seg_1603" n="HIAT:u" s="T330">
                  <ts e="T331" id="Seg_1605" n="HIAT:w" s="T330">Sĭreʔpne</ts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1608" n="HIAT:w" s="T331">iʔbölaʔbə</ts>
                  <nts id="Seg_1609" n="HIAT:ip">,</nts>
                  <nts id="Seg_1610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1612" n="HIAT:w" s="T332">ende</ts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1615" n="HIAT:w" s="T333">segi</ts>
                  <nts id="Seg_1616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1618" n="HIAT:w" s="T334">bütnə</ts>
                  <nts id="Seg_1619" n="HIAT:ip">.</nts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T337" id="Seg_1622" n="HIAT:u" s="T335">
                  <ts e="T336" id="Seg_1624" n="HIAT:w" s="T335">Munuʔi</ts>
                  <nts id="Seg_1625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1627" n="HIAT:w" s="T336">iʔbölaʔbəʔjə</ts>
                  <nts id="Seg_1628" n="HIAT:ip">.</nts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T341" id="Seg_1631" n="HIAT:u" s="T337">
                  <nts id="Seg_1632" n="HIAT:ip">(</nts>
                  <ts e="T338" id="Seg_1634" n="HIAT:w" s="T337">Ikə</ts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1637" n="HIAT:w" s="T338">-</ts>
                  <nts id="Seg_1638" n="HIAT:ip">)</nts>
                  <nts id="Seg_1639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1641" n="HIAT:w" s="T339">Ipek</ts>
                  <nts id="Seg_1642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1644" n="HIAT:w" s="T340">amaʔ</ts>
                  <nts id="Seg_1645" n="HIAT:ip">!</nts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T343" id="Seg_1648" n="HIAT:u" s="T341">
                  <ts e="T342" id="Seg_1650" n="HIAT:w" s="T341">Uja</ts>
                  <nts id="Seg_1651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1653" n="HIAT:w" s="T342">amaʔ</ts>
                  <nts id="Seg_1654" n="HIAT:ip">!</nts>
                  <nts id="Seg_1655" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T345" id="Seg_1657" n="HIAT:u" s="T343">
                  <ts e="T344" id="Seg_1659" n="HIAT:w" s="T343">Kola</ts>
                  <nts id="Seg_1660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1662" n="HIAT:w" s="T344">amaʔ</ts>
                  <nts id="Seg_1663" n="HIAT:ip">!</nts>
                  <nts id="Seg_1664" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T348" id="Seg_1666" n="HIAT:u" s="T345">
                  <ts e="T346" id="Seg_1668" n="HIAT:w" s="T345">Munuj</ts>
                  <nts id="Seg_1669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1671" n="HIAT:w" s="T346">iʔbölaʔbə</ts>
                  <nts id="Seg_1672" n="HIAT:ip">,</nts>
                  <nts id="Seg_1673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1675" n="HIAT:w" s="T347">amaʔ</ts>
                  <nts id="Seg_1676" n="HIAT:ip">!</nts>
                  <nts id="Seg_1677" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T352" id="Seg_1679" n="HIAT:u" s="T348">
                  <ts e="T349" id="Seg_1681" n="HIAT:w" s="T348">Kajaʔ</ts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1684" n="HIAT:w" s="T349">amaʔ</ts>
                  <nts id="Seg_1685" n="HIAT:ip">,</nts>
                  <nts id="Seg_1686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1688" n="HIAT:w" s="T350">oroma</ts>
                  <nts id="Seg_1689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1691" n="HIAT:w" s="T351">amaʔ</ts>
                  <nts id="Seg_1692" n="HIAT:ip">!</nts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T355" id="Seg_1695" n="HIAT:u" s="T352">
                  <ts e="T353" id="Seg_1697" n="HIAT:w" s="T352">Namzəga</ts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1700" n="HIAT:w" s="T353">süt</ts>
                  <nts id="Seg_1701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1703" n="HIAT:w" s="T354">amaʔ</ts>
                  <nts id="Seg_1704" n="HIAT:ip">!</nts>
                  <nts id="Seg_1705" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T357" id="Seg_1707" n="HIAT:u" s="T355">
                  <ts e="T356" id="Seg_1709" n="HIAT:w" s="T355">Kola</ts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1712" n="HIAT:w" s="T356">amaʔ</ts>
                  <nts id="Seg_1713" n="HIAT:ip">!</nts>
                  <nts id="Seg_1714" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T358" id="Seg_1716" n="HIAT:u" s="T357">
                  <ts e="T358" id="Seg_1718" n="HIAT:w" s="T357">Хватит</ts>
                  <nts id="Seg_1719" n="HIAT:ip">.</nts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T362" id="Seg_1722" n="HIAT:u" s="T358">
                  <ts e="T359" id="Seg_1724" n="HIAT:w" s="T358">Nada</ts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1727" n="HIAT:w" s="T359">măna</ts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1730" n="HIAT:w" s="T360">jama</ts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1733" n="HIAT:w" s="T361">šöʔsittə</ts>
                  <nts id="Seg_1734" n="HIAT:ip">.</nts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T364" id="Seg_1737" n="HIAT:u" s="T362">
                  <ts e="T363" id="Seg_1739" n="HIAT:w" s="T362">Parga</ts>
                  <nts id="Seg_1740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1742" n="HIAT:w" s="T363">šöʔsittə</ts>
                  <nts id="Seg_1743" n="HIAT:ip">.</nts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T370" id="Seg_1746" n="HIAT:u" s="T364">
                  <ts e="T365" id="Seg_1748" n="HIAT:w" s="T364">Nada</ts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1750" n="HIAT:ip">(</nts>
                  <ts e="T366" id="Seg_1752" n="HIAT:w" s="T365">kujnet</ts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1755" n="HIAT:w" s="T366">-</ts>
                  <nts id="Seg_1756" n="HIAT:ip">)</nts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1759" n="HIAT:w" s="T367">kujnek</ts>
                  <nts id="Seg_1760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1762" n="HIAT:w" s="T368">šöʔsittə</ts>
                  <nts id="Seg_1763" n="HIAT:ip">,</nts>
                  <nts id="Seg_1764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1766" n="HIAT:w" s="T369">izittə</ts>
                  <nts id="Seg_1767" n="HIAT:ip">.</nts>
                  <nts id="Seg_1768" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T375" id="Seg_1770" n="HIAT:u" s="T370">
                  <nts id="Seg_1771" n="HIAT:ip">(</nts>
                  <ts e="T371" id="Seg_1773" n="HIAT:w" s="T370">Na</ts>
                  <nts id="Seg_1774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1776" n="HIAT:w" s="T371">-</ts>
                  <nts id="Seg_1777" n="HIAT:ip">)</nts>
                  <nts id="Seg_1778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1780" n="HIAT:w" s="T372">Nada</ts>
                  <nts id="Seg_1781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1783" n="HIAT:w" s="T373">piʔmə</ts>
                  <nts id="Seg_1784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1786" n="HIAT:w" s="T374">šöʔsittə</ts>
                  <nts id="Seg_1787" n="HIAT:ip">.</nts>
                  <nts id="Seg_1788" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T382" id="Seg_1790" n="HIAT:u" s="T375">
                  <ts e="T375.tx-PKZ.1" id="Seg_1792" n="HIAT:w" s="T375">A</ts>
                  <nts id="Seg_1793" n="HIAT:ip">_</nts>
                  <ts e="T376" id="Seg_1795" n="HIAT:w" s="T375.tx-PKZ.1">to</ts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1798" n="HIAT:w" s="T376">măn</ts>
                  <nts id="Seg_1799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1801" n="HIAT:w" s="T377">nʼim</ts>
                  <nts id="Seg_1802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1804" n="HIAT:w" s="T378">bar</ts>
                  <nts id="Seg_1805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1807" n="HIAT:w" s="T379">šerzittə</ts>
                  <nts id="Seg_1808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1810" n="HIAT:w" s="T380">naga</ts>
                  <nts id="Seg_1811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1813" n="HIAT:w" s="T381">ĭmbi</ts>
                  <nts id="Seg_1814" n="HIAT:ip">.</nts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T384" id="Seg_1817" n="HIAT:u" s="T382">
                  <ts e="T383" id="Seg_1819" n="HIAT:w" s="T382">Ĭmbidə</ts>
                  <nts id="Seg_1820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1822" n="HIAT:w" s="T383">naga</ts>
                  <nts id="Seg_1823" n="HIAT:ip">.</nts>
                  <nts id="Seg_1824" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T392" id="Seg_1826" n="HIAT:u" s="T384">
                  <ts e="T385" id="Seg_1828" n="HIAT:w" s="T384">Măn</ts>
                  <nts id="Seg_1829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1831" n="HIAT:w" s="T385">kallam</ts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1834" n="HIAT:w" s="T386">Kazan</ts>
                  <nts id="Seg_1835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1837" n="HIAT:w" s="T387">turanə</ts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1840" n="HIAT:w" s="T388">i</ts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1843" n="HIAT:w" s="T389">nada</ts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1846" n="HIAT:w" s="T390">ipek</ts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1849" n="HIAT:w" s="T391">izittə</ts>
                  <nts id="Seg_1850" n="HIAT:ip">.</nts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_1853" n="HIAT:u" s="T392">
                  <ts e="T393" id="Seg_1855" n="HIAT:w" s="T392">Nada</ts>
                  <nts id="Seg_1856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1858" n="HIAT:w" s="T393">sĭreʔpne</ts>
                  <nts id="Seg_1859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1861" n="HIAT:w" s="T394">izittə</ts>
                  <nts id="Seg_1862" n="HIAT:ip">.</nts>
                  <nts id="Seg_1863" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T401" id="Seg_1865" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_1867" n="HIAT:w" s="T395">Nada</ts>
                  <nts id="Seg_1868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1869" n="HIAT:ip">(</nts>
                  <ts e="T397" id="Seg_1871" n="HIAT:w" s="T396">mul</ts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1874" n="HIAT:w" s="T397">-</ts>
                  <nts id="Seg_1875" n="HIAT:ip">)</nts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1878" n="HIAT:w" s="T398">multukdə</ts>
                  <nts id="Seg_1879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1881" n="HIAT:w" s="T399">ipek</ts>
                  <nts id="Seg_1882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1884" n="HIAT:w" s="T400">izittə</ts>
                  <nts id="Seg_1885" n="HIAT:ip">.</nts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T402" id="Seg_1888" n="HIAT:u" s="T401">
                  <ts e="T402" id="Seg_1890" n="HIAT:w" s="T401">Nada</ts>
                  <nts id="Seg_1891" n="HIAT:ip">.</nts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T571" id="Seg_1893" n="sc" s="T408">
               <ts e="T410" id="Seg_1895" n="HIAT:u" s="T408">
                  <ts e="T409" id="Seg_1897" n="HIAT:w" s="T408">No</ts>
                  <nts id="Seg_1898" n="HIAT:ip">,</nts>
                  <nts id="Seg_1899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1901" n="HIAT:w" s="T409">ipek</ts>
                  <nts id="Seg_1902" n="HIAT:ip">.</nts>
                  <nts id="Seg_1903" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T413" id="Seg_1905" n="HIAT:u" s="T410">
                  <ts e="T411" id="Seg_1907" n="HIAT:w" s="T410">Ну</ts>
                  <nts id="Seg_1908" n="HIAT:ip">,</nts>
                  <nts id="Seg_1909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1911" n="HIAT:w" s="T411">все</ts>
                  <nts id="Seg_1912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1914" n="HIAT:w" s="T412">DMG</ts>
                  <nts id="Seg_1915" n="HIAT:ip">.</nts>
                  <nts id="Seg_1916" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T415" id="Seg_1918" n="HIAT:u" s="T413">
                  <nts id="Seg_1919" n="HIAT:ip">(</nts>
                  <nts id="Seg_1920" n="HIAT:ip">(</nts>
                  <ats e="T414" id="Seg_1921" n="HIAT:non-pho" s="T413">DMG</ats>
                  <nts id="Seg_1922" n="HIAT:ip">)</nts>
                  <nts id="Seg_1923" n="HIAT:ip">)</nts>
                  <nts id="Seg_1924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1926" n="HIAT:w" s="T414">думала</ts>
                  <nts id="Seg_1927" n="HIAT:ip">.</nts>
                  <nts id="Seg_1928" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T419" id="Seg_1930" n="HIAT:u" s="T415">
                  <ts e="T416" id="Seg_1932" n="HIAT:w" s="T415">Măn</ts>
                  <nts id="Seg_1933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1935" n="HIAT:w" s="T416">dʼijenə</ts>
                  <nts id="Seg_1936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1938" n="HIAT:w" s="T417">dĭgəttə</ts>
                  <nts id="Seg_1939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1941" n="HIAT:w" s="T418">kallam</ts>
                  <nts id="Seg_1942" n="HIAT:ip">.</nts>
                  <nts id="Seg_1943" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T421" id="Seg_1945" n="HIAT:u" s="T419">
                  <ts e="T420" id="Seg_1947" n="HIAT:w" s="T419">Ну</ts>
                  <nts id="Seg_1948" n="HIAT:ip">,</nts>
                  <nts id="Seg_1949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1951" n="HIAT:w" s="T420">все</ts>
                  <nts id="Seg_1952" n="HIAT:ip">.</nts>
                  <nts id="Seg_1953" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T427" id="Seg_1955" n="HIAT:u" s="T421">
                  <ts e="T422" id="Seg_1957" n="HIAT:w" s="T421">Teinen</ts>
                  <nts id="Seg_1958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1959" n="HIAT:ip">(</nts>
                  <ts e="T423" id="Seg_1961" n="HIAT:w" s="T422">e</ts>
                  <nts id="Seg_1962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1964" n="HIAT:w" s="T423">-</ts>
                  <nts id="Seg_1965" n="HIAT:ip">)</nts>
                  <nts id="Seg_1966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1968" n="HIAT:w" s="T424">dĭzeŋ</ts>
                  <nts id="Seg_1969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1971" n="HIAT:w" s="T425">dʼijegəʔ</ts>
                  <nts id="Seg_1972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1974" n="HIAT:w" s="T426">šoləʔi</ts>
                  <nts id="Seg_1975" n="HIAT:ip">.</nts>
                  <nts id="Seg_1976" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T430" id="Seg_1978" n="HIAT:u" s="T427">
                  <ts e="T428" id="Seg_1980" n="HIAT:w" s="T427">Padʼi</ts>
                  <nts id="Seg_1981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1983" n="HIAT:w" s="T428">uja</ts>
                  <nts id="Seg_1984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1986" n="HIAT:w" s="T429">detleʔi</ts>
                  <nts id="Seg_1987" n="HIAT:ip">.</nts>
                  <nts id="Seg_1988" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T432" id="Seg_1990" n="HIAT:u" s="T430">
                  <ts e="T431" id="Seg_1992" n="HIAT:w" s="T430">Kalba</ts>
                  <nts id="Seg_1993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1995" n="HIAT:w" s="T431">deʔleʔi</ts>
                  <nts id="Seg_1996" n="HIAT:ip">.</nts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T437" id="Seg_1999" n="HIAT:u" s="T432">
                  <nts id="Seg_2000" n="HIAT:ip">(</nts>
                  <ts e="T433" id="Seg_2002" n="HIAT:w" s="T432">Tʼagarlaʔ-</ts>
                  <nts id="Seg_2003" n="HIAT:ip">)</nts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_2006" n="HIAT:w" s="T433">Tʼagarzittə</ts>
                  <nts id="Seg_2007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_2009" n="HIAT:w" s="T434">nada</ts>
                  <nts id="Seg_2010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_2012" n="HIAT:w" s="T435">da</ts>
                  <nts id="Seg_2013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_2015" n="HIAT:w" s="T436">tustʼarzittə</ts>
                  <nts id="Seg_2016" n="HIAT:ip">.</nts>
                  <nts id="Seg_2017" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T447" id="Seg_2019" n="HIAT:u" s="T437">
                  <ts e="T438" id="Seg_2021" n="HIAT:w" s="T437">Munujʔ</ts>
                  <nts id="Seg_2022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_2024" n="HIAT:w" s="T438">enzittə</ts>
                  <nts id="Seg_2025" n="HIAT:ip">,</nts>
                  <nts id="Seg_2026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_2028" n="HIAT:w" s="T439">oroma</ts>
                  <nts id="Seg_2029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_2031" n="HIAT:w" s="T440">enzittə</ts>
                  <nts id="Seg_2032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_2034" n="HIAT:w" s="T441">i</ts>
                  <nts id="Seg_2035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_2037" n="HIAT:w" s="T442">bü</ts>
                  <nts id="Seg_2038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_2040" n="HIAT:w" s="T443">kămnasʼtə</ts>
                  <nts id="Seg_2041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_2043" n="HIAT:w" s="T444">i</ts>
                  <nts id="Seg_2044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_2046" n="HIAT:w" s="T445">amorzittə</ts>
                  <nts id="Seg_2047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_2049" n="HIAT:w" s="T446">šamnakziʔ</ts>
                  <nts id="Seg_2050" n="HIAT:ip">.</nts>
                  <nts id="Seg_2051" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T456" id="Seg_2053" n="HIAT:u" s="T447">
                  <ts e="T448" id="Seg_2055" n="HIAT:w" s="T447">Nada</ts>
                  <nts id="Seg_2056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_2058" n="HIAT:w" s="T448">kanzittə</ts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2060" n="HIAT:ip">(</nts>
                  <ts e="T450" id="Seg_2062" n="HIAT:w" s="T449">tol</ts>
                  <nts id="Seg_2063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_2065" n="HIAT:w" s="T450">-</ts>
                  <nts id="Seg_2066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_2068" n="HIAT:w" s="T451">to</ts>
                  <nts id="Seg_2069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_2071" n="HIAT:w" s="T452">-</ts>
                  <nts id="Seg_2072" n="HIAT:ip">)</nts>
                  <nts id="Seg_2073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_2075" n="HIAT:w" s="T453">kanzittə</ts>
                  <nts id="Seg_2076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2077" n="HIAT:ip">(</nts>
                  <ts e="T455" id="Seg_2079" n="HIAT:w" s="T454">toli</ts>
                  <nts id="Seg_2080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_2082" n="HIAT:w" s="T455">-</ts>
                  <nts id="Seg_2083" n="HIAT:ip">)</nts>
                  <nts id="Seg_2084" n="HIAT:ip">.</nts>
                  <nts id="Seg_2085" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T458" id="Seg_2087" n="HIAT:u" s="T456">
                  <ts e="T458" id="Seg_2089" n="HIAT:w" s="T456">Погоди</ts>
                  <nts id="Seg_2090" n="HIAT:ip">…</nts>
                  <nts id="Seg_2091" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T461" id="Seg_2093" n="HIAT:u" s="T458">
                  <ts e="T459" id="Seg_2095" n="HIAT:w" s="T458">Talerzittə</ts>
                  <nts id="Seg_2096" n="HIAT:ip">,</nts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_2099" n="HIAT:w" s="T459">ipek</ts>
                  <nts id="Seg_2100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_2102" n="HIAT:w" s="T460">kuʔsittə</ts>
                  <nts id="Seg_2103" n="HIAT:ip">.</nts>
                  <nts id="Seg_2104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T465" id="Seg_2106" n="HIAT:u" s="T461">
                  <ts e="T462" id="Seg_2108" n="HIAT:w" s="T461">Dĭgəttə</ts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_2111" n="HIAT:w" s="T462">özerləj</ts>
                  <nts id="Seg_2112" n="HIAT:ip">,</nts>
                  <nts id="Seg_2113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_2115" n="HIAT:w" s="T463">pădəsʼtə</ts>
                  <nts id="Seg_2116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_2118" n="HIAT:w" s="T464">nada</ts>
                  <nts id="Seg_2119" n="HIAT:ip">.</nts>
                  <nts id="Seg_2120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T468" id="Seg_2122" n="HIAT:u" s="T465">
                  <ts e="T466" id="Seg_2124" n="HIAT:w" s="T465">Dĭgəttə</ts>
                  <nts id="Seg_2125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_2127" n="HIAT:w" s="T466">toʔnarzittə</ts>
                  <nts id="Seg_2128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_2130" n="HIAT:w" s="T467">nada</ts>
                  <nts id="Seg_2131" n="HIAT:ip">.</nts>
                  <nts id="Seg_2132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T478" id="Seg_2134" n="HIAT:u" s="T468">
                  <ts e="T469" id="Seg_2136" n="HIAT:w" s="T468">Dĭgəttə</ts>
                  <nts id="Seg_2137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2138" n="HIAT:ip">(</nts>
                  <nts id="Seg_2139" n="HIAT:ip">(</nts>
                  <ats e="T470" id="Seg_2140" n="HIAT:non-pho" s="T469">PAUSE</ats>
                  <nts id="Seg_2141" n="HIAT:ip">)</nts>
                  <nts id="Seg_2142" n="HIAT:ip">)</nts>
                  <nts id="Seg_2143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_2145" n="HIAT:w" s="T470">Погоди</ts>
                  <nts id="Seg_2146" n="HIAT:ip">,</nts>
                  <nts id="Seg_2147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_2149" n="HIAT:w" s="T471">вот</ts>
                  <nts id="Seg_2150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2151" n="HIAT:ip">(</nts>
                  <nts id="Seg_2152" n="HIAT:ip">(</nts>
                  <ats e="T474" id="Seg_2153" n="HIAT:non-pho" s="T473">DMG</ats>
                  <nts id="Seg_2154" n="HIAT:ip">)</nts>
                  <nts id="Seg_2155" n="HIAT:ip">)</nts>
                  <nts id="Seg_2156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_2158" n="HIAT:w" s="T474">пустил</ts>
                  <nts id="Seg_2159" n="HIAT:ip">,</nts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_2162" n="HIAT:w" s="T475">dĭgəttə</ts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_2165" n="HIAT:w" s="T476">nʼeʔsittə</ts>
                  <nts id="Seg_2166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_2168" n="HIAT:w" s="T477">nada</ts>
                  <nts id="Seg_2169" n="HIAT:ip">.</nts>
                  <nts id="Seg_2170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T483" id="Seg_2172" n="HIAT:u" s="T478">
                  <nts id="Seg_2173" n="HIAT:ip">(</nts>
                  <ts e="T479" id="Seg_2175" n="HIAT:w" s="T478">Dĭgət</ts>
                  <nts id="Seg_2176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_2178" n="HIAT:w" s="T479">-</ts>
                  <nts id="Seg_2179" n="HIAT:ip">)</nts>
                  <nts id="Seg_2180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_2182" n="HIAT:w" s="T480">Dĭgəttə</ts>
                  <nts id="Seg_2183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_2185" n="HIAT:w" s="T481">nada</ts>
                  <nts id="Seg_2186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_2188" n="HIAT:w" s="T482">pürzittə</ts>
                  <nts id="Seg_2189" n="HIAT:ip">.</nts>
                  <nts id="Seg_2190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T486" id="Seg_2192" n="HIAT:u" s="T483">
                  <ts e="T484" id="Seg_2194" n="HIAT:w" s="T483">Dĭgəttə</ts>
                  <nts id="Seg_2195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_2197" n="HIAT:w" s="T484">amorzittə</ts>
                  <nts id="Seg_2198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_2200" n="HIAT:w" s="T485">nada</ts>
                  <nts id="Seg_2201" n="HIAT:ip">.</nts>
                  <nts id="Seg_2202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T488" id="Seg_2204" n="HIAT:u" s="T486">
                  <ts e="T488" id="Seg_2206" n="HIAT:w" s="T486">Вот</ts>
                  <nts id="Seg_2207" n="HIAT:ip">…</nts>
                  <nts id="Seg_2208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T496" id="Seg_2210" n="HIAT:u" s="T488">
                  <ts e="T489" id="Seg_2212" n="HIAT:w" s="T488">Teinen</ts>
                  <nts id="Seg_2213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_2215" n="HIAT:w" s="T489">urgo</ts>
                  <nts id="Seg_2216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_2218" n="HIAT:w" s="T490">dʼala</ts>
                  <nts id="Seg_2219" n="HIAT:ip">,</nts>
                  <nts id="Seg_2220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2221" n="HIAT:ip">(</nts>
                  <ts e="T492" id="Seg_2223" n="HIAT:w" s="T491">i</ts>
                  <nts id="Seg_2224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_2226" n="HIAT:w" s="T492">-</ts>
                  <nts id="Seg_2227" n="HIAT:ip">)</nts>
                  <nts id="Seg_2228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_2230" n="HIAT:w" s="T493">il</ts>
                  <nts id="Seg_2231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_2233" n="HIAT:w" s="T494">ugaːndə</ts>
                  <nts id="Seg_2234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_2236" n="HIAT:w" s="T495">iʔgö</ts>
                  <nts id="Seg_2237" n="HIAT:ip">.</nts>
                  <nts id="Seg_2238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T498" id="Seg_2240" n="HIAT:u" s="T496">
                  <ts e="T497" id="Seg_2242" n="HIAT:w" s="T496">Kuvasəʔi</ts>
                  <nts id="Seg_2243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_2245" n="HIAT:w" s="T497">bar</ts>
                  <nts id="Seg_2246" n="HIAT:ip">.</nts>
                  <nts id="Seg_2247" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T500" id="Seg_2249" n="HIAT:u" s="T498">
                  <ts e="T499" id="Seg_2251" n="HIAT:w" s="T498">Ara</ts>
                  <nts id="Seg_2252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_2254" n="HIAT:w" s="T499">bĭtleʔbəʔjə</ts>
                  <nts id="Seg_2255" n="HIAT:ip">.</nts>
                  <nts id="Seg_2256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T502" id="Seg_2258" n="HIAT:u" s="T500">
                  <ts e="T501" id="Seg_2260" n="HIAT:w" s="T500">Sʼarlaʔbəʔjə</ts>
                  <nts id="Seg_2261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_2263" n="HIAT:w" s="T501">гармонию</ts>
                  <nts id="Seg_2264" n="HIAT:ip">.</nts>
                  <nts id="Seg_2265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T503" id="Seg_2267" n="HIAT:u" s="T502">
                  <ts e="T503" id="Seg_2269" n="HIAT:w" s="T502">Suʔmiːleʔbəʔjə</ts>
                  <nts id="Seg_2270" n="HIAT:ip">.</nts>
                  <nts id="Seg_2271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T505" id="Seg_2273" n="HIAT:u" s="T503">
                  <ts e="T504" id="Seg_2275" n="HIAT:w" s="T503">Nüjnə</ts>
                  <nts id="Seg_2276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_2278" n="HIAT:w" s="T504">nüjleʔbəʔjə</ts>
                  <nts id="Seg_2279" n="HIAT:ip">.</nts>
                  <nts id="Seg_2280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T507" id="Seg_2282" n="HIAT:u" s="T505">
                  <ts e="T506" id="Seg_2284" n="HIAT:w" s="T505">Jakšə</ts>
                  <nts id="Seg_2285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_2287" n="HIAT:w" s="T506">nüke</ts>
                  <nts id="Seg_2288" n="HIAT:ip">!</nts>
                  <nts id="Seg_2289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T513" id="Seg_2291" n="HIAT:u" s="T507">
                  <ts e="T508" id="Seg_2293" n="HIAT:w" s="T507">Ipek</ts>
                  <nts id="Seg_2294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_2296" n="HIAT:w" s="T508">bar</ts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2298" n="HIAT:ip">(</nts>
                  <ts e="T510" id="Seg_2300" n="HIAT:w" s="T509">pür</ts>
                  <nts id="Seg_2301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_2303" n="HIAT:w" s="T510">-</ts>
                  <nts id="Seg_2304" n="HIAT:ip">)</nts>
                  <nts id="Seg_2305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_2307" n="HIAT:w" s="T511">pürbi</ts>
                  <nts id="Seg_2308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_2310" n="HIAT:w" s="T512">jakšə</ts>
                  <nts id="Seg_2311" n="HIAT:ip">.</nts>
                  <nts id="Seg_2312" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T514" id="Seg_2314" n="HIAT:u" s="T513">
                  <ts e="T514" id="Seg_2316" n="HIAT:w" s="T513">Nömər</ts>
                  <nts id="Seg_2317" n="HIAT:ip">.</nts>
                  <nts id="Seg_2318" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T520" id="Seg_2320" n="HIAT:u" s="T514">
                  <ts e="T515" id="Seg_2322" n="HIAT:w" s="T514">Tüjö</ts>
                  <nts id="Seg_2323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2324" n="HIAT:ip">(</nts>
                  <ts e="T516" id="Seg_2326" n="HIAT:w" s="T515">amo</ts>
                  <nts id="Seg_2327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_2329" n="HIAT:w" s="T516">-</ts>
                  <nts id="Seg_2330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_2332" n="HIAT:w" s="T517">amo</ts>
                  <nts id="Seg_2333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_2335" n="HIAT:w" s="T518">-</ts>
                  <nts id="Seg_2336" n="HIAT:ip">)</nts>
                  <nts id="Seg_2337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_2339" n="HIAT:w" s="T519">amorgaʔ</ts>
                  <nts id="Seg_2340" n="HIAT:ip">!</nts>
                  <nts id="Seg_2341" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T522" id="Seg_2343" n="HIAT:u" s="T520">
                  <ts e="T521" id="Seg_2345" n="HIAT:w" s="T520">Padʼi</ts>
                  <nts id="Seg_2346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_2348" n="HIAT:w" s="T521">püjöliaʔi</ts>
                  <nts id="Seg_2349" n="HIAT:ip">.</nts>
                  <nts id="Seg_2350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T524" id="Seg_2352" n="HIAT:u" s="T522">
                  <ts e="T523" id="Seg_2354" n="HIAT:w" s="T522">Tüjö</ts>
                  <nts id="Seg_2355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_2357" n="HIAT:w" s="T523">uʔblam</ts>
                  <nts id="Seg_2358" n="HIAT:ip">.</nts>
                  <nts id="Seg_2359" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T527" id="Seg_2361" n="HIAT:u" s="T524">
                  <ts e="T525" id="Seg_2363" n="HIAT:w" s="T524">Amgaʔ</ts>
                  <nts id="Seg_2364" n="HIAT:ip">,</nts>
                  <nts id="Seg_2365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2366" n="HIAT:ip">(</nts>
                  <ts e="T526" id="Seg_2368" n="HIAT:w" s="T525">amno=</ts>
                  <nts id="Seg_2369" n="HIAT:ip">)</nts>
                  <nts id="Seg_2370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_2372" n="HIAT:w" s="T526">amnogaʔ</ts>
                  <nts id="Seg_2373" n="HIAT:ip">!</nts>
                  <nts id="Seg_2374" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T530" id="Seg_2376" n="HIAT:u" s="T527">
                  <ts e="T528" id="Seg_2378" n="HIAT:w" s="T527">Nʼešpək</ts>
                  <nts id="Seg_2379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_2381" n="HIAT:w" s="T528">kuza</ts>
                  <nts id="Seg_2382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_2384" n="HIAT:w" s="T529">ambi</ts>
                  <nts id="Seg_2385" n="HIAT:ip">.</nts>
                  <nts id="Seg_2386" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T533" id="Seg_2388" n="HIAT:u" s="T530">
                  <ts e="T531" id="Seg_2390" n="HIAT:w" s="T530">Ipek</ts>
                  <nts id="Seg_2391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_2393" n="HIAT:w" s="T531">lem</ts>
                  <nts id="Seg_2394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_2396" n="HIAT:w" s="T532">keʔbdeziʔ</ts>
                  <nts id="Seg_2397" n="HIAT:ip">.</nts>
                  <nts id="Seg_2398" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T535" id="Seg_2400" n="HIAT:u" s="T533">
                  <ts e="T534" id="Seg_2402" n="HIAT:w" s="T533">Oroma</ts>
                  <nts id="Seg_2403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_2405" n="HIAT:w" s="T534">nuldəbiam</ts>
                  <nts id="Seg_2406" n="HIAT:ip">.</nts>
                  <nts id="Seg_2407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T541" id="Seg_2409" n="HIAT:u" s="T535">
                  <ts e="T536" id="Seg_2411" n="HIAT:w" s="T535">Amorgaʔ</ts>
                  <nts id="Seg_2412" n="HIAT:ip">,</nts>
                  <nts id="Seg_2413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2414" n="HIAT:ip">(</nts>
                  <ts e="T537" id="Seg_2416" n="HIAT:w" s="T536">šut</ts>
                  <nts id="Seg_2417" n="HIAT:ip">)</nts>
                  <nts id="Seg_2418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2420" n="HIAT:w" s="T537">süt</ts>
                  <nts id="Seg_2421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2422" n="HIAT:ip">(</nts>
                  <ts e="T539" id="Seg_2424" n="HIAT:w" s="T538">bĭt</ts>
                  <nts id="Seg_2425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_2427" n="HIAT:w" s="T539">-</ts>
                  <nts id="Seg_2428" n="HIAT:ip">)</nts>
                  <nts id="Seg_2429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_2431" n="HIAT:w" s="T540">bĭttə</ts>
                  <nts id="Seg_2432" n="HIAT:ip">!</nts>
                  <nts id="Seg_2433" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T543" id="Seg_2435" n="HIAT:u" s="T541">
                  <ts e="T543" id="Seg_2437" n="HIAT:w" s="T541">Всё</ts>
                  <nts id="Seg_2438" n="HIAT:ip">…</nts>
                  <nts id="Seg_2439" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T547" id="Seg_2441" n="HIAT:u" s="T543">
                  <ts e="T544" id="Seg_2443" n="HIAT:w" s="T543">Tăn</ts>
                  <nts id="Seg_2444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2446" n="HIAT:w" s="T544">iššo</ts>
                  <nts id="Seg_2447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2449" n="HIAT:w" s="T545">kunoːllaʔbəl</ts>
                  <nts id="Seg_2450" n="HIAT:ip">,</nts>
                  <nts id="Seg_2451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2453" n="HIAT:w" s="T546">uʔbdaʔ</ts>
                  <nts id="Seg_2454" n="HIAT:ip">!</nts>
                  <nts id="Seg_2455" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T554" id="Seg_2457" n="HIAT:u" s="T547">
                  <ts e="T547.tx-PKZ.1" id="Seg_2459" n="HIAT:w" s="T547">A</ts>
                  <nts id="Seg_2460" n="HIAT:ip">_</nts>
                  <ts e="T548" id="Seg_2462" n="HIAT:w" s="T547.tx-PKZ.1">to</ts>
                  <nts id="Seg_2463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2465" n="HIAT:w" s="T548">šĭšəge</ts>
                  <nts id="Seg_2466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2468" n="HIAT:w" s="T549">bü</ts>
                  <nts id="Seg_2469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2471" n="HIAT:w" s="T550">detləm</ts>
                  <nts id="Seg_2472" n="HIAT:ip">,</nts>
                  <nts id="Seg_2473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2475" n="HIAT:w" s="T551">tănan</ts>
                  <nts id="Seg_2476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2478" n="HIAT:w" s="T552">kămnam</ts>
                  <nts id="Seg_2479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2481" n="HIAT:w" s="T553">bar</ts>
                  <nts id="Seg_2482" n="HIAT:ip">.</nts>
                  <nts id="Seg_2483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T557" id="Seg_2485" n="HIAT:u" s="T554">
                  <ts e="T555" id="Seg_2487" n="HIAT:w" s="T554">Dĭgəttə</ts>
                  <nts id="Seg_2488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2490" n="HIAT:w" s="T555">bar</ts>
                  <nts id="Seg_2491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2493" n="HIAT:w" s="T556">süʔməluʔləl</ts>
                  <nts id="Seg_2494" n="HIAT:ip">.</nts>
                  <nts id="Seg_2495" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T559" id="Seg_2497" n="HIAT:u" s="T557">
                  <ts e="T558" id="Seg_2499" n="HIAT:w" s="T557">Всё</ts>
                  <nts id="Seg_2500" n="HIAT:ip">,</nts>
                  <nts id="Seg_2501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2503" n="HIAT:w" s="T558">наверное</ts>
                  <nts id="Seg_2504" n="HIAT:ip">.</nts>
                  <nts id="Seg_2505" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T564" id="Seg_2507" n="HIAT:u" s="T559">
                  <ts e="T560" id="Seg_2509" n="HIAT:w" s="T559">Погоди</ts>
                  <nts id="Seg_2510" n="HIAT:ip">,</nts>
                  <nts id="Seg_2511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2513" n="HIAT:w" s="T560">погоди</ts>
                  <nts id="Seg_2514" n="HIAT:ip">,</nts>
                  <nts id="Seg_2515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2517" n="HIAT:w" s="T561">как</ts>
                  <nts id="Seg_2518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2520" n="HIAT:w" s="T562">нас</ts>
                  <nts id="Seg_2521" n="HIAT:ip">…</nts>
                  <nts id="Seg_2522" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T566" id="Seg_2524" n="HIAT:u" s="T564">
                  <nts id="Seg_2525" n="HIAT:ip">(</nts>
                  <nts id="Seg_2526" n="HIAT:ip">(</nts>
                  <ats e="T565" id="Seg_2527" n="HIAT:non-pho" s="T564">DMG</ats>
                  <nts id="Seg_2528" n="HIAT:ip">)</nts>
                  <nts id="Seg_2529" n="HIAT:ip">)</nts>
                  <nts id="Seg_2530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2531" n="HIAT:ip">(</nts>
                  <ts e="T566" id="Seg_2533" n="HIAT:w" s="T565">-dot</ts>
                  <nts id="Seg_2534" n="HIAT:ip">)</nts>
                  <nts id="Seg_2535" n="HIAT:ip">.</nts>
                  <nts id="Seg_2536" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T568" id="Seg_2538" n="HIAT:u" s="T566">
                  <ts e="T567" id="Seg_2540" n="HIAT:w" s="T566">Ej</ts>
                  <nts id="Seg_2541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2543" n="HIAT:w" s="T567">nʼilgöt</ts>
                  <nts id="Seg_2544" n="HIAT:ip">!</nts>
                  <nts id="Seg_2545" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T570" id="Seg_2547" n="HIAT:u" s="T568">
                  <nts id="Seg_2548" n="HIAT:ip">(</nts>
                  <ts e="T569" id="Seg_2550" n="HIAT:w" s="T568">Iʔ</ts>
                  <nts id="Seg_2551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2553" n="HIAT:w" s="T569">jaʔ</ts>
                  <nts id="Seg_2554" n="HIAT:ip">)</nts>
                  <nts id="Seg_2555" n="HIAT:ip">!</nts>
                  <nts id="Seg_2556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T571" id="Seg_2558" n="HIAT:u" s="T570">
                  <ts e="T571" id="Seg_2560" n="HIAT:w" s="T570">Господи</ts>
                  <nts id="Seg_2561" n="HIAT:ip">.</nts>
                  <nts id="Seg_2562" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T575" id="Seg_2563" n="sc" s="T573">
               <ts e="T575" id="Seg_2565" n="HIAT:u" s="T573">
                  <ts e="T574" id="Seg_2567" n="HIAT:w" s="T573">Iʔ</ts>
                  <nts id="Seg_2568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2570" n="HIAT:w" s="T574">dʼăbaktəraʔ</ts>
                  <nts id="Seg_2571" n="HIAT:ip">!</nts>
                  <nts id="Seg_2572" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T579" id="Seg_2573" n="sc" s="T577">
               <ts e="T579" id="Seg_2575" n="HIAT:u" s="T577">
                  <ts e="T578" id="Seg_2577" n="HIAT:w" s="T577">Iʔ</ts>
                  <nts id="Seg_2578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2580" n="HIAT:w" s="T578">kudonzaʔ</ts>
                  <nts id="Seg_2581" n="HIAT:ip">!</nts>
                  <nts id="Seg_2582" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T584" id="Seg_2583" n="sc" s="T582">
               <ts e="T584" id="Seg_2585" n="HIAT:u" s="T582">
                  <ts e="T583" id="Seg_2587" n="HIAT:w" s="T582">Iʔ</ts>
                  <nts id="Seg_2588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2590" n="HIAT:w" s="T583">kirgaːraʔ</ts>
                  <nts id="Seg_2591" n="HIAT:ip">!</nts>
                  <nts id="Seg_2592" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T590" id="Seg_2593" n="sc" s="T586">
               <ts e="T588" id="Seg_2595" n="HIAT:u" s="T586">
                  <ts e="T587" id="Seg_2597" n="HIAT:w" s="T586">Kuʔlə</ts>
                  <nts id="Seg_2598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2600" n="HIAT:w" s="T587">mʼaŋŋaʔbə</ts>
                  <nts id="Seg_2601" n="HIAT:ip">.</nts>
                  <nts id="Seg_2602" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T590" id="Seg_2604" n="HIAT:u" s="T588">
                  <ts e="T589" id="Seg_2606" n="HIAT:w" s="T588">Kĭški-t</ts>
                  <nts id="Seg_2607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2609" n="HIAT:w" s="T589">kuʔlə</ts>
                  <nts id="Seg_2610" n="HIAT:ip">.</nts>
                  <nts id="Seg_2611" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T605" id="Seg_2612" n="sc" s="T592">
               <ts e="T594" id="Seg_2614" n="HIAT:u" s="T592">
                  <ts e="T593" id="Seg_2616" n="HIAT:w" s="T592">Iʔ</ts>
                  <nts id="Seg_2617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2619" n="HIAT:w" s="T593">nuʔməʔ</ts>
                  <nts id="Seg_2620" n="HIAT:ip">!</nts>
                  <nts id="Seg_2621" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T596" id="Seg_2623" n="HIAT:u" s="T594">
                  <ts e="T595" id="Seg_2625" n="HIAT:w" s="T594">Iʔ</ts>
                  <nts id="Seg_2626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2628" n="HIAT:w" s="T595">suʔmiʔ</ts>
                  <nts id="Seg_2629" n="HIAT:ip">!</nts>
                  <nts id="Seg_2630" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T598" id="Seg_2632" n="HIAT:u" s="T596">
                  <ts e="T597" id="Seg_2634" n="HIAT:w" s="T596">Iʔ</ts>
                  <nts id="Seg_2635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2637" n="HIAT:w" s="T597">nüjnəʔ</ts>
                  <nts id="Seg_2638" n="HIAT:ip">!</nts>
                  <nts id="Seg_2639" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T600" id="Seg_2641" n="HIAT:u" s="T598">
                  <ts e="T599" id="Seg_2643" n="HIAT:w" s="T598">Ugaːndə</ts>
                  <nts id="Seg_2644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2646" n="HIAT:w" s="T599">jakšə</ts>
                  <nts id="Seg_2647" n="HIAT:ip">.</nts>
                  <nts id="Seg_2648" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T602" id="Seg_2650" n="HIAT:u" s="T600">
                  <ts e="T601" id="Seg_2652" n="HIAT:w" s="T600">Ugaːndə</ts>
                  <nts id="Seg_2653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2655" n="HIAT:w" s="T601">kuvas</ts>
                  <nts id="Seg_2656" n="HIAT:ip">.</nts>
                  <nts id="Seg_2657" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T604" id="Seg_2659" n="HIAT:u" s="T602">
                  <ts e="T603" id="Seg_2661" n="HIAT:w" s="T602">Ugaːndə</ts>
                  <nts id="Seg_2662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2664" n="HIAT:w" s="T603">komu</ts>
                  <nts id="Seg_2665" n="HIAT:ip">.</nts>
                  <nts id="Seg_2666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T605" id="Seg_2668" n="HIAT:u" s="T604">
                  <ts e="T605" id="Seg_2670" n="HIAT:w" s="T604">Ugaːndə</ts>
                  <nts id="Seg_2671" n="HIAT:ip">.</nts>
                  <nts id="Seg_2672" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T609" id="Seg_2673" n="sc" s="T606">
               <ts e="T609" id="Seg_2675" n="HIAT:u" s="T606">
                  <ts e="T607" id="Seg_2677" n="HIAT:w" s="T606">Kəbo</ts>
                  <nts id="Seg_2678" n="HIAT:ip">,</nts>
                  <nts id="Seg_2679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2681" n="HIAT:w" s="T607">ugaːndə</ts>
                  <nts id="Seg_2682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2684" n="HIAT:w" s="T608">numo</ts>
                  <nts id="Seg_2685" n="HIAT:ip">.</nts>
                  <nts id="Seg_2686" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T642" id="Seg_2687" n="sc" s="T612">
               <ts e="T614" id="Seg_2689" n="HIAT:u" s="T612">
                  <ts e="T613" id="Seg_2691" n="HIAT:w" s="T612">Ugaːndə</ts>
                  <nts id="Seg_2692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2694" n="HIAT:w" s="T613">üdʼüge</ts>
                  <nts id="Seg_2695" n="HIAT:ip">.</nts>
                  <nts id="Seg_2696" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T618" id="Seg_2698" n="HIAT:u" s="T614">
                  <ts e="T615" id="Seg_2700" n="HIAT:w" s="T614">Ну</ts>
                  <nts id="Seg_2701" n="HIAT:ip">,</nts>
                  <nts id="Seg_2702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2704" n="HIAT:w" s="T615">теперь</ts>
                  <nts id="Seg_2705" n="HIAT:ip">,</nts>
                  <nts id="Seg_2706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2708" n="HIAT:w" s="T616">наверное</ts>
                  <nts id="Seg_2709" n="HIAT:ip">,</nts>
                  <nts id="Seg_2710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2713" n="HIAT:w" s="T617">всё</ts>
                  <nts id="Seg_2714" n="HIAT:ip">.</nts>
                  <nts id="Seg_2715" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T621" id="Seg_2717" n="HIAT:u" s="T618">
                  <ts e="T619" id="Seg_2719" n="HIAT:w" s="T618">Padʼi</ts>
                  <nts id="Seg_2720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2722" n="HIAT:w" s="T619">plat</ts>
                  <nts id="Seg_2723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2725" n="HIAT:w" s="T620">tojirbial</ts>
                  <nts id="Seg_2726" n="HIAT:ip">.</nts>
                  <nts id="Seg_2727" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T623" id="Seg_2729" n="HIAT:u" s="T621">
                  <ts e="T621.tx-PKZ.1" id="Seg_2731" n="HIAT:w" s="T621">Na</ts>
                  <nts id="Seg_2732" n="HIAT:ip">_</nts>
                  <ts e="T622" id="Seg_2734" n="HIAT:w" s="T621.tx-PKZ.1">što</ts>
                  <nts id="Seg_2735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2737" n="HIAT:w" s="T622">deʔpiel</ts>
                  <nts id="Seg_2738" n="HIAT:ip">?</nts>
                  <nts id="Seg_2739" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T626" id="Seg_2741" n="HIAT:u" s="T623">
                  <ts e="T624" id="Seg_2743" n="HIAT:w" s="T623">Măn</ts>
                  <nts id="Seg_2744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2746" n="HIAT:w" s="T624">ej</ts>
                  <nts id="Seg_2747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2749" n="HIAT:w" s="T625">ilim</ts>
                  <nts id="Seg_2750" n="HIAT:ip">.</nts>
                  <nts id="Seg_2751" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T633" id="Seg_2753" n="HIAT:u" s="T626">
                  <ts e="T627" id="Seg_2755" n="HIAT:w" s="T626">Nʼe</ts>
                  <nts id="Seg_2756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2758" n="HIAT:w" s="T627">nada</ts>
                  <nts id="Seg_2759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2761" n="HIAT:w" s="T628">tojirzittə</ts>
                  <nts id="Seg_2762" n="HIAT:ip">,</nts>
                  <nts id="Seg_2763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629.tx-PKZ.1" id="Seg_2765" n="HIAT:w" s="T629">a</ts>
                  <nts id="Seg_2766" n="HIAT:ip">_</nts>
                  <ts e="T630" id="Seg_2768" n="HIAT:w" s="T629.tx-PKZ.1">to</ts>
                  <nts id="Seg_2769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2771" n="HIAT:w" s="T630">ej</ts>
                  <nts id="Seg_2772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2774" n="HIAT:w" s="T631">jakšə</ts>
                  <nts id="Seg_2775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2777" n="HIAT:w" s="T632">moləj</ts>
                  <nts id="Seg_2778" n="HIAT:ip">.</nts>
                  <nts id="Seg_2779" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T638" id="Seg_2781" n="HIAT:u" s="T633">
                  <ts e="T634" id="Seg_2783" n="HIAT:w" s="T633">Udam</ts>
                  <nts id="Seg_2784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2786" n="HIAT:w" s="T634">băʔpiam</ts>
                  <nts id="Seg_2787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2789" n="HIAT:w" s="T635">bar</ts>
                  <nts id="Seg_2790" n="HIAT:ip">,</nts>
                  <nts id="Seg_2791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2793" n="HIAT:w" s="T636">kem</ts>
                  <nts id="Seg_2794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2796" n="HIAT:w" s="T637">mʼaŋŋaʔbə</ts>
                  <nts id="Seg_2797" n="HIAT:ip">.</nts>
                  <nts id="Seg_2798" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T642" id="Seg_2800" n="HIAT:u" s="T638">
                  <ts e="T639" id="Seg_2802" n="HIAT:w" s="T638">Deʔ</ts>
                  <nts id="Seg_2803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2805" n="HIAT:w" s="T639">ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_2806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2808" n="HIAT:w" s="T640">udam</ts>
                  <nts id="Seg_2809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2811" n="HIAT:w" s="T641">sarzittə</ts>
                  <nts id="Seg_2812" n="HIAT:ip">!</nts>
                  <nts id="Seg_2813" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T766" id="Seg_2814" n="sc" s="T645">
               <ts e="T650" id="Seg_2816" n="HIAT:u" s="T645">
                  <ts e="T645.tx-PKZ.1" id="Seg_2818" n="HIAT:w" s="T645">Na</ts>
                  <nts id="Seg_2819" n="HIAT:ip">_</nts>
                  <ts e="T647" id="Seg_2821" n="HIAT:w" s="T645.tx-PKZ.1">što</ts>
                  <nts id="Seg_2822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2823" n="HIAT:ip">(</nts>
                  <ts e="T648" id="Seg_2825" n="HIAT:w" s="T647">tagajziʔ=</ts>
                  <nts id="Seg_2826" n="HIAT:ip">)</nts>
                  <nts id="Seg_2827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2829" n="HIAT:w" s="T648">tagaj</ts>
                  <nts id="Seg_2830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2832" n="HIAT:w" s="T649">ibiel</ts>
                  <nts id="Seg_2833" n="HIAT:ip">?</nts>
                  <nts id="Seg_2834" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T654" id="Seg_2836" n="HIAT:u" s="T650">
                  <ts e="T651" id="Seg_2838" n="HIAT:w" s="T650">Tănan</ts>
                  <nts id="Seg_2839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2841" n="HIAT:w" s="T651">iššo</ts>
                  <nts id="Seg_2842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2844" n="HIAT:w" s="T652">nada</ts>
                  <nts id="Seg_2845" n="HIAT:ip">…</nts>
                  <nts id="Seg_2846" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T658" id="Seg_2848" n="HIAT:u" s="T654">
                  <ts e="T655" id="Seg_2850" n="HIAT:w" s="T654">Beržə</ts>
                  <nts id="Seg_2851" n="HIAT:ip">,</nts>
                  <nts id="Seg_2852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2854" n="HIAT:w" s="T655">surno</ts>
                  <nts id="Seg_2855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2857" n="HIAT:w" s="T656">bar</ts>
                  <nts id="Seg_2858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2860" n="HIAT:w" s="T657">šonəga</ts>
                  <nts id="Seg_2861" n="HIAT:ip">.</nts>
                  <nts id="Seg_2862" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T662" id="Seg_2864" n="HIAT:u" s="T658">
                  <ts e="T659" id="Seg_2866" n="HIAT:w" s="T658">Sĭre</ts>
                  <nts id="Seg_2867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2869" n="HIAT:w" s="T659">bar</ts>
                  <nts id="Seg_2870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2872" n="HIAT:w" s="T660">šonəga</ts>
                  <nts id="Seg_2873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2875" n="HIAT:w" s="T661">beržəziʔ</ts>
                  <nts id="Seg_2876" n="HIAT:ip">.</nts>
                  <nts id="Seg_2877" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T664" id="Seg_2879" n="HIAT:u" s="T662">
                  <ts e="T663" id="Seg_2881" n="HIAT:w" s="T662">Ugaːndə</ts>
                  <nts id="Seg_2882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2884" n="HIAT:w" s="T663">šĭšəge</ts>
                  <nts id="Seg_2885" n="HIAT:ip">.</nts>
                  <nts id="Seg_2886" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T667" id="Seg_2888" n="HIAT:u" s="T664">
                  <ts e="T665" id="Seg_2890" n="HIAT:w" s="T664">Măn</ts>
                  <nts id="Seg_2891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2893" n="HIAT:w" s="T665">ugaːndə</ts>
                  <nts id="Seg_2894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2896" n="HIAT:w" s="T666">kămbiam</ts>
                  <nts id="Seg_2897" n="HIAT:ip">.</nts>
                  <nts id="Seg_2898" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T670" id="Seg_2900" n="HIAT:u" s="T667">
                  <ts e="T668" id="Seg_2902" n="HIAT:w" s="T667">Da</ts>
                  <nts id="Seg_2903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2905" n="HIAT:w" s="T668">tăŋ</ts>
                  <nts id="Seg_2906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2908" n="HIAT:w" s="T669">kămbiam</ts>
                  <nts id="Seg_2909" n="HIAT:ip">.</nts>
                  <nts id="Seg_2910" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T672" id="Seg_2912" n="HIAT:u" s="T670">
                  <ts e="T671" id="Seg_2914" n="HIAT:w" s="T670">Ujum</ts>
                  <nts id="Seg_2915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2917" n="HIAT:w" s="T671">kămbi</ts>
                  <nts id="Seg_2918" n="HIAT:ip">.</nts>
                  <nts id="Seg_2919" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T674" id="Seg_2921" n="HIAT:u" s="T672">
                  <ts e="T673" id="Seg_2923" n="HIAT:w" s="T672">Udam</ts>
                  <nts id="Seg_2924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2926" n="HIAT:w" s="T673">kămbi</ts>
                  <nts id="Seg_2927" n="HIAT:ip">.</nts>
                  <nts id="Seg_2928" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T677" id="Seg_2930" n="HIAT:u" s="T674">
                  <ts e="T675" id="Seg_2932" n="HIAT:w" s="T674">Kadəlbə</ts>
                  <nts id="Seg_2933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2935" n="HIAT:w" s="T675">bar</ts>
                  <nts id="Seg_2936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2938" n="HIAT:w" s="T676">kămbi</ts>
                  <nts id="Seg_2939" n="HIAT:ip">.</nts>
                  <nts id="Seg_2940" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T682" id="Seg_2942" n="HIAT:u" s="T677">
                  <ts e="T678" id="Seg_2944" n="HIAT:w" s="T677">Teinen</ts>
                  <nts id="Seg_2945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2946" n="HIAT:ip">(</nts>
                  <ts e="T679" id="Seg_2948" n="HIAT:w" s="T678">jak</ts>
                  <nts id="Seg_2949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2951" n="HIAT:w" s="T679">-</ts>
                  <nts id="Seg_2952" n="HIAT:ip">)</nts>
                  <nts id="Seg_2953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2955" n="HIAT:w" s="T680">kuvas</ts>
                  <nts id="Seg_2956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2958" n="HIAT:w" s="T681">dʼala</ts>
                  <nts id="Seg_2959" n="HIAT:ip">.</nts>
                  <nts id="Seg_2960" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T685" id="Seg_2962" n="HIAT:u" s="T682">
                  <ts e="T683" id="Seg_2964" n="HIAT:w" s="T682">Kuja</ts>
                  <nts id="Seg_2965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2967" n="HIAT:w" s="T683">bar</ts>
                  <nts id="Seg_2968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2970" n="HIAT:w" s="T684">măndolaʔbə</ts>
                  <nts id="Seg_2971" n="HIAT:ip">.</nts>
                  <nts id="Seg_2972" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T688" id="Seg_2974" n="HIAT:u" s="T685">
                  <ts e="T686" id="Seg_2976" n="HIAT:w" s="T685">Nʼuʔnun</ts>
                  <nts id="Seg_2977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2979" n="HIAT:w" s="T686">ĭmbidə</ts>
                  <nts id="Seg_2980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2982" n="HIAT:w" s="T687">naga</ts>
                  <nts id="Seg_2983" n="HIAT:ip">.</nts>
                  <nts id="Seg_2984" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T691" id="Seg_2986" n="HIAT:u" s="T688">
                  <ts e="T689" id="Seg_2988" n="HIAT:w" s="T688">Ugaːndə</ts>
                  <nts id="Seg_2989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2991" n="HIAT:w" s="T689">jakšə</ts>
                  <nts id="Seg_2992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2994" n="HIAT:w" s="T690">dʼala</ts>
                  <nts id="Seg_2995" n="HIAT:ip">.</nts>
                  <nts id="Seg_2996" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T694" id="Seg_2998" n="HIAT:u" s="T691">
                  <ts e="T692" id="Seg_3000" n="HIAT:w" s="T691">Il</ts>
                  <nts id="Seg_3001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_3003" n="HIAT:w" s="T692">bar</ts>
                  <nts id="Seg_3004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_3006" n="HIAT:w" s="T693">togonorlaʔbəʔjə</ts>
                  <nts id="Seg_3007" n="HIAT:ip">.</nts>
                  <nts id="Seg_3008" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T696" id="Seg_3010" n="HIAT:u" s="T694">
                  <ts e="T695" id="Seg_3012" n="HIAT:w" s="T694">Toltano</ts>
                  <nts id="Seg_3013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_3015" n="HIAT:w" s="T695">amlaʔbəʔjə</ts>
                  <nts id="Seg_3016" n="HIAT:ip">.</nts>
                  <nts id="Seg_3017" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T699" id="Seg_3019" n="HIAT:u" s="T696">
                  <ts e="T697" id="Seg_3021" n="HIAT:w" s="T696">Ineʔi</ts>
                  <nts id="Seg_3022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_3024" n="HIAT:w" s="T697">tarirlaʔbəʔjə</ts>
                  <nts id="Seg_3025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_3027" n="HIAT:w" s="T698">bar</ts>
                  <nts id="Seg_3028" n="HIAT:ip">.</nts>
                  <nts id="Seg_3029" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T703" id="Seg_3031" n="HIAT:u" s="T699">
                  <ts e="T700" id="Seg_3033" n="HIAT:w" s="T699">Nada</ts>
                  <nts id="Seg_3034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_3036" n="HIAT:w" s="T700">kanzittə</ts>
                  <nts id="Seg_3037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_3039" n="HIAT:w" s="T701">noʔ</ts>
                  <nts id="Seg_3040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_3042" n="HIAT:w" s="T702">jaʔsittə</ts>
                  <nts id="Seg_3043" n="HIAT:ip">.</nts>
                  <nts id="Seg_3044" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T706" id="Seg_3046" n="HIAT:u" s="T703">
                  <ts e="T704" id="Seg_3048" n="HIAT:w" s="T703">Šapku</ts>
                  <nts id="Seg_3049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_3051" n="HIAT:w" s="T704">nada</ts>
                  <nts id="Seg_3052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_3054" n="HIAT:w" s="T705">izittə</ts>
                  <nts id="Seg_3055" n="HIAT:ip">.</nts>
                  <nts id="Seg_3056" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T709" id="Seg_3058" n="HIAT:u" s="T706">
                  <ts e="T707" id="Seg_3060" n="HIAT:w" s="T706">Nada</ts>
                  <nts id="Seg_3061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_3063" n="HIAT:w" s="T707">bü</ts>
                  <nts id="Seg_3064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_3066" n="HIAT:w" s="T708">izittə</ts>
                  <nts id="Seg_3067" n="HIAT:ip">.</nts>
                  <nts id="Seg_3068" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T718" id="Seg_3070" n="HIAT:u" s="T709">
                  <ts e="T710" id="Seg_3072" n="HIAT:w" s="T709">Süt</ts>
                  <nts id="Seg_3073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_3075" n="HIAT:w" s="T710">izittə</ts>
                  <nts id="Seg_3076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_3078" n="HIAT:w" s="T711">nada</ts>
                  <nts id="Seg_3079" n="HIAT:ip">,</nts>
                  <nts id="Seg_3080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_3082" n="HIAT:w" s="T712">ipek</ts>
                  <nts id="Seg_3083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_3085" n="HIAT:w" s="T713">izittə</ts>
                  <nts id="Seg_3086" n="HIAT:ip">,</nts>
                  <nts id="Seg_3087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_3089" n="HIAT:w" s="T714">tus</ts>
                  <nts id="Seg_3090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_3092" n="HIAT:w" s="T715">izittə</ts>
                  <nts id="Seg_3093" n="HIAT:ip">,</nts>
                  <nts id="Seg_3094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_3096" n="HIAT:w" s="T716">munujʔ</ts>
                  <nts id="Seg_3097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_3099" n="HIAT:w" s="T717">izittə</ts>
                  <nts id="Seg_3100" n="HIAT:ip">.</nts>
                  <nts id="Seg_3101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T722" id="Seg_3103" n="HIAT:u" s="T718">
                  <ts e="T719" id="Seg_3105" n="HIAT:w" s="T718">Pi</ts>
                  <nts id="Seg_3106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_3108" n="HIAT:w" s="T719">nada</ts>
                  <nts id="Seg_3109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_3111" n="HIAT:w" s="T720">izittə</ts>
                  <nts id="Seg_3112" n="HIAT:ip">,</nts>
                  <nts id="Seg_3113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_3115" n="HIAT:w" s="T721">šapku</ts>
                  <nts id="Seg_3116" n="HIAT:ip">.</nts>
                  <nts id="Seg_3117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T725" id="Seg_3119" n="HIAT:u" s="T722">
                  <ts e="T723" id="Seg_3121" n="HIAT:w" s="T722">Albuga</ts>
                  <nts id="Seg_3122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_3124" n="HIAT:w" s="T723">teinen</ts>
                  <nts id="Seg_3125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_3127" n="HIAT:w" s="T724">kuʔpiem</ts>
                  <nts id="Seg_3128" n="HIAT:ip">.</nts>
                  <nts id="Seg_3129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T727" id="Seg_3131" n="HIAT:u" s="T725">
                  <ts e="T726" id="Seg_3133" n="HIAT:w" s="T725">Tažəp</ts>
                  <nts id="Seg_3134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_3136" n="HIAT:w" s="T726">kuʔpiem</ts>
                  <nts id="Seg_3137" n="HIAT:ip">.</nts>
                  <nts id="Seg_3138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T729" id="Seg_3140" n="HIAT:u" s="T727">
                  <ts e="T728" id="Seg_3142" n="HIAT:w" s="T727">Urgaːba</ts>
                  <nts id="Seg_3143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_3145" n="HIAT:w" s="T728">kuʔpiem</ts>
                  <nts id="Seg_3146" n="HIAT:ip">.</nts>
                  <nts id="Seg_3147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T731" id="Seg_3149" n="HIAT:u" s="T729">
                  <ts e="T730" id="Seg_3151" n="HIAT:w" s="T729">Söːn</ts>
                  <nts id="Seg_3152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_3154" n="HIAT:w" s="T730">kuʔpiem</ts>
                  <nts id="Seg_3155" n="HIAT:ip">.</nts>
                  <nts id="Seg_3156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T733" id="Seg_3158" n="HIAT:u" s="T731">
                  <ts e="T732" id="Seg_3160" n="HIAT:w" s="T731">Askər</ts>
                  <nts id="Seg_3161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_3163" n="HIAT:w" s="T732">kuʔpiem</ts>
                  <nts id="Seg_3164" n="HIAT:ip">.</nts>
                  <nts id="Seg_3165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T736" id="Seg_3167" n="HIAT:u" s="T733">
                  <ts e="T734" id="Seg_3169" n="HIAT:w" s="T733">Poʔto</ts>
                  <nts id="Seg_3170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_3172" n="HIAT:w" s="T734">kuʔpiem</ts>
                  <nts id="Seg_3173" n="HIAT:ip">,</nts>
                  <nts id="Seg_3174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_3176" n="HIAT:w" s="T735">multuksiʔ</ts>
                  <nts id="Seg_3177" n="HIAT:ip">.</nts>
                  <nts id="Seg_3178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T740" id="Seg_3180" n="HIAT:u" s="T736">
                  <ts e="T737" id="Seg_3182" n="HIAT:w" s="T736">Tüžöjʔi</ts>
                  <nts id="Seg_3183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_3185" n="HIAT:w" s="T737">bar</ts>
                  <nts id="Seg_3186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_3188" n="HIAT:w" s="T738">noʔ</ts>
                  <nts id="Seg_3189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_3191" n="HIAT:w" s="T739">amniaʔi</ts>
                  <nts id="Seg_3192" n="HIAT:ip">.</nts>
                  <nts id="Seg_3193" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T742" id="Seg_3195" n="HIAT:u" s="T740">
                  <ts e="T741" id="Seg_3197" n="HIAT:w" s="T740">Mĭlleʔbəʔjə</ts>
                  <nts id="Seg_3198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_3200" n="HIAT:w" s="T741">dĭn</ts>
                  <nts id="Seg_3201" n="HIAT:ip">.</nts>
                  <nts id="Seg_3202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T747" id="Seg_3204" n="HIAT:u" s="T742">
                  <ts e="T743" id="Seg_3206" n="HIAT:w" s="T742">Dĭ</ts>
                  <nts id="Seg_3207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_3209" n="HIAT:w" s="T743">kuza</ts>
                  <nts id="Seg_3210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_3212" n="HIAT:w" s="T744">detləj</ts>
                  <nts id="Seg_3213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_3215" n="HIAT:w" s="T745">их</ts>
                  <nts id="Seg_3216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_3218" n="HIAT:w" s="T746">maʔndə</ts>
                  <nts id="Seg_3219" n="HIAT:ip">.</nts>
                  <nts id="Seg_3220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T748" id="Seg_3222" n="HIAT:u" s="T747">
                  <ts e="T748" id="Seg_3224" n="HIAT:w" s="T747">Всё</ts>
                  <nts id="Seg_3225" n="HIAT:ip">.</nts>
                  <nts id="Seg_3226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T751" id="Seg_3228" n="HIAT:u" s="T748">
                  <ts e="T749" id="Seg_3230" n="HIAT:w" s="T748">Nada</ts>
                  <nts id="Seg_3231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_3233" n="HIAT:w" s="T749">eššim</ts>
                  <nts id="Seg_3234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_3236" n="HIAT:w" s="T750">bazəsʼtə</ts>
                  <nts id="Seg_3237" n="HIAT:ip">.</nts>
                  <nts id="Seg_3238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T757" id="Seg_3240" n="HIAT:u" s="T751">
                  <ts e="T752" id="Seg_3242" n="HIAT:w" s="T751">Nada</ts>
                  <nts id="Seg_3243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3244" n="HIAT:ip">(</nts>
                  <ts e="T753" id="Seg_3246" n="HIAT:w" s="T752">e</ts>
                  <nts id="Seg_3247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_3249" n="HIAT:w" s="T753">-</ts>
                  <nts id="Seg_3250" n="HIAT:ip">)</nts>
                  <nts id="Seg_3251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_3253" n="HIAT:w" s="T754">eššim</ts>
                  <nts id="Seg_3254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_3256" n="HIAT:w" s="T755">bar</ts>
                  <nts id="Seg_3257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_3259" n="HIAT:w" s="T756">dʼorlaʔbə</ts>
                  <nts id="Seg_3260" n="HIAT:ip">.</nts>
                  <nts id="Seg_3261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T760" id="Seg_3263" n="HIAT:u" s="T757">
                  <ts e="T758" id="Seg_3265" n="HIAT:w" s="T757">Nada</ts>
                  <nts id="Seg_3266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_3268" n="HIAT:w" s="T758">dĭm</ts>
                  <nts id="Seg_3269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_3271" n="HIAT:w" s="T759">köːdərzittə</ts>
                  <nts id="Seg_3272" n="HIAT:ip">.</nts>
                  <nts id="Seg_3273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T763" id="Seg_3275" n="HIAT:u" s="T760">
                  <ts e="T761" id="Seg_3277" n="HIAT:w" s="T760">Nada</ts>
                  <nts id="Seg_3278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_3280" n="HIAT:w" s="T761">amorzittə</ts>
                  <nts id="Seg_3281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_3283" n="HIAT:w" s="T762">mĭzittə</ts>
                  <nts id="Seg_3284" n="HIAT:ip">.</nts>
                  <nts id="Seg_3285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T766" id="Seg_3287" n="HIAT:u" s="T763">
                  <ts e="T764" id="Seg_3289" n="HIAT:w" s="T763">Nada</ts>
                  <nts id="Seg_3290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_3292" n="HIAT:w" s="T764">kunoːlzittə</ts>
                  <nts id="Seg_3293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_3295" n="HIAT:w" s="T765">enzittə</ts>
                  <nts id="Seg_3296" n="HIAT:ip">.</nts>
                  <nts id="Seg_3297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T982" id="Seg_3298" n="sc" s="T770">
               <ts e="T773" id="Seg_3300" n="HIAT:u" s="T770">
                  <ts e="T771" id="Seg_3302" n="HIAT:w" s="T770">Oːrdəʔ</ts>
                  <nts id="Seg_3303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_3305" n="HIAT:w" s="T771">eššim</ts>
                  <nts id="Seg_3306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_3308" n="HIAT:w" s="T772">bar</ts>
                  <nts id="Seg_3309" n="HIAT:ip">!</nts>
                  <nts id="Seg_3310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T775" id="Seg_3312" n="HIAT:u" s="T773">
                  <ts e="T774" id="Seg_3314" n="HIAT:w" s="T773">Pušaj</ts>
                  <nts id="Seg_3315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_3317" n="HIAT:w" s="T774">kunoːllaʔbə</ts>
                  <nts id="Seg_3318" n="HIAT:ip">.</nts>
                  <nts id="Seg_3319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T779" id="Seg_3321" n="HIAT:u" s="T775">
                  <ts e="T776" id="Seg_3323" n="HIAT:w" s="T775">Amnoʔ</ts>
                  <nts id="Seg_3324" n="HIAT:ip">,</nts>
                  <nts id="Seg_3325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_3327" n="HIAT:w" s="T776">tüjö</ts>
                  <nts id="Seg_3328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_3330" n="HIAT:w" s="T777">büjleʔ</ts>
                  <nts id="Seg_3331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_3333" n="HIAT:w" s="T778">kallam</ts>
                  <nts id="Seg_3334" n="HIAT:ip">.</nts>
                  <nts id="Seg_3335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T781" id="Seg_3337" n="HIAT:u" s="T779">
                  <ts e="T780" id="Seg_3339" n="HIAT:w" s="T779">Bü</ts>
                  <nts id="Seg_3340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_3342" n="HIAT:w" s="T780">detlem</ts>
                  <nts id="Seg_3343" n="HIAT:ip">.</nts>
                  <nts id="Seg_3344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T783" id="Seg_3346" n="HIAT:u" s="T781">
                  <ts e="T782" id="Seg_3348" n="HIAT:w" s="T781">Dĭgəttə</ts>
                  <nts id="Seg_3349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_3351" n="HIAT:w" s="T782">bĭtlel</ts>
                  <nts id="Seg_3352" n="HIAT:ip">.</nts>
                  <nts id="Seg_3353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T785" id="Seg_3355" n="HIAT:u" s="T783">
                  <ts e="T784" id="Seg_3357" n="HIAT:w" s="T783">Iʔ</ts>
                  <nts id="Seg_3358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_3360" n="HIAT:w" s="T784">kanaʔ</ts>
                  <nts id="Seg_3361" n="HIAT:ip">!</nts>
                  <nts id="Seg_3362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T789" id="Seg_3364" n="HIAT:u" s="T785">
                  <ts e="T786" id="Seg_3366" n="HIAT:w" s="T785">A</ts>
                  <nts id="Seg_3367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_3369" n="HIAT:w" s="T786">dĭgəttə</ts>
                  <nts id="Seg_3370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_3372" n="HIAT:w" s="T787">kallal</ts>
                  <nts id="Seg_3373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_3375" n="HIAT:w" s="T788">maʔnəl</ts>
                  <nts id="Seg_3376" n="HIAT:ip">.</nts>
                  <nts id="Seg_3377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T792" id="Seg_3379" n="HIAT:u" s="T789">
                  <ts e="T790" id="Seg_3381" n="HIAT:w" s="T789">Tüjö</ts>
                  <nts id="Seg_3382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_3384" n="HIAT:w" s="T790">kem</ts>
                  <nts id="Seg_3385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_3387" n="HIAT:w" s="T791">pürlem</ts>
                  <nts id="Seg_3388" n="HIAT:ip">.</nts>
                  <nts id="Seg_3389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T801" id="Seg_3391" n="HIAT:u" s="T792">
                  <ts e="T793" id="Seg_3393" n="HIAT:w" s="T792">Nada</ts>
                  <nts id="Seg_3394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_3396" n="HIAT:w" s="T793">dibər</ts>
                  <nts id="Seg_3397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_3399" n="HIAT:w" s="T794">süt</ts>
                  <nts id="Seg_3400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_3402" n="HIAT:w" s="T795">kămnasʼtə</ts>
                  <nts id="Seg_3403" n="HIAT:ip">,</nts>
                  <nts id="Seg_3404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_3406" n="HIAT:w" s="T796">kajak</ts>
                  <nts id="Seg_3407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_3409" n="HIAT:w" s="T797">enzittə</ts>
                  <nts id="Seg_3410" n="HIAT:ip">,</nts>
                  <nts id="Seg_3411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_3413" n="HIAT:w" s="T798">alʼi</ts>
                  <nts id="Seg_3414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_3416" n="HIAT:w" s="T799">sil</ts>
                  <nts id="Seg_3417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_3419" n="HIAT:w" s="T800">enzittə</ts>
                  <nts id="Seg_3420" n="HIAT:ip">.</nts>
                  <nts id="Seg_3421" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T803" id="Seg_3423" n="HIAT:u" s="T801">
                  <ts e="T802" id="Seg_3425" n="HIAT:w" s="T801">Tüjö</ts>
                  <nts id="Seg_3426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_3428" n="HIAT:w" s="T802">amorləbəj</ts>
                  <nts id="Seg_3429" n="HIAT:ip">.</nts>
                  <nts id="Seg_3430" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T807" id="Seg_3432" n="HIAT:u" s="T803">
                  <ts e="T804" id="Seg_3434" n="HIAT:w" s="T803">Iʔ</ts>
                  <nts id="Seg_3435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3436" n="HIAT:ip">(</nts>
                  <ts e="T805" id="Seg_3438" n="HIAT:w" s="T804">baza</ts>
                  <nts id="Seg_3439" n="HIAT:ip">)</nts>
                  <nts id="Seg_3440" n="HIAT:ip">,</nts>
                  <nts id="Seg_3441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805.tx-PKZ.1" id="Seg_3443" n="HIAT:w" s="T805">a</ts>
                  <nts id="Seg_3444" n="HIAT:ip">_</nts>
                  <ts e="T806" id="Seg_3446" n="HIAT:w" s="T805.tx-PKZ.1">to</ts>
                  <nts id="Seg_3447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_3449" n="HIAT:w" s="T806">dʼibige</ts>
                  <nts id="Seg_3450" n="HIAT:ip">.</nts>
                  <nts id="Seg_3451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T811" id="Seg_3453" n="HIAT:u" s="T807">
                  <nts id="Seg_3454" n="HIAT:ip">(</nts>
                  <ts e="T808" id="Seg_3456" n="HIAT:w" s="T807">Ipek</ts>
                  <nts id="Seg_3457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_3459" n="HIAT:w" s="T808">-</ts>
                  <nts id="Seg_3460" n="HIAT:ip">)</nts>
                  <nts id="Seg_3461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_3463" n="HIAT:w" s="T809">Ipeksiʔ</ts>
                  <nts id="Seg_3464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_3466" n="HIAT:w" s="T810">amaʔ</ts>
                  <nts id="Seg_3467" n="HIAT:ip">!</nts>
                  <nts id="Seg_3468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T814" id="Seg_3470" n="HIAT:u" s="T811">
                  <nts id="Seg_3471" n="HIAT:ip">(</nts>
                  <ts e="T812" id="Seg_3473" n="HIAT:w" s="T811">Koʔbdo</ts>
                  <nts id="Seg_3474" n="HIAT:ip">)</nts>
                  <nts id="Seg_3475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_3477" n="HIAT:w" s="T812">šobi</ts>
                  <nts id="Seg_3478" n="HIAT:ip">.</nts>
                  <nts id="Seg_3479" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T816" id="Seg_3481" n="HIAT:u" s="T814">
                  <ts e="T815" id="Seg_3483" n="HIAT:w" s="T814">Eʔbdə</ts>
                  <nts id="Seg_3484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_3486" n="HIAT:w" s="T815">sĭre</ts>
                  <nts id="Seg_3487" n="HIAT:ip">.</nts>
                  <nts id="Seg_3488" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T819" id="Seg_3490" n="HIAT:u" s="T816">
                  <ts e="T817" id="Seg_3492" n="HIAT:w" s="T816">Bostə</ts>
                  <nts id="Seg_3493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_3495" n="HIAT:w" s="T817">bar</ts>
                  <nts id="Seg_3496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_3498" n="HIAT:w" s="T818">todam</ts>
                  <nts id="Seg_3499" n="HIAT:ip">.</nts>
                  <nts id="Seg_3500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T825" id="Seg_3502" n="HIAT:u" s="T819">
                  <ts e="T820" id="Seg_3504" n="HIAT:w" s="T819">I</ts>
                  <nts id="Seg_3505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_3507" n="HIAT:w" s="T820">dĭ</ts>
                  <nts id="Seg_3508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_3510" n="HIAT:w" s="T821">kuza</ts>
                  <nts id="Seg_3511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_3513" n="HIAT:w" s="T822">šobi</ts>
                  <nts id="Seg_3514" n="HIAT:ip">,</nts>
                  <nts id="Seg_3515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_3517" n="HIAT:w" s="T823">nʼešpək</ts>
                  <nts id="Seg_3518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3520" n="HIAT:w" s="T824">ugaːndə</ts>
                  <nts id="Seg_3521" n="HIAT:ip">.</nts>
                  <nts id="Seg_3522" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T827" id="Seg_3524" n="HIAT:u" s="T825">
                  <ts e="T826" id="Seg_3526" n="HIAT:w" s="T825">I</ts>
                  <nts id="Seg_3527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_3529" n="HIAT:w" s="T826">mĭʔ</ts>
                  <nts id="Seg_3530" n="HIAT:ip">!</nts>
                  <nts id="Seg_3531" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T829" id="Seg_3533" n="HIAT:u" s="T827">
                  <ts e="T828" id="Seg_3535" n="HIAT:w" s="T827">Elet</ts>
                  <nts id="Seg_3536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_3538" n="HIAT:w" s="T828">šobi</ts>
                  <nts id="Seg_3539" n="HIAT:ip">.</nts>
                  <nts id="Seg_3540" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T830" id="Seg_3542" n="HIAT:u" s="T829">
                  <ts e="T830" id="Seg_3544" n="HIAT:w" s="T829">Всё</ts>
                  <nts id="Seg_3545" n="HIAT:ip">.</nts>
                  <nts id="Seg_3546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T834" id="Seg_3548" n="HIAT:u" s="T830">
                  <ts e="T831" id="Seg_3550" n="HIAT:w" s="T830">Dĭzeŋ</ts>
                  <nts id="Seg_3551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3553" n="HIAT:w" s="T831">toltano</ts>
                  <nts id="Seg_3554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_3556" n="HIAT:w" s="T832">ej</ts>
                  <nts id="Seg_3557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_3559" n="HIAT:w" s="T833">amnəbiʔi</ts>
                  <nts id="Seg_3560" n="HIAT:ip">.</nts>
                  <nts id="Seg_3561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T838" id="Seg_3563" n="HIAT:u" s="T834">
                  <ts e="T835" id="Seg_3565" n="HIAT:w" s="T834">Măn</ts>
                  <nts id="Seg_3566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_3568" n="HIAT:w" s="T835">abam</ts>
                  <nts id="Seg_3569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_3571" n="HIAT:w" s="T836">dĭzeŋdə</ts>
                  <nts id="Seg_3572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_3574" n="HIAT:w" s="T837">amnəlbi</ts>
                  <nts id="Seg_3575" n="HIAT:ip">.</nts>
                  <nts id="Seg_3576" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T845" id="Seg_3578" n="HIAT:u" s="T838">
                  <ts e="T839" id="Seg_3580" n="HIAT:w" s="T838">Dĭzeŋ</ts>
                  <nts id="Seg_3581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_3583" n="HIAT:w" s="T839">uja</ts>
                  <nts id="Seg_3584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_3586" n="HIAT:w" s="T840">iʔgö</ts>
                  <nts id="Seg_3587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_3589" n="HIAT:w" s="T841">ibi</ts>
                  <nts id="Seg_3590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_3592" n="HIAT:w" s="T842">bar</ts>
                  <nts id="Seg_3593" n="HIAT:ip">,</nts>
                  <nts id="Seg_3594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_3596" n="HIAT:w" s="T843">uja</ts>
                  <nts id="Seg_3597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_3599" n="HIAT:w" s="T844">ambiʔi</ts>
                  <nts id="Seg_3600" n="HIAT:ip">.</nts>
                  <nts id="Seg_3601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T848" id="Seg_3603" n="HIAT:u" s="T845">
                  <ts e="T846" id="Seg_3605" n="HIAT:w" s="T845">Ipek</ts>
                  <nts id="Seg_3606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_3608" n="HIAT:w" s="T846">amga</ts>
                  <nts id="Seg_3609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_3611" n="HIAT:w" s="T847">ibi</ts>
                  <nts id="Seg_3612" n="HIAT:ip">.</nts>
                  <nts id="Seg_3613" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T854" id="Seg_3615" n="HIAT:u" s="T848">
                  <ts e="T849" id="Seg_3617" n="HIAT:w" s="T848">A</ts>
                  <nts id="Seg_3618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_3620" n="HIAT:w" s="T849">daška</ts>
                  <nts id="Seg_3621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_3623" n="HIAT:w" s="T850">ĭmbidə</ts>
                  <nts id="Seg_3624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3625" n="HIAT:ip">(</nts>
                  <ts e="T852" id="Seg_3627" n="HIAT:w" s="T851">n</ts>
                  <nts id="Seg_3628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_3630" n="HIAT:w" s="T852">-</ts>
                  <nts id="Seg_3631" n="HIAT:ip">)</nts>
                  <nts id="Seg_3632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_3634" n="HIAT:w" s="T853">naga</ts>
                  <nts id="Seg_3635" n="HIAT:ip">.</nts>
                  <nts id="Seg_3636" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T858" id="Seg_3638" n="HIAT:u" s="T854">
                  <ts e="T855" id="Seg_3640" n="HIAT:w" s="T854">Ĭmbi</ts>
                  <nts id="Seg_3641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_3643" n="HIAT:w" s="T855">amnolaʔbəl</ts>
                  <nts id="Seg_3644" n="HIAT:ip">,</nts>
                  <nts id="Seg_3645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_3647" n="HIAT:w" s="T856">ej</ts>
                  <nts id="Seg_3648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_3650" n="HIAT:w" s="T857">dʼăbaktərial</ts>
                  <nts id="Seg_3651" n="HIAT:ip">.</nts>
                  <nts id="Seg_3652" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T860" id="Seg_3654" n="HIAT:u" s="T858">
                  <ts e="T859" id="Seg_3656" n="HIAT:w" s="T858">Dʼăbaktəraʔ</ts>
                  <nts id="Seg_3657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_3659" n="HIAT:w" s="T859">ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_3660" n="HIAT:ip">!</nts>
                  <nts id="Seg_3661" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T867" id="Seg_3663" n="HIAT:u" s="T860">
                  <nts id="Seg_3664" n="HIAT:ip">(</nts>
                  <ts e="T861" id="Seg_3666" n="HIAT:w" s="T860">Sĭjbə</ts>
                  <nts id="Seg_3667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_3669" n="HIAT:w" s="T861">verna</ts>
                  <nts id="Seg_3670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_3672" n="HIAT:w" s="T862">ĭze</ts>
                  <nts id="Seg_3673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_3675" n="HIAT:w" s="T863">-</ts>
                  <nts id="Seg_3676" n="HIAT:ip">)</nts>
                  <nts id="Seg_3677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_3679" n="HIAT:w" s="T864">Sĭjlə</ts>
                  <nts id="Seg_3680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_3682" n="HIAT:w" s="T865">verna</ts>
                  <nts id="Seg_3683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_3685" n="HIAT:w" s="T866">ĭzemnie</ts>
                  <nts id="Seg_3686" n="HIAT:ip">.</nts>
                  <nts id="Seg_3687" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T870" id="Seg_3689" n="HIAT:u" s="T867">
                  <ts e="T868" id="Seg_3691" n="HIAT:w" s="T867">Ĭmbidə</ts>
                  <nts id="Seg_3692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_3694" n="HIAT:w" s="T868">ej</ts>
                  <nts id="Seg_3695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_3697" n="HIAT:w" s="T869">dʼăbaktərial</ts>
                  <nts id="Seg_3698" n="HIAT:ip">.</nts>
                  <nts id="Seg_3699" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T871" id="Seg_3701" n="HIAT:u" s="T870">
                  <ts e="T871" id="Seg_3703" n="HIAT:w" s="T870">Dʼăbaktəraʔ</ts>
                  <nts id="Seg_3704" n="HIAT:ip">!</nts>
                  <nts id="Seg_3705" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T872" id="Seg_3707" n="HIAT:u" s="T871">
                  <ts e="T872" id="Seg_3709" n="HIAT:w" s="T871">Всё</ts>
                  <nts id="Seg_3710" n="HIAT:ip">.</nts>
                  <nts id="Seg_3711" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T876" id="Seg_3713" n="HIAT:u" s="T872">
                  <nts id="Seg_3714" n="HIAT:ip">(</nts>
                  <ts e="T873" id="Seg_3716" n="HIAT:w" s="T872">Gibə</ts>
                  <nts id="Seg_3717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_3719" n="HIAT:w" s="T873">-</ts>
                  <nts id="Seg_3720" n="HIAT:ip">)</nts>
                  <nts id="Seg_3721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_3723" n="HIAT:w" s="T874">Gibər</ts>
                  <nts id="Seg_3724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_3726" n="HIAT:w" s="T875">kandəgal</ts>
                  <nts id="Seg_3727" n="HIAT:ip">?</nts>
                  <nts id="Seg_3728" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T880" id="Seg_3730" n="HIAT:u" s="T876">
                  <ts e="T877" id="Seg_3732" n="HIAT:w" s="T876">Gijen</ts>
                  <nts id="Seg_3733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_3735" n="HIAT:w" s="T877">ibiel</ts>
                  <nts id="Seg_3736" n="HIAT:ip">,</nts>
                  <nts id="Seg_3737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_3739" n="HIAT:w" s="T878">šoʔ</ts>
                  <nts id="Seg_3740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T880" id="Seg_3742" n="HIAT:w" s="T879">döːbər</ts>
                  <nts id="Seg_3743" n="HIAT:ip">!</nts>
                  <nts id="Seg_3744" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T883" id="Seg_3746" n="HIAT:u" s="T880">
                  <ts e="T881" id="Seg_3748" n="HIAT:w" s="T880">Ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_3749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_3751" n="HIAT:w" s="T881">nörbəʔ</ts>
                  <nts id="Seg_3752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_3754" n="HIAT:w" s="T882">măna</ts>
                  <nts id="Seg_3755" n="HIAT:ip">!</nts>
                  <nts id="Seg_3756" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T885" id="Seg_3758" n="HIAT:u" s="T883">
                  <ts e="T883.tx-PKZ.1" id="Seg_3760" n="HIAT:w" s="T883">Опять</ts>
                  <nts id="Seg_3761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_3763" n="HIAT:w" s="T883.tx-PKZ.1">забыла</ts>
                  <nts id="Seg_3764" n="HIAT:ip">.</nts>
                  <nts id="Seg_3765" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T888" id="Seg_3767" n="HIAT:u" s="T885">
                  <ts e="T886" id="Seg_3769" n="HIAT:w" s="T885">Büzʼe</ts>
                  <nts id="Seg_3770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887" id="Seg_3772" n="HIAT:w" s="T886">nüketsiʔ</ts>
                  <nts id="Seg_3773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T888" id="Seg_3775" n="HIAT:w" s="T887">amnolaʔpiʔi</ts>
                  <nts id="Seg_3776" n="HIAT:ip">.</nts>
                  <nts id="Seg_3777" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T891" id="Seg_3779" n="HIAT:u" s="T888">
                  <ts e="T889" id="Seg_3781" n="HIAT:w" s="T888">Dĭzeŋ</ts>
                  <nts id="Seg_3782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_3784" n="HIAT:w" s="T889">esseŋdə</ts>
                  <nts id="Seg_3785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_3787" n="HIAT:w" s="T890">ibiʔi</ts>
                  <nts id="Seg_3788" n="HIAT:ip">.</nts>
                  <nts id="Seg_3789" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T894" id="Seg_3791" n="HIAT:u" s="T891">
                  <ts e="T892" id="Seg_3793" n="HIAT:w" s="T891">Koʔbdo</ts>
                  <nts id="Seg_3794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_3796" n="HIAT:w" s="T892">da</ts>
                  <nts id="Seg_3797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_3799" n="HIAT:w" s="T893">nʼi</ts>
                  <nts id="Seg_3800" n="HIAT:ip">.</nts>
                  <nts id="Seg_3801" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T896" id="Seg_3803" n="HIAT:u" s="T894">
                  <ts e="T895" id="Seg_3805" n="HIAT:w" s="T894">Tura</ts>
                  <nts id="Seg_3806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_3808" n="HIAT:w" s="T895">ibi</ts>
                  <nts id="Seg_3809" n="HIAT:ip">.</nts>
                  <nts id="Seg_3810" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T898" id="Seg_3812" n="HIAT:u" s="T896">
                  <ts e="T897" id="Seg_3814" n="HIAT:w" s="T896">Tüžöj</ts>
                  <nts id="Seg_3815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_3817" n="HIAT:w" s="T897">ibi</ts>
                  <nts id="Seg_3818" n="HIAT:ip">.</nts>
                  <nts id="Seg_3819" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T902" id="Seg_3821" n="HIAT:u" s="T898">
                  <ts e="T899" id="Seg_3823" n="HIAT:w" s="T898">Dĭ</ts>
                  <nts id="Seg_3824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_3826" n="HIAT:w" s="T899">nüke</ts>
                  <nts id="Seg_3827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_3829" n="HIAT:w" s="T900">tüžöj</ts>
                  <nts id="Seg_3830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_3832" n="HIAT:w" s="T901">surdobi</ts>
                  <nts id="Seg_3833" n="HIAT:ip">.</nts>
                  <nts id="Seg_3834" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T904" id="Seg_3836" n="HIAT:u" s="T902">
                  <ts e="T903" id="Seg_3838" n="HIAT:w" s="T902">Esseŋdə</ts>
                  <nts id="Seg_3839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T904" id="Seg_3841" n="HIAT:w" s="T903">mĭbi</ts>
                  <nts id="Seg_3842" n="HIAT:ip">.</nts>
                  <nts id="Seg_3843" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T906" id="Seg_3845" n="HIAT:u" s="T904">
                  <ts e="T905" id="Seg_3847" n="HIAT:w" s="T904">Ipek</ts>
                  <nts id="Seg_3848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_3850" n="HIAT:w" s="T905">mĭbi</ts>
                  <nts id="Seg_3851" n="HIAT:ip">.</nts>
                  <nts id="Seg_3852" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T908" id="Seg_3854" n="HIAT:u" s="T906">
                  <ts e="T907" id="Seg_3856" n="HIAT:w" s="T906">Dĭzeŋ</ts>
                  <nts id="Seg_3857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_3859" n="HIAT:w" s="T907">amniaʔi</ts>
                  <nts id="Seg_3860" n="HIAT:ip">.</nts>
                  <nts id="Seg_3861" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T909" id="Seg_3863" n="HIAT:u" s="T908">
                  <ts e="T909" id="Seg_3865" n="HIAT:w" s="T908">Всё</ts>
                  <nts id="Seg_3866" n="HIAT:ip">.</nts>
                  <nts id="Seg_3867" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T913" id="Seg_3869" n="HIAT:u" s="T909">
                  <ts e="T910" id="Seg_3871" n="HIAT:w" s="T909">Dĭ</ts>
                  <nts id="Seg_3872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_3874" n="HIAT:w" s="T910">šidegöʔ</ts>
                  <nts id="Seg_3875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T912" id="Seg_3877" n="HIAT:w" s="T911">kuza</ts>
                  <nts id="Seg_3878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T913" id="Seg_3880" n="HIAT:w" s="T912">šobiʔi</ts>
                  <nts id="Seg_3881" n="HIAT:ip">.</nts>
                  <nts id="Seg_3882" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T920" id="Seg_3884" n="HIAT:u" s="T913">
                  <ts e="T914" id="Seg_3886" n="HIAT:w" s="T913">Tibi</ts>
                  <nts id="Seg_3887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T915" id="Seg_3889" n="HIAT:w" s="T914">da</ts>
                  <nts id="Seg_3890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_3892" n="HIAT:w" s="T915">koʔbdo</ts>
                  <nts id="Seg_3893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T917" id="Seg_3895" n="HIAT:w" s="T916">tănan</ts>
                  <nts id="Seg_3896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T918" id="Seg_3898" n="HIAT:w" s="T917">padʼi</ts>
                  <nts id="Seg_3899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T919" id="Seg_3901" n="HIAT:w" s="T918">ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_3902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T920" id="Seg_3904" n="HIAT:w" s="T919">deʔpiʔi</ts>
                  <nts id="Seg_3905" n="HIAT:ip">.</nts>
                  <nts id="Seg_3906" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T922" id="Seg_3908" n="HIAT:u" s="T920">
                  <ts e="T921" id="Seg_3910" n="HIAT:w" s="T920">Sĭreʔpne</ts>
                  <nts id="Seg_3911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_3913" n="HIAT:w" s="T921">deʔpiʔi</ts>
                  <nts id="Seg_3914" n="HIAT:ip">.</nts>
                  <nts id="Seg_3915" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T927" id="Seg_3917" n="HIAT:u" s="T922">
                  <nts id="Seg_3918" n="HIAT:ip">(</nts>
                  <ts e="T923" id="Seg_3920" n="HIAT:w" s="T922">Segə</ts>
                  <nts id="Seg_3921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T924" id="Seg_3923" n="HIAT:w" s="T923">-</ts>
                  <nts id="Seg_3924" n="HIAT:ip">)</nts>
                  <nts id="Seg_3925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T925" id="Seg_3927" n="HIAT:w" s="T924">Segi</ts>
                  <nts id="Seg_3928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T926" id="Seg_3930" n="HIAT:w" s="T925">bü</ts>
                  <nts id="Seg_3931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T927" id="Seg_3933" n="HIAT:w" s="T926">bĭtlel</ts>
                  <nts id="Seg_3934" n="HIAT:ip">.</nts>
                  <nts id="Seg_3935" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T931" id="Seg_3937" n="HIAT:u" s="T927">
                  <ts e="T928" id="Seg_3939" n="HIAT:w" s="T927">Padʼi</ts>
                  <nts id="Seg_3940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T929" id="Seg_3942" n="HIAT:w" s="T928">kanfeti</ts>
                  <nts id="Seg_3943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_3945" n="HIAT:w" s="T929">deʔpiʔi</ts>
                  <nts id="Seg_3946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T931" id="Seg_3948" n="HIAT:w" s="T930">tănan</ts>
                  <nts id="Seg_3949" n="HIAT:ip">.</nts>
                  <nts id="Seg_3950" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T934" id="Seg_3952" n="HIAT:u" s="T931">
                  <ts e="T932" id="Seg_3954" n="HIAT:w" s="T931">Kujnek</ts>
                  <nts id="Seg_3955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T933" id="Seg_3957" n="HIAT:w" s="T932">padʼi</ts>
                  <nts id="Seg_3958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T934" id="Seg_3960" n="HIAT:w" s="T933">deʔpiʔi</ts>
                  <nts id="Seg_3961" n="HIAT:ip">.</nts>
                  <nts id="Seg_3962" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T938" id="Seg_3964" n="HIAT:u" s="T934">
                  <ts e="T935" id="Seg_3966" n="HIAT:w" s="T934">Piʔmə</ts>
                  <nts id="Seg_3967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T936" id="Seg_3969" n="HIAT:w" s="T935">padʼi</ts>
                  <nts id="Seg_3970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T937" id="Seg_3972" n="HIAT:w" s="T936">deʔpiʔi</ts>
                  <nts id="Seg_3973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T938" id="Seg_3975" n="HIAT:w" s="T937">tănan</ts>
                  <nts id="Seg_3976" n="HIAT:ip">.</nts>
                  <nts id="Seg_3977" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T941" id="Seg_3979" n="HIAT:u" s="T938">
                  <ts e="T939" id="Seg_3981" n="HIAT:w" s="T938">Predsʼedatʼelʼən</ts>
                  <nts id="Seg_3982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T940" id="Seg_3984" n="HIAT:w" s="T939">turat</ts>
                  <nts id="Seg_3985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_3987" n="HIAT:w" s="T940">urgo</ts>
                  <nts id="Seg_3988" n="HIAT:ip">.</nts>
                  <nts id="Seg_3989" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T943" id="Seg_3991" n="HIAT:u" s="T941">
                  <ts e="T942" id="Seg_3993" n="HIAT:w" s="T941">Il</ts>
                  <nts id="Seg_3994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_3996" n="HIAT:w" s="T942">iʔgö</ts>
                  <nts id="Seg_3997" n="HIAT:ip">.</nts>
                  <nts id="Seg_3998" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T949" id="Seg_4000" n="HIAT:u" s="T943">
                  <nts id="Seg_4001" n="HIAT:ip">(</nts>
                  <ts e="T944" id="Seg_4003" n="HIAT:w" s="T943">Ši</ts>
                  <nts id="Seg_4004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T945" id="Seg_4006" n="HIAT:w" s="T944">-</ts>
                  <nts id="Seg_4007" n="HIAT:ip">)</nts>
                  <nts id="Seg_4008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_4010" n="HIAT:w" s="T945">Šide</ts>
                  <nts id="Seg_4011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947" id="Seg_4013" n="HIAT:w" s="T946">nʼi</ts>
                  <nts id="Seg_4014" n="HIAT:ip">,</nts>
                  <nts id="Seg_4015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_4017" n="HIAT:w" s="T947">šide</ts>
                  <nts id="Seg_4018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T949" id="Seg_4020" n="HIAT:w" s="T948">koʔbdo</ts>
                  <nts id="Seg_4021" n="HIAT:ip">.</nts>
                  <nts id="Seg_4022" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T951" id="Seg_4024" n="HIAT:u" s="T949">
                  <ts e="T950" id="Seg_4026" n="HIAT:w" s="T949">Nüke</ts>
                  <nts id="Seg_4027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T951" id="Seg_4029" n="HIAT:w" s="T950">büzʼeʔiziʔ</ts>
                  <nts id="Seg_4030" n="HIAT:ip">.</nts>
                  <nts id="Seg_4031" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T953" id="Seg_4033" n="HIAT:u" s="T951">
                  <ts e="T952" id="Seg_4035" n="HIAT:w" s="T951">Bostə</ts>
                  <nts id="Seg_4036" n="HIAT:ip">,</nts>
                  <nts id="Seg_4037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T953" id="Seg_4039" n="HIAT:w" s="T952">ne</ts>
                  <nts id="Seg_4040" n="HIAT:ip">.</nts>
                  <nts id="Seg_4041" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T956" id="Seg_4043" n="HIAT:u" s="T953">
                  <nts id="Seg_4044" n="HIAT:ip">(</nts>
                  <ts e="T954" id="Seg_4046" n="HIAT:w" s="T953">Me-</ts>
                  <nts id="Seg_4047" n="HIAT:ip">)</nts>
                  <nts id="Seg_4048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T955" id="Seg_4050" n="HIAT:w" s="T954">Menzeŋdə</ts>
                  <nts id="Seg_4051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T956" id="Seg_4053" n="HIAT:w" s="T955">šide</ts>
                  <nts id="Seg_4054" n="HIAT:ip">.</nts>
                  <nts id="Seg_4055" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T966" id="Seg_4057" n="HIAT:u" s="T956">
                  <ts e="T957" id="Seg_4059" n="HIAT:w" s="T956">Onʼiʔ</ts>
                  <nts id="Seg_4060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T958" id="Seg_4062" n="HIAT:w" s="T957">jakšə</ts>
                  <nts id="Seg_4063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T959" id="Seg_4065" n="HIAT:w" s="T958">men</ts>
                  <nts id="Seg_4066" n="HIAT:ip">,</nts>
                  <nts id="Seg_4067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T960" id="Seg_4069" n="HIAT:w" s="T959">onʼiʔ</ts>
                  <nts id="Seg_4070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T961" id="Seg_4072" n="HIAT:w" s="T960">nagur</ts>
                  <nts id="Seg_4073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T962" id="Seg_4075" n="HIAT:w" s="T961">uju</ts>
                  <nts id="Seg_4076" n="HIAT:ip">,</nts>
                  <nts id="Seg_4077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T963" id="Seg_4079" n="HIAT:w" s="T962">a</ts>
                  <nts id="Seg_4080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964" id="Seg_4082" n="HIAT:w" s="T963">teʔtə</ts>
                  <nts id="Seg_4083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_4085" n="HIAT:w" s="T964">uju</ts>
                  <nts id="Seg_4086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T966" id="Seg_4088" n="HIAT:w" s="T965">naga</ts>
                  <nts id="Seg_4089" n="HIAT:ip">.</nts>
                  <nts id="Seg_4090" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T969" id="Seg_4092" n="HIAT:u" s="T966">
                  <ts e="T966.tx-PKZ.1" id="Seg_4094" n="HIAT:w" s="T966">Четвертой</ts>
                  <nts id="Seg_4095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T966.tx-PKZ.2" id="Seg_4097" n="HIAT:w" s="T966.tx-PKZ.1">ноги</ts>
                  <nts id="Seg_4098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T969" id="Seg_4100" n="HIAT:w" s="T966.tx-PKZ.2">нету</ts>
                  <nts id="Seg_4101" n="HIAT:ip">.</nts>
                  <nts id="Seg_4102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T975" id="Seg_4104" n="HIAT:u" s="T969">
                  <ts e="T970" id="Seg_4106" n="HIAT:w" s="T969">Tăn</ts>
                  <nts id="Seg_4107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T971" id="Seg_4109" n="HIAT:w" s="T970">măna</ts>
                  <nts id="Seg_4110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T972" id="Seg_4112" n="HIAT:w" s="T971">münörəʔ</ts>
                  <nts id="Seg_4113" n="HIAT:ip">,</nts>
                  <nts id="Seg_4114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T973" id="Seg_4116" n="HIAT:w" s="T972">a</ts>
                  <nts id="Seg_4117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T974" id="Seg_4119" n="HIAT:w" s="T973">ĭmbiziʔ</ts>
                  <nts id="Seg_4120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T975" id="Seg_4122" n="HIAT:w" s="T974">münörlaʔbəl</ts>
                  <nts id="Seg_4123" n="HIAT:ip">?</nts>
                  <nts id="Seg_4124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T982" id="Seg_4126" n="HIAT:u" s="T975">
                  <ts e="T976" id="Seg_4128" n="HIAT:w" s="T975">Muzuruksiʔ</ts>
                  <nts id="Seg_4129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T977" id="Seg_4131" n="HIAT:w" s="T976">iʔ</ts>
                  <nts id="Seg_4132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T978" id="Seg_4134" n="HIAT:w" s="T977">münörəʔ</ts>
                  <nts id="Seg_4135" n="HIAT:ip">,</nts>
                  <nts id="Seg_4136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T978.tx-PKZ.1" id="Seg_4138" n="HIAT:w" s="T978">a</ts>
                  <nts id="Seg_4139" n="HIAT:ip">_</nts>
                  <ts e="T979" id="Seg_4141" n="HIAT:w" s="T978.tx-PKZ.1">to</ts>
                  <nts id="Seg_4142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T980" id="Seg_4144" n="HIAT:w" s="T979">măna</ts>
                  <nts id="Seg_4145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T981" id="Seg_4147" n="HIAT:w" s="T980">ĭzemnəj</ts>
                  <nts id="Seg_4148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T982" id="Seg_4150" n="HIAT:w" s="T981">bar</ts>
                  <nts id="Seg_4151" n="HIAT:ip">.</nts>
                  <nts id="Seg_4152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PKZ">
            <ts e="T13" id="Seg_4153" n="sc" s="T4">
               <ts e="T5" id="Seg_4155" n="e" s="T4">((DMG)) </ts>
               <ts e="T6" id="Seg_4157" n="e" s="T5">Я </ts>
               <ts e="T7" id="Seg_4159" n="e" s="T6">сперва </ts>
               <ts e="T8" id="Seg_4161" n="e" s="T7">расскажу, </ts>
               <ts e="T9" id="Seg_4163" n="e" s="T8">погоди, </ts>
               <ts e="T10" id="Seg_4165" n="e" s="T9">не </ts>
               <ts e="T13" id="Seg_4167" n="e" s="T10">сейчас… </ts>
            </ts>
            <ts e="T42" id="Seg_4168" n="sc" s="T14">
               <ts e="T15" id="Seg_4170" n="e" s="T14">Dʼijenə </ts>
               <ts e="T16" id="Seg_4172" n="e" s="T15">teinen </ts>
               <ts e="T17" id="Seg_4174" n="e" s="T16">kallam, </ts>
               <ts e="T18" id="Seg_4176" n="e" s="T17">(in </ts>
               <ts e="T19" id="Seg_4178" n="e" s="T18">-) </ts>
               <ts e="T20" id="Seg_4180" n="e" s="T19">ineziʔ. </ts>
               <ts e="T21" id="Seg_4182" n="e" s="T20">Gijen </ts>
               <ts e="T22" id="Seg_4184" n="e" s="T21">băra, </ts>
               <ts e="T23" id="Seg_4186" n="e" s="T22">nada </ts>
               <ts e="T24" id="Seg_4188" n="e" s="T23">ipek </ts>
               <ts e="T25" id="Seg_4190" n="e" s="T24">enzittə, </ts>
               <ts e="T26" id="Seg_4192" n="e" s="T25">tus </ts>
               <ts e="T27" id="Seg_4194" n="e" s="T26">enzittə, </ts>
               <ts e="T28" id="Seg_4196" n="e" s="T27">šamnak </ts>
               <ts e="T29" id="Seg_4198" n="e" s="T28">nada </ts>
               <ts e="T30" id="Seg_4200" n="e" s="T29">enzittə. </ts>
               <ts e="T31" id="Seg_4202" n="e" s="T30">Nada </ts>
               <ts e="T32" id="Seg_4204" n="e" s="T31">uja </ts>
               <ts e="T33" id="Seg_4206" n="e" s="T32">enzittə. </ts>
               <ts e="T34" id="Seg_4208" n="e" s="T33">Nada </ts>
               <ts e="T35" id="Seg_4210" n="e" s="T34">ipek </ts>
               <ts e="T36" id="Seg_4212" n="e" s="T35">enzittə </ts>
               <ts e="T37" id="Seg_4214" n="e" s="T36">(naʔ </ts>
               <ts e="T38" id="Seg_4216" n="e" s="T37">-). </ts>
               <ts e="T42" id="Seg_4218" n="e" s="T38">Говорю, господи прости! </ts>
            </ts>
            <ts e="T74" id="Seg_4219" n="sc" s="T46">
               <ts e="T49" id="Seg_4221" n="e" s="T46">Ipek </ts>
               <ts e="T50" id="Seg_4223" n="e" s="T49">enzittə </ts>
               <ts e="T51" id="Seg_4225" n="e" s="T50">nada. </ts>
               <ts e="T52" id="Seg_4227" n="e" s="T51">Kajak </ts>
               <ts e="T53" id="Seg_4229" n="e" s="T52">enzittə </ts>
               <ts e="T54" id="Seg_4231" n="e" s="T53">nada. </ts>
               <ts e="T55" id="Seg_4233" n="e" s="T54">Munujʔ </ts>
               <ts e="T56" id="Seg_4235" n="e" s="T55">enzittə. </ts>
               <ts e="T57" id="Seg_4237" n="e" s="T56">Šamnak </ts>
               <ts e="T58" id="Seg_4239" n="e" s="T57">nada </ts>
               <ts e="T59" id="Seg_4241" n="e" s="T58">enzittə. </ts>
               <ts e="T60" id="Seg_4243" n="e" s="T59">Nada </ts>
               <ts e="T61" id="Seg_4245" n="e" s="T60">aspaʔ </ts>
               <ts e="T62" id="Seg_4247" n="e" s="T61">enzittə. </ts>
               <ts e="T63" id="Seg_4249" n="e" s="T62">Men </ts>
               <ts e="T64" id="Seg_4251" n="e" s="T63">izittə </ts>
               <ts e="T65" id="Seg_4253" n="e" s="T64">nada. </ts>
               <ts e="T66" id="Seg_4255" n="e" s="T65">Multuk </ts>
               <ts e="T67" id="Seg_4257" n="e" s="T66">izittə </ts>
               <ts e="T68" id="Seg_4259" n="e" s="T67">nada. </ts>
               <ts e="T69" id="Seg_4261" n="e" s="T68">Nada </ts>
               <ts e="T70" id="Seg_4263" n="e" s="T69">baltu </ts>
               <ts e="T71" id="Seg_4265" n="e" s="T70">izittə. </ts>
               <ts e="T72" id="Seg_4267" n="e" s="T71">Dagaj </ts>
               <ts e="T73" id="Seg_4269" n="e" s="T72">izittə </ts>
               <ts e="T74" id="Seg_4271" n="e" s="T73">nada. </ts>
            </ts>
            <ts e="T86" id="Seg_4272" n="sc" s="T85">
               <ts e="T86" id="Seg_4274" n="e" s="T85">((kukganʼanʼi)). </ts>
            </ts>
            <ts e="T402" id="Seg_4275" n="sc" s="T90">
               <ts e="T91" id="Seg_4277" n="e" s="T90">Nu </ts>
               <ts e="T92" id="Seg_4279" n="e" s="T91">kuzasogə </ts>
               <ts e="T93" id="Seg_4281" n="e" s="T92">šonəgaʔi </ts>
               <ts e="T94" id="Seg_4283" n="e" s="T93">dʼăbaktərzittə. </ts>
               <ts e="T95" id="Seg_4285" n="e" s="T94">Amnaʔ! </ts>
               <ts e="T96" id="Seg_4287" n="e" s="T95">Dʼăbaktərləbaʔ! </ts>
               <ts e="T97" id="Seg_4289" n="e" s="T96">Amorzittə </ts>
               <ts e="T98" id="Seg_4291" n="e" s="T97">padʼi </ts>
               <ts e="T99" id="Seg_4293" n="e" s="T98">axota, </ts>
               <ts e="T100" id="Seg_4295" n="e" s="T99">amnaʔ, </ts>
               <ts e="T101" id="Seg_4297" n="e" s="T100">amoraʔ! </ts>
               <ts e="T102" id="Seg_4299" n="e" s="T101">Dĭn </ts>
               <ts e="T103" id="Seg_4301" n="e" s="T102">ipek </ts>
               <ts e="T104" id="Seg_4303" n="e" s="T103">iʔbölaʔbə. </ts>
               <ts e="T105" id="Seg_4305" n="e" s="T104">Kajak </ts>
               <ts e="T106" id="Seg_4307" n="e" s="T105">nuga. </ts>
               <ts e="T107" id="Seg_4309" n="e" s="T106">Šamnak </ts>
               <ts e="T108" id="Seg_4311" n="e" s="T107">ige, </ts>
               <ts e="T109" id="Seg_4313" n="e" s="T108">tus </ts>
               <ts e="T110" id="Seg_4315" n="e" s="T109">ige, </ts>
               <ts e="T111" id="Seg_4317" n="e" s="T110">munujʔ, </ts>
               <ts e="T112" id="Seg_4319" n="e" s="T111">uja, </ts>
               <ts e="T113" id="Seg_4321" n="e" s="T112">kola, </ts>
               <ts e="T114" id="Seg_4323" n="e" s="T113">amaʔ </ts>
               <ts e="T115" id="Seg_4325" n="e" s="T114">bar! </ts>
               <ts e="T116" id="Seg_4327" n="e" s="T115">Iʔ </ts>
               <ts e="T117" id="Seg_4329" n="e" s="T116">alomaʔ! </ts>
               <ts e="T118" id="Seg_4331" n="e" s="T117">Iʔ </ts>
               <ts e="T119" id="Seg_4333" n="e" s="T118">kirgaːraʔ! </ts>
               <ts e="T120" id="Seg_4335" n="e" s="T119">A_to </ts>
               <ts e="T121" id="Seg_4337" n="e" s="T120">münörlem </ts>
               <ts e="T122" id="Seg_4339" n="e" s="T121">tănan! </ts>
               <ts e="T123" id="Seg_4341" n="e" s="T122">Iʔ </ts>
               <ts e="T124" id="Seg_4343" n="e" s="T123">dʼoraʔ! </ts>
               <ts e="T125" id="Seg_4345" n="e" s="T124">Iʔ </ts>
               <ts e="T126" id="Seg_4347" n="e" s="T125">kuroːmaʔ! </ts>
               <ts e="T127" id="Seg_4349" n="e" s="T126">Iʔ </ts>
               <ts e="T128" id="Seg_4351" n="e" s="T127">šaːmaʔ! </ts>
               <ts e="T129" id="Seg_4353" n="e" s="T128">Amnoːlaʔbəl </ts>
               <ts e="T130" id="Seg_4355" n="e" s="T129">bar, </ts>
               <ts e="T131" id="Seg_4357" n="e" s="T130">šaːmnaʔbəl! </ts>
               <ts e="T132" id="Seg_4359" n="e" s="T131">Tăn </ts>
               <ts e="T133" id="Seg_4361" n="e" s="T132">ugaːndə </ts>
               <ts e="T134" id="Seg_4363" n="e" s="T133">iʔgö </ts>
               <ts e="T135" id="Seg_4365" n="e" s="T134">šaːmnaʔbəl. </ts>
               <ts e="T136" id="Seg_4367" n="e" s="T135">Все, </ts>
               <ts e="T137" id="Seg_4369" n="e" s="T136">наверное. </ts>
               <ts e="T138" id="Seg_4371" n="e" s="T137">Măn </ts>
               <ts e="T139" id="Seg_4373" n="e" s="T138">tüjö </ts>
               <ts e="T140" id="Seg_4375" n="e" s="T139">kallam </ts>
               <ts e="T141" id="Seg_4377" n="e" s="T140">šumuranə. </ts>
               <ts e="T142" id="Seg_4379" n="e" s="T141">A </ts>
               <ts e="T143" id="Seg_4381" n="e" s="T142">tăn </ts>
               <ts e="T144" id="Seg_4383" n="e" s="T143">maʔnəl </ts>
               <ts e="T145" id="Seg_4385" n="e" s="T144">amnoʔ! </ts>
               <ts e="T146" id="Seg_4387" n="e" s="T145">Iʔ </ts>
               <ts e="T147" id="Seg_4389" n="e" s="T146">alomaʔ! </ts>
               <ts e="T148" id="Seg_4391" n="e" s="T147">Măn </ts>
               <ts e="T149" id="Seg_4393" n="e" s="T148">ĭmbi-nʼibudʼ </ts>
               <ts e="T150" id="Seg_4395" n="e" s="T149">tănan </ts>
               <ts e="T151" id="Seg_4397" n="e" s="T150">detlem. </ts>
               <ts e="T152" id="Seg_4399" n="e" s="T151">Dĭgəttə </ts>
               <ts e="T153" id="Seg_4401" n="e" s="T152">mĭlem </ts>
               <ts e="T154" id="Seg_4403" n="e" s="T153">tănan. </ts>
               <ts e="T155" id="Seg_4405" n="e" s="T154">Taːldʼen </ts>
               <ts e="T156" id="Seg_4407" n="e" s="T155">(di </ts>
               <ts e="T157" id="Seg_4409" n="e" s="T156">-) </ts>
               <ts e="T158" id="Seg_4411" n="e" s="T157">taːldʼen </ts>
               <ts e="T159" id="Seg_4413" n="e" s="T158">šobi </ts>
               <ts e="T160" id="Seg_4415" n="e" s="T159">măn </ts>
               <ts e="T161" id="Seg_4417" n="e" s="T160">tuganbə. </ts>
               <ts e="T162" id="Seg_4419" n="e" s="T161">Măn </ts>
               <ts e="T163" id="Seg_4421" n="e" s="T162">mămbiam: </ts>
               <ts e="T164" id="Seg_4423" n="e" s="T163">iʔbeʔ </ts>
               <ts e="T165" id="Seg_4425" n="e" s="T164">kunoːlzittə! </ts>
               <ts e="T166" id="Seg_4427" n="e" s="T165">Dĭgəttə </ts>
               <ts e="T167" id="Seg_4429" n="e" s="T166">mămbiam: </ts>
               <ts e="T168" id="Seg_4431" n="e" s="T167">amoraʔ, </ts>
               <ts e="T169" id="Seg_4433" n="e" s="T168">padʼi </ts>
               <ts e="T170" id="Seg_4435" n="e" s="T169">amorzittə </ts>
               <ts e="T171" id="Seg_4437" n="e" s="T170">axota. </ts>
               <ts e="T172" id="Seg_4439" n="e" s="T171">Dĭ </ts>
               <ts e="T173" id="Seg_4441" n="e" s="T172">mămbi: </ts>
               <ts e="T174" id="Seg_4443" n="e" s="T173">(măn) </ts>
               <ts e="T175" id="Seg_4445" n="e" s="T174">măn </ts>
               <ts e="T176" id="Seg_4447" n="e" s="T175">ej </ts>
               <ts e="T177" id="Seg_4449" n="e" s="T176">(amori </ts>
               <ts e="T178" id="Seg_4451" n="e" s="T177">-) </ts>
               <ts e="T179" id="Seg_4453" n="e" s="T178">amorzittə </ts>
               <ts e="T180" id="Seg_4455" n="e" s="T179">((n)). </ts>
               <ts e="T181" id="Seg_4457" n="e" s="T180">Măn </ts>
               <ts e="T182" id="Seg_4459" n="e" s="T181">turagən </ts>
               <ts e="T183" id="Seg_4461" n="e" s="T182">ambiam </ts>
               <ts e="T184" id="Seg_4463" n="e" s="T183">dĭn. </ts>
               <ts e="T185" id="Seg_4465" n="e" s="T184">Dĭgəttə </ts>
               <ts e="T186" id="Seg_4467" n="e" s="T185">(iʔbə </ts>
               <ts e="T187" id="Seg_4469" n="e" s="T186">-) </ts>
               <ts e="T188" id="Seg_4471" n="e" s="T187">iʔbəbi </ts>
               <ts e="T189" id="Seg_4473" n="e" s="T188">kunoːlzittə. </ts>
               <ts e="T190" id="Seg_4475" n="e" s="T189">Măn </ts>
               <ts e="T191" id="Seg_4477" n="e" s="T190">iʔbəbiem. </ts>
               <ts e="T192" id="Seg_4479" n="e" s="T191">Erten </ts>
               <ts e="T193" id="Seg_4481" n="e" s="T192">uʔbdəbiam. </ts>
               <ts e="T194" id="Seg_4483" n="e" s="T193">Šumuranə </ts>
               <ts e="T195" id="Seg_4485" n="e" s="T194">mĭmbiem. </ts>
               <ts e="T196" id="Seg_4487" n="e" s="T195">Šobiam. </ts>
               <ts e="T197" id="Seg_4489" n="e" s="T196">Dĭʔnə </ts>
               <ts e="T198" id="Seg_4491" n="e" s="T197">mămbiam: </ts>
               <ts e="T199" id="Seg_4493" n="e" s="T198">uʔbdaʔ! </ts>
               <ts e="T200" id="Seg_4495" n="e" s="T199">Amoraʔ, </ts>
               <ts e="T201" id="Seg_4497" n="e" s="T200">dĭn </ts>
               <ts e="T202" id="Seg_4499" n="e" s="T201">namzəga </ts>
               <ts e="T203" id="Seg_4501" n="e" s="T202">(su </ts>
               <ts e="T204" id="Seg_4503" n="e" s="T203">ig </ts>
               <ts e="T205" id="Seg_4505" n="e" s="T204">-) </ts>
               <ts e="T206" id="Seg_4507" n="e" s="T205">süt </ts>
               <ts e="T207" id="Seg_4509" n="e" s="T206">ige. </ts>
               <ts e="T208" id="Seg_4511" n="e" s="T207">Keʔbde </ts>
               <ts e="T209" id="Seg_4513" n="e" s="T208">ige, </ts>
               <ts e="T210" id="Seg_4515" n="e" s="T209">munujʔ, </ts>
               <ts e="T211" id="Seg_4517" n="e" s="T210">ipek. </ts>
               <ts e="T212" id="Seg_4519" n="e" s="T211">Amoraʔ, </ts>
               <ts e="T213" id="Seg_4521" n="e" s="T212">amnaʔ. </ts>
               <ts e="T214" id="Seg_4523" n="e" s="T213">(Dĭ </ts>
               <ts e="T215" id="Seg_4525" n="e" s="T214">ba </ts>
               <ts e="T216" id="Seg_4527" n="e" s="T215">-) </ts>
               <ts e="T217" id="Seg_4529" n="e" s="T216">dĭ </ts>
               <ts e="T218" id="Seg_4531" n="e" s="T217">uʔpi. </ts>
               <ts e="T219" id="Seg_4533" n="e" s="T218">Bozəjzəbi. </ts>
               <ts e="T220" id="Seg_4535" n="e" s="T219">Dĭgəttə </ts>
               <ts e="T221" id="Seg_4537" n="e" s="T220">amnobi </ts>
               <ts e="T222" id="Seg_4539" n="e" s="T221">amorzittə. </ts>
               <ts e="T223" id="Seg_4541" n="e" s="T222">Всё. </ts>
               <ts e="T224" id="Seg_4543" n="e" s="T223">Dĭgəttə </ts>
               <ts e="T225" id="Seg_4545" n="e" s="T224">kala </ts>
               <ts e="T226" id="Seg_4547" n="e" s="T225">dʼürbi. </ts>
               <ts e="T227" id="Seg_4549" n="e" s="T226">A </ts>
               <ts e="T228" id="Seg_4551" n="e" s="T227">măn </ts>
               <ts e="T229" id="Seg_4553" n="e" s="T228">bozəjbiam. </ts>
               <ts e="T230" id="Seg_4555" n="e" s="T229">Ну, </ts>
               <ts e="T231" id="Seg_4557" n="e" s="T230">все </ts>
               <ts e="T232" id="Seg_4559" n="e" s="T231">((…)). </ts>
               <ts e="T233" id="Seg_4561" n="e" s="T232">Taːldʼen </ts>
               <ts e="T234" id="Seg_4563" n="e" s="T233">măn </ts>
               <ts e="T235" id="Seg_4565" n="e" s="T234">miʔ </ts>
               <ts e="T236" id="Seg_4567" n="e" s="T235">tuʔluʔ. </ts>
               <ts e="T237" id="Seg_4569" n="e" s="T236">(Tu) </ts>
               <ts e="T238" id="Seg_4571" n="e" s="T237">tažerbibaʔ, </ts>
               <ts e="T239" id="Seg_4573" n="e" s="T238">ineziʔ. </ts>
               <ts e="T240" id="Seg_4575" n="e" s="T239">Šide </ts>
               <ts e="T241" id="Seg_4577" n="e" s="T240">koʔbdo </ts>
               <ts e="T242" id="Seg_4579" n="e" s="T241">embiʔi. </ts>
               <ts e="T243" id="Seg_4581" n="e" s="T242">Onʼiʔ </ts>
               <ts e="T244" id="Seg_4583" n="e" s="T243">tibi </ts>
               <ts e="T245" id="Seg_4585" n="e" s="T244">măn </ts>
               <ts e="T246" id="Seg_4587" n="e" s="T245">(bospəm) </ts>
               <ts e="T247" id="Seg_4589" n="e" s="T246">(u </ts>
               <ts e="T248" id="Seg_4591" n="e" s="T247">-) </ts>
               <ts e="T249" id="Seg_4593" n="e" s="T248">embiem. </ts>
               <ts e="T250" id="Seg_4595" n="e" s="T249">Tažerbibaʔ. </ts>
               <ts e="T251" id="Seg_4597" n="e" s="T250">Погоди, </ts>
               <ts e="T253" id="Seg_4599" n="e" s="T251">как </ts>
               <ts e="T254" id="Seg_4601" n="e" s="T253">я </ts>
               <ts e="T255" id="Seg_4603" n="e" s="T254">это </ts>
               <ts e="T256" id="Seg_4605" n="e" s="T255">забыла. </ts>
               <ts e="T257" id="Seg_4607" n="e" s="T256">Măn </ts>
               <ts e="T258" id="Seg_4609" n="e" s="T257">karəldʼaːn </ts>
               <ts e="T259" id="Seg_4611" n="e" s="T258">multʼanə </ts>
               <ts e="T260" id="Seg_4613" n="e" s="T259">mĭmbiem. </ts>
               <ts e="T261" id="Seg_4615" n="e" s="T260">Bozəjbiam, </ts>
               <ts e="T262" id="Seg_4617" n="e" s="T261">šišəge </ts>
               <ts e="T263" id="Seg_4619" n="e" s="T262">bü </ts>
               <ts e="T264" id="Seg_4621" n="e" s="T263">ibi. </ts>
               <ts e="T265" id="Seg_4623" n="e" s="T264">Dʼibige </ts>
               <ts e="T266" id="Seg_4625" n="e" s="T265">bü. </ts>
               <ts e="T267" id="Seg_4627" n="e" s="T266">Saːbən </ts>
               <ts e="T268" id="Seg_4629" n="e" s="T267">ibiem. </ts>
               <ts e="T269" id="Seg_4631" n="e" s="T268">Kujnek </ts>
               <ts e="T270" id="Seg_4633" n="e" s="T269">ibiem. </ts>
               <ts e="T271" id="Seg_4635" n="e" s="T270">Multʼagəʔ </ts>
               <ts e="T272" id="Seg_4637" n="e" s="T271">šobiam, </ts>
               <ts e="T273" id="Seg_4639" n="e" s="T272">tüžöj </ts>
               <ts e="T274" id="Seg_4641" n="e" s="T273">šobi. </ts>
               <ts e="T275" id="Seg_4643" n="e" s="T274">Măn </ts>
               <ts e="T276" id="Seg_4645" n="e" s="T275">surdobiam. </ts>
               <ts e="T277" id="Seg_4647" n="e" s="T276">Ugaːndə </ts>
               <ts e="T278" id="Seg_4649" n="e" s="T277">taraːruʔbiem. </ts>
               <ts e="T279" id="Seg_4651" n="e" s="T278">Saʔməluʔpam </ts>
               <ts e="T280" id="Seg_4653" n="e" s="T279">(i) </ts>
               <ts e="T281" id="Seg_4655" n="e" s="T280">i </ts>
               <ts e="T282" id="Seg_4657" n="e" s="T281">kunoːlbiam. </ts>
               <ts e="T283" id="Seg_4659" n="e" s="T282">Teinen </ts>
               <ts e="T284" id="Seg_4661" n="e" s="T283">noʔ </ts>
               <ts e="T285" id="Seg_4663" n="e" s="T284">măn </ts>
               <ts e="T286" id="Seg_4665" n="e" s="T285">kallam </ts>
               <ts e="T287" id="Seg_4667" n="e" s="T286">beškejleʔ, </ts>
               <ts e="T288" id="Seg_4669" n="e" s="T287">beške </ts>
               <ts e="T289" id="Seg_4671" n="e" s="T288">detlem. </ts>
               <ts e="T290" id="Seg_4673" n="e" s="T289">Dĭgəttə </ts>
               <ts e="T291" id="Seg_4675" n="e" s="T290">bozlam. </ts>
               <ts e="T292" id="Seg_4677" n="e" s="T291">Tustʼarlim </ts>
               <ts e="T293" id="Seg_4679" n="e" s="T292">bar. </ts>
               <ts e="T294" id="Seg_4681" n="e" s="T293">Dĭgəttə </ts>
               <ts e="T295" id="Seg_4683" n="e" s="T294">tustʼarləj </ts>
               <ts e="T296" id="Seg_4685" n="e" s="T295">bar. </ts>
               <ts e="T297" id="Seg_4687" n="e" s="T296">Dĭgəttə </ts>
               <ts e="T298" id="Seg_4689" n="e" s="T297">amzittə </ts>
               <ts e="T299" id="Seg_4691" n="e" s="T298">i </ts>
               <ts e="T300" id="Seg_4693" n="e" s="T299">možna. </ts>
               <ts e="T301" id="Seg_4695" n="e" s="T300">Koŋ </ts>
               <ts e="T302" id="Seg_4697" n="e" s="T301">bar </ts>
               <ts e="T303" id="Seg_4699" n="e" s="T302">amnoːlaʔpi </ts>
               <ts e="T304" id="Seg_4701" n="e" s="T303">bar, </ts>
               <ts e="T305" id="Seg_4703" n="e" s="T304">ugaːndə </ts>
               <ts e="T306" id="Seg_4705" n="e" s="T305">jezerik. </ts>
               <ts e="T307" id="Seg_4707" n="e" s="T306">Ara </ts>
               <ts e="T308" id="Seg_4709" n="e" s="T307">bĭʔpi, </ts>
               <ts e="T309" id="Seg_4711" n="e" s="T308">ugaːndə </ts>
               <ts e="T310" id="Seg_4713" n="e" s="T309">iʔgö </ts>
               <ts e="T311" id="Seg_4715" n="e" s="T310">bĭʔpi. </ts>
               <ts e="T312" id="Seg_4717" n="e" s="T311">Saʔməluʔpi </ts>
               <ts e="T313" id="Seg_4719" n="e" s="T312">bar, </ts>
               <ts e="T314" id="Seg_4721" n="e" s="T313">mut-mat </ts>
               <ts e="T315" id="Seg_4723" n="e" s="T314">(kudo-) </ts>
               <ts e="T316" id="Seg_4725" n="e" s="T315">kudonzlaʔbə. </ts>
               <ts e="T317" id="Seg_4727" n="e" s="T316">Măn </ts>
               <ts e="T318" id="Seg_4729" n="e" s="T317">dĭm </ts>
               <ts e="T319" id="Seg_4731" n="e" s="T318">amnəːlbiam. </ts>
               <ts e="T320" id="Seg_4733" n="e" s="T319">Amnoʔ, </ts>
               <ts e="T321" id="Seg_4735" n="e" s="T320">mămbiam. </ts>
               <ts e="T322" id="Seg_4737" n="e" s="T321">Măn </ts>
               <ts e="T323" id="Seg_4739" n="e" s="T322">mămbiam: </ts>
               <ts e="T324" id="Seg_4741" n="e" s="T323">Iʔ </ts>
               <ts e="T325" id="Seg_4743" n="e" s="T324">kudonzaʔ, </ts>
               <ts e="T326" id="Seg_4745" n="e" s="T325">amnoʔ! </ts>
               <ts e="T327" id="Seg_4747" n="e" s="T326">Amnoʔ </ts>
               <ts e="T328" id="Seg_4749" n="e" s="T327">segi </ts>
               <ts e="T329" id="Seg_4751" n="e" s="T328">bü </ts>
               <ts e="T330" id="Seg_4753" n="e" s="T329">bĭssittə. </ts>
               <ts e="T331" id="Seg_4755" n="e" s="T330">Sĭreʔpne </ts>
               <ts e="T332" id="Seg_4757" n="e" s="T331">iʔbölaʔbə, </ts>
               <ts e="T333" id="Seg_4759" n="e" s="T332">ende </ts>
               <ts e="T334" id="Seg_4761" n="e" s="T333">segi </ts>
               <ts e="T335" id="Seg_4763" n="e" s="T334">bütnə. </ts>
               <ts e="T336" id="Seg_4765" n="e" s="T335">Munuʔi </ts>
               <ts e="T337" id="Seg_4767" n="e" s="T336">iʔbölaʔbəʔjə. </ts>
               <ts e="T338" id="Seg_4769" n="e" s="T337">(Ikə </ts>
               <ts e="T339" id="Seg_4771" n="e" s="T338">-) </ts>
               <ts e="T340" id="Seg_4773" n="e" s="T339">Ipek </ts>
               <ts e="T341" id="Seg_4775" n="e" s="T340">amaʔ! </ts>
               <ts e="T342" id="Seg_4777" n="e" s="T341">Uja </ts>
               <ts e="T343" id="Seg_4779" n="e" s="T342">amaʔ! </ts>
               <ts e="T344" id="Seg_4781" n="e" s="T343">Kola </ts>
               <ts e="T345" id="Seg_4783" n="e" s="T344">amaʔ! </ts>
               <ts e="T346" id="Seg_4785" n="e" s="T345">Munuj </ts>
               <ts e="T347" id="Seg_4787" n="e" s="T346">iʔbölaʔbə, </ts>
               <ts e="T348" id="Seg_4789" n="e" s="T347">amaʔ! </ts>
               <ts e="T349" id="Seg_4791" n="e" s="T348">Kajaʔ </ts>
               <ts e="T350" id="Seg_4793" n="e" s="T349">amaʔ, </ts>
               <ts e="T351" id="Seg_4795" n="e" s="T350">oroma </ts>
               <ts e="T352" id="Seg_4797" n="e" s="T351">amaʔ! </ts>
               <ts e="T353" id="Seg_4799" n="e" s="T352">Namzəga </ts>
               <ts e="T354" id="Seg_4801" n="e" s="T353">süt </ts>
               <ts e="T355" id="Seg_4803" n="e" s="T354">amaʔ! </ts>
               <ts e="T356" id="Seg_4805" n="e" s="T355">Kola </ts>
               <ts e="T357" id="Seg_4807" n="e" s="T356">amaʔ! </ts>
               <ts e="T358" id="Seg_4809" n="e" s="T357">Хватит. </ts>
               <ts e="T359" id="Seg_4811" n="e" s="T358">Nada </ts>
               <ts e="T360" id="Seg_4813" n="e" s="T359">măna </ts>
               <ts e="T361" id="Seg_4815" n="e" s="T360">jama </ts>
               <ts e="T362" id="Seg_4817" n="e" s="T361">šöʔsittə. </ts>
               <ts e="T363" id="Seg_4819" n="e" s="T362">Parga </ts>
               <ts e="T364" id="Seg_4821" n="e" s="T363">šöʔsittə. </ts>
               <ts e="T365" id="Seg_4823" n="e" s="T364">Nada </ts>
               <ts e="T366" id="Seg_4825" n="e" s="T365">(kujnet </ts>
               <ts e="T367" id="Seg_4827" n="e" s="T366">-) </ts>
               <ts e="T368" id="Seg_4829" n="e" s="T367">kujnek </ts>
               <ts e="T369" id="Seg_4831" n="e" s="T368">šöʔsittə, </ts>
               <ts e="T370" id="Seg_4833" n="e" s="T369">izittə. </ts>
               <ts e="T371" id="Seg_4835" n="e" s="T370">(Na </ts>
               <ts e="T372" id="Seg_4837" n="e" s="T371">-) </ts>
               <ts e="T373" id="Seg_4839" n="e" s="T372">Nada </ts>
               <ts e="T374" id="Seg_4841" n="e" s="T373">piʔmə </ts>
               <ts e="T375" id="Seg_4843" n="e" s="T374">šöʔsittə. </ts>
               <ts e="T376" id="Seg_4845" n="e" s="T375">A_to </ts>
               <ts e="T377" id="Seg_4847" n="e" s="T376">măn </ts>
               <ts e="T378" id="Seg_4849" n="e" s="T377">nʼim </ts>
               <ts e="T379" id="Seg_4851" n="e" s="T378">bar </ts>
               <ts e="T380" id="Seg_4853" n="e" s="T379">šerzittə </ts>
               <ts e="T381" id="Seg_4855" n="e" s="T380">naga </ts>
               <ts e="T382" id="Seg_4857" n="e" s="T381">ĭmbi. </ts>
               <ts e="T383" id="Seg_4859" n="e" s="T382">Ĭmbidə </ts>
               <ts e="T384" id="Seg_4861" n="e" s="T383">naga. </ts>
               <ts e="T385" id="Seg_4863" n="e" s="T384">Măn </ts>
               <ts e="T386" id="Seg_4865" n="e" s="T385">kallam </ts>
               <ts e="T387" id="Seg_4867" n="e" s="T386">Kazan </ts>
               <ts e="T388" id="Seg_4869" n="e" s="T387">turanə </ts>
               <ts e="T389" id="Seg_4871" n="e" s="T388">i </ts>
               <ts e="T390" id="Seg_4873" n="e" s="T389">nada </ts>
               <ts e="T391" id="Seg_4875" n="e" s="T390">ipek </ts>
               <ts e="T392" id="Seg_4877" n="e" s="T391">izittə. </ts>
               <ts e="T393" id="Seg_4879" n="e" s="T392">Nada </ts>
               <ts e="T394" id="Seg_4881" n="e" s="T393">sĭreʔpne </ts>
               <ts e="T395" id="Seg_4883" n="e" s="T394">izittə. </ts>
               <ts e="T396" id="Seg_4885" n="e" s="T395">Nada </ts>
               <ts e="T397" id="Seg_4887" n="e" s="T396">(mul </ts>
               <ts e="T398" id="Seg_4889" n="e" s="T397">-) </ts>
               <ts e="T399" id="Seg_4891" n="e" s="T398">multukdə </ts>
               <ts e="T400" id="Seg_4893" n="e" s="T399">ipek </ts>
               <ts e="T401" id="Seg_4895" n="e" s="T400">izittə. </ts>
               <ts e="T402" id="Seg_4897" n="e" s="T401">Nada. </ts>
            </ts>
            <ts e="T571" id="Seg_4898" n="sc" s="T408">
               <ts e="T409" id="Seg_4900" n="e" s="T408">No, </ts>
               <ts e="T410" id="Seg_4902" n="e" s="T409">ipek. </ts>
               <ts e="T411" id="Seg_4904" n="e" s="T410">Ну, </ts>
               <ts e="T412" id="Seg_4906" n="e" s="T411">все </ts>
               <ts e="T413" id="Seg_4908" n="e" s="T412">DMG. </ts>
               <ts e="T414" id="Seg_4910" n="e" s="T413">((DMG)) </ts>
               <ts e="T415" id="Seg_4912" n="e" s="T414">думала. </ts>
               <ts e="T416" id="Seg_4914" n="e" s="T415">Măn </ts>
               <ts e="T417" id="Seg_4916" n="e" s="T416">dʼijenə </ts>
               <ts e="T418" id="Seg_4918" n="e" s="T417">dĭgəttə </ts>
               <ts e="T419" id="Seg_4920" n="e" s="T418">kallam. </ts>
               <ts e="T420" id="Seg_4922" n="e" s="T419">Ну, </ts>
               <ts e="T421" id="Seg_4924" n="e" s="T420">все. </ts>
               <ts e="T422" id="Seg_4926" n="e" s="T421">Teinen </ts>
               <ts e="T423" id="Seg_4928" n="e" s="T422">(e </ts>
               <ts e="T424" id="Seg_4930" n="e" s="T423">-) </ts>
               <ts e="T425" id="Seg_4932" n="e" s="T424">dĭzeŋ </ts>
               <ts e="T426" id="Seg_4934" n="e" s="T425">dʼijegəʔ </ts>
               <ts e="T427" id="Seg_4936" n="e" s="T426">šoləʔi. </ts>
               <ts e="T428" id="Seg_4938" n="e" s="T427">Padʼi </ts>
               <ts e="T429" id="Seg_4940" n="e" s="T428">uja </ts>
               <ts e="T430" id="Seg_4942" n="e" s="T429">detleʔi. </ts>
               <ts e="T431" id="Seg_4944" n="e" s="T430">Kalba </ts>
               <ts e="T432" id="Seg_4946" n="e" s="T431">deʔleʔi. </ts>
               <ts e="T433" id="Seg_4948" n="e" s="T432">(Tʼagarlaʔ-) </ts>
               <ts e="T434" id="Seg_4950" n="e" s="T433">Tʼagarzittə </ts>
               <ts e="T435" id="Seg_4952" n="e" s="T434">nada </ts>
               <ts e="T436" id="Seg_4954" n="e" s="T435">da </ts>
               <ts e="T437" id="Seg_4956" n="e" s="T436">tustʼarzittə. </ts>
               <ts e="T438" id="Seg_4958" n="e" s="T437">Munujʔ </ts>
               <ts e="T439" id="Seg_4960" n="e" s="T438">enzittə, </ts>
               <ts e="T440" id="Seg_4962" n="e" s="T439">oroma </ts>
               <ts e="T441" id="Seg_4964" n="e" s="T440">enzittə </ts>
               <ts e="T442" id="Seg_4966" n="e" s="T441">i </ts>
               <ts e="T443" id="Seg_4968" n="e" s="T442">bü </ts>
               <ts e="T444" id="Seg_4970" n="e" s="T443">kămnasʼtə </ts>
               <ts e="T445" id="Seg_4972" n="e" s="T444">i </ts>
               <ts e="T446" id="Seg_4974" n="e" s="T445">amorzittə </ts>
               <ts e="T447" id="Seg_4976" n="e" s="T446">šamnakziʔ. </ts>
               <ts e="T448" id="Seg_4978" n="e" s="T447">Nada </ts>
               <ts e="T449" id="Seg_4980" n="e" s="T448">kanzittə </ts>
               <ts e="T450" id="Seg_4982" n="e" s="T449">(tol </ts>
               <ts e="T451" id="Seg_4984" n="e" s="T450">- </ts>
               <ts e="T452" id="Seg_4986" n="e" s="T451">to </ts>
               <ts e="T453" id="Seg_4988" n="e" s="T452">-) </ts>
               <ts e="T454" id="Seg_4990" n="e" s="T453">kanzittə </ts>
               <ts e="T455" id="Seg_4992" n="e" s="T454">(toli </ts>
               <ts e="T456" id="Seg_4994" n="e" s="T455">-). </ts>
               <ts e="T458" id="Seg_4996" n="e" s="T456">Погоди… </ts>
               <ts e="T459" id="Seg_4998" n="e" s="T458">Talerzittə, </ts>
               <ts e="T460" id="Seg_5000" n="e" s="T459">ipek </ts>
               <ts e="T461" id="Seg_5002" n="e" s="T460">kuʔsittə. </ts>
               <ts e="T462" id="Seg_5004" n="e" s="T461">Dĭgəttə </ts>
               <ts e="T463" id="Seg_5006" n="e" s="T462">özerləj, </ts>
               <ts e="T464" id="Seg_5008" n="e" s="T463">pădəsʼtə </ts>
               <ts e="T465" id="Seg_5010" n="e" s="T464">nada. </ts>
               <ts e="T466" id="Seg_5012" n="e" s="T465">Dĭgəttə </ts>
               <ts e="T467" id="Seg_5014" n="e" s="T466">toʔnarzittə </ts>
               <ts e="T468" id="Seg_5016" n="e" s="T467">nada. </ts>
               <ts e="T469" id="Seg_5018" n="e" s="T468">Dĭgəttə </ts>
               <ts e="T470" id="Seg_5020" n="e" s="T469">((PAUSE)) </ts>
               <ts e="T471" id="Seg_5022" n="e" s="T470">Погоди, </ts>
               <ts e="T473" id="Seg_5024" n="e" s="T471">вот </ts>
               <ts e="T474" id="Seg_5026" n="e" s="T473">((DMG)) </ts>
               <ts e="T475" id="Seg_5028" n="e" s="T474">пустил, </ts>
               <ts e="T476" id="Seg_5030" n="e" s="T475">dĭgəttə </ts>
               <ts e="T477" id="Seg_5032" n="e" s="T476">nʼeʔsittə </ts>
               <ts e="T478" id="Seg_5034" n="e" s="T477">nada. </ts>
               <ts e="T479" id="Seg_5036" n="e" s="T478">(Dĭgət </ts>
               <ts e="T480" id="Seg_5038" n="e" s="T479">-) </ts>
               <ts e="T481" id="Seg_5040" n="e" s="T480">Dĭgəttə </ts>
               <ts e="T482" id="Seg_5042" n="e" s="T481">nada </ts>
               <ts e="T483" id="Seg_5044" n="e" s="T482">pürzittə. </ts>
               <ts e="T484" id="Seg_5046" n="e" s="T483">Dĭgəttə </ts>
               <ts e="T485" id="Seg_5048" n="e" s="T484">amorzittə </ts>
               <ts e="T486" id="Seg_5050" n="e" s="T485">nada. </ts>
               <ts e="T488" id="Seg_5052" n="e" s="T486">Вот… </ts>
               <ts e="T489" id="Seg_5054" n="e" s="T488">Teinen </ts>
               <ts e="T490" id="Seg_5056" n="e" s="T489">urgo </ts>
               <ts e="T491" id="Seg_5058" n="e" s="T490">dʼala, </ts>
               <ts e="T492" id="Seg_5060" n="e" s="T491">(i </ts>
               <ts e="T493" id="Seg_5062" n="e" s="T492">-) </ts>
               <ts e="T494" id="Seg_5064" n="e" s="T493">il </ts>
               <ts e="T495" id="Seg_5066" n="e" s="T494">ugaːndə </ts>
               <ts e="T496" id="Seg_5068" n="e" s="T495">iʔgö. </ts>
               <ts e="T497" id="Seg_5070" n="e" s="T496">Kuvasəʔi </ts>
               <ts e="T498" id="Seg_5072" n="e" s="T497">bar. </ts>
               <ts e="T499" id="Seg_5074" n="e" s="T498">Ara </ts>
               <ts e="T500" id="Seg_5076" n="e" s="T499">bĭtleʔbəʔjə. </ts>
               <ts e="T501" id="Seg_5078" n="e" s="T500">Sʼarlaʔbəʔjə </ts>
               <ts e="T502" id="Seg_5080" n="e" s="T501">гармонию. </ts>
               <ts e="T503" id="Seg_5082" n="e" s="T502">Suʔmiːleʔbəʔjə. </ts>
               <ts e="T504" id="Seg_5084" n="e" s="T503">Nüjnə </ts>
               <ts e="T505" id="Seg_5086" n="e" s="T504">nüjleʔbəʔjə. </ts>
               <ts e="T506" id="Seg_5088" n="e" s="T505">Jakšə </ts>
               <ts e="T507" id="Seg_5090" n="e" s="T506">nüke! </ts>
               <ts e="T508" id="Seg_5092" n="e" s="T507">Ipek </ts>
               <ts e="T509" id="Seg_5094" n="e" s="T508">bar </ts>
               <ts e="T510" id="Seg_5096" n="e" s="T509">(pür </ts>
               <ts e="T511" id="Seg_5098" n="e" s="T510">-) </ts>
               <ts e="T512" id="Seg_5100" n="e" s="T511">pürbi </ts>
               <ts e="T513" id="Seg_5102" n="e" s="T512">jakšə. </ts>
               <ts e="T514" id="Seg_5104" n="e" s="T513">Nömər. </ts>
               <ts e="T515" id="Seg_5106" n="e" s="T514">Tüjö </ts>
               <ts e="T516" id="Seg_5108" n="e" s="T515">(amo </ts>
               <ts e="T517" id="Seg_5110" n="e" s="T516">- </ts>
               <ts e="T518" id="Seg_5112" n="e" s="T517">amo </ts>
               <ts e="T519" id="Seg_5114" n="e" s="T518">-) </ts>
               <ts e="T520" id="Seg_5116" n="e" s="T519">amorgaʔ! </ts>
               <ts e="T521" id="Seg_5118" n="e" s="T520">Padʼi </ts>
               <ts e="T522" id="Seg_5120" n="e" s="T521">püjöliaʔi. </ts>
               <ts e="T523" id="Seg_5122" n="e" s="T522">Tüjö </ts>
               <ts e="T524" id="Seg_5124" n="e" s="T523">uʔblam. </ts>
               <ts e="T525" id="Seg_5126" n="e" s="T524">Amgaʔ, </ts>
               <ts e="T526" id="Seg_5128" n="e" s="T525">(amno=) </ts>
               <ts e="T527" id="Seg_5130" n="e" s="T526">amnogaʔ! </ts>
               <ts e="T528" id="Seg_5132" n="e" s="T527">Nʼešpək </ts>
               <ts e="T529" id="Seg_5134" n="e" s="T528">kuza </ts>
               <ts e="T530" id="Seg_5136" n="e" s="T529">ambi. </ts>
               <ts e="T531" id="Seg_5138" n="e" s="T530">Ipek </ts>
               <ts e="T532" id="Seg_5140" n="e" s="T531">lem </ts>
               <ts e="T533" id="Seg_5142" n="e" s="T532">keʔbdeziʔ. </ts>
               <ts e="T534" id="Seg_5144" n="e" s="T533">Oroma </ts>
               <ts e="T535" id="Seg_5146" n="e" s="T534">nuldəbiam. </ts>
               <ts e="T536" id="Seg_5148" n="e" s="T535">Amorgaʔ, </ts>
               <ts e="T537" id="Seg_5150" n="e" s="T536">(šut) </ts>
               <ts e="T538" id="Seg_5152" n="e" s="T537">süt </ts>
               <ts e="T539" id="Seg_5154" n="e" s="T538">(bĭt </ts>
               <ts e="T540" id="Seg_5156" n="e" s="T539">-) </ts>
               <ts e="T541" id="Seg_5158" n="e" s="T540">bĭttə! </ts>
               <ts e="T543" id="Seg_5160" n="e" s="T541">Всё… </ts>
               <ts e="T544" id="Seg_5162" n="e" s="T543">Tăn </ts>
               <ts e="T545" id="Seg_5164" n="e" s="T544">iššo </ts>
               <ts e="T546" id="Seg_5166" n="e" s="T545">kunoːllaʔbəl, </ts>
               <ts e="T547" id="Seg_5168" n="e" s="T546">uʔbdaʔ! </ts>
               <ts e="T548" id="Seg_5170" n="e" s="T547">A_to </ts>
               <ts e="T549" id="Seg_5172" n="e" s="T548">šĭšəge </ts>
               <ts e="T550" id="Seg_5174" n="e" s="T549">bü </ts>
               <ts e="T551" id="Seg_5176" n="e" s="T550">detləm, </ts>
               <ts e="T552" id="Seg_5178" n="e" s="T551">tănan </ts>
               <ts e="T553" id="Seg_5180" n="e" s="T552">kămnam </ts>
               <ts e="T554" id="Seg_5182" n="e" s="T553">bar. </ts>
               <ts e="T555" id="Seg_5184" n="e" s="T554">Dĭgəttə </ts>
               <ts e="T556" id="Seg_5186" n="e" s="T555">bar </ts>
               <ts e="T557" id="Seg_5188" n="e" s="T556">süʔməluʔləl. </ts>
               <ts e="T558" id="Seg_5190" n="e" s="T557">Всё, </ts>
               <ts e="T559" id="Seg_5192" n="e" s="T558">наверное. </ts>
               <ts e="T560" id="Seg_5194" n="e" s="T559">Погоди, </ts>
               <ts e="T561" id="Seg_5196" n="e" s="T560">погоди, </ts>
               <ts e="T562" id="Seg_5198" n="e" s="T561">как </ts>
               <ts e="T564" id="Seg_5200" n="e" s="T562">нас… </ts>
               <ts e="T565" id="Seg_5202" n="e" s="T564">((DMG)) </ts>
               <ts e="T566" id="Seg_5204" n="e" s="T565">(-dot). </ts>
               <ts e="T567" id="Seg_5206" n="e" s="T566">Ej </ts>
               <ts e="T568" id="Seg_5208" n="e" s="T567">nʼilgöt! </ts>
               <ts e="T569" id="Seg_5210" n="e" s="T568">(Iʔ </ts>
               <ts e="T570" id="Seg_5212" n="e" s="T569">jaʔ)! </ts>
               <ts e="T571" id="Seg_5214" n="e" s="T570">Господи. </ts>
            </ts>
            <ts e="T575" id="Seg_5215" n="sc" s="T573">
               <ts e="T574" id="Seg_5217" n="e" s="T573">Iʔ </ts>
               <ts e="T575" id="Seg_5219" n="e" s="T574">dʼăbaktəraʔ! </ts>
            </ts>
            <ts e="T579" id="Seg_5220" n="sc" s="T577">
               <ts e="T578" id="Seg_5222" n="e" s="T577">Iʔ </ts>
               <ts e="T579" id="Seg_5224" n="e" s="T578">kudonzaʔ! </ts>
            </ts>
            <ts e="T584" id="Seg_5225" n="sc" s="T582">
               <ts e="T583" id="Seg_5227" n="e" s="T582">Iʔ </ts>
               <ts e="T584" id="Seg_5229" n="e" s="T583">kirgaːraʔ! </ts>
            </ts>
            <ts e="T590" id="Seg_5230" n="sc" s="T586">
               <ts e="T587" id="Seg_5232" n="e" s="T586">Kuʔlə </ts>
               <ts e="T588" id="Seg_5234" n="e" s="T587">mʼaŋŋaʔbə. </ts>
               <ts e="T589" id="Seg_5236" n="e" s="T588">Kĭški-t </ts>
               <ts e="T590" id="Seg_5238" n="e" s="T589">kuʔlə. </ts>
            </ts>
            <ts e="T605" id="Seg_5239" n="sc" s="T592">
               <ts e="T593" id="Seg_5241" n="e" s="T592">Iʔ </ts>
               <ts e="T594" id="Seg_5243" n="e" s="T593">nuʔməʔ! </ts>
               <ts e="T595" id="Seg_5245" n="e" s="T594">Iʔ </ts>
               <ts e="T596" id="Seg_5247" n="e" s="T595">suʔmiʔ! </ts>
               <ts e="T597" id="Seg_5249" n="e" s="T596">Iʔ </ts>
               <ts e="T598" id="Seg_5251" n="e" s="T597">nüjnəʔ! </ts>
               <ts e="T599" id="Seg_5253" n="e" s="T598">Ugaːndə </ts>
               <ts e="T600" id="Seg_5255" n="e" s="T599">jakšə. </ts>
               <ts e="T601" id="Seg_5257" n="e" s="T600">Ugaːndə </ts>
               <ts e="T602" id="Seg_5259" n="e" s="T601">kuvas. </ts>
               <ts e="T603" id="Seg_5261" n="e" s="T602">Ugaːndə </ts>
               <ts e="T604" id="Seg_5263" n="e" s="T603">komu. </ts>
               <ts e="T605" id="Seg_5265" n="e" s="T604">Ugaːndə. </ts>
            </ts>
            <ts e="T609" id="Seg_5266" n="sc" s="T606">
               <ts e="T607" id="Seg_5268" n="e" s="T606">Kəbo, </ts>
               <ts e="T608" id="Seg_5270" n="e" s="T607">ugaːndə </ts>
               <ts e="T609" id="Seg_5272" n="e" s="T608">numo. </ts>
            </ts>
            <ts e="T642" id="Seg_5273" n="sc" s="T612">
               <ts e="T613" id="Seg_5275" n="e" s="T612">Ugaːndə </ts>
               <ts e="T614" id="Seg_5277" n="e" s="T613">üdʼüge. </ts>
               <ts e="T615" id="Seg_5279" n="e" s="T614">Ну, </ts>
               <ts e="T616" id="Seg_5281" n="e" s="T615">теперь, </ts>
               <ts e="T617" id="Seg_5283" n="e" s="T616">наверное, </ts>
               <ts e="T618" id="Seg_5285" n="e" s="T617"> всё. </ts>
               <ts e="T619" id="Seg_5287" n="e" s="T618">Padʼi </ts>
               <ts e="T620" id="Seg_5289" n="e" s="T619">plat </ts>
               <ts e="T621" id="Seg_5291" n="e" s="T620">tojirbial. </ts>
               <ts e="T622" id="Seg_5293" n="e" s="T621">Na_što </ts>
               <ts e="T623" id="Seg_5295" n="e" s="T622">deʔpiel? </ts>
               <ts e="T624" id="Seg_5297" n="e" s="T623">Măn </ts>
               <ts e="T625" id="Seg_5299" n="e" s="T624">ej </ts>
               <ts e="T626" id="Seg_5301" n="e" s="T625">ilim. </ts>
               <ts e="T627" id="Seg_5303" n="e" s="T626">Nʼe </ts>
               <ts e="T628" id="Seg_5305" n="e" s="T627">nada </ts>
               <ts e="T629" id="Seg_5307" n="e" s="T628">tojirzittə, </ts>
               <ts e="T630" id="Seg_5309" n="e" s="T629">a_to </ts>
               <ts e="T631" id="Seg_5311" n="e" s="T630">ej </ts>
               <ts e="T632" id="Seg_5313" n="e" s="T631">jakšə </ts>
               <ts e="T633" id="Seg_5315" n="e" s="T632">moləj. </ts>
               <ts e="T634" id="Seg_5317" n="e" s="T633">Udam </ts>
               <ts e="T635" id="Seg_5319" n="e" s="T634">băʔpiam </ts>
               <ts e="T636" id="Seg_5321" n="e" s="T635">bar, </ts>
               <ts e="T637" id="Seg_5323" n="e" s="T636">kem </ts>
               <ts e="T638" id="Seg_5325" n="e" s="T637">mʼaŋŋaʔbə. </ts>
               <ts e="T639" id="Seg_5327" n="e" s="T638">Deʔ </ts>
               <ts e="T640" id="Seg_5329" n="e" s="T639">ĭmbi-nʼibudʼ </ts>
               <ts e="T641" id="Seg_5331" n="e" s="T640">udam </ts>
               <ts e="T642" id="Seg_5333" n="e" s="T641">sarzittə! </ts>
            </ts>
            <ts e="T766" id="Seg_5334" n="sc" s="T645">
               <ts e="T647" id="Seg_5336" n="e" s="T645">Na_što </ts>
               <ts e="T648" id="Seg_5338" n="e" s="T647">(tagajziʔ=) </ts>
               <ts e="T649" id="Seg_5340" n="e" s="T648">tagaj </ts>
               <ts e="T650" id="Seg_5342" n="e" s="T649">ibiel? </ts>
               <ts e="T651" id="Seg_5344" n="e" s="T650">Tănan </ts>
               <ts e="T652" id="Seg_5346" n="e" s="T651">iššo </ts>
               <ts e="T654" id="Seg_5348" n="e" s="T652">nada… </ts>
               <ts e="T655" id="Seg_5350" n="e" s="T654">Beržə, </ts>
               <ts e="T656" id="Seg_5352" n="e" s="T655">surno </ts>
               <ts e="T657" id="Seg_5354" n="e" s="T656">bar </ts>
               <ts e="T658" id="Seg_5356" n="e" s="T657">šonəga. </ts>
               <ts e="T659" id="Seg_5358" n="e" s="T658">Sĭre </ts>
               <ts e="T660" id="Seg_5360" n="e" s="T659">bar </ts>
               <ts e="T661" id="Seg_5362" n="e" s="T660">šonəga </ts>
               <ts e="T662" id="Seg_5364" n="e" s="T661">beržəziʔ. </ts>
               <ts e="T663" id="Seg_5366" n="e" s="T662">Ugaːndə </ts>
               <ts e="T664" id="Seg_5368" n="e" s="T663">šĭšəge. </ts>
               <ts e="T665" id="Seg_5370" n="e" s="T664">Măn </ts>
               <ts e="T666" id="Seg_5372" n="e" s="T665">ugaːndə </ts>
               <ts e="T667" id="Seg_5374" n="e" s="T666">kămbiam. </ts>
               <ts e="T668" id="Seg_5376" n="e" s="T667">Da </ts>
               <ts e="T669" id="Seg_5378" n="e" s="T668">tăŋ </ts>
               <ts e="T670" id="Seg_5380" n="e" s="T669">kămbiam. </ts>
               <ts e="T671" id="Seg_5382" n="e" s="T670">Ujum </ts>
               <ts e="T672" id="Seg_5384" n="e" s="T671">kămbi. </ts>
               <ts e="T673" id="Seg_5386" n="e" s="T672">Udam </ts>
               <ts e="T674" id="Seg_5388" n="e" s="T673">kămbi. </ts>
               <ts e="T675" id="Seg_5390" n="e" s="T674">Kadəlbə </ts>
               <ts e="T676" id="Seg_5392" n="e" s="T675">bar </ts>
               <ts e="T677" id="Seg_5394" n="e" s="T676">kămbi. </ts>
               <ts e="T678" id="Seg_5396" n="e" s="T677">Teinen </ts>
               <ts e="T679" id="Seg_5398" n="e" s="T678">(jak </ts>
               <ts e="T680" id="Seg_5400" n="e" s="T679">-) </ts>
               <ts e="T681" id="Seg_5402" n="e" s="T680">kuvas </ts>
               <ts e="T682" id="Seg_5404" n="e" s="T681">dʼala. </ts>
               <ts e="T683" id="Seg_5406" n="e" s="T682">Kuja </ts>
               <ts e="T684" id="Seg_5408" n="e" s="T683">bar </ts>
               <ts e="T685" id="Seg_5410" n="e" s="T684">măndolaʔbə. </ts>
               <ts e="T686" id="Seg_5412" n="e" s="T685">Nʼuʔnun </ts>
               <ts e="T687" id="Seg_5414" n="e" s="T686">ĭmbidə </ts>
               <ts e="T688" id="Seg_5416" n="e" s="T687">naga. </ts>
               <ts e="T689" id="Seg_5418" n="e" s="T688">Ugaːndə </ts>
               <ts e="T690" id="Seg_5420" n="e" s="T689">jakšə </ts>
               <ts e="T691" id="Seg_5422" n="e" s="T690">dʼala. </ts>
               <ts e="T692" id="Seg_5424" n="e" s="T691">Il </ts>
               <ts e="T693" id="Seg_5426" n="e" s="T692">bar </ts>
               <ts e="T694" id="Seg_5428" n="e" s="T693">togonorlaʔbəʔjə. </ts>
               <ts e="T695" id="Seg_5430" n="e" s="T694">Toltano </ts>
               <ts e="T696" id="Seg_5432" n="e" s="T695">amlaʔbəʔjə. </ts>
               <ts e="T697" id="Seg_5434" n="e" s="T696">Ineʔi </ts>
               <ts e="T698" id="Seg_5436" n="e" s="T697">tarirlaʔbəʔjə </ts>
               <ts e="T699" id="Seg_5438" n="e" s="T698">bar. </ts>
               <ts e="T700" id="Seg_5440" n="e" s="T699">Nada </ts>
               <ts e="T701" id="Seg_5442" n="e" s="T700">kanzittə </ts>
               <ts e="T702" id="Seg_5444" n="e" s="T701">noʔ </ts>
               <ts e="T703" id="Seg_5446" n="e" s="T702">jaʔsittə. </ts>
               <ts e="T704" id="Seg_5448" n="e" s="T703">Šapku </ts>
               <ts e="T705" id="Seg_5450" n="e" s="T704">nada </ts>
               <ts e="T706" id="Seg_5452" n="e" s="T705">izittə. </ts>
               <ts e="T707" id="Seg_5454" n="e" s="T706">Nada </ts>
               <ts e="T708" id="Seg_5456" n="e" s="T707">bü </ts>
               <ts e="T709" id="Seg_5458" n="e" s="T708">izittə. </ts>
               <ts e="T710" id="Seg_5460" n="e" s="T709">Süt </ts>
               <ts e="T711" id="Seg_5462" n="e" s="T710">izittə </ts>
               <ts e="T712" id="Seg_5464" n="e" s="T711">nada, </ts>
               <ts e="T713" id="Seg_5466" n="e" s="T712">ipek </ts>
               <ts e="T714" id="Seg_5468" n="e" s="T713">izittə, </ts>
               <ts e="T715" id="Seg_5470" n="e" s="T714">tus </ts>
               <ts e="T716" id="Seg_5472" n="e" s="T715">izittə, </ts>
               <ts e="T717" id="Seg_5474" n="e" s="T716">munujʔ </ts>
               <ts e="T718" id="Seg_5476" n="e" s="T717">izittə. </ts>
               <ts e="T719" id="Seg_5478" n="e" s="T718">Pi </ts>
               <ts e="T720" id="Seg_5480" n="e" s="T719">nada </ts>
               <ts e="T721" id="Seg_5482" n="e" s="T720">izittə, </ts>
               <ts e="T722" id="Seg_5484" n="e" s="T721">šapku. </ts>
               <ts e="T723" id="Seg_5486" n="e" s="T722">Albuga </ts>
               <ts e="T724" id="Seg_5488" n="e" s="T723">teinen </ts>
               <ts e="T725" id="Seg_5490" n="e" s="T724">kuʔpiem. </ts>
               <ts e="T726" id="Seg_5492" n="e" s="T725">Tažəp </ts>
               <ts e="T727" id="Seg_5494" n="e" s="T726">kuʔpiem. </ts>
               <ts e="T728" id="Seg_5496" n="e" s="T727">Urgaːba </ts>
               <ts e="T729" id="Seg_5498" n="e" s="T728">kuʔpiem. </ts>
               <ts e="T730" id="Seg_5500" n="e" s="T729">Söːn </ts>
               <ts e="T731" id="Seg_5502" n="e" s="T730">kuʔpiem. </ts>
               <ts e="T732" id="Seg_5504" n="e" s="T731">Askər </ts>
               <ts e="T733" id="Seg_5506" n="e" s="T732">kuʔpiem. </ts>
               <ts e="T734" id="Seg_5508" n="e" s="T733">Poʔto </ts>
               <ts e="T735" id="Seg_5510" n="e" s="T734">kuʔpiem, </ts>
               <ts e="T736" id="Seg_5512" n="e" s="T735">multuksiʔ. </ts>
               <ts e="T737" id="Seg_5514" n="e" s="T736">Tüžöjʔi </ts>
               <ts e="T738" id="Seg_5516" n="e" s="T737">bar </ts>
               <ts e="T739" id="Seg_5518" n="e" s="T738">noʔ </ts>
               <ts e="T740" id="Seg_5520" n="e" s="T739">amniaʔi. </ts>
               <ts e="T741" id="Seg_5522" n="e" s="T740">Mĭlleʔbəʔjə </ts>
               <ts e="T742" id="Seg_5524" n="e" s="T741">dĭn. </ts>
               <ts e="T743" id="Seg_5526" n="e" s="T742">Dĭ </ts>
               <ts e="T744" id="Seg_5528" n="e" s="T743">kuza </ts>
               <ts e="T745" id="Seg_5530" n="e" s="T744">detləj </ts>
               <ts e="T746" id="Seg_5532" n="e" s="T745">их </ts>
               <ts e="T747" id="Seg_5534" n="e" s="T746">maʔndə. </ts>
               <ts e="T748" id="Seg_5536" n="e" s="T747">Всё. </ts>
               <ts e="T749" id="Seg_5538" n="e" s="T748">Nada </ts>
               <ts e="T750" id="Seg_5540" n="e" s="T749">eššim </ts>
               <ts e="T751" id="Seg_5542" n="e" s="T750">bazəsʼtə. </ts>
               <ts e="T752" id="Seg_5544" n="e" s="T751">Nada </ts>
               <ts e="T753" id="Seg_5546" n="e" s="T752">(e </ts>
               <ts e="T754" id="Seg_5548" n="e" s="T753">-) </ts>
               <ts e="T755" id="Seg_5550" n="e" s="T754">eššim </ts>
               <ts e="T756" id="Seg_5552" n="e" s="T755">bar </ts>
               <ts e="T757" id="Seg_5554" n="e" s="T756">dʼorlaʔbə. </ts>
               <ts e="T758" id="Seg_5556" n="e" s="T757">Nada </ts>
               <ts e="T759" id="Seg_5558" n="e" s="T758">dĭm </ts>
               <ts e="T760" id="Seg_5560" n="e" s="T759">köːdərzittə. </ts>
               <ts e="T761" id="Seg_5562" n="e" s="T760">Nada </ts>
               <ts e="T762" id="Seg_5564" n="e" s="T761">amorzittə </ts>
               <ts e="T763" id="Seg_5566" n="e" s="T762">mĭzittə. </ts>
               <ts e="T764" id="Seg_5568" n="e" s="T763">Nada </ts>
               <ts e="T765" id="Seg_5570" n="e" s="T764">kunoːlzittə </ts>
               <ts e="T766" id="Seg_5572" n="e" s="T765">enzittə. </ts>
            </ts>
            <ts e="T982" id="Seg_5573" n="sc" s="T770">
               <ts e="T771" id="Seg_5575" n="e" s="T770">Oːrdəʔ </ts>
               <ts e="T772" id="Seg_5577" n="e" s="T771">eššim </ts>
               <ts e="T773" id="Seg_5579" n="e" s="T772">bar! </ts>
               <ts e="T774" id="Seg_5581" n="e" s="T773">Pušaj </ts>
               <ts e="T775" id="Seg_5583" n="e" s="T774">kunoːllaʔbə. </ts>
               <ts e="T776" id="Seg_5585" n="e" s="T775">Amnoʔ, </ts>
               <ts e="T777" id="Seg_5587" n="e" s="T776">tüjö </ts>
               <ts e="T778" id="Seg_5589" n="e" s="T777">büjleʔ </ts>
               <ts e="T779" id="Seg_5591" n="e" s="T778">kallam. </ts>
               <ts e="T780" id="Seg_5593" n="e" s="T779">Bü </ts>
               <ts e="T781" id="Seg_5595" n="e" s="T780">detlem. </ts>
               <ts e="T782" id="Seg_5597" n="e" s="T781">Dĭgəttə </ts>
               <ts e="T783" id="Seg_5599" n="e" s="T782">bĭtlel. </ts>
               <ts e="T784" id="Seg_5601" n="e" s="T783">Iʔ </ts>
               <ts e="T785" id="Seg_5603" n="e" s="T784">kanaʔ! </ts>
               <ts e="T786" id="Seg_5605" n="e" s="T785">A </ts>
               <ts e="T787" id="Seg_5607" n="e" s="T786">dĭgəttə </ts>
               <ts e="T788" id="Seg_5609" n="e" s="T787">kallal </ts>
               <ts e="T789" id="Seg_5611" n="e" s="T788">maʔnəl. </ts>
               <ts e="T790" id="Seg_5613" n="e" s="T789">Tüjö </ts>
               <ts e="T791" id="Seg_5615" n="e" s="T790">kem </ts>
               <ts e="T792" id="Seg_5617" n="e" s="T791">pürlem. </ts>
               <ts e="T793" id="Seg_5619" n="e" s="T792">Nada </ts>
               <ts e="T794" id="Seg_5621" n="e" s="T793">dibər </ts>
               <ts e="T795" id="Seg_5623" n="e" s="T794">süt </ts>
               <ts e="T796" id="Seg_5625" n="e" s="T795">kămnasʼtə, </ts>
               <ts e="T797" id="Seg_5627" n="e" s="T796">kajak </ts>
               <ts e="T798" id="Seg_5629" n="e" s="T797">enzittə, </ts>
               <ts e="T799" id="Seg_5631" n="e" s="T798">alʼi </ts>
               <ts e="T800" id="Seg_5633" n="e" s="T799">sil </ts>
               <ts e="T801" id="Seg_5635" n="e" s="T800">enzittə. </ts>
               <ts e="T802" id="Seg_5637" n="e" s="T801">Tüjö </ts>
               <ts e="T803" id="Seg_5639" n="e" s="T802">amorləbəj. </ts>
               <ts e="T804" id="Seg_5641" n="e" s="T803">Iʔ </ts>
               <ts e="T805" id="Seg_5643" n="e" s="T804">(baza), </ts>
               <ts e="T806" id="Seg_5645" n="e" s="T805">a_to </ts>
               <ts e="T807" id="Seg_5647" n="e" s="T806">dʼibige. </ts>
               <ts e="T808" id="Seg_5649" n="e" s="T807">(Ipek </ts>
               <ts e="T809" id="Seg_5651" n="e" s="T808">-) </ts>
               <ts e="T810" id="Seg_5653" n="e" s="T809">Ipeksiʔ </ts>
               <ts e="T811" id="Seg_5655" n="e" s="T810">amaʔ! </ts>
               <ts e="T812" id="Seg_5657" n="e" s="T811">(Koʔbdo) </ts>
               <ts e="T814" id="Seg_5659" n="e" s="T812">šobi. </ts>
               <ts e="T815" id="Seg_5661" n="e" s="T814">Eʔbdə </ts>
               <ts e="T816" id="Seg_5663" n="e" s="T815">sĭre. </ts>
               <ts e="T817" id="Seg_5665" n="e" s="T816">Bostə </ts>
               <ts e="T818" id="Seg_5667" n="e" s="T817">bar </ts>
               <ts e="T819" id="Seg_5669" n="e" s="T818">todam. </ts>
               <ts e="T820" id="Seg_5671" n="e" s="T819">I </ts>
               <ts e="T821" id="Seg_5673" n="e" s="T820">dĭ </ts>
               <ts e="T822" id="Seg_5675" n="e" s="T821">kuza </ts>
               <ts e="T823" id="Seg_5677" n="e" s="T822">šobi, </ts>
               <ts e="T824" id="Seg_5679" n="e" s="T823">nʼešpək </ts>
               <ts e="T825" id="Seg_5681" n="e" s="T824">ugaːndə. </ts>
               <ts e="T826" id="Seg_5683" n="e" s="T825">I </ts>
               <ts e="T827" id="Seg_5685" n="e" s="T826">mĭʔ! </ts>
               <ts e="T828" id="Seg_5687" n="e" s="T827">Elet </ts>
               <ts e="T829" id="Seg_5689" n="e" s="T828">šobi. </ts>
               <ts e="T830" id="Seg_5691" n="e" s="T829">Всё. </ts>
               <ts e="T831" id="Seg_5693" n="e" s="T830">Dĭzeŋ </ts>
               <ts e="T832" id="Seg_5695" n="e" s="T831">toltano </ts>
               <ts e="T833" id="Seg_5697" n="e" s="T832">ej </ts>
               <ts e="T834" id="Seg_5699" n="e" s="T833">amnəbiʔi. </ts>
               <ts e="T835" id="Seg_5701" n="e" s="T834">Măn </ts>
               <ts e="T836" id="Seg_5703" n="e" s="T835">abam </ts>
               <ts e="T837" id="Seg_5705" n="e" s="T836">dĭzeŋdə </ts>
               <ts e="T838" id="Seg_5707" n="e" s="T837">amnəlbi. </ts>
               <ts e="T839" id="Seg_5709" n="e" s="T838">Dĭzeŋ </ts>
               <ts e="T840" id="Seg_5711" n="e" s="T839">uja </ts>
               <ts e="T841" id="Seg_5713" n="e" s="T840">iʔgö </ts>
               <ts e="T842" id="Seg_5715" n="e" s="T841">ibi </ts>
               <ts e="T843" id="Seg_5717" n="e" s="T842">bar, </ts>
               <ts e="T844" id="Seg_5719" n="e" s="T843">uja </ts>
               <ts e="T845" id="Seg_5721" n="e" s="T844">ambiʔi. </ts>
               <ts e="T846" id="Seg_5723" n="e" s="T845">Ipek </ts>
               <ts e="T847" id="Seg_5725" n="e" s="T846">amga </ts>
               <ts e="T848" id="Seg_5727" n="e" s="T847">ibi. </ts>
               <ts e="T849" id="Seg_5729" n="e" s="T848">A </ts>
               <ts e="T850" id="Seg_5731" n="e" s="T849">daška </ts>
               <ts e="T851" id="Seg_5733" n="e" s="T850">ĭmbidə </ts>
               <ts e="T852" id="Seg_5735" n="e" s="T851">(n </ts>
               <ts e="T853" id="Seg_5737" n="e" s="T852">-) </ts>
               <ts e="T854" id="Seg_5739" n="e" s="T853">naga. </ts>
               <ts e="T855" id="Seg_5741" n="e" s="T854">Ĭmbi </ts>
               <ts e="T856" id="Seg_5743" n="e" s="T855">amnolaʔbəl, </ts>
               <ts e="T857" id="Seg_5745" n="e" s="T856">ej </ts>
               <ts e="T858" id="Seg_5747" n="e" s="T857">dʼăbaktərial. </ts>
               <ts e="T859" id="Seg_5749" n="e" s="T858">Dʼăbaktəraʔ </ts>
               <ts e="T860" id="Seg_5751" n="e" s="T859">ĭmbi-nʼibudʼ! </ts>
               <ts e="T861" id="Seg_5753" n="e" s="T860">(Sĭjbə </ts>
               <ts e="T862" id="Seg_5755" n="e" s="T861">verna </ts>
               <ts e="T863" id="Seg_5757" n="e" s="T862">ĭze </ts>
               <ts e="T864" id="Seg_5759" n="e" s="T863">-) </ts>
               <ts e="T865" id="Seg_5761" n="e" s="T864">Sĭjlə </ts>
               <ts e="T866" id="Seg_5763" n="e" s="T865">verna </ts>
               <ts e="T867" id="Seg_5765" n="e" s="T866">ĭzemnie. </ts>
               <ts e="T868" id="Seg_5767" n="e" s="T867">Ĭmbidə </ts>
               <ts e="T869" id="Seg_5769" n="e" s="T868">ej </ts>
               <ts e="T870" id="Seg_5771" n="e" s="T869">dʼăbaktərial. </ts>
               <ts e="T871" id="Seg_5773" n="e" s="T870">Dʼăbaktəraʔ! </ts>
               <ts e="T872" id="Seg_5775" n="e" s="T871">Всё. </ts>
               <ts e="T873" id="Seg_5777" n="e" s="T872">(Gibə </ts>
               <ts e="T874" id="Seg_5779" n="e" s="T873">-) </ts>
               <ts e="T875" id="Seg_5781" n="e" s="T874">Gibər </ts>
               <ts e="T876" id="Seg_5783" n="e" s="T875">kandəgal? </ts>
               <ts e="T877" id="Seg_5785" n="e" s="T876">Gijen </ts>
               <ts e="T878" id="Seg_5787" n="e" s="T877">ibiel, </ts>
               <ts e="T879" id="Seg_5789" n="e" s="T878">šoʔ </ts>
               <ts e="T880" id="Seg_5791" n="e" s="T879">döːbər! </ts>
               <ts e="T881" id="Seg_5793" n="e" s="T880">Ĭmbi-nʼibudʼ </ts>
               <ts e="T882" id="Seg_5795" n="e" s="T881">nörbəʔ </ts>
               <ts e="T883" id="Seg_5797" n="e" s="T882">măna! </ts>
               <ts e="T885" id="Seg_5799" n="e" s="T883">Опять забыла. </ts>
               <ts e="T886" id="Seg_5801" n="e" s="T885">Büzʼe </ts>
               <ts e="T887" id="Seg_5803" n="e" s="T886">nüketsiʔ </ts>
               <ts e="T888" id="Seg_5805" n="e" s="T887">amnolaʔpiʔi. </ts>
               <ts e="T889" id="Seg_5807" n="e" s="T888">Dĭzeŋ </ts>
               <ts e="T890" id="Seg_5809" n="e" s="T889">esseŋdə </ts>
               <ts e="T891" id="Seg_5811" n="e" s="T890">ibiʔi. </ts>
               <ts e="T892" id="Seg_5813" n="e" s="T891">Koʔbdo </ts>
               <ts e="T893" id="Seg_5815" n="e" s="T892">da </ts>
               <ts e="T894" id="Seg_5817" n="e" s="T893">nʼi. </ts>
               <ts e="T895" id="Seg_5819" n="e" s="T894">Tura </ts>
               <ts e="T896" id="Seg_5821" n="e" s="T895">ibi. </ts>
               <ts e="T897" id="Seg_5823" n="e" s="T896">Tüžöj </ts>
               <ts e="T898" id="Seg_5825" n="e" s="T897">ibi. </ts>
               <ts e="T899" id="Seg_5827" n="e" s="T898">Dĭ </ts>
               <ts e="T900" id="Seg_5829" n="e" s="T899">nüke </ts>
               <ts e="T901" id="Seg_5831" n="e" s="T900">tüžöj </ts>
               <ts e="T902" id="Seg_5833" n="e" s="T901">surdobi. </ts>
               <ts e="T903" id="Seg_5835" n="e" s="T902">Esseŋdə </ts>
               <ts e="T904" id="Seg_5837" n="e" s="T903">mĭbi. </ts>
               <ts e="T905" id="Seg_5839" n="e" s="T904">Ipek </ts>
               <ts e="T906" id="Seg_5841" n="e" s="T905">mĭbi. </ts>
               <ts e="T907" id="Seg_5843" n="e" s="T906">Dĭzeŋ </ts>
               <ts e="T908" id="Seg_5845" n="e" s="T907">amniaʔi. </ts>
               <ts e="T909" id="Seg_5847" n="e" s="T908">Всё. </ts>
               <ts e="T910" id="Seg_5849" n="e" s="T909">Dĭ </ts>
               <ts e="T911" id="Seg_5851" n="e" s="T910">šidegöʔ </ts>
               <ts e="T912" id="Seg_5853" n="e" s="T911">kuza </ts>
               <ts e="T913" id="Seg_5855" n="e" s="T912">šobiʔi. </ts>
               <ts e="T914" id="Seg_5857" n="e" s="T913">Tibi </ts>
               <ts e="T915" id="Seg_5859" n="e" s="T914">da </ts>
               <ts e="T916" id="Seg_5861" n="e" s="T915">koʔbdo </ts>
               <ts e="T917" id="Seg_5863" n="e" s="T916">tănan </ts>
               <ts e="T918" id="Seg_5865" n="e" s="T917">padʼi </ts>
               <ts e="T919" id="Seg_5867" n="e" s="T918">ĭmbi-nʼibudʼ </ts>
               <ts e="T920" id="Seg_5869" n="e" s="T919">deʔpiʔi. </ts>
               <ts e="T921" id="Seg_5871" n="e" s="T920">Sĭreʔpne </ts>
               <ts e="T922" id="Seg_5873" n="e" s="T921">deʔpiʔi. </ts>
               <ts e="T923" id="Seg_5875" n="e" s="T922">(Segə </ts>
               <ts e="T924" id="Seg_5877" n="e" s="T923">-) </ts>
               <ts e="T925" id="Seg_5879" n="e" s="T924">Segi </ts>
               <ts e="T926" id="Seg_5881" n="e" s="T925">bü </ts>
               <ts e="T927" id="Seg_5883" n="e" s="T926">bĭtlel. </ts>
               <ts e="T928" id="Seg_5885" n="e" s="T927">Padʼi </ts>
               <ts e="T929" id="Seg_5887" n="e" s="T928">kanfeti </ts>
               <ts e="T930" id="Seg_5889" n="e" s="T929">deʔpiʔi </ts>
               <ts e="T931" id="Seg_5891" n="e" s="T930">tănan. </ts>
               <ts e="T932" id="Seg_5893" n="e" s="T931">Kujnek </ts>
               <ts e="T933" id="Seg_5895" n="e" s="T932">padʼi </ts>
               <ts e="T934" id="Seg_5897" n="e" s="T933">deʔpiʔi. </ts>
               <ts e="T935" id="Seg_5899" n="e" s="T934">Piʔmə </ts>
               <ts e="T936" id="Seg_5901" n="e" s="T935">padʼi </ts>
               <ts e="T937" id="Seg_5903" n="e" s="T936">deʔpiʔi </ts>
               <ts e="T938" id="Seg_5905" n="e" s="T937">tănan. </ts>
               <ts e="T939" id="Seg_5907" n="e" s="T938">Predsʼedatʼelʼən </ts>
               <ts e="T940" id="Seg_5909" n="e" s="T939">turat </ts>
               <ts e="T941" id="Seg_5911" n="e" s="T940">urgo. </ts>
               <ts e="T942" id="Seg_5913" n="e" s="T941">Il </ts>
               <ts e="T943" id="Seg_5915" n="e" s="T942">iʔgö. </ts>
               <ts e="T944" id="Seg_5917" n="e" s="T943">(Ši </ts>
               <ts e="T945" id="Seg_5919" n="e" s="T944">-) </ts>
               <ts e="T946" id="Seg_5921" n="e" s="T945">Šide </ts>
               <ts e="T947" id="Seg_5923" n="e" s="T946">nʼi, </ts>
               <ts e="T948" id="Seg_5925" n="e" s="T947">šide </ts>
               <ts e="T949" id="Seg_5927" n="e" s="T948">koʔbdo. </ts>
               <ts e="T950" id="Seg_5929" n="e" s="T949">Nüke </ts>
               <ts e="T951" id="Seg_5931" n="e" s="T950">büzʼeʔiziʔ. </ts>
               <ts e="T952" id="Seg_5933" n="e" s="T951">Bostə, </ts>
               <ts e="T953" id="Seg_5935" n="e" s="T952">ne. </ts>
               <ts e="T954" id="Seg_5937" n="e" s="T953">(Me-) </ts>
               <ts e="T955" id="Seg_5939" n="e" s="T954">Menzeŋdə </ts>
               <ts e="T956" id="Seg_5941" n="e" s="T955">šide. </ts>
               <ts e="T957" id="Seg_5943" n="e" s="T956">Onʼiʔ </ts>
               <ts e="T958" id="Seg_5945" n="e" s="T957">jakšə </ts>
               <ts e="T959" id="Seg_5947" n="e" s="T958">men, </ts>
               <ts e="T960" id="Seg_5949" n="e" s="T959">onʼiʔ </ts>
               <ts e="T961" id="Seg_5951" n="e" s="T960">nagur </ts>
               <ts e="T962" id="Seg_5953" n="e" s="T961">uju, </ts>
               <ts e="T963" id="Seg_5955" n="e" s="T962">a </ts>
               <ts e="T964" id="Seg_5957" n="e" s="T963">teʔtə </ts>
               <ts e="T965" id="Seg_5959" n="e" s="T964">uju </ts>
               <ts e="T966" id="Seg_5961" n="e" s="T965">naga. </ts>
               <ts e="T969" id="Seg_5963" n="e" s="T966">Четвертой ноги нету. </ts>
               <ts e="T970" id="Seg_5965" n="e" s="T969">Tăn </ts>
               <ts e="T971" id="Seg_5967" n="e" s="T970">măna </ts>
               <ts e="T972" id="Seg_5969" n="e" s="T971">münörəʔ, </ts>
               <ts e="T973" id="Seg_5971" n="e" s="T972">a </ts>
               <ts e="T974" id="Seg_5973" n="e" s="T973">ĭmbiziʔ </ts>
               <ts e="T975" id="Seg_5975" n="e" s="T974">münörlaʔbəl? </ts>
               <ts e="T976" id="Seg_5977" n="e" s="T975">Muzuruksiʔ </ts>
               <ts e="T977" id="Seg_5979" n="e" s="T976">iʔ </ts>
               <ts e="T978" id="Seg_5981" n="e" s="T977">münörəʔ, </ts>
               <ts e="T979" id="Seg_5983" n="e" s="T978">a_to </ts>
               <ts e="T980" id="Seg_5985" n="e" s="T979">măna </ts>
               <ts e="T981" id="Seg_5987" n="e" s="T980">ĭzemnəj </ts>
               <ts e="T982" id="Seg_5989" n="e" s="T981">bar. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PKZ">
            <ta e="T13" id="Seg_5990" s="T4">PKZ_1964_SU0205.PKZ.001 (002)</ta>
            <ta e="T20" id="Seg_5991" s="T14">PKZ_1964_SU0205.PKZ.002 (004.001)</ta>
            <ta e="T30" id="Seg_5992" s="T20">PKZ_1964_SU0205.PKZ.003 (004.002)</ta>
            <ta e="T33" id="Seg_5993" s="T30">PKZ_1964_SU0205.PKZ.004 (004.003)</ta>
            <ta e="T38" id="Seg_5994" s="T33">PKZ_1964_SU0205.PKZ.005 (004.004)</ta>
            <ta e="T42" id="Seg_5995" s="T38">PKZ_1964_SU0205.PKZ.006 (005)</ta>
            <ta e="T51" id="Seg_5996" s="T46">PKZ_1964_SU0205.PKZ.007 (007.001)</ta>
            <ta e="T54" id="Seg_5997" s="T51">PKZ_1964_SU0205.PKZ.008 (007.002)</ta>
            <ta e="T56" id="Seg_5998" s="T54">PKZ_1964_SU0205.PKZ.009 (007.003)</ta>
            <ta e="T59" id="Seg_5999" s="T56">PKZ_1964_SU0205.PKZ.010 (007.004)</ta>
            <ta e="T62" id="Seg_6000" s="T59">PKZ_1964_SU0205.PKZ.011 (007.005)</ta>
            <ta e="T65" id="Seg_6001" s="T62">PKZ_1964_SU0205.PKZ.012 (007.006)</ta>
            <ta e="T68" id="Seg_6002" s="T65">PKZ_1964_SU0205.PKZ.013 (007.007)</ta>
            <ta e="T71" id="Seg_6003" s="T68">PKZ_1964_SU0205.PKZ.014 (007.008)</ta>
            <ta e="T74" id="Seg_6004" s="T71">PKZ_1964_SU0205.PKZ.015 (007.009)</ta>
            <ta e="T86" id="Seg_6005" s="T85">PKZ_1964_SU0205.PKZ.016 (009)</ta>
            <ta e="T94" id="Seg_6006" s="T90">PKZ_1964_SU0205.PKZ.017 (012.001)</ta>
            <ta e="T95" id="Seg_6007" s="T94">PKZ_1964_SU0205.PKZ.018 (012.002)</ta>
            <ta e="T96" id="Seg_6008" s="T95">PKZ_1964_SU0205.PKZ.019 (012.003)</ta>
            <ta e="T101" id="Seg_6009" s="T96">PKZ_1964_SU0205.PKZ.020 (012.004)</ta>
            <ta e="T104" id="Seg_6010" s="T101">PKZ_1964_SU0205.PKZ.021 (012.005)</ta>
            <ta e="T106" id="Seg_6011" s="T104">PKZ_1964_SU0205.PKZ.022 (012.006)</ta>
            <ta e="T115" id="Seg_6012" s="T106">PKZ_1964_SU0205.PKZ.023 (012.007)</ta>
            <ta e="T117" id="Seg_6013" s="T115">PKZ_1964_SU0205.PKZ.024 (013.001)</ta>
            <ta e="T119" id="Seg_6014" s="T117">PKZ_1964_SU0205.PKZ.025 (013.002)</ta>
            <ta e="T122" id="Seg_6015" s="T119">PKZ_1964_SU0205.PKZ.026 (013.003)</ta>
            <ta e="T124" id="Seg_6016" s="T122">PKZ_1964_SU0205.PKZ.027 (013.004)</ta>
            <ta e="T126" id="Seg_6017" s="T124">PKZ_1964_SU0205.PKZ.028 (013.005)</ta>
            <ta e="T128" id="Seg_6018" s="T126">PKZ_1964_SU0205.PKZ.029 (013.006)</ta>
            <ta e="T131" id="Seg_6019" s="T128">PKZ_1964_SU0205.PKZ.030 (013.007)</ta>
            <ta e="T135" id="Seg_6020" s="T131">PKZ_1964_SU0205.PKZ.031 (013.008)</ta>
            <ta e="T137" id="Seg_6021" s="T135">PKZ_1964_SU0205.PKZ.032 (013.009)</ta>
            <ta e="T141" id="Seg_6022" s="T137">PKZ_1964_SU0205.PKZ.033 (014.001)</ta>
            <ta e="T145" id="Seg_6023" s="T141">PKZ_1964_SU0205.PKZ.034 (014.002)</ta>
            <ta e="T147" id="Seg_6024" s="T145">PKZ_1964_SU0205.PKZ.035 (014.003)</ta>
            <ta e="T151" id="Seg_6025" s="T147">PKZ_1964_SU0205.PKZ.036 (014.004)</ta>
            <ta e="T154" id="Seg_6026" s="T151">PKZ_1964_SU0205.PKZ.037 (014.005)</ta>
            <ta e="T161" id="Seg_6027" s="T154">PKZ_1964_SU0205.PKZ.038 (015.001)</ta>
            <ta e="T165" id="Seg_6028" s="T161">PKZ_1964_SU0205.PKZ.039 (015.002)</ta>
            <ta e="T171" id="Seg_6029" s="T165">PKZ_1964_SU0205.PKZ.040 (015.003)</ta>
            <ta e="T180" id="Seg_6030" s="T171">PKZ_1964_SU0205.PKZ.041 (015.004)</ta>
            <ta e="T184" id="Seg_6031" s="T180">PKZ_1964_SU0205.PKZ.042 (015.005)</ta>
            <ta e="T189" id="Seg_6032" s="T184">PKZ_1964_SU0205.PKZ.043 (015.006)</ta>
            <ta e="T191" id="Seg_6033" s="T189">PKZ_1964_SU0205.PKZ.044 (015.007)</ta>
            <ta e="T193" id="Seg_6034" s="T191">PKZ_1964_SU0205.PKZ.045 (015.008)</ta>
            <ta e="T195" id="Seg_6035" s="T193">PKZ_1964_SU0205.PKZ.046 (015.009)</ta>
            <ta e="T196" id="Seg_6036" s="T195">PKZ_1964_SU0205.PKZ.047 (015.010)</ta>
            <ta e="T199" id="Seg_6037" s="T196">PKZ_1964_SU0205.PKZ.048 (015.011)</ta>
            <ta e="T207" id="Seg_6038" s="T199">PKZ_1964_SU0205.PKZ.049 (015.012)</ta>
            <ta e="T211" id="Seg_6039" s="T207">PKZ_1964_SU0205.PKZ.050 (015.013)</ta>
            <ta e="T213" id="Seg_6040" s="T211">PKZ_1964_SU0205.PKZ.051 (015.014)</ta>
            <ta e="T218" id="Seg_6041" s="T213">PKZ_1964_SU0205.PKZ.052 (015.015)</ta>
            <ta e="T219" id="Seg_6042" s="T218">PKZ_1964_SU0205.PKZ.053 (015.016)</ta>
            <ta e="T222" id="Seg_6043" s="T219">PKZ_1964_SU0205.PKZ.054 (015.017)</ta>
            <ta e="T223" id="Seg_6044" s="T222">PKZ_1964_SU0205.PKZ.055 (015.018)</ta>
            <ta e="T226" id="Seg_6045" s="T223">PKZ_1964_SU0205.PKZ.056 (016.001)</ta>
            <ta e="T229" id="Seg_6046" s="T226">PKZ_1964_SU0205.PKZ.057 (016.002)</ta>
            <ta e="T232" id="Seg_6047" s="T229">PKZ_1964_SU0205.PKZ.058 (016.003)</ta>
            <ta e="T236" id="Seg_6048" s="T232">PKZ_1964_SU0205.PKZ.059 (017.001)</ta>
            <ta e="T239" id="Seg_6049" s="T236">PKZ_1964_SU0205.PKZ.060 (017.002)</ta>
            <ta e="T242" id="Seg_6050" s="T239">PKZ_1964_SU0205.PKZ.061 (017.003)</ta>
            <ta e="T249" id="Seg_6051" s="T242">PKZ_1964_SU0205.PKZ.062 (017.004)</ta>
            <ta e="T250" id="Seg_6052" s="T249">PKZ_1964_SU0205.PKZ.063 (017.005)</ta>
            <ta e="T256" id="Seg_6053" s="T250">PKZ_1964_SU0205.PKZ.064 (018.001)</ta>
            <ta e="T260" id="Seg_6054" s="T256">PKZ_1964_SU0205.PKZ.065 (018.002)</ta>
            <ta e="T264" id="Seg_6055" s="T260">PKZ_1964_SU0205.PKZ.066 (018.003)</ta>
            <ta e="T266" id="Seg_6056" s="T264">PKZ_1964_SU0205.PKZ.067 (018.004)</ta>
            <ta e="T268" id="Seg_6057" s="T266">PKZ_1964_SU0205.PKZ.068 (018.005)</ta>
            <ta e="T270" id="Seg_6058" s="T268">PKZ_1964_SU0205.PKZ.069 (018.006)</ta>
            <ta e="T274" id="Seg_6059" s="T270">PKZ_1964_SU0205.PKZ.070 (018.007)</ta>
            <ta e="T276" id="Seg_6060" s="T274">PKZ_1964_SU0205.PKZ.071 (018.008)</ta>
            <ta e="T278" id="Seg_6061" s="T276">PKZ_1964_SU0205.PKZ.072 (018.009)</ta>
            <ta e="T282" id="Seg_6062" s="T278">PKZ_1964_SU0205.PKZ.073 (018.010)</ta>
            <ta e="T289" id="Seg_6063" s="T282">PKZ_1964_SU0205.PKZ.074 (019.001)</ta>
            <ta e="T291" id="Seg_6064" s="T289">PKZ_1964_SU0205.PKZ.075 (019.002)</ta>
            <ta e="T293" id="Seg_6065" s="T291">PKZ_1964_SU0205.PKZ.076 (019.003)</ta>
            <ta e="T296" id="Seg_6066" s="T293">PKZ_1964_SU0205.PKZ.077 (019.004)</ta>
            <ta e="T300" id="Seg_6067" s="T296">PKZ_1964_SU0205.PKZ.078 (019.005)</ta>
            <ta e="T306" id="Seg_6068" s="T300">PKZ_1964_SU0205.PKZ.079 (020.001)</ta>
            <ta e="T311" id="Seg_6069" s="T306">PKZ_1964_SU0205.PKZ.080 (020.002)</ta>
            <ta e="T316" id="Seg_6070" s="T311">PKZ_1964_SU0205.PKZ.081 (020.003)</ta>
            <ta e="T319" id="Seg_6071" s="T316">PKZ_1964_SU0205.PKZ.082 (020.004)</ta>
            <ta e="T321" id="Seg_6072" s="T319">PKZ_1964_SU0205.PKZ.083 (020.005)</ta>
            <ta e="T326" id="Seg_6073" s="T321">PKZ_1964_SU0205.PKZ.084 (020.006) </ta>
            <ta e="T330" id="Seg_6074" s="T326">PKZ_1964_SU0205.PKZ.085 (020.008)</ta>
            <ta e="T335" id="Seg_6075" s="T330">PKZ_1964_SU0205.PKZ.086 (020.009)</ta>
            <ta e="T337" id="Seg_6076" s="T335">PKZ_1964_SU0205.PKZ.087 (020.010)</ta>
            <ta e="T341" id="Seg_6077" s="T337">PKZ_1964_SU0205.PKZ.088 (020.011)</ta>
            <ta e="T343" id="Seg_6078" s="T341">PKZ_1964_SU0205.PKZ.089 (020.012)</ta>
            <ta e="T345" id="Seg_6079" s="T343">PKZ_1964_SU0205.PKZ.090 (020.013)</ta>
            <ta e="T348" id="Seg_6080" s="T345">PKZ_1964_SU0205.PKZ.091 (020.014)</ta>
            <ta e="T352" id="Seg_6081" s="T348">PKZ_1964_SU0205.PKZ.092 (020.015)</ta>
            <ta e="T355" id="Seg_6082" s="T352">PKZ_1964_SU0205.PKZ.093 (020.016)</ta>
            <ta e="T357" id="Seg_6083" s="T355">PKZ_1964_SU0205.PKZ.094 (020.017)</ta>
            <ta e="T358" id="Seg_6084" s="T357">PKZ_1964_SU0205.PKZ.095 (020.018)</ta>
            <ta e="T362" id="Seg_6085" s="T358">PKZ_1964_SU0205.PKZ.096 (021.001)</ta>
            <ta e="T364" id="Seg_6086" s="T362">PKZ_1964_SU0205.PKZ.097 (021.002)</ta>
            <ta e="T370" id="Seg_6087" s="T364">PKZ_1964_SU0205.PKZ.098 (021.003)</ta>
            <ta e="T375" id="Seg_6088" s="T370">PKZ_1964_SU0205.PKZ.099 (021.004)</ta>
            <ta e="T382" id="Seg_6089" s="T375">PKZ_1964_SU0205.PKZ.100 (021.005)</ta>
            <ta e="T384" id="Seg_6090" s="T382">PKZ_1964_SU0205.PKZ.101 (021.006)</ta>
            <ta e="T392" id="Seg_6091" s="T384">PKZ_1964_SU0205.PKZ.102 (021.007)</ta>
            <ta e="T395" id="Seg_6092" s="T392">PKZ_1964_SU0205.PKZ.103 (021.008)</ta>
            <ta e="T401" id="Seg_6093" s="T395">PKZ_1964_SU0205.PKZ.104 (021.009)</ta>
            <ta e="T402" id="Seg_6094" s="T401">PKZ_1964_SU0205.PKZ.105 (021.010)</ta>
            <ta e="T410" id="Seg_6095" s="T408">PKZ_1964_SU0205.PKZ.106 (023.001)</ta>
            <ta e="T413" id="Seg_6096" s="T410">PKZ_1964_SU0205.PKZ.107 (023.002)</ta>
            <ta e="T415" id="Seg_6097" s="T413">PKZ_1964_SU0205.PKZ.108 (024.001)</ta>
            <ta e="T419" id="Seg_6098" s="T415">PKZ_1964_SU0205.PKZ.109 (024.002)</ta>
            <ta e="T421" id="Seg_6099" s="T419">PKZ_1964_SU0205.PKZ.110 (024.003)</ta>
            <ta e="T427" id="Seg_6100" s="T421">PKZ_1964_SU0205.PKZ.111 (025.001)</ta>
            <ta e="T430" id="Seg_6101" s="T427">PKZ_1964_SU0205.PKZ.112 (025.002)</ta>
            <ta e="T432" id="Seg_6102" s="T430">PKZ_1964_SU0205.PKZ.113 (025.003)</ta>
            <ta e="T437" id="Seg_6103" s="T432">PKZ_1964_SU0205.PKZ.114 (025.004)</ta>
            <ta e="T447" id="Seg_6104" s="T437">PKZ_1964_SU0205.PKZ.115 (025.005)</ta>
            <ta e="T456" id="Seg_6105" s="T447">PKZ_1964_SU0205.PKZ.116 (026.001)</ta>
            <ta e="T458" id="Seg_6106" s="T456">PKZ_1964_SU0205.PKZ.117 (026.002)</ta>
            <ta e="T461" id="Seg_6107" s="T458">PKZ_1964_SU0205.PKZ.118 (026.003)</ta>
            <ta e="T465" id="Seg_6108" s="T461">PKZ_1964_SU0205.PKZ.119 (026.004)</ta>
            <ta e="T468" id="Seg_6109" s="T465">PKZ_1964_SU0205.PKZ.120 (026.005)</ta>
            <ta e="T478" id="Seg_6110" s="T468">PKZ_1964_SU0205.PKZ.121 (026.006)</ta>
            <ta e="T483" id="Seg_6111" s="T478">PKZ_1964_SU0205.PKZ.122 (026.007)</ta>
            <ta e="T486" id="Seg_6112" s="T483">PKZ_1964_SU0205.PKZ.123 (026.008)</ta>
            <ta e="T488" id="Seg_6113" s="T486">PKZ_1964_SU0205.PKZ.124 (027)</ta>
            <ta e="T496" id="Seg_6114" s="T488">PKZ_1964_SU0205.PKZ.125 (028.001)</ta>
            <ta e="T498" id="Seg_6115" s="T496">PKZ_1964_SU0205.PKZ.126 (028.002)</ta>
            <ta e="T500" id="Seg_6116" s="T498">PKZ_1964_SU0205.PKZ.127 (028.003)</ta>
            <ta e="T502" id="Seg_6117" s="T500">PKZ_1964_SU0205.PKZ.128 (028.004)</ta>
            <ta e="T503" id="Seg_6118" s="T502">PKZ_1964_SU0205.PKZ.129 (028.005)</ta>
            <ta e="T505" id="Seg_6119" s="T503">PKZ_1964_SU0205.PKZ.130 (028.006)</ta>
            <ta e="T507" id="Seg_6120" s="T505">PKZ_1964_SU0205.PKZ.131 (029.001)</ta>
            <ta e="T513" id="Seg_6121" s="T507">PKZ_1964_SU0205.PKZ.132 (029.002)</ta>
            <ta e="T514" id="Seg_6122" s="T513">PKZ_1964_SU0205.PKZ.133 (029.003)</ta>
            <ta e="T520" id="Seg_6123" s="T514">PKZ_1964_SU0205.PKZ.134 (029.004)</ta>
            <ta e="T522" id="Seg_6124" s="T520">PKZ_1964_SU0205.PKZ.135 (029.005)</ta>
            <ta e="T524" id="Seg_6125" s="T522">PKZ_1964_SU0205.PKZ.136 (029.006)</ta>
            <ta e="T527" id="Seg_6126" s="T524">PKZ_1964_SU0205.PKZ.137 (029.007)</ta>
            <ta e="T530" id="Seg_6127" s="T527">PKZ_1964_SU0205.PKZ.138 (029.008)</ta>
            <ta e="T533" id="Seg_6128" s="T530">PKZ_1964_SU0205.PKZ.139 (029.009)</ta>
            <ta e="T535" id="Seg_6129" s="T533">PKZ_1964_SU0205.PKZ.140 (029.010)</ta>
            <ta e="T541" id="Seg_6130" s="T535">PKZ_1964_SU0205.PKZ.141 (029.011)</ta>
            <ta e="T543" id="Seg_6131" s="T541">PKZ_1964_SU0205.PKZ.142 (029.012)</ta>
            <ta e="T547" id="Seg_6132" s="T543">PKZ_1964_SU0205.PKZ.143 (030.001)</ta>
            <ta e="T554" id="Seg_6133" s="T547">PKZ_1964_SU0205.PKZ.144 (030.002)</ta>
            <ta e="T557" id="Seg_6134" s="T554">PKZ_1964_SU0205.PKZ.145 (030.003)</ta>
            <ta e="T559" id="Seg_6135" s="T557">PKZ_1964_SU0205.PKZ.146 (030.004)</ta>
            <ta e="T564" id="Seg_6136" s="T559">PKZ_1964_SU0205.PKZ.147 (031.001)</ta>
            <ta e="T566" id="Seg_6137" s="T564">PKZ_1964_SU0205.PKZ.148 (031.002)</ta>
            <ta e="T568" id="Seg_6138" s="T566">PKZ_1964_SU0205.PKZ.149 (032.001)</ta>
            <ta e="T570" id="Seg_6139" s="T568">PKZ_1964_SU0205.PKZ.150 (032.002)</ta>
            <ta e="T571" id="Seg_6140" s="T570">PKZ_1964_SU0205.PKZ.151 (032.003)</ta>
            <ta e="T575" id="Seg_6141" s="T573">PKZ_1964_SU0205.PKZ.152 (033.002)</ta>
            <ta e="T579" id="Seg_6142" s="T577">PKZ_1964_SU0205.PKZ.153 (034.002)</ta>
            <ta e="T584" id="Seg_6143" s="T582">PKZ_1964_SU0205.PKZ.154 (035.002)</ta>
            <ta e="T588" id="Seg_6144" s="T586">PKZ_1964_SU0205.PKZ.155 (036.002)</ta>
            <ta e="T590" id="Seg_6145" s="T588">PKZ_1964_SU0205.PKZ.156 (036.003)</ta>
            <ta e="T594" id="Seg_6146" s="T592">PKZ_1964_SU0205.PKZ.157 (037.002)</ta>
            <ta e="T596" id="Seg_6147" s="T594">PKZ_1964_SU0205.PKZ.158 (037.003)</ta>
            <ta e="T598" id="Seg_6148" s="T596">PKZ_1964_SU0205.PKZ.159 (037.004)</ta>
            <ta e="T600" id="Seg_6149" s="T598">PKZ_1964_SU0205.PKZ.160 (038.001)</ta>
            <ta e="T602" id="Seg_6150" s="T600">PKZ_1964_SU0205.PKZ.161 (038.002)</ta>
            <ta e="T604" id="Seg_6151" s="T602">PKZ_1964_SU0205.PKZ.162 (038.003)</ta>
            <ta e="T605" id="Seg_6152" s="T604">PKZ_1964_SU0205.PKZ.163 (038.004)</ta>
            <ta e="T609" id="Seg_6153" s="T606">PKZ_1964_SU0205.PKZ.164 (039.002)</ta>
            <ta e="T614" id="Seg_6154" s="T612">PKZ_1964_SU0205.PKZ.165 (039.005)</ta>
            <ta e="T618" id="Seg_6155" s="T614">PKZ_1964_SU0205.PKZ.166 (039.006)</ta>
            <ta e="T621" id="Seg_6156" s="T618">PKZ_1964_SU0205.PKZ.167 (040.001)</ta>
            <ta e="T623" id="Seg_6157" s="T621">PKZ_1964_SU0205.PKZ.168 (040.002)</ta>
            <ta e="T626" id="Seg_6158" s="T623">PKZ_1964_SU0205.PKZ.169 (040.003)</ta>
            <ta e="T633" id="Seg_6159" s="T626">PKZ_1964_SU0205.PKZ.170 (040.004)</ta>
            <ta e="T638" id="Seg_6160" s="T633">PKZ_1964_SU0205.PKZ.171 (041.001)</ta>
            <ta e="T642" id="Seg_6161" s="T638">PKZ_1964_SU0205.PKZ.172 (041.002)</ta>
            <ta e="T650" id="Seg_6162" s="T645">PKZ_1964_SU0205.PKZ.173 (041.004)</ta>
            <ta e="T654" id="Seg_6163" s="T650">PKZ_1964_SU0205.PKZ.174 (041.005)</ta>
            <ta e="T658" id="Seg_6164" s="T654">PKZ_1964_SU0205.PKZ.175 (042.001)</ta>
            <ta e="T662" id="Seg_6165" s="T658">PKZ_1964_SU0205.PKZ.176 (042.002)</ta>
            <ta e="T664" id="Seg_6166" s="T662">PKZ_1964_SU0205.PKZ.177 (042.003)</ta>
            <ta e="T667" id="Seg_6167" s="T664">PKZ_1964_SU0205.PKZ.178 (042.004)</ta>
            <ta e="T670" id="Seg_6168" s="T667">PKZ_1964_SU0205.PKZ.179 (042.005)</ta>
            <ta e="T672" id="Seg_6169" s="T670">PKZ_1964_SU0205.PKZ.180 (042.006)</ta>
            <ta e="T674" id="Seg_6170" s="T672">PKZ_1964_SU0205.PKZ.181 (042.007)</ta>
            <ta e="T677" id="Seg_6171" s="T674">PKZ_1964_SU0205.PKZ.182 (042.008)</ta>
            <ta e="T682" id="Seg_6172" s="T677">PKZ_1964_SU0205.PKZ.183 (043.001)</ta>
            <ta e="T685" id="Seg_6173" s="T682">PKZ_1964_SU0205.PKZ.184 (043.002)</ta>
            <ta e="T688" id="Seg_6174" s="T685">PKZ_1964_SU0205.PKZ.185 (043.003)</ta>
            <ta e="T691" id="Seg_6175" s="T688">PKZ_1964_SU0205.PKZ.186 (043.004)</ta>
            <ta e="T694" id="Seg_6176" s="T691">PKZ_1964_SU0205.PKZ.187 (043.005)</ta>
            <ta e="T696" id="Seg_6177" s="T694">PKZ_1964_SU0205.PKZ.188 (043.006)</ta>
            <ta e="T699" id="Seg_6178" s="T696">PKZ_1964_SU0205.PKZ.189 (043.007)</ta>
            <ta e="T703" id="Seg_6179" s="T699">PKZ_1964_SU0205.PKZ.190 (044.001)</ta>
            <ta e="T706" id="Seg_6180" s="T703">PKZ_1964_SU0205.PKZ.191 (044.002)</ta>
            <ta e="T709" id="Seg_6181" s="T706">PKZ_1964_SU0205.PKZ.192 (044.003)</ta>
            <ta e="T718" id="Seg_6182" s="T709">PKZ_1964_SU0205.PKZ.193 (044.004)</ta>
            <ta e="T722" id="Seg_6183" s="T718">PKZ_1964_SU0205.PKZ.194 (044.005)</ta>
            <ta e="T725" id="Seg_6184" s="T722">PKZ_1964_SU0205.PKZ.195 (045.001)</ta>
            <ta e="T727" id="Seg_6185" s="T725">PKZ_1964_SU0205.PKZ.196 (045.002)</ta>
            <ta e="T729" id="Seg_6186" s="T727">PKZ_1964_SU0205.PKZ.197 (045.003)</ta>
            <ta e="T731" id="Seg_6187" s="T729">PKZ_1964_SU0205.PKZ.198 (045.004)</ta>
            <ta e="T733" id="Seg_6188" s="T731">PKZ_1964_SU0205.PKZ.199 (045.005)</ta>
            <ta e="T736" id="Seg_6189" s="T733">PKZ_1964_SU0205.PKZ.200 (045.006)</ta>
            <ta e="T740" id="Seg_6190" s="T736">PKZ_1964_SU0205.PKZ.201 (046.001)</ta>
            <ta e="T742" id="Seg_6191" s="T740">PKZ_1964_SU0205.PKZ.202 (046.002)</ta>
            <ta e="T747" id="Seg_6192" s="T742">PKZ_1964_SU0205.PKZ.203 (046.003)</ta>
            <ta e="T748" id="Seg_6193" s="T747">PKZ_1964_SU0205.PKZ.204 (046.004)</ta>
            <ta e="T751" id="Seg_6194" s="T748">PKZ_1964_SU0205.PKZ.205 (047.001)</ta>
            <ta e="T757" id="Seg_6195" s="T751">PKZ_1964_SU0205.PKZ.206 (047.002)</ta>
            <ta e="T760" id="Seg_6196" s="T757">PKZ_1964_SU0205.PKZ.207 (047.003)</ta>
            <ta e="T763" id="Seg_6197" s="T760">PKZ_1964_SU0205.PKZ.208 (047.004)</ta>
            <ta e="T766" id="Seg_6198" s="T763">PKZ_1964_SU0205.PKZ.209 (047.005)</ta>
            <ta e="T773" id="Seg_6199" s="T770">PKZ_1964_SU0205.PKZ.210 (047.006)</ta>
            <ta e="T775" id="Seg_6200" s="T773">PKZ_1964_SU0205.PKZ.211 (047.007)</ta>
            <ta e="T779" id="Seg_6201" s="T775">PKZ_1964_SU0205.PKZ.212 (048.001)</ta>
            <ta e="T781" id="Seg_6202" s="T779">PKZ_1964_SU0205.PKZ.213 (048.002)</ta>
            <ta e="T783" id="Seg_6203" s="T781">PKZ_1964_SU0205.PKZ.214 (048.003)</ta>
            <ta e="T785" id="Seg_6204" s="T783">PKZ_1964_SU0205.PKZ.215 (048.004)</ta>
            <ta e="T789" id="Seg_6205" s="T785">PKZ_1964_SU0205.PKZ.216 (048.005)</ta>
            <ta e="T792" id="Seg_6206" s="T789">PKZ_1964_SU0205.PKZ.217 (049.001)</ta>
            <ta e="T801" id="Seg_6207" s="T792">PKZ_1964_SU0205.PKZ.218 (049.002)</ta>
            <ta e="T803" id="Seg_6208" s="T801">PKZ_1964_SU0205.PKZ.219 (049.003)</ta>
            <ta e="T807" id="Seg_6209" s="T803">PKZ_1964_SU0205.PKZ.220 (049.004)</ta>
            <ta e="T811" id="Seg_6210" s="T807">PKZ_1964_SU0205.PKZ.221 (049.005)</ta>
            <ta e="T814" id="Seg_6211" s="T811">PKZ_1964_SU0205.PKZ.222 (050.001)</ta>
            <ta e="T816" id="Seg_6212" s="T814">PKZ_1964_SU0205.PKZ.223 (050.002)</ta>
            <ta e="T819" id="Seg_6213" s="T816">PKZ_1964_SU0205.PKZ.224 (050.003)</ta>
            <ta e="T825" id="Seg_6214" s="T819">PKZ_1964_SU0205.PKZ.225 (050.004)</ta>
            <ta e="T827" id="Seg_6215" s="T825">PKZ_1964_SU0205.PKZ.226 (050.005)</ta>
            <ta e="T829" id="Seg_6216" s="T827">PKZ_1964_SU0205.PKZ.227 (050.006)</ta>
            <ta e="T830" id="Seg_6217" s="T829">PKZ_1964_SU0205.PKZ.228 (050.007)</ta>
            <ta e="T834" id="Seg_6218" s="T830">PKZ_1964_SU0205.PKZ.229 (051.001)</ta>
            <ta e="T838" id="Seg_6219" s="T834">PKZ_1964_SU0205.PKZ.230 (051.002)</ta>
            <ta e="T845" id="Seg_6220" s="T838">PKZ_1964_SU0205.PKZ.231 (051.003)</ta>
            <ta e="T848" id="Seg_6221" s="T845">PKZ_1964_SU0205.PKZ.232 (051.004)</ta>
            <ta e="T854" id="Seg_6222" s="T848">PKZ_1964_SU0205.PKZ.233 (051.005)</ta>
            <ta e="T858" id="Seg_6223" s="T854">PKZ_1964_SU0205.PKZ.234 (051.006)</ta>
            <ta e="T860" id="Seg_6224" s="T858">PKZ_1964_SU0205.PKZ.235 (051.007)</ta>
            <ta e="T867" id="Seg_6225" s="T860">PKZ_1964_SU0205.PKZ.236 (051.008)</ta>
            <ta e="T870" id="Seg_6226" s="T867">PKZ_1964_SU0205.PKZ.237 (051.009)</ta>
            <ta e="T871" id="Seg_6227" s="T870">PKZ_1964_SU0205.PKZ.238 (051.010)</ta>
            <ta e="T872" id="Seg_6228" s="T871">PKZ_1964_SU0205.PKZ.239 (051.011)</ta>
            <ta e="T876" id="Seg_6229" s="T872">PKZ_1964_SU0205.PKZ.240 (052.001)</ta>
            <ta e="T880" id="Seg_6230" s="T876">PKZ_1964_SU0205.PKZ.241 (052.002)</ta>
            <ta e="T883" id="Seg_6231" s="T880">PKZ_1964_SU0205.PKZ.242 (052.003)</ta>
            <ta e="T885" id="Seg_6232" s="T883">PKZ_1964_SU0205.PKZ.243 (052.004)</ta>
            <ta e="T888" id="Seg_6233" s="T885">PKZ_1964_SU0205.PKZ.244 (053.001)</ta>
            <ta e="T891" id="Seg_6234" s="T888">PKZ_1964_SU0205.PKZ.245 (053.002)</ta>
            <ta e="T894" id="Seg_6235" s="T891">PKZ_1964_SU0205.PKZ.246 (053.003)</ta>
            <ta e="T896" id="Seg_6236" s="T894">PKZ_1964_SU0205.PKZ.247 (053.004)</ta>
            <ta e="T898" id="Seg_6237" s="T896">PKZ_1964_SU0205.PKZ.248 (053.005)</ta>
            <ta e="T902" id="Seg_6238" s="T898">PKZ_1964_SU0205.PKZ.249 (053.006)</ta>
            <ta e="T904" id="Seg_6239" s="T902">PKZ_1964_SU0205.PKZ.250 (053.007)</ta>
            <ta e="T906" id="Seg_6240" s="T904">PKZ_1964_SU0205.PKZ.251 (053.008)</ta>
            <ta e="T908" id="Seg_6241" s="T906">PKZ_1964_SU0205.PKZ.252 (053.009)</ta>
            <ta e="T909" id="Seg_6242" s="T908">PKZ_1964_SU0205.PKZ.253 (053.010)</ta>
            <ta e="T913" id="Seg_6243" s="T909">PKZ_1964_SU0205.PKZ.254 (054.001)</ta>
            <ta e="T920" id="Seg_6244" s="T913">PKZ_1964_SU0205.PKZ.255 (054.002)</ta>
            <ta e="T922" id="Seg_6245" s="T920">PKZ_1964_SU0205.PKZ.256 (054.003)</ta>
            <ta e="T927" id="Seg_6246" s="T922">PKZ_1964_SU0205.PKZ.257 (054.004)</ta>
            <ta e="T931" id="Seg_6247" s="T927">PKZ_1964_SU0205.PKZ.258 (054.005)</ta>
            <ta e="T934" id="Seg_6248" s="T931">PKZ_1964_SU0205.PKZ.259 (054.006)</ta>
            <ta e="T938" id="Seg_6249" s="T934">PKZ_1964_SU0205.PKZ.260 (054.007)</ta>
            <ta e="T941" id="Seg_6250" s="T938">PKZ_1964_SU0205.PKZ.261 (055.001)</ta>
            <ta e="T943" id="Seg_6251" s="T941">PKZ_1964_SU0205.PKZ.262 (055.002)</ta>
            <ta e="T949" id="Seg_6252" s="T943">PKZ_1964_SU0205.PKZ.263 (055.003)</ta>
            <ta e="T951" id="Seg_6253" s="T949">PKZ_1964_SU0205.PKZ.264 (055.004)</ta>
            <ta e="T953" id="Seg_6254" s="T951">PKZ_1964_SU0205.PKZ.265 (055.005)</ta>
            <ta e="T956" id="Seg_6255" s="T953">PKZ_1964_SU0205.PKZ.266 (055.006)</ta>
            <ta e="T966" id="Seg_6256" s="T956">PKZ_1964_SU0205.PKZ.267 (056.001)</ta>
            <ta e="T969" id="Seg_6257" s="T966">PKZ_1964_SU0205.PKZ.268 (056.002)</ta>
            <ta e="T975" id="Seg_6258" s="T969">PKZ_1964_SU0205.PKZ.269 (057)</ta>
            <ta e="T982" id="Seg_6259" s="T975">PKZ_1964_SU0205.PKZ.270 (058)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PKZ">
            <ta e="T13" id="Seg_6260" s="T4">((DMG)) Я сперва расскажу, погоди, не…</ta>
            <ta e="T20" id="Seg_6261" s="T14">Dʼijenə teinen kallam, (in -) ineziʔ. </ta>
            <ta e="T30" id="Seg_6262" s="T20">Gijen băra, nada ipek enzittə, tus enzittə, šamnak nada enzittə. </ta>
            <ta e="T33" id="Seg_6263" s="T30">Nada uja enzittə. </ta>
            <ta e="T38" id="Seg_6264" s="T33">Nada ipek enzittə ((na -)). </ta>
            <ta e="T42" id="Seg_6265" s="T38">Говорю, господи прости! </ta>
            <ta e="T51" id="Seg_6266" s="T46">Ipek enzittə nada. </ta>
            <ta e="T54" id="Seg_6267" s="T51">Kajak enzittə nada. </ta>
            <ta e="T56" id="Seg_6268" s="T54">Munujʔ enzittə. </ta>
            <ta e="T59" id="Seg_6269" s="T56">Šamnak nada enzittə. </ta>
            <ta e="T62" id="Seg_6270" s="T59">Nada aspaʔ enzittə. </ta>
            <ta e="T65" id="Seg_6271" s="T62">Men izittə nada. </ta>
            <ta e="T68" id="Seg_6272" s="T65">Multuk izittə nada. </ta>
            <ta e="T71" id="Seg_6273" s="T68">Nada baltu izittə. </ta>
            <ta e="T74" id="Seg_6274" s="T71">Dagaj izittə nada. </ta>
            <ta e="T86" id="Seg_6275" s="T85">((kukganʼanʼi)). </ta>
            <ta e="T94" id="Seg_6276" s="T90">Nu kuzasogə šonəgaʔi dʼăbaktərzittə. </ta>
            <ta e="T95" id="Seg_6277" s="T94">Amnaʔ! </ta>
            <ta e="T96" id="Seg_6278" s="T95">Dʼăbaktərləbaʔ! </ta>
            <ta e="T101" id="Seg_6279" s="T96">Amorzittə padʼi axota, amnaʔ, amoraʔ! </ta>
            <ta e="T104" id="Seg_6280" s="T101">Dĭn ipek iʔbölaʔbə. </ta>
            <ta e="T106" id="Seg_6281" s="T104">Kajak nuga. </ta>
            <ta e="T115" id="Seg_6282" s="T106">Šamnak ige, tus ige, munujʔ, uja, kola, amaʔ bar! </ta>
            <ta e="T117" id="Seg_6283" s="T115">(Iʔ alomaʔ!). </ta>
            <ta e="T119" id="Seg_6284" s="T117">Iʔ kirgaːraʔ! </ta>
            <ta e="T122" id="Seg_6285" s="T119">A to münörlem tănan! </ta>
            <ta e="T124" id="Seg_6286" s="T122">Iʔ dʼoraʔ! </ta>
            <ta e="T126" id="Seg_6287" s="T124">Iʔ kuroːmaʔ! </ta>
            <ta e="T128" id="Seg_6288" s="T126">Iʔ šaːmaʔ! </ta>
            <ta e="T131" id="Seg_6289" s="T128">Amnoːlaʔbəl bar, šaːmnaʔbəl! </ta>
            <ta e="T135" id="Seg_6290" s="T131">Tăn ugaːndə iʔgö šaːmnaʔbəl. </ta>
            <ta e="T137" id="Seg_6291" s="T135">Все, наверное. ((DMG))</ta>
            <ta e="T141" id="Seg_6292" s="T137">Măn tüjö kallam šumuranə. </ta>
            <ta e="T145" id="Seg_6293" s="T141">A tăn maʔnəl amnoʔ! </ta>
            <ta e="T147" id="Seg_6294" s="T145">(Iʔ alomaʔ!) </ta>
            <ta e="T151" id="Seg_6295" s="T147">Măn ĭmbi-nʼibudʼ tănan detlem. </ta>
            <ta e="T154" id="Seg_6296" s="T151">Dĭgəttə mĭlem tănan. </ta>
            <ta e="T161" id="Seg_6297" s="T154">Taːldʼen (di -) taːldʼen šobi măn tuganbə. </ta>
            <ta e="T165" id="Seg_6298" s="T161">Măn mămbiam: iʔbeʔ kunoːlzittə! </ta>
            <ta e="T171" id="Seg_6299" s="T165">Dĭgəttə mămbiam: amoraʔ, padʼi amorzittə axota. </ta>
            <ta e="T180" id="Seg_6300" s="T171">Dĭ mămbi: (măn) măn ej (amori -) amorzittə ((n)). </ta>
            <ta e="T184" id="Seg_6301" s="T180">Măn turagən ambiam dĭn. </ta>
            <ta e="T189" id="Seg_6302" s="T184">Dĭgəttə (iʔbə -) iʔbəbi kunoːlzittə. </ta>
            <ta e="T191" id="Seg_6303" s="T189">Măn iʔbəbiem. </ta>
            <ta e="T193" id="Seg_6304" s="T191">Erten uʔbdəbiam. </ta>
            <ta e="T195" id="Seg_6305" s="T193">Šumuranə mĭmbiem. </ta>
            <ta e="T196" id="Seg_6306" s="T195">Šobiam. </ta>
            <ta e="T199" id="Seg_6307" s="T196">Dĭʔnə mămbiam: uʔbdaʔ! </ta>
            <ta e="T207" id="Seg_6308" s="T199">Amoraʔ, dĭn namzəga (su ig-) süt ige. </ta>
            <ta e="T211" id="Seg_6309" s="T207">Keʔbde ige, munujʔ, ipek. </ta>
            <ta e="T213" id="Seg_6310" s="T211">Amoraʔ, amnaʔ. </ta>
            <ta e="T218" id="Seg_6311" s="T213">(Dĭ ba -) dĭ uʔpi. </ta>
            <ta e="T219" id="Seg_6312" s="T218">Bozəjzəbi. </ta>
            <ta e="T222" id="Seg_6313" s="T219">Dĭgəttə amnobi amorzittə. </ta>
            <ta e="T223" id="Seg_6314" s="T222">Всё. </ta>
            <ta e="T226" id="Seg_6315" s="T223">Dĭgəttə kala dʼürbi. </ta>
            <ta e="T229" id="Seg_6316" s="T226">A măn bozəjbiam. </ta>
            <ta e="T232" id="Seg_6317" s="T229">Ну, все ((…))((DMG)). </ta>
            <ta e="T236" id="Seg_6318" s="T232">Taːldʼen măn miʔ tuʔluʔ. </ta>
            <ta e="T239" id="Seg_6319" s="T236">(Tu) tažerbibaʔ, ineziʔ. </ta>
            <ta e="T242" id="Seg_6320" s="T239">Šide koʔbdo embiʔi. </ta>
            <ta e="T249" id="Seg_6321" s="T242">Onʼiʔ tibi măn (bospəm) (u -) embiem. </ta>
            <ta e="T250" id="Seg_6322" s="T249">Tažerbibaʔ. </ta>
            <ta e="T256" id="Seg_6323" s="T250">Погоди, как, я это забыла. ((DMG))</ta>
            <ta e="T260" id="Seg_6324" s="T256">Măn karəldʼaːn multʼanə mĭmbiem. </ta>
            <ta e="T264" id="Seg_6325" s="T260">Bozəjbiam, šišəge bü ibi. </ta>
            <ta e="T266" id="Seg_6326" s="T264">Dʼibige bü. </ta>
            <ta e="T268" id="Seg_6327" s="T266">Saːbən ibiem. </ta>
            <ta e="T270" id="Seg_6328" s="T268">Kujnek ibiem. </ta>
            <ta e="T274" id="Seg_6329" s="T270">Multʼagəʔ šobiam, tüžöj šobi. </ta>
            <ta e="T276" id="Seg_6330" s="T274">Măn surdobiam. </ta>
            <ta e="T278" id="Seg_6331" s="T276">Ugaːndə taraːruʔbiem. </ta>
            <ta e="T282" id="Seg_6332" s="T278">Saʔməluʔpam (i) i kunoːlbiam. </ta>
            <ta e="T289" id="Seg_6333" s="T282">Teinen no măn kallam beškejleʔ, beške detlem. </ta>
            <ta e="T291" id="Seg_6334" s="T289">Dĭgəttə bozlam. </ta>
            <ta e="T293" id="Seg_6335" s="T291">Tustʼarlim bar. </ta>
            <ta e="T296" id="Seg_6336" s="T293">Dĭgəttə tustʼarləj bar. </ta>
            <ta e="T300" id="Seg_6337" s="T296">Dĭgəttə amzittə i možna. </ta>
            <ta e="T306" id="Seg_6338" s="T300">Koŋ bar amnoːlaʔpi bar, ugaːndə jezerik. </ta>
            <ta e="T311" id="Seg_6339" s="T306">Ara bĭʔpi, ugaːndə iʔgö bĭʔpi. </ta>
            <ta e="T316" id="Seg_6340" s="T311">Saʔməluʔpi bar, mut-mat (kudo-) kudonzlaʔbə. </ta>
            <ta e="T319" id="Seg_6341" s="T316">Măn dĭm amnəːlbiam. </ta>
            <ta e="T321" id="Seg_6342" s="T319">Amnoʔ, mămbiam. </ta>
            <ta e="T326" id="Seg_6343" s="T321">Măn mămbiam: "Iʔ kudonzaʔ, amnoʔ! </ta>
            <ta e="T330" id="Seg_6344" s="T326">Amnoʔ segi bü bĭssittə. </ta>
            <ta e="T335" id="Seg_6345" s="T330">Sĭreʔpne iʔbölaʔbə, ende segi bütnə. </ta>
            <ta e="T337" id="Seg_6346" s="T335">Munuʔi iʔbölaʔbəʔjə. </ta>
            <ta e="T341" id="Seg_6347" s="T337">(Ikə -) Ipek amaʔ! </ta>
            <ta e="T343" id="Seg_6348" s="T341">Uja amaʔ! </ta>
            <ta e="T345" id="Seg_6349" s="T343">Kola amaʔ! </ta>
            <ta e="T348" id="Seg_6350" s="T345">Munuj iʔbölaʔbə, amaʔ! </ta>
            <ta e="T352" id="Seg_6351" s="T348">Kajaʔ amaʔ, oroma amaʔ! </ta>
            <ta e="T355" id="Seg_6352" s="T352">Namzəga süt amaʔ! </ta>
            <ta e="T357" id="Seg_6353" s="T355">Kola amaʔ! </ta>
            <ta e="T358" id="Seg_6354" s="T357">Хватит. </ta>
            <ta e="T362" id="Seg_6355" s="T358">Nada măna jama šöʔsittə. </ta>
            <ta e="T364" id="Seg_6356" s="T362">Parga šöʔsittə. </ta>
            <ta e="T370" id="Seg_6357" s="T364">Nada (kujnet -) kujnek šöʔsittə, izittə. </ta>
            <ta e="T375" id="Seg_6358" s="T370">(Na -) Nada piʔmə šöʔsittə. </ta>
            <ta e="T382" id="Seg_6359" s="T375">A to măn nʼim bar šerzittə naga ĭmbi. </ta>
            <ta e="T384" id="Seg_6360" s="T382">Ĭmbidə naga. </ta>
            <ta e="T392" id="Seg_6361" s="T384">Măn kallam Kazan turanə i nada ipek izittə. </ta>
            <ta e="T395" id="Seg_6362" s="T392">Nada sĭreʔpne izittə. </ta>
            <ta e="T401" id="Seg_6363" s="T395">Nada (mul -) multukdə ipek izittə. </ta>
            <ta e="T402" id="Seg_6364" s="T401">Nada. </ta>
            <ta e="T410" id="Seg_6365" s="T408">No, ipek. </ta>
            <ta e="T413" id="Seg_6366" s="T410">Ну, всё ((DMG)). </ta>
            <ta e="T415" id="Seg_6367" s="T413">((DMG)) думала. </ta>
            <ta e="T419" id="Seg_6368" s="T415">Măn dʼijenə dĭgəttə kallam. </ta>
            <ta e="T421" id="Seg_6369" s="T419">Ну, всё. </ta>
            <ta e="T427" id="Seg_6370" s="T421">Teinen (e -) dĭzeŋ dʼijegəʔ šoləʔi. </ta>
            <ta e="T430" id="Seg_6371" s="T427">Padʼi uja detleʔi. </ta>
            <ta e="T432" id="Seg_6372" s="T430">Kalba deʔleʔi. </ta>
            <ta e="T437" id="Seg_6373" s="T432">(Tʼaarlaʔ-) Tʼagarzittə nada da tustʼarzittə. </ta>
            <ta e="T447" id="Seg_6374" s="T437">Munujʔ enzittə, oroma enzittə i bü kămnasʼtə i amorzittə šamnakziʔ. </ta>
            <ta e="T456" id="Seg_6375" s="T447">Nada kanzittə (tol - to -) kanzittə (toli -). </ta>
            <ta e="T458" id="Seg_6376" s="T456">Погоди… </ta>
            <ta e="T461" id="Seg_6377" s="T458">((DMG)) Talerzittə, ipek kuʔsittə. </ta>
            <ta e="T465" id="Seg_6378" s="T461">Dĭgəttə özerləj, pădəsʼtə nada. </ta>
            <ta e="T468" id="Seg_6379" s="T465">Dĭgəttə toʔnarzittə nada. </ta>
            <ta e="T478" id="Seg_6380" s="T468">Dĭgəttə ((PAUSE)) Погоди, вот ((DMG)) пустил, dĭgəttə nʼeʔsittə nada. </ta>
            <ta e="T483" id="Seg_6381" s="T478">(Dĭgət -) Dĭgəttə nada pürzittə. </ta>
            <ta e="T486" id="Seg_6382" s="T483">Dĭgəttə amorzittə nada. </ta>
            <ta e="T488" id="Seg_6383" s="T486">Вот… ((DMG))</ta>
            <ta e="T496" id="Seg_6384" s="T488">Teinen urgo dʼala, (i -) il ugaːndə iʔgö. </ta>
            <ta e="T498" id="Seg_6385" s="T496">Kuvasəʔi bar. </ta>
            <ta e="T500" id="Seg_6386" s="T498">Ara bĭtleʔbəʔjə. </ta>
            <ta e="T502" id="Seg_6387" s="T500">Sʼarlaʔbəʔjə гармонию. </ta>
            <ta e="T503" id="Seg_6388" s="T502">Suʔmiːleʔbəʔjə. </ta>
            <ta e="T505" id="Seg_6389" s="T503">Nüjnə nüjleʔbəʔjə. </ta>
            <ta e="T507" id="Seg_6390" s="T505">Jakšə nüke! </ta>
            <ta e="T513" id="Seg_6391" s="T507">Ipek bar (pür -) pürbi jakšə. </ta>
            <ta e="T514" id="Seg_6392" s="T513">Nömər. </ta>
            <ta e="T520" id="Seg_6393" s="T514">Tüjö (amo - amo -) amorgaʔ! </ta>
            <ta e="T522" id="Seg_6394" s="T520">Padʼi püjöliaʔi. </ta>
            <ta e="T524" id="Seg_6395" s="T522">Tüjö uʔblam. </ta>
            <ta e="T527" id="Seg_6396" s="T524">Amgaʔ, (amno=) amnogaʔ! </ta>
            <ta e="T530" id="Seg_6397" s="T527">Nʼešpək kuza ambi. </ta>
            <ta e="T533" id="Seg_6398" s="T530">Ipek lem keʔbdeziʔ. </ta>
            <ta e="T535" id="Seg_6399" s="T533">Oroma nuldəbiam. </ta>
            <ta e="T541" id="Seg_6400" s="T535">Amorgaʔ, (šut) süt (bĭt -) bĭttə! </ta>
            <ta e="T543" id="Seg_6401" s="T541">Всё… </ta>
            <ta e="T547" id="Seg_6402" s="T543">((DMG)) Tăn iššo kunoːllaʔbəl, uʔbdaʔ! </ta>
            <ta e="T554" id="Seg_6403" s="T547">A to šĭšəge bü detləm, tănan kămnam bar. </ta>
            <ta e="T557" id="Seg_6404" s="T554">Dĭgəttə bar süʔməluʔləl. </ta>
            <ta e="T559" id="Seg_6405" s="T557">Всё, наверное. </ta>
            <ta e="T564" id="Seg_6406" s="T559">Погоди, погоди, как нас… </ta>
            <ta e="T566" id="Seg_6407" s="T564">((DMG)) (-dot). </ta>
            <ta e="T568" id="Seg_6408" s="T566">Ej nʼilgöt! </ta>
            <ta e="T570" id="Seg_6409" s="T568">(Iʔ jaʔ)! </ta>
            <ta e="T571" id="Seg_6410" s="T570">Господи. </ta>
            <ta e="T575" id="Seg_6411" s="T573">Iʔ dʼăbaktəraʔ! </ta>
            <ta e="T579" id="Seg_6412" s="T577">Iʔ kudonzaʔ! </ta>
            <ta e="T584" id="Seg_6413" s="T582">Iʔ kirgaːraʔ! </ta>
            <ta e="T588" id="Seg_6414" s="T586">Kuʔlə mʼaŋŋaʔbə. </ta>
            <ta e="T590" id="Seg_6415" s="T588">Kĭškit kuʔlə. </ta>
            <ta e="T594" id="Seg_6416" s="T592">Iʔ nuʔməʔ! </ta>
            <ta e="T596" id="Seg_6417" s="T594">Iʔ suʔmiʔ! </ta>
            <ta e="T598" id="Seg_6418" s="T596">Iʔ nüjnəʔ! </ta>
            <ta e="T600" id="Seg_6419" s="T598">Ugaːndə jakšə. </ta>
            <ta e="T602" id="Seg_6420" s="T600">Ugaːndə kuvas. </ta>
            <ta e="T604" id="Seg_6421" s="T602">Ugaːndə komu. </ta>
            <ta e="T605" id="Seg_6422" s="T604">Ugaːndə. </ta>
            <ta e="T609" id="Seg_6423" s="T606">Kəbo, ugaːndə numo. </ta>
            <ta e="T614" id="Seg_6424" s="T612">Ugaːndə üdʼüge. </ta>
            <ta e="T618" id="Seg_6425" s="T614">Ну, теперь, наверное, всё. </ta>
            <ta e="T621" id="Seg_6426" s="T618">Padʼi plat tojirbial. </ta>
            <ta e="T623" id="Seg_6427" s="T621">Na što deʔpiel? </ta>
            <ta e="T626" id="Seg_6428" s="T623">Măn ej ilim. </ta>
            <ta e="T633" id="Seg_6429" s="T626">Nʼe nada tojirzittə, a to ej jakšə moləj. </ta>
            <ta e="T638" id="Seg_6430" s="T633">Udam băʔpiam bar, kem mʼaŋŋaʔbə. </ta>
            <ta e="T642" id="Seg_6431" s="T638">Deʔ ĭmbi-nʼibudʼ udam sarzittə! </ta>
            <ta e="T650" id="Seg_6432" s="T645">Na što (tagajziʔ=) tagaj ibiel? </ta>
            <ta e="T654" id="Seg_6433" s="T650">Tănan iššo nada … </ta>
            <ta e="T658" id="Seg_6434" s="T654">Beržə, surno bar šonəga. </ta>
            <ta e="T662" id="Seg_6435" s="T658">Sĭre bar šonəga beržəziʔ. </ta>
            <ta e="T664" id="Seg_6436" s="T662">Ugaːndə šĭšəge. </ta>
            <ta e="T667" id="Seg_6437" s="T664">Măn ugaːndə kămbiam. </ta>
            <ta e="T670" id="Seg_6438" s="T667">Da tăŋ kămbiam. </ta>
            <ta e="T672" id="Seg_6439" s="T670">Ujum kămbi. </ta>
            <ta e="T674" id="Seg_6440" s="T672">Udam kămbi. </ta>
            <ta e="T677" id="Seg_6441" s="T674">Kadəlbə bar kămbi. </ta>
            <ta e="T682" id="Seg_6442" s="T677">Teinen (jak -) kuvas dʼala. </ta>
            <ta e="T685" id="Seg_6443" s="T682">Kuja bar măndolaʔbə. </ta>
            <ta e="T688" id="Seg_6444" s="T685">Nʼuʔnun ĭmbidə naga. </ta>
            <ta e="T691" id="Seg_6445" s="T688">Ugaːndə jakšə dʼala. </ta>
            <ta e="T694" id="Seg_6446" s="T691">Il bar togonorlaʔbəʔjə. </ta>
            <ta e="T696" id="Seg_6447" s="T694">Toltano amlaʔbəʔjə. </ta>
            <ta e="T699" id="Seg_6448" s="T696">Ineʔi tarirlaʔbəʔjə bar. </ta>
            <ta e="T703" id="Seg_6449" s="T699">Nada kanzittə noʔ jaʔsittə. </ta>
            <ta e="T706" id="Seg_6450" s="T703">Šapku nada izittə. </ta>
            <ta e="T709" id="Seg_6451" s="T706">Nada bü izittə. </ta>
            <ta e="T718" id="Seg_6452" s="T709">Süt izittə nada, ipek izittə, tus izittə, munujʔ izittə. </ta>
            <ta e="T722" id="Seg_6453" s="T718">Pi nada izittə, šapku. </ta>
            <ta e="T725" id="Seg_6454" s="T722">Albuga teinen kuʔpiem. </ta>
            <ta e="T727" id="Seg_6455" s="T725">Tažəp kuʔpiem. </ta>
            <ta e="T729" id="Seg_6456" s="T727">Urgaːba kuʔpiem. </ta>
            <ta e="T731" id="Seg_6457" s="T729">Söːn kuʔpiem. </ta>
            <ta e="T733" id="Seg_6458" s="T731">Askər kuʔpiem. </ta>
            <ta e="T736" id="Seg_6459" s="T733">Poʔto kuʔpiem, multuksiʔ. </ta>
            <ta e="T740" id="Seg_6460" s="T736">Tüžöjʔi bar noʔ amniaʔi. </ta>
            <ta e="T742" id="Seg_6461" s="T740">Mĭlleʔbəʔjə dĭn. </ta>
            <ta e="T747" id="Seg_6462" s="T742">Dĭ kuza detləj их maʔndə. </ta>
            <ta e="T748" id="Seg_6463" s="T747">Всё. </ta>
            <ta e="T751" id="Seg_6464" s="T748">Nada eššim bazəsʼtə. </ta>
            <ta e="T757" id="Seg_6465" s="T751">Nada (e -) eššim bar dʼorlaʔbə. </ta>
            <ta e="T760" id="Seg_6466" s="T757">Nada dĭm köːdərzittə. </ta>
            <ta e="T763" id="Seg_6467" s="T760">Nada amorzittə mĭzittə. </ta>
            <ta e="T766" id="Seg_6468" s="T763">Nada kunoːlzittə enzittə. </ta>
            <ta e="T773" id="Seg_6469" s="T770">Oːrdəʔ eššim bar! </ta>
            <ta e="T775" id="Seg_6470" s="T773">Pušaj kunoːllaʔbə. </ta>
            <ta e="T779" id="Seg_6471" s="T775">Amnoʔ, tüjö büjleʔ kallam. </ta>
            <ta e="T781" id="Seg_6472" s="T779">Bü detlem. </ta>
            <ta e="T783" id="Seg_6473" s="T781">Dĭgəttə bĭtlel. </ta>
            <ta e="T785" id="Seg_6474" s="T783">Iʔ kanaʔ! </ta>
            <ta e="T789" id="Seg_6475" s="T785">A dĭgəttə kallal maʔnəl. </ta>
            <ta e="T792" id="Seg_6476" s="T789">Tüjö kem pürlem. </ta>
            <ta e="T801" id="Seg_6477" s="T792">Nada dibər süt kămnasʼtə, kajak enzittə, alʼi sil enzittə. </ta>
            <ta e="T803" id="Seg_6478" s="T801">Tüjö amorləbəj. </ta>
            <ta e="T807" id="Seg_6479" s="T803">Iʔ (baza), a to dʼibige. </ta>
            <ta e="T811" id="Seg_6480" s="T807">(Ipek -) Ipeksiʔ amaʔ! </ta>
            <ta e="T814" id="Seg_6481" s="T811">((DMG)) (Koʔbdo) šobi. </ta>
            <ta e="T816" id="Seg_6482" s="T814">Eʔbdə sĭre. </ta>
            <ta e="T819" id="Seg_6483" s="T816">Bostə bar todam. </ta>
            <ta e="T825" id="Seg_6484" s="T819">I dĭ kuza šobi, nʼešpək ugaːndə. </ta>
            <ta e="T827" id="Seg_6485" s="T825">(I mĭʔ!)</ta>
            <ta e="T829" id="Seg_6486" s="T827">Elet šobi. </ta>
            <ta e="T830" id="Seg_6487" s="T829">Всё. </ta>
            <ta e="T834" id="Seg_6488" s="T830">Dĭzeŋ toltano ej amnəbiʔi. </ta>
            <ta e="T838" id="Seg_6489" s="T834">Măn abam dĭzeŋdə amnəlbi. </ta>
            <ta e="T845" id="Seg_6490" s="T838">Dĭzeŋ uja iʔgö ibi bar, uja ambiʔi. </ta>
            <ta e="T848" id="Seg_6491" s="T845">Ipek amga ibi. </ta>
            <ta e="T854" id="Seg_6492" s="T848">A daška ĭmbidə (n -) naga. </ta>
            <ta e="T858" id="Seg_6493" s="T854">((DMG)) Ĭmbi amnolaʔbəl, ej dʼăbaktərial. </ta>
            <ta e="T860" id="Seg_6494" s="T858">Dʼăbaktəraʔ ĭmbi-nʼibudʼ! </ta>
            <ta e="T867" id="Seg_6495" s="T860">(Sĭjbə verna ĭze-) Sĭjlə verna ĭzemnie. </ta>
            <ta e="T870" id="Seg_6496" s="T867">Ĭmbidə ej dʼăbaktərial. </ta>
            <ta e="T871" id="Seg_6497" s="T870">Dʼăbaktəraʔ! </ta>
            <ta e="T872" id="Seg_6498" s="T871">Всё. </ta>
            <ta e="T876" id="Seg_6499" s="T872">(Gibə -) Gibər kandəgal? </ta>
            <ta e="T880" id="Seg_6500" s="T876">Gijen ibiel, šoʔ döːbər! </ta>
            <ta e="T883" id="Seg_6501" s="T880">Ĭmbi-nʼibudʼ nörbəʔ măna! </ta>
            <ta e="T885" id="Seg_6502" s="T883">Опять забыла. ((DMG)). </ta>
            <ta e="T888" id="Seg_6503" s="T885">Büzʼe nüketsiʔ amnolaʔpiʔi. </ta>
            <ta e="T891" id="Seg_6504" s="T888">Dĭzeŋ esseŋdə ibiʔi. </ta>
            <ta e="T894" id="Seg_6505" s="T891">Koʔbdo da nʼi. </ta>
            <ta e="T896" id="Seg_6506" s="T894">Tura ibi. </ta>
            <ta e="T898" id="Seg_6507" s="T896">Tüžöj ibi. </ta>
            <ta e="T902" id="Seg_6508" s="T898">Dĭ nüke tüžöj surdobi. </ta>
            <ta e="T904" id="Seg_6509" s="T902">Esseŋdə mĭbi. </ta>
            <ta e="T906" id="Seg_6510" s="T904">Ipek mĭbi. </ta>
            <ta e="T908" id="Seg_6511" s="T906">Dĭzeŋ amniaʔi. </ta>
            <ta e="T909" id="Seg_6512" s="T908">Всё. </ta>
            <ta e="T913" id="Seg_6513" s="T909">Dĭ šidegöʔ kuza šobiʔi. </ta>
            <ta e="T920" id="Seg_6514" s="T913">Tibi da koʔbdo tănan padʼi ĭmbi-nʼibudʼ deʔpiʔi. </ta>
            <ta e="T922" id="Seg_6515" s="T920">Sĭreʔpne deʔpiʔi. </ta>
            <ta e="T927" id="Seg_6516" s="T922">(Segə -) Segi bü bĭtlel. </ta>
            <ta e="T931" id="Seg_6517" s="T927">Padʼi kanfeti deʔpiʔi tănan. </ta>
            <ta e="T934" id="Seg_6518" s="T931">Kujnek padʼi deʔpiʔi. </ta>
            <ta e="T938" id="Seg_6519" s="T934">Piʔmə padʼi deʔpiʔi tănan. </ta>
            <ta e="T941" id="Seg_6520" s="T938">Predsʼedatʼelʼən turat urgo. </ta>
            <ta e="T943" id="Seg_6521" s="T941">Il iʔgö. </ta>
            <ta e="T949" id="Seg_6522" s="T943">(Ši -) Šide nʼi, šide koʔbdo. </ta>
            <ta e="T951" id="Seg_6523" s="T949">Nüke büzʼeʔiziʔ. </ta>
            <ta e="T953" id="Seg_6524" s="T951">Bostə, ne. </ta>
            <ta e="T956" id="Seg_6525" s="T953">(Me-) Menzeŋdə šide. </ta>
            <ta e="T966" id="Seg_6526" s="T956">Onʼiʔ jakšə men, onʼiʔ nagur uju, a teʔtə uju naga. </ta>
            <ta e="T969" id="Seg_6527" s="T966">Четвертой ноги нету. </ta>
            <ta e="T975" id="Seg_6528" s="T969">Tăn măna münörəʔ, a ĭmbiziʔ münörlaʔbəl? </ta>
            <ta e="T982" id="Seg_6529" s="T975">Muzuruksiʔ iʔ münörəʔ, a to măna ĭzemnəj bar. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-PKZ">
            <ta e="T15" id="Seg_6530" s="T14">dʼije-nə</ta>
            <ta e="T16" id="Seg_6531" s="T15">teinen</ta>
            <ta e="T17" id="Seg_6532" s="T16">kal-la-m</ta>
            <ta e="T20" id="Seg_6533" s="T19">ine-ziʔ</ta>
            <ta e="T21" id="Seg_6534" s="T20">gijen</ta>
            <ta e="T22" id="Seg_6535" s="T21">băra</ta>
            <ta e="T23" id="Seg_6536" s="T22">nada</ta>
            <ta e="T24" id="Seg_6537" s="T23">ipek</ta>
            <ta e="T25" id="Seg_6538" s="T24">en-zittə</ta>
            <ta e="T26" id="Seg_6539" s="T25">tus</ta>
            <ta e="T27" id="Seg_6540" s="T26">en-zittə</ta>
            <ta e="T28" id="Seg_6541" s="T27">šamnak</ta>
            <ta e="T29" id="Seg_6542" s="T28">nada</ta>
            <ta e="T30" id="Seg_6543" s="T29">en-zittə</ta>
            <ta e="T31" id="Seg_6544" s="T30">nada</ta>
            <ta e="T32" id="Seg_6545" s="T31">uja</ta>
            <ta e="T33" id="Seg_6546" s="T32">en-zittə</ta>
            <ta e="T34" id="Seg_6547" s="T33">nada</ta>
            <ta e="T35" id="Seg_6548" s="T34">ipek</ta>
            <ta e="T36" id="Seg_6549" s="T35">en-zittə</ta>
            <ta e="T49" id="Seg_6550" s="T46">ipek</ta>
            <ta e="T50" id="Seg_6551" s="T49">en-zittə</ta>
            <ta e="T51" id="Seg_6552" s="T50">nada</ta>
            <ta e="T52" id="Seg_6553" s="T51">kajak</ta>
            <ta e="T53" id="Seg_6554" s="T52">en-zittə</ta>
            <ta e="T54" id="Seg_6555" s="T53">nada</ta>
            <ta e="T55" id="Seg_6556" s="T54">munuj-ʔ</ta>
            <ta e="T56" id="Seg_6557" s="T55">en-zittə</ta>
            <ta e="T57" id="Seg_6558" s="T56">šamnak</ta>
            <ta e="T58" id="Seg_6559" s="T57">nada</ta>
            <ta e="T59" id="Seg_6560" s="T58">en-zittə</ta>
            <ta e="T60" id="Seg_6561" s="T59">nada</ta>
            <ta e="T61" id="Seg_6562" s="T60">aspaʔ</ta>
            <ta e="T62" id="Seg_6563" s="T61">en-zittə</ta>
            <ta e="T63" id="Seg_6564" s="T62">men</ta>
            <ta e="T64" id="Seg_6565" s="T63">i-zittə</ta>
            <ta e="T65" id="Seg_6566" s="T64">nada</ta>
            <ta e="T66" id="Seg_6567" s="T65">multuk</ta>
            <ta e="T67" id="Seg_6568" s="T66">i-zittə</ta>
            <ta e="T68" id="Seg_6569" s="T67">nada</ta>
            <ta e="T69" id="Seg_6570" s="T68">nada</ta>
            <ta e="T70" id="Seg_6571" s="T69">baltu</ta>
            <ta e="T71" id="Seg_6572" s="T70">i-zittə</ta>
            <ta e="T72" id="Seg_6573" s="T71">dagaj</ta>
            <ta e="T73" id="Seg_6574" s="T72">i-zittə</ta>
            <ta e="T74" id="Seg_6575" s="T73">nada</ta>
            <ta e="T86" id="Seg_6576" s="T85">kukganʼanʼi</ta>
            <ta e="T91" id="Seg_6577" s="T90">nu</ta>
            <ta e="T92" id="Seg_6578" s="T91">kuza-so-gə</ta>
            <ta e="T93" id="Seg_6579" s="T92">šonə-ga-ʔi</ta>
            <ta e="T94" id="Seg_6580" s="T93">dʼăbaktər-zittə</ta>
            <ta e="T95" id="Seg_6581" s="T94">amna-ʔ</ta>
            <ta e="T96" id="Seg_6582" s="T95">dʼăbaktər-lə-baʔ</ta>
            <ta e="T97" id="Seg_6583" s="T96">amor-zittə</ta>
            <ta e="T98" id="Seg_6584" s="T97">padʼi</ta>
            <ta e="T99" id="Seg_6585" s="T98">axota</ta>
            <ta e="T100" id="Seg_6586" s="T99">amna-ʔ</ta>
            <ta e="T101" id="Seg_6587" s="T100">amor-a-ʔ</ta>
            <ta e="T102" id="Seg_6588" s="T101">dĭn</ta>
            <ta e="T103" id="Seg_6589" s="T102">ipek</ta>
            <ta e="T104" id="Seg_6590" s="T103">iʔbö-laʔbə</ta>
            <ta e="T105" id="Seg_6591" s="T104">kajak</ta>
            <ta e="T106" id="Seg_6592" s="T105">nu-ga</ta>
            <ta e="T107" id="Seg_6593" s="T106">šamnak</ta>
            <ta e="T108" id="Seg_6594" s="T107">i-ge</ta>
            <ta e="T109" id="Seg_6595" s="T108">tus</ta>
            <ta e="T110" id="Seg_6596" s="T109">i-ge</ta>
            <ta e="T111" id="Seg_6597" s="T110">munuj-ʔ</ta>
            <ta e="T112" id="Seg_6598" s="T111">uja</ta>
            <ta e="T113" id="Seg_6599" s="T112">kola</ta>
            <ta e="T114" id="Seg_6600" s="T113">am-a-ʔ</ta>
            <ta e="T115" id="Seg_6601" s="T114">bar</ta>
            <ta e="T116" id="Seg_6602" s="T115">i-ʔ</ta>
            <ta e="T117" id="Seg_6603" s="T116">aloma-ʔ</ta>
            <ta e="T118" id="Seg_6604" s="T117">i-ʔ</ta>
            <ta e="T119" id="Seg_6605" s="T118">kirgaːr-a-ʔ</ta>
            <ta e="T120" id="Seg_6606" s="T119">ato</ta>
            <ta e="T121" id="Seg_6607" s="T120">münör-le-m</ta>
            <ta e="T122" id="Seg_6608" s="T121">tănan</ta>
            <ta e="T123" id="Seg_6609" s="T122">i-ʔ</ta>
            <ta e="T124" id="Seg_6610" s="T123">dʼor-a-ʔ</ta>
            <ta e="T125" id="Seg_6611" s="T124">i-ʔ</ta>
            <ta e="T126" id="Seg_6612" s="T125">kuroː-m-a-ʔ</ta>
            <ta e="T127" id="Seg_6613" s="T126">i-ʔ</ta>
            <ta e="T128" id="Seg_6614" s="T127">šaːm-a-ʔ</ta>
            <ta e="T129" id="Seg_6615" s="T128">amnoː-laʔbə-l</ta>
            <ta e="T130" id="Seg_6616" s="T129">bar</ta>
            <ta e="T131" id="Seg_6617" s="T130">šaːm-naʔbə-l</ta>
            <ta e="T132" id="Seg_6618" s="T131">tăn</ta>
            <ta e="T133" id="Seg_6619" s="T132">ugaːndə</ta>
            <ta e="T134" id="Seg_6620" s="T133">iʔgö</ta>
            <ta e="T135" id="Seg_6621" s="T134">šaːm-naʔbə-l</ta>
            <ta e="T138" id="Seg_6622" s="T137">măn</ta>
            <ta e="T139" id="Seg_6623" s="T138">tüjö</ta>
            <ta e="T140" id="Seg_6624" s="T139">kal-la-m</ta>
            <ta e="T141" id="Seg_6625" s="T140">šumura-nə</ta>
            <ta e="T142" id="Seg_6626" s="T141">a</ta>
            <ta e="T143" id="Seg_6627" s="T142">tăn</ta>
            <ta e="T144" id="Seg_6628" s="T143">maʔ-nə-l</ta>
            <ta e="T145" id="Seg_6629" s="T144">amno-ʔ</ta>
            <ta e="T146" id="Seg_6630" s="T145">i-ʔ</ta>
            <ta e="T147" id="Seg_6631" s="T146">aloma-ʔ</ta>
            <ta e="T148" id="Seg_6632" s="T147">măn</ta>
            <ta e="T149" id="Seg_6633" s="T148">ĭmbi=nʼibudʼ</ta>
            <ta e="T150" id="Seg_6634" s="T149">tănan</ta>
            <ta e="T151" id="Seg_6635" s="T150">det-le-m</ta>
            <ta e="T152" id="Seg_6636" s="T151">dĭgəttə</ta>
            <ta e="T153" id="Seg_6637" s="T152">mĭ-le-m</ta>
            <ta e="T154" id="Seg_6638" s="T153">tănan</ta>
            <ta e="T155" id="Seg_6639" s="T154">taːldʼen</ta>
            <ta e="T158" id="Seg_6640" s="T157">taːldʼen</ta>
            <ta e="T159" id="Seg_6641" s="T158">šo-bi</ta>
            <ta e="T160" id="Seg_6642" s="T159">măn</ta>
            <ta e="T161" id="Seg_6643" s="T160">tugan-bə</ta>
            <ta e="T162" id="Seg_6644" s="T161">măn</ta>
            <ta e="T163" id="Seg_6645" s="T162">măm-bia-m</ta>
            <ta e="T164" id="Seg_6646" s="T163">iʔb-eʔ</ta>
            <ta e="T165" id="Seg_6647" s="T164">kunoːl-zittə</ta>
            <ta e="T166" id="Seg_6648" s="T165">dĭgəttə</ta>
            <ta e="T167" id="Seg_6649" s="T166">măm-bia-m</ta>
            <ta e="T168" id="Seg_6650" s="T167">amor-a-ʔ</ta>
            <ta e="T169" id="Seg_6651" s="T168">padʼi</ta>
            <ta e="T170" id="Seg_6652" s="T169">amor-zittə</ta>
            <ta e="T171" id="Seg_6653" s="T170">axota</ta>
            <ta e="T172" id="Seg_6654" s="T171">dĭ</ta>
            <ta e="T173" id="Seg_6655" s="T172">măm-bi</ta>
            <ta e="T174" id="Seg_6656" s="T173">măn</ta>
            <ta e="T175" id="Seg_6657" s="T174">măn</ta>
            <ta e="T176" id="Seg_6658" s="T175">ej</ta>
            <ta e="T179" id="Seg_6659" s="T178">amor-zittə</ta>
            <ta e="T181" id="Seg_6660" s="T180">măn</ta>
            <ta e="T182" id="Seg_6661" s="T181">tura-gən</ta>
            <ta e="T183" id="Seg_6662" s="T182">am-bia-m</ta>
            <ta e="T184" id="Seg_6663" s="T183">dĭn</ta>
            <ta e="T185" id="Seg_6664" s="T184">dĭgəttə</ta>
            <ta e="T188" id="Seg_6665" s="T187">iʔbə-bi</ta>
            <ta e="T189" id="Seg_6666" s="T188">kunoːl-zittə</ta>
            <ta e="T190" id="Seg_6667" s="T189">măn</ta>
            <ta e="T191" id="Seg_6668" s="T190">iʔbə-bie-m</ta>
            <ta e="T192" id="Seg_6669" s="T191">erte-n</ta>
            <ta e="T193" id="Seg_6670" s="T192">uʔbdə-bia-m</ta>
            <ta e="T194" id="Seg_6671" s="T193">šumura-nə</ta>
            <ta e="T195" id="Seg_6672" s="T194">mĭm-bie-m</ta>
            <ta e="T196" id="Seg_6673" s="T195">šo-bia-m</ta>
            <ta e="T197" id="Seg_6674" s="T196">dĭʔ-nə</ta>
            <ta e="T198" id="Seg_6675" s="T197">măm-bia-m</ta>
            <ta e="T199" id="Seg_6676" s="T198">uʔbd-aʔ</ta>
            <ta e="T200" id="Seg_6677" s="T199">amor-a-ʔ</ta>
            <ta e="T201" id="Seg_6678" s="T200">dĭn</ta>
            <ta e="T202" id="Seg_6679" s="T201">namzəga</ta>
            <ta e="T206" id="Seg_6680" s="T205">süt</ta>
            <ta e="T207" id="Seg_6681" s="T206">i-ge</ta>
            <ta e="T208" id="Seg_6682" s="T207">keʔbde</ta>
            <ta e="T209" id="Seg_6683" s="T208">i-ge</ta>
            <ta e="T210" id="Seg_6684" s="T209">munuj-ʔ</ta>
            <ta e="T211" id="Seg_6685" s="T210">ipek</ta>
            <ta e="T212" id="Seg_6686" s="T211">amor-a-ʔ</ta>
            <ta e="T213" id="Seg_6687" s="T212">amna-ʔ</ta>
            <ta e="T214" id="Seg_6688" s="T213">dĭ</ta>
            <ta e="T217" id="Seg_6689" s="T216">dĭ</ta>
            <ta e="T218" id="Seg_6690" s="T217">uʔ-pi</ta>
            <ta e="T219" id="Seg_6691" s="T218">bozəj-zə-bi</ta>
            <ta e="T220" id="Seg_6692" s="T219">dĭgəttə</ta>
            <ta e="T221" id="Seg_6693" s="T220">amno-bi</ta>
            <ta e="T222" id="Seg_6694" s="T221">amor-zittə</ta>
            <ta e="T224" id="Seg_6695" s="T223">dĭgəttə</ta>
            <ta e="T225" id="Seg_6696" s="T224">ka-la</ta>
            <ta e="T226" id="Seg_6697" s="T225">dʼür-bi</ta>
            <ta e="T227" id="Seg_6698" s="T226">a</ta>
            <ta e="T228" id="Seg_6699" s="T227">măn</ta>
            <ta e="T229" id="Seg_6700" s="T228">bozəj-bia-m</ta>
            <ta e="T233" id="Seg_6701" s="T232">taːldʼen</ta>
            <ta e="T234" id="Seg_6702" s="T233">măn</ta>
            <ta e="T235" id="Seg_6703" s="T234">miʔ</ta>
            <ta e="T236" id="Seg_6704" s="T235">tuʔluʔ</ta>
            <ta e="T238" id="Seg_6705" s="T237">tažer-bi-baʔ</ta>
            <ta e="T239" id="Seg_6706" s="T238">ine-ziʔ</ta>
            <ta e="T240" id="Seg_6707" s="T239">šide</ta>
            <ta e="T241" id="Seg_6708" s="T240">koʔbdo</ta>
            <ta e="T242" id="Seg_6709" s="T241">em-bi-ʔi</ta>
            <ta e="T243" id="Seg_6710" s="T242">onʼiʔ</ta>
            <ta e="T244" id="Seg_6711" s="T243">tibi</ta>
            <ta e="T245" id="Seg_6712" s="T244">măn</ta>
            <ta e="T246" id="Seg_6713" s="T245">bospə-m</ta>
            <ta e="T249" id="Seg_6714" s="T248">em-bie-m</ta>
            <ta e="T250" id="Seg_6715" s="T249">tažer-bi-baʔ</ta>
            <ta e="T257" id="Seg_6716" s="T256">măn</ta>
            <ta e="T258" id="Seg_6717" s="T257">karəldʼaːn</ta>
            <ta e="T259" id="Seg_6718" s="T258">multʼa-nə</ta>
            <ta e="T260" id="Seg_6719" s="T259">mĭm-bie-m</ta>
            <ta e="T261" id="Seg_6720" s="T260">bozəj-bia-m</ta>
            <ta e="T262" id="Seg_6721" s="T261">šišəge</ta>
            <ta e="T263" id="Seg_6722" s="T262">bü</ta>
            <ta e="T264" id="Seg_6723" s="T263">i-bi</ta>
            <ta e="T265" id="Seg_6724" s="T264">dʼibige</ta>
            <ta e="T266" id="Seg_6725" s="T265">bü</ta>
            <ta e="T267" id="Seg_6726" s="T266">saːbən</ta>
            <ta e="T268" id="Seg_6727" s="T267">i-bie-m</ta>
            <ta e="T269" id="Seg_6728" s="T268">kujnek</ta>
            <ta e="T270" id="Seg_6729" s="T269">i-bie-m</ta>
            <ta e="T271" id="Seg_6730" s="T270">multʼa-gəʔ</ta>
            <ta e="T272" id="Seg_6731" s="T271">šo-bia-m</ta>
            <ta e="T273" id="Seg_6732" s="T272">tüžöj</ta>
            <ta e="T274" id="Seg_6733" s="T273">šo-bi</ta>
            <ta e="T275" id="Seg_6734" s="T274">măn</ta>
            <ta e="T276" id="Seg_6735" s="T275">surdo-bia-m</ta>
            <ta e="T277" id="Seg_6736" s="T276">ugaːndə</ta>
            <ta e="T278" id="Seg_6737" s="T277">taraːr-uʔ-bie-m</ta>
            <ta e="T279" id="Seg_6738" s="T278">saʔmə-luʔ-pa-m</ta>
            <ta e="T280" id="Seg_6739" s="T279">i</ta>
            <ta e="T281" id="Seg_6740" s="T280">i</ta>
            <ta e="T282" id="Seg_6741" s="T281">kunoːl-bia-m</ta>
            <ta e="T283" id="Seg_6742" s="T282">teinen</ta>
            <ta e="T284" id="Seg_6743" s="T283">noʔ</ta>
            <ta e="T285" id="Seg_6744" s="T284">măn</ta>
            <ta e="T286" id="Seg_6745" s="T285">kal-la-m</ta>
            <ta e="T287" id="Seg_6746" s="T286">beške-j-leʔ</ta>
            <ta e="T288" id="Seg_6747" s="T287">beške</ta>
            <ta e="T289" id="Seg_6748" s="T288">det-le-m</ta>
            <ta e="T290" id="Seg_6749" s="T289">dĭgəttə</ta>
            <ta e="T291" id="Seg_6750" s="T290">boz-la-m</ta>
            <ta e="T292" id="Seg_6751" s="T291">tustʼar-li-m</ta>
            <ta e="T293" id="Seg_6752" s="T292">bar</ta>
            <ta e="T294" id="Seg_6753" s="T293">dĭgəttə</ta>
            <ta e="T295" id="Seg_6754" s="T294">tustʼar-lə-j</ta>
            <ta e="T296" id="Seg_6755" s="T295">bar</ta>
            <ta e="T297" id="Seg_6756" s="T296">dĭgəttə</ta>
            <ta e="T298" id="Seg_6757" s="T297">am-zittə</ta>
            <ta e="T299" id="Seg_6758" s="T298">i</ta>
            <ta e="T300" id="Seg_6759" s="T299">možna</ta>
            <ta e="T301" id="Seg_6760" s="T300">koŋ</ta>
            <ta e="T302" id="Seg_6761" s="T301">bar</ta>
            <ta e="T303" id="Seg_6762" s="T302">amnoː-laʔ-pi</ta>
            <ta e="T304" id="Seg_6763" s="T303">bar</ta>
            <ta e="T305" id="Seg_6764" s="T304">ugaːndə</ta>
            <ta e="T306" id="Seg_6765" s="T305">jezerik</ta>
            <ta e="T307" id="Seg_6766" s="T306">ara</ta>
            <ta e="T308" id="Seg_6767" s="T307">bĭʔ-pi</ta>
            <ta e="T309" id="Seg_6768" s="T308">ugaːndə</ta>
            <ta e="T310" id="Seg_6769" s="T309">iʔgö</ta>
            <ta e="T311" id="Seg_6770" s="T310">bĭʔ-pi</ta>
            <ta e="T312" id="Seg_6771" s="T311">saʔmə-luʔ-pi</ta>
            <ta e="T313" id="Seg_6772" s="T312">bar</ta>
            <ta e="T314" id="Seg_6773" s="T313">mut_mat</ta>
            <ta e="T316" id="Seg_6774" s="T315">kudo-nz-laʔbə</ta>
            <ta e="T317" id="Seg_6775" s="T316">măn</ta>
            <ta e="T318" id="Seg_6776" s="T317">dĭ-m</ta>
            <ta e="T319" id="Seg_6777" s="T318">amnəːl-bia-m</ta>
            <ta e="T320" id="Seg_6778" s="T319">amno-ʔ</ta>
            <ta e="T321" id="Seg_6779" s="T320">măm-bia-m</ta>
            <ta e="T322" id="Seg_6780" s="T321">măn</ta>
            <ta e="T323" id="Seg_6781" s="T322">măm-bia-m</ta>
            <ta e="T324" id="Seg_6782" s="T323">i-ʔ</ta>
            <ta e="T325" id="Seg_6783" s="T324">kudo-nza-ʔ</ta>
            <ta e="T326" id="Seg_6784" s="T325">amno-ʔ</ta>
            <ta e="T327" id="Seg_6785" s="T326">amno-ʔ</ta>
            <ta e="T328" id="Seg_6786" s="T327">segi</ta>
            <ta e="T329" id="Seg_6787" s="T328">bü</ta>
            <ta e="T330" id="Seg_6788" s="T329">bĭs-sittə</ta>
            <ta e="T331" id="Seg_6789" s="T330">sĭreʔpne</ta>
            <ta e="T332" id="Seg_6790" s="T331">iʔbö-laʔbə</ta>
            <ta e="T333" id="Seg_6791" s="T332">en-de</ta>
            <ta e="T334" id="Seg_6792" s="T333">segi</ta>
            <ta e="T335" id="Seg_6793" s="T334">büt-nə</ta>
            <ta e="T336" id="Seg_6794" s="T335">munu-ʔi</ta>
            <ta e="T337" id="Seg_6795" s="T336">iʔbö-laʔbə-ʔjə</ta>
            <ta e="T340" id="Seg_6796" s="T339">ipek</ta>
            <ta e="T341" id="Seg_6797" s="T340">am-a-ʔ</ta>
            <ta e="T342" id="Seg_6798" s="T341">uja</ta>
            <ta e="T343" id="Seg_6799" s="T342">am-a-ʔ</ta>
            <ta e="T344" id="Seg_6800" s="T343">kola</ta>
            <ta e="T345" id="Seg_6801" s="T344">am-a-ʔ</ta>
            <ta e="T346" id="Seg_6802" s="T345">munuj</ta>
            <ta e="T347" id="Seg_6803" s="T346">iʔbö-laʔbə</ta>
            <ta e="T348" id="Seg_6804" s="T347">am-a-ʔ</ta>
            <ta e="T349" id="Seg_6805" s="T348">kajaʔ</ta>
            <ta e="T350" id="Seg_6806" s="T349">am-a-ʔ</ta>
            <ta e="T351" id="Seg_6807" s="T350">oroma</ta>
            <ta e="T352" id="Seg_6808" s="T351">am-a-ʔ</ta>
            <ta e="T353" id="Seg_6809" s="T352">namzəga</ta>
            <ta e="T354" id="Seg_6810" s="T353">süt</ta>
            <ta e="T355" id="Seg_6811" s="T354">am-a-ʔ</ta>
            <ta e="T356" id="Seg_6812" s="T355">kola</ta>
            <ta e="T357" id="Seg_6813" s="T356">am-a-ʔ</ta>
            <ta e="T359" id="Seg_6814" s="T358">nada</ta>
            <ta e="T360" id="Seg_6815" s="T359">măna</ta>
            <ta e="T361" id="Seg_6816" s="T360">jama</ta>
            <ta e="T362" id="Seg_6817" s="T361">šöʔ-sittə</ta>
            <ta e="T363" id="Seg_6818" s="T362">parga</ta>
            <ta e="T364" id="Seg_6819" s="T363">šöʔ-sittə</ta>
            <ta e="T365" id="Seg_6820" s="T364">nada</ta>
            <ta e="T368" id="Seg_6821" s="T367">kujnek</ta>
            <ta e="T369" id="Seg_6822" s="T368">šöʔ-sittə</ta>
            <ta e="T370" id="Seg_6823" s="T369">i-zittə</ta>
            <ta e="T373" id="Seg_6824" s="T372">nada</ta>
            <ta e="T374" id="Seg_6825" s="T373">piʔmə</ta>
            <ta e="T375" id="Seg_6826" s="T374">šöʔ-sittə</ta>
            <ta e="T376" id="Seg_6827" s="T375">ato</ta>
            <ta e="T377" id="Seg_6828" s="T376">măn</ta>
            <ta e="T378" id="Seg_6829" s="T377">nʼi-m</ta>
            <ta e="T379" id="Seg_6830" s="T378">bar</ta>
            <ta e="T380" id="Seg_6831" s="T379">šer-zittə</ta>
            <ta e="T381" id="Seg_6832" s="T380">naga</ta>
            <ta e="T382" id="Seg_6833" s="T381">ĭmbi</ta>
            <ta e="T383" id="Seg_6834" s="T382">ĭmbi=də</ta>
            <ta e="T384" id="Seg_6835" s="T383">naga</ta>
            <ta e="T385" id="Seg_6836" s="T384">măn</ta>
            <ta e="T386" id="Seg_6837" s="T385">kal-la-m</ta>
            <ta e="T387" id="Seg_6838" s="T386">Kazan</ta>
            <ta e="T388" id="Seg_6839" s="T387">tura-nə</ta>
            <ta e="T389" id="Seg_6840" s="T388">i</ta>
            <ta e="T390" id="Seg_6841" s="T389">nada</ta>
            <ta e="T391" id="Seg_6842" s="T390">ipek</ta>
            <ta e="T392" id="Seg_6843" s="T391">i-zittə</ta>
            <ta e="T393" id="Seg_6844" s="T392">nada</ta>
            <ta e="T394" id="Seg_6845" s="T393">sĭreʔpne</ta>
            <ta e="T395" id="Seg_6846" s="T394">i-zittə</ta>
            <ta e="T396" id="Seg_6847" s="T395">nada</ta>
            <ta e="T399" id="Seg_6848" s="T398">multuk-də</ta>
            <ta e="T400" id="Seg_6849" s="T399">ipek</ta>
            <ta e="T401" id="Seg_6850" s="T400">i-zittə</ta>
            <ta e="T402" id="Seg_6851" s="T401">nada</ta>
            <ta e="T409" id="Seg_6852" s="T408">no</ta>
            <ta e="T410" id="Seg_6853" s="T409">ipek</ta>
            <ta e="T416" id="Seg_6854" s="T415">măn</ta>
            <ta e="T417" id="Seg_6855" s="T416">dʼije-nə</ta>
            <ta e="T418" id="Seg_6856" s="T417">dĭgəttə</ta>
            <ta e="T419" id="Seg_6857" s="T418">kal-la-m</ta>
            <ta e="T422" id="Seg_6858" s="T421">teinen</ta>
            <ta e="T425" id="Seg_6859" s="T424">dĭ-zeŋ</ta>
            <ta e="T426" id="Seg_6860" s="T425">dʼije-gəʔ</ta>
            <ta e="T427" id="Seg_6861" s="T426">šo-lə-ʔi</ta>
            <ta e="T428" id="Seg_6862" s="T427">padʼi</ta>
            <ta e="T429" id="Seg_6863" s="T428">uja</ta>
            <ta e="T430" id="Seg_6864" s="T429">det-le-ʔi</ta>
            <ta e="T431" id="Seg_6865" s="T430">kalba</ta>
            <ta e="T432" id="Seg_6866" s="T431">deʔ-le-ʔi</ta>
            <ta e="T434" id="Seg_6867" s="T433">tʼagar-zittə</ta>
            <ta e="T435" id="Seg_6868" s="T434">nada</ta>
            <ta e="T436" id="Seg_6869" s="T435">da</ta>
            <ta e="T437" id="Seg_6870" s="T436">tustʼar-zittə</ta>
            <ta e="T438" id="Seg_6871" s="T437">munuj-ʔ</ta>
            <ta e="T439" id="Seg_6872" s="T438">en-zittə</ta>
            <ta e="T440" id="Seg_6873" s="T439">oroma</ta>
            <ta e="T441" id="Seg_6874" s="T440">en-zittə</ta>
            <ta e="T442" id="Seg_6875" s="T441">i</ta>
            <ta e="T443" id="Seg_6876" s="T442">bü</ta>
            <ta e="T444" id="Seg_6877" s="T443">kămna-sʼtə</ta>
            <ta e="T445" id="Seg_6878" s="T444">i</ta>
            <ta e="T446" id="Seg_6879" s="T445">amor-zittə</ta>
            <ta e="T447" id="Seg_6880" s="T446">šamnak-ziʔ</ta>
            <ta e="T448" id="Seg_6881" s="T447">nada</ta>
            <ta e="T449" id="Seg_6882" s="T448">kan-zittə</ta>
            <ta e="T454" id="Seg_6883" s="T453">kan-zittə</ta>
            <ta e="T459" id="Seg_6884" s="T458">taler-zittə</ta>
            <ta e="T460" id="Seg_6885" s="T459">ipek</ta>
            <ta e="T461" id="Seg_6886" s="T460">kuʔ-sittə</ta>
            <ta e="T462" id="Seg_6887" s="T461">dĭgəttə</ta>
            <ta e="T463" id="Seg_6888" s="T462">özer-lə-j</ta>
            <ta e="T464" id="Seg_6889" s="T463">pădə-sʼtə</ta>
            <ta e="T465" id="Seg_6890" s="T464">nada</ta>
            <ta e="T466" id="Seg_6891" s="T465">dĭgəttə</ta>
            <ta e="T467" id="Seg_6892" s="T466">toʔ-nar-zittə</ta>
            <ta e="T468" id="Seg_6893" s="T467">nada</ta>
            <ta e="T469" id="Seg_6894" s="T468">dĭgəttə</ta>
            <ta e="T476" id="Seg_6895" s="T475">dĭgəttə</ta>
            <ta e="T477" id="Seg_6896" s="T476">nʼeʔ-sittə</ta>
            <ta e="T478" id="Seg_6897" s="T477">nada</ta>
            <ta e="T481" id="Seg_6898" s="T480">dĭgəttə</ta>
            <ta e="T482" id="Seg_6899" s="T481">nada</ta>
            <ta e="T483" id="Seg_6900" s="T482">pür-zittə</ta>
            <ta e="T484" id="Seg_6901" s="T483">dĭgəttə</ta>
            <ta e="T485" id="Seg_6902" s="T484">amor-zittə</ta>
            <ta e="T486" id="Seg_6903" s="T485">nada</ta>
            <ta e="T489" id="Seg_6904" s="T488">teinen</ta>
            <ta e="T490" id="Seg_6905" s="T489">urgo</ta>
            <ta e="T491" id="Seg_6906" s="T490">dʼala</ta>
            <ta e="T494" id="Seg_6907" s="T493">il</ta>
            <ta e="T495" id="Seg_6908" s="T494">ugaːndə</ta>
            <ta e="T496" id="Seg_6909" s="T495">iʔgö</ta>
            <ta e="T497" id="Seg_6910" s="T496">kuvas-əʔi</ta>
            <ta e="T498" id="Seg_6911" s="T497">bar</ta>
            <ta e="T499" id="Seg_6912" s="T498">ara</ta>
            <ta e="T500" id="Seg_6913" s="T499">bĭt-leʔbə-ʔjə</ta>
            <ta e="T501" id="Seg_6914" s="T500">sʼar-laʔbə-ʔjə</ta>
            <ta e="T503" id="Seg_6915" s="T502">suʔmiː-leʔbə-ʔjə</ta>
            <ta e="T504" id="Seg_6916" s="T503">nüjnə</ta>
            <ta e="T505" id="Seg_6917" s="T504">nüj-leʔbə-ʔjə</ta>
            <ta e="T506" id="Seg_6918" s="T505">jakšə</ta>
            <ta e="T507" id="Seg_6919" s="T506">nüke</ta>
            <ta e="T508" id="Seg_6920" s="T507">ipek</ta>
            <ta e="T509" id="Seg_6921" s="T508">bar</ta>
            <ta e="T512" id="Seg_6922" s="T511">pür-bi</ta>
            <ta e="T513" id="Seg_6923" s="T512">jakšə</ta>
            <ta e="T514" id="Seg_6924" s="T513">nömər</ta>
            <ta e="T515" id="Seg_6925" s="T514">tüjö</ta>
            <ta e="T520" id="Seg_6926" s="T519">amor-gaʔ</ta>
            <ta e="T521" id="Seg_6927" s="T520">padʼi</ta>
            <ta e="T522" id="Seg_6928" s="T521">püjö-lia-ʔi</ta>
            <ta e="T523" id="Seg_6929" s="T522">tüjö</ta>
            <ta e="T524" id="Seg_6930" s="T523">uʔb-la-m</ta>
            <ta e="T525" id="Seg_6931" s="T524">am-gaʔ</ta>
            <ta e="T526" id="Seg_6932" s="T525">amno</ta>
            <ta e="T527" id="Seg_6933" s="T526">amno-gaʔ</ta>
            <ta e="T528" id="Seg_6934" s="T527">nʼešpək</ta>
            <ta e="T529" id="Seg_6935" s="T528">kuza</ta>
            <ta e="T530" id="Seg_6936" s="T529">am-bi</ta>
            <ta e="T531" id="Seg_6937" s="T530">ipek</ta>
            <ta e="T532" id="Seg_6938" s="T531">lem</ta>
            <ta e="T533" id="Seg_6939" s="T532">keʔbde-ziʔ</ta>
            <ta e="T534" id="Seg_6940" s="T533">oroma</ta>
            <ta e="T535" id="Seg_6941" s="T534">nuldə-bia-m</ta>
            <ta e="T536" id="Seg_6942" s="T535">amor-gaʔ</ta>
            <ta e="T538" id="Seg_6943" s="T537">süt</ta>
            <ta e="T541" id="Seg_6944" s="T540">bĭt-tə</ta>
            <ta e="T544" id="Seg_6945" s="T543">tăn</ta>
            <ta e="T545" id="Seg_6946" s="T544">iššo</ta>
            <ta e="T546" id="Seg_6947" s="T545">kunoːl-laʔbə-l</ta>
            <ta e="T547" id="Seg_6948" s="T546">uʔbda-ʔ</ta>
            <ta e="T548" id="Seg_6949" s="T547">ato</ta>
            <ta e="T549" id="Seg_6950" s="T548">šĭšəge</ta>
            <ta e="T550" id="Seg_6951" s="T549">bü</ta>
            <ta e="T551" id="Seg_6952" s="T550">det-lə-m</ta>
            <ta e="T552" id="Seg_6953" s="T551">tănan</ta>
            <ta e="T553" id="Seg_6954" s="T552">kăm-na-m</ta>
            <ta e="T554" id="Seg_6955" s="T553">bar</ta>
            <ta e="T555" id="Seg_6956" s="T554">dĭgəttə</ta>
            <ta e="T556" id="Seg_6957" s="T555">bar</ta>
            <ta e="T557" id="Seg_6958" s="T556">süʔmə-luʔ-lə-l</ta>
            <ta e="T567" id="Seg_6959" s="T566">ej</ta>
            <ta e="T568" id="Seg_6960" s="T567">nʼilgö-t</ta>
            <ta e="T569" id="Seg_6961" s="T568">i-ʔ</ta>
            <ta e="T570" id="Seg_6962" s="T569">ja-ʔ</ta>
            <ta e="T574" id="Seg_6963" s="T573">i-ʔ</ta>
            <ta e="T575" id="Seg_6964" s="T574">dʼăbaktər-a-ʔ</ta>
            <ta e="T578" id="Seg_6965" s="T577">i-ʔ</ta>
            <ta e="T579" id="Seg_6966" s="T578">kudo-nza-ʔ</ta>
            <ta e="T583" id="Seg_6967" s="T582">i-ʔ</ta>
            <ta e="T584" id="Seg_6968" s="T583">kirgaːr-a-ʔ</ta>
            <ta e="T587" id="Seg_6969" s="T586">kuʔ-lə</ta>
            <ta e="T588" id="Seg_6970" s="T587">mʼaŋ-ŋaʔbə</ta>
            <ta e="T589" id="Seg_6971" s="T588">kĭški-t</ta>
            <ta e="T590" id="Seg_6972" s="T589">kuʔ-lə</ta>
            <ta e="T593" id="Seg_6973" s="T592">i-ʔ</ta>
            <ta e="T594" id="Seg_6974" s="T593">nuʔmə-ʔ</ta>
            <ta e="T595" id="Seg_6975" s="T594">i-ʔ</ta>
            <ta e="T596" id="Seg_6976" s="T595">suʔmi-ʔ</ta>
            <ta e="T597" id="Seg_6977" s="T596">i-ʔ</ta>
            <ta e="T598" id="Seg_6978" s="T597">nüjnə-ʔ</ta>
            <ta e="T599" id="Seg_6979" s="T598">ugaːndə</ta>
            <ta e="T600" id="Seg_6980" s="T599">jakšə</ta>
            <ta e="T601" id="Seg_6981" s="T600">ugaːndə</ta>
            <ta e="T602" id="Seg_6982" s="T601">kuvas</ta>
            <ta e="T603" id="Seg_6983" s="T602">ugaːndə</ta>
            <ta e="T604" id="Seg_6984" s="T603">komu</ta>
            <ta e="T605" id="Seg_6985" s="T604">ugaːndə</ta>
            <ta e="T607" id="Seg_6986" s="T606">kəbo</ta>
            <ta e="T608" id="Seg_6987" s="T607">ugaːndə</ta>
            <ta e="T609" id="Seg_6988" s="T608">numo</ta>
            <ta e="T613" id="Seg_6989" s="T612">ugaːndə</ta>
            <ta e="T614" id="Seg_6990" s="T613">üdʼüge</ta>
            <ta e="T619" id="Seg_6991" s="T618">padʼi</ta>
            <ta e="T620" id="Seg_6992" s="T619">plat</ta>
            <ta e="T621" id="Seg_6993" s="T620">tojir-bia-l</ta>
            <ta e="T622" id="Seg_6994" s="T621">našto</ta>
            <ta e="T623" id="Seg_6995" s="T622">deʔ-pie-l</ta>
            <ta e="T624" id="Seg_6996" s="T623">măn</ta>
            <ta e="T625" id="Seg_6997" s="T624">ej</ta>
            <ta e="T626" id="Seg_6998" s="T625">i-li-m</ta>
            <ta e="T627" id="Seg_6999" s="T626">nʼe</ta>
            <ta e="T628" id="Seg_7000" s="T627">nada</ta>
            <ta e="T629" id="Seg_7001" s="T628">tojir-zittə</ta>
            <ta e="T630" id="Seg_7002" s="T629">ato</ta>
            <ta e="T631" id="Seg_7003" s="T630">ej</ta>
            <ta e="T632" id="Seg_7004" s="T631">jakšə</ta>
            <ta e="T633" id="Seg_7005" s="T632">mo-lə-j</ta>
            <ta e="T634" id="Seg_7006" s="T633">uda-m</ta>
            <ta e="T635" id="Seg_7007" s="T634">băʔ-pia-m</ta>
            <ta e="T636" id="Seg_7008" s="T635">bar</ta>
            <ta e="T637" id="Seg_7009" s="T636">kem</ta>
            <ta e="T638" id="Seg_7010" s="T637">mʼaŋ-ŋaʔbə</ta>
            <ta e="T639" id="Seg_7011" s="T638">de-ʔ</ta>
            <ta e="T640" id="Seg_7012" s="T639">ĭmbi=nʼibudʼ</ta>
            <ta e="T641" id="Seg_7013" s="T640">uda-m</ta>
            <ta e="T642" id="Seg_7014" s="T641">sar-zittə</ta>
            <ta e="T647" id="Seg_7015" s="T645">našto</ta>
            <ta e="T648" id="Seg_7016" s="T647">tagaj-ziʔ</ta>
            <ta e="T649" id="Seg_7017" s="T648">tagaj</ta>
            <ta e="T650" id="Seg_7018" s="T649">i-bie-l</ta>
            <ta e="T651" id="Seg_7019" s="T650">tănan</ta>
            <ta e="T652" id="Seg_7020" s="T651">iššo</ta>
            <ta e="T654" id="Seg_7021" s="T652">nada</ta>
            <ta e="T655" id="Seg_7022" s="T654">beržə</ta>
            <ta e="T656" id="Seg_7023" s="T655">surno</ta>
            <ta e="T657" id="Seg_7024" s="T656">bar</ta>
            <ta e="T658" id="Seg_7025" s="T657">šonə-ga</ta>
            <ta e="T659" id="Seg_7026" s="T658">sĭre</ta>
            <ta e="T660" id="Seg_7027" s="T659">bar</ta>
            <ta e="T661" id="Seg_7028" s="T660">šonə-ga</ta>
            <ta e="T662" id="Seg_7029" s="T661">beržə-ziʔ</ta>
            <ta e="T663" id="Seg_7030" s="T662">ugaːndə</ta>
            <ta e="T664" id="Seg_7031" s="T663">šĭšəge</ta>
            <ta e="T665" id="Seg_7032" s="T664">măn</ta>
            <ta e="T666" id="Seg_7033" s="T665">ugaːndə</ta>
            <ta e="T667" id="Seg_7034" s="T666">kăm-bia-m</ta>
            <ta e="T668" id="Seg_7035" s="T667">da</ta>
            <ta e="T669" id="Seg_7036" s="T668">tăŋ</ta>
            <ta e="T670" id="Seg_7037" s="T669">kăm-bia-m</ta>
            <ta e="T671" id="Seg_7038" s="T670">uju-m</ta>
            <ta e="T672" id="Seg_7039" s="T671">kăm-bi</ta>
            <ta e="T673" id="Seg_7040" s="T672">uda-m</ta>
            <ta e="T674" id="Seg_7041" s="T673">kăm-bi</ta>
            <ta e="T675" id="Seg_7042" s="T674">kadəl-bə</ta>
            <ta e="T676" id="Seg_7043" s="T675">bar</ta>
            <ta e="T677" id="Seg_7044" s="T676">kăm-bi</ta>
            <ta e="T678" id="Seg_7045" s="T677">teinen</ta>
            <ta e="T681" id="Seg_7046" s="T680">kuvas</ta>
            <ta e="T682" id="Seg_7047" s="T681">dʼala</ta>
            <ta e="T683" id="Seg_7048" s="T682">kuja</ta>
            <ta e="T684" id="Seg_7049" s="T683">bar</ta>
            <ta e="T685" id="Seg_7050" s="T684">măndo-laʔbǝ</ta>
            <ta e="T686" id="Seg_7051" s="T685">nʼuʔnun</ta>
            <ta e="T687" id="Seg_7052" s="T686">ĭmbi=də</ta>
            <ta e="T688" id="Seg_7053" s="T687">naga</ta>
            <ta e="T689" id="Seg_7054" s="T688">ugaːndə</ta>
            <ta e="T690" id="Seg_7055" s="T689">jakšə</ta>
            <ta e="T691" id="Seg_7056" s="T690">dʼala</ta>
            <ta e="T692" id="Seg_7057" s="T691">il</ta>
            <ta e="T693" id="Seg_7058" s="T692">bar</ta>
            <ta e="T694" id="Seg_7059" s="T693">togonor-laʔbə-ʔjə</ta>
            <ta e="T695" id="Seg_7060" s="T694">toltano</ta>
            <ta e="T696" id="Seg_7061" s="T695">am-laʔbə-ʔjə</ta>
            <ta e="T697" id="Seg_7062" s="T696">ine-ʔi</ta>
            <ta e="T698" id="Seg_7063" s="T697">tarir-laʔbə-ʔjə</ta>
            <ta e="T699" id="Seg_7064" s="T698">bar</ta>
            <ta e="T700" id="Seg_7065" s="T699">nada</ta>
            <ta e="T701" id="Seg_7066" s="T700">kan-zittə</ta>
            <ta e="T702" id="Seg_7067" s="T701">noʔ</ta>
            <ta e="T703" id="Seg_7068" s="T702">jaʔ-sittə</ta>
            <ta e="T704" id="Seg_7069" s="T703">šapku</ta>
            <ta e="T705" id="Seg_7070" s="T704">nada</ta>
            <ta e="T706" id="Seg_7071" s="T705">i-zittə</ta>
            <ta e="T707" id="Seg_7072" s="T706">nada</ta>
            <ta e="T708" id="Seg_7073" s="T707">bü</ta>
            <ta e="T709" id="Seg_7074" s="T708">i-zittə</ta>
            <ta e="T710" id="Seg_7075" s="T709">süt</ta>
            <ta e="T711" id="Seg_7076" s="T710">i-zittə</ta>
            <ta e="T712" id="Seg_7077" s="T711">nada</ta>
            <ta e="T713" id="Seg_7078" s="T712">ipek</ta>
            <ta e="T714" id="Seg_7079" s="T713">i-zittə</ta>
            <ta e="T715" id="Seg_7080" s="T714">tus</ta>
            <ta e="T716" id="Seg_7081" s="T715">i-zittə</ta>
            <ta e="T717" id="Seg_7082" s="T716">munuj-ʔ</ta>
            <ta e="T718" id="Seg_7083" s="T717">i-zittə</ta>
            <ta e="T719" id="Seg_7084" s="T718">pi</ta>
            <ta e="T720" id="Seg_7085" s="T719">nada</ta>
            <ta e="T721" id="Seg_7086" s="T720">i-zittə</ta>
            <ta e="T722" id="Seg_7087" s="T721">šapku</ta>
            <ta e="T723" id="Seg_7088" s="T722">albuga</ta>
            <ta e="T724" id="Seg_7089" s="T723">teinen</ta>
            <ta e="T725" id="Seg_7090" s="T724">kuʔ-pie-m</ta>
            <ta e="T726" id="Seg_7091" s="T725">tažəp</ta>
            <ta e="T727" id="Seg_7092" s="T726">kuʔ-pie-m</ta>
            <ta e="T728" id="Seg_7093" s="T727">urgaːba</ta>
            <ta e="T729" id="Seg_7094" s="T728">kuʔ-pie-m</ta>
            <ta e="T730" id="Seg_7095" s="T729">söːn</ta>
            <ta e="T731" id="Seg_7096" s="T730">kuʔ-pie-m</ta>
            <ta e="T732" id="Seg_7097" s="T731">askər</ta>
            <ta e="T733" id="Seg_7098" s="T732">kuʔ-pie-m</ta>
            <ta e="T734" id="Seg_7099" s="T733">poʔto</ta>
            <ta e="T735" id="Seg_7100" s="T734">kuʔ-pie-m</ta>
            <ta e="T736" id="Seg_7101" s="T735">multuk-siʔ</ta>
            <ta e="T737" id="Seg_7102" s="T736">tüžöj-ʔi</ta>
            <ta e="T738" id="Seg_7103" s="T737">bar</ta>
            <ta e="T739" id="Seg_7104" s="T738">noʔ</ta>
            <ta e="T740" id="Seg_7105" s="T739">am-nia-ʔi</ta>
            <ta e="T741" id="Seg_7106" s="T740">mĭl-leʔbə-ʔjə</ta>
            <ta e="T742" id="Seg_7107" s="T741">dĭn</ta>
            <ta e="T743" id="Seg_7108" s="T742">dĭ</ta>
            <ta e="T744" id="Seg_7109" s="T743">kuza</ta>
            <ta e="T745" id="Seg_7110" s="T744">det-lə-j</ta>
            <ta e="T747" id="Seg_7111" s="T746">maʔ-ndə</ta>
            <ta e="T749" id="Seg_7112" s="T748">nada</ta>
            <ta e="T750" id="Seg_7113" s="T749">ešši-m</ta>
            <ta e="T751" id="Seg_7114" s="T750">bazə-sʼtə</ta>
            <ta e="T752" id="Seg_7115" s="T751">nada</ta>
            <ta e="T755" id="Seg_7116" s="T754">ešši-m</ta>
            <ta e="T756" id="Seg_7117" s="T755">bar</ta>
            <ta e="T757" id="Seg_7118" s="T756">dʼor-laʔbə</ta>
            <ta e="T758" id="Seg_7119" s="T757">nada</ta>
            <ta e="T759" id="Seg_7120" s="T758">dĭ-m</ta>
            <ta e="T760" id="Seg_7121" s="T759">köːdər-zittə</ta>
            <ta e="T761" id="Seg_7122" s="T760">nada</ta>
            <ta e="T762" id="Seg_7123" s="T761">amor-zittə</ta>
            <ta e="T763" id="Seg_7124" s="T762">mĭ-zittə</ta>
            <ta e="T764" id="Seg_7125" s="T763">nada</ta>
            <ta e="T765" id="Seg_7126" s="T764">kunoːl-zittə</ta>
            <ta e="T766" id="Seg_7127" s="T765">en-zittə</ta>
            <ta e="T771" id="Seg_7128" s="T770">oːrdə-ʔ</ta>
            <ta e="T772" id="Seg_7129" s="T771">ešši-m</ta>
            <ta e="T773" id="Seg_7130" s="T772">bar</ta>
            <ta e="T774" id="Seg_7131" s="T773">pušaj</ta>
            <ta e="T775" id="Seg_7132" s="T774">kunoːl-laʔbə</ta>
            <ta e="T776" id="Seg_7133" s="T775">amno-ʔ</ta>
            <ta e="T777" id="Seg_7134" s="T776">tüjö</ta>
            <ta e="T778" id="Seg_7135" s="T777">bü-j-leʔ</ta>
            <ta e="T779" id="Seg_7136" s="T778">kal-la-m</ta>
            <ta e="T780" id="Seg_7137" s="T779">bü</ta>
            <ta e="T781" id="Seg_7138" s="T780">det-le-m</ta>
            <ta e="T782" id="Seg_7139" s="T781">dĭgəttə</ta>
            <ta e="T783" id="Seg_7140" s="T782">bĭt-le-l</ta>
            <ta e="T784" id="Seg_7141" s="T783">i-ʔ</ta>
            <ta e="T785" id="Seg_7142" s="T784">kan-a-ʔ</ta>
            <ta e="T786" id="Seg_7143" s="T785">a</ta>
            <ta e="T787" id="Seg_7144" s="T786">dĭgəttə</ta>
            <ta e="T788" id="Seg_7145" s="T787">kal-la-l</ta>
            <ta e="T789" id="Seg_7146" s="T788">maʔ-nə-l</ta>
            <ta e="T790" id="Seg_7147" s="T789">tüjö</ta>
            <ta e="T791" id="Seg_7148" s="T790">kem</ta>
            <ta e="T792" id="Seg_7149" s="T791">pür-le-m</ta>
            <ta e="T793" id="Seg_7150" s="T792">nada</ta>
            <ta e="T794" id="Seg_7151" s="T793">dibər</ta>
            <ta e="T795" id="Seg_7152" s="T794">süt</ta>
            <ta e="T796" id="Seg_7153" s="T795">kămna-sʼtə</ta>
            <ta e="T797" id="Seg_7154" s="T796">kajak</ta>
            <ta e="T798" id="Seg_7155" s="T797">en-zittə</ta>
            <ta e="T799" id="Seg_7156" s="T798">alʼi</ta>
            <ta e="T800" id="Seg_7157" s="T799">sil</ta>
            <ta e="T801" id="Seg_7158" s="T800">en-zittə</ta>
            <ta e="T802" id="Seg_7159" s="T801">tüjö</ta>
            <ta e="T803" id="Seg_7160" s="T802">amor-lə-bəj</ta>
            <ta e="T804" id="Seg_7161" s="T803">i-ʔ</ta>
            <ta e="T805" id="Seg_7162" s="T804">baza</ta>
            <ta e="T806" id="Seg_7163" s="T805">ato</ta>
            <ta e="T807" id="Seg_7164" s="T806">dʼibige</ta>
            <ta e="T810" id="Seg_7165" s="T809">ipek-siʔ</ta>
            <ta e="T811" id="Seg_7166" s="T810">am-a-ʔ</ta>
            <ta e="T812" id="Seg_7167" s="T811">Koʔbdo</ta>
            <ta e="T814" id="Seg_7168" s="T812">šo-bi</ta>
            <ta e="T815" id="Seg_7169" s="T814">eʔbdə</ta>
            <ta e="T816" id="Seg_7170" s="T815">sĭre</ta>
            <ta e="T817" id="Seg_7171" s="T816">bos-tə</ta>
            <ta e="T818" id="Seg_7172" s="T817">bar</ta>
            <ta e="T819" id="Seg_7173" s="T818">todam</ta>
            <ta e="T820" id="Seg_7174" s="T819">i</ta>
            <ta e="T821" id="Seg_7175" s="T820">dĭ</ta>
            <ta e="T822" id="Seg_7176" s="T821">kuza</ta>
            <ta e="T823" id="Seg_7177" s="T822">šo-bi</ta>
            <ta e="T824" id="Seg_7178" s="T823">nʼešpək</ta>
            <ta e="T825" id="Seg_7179" s="T824">ugaːndə</ta>
            <ta e="T826" id="Seg_7180" s="T825">i</ta>
            <ta e="T827" id="Seg_7181" s="T826">mĭ-ʔ</ta>
            <ta e="T828" id="Seg_7182" s="T827">ele-t</ta>
            <ta e="T829" id="Seg_7183" s="T828">šo-bi</ta>
            <ta e="T831" id="Seg_7184" s="T830">dĭ-zeŋ</ta>
            <ta e="T832" id="Seg_7185" s="T831">toltano</ta>
            <ta e="T833" id="Seg_7186" s="T832">ej</ta>
            <ta e="T834" id="Seg_7187" s="T833">amnə-bi-ʔi</ta>
            <ta e="T835" id="Seg_7188" s="T834">măn</ta>
            <ta e="T836" id="Seg_7189" s="T835">aba-m</ta>
            <ta e="T837" id="Seg_7190" s="T836">dĭ-zeŋ-də</ta>
            <ta e="T838" id="Seg_7191" s="T837">amnəl-bi</ta>
            <ta e="T839" id="Seg_7192" s="T838">dĭ-zeŋ</ta>
            <ta e="T840" id="Seg_7193" s="T839">uja</ta>
            <ta e="T841" id="Seg_7194" s="T840">iʔgö</ta>
            <ta e="T842" id="Seg_7195" s="T841">i-bi</ta>
            <ta e="T843" id="Seg_7196" s="T842">bar</ta>
            <ta e="T844" id="Seg_7197" s="T843">uja</ta>
            <ta e="T845" id="Seg_7198" s="T844">am-bi-ʔi</ta>
            <ta e="T846" id="Seg_7199" s="T845">ipek</ta>
            <ta e="T847" id="Seg_7200" s="T846">amga</ta>
            <ta e="T848" id="Seg_7201" s="T847">i-bi</ta>
            <ta e="T849" id="Seg_7202" s="T848">a</ta>
            <ta e="T850" id="Seg_7203" s="T849">daška</ta>
            <ta e="T851" id="Seg_7204" s="T850">ĭmbi=də</ta>
            <ta e="T854" id="Seg_7205" s="T853">naga</ta>
            <ta e="T855" id="Seg_7206" s="T854">ĭmbi</ta>
            <ta e="T856" id="Seg_7207" s="T855">amno-laʔbə-l</ta>
            <ta e="T857" id="Seg_7208" s="T856">ej</ta>
            <ta e="T858" id="Seg_7209" s="T857">dʼăbaktər-ia-l</ta>
            <ta e="T859" id="Seg_7210" s="T858">dʼăbaktər-a-ʔ</ta>
            <ta e="T860" id="Seg_7211" s="T859">ĭmbi=nʼibudʼ</ta>
            <ta e="T861" id="Seg_7212" s="T860">sĭj-bə</ta>
            <ta e="T862" id="Seg_7213" s="T861">verna</ta>
            <ta e="T865" id="Seg_7214" s="T864">sĭj-lə</ta>
            <ta e="T866" id="Seg_7215" s="T865">verna</ta>
            <ta e="T867" id="Seg_7216" s="T866">ĭzem-nie</ta>
            <ta e="T868" id="Seg_7217" s="T867">ĭmbi=də</ta>
            <ta e="T869" id="Seg_7218" s="T868">ej</ta>
            <ta e="T870" id="Seg_7219" s="T869">dʼăbaktər-ia-l</ta>
            <ta e="T871" id="Seg_7220" s="T870">dʼăbaktər-a-ʔ</ta>
            <ta e="T875" id="Seg_7221" s="T874">gibər</ta>
            <ta e="T876" id="Seg_7222" s="T875">kandə-ga-l</ta>
            <ta e="T877" id="Seg_7223" s="T876">gijen</ta>
            <ta e="T878" id="Seg_7224" s="T877">i-bie-l</ta>
            <ta e="T879" id="Seg_7225" s="T878">šo-ʔ</ta>
            <ta e="T880" id="Seg_7226" s="T879">döːbər</ta>
            <ta e="T881" id="Seg_7227" s="T880">ĭmbi=nʼibudʼ</ta>
            <ta e="T882" id="Seg_7228" s="T881">nörbə-ʔ</ta>
            <ta e="T883" id="Seg_7229" s="T882">măna</ta>
            <ta e="T886" id="Seg_7230" s="T885">büzʼe</ta>
            <ta e="T887" id="Seg_7231" s="T886">nüke-t-siʔ</ta>
            <ta e="T888" id="Seg_7232" s="T887">amno-laʔpi-ʔi</ta>
            <ta e="T889" id="Seg_7233" s="T888">dĭ-zeŋ</ta>
            <ta e="T890" id="Seg_7234" s="T889">es-seŋ-də</ta>
            <ta e="T891" id="Seg_7235" s="T890">i-bi-ʔi</ta>
            <ta e="T892" id="Seg_7236" s="T891">koʔbdo</ta>
            <ta e="T893" id="Seg_7237" s="T892">da</ta>
            <ta e="T894" id="Seg_7238" s="T893">nʼi</ta>
            <ta e="T895" id="Seg_7239" s="T894">tura</ta>
            <ta e="T896" id="Seg_7240" s="T895">i-bi</ta>
            <ta e="T897" id="Seg_7241" s="T896">tüžöj</ta>
            <ta e="T898" id="Seg_7242" s="T897">i-bi</ta>
            <ta e="T899" id="Seg_7243" s="T898">dĭ</ta>
            <ta e="T900" id="Seg_7244" s="T899">nüke</ta>
            <ta e="T901" id="Seg_7245" s="T900">tüžöj</ta>
            <ta e="T902" id="Seg_7246" s="T901">surdo-bi</ta>
            <ta e="T903" id="Seg_7247" s="T902">es-seŋ-də</ta>
            <ta e="T904" id="Seg_7248" s="T903">mĭ-bi</ta>
            <ta e="T905" id="Seg_7249" s="T904">ipek</ta>
            <ta e="T906" id="Seg_7250" s="T905">mĭ-bi</ta>
            <ta e="T907" id="Seg_7251" s="T906">dĭ-zeŋ</ta>
            <ta e="T908" id="Seg_7252" s="T907">am-nia-ʔi</ta>
            <ta e="T910" id="Seg_7253" s="T909">dĭ</ta>
            <ta e="T911" id="Seg_7254" s="T910">šide-göʔ</ta>
            <ta e="T912" id="Seg_7255" s="T911">kuza</ta>
            <ta e="T913" id="Seg_7256" s="T912">šo-bi-ʔi</ta>
            <ta e="T914" id="Seg_7257" s="T913">tibi</ta>
            <ta e="T915" id="Seg_7258" s="T914">da</ta>
            <ta e="T916" id="Seg_7259" s="T915">koʔbdo</ta>
            <ta e="T917" id="Seg_7260" s="T916">tănan</ta>
            <ta e="T918" id="Seg_7261" s="T917">padʼi</ta>
            <ta e="T919" id="Seg_7262" s="T918">ĭmbi=nʼibudʼ</ta>
            <ta e="T920" id="Seg_7263" s="T919">deʔ-pi-ʔi</ta>
            <ta e="T921" id="Seg_7264" s="T920">sĭreʔpne</ta>
            <ta e="T922" id="Seg_7265" s="T921">deʔ-pi-ʔi</ta>
            <ta e="T925" id="Seg_7266" s="T924">segi</ta>
            <ta e="T926" id="Seg_7267" s="T925">bü</ta>
            <ta e="T927" id="Seg_7268" s="T926">bĭt-le-l</ta>
            <ta e="T928" id="Seg_7269" s="T927">padʼi</ta>
            <ta e="T929" id="Seg_7270" s="T928">kanfeti</ta>
            <ta e="T930" id="Seg_7271" s="T929">deʔ-pi-ʔi</ta>
            <ta e="T931" id="Seg_7272" s="T930">tănan</ta>
            <ta e="T932" id="Seg_7273" s="T931">kujnek</ta>
            <ta e="T933" id="Seg_7274" s="T932">padʼi</ta>
            <ta e="T934" id="Seg_7275" s="T933">deʔ-pi-ʔi</ta>
            <ta e="T935" id="Seg_7276" s="T934">piʔmə</ta>
            <ta e="T936" id="Seg_7277" s="T935">padʼi</ta>
            <ta e="T937" id="Seg_7278" s="T936">deʔ-pi-ʔi</ta>
            <ta e="T938" id="Seg_7279" s="T937">tănan</ta>
            <ta e="T939" id="Seg_7280" s="T938">predsʼedatʼelʼ-ən</ta>
            <ta e="T940" id="Seg_7281" s="T939">tura-t</ta>
            <ta e="T941" id="Seg_7282" s="T940">urgo</ta>
            <ta e="T942" id="Seg_7283" s="T941">il</ta>
            <ta e="T943" id="Seg_7284" s="T942">iʔgö</ta>
            <ta e="T946" id="Seg_7285" s="T945">šide</ta>
            <ta e="T947" id="Seg_7286" s="T946">nʼi</ta>
            <ta e="T948" id="Seg_7287" s="T947">šide</ta>
            <ta e="T949" id="Seg_7288" s="T948">koʔbdo</ta>
            <ta e="T950" id="Seg_7289" s="T949">nüke</ta>
            <ta e="T951" id="Seg_7290" s="T950">büzʼe-ʔi-ziʔ</ta>
            <ta e="T952" id="Seg_7291" s="T951">bos-tə</ta>
            <ta e="T953" id="Seg_7292" s="T952">ne</ta>
            <ta e="T955" id="Seg_7293" s="T954">men-zeŋ-də</ta>
            <ta e="T956" id="Seg_7294" s="T955">šide</ta>
            <ta e="T957" id="Seg_7295" s="T956">onʼiʔ</ta>
            <ta e="T958" id="Seg_7296" s="T957">jakšə</ta>
            <ta e="T959" id="Seg_7297" s="T958">men</ta>
            <ta e="T960" id="Seg_7298" s="T959">onʼiʔ</ta>
            <ta e="T961" id="Seg_7299" s="T960">nagur</ta>
            <ta e="T962" id="Seg_7300" s="T961">uju</ta>
            <ta e="T963" id="Seg_7301" s="T962">a</ta>
            <ta e="T964" id="Seg_7302" s="T963">teʔtə</ta>
            <ta e="T965" id="Seg_7303" s="T964">uju</ta>
            <ta e="T966" id="Seg_7304" s="T965">naga</ta>
            <ta e="T970" id="Seg_7305" s="T969">tăn</ta>
            <ta e="T971" id="Seg_7306" s="T970">măna</ta>
            <ta e="T972" id="Seg_7307" s="T971">münör-ə-ʔ</ta>
            <ta e="T973" id="Seg_7308" s="T972">a</ta>
            <ta e="T974" id="Seg_7309" s="T973">ĭmbi-ziʔ</ta>
            <ta e="T975" id="Seg_7310" s="T974">münör-laʔbə-l</ta>
            <ta e="T976" id="Seg_7311" s="T975">muzuruk-siʔ</ta>
            <ta e="T977" id="Seg_7312" s="T976">i-ʔ</ta>
            <ta e="T978" id="Seg_7313" s="T977">münör-ə-ʔ</ta>
            <ta e="T979" id="Seg_7314" s="T978">ato</ta>
            <ta e="T980" id="Seg_7315" s="T979">măna</ta>
            <ta e="T981" id="Seg_7316" s="T980">ĭzem-nə-j</ta>
            <ta e="T982" id="Seg_7317" s="T981">bar</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PKZ">
            <ta e="T15" id="Seg_7318" s="T14">dʼije-Tə</ta>
            <ta e="T16" id="Seg_7319" s="T15">teinen</ta>
            <ta e="T17" id="Seg_7320" s="T16">kan-lV-m</ta>
            <ta e="T20" id="Seg_7321" s="T19">ine-ziʔ</ta>
            <ta e="T21" id="Seg_7322" s="T20">gijen</ta>
            <ta e="T22" id="Seg_7323" s="T21">băra</ta>
            <ta e="T23" id="Seg_7324" s="T22">nadə</ta>
            <ta e="T24" id="Seg_7325" s="T23">ipek</ta>
            <ta e="T25" id="Seg_7326" s="T24">hen-zittə</ta>
            <ta e="T26" id="Seg_7327" s="T25">tus</ta>
            <ta e="T27" id="Seg_7328" s="T26">hen-zittə</ta>
            <ta e="T28" id="Seg_7329" s="T27">šamnak</ta>
            <ta e="T29" id="Seg_7330" s="T28">nadə</ta>
            <ta e="T30" id="Seg_7331" s="T29">hen-zittə</ta>
            <ta e="T31" id="Seg_7332" s="T30">nadə</ta>
            <ta e="T32" id="Seg_7333" s="T31">uja</ta>
            <ta e="T33" id="Seg_7334" s="T32">hen-zittə</ta>
            <ta e="T34" id="Seg_7335" s="T33">nadə</ta>
            <ta e="T35" id="Seg_7336" s="T34">ipek</ta>
            <ta e="T36" id="Seg_7337" s="T35">hen-zittə</ta>
            <ta e="T49" id="Seg_7338" s="T46">ipek</ta>
            <ta e="T50" id="Seg_7339" s="T49">hen-zittə</ta>
            <ta e="T51" id="Seg_7340" s="T50">nadə</ta>
            <ta e="T52" id="Seg_7341" s="T51">kajaʔ</ta>
            <ta e="T53" id="Seg_7342" s="T52">hen-zittə</ta>
            <ta e="T54" id="Seg_7343" s="T53">nadə</ta>
            <ta e="T55" id="Seg_7344" s="T54">munəj-jəʔ</ta>
            <ta e="T56" id="Seg_7345" s="T55">hen-zittə</ta>
            <ta e="T57" id="Seg_7346" s="T56">šamnak</ta>
            <ta e="T58" id="Seg_7347" s="T57">nadə</ta>
            <ta e="T59" id="Seg_7348" s="T58">hen-zittə</ta>
            <ta e="T60" id="Seg_7349" s="T59">nadə</ta>
            <ta e="T61" id="Seg_7350" s="T60">aspaʔ</ta>
            <ta e="T62" id="Seg_7351" s="T61">hen-zittə</ta>
            <ta e="T63" id="Seg_7352" s="T62">men</ta>
            <ta e="T64" id="Seg_7353" s="T63">i-zittə</ta>
            <ta e="T65" id="Seg_7354" s="T64">nadə</ta>
            <ta e="T66" id="Seg_7355" s="T65">multuk</ta>
            <ta e="T67" id="Seg_7356" s="T66">i-zittə</ta>
            <ta e="T68" id="Seg_7357" s="T67">nadə</ta>
            <ta e="T69" id="Seg_7358" s="T68">nadə</ta>
            <ta e="T70" id="Seg_7359" s="T69">baltu</ta>
            <ta e="T71" id="Seg_7360" s="T70">i-zittə</ta>
            <ta e="T72" id="Seg_7361" s="T71">tagaj</ta>
            <ta e="T73" id="Seg_7362" s="T72">i-zittə</ta>
            <ta e="T74" id="Seg_7363" s="T73">nadə</ta>
            <ta e="T86" id="Seg_7364" s="T85">kukganʼanʼi</ta>
            <ta e="T91" id="Seg_7365" s="T90">nu</ta>
            <ta e="T92" id="Seg_7366" s="T91">kuza-so-gəj</ta>
            <ta e="T93" id="Seg_7367" s="T92">šonə-gA-jəʔ</ta>
            <ta e="T94" id="Seg_7368" s="T93">tʼăbaktər-zittə</ta>
            <ta e="T95" id="Seg_7369" s="T94">amnə-ʔ</ta>
            <ta e="T96" id="Seg_7370" s="T95">tʼăbaktər-lV-bAʔ</ta>
            <ta e="T97" id="Seg_7371" s="T96">amor-zittə</ta>
            <ta e="T98" id="Seg_7372" s="T97">padʼi</ta>
            <ta e="T99" id="Seg_7373" s="T98">axota</ta>
            <ta e="T100" id="Seg_7374" s="T99">amnə-ʔ</ta>
            <ta e="T101" id="Seg_7375" s="T100">amor-ə-ʔ</ta>
            <ta e="T102" id="Seg_7376" s="T101">dĭn</ta>
            <ta e="T103" id="Seg_7377" s="T102">ipek</ta>
            <ta e="T104" id="Seg_7378" s="T103">iʔbö-laʔbə</ta>
            <ta e="T105" id="Seg_7379" s="T104">kajaʔ</ta>
            <ta e="T106" id="Seg_7380" s="T105">nu-gA</ta>
            <ta e="T107" id="Seg_7381" s="T106">šamnak</ta>
            <ta e="T108" id="Seg_7382" s="T107">i-gA</ta>
            <ta e="T109" id="Seg_7383" s="T108">tus</ta>
            <ta e="T110" id="Seg_7384" s="T109">i-gA</ta>
            <ta e="T111" id="Seg_7385" s="T110">munəj-jəʔ</ta>
            <ta e="T112" id="Seg_7386" s="T111">uja</ta>
            <ta e="T113" id="Seg_7387" s="T112">kola</ta>
            <ta e="T114" id="Seg_7388" s="T113">am-ə-ʔ</ta>
            <ta e="T115" id="Seg_7389" s="T114">bar</ta>
            <ta e="T116" id="Seg_7390" s="T115">e-ʔ</ta>
            <ta e="T117" id="Seg_7391" s="T116">aloma-ʔ</ta>
            <ta e="T118" id="Seg_7392" s="T117">e-ʔ</ta>
            <ta e="T119" id="Seg_7393" s="T118">kirgaːr-ə-ʔ</ta>
            <ta e="T120" id="Seg_7394" s="T119">ato</ta>
            <ta e="T121" id="Seg_7395" s="T120">münör-lV-m</ta>
            <ta e="T122" id="Seg_7396" s="T121">tănan</ta>
            <ta e="T123" id="Seg_7397" s="T122">e-ʔ</ta>
            <ta e="T124" id="Seg_7398" s="T123">tʼor-ə-ʔ</ta>
            <ta e="T125" id="Seg_7399" s="T124">e-ʔ</ta>
            <ta e="T126" id="Seg_7400" s="T125">kuroː-m-ə-ʔ</ta>
            <ta e="T127" id="Seg_7401" s="T126">e-ʔ</ta>
            <ta e="T128" id="Seg_7402" s="T127">šʼaːm-ə-ʔ</ta>
            <ta e="T129" id="Seg_7403" s="T128">amno-laʔbə-l</ta>
            <ta e="T130" id="Seg_7404" s="T129">bar</ta>
            <ta e="T131" id="Seg_7405" s="T130">šʼaːm-laʔbə-l</ta>
            <ta e="T132" id="Seg_7406" s="T131">tăn</ta>
            <ta e="T133" id="Seg_7407" s="T132">ugaːndə</ta>
            <ta e="T134" id="Seg_7408" s="T133">iʔgö</ta>
            <ta e="T135" id="Seg_7409" s="T134">šʼaːm-laʔbə-l</ta>
            <ta e="T138" id="Seg_7410" s="T137">măn</ta>
            <ta e="T139" id="Seg_7411" s="T138">tüjö</ta>
            <ta e="T140" id="Seg_7412" s="T139">kan-lV-m</ta>
            <ta e="T141" id="Seg_7413" s="T140">šumura-Tə</ta>
            <ta e="T142" id="Seg_7414" s="T141">a</ta>
            <ta e="T143" id="Seg_7415" s="T142">tăn</ta>
            <ta e="T144" id="Seg_7416" s="T143">maʔ-Tə-l</ta>
            <ta e="T145" id="Seg_7417" s="T144">amno-ʔ</ta>
            <ta e="T146" id="Seg_7418" s="T145">e-ʔ</ta>
            <ta e="T147" id="Seg_7419" s="T146">aloma-ʔ</ta>
            <ta e="T148" id="Seg_7420" s="T147">măn</ta>
            <ta e="T149" id="Seg_7421" s="T148">ĭmbi=nʼibudʼ</ta>
            <ta e="T150" id="Seg_7422" s="T149">tănan</ta>
            <ta e="T151" id="Seg_7423" s="T150">det-lV-m</ta>
            <ta e="T152" id="Seg_7424" s="T151">dĭgəttə</ta>
            <ta e="T153" id="Seg_7425" s="T152">mĭ-lV-m</ta>
            <ta e="T154" id="Seg_7426" s="T153">tănan</ta>
            <ta e="T155" id="Seg_7427" s="T154">taldʼen</ta>
            <ta e="T158" id="Seg_7428" s="T157">taldʼen</ta>
            <ta e="T159" id="Seg_7429" s="T158">šo-bi</ta>
            <ta e="T160" id="Seg_7430" s="T159">măn</ta>
            <ta e="T161" id="Seg_7431" s="T160">tugan-m</ta>
            <ta e="T162" id="Seg_7432" s="T161">măn</ta>
            <ta e="T163" id="Seg_7433" s="T162">măn-bi-m</ta>
            <ta e="T164" id="Seg_7434" s="T163">iʔbö-ʔ</ta>
            <ta e="T165" id="Seg_7435" s="T164">kunol-zittə</ta>
            <ta e="T166" id="Seg_7436" s="T165">dĭgəttə</ta>
            <ta e="T167" id="Seg_7437" s="T166">măn-bi-m</ta>
            <ta e="T168" id="Seg_7438" s="T167">amor-ə-ʔ</ta>
            <ta e="T169" id="Seg_7439" s="T168">padʼi</ta>
            <ta e="T170" id="Seg_7440" s="T169">amor-zittə</ta>
            <ta e="T171" id="Seg_7441" s="T170">axota</ta>
            <ta e="T172" id="Seg_7442" s="T171">dĭ</ta>
            <ta e="T173" id="Seg_7443" s="T172">măn-bi</ta>
            <ta e="T174" id="Seg_7444" s="T173">măn</ta>
            <ta e="T175" id="Seg_7445" s="T174">măn</ta>
            <ta e="T176" id="Seg_7446" s="T175">ej</ta>
            <ta e="T179" id="Seg_7447" s="T178">amor-zittə</ta>
            <ta e="T181" id="Seg_7448" s="T180">măn</ta>
            <ta e="T182" id="Seg_7449" s="T181">tura-Kən</ta>
            <ta e="T183" id="Seg_7450" s="T182">am-bi-m</ta>
            <ta e="T184" id="Seg_7451" s="T183">dĭn</ta>
            <ta e="T185" id="Seg_7452" s="T184">dĭgəttə</ta>
            <ta e="T188" id="Seg_7453" s="T187">iʔbö-bi</ta>
            <ta e="T189" id="Seg_7454" s="T188">kunol-zittə</ta>
            <ta e="T190" id="Seg_7455" s="T189">măn</ta>
            <ta e="T191" id="Seg_7456" s="T190">iʔbö-bi-m</ta>
            <ta e="T192" id="Seg_7457" s="T191">ertə-n</ta>
            <ta e="T193" id="Seg_7458" s="T192">uʔbdə-bi-m</ta>
            <ta e="T194" id="Seg_7459" s="T193">šumura-Tə</ta>
            <ta e="T195" id="Seg_7460" s="T194">mĭn-bi-m</ta>
            <ta e="T196" id="Seg_7461" s="T195">šo-bi-m</ta>
            <ta e="T197" id="Seg_7462" s="T196">dĭ-Tə</ta>
            <ta e="T198" id="Seg_7463" s="T197">măn-bi-m</ta>
            <ta e="T199" id="Seg_7464" s="T198">uʔbdə-ʔ</ta>
            <ta e="T200" id="Seg_7465" s="T199">amor-ə-ʔ</ta>
            <ta e="T201" id="Seg_7466" s="T200">dĭn</ta>
            <ta e="T202" id="Seg_7467" s="T201">namzəga</ta>
            <ta e="T206" id="Seg_7468" s="T205">süt</ta>
            <ta e="T207" id="Seg_7469" s="T206">i-gA</ta>
            <ta e="T208" id="Seg_7470" s="T207">keʔbde</ta>
            <ta e="T209" id="Seg_7471" s="T208">i-gA</ta>
            <ta e="T210" id="Seg_7472" s="T209">munəj-jəʔ</ta>
            <ta e="T211" id="Seg_7473" s="T210">ipek</ta>
            <ta e="T212" id="Seg_7474" s="T211">amor-ə-ʔ</ta>
            <ta e="T213" id="Seg_7475" s="T212">amnə-ʔ</ta>
            <ta e="T214" id="Seg_7476" s="T213">dĭ</ta>
            <ta e="T217" id="Seg_7477" s="T216">dĭ</ta>
            <ta e="T218" id="Seg_7478" s="T217">uʔbdə-bi</ta>
            <ta e="T219" id="Seg_7479" s="T218">bozəj-nzə-bi</ta>
            <ta e="T220" id="Seg_7480" s="T219">dĭgəttə</ta>
            <ta e="T221" id="Seg_7481" s="T220">amno-bi</ta>
            <ta e="T222" id="Seg_7482" s="T221">amor-zittə</ta>
            <ta e="T224" id="Seg_7483" s="T223">dĭgəttə</ta>
            <ta e="T225" id="Seg_7484" s="T224">kan-lAʔ</ta>
            <ta e="T226" id="Seg_7485" s="T225">tʼür-bi</ta>
            <ta e="T227" id="Seg_7486" s="T226">a</ta>
            <ta e="T228" id="Seg_7487" s="T227">măn</ta>
            <ta e="T229" id="Seg_7488" s="T228">bozəj-bi-m</ta>
            <ta e="T233" id="Seg_7489" s="T232">taldʼen</ta>
            <ta e="T234" id="Seg_7490" s="T233">măn</ta>
            <ta e="T235" id="Seg_7491" s="T234">miʔ</ta>
            <ta e="T236" id="Seg_7492" s="T235">tuʔluʔ</ta>
            <ta e="T238" id="Seg_7493" s="T237">tajər-bi-bAʔ</ta>
            <ta e="T239" id="Seg_7494" s="T238">ine-ziʔ</ta>
            <ta e="T240" id="Seg_7495" s="T239">šide</ta>
            <ta e="T241" id="Seg_7496" s="T240">koʔbdo</ta>
            <ta e="T242" id="Seg_7497" s="T241">hen-bi-jəʔ</ta>
            <ta e="T243" id="Seg_7498" s="T242">onʼiʔ</ta>
            <ta e="T244" id="Seg_7499" s="T243">tibi</ta>
            <ta e="T245" id="Seg_7500" s="T244">măn</ta>
            <ta e="T246" id="Seg_7501" s="T245">bospə-m</ta>
            <ta e="T249" id="Seg_7502" s="T248">hen-bi-m</ta>
            <ta e="T250" id="Seg_7503" s="T249">tajər-bi-bAʔ</ta>
            <ta e="T257" id="Seg_7504" s="T256">măn</ta>
            <ta e="T258" id="Seg_7505" s="T257">karəldʼaːn</ta>
            <ta e="T259" id="Seg_7506" s="T258">multʼa-Tə</ta>
            <ta e="T260" id="Seg_7507" s="T259">mĭn-bi-m</ta>
            <ta e="T261" id="Seg_7508" s="T260">bozəj-bi-m</ta>
            <ta e="T262" id="Seg_7509" s="T261">šišəge</ta>
            <ta e="T263" id="Seg_7510" s="T262">bü</ta>
            <ta e="T264" id="Seg_7511" s="T263">i-bi</ta>
            <ta e="T265" id="Seg_7512" s="T264">dʼibige</ta>
            <ta e="T266" id="Seg_7513" s="T265">bü</ta>
            <ta e="T267" id="Seg_7514" s="T266">sabən</ta>
            <ta e="T268" id="Seg_7515" s="T267">i-bi-m</ta>
            <ta e="T269" id="Seg_7516" s="T268">kujnek</ta>
            <ta e="T270" id="Seg_7517" s="T269">i-bi-m</ta>
            <ta e="T271" id="Seg_7518" s="T270">multʼa-gəʔ</ta>
            <ta e="T272" id="Seg_7519" s="T271">šo-bi-m</ta>
            <ta e="T273" id="Seg_7520" s="T272">tüžöj</ta>
            <ta e="T274" id="Seg_7521" s="T273">šo-bi</ta>
            <ta e="T275" id="Seg_7522" s="T274">măn</ta>
            <ta e="T276" id="Seg_7523" s="T275">surdo-bi-m</ta>
            <ta e="T277" id="Seg_7524" s="T276">ugaːndə</ta>
            <ta e="T278" id="Seg_7525" s="T277">tarar-luʔbdə-bi-m</ta>
            <ta e="T279" id="Seg_7526" s="T278">saʔmə-luʔbdə-bi-m</ta>
            <ta e="T280" id="Seg_7527" s="T279">i</ta>
            <ta e="T281" id="Seg_7528" s="T280">i</ta>
            <ta e="T282" id="Seg_7529" s="T281">kunol-bi-m</ta>
            <ta e="T283" id="Seg_7530" s="T282">teinen</ta>
            <ta e="T284" id="Seg_7531" s="T283">noʔ</ta>
            <ta e="T285" id="Seg_7532" s="T284">măn</ta>
            <ta e="T286" id="Seg_7533" s="T285">kan-lV-m</ta>
            <ta e="T287" id="Seg_7534" s="T286">beške-j-lAʔ</ta>
            <ta e="T288" id="Seg_7535" s="T287">beške</ta>
            <ta e="T289" id="Seg_7536" s="T288">det-lV-m</ta>
            <ta e="T290" id="Seg_7537" s="T289">dĭgəttə</ta>
            <ta e="T291" id="Seg_7538" s="T290">bazə-lV-m</ta>
            <ta e="T292" id="Seg_7539" s="T291">tustʼar-lV-m</ta>
            <ta e="T293" id="Seg_7540" s="T292">bar</ta>
            <ta e="T294" id="Seg_7541" s="T293">dĭgəttə</ta>
            <ta e="T295" id="Seg_7542" s="T294">tustʼar-lV-j</ta>
            <ta e="T296" id="Seg_7543" s="T295">bar</ta>
            <ta e="T297" id="Seg_7544" s="T296">dĭgəttə</ta>
            <ta e="T298" id="Seg_7545" s="T297">am-zittə</ta>
            <ta e="T299" id="Seg_7546" s="T298">i</ta>
            <ta e="T300" id="Seg_7547" s="T299">možna</ta>
            <ta e="T301" id="Seg_7548" s="T300">koŋ</ta>
            <ta e="T302" id="Seg_7549" s="T301">bar</ta>
            <ta e="T303" id="Seg_7550" s="T302">amno-laʔbə-bi</ta>
            <ta e="T304" id="Seg_7551" s="T303">bar</ta>
            <ta e="T305" id="Seg_7552" s="T304">ugaːndə</ta>
            <ta e="T306" id="Seg_7553" s="T305">jezerik</ta>
            <ta e="T307" id="Seg_7554" s="T306">ara</ta>
            <ta e="T308" id="Seg_7555" s="T307">bĭs-bi</ta>
            <ta e="T309" id="Seg_7556" s="T308">ugaːndə</ta>
            <ta e="T310" id="Seg_7557" s="T309">iʔgö</ta>
            <ta e="T311" id="Seg_7558" s="T310">bĭs-bi</ta>
            <ta e="T312" id="Seg_7559" s="T311">saʔmə-luʔbdə-bi</ta>
            <ta e="T313" id="Seg_7560" s="T312">bar</ta>
            <ta e="T314" id="Seg_7561" s="T313">mut_mat</ta>
            <ta e="T316" id="Seg_7562" s="T315">kudo-nzə-laʔbə</ta>
            <ta e="T317" id="Seg_7563" s="T316">măn</ta>
            <ta e="T318" id="Seg_7564" s="T317">dĭ-m</ta>
            <ta e="T319" id="Seg_7565" s="T318">amnəl-bi-m</ta>
            <ta e="T320" id="Seg_7566" s="T319">amno-ʔ</ta>
            <ta e="T321" id="Seg_7567" s="T320">măn-bi-m</ta>
            <ta e="T322" id="Seg_7568" s="T321">măn</ta>
            <ta e="T323" id="Seg_7569" s="T322">măn-bi-m</ta>
            <ta e="T324" id="Seg_7570" s="T323">e-ʔ</ta>
            <ta e="T325" id="Seg_7571" s="T324">kudo-nzə-ʔ</ta>
            <ta e="T326" id="Seg_7572" s="T325">amno-ʔ</ta>
            <ta e="T327" id="Seg_7573" s="T326">amno-ʔ</ta>
            <ta e="T328" id="Seg_7574" s="T327">segi</ta>
            <ta e="T329" id="Seg_7575" s="T328">bü</ta>
            <ta e="T330" id="Seg_7576" s="T329">bĭs-zittə</ta>
            <ta e="T331" id="Seg_7577" s="T330">sĭreʔp</ta>
            <ta e="T332" id="Seg_7578" s="T331">iʔbö-laʔbə</ta>
            <ta e="T333" id="Seg_7579" s="T332">hen-t</ta>
            <ta e="T334" id="Seg_7580" s="T333">segi</ta>
            <ta e="T335" id="Seg_7581" s="T334">bü-Tə</ta>
            <ta e="T336" id="Seg_7582" s="T335">munəj-jəʔ</ta>
            <ta e="T337" id="Seg_7583" s="T336">iʔbö-laʔbə-jəʔ</ta>
            <ta e="T340" id="Seg_7584" s="T339">ipek</ta>
            <ta e="T341" id="Seg_7585" s="T340">am-ə-ʔ</ta>
            <ta e="T342" id="Seg_7586" s="T341">uja</ta>
            <ta e="T343" id="Seg_7587" s="T342">am-ə-ʔ</ta>
            <ta e="T344" id="Seg_7588" s="T343">kola</ta>
            <ta e="T345" id="Seg_7589" s="T344">am-ə-ʔ</ta>
            <ta e="T346" id="Seg_7590" s="T345">munəj</ta>
            <ta e="T347" id="Seg_7591" s="T346">iʔbö-laʔbə</ta>
            <ta e="T348" id="Seg_7592" s="T347">am-ə-ʔ</ta>
            <ta e="T349" id="Seg_7593" s="T348">kajaʔ</ta>
            <ta e="T350" id="Seg_7594" s="T349">am-ə-ʔ</ta>
            <ta e="T351" id="Seg_7595" s="T350">oroma</ta>
            <ta e="T352" id="Seg_7596" s="T351">am-ə-ʔ</ta>
            <ta e="T353" id="Seg_7597" s="T352">namzəga</ta>
            <ta e="T354" id="Seg_7598" s="T353">süt</ta>
            <ta e="T355" id="Seg_7599" s="T354">am-ə-ʔ</ta>
            <ta e="T356" id="Seg_7600" s="T355">kola</ta>
            <ta e="T357" id="Seg_7601" s="T356">am-ə-ʔ</ta>
            <ta e="T359" id="Seg_7602" s="T358">nadə</ta>
            <ta e="T360" id="Seg_7603" s="T359">măna</ta>
            <ta e="T361" id="Seg_7604" s="T360">jama</ta>
            <ta e="T362" id="Seg_7605" s="T361">šöʔ-zittə</ta>
            <ta e="T363" id="Seg_7606" s="T362">parga</ta>
            <ta e="T364" id="Seg_7607" s="T363">šöʔ-zittə</ta>
            <ta e="T365" id="Seg_7608" s="T364">nadə</ta>
            <ta e="T368" id="Seg_7609" s="T367">kujnek</ta>
            <ta e="T369" id="Seg_7610" s="T368">šöʔ-zittə</ta>
            <ta e="T370" id="Seg_7611" s="T369">i-zittə</ta>
            <ta e="T373" id="Seg_7612" s="T372">nadə</ta>
            <ta e="T374" id="Seg_7613" s="T373">piʔme</ta>
            <ta e="T375" id="Seg_7614" s="T374">šöʔ-zittə</ta>
            <ta e="T376" id="Seg_7615" s="T375">ato</ta>
            <ta e="T377" id="Seg_7616" s="T376">măn</ta>
            <ta e="T378" id="Seg_7617" s="T377">nʼi-m</ta>
            <ta e="T379" id="Seg_7618" s="T378">bar</ta>
            <ta e="T380" id="Seg_7619" s="T379">šer-zittə</ta>
            <ta e="T381" id="Seg_7620" s="T380">naga</ta>
            <ta e="T382" id="Seg_7621" s="T381">ĭmbi</ta>
            <ta e="T383" id="Seg_7622" s="T382">ĭmbi=də</ta>
            <ta e="T384" id="Seg_7623" s="T383">naga</ta>
            <ta e="T385" id="Seg_7624" s="T384">măn</ta>
            <ta e="T386" id="Seg_7625" s="T385">kan-lV-m</ta>
            <ta e="T387" id="Seg_7626" s="T386">Kazan</ta>
            <ta e="T388" id="Seg_7627" s="T387">tura-Tə</ta>
            <ta e="T389" id="Seg_7628" s="T388">i</ta>
            <ta e="T390" id="Seg_7629" s="T389">nadə</ta>
            <ta e="T391" id="Seg_7630" s="T390">ipek</ta>
            <ta e="T392" id="Seg_7631" s="T391">i-zittə</ta>
            <ta e="T393" id="Seg_7632" s="T392">nadə</ta>
            <ta e="T394" id="Seg_7633" s="T393">sĭreʔp</ta>
            <ta e="T395" id="Seg_7634" s="T394">i-zittə</ta>
            <ta e="T396" id="Seg_7635" s="T395">nadə</ta>
            <ta e="T399" id="Seg_7636" s="T398">multuk-də</ta>
            <ta e="T400" id="Seg_7637" s="T399">ipek</ta>
            <ta e="T401" id="Seg_7638" s="T400">i-zittə</ta>
            <ta e="T402" id="Seg_7639" s="T401">nadə</ta>
            <ta e="T409" id="Seg_7640" s="T408">no</ta>
            <ta e="T410" id="Seg_7641" s="T409">ipek</ta>
            <ta e="T416" id="Seg_7642" s="T415">măn</ta>
            <ta e="T417" id="Seg_7643" s="T416">dʼije-Tə</ta>
            <ta e="T418" id="Seg_7644" s="T417">dĭgəttə</ta>
            <ta e="T419" id="Seg_7645" s="T418">kan-lV-m</ta>
            <ta e="T422" id="Seg_7646" s="T421">teinen</ta>
            <ta e="T425" id="Seg_7647" s="T424">dĭ-zAŋ</ta>
            <ta e="T426" id="Seg_7648" s="T425">dʼije-gəʔ</ta>
            <ta e="T427" id="Seg_7649" s="T426">šo-lV-jəʔ</ta>
            <ta e="T428" id="Seg_7650" s="T427">padʼi</ta>
            <ta e="T429" id="Seg_7651" s="T428">uja</ta>
            <ta e="T430" id="Seg_7652" s="T429">det-lV-jəʔ</ta>
            <ta e="T431" id="Seg_7653" s="T430">kalba</ta>
            <ta e="T432" id="Seg_7654" s="T431">det-lV-jəʔ</ta>
            <ta e="T434" id="Seg_7655" s="T433">tʼăgar-zittə</ta>
            <ta e="T435" id="Seg_7656" s="T434">nadə</ta>
            <ta e="T436" id="Seg_7657" s="T435">da</ta>
            <ta e="T437" id="Seg_7658" s="T436">tustʼar-zittə</ta>
            <ta e="T438" id="Seg_7659" s="T437">munəj-jəʔ</ta>
            <ta e="T439" id="Seg_7660" s="T438">hen-zittə</ta>
            <ta e="T440" id="Seg_7661" s="T439">oroma</ta>
            <ta e="T441" id="Seg_7662" s="T440">hen-zittə</ta>
            <ta e="T442" id="Seg_7663" s="T441">i</ta>
            <ta e="T443" id="Seg_7664" s="T442">bü</ta>
            <ta e="T444" id="Seg_7665" s="T443">kămnə-zittə</ta>
            <ta e="T445" id="Seg_7666" s="T444">i</ta>
            <ta e="T446" id="Seg_7667" s="T445">amor-zittə</ta>
            <ta e="T447" id="Seg_7668" s="T446">šamnak-ziʔ</ta>
            <ta e="T448" id="Seg_7669" s="T447">nadə</ta>
            <ta e="T449" id="Seg_7670" s="T448">kan-zittə</ta>
            <ta e="T454" id="Seg_7671" s="T453">kan-zittə</ta>
            <ta e="T459" id="Seg_7672" s="T458">tajər-zittə</ta>
            <ta e="T460" id="Seg_7673" s="T459">ipek</ta>
            <ta e="T461" id="Seg_7674" s="T460">kuʔ-zittə</ta>
            <ta e="T462" id="Seg_7675" s="T461">dĭgəttə</ta>
            <ta e="T463" id="Seg_7676" s="T462">özer-lV-j</ta>
            <ta e="T464" id="Seg_7677" s="T463">padə-zittə</ta>
            <ta e="T465" id="Seg_7678" s="T464">nadə</ta>
            <ta e="T466" id="Seg_7679" s="T465">dĭgəttə</ta>
            <ta e="T467" id="Seg_7680" s="T466">toʔ-nar-zittə</ta>
            <ta e="T468" id="Seg_7681" s="T467">nadə</ta>
            <ta e="T469" id="Seg_7682" s="T468">dĭgəttə</ta>
            <ta e="T476" id="Seg_7683" s="T475">dĭgəttə</ta>
            <ta e="T477" id="Seg_7684" s="T476">nʼeʔbdə-zittə</ta>
            <ta e="T478" id="Seg_7685" s="T477">nadə</ta>
            <ta e="T481" id="Seg_7686" s="T480">dĭgəttə</ta>
            <ta e="T482" id="Seg_7687" s="T481">nadə</ta>
            <ta e="T483" id="Seg_7688" s="T482">pür-zittə</ta>
            <ta e="T484" id="Seg_7689" s="T483">dĭgəttə</ta>
            <ta e="T485" id="Seg_7690" s="T484">amor-zittə</ta>
            <ta e="T486" id="Seg_7691" s="T485">nadə</ta>
            <ta e="T489" id="Seg_7692" s="T488">teinen</ta>
            <ta e="T490" id="Seg_7693" s="T489">urgo</ta>
            <ta e="T491" id="Seg_7694" s="T490">tʼala</ta>
            <ta e="T494" id="Seg_7695" s="T493">il</ta>
            <ta e="T495" id="Seg_7696" s="T494">ugaːndə</ta>
            <ta e="T496" id="Seg_7697" s="T495">iʔgö</ta>
            <ta e="T497" id="Seg_7698" s="T496">kuvas-jəʔ</ta>
            <ta e="T498" id="Seg_7699" s="T497">bar</ta>
            <ta e="T499" id="Seg_7700" s="T498">ara</ta>
            <ta e="T500" id="Seg_7701" s="T499">bĭs-laʔbə-jəʔ</ta>
            <ta e="T501" id="Seg_7702" s="T500">sʼar-laʔbə-jəʔ</ta>
            <ta e="T503" id="Seg_7703" s="T502">süʔmə-laʔbə-jəʔ</ta>
            <ta e="T504" id="Seg_7704" s="T503">nüjnə</ta>
            <ta e="T505" id="Seg_7705" s="T504">nüj-laʔbə-jəʔ</ta>
            <ta e="T506" id="Seg_7706" s="T505">jakšə</ta>
            <ta e="T507" id="Seg_7707" s="T506">nüke</ta>
            <ta e="T508" id="Seg_7708" s="T507">ipek</ta>
            <ta e="T509" id="Seg_7709" s="T508">bar</ta>
            <ta e="T512" id="Seg_7710" s="T511">pür-bi</ta>
            <ta e="T513" id="Seg_7711" s="T512">jakšə</ta>
            <ta e="T514" id="Seg_7712" s="T513">nömər</ta>
            <ta e="T515" id="Seg_7713" s="T514">tüjö</ta>
            <ta e="T520" id="Seg_7714" s="T519">amor-KAʔ</ta>
            <ta e="T521" id="Seg_7715" s="T520">padʼi</ta>
            <ta e="T522" id="Seg_7716" s="T521">püjö-liA-jəʔ</ta>
            <ta e="T523" id="Seg_7717" s="T522">tüjö</ta>
            <ta e="T524" id="Seg_7718" s="T523">uʔbdə-lV-m</ta>
            <ta e="T525" id="Seg_7719" s="T524">am-KAʔ</ta>
            <ta e="T526" id="Seg_7720" s="T525">amnə</ta>
            <ta e="T527" id="Seg_7721" s="T526">amno-KAʔ</ta>
            <ta e="T528" id="Seg_7722" s="T527">nʼešpək</ta>
            <ta e="T529" id="Seg_7723" s="T528">kuza</ta>
            <ta e="T530" id="Seg_7724" s="T529">am-bi</ta>
            <ta e="T531" id="Seg_7725" s="T530">ipek</ta>
            <ta e="T532" id="Seg_7726" s="T531">lem</ta>
            <ta e="T533" id="Seg_7727" s="T532">keʔbde-ziʔ</ta>
            <ta e="T534" id="Seg_7728" s="T533">oroma</ta>
            <ta e="T535" id="Seg_7729" s="T534">nuldə-bi-m</ta>
            <ta e="T536" id="Seg_7730" s="T535">amor-KAʔ</ta>
            <ta e="T538" id="Seg_7731" s="T537">süt</ta>
            <ta e="T541" id="Seg_7732" s="T540">bĭs-t</ta>
            <ta e="T544" id="Seg_7733" s="T543">tăn</ta>
            <ta e="T545" id="Seg_7734" s="T544">ĭššo</ta>
            <ta e="T546" id="Seg_7735" s="T545">kunol-laʔbə-l</ta>
            <ta e="T547" id="Seg_7736" s="T546">uʔbdə-ʔ</ta>
            <ta e="T548" id="Seg_7737" s="T547">ato</ta>
            <ta e="T549" id="Seg_7738" s="T548">šišəge</ta>
            <ta e="T550" id="Seg_7739" s="T549">bü</ta>
            <ta e="T551" id="Seg_7740" s="T550">det-lV-m</ta>
            <ta e="T552" id="Seg_7741" s="T551">tănan</ta>
            <ta e="T553" id="Seg_7742" s="T552">kămnə-lV-m</ta>
            <ta e="T554" id="Seg_7743" s="T553">bar</ta>
            <ta e="T555" id="Seg_7744" s="T554">dĭgəttə</ta>
            <ta e="T556" id="Seg_7745" s="T555">bar</ta>
            <ta e="T557" id="Seg_7746" s="T556">süʔmə-luʔbdə-lV-l</ta>
            <ta e="T567" id="Seg_7747" s="T566">ej</ta>
            <ta e="T568" id="Seg_7748" s="T567">nʼilgö-t</ta>
            <ta e="T569" id="Seg_7749" s="T568">e-ʔ</ta>
            <ta e="T570" id="Seg_7750" s="T569">hʼaʔ-ʔ</ta>
            <ta e="T574" id="Seg_7751" s="T573">e-ʔ</ta>
            <ta e="T575" id="Seg_7752" s="T574">tʼăbaktər-ə-ʔ</ta>
            <ta e="T578" id="Seg_7753" s="T577">e-ʔ</ta>
            <ta e="T579" id="Seg_7754" s="T578">kudo-nzə-ʔ</ta>
            <ta e="T583" id="Seg_7755" s="T582">e-ʔ</ta>
            <ta e="T584" id="Seg_7756" s="T583">kirgaːr-ə-ʔ</ta>
            <ta e="T587" id="Seg_7757" s="T586">kuʔ-l</ta>
            <ta e="T588" id="Seg_7758" s="T587">mʼaŋ-laʔbə</ta>
            <ta e="T589" id="Seg_7759" s="T588">kĭški-t</ta>
            <ta e="T590" id="Seg_7760" s="T589">kuʔ-l</ta>
            <ta e="T593" id="Seg_7761" s="T592">e-ʔ</ta>
            <ta e="T594" id="Seg_7762" s="T593">nuʔmə-ʔ</ta>
            <ta e="T595" id="Seg_7763" s="T594">e-ʔ</ta>
            <ta e="T596" id="Seg_7764" s="T595">süʔmə-ʔ</ta>
            <ta e="T597" id="Seg_7765" s="T596">e-ʔ</ta>
            <ta e="T598" id="Seg_7766" s="T597">nüj-ʔ</ta>
            <ta e="T599" id="Seg_7767" s="T598">ugaːndə</ta>
            <ta e="T600" id="Seg_7768" s="T599">jakšə</ta>
            <ta e="T601" id="Seg_7769" s="T600">ugaːndə</ta>
            <ta e="T602" id="Seg_7770" s="T601">kuvas</ta>
            <ta e="T603" id="Seg_7771" s="T602">ugaːndə</ta>
            <ta e="T604" id="Seg_7772" s="T603">kömə</ta>
            <ta e="T605" id="Seg_7773" s="T604">ugaːndə</ta>
            <ta e="T607" id="Seg_7774" s="T606">kəbo</ta>
            <ta e="T608" id="Seg_7775" s="T607">ugaːndə</ta>
            <ta e="T609" id="Seg_7776" s="T608">numo</ta>
            <ta e="T613" id="Seg_7777" s="T612">ugaːndə</ta>
            <ta e="T614" id="Seg_7778" s="T613">üdʼžʼüge</ta>
            <ta e="T619" id="Seg_7779" s="T618">padʼi</ta>
            <ta e="T620" id="Seg_7780" s="T619">plat</ta>
            <ta e="T621" id="Seg_7781" s="T620">tojar-bi-l</ta>
            <ta e="T622" id="Seg_7782" s="T621">našto</ta>
            <ta e="T623" id="Seg_7783" s="T622">det-bi-l</ta>
            <ta e="T624" id="Seg_7784" s="T623">măn</ta>
            <ta e="T625" id="Seg_7785" s="T624">ej</ta>
            <ta e="T626" id="Seg_7786" s="T625">i-lV-m</ta>
            <ta e="T627" id="Seg_7787" s="T626">nʼe</ta>
            <ta e="T628" id="Seg_7788" s="T627">nadə</ta>
            <ta e="T629" id="Seg_7789" s="T628">tojar-zittə</ta>
            <ta e="T630" id="Seg_7790" s="T629">ato</ta>
            <ta e="T631" id="Seg_7791" s="T630">ej</ta>
            <ta e="T632" id="Seg_7792" s="T631">jakšə</ta>
            <ta e="T633" id="Seg_7793" s="T632">mo-lV-j</ta>
            <ta e="T634" id="Seg_7794" s="T633">uda-m</ta>
            <ta e="T635" id="Seg_7795" s="T634">băt-bi-m</ta>
            <ta e="T636" id="Seg_7796" s="T635">bar</ta>
            <ta e="T637" id="Seg_7797" s="T636">kem</ta>
            <ta e="T638" id="Seg_7798" s="T637">mʼaŋ-laʔbə</ta>
            <ta e="T639" id="Seg_7799" s="T638">det-ʔ</ta>
            <ta e="T640" id="Seg_7800" s="T639">ĭmbi=nʼibudʼ</ta>
            <ta e="T641" id="Seg_7801" s="T640">uda-m</ta>
            <ta e="T642" id="Seg_7802" s="T641">sar-zittə</ta>
            <ta e="T647" id="Seg_7803" s="T645">našto</ta>
            <ta e="T648" id="Seg_7804" s="T647">tagaj-ziʔ</ta>
            <ta e="T649" id="Seg_7805" s="T648">tagaj</ta>
            <ta e="T650" id="Seg_7806" s="T649">i-bi-l</ta>
            <ta e="T651" id="Seg_7807" s="T650">tănan</ta>
            <ta e="T652" id="Seg_7808" s="T651">ĭššo</ta>
            <ta e="T654" id="Seg_7809" s="T652">nadə</ta>
            <ta e="T655" id="Seg_7810" s="T654">beržə</ta>
            <ta e="T656" id="Seg_7811" s="T655">surno</ta>
            <ta e="T657" id="Seg_7812" s="T656">bar</ta>
            <ta e="T658" id="Seg_7813" s="T657">šonə-gA</ta>
            <ta e="T659" id="Seg_7814" s="T658">sĭri</ta>
            <ta e="T660" id="Seg_7815" s="T659">bar</ta>
            <ta e="T661" id="Seg_7816" s="T660">šonə-gA</ta>
            <ta e="T662" id="Seg_7817" s="T661">beržə-ziʔ</ta>
            <ta e="T663" id="Seg_7818" s="T662">ugaːndə</ta>
            <ta e="T664" id="Seg_7819" s="T663">šišəge</ta>
            <ta e="T665" id="Seg_7820" s="T664">măn</ta>
            <ta e="T666" id="Seg_7821" s="T665">ugaːndə</ta>
            <ta e="T667" id="Seg_7822" s="T666">kăn-bi-m</ta>
            <ta e="T668" id="Seg_7823" s="T667">da</ta>
            <ta e="T669" id="Seg_7824" s="T668">tăŋ</ta>
            <ta e="T670" id="Seg_7825" s="T669">kăn-bi-m</ta>
            <ta e="T671" id="Seg_7826" s="T670">üjü-m</ta>
            <ta e="T672" id="Seg_7827" s="T671">kăn-bi</ta>
            <ta e="T673" id="Seg_7828" s="T672">uda-m</ta>
            <ta e="T674" id="Seg_7829" s="T673">kăn-bi</ta>
            <ta e="T675" id="Seg_7830" s="T674">kadul-m</ta>
            <ta e="T676" id="Seg_7831" s="T675">bar</ta>
            <ta e="T677" id="Seg_7832" s="T676">kăn-bi</ta>
            <ta e="T678" id="Seg_7833" s="T677">teinen</ta>
            <ta e="T681" id="Seg_7834" s="T680">kuvas</ta>
            <ta e="T682" id="Seg_7835" s="T681">tʼala</ta>
            <ta e="T683" id="Seg_7836" s="T682">kuja</ta>
            <ta e="T684" id="Seg_7837" s="T683">bar</ta>
            <ta e="T685" id="Seg_7838" s="T684">măndo-laʔbə</ta>
            <ta e="T686" id="Seg_7839" s="T685">nʼuʔnun</ta>
            <ta e="T687" id="Seg_7840" s="T686">ĭmbi=də</ta>
            <ta e="T688" id="Seg_7841" s="T687">naga</ta>
            <ta e="T689" id="Seg_7842" s="T688">ugaːndə</ta>
            <ta e="T690" id="Seg_7843" s="T689">jakšə</ta>
            <ta e="T691" id="Seg_7844" s="T690">tʼala</ta>
            <ta e="T692" id="Seg_7845" s="T691">il</ta>
            <ta e="T693" id="Seg_7846" s="T692">bar</ta>
            <ta e="T694" id="Seg_7847" s="T693">togonər-laʔbə-jəʔ</ta>
            <ta e="T695" id="Seg_7848" s="T694">toltano</ta>
            <ta e="T696" id="Seg_7849" s="T695">am-laʔbə-jəʔ</ta>
            <ta e="T697" id="Seg_7850" s="T696">ine-jəʔ</ta>
            <ta e="T698" id="Seg_7851" s="T697">tajər-laʔbə-jəʔ</ta>
            <ta e="T699" id="Seg_7852" s="T698">bar</ta>
            <ta e="T700" id="Seg_7853" s="T699">nadə</ta>
            <ta e="T701" id="Seg_7854" s="T700">kan-zittə</ta>
            <ta e="T702" id="Seg_7855" s="T701">noʔ</ta>
            <ta e="T703" id="Seg_7856" s="T702">hʼaʔ-zittə</ta>
            <ta e="T704" id="Seg_7857" s="T703">šapku</ta>
            <ta e="T705" id="Seg_7858" s="T704">nadə</ta>
            <ta e="T706" id="Seg_7859" s="T705">i-zittə</ta>
            <ta e="T707" id="Seg_7860" s="T706">nadə</ta>
            <ta e="T708" id="Seg_7861" s="T707">bü</ta>
            <ta e="T709" id="Seg_7862" s="T708">i-zittə</ta>
            <ta e="T710" id="Seg_7863" s="T709">süt</ta>
            <ta e="T711" id="Seg_7864" s="T710">i-zittə</ta>
            <ta e="T712" id="Seg_7865" s="T711">nadə</ta>
            <ta e="T713" id="Seg_7866" s="T712">ipek</ta>
            <ta e="T714" id="Seg_7867" s="T713">i-zittə</ta>
            <ta e="T715" id="Seg_7868" s="T714">tus</ta>
            <ta e="T716" id="Seg_7869" s="T715">i-zittə</ta>
            <ta e="T717" id="Seg_7870" s="T716">munəj-jəʔ</ta>
            <ta e="T718" id="Seg_7871" s="T717">i-zittə</ta>
            <ta e="T719" id="Seg_7872" s="T718">pi</ta>
            <ta e="T720" id="Seg_7873" s="T719">nadə</ta>
            <ta e="T721" id="Seg_7874" s="T720">i-zittə</ta>
            <ta e="T722" id="Seg_7875" s="T721">šapku</ta>
            <ta e="T723" id="Seg_7876" s="T722">albuga</ta>
            <ta e="T724" id="Seg_7877" s="T723">teinen</ta>
            <ta e="T725" id="Seg_7878" s="T724">kut-bi-m</ta>
            <ta e="T726" id="Seg_7879" s="T725">tažəp</ta>
            <ta e="T727" id="Seg_7880" s="T726">kut-bi-m</ta>
            <ta e="T728" id="Seg_7881" s="T727">urgaːba</ta>
            <ta e="T729" id="Seg_7882" s="T728">kut-bi-m</ta>
            <ta e="T730" id="Seg_7883" s="T729">sĭgən</ta>
            <ta e="T731" id="Seg_7884" s="T730">kut-bi-m</ta>
            <ta e="T732" id="Seg_7885" s="T731">askər</ta>
            <ta e="T733" id="Seg_7886" s="T732">kut-bi-m</ta>
            <ta e="T734" id="Seg_7887" s="T733">poʔto</ta>
            <ta e="T735" id="Seg_7888" s="T734">kut-bi-m</ta>
            <ta e="T736" id="Seg_7889" s="T735">multuk-ziʔ</ta>
            <ta e="T737" id="Seg_7890" s="T736">tüžöj-jəʔ</ta>
            <ta e="T738" id="Seg_7891" s="T737">bar</ta>
            <ta e="T739" id="Seg_7892" s="T738">noʔ</ta>
            <ta e="T740" id="Seg_7893" s="T739">am-liA-jəʔ</ta>
            <ta e="T741" id="Seg_7894" s="T740">mĭn-laʔbə-jəʔ</ta>
            <ta e="T742" id="Seg_7895" s="T741">dĭn</ta>
            <ta e="T743" id="Seg_7896" s="T742">dĭ</ta>
            <ta e="T744" id="Seg_7897" s="T743">kuza</ta>
            <ta e="T745" id="Seg_7898" s="T744">det-lV-j</ta>
            <ta e="T747" id="Seg_7899" s="T746">maʔ-gəndə</ta>
            <ta e="T749" id="Seg_7900" s="T748">nadə</ta>
            <ta e="T750" id="Seg_7901" s="T749">ešši-m</ta>
            <ta e="T751" id="Seg_7902" s="T750">bazə-zittə</ta>
            <ta e="T752" id="Seg_7903" s="T751">nadə</ta>
            <ta e="T755" id="Seg_7904" s="T754">ešši-m</ta>
            <ta e="T756" id="Seg_7905" s="T755">bar</ta>
            <ta e="T757" id="Seg_7906" s="T756">tʼor-laʔbə</ta>
            <ta e="T758" id="Seg_7907" s="T757">nadə</ta>
            <ta e="T759" id="Seg_7908" s="T758">dĭ-m</ta>
            <ta e="T760" id="Seg_7909" s="T759">köːdər-zittə</ta>
            <ta e="T761" id="Seg_7910" s="T760">nadə</ta>
            <ta e="T762" id="Seg_7911" s="T761">amor-zittə</ta>
            <ta e="T763" id="Seg_7912" s="T762">mĭ-zittə</ta>
            <ta e="T764" id="Seg_7913" s="T763">nadə</ta>
            <ta e="T765" id="Seg_7914" s="T764">kunol-zittə</ta>
            <ta e="T766" id="Seg_7915" s="T765">hen-zittə</ta>
            <ta e="T771" id="Seg_7916" s="T770">oːrdə-ʔ</ta>
            <ta e="T772" id="Seg_7917" s="T771">ešši-m</ta>
            <ta e="T773" id="Seg_7918" s="T772">bar</ta>
            <ta e="T774" id="Seg_7919" s="T773">pušaj</ta>
            <ta e="T775" id="Seg_7920" s="T774">kunol-laʔbə</ta>
            <ta e="T776" id="Seg_7921" s="T775">amnə-ʔ</ta>
            <ta e="T777" id="Seg_7922" s="T776">tüjö</ta>
            <ta e="T778" id="Seg_7923" s="T777">bü-j-lAʔ</ta>
            <ta e="T779" id="Seg_7924" s="T778">kan-lV-m</ta>
            <ta e="T780" id="Seg_7925" s="T779">bü</ta>
            <ta e="T781" id="Seg_7926" s="T780">det-lV-m</ta>
            <ta e="T782" id="Seg_7927" s="T781">dĭgəttə</ta>
            <ta e="T783" id="Seg_7928" s="T782">bĭs-lV-l</ta>
            <ta e="T784" id="Seg_7929" s="T783">e-ʔ</ta>
            <ta e="T785" id="Seg_7930" s="T784">kan-ə-ʔ</ta>
            <ta e="T786" id="Seg_7931" s="T785">a</ta>
            <ta e="T787" id="Seg_7932" s="T786">dĭgəttə</ta>
            <ta e="T788" id="Seg_7933" s="T787">kan-lV-l</ta>
            <ta e="T789" id="Seg_7934" s="T788">maʔ-Tə-l</ta>
            <ta e="T790" id="Seg_7935" s="T789">tüjö</ta>
            <ta e="T791" id="Seg_7936" s="T790">kem</ta>
            <ta e="T792" id="Seg_7937" s="T791">pür-lV-m</ta>
            <ta e="T793" id="Seg_7938" s="T792">nadə</ta>
            <ta e="T794" id="Seg_7939" s="T793">dĭbər</ta>
            <ta e="T795" id="Seg_7940" s="T794">süt</ta>
            <ta e="T796" id="Seg_7941" s="T795">kămnə-zittə</ta>
            <ta e="T797" id="Seg_7942" s="T796">kajaʔ</ta>
            <ta e="T798" id="Seg_7943" s="T797">hen-zittə</ta>
            <ta e="T799" id="Seg_7944" s="T798">aľi</ta>
            <ta e="T800" id="Seg_7945" s="T799">sil</ta>
            <ta e="T801" id="Seg_7946" s="T800">hen-zittə</ta>
            <ta e="T802" id="Seg_7947" s="T801">tüjö</ta>
            <ta e="T803" id="Seg_7948" s="T802">amor-lV-bəj</ta>
            <ta e="T804" id="Seg_7949" s="T803">e-ʔ</ta>
            <ta e="T805" id="Seg_7950" s="T804">baza</ta>
            <ta e="T806" id="Seg_7951" s="T805">ato</ta>
            <ta e="T807" id="Seg_7952" s="T806">dʼibige</ta>
            <ta e="T810" id="Seg_7953" s="T809">ipek-ziʔ</ta>
            <ta e="T811" id="Seg_7954" s="T810">am-ə-ʔ</ta>
            <ta e="T812" id="Seg_7955" s="T811">Koʔbdo</ta>
            <ta e="T814" id="Seg_7956" s="T812">šo-bi</ta>
            <ta e="T815" id="Seg_7957" s="T814">eʔbdə</ta>
            <ta e="T816" id="Seg_7958" s="T815">sĭri</ta>
            <ta e="T817" id="Seg_7959" s="T816">bos-də</ta>
            <ta e="T818" id="Seg_7960" s="T817">bar</ta>
            <ta e="T819" id="Seg_7961" s="T818">todam</ta>
            <ta e="T820" id="Seg_7962" s="T819">i</ta>
            <ta e="T821" id="Seg_7963" s="T820">dĭ</ta>
            <ta e="T822" id="Seg_7964" s="T821">kuza</ta>
            <ta e="T823" id="Seg_7965" s="T822">šo-bi</ta>
            <ta e="T824" id="Seg_7966" s="T823">nʼešpək</ta>
            <ta e="T825" id="Seg_7967" s="T824">ugaːndə</ta>
            <ta e="T826" id="Seg_7968" s="T825">i</ta>
            <ta e="T827" id="Seg_7969" s="T826">mĭ-ʔ</ta>
            <ta e="T828" id="Seg_7970" s="T827">hele-t</ta>
            <ta e="T829" id="Seg_7971" s="T828">šo-bi</ta>
            <ta e="T831" id="Seg_7972" s="T830">dĭ-zAŋ</ta>
            <ta e="T832" id="Seg_7973" s="T831">toltano</ta>
            <ta e="T833" id="Seg_7974" s="T832">ej</ta>
            <ta e="T834" id="Seg_7975" s="T833">amnə-bi-jəʔ</ta>
            <ta e="T835" id="Seg_7976" s="T834">măn</ta>
            <ta e="T836" id="Seg_7977" s="T835">aba-m</ta>
            <ta e="T837" id="Seg_7978" s="T836">dĭ-zAŋ-Tə</ta>
            <ta e="T838" id="Seg_7979" s="T837">amnəl-bi</ta>
            <ta e="T839" id="Seg_7980" s="T838">dĭ-zAŋ</ta>
            <ta e="T840" id="Seg_7981" s="T839">uja</ta>
            <ta e="T841" id="Seg_7982" s="T840">iʔgö</ta>
            <ta e="T842" id="Seg_7983" s="T841">i-bi</ta>
            <ta e="T843" id="Seg_7984" s="T842">bar</ta>
            <ta e="T844" id="Seg_7985" s="T843">uja</ta>
            <ta e="T845" id="Seg_7986" s="T844">am-bi-jəʔ</ta>
            <ta e="T846" id="Seg_7987" s="T845">ipek</ta>
            <ta e="T847" id="Seg_7988" s="T846">amka</ta>
            <ta e="T848" id="Seg_7989" s="T847">i-bi</ta>
            <ta e="T849" id="Seg_7990" s="T848">a</ta>
            <ta e="T850" id="Seg_7991" s="T849">daška</ta>
            <ta e="T851" id="Seg_7992" s="T850">ĭmbi=də</ta>
            <ta e="T854" id="Seg_7993" s="T853">naga</ta>
            <ta e="T855" id="Seg_7994" s="T854">ĭmbi</ta>
            <ta e="T856" id="Seg_7995" s="T855">amno-laʔbə-l</ta>
            <ta e="T857" id="Seg_7996" s="T856">ej</ta>
            <ta e="T858" id="Seg_7997" s="T857">tʼăbaktər-liA-l</ta>
            <ta e="T859" id="Seg_7998" s="T858">tʼăbaktər-ə-ʔ</ta>
            <ta e="T860" id="Seg_7999" s="T859">ĭmbi=nʼibudʼ</ta>
            <ta e="T861" id="Seg_8000" s="T860">sĭj-m</ta>
            <ta e="T862" id="Seg_8001" s="T861">verna</ta>
            <ta e="T865" id="Seg_8002" s="T864">sĭj-l</ta>
            <ta e="T866" id="Seg_8003" s="T865">verna</ta>
            <ta e="T867" id="Seg_8004" s="T866">ĭzem-liA</ta>
            <ta e="T868" id="Seg_8005" s="T867">ĭmbi=də</ta>
            <ta e="T869" id="Seg_8006" s="T868">ej</ta>
            <ta e="T870" id="Seg_8007" s="T869">tʼăbaktər-liA-l</ta>
            <ta e="T871" id="Seg_8008" s="T870">tʼăbaktər-ə-ʔ</ta>
            <ta e="T875" id="Seg_8009" s="T874">gibər</ta>
            <ta e="T876" id="Seg_8010" s="T875">kandə-gA-l</ta>
            <ta e="T877" id="Seg_8011" s="T876">gijen</ta>
            <ta e="T878" id="Seg_8012" s="T877">i-bi-l</ta>
            <ta e="T879" id="Seg_8013" s="T878">šo-ʔ</ta>
            <ta e="T880" id="Seg_8014" s="T879">döbər</ta>
            <ta e="T881" id="Seg_8015" s="T880">ĭmbi=nʼibudʼ</ta>
            <ta e="T882" id="Seg_8016" s="T881">nörbə-ʔ</ta>
            <ta e="T883" id="Seg_8017" s="T882">măna</ta>
            <ta e="T886" id="Seg_8018" s="T885">büzʼe</ta>
            <ta e="T887" id="Seg_8019" s="T886">nüke-t-ziʔ</ta>
            <ta e="T888" id="Seg_8020" s="T887">amno-laʔpi-jəʔ</ta>
            <ta e="T889" id="Seg_8021" s="T888">dĭ-zAŋ</ta>
            <ta e="T890" id="Seg_8022" s="T889">ešši-zAŋ-Tə</ta>
            <ta e="T891" id="Seg_8023" s="T890">i-bi-jəʔ</ta>
            <ta e="T892" id="Seg_8024" s="T891">koʔbdo</ta>
            <ta e="T893" id="Seg_8025" s="T892">da</ta>
            <ta e="T894" id="Seg_8026" s="T893">nʼi</ta>
            <ta e="T895" id="Seg_8027" s="T894">tura</ta>
            <ta e="T896" id="Seg_8028" s="T895">i-bi</ta>
            <ta e="T897" id="Seg_8029" s="T896">tüžöj</ta>
            <ta e="T898" id="Seg_8030" s="T897">i-bi</ta>
            <ta e="T899" id="Seg_8031" s="T898">dĭ</ta>
            <ta e="T900" id="Seg_8032" s="T899">nüke</ta>
            <ta e="T901" id="Seg_8033" s="T900">tüžöj</ta>
            <ta e="T902" id="Seg_8034" s="T901">surdo-bi</ta>
            <ta e="T903" id="Seg_8035" s="T902">ešši-zAŋ-Tə</ta>
            <ta e="T904" id="Seg_8036" s="T903">mĭ-bi</ta>
            <ta e="T905" id="Seg_8037" s="T904">ipek</ta>
            <ta e="T906" id="Seg_8038" s="T905">mĭ-bi</ta>
            <ta e="T907" id="Seg_8039" s="T906">dĭ-zAŋ</ta>
            <ta e="T908" id="Seg_8040" s="T907">am-liA-jəʔ</ta>
            <ta e="T910" id="Seg_8041" s="T909">dĭ</ta>
            <ta e="T911" id="Seg_8042" s="T910">šide-göʔ</ta>
            <ta e="T912" id="Seg_8043" s="T911">kuza</ta>
            <ta e="T913" id="Seg_8044" s="T912">šo-bi-jəʔ</ta>
            <ta e="T914" id="Seg_8045" s="T913">tibi</ta>
            <ta e="T915" id="Seg_8046" s="T914">da</ta>
            <ta e="T916" id="Seg_8047" s="T915">koʔbdo</ta>
            <ta e="T917" id="Seg_8048" s="T916">tănan</ta>
            <ta e="T918" id="Seg_8049" s="T917">padʼi</ta>
            <ta e="T919" id="Seg_8050" s="T918">ĭmbi=nʼibudʼ</ta>
            <ta e="T920" id="Seg_8051" s="T919">det-bi-jəʔ</ta>
            <ta e="T921" id="Seg_8052" s="T920">sĭreʔp</ta>
            <ta e="T922" id="Seg_8053" s="T921">det-bi-jəʔ</ta>
            <ta e="T925" id="Seg_8054" s="T924">segi</ta>
            <ta e="T926" id="Seg_8055" s="T925">bü</ta>
            <ta e="T927" id="Seg_8056" s="T926">bĭs-lV-l</ta>
            <ta e="T928" id="Seg_8057" s="T927">padʼi</ta>
            <ta e="T929" id="Seg_8058" s="T928">kanfeti</ta>
            <ta e="T930" id="Seg_8059" s="T929">det-bi-jəʔ</ta>
            <ta e="T931" id="Seg_8060" s="T930">tănan</ta>
            <ta e="T932" id="Seg_8061" s="T931">kujnek</ta>
            <ta e="T933" id="Seg_8062" s="T932">padʼi</ta>
            <ta e="T934" id="Seg_8063" s="T933">det-bi-jəʔ</ta>
            <ta e="T935" id="Seg_8064" s="T934">piʔme</ta>
            <ta e="T936" id="Seg_8065" s="T935">padʼi</ta>
            <ta e="T937" id="Seg_8066" s="T936">det-bi-jəʔ</ta>
            <ta e="T938" id="Seg_8067" s="T937">tănan</ta>
            <ta e="T939" id="Seg_8068" s="T938">predsʼedatʼelʼ-n</ta>
            <ta e="T940" id="Seg_8069" s="T939">tura-t</ta>
            <ta e="T941" id="Seg_8070" s="T940">urgo</ta>
            <ta e="T942" id="Seg_8071" s="T941">il</ta>
            <ta e="T943" id="Seg_8072" s="T942">iʔgö</ta>
            <ta e="T946" id="Seg_8073" s="T945">šide</ta>
            <ta e="T947" id="Seg_8074" s="T946">nʼi</ta>
            <ta e="T948" id="Seg_8075" s="T947">šide</ta>
            <ta e="T949" id="Seg_8076" s="T948">koʔbdo</ta>
            <ta e="T950" id="Seg_8077" s="T949">nüke</ta>
            <ta e="T951" id="Seg_8078" s="T950">büzʼe-jəʔ-ziʔ</ta>
            <ta e="T952" id="Seg_8079" s="T951">bos-də</ta>
            <ta e="T953" id="Seg_8080" s="T952">ne</ta>
            <ta e="T955" id="Seg_8081" s="T954">men-zAŋ-də</ta>
            <ta e="T956" id="Seg_8082" s="T955">šide</ta>
            <ta e="T957" id="Seg_8083" s="T956">onʼiʔ</ta>
            <ta e="T958" id="Seg_8084" s="T957">jakšə</ta>
            <ta e="T959" id="Seg_8085" s="T958">men</ta>
            <ta e="T960" id="Seg_8086" s="T959">onʼiʔ</ta>
            <ta e="T961" id="Seg_8087" s="T960">nagur</ta>
            <ta e="T962" id="Seg_8088" s="T961">üjü</ta>
            <ta e="T963" id="Seg_8089" s="T962">a</ta>
            <ta e="T964" id="Seg_8090" s="T963">teʔdə</ta>
            <ta e="T965" id="Seg_8091" s="T964">üjü</ta>
            <ta e="T966" id="Seg_8092" s="T965">naga</ta>
            <ta e="T970" id="Seg_8093" s="T969">tăn</ta>
            <ta e="T971" id="Seg_8094" s="T970">măna</ta>
            <ta e="T972" id="Seg_8095" s="T971">münör-ə-ʔ</ta>
            <ta e="T973" id="Seg_8096" s="T972">a</ta>
            <ta e="T974" id="Seg_8097" s="T973">ĭmbi-ziʔ</ta>
            <ta e="T975" id="Seg_8098" s="T974">münör-laʔbə-l</ta>
            <ta e="T976" id="Seg_8099" s="T975">muzuruk-ziʔ</ta>
            <ta e="T977" id="Seg_8100" s="T976">e-ʔ</ta>
            <ta e="T978" id="Seg_8101" s="T977">münör-ə-ʔ</ta>
            <ta e="T979" id="Seg_8102" s="T978">ato</ta>
            <ta e="T980" id="Seg_8103" s="T979">măna</ta>
            <ta e="T981" id="Seg_8104" s="T980">ĭzem-lV-j</ta>
            <ta e="T982" id="Seg_8105" s="T981">bar</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PKZ">
            <ta e="T15" id="Seg_8106" s="T14">forest-LAT</ta>
            <ta e="T16" id="Seg_8107" s="T15">today</ta>
            <ta e="T17" id="Seg_8108" s="T16">go-FUT-1SG</ta>
            <ta e="T20" id="Seg_8109" s="T19">horse-INS</ta>
            <ta e="T21" id="Seg_8110" s="T20">where</ta>
            <ta e="T22" id="Seg_8111" s="T21">sack.[NOM.SG]</ta>
            <ta e="T23" id="Seg_8112" s="T22">one.should</ta>
            <ta e="T24" id="Seg_8113" s="T23">bread.[NOM.SG]</ta>
            <ta e="T25" id="Seg_8114" s="T24">put-INF.LAT</ta>
            <ta e="T26" id="Seg_8115" s="T25">salt.[NOM.SG]</ta>
            <ta e="T27" id="Seg_8116" s="T26">put-INF.LAT</ta>
            <ta e="T28" id="Seg_8117" s="T27">spoon.[NOM.SG]</ta>
            <ta e="T29" id="Seg_8118" s="T28">one.should</ta>
            <ta e="T30" id="Seg_8119" s="T29">put-INF.LAT</ta>
            <ta e="T31" id="Seg_8120" s="T30">one.should</ta>
            <ta e="T32" id="Seg_8121" s="T31">meat.[NOM.SG]</ta>
            <ta e="T33" id="Seg_8122" s="T32">put-INF.LAT</ta>
            <ta e="T34" id="Seg_8123" s="T33">one.should</ta>
            <ta e="T35" id="Seg_8124" s="T34">bread.[NOM.SG]</ta>
            <ta e="T36" id="Seg_8125" s="T35">put-INF.LAT</ta>
            <ta e="T49" id="Seg_8126" s="T46">bread.[NOM.SG]</ta>
            <ta e="T50" id="Seg_8127" s="T49">put-INF.LAT</ta>
            <ta e="T51" id="Seg_8128" s="T50">one.should</ta>
            <ta e="T52" id="Seg_8129" s="T51">butter.[NOM.SG]</ta>
            <ta e="T53" id="Seg_8130" s="T52">put-INF.LAT</ta>
            <ta e="T54" id="Seg_8131" s="T53">one.should</ta>
            <ta e="T55" id="Seg_8132" s="T54">egg-PL</ta>
            <ta e="T56" id="Seg_8133" s="T55">put-INF.LAT</ta>
            <ta e="T57" id="Seg_8134" s="T56">spoon.[NOM.SG]</ta>
            <ta e="T58" id="Seg_8135" s="T57">one.should</ta>
            <ta e="T59" id="Seg_8136" s="T58">put-INF.LAT</ta>
            <ta e="T60" id="Seg_8137" s="T59">one.should</ta>
            <ta e="T61" id="Seg_8138" s="T60">cauldron.[NOM.SG]</ta>
            <ta e="T62" id="Seg_8139" s="T61">put-INF.LAT</ta>
            <ta e="T63" id="Seg_8140" s="T62">dog.[NOM.SG]</ta>
            <ta e="T64" id="Seg_8141" s="T63">take-INF.LAT</ta>
            <ta e="T65" id="Seg_8142" s="T64">one.should</ta>
            <ta e="T66" id="Seg_8143" s="T65">gun</ta>
            <ta e="T67" id="Seg_8144" s="T66">take-INF.LAT</ta>
            <ta e="T68" id="Seg_8145" s="T67">one.should</ta>
            <ta e="T69" id="Seg_8146" s="T68">one.should</ta>
            <ta e="T70" id="Seg_8147" s="T69">axe.[NOM.SG]</ta>
            <ta e="T71" id="Seg_8148" s="T70">take-INF.LAT</ta>
            <ta e="T72" id="Seg_8149" s="T71">knife</ta>
            <ta e="T73" id="Seg_8150" s="T72">take-INF.LAT</ta>
            <ta e="T74" id="Seg_8151" s="T73">one.should</ta>
            <ta e="T86" id="Seg_8152" s="T85">%%</ta>
            <ta e="T91" id="Seg_8153" s="T90">well</ta>
            <ta e="T92" id="Seg_8154" s="T91">man-DYA-DU</ta>
            <ta e="T93" id="Seg_8155" s="T92">come-PRS-3PL</ta>
            <ta e="T94" id="Seg_8156" s="T93">speak-INF.LAT</ta>
            <ta e="T95" id="Seg_8157" s="T94">sit-IMP.2SG</ta>
            <ta e="T96" id="Seg_8158" s="T95">speak-FUT-1PL</ta>
            <ta e="T97" id="Seg_8159" s="T96">eat-INF.LAT</ta>
            <ta e="T98" id="Seg_8160" s="T97">probably</ta>
            <ta e="T99" id="Seg_8161" s="T98">one.wants</ta>
            <ta e="T100" id="Seg_8162" s="T99">sit-IMP.2SG</ta>
            <ta e="T101" id="Seg_8163" s="T100">eat-EP-IMP.2SG</ta>
            <ta e="T102" id="Seg_8164" s="T101">there</ta>
            <ta e="T103" id="Seg_8165" s="T102">bread.[NOM.SG]</ta>
            <ta e="T104" id="Seg_8166" s="T103">lie-DUR.[3SG]</ta>
            <ta e="T105" id="Seg_8167" s="T104">butter</ta>
            <ta e="T106" id="Seg_8168" s="T105">stand-PRS.[3SG]</ta>
            <ta e="T107" id="Seg_8169" s="T106">spoon.[NOM.SG]</ta>
            <ta e="T108" id="Seg_8170" s="T107">be-PRS.[3SG]</ta>
            <ta e="T109" id="Seg_8171" s="T108">salt</ta>
            <ta e="T110" id="Seg_8172" s="T109">be-PRS.[3SG]</ta>
            <ta e="T111" id="Seg_8173" s="T110">egg-PL</ta>
            <ta e="T112" id="Seg_8174" s="T111">meat.[NOM.SG]</ta>
            <ta e="T113" id="Seg_8175" s="T112">fish.[NOM.SG]</ta>
            <ta e="T114" id="Seg_8176" s="T113">eat-EP-IMP.2SG</ta>
            <ta e="T115" id="Seg_8177" s="T114">all</ta>
            <ta e="T116" id="Seg_8178" s="T115">NEG.AUX-IMP.2SG</ta>
            <ta e="T117" id="Seg_8179" s="T116">shout-CNG</ta>
            <ta e="T118" id="Seg_8180" s="T117">NEG.AUX-IMP.2SG</ta>
            <ta e="T119" id="Seg_8181" s="T118">shout-EP-CNG</ta>
            <ta e="T120" id="Seg_8182" s="T119">otherwise</ta>
            <ta e="T121" id="Seg_8183" s="T120">beat-FUT-1SG</ta>
            <ta e="T122" id="Seg_8184" s="T121">you.DAT</ta>
            <ta e="T123" id="Seg_8185" s="T122">NEG.AUX-IMP.2SG</ta>
            <ta e="T124" id="Seg_8186" s="T123">cry-EP-CNG</ta>
            <ta e="T125" id="Seg_8187" s="T124">NEG.AUX-IMP.2SG</ta>
            <ta e="T126" id="Seg_8188" s="T125">be.angry-FACT-EP-CNG</ta>
            <ta e="T127" id="Seg_8189" s="T126">NEG.AUX-IMP.2SG</ta>
            <ta e="T128" id="Seg_8190" s="T127">lie-EP-CNG</ta>
            <ta e="T129" id="Seg_8191" s="T128">sit-DUR-2SG</ta>
            <ta e="T130" id="Seg_8192" s="T129">all</ta>
            <ta e="T131" id="Seg_8193" s="T130">lie-DUR-2SG</ta>
            <ta e="T132" id="Seg_8194" s="T131">you.GEN</ta>
            <ta e="T133" id="Seg_8195" s="T132">very</ta>
            <ta e="T134" id="Seg_8196" s="T133">many</ta>
            <ta e="T135" id="Seg_8197" s="T134">lie-DUR-2SG</ta>
            <ta e="T138" id="Seg_8198" s="T137">I.NOM</ta>
            <ta e="T139" id="Seg_8199" s="T138">soon</ta>
            <ta e="T140" id="Seg_8200" s="T139">go-FUT-1SG</ta>
            <ta e="T141" id="Seg_8201" s="T140">%%-LAT</ta>
            <ta e="T142" id="Seg_8202" s="T141">and</ta>
            <ta e="T143" id="Seg_8203" s="T142">you.GEN</ta>
            <ta e="T144" id="Seg_8204" s="T143">tent-LAT-2SG</ta>
            <ta e="T145" id="Seg_8205" s="T144">sit-IMP.2SG</ta>
            <ta e="T146" id="Seg_8206" s="T145">NEG.AUX-IMP.2SG</ta>
            <ta e="T147" id="Seg_8207" s="T146">shout-CNG</ta>
            <ta e="T148" id="Seg_8208" s="T147">I.NOM</ta>
            <ta e="T149" id="Seg_8209" s="T148">what=INDEF</ta>
            <ta e="T150" id="Seg_8210" s="T149">you.DAT</ta>
            <ta e="T151" id="Seg_8211" s="T150">bring-FUT-1SG</ta>
            <ta e="T152" id="Seg_8212" s="T151">then</ta>
            <ta e="T153" id="Seg_8213" s="T152">give-FUT-1SG</ta>
            <ta e="T154" id="Seg_8214" s="T153">you.DAT</ta>
            <ta e="T155" id="Seg_8215" s="T154">yesterday</ta>
            <ta e="T158" id="Seg_8216" s="T157">yesterday</ta>
            <ta e="T159" id="Seg_8217" s="T158">come-PST.[3SG]</ta>
            <ta e="T160" id="Seg_8218" s="T159">I.NOM</ta>
            <ta e="T161" id="Seg_8219" s="T160">relative-NOM/GEN/ACC.1SG</ta>
            <ta e="T162" id="Seg_8220" s="T161">I.NOM</ta>
            <ta e="T163" id="Seg_8221" s="T162">say-PST-1SG</ta>
            <ta e="T164" id="Seg_8222" s="T163">lie-IMP.2SG</ta>
            <ta e="T165" id="Seg_8223" s="T164">sleep-INF.LAT</ta>
            <ta e="T166" id="Seg_8224" s="T165">then</ta>
            <ta e="T167" id="Seg_8225" s="T166">say-PST-1SG</ta>
            <ta e="T168" id="Seg_8226" s="T167">eat-EP-IMP.2SG</ta>
            <ta e="T169" id="Seg_8227" s="T168">probably</ta>
            <ta e="T170" id="Seg_8228" s="T169">eat-INF.LAT</ta>
            <ta e="T171" id="Seg_8229" s="T170">one.wants</ta>
            <ta e="T172" id="Seg_8230" s="T171">this.[NOM.SG]</ta>
            <ta e="T173" id="Seg_8231" s="T172">say-PST.[3SG]</ta>
            <ta e="T174" id="Seg_8232" s="T173">I.NOM</ta>
            <ta e="T175" id="Seg_8233" s="T174">I.NOM</ta>
            <ta e="T176" id="Seg_8234" s="T175">NEG</ta>
            <ta e="T179" id="Seg_8235" s="T178">eat-INF.LAT</ta>
            <ta e="T181" id="Seg_8236" s="T180">I.NOM</ta>
            <ta e="T182" id="Seg_8237" s="T181">house-LOC</ta>
            <ta e="T183" id="Seg_8238" s="T182">eat-PST-1SG</ta>
            <ta e="T184" id="Seg_8239" s="T183">there</ta>
            <ta e="T185" id="Seg_8240" s="T184">then</ta>
            <ta e="T188" id="Seg_8241" s="T187">lie-PST.[3SG]</ta>
            <ta e="T189" id="Seg_8242" s="T188">sleep-INF.LAT</ta>
            <ta e="T190" id="Seg_8243" s="T189">I.NOM</ta>
            <ta e="T191" id="Seg_8244" s="T190">lie-PST-1SG</ta>
            <ta e="T192" id="Seg_8245" s="T191">morning-LOC.ADV</ta>
            <ta e="T193" id="Seg_8246" s="T192">get.up-PST-1SG</ta>
            <ta e="T194" id="Seg_8247" s="T193">%%-LAT</ta>
            <ta e="T195" id="Seg_8248" s="T194">go-PST-1SG</ta>
            <ta e="T196" id="Seg_8249" s="T195">come-PST-1SG</ta>
            <ta e="T197" id="Seg_8250" s="T196">this-LAT</ta>
            <ta e="T198" id="Seg_8251" s="T197">say-PST-1SG</ta>
            <ta e="T199" id="Seg_8252" s="T198">get.up-IMP.2SG</ta>
            <ta e="T200" id="Seg_8253" s="T199">eat-EP-IMP.2SG</ta>
            <ta e="T201" id="Seg_8254" s="T200">there</ta>
            <ta e="T202" id="Seg_8255" s="T201">sour.[NOM.SG]</ta>
            <ta e="T206" id="Seg_8256" s="T205">milk.[NOM.SG]</ta>
            <ta e="T207" id="Seg_8257" s="T206">be-PRS.[3SG]</ta>
            <ta e="T208" id="Seg_8258" s="T207">berry</ta>
            <ta e="T209" id="Seg_8259" s="T208">be-PRS.[3SG]</ta>
            <ta e="T210" id="Seg_8260" s="T209">egg-PL</ta>
            <ta e="T211" id="Seg_8261" s="T210">bread.[NOM.SG]</ta>
            <ta e="T212" id="Seg_8262" s="T211">eat-EP-IMP.2SG</ta>
            <ta e="T213" id="Seg_8263" s="T212">sit-IMP.2SG</ta>
            <ta e="T214" id="Seg_8264" s="T213">this.[NOM.SG]</ta>
            <ta e="T217" id="Seg_8265" s="T216">this.[NOM.SG]</ta>
            <ta e="T218" id="Seg_8266" s="T217">get.up-PST.[3SG]</ta>
            <ta e="T219" id="Seg_8267" s="T218">wash.oneself-DES-PST.[3SG]</ta>
            <ta e="T220" id="Seg_8268" s="T219">then</ta>
            <ta e="T221" id="Seg_8269" s="T220">sit-PST.[3SG]</ta>
            <ta e="T222" id="Seg_8270" s="T221">eat-INF.LAT</ta>
            <ta e="T224" id="Seg_8271" s="T223">then</ta>
            <ta e="T225" id="Seg_8272" s="T224">go-CVB</ta>
            <ta e="T226" id="Seg_8273" s="T225">disappear-PST.[3SG]</ta>
            <ta e="T227" id="Seg_8274" s="T226">and</ta>
            <ta e="T228" id="Seg_8275" s="T227">I.NOM</ta>
            <ta e="T229" id="Seg_8276" s="T228">wash.oneself-PST-1SG</ta>
            <ta e="T233" id="Seg_8277" s="T232">yesterday</ta>
            <ta e="T234" id="Seg_8278" s="T233">I.NOM</ta>
            <ta e="T235" id="Seg_8279" s="T234">we.NOM</ta>
            <ta e="T236" id="Seg_8280" s="T235">%%</ta>
            <ta e="T238" id="Seg_8281" s="T237">plough-PST-1PL</ta>
            <ta e="T239" id="Seg_8282" s="T238">horse-INS</ta>
            <ta e="T240" id="Seg_8283" s="T239">two.[NOM.SG]</ta>
            <ta e="T241" id="Seg_8284" s="T240">girl.[NOM.SG]</ta>
            <ta e="T242" id="Seg_8285" s="T241">put-PST-3PL</ta>
            <ta e="T243" id="Seg_8286" s="T242">single.[NOM.SG]</ta>
            <ta e="T244" id="Seg_8287" s="T243">man.[NOM.SG]</ta>
            <ta e="T245" id="Seg_8288" s="T244">I.NOM</ta>
            <ta e="T246" id="Seg_8289" s="T245">self-1SG</ta>
            <ta e="T249" id="Seg_8290" s="T248">put-PST-1SG</ta>
            <ta e="T250" id="Seg_8291" s="T249">plough-PST-1PL</ta>
            <ta e="T257" id="Seg_8292" s="T256">I.NOM</ta>
            <ta e="T258" id="Seg_8293" s="T257">tomorrow</ta>
            <ta e="T259" id="Seg_8294" s="T258">sauna-LAT</ta>
            <ta e="T260" id="Seg_8295" s="T259">go-PST-1SG</ta>
            <ta e="T261" id="Seg_8296" s="T260">wash.oneself-PST-1SG</ta>
            <ta e="T262" id="Seg_8297" s="T261">cold.[NOM.SG]</ta>
            <ta e="T263" id="Seg_8298" s="T262">water.[NOM.SG]</ta>
            <ta e="T264" id="Seg_8299" s="T263">be-PST.[3SG]</ta>
            <ta e="T265" id="Seg_8300" s="T264">warm.[NOM.SG]</ta>
            <ta e="T266" id="Seg_8301" s="T265">water.[NOM.SG]</ta>
            <ta e="T267" id="Seg_8302" s="T266">soap</ta>
            <ta e="T268" id="Seg_8303" s="T267">take-PST-1SG</ta>
            <ta e="T269" id="Seg_8304" s="T268">shirt.[NOM.SG]</ta>
            <ta e="T270" id="Seg_8305" s="T269">take-PST-1SG</ta>
            <ta e="T271" id="Seg_8306" s="T270">sauna-ABL</ta>
            <ta e="T272" id="Seg_8307" s="T271">come-PST-1SG</ta>
            <ta e="T273" id="Seg_8308" s="T272">cow.[NOM.SG]</ta>
            <ta e="T274" id="Seg_8309" s="T273">come-PST.[3SG]</ta>
            <ta e="T275" id="Seg_8310" s="T274">I.NOM</ta>
            <ta e="T276" id="Seg_8311" s="T275">milk-PST-1SG</ta>
            <ta e="T277" id="Seg_8312" s="T276">very</ta>
            <ta e="T278" id="Seg_8313" s="T277">get.tired-MOM-PST-1SG</ta>
            <ta e="T279" id="Seg_8314" s="T278">fall-MOM-PST-1SG</ta>
            <ta e="T280" id="Seg_8315" s="T279">and</ta>
            <ta e="T281" id="Seg_8316" s="T280">and</ta>
            <ta e="T282" id="Seg_8317" s="T281">sleep-PST-1SG</ta>
            <ta e="T283" id="Seg_8318" s="T282">today</ta>
            <ta e="T284" id="Seg_8319" s="T283">%%</ta>
            <ta e="T285" id="Seg_8320" s="T284">I.NOM</ta>
            <ta e="T286" id="Seg_8321" s="T285">go-FUT-1SG</ta>
            <ta e="T287" id="Seg_8322" s="T286">mushroom-VBLZ-CVB</ta>
            <ta e="T288" id="Seg_8323" s="T287">mushroom.[NOM.SG]</ta>
            <ta e="T289" id="Seg_8324" s="T288">bring-FUT-1SG</ta>
            <ta e="T290" id="Seg_8325" s="T289">then</ta>
            <ta e="T291" id="Seg_8326" s="T290">wash-FUT-1SG</ta>
            <ta e="T292" id="Seg_8327" s="T291">salt-FUT-1SG</ta>
            <ta e="T293" id="Seg_8328" s="T292">PTCL</ta>
            <ta e="T294" id="Seg_8329" s="T293">then</ta>
            <ta e="T295" id="Seg_8330" s="T294">salt-FUT-3SG</ta>
            <ta e="T296" id="Seg_8331" s="T295">all</ta>
            <ta e="T297" id="Seg_8332" s="T296">then</ta>
            <ta e="T298" id="Seg_8333" s="T297">eat-INF.LAT</ta>
            <ta e="T299" id="Seg_8334" s="T298">and</ta>
            <ta e="T300" id="Seg_8335" s="T299">one.can</ta>
            <ta e="T301" id="Seg_8336" s="T300">chief.[NOM.SG]</ta>
            <ta e="T302" id="Seg_8337" s="T301">all</ta>
            <ta e="T303" id="Seg_8338" s="T302">sit-DUR-PST.[3SG]</ta>
            <ta e="T304" id="Seg_8339" s="T303">all</ta>
            <ta e="T305" id="Seg_8340" s="T304">very</ta>
            <ta e="T306" id="Seg_8341" s="T305">drunk.[NOM.SG]</ta>
            <ta e="T307" id="Seg_8342" s="T306">vodka.[NOM.SG]</ta>
            <ta e="T308" id="Seg_8343" s="T307">drink-PST.[3SG]</ta>
            <ta e="T309" id="Seg_8344" s="T308">very</ta>
            <ta e="T310" id="Seg_8345" s="T309">many</ta>
            <ta e="T311" id="Seg_8346" s="T310">drink-PST.[3SG]</ta>
            <ta e="T312" id="Seg_8347" s="T311">fall-MOM-PST.[3SG]</ta>
            <ta e="T313" id="Seg_8348" s="T312">all</ta>
            <ta e="T314" id="Seg_8349" s="T313">various</ta>
            <ta e="T316" id="Seg_8350" s="T315">scold-DES-DUR.[3SG]</ta>
            <ta e="T317" id="Seg_8351" s="T316">I.NOM</ta>
            <ta e="T318" id="Seg_8352" s="T317">this-ACC</ta>
            <ta e="T319" id="Seg_8353" s="T318">seat-PST-1SG</ta>
            <ta e="T320" id="Seg_8354" s="T319">sit-IMP.2SG</ta>
            <ta e="T321" id="Seg_8355" s="T320">say-PST-1SG</ta>
            <ta e="T322" id="Seg_8356" s="T321">I.NOM</ta>
            <ta e="T323" id="Seg_8357" s="T322">say-PST-1SG</ta>
            <ta e="T324" id="Seg_8358" s="T323">NEG.AUX-IMP.2SG</ta>
            <ta e="T325" id="Seg_8359" s="T324">scold-DES-CNG</ta>
            <ta e="T326" id="Seg_8360" s="T325">sit-IMP.2SG</ta>
            <ta e="T327" id="Seg_8361" s="T326">sit-IMP.2SG</ta>
            <ta e="T328" id="Seg_8362" s="T327">yellow</ta>
            <ta e="T329" id="Seg_8363" s="T328">water.[NOM.SG]</ta>
            <ta e="T330" id="Seg_8364" s="T329">drink-INF.LAT</ta>
            <ta e="T331" id="Seg_8365" s="T330">sugar</ta>
            <ta e="T332" id="Seg_8366" s="T331">lie-DUR.[3SG]</ta>
            <ta e="T333" id="Seg_8367" s="T332">put-IMP.2SG.O</ta>
            <ta e="T334" id="Seg_8368" s="T333">yellow.[NOM.SG]</ta>
            <ta e="T335" id="Seg_8369" s="T334">water-LAT</ta>
            <ta e="T336" id="Seg_8370" s="T335">egg-PL</ta>
            <ta e="T337" id="Seg_8371" s="T336">lie-DUR-3PL</ta>
            <ta e="T340" id="Seg_8372" s="T339">bread.[NOM.SG]</ta>
            <ta e="T341" id="Seg_8373" s="T340">eat-EP-IMP.2SG</ta>
            <ta e="T342" id="Seg_8374" s="T341">meat.[NOM.SG]</ta>
            <ta e="T343" id="Seg_8375" s="T342">eat-EP-IMP.2SG</ta>
            <ta e="T344" id="Seg_8376" s="T343">fish</ta>
            <ta e="T345" id="Seg_8377" s="T344">eat-EP-IMP.2SG</ta>
            <ta e="T346" id="Seg_8378" s="T345">egg</ta>
            <ta e="T347" id="Seg_8379" s="T346">lie-DUR.[3SG]</ta>
            <ta e="T348" id="Seg_8380" s="T347">eat-EP-IMP.2SG</ta>
            <ta e="T349" id="Seg_8381" s="T348">butter.[NOM.SG]</ta>
            <ta e="T350" id="Seg_8382" s="T349">eat-EP-IMP.2SG</ta>
            <ta e="T351" id="Seg_8383" s="T350">cream.[NOM.SG]</ta>
            <ta e="T352" id="Seg_8384" s="T351">eat-EP-IMP.2SG</ta>
            <ta e="T353" id="Seg_8385" s="T352">sour.[NOM.SG]</ta>
            <ta e="T354" id="Seg_8386" s="T353">milk.[NOM.SG]</ta>
            <ta e="T355" id="Seg_8387" s="T354">eat-EP-IMP.2SG</ta>
            <ta e="T356" id="Seg_8388" s="T355">fish.[NOM.SG]</ta>
            <ta e="T357" id="Seg_8389" s="T356">eat-EP-IMP.2SG</ta>
            <ta e="T359" id="Seg_8390" s="T358">one.should</ta>
            <ta e="T360" id="Seg_8391" s="T359">I.LAT</ta>
            <ta e="T361" id="Seg_8392" s="T360">boot.[NOM.SG]</ta>
            <ta e="T362" id="Seg_8393" s="T361">sew-INF.LAT</ta>
            <ta e="T363" id="Seg_8394" s="T362">fur.coat.[NOM.SG]</ta>
            <ta e="T364" id="Seg_8395" s="T363">sew-INF.LAT</ta>
            <ta e="T365" id="Seg_8396" s="T364">one.should</ta>
            <ta e="T368" id="Seg_8397" s="T367">shirt.[NOM.SG]</ta>
            <ta e="T369" id="Seg_8398" s="T368">sew-INF.LAT</ta>
            <ta e="T370" id="Seg_8399" s="T369">take-INF.LAT</ta>
            <ta e="T373" id="Seg_8400" s="T372">one.should</ta>
            <ta e="T374" id="Seg_8401" s="T373">pants.[NOM.SG]</ta>
            <ta e="T375" id="Seg_8402" s="T374">sew-INF.LAT</ta>
            <ta e="T376" id="Seg_8403" s="T375">otherwise</ta>
            <ta e="T377" id="Seg_8404" s="T376">I.NOM</ta>
            <ta e="T378" id="Seg_8405" s="T377">boy-NOM/GEN/ACC.1SG</ta>
            <ta e="T379" id="Seg_8406" s="T378">all</ta>
            <ta e="T380" id="Seg_8407" s="T379">dress-INF.LAT</ta>
            <ta e="T381" id="Seg_8408" s="T380">NEG.EX.[3SG]</ta>
            <ta e="T382" id="Seg_8409" s="T381">what</ta>
            <ta e="T383" id="Seg_8410" s="T382">what.[NOM.SG]=INDEF</ta>
            <ta e="T384" id="Seg_8411" s="T383">NEG.EX.[3SG]</ta>
            <ta e="T385" id="Seg_8412" s="T384">I.NOM</ta>
            <ta e="T386" id="Seg_8413" s="T385">go-FUT-1SG</ta>
            <ta e="T387" id="Seg_8414" s="T386">Aginskoe</ta>
            <ta e="T388" id="Seg_8415" s="T387">settlement-LAT</ta>
            <ta e="T389" id="Seg_8416" s="T388">and</ta>
            <ta e="T390" id="Seg_8417" s="T389">one.should</ta>
            <ta e="T391" id="Seg_8418" s="T390">bread.[NOM.SG]</ta>
            <ta e="T392" id="Seg_8419" s="T391">take-INF.LAT</ta>
            <ta e="T393" id="Seg_8420" s="T392">one.should</ta>
            <ta e="T394" id="Seg_8421" s="T393">sugar.[NOM.SG]</ta>
            <ta e="T395" id="Seg_8422" s="T394">take-INF.LAT</ta>
            <ta e="T396" id="Seg_8423" s="T395">one.should</ta>
            <ta e="T399" id="Seg_8424" s="T398">gun-NOM/GEN/ACC.3SG</ta>
            <ta e="T400" id="Seg_8425" s="T399">bread.[NOM.SG]</ta>
            <ta e="T401" id="Seg_8426" s="T400">take-INF.LAT</ta>
            <ta e="T402" id="Seg_8427" s="T401">one.should</ta>
            <ta e="T409" id="Seg_8428" s="T408">well</ta>
            <ta e="T410" id="Seg_8429" s="T409">bread.[NOM.SG]</ta>
            <ta e="T416" id="Seg_8430" s="T415">I.NOM</ta>
            <ta e="T417" id="Seg_8431" s="T416">forest-LAT</ta>
            <ta e="T418" id="Seg_8432" s="T417">then</ta>
            <ta e="T419" id="Seg_8433" s="T418">go-FUT-1SG</ta>
            <ta e="T422" id="Seg_8434" s="T421">today</ta>
            <ta e="T425" id="Seg_8435" s="T424">this-PL</ta>
            <ta e="T426" id="Seg_8436" s="T425">forest-ABL</ta>
            <ta e="T427" id="Seg_8437" s="T426">come-FUT-3PL</ta>
            <ta e="T428" id="Seg_8438" s="T427">probably</ta>
            <ta e="T429" id="Seg_8439" s="T428">meat.[NOM.SG]</ta>
            <ta e="T430" id="Seg_8440" s="T429">bring-FUT-3PL</ta>
            <ta e="T431" id="Seg_8441" s="T430">bear.leek.[NOM.SG]</ta>
            <ta e="T432" id="Seg_8442" s="T431">bring-FUT-3PL</ta>
            <ta e="T434" id="Seg_8443" s="T433">stab-INF.LAT</ta>
            <ta e="T435" id="Seg_8444" s="T434">one.should</ta>
            <ta e="T436" id="Seg_8445" s="T435">and</ta>
            <ta e="T437" id="Seg_8446" s="T436">salt-INF.LAT</ta>
            <ta e="T438" id="Seg_8447" s="T437">egg-PL</ta>
            <ta e="T439" id="Seg_8448" s="T438">put-INF.LAT</ta>
            <ta e="T440" id="Seg_8449" s="T439">cream.[NOM.SG]</ta>
            <ta e="T441" id="Seg_8450" s="T440">put-INF.LAT</ta>
            <ta e="T442" id="Seg_8451" s="T441">and</ta>
            <ta e="T443" id="Seg_8452" s="T442">water.[NOM.SG]</ta>
            <ta e="T444" id="Seg_8453" s="T443">pour-INF.LAT</ta>
            <ta e="T445" id="Seg_8454" s="T444">and</ta>
            <ta e="T446" id="Seg_8455" s="T445">eat-INF.LAT</ta>
            <ta e="T447" id="Seg_8456" s="T446">spoon-INS</ta>
            <ta e="T448" id="Seg_8457" s="T447">one.should</ta>
            <ta e="T449" id="Seg_8458" s="T448">go-INF.LAT</ta>
            <ta e="T454" id="Seg_8459" s="T453">go-INF.LAT</ta>
            <ta e="T459" id="Seg_8460" s="T458">plough-INF.LAT</ta>
            <ta e="T460" id="Seg_8461" s="T459">bread.[NOM.SG]</ta>
            <ta e="T461" id="Seg_8462" s="T460">sprinkle-INF.LAT</ta>
            <ta e="T462" id="Seg_8463" s="T461">then</ta>
            <ta e="T463" id="Seg_8464" s="T462">grow-FUT-3SG</ta>
            <ta e="T464" id="Seg_8465" s="T463">put-INF.LAT</ta>
            <ta e="T465" id="Seg_8466" s="T464">one.should</ta>
            <ta e="T466" id="Seg_8467" s="T465">then</ta>
            <ta e="T467" id="Seg_8468" s="T466">jolt-MULT-INF.LAT</ta>
            <ta e="T468" id="Seg_8469" s="T467">one.should</ta>
            <ta e="T469" id="Seg_8470" s="T468">then</ta>
            <ta e="T476" id="Seg_8471" s="T475">then</ta>
            <ta e="T477" id="Seg_8472" s="T476">pull-INF.LAT</ta>
            <ta e="T478" id="Seg_8473" s="T477">one.should</ta>
            <ta e="T481" id="Seg_8474" s="T480">then</ta>
            <ta e="T482" id="Seg_8475" s="T481">one.should</ta>
            <ta e="T483" id="Seg_8476" s="T482">bake-INF.LAT</ta>
            <ta e="T484" id="Seg_8477" s="T483">then</ta>
            <ta e="T485" id="Seg_8478" s="T484">eat-INF.LAT</ta>
            <ta e="T486" id="Seg_8479" s="T485">one.should</ta>
            <ta e="T489" id="Seg_8480" s="T488">today</ta>
            <ta e="T490" id="Seg_8481" s="T489">big.[NOM.SG]</ta>
            <ta e="T491" id="Seg_8482" s="T490">day.[NOM.SG]</ta>
            <ta e="T494" id="Seg_8483" s="T493">people.[NOM.SG]</ta>
            <ta e="T495" id="Seg_8484" s="T494">very</ta>
            <ta e="T496" id="Seg_8485" s="T495">many</ta>
            <ta e="T497" id="Seg_8486" s="T496">beautiful-PL</ta>
            <ta e="T498" id="Seg_8487" s="T497">all</ta>
            <ta e="T499" id="Seg_8488" s="T498">vodka.[NOM.SG]</ta>
            <ta e="T500" id="Seg_8489" s="T499">drink-DUR-3PL</ta>
            <ta e="T501" id="Seg_8490" s="T500">play-DUR-3PL</ta>
            <ta e="T503" id="Seg_8491" s="T502">jump-DUR-3PL</ta>
            <ta e="T504" id="Seg_8492" s="T503">song</ta>
            <ta e="T505" id="Seg_8493" s="T504">sing-DUR-3PL</ta>
            <ta e="T506" id="Seg_8494" s="T505">good.[NOM.SG]</ta>
            <ta e="T507" id="Seg_8495" s="T506">woman.[NOM.SG]</ta>
            <ta e="T508" id="Seg_8496" s="T507">bread.[NOM.SG]</ta>
            <ta e="T509" id="Seg_8497" s="T508">all</ta>
            <ta e="T512" id="Seg_8498" s="T511">bake-PST.[3SG]</ta>
            <ta e="T513" id="Seg_8499" s="T512">good.[NOM.SG]</ta>
            <ta e="T514" id="Seg_8500" s="T513">soft.[NOM.SG]</ta>
            <ta e="T515" id="Seg_8501" s="T514">soon</ta>
            <ta e="T520" id="Seg_8502" s="T519">eat-IMP.2PL</ta>
            <ta e="T521" id="Seg_8503" s="T520">probably</ta>
            <ta e="T522" id="Seg_8504" s="T521">be.hungry-PRS-3PL</ta>
            <ta e="T523" id="Seg_8505" s="T522">soon</ta>
            <ta e="T524" id="Seg_8506" s="T523">get.up-FUT-1SG</ta>
            <ta e="T525" id="Seg_8507" s="T524">eat-IMP.2PL</ta>
            <ta e="T526" id="Seg_8508" s="T525">sit</ta>
            <ta e="T527" id="Seg_8509" s="T526">sit-IMP.2PL</ta>
            <ta e="T528" id="Seg_8510" s="T527">thick.[NOM.SG]</ta>
            <ta e="T529" id="Seg_8511" s="T528">man.[NOM.SG]</ta>
            <ta e="T530" id="Seg_8512" s="T529">eat-PST.[3SG]</ta>
            <ta e="T531" id="Seg_8513" s="T530">bread.[NOM.SG]</ta>
            <ta e="T532" id="Seg_8514" s="T531">bird.cherry</ta>
            <ta e="T533" id="Seg_8515" s="T532">berry-INS</ta>
            <ta e="T534" id="Seg_8516" s="T533">cream.[NOM.SG]</ta>
            <ta e="T535" id="Seg_8517" s="T534">place-PST-1SG</ta>
            <ta e="T536" id="Seg_8518" s="T535">eat-IMP.2PL</ta>
            <ta e="T538" id="Seg_8519" s="T537">milk.[NOM.SG]</ta>
            <ta e="T541" id="Seg_8520" s="T540">drink-IMP.2SG.O</ta>
            <ta e="T544" id="Seg_8521" s="T543">you.GEN</ta>
            <ta e="T545" id="Seg_8522" s="T544">more</ta>
            <ta e="T546" id="Seg_8523" s="T545">sleep-DUR-2SG</ta>
            <ta e="T547" id="Seg_8524" s="T546">get.up-IMP.2SG</ta>
            <ta e="T548" id="Seg_8525" s="T547">otherwise</ta>
            <ta e="T549" id="Seg_8526" s="T548">cold.[NOM.SG]</ta>
            <ta e="T550" id="Seg_8527" s="T549">water.[NOM.SG]</ta>
            <ta e="T551" id="Seg_8528" s="T550">bring-FUT-1SG</ta>
            <ta e="T552" id="Seg_8529" s="T551">you.DAT</ta>
            <ta e="T553" id="Seg_8530" s="T552">pour-FUT-1SG</ta>
            <ta e="T554" id="Seg_8531" s="T553">all</ta>
            <ta e="T555" id="Seg_8532" s="T554">then</ta>
            <ta e="T556" id="Seg_8533" s="T555">all</ta>
            <ta e="T557" id="Seg_8534" s="T556">jump-MOM-FUT-FRQ</ta>
            <ta e="T567" id="Seg_8535" s="T566">NEG</ta>
            <ta e="T568" id="Seg_8536" s="T567">listen-IMP.2SG.O</ta>
            <ta e="T569" id="Seg_8537" s="T568">NEG.AUX-IMP.2SG</ta>
            <ta e="T570" id="Seg_8538" s="T569">cut-CNG</ta>
            <ta e="T574" id="Seg_8539" s="T573">NEG.AUX-IMP.2SG</ta>
            <ta e="T575" id="Seg_8540" s="T574">speak-EP-CNG</ta>
            <ta e="T578" id="Seg_8541" s="T577">NEG.AUX-IMP.2SG</ta>
            <ta e="T579" id="Seg_8542" s="T578">scold-DES-CNG</ta>
            <ta e="T583" id="Seg_8543" s="T582">NEG.AUX-IMP.2SG</ta>
            <ta e="T584" id="Seg_8544" s="T583">shout-EP-CNG</ta>
            <ta e="T587" id="Seg_8545" s="T586">snot-NOM/GEN/ACC.2SG</ta>
            <ta e="T588" id="Seg_8546" s="T587">flow-DUR.[3SG]</ta>
            <ta e="T589" id="Seg_8547" s="T588">wipe-IMP.2SG.O</ta>
            <ta e="T590" id="Seg_8548" s="T589">snot-NOM/GEN/ACC.2SG</ta>
            <ta e="T593" id="Seg_8549" s="T592">NEG.AUX-IMP.2SG</ta>
            <ta e="T594" id="Seg_8550" s="T593">run-CNG</ta>
            <ta e="T595" id="Seg_8551" s="T594">NEG.AUX-IMP.2SG</ta>
            <ta e="T596" id="Seg_8552" s="T595">jump-CNG</ta>
            <ta e="T597" id="Seg_8553" s="T596">NEG.AUX-IMP.2SG</ta>
            <ta e="T598" id="Seg_8554" s="T597">sing-CNG</ta>
            <ta e="T599" id="Seg_8555" s="T598">very</ta>
            <ta e="T600" id="Seg_8556" s="T599">good.[NOM.SG]</ta>
            <ta e="T601" id="Seg_8557" s="T600">very</ta>
            <ta e="T602" id="Seg_8558" s="T601">beautiful.[NOM.SG]</ta>
            <ta e="T603" id="Seg_8559" s="T602">very</ta>
            <ta e="T604" id="Seg_8560" s="T603">red.[NOM.SG]</ta>
            <ta e="T605" id="Seg_8561" s="T604">very</ta>
            <ta e="T607" id="Seg_8562" s="T606">fat</ta>
            <ta e="T608" id="Seg_8563" s="T607">very</ta>
            <ta e="T609" id="Seg_8564" s="T608">long.[NOM.SG]</ta>
            <ta e="T613" id="Seg_8565" s="T612">very</ta>
            <ta e="T614" id="Seg_8566" s="T613">small.[NOM.SG]</ta>
            <ta e="T619" id="Seg_8567" s="T618">probably</ta>
            <ta e="T620" id="Seg_8568" s="T619">scarf.[NOM.SG]</ta>
            <ta e="T621" id="Seg_8569" s="T620">steal-PST-2SG</ta>
            <ta e="T622" id="Seg_8570" s="T621">what.for</ta>
            <ta e="T623" id="Seg_8571" s="T622">bring-PST-2SG</ta>
            <ta e="T624" id="Seg_8572" s="T623">I.NOM</ta>
            <ta e="T625" id="Seg_8573" s="T624">NEG</ta>
            <ta e="T626" id="Seg_8574" s="T625">take-FUT-1SG</ta>
            <ta e="T627" id="Seg_8575" s="T626">not</ta>
            <ta e="T628" id="Seg_8576" s="T627">one.should</ta>
            <ta e="T629" id="Seg_8577" s="T628">steal-INF.LAT</ta>
            <ta e="T630" id="Seg_8578" s="T629">otherwise</ta>
            <ta e="T631" id="Seg_8579" s="T630">NEG</ta>
            <ta e="T632" id="Seg_8580" s="T631">good.[NOM.SG]</ta>
            <ta e="T633" id="Seg_8581" s="T632">become-FUT-3SG</ta>
            <ta e="T634" id="Seg_8582" s="T633">hand-NOM/GEN/ACC.1SG</ta>
            <ta e="T635" id="Seg_8583" s="T634">cut-PST-1SG</ta>
            <ta e="T636" id="Seg_8584" s="T635">all</ta>
            <ta e="T637" id="Seg_8585" s="T636">blood.[NOM.SG]</ta>
            <ta e="T638" id="Seg_8586" s="T637">flow-DUR.[3SG]</ta>
            <ta e="T639" id="Seg_8587" s="T638">bring-IMP.2SG</ta>
            <ta e="T640" id="Seg_8588" s="T639">what=INDEF</ta>
            <ta e="T641" id="Seg_8589" s="T640">hand-NOM/GEN/ACC.1SG</ta>
            <ta e="T642" id="Seg_8590" s="T641">bind-INF.LAT</ta>
            <ta e="T647" id="Seg_8591" s="T645">what.for</ta>
            <ta e="T648" id="Seg_8592" s="T647">knife-INS</ta>
            <ta e="T649" id="Seg_8593" s="T648">knife.[NOM.SG]</ta>
            <ta e="T650" id="Seg_8594" s="T649">take-PST-2SG</ta>
            <ta e="T651" id="Seg_8595" s="T650">you.DAT</ta>
            <ta e="T652" id="Seg_8596" s="T651">more</ta>
            <ta e="T654" id="Seg_8597" s="T652">one.should</ta>
            <ta e="T655" id="Seg_8598" s="T654">wind.[NOM.SG]</ta>
            <ta e="T656" id="Seg_8599" s="T655">rain.[NOM.SG]</ta>
            <ta e="T657" id="Seg_8600" s="T656">all</ta>
            <ta e="T658" id="Seg_8601" s="T657">come-PRS.[3SG]</ta>
            <ta e="T659" id="Seg_8602" s="T658">snow.[NOM.SG]</ta>
            <ta e="T660" id="Seg_8603" s="T659">all</ta>
            <ta e="T661" id="Seg_8604" s="T660">come-PRS.[3SG]</ta>
            <ta e="T662" id="Seg_8605" s="T661">wind-INS</ta>
            <ta e="T663" id="Seg_8606" s="T662">very</ta>
            <ta e="T664" id="Seg_8607" s="T663">cold.[NOM.SG]</ta>
            <ta e="T665" id="Seg_8608" s="T664">I.NOM</ta>
            <ta e="T666" id="Seg_8609" s="T665">very</ta>
            <ta e="T667" id="Seg_8610" s="T666">freeze-PST-1SG</ta>
            <ta e="T668" id="Seg_8611" s="T667">and</ta>
            <ta e="T669" id="Seg_8612" s="T668">strongly</ta>
            <ta e="T670" id="Seg_8613" s="T669">freeze-PST-1SG</ta>
            <ta e="T671" id="Seg_8614" s="T670">foot-NOM/GEN/ACC.1SG</ta>
            <ta e="T672" id="Seg_8615" s="T671">freeze-PST.[3SG]</ta>
            <ta e="T673" id="Seg_8616" s="T672">hand-NOM/GEN/ACC.1SG</ta>
            <ta e="T674" id="Seg_8617" s="T673">freeze-PST.[3SG]</ta>
            <ta e="T675" id="Seg_8618" s="T674">face-NOM/GEN/ACC.1SG</ta>
            <ta e="T676" id="Seg_8619" s="T675">all</ta>
            <ta e="T677" id="Seg_8620" s="T676">freeze-PST.[3SG]</ta>
            <ta e="T678" id="Seg_8621" s="T677">today</ta>
            <ta e="T681" id="Seg_8622" s="T680">beautiful.[NOM.SG]</ta>
            <ta e="T682" id="Seg_8623" s="T681">day.[NOM.SG]</ta>
            <ta e="T683" id="Seg_8624" s="T682">sun.[NOM.SG]</ta>
            <ta e="T684" id="Seg_8625" s="T683">all</ta>
            <ta e="T685" id="Seg_8626" s="T684">shine-DUR.[3SG]</ta>
            <ta e="T686" id="Seg_8627" s="T685">%above</ta>
            <ta e="T687" id="Seg_8628" s="T686">what.[NOM.SG]=INDEF</ta>
            <ta e="T688" id="Seg_8629" s="T687">NEG.EX.[3SG]</ta>
            <ta e="T689" id="Seg_8630" s="T688">very</ta>
            <ta e="T690" id="Seg_8631" s="T689">good.[NOM.SG]</ta>
            <ta e="T691" id="Seg_8632" s="T690">day.[NOM.SG]</ta>
            <ta e="T692" id="Seg_8633" s="T691">people.[NOM.SG]</ta>
            <ta e="T693" id="Seg_8634" s="T692">all</ta>
            <ta e="T694" id="Seg_8635" s="T693">work-DUR-3PL</ta>
            <ta e="T695" id="Seg_8636" s="T694">potato.[NOM.SG]</ta>
            <ta e="T696" id="Seg_8637" s="T695">eat-DUR-3PL</ta>
            <ta e="T697" id="Seg_8638" s="T696">horse-PL</ta>
            <ta e="T698" id="Seg_8639" s="T697">plough-DUR-3PL</ta>
            <ta e="T699" id="Seg_8640" s="T698">all</ta>
            <ta e="T700" id="Seg_8641" s="T699">one.should</ta>
            <ta e="T701" id="Seg_8642" s="T700">go-INF.LAT</ta>
            <ta e="T702" id="Seg_8643" s="T701">grass.[NOM.SG]</ta>
            <ta e="T703" id="Seg_8644" s="T702">cut-INF.LAT</ta>
            <ta e="T704" id="Seg_8645" s="T703">scythe.[NOM.SG]</ta>
            <ta e="T705" id="Seg_8646" s="T704">one.should</ta>
            <ta e="T706" id="Seg_8647" s="T705">take-INF.LAT</ta>
            <ta e="T707" id="Seg_8648" s="T706">one.should</ta>
            <ta e="T708" id="Seg_8649" s="T707">water.[NOM.SG]</ta>
            <ta e="T709" id="Seg_8650" s="T708">take-INF.LAT</ta>
            <ta e="T710" id="Seg_8651" s="T709">milk.[NOM.SG]</ta>
            <ta e="T711" id="Seg_8652" s="T710">take-INF.LAT</ta>
            <ta e="T712" id="Seg_8653" s="T711">one.should</ta>
            <ta e="T713" id="Seg_8654" s="T712">bread.[NOM.SG]</ta>
            <ta e="T714" id="Seg_8655" s="T713">take-INF.LAT</ta>
            <ta e="T715" id="Seg_8656" s="T714">salt</ta>
            <ta e="T716" id="Seg_8657" s="T715">take-INF.LAT</ta>
            <ta e="T717" id="Seg_8658" s="T716">egg-PL</ta>
            <ta e="T718" id="Seg_8659" s="T717">take-INF.LAT</ta>
            <ta e="T719" id="Seg_8660" s="T718">stone.[NOM.SG]</ta>
            <ta e="T720" id="Seg_8661" s="T719">one.should</ta>
            <ta e="T721" id="Seg_8662" s="T720">take-INF.LAT</ta>
            <ta e="T722" id="Seg_8663" s="T721">scythe.[NOM.SG]</ta>
            <ta e="T723" id="Seg_8664" s="T722">sable.[NOM.SG]</ta>
            <ta e="T724" id="Seg_8665" s="T723">today</ta>
            <ta e="T725" id="Seg_8666" s="T724">kill-PST-1SG</ta>
            <ta e="T726" id="Seg_8667" s="T725">squirrel.[NOM.SG]</ta>
            <ta e="T727" id="Seg_8668" s="T726">kill-PST-1SG</ta>
            <ta e="T728" id="Seg_8669" s="T727">bear.[NOM.SG]</ta>
            <ta e="T729" id="Seg_8670" s="T728">kill-PST-1SG</ta>
            <ta e="T730" id="Seg_8671" s="T729">deer.[NOM.SG]</ta>
            <ta e="T731" id="Seg_8672" s="T730">kill-PST-1SG</ta>
            <ta e="T732" id="Seg_8673" s="T731">stallion.[NOM.SG]</ta>
            <ta e="T733" id="Seg_8674" s="T732">kill-PST-1SG</ta>
            <ta e="T734" id="Seg_8675" s="T733">goat.[NOM.SG]</ta>
            <ta e="T735" id="Seg_8676" s="T734">kill-PST-1SG</ta>
            <ta e="T736" id="Seg_8677" s="T735">gun-INS</ta>
            <ta e="T737" id="Seg_8678" s="T736">cow-PL</ta>
            <ta e="T738" id="Seg_8679" s="T737">all</ta>
            <ta e="T739" id="Seg_8680" s="T738">grass.[NOM.SG]</ta>
            <ta e="T740" id="Seg_8681" s="T739">eat-PRS-3PL</ta>
            <ta e="T741" id="Seg_8682" s="T740">go-DUR-3PL</ta>
            <ta e="T742" id="Seg_8683" s="T741">there</ta>
            <ta e="T743" id="Seg_8684" s="T742">this.[NOM.SG]</ta>
            <ta e="T744" id="Seg_8685" s="T743">man.[NOM.SG]</ta>
            <ta e="T745" id="Seg_8686" s="T744">bring-FUT-3SG</ta>
            <ta e="T747" id="Seg_8687" s="T746">tent-LAT/LOC.3SG</ta>
            <ta e="T749" id="Seg_8688" s="T748">one.should</ta>
            <ta e="T750" id="Seg_8689" s="T749">child-NOM/GEN/ACC.1SG</ta>
            <ta e="T751" id="Seg_8690" s="T750">wash-INF.LAT</ta>
            <ta e="T752" id="Seg_8691" s="T751">one.should</ta>
            <ta e="T755" id="Seg_8692" s="T754">child-NOM/GEN/ACC.1SG</ta>
            <ta e="T756" id="Seg_8693" s="T755">all</ta>
            <ta e="T757" id="Seg_8694" s="T756">cry-DUR.[3SG]</ta>
            <ta e="T758" id="Seg_8695" s="T757">one.should</ta>
            <ta e="T759" id="Seg_8696" s="T758">this-ACC</ta>
            <ta e="T760" id="Seg_8697" s="T759">unwrap-INF.LAT</ta>
            <ta e="T761" id="Seg_8698" s="T760">one.should</ta>
            <ta e="T762" id="Seg_8699" s="T761">eat-INF.LAT</ta>
            <ta e="T763" id="Seg_8700" s="T762">give-INF.LAT</ta>
            <ta e="T764" id="Seg_8701" s="T763">one.should</ta>
            <ta e="T765" id="Seg_8702" s="T764">sleep-INF.LAT</ta>
            <ta e="T766" id="Seg_8703" s="T765">put-INF.LAT</ta>
            <ta e="T771" id="Seg_8704" s="T770">lull-IMP.2SG</ta>
            <ta e="T772" id="Seg_8705" s="T771">child-NOM/GEN/ACC.1SG</ta>
            <ta e="T773" id="Seg_8706" s="T772">PTCL</ta>
            <ta e="T774" id="Seg_8707" s="T773">JUSS</ta>
            <ta e="T775" id="Seg_8708" s="T774">sleep-DUR.[3SG]</ta>
            <ta e="T776" id="Seg_8709" s="T775">sit-IMP.2SG</ta>
            <ta e="T777" id="Seg_8710" s="T776">soon</ta>
            <ta e="T778" id="Seg_8711" s="T777">water-VBLZ-CVB</ta>
            <ta e="T779" id="Seg_8712" s="T778">go-FUT-1SG</ta>
            <ta e="T780" id="Seg_8713" s="T779">water.[NOM.SG]</ta>
            <ta e="T781" id="Seg_8714" s="T780">bring-FUT-1SG</ta>
            <ta e="T782" id="Seg_8715" s="T781">then</ta>
            <ta e="T783" id="Seg_8716" s="T782">drink-FUT-2SG</ta>
            <ta e="T784" id="Seg_8717" s="T783">NEG.AUX-IMP.2SG</ta>
            <ta e="T785" id="Seg_8718" s="T784">go-EP-CNG</ta>
            <ta e="T786" id="Seg_8719" s="T785">and</ta>
            <ta e="T787" id="Seg_8720" s="T786">then</ta>
            <ta e="T788" id="Seg_8721" s="T787">go-FUT-2SG</ta>
            <ta e="T789" id="Seg_8722" s="T788">tent-LAT-2SG</ta>
            <ta e="T790" id="Seg_8723" s="T789">soon</ta>
            <ta e="T791" id="Seg_8724" s="T790">blood.[NOM.SG]</ta>
            <ta e="T792" id="Seg_8725" s="T791">bake-FUT-1SG</ta>
            <ta e="T793" id="Seg_8726" s="T792">one.should</ta>
            <ta e="T794" id="Seg_8727" s="T793">there</ta>
            <ta e="T795" id="Seg_8728" s="T794">milk.[NOM.SG]</ta>
            <ta e="T796" id="Seg_8729" s="T795">pour-INF.LAT</ta>
            <ta e="T797" id="Seg_8730" s="T796">butter.[NOM.SG]</ta>
            <ta e="T798" id="Seg_8731" s="T797">put-INF.LAT</ta>
            <ta e="T799" id="Seg_8732" s="T798">or</ta>
            <ta e="T800" id="Seg_8733" s="T799">fat.[NOM.SG]</ta>
            <ta e="T801" id="Seg_8734" s="T800">put-INF.LAT</ta>
            <ta e="T802" id="Seg_8735" s="T801">soon</ta>
            <ta e="T803" id="Seg_8736" s="T802">eat-FUT-1DU</ta>
            <ta e="T804" id="Seg_8737" s="T803">NEG.AUX-IMP.2SG</ta>
            <ta e="T805" id="Seg_8738" s="T804">%%</ta>
            <ta e="T806" id="Seg_8739" s="T805">otherwise</ta>
            <ta e="T807" id="Seg_8740" s="T806">warm.[NOM.SG]</ta>
            <ta e="T810" id="Seg_8741" s="T809">bread-INS</ta>
            <ta e="T811" id="Seg_8742" s="T810">eat-EP-IMP.2SG</ta>
            <ta e="T812" id="Seg_8743" s="T811">girl.[NOM.SG]</ta>
            <ta e="T814" id="Seg_8744" s="T812">come-PST.[3SG]</ta>
            <ta e="T815" id="Seg_8745" s="T814">hair.[NOM.SG]</ta>
            <ta e="T816" id="Seg_8746" s="T815">snow.[NOM.SG]</ta>
            <ta e="T817" id="Seg_8747" s="T816">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T818" id="Seg_8748" s="T817">all</ta>
            <ta e="T819" id="Seg_8749" s="T818">thin.[NOM.SG]</ta>
            <ta e="T820" id="Seg_8750" s="T819">and</ta>
            <ta e="T821" id="Seg_8751" s="T820">this.[NOM.SG]</ta>
            <ta e="T822" id="Seg_8752" s="T821">man.[NOM.SG]</ta>
            <ta e="T823" id="Seg_8753" s="T822">come-PST.[3SG]</ta>
            <ta e="T824" id="Seg_8754" s="T823">thick.[NOM.SG]</ta>
            <ta e="T825" id="Seg_8755" s="T824">very</ta>
            <ta e="T826" id="Seg_8756" s="T825">and</ta>
            <ta e="T827" id="Seg_8757" s="T826">give-IMP.2SG</ta>
            <ta e="T828" id="Seg_8758" s="T827">companion-NOM/GEN.3SG</ta>
            <ta e="T829" id="Seg_8759" s="T828">come-PST.[3SG]</ta>
            <ta e="T831" id="Seg_8760" s="T830">this-PL</ta>
            <ta e="T832" id="Seg_8761" s="T831">potato.[NOM.SG]</ta>
            <ta e="T833" id="Seg_8762" s="T832">NEG</ta>
            <ta e="T834" id="Seg_8763" s="T833">sit-PST-3PL</ta>
            <ta e="T835" id="Seg_8764" s="T834">I.NOM</ta>
            <ta e="T836" id="Seg_8765" s="T835">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T837" id="Seg_8766" s="T836">this-PL-LAT</ta>
            <ta e="T838" id="Seg_8767" s="T837">seat-PST.[3SG]</ta>
            <ta e="T839" id="Seg_8768" s="T838">this-PL</ta>
            <ta e="T840" id="Seg_8769" s="T839">meat.[NOM.SG]</ta>
            <ta e="T841" id="Seg_8770" s="T840">many</ta>
            <ta e="T842" id="Seg_8771" s="T841">take-PST.[3SG]</ta>
            <ta e="T843" id="Seg_8772" s="T842">all</ta>
            <ta e="T844" id="Seg_8773" s="T843">meat.[NOM.SG]</ta>
            <ta e="T845" id="Seg_8774" s="T844">eat-PST-3PL</ta>
            <ta e="T846" id="Seg_8775" s="T845">bread.[NOM.SG]</ta>
            <ta e="T847" id="Seg_8776" s="T846">few</ta>
            <ta e="T848" id="Seg_8777" s="T847">be-PST.[3SG]</ta>
            <ta e="T849" id="Seg_8778" s="T848">and</ta>
            <ta e="T850" id="Seg_8779" s="T849">else</ta>
            <ta e="T851" id="Seg_8780" s="T850">what.[NOM.SG]=INDEF</ta>
            <ta e="T854" id="Seg_8781" s="T853">NEG.EX.[3SG]</ta>
            <ta e="T855" id="Seg_8782" s="T854">what</ta>
            <ta e="T856" id="Seg_8783" s="T855">sit-DUR-2SG</ta>
            <ta e="T857" id="Seg_8784" s="T856">NEG</ta>
            <ta e="T858" id="Seg_8785" s="T857">speak-PRS-2SG</ta>
            <ta e="T859" id="Seg_8786" s="T858">speak-EP-IMP.2SG</ta>
            <ta e="T860" id="Seg_8787" s="T859">what=INDEF</ta>
            <ta e="T861" id="Seg_8788" s="T860">heart-NOM/GEN/ACC.1SG</ta>
            <ta e="T862" id="Seg_8789" s="T861">probably</ta>
            <ta e="T865" id="Seg_8790" s="T864">heart-NOM/GEN/ACC.2SG</ta>
            <ta e="T866" id="Seg_8791" s="T865">probably</ta>
            <ta e="T867" id="Seg_8792" s="T866">hurt-PRS.[3SG]</ta>
            <ta e="T868" id="Seg_8793" s="T867">what.[NOM.SG]=INDEF</ta>
            <ta e="T869" id="Seg_8794" s="T868">NEG</ta>
            <ta e="T870" id="Seg_8795" s="T869">speak-PRS-2SG</ta>
            <ta e="T871" id="Seg_8796" s="T870">speak-EP-IMP.2SG</ta>
            <ta e="T875" id="Seg_8797" s="T874">where.to</ta>
            <ta e="T876" id="Seg_8798" s="T875">walk-PRS-2SG</ta>
            <ta e="T877" id="Seg_8799" s="T876">where</ta>
            <ta e="T878" id="Seg_8800" s="T877">be-PST-2SG</ta>
            <ta e="T879" id="Seg_8801" s="T878">come-IMP.2SG</ta>
            <ta e="T880" id="Seg_8802" s="T879">here</ta>
            <ta e="T881" id="Seg_8803" s="T880">what=INDEF</ta>
            <ta e="T882" id="Seg_8804" s="T881">tell-IMP.2SG</ta>
            <ta e="T883" id="Seg_8805" s="T882">I.LAT</ta>
            <ta e="T886" id="Seg_8806" s="T885">man.[NOM.SG]</ta>
            <ta e="T887" id="Seg_8807" s="T886">woman-3SG-INS</ta>
            <ta e="T888" id="Seg_8808" s="T887">live-DUR.PST-3PL</ta>
            <ta e="T889" id="Seg_8809" s="T888">this-PL</ta>
            <ta e="T890" id="Seg_8810" s="T889">child-PL-LAT</ta>
            <ta e="T891" id="Seg_8811" s="T890">be-PST-3PL</ta>
            <ta e="T892" id="Seg_8812" s="T891">girl.[NOM.SG]</ta>
            <ta e="T893" id="Seg_8813" s="T892">and</ta>
            <ta e="T894" id="Seg_8814" s="T893">boy.[NOM.SG]</ta>
            <ta e="T895" id="Seg_8815" s="T894">house</ta>
            <ta e="T896" id="Seg_8816" s="T895">be-PST.[3SG]</ta>
            <ta e="T897" id="Seg_8817" s="T896">cow.[NOM.SG]</ta>
            <ta e="T898" id="Seg_8818" s="T897">be-PST.[3SG]</ta>
            <ta e="T899" id="Seg_8819" s="T898">this.[NOM.SG]</ta>
            <ta e="T900" id="Seg_8820" s="T899">woman.[NOM.SG]</ta>
            <ta e="T901" id="Seg_8821" s="T900">cow.[NOM.SG]</ta>
            <ta e="T902" id="Seg_8822" s="T901">milk-PST.[3SG]</ta>
            <ta e="T903" id="Seg_8823" s="T902">child-PL-LAT</ta>
            <ta e="T904" id="Seg_8824" s="T903">give-PST.[3SG]</ta>
            <ta e="T905" id="Seg_8825" s="T904">bread.[NOM.SG]</ta>
            <ta e="T906" id="Seg_8826" s="T905">give-PST.[3SG]</ta>
            <ta e="T907" id="Seg_8827" s="T906">this-PL</ta>
            <ta e="T908" id="Seg_8828" s="T907">eat-PRS-3PL</ta>
            <ta e="T910" id="Seg_8829" s="T909">this.[NOM.SG]</ta>
            <ta e="T911" id="Seg_8830" s="T910">two-COLL</ta>
            <ta e="T912" id="Seg_8831" s="T911">man.[NOM.SG]</ta>
            <ta e="T913" id="Seg_8832" s="T912">come-PST-3PL</ta>
            <ta e="T914" id="Seg_8833" s="T913">man.[NOM.SG]</ta>
            <ta e="T915" id="Seg_8834" s="T914">and</ta>
            <ta e="T916" id="Seg_8835" s="T915">girl.[NOM.SG]</ta>
            <ta e="T917" id="Seg_8836" s="T916">you.DAT</ta>
            <ta e="T918" id="Seg_8837" s="T917">probably</ta>
            <ta e="T919" id="Seg_8838" s="T918">what=INDEF</ta>
            <ta e="T920" id="Seg_8839" s="T919">bring-PST-3PL</ta>
            <ta e="T921" id="Seg_8840" s="T920">sugar.[NOM.SG]</ta>
            <ta e="T922" id="Seg_8841" s="T921">bring-PST-3PL</ta>
            <ta e="T925" id="Seg_8842" s="T924">yellow</ta>
            <ta e="T926" id="Seg_8843" s="T925">water.[NOM.SG]</ta>
            <ta e="T927" id="Seg_8844" s="T926">drink-FUT-2SG</ta>
            <ta e="T928" id="Seg_8845" s="T927">probably</ta>
            <ta e="T929" id="Seg_8846" s="T928">candies</ta>
            <ta e="T930" id="Seg_8847" s="T929">bring-PST-3PL</ta>
            <ta e="T931" id="Seg_8848" s="T930">you.DAT</ta>
            <ta e="T932" id="Seg_8849" s="T931">shirt.[NOM.SG]</ta>
            <ta e="T933" id="Seg_8850" s="T932">probably</ta>
            <ta e="T934" id="Seg_8851" s="T933">bring-PST-3PL</ta>
            <ta e="T935" id="Seg_8852" s="T934">pants.[NOM.SG]</ta>
            <ta e="T936" id="Seg_8853" s="T935">probably</ta>
            <ta e="T937" id="Seg_8854" s="T936">bring-PST-3PL</ta>
            <ta e="T938" id="Seg_8855" s="T937">you.DAT</ta>
            <ta e="T939" id="Seg_8856" s="T938">chairman-GEN</ta>
            <ta e="T940" id="Seg_8857" s="T939">house-NOM/GEN.3SG</ta>
            <ta e="T941" id="Seg_8858" s="T940">big.[NOM.SG]</ta>
            <ta e="T942" id="Seg_8859" s="T941">people.[NOM.SG]</ta>
            <ta e="T943" id="Seg_8860" s="T942">many</ta>
            <ta e="T946" id="Seg_8861" s="T945">two.[NOM.SG]</ta>
            <ta e="T947" id="Seg_8862" s="T946">boy.[NOM.SG]</ta>
            <ta e="T948" id="Seg_8863" s="T947">two.[NOM.SG]</ta>
            <ta e="T949" id="Seg_8864" s="T948">girl.[NOM.SG]</ta>
            <ta e="T950" id="Seg_8865" s="T949">woman.[NOM.SG]</ta>
            <ta e="T951" id="Seg_8866" s="T950">man-PL-INS</ta>
            <ta e="T952" id="Seg_8867" s="T951">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T953" id="Seg_8868" s="T952">woman.[NOM.SG]</ta>
            <ta e="T955" id="Seg_8869" s="T954">dog-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T956" id="Seg_8870" s="T955">two.[NOM.SG]</ta>
            <ta e="T957" id="Seg_8871" s="T956">single.[NOM.SG]</ta>
            <ta e="T958" id="Seg_8872" s="T957">good.[NOM.SG]</ta>
            <ta e="T959" id="Seg_8873" s="T958">dog.[NOM.SG]</ta>
            <ta e="T960" id="Seg_8874" s="T959">single.[NOM.SG]</ta>
            <ta e="T961" id="Seg_8875" s="T960">three.[NOM.SG]</ta>
            <ta e="T962" id="Seg_8876" s="T961">foot</ta>
            <ta e="T963" id="Seg_8877" s="T962">and</ta>
            <ta e="T964" id="Seg_8878" s="T963">four</ta>
            <ta e="T965" id="Seg_8879" s="T964">foot</ta>
            <ta e="T966" id="Seg_8880" s="T965">NEG.EX.[3SG]</ta>
            <ta e="T970" id="Seg_8881" s="T969">you.GEN</ta>
            <ta e="T971" id="Seg_8882" s="T970">I.LAT</ta>
            <ta e="T972" id="Seg_8883" s="T971">beat-EP-IMP.2SG</ta>
            <ta e="T973" id="Seg_8884" s="T972">and</ta>
            <ta e="T974" id="Seg_8885" s="T973">what-INS</ta>
            <ta e="T975" id="Seg_8886" s="T974">beat-DUR-2SG</ta>
            <ta e="T976" id="Seg_8887" s="T975">fist-INS</ta>
            <ta e="T977" id="Seg_8888" s="T976">NEG.AUX-IMP.2SG</ta>
            <ta e="T978" id="Seg_8889" s="T977">beat-EP-CNG</ta>
            <ta e="T979" id="Seg_8890" s="T978">otherwise</ta>
            <ta e="T980" id="Seg_8891" s="T979">I.LAT</ta>
            <ta e="T981" id="Seg_8892" s="T980">hurt-FUT-3SG</ta>
            <ta e="T982" id="Seg_8893" s="T981">all</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PKZ">
            <ta e="T15" id="Seg_8894" s="T14">лес-LAT</ta>
            <ta e="T16" id="Seg_8895" s="T15">сегодня</ta>
            <ta e="T17" id="Seg_8896" s="T16">пойти-FUT-1SG</ta>
            <ta e="T20" id="Seg_8897" s="T19">лошадь-INS</ta>
            <ta e="T21" id="Seg_8898" s="T20">где</ta>
            <ta e="T22" id="Seg_8899" s="T21">мешок.[NOM.SG]</ta>
            <ta e="T23" id="Seg_8900" s="T22">надо</ta>
            <ta e="T24" id="Seg_8901" s="T23">хлеб.[NOM.SG]</ta>
            <ta e="T25" id="Seg_8902" s="T24">класть-INF.LAT</ta>
            <ta e="T26" id="Seg_8903" s="T25">соль.[NOM.SG]</ta>
            <ta e="T27" id="Seg_8904" s="T26">класть-INF.LAT</ta>
            <ta e="T28" id="Seg_8905" s="T27">ложка.[NOM.SG]</ta>
            <ta e="T29" id="Seg_8906" s="T28">надо</ta>
            <ta e="T30" id="Seg_8907" s="T29">класть-INF.LAT</ta>
            <ta e="T31" id="Seg_8908" s="T30">надо</ta>
            <ta e="T32" id="Seg_8909" s="T31">мясо.[NOM.SG]</ta>
            <ta e="T33" id="Seg_8910" s="T32">класть-INF.LAT</ta>
            <ta e="T34" id="Seg_8911" s="T33">надо</ta>
            <ta e="T35" id="Seg_8912" s="T34">хлеб.[NOM.SG]</ta>
            <ta e="T36" id="Seg_8913" s="T35">класть-INF.LAT</ta>
            <ta e="T49" id="Seg_8914" s="T46">хлеб.[NOM.SG]</ta>
            <ta e="T50" id="Seg_8915" s="T49">класть-INF.LAT</ta>
            <ta e="T51" id="Seg_8916" s="T50">надо</ta>
            <ta e="T52" id="Seg_8917" s="T51">масло.[NOM.SG]</ta>
            <ta e="T53" id="Seg_8918" s="T52">класть-INF.LAT</ta>
            <ta e="T54" id="Seg_8919" s="T53">надо</ta>
            <ta e="T55" id="Seg_8920" s="T54">яйцо-PL</ta>
            <ta e="T56" id="Seg_8921" s="T55">класть-INF.LAT</ta>
            <ta e="T57" id="Seg_8922" s="T56">ложка.[NOM.SG]</ta>
            <ta e="T58" id="Seg_8923" s="T57">надо</ta>
            <ta e="T59" id="Seg_8924" s="T58">класть-INF.LAT</ta>
            <ta e="T60" id="Seg_8925" s="T59">надо</ta>
            <ta e="T61" id="Seg_8926" s="T60">котел.[NOM.SG]</ta>
            <ta e="T62" id="Seg_8927" s="T61">класть-INF.LAT</ta>
            <ta e="T63" id="Seg_8928" s="T62">собака.[NOM.SG]</ta>
            <ta e="T64" id="Seg_8929" s="T63">взять-INF.LAT</ta>
            <ta e="T65" id="Seg_8930" s="T64">надо</ta>
            <ta e="T66" id="Seg_8931" s="T65">ружье</ta>
            <ta e="T67" id="Seg_8932" s="T66">взять-INF.LAT</ta>
            <ta e="T68" id="Seg_8933" s="T67">надо</ta>
            <ta e="T69" id="Seg_8934" s="T68">надо</ta>
            <ta e="T70" id="Seg_8935" s="T69">топор.[NOM.SG]</ta>
            <ta e="T71" id="Seg_8936" s="T70">взять-INF.LAT</ta>
            <ta e="T72" id="Seg_8937" s="T71">нож</ta>
            <ta e="T73" id="Seg_8938" s="T72">взять-INF.LAT</ta>
            <ta e="T74" id="Seg_8939" s="T73">надо</ta>
            <ta e="T86" id="Seg_8940" s="T85">%%</ta>
            <ta e="T91" id="Seg_8941" s="T90">ну</ta>
            <ta e="T92" id="Seg_8942" s="T91">мужчина-DYA-DU</ta>
            <ta e="T93" id="Seg_8943" s="T92">прийти-PRS-3PL</ta>
            <ta e="T94" id="Seg_8944" s="T93">говорить-INF.LAT</ta>
            <ta e="T95" id="Seg_8945" s="T94">сидеть-IMP.2SG</ta>
            <ta e="T96" id="Seg_8946" s="T95">говорить-FUT-1PL</ta>
            <ta e="T97" id="Seg_8947" s="T96">есть-INF.LAT</ta>
            <ta e="T98" id="Seg_8948" s="T97">наверное</ta>
            <ta e="T99" id="Seg_8949" s="T98">хочется</ta>
            <ta e="T100" id="Seg_8950" s="T99">сидеть-IMP.2SG</ta>
            <ta e="T101" id="Seg_8951" s="T100">есть-EP-IMP.2SG</ta>
            <ta e="T102" id="Seg_8952" s="T101">там</ta>
            <ta e="T103" id="Seg_8953" s="T102">хлеб.[NOM.SG]</ta>
            <ta e="T104" id="Seg_8954" s="T103">лежать-DUR.[3SG]</ta>
            <ta e="T105" id="Seg_8955" s="T104">масло</ta>
            <ta e="T106" id="Seg_8956" s="T105">стоять-PRS.[3SG]</ta>
            <ta e="T107" id="Seg_8957" s="T106">ложка.[NOM.SG]</ta>
            <ta e="T108" id="Seg_8958" s="T107">быть-PRS.[3SG]</ta>
            <ta e="T109" id="Seg_8959" s="T108">соль</ta>
            <ta e="T110" id="Seg_8960" s="T109">быть-PRS.[3SG]</ta>
            <ta e="T111" id="Seg_8961" s="T110">яйцо-PL</ta>
            <ta e="T112" id="Seg_8962" s="T111">мясо.[NOM.SG]</ta>
            <ta e="T113" id="Seg_8963" s="T112">рыба.[NOM.SG]</ta>
            <ta e="T114" id="Seg_8964" s="T113">съесть-EP-IMP.2SG</ta>
            <ta e="T115" id="Seg_8965" s="T114">весь</ta>
            <ta e="T116" id="Seg_8966" s="T115">NEG.AUX-IMP.2SG</ta>
            <ta e="T117" id="Seg_8967" s="T116">кричать-CNG</ta>
            <ta e="T118" id="Seg_8968" s="T117">NEG.AUX-IMP.2SG</ta>
            <ta e="T119" id="Seg_8969" s="T118">кричать-EP-CNG</ta>
            <ta e="T120" id="Seg_8970" s="T119">а.то</ta>
            <ta e="T121" id="Seg_8971" s="T120">бить-FUT-1SG</ta>
            <ta e="T122" id="Seg_8972" s="T121">ты.DAT</ta>
            <ta e="T123" id="Seg_8973" s="T122">NEG.AUX-IMP.2SG</ta>
            <ta e="T124" id="Seg_8974" s="T123">плакать-EP-CNG</ta>
            <ta e="T125" id="Seg_8975" s="T124">NEG.AUX-IMP.2SG</ta>
            <ta e="T126" id="Seg_8976" s="T125">сердиться-FACT-EP-CNG</ta>
            <ta e="T127" id="Seg_8977" s="T126">NEG.AUX-IMP.2SG</ta>
            <ta e="T128" id="Seg_8978" s="T127">лгать-EP-CNG</ta>
            <ta e="T129" id="Seg_8979" s="T128">сидеть-DUR-2SG</ta>
            <ta e="T130" id="Seg_8980" s="T129">весь</ta>
            <ta e="T131" id="Seg_8981" s="T130">лгать-DUR-2SG</ta>
            <ta e="T132" id="Seg_8982" s="T131">ты.GEN</ta>
            <ta e="T133" id="Seg_8983" s="T132">очень</ta>
            <ta e="T134" id="Seg_8984" s="T133">много</ta>
            <ta e="T135" id="Seg_8985" s="T134">лгать-DUR-2SG</ta>
            <ta e="T138" id="Seg_8986" s="T137">я.NOM</ta>
            <ta e="T139" id="Seg_8987" s="T138">скоро</ta>
            <ta e="T140" id="Seg_8988" s="T139">пойти-FUT-1SG</ta>
            <ta e="T141" id="Seg_8989" s="T140">%%-LAT</ta>
            <ta e="T142" id="Seg_8990" s="T141">а</ta>
            <ta e="T143" id="Seg_8991" s="T142">ты.GEN</ta>
            <ta e="T144" id="Seg_8992" s="T143">чум-LAT-2SG</ta>
            <ta e="T145" id="Seg_8993" s="T144">сидеть-IMP.2SG</ta>
            <ta e="T146" id="Seg_8994" s="T145">NEG.AUX-IMP.2SG</ta>
            <ta e="T147" id="Seg_8995" s="T146">кричать-CNG</ta>
            <ta e="T148" id="Seg_8996" s="T147">я.NOM</ta>
            <ta e="T149" id="Seg_8997" s="T148">что=INDEF</ta>
            <ta e="T150" id="Seg_8998" s="T149">ты.DAT</ta>
            <ta e="T151" id="Seg_8999" s="T150">принести-FUT-1SG</ta>
            <ta e="T152" id="Seg_9000" s="T151">тогда</ta>
            <ta e="T153" id="Seg_9001" s="T152">дать-FUT-1SG</ta>
            <ta e="T154" id="Seg_9002" s="T153">ты.DAT</ta>
            <ta e="T155" id="Seg_9003" s="T154">вчера</ta>
            <ta e="T158" id="Seg_9004" s="T157">вчера</ta>
            <ta e="T159" id="Seg_9005" s="T158">прийти-PST.[3SG]</ta>
            <ta e="T160" id="Seg_9006" s="T159">я.NOM</ta>
            <ta e="T161" id="Seg_9007" s="T160">родственник-NOM/GEN/ACC.1SG</ta>
            <ta e="T162" id="Seg_9008" s="T161">я.NOM</ta>
            <ta e="T163" id="Seg_9009" s="T162">сказать-PST-1SG</ta>
            <ta e="T164" id="Seg_9010" s="T163">лежать-IMP.2SG</ta>
            <ta e="T165" id="Seg_9011" s="T164">спать-INF.LAT</ta>
            <ta e="T166" id="Seg_9012" s="T165">тогда</ta>
            <ta e="T167" id="Seg_9013" s="T166">сказать-PST-1SG</ta>
            <ta e="T168" id="Seg_9014" s="T167">есть-EP-IMP.2SG</ta>
            <ta e="T169" id="Seg_9015" s="T168">наверное</ta>
            <ta e="T170" id="Seg_9016" s="T169">есть-INF.LAT</ta>
            <ta e="T171" id="Seg_9017" s="T170">хочется</ta>
            <ta e="T172" id="Seg_9018" s="T171">этот.[NOM.SG]</ta>
            <ta e="T173" id="Seg_9019" s="T172">сказать-PST.[3SG]</ta>
            <ta e="T174" id="Seg_9020" s="T173">я.NOM</ta>
            <ta e="T175" id="Seg_9021" s="T174">я.NOM</ta>
            <ta e="T176" id="Seg_9022" s="T175">NEG</ta>
            <ta e="T179" id="Seg_9023" s="T178">есть-INF.LAT</ta>
            <ta e="T181" id="Seg_9024" s="T180">я.NOM</ta>
            <ta e="T182" id="Seg_9025" s="T181">дом-LOC</ta>
            <ta e="T183" id="Seg_9026" s="T182">съесть-PST-1SG</ta>
            <ta e="T184" id="Seg_9027" s="T183">там</ta>
            <ta e="T185" id="Seg_9028" s="T184">тогда</ta>
            <ta e="T188" id="Seg_9029" s="T187">лежать-PST.[3SG]</ta>
            <ta e="T189" id="Seg_9030" s="T188">спать-INF.LAT</ta>
            <ta e="T190" id="Seg_9031" s="T189">я.NOM</ta>
            <ta e="T191" id="Seg_9032" s="T190">лежать-PST-1SG</ta>
            <ta e="T192" id="Seg_9033" s="T191">утро-LOC.ADV</ta>
            <ta e="T193" id="Seg_9034" s="T192">встать-PST-1SG</ta>
            <ta e="T194" id="Seg_9035" s="T193">%%-LAT</ta>
            <ta e="T195" id="Seg_9036" s="T194">идти-PST-1SG</ta>
            <ta e="T196" id="Seg_9037" s="T195">прийти-PST-1SG</ta>
            <ta e="T197" id="Seg_9038" s="T196">этот-LAT</ta>
            <ta e="T198" id="Seg_9039" s="T197">сказать-PST-1SG</ta>
            <ta e="T199" id="Seg_9040" s="T198">встать-IMP.2SG</ta>
            <ta e="T200" id="Seg_9041" s="T199">есть-EP-IMP.2SG</ta>
            <ta e="T201" id="Seg_9042" s="T200">там</ta>
            <ta e="T202" id="Seg_9043" s="T201">кислый.[NOM.SG]</ta>
            <ta e="T206" id="Seg_9044" s="T205">молоко.[NOM.SG]</ta>
            <ta e="T207" id="Seg_9045" s="T206">быть-PRS.[3SG]</ta>
            <ta e="T208" id="Seg_9046" s="T207">ягода</ta>
            <ta e="T209" id="Seg_9047" s="T208">быть-PRS.[3SG]</ta>
            <ta e="T210" id="Seg_9048" s="T209">яйцо-PL</ta>
            <ta e="T211" id="Seg_9049" s="T210">хлеб.[NOM.SG]</ta>
            <ta e="T212" id="Seg_9050" s="T211">есть-EP-IMP.2SG</ta>
            <ta e="T213" id="Seg_9051" s="T212">сидеть-IMP.2SG</ta>
            <ta e="T214" id="Seg_9052" s="T213">этот.[NOM.SG]</ta>
            <ta e="T217" id="Seg_9053" s="T216">этот.[NOM.SG]</ta>
            <ta e="T218" id="Seg_9054" s="T217">встать-PST.[3SG]</ta>
            <ta e="T219" id="Seg_9055" s="T218">мыться-DES-PST.[3SG]</ta>
            <ta e="T220" id="Seg_9056" s="T219">тогда</ta>
            <ta e="T221" id="Seg_9057" s="T220">сидеть-PST.[3SG]</ta>
            <ta e="T222" id="Seg_9058" s="T221">есть-INF.LAT</ta>
            <ta e="T224" id="Seg_9059" s="T223">тогда</ta>
            <ta e="T225" id="Seg_9060" s="T224">пойти-CVB</ta>
            <ta e="T226" id="Seg_9061" s="T225">исчезнуть-PST.[3SG]</ta>
            <ta e="T227" id="Seg_9062" s="T226">а</ta>
            <ta e="T228" id="Seg_9063" s="T227">я.NOM</ta>
            <ta e="T229" id="Seg_9064" s="T228">мыться-PST-1SG</ta>
            <ta e="T233" id="Seg_9065" s="T232">вчера</ta>
            <ta e="T234" id="Seg_9066" s="T233">я.NOM</ta>
            <ta e="T235" id="Seg_9067" s="T234">мы.NOM</ta>
            <ta e="T236" id="Seg_9068" s="T235">%%</ta>
            <ta e="T238" id="Seg_9069" s="T237">пахать-PST-1PL</ta>
            <ta e="T239" id="Seg_9070" s="T238">лошадь-INS</ta>
            <ta e="T240" id="Seg_9071" s="T239">два.[NOM.SG]</ta>
            <ta e="T241" id="Seg_9072" s="T240">девушка.[NOM.SG]</ta>
            <ta e="T242" id="Seg_9073" s="T241">класть-PST-3PL</ta>
            <ta e="T243" id="Seg_9074" s="T242">один.[NOM.SG]</ta>
            <ta e="T244" id="Seg_9075" s="T243">человек.[NOM.SG]</ta>
            <ta e="T245" id="Seg_9076" s="T244">я.NOM</ta>
            <ta e="T246" id="Seg_9077" s="T245">сам-1SG</ta>
            <ta e="T249" id="Seg_9078" s="T248">класть-PST-1SG</ta>
            <ta e="T250" id="Seg_9079" s="T249">плуг-PST-1PL</ta>
            <ta e="T257" id="Seg_9080" s="T256">я.NOM</ta>
            <ta e="T258" id="Seg_9081" s="T257">завтра</ta>
            <ta e="T259" id="Seg_9082" s="T258">баня-LAT</ta>
            <ta e="T260" id="Seg_9083" s="T259">идти-PST-1SG</ta>
            <ta e="T261" id="Seg_9084" s="T260">мыться-PST-1SG</ta>
            <ta e="T262" id="Seg_9085" s="T261">холодный.[NOM.SG]</ta>
            <ta e="T263" id="Seg_9086" s="T262">вода.[NOM.SG]</ta>
            <ta e="T264" id="Seg_9087" s="T263">быть-PST.[3SG]</ta>
            <ta e="T265" id="Seg_9088" s="T264">теплый.[NOM.SG]</ta>
            <ta e="T266" id="Seg_9089" s="T265">вода.[NOM.SG]</ta>
            <ta e="T267" id="Seg_9090" s="T266">мыло</ta>
            <ta e="T268" id="Seg_9091" s="T267">взять-PST-1SG</ta>
            <ta e="T269" id="Seg_9092" s="T268">рубашка.[NOM.SG]</ta>
            <ta e="T270" id="Seg_9093" s="T269">взять-PST-1SG</ta>
            <ta e="T271" id="Seg_9094" s="T270">баня-ABL</ta>
            <ta e="T272" id="Seg_9095" s="T271">прийти-PST-1SG</ta>
            <ta e="T273" id="Seg_9096" s="T272">корова.[NOM.SG]</ta>
            <ta e="T274" id="Seg_9097" s="T273">прийти-PST.[3SG]</ta>
            <ta e="T275" id="Seg_9098" s="T274">я.NOM</ta>
            <ta e="T276" id="Seg_9099" s="T275">доить-PST-1SG</ta>
            <ta e="T277" id="Seg_9100" s="T276">очень</ta>
            <ta e="T278" id="Seg_9101" s="T277">устать-MOM-PST-1SG</ta>
            <ta e="T279" id="Seg_9102" s="T278">упасть-MOM-PST-1SG</ta>
            <ta e="T280" id="Seg_9103" s="T279">и</ta>
            <ta e="T281" id="Seg_9104" s="T280">и</ta>
            <ta e="T282" id="Seg_9105" s="T281">спать-PST-1SG</ta>
            <ta e="T283" id="Seg_9106" s="T282">сегодня</ta>
            <ta e="T284" id="Seg_9107" s="T283">%%</ta>
            <ta e="T285" id="Seg_9108" s="T284">я.NOM</ta>
            <ta e="T286" id="Seg_9109" s="T285">пойти-FUT-1SG</ta>
            <ta e="T287" id="Seg_9110" s="T286">гриб-VBLZ-CVB</ta>
            <ta e="T288" id="Seg_9111" s="T287">гриб.[NOM.SG]</ta>
            <ta e="T289" id="Seg_9112" s="T288">принести-FUT-1SG</ta>
            <ta e="T290" id="Seg_9113" s="T289">тогда</ta>
            <ta e="T291" id="Seg_9114" s="T290">мыть-FUT-1SG</ta>
            <ta e="T292" id="Seg_9115" s="T291">солить-FUT-1SG</ta>
            <ta e="T293" id="Seg_9116" s="T292">весь</ta>
            <ta e="T294" id="Seg_9117" s="T293">тогда</ta>
            <ta e="T295" id="Seg_9118" s="T294">солить-FUT-3SG</ta>
            <ta e="T296" id="Seg_9119" s="T295">весь</ta>
            <ta e="T297" id="Seg_9120" s="T296">тогда</ta>
            <ta e="T298" id="Seg_9121" s="T297">съесть-INF.LAT</ta>
            <ta e="T299" id="Seg_9122" s="T298">и</ta>
            <ta e="T300" id="Seg_9123" s="T299">можно</ta>
            <ta e="T301" id="Seg_9124" s="T300">вождь.[NOM.SG]</ta>
            <ta e="T302" id="Seg_9125" s="T301">весь</ta>
            <ta e="T303" id="Seg_9126" s="T302">сидеть-DUR-PST.[3SG]</ta>
            <ta e="T304" id="Seg_9127" s="T303">весь</ta>
            <ta e="T305" id="Seg_9128" s="T304">очень</ta>
            <ta e="T306" id="Seg_9129" s="T305">пьяный.[NOM.SG]</ta>
            <ta e="T307" id="Seg_9130" s="T306">водка.[NOM.SG]</ta>
            <ta e="T308" id="Seg_9131" s="T307">пить-PST.[3SG]</ta>
            <ta e="T309" id="Seg_9132" s="T308">очень</ta>
            <ta e="T310" id="Seg_9133" s="T309">много</ta>
            <ta e="T311" id="Seg_9134" s="T310">пить-PST.[3SG]</ta>
            <ta e="T312" id="Seg_9135" s="T311">упасть-MOM-PST.[3SG]</ta>
            <ta e="T313" id="Seg_9136" s="T312">весь</ta>
            <ta e="T314" id="Seg_9137" s="T313">разный</ta>
            <ta e="T316" id="Seg_9138" s="T315">ругать-DES-DUR.[3SG]</ta>
            <ta e="T317" id="Seg_9139" s="T316">я.NOM</ta>
            <ta e="T318" id="Seg_9140" s="T317">этот-ACC</ta>
            <ta e="T319" id="Seg_9141" s="T318">сажать-PST-1SG</ta>
            <ta e="T320" id="Seg_9142" s="T319">сидеть-IMP.2SG</ta>
            <ta e="T321" id="Seg_9143" s="T320">сказать-PST-1SG</ta>
            <ta e="T322" id="Seg_9144" s="T321">я.NOM</ta>
            <ta e="T323" id="Seg_9145" s="T322">сказать-PST-1SG</ta>
            <ta e="T324" id="Seg_9146" s="T323">NEG.AUX-IMP.2SG</ta>
            <ta e="T325" id="Seg_9147" s="T324">ругать-DES-CNG</ta>
            <ta e="T326" id="Seg_9148" s="T325">сидеть-IMP.2SG</ta>
            <ta e="T327" id="Seg_9149" s="T326">сидеть-IMP.2SG</ta>
            <ta e="T328" id="Seg_9150" s="T327">желтый</ta>
            <ta e="T329" id="Seg_9151" s="T328">вода.[NOM.SG]</ta>
            <ta e="T330" id="Seg_9152" s="T329">пить-INF.LAT</ta>
            <ta e="T331" id="Seg_9153" s="T330">сахар</ta>
            <ta e="T332" id="Seg_9154" s="T331">лежать-DUR.[3SG]</ta>
            <ta e="T333" id="Seg_9155" s="T332">класть-IMP.2SG.O</ta>
            <ta e="T334" id="Seg_9156" s="T333">желтый.[NOM.SG]</ta>
            <ta e="T335" id="Seg_9157" s="T334">вода-LAT</ta>
            <ta e="T336" id="Seg_9158" s="T335">яйцо-PL</ta>
            <ta e="T337" id="Seg_9159" s="T336">лежать-DUR-3PL</ta>
            <ta e="T340" id="Seg_9160" s="T339">хлеб.[NOM.SG]</ta>
            <ta e="T341" id="Seg_9161" s="T340">съесть-EP-IMP.2SG</ta>
            <ta e="T342" id="Seg_9162" s="T341">мясо.[NOM.SG]</ta>
            <ta e="T343" id="Seg_9163" s="T342">съесть-EP-IMP.2SG</ta>
            <ta e="T344" id="Seg_9164" s="T343">рыба</ta>
            <ta e="T345" id="Seg_9165" s="T344">съесть-EP-IMP.2SG</ta>
            <ta e="T346" id="Seg_9166" s="T345">яйцо</ta>
            <ta e="T347" id="Seg_9167" s="T346">лежать-DUR.[3SG]</ta>
            <ta e="T348" id="Seg_9168" s="T347">съесть-EP-IMP.2SG</ta>
            <ta e="T349" id="Seg_9169" s="T348">масло.[NOM.SG]</ta>
            <ta e="T350" id="Seg_9170" s="T349">съесть-EP-IMP.2SG</ta>
            <ta e="T351" id="Seg_9171" s="T350">сметана.[NOM.SG]</ta>
            <ta e="T352" id="Seg_9172" s="T351">съесть-EP-IMP.2SG</ta>
            <ta e="T353" id="Seg_9173" s="T352">кислый.[NOM.SG]</ta>
            <ta e="T354" id="Seg_9174" s="T353">молоко.[NOM.SG]</ta>
            <ta e="T355" id="Seg_9175" s="T354">съесть-EP-IMP.2SG</ta>
            <ta e="T356" id="Seg_9176" s="T355">рыба.[NOM.SG]</ta>
            <ta e="T357" id="Seg_9177" s="T356">съесть-EP-IMP.2SG</ta>
            <ta e="T359" id="Seg_9178" s="T358">надо</ta>
            <ta e="T360" id="Seg_9179" s="T359">я.LAT</ta>
            <ta e="T361" id="Seg_9180" s="T360">сапог.[NOM.SG]</ta>
            <ta e="T362" id="Seg_9181" s="T361">шить-INF.LAT</ta>
            <ta e="T363" id="Seg_9182" s="T362">парка.[NOM.SG]</ta>
            <ta e="T364" id="Seg_9183" s="T363">шить-INF.LAT</ta>
            <ta e="T365" id="Seg_9184" s="T364">надо</ta>
            <ta e="T368" id="Seg_9185" s="T367">рубашка.[NOM.SG]</ta>
            <ta e="T369" id="Seg_9186" s="T368">шить-INF.LAT</ta>
            <ta e="T370" id="Seg_9187" s="T369">взять-INF.LAT</ta>
            <ta e="T373" id="Seg_9188" s="T372">надо</ta>
            <ta e="T374" id="Seg_9189" s="T373">штаны.[NOM.SG]</ta>
            <ta e="T375" id="Seg_9190" s="T374">шить-INF.LAT</ta>
            <ta e="T376" id="Seg_9191" s="T375">а.то</ta>
            <ta e="T377" id="Seg_9192" s="T376">я.NOM</ta>
            <ta e="T378" id="Seg_9193" s="T377">мальчик-NOM/GEN/ACC.1SG</ta>
            <ta e="T379" id="Seg_9194" s="T378">весь</ta>
            <ta e="T380" id="Seg_9195" s="T379">надеть-INF.LAT</ta>
            <ta e="T381" id="Seg_9196" s="T380">NEG.EX.[3SG]</ta>
            <ta e="T382" id="Seg_9197" s="T381">что</ta>
            <ta e="T383" id="Seg_9198" s="T382">что.[NOM.SG]=INDEF</ta>
            <ta e="T384" id="Seg_9199" s="T383">NEG.EX.[3SG]</ta>
            <ta e="T385" id="Seg_9200" s="T384">я.NOM</ta>
            <ta e="T386" id="Seg_9201" s="T385">пойти-FUT-1SG</ta>
            <ta e="T387" id="Seg_9202" s="T386">Агинское</ta>
            <ta e="T388" id="Seg_9203" s="T387">поселение-LAT</ta>
            <ta e="T389" id="Seg_9204" s="T388">и</ta>
            <ta e="T390" id="Seg_9205" s="T389">надо</ta>
            <ta e="T391" id="Seg_9206" s="T390">хлеб.[NOM.SG]</ta>
            <ta e="T392" id="Seg_9207" s="T391">взять-INF.LAT</ta>
            <ta e="T393" id="Seg_9208" s="T392">надо</ta>
            <ta e="T394" id="Seg_9209" s="T393">сахар.[NOM.SG]</ta>
            <ta e="T395" id="Seg_9210" s="T394">взять-INF.LAT</ta>
            <ta e="T396" id="Seg_9211" s="T395">надо</ta>
            <ta e="T399" id="Seg_9212" s="T398">ружье-NOM/GEN/ACC.3SG</ta>
            <ta e="T400" id="Seg_9213" s="T399">хлеб.[NOM.SG]</ta>
            <ta e="T401" id="Seg_9214" s="T400">взять-INF.LAT</ta>
            <ta e="T402" id="Seg_9215" s="T401">надо</ta>
            <ta e="T409" id="Seg_9216" s="T408">ну</ta>
            <ta e="T410" id="Seg_9217" s="T409">хлеб.[NOM.SG]</ta>
            <ta e="T416" id="Seg_9218" s="T415">я.NOM</ta>
            <ta e="T417" id="Seg_9219" s="T416">лес-LAT</ta>
            <ta e="T418" id="Seg_9220" s="T417">тогда</ta>
            <ta e="T419" id="Seg_9221" s="T418">пойти-FUT-1SG</ta>
            <ta e="T422" id="Seg_9222" s="T421">сегодня</ta>
            <ta e="T425" id="Seg_9223" s="T424">этот-PL</ta>
            <ta e="T426" id="Seg_9224" s="T425">лес-ABL</ta>
            <ta e="T427" id="Seg_9225" s="T426">прийти-FUT-3PL</ta>
            <ta e="T428" id="Seg_9226" s="T427">наверное</ta>
            <ta e="T429" id="Seg_9227" s="T428">мясо.[NOM.SG]</ta>
            <ta e="T430" id="Seg_9228" s="T429">принести-FUT-3PL</ta>
            <ta e="T431" id="Seg_9229" s="T430">черемша.[NOM.SG]</ta>
            <ta e="T432" id="Seg_9230" s="T431">принести-FUT-3PL</ta>
            <ta e="T434" id="Seg_9231" s="T433">колоть-INF.LAT</ta>
            <ta e="T435" id="Seg_9232" s="T434">надо</ta>
            <ta e="T436" id="Seg_9233" s="T435">и</ta>
            <ta e="T437" id="Seg_9234" s="T436">солить-INF.LAT</ta>
            <ta e="T438" id="Seg_9235" s="T437">яйцо-PL</ta>
            <ta e="T439" id="Seg_9236" s="T438">класть-INF.LAT</ta>
            <ta e="T440" id="Seg_9237" s="T439">сметана.[NOM.SG]</ta>
            <ta e="T441" id="Seg_9238" s="T440">класть-INF.LAT</ta>
            <ta e="T442" id="Seg_9239" s="T441">и</ta>
            <ta e="T443" id="Seg_9240" s="T442">вода.[NOM.SG]</ta>
            <ta e="T444" id="Seg_9241" s="T443">лить-INF.LAT</ta>
            <ta e="T445" id="Seg_9242" s="T444">и</ta>
            <ta e="T446" id="Seg_9243" s="T445">есть-INF.LAT</ta>
            <ta e="T447" id="Seg_9244" s="T446">ложка-INS</ta>
            <ta e="T448" id="Seg_9245" s="T447">надо</ta>
            <ta e="T449" id="Seg_9246" s="T448">пойти-INF.LAT</ta>
            <ta e="T454" id="Seg_9247" s="T453">пойти-INF.LAT</ta>
            <ta e="T459" id="Seg_9248" s="T458">пахать-INF.LAT</ta>
            <ta e="T460" id="Seg_9249" s="T459">хлеб.[NOM.SG]</ta>
            <ta e="T461" id="Seg_9250" s="T460">брызгать-INF.LAT</ta>
            <ta e="T462" id="Seg_9251" s="T461">тогда</ta>
            <ta e="T463" id="Seg_9252" s="T462">расти-FUT-3SG</ta>
            <ta e="T464" id="Seg_9253" s="T463">вставлять-INF.LAT</ta>
            <ta e="T465" id="Seg_9254" s="T464">надо</ta>
            <ta e="T466" id="Seg_9255" s="T465">тогда</ta>
            <ta e="T467" id="Seg_9256" s="T466">трясти-MULT-INF.LAT</ta>
            <ta e="T468" id="Seg_9257" s="T467">надо</ta>
            <ta e="T469" id="Seg_9258" s="T468">тогда</ta>
            <ta e="T476" id="Seg_9259" s="T475">тогда</ta>
            <ta e="T477" id="Seg_9260" s="T476">тянуть-INF.LAT</ta>
            <ta e="T478" id="Seg_9261" s="T477">надо</ta>
            <ta e="T481" id="Seg_9262" s="T480">тогда</ta>
            <ta e="T482" id="Seg_9263" s="T481">надо</ta>
            <ta e="T483" id="Seg_9264" s="T482">печь-INF.LAT</ta>
            <ta e="T484" id="Seg_9265" s="T483">тогда</ta>
            <ta e="T485" id="Seg_9266" s="T484">есть-INF.LAT</ta>
            <ta e="T486" id="Seg_9267" s="T485">надо</ta>
            <ta e="T489" id="Seg_9268" s="T488">сегодня</ta>
            <ta e="T490" id="Seg_9269" s="T489">большой.[NOM.SG]</ta>
            <ta e="T491" id="Seg_9270" s="T490">день.[NOM.SG]</ta>
            <ta e="T494" id="Seg_9271" s="T493">люди.[NOM.SG]</ta>
            <ta e="T495" id="Seg_9272" s="T494">очень</ta>
            <ta e="T496" id="Seg_9273" s="T495">много</ta>
            <ta e="T497" id="Seg_9274" s="T496">красивый-PL</ta>
            <ta e="T498" id="Seg_9275" s="T497">весь</ta>
            <ta e="T499" id="Seg_9276" s="T498">водка.[NOM.SG]</ta>
            <ta e="T500" id="Seg_9277" s="T499">пить-DUR-3PL</ta>
            <ta e="T501" id="Seg_9278" s="T500">играть-DUR-3PL</ta>
            <ta e="T503" id="Seg_9279" s="T502">прыгнуть-DUR-3PL</ta>
            <ta e="T504" id="Seg_9280" s="T503">песня</ta>
            <ta e="T505" id="Seg_9281" s="T504">петь-DUR-3PL</ta>
            <ta e="T506" id="Seg_9282" s="T505">хороший.[NOM.SG]</ta>
            <ta e="T507" id="Seg_9283" s="T506">женщина.[NOM.SG]</ta>
            <ta e="T508" id="Seg_9284" s="T507">хлеб.[NOM.SG]</ta>
            <ta e="T509" id="Seg_9285" s="T508">весь</ta>
            <ta e="T512" id="Seg_9286" s="T511">печь-PST.[3SG]</ta>
            <ta e="T513" id="Seg_9287" s="T512">хороший.[NOM.SG]</ta>
            <ta e="T514" id="Seg_9288" s="T513">мягкий.[NOM.SG]</ta>
            <ta e="T515" id="Seg_9289" s="T514">скоро</ta>
            <ta e="T520" id="Seg_9290" s="T519">есть-IMP.2PL</ta>
            <ta e="T521" id="Seg_9291" s="T520">наверное</ta>
            <ta e="T522" id="Seg_9292" s="T521">быть.голодным-PRS-3PL</ta>
            <ta e="T523" id="Seg_9293" s="T522">скоро</ta>
            <ta e="T524" id="Seg_9294" s="T523">встать-FUT-1SG</ta>
            <ta e="T525" id="Seg_9295" s="T524">съесть-IMP.2PL</ta>
            <ta e="T526" id="Seg_9296" s="T525">сидеть</ta>
            <ta e="T527" id="Seg_9297" s="T526">сидеть-IMP.2PL</ta>
            <ta e="T528" id="Seg_9298" s="T527">толстый.[NOM.SG]</ta>
            <ta e="T529" id="Seg_9299" s="T528">мужчина.[NOM.SG]</ta>
            <ta e="T530" id="Seg_9300" s="T529">съесть-PST.[3SG]</ta>
            <ta e="T531" id="Seg_9301" s="T530">хлеб.[NOM.SG]</ta>
            <ta e="T532" id="Seg_9302" s="T531">черемуха</ta>
            <ta e="T533" id="Seg_9303" s="T532">ягода-INS</ta>
            <ta e="T534" id="Seg_9304" s="T533">сметана.[NOM.SG]</ta>
            <ta e="T535" id="Seg_9305" s="T534">поставить-PST-1SG</ta>
            <ta e="T536" id="Seg_9306" s="T535">есть-IMP.2PL</ta>
            <ta e="T538" id="Seg_9307" s="T537">молоко.[NOM.SG]</ta>
            <ta e="T541" id="Seg_9308" s="T540">пить-IMP.2SG.O</ta>
            <ta e="T544" id="Seg_9309" s="T543">ты.GEN</ta>
            <ta e="T545" id="Seg_9310" s="T544">еще</ta>
            <ta e="T546" id="Seg_9311" s="T545">спать-DUR-2SG</ta>
            <ta e="T547" id="Seg_9312" s="T546">встать-IMP.2SG</ta>
            <ta e="T548" id="Seg_9313" s="T547">а.то</ta>
            <ta e="T549" id="Seg_9314" s="T548">холодный.[NOM.SG]</ta>
            <ta e="T550" id="Seg_9315" s="T549">вода.[NOM.SG]</ta>
            <ta e="T551" id="Seg_9316" s="T550">принести-FUT-1SG</ta>
            <ta e="T552" id="Seg_9317" s="T551">ты.DAT</ta>
            <ta e="T553" id="Seg_9318" s="T552">лить-FUT-1SG</ta>
            <ta e="T554" id="Seg_9319" s="T553">весь</ta>
            <ta e="T555" id="Seg_9320" s="T554">тогда</ta>
            <ta e="T556" id="Seg_9321" s="T555">весь</ta>
            <ta e="T557" id="Seg_9322" s="T556">прыгнуть-MOM-FUT-FRQ</ta>
            <ta e="T567" id="Seg_9323" s="T566">NEG</ta>
            <ta e="T568" id="Seg_9324" s="T567">слушать-IMP.2SG.O</ta>
            <ta e="T569" id="Seg_9325" s="T568">NEG.AUX-IMP.2SG</ta>
            <ta e="T570" id="Seg_9326" s="T569">резать-CNG</ta>
            <ta e="T574" id="Seg_9327" s="T573">NEG.AUX-IMP.2SG</ta>
            <ta e="T575" id="Seg_9328" s="T574">говорить-EP-CNG</ta>
            <ta e="T578" id="Seg_9329" s="T577">NEG.AUX-IMP.2SG</ta>
            <ta e="T579" id="Seg_9330" s="T578">ругать-DES-CNG</ta>
            <ta e="T583" id="Seg_9331" s="T582">NEG.AUX-IMP.2SG</ta>
            <ta e="T584" id="Seg_9332" s="T583">кричать-EP-CNG</ta>
            <ta e="T587" id="Seg_9333" s="T586">сопли-NOM/GEN/ACC.2SG</ta>
            <ta e="T588" id="Seg_9334" s="T587">течь-DUR.[3SG]</ta>
            <ta e="T589" id="Seg_9335" s="T588">вытереть-IMP.2SG.O</ta>
            <ta e="T590" id="Seg_9336" s="T589">сопли-NOM/GEN/ACC.2SG</ta>
            <ta e="T593" id="Seg_9337" s="T592">NEG.AUX-IMP.2SG</ta>
            <ta e="T594" id="Seg_9338" s="T593">бежать-CNG</ta>
            <ta e="T595" id="Seg_9339" s="T594">NEG.AUX-IMP.2SG</ta>
            <ta e="T596" id="Seg_9340" s="T595">прыгнуть-CNG</ta>
            <ta e="T597" id="Seg_9341" s="T596">NEG.AUX-IMP.2SG</ta>
            <ta e="T598" id="Seg_9342" s="T597">петь-CNG</ta>
            <ta e="T599" id="Seg_9343" s="T598">очень</ta>
            <ta e="T600" id="Seg_9344" s="T599">хороший.[NOM.SG]</ta>
            <ta e="T601" id="Seg_9345" s="T600">очень</ta>
            <ta e="T602" id="Seg_9346" s="T601">красивый.[NOM.SG]</ta>
            <ta e="T603" id="Seg_9347" s="T602">очень</ta>
            <ta e="T604" id="Seg_9348" s="T603">красный.[NOM.SG]</ta>
            <ta e="T605" id="Seg_9349" s="T604">очень</ta>
            <ta e="T607" id="Seg_9350" s="T606">жирный</ta>
            <ta e="T608" id="Seg_9351" s="T607">очень</ta>
            <ta e="T609" id="Seg_9352" s="T608">длинный.[NOM.SG]</ta>
            <ta e="T613" id="Seg_9353" s="T612">очень</ta>
            <ta e="T614" id="Seg_9354" s="T613">маленький.[NOM.SG]</ta>
            <ta e="T619" id="Seg_9355" s="T618">наверное</ta>
            <ta e="T620" id="Seg_9356" s="T619">платок.[NOM.SG]</ta>
            <ta e="T621" id="Seg_9357" s="T620">украсть-PST-2SG</ta>
            <ta e="T622" id="Seg_9358" s="T621">зачем</ta>
            <ta e="T623" id="Seg_9359" s="T622">принести-PST-2SG</ta>
            <ta e="T624" id="Seg_9360" s="T623">я.NOM</ta>
            <ta e="T625" id="Seg_9361" s="T624">NEG</ta>
            <ta e="T626" id="Seg_9362" s="T625">взять-FUT-1SG</ta>
            <ta e="T627" id="Seg_9363" s="T626">не</ta>
            <ta e="T628" id="Seg_9364" s="T627">надо</ta>
            <ta e="T629" id="Seg_9365" s="T628">украсть-INF.LAT</ta>
            <ta e="T630" id="Seg_9366" s="T629">а.то</ta>
            <ta e="T631" id="Seg_9367" s="T630">NEG</ta>
            <ta e="T632" id="Seg_9368" s="T631">хороший.[NOM.SG]</ta>
            <ta e="T633" id="Seg_9369" s="T632">стать-FUT-3SG</ta>
            <ta e="T634" id="Seg_9370" s="T633">рука-NOM/GEN/ACC.1SG</ta>
            <ta e="T635" id="Seg_9371" s="T634">резать-PST-1SG</ta>
            <ta e="T636" id="Seg_9372" s="T635">весь</ta>
            <ta e="T637" id="Seg_9373" s="T636">кровь.[NOM.SG]</ta>
            <ta e="T638" id="Seg_9374" s="T637">течь-DUR.[3SG]</ta>
            <ta e="T639" id="Seg_9375" s="T638">принести-IMP.2SG</ta>
            <ta e="T640" id="Seg_9376" s="T639">что=INDEF</ta>
            <ta e="T641" id="Seg_9377" s="T640">рука-NOM/GEN/ACC.1SG</ta>
            <ta e="T642" id="Seg_9378" s="T641">завязать-INF.LAT</ta>
            <ta e="T647" id="Seg_9379" s="T645">зачем</ta>
            <ta e="T648" id="Seg_9380" s="T647">нож-INS</ta>
            <ta e="T649" id="Seg_9381" s="T648">нож.[NOM.SG]</ta>
            <ta e="T650" id="Seg_9382" s="T649">взять-PST-2SG</ta>
            <ta e="T651" id="Seg_9383" s="T650">ты.DAT</ta>
            <ta e="T652" id="Seg_9384" s="T651">еще</ta>
            <ta e="T654" id="Seg_9385" s="T652">надо</ta>
            <ta e="T655" id="Seg_9386" s="T654">ветер.[NOM.SG]</ta>
            <ta e="T656" id="Seg_9387" s="T655">дождь.[NOM.SG]</ta>
            <ta e="T657" id="Seg_9388" s="T656">весь</ta>
            <ta e="T658" id="Seg_9389" s="T657">прийти-PRS.[3SG]</ta>
            <ta e="T659" id="Seg_9390" s="T658">снег.[NOM.SG]</ta>
            <ta e="T660" id="Seg_9391" s="T659">весь</ta>
            <ta e="T661" id="Seg_9392" s="T660">прийти-PRS.[3SG]</ta>
            <ta e="T662" id="Seg_9393" s="T661">ветер-INS</ta>
            <ta e="T663" id="Seg_9394" s="T662">очень</ta>
            <ta e="T664" id="Seg_9395" s="T663">холодный.[NOM.SG]</ta>
            <ta e="T665" id="Seg_9396" s="T664">я.NOM</ta>
            <ta e="T666" id="Seg_9397" s="T665">очень</ta>
            <ta e="T667" id="Seg_9398" s="T666">мерзнуть-PST-1SG</ta>
            <ta e="T668" id="Seg_9399" s="T667">и</ta>
            <ta e="T669" id="Seg_9400" s="T668">сильно</ta>
            <ta e="T670" id="Seg_9401" s="T669">мерзнуть-PST-1SG</ta>
            <ta e="T671" id="Seg_9402" s="T670">нога-NOM/GEN/ACC.1SG</ta>
            <ta e="T672" id="Seg_9403" s="T671">мерзнуть-PST.[3SG]</ta>
            <ta e="T673" id="Seg_9404" s="T672">рука-NOM/GEN/ACC.1SG</ta>
            <ta e="T674" id="Seg_9405" s="T673">мерзнуть-PST.[3SG]</ta>
            <ta e="T675" id="Seg_9406" s="T674">лицо-NOM/GEN/ACC.1SG</ta>
            <ta e="T676" id="Seg_9407" s="T675">весь</ta>
            <ta e="T677" id="Seg_9408" s="T676">мерзнуть-PST.[3SG]</ta>
            <ta e="T678" id="Seg_9409" s="T677">сегодня</ta>
            <ta e="T681" id="Seg_9410" s="T680">красивый.[NOM.SG]</ta>
            <ta e="T682" id="Seg_9411" s="T681">день.[NOM.SG]</ta>
            <ta e="T683" id="Seg_9412" s="T682">солнце.[NOM.SG]</ta>
            <ta e="T684" id="Seg_9413" s="T683">весь</ta>
            <ta e="T685" id="Seg_9414" s="T684">сверкать-DUR.[3SG]</ta>
            <ta e="T686" id="Seg_9415" s="T685">наверху</ta>
            <ta e="T687" id="Seg_9416" s="T686">что.[NOM.SG]=INDEF</ta>
            <ta e="T688" id="Seg_9417" s="T687">NEG.EX.[3SG]</ta>
            <ta e="T689" id="Seg_9418" s="T688">очень</ta>
            <ta e="T690" id="Seg_9419" s="T689">хороший.[NOM.SG]</ta>
            <ta e="T691" id="Seg_9420" s="T690">день.[NOM.SG]</ta>
            <ta e="T692" id="Seg_9421" s="T691">люди.[NOM.SG]</ta>
            <ta e="T693" id="Seg_9422" s="T692">весь</ta>
            <ta e="T694" id="Seg_9423" s="T693">работать-DUR-3PL</ta>
            <ta e="T695" id="Seg_9424" s="T694">картофель.[NOM.SG]</ta>
            <ta e="T696" id="Seg_9425" s="T695">съесть-DUR-3PL</ta>
            <ta e="T697" id="Seg_9426" s="T696">лошадь-PL</ta>
            <ta e="T698" id="Seg_9427" s="T697">плуг-DUR-3PL</ta>
            <ta e="T699" id="Seg_9428" s="T698">весь</ta>
            <ta e="T700" id="Seg_9429" s="T699">надо</ta>
            <ta e="T701" id="Seg_9430" s="T700">пойти-INF.LAT</ta>
            <ta e="T702" id="Seg_9431" s="T701">трава.[NOM.SG]</ta>
            <ta e="T703" id="Seg_9432" s="T702">резать-INF.LAT</ta>
            <ta e="T704" id="Seg_9433" s="T703">коса.[NOM.SG]</ta>
            <ta e="T705" id="Seg_9434" s="T704">надо</ta>
            <ta e="T706" id="Seg_9435" s="T705">взять-INF.LAT</ta>
            <ta e="T707" id="Seg_9436" s="T706">надо</ta>
            <ta e="T708" id="Seg_9437" s="T707">вода.[NOM.SG]</ta>
            <ta e="T709" id="Seg_9438" s="T708">взять-INF.LAT</ta>
            <ta e="T710" id="Seg_9439" s="T709">молоко.[NOM.SG]</ta>
            <ta e="T711" id="Seg_9440" s="T710">взять-INF.LAT</ta>
            <ta e="T712" id="Seg_9441" s="T711">надо</ta>
            <ta e="T713" id="Seg_9442" s="T712">хлеб.[NOM.SG]</ta>
            <ta e="T714" id="Seg_9443" s="T713">взять-INF.LAT</ta>
            <ta e="T715" id="Seg_9444" s="T714">соль</ta>
            <ta e="T716" id="Seg_9445" s="T715">взять-INF.LAT</ta>
            <ta e="T717" id="Seg_9446" s="T716">яйцо-PL</ta>
            <ta e="T718" id="Seg_9447" s="T717">взять-INF.LAT</ta>
            <ta e="T719" id="Seg_9448" s="T718">камень.[NOM.SG]</ta>
            <ta e="T720" id="Seg_9449" s="T719">надо</ta>
            <ta e="T721" id="Seg_9450" s="T720">взять-INF.LAT</ta>
            <ta e="T722" id="Seg_9451" s="T721">коса.[NOM.SG]</ta>
            <ta e="T723" id="Seg_9452" s="T722">соболь.[NOM.SG]</ta>
            <ta e="T724" id="Seg_9453" s="T723">сегодня</ta>
            <ta e="T725" id="Seg_9454" s="T724">убить-PST-1SG</ta>
            <ta e="T726" id="Seg_9455" s="T725">белка.[NOM.SG]</ta>
            <ta e="T727" id="Seg_9456" s="T726">убить-PST-1SG</ta>
            <ta e="T728" id="Seg_9457" s="T727">медведь.[NOM.SG]</ta>
            <ta e="T729" id="Seg_9458" s="T728">убить-PST-1SG</ta>
            <ta e="T730" id="Seg_9459" s="T729">олень.[NOM.SG]</ta>
            <ta e="T731" id="Seg_9460" s="T730">убить-PST-1SG</ta>
            <ta e="T732" id="Seg_9461" s="T731">жеребец.[NOM.SG]</ta>
            <ta e="T733" id="Seg_9462" s="T732">убить-PST-1SG</ta>
            <ta e="T734" id="Seg_9463" s="T733">коза.[NOM.SG]</ta>
            <ta e="T735" id="Seg_9464" s="T734">убить-PST-1SG</ta>
            <ta e="T736" id="Seg_9465" s="T735">ружье-INS</ta>
            <ta e="T737" id="Seg_9466" s="T736">корова-PL</ta>
            <ta e="T738" id="Seg_9467" s="T737">весь</ta>
            <ta e="T739" id="Seg_9468" s="T738">трава.[NOM.SG]</ta>
            <ta e="T740" id="Seg_9469" s="T739">съесть-PRS-3PL</ta>
            <ta e="T741" id="Seg_9470" s="T740">идти-DUR-3PL</ta>
            <ta e="T742" id="Seg_9471" s="T741">там</ta>
            <ta e="T743" id="Seg_9472" s="T742">этот.[NOM.SG]</ta>
            <ta e="T744" id="Seg_9473" s="T743">мужчина.[NOM.SG]</ta>
            <ta e="T745" id="Seg_9474" s="T744">принести-FUT-3SG</ta>
            <ta e="T747" id="Seg_9475" s="T746">чум-LAT/LOC.3SG</ta>
            <ta e="T749" id="Seg_9476" s="T748">надо</ta>
            <ta e="T750" id="Seg_9477" s="T749">ребенок-NOM/GEN/ACC.1SG</ta>
            <ta e="T751" id="Seg_9478" s="T750">мыть-INF.LAT</ta>
            <ta e="T752" id="Seg_9479" s="T751">надо</ta>
            <ta e="T755" id="Seg_9480" s="T754">ребенок-NOM/GEN/ACC.1SG</ta>
            <ta e="T756" id="Seg_9481" s="T755">весь</ta>
            <ta e="T757" id="Seg_9482" s="T756">плакать-DUR.[3SG]</ta>
            <ta e="T758" id="Seg_9483" s="T757">надо</ta>
            <ta e="T759" id="Seg_9484" s="T758">этот-ACC</ta>
            <ta e="T760" id="Seg_9485" s="T759">развернуть-INF.LAT</ta>
            <ta e="T761" id="Seg_9486" s="T760">надо</ta>
            <ta e="T762" id="Seg_9487" s="T761">есть-INF.LAT</ta>
            <ta e="T763" id="Seg_9488" s="T762">дать-INF.LAT</ta>
            <ta e="T764" id="Seg_9489" s="T763">надо</ta>
            <ta e="T765" id="Seg_9490" s="T764">спать-INF.LAT</ta>
            <ta e="T766" id="Seg_9491" s="T765">класть-INF.LAT</ta>
            <ta e="T771" id="Seg_9492" s="T770">баюкать-IMP.2SG</ta>
            <ta e="T772" id="Seg_9493" s="T771">ребенок-NOM/GEN/ACC.1SG</ta>
            <ta e="T773" id="Seg_9494" s="T772">PTCL</ta>
            <ta e="T774" id="Seg_9495" s="T773">JUSS</ta>
            <ta e="T775" id="Seg_9496" s="T774">спать-DUR.[3SG]</ta>
            <ta e="T776" id="Seg_9497" s="T775">сидеть-IMP.2SG</ta>
            <ta e="T777" id="Seg_9498" s="T776">скоро</ta>
            <ta e="T778" id="Seg_9499" s="T777">вода-VBLZ-CVB</ta>
            <ta e="T779" id="Seg_9500" s="T778">пойти-FUT-1SG</ta>
            <ta e="T780" id="Seg_9501" s="T779">вода.[NOM.SG]</ta>
            <ta e="T781" id="Seg_9502" s="T780">принести-FUT-1SG</ta>
            <ta e="T782" id="Seg_9503" s="T781">тогда</ta>
            <ta e="T783" id="Seg_9504" s="T782">пить-FUT-2SG</ta>
            <ta e="T784" id="Seg_9505" s="T783">NEG.AUX-IMP.2SG</ta>
            <ta e="T785" id="Seg_9506" s="T784">пойти-EP-CNG</ta>
            <ta e="T786" id="Seg_9507" s="T785">а</ta>
            <ta e="T787" id="Seg_9508" s="T786">тогда</ta>
            <ta e="T788" id="Seg_9509" s="T787">пойти-FUT-2SG</ta>
            <ta e="T789" id="Seg_9510" s="T788">чум-LAT-2SG</ta>
            <ta e="T790" id="Seg_9511" s="T789">скоро</ta>
            <ta e="T791" id="Seg_9512" s="T790">кровь.[NOM.SG]</ta>
            <ta e="T792" id="Seg_9513" s="T791">печь-FUT-1SG</ta>
            <ta e="T793" id="Seg_9514" s="T792">надо</ta>
            <ta e="T794" id="Seg_9515" s="T793">там</ta>
            <ta e="T795" id="Seg_9516" s="T794">молоко.[NOM.SG]</ta>
            <ta e="T796" id="Seg_9517" s="T795">лить-INF.LAT</ta>
            <ta e="T797" id="Seg_9518" s="T796">масло.[NOM.SG]</ta>
            <ta e="T798" id="Seg_9519" s="T797">класть-INF.LAT</ta>
            <ta e="T799" id="Seg_9520" s="T798">или</ta>
            <ta e="T800" id="Seg_9521" s="T799">жир.[NOM.SG]</ta>
            <ta e="T801" id="Seg_9522" s="T800">класть-INF.LAT</ta>
            <ta e="T802" id="Seg_9523" s="T801">скоро</ta>
            <ta e="T803" id="Seg_9524" s="T802">есть-FUT-1DU</ta>
            <ta e="T804" id="Seg_9525" s="T803">NEG.AUX-IMP.2SG</ta>
            <ta e="T805" id="Seg_9526" s="T804">%%</ta>
            <ta e="T806" id="Seg_9527" s="T805">а.то</ta>
            <ta e="T807" id="Seg_9528" s="T806">теплый.[NOM.SG]</ta>
            <ta e="T810" id="Seg_9529" s="T809">хлеб-INS</ta>
            <ta e="T811" id="Seg_9530" s="T810">съесть-EP-IMP.2SG</ta>
            <ta e="T812" id="Seg_9531" s="T811">девушка.[NOM.SG]</ta>
            <ta e="T814" id="Seg_9532" s="T812">прийти-PST.[3SG]</ta>
            <ta e="T815" id="Seg_9533" s="T814">волосы.[NOM.SG]</ta>
            <ta e="T816" id="Seg_9534" s="T815">снег.[NOM.SG]</ta>
            <ta e="T817" id="Seg_9535" s="T816">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T818" id="Seg_9536" s="T817">весь</ta>
            <ta e="T819" id="Seg_9537" s="T818">худой.[NOM.SG]</ta>
            <ta e="T820" id="Seg_9538" s="T819">и</ta>
            <ta e="T821" id="Seg_9539" s="T820">этот.[NOM.SG]</ta>
            <ta e="T822" id="Seg_9540" s="T821">мужчина.[NOM.SG]</ta>
            <ta e="T823" id="Seg_9541" s="T822">прийти-PST.[3SG]</ta>
            <ta e="T824" id="Seg_9542" s="T823">толстый.[NOM.SG]</ta>
            <ta e="T825" id="Seg_9543" s="T824">очень</ta>
            <ta e="T826" id="Seg_9544" s="T825">и</ta>
            <ta e="T827" id="Seg_9545" s="T826">дать-IMP.2SG</ta>
            <ta e="T828" id="Seg_9546" s="T827">товарищ-NOM/GEN.3SG</ta>
            <ta e="T829" id="Seg_9547" s="T828">прийти-PST.[3SG]</ta>
            <ta e="T831" id="Seg_9548" s="T830">этот-PL</ta>
            <ta e="T832" id="Seg_9549" s="T831">картофель.[NOM.SG]</ta>
            <ta e="T833" id="Seg_9550" s="T832">NEG</ta>
            <ta e="T834" id="Seg_9551" s="T833">сидеть-PST-3PL</ta>
            <ta e="T835" id="Seg_9552" s="T834">я.NOM</ta>
            <ta e="T836" id="Seg_9553" s="T835">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T837" id="Seg_9554" s="T836">этот-PL-LAT</ta>
            <ta e="T838" id="Seg_9555" s="T837">сажать-PST.[3SG]</ta>
            <ta e="T839" id="Seg_9556" s="T838">этот-PL</ta>
            <ta e="T840" id="Seg_9557" s="T839">мясо.[NOM.SG]</ta>
            <ta e="T841" id="Seg_9558" s="T840">много</ta>
            <ta e="T842" id="Seg_9559" s="T841">взять-PST.[3SG]</ta>
            <ta e="T843" id="Seg_9560" s="T842">весь</ta>
            <ta e="T844" id="Seg_9561" s="T843">мясо.[NOM.SG]</ta>
            <ta e="T845" id="Seg_9562" s="T844">съесть-PST-3PL</ta>
            <ta e="T846" id="Seg_9563" s="T845">хлеб.[NOM.SG]</ta>
            <ta e="T847" id="Seg_9564" s="T846">мало</ta>
            <ta e="T848" id="Seg_9565" s="T847">быть-PST.[3SG]</ta>
            <ta e="T849" id="Seg_9566" s="T848">а</ta>
            <ta e="T850" id="Seg_9567" s="T849">еще</ta>
            <ta e="T851" id="Seg_9568" s="T850">что.[NOM.SG]=INDEF</ta>
            <ta e="T854" id="Seg_9569" s="T853">NEG.EX.[3SG]</ta>
            <ta e="T855" id="Seg_9570" s="T854">что</ta>
            <ta e="T856" id="Seg_9571" s="T855">сидеть-DUR-2SG</ta>
            <ta e="T857" id="Seg_9572" s="T856">NEG</ta>
            <ta e="T858" id="Seg_9573" s="T857">говорить-PRS-2SG</ta>
            <ta e="T859" id="Seg_9574" s="T858">говорить-EP-IMP.2SG</ta>
            <ta e="T860" id="Seg_9575" s="T859">что=INDEF</ta>
            <ta e="T861" id="Seg_9576" s="T860">сердце-NOM/GEN/ACC.1SG</ta>
            <ta e="T862" id="Seg_9577" s="T861">наверное</ta>
            <ta e="T865" id="Seg_9578" s="T864">сердце-NOM/GEN/ACC.2SG</ta>
            <ta e="T866" id="Seg_9579" s="T865">наверное</ta>
            <ta e="T867" id="Seg_9580" s="T866">болеть-PRS.[3SG]</ta>
            <ta e="T868" id="Seg_9581" s="T867">что.[NOM.SG]=INDEF</ta>
            <ta e="T869" id="Seg_9582" s="T868">NEG</ta>
            <ta e="T870" id="Seg_9583" s="T869">говорить-PRS-2SG</ta>
            <ta e="T871" id="Seg_9584" s="T870">говорить-EP-IMP.2SG</ta>
            <ta e="T875" id="Seg_9585" s="T874">куда</ta>
            <ta e="T876" id="Seg_9586" s="T875">идти-PRS-2SG</ta>
            <ta e="T877" id="Seg_9587" s="T876">где</ta>
            <ta e="T878" id="Seg_9588" s="T877">быть-PST-2SG</ta>
            <ta e="T879" id="Seg_9589" s="T878">прийти-IMP.2SG</ta>
            <ta e="T880" id="Seg_9590" s="T879">здесь</ta>
            <ta e="T881" id="Seg_9591" s="T880">что=INDEF</ta>
            <ta e="T882" id="Seg_9592" s="T881">сказать-IMP.2SG</ta>
            <ta e="T883" id="Seg_9593" s="T882">я.LAT</ta>
            <ta e="T886" id="Seg_9594" s="T885">мужчина.[NOM.SG]</ta>
            <ta e="T887" id="Seg_9595" s="T886">женщина-3SG-INS</ta>
            <ta e="T888" id="Seg_9596" s="T887">жить-DUR.PST-3PL</ta>
            <ta e="T889" id="Seg_9597" s="T888">этот-PL</ta>
            <ta e="T890" id="Seg_9598" s="T889">ребенок-PL-LAT</ta>
            <ta e="T891" id="Seg_9599" s="T890">быть-PST-3PL</ta>
            <ta e="T892" id="Seg_9600" s="T891">девушка.[NOM.SG]</ta>
            <ta e="T893" id="Seg_9601" s="T892">и</ta>
            <ta e="T894" id="Seg_9602" s="T893">мальчик.[NOM.SG]</ta>
            <ta e="T895" id="Seg_9603" s="T894">дом</ta>
            <ta e="T896" id="Seg_9604" s="T895">быть-PST.[3SG]</ta>
            <ta e="T897" id="Seg_9605" s="T896">корова.[NOM.SG]</ta>
            <ta e="T898" id="Seg_9606" s="T897">быть-PST.[3SG]</ta>
            <ta e="T899" id="Seg_9607" s="T898">этот.[NOM.SG]</ta>
            <ta e="T900" id="Seg_9608" s="T899">женщина.[NOM.SG]</ta>
            <ta e="T901" id="Seg_9609" s="T900">корова.[NOM.SG]</ta>
            <ta e="T902" id="Seg_9610" s="T901">доить-PST.[3SG]</ta>
            <ta e="T903" id="Seg_9611" s="T902">ребенок-PL-LAT</ta>
            <ta e="T904" id="Seg_9612" s="T903">дать-PST.[3SG]</ta>
            <ta e="T905" id="Seg_9613" s="T904">хлеб.[NOM.SG]</ta>
            <ta e="T906" id="Seg_9614" s="T905">дать-PST.[3SG]</ta>
            <ta e="T907" id="Seg_9615" s="T906">этот-PL</ta>
            <ta e="T908" id="Seg_9616" s="T907">съесть-PRS-3PL</ta>
            <ta e="T910" id="Seg_9617" s="T909">этот.[NOM.SG]</ta>
            <ta e="T911" id="Seg_9618" s="T910">два-COLL</ta>
            <ta e="T912" id="Seg_9619" s="T911">мужчина.[NOM.SG]</ta>
            <ta e="T913" id="Seg_9620" s="T912">прийти-PST-3PL</ta>
            <ta e="T914" id="Seg_9621" s="T913">человек.[NOM.SG]</ta>
            <ta e="T915" id="Seg_9622" s="T914">и</ta>
            <ta e="T916" id="Seg_9623" s="T915">девушка.[NOM.SG]</ta>
            <ta e="T917" id="Seg_9624" s="T916">ты.DAT</ta>
            <ta e="T918" id="Seg_9625" s="T917">наверное</ta>
            <ta e="T919" id="Seg_9626" s="T918">что=INDEF</ta>
            <ta e="T920" id="Seg_9627" s="T919">принести-PST-3PL</ta>
            <ta e="T921" id="Seg_9628" s="T920">сахар.[NOM.SG]</ta>
            <ta e="T922" id="Seg_9629" s="T921">принести-PST-3PL</ta>
            <ta e="T925" id="Seg_9630" s="T924">желтый</ta>
            <ta e="T926" id="Seg_9631" s="T925">вода.[NOM.SG]</ta>
            <ta e="T927" id="Seg_9632" s="T926">пить-FUT-2SG</ta>
            <ta e="T928" id="Seg_9633" s="T927">наверное</ta>
            <ta e="T929" id="Seg_9634" s="T928">конфеты </ta>
            <ta e="T930" id="Seg_9635" s="T929">принести-PST-3PL</ta>
            <ta e="T931" id="Seg_9636" s="T930">ты.DAT</ta>
            <ta e="T932" id="Seg_9637" s="T931">рубашка.[NOM.SG]</ta>
            <ta e="T933" id="Seg_9638" s="T932">наверное</ta>
            <ta e="T934" id="Seg_9639" s="T933">принести-PST-3PL</ta>
            <ta e="T935" id="Seg_9640" s="T934">штаны.[NOM.SG]</ta>
            <ta e="T936" id="Seg_9641" s="T935">наверное</ta>
            <ta e="T937" id="Seg_9642" s="T936">принести-PST-3PL</ta>
            <ta e="T938" id="Seg_9643" s="T937">ты.DAT</ta>
            <ta e="T939" id="Seg_9644" s="T938">председатель-GEN</ta>
            <ta e="T940" id="Seg_9645" s="T939">дом-NOM/GEN.3SG</ta>
            <ta e="T941" id="Seg_9646" s="T940">большой.[NOM.SG]</ta>
            <ta e="T942" id="Seg_9647" s="T941">люди.[NOM.SG]</ta>
            <ta e="T943" id="Seg_9648" s="T942">много</ta>
            <ta e="T946" id="Seg_9649" s="T945">два.[NOM.SG]</ta>
            <ta e="T947" id="Seg_9650" s="T946">мальчик.[NOM.SG]</ta>
            <ta e="T948" id="Seg_9651" s="T947">два.[NOM.SG]</ta>
            <ta e="T949" id="Seg_9652" s="T948">девушка.[NOM.SG]</ta>
            <ta e="T950" id="Seg_9653" s="T949">женщина.[NOM.SG]</ta>
            <ta e="T951" id="Seg_9654" s="T950">мужчина-PL-INS</ta>
            <ta e="T952" id="Seg_9655" s="T951">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T953" id="Seg_9656" s="T952">женщина.[NOM.SG]</ta>
            <ta e="T955" id="Seg_9657" s="T954">собака-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T956" id="Seg_9658" s="T955">два.[NOM.SG]</ta>
            <ta e="T957" id="Seg_9659" s="T956">один.[NOM.SG]</ta>
            <ta e="T958" id="Seg_9660" s="T957">хороший.[NOM.SG]</ta>
            <ta e="T959" id="Seg_9661" s="T958">собака.[NOM.SG]</ta>
            <ta e="T960" id="Seg_9662" s="T959">один.[NOM.SG]</ta>
            <ta e="T961" id="Seg_9663" s="T960">три.[NOM.SG]</ta>
            <ta e="T962" id="Seg_9664" s="T961">нога</ta>
            <ta e="T963" id="Seg_9665" s="T962">а</ta>
            <ta e="T964" id="Seg_9666" s="T963">четыре</ta>
            <ta e="T965" id="Seg_9667" s="T964">нога</ta>
            <ta e="T966" id="Seg_9668" s="T965">NEG.EX.[3SG]</ta>
            <ta e="T970" id="Seg_9669" s="T969">ты.GEN</ta>
            <ta e="T971" id="Seg_9670" s="T970">я.LAT</ta>
            <ta e="T972" id="Seg_9671" s="T971">бить-EP-IMP.2SG</ta>
            <ta e="T973" id="Seg_9672" s="T972">а</ta>
            <ta e="T974" id="Seg_9673" s="T973">что-INS</ta>
            <ta e="T975" id="Seg_9674" s="T974">бить-DUR-2SG</ta>
            <ta e="T976" id="Seg_9675" s="T975">кулак-INS</ta>
            <ta e="T977" id="Seg_9676" s="T976">NEG.AUX-IMP.2SG</ta>
            <ta e="T978" id="Seg_9677" s="T977">бить-EP-CNG</ta>
            <ta e="T979" id="Seg_9678" s="T978">а.то</ta>
            <ta e="T980" id="Seg_9679" s="T979">я.LAT</ta>
            <ta e="T981" id="Seg_9680" s="T980">болеть-FUT-3SG</ta>
            <ta e="T982" id="Seg_9681" s="T981">весь</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PKZ">
            <ta e="T15" id="Seg_9682" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_9683" s="T15">adv</ta>
            <ta e="T17" id="Seg_9684" s="T16">v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_9685" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_9686" s="T20">que</ta>
            <ta e="T22" id="Seg_9687" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_9688" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_9689" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_9690" s="T24">v-v:n.fin</ta>
            <ta e="T26" id="Seg_9691" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_9692" s="T26">v-v:n.fin</ta>
            <ta e="T28" id="Seg_9693" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_9694" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_9695" s="T29">v-v:n.fin</ta>
            <ta e="T31" id="Seg_9696" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_9697" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_9698" s="T32">v-v:n.fin</ta>
            <ta e="T34" id="Seg_9699" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_9700" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_9701" s="T35">v-v:n.fin</ta>
            <ta e="T49" id="Seg_9702" s="T46">n-n:case</ta>
            <ta e="T50" id="Seg_9703" s="T49">v-v:n.fin</ta>
            <ta e="T51" id="Seg_9704" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_9705" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_9706" s="T52">v-v:n.fin</ta>
            <ta e="T54" id="Seg_9707" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_9708" s="T54">n-n:num</ta>
            <ta e="T56" id="Seg_9709" s="T55">v-v:n.fin</ta>
            <ta e="T57" id="Seg_9710" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_9711" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_9712" s="T58">v-v:n.fin</ta>
            <ta e="T60" id="Seg_9713" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_9714" s="T60">n-n:case</ta>
            <ta e="T62" id="Seg_9715" s="T61">v-v:n.fin</ta>
            <ta e="T63" id="Seg_9716" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_9717" s="T63">v-v:n.fin</ta>
            <ta e="T65" id="Seg_9718" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_9719" s="T65">n</ta>
            <ta e="T67" id="Seg_9720" s="T66">v-v:n.fin</ta>
            <ta e="T68" id="Seg_9721" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_9722" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_9723" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_9724" s="T70">v-v:n.fin</ta>
            <ta e="T72" id="Seg_9725" s="T71">n</ta>
            <ta e="T73" id="Seg_9726" s="T72">v-v:n.fin</ta>
            <ta e="T74" id="Seg_9727" s="T73">ptcl</ta>
            <ta e="T86" id="Seg_9728" s="T85">%%</ta>
            <ta e="T91" id="Seg_9729" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_9730" s="T91">n-n&gt;n-n:num</ta>
            <ta e="T93" id="Seg_9731" s="T92">v-v:tense-v:pn</ta>
            <ta e="T94" id="Seg_9732" s="T93">v-v:n.fin</ta>
            <ta e="T95" id="Seg_9733" s="T94">v-v:mood.pn</ta>
            <ta e="T96" id="Seg_9734" s="T95">v-v:tense-v:pn</ta>
            <ta e="T97" id="Seg_9735" s="T96">v-v:n.fin</ta>
            <ta e="T98" id="Seg_9736" s="T97">ptcl</ta>
            <ta e="T99" id="Seg_9737" s="T98">n</ta>
            <ta e="T100" id="Seg_9738" s="T99">v-v:mood.pn</ta>
            <ta e="T101" id="Seg_9739" s="T100">v-v:ins-v:mood.pn</ta>
            <ta e="T102" id="Seg_9740" s="T101">adv</ta>
            <ta e="T103" id="Seg_9741" s="T102">n-n:case</ta>
            <ta e="T104" id="Seg_9742" s="T103">v-v&gt;v-v:pn</ta>
            <ta e="T105" id="Seg_9743" s="T104">n</ta>
            <ta e="T106" id="Seg_9744" s="T105">v-v:tense-v:pn</ta>
            <ta e="T107" id="Seg_9745" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_9746" s="T107">v-v:tense-v:pn</ta>
            <ta e="T109" id="Seg_9747" s="T108">n</ta>
            <ta e="T110" id="Seg_9748" s="T109">v-v:tense-v:pn</ta>
            <ta e="T111" id="Seg_9749" s="T110">n-n:num</ta>
            <ta e="T112" id="Seg_9750" s="T111">n-n:case</ta>
            <ta e="T113" id="Seg_9751" s="T112">n-n:case</ta>
            <ta e="T114" id="Seg_9752" s="T113">v-v:ins-v:mood.pn</ta>
            <ta e="T115" id="Seg_9753" s="T114">quant</ta>
            <ta e="T116" id="Seg_9754" s="T115">aux-v:mood.pn</ta>
            <ta e="T117" id="Seg_9755" s="T116">v-v:n.fin</ta>
            <ta e="T118" id="Seg_9756" s="T117">aux-v:mood.pn</ta>
            <ta e="T119" id="Seg_9757" s="T118">v-v:ins-v:mood.pn</ta>
            <ta e="T120" id="Seg_9758" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_9759" s="T120">v-v:tense-v:pn</ta>
            <ta e="T122" id="Seg_9760" s="T121">pers</ta>
            <ta e="T123" id="Seg_9761" s="T122">v-v:mood.pn</ta>
            <ta e="T124" id="Seg_9762" s="T123">v-v:ins-v:n.fin</ta>
            <ta e="T125" id="Seg_9763" s="T124">aux-v:mood.pn</ta>
            <ta e="T126" id="Seg_9764" s="T125">v-v&gt;v-v:ins-v:n.fin</ta>
            <ta e="T127" id="Seg_9765" s="T126">aux-v:mood.pn</ta>
            <ta e="T128" id="Seg_9766" s="T127">v-v:ins-v:n.fin</ta>
            <ta e="T129" id="Seg_9767" s="T128">v-v&gt;v-v:pn</ta>
            <ta e="T130" id="Seg_9768" s="T129">quant</ta>
            <ta e="T131" id="Seg_9769" s="T130">v-v&gt;v-v:pn</ta>
            <ta e="T132" id="Seg_9770" s="T131">pers</ta>
            <ta e="T133" id="Seg_9771" s="T132">adv</ta>
            <ta e="T134" id="Seg_9772" s="T133">quant</ta>
            <ta e="T135" id="Seg_9773" s="T134">v-v&gt;v-v:pn</ta>
            <ta e="T138" id="Seg_9774" s="T137">pers</ta>
            <ta e="T139" id="Seg_9775" s="T138">adv</ta>
            <ta e="T140" id="Seg_9776" s="T139">v-v:tense-v:pn</ta>
            <ta e="T141" id="Seg_9777" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_9778" s="T141">conj</ta>
            <ta e="T143" id="Seg_9779" s="T142">pers</ta>
            <ta e="T144" id="Seg_9780" s="T143">n-n:case-n:case.poss</ta>
            <ta e="T145" id="Seg_9781" s="T144">v-v:mood.pn</ta>
            <ta e="T146" id="Seg_9782" s="T145">aux-v:mood.pn</ta>
            <ta e="T147" id="Seg_9783" s="T146">v-v:n.fin</ta>
            <ta e="T148" id="Seg_9784" s="T147">pers</ta>
            <ta e="T149" id="Seg_9785" s="T148">que=ptcl</ta>
            <ta e="T150" id="Seg_9786" s="T149">pers</ta>
            <ta e="T151" id="Seg_9787" s="T150">v-v:tense-v:pn</ta>
            <ta e="T152" id="Seg_9788" s="T151">adv</ta>
            <ta e="T153" id="Seg_9789" s="T152">v-v:tense-v:pn</ta>
            <ta e="T154" id="Seg_9790" s="T153">pers</ta>
            <ta e="T155" id="Seg_9791" s="T154">adv</ta>
            <ta e="T158" id="Seg_9792" s="T157">adv</ta>
            <ta e="T159" id="Seg_9793" s="T158">v-v:tense-v:pn</ta>
            <ta e="T160" id="Seg_9794" s="T159">pers</ta>
            <ta e="T161" id="Seg_9795" s="T160">n-n:case.poss</ta>
            <ta e="T162" id="Seg_9796" s="T161">pers</ta>
            <ta e="T163" id="Seg_9797" s="T162">v-v:tense-v:pn</ta>
            <ta e="T164" id="Seg_9798" s="T163">v-v:mood.pn</ta>
            <ta e="T165" id="Seg_9799" s="T164">v-v:n.fin</ta>
            <ta e="T166" id="Seg_9800" s="T165">adv</ta>
            <ta e="T167" id="Seg_9801" s="T166">v-v:tense-v:pn</ta>
            <ta e="T168" id="Seg_9802" s="T167">v-v:ins-v:mood.pn</ta>
            <ta e="T169" id="Seg_9803" s="T168">ptcl</ta>
            <ta e="T170" id="Seg_9804" s="T169">v-v:n.fin</ta>
            <ta e="T171" id="Seg_9805" s="T170">n</ta>
            <ta e="T172" id="Seg_9806" s="T171">dempro-n:case</ta>
            <ta e="T173" id="Seg_9807" s="T172">v-v:tense-v:pn</ta>
            <ta e="T174" id="Seg_9808" s="T173">pers</ta>
            <ta e="T175" id="Seg_9809" s="T174">pers</ta>
            <ta e="T176" id="Seg_9810" s="T175">ptcl</ta>
            <ta e="T179" id="Seg_9811" s="T178">v-v:n.fin</ta>
            <ta e="T181" id="Seg_9812" s="T180">pers</ta>
            <ta e="T182" id="Seg_9813" s="T181">n-n:case</ta>
            <ta e="T183" id="Seg_9814" s="T182">v-v:tense-v:pn</ta>
            <ta e="T184" id="Seg_9815" s="T183">adv</ta>
            <ta e="T185" id="Seg_9816" s="T184">adv</ta>
            <ta e="T188" id="Seg_9817" s="T187">v-v:tense-v:pn</ta>
            <ta e="T189" id="Seg_9818" s="T188">v-v:n.fin</ta>
            <ta e="T190" id="Seg_9819" s="T189">pers</ta>
            <ta e="T191" id="Seg_9820" s="T190">v-v:tense-v:pn</ta>
            <ta e="T192" id="Seg_9821" s="T191">n-adv:case</ta>
            <ta e="T193" id="Seg_9822" s="T192">v-v:tense-v:pn</ta>
            <ta e="T194" id="Seg_9823" s="T193">n-n:case</ta>
            <ta e="T195" id="Seg_9824" s="T194">v-v:tense-v:pn</ta>
            <ta e="T196" id="Seg_9825" s="T195">v-v:tense-v:pn</ta>
            <ta e="T197" id="Seg_9826" s="T196">dempro-n:case</ta>
            <ta e="T198" id="Seg_9827" s="T197">v-v:tense-v:pn</ta>
            <ta e="T199" id="Seg_9828" s="T198">v-v:mood.pn</ta>
            <ta e="T200" id="Seg_9829" s="T199">v-v:ins-v:mood.pn</ta>
            <ta e="T201" id="Seg_9830" s="T200">adv</ta>
            <ta e="T202" id="Seg_9831" s="T201">adj-n:case</ta>
            <ta e="T206" id="Seg_9832" s="T205">n-n:case</ta>
            <ta e="T207" id="Seg_9833" s="T206">v-v:tense-v:pn</ta>
            <ta e="T208" id="Seg_9834" s="T207">n</ta>
            <ta e="T209" id="Seg_9835" s="T208">v-v:tense-v:pn</ta>
            <ta e="T210" id="Seg_9836" s="T209">n-n:num</ta>
            <ta e="T211" id="Seg_9837" s="T210">n-n:case</ta>
            <ta e="T212" id="Seg_9838" s="T211">v-v:ins-v:mood.pn</ta>
            <ta e="T213" id="Seg_9839" s="T212">v-v:mood.pn</ta>
            <ta e="T214" id="Seg_9840" s="T213">dempro-n:case</ta>
            <ta e="T217" id="Seg_9841" s="T216">dempro-n:case</ta>
            <ta e="T218" id="Seg_9842" s="T217">v-v:tense-v:pn</ta>
            <ta e="T219" id="Seg_9843" s="T218">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T220" id="Seg_9844" s="T219">adv</ta>
            <ta e="T221" id="Seg_9845" s="T220">v-v:tense-v:pn</ta>
            <ta e="T222" id="Seg_9846" s="T221">v-v:n.fin</ta>
            <ta e="T224" id="Seg_9847" s="T223">adv</ta>
            <ta e="T225" id="Seg_9848" s="T224">v-v:n.fin</ta>
            <ta e="T226" id="Seg_9849" s="T225">v-v:tense-v:pn</ta>
            <ta e="T227" id="Seg_9850" s="T226">conj</ta>
            <ta e="T228" id="Seg_9851" s="T227">pers</ta>
            <ta e="T229" id="Seg_9852" s="T228">v-v:tense-v:pn</ta>
            <ta e="T233" id="Seg_9853" s="T232">adv</ta>
            <ta e="T234" id="Seg_9854" s="T233">pers</ta>
            <ta e="T235" id="Seg_9855" s="T234">pers</ta>
            <ta e="T236" id="Seg_9856" s="T235">%%</ta>
            <ta e="T238" id="Seg_9857" s="T237">v-v:tense-v:pn</ta>
            <ta e="T239" id="Seg_9858" s="T238">n-n:case</ta>
            <ta e="T240" id="Seg_9859" s="T239">num-n:case</ta>
            <ta e="T241" id="Seg_9860" s="T240">n-n:case</ta>
            <ta e="T242" id="Seg_9861" s="T241">v-v:tense-v:pn</ta>
            <ta e="T243" id="Seg_9862" s="T242">adj-n:case</ta>
            <ta e="T244" id="Seg_9863" s="T243">n-n:case</ta>
            <ta e="T245" id="Seg_9864" s="T244">pers</ta>
            <ta e="T246" id="Seg_9865" s="T245">refl-n:case.poss</ta>
            <ta e="T249" id="Seg_9866" s="T248">v-v:tense-v:pn</ta>
            <ta e="T250" id="Seg_9867" s="T249">v-v:tense-v:pn</ta>
            <ta e="T257" id="Seg_9868" s="T256">pers</ta>
            <ta e="T258" id="Seg_9869" s="T257">adv</ta>
            <ta e="T259" id="Seg_9870" s="T258">n-n:case</ta>
            <ta e="T260" id="Seg_9871" s="T259">v-v:tense-v:pn</ta>
            <ta e="T261" id="Seg_9872" s="T260">v-v:tense-v:pn</ta>
            <ta e="T262" id="Seg_9873" s="T261">adj-n:case</ta>
            <ta e="T263" id="Seg_9874" s="T262">n-n:case</ta>
            <ta e="T264" id="Seg_9875" s="T263">v-v:tense-v:pn</ta>
            <ta e="T265" id="Seg_9876" s="T264">adj-n:case</ta>
            <ta e="T266" id="Seg_9877" s="T265">n-n:case</ta>
            <ta e="T267" id="Seg_9878" s="T266">n</ta>
            <ta e="T268" id="Seg_9879" s="T267">v-v:tense-v:pn</ta>
            <ta e="T269" id="Seg_9880" s="T268">n-n:case</ta>
            <ta e="T270" id="Seg_9881" s="T269">v-v:tense-v:pn</ta>
            <ta e="T271" id="Seg_9882" s="T270">n-n:case</ta>
            <ta e="T272" id="Seg_9883" s="T271">v-v:tense-v:pn</ta>
            <ta e="T273" id="Seg_9884" s="T272">n-n:case</ta>
            <ta e="T274" id="Seg_9885" s="T273">v-v:tense-v:pn</ta>
            <ta e="T275" id="Seg_9886" s="T274">pers</ta>
            <ta e="T276" id="Seg_9887" s="T275">v-v:tense-v:pn</ta>
            <ta e="T277" id="Seg_9888" s="T276">adv</ta>
            <ta e="T278" id="Seg_9889" s="T277">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T279" id="Seg_9890" s="T278">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T280" id="Seg_9891" s="T279">conj</ta>
            <ta e="T281" id="Seg_9892" s="T280">conj</ta>
            <ta e="T282" id="Seg_9893" s="T281">v-v:tense-v:pn</ta>
            <ta e="T283" id="Seg_9894" s="T282">adv</ta>
            <ta e="T285" id="Seg_9895" s="T284">pers</ta>
            <ta e="T286" id="Seg_9896" s="T285">v-v:tense-v:pn</ta>
            <ta e="T287" id="Seg_9897" s="T286">n-n&gt;v-v:n.fin</ta>
            <ta e="T288" id="Seg_9898" s="T287">n-n:case</ta>
            <ta e="T289" id="Seg_9899" s="T288">v-v:tense-v:pn</ta>
            <ta e="T290" id="Seg_9900" s="T289">adv</ta>
            <ta e="T291" id="Seg_9901" s="T290">v-v:tense-v:pn</ta>
            <ta e="T292" id="Seg_9902" s="T291">v-v:tense-v:pn</ta>
            <ta e="T293" id="Seg_9903" s="T292">ptcl</ta>
            <ta e="T294" id="Seg_9904" s="T293">adv</ta>
            <ta e="T295" id="Seg_9905" s="T294">v-v:tense-v:pn</ta>
            <ta e="T296" id="Seg_9906" s="T295">quant</ta>
            <ta e="T297" id="Seg_9907" s="T296">adv</ta>
            <ta e="T298" id="Seg_9908" s="T297">v-v:n.fin</ta>
            <ta e="T299" id="Seg_9909" s="T298">conj</ta>
            <ta e="T300" id="Seg_9910" s="T299">adv</ta>
            <ta e="T301" id="Seg_9911" s="T300">n-n:case</ta>
            <ta e="T302" id="Seg_9912" s="T301">quant</ta>
            <ta e="T303" id="Seg_9913" s="T302">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T304" id="Seg_9914" s="T303">quant</ta>
            <ta e="T305" id="Seg_9915" s="T304">adv</ta>
            <ta e="T306" id="Seg_9916" s="T305">adj-n:case</ta>
            <ta e="T307" id="Seg_9917" s="T306">n.[n:case]</ta>
            <ta e="T308" id="Seg_9918" s="T307">v-v:tense-v:pn</ta>
            <ta e="T309" id="Seg_9919" s="T308">adv</ta>
            <ta e="T310" id="Seg_9920" s="T309">quant</ta>
            <ta e="T311" id="Seg_9921" s="T310">v-v:tense-v:pn</ta>
            <ta e="T312" id="Seg_9922" s="T311">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T313" id="Seg_9923" s="T312">quant</ta>
            <ta e="T314" id="Seg_9924" s="T313">adj</ta>
            <ta e="T316" id="Seg_9925" s="T315">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T317" id="Seg_9926" s="T316">pers</ta>
            <ta e="T318" id="Seg_9927" s="T317">dempro-n:case</ta>
            <ta e="T319" id="Seg_9928" s="T318">v-v:tense-v:pn</ta>
            <ta e="T320" id="Seg_9929" s="T319">v-v:mood.pn</ta>
            <ta e="T321" id="Seg_9930" s="T320">v-v:tense-v:pn</ta>
            <ta e="T322" id="Seg_9931" s="T321">pers</ta>
            <ta e="T323" id="Seg_9932" s="T322">v-v:tense-v:pn</ta>
            <ta e="T324" id="Seg_9933" s="T323">aux-v:mood.pn</ta>
            <ta e="T325" id="Seg_9934" s="T324">v-v&gt;v-v:n.fin</ta>
            <ta e="T326" id="Seg_9935" s="T325">v-v:mood.pn</ta>
            <ta e="T327" id="Seg_9936" s="T326">v-v:mood.pn</ta>
            <ta e="T328" id="Seg_9937" s="T327">adj</ta>
            <ta e="T329" id="Seg_9938" s="T328">n-n:case</ta>
            <ta e="T330" id="Seg_9939" s="T329">v-v:n.fin</ta>
            <ta e="T331" id="Seg_9940" s="T330">n</ta>
            <ta e="T332" id="Seg_9941" s="T331">v-v&gt;v-v:pn</ta>
            <ta e="T333" id="Seg_9942" s="T332">v-v:mood.pn</ta>
            <ta e="T334" id="Seg_9943" s="T333">adj-n:case</ta>
            <ta e="T335" id="Seg_9944" s="T334">n-n:case</ta>
            <ta e="T336" id="Seg_9945" s="T335">n-n:num</ta>
            <ta e="T337" id="Seg_9946" s="T336">v-v&gt;v-v:pn</ta>
            <ta e="T340" id="Seg_9947" s="T339">n-n:case</ta>
            <ta e="T341" id="Seg_9948" s="T340">v-v:ins-v:mood.pn</ta>
            <ta e="T342" id="Seg_9949" s="T341">n-n:case</ta>
            <ta e="T343" id="Seg_9950" s="T342">v-v:ins-v:mood.pn</ta>
            <ta e="T344" id="Seg_9951" s="T343">n</ta>
            <ta e="T345" id="Seg_9952" s="T344">v-v:ins-v:mood.pn</ta>
            <ta e="T346" id="Seg_9953" s="T345">n</ta>
            <ta e="T347" id="Seg_9954" s="T346">v-v&gt;v-v:pn</ta>
            <ta e="T348" id="Seg_9955" s="T347">v-v:ins-v:mood.pn</ta>
            <ta e="T349" id="Seg_9956" s="T348">n-n:case</ta>
            <ta e="T350" id="Seg_9957" s="T349">v-v:ins-v:mood.pn</ta>
            <ta e="T351" id="Seg_9958" s="T350">n-n:case</ta>
            <ta e="T352" id="Seg_9959" s="T351">v-v:ins-v:mood.pn</ta>
            <ta e="T353" id="Seg_9960" s="T352">adj-n:case</ta>
            <ta e="T354" id="Seg_9961" s="T353">n-n:case</ta>
            <ta e="T355" id="Seg_9962" s="T354">v-v:ins-v:mood.pn</ta>
            <ta e="T356" id="Seg_9963" s="T355">n-n:case</ta>
            <ta e="T357" id="Seg_9964" s="T356">v-v:ins-v:mood.pn</ta>
            <ta e="T359" id="Seg_9965" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_9966" s="T359">pers</ta>
            <ta e="T361" id="Seg_9967" s="T360">n-n:case</ta>
            <ta e="T362" id="Seg_9968" s="T361">v-v:n.fin</ta>
            <ta e="T363" id="Seg_9969" s="T362">n-n:case</ta>
            <ta e="T364" id="Seg_9970" s="T363">v-v:n.fin</ta>
            <ta e="T365" id="Seg_9971" s="T364">ptcl</ta>
            <ta e="T368" id="Seg_9972" s="T367">n-n:case</ta>
            <ta e="T369" id="Seg_9973" s="T368">v-v:n.fin</ta>
            <ta e="T370" id="Seg_9974" s="T369">v-v:n.fin</ta>
            <ta e="T373" id="Seg_9975" s="T372">ptcl</ta>
            <ta e="T374" id="Seg_9976" s="T373">n-n:case</ta>
            <ta e="T375" id="Seg_9977" s="T374">v-v:n.fin</ta>
            <ta e="T376" id="Seg_9978" s="T375">ptcl</ta>
            <ta e="T377" id="Seg_9979" s="T376">pers</ta>
            <ta e="T378" id="Seg_9980" s="T377">n-n:case.poss</ta>
            <ta e="T379" id="Seg_9981" s="T378">quant</ta>
            <ta e="T380" id="Seg_9982" s="T379">v-v:n.fin</ta>
            <ta e="T381" id="Seg_9983" s="T380">v-v:pn</ta>
            <ta e="T382" id="Seg_9984" s="T381">que</ta>
            <ta e="T383" id="Seg_9985" s="T382">que-n:case=ptcl</ta>
            <ta e="T384" id="Seg_9986" s="T383">v-v:pn</ta>
            <ta e="T385" id="Seg_9987" s="T384">pers</ta>
            <ta e="T386" id="Seg_9988" s="T385">v-v:tense-v:pn</ta>
            <ta e="T387" id="Seg_9989" s="T386">propr</ta>
            <ta e="T388" id="Seg_9990" s="T387">n-n:case</ta>
            <ta e="T389" id="Seg_9991" s="T388">conj</ta>
            <ta e="T390" id="Seg_9992" s="T389">ptcl</ta>
            <ta e="T391" id="Seg_9993" s="T390">n-n:case</ta>
            <ta e="T392" id="Seg_9994" s="T391">v-v:n.fin</ta>
            <ta e="T393" id="Seg_9995" s="T392">ptcl</ta>
            <ta e="T394" id="Seg_9996" s="T393">n-n:case</ta>
            <ta e="T395" id="Seg_9997" s="T394">v-v:n.fin</ta>
            <ta e="T396" id="Seg_9998" s="T395">ptcl</ta>
            <ta e="T399" id="Seg_9999" s="T398">n-n:case.poss</ta>
            <ta e="T400" id="Seg_10000" s="T399">n-n:case</ta>
            <ta e="T401" id="Seg_10001" s="T400">v-v:n.fin</ta>
            <ta e="T402" id="Seg_10002" s="T401">ptcl</ta>
            <ta e="T409" id="Seg_10003" s="T408">ptcl</ta>
            <ta e="T410" id="Seg_10004" s="T409">n-n:case</ta>
            <ta e="T416" id="Seg_10005" s="T415">pers</ta>
            <ta e="T417" id="Seg_10006" s="T416">n-n:case</ta>
            <ta e="T418" id="Seg_10007" s="T417">adv</ta>
            <ta e="T419" id="Seg_10008" s="T418">v-v:tense-v:pn</ta>
            <ta e="T422" id="Seg_10009" s="T421">adv</ta>
            <ta e="T425" id="Seg_10010" s="T424">dempro-n:num</ta>
            <ta e="T426" id="Seg_10011" s="T425">n-n:case</ta>
            <ta e="T427" id="Seg_10012" s="T426">v-v:tense-v:pn</ta>
            <ta e="T428" id="Seg_10013" s="T427">ptcl</ta>
            <ta e="T429" id="Seg_10014" s="T428">n-n:case</ta>
            <ta e="T430" id="Seg_10015" s="T429">v-v:tense-v:pn</ta>
            <ta e="T431" id="Seg_10016" s="T430">n-n:case</ta>
            <ta e="T432" id="Seg_10017" s="T431">v-v:tense-v:pn</ta>
            <ta e="T434" id="Seg_10018" s="T433">v-v:n.fin</ta>
            <ta e="T435" id="Seg_10019" s="T434">ptcl</ta>
            <ta e="T436" id="Seg_10020" s="T435">conj</ta>
            <ta e="T437" id="Seg_10021" s="T436">v-v:n.fin</ta>
            <ta e="T438" id="Seg_10022" s="T437">n-n:num</ta>
            <ta e="T439" id="Seg_10023" s="T438">v-v:n.fin</ta>
            <ta e="T440" id="Seg_10024" s="T439">n-n:case</ta>
            <ta e="T441" id="Seg_10025" s="T440">v-v:n.fin</ta>
            <ta e="T442" id="Seg_10026" s="T441">conj</ta>
            <ta e="T443" id="Seg_10027" s="T442">n-n:case</ta>
            <ta e="T444" id="Seg_10028" s="T443">v-v:n.fin</ta>
            <ta e="T445" id="Seg_10029" s="T444">conj</ta>
            <ta e="T446" id="Seg_10030" s="T445">v-v:n.fin</ta>
            <ta e="T447" id="Seg_10031" s="T446">n-n:case</ta>
            <ta e="T448" id="Seg_10032" s="T447">ptcl</ta>
            <ta e="T449" id="Seg_10033" s="T448">v-v:n.fin</ta>
            <ta e="T454" id="Seg_10034" s="T453">v-v:n.fin</ta>
            <ta e="T459" id="Seg_10035" s="T458">v-v:n.fin</ta>
            <ta e="T460" id="Seg_10036" s="T459">n-n:case</ta>
            <ta e="T461" id="Seg_10037" s="T460">v-v:n.fin</ta>
            <ta e="T462" id="Seg_10038" s="T461">adv</ta>
            <ta e="T463" id="Seg_10039" s="T462">v-v:tense-v:pn</ta>
            <ta e="T464" id="Seg_10040" s="T463">v-v:n.fin</ta>
            <ta e="T465" id="Seg_10041" s="T464">ptcl</ta>
            <ta e="T466" id="Seg_10042" s="T465">adv</ta>
            <ta e="T467" id="Seg_10043" s="T466">v-v&gt;v-v:n.fin</ta>
            <ta e="T468" id="Seg_10044" s="T467">ptcl</ta>
            <ta e="T469" id="Seg_10045" s="T468">adv</ta>
            <ta e="T476" id="Seg_10046" s="T475">adv</ta>
            <ta e="T477" id="Seg_10047" s="T476">v-v:n.fin</ta>
            <ta e="T478" id="Seg_10048" s="T477">ptcl</ta>
            <ta e="T481" id="Seg_10049" s="T480">adv</ta>
            <ta e="T482" id="Seg_10050" s="T481">ptcl</ta>
            <ta e="T483" id="Seg_10051" s="T482">v-v:n.fin</ta>
            <ta e="T484" id="Seg_10052" s="T483">adv</ta>
            <ta e="T485" id="Seg_10053" s="T484">v-v:n.fin</ta>
            <ta e="T486" id="Seg_10054" s="T485">ptcl</ta>
            <ta e="T489" id="Seg_10055" s="T488">adv</ta>
            <ta e="T490" id="Seg_10056" s="T489">adj-n:case</ta>
            <ta e="T491" id="Seg_10057" s="T490">n-n:case</ta>
            <ta e="T494" id="Seg_10058" s="T493">n-n:case</ta>
            <ta e="T495" id="Seg_10059" s="T494">adv</ta>
            <ta e="T496" id="Seg_10060" s="T495">quant</ta>
            <ta e="T497" id="Seg_10061" s="T496">adj-n:num</ta>
            <ta e="T498" id="Seg_10062" s="T497">quant</ta>
            <ta e="T499" id="Seg_10063" s="T498">n.[n:case]</ta>
            <ta e="T500" id="Seg_10064" s="T499">v-v&gt;v-v:pn</ta>
            <ta e="T501" id="Seg_10065" s="T500">v-v&gt;v-v:pn</ta>
            <ta e="T503" id="Seg_10066" s="T502">v-v&gt;v-v:pn</ta>
            <ta e="T504" id="Seg_10067" s="T503">n</ta>
            <ta e="T505" id="Seg_10068" s="T504">v-v&gt;v-v:pn</ta>
            <ta e="T506" id="Seg_10069" s="T505">adj-n:case</ta>
            <ta e="T507" id="Seg_10070" s="T506">n-n:case</ta>
            <ta e="T508" id="Seg_10071" s="T507">n-n:case</ta>
            <ta e="T509" id="Seg_10072" s="T508">quant</ta>
            <ta e="T512" id="Seg_10073" s="T511">v-v:tense-v:pn</ta>
            <ta e="T513" id="Seg_10074" s="T512">adj-n:case</ta>
            <ta e="T514" id="Seg_10075" s="T513">adj-n:case</ta>
            <ta e="T515" id="Seg_10076" s="T514">adv</ta>
            <ta e="T520" id="Seg_10077" s="T519">v-v:mood.pn</ta>
            <ta e="T521" id="Seg_10078" s="T520">ptcl</ta>
            <ta e="T522" id="Seg_10079" s="T521">v-v:tense-v:pn</ta>
            <ta e="T523" id="Seg_10080" s="T522">adv</ta>
            <ta e="T524" id="Seg_10081" s="T523">v-v:tense-v:pn</ta>
            <ta e="T525" id="Seg_10082" s="T524">v-v:mood.pn</ta>
            <ta e="T526" id="Seg_10083" s="T525">v</ta>
            <ta e="T527" id="Seg_10084" s="T526">v-v:mood.pn</ta>
            <ta e="T528" id="Seg_10085" s="T527">adj-n:case</ta>
            <ta e="T529" id="Seg_10086" s="T528">n-n:case</ta>
            <ta e="T530" id="Seg_10087" s="T529">v-v:tense-v:pn</ta>
            <ta e="T531" id="Seg_10088" s="T530">n-n:case</ta>
            <ta e="T532" id="Seg_10089" s="T531">n</ta>
            <ta e="T533" id="Seg_10090" s="T532">n-n:case</ta>
            <ta e="T534" id="Seg_10091" s="T533">n-n:case</ta>
            <ta e="T535" id="Seg_10092" s="T534">v-v:tense-v:pn</ta>
            <ta e="T536" id="Seg_10093" s="T535">v-v:mood.pn</ta>
            <ta e="T538" id="Seg_10094" s="T537">n-n:case</ta>
            <ta e="T541" id="Seg_10095" s="T540">v-v:mood.pn</ta>
            <ta e="T544" id="Seg_10096" s="T543">pers</ta>
            <ta e="T545" id="Seg_10097" s="T544">adv</ta>
            <ta e="T546" id="Seg_10098" s="T545">v-v&gt;v-v:pn</ta>
            <ta e="T547" id="Seg_10099" s="T546">v-v:mood.pn</ta>
            <ta e="T548" id="Seg_10100" s="T547">ptcl</ta>
            <ta e="T549" id="Seg_10101" s="T548">adj-n:case</ta>
            <ta e="T550" id="Seg_10102" s="T549">n-n:case</ta>
            <ta e="T551" id="Seg_10103" s="T550">v-v:tense-v:pn</ta>
            <ta e="T552" id="Seg_10104" s="T551">pers</ta>
            <ta e="T553" id="Seg_10105" s="T552">v-v:tense-v:pn</ta>
            <ta e="T554" id="Seg_10106" s="T553">quant</ta>
            <ta e="T555" id="Seg_10107" s="T554">adv</ta>
            <ta e="T556" id="Seg_10108" s="T555">quant</ta>
            <ta e="T557" id="Seg_10109" s="T556">v-v&gt;v-v:tense-v&gt;v</ta>
            <ta e="T567" id="Seg_10110" s="T566">ptcl</ta>
            <ta e="T568" id="Seg_10111" s="T567">v-v:mood.pn</ta>
            <ta e="T569" id="Seg_10112" s="T568">aux-v:mood.pn</ta>
            <ta e="T570" id="Seg_10113" s="T569">v-v:n.fin</ta>
            <ta e="T574" id="Seg_10114" s="T573">aux-v:mood.pn</ta>
            <ta e="T575" id="Seg_10115" s="T574">v-v:ins-v:n.fin</ta>
            <ta e="T578" id="Seg_10116" s="T577">aux-v:mood.pn</ta>
            <ta e="T579" id="Seg_10117" s="T578">v-v&gt;v-v:n.fin</ta>
            <ta e="T583" id="Seg_10118" s="T582">aux-v:mood.pn</ta>
            <ta e="T584" id="Seg_10119" s="T583">v-v:ins-v:n.fin</ta>
            <ta e="T587" id="Seg_10120" s="T586">n-n:case.poss</ta>
            <ta e="T588" id="Seg_10121" s="T587">v-v&gt;v-v:pn</ta>
            <ta e="T589" id="Seg_10122" s="T588">v-v:mood.pn</ta>
            <ta e="T590" id="Seg_10123" s="T589">n-n:case.poss</ta>
            <ta e="T593" id="Seg_10124" s="T592">aux-v:mood.pn</ta>
            <ta e="T594" id="Seg_10125" s="T593">v-v:n.fin</ta>
            <ta e="T595" id="Seg_10126" s="T594">aux-v:mood.pn</ta>
            <ta e="T596" id="Seg_10127" s="T595">v-v:n.fin</ta>
            <ta e="T597" id="Seg_10128" s="T596">aux-v:mood.pn</ta>
            <ta e="T598" id="Seg_10129" s="T597">v-v:n.fin</ta>
            <ta e="T599" id="Seg_10130" s="T598">adv</ta>
            <ta e="T600" id="Seg_10131" s="T599">adj-n:case</ta>
            <ta e="T601" id="Seg_10132" s="T600">adv</ta>
            <ta e="T602" id="Seg_10133" s="T601">adj-n:case</ta>
            <ta e="T603" id="Seg_10134" s="T602">adv</ta>
            <ta e="T604" id="Seg_10135" s="T603">adj-n:case</ta>
            <ta e="T605" id="Seg_10136" s="T604">adv</ta>
            <ta e="T607" id="Seg_10137" s="T606">adj</ta>
            <ta e="T608" id="Seg_10138" s="T607">adv</ta>
            <ta e="T609" id="Seg_10139" s="T608">adj-n:case</ta>
            <ta e="T613" id="Seg_10140" s="T612">adv</ta>
            <ta e="T614" id="Seg_10141" s="T613">adj-n:case</ta>
            <ta e="T619" id="Seg_10142" s="T618">ptcl</ta>
            <ta e="T620" id="Seg_10143" s="T619">n-n:case</ta>
            <ta e="T621" id="Seg_10144" s="T620">v-v:tense-v:pn</ta>
            <ta e="T622" id="Seg_10145" s="T621">adv</ta>
            <ta e="T623" id="Seg_10146" s="T622">v-v:tense-v:pn</ta>
            <ta e="T624" id="Seg_10147" s="T623">pers</ta>
            <ta e="T625" id="Seg_10148" s="T624">ptcl</ta>
            <ta e="T626" id="Seg_10149" s="T625">v-v:tense-v:pn</ta>
            <ta e="T627" id="Seg_10150" s="T626">ptcl</ta>
            <ta e="T628" id="Seg_10151" s="T627">ptcl</ta>
            <ta e="T629" id="Seg_10152" s="T628">v-v:n.fin</ta>
            <ta e="T630" id="Seg_10153" s="T629">ptcl</ta>
            <ta e="T631" id="Seg_10154" s="T630">ptcl</ta>
            <ta e="T632" id="Seg_10155" s="T631">adj-n:case</ta>
            <ta e="T633" id="Seg_10156" s="T632">v-v:tense-v:pn</ta>
            <ta e="T634" id="Seg_10157" s="T633">n-n:case.poss</ta>
            <ta e="T635" id="Seg_10158" s="T634">v-v:tense-v:pn</ta>
            <ta e="T636" id="Seg_10159" s="T635">quant</ta>
            <ta e="T637" id="Seg_10160" s="T636">n-n:case</ta>
            <ta e="T638" id="Seg_10161" s="T637">v-v&gt;v-v:pn</ta>
            <ta e="T639" id="Seg_10162" s="T638">v-v:mood.pn</ta>
            <ta e="T640" id="Seg_10163" s="T639">que=ptcl</ta>
            <ta e="T641" id="Seg_10164" s="T640">n-n:case.poss</ta>
            <ta e="T642" id="Seg_10165" s="T641">v-v:n.fin</ta>
            <ta e="T647" id="Seg_10166" s="T645">adv</ta>
            <ta e="T648" id="Seg_10167" s="T647">n-n:case</ta>
            <ta e="T649" id="Seg_10168" s="T648">n-n:case</ta>
            <ta e="T650" id="Seg_10169" s="T649">v-v:tense-v:pn</ta>
            <ta e="T651" id="Seg_10170" s="T650">pers</ta>
            <ta e="T652" id="Seg_10171" s="T651">adv</ta>
            <ta e="T654" id="Seg_10172" s="T652">ptcl</ta>
            <ta e="T655" id="Seg_10173" s="T654">n-n:case</ta>
            <ta e="T656" id="Seg_10174" s="T655">n-n:case</ta>
            <ta e="T657" id="Seg_10175" s="T656">quant</ta>
            <ta e="T658" id="Seg_10176" s="T657">v-v:tense-v:pn</ta>
            <ta e="T659" id="Seg_10177" s="T658">n-n:case</ta>
            <ta e="T660" id="Seg_10178" s="T659">quant</ta>
            <ta e="T661" id="Seg_10179" s="T660">v-v:tense-v:pn</ta>
            <ta e="T662" id="Seg_10180" s="T661">n-n:case</ta>
            <ta e="T663" id="Seg_10181" s="T662">adv</ta>
            <ta e="T664" id="Seg_10182" s="T663">adj-n:case</ta>
            <ta e="T665" id="Seg_10183" s="T664">pers</ta>
            <ta e="T666" id="Seg_10184" s="T665">adv</ta>
            <ta e="T667" id="Seg_10185" s="T666">v-v:tense-v:pn</ta>
            <ta e="T668" id="Seg_10186" s="T667">conj</ta>
            <ta e="T669" id="Seg_10187" s="T668">adv</ta>
            <ta e="T670" id="Seg_10188" s="T669">v-v:tense-v:pn</ta>
            <ta e="T671" id="Seg_10189" s="T670">n-n:case.poss</ta>
            <ta e="T672" id="Seg_10190" s="T671">v-v:tense-v:pn</ta>
            <ta e="T673" id="Seg_10191" s="T672">n-n:case.poss</ta>
            <ta e="T674" id="Seg_10192" s="T673">v-v:tense-v:pn</ta>
            <ta e="T675" id="Seg_10193" s="T674">n-n:case.poss</ta>
            <ta e="T676" id="Seg_10194" s="T675">quant</ta>
            <ta e="T677" id="Seg_10195" s="T676">v-v:tense-v:pn</ta>
            <ta e="T678" id="Seg_10196" s="T677">adv</ta>
            <ta e="T681" id="Seg_10197" s="T680">adj-n:case</ta>
            <ta e="T682" id="Seg_10198" s="T681">n-n:case</ta>
            <ta e="T683" id="Seg_10199" s="T682">n-n:case</ta>
            <ta e="T684" id="Seg_10200" s="T683">quant</ta>
            <ta e="T685" id="Seg_10201" s="T684">v-v&gt;v-v:pn</ta>
            <ta e="T686" id="Seg_10202" s="T685">adv</ta>
            <ta e="T687" id="Seg_10203" s="T686">que-n:case=ptcl</ta>
            <ta e="T688" id="Seg_10204" s="T687">v-v:pn</ta>
            <ta e="T689" id="Seg_10205" s="T688">adv</ta>
            <ta e="T690" id="Seg_10206" s="T689">adj-n:case</ta>
            <ta e="T691" id="Seg_10207" s="T690">n-n:case</ta>
            <ta e="T692" id="Seg_10208" s="T691">n-n:case</ta>
            <ta e="T693" id="Seg_10209" s="T692">quant</ta>
            <ta e="T694" id="Seg_10210" s="T693">v-v&gt;v-v:pn</ta>
            <ta e="T695" id="Seg_10211" s="T694">n-n:case</ta>
            <ta e="T696" id="Seg_10212" s="T695">v-v&gt;v-v:pn</ta>
            <ta e="T697" id="Seg_10213" s="T696">n-n:num</ta>
            <ta e="T698" id="Seg_10214" s="T697">v-v&gt;v-v:pn</ta>
            <ta e="T699" id="Seg_10215" s="T698">quant</ta>
            <ta e="T700" id="Seg_10216" s="T699">ptcl</ta>
            <ta e="T701" id="Seg_10217" s="T700">v-v:n.fin</ta>
            <ta e="T702" id="Seg_10218" s="T701">n-n:case</ta>
            <ta e="T703" id="Seg_10219" s="T702">v-v:n.fin</ta>
            <ta e="T704" id="Seg_10220" s="T703">n-n:case</ta>
            <ta e="T705" id="Seg_10221" s="T704">ptcl</ta>
            <ta e="T706" id="Seg_10222" s="T705">v-v:n.fin</ta>
            <ta e="T707" id="Seg_10223" s="T706">ptcl</ta>
            <ta e="T708" id="Seg_10224" s="T707">n-n:case</ta>
            <ta e="T709" id="Seg_10225" s="T708">v-v:n.fin</ta>
            <ta e="T710" id="Seg_10226" s="T709">n-n:case</ta>
            <ta e="T711" id="Seg_10227" s="T710">v-v:n.fin</ta>
            <ta e="T712" id="Seg_10228" s="T711">ptcl</ta>
            <ta e="T713" id="Seg_10229" s="T712">n-n:case</ta>
            <ta e="T714" id="Seg_10230" s="T713">v-v:n.fin</ta>
            <ta e="T715" id="Seg_10231" s="T714">n</ta>
            <ta e="T716" id="Seg_10232" s="T715">v-v:n.fin</ta>
            <ta e="T717" id="Seg_10233" s="T716">n-n:num</ta>
            <ta e="T718" id="Seg_10234" s="T717">v-v:n.fin</ta>
            <ta e="T719" id="Seg_10235" s="T718">n-n:case</ta>
            <ta e="T720" id="Seg_10236" s="T719">ptcl</ta>
            <ta e="T721" id="Seg_10237" s="T720">v-v:n.fin</ta>
            <ta e="T722" id="Seg_10238" s="T721">n-n:case</ta>
            <ta e="T723" id="Seg_10239" s="T722">n-n:case</ta>
            <ta e="T724" id="Seg_10240" s="T723">adv</ta>
            <ta e="T725" id="Seg_10241" s="T724">v-v:tense-v:pn</ta>
            <ta e="T726" id="Seg_10242" s="T725">n-n:case</ta>
            <ta e="T727" id="Seg_10243" s="T726">v-v:tense-v:pn</ta>
            <ta e="T728" id="Seg_10244" s="T727">n-n:case</ta>
            <ta e="T729" id="Seg_10245" s="T728">v-v:tense-v:pn</ta>
            <ta e="T730" id="Seg_10246" s="T729">n-n:case</ta>
            <ta e="T731" id="Seg_10247" s="T730">v-v:tense-v:pn</ta>
            <ta e="T732" id="Seg_10248" s="T731">n-n:case</ta>
            <ta e="T733" id="Seg_10249" s="T732">v-v:tense-v:pn</ta>
            <ta e="T734" id="Seg_10250" s="T733">n-n:case</ta>
            <ta e="T735" id="Seg_10251" s="T734">v-v:tense-v:pn</ta>
            <ta e="T736" id="Seg_10252" s="T735">n-n:case</ta>
            <ta e="T737" id="Seg_10253" s="T736">n-n:num</ta>
            <ta e="T738" id="Seg_10254" s="T737">quant</ta>
            <ta e="T739" id="Seg_10255" s="T738">n-n:case</ta>
            <ta e="T740" id="Seg_10256" s="T739">v-v:tense-v:pn</ta>
            <ta e="T741" id="Seg_10257" s="T740">v-v&gt;v-v:pn</ta>
            <ta e="T742" id="Seg_10258" s="T741">adv</ta>
            <ta e="T743" id="Seg_10259" s="T742">dempro-n:case</ta>
            <ta e="T744" id="Seg_10260" s="T743">n-n:case</ta>
            <ta e="T745" id="Seg_10261" s="T744">v-v:tense-v:pn</ta>
            <ta e="T747" id="Seg_10262" s="T746">n-n:case.poss</ta>
            <ta e="T749" id="Seg_10263" s="T748">ptcl</ta>
            <ta e="T750" id="Seg_10264" s="T749">n-n:case.poss</ta>
            <ta e="T751" id="Seg_10265" s="T750">v-v:n.fin</ta>
            <ta e="T752" id="Seg_10266" s="T751">ptcl</ta>
            <ta e="T755" id="Seg_10267" s="T754">n-n:case.poss</ta>
            <ta e="T756" id="Seg_10268" s="T755">quant</ta>
            <ta e="T757" id="Seg_10269" s="T756">v-v&gt;v-v:pn</ta>
            <ta e="T758" id="Seg_10270" s="T757">ptcl</ta>
            <ta e="T759" id="Seg_10271" s="T758">dempro-n:case</ta>
            <ta e="T760" id="Seg_10272" s="T759">v-v:n.fin</ta>
            <ta e="T761" id="Seg_10273" s="T760">ptcl</ta>
            <ta e="T762" id="Seg_10274" s="T761">v-v:n.fin</ta>
            <ta e="T763" id="Seg_10275" s="T762">v-v:n.fin</ta>
            <ta e="T764" id="Seg_10276" s="T763">ptcl</ta>
            <ta e="T765" id="Seg_10277" s="T764">v-v:n.fin</ta>
            <ta e="T766" id="Seg_10278" s="T765">v-v:n.fin</ta>
            <ta e="T771" id="Seg_10279" s="T770">v-v:mood-pn</ta>
            <ta e="T772" id="Seg_10280" s="T771">n-n:case-poss</ta>
            <ta e="T773" id="Seg_10281" s="T772">ptcl</ta>
            <ta e="T774" id="Seg_10282" s="T773">ptcl</ta>
            <ta e="T775" id="Seg_10283" s="T774">v-v&gt;v-v:pn</ta>
            <ta e="T776" id="Seg_10284" s="T775">v-v:mood.pn</ta>
            <ta e="T777" id="Seg_10285" s="T776">adv</ta>
            <ta e="T778" id="Seg_10286" s="T777">n-n&gt;v-v:n.fin</ta>
            <ta e="T779" id="Seg_10287" s="T778">v-v:tense-v:pn</ta>
            <ta e="T780" id="Seg_10288" s="T779">n-n:case</ta>
            <ta e="T781" id="Seg_10289" s="T780">v-v:tense-v:pn</ta>
            <ta e="T782" id="Seg_10290" s="T781">adv</ta>
            <ta e="T783" id="Seg_10291" s="T782">v-v:tense-v:pn</ta>
            <ta e="T784" id="Seg_10292" s="T783">aux-v:mood.pn</ta>
            <ta e="T785" id="Seg_10293" s="T784">v-v:ins-v:n.fin</ta>
            <ta e="T786" id="Seg_10294" s="T785">conj</ta>
            <ta e="T787" id="Seg_10295" s="T786">adv</ta>
            <ta e="T788" id="Seg_10296" s="T787">v-v:tense-v:pn</ta>
            <ta e="T789" id="Seg_10297" s="T788">n-n:case-n:case.poss</ta>
            <ta e="T790" id="Seg_10298" s="T789">adv</ta>
            <ta e="T791" id="Seg_10299" s="T790">n-n:case</ta>
            <ta e="T792" id="Seg_10300" s="T791">v-v:tense-v:pn</ta>
            <ta e="T793" id="Seg_10301" s="T792">ptcl</ta>
            <ta e="T794" id="Seg_10302" s="T793">adv</ta>
            <ta e="T795" id="Seg_10303" s="T794">n-n:case</ta>
            <ta e="T796" id="Seg_10304" s="T795">v-v:n.fin</ta>
            <ta e="T797" id="Seg_10305" s="T796">n-n:case</ta>
            <ta e="T798" id="Seg_10306" s="T797">v-v:n.fin</ta>
            <ta e="T799" id="Seg_10307" s="T798">conj</ta>
            <ta e="T800" id="Seg_10308" s="T799">n-n:case</ta>
            <ta e="T801" id="Seg_10309" s="T800">v-v:n.fin</ta>
            <ta e="T802" id="Seg_10310" s="T801">adv</ta>
            <ta e="T803" id="Seg_10311" s="T802">v-v:tense-v:pn</ta>
            <ta e="T804" id="Seg_10312" s="T803">aux-v:mood.pn</ta>
            <ta e="T805" id="Seg_10313" s="T804">v</ta>
            <ta e="T806" id="Seg_10314" s="T805">ptcl</ta>
            <ta e="T807" id="Seg_10315" s="T806">adj-n:case</ta>
            <ta e="T810" id="Seg_10316" s="T809">n-n:case</ta>
            <ta e="T811" id="Seg_10317" s="T810">v-v:ins-v:mood.pn</ta>
            <ta e="T812" id="Seg_10318" s="T811">n-n:case</ta>
            <ta e="T814" id="Seg_10319" s="T812">v-v:tense-v:pn</ta>
            <ta e="T815" id="Seg_10320" s="T814">n-n:case</ta>
            <ta e="T816" id="Seg_10321" s="T815">n-n:case</ta>
            <ta e="T817" id="Seg_10322" s="T816">refl-n:case.poss</ta>
            <ta e="T818" id="Seg_10323" s="T817">quant</ta>
            <ta e="T819" id="Seg_10324" s="T818">adj-n:case</ta>
            <ta e="T820" id="Seg_10325" s="T819">conj</ta>
            <ta e="T821" id="Seg_10326" s="T820">dempro-n:case</ta>
            <ta e="T822" id="Seg_10327" s="T821">n-n:case</ta>
            <ta e="T823" id="Seg_10328" s="T822">v-v:tense-v:pn</ta>
            <ta e="T824" id="Seg_10329" s="T823">adj-n:case</ta>
            <ta e="T825" id="Seg_10330" s="T824">adv</ta>
            <ta e="T826" id="Seg_10331" s="T825">conj</ta>
            <ta e="T827" id="Seg_10332" s="T826">v-v:mood.pn</ta>
            <ta e="T828" id="Seg_10333" s="T827">n-n:case.poss</ta>
            <ta e="T829" id="Seg_10334" s="T828">v-v:tense-v:pn</ta>
            <ta e="T831" id="Seg_10335" s="T830">dempro-n:num</ta>
            <ta e="T832" id="Seg_10336" s="T831">n-n:case</ta>
            <ta e="T833" id="Seg_10337" s="T832">ptcl</ta>
            <ta e="T834" id="Seg_10338" s="T833">v-v:tense-v:pn</ta>
            <ta e="T835" id="Seg_10339" s="T834">pers</ta>
            <ta e="T836" id="Seg_10340" s="T835">n-n:case.poss</ta>
            <ta e="T837" id="Seg_10341" s="T836">dempro-n:num-n:case</ta>
            <ta e="T838" id="Seg_10342" s="T837">v-v:tense-v:pn</ta>
            <ta e="T839" id="Seg_10343" s="T838">dempro-n:num</ta>
            <ta e="T840" id="Seg_10344" s="T839">n-n:case</ta>
            <ta e="T841" id="Seg_10345" s="T840">quant</ta>
            <ta e="T842" id="Seg_10346" s="T841">v-v:tense-v:pn</ta>
            <ta e="T843" id="Seg_10347" s="T842">quant</ta>
            <ta e="T844" id="Seg_10348" s="T843">n-n:case</ta>
            <ta e="T845" id="Seg_10349" s="T844">v-v:tense-v:pn</ta>
            <ta e="T846" id="Seg_10350" s="T845">n-n:case</ta>
            <ta e="T847" id="Seg_10351" s="T846">adv</ta>
            <ta e="T848" id="Seg_10352" s="T847">v-v:tense-v:pn</ta>
            <ta e="T849" id="Seg_10353" s="T848">conj</ta>
            <ta e="T850" id="Seg_10354" s="T849">adv</ta>
            <ta e="T851" id="Seg_10355" s="T850">que-n:case=ptcl</ta>
            <ta e="T854" id="Seg_10356" s="T853">v-v:pn</ta>
            <ta e="T855" id="Seg_10357" s="T854">que</ta>
            <ta e="T856" id="Seg_10358" s="T855">v-v&gt;v-v:pn</ta>
            <ta e="T857" id="Seg_10359" s="T856">ptcl</ta>
            <ta e="T858" id="Seg_10360" s="T857">v-v:tense-v:pn</ta>
            <ta e="T859" id="Seg_10361" s="T858">v-v:ins-v:mood.pn</ta>
            <ta e="T860" id="Seg_10362" s="T859">que=ptcl</ta>
            <ta e="T861" id="Seg_10363" s="T860">n-n:case.poss</ta>
            <ta e="T862" id="Seg_10364" s="T861">adv</ta>
            <ta e="T865" id="Seg_10365" s="T864">n-n:case.poss</ta>
            <ta e="T866" id="Seg_10366" s="T865">adv</ta>
            <ta e="T867" id="Seg_10367" s="T866">v-v:tense-v:pn</ta>
            <ta e="T868" id="Seg_10368" s="T867">que-n:case=ptcl</ta>
            <ta e="T869" id="Seg_10369" s="T868">ptcl</ta>
            <ta e="T870" id="Seg_10370" s="T869">v-v:tense-v:pn</ta>
            <ta e="T871" id="Seg_10371" s="T870">v-v:ins-v:mood.pn</ta>
            <ta e="T875" id="Seg_10372" s="T874">que</ta>
            <ta e="T876" id="Seg_10373" s="T875">v-v:tense-v:pn</ta>
            <ta e="T877" id="Seg_10374" s="T876">que</ta>
            <ta e="T878" id="Seg_10375" s="T877">v-v:tense-v:pn</ta>
            <ta e="T879" id="Seg_10376" s="T878">v-v:mood.pn</ta>
            <ta e="T880" id="Seg_10377" s="T879">adv</ta>
            <ta e="T881" id="Seg_10378" s="T880">que=ptcl</ta>
            <ta e="T882" id="Seg_10379" s="T881">v-v:mood.pn</ta>
            <ta e="T883" id="Seg_10380" s="T882">pers</ta>
            <ta e="T886" id="Seg_10381" s="T885">n-n:case</ta>
            <ta e="T887" id="Seg_10382" s="T886">n-n:case.poss-n:case</ta>
            <ta e="T888" id="Seg_10383" s="T887">v-v:tense-v:pn</ta>
            <ta e="T889" id="Seg_10384" s="T888">dempro-n:num</ta>
            <ta e="T890" id="Seg_10385" s="T889">n-n:num-n:case</ta>
            <ta e="T891" id="Seg_10386" s="T890">v-v:tense-v:pn</ta>
            <ta e="T892" id="Seg_10387" s="T891">n-n:case</ta>
            <ta e="T893" id="Seg_10388" s="T892">conj</ta>
            <ta e="T894" id="Seg_10389" s="T893">n-n:case</ta>
            <ta e="T895" id="Seg_10390" s="T894">n</ta>
            <ta e="T896" id="Seg_10391" s="T895">v-v:tense-v:pn</ta>
            <ta e="T897" id="Seg_10392" s="T896">n-n:case</ta>
            <ta e="T898" id="Seg_10393" s="T897">v-v:tense-v:pn</ta>
            <ta e="T899" id="Seg_10394" s="T898">dempro-n:case</ta>
            <ta e="T900" id="Seg_10395" s="T899">n-n:case</ta>
            <ta e="T901" id="Seg_10396" s="T900">n-n:case</ta>
            <ta e="T902" id="Seg_10397" s="T901">v-v:tense-v:pn</ta>
            <ta e="T903" id="Seg_10398" s="T902">n-n:num-n:case</ta>
            <ta e="T904" id="Seg_10399" s="T903">v-v:tense-v:pn</ta>
            <ta e="T905" id="Seg_10400" s="T904">n-n:case</ta>
            <ta e="T906" id="Seg_10401" s="T905">v-v:tense-v:pn</ta>
            <ta e="T907" id="Seg_10402" s="T906">dempro-n:num</ta>
            <ta e="T908" id="Seg_10403" s="T907">v-v:tense-v:pn</ta>
            <ta e="T910" id="Seg_10404" s="T909">dempro-n:case</ta>
            <ta e="T911" id="Seg_10405" s="T910">num-num&gt;num</ta>
            <ta e="T912" id="Seg_10406" s="T911">n-n:case</ta>
            <ta e="T913" id="Seg_10407" s="T912">v-v:tense-v:pn</ta>
            <ta e="T914" id="Seg_10408" s="T913">n-n:case</ta>
            <ta e="T915" id="Seg_10409" s="T914">conj</ta>
            <ta e="T916" id="Seg_10410" s="T915">n-n:case</ta>
            <ta e="T917" id="Seg_10411" s="T916">pers</ta>
            <ta e="T918" id="Seg_10412" s="T917">ptcl</ta>
            <ta e="T919" id="Seg_10413" s="T918">que=ptcl</ta>
            <ta e="T920" id="Seg_10414" s="T919">v-v:tense-v:pn</ta>
            <ta e="T921" id="Seg_10415" s="T920">n-n:case</ta>
            <ta e="T922" id="Seg_10416" s="T921">v-v:tense-v:pn</ta>
            <ta e="T925" id="Seg_10417" s="T924">adj</ta>
            <ta e="T926" id="Seg_10418" s="T925">n-n:case</ta>
            <ta e="T927" id="Seg_10419" s="T926">v-v:tense-v:pn</ta>
            <ta e="T928" id="Seg_10420" s="T927">ptcl</ta>
            <ta e="T929" id="Seg_10421" s="T928">n</ta>
            <ta e="T930" id="Seg_10422" s="T929">v-v:tense-v:pn</ta>
            <ta e="T931" id="Seg_10423" s="T930">pers</ta>
            <ta e="T932" id="Seg_10424" s="T931">n-n:case</ta>
            <ta e="T933" id="Seg_10425" s="T932">ptcl</ta>
            <ta e="T934" id="Seg_10426" s="T933">v-v:tense-v:pn</ta>
            <ta e="T935" id="Seg_10427" s="T934">n-n:case</ta>
            <ta e="T936" id="Seg_10428" s="T935">ptcl</ta>
            <ta e="T937" id="Seg_10429" s="T936">v-v:tense-v:pn</ta>
            <ta e="T938" id="Seg_10430" s="T937">pers</ta>
            <ta e="T939" id="Seg_10431" s="T938">n-n:case</ta>
            <ta e="T940" id="Seg_10432" s="T939">n-n:case.poss</ta>
            <ta e="T941" id="Seg_10433" s="T940">adj-n:case</ta>
            <ta e="T942" id="Seg_10434" s="T941">n-n:case</ta>
            <ta e="T943" id="Seg_10435" s="T942">quant</ta>
            <ta e="T946" id="Seg_10436" s="T945">num-n:case</ta>
            <ta e="T947" id="Seg_10437" s="T946">n-n:case</ta>
            <ta e="T948" id="Seg_10438" s="T947">num-n:case</ta>
            <ta e="T949" id="Seg_10439" s="T948">n-n:case</ta>
            <ta e="T950" id="Seg_10440" s="T949">n-n:case</ta>
            <ta e="T951" id="Seg_10441" s="T950">n-n:num-n:case</ta>
            <ta e="T952" id="Seg_10442" s="T951">refl-n:case.poss</ta>
            <ta e="T953" id="Seg_10443" s="T952">n-n:case</ta>
            <ta e="T955" id="Seg_10444" s="T954">n-n:num-n:case.poss</ta>
            <ta e="T956" id="Seg_10445" s="T955">num-n:case</ta>
            <ta e="T957" id="Seg_10446" s="T956">adj-n:case</ta>
            <ta e="T958" id="Seg_10447" s="T957">adj-n:case</ta>
            <ta e="T959" id="Seg_10448" s="T958">n-n:case</ta>
            <ta e="T960" id="Seg_10449" s="T959">adj-n:case</ta>
            <ta e="T961" id="Seg_10450" s="T960">num-n:case</ta>
            <ta e="T962" id="Seg_10451" s="T961">n</ta>
            <ta e="T963" id="Seg_10452" s="T962">conj</ta>
            <ta e="T964" id="Seg_10453" s="T963">num</ta>
            <ta e="T965" id="Seg_10454" s="T964">n</ta>
            <ta e="T966" id="Seg_10455" s="T965">v-v:pn</ta>
            <ta e="T970" id="Seg_10456" s="T969">pers</ta>
            <ta e="T971" id="Seg_10457" s="T970">pers</ta>
            <ta e="T972" id="Seg_10458" s="T971">v-v:ins-v:mood.pn</ta>
            <ta e="T973" id="Seg_10459" s="T972">conj</ta>
            <ta e="T974" id="Seg_10460" s="T973">que-n:case</ta>
            <ta e="T975" id="Seg_10461" s="T974">v-v&gt;v-v:pn</ta>
            <ta e="T976" id="Seg_10462" s="T975">n-n:case</ta>
            <ta e="T977" id="Seg_10463" s="T976">aux-v:mood.pn</ta>
            <ta e="T978" id="Seg_10464" s="T977">v-v:ins-v:n.fin</ta>
            <ta e="T979" id="Seg_10465" s="T978">ptcl</ta>
            <ta e="T980" id="Seg_10466" s="T979">pers</ta>
            <ta e="T981" id="Seg_10467" s="T980">v-v:tense-v:pn</ta>
            <ta e="T982" id="Seg_10468" s="T981">quant</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PKZ">
            <ta e="T15" id="Seg_10469" s="T14">n</ta>
            <ta e="T16" id="Seg_10470" s="T15">adv</ta>
            <ta e="T17" id="Seg_10471" s="T16">v</ta>
            <ta e="T20" id="Seg_10472" s="T19">n</ta>
            <ta e="T21" id="Seg_10473" s="T20">que</ta>
            <ta e="T22" id="Seg_10474" s="T21">n</ta>
            <ta e="T23" id="Seg_10475" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_10476" s="T23">n</ta>
            <ta e="T25" id="Seg_10477" s="T24">v</ta>
            <ta e="T26" id="Seg_10478" s="T25">n</ta>
            <ta e="T27" id="Seg_10479" s="T26">v</ta>
            <ta e="T28" id="Seg_10480" s="T27">n</ta>
            <ta e="T29" id="Seg_10481" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_10482" s="T29">v</ta>
            <ta e="T31" id="Seg_10483" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_10484" s="T31">n</ta>
            <ta e="T33" id="Seg_10485" s="T32">v</ta>
            <ta e="T34" id="Seg_10486" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_10487" s="T34">n</ta>
            <ta e="T36" id="Seg_10488" s="T35">v</ta>
            <ta e="T49" id="Seg_10489" s="T46">n</ta>
            <ta e="T50" id="Seg_10490" s="T49">v</ta>
            <ta e="T51" id="Seg_10491" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_10492" s="T51">n</ta>
            <ta e="T53" id="Seg_10493" s="T52">v</ta>
            <ta e="T54" id="Seg_10494" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_10495" s="T54">n</ta>
            <ta e="T56" id="Seg_10496" s="T55">v</ta>
            <ta e="T57" id="Seg_10497" s="T56">n</ta>
            <ta e="T58" id="Seg_10498" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_10499" s="T58">v</ta>
            <ta e="T60" id="Seg_10500" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_10501" s="T60">n</ta>
            <ta e="T62" id="Seg_10502" s="T61">v</ta>
            <ta e="T63" id="Seg_10503" s="T62">n</ta>
            <ta e="T64" id="Seg_10504" s="T63">v</ta>
            <ta e="T65" id="Seg_10505" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_10506" s="T65">n</ta>
            <ta e="T67" id="Seg_10507" s="T66">v</ta>
            <ta e="T68" id="Seg_10508" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_10509" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_10510" s="T69">n</ta>
            <ta e="T71" id="Seg_10511" s="T70">v</ta>
            <ta e="T72" id="Seg_10512" s="T71">n</ta>
            <ta e="T73" id="Seg_10513" s="T72">v</ta>
            <ta e="T74" id="Seg_10514" s="T73">ptcl</ta>
            <ta e="T91" id="Seg_10515" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_10516" s="T91">n</ta>
            <ta e="T93" id="Seg_10517" s="T92">v</ta>
            <ta e="T94" id="Seg_10518" s="T93">v</ta>
            <ta e="T95" id="Seg_10519" s="T94">v</ta>
            <ta e="T96" id="Seg_10520" s="T95">v</ta>
            <ta e="T97" id="Seg_10521" s="T96">v</ta>
            <ta e="T98" id="Seg_10522" s="T97">ptcl</ta>
            <ta e="T99" id="Seg_10523" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_10524" s="T99">v</ta>
            <ta e="T101" id="Seg_10525" s="T100">v</ta>
            <ta e="T102" id="Seg_10526" s="T101">adv</ta>
            <ta e="T103" id="Seg_10527" s="T102">n</ta>
            <ta e="T104" id="Seg_10528" s="T103">v</ta>
            <ta e="T105" id="Seg_10529" s="T104">n</ta>
            <ta e="T106" id="Seg_10530" s="T105">v</ta>
            <ta e="T107" id="Seg_10531" s="T106">n</ta>
            <ta e="T108" id="Seg_10532" s="T107">v</ta>
            <ta e="T109" id="Seg_10533" s="T108">n</ta>
            <ta e="T110" id="Seg_10534" s="T109">v</ta>
            <ta e="T111" id="Seg_10535" s="T110">n</ta>
            <ta e="T112" id="Seg_10536" s="T111">n</ta>
            <ta e="T113" id="Seg_10537" s="T112">n</ta>
            <ta e="T114" id="Seg_10538" s="T113">v</ta>
            <ta e="T115" id="Seg_10539" s="T114">quant</ta>
            <ta e="T116" id="Seg_10540" s="T115">aux</ta>
            <ta e="T117" id="Seg_10541" s="T116">v</ta>
            <ta e="T118" id="Seg_10542" s="T117">aux</ta>
            <ta e="T119" id="Seg_10543" s="T118">v</ta>
            <ta e="T120" id="Seg_10544" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_10545" s="T120">v</ta>
            <ta e="T122" id="Seg_10546" s="T121">pers</ta>
            <ta e="T123" id="Seg_10547" s="T122">aux</ta>
            <ta e="T124" id="Seg_10548" s="T123">v</ta>
            <ta e="T125" id="Seg_10549" s="T124">aux</ta>
            <ta e="T126" id="Seg_10550" s="T125">v</ta>
            <ta e="T127" id="Seg_10551" s="T126">aux</ta>
            <ta e="T128" id="Seg_10552" s="T127">v</ta>
            <ta e="T129" id="Seg_10553" s="T128">v</ta>
            <ta e="T130" id="Seg_10554" s="T129">quant</ta>
            <ta e="T131" id="Seg_10555" s="T130">v</ta>
            <ta e="T132" id="Seg_10556" s="T131">pers</ta>
            <ta e="T133" id="Seg_10557" s="T132">adv</ta>
            <ta e="T134" id="Seg_10558" s="T133">quant</ta>
            <ta e="T135" id="Seg_10559" s="T134">v</ta>
            <ta e="T138" id="Seg_10560" s="T137">pers</ta>
            <ta e="T139" id="Seg_10561" s="T138">adv</ta>
            <ta e="T140" id="Seg_10562" s="T139">v</ta>
            <ta e="T141" id="Seg_10563" s="T140">n</ta>
            <ta e="T142" id="Seg_10564" s="T141">conj</ta>
            <ta e="T143" id="Seg_10565" s="T142">pers</ta>
            <ta e="T144" id="Seg_10566" s="T143">n</ta>
            <ta e="T145" id="Seg_10567" s="T144">v</ta>
            <ta e="T146" id="Seg_10568" s="T145">aux</ta>
            <ta e="T147" id="Seg_10569" s="T146">v</ta>
            <ta e="T148" id="Seg_10570" s="T147">pers</ta>
            <ta e="T149" id="Seg_10571" s="T148">que</ta>
            <ta e="T150" id="Seg_10572" s="T149">pers</ta>
            <ta e="T151" id="Seg_10573" s="T150">v</ta>
            <ta e="T152" id="Seg_10574" s="T151">adv</ta>
            <ta e="T153" id="Seg_10575" s="T152">v</ta>
            <ta e="T154" id="Seg_10576" s="T153">pers</ta>
            <ta e="T155" id="Seg_10577" s="T154">adv</ta>
            <ta e="T158" id="Seg_10578" s="T157">adv</ta>
            <ta e="T159" id="Seg_10579" s="T158">v</ta>
            <ta e="T160" id="Seg_10580" s="T159">pers</ta>
            <ta e="T161" id="Seg_10581" s="T160">n</ta>
            <ta e="T162" id="Seg_10582" s="T161">pers</ta>
            <ta e="T163" id="Seg_10583" s="T162">v</ta>
            <ta e="T164" id="Seg_10584" s="T163">v</ta>
            <ta e="T165" id="Seg_10585" s="T164">v</ta>
            <ta e="T166" id="Seg_10586" s="T165">adv</ta>
            <ta e="T167" id="Seg_10587" s="T166">v</ta>
            <ta e="T168" id="Seg_10588" s="T167">v</ta>
            <ta e="T169" id="Seg_10589" s="T168">ptcl</ta>
            <ta e="T170" id="Seg_10590" s="T169">v</ta>
            <ta e="T171" id="Seg_10591" s="T170">ptcl</ta>
            <ta e="T172" id="Seg_10592" s="T171">dempro</ta>
            <ta e="T173" id="Seg_10593" s="T172">v</ta>
            <ta e="T174" id="Seg_10594" s="T173">pers</ta>
            <ta e="T175" id="Seg_10595" s="T174">pers</ta>
            <ta e="T176" id="Seg_10596" s="T175">ptcl</ta>
            <ta e="T179" id="Seg_10597" s="T178">v</ta>
            <ta e="T181" id="Seg_10598" s="T180">pers</ta>
            <ta e="T182" id="Seg_10599" s="T181">n</ta>
            <ta e="T183" id="Seg_10600" s="T182">v</ta>
            <ta e="T184" id="Seg_10601" s="T183">adv</ta>
            <ta e="T185" id="Seg_10602" s="T184">adv</ta>
            <ta e="T188" id="Seg_10603" s="T187">v</ta>
            <ta e="T189" id="Seg_10604" s="T188">v</ta>
            <ta e="T190" id="Seg_10605" s="T189">pers</ta>
            <ta e="T191" id="Seg_10606" s="T190">v</ta>
            <ta e="T192" id="Seg_10607" s="T191">adv</ta>
            <ta e="T193" id="Seg_10608" s="T192">v</ta>
            <ta e="T194" id="Seg_10609" s="T193">n</ta>
            <ta e="T195" id="Seg_10610" s="T194">v</ta>
            <ta e="T196" id="Seg_10611" s="T195">v</ta>
            <ta e="T197" id="Seg_10612" s="T196">dempro</ta>
            <ta e="T198" id="Seg_10613" s="T197">v</ta>
            <ta e="T199" id="Seg_10614" s="T198">v</ta>
            <ta e="T200" id="Seg_10615" s="T199">v</ta>
            <ta e="T201" id="Seg_10616" s="T200">adv</ta>
            <ta e="T202" id="Seg_10617" s="T201">adj</ta>
            <ta e="T206" id="Seg_10618" s="T205">n</ta>
            <ta e="T207" id="Seg_10619" s="T206">v</ta>
            <ta e="T208" id="Seg_10620" s="T207">n</ta>
            <ta e="T209" id="Seg_10621" s="T208">v</ta>
            <ta e="T210" id="Seg_10622" s="T209">n</ta>
            <ta e="T211" id="Seg_10623" s="T210">n</ta>
            <ta e="T212" id="Seg_10624" s="T211">v</ta>
            <ta e="T213" id="Seg_10625" s="T212">v</ta>
            <ta e="T214" id="Seg_10626" s="T213">dempro</ta>
            <ta e="T217" id="Seg_10627" s="T216">dempro</ta>
            <ta e="T218" id="Seg_10628" s="T217">v</ta>
            <ta e="T219" id="Seg_10629" s="T218">v</ta>
            <ta e="T220" id="Seg_10630" s="T219">adv</ta>
            <ta e="T221" id="Seg_10631" s="T220">v</ta>
            <ta e="T222" id="Seg_10632" s="T221">v</ta>
            <ta e="T224" id="Seg_10633" s="T223">adv</ta>
            <ta e="T225" id="Seg_10634" s="T224">v</ta>
            <ta e="T226" id="Seg_10635" s="T225">v</ta>
            <ta e="T227" id="Seg_10636" s="T226">conj</ta>
            <ta e="T228" id="Seg_10637" s="T227">pers</ta>
            <ta e="T229" id="Seg_10638" s="T228">v</ta>
            <ta e="T233" id="Seg_10639" s="T232">adv</ta>
            <ta e="T234" id="Seg_10640" s="T233">pers</ta>
            <ta e="T235" id="Seg_10641" s="T234">pers</ta>
            <ta e="T238" id="Seg_10642" s="T237">v</ta>
            <ta e="T239" id="Seg_10643" s="T238">n</ta>
            <ta e="T240" id="Seg_10644" s="T239">num</ta>
            <ta e="T241" id="Seg_10645" s="T240">n</ta>
            <ta e="T242" id="Seg_10646" s="T241">v</ta>
            <ta e="T243" id="Seg_10647" s="T242">adj</ta>
            <ta e="T244" id="Seg_10648" s="T243">n</ta>
            <ta e="T245" id="Seg_10649" s="T244">pers</ta>
            <ta e="T246" id="Seg_10650" s="T245">refl</ta>
            <ta e="T249" id="Seg_10651" s="T248">v</ta>
            <ta e="T250" id="Seg_10652" s="T249">v</ta>
            <ta e="T257" id="Seg_10653" s="T256">pers</ta>
            <ta e="T258" id="Seg_10654" s="T257">adv</ta>
            <ta e="T259" id="Seg_10655" s="T258">n</ta>
            <ta e="T260" id="Seg_10656" s="T259">v</ta>
            <ta e="T261" id="Seg_10657" s="T260">v</ta>
            <ta e="T262" id="Seg_10658" s="T261">adj</ta>
            <ta e="T263" id="Seg_10659" s="T262">n</ta>
            <ta e="T264" id="Seg_10660" s="T263">v</ta>
            <ta e="T265" id="Seg_10661" s="T264">adj</ta>
            <ta e="T266" id="Seg_10662" s="T265">n</ta>
            <ta e="T267" id="Seg_10663" s="T266">n</ta>
            <ta e="T268" id="Seg_10664" s="T267">v</ta>
            <ta e="T269" id="Seg_10665" s="T268">n</ta>
            <ta e="T270" id="Seg_10666" s="T269">v</ta>
            <ta e="T271" id="Seg_10667" s="T270">n</ta>
            <ta e="T272" id="Seg_10668" s="T271">v</ta>
            <ta e="T273" id="Seg_10669" s="T272">n</ta>
            <ta e="T274" id="Seg_10670" s="T273">v</ta>
            <ta e="T275" id="Seg_10671" s="T274">pers</ta>
            <ta e="T276" id="Seg_10672" s="T275">v</ta>
            <ta e="T277" id="Seg_10673" s="T276">adv</ta>
            <ta e="T278" id="Seg_10674" s="T277">v</ta>
            <ta e="T279" id="Seg_10675" s="T278">v</ta>
            <ta e="T280" id="Seg_10676" s="T279">conj</ta>
            <ta e="T281" id="Seg_10677" s="T280">conj</ta>
            <ta e="T282" id="Seg_10678" s="T281">v</ta>
            <ta e="T283" id="Seg_10679" s="T282">adv</ta>
            <ta e="T285" id="Seg_10680" s="T284">pers</ta>
            <ta e="T286" id="Seg_10681" s="T285">v</ta>
            <ta e="T287" id="Seg_10682" s="T286">v</ta>
            <ta e="T288" id="Seg_10683" s="T287">n</ta>
            <ta e="T289" id="Seg_10684" s="T288">v</ta>
            <ta e="T290" id="Seg_10685" s="T289">adv</ta>
            <ta e="T291" id="Seg_10686" s="T290">v</ta>
            <ta e="T292" id="Seg_10687" s="T291">v</ta>
            <ta e="T293" id="Seg_10688" s="T292">ptcl</ta>
            <ta e="T294" id="Seg_10689" s="T293">adv</ta>
            <ta e="T295" id="Seg_10690" s="T294">v</ta>
            <ta e="T296" id="Seg_10691" s="T295">quant</ta>
            <ta e="T297" id="Seg_10692" s="T296">adv</ta>
            <ta e="T298" id="Seg_10693" s="T297">v</ta>
            <ta e="T299" id="Seg_10694" s="T298">conj</ta>
            <ta e="T300" id="Seg_10695" s="T299">ptcl</ta>
            <ta e="T301" id="Seg_10696" s="T300">n</ta>
            <ta e="T302" id="Seg_10697" s="T301">quant</ta>
            <ta e="T303" id="Seg_10698" s="T302">v</ta>
            <ta e="T304" id="Seg_10699" s="T303">quant</ta>
            <ta e="T305" id="Seg_10700" s="T304">adv</ta>
            <ta e="T306" id="Seg_10701" s="T305">adj</ta>
            <ta e="T307" id="Seg_10702" s="T306">n</ta>
            <ta e="T308" id="Seg_10703" s="T307">v</ta>
            <ta e="T309" id="Seg_10704" s="T308">adv</ta>
            <ta e="T310" id="Seg_10705" s="T309">quant</ta>
            <ta e="T311" id="Seg_10706" s="T310">v</ta>
            <ta e="T312" id="Seg_10707" s="T311">v</ta>
            <ta e="T313" id="Seg_10708" s="T312">quant</ta>
            <ta e="T314" id="Seg_10709" s="T313">adj</ta>
            <ta e="T316" id="Seg_10710" s="T315">v</ta>
            <ta e="T317" id="Seg_10711" s="T316">pers</ta>
            <ta e="T318" id="Seg_10712" s="T317">dempro</ta>
            <ta e="T319" id="Seg_10713" s="T318">v</ta>
            <ta e="T320" id="Seg_10714" s="T319">v</ta>
            <ta e="T321" id="Seg_10715" s="T320">v</ta>
            <ta e="T322" id="Seg_10716" s="T321">pers</ta>
            <ta e="T323" id="Seg_10717" s="T322">v</ta>
            <ta e="T324" id="Seg_10718" s="T323">aux</ta>
            <ta e="T325" id="Seg_10719" s="T324">v</ta>
            <ta e="T326" id="Seg_10720" s="T325">v</ta>
            <ta e="T327" id="Seg_10721" s="T326">v</ta>
            <ta e="T328" id="Seg_10722" s="T327">adj</ta>
            <ta e="T329" id="Seg_10723" s="T328">n</ta>
            <ta e="T330" id="Seg_10724" s="T329">v</ta>
            <ta e="T331" id="Seg_10725" s="T330">n</ta>
            <ta e="T332" id="Seg_10726" s="T331">v</ta>
            <ta e="T333" id="Seg_10727" s="T332">v</ta>
            <ta e="T334" id="Seg_10728" s="T333">adj</ta>
            <ta e="T335" id="Seg_10729" s="T334">n</ta>
            <ta e="T336" id="Seg_10730" s="T335">n</ta>
            <ta e="T337" id="Seg_10731" s="T336">v</ta>
            <ta e="T340" id="Seg_10732" s="T339">n</ta>
            <ta e="T341" id="Seg_10733" s="T340">v</ta>
            <ta e="T342" id="Seg_10734" s="T341">n</ta>
            <ta e="T343" id="Seg_10735" s="T342">v</ta>
            <ta e="T344" id="Seg_10736" s="T343">n</ta>
            <ta e="T345" id="Seg_10737" s="T344">v</ta>
            <ta e="T346" id="Seg_10738" s="T345">n</ta>
            <ta e="T347" id="Seg_10739" s="T346">v</ta>
            <ta e="T348" id="Seg_10740" s="T347">v</ta>
            <ta e="T349" id="Seg_10741" s="T348">n</ta>
            <ta e="T350" id="Seg_10742" s="T349">v</ta>
            <ta e="T351" id="Seg_10743" s="T350">n</ta>
            <ta e="T352" id="Seg_10744" s="T351">v</ta>
            <ta e="T353" id="Seg_10745" s="T352">adj</ta>
            <ta e="T354" id="Seg_10746" s="T353">n</ta>
            <ta e="T355" id="Seg_10747" s="T354">v</ta>
            <ta e="T356" id="Seg_10748" s="T355">n</ta>
            <ta e="T357" id="Seg_10749" s="T356">v</ta>
            <ta e="T359" id="Seg_10750" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_10751" s="T359">pers</ta>
            <ta e="T361" id="Seg_10752" s="T360">n</ta>
            <ta e="T362" id="Seg_10753" s="T361">v</ta>
            <ta e="T363" id="Seg_10754" s="T362">n</ta>
            <ta e="T364" id="Seg_10755" s="T363">v</ta>
            <ta e="T365" id="Seg_10756" s="T364">ptcl</ta>
            <ta e="T368" id="Seg_10757" s="T367">n</ta>
            <ta e="T369" id="Seg_10758" s="T368">v</ta>
            <ta e="T370" id="Seg_10759" s="T369">v</ta>
            <ta e="T373" id="Seg_10760" s="T372">ptcl</ta>
            <ta e="T374" id="Seg_10761" s="T373">n</ta>
            <ta e="T375" id="Seg_10762" s="T374">v</ta>
            <ta e="T376" id="Seg_10763" s="T375">ptcl</ta>
            <ta e="T377" id="Seg_10764" s="T376">pers</ta>
            <ta e="T378" id="Seg_10765" s="T377">n</ta>
            <ta e="T379" id="Seg_10766" s="T378">quant</ta>
            <ta e="T380" id="Seg_10767" s="T379">v</ta>
            <ta e="T381" id="Seg_10768" s="T380">v</ta>
            <ta e="T382" id="Seg_10769" s="T381">que</ta>
            <ta e="T383" id="Seg_10770" s="T382">que</ta>
            <ta e="T384" id="Seg_10771" s="T383">v</ta>
            <ta e="T385" id="Seg_10772" s="T384">pers</ta>
            <ta e="T386" id="Seg_10773" s="T385">v</ta>
            <ta e="T387" id="Seg_10774" s="T386">propr</ta>
            <ta e="T388" id="Seg_10775" s="T387">n</ta>
            <ta e="T389" id="Seg_10776" s="T388">conj</ta>
            <ta e="T390" id="Seg_10777" s="T389">ptcl</ta>
            <ta e="T391" id="Seg_10778" s="T390">n</ta>
            <ta e="T392" id="Seg_10779" s="T391">v</ta>
            <ta e="T393" id="Seg_10780" s="T392">ptcl</ta>
            <ta e="T394" id="Seg_10781" s="T393">n</ta>
            <ta e="T395" id="Seg_10782" s="T394">v</ta>
            <ta e="T396" id="Seg_10783" s="T395">ptcl</ta>
            <ta e="T399" id="Seg_10784" s="T398">n</ta>
            <ta e="T400" id="Seg_10785" s="T399">n</ta>
            <ta e="T401" id="Seg_10786" s="T400">v</ta>
            <ta e="T402" id="Seg_10787" s="T401">ptcl</ta>
            <ta e="T409" id="Seg_10788" s="T408">ptcl</ta>
            <ta e="T410" id="Seg_10789" s="T409">n</ta>
            <ta e="T416" id="Seg_10790" s="T415">pers</ta>
            <ta e="T417" id="Seg_10791" s="T416">n</ta>
            <ta e="T418" id="Seg_10792" s="T417">adv</ta>
            <ta e="T419" id="Seg_10793" s="T418">v</ta>
            <ta e="T422" id="Seg_10794" s="T421">adv</ta>
            <ta e="T425" id="Seg_10795" s="T424">dempro</ta>
            <ta e="T426" id="Seg_10796" s="T425">n</ta>
            <ta e="T427" id="Seg_10797" s="T426">v</ta>
            <ta e="T428" id="Seg_10798" s="T427">ptcl</ta>
            <ta e="T429" id="Seg_10799" s="T428">n</ta>
            <ta e="T430" id="Seg_10800" s="T429">v</ta>
            <ta e="T431" id="Seg_10801" s="T430">n</ta>
            <ta e="T432" id="Seg_10802" s="T431">v</ta>
            <ta e="T434" id="Seg_10803" s="T433">v</ta>
            <ta e="T435" id="Seg_10804" s="T434">ptcl</ta>
            <ta e="T436" id="Seg_10805" s="T435">conj</ta>
            <ta e="T437" id="Seg_10806" s="T436">v</ta>
            <ta e="T438" id="Seg_10807" s="T437">n</ta>
            <ta e="T439" id="Seg_10808" s="T438">v</ta>
            <ta e="T440" id="Seg_10809" s="T439">n</ta>
            <ta e="T441" id="Seg_10810" s="T440">v</ta>
            <ta e="T442" id="Seg_10811" s="T441">conj</ta>
            <ta e="T443" id="Seg_10812" s="T442">n</ta>
            <ta e="T444" id="Seg_10813" s="T443">v</ta>
            <ta e="T445" id="Seg_10814" s="T444">conj</ta>
            <ta e="T446" id="Seg_10815" s="T445">v</ta>
            <ta e="T447" id="Seg_10816" s="T446">n</ta>
            <ta e="T448" id="Seg_10817" s="T447">ptcl</ta>
            <ta e="T449" id="Seg_10818" s="T448">v</ta>
            <ta e="T454" id="Seg_10819" s="T453">v</ta>
            <ta e="T459" id="Seg_10820" s="T458">v</ta>
            <ta e="T460" id="Seg_10821" s="T459">n</ta>
            <ta e="T461" id="Seg_10822" s="T460">v</ta>
            <ta e="T462" id="Seg_10823" s="T461">adv</ta>
            <ta e="T463" id="Seg_10824" s="T462">v</ta>
            <ta e="T464" id="Seg_10825" s="T463">v</ta>
            <ta e="T465" id="Seg_10826" s="T464">ptcl</ta>
            <ta e="T466" id="Seg_10827" s="T465">adv</ta>
            <ta e="T467" id="Seg_10828" s="T466">v</ta>
            <ta e="T468" id="Seg_10829" s="T467">ptcl</ta>
            <ta e="T469" id="Seg_10830" s="T468">adv</ta>
            <ta e="T476" id="Seg_10831" s="T475">adv</ta>
            <ta e="T477" id="Seg_10832" s="T476">v</ta>
            <ta e="T478" id="Seg_10833" s="T477">ptcl</ta>
            <ta e="T481" id="Seg_10834" s="T480">adv</ta>
            <ta e="T482" id="Seg_10835" s="T481">ptcl</ta>
            <ta e="T483" id="Seg_10836" s="T482">v</ta>
            <ta e="T484" id="Seg_10837" s="T483">adv</ta>
            <ta e="T485" id="Seg_10838" s="T484">v</ta>
            <ta e="T486" id="Seg_10839" s="T485">ptcl</ta>
            <ta e="T489" id="Seg_10840" s="T488">adv</ta>
            <ta e="T490" id="Seg_10841" s="T489">adj</ta>
            <ta e="T491" id="Seg_10842" s="T490">n</ta>
            <ta e="T494" id="Seg_10843" s="T493">n</ta>
            <ta e="T495" id="Seg_10844" s="T494">adv</ta>
            <ta e="T496" id="Seg_10845" s="T495">quant</ta>
            <ta e="T497" id="Seg_10846" s="T496">adj</ta>
            <ta e="T498" id="Seg_10847" s="T497">quant</ta>
            <ta e="T499" id="Seg_10848" s="T498">n</ta>
            <ta e="T500" id="Seg_10849" s="T499">v</ta>
            <ta e="T501" id="Seg_10850" s="T500">v</ta>
            <ta e="T503" id="Seg_10851" s="T502">v</ta>
            <ta e="T504" id="Seg_10852" s="T503">n</ta>
            <ta e="T505" id="Seg_10853" s="T504">v</ta>
            <ta e="T506" id="Seg_10854" s="T505">adj</ta>
            <ta e="T507" id="Seg_10855" s="T506">n</ta>
            <ta e="T508" id="Seg_10856" s="T507">n</ta>
            <ta e="T509" id="Seg_10857" s="T508">quant</ta>
            <ta e="T512" id="Seg_10858" s="T511">v</ta>
            <ta e="T513" id="Seg_10859" s="T512">adj</ta>
            <ta e="T514" id="Seg_10860" s="T513">adj</ta>
            <ta e="T515" id="Seg_10861" s="T514">adv</ta>
            <ta e="T520" id="Seg_10862" s="T519">v</ta>
            <ta e="T521" id="Seg_10863" s="T520">ptcl</ta>
            <ta e="T522" id="Seg_10864" s="T521">v</ta>
            <ta e="T523" id="Seg_10865" s="T522">adv</ta>
            <ta e="T524" id="Seg_10866" s="T523">v</ta>
            <ta e="T525" id="Seg_10867" s="T524">v</ta>
            <ta e="T526" id="Seg_10868" s="T525">v</ta>
            <ta e="T527" id="Seg_10869" s="T526">v</ta>
            <ta e="T528" id="Seg_10870" s="T527">adj</ta>
            <ta e="T529" id="Seg_10871" s="T528">n</ta>
            <ta e="T530" id="Seg_10872" s="T529">v</ta>
            <ta e="T531" id="Seg_10873" s="T530">n</ta>
            <ta e="T532" id="Seg_10874" s="T531">n</ta>
            <ta e="T533" id="Seg_10875" s="T532">n</ta>
            <ta e="T534" id="Seg_10876" s="T533">n</ta>
            <ta e="T535" id="Seg_10877" s="T534">v</ta>
            <ta e="T536" id="Seg_10878" s="T535">v</ta>
            <ta e="T538" id="Seg_10879" s="T537">n</ta>
            <ta e="T541" id="Seg_10880" s="T540">v</ta>
            <ta e="T544" id="Seg_10881" s="T543">pers</ta>
            <ta e="T545" id="Seg_10882" s="T544">adv</ta>
            <ta e="T546" id="Seg_10883" s="T545">v</ta>
            <ta e="T547" id="Seg_10884" s="T546">v</ta>
            <ta e="T548" id="Seg_10885" s="T547">ptcl</ta>
            <ta e="T549" id="Seg_10886" s="T548">adj</ta>
            <ta e="T550" id="Seg_10887" s="T549">n</ta>
            <ta e="T551" id="Seg_10888" s="T550">v</ta>
            <ta e="T552" id="Seg_10889" s="T551">pers</ta>
            <ta e="T553" id="Seg_10890" s="T552">v</ta>
            <ta e="T554" id="Seg_10891" s="T553">quant</ta>
            <ta e="T555" id="Seg_10892" s="T554">adv</ta>
            <ta e="T556" id="Seg_10893" s="T555">quant</ta>
            <ta e="T557" id="Seg_10894" s="T556">v</ta>
            <ta e="T567" id="Seg_10895" s="T566">ptcl</ta>
            <ta e="T568" id="Seg_10896" s="T567">v</ta>
            <ta e="T569" id="Seg_10897" s="T568">aux</ta>
            <ta e="T570" id="Seg_10898" s="T569">v</ta>
            <ta e="T574" id="Seg_10899" s="T573">aux</ta>
            <ta e="T575" id="Seg_10900" s="T574">v</ta>
            <ta e="T578" id="Seg_10901" s="T577">aux</ta>
            <ta e="T579" id="Seg_10902" s="T578">v</ta>
            <ta e="T583" id="Seg_10903" s="T582">aux</ta>
            <ta e="T584" id="Seg_10904" s="T583">v</ta>
            <ta e="T587" id="Seg_10905" s="T586">n</ta>
            <ta e="T588" id="Seg_10906" s="T587">v</ta>
            <ta e="T589" id="Seg_10907" s="T588">v</ta>
            <ta e="T590" id="Seg_10908" s="T589">n</ta>
            <ta e="T593" id="Seg_10909" s="T592">aux</ta>
            <ta e="T594" id="Seg_10910" s="T593">v</ta>
            <ta e="T595" id="Seg_10911" s="T594">aux</ta>
            <ta e="T596" id="Seg_10912" s="T595">v</ta>
            <ta e="T597" id="Seg_10913" s="T596">aux</ta>
            <ta e="T598" id="Seg_10914" s="T597">v</ta>
            <ta e="T599" id="Seg_10915" s="T598">adv</ta>
            <ta e="T600" id="Seg_10916" s="T599">adj</ta>
            <ta e="T601" id="Seg_10917" s="T600">adv</ta>
            <ta e="T602" id="Seg_10918" s="T601">adj</ta>
            <ta e="T603" id="Seg_10919" s="T602">adv</ta>
            <ta e="T604" id="Seg_10920" s="T603">adj</ta>
            <ta e="T605" id="Seg_10921" s="T604">adv</ta>
            <ta e="T607" id="Seg_10922" s="T606">adj</ta>
            <ta e="T608" id="Seg_10923" s="T607">adv</ta>
            <ta e="T609" id="Seg_10924" s="T608">adj</ta>
            <ta e="T613" id="Seg_10925" s="T612">adv</ta>
            <ta e="T614" id="Seg_10926" s="T613">adj</ta>
            <ta e="T619" id="Seg_10927" s="T618">ptcl</ta>
            <ta e="T620" id="Seg_10928" s="T619">n</ta>
            <ta e="T621" id="Seg_10929" s="T620">v</ta>
            <ta e="T622" id="Seg_10930" s="T621">adv</ta>
            <ta e="T623" id="Seg_10931" s="T622">v</ta>
            <ta e="T624" id="Seg_10932" s="T623">pers</ta>
            <ta e="T625" id="Seg_10933" s="T624">ptcl</ta>
            <ta e="T626" id="Seg_10934" s="T625">v</ta>
            <ta e="T627" id="Seg_10935" s="T626">ptcl</ta>
            <ta e="T628" id="Seg_10936" s="T627">ptcl</ta>
            <ta e="T629" id="Seg_10937" s="T628">v</ta>
            <ta e="T630" id="Seg_10938" s="T629">ptcl</ta>
            <ta e="T631" id="Seg_10939" s="T630">ptcl</ta>
            <ta e="T632" id="Seg_10940" s="T631">adj</ta>
            <ta e="T633" id="Seg_10941" s="T632">v</ta>
            <ta e="T634" id="Seg_10942" s="T633">n</ta>
            <ta e="T635" id="Seg_10943" s="T634">v</ta>
            <ta e="T636" id="Seg_10944" s="T635">quant</ta>
            <ta e="T637" id="Seg_10945" s="T636">n</ta>
            <ta e="T638" id="Seg_10946" s="T637">v</ta>
            <ta e="T639" id="Seg_10947" s="T638">v</ta>
            <ta e="T640" id="Seg_10948" s="T639">que</ta>
            <ta e="T641" id="Seg_10949" s="T640">n</ta>
            <ta e="T642" id="Seg_10950" s="T641">v</ta>
            <ta e="T647" id="Seg_10951" s="T645">adv</ta>
            <ta e="T648" id="Seg_10952" s="T647">n</ta>
            <ta e="T649" id="Seg_10953" s="T648">n</ta>
            <ta e="T650" id="Seg_10954" s="T649">v</ta>
            <ta e="T651" id="Seg_10955" s="T650">pers</ta>
            <ta e="T652" id="Seg_10956" s="T651">adv</ta>
            <ta e="T654" id="Seg_10957" s="T652">ptcl</ta>
            <ta e="T655" id="Seg_10958" s="T654">n</ta>
            <ta e="T656" id="Seg_10959" s="T655">n</ta>
            <ta e="T657" id="Seg_10960" s="T656">quant</ta>
            <ta e="T658" id="Seg_10961" s="T657">v</ta>
            <ta e="T659" id="Seg_10962" s="T658">n</ta>
            <ta e="T660" id="Seg_10963" s="T659">quant</ta>
            <ta e="T661" id="Seg_10964" s="T660">v</ta>
            <ta e="T662" id="Seg_10965" s="T661">n</ta>
            <ta e="T663" id="Seg_10966" s="T662">adv</ta>
            <ta e="T664" id="Seg_10967" s="T663">adj</ta>
            <ta e="T665" id="Seg_10968" s="T664">pers</ta>
            <ta e="T666" id="Seg_10969" s="T665">adv</ta>
            <ta e="T667" id="Seg_10970" s="T666">v</ta>
            <ta e="T668" id="Seg_10971" s="T667">conj</ta>
            <ta e="T669" id="Seg_10972" s="T668">adv</ta>
            <ta e="T670" id="Seg_10973" s="T669">v</ta>
            <ta e="T671" id="Seg_10974" s="T670">n</ta>
            <ta e="T672" id="Seg_10975" s="T671">v</ta>
            <ta e="T673" id="Seg_10976" s="T672">n</ta>
            <ta e="T674" id="Seg_10977" s="T673">v</ta>
            <ta e="T675" id="Seg_10978" s="T674">n</ta>
            <ta e="T676" id="Seg_10979" s="T675">quant</ta>
            <ta e="T677" id="Seg_10980" s="T676">v</ta>
            <ta e="T678" id="Seg_10981" s="T677">adv</ta>
            <ta e="T681" id="Seg_10982" s="T680">adj</ta>
            <ta e="T682" id="Seg_10983" s="T681">n</ta>
            <ta e="T683" id="Seg_10984" s="T682">n</ta>
            <ta e="T684" id="Seg_10985" s="T683">quant</ta>
            <ta e="T685" id="Seg_10986" s="T684">v</ta>
            <ta e="T686" id="Seg_10987" s="T685">adv</ta>
            <ta e="T687" id="Seg_10988" s="T686">que</ta>
            <ta e="T688" id="Seg_10989" s="T687">v</ta>
            <ta e="T689" id="Seg_10990" s="T688">adv</ta>
            <ta e="T690" id="Seg_10991" s="T689">adj</ta>
            <ta e="T691" id="Seg_10992" s="T690">n</ta>
            <ta e="T692" id="Seg_10993" s="T691">n</ta>
            <ta e="T693" id="Seg_10994" s="T692">quant</ta>
            <ta e="T694" id="Seg_10995" s="T693">v</ta>
            <ta e="T695" id="Seg_10996" s="T694">n</ta>
            <ta e="T696" id="Seg_10997" s="T695">v</ta>
            <ta e="T697" id="Seg_10998" s="T696">n</ta>
            <ta e="T698" id="Seg_10999" s="T697">v</ta>
            <ta e="T699" id="Seg_11000" s="T698">quant</ta>
            <ta e="T700" id="Seg_11001" s="T699">ptcl</ta>
            <ta e="T701" id="Seg_11002" s="T700">v</ta>
            <ta e="T702" id="Seg_11003" s="T701">n</ta>
            <ta e="T703" id="Seg_11004" s="T702">v</ta>
            <ta e="T704" id="Seg_11005" s="T703">n</ta>
            <ta e="T705" id="Seg_11006" s="T704">ptcl</ta>
            <ta e="T706" id="Seg_11007" s="T705">v</ta>
            <ta e="T707" id="Seg_11008" s="T706">ptcl</ta>
            <ta e="T708" id="Seg_11009" s="T707">n</ta>
            <ta e="T709" id="Seg_11010" s="T708">v</ta>
            <ta e="T710" id="Seg_11011" s="T709">n</ta>
            <ta e="T711" id="Seg_11012" s="T710">v</ta>
            <ta e="T712" id="Seg_11013" s="T711">ptcl</ta>
            <ta e="T713" id="Seg_11014" s="T712">n</ta>
            <ta e="T714" id="Seg_11015" s="T713">v</ta>
            <ta e="T715" id="Seg_11016" s="T714">n</ta>
            <ta e="T716" id="Seg_11017" s="T715">v</ta>
            <ta e="T717" id="Seg_11018" s="T716">n</ta>
            <ta e="T718" id="Seg_11019" s="T717">v</ta>
            <ta e="T719" id="Seg_11020" s="T718">n</ta>
            <ta e="T720" id="Seg_11021" s="T719">ptcl</ta>
            <ta e="T721" id="Seg_11022" s="T720">v</ta>
            <ta e="T722" id="Seg_11023" s="T721">n</ta>
            <ta e="T723" id="Seg_11024" s="T722">n</ta>
            <ta e="T724" id="Seg_11025" s="T723">adv</ta>
            <ta e="T725" id="Seg_11026" s="T724">v</ta>
            <ta e="T726" id="Seg_11027" s="T725">n</ta>
            <ta e="T727" id="Seg_11028" s="T726">v</ta>
            <ta e="T728" id="Seg_11029" s="T727">n</ta>
            <ta e="T729" id="Seg_11030" s="T728">v</ta>
            <ta e="T730" id="Seg_11031" s="T729">n</ta>
            <ta e="T731" id="Seg_11032" s="T730">v</ta>
            <ta e="T732" id="Seg_11033" s="T731">n</ta>
            <ta e="T733" id="Seg_11034" s="T732">v</ta>
            <ta e="T734" id="Seg_11035" s="T733">n</ta>
            <ta e="T735" id="Seg_11036" s="T734">v</ta>
            <ta e="T736" id="Seg_11037" s="T735">n</ta>
            <ta e="T737" id="Seg_11038" s="T736">n</ta>
            <ta e="T738" id="Seg_11039" s="T737">quant</ta>
            <ta e="T739" id="Seg_11040" s="T738">n</ta>
            <ta e="T740" id="Seg_11041" s="T739">v</ta>
            <ta e="T741" id="Seg_11042" s="T740">v</ta>
            <ta e="T742" id="Seg_11043" s="T741">adv</ta>
            <ta e="T743" id="Seg_11044" s="T742">dempro</ta>
            <ta e="T744" id="Seg_11045" s="T743">n</ta>
            <ta e="T745" id="Seg_11046" s="T744">v</ta>
            <ta e="T747" id="Seg_11047" s="T746">n</ta>
            <ta e="T749" id="Seg_11048" s="T748">ptcl</ta>
            <ta e="T750" id="Seg_11049" s="T749">n</ta>
            <ta e="T751" id="Seg_11050" s="T750">v</ta>
            <ta e="T752" id="Seg_11051" s="T751">ptcl</ta>
            <ta e="T755" id="Seg_11052" s="T754">n</ta>
            <ta e="T756" id="Seg_11053" s="T755">quant</ta>
            <ta e="T757" id="Seg_11054" s="T756">v</ta>
            <ta e="T758" id="Seg_11055" s="T757">ptcl</ta>
            <ta e="T759" id="Seg_11056" s="T758">dempro</ta>
            <ta e="T760" id="Seg_11057" s="T759">v</ta>
            <ta e="T761" id="Seg_11058" s="T760">ptcl</ta>
            <ta e="T762" id="Seg_11059" s="T761">v</ta>
            <ta e="T763" id="Seg_11060" s="T762">v</ta>
            <ta e="T764" id="Seg_11061" s="T763">ptcl</ta>
            <ta e="T765" id="Seg_11062" s="T764">v</ta>
            <ta e="T766" id="Seg_11063" s="T765">v</ta>
            <ta e="T771" id="Seg_11064" s="T770">v</ta>
            <ta e="T772" id="Seg_11065" s="T771">n</ta>
            <ta e="T773" id="Seg_11066" s="T772">ptcl</ta>
            <ta e="T774" id="Seg_11067" s="T773">ptcl</ta>
            <ta e="T775" id="Seg_11068" s="T774">v</ta>
            <ta e="T776" id="Seg_11069" s="T775">v</ta>
            <ta e="T777" id="Seg_11070" s="T776">adv</ta>
            <ta e="T778" id="Seg_11071" s="T777">v</ta>
            <ta e="T779" id="Seg_11072" s="T778">v</ta>
            <ta e="T780" id="Seg_11073" s="T779">n</ta>
            <ta e="T781" id="Seg_11074" s="T780">v</ta>
            <ta e="T782" id="Seg_11075" s="T781">adv</ta>
            <ta e="T783" id="Seg_11076" s="T782">v</ta>
            <ta e="T784" id="Seg_11077" s="T783">aux</ta>
            <ta e="T785" id="Seg_11078" s="T784">v</ta>
            <ta e="T786" id="Seg_11079" s="T785">conj</ta>
            <ta e="T787" id="Seg_11080" s="T786">adv</ta>
            <ta e="T788" id="Seg_11081" s="T787">v</ta>
            <ta e="T789" id="Seg_11082" s="T788">n</ta>
            <ta e="T790" id="Seg_11083" s="T789">adv</ta>
            <ta e="T791" id="Seg_11084" s="T790">n</ta>
            <ta e="T792" id="Seg_11085" s="T791">v</ta>
            <ta e="T793" id="Seg_11086" s="T792">ptcl</ta>
            <ta e="T794" id="Seg_11087" s="T793">adv</ta>
            <ta e="T795" id="Seg_11088" s="T794">n</ta>
            <ta e="T796" id="Seg_11089" s="T795">v</ta>
            <ta e="T797" id="Seg_11090" s="T796">n</ta>
            <ta e="T798" id="Seg_11091" s="T797">v</ta>
            <ta e="T799" id="Seg_11092" s="T798">conj</ta>
            <ta e="T800" id="Seg_11093" s="T799">n</ta>
            <ta e="T801" id="Seg_11094" s="T800">v</ta>
            <ta e="T802" id="Seg_11095" s="T801">adv</ta>
            <ta e="T803" id="Seg_11096" s="T802">v</ta>
            <ta e="T804" id="Seg_11097" s="T803">aux</ta>
            <ta e="T805" id="Seg_11098" s="T804">v</ta>
            <ta e="T806" id="Seg_11099" s="T805">ptcl</ta>
            <ta e="T807" id="Seg_11100" s="T806">adj</ta>
            <ta e="T810" id="Seg_11101" s="T809">n</ta>
            <ta e="T811" id="Seg_11102" s="T810">v</ta>
            <ta e="T814" id="Seg_11103" s="T812">v</ta>
            <ta e="T815" id="Seg_11104" s="T814">n</ta>
            <ta e="T816" id="Seg_11105" s="T815">n</ta>
            <ta e="T817" id="Seg_11106" s="T816">refl</ta>
            <ta e="T818" id="Seg_11107" s="T817">quant</ta>
            <ta e="T819" id="Seg_11108" s="T818">adj</ta>
            <ta e="T820" id="Seg_11109" s="T819">conj</ta>
            <ta e="T821" id="Seg_11110" s="T820">dempro</ta>
            <ta e="T822" id="Seg_11111" s="T821">n</ta>
            <ta e="T823" id="Seg_11112" s="T822">v</ta>
            <ta e="T824" id="Seg_11113" s="T823">adj</ta>
            <ta e="T825" id="Seg_11114" s="T824">adv</ta>
            <ta e="T826" id="Seg_11115" s="T825">conj</ta>
            <ta e="T827" id="Seg_11116" s="T826">pers</ta>
            <ta e="T828" id="Seg_11117" s="T827">n</ta>
            <ta e="T829" id="Seg_11118" s="T828">v</ta>
            <ta e="T831" id="Seg_11119" s="T830">dempro</ta>
            <ta e="T832" id="Seg_11120" s="T831">n</ta>
            <ta e="T833" id="Seg_11121" s="T832">ptcl</ta>
            <ta e="T834" id="Seg_11122" s="T833">v</ta>
            <ta e="T835" id="Seg_11123" s="T834">pers</ta>
            <ta e="T836" id="Seg_11124" s="T835">n</ta>
            <ta e="T837" id="Seg_11125" s="T836">dempro</ta>
            <ta e="T838" id="Seg_11126" s="T837">v</ta>
            <ta e="T839" id="Seg_11127" s="T838">dempro</ta>
            <ta e="T840" id="Seg_11128" s="T839">n</ta>
            <ta e="T841" id="Seg_11129" s="T840">quant</ta>
            <ta e="T842" id="Seg_11130" s="T841">v</ta>
            <ta e="T843" id="Seg_11131" s="T842">quant</ta>
            <ta e="T844" id="Seg_11132" s="T843">n</ta>
            <ta e="T845" id="Seg_11133" s="T844">v</ta>
            <ta e="T846" id="Seg_11134" s="T845">n</ta>
            <ta e="T847" id="Seg_11135" s="T846">adv</ta>
            <ta e="T848" id="Seg_11136" s="T847">v</ta>
            <ta e="T849" id="Seg_11137" s="T848">conj</ta>
            <ta e="T850" id="Seg_11138" s="T849">adv</ta>
            <ta e="T851" id="Seg_11139" s="T850">que</ta>
            <ta e="T854" id="Seg_11140" s="T853">v</ta>
            <ta e="T855" id="Seg_11141" s="T854">que</ta>
            <ta e="T856" id="Seg_11142" s="T855">v</ta>
            <ta e="T857" id="Seg_11143" s="T856">ptcl</ta>
            <ta e="T858" id="Seg_11144" s="T857">v</ta>
            <ta e="T859" id="Seg_11145" s="T858">v</ta>
            <ta e="T860" id="Seg_11146" s="T859">que</ta>
            <ta e="T861" id="Seg_11147" s="T860">n</ta>
            <ta e="T862" id="Seg_11148" s="T861">adv</ta>
            <ta e="T865" id="Seg_11149" s="T864">n</ta>
            <ta e="T866" id="Seg_11150" s="T865">adv</ta>
            <ta e="T867" id="Seg_11151" s="T866">v</ta>
            <ta e="T868" id="Seg_11152" s="T867">que</ta>
            <ta e="T869" id="Seg_11153" s="T868">ptcl</ta>
            <ta e="T870" id="Seg_11154" s="T869">v</ta>
            <ta e="T871" id="Seg_11155" s="T870">v</ta>
            <ta e="T875" id="Seg_11156" s="T874">que</ta>
            <ta e="T876" id="Seg_11157" s="T875">v</ta>
            <ta e="T877" id="Seg_11158" s="T876">que</ta>
            <ta e="T878" id="Seg_11159" s="T877">v</ta>
            <ta e="T879" id="Seg_11160" s="T878">v</ta>
            <ta e="T880" id="Seg_11161" s="T879">adv</ta>
            <ta e="T881" id="Seg_11162" s="T880">que</ta>
            <ta e="T882" id="Seg_11163" s="T881">v</ta>
            <ta e="T883" id="Seg_11164" s="T882">pers</ta>
            <ta e="T886" id="Seg_11165" s="T885">n</ta>
            <ta e="T887" id="Seg_11166" s="T886">n</ta>
            <ta e="T888" id="Seg_11167" s="T887">v</ta>
            <ta e="T889" id="Seg_11168" s="T888">dempro</ta>
            <ta e="T890" id="Seg_11169" s="T889">n</ta>
            <ta e="T891" id="Seg_11170" s="T890">v</ta>
            <ta e="T892" id="Seg_11171" s="T891">n</ta>
            <ta e="T893" id="Seg_11172" s="T892">conj</ta>
            <ta e="T894" id="Seg_11173" s="T893">n</ta>
            <ta e="T895" id="Seg_11174" s="T894">n</ta>
            <ta e="T896" id="Seg_11175" s="T895">v</ta>
            <ta e="T897" id="Seg_11176" s="T896">n</ta>
            <ta e="T898" id="Seg_11177" s="T897">v</ta>
            <ta e="T899" id="Seg_11178" s="T898">dempro</ta>
            <ta e="T900" id="Seg_11179" s="T899">n</ta>
            <ta e="T901" id="Seg_11180" s="T900">n</ta>
            <ta e="T902" id="Seg_11181" s="T901">v</ta>
            <ta e="T903" id="Seg_11182" s="T902">n</ta>
            <ta e="T904" id="Seg_11183" s="T903">v</ta>
            <ta e="T905" id="Seg_11184" s="T904">n</ta>
            <ta e="T906" id="Seg_11185" s="T905">v</ta>
            <ta e="T907" id="Seg_11186" s="T906">dempro</ta>
            <ta e="T908" id="Seg_11187" s="T907">v</ta>
            <ta e="T910" id="Seg_11188" s="T909">dempro</ta>
            <ta e="T911" id="Seg_11189" s="T910">num</ta>
            <ta e="T912" id="Seg_11190" s="T911">n</ta>
            <ta e="T913" id="Seg_11191" s="T912">v</ta>
            <ta e="T914" id="Seg_11192" s="T913">n</ta>
            <ta e="T915" id="Seg_11193" s="T914">conj</ta>
            <ta e="T916" id="Seg_11194" s="T915">n</ta>
            <ta e="T917" id="Seg_11195" s="T916">pers</ta>
            <ta e="T918" id="Seg_11196" s="T917">ptcl</ta>
            <ta e="T919" id="Seg_11197" s="T918">que</ta>
            <ta e="T920" id="Seg_11198" s="T919">v</ta>
            <ta e="T921" id="Seg_11199" s="T920">n</ta>
            <ta e="T922" id="Seg_11200" s="T921">v</ta>
            <ta e="T925" id="Seg_11201" s="T924">adj</ta>
            <ta e="T926" id="Seg_11202" s="T925">n</ta>
            <ta e="T927" id="Seg_11203" s="T926">v</ta>
            <ta e="T928" id="Seg_11204" s="T927">ptcl</ta>
            <ta e="T929" id="Seg_11205" s="T928">n</ta>
            <ta e="T930" id="Seg_11206" s="T929">v</ta>
            <ta e="T931" id="Seg_11207" s="T930">pers</ta>
            <ta e="T932" id="Seg_11208" s="T931">n</ta>
            <ta e="T933" id="Seg_11209" s="T932">ptcl</ta>
            <ta e="T934" id="Seg_11210" s="T933">v</ta>
            <ta e="T935" id="Seg_11211" s="T934">n</ta>
            <ta e="T936" id="Seg_11212" s="T935">ptcl</ta>
            <ta e="T937" id="Seg_11213" s="T936">v</ta>
            <ta e="T938" id="Seg_11214" s="T937">pers</ta>
            <ta e="T939" id="Seg_11215" s="T938">n</ta>
            <ta e="T940" id="Seg_11216" s="T939">n</ta>
            <ta e="T941" id="Seg_11217" s="T940">adj</ta>
            <ta e="T942" id="Seg_11218" s="T941">n</ta>
            <ta e="T943" id="Seg_11219" s="T942">quant</ta>
            <ta e="T946" id="Seg_11220" s="T945">num</ta>
            <ta e="T947" id="Seg_11221" s="T946">n</ta>
            <ta e="T948" id="Seg_11222" s="T947">num</ta>
            <ta e="T949" id="Seg_11223" s="T948">n</ta>
            <ta e="T950" id="Seg_11224" s="T949">n</ta>
            <ta e="T951" id="Seg_11225" s="T950">n</ta>
            <ta e="T952" id="Seg_11226" s="T951">refl</ta>
            <ta e="T953" id="Seg_11227" s="T952">n</ta>
            <ta e="T955" id="Seg_11228" s="T954">n</ta>
            <ta e="T956" id="Seg_11229" s="T955">num</ta>
            <ta e="T957" id="Seg_11230" s="T956">adj</ta>
            <ta e="T958" id="Seg_11231" s="T957">adj</ta>
            <ta e="T959" id="Seg_11232" s="T958">n</ta>
            <ta e="T960" id="Seg_11233" s="T959">adj</ta>
            <ta e="T961" id="Seg_11234" s="T960">num</ta>
            <ta e="T962" id="Seg_11235" s="T961">n</ta>
            <ta e="T963" id="Seg_11236" s="T962">conj</ta>
            <ta e="T964" id="Seg_11237" s="T963">num</ta>
            <ta e="T965" id="Seg_11238" s="T964">n</ta>
            <ta e="T966" id="Seg_11239" s="T965">v</ta>
            <ta e="T970" id="Seg_11240" s="T969">pers</ta>
            <ta e="T971" id="Seg_11241" s="T970">pers</ta>
            <ta e="T972" id="Seg_11242" s="T971">v</ta>
            <ta e="T973" id="Seg_11243" s="T972">conj</ta>
            <ta e="T974" id="Seg_11244" s="T973">que</ta>
            <ta e="T975" id="Seg_11245" s="T974">v</ta>
            <ta e="T976" id="Seg_11246" s="T975">n</ta>
            <ta e="T977" id="Seg_11247" s="T976">aux</ta>
            <ta e="T978" id="Seg_11248" s="T977">v</ta>
            <ta e="T979" id="Seg_11249" s="T978">ptcl</ta>
            <ta e="T980" id="Seg_11250" s="T979">pers</ta>
            <ta e="T981" id="Seg_11251" s="T980">v</ta>
            <ta e="T982" id="Seg_11252" s="T981">quant</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-PKZ">
            <ta e="T17" id="Seg_11253" s="T16">0.1.h:S v:pred</ta>
            <ta e="T22" id="Seg_11254" s="T21">np:S</ta>
            <ta e="T23" id="Seg_11255" s="T22">ptcl:pred</ta>
            <ta e="T24" id="Seg_11256" s="T23">np:O</ta>
            <ta e="T25" id="Seg_11257" s="T24">v:pred</ta>
            <ta e="T26" id="Seg_11258" s="T25">np:O</ta>
            <ta e="T27" id="Seg_11259" s="T26">v:pred</ta>
            <ta e="T28" id="Seg_11260" s="T27">np:O</ta>
            <ta e="T29" id="Seg_11261" s="T28">ptcl:pred</ta>
            <ta e="T30" id="Seg_11262" s="T29">v:pred</ta>
            <ta e="T31" id="Seg_11263" s="T30">ptcl:pred</ta>
            <ta e="T32" id="Seg_11264" s="T31">np:O</ta>
            <ta e="T33" id="Seg_11265" s="T32">v:pred</ta>
            <ta e="T34" id="Seg_11266" s="T33">ptcl:pred</ta>
            <ta e="T35" id="Seg_11267" s="T34">np:O</ta>
            <ta e="T36" id="Seg_11268" s="T35">v:pred</ta>
            <ta e="T49" id="Seg_11269" s="T46">np:O</ta>
            <ta e="T50" id="Seg_11270" s="T49">v:pred</ta>
            <ta e="T51" id="Seg_11271" s="T50">ptcl:pred</ta>
            <ta e="T52" id="Seg_11272" s="T51">np:O</ta>
            <ta e="T53" id="Seg_11273" s="T52">v:pred</ta>
            <ta e="T54" id="Seg_11274" s="T53">ptcl:pred</ta>
            <ta e="T55" id="Seg_11275" s="T54">np:O</ta>
            <ta e="T56" id="Seg_11276" s="T55">v:pred</ta>
            <ta e="T57" id="Seg_11277" s="T56">np:O</ta>
            <ta e="T58" id="Seg_11278" s="T57">ptcl:pred</ta>
            <ta e="T59" id="Seg_11279" s="T58">v:pred</ta>
            <ta e="T60" id="Seg_11280" s="T59">ptcl:pred</ta>
            <ta e="T61" id="Seg_11281" s="T60">np:O</ta>
            <ta e="T62" id="Seg_11282" s="T61">v:pred</ta>
            <ta e="T63" id="Seg_11283" s="T62">np:O</ta>
            <ta e="T64" id="Seg_11284" s="T63">v:pred</ta>
            <ta e="T65" id="Seg_11285" s="T64">ptcl:pred</ta>
            <ta e="T66" id="Seg_11286" s="T65">np:O</ta>
            <ta e="T68" id="Seg_11287" s="T67">ptcl:pred</ta>
            <ta e="T69" id="Seg_11288" s="T68">ptcl:pred</ta>
            <ta e="T70" id="Seg_11289" s="T69">np:O</ta>
            <ta e="T71" id="Seg_11290" s="T70">v:pred</ta>
            <ta e="T72" id="Seg_11291" s="T71">np:O</ta>
            <ta e="T73" id="Seg_11292" s="T72">v:pred</ta>
            <ta e="T74" id="Seg_11293" s="T73">ptcl:pred</ta>
            <ta e="T92" id="Seg_11294" s="T91">np.h:S</ta>
            <ta e="T93" id="Seg_11295" s="T92">v:pred</ta>
            <ta e="T94" id="Seg_11296" s="T93">s:purp</ta>
            <ta e="T95" id="Seg_11297" s="T94">v:pred 0.2.h:S</ta>
            <ta e="T96" id="Seg_11298" s="T95">v:pred 0.1.h:S</ta>
            <ta e="T97" id="Seg_11299" s="T96">v:pred</ta>
            <ta e="T99" id="Seg_11300" s="T98">ptcl:pred</ta>
            <ta e="T100" id="Seg_11301" s="T99">v:pred 0.2.h:S</ta>
            <ta e="T101" id="Seg_11302" s="T100">v:pred 0.2.h:S</ta>
            <ta e="T103" id="Seg_11303" s="T102">np:S</ta>
            <ta e="T104" id="Seg_11304" s="T103">v:pred</ta>
            <ta e="T105" id="Seg_11305" s="T104">np:S</ta>
            <ta e="T106" id="Seg_11306" s="T105">v:pred</ta>
            <ta e="T107" id="Seg_11307" s="T106">np:S</ta>
            <ta e="T108" id="Seg_11308" s="T107">v:pred</ta>
            <ta e="T109" id="Seg_11309" s="T108">np:S</ta>
            <ta e="T110" id="Seg_11310" s="T109">v:pred</ta>
            <ta e="T111" id="Seg_11311" s="T110">np:S</ta>
            <ta e="T112" id="Seg_11312" s="T111">np:S</ta>
            <ta e="T113" id="Seg_11313" s="T112">np:S</ta>
            <ta e="T114" id="Seg_11314" s="T113">v:pred 0.2.h:S</ta>
            <ta e="T115" id="Seg_11315" s="T114">np:O</ta>
            <ta e="T116" id="Seg_11316" s="T115">v:pred 0.2.h:S</ta>
            <ta e="T118" id="Seg_11317" s="T117">v:pred 0.2.h:S</ta>
            <ta e="T121" id="Seg_11318" s="T120">v:pred 0.1.h:S</ta>
            <ta e="T122" id="Seg_11319" s="T121">pro.h:O</ta>
            <ta e="T123" id="Seg_11320" s="T122">v:pred 0.2.h:S</ta>
            <ta e="T125" id="Seg_11321" s="T124">v:pred 0.2.h:S</ta>
            <ta e="T127" id="Seg_11322" s="T126">v:pred 0.2.h:S</ta>
            <ta e="T129" id="Seg_11323" s="T128">v:pred 0.2.h:S</ta>
            <ta e="T131" id="Seg_11324" s="T130">v:pred 0.2.h:S</ta>
            <ta e="T132" id="Seg_11325" s="T131">pro.h:S</ta>
            <ta e="T135" id="Seg_11326" s="T134">v:pred</ta>
            <ta e="T138" id="Seg_11327" s="T137">pro.h:S</ta>
            <ta e="T140" id="Seg_11328" s="T139">v:pred</ta>
            <ta e="T145" id="Seg_11329" s="T144">v:pred 0.2.h:S</ta>
            <ta e="T146" id="Seg_11330" s="T145">v:pred 0.2.h:S</ta>
            <ta e="T148" id="Seg_11331" s="T147">pro.h:S</ta>
            <ta e="T149" id="Seg_11332" s="T148">pro:O</ta>
            <ta e="T151" id="Seg_11333" s="T150">v:pred</ta>
            <ta e="T153" id="Seg_11334" s="T152">v:pred 0.1.h:S</ta>
            <ta e="T159" id="Seg_11335" s="T158">v:pred</ta>
            <ta e="T161" id="Seg_11336" s="T160">np.h:S</ta>
            <ta e="T162" id="Seg_11337" s="T161">pro.h:S</ta>
            <ta e="T163" id="Seg_11338" s="T162">v:pred</ta>
            <ta e="T164" id="Seg_11339" s="T163">v:pred 0.2.h:S</ta>
            <ta e="T165" id="Seg_11340" s="T164">s:purp</ta>
            <ta e="T167" id="Seg_11341" s="T166">v:pred 0.1.h:S</ta>
            <ta e="T168" id="Seg_11342" s="T167">v:pred 0.2.h:S</ta>
            <ta e="T170" id="Seg_11343" s="T169">v:pred</ta>
            <ta e="T171" id="Seg_11344" s="T170">ptcl:pred</ta>
            <ta e="T172" id="Seg_11345" s="T171">pro.h:S</ta>
            <ta e="T173" id="Seg_11346" s="T172">v:pred</ta>
            <ta e="T174" id="Seg_11347" s="T173">pro.h:S</ta>
            <ta e="T175" id="Seg_11348" s="T174">pro.h:S</ta>
            <ta e="T176" id="Seg_11349" s="T175">ptcl.neg</ta>
            <ta e="T179" id="Seg_11350" s="T178">v:pred</ta>
            <ta e="T181" id="Seg_11351" s="T180">pro.h:S</ta>
            <ta e="T183" id="Seg_11352" s="T182">v:pred</ta>
            <ta e="T188" id="Seg_11353" s="T187">v:pred 0.3.h:S</ta>
            <ta e="T189" id="Seg_11354" s="T188">s:purp</ta>
            <ta e="T190" id="Seg_11355" s="T189">pro.h:S</ta>
            <ta e="T191" id="Seg_11356" s="T190">v:pred</ta>
            <ta e="T193" id="Seg_11357" s="T192">v:pred 0.1.h:S</ta>
            <ta e="T195" id="Seg_11358" s="T194">v:pred 0.1.h:S</ta>
            <ta e="T196" id="Seg_11359" s="T195">v:pred 0.1.h:S</ta>
            <ta e="T198" id="Seg_11360" s="T197">v:pred 0.1.h:S</ta>
            <ta e="T199" id="Seg_11361" s="T198">v:pred 0.2.h:S</ta>
            <ta e="T200" id="Seg_11362" s="T199">v:pred 0.2.h:S</ta>
            <ta e="T206" id="Seg_11363" s="T205">np:S</ta>
            <ta e="T207" id="Seg_11364" s="T206">v:pred</ta>
            <ta e="T208" id="Seg_11365" s="T207">np:S</ta>
            <ta e="T209" id="Seg_11366" s="T208">v:pred</ta>
            <ta e="T210" id="Seg_11367" s="T209">np:S</ta>
            <ta e="T211" id="Seg_11368" s="T210">np:S</ta>
            <ta e="T212" id="Seg_11369" s="T211">v:pred 0.2.h:S</ta>
            <ta e="T213" id="Seg_11370" s="T212">v:pred 0.2.h:S</ta>
            <ta e="T214" id="Seg_11371" s="T213">pro.h:S</ta>
            <ta e="T217" id="Seg_11372" s="T216">pro.h:S</ta>
            <ta e="T218" id="Seg_11373" s="T217">v:pred</ta>
            <ta e="T219" id="Seg_11374" s="T218">v:pred 0.3.h:S</ta>
            <ta e="T221" id="Seg_11375" s="T220">v:pred 0.3.h:S</ta>
            <ta e="T222" id="Seg_11376" s="T221">s:purp</ta>
            <ta e="T225" id="Seg_11377" s="T224">conv:pred</ta>
            <ta e="T226" id="Seg_11378" s="T225">v:pred 0.3.h:S</ta>
            <ta e="T228" id="Seg_11379" s="T227">pro.h:S</ta>
            <ta e="T229" id="Seg_11380" s="T228">v:pred</ta>
            <ta e="T234" id="Seg_11381" s="T233">pro.h:S</ta>
            <ta e="T235" id="Seg_11382" s="T234">pro.h:S</ta>
            <ta e="T238" id="Seg_11383" s="T237">v:pred 0.1.h:S</ta>
            <ta e="T241" id="Seg_11384" s="T240">np.h:O</ta>
            <ta e="T242" id="Seg_11385" s="T241">v:pred 0.3.h:S</ta>
            <ta e="T244" id="Seg_11386" s="T243">np.h:O</ta>
            <ta e="T245" id="Seg_11387" s="T244">pro.h:S</ta>
            <ta e="T249" id="Seg_11388" s="T248">v:pred</ta>
            <ta e="T250" id="Seg_11389" s="T249">v:pred 0.1.h:S</ta>
            <ta e="T257" id="Seg_11390" s="T256">pro.h:S</ta>
            <ta e="T260" id="Seg_11391" s="T259">v:pred</ta>
            <ta e="T261" id="Seg_11392" s="T260">v:pred 0.1.h:S</ta>
            <ta e="T263" id="Seg_11393" s="T262">np:S</ta>
            <ta e="T264" id="Seg_11394" s="T263">v:pred</ta>
            <ta e="T267" id="Seg_11395" s="T266">np:O</ta>
            <ta e="T268" id="Seg_11396" s="T267">v:pred 0.1.h:S</ta>
            <ta e="T269" id="Seg_11397" s="T268">np:O</ta>
            <ta e="T270" id="Seg_11398" s="T269">v:pred 0.1.h:S</ta>
            <ta e="T272" id="Seg_11399" s="T271">v:pred 0.1.h:S</ta>
            <ta e="T273" id="Seg_11400" s="T272">np:S</ta>
            <ta e="T274" id="Seg_11401" s="T273">v:pred</ta>
            <ta e="T275" id="Seg_11402" s="T274">pro.h:S</ta>
            <ta e="T276" id="Seg_11403" s="T275">v:pred</ta>
            <ta e="T278" id="Seg_11404" s="T277">v:pred 0.1.h:S</ta>
            <ta e="T279" id="Seg_11405" s="T278">v:pred 0.1.h:S</ta>
            <ta e="T282" id="Seg_11406" s="T281">v:pred 0.1.h:S</ta>
            <ta e="T285" id="Seg_11407" s="T284">pro.h:S</ta>
            <ta e="T286" id="Seg_11408" s="T285">v:pred</ta>
            <ta e="T287" id="Seg_11409" s="T286">conv:pred</ta>
            <ta e="T288" id="Seg_11410" s="T287">np:O</ta>
            <ta e="T289" id="Seg_11411" s="T288">v:pred 0.1.h:S</ta>
            <ta e="T291" id="Seg_11412" s="T290">v:pred 0.1.h:S</ta>
            <ta e="T292" id="Seg_11413" s="T291">v:pred 0.1.h:S</ta>
            <ta e="T295" id="Seg_11414" s="T294">v:pred 0.3:S</ta>
            <ta e="T298" id="Seg_11415" s="T297">v:pred</ta>
            <ta e="T300" id="Seg_11416" s="T299">ptcl:pred</ta>
            <ta e="T301" id="Seg_11417" s="T300">np.h:S</ta>
            <ta e="T303" id="Seg_11418" s="T302">v:pred</ta>
            <ta e="T307" id="Seg_11419" s="T306">np:O</ta>
            <ta e="T308" id="Seg_11420" s="T307">v:pred 0.3.h:S</ta>
            <ta e="T310" id="Seg_11421" s="T309">np:O</ta>
            <ta e="T311" id="Seg_11422" s="T310">v:pred 0.3.h:S</ta>
            <ta e="T312" id="Seg_11423" s="T311">v:pred 0.3.h:S</ta>
            <ta e="T316" id="Seg_11424" s="T315">v:pred 0.3.h:S</ta>
            <ta e="T317" id="Seg_11425" s="T316">pro.h:S</ta>
            <ta e="T318" id="Seg_11426" s="T317">pro.h:O</ta>
            <ta e="T319" id="Seg_11427" s="T318">v:pred</ta>
            <ta e="T320" id="Seg_11428" s="T319">v:pred 0.2.h:S</ta>
            <ta e="T321" id="Seg_11429" s="T320">v:pred 0.1.h:S</ta>
            <ta e="T322" id="Seg_11430" s="T321">pro.h:S</ta>
            <ta e="T323" id="Seg_11431" s="T322">v:pred</ta>
            <ta e="T324" id="Seg_11432" s="T323">v:pred 0.2.h:S</ta>
            <ta e="T326" id="Seg_11433" s="T325">v:pred 0.2.h:S</ta>
            <ta e="T327" id="Seg_11434" s="T326">v:pred 0.2.h:S</ta>
            <ta e="T329" id="Seg_11435" s="T328">np:O</ta>
            <ta e="T330" id="Seg_11436" s="T329">v:pred</ta>
            <ta e="T331" id="Seg_11437" s="T330">np:S</ta>
            <ta e="T332" id="Seg_11438" s="T331">v:pred</ta>
            <ta e="T333" id="Seg_11439" s="T332">v:pred 0.2.h:S</ta>
            <ta e="T336" id="Seg_11440" s="T335">np:S</ta>
            <ta e="T337" id="Seg_11441" s="T336">v:pred</ta>
            <ta e="T340" id="Seg_11442" s="T339">np:O</ta>
            <ta e="T341" id="Seg_11443" s="T340">v:pred 0.2.h:S</ta>
            <ta e="T342" id="Seg_11444" s="T341">np:O</ta>
            <ta e="T343" id="Seg_11445" s="T342">v:pred 0.2.h:S</ta>
            <ta e="T344" id="Seg_11446" s="T343">np:O</ta>
            <ta e="T345" id="Seg_11447" s="T344">v:pred 0.2.h:S</ta>
            <ta e="T346" id="Seg_11448" s="T345">np:S</ta>
            <ta e="T347" id="Seg_11449" s="T346">v:pred</ta>
            <ta e="T348" id="Seg_11450" s="T347">v:pred 0.2.h:S</ta>
            <ta e="T349" id="Seg_11451" s="T348">np:O</ta>
            <ta e="T350" id="Seg_11452" s="T349">v:pred 0.2.h:S</ta>
            <ta e="T351" id="Seg_11453" s="T350">np:O</ta>
            <ta e="T352" id="Seg_11454" s="T351">v:pred 0.2.h:S</ta>
            <ta e="T354" id="Seg_11455" s="T353">np:O</ta>
            <ta e="T355" id="Seg_11456" s="T354">v:pred 0.2.h:S</ta>
            <ta e="T356" id="Seg_11457" s="T355">np:O</ta>
            <ta e="T357" id="Seg_11458" s="T356">v:pred 0.2.h:S</ta>
            <ta e="T359" id="Seg_11459" s="T358">ptcl:pred</ta>
            <ta e="T360" id="Seg_11460" s="T359">pro.h:S</ta>
            <ta e="T361" id="Seg_11461" s="T360">np:O</ta>
            <ta e="T362" id="Seg_11462" s="T361">v:pred</ta>
            <ta e="T363" id="Seg_11463" s="T362">np:O</ta>
            <ta e="T364" id="Seg_11464" s="T363">v:pred</ta>
            <ta e="T365" id="Seg_11465" s="T364">ptcl:pred</ta>
            <ta e="T368" id="Seg_11466" s="T367">np:S</ta>
            <ta e="T369" id="Seg_11467" s="T368">v:pred</ta>
            <ta e="T370" id="Seg_11468" s="T369">v:pred</ta>
            <ta e="T373" id="Seg_11469" s="T372">ptcl:pred</ta>
            <ta e="T374" id="Seg_11470" s="T373">np:S</ta>
            <ta e="T375" id="Seg_11471" s="T374">v:pred</ta>
            <ta e="T379" id="Seg_11472" s="T378">np:S</ta>
            <ta e="T381" id="Seg_11473" s="T380">v:pred</ta>
            <ta e="T383" id="Seg_11474" s="T382">pro:S</ta>
            <ta e="T384" id="Seg_11475" s="T383">v:pred</ta>
            <ta e="T385" id="Seg_11476" s="T384">pro.h:S</ta>
            <ta e="T386" id="Seg_11477" s="T385">v:pred</ta>
            <ta e="T390" id="Seg_11478" s="T389">ptcl:pred</ta>
            <ta e="T391" id="Seg_11479" s="T390">np:O</ta>
            <ta e="T392" id="Seg_11480" s="T391">v:pred</ta>
            <ta e="T393" id="Seg_11481" s="T392">ptcl:pred</ta>
            <ta e="T394" id="Seg_11482" s="T393">np:O</ta>
            <ta e="T395" id="Seg_11483" s="T394">v:pred</ta>
            <ta e="T396" id="Seg_11484" s="T395">ptcl:pred</ta>
            <ta e="T400" id="Seg_11485" s="T399">np:O</ta>
            <ta e="T401" id="Seg_11486" s="T400">v:pred</ta>
            <ta e="T402" id="Seg_11487" s="T401">ptcl:pred</ta>
            <ta e="T416" id="Seg_11488" s="T415">pro.h:S</ta>
            <ta e="T419" id="Seg_11489" s="T418">v:pred</ta>
            <ta e="T425" id="Seg_11490" s="T424">pro.h:S</ta>
            <ta e="T427" id="Seg_11491" s="T426">v:pred</ta>
            <ta e="T429" id="Seg_11492" s="T428">np:O</ta>
            <ta e="T430" id="Seg_11493" s="T429">v:pred 0.3.h:S</ta>
            <ta e="T431" id="Seg_11494" s="T430">np:O</ta>
            <ta e="T432" id="Seg_11495" s="T431">v:pred 0.3.h:S</ta>
            <ta e="T434" id="Seg_11496" s="T433">v:pred</ta>
            <ta e="T435" id="Seg_11497" s="T434">ptcl:pred</ta>
            <ta e="T437" id="Seg_11498" s="T436">v:pred</ta>
            <ta e="T438" id="Seg_11499" s="T437">np:O</ta>
            <ta e="T439" id="Seg_11500" s="T438">v:pred</ta>
            <ta e="T440" id="Seg_11501" s="T439">np:O</ta>
            <ta e="T441" id="Seg_11502" s="T440">v:pred</ta>
            <ta e="T443" id="Seg_11503" s="T442">np:O</ta>
            <ta e="T444" id="Seg_11504" s="T443">v:pred</ta>
            <ta e="T446" id="Seg_11505" s="T445">v:pred</ta>
            <ta e="T448" id="Seg_11506" s="T447">ptcl:pred</ta>
            <ta e="T449" id="Seg_11507" s="T448">v:pred</ta>
            <ta e="T454" id="Seg_11508" s="T453">v:pred</ta>
            <ta e="T459" id="Seg_11509" s="T458">s:purp</ta>
            <ta e="T460" id="Seg_11510" s="T459">np:O</ta>
            <ta e="T461" id="Seg_11511" s="T460">s:purp</ta>
            <ta e="T463" id="Seg_11512" s="T462">v:pred 0.3:S</ta>
            <ta e="T464" id="Seg_11513" s="T463">v:pred</ta>
            <ta e="T465" id="Seg_11514" s="T464">ptcl:pred</ta>
            <ta e="T467" id="Seg_11515" s="T466">v:pred</ta>
            <ta e="T468" id="Seg_11516" s="T467">ptcl:pred</ta>
            <ta e="T477" id="Seg_11517" s="T476">v:pred</ta>
            <ta e="T478" id="Seg_11518" s="T477">ptcl:pred</ta>
            <ta e="T482" id="Seg_11519" s="T481">ptcl:pred</ta>
            <ta e="T483" id="Seg_11520" s="T482">v:pred</ta>
            <ta e="T485" id="Seg_11521" s="T484">v:pred</ta>
            <ta e="T486" id="Seg_11522" s="T485">ptcl:pred</ta>
            <ta e="T491" id="Seg_11523" s="T490">n:pred</ta>
            <ta e="T494" id="Seg_11524" s="T493">np.h:S</ta>
            <ta e="T496" id="Seg_11525" s="T495">adj:pred</ta>
            <ta e="T497" id="Seg_11526" s="T496">adj:pred</ta>
            <ta e="T499" id="Seg_11527" s="T498">np:O</ta>
            <ta e="T500" id="Seg_11528" s="T499">v:pred 0.3.h:S</ta>
            <ta e="T501" id="Seg_11529" s="T500">v:pred 0.3.h:S</ta>
            <ta e="T503" id="Seg_11530" s="T502">v:pred 0.3.h:S</ta>
            <ta e="T504" id="Seg_11531" s="T503">np:O</ta>
            <ta e="T505" id="Seg_11532" s="T504">v:pred 0.3.h:S</ta>
            <ta e="T508" id="Seg_11533" s="T507">np:O</ta>
            <ta e="T512" id="Seg_11534" s="T511">v:pred 0.3.h:S</ta>
            <ta e="T520" id="Seg_11535" s="T519">v:pred 0.2.h:S</ta>
            <ta e="T522" id="Seg_11536" s="T521">v:pred 0.3.h:S</ta>
            <ta e="T524" id="Seg_11537" s="T523">v:pred 0.1.h:S</ta>
            <ta e="T525" id="Seg_11538" s="T524">v:pred 0.2.h:S</ta>
            <ta e="T526" id="Seg_11539" s="T525">v:pred</ta>
            <ta e="T527" id="Seg_11540" s="T526">v:pred 0.2.h:S</ta>
            <ta e="T529" id="Seg_11541" s="T528">np.h:S</ta>
            <ta e="T530" id="Seg_11542" s="T529">v:pred</ta>
            <ta e="T531" id="Seg_11543" s="T530">np:O</ta>
            <ta e="T534" id="Seg_11544" s="T533">np:O</ta>
            <ta e="T535" id="Seg_11545" s="T534">v:pred 0.1.h:S</ta>
            <ta e="T536" id="Seg_11546" s="T535">v:pred 0.2.h:S</ta>
            <ta e="T538" id="Seg_11547" s="T537">np:O</ta>
            <ta e="T541" id="Seg_11548" s="T540">v:pred 0.2.h:S</ta>
            <ta e="T544" id="Seg_11549" s="T543">pro.h:S</ta>
            <ta e="T546" id="Seg_11550" s="T545">v:pred</ta>
            <ta e="T547" id="Seg_11551" s="T546">v:pred 0.2.h:S</ta>
            <ta e="T550" id="Seg_11552" s="T549">np:O</ta>
            <ta e="T551" id="Seg_11553" s="T550">v:pred 0.1.h:S</ta>
            <ta e="T553" id="Seg_11554" s="T552">v:pred 0.1.h:S</ta>
            <ta e="T554" id="Seg_11555" s="T553">np:O</ta>
            <ta e="T557" id="Seg_11556" s="T556">v:pred</ta>
            <ta e="T567" id="Seg_11557" s="T566">ptcl.neg</ta>
            <ta e="T568" id="Seg_11558" s="T567">v:pred 0.2.h:S</ta>
            <ta e="T569" id="Seg_11559" s="T568">v:pred 0.2.h:S</ta>
            <ta e="T574" id="Seg_11560" s="T573">v:pred 0.2.h:S</ta>
            <ta e="T578" id="Seg_11561" s="T577">v:pred 0.2.h:S</ta>
            <ta e="T583" id="Seg_11562" s="T582">v:pred 0.2.h:S</ta>
            <ta e="T587" id="Seg_11563" s="T586">np:S</ta>
            <ta e="T588" id="Seg_11564" s="T587">v:pred</ta>
            <ta e="T589" id="Seg_11565" s="T588">v:pred 0.2.h:S</ta>
            <ta e="T590" id="Seg_11566" s="T589">np:S</ta>
            <ta e="T593" id="Seg_11567" s="T592">v:pred 0.2.h:S</ta>
            <ta e="T595" id="Seg_11568" s="T594">v:pred 0.2.h:S</ta>
            <ta e="T597" id="Seg_11569" s="T596">v:pred 0.2.h:S</ta>
            <ta e="T620" id="Seg_11570" s="T619">np:O</ta>
            <ta e="T621" id="Seg_11571" s="T620">v:pred 0.2.h:S</ta>
            <ta e="T623" id="Seg_11572" s="T622">v:pred 0.2.h:S</ta>
            <ta e="T624" id="Seg_11573" s="T623">pro.h:S</ta>
            <ta e="T625" id="Seg_11574" s="T624">ptcl.neg</ta>
            <ta e="T626" id="Seg_11575" s="T625">v:pred</ta>
            <ta e="T627" id="Seg_11576" s="T626">ptcl.neg</ta>
            <ta e="T628" id="Seg_11577" s="T627">ptcl:pred</ta>
            <ta e="T629" id="Seg_11578" s="T628">v:pred</ta>
            <ta e="T631" id="Seg_11579" s="T630">ptcl.neg</ta>
            <ta e="T632" id="Seg_11580" s="T631">adj:pred</ta>
            <ta e="T633" id="Seg_11581" s="T632">cop 0.3:S</ta>
            <ta e="T634" id="Seg_11582" s="T633">np:O</ta>
            <ta e="T635" id="Seg_11583" s="T634">v:pred 0.1.h:S</ta>
            <ta e="T637" id="Seg_11584" s="T636">np:S</ta>
            <ta e="T638" id="Seg_11585" s="T637">v:pred</ta>
            <ta e="T639" id="Seg_11586" s="T638">v:pred 0.2.h:S</ta>
            <ta e="T640" id="Seg_11587" s="T639">pro:O</ta>
            <ta e="T641" id="Seg_11588" s="T640">np:O</ta>
            <ta e="T642" id="Seg_11589" s="T641">s:purp</ta>
            <ta e="T649" id="Seg_11590" s="T648">np:O</ta>
            <ta e="T650" id="Seg_11591" s="T649">v:pred 0.2.h:S</ta>
            <ta e="T651" id="Seg_11592" s="T650">pro.h:B</ta>
            <ta e="T654" id="Seg_11593" s="T652">ptcl:pred</ta>
            <ta e="T655" id="Seg_11594" s="T654">np:S</ta>
            <ta e="T656" id="Seg_11595" s="T655">np:S</ta>
            <ta e="T658" id="Seg_11596" s="T657">v:pred</ta>
            <ta e="T659" id="Seg_11597" s="T658">np:S</ta>
            <ta e="T661" id="Seg_11598" s="T660">v:pred</ta>
            <ta e="T665" id="Seg_11599" s="T664">pro.h:S</ta>
            <ta e="T667" id="Seg_11600" s="T666">v:pred</ta>
            <ta e="T670" id="Seg_11601" s="T669">v:pred 0.1.h:S</ta>
            <ta e="T671" id="Seg_11602" s="T670">np:S </ta>
            <ta e="T672" id="Seg_11603" s="T671">v:pred</ta>
            <ta e="T673" id="Seg_11604" s="T672">np:S</ta>
            <ta e="T674" id="Seg_11605" s="T673">v:pred</ta>
            <ta e="T675" id="Seg_11606" s="T674">np:S</ta>
            <ta e="T677" id="Seg_11607" s="T676">v:pred</ta>
            <ta e="T682" id="Seg_11608" s="T681">n:pred</ta>
            <ta e="T683" id="Seg_11609" s="T682">np:S</ta>
            <ta e="T685" id="Seg_11610" s="T684">v:pred</ta>
            <ta e="T687" id="Seg_11611" s="T686">pro:S</ta>
            <ta e="T688" id="Seg_11612" s="T687">v:pred</ta>
            <ta e="T692" id="Seg_11613" s="T691">np.h:S</ta>
            <ta e="T694" id="Seg_11614" s="T693">v:pred</ta>
            <ta e="T695" id="Seg_11615" s="T694">np:O</ta>
            <ta e="T696" id="Seg_11616" s="T695">v:pred 0.3.h:S</ta>
            <ta e="T697" id="Seg_11617" s="T696">np:S</ta>
            <ta e="T698" id="Seg_11618" s="T697">v:pred</ta>
            <ta e="T700" id="Seg_11619" s="T699">ptcl:pred</ta>
            <ta e="T701" id="Seg_11620" s="T700">v:pred</ta>
            <ta e="T702" id="Seg_11621" s="T701">np:O</ta>
            <ta e="T703" id="Seg_11622" s="T702">s:purp</ta>
            <ta e="T704" id="Seg_11623" s="T703">np:O</ta>
            <ta e="T705" id="Seg_11624" s="T704">ptcl:pred</ta>
            <ta e="T706" id="Seg_11625" s="T705">v:pred</ta>
            <ta e="T707" id="Seg_11626" s="T706">ptcl:pred</ta>
            <ta e="T708" id="Seg_11627" s="T707">np:O</ta>
            <ta e="T709" id="Seg_11628" s="T708">v:pred</ta>
            <ta e="T710" id="Seg_11629" s="T709">np:O</ta>
            <ta e="T711" id="Seg_11630" s="T710">v:pred</ta>
            <ta e="T712" id="Seg_11631" s="T711">ptcl:pred</ta>
            <ta e="T713" id="Seg_11632" s="T712">np:O</ta>
            <ta e="T714" id="Seg_11633" s="T713">v:pred</ta>
            <ta e="T715" id="Seg_11634" s="T714">np:O</ta>
            <ta e="T716" id="Seg_11635" s="T715">v:pred</ta>
            <ta e="T717" id="Seg_11636" s="T716">np:O</ta>
            <ta e="T718" id="Seg_11637" s="T717">v:pred</ta>
            <ta e="T719" id="Seg_11638" s="T718">np:O</ta>
            <ta e="T720" id="Seg_11639" s="T719">ptcl:pred</ta>
            <ta e="T721" id="Seg_11640" s="T720">v:pred</ta>
            <ta e="T723" id="Seg_11641" s="T722">np:O</ta>
            <ta e="T725" id="Seg_11642" s="T724">v:pred 0.1.h:S</ta>
            <ta e="T726" id="Seg_11643" s="T725">np:O</ta>
            <ta e="T727" id="Seg_11644" s="T726">v:pred 0.1.h:S</ta>
            <ta e="T728" id="Seg_11645" s="T727">np:O</ta>
            <ta e="T729" id="Seg_11646" s="T728">v:pred 0.1.h:S</ta>
            <ta e="T730" id="Seg_11647" s="T729">np:O</ta>
            <ta e="T731" id="Seg_11648" s="T730">v:pred 0.1.h:S</ta>
            <ta e="T732" id="Seg_11649" s="T731">np:O</ta>
            <ta e="T733" id="Seg_11650" s="T732">v:pred 0.1.h:S</ta>
            <ta e="T734" id="Seg_11651" s="T733">np:O</ta>
            <ta e="T735" id="Seg_11652" s="T734">v:pred 0.1.h:S</ta>
            <ta e="T737" id="Seg_11653" s="T736">np:S</ta>
            <ta e="T739" id="Seg_11654" s="T738">np:O</ta>
            <ta e="T740" id="Seg_11655" s="T739">v:pred</ta>
            <ta e="T741" id="Seg_11656" s="T740">v:pred 0.3:S</ta>
            <ta e="T744" id="Seg_11657" s="T743">np.h:S</ta>
            <ta e="T745" id="Seg_11658" s="T744">v:pred</ta>
            <ta e="T749" id="Seg_11659" s="T748">ptcl:pred</ta>
            <ta e="T750" id="Seg_11660" s="T749">np.h:S </ta>
            <ta e="T751" id="Seg_11661" s="T750">v:pred</ta>
            <ta e="T752" id="Seg_11662" s="T751">ptcl:pred</ta>
            <ta e="T755" id="Seg_11663" s="T754">np.h:S</ta>
            <ta e="T757" id="Seg_11664" s="T756">v:pred</ta>
            <ta e="T758" id="Seg_11665" s="T757">ptcl:pred</ta>
            <ta e="T759" id="Seg_11666" s="T758">pro.h:O</ta>
            <ta e="T760" id="Seg_11667" s="T759">v:pred</ta>
            <ta e="T761" id="Seg_11668" s="T760">ptcl:pred</ta>
            <ta e="T762" id="Seg_11669" s="T761">s:purp</ta>
            <ta e="T763" id="Seg_11670" s="T762">v:pred</ta>
            <ta e="T764" id="Seg_11671" s="T763">ptcl:pred</ta>
            <ta e="T765" id="Seg_11672" s="T764">s:purp</ta>
            <ta e="T766" id="Seg_11673" s="T765">v:pred</ta>
            <ta e="T771" id="Seg_11674" s="T770">v:pred 0.2.h:S</ta>
            <ta e="T772" id="Seg_11675" s="T771">np.h:O</ta>
            <ta e="T775" id="Seg_11676" s="T774">v:pred 0.3.h:S</ta>
            <ta e="T776" id="Seg_11677" s="T775">v:pred 0.2.h:S</ta>
            <ta e="T778" id="Seg_11678" s="T777">conv:pred</ta>
            <ta e="T779" id="Seg_11679" s="T778">v:pred 0.1.h:S</ta>
            <ta e="T780" id="Seg_11680" s="T779">np:O</ta>
            <ta e="T781" id="Seg_11681" s="T780">v:pred 0.1.h:S</ta>
            <ta e="T783" id="Seg_11682" s="T782">v:pred 0.2.h:S</ta>
            <ta e="T784" id="Seg_11683" s="T783">v:pred 0.2.h:S</ta>
            <ta e="T788" id="Seg_11684" s="T787">v:pred 0.2.h:S</ta>
            <ta e="T791" id="Seg_11685" s="T790">np:O</ta>
            <ta e="T792" id="Seg_11686" s="T791">v:pred 0.1.h:S</ta>
            <ta e="T793" id="Seg_11687" s="T792">ptcl:pred</ta>
            <ta e="T795" id="Seg_11688" s="T794">np:O</ta>
            <ta e="T796" id="Seg_11689" s="T795">v:pred</ta>
            <ta e="T797" id="Seg_11690" s="T796">np:O</ta>
            <ta e="T798" id="Seg_11691" s="T797">v:pred</ta>
            <ta e="T800" id="Seg_11692" s="T799">np:O</ta>
            <ta e="T801" id="Seg_11693" s="T800">v:pred</ta>
            <ta e="T803" id="Seg_11694" s="T802">v:pred 0.1.h:S</ta>
            <ta e="T804" id="Seg_11695" s="T803">v:pred 0.2.h:S</ta>
            <ta e="T807" id="Seg_11696" s="T806">adj:pred</ta>
            <ta e="T811" id="Seg_11697" s="T810">v:pred 0.2.h:S</ta>
            <ta e="T812" id="Seg_11698" s="T811">np.h:S</ta>
            <ta e="T814" id="Seg_11699" s="T812">v:pred</ta>
            <ta e="T819" id="Seg_11700" s="T818">adj:pred</ta>
            <ta e="T822" id="Seg_11701" s="T821">np.h:S</ta>
            <ta e="T823" id="Seg_11702" s="T822">v:pred</ta>
            <ta e="T824" id="Seg_11703" s="T823">adj:pred</ta>
            <ta e="T827" id="Seg_11704" s="T826">v:pred 0.2.h:S</ta>
            <ta e="T828" id="Seg_11705" s="T827">np.h:S</ta>
            <ta e="T829" id="Seg_11706" s="T828">v:pred</ta>
            <ta e="T831" id="Seg_11707" s="T830">pro.h:S</ta>
            <ta e="T832" id="Seg_11708" s="T831">np:O</ta>
            <ta e="T833" id="Seg_11709" s="T832">ptcl.neg</ta>
            <ta e="T834" id="Seg_11710" s="T833">v:pred</ta>
            <ta e="T836" id="Seg_11711" s="T835">np.h:S</ta>
            <ta e="T838" id="Seg_11712" s="T837">v:pred</ta>
            <ta e="T839" id="Seg_11713" s="T838">pro.h:S</ta>
            <ta e="T840" id="Seg_11714" s="T839">np:O</ta>
            <ta e="T842" id="Seg_11715" s="T841">v:pred</ta>
            <ta e="T844" id="Seg_11716" s="T843">np:O</ta>
            <ta e="T845" id="Seg_11717" s="T844">v:pred 0.3.h:S</ta>
            <ta e="T846" id="Seg_11718" s="T845">np:S</ta>
            <ta e="T847" id="Seg_11719" s="T846">adj:pred</ta>
            <ta e="T848" id="Seg_11720" s="T847">cop</ta>
            <ta e="T851" id="Seg_11721" s="T850">pro:S</ta>
            <ta e="T854" id="Seg_11722" s="T853">v:pred</ta>
            <ta e="T856" id="Seg_11723" s="T855">v:pred 0.2.h:S</ta>
            <ta e="T857" id="Seg_11724" s="T856">ptcl.neg</ta>
            <ta e="T858" id="Seg_11725" s="T857">v:pred 0.2.h:S</ta>
            <ta e="T859" id="Seg_11726" s="T858">v:pred 0.2.h:S</ta>
            <ta e="T860" id="Seg_11727" s="T859">pro:O</ta>
            <ta e="T865" id="Seg_11728" s="T864">np:S</ta>
            <ta e="T867" id="Seg_11729" s="T866">v:pred</ta>
            <ta e="T868" id="Seg_11730" s="T867">pro:O</ta>
            <ta e="T869" id="Seg_11731" s="T868">ptcl.neg</ta>
            <ta e="T870" id="Seg_11732" s="T869">v:pred 0.2.h:S</ta>
            <ta e="T871" id="Seg_11733" s="T870">v:pred 0.2.h:S</ta>
            <ta e="T876" id="Seg_11734" s="T875">v:pred 0.2.h:S</ta>
            <ta e="T878" id="Seg_11735" s="T877">v:pred 0.2.h:S</ta>
            <ta e="T879" id="Seg_11736" s="T878">v:pred 0.2.h:S</ta>
            <ta e="T881" id="Seg_11737" s="T880">pro:O</ta>
            <ta e="T882" id="Seg_11738" s="T881">v:pred 0.2.h:S</ta>
            <ta e="T886" id="Seg_11739" s="T885">np.h:S</ta>
            <ta e="T888" id="Seg_11740" s="T887">v:pred</ta>
            <ta e="T890" id="Seg_11741" s="T889">np.h:S</ta>
            <ta e="T891" id="Seg_11742" s="T890">v:pred</ta>
            <ta e="T895" id="Seg_11743" s="T894">np:S</ta>
            <ta e="T896" id="Seg_11744" s="T895">v:pred</ta>
            <ta e="T897" id="Seg_11745" s="T896">np:S</ta>
            <ta e="T898" id="Seg_11746" s="T897">v:pred </ta>
            <ta e="T900" id="Seg_11747" s="T899">np.h:S</ta>
            <ta e="T901" id="Seg_11748" s="T900">np:O</ta>
            <ta e="T902" id="Seg_11749" s="T901">v:pred</ta>
            <ta e="T904" id="Seg_11750" s="T903">v:pred 0.3.h:S</ta>
            <ta e="T905" id="Seg_11751" s="T904">np:O</ta>
            <ta e="T906" id="Seg_11752" s="T905">v:pred 0.3.h:S</ta>
            <ta e="T907" id="Seg_11753" s="T906">pro.h:S</ta>
            <ta e="T908" id="Seg_11754" s="T907">v:pred</ta>
            <ta e="T912" id="Seg_11755" s="T911">np.h:S</ta>
            <ta e="T913" id="Seg_11756" s="T912">v:pred</ta>
            <ta e="T914" id="Seg_11757" s="T913">np:h:S</ta>
            <ta e="T916" id="Seg_11758" s="T915">np.h:S</ta>
            <ta e="T919" id="Seg_11759" s="T918">pro:O</ta>
            <ta e="T920" id="Seg_11760" s="T919">v:pred</ta>
            <ta e="T921" id="Seg_11761" s="T920">np:O</ta>
            <ta e="T922" id="Seg_11762" s="T921">v:pred 0.3.h:S</ta>
            <ta e="T926" id="Seg_11763" s="T925">np:O</ta>
            <ta e="T927" id="Seg_11764" s="T926">v:pred 0.2.h:S</ta>
            <ta e="T929" id="Seg_11765" s="T928">np:O</ta>
            <ta e="T930" id="Seg_11766" s="T929">v:pred 0.3.h:S</ta>
            <ta e="T932" id="Seg_11767" s="T931">np:O</ta>
            <ta e="T934" id="Seg_11768" s="T933">v:pred 0.3.h:S</ta>
            <ta e="T935" id="Seg_11769" s="T934">np:O</ta>
            <ta e="T937" id="Seg_11770" s="T936">v:pred 0.3.h:S</ta>
            <ta e="T940" id="Seg_11771" s="T939">np:S</ta>
            <ta e="T941" id="Seg_11772" s="T940">adj:pred</ta>
            <ta e="T959" id="Seg_11773" s="T958">np:S</ta>
            <ta e="T962" id="Seg_11774" s="T961">np:S</ta>
            <ta e="T965" id="Seg_11775" s="T964">np:S</ta>
            <ta e="T966" id="Seg_11776" s="T965">v:pred</ta>
            <ta e="T970" id="Seg_11777" s="T969">pro.h:S</ta>
            <ta e="T971" id="Seg_11778" s="T970">pro.h:O</ta>
            <ta e="T972" id="Seg_11779" s="T971">v:pred</ta>
            <ta e="T975" id="Seg_11780" s="T974">v:pred 0.2.h:S</ta>
            <ta e="T977" id="Seg_11781" s="T976">v:pred 0.2.h:S</ta>
            <ta e="T980" id="Seg_11782" s="T979">pro.h:O</ta>
            <ta e="T981" id="Seg_11783" s="T980">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PKZ">
            <ta e="T15" id="Seg_11784" s="T14">np:G</ta>
            <ta e="T16" id="Seg_11785" s="T15">adv:Time</ta>
            <ta e="T17" id="Seg_11786" s="T16">0.1.h:A </ta>
            <ta e="T20" id="Seg_11787" s="T19">np:Ins</ta>
            <ta e="T21" id="Seg_11788" s="T20">pro:L</ta>
            <ta e="T22" id="Seg_11789" s="T21">np:Th</ta>
            <ta e="T24" id="Seg_11790" s="T23">np:Th</ta>
            <ta e="T26" id="Seg_11791" s="T25">np:Th</ta>
            <ta e="T28" id="Seg_11792" s="T27">np:Th</ta>
            <ta e="T32" id="Seg_11793" s="T31">np:Th</ta>
            <ta e="T35" id="Seg_11794" s="T34">np:Th</ta>
            <ta e="T49" id="Seg_11795" s="T46">np:Th</ta>
            <ta e="T52" id="Seg_11796" s="T51">np:Th</ta>
            <ta e="T55" id="Seg_11797" s="T54">np:Th</ta>
            <ta e="T57" id="Seg_11798" s="T56">np:Th</ta>
            <ta e="T61" id="Seg_11799" s="T60">np:Th</ta>
            <ta e="T63" id="Seg_11800" s="T62">np:Th</ta>
            <ta e="T66" id="Seg_11801" s="T65">np:Th</ta>
            <ta e="T70" id="Seg_11802" s="T69">np:Th</ta>
            <ta e="T72" id="Seg_11803" s="T71">np:Th</ta>
            <ta e="T92" id="Seg_11804" s="T91">np.h:A</ta>
            <ta e="T95" id="Seg_11805" s="T94">0.2.h:Th</ta>
            <ta e="T96" id="Seg_11806" s="T95">0.1.h:A</ta>
            <ta e="T100" id="Seg_11807" s="T99">0.2.h:Th</ta>
            <ta e="T101" id="Seg_11808" s="T100">0.2.h:A</ta>
            <ta e="T102" id="Seg_11809" s="T101">adv:L</ta>
            <ta e="T103" id="Seg_11810" s="T102">np:Th</ta>
            <ta e="T105" id="Seg_11811" s="T104">np:Th</ta>
            <ta e="T107" id="Seg_11812" s="T106">np:Th</ta>
            <ta e="T109" id="Seg_11813" s="T108">np:Th</ta>
            <ta e="T111" id="Seg_11814" s="T110">np:Th</ta>
            <ta e="T112" id="Seg_11815" s="T111">np:Th</ta>
            <ta e="T113" id="Seg_11816" s="T112">np:Th</ta>
            <ta e="T115" id="Seg_11817" s="T114">np:P</ta>
            <ta e="T116" id="Seg_11818" s="T115">0.2.h:A</ta>
            <ta e="T118" id="Seg_11819" s="T117">0.2.h:A</ta>
            <ta e="T121" id="Seg_11820" s="T120">0.1.h:A</ta>
            <ta e="T122" id="Seg_11821" s="T121">pro.h:P</ta>
            <ta e="T123" id="Seg_11822" s="T122">0.2.h:E</ta>
            <ta e="T125" id="Seg_11823" s="T124">0.2.h:E</ta>
            <ta e="T127" id="Seg_11824" s="T126">0.2.h:A</ta>
            <ta e="T129" id="Seg_11825" s="T128">0.2.h:Th</ta>
            <ta e="T131" id="Seg_11826" s="T130">0.2.h:A</ta>
            <ta e="T132" id="Seg_11827" s="T131">pro.h:A</ta>
            <ta e="T138" id="Seg_11828" s="T137">pro.h:A</ta>
            <ta e="T139" id="Seg_11829" s="T138">adv:Time</ta>
            <ta e="T141" id="Seg_11830" s="T140">np:G</ta>
            <ta e="T143" id="Seg_11831" s="T142">pro.h:Poss</ta>
            <ta e="T144" id="Seg_11832" s="T143">np:L</ta>
            <ta e="T145" id="Seg_11833" s="T144">0.2.h:Th</ta>
            <ta e="T146" id="Seg_11834" s="T145">0.2.h:A</ta>
            <ta e="T148" id="Seg_11835" s="T147">pro.h:A</ta>
            <ta e="T149" id="Seg_11836" s="T148">pro:Th</ta>
            <ta e="T150" id="Seg_11837" s="T149">pro.h:R</ta>
            <ta e="T152" id="Seg_11838" s="T151">adv:Time</ta>
            <ta e="T153" id="Seg_11839" s="T152">0.1.h:A</ta>
            <ta e="T154" id="Seg_11840" s="T153">pro.h:R</ta>
            <ta e="T155" id="Seg_11841" s="T154">adv:Time</ta>
            <ta e="T158" id="Seg_11842" s="T157">adv:Time</ta>
            <ta e="T160" id="Seg_11843" s="T159">pro.h:Poss</ta>
            <ta e="T161" id="Seg_11844" s="T160">np.h:A</ta>
            <ta e="T162" id="Seg_11845" s="T161">pro.h:A</ta>
            <ta e="T164" id="Seg_11846" s="T163">0.2.h:Th</ta>
            <ta e="T167" id="Seg_11847" s="T166">0.1.h:A</ta>
            <ta e="T172" id="Seg_11848" s="T171">pro.h:A</ta>
            <ta e="T174" id="Seg_11849" s="T173">pro.h:A</ta>
            <ta e="T175" id="Seg_11850" s="T174">pro.h:A</ta>
            <ta e="T181" id="Seg_11851" s="T180">pro.h:A</ta>
            <ta e="T182" id="Seg_11852" s="T181">np:L</ta>
            <ta e="T184" id="Seg_11853" s="T183">adv:L</ta>
            <ta e="T185" id="Seg_11854" s="T184">adv:Time</ta>
            <ta e="T188" id="Seg_11855" s="T187">0.3.h:Th</ta>
            <ta e="T190" id="Seg_11856" s="T189">pro.h:A</ta>
            <ta e="T192" id="Seg_11857" s="T191">adv:Time</ta>
            <ta e="T193" id="Seg_11858" s="T192">0.1.h:A</ta>
            <ta e="T194" id="Seg_11859" s="T193">np:L</ta>
            <ta e="T195" id="Seg_11860" s="T194">0.1.h:A</ta>
            <ta e="T196" id="Seg_11861" s="T195">0.1.h:A</ta>
            <ta e="T197" id="Seg_11862" s="T196">pro.h:R</ta>
            <ta e="T198" id="Seg_11863" s="T197">0.1.h:A</ta>
            <ta e="T199" id="Seg_11864" s="T198">0.2.h:A</ta>
            <ta e="T200" id="Seg_11865" s="T199">0.2.h:A</ta>
            <ta e="T201" id="Seg_11866" s="T200">adv:L</ta>
            <ta e="T206" id="Seg_11867" s="T205">np:Th</ta>
            <ta e="T208" id="Seg_11868" s="T207">np:Th</ta>
            <ta e="T210" id="Seg_11869" s="T209">np:Th</ta>
            <ta e="T211" id="Seg_11870" s="T210">np:Th</ta>
            <ta e="T212" id="Seg_11871" s="T211">0.2.h:A</ta>
            <ta e="T213" id="Seg_11872" s="T212">0.2.h:Th</ta>
            <ta e="T214" id="Seg_11873" s="T213">pro.h:A</ta>
            <ta e="T217" id="Seg_11874" s="T216">pro.h:A</ta>
            <ta e="T219" id="Seg_11875" s="T218">0.3.h:A</ta>
            <ta e="T220" id="Seg_11876" s="T219">adv:Time</ta>
            <ta e="T221" id="Seg_11877" s="T220">0.3.h:Th</ta>
            <ta e="T224" id="Seg_11878" s="T223">adv:Time</ta>
            <ta e="T226" id="Seg_11879" s="T225">0.3.h:A</ta>
            <ta e="T228" id="Seg_11880" s="T227">pro.h:A</ta>
            <ta e="T233" id="Seg_11881" s="T232">adv:Time</ta>
            <ta e="T239" id="Seg_11882" s="T238">np:Ins</ta>
            <ta e="T241" id="Seg_11883" s="T240">np.h:Th</ta>
            <ta e="T242" id="Seg_11884" s="T241">0.3.h:A</ta>
            <ta e="T244" id="Seg_11885" s="T243">np.h:Th</ta>
            <ta e="T245" id="Seg_11886" s="T244">pro.h:A</ta>
            <ta e="T250" id="Seg_11887" s="T249">0.1.h:A</ta>
            <ta e="T257" id="Seg_11888" s="T256">pro.h:A</ta>
            <ta e="T258" id="Seg_11889" s="T257">adv:Time</ta>
            <ta e="T259" id="Seg_11890" s="T258">np:G</ta>
            <ta e="T261" id="Seg_11891" s="T260">0.1.h:A</ta>
            <ta e="T263" id="Seg_11892" s="T262">np:Th</ta>
            <ta e="T267" id="Seg_11893" s="T266">np:Th</ta>
            <ta e="T268" id="Seg_11894" s="T267">0.1.h:A</ta>
            <ta e="T269" id="Seg_11895" s="T268">np:Th</ta>
            <ta e="T270" id="Seg_11896" s="T269">0.1.h:A</ta>
            <ta e="T271" id="Seg_11897" s="T270">np:So</ta>
            <ta e="T272" id="Seg_11898" s="T271">0.1.h:A</ta>
            <ta e="T273" id="Seg_11899" s="T272">np:A</ta>
            <ta e="T275" id="Seg_11900" s="T274">pro.h:A</ta>
            <ta e="T278" id="Seg_11901" s="T277">0.1.h:E</ta>
            <ta e="T279" id="Seg_11902" s="T278">0.1.h:E</ta>
            <ta e="T282" id="Seg_11903" s="T281">0.1.h:E</ta>
            <ta e="T283" id="Seg_11904" s="T282">adv:Time</ta>
            <ta e="T285" id="Seg_11905" s="T284">pro.h:A</ta>
            <ta e="T288" id="Seg_11906" s="T287">np:Th</ta>
            <ta e="T289" id="Seg_11907" s="T288">0.1.h:A</ta>
            <ta e="T290" id="Seg_11908" s="T289">adv:Time</ta>
            <ta e="T291" id="Seg_11909" s="T290">0.1.h:A</ta>
            <ta e="T292" id="Seg_11910" s="T291">0.1.h:A</ta>
            <ta e="T294" id="Seg_11911" s="T293">adv:Time</ta>
            <ta e="T295" id="Seg_11912" s="T294">0.3:P</ta>
            <ta e="T297" id="Seg_11913" s="T296">adv:Time</ta>
            <ta e="T301" id="Seg_11914" s="T300">np.h:Th</ta>
            <ta e="T307" id="Seg_11915" s="T306">np:P</ta>
            <ta e="T308" id="Seg_11916" s="T307">0.3.h:A</ta>
            <ta e="T310" id="Seg_11917" s="T309">np:P</ta>
            <ta e="T311" id="Seg_11918" s="T310">0.3.h:A</ta>
            <ta e="T312" id="Seg_11919" s="T311">0.3.h:E</ta>
            <ta e="T316" id="Seg_11920" s="T315">0.3.h:A</ta>
            <ta e="T317" id="Seg_11921" s="T316">pro.h:A</ta>
            <ta e="T318" id="Seg_11922" s="T317">pro.h:Th</ta>
            <ta e="T320" id="Seg_11923" s="T319">0.2.h:Th</ta>
            <ta e="T321" id="Seg_11924" s="T320">0.1.h:A</ta>
            <ta e="T322" id="Seg_11925" s="T321">pro.h:A</ta>
            <ta e="T324" id="Seg_11926" s="T323">0.2.h:A</ta>
            <ta e="T326" id="Seg_11927" s="T325">0.2.h:Th</ta>
            <ta e="T327" id="Seg_11928" s="T326">0.2.h:Th</ta>
            <ta e="T329" id="Seg_11929" s="T328">np:Th</ta>
            <ta e="T331" id="Seg_11930" s="T330">np:Th</ta>
            <ta e="T333" id="Seg_11931" s="T332">0.2.h:A</ta>
            <ta e="T335" id="Seg_11932" s="T334">np:G</ta>
            <ta e="T336" id="Seg_11933" s="T335">np:Th</ta>
            <ta e="T340" id="Seg_11934" s="T339">np:P</ta>
            <ta e="T342" id="Seg_11935" s="T341">np:P</ta>
            <ta e="T344" id="Seg_11936" s="T343">np:P</ta>
            <ta e="T346" id="Seg_11937" s="T345">np:Th</ta>
            <ta e="T348" id="Seg_11938" s="T347">0.2.h:A</ta>
            <ta e="T349" id="Seg_11939" s="T348">np:P</ta>
            <ta e="T351" id="Seg_11940" s="T350">np:P</ta>
            <ta e="T354" id="Seg_11941" s="T353">np:P</ta>
            <ta e="T356" id="Seg_11942" s="T355">np:P</ta>
            <ta e="T360" id="Seg_11943" s="T359">pro.h:A</ta>
            <ta e="T361" id="Seg_11944" s="T360">np:P</ta>
            <ta e="T363" id="Seg_11945" s="T362">np:P</ta>
            <ta e="T368" id="Seg_11946" s="T367">np:P</ta>
            <ta e="T374" id="Seg_11947" s="T373">np:P</ta>
            <ta e="T377" id="Seg_11948" s="T376">pro.h:Poss</ta>
            <ta e="T378" id="Seg_11949" s="T377">np.h:Poss</ta>
            <ta e="T379" id="Seg_11950" s="T378">np:Th</ta>
            <ta e="T383" id="Seg_11951" s="T382">pro:Th</ta>
            <ta e="T385" id="Seg_11952" s="T384">pro.h:A</ta>
            <ta e="T388" id="Seg_11953" s="T387">np:G</ta>
            <ta e="T391" id="Seg_11954" s="T390">np:Th</ta>
            <ta e="T394" id="Seg_11955" s="T393">np:Th</ta>
            <ta e="T399" id="Seg_11956" s="T398">np:B</ta>
            <ta e="T400" id="Seg_11957" s="T399">np:Th</ta>
            <ta e="T416" id="Seg_11958" s="T415">pro.h:A</ta>
            <ta e="T417" id="Seg_11959" s="T416">np:G</ta>
            <ta e="T418" id="Seg_11960" s="T417">adv:Time</ta>
            <ta e="T422" id="Seg_11961" s="T421">adv:Time</ta>
            <ta e="T425" id="Seg_11962" s="T424">pro.h:A</ta>
            <ta e="T426" id="Seg_11963" s="T425">np:So</ta>
            <ta e="T429" id="Seg_11964" s="T428">np:Th</ta>
            <ta e="T430" id="Seg_11965" s="T429">0.3.h:A</ta>
            <ta e="T431" id="Seg_11966" s="T430">np:Th</ta>
            <ta e="T432" id="Seg_11967" s="T431">0.3.h:A</ta>
            <ta e="T438" id="Seg_11968" s="T437">np:Th</ta>
            <ta e="T440" id="Seg_11969" s="T439">np:Th</ta>
            <ta e="T443" id="Seg_11970" s="T442">np:Th</ta>
            <ta e="T447" id="Seg_11971" s="T446">np:Ins</ta>
            <ta e="T460" id="Seg_11972" s="T459">np:Th</ta>
            <ta e="T462" id="Seg_11973" s="T461">adv:Time</ta>
            <ta e="T463" id="Seg_11974" s="T462">0.3:P</ta>
            <ta e="T466" id="Seg_11975" s="T465">adv:Time</ta>
            <ta e="T469" id="Seg_11976" s="T468">adv:Time</ta>
            <ta e="T476" id="Seg_11977" s="T475">adv:Time</ta>
            <ta e="T481" id="Seg_11978" s="T480">adv:Time</ta>
            <ta e="T484" id="Seg_11979" s="T483">adv:Time</ta>
            <ta e="T489" id="Seg_11980" s="T488">adv:Time</ta>
            <ta e="T491" id="Seg_11981" s="T490">n:Time</ta>
            <ta e="T494" id="Seg_11982" s="T493">np.h:Th</ta>
            <ta e="T499" id="Seg_11983" s="T498">np:P</ta>
            <ta e="T500" id="Seg_11984" s="T499">0.3.h:A</ta>
            <ta e="T501" id="Seg_11985" s="T500">0.3.h:A</ta>
            <ta e="T503" id="Seg_11986" s="T502">0.3.h:A</ta>
            <ta e="T504" id="Seg_11987" s="T503">np:Th</ta>
            <ta e="T505" id="Seg_11988" s="T504">0.3.h:A</ta>
            <ta e="T508" id="Seg_11989" s="T507">np:P</ta>
            <ta e="T512" id="Seg_11990" s="T511">0.3.h:A</ta>
            <ta e="T520" id="Seg_11991" s="T519">0.2.h:A</ta>
            <ta e="T522" id="Seg_11992" s="T521">0.3.h:E</ta>
            <ta e="T523" id="Seg_11993" s="T522">adv:Time</ta>
            <ta e="T524" id="Seg_11994" s="T523">0.1.h:A</ta>
            <ta e="T525" id="Seg_11995" s="T524">0.2.h:A</ta>
            <ta e="T527" id="Seg_11996" s="T526">0.2.h:Th</ta>
            <ta e="T529" id="Seg_11997" s="T528">np.h:A</ta>
            <ta e="T531" id="Seg_11998" s="T530">np:P</ta>
            <ta e="T533" id="Seg_11999" s="T532">np:Com</ta>
            <ta e="T534" id="Seg_12000" s="T533">np:Th</ta>
            <ta e="T535" id="Seg_12001" s="T534">0.1.h:A</ta>
            <ta e="T536" id="Seg_12002" s="T535">0.2.h:A</ta>
            <ta e="T538" id="Seg_12003" s="T537">np:Th</ta>
            <ta e="T541" id="Seg_12004" s="T540">0.2.h:A</ta>
            <ta e="T544" id="Seg_12005" s="T543">pro.h:E</ta>
            <ta e="T547" id="Seg_12006" s="T546">0.2.h:A</ta>
            <ta e="T550" id="Seg_12007" s="T549">np:Th</ta>
            <ta e="T551" id="Seg_12008" s="T550">0.1.h:A</ta>
            <ta e="T552" id="Seg_12009" s="T551">pro:G</ta>
            <ta e="T553" id="Seg_12010" s="T552">0.1.h:A</ta>
            <ta e="T554" id="Seg_12011" s="T553">np:Th</ta>
            <ta e="T555" id="Seg_12012" s="T554">adv:Time</ta>
            <ta e="T568" id="Seg_12013" s="T567">0.2.h:E</ta>
            <ta e="T569" id="Seg_12014" s="T568">0.2.h:A</ta>
            <ta e="T574" id="Seg_12015" s="T573">0.2.h:A</ta>
            <ta e="T578" id="Seg_12016" s="T577">0.2.h:A</ta>
            <ta e="T583" id="Seg_12017" s="T582">0.2.h:A</ta>
            <ta e="T587" id="Seg_12018" s="T586">np:Th</ta>
            <ta e="T589" id="Seg_12019" s="T588">0.2.h:A</ta>
            <ta e="T590" id="Seg_12020" s="T589">np:Th</ta>
            <ta e="T593" id="Seg_12021" s="T592">0.2.h:A</ta>
            <ta e="T595" id="Seg_12022" s="T594">0.2.h:A</ta>
            <ta e="T597" id="Seg_12023" s="T596">0.2.h:A</ta>
            <ta e="T620" id="Seg_12024" s="T619">np:Th</ta>
            <ta e="T621" id="Seg_12025" s="T620">0.2.h:A</ta>
            <ta e="T623" id="Seg_12026" s="T622">0.2.h:A</ta>
            <ta e="T624" id="Seg_12027" s="T623">pro.h:A</ta>
            <ta e="T634" id="Seg_12028" s="T633">np:P</ta>
            <ta e="T635" id="Seg_12029" s="T634">0.1.h:E</ta>
            <ta e="T637" id="Seg_12030" s="T636">np:Th</ta>
            <ta e="T639" id="Seg_12031" s="T638">0.2.h:A</ta>
            <ta e="T640" id="Seg_12032" s="T639">pro:Th</ta>
            <ta e="T641" id="Seg_12033" s="T640">np:B</ta>
            <ta e="T649" id="Seg_12034" s="T648">np:Th</ta>
            <ta e="T650" id="Seg_12035" s="T649">0.2.h:A</ta>
            <ta e="T655" id="Seg_12036" s="T654">np:Th</ta>
            <ta e="T656" id="Seg_12037" s="T655">np:Th</ta>
            <ta e="T659" id="Seg_12038" s="T658">np:Th</ta>
            <ta e="T662" id="Seg_12039" s="T661">np:Ins</ta>
            <ta e="T665" id="Seg_12040" s="T664">pro.h:E</ta>
            <ta e="T670" id="Seg_12041" s="T669">0.1.h:E</ta>
            <ta e="T671" id="Seg_12042" s="T670">np:E 0.1.h:Poss</ta>
            <ta e="T673" id="Seg_12043" s="T672">np:E 0.1.h:Poss</ta>
            <ta e="T675" id="Seg_12044" s="T674">np:E 0.1.h:Poss</ta>
            <ta e="T678" id="Seg_12045" s="T677">adv:Time</ta>
            <ta e="T683" id="Seg_12046" s="T682">np:Th</ta>
            <ta e="T687" id="Seg_12047" s="T686">pro:Th</ta>
            <ta e="T692" id="Seg_12048" s="T691">np.h:A</ta>
            <ta e="T695" id="Seg_12049" s="T694">np:P</ta>
            <ta e="T696" id="Seg_12050" s="T695">0.3.h:A</ta>
            <ta e="T697" id="Seg_12051" s="T696">np:A</ta>
            <ta e="T702" id="Seg_12052" s="T701">np:P</ta>
            <ta e="T704" id="Seg_12053" s="T703">np:Th</ta>
            <ta e="T708" id="Seg_12054" s="T707">np:Th</ta>
            <ta e="T710" id="Seg_12055" s="T709">np:Th</ta>
            <ta e="T713" id="Seg_12056" s="T712">np:Th</ta>
            <ta e="T715" id="Seg_12057" s="T714">np:Th</ta>
            <ta e="T717" id="Seg_12058" s="T716">np:Th</ta>
            <ta e="T719" id="Seg_12059" s="T718">np:Th</ta>
            <ta e="T722" id="Seg_12060" s="T721">np:B</ta>
            <ta e="T723" id="Seg_12061" s="T722">np:P</ta>
            <ta e="T724" id="Seg_12062" s="T723">adv:Time</ta>
            <ta e="T725" id="Seg_12063" s="T724">0.1.h:A</ta>
            <ta e="T726" id="Seg_12064" s="T725">np:P</ta>
            <ta e="T727" id="Seg_12065" s="T726">0.1.h:A</ta>
            <ta e="T728" id="Seg_12066" s="T727">np:P</ta>
            <ta e="T729" id="Seg_12067" s="T728">0.1.h:A</ta>
            <ta e="T730" id="Seg_12068" s="T729">np:P</ta>
            <ta e="T731" id="Seg_12069" s="T730">0.1.h:A</ta>
            <ta e="T732" id="Seg_12070" s="T731">np:P</ta>
            <ta e="T733" id="Seg_12071" s="T732">0.1.h:A</ta>
            <ta e="T734" id="Seg_12072" s="T733">np:P</ta>
            <ta e="T735" id="Seg_12073" s="T734">0.1.h:A</ta>
            <ta e="T736" id="Seg_12074" s="T735">np:Ins</ta>
            <ta e="T737" id="Seg_12075" s="T736">np:A</ta>
            <ta e="T739" id="Seg_12076" s="T738">np:P</ta>
            <ta e="T741" id="Seg_12077" s="T740">0.3:A</ta>
            <ta e="T742" id="Seg_12078" s="T741">adv:L</ta>
            <ta e="T744" id="Seg_12079" s="T743">np.h:A</ta>
            <ta e="T747" id="Seg_12080" s="T746">np:G</ta>
            <ta e="T750" id="Seg_12081" s="T749">np.h:P 0.1.h:Poss</ta>
            <ta e="T755" id="Seg_12082" s="T754">np.h:E 0.1.h:Poss</ta>
            <ta e="T759" id="Seg_12083" s="T758">pro.h:Th</ta>
            <ta e="T771" id="Seg_12084" s="T770">0.2.h:A</ta>
            <ta e="T772" id="Seg_12085" s="T771">np.h:Th 0.1.h:Poss</ta>
            <ta e="T775" id="Seg_12086" s="T774">0.3.h:E</ta>
            <ta e="T776" id="Seg_12087" s="T775">0.2.h:Th</ta>
            <ta e="T777" id="Seg_12088" s="T776">adv:Time</ta>
            <ta e="T779" id="Seg_12089" s="T778">0.1.h:A</ta>
            <ta e="T780" id="Seg_12090" s="T779">np:Th</ta>
            <ta e="T781" id="Seg_12091" s="T780">0.1.h:A</ta>
            <ta e="T782" id="Seg_12092" s="T781">adv:Time</ta>
            <ta e="T783" id="Seg_12093" s="T782">0.2.h:A</ta>
            <ta e="T784" id="Seg_12094" s="T783">0.2.h:A</ta>
            <ta e="T787" id="Seg_12095" s="T786">adv:Time</ta>
            <ta e="T788" id="Seg_12096" s="T787">0.2.h:A</ta>
            <ta e="T789" id="Seg_12097" s="T788">np:G</ta>
            <ta e="T790" id="Seg_12098" s="T789">adv:Time</ta>
            <ta e="T791" id="Seg_12099" s="T790">np:P</ta>
            <ta e="T792" id="Seg_12100" s="T791">0.1.h:A</ta>
            <ta e="T794" id="Seg_12101" s="T793">adv:L</ta>
            <ta e="T795" id="Seg_12102" s="T794">np:Th</ta>
            <ta e="T797" id="Seg_12103" s="T796">np:Th</ta>
            <ta e="T800" id="Seg_12104" s="T799">np:Th</ta>
            <ta e="T802" id="Seg_12105" s="T801">adv:Time</ta>
            <ta e="T803" id="Seg_12106" s="T802">0.1.h:A</ta>
            <ta e="T810" id="Seg_12107" s="T809">np:Ins</ta>
            <ta e="T812" id="Seg_12108" s="T811">np.h:A</ta>
            <ta e="T822" id="Seg_12109" s="T821">np.h:A</ta>
            <ta e="T828" id="Seg_12110" s="T827">np.h:A 0.3.h:Poss</ta>
            <ta e="T831" id="Seg_12111" s="T830">pro.h:A</ta>
            <ta e="T832" id="Seg_12112" s="T831">np:Th</ta>
            <ta e="T835" id="Seg_12113" s="T834">pro.h:Poss</ta>
            <ta e="T836" id="Seg_12114" s="T835">np.h:A</ta>
            <ta e="T837" id="Seg_12115" s="T836">pro.h:B</ta>
            <ta e="T839" id="Seg_12116" s="T838">pro.h:A</ta>
            <ta e="T840" id="Seg_12117" s="T839">np:Th</ta>
            <ta e="T844" id="Seg_12118" s="T843">np:P</ta>
            <ta e="T845" id="Seg_12119" s="T844">0.3.h:A</ta>
            <ta e="T846" id="Seg_12120" s="T845">np:Th</ta>
            <ta e="T851" id="Seg_12121" s="T850">pro:Th</ta>
            <ta e="T856" id="Seg_12122" s="T855">0.2.h:A</ta>
            <ta e="T858" id="Seg_12123" s="T857">0.2.h:A</ta>
            <ta e="T859" id="Seg_12124" s="T858">0.2.h:A</ta>
            <ta e="T860" id="Seg_12125" s="T859">pro:Th</ta>
            <ta e="T865" id="Seg_12126" s="T864">np:E 0.2.h:Poss</ta>
            <ta e="T868" id="Seg_12127" s="T867">pro:Th</ta>
            <ta e="T870" id="Seg_12128" s="T869">0.2.h:A</ta>
            <ta e="T871" id="Seg_12129" s="T870">0.2.h:A</ta>
            <ta e="T875" id="Seg_12130" s="T874">pro:G</ta>
            <ta e="T876" id="Seg_12131" s="T875">0.2.h:A</ta>
            <ta e="T877" id="Seg_12132" s="T876">pro:L</ta>
            <ta e="T878" id="Seg_12133" s="T877">0.2.h:Th</ta>
            <ta e="T879" id="Seg_12134" s="T878">0.2.h:A</ta>
            <ta e="T880" id="Seg_12135" s="T879">adv:L</ta>
            <ta e="T881" id="Seg_12136" s="T880">pro:Th</ta>
            <ta e="T883" id="Seg_12137" s="T882">pro.h:R</ta>
            <ta e="T886" id="Seg_12138" s="T885">np.h:E</ta>
            <ta e="T887" id="Seg_12139" s="T886">np.h:Com</ta>
            <ta e="T889" id="Seg_12140" s="T888">pro.h:Poss</ta>
            <ta e="T890" id="Seg_12141" s="T889">np.h:Th</ta>
            <ta e="T895" id="Seg_12142" s="T894">np:Th</ta>
            <ta e="T897" id="Seg_12143" s="T896">np:Th</ta>
            <ta e="T900" id="Seg_12144" s="T899">np.h:A</ta>
            <ta e="T901" id="Seg_12145" s="T900">np:Th</ta>
            <ta e="T903" id="Seg_12146" s="T902">np.h:R</ta>
            <ta e="T904" id="Seg_12147" s="T903">0.3.h:A</ta>
            <ta e="T905" id="Seg_12148" s="T904">np:Th</ta>
            <ta e="T906" id="Seg_12149" s="T905">0.3.h:A</ta>
            <ta e="T907" id="Seg_12150" s="T906">pro.h:A</ta>
            <ta e="T912" id="Seg_12151" s="T911">np.h:A</ta>
            <ta e="T914" id="Seg_12152" s="T913">np.h:A</ta>
            <ta e="T916" id="Seg_12153" s="T915">np.h:A</ta>
            <ta e="T917" id="Seg_12154" s="T916">pro.h:R</ta>
            <ta e="T919" id="Seg_12155" s="T918">pro:Th</ta>
            <ta e="T921" id="Seg_12156" s="T920">np:Th</ta>
            <ta e="T922" id="Seg_12157" s="T921">0.3.h:A</ta>
            <ta e="T926" id="Seg_12158" s="T925">np:Th</ta>
            <ta e="T927" id="Seg_12159" s="T926">0.2.h:A</ta>
            <ta e="T929" id="Seg_12160" s="T928">np:Th</ta>
            <ta e="T930" id="Seg_12161" s="T929">0.3.h:A</ta>
            <ta e="T931" id="Seg_12162" s="T930">pro.h:R</ta>
            <ta e="T932" id="Seg_12163" s="T931">np:Th</ta>
            <ta e="T934" id="Seg_12164" s="T933">0.3.h:A</ta>
            <ta e="T935" id="Seg_12165" s="T934">np:Th</ta>
            <ta e="T937" id="Seg_12166" s="T936">0.3.h:A</ta>
            <ta e="T938" id="Seg_12167" s="T937">pro.h:R</ta>
            <ta e="T939" id="Seg_12168" s="T938">np.h:Poss</ta>
            <ta e="T940" id="Seg_12169" s="T939">np:Th</ta>
            <ta e="T959" id="Seg_12170" s="T958">np:Poss</ta>
            <ta e="T962" id="Seg_12171" s="T961">np:Th</ta>
            <ta e="T965" id="Seg_12172" s="T964">np:Th</ta>
            <ta e="T970" id="Seg_12173" s="T969">pro.h:A</ta>
            <ta e="T971" id="Seg_12174" s="T970">pro.h:E</ta>
            <ta e="T974" id="Seg_12175" s="T973">pro:Ins</ta>
            <ta e="T975" id="Seg_12176" s="T974">0.2.h:A</ta>
            <ta e="T976" id="Seg_12177" s="T975">np:Ins</ta>
            <ta e="T977" id="Seg_12178" s="T976">0.2.h:A</ta>
            <ta e="T980" id="Seg_12179" s="T979">pro.h:E</ta>
            <ta e="T981" id="Seg_12180" s="T980">0.3.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST-PKZ" />
         <annotation name="BOR" tierref="BOR-PKZ">
            <ta e="T23" id="Seg_12181" s="T22">RUS:mod</ta>
            <ta e="T26" id="Seg_12182" s="T25">TURK:cult</ta>
            <ta e="T29" id="Seg_12183" s="T28">RUS:mod</ta>
            <ta e="T31" id="Seg_12184" s="T30">RUS:mod</ta>
            <ta e="T34" id="Seg_12185" s="T33">RUS:mod</ta>
            <ta e="T51" id="Seg_12186" s="T50">RUS:mod</ta>
            <ta e="T54" id="Seg_12187" s="T53">RUS:mod</ta>
            <ta e="T58" id="Seg_12188" s="T57">RUS:mod</ta>
            <ta e="T60" id="Seg_12189" s="T59">RUS:mod</ta>
            <ta e="T65" id="Seg_12190" s="T64">RUS:mod</ta>
            <ta e="T66" id="Seg_12191" s="T65">TURK:cult</ta>
            <ta e="T68" id="Seg_12192" s="T67">RUS:mod</ta>
            <ta e="T69" id="Seg_12193" s="T68">RUS:mod</ta>
            <ta e="T74" id="Seg_12194" s="T73">RUS:mod</ta>
            <ta e="T94" id="Seg_12195" s="T93">%TURK:core</ta>
            <ta e="T96" id="Seg_12196" s="T95">%TURK:core</ta>
            <ta e="T98" id="Seg_12197" s="T97">RUS:mod</ta>
            <ta e="T99" id="Seg_12198" s="T98">RUS:mod</ta>
            <ta e="T109" id="Seg_12199" s="T108">TURK:cult</ta>
            <ta e="T115" id="Seg_12200" s="T114">TURK:disc</ta>
            <ta e="T120" id="Seg_12201" s="T119">RUS:gram</ta>
            <ta e="T130" id="Seg_12202" s="T129">TURK:disc</ta>
            <ta e="T142" id="Seg_12203" s="T141">RUS:gram</ta>
            <ta e="T149" id="Seg_12204" s="T148">RUS:gram(INDEF)</ta>
            <ta e="T161" id="Seg_12205" s="T160">TURK:core</ta>
            <ta e="T169" id="Seg_12206" s="T168">RUS:mod</ta>
            <ta e="T171" id="Seg_12207" s="T170">RUS:mod</ta>
            <ta e="T182" id="Seg_12208" s="T181">TAT:cult</ta>
            <ta e="T206" id="Seg_12209" s="T205">TURK:cult</ta>
            <ta e="T227" id="Seg_12210" s="T226">RUS:gram</ta>
            <ta e="T273" id="Seg_12211" s="T272">TURK:cult</ta>
            <ta e="T280" id="Seg_12212" s="T279">RUS:gram</ta>
            <ta e="T281" id="Seg_12213" s="T280">RUS:gram</ta>
            <ta e="T293" id="Seg_12214" s="T292">TURK:disc</ta>
            <ta e="T296" id="Seg_12215" s="T295">TURK:disc</ta>
            <ta e="T299" id="Seg_12216" s="T298">RUS:gram</ta>
            <ta e="T300" id="Seg_12217" s="T299">RUS:mod</ta>
            <ta e="T301" id="Seg_12218" s="T300">TURK:cult</ta>
            <ta e="T302" id="Seg_12219" s="T301">TURK:disc</ta>
            <ta e="T304" id="Seg_12220" s="T303">TURK:disc</ta>
            <ta e="T307" id="Seg_12221" s="T306">TURK:cult</ta>
            <ta e="T313" id="Seg_12222" s="T312">TURK:disc</ta>
            <ta e="T351" id="Seg_12223" s="T350">TURK:cult</ta>
            <ta e="T354" id="Seg_12224" s="T353">TURK:cult</ta>
            <ta e="T359" id="Seg_12225" s="T358">RUS:mod</ta>
            <ta e="T365" id="Seg_12226" s="T364">RUS:mod</ta>
            <ta e="T373" id="Seg_12227" s="T372">RUS:mod</ta>
            <ta e="T376" id="Seg_12228" s="T375">RUS:gram</ta>
            <ta e="T379" id="Seg_12229" s="T378">TURK:disc</ta>
            <ta e="T383" id="Seg_12230" s="T382">TURK:gram(INDEF)</ta>
            <ta e="T388" id="Seg_12231" s="T387">TAT:cult</ta>
            <ta e="T389" id="Seg_12232" s="T388">RUS:gram</ta>
            <ta e="T390" id="Seg_12233" s="T389">RUS:mod</ta>
            <ta e="T393" id="Seg_12234" s="T392">RUS:mod</ta>
            <ta e="T396" id="Seg_12235" s="T395">RUS:mod</ta>
            <ta e="T399" id="Seg_12236" s="T398">TURK:cult</ta>
            <ta e="T402" id="Seg_12237" s="T401">RUS:mod</ta>
            <ta e="T409" id="Seg_12238" s="T408">RUS:disc</ta>
            <ta e="T428" id="Seg_12239" s="T427">RUS:mod</ta>
            <ta e="T435" id="Seg_12240" s="T434">RUS:mod</ta>
            <ta e="T436" id="Seg_12241" s="T435">RUS:gram</ta>
            <ta e="T440" id="Seg_12242" s="T439">TURK:cult</ta>
            <ta e="T442" id="Seg_12243" s="T441">RUS:gram</ta>
            <ta e="T445" id="Seg_12244" s="T444">RUS:gram</ta>
            <ta e="T448" id="Seg_12245" s="T447">RUS:mod</ta>
            <ta e="T465" id="Seg_12246" s="T464">RUS:mod</ta>
            <ta e="T468" id="Seg_12247" s="T467">RUS:mod</ta>
            <ta e="T478" id="Seg_12248" s="T477">RUS:mod</ta>
            <ta e="T482" id="Seg_12249" s="T481">RUS:mod</ta>
            <ta e="T486" id="Seg_12250" s="T485">RUS:mod</ta>
            <ta e="T498" id="Seg_12251" s="T497">TURK:disc</ta>
            <ta e="T499" id="Seg_12252" s="T498">TURK:cult</ta>
            <ta e="T506" id="Seg_12253" s="T505">TURK:core</ta>
            <ta e="T509" id="Seg_12254" s="T508">TURK:disc</ta>
            <ta e="T513" id="Seg_12255" s="T512">TURK:core</ta>
            <ta e="T521" id="Seg_12256" s="T520">RUS:mod</ta>
            <ta e="T534" id="Seg_12257" s="T533">TURK:cult</ta>
            <ta e="T538" id="Seg_12258" s="T537">TURK:cult</ta>
            <ta e="T545" id="Seg_12259" s="T544">RUS:mod</ta>
            <ta e="T548" id="Seg_12260" s="T547">RUS:gram</ta>
            <ta e="T554" id="Seg_12261" s="T553">TURK:disc</ta>
            <ta e="T556" id="Seg_12262" s="T555">TURK:disc</ta>
            <ta e="T575" id="Seg_12263" s="T574">%TURK:core</ta>
            <ta e="T600" id="Seg_12264" s="T599">TURK:core</ta>
            <ta e="T619" id="Seg_12265" s="T618">RUS:mod</ta>
            <ta e="T620" id="Seg_12266" s="T619">RUS:cult</ta>
            <ta e="T622" id="Seg_12267" s="T621">RUS:mod</ta>
            <ta e="T627" id="Seg_12268" s="T626">RUS:gram</ta>
            <ta e="T628" id="Seg_12269" s="T627">RUS:mod</ta>
            <ta e="T630" id="Seg_12270" s="T629">RUS:gram</ta>
            <ta e="T632" id="Seg_12271" s="T631">TURK:core</ta>
            <ta e="T636" id="Seg_12272" s="T635">TURK:disc</ta>
            <ta e="T640" id="Seg_12273" s="T639">RUS:gram(INDEF)</ta>
            <ta e="T647" id="Seg_12274" s="T645">RUS:mod</ta>
            <ta e="T652" id="Seg_12275" s="T651">RUS:mod</ta>
            <ta e="T654" id="Seg_12276" s="T652">RUS:mod</ta>
            <ta e="T657" id="Seg_12277" s="T656">TURK:disc</ta>
            <ta e="T660" id="Seg_12278" s="T659">TURK:disc</ta>
            <ta e="T668" id="Seg_12279" s="T667">RUS:gram</ta>
            <ta e="T676" id="Seg_12280" s="T675">TURK:disc</ta>
            <ta e="T684" id="Seg_12281" s="T683">TURK:disc</ta>
            <ta e="T687" id="Seg_12282" s="T686">TURK:gram(INDEF)</ta>
            <ta e="T690" id="Seg_12283" s="T689">TURK:core</ta>
            <ta e="T693" id="Seg_12284" s="T692">TURK:disc</ta>
            <ta e="T699" id="Seg_12285" s="T698">TURK:disc</ta>
            <ta e="T700" id="Seg_12286" s="T699">RUS:mod</ta>
            <ta e="T705" id="Seg_12287" s="T704">RUS:mod</ta>
            <ta e="T707" id="Seg_12288" s="T706">RUS:mod</ta>
            <ta e="T710" id="Seg_12289" s="T709">TURK:cult</ta>
            <ta e="T712" id="Seg_12290" s="T711">RUS:mod</ta>
            <ta e="T715" id="Seg_12291" s="T714">TURK:cult</ta>
            <ta e="T720" id="Seg_12292" s="T719">RUS:mod</ta>
            <ta e="T732" id="Seg_12293" s="T731">TURK:cult</ta>
            <ta e="T736" id="Seg_12294" s="T735">TURK:cult</ta>
            <ta e="T737" id="Seg_12295" s="T736">TURK:cult</ta>
            <ta e="T738" id="Seg_12296" s="T737">TURK:disc</ta>
            <ta e="T749" id="Seg_12297" s="T748">RUS:mod</ta>
            <ta e="T752" id="Seg_12298" s="T751">RUS:mod</ta>
            <ta e="T756" id="Seg_12299" s="T755">TURK:disc</ta>
            <ta e="T758" id="Seg_12300" s="T757">RUS:mod</ta>
            <ta e="T761" id="Seg_12301" s="T760">RUS:mod</ta>
            <ta e="T764" id="Seg_12302" s="T763">RUS:mod</ta>
            <ta e="T773" id="Seg_12303" s="T772">TURK:disc</ta>
            <ta e="T774" id="Seg_12304" s="T773">RUS:gram</ta>
            <ta e="T786" id="Seg_12305" s="T785">RUS:gram</ta>
            <ta e="T793" id="Seg_12306" s="T792">RUS:mod</ta>
            <ta e="T795" id="Seg_12307" s="T794">TURK:cult</ta>
            <ta e="T799" id="Seg_12308" s="T798">RUS:gram</ta>
            <ta e="T806" id="Seg_12309" s="T805">RUS:gram</ta>
            <ta e="T818" id="Seg_12310" s="T817">TURK:disc</ta>
            <ta e="T820" id="Seg_12311" s="T819">RUS:gram</ta>
            <ta e="T826" id="Seg_12312" s="T825">RUS:gram</ta>
            <ta e="T843" id="Seg_12313" s="T842">TURK:disc</ta>
            <ta e="T849" id="Seg_12314" s="T848">RUS:gram</ta>
            <ta e="T851" id="Seg_12315" s="T850">TURK:gram(INDEF)</ta>
            <ta e="T858" id="Seg_12316" s="T857">%TURK:core</ta>
            <ta e="T859" id="Seg_12317" s="T858">%TURK:core</ta>
            <ta e="T860" id="Seg_12318" s="T859">RUS:gram(INDEF)</ta>
            <ta e="T862" id="Seg_12319" s="T861">RUS:mod</ta>
            <ta e="T866" id="Seg_12320" s="T865">RUS:mod</ta>
            <ta e="T868" id="Seg_12321" s="T867">TURK:gram(INDEF)</ta>
            <ta e="T870" id="Seg_12322" s="T869">%TURK:core</ta>
            <ta e="T871" id="Seg_12323" s="T870">%TURK:core</ta>
            <ta e="T881" id="Seg_12324" s="T880">RUS:gram(INDEF)</ta>
            <ta e="T893" id="Seg_12325" s="T892">RUS:gram</ta>
            <ta e="T895" id="Seg_12326" s="T894">TAT:cult</ta>
            <ta e="T897" id="Seg_12327" s="T896">TURK:cult</ta>
            <ta e="T901" id="Seg_12328" s="T900">TURK:cult</ta>
            <ta e="T915" id="Seg_12329" s="T914">RUS:gram</ta>
            <ta e="T918" id="Seg_12330" s="T917">RUS:mod</ta>
            <ta e="T919" id="Seg_12331" s="T918">RUS:gram(INDEF)</ta>
            <ta e="T928" id="Seg_12332" s="T927">RUS:mod</ta>
            <ta e="T929" id="Seg_12333" s="T928">RUS:cult</ta>
            <ta e="T933" id="Seg_12334" s="T932">RUS:mod</ta>
            <ta e="T936" id="Seg_12335" s="T935">RUS:mod</ta>
            <ta e="T939" id="Seg_12336" s="T938">RUS:cult</ta>
            <ta e="T940" id="Seg_12337" s="T939">TAT:cult</ta>
            <ta e="T958" id="Seg_12338" s="T957">TURK:core</ta>
            <ta e="T963" id="Seg_12339" s="T962">RUS:gram</ta>
            <ta e="T973" id="Seg_12340" s="T972">RUS:gram</ta>
            <ta e="T979" id="Seg_12341" s="T978">RUS:gram</ta>
            <ta e="T982" id="Seg_12342" s="T981">TURK:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-PKZ" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-PKZ">
            <ta e="T929" id="Seg_12343" s="T928">parad:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS-PKZ">
            <ta e="T13" id="Seg_12344" s="T4">RUS:ext</ta>
            <ta e="T42" id="Seg_12345" s="T38">RUS:ext</ta>
            <ta e="T137" id="Seg_12346" s="T135">RUS:ext</ta>
            <ta e="T223" id="Seg_12347" s="T222">RUS:ext</ta>
            <ta e="T232" id="Seg_12348" s="T229">RUS:ext</ta>
            <ta e="T256" id="Seg_12349" s="T250">RUS:ext</ta>
            <ta e="T358" id="Seg_12350" s="T357">RUS:ext</ta>
            <ta e="T360" id="Seg_12351" s="T358">RUS:calq</ta>
            <ta e="T415" id="Seg_12352" s="T410">RUS:ext</ta>
            <ta e="T421" id="Seg_12353" s="T419">RUS:ext</ta>
            <ta e="T458" id="Seg_12354" s="T456">RUS:ext</ta>
            <ta e="T475" id="Seg_12355" s="T470">RUS:int.ins</ta>
            <ta e="T488" id="Seg_12356" s="T486">RUS:ext</ta>
            <ta e="T502" id="Seg_12357" s="T501">RUS:int.ins</ta>
            <ta e="T543" id="Seg_12358" s="T541">RUS:ext</ta>
            <ta e="T564" id="Seg_12359" s="T557">RUS:ext</ta>
            <ta e="T571" id="Seg_12360" s="T570">RUS:ext</ta>
            <ta e="T618" id="Seg_12361" s="T614">RUS:ext</ta>
            <ta e="T746" id="Seg_12362" s="T745">RUS:int</ta>
            <ta e="T748" id="Seg_12363" s="T747">RUS:ext</ta>
            <ta e="T830" id="Seg_12364" s="T829">RUS:ext</ta>
            <ta e="T872" id="Seg_12365" s="T871">RUS:ext</ta>
            <ta e="T885" id="Seg_12366" s="T883">RUS:ext</ta>
            <ta e="T909" id="Seg_12367" s="T908">RUS:ext</ta>
            <ta e="T969" id="Seg_12368" s="T966">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-PKZ">
            <ta e="T13" id="Seg_12369" s="T4">Я сперва расскажу, погоди, не…</ta>
            <ta e="T20" id="Seg_12370" s="T14">Сегодня я поеду на лошади в тайгу.</ta>
            <ta e="T30" id="Seg_12371" s="T20">Где мешок, надо хлеб положить, соль положить, ложку надо положить.</ta>
            <ta e="T33" id="Seg_12372" s="T30">Надо мясо положить.</ta>
            <ta e="T38" id="Seg_12373" s="T33">Надо хлеб положить.</ta>
            <ta e="T42" id="Seg_12374" s="T38">(…) Говорю, господи прости! </ta>
            <ta e="T51" id="Seg_12375" s="T46">Хлеб положить надо.</ta>
            <ta e="T54" id="Seg_12376" s="T51">Масло положить надо.</ta>
            <ta e="T56" id="Seg_12377" s="T54">Яйца положить.</ta>
            <ta e="T59" id="Seg_12378" s="T56">Ложку надо положить</ta>
            <ta e="T62" id="Seg_12379" s="T59">Надо котелок положить.</ta>
            <ta e="T65" id="Seg_12380" s="T62">Собаку взять надо.</ta>
            <ta e="T68" id="Seg_12381" s="T65">Ружье взять надо.</ta>
            <ta e="T71" id="Seg_12382" s="T68">Надо топор взять.</ta>
            <ta e="T74" id="Seg_12383" s="T71">Нож надо взять.</ta>
            <ta e="T86" id="Seg_12384" s="T85">[?]</ta>
            <ta e="T94" id="Seg_12385" s="T90">Ну, два человека идут разговаривать.</ta>
            <ta e="T95" id="Seg_12386" s="T94">Садись!</ta>
            <ta e="T96" id="Seg_12387" s="T95">Мы будем говорить!</ta>
            <ta e="T101" id="Seg_12388" s="T96">Тебе, наверное, есть хочется, садись, ешь!</ta>
            <ta e="T104" id="Seg_12389" s="T101">Тут хлеб лежит.</ta>
            <ta e="T106" id="Seg_12390" s="T104">Масло стоит.</ta>
            <ta e="T115" id="Seg_12391" s="T106">Ложка есть, соль есть, яйца, масло, рыба, ешь все!</ta>
            <ta e="T117" id="Seg_12392" s="T115">Не шуми!</ta>
            <ta e="T119" id="Seg_12393" s="T117">Не кричи!</ta>
            <ta e="T122" id="Seg_12394" s="T119">Иначе я тебя побью!</ta>
            <ta e="T124" id="Seg_12395" s="T122">Не плачь!</ta>
            <ta e="T126" id="Seg_12396" s="T124">Не сердись!</ta>
            <ta e="T128" id="Seg_12397" s="T126">Не ври!</ta>
            <ta e="T131" id="Seg_12398" s="T128">Ты (сидишь?) и врешь.</ta>
            <ta e="T135" id="Seg_12399" s="T131">Ты очень много врешь!</ta>
            <ta e="T137" id="Seg_12400" s="T135">Все, наверное. </ta>
            <ta e="T141" id="Seg_12401" s="T137">Я сейчас пойду (на то место?).</ta>
            <ta e="T145" id="Seg_12402" s="T141">А ты сиди дома!</ta>
            <ta e="T147" id="Seg_12403" s="T145">Не (шуми?)!</ta>
            <ta e="T151" id="Seg_12404" s="T147">Я тебе что-нибудь принесу.</ta>
            <ta e="T154" id="Seg_12405" s="T151">Тогда я тебе дам.</ta>
            <ta e="T161" id="Seg_12406" s="T154">Вчера пришел мой родственник.</ta>
            <ta e="T165" id="Seg_12407" s="T161">Я сказала: ложись спать!</ta>
            <ta e="T171" id="Seg_12408" s="T165">Потом я сказала: ешь, наверное есть хочешь.</ta>
            <ta e="T180" id="Seg_12409" s="T171">Он сказал: я не (буду?) есть.</ta>
            <ta e="T184" id="Seg_12410" s="T180">Я дома поел там.</ta>
            <ta e="T189" id="Seg_12411" s="T184">Потом он лег спать.</ta>
            <ta e="T191" id="Seg_12412" s="T189">Я легла.</ta>
            <ta e="T193" id="Seg_12413" s="T191">Утром я встала.</ta>
            <ta e="T195" id="Seg_12414" s="T193">Я пошла (на то место?).</ta>
            <ta e="T196" id="Seg_12415" s="T195">Я пришла.</ta>
            <ta e="T199" id="Seg_12416" s="T196">Сказала ему: вставай.</ta>
            <ta e="T207" id="Seg_12417" s="T199">Поешь, тут сладкое молоко есть.</ta>
            <ta e="T211" id="Seg_12418" s="T207">Ягоды есть, яйца, хлеб.</ta>
            <ta e="T213" id="Seg_12419" s="T211">Ешь, садись.</ta>
            <ta e="T218" id="Seg_12420" s="T213">Он встал.</ta>
            <ta e="T219" id="Seg_12421" s="T218">Он помылся.</ta>
            <ta e="T222" id="Seg_12422" s="T219">Потом он сел есть.</ta>
            <ta e="T223" id="Seg_12423" s="T222">Всё. </ta>
            <ta e="T226" id="Seg_12424" s="T223">Потом он ушел.</ta>
            <ta e="T229" id="Seg_12425" s="T226">А я помылась.</ta>
            <ta e="T232" id="Seg_12426" s="T229">Ну, всё (?). </ta>
            <ta e="T236" id="Seg_12427" s="T232">Вчера мы (?).</ta>
            <ta e="T239" id="Seg_12428" s="T236">Мы (?) пахали на лошади.</ta>
            <ta e="T242" id="Seg_12429" s="T239">Двух девушек поставили. [?]</ta>
            <ta e="T249" id="Seg_12430" s="T242">Я сама одного мужчину поставила. [?]</ta>
            <ta e="T250" id="Seg_12431" s="T249">Мы пахали.</ta>
            <ta e="T256" id="Seg_12432" s="T250">Погоди, как, я это забыла. </ta>
            <ta e="T260" id="Seg_12433" s="T256">Я завтра [=вчера?] пошла в баню.</ta>
            <ta e="T264" id="Seg_12434" s="T260">Я помылась, холодная вода была.</ta>
            <ta e="T266" id="Seg_12435" s="T264">Горячая вода.</ta>
            <ta e="T268" id="Seg_12436" s="T266">Я взяла мыло.</ta>
            <ta e="T270" id="Seg_12437" s="T268">Я взяла рубашку.</ta>
            <ta e="T274" id="Seg_12438" s="T270">Я пришла из бани, корова пришла.</ta>
            <ta e="T276" id="Seg_12439" s="T274">Я ее подоила.</ta>
            <ta e="T278" id="Seg_12440" s="T276">Я очень устала.</ta>
            <ta e="T282" id="Seg_12441" s="T278">Я упала [на кровать] и заснула.</ta>
            <ta e="T289" id="Seg_12442" s="T282">Сегодня я пойду за грибами, грибов принесу.</ta>
            <ta e="T291" id="Seg_12443" s="T289">Потом я их помою.</ta>
            <ta e="T293" id="Seg_12444" s="T291">Посолю.</ta>
            <ta e="T296" id="Seg_12445" s="T293">Потом они будут (солиться?).</ta>
            <ta e="T300" id="Seg_12446" s="T296">Потом можно есть.</ta>
            <ta e="T306" id="Seg_12447" s="T300">Начальник сидит, очень пьяный.</ta>
            <ta e="T311" id="Seg_12448" s="T306">Он выпил водки, очень много выпил.</ta>
            <ta e="T316" id="Seg_12449" s="T311">Упал, по-всякому ругался.</ta>
            <ta e="T319" id="Seg_12450" s="T316">Я его посадила.</ta>
            <ta e="T321" id="Seg_12451" s="T319">Сиди, сказала.</ta>
            <ta e="T323" id="Seg_12452" s="T321">Я сказала:</ta>
            <ta e="T326" id="Seg_12453" s="T323">«Не ругайся, сиди.</ta>
            <ta e="T330" id="Seg_12454" s="T326">Сиди и пей чай.</ta>
            <ta e="T335" id="Seg_12455" s="T330">Сахар лежит, положи в чай.</ta>
            <ta e="T337" id="Seg_12456" s="T335">Яйца лежат.</ta>
            <ta e="T341" id="Seg_12457" s="T337">Хлеб ешь!</ta>
            <ta e="T343" id="Seg_12458" s="T341">Мясо ешь!</ta>
            <ta e="T345" id="Seg_12459" s="T343">Рыбу ешь!</ta>
            <ta e="T348" id="Seg_12460" s="T345">Яйцо лежит, ешь!</ta>
            <ta e="T352" id="Seg_12461" s="T348">Ешь масло, ешь сметану.</ta>
            <ta e="T355" id="Seg_12462" s="T352">Сладкое молоко ешь!</ta>
            <ta e="T357" id="Seg_12463" s="T355">Рыбу ешь!</ta>
            <ta e="T358" id="Seg_12464" s="T357">Хватит!</ta>
            <ta e="T362" id="Seg_12465" s="T358">Сапоги сшить надо.</ta>
            <ta e="T364" id="Seg_12466" s="T362">Шубу сшить.</ta>
            <ta e="T370" id="Seg_12467" s="T364">Надо рубашку сшить, взять/купить.</ta>
            <ta e="T375" id="Seg_12468" s="T370">Надо штаны сшить.</ta>
            <ta e="T382" id="Seg_12469" s="T375">А то моему сыну нечего надеть.</ta>
            <ta e="T384" id="Seg_12470" s="T382">Ничего нет.</ta>
            <ta e="T392" id="Seg_12471" s="T384">Я поеду в Агинское, и надо хлеб купить.</ta>
            <ta e="T395" id="Seg_12472" s="T392">Надо сахар купить.</ta>
            <ta e="T401" id="Seg_12473" s="T395">Надо зерно для ружья [=дробь?] купить.</ta>
            <ta e="T402" id="Seg_12474" s="T401">Надо…</ta>
            <ta e="T410" id="Seg_12475" s="T408">Ну, хлеб.</ta>
            <ta e="T413" id="Seg_12476" s="T410">Ну, всё (?).</ta>
            <ta e="T415" id="Seg_12477" s="T413">(…) думала.</ta>
            <ta e="T419" id="Seg_12478" s="T415">Я потом в лес пойду.</ta>
            <ta e="T421" id="Seg_12479" s="T419">Ну, всё.</ta>
            <ta e="T427" id="Seg_12480" s="T421">Сегодня они придут из леса.</ta>
            <ta e="T430" id="Seg_12481" s="T427">Наверное, мясо принесут.</ta>
            <ta e="T432" id="Seg_12482" s="T430">Черемшу принесут.</ta>
            <ta e="T437" id="Seg_12483" s="T432">Надо [ее] порезать и посолить.</ta>
            <ta e="T447" id="Seg_12484" s="T437">Яйца положить, сметану положить и воду налить, и есть ложкой.</ta>
            <ta e="T456" id="Seg_12485" s="T447">Надо идти… идти…</ta>
            <ta e="T458" id="Seg_12486" s="T456">Погоди… </ta>
            <ta e="T461" id="Seg_12487" s="T458">Пахать, хлеб сеять.</ta>
            <ta e="T465" id="Seg_12488" s="T461">Потом он вырастет, (срезать?) надо.</ta>
            <ta e="T468" id="Seg_12489" s="T465">Потом молотить надо.</ta>
            <ta e="T478" id="Seg_12490" s="T468">Потом… Погоди, вот (?) потом веять надо.</ta>
            <ta e="T483" id="Seg_12491" s="T478">Потом надо печь.</ta>
            <ta e="T486" id="Seg_12492" s="T483">Потом надо есть.</ta>
            <ta e="T488" id="Seg_12493" s="T486">Вот… </ta>
            <ta e="T496" id="Seg_12494" s="T488">Сегодня большой день [=праздник], народу очень много.</ta>
            <ta e="T498" id="Seg_12495" s="T496">Все красивые.</ta>
            <ta e="T500" id="Seg_12496" s="T498">Водку пьют.</ta>
            <ta e="T502" id="Seg_12497" s="T500">Играют на гармони.</ta>
            <ta e="T503" id="Seg_12498" s="T502">Танцуют.</ta>
            <ta e="T505" id="Seg_12499" s="T503">Песни поют.</ta>
            <ta e="T507" id="Seg_12500" s="T505">Хорошая женщина!</ta>
            <ta e="T513" id="Seg_12501" s="T507">Хлеб испекла хороший.</ta>
            <ta e="T514" id="Seg_12502" s="T513">Мягкий.</ta>
            <ta e="T520" id="Seg_12503" s="T514">Теперь ешьте!</ta>
            <ta e="T522" id="Seg_12504" s="T520">Наверное, голодные.</ta>
            <ta e="T524" id="Seg_12505" s="T522">Сейчас я встану.</ta>
            <ta e="T527" id="Seg_12506" s="T524">Ешьте, сидите!</ta>
            <ta e="T530" id="Seg_12507" s="T527">Толстый человек поел.</ta>
            <ta e="T533" id="Seg_12508" s="T530">Хлеб с черемухой.</ta>
            <ta e="T535" id="Seg_12509" s="T533">Я положила сметану.</ta>
            <ta e="T541" id="Seg_12510" s="T535">Ешьте, пейте молоко.</ta>
            <ta e="T543" id="Seg_12511" s="T541">Всё… </ta>
            <ta e="T547" id="Seg_12512" s="T543">Ты еще спишь, вставай!</ta>
            <ta e="T554" id="Seg_12513" s="T547">А то я принесу холодной воды и вылью на тебя.</ta>
            <ta e="T557" id="Seg_12514" s="T554">Тогда ты подпрыгнешь.</ta>
            <ta e="T559" id="Seg_12515" s="T557">Всё, наверное. </ta>
            <ta e="T564" id="Seg_12516" s="T559">Погоди, погоди, как нас… </ta>
            <ta e="T566" id="Seg_12517" s="T564">(…).</ta>
            <ta e="T568" id="Seg_12518" s="T566">Не слушай!</ta>
            <ta e="T570" id="Seg_12519" s="T568">Не (режь?).</ta>
            <ta e="T571" id="Seg_12520" s="T570">Господи.</ta>
            <ta e="T575" id="Seg_12521" s="T573">Не разговаривай!</ta>
            <ta e="T579" id="Seg_12522" s="T577">Не ругайся!</ta>
            <ta e="T584" id="Seg_12523" s="T582">Не кричи!</ta>
            <ta e="T588" id="Seg_12524" s="T586">У тебя сопли текут.</ta>
            <ta e="T590" id="Seg_12525" s="T588">Вытри сопли.</ta>
            <ta e="T594" id="Seg_12526" s="T592">Не беги!</ta>
            <ta e="T596" id="Seg_12527" s="T594">Не прыгай!</ta>
            <ta e="T598" id="Seg_12528" s="T596">Не пой!</ta>
            <ta e="T600" id="Seg_12529" s="T598">Очень хороший.</ta>
            <ta e="T602" id="Seg_12530" s="T600">Очень красивый.</ta>
            <ta e="T604" id="Seg_12531" s="T602">Очень красный.</ta>
            <ta e="T605" id="Seg_12532" s="T604">Очень.</ta>
            <ta e="T609" id="Seg_12533" s="T606">(Толстый?), очень длинный.</ta>
            <ta e="T614" id="Seg_12534" s="T612">Очень маленький.</ta>
            <ta e="T618" id="Seg_12535" s="T614">Ну, теперь, наверное, всё. </ta>
            <ta e="T621" id="Seg_12536" s="T618">Наверное, платок украл.</ta>
            <ta e="T623" id="Seg_12537" s="T621">Зачем ты его принес?</ta>
            <ta e="T626" id="Seg_12538" s="T623">Я его не возьму.</ta>
            <ta e="T633" id="Seg_12539" s="T626">Не надо красть, а то плохо будет.</ta>
            <ta e="T638" id="Seg_12540" s="T633">Я порезала руку, кровь течет.</ta>
            <ta e="T642" id="Seg_12541" s="T638">Принесите что-нибудь, чтобы перевязать мне руку.</ta>
            <ta e="T650" id="Seg_12542" s="T645">А зачем ты нож взял?</ta>
            <ta e="T654" id="Seg_12543" s="T650">Тебе еще надо…</ta>
            <ta e="T658" id="Seg_12544" s="T654">Ветер, дождь идет.</ta>
            <ta e="T662" id="Seg_12545" s="T658">Снег идет с ветром.</ta>
            <ta e="T664" id="Seg_12546" s="T662">Очень холодно.</ta>
            <ta e="T667" id="Seg_12547" s="T664">Я очень замерзла.</ta>
            <ta e="T670" id="Seg_12548" s="T667">И сильно замерзла. [?]</ta>
            <ta e="T672" id="Seg_12549" s="T670">У меня ноги замерзли.</ta>
            <ta e="T674" id="Seg_12550" s="T672">У меня руки замерзли.</ta>
            <ta e="T677" id="Seg_12551" s="T674">У меня лицо замерзло.</ta>
            <ta e="T682" id="Seg_12552" s="T677">Сегодня хороший день.</ta>
            <ta e="T685" id="Seg_12553" s="T682">Солнце светит.</ta>
            <ta e="T688" id="Seg_12554" s="T685">Наверху ничего нет.</ta>
            <ta e="T691" id="Seg_12555" s="T688">Очень хороший день.</ta>
            <ta e="T694" id="Seg_12556" s="T691">Люди работают.</ta>
            <ta e="T696" id="Seg_12557" s="T694">Они сажают картошку.</ta>
            <ta e="T699" id="Seg_12558" s="T696">Лошади пашут.</ta>
            <ta e="T703" id="Seg_12559" s="T699">Надо пойти косить траву.</ta>
            <ta e="T706" id="Seg_12560" s="T703">Косу надо взять.</ta>
            <ta e="T709" id="Seg_12561" s="T706">Надо воду взять.</ta>
            <ta e="T718" id="Seg_12562" s="T709">Молоко надо взять, хлеб взять, соль взять, яйца взять.</ta>
            <ta e="T722" id="Seg_12563" s="T718">Камень надо взять для косы.</ta>
            <ta e="T725" id="Seg_12564" s="T722">Я сегодня убил соболя.</ta>
            <ta e="T727" id="Seg_12565" s="T725">Я убил белку.</ta>
            <ta e="T729" id="Seg_12566" s="T727">Я убил медведя.</ta>
            <ta e="T731" id="Seg_12567" s="T729">Я убил оленя.</ta>
            <ta e="T733" id="Seg_12568" s="T731">Я убил жеребца.</ta>
            <ta e="T736" id="Seg_12569" s="T733">Я убил козу из ружья.</ta>
            <ta e="T740" id="Seg_12570" s="T736">Коровы едят траву.</ta>
            <ta e="T742" id="Seg_12571" s="T740">Они ходят здесь.</ta>
            <ta e="T747" id="Seg_12572" s="T742">Этот человек отведет их домой.</ta>
            <ta e="T748" id="Seg_12573" s="T747">Всё.</ta>
            <ta e="T751" id="Seg_12574" s="T748">Надо ребенка помыть.</ta>
            <ta e="T757" id="Seg_12575" s="T751">Надо… ребенок плачет.</ta>
            <ta e="T760" id="Seg_12576" s="T757">Надо его распеленать.</ta>
            <ta e="T763" id="Seg_12577" s="T760">Надо [его] покормить.</ta>
            <ta e="T766" id="Seg_12578" s="T763">Надо спать положить.</ta>
            <ta e="T773" id="Seg_12579" s="T770">Укачай ребенка!</ta>
            <ta e="T775" id="Seg_12580" s="T773">Пусть спит.</ta>
            <ta e="T779" id="Seg_12581" s="T775">Сиди, я сейчас пойду за водой.</ta>
            <ta e="T781" id="Seg_12582" s="T779">Воды принесу.</ta>
            <ta e="T783" id="Seg_12583" s="T781">Потом попьешь.</ta>
            <ta e="T785" id="Seg_12584" s="T783">Не ходи!</ta>
            <ta e="T789" id="Seg_12585" s="T785">А потом пойдешь домой.</ta>
            <ta e="T792" id="Seg_12586" s="T789">Сейчас я буду печь кровь.</ta>
            <ta e="T801" id="Seg_12587" s="T792">Надо туда молоко налить, масло положить или жир положить.</ta>
            <ta e="T803" id="Seg_12588" s="T801">Сейчас мы будем есть.</ta>
            <ta e="T807" id="Seg_12589" s="T803">Не (?), а то горячо.</ta>
            <ta e="T811" id="Seg_12590" s="T807">С хлебом ешь!</ta>
            <ta e="T814" id="Seg_12591" s="T811">(…) пришел.</ta>
            <ta e="T816" id="Seg_12592" s="T814">Волосы белые.</ta>
            <ta e="T819" id="Seg_12593" s="T816">Сама стройная.</ta>
            <ta e="T825" id="Seg_12594" s="T819">И мужчина пришел, очень толстый.</ta>
            <ta e="T827" id="Seg_12595" s="T825">И дай. (?)</ta>
            <ta e="T829" id="Seg_12596" s="T827">Его/ее друг пришел.</ta>
            <ta e="T830" id="Seg_12597" s="T829">Всё.</ta>
            <ta e="T834" id="Seg_12598" s="T830">Они картошку не (сажали?).</ta>
            <ta e="T838" id="Seg_12599" s="T834">Мой отец им сажал.</ta>
            <ta e="T845" id="Seg_12600" s="T838">У них мяса много было, они мясо ели.</ta>
            <ta e="T848" id="Seg_12601" s="T845">Хлеба мало было.</ta>
            <ta e="T854" id="Seg_12602" s="T848">А больше ничего не было.</ta>
            <ta e="T858" id="Seg_12603" s="T854">Что ты сидишь, не говоришь.</ta>
            <ta e="T860" id="Seg_12604" s="T858">Скажи что-нибудь!</ta>
            <ta e="T867" id="Seg_12605" s="T860">У тебя сердце, наверное, болит.</ta>
            <ta e="T870" id="Seg_12606" s="T867">Ты ничего не говоришь.</ta>
            <ta e="T871" id="Seg_12607" s="T870">Говори!</ta>
            <ta e="T872" id="Seg_12608" s="T871">Всё.</ta>
            <ta e="T876" id="Seg_12609" s="T872">Куда ты идешь?</ta>
            <ta e="T880" id="Seg_12610" s="T876">Где ты был, иди сюда!</ta>
            <ta e="T883" id="Seg_12611" s="T880">Скажи мне что-нибудь!</ta>
            <ta e="T885" id="Seg_12612" s="T883">Опять забыла. </ta>
            <ta e="T888" id="Seg_12613" s="T885">Мужчина с женщиной жили.</ta>
            <ta e="T891" id="Seg_12614" s="T888">У них были дети.</ta>
            <ta e="T894" id="Seg_12615" s="T891">Девочка и мальчик.</ta>
            <ta e="T896" id="Seg_12616" s="T894">У них был дом.</ta>
            <ta e="T898" id="Seg_12617" s="T896">У них была корова.</ta>
            <ta e="T902" id="Seg_12618" s="T898">Эта женщина доила корову.</ta>
            <ta e="T904" id="Seg_12619" s="T902">Давала детям [молоко].</ta>
            <ta e="T906" id="Seg_12620" s="T904">Давала им хлеб.</ta>
            <ta e="T908" id="Seg_12621" s="T906">Они едят.</ta>
            <ta e="T909" id="Seg_12622" s="T908">Всё.</ta>
            <ta e="T913" id="Seg_12623" s="T909">Эти две человека пришли.</ta>
            <ta e="T920" id="Seg_12624" s="T913">Мужчина и девушка, наверное, принесли тебе что-нибудь.</ta>
            <ta e="T922" id="Seg_12625" s="T920">Сахар принесли.</ta>
            <ta e="T927" id="Seg_12626" s="T922">Ты чай будешь пить.</ta>
            <ta e="T931" id="Seg_12627" s="T927">Наверное, конфеты принесли.</ta>
            <ta e="T934" id="Seg_12628" s="T931">Наверное, рубашку принесли.</ta>
            <ta e="T938" id="Seg_12629" s="T934">Штаны, наверное, принесли тебе.</ta>
            <ta e="T941" id="Seg_12630" s="T938">У председателя дом большой.</ta>
            <ta e="T943" id="Seg_12631" s="T941">Народу много.</ta>
            <ta e="T949" id="Seg_12632" s="T943">Два мальчика, две девочки.</ta>
            <ta e="T951" id="Seg_12633" s="T949">Женщина с мужчиной.</ta>
            <ta e="T953" id="Seg_12634" s="T951">Он сам, жена.</ta>
            <ta e="T956" id="Seg_12635" s="T953">Две его собаки.</ta>
            <ta e="T966" id="Seg_12636" s="T956">Одна хорошая собака, у одной три оги, а четвертой ноги нет.</ta>
            <ta e="T969" id="Seg_12637" s="T966">Четвертой ноги нету. </ta>
            <ta e="T975" id="Seg_12638" s="T969">Ты меня бей, а чем ты меня бьешь?</ta>
            <ta e="T982" id="Seg_12639" s="T975">Не бей кулаком, а то мне будет больно.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-PKZ">
            <ta e="T13" id="Seg_12640" s="T4">I'll first tell, wait, not…</ta>
            <ta e="T20" id="Seg_12641" s="T14">Today I will go to the taiga with a horse.</ta>
            <ta e="T30" id="Seg_12642" s="T20">Where is the bag, I should put bread in, put salt in, I should put a spoon in.</ta>
            <ta e="T33" id="Seg_12643" s="T30">I should put meat in.</ta>
            <ta e="T38" id="Seg_12644" s="T33">I should put bread in.</ta>
            <ta e="T42" id="Seg_12645" s="T38">(…) I'm saying, God bless me!</ta>
            <ta e="T51" id="Seg_12646" s="T46">I should put bread in.</ta>
            <ta e="T54" id="Seg_12647" s="T51">I should put butter in.</ta>
            <ta e="T56" id="Seg_12648" s="T54">Put eggs in.</ta>
            <ta e="T59" id="Seg_12649" s="T56">I should put a spoon in.</ta>
            <ta e="T62" id="Seg_12650" s="T59">I should put a cauldron in.</ta>
            <ta e="T65" id="Seg_12651" s="T62">I should take the dog.</ta>
            <ta e="T68" id="Seg_12652" s="T65">I should take the gun.</ta>
            <ta e="T71" id="Seg_12653" s="T68">I should take the axe.</ta>
            <ta e="T74" id="Seg_12654" s="T71">I should take the knife.</ta>
            <ta e="T86" id="Seg_12655" s="T85">[?]</ta>
            <ta e="T94" id="Seg_12656" s="T90">Two persons are coming to speak.</ta>
            <ta e="T95" id="Seg_12657" s="T94">Sit!</ta>
            <ta e="T96" id="Seg_12658" s="T95">We will speak!</ta>
            <ta e="T101" id="Seg_12659" s="T96">You probably want to eat, sit, eat!</ta>
            <ta e="T104" id="Seg_12660" s="T101">There is bread lying.</ta>
            <ta e="T106" id="Seg_12661" s="T104">There is butter lying.</ta>
            <ta e="T115" id="Seg_12662" s="T106">There is a spoon, there is salt, eggs, meat, fish, eat all of it!</ta>
            <ta e="T117" id="Seg_12663" s="T115">Don't make noise.</ta>
            <ta e="T119" id="Seg_12664" s="T117">Don't shout!</ta>
            <ta e="T122" id="Seg_12665" s="T119">Otherwise I will beat you!</ta>
            <ta e="T124" id="Seg_12666" s="T122">Don't cry!</ta>
            <ta e="T126" id="Seg_12667" s="T124">Don't get angry!</ta>
            <ta e="T128" id="Seg_12668" s="T126">Don't lie!</ta>
            <ta e="T131" id="Seg_12669" s="T128">You are (sitting?), lying!</ta>
            <ta e="T135" id="Seg_12670" s="T131">You lie very much!</ta>
            <ta e="T137" id="Seg_12671" s="T135">That's all, maybe.</ta>
            <ta e="T141" id="Seg_12672" s="T137">Now I will go to (that place?).</ta>
            <ta e="T145" id="Seg_12673" s="T141">And you sit at home!</ta>
            <ta e="T147" id="Seg_12674" s="T145">Don't (make noise?)!</ta>
            <ta e="T151" id="Seg_12675" s="T147">I will bring you something.</ta>
            <ta e="T154" id="Seg_12676" s="T151">Then I will give you.</ta>
            <ta e="T161" id="Seg_12677" s="T154">Yesterday, yesterday my relative came.</ta>
            <ta e="T165" id="Seg_12678" s="T161">I said: lie down to sleep!</ta>
            <ta e="T171" id="Seg_12679" s="T165">Then I said: eat, you probably want to eat.</ta>
            <ta e="T180" id="Seg_12680" s="T171">S/he said: I, I (will?) not eat…</ta>
            <ta e="T184" id="Seg_12681" s="T180">I ate at the house there.</ta>
            <ta e="T189" id="Seg_12682" s="T184">Then s/he lied down to sleep.</ta>
            <ta e="T191" id="Seg_12683" s="T189">I lied down.</ta>
            <ta e="T193" id="Seg_12684" s="T191">In the morning I got up.</ta>
            <ta e="T195" id="Seg_12685" s="T193">I went to (that place?).</ta>
            <ta e="T196" id="Seg_12686" s="T195">I came.</ta>
            <ta e="T199" id="Seg_12687" s="T196">I told her/him: get up!</ta>
            <ta e="T207" id="Seg_12688" s="T199">Eat, there is sweet milk.</ta>
            <ta e="T211" id="Seg_12689" s="T207">There are berries, eggs, bread.</ta>
            <ta e="T213" id="Seg_12690" s="T211">Eat, sit!</ta>
            <ta e="T218" id="Seg_12691" s="T213">S/he got up.</ta>
            <ta e="T219" id="Seg_12692" s="T218">S/he washed.</ta>
            <ta e="T222" id="Seg_12693" s="T219">Then s/he sat down to eat.</ta>
            <ta e="T223" id="Seg_12694" s="T222">That's all.</ta>
            <ta e="T226" id="Seg_12695" s="T223">Then s/he left.</ta>
            <ta e="T229" id="Seg_12696" s="T226">And I washed.</ta>
            <ta e="T232" id="Seg_12697" s="T229">Well, that's all (?).</ta>
            <ta e="T236" id="Seg_12698" s="T232">Yesterday we (?).</ta>
            <ta e="T239" id="Seg_12699" s="T236">We (?) ploughed with a horse.</ta>
            <ta e="T242" id="Seg_12700" s="T239">They put two girls. [?]</ta>
            <ta e="T249" id="Seg_12701" s="T242">I put one man myself. [?]</ta>
            <ta e="T250" id="Seg_12702" s="T249">We ploughed.</ta>
            <ta e="T256" id="Seg_12703" s="T250">Wait, I forgot that.</ta>
            <ta e="T260" id="Seg_12704" s="T256">I went to sauna tomorrow [=yesterday?].</ta>
            <ta e="T264" id="Seg_12705" s="T260">I washed, there was cold water.</ta>
            <ta e="T266" id="Seg_12706" s="T264">Warm water.</ta>
            <ta e="T268" id="Seg_12707" s="T266">I took soap.</ta>
            <ta e="T270" id="Seg_12708" s="T268">I took a shirt.</ta>
            <ta e="T274" id="Seg_12709" s="T270">I came from the sauna, the cow came.</ta>
            <ta e="T276" id="Seg_12710" s="T274">I milked it.</ta>
            <ta e="T278" id="Seg_12711" s="T276">I got very tired.</ta>
            <ta e="T282" id="Seg_12712" s="T278">I fell down [on the bed] and fell asleep.</ta>
            <ta e="T289" id="Seg_12713" s="T282">Today I will go to pick mushrooms, I will bring mushrooms.</ta>
            <ta e="T291" id="Seg_12714" s="T289">Then I will wash them.</ta>
            <ta e="T293" id="Seg_12715" s="T291">I will salt all of them.</ta>
            <ta e="T296" id="Seg_12716" s="T293">Then they'll (get salted?).</ta>
            <ta e="T300" id="Seg_12717" s="T296">Then they can be eaten.</ta>
            <ta e="T306" id="Seg_12718" s="T300">The chief was sitting, very drunk.</ta>
            <ta e="T311" id="Seg_12719" s="T306">He drank vodka, he drank very much.</ta>
            <ta e="T316" id="Seg_12720" s="T311">He fell down, he is swearing with all kinds of words.</ta>
            <ta e="T319" id="Seg_12721" s="T316">I sat him down.</ta>
            <ta e="T321" id="Seg_12722" s="T319">Sit, I said.</ta>
            <ta e="T323" id="Seg_12723" s="T321">I said:</ta>
            <ta e="T326" id="Seg_12724" s="T323">"Don’t swear, sit!</ta>
            <ta e="T330" id="Seg_12725" s="T326">Sit and drink tea.</ta>
            <ta e="T335" id="Seg_12726" s="T330">There is sugar, put (some) in the tea.</ta>
            <ta e="T337" id="Seg_12727" s="T335">There are eggs.</ta>
            <ta e="T341" id="Seg_12728" s="T337">Eat bread!</ta>
            <ta e="T343" id="Seg_12729" s="T341">Eat meat!</ta>
            <ta e="T345" id="Seg_12730" s="T343">Eat fish!</ta>
            <ta e="T348" id="Seg_12731" s="T345">There is an egg lying, eat!</ta>
            <ta e="T352" id="Seg_12732" s="T348">Eat butter, eat cream!</ta>
            <ta e="T355" id="Seg_12733" s="T352">Eat sweet milk!</ta>
            <ta e="T357" id="Seg_12734" s="T355">Eat fish!</ta>
            <ta e="T358" id="Seg_12735" s="T357">Enough.</ta>
            <ta e="T362" id="Seg_12736" s="T358">I must sew boots.</ta>
            <ta e="T364" id="Seg_12737" s="T362">To sew a coat.</ta>
            <ta e="T370" id="Seg_12738" s="T364">I need to sew a shirt, to take/buy.</ta>
            <ta e="T375" id="Seg_12739" s="T370">I need to sew trousers.</ta>
            <ta e="T382" id="Seg_12740" s="T375">Otherwise my son has nothing to wear.</ta>
            <ta e="T384" id="Seg_12741" s="T382">There’s nothing.</ta>
            <ta e="T392" id="Seg_12742" s="T384">I will go to Aginskoye and I need to buy bread.</ta>
            <ta e="T395" id="Seg_12743" s="T392">I need to buy sugar.</ta>
            <ta e="T401" id="Seg_12744" s="T395">I need to buy grain for the gun [=shot?].</ta>
            <ta e="T402" id="Seg_12745" s="T401">I need to…</ta>
            <ta e="T410" id="Seg_12746" s="T408">Well, bread.</ta>
            <ta e="T413" id="Seg_12747" s="T410">Well, that's all (?).</ta>
            <ta e="T415" id="Seg_12748" s="T413">(…) I thought.</ta>
            <ta e="T419" id="Seg_12749" s="T415">I will then go to the taiga.</ta>
            <ta e="T421" id="Seg_12750" s="T419">Well, that's all.</ta>
            <ta e="T427" id="Seg_12751" s="T421">Today they will come from the taiga.</ta>
            <ta e="T430" id="Seg_12752" s="T427">Perhaps they will bring meat.</ta>
            <ta e="T432" id="Seg_12753" s="T430">They will bring bear leek.</ta>
            <ta e="T437" id="Seg_12754" s="T432">It needs to be cut and salted.</ta>
            <ta e="T447" id="Seg_12755" s="T437">To add egg, to add cream and pour water and to eat with a spoon.</ta>
            <ta e="T456" id="Seg_12756" s="T447">I should go… go…</ta>
            <ta e="T458" id="Seg_12757" s="T456">Wait…</ta>
            <ta e="T461" id="Seg_12758" s="T458">To plough, to sow grain.</ta>
            <ta e="T465" id="Seg_12759" s="T461">Then it will grow, and one should (cut?) it.</ta>
            <ta e="T468" id="Seg_12760" s="T465">Then one should thresh it.</ta>
            <ta e="T478" id="Seg_12761" s="T468">Then… Wait, here (?) then one should winnow.</ta>
            <ta e="T483" id="Seg_12762" s="T478">Then one should bake it.</ta>
            <ta e="T486" id="Seg_12763" s="T483">Then one should eat it.</ta>
            <ta e="T488" id="Seg_12764" s="T486">That's it…</ta>
            <ta e="T496" id="Seg_12765" s="T488">Today is a big day [=holiday], [there are] very many people.</ta>
            <ta e="T498" id="Seg_12766" s="T496">All beautiful.</ta>
            <ta e="T500" id="Seg_12767" s="T498">They are drinking vodka.</ta>
            <ta e="T502" id="Seg_12768" s="T500">They are playing the accordeon.</ta>
            <ta e="T503" id="Seg_12769" s="T502">They are dancing.</ta>
            <ta e="T505" id="Seg_12770" s="T503">They are singing song[s].</ta>
            <ta e="T507" id="Seg_12771" s="T505">Good woman!</ta>
            <ta e="T513" id="Seg_12772" s="T507">She baked good bread.</ta>
            <ta e="T514" id="Seg_12773" s="T513">Soft!</ta>
            <ta e="T520" id="Seg_12774" s="T514">Now eat!</ta>
            <ta e="T522" id="Seg_12775" s="T520">Perhaps (you) are hungry.</ta>
            <ta e="T524" id="Seg_12776" s="T522">Now I will get up.</ta>
            <ta e="T527" id="Seg_12777" s="T524">Eat, sit!</ta>
            <ta e="T530" id="Seg_12778" s="T527">The fat man ate.</ta>
            <ta e="T533" id="Seg_12779" s="T530">Bread with bird cherries.</ta>
            <ta e="T535" id="Seg_12780" s="T533">I put cream.</ta>
            <ta e="T541" id="Seg_12781" s="T535">Eat, drink milk!</ta>
            <ta e="T543" id="Seg_12782" s="T541">That's all…</ta>
            <ta e="T547" id="Seg_12783" s="T543">You are still sleeping, get up!</ta>
            <ta e="T554" id="Seg_12784" s="T547">Otherwise I will bring cold water, I will pour it all on you.</ta>
            <ta e="T557" id="Seg_12785" s="T554">Then you will jump.</ta>
            <ta e="T559" id="Seg_12786" s="T557">That's probably all.</ta>
            <ta e="T564" id="Seg_12787" s="T559">Wait, wait, how we…</ta>
            <ta e="T566" id="Seg_12788" s="T564">(…).</ta>
            <ta e="T568" id="Seg_12789" s="T566">Do not listen!</ta>
            <ta e="T570" id="Seg_12790" s="T568">Don’t (cut?).</ta>
            <ta e="T571" id="Seg_12791" s="T570">Oh god.</ta>
            <ta e="T575" id="Seg_12792" s="T573">Don’t speak!</ta>
            <ta e="T579" id="Seg_12793" s="T577">Don’t swear!</ta>
            <ta e="T584" id="Seg_12794" s="T582">Don’t shout!</ta>
            <ta e="T588" id="Seg_12795" s="T586">Your snot is flowing.</ta>
            <ta e="T590" id="Seg_12796" s="T588">Wipe your snot!</ta>
            <ta e="T594" id="Seg_12797" s="T592">Don’t run!</ta>
            <ta e="T596" id="Seg_12798" s="T594">Don’t jump!</ta>
            <ta e="T598" id="Seg_12799" s="T596">Don’t sing!</ta>
            <ta e="T600" id="Seg_12800" s="T598">Very good.</ta>
            <ta e="T602" id="Seg_12801" s="T600">Very beautiful.</ta>
            <ta e="T604" id="Seg_12802" s="T602">Very red.</ta>
            <ta e="T605" id="Seg_12803" s="T604">Very.</ta>
            <ta e="T609" id="Seg_12804" s="T606">(Fat?), very long.</ta>
            <ta e="T614" id="Seg_12805" s="T612">Very small.</ta>
            <ta e="T618" id="Seg_12806" s="T614">Well, now that's probably all.</ta>
            <ta e="T621" id="Seg_12807" s="T618">Perhaps you stole a scarf.</ta>
            <ta e="T623" id="Seg_12808" s="T621">Why did you bring it?</ta>
            <ta e="T626" id="Seg_12809" s="T623">I will not take it.</ta>
            <ta e="T633" id="Seg_12810" s="T626">You shouldn’t steal, otherwise it will be no good.</ta>
            <ta e="T638" id="Seg_12811" s="T633">I cut my hand, blood is flowing.</ta>
            <ta e="T642" id="Seg_12812" s="T638">Bring something to tie my hand.</ta>
            <ta e="T650" id="Seg_12813" s="T645">Why did you take a knife?</ta>
            <ta e="T654" id="Seg_12814" s="T650">You need more…</ta>
            <ta e="T658" id="Seg_12815" s="T654">It's windy, it rains.</ta>
            <ta e="T662" id="Seg_12816" s="T658">It snows and it's windy.</ta>
            <ta e="T664" id="Seg_12817" s="T662">Very cold.</ta>
            <ta e="T667" id="Seg_12818" s="T664">I was freezing.</ta>
            <ta e="T670" id="Seg_12819" s="T667">And I froze very much. [?]</ta>
            <ta e="T672" id="Seg_12820" s="T670">My feet froze.</ta>
            <ta e="T674" id="Seg_12821" s="T672">My hands froze.</ta>
            <ta e="T677" id="Seg_12822" s="T674">My face froze.</ta>
            <ta e="T682" id="Seg_12823" s="T677">Today is a beautiful day.</ta>
            <ta e="T685" id="Seg_12824" s="T682">The sun is shining.</ta>
            <ta e="T688" id="Seg_12825" s="T685">There is nothing up there.</ta>
            <ta e="T691" id="Seg_12826" s="T688">A very good day.</ta>
            <ta e="T694" id="Seg_12827" s="T691">People are working.</ta>
            <ta e="T696" id="Seg_12828" s="T694">They are planting potatoes.</ta>
            <ta e="T699" id="Seg_12829" s="T696">Horses are ploughing.</ta>
            <ta e="T703" id="Seg_12830" s="T699">One should go to cut the grass.</ta>
            <ta e="T706" id="Seg_12831" s="T703">One should take the scythe.</ta>
            <ta e="T709" id="Seg_12832" s="T706">One should take water.</ta>
            <ta e="T718" id="Seg_12833" s="T709">One should take milk, take bread, take salt, take eggs.</ta>
            <ta e="T722" id="Seg_12834" s="T718">One should take the scythe stone.</ta>
            <ta e="T725" id="Seg_12835" s="T722">I killed a sable today.</ta>
            <ta e="T727" id="Seg_12836" s="T725">I killed a squirrel.</ta>
            <ta e="T729" id="Seg_12837" s="T727">I killed a bear.</ta>
            <ta e="T731" id="Seg_12838" s="T729">I killed a deer.</ta>
            <ta e="T733" id="Seg_12839" s="T731">I killed a stallion.</ta>
            <ta e="T736" id="Seg_12840" s="T733">I killed a goat with a gun.</ta>
            <ta e="T740" id="Seg_12841" s="T736">Cows are eating grass.</ta>
            <ta e="T742" id="Seg_12842" s="T740">They are walking around there.</ta>
            <ta e="T747" id="Seg_12843" s="T742">This man will bring them home.</ta>
            <ta e="T748" id="Seg_12844" s="T747">That's all.</ta>
            <ta e="T751" id="Seg_12845" s="T748">I need to wash the child.</ta>
            <ta e="T757" id="Seg_12846" s="T751">I need… my child is crying.</ta>
            <ta e="T760" id="Seg_12847" s="T757">I should unwrap it.</ta>
            <ta e="T763" id="Seg_12848" s="T760">I should give [him/her] something to eat.</ta>
            <ta e="T766" id="Seg_12849" s="T763">I should put him/her to sleep.</ta>
            <ta e="T773" id="Seg_12850" s="T770">Lull the child!</ta>
            <ta e="T775" id="Seg_12851" s="T773">Let him/her sleep.</ta>
            <ta e="T779" id="Seg_12852" s="T775">Sit, now I will go to fetch water.</ta>
            <ta e="T781" id="Seg_12853" s="T779">I will bring water.</ta>
            <ta e="T783" id="Seg_12854" s="T781">Then you will drink.</ta>
            <ta e="T785" id="Seg_12855" s="T783">Don’t go!</ta>
            <ta e="T789" id="Seg_12856" s="T785">And then you will go home.</ta>
            <ta e="T792" id="Seg_12857" s="T789">Now I will bake blood.</ta>
            <ta e="T801" id="Seg_12858" s="T792">I should pour milk there, put butter, or put fat.</ta>
            <ta e="T803" id="Seg_12859" s="T801">Now we will eat.</ta>
            <ta e="T807" id="Seg_12860" s="T803">Don’t (?), it is warm.</ta>
            <ta e="T811" id="Seg_12861" s="T807">Eat with bread!</ta>
            <ta e="T814" id="Seg_12862" s="T811">(…) came.</ta>
            <ta e="T816" id="Seg_12863" s="T814">White hair.</ta>
            <ta e="T819" id="Seg_12864" s="T816">Herself is slim.</ta>
            <ta e="T825" id="Seg_12865" s="T819">And the man came, very fat.</ta>
            <ta e="T827" id="Seg_12866" s="T825">And give. (?)</ta>
            <ta e="T829" id="Seg_12867" s="T827">His/her friend came.</ta>
            <ta e="T830" id="Seg_12868" s="T829">That's all.</ta>
            <ta e="T834" id="Seg_12869" s="T830">They did not (plant?) potatoes.</ta>
            <ta e="T838" id="Seg_12870" s="T834">My father planted for them.</ta>
            <ta e="T845" id="Seg_12871" s="T838">They had a lot of meat, they ate meat.</ta>
            <ta e="T848" id="Seg_12872" s="T845">There was little bread.</ta>
            <ta e="T854" id="Seg_12873" s="T848">And there was nothing else.</ta>
            <ta e="T858" id="Seg_12874" s="T854">Why are you sitting without speaking.</ta>
            <ta e="T860" id="Seg_12875" s="T858">Say something!</ta>
            <ta e="T867" id="Seg_12876" s="T860">Your heart probably hurts.</ta>
            <ta e="T870" id="Seg_12877" s="T867">You do not say anything.</ta>
            <ta e="T871" id="Seg_12878" s="T870">Speak!</ta>
            <ta e="T872" id="Seg_12879" s="T871">That's all.</ta>
            <ta e="T876" id="Seg_12880" s="T872">Where do you go?</ta>
            <ta e="T880" id="Seg_12881" s="T876">Where were you, come here!</ta>
            <ta e="T883" id="Seg_12882" s="T880">Tell me something!</ta>
            <ta e="T885" id="Seg_12883" s="T883">I forgot again.</ta>
            <ta e="T888" id="Seg_12884" s="T885">A man lived with a woman.</ta>
            <ta e="T891" id="Seg_12885" s="T888">They had children.</ta>
            <ta e="T894" id="Seg_12886" s="T891">A girl and a boy.</ta>
            <ta e="T896" id="Seg_12887" s="T894">They had a house.</ta>
            <ta e="T898" id="Seg_12888" s="T896">They had a cow.</ta>
            <ta e="T902" id="Seg_12889" s="T898">The woman milked the cow.</ta>
            <ta e="T904" id="Seg_12890" s="T902">Gave [milk] to the children.</ta>
            <ta e="T906" id="Seg_12891" s="T904">Gave them bread.</ta>
            <ta e="T908" id="Seg_12892" s="T906">They eat.</ta>
            <ta e="T909" id="Seg_12893" s="T908">That's all.</ta>
            <ta e="T913" id="Seg_12894" s="T909">These two men came.</ta>
            <ta e="T920" id="Seg_12895" s="T913">The man and the girl perhaps brought you something.</ta>
            <ta e="T922" id="Seg_12896" s="T920">They brought sugar.</ta>
            <ta e="T927" id="Seg_12897" s="T922">You will drink tea.</ta>
            <ta e="T931" id="Seg_12898" s="T927">Perhaps they brought you candy.</ta>
            <ta e="T934" id="Seg_12899" s="T931">Perhaps they brought a shirt.</ta>
            <ta e="T938" id="Seg_12900" s="T934">Perhaps they brought you pants.</ta>
            <ta e="T941" id="Seg_12901" s="T938">The house of the head of the village is big.</ta>
            <ta e="T943" id="Seg_12902" s="T941">A lot of people.</ta>
            <ta e="T949" id="Seg_12903" s="T943">Two boys, two girls.</ta>
            <ta e="T951" id="Seg_12904" s="T949">A woman and a man.</ta>
            <ta e="T953" id="Seg_12905" s="T951">Himself, his wife.</ta>
            <ta e="T956" id="Seg_12906" s="T953">His two dogs.</ta>
            <ta e="T966" id="Seg_12907" s="T956">One good dog, one with three legs, and it doesn't have the fourth leg.</ta>
            <ta e="T969" id="Seg_12908" s="T966">No fourth leg.</ta>
            <ta e="T975" id="Seg_12909" s="T969">You beat me, but with what are you beating?</ta>
            <ta e="T982" id="Seg_12910" s="T975">Don't beat with the fist, otherwise I will get hurt.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PKZ">
            <ta e="T13" id="Seg_12911" s="T4">Als Erstes, erzähle ich, Moment, nicht…</ta>
            <ta e="T20" id="Seg_12912" s="T14">Heute werde ich mit einem Pferd auf die Taiga ausreiten.</ta>
            <ta e="T30" id="Seg_12913" s="T20">Wo ist der Beutel, ich sollte mir Brot einstecken, Salz einstecken, ich sollte einen Löffel einstecken.</ta>
            <ta e="T33" id="Seg_12914" s="T30">Ich sollte mir Fleisch einstecken.</ta>
            <ta e="T38" id="Seg_12915" s="T33">Ich sollte mir Brot einstecken.</ta>
            <ta e="T42" id="Seg_12916" s="T38">(…) Ich sag’s dir, Gott segne mich!</ta>
            <ta e="T51" id="Seg_12917" s="T46">Ich sollte mir Brot einstecken.</ta>
            <ta e="T54" id="Seg_12918" s="T51">Ich sollte mir Butter einstecken. </ta>
            <ta e="T56" id="Seg_12919" s="T54">Eier einstecken.</ta>
            <ta e="T59" id="Seg_12920" s="T56">Ich sollte mir einen Löffel einstecken.</ta>
            <ta e="T62" id="Seg_12921" s="T59">Ich sollte mir einen Kessel einstecken.</ta>
            <ta e="T65" id="Seg_12922" s="T62">Ich sollte den Hund mitnehmen.</ta>
            <ta e="T68" id="Seg_12923" s="T65">Ich sollte das Gewehr mitnehmen.</ta>
            <ta e="T71" id="Seg_12924" s="T68">Ich sollte die Axt mitnehmen.</ta>
            <ta e="T74" id="Seg_12925" s="T71">Ich sollte das Messer mitnehmen.</ta>
            <ta e="T86" id="Seg_12926" s="T85">[?]</ta>
            <ta e="T94" id="Seg_12927" s="T90">Zwei Personen kommen um zu sprechen.</ta>
            <ta e="T95" id="Seg_12928" s="T94">Setzt euch!</ta>
            <ta e="T96" id="Seg_12929" s="T95">Wir werden sprechen!</ta>
            <ta e="T101" id="Seg_12930" s="T96">Ihr wollt bestimmt essen, setzt euch, esst!</ta>
            <ta e="T104" id="Seg_12931" s="T101">Dort liegt Brot.</ta>
            <ta e="T106" id="Seg_12932" s="T104">Dort liegt Butter.</ta>
            <ta e="T115" id="Seg_12933" s="T106">Dort ist ein Löffel, dort ist Salz, Eier, Fleisch, Fisch, esst von allem!</ta>
            <ta e="T117" id="Seg_12934" s="T115">Mach kein Lärm!</ta>
            <ta e="T119" id="Seg_12935" s="T117">Gröll nicht!</ta>
            <ta e="T122" id="Seg_12936" s="T119">Sonst gebe ich dir Dresche!</ta>
            <ta e="T124" id="Seg_12937" s="T122">Weint nicht!</ta>
            <ta e="T126" id="Seg_12938" s="T124">Wird nicht wütend!</ta>
            <ta e="T128" id="Seg_12939" s="T126">Lüge nicht!</ta>
            <ta e="T131" id="Seg_12940" s="T128">Du (sitzt?) und lügst!</ta>
            <ta e="T135" id="Seg_12941" s="T131">Ihr lügst sehr viel!</ta>
            <ta e="T137" id="Seg_12942" s="T135">Das ist alles, vielleicht.</ta>
            <ta e="T141" id="Seg_12943" s="T137">Nun werde ich mich (zu dem Ort?) begeben. </ta>
            <ta e="T145" id="Seg_12944" s="T141">Und du sitze zu Hause!</ta>
            <ta e="T147" id="Seg_12945" s="T145">Macht kein (Lärm?)!</ta>
            <ta e="T151" id="Seg_12946" s="T147">Ich werde euch etwas bringen.</ta>
            <ta e="T154" id="Seg_12947" s="T151">Dann gebe ich euch.</ta>
            <ta e="T161" id="Seg_12948" s="T154">Gestern, gestern kam mein Verwandte.</ta>
            <ta e="T165" id="Seg_12949" s="T161">Ich sagte, leg dich hin und schlafe!</ta>
            <ta e="T171" id="Seg_12950" s="T165">Dann sagte ich: iss, du willst bestimmt essen.</ta>
            <ta e="T180" id="Seg_12951" s="T171">Sie/Er sagte: Ich, Ich (will?) nicht essen…</ta>
            <ta e="T184" id="Seg_12952" s="T180">Ich aß im Haus dort.</ta>
            <ta e="T189" id="Seg_12953" s="T184">Dann legte sie/er sich hin und schlief.</ta>
            <ta e="T191" id="Seg_12954" s="T189">Ich legte mich hin.</ta>
            <ta e="T193" id="Seg_12955" s="T191">Am Morgen stand ich auf.</ta>
            <ta e="T195" id="Seg_12956" s="T193">Ich ging zu (dem Ort?).</ta>
            <ta e="T196" id="Seg_12957" s="T195">Ich kam.</ta>
            <ta e="T199" id="Seg_12958" s="T196">Ich sagte ihr/ihn: steh auf!</ta>
            <ta e="T207" id="Seg_12959" s="T199">Iss, es ist süße Milch.</ta>
            <ta e="T211" id="Seg_12960" s="T207">Es sind Beeren, Eier, Brot.</ta>
            <ta e="T213" id="Seg_12961" s="T211">Iss, sitz!</ta>
            <ta e="T218" id="Seg_12962" s="T213">Sie/Er stand auf.</ta>
            <ta e="T219" id="Seg_12963" s="T218">Sie/Er wusch sich.</ta>
            <ta e="T222" id="Seg_12964" s="T219">Dann setzte sie/er sich um zu essen.</ta>
            <ta e="T223" id="Seg_12965" s="T222">Das ist alles.</ta>
            <ta e="T226" id="Seg_12966" s="T223">Dann ging sie/er.</ta>
            <ta e="T229" id="Seg_12967" s="T226">Und ich wusch mich.</ta>
            <ta e="T232" id="Seg_12968" s="T229">Also, das ist alles (?).</ta>
            <ta e="T236" id="Seg_12969" s="T232">Gestern, haben wir (?).</ta>
            <ta e="T239" id="Seg_12970" s="T236">Wir (?) haben mit einem Pferd gepflügt.</ta>
            <ta e="T242" id="Seg_12971" s="T239">Zwei Mädchen wurden eingesetzt. [?]</ta>
            <ta e="T249" id="Seg_12972" s="T242">Ich setzte einen Mann selber ein. [?]</ta>
            <ta e="T250" id="Seg_12973" s="T249">Wir pflügten.</ta>
            <ta e="T256" id="Seg_12974" s="T250">Warte, das habe ich vergessen.</ta>
            <ta e="T260" id="Seg_12975" s="T256">Ich ging morgen [=gestern?] zur Sauna.</ta>
            <ta e="T264" id="Seg_12976" s="T260">Ich habe mich gewaschen, es war kaltes Wasser.</ta>
            <ta e="T266" id="Seg_12977" s="T264">Warmes Wasser.</ta>
            <ta e="T268" id="Seg_12978" s="T266">Ich nahm Seife</ta>
            <ta e="T270" id="Seg_12979" s="T268">Ich nahm ein Hemd.</ta>
            <ta e="T274" id="Seg_12980" s="T270">Ich kam von der Sauna, die Kuh kam.</ta>
            <ta e="T276" id="Seg_12981" s="T274">Ich molk sie.</ta>
            <ta e="T278" id="Seg_12982" s="T276">Ich wurde sehr müde.</ta>
            <ta e="T282" id="Seg_12983" s="T278">Ich fiel [auf das Bett] und schlief ein.</ta>
            <ta e="T289" id="Seg_12984" s="T282">Heute werde ich Pilze sammeln. Ich werde Pilze bringen.</ta>
            <ta e="T291" id="Seg_12985" s="T289">Dann werde ich sie waschen.</ta>
            <ta e="T293" id="Seg_12986" s="T291">Ich werde sie alle salzen.</ta>
            <ta e="T296" id="Seg_12987" s="T293">Dann werden sie (salzig?).</ta>
            <ta e="T300" id="Seg_12988" s="T296">Dann können sie gegessen werden.</ta>
            <ta e="T306" id="Seg_12989" s="T300">Der Häuptling saß, sehr betrunken. </ta>
            <ta e="T311" id="Seg_12990" s="T306">Er trank Vodka, er trank sehr viel.</ta>
            <ta e="T316" id="Seg_12991" s="T311">Er fiel hin, er fluchte mit aller Art von Wörtern.</ta>
            <ta e="T319" id="Seg_12992" s="T316">Ich setzte ihn hin.</ta>
            <ta e="T321" id="Seg_12993" s="T319">Sitz, sagte ich.</ta>
            <ta e="T323" id="Seg_12994" s="T321">Ich sagte:</ta>
            <ta e="T326" id="Seg_12995" s="T323">"Fluch nicht, sitz!</ta>
            <ta e="T330" id="Seg_12996" s="T326">Sitz und trinke Tee.</ta>
            <ta e="T335" id="Seg_12997" s="T330">Es ist Zucker, tu (etwas) in den Tee.</ta>
            <ta e="T337" id="Seg_12998" s="T335">Es sind Eier.</ta>
            <ta e="T341" id="Seg_12999" s="T337">Iss Brot!</ta>
            <ta e="T343" id="Seg_13000" s="T341">Iss Fleisch!</ta>
            <ta e="T345" id="Seg_13001" s="T343">Iss Fisch!</ta>
            <ta e="T348" id="Seg_13002" s="T345">Es liegt ein Ei, iss!</ta>
            <ta e="T352" id="Seg_13003" s="T348">Iss Butter, iss saure Sahne!</ta>
            <ta e="T355" id="Seg_13004" s="T352">Iss süße Milch!</ta>
            <ta e="T357" id="Seg_13005" s="T355">Iss Fisch!</ta>
            <ta e="T358" id="Seg_13006" s="T357">Genug.</ta>
            <ta e="T362" id="Seg_13007" s="T358">Ich muss Stiefel nähen.</ta>
            <ta e="T364" id="Seg_13008" s="T362">Einen Mantel nähen.</ta>
            <ta e="T370" id="Seg_13009" s="T364">Ich muss ein Hemd nähen, um mit zu nehmen / zu kaufen.</ta>
            <ta e="T375" id="Seg_13010" s="T370">Ich muss eine Hose nähen.</ta>
            <ta e="T382" id="Seg_13011" s="T375">Sonst hat mein Sohn nichts anzuziehen.</ta>
            <ta e="T384" id="Seg_13012" s="T382">Es gibt nichts.</ta>
            <ta e="T392" id="Seg_13013" s="T384">Ich werde nach Aginskoye gehen, und ich muss Brot kaufen.</ta>
            <ta e="T395" id="Seg_13014" s="T392">Ich muss Zucker kaufen.</ta>
            <ta e="T401" id="Seg_13015" s="T395">Ich muss Schrot für das Gewehr kaufen [=Schrot?]</ta>
            <ta e="T402" id="Seg_13016" s="T401">Ich muss …</ta>
            <ta e="T410" id="Seg_13017" s="T408">Also, Brot.</ta>
            <ta e="T413" id="Seg_13018" s="T410">Also, das ist alles (?).</ta>
            <ta e="T415" id="Seg_13019" s="T413">(…) ich dachte.</ta>
            <ta e="T419" id="Seg_13020" s="T415">Dann werde ich in die Taiga gehen. </ta>
            <ta e="T421" id="Seg_13021" s="T419">Also, das ist alles.</ta>
            <ta e="T427" id="Seg_13022" s="T421">Heute werden sie aus der Taiga kommen.</ta>
            <ta e="T430" id="Seg_13023" s="T427">Vielleicht werden sie Fleisch bringen.</ta>
            <ta e="T432" id="Seg_13024" s="T430">Sie werden Bärlauch bringen.</ta>
            <ta e="T437" id="Seg_13025" s="T432">Es muss geschnitten und gesalzen werden.</ta>
            <ta e="T447" id="Seg_13026" s="T437">Um Ei hinzugeben, um Sahne hinzugeben und Wasser zu gießen, um mit einem Löffel zu essen.</ta>
            <ta e="T456" id="Seg_13027" s="T447">Ich sollte gehen… gehen…</ta>
            <ta e="T458" id="Seg_13028" s="T456">Warte…</ta>
            <ta e="T461" id="Seg_13029" s="T458">Um zu pflügen, Getreide zu sähen.</ta>
            <ta e="T465" id="Seg_13030" s="T461">Dann wird es wachsen, und man sollte es (schneiden?).</ta>
            <ta e="T468" id="Seg_13031" s="T465">Dann sollte man es dreschen.</ta>
            <ta e="T478" id="Seg_13032" s="T468">Dann… Warte mal, hier (?) dann sollte man schwingen.</ta>
            <ta e="T483" id="Seg_13033" s="T478">Dann sollte man es backen.</ta>
            <ta e="T486" id="Seg_13034" s="T483">Dann sollte man es essen.</ta>
            <ta e="T488" id="Seg_13035" s="T486">Das ist alles…</ta>
            <ta e="T496" id="Seg_13036" s="T488">Heute ist ein großer Tag [=Feiertag], [es sind] sehr viele Menschen.</ta>
            <ta e="T498" id="Seg_13037" s="T496">Alle schön.</ta>
            <ta e="T500" id="Seg_13038" s="T498">Sie trinken Vodka.</ta>
            <ta e="T502" id="Seg_13039" s="T500">Sie spielen das Akkordeon.</ta>
            <ta e="T503" id="Seg_13040" s="T502">Sie tanzen.</ta>
            <ta e="T505" id="Seg_13041" s="T503">Sie singen Lied[er].</ta>
            <ta e="T507" id="Seg_13042" s="T505">Gute Frau!</ta>
            <ta e="T513" id="Seg_13043" s="T507">Sie backte gutes Brot.</ta>
            <ta e="T514" id="Seg_13044" s="T513">Weich!</ta>
            <ta e="T520" id="Seg_13045" s="T514">Nun esst!</ta>
            <ta e="T522" id="Seg_13046" s="T520">Vielleicht habt (ihr) Hunger.</ta>
            <ta e="T524" id="Seg_13047" s="T522">Nun werde ich aufstehen.</ta>
            <ta e="T527" id="Seg_13048" s="T524">Iss, sitz!</ta>
            <ta e="T530" id="Seg_13049" s="T527">Der Fette aß.</ta>
            <ta e="T533" id="Seg_13050" s="T530">Brot mit Vogelkirschen.</ta>
            <ta e="T535" id="Seg_13051" s="T533">Ich setzte Sahne.</ta>
            <ta e="T541" id="Seg_13052" s="T535">Isst, trinkt Milch!</ta>
            <ta e="T543" id="Seg_13053" s="T541">Das ist alles…</ta>
            <ta e="T547" id="Seg_13054" s="T543">Du schläfst noch, steh auf!</ta>
            <ta e="T554" id="Seg_13055" s="T547">Sonst bringe ich kaltes Wasser, ich schütte es auf dich.</ta>
            <ta e="T557" id="Seg_13056" s="T554">Dann wirst du springen.</ta>
            <ta e="T559" id="Seg_13057" s="T557">Das ist wahrscheinlich alles.</ta>
            <ta e="T564" id="Seg_13058" s="T559">Warte, warte, wie wir…</ta>
            <ta e="T566" id="Seg_13059" s="T564">(…).</ta>
            <ta e="T568" id="Seg_13060" s="T566">Hör nicht zu!</ta>
            <ta e="T570" id="Seg_13061" s="T568">(Schneide?) nicht.</ta>
            <ta e="T571" id="Seg_13062" s="T570">Oh Gott.</ta>
            <ta e="T575" id="Seg_13063" s="T573">Sprich nicht!</ta>
            <ta e="T579" id="Seg_13064" s="T577">Fluch nicht!</ta>
            <ta e="T584" id="Seg_13065" s="T582">Ruf nicht!</ta>
            <ta e="T588" id="Seg_13066" s="T586">Deine Rotze läuft.</ta>
            <ta e="T590" id="Seg_13067" s="T588">Wisch Deine Rotze weg!</ta>
            <ta e="T594" id="Seg_13068" s="T592">Lauf nicht!</ta>
            <ta e="T596" id="Seg_13069" s="T594">Spring nicht!</ta>
            <ta e="T598" id="Seg_13070" s="T596">Sing nicht!</ta>
            <ta e="T600" id="Seg_13071" s="T598">Sehr gut.</ta>
            <ta e="T602" id="Seg_13072" s="T600">Sehr schön.</ta>
            <ta e="T604" id="Seg_13073" s="T602">Sehr rot.</ta>
            <ta e="T605" id="Seg_13074" s="T604">Sehr.</ta>
            <ta e="T609" id="Seg_13075" s="T606">(Fett?), sehr lang.</ta>
            <ta e="T614" id="Seg_13076" s="T612">Sehr klein.</ta>
            <ta e="T618" id="Seg_13077" s="T614">Also, nun, das ist wahrscheinlich alles.</ta>
            <ta e="T621" id="Seg_13078" s="T618">Vielleicht hast du ein Schaal gestohlen.</ta>
            <ta e="T623" id="Seg_13079" s="T621">Warum hast du ihn gebracht?</ta>
            <ta e="T626" id="Seg_13080" s="T623">Ich werde ihn nicht nehmen.</ta>
            <ta e="T633" id="Seg_13081" s="T626">Du solltest nicht stehlen, sonst wird es nicht gut.</ta>
            <ta e="T638" id="Seg_13082" s="T633">Ich habe mich an der Hand geschnitten, Blut fließt</ta>
            <ta e="T642" id="Seg_13083" s="T638">Bring mir etwas, um meine Hand zu verbinden.</ta>
            <ta e="T650" id="Seg_13084" s="T645">Warum hast du ein Messer genommen?</ta>
            <ta e="T654" id="Seg_13085" s="T650">Du brauchst mehr…</ta>
            <ta e="T658" id="Seg_13086" s="T654">Es windet, es regnet.</ta>
            <ta e="T662" id="Seg_13087" s="T658">Es schneet und Wind.</ta>
            <ta e="T664" id="Seg_13088" s="T662">Sehr kalt.</ta>
            <ta e="T667" id="Seg_13089" s="T664">Mich fror es.</ta>
            <ta e="T670" id="Seg_13090" s="T667">Und mich frierte es sehr. [?]</ta>
            <ta e="T672" id="Seg_13091" s="T670">Meine Füße froren.</ta>
            <ta e="T674" id="Seg_13092" s="T672">Meine Hände froren.</ta>
            <ta e="T677" id="Seg_13093" s="T674">Mein Gesicht fror.</ta>
            <ta e="T682" id="Seg_13094" s="T677">Heute ist ein schöner Tag.</ta>
            <ta e="T685" id="Seg_13095" s="T682">Die Sonne scheint.</ta>
            <ta e="T688" id="Seg_13096" s="T685">Es ist nichts dort oben.</ta>
            <ta e="T691" id="Seg_13097" s="T688">Ein sehr guter Tag.</ta>
            <ta e="T694" id="Seg_13098" s="T691">Menschen arbeiten.</ta>
            <ta e="T696" id="Seg_13099" s="T694">Sie pflanzen Kartoffel.</ta>
            <ta e="T699" id="Seg_13100" s="T696">Pferde pflügen.</ta>
            <ta e="T703" id="Seg_13101" s="T699">Man sollte das Gras schneiden gehen.</ta>
            <ta e="T706" id="Seg_13102" s="T703">Man sollte die Sense nehmen.</ta>
            <ta e="T709" id="Seg_13103" s="T706">Man sollte Wasser nehmen.</ta>
            <ta e="T718" id="Seg_13104" s="T709">Man sollte Milch nehmen, Brot nehmen, Salz nehmen, Eier nehmen.</ta>
            <ta e="T722" id="Seg_13105" s="T718">Man sollte den Sensen-Stein nehmen.</ta>
            <ta e="T725" id="Seg_13106" s="T722">Heute habe ich einen Zobel getötet.</ta>
            <ta e="T727" id="Seg_13107" s="T725">Ich habe ein Eichhörnchen getötet.</ta>
            <ta e="T729" id="Seg_13108" s="T727">Ich habe einen Bären getötet.</ta>
            <ta e="T731" id="Seg_13109" s="T729">Ich habe ein Reh getötet.</ta>
            <ta e="T733" id="Seg_13110" s="T731">Ich habe einen Hengst getötet.</ta>
            <ta e="T736" id="Seg_13111" s="T733">Ich habe eine Ziege mit einem Gewehr erschossen.</ta>
            <ta e="T740" id="Seg_13112" s="T736">Kühe fressen Gras.</ta>
            <ta e="T742" id="Seg_13113" s="T740">Sie laufen dort herum.</ta>
            <ta e="T747" id="Seg_13114" s="T742">Dieser Mann wird sie nach Hause bringen.</ta>
            <ta e="T748" id="Seg_13115" s="T747">Das ist alles.</ta>
            <ta e="T751" id="Seg_13116" s="T748">Ich muss das Kind waschen.</ta>
            <ta e="T757" id="Seg_13117" s="T751">Ich muss … mein Kind weint.</ta>
            <ta e="T760" id="Seg_13118" s="T757">Ich sollte es auswickeln.</ta>
            <ta e="T763" id="Seg_13119" s="T760">Ich sollte [ihm/ihr] etwas zu essen geben.</ta>
            <ta e="T766" id="Seg_13120" s="T763">Ich sollte ihn/sie schlafen legen.</ta>
            <ta e="T773" id="Seg_13121" s="T770">Wiege das Kind ein!</ta>
            <ta e="T775" id="Seg_13122" s="T773">Lass ihn/sie schlafen.</ta>
            <ta e="T779" id="Seg_13123" s="T775">Sitz, nun werde ich Wasser holen.</ta>
            <ta e="T781" id="Seg_13124" s="T779">Ich werde Wasser holen.</ta>
            <ta e="T783" id="Seg_13125" s="T781">Dann wirst du trinken.</ta>
            <ta e="T785" id="Seg_13126" s="T783">Geh nicht!</ta>
            <ta e="T789" id="Seg_13127" s="T785">Und dann wirst du nach Hause gehen.</ta>
            <ta e="T792" id="Seg_13128" s="T789">Nun werde ich Blut backen.</ta>
            <ta e="T801" id="Seg_13129" s="T792">Ich sollte Milch dort gießen, hinzutun Butter, oder hinzutun Fett.</ta>
            <ta e="T803" id="Seg_13130" s="T801">Nun werden wir essen.</ta>
            <ta e="T807" id="Seg_13131" s="T803">Tu nicht (?), es ist warm.</ta>
            <ta e="T811" id="Seg_13132" s="T807">Iss es mit Brot!</ta>
            <ta e="T814" id="Seg_13133" s="T811">(…) kam.</ta>
            <ta e="T816" id="Seg_13134" s="T814">Weisses Haar.</ta>
            <ta e="T819" id="Seg_13135" s="T816">Sie selbst ist schlank.</ta>
            <ta e="T825" id="Seg_13136" s="T819">Und der Man kam, sehr fett.</ta>
            <ta e="T827" id="Seg_13137" s="T825">Und gebe. (?)</ta>
            <ta e="T829" id="Seg_13138" s="T827">Sein/Ihr Freund kam.</ta>
            <ta e="T830" id="Seg_13139" s="T829">Das ist alles.</ta>
            <ta e="T834" id="Seg_13140" s="T830">Sie haben nicht Kartoffel (gepflanzt?).</ta>
            <ta e="T838" id="Seg_13141" s="T834">Mein Vater hat für sie gepflanzt.</ta>
            <ta e="T845" id="Seg_13142" s="T838">Sie hatten viel Fleisch, sie aßen Fleisch.</ta>
            <ta e="T848" id="Seg_13143" s="T845">Es war wenig Brot.</ta>
            <ta e="T854" id="Seg_13144" s="T848">Und es gab nichts anderes.</ta>
            <ta e="T858" id="Seg_13145" s="T854">Warum sitzt du, ohne zu sprechen.</ta>
            <ta e="T860" id="Seg_13146" s="T858">Sag etwas!</ta>
            <ta e="T867" id="Seg_13147" s="T860">Mein Herz schmerzt wahrscheinlich.</ta>
            <ta e="T870" id="Seg_13148" s="T867">Du sagst nichts.</ta>
            <ta e="T871" id="Seg_13149" s="T870">Sprich!</ta>
            <ta e="T872" id="Seg_13150" s="T871">Das ist alles.</ta>
            <ta e="T876" id="Seg_13151" s="T872">Wo gehst du hin?</ta>
            <ta e="T880" id="Seg_13152" s="T876">Wo warst du, komm her!</ta>
            <ta e="T883" id="Seg_13153" s="T880">Erzähl mir etwas!</ta>
            <ta e="T885" id="Seg_13154" s="T883">Ich habe wieder vergessen.</ta>
            <ta e="T888" id="Seg_13155" s="T885">Ein Mann lebte mit einer Frau.</ta>
            <ta e="T891" id="Seg_13156" s="T888">Sie hatten Kinder.</ta>
            <ta e="T894" id="Seg_13157" s="T891">Ein Mädchen und ein Junge.</ta>
            <ta e="T896" id="Seg_13158" s="T894">Sie hatten ein Haus.</ta>
            <ta e="T898" id="Seg_13159" s="T896">Sie hatten eine Kuh.</ta>
            <ta e="T902" id="Seg_13160" s="T898">Die Frau molk die Kuh.</ta>
            <ta e="T904" id="Seg_13161" s="T902">Gab den Kindern [Milch].</ta>
            <ta e="T906" id="Seg_13162" s="T904">Gab ihnen Brot.</ta>
            <ta e="T908" id="Seg_13163" s="T906">Sie essen.</ta>
            <ta e="T909" id="Seg_13164" s="T908">Das ist alles.</ta>
            <ta e="T913" id="Seg_13165" s="T909">Diese zwei Männer kamen.</ta>
            <ta e="T920" id="Seg_13166" s="T913">Der Man und das Mädchen haben dir vielleicht etwas gebracht.</ta>
            <ta e="T922" id="Seg_13167" s="T920">Sie brachten Zucker.</ta>
            <ta e="T927" id="Seg_13168" s="T922">Du wirst Tee trinken.</ta>
            <ta e="T931" id="Seg_13169" s="T927">Vielleicht haben Sie dir Süßigkeiten gebracht.</ta>
            <ta e="T934" id="Seg_13170" s="T931">Vielleicht haben sie ein Hemd gebracht.</ta>
            <ta e="T938" id="Seg_13171" s="T934">Vielleicht haben sie dir eine Hose gebracht.</ta>
            <ta e="T941" id="Seg_13172" s="T938">Das Haus des Dorfoberhauptes ist groß.</ta>
            <ta e="T943" id="Seg_13173" s="T941">Eine Menge Leute.</ta>
            <ta e="T949" id="Seg_13174" s="T943">Zwei Jungen, zwei Mädchen.</ta>
            <ta e="T951" id="Seg_13175" s="T949">Eine Frau und ein Mann.</ta>
            <ta e="T953" id="Seg_13176" s="T951">Er selbst, seine Frau.</ta>
            <ta e="T956" id="Seg_13177" s="T953">Seine zwei Hunde.</ta>
            <ta e="T966" id="Seg_13178" s="T956">Ein guter Hund, einer mit drei Beinen, und er hat das vierte Bein nicht.</ta>
            <ta e="T969" id="Seg_13179" s="T966">Kein viertes Bein.</ta>
            <ta e="T975" id="Seg_13180" s="T969">Du schlägst mich, aber womit schlägst du mich?</ta>
            <ta e="T982" id="Seg_13181" s="T975">Schlag nicht mit der Faust, sonst werde ich verletzt.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-PKZ">
            <ta e="T54" id="Seg_13182" s="T51">[GVY]: Donner (p. 25a) has kajaʔ from Turkic kajak.</ta>
            <ta e="T86" id="Seg_13183" s="T85">[GVY]: kük 'grün etc.; pearl' (Donner 35a?)</ta>
            <ta e="T94" id="Seg_13184" s="T90">(nʼ) dʼăbaktərizittə? Nu - просто ну. Было šonəgaʔi tʼăbaktərzittə. Было "A kamas man are coming to speak".</ta>
            <ta e="T117" id="Seg_13185" s="T115">[GVY]: "Be quiet"?</ta>
            <ta e="T219" id="Seg_13186" s="T218">[KIT]: ? derivation bazəjzəbi?</ta>
            <ta e="T236" id="Seg_13187" s="T232">[KIT]: dʼü?; plough ’tajər’ </ta>
            <ta e="T260" id="Seg_13188" s="T256">[KlT]: (intended =yesterday!)</ta>
            <ta e="T319" id="Seg_13189" s="T316">[GVY]: Stem amnoːl- 'sich setzen, wohnen' probably used instead of amnəʔl- 'setzen, stellen' (both Donner 5a)</ta>
            <ta e="T335" id="Seg_13190" s="T330">[GVY:] The form bütnə might reflect the old shape of this Samoyed stem büʔ (&lt; *büt), However, rhis is so far the only attestation of this stem with a final consonant, and the form bünə is very frequent in PKZ's speech.</ta>
            <ta e="T355" id="Seg_13191" s="T352">[GVY:] It is not clear what she means here, maybe just "tasty milk".</ta>
            <ta e="T362" id="Seg_13192" s="T358">[GVY:] maybe here and in the next setences she means 'seu up, mend' ('зашить')</ta>
            <ta e="T498" id="Seg_13193" s="T496"> || kuvazəʔi?</ta>
            <ta e="T522" id="Seg_13194" s="T520">[GVY]: 3rd person plural instead of the expected 2nd; but at the end there is some -hi, clearly enough.</ta>
            <ta e="T524" id="Seg_13195" s="T522">[KIT]: should be uʔbdlam?</ta>
            <ta e="T568" id="Seg_13196" s="T566">[KIT]: Doublecheck the base form.</ta>
            <ta e="T670" id="Seg_13197" s="T667">[GVY:] kămbiam = kămbial.</ta>
            <ta e="T672" id="Seg_13198" s="T670">[KlT]: (=foot)</ta>
            <ta e="T674" id="Seg_13199" s="T672">[KlT]: hand?</ta>
            <ta e="T696" id="Seg_13200" s="T694">[GVY]: amlaʔbəʔjə 'they are eating' maybe instead of amnəʔlaʔbəʔjə 'they are planting'.</ta>
            <ta e="T699" id="Seg_13201" s="T696">[KIT]: Plough ’tajər’.</ta>
            <ta e="T722" id="Seg_13202" s="T718">[KlT]: (=sharpening stone)</ta>
            <ta e="T803" id="Seg_13203" s="T801">[KlT:] DU!</ta>
            <ta e="T814" id="Seg_13204" s="T811">Koʔbdo K/P.</ta>
            <ta e="T827" id="Seg_13205" s="T825">[GVY:] Unclear.</ta>
            <ta e="T834" id="Seg_13206" s="T830">[GVY]: amnobiʔi=amnəʔbiʔi plant-PST-3PL </ta>
            <ta e="T838" id="Seg_13207" s="T834">[GVY]: amnolbi = amnəʔbi [plant-PST-[3SG]]</ta>
            <ta e="T870" id="Seg_13208" s="T867">[GVY:] Or "for some reason, you aren't speaking'</ta>
            <ta e="T951" id="Seg_13209" s="T949">[KlT]: (his parents?) [AAV] Or "wives with husbands"? (chairman's daughters with their husbands?)</ta>
            <ta e="T966" id="Seg_13210" s="T956">[KIT]: Cardinal numbers instead of expected ordinal forms.</ta>
            <ta e="T982" id="Seg_13211" s="T975">[KIT]: Muzəruk ’fist’ (Janurik), doublecheck the form!</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T984" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T985" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T979" />
            <conversion-tli id="T980" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T983" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-KA"
                          name="ref"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KA"
                          name="ts"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KA"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KA"
                          type="t" />
         <conversion-tier category="CS"
                          display-name="CS-KA"
                          name="CS"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KA"
                          name="fr"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KA"
                          name="fe"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KA"
                          name="fg"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KA"
                          name="nt"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-PKZ"
                          name="ref"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PKZ"
                          name="ts"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PKZ"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PKZ"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PKZ"
                          name="mb"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PKZ"
                          name="mp"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PKZ"
                          name="ge"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PKZ"
                          name="gr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PKZ"
                          name="mc"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PKZ"
                          name="ps"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PKZ"
                          name="SyF"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PKZ"
                          name="SeR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PKZ"
                          name="IST"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PKZ"
                          name="BOR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PKZ"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PKZ"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PKZ"
                          name="CS"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PKZ"
                          name="fr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PKZ"
                          name="fe"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PKZ"
                          name="fg"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PKZ"
                          name="nt"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
