<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDDC82C7B7-300C-FB9A-4392-7C98F8D95AB2">
   <head>
      <meta-information>
         <project-name>Kamas</project-name>
         <transcription-name>PKZ_197008_09341_2a</transcription-name>
         <referenced-file url="PKZ_197008_09341-2a.wav" />
         <referenced-file url="PKZ_197008_09341-2a.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_197008_09341-2a\PKZ_197008_09341-2a.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">491</ud-information>
            <ud-information attribute-name="# HIAT:w">240</ud-information>
            <ud-information attribute-name="# e">266</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">27</ud-information>
            <ud-information attribute-name="# HIAT:u">75</ud-information>
            <ud-information attribute-name="# sc">142</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="KA">
            <abbreviation>KA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="3.34" type="appl" />
         <tli id="T2" time="3.93125" type="appl" />
         <tli id="T3" time="4.5225" type="appl" />
         <tli id="T4" time="5.11375" type="appl" />
         <tli id="T5" time="5.705" type="appl" />
         <tli id="T6" time="6.296250000000001" type="appl" />
         <tli id="T7" time="6.8875" type="appl" />
         <tli id="T8" time="7.04" type="appl" />
         <tli id="T9" time="7.385" type="appl" />
         <tli id="T10" time="7.47875" type="appl" />
         <tli id="T11" time="7.7299999999999995" type="appl" />
         <tli id="T12" time="8.07" type="appl" />
         <tli id="T13" time="8.075" type="appl" />
         <tli id="T14" time="8.42" type="appl" />
         <tli id="T15" time="8.765" type="appl" />
         <tli id="T16" time="9.11" type="appl" />
         <tli id="T17" time="9.455" type="appl" />
         <tli id="T18" time="9.799999999999999" type="appl" />
         <tli id="T19" time="10.145" type="appl" />
         <tli id="T20" time="10.489999999999998" type="appl" />
         <tli id="T21" time="10.834999999999999" type="appl" />
         <tli id="T22" time="11.18" type="appl" />
         <tli id="T23" time="11.524999999999999" type="appl" />
         <tli id="T24" time="11.87" type="appl" />
         <tli id="T25" time="12.44" type="appl" />
         <tli id="T333" />
         <tli id="T334" />
         <tli id="T26" time="17.88" type="appl" />
         <tli id="T27" time="291.97" type="appl" />
         <tli id="T28" time="292.5666666666667" type="appl" />
         <tli id="T29" time="293.16333333333336" type="appl" />
         <tli id="T30" time="293.76" type="appl" />
         <tli id="T31" time="294.3566666666667" type="appl" />
         <tli id="T32" time="294.9533333333334" type="appl" />
         <tli id="T33" time="295.55" type="appl" />
         <tli id="T34" time="295.8" type="appl" />
         <tli id="T35" time="296.418" type="appl" />
         <tli id="T36" time="297.036" type="appl" />
         <tli id="T37" time="297.654" type="appl" />
         <tli id="T38" time="298.272" type="appl" />
         <tli id="T39" time="298.89" type="appl" />
         <tli id="T40" time="299.83" type="appl" />
         <tli id="T41" time="300.28666666666663" type="appl" />
         <tli id="T42" time="300.74333333333334" type="appl" />
         <tli id="T43" time="301.2" type="appl" />
         <tli id="T44" time="301.65666666666664" type="appl" />
         <tli id="T45" time="302.11333333333334" type="appl" />
         <tli id="T46" time="302.57" type="appl" />
         <tli id="T47" time="302.72" type="appl" />
         <tli id="T48" time="303.20000000000005" type="appl" />
         <tli id="T49" time="303.68" type="appl" />
         <tli id="T50" time="304.16" type="appl" />
         <tli id="T51" time="305.11" type="appl" />
         <tli id="T52" time="305.55" type="appl" />
         <tli id="T53" time="305.99" type="appl" />
         <tli id="T54" time="306.43" type="appl" />
         <tli id="T55" time="306.87" type="appl" />
         <tli id="T56" time="307.03" type="appl" />
         <tli id="T57" time="307.31" type="appl" />
         <tli id="T58" time="307.775" type="appl" />
         <tli id="T59" time="308.52" type="appl" />
         <tli id="T60" time="320.37" type="appl" />
         <tli id="T61" time="321.6066666666667" type="appl" />
         <tli id="T62" time="322.84333333333336" type="appl" />
         <tli id="T63" time="324.08000000000004" type="appl" />
         <tli id="T64" time="325.31666666666666" type="appl" />
         <tli id="T65" time="326.55333333333334" type="appl" />
         <tli id="T66" time="327.79" type="appl" />
         <tli id="T67" time="328.07" type="appl" />
         <tli id="T68" time="329.34" type="appl" />
         <tli id="T69" time="330.61" type="appl" />
         <tli id="T70" time="331.88" type="appl" />
         <tli id="T71" time="333.15" type="appl" />
         <tli id="T72" time="334.42" type="appl" />
         <tli id="T73" time="335.69" type="appl" />
         <tli id="T74" time="335.88" type="appl" />
         <tli id="T75" time="337.202" type="appl" />
         <tli id="T76" time="338.524" type="appl" />
         <tli id="T77" time="339.846" type="appl" />
         <tli id="T78" time="341.168" type="appl" />
         <tli id="T79" time="342.49" type="appl" />
         <tli id="T80" time="342.74" type="appl" />
         <tli id="T81" time="343.87983333333335" type="appl" />
         <tli id="T82" time="345.0196666666667" type="appl" />
         <tli id="T83" time="346.1595" type="appl" />
         <tli id="T84" time="347.2993333333333" type="appl" />
         <tli id="T85" time="348.43916666666667" type="appl" />
         <tli id="T86" time="349.579" type="appl" />
         <tli id="T87" time="349.69" type="appl" />
         <tli id="T88" time="351.75" type="appl" />
         <tli id="T89" time="353.81" type="appl" />
         <tli id="T90" time="355.87" type="appl" />
         <tli id="T91" time="357.93" type="appl" />
         <tli id="T92" time="359.37" type="appl" />
         <tli id="T335" />
         <tli id="T336" />
         <tli id="T93" time="396.99" type="appl" />
         <tli id="T94" time="397.23" type="appl" />
         <tli id="T95" time="398.06142857142856" type="appl" />
         <tli id="T96" time="398.89285714285717" type="appl" />
         <tli id="T97" time="399.7242857142857" type="appl" />
         <tli id="T98" time="400.5557142857143" type="appl" />
         <tli id="T99" time="401.38714285714286" type="appl" />
         <tli id="T100" time="402.21857142857147" type="appl" />
         <tli id="T101" time="403.05" type="appl" />
         <tli id="T102" time="403.88142857142856" type="appl" />
         <tli id="T103" time="404.71285714285716" type="appl" />
         <tli id="T104" time="405.5442857142857" type="appl" />
         <tli id="T105" time="406.3757142857143" type="appl" />
         <tli id="T106" time="407.20714285714286" type="appl" />
         <tli id="T107" time="408.03857142857146" type="appl" />
         <tli id="T108" time="408.84" type="appl" />
         <tli id="T109" time="408.87" type="appl" />
         <tli id="T110" time="410.01" type="appl" />
         <tli id="T111" time="410.54" type="appl" />
         <tli id="T112" time="411.28" type="appl" />
         <tli id="T113" time="415.2" type="appl" />
         <tli id="T337" />
         <tli id="T338" />
         <tli id="T114" time="417.75" type="appl" />
         <tli id="T115" time="417.91" type="appl" />
         <tli id="T116" time="418.6" type="appl" />
         <tli id="T117" time="418.96000000000004" type="appl" />
         <tli id="T118" time="419.32000000000005" type="appl" />
         <tli id="T119" time="419.68" type="appl" />
         <tli id="T120" time="420.04" type="appl" />
         <tli id="T121" time="420.40000000000003" type="appl" />
         <tli id="T122" time="420.76" type="appl" />
         <tli id="T123" time="421.12" type="appl" />
         <tli id="T124" time="421.48" type="appl" />
         <tli id="T125" time="422.82" type="appl" />
         <tli id="T126" time="424.2824112249618" />
         <tli id="T127" time="426.17" type="appl" />
         <tli id="T128" time="427.355" type="appl" />
         <tli id="T129" time="428.54" type="appl" />
         <tli id="T130" time="428.61" type="appl" />
         <tli id="T131" time="429.2766666666667" type="appl" />
         <tli id="T373" time="429.562380952381" type="intp" />
         <tli id="T132" time="429.9433333333333" type="appl" />
         <tli id="T133" time="430.61" type="appl" />
         <tli id="T134" time="430.64" type="appl" />
         <tli id="T135" time="433.88" type="appl" />
         <tli id="T136" time="434.61333333333334" type="appl" />
         <tli id="T137" time="434.99" type="appl" />
         <tli id="T138" time="435.34666666666664" type="appl" />
         <tli id="T139" time="436.08" type="appl" />
         <tli id="T140" time="436.695" type="appl" />
         <tli id="T141" time="437.31" type="appl" />
         <tli id="T142" time="437.925" type="appl" />
         <tli id="T143" time="438.54" type="appl" />
         <tli id="T144" time="438.63" type="appl" />
         <tli id="T145" time="439.29" type="appl" />
         <tli id="T146" time="439.88" type="appl" />
         <tli id="T339" />
         <tli id="T340" />
         <tli id="T147" time="443.91" type="appl" />
         <tli id="T148" time="444.45" type="appl" />
         <tli id="T341" />
         <tli id="T342" />
         <tli id="T149" time="446.4" type="appl" />
         <tli id="T150" time="446.825" type="appl" />
         <tli id="T151" time="447.25" type="appl" />
         <tli id="T152" time="447.675" type="appl" />
         <tli id="T153" time="448.1" type="appl" />
         <tli id="T154" time="448.942163894087" />
         <tli id="T155" time="449.734" type="appl" />
         <tli id="T156" time="450.58799999999997" type="appl" />
         <tli id="T157" time="451.442" type="appl" />
         <tli id="T158" time="452.296" type="appl" />
         <tli id="T159" time="453.15" type="appl" />
         <tli id="T160" time="454.06877914214255" />
         <tli id="T161" time="454.7954385206055" />
         <tli id="T162" time="455.108768711319" />
         <tli id="T163" time="456.11571428571426" type="appl" />
         <tli id="T164" time="456.64428571428573" type="appl" />
         <tli id="T165" time="457.17285714285714" type="appl" />
         <tli id="T166" time="457.7014285714286" type="appl" />
         <tli id="T167" time="458.23" type="appl" />
         <tli id="T168" time="458.42873541292056" />
         <tli id="T169" time="459.52133333333336" type="appl" />
         <tli id="T170" time="460.44266666666664" type="appl" />
         <tli id="T171" time="461.364" type="appl" />
         <tli id="T172" time="461.97" type="appl" />
         <tli id="T173" time="462.72571428571433" type="appl" />
         <tli id="T174" time="463.4814285714286" type="appl" />
         <tli id="T175" time="464.2371428571429" type="appl" />
         <tli id="T176" time="464.99285714285713" type="appl" />
         <tli id="T177" time="465.74857142857144" type="appl" />
         <tli id="T178" time="466.5042857142857" type="appl" />
         <tli id="T179" time="467.5286441432141" />
         <tli id="T180" time="467.984" type="appl" />
         <tli id="T343" />
         <tli id="T344" />
         <tli id="T181" time="473.194" type="appl" />
         <tli id="T182" time="473.29" type="appl" />
         <tli id="T345" />
         <tli id="T346" />
         <tli id="T183" time="483.63" type="appl" />
         <tli id="T184" time="484.48" type="appl" />
         <tli id="T347" />
         <tli id="T348" />
         <tli id="T185" time="485.69" type="appl" />
         <tli id="T349" />
         <tli id="T350" />
         <tli id="T186" time="489.7" type="appl" />
         <tli id="T187" time="490.2" type="appl" />
         <tli id="T351" />
         <tli id="T352" />
         <tli id="T188" time="493.14" type="appl" />
         <tli id="T189" time="493.28" type="appl" />
         <tli id="T190" time="493.61749999999995" type="appl" />
         <tli id="T191" time="493.955" type="appl" />
         <tli id="T192" time="494.29249999999996" type="appl" />
         <tli id="T193" time="494.63" type="appl" />
         <tli id="T194" time="494.9675" type="appl" />
         <tli id="T195" time="495.30499999999995" type="appl" />
         <tli id="T196" time="495.6425" type="appl" />
         <tli id="T197" time="495.97999999999996" type="appl" />
         <tli id="T198" time="496.3175" type="appl" />
         <tli id="T199" time="496.655" type="appl" />
         <tli id="T200" time="496.9925" type="appl" />
         <tli id="T201" time="497.33" type="appl" />
         <tli id="T202" time="498.04" type="appl" />
         <tli id="T203" time="499.44" type="appl" />
         <tli id="T204" time="499.45" type="appl" />
         <tli id="T205" time="499.9875" type="appl" />
         <tli id="T206" time="500.525" type="appl" />
         <tli id="T207" time="501.0625" type="appl" />
         <tli id="T208" time="501.6" type="appl" />
         <tli id="T209" time="501.68" type="appl" />
         <tli id="T210" time="502.37666666666667" type="appl" />
         <tli id="T211" time="503.0733333333333" type="appl" />
         <tli id="T212" time="503.77" type="appl" />
         <tli id="T213" time="503.9" type="appl" />
         <tli id="T214" time="504.435" type="appl" />
         <tli id="T215" time="504.97" type="appl" />
         <tli id="T216" time="505.505" type="appl" />
         <tli id="T217" time="506.1066426326377" />
         <tli id="T218" time="506.1133092324401" />
         <tli id="T219" time="506.5625" type="appl" />
         <tli id="T220" time="507.04499999999996" type="appl" />
         <tli id="T221" time="507.5275" type="appl" />
         <tli id="T222" time="508.01" type="appl" />
         <tli id="T223" time="508.04" type="appl" />
         <tli id="T224" time="508.67333333333335" type="appl" />
         <tli id="T225" time="509.3066666666667" type="appl" />
         <tli id="T226" time="509.94" type="appl" />
         <tli id="T227" time="510.34" type="appl" />
         <tli id="T353" />
         <tli id="T354" />
         <tli id="T228" time="519.45" type="appl" />
         <tli id="T229" time="519.79" type="appl" />
         <tli id="T355" />
         <tli id="T356" />
         <tli id="T230" time="521.74" type="appl" />
         <tli id="T231" time="523.78" type="appl" />
         <tli id="T357" />
         <tli id="T358" />
         <tli id="T232" time="527.29" type="appl" />
         <tli id="T233" time="527.32" type="appl" />
         <tli id="T234" time="527.74625" type="appl" />
         <tli id="T235" time="528.1725" type="appl" />
         <tli id="T236" time="528.59875" type="appl" />
         <tli id="T237" time="529.0250000000001" type="appl" />
         <tli id="T238" time="529.4512500000001" type="appl" />
         <tli id="T239" time="529.8775" type="appl" />
         <tli id="T240" time="530.30375" type="appl" />
         <tli id="T241" time="530.8033220147308" />
         <tli id="T242" time="530.8099886145334" />
         <tli id="T359" />
         <tli id="T360" />
         <tli id="T243" time="532.04" type="appl" />
         <tli id="T244" time="532.23" type="appl" />
         <tli id="T245" time="532.845" type="appl" />
         <tli id="T246" time="533.46" type="appl" />
         <tli id="T247" time="534.075" type="appl" />
         <tli id="T248" time="534.6899999999999" type="appl" />
         <tli id="T249" time="535.305" type="appl" />
         <tli id="T250" time="535.92" type="appl" />
         <tli id="T251" time="536.31" type="appl" />
         <tli id="T252" time="537.5799999999999" type="appl" />
         <tli id="T253" time="538.85" type="appl" />
         <tli id="T254" time="539.26" type="appl" />
         <tli id="T255" time="540.155" type="appl" />
         <tli id="T256" time="541.05" type="appl" />
         <tli id="T257" time="541.47" type="appl" />
         <tli id="T361" />
         <tli id="T362" />
         <tli id="T258" time="554.66" type="appl" />
         <tli id="T259" time="555.76" type="appl" />
         <tli id="T363" />
         <tli id="T364" />
         <tli id="T260" time="559.02" type="appl" />
         <tli id="T261" time="559.29" type="appl" />
         <tli id="T262" time="560.1039999999999" type="appl" />
         <tli id="T263" time="560.918" type="appl" />
         <tli id="T264" time="561.732" type="appl" />
         <tli id="T265" time="562.546" type="appl" />
         <tli id="T266" time="563.36" type="appl" />
         <tli id="T267" time="566.17" type="appl" />
         <tli id="T268" time="566.9333333333333" type="appl" />
         <tli id="T269" time="567.6966666666667" type="appl" />
         <tli id="T270" time="568.46" type="appl" />
         <tli id="T271" time="568.54" type="appl" />
         <tli id="T272" time="569.2328" type="appl" />
         <tli id="T273" time="569.9256" type="appl" />
         <tli id="T274" time="570.6184" type="appl" />
         <tli id="T275" time="571.3112" type="appl" />
         <tli id="T276" time="572.004" type="appl" />
         <tli id="T277" time="572.07" type="appl" />
         <tli id="T278" time="572.9025" type="appl" />
         <tli id="T279" time="573.735" type="appl" />
         <tli id="T280" time="574.5675" type="appl" />
         <tli id="T281" time="575.4" type="appl" />
         <tli id="T282" time="575.51" type="appl" />
         <tli id="T283" time="576.478" type="appl" />
         <tli id="T284" time="577.446" type="appl" />
         <tli id="T285" time="578.414" type="appl" />
         <tli id="T286" time="579.382" type="appl" />
         <tli id="T287" time="580.35" type="appl" />
         <tli id="T288" time="581.4408349670426" />
         <tli id="T289" time="582.43" type="appl" />
         <tli id="T365" />
         <tli id="T366" />
         <tli id="T290" time="586.07" type="appl" />
         <tli id="T291" time="586.38" type="appl" />
         <tli id="T367" />
         <tli id="T368" />
         <tli id="T292" time="593.94" type="appl" />
         <tli id="T293" time="594.14" type="appl" />
         <tli id="T294" time="594.6733333333333" type="appl" />
         <tli id="T295" time="595.2066666666667" type="appl" />
         <tli id="T296" time="595.74" type="appl" />
         <tli id="T297" time="595.86" type="appl" />
         <tli id="T298" time="596.1" type="appl" />
         <tli id="T299" time="596.386" type="appl" />
         <tli id="T300" time="596.912" type="appl" />
         <tli id="T301" time="597.25" type="appl" />
         <tli id="T302" time="597.438" type="appl" />
         <tli id="T303" time="597.964" type="appl" />
         <tli id="T304" time="598.28" type="appl" />
         <tli id="T305" time="598.3806650649738" />
         <tli id="T306" time="602.05" type="appl" />
         <tli id="T307" time="603.16" type="appl" />
         <tli id="T369" />
         <tli id="T370" />
         <tli id="T308" time="606.66" type="appl" />
         <tli id="T309" time="607.2116666666666" type="appl" />
         <tli id="T310" time="607.7633333333333" type="appl" />
         <tli id="T311" time="608.315" type="appl" />
         <tli id="T312" time="608.8666666666667" type="appl" />
         <tli id="T313" time="609.4183333333333" type="appl" />
         <tli id="T314" time="609.97" type="appl" />
         <tli id="T315" time="611.45" type="appl" />
         <tli id="T316" time="612.5466666666667" type="appl" />
         <tli id="T374" time="613.0166666666667" type="intp" />
         <tli id="T317" time="613.6433333333333" type="appl" />
         <tli id="T318" time="614.74" type="appl" />
         <tli id="T319" time="618.82" type="appl" />
         <tli id="T320" time="619.835" type="appl" />
         <tli id="T321" time="620.85" type="appl" />
         <tli id="T322" time="621.865" type="appl" />
         <tli id="T323" time="622.88" type="appl" />
         <tli id="T324" time="623.895" type="appl" />
         <tli id="T325" time="624.91" type="appl" />
         <tli id="T326" time="625.24" type="appl" />
         <tli id="T371" />
         <tli id="T372" />
         <tli id="T327" time="631.82" type="appl" />
         <tli id="T328" time="632.08" type="appl" />
         <tli id="T329" time="632.7750000000001" type="appl" />
         <tli id="T330" time="633.47" type="appl" />
         <tli id="T331" time="634.165" type="appl" />
         <tli id="T332" time="634.86" type="appl" />
         <tli id="T0" time="637.686" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-PKZ"
                      id="tx-PKZ"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PKZ">
            <ts e="T12" id="Seg_0" n="sc" s="T1">
               <ts e="T12" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Она</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">меня</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">направляла:</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_12" n="HIAT:ip">"</nts>
                  <ts e="T5" id="Seg_14" n="HIAT:w" s="T4">Поезжай</ts>
                  <nts id="Seg_15" n="HIAT:ip">,</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_17" n="HIAT:ip">(</nts>
                  <nts id="Seg_18" n="HIAT:ip">(</nts>
                  <ats e="T6" id="Seg_19" n="HIAT:non-pho" s="T5">…</ats>
                  <nts id="Seg_20" n="HIAT:ip">)</nts>
                  <nts id="Seg_21" n="HIAT:ip">)</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_24" n="HIAT:w" s="T6">какой</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_27" n="HIAT:w" s="T7">хороший</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_30" n="HIAT:w" s="T10">человек</ts>
                  <nts id="Seg_31" n="HIAT:ip">"</nts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T46" id="Seg_34" n="sc" s="T40">
               <ts e="T46" id="Seg_36" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_38" n="HIAT:w" s="T40">Камасинскую</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_41" n="HIAT:w" s="T41">не</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_44" n="HIAT:w" s="T42">спою</ts>
                  <nts id="Seg_45" n="HIAT:ip">,</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_48" n="HIAT:w" s="T43">а</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_51" n="HIAT:w" s="T44">вот</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_54" n="HIAT:w" s="T45">так</ts>
                  <nts id="Seg_55" n="HIAT:ip">…</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T50" id="Seg_57" n="sc" s="T47">
               <ts e="T50" id="Seg_59" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_61" n="HIAT:w" s="T47">Спою</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_64" n="HIAT:w" s="T48">стих</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_67" n="HIAT:w" s="T49">один</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T59" id="Seg_70" n="sc" s="T56">
               <ts e="T59" id="Seg_72" n="HIAT:u" s="T56">
                  <ts e="T58" id="Seg_74" n="HIAT:w" s="T56">Камасинский</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_76" n="HIAT:ip">(</nts>
                  <nts id="Seg_77" n="HIAT:ip">(</nts>
                  <ats e="T59" id="Seg_78" n="HIAT:non-pho" s="T58">…</ats>
                  <nts id="Seg_79" n="HIAT:ip">)</nts>
                  <nts id="Seg_80" n="HIAT:ip">)</nts>
                  <nts id="Seg_81" n="HIAT:ip">.</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T66" id="Seg_83" n="sc" s="T60">
               <ts e="T66" id="Seg_85" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_87" n="HIAT:w" s="T60">Tăn</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_90" n="HIAT:w" s="T61">kudaj</ts>
                  <nts id="Seg_91" n="HIAT:ip">,</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_94" n="HIAT:w" s="T62">kudaj</ts>
                  <nts id="Seg_95" n="HIAT:ip">,</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_98" n="HIAT:w" s="T63">iʔ</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_101" n="HIAT:w" s="T64">maʔtə</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_104" n="HIAT:w" s="T65">măna</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T73" id="Seg_107" n="sc" s="T67">
               <ts e="T73" id="Seg_109" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_111" n="HIAT:w" s="T67">Iʔ</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_114" n="HIAT:w" s="T68">maʔtə</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_117" n="HIAT:w" s="T69">măna</ts>
                  <nts id="Seg_118" n="HIAT:ip">,</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_121" n="HIAT:w" s="T70">iʔ</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_124" n="HIAT:w" s="T71">barəʔtə</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_127" n="HIAT:w" s="T72">măna</ts>
                  <nts id="Seg_128" n="HIAT:ip">.</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T79" id="Seg_130" n="sc" s="T74">
               <ts e="T79" id="Seg_132" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_134" n="HIAT:w" s="T74">Tăn</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_137" n="HIAT:w" s="T75">iʔ</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_140" n="HIAT:w" s="T76">măna</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_143" n="HIAT:w" s="T77">sĭjdə</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_146" n="HIAT:w" s="T78">sagəšzəbi</ts>
                  <nts id="Seg_147" n="HIAT:ip">.</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T86" id="Seg_149" n="sc" s="T80">
               <ts e="T86" id="Seg_151" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_153" n="HIAT:w" s="T80">Tăn</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_156" n="HIAT:w" s="T81">deʔ</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_159" n="HIAT:w" s="T82">măna</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_162" n="HIAT:w" s="T83">sĭjdə</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_165" n="HIAT:w" s="T84">sagəštə</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_168" n="HIAT:w" s="T85">iʔgö</ts>
                  <nts id="Seg_169" n="HIAT:ip">.</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T91" id="Seg_171" n="sc" s="T87">
               <ts e="T91" id="Seg_173" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_175" n="HIAT:w" s="T87">Tüšəleʔ</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_178" n="HIAT:w" s="T88">măna</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_181" n="HIAT:w" s="T89">maktanərzittə</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_184" n="HIAT:w" s="T90">tănzi</ts>
                  <nts id="Seg_185" n="HIAT:ip">.</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T110" id="Seg_187" n="sc" s="T108">
               <ts e="T110" id="Seg_189" n="HIAT:u" s="T108">
                  <ts e="T110" id="Seg_191" n="HIAT:w" s="T108">Спасибо</ts>
                  <nts id="Seg_192" n="HIAT:ip">!</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T116" id="Seg_194" n="sc" s="T115">
               <ts e="T116" id="Seg_196" n="HIAT:u" s="T115">
                  <nts id="Seg_197" n="HIAT:ip">(</nts>
                  <nts id="Seg_198" n="HIAT:ip">(</nts>
                  <ats e="T116" id="Seg_199" n="HIAT:non-pho" s="T115">BRK</ats>
                  <nts id="Seg_200" n="HIAT:ip">)</nts>
                  <nts id="Seg_201" n="HIAT:ip">)</nts>
                  <nts id="Seg_202" n="HIAT:ip">.</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T126" id="Seg_204" n="sc" s="T125">
               <ts e="T126" id="Seg_206" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_208" n="HIAT:w" s="T125">Nagobi</ts>
                  <nts id="Seg_209" n="HIAT:ip">.</nts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T129" id="Seg_211" n="sc" s="T127">
               <ts e="T129" id="Seg_213" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_215" n="HIAT:w" s="T127">Nagobi</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_218" n="HIAT:w" s="T128">miʔnʼibeʔ</ts>
                  <nts id="Seg_219" n="HIAT:ip">.</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T133" id="Seg_221" n="sc" s="T130">
               <ts e="T133" id="Seg_223" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_225" n="HIAT:w" s="T130">Tolʼkă</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_228" n="HIAT:w" s="T131">Kazan</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_231" n="HIAT:w" s="T373">turagən</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_234" n="HIAT:w" s="T132">ibi</ts>
                  <nts id="Seg_235" n="HIAT:ip">.</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T143" id="Seg_237" n="sc" s="T135">
               <ts e="T139" id="Seg_239" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_241" n="HIAT:w" s="T135">Dĭn</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_244" n="HIAT:w" s="T136">ibiʔi</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_247" n="HIAT:w" s="T138">pladəʔi</ts>
                  <nts id="Seg_248" n="HIAT:ip">.</nts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_251" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_253" n="HIAT:w" s="T139">Oldʼa</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_256" n="HIAT:w" s="T140">dĭn</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_259" n="HIAT:w" s="T141">dĭzeŋ</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_262" n="HIAT:w" s="T142">ibiʔi</ts>
                  <nts id="Seg_263" n="HIAT:ip">.</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T159" id="Seg_265" n="sc" s="T154">
               <ts e="T159" id="Seg_267" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_269" n="HIAT:w" s="T154">Tus</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_272" n="HIAT:w" s="T155">i</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_275" n="HIAT:w" s="T156">sĭreʔpne</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_278" n="HIAT:w" s="T157">dĭn</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_281" n="HIAT:w" s="T158">ibiʔi</ts>
                  <nts id="Seg_282" n="HIAT:ip">.</nts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T167" id="Seg_284" n="sc" s="T160">
               <ts e="T167" id="Seg_286" n="HIAT:u" s="T160">
                  <nts id="Seg_287" n="HIAT:ip">(</nts>
                  <ts e="T161" id="Seg_289" n="HIAT:w" s="T160">I-</ts>
                  <nts id="Seg_290" n="HIAT:ip">)</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_293" n="HIAT:w" s="T161">I</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_296" n="HIAT:w" s="T162">ipek</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_299" n="HIAT:w" s="T163">dĭn</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_302" n="HIAT:w" s="T164">ibiʔi</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_305" n="HIAT:w" s="T165">dĭzeŋ</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_308" n="HIAT:w" s="T166">nagobi</ts>
                  <nts id="Seg_309" n="HIAT:ip">.</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T171" id="Seg_311" n="sc" s="T168">
               <ts e="T171" id="Seg_313" n="HIAT:u" s="T168">
                  <nts id="Seg_314" n="HIAT:ip">(</nts>
                  <ts e="T169" id="Seg_316" n="HIAT:w" s="T168">Kuʔ-</ts>
                  <nts id="Seg_317" n="HIAT:ip">)</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_320" n="HIAT:w" s="T169">Kuʔləʔjə</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_323" n="HIAT:w" s="T170">ipek</ts>
                  <nts id="Seg_324" n="HIAT:ip">.</nts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T179" id="Seg_326" n="sc" s="T172">
               <ts e="T179" id="Seg_328" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_330" n="HIAT:w" s="T172">Dĭgəttə</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_333" n="HIAT:w" s="T173">dĭʔnə</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_336" n="HIAT:w" s="T174">mĭləʔi</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_339" n="HIAT:w" s="T175">dĭzeŋ</ts>
                  <nts id="Seg_340" n="HIAT:ip">,</nts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_343" n="HIAT:w" s="T176">dʼijənə</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_346" n="HIAT:w" s="T177">kamləʔi</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_349" n="HIAT:w" s="T178">saməjdəsʼtə</ts>
                  <nts id="Seg_350" n="HIAT:ip">.</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T203" id="Seg_352" n="sc" s="T202">
               <ts e="T203" id="Seg_354" n="HIAT:u" s="T202">
                  <ts e="T203" id="Seg_356" n="HIAT:w" s="T202">Nagobi</ts>
                  <nts id="Seg_357" n="HIAT:ip">.</nts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T208" id="Seg_359" n="sc" s="T204">
               <ts e="T208" id="Seg_361" n="HIAT:u" s="T204">
                  <ts e="T205" id="Seg_363" n="HIAT:w" s="T204">Miʔ</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_365" n="HIAT:ip">(</nts>
                  <ts e="T206" id="Seg_367" n="HIAT:w" s="T205">ĭmbidə</ts>
                  <nts id="Seg_368" n="HIAT:ip">)</nts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_371" n="HIAT:w" s="T206">ej</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_374" n="HIAT:w" s="T207">kübibaʔ</ts>
                  <nts id="Seg_375" n="HIAT:ip">.</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T212" id="Seg_377" n="sc" s="T209">
               <ts e="T212" id="Seg_379" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_381" n="HIAT:w" s="T209">Uja</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_384" n="HIAT:w" s="T210">iʔgö</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_387" n="HIAT:w" s="T211">ibi</ts>
                  <nts id="Seg_388" n="HIAT:ip">.</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T217" id="Seg_390" n="sc" s="T213">
               <ts e="T217" id="Seg_392" n="HIAT:u" s="T213">
                  <ts e="T214" id="Seg_394" n="HIAT:w" s="T213">Ipek</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_396" n="HIAT:ip">(</nts>
                  <ts e="T215" id="Seg_398" n="HIAT:w" s="T214">da</ts>
                  <nts id="Seg_399" n="HIAT:ip">)</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_402" n="HIAT:w" s="T215">ej</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_405" n="HIAT:w" s="T216">ambibaʔ</ts>
                  <nts id="Seg_406" n="HIAT:ip">.</nts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T222" id="Seg_408" n="sc" s="T218">
               <ts e="T222" id="Seg_410" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_412" n="HIAT:w" s="T218">Üge</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_415" n="HIAT:w" s="T219">uja</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_418" n="HIAT:w" s="T220">i</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_421" n="HIAT:w" s="T221">uja</ts>
                  <nts id="Seg_422" n="HIAT:ip">.</nts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T226" id="Seg_424" n="sc" s="T223">
               <ts e="T226" id="Seg_426" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_428" n="HIAT:w" s="T223">Da</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_431" n="HIAT:w" s="T224">bü</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_434" n="HIAT:w" s="T225">bĭʔpibeʔ</ts>
                  <nts id="Seg_435" n="HIAT:ip">.</nts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T250" id="Seg_437" n="sc" s="T244">
               <ts e="T250" id="Seg_439" n="HIAT:u" s="T244">
                  <nts id="Seg_440" n="HIAT:ip">(</nts>
                  <ts e="T245" id="Seg_442" n="HIAT:w" s="T244">Dĭze-</ts>
                  <nts id="Seg_443" n="HIAT:ip">)</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_446" n="HIAT:w" s="T245">Dĭn</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_449" n="HIAT:w" s="T246">bü</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_452" n="HIAT:w" s="T247">kamendə</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_455" n="HIAT:w" s="T248">ej</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_458" n="HIAT:w" s="T249">kănlia</ts>
                  <nts id="Seg_459" n="HIAT:ip">.</nts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T253" id="Seg_461" n="sc" s="T251">
               <ts e="T253" id="Seg_463" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_465" n="HIAT:w" s="T251">Naga</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_468" n="HIAT:w" s="T252">kălotsăʔi</ts>
                  <nts id="Seg_469" n="HIAT:ip">.</nts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T256" id="Seg_471" n="sc" s="T254">
               <ts e="T256" id="Seg_473" n="HIAT:u" s="T254">
                  <ts e="T255" id="Seg_475" n="HIAT:w" s="T254">Bü</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_478" n="HIAT:w" s="T255">mʼaŋnaʔbə</ts>
                  <nts id="Seg_479" n="HIAT:ip">.</nts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T270" id="Seg_481" n="sc" s="T267">
               <ts e="T270" id="Seg_483" n="HIAT:u" s="T267">
                  <ts e="T268" id="Seg_485" n="HIAT:w" s="T267">Dĭzeŋ</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_488" n="HIAT:w" s="T268">ej</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_491" n="HIAT:w" s="T269">torirbiʔi</ts>
                  <nts id="Seg_492" n="HIAT:ip">.</nts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T276" id="Seg_494" n="sc" s="T271">
               <ts e="T276" id="Seg_496" n="HIAT:u" s="T271">
                  <ts e="T272" id="Seg_498" n="HIAT:w" s="T271">Tolʼkă</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_501" n="HIAT:w" s="T272">măn</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_504" n="HIAT:w" s="T273">abam</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_506" n="HIAT:ip">(</nts>
                  <ts e="T275" id="Seg_508" n="HIAT:w" s="T274">tojirbi-</ts>
                  <nts id="Seg_509" n="HIAT:ip">)</nts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_512" n="HIAT:w" s="T275">tojirlaʔpi</ts>
                  <nts id="Seg_513" n="HIAT:ip">.</nts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T281" id="Seg_515" n="sc" s="T277">
               <ts e="T281" id="Seg_517" n="HIAT:u" s="T277">
                  <nts id="Seg_518" n="HIAT:ip">(</nts>
                  <ts e="T278" id="Seg_520" n="HIAT:w" s="T277">Dĭʔnə=</ts>
                  <nts id="Seg_521" n="HIAT:ip">)</nts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_524" n="HIAT:w" s="T278">Dĭzeŋdə</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_527" n="HIAT:w" s="T279">toltanoʔ</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_530" n="HIAT:w" s="T280">amnolbi</ts>
                  <nts id="Seg_531" n="HIAT:ip">.</nts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T288" id="Seg_533" n="sc" s="T282">
               <ts e="T288" id="Seg_535" n="HIAT:u" s="T282">
                  <ts e="T283" id="Seg_537" n="HIAT:w" s="T282">Dĭzeŋ</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_540" n="HIAT:w" s="T283">dĭʔnə</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_543" n="HIAT:w" s="T284">ipek</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_545" n="HIAT:ip">(</nts>
                  <ts e="T286" id="Seg_547" n="HIAT:w" s="T285">pĭdə-</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_550" n="HIAT:w" s="T286">pădə-</ts>
                  <nts id="Seg_551" n="HIAT:ip">)</nts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_554" n="HIAT:w" s="T287">pĭdəbiʔi</ts>
                  <nts id="Seg_555" n="HIAT:ip">.</nts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T296" id="Seg_557" n="sc" s="T293">
               <ts e="T296" id="Seg_559" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_561" n="HIAT:w" s="T293">Они</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_564" n="HIAT:w" s="T294">не</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_567" n="HIAT:w" s="T295">пахали</ts>
                  <nts id="Seg_568" n="HIAT:ip">.</nts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T305" id="Seg_570" n="sc" s="T297">
               <ts e="T305" id="Seg_572" n="HIAT:u" s="T297">
                  <ts e="T299" id="Seg_574" n="HIAT:w" s="T297">Только</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_577" n="HIAT:w" s="T299">один</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_580" n="HIAT:w" s="T300">отец</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_583" n="HIAT:w" s="T302">мой</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_586" n="HIAT:w" s="T303">пахал</ts>
                  <nts id="Seg_587" n="HIAT:ip">.</nts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T318" id="Seg_589" n="sc" s="T315">
               <ts e="T318" id="Seg_591" n="HIAT:u" s="T315">
                  <ts e="T316" id="Seg_593" n="HIAT:w" s="T315">A</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_596" n="HIAT:w" s="T316">Kazan</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_599" n="HIAT:w" s="T374">turagən</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_602" n="HIAT:w" s="T317">iləʔi</ts>
                  <nts id="Seg_603" n="HIAT:ip">.</nts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T325" id="Seg_605" n="sc" s="T319">
               <ts e="T325" id="Seg_607" n="HIAT:u" s="T319">
                  <nts id="Seg_608" n="HIAT:ip">(</nts>
                  <ts e="T320" id="Seg_610" n="HIAT:w" s="T319">Nagurdə</ts>
                  <nts id="Seg_611" n="HIAT:ip">)</nts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_614" n="HIAT:w" s="T320">seləj</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_617" n="HIAT:w" s="T321">aspaʔ</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_620" n="HIAT:w" s="T322">iləʔi</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_623" n="HIAT:w" s="T323">i</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_626" n="HIAT:w" s="T324">biʔpiʔi</ts>
                  <nts id="Seg_627" n="HIAT:ip">.</nts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T332" id="Seg_629" n="sc" s="T328">
               <ts e="T332" id="Seg_631" n="HIAT:u" s="T328">
                  <ts e="T329" id="Seg_633" n="HIAT:w" s="T328">Целое</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_636" n="HIAT:w" s="T329">ведро</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_639" n="HIAT:w" s="T330">купят</ts>
                  <nts id="Seg_640" n="HIAT:ip">,</nts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_643" n="HIAT:w" s="T331">напьются</ts>
                  <nts id="Seg_644" n="HIAT:ip">.</nts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PKZ">
            <ts e="T12" id="Seg_646" n="sc" s="T1">
               <ts e="T2" id="Seg_648" n="e" s="T1">Она </ts>
               <ts e="T3" id="Seg_650" n="e" s="T2">меня </ts>
               <ts e="T4" id="Seg_652" n="e" s="T3">направляла: </ts>
               <ts e="T5" id="Seg_654" n="e" s="T4">"Поезжай, </ts>
               <ts e="T6" id="Seg_656" n="e" s="T5">((…)) </ts>
               <ts e="T7" id="Seg_658" n="e" s="T6">какой </ts>
               <ts e="T10" id="Seg_660" n="e" s="T7">хороший </ts>
               <ts e="T12" id="Seg_662" n="e" s="T10">человек". </ts>
            </ts>
            <ts e="T46" id="Seg_663" n="sc" s="T40">
               <ts e="T41" id="Seg_665" n="e" s="T40">Камасинскую </ts>
               <ts e="T42" id="Seg_667" n="e" s="T41">не </ts>
               <ts e="T43" id="Seg_669" n="e" s="T42">спою, </ts>
               <ts e="T44" id="Seg_671" n="e" s="T43">а </ts>
               <ts e="T45" id="Seg_673" n="e" s="T44">вот </ts>
               <ts e="T46" id="Seg_675" n="e" s="T45">так… </ts>
            </ts>
            <ts e="T50" id="Seg_676" n="sc" s="T47">
               <ts e="T48" id="Seg_678" n="e" s="T47">Спою </ts>
               <ts e="T49" id="Seg_680" n="e" s="T48">стих </ts>
               <ts e="T50" id="Seg_682" n="e" s="T49">один. </ts>
            </ts>
            <ts e="T59" id="Seg_683" n="sc" s="T56">
               <ts e="T58" id="Seg_685" n="e" s="T56">Камасинский </ts>
               <ts e="T59" id="Seg_687" n="e" s="T58">((…)). </ts>
            </ts>
            <ts e="T66" id="Seg_688" n="sc" s="T60">
               <ts e="T61" id="Seg_690" n="e" s="T60">Tăn </ts>
               <ts e="T62" id="Seg_692" n="e" s="T61">kudaj, </ts>
               <ts e="T63" id="Seg_694" n="e" s="T62">kudaj, </ts>
               <ts e="T64" id="Seg_696" n="e" s="T63">iʔ </ts>
               <ts e="T65" id="Seg_698" n="e" s="T64">maʔtə </ts>
               <ts e="T66" id="Seg_700" n="e" s="T65">măna. </ts>
            </ts>
            <ts e="T73" id="Seg_701" n="sc" s="T67">
               <ts e="T68" id="Seg_703" n="e" s="T67">Iʔ </ts>
               <ts e="T69" id="Seg_705" n="e" s="T68">maʔtə </ts>
               <ts e="T70" id="Seg_707" n="e" s="T69">măna, </ts>
               <ts e="T71" id="Seg_709" n="e" s="T70">iʔ </ts>
               <ts e="T72" id="Seg_711" n="e" s="T71">barəʔtə </ts>
               <ts e="T73" id="Seg_713" n="e" s="T72">măna. </ts>
            </ts>
            <ts e="T79" id="Seg_714" n="sc" s="T74">
               <ts e="T75" id="Seg_716" n="e" s="T74">Tăn </ts>
               <ts e="T76" id="Seg_718" n="e" s="T75">iʔ </ts>
               <ts e="T77" id="Seg_720" n="e" s="T76">măna </ts>
               <ts e="T78" id="Seg_722" n="e" s="T77">sĭjdə </ts>
               <ts e="T79" id="Seg_724" n="e" s="T78">sagəšzəbi. </ts>
            </ts>
            <ts e="T86" id="Seg_725" n="sc" s="T80">
               <ts e="T81" id="Seg_727" n="e" s="T80">Tăn </ts>
               <ts e="T82" id="Seg_729" n="e" s="T81">deʔ </ts>
               <ts e="T83" id="Seg_731" n="e" s="T82">măna </ts>
               <ts e="T84" id="Seg_733" n="e" s="T83">sĭjdə </ts>
               <ts e="T85" id="Seg_735" n="e" s="T84">sagəštə </ts>
               <ts e="T86" id="Seg_737" n="e" s="T85">iʔgö. </ts>
            </ts>
            <ts e="T91" id="Seg_738" n="sc" s="T87">
               <ts e="T88" id="Seg_740" n="e" s="T87">Tüšəleʔ </ts>
               <ts e="T89" id="Seg_742" n="e" s="T88">măna </ts>
               <ts e="T90" id="Seg_744" n="e" s="T89">maktanərzittə </ts>
               <ts e="T91" id="Seg_746" n="e" s="T90">tănzi. </ts>
            </ts>
            <ts e="T110" id="Seg_747" n="sc" s="T108">
               <ts e="T110" id="Seg_749" n="e" s="T108">Спасибо! </ts>
            </ts>
            <ts e="T116" id="Seg_750" n="sc" s="T115">
               <ts e="T116" id="Seg_752" n="e" s="T115">((BRK)). </ts>
            </ts>
            <ts e="T126" id="Seg_753" n="sc" s="T125">
               <ts e="T126" id="Seg_755" n="e" s="T125">Nagobi. </ts>
            </ts>
            <ts e="T129" id="Seg_756" n="sc" s="T127">
               <ts e="T128" id="Seg_758" n="e" s="T127">Nagobi </ts>
               <ts e="T129" id="Seg_760" n="e" s="T128">miʔnʼibeʔ. </ts>
            </ts>
            <ts e="T133" id="Seg_761" n="sc" s="T130">
               <ts e="T131" id="Seg_763" n="e" s="T130">Tolʼkă </ts>
               <ts e="T373" id="Seg_765" n="e" s="T131">Kazan </ts>
               <ts e="T132" id="Seg_767" n="e" s="T373">turagən </ts>
               <ts e="T133" id="Seg_769" n="e" s="T132">ibi. </ts>
            </ts>
            <ts e="T143" id="Seg_770" n="sc" s="T135">
               <ts e="T136" id="Seg_772" n="e" s="T135">Dĭn </ts>
               <ts e="T138" id="Seg_774" n="e" s="T136">ibiʔi </ts>
               <ts e="T139" id="Seg_776" n="e" s="T138">pladəʔi. </ts>
               <ts e="T140" id="Seg_778" n="e" s="T139">Oldʼa </ts>
               <ts e="T141" id="Seg_780" n="e" s="T140">dĭn </ts>
               <ts e="T142" id="Seg_782" n="e" s="T141">dĭzeŋ </ts>
               <ts e="T143" id="Seg_784" n="e" s="T142">ibiʔi. </ts>
            </ts>
            <ts e="T159" id="Seg_785" n="sc" s="T154">
               <ts e="T155" id="Seg_787" n="e" s="T154">Tus </ts>
               <ts e="T156" id="Seg_789" n="e" s="T155">i </ts>
               <ts e="T157" id="Seg_791" n="e" s="T156">sĭreʔpne </ts>
               <ts e="T158" id="Seg_793" n="e" s="T157">dĭn </ts>
               <ts e="T159" id="Seg_795" n="e" s="T158">ibiʔi. </ts>
            </ts>
            <ts e="T167" id="Seg_796" n="sc" s="T160">
               <ts e="T161" id="Seg_798" n="e" s="T160">(I-) </ts>
               <ts e="T162" id="Seg_800" n="e" s="T161">I </ts>
               <ts e="T163" id="Seg_802" n="e" s="T162">ipek </ts>
               <ts e="T164" id="Seg_804" n="e" s="T163">dĭn </ts>
               <ts e="T165" id="Seg_806" n="e" s="T164">ibiʔi </ts>
               <ts e="T166" id="Seg_808" n="e" s="T165">dĭzeŋ </ts>
               <ts e="T167" id="Seg_810" n="e" s="T166">nagobi. </ts>
            </ts>
            <ts e="T171" id="Seg_811" n="sc" s="T168">
               <ts e="T169" id="Seg_813" n="e" s="T168">(Kuʔ-) </ts>
               <ts e="T170" id="Seg_815" n="e" s="T169">Kuʔləʔjə </ts>
               <ts e="T171" id="Seg_817" n="e" s="T170">ipek. </ts>
            </ts>
            <ts e="T179" id="Seg_818" n="sc" s="T172">
               <ts e="T173" id="Seg_820" n="e" s="T172">Dĭgəttə </ts>
               <ts e="T174" id="Seg_822" n="e" s="T173">dĭʔnə </ts>
               <ts e="T175" id="Seg_824" n="e" s="T174">mĭləʔi </ts>
               <ts e="T176" id="Seg_826" n="e" s="T175">dĭzeŋ, </ts>
               <ts e="T177" id="Seg_828" n="e" s="T176">dʼijənə </ts>
               <ts e="T178" id="Seg_830" n="e" s="T177">kamləʔi </ts>
               <ts e="T179" id="Seg_832" n="e" s="T178">saməjdəsʼtə. </ts>
            </ts>
            <ts e="T203" id="Seg_833" n="sc" s="T202">
               <ts e="T203" id="Seg_835" n="e" s="T202">Nagobi. </ts>
            </ts>
            <ts e="T208" id="Seg_836" n="sc" s="T204">
               <ts e="T205" id="Seg_838" n="e" s="T204">Miʔ </ts>
               <ts e="T206" id="Seg_840" n="e" s="T205">(ĭmbidə) </ts>
               <ts e="T207" id="Seg_842" n="e" s="T206">ej </ts>
               <ts e="T208" id="Seg_844" n="e" s="T207">kübibaʔ. </ts>
            </ts>
            <ts e="T212" id="Seg_845" n="sc" s="T209">
               <ts e="T210" id="Seg_847" n="e" s="T209">Uja </ts>
               <ts e="T211" id="Seg_849" n="e" s="T210">iʔgö </ts>
               <ts e="T212" id="Seg_851" n="e" s="T211">ibi. </ts>
            </ts>
            <ts e="T217" id="Seg_852" n="sc" s="T213">
               <ts e="T214" id="Seg_854" n="e" s="T213">Ipek </ts>
               <ts e="T215" id="Seg_856" n="e" s="T214">(da) </ts>
               <ts e="T216" id="Seg_858" n="e" s="T215">ej </ts>
               <ts e="T217" id="Seg_860" n="e" s="T216">ambibaʔ. </ts>
            </ts>
            <ts e="T222" id="Seg_861" n="sc" s="T218">
               <ts e="T219" id="Seg_863" n="e" s="T218">Üge </ts>
               <ts e="T220" id="Seg_865" n="e" s="T219">uja </ts>
               <ts e="T221" id="Seg_867" n="e" s="T220">i </ts>
               <ts e="T222" id="Seg_869" n="e" s="T221">uja. </ts>
            </ts>
            <ts e="T226" id="Seg_870" n="sc" s="T223">
               <ts e="T224" id="Seg_872" n="e" s="T223">Da </ts>
               <ts e="T225" id="Seg_874" n="e" s="T224">bü </ts>
               <ts e="T226" id="Seg_876" n="e" s="T225">bĭʔpibeʔ. </ts>
            </ts>
            <ts e="T250" id="Seg_877" n="sc" s="T244">
               <ts e="T245" id="Seg_879" n="e" s="T244">(Dĭze-) </ts>
               <ts e="T246" id="Seg_881" n="e" s="T245">Dĭn </ts>
               <ts e="T247" id="Seg_883" n="e" s="T246">bü </ts>
               <ts e="T248" id="Seg_885" n="e" s="T247">kamendə </ts>
               <ts e="T249" id="Seg_887" n="e" s="T248">ej </ts>
               <ts e="T250" id="Seg_889" n="e" s="T249">kănlia. </ts>
            </ts>
            <ts e="T253" id="Seg_890" n="sc" s="T251">
               <ts e="T252" id="Seg_892" n="e" s="T251">Naga </ts>
               <ts e="T253" id="Seg_894" n="e" s="T252">kălotsăʔi. </ts>
            </ts>
            <ts e="T256" id="Seg_895" n="sc" s="T254">
               <ts e="T255" id="Seg_897" n="e" s="T254">Bü </ts>
               <ts e="T256" id="Seg_899" n="e" s="T255">mʼaŋnaʔbə. </ts>
            </ts>
            <ts e="T270" id="Seg_900" n="sc" s="T267">
               <ts e="T268" id="Seg_902" n="e" s="T267">Dĭzeŋ </ts>
               <ts e="T269" id="Seg_904" n="e" s="T268">ej </ts>
               <ts e="T270" id="Seg_906" n="e" s="T269">torirbiʔi. </ts>
            </ts>
            <ts e="T276" id="Seg_907" n="sc" s="T271">
               <ts e="T272" id="Seg_909" n="e" s="T271">Tolʼkă </ts>
               <ts e="T273" id="Seg_911" n="e" s="T272">măn </ts>
               <ts e="T274" id="Seg_913" n="e" s="T273">abam </ts>
               <ts e="T275" id="Seg_915" n="e" s="T274">(tojirbi-) </ts>
               <ts e="T276" id="Seg_917" n="e" s="T275">tojirlaʔpi. </ts>
            </ts>
            <ts e="T281" id="Seg_918" n="sc" s="T277">
               <ts e="T278" id="Seg_920" n="e" s="T277">(Dĭʔnə=) </ts>
               <ts e="T279" id="Seg_922" n="e" s="T278">Dĭzeŋdə </ts>
               <ts e="T280" id="Seg_924" n="e" s="T279">toltanoʔ </ts>
               <ts e="T281" id="Seg_926" n="e" s="T280">amnolbi. </ts>
            </ts>
            <ts e="T288" id="Seg_927" n="sc" s="T282">
               <ts e="T283" id="Seg_929" n="e" s="T282">Dĭzeŋ </ts>
               <ts e="T284" id="Seg_931" n="e" s="T283">dĭʔnə </ts>
               <ts e="T285" id="Seg_933" n="e" s="T284">ipek </ts>
               <ts e="T286" id="Seg_935" n="e" s="T285">(pĭdə- </ts>
               <ts e="T287" id="Seg_937" n="e" s="T286">pădə-) </ts>
               <ts e="T288" id="Seg_939" n="e" s="T287">pĭdəbiʔi. </ts>
            </ts>
            <ts e="T296" id="Seg_940" n="sc" s="T293">
               <ts e="T294" id="Seg_942" n="e" s="T293">Они </ts>
               <ts e="T295" id="Seg_944" n="e" s="T294">не </ts>
               <ts e="T296" id="Seg_946" n="e" s="T295">пахали. </ts>
            </ts>
            <ts e="T305" id="Seg_947" n="sc" s="T297">
               <ts e="T299" id="Seg_949" n="e" s="T297">Только </ts>
               <ts e="T300" id="Seg_951" n="e" s="T299">один </ts>
               <ts e="T302" id="Seg_953" n="e" s="T300">отец </ts>
               <ts e="T303" id="Seg_955" n="e" s="T302">мой </ts>
               <ts e="T305" id="Seg_957" n="e" s="T303">пахал. </ts>
            </ts>
            <ts e="T318" id="Seg_958" n="sc" s="T315">
               <ts e="T316" id="Seg_960" n="e" s="T315">A </ts>
               <ts e="T374" id="Seg_962" n="e" s="T316">Kazan </ts>
               <ts e="T317" id="Seg_964" n="e" s="T374">turagən </ts>
               <ts e="T318" id="Seg_966" n="e" s="T317">iləʔi. </ts>
            </ts>
            <ts e="T325" id="Seg_967" n="sc" s="T319">
               <ts e="T320" id="Seg_969" n="e" s="T319">(Nagurdə) </ts>
               <ts e="T321" id="Seg_971" n="e" s="T320">seləj </ts>
               <ts e="T322" id="Seg_973" n="e" s="T321">aspaʔ </ts>
               <ts e="T323" id="Seg_975" n="e" s="T322">iləʔi </ts>
               <ts e="T324" id="Seg_977" n="e" s="T323">i </ts>
               <ts e="T325" id="Seg_979" n="e" s="T324">biʔpiʔi. </ts>
            </ts>
            <ts e="T332" id="Seg_980" n="sc" s="T328">
               <ts e="T329" id="Seg_982" n="e" s="T328">Целое </ts>
               <ts e="T330" id="Seg_984" n="e" s="T329">ведро </ts>
               <ts e="T331" id="Seg_986" n="e" s="T330">купят, </ts>
               <ts e="T332" id="Seg_988" n="e" s="T331">напьются. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PKZ">
            <ta e="T12" id="Seg_989" s="T1">PKZ_197008_09341-2a.PKZ.001 (001)</ta>
            <ta e="T46" id="Seg_990" s="T40">PKZ_197008_09341-2a.PKZ.002 (006)</ta>
            <ta e="T50" id="Seg_991" s="T47">PKZ_197008_09341-2a.PKZ.003 (007)</ta>
            <ta e="T59" id="Seg_992" s="T56">PKZ_197008_09341-2a.PKZ.004 (009)</ta>
            <ta e="T66" id="Seg_993" s="T60">PKZ_197008_09341-2a.PKZ.005 (011)</ta>
            <ta e="T73" id="Seg_994" s="T67">PKZ_197008_09341-2a.PKZ.006 (012)</ta>
            <ta e="T79" id="Seg_995" s="T74">PKZ_197008_09341-2a.PKZ.007 (013)</ta>
            <ta e="T86" id="Seg_996" s="T80">PKZ_197008_09341-2a.PKZ.008 (014)</ta>
            <ta e="T91" id="Seg_997" s="T87">PKZ_197008_09341-2a.PKZ.009 (015)</ta>
            <ta e="T110" id="Seg_998" s="T108">PKZ_197008_09341-2a.PKZ.010 (018)</ta>
            <ta e="T116" id="Seg_999" s="T115">PKZ_197008_09341-2a.PKZ.011 (021)</ta>
            <ta e="T126" id="Seg_1000" s="T125">PKZ_197008_09341-2a.PKZ.012 (023)</ta>
            <ta e="T129" id="Seg_1001" s="T127">PKZ_197008_09341-2a.PKZ.013 (024)</ta>
            <ta e="T133" id="Seg_1002" s="T130">PKZ_197008_09341-2a.PKZ.014 (025)</ta>
            <ta e="T139" id="Seg_1003" s="T135">PKZ_197008_09341-2a.PKZ.015 (027)</ta>
            <ta e="T143" id="Seg_1004" s="T139">PKZ_197008_09341-2a.PKZ.016 (028)</ta>
            <ta e="T159" id="Seg_1005" s="T154">PKZ_197008_09341-2a.PKZ.017 (033)</ta>
            <ta e="T167" id="Seg_1006" s="T160">PKZ_197008_09341-2a.PKZ.018 (035)</ta>
            <ta e="T171" id="Seg_1007" s="T168">PKZ_197008_09341-2a.PKZ.019 (036)</ta>
            <ta e="T179" id="Seg_1008" s="T172">PKZ_197008_09341-2a.PKZ.020 (037)</ta>
            <ta e="T203" id="Seg_1009" s="T202">PKZ_197008_09341-2a.PKZ.021 (044)</ta>
            <ta e="T208" id="Seg_1010" s="T204">PKZ_197008_09341-2a.PKZ.022 (045)</ta>
            <ta e="T212" id="Seg_1011" s="T209">PKZ_197008_09341-2a.PKZ.023 (046)</ta>
            <ta e="T217" id="Seg_1012" s="T213">PKZ_197008_09341-2a.PKZ.024 (047)</ta>
            <ta e="T222" id="Seg_1013" s="T218">PKZ_197008_09341-2a.PKZ.025 (048)</ta>
            <ta e="T226" id="Seg_1014" s="T223">PKZ_197008_09341-2a.PKZ.026 (049)</ta>
            <ta e="T250" id="Seg_1015" s="T244">PKZ_197008_09341-2a.PKZ.027 (055)</ta>
            <ta e="T253" id="Seg_1016" s="T251">PKZ_197008_09341-2a.PKZ.028 (056)</ta>
            <ta e="T256" id="Seg_1017" s="T254">PKZ_197008_09341-2a.PKZ.029 (057)</ta>
            <ta e="T270" id="Seg_1018" s="T267">PKZ_197008_09341-2a.PKZ.030 (061)</ta>
            <ta e="T276" id="Seg_1019" s="T271">PKZ_197008_09341-2a.PKZ.031 (062)</ta>
            <ta e="T281" id="Seg_1020" s="T277">PKZ_197008_09341-2a.PKZ.032 (063)</ta>
            <ta e="T288" id="Seg_1021" s="T282">PKZ_197008_09341-2a.PKZ.033 (064)</ta>
            <ta e="T296" id="Seg_1022" s="T293">PKZ_197008_09341-2a.PKZ.034 (067)</ta>
            <ta e="T305" id="Seg_1023" s="T297">PKZ_197008_09341-2a.PKZ.035 (068)</ta>
            <ta e="T318" id="Seg_1024" s="T315">PKZ_197008_09341-2a.PKZ.036 (073)</ta>
            <ta e="T325" id="Seg_1025" s="T319">PKZ_197008_09341-2a.PKZ.037 (074)</ta>
            <ta e="T332" id="Seg_1026" s="T328">PKZ_197008_09341-2a.PKZ.038 (076)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PKZ">
            <ta e="T12" id="Seg_1027" s="T1">Она меня направляла: "Поезжай, ((…)) какой хороший человек". </ta>
            <ta e="T46" id="Seg_1028" s="T40">Камасинскую не спою, а вот так… </ta>
            <ta e="T50" id="Seg_1029" s="T47">Спою стих один. </ta>
            <ta e="T59" id="Seg_1030" s="T56">Камасинский ((…)). </ta>
            <ta e="T66" id="Seg_1031" s="T60">Tăn kudaj, kudaj, iʔ maʔtə măna. </ta>
            <ta e="T73" id="Seg_1032" s="T67">Iʔ maʔtə măna, iʔ barəʔtə măna. </ta>
            <ta e="T79" id="Seg_1033" s="T74">Tăn iʔ măna sĭjdə sagəšzəbi. </ta>
            <ta e="T86" id="Seg_1034" s="T80">Tăn deʔ măna sĭjdə sagəštə iʔgö. </ta>
            <ta e="T91" id="Seg_1035" s="T87">Tüšəleʔ măna maktanərzittə tănzi. </ta>
            <ta e="T110" id="Seg_1036" s="T108">Спасибо! </ta>
            <ta e="T116" id="Seg_1037" s="T115">((BRK)). </ta>
            <ta e="T126" id="Seg_1038" s="T125">((NOISE)) Nagobi. </ta>
            <ta e="T129" id="Seg_1039" s="T127">((NOISE)) Nagobi miʔnʼibeʔ. </ta>
            <ta e="T133" id="Seg_1040" s="T130">Tolʼkă Kazan turagən ibi. </ta>
            <ta e="T139" id="Seg_1041" s="T135">Dĭn ibiʔi pladəʔi. </ta>
            <ta e="T143" id="Seg_1042" s="T139">Oldʼa dĭn dĭzeŋ ibiʔi. </ta>
            <ta e="T159" id="Seg_1043" s="T154">((NOISE)) Tus i sĭreʔpne dĭn ibiʔi. </ta>
            <ta e="T167" id="Seg_1044" s="T160">(I-) I ipek dĭn ibiʔi dĭzeŋ nagobi. </ta>
            <ta e="T171" id="Seg_1045" s="T168">(Kuʔ-) Kuʔləʔjə ipek. </ta>
            <ta e="T179" id="Seg_1046" s="T172">Dĭgəttə dĭʔnə mĭləʔi dĭzeŋ, dʼijənə kamləʔi saməjdəsʼtə. </ta>
            <ta e="T203" id="Seg_1047" s="T202">Nagobi. </ta>
            <ta e="T208" id="Seg_1048" s="T204">Miʔ (ĭmbidə) ej kübibaʔ. </ta>
            <ta e="T212" id="Seg_1049" s="T209">Uja iʔgö ibi. </ta>
            <ta e="T217" id="Seg_1050" s="T213">Ipek (da) ej ambibaʔ. </ta>
            <ta e="T222" id="Seg_1051" s="T218">Üge uja i uja. </ta>
            <ta e="T226" id="Seg_1052" s="T223">Da bü bĭʔpibeʔ. </ta>
            <ta e="T250" id="Seg_1053" s="T244">(Dĭze-) Dĭn bü kamendə ej kănlia. </ta>
            <ta e="T253" id="Seg_1054" s="T251">Naga kălotsăʔi. </ta>
            <ta e="T256" id="Seg_1055" s="T254">Bü mʼaŋnaʔbə. </ta>
            <ta e="T270" id="Seg_1056" s="T267">Dĭzeŋ ej torirbiʔi. </ta>
            <ta e="T276" id="Seg_1057" s="T271">Tolʼkă măn abam (tojirbi-) tojirlaʔpi. </ta>
            <ta e="T281" id="Seg_1058" s="T277">(Dĭʔnə=) Dĭzeŋdə toltanoʔ amnolbi. </ta>
            <ta e="T288" id="Seg_1059" s="T282">Dĭzeŋ dĭʔnə ipek (pĭdə- pădə-) pĭdəbiʔi. </ta>
            <ta e="T296" id="Seg_1060" s="T293">Они не пахали. </ta>
            <ta e="T305" id="Seg_1061" s="T297">Только один отец мой пахал. </ta>
            <ta e="T318" id="Seg_1062" s="T315">A Kazan turagən iləʔi. </ta>
            <ta e="T325" id="Seg_1063" s="T319">(Nagurdə) seləj aspaʔ iləʔi i biʔpiʔi. </ta>
            <ta e="T332" id="Seg_1064" s="T328">Целое ведро купят, напьются. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-PKZ">
            <ta e="T61" id="Seg_1065" s="T60">tăn</ta>
            <ta e="T62" id="Seg_1066" s="T61">kudaj</ta>
            <ta e="T63" id="Seg_1067" s="T62">kudaj</ta>
            <ta e="T64" id="Seg_1068" s="T63">i-ʔ</ta>
            <ta e="T65" id="Seg_1069" s="T64">maʔ-tə</ta>
            <ta e="T66" id="Seg_1070" s="T65">măna</ta>
            <ta e="T68" id="Seg_1071" s="T67">i-ʔ</ta>
            <ta e="T69" id="Seg_1072" s="T68">maʔ-tə</ta>
            <ta e="T70" id="Seg_1073" s="T69">măna</ta>
            <ta e="T71" id="Seg_1074" s="T70">i-ʔ</ta>
            <ta e="T72" id="Seg_1075" s="T71">barəʔ-tə</ta>
            <ta e="T73" id="Seg_1076" s="T72">măna</ta>
            <ta e="T75" id="Seg_1077" s="T74">tăn</ta>
            <ta e="T76" id="Seg_1078" s="T75">i-ʔ</ta>
            <ta e="T77" id="Seg_1079" s="T76">măna</ta>
            <ta e="T78" id="Seg_1080" s="T77">sĭj-də</ta>
            <ta e="T81" id="Seg_1081" s="T80">tăn</ta>
            <ta e="T82" id="Seg_1082" s="T81">de-ʔ</ta>
            <ta e="T83" id="Seg_1083" s="T82">măna</ta>
            <ta e="T84" id="Seg_1084" s="T83">sĭj-də</ta>
            <ta e="T85" id="Seg_1085" s="T84">sagəštə</ta>
            <ta e="T86" id="Seg_1086" s="T85">iʔgö</ta>
            <ta e="T88" id="Seg_1087" s="T87">tüšəl-e-ʔ</ta>
            <ta e="T89" id="Seg_1088" s="T88">măna</ta>
            <ta e="T90" id="Seg_1089" s="T89">maktanər-zittə</ta>
            <ta e="T91" id="Seg_1090" s="T90">tăn-zi</ta>
            <ta e="T126" id="Seg_1091" s="T125">nago-bi</ta>
            <ta e="T128" id="Seg_1092" s="T127">nago-bi</ta>
            <ta e="T129" id="Seg_1093" s="T128">miʔnʼibeʔ</ta>
            <ta e="T131" id="Seg_1094" s="T130">tolʼkă</ta>
            <ta e="T373" id="Seg_1095" s="T131">Kazan</ta>
            <ta e="T132" id="Seg_1096" s="T373">tura-gən</ta>
            <ta e="T133" id="Seg_1097" s="T132">i-bi</ta>
            <ta e="T136" id="Seg_1098" s="T135">dĭn</ta>
            <ta e="T138" id="Seg_1099" s="T136">i-bi-ʔi</ta>
            <ta e="T139" id="Seg_1100" s="T138">plad-əʔi</ta>
            <ta e="T140" id="Seg_1101" s="T139">oldʼa</ta>
            <ta e="T141" id="Seg_1102" s="T140">dĭn</ta>
            <ta e="T142" id="Seg_1103" s="T141">dĭ-zeŋ</ta>
            <ta e="T143" id="Seg_1104" s="T142">i-bi-ʔi</ta>
            <ta e="T155" id="Seg_1105" s="T154">tus</ta>
            <ta e="T156" id="Seg_1106" s="T155">i</ta>
            <ta e="T157" id="Seg_1107" s="T156">sĭreʔpne</ta>
            <ta e="T158" id="Seg_1108" s="T157">dĭn</ta>
            <ta e="T159" id="Seg_1109" s="T158">i-bi-ʔi</ta>
            <ta e="T163" id="Seg_1110" s="T162">ipek</ta>
            <ta e="T164" id="Seg_1111" s="T163">dĭn</ta>
            <ta e="T165" id="Seg_1112" s="T164">i-bi-ʔi</ta>
            <ta e="T166" id="Seg_1113" s="T165">dĭ-zeŋ</ta>
            <ta e="T167" id="Seg_1114" s="T166">nago-bi</ta>
            <ta e="T170" id="Seg_1115" s="T169">kuʔ-lə-ʔjə</ta>
            <ta e="T171" id="Seg_1116" s="T170">ipek</ta>
            <ta e="T173" id="Seg_1117" s="T172">dĭgəttə</ta>
            <ta e="T174" id="Seg_1118" s="T173">dĭʔ-nə</ta>
            <ta e="T175" id="Seg_1119" s="T174">mĭ-lə-ʔi</ta>
            <ta e="T176" id="Seg_1120" s="T175">dĭ-zeŋ</ta>
            <ta e="T177" id="Seg_1121" s="T176">dʼijə-nə</ta>
            <ta e="T178" id="Seg_1122" s="T177">kam-lə-ʔi</ta>
            <ta e="T179" id="Seg_1123" s="T178">saməj-də-sʼtə</ta>
            <ta e="T203" id="Seg_1124" s="T202">nago-bi</ta>
            <ta e="T205" id="Seg_1125" s="T204">miʔ</ta>
            <ta e="T206" id="Seg_1126" s="T205">ĭmbi=də</ta>
            <ta e="T207" id="Seg_1127" s="T206">ej</ta>
            <ta e="T208" id="Seg_1128" s="T207">kü-bi-baʔ</ta>
            <ta e="T210" id="Seg_1129" s="T209">uja</ta>
            <ta e="T211" id="Seg_1130" s="T210">iʔgö</ta>
            <ta e="T212" id="Seg_1131" s="T211">i-bi</ta>
            <ta e="T214" id="Seg_1132" s="T213">ipek</ta>
            <ta e="T215" id="Seg_1133" s="T214">da</ta>
            <ta e="T216" id="Seg_1134" s="T215">ej</ta>
            <ta e="T217" id="Seg_1135" s="T216">am-bi-baʔ</ta>
            <ta e="T219" id="Seg_1136" s="T218">üge</ta>
            <ta e="T220" id="Seg_1137" s="T219">uja</ta>
            <ta e="T221" id="Seg_1138" s="T220">i</ta>
            <ta e="T222" id="Seg_1139" s="T221">uja</ta>
            <ta e="T224" id="Seg_1140" s="T223">da</ta>
            <ta e="T225" id="Seg_1141" s="T224">bü</ta>
            <ta e="T226" id="Seg_1142" s="T225">bĭʔ-pi-beʔ</ta>
            <ta e="T246" id="Seg_1143" s="T245">dĭn</ta>
            <ta e="T247" id="Seg_1144" s="T246">bü</ta>
            <ta e="T248" id="Seg_1145" s="T247">kamen=də</ta>
            <ta e="T249" id="Seg_1146" s="T248">ej</ta>
            <ta e="T250" id="Seg_1147" s="T249">kăn-lia</ta>
            <ta e="T252" id="Seg_1148" s="T251">naga</ta>
            <ta e="T253" id="Seg_1149" s="T252">kălotsă-ʔi</ta>
            <ta e="T255" id="Seg_1150" s="T254">bü</ta>
            <ta e="T256" id="Seg_1151" s="T255">mʼaŋ-naʔbə</ta>
            <ta e="T268" id="Seg_1152" s="T267">dĭ-zeŋ</ta>
            <ta e="T269" id="Seg_1153" s="T268">ej</ta>
            <ta e="T270" id="Seg_1154" s="T269">torir-bi-ʔi</ta>
            <ta e="T272" id="Seg_1155" s="T271">tolʼkă</ta>
            <ta e="T273" id="Seg_1156" s="T272">măn</ta>
            <ta e="T274" id="Seg_1157" s="T273">aba-m</ta>
            <ta e="T275" id="Seg_1158" s="T274">tojir-bi</ta>
            <ta e="T276" id="Seg_1159" s="T275">tojir-laʔpi</ta>
            <ta e="T278" id="Seg_1160" s="T277">dĭʔ-nə</ta>
            <ta e="T279" id="Seg_1161" s="T278">dĭ-zeŋ-də</ta>
            <ta e="T280" id="Seg_1162" s="T279">toltanoʔ</ta>
            <ta e="T281" id="Seg_1163" s="T280">amnol-bi</ta>
            <ta e="T283" id="Seg_1164" s="T282">dĭ-zeŋ</ta>
            <ta e="T284" id="Seg_1165" s="T283">dĭʔ-nə</ta>
            <ta e="T285" id="Seg_1166" s="T284">ipek</ta>
            <ta e="T288" id="Seg_1167" s="T287">pĭdə-bi-ʔi</ta>
            <ta e="T316" id="Seg_1168" s="T315">a</ta>
            <ta e="T374" id="Seg_1169" s="T316">Kazan</ta>
            <ta e="T317" id="Seg_1170" s="T374">tura-gən</ta>
            <ta e="T318" id="Seg_1171" s="T317">i-lə-ʔi</ta>
            <ta e="T320" id="Seg_1172" s="T319">nagur-də</ta>
            <ta e="T321" id="Seg_1173" s="T320">seləj</ta>
            <ta e="T322" id="Seg_1174" s="T321">aspaʔ</ta>
            <ta e="T323" id="Seg_1175" s="T322">i-lə-ʔi</ta>
            <ta e="T324" id="Seg_1176" s="T323">i</ta>
            <ta e="T325" id="Seg_1177" s="T324">biʔ-pi-ʔi</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PKZ">
            <ta e="T61" id="Seg_1178" s="T60">tăn</ta>
            <ta e="T62" id="Seg_1179" s="T61">kudaj</ta>
            <ta e="T63" id="Seg_1180" s="T62">kudaj</ta>
            <ta e="T64" id="Seg_1181" s="T63">e-ʔ</ta>
            <ta e="T65" id="Seg_1182" s="T64">ma-t</ta>
            <ta e="T66" id="Seg_1183" s="T65">măna</ta>
            <ta e="T68" id="Seg_1184" s="T67">e-ʔ</ta>
            <ta e="T69" id="Seg_1185" s="T68">ma-t</ta>
            <ta e="T70" id="Seg_1186" s="T69">măna</ta>
            <ta e="T71" id="Seg_1187" s="T70">e-ʔ</ta>
            <ta e="T72" id="Seg_1188" s="T71">barəʔ-t</ta>
            <ta e="T73" id="Seg_1189" s="T72">măna</ta>
            <ta e="T75" id="Seg_1190" s="T74">tăn</ta>
            <ta e="T76" id="Seg_1191" s="T75">i-ʔ</ta>
            <ta e="T77" id="Seg_1192" s="T76">măna</ta>
            <ta e="T78" id="Seg_1193" s="T77">sĭj-də</ta>
            <ta e="T81" id="Seg_1194" s="T80">tăn</ta>
            <ta e="T82" id="Seg_1195" s="T81">det-ʔ</ta>
            <ta e="T83" id="Seg_1196" s="T82">măna</ta>
            <ta e="T84" id="Seg_1197" s="T83">sĭj-də</ta>
            <ta e="T85" id="Seg_1198" s="T84">sagəštə</ta>
            <ta e="T86" id="Seg_1199" s="T85">iʔgö</ta>
            <ta e="T88" id="Seg_1200" s="T87">tüšəl-ə-ʔ</ta>
            <ta e="T89" id="Seg_1201" s="T88">măna</ta>
            <ta e="T90" id="Seg_1202" s="T89">maktanər-zittə</ta>
            <ta e="T91" id="Seg_1203" s="T90">tăn-ziʔ</ta>
            <ta e="T126" id="Seg_1204" s="T125">naga-bi</ta>
            <ta e="T128" id="Seg_1205" s="T127">naga-bi</ta>
            <ta e="T129" id="Seg_1206" s="T128">miʔnʼibeʔ</ta>
            <ta e="T131" id="Seg_1207" s="T130">tolʼko</ta>
            <ta e="T373" id="Seg_1208" s="T131">Kazan</ta>
            <ta e="T132" id="Seg_1209" s="T373">tura-Kən</ta>
            <ta e="T133" id="Seg_1210" s="T132">i-bi</ta>
            <ta e="T136" id="Seg_1211" s="T135">dĭn</ta>
            <ta e="T138" id="Seg_1212" s="T136">i-bi-jəʔ</ta>
            <ta e="T139" id="Seg_1213" s="T138">plat-jəʔ</ta>
            <ta e="T140" id="Seg_1214" s="T139">oldʼa</ta>
            <ta e="T141" id="Seg_1215" s="T140">dĭn</ta>
            <ta e="T142" id="Seg_1216" s="T141">dĭ-zAŋ</ta>
            <ta e="T143" id="Seg_1217" s="T142">i-bi-jəʔ</ta>
            <ta e="T155" id="Seg_1218" s="T154">tus</ta>
            <ta e="T156" id="Seg_1219" s="T155">i</ta>
            <ta e="T157" id="Seg_1220" s="T156">sĭreʔp</ta>
            <ta e="T158" id="Seg_1221" s="T157">dĭn</ta>
            <ta e="T159" id="Seg_1222" s="T158">i-bi-jəʔ</ta>
            <ta e="T163" id="Seg_1223" s="T162">ipek</ta>
            <ta e="T164" id="Seg_1224" s="T163">dĭn</ta>
            <ta e="T165" id="Seg_1225" s="T164">i-bi-jəʔ</ta>
            <ta e="T166" id="Seg_1226" s="T165">dĭ-zAŋ</ta>
            <ta e="T167" id="Seg_1227" s="T166">naga-bi</ta>
            <ta e="T170" id="Seg_1228" s="T169">koʔ-lV-jəʔ</ta>
            <ta e="T171" id="Seg_1229" s="T170">ipek</ta>
            <ta e="T173" id="Seg_1230" s="T172">dĭgəttə</ta>
            <ta e="T174" id="Seg_1231" s="T173">dĭ-Tə</ta>
            <ta e="T175" id="Seg_1232" s="T174">mĭ-lV-jəʔ</ta>
            <ta e="T176" id="Seg_1233" s="T175">dĭ-zAŋ</ta>
            <ta e="T177" id="Seg_1234" s="T176">dʼije-Tə</ta>
            <ta e="T178" id="Seg_1235" s="T177">kan-lV-jəʔ</ta>
            <ta e="T179" id="Seg_1236" s="T178">saməj-ntə-zittə</ta>
            <ta e="T203" id="Seg_1237" s="T202">naga-bi</ta>
            <ta e="T205" id="Seg_1238" s="T204">miʔ</ta>
            <ta e="T206" id="Seg_1239" s="T205">ĭmbi=də</ta>
            <ta e="T207" id="Seg_1240" s="T206">ej</ta>
            <ta e="T208" id="Seg_1241" s="T207">kü-bi-bAʔ</ta>
            <ta e="T210" id="Seg_1242" s="T209">uja</ta>
            <ta e="T211" id="Seg_1243" s="T210">iʔgö</ta>
            <ta e="T212" id="Seg_1244" s="T211">i-bi</ta>
            <ta e="T214" id="Seg_1245" s="T213">ipek</ta>
            <ta e="T215" id="Seg_1246" s="T214">da</ta>
            <ta e="T216" id="Seg_1247" s="T215">ej</ta>
            <ta e="T217" id="Seg_1248" s="T216">am-bi-bAʔ</ta>
            <ta e="T219" id="Seg_1249" s="T218">üge</ta>
            <ta e="T220" id="Seg_1250" s="T219">uja</ta>
            <ta e="T221" id="Seg_1251" s="T220">i</ta>
            <ta e="T222" id="Seg_1252" s="T221">uja</ta>
            <ta e="T224" id="Seg_1253" s="T223">da</ta>
            <ta e="T225" id="Seg_1254" s="T224">bü</ta>
            <ta e="T226" id="Seg_1255" s="T225">bĭs-bi-bAʔ</ta>
            <ta e="T246" id="Seg_1256" s="T245">dĭn</ta>
            <ta e="T247" id="Seg_1257" s="T246">bü</ta>
            <ta e="T248" id="Seg_1258" s="T247">kamən=də</ta>
            <ta e="T249" id="Seg_1259" s="T248">ej</ta>
            <ta e="T250" id="Seg_1260" s="T249">kănzə-liA</ta>
            <ta e="T252" id="Seg_1261" s="T251">naga</ta>
            <ta e="T253" id="Seg_1262" s="T252">kălotsă-jəʔ</ta>
            <ta e="T255" id="Seg_1263" s="T254">bü</ta>
            <ta e="T256" id="Seg_1264" s="T255">mʼaŋ-laʔbə</ta>
            <ta e="T268" id="Seg_1265" s="T267">dĭ-zAŋ</ta>
            <ta e="T269" id="Seg_1266" s="T268">ej</ta>
            <ta e="T270" id="Seg_1267" s="T269">tajər-bi-jəʔ</ta>
            <ta e="T272" id="Seg_1268" s="T271">tolʼko</ta>
            <ta e="T273" id="Seg_1269" s="T272">măn</ta>
            <ta e="T274" id="Seg_1270" s="T273">aba-m</ta>
            <ta e="T275" id="Seg_1271" s="T274">tajər-bi</ta>
            <ta e="T276" id="Seg_1272" s="T275">tajər-laʔpi</ta>
            <ta e="T278" id="Seg_1273" s="T277">dĭ-Tə</ta>
            <ta e="T279" id="Seg_1274" s="T278">dĭ-zAŋ-Tə</ta>
            <ta e="T280" id="Seg_1275" s="T279">toltano</ta>
            <ta e="T281" id="Seg_1276" s="T280">amnol-bi</ta>
            <ta e="T283" id="Seg_1277" s="T282">dĭ-zAŋ</ta>
            <ta e="T284" id="Seg_1278" s="T283">dĭ-Tə</ta>
            <ta e="T285" id="Seg_1279" s="T284">ipek</ta>
            <ta e="T288" id="Seg_1280" s="T287">püdə-bi-jəʔ</ta>
            <ta e="T316" id="Seg_1281" s="T315">a</ta>
            <ta e="T374" id="Seg_1282" s="T316">Kazan</ta>
            <ta e="T317" id="Seg_1283" s="T374">tura-Kən</ta>
            <ta e="T318" id="Seg_1284" s="T317">i-lV-jəʔ</ta>
            <ta e="T320" id="Seg_1285" s="T319">nagur-Tə</ta>
            <ta e="T321" id="Seg_1286" s="T320">celɨj</ta>
            <ta e="T322" id="Seg_1287" s="T321">aspaʔ</ta>
            <ta e="T323" id="Seg_1288" s="T322">i-lV-jəʔ</ta>
            <ta e="T324" id="Seg_1289" s="T323">i</ta>
            <ta e="T325" id="Seg_1290" s="T324">bĭs-bi-jəʔ</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PKZ">
            <ta e="T61" id="Seg_1291" s="T60">you.NOM</ta>
            <ta e="T62" id="Seg_1292" s="T61">God.[NOM.SG]</ta>
            <ta e="T63" id="Seg_1293" s="T62">God.[NOM.SG]</ta>
            <ta e="T64" id="Seg_1294" s="T63">NEG.AUX-IMP.2SG</ta>
            <ta e="T65" id="Seg_1295" s="T64">leave-IMP.2SG.O</ta>
            <ta e="T66" id="Seg_1296" s="T65">I.ACC</ta>
            <ta e="T68" id="Seg_1297" s="T67">NEG.AUX-IMP.2SG</ta>
            <ta e="T69" id="Seg_1298" s="T68">leave-IMP.2SG.O</ta>
            <ta e="T70" id="Seg_1299" s="T69">I.ACC</ta>
            <ta e="T71" id="Seg_1300" s="T70">NEG.AUX-IMP.2SG</ta>
            <ta e="T72" id="Seg_1301" s="T71">throw.away-IMP.2SG.O</ta>
            <ta e="T73" id="Seg_1302" s="T72">I.ACC</ta>
            <ta e="T75" id="Seg_1303" s="T74">you.NOM</ta>
            <ta e="T76" id="Seg_1304" s="T75">take-IMP.2SG</ta>
            <ta e="T77" id="Seg_1305" s="T76">I.ACC</ta>
            <ta e="T78" id="Seg_1306" s="T77">heart-NOM/GEN/ACC.3SG</ta>
            <ta e="T81" id="Seg_1307" s="T80">you.NOM</ta>
            <ta e="T82" id="Seg_1308" s="T81">bring-IMP.2SG</ta>
            <ta e="T83" id="Seg_1309" s="T82">I.LAT</ta>
            <ta e="T84" id="Seg_1310" s="T83">heart-NOM/GEN/ACC.3SG</ta>
            <ta e="T85" id="Seg_1311" s="T84">silly.[NOM.SG]</ta>
            <ta e="T86" id="Seg_1312" s="T85">many</ta>
            <ta e="T88" id="Seg_1313" s="T87">study-EP-IMP.2SG</ta>
            <ta e="T89" id="Seg_1314" s="T88">I.LAT</ta>
            <ta e="T90" id="Seg_1315" s="T89">praise-INF.LAT</ta>
            <ta e="T91" id="Seg_1316" s="T90">you.NOM-COM</ta>
            <ta e="T126" id="Seg_1317" s="T125">NEG.EX-PST.[3SG]</ta>
            <ta e="T128" id="Seg_1318" s="T127">NEG.EX-PST.[3SG]</ta>
            <ta e="T129" id="Seg_1319" s="T128">we.ACC</ta>
            <ta e="T131" id="Seg_1320" s="T130">only</ta>
            <ta e="T373" id="Seg_1321" s="T131">Aginskoe</ta>
            <ta e="T132" id="Seg_1322" s="T373">settlement-LOC</ta>
            <ta e="T133" id="Seg_1323" s="T132">be-PST.[3SG]</ta>
            <ta e="T136" id="Seg_1324" s="T135">there</ta>
            <ta e="T138" id="Seg_1325" s="T136">be-PST-3PL</ta>
            <ta e="T139" id="Seg_1326" s="T138">scarf-PL</ta>
            <ta e="T140" id="Seg_1327" s="T139">clothing.[NOM.SG]</ta>
            <ta e="T141" id="Seg_1328" s="T140">there</ta>
            <ta e="T142" id="Seg_1329" s="T141">this-PL</ta>
            <ta e="T143" id="Seg_1330" s="T142">take-PST-3PL</ta>
            <ta e="T155" id="Seg_1331" s="T154">salt.[NOM.SG]</ta>
            <ta e="T156" id="Seg_1332" s="T155">and</ta>
            <ta e="T157" id="Seg_1333" s="T156">sugar.[NOM.SG]</ta>
            <ta e="T158" id="Seg_1334" s="T157">there</ta>
            <ta e="T159" id="Seg_1335" s="T158">take-PST-3PL</ta>
            <ta e="T163" id="Seg_1336" s="T162">bread.[NOM.SG]</ta>
            <ta e="T164" id="Seg_1337" s="T163">there</ta>
            <ta e="T165" id="Seg_1338" s="T164">take-PST-3PL</ta>
            <ta e="T166" id="Seg_1339" s="T165">this-PL</ta>
            <ta e="T167" id="Seg_1340" s="T166">NEG.EX-PST.[3SG]</ta>
            <ta e="T170" id="Seg_1341" s="T169">dry-FUT-3PL</ta>
            <ta e="T171" id="Seg_1342" s="T170">bread.[NOM.SG]</ta>
            <ta e="T173" id="Seg_1343" s="T172">then</ta>
            <ta e="T174" id="Seg_1344" s="T173">this-LAT</ta>
            <ta e="T175" id="Seg_1345" s="T174">give-FUT-3PL</ta>
            <ta e="T176" id="Seg_1346" s="T175">this-PL</ta>
            <ta e="T177" id="Seg_1347" s="T176">forest-LAT</ta>
            <ta e="T178" id="Seg_1348" s="T177">go-FUT-3PL</ta>
            <ta e="T179" id="Seg_1349" s="T178">hunt-IPFVZ-INF.LAT</ta>
            <ta e="T203" id="Seg_1350" s="T202">NEG.EX-PST.[3SG]</ta>
            <ta e="T205" id="Seg_1351" s="T204">we.NOM</ta>
            <ta e="T206" id="Seg_1352" s="T205">what.[NOM.SG]=INDEF</ta>
            <ta e="T207" id="Seg_1353" s="T206">NEG</ta>
            <ta e="T208" id="Seg_1354" s="T207">die-PST-1PL</ta>
            <ta e="T210" id="Seg_1355" s="T209">meat.[NOM.SG]</ta>
            <ta e="T211" id="Seg_1356" s="T210">many</ta>
            <ta e="T212" id="Seg_1357" s="T211">be-PST.[3SG]</ta>
            <ta e="T214" id="Seg_1358" s="T213">bread.[NOM.SG]</ta>
            <ta e="T215" id="Seg_1359" s="T214">and</ta>
            <ta e="T216" id="Seg_1360" s="T215">NEG</ta>
            <ta e="T217" id="Seg_1361" s="T216">eat-PST-1PL</ta>
            <ta e="T219" id="Seg_1362" s="T218">always</ta>
            <ta e="T220" id="Seg_1363" s="T219">meat.[NOM.SG]</ta>
            <ta e="T221" id="Seg_1364" s="T220">and</ta>
            <ta e="T222" id="Seg_1365" s="T221">meat.[NOM.SG]</ta>
            <ta e="T224" id="Seg_1366" s="T223">and</ta>
            <ta e="T225" id="Seg_1367" s="T224">water.[NOM.SG]</ta>
            <ta e="T226" id="Seg_1368" s="T225">drink-PST-1PL</ta>
            <ta e="T246" id="Seg_1369" s="T245">there</ta>
            <ta e="T247" id="Seg_1370" s="T246">water.[NOM.SG]</ta>
            <ta e="T248" id="Seg_1371" s="T247">when=INDEF</ta>
            <ta e="T249" id="Seg_1372" s="T248">NEG</ta>
            <ta e="T250" id="Seg_1373" s="T249">freeze-PRS.[3SG]</ta>
            <ta e="T252" id="Seg_1374" s="T251">NEG.EX.[3SG]</ta>
            <ta e="T253" id="Seg_1375" s="T252">well-NOM/GEN/ACC.3PL</ta>
            <ta e="T255" id="Seg_1376" s="T254">water.[NOM.SG]</ta>
            <ta e="T256" id="Seg_1377" s="T255">flow-DUR.[3SG]</ta>
            <ta e="T268" id="Seg_1378" s="T267">this-PL</ta>
            <ta e="T269" id="Seg_1379" s="T268">NEG</ta>
            <ta e="T270" id="Seg_1380" s="T269">plough-PST-3PL</ta>
            <ta e="T272" id="Seg_1381" s="T271">only</ta>
            <ta e="T273" id="Seg_1382" s="T272">I.NOM</ta>
            <ta e="T274" id="Seg_1383" s="T273">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T275" id="Seg_1384" s="T274">plough-PST.[3SG]</ta>
            <ta e="T276" id="Seg_1385" s="T275">plough-DUR.PST.[3SG]</ta>
            <ta e="T278" id="Seg_1386" s="T277">this-LAT</ta>
            <ta e="T279" id="Seg_1387" s="T278">this-PL-LAT</ta>
            <ta e="T280" id="Seg_1388" s="T279">potato.[NOM.SG]</ta>
            <ta e="T281" id="Seg_1389" s="T280">seat-PST.[3SG]</ta>
            <ta e="T283" id="Seg_1390" s="T282">this-PL</ta>
            <ta e="T284" id="Seg_1391" s="T283">this-LAT</ta>
            <ta e="T285" id="Seg_1392" s="T284">bread.[NOM.SG]</ta>
            <ta e="T288" id="Seg_1393" s="T287">mow-PST-3PL</ta>
            <ta e="T316" id="Seg_1394" s="T315">and</ta>
            <ta e="T374" id="Seg_1395" s="T316">Aginskoe</ta>
            <ta e="T317" id="Seg_1396" s="T374">settlement-LOC</ta>
            <ta e="T318" id="Seg_1397" s="T317">take-FUT-3PL</ta>
            <ta e="T320" id="Seg_1398" s="T319">three-LAT</ta>
            <ta e="T321" id="Seg_1399" s="T320">whole</ta>
            <ta e="T322" id="Seg_1400" s="T321">cauldron.[NOM.SG]</ta>
            <ta e="T323" id="Seg_1401" s="T322">take-FUT-3PL</ta>
            <ta e="T324" id="Seg_1402" s="T323">and</ta>
            <ta e="T325" id="Seg_1403" s="T324">drink-PST-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PKZ">
            <ta e="T61" id="Seg_1404" s="T60">ты.NOM</ta>
            <ta e="T62" id="Seg_1405" s="T61">бог.[NOM.SG]</ta>
            <ta e="T63" id="Seg_1406" s="T62">бог.[NOM.SG]</ta>
            <ta e="T64" id="Seg_1407" s="T63">NEG.AUX-IMP.2SG</ta>
            <ta e="T65" id="Seg_1408" s="T64">оставить-IMP.2SG.O</ta>
            <ta e="T66" id="Seg_1409" s="T65">я.ACC</ta>
            <ta e="T68" id="Seg_1410" s="T67">NEG.AUX-IMP.2SG</ta>
            <ta e="T69" id="Seg_1411" s="T68">оставить-IMP.2SG.O</ta>
            <ta e="T70" id="Seg_1412" s="T69">я.ACC</ta>
            <ta e="T71" id="Seg_1413" s="T70">NEG.AUX-IMP.2SG</ta>
            <ta e="T72" id="Seg_1414" s="T71">выбросить-IMP.2SG.O</ta>
            <ta e="T73" id="Seg_1415" s="T72">я.ACC</ta>
            <ta e="T75" id="Seg_1416" s="T74">ты.NOM</ta>
            <ta e="T76" id="Seg_1417" s="T75">взять-IMP.2SG</ta>
            <ta e="T77" id="Seg_1418" s="T76">я.ACC</ta>
            <ta e="T78" id="Seg_1419" s="T77">сердце-NOM/GEN/ACC.3SG</ta>
            <ta e="T81" id="Seg_1420" s="T80">ты.NOM</ta>
            <ta e="T82" id="Seg_1421" s="T81">принести-IMP.2SG</ta>
            <ta e="T83" id="Seg_1422" s="T82">я.LAT</ta>
            <ta e="T84" id="Seg_1423" s="T83">сердце-NOM/GEN/ACC.3SG</ta>
            <ta e="T85" id="Seg_1424" s="T84">глупый.[NOM.SG]</ta>
            <ta e="T86" id="Seg_1425" s="T85">много</ta>
            <ta e="T88" id="Seg_1426" s="T87">учиться-EP-IMP.2SG</ta>
            <ta e="T89" id="Seg_1427" s="T88">я.LAT</ta>
            <ta e="T90" id="Seg_1428" s="T89">восхвалять-INF.LAT</ta>
            <ta e="T91" id="Seg_1429" s="T90">ты.NOM-COM</ta>
            <ta e="T126" id="Seg_1430" s="T125">NEG.EX-PST.[3SG]</ta>
            <ta e="T128" id="Seg_1431" s="T127">NEG.EX-PST.[3SG]</ta>
            <ta e="T129" id="Seg_1432" s="T128">мы.ACC</ta>
            <ta e="T131" id="Seg_1433" s="T130">только</ta>
            <ta e="T373" id="Seg_1434" s="T131">Агинское</ta>
            <ta e="T132" id="Seg_1435" s="T373">поселение-LOC</ta>
            <ta e="T133" id="Seg_1436" s="T132">быть-PST.[3SG]</ta>
            <ta e="T136" id="Seg_1437" s="T135">там</ta>
            <ta e="T138" id="Seg_1438" s="T136">быть-PST-3PL</ta>
            <ta e="T139" id="Seg_1439" s="T138">платок-PL</ta>
            <ta e="T140" id="Seg_1440" s="T139">одежда.[NOM.SG]</ta>
            <ta e="T141" id="Seg_1441" s="T140">там</ta>
            <ta e="T142" id="Seg_1442" s="T141">этот-PL</ta>
            <ta e="T143" id="Seg_1443" s="T142">взять-PST-3PL</ta>
            <ta e="T155" id="Seg_1444" s="T154">соль.[NOM.SG]</ta>
            <ta e="T156" id="Seg_1445" s="T155">и</ta>
            <ta e="T157" id="Seg_1446" s="T156">сахар.[NOM.SG]</ta>
            <ta e="T158" id="Seg_1447" s="T157">там</ta>
            <ta e="T159" id="Seg_1448" s="T158">взять-PST-3PL</ta>
            <ta e="T163" id="Seg_1449" s="T162">хлеб.[NOM.SG]</ta>
            <ta e="T164" id="Seg_1450" s="T163">там</ta>
            <ta e="T165" id="Seg_1451" s="T164">взять-PST-3PL</ta>
            <ta e="T166" id="Seg_1452" s="T165">этот-PL</ta>
            <ta e="T167" id="Seg_1453" s="T166">NEG.EX-PST.[3SG]</ta>
            <ta e="T170" id="Seg_1454" s="T169">сушить-FUT-3PL</ta>
            <ta e="T171" id="Seg_1455" s="T170">хлеб.[NOM.SG]</ta>
            <ta e="T173" id="Seg_1456" s="T172">тогда</ta>
            <ta e="T174" id="Seg_1457" s="T173">этот-LAT</ta>
            <ta e="T175" id="Seg_1458" s="T174">дать-FUT-3PL</ta>
            <ta e="T176" id="Seg_1459" s="T175">этот-PL</ta>
            <ta e="T177" id="Seg_1460" s="T176">лес-LAT</ta>
            <ta e="T178" id="Seg_1461" s="T177">пойти-FUT-3PL</ta>
            <ta e="T179" id="Seg_1462" s="T178">охотиться-IPFVZ-INF.LAT</ta>
            <ta e="T203" id="Seg_1463" s="T202">NEG.EX-PST.[3SG]</ta>
            <ta e="T205" id="Seg_1464" s="T204">мы.NOM</ta>
            <ta e="T206" id="Seg_1465" s="T205">что.[NOM.SG]=INDEF</ta>
            <ta e="T207" id="Seg_1466" s="T206">NEG</ta>
            <ta e="T208" id="Seg_1467" s="T207">умереть-PST-1PL</ta>
            <ta e="T210" id="Seg_1468" s="T209">мясо.[NOM.SG]</ta>
            <ta e="T211" id="Seg_1469" s="T210">много</ta>
            <ta e="T212" id="Seg_1470" s="T211">быть-PST.[3SG]</ta>
            <ta e="T214" id="Seg_1471" s="T213">хлеб.[NOM.SG]</ta>
            <ta e="T215" id="Seg_1472" s="T214">и</ta>
            <ta e="T216" id="Seg_1473" s="T215">NEG</ta>
            <ta e="T217" id="Seg_1474" s="T216">съесть-PST-1PL</ta>
            <ta e="T219" id="Seg_1475" s="T218">всегда</ta>
            <ta e="T220" id="Seg_1476" s="T219">мясо.[NOM.SG]</ta>
            <ta e="T221" id="Seg_1477" s="T220">и</ta>
            <ta e="T222" id="Seg_1478" s="T221">мясо.[NOM.SG]</ta>
            <ta e="T224" id="Seg_1479" s="T223">и</ta>
            <ta e="T225" id="Seg_1480" s="T224">вода.[NOM.SG]</ta>
            <ta e="T226" id="Seg_1481" s="T225">пить-PST-1PL</ta>
            <ta e="T246" id="Seg_1482" s="T245">там</ta>
            <ta e="T247" id="Seg_1483" s="T246">вода.[NOM.SG]</ta>
            <ta e="T248" id="Seg_1484" s="T247">когда=INDEF</ta>
            <ta e="T249" id="Seg_1485" s="T248">NEG</ta>
            <ta e="T250" id="Seg_1486" s="T249">замерзнуть-PRS.[3SG]</ta>
            <ta e="T252" id="Seg_1487" s="T251">NEG.EX.[3SG]</ta>
            <ta e="T253" id="Seg_1488" s="T252">колодец-NOM/GEN/ACC.3PL</ta>
            <ta e="T255" id="Seg_1489" s="T254">вода.[NOM.SG]</ta>
            <ta e="T256" id="Seg_1490" s="T255">течь-DUR.[3SG]</ta>
            <ta e="T268" id="Seg_1491" s="T267">этот-PL</ta>
            <ta e="T269" id="Seg_1492" s="T268">NEG</ta>
            <ta e="T270" id="Seg_1493" s="T269">пахать-PST-3PL</ta>
            <ta e="T272" id="Seg_1494" s="T271">только</ta>
            <ta e="T273" id="Seg_1495" s="T272">я.NOM</ta>
            <ta e="T274" id="Seg_1496" s="T273">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T275" id="Seg_1497" s="T274">пахать-PST.[3SG]</ta>
            <ta e="T276" id="Seg_1498" s="T275">пахать-DUR.PST.[3SG]</ta>
            <ta e="T278" id="Seg_1499" s="T277">этот-LAT</ta>
            <ta e="T279" id="Seg_1500" s="T278">этот-PL-LAT</ta>
            <ta e="T280" id="Seg_1501" s="T279">картофель.[NOM.SG]</ta>
            <ta e="T281" id="Seg_1502" s="T280">сажать-PST.[3SG]</ta>
            <ta e="T283" id="Seg_1503" s="T282">этот-PL</ta>
            <ta e="T284" id="Seg_1504" s="T283">этот-LAT</ta>
            <ta e="T285" id="Seg_1505" s="T284">хлеб.[NOM.SG]</ta>
            <ta e="T288" id="Seg_1506" s="T287">жать-PST-3PL</ta>
            <ta e="T316" id="Seg_1507" s="T315">а</ta>
            <ta e="T374" id="Seg_1508" s="T316">Агинское</ta>
            <ta e="T317" id="Seg_1509" s="T374">поселение-LOC</ta>
            <ta e="T318" id="Seg_1510" s="T317">взять-FUT-3PL</ta>
            <ta e="T320" id="Seg_1511" s="T319">три-LAT</ta>
            <ta e="T321" id="Seg_1512" s="T320">целый</ta>
            <ta e="T322" id="Seg_1513" s="T321">котел.[NOM.SG]</ta>
            <ta e="T323" id="Seg_1514" s="T322">взять-FUT-3PL</ta>
            <ta e="T324" id="Seg_1515" s="T323">и</ta>
            <ta e="T325" id="Seg_1516" s="T324">пить-PST-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PKZ">
            <ta e="T61" id="Seg_1517" s="T60">pers</ta>
            <ta e="T62" id="Seg_1518" s="T61">n.[n:case]</ta>
            <ta e="T63" id="Seg_1519" s="T62">n.[n:case]</ta>
            <ta e="T64" id="Seg_1520" s="T63">aux-v:mood.pn</ta>
            <ta e="T65" id="Seg_1521" s="T64">v-v:mood.pn</ta>
            <ta e="T66" id="Seg_1522" s="T65">pers</ta>
            <ta e="T68" id="Seg_1523" s="T67">aux-v:mood.pn</ta>
            <ta e="T69" id="Seg_1524" s="T68">v-v:mood.pn</ta>
            <ta e="T70" id="Seg_1525" s="T69">pers</ta>
            <ta e="T71" id="Seg_1526" s="T70">aux-v:mood.pn</ta>
            <ta e="T72" id="Seg_1527" s="T71">v-v:mood.pn</ta>
            <ta e="T73" id="Seg_1528" s="T72">pers</ta>
            <ta e="T75" id="Seg_1529" s="T74">pers</ta>
            <ta e="T76" id="Seg_1530" s="T75">v-v:mood.pn</ta>
            <ta e="T77" id="Seg_1531" s="T76">pers</ta>
            <ta e="T78" id="Seg_1532" s="T77">n-n:case.poss</ta>
            <ta e="T81" id="Seg_1533" s="T80">pers</ta>
            <ta e="T82" id="Seg_1534" s="T81">v-v:mood.pn</ta>
            <ta e="T83" id="Seg_1535" s="T82">pers</ta>
            <ta e="T84" id="Seg_1536" s="T83">n-n:case.poss</ta>
            <ta e="T85" id="Seg_1537" s="T84">adj.[n:case]</ta>
            <ta e="T86" id="Seg_1538" s="T85">quant</ta>
            <ta e="T88" id="Seg_1539" s="T87">v-v:ins-v:n.fin</ta>
            <ta e="T89" id="Seg_1540" s="T88">pers</ta>
            <ta e="T90" id="Seg_1541" s="T89">v-v:n.fin</ta>
            <ta e="T91" id="Seg_1542" s="T90">pers-n:case</ta>
            <ta e="T126" id="Seg_1543" s="T125">v-v:tense.[v:pn]</ta>
            <ta e="T128" id="Seg_1544" s="T127">v-v:tense.[v:pn]</ta>
            <ta e="T129" id="Seg_1545" s="T128">pers</ta>
            <ta e="T131" id="Seg_1546" s="T130">adv</ta>
            <ta e="T373" id="Seg_1547" s="T131">propr</ta>
            <ta e="T132" id="Seg_1548" s="T373">n-n:case</ta>
            <ta e="T133" id="Seg_1549" s="T132">v-v:tense.[v:pn]</ta>
            <ta e="T136" id="Seg_1550" s="T135">adv</ta>
            <ta e="T138" id="Seg_1551" s="T136">v-v:tense-v:pn</ta>
            <ta e="T139" id="Seg_1552" s="T138">n-n:num</ta>
            <ta e="T140" id="Seg_1553" s="T139">n.[n:case]</ta>
            <ta e="T141" id="Seg_1554" s="T140">adv</ta>
            <ta e="T142" id="Seg_1555" s="T141">dempro-n:num</ta>
            <ta e="T143" id="Seg_1556" s="T142">v-v:tense-v:pn</ta>
            <ta e="T155" id="Seg_1557" s="T154">n.[n:case]</ta>
            <ta e="T156" id="Seg_1558" s="T155">conj</ta>
            <ta e="T157" id="Seg_1559" s="T156">n.[n:case]</ta>
            <ta e="T158" id="Seg_1560" s="T157">adv</ta>
            <ta e="T159" id="Seg_1561" s="T158">v-v:tense-v:pn</ta>
            <ta e="T163" id="Seg_1562" s="T162">n.[n:case]</ta>
            <ta e="T164" id="Seg_1563" s="T163">adv</ta>
            <ta e="T165" id="Seg_1564" s="T164">v-v:tense-v:pn</ta>
            <ta e="T166" id="Seg_1565" s="T165">dempro-n:num</ta>
            <ta e="T167" id="Seg_1566" s="T166">v-v:tense.[v:pn]</ta>
            <ta e="T170" id="Seg_1567" s="T169">v-v:tense-v:pn</ta>
            <ta e="T171" id="Seg_1568" s="T170">n.[n:case]</ta>
            <ta e="T173" id="Seg_1569" s="T172">adv</ta>
            <ta e="T174" id="Seg_1570" s="T173">dempro-n:case</ta>
            <ta e="T175" id="Seg_1571" s="T174">v-v:tense-v:pn</ta>
            <ta e="T176" id="Seg_1572" s="T175">dempro-n:num</ta>
            <ta e="T177" id="Seg_1573" s="T176">n-n:case</ta>
            <ta e="T178" id="Seg_1574" s="T177">v-v:tense-v:pn</ta>
            <ta e="T179" id="Seg_1575" s="T178">v-v&gt;v-v:n.fin</ta>
            <ta e="T203" id="Seg_1576" s="T202">v-v:tense.[v:pn]</ta>
            <ta e="T205" id="Seg_1577" s="T204">pers</ta>
            <ta e="T206" id="Seg_1578" s="T205">que.[n:case]=ptcl</ta>
            <ta e="T207" id="Seg_1579" s="T206">ptcl</ta>
            <ta e="T208" id="Seg_1580" s="T207">v-v:tense-v:pn</ta>
            <ta e="T210" id="Seg_1581" s="T209">n.[n:case]</ta>
            <ta e="T211" id="Seg_1582" s="T210">quant</ta>
            <ta e="T212" id="Seg_1583" s="T211">v-v:tense.[v:pn]</ta>
            <ta e="T214" id="Seg_1584" s="T213">n.[n:case]</ta>
            <ta e="T215" id="Seg_1585" s="T214">conj</ta>
            <ta e="T216" id="Seg_1586" s="T215">ptcl</ta>
            <ta e="T217" id="Seg_1587" s="T216">v-v:tense-v:pn</ta>
            <ta e="T219" id="Seg_1588" s="T218">adv</ta>
            <ta e="T220" id="Seg_1589" s="T219">n.[n:case]</ta>
            <ta e="T221" id="Seg_1590" s="T220">conj</ta>
            <ta e="T222" id="Seg_1591" s="T221">n.[n:case]</ta>
            <ta e="T224" id="Seg_1592" s="T223">conj</ta>
            <ta e="T225" id="Seg_1593" s="T224">n.[n:case]</ta>
            <ta e="T226" id="Seg_1594" s="T225">v-v:tense-v:pn</ta>
            <ta e="T246" id="Seg_1595" s="T245">adv</ta>
            <ta e="T247" id="Seg_1596" s="T246">n.[n:case]</ta>
            <ta e="T248" id="Seg_1597" s="T247">que=ptcl</ta>
            <ta e="T249" id="Seg_1598" s="T248">ptcl</ta>
            <ta e="T250" id="Seg_1599" s="T249">v-v:tense.[v:pn]</ta>
            <ta e="T252" id="Seg_1600" s="T251">v.[v:pn]</ta>
            <ta e="T253" id="Seg_1601" s="T252">n-n:case.poss</ta>
            <ta e="T255" id="Seg_1602" s="T254">n.[n:case]</ta>
            <ta e="T256" id="Seg_1603" s="T255">v-v&gt;v.[v:pn]</ta>
            <ta e="T268" id="Seg_1604" s="T267">dempro-n:num</ta>
            <ta e="T269" id="Seg_1605" s="T268">ptcl</ta>
            <ta e="T270" id="Seg_1606" s="T269">v-v:tense-v:pn</ta>
            <ta e="T272" id="Seg_1607" s="T271">adv</ta>
            <ta e="T273" id="Seg_1608" s="T272">pers</ta>
            <ta e="T274" id="Seg_1609" s="T273">n-n:case.poss</ta>
            <ta e="T275" id="Seg_1610" s="T274">v-v:tense.[v:pn]</ta>
            <ta e="T276" id="Seg_1611" s="T275">v-v:tense.[v:pn]</ta>
            <ta e="T278" id="Seg_1612" s="T277">dempro-n:case</ta>
            <ta e="T279" id="Seg_1613" s="T278">dempro-n:num-n:case</ta>
            <ta e="T280" id="Seg_1614" s="T279">n.[n:case]</ta>
            <ta e="T281" id="Seg_1615" s="T280">v-v:tense.[v:pn]</ta>
            <ta e="T283" id="Seg_1616" s="T282">dempro-n:num</ta>
            <ta e="T284" id="Seg_1617" s="T283">dempro-n:case</ta>
            <ta e="T285" id="Seg_1618" s="T284">n.[n:case]</ta>
            <ta e="T288" id="Seg_1619" s="T287">v-v:tense-v:pn</ta>
            <ta e="T316" id="Seg_1620" s="T315">conj</ta>
            <ta e="T374" id="Seg_1621" s="T316">propr</ta>
            <ta e="T317" id="Seg_1622" s="T374">n-n:case</ta>
            <ta e="T318" id="Seg_1623" s="T317">v-v:tense-v:pn</ta>
            <ta e="T320" id="Seg_1624" s="T319">num-n:case</ta>
            <ta e="T321" id="Seg_1625" s="T320">adj</ta>
            <ta e="T322" id="Seg_1626" s="T321">n.[n:case]</ta>
            <ta e="T323" id="Seg_1627" s="T322">v-v:tense-v:pn</ta>
            <ta e="T324" id="Seg_1628" s="T323">conj</ta>
            <ta e="T325" id="Seg_1629" s="T324">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PKZ">
            <ta e="T61" id="Seg_1630" s="T60">pers</ta>
            <ta e="T62" id="Seg_1631" s="T61">n</ta>
            <ta e="T63" id="Seg_1632" s="T62">n</ta>
            <ta e="T64" id="Seg_1633" s="T63">aux</ta>
            <ta e="T65" id="Seg_1634" s="T64">v</ta>
            <ta e="T66" id="Seg_1635" s="T65">pers</ta>
            <ta e="T68" id="Seg_1636" s="T67">aux</ta>
            <ta e="T69" id="Seg_1637" s="T68">v</ta>
            <ta e="T70" id="Seg_1638" s="T69">pers</ta>
            <ta e="T71" id="Seg_1639" s="T70">aux</ta>
            <ta e="T72" id="Seg_1640" s="T71">v</ta>
            <ta e="T73" id="Seg_1641" s="T72">pers</ta>
            <ta e="T75" id="Seg_1642" s="T74">pers</ta>
            <ta e="T76" id="Seg_1643" s="T75">v</ta>
            <ta e="T77" id="Seg_1644" s="T76">pers</ta>
            <ta e="T78" id="Seg_1645" s="T77">n</ta>
            <ta e="T81" id="Seg_1646" s="T80">pers</ta>
            <ta e="T82" id="Seg_1647" s="T81">v</ta>
            <ta e="T83" id="Seg_1648" s="T82">pers</ta>
            <ta e="T84" id="Seg_1649" s="T83">n</ta>
            <ta e="T85" id="Seg_1650" s="T84">adj</ta>
            <ta e="T86" id="Seg_1651" s="T85">quant</ta>
            <ta e="T88" id="Seg_1652" s="T87">v</ta>
            <ta e="T89" id="Seg_1653" s="T88">pers</ta>
            <ta e="T90" id="Seg_1654" s="T89">v</ta>
            <ta e="T91" id="Seg_1655" s="T90">pers</ta>
            <ta e="T126" id="Seg_1656" s="T125">v</ta>
            <ta e="T128" id="Seg_1657" s="T127">v</ta>
            <ta e="T129" id="Seg_1658" s="T128">pers</ta>
            <ta e="T131" id="Seg_1659" s="T130">adv</ta>
            <ta e="T373" id="Seg_1660" s="T131">propr</ta>
            <ta e="T132" id="Seg_1661" s="T373">n</ta>
            <ta e="T133" id="Seg_1662" s="T132">v</ta>
            <ta e="T136" id="Seg_1663" s="T135">adv</ta>
            <ta e="T138" id="Seg_1664" s="T136">v</ta>
            <ta e="T139" id="Seg_1665" s="T138">n</ta>
            <ta e="T140" id="Seg_1666" s="T139">n</ta>
            <ta e="T141" id="Seg_1667" s="T140">adv</ta>
            <ta e="T142" id="Seg_1668" s="T141">dempro</ta>
            <ta e="T143" id="Seg_1669" s="T142">v</ta>
            <ta e="T155" id="Seg_1670" s="T154">n</ta>
            <ta e="T156" id="Seg_1671" s="T155">conj</ta>
            <ta e="T157" id="Seg_1672" s="T156">n</ta>
            <ta e="T158" id="Seg_1673" s="T157">adv</ta>
            <ta e="T159" id="Seg_1674" s="T158">v</ta>
            <ta e="T163" id="Seg_1675" s="T162">n</ta>
            <ta e="T164" id="Seg_1676" s="T163">adv</ta>
            <ta e="T165" id="Seg_1677" s="T164">v</ta>
            <ta e="T166" id="Seg_1678" s="T165">dempro</ta>
            <ta e="T167" id="Seg_1679" s="T166">v</ta>
            <ta e="T170" id="Seg_1680" s="T169">v</ta>
            <ta e="T171" id="Seg_1681" s="T170">n</ta>
            <ta e="T173" id="Seg_1682" s="T172">adv</ta>
            <ta e="T174" id="Seg_1683" s="T173">dempro</ta>
            <ta e="T175" id="Seg_1684" s="T174">v</ta>
            <ta e="T176" id="Seg_1685" s="T175">dempro</ta>
            <ta e="T177" id="Seg_1686" s="T176">n</ta>
            <ta e="T178" id="Seg_1687" s="T177">v</ta>
            <ta e="T179" id="Seg_1688" s="T178">v</ta>
            <ta e="T203" id="Seg_1689" s="T202">v</ta>
            <ta e="T205" id="Seg_1690" s="T204">pers</ta>
            <ta e="T206" id="Seg_1691" s="T205">que</ta>
            <ta e="T207" id="Seg_1692" s="T206">ptcl</ta>
            <ta e="T208" id="Seg_1693" s="T207">v</ta>
            <ta e="T210" id="Seg_1694" s="T209">n</ta>
            <ta e="T211" id="Seg_1695" s="T210">quant</ta>
            <ta e="T212" id="Seg_1696" s="T211">v</ta>
            <ta e="T214" id="Seg_1697" s="T213">n</ta>
            <ta e="T215" id="Seg_1698" s="T214">conj</ta>
            <ta e="T216" id="Seg_1699" s="T215">ptcl</ta>
            <ta e="T217" id="Seg_1700" s="T216">v</ta>
            <ta e="T219" id="Seg_1701" s="T218">adv</ta>
            <ta e="T220" id="Seg_1702" s="T219">n</ta>
            <ta e="T221" id="Seg_1703" s="T220">conj</ta>
            <ta e="T222" id="Seg_1704" s="T221">n</ta>
            <ta e="T224" id="Seg_1705" s="T223">conj</ta>
            <ta e="T225" id="Seg_1706" s="T224">n</ta>
            <ta e="T226" id="Seg_1707" s="T225">v</ta>
            <ta e="T246" id="Seg_1708" s="T245">adv</ta>
            <ta e="T247" id="Seg_1709" s="T246">n</ta>
            <ta e="T248" id="Seg_1710" s="T247">que</ta>
            <ta e="T249" id="Seg_1711" s="T248">ptcl</ta>
            <ta e="T250" id="Seg_1712" s="T249">v</ta>
            <ta e="T252" id="Seg_1713" s="T251">v</ta>
            <ta e="T253" id="Seg_1714" s="T252">n</ta>
            <ta e="T255" id="Seg_1715" s="T254">n</ta>
            <ta e="T256" id="Seg_1716" s="T255">v</ta>
            <ta e="T268" id="Seg_1717" s="T267">dempro</ta>
            <ta e="T269" id="Seg_1718" s="T268">ptcl</ta>
            <ta e="T270" id="Seg_1719" s="T269">v</ta>
            <ta e="T272" id="Seg_1720" s="T271">adv</ta>
            <ta e="T273" id="Seg_1721" s="T272">pers</ta>
            <ta e="T274" id="Seg_1722" s="T273">n</ta>
            <ta e="T275" id="Seg_1723" s="T274">v</ta>
            <ta e="T276" id="Seg_1724" s="T275">v</ta>
            <ta e="T278" id="Seg_1725" s="T277">dempro</ta>
            <ta e="T279" id="Seg_1726" s="T278">dempro</ta>
            <ta e="T280" id="Seg_1727" s="T279">n</ta>
            <ta e="T281" id="Seg_1728" s="T280">v</ta>
            <ta e="T283" id="Seg_1729" s="T282">dempro</ta>
            <ta e="T284" id="Seg_1730" s="T283">dempro</ta>
            <ta e="T285" id="Seg_1731" s="T284">n</ta>
            <ta e="T288" id="Seg_1732" s="T287">v</ta>
            <ta e="T316" id="Seg_1733" s="T315">conj</ta>
            <ta e="T374" id="Seg_1734" s="T316">propr</ta>
            <ta e="T317" id="Seg_1735" s="T374">n</ta>
            <ta e="T318" id="Seg_1736" s="T317">v</ta>
            <ta e="T320" id="Seg_1737" s="T319">num</ta>
            <ta e="T321" id="Seg_1738" s="T320">adj</ta>
            <ta e="T322" id="Seg_1739" s="T321">n</ta>
            <ta e="T323" id="Seg_1740" s="T322">v</ta>
            <ta e="T324" id="Seg_1741" s="T323">conj</ta>
            <ta e="T325" id="Seg_1742" s="T324">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PKZ">
            <ta e="T64" id="Seg_1743" s="T63">0.2.h:A</ta>
            <ta e="T66" id="Seg_1744" s="T65">pro.h:Th</ta>
            <ta e="T68" id="Seg_1745" s="T67">0.2.h:A</ta>
            <ta e="T70" id="Seg_1746" s="T69">pro.h:Th</ta>
            <ta e="T71" id="Seg_1747" s="T70">0.2.h:A</ta>
            <ta e="T73" id="Seg_1748" s="T72">pro.h:Th</ta>
            <ta e="T75" id="Seg_1749" s="T74">pro.h:A</ta>
            <ta e="T77" id="Seg_1750" s="T76">pro.h:Poss</ta>
            <ta e="T78" id="Seg_1751" s="T77">np:Th</ta>
            <ta e="T81" id="Seg_1752" s="T80">pro.h:A</ta>
            <ta e="T83" id="Seg_1753" s="T82">pro.h:R</ta>
            <ta e="T84" id="Seg_1754" s="T83">np:Th</ta>
            <ta e="T88" id="Seg_1755" s="T87">0.2.h:A</ta>
            <ta e="T89" id="Seg_1756" s="T88">pro.h:R</ta>
            <ta e="T91" id="Seg_1757" s="T90">pro:R</ta>
            <ta e="T126" id="Seg_1758" s="T125">0.3:Th</ta>
            <ta e="T128" id="Seg_1759" s="T127">0.3:Th</ta>
            <ta e="T129" id="Seg_1760" s="T128">pro:L</ta>
            <ta e="T132" id="Seg_1761" s="T131">np:L</ta>
            <ta e="T133" id="Seg_1762" s="T132">0.3:Th</ta>
            <ta e="T136" id="Seg_1763" s="T135">adv:L</ta>
            <ta e="T139" id="Seg_1764" s="T138">np:Th</ta>
            <ta e="T140" id="Seg_1765" s="T139">np:Th</ta>
            <ta e="T141" id="Seg_1766" s="T140">adv:L</ta>
            <ta e="T142" id="Seg_1767" s="T141">pro.h:B</ta>
            <ta e="T155" id="Seg_1768" s="T154">np:Th</ta>
            <ta e="T157" id="Seg_1769" s="T156">np:Th</ta>
            <ta e="T158" id="Seg_1770" s="T157">adv:L</ta>
            <ta e="T163" id="Seg_1771" s="T162">np:Th</ta>
            <ta e="T164" id="Seg_1772" s="T163">adv:L</ta>
            <ta e="T166" id="Seg_1773" s="T165">pro:Th</ta>
            <ta e="T170" id="Seg_1774" s="T169">0.3.h:A</ta>
            <ta e="T171" id="Seg_1775" s="T170">np:P</ta>
            <ta e="T173" id="Seg_1776" s="T172">adv:Time</ta>
            <ta e="T174" id="Seg_1777" s="T173">pro.h:R</ta>
            <ta e="T175" id="Seg_1778" s="T174">0.3.h:A</ta>
            <ta e="T176" id="Seg_1779" s="T175">pro.h:A</ta>
            <ta e="T177" id="Seg_1780" s="T176">np:G</ta>
            <ta e="T203" id="Seg_1781" s="T202">0.3:Th</ta>
            <ta e="T205" id="Seg_1782" s="T204">pro.h:P</ta>
            <ta e="T210" id="Seg_1783" s="T209">np:Th</ta>
            <ta e="T214" id="Seg_1784" s="T213">np:P</ta>
            <ta e="T217" id="Seg_1785" s="T216">0.1.h:A</ta>
            <ta e="T225" id="Seg_1786" s="T224">np:P</ta>
            <ta e="T226" id="Seg_1787" s="T225">0.1.h:A</ta>
            <ta e="T246" id="Seg_1788" s="T245">adv:L</ta>
            <ta e="T247" id="Seg_1789" s="T246">np:P</ta>
            <ta e="T253" id="Seg_1790" s="T252">np:Th</ta>
            <ta e="T255" id="Seg_1791" s="T254">np:Th</ta>
            <ta e="T268" id="Seg_1792" s="T267">pro.h:A</ta>
            <ta e="T273" id="Seg_1793" s="T272">pro.h:Poss</ta>
            <ta e="T274" id="Seg_1794" s="T273">np.h:A</ta>
            <ta e="T279" id="Seg_1795" s="T278">pro:B</ta>
            <ta e="T280" id="Seg_1796" s="T279">np:Th</ta>
            <ta e="T281" id="Seg_1797" s="T280">0.3.h:A</ta>
            <ta e="T283" id="Seg_1798" s="T282">pro.h:A</ta>
            <ta e="T284" id="Seg_1799" s="T283">pro:B</ta>
            <ta e="T285" id="Seg_1800" s="T284">np:P</ta>
            <ta e="T317" id="Seg_1801" s="T316">np:L</ta>
            <ta e="T318" id="Seg_1802" s="T317">0.3.h:A</ta>
            <ta e="T322" id="Seg_1803" s="T321">np:Th</ta>
            <ta e="T323" id="Seg_1804" s="T322">0.3.h:A</ta>
            <ta e="T325" id="Seg_1805" s="T324">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-PKZ">
            <ta e="T64" id="Seg_1806" s="T63">v:pred 0.2.h:S</ta>
            <ta e="T66" id="Seg_1807" s="T65">pro.h:O</ta>
            <ta e="T68" id="Seg_1808" s="T67">v:pred 0.2.h:S</ta>
            <ta e="T70" id="Seg_1809" s="T69">pro.h:O</ta>
            <ta e="T71" id="Seg_1810" s="T70">v:pred 0.2.h:S</ta>
            <ta e="T73" id="Seg_1811" s="T72">pro.h:O</ta>
            <ta e="T75" id="Seg_1812" s="T74">pro.h:S</ta>
            <ta e="T76" id="Seg_1813" s="T75">v:pred</ta>
            <ta e="T78" id="Seg_1814" s="T77">np:O</ta>
            <ta e="T81" id="Seg_1815" s="T80">pro.h:S</ta>
            <ta e="T82" id="Seg_1816" s="T81">v:pred</ta>
            <ta e="T84" id="Seg_1817" s="T83">np:O</ta>
            <ta e="T88" id="Seg_1818" s="T87">v:pred 0.2.h:S</ta>
            <ta e="T126" id="Seg_1819" s="T125">v:pred 0.3:S</ta>
            <ta e="T128" id="Seg_1820" s="T127">v:pred 0.3:S</ta>
            <ta e="T133" id="Seg_1821" s="T132">v:pred 0.3:S</ta>
            <ta e="T138" id="Seg_1822" s="T136">v:pred</ta>
            <ta e="T139" id="Seg_1823" s="T138">np:S</ta>
            <ta e="T140" id="Seg_1824" s="T139">np:S</ta>
            <ta e="T143" id="Seg_1825" s="T142">v:pred</ta>
            <ta e="T155" id="Seg_1826" s="T154">np:S</ta>
            <ta e="T157" id="Seg_1827" s="T156">np:S</ta>
            <ta e="T159" id="Seg_1828" s="T158">v:pred</ta>
            <ta e="T163" id="Seg_1829" s="T162">np:S</ta>
            <ta e="T165" id="Seg_1830" s="T164">v:pred</ta>
            <ta e="T166" id="Seg_1831" s="T165">pro:S</ta>
            <ta e="T167" id="Seg_1832" s="T166">v:pred</ta>
            <ta e="T170" id="Seg_1833" s="T169">v:pred 0.3.h:S</ta>
            <ta e="T171" id="Seg_1834" s="T170">np:O</ta>
            <ta e="T175" id="Seg_1835" s="T174">v:pred 0.3.h:S</ta>
            <ta e="T176" id="Seg_1836" s="T175">pro.h:S</ta>
            <ta e="T178" id="Seg_1837" s="T177">v:pred</ta>
            <ta e="T179" id="Seg_1838" s="T178">s:purp</ta>
            <ta e="T203" id="Seg_1839" s="T202">v:pred 0.3:S</ta>
            <ta e="T205" id="Seg_1840" s="T204">pro.h:S</ta>
            <ta e="T207" id="Seg_1841" s="T206">ptcl.neg</ta>
            <ta e="T208" id="Seg_1842" s="T207">v:pred</ta>
            <ta e="T210" id="Seg_1843" s="T209">np:S</ta>
            <ta e="T211" id="Seg_1844" s="T210">adj:pred</ta>
            <ta e="T212" id="Seg_1845" s="T211">cop</ta>
            <ta e="T214" id="Seg_1846" s="T213">np:O</ta>
            <ta e="T216" id="Seg_1847" s="T215">ptcl.neg</ta>
            <ta e="T217" id="Seg_1848" s="T216">v:pred 0.1.h:S</ta>
            <ta e="T225" id="Seg_1849" s="T224">np:O</ta>
            <ta e="T226" id="Seg_1850" s="T225">v:pred 0.1.h:S</ta>
            <ta e="T247" id="Seg_1851" s="T246">np:S</ta>
            <ta e="T249" id="Seg_1852" s="T248">ptcl.neg</ta>
            <ta e="T250" id="Seg_1853" s="T249">v:pred</ta>
            <ta e="T252" id="Seg_1854" s="T251">v:pred</ta>
            <ta e="T253" id="Seg_1855" s="T252">np:S</ta>
            <ta e="T255" id="Seg_1856" s="T254">np:S</ta>
            <ta e="T256" id="Seg_1857" s="T255">v:pred</ta>
            <ta e="T268" id="Seg_1858" s="T267">pro.h:S</ta>
            <ta e="T269" id="Seg_1859" s="T268">ptcl.neg</ta>
            <ta e="T270" id="Seg_1860" s="T269">v:pred</ta>
            <ta e="T274" id="Seg_1861" s="T273">np.h:S</ta>
            <ta e="T276" id="Seg_1862" s="T275">v:pred</ta>
            <ta e="T280" id="Seg_1863" s="T279">np:O</ta>
            <ta e="T281" id="Seg_1864" s="T280">v:pred 0.3.h:S</ta>
            <ta e="T283" id="Seg_1865" s="T282">pro.h:S</ta>
            <ta e="T285" id="Seg_1866" s="T284">np:O</ta>
            <ta e="T288" id="Seg_1867" s="T287">v:pred</ta>
            <ta e="T318" id="Seg_1868" s="T317">v:pred 0.3.h:S</ta>
            <ta e="T322" id="Seg_1869" s="T321">np:O</ta>
            <ta e="T323" id="Seg_1870" s="T322">v:pred 0.3.h:S</ta>
            <ta e="T325" id="Seg_1871" s="T324">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST-PKZ" />
         <annotation name="BOR" tierref="BOR-PKZ">
            <ta e="T131" id="Seg_1872" s="T130">RUS:mod</ta>
            <ta e="T132" id="Seg_1873" s="T373">TAT:cult</ta>
            <ta e="T139" id="Seg_1874" s="T138">RUS:cult</ta>
            <ta e="T155" id="Seg_1875" s="T154">TURK:cult</ta>
            <ta e="T156" id="Seg_1876" s="T155">RUS:gram</ta>
            <ta e="T163" id="Seg_1877" s="T162">TURK:cult</ta>
            <ta e="T171" id="Seg_1878" s="T170">TURK:cult</ta>
            <ta e="T206" id="Seg_1879" s="T205">TURK:gram(INDEF)</ta>
            <ta e="T214" id="Seg_1880" s="T213">TURK:cult</ta>
            <ta e="T215" id="Seg_1881" s="T214">RUS:gram</ta>
            <ta e="T219" id="Seg_1882" s="T218">TURK:core</ta>
            <ta e="T221" id="Seg_1883" s="T220">RUS:gram</ta>
            <ta e="T224" id="Seg_1884" s="T223">RUS:gram</ta>
            <ta e="T248" id="Seg_1885" s="T247">TURK:gram(INDEF)</ta>
            <ta e="T253" id="Seg_1886" s="T252">RUS:cult</ta>
            <ta e="T272" id="Seg_1887" s="T271">RUS:mod</ta>
            <ta e="T274" id="Seg_1888" s="T273">TURK:core</ta>
            <ta e="T285" id="Seg_1889" s="T284">TURK:cult</ta>
            <ta e="T316" id="Seg_1890" s="T315">RUS:gram</ta>
            <ta e="T317" id="Seg_1891" s="T374">TAT:cult</ta>
            <ta e="T321" id="Seg_1892" s="T320">RUS:core</ta>
            <ta e="T324" id="Seg_1893" s="T323">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-PKZ" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-PKZ" />
         <annotation name="CS" tierref="CS-PKZ">
            <ta e="T12" id="Seg_1894" s="T1">RUS:ext</ta>
            <ta e="T46" id="Seg_1895" s="T40">RUS:ext</ta>
            <ta e="T50" id="Seg_1896" s="T47">RUS:ext</ta>
            <ta e="T59" id="Seg_1897" s="T56">RUS:ext</ta>
            <ta e="T110" id="Seg_1898" s="T108">RUS:ext</ta>
            <ta e="T296" id="Seg_1899" s="T293">RUS:ext</ta>
            <ta e="T305" id="Seg_1900" s="T297">RUS:ext</ta>
            <ta e="T332" id="Seg_1901" s="T328">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-PKZ">
            <ta e="T66" id="Seg_1902" s="T60">Ты, Бог, Бог, не оставляй меня.</ta>
            <ta e="T73" id="Seg_1903" s="T67">Не оставляй меня, не бросай меня.</ta>
            <ta e="T79" id="Seg_1904" s="T74">Ты возьми у меня сердце глупое.</ta>
            <ta e="T86" id="Seg_1905" s="T80">Ты дай мне сердце, в котором много мудрости.</ta>
            <ta e="T91" id="Seg_1906" s="T87">Научи меня тебя восхвалять.</ta>
            <ta e="T126" id="Seg_1907" s="T125">Не было.</ta>
            <ta e="T129" id="Seg_1908" s="T127">Не было у нас.</ta>
            <ta e="T133" id="Seg_1909" s="T130">Только в Агинском было.</ta>
            <ta e="T139" id="Seg_1910" s="T135">Там были платки (/Они покупали там платки).</ta>
            <ta e="T143" id="Seg_1911" s="T139">Они там одежду покупали.</ta>
            <ta e="T159" id="Seg_1912" s="T154">Соль и сахар там покупали.</ta>
            <ta e="T167" id="Seg_1913" s="T160">И хлеб там покупали они, не было. [?]</ta>
            <ta e="T171" id="Seg_1914" s="T168">Насушат хлеба.</ta>
            <ta e="T179" id="Seg_1915" s="T172">Потом дадут (ему?), и они идут в лес охотиться.</ta>
            <ta e="T203" id="Seg_1916" s="T202">Не было.</ta>
            <ta e="T208" id="Seg_1917" s="T204">Мы (ни от чего?) не умирали.</ta>
            <ta e="T212" id="Seg_1918" s="T209">Мяса много было.</ta>
            <ta e="T217" id="Seg_1919" s="T213">Хлеб мы не ели.</ta>
            <ta e="T222" id="Seg_1920" s="T218">Все время мясо да мясо.</ta>
            <ta e="T226" id="Seg_1921" s="T223">Да воду пили.</ta>
            <ta e="T250" id="Seg_1922" s="T244">Вода там никогда не замерзала.</ta>
            <ta e="T253" id="Seg_1923" s="T251">Не [было] колодцев.</ta>
            <ta e="T256" id="Seg_1924" s="T254">Вода течет.</ta>
            <ta e="T270" id="Seg_1925" s="T267">Они не пахали.</ta>
            <ta e="T276" id="Seg_1926" s="T271">Только мой отец пахал.</ta>
            <ta e="T281" id="Seg_1927" s="T277">Он им картошку сажал.</ta>
            <ta e="T288" id="Seg_1928" s="T282">Они ему жали хлеб.</ta>
            <ta e="T318" id="Seg_1929" s="T315">А в Агинском возьмут.</ta>
            <ta e="T325" id="Seg_1930" s="T319">(На троих?) целое ведро возьмут и пьют.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-PKZ">
            <ta e="T12" id="Seg_1931" s="T1">She told me, "Go, (…) what a good person."</ta>
            <ta e="T46" id="Seg_1932" s="T40">I won’t sing on Kamass, but like that …</ta>
            <ta e="T50" id="Seg_1933" s="T47">I will sing one verse.</ta>
            <ta e="T59" id="Seg_1934" s="T56">Kamass (…)</ta>
            <ta e="T66" id="Seg_1935" s="T60">You, God, God, don't leave me.</ta>
            <ta e="T73" id="Seg_1936" s="T67">Don't leave me, don’t abandon me.</ta>
            <ta e="T79" id="Seg_1937" s="T74">You take from me my stupid heart.</ta>
            <ta e="T86" id="Seg_1938" s="T80">You give me a heart which has much wisdom.</ta>
            <ta e="T91" id="Seg_1939" s="T87">Teach me to praise you.</ta>
            <ta e="T110" id="Seg_1940" s="T108">Thank you.</ta>
            <ta e="T126" id="Seg_1941" s="T125">There was no.</ta>
            <ta e="T129" id="Seg_1942" s="T127">In our [village] there wasn't any.</ta>
            <ta e="T133" id="Seg_1943" s="T130">There was only in Aginskoye.</ta>
            <ta e="T139" id="Seg_1944" s="T135">There were scarves (/They bought there scarves).</ta>
            <ta e="T143" id="Seg_1945" s="T139">They bought clothes there.</ta>
            <ta e="T159" id="Seg_1946" s="T154">They bought there salt and sugar.</ta>
            <ta e="T167" id="Seg_1947" s="T160">They bought there bread, too, there wasn't. [?]</ta>
            <ta e="T171" id="Seg_1948" s="T168">They will dry bread.</ta>
            <ta e="T179" id="Seg_1949" s="T172">Then they give [it] (him?), and they go hunting into the forest.</ta>
            <ta e="T203" id="Seg_1950" s="T202">There wasn't.</ta>
            <ta e="T208" id="Seg_1951" s="T204">We didn't die (for any reason?).</ta>
            <ta e="T212" id="Seg_1952" s="T209">There was much meat.</ta>
            <ta e="T217" id="Seg_1953" s="T213">We didn't eat bread.</ta>
            <ta e="T222" id="Seg_1954" s="T218">It was always meat.</ta>
            <ta e="T226" id="Seg_1955" s="T223">And we drank water.</ta>
            <ta e="T250" id="Seg_1956" s="T244">Water didn't freeze there.</ta>
            <ta e="T253" id="Seg_1957" s="T251">There were no wells.</ta>
            <ta e="T256" id="Seg_1958" s="T254">Water flows.</ta>
            <ta e="T270" id="Seg_1959" s="T267">They didn't plough.</ta>
            <ta e="T276" id="Seg_1960" s="T271">Only my father ploughed.</ta>
            <ta e="T281" id="Seg_1961" s="T277">He planted potatoes for them.</ta>
            <ta e="T288" id="Seg_1962" s="T282">They harvested bread for him.</ta>
            <ta e="T296" id="Seg_1963" s="T293">They didn't plough.</ta>
            <ta e="T305" id="Seg_1964" s="T297">Only my father ploughed.</ta>
            <ta e="T318" id="Seg_1965" s="T315">They bought it in Aginskoye.</ta>
            <ta e="T325" id="Seg_1966" s="T319">They bought a full bucket [of it] (for three persons?) and drank it.</ta>
            <ta e="T332" id="Seg_1967" s="T328">They will buy a whole bucket and get drunk.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PKZ">
            <ta e="T12" id="Seg_1968" s="T1">Sie wies mich an: "Geh, (…) was für ein guter Mensch."</ta>
            <ta e="T46" id="Seg_1969" s="T40">Ich werde nicht auf Kamassisch singen, aber so…</ta>
            <ta e="T50" id="Seg_1970" s="T47">Ich werde einen Vers singen.</ta>
            <ta e="T59" id="Seg_1971" s="T56">Kamassisch (…)</ta>
            <ta e="T66" id="Seg_1972" s="T60">Du, Gott, Gott, verlass mich nicht.</ta>
            <ta e="T73" id="Seg_1973" s="T67">Verlass mich nicht, wirf [= verlass] mich nicht.</ta>
            <ta e="T79" id="Seg_1974" s="T74">Du nimmst mir mein dummes Herz weg.</ta>
            <ta e="T86" id="Seg_1975" s="T80">Du gibst mir ein Herz, das viel Weisheit hat.</ta>
            <ta e="T91" id="Seg_1976" s="T87">Lehre mich, um dich zu loben.</ta>
            <ta e="T110" id="Seg_1977" s="T108">Danke.</ta>
            <ta e="T126" id="Seg_1978" s="T125">Es gibt nichts.</ta>
            <ta e="T129" id="Seg_1979" s="T127">In unserem [Dorf], gab es keins.</ta>
            <ta e="T133" id="Seg_1980" s="T130">Nur in Aginskoye.</ta>
            <ta e="T139" id="Seg_1981" s="T135">Es gab Schals (/Sie kauften dort Schals). </ta>
            <ta e="T143" id="Seg_1982" s="T139">Sie kauften dort Kleidung.</ta>
            <ta e="T159" id="Seg_1983" s="T154">Sie kauften dort Salz und Zucker.</ta>
            <ta e="T167" id="Seg_1984" s="T160">Sie kauften dort auch Brot, da war nicht. [?]</ta>
            <ta e="T171" id="Seg_1985" s="T168">Sie werden das Brot trocknen.</ta>
            <ta e="T179" id="Seg_1986" s="T172">Dann geben sie [es] (ihm?), und sie gehen in den Wald jagen.</ta>
            <ta e="T203" id="Seg_1987" s="T202">Es gibt nichts.</ta>
            <ta e="T208" id="Seg_1988" s="T204">Wir sind nicht gestorben.</ta>
            <ta e="T212" id="Seg_1989" s="T209">Es gab viel Fleisch.</ta>
            <ta e="T217" id="Seg_1990" s="T213">Wir aßen kein Brot.</ta>
            <ta e="T222" id="Seg_1991" s="T218">Es war immer Fleisch.</ta>
            <ta e="T226" id="Seg_1992" s="T223">Und wir tranken Wasser.</ta>
            <ta e="T250" id="Seg_1993" s="T244">Wasser gefror dort nicht.</ta>
            <ta e="T253" id="Seg_1994" s="T251">Es gab keine Brunnen dort.</ta>
            <ta e="T256" id="Seg_1995" s="T254">Wasser fließt.</ta>
            <ta e="T270" id="Seg_1996" s="T267">Sie pflügten nicht.</ta>
            <ta e="T276" id="Seg_1997" s="T271">Nur mein Vater pflügte.</ta>
            <ta e="T281" id="Seg_1998" s="T277">Er pflanzte Kartoffeln für sie.</ta>
            <ta e="T288" id="Seg_1999" s="T282">Sie ernteten Brot für ihn.</ta>
            <ta e="T296" id="Seg_2000" s="T293">Sie pflügten nicht.</ta>
            <ta e="T305" id="Seg_2001" s="T297">Nur mein Vater hat gepflügt.</ta>
            <ta e="T318" id="Seg_2002" s="T315">Sie kauften es in Aginskoye.</ta>
            <ta e="T325" id="Seg_2003" s="T319">Sie kauften einen vollen Eimer [davon] (für drei Personen) und tranken es. </ta>
            <ta e="T332" id="Seg_2004" s="T328">Sie werden einen ganzen Eimer kaufen und sich betrinken.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-PKZ">
            <ta e="T91" id="Seg_2005" s="T87">[WNB] tüšüʔ- 'teach'</ta>
            <ta e="T167" id="Seg_2006" s="T160">[GVY:] They have no bread?</ta>
            <ta e="T179" id="Seg_2007" s="T172">[GVY:] Not clear, to whom they gave the bread.</ta>
            <ta e="T217" id="Seg_2008" s="T213">[GVY:] "da" is unclear.</ta>
            <ta e="T325" id="Seg_2009" s="T319">[GVY:] nagur tĭ?</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-KA"
                      id="tx-KA"
                      speaker="KA"
                      type="t">
         <timeline-fork end="T137" start="T134">
            <tli id="T134.tx-KA.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KA">
            <ts e="T24" id="Seg_2010" n="sc" s="T8">
               <ts e="T24" id="Seg_2012" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_2014" n="HIAT:w" s="T8">Ты</ts>
                  <nts id="Seg_2015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_2017" n="HIAT:w" s="T9">прости</ts>
                  <nts id="Seg_2018" n="HIAT:ip">,</nts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_2021" n="HIAT:w" s="T11">сейчас</ts>
                  <nts id="Seg_2022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_2024" n="HIAT:w" s="T13">меня</ts>
                  <nts id="Seg_2025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_2027" n="HIAT:w" s="T14">хотят</ts>
                  <nts id="Seg_2028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_2030" n="HIAT:w" s="T15">для</ts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_2033" n="HIAT:w" s="T16">радио</ts>
                  <nts id="Seg_2034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_2036" n="HIAT:w" s="T17">спросить</ts>
                  <nts id="Seg_2037" n="HIAT:ip">,</nts>
                  <nts id="Seg_2038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_2040" n="HIAT:w" s="T18">я</ts>
                  <nts id="Seg_2041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_2043" n="HIAT:w" s="T19">что-то</ts>
                  <nts id="Seg_2044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_2046" n="HIAT:w" s="T20">им</ts>
                  <nts id="Seg_2047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_2049" n="HIAT:w" s="T21">тут</ts>
                  <nts id="Seg_2050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_2052" n="HIAT:w" s="T22">наболтаю</ts>
                  <nts id="Seg_2053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_2055" n="HIAT:w" s="T23">сейчас</ts>
                  <nts id="Seg_2056" n="HIAT:ip">.</nts>
                  <nts id="Seg_2057" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T26" id="Seg_2058" n="sc" s="T25">
               <ts e="T26" id="Seg_2060" n="HIAT:u" s="T25">
                  <nts id="Seg_2061" n="HIAT:ip">(</nts>
                  <nts id="Seg_2062" n="HIAT:ip">(</nts>
                  <ats e="T26" id="Seg_2063" n="HIAT:non-pho" s="T25">…</ats>
                  <nts id="Seg_2064" n="HIAT:ip">)</nts>
                  <nts id="Seg_2065" n="HIAT:ip">)</nts>
                  <nts id="Seg_2066" n="HIAT:ip">.</nts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T33" id="Seg_2068" n="sc" s="T27">
               <ts e="T33" id="Seg_2070" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_2072" n="HIAT:w" s="T27">Ну</ts>
                  <nts id="Seg_2073" n="HIAT:ip">,</nts>
                  <nts id="Seg_2074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_2076" n="HIAT:w" s="T28">как</ts>
                  <nts id="Seg_2077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_2079" n="HIAT:w" s="T29">у</ts>
                  <nts id="Seg_2080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_2082" n="HIAT:w" s="T30">тебя</ts>
                  <nts id="Seg_2083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_2085" n="HIAT:w" s="T31">насчет</ts>
                  <nts id="Seg_2086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_2088" n="HIAT:w" s="T32">песенки</ts>
                  <nts id="Seg_2089" n="HIAT:ip">?</nts>
                  <nts id="Seg_2090" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T39" id="Seg_2091" n="sc" s="T34">
               <ts e="T39" id="Seg_2093" n="HIAT:u" s="T34">
                  <nts id="Seg_2094" n="HIAT:ip">(</nts>
                  <ts e="T35" id="Seg_2096" n="HIAT:w" s="T34">Как=</ts>
                  <nts id="Seg_2097" n="HIAT:ip">)</nts>
                  <nts id="Seg_2098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_2100" n="HIAT:w" s="T35">Что-нибудь</ts>
                  <nts id="Seg_2101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_2103" n="HIAT:w" s="T36">камасинское</ts>
                  <nts id="Seg_2104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_2106" n="HIAT:w" s="T37">вспомнится</ts>
                  <nts id="Seg_2107" n="HIAT:ip">,</nts>
                  <nts id="Seg_2108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_2110" n="HIAT:w" s="T38">нет</ts>
                  <nts id="Seg_2111" n="HIAT:ip">?</nts>
                  <nts id="Seg_2112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T57" id="Seg_2113" n="sc" s="T51">
               <ts e="T57" id="Seg_2115" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_2117" n="HIAT:w" s="T51">Все</ts>
                  <nts id="Seg_2118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_2120" n="HIAT:w" s="T52">же</ts>
                  <nts id="Seg_2121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_2123" n="HIAT:w" s="T53">на</ts>
                  <nts id="Seg_2124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_2126" n="HIAT:w" s="T54">камасинском</ts>
                  <nts id="Seg_2127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2128" n="HIAT:ip">(</nts>
                  <nts id="Seg_2129" n="HIAT:ip">(</nts>
                  <ats e="T57" id="Seg_2130" n="HIAT:non-pho" s="T55">…</ats>
                  <nts id="Seg_2131" n="HIAT:ip">)</nts>
                  <nts id="Seg_2132" n="HIAT:ip">)</nts>
                  <nts id="Seg_2133" n="HIAT:ip">.</nts>
                  <nts id="Seg_2134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T93" id="Seg_2135" n="sc" s="T92">
               <ts e="T93" id="Seg_2137" n="HIAT:u" s="T92">
                  <nts id="Seg_2138" n="HIAT:ip">(</nts>
                  <nts id="Seg_2139" n="HIAT:ip">(</nts>
                  <ats e="T93" id="Seg_2140" n="HIAT:non-pho" s="T92">…</ats>
                  <nts id="Seg_2141" n="HIAT:ip">)</nts>
                  <nts id="Seg_2142" n="HIAT:ip">)</nts>
                  <nts id="Seg_2143" n="HIAT:ip">.</nts>
                  <nts id="Seg_2144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T109" id="Seg_2145" n="sc" s="T94">
               <ts e="T109" id="Seg_2147" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_2149" n="HIAT:w" s="T94">И</ts>
                  <nts id="Seg_2150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_2152" n="HIAT:w" s="T95">они</ts>
                  <nts id="Seg_2153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_2155" n="HIAT:w" s="T96">желают</ts>
                  <nts id="Seg_2156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_2158" n="HIAT:w" s="T97">тебе</ts>
                  <nts id="Seg_2159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_2161" n="HIAT:w" s="T98">много</ts>
                  <nts id="Seg_2162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_2164" n="HIAT:w" s="T99">лет</ts>
                  <nts id="Seg_2165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_2167" n="HIAT:w" s="T100">жизни</ts>
                  <nts id="Seg_2168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_2170" n="HIAT:w" s="T101">и</ts>
                  <nts id="Seg_2171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_2173" n="HIAT:w" s="T102">всего</ts>
                  <nts id="Seg_2174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_2176" n="HIAT:w" s="T103">хорошего</ts>
                  <nts id="Seg_2177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_2179" n="HIAT:w" s="T104">и</ts>
                  <nts id="Seg_2180" n="HIAT:ip">,</nts>
                  <nts id="Seg_2181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_2183" n="HIAT:w" s="T105">значит</ts>
                  <nts id="Seg_2184" n="HIAT:ip">,</nts>
                  <nts id="Seg_2185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_2187" n="HIAT:w" s="T106">радостного</ts>
                  <nts id="Seg_2188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_2190" n="HIAT:w" s="T107">настроения</ts>
                  <nts id="Seg_2191" n="HIAT:ip">.</nts>
                  <nts id="Seg_2192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T112" id="Seg_2193" n="sc" s="T111">
               <ts e="T112" id="Seg_2195" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_2197" n="HIAT:w" s="T111">Kiitos</ts>
                  <nts id="Seg_2198" n="HIAT:ip">.</nts>
                  <nts id="Seg_2199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T114" id="Seg_2200" n="sc" s="T113">
               <ts e="T114" id="Seg_2202" n="HIAT:u" s="T113">
                  <nts id="Seg_2203" n="HIAT:ip">(</nts>
                  <nts id="Seg_2204" n="HIAT:ip">(</nts>
                  <ats e="T114" id="Seg_2205" n="HIAT:non-pho" s="T113">…</ats>
                  <nts id="Seg_2206" n="HIAT:ip">)</nts>
                  <nts id="Seg_2207" n="HIAT:ip">)</nts>
                  <nts id="Seg_2208" n="HIAT:ip">.</nts>
                  <nts id="Seg_2209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T124" id="Seg_2210" n="sc" s="T116">
               <ts e="T124" id="Seg_2212" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_2214" n="HIAT:w" s="T116">магазинов</ts>
                  <nts id="Seg_2215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_2217" n="HIAT:w" s="T117">было</ts>
                  <nts id="Seg_2218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_2220" n="HIAT:w" s="T118">тогда</ts>
                  <nts id="Seg_2221" n="HIAT:ip">,</nts>
                  <nts id="Seg_2222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_2224" n="HIAT:w" s="T119">когда</ts>
                  <nts id="Seg_2225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_2227" n="HIAT:w" s="T120">ты</ts>
                  <nts id="Seg_2228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_2230" n="HIAT:w" s="T121">еще</ts>
                  <nts id="Seg_2231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_2233" n="HIAT:w" s="T122">была</ts>
                  <nts id="Seg_2234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_2236" n="HIAT:w" s="T123">мала</ts>
                  <nts id="Seg_2237" n="HIAT:ip">.</nts>
                  <nts id="Seg_2238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T137" id="Seg_2239" n="sc" s="T134">
               <ts e="T137" id="Seg_2241" n="HIAT:u" s="T134">
                  <nts id="Seg_2242" n="HIAT:ip">(</nts>
                  <nts id="Seg_2243" n="HIAT:ip">(</nts>
                  <ats e="T134.tx-KA.1" id="Seg_2244" n="HIAT:non-pho" s="T134">…</ats>
                  <nts id="Seg_2245" n="HIAT:ip">)</nts>
                  <nts id="Seg_2246" n="HIAT:ip">)</nts>
                  <nts id="Seg_2247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2248" n="HIAT:ip">(</nts>
                  <nts id="Seg_2249" n="HIAT:ip">(</nts>
                  <ats e="T137" id="Seg_2250" n="HIAT:non-pho" s="T134.tx-KA.1">NOISE</ats>
                  <nts id="Seg_2251" n="HIAT:ip">)</nts>
                  <nts id="Seg_2252" n="HIAT:ip">)</nts>
                  <nts id="Seg_2253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T145" id="Seg_2254" n="sc" s="T144">
               <ts e="T145" id="Seg_2256" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_2258" n="HIAT:w" s="T144">Ja</ts>
                  <nts id="Seg_2259" n="HIAT:ip">.</nts>
                  <nts id="Seg_2260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T147" id="Seg_2261" n="sc" s="T146">
               <ts e="T147" id="Seg_2263" n="HIAT:u" s="T146">
                  <nts id="Seg_2264" n="HIAT:ip">(</nts>
                  <nts id="Seg_2265" n="HIAT:ip">(</nts>
                  <ats e="T147" id="Seg_2266" n="HIAT:non-pho" s="T146">…</ats>
                  <nts id="Seg_2267" n="HIAT:ip">)</nts>
                  <nts id="Seg_2268" n="HIAT:ip">)</nts>
                  <nts id="Seg_2269" n="HIAT:ip">.</nts>
                  <nts id="Seg_2270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T153" id="Seg_2271" n="sc" s="T148">
               <ts e="T149" id="Seg_2273" n="HIAT:u" s="T148">
                  <nts id="Seg_2274" n="HIAT:ip">(</nts>
                  <nts id="Seg_2275" n="HIAT:ip">(</nts>
                  <ats e="T149" id="Seg_2276" n="HIAT:non-pho" s="T148">…</ats>
                  <nts id="Seg_2277" n="HIAT:ip">)</nts>
                  <nts id="Seg_2278" n="HIAT:ip">)</nts>
                  <nts id="Seg_2279" n="HIAT:ip">.</nts>
                  <nts id="Seg_2280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_2282" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_2284" n="HIAT:w" s="T149">Наверное</ts>
                  <nts id="Seg_2285" n="HIAT:ip">,</nts>
                  <nts id="Seg_2286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_2288" n="HIAT:w" s="T150">соль</ts>
                  <nts id="Seg_2289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_2291" n="HIAT:w" s="T151">и</ts>
                  <nts id="Seg_2292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_2294" n="HIAT:w" s="T152">сахар</ts>
                  <nts id="Seg_2295" n="HIAT:ip">.</nts>
                  <nts id="Seg_2296" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T161" id="Seg_2297" n="sc" s="T160">
               <ts e="T161" id="Seg_2299" n="HIAT:u" s="T160">
                  <nts id="Seg_2300" n="HIAT:ip">(</nts>
                  <ts e="T161" id="Seg_2302" n="HIAT:w" s="T160">ši-</ts>
                  <nts id="Seg_2303" n="HIAT:ip">)</nts>
                  <nts id="Seg_2304" n="HIAT:ip">…</nts>
                  <nts id="Seg_2305" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T181" id="Seg_2306" n="sc" s="T180">
               <ts e="T181" id="Seg_2308" n="HIAT:u" s="T180">
                  <nts id="Seg_2309" n="HIAT:ip">(</nts>
                  <nts id="Seg_2310" n="HIAT:ip">(</nts>
                  <ats e="T181" id="Seg_2311" n="HIAT:non-pho" s="T180">…</ats>
                  <nts id="Seg_2312" n="HIAT:ip">)</nts>
                  <nts id="Seg_2313" n="HIAT:ip">)</nts>
                  <nts id="Seg_2314" n="HIAT:ip">.</nts>
                  <nts id="Seg_2315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T183" id="Seg_2316" n="sc" s="T182">
               <ts e="T183" id="Seg_2318" n="HIAT:u" s="T182">
                  <nts id="Seg_2319" n="HIAT:ip">(</nts>
                  <nts id="Seg_2320" n="HIAT:ip">(</nts>
                  <ats e="T183" id="Seg_2321" n="HIAT:non-pho" s="T182">…</ats>
                  <nts id="Seg_2322" n="HIAT:ip">)</nts>
                  <nts id="Seg_2323" n="HIAT:ip">)</nts>
                  <nts id="Seg_2324" n="HIAT:ip">.</nts>
                  <nts id="Seg_2325" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T186" id="Seg_2326" n="sc" s="T184">
               <ts e="T185" id="Seg_2328" n="HIAT:u" s="T184">
                  <nts id="Seg_2329" n="HIAT:ip">(</nts>
                  <nts id="Seg_2330" n="HIAT:ip">(</nts>
                  <ats e="T185" id="Seg_2331" n="HIAT:non-pho" s="T184">…</ats>
                  <nts id="Seg_2332" n="HIAT:ip">)</nts>
                  <nts id="Seg_2333" n="HIAT:ip">)</nts>
                  <nts id="Seg_2334" n="HIAT:ip">.</nts>
                  <nts id="Seg_2335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_2337" n="HIAT:u" s="T185">
                  <nts id="Seg_2338" n="HIAT:ip">(</nts>
                  <nts id="Seg_2339" n="HIAT:ip">(</nts>
                  <ats e="T186" id="Seg_2340" n="HIAT:non-pho" s="T185">…</ats>
                  <nts id="Seg_2341" n="HIAT:ip">)</nts>
                  <nts id="Seg_2342" n="HIAT:ip">)</nts>
                  <nts id="Seg_2343" n="HIAT:ip">.</nts>
                  <nts id="Seg_2344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T188" id="Seg_2345" n="sc" s="T187">
               <ts e="T188" id="Seg_2347" n="HIAT:u" s="T187">
                  <nts id="Seg_2348" n="HIAT:ip">(</nts>
                  <nts id="Seg_2349" n="HIAT:ip">(</nts>
                  <ats e="T188" id="Seg_2350" n="HIAT:non-pho" s="T187">…</ats>
                  <nts id="Seg_2351" n="HIAT:ip">)</nts>
                  <nts id="Seg_2352" n="HIAT:ip">)</nts>
                  <nts id="Seg_2353" n="HIAT:ip">.</nts>
                  <nts id="Seg_2354" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T201" id="Seg_2355" n="sc" s="T189">
               <ts e="T201" id="Seg_2357" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_2359" n="HIAT:w" s="T189">Был</ts>
                  <nts id="Seg_2360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_2362" n="HIAT:w" s="T190">ли</ts>
                  <nts id="Seg_2363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_2365" n="HIAT:w" s="T191">часто</ts>
                  <nts id="Seg_2366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_2368" n="HIAT:w" s="T192">голод</ts>
                  <nts id="Seg_2369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_2371" n="HIAT:w" s="T193">в</ts>
                  <nts id="Seg_2372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_2374" n="HIAT:w" s="T194">те</ts>
                  <nts id="Seg_2375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_2377" n="HIAT:w" s="T195">времена</ts>
                  <nts id="Seg_2378" n="HIAT:ip">,</nts>
                  <nts id="Seg_2379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_2381" n="HIAT:w" s="T196">когда</ts>
                  <nts id="Seg_2382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_2384" n="HIAT:w" s="T197">ты</ts>
                  <nts id="Seg_2385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_2387" n="HIAT:w" s="T198">была</ts>
                  <nts id="Seg_2388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_2390" n="HIAT:w" s="T199">еще</ts>
                  <nts id="Seg_2391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_2393" n="HIAT:w" s="T200">мала</ts>
                  <nts id="Seg_2394" n="HIAT:ip">?</nts>
                  <nts id="Seg_2395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T228" id="Seg_2396" n="sc" s="T227">
               <ts e="T228" id="Seg_2398" n="HIAT:u" s="T227">
                  <nts id="Seg_2399" n="HIAT:ip">(</nts>
                  <nts id="Seg_2400" n="HIAT:ip">(</nts>
                  <ats e="T228" id="Seg_2401" n="HIAT:non-pho" s="T227">…</ats>
                  <nts id="Seg_2402" n="HIAT:ip">)</nts>
                  <nts id="Seg_2403" n="HIAT:ip">)</nts>
                  <nts id="Seg_2404" n="HIAT:ip">.</nts>
                  <nts id="Seg_2405" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T230" id="Seg_2406" n="sc" s="T229">
               <ts e="T230" id="Seg_2408" n="HIAT:u" s="T229">
                  <nts id="Seg_2409" n="HIAT:ip">(</nts>
                  <nts id="Seg_2410" n="HIAT:ip">(</nts>
                  <ats e="T230" id="Seg_2411" n="HIAT:non-pho" s="T229">…</ats>
                  <nts id="Seg_2412" n="HIAT:ip">)</nts>
                  <nts id="Seg_2413" n="HIAT:ip">)</nts>
                  <nts id="Seg_2414" n="HIAT:ip">.</nts>
                  <nts id="Seg_2415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T232" id="Seg_2416" n="sc" s="T231">
               <ts e="T232" id="Seg_2418" n="HIAT:u" s="T231">
                  <nts id="Seg_2419" n="HIAT:ip">(</nts>
                  <nts id="Seg_2420" n="HIAT:ip">(</nts>
                  <ats e="T232" id="Seg_2421" n="HIAT:non-pho" s="T231">…</ats>
                  <nts id="Seg_2422" n="HIAT:ip">)</nts>
                  <nts id="Seg_2423" n="HIAT:ip">)</nts>
                  <nts id="Seg_2424" n="HIAT:ip">.</nts>
                  <nts id="Seg_2425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T241" id="Seg_2426" n="sc" s="T233">
               <ts e="T241" id="Seg_2428" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_2430" n="HIAT:w" s="T233">Делали</ts>
                  <nts id="Seg_2431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_2433" n="HIAT:w" s="T234">ли</ts>
                  <nts id="Seg_2434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_2436" n="HIAT:w" s="T235">колодец</ts>
                  <nts id="Seg_2437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_2439" n="HIAT:w" s="T236">для</ts>
                  <nts id="Seg_2440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_2442" n="HIAT:w" s="T237">того</ts>
                  <nts id="Seg_2443" n="HIAT:ip">,</nts>
                  <nts id="Seg_2444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_2446" n="HIAT:w" s="T238">чтобы</ts>
                  <nts id="Seg_2447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_2449" n="HIAT:w" s="T239">достать</ts>
                  <nts id="Seg_2450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_2452" n="HIAT:w" s="T240">воду</ts>
                  <nts id="Seg_2453" n="HIAT:ip">?</nts>
                  <nts id="Seg_2454" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T243" id="Seg_2455" n="sc" s="T242">
               <ts e="T243" id="Seg_2457" n="HIAT:u" s="T242">
                  <nts id="Seg_2458" n="HIAT:ip">(</nts>
                  <nts id="Seg_2459" n="HIAT:ip">(</nts>
                  <ats e="T243" id="Seg_2460" n="HIAT:non-pho" s="T242">…</ats>
                  <nts id="Seg_2461" n="HIAT:ip">)</nts>
                  <nts id="Seg_2462" n="HIAT:ip">)</nts>
                  <nts id="Seg_2463" n="HIAT:ip">.</nts>
                  <nts id="Seg_2464" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T258" id="Seg_2465" n="sc" s="T257">
               <ts e="T258" id="Seg_2467" n="HIAT:u" s="T257">
                  <nts id="Seg_2468" n="HIAT:ip">(</nts>
                  <nts id="Seg_2469" n="HIAT:ip">(</nts>
                  <ats e="T258" id="Seg_2470" n="HIAT:non-pho" s="T257">…</ats>
                  <nts id="Seg_2471" n="HIAT:ip">)</nts>
                  <nts id="Seg_2472" n="HIAT:ip">)</nts>
                  <nts id="Seg_2473" n="HIAT:ip">.</nts>
                  <nts id="Seg_2474" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T260" id="Seg_2475" n="sc" s="T259">
               <ts e="T260" id="Seg_2477" n="HIAT:u" s="T259">
                  <nts id="Seg_2478" n="HIAT:ip">(</nts>
                  <nts id="Seg_2479" n="HIAT:ip">(</nts>
                  <ats e="T260" id="Seg_2480" n="HIAT:non-pho" s="T259">…</ats>
                  <nts id="Seg_2481" n="HIAT:ip">)</nts>
                  <nts id="Seg_2482" n="HIAT:ip">)</nts>
                  <nts id="Seg_2483" n="HIAT:ip">.</nts>
                  <nts id="Seg_2484" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T266" id="Seg_2485" n="sc" s="T261">
               <ts e="T266" id="Seg_2487" n="HIAT:u" s="T261">
                  <ts e="T262" id="Seg_2489" n="HIAT:w" s="T261">Когда</ts>
                  <nts id="Seg_2490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_2492" n="HIAT:w" s="T262">стали</ts>
                  <nts id="Seg_2493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_2495" n="HIAT:w" s="T263">камасинцы</ts>
                  <nts id="Seg_2496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_2498" n="HIAT:w" s="T264">земледелием</ts>
                  <nts id="Seg_2499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_2501" n="HIAT:w" s="T265">заниматься</ts>
                  <nts id="Seg_2502" n="HIAT:ip">?</nts>
                  <nts id="Seg_2503" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T290" id="Seg_2504" n="sc" s="T289">
               <ts e="T290" id="Seg_2506" n="HIAT:u" s="T289">
                  <nts id="Seg_2507" n="HIAT:ip">(</nts>
                  <nts id="Seg_2508" n="HIAT:ip">(</nts>
                  <ats e="T290" id="Seg_2509" n="HIAT:non-pho" s="T289">…</ats>
                  <nts id="Seg_2510" n="HIAT:ip">)</nts>
                  <nts id="Seg_2511" n="HIAT:ip">)</nts>
                  <nts id="Seg_2512" n="HIAT:ip">.</nts>
                  <nts id="Seg_2513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T292" id="Seg_2514" n="sc" s="T291">
               <ts e="T292" id="Seg_2516" n="HIAT:u" s="T291">
                  <nts id="Seg_2517" n="HIAT:ip">(</nts>
                  <nts id="Seg_2518" n="HIAT:ip">(</nts>
                  <ats e="T292" id="Seg_2519" n="HIAT:non-pho" s="T291">…</ats>
                  <nts id="Seg_2520" n="HIAT:ip">)</nts>
                  <nts id="Seg_2521" n="HIAT:ip">)</nts>
                  <nts id="Seg_2522" n="HIAT:ip">.</nts>
                  <nts id="Seg_2523" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T301" id="Seg_2524" n="sc" s="T298">
               <ts e="T301" id="Seg_2526" n="HIAT:u" s="T298">
                  <ts e="T301" id="Seg_2528" n="HIAT:w" s="T298">Ja</ts>
                  <nts id="Seg_2529" n="HIAT:ip">.</nts>
                  <nts id="Seg_2530" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T306" id="Seg_2531" n="sc" s="T304">
               <ts e="T306" id="Seg_2533" n="HIAT:u" s="T304">
                  <nts id="Seg_2534" n="HIAT:ip">(</nts>
                  <nts id="Seg_2535" n="HIAT:ip">(</nts>
                  <ats e="T306" id="Seg_2536" n="HIAT:non-pho" s="T304">…</ats>
                  <nts id="Seg_2537" n="HIAT:ip">)</nts>
                  <nts id="Seg_2538" n="HIAT:ip">)</nts>
                  <nts id="Seg_2539" n="HIAT:ip">.</nts>
                  <nts id="Seg_2540" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T314" id="Seg_2541" n="sc" s="T307">
               <ts e="T308" id="Seg_2543" n="HIAT:u" s="T307">
                  <nts id="Seg_2544" n="HIAT:ip">(</nts>
                  <nts id="Seg_2545" n="HIAT:ip">(</nts>
                  <ats e="T308" id="Seg_2546" n="HIAT:non-pho" s="T307">…</ats>
                  <nts id="Seg_2547" n="HIAT:ip">)</nts>
                  <nts id="Seg_2548" n="HIAT:ip">)</nts>
                  <nts id="Seg_2549" n="HIAT:ip">.</nts>
                  <nts id="Seg_2550" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T314" id="Seg_2552" n="HIAT:u" s="T308">
                  <ts e="T309" id="Seg_2554" n="HIAT:w" s="T308">Где</ts>
                  <nts id="Seg_2555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_2557" n="HIAT:w" s="T309">мужики</ts>
                  <nts id="Seg_2558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_2560" n="HIAT:w" s="T310">водку</ts>
                  <nts id="Seg_2561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_2563" n="HIAT:w" s="T311">достали</ts>
                  <nts id="Seg_2564" n="HIAT:ip">,</nts>
                  <nts id="Seg_2565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_2567" n="HIAT:w" s="T312">которую</ts>
                  <nts id="Seg_2568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_2570" n="HIAT:w" s="T313">пили</ts>
                  <nts id="Seg_2571" n="HIAT:ip">?</nts>
                  <nts id="Seg_2572" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T327" id="Seg_2573" n="sc" s="T326">
               <ts e="T327" id="Seg_2575" n="HIAT:u" s="T326">
                  <nts id="Seg_2576" n="HIAT:ip">(</nts>
                  <nts id="Seg_2577" n="HIAT:ip">(</nts>
                  <ats e="T327" id="Seg_2578" n="HIAT:non-pho" s="T326">…</ats>
                  <nts id="Seg_2579" n="HIAT:ip">)</nts>
                  <nts id="Seg_2580" n="HIAT:ip">)</nts>
                  <nts id="Seg_2581" n="HIAT:ip">.</nts>
                  <nts id="Seg_2582" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KA">
            <ts e="T24" id="Seg_2583" n="sc" s="T8">
               <ts e="T9" id="Seg_2585" n="e" s="T8">Ты </ts>
               <ts e="T11" id="Seg_2587" n="e" s="T9">прости, </ts>
               <ts e="T13" id="Seg_2589" n="e" s="T11">сейчас </ts>
               <ts e="T14" id="Seg_2591" n="e" s="T13">меня </ts>
               <ts e="T15" id="Seg_2593" n="e" s="T14">хотят </ts>
               <ts e="T16" id="Seg_2595" n="e" s="T15">для </ts>
               <ts e="T17" id="Seg_2597" n="e" s="T16">радио </ts>
               <ts e="T18" id="Seg_2599" n="e" s="T17">спросить, </ts>
               <ts e="T19" id="Seg_2601" n="e" s="T18">я </ts>
               <ts e="T20" id="Seg_2603" n="e" s="T19">что-то </ts>
               <ts e="T21" id="Seg_2605" n="e" s="T20">им </ts>
               <ts e="T22" id="Seg_2607" n="e" s="T21">тут </ts>
               <ts e="T23" id="Seg_2609" n="e" s="T22">наболтаю </ts>
               <ts e="T24" id="Seg_2611" n="e" s="T23">сейчас. </ts>
            </ts>
            <ts e="T26" id="Seg_2612" n="sc" s="T25">
               <ts e="T26" id="Seg_2614" n="e" s="T25">((…)). </ts>
            </ts>
            <ts e="T33" id="Seg_2615" n="sc" s="T27">
               <ts e="T28" id="Seg_2617" n="e" s="T27">Ну, </ts>
               <ts e="T29" id="Seg_2619" n="e" s="T28">как </ts>
               <ts e="T30" id="Seg_2621" n="e" s="T29">у </ts>
               <ts e="T31" id="Seg_2623" n="e" s="T30">тебя </ts>
               <ts e="T32" id="Seg_2625" n="e" s="T31">насчет </ts>
               <ts e="T33" id="Seg_2627" n="e" s="T32">песенки? </ts>
            </ts>
            <ts e="T39" id="Seg_2628" n="sc" s="T34">
               <ts e="T35" id="Seg_2630" n="e" s="T34">(Как=) </ts>
               <ts e="T36" id="Seg_2632" n="e" s="T35">Что-нибудь </ts>
               <ts e="T37" id="Seg_2634" n="e" s="T36">камасинское </ts>
               <ts e="T38" id="Seg_2636" n="e" s="T37">вспомнится, </ts>
               <ts e="T39" id="Seg_2638" n="e" s="T38">нет? </ts>
            </ts>
            <ts e="T57" id="Seg_2639" n="sc" s="T51">
               <ts e="T52" id="Seg_2641" n="e" s="T51">Все </ts>
               <ts e="T53" id="Seg_2643" n="e" s="T52">же </ts>
               <ts e="T54" id="Seg_2645" n="e" s="T53">на </ts>
               <ts e="T55" id="Seg_2647" n="e" s="T54">камасинском </ts>
               <ts e="T57" id="Seg_2649" n="e" s="T55">((…)). </ts>
            </ts>
            <ts e="T93" id="Seg_2650" n="sc" s="T92">
               <ts e="T93" id="Seg_2652" n="e" s="T92">((…)). </ts>
            </ts>
            <ts e="T109" id="Seg_2653" n="sc" s="T94">
               <ts e="T95" id="Seg_2655" n="e" s="T94">И </ts>
               <ts e="T96" id="Seg_2657" n="e" s="T95">они </ts>
               <ts e="T97" id="Seg_2659" n="e" s="T96">желают </ts>
               <ts e="T98" id="Seg_2661" n="e" s="T97">тебе </ts>
               <ts e="T99" id="Seg_2663" n="e" s="T98">много </ts>
               <ts e="T100" id="Seg_2665" n="e" s="T99">лет </ts>
               <ts e="T101" id="Seg_2667" n="e" s="T100">жизни </ts>
               <ts e="T102" id="Seg_2669" n="e" s="T101">и </ts>
               <ts e="T103" id="Seg_2671" n="e" s="T102">всего </ts>
               <ts e="T104" id="Seg_2673" n="e" s="T103">хорошего </ts>
               <ts e="T105" id="Seg_2675" n="e" s="T104">и, </ts>
               <ts e="T106" id="Seg_2677" n="e" s="T105">значит, </ts>
               <ts e="T107" id="Seg_2679" n="e" s="T106">радостного </ts>
               <ts e="T109" id="Seg_2681" n="e" s="T107">настроения. </ts>
            </ts>
            <ts e="T112" id="Seg_2682" n="sc" s="T111">
               <ts e="T112" id="Seg_2684" n="e" s="T111">Kiitos. </ts>
            </ts>
            <ts e="T114" id="Seg_2685" n="sc" s="T113">
               <ts e="T114" id="Seg_2687" n="e" s="T113">((…)). </ts>
            </ts>
            <ts e="T124" id="Seg_2688" n="sc" s="T116">
               <ts e="T117" id="Seg_2690" n="e" s="T116">магазинов </ts>
               <ts e="T118" id="Seg_2692" n="e" s="T117">было </ts>
               <ts e="T119" id="Seg_2694" n="e" s="T118">тогда, </ts>
               <ts e="T120" id="Seg_2696" n="e" s="T119">когда </ts>
               <ts e="T121" id="Seg_2698" n="e" s="T120">ты </ts>
               <ts e="T122" id="Seg_2700" n="e" s="T121">еще </ts>
               <ts e="T123" id="Seg_2702" n="e" s="T122">была </ts>
               <ts e="T124" id="Seg_2704" n="e" s="T123">мала. </ts>
            </ts>
            <ts e="T137" id="Seg_2705" n="sc" s="T134">
               <ts e="T137" id="Seg_2707" n="e" s="T134">((…)) ((NOISE)) </ts>
            </ts>
            <ts e="T145" id="Seg_2708" n="sc" s="T144">
               <ts e="T145" id="Seg_2710" n="e" s="T144">Ja. </ts>
            </ts>
            <ts e="T147" id="Seg_2711" n="sc" s="T146">
               <ts e="T147" id="Seg_2713" n="e" s="T146">((…)). </ts>
            </ts>
            <ts e="T153" id="Seg_2714" n="sc" s="T148">
               <ts e="T149" id="Seg_2716" n="e" s="T148">((…)). </ts>
               <ts e="T150" id="Seg_2718" n="e" s="T149">Наверное, </ts>
               <ts e="T151" id="Seg_2720" n="e" s="T150">соль </ts>
               <ts e="T152" id="Seg_2722" n="e" s="T151">и </ts>
               <ts e="T153" id="Seg_2724" n="e" s="T152">сахар. </ts>
            </ts>
            <ts e="T161" id="Seg_2725" n="sc" s="T160">
               <ts e="T161" id="Seg_2727" n="e" s="T160">(ši-)… </ts>
            </ts>
            <ts e="T181" id="Seg_2728" n="sc" s="T180">
               <ts e="T181" id="Seg_2730" n="e" s="T180">((…)). </ts>
            </ts>
            <ts e="T183" id="Seg_2731" n="sc" s="T182">
               <ts e="T183" id="Seg_2733" n="e" s="T182">((…)). </ts>
            </ts>
            <ts e="T186" id="Seg_2734" n="sc" s="T184">
               <ts e="T185" id="Seg_2736" n="e" s="T184">((…)). </ts>
               <ts e="T186" id="Seg_2738" n="e" s="T185">((…)). </ts>
            </ts>
            <ts e="T188" id="Seg_2739" n="sc" s="T187">
               <ts e="T188" id="Seg_2741" n="e" s="T187">((…)). </ts>
            </ts>
            <ts e="T201" id="Seg_2742" n="sc" s="T189">
               <ts e="T190" id="Seg_2744" n="e" s="T189">Был </ts>
               <ts e="T191" id="Seg_2746" n="e" s="T190">ли </ts>
               <ts e="T192" id="Seg_2748" n="e" s="T191">часто </ts>
               <ts e="T193" id="Seg_2750" n="e" s="T192">голод </ts>
               <ts e="T194" id="Seg_2752" n="e" s="T193">в </ts>
               <ts e="T195" id="Seg_2754" n="e" s="T194">те </ts>
               <ts e="T196" id="Seg_2756" n="e" s="T195">времена, </ts>
               <ts e="T197" id="Seg_2758" n="e" s="T196">когда </ts>
               <ts e="T198" id="Seg_2760" n="e" s="T197">ты </ts>
               <ts e="T199" id="Seg_2762" n="e" s="T198">была </ts>
               <ts e="T200" id="Seg_2764" n="e" s="T199">еще </ts>
               <ts e="T201" id="Seg_2766" n="e" s="T200">мала? </ts>
            </ts>
            <ts e="T228" id="Seg_2767" n="sc" s="T227">
               <ts e="T228" id="Seg_2769" n="e" s="T227">((…)). </ts>
            </ts>
            <ts e="T230" id="Seg_2770" n="sc" s="T229">
               <ts e="T230" id="Seg_2772" n="e" s="T229">((…)). </ts>
            </ts>
            <ts e="T232" id="Seg_2773" n="sc" s="T231">
               <ts e="T232" id="Seg_2775" n="e" s="T231">((…)). </ts>
            </ts>
            <ts e="T241" id="Seg_2776" n="sc" s="T233">
               <ts e="T234" id="Seg_2778" n="e" s="T233">Делали </ts>
               <ts e="T235" id="Seg_2780" n="e" s="T234">ли </ts>
               <ts e="T236" id="Seg_2782" n="e" s="T235">колодец </ts>
               <ts e="T237" id="Seg_2784" n="e" s="T236">для </ts>
               <ts e="T238" id="Seg_2786" n="e" s="T237">того, </ts>
               <ts e="T239" id="Seg_2788" n="e" s="T238">чтобы </ts>
               <ts e="T240" id="Seg_2790" n="e" s="T239">достать </ts>
               <ts e="T241" id="Seg_2792" n="e" s="T240">воду? </ts>
            </ts>
            <ts e="T243" id="Seg_2793" n="sc" s="T242">
               <ts e="T243" id="Seg_2795" n="e" s="T242">((…)). </ts>
            </ts>
            <ts e="T258" id="Seg_2796" n="sc" s="T257">
               <ts e="T258" id="Seg_2798" n="e" s="T257">((…)). </ts>
            </ts>
            <ts e="T260" id="Seg_2799" n="sc" s="T259">
               <ts e="T260" id="Seg_2801" n="e" s="T259">((…)). </ts>
            </ts>
            <ts e="T266" id="Seg_2802" n="sc" s="T261">
               <ts e="T262" id="Seg_2804" n="e" s="T261">Когда </ts>
               <ts e="T263" id="Seg_2806" n="e" s="T262">стали </ts>
               <ts e="T264" id="Seg_2808" n="e" s="T263">камасинцы </ts>
               <ts e="T265" id="Seg_2810" n="e" s="T264">земледелием </ts>
               <ts e="T266" id="Seg_2812" n="e" s="T265">заниматься? </ts>
            </ts>
            <ts e="T290" id="Seg_2813" n="sc" s="T289">
               <ts e="T290" id="Seg_2815" n="e" s="T289">((…)). </ts>
            </ts>
            <ts e="T292" id="Seg_2816" n="sc" s="T291">
               <ts e="T292" id="Seg_2818" n="e" s="T291">((…)). </ts>
            </ts>
            <ts e="T301" id="Seg_2819" n="sc" s="T298">
               <ts e="T301" id="Seg_2821" n="e" s="T298">Ja. </ts>
            </ts>
            <ts e="T306" id="Seg_2822" n="sc" s="T304">
               <ts e="T306" id="Seg_2824" n="e" s="T304">((…)). </ts>
            </ts>
            <ts e="T314" id="Seg_2825" n="sc" s="T307">
               <ts e="T308" id="Seg_2827" n="e" s="T307">((…)). </ts>
               <ts e="T309" id="Seg_2829" n="e" s="T308">Где </ts>
               <ts e="T310" id="Seg_2831" n="e" s="T309">мужики </ts>
               <ts e="T311" id="Seg_2833" n="e" s="T310">водку </ts>
               <ts e="T312" id="Seg_2835" n="e" s="T311">достали, </ts>
               <ts e="T313" id="Seg_2837" n="e" s="T312">которую </ts>
               <ts e="T314" id="Seg_2839" n="e" s="T313">пили? </ts>
            </ts>
            <ts e="T327" id="Seg_2840" n="sc" s="T326">
               <ts e="T327" id="Seg_2842" n="e" s="T326">((…)). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KA">
            <ta e="T24" id="Seg_2843" s="T8">PKZ_197008_09341-2a.KA.001 (002)</ta>
            <ta e="T26" id="Seg_2844" s="T25">PKZ_197008_09341-2a.KA.002 (003)</ta>
            <ta e="T33" id="Seg_2845" s="T27">PKZ_197008_09341-2a.KA.003 (004)</ta>
            <ta e="T39" id="Seg_2846" s="T34">PKZ_197008_09341-2a.KA.004 (005)</ta>
            <ta e="T57" id="Seg_2847" s="T51">PKZ_197008_09341-2a.KA.005 (008)</ta>
            <ta e="T93" id="Seg_2848" s="T92">PKZ_197008_09341-2a.KA.006 (016)</ta>
            <ta e="T109" id="Seg_2849" s="T94">PKZ_197008_09341-2a.KA.007 (017)</ta>
            <ta e="T112" id="Seg_2850" s="T111">PKZ_197008_09341-2a.KA.008 (019)</ta>
            <ta e="T114" id="Seg_2851" s="T113">PKZ_197008_09341-2a.KA.009 (020)</ta>
            <ta e="T124" id="Seg_2852" s="T116">PKZ_197008_09341-2a.KA.010 (022)</ta>
            <ta e="T137" id="Seg_2853" s="T134">PKZ_197008_09341-2a.KA.011 (026)</ta>
            <ta e="T145" id="Seg_2854" s="T144">PKZ_197008_09341-2a.KA.012 (029)</ta>
            <ta e="T147" id="Seg_2855" s="T146">PKZ_197008_09341-2a.KA.013 (030)</ta>
            <ta e="T149" id="Seg_2856" s="T148">PKZ_197008_09341-2a.KA.014 (031)</ta>
            <ta e="T153" id="Seg_2857" s="T149">PKZ_197008_09341-2a.KA.015 (032)</ta>
            <ta e="T161" id="Seg_2858" s="T160">PKZ_197008_09341-2a.KA.016 (034)</ta>
            <ta e="T181" id="Seg_2859" s="T180">PKZ_197008_09341-2a.KA.017 (038)</ta>
            <ta e="T183" id="Seg_2860" s="T182">PKZ_197008_09341-2a.KA.018 (039)</ta>
            <ta e="T185" id="Seg_2861" s="T184">PKZ_197008_09341-2a.KA.019 (040)</ta>
            <ta e="T186" id="Seg_2862" s="T185">PKZ_197008_09341-2a.KA.020 (041)</ta>
            <ta e="T188" id="Seg_2863" s="T187">PKZ_197008_09341-2a.KA.021 (042)</ta>
            <ta e="T201" id="Seg_2864" s="T189">PKZ_197008_09341-2a.KA.022 (043)</ta>
            <ta e="T228" id="Seg_2865" s="T227">PKZ_197008_09341-2a.KA.023 (050)</ta>
            <ta e="T230" id="Seg_2866" s="T229">PKZ_197008_09341-2a.KA.024 (051)</ta>
            <ta e="T232" id="Seg_2867" s="T231">PKZ_197008_09341-2a.KA.025 (052)</ta>
            <ta e="T241" id="Seg_2868" s="T233">PKZ_197008_09341-2a.KA.026 (053)</ta>
            <ta e="T243" id="Seg_2869" s="T242">PKZ_197008_09341-2a.KA.027 (054)</ta>
            <ta e="T258" id="Seg_2870" s="T257">PKZ_197008_09341-2a.KA.028 (058)</ta>
            <ta e="T260" id="Seg_2871" s="T259">PKZ_197008_09341-2a.KA.029 (059)</ta>
            <ta e="T266" id="Seg_2872" s="T261">PKZ_197008_09341-2a.KA.030 (060)</ta>
            <ta e="T290" id="Seg_2873" s="T289">PKZ_197008_09341-2a.KA.031 (065)</ta>
            <ta e="T292" id="Seg_2874" s="T291">PKZ_197008_09341-2a.KA.032 (066)</ta>
            <ta e="T301" id="Seg_2875" s="T298">PKZ_197008_09341-2a.KA.033 (069)</ta>
            <ta e="T306" id="Seg_2876" s="T304">PKZ_197008_09341-2a.KA.034 (070)</ta>
            <ta e="T308" id="Seg_2877" s="T307">PKZ_197008_09341-2a.KA.035 (071)</ta>
            <ta e="T314" id="Seg_2878" s="T308">PKZ_197008_09341-2a.KA.036 (072)</ta>
            <ta e="T327" id="Seg_2879" s="T326">PKZ_197008_09341-2a.KA.037 (075)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KA">
            <ta e="T24" id="Seg_2880" s="T8">Ты прости, сейчас меня хотят для радио спросить, я что-то им тут наболтаю сейчас. </ta>
            <ta e="T26" id="Seg_2881" s="T25">((…)). </ta>
            <ta e="T33" id="Seg_2882" s="T27">Ну, как у тебя насчет песенки? </ta>
            <ta e="T39" id="Seg_2883" s="T34">(Как=) Что-нибудь камасинское вспомнится, нет? </ta>
            <ta e="T57" id="Seg_2884" s="T51">Все же на камасинском ((…)). </ta>
            <ta e="T93" id="Seg_2885" s="T92">((…)). </ta>
            <ta e="T109" id="Seg_2886" s="T94">И они желают тебе много лет жизни и всего хорошего и, значит, радостного настроения. </ta>
            <ta e="T112" id="Seg_2887" s="T111">Kiitos. </ta>
            <ta e="T114" id="Seg_2888" s="T113">((…)). </ta>
            <ta e="T124" id="Seg_2889" s="T116">((DMG)) магазинов было тогда, когда ты еще была мала. </ta>
            <ta e="T137" id="Seg_2890" s="T134">((…)) ((NOISE)) </ta>
            <ta e="T145" id="Seg_2891" s="T144">Ja. </ta>
            <ta e="T147" id="Seg_2892" s="T146">((…)). </ta>
            <ta e="T149" id="Seg_2893" s="T148">((…)). </ta>
            <ta e="T153" id="Seg_2894" s="T149">Наверное, соль и сахар. </ta>
            <ta e="T161" id="Seg_2895" s="T160">(ši-)…</ta>
            <ta e="T181" id="Seg_2896" s="T180">((…)). </ta>
            <ta e="T183" id="Seg_2897" s="T182">((…)). </ta>
            <ta e="T185" id="Seg_2898" s="T184">((…)). </ta>
            <ta e="T186" id="Seg_2899" s="T185">((…)). </ta>
            <ta e="T188" id="Seg_2900" s="T187">((…)). </ta>
            <ta e="T201" id="Seg_2901" s="T189">Был ли часто голод в те времена, когда ты была еще мала? </ta>
            <ta e="T228" id="Seg_2902" s="T227">((…)). </ta>
            <ta e="T230" id="Seg_2903" s="T229">((…)). </ta>
            <ta e="T232" id="Seg_2904" s="T231">((…)). </ta>
            <ta e="T241" id="Seg_2905" s="T233">Делали ли колодец для того, чтобы достать воду? </ta>
            <ta e="T243" id="Seg_2906" s="T242">((…)). </ta>
            <ta e="T258" id="Seg_2907" s="T257">((…)). </ta>
            <ta e="T260" id="Seg_2908" s="T259">((…)). </ta>
            <ta e="T266" id="Seg_2909" s="T261">Когда стали камасинцы земледелием заниматься? </ta>
            <ta e="T290" id="Seg_2910" s="T289">((…)). </ta>
            <ta e="T292" id="Seg_2911" s="T291">((…)). </ta>
            <ta e="T301" id="Seg_2912" s="T298">Ja. </ta>
            <ta e="T306" id="Seg_2913" s="T304">((…)). </ta>
            <ta e="T308" id="Seg_2914" s="T307">((…)). </ta>
            <ta e="T314" id="Seg_2915" s="T308">Где мужики водку достали, которую пили? </ta>
            <ta e="T327" id="Seg_2916" s="T326">((…)). </ta>
         </annotation>
         <annotation name="CS" tierref="CS-KA">
            <ta e="T24" id="Seg_2917" s="T8">RUS:ext</ta>
            <ta e="T26" id="Seg_2918" s="T25">FIN:ext</ta>
            <ta e="T33" id="Seg_2919" s="T27">RUS:ext</ta>
            <ta e="T39" id="Seg_2920" s="T34">RUS:ext</ta>
            <ta e="T57" id="Seg_2921" s="T51">RUS:ext</ta>
            <ta e="T93" id="Seg_2922" s="T92">FIN:ext</ta>
            <ta e="T109" id="Seg_2923" s="T94">RUS:ext</ta>
            <ta e="T114" id="Seg_2924" s="T113">FIN:ext</ta>
            <ta e="T124" id="Seg_2925" s="T116">RUS:ext</ta>
            <ta e="T137" id="Seg_2926" s="T134">FIN:ext</ta>
            <ta e="T147" id="Seg_2927" s="T146">FIN:ext</ta>
            <ta e="T149" id="Seg_2928" s="T148">FIN:ext</ta>
            <ta e="T153" id="Seg_2929" s="T149">RUS:ext</ta>
            <ta e="T181" id="Seg_2930" s="T180">FIN:ext</ta>
            <ta e="T183" id="Seg_2931" s="T182">FIN:ext</ta>
            <ta e="T185" id="Seg_2932" s="T184">FIN:ext</ta>
            <ta e="T186" id="Seg_2933" s="T185">FIN:ext</ta>
            <ta e="T188" id="Seg_2934" s="T187">FIN:ext</ta>
            <ta e="T201" id="Seg_2935" s="T189">RUS:ext</ta>
            <ta e="T228" id="Seg_2936" s="T227">FIN:ext</ta>
            <ta e="T230" id="Seg_2937" s="T229">FIN:ext</ta>
            <ta e="T232" id="Seg_2938" s="T231">FIN:ext</ta>
            <ta e="T241" id="Seg_2939" s="T233">RUS:ext</ta>
            <ta e="T243" id="Seg_2940" s="T242">FIN:ext</ta>
            <ta e="T258" id="Seg_2941" s="T257">FIN:ext</ta>
            <ta e="T260" id="Seg_2942" s="T259">FIN:ext</ta>
            <ta e="T266" id="Seg_2943" s="T261">RUS:ext</ta>
            <ta e="T290" id="Seg_2944" s="T289">FIN:ext</ta>
            <ta e="T292" id="Seg_2945" s="T291">FIN:ext</ta>
            <ta e="T306" id="Seg_2946" s="T304">FIN:ext</ta>
            <ta e="T308" id="Seg_2947" s="T307">FIN:ext</ta>
            <ta e="T314" id="Seg_2948" s="T308">RUS:ext</ta>
            <ta e="T327" id="Seg_2949" s="T326">FIN:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KA">
            <ta e="T112" id="Seg_2950" s="T111">Спасибо!</ta>
            <ta e="T137" id="Seg_2951" s="T134">(…)</ta>
         </annotation>
         <annotation name="fe" tierref="fe-KA">
            <ta e="T24" id="Seg_2952" s="T8">Forgive me, they want to ask me for the radio now, I’ll chat with them here now.</ta>
            <ta e="T33" id="Seg_2953" s="T27">Well, what about your song?</ta>
            <ta e="T39" id="Seg_2954" s="T34">Do you remember anything kamassian?</ta>
            <ta e="T57" id="Seg_2955" s="T51">Now in Kamass (…)</ta>
            <ta e="T109" id="Seg_2956" s="T94">And they wish you many years of life and all the best and a joyful mood.</ta>
            <ta e="T112" id="Seg_2957" s="T111">Thank you!</ta>
            <ta e="T124" id="Seg_2958" s="T116">There were shops when you were still small.</ta>
            <ta e="T137" id="Seg_2959" s="T134">(…)</ta>
            <ta e="T153" id="Seg_2960" s="T149">Probably salt and sugar.</ta>
            <ta e="T201" id="Seg_2961" s="T189">Was there often hunger in the days when you were still young?</ta>
            <ta e="T241" id="Seg_2962" s="T233">Did they make a well to get water?</ta>
            <ta e="T266" id="Seg_2963" s="T261">When did the Kamssians start farming?</ta>
            <ta e="T314" id="Seg_2964" s="T308">Where did the men get the vodka they drank?</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KA">
            <ta e="T24" id="Seg_2965" s="T8">Verzeih mir, sie möchten mich jetzt nach dem Radio fragen. Ich werde jetzt hier mit ihnen reden.</ta>
            <ta e="T33" id="Seg_2966" s="T27">Nun, was ist mit deinem Lied?</ta>
            <ta e="T39" id="Seg_2967" s="T34">Erinnerst du dich an etwas Kamassisches?</ta>
            <ta e="T57" id="Seg_2968" s="T51">Jetzt alles auf Kamassisch (…)</ta>
            <ta e="T109" id="Seg_2969" s="T94">Und sie wünschen Ihnen viele Lebensjahre und alles Gute und eine gute Verfassung.</ta>
            <ta e="T112" id="Seg_2970" s="T111">Danke.</ta>
            <ta e="T124" id="Seg_2971" s="T116">Es gab Läden als du noch klein warst.</ta>
            <ta e="T137" id="Seg_2972" s="T134">(…)</ta>
            <ta e="T153" id="Seg_2973" s="T149">Wahrscheinlich Salz und Zucker.</ta>
            <ta e="T201" id="Seg_2974" s="T189">Hattest du oft Hunger als du noch klein warst?</ta>
            <ta e="T241" id="Seg_2975" s="T233">Haben sie einen Brunnen gemacht, um Wasser zu bekommen?</ta>
            <ta e="T266" id="Seg_2976" s="T261">Wann haben die Kamassen mit der Landwirtschaft begonnen?</ta>
            <ta e="T314" id="Seg_2977" s="T308">Woher hatten die Männer den Wodka, den sie getrunken haben?</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KA">
            <ta e="T26" id="Seg_2978" s="T25">[GVY:] A conversation in FINNISH until 04:52</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T373" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T185" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T374" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-PKZ"
                          name="ref"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PKZ"
                          name="ts"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PKZ"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PKZ"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PKZ"
                          name="mb"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PKZ"
                          name="mp"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PKZ"
                          name="ge"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PKZ"
                          name="gr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PKZ"
                          name="mc"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PKZ"
                          name="ps"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PKZ"
                          name="SeR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PKZ"
                          name="SyF"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PKZ"
                          name="IST"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PKZ"
                          name="BOR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PKZ"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PKZ"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PKZ"
                          name="CS"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PKZ"
                          name="fr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PKZ"
                          name="fe"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PKZ"
                          name="fg"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PKZ"
                          name="nt"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-KA"
                          name="ref"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KA"
                          name="ts"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KA"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KA"
                          type="t" />
         <conversion-tier category="CS"
                          display-name="CS-KA"
                          name="CS"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KA"
                          name="fr"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KA"
                          name="fe"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KA"
                          name="fg"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KA"
                          name="nt"
                          segmented-tier-id="tx-KA"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
