<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID2D0A30B7-339A-575E-6268-4AA77226F594">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_SU0225.wav" />
         <referenced-file url="PKZ_196X_SU0225.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_196X_SU0225\PKZ_196X_SU0225.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1652</ud-information>
            <ud-information attribute-name="# HIAT:w">1041</ud-information>
            <ud-information attribute-name="# e">1058</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">25</ud-information>
            <ud-information attribute-name="# HIAT:u">243</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.47" type="appl" />
         <tli id="T1" time="1.55" type="appl" />
         <tli id="T2" time="2.63" type="appl" />
         <tli id="T3" time="3.71" type="appl" />
         <tli id="T4" time="6.359970846522041" />
         <tli id="T5" time="7.725" type="appl" />
         <tli id="T6" time="8.93" type="appl" />
         <tli id="T7" time="10.135" type="appl" />
         <tli id="T8" time="11.63994664363468" />
         <tli id="T9" time="12.945" type="appl" />
         <tli id="T10" time="14.43" type="appl" />
         <tli id="T11" time="15.523" type="appl" />
         <tli id="T12" time="16.347" type="appl" />
         <tli id="T13" time="17.17" type="appl" />
         <tli id="T14" time="18.306" type="appl" />
         <tli id="T15" time="19.382" type="appl" />
         <tli id="T16" time="20.458" type="appl" />
         <tli id="T17" time="21.534" type="appl" />
         <tli id="T18" time="22.779895579209448" />
         <tli id="T19" time="23.695" type="appl" />
         <tli id="T20" time="24.54" type="appl" />
         <tli id="T21" time="25.385" type="appl" />
         <tli id="T22" time="26.23" type="appl" />
         <tli id="T23" time="27.075" type="appl" />
         <tli id="T24" time="27.92" type="appl" />
         <tli id="T25" time="28.71" type="appl" />
         <tli id="T26" time="29.41" type="appl" />
         <tli id="T27" time="30.267" type="appl" />
         <tli id="T28" time="31.123" type="appl" />
         <tli id="T29" time="33.02651527638385" />
         <tli id="T30" time="33.744" type="appl" />
         <tli id="T31" time="34.899" type="appl" />
         <tli id="T32" time="36.053" type="appl" />
         <tli id="T33" time="37.208" type="appl" />
         <tli id="T34" time="38.362" type="appl" />
         <tli id="T35" time="38.613" type="appl" />
         <tli id="T36" time="40.35314835848838" />
         <tli id="T37" time="41.487" type="appl" />
         <tli id="T38" time="42.513" type="appl" />
         <tli id="T39" time="43.54" type="appl" />
         <tli id="T40" time="44.567" type="appl" />
         <tli id="T41" time="45.593" type="appl" />
         <tli id="T42" time="46.62" type="appl" />
         <tli id="T43" time="47.332" type="appl" />
         <tli id="T44" time="48.043" type="appl" />
         <tli id="T45" time="48.754" type="appl" />
         <tli id="T46" time="49.466" type="appl" />
         <tli id="T47" time="50.178" type="appl" />
         <tli id="T48" time="51.9264286410484" />
         <tli id="T49" time="52.478" type="appl" />
         <tli id="T50" time="52.823" type="appl" />
         <tli id="T51" time="53.167" type="appl" />
         <tli id="T52" time="53.511" type="appl" />
         <tli id="T53" time="53.855" type="appl" />
         <tli id="T54" time="54.2" type="appl" />
         <tli id="T55" time="55.033081067127306" />
         <tli id="T56" time="56.14" type="appl" />
         <tli id="T57" time="57.82640159615533" />
         <tli id="T58" time="58.637" type="appl" />
         <tli id="T59" time="59.109" type="appl" />
         <tli id="T60" time="59.581" type="appl" />
         <tli id="T61" time="60.052" type="appl" />
         <tli id="T62" time="60.524" type="appl" />
         <tli id="T63" time="60.996" type="appl" />
         <tli id="T64" time="61.468" type="appl" />
         <tli id="T65" time="62.131" type="appl" />
         <tli id="T66" time="62.793" type="appl" />
         <tli id="T67" time="63.456" type="appl" />
         <tli id="T68" time="64.606" type="appl" />
         <tli id="T69" time="65.755" type="appl" />
         <tli id="T70" time="66.904" type="appl" />
         <tli id="T71" time="68.054" type="appl" />
         <tli id="T72" time="69.105" type="appl" />
         <tli id="T73" time="69.65" type="appl" />
         <tli id="T74" time="70.195" type="appl" />
         <tli id="T75" time="70.74" type="appl" />
         <tli id="T76" time="71.285" type="appl" />
         <tli id="T77" time="72.1330026827762" />
         <tli id="T78" time="73.253" type="appl" />
         <tli id="T79" time="74.273" type="appl" />
         <tli id="T80" time="75.293" type="appl" />
         <tli id="T81" time="76.93964731625879" />
         <tli id="T82" time="78.046" type="appl" />
         <tli id="T83" time="79.022" type="appl" />
         <tli id="T84" time="79.998" type="appl" />
         <tli id="T85" time="80.974" type="appl" />
         <tli id="T86" time="81.949" type="appl" />
         <tli id="T87" time="82.925" type="appl" />
         <tli id="T88" time="83.901" type="appl" />
         <tli id="T89" time="84.877" type="appl" />
         <tli id="T90" time="86.46627031382691" />
         <tli id="T91" time="87.832" type="appl" />
         <tli id="T92" time="88.764" type="appl" />
         <tli id="T93" time="89.91958781749402" />
         <tli id="T94" time="90.813" type="appl" />
         <tli id="T95" time="91.577" type="appl" />
         <tli id="T96" time="92.41290972168609" />
         <tli id="T97" time="93.188" type="appl" />
         <tli id="T98" time="93.916" type="appl" />
         <tli id="T99" time="94.644" type="appl" />
         <tli id="T100" time="95.372" type="appl" />
         <tli id="T101" time="100.98620375588668" />
         <tli id="T102" time="102.072" type="appl" />
         <tli id="T103" time="102.83" type="appl" />
         <tli id="T104" time="103.588" type="appl" />
         <tli id="T105" time="104.345" type="appl" />
         <tli id="T106" time="105.347" type="appl" />
         <tli id="T107" time="106.107" type="appl" />
         <tli id="T108" time="107.184" type="appl" />
         <tli id="T109" time="108.109" type="appl" />
         <tli id="T110" time="109.034" type="appl" />
         <tli id="T111" time="109.959" type="appl" />
         <tli id="T112" time="110.907" type="appl" />
         <tli id="T113" time="111.687" type="appl" />
         <tli id="T114" time="112.467" type="appl" />
         <tli id="T115" time="113.247" type="appl" />
         <tli id="T116" time="114.6394745039759" />
         <tli id="T117" time="115.438" type="appl" />
         <tli id="T118" time="116.22" type="appl" />
         <tli id="T119" time="117.002" type="appl" />
         <tli id="T120" time="117.785" type="appl" />
         <tli id="T121" time="118.731" type="appl" />
         <tli id="T122" time="119.65" type="appl" />
         <tli id="T123" time="120.568" type="appl" />
         <tli id="T124" time="125.62609080907897" />
         <tli id="T125" time="126.303" type="appl" />
         <tli id="T126" time="127.035" type="appl" />
         <tli id="T127" time="127.767" type="appl" />
         <tli id="T128" time="128.499" type="appl" />
         <tli id="T129" time="129.51273965973132" />
         <tli id="T130" time="130.287" type="appl" />
         <tli id="T131" time="130.882" type="appl" />
         <tli id="T132" time="131.477" type="appl" />
         <tli id="T133" time="132.22606055546976" />
         <tli id="T134" time="132.966" type="appl" />
         <tli id="T135" time="133.56" type="appl" />
         <tli id="T136" time="134.153" type="appl" />
         <tli id="T137" time="134.664" type="appl" />
         <tli id="T138" time="135.175" type="appl" />
         <tli id="T139" time="135.686" type="appl" />
         <tli id="T140" time="136.197" type="appl" />
         <tli id="T141" time="136.709" type="appl" />
         <tli id="T142" time="137.22" type="appl" />
         <tli id="T143" time="137.731" type="appl" />
         <tli id="T144" time="138.242" type="appl" />
         <tli id="T145" time="138.9393631156875" />
         <tli id="T146" time="140.327" type="appl" />
         <tli id="T147" time="142.88601169130703" />
         <tli id="T148" time="143.598" type="appl" />
         <tli id="T149" time="144.246" type="appl" />
         <tli id="T150" time="144.893" type="appl" />
         <tli id="T151" time="145.541" type="appl" />
         <tli id="T152" time="146.189" type="appl" />
         <tli id="T153" time="146.837" type="appl" />
         <tli id="T154" time="147.484" type="appl" />
         <tli id="T155" time="148.132" type="appl" />
         <tli id="T156" time="148.90598409634833" />
         <tli id="T157" time="151.09930737570446" />
         <tli id="T1074" time="151.52193474076392" type="intp" />
         <tli id="T158" time="152.015" type="appl" />
         <tli id="T159" time="152.82" type="appl" />
         <tli id="T160" time="153.625" type="appl" />
         <tli id="T161" time="154.43" type="appl" />
         <tli id="T162" time="155.235" type="appl" />
         <tli id="T163" time="156.04" type="appl" />
         <tli id="T164" time="156.845" type="appl" />
         <tli id="T165" time="157.65" type="appl" />
         <tli id="T166" time="158.455" type="appl" />
         <tli id="T167" time="159.26" type="appl" />
         <tli id="T168" time="160.065" type="appl" />
         <tli id="T169" time="160.857" type="appl" />
         <tli id="T170" time="161.414" type="appl" />
         <tli id="T171" time="161.971" type="appl" />
         <tli id="T172" time="162.529" type="appl" />
         <tli id="T173" time="163.086" type="appl" />
         <tli id="T174" time="163.643" type="appl" />
         <tli id="T175" time="164.2" type="appl" />
         <tli id="T176" time="164.865" type="appl" />
         <tli id="T177" time="165.337" type="appl" />
         <tli id="T178" time="165.809" type="appl" />
         <tli id="T179" time="166.281" type="appl" />
         <tli id="T180" time="166.97923458368717" />
         <tli id="T181" time="168.038" type="appl" />
         <tli id="T182" time="170.67" type="appl" />
         <tli id="T183" time="171.345" type="appl" />
         <tli id="T184" time="171.91" type="appl" />
         <tli id="T185" time="172.475" type="appl" />
         <tli id="T186" time="173.04" type="appl" />
         <tli id="T187" time="174.054" type="appl" />
         <tli id="T188" time="175.069" type="appl" />
         <tli id="T189" time="175.88" type="appl" />
         <tli id="T190" time="176.552" type="appl" />
         <tli id="T191" time="177.224" type="appl" />
         <tli id="T192" time="177.897" type="appl" />
         <tli id="T193" time="178.57" type="appl" />
         <tli id="T194" time="179.242" type="appl" />
         <tli id="T195" time="179.914" type="appl" />
         <tli id="T196" time="180.78583796224814" />
         <tli id="T197" time="182.11" type="appl" />
         <tli id="T198" time="183.133" type="appl" />
         <tli id="T199" time="184.157" type="appl" />
         <tli id="T200" time="185.18" type="appl" />
         <tli id="T201" time="186.057" type="appl" />
         <tli id="T202" time="186.773" type="appl" />
         <tli id="T203" time="187.49" type="appl" />
         <tli id="T204" time="188.207" type="appl" />
         <tli id="T205" time="188.923" type="appl" />
         <tli id="T206" time="189.95246261001313" />
         <tli id="T207" time="191.037" type="appl" />
         <tli id="T208" time="192.83" type="appl" />
         <tli id="T209" time="193.75" type="appl" />
         <tli id="T210" time="194.67" type="appl" />
         <tli id="T211" time="195.59" type="appl" />
         <tli id="T212" time="197.08576324500117" />
         <tli id="T213" time="198.217" type="appl" />
         <tli id="T214" time="199.042" type="appl" />
         <tli id="T215" time="199.867" type="appl" />
         <tli id="T216" time="200.75907974021462" />
         <tli id="T217" time="201.574" type="appl" />
         <tli id="T218" time="202.186" type="appl" />
         <tli id="T219" time="202.798" type="appl" />
         <tli id="T220" time="203.41" type="appl" />
         <tli id="T221" time="204.022" type="appl" />
         <tli id="T222" time="204.79" type="appl" />
         <tli id="T223" time="205.452" type="appl" />
         <tli id="T224" time="206.114" type="appl" />
         <tli id="T225" time="207.25238330888598" />
         <tli id="T226" time="209.08570823843897" />
         <tli id="T227" time="209.763" type="appl" />
         <tli id="T228" time="210.358" type="appl" />
         <tli id="T229" time="210.953" type="appl" />
         <tli id="T230" time="211.71902950088784" />
         <tli id="T231" time="212.6" type="appl" />
         <tli id="T232" time="213.52" type="appl" />
         <tli id="T233" time="214.44" type="appl" />
         <tli id="T234" time="215.36" type="appl" />
         <tli id="T235" time="216.28" type="appl" />
         <tli id="T236" time="217.87900126418592" />
         <tli id="T237" time="219.21" type="appl" />
         <tli id="T238" time="220.87898751254536" />
         <tli id="T239" time="221.451" type="appl" />
         <tli id="T240" time="221.961" type="appl" />
         <tli id="T241" time="222.471" type="appl" />
         <tli id="T242" time="222.981" type="appl" />
         <tli id="T243" time="223.89897366922722" />
         <tli id="T244" time="226.13896340133562" />
         <tli id="T245" time="226.893" type="appl" />
         <tli id="T246" time="227.676" type="appl" />
         <tli id="T247" time="228.459" type="appl" />
         <tli id="T248" time="229.241" type="appl" />
         <tli id="T249" time="230.024" type="appl" />
         <tli id="T250" time="230.807" type="appl" />
         <tli id="T251" time="231.59" type="appl" />
         <tli id="T252" time="232.272" type="appl" />
         <tli id="T253" time="232.784" type="appl" />
         <tli id="T254" time="233.295" type="appl" />
         <tli id="T255" time="233.807" type="appl" />
         <tli id="T256" time="234.318" type="appl" />
         <tli id="T257" time="234.83" type="appl" />
         <tli id="T258" time="235.91225193487992" />
         <tli id="T259" time="237.3922451507373" />
         <tli id="T260" time="238.218" type="appl" />
         <tli id="T261" time="238.944" type="appl" />
         <tli id="T262" time="239.671" type="appl" />
         <tli id="T263" time="240.533" type="appl" />
         <tli id="T264" time="241.279" type="appl" />
         <tli id="T265" time="242.10555687871533" />
         <tli id="T266" time="242.76" type="appl" />
         <tli id="T267" time="243.451" type="appl" />
         <tli id="T268" time="244.141" type="appl" />
         <tli id="T269" time="244.831" type="appl" />
         <tli id="T270" time="245.521" type="appl" />
         <tli id="T271" time="246.212" type="appl" />
         <tli id="T272" time="246.902" type="appl" />
         <tli id="T273" time="247.592" type="appl" />
         <tli id="T274" time="248.282" type="appl" />
         <tli id="T275" time="248.973" type="appl" />
         <tli id="T276" time="250.63885109627114" />
         <tli id="T277" time="251.677" type="appl" />
         <tli id="T278" time="252.094" type="appl" />
         <tli id="T279" time="252.511" type="appl" />
         <tli id="T280" time="252.928" type="appl" />
         <tli id="T281" time="253.998" type="appl" />
         <tli id="T282" time="255.067" type="appl" />
         <tli id="T283" time="256.137" type="appl" />
         <tli id="T284" time="256.73" type="appl" />
         <tli id="T285" time="257.323" type="appl" />
         <tli id="T286" time="257.917" type="appl" />
         <tli id="T287" time="258.51" type="appl" />
         <tli id="T288" time="259.499" type="appl" />
         <tli id="T289" time="261.6854671263414" />
         <tli id="T290" time="262.317" type="appl" />
         <tli id="T291" time="262.953" type="appl" />
         <tli id="T292" time="263.5699897378759" />
         <tli id="T293" time="264.392" type="appl" />
         <tli id="T294" time="265.193" type="appl" />
         <tli id="T295" time="266.42544539874933" />
         <tli id="T296" time="267.219" type="appl" />
         <tli id="T297" time="267.957" type="appl" />
         <tli id="T298" time="268.696" type="appl" />
         <tli id="T299" time="269.434" type="appl" />
         <tli id="T300" time="270.39209388269126" />
         <tli id="T301" time="271.342" type="appl" />
         <tli id="T302" time="271.973" type="appl" />
         <tli id="T303" time="272.605" type="appl" />
         <tli id="T304" time="273.237" type="appl" />
         <tli id="T305" time="273.869" type="appl" />
         <tli id="T306" time="274.5" type="appl" />
         <tli id="T307" time="275.132" type="appl" />
         <tli id="T308" time="275.714" type="appl" />
         <tli id="T309" time="276.297" type="appl" />
         <tli id="T310" time="276.879" type="appl" />
         <tli id="T311" time="277.462" type="appl" />
         <tli id="T312" time="278.25872448950054" />
         <tli id="T313" time="278.808" type="appl" />
         <tli id="T314" time="279.571" type="appl" />
         <tli id="T315" time="280.334" type="appl" />
         <tli id="T316" time="284.53202906662546" />
         <tli id="T317" time="285.863" type="appl" />
         <tli id="T318" time="287.29" type="appl" />
         <tli id="T319" time="288.717" type="appl" />
         <tli id="T320" time="290.144" type="appl" />
         <tli id="T321" time="291.571" type="appl" />
         <tli id="T322" time="292.15" type="appl" />
         <tli id="T323" time="292.73" type="appl" />
         <tli id="T324" time="293.313" type="appl" />
         <tli id="T325" time="293.896" type="appl" />
         <tli id="T326" time="294.478" type="appl" />
         <tli id="T327" time="295.061" type="appl" />
         <tli id="T328" time="295.644" type="appl" />
         <tli id="T329" time="296.33" type="appl" />
         <tli id="T330" time="297.1519712180576" />
         <tli id="T331" time="297.641" type="appl" />
         <tli id="T332" time="298.261" type="appl" />
         <tli id="T333" time="298.88" type="appl" />
         <tli id="T334" time="299.5" type="appl" />
         <tli id="T335" time="300.12" type="appl" />
         <tli id="T336" time="301.31195214911605" />
         <tli id="T337" time="302.33" type="appl" />
         <tli id="T338" time="303.05" type="appl" />
         <tli id="T339" time="304.2652719447232" />
         <tli id="T340" time="305.045" type="appl" />
         <tli id="T341" time="306.138596690921" />
         <tli id="T342" time="306.93" type="appl" />
         <tli id="T343" time="307.321" type="appl" />
         <tli id="T344" time="307.711" type="appl" />
         <tli id="T345" time="308.367" type="appl" />
         <tli id="T346" time="309.024" type="appl" />
         <tli id="T347" time="309.68" type="appl" />
         <tli id="T348" time="310.336" type="appl" />
         <tli id="T349" time="310.993" type="appl" />
         <tli id="T350" time="311.649" type="appl" />
         <tli id="T351" time="312.31" type="appl" />
         <tli id="T352" time="312.97" type="appl" />
         <tli id="T353" time="313.631" type="appl" />
         <tli id="T354" time="314.78522372230367" />
         <tli id="T355" time="315.475" type="appl" />
         <tli id="T356" time="315.99" type="appl" />
         <tli id="T357" time="316.504" type="appl" />
         <tli id="T358" time="317.019" type="appl" />
         <tli id="T359" time="317.6652105207288" />
         <tli id="T360" time="319.01187101443685" />
         <tli id="T361" time="319.529" type="appl" />
         <tli id="T362" time="320.225" type="appl" />
         <tli id="T363" time="320.922" type="appl" />
         <tli id="T364" time="321.618" type="appl" />
         <tli id="T365" time="322.315" type="appl" />
         <tli id="T366" time="323.012" type="appl" />
         <tli id="T367" time="323.708" type="appl" />
         <tli id="T368" time="324.405" type="appl" />
         <tli id="T369" time="325.177" type="appl" />
         <tli id="T370" time="325.919" type="appl" />
         <tli id="T371" time="326.66" type="appl" />
         <tli id="T372" time="327.674" type="appl" />
         <tli id="T373" time="328.687" type="appl" />
         <tli id="T374" time="331.85847879352275" />
         <tli id="T375" time="332.345" type="appl" />
         <tli id="T376" time="332.978" type="appl" />
         <tli id="T377" time="333.612" type="appl" />
         <tli id="T378" time="334.245" type="appl" />
         <tli id="T379" time="334.878" type="appl" />
         <tli id="T380" time="335.511" type="appl" />
         <tli id="T381" time="336.285" type="appl" />
         <tli id="T382" time="337.058" type="appl" />
         <tli id="T383" time="337.832" type="appl" />
         <tli id="T384" time="338.70511407588975" />
         <tli id="T385" time="339.656" type="appl" />
         <tli id="T386" time="340.707" type="appl" />
         <tli id="T387" time="341.781" type="appl" />
         <tli id="T388" time="342.854" type="appl" />
         <tli id="T389" time="343.928" type="appl" />
         <tli id="T390" time="345.002" type="appl" />
         <tli id="T391" time="346.076" type="appl" />
         <tli id="T392" time="347.149" type="appl" />
         <tli id="T393" time="348.5850687871536" />
         <tli id="T394" time="350.32506081120204" />
         <tli id="T395" time="351.422" type="appl" />
         <tli id="T396" time="352.4717176378059" />
         <tli id="T397" time="352.846" type="appl" />
         <tli id="T398" time="353.379" type="appl" />
         <tli id="T399" time="354.21837629796187" />
         <tli id="T400" time="354.511" type="appl" />
         <tli id="T401" time="355.111" type="appl" />
         <tli id="T402" time="355.71" type="appl" />
         <tli id="T403" time="356.309" type="appl" />
         <tli id="T404" time="356.908" type="appl" />
         <tli id="T405" time="357.508" type="appl" />
         <tli id="T406" time="360.3050150640778" />
         <tli id="T407" time="361.548" type="appl" />
         <tli id="T408" time="362.505" type="appl" />
         <tli id="T409" time="363.462" type="appl" />
         <tli id="T410" time="364.5649955367482" />
         <tli id="T411" time="366.28498765247434" />
         <tli id="T412" time="366.846" type="appl" />
         <tli id="T413" time="367.477" type="appl" />
         <tli id="T414" time="368.108" type="appl" />
         <tli id="T415" time="368.738" type="appl" />
         <tli id="T416" time="369.654" type="appl" />
         <tli id="T417" time="370.57" type="appl" />
         <tli id="T418" time="371.486" type="appl" />
         <tli id="T419" time="372.402" type="appl" />
         <tli id="T420" time="373.318" type="appl" />
         <tli id="T421" time="374.234" type="appl" />
         <tli id="T422" time="375.9916098249441" />
         <tli id="T423" time="377.177" type="appl" />
         <tli id="T424" time="378.05826701825833" />
         <tli id="T425" time="378.685" type="appl" />
         <tli id="T426" time="379.388" type="appl" />
         <tli id="T427" time="380.09" type="appl" />
         <tli id="T428" time="382.3915804881109" />
         <tli id="T429" time="383.152" type="appl" />
         <tli id="T430" time="383.785" type="appl" />
         <tli id="T431" time="384.417" type="appl" />
         <tli id="T432" time="384.839" type="appl" />
         <tli id="T433" time="385.261" type="appl" />
         <tli id="T434" time="385.683" type="appl" />
         <tli id="T435" time="386.105" type="appl" />
         <tli id="T436" time="386.7382272301783" />
         <tli id="T437" time="387.432" type="appl" />
         <tli id="T438" time="388.198" type="appl" />
         <tli id="T439" time="388.964" type="appl" />
         <tli id="T440" time="389.731" type="appl" />
         <tli id="T441" time="390.5" type="appl" />
         <tli id="T442" time="391.269" type="appl" />
         <tli id="T443" time="392.038" type="appl" />
         <tli id="T444" time="392.806" type="appl" />
         <tli id="T445" time="393.575" type="appl" />
         <tli id="T446" time="394.344" type="appl" />
         <tli id="T447" time="395.113" type="appl" />
         <tli id="T448" time="397.34484527715586" />
         <tli id="T449" time="398.494" type="appl" />
         <tli id="T450" time="399.683" type="appl" />
         <tli id="T451" time="400.873" type="appl" />
         <tli id="T452" time="401.949" type="appl" />
         <tli id="T453" time="403.025" type="appl" />
         <tli id="T454" time="404.101" type="appl" />
         <tli id="T455" time="405.178" type="appl" />
         <tli id="T456" time="406.254" type="appl" />
         <tli id="T457" time="407.33" type="appl" />
         <tli id="T1072" time="407.7604" type="intp" />
         <tli id="T458" time="408.406" type="appl" />
         <tli id="T459" time="409.831" type="appl" />
         <tli id="T460" time="411.257" type="appl" />
         <tli id="T461" time="412.682" type="appl" />
         <tli id="T462" time="414.107" type="appl" />
         <tli id="T463" time="415.533" type="appl" />
         <tli id="T464" time="416.958" type="appl" />
         <tli id="T465" time="417.78" type="appl" />
         <tli id="T466" time="418.602" type="appl" />
         <tli id="T467" time="419.423" type="appl" />
         <tli id="T468" time="420.245" type="appl" />
         <tli id="T469" time="421.22473581409713" />
         <tli id="T470" time="421.951" type="appl" />
         <tli id="T471" time="422.835" type="appl" />
         <tli id="T472" time="423.718" type="appl" />
         <tli id="T473" time="424.602" type="appl" />
         <tli id="T474" time="425.486" type="appl" />
         <tli id="T475" time="428.3913696296225" />
         <tli id="T476" time="429.621" type="appl" />
         <tli id="T477" time="430.693" type="appl" />
         <tli id="T478" time="431.764" type="appl" />
         <tli id="T479" time="432.836" type="appl" />
         <tli id="T480" time="434.9713394676909" />
         <tli id="T481" time="435.861" type="appl" />
         <tli id="T482" time="436.572" type="appl" />
         <tli id="T483" time="437.283" type="appl" />
         <tli id="T484" time="437.995" type="appl" />
         <tli id="T485" time="438.706" type="appl" />
         <tli id="T486" time="439.417" type="appl" />
         <tli id="T487" time="440.128" type="appl" />
         <tli id="T488" time="440.839" type="appl" />
         <tli id="T489" time="441.55" type="appl" />
         <tli id="T490" time="442.262" type="appl" />
         <tli id="T491" time="442.973" type="appl" />
         <tli id="T492" time="443.684" type="appl" />
         <tli id="T493" time="444.395" type="appl" />
         <tli id="T494" time="445.345" type="appl" />
         <tli id="T495" time="446.11" type="appl" />
         <tli id="T496" time="446.875" type="appl" />
         <tli id="T497" time="447.64" type="appl" />
         <tli id="T498" time="451.1645985727245" />
         <tli id="T499" time="451.923" type="appl" />
         <tli id="T500" time="452.891" type="appl" />
         <tli id="T501" time="453.858" type="appl" />
         <tli id="T502" time="454.826" type="appl" />
         <tli id="T503" time="455.794" type="appl" />
         <tli id="T504" time="456.762" type="appl" />
         <tli id="T505" time="457.737" type="appl" />
         <tli id="T506" time="458.712" type="appl" />
         <tli id="T507" time="459.687" type="appl" />
         <tli id="T508" time="461.0378866478808" />
         <tli id="T509" time="462.049" type="appl" />
         <tli id="T510" time="462.837" type="appl" />
         <tli id="T511" time="463.626" type="appl" />
         <tli id="T512" time="464.39" type="appl" />
         <tli id="T513" time="465.154" type="appl" />
         <tli id="T514" time="465.918" type="appl" />
         <tli id="T515" time="466.683" type="appl" />
         <tli id="T516" time="467.447" type="appl" />
         <tli id="T517" time="468.211" type="appl" />
         <tli id="T518" time="470.5978428259863" />
         <tli id="T519" time="471.135" type="appl" />
         <tli id="T520" time="471.793" type="appl" />
         <tli id="T521" time="472.452" type="appl" />
         <tli id="T522" time="473.11" type="appl" />
         <tli id="T523" time="473.768" type="appl" />
         <tli id="T524" time="474.426" type="appl" />
         <tli id="T525" time="475.085" type="appl" />
         <tli id="T526" time="475.743" type="appl" />
         <tli id="T527" time="477.8311430025862" />
         <tli id="T528" time="478.793" type="appl" />
         <tli id="T529" time="479.527" type="appl" />
         <tli id="T530" time="480.26" type="appl" />
         <tli id="T531" time="480.994" type="appl" />
         <tli id="T532" time="481.727" type="appl" />
         <tli id="T533" time="482.461" type="appl" />
         <tli id="T534" time="483.194" type="appl" />
         <tli id="T535" time="483.831" type="appl" />
         <tli id="T536" time="484.313" type="appl" />
         <tli id="T537" time="484.794" type="appl" />
         <tli id="T538" time="485.276" type="appl" />
         <tli id="T539" time="485.757" type="appl" />
         <tli id="T540" time="486.239" type="appl" />
         <tli id="T541" time="487.97776315814866" />
         <tli id="T542" time="489.89108772099127" />
         <tli id="T543" time="490.72" type="appl" />
         <tli id="T544" time="491.38" type="appl" />
         <tli id="T545" time="492.04" type="appl" />
         <tli id="T546" time="492.8644074249209" />
         <tli id="T547" time="493.515" type="appl" />
         <tli id="T548" time="494.1" type="appl" />
         <tli id="T549" time="494.685" type="appl" />
         <tli id="T550" time="495.27" type="appl" />
         <tli id="T551" time="496.623" type="appl" />
         <tli id="T552" time="497.976" type="appl" />
         <tli id="T553" time="499.33" type="appl" />
         <tli id="T554" time="500.87103738998684" />
         <tli id="T555" time="501.45" type="appl" />
         <tli id="T556" time="502.218" type="appl" />
         <tli id="T557" time="502.985" type="appl" />
         <tli id="T558" time="503.753" type="appl" />
         <tli id="T559" time="504.9443520516483" />
         <tli id="T560" time="505.509" type="appl" />
         <tli id="T561" time="506.099" type="appl" />
         <tli id="T562" time="506.688" type="appl" />
         <tli id="T563" time="507.277" type="appl" />
         <tli id="T564" time="507.866" type="appl" />
         <tli id="T565" time="508.456" type="appl" />
         <tli id="T566" time="509.045" type="appl" />
         <tli id="T567" time="512.115" type="appl" />
         <tli id="T568" time="513.407" type="appl" />
         <tli id="T569" time="514.424" type="appl" />
         <tli id="T570" time="515.441" type="appl" />
         <tli id="T571" time="516.459" type="appl" />
         <tli id="T572" time="517.476" type="appl" />
         <tli id="T573" time="518.493" type="appl" />
         <tli id="T574" time="518.6976223413495" />
         <tli id="T575" time="523.4242673415425" />
         <tli id="T576" time="524.245" type="appl" />
         <tli id="T577" time="525.129" type="appl" />
         <tli id="T578" time="526.013" type="appl" />
         <tli id="T579" time="526.897" type="appl" />
         <tli id="T580" time="527.781" type="appl" />
         <tli id="T581" time="528.665" type="appl" />
         <tli id="T582" time="529.549" type="appl" />
         <tli id="T583" time="530.433" type="appl" />
         <tli id="T584" time="531.317" type="appl" />
         <tli id="T585" time="532.6175585337373" />
         <tli id="T586" time="533.955" type="appl" />
         <tli id="T587" time="534.914" type="appl" />
         <tli id="T588" time="535.874" type="appl" />
         <tli id="T589" time="537.0642048174167" />
         <tli id="T590" time="537.9" type="appl" />
         <tli id="T591" time="538.965" type="appl" />
         <tli id="T592" time="540.1975237879255" />
         <tli id="T593" time="540.848" type="appl" />
         <tli id="T594" time="541.665" type="appl" />
         <tli id="T595" time="542.482" type="appl" />
         <tli id="T596" time="543.510841933336" />
         <tli id="T597" time="543.863" type="appl" />
         <tli id="T598" time="544.428" type="appl" />
         <tli id="T599" time="544.993" type="appl" />
         <tli id="T600" time="545.8708311153788" />
         <tli id="T601" time="546.736" type="appl" />
         <tli id="T602" time="547.916" type="appl" />
         <tli id="T603" time="549.095" type="appl" />
         <tli id="T604" time="550.275" type="appl" />
         <tli id="T605" time="551.454" type="appl" />
         <tli id="T606" time="552.298" type="appl" />
         <tli id="T607" time="553.141" type="appl" />
         <tli id="T608" time="553.985" type="appl" />
         <tli id="T609" time="554.829" type="appl" />
         <tli id="T610" time="555.673" type="appl" />
         <tli id="T611" time="556.516" type="appl" />
         <tli id="T612" time="557.8307762921717" />
         <tli id="T613" time="558.789" type="appl" />
         <tli id="T614" time="560.218" type="appl" />
         <tli id="T615" time="561.646" type="appl" />
         <tli id="T616" time="563.075" type="appl" />
         <tli id="T617" time="564.504" type="appl" />
         <tli id="T618" time="565.933" type="appl" />
         <tli id="T619" time="567.361" type="appl" />
         <tli id="T620" time="568.79" type="appl" />
         <tli id="T621" time="570.219" type="appl" />
         <tli id="T622" time="571.196" type="appl" />
         <tli id="T623" time="572.174" type="appl" />
         <tli id="T624" time="573.151" type="appl" />
         <tli id="T625" time="574.128" type="appl" />
         <tli id="T626" time="575.105" type="appl" />
         <tli id="T627" time="576.082" type="appl" />
         <tli id="T628" time="577.06" type="appl" />
         <tli id="T629" time="578.037" type="appl" />
         <tli id="T630" time="578.666" type="appl" />
         <tli id="T631" time="579.295" type="appl" />
         <tli id="T632" time="579.924" type="appl" />
         <tli id="T633" time="580.467" type="appl" />
         <tli id="T634" time="581.01" type="appl" />
         <tli id="T635" time="581.552" type="appl" />
         <tli id="T636" time="582.095" type="appl" />
         <tli id="T637" time="583.7639907502123" />
         <tli id="T638" time="584.952" type="appl" />
         <tli id="T639" time="585.885" type="appl" />
         <tli id="T640" time="586.817" type="appl" />
         <tli id="T641" time="587.67" type="appl" />
         <tli id="T642" time="588.524" type="appl" />
         <tli id="T643" time="589.378" type="appl" />
         <tli id="T644" time="590.231" type="appl" />
         <tli id="T645" time="591.084" type="appl" />
         <tli id="T646" time="591.938" type="appl" />
         <tli id="T647" time="592.792" type="appl" />
         <tli id="T648" time="593.645" type="appl" />
         <tli id="T649" time="594.498" type="appl" />
         <tli id="T650" time="595.6639362020381" />
         <tli id="T651" time="596.15" type="appl" />
         <tli id="T652" time="596.947" type="appl" />
         <tli id="T653" time="597.744" type="appl" />
         <tli id="T654" time="598.542" type="appl" />
         <tli id="T655" time="599.441" type="appl" />
         <tli id="T656" time="600.34" type="appl" />
         <tli id="T657" time="601.238" type="appl" />
         <tli id="T658" time="602.137" type="appl" />
         <tli id="T659" time="603.036" type="appl" />
         <tli id="T660" time="603.972" type="appl" />
         <tli id="T661" time="604.907" type="appl" />
         <tli id="T662" time="606.2105545240486" />
         <tli id="T663" time="606.941" type="appl" />
         <tli id="T664" time="607.633" type="appl" />
         <tli id="T665" time="608.324" type="appl" />
         <tli id="T666" time="609.015" type="appl" />
         <tli id="T667" time="609.706" type="appl" />
         <tli id="T668" time="610.398" type="appl" />
         <tli id="T669" time="611.4705304128387" />
         <tli id="T670" time="612.764" type="appl" />
         <tli id="T671" time="613.888" type="appl" />
         <tli id="T672" time="615.012" type="appl" />
         <tli id="T673" time="616.136" type="appl" />
         <tli id="T674" time="616.911" type="appl" />
         <tli id="T675" time="617.686" type="appl" />
         <tli id="T676" time="618.462" type="appl" />
         <tli id="T677" time="619.237" type="appl" />
         <tli id="T678" time="621.4838178462519" />
         <tli id="T679" time="622.716" type="appl" />
         <tli id="T680" time="623.395" type="appl" />
         <tli id="T681" time="624.074" type="appl" />
         <tli id="T682" time="624.753" type="appl" />
         <tli id="T683" time="625.408" type="appl" />
         <tli id="T684" time="626.063" type="appl" />
         <tli id="T685" time="626.718" type="appl" />
         <tli id="T686" time="627.372" type="appl" />
         <tli id="T687" time="628.027" type="appl" />
         <tli id="T688" time="629.2837820919864" />
         <tli id="T689" time="629.767" type="appl" />
         <tli id="T690" time="630.546" type="appl" />
         <tli id="T691" time="631.325" type="appl" />
         <tli id="T692" time="632.104" type="appl" />
         <tli id="T693" time="633.313" type="appl" />
         <tli id="T694" time="634.521" type="appl" />
         <tli id="T695" time="635.73" type="appl" />
         <tli id="T696" time="636.341" type="appl" />
         <tli id="T697" time="636.952" type="appl" />
         <tli id="T698" time="637.562" type="appl" />
         <tli id="T699" time="638.173" type="appl" />
         <tli id="T700" time="638.784" type="appl" />
         <tli id="T701" time="639.394" type="appl" />
         <tli id="T702" time="640.005" type="appl" />
         <tli id="T703" time="640.750396196827" />
         <tli id="T704" time="641.266" type="appl" />
         <tli id="T705" time="641.916" type="appl" />
         <tli id="T706" time="642.567" type="appl" />
         <tli id="T707" time="643.4103840037056" />
         <tli id="T708" time="644.142" type="appl" />
         <tli id="T709" time="645.068" type="appl" />
         <tli id="T710" time="646.1103716272291" />
         <tli id="T711" time="646.912" type="appl" />
         <tli id="T712" time="647.83" type="appl" />
         <tli id="T713" time="648.595" type="appl" />
         <tli id="T714" time="649.361" type="appl" />
         <tli id="T715" time="650.126" type="appl" />
         <tli id="T716" time="650.892" type="appl" />
         <tli id="T717" time="652.2236769377749" />
         <tli id="T718" time="652.588" type="appl" />
         <tli id="T719" time="653.52" type="appl" />
         <tli id="T720" time="654.451" type="appl" />
         <tli id="T721" time="655.5303284470779" />
         <tli id="T722" time="656.546" type="appl" />
         <tli id="T723" time="657.711" type="appl" />
         <tli id="T724" time="658.876" type="appl" />
         <tli id="T725" time="660.91697042191" />
         <tli id="T726" time="661.319" type="appl" />
         <tli id="T727" time="662.597" type="appl" />
         <tli id="T728" time="663.876" type="appl" />
         <tli id="T729" time="665.155" type="appl" />
         <tli id="T730" time="666.433" type="appl" />
         <tli id="T731" time="668.0769376013279" />
         <tli id="T732" time="668.696" type="appl" />
         <tli id="T733" time="669.68" type="appl" />
         <tli id="T734" time="670.6506758035663" />
         <tli id="T735" time="671.308" type="appl" />
         <tli id="T736" time="671.952" type="appl" />
         <tli id="T737" time="672.597" type="appl" />
         <tli id="T738" time="673.4035798511927" />
         <tli id="T739" time="674.243" type="appl" />
         <tli id="T740" time="675.168" type="appl" />
         <tli id="T741" time="676.093" type="appl" />
         <tli id="T742" time="677.018" type="appl" />
         <tli id="T743" time="677.7768931376902" />
         <tli id="T744" time="678.489" type="appl" />
         <tli id="T745" time="679.036" type="appl" />
         <tli id="T746" time="679.582" type="appl" />
         <tli id="T747" time="680.128" type="appl" />
         <tli id="T748" time="680.675" type="appl" />
         <tli id="T749" time="681.221" type="appl" />
         <tli id="T750" time="681.768" type="appl" />
         <tli id="T751" time="682.314" type="appl" />
         <tli id="T752" time="683.076" type="appl" />
         <tli id="T753" time="683.839" type="appl" />
         <tli id="T754" time="686.5168530745773" />
         <tli id="T755" time="687.698" type="appl" />
         <tli id="T756" time="688.727" type="appl" />
         <tli id="T757" time="689.756" type="appl" />
         <tli id="T758" time="691.1901649859105" />
         <tli id="T759" time="692.361" type="appl" />
         <tli id="T760" time="693.372" type="appl" />
         <tli id="T761" time="694.384" type="appl" />
         <tli id="T762" time="697.8634677294835" />
         <tli id="T763" time="698.464" type="appl" />
         <tli id="T764" time="699.175" type="appl" />
         <tli id="T765" time="699.886" type="appl" />
         <tli id="T766" time="700.597" type="appl" />
         <tli id="T767" time="701.308" type="appl" />
         <tli id="T768" time="702.028" type="appl" />
         <tli id="T769" time="702.747" type="appl" />
         <tli id="T770" time="703.467" type="appl" />
         <tli id="T771" time="704.514" type="appl" />
         <tli id="T772" time="705.56" type="appl" />
         <tli id="T773" time="706.168" type="appl" />
         <tli id="T774" time="706.777" type="appl" />
         <tli id="T775" time="707.386" type="appl" />
         <tli id="T776" time="707.994" type="appl" />
         <tli id="T777" time="708.602" type="appl" />
         <tli id="T778" time="709.211" type="appl" />
         <tli id="T779" time="709.82" type="appl" />
         <tli id="T780" time="710.428" type="appl" />
         <tli id="T781" time="711.036" type="appl" />
         <tli id="T782" time="711.8567369190534" />
         <tli id="T783" time="712.252" type="appl" />
         <tli id="T784" time="712.858" type="appl" />
         <tli id="T785" time="713.7567282096811" />
         <tli id="T786" time="714.28" type="appl" />
         <tli id="T787" time="715.095" type="appl" />
         <tli id="T788" time="715.91" type="appl" />
         <tli id="T789" time="717.1433793522735" />
         <tli id="T790" time="717.88" type="appl" />
         <tli id="T791" time="718.9167045568594" />
         <tli id="T792" time="719.495" type="appl" />
         <tli id="T793" time="720.234" type="appl" />
         <tli id="T794" time="720.974" type="appl" />
         <tli id="T795" time="721.714" type="appl" />
         <tli id="T796" time="722.454" type="appl" />
         <tli id="T797" time="723.194" type="appl" />
         <tli id="T798" time="723.933" type="appl" />
         <tli id="T799" time="725.9900054668802" />
         <tli id="T800" time="726.631" type="appl" />
         <tli id="T801" time="727.219" type="appl" />
         <tli id="T802" time="727.808" type="appl" />
         <tli id="T803" time="728.5299938238247" />
         <tli id="T804" time="729.066" type="appl" />
         <tli id="T805" time="729.736" type="appl" />
         <tli id="T806" time="730.405" type="appl" />
         <tli id="T807" time="731.1033153613062" />
         <tli id="T808" time="731.865" type="appl" />
         <tli id="T809" time="732.655" type="appl" />
         <tli id="T810" time="733.446" type="appl" />
         <tli id="T811" time="734.236" type="appl" />
         <tli id="T812" time="735.026" type="appl" />
         <tli id="T813" time="735.816" type="appl" />
         <tli id="T814" time="736.419" type="appl" />
         <tli id="T815" time="737.022" type="appl" />
         <tli id="T816" time="737.624" type="appl" />
         <tli id="T817" time="738.227" type="appl" />
         <tli id="T818" time="738.83" type="appl" />
         <tli id="T819" time="739.433" type="appl" />
         <tli id="T820" time="740.036" type="appl" />
         <tli id="T821" time="740.639" type="appl" />
         <tli id="T822" time="741.242" type="appl" />
         <tli id="T823" time="741.844" type="appl" />
         <tli id="T824" time="742.447" type="appl" />
         <tli id="T825" time="746.2699125057901" />
         <tli id="T826" time="746.977" type="appl" />
         <tli id="T827" time="747.464" type="appl" />
         <tli id="T828" time="747.951" type="appl" />
         <tli id="T829" time="748.438" type="appl" />
         <tli id="T830" time="748.925" type="appl" />
         <tli id="T831" time="749.506" type="appl" />
         <tli id="T832" time="750.088" type="appl" />
         <tli id="T833" time="750.669" type="appl" />
         <tli id="T834" time="751.25" type="appl" />
         <tli id="T835" time="751.832" type="appl" />
         <tli id="T836" time="752.413" type="appl" />
         <tli id="T837" time="753.445" type="appl" />
         <tli id="T838" time="754.476" type="appl" />
         <tli id="T839" time="755.508" type="appl" />
         <tli id="T840" time="757.5431941635142" />
         <tli id="T841" time="757.871" type="appl" />
         <tli id="T842" time="758.597" type="appl" />
         <tli id="T843" time="759.322" type="appl" />
         <tli id="T844" time="760.048" type="appl" />
         <tli id="T845" time="760.774" type="appl" />
         <tli id="T846" time="761.5" type="appl" />
         <tli id="T847" time="762.059" type="appl" />
         <tli id="T848" time="762.618" type="appl" />
         <tli id="T849" time="763.177" type="appl" />
         <tli id="T850" time="763.956498098896" />
         <tli id="T851" time="764.239" type="appl" />
         <tli id="T852" time="764.742" type="appl" />
         <tli id="T853" time="765.245" type="appl" />
         <tli id="T854" time="765.748" type="appl" />
         <tli id="T855" time="766.251" type="appl" />
         <tli id="T856" time="766.754" type="appl" />
         <tli id="T857" time="767.509815144175" />
         <tli id="T858" time="768.304" type="appl" />
         <tli id="T859" time="769.35" type="appl" />
         <tli id="T860" time="770.396" type="appl" />
         <tli id="T861" time="771.9031283389949" />
         <tli id="T862" time="772.369" type="appl" />
         <tli id="T863" time="773.295" type="appl" />
         <tli id="T864" time="774.221" type="appl" />
         <tli id="T865" time="775.147" type="appl" />
         <tli id="T866" time="776.073" type="appl" />
         <tli id="T867" time="776.999" type="appl" />
         <tli id="T868" time="777.682" type="appl" />
         <tli id="T869" time="778.364" type="appl" />
         <tli id="T870" time="779.047" type="appl" />
         <tli id="T871" time="779.73" type="appl" />
         <tli id="T872" time="780.323" type="appl" />
         <tli id="T873" time="780.916" type="appl" />
         <tli id="T874" time="781.508" type="appl" />
         <tli id="T875" time="782.101" type="appl" />
         <tli id="T876" time="783.1297435439667" />
         <tli id="T877" time="783.952" type="appl" />
         <tli id="T878" time="784.663" type="appl" />
         <tli id="T879" time="785.375" type="appl" />
         <tli id="T880" time="786.087" type="appl" />
         <tli id="T881" time="786.798" type="appl" />
         <tli id="T882" time="787.51" type="appl" />
         <tli id="T883" time="788.222" type="appl" />
         <tli id="T884" time="788.933" type="appl" />
         <tli id="T885" time="789.645" type="appl" />
         <tli id="T886" time="790.357" type="appl" />
         <tli id="T887" time="791.068" type="appl" />
         <tli id="T888" time="792.1430355612599" />
         <tli id="T889" time="792.923" type="appl" />
         <tli id="T890" time="793.496" type="appl" />
         <tli id="T891" time="794.068" type="appl" />
         <tli id="T892" time="794.9296894541806" />
         <tli id="T893" time="800.6229966899559" />
         <tli id="T894" time="801.854" type="appl" />
         <tli id="T895" time="803.148" type="appl" />
         <tli id="T896" time="804.442" type="appl" />
         <tli id="T897" time="805.849639398209" />
         <tli id="T898" time="806.453" type="appl" />
         <tli id="T899" time="807.171" type="appl" />
         <tli id="T900" time="807.889" type="appl" />
         <tli id="T901" time="808.6629598355594" />
         <tli id="T902" time="809.648" type="appl" />
         <tli id="T903" time="810.689" type="appl" />
         <tli id="T904" time="811.731" type="appl" />
         <tli id="T905" time="812.772" type="appl" />
         <tli id="T906" time="813.813" type="appl" />
         <tli id="T907" time="814.356" type="appl" />
         <tli id="T908" time="814.9" type="appl" />
         <tli id="T909" time="815.443" type="appl" />
         <tli id="T910" time="815.876260103837" />
         <tli id="T911" time="817.636" type="appl" />
         <tli id="T912" time="819.2695778825369" />
         <tli id="T913" time="820.068" type="appl" />
         <tli id="T914" time="820.764" type="appl" />
         <tli id="T915" time="821.461" type="appl" />
         <tli id="T916" time="822.158" type="appl" />
         <tli id="T917" time="822.854" type="appl" />
         <tli id="T918" time="823.551" type="appl" />
         <tli id="T919" time="824.261" type="appl" />
         <tli id="T920" time="824.971" type="appl" />
         <tli id="T921" time="825.68" type="appl" />
         <tli id="T922" time="826.39" type="appl" />
         <tli id="T923" time="827.1" type="appl" />
         <tli id="T924" time="827.81" type="appl" />
         <tli id="T925" time="828.519" type="appl" />
         <tli id="T926" time="829.229" type="appl" />
         <tli id="T927" time="829.939" type="appl" />
         <tli id="T928" time="831.136" type="appl" />
         <tli id="T1073" time="831.3606249999999" type="intp" />
         <tli id="T1071" time="831.7349999999999" type="intp" />
         <tli id="T929" time="833.4628461553308" />
         <tli id="T930" time="834.349" type="appl" />
         <tli id="T931" time="835.096" type="appl" />
         <tli id="T932" time="835.842" type="appl" />
         <tli id="T933" time="836.588" type="appl" />
         <tli id="T934" time="837.335" type="appl" />
         <tli id="T935" time="838.4228234192851" />
         <tli id="T936" time="839.129" type="appl" />
         <tli id="T937" time="840.177" type="appl" />
         <tli id="T1075" time="840.6611538461539" type="intp" />
         <tli id="T938" time="841.226" type="appl" />
         <tli id="T939" time="842.274" type="appl" />
         <tli id="T940" time="843.8020175115909" />
         <tli id="T941" time="844.66" type="appl" />
         <tli id="T942" time="845.26" type="appl" />
         <tli id="T943" time="845.859" type="appl" />
         <tli id="T944" time="846.459" type="appl" />
         <tli id="T945" time="847.059" type="appl" />
         <tli id="T946" time="847.931" type="appl" />
         <tli id="T947" time="848.803" type="appl" />
         <tli id="T948" time="849.675" type="appl" />
         <tli id="T949" time="850.716" type="appl" />
         <tli id="T950" time="851.9446885197249" />
         <tli id="T951" time="852.722" type="appl" />
         <tli id="T952" time="853.686" type="appl" />
         <tli id="T953" time="854.273" type="appl" />
         <tli id="T954" time="854.859" type="appl" />
         <tli id="T955" time="855.446" type="appl" />
         <tli id="T956" time="856.284" type="appl" />
         <tli id="T957" time="857.122" type="appl" />
         <tli id="T958" time="857.961" type="appl" />
         <tli id="T959" time="858.799" type="appl" />
         <tli id="T960" time="859.278" type="appl" />
         <tli id="T961" time="859.756" type="appl" />
         <tli id="T962" time="860.3227230323091" />
         <tli id="T963" time="860.797" type="appl" />
         <tli id="T964" time="861.359" type="appl" />
         <tli id="T965" time="861.921" type="appl" />
         <tli id="T966" time="862.483" type="appl" />
         <tli id="T967" time="863.045" type="appl" />
         <tli id="T968" time="863.607" type="appl" />
         <tli id="T969" time="864.017" type="appl" />
         <tli id="T970" time="864.427" type="appl" />
         <tli id="T971" time="864.837" type="appl" />
         <tli id="T972" time="865.247" type="appl" />
         <tli id="T973" time="865.657" type="appl" />
         <tli id="T974" time="866.323" type="appl" />
         <tli id="T975" time="866.99" type="appl" />
         <tli id="T976" time="867.656" type="appl" />
         <tli id="T977" time="868.322" type="appl" />
         <tli id="T978" time="868.988" type="appl" />
         <tli id="T979" time="869.655" type="appl" />
         <tli id="T980" time="871.8360035899019" />
         <tli id="T981" time="872.559" type="appl" />
         <tli id="T982" time="873.146" type="appl" />
         <tli id="T983" time="873.732" type="appl" />
         <tli id="T984" time="874.319" type="appl" />
         <tli id="T985" time="874.9983849271916" />
         <tli id="T986" time="875.478" type="appl" />
         <tli id="T987" time="876.051" type="appl" />
         <tli id="T988" time="876.921" type="appl" />
         <tli id="T989" time="877.791" type="appl" />
         <tli id="T990" time="878.66" type="appl" />
         <tli id="T991" time="880.5426303462519" />
         <tli id="T992" time="881.444" type="appl" />
         <tli id="T993" time="883.5226166862889" />
         <tli id="T994" time="883.864" type="appl" />
         <tli id="T995" time="885.181" type="appl" />
         <tli id="T996" time="886.5826026596154" />
         <tli id="T997" time="887.342" type="appl" />
         <tli id="T998" time="888.186" type="appl" />
         <tli id="T999" time="889.031" type="appl" />
         <tli id="T1000" time="889.875" type="appl" />
         <tli id="T1001" time="890.72" type="appl" />
         <tli id="T1002" time="891.52" type="appl" />
         <tli id="T1003" time="892.319" type="appl" />
         <tli id="T1004" time="893.119" type="appl" />
         <tli id="T1005" time="893.918" type="appl" />
         <tli id="T1006" time="894.8292315245502" />
         <tli id="T1007" time="895.404" type="appl" />
         <tli id="T1008" time="896.089" type="appl" />
         <tli id="T1009" time="896.74" type="appl" />
         <tli id="T1010" time="897.392" type="appl" />
         <tli id="T1011" time="898.043" type="appl" />
         <tli id="T1012" time="898.695" type="appl" />
         <tli id="T1013" time="899.346" type="appl" />
         <tli id="T1014" time="899.997" type="appl" />
         <tli id="T1015" time="900.649" type="appl" />
         <tli id="T1016" time="901.9891987039682" />
         <tli id="T1017" time="903.3358591976762" />
         <tli id="T1018" time="903.8958566307033" />
         <tli id="T1019" time="904.694" type="appl" />
         <tli id="T1020" time="905.408" type="appl" />
         <tli id="T1021" time="906.123" type="appl" />
         <tli id="T1022" time="906.837" type="appl" />
         <tli id="T1023" time="907.551" type="appl" />
         <tli id="T1024" time="908.279" type="appl" />
         <tli id="T1025" time="909.007" type="appl" />
         <tli id="T1026" time="910.066" type="appl" />
         <tli id="T1027" time="911.126" type="appl" />
         <tli id="T1028" time="912.185" type="appl" />
         <tli id="T1029" time="913.245" type="appl" />
         <tli id="T1030" time="914.5024746776808" />
         <tli id="T1031" time="915.511" type="appl" />
         <tli id="T1032" time="916.718" type="appl" />
         <tli id="T1033" time="917.45" type="appl" />
         <tli id="T1034" time="918.3157905311512" />
         <tli id="T1035" time="918.843" type="appl" />
         <tli id="T1036" time="919.506" type="appl" />
         <tli id="T1037" time="920.168" type="appl" />
         <tli id="T1038" time="920.83" type="appl" />
         <tli id="T1039" time="921.819" type="appl" />
         <tli id="T1040" time="922.808" type="appl" />
         <tli id="T1041" time="923.543" type="appl" />
         <tli id="T1042" time="924.278" type="appl" />
         <tli id="T1043" time="925.22" type="appl" />
         <tli id="T1044" time="926.162" type="appl" />
         <tli id="T1045" time="927.105" type="appl" />
         <tli id="T1046" time="928.047" type="appl" />
         <tli id="T1047" time="928.989" type="appl" />
         <tli id="T1048" time="929.931" type="appl" />
         <tli id="T1049" time="930.874" type="appl" />
         <tli id="T1050" time="931.816" type="appl" />
         <tli id="T1051" time="933.0290564203274" />
         <tli id="T1052" time="937.212" type="appl" />
         <tli id="T1053" time="937.935" type="appl" />
         <tli id="T1054" time="938.658" type="appl" />
         <tli id="T1055" time="939.382" type="appl" />
         <tli id="T1056" time="940.106" type="appl" />
         <tli id="T1057" time="940.829" type="appl" />
         <tli id="T1058" time="941.552" type="appl" />
         <tli id="T1059" time="942.276" type="appl" />
         <tli id="T1060" time="943.028" type="appl" />
         <tli id="T1061" time="943.781" type="appl" />
         <tli id="T1062" time="944.533" type="appl" />
         <tli id="T1063" time="945.194" type="appl" />
         <tli id="T1064" time="945.854" type="appl" />
         <tli id="T1065" time="946.515" type="appl" />
         <tli id="T1066" time="947.176" type="appl" />
         <tli id="T1067" time="947.837" type="appl" />
         <tli id="T1068" time="948.497" type="appl" />
         <tli id="T1069" time="949.4823143335522" />
         <tli id="T1070" time="949.882" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-PKZ"
                      id="tx-PKZ"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T61" start="T57">
            <tli id="T57.tx-PKZ.1" />
            <tli id="T57.tx-PKZ.2" />
            <tli id="T57.tx-PKZ.3" />
         </timeline-fork>
         <timeline-fork end="T238" start="T236">
            <tli id="T236.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T425" start="T424">
            <tli id="T424.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T699" start="T698">
            <tli id="T698.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T874" start="T873">
            <tli id="T873.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T880" start="T877">
            <tli id="T877.tx-PKZ.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PKZ">
            <ts e="T1069" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Miʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">kamen</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">šobibaʔ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">Abalakoftə</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Miʔnʼibeʔ</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">jakše</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">ibi</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">il</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_32" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">Šaːbibaʔ</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">jakše</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_41" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">Dĭgəttə</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">mĭmbibeʔ</ts>
                  <nts id="Seg_47" n="HIAT:ip">,</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">mĭmbibeʔ</ts>
                  <nts id="Seg_51" n="HIAT:ip">.</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_54" n="HIAT:u" s="T13">
                  <nts id="Seg_55" n="HIAT:ip">(</nts>
                  <ts e="T14" id="Seg_57" n="HIAT:w" s="T13">Koj-</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_60" n="HIAT:w" s="T14">kojak-</ts>
                  <nts id="Seg_61" n="HIAT:ip">)</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_64" n="HIAT:w" s="T15">girgidəʔi</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_67" n="HIAT:w" s="T16">büzʼeʔi</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_70" n="HIAT:w" s="T17">kubibaʔ</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_74" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_76" n="HIAT:w" s="T18">Dʼăbaktərbibaʔ</ts>
                  <nts id="Seg_77" n="HIAT:ip">,</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_80" n="HIAT:w" s="T19">dĭzeŋ</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_83" n="HIAT:w" s="T20">ĭmbidə</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_86" n="HIAT:w" s="T21">ej</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_89" n="HIAT:w" s="T22">tĭmneʔi</ts>
                  <nts id="Seg_90" n="HIAT:ip">,</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_93" n="HIAT:w" s="T23">nuzaŋ</ts>
                  <nts id="Seg_94" n="HIAT:ip">.</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_97" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_99" n="HIAT:w" s="T24">Nuzaŋ</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_102" n="HIAT:w" s="T25">il</ts>
                  <nts id="Seg_103" n="HIAT:ip">.</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_106" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_108" n="HIAT:w" s="T26">Kăde</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_111" n="HIAT:w" s="T27">dĭzeŋ</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_114" n="HIAT:w" s="T28">amnolaʔbəʔjə</ts>
                  <nts id="Seg_115" n="HIAT:ip">!</nts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_118" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_120" n="HIAT:w" s="T29">Miʔ</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_123" n="HIAT:w" s="T30">tenəluʔpibeʔ</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_126" n="HIAT:w" s="T31">bazo</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_129" n="HIAT:w" s="T32">parzittə</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_132" n="HIAT:w" s="T33">maːndə</ts>
                  <nts id="Seg_133" n="HIAT:ip">.</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_136" n="HIAT:u" s="T34">
                  <nts id="Seg_137" n="HIAT:ip">(</nts>
                  <nts id="Seg_138" n="HIAT:ip">(</nts>
                  <ats e="T35" id="Seg_139" n="HIAT:non-pho" s="T34">BRK</ats>
                  <nts id="Seg_140" n="HIAT:ip">)</nts>
                  <nts id="Seg_141" n="HIAT:ip">)</nts>
                  <nts id="Seg_142" n="HIAT:ip">.</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_145" n="HIAT:u" s="T35">
                  <nts id="Seg_146" n="HIAT:ip">(</nts>
                  <nts id="Seg_147" n="HIAT:ip">(</nts>
                  <ats e="T36" id="Seg_148" n="HIAT:non-pho" s="T35">BRK</ats>
                  <nts id="Seg_149" n="HIAT:ip">)</nts>
                  <nts id="Seg_150" n="HIAT:ip">)</nts>
                  <nts id="Seg_151" n="HIAT:ip">.</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_154" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_156" n="HIAT:w" s="T36">Onʼiʔ</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_159" n="HIAT:w" s="T37">koʔbdo</ts>
                  <nts id="Seg_160" n="HIAT:ip">,</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_163" n="HIAT:w" s="T38">Jelʼa</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_166" n="HIAT:w" s="T39">kambi</ts>
                  <nts id="Seg_167" n="HIAT:ip">,</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_170" n="HIAT:w" s="T40">süt</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_173" n="HIAT:w" s="T41">măndərdəsʼtə</ts>
                  <nts id="Seg_174" n="HIAT:ip">.</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_177" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_179" n="HIAT:w" s="T42">I</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_181" n="HIAT:ip">(</nts>
                  <ts e="T44" id="Seg_183" n="HIAT:w" s="T43">šo-</ts>
                  <nts id="Seg_184" n="HIAT:ip">)</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_187" n="HIAT:w" s="T44">šobi</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_190" n="HIAT:w" s="T45">nükenə</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_193" n="HIAT:w" s="T46">onʼiʔ</ts>
                  <nts id="Seg_194" n="HIAT:ip">,</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_197" n="HIAT:w" s="T47">nükenə</ts>
                  <nts id="Seg_198" n="HIAT:ip">.</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_201" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_203" n="HIAT:w" s="T48">Dĭ</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_206" n="HIAT:w" s="T49">nu</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_209" n="HIAT:w" s="T50">ige</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_211" n="HIAT:ip">(</nts>
                  <ts e="T52" id="Seg_213" n="HIAT:w" s="T51">dĭ</ts>
                  <nts id="Seg_214" n="HIAT:ip">)</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_217" n="HIAT:w" s="T52">nüke</ts>
                  <nts id="Seg_218" n="HIAT:ip">,</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_221" n="HIAT:w" s="T53">dĭ</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_224" n="HIAT:w" s="T54">măndə:</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_227" n="HIAT:w" s="T55">Лицо</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_229" n="HIAT:ip">—</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_232" n="HIAT:w" s="T56">kadəl</ts>
                  <nts id="Seg_233" n="HIAT:ip">.</nts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_236" n="HIAT:u" s="T57">
                  <nts id="Seg_237" n="HIAT:ip">(</nts>
                  <ts e="T57.tx-PKZ.1" id="Seg_239" n="HIAT:w" s="T57">A</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57.tx-PKZ.2" id="Seg_242" n="HIAT:w" s="T57.tx-PKZ.1">-</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57.tx-PKZ.3" id="Seg_245" n="HIAT:w" s="T57.tx-PKZ.2">a</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_248" n="HIAT:w" s="T57.tx-PKZ.3">-</ts>
                  <nts id="Seg_249" n="HIAT:ip">)</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_252" n="HIAT:w" s="T61">A</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_255" n="HIAT:w" s="T62">волосья</ts>
                  <nts id="Seg_256" n="HIAT:ip">—</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_259" n="HIAT:w" s="T63">eʔbdə</ts>
                  <nts id="Seg_260" n="HIAT:ip">.</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_263" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_265" n="HIAT:w" s="T64">Dĭ</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_268" n="HIAT:w" s="T65">bar</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_271" n="HIAT:w" s="T66">parluʔpi</ts>
                  <nts id="Seg_272" n="HIAT:ip">.</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_275" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_277" n="HIAT:w" s="T67">Bar</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_280" n="HIAT:w" s="T68">sĭjdə</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_283" n="HIAT:w" s="T69">ugandə</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_286" n="HIAT:w" s="T70">jakše</ts>
                  <nts id="Seg_287" n="HIAT:ip">.</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_290" n="HIAT:u" s="T71">
                  <nts id="Seg_291" n="HIAT:ip">(</nts>
                  <ts e="T72" id="Seg_293" n="HIAT:w" s="T71">Măn-</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_296" n="HIAT:w" s="T72">măn-</ts>
                  <nts id="Seg_297" n="HIAT:ip">)</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_300" n="HIAT:w" s="T73">Mămbi:</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_303" n="HIAT:w" s="T74">Dön</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_306" n="HIAT:w" s="T75">nüke</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_309" n="HIAT:w" s="T76">ige</ts>
                  <nts id="Seg_310" n="HIAT:ip">.</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_313" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_315" n="HIAT:w" s="T77">Nudla</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_318" n="HIAT:w" s="T78">molia</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_321" n="HIAT:w" s="T79">dʼăbaktərzittə</ts>
                  <nts id="Seg_322" n="HIAT:ip">.</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_325" n="HIAT:u" s="T80">
                  <nts id="Seg_326" n="HIAT:ip">(</nts>
                  <nts id="Seg_327" n="HIAT:ip">(</nts>
                  <ats e="T81" id="Seg_328" n="HIAT:non-pho" s="T80">BRK</ats>
                  <nts id="Seg_329" n="HIAT:ip">)</nts>
                  <nts id="Seg_330" n="HIAT:ip">)</nts>
                  <nts id="Seg_331" n="HIAT:ip">.</nts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_334" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_336" n="HIAT:w" s="T81">Dĭ</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_339" n="HIAT:w" s="T82">măna</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_342" n="HIAT:w" s="T83">nörbəbi:</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_345" n="HIAT:w" s="T84">девушка</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_348" n="HIAT:w" s="T85">koʔbdo</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_351" n="HIAT:w" s="T86">ige</ts>
                  <nts id="Seg_352" n="HIAT:ip">,</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_355" n="HIAT:w" s="T87">a</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_358" n="HIAT:w" s="T88">парень</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_361" n="HIAT:w" s="T89">nʼi</ts>
                  <nts id="Seg_362" n="HIAT:ip">.</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_365" n="HIAT:u" s="T90">
                  <ts e="T92" id="Seg_367" n="HIAT:w" s="T90">Dĭgəttə</ts>
                  <nts id="Seg_368" n="HIAT:ip">…</nts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_371" n="HIAT:u" s="T92">
                  <nts id="Seg_372" n="HIAT:ip">(</nts>
                  <nts id="Seg_373" n="HIAT:ip">(</nts>
                  <ats e="T93" id="Seg_374" n="HIAT:non-pho" s="T92">BRK</ats>
                  <nts id="Seg_375" n="HIAT:ip">)</nts>
                  <nts id="Seg_376" n="HIAT:ip">)</nts>
                  <nts id="Seg_377" n="HIAT:ip">.</nts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_380" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_382" n="HIAT:w" s="T93">Dĭgəttə</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_385" n="HIAT:w" s="T94">miʔ</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_388" n="HIAT:w" s="T95">maluʔpibeʔ</ts>
                  <nts id="Seg_389" n="HIAT:ip">.</nts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_392" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_394" n="HIAT:w" s="T96">Dĭ</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_397" n="HIAT:w" s="T97">nükenə</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_399" n="HIAT:ip">(</nts>
                  <ts e="T99" id="Seg_401" n="HIAT:w" s="T98">mĭm-</ts>
                  <nts id="Seg_402" n="HIAT:ip">)</nts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_405" n="HIAT:w" s="T99">mĭmbibeʔ</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_408" n="HIAT:w" s="T100">dʼăbaktərzittə</ts>
                  <nts id="Seg_409" n="HIAT:ip">.</nts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_412" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_414" n="HIAT:w" s="T101">Nagur</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_417" n="HIAT:w" s="T102">bʼeʔ</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_420" n="HIAT:w" s="T103">dʼala</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_423" n="HIAT:w" s="T104">dʼăbaktərbibaʔ</ts>
                  <nts id="Seg_424" n="HIAT:ip">.</nts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_427" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_429" n="HIAT:w" s="T105">Bar</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_432" n="HIAT:w" s="T106">ĭmbi</ts>
                  <nts id="Seg_433" n="HIAT:ip">.</nts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_436" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_438" n="HIAT:w" s="T107">Nörbəbi</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_441" n="HIAT:w" s="T108">bostə</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_443" n="HIAT:ip">(</nts>
                  <ts e="T110" id="Seg_445" n="HIAT:w" s="T109">šĭ-</ts>
                  <nts id="Seg_446" n="HIAT:ip">)</nts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_449" n="HIAT:w" s="T110">šĭkətsi</ts>
                  <nts id="Seg_450" n="HIAT:ip">.</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_453" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_455" n="HIAT:w" s="T111">Dăre</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_458" n="HIAT:w" s="T112">mămbi:</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_461" n="HIAT:w" s="T113">Măn</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_464" n="HIAT:w" s="T114">tʼotkam</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_467" n="HIAT:w" s="T115">külambi</ts>
                  <nts id="Seg_468" n="HIAT:ip">.</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_471" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_473" n="HIAT:w" s="T116">Šide</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_476" n="HIAT:w" s="T117">bʼeʔ</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_479" n="HIAT:w" s="T118">pʼe</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_482" n="HIAT:w" s="T119">kambi</ts>
                  <nts id="Seg_483" n="HIAT:ip">.</nts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_486" n="HIAT:u" s="T120">
                  <nts id="Seg_487" n="HIAT:ip">(</nts>
                  <ts e="T121" id="Seg_489" n="HIAT:w" s="T120">Šindəzizdə</ts>
                  <nts id="Seg_490" n="HIAT:ip">)</nts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_493" n="HIAT:w" s="T121">ej</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_496" n="HIAT:w" s="T122">dʼăbaktəriam</ts>
                  <nts id="Seg_497" n="HIAT:ip">.</nts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_500" n="HIAT:u" s="T123">
                  <nts id="Seg_501" n="HIAT:ip">(</nts>
                  <nts id="Seg_502" n="HIAT:ip">(</nts>
                  <ats e="T124" id="Seg_503" n="HIAT:non-pho" s="T123">BRK</ats>
                  <nts id="Seg_504" n="HIAT:ip">)</nts>
                  <nts id="Seg_505" n="HIAT:ip">)</nts>
                  <nts id="Seg_506" n="HIAT:ip">.</nts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_509" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_511" n="HIAT:w" s="T124">Dĭʔnə</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_514" n="HIAT:w" s="T125">tüj</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_516" n="HIAT:ip">(</nts>
                  <ts e="T127" id="Seg_518" n="HIAT:w" s="T126">š-</ts>
                  <nts id="Seg_519" n="HIAT:ip">)</nts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_522" n="HIAT:w" s="T127">sejʔpü</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_525" n="HIAT:w" s="T128">pʼe</ts>
                  <nts id="Seg_526" n="HIAT:ip">.</nts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_529" n="HIAT:u" s="T129">
                  <ts e="T130" id="Seg_531" n="HIAT:w" s="T129">Dĭ</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_534" n="HIAT:w" s="T130">ugandə</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_537" n="HIAT:w" s="T131">jakše</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_540" n="HIAT:w" s="T132">nüke</ts>
                  <nts id="Seg_541" n="HIAT:ip">.</nts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_544" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_546" n="HIAT:w" s="T133">Bar</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_549" n="HIAT:w" s="T134">ĭmbit</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_552" n="HIAT:w" s="T135">ige</ts>
                  <nts id="Seg_553" n="HIAT:ip">.</nts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_556" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_558" n="HIAT:w" s="T136">Amzittə</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_561" n="HIAT:w" s="T137">mĭlie</ts>
                  <nts id="Seg_562" n="HIAT:ip">,</nts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_565" n="HIAT:w" s="T138">a</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_568" n="HIAT:w" s="T139">ej</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_571" n="HIAT:w" s="T140">amnal</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_574" n="HIAT:w" s="T141">dăk</ts>
                  <nts id="Seg_575" n="HIAT:ip">,</nts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_578" n="HIAT:w" s="T142">dĭ</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_581" n="HIAT:w" s="T143">bar</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_584" n="HIAT:w" s="T144">kurollaʔbə</ts>
                  <nts id="Seg_585" n="HIAT:ip">.</nts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_588" n="HIAT:u" s="T145">
                  <nts id="Seg_589" n="HIAT:ip">(</nts>
                  <nts id="Seg_590" n="HIAT:ip">(</nts>
                  <ats e="T146" id="Seg_591" n="HIAT:non-pho" s="T145">BRK</ats>
                  <nts id="Seg_592" n="HIAT:ip">)</nts>
                  <nts id="Seg_593" n="HIAT:ip">)</nts>
                  <nts id="Seg_594" n="HIAT:ip">.</nts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_597" n="HIAT:u" s="T146">
                  <nts id="Seg_598" n="HIAT:ip">(</nts>
                  <nts id="Seg_599" n="HIAT:ip">(</nts>
                  <ats e="T147" id="Seg_600" n="HIAT:non-pho" s="T146">BRK</ats>
                  <nts id="Seg_601" n="HIAT:ip">)</nts>
                  <nts id="Seg_602" n="HIAT:ip">)</nts>
                  <nts id="Seg_603" n="HIAT:ip">.</nts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_606" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_608" n="HIAT:w" s="T147">Dĭgəttə</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_611" n="HIAT:w" s="T148">dĭ</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_614" n="HIAT:w" s="T149">üge</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_617" n="HIAT:w" s="T150">maːʔndə</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_620" n="HIAT:w" s="T151">iʔgö</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_623" n="HIAT:w" s="T152">togonoria</ts>
                  <nts id="Seg_624" n="HIAT:ip">,</nts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_627" n="HIAT:w" s="T153">ej</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_630" n="HIAT:w" s="T154">sedem</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_633" n="HIAT:w" s="T155">üjündə</ts>
                  <nts id="Seg_634" n="HIAT:ip">.</nts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_637" n="HIAT:u" s="T156">
                  <nts id="Seg_638" n="HIAT:ip">(</nts>
                  <nts id="Seg_639" n="HIAT:ip">(</nts>
                  <ats e="T157" id="Seg_640" n="HIAT:non-pho" s="T156">BRK</ats>
                  <nts id="Seg_641" n="HIAT:ip">)</nts>
                  <nts id="Seg_642" n="HIAT:ip">)</nts>
                  <nts id="Seg_643" n="HIAT:ip">.</nts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_646" n="HIAT:u" s="T157">
                  <ts e="T1074" id="Seg_648" n="HIAT:w" s="T157">Kazan</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_651" n="HIAT:w" s="T1074">turanə</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_653" n="HIAT:ip">(</nts>
                  <ts e="T159" id="Seg_655" n="HIAT:w" s="T158">bʼeʔ</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_658" n="HIAT:w" s="T159">su-</ts>
                  <nts id="Seg_659" n="HIAT:ip">)</nts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_662" n="HIAT:w" s="T160">bʼeʔ</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_665" n="HIAT:w" s="T161">šide</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_668" n="HIAT:w" s="T162">sumna</ts>
                  <nts id="Seg_669" n="HIAT:ip">,</nts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_672" n="HIAT:w" s="T163">dĭ</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_675" n="HIAT:w" s="T164">bar</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_677" n="HIAT:ip">(</nts>
                  <ts e="T166" id="Seg_679" n="HIAT:w" s="T165">ka-</ts>
                  <nts id="Seg_680" n="HIAT:ip">)</nts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_683" n="HIAT:w" s="T166">kalia</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_686" n="HIAT:w" s="T167">üjüzi</ts>
                  <nts id="Seg_687" n="HIAT:ip">.</nts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_690" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_692" n="HIAT:w" s="T168">A</ts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_695" n="HIAT:w" s="T169">mašinaʔi</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_698" n="HIAT:w" s="T170">mĭllieʔi</ts>
                  <nts id="Seg_699" n="HIAT:ip">,</nts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_702" n="HIAT:w" s="T171">dĭ</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_705" n="HIAT:w" s="T172">dĭbər</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_708" n="HIAT:w" s="T173">ej</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_711" n="HIAT:w" s="T174">amlia</ts>
                  <nts id="Seg_712" n="HIAT:ip">.</nts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T180" id="Seg_715" n="HIAT:u" s="T175">
                  <ts e="T176" id="Seg_717" n="HIAT:w" s="T175">A</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_720" n="HIAT:w" s="T176">üdʼin</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_722" n="HIAT:ip">(</nts>
                  <ts e="T178" id="Seg_724" n="HIAT:w" s="T177">ka-</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_727" n="HIAT:w" s="T178">kan-</ts>
                  <nts id="Seg_728" n="HIAT:ip">)</nts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_731" n="HIAT:w" s="T179">kaləj</ts>
                  <nts id="Seg_732" n="HIAT:ip">.</nts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_735" n="HIAT:u" s="T180">
                  <nts id="Seg_736" n="HIAT:ip">(</nts>
                  <nts id="Seg_737" n="HIAT:ip">(</nts>
                  <ats e="T181" id="Seg_738" n="HIAT:non-pho" s="T180">BRK</ats>
                  <nts id="Seg_739" n="HIAT:ip">)</nts>
                  <nts id="Seg_740" n="HIAT:ip">)</nts>
                  <nts id="Seg_741" n="HIAT:ip">.</nts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_744" n="HIAT:u" s="T181">
                  <nts id="Seg_745" n="HIAT:ip">(</nts>
                  <nts id="Seg_746" n="HIAT:ip">(</nts>
                  <ats e="T182" id="Seg_747" n="HIAT:non-pho" s="T181">BRK</ats>
                  <nts id="Seg_748" n="HIAT:ip">)</nts>
                  <nts id="Seg_749" n="HIAT:ip">)</nts>
                  <nts id="Seg_750" n="HIAT:ip">.</nts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_753" n="HIAT:u" s="T182">
                  <nts id="Seg_754" n="HIAT:ip">"</nts>
                  <ts e="T183" id="Seg_756" n="HIAT:w" s="T182">Măn</ts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_759" n="HIAT:w" s="T183">üjüzi</ts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_762" n="HIAT:w" s="T184">lutʼšə</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_765" n="HIAT:w" s="T185">kalam</ts>
                  <nts id="Seg_766" n="HIAT:ip">.</nts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_769" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_771" n="HIAT:w" s="T186">Aʔtʼi</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_774" n="HIAT:w" s="T187">прямой</ts>
                  <nts id="Seg_775" n="HIAT:ip">.</nts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_778" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_780" n="HIAT:w" s="T188">A</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_783" n="HIAT:w" s="T189">mašinaʔizi</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_786" n="HIAT:w" s="T190">ugandə</ts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_789" n="HIAT:w" s="T191">kuŋgəŋ</ts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_792" n="HIAT:w" s="T192">kanzittə</ts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_795" n="HIAT:w" s="T193">da</ts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_798" n="HIAT:w" s="T194">bar</ts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_801" n="HIAT:w" s="T195">sădərlaʔbə</ts>
                  <nts id="Seg_802" n="HIAT:ip">"</nts>
                  <nts id="Seg_803" n="HIAT:ip">.</nts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T197" id="Seg_806" n="HIAT:u" s="T196">
                  <nts id="Seg_807" n="HIAT:ip">(</nts>
                  <nts id="Seg_808" n="HIAT:ip">(</nts>
                  <ats e="T197" id="Seg_809" n="HIAT:non-pho" s="T196">BRK</ats>
                  <nts id="Seg_810" n="HIAT:ip">)</nts>
                  <nts id="Seg_811" n="HIAT:ip">)</nts>
                  <nts id="Seg_812" n="HIAT:ip">.</nts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T200" id="Seg_815" n="HIAT:u" s="T197">
                  <ts e="T198" id="Seg_817" n="HIAT:w" s="T197">Girgit</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_820" n="HIAT:w" s="T198">судьба</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_823" n="HIAT:w" s="T199">nuzaŋgən</ts>
                  <nts id="Seg_824" n="HIAT:ip">.</nts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_827" n="HIAT:u" s="T200">
                  <ts e="T201" id="Seg_829" n="HIAT:w" s="T200">Šində</ts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_832" n="HIAT:w" s="T201">išo</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_835" n="HIAT:w" s="T202">molia</ts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_838" n="HIAT:w" s="T203">dʼăbaktərzittə</ts>
                  <nts id="Seg_839" n="HIAT:ip">,</nts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_842" n="HIAT:w" s="T204">ali</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_844" n="HIAT:ip">(</nts>
                  <ts e="T206" id="Seg_846" n="HIAT:w" s="T205">ej</ts>
                  <nts id="Seg_847" n="HIAT:ip">)</nts>
                  <nts id="Seg_848" n="HIAT:ip">.</nts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_851" n="HIAT:u" s="T206">
                  <nts id="Seg_852" n="HIAT:ip">(</nts>
                  <nts id="Seg_853" n="HIAT:ip">(</nts>
                  <ats e="T207" id="Seg_854" n="HIAT:non-pho" s="T206">BRK</ats>
                  <nts id="Seg_855" n="HIAT:ip">)</nts>
                  <nts id="Seg_856" n="HIAT:ip">)</nts>
                  <nts id="Seg_857" n="HIAT:ip">.</nts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_860" n="HIAT:u" s="T207">
                  <nts id="Seg_861" n="HIAT:ip">(</nts>
                  <ts e="T208" id="Seg_863" n="HIAT:w" s="T207">Nʼiʔnen</ts>
                  <nts id="Seg_864" n="HIAT:ip">)</nts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_867" n="HIAT:w" s="T208">Abalakovan</ts>
                  <nts id="Seg_868" n="HIAT:ip">,</nts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_871" n="HIAT:w" s="T209">esseŋdə</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_874" n="HIAT:w" s="T210">možna</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_877" n="HIAT:w" s="T211">kuzittə</ts>
                  <nts id="Seg_878" n="HIAT:ip">.</nts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T216" id="Seg_881" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_883" n="HIAT:w" s="T212">Sĭre</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_886" n="HIAT:w" s="T213">ulut</ts>
                  <nts id="Seg_887" n="HIAT:ip">,</nts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_890" n="HIAT:w" s="T214">simat</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_893" n="HIAT:w" s="T215">sagər</ts>
                  <nts id="Seg_894" n="HIAT:ip">.</nts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T221" id="Seg_897" n="HIAT:u" s="T216">
                  <ts e="T217" id="Seg_899" n="HIAT:w" s="T216">Не</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_902" n="HIAT:w" s="T217">только</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_905" n="HIAT:w" s="T218">kazak</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_908" n="HIAT:w" s="T219">kem</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_911" n="HIAT:w" s="T220">ige</ts>
                  <nts id="Seg_912" n="HIAT:ip">.</nts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_915" n="HIAT:u" s="T221">
                  <ts e="T222" id="Seg_917" n="HIAT:w" s="T221">I</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_920" n="HIAT:w" s="T222">kem</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_923" n="HIAT:w" s="T223">ige</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_926" n="HIAT:w" s="T224">nuzaŋ</ts>
                  <nts id="Seg_927" n="HIAT:ip">.</nts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_930" n="HIAT:u" s="T225">
                  <nts id="Seg_931" n="HIAT:ip">(</nts>
                  <nts id="Seg_932" n="HIAT:ip">(</nts>
                  <ats e="T226" id="Seg_933" n="HIAT:non-pho" s="T225">BRK</ats>
                  <nts id="Seg_934" n="HIAT:ip">)</nts>
                  <nts id="Seg_935" n="HIAT:ip">)</nts>
                  <nts id="Seg_936" n="HIAT:ip">.</nts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T230" id="Seg_939" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_941" n="HIAT:w" s="T226">Girgit</ts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_944" n="HIAT:w" s="T227">bar</ts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_947" n="HIAT:w" s="T228">özerleʔbəʔi</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_949" n="HIAT:ip">(</nts>
                  <ts e="T230" id="Seg_951" n="HIAT:w" s="T229">li</ts>
                  <nts id="Seg_952" n="HIAT:ip">)</nts>
                  <nts id="Seg_953" n="HIAT:ip">.</nts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T235" id="Seg_956" n="HIAT:u" s="T230">
                  <ts e="T231" id="Seg_958" n="HIAT:w" s="T230">Kambiʔi</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_961" n="HIAT:w" s="T231">togonorzittə</ts>
                  <nts id="Seg_962" n="HIAT:ip">,</nts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_965" n="HIAT:w" s="T232">dĭbər-döbər</ts>
                  <nts id="Seg_966" n="HIAT:ip">,</nts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_969" n="HIAT:w" s="T233">vot</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_972" n="HIAT:w" s="T234">Проня</ts>
                  <nts id="Seg_973" n="HIAT:ip">.</nts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_976" n="HIAT:u" s="T235">
                  <ts e="T236" id="Seg_978" n="HIAT:w" s="T235">Анджигатов</ts>
                  <nts id="Seg_979" n="HIAT:ip">.</nts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_982" n="HIAT:u" s="T236">
                  <ts e="T236.tx-PKZ.1" id="Seg_984" n="HIAT:w" s="T236">Председатель</ts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_987" n="HIAT:w" s="T236.tx-PKZ.1">сельсовета</ts>
                  <nts id="Seg_988" n="HIAT:ip">.</nts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_991" n="HIAT:u" s="T238">
                  <ts e="T239" id="Seg_993" n="HIAT:w" s="T238">A</ts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_996" n="HIAT:w" s="T239">bostə</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_999" n="HIAT:w" s="T240">šĭket</ts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_1002" n="HIAT:w" s="T241">ej</ts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_1005" n="HIAT:w" s="T242">tĭmnet</ts>
                  <nts id="Seg_1006" n="HIAT:ip">.</nts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_1009" n="HIAT:u" s="T243">
                  <nts id="Seg_1010" n="HIAT:ip">(</nts>
                  <nts id="Seg_1011" n="HIAT:ip">(</nts>
                  <ats e="T244" id="Seg_1012" n="HIAT:non-pho" s="T243">BRK</ats>
                  <nts id="Seg_1013" n="HIAT:ip">)</nts>
                  <nts id="Seg_1014" n="HIAT:ip">)</nts>
                  <nts id="Seg_1015" n="HIAT:ip">.</nts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_1018" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_1020" n="HIAT:w" s="T244">Tolʼkă</ts>
                  <nts id="Seg_1021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_1023" n="HIAT:w" s="T245">dĭ</ts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_1026" n="HIAT:w" s="T246">nuzaŋ</ts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_1029" n="HIAT:w" s="T247">šĭket</ts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_1032" n="HIAT:w" s="T248">onʼiʔ</ts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_1035" n="HIAT:w" s="T249">nüke</ts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_1038" n="HIAT:w" s="T250">tĭmnet</ts>
                  <nts id="Seg_1039" n="HIAT:ip">.</nts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T258" id="Seg_1042" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_1044" n="HIAT:w" s="T251">A</ts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_1047" n="HIAT:w" s="T252">daška</ts>
                  <nts id="Seg_1048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_1050" n="HIAT:w" s="T253">šindidə</ts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_1053" n="HIAT:w" s="T254">ej</ts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_1056" n="HIAT:w" s="T255">tĭmne</ts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_1059" n="HIAT:w" s="T256">dĭ</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_1062" n="HIAT:w" s="T257">šĭkem</ts>
                  <nts id="Seg_1063" n="HIAT:ip">.</nts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T259" id="Seg_1066" n="HIAT:u" s="T258">
                  <nts id="Seg_1067" n="HIAT:ip">(</nts>
                  <nts id="Seg_1068" n="HIAT:ip">(</nts>
                  <ats e="T259" id="Seg_1069" n="HIAT:non-pho" s="T258">BRK</ats>
                  <nts id="Seg_1070" n="HIAT:ip">)</nts>
                  <nts id="Seg_1071" n="HIAT:ip">)</nts>
                  <nts id="Seg_1072" n="HIAT:ip">.</nts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T262" id="Seg_1075" n="HIAT:u" s="T259">
                  <ts e="T260" id="Seg_1077" n="HIAT:w" s="T259">Tăn</ts>
                  <nts id="Seg_1078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_1080" n="HIAT:w" s="T260">măna</ts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1083" n="HIAT:w" s="T261">šaːmbial</ts>
                  <nts id="Seg_1084" n="HIAT:ip">.</nts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T265" id="Seg_1087" n="HIAT:u" s="T262">
                  <ts e="T263" id="Seg_1089" n="HIAT:w" s="T262">Tăn</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1092" n="HIAT:w" s="T263">măna</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1095" n="HIAT:w" s="T264">mʼegluʔpiel</ts>
                  <nts id="Seg_1096" n="HIAT:ip">.</nts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T276" id="Seg_1099" n="HIAT:u" s="T265">
                  <ts e="T266" id="Seg_1101" n="HIAT:w" s="T265">Dĭgəttə</ts>
                  <nts id="Seg_1102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1104" n="HIAT:w" s="T266">šaʔlambiam</ts>
                  <nts id="Seg_1105" n="HIAT:ip">,</nts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1108" n="HIAT:w" s="T267">a</ts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1111" n="HIAT:w" s="T268">măn</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1114" n="HIAT:w" s="T269">tănan</ts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1117" n="HIAT:w" s="T270">măndərbiam</ts>
                  <nts id="Seg_1118" n="HIAT:ip">,</nts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1121" n="HIAT:w" s="T271">măndərbiam</ts>
                  <nts id="Seg_1122" n="HIAT:ip">,</nts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1125" n="HIAT:w" s="T272">ej</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1127" n="HIAT:ip">(</nts>
                  <ts e="T274" id="Seg_1129" n="HIAT:w" s="T273">mo-</ts>
                  <nts id="Seg_1130" n="HIAT:ip">)</nts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1133" n="HIAT:w" s="T274">moliam</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1136" n="HIAT:w" s="T275">măndərzittə</ts>
                  <nts id="Seg_1137" n="HIAT:ip">.</nts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T280" id="Seg_1140" n="HIAT:u" s="T276">
                  <ts e="T277" id="Seg_1142" n="HIAT:w" s="T276">Ej</ts>
                  <nts id="Seg_1143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1145" n="HIAT:w" s="T277">jakše</ts>
                  <nts id="Seg_1146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1148" n="HIAT:w" s="T278">kuza</ts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1151" n="HIAT:w" s="T279">amnobi</ts>
                  <nts id="Seg_1152" n="HIAT:ip">.</nts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T283" id="Seg_1155" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_1157" n="HIAT:w" s="T280">Dĭm</ts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1160" n="HIAT:w" s="T281">numəjleʔpiʔi</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1163" n="HIAT:w" s="T282">Tartʼəberdʼə</ts>
                  <nts id="Seg_1164" n="HIAT:ip">.</nts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T287" id="Seg_1167" n="HIAT:u" s="T283">
                  <ts e="T284" id="Seg_1169" n="HIAT:w" s="T283">Dĭ</ts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1172" n="HIAT:w" s="T284">bar</ts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1175" n="HIAT:w" s="T285">šojmü</ts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1178" n="HIAT:w" s="T286">dʼaʔpi</ts>
                  <nts id="Seg_1179" n="HIAT:ip">.</nts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T289" id="Seg_1182" n="HIAT:u" s="T287">
                  <ts e="T288" id="Seg_1184" n="HIAT:w" s="T287">Üjüt</ts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1187" n="HIAT:w" s="T288">ĭzemnie</ts>
                  <nts id="Seg_1188" n="HIAT:ip">.</nts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T292" id="Seg_1191" n="HIAT:u" s="T289">
                  <ts e="T290" id="Seg_1193" n="HIAT:w" s="T289">Kanzittə</ts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1196" n="HIAT:w" s="T290">ej</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1199" n="HIAT:w" s="T291">molia</ts>
                  <nts id="Seg_1200" n="HIAT:ip">.</nts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T295" id="Seg_1203" n="HIAT:u" s="T292">
                  <ts e="T293" id="Seg_1205" n="HIAT:w" s="T292">Dĭgəttə</ts>
                  <nts id="Seg_1206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1208" n="HIAT:w" s="T293">kambi</ts>
                  <nts id="Seg_1209" n="HIAT:ip">,</nts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1212" n="HIAT:w" s="T294">kambi</ts>
                  <nts id="Seg_1213" n="HIAT:ip">.</nts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_1216" n="HIAT:u" s="T295">
                  <ts e="T296" id="Seg_1218" n="HIAT:w" s="T295">Urgaːba</ts>
                  <nts id="Seg_1219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1221" n="HIAT:w" s="T296">šonəga:</ts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1224" n="HIAT:w" s="T297">Iʔ</ts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1227" n="HIAT:w" s="T298">măna</ts>
                  <nts id="Seg_1228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1229" n="HIAT:ip">(</nts>
                  <ts e="T300" id="Seg_1231" n="HIAT:w" s="T299">bostəzi</ts>
                  <nts id="Seg_1232" n="HIAT:ip">)</nts>
                  <nts id="Seg_1233" n="HIAT:ip">!</nts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T307" id="Seg_1236" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_1238" n="HIAT:w" s="T300">No</ts>
                  <nts id="Seg_1239" n="HIAT:ip">,</nts>
                  <nts id="Seg_1240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1242" n="HIAT:w" s="T301">tolʼkă</ts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1245" n="HIAT:w" s="T302">sagər</ts>
                  <nts id="Seg_1246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1247" n="HIAT:ip">(</nts>
                  <ts e="T304" id="Seg_1249" n="HIAT:w" s="T303">mu-</ts>
                  <nts id="Seg_1250" n="HIAT:ip">)</nts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1253" n="HIAT:w" s="T304">băranə</ts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1255" n="HIAT:ip">(</nts>
                  <ts e="T306" id="Seg_1257" n="HIAT:w" s="T305">amdəldə</ts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1260" n="HIAT:w" s="T306">măna</ts>
                  <nts id="Seg_1261" n="HIAT:ip">)</nts>
                  <nts id="Seg_1262" n="HIAT:ip">.</nts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T312" id="Seg_1265" n="HIAT:u" s="T307">
                  <ts e="T308" id="Seg_1267" n="HIAT:w" s="T307">Dĭ</ts>
                  <nts id="Seg_1268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1270" n="HIAT:w" s="T308">dĭm</ts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1273" n="HIAT:w" s="T309">amnolbi</ts>
                  <nts id="Seg_1274" n="HIAT:ip">,</nts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1277" n="HIAT:w" s="T310">bazo</ts>
                  <nts id="Seg_1278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1280" n="HIAT:w" s="T311">kandəga</ts>
                  <nts id="Seg_1281" n="HIAT:ip">.</nts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T316" id="Seg_1284" n="HIAT:u" s="T312">
                  <ts e="T313" id="Seg_1286" n="HIAT:w" s="T312">Kandəga</ts>
                  <nts id="Seg_1287" n="HIAT:ip">,</nts>
                  <nts id="Seg_1288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1290" n="HIAT:w" s="T313">kandəga</ts>
                  <nts id="Seg_1291" n="HIAT:ip">,</nts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1294" n="HIAT:w" s="T314">dĭgəttə</ts>
                  <nts id="Seg_1295" n="HIAT:ip">…</nts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T321" id="Seg_1298" n="HIAT:u" s="T316">
                  <ts e="T317" id="Seg_1300" n="HIAT:w" s="T316">Dĭgəttə</ts>
                  <nts id="Seg_1301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1303" n="HIAT:w" s="T317">urgo</ts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1306" n="HIAT:w" s="T318">men</ts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1309" n="HIAT:w" s="T319">dʼijegə</ts>
                  <nts id="Seg_1310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1312" n="HIAT:w" s="T320">šobi</ts>
                  <nts id="Seg_1313" n="HIAT:ip">.</nts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T323" id="Seg_1316" n="HIAT:u" s="T321">
                  <ts e="T322" id="Seg_1318" n="HIAT:w" s="T321">Iʔ</ts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1321" n="HIAT:w" s="T322">măna</ts>
                  <nts id="Seg_1322" n="HIAT:ip">.</nts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T328" id="Seg_1325" n="HIAT:u" s="T323">
                  <ts e="T324" id="Seg_1327" n="HIAT:w" s="T323">Tolʼkă</ts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1330" n="HIAT:w" s="T324">sagər</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1333" n="HIAT:w" s="T325">măna</ts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1336" n="HIAT:w" s="T326">băranə</ts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1339" n="HIAT:w" s="T327">amdəldə</ts>
                  <nts id="Seg_1340" n="HIAT:ip">!</nts>
                  <nts id="Seg_1341" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T330" id="Seg_1343" n="HIAT:u" s="T328">
                  <ts e="T329" id="Seg_1345" n="HIAT:w" s="T328">Dĭ</ts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1348" n="HIAT:w" s="T329">amnoldəbi</ts>
                  <nts id="Seg_1349" n="HIAT:ip">.</nts>
                  <nts id="Seg_1350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T336" id="Seg_1352" n="HIAT:u" s="T330">
                  <ts e="T331" id="Seg_1354" n="HIAT:w" s="T330">Kambi</ts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1357" n="HIAT:w" s="T331">bazoʔ</ts>
                  <nts id="Seg_1358" n="HIAT:ip">,</nts>
                  <nts id="Seg_1359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1361" n="HIAT:w" s="T332">šonəga</ts>
                  <nts id="Seg_1362" n="HIAT:ip">,</nts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1365" n="HIAT:w" s="T333">šonəga</ts>
                  <nts id="Seg_1366" n="HIAT:ip">,</nts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1368" n="HIAT:ip">(</nts>
                  <ts e="T335" id="Seg_1370" n="HIAT:w" s="T334">šo-</ts>
                  <nts id="Seg_1371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1373" n="HIAT:w" s="T335">šo-</ts>
                  <nts id="Seg_1374" n="HIAT:ip">)</nts>
                  <nts id="Seg_1375" n="HIAT:ip">.</nts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T339" id="Seg_1378" n="HIAT:u" s="T336">
                  <nts id="Seg_1379" n="HIAT:ip">(</nts>
                  <ts e="T337" id="Seg_1381" n="HIAT:w" s="T336">Li-</ts>
                  <nts id="Seg_1382" n="HIAT:ip">)</nts>
                  <nts id="Seg_1383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1385" n="HIAT:w" s="T337">Lisa</ts>
                  <nts id="Seg_1386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1388" n="HIAT:w" s="T338">šolaʔbə</ts>
                  <nts id="Seg_1389" n="HIAT:ip">.</nts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T341" id="Seg_1392" n="HIAT:u" s="T339">
                  <ts e="T340" id="Seg_1394" n="HIAT:w" s="T339">Iʔ</ts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1397" n="HIAT:w" s="T340">măna</ts>
                  <nts id="Seg_1398" n="HIAT:ip">!</nts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T344" id="Seg_1401" n="HIAT:u" s="T341">
                  <ts e="T342" id="Seg_1403" n="HIAT:w" s="T341">Dĭ</ts>
                  <nts id="Seg_1404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1406" n="HIAT:w" s="T342">dĭm</ts>
                  <nts id="Seg_1407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1409" n="HIAT:w" s="T343">ibi</ts>
                  <nts id="Seg_1410" n="HIAT:ip">.</nts>
                  <nts id="Seg_1411" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T350" id="Seg_1413" n="HIAT:u" s="T344">
                  <ts e="T345" id="Seg_1415" n="HIAT:w" s="T344">Sagər</ts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1418" n="HIAT:w" s="T345">băranə</ts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1421" n="HIAT:w" s="T346">amnolbi</ts>
                  <nts id="Seg_1422" n="HIAT:ip">,</nts>
                  <nts id="Seg_1423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1425" n="HIAT:w" s="T347">dĭgəttə</ts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1428" n="HIAT:w" s="T348">bazo</ts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1431" n="HIAT:w" s="T349">kandəga</ts>
                  <nts id="Seg_1432" n="HIAT:ip">.</nts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T354" id="Seg_1435" n="HIAT:u" s="T350">
                  <ts e="T351" id="Seg_1437" n="HIAT:w" s="T350">Bü</ts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1440" n="HIAT:w" s="T351">šonəga:</ts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1443" n="HIAT:w" s="T352">Iʔ</ts>
                  <nts id="Seg_1444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1446" n="HIAT:w" s="T353">măna</ts>
                  <nts id="Seg_1447" n="HIAT:ip">!</nts>
                  <nts id="Seg_1448" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T359" id="Seg_1450" n="HIAT:u" s="T354">
                  <ts e="T355" id="Seg_1452" n="HIAT:w" s="T354">Dĭgəttə</ts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1455" n="HIAT:w" s="T355">dĭ</ts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1458" n="HIAT:w" s="T356">bü</ts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1461" n="HIAT:w" s="T357">kămnəbi</ts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1464" n="HIAT:w" s="T358">dĭbər</ts>
                  <nts id="Seg_1465" n="HIAT:ip">.</nts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T360" id="Seg_1468" n="HIAT:u" s="T359">
                  <ts e="T360" id="Seg_1470" n="HIAT:w" s="T359">Šobi</ts>
                  <nts id="Seg_1471" n="HIAT:ip">.</nts>
                  <nts id="Seg_1472" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T368" id="Seg_1474" n="HIAT:u" s="T360">
                  <ts e="T361" id="Seg_1476" n="HIAT:w" s="T360">Dĭn</ts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1479" n="HIAT:w" s="T361">bar</ts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1482" n="HIAT:w" s="T362">turaʔi</ts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1485" n="HIAT:w" s="T363">nugaʔi</ts>
                  <nts id="Seg_1486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1488" n="HIAT:w" s="T364">i</ts>
                  <nts id="Seg_1489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1491" n="HIAT:w" s="T365">bü</ts>
                  <nts id="Seg_1492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1494" n="HIAT:w" s="T366">ugaːndə</ts>
                  <nts id="Seg_1495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1497" n="HIAT:w" s="T367">urgo</ts>
                  <nts id="Seg_1498" n="HIAT:ip">.</nts>
                  <nts id="Seg_1499" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T371" id="Seg_1501" n="HIAT:u" s="T368">
                  <ts e="T369" id="Seg_1503" n="HIAT:w" s="T368">Dĭ</ts>
                  <nts id="Seg_1504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1506" n="HIAT:w" s="T369">šöjmü</ts>
                  <nts id="Seg_1507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1509" n="HIAT:w" s="T370">sarbi</ts>
                  <nts id="Seg_1510" n="HIAT:ip">.</nts>
                  <nts id="Seg_1511" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T374" id="Seg_1513" n="HIAT:u" s="T371">
                  <ts e="T372" id="Seg_1515" n="HIAT:w" s="T371">Šü</ts>
                  <nts id="Seg_1516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1518" n="HIAT:w" s="T372">embi</ts>
                  <nts id="Seg_1519" n="HIAT:ip">,</nts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1522" n="HIAT:w" s="T373">amnolaʔbə</ts>
                  <nts id="Seg_1523" n="HIAT:ip">.</nts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T380" id="Seg_1526" n="HIAT:u" s="T374">
                  <ts e="T375" id="Seg_1528" n="HIAT:w" s="T374">Dĭm</ts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1531" n="HIAT:w" s="T375">kuluʔpi</ts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1534" n="HIAT:w" s="T376">koŋ:</ts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1537" n="HIAT:w" s="T377">Šində</ts>
                  <nts id="Seg_1538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1540" n="HIAT:w" s="T378">dĭn</ts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1543" n="HIAT:w" s="T379">amnolaʔbə</ts>
                  <nts id="Seg_1544" n="HIAT:ip">?</nts>
                  <nts id="Seg_1545" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T384" id="Seg_1547" n="HIAT:u" s="T380">
                  <ts e="T381" id="Seg_1549" n="HIAT:w" s="T380">Măn</ts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1551" n="HIAT:ip">(</nts>
                  <ts e="T382" id="Seg_1553" n="HIAT:w" s="T381">noʔ-</ts>
                  <nts id="Seg_1554" n="HIAT:ip">)</nts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1557" n="HIAT:w" s="T382">noʔdə</ts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1560" n="HIAT:w" s="T383">tonəʔlaʔbə</ts>
                  <nts id="Seg_1561" n="HIAT:ip">.</nts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T386" id="Seg_1564" n="HIAT:u" s="T384">
                  <ts e="T385" id="Seg_1566" n="HIAT:w" s="T384">Šöjmüt</ts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1569" n="HIAT:w" s="T385">amnaʔbə</ts>
                  <nts id="Seg_1570" n="HIAT:ip">.</nts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T393" id="Seg_1573" n="HIAT:u" s="T386">
                  <ts e="T387" id="Seg_1575" n="HIAT:w" s="T386">Dĭn</ts>
                  <nts id="Seg_1576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1578" n="HIAT:w" s="T387">üdʼügen</ts>
                  <nts id="Seg_1579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1581" n="HIAT:w" s="T388">amnoləj</ts>
                  <nts id="Seg_1582" n="HIAT:ip">,</nts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1585" n="HIAT:w" s="T389">dĭgəttə</ts>
                  <nts id="Seg_1586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1588" n="HIAT:w" s="T390">bazoʔ</ts>
                  <nts id="Seg_1589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1591" n="HIAT:w" s="T391">bar</ts>
                  <nts id="Seg_1592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1594" n="HIAT:w" s="T392">tonəʔləj</ts>
                  <nts id="Seg_1595" n="HIAT:ip">.</nts>
                  <nts id="Seg_1596" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T394" id="Seg_1598" n="HIAT:u" s="T393">
                  <ts e="T394" id="Seg_1600" n="HIAT:w" s="T393">Kanaʔ</ts>
                  <nts id="Seg_1601" n="HIAT:ip">!</nts>
                  <nts id="Seg_1602" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T396" id="Seg_1604" n="HIAT:u" s="T394">
                  <ts e="T395" id="Seg_1606" n="HIAT:w" s="T394">Nʼibə</ts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1609" n="HIAT:w" s="T395">öʔlubi</ts>
                  <nts id="Seg_1610" n="HIAT:ip">.</nts>
                  <nts id="Seg_1611" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T399" id="Seg_1613" n="HIAT:u" s="T396">
                  <ts e="T397" id="Seg_1615" n="HIAT:w" s="T396">Ĭmbi</ts>
                  <nts id="Seg_1616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1618" n="HIAT:w" s="T397">dĭʔnə</ts>
                  <nts id="Seg_1619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1621" n="HIAT:w" s="T398">kereʔ</ts>
                  <nts id="Seg_1622" n="HIAT:ip">?</nts>
                  <nts id="Seg_1623" n="HIAT:ip">"</nts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T406" id="Seg_1626" n="HIAT:u" s="T399">
                  <ts e="T400" id="Seg_1628" n="HIAT:w" s="T399">Dĭ</ts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1631" n="HIAT:w" s="T400">šobi</ts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1633" n="HIAT:ip">(</nts>
                  <ts e="T402" id="Seg_1635" n="HIAT:w" s="T401">surarla-</ts>
                  <nts id="Seg_1636" n="HIAT:ip">)</nts>
                  <nts id="Seg_1637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1639" n="HIAT:w" s="T402">surarlaʔbə:</ts>
                  <nts id="Seg_1640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1642" n="HIAT:w" s="T403">Ĭmbi</ts>
                  <nts id="Seg_1643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1645" n="HIAT:w" s="T404">tănan</ts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1648" n="HIAT:w" s="T405">kereʔ</ts>
                  <nts id="Seg_1649" n="HIAT:ip">?</nts>
                  <nts id="Seg_1650" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T410" id="Seg_1652" n="HIAT:u" s="T406">
                  <ts e="T407" id="Seg_1654" n="HIAT:w" s="T406">Dĭn</ts>
                  <nts id="Seg_1655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1657" n="HIAT:w" s="T407">koŋdə</ts>
                  <nts id="Seg_1658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1660" n="HIAT:w" s="T408">koʔbdobə</ts>
                  <nts id="Seg_1661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1663" n="HIAT:w" s="T409">ilim</ts>
                  <nts id="Seg_1664" n="HIAT:ip">"</nts>
                  <nts id="Seg_1665" n="HIAT:ip">.</nts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T411" id="Seg_1668" n="HIAT:u" s="T410">
                  <nts id="Seg_1669" n="HIAT:ip">(</nts>
                  <nts id="Seg_1670" n="HIAT:ip">(</nts>
                  <ats e="T411" id="Seg_1671" n="HIAT:non-pho" s="T410">BRK</ats>
                  <nts id="Seg_1672" n="HIAT:ip">)</nts>
                  <nts id="Seg_1673" n="HIAT:ip">)</nts>
                  <nts id="Seg_1674" n="HIAT:ip">.</nts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T422" id="Seg_1677" n="HIAT:u" s="T411">
                  <ts e="T412" id="Seg_1679" n="HIAT:w" s="T411">Dĭ</ts>
                  <nts id="Seg_1680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1682" n="HIAT:w" s="T412">nʼi</ts>
                  <nts id="Seg_1683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1685" n="HIAT:w" s="T413">parluʔbi</ts>
                  <nts id="Seg_1686" n="HIAT:ip">,</nts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1689" n="HIAT:w" s="T414">măndə:</ts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1692" n="HIAT:w" s="T415">Dĭ</ts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1695" n="HIAT:w" s="T416">măndə:</ts>
                  <nts id="Seg_1696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1697" n="HIAT:ip">(</nts>
                  <ts e="T418" id="Seg_1699" n="HIAT:w" s="T417">pušaj</ts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1702" n="HIAT:w" s="T418">dĭm=</ts>
                  <nts id="Seg_1703" n="HIAT:ip">)</nts>
                  <nts id="Seg_1704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1706" n="HIAT:w" s="T419">koŋdə</ts>
                  <nts id="Seg_1707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1709" n="HIAT:w" s="T420">koʔbdobə</ts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1712" n="HIAT:w" s="T421">ilim</ts>
                  <nts id="Seg_1713" n="HIAT:ip">!</nts>
                  <nts id="Seg_1714" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T424" id="Seg_1716" n="HIAT:u" s="T422">
                  <ts e="T423" id="Seg_1718" n="HIAT:w" s="T422">Pušaj</ts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1720" n="HIAT:ip">(</nts>
                  <ts e="T424" id="Seg_1722" n="HIAT:w" s="T423">kangəʔjə</ts>
                  <nts id="Seg_1723" n="HIAT:ip">)</nts>
                  <nts id="Seg_1724" n="HIAT:ip">.</nts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T428" id="Seg_1727" n="HIAT:u" s="T424">
                  <ts e="T424.tx-PKZ.1" id="Seg_1729" n="HIAT:w" s="T424">A</ts>
                  <nts id="Seg_1730" n="HIAT:ip">_</nts>
                  <ts e="T425" id="Seg_1732" n="HIAT:w" s="T424.tx-PKZ.1">to</ts>
                  <nts id="Seg_1733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1735" n="HIAT:w" s="T425">ej</ts>
                  <nts id="Seg_1736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1738" n="HIAT:w" s="T426">jakše</ts>
                  <nts id="Seg_1739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1741" n="HIAT:w" s="T427">moləj</ts>
                  <nts id="Seg_1742" n="HIAT:ip">.</nts>
                  <nts id="Seg_1743" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T431" id="Seg_1745" n="HIAT:u" s="T428">
                  <nts id="Seg_1746" n="HIAT:ip">(</nts>
                  <ts e="T429" id="Seg_1748" n="HIAT:w" s="T428">Em=</ts>
                  <nts id="Seg_1749" n="HIAT:ip">)</nts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1752" n="HIAT:w" s="T429">Ej</ts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1755" n="HIAT:w" s="T430">mĭlem</ts>
                  <nts id="Seg_1756" n="HIAT:ip">!</nts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T440" id="Seg_1759" n="HIAT:u" s="T431">
                  <ts e="T432" id="Seg_1761" n="HIAT:w" s="T431">Dĭ</ts>
                  <nts id="Seg_1762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1764" n="HIAT:w" s="T432">šobi</ts>
                  <nts id="Seg_1765" n="HIAT:ip">,</nts>
                  <nts id="Seg_1766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1767" n="HIAT:ip">(</nts>
                  <ts e="T434" id="Seg_1769" n="HIAT:w" s="T433">măn-</ts>
                  <nts id="Seg_1770" n="HIAT:ip">)</nts>
                  <nts id="Seg_1771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1773" n="HIAT:w" s="T434">mămbi</ts>
                  <nts id="Seg_1774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1776" n="HIAT:w" s="T435">dăre:</ts>
                  <nts id="Seg_1777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1779" n="HIAT:w" s="T436">Koŋ</ts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1782" n="HIAT:w" s="T437">ej</ts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1785" n="HIAT:w" s="T438">mĭliet</ts>
                  <nts id="Seg_1786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1788" n="HIAT:w" s="T439">koʔbdobə</ts>
                  <nts id="Seg_1789" n="HIAT:ip">.</nts>
                  <nts id="Seg_1790" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T447" id="Seg_1792" n="HIAT:u" s="T440">
                  <ts e="T441" id="Seg_1794" n="HIAT:w" s="T440">Ej</ts>
                  <nts id="Seg_1795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1797" n="HIAT:w" s="T441">mĭləj</ts>
                  <nts id="Seg_1798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1800" n="HIAT:w" s="T442">jakšeŋ</ts>
                  <nts id="Seg_1801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1803" n="HIAT:w" s="T443">dăk</ts>
                  <nts id="Seg_1804" n="HIAT:ip">,</nts>
                  <nts id="Seg_1805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1807" n="HIAT:w" s="T444">ej</ts>
                  <nts id="Seg_1808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1810" n="HIAT:w" s="T445">jakše</ts>
                  <nts id="Seg_1811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1813" n="HIAT:w" s="T446">mĭləj</ts>
                  <nts id="Seg_1814" n="HIAT:ip">.</nts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T448" id="Seg_1817" n="HIAT:u" s="T447">
                  <nts id="Seg_1818" n="HIAT:ip">(</nts>
                  <nts id="Seg_1819" n="HIAT:ip">(</nts>
                  <ats e="T448" id="Seg_1820" n="HIAT:non-pho" s="T447">BRK</ats>
                  <nts id="Seg_1821" n="HIAT:ip">)</nts>
                  <nts id="Seg_1822" n="HIAT:ip">)</nts>
                  <nts id="Seg_1823" n="HIAT:ip">.</nts>
                  <nts id="Seg_1824" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T451" id="Seg_1826" n="HIAT:u" s="T448">
                  <nts id="Seg_1827" n="HIAT:ip">"</nts>
                  <ts e="T449" id="Seg_1829" n="HIAT:w" s="T448">Öʔleʔ</ts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1832" n="HIAT:w" s="T449">дикий</ts>
                  <nts id="Seg_1833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1835" n="HIAT:w" s="T450">ineʔi</ts>
                  <nts id="Seg_1836" n="HIAT:ip">.</nts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T458" id="Seg_1839" n="HIAT:u" s="T451">
                  <ts e="T452" id="Seg_1841" n="HIAT:w" s="T451">Dĭzeŋ</ts>
                  <nts id="Seg_1842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1844" n="HIAT:w" s="T452">bar</ts>
                  <nts id="Seg_1845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1847" n="HIAT:w" s="T453">dĭn</ts>
                  <nts id="Seg_1848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1850" n="HIAT:w" s="T454">šöjmündə</ts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1853" n="HIAT:w" s="T455">sajnʼiʔluʔləʔjə</ts>
                  <nts id="Seg_1854" n="HIAT:ip">,</nts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1857" n="HIAT:w" s="T456">dĭgəttə</ts>
                  <nts id="Seg_1858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1072" id="Seg_1860" n="HIAT:w" s="T457">kalla</ts>
                  <nts id="Seg_1861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1863" n="HIAT:w" s="T1072">dʼürləj</ts>
                  <nts id="Seg_1864" n="HIAT:ip">.</nts>
                  <nts id="Seg_1865" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T464" id="Seg_1867" n="HIAT:u" s="T458">
                  <ts e="T459" id="Seg_1869" n="HIAT:w" s="T458">Dĭgəttə</ts>
                  <nts id="Seg_1870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1872" n="HIAT:w" s="T459">dĭ</ts>
                  <nts id="Seg_1873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1875" n="HIAT:w" s="T460">urgaːba</ts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1878" n="HIAT:w" s="T461">öʔlubi</ts>
                  <nts id="Seg_1879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1881" n="HIAT:w" s="T462">sagər</ts>
                  <nts id="Seg_1882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1884" n="HIAT:w" s="T463">băragəʔ</ts>
                  <nts id="Seg_1885" n="HIAT:ip">.</nts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T469" id="Seg_1888" n="HIAT:u" s="T464">
                  <ts e="T465" id="Seg_1890" n="HIAT:w" s="T464">Dĭ</ts>
                  <nts id="Seg_1891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1893" n="HIAT:w" s="T465">urgaːba</ts>
                  <nts id="Seg_1894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1896" n="HIAT:w" s="T466">ineʔi</ts>
                  <nts id="Seg_1897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1899" n="HIAT:w" s="T467">bar</ts>
                  <nts id="Seg_1900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1902" n="HIAT:w" s="T468">sürerluʔpi</ts>
                  <nts id="Seg_1903" n="HIAT:ip">.</nts>
                  <nts id="Seg_1904" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T475" id="Seg_1906" n="HIAT:u" s="T469">
                  <ts e="T470" id="Seg_1908" n="HIAT:w" s="T469">Dĭgəttə</ts>
                  <nts id="Seg_1909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1911" n="HIAT:w" s="T470">măndə:</ts>
                  <nts id="Seg_1912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1914" n="HIAT:w" s="T471">Öʔlugeʔ</ts>
                  <nts id="Seg_1915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1917" n="HIAT:w" s="T472">tüžöjʔjə</ts>
                  <nts id="Seg_1918" n="HIAT:ip">,</nts>
                  <nts id="Seg_1919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1921" n="HIAT:w" s="T473">pušaj</ts>
                  <nts id="Seg_1922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1924" n="HIAT:w" s="T474">tüžöjʔjə</ts>
                  <nts id="Seg_1925" n="HIAT:ip">.</nts>
                  <nts id="Seg_1926" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T480" id="Seg_1928" n="HIAT:u" s="T475">
                  <ts e="T476" id="Seg_1930" n="HIAT:w" s="T475">Amnutsi</ts>
                  <nts id="Seg_1931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1933" n="HIAT:w" s="T476">bar</ts>
                  <nts id="Seg_1934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1936" n="HIAT:w" s="T477">inebə</ts>
                  <nts id="Seg_1937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1939" n="HIAT:w" s="T478">bar</ts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1942" n="HIAT:w" s="T479">kutləʔjə</ts>
                  <nts id="Seg_1943" n="HIAT:ip">.</nts>
                  <nts id="Seg_1944" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T493" id="Seg_1946" n="HIAT:u" s="T480">
                  <nts id="Seg_1947" n="HIAT:ip">(</nts>
                  <ts e="T481" id="Seg_1949" n="HIAT:w" s="T480">Dĭ=</ts>
                  <nts id="Seg_1950" n="HIAT:ip">)</nts>
                  <nts id="Seg_1951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1953" n="HIAT:w" s="T481">Dĭ</ts>
                  <nts id="Seg_1954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1956" n="HIAT:w" s="T482">bar</ts>
                  <nts id="Seg_1957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1959" n="HIAT:w" s="T483">băragə</ts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1962" n="HIAT:w" s="T484">öʔlubi</ts>
                  <nts id="Seg_1963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1965" n="HIAT:w" s="T485">urgo</ts>
                  <nts id="Seg_1966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1968" n="HIAT:w" s="T486">men</ts>
                  <nts id="Seg_1969" n="HIAT:ip">,</nts>
                  <nts id="Seg_1970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1971" n="HIAT:ip">(</nts>
                  <ts e="T488" id="Seg_1973" n="HIAT:w" s="T487">dĭ=</ts>
                  <nts id="Seg_1974" n="HIAT:ip">)</nts>
                  <nts id="Seg_1975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1977" n="HIAT:w" s="T488">dĭ</ts>
                  <nts id="Seg_1978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1980" n="HIAT:w" s="T489">kambi</ts>
                  <nts id="Seg_1981" n="HIAT:ip">,</nts>
                  <nts id="Seg_1982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1984" n="HIAT:w" s="T490">dĭzeŋ</ts>
                  <nts id="Seg_1985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1987" n="HIAT:w" s="T491">bar</ts>
                  <nts id="Seg_1988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1990" n="HIAT:w" s="T492">sürerlüʔbi</ts>
                  <nts id="Seg_1991" n="HIAT:ip">.</nts>
                  <nts id="Seg_1992" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T497" id="Seg_1994" n="HIAT:u" s="T493">
                  <ts e="T494" id="Seg_1996" n="HIAT:w" s="T493">Dĭgəttə</ts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1999" n="HIAT:w" s="T494">măndə:</ts>
                  <nts id="Seg_2000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_2002" n="HIAT:w" s="T495">Öʔlugeʔ</ts>
                  <nts id="Seg_2003" n="HIAT:ip">…</nts>
                  <nts id="Seg_2004" n="HIAT:ip">"</nts>
                  <nts id="Seg_2005" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T498" id="Seg_2007" n="HIAT:u" s="T497">
                  <nts id="Seg_2008" n="HIAT:ip">(</nts>
                  <nts id="Seg_2009" n="HIAT:ip">(</nts>
                  <ats e="T498" id="Seg_2010" n="HIAT:non-pho" s="T497">BRK</ats>
                  <nts id="Seg_2011" n="HIAT:ip">)</nts>
                  <nts id="Seg_2012" n="HIAT:ip">)</nts>
                  <nts id="Seg_2013" n="HIAT:ip">.</nts>
                  <nts id="Seg_2014" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T504" id="Seg_2016" n="HIAT:u" s="T498">
                  <ts e="T499" id="Seg_2018" n="HIAT:w" s="T498">Dĭgəttə</ts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_2021" n="HIAT:w" s="T499">koŋ:</ts>
                  <nts id="Seg_2022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2023" n="HIAT:ip">(</nts>
                  <ts e="T501" id="Seg_2025" n="HIAT:w" s="T500">Ölia-</ts>
                  <nts id="Seg_2026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_2028" n="HIAT:w" s="T501">öli-</ts>
                  <nts id="Seg_2029" n="HIAT:ip">)</nts>
                  <nts id="Seg_2030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_2032" n="HIAT:w" s="T502">Öʔleʔ</ts>
                  <nts id="Seg_2033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_2035" n="HIAT:w" s="T503">menzeŋ</ts>
                  <nts id="Seg_2036" n="HIAT:ip">!</nts>
                  <nts id="Seg_2037" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T508" id="Seg_2039" n="HIAT:u" s="T504">
                  <ts e="T505" id="Seg_2041" n="HIAT:w" s="T504">Pušaj</ts>
                  <nts id="Seg_2042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_2044" n="HIAT:w" s="T505">menzeŋ</ts>
                  <nts id="Seg_2045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_2047" n="HIAT:w" s="T506">dĭm</ts>
                  <nts id="Seg_2048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_2050" n="HIAT:w" s="T507">sajnüzəʔləʔjə</ts>
                  <nts id="Seg_2051" n="HIAT:ip">.</nts>
                  <nts id="Seg_2052" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T511" id="Seg_2054" n="HIAT:u" s="T508">
                  <ts e="T509" id="Seg_2056" n="HIAT:w" s="T508">Dĭ</ts>
                  <nts id="Seg_2057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_2059" n="HIAT:w" s="T509">öʔlubi</ts>
                  <nts id="Seg_2060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_2062" n="HIAT:w" s="T510">mendən</ts>
                  <nts id="Seg_2063" n="HIAT:ip">.</nts>
                  <nts id="Seg_2064" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T518" id="Seg_2066" n="HIAT:u" s="T511">
                  <ts e="T512" id="Seg_2068" n="HIAT:w" s="T511">A</ts>
                  <nts id="Seg_2069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_2071" n="HIAT:w" s="T512">dĭ</ts>
                  <nts id="Seg_2072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_2074" n="HIAT:w" s="T513">lʼisa</ts>
                  <nts id="Seg_2075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_2077" n="HIAT:w" s="T514">öʔlubi</ts>
                  <nts id="Seg_2078" n="HIAT:ip">,</nts>
                  <nts id="Seg_2079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_2081" n="HIAT:w" s="T515">menzeŋ</ts>
                  <nts id="Seg_2082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_2084" n="HIAT:w" s="T516">lʼisagən</ts>
                  <nts id="Seg_2085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_2087" n="HIAT:w" s="T517">nuʔməluʔpiʔi</ts>
                  <nts id="Seg_2088" n="HIAT:ip">.</nts>
                  <nts id="Seg_2089" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T527" id="Seg_2091" n="HIAT:u" s="T518">
                  <ts e="T519" id="Seg_2093" n="HIAT:w" s="T518">Dĭgəttə</ts>
                  <nts id="Seg_2094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_2096" n="HIAT:w" s="T519">dĭ</ts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_2099" n="HIAT:w" s="T520">koŋ</ts>
                  <nts id="Seg_2100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_2102" n="HIAT:w" s="T521">bar</ts>
                  <nts id="Seg_2103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_2105" n="HIAT:w" s="T522">il</ts>
                  <nts id="Seg_2106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_2108" n="HIAT:w" s="T523">oʔbdəbi:</ts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_2111" n="HIAT:w" s="T524">Kanžəbəj</ts>
                  <nts id="Seg_2112" n="HIAT:ip">,</nts>
                  <nts id="Seg_2113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_2115" n="HIAT:w" s="T525">dĭm</ts>
                  <nts id="Seg_2116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_2118" n="HIAT:w" s="T526">iləbəj</ts>
                  <nts id="Seg_2119" n="HIAT:ip">!</nts>
                  <nts id="Seg_2120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T534" id="Seg_2122" n="HIAT:u" s="T527">
                  <ts e="T528" id="Seg_2124" n="HIAT:w" s="T527">Il</ts>
                  <nts id="Seg_2125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_2127" n="HIAT:w" s="T528">šobiʔi</ts>
                  <nts id="Seg_2128" n="HIAT:ip">,</nts>
                  <nts id="Seg_2129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_2131" n="HIAT:w" s="T529">dĭ</ts>
                  <nts id="Seg_2132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_2134" n="HIAT:w" s="T530">bar</ts>
                  <nts id="Seg_2135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_2137" n="HIAT:w" s="T531">bărabə</ts>
                  <nts id="Seg_2138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_2140" n="HIAT:w" s="T532">bü</ts>
                  <nts id="Seg_2141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_2143" n="HIAT:w" s="T533">kămnəbi</ts>
                  <nts id="Seg_2144" n="HIAT:ip">.</nts>
                  <nts id="Seg_2145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T541" id="Seg_2147" n="HIAT:u" s="T534">
                  <nts id="Seg_2148" n="HIAT:ip">(</nts>
                  <ts e="T535" id="Seg_2150" n="HIAT:w" s="T534">I</ts>
                  <nts id="Seg_2151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_2153" n="HIAT:w" s="T535">bü</ts>
                  <nts id="Seg_2154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_2156" n="HIAT:w" s="T536">il=</ts>
                  <nts id="Seg_2157" n="HIAT:ip">)</nts>
                  <nts id="Seg_2158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2160" n="HIAT:w" s="T537">Il</ts>
                  <nts id="Seg_2161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2163" n="HIAT:w" s="T538">bar</ts>
                  <nts id="Seg_2164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_2166" n="HIAT:w" s="T539">nuʔməluʔbiʔi</ts>
                  <nts id="Seg_2167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_2169" n="HIAT:w" s="T540">bügə</ts>
                  <nts id="Seg_2170" n="HIAT:ip">.</nts>
                  <nts id="Seg_2171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T542" id="Seg_2173" n="HIAT:u" s="T541">
                  <nts id="Seg_2174" n="HIAT:ip">(</nts>
                  <nts id="Seg_2175" n="HIAT:ip">(</nts>
                  <ats e="T542" id="Seg_2176" n="HIAT:non-pho" s="T541">BRK</ats>
                  <nts id="Seg_2177" n="HIAT:ip">)</nts>
                  <nts id="Seg_2178" n="HIAT:ip">)</nts>
                  <nts id="Seg_2179" n="HIAT:ip">.</nts>
                  <nts id="Seg_2180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T550" id="Seg_2182" n="HIAT:u" s="T542">
                  <ts e="T543" id="Seg_2184" n="HIAT:w" s="T542">Dĭgəttə</ts>
                  <nts id="Seg_2185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2187" n="HIAT:w" s="T543">dĭ</ts>
                  <nts id="Seg_2188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2190" n="HIAT:w" s="T544">koŋ</ts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2193" n="HIAT:w" s="T545">kirgarlaʔbə:</ts>
                  <nts id="Seg_2194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2196" n="HIAT:w" s="T546">Pʼeldə</ts>
                  <nts id="Seg_2197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2198" n="HIAT:ip">(</nts>
                  <ts e="T548" id="Seg_2200" n="HIAT:w" s="T547">imĭʔ-</ts>
                  <nts id="Seg_2201" n="HIAT:ip">)</nts>
                  <nts id="Seg_2202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2204" n="HIAT:w" s="T548">ige</ts>
                  <nts id="Seg_2205" n="HIAT:ip">,</nts>
                  <nts id="Seg_2206" n="HIAT:ip">—</nts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2209" n="HIAT:w" s="T549">mămbi</ts>
                  <nts id="Seg_2210" n="HIAT:ip">.</nts>
                  <nts id="Seg_2211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T554" id="Seg_2213" n="HIAT:u" s="T550">
                  <ts e="T551" id="Seg_2215" n="HIAT:w" s="T550">Măn</ts>
                  <nts id="Seg_2216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2218" n="HIAT:w" s="T551">ige</ts>
                  <nts id="Seg_2219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2221" n="HIAT:w" s="T552">tüžöjʔi</ts>
                  <nts id="Seg_2222" n="HIAT:ip">,</nts>
                  <nts id="Seg_2223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2224" n="HIAT:ip">(</nts>
                  <ts e="T554" id="Seg_2226" n="HIAT:w" s="T553">ineʔi</ts>
                  <nts id="Seg_2227" n="HIAT:ip">)</nts>
                  <nts id="Seg_2228" n="HIAT:ip">.</nts>
                  <nts id="Seg_2229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T559" id="Seg_2231" n="HIAT:u" s="T554">
                  <ts e="T555" id="Seg_2233" n="HIAT:w" s="T554">Menzeŋdə</ts>
                  <nts id="Seg_2234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2236" n="HIAT:w" s="T555">mămbiem</ts>
                  <nts id="Seg_2237" n="HIAT:ip">,</nts>
                  <nts id="Seg_2238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2240" n="HIAT:w" s="T556">aktʼa</ts>
                  <nts id="Seg_2241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2243" n="HIAT:w" s="T557">iʔgö</ts>
                  <nts id="Seg_2244" n="HIAT:ip">,</nts>
                  <nts id="Seg_2245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2247" n="HIAT:w" s="T558">mămbi</ts>
                  <nts id="Seg_2248" n="HIAT:ip">.</nts>
                  <nts id="Seg_2249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T566" id="Seg_2251" n="HIAT:u" s="T559">
                  <ts e="T560" id="Seg_2253" n="HIAT:w" s="T559">Ĭmbidə</ts>
                  <nts id="Seg_2254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2256" n="HIAT:w" s="T560">măna</ts>
                  <nts id="Seg_2257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2259" n="HIAT:w" s="T561">ej</ts>
                  <nts id="Seg_2260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2262" n="HIAT:w" s="T562">kereʔ</ts>
                  <nts id="Seg_2263" n="HIAT:ip">,</nts>
                  <nts id="Seg_2264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2266" n="HIAT:w" s="T563">tolʼkă</ts>
                  <nts id="Seg_2267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2269" n="HIAT:w" s="T564">koʔbdom</ts>
                  <nts id="Seg_2270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2272" n="HIAT:w" s="T565">izittə</ts>
                  <nts id="Seg_2273" n="HIAT:ip">"</nts>
                  <nts id="Seg_2274" n="HIAT:ip">.</nts>
                  <nts id="Seg_2275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T567" id="Seg_2277" n="HIAT:u" s="T566">
                  <nts id="Seg_2278" n="HIAT:ip">(</nts>
                  <nts id="Seg_2279" n="HIAT:ip">(</nts>
                  <ats e="T567" id="Seg_2280" n="HIAT:non-pho" s="T566">BRK</ats>
                  <nts id="Seg_2281" n="HIAT:ip">)</nts>
                  <nts id="Seg_2282" n="HIAT:ip">)</nts>
                  <nts id="Seg_2283" n="HIAT:ip">.</nts>
                  <nts id="Seg_2284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T574" id="Seg_2286" n="HIAT:u" s="T567">
                  <ts e="T568" id="Seg_2288" n="HIAT:w" s="T567">Dĭgəttə</ts>
                  <nts id="Seg_2289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2291" n="HIAT:w" s="T568">sagər</ts>
                  <nts id="Seg_2292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2294" n="HIAT:w" s="T569">băranə</ts>
                  <nts id="Seg_2295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2297" n="HIAT:w" s="T570">bü</ts>
                  <nts id="Seg_2298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2300" n="HIAT:w" s="T571">ibi</ts>
                  <nts id="Seg_2301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2303" n="HIAT:w" s="T572">i</ts>
                  <nts id="Seg_2304" n="HIAT:ip">…</nts>
                  <nts id="Seg_2305" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T575" id="Seg_2307" n="HIAT:u" s="T574">
                  <nts id="Seg_2308" n="HIAT:ip">(</nts>
                  <nts id="Seg_2309" n="HIAT:ip">(</nts>
                  <ats e="T575" id="Seg_2310" n="HIAT:non-pho" s="T574">BRK</ats>
                  <nts id="Seg_2311" n="HIAT:ip">)</nts>
                  <nts id="Seg_2312" n="HIAT:ip">)</nts>
                  <nts id="Seg_2313" n="HIAT:ip">.</nts>
                  <nts id="Seg_2314" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T585" id="Seg_2316" n="HIAT:u" s="T575">
                  <ts e="T576" id="Seg_2318" n="HIAT:w" s="T575">Dĭgəttə</ts>
                  <nts id="Seg_2319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2321" n="HIAT:w" s="T576">mĭluʔpi</ts>
                  <nts id="Seg_2322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2324" n="HIAT:w" s="T577">koʔbdobə</ts>
                  <nts id="Seg_2325" n="HIAT:ip">,</nts>
                  <nts id="Seg_2326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2328" n="HIAT:w" s="T578">dĭ</ts>
                  <nts id="Seg_2329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2330" n="HIAT:ip">(</nts>
                  <ts e="T580" id="Seg_2332" n="HIAT:w" s="T579">amnolbi=</ts>
                  <nts id="Seg_2333" n="HIAT:ip">)</nts>
                  <nts id="Seg_2334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2336" n="HIAT:w" s="T580">amnolbi</ts>
                  <nts id="Seg_2337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2339" n="HIAT:w" s="T581">šöjmünə</ts>
                  <nts id="Seg_2340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2342" n="HIAT:w" s="T582">i</ts>
                  <nts id="Seg_2343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2345" n="HIAT:w" s="T583">kambi</ts>
                  <nts id="Seg_2346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2348" n="HIAT:w" s="T584">maʔndə</ts>
                  <nts id="Seg_2349" n="HIAT:ip">.</nts>
                  <nts id="Seg_2350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T589" id="Seg_2352" n="HIAT:u" s="T585">
                  <ts e="T586" id="Seg_2354" n="HIAT:w" s="T585">Šide</ts>
                  <nts id="Seg_2355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2357" n="HIAT:w" s="T586">kaga</ts>
                  <nts id="Seg_2358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2359" n="HIAT:ip">(</nts>
                  <ts e="T588" id="Seg_2361" n="HIAT:w" s="T587">amnolaʔpi=</ts>
                  <nts id="Seg_2362" n="HIAT:ip">)</nts>
                  <nts id="Seg_2363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2365" n="HIAT:w" s="T588">amnolaʔbəbi</ts>
                  <nts id="Seg_2366" n="HIAT:ip">.</nts>
                  <nts id="Seg_2367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T592" id="Seg_2369" n="HIAT:u" s="T589">
                  <ts e="T590" id="Seg_2371" n="HIAT:w" s="T589">Dĭzeŋ</ts>
                  <nts id="Seg_2372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2374" n="HIAT:w" s="T590">nezeŋdə</ts>
                  <nts id="Seg_2375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2377" n="HIAT:w" s="T591">ibiʔi</ts>
                  <nts id="Seg_2378" n="HIAT:ip">.</nts>
                  <nts id="Seg_2379" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T596" id="Seg_2381" n="HIAT:u" s="T592">
                  <ts e="T593" id="Seg_2383" n="HIAT:w" s="T592">Dĭzeŋ</ts>
                  <nts id="Seg_2384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2386" n="HIAT:w" s="T593">oʔbdəbiʔi</ts>
                  <nts id="Seg_2387" n="HIAT:ip">,</nts>
                  <nts id="Seg_2388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2390" n="HIAT:w" s="T594">kambiʔi</ts>
                  <nts id="Seg_2391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2393" n="HIAT:w" s="T595">dʼijenə</ts>
                  <nts id="Seg_2394" n="HIAT:ip">.</nts>
                  <nts id="Seg_2395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T600" id="Seg_2397" n="HIAT:u" s="T596">
                  <ts e="T597" id="Seg_2399" n="HIAT:w" s="T596">A</ts>
                  <nts id="Seg_2400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2402" n="HIAT:w" s="T597">nezeŋ</ts>
                  <nts id="Seg_2403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2405" n="HIAT:w" s="T598">maʔndə</ts>
                  <nts id="Seg_2406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2408" n="HIAT:w" s="T599">bar</ts>
                  <nts id="Seg_2409" n="HIAT:ip">.</nts>
                  <nts id="Seg_2410" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T605" id="Seg_2412" n="HIAT:u" s="T600">
                  <nts id="Seg_2413" n="HIAT:ip">(</nts>
                  <ts e="T601" id="Seg_2415" n="HIAT:w" s="T600">Nüdʼi=</ts>
                  <nts id="Seg_2416" n="HIAT:ip">)</nts>
                  <nts id="Seg_2417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2419" n="HIAT:w" s="T601">Nüdʼin</ts>
                  <nts id="Seg_2420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2421" n="HIAT:ip">(</nts>
                  <ts e="T603" id="Seg_2423" n="HIAT:w" s="T602">amzit-</ts>
                  <nts id="Seg_2424" n="HIAT:ip">)</nts>
                  <nts id="Seg_2425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2427" n="HIAT:w" s="T603">amorzittə</ts>
                  <nts id="Seg_2428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2430" n="HIAT:w" s="T604">amnəbiʔi</ts>
                  <nts id="Seg_2431" n="HIAT:ip">.</nts>
                  <nts id="Seg_2432" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T612" id="Seg_2434" n="HIAT:u" s="T605">
                  <ts e="T606" id="Seg_2436" n="HIAT:w" s="T605">Dĭgəttə</ts>
                  <nts id="Seg_2437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2439" n="HIAT:w" s="T606">măndoliaʔi</ts>
                  <nts id="Seg_2440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2442" n="HIAT:w" s="T607">aspaʔən</ts>
                  <nts id="Seg_2443" n="HIAT:ip">,</nts>
                  <nts id="Seg_2444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2445" n="HIAT:ip">(</nts>
                  <ts e="T609" id="Seg_2447" n="HIAT:w" s="T608">gittə-</ts>
                  <nts id="Seg_2448" n="HIAT:ip">)</nts>
                  <nts id="Seg_2449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2451" n="HIAT:w" s="T609">girgitdə</ts>
                  <nts id="Seg_2452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2454" n="HIAT:w" s="T610">kuza</ts>
                  <nts id="Seg_2455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2457" n="HIAT:w" s="T611">amnolaʔbə</ts>
                  <nts id="Seg_2458" n="HIAT:ip">.</nts>
                  <nts id="Seg_2459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T621" id="Seg_2461" n="HIAT:u" s="T612">
                  <ts e="T613" id="Seg_2463" n="HIAT:w" s="T612">Dĭzeŋ</ts>
                  <nts id="Seg_2464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2466" n="HIAT:w" s="T613">moːʔi</ts>
                  <nts id="Seg_2467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2468" n="HIAT:ip">(</nts>
                  <ts e="T615" id="Seg_2470" n="HIAT:w" s="T614">со-</ts>
                  <nts id="Seg_2471" n="HIAT:ip">)</nts>
                  <nts id="Seg_2472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2474" n="HIAT:w" s="T615">oʔbdəbiʔi</ts>
                  <nts id="Seg_2475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2477" n="HIAT:w" s="T616">i</ts>
                  <nts id="Seg_2478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2479" n="HIAT:ip">(</nts>
                  <ts e="T618" id="Seg_2481" n="HIAT:w" s="T617">aspaʔdə</ts>
                  <nts id="Seg_2482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2484" n="HIAT:w" s="T618">il-</ts>
                  <nts id="Seg_2485" n="HIAT:ip">)</nts>
                  <nts id="Seg_2486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2488" n="HIAT:w" s="T619">aspaʔdə</ts>
                  <nts id="Seg_2489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2491" n="HIAT:w" s="T620">embiʔi</ts>
                  <nts id="Seg_2492" n="HIAT:ip">.</nts>
                  <nts id="Seg_2493" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T629" id="Seg_2495" n="HIAT:u" s="T621">
                  <ts e="T622" id="Seg_2497" n="HIAT:w" s="T621">A</ts>
                  <nts id="Seg_2498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2500" n="HIAT:w" s="T622">dĭ</ts>
                  <nts id="Seg_2501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2503" n="HIAT:w" s="T623">mu</ts>
                  <nts id="Seg_2504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2506" n="HIAT:w" s="T624">bar</ts>
                  <nts id="Seg_2507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2508" n="HIAT:ip">(</nts>
                  <ts e="T627" id="Seg_2510" n="HIAT:w" s="T626">dʼo-</ts>
                  <nts id="Seg_2511" n="HIAT:ip">)</nts>
                  <nts id="Seg_2512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2514" n="HIAT:w" s="T627">dʼüʔpi</ts>
                  <nts id="Seg_2515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2517" n="HIAT:w" s="T628">ibi</ts>
                  <nts id="Seg_2518" n="HIAT:ip">.</nts>
                  <nts id="Seg_2519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T632" id="Seg_2521" n="HIAT:u" s="T629">
                  <ts e="T630" id="Seg_2523" n="HIAT:w" s="T629">Dĭgəttə</ts>
                  <nts id="Seg_2524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2526" n="HIAT:w" s="T630">dĭ</ts>
                  <nts id="Seg_2527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2529" n="HIAT:w" s="T631">šobi</ts>
                  <nts id="Seg_2530" n="HIAT:ip">.</nts>
                  <nts id="Seg_2531" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T637" id="Seg_2533" n="HIAT:u" s="T632">
                  <ts e="T633" id="Seg_2535" n="HIAT:w" s="T632">Ej</ts>
                  <nts id="Seg_2536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2538" n="HIAT:w" s="T633">mobi</ts>
                  <nts id="Seg_2539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2541" n="HIAT:w" s="T634">nendəsʼtə</ts>
                  <nts id="Seg_2542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2544" n="HIAT:w" s="T635">dĭ</ts>
                  <nts id="Seg_2545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2547" n="HIAT:w" s="T636">kuza</ts>
                  <nts id="Seg_2548" n="HIAT:ip">.</nts>
                  <nts id="Seg_2549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T640" id="Seg_2551" n="HIAT:u" s="T637">
                  <ts e="T638" id="Seg_2553" n="HIAT:w" s="T637">Onʼiʔ</ts>
                  <nts id="Seg_2554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2556" n="HIAT:w" s="T638">nem</ts>
                  <nts id="Seg_2557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2559" n="HIAT:w" s="T639">kutlaʔpi</ts>
                  <nts id="Seg_2560" n="HIAT:ip">.</nts>
                  <nts id="Seg_2561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T650" id="Seg_2563" n="HIAT:u" s="T640">
                  <ts e="T641" id="Seg_2565" n="HIAT:w" s="T640">Dĭgəttə</ts>
                  <nts id="Seg_2566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2568" n="HIAT:w" s="T641">šide</ts>
                  <nts id="Seg_2569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2570" n="HIAT:ip">(</nts>
                  <ts e="T643" id="Seg_2572" n="HIAT:w" s="T642">š-</ts>
                  <nts id="Seg_2573" n="HIAT:ip">)</nts>
                  <nts id="Seg_2574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2576" n="HIAT:w" s="T643">bazoʔ</ts>
                  <nts id="Seg_2577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2579" n="HIAT:w" s="T644">nenə</ts>
                  <nts id="Seg_2580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2582" n="HIAT:w" s="T645">măndə:</ts>
                  <nts id="Seg_2583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2585" n="HIAT:w" s="T646">Ĭmbi</ts>
                  <nts id="Seg_2586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2588" n="HIAT:w" s="T647">tăn</ts>
                  <nts id="Seg_2589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2591" n="HIAT:w" s="T648">amnolaʔbəl</ts>
                  <nts id="Seg_2592" n="HIAT:ip">,</nts>
                  <nts id="Seg_2593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2595" n="HIAT:w" s="T649">nendeʔ</ts>
                  <nts id="Seg_2596" n="HIAT:ip">!</nts>
                  <nts id="Seg_2597" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T654" id="Seg_2599" n="HIAT:u" s="T650">
                  <ts e="T651" id="Seg_2601" n="HIAT:w" s="T650">Dĭ</ts>
                  <nts id="Seg_2602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2604" n="HIAT:w" s="T651">bar</ts>
                  <nts id="Seg_2605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2607" n="HIAT:w" s="T652">начал</ts>
                  <nts id="Seg_2608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2610" n="HIAT:w" s="T653">nendəsʼtə</ts>
                  <nts id="Seg_2611" n="HIAT:ip">.</nts>
                  <nts id="Seg_2612" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T659" id="Seg_2614" n="HIAT:u" s="T654">
                  <ts e="T655" id="Seg_2616" n="HIAT:w" s="T654">Dĭ</ts>
                  <nts id="Seg_2617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2619" n="HIAT:w" s="T655">dĭʔnə</ts>
                  <nts id="Seg_2620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2622" n="HIAT:w" s="T656">baltuziʔ</ts>
                  <nts id="Seg_2623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2625" n="HIAT:w" s="T657">ulubə</ts>
                  <nts id="Seg_2626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2628" n="HIAT:w" s="T658">jaʔpi</ts>
                  <nts id="Seg_2629" n="HIAT:ip">.</nts>
                  <nts id="Seg_2630" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T662" id="Seg_2632" n="HIAT:u" s="T659">
                  <ts e="T660" id="Seg_2634" n="HIAT:w" s="T659">Dĭgəttə</ts>
                  <nts id="Seg_2635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2637" n="HIAT:w" s="T660">esseŋdə</ts>
                  <nts id="Seg_2638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2640" n="HIAT:w" s="T661">kutlaːmbi</ts>
                  <nts id="Seg_2641" n="HIAT:ip">.</nts>
                  <nts id="Seg_2642" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T669" id="Seg_2644" n="HIAT:u" s="T662">
                  <ts e="T663" id="Seg_2646" n="HIAT:w" s="T662">Dĭgəttə</ts>
                  <nts id="Seg_2647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2649" n="HIAT:w" s="T663">tĭ</ts>
                  <nts id="Seg_2650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2651" n="HIAT:ip">(</nts>
                  <ts e="T665" id="Seg_2653" n="HIAT:w" s="T664">dĭz-</ts>
                  <nts id="Seg_2654" n="HIAT:ip">)</nts>
                  <nts id="Seg_2655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2657" n="HIAT:w" s="T665">dĭzeŋdə</ts>
                  <nts id="Seg_2658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2660" n="HIAT:w" s="T666">maːndə</ts>
                  <nts id="Seg_2661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2662" n="HIAT:ip">(</nts>
                  <ts e="T668" id="Seg_2664" n="HIAT:w" s="T667">ma-</ts>
                  <nts id="Seg_2665" n="HIAT:ip">)</nts>
                  <nts id="Seg_2666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2668" n="HIAT:w" s="T668">parluʔpi</ts>
                  <nts id="Seg_2669" n="HIAT:ip">.</nts>
                  <nts id="Seg_2670" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T673" id="Seg_2672" n="HIAT:u" s="T669">
                  <ts e="T670" id="Seg_2674" n="HIAT:w" s="T669">Dĭzeŋ</ts>
                  <nts id="Seg_2675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2676" n="HIAT:ip">(</nts>
                  <ts e="T671" id="Seg_2678" n="HIAT:w" s="T670">neze-</ts>
                  <nts id="Seg_2679" n="HIAT:ip">)</nts>
                  <nts id="Seg_2680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2682" n="HIAT:w" s="T671">nezeŋdə</ts>
                  <nts id="Seg_2683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2685" n="HIAT:w" s="T672">naga</ts>
                  <nts id="Seg_2686" n="HIAT:ip">.</nts>
                  <nts id="Seg_2687" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T678" id="Seg_2689" n="HIAT:u" s="T673">
                  <ts e="T674" id="Seg_2691" n="HIAT:w" s="T673">Šindidə</ts>
                  <nts id="Seg_2692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2694" n="HIAT:w" s="T674">kuʔpi</ts>
                  <nts id="Seg_2695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2697" n="HIAT:w" s="T675">essem</ts>
                  <nts id="Seg_2698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2700" n="HIAT:w" s="T676">i</ts>
                  <nts id="Seg_2701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2703" n="HIAT:w" s="T677">nezeŋdə</ts>
                  <nts id="Seg_2704" n="HIAT:ip">.</nts>
                  <nts id="Seg_2705" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T682" id="Seg_2707" n="HIAT:u" s="T678">
                  <nts id="Seg_2708" n="HIAT:ip">(</nts>
                  <ts e="T679" id="Seg_2710" n="HIAT:w" s="T678">Amo-</ts>
                  <nts id="Seg_2711" n="HIAT:ip">)</nts>
                  <nts id="Seg_2712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2714" n="HIAT:w" s="T679">Amnobiʔi</ts>
                  <nts id="Seg_2715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2717" n="HIAT:w" s="T680">büzʼe</ts>
                  <nts id="Seg_2718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2720" n="HIAT:w" s="T681">nüketsi</ts>
                  <nts id="Seg_2721" n="HIAT:ip">.</nts>
                  <nts id="Seg_2722" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T688" id="Seg_2724" n="HIAT:u" s="T682">
                  <ts e="T683" id="Seg_2726" n="HIAT:w" s="T682">Dĭzeŋ</ts>
                  <nts id="Seg_2727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2729" n="HIAT:w" s="T683">ĭmbidə</ts>
                  <nts id="Seg_2730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2732" n="HIAT:w" s="T684">nagobi</ts>
                  <nts id="Seg_2733" n="HIAT:ip">,</nts>
                  <nts id="Seg_2734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2736" n="HIAT:w" s="T685">tolʼkă</ts>
                  <nts id="Seg_2737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2739" n="HIAT:w" s="T686">onʼiʔ</ts>
                  <nts id="Seg_2740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2742" n="HIAT:w" s="T687">tüžöj</ts>
                  <nts id="Seg_2743" n="HIAT:ip">.</nts>
                  <nts id="Seg_2744" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T692" id="Seg_2746" n="HIAT:u" s="T688">
                  <ts e="T689" id="Seg_2748" n="HIAT:w" s="T688">Dĭzeŋ</ts>
                  <nts id="Seg_2749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2751" n="HIAT:w" s="T689">amorzittə</ts>
                  <nts id="Seg_2752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2754" n="HIAT:w" s="T690">naga</ts>
                  <nts id="Seg_2755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2757" n="HIAT:w" s="T691">ibi</ts>
                  <nts id="Seg_2758" n="HIAT:ip">.</nts>
                  <nts id="Seg_2759" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T695" id="Seg_2761" n="HIAT:u" s="T692">
                  <ts e="T693" id="Seg_2763" n="HIAT:w" s="T692">Dĭgəttə</ts>
                  <nts id="Seg_2764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2766" n="HIAT:w" s="T693">tüžöjdə</ts>
                  <nts id="Seg_2767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2769" n="HIAT:w" s="T694">bătluʔpi</ts>
                  <nts id="Seg_2770" n="HIAT:ip">.</nts>
                  <nts id="Seg_2771" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T703" id="Seg_2773" n="HIAT:u" s="T695">
                  <ts e="T696" id="Seg_2775" n="HIAT:w" s="T695">Nükem</ts>
                  <nts id="Seg_2776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2778" n="HIAT:w" s="T696">sürerluʔpi:</ts>
                  <nts id="Seg_2779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2781" n="HIAT:w" s="T697">Kanaʔ</ts>
                  <nts id="Seg_2782" n="HIAT:ip">,</nts>
                  <nts id="Seg_2783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698.tx-PKZ.1" id="Seg_2785" n="HIAT:w" s="T698">a</ts>
                  <nts id="Seg_2786" n="HIAT:ip">_</nts>
                  <ts e="T699" id="Seg_2788" n="HIAT:w" s="T698.tx-PKZ.1">to</ts>
                  <nts id="Seg_2789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2791" n="HIAT:w" s="T699">măna</ts>
                  <nts id="Seg_2792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2794" n="HIAT:w" s="T700">boskəndəm</ts>
                  <nts id="Seg_2795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2797" n="HIAT:w" s="T701">amga</ts>
                  <nts id="Seg_2798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2800" n="HIAT:w" s="T702">moləj</ts>
                  <nts id="Seg_2801" n="HIAT:ip">.</nts>
                  <nts id="Seg_2802" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T707" id="Seg_2804" n="HIAT:u" s="T703">
                  <ts e="T704" id="Seg_2806" n="HIAT:w" s="T703">Dĭ</ts>
                  <nts id="Seg_2807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2809" n="HIAT:w" s="T704">nüke</ts>
                  <nts id="Seg_2810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2812" n="HIAT:w" s="T705">kambi</ts>
                  <nts id="Seg_2813" n="HIAT:ip">,</nts>
                  <nts id="Seg_2814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2816" n="HIAT:w" s="T706">kambi</ts>
                  <nts id="Seg_2817" n="HIAT:ip">.</nts>
                  <nts id="Seg_2818" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T710" id="Seg_2820" n="HIAT:u" s="T707">
                  <ts e="T708" id="Seg_2822" n="HIAT:w" s="T707">Išo</ts>
                  <nts id="Seg_2823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2825" n="HIAT:w" s="T708">maʔ</ts>
                  <nts id="Seg_2826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2828" n="HIAT:w" s="T709">kubi</ts>
                  <nts id="Seg_2829" n="HIAT:ip">.</nts>
                  <nts id="Seg_2830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T712" id="Seg_2832" n="HIAT:u" s="T710">
                  <ts e="T711" id="Seg_2834" n="HIAT:w" s="T710">Maʔdə</ts>
                  <nts id="Seg_2835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2837" n="HIAT:w" s="T711">šobi</ts>
                  <nts id="Seg_2838" n="HIAT:ip">.</nts>
                  <nts id="Seg_2839" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T717" id="Seg_2841" n="HIAT:u" s="T712">
                  <ts e="T713" id="Seg_2843" n="HIAT:w" s="T712">Dĭn</ts>
                  <nts id="Seg_2844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2846" n="HIAT:w" s="T713">sʼel</ts>
                  <nts id="Seg_2847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2849" n="HIAT:w" s="T714">iʔbolaʔbə</ts>
                  <nts id="Seg_2850" n="HIAT:ip">,</nts>
                  <nts id="Seg_2851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2853" n="HIAT:w" s="T715">uja</ts>
                  <nts id="Seg_2854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2856" n="HIAT:w" s="T716">iʔbolaʔbə</ts>
                  <nts id="Seg_2857" n="HIAT:ip">.</nts>
                  <nts id="Seg_2858" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T721" id="Seg_2860" n="HIAT:u" s="T717">
                  <ts e="T718" id="Seg_2862" n="HIAT:w" s="T717">Dĭ</ts>
                  <nts id="Seg_2863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2865" n="HIAT:w" s="T718">mĭnzərbi</ts>
                  <nts id="Seg_2866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2868" n="HIAT:w" s="T719">bar</ts>
                  <nts id="Seg_2869" n="HIAT:ip">,</nts>
                  <nts id="Seg_2870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2872" n="HIAT:w" s="T720">amorbi</ts>
                  <nts id="Seg_2873" n="HIAT:ip">.</nts>
                  <nts id="Seg_2874" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T725" id="Seg_2876" n="HIAT:u" s="T721">
                  <ts e="T722" id="Seg_2878" n="HIAT:w" s="T721">Nünniet</ts>
                  <nts id="Seg_2879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2881" n="HIAT:w" s="T722">bar:</ts>
                  <nts id="Seg_2882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2884" n="HIAT:w" s="T723">šindidə</ts>
                  <nts id="Seg_2885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2887" n="HIAT:w" s="T724">šonəga</ts>
                  <nts id="Seg_2888" n="HIAT:ip">.</nts>
                  <nts id="Seg_2889" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T731" id="Seg_2891" n="HIAT:u" s="T725">
                  <ts e="T726" id="Seg_2893" n="HIAT:w" s="T725">Iʔgö</ts>
                  <nts id="Seg_2894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2896" n="HIAT:w" s="T726">ular</ts>
                  <nts id="Seg_2897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2898" n="HIAT:ip">(</nts>
                  <ts e="T728" id="Seg_2900" n="HIAT:w" s="T727">dep-</ts>
                  <nts id="Seg_2901" n="HIAT:ip">)</nts>
                  <nts id="Seg_2902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2904" n="HIAT:w" s="T728">deʔpi</ts>
                  <nts id="Seg_2905" n="HIAT:ip">,</nts>
                  <nts id="Seg_2906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2908" n="HIAT:w" s="T729">tüžöjʔi</ts>
                  <nts id="Seg_2909" n="HIAT:ip">,</nts>
                  <nts id="Seg_2910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2912" n="HIAT:w" s="T730">ineʔi</ts>
                  <nts id="Seg_2913" n="HIAT:ip">.</nts>
                  <nts id="Seg_2914" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T734" id="Seg_2916" n="HIAT:u" s="T731">
                  <ts e="T732" id="Seg_2918" n="HIAT:w" s="T731">Dĭgəttə</ts>
                  <nts id="Seg_2919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2921" n="HIAT:w" s="T732">šobi</ts>
                  <nts id="Seg_2922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2924" n="HIAT:w" s="T733">maʔdə</ts>
                  <nts id="Seg_2925" n="HIAT:ip">.</nts>
                  <nts id="Seg_2926" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T738" id="Seg_2928" n="HIAT:u" s="T734">
                  <ts e="T735" id="Seg_2930" n="HIAT:w" s="T734">Măndə:</ts>
                  <nts id="Seg_2931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2932" n="HIAT:ip">(</nts>
                  <ts e="T736" id="Seg_2934" n="HIAT:w" s="T735">Ka-</ts>
                  <nts id="Seg_2935" n="HIAT:ip">)</nts>
                  <nts id="Seg_2936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2938" n="HIAT:w" s="T736">Kardə</ts>
                  <nts id="Seg_2939" n="HIAT:ip">,</nts>
                  <nts id="Seg_2940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2942" n="HIAT:w" s="T737">aji</ts>
                  <nts id="Seg_2943" n="HIAT:ip">!</nts>
                  <nts id="Seg_2944" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T743" id="Seg_2946" n="HIAT:u" s="T738">
                  <ts e="T739" id="Seg_2948" n="HIAT:w" s="T738">Aji</ts>
                  <nts id="Seg_2949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2951" n="HIAT:w" s="T739">karluʔpi</ts>
                  <nts id="Seg_2952" n="HIAT:ip">,</nts>
                  <nts id="Seg_2953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2955" n="HIAT:w" s="T740">dĭ</ts>
                  <nts id="Seg_2956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2958" n="HIAT:w" s="T741">šobi</ts>
                  <nts id="Seg_2959" n="HIAT:ip">,</nts>
                  <nts id="Seg_2960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2962" n="HIAT:w" s="T742">măndərla</ts>
                  <nts id="Seg_2963" n="HIAT:ip">.</nts>
                  <nts id="Seg_2964" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T751" id="Seg_2966" n="HIAT:u" s="T743">
                  <ts e="T744" id="Seg_2968" n="HIAT:w" s="T743">Šindidə</ts>
                  <nts id="Seg_2969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2971" n="HIAT:w" s="T744">ibi</ts>
                  <nts id="Seg_2972" n="HIAT:ip">,</nts>
                  <nts id="Seg_2973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2975" n="HIAT:w" s="T745">sʼel</ts>
                  <nts id="Seg_2976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2978" n="HIAT:w" s="T746">naga</ts>
                  <nts id="Seg_2979" n="HIAT:ip">,</nts>
                  <nts id="Seg_2980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2982" n="HIAT:w" s="T747">uja</ts>
                  <nts id="Seg_2983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2985" n="HIAT:w" s="T748">naga</ts>
                  <nts id="Seg_2986" n="HIAT:ip">,</nts>
                  <nts id="Seg_2987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_2989" n="HIAT:w" s="T749">ipek</ts>
                  <nts id="Seg_2990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_2992" n="HIAT:w" s="T750">naga</ts>
                  <nts id="Seg_2993" n="HIAT:ip">.</nts>
                  <nts id="Seg_2994" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T754" id="Seg_2996" n="HIAT:u" s="T751">
                  <ts e="T752" id="Seg_2998" n="HIAT:w" s="T751">Šində</ts>
                  <nts id="Seg_2999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_3001" n="HIAT:w" s="T752">dön</ts>
                  <nts id="Seg_3002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_3004" n="HIAT:w" s="T753">ibi</ts>
                  <nts id="Seg_3005" n="HIAT:ip">?</nts>
                  <nts id="Seg_3006" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T758" id="Seg_3008" n="HIAT:u" s="T754">
                  <ts e="T755" id="Seg_3010" n="HIAT:w" s="T754">Tibi</ts>
                  <nts id="Seg_3011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_3013" n="HIAT:w" s="T755">dăk</ts>
                  <nts id="Seg_3014" n="HIAT:ip">,</nts>
                  <nts id="Seg_3015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_3017" n="HIAT:w" s="T756">büzʼem</ts>
                  <nts id="Seg_3018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_3020" n="HIAT:w" s="T757">moləj</ts>
                  <nts id="Seg_3021" n="HIAT:ip">.</nts>
                  <nts id="Seg_3022" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T762" id="Seg_3024" n="HIAT:u" s="T758">
                  <ts e="T759" id="Seg_3026" n="HIAT:w" s="T758">Ne</ts>
                  <nts id="Seg_3027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_3029" n="HIAT:w" s="T759">dăk</ts>
                  <nts id="Seg_3030" n="HIAT:ip">,</nts>
                  <nts id="Seg_3031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_3033" n="HIAT:w" s="T760">helem</ts>
                  <nts id="Seg_3034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_3036" n="HIAT:w" s="T761">moləj</ts>
                  <nts id="Seg_3037" n="HIAT:ip">.</nts>
                  <nts id="Seg_3038" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T767" id="Seg_3040" n="HIAT:u" s="T762">
                  <ts e="T763" id="Seg_3042" n="HIAT:w" s="T762">Dĭgəttə</ts>
                  <nts id="Seg_3043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_3045" n="HIAT:w" s="T763">dĭ</ts>
                  <nts id="Seg_3046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_3048" n="HIAT:w" s="T764">nüke</ts>
                  <nts id="Seg_3049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_3051" n="HIAT:w" s="T765">uʔbdəbi</ts>
                  <nts id="Seg_3052" n="HIAT:ip">,</nts>
                  <nts id="Seg_3053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_3055" n="HIAT:w" s="T766">šobi</ts>
                  <nts id="Seg_3056" n="HIAT:ip">.</nts>
                  <nts id="Seg_3057" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T770" id="Seg_3059" n="HIAT:u" s="T767">
                  <ts e="T768" id="Seg_3061" n="HIAT:w" s="T767">Dĭ</ts>
                  <nts id="Seg_3062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_3064" n="HIAT:w" s="T768">dĭm</ts>
                  <nts id="Seg_3065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_3067" n="HIAT:w" s="T769">amnolbi</ts>
                  <nts id="Seg_3068" n="HIAT:ip">.</nts>
                  <nts id="Seg_3069" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T772" id="Seg_3071" n="HIAT:u" s="T770">
                  <ts e="T771" id="Seg_3073" n="HIAT:w" s="T770">Amorla</ts>
                  <nts id="Seg_3074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_3076" n="HIAT:w" s="T771">amnobiʔi</ts>
                  <nts id="Seg_3077" n="HIAT:ip">.</nts>
                  <nts id="Seg_3078" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T782" id="Seg_3080" n="HIAT:u" s="T772">
                  <ts e="T773" id="Seg_3082" n="HIAT:w" s="T772">Dĭ</ts>
                  <nts id="Seg_3083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_3085" n="HIAT:w" s="T773">surarlaʔbə:</ts>
                  <nts id="Seg_3086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_3088" n="HIAT:w" s="T774">Ĭmbi</ts>
                  <nts id="Seg_3089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_3091" n="HIAT:w" s="T775">tăn</ts>
                  <nts id="Seg_3092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_3094" n="HIAT:w" s="T776">udal</ts>
                  <nts id="Seg_3095" n="HIAT:ip">,</nts>
                  <nts id="Seg_3096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_3098" n="HIAT:w" s="T777">üjül</ts>
                  <nts id="Seg_3099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_3101" n="HIAT:w" s="T778">naga</ts>
                  <nts id="Seg_3102" n="HIAT:ip">,</nts>
                  <nts id="Seg_3103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_3105" n="HIAT:w" s="T779">kăde</ts>
                  <nts id="Seg_3106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_3108" n="HIAT:w" s="T780">tăn</ts>
                  <nts id="Seg_3109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_3111" n="HIAT:w" s="T781">mĭnliel</ts>
                  <nts id="Seg_3112" n="HIAT:ip">?</nts>
                  <nts id="Seg_3113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T785" id="Seg_3115" n="HIAT:u" s="T782">
                  <ts e="T783" id="Seg_3117" n="HIAT:w" s="T782">Da</ts>
                  <nts id="Seg_3118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_3120" n="HIAT:w" s="T783">dăre</ts>
                  <nts id="Seg_3121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_3123" n="HIAT:w" s="T784">mĭnliel</ts>
                  <nts id="Seg_3124" n="HIAT:ip">.</nts>
                  <nts id="Seg_3125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T788" id="Seg_3127" n="HIAT:u" s="T785">
                  <ts e="T786" id="Seg_3129" n="HIAT:w" s="T785">Tăn</ts>
                  <nts id="Seg_3130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_3132" n="HIAT:w" s="T786">jamanə</ts>
                  <nts id="Seg_3133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_3135" n="HIAT:w" s="T787">suʔmiləl</ts>
                  <nts id="Seg_3136" n="HIAT:ip">?</nts>
                  <nts id="Seg_3137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T789" id="Seg_3139" n="HIAT:u" s="T788">
                  <ts e="T789" id="Seg_3141" n="HIAT:w" s="T788">Suʔmiləm</ts>
                  <nts id="Seg_3142" n="HIAT:ip">.</nts>
                  <nts id="Seg_3143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T791" id="Seg_3145" n="HIAT:u" s="T789">
                  <ts e="T790" id="Seg_3147" n="HIAT:w" s="T789">Dĭgəttə</ts>
                  <nts id="Seg_3148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_3150" n="HIAT:w" s="T790">kambiʔi</ts>
                  <nts id="Seg_3151" n="HIAT:ip">.</nts>
                  <nts id="Seg_3152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T799" id="Seg_3154" n="HIAT:u" s="T791">
                  <ts e="T792" id="Seg_3156" n="HIAT:w" s="T791">Dĭ</ts>
                  <nts id="Seg_3157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_3159" n="HIAT:w" s="T792">süʔmibi</ts>
                  <nts id="Seg_3160" n="HIAT:ip">,</nts>
                  <nts id="Seg_3161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_3163" n="HIAT:w" s="T793">dĭ</ts>
                  <nts id="Seg_3164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_3166" n="HIAT:w" s="T794">dĭʔnə</ts>
                  <nts id="Seg_3167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_3169" n="HIAT:w" s="T795">baltuzi</ts>
                  <nts id="Seg_3170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_3172" n="HIAT:w" s="T796">ulubə</ts>
                  <nts id="Seg_3173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_3175" n="HIAT:w" s="T797">bar</ts>
                  <nts id="Seg_3176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_3178" n="HIAT:w" s="T798">sajjaʔpi</ts>
                  <nts id="Seg_3179" n="HIAT:ip">.</nts>
                  <nts id="Seg_3180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T803" id="Seg_3182" n="HIAT:u" s="T799">
                  <ts e="T800" id="Seg_3184" n="HIAT:w" s="T799">Dĭgəttə</ts>
                  <nts id="Seg_3185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_3187" n="HIAT:w" s="T800">bar</ts>
                  <nts id="Seg_3188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3189" n="HIAT:ip">(</nts>
                  <ts e="T802" id="Seg_3191" n="HIAT:w" s="T801">šama</ts>
                  <nts id="Seg_3192" n="HIAT:ip">)</nts>
                  <nts id="Seg_3193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_3195" n="HIAT:w" s="T802">ibi</ts>
                  <nts id="Seg_3196" n="HIAT:ip">.</nts>
                  <nts id="Seg_3197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T807" id="Seg_3199" n="HIAT:u" s="T803">
                  <ts e="T804" id="Seg_3201" n="HIAT:w" s="T803">Sʼeldə</ts>
                  <nts id="Seg_3202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_3204" n="HIAT:w" s="T804">ibi</ts>
                  <nts id="Seg_3205" n="HIAT:ip">,</nts>
                  <nts id="Seg_3206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_3208" n="HIAT:w" s="T805">uja</ts>
                  <nts id="Seg_3209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_3211" n="HIAT:w" s="T806">ibi</ts>
                  <nts id="Seg_3212" n="HIAT:ip">.</nts>
                  <nts id="Seg_3213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T813" id="Seg_3215" n="HIAT:u" s="T807">
                  <ts e="T808" id="Seg_3217" n="HIAT:w" s="T807">Puzɨrʼdə</ts>
                  <nts id="Seg_3218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_3220" n="HIAT:w" s="T808">embi</ts>
                  <nts id="Seg_3221" n="HIAT:ip">,</nts>
                  <nts id="Seg_3222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_3224" n="HIAT:w" s="T809">dĭgəttə</ts>
                  <nts id="Seg_3225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_3227" n="HIAT:w" s="T810">parluʔbi</ts>
                  <nts id="Seg_3228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_3230" n="HIAT:w" s="T811">bostə</ts>
                  <nts id="Seg_3231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_3233" n="HIAT:w" s="T812">büzʼendə</ts>
                  <nts id="Seg_3234" n="HIAT:ip">.</nts>
                  <nts id="Seg_3235" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T825" id="Seg_3237" n="HIAT:u" s="T813">
                  <ts e="T814" id="Seg_3239" n="HIAT:w" s="T813">Maʔdə</ts>
                  <nts id="Seg_3240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3241" n="HIAT:ip">(</nts>
                  <ts e="T815" id="Seg_3243" n="HIAT:w" s="T814">saj-</ts>
                  <nts id="Seg_3244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_3246" n="HIAT:w" s="T815">š-</ts>
                  <nts id="Seg_3247" n="HIAT:ip">)</nts>
                  <nts id="Seg_3248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_3250" n="HIAT:w" s="T816">sʼabi</ts>
                  <nts id="Seg_3251" n="HIAT:ip">,</nts>
                  <nts id="Seg_3252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_3254" n="HIAT:w" s="T817">a</ts>
                  <nts id="Seg_3255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_3257" n="HIAT:w" s="T818">büzʼe</ts>
                  <nts id="Seg_3258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_3260" n="HIAT:w" s="T819">kössi</ts>
                  <nts id="Seg_3261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3262" n="HIAT:ip">(</nts>
                  <ts e="T821" id="Seg_3264" n="HIAT:w" s="T820">uda</ts>
                  <nts id="Seg_3265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_3267" n="HIAT:w" s="T821">d-</ts>
                  <nts id="Seg_3268" n="HIAT:ip">)</nts>
                  <nts id="Seg_3269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_3271" n="HIAT:w" s="T822">udabə</ts>
                  <nts id="Seg_3272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3274" n="HIAT:w" s="T823">bar</ts>
                  <nts id="Seg_3275" n="HIAT:ip">…</nts>
                  <nts id="Seg_3276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T830" id="Seg_3278" n="HIAT:u" s="T825">
                  <ts e="T826" id="Seg_3280" n="HIAT:w" s="T825">Udabə</ts>
                  <nts id="Seg_3281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3282" n="HIAT:ip">(</nts>
                  <ts e="T827" id="Seg_3284" n="HIAT:w" s="T826">kös-</ts>
                  <nts id="Seg_3285" n="HIAT:ip">)</nts>
                  <nts id="Seg_3286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_3288" n="HIAT:w" s="T827">kössi</ts>
                  <nts id="Seg_3289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_3291" n="HIAT:w" s="T828">bar</ts>
                  <nts id="Seg_3292" n="HIAT:ip">…</nts>
                  <nts id="Seg_3293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T836" id="Seg_3295" n="HIAT:u" s="T830">
                  <ts e="T831" id="Seg_3297" n="HIAT:w" s="T830">Döm</ts>
                  <nts id="Seg_3298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3300" n="HIAT:w" s="T831">ujam</ts>
                  <nts id="Seg_3301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_3303" n="HIAT:w" s="T832">amnəm</ts>
                  <nts id="Seg_3304" n="HIAT:ip">,</nts>
                  <nts id="Seg_3305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_3307" n="HIAT:w" s="T833">döm</ts>
                  <nts id="Seg_3308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_3310" n="HIAT:w" s="T834">ujam</ts>
                  <nts id="Seg_3311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_3313" n="HIAT:w" s="T835">amnəm</ts>
                  <nts id="Seg_3314" n="HIAT:ip">.</nts>
                  <nts id="Seg_3315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T840" id="Seg_3317" n="HIAT:u" s="T836">
                  <ts e="T837" id="Seg_3319" n="HIAT:w" s="T836">A</ts>
                  <nts id="Seg_3320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_3322" n="HIAT:w" s="T837">dĭ</ts>
                  <nts id="Seg_3323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_3325" n="HIAT:w" s="T838">barəʔluʔpi</ts>
                  <nts id="Seg_3326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_3328" n="HIAT:w" s="T839">dĭʔnə</ts>
                  <nts id="Seg_3329" n="HIAT:ip">.</nts>
                  <nts id="Seg_3330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T846" id="Seg_3332" n="HIAT:u" s="T840">
                  <ts e="T841" id="Seg_3334" n="HIAT:w" s="T840">Dĭ</ts>
                  <nts id="Seg_3335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_3337" n="HIAT:w" s="T841">kabarluʔpi:</ts>
                  <nts id="Seg_3338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_3340" n="HIAT:w" s="T842">Vot</ts>
                  <nts id="Seg_3341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_3343" n="HIAT:w" s="T843">kudaj</ts>
                  <nts id="Seg_3344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_3346" n="HIAT:w" s="T844">măna</ts>
                  <nts id="Seg_3347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_3349" n="HIAT:w" s="T845">mĭbi</ts>
                  <nts id="Seg_3350" n="HIAT:ip">!</nts>
                  <nts id="Seg_3351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T850" id="Seg_3353" n="HIAT:u" s="T846">
                  <ts e="T847" id="Seg_3355" n="HIAT:w" s="T846">Da</ts>
                  <nts id="Seg_3356" n="HIAT:ip">,</nts>
                  <nts id="Seg_3357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_3359" n="HIAT:w" s="T847">kudaj</ts>
                  <nts id="Seg_3360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_3362" n="HIAT:w" s="T848">tănan</ts>
                  <nts id="Seg_3363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_3365" n="HIAT:w" s="T849">mĭləj</ts>
                  <nts id="Seg_3366" n="HIAT:ip">!</nts>
                  <nts id="Seg_3367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T857" id="Seg_3369" n="HIAT:u" s="T850">
                  <ts e="T851" id="Seg_3371" n="HIAT:w" s="T850">Pi</ts>
                  <nts id="Seg_3372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_3374" n="HIAT:w" s="T851">tănan</ts>
                  <nts id="Seg_3375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_3377" n="HIAT:w" s="T852">mĭləj</ts>
                  <nts id="Seg_3378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_3380" n="HIAT:w" s="T853">kudaj</ts>
                  <nts id="Seg_3381" n="HIAT:ip">,</nts>
                  <nts id="Seg_3382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_3384" n="HIAT:w" s="T854">dĭm</ts>
                  <nts id="Seg_3385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_3387" n="HIAT:w" s="T855">măn</ts>
                  <nts id="Seg_3388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_3390" n="HIAT:w" s="T856">mĭbiem</ts>
                  <nts id="Seg_3391" n="HIAT:ip">.</nts>
                  <nts id="Seg_3392" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T861" id="Seg_3394" n="HIAT:u" s="T857">
                  <ts e="T858" id="Seg_3396" n="HIAT:w" s="T857">Dĭgəttə</ts>
                  <nts id="Seg_3397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T859" id="Seg_3399" n="HIAT:w" s="T858">dĭ</ts>
                  <nts id="Seg_3400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_3402" n="HIAT:w" s="T859">amorbi</ts>
                  <nts id="Seg_3403" n="HIAT:ip">,</nts>
                  <nts id="Seg_3404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_3406" n="HIAT:w" s="T860">kambiʔi</ts>
                  <nts id="Seg_3407" n="HIAT:ip">.</nts>
                  <nts id="Seg_3408" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T867" id="Seg_3410" n="HIAT:u" s="T861">
                  <ts e="T862" id="Seg_3412" n="HIAT:w" s="T861">Dĭn</ts>
                  <nts id="Seg_3413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_3415" n="HIAT:w" s="T862">ineʔi</ts>
                  <nts id="Seg_3416" n="HIAT:ip">,</nts>
                  <nts id="Seg_3417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_3419" n="HIAT:w" s="T863">tüžöjʔi</ts>
                  <nts id="Seg_3420" n="HIAT:ip">,</nts>
                  <nts id="Seg_3421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_3423" n="HIAT:w" s="T864">ularəʔi</ts>
                  <nts id="Seg_3424" n="HIAT:ip">,</nts>
                  <nts id="Seg_3425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_3427" n="HIAT:w" s="T865">šobiʔi</ts>
                  <nts id="Seg_3428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_3430" n="HIAT:w" s="T866">dĭbər</ts>
                  <nts id="Seg_3431" n="HIAT:ip">.</nts>
                  <nts id="Seg_3432" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T871" id="Seg_3434" n="HIAT:u" s="T867">
                  <ts e="T868" id="Seg_3436" n="HIAT:w" s="T867">Dĭ</ts>
                  <nts id="Seg_3437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_3439" n="HIAT:w" s="T868">măndə:</ts>
                  <nts id="Seg_3440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_3442" n="HIAT:w" s="T869">Pünörzittə</ts>
                  <nts id="Seg_3443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_3445" n="HIAT:w" s="T870">axota</ts>
                  <nts id="Seg_3446" n="HIAT:ip">!</nts>
                  <nts id="Seg_3447" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T876" id="Seg_3449" n="HIAT:u" s="T871">
                  <ts e="T872" id="Seg_3451" n="HIAT:w" s="T871">Iʔ</ts>
                  <nts id="Seg_3452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_3454" n="HIAT:w" s="T872">pünöraʔ</ts>
                  <nts id="Seg_3455" n="HIAT:ip">,</nts>
                  <nts id="Seg_3456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873.tx-PKZ.1" id="Seg_3458" n="HIAT:w" s="T873">a</ts>
                  <nts id="Seg_3459" n="HIAT:ip">_</nts>
                  <ts e="T874" id="Seg_3461" n="HIAT:w" s="T873.tx-PKZ.1">to</ts>
                  <nts id="Seg_3462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_3464" n="HIAT:w" s="T874">bar</ts>
                  <nts id="Seg_3465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_3467" n="HIAT:w" s="T875">nuʔməluʔləʔjə</ts>
                  <nts id="Seg_3468" n="HIAT:ip">!</nts>
                  <nts id="Seg_3469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T888" id="Seg_3471" n="HIAT:u" s="T876">
                  <ts e="T877" id="Seg_3473" n="HIAT:w" s="T876">Dĭ</ts>
                  <nts id="Seg_3474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877.tx-PKZ.1" id="Seg_3476" n="HIAT:w" s="T877">vsʼo</ts>
                  <nts id="Seg_3477" n="HIAT:ip">_</nts>
                  <ts e="T880" id="Seg_3479" n="HIAT:w" s="T877.tx-PKZ.1">taki</ts>
                  <nts id="Seg_3480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T881" id="Seg_3482" n="HIAT:w" s="T880">pünörbi</ts>
                  <nts id="Seg_3483" n="HIAT:ip">,</nts>
                  <nts id="Seg_3484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_3486" n="HIAT:w" s="T881">i</ts>
                  <nts id="Seg_3487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_3489" n="HIAT:w" s="T882">bar</ts>
                  <nts id="Seg_3490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3491" n="HIAT:ip">(</nts>
                  <ts e="T884" id="Seg_3493" n="HIAT:w" s="T883">inezeŋdə</ts>
                  <nts id="Seg_3494" n="HIAT:ip">)</nts>
                  <nts id="Seg_3495" n="HIAT:ip">,</nts>
                  <nts id="Seg_3496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_3498" n="HIAT:w" s="T884">ularzaŋdə</ts>
                  <nts id="Seg_3499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_3501" n="HIAT:w" s="T885">i</ts>
                  <nts id="Seg_3502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887" id="Seg_3504" n="HIAT:w" s="T886">tüžöjʔjə</ts>
                  <nts id="Seg_3505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T888" id="Seg_3507" n="HIAT:w" s="T887">üʔməluʔpiʔi</ts>
                  <nts id="Seg_3508" n="HIAT:ip">.</nts>
                  <nts id="Seg_3509" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T892" id="Seg_3511" n="HIAT:u" s="T888">
                  <ts e="T889" id="Seg_3513" n="HIAT:w" s="T888">Dĭ</ts>
                  <nts id="Seg_3514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_3516" n="HIAT:w" s="T889">davaj</ts>
                  <nts id="Seg_3517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T892" id="Seg_3519" n="HIAT:w" s="T890">sütsi</ts>
                  <nts id="Seg_3520" n="HIAT:ip">…</nts>
                  <nts id="Seg_3521" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T893" id="Seg_3523" n="HIAT:u" s="T892">
                  <nts id="Seg_3524" n="HIAT:ip">(</nts>
                  <nts id="Seg_3525" n="HIAT:ip">(</nts>
                  <ats e="T893" id="Seg_3526" n="HIAT:non-pho" s="T892">BRK</ats>
                  <nts id="Seg_3527" n="HIAT:ip">)</nts>
                  <nts id="Seg_3528" n="HIAT:ip">)</nts>
                  <nts id="Seg_3529" n="HIAT:ip">.</nts>
                  <nts id="Seg_3530" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T897" id="Seg_3532" n="HIAT:u" s="T893">
                  <nts id="Seg_3533" n="HIAT:ip">(</nts>
                  <ts e="T894" id="Seg_3535" n="HIAT:w" s="T893">süt-</ts>
                  <nts id="Seg_3536" n="HIAT:ip">)</nts>
                  <nts id="Seg_3537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_3539" n="HIAT:w" s="T894">sütsi</ts>
                  <nts id="Seg_3540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_3542" n="HIAT:w" s="T895">kămnasʼtə</ts>
                  <nts id="Seg_3543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_3545" n="HIAT:w" s="T896">dĭzeŋdə</ts>
                  <nts id="Seg_3546" n="HIAT:ip">.</nts>
                  <nts id="Seg_3547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T901" id="Seg_3549" n="HIAT:u" s="T897">
                  <ts e="T898" id="Seg_3551" n="HIAT:w" s="T897">Ular</ts>
                  <nts id="Seg_3552" n="HIAT:ip">,</nts>
                  <nts id="Seg_3553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_3555" n="HIAT:w" s="T898">šiʔ</ts>
                  <nts id="Seg_3556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_3558" n="HIAT:w" s="T899">poʔto</ts>
                  <nts id="Seg_3559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_3561" n="HIAT:w" s="T900">molaʔ</ts>
                  <nts id="Seg_3562" n="HIAT:ip">!</nts>
                  <nts id="Seg_3563" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T906" id="Seg_3565" n="HIAT:u" s="T901">
                  <ts e="T902" id="Seg_3567" n="HIAT:w" s="T901">Tüžöjəʔi</ts>
                  <nts id="Seg_3568" n="HIAT:ip">,</nts>
                  <nts id="Seg_3569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3570" n="HIAT:ip">(</nts>
                  <ts e="T903" id="Seg_3572" n="HIAT:w" s="T902">šiʔ=</ts>
                  <nts id="Seg_3573" n="HIAT:ip">)</nts>
                  <nts id="Seg_3574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T904" id="Seg_3576" n="HIAT:w" s="T903">šiʔ</ts>
                  <nts id="Seg_3577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T905" id="Seg_3579" n="HIAT:w" s="T904">pulan</ts>
                  <nts id="Seg_3580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_3582" n="HIAT:w" s="T905">mogaʔ</ts>
                  <nts id="Seg_3583" n="HIAT:ip">!</nts>
                  <nts id="Seg_3584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T910" id="Seg_3586" n="HIAT:u" s="T906">
                  <ts e="T907" id="Seg_3588" n="HIAT:w" s="T906">A</ts>
                  <nts id="Seg_3589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_3591" n="HIAT:w" s="T907">šiʔ</ts>
                  <nts id="Seg_3592" n="HIAT:ip">,</nts>
                  <nts id="Seg_3593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_3595" n="HIAT:w" s="T908">ineʔi</ts>
                  <nts id="Seg_3596" n="HIAT:ip">…</nts>
                  <nts id="Seg_3597" n="HIAT:ip">"</nts>
                  <nts id="Seg_3598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T911" id="Seg_3600" n="HIAT:u" s="T910">
                  <ts e="T911" id="Seg_3602" n="HIAT:w" s="T910">Nöməlluʔpiem</ts>
                  <nts id="Seg_3603" n="HIAT:ip">.</nts>
                  <nts id="Seg_3604" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T912" id="Seg_3606" n="HIAT:u" s="T911">
                  <nts id="Seg_3607" n="HIAT:ip">(</nts>
                  <nts id="Seg_3608" n="HIAT:ip">(</nts>
                  <ats e="T912" id="Seg_3609" n="HIAT:non-pho" s="T911">BRK</ats>
                  <nts id="Seg_3610" n="HIAT:ip">)</nts>
                  <nts id="Seg_3611" n="HIAT:ip">)</nts>
                  <nts id="Seg_3612" n="HIAT:ip">.</nts>
                  <nts id="Seg_3613" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T918" id="Seg_3615" n="HIAT:u" s="T912">
                  <nts id="Seg_3616" n="HIAT:ip">"</nts>
                  <ts e="T913" id="Seg_3618" n="HIAT:w" s="T912">Ineʔi</ts>
                  <nts id="Seg_3619" n="HIAT:ip">,</nts>
                  <nts id="Seg_3620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3621" n="HIAT:ip">(</nts>
                  <ts e="T914" id="Seg_3623" n="HIAT:w" s="T913">măm-</ts>
                  <nts id="Seg_3624" n="HIAT:ip">)</nts>
                  <nts id="Seg_3625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T915" id="Seg_3627" n="HIAT:w" s="T914">mămbi</ts>
                  <nts id="Seg_3628" n="HIAT:ip">,</nts>
                  <nts id="Seg_3629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_3631" n="HIAT:w" s="T915">pušaj</ts>
                  <nts id="Seg_3632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T917" id="Seg_3634" n="HIAT:w" s="T916">izʼubrʼaʔi</ts>
                  <nts id="Seg_3635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T918" id="Seg_3637" n="HIAT:w" s="T917">moluʔjəʔ</ts>
                  <nts id="Seg_3638" n="HIAT:ip">.</nts>
                  <nts id="Seg_3639" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T927" id="Seg_3641" n="HIAT:u" s="T918">
                  <ts e="T919" id="Seg_3643" n="HIAT:w" s="T918">Dĭgəttə</ts>
                  <nts id="Seg_3644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T920" id="Seg_3646" n="HIAT:w" s="T919">nüke</ts>
                  <nts id="Seg_3647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T921" id="Seg_3649" n="HIAT:w" s="T920">i</ts>
                  <nts id="Seg_3650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_3652" n="HIAT:w" s="T921">büzʼe</ts>
                  <nts id="Seg_3653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_3655" n="HIAT:w" s="T922">baʔluʔbi</ts>
                  <nts id="Seg_3656" n="HIAT:ip">,</nts>
                  <nts id="Seg_3657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T924" id="Seg_3659" n="HIAT:w" s="T923">ĭmbidə</ts>
                  <nts id="Seg_3660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T925" id="Seg_3662" n="HIAT:w" s="T924">bazoʔ</ts>
                  <nts id="Seg_3663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T926" id="Seg_3665" n="HIAT:w" s="T925">naga</ts>
                  <nts id="Seg_3666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T927" id="Seg_3668" n="HIAT:w" s="T926">dĭzeŋdə</ts>
                  <nts id="Seg_3669" n="HIAT:ip">.</nts>
                  <nts id="Seg_3670" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1071" id="Seg_3672" n="HIAT:u" s="T927">
                  <ts e="T928" id="Seg_3674" n="HIAT:w" s="T927">Bar</ts>
                  <nts id="Seg_3675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1073" id="Seg_3677" n="HIAT:w" s="T928">kalla</ts>
                  <nts id="Seg_3678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1071" id="Seg_3680" n="HIAT:w" s="T1073">dʼürbiʔi</ts>
                  <nts id="Seg_3681" n="HIAT:ip">.</nts>
                  <nts id="Seg_3682" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T935" id="Seg_3684" n="HIAT:u" s="T1071">
                  <nts id="Seg_3685" n="HIAT:ip">(</nts>
                  <nts id="Seg_3686" n="HIAT:ip">(</nts>
                  <ats e="T929" id="Seg_3687" n="HIAT:non-pho" s="T1071">BRK</ats>
                  <nts id="Seg_3688" n="HIAT:ip">)</nts>
                  <nts id="Seg_3689" n="HIAT:ip">)</nts>
                  <nts id="Seg_3690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_3692" n="HIAT:w" s="T929">Amnobi</ts>
                  <nts id="Seg_3693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T931" id="Seg_3695" n="HIAT:w" s="T930">büzʼe</ts>
                  <nts id="Seg_3696" n="HIAT:ip">,</nts>
                  <nts id="Seg_3697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T932" id="Seg_3699" n="HIAT:w" s="T931">dĭn</ts>
                  <nts id="Seg_3700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T933" id="Seg_3702" n="HIAT:w" s="T932">nagur</ts>
                  <nts id="Seg_3703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T934" id="Seg_3705" n="HIAT:w" s="T933">koʔbdot</ts>
                  <nts id="Seg_3706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_3708" n="HIAT:w" s="T934">ibi</ts>
                  <nts id="Seg_3709" n="HIAT:ip">.</nts>
                  <nts id="Seg_3710" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T940" id="Seg_3712" n="HIAT:u" s="T935">
                  <ts e="T936" id="Seg_3714" n="HIAT:w" s="T935">Dĭm</ts>
                  <nts id="Seg_3715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T937" id="Seg_3717" n="HIAT:w" s="T936">kăštəbiʔi</ts>
                  <nts id="Seg_3718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1075" id="Seg_3720" n="HIAT:w" s="T937">Kazan</ts>
                  <nts id="Seg_3721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T938" id="Seg_3723" n="HIAT:w" s="T1075">turanə</ts>
                  <nts id="Seg_3724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T939" id="Seg_3726" n="HIAT:w" s="T938">štobɨ</ts>
                  <nts id="Seg_3727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T940" id="Seg_3729" n="HIAT:w" s="T939">šobi</ts>
                  <nts id="Seg_3730" n="HIAT:ip">.</nts>
                  <nts id="Seg_3731" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T945" id="Seg_3733" n="HIAT:u" s="T940">
                  <ts e="T941" id="Seg_3735" n="HIAT:w" s="T940">A</ts>
                  <nts id="Seg_3736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T942" id="Seg_3738" n="HIAT:w" s="T941">urgo</ts>
                  <nts id="Seg_3739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_3741" n="HIAT:w" s="T942">koʔbdot:</ts>
                  <nts id="Seg_3742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T944" id="Seg_3744" n="HIAT:w" s="T943">Măn</ts>
                  <nts id="Seg_3745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T945" id="Seg_3747" n="HIAT:w" s="T944">kalam</ts>
                  <nts id="Seg_3748" n="HIAT:ip">!</nts>
                  <nts id="Seg_3749" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T948" id="Seg_3751" n="HIAT:u" s="T945">
                  <ts e="T946" id="Seg_3753" n="HIAT:w" s="T945">Eʔbdəbə</ts>
                  <nts id="Seg_3754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3755" n="HIAT:ip">(</nts>
                  <ts e="T947" id="Seg_3757" n="HIAT:w" s="T946">bă-</ts>
                  <nts id="Seg_3758" n="HIAT:ip">)</nts>
                  <nts id="Seg_3759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_3761" n="HIAT:w" s="T947">băʔpi</ts>
                  <nts id="Seg_3762" n="HIAT:ip">.</nts>
                  <nts id="Seg_3763" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T950" id="Seg_3765" n="HIAT:u" s="T948">
                  <ts e="T949" id="Seg_3767" n="HIAT:w" s="T948">Piʔmə</ts>
                  <nts id="Seg_3768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T950" id="Seg_3770" n="HIAT:w" s="T949">šerbi</ts>
                  <nts id="Seg_3771" n="HIAT:ip">.</nts>
                  <nts id="Seg_3772" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T952" id="Seg_3774" n="HIAT:u" s="T950">
                  <ts e="T951" id="Seg_3776" n="HIAT:w" s="T950">Kujnek</ts>
                  <nts id="Seg_3777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T952" id="Seg_3779" n="HIAT:w" s="T951">šerbi</ts>
                  <nts id="Seg_3780" n="HIAT:ip">.</nts>
                  <nts id="Seg_3781" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T955" id="Seg_3783" n="HIAT:u" s="T952">
                  <ts e="T953" id="Seg_3785" n="HIAT:w" s="T952">Üžü</ts>
                  <nts id="Seg_3786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T954" id="Seg_3788" n="HIAT:w" s="T953">šerbi</ts>
                  <nts id="Seg_3789" n="HIAT:ip">,</nts>
                  <nts id="Seg_3790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T955" id="Seg_3792" n="HIAT:w" s="T954">kambi</ts>
                  <nts id="Seg_3793" n="HIAT:ip">.</nts>
                  <nts id="Seg_3794" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T959" id="Seg_3796" n="HIAT:u" s="T955">
                  <ts e="T956" id="Seg_3798" n="HIAT:w" s="T955">A</ts>
                  <nts id="Seg_3799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T957" id="Seg_3801" n="HIAT:w" s="T956">abat</ts>
                  <nts id="Seg_3802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T958" id="Seg_3804" n="HIAT:w" s="T957">urgaːbazi</ts>
                  <nts id="Seg_3805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T959" id="Seg_3807" n="HIAT:w" s="T958">abi</ts>
                  <nts id="Seg_3808" n="HIAT:ip">.</nts>
                  <nts id="Seg_3809" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T962" id="Seg_3811" n="HIAT:u" s="T959">
                  <ts e="T960" id="Seg_3813" n="HIAT:w" s="T959">I</ts>
                  <nts id="Seg_3814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T961" id="Seg_3816" n="HIAT:w" s="T960">dĭʔnə</ts>
                  <nts id="Seg_3817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T962" id="Seg_3819" n="HIAT:w" s="T961">šonəga</ts>
                  <nts id="Seg_3820" n="HIAT:ip">.</nts>
                  <nts id="Seg_3821" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T968" id="Seg_3823" n="HIAT:u" s="T962">
                  <ts e="T963" id="Seg_3825" n="HIAT:w" s="T962">Dĭ</ts>
                  <nts id="Seg_3826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964" id="Seg_3828" n="HIAT:w" s="T963">bar</ts>
                  <nts id="Seg_3829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_3831" n="HIAT:w" s="T964">nereʔluʔpi</ts>
                  <nts id="Seg_3832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T966" id="Seg_3834" n="HIAT:w" s="T965">i</ts>
                  <nts id="Seg_3835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T967" id="Seg_3837" n="HIAT:w" s="T966">maʔndə</ts>
                  <nts id="Seg_3838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T968" id="Seg_3840" n="HIAT:w" s="T967">nuʔməluʔpi</ts>
                  <nts id="Seg_3841" n="HIAT:ip">.</nts>
                  <nts id="Seg_3842" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T973" id="Seg_3844" n="HIAT:u" s="T968">
                  <ts e="T969" id="Seg_3846" n="HIAT:w" s="T968">Dĭ</ts>
                  <nts id="Seg_3847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T970" id="Seg_3849" n="HIAT:w" s="T969">šobi:</ts>
                  <nts id="Seg_3850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T971" id="Seg_3852" n="HIAT:w" s="T970">Ĭmbi</ts>
                  <nts id="Seg_3853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T972" id="Seg_3855" n="HIAT:w" s="T971">ej</ts>
                  <nts id="Seg_3856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T973" id="Seg_3858" n="HIAT:w" s="T972">kambiam</ts>
                  <nts id="Seg_3859" n="HIAT:ip">?</nts>
                  <nts id="Seg_3860" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T980" id="Seg_3862" n="HIAT:u" s="T973">
                  <nts id="Seg_3863" n="HIAT:ip">(</nts>
                  <ts e="T974" id="Seg_3865" n="HIAT:w" s="T973">Там=</ts>
                  <nts id="Seg_3866" n="HIAT:ip">)</nts>
                  <nts id="Seg_3867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T975" id="Seg_3869" n="HIAT:w" s="T974">Dĭn</ts>
                  <nts id="Seg_3870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T976" id="Seg_3872" n="HIAT:w" s="T975">urgaːba</ts>
                  <nts id="Seg_3873" n="HIAT:ip">,</nts>
                  <nts id="Seg_3874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T977" id="Seg_3876" n="HIAT:w" s="T976">măn</ts>
                  <nts id="Seg_3877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T978" id="Seg_3879" n="HIAT:w" s="T977">nereʔluʔpiem</ts>
                  <nts id="Seg_3880" n="HIAT:ip">,</nts>
                  <nts id="Seg_3881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T979" id="Seg_3883" n="HIAT:w" s="T978">maʔndə</ts>
                  <nts id="Seg_3884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T980" id="Seg_3886" n="HIAT:w" s="T979">šobiam</ts>
                  <nts id="Seg_3887" n="HIAT:ip">.</nts>
                  <nts id="Seg_3888" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T985" id="Seg_3890" n="HIAT:u" s="T980">
                  <ts e="T981" id="Seg_3892" n="HIAT:w" s="T980">Dĭgəttə</ts>
                  <nts id="Seg_3893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T982" id="Seg_3895" n="HIAT:w" s="T981">baška</ts>
                  <nts id="Seg_3896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T983" id="Seg_3898" n="HIAT:w" s="T982">koʔbdo:</ts>
                  <nts id="Seg_3899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T984" id="Seg_3901" n="HIAT:w" s="T983">Măn</ts>
                  <nts id="Seg_3902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T985" id="Seg_3904" n="HIAT:w" s="T984">kalam</ts>
                  <nts id="Seg_3905" n="HIAT:ip">!</nts>
                  <nts id="Seg_3906" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T987" id="Seg_3908" n="HIAT:u" s="T985">
                  <ts e="T986" id="Seg_3910" n="HIAT:w" s="T985">No</ts>
                  <nts id="Seg_3911" n="HIAT:ip">,</nts>
                  <nts id="Seg_3912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T987" id="Seg_3914" n="HIAT:w" s="T986">kanaʔ</ts>
                  <nts id="Seg_3915" n="HIAT:ip">.</nts>
                  <nts id="Seg_3916" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T991" id="Seg_3918" n="HIAT:u" s="T987">
                  <ts e="T988" id="Seg_3920" n="HIAT:w" s="T987">Šerbi</ts>
                  <nts id="Seg_3921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T989" id="Seg_3923" n="HIAT:w" s="T988">piʔməʔi</ts>
                  <nts id="Seg_3924" n="HIAT:ip">,</nts>
                  <nts id="Seg_3925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T990" id="Seg_3927" n="HIAT:w" s="T989">kujnek</ts>
                  <nts id="Seg_3928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T991" id="Seg_3930" n="HIAT:w" s="T990">šerbi</ts>
                  <nts id="Seg_3931" n="HIAT:ip">.</nts>
                  <nts id="Seg_3932" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T993" id="Seg_3934" n="HIAT:u" s="T991">
                  <ts e="T992" id="Seg_3936" n="HIAT:w" s="T991">Eʔbdəbə</ts>
                  <nts id="Seg_3937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T993" id="Seg_3939" n="HIAT:w" s="T992">băʔpi</ts>
                  <nts id="Seg_3940" n="HIAT:ip">.</nts>
                  <nts id="Seg_3941" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T996" id="Seg_3943" n="HIAT:u" s="T993">
                  <ts e="T994" id="Seg_3945" n="HIAT:w" s="T993">Üžübə</ts>
                  <nts id="Seg_3946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T995" id="Seg_3948" n="HIAT:w" s="T994">šerbi</ts>
                  <nts id="Seg_3949" n="HIAT:ip">,</nts>
                  <nts id="Seg_3950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T996" id="Seg_3952" n="HIAT:w" s="T995">kambi</ts>
                  <nts id="Seg_3953" n="HIAT:ip">.</nts>
                  <nts id="Seg_3954" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1001" id="Seg_3956" n="HIAT:u" s="T996">
                  <ts e="T997" id="Seg_3958" n="HIAT:w" s="T996">Dĭgəttə</ts>
                  <nts id="Seg_3959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T998" id="Seg_3961" n="HIAT:w" s="T997">abat</ts>
                  <nts id="Seg_3962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T999" id="Seg_3964" n="HIAT:w" s="T998">molaːmbi</ts>
                  <nts id="Seg_3965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1000" id="Seg_3967" n="HIAT:w" s="T999">urgaːbazi</ts>
                  <nts id="Seg_3968" n="HIAT:ip">,</nts>
                  <nts id="Seg_3969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1001" id="Seg_3971" n="HIAT:w" s="T1000">kambi</ts>
                  <nts id="Seg_3972" n="HIAT:ip">.</nts>
                  <nts id="Seg_3973" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1006" id="Seg_3975" n="HIAT:u" s="T1001">
                  <ts e="T1002" id="Seg_3977" n="HIAT:w" s="T1001">Dĭ</ts>
                  <nts id="Seg_3978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1003" id="Seg_3980" n="HIAT:w" s="T1002">bar</ts>
                  <nts id="Seg_3981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1004" id="Seg_3983" n="HIAT:w" s="T1003">nereʔluʔpi</ts>
                  <nts id="Seg_3984" n="HIAT:ip">,</nts>
                  <nts id="Seg_3985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1005" id="Seg_3987" n="HIAT:w" s="T1004">maʔndə</ts>
                  <nts id="Seg_3988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1006" id="Seg_3990" n="HIAT:w" s="T1005">šobi</ts>
                  <nts id="Seg_3991" n="HIAT:ip">.</nts>
                  <nts id="Seg_3992" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1008" id="Seg_3994" n="HIAT:u" s="T1006">
                  <ts e="T1007" id="Seg_3996" n="HIAT:w" s="T1006">Ĭmbi</ts>
                  <nts id="Seg_3997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1008" id="Seg_3999" n="HIAT:w" s="T1007">šobial</ts>
                  <nts id="Seg_4000" n="HIAT:ip">?</nts>
                  <nts id="Seg_4001" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1016" id="Seg_4003" n="HIAT:u" s="T1008">
                  <ts e="T1009" id="Seg_4005" n="HIAT:w" s="T1008">Da</ts>
                  <nts id="Seg_4006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1010" id="Seg_4008" n="HIAT:w" s="T1009">dĭn</ts>
                  <nts id="Seg_4009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4010" n="HIAT:ip">(</nts>
                  <ts e="T1011" id="Seg_4012" n="HIAT:w" s="T1010">urgaːba</ts>
                  <nts id="Seg_4013" n="HIAT:ip">)</nts>
                  <nts id="Seg_4014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1012" id="Seg_4016" n="HIAT:w" s="T1011">šonəga</ts>
                  <nts id="Seg_4017" n="HIAT:ip">,</nts>
                  <nts id="Seg_4018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1013" id="Seg_4020" n="HIAT:w" s="T1012">măn</ts>
                  <nts id="Seg_4021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1014" id="Seg_4023" n="HIAT:w" s="T1013">nereʔluʔpiem</ts>
                  <nts id="Seg_4024" n="HIAT:ip">,</nts>
                  <nts id="Seg_4025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1015" id="Seg_4027" n="HIAT:w" s="T1014">maʔndə</ts>
                  <nts id="Seg_4028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1016" id="Seg_4030" n="HIAT:w" s="T1015">nuʔməluʔpiem</ts>
                  <nts id="Seg_4031" n="HIAT:ip">.</nts>
                  <nts id="Seg_4032" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1017" id="Seg_4034" n="HIAT:u" s="T1016">
                  <ts e="T1017" id="Seg_4036" n="HIAT:w" s="T1016">Kabarləj</ts>
                  <nts id="Seg_4037" n="HIAT:ip">.</nts>
                  <nts id="Seg_4038" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1018" id="Seg_4040" n="HIAT:u" s="T1017">
                  <nts id="Seg_4041" n="HIAT:ip">(</nts>
                  <nts id="Seg_4042" n="HIAT:ip">(</nts>
                  <ats e="T1018" id="Seg_4043" n="HIAT:non-pho" s="T1017">BRK</ats>
                  <nts id="Seg_4044" n="HIAT:ip">)</nts>
                  <nts id="Seg_4045" n="HIAT:ip">)</nts>
                  <nts id="Seg_4046" n="HIAT:ip">.</nts>
                  <nts id="Seg_4047" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1023" id="Seg_4049" n="HIAT:u" s="T1018">
                  <ts e="T1019" id="Seg_4051" n="HIAT:w" s="T1018">Dĭgəttə</ts>
                  <nts id="Seg_4052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1020" id="Seg_4054" n="HIAT:w" s="T1019">üdʼüge</ts>
                  <nts id="Seg_4055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1021" id="Seg_4057" n="HIAT:w" s="T1020">koʔbdo:</ts>
                  <nts id="Seg_4058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1022" id="Seg_4060" n="HIAT:w" s="T1021">Măn</ts>
                  <nts id="Seg_4061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1023" id="Seg_4063" n="HIAT:w" s="T1022">kalam</ts>
                  <nts id="Seg_4064" n="HIAT:ip">!</nts>
                  <nts id="Seg_4065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1025" id="Seg_4067" n="HIAT:u" s="T1023">
                  <ts e="T1024" id="Seg_4069" n="HIAT:w" s="T1023">No</ts>
                  <nts id="Seg_4070" n="HIAT:ip">,</nts>
                  <nts id="Seg_4071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1025" id="Seg_4073" n="HIAT:w" s="T1024">kanaʔ</ts>
                  <nts id="Seg_4074" n="HIAT:ip">.</nts>
                  <nts id="Seg_4075" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1030" id="Seg_4077" n="HIAT:u" s="T1025">
                  <ts e="T1026" id="Seg_4079" n="HIAT:w" s="T1025">Tože</ts>
                  <nts id="Seg_4080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1027" id="Seg_4082" n="HIAT:w" s="T1026">dăre</ts>
                  <nts id="Seg_4083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1028" id="Seg_4085" n="HIAT:w" s="T1027">že</ts>
                  <nts id="Seg_4086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1029" id="Seg_4088" n="HIAT:w" s="T1028">eʔbdəbə</ts>
                  <nts id="Seg_4089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1030" id="Seg_4091" n="HIAT:w" s="T1029">bătluʔpi</ts>
                  <nts id="Seg_4092" n="HIAT:ip">.</nts>
                  <nts id="Seg_4093" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1032" id="Seg_4095" n="HIAT:u" s="T1030">
                  <ts e="T1031" id="Seg_4097" n="HIAT:w" s="T1030">Piʔməʔi</ts>
                  <nts id="Seg_4098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1032" id="Seg_4100" n="HIAT:w" s="T1031">šerbi</ts>
                  <nts id="Seg_4101" n="HIAT:ip">.</nts>
                  <nts id="Seg_4102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1034" id="Seg_4104" n="HIAT:u" s="T1032">
                  <ts e="T1033" id="Seg_4106" n="HIAT:w" s="T1032">Kujnek</ts>
                  <nts id="Seg_4107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1034" id="Seg_4109" n="HIAT:w" s="T1033">šerbi</ts>
                  <nts id="Seg_4110" n="HIAT:ip">.</nts>
                  <nts id="Seg_4111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1038" id="Seg_4113" n="HIAT:u" s="T1034">
                  <ts e="T1035" id="Seg_4115" n="HIAT:w" s="T1034">I</ts>
                  <nts id="Seg_4116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1036" id="Seg_4118" n="HIAT:w" s="T1035">üžü</ts>
                  <nts id="Seg_4119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1037" id="Seg_4121" n="HIAT:w" s="T1036">šerbi</ts>
                  <nts id="Seg_4122" n="HIAT:ip">,</nts>
                  <nts id="Seg_4123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1038" id="Seg_4125" n="HIAT:w" s="T1037">kambi</ts>
                  <nts id="Seg_4126" n="HIAT:ip">.</nts>
                  <nts id="Seg_4127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1040" id="Seg_4129" n="HIAT:u" s="T1038">
                  <ts e="T1039" id="Seg_4131" n="HIAT:w" s="T1038">Kandəga</ts>
                  <nts id="Seg_4132" n="HIAT:ip">,</nts>
                  <nts id="Seg_4133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1040" id="Seg_4135" n="HIAT:w" s="T1039">kandəga</ts>
                  <nts id="Seg_4136" n="HIAT:ip">.</nts>
                  <nts id="Seg_4137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1042" id="Seg_4139" n="HIAT:u" s="T1040">
                  <ts e="T1041" id="Seg_4141" n="HIAT:w" s="T1040">Urgaːba</ts>
                  <nts id="Seg_4142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1042" id="Seg_4144" n="HIAT:w" s="T1041">šonəga</ts>
                  <nts id="Seg_4145" n="HIAT:ip">.</nts>
                  <nts id="Seg_4146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1051" id="Seg_4148" n="HIAT:u" s="T1042">
                  <ts e="T1043" id="Seg_4150" n="HIAT:w" s="T1042">Dĭ</ts>
                  <nts id="Seg_4151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1044" id="Seg_4153" n="HIAT:w" s="T1043">strelazi</ts>
                  <nts id="Seg_4154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1045" id="Seg_4156" n="HIAT:w" s="T1044">bar</ts>
                  <nts id="Seg_4157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1046" id="Seg_4159" n="HIAT:w" s="T1045">dĭm</ts>
                  <nts id="Seg_4160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4161" n="HIAT:ip">(</nts>
                  <ts e="T1047" id="Seg_4163" n="HIAT:w" s="T1046">ku-</ts>
                  <nts id="Seg_4164" n="HIAT:ip">)</nts>
                  <nts id="Seg_4165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4166" n="HIAT:ip">(</nts>
                  <ts e="T1048" id="Seg_4168" n="HIAT:w" s="T1047">simandə=</ts>
                  <nts id="Seg_4169" n="HIAT:ip">)</nts>
                  <nts id="Seg_4170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1049" id="Seg_4172" n="HIAT:w" s="T1048">simat</ts>
                  <nts id="Seg_4173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1051" id="Seg_4175" n="HIAT:w" s="T1049">bar</ts>
                  <nts id="Seg_4176" n="HIAT:ip">…</nts>
                  <nts id="Seg_4177" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1059" id="Seg_4179" n="HIAT:u" s="T1051">
                  <ts e="T1052" id="Seg_4181" n="HIAT:w" s="T1051">Dĭ</ts>
                  <nts id="Seg_4182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1053" id="Seg_4184" n="HIAT:w" s="T1052">strelazi</ts>
                  <nts id="Seg_4185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1054" id="Seg_4187" n="HIAT:w" s="T1053">simandə</ts>
                  <nts id="Seg_4188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1055" id="Seg_4190" n="HIAT:w" s="T1054">toʔnarbi</ts>
                  <nts id="Seg_4191" n="HIAT:ip">,</nts>
                  <nts id="Seg_4192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1056" id="Seg_4194" n="HIAT:w" s="T1055">i</ts>
                  <nts id="Seg_4195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1057" id="Seg_4197" n="HIAT:w" s="T1056">simat</ts>
                  <nts id="Seg_4198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4199" n="HIAT:ip">(</nts>
                  <ts e="T1058" id="Seg_4201" n="HIAT:w" s="T1057">naga-</ts>
                  <nts id="Seg_4202" n="HIAT:ip">)</nts>
                  <nts id="Seg_4203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1059" id="Seg_4205" n="HIAT:w" s="T1058">nagoluʔpi</ts>
                  <nts id="Seg_4206" n="HIAT:ip">.</nts>
                  <nts id="Seg_4207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1062" id="Seg_4209" n="HIAT:u" s="T1059">
                  <ts e="T1060" id="Seg_4211" n="HIAT:w" s="T1059">Dĭgəttə</ts>
                  <nts id="Seg_4212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1061" id="Seg_4214" n="HIAT:w" s="T1060">kambi</ts>
                  <nts id="Seg_4215" n="HIAT:ip">,</nts>
                  <nts id="Seg_4216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1062" id="Seg_4218" n="HIAT:w" s="T1061">kambi</ts>
                  <nts id="Seg_4219" n="HIAT:ip">.</nts>
                  <nts id="Seg_4220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1069" id="Seg_4222" n="HIAT:u" s="T1062">
                  <ts e="T1063" id="Seg_4224" n="HIAT:w" s="T1062">Šonəga</ts>
                  <nts id="Seg_4225" n="HIAT:ip">,</nts>
                  <nts id="Seg_4226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1064" id="Seg_4228" n="HIAT:w" s="T1063">nüke</ts>
                  <nts id="Seg_4229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1065" id="Seg_4231" n="HIAT:w" s="T1064">amnolaʔbə</ts>
                  <nts id="Seg_4232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1066" id="Seg_4234" n="HIAT:w" s="T1065">i</ts>
                  <nts id="Seg_4235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1067" id="Seg_4237" n="HIAT:w" s="T1066">nüken</ts>
                  <nts id="Seg_4238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1068" id="Seg_4240" n="HIAT:w" s="T1067">nʼit</ts>
                  <nts id="Seg_4241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1069" id="Seg_4243" n="HIAT:w" s="T1068">amnolaʔbə</ts>
                  <nts id="Seg_4244" n="HIAT:ip">.</nts>
                  <nts id="Seg_4245" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PKZ">
            <ts e="T1069" id="Seg_4246" n="sc" s="T0">
               <ts e="T1" id="Seg_4248" n="e" s="T0">Miʔ </ts>
               <ts e="T2" id="Seg_4250" n="e" s="T1">kamen </ts>
               <ts e="T3" id="Seg_4252" n="e" s="T2">šobibaʔ </ts>
               <ts e="T4" id="Seg_4254" n="e" s="T3">Abalakoftə. </ts>
               <ts e="T5" id="Seg_4256" n="e" s="T4">Miʔnʼibeʔ </ts>
               <ts e="T6" id="Seg_4258" n="e" s="T5">jakše </ts>
               <ts e="T7" id="Seg_4260" n="e" s="T6">ibi </ts>
               <ts e="T8" id="Seg_4262" n="e" s="T7">il. </ts>
               <ts e="T9" id="Seg_4264" n="e" s="T8">Šaːbibaʔ </ts>
               <ts e="T10" id="Seg_4266" n="e" s="T9">jakše. </ts>
               <ts e="T11" id="Seg_4268" n="e" s="T10">Dĭgəttə </ts>
               <ts e="T12" id="Seg_4270" n="e" s="T11">mĭmbibeʔ, </ts>
               <ts e="T13" id="Seg_4272" n="e" s="T12">mĭmbibeʔ. </ts>
               <ts e="T14" id="Seg_4274" n="e" s="T13">(Koj- </ts>
               <ts e="T15" id="Seg_4276" n="e" s="T14">kojak-) </ts>
               <ts e="T16" id="Seg_4278" n="e" s="T15">girgidəʔi </ts>
               <ts e="T17" id="Seg_4280" n="e" s="T16">büzʼeʔi </ts>
               <ts e="T18" id="Seg_4282" n="e" s="T17">kubibaʔ. </ts>
               <ts e="T19" id="Seg_4284" n="e" s="T18">Dʼăbaktərbibaʔ, </ts>
               <ts e="T20" id="Seg_4286" n="e" s="T19">dĭzeŋ </ts>
               <ts e="T21" id="Seg_4288" n="e" s="T20">ĭmbidə </ts>
               <ts e="T22" id="Seg_4290" n="e" s="T21">ej </ts>
               <ts e="T23" id="Seg_4292" n="e" s="T22">tĭmneʔi, </ts>
               <ts e="T24" id="Seg_4294" n="e" s="T23">nuzaŋ. </ts>
               <ts e="T25" id="Seg_4296" n="e" s="T24">Nuzaŋ </ts>
               <ts e="T26" id="Seg_4298" n="e" s="T25">il. </ts>
               <ts e="T27" id="Seg_4300" n="e" s="T26">Kăde </ts>
               <ts e="T28" id="Seg_4302" n="e" s="T27">dĭzeŋ </ts>
               <ts e="T29" id="Seg_4304" n="e" s="T28">amnolaʔbəʔjə! </ts>
               <ts e="T30" id="Seg_4306" n="e" s="T29">Miʔ </ts>
               <ts e="T31" id="Seg_4308" n="e" s="T30">tenəluʔpibeʔ </ts>
               <ts e="T32" id="Seg_4310" n="e" s="T31">bazo </ts>
               <ts e="T33" id="Seg_4312" n="e" s="T32">parzittə </ts>
               <ts e="T34" id="Seg_4314" n="e" s="T33">maːndə. </ts>
               <ts e="T35" id="Seg_4316" n="e" s="T34">((BRK)). </ts>
               <ts e="T36" id="Seg_4318" n="e" s="T35">((BRK)). </ts>
               <ts e="T37" id="Seg_4320" n="e" s="T36">Onʼiʔ </ts>
               <ts e="T38" id="Seg_4322" n="e" s="T37">koʔbdo, </ts>
               <ts e="T39" id="Seg_4324" n="e" s="T38">Jelʼa </ts>
               <ts e="T40" id="Seg_4326" n="e" s="T39">kambi, </ts>
               <ts e="T41" id="Seg_4328" n="e" s="T40">süt </ts>
               <ts e="T42" id="Seg_4330" n="e" s="T41">măndərdəsʼtə. </ts>
               <ts e="T43" id="Seg_4332" n="e" s="T42">I </ts>
               <ts e="T44" id="Seg_4334" n="e" s="T43">(šo-) </ts>
               <ts e="T45" id="Seg_4336" n="e" s="T44">šobi </ts>
               <ts e="T46" id="Seg_4338" n="e" s="T45">nükenə </ts>
               <ts e="T47" id="Seg_4340" n="e" s="T46">onʼiʔ, </ts>
               <ts e="T48" id="Seg_4342" n="e" s="T47">nükenə. </ts>
               <ts e="T49" id="Seg_4344" n="e" s="T48">Dĭ </ts>
               <ts e="T50" id="Seg_4346" n="e" s="T49">nu </ts>
               <ts e="T51" id="Seg_4348" n="e" s="T50">ige </ts>
               <ts e="T52" id="Seg_4350" n="e" s="T51">(dĭ) </ts>
               <ts e="T53" id="Seg_4352" n="e" s="T52">nüke, </ts>
               <ts e="T54" id="Seg_4354" n="e" s="T53">dĭ </ts>
               <ts e="T55" id="Seg_4356" n="e" s="T54">măndə: </ts>
               <ts e="T56" id="Seg_4358" n="e" s="T55">Лицо — </ts>
               <ts e="T57" id="Seg_4360" n="e" s="T56">kadəl. </ts>
               <ts e="T61" id="Seg_4362" n="e" s="T57">(A - a -) </ts>
               <ts e="T62" id="Seg_4364" n="e" s="T61">A </ts>
               <ts e="T63" id="Seg_4366" n="e" s="T62">волосья— </ts>
               <ts e="T64" id="Seg_4368" n="e" s="T63">eʔbdə. </ts>
               <ts e="T65" id="Seg_4370" n="e" s="T64">Dĭ </ts>
               <ts e="T66" id="Seg_4372" n="e" s="T65">bar </ts>
               <ts e="T67" id="Seg_4374" n="e" s="T66">parluʔpi. </ts>
               <ts e="T68" id="Seg_4376" n="e" s="T67">Bar </ts>
               <ts e="T69" id="Seg_4378" n="e" s="T68">sĭjdə </ts>
               <ts e="T70" id="Seg_4380" n="e" s="T69">ugandə </ts>
               <ts e="T71" id="Seg_4382" n="e" s="T70">jakše. </ts>
               <ts e="T72" id="Seg_4384" n="e" s="T71">(Măn- </ts>
               <ts e="T73" id="Seg_4386" n="e" s="T72">măn-) </ts>
               <ts e="T74" id="Seg_4388" n="e" s="T73">Mămbi: </ts>
               <ts e="T75" id="Seg_4390" n="e" s="T74">Dön </ts>
               <ts e="T76" id="Seg_4392" n="e" s="T75">nüke </ts>
               <ts e="T77" id="Seg_4394" n="e" s="T76">ige. </ts>
               <ts e="T78" id="Seg_4396" n="e" s="T77">Nudla </ts>
               <ts e="T79" id="Seg_4398" n="e" s="T78">molia </ts>
               <ts e="T80" id="Seg_4400" n="e" s="T79">dʼăbaktərzittə. </ts>
               <ts e="T81" id="Seg_4402" n="e" s="T80">((BRK)). </ts>
               <ts e="T82" id="Seg_4404" n="e" s="T81">Dĭ </ts>
               <ts e="T83" id="Seg_4406" n="e" s="T82">măna </ts>
               <ts e="T84" id="Seg_4408" n="e" s="T83">nörbəbi: </ts>
               <ts e="T85" id="Seg_4410" n="e" s="T84">девушка </ts>
               <ts e="T86" id="Seg_4412" n="e" s="T85">koʔbdo </ts>
               <ts e="T87" id="Seg_4414" n="e" s="T86">ige, </ts>
               <ts e="T88" id="Seg_4416" n="e" s="T87">a </ts>
               <ts e="T89" id="Seg_4418" n="e" s="T88">парень </ts>
               <ts e="T90" id="Seg_4420" n="e" s="T89">nʼi. </ts>
               <ts e="T92" id="Seg_4422" n="e" s="T90">Dĭgəttə… </ts>
               <ts e="T93" id="Seg_4424" n="e" s="T92">((BRK)). </ts>
               <ts e="T94" id="Seg_4426" n="e" s="T93">Dĭgəttə </ts>
               <ts e="T95" id="Seg_4428" n="e" s="T94">miʔ </ts>
               <ts e="T96" id="Seg_4430" n="e" s="T95">maluʔpibeʔ. </ts>
               <ts e="T97" id="Seg_4432" n="e" s="T96">Dĭ </ts>
               <ts e="T98" id="Seg_4434" n="e" s="T97">nükenə </ts>
               <ts e="T99" id="Seg_4436" n="e" s="T98">(mĭm-) </ts>
               <ts e="T100" id="Seg_4438" n="e" s="T99">mĭmbibeʔ </ts>
               <ts e="T101" id="Seg_4440" n="e" s="T100">dʼăbaktərzittə. </ts>
               <ts e="T102" id="Seg_4442" n="e" s="T101">Nagur </ts>
               <ts e="T103" id="Seg_4444" n="e" s="T102">bʼeʔ </ts>
               <ts e="T104" id="Seg_4446" n="e" s="T103">dʼala </ts>
               <ts e="T105" id="Seg_4448" n="e" s="T104">dʼăbaktərbibaʔ. </ts>
               <ts e="T106" id="Seg_4450" n="e" s="T105">Bar </ts>
               <ts e="T107" id="Seg_4452" n="e" s="T106">ĭmbi. </ts>
               <ts e="T108" id="Seg_4454" n="e" s="T107">Nörbəbi </ts>
               <ts e="T109" id="Seg_4456" n="e" s="T108">bostə </ts>
               <ts e="T110" id="Seg_4458" n="e" s="T109">(šĭ-) </ts>
               <ts e="T111" id="Seg_4460" n="e" s="T110">šĭkətsi. </ts>
               <ts e="T112" id="Seg_4462" n="e" s="T111">Dăre </ts>
               <ts e="T113" id="Seg_4464" n="e" s="T112">mămbi: </ts>
               <ts e="T114" id="Seg_4466" n="e" s="T113">Măn </ts>
               <ts e="T115" id="Seg_4468" n="e" s="T114">tʼotkam </ts>
               <ts e="T116" id="Seg_4470" n="e" s="T115">külambi. </ts>
               <ts e="T117" id="Seg_4472" n="e" s="T116">Šide </ts>
               <ts e="T118" id="Seg_4474" n="e" s="T117">bʼeʔ </ts>
               <ts e="T119" id="Seg_4476" n="e" s="T118">pʼe </ts>
               <ts e="T120" id="Seg_4478" n="e" s="T119">kambi. </ts>
               <ts e="T121" id="Seg_4480" n="e" s="T120">(Šindəzizdə) </ts>
               <ts e="T122" id="Seg_4482" n="e" s="T121">ej </ts>
               <ts e="T123" id="Seg_4484" n="e" s="T122">dʼăbaktəriam. </ts>
               <ts e="T124" id="Seg_4486" n="e" s="T123">((BRK)). </ts>
               <ts e="T125" id="Seg_4488" n="e" s="T124">Dĭʔnə </ts>
               <ts e="T126" id="Seg_4490" n="e" s="T125">tüj </ts>
               <ts e="T127" id="Seg_4492" n="e" s="T126">(š-) </ts>
               <ts e="T128" id="Seg_4494" n="e" s="T127">sejʔpü </ts>
               <ts e="T129" id="Seg_4496" n="e" s="T128">pʼe. </ts>
               <ts e="T130" id="Seg_4498" n="e" s="T129">Dĭ </ts>
               <ts e="T131" id="Seg_4500" n="e" s="T130">ugandə </ts>
               <ts e="T132" id="Seg_4502" n="e" s="T131">jakše </ts>
               <ts e="T133" id="Seg_4504" n="e" s="T132">nüke. </ts>
               <ts e="T134" id="Seg_4506" n="e" s="T133">Bar </ts>
               <ts e="T135" id="Seg_4508" n="e" s="T134">ĭmbit </ts>
               <ts e="T136" id="Seg_4510" n="e" s="T135">ige. </ts>
               <ts e="T137" id="Seg_4512" n="e" s="T136">Amzittə </ts>
               <ts e="T138" id="Seg_4514" n="e" s="T137">mĭlie, </ts>
               <ts e="T139" id="Seg_4516" n="e" s="T138">a </ts>
               <ts e="T140" id="Seg_4518" n="e" s="T139">ej </ts>
               <ts e="T141" id="Seg_4520" n="e" s="T140">amnal </ts>
               <ts e="T142" id="Seg_4522" n="e" s="T141">dăk, </ts>
               <ts e="T143" id="Seg_4524" n="e" s="T142">dĭ </ts>
               <ts e="T144" id="Seg_4526" n="e" s="T143">bar </ts>
               <ts e="T145" id="Seg_4528" n="e" s="T144">kurollaʔbə. </ts>
               <ts e="T146" id="Seg_4530" n="e" s="T145">((BRK)). </ts>
               <ts e="T147" id="Seg_4532" n="e" s="T146">((BRK)). </ts>
               <ts e="T148" id="Seg_4534" n="e" s="T147">Dĭgəttə </ts>
               <ts e="T149" id="Seg_4536" n="e" s="T148">dĭ </ts>
               <ts e="T150" id="Seg_4538" n="e" s="T149">üge </ts>
               <ts e="T151" id="Seg_4540" n="e" s="T150">maːʔndə </ts>
               <ts e="T152" id="Seg_4542" n="e" s="T151">iʔgö </ts>
               <ts e="T153" id="Seg_4544" n="e" s="T152">togonoria, </ts>
               <ts e="T154" id="Seg_4546" n="e" s="T153">ej </ts>
               <ts e="T155" id="Seg_4548" n="e" s="T154">sedem </ts>
               <ts e="T156" id="Seg_4550" n="e" s="T155">üjündə. </ts>
               <ts e="T157" id="Seg_4552" n="e" s="T156">((BRK)). </ts>
               <ts e="T1074" id="Seg_4554" n="e" s="T157">Kazan </ts>
               <ts e="T158" id="Seg_4556" n="e" s="T1074">turanə </ts>
               <ts e="T159" id="Seg_4558" n="e" s="T158">(bʼeʔ </ts>
               <ts e="T160" id="Seg_4560" n="e" s="T159">su-) </ts>
               <ts e="T161" id="Seg_4562" n="e" s="T160">bʼeʔ </ts>
               <ts e="T162" id="Seg_4564" n="e" s="T161">šide </ts>
               <ts e="T163" id="Seg_4566" n="e" s="T162">sumna, </ts>
               <ts e="T164" id="Seg_4568" n="e" s="T163">dĭ </ts>
               <ts e="T165" id="Seg_4570" n="e" s="T164">bar </ts>
               <ts e="T166" id="Seg_4572" n="e" s="T165">(ka-) </ts>
               <ts e="T167" id="Seg_4574" n="e" s="T166">kalia </ts>
               <ts e="T168" id="Seg_4576" n="e" s="T167">üjüzi. </ts>
               <ts e="T169" id="Seg_4578" n="e" s="T168">A </ts>
               <ts e="T170" id="Seg_4580" n="e" s="T169">mašinaʔi </ts>
               <ts e="T171" id="Seg_4582" n="e" s="T170">mĭllieʔi, </ts>
               <ts e="T172" id="Seg_4584" n="e" s="T171">dĭ </ts>
               <ts e="T173" id="Seg_4586" n="e" s="T172">dĭbər </ts>
               <ts e="T174" id="Seg_4588" n="e" s="T173">ej </ts>
               <ts e="T175" id="Seg_4590" n="e" s="T174">amlia. </ts>
               <ts e="T176" id="Seg_4592" n="e" s="T175">A </ts>
               <ts e="T177" id="Seg_4594" n="e" s="T176">üdʼin </ts>
               <ts e="T178" id="Seg_4596" n="e" s="T177">(ka- </ts>
               <ts e="T179" id="Seg_4598" n="e" s="T178">kan-) </ts>
               <ts e="T180" id="Seg_4600" n="e" s="T179">kaləj. </ts>
               <ts e="T181" id="Seg_4602" n="e" s="T180">((BRK)). </ts>
               <ts e="T182" id="Seg_4604" n="e" s="T181">((BRK)). </ts>
               <ts e="T183" id="Seg_4606" n="e" s="T182">"Măn </ts>
               <ts e="T184" id="Seg_4608" n="e" s="T183">üjüzi </ts>
               <ts e="T185" id="Seg_4610" n="e" s="T184">lutʼšə </ts>
               <ts e="T186" id="Seg_4612" n="e" s="T185">kalam. </ts>
               <ts e="T187" id="Seg_4614" n="e" s="T186">Aʔtʼi </ts>
               <ts e="T188" id="Seg_4616" n="e" s="T187">прямой. </ts>
               <ts e="T189" id="Seg_4618" n="e" s="T188">A </ts>
               <ts e="T190" id="Seg_4620" n="e" s="T189">mašinaʔizi </ts>
               <ts e="T191" id="Seg_4622" n="e" s="T190">ugandə </ts>
               <ts e="T192" id="Seg_4624" n="e" s="T191">kuŋgəŋ </ts>
               <ts e="T193" id="Seg_4626" n="e" s="T192">kanzittə </ts>
               <ts e="T194" id="Seg_4628" n="e" s="T193">da </ts>
               <ts e="T195" id="Seg_4630" n="e" s="T194">bar </ts>
               <ts e="T196" id="Seg_4632" n="e" s="T195">sădərlaʔbə". </ts>
               <ts e="T197" id="Seg_4634" n="e" s="T196">((BRK)). </ts>
               <ts e="T198" id="Seg_4636" n="e" s="T197">Girgit </ts>
               <ts e="T199" id="Seg_4638" n="e" s="T198">судьба </ts>
               <ts e="T200" id="Seg_4640" n="e" s="T199">nuzaŋgən. </ts>
               <ts e="T201" id="Seg_4642" n="e" s="T200">Šində </ts>
               <ts e="T202" id="Seg_4644" n="e" s="T201">išo </ts>
               <ts e="T203" id="Seg_4646" n="e" s="T202">molia </ts>
               <ts e="T204" id="Seg_4648" n="e" s="T203">dʼăbaktərzittə, </ts>
               <ts e="T205" id="Seg_4650" n="e" s="T204">ali </ts>
               <ts e="T206" id="Seg_4652" n="e" s="T205">(ej). </ts>
               <ts e="T207" id="Seg_4654" n="e" s="T206">((BRK)). </ts>
               <ts e="T208" id="Seg_4656" n="e" s="T207">(Nʼiʔnen) </ts>
               <ts e="T209" id="Seg_4658" n="e" s="T208">Abalakovan, </ts>
               <ts e="T210" id="Seg_4660" n="e" s="T209">esseŋdə </ts>
               <ts e="T211" id="Seg_4662" n="e" s="T210">možna </ts>
               <ts e="T212" id="Seg_4664" n="e" s="T211">kuzittə. </ts>
               <ts e="T213" id="Seg_4666" n="e" s="T212">Sĭre </ts>
               <ts e="T214" id="Seg_4668" n="e" s="T213">ulut, </ts>
               <ts e="T215" id="Seg_4670" n="e" s="T214">simat </ts>
               <ts e="T216" id="Seg_4672" n="e" s="T215">sagər. </ts>
               <ts e="T217" id="Seg_4674" n="e" s="T216">Не </ts>
               <ts e="T218" id="Seg_4676" n="e" s="T217">только </ts>
               <ts e="T219" id="Seg_4678" n="e" s="T218">kazak </ts>
               <ts e="T220" id="Seg_4680" n="e" s="T219">kem </ts>
               <ts e="T221" id="Seg_4682" n="e" s="T220">ige. </ts>
               <ts e="T222" id="Seg_4684" n="e" s="T221">I </ts>
               <ts e="T223" id="Seg_4686" n="e" s="T222">kem </ts>
               <ts e="T224" id="Seg_4688" n="e" s="T223">ige </ts>
               <ts e="T225" id="Seg_4690" n="e" s="T224">nuzaŋ. </ts>
               <ts e="T226" id="Seg_4692" n="e" s="T225">((BRK)). </ts>
               <ts e="T227" id="Seg_4694" n="e" s="T226">Girgit </ts>
               <ts e="T228" id="Seg_4696" n="e" s="T227">bar </ts>
               <ts e="T229" id="Seg_4698" n="e" s="T228">özerleʔbəʔi </ts>
               <ts e="T230" id="Seg_4700" n="e" s="T229">(li). </ts>
               <ts e="T231" id="Seg_4702" n="e" s="T230">Kambiʔi </ts>
               <ts e="T232" id="Seg_4704" n="e" s="T231">togonorzittə, </ts>
               <ts e="T233" id="Seg_4706" n="e" s="T232">dĭbər-döbər, </ts>
               <ts e="T234" id="Seg_4708" n="e" s="T233">vot </ts>
               <ts e="T235" id="Seg_4710" n="e" s="T234">Проня. </ts>
               <ts e="T236" id="Seg_4712" n="e" s="T235">Анджигатов. </ts>
               <ts e="T238" id="Seg_4714" n="e" s="T236">Председатель сельсовета. </ts>
               <ts e="T239" id="Seg_4716" n="e" s="T238">A </ts>
               <ts e="T240" id="Seg_4718" n="e" s="T239">bostə </ts>
               <ts e="T241" id="Seg_4720" n="e" s="T240">šĭket </ts>
               <ts e="T242" id="Seg_4722" n="e" s="T241">ej </ts>
               <ts e="T243" id="Seg_4724" n="e" s="T242">tĭmnet. </ts>
               <ts e="T244" id="Seg_4726" n="e" s="T243">((BRK)). </ts>
               <ts e="T245" id="Seg_4728" n="e" s="T244">Tolʼkă </ts>
               <ts e="T246" id="Seg_4730" n="e" s="T245">dĭ </ts>
               <ts e="T247" id="Seg_4732" n="e" s="T246">nuzaŋ </ts>
               <ts e="T248" id="Seg_4734" n="e" s="T247">šĭket </ts>
               <ts e="T249" id="Seg_4736" n="e" s="T248">onʼiʔ </ts>
               <ts e="T250" id="Seg_4738" n="e" s="T249">nüke </ts>
               <ts e="T251" id="Seg_4740" n="e" s="T250">tĭmnet. </ts>
               <ts e="T252" id="Seg_4742" n="e" s="T251">A </ts>
               <ts e="T253" id="Seg_4744" n="e" s="T252">daška </ts>
               <ts e="T254" id="Seg_4746" n="e" s="T253">šindidə </ts>
               <ts e="T255" id="Seg_4748" n="e" s="T254">ej </ts>
               <ts e="T256" id="Seg_4750" n="e" s="T255">tĭmne </ts>
               <ts e="T257" id="Seg_4752" n="e" s="T256">dĭ </ts>
               <ts e="T258" id="Seg_4754" n="e" s="T257">šĭkem. </ts>
               <ts e="T259" id="Seg_4756" n="e" s="T258">((BRK)). </ts>
               <ts e="T260" id="Seg_4758" n="e" s="T259">Tăn </ts>
               <ts e="T261" id="Seg_4760" n="e" s="T260">măna </ts>
               <ts e="T262" id="Seg_4762" n="e" s="T261">šaːmbial. </ts>
               <ts e="T263" id="Seg_4764" n="e" s="T262">Tăn </ts>
               <ts e="T264" id="Seg_4766" n="e" s="T263">măna </ts>
               <ts e="T265" id="Seg_4768" n="e" s="T264">mʼegluʔpiel. </ts>
               <ts e="T266" id="Seg_4770" n="e" s="T265">Dĭgəttə </ts>
               <ts e="T267" id="Seg_4772" n="e" s="T266">šaʔlambiam, </ts>
               <ts e="T268" id="Seg_4774" n="e" s="T267">a </ts>
               <ts e="T269" id="Seg_4776" n="e" s="T268">măn </ts>
               <ts e="T270" id="Seg_4778" n="e" s="T269">tănan </ts>
               <ts e="T271" id="Seg_4780" n="e" s="T270">măndərbiam, </ts>
               <ts e="T272" id="Seg_4782" n="e" s="T271">măndərbiam, </ts>
               <ts e="T273" id="Seg_4784" n="e" s="T272">ej </ts>
               <ts e="T274" id="Seg_4786" n="e" s="T273">(mo-) </ts>
               <ts e="T275" id="Seg_4788" n="e" s="T274">moliam </ts>
               <ts e="T276" id="Seg_4790" n="e" s="T275">măndərzittə. </ts>
               <ts e="T277" id="Seg_4792" n="e" s="T276">Ej </ts>
               <ts e="T278" id="Seg_4794" n="e" s="T277">jakše </ts>
               <ts e="T279" id="Seg_4796" n="e" s="T278">kuza </ts>
               <ts e="T280" id="Seg_4798" n="e" s="T279">amnobi. </ts>
               <ts e="T281" id="Seg_4800" n="e" s="T280">Dĭm </ts>
               <ts e="T282" id="Seg_4802" n="e" s="T281">numəjleʔpiʔi </ts>
               <ts e="T283" id="Seg_4804" n="e" s="T282">Tartʼəberdʼə. </ts>
               <ts e="T284" id="Seg_4806" n="e" s="T283">Dĭ </ts>
               <ts e="T285" id="Seg_4808" n="e" s="T284">bar </ts>
               <ts e="T286" id="Seg_4810" n="e" s="T285">šojmü </ts>
               <ts e="T287" id="Seg_4812" n="e" s="T286">dʼaʔpi. </ts>
               <ts e="T288" id="Seg_4814" n="e" s="T287">Üjüt </ts>
               <ts e="T289" id="Seg_4816" n="e" s="T288">ĭzemnie. </ts>
               <ts e="T290" id="Seg_4818" n="e" s="T289">Kanzittə </ts>
               <ts e="T291" id="Seg_4820" n="e" s="T290">ej </ts>
               <ts e="T292" id="Seg_4822" n="e" s="T291">molia. </ts>
               <ts e="T293" id="Seg_4824" n="e" s="T292">Dĭgəttə </ts>
               <ts e="T294" id="Seg_4826" n="e" s="T293">kambi, </ts>
               <ts e="T295" id="Seg_4828" n="e" s="T294">kambi. </ts>
               <ts e="T296" id="Seg_4830" n="e" s="T295">Urgaːba </ts>
               <ts e="T297" id="Seg_4832" n="e" s="T296">šonəga: </ts>
               <ts e="T298" id="Seg_4834" n="e" s="T297">Iʔ </ts>
               <ts e="T299" id="Seg_4836" n="e" s="T298">măna </ts>
               <ts e="T300" id="Seg_4838" n="e" s="T299">(bostəzi)! </ts>
               <ts e="T301" id="Seg_4840" n="e" s="T300">No, </ts>
               <ts e="T302" id="Seg_4842" n="e" s="T301">tolʼkă </ts>
               <ts e="T303" id="Seg_4844" n="e" s="T302">sagər </ts>
               <ts e="T304" id="Seg_4846" n="e" s="T303">(mu-) </ts>
               <ts e="T305" id="Seg_4848" n="e" s="T304">băranə </ts>
               <ts e="T306" id="Seg_4850" n="e" s="T305">(amdəldə </ts>
               <ts e="T307" id="Seg_4852" n="e" s="T306">măna). </ts>
               <ts e="T308" id="Seg_4854" n="e" s="T307">Dĭ </ts>
               <ts e="T309" id="Seg_4856" n="e" s="T308">dĭm </ts>
               <ts e="T310" id="Seg_4858" n="e" s="T309">amnolbi, </ts>
               <ts e="T311" id="Seg_4860" n="e" s="T310">bazo </ts>
               <ts e="T312" id="Seg_4862" n="e" s="T311">kandəga. </ts>
               <ts e="T313" id="Seg_4864" n="e" s="T312">Kandəga, </ts>
               <ts e="T314" id="Seg_4866" n="e" s="T313">kandəga, </ts>
               <ts e="T316" id="Seg_4868" n="e" s="T314">dĭgəttə… </ts>
               <ts e="T317" id="Seg_4870" n="e" s="T316">Dĭgəttə </ts>
               <ts e="T318" id="Seg_4872" n="e" s="T317">urgo </ts>
               <ts e="T319" id="Seg_4874" n="e" s="T318">men </ts>
               <ts e="T320" id="Seg_4876" n="e" s="T319">dʼijegə </ts>
               <ts e="T321" id="Seg_4878" n="e" s="T320">šobi. </ts>
               <ts e="T322" id="Seg_4880" n="e" s="T321">Iʔ </ts>
               <ts e="T323" id="Seg_4882" n="e" s="T322">măna. </ts>
               <ts e="T324" id="Seg_4884" n="e" s="T323">Tolʼkă </ts>
               <ts e="T325" id="Seg_4886" n="e" s="T324">sagər </ts>
               <ts e="T326" id="Seg_4888" n="e" s="T325">măna </ts>
               <ts e="T327" id="Seg_4890" n="e" s="T326">băranə </ts>
               <ts e="T328" id="Seg_4892" n="e" s="T327">amdəldə! </ts>
               <ts e="T329" id="Seg_4894" n="e" s="T328">Dĭ </ts>
               <ts e="T330" id="Seg_4896" n="e" s="T329">amnoldəbi. </ts>
               <ts e="T331" id="Seg_4898" n="e" s="T330">Kambi </ts>
               <ts e="T332" id="Seg_4900" n="e" s="T331">bazoʔ, </ts>
               <ts e="T333" id="Seg_4902" n="e" s="T332">šonəga, </ts>
               <ts e="T334" id="Seg_4904" n="e" s="T333">šonəga, </ts>
               <ts e="T335" id="Seg_4906" n="e" s="T334">(šo- </ts>
               <ts e="T336" id="Seg_4908" n="e" s="T335">šo-). </ts>
               <ts e="T337" id="Seg_4910" n="e" s="T336">(Li-) </ts>
               <ts e="T338" id="Seg_4912" n="e" s="T337">Lisa </ts>
               <ts e="T339" id="Seg_4914" n="e" s="T338">šolaʔbə. </ts>
               <ts e="T340" id="Seg_4916" n="e" s="T339">Iʔ </ts>
               <ts e="T341" id="Seg_4918" n="e" s="T340">măna! </ts>
               <ts e="T342" id="Seg_4920" n="e" s="T341">Dĭ </ts>
               <ts e="T343" id="Seg_4922" n="e" s="T342">dĭm </ts>
               <ts e="T344" id="Seg_4924" n="e" s="T343">ibi. </ts>
               <ts e="T345" id="Seg_4926" n="e" s="T344">Sagər </ts>
               <ts e="T346" id="Seg_4928" n="e" s="T345">băranə </ts>
               <ts e="T347" id="Seg_4930" n="e" s="T346">amnolbi, </ts>
               <ts e="T348" id="Seg_4932" n="e" s="T347">dĭgəttə </ts>
               <ts e="T349" id="Seg_4934" n="e" s="T348">bazo </ts>
               <ts e="T350" id="Seg_4936" n="e" s="T349">kandəga. </ts>
               <ts e="T351" id="Seg_4938" n="e" s="T350">Bü </ts>
               <ts e="T352" id="Seg_4940" n="e" s="T351">šonəga: </ts>
               <ts e="T353" id="Seg_4942" n="e" s="T352">Iʔ </ts>
               <ts e="T354" id="Seg_4944" n="e" s="T353">măna! </ts>
               <ts e="T355" id="Seg_4946" n="e" s="T354">Dĭgəttə </ts>
               <ts e="T356" id="Seg_4948" n="e" s="T355">dĭ </ts>
               <ts e="T357" id="Seg_4950" n="e" s="T356">bü </ts>
               <ts e="T358" id="Seg_4952" n="e" s="T357">kămnəbi </ts>
               <ts e="T359" id="Seg_4954" n="e" s="T358">dĭbər. </ts>
               <ts e="T360" id="Seg_4956" n="e" s="T359">Šobi. </ts>
               <ts e="T361" id="Seg_4958" n="e" s="T360">Dĭn </ts>
               <ts e="T362" id="Seg_4960" n="e" s="T361">bar </ts>
               <ts e="T363" id="Seg_4962" n="e" s="T362">turaʔi </ts>
               <ts e="T364" id="Seg_4964" n="e" s="T363">nugaʔi </ts>
               <ts e="T365" id="Seg_4966" n="e" s="T364">i </ts>
               <ts e="T366" id="Seg_4968" n="e" s="T365">bü </ts>
               <ts e="T367" id="Seg_4970" n="e" s="T366">ugaːndə </ts>
               <ts e="T368" id="Seg_4972" n="e" s="T367">urgo. </ts>
               <ts e="T369" id="Seg_4974" n="e" s="T368">Dĭ </ts>
               <ts e="T370" id="Seg_4976" n="e" s="T369">šöjmü </ts>
               <ts e="T371" id="Seg_4978" n="e" s="T370">sarbi. </ts>
               <ts e="T372" id="Seg_4980" n="e" s="T371">Šü </ts>
               <ts e="T373" id="Seg_4982" n="e" s="T372">embi, </ts>
               <ts e="T374" id="Seg_4984" n="e" s="T373">amnolaʔbə. </ts>
               <ts e="T375" id="Seg_4986" n="e" s="T374">Dĭm </ts>
               <ts e="T376" id="Seg_4988" n="e" s="T375">kuluʔpi </ts>
               <ts e="T377" id="Seg_4990" n="e" s="T376">koŋ: </ts>
               <ts e="T378" id="Seg_4992" n="e" s="T377">Šində </ts>
               <ts e="T379" id="Seg_4994" n="e" s="T378">dĭn </ts>
               <ts e="T380" id="Seg_4996" n="e" s="T379">amnolaʔbə? </ts>
               <ts e="T381" id="Seg_4998" n="e" s="T380">Măn </ts>
               <ts e="T382" id="Seg_5000" n="e" s="T381">(noʔ-) </ts>
               <ts e="T383" id="Seg_5002" n="e" s="T382">noʔdə </ts>
               <ts e="T384" id="Seg_5004" n="e" s="T383">tonəʔlaʔbə. </ts>
               <ts e="T385" id="Seg_5006" n="e" s="T384">Šöjmüt </ts>
               <ts e="T386" id="Seg_5008" n="e" s="T385">amnaʔbə. </ts>
               <ts e="T387" id="Seg_5010" n="e" s="T386">Dĭn </ts>
               <ts e="T388" id="Seg_5012" n="e" s="T387">üdʼügen </ts>
               <ts e="T389" id="Seg_5014" n="e" s="T388">amnoləj, </ts>
               <ts e="T390" id="Seg_5016" n="e" s="T389">dĭgəttə </ts>
               <ts e="T391" id="Seg_5018" n="e" s="T390">bazoʔ </ts>
               <ts e="T392" id="Seg_5020" n="e" s="T391">bar </ts>
               <ts e="T393" id="Seg_5022" n="e" s="T392">tonəʔləj. </ts>
               <ts e="T394" id="Seg_5024" n="e" s="T393">Kanaʔ! </ts>
               <ts e="T395" id="Seg_5026" n="e" s="T394">Nʼibə </ts>
               <ts e="T396" id="Seg_5028" n="e" s="T395">öʔlubi. </ts>
               <ts e="T397" id="Seg_5030" n="e" s="T396">Ĭmbi </ts>
               <ts e="T398" id="Seg_5032" n="e" s="T397">dĭʔnə </ts>
               <ts e="T399" id="Seg_5034" n="e" s="T398">kereʔ?" </ts>
               <ts e="T400" id="Seg_5036" n="e" s="T399">Dĭ </ts>
               <ts e="T401" id="Seg_5038" n="e" s="T400">šobi </ts>
               <ts e="T402" id="Seg_5040" n="e" s="T401">(surarla-) </ts>
               <ts e="T403" id="Seg_5042" n="e" s="T402">surarlaʔbə: </ts>
               <ts e="T404" id="Seg_5044" n="e" s="T403">Ĭmbi </ts>
               <ts e="T405" id="Seg_5046" n="e" s="T404">tănan </ts>
               <ts e="T406" id="Seg_5048" n="e" s="T405">kereʔ? </ts>
               <ts e="T407" id="Seg_5050" n="e" s="T406">Dĭn </ts>
               <ts e="T408" id="Seg_5052" n="e" s="T407">koŋdə </ts>
               <ts e="T409" id="Seg_5054" n="e" s="T408">koʔbdobə </ts>
               <ts e="T410" id="Seg_5056" n="e" s="T409">ilim". </ts>
               <ts e="T411" id="Seg_5058" n="e" s="T410">((BRK)). </ts>
               <ts e="T412" id="Seg_5060" n="e" s="T411">Dĭ </ts>
               <ts e="T413" id="Seg_5062" n="e" s="T412">nʼi </ts>
               <ts e="T414" id="Seg_5064" n="e" s="T413">parluʔbi, </ts>
               <ts e="T415" id="Seg_5066" n="e" s="T414">măndə: </ts>
               <ts e="T416" id="Seg_5068" n="e" s="T415">Dĭ </ts>
               <ts e="T417" id="Seg_5070" n="e" s="T416">măndə: </ts>
               <ts e="T418" id="Seg_5072" n="e" s="T417">(pušaj </ts>
               <ts e="T419" id="Seg_5074" n="e" s="T418">dĭm=) </ts>
               <ts e="T420" id="Seg_5076" n="e" s="T419">koŋdə </ts>
               <ts e="T421" id="Seg_5078" n="e" s="T420">koʔbdobə </ts>
               <ts e="T422" id="Seg_5080" n="e" s="T421">ilim! </ts>
               <ts e="T423" id="Seg_5082" n="e" s="T422">Pušaj </ts>
               <ts e="T424" id="Seg_5084" n="e" s="T423">(kangəʔjə). </ts>
               <ts e="T425" id="Seg_5086" n="e" s="T424">A_to </ts>
               <ts e="T426" id="Seg_5088" n="e" s="T425">ej </ts>
               <ts e="T427" id="Seg_5090" n="e" s="T426">jakše </ts>
               <ts e="T428" id="Seg_5092" n="e" s="T427">moləj. </ts>
               <ts e="T429" id="Seg_5094" n="e" s="T428">(Em=) </ts>
               <ts e="T430" id="Seg_5096" n="e" s="T429">Ej </ts>
               <ts e="T431" id="Seg_5098" n="e" s="T430">mĭlem! </ts>
               <ts e="T432" id="Seg_5100" n="e" s="T431">Dĭ </ts>
               <ts e="T433" id="Seg_5102" n="e" s="T432">šobi, </ts>
               <ts e="T434" id="Seg_5104" n="e" s="T433">(măn-) </ts>
               <ts e="T435" id="Seg_5106" n="e" s="T434">mămbi </ts>
               <ts e="T436" id="Seg_5108" n="e" s="T435">dăre: </ts>
               <ts e="T437" id="Seg_5110" n="e" s="T436">Koŋ </ts>
               <ts e="T438" id="Seg_5112" n="e" s="T437">ej </ts>
               <ts e="T439" id="Seg_5114" n="e" s="T438">mĭliet </ts>
               <ts e="T440" id="Seg_5116" n="e" s="T439">koʔbdobə. </ts>
               <ts e="T441" id="Seg_5118" n="e" s="T440">Ej </ts>
               <ts e="T442" id="Seg_5120" n="e" s="T441">mĭləj </ts>
               <ts e="T443" id="Seg_5122" n="e" s="T442">jakšeŋ </ts>
               <ts e="T444" id="Seg_5124" n="e" s="T443">dăk, </ts>
               <ts e="T445" id="Seg_5126" n="e" s="T444">ej </ts>
               <ts e="T446" id="Seg_5128" n="e" s="T445">jakše </ts>
               <ts e="T447" id="Seg_5130" n="e" s="T446">mĭləj. </ts>
               <ts e="T448" id="Seg_5132" n="e" s="T447">((BRK)). </ts>
               <ts e="T449" id="Seg_5134" n="e" s="T448">"Öʔleʔ </ts>
               <ts e="T450" id="Seg_5136" n="e" s="T449">дикий </ts>
               <ts e="T451" id="Seg_5138" n="e" s="T450">ineʔi. </ts>
               <ts e="T452" id="Seg_5140" n="e" s="T451">Dĭzeŋ </ts>
               <ts e="T453" id="Seg_5142" n="e" s="T452">bar </ts>
               <ts e="T454" id="Seg_5144" n="e" s="T453">dĭn </ts>
               <ts e="T455" id="Seg_5146" n="e" s="T454">šöjmündə </ts>
               <ts e="T456" id="Seg_5148" n="e" s="T455">sajnʼiʔluʔləʔjə, </ts>
               <ts e="T457" id="Seg_5150" n="e" s="T456">dĭgəttə </ts>
               <ts e="T1072" id="Seg_5152" n="e" s="T457">kalla </ts>
               <ts e="T458" id="Seg_5154" n="e" s="T1072">dʼürləj. </ts>
               <ts e="T459" id="Seg_5156" n="e" s="T458">Dĭgəttə </ts>
               <ts e="T460" id="Seg_5158" n="e" s="T459">dĭ </ts>
               <ts e="T461" id="Seg_5160" n="e" s="T460">urgaːba </ts>
               <ts e="T462" id="Seg_5162" n="e" s="T461">öʔlubi </ts>
               <ts e="T463" id="Seg_5164" n="e" s="T462">sagər </ts>
               <ts e="T464" id="Seg_5166" n="e" s="T463">băragəʔ. </ts>
               <ts e="T465" id="Seg_5168" n="e" s="T464">Dĭ </ts>
               <ts e="T466" id="Seg_5170" n="e" s="T465">urgaːba </ts>
               <ts e="T467" id="Seg_5172" n="e" s="T466">ineʔi </ts>
               <ts e="T468" id="Seg_5174" n="e" s="T467">bar </ts>
               <ts e="T469" id="Seg_5176" n="e" s="T468">sürerluʔpi. </ts>
               <ts e="T470" id="Seg_5178" n="e" s="T469">Dĭgəttə </ts>
               <ts e="T471" id="Seg_5180" n="e" s="T470">măndə: </ts>
               <ts e="T472" id="Seg_5182" n="e" s="T471">Öʔlugeʔ </ts>
               <ts e="T473" id="Seg_5184" n="e" s="T472">tüžöjʔjə, </ts>
               <ts e="T474" id="Seg_5186" n="e" s="T473">pušaj </ts>
               <ts e="T475" id="Seg_5188" n="e" s="T474">tüžöjʔjə. </ts>
               <ts e="T476" id="Seg_5190" n="e" s="T475">Amnutsi </ts>
               <ts e="T477" id="Seg_5192" n="e" s="T476">bar </ts>
               <ts e="T478" id="Seg_5194" n="e" s="T477">inebə </ts>
               <ts e="T479" id="Seg_5196" n="e" s="T478">bar </ts>
               <ts e="T480" id="Seg_5198" n="e" s="T479">kutləʔjə. </ts>
               <ts e="T481" id="Seg_5200" n="e" s="T480">(Dĭ=) </ts>
               <ts e="T482" id="Seg_5202" n="e" s="T481">Dĭ </ts>
               <ts e="T483" id="Seg_5204" n="e" s="T482">bar </ts>
               <ts e="T484" id="Seg_5206" n="e" s="T483">băragə </ts>
               <ts e="T485" id="Seg_5208" n="e" s="T484">öʔlubi </ts>
               <ts e="T486" id="Seg_5210" n="e" s="T485">urgo </ts>
               <ts e="T487" id="Seg_5212" n="e" s="T486">men, </ts>
               <ts e="T488" id="Seg_5214" n="e" s="T487">(dĭ=) </ts>
               <ts e="T489" id="Seg_5216" n="e" s="T488">dĭ </ts>
               <ts e="T490" id="Seg_5218" n="e" s="T489">kambi, </ts>
               <ts e="T491" id="Seg_5220" n="e" s="T490">dĭzeŋ </ts>
               <ts e="T492" id="Seg_5222" n="e" s="T491">bar </ts>
               <ts e="T493" id="Seg_5224" n="e" s="T492">sürerlüʔbi. </ts>
               <ts e="T494" id="Seg_5226" n="e" s="T493">Dĭgəttə </ts>
               <ts e="T495" id="Seg_5228" n="e" s="T494">măndə: </ts>
               <ts e="T497" id="Seg_5230" n="e" s="T495">Öʔlugeʔ…" </ts>
               <ts e="T498" id="Seg_5232" n="e" s="T497">((BRK)). </ts>
               <ts e="T499" id="Seg_5234" n="e" s="T498">Dĭgəttə </ts>
               <ts e="T500" id="Seg_5236" n="e" s="T499">koŋ: </ts>
               <ts e="T501" id="Seg_5238" n="e" s="T500">(Ölia- </ts>
               <ts e="T502" id="Seg_5240" n="e" s="T501">öli-) </ts>
               <ts e="T503" id="Seg_5242" n="e" s="T502">Öʔleʔ </ts>
               <ts e="T504" id="Seg_5244" n="e" s="T503">menzeŋ! </ts>
               <ts e="T505" id="Seg_5246" n="e" s="T504">Pušaj </ts>
               <ts e="T506" id="Seg_5248" n="e" s="T505">menzeŋ </ts>
               <ts e="T507" id="Seg_5250" n="e" s="T506">dĭm </ts>
               <ts e="T508" id="Seg_5252" n="e" s="T507">sajnüzəʔləʔjə. </ts>
               <ts e="T509" id="Seg_5254" n="e" s="T508">Dĭ </ts>
               <ts e="T510" id="Seg_5256" n="e" s="T509">öʔlubi </ts>
               <ts e="T511" id="Seg_5258" n="e" s="T510">mendən. </ts>
               <ts e="T512" id="Seg_5260" n="e" s="T511">A </ts>
               <ts e="T513" id="Seg_5262" n="e" s="T512">dĭ </ts>
               <ts e="T514" id="Seg_5264" n="e" s="T513">lʼisa </ts>
               <ts e="T515" id="Seg_5266" n="e" s="T514">öʔlubi, </ts>
               <ts e="T516" id="Seg_5268" n="e" s="T515">menzeŋ </ts>
               <ts e="T517" id="Seg_5270" n="e" s="T516">lʼisagən </ts>
               <ts e="T518" id="Seg_5272" n="e" s="T517">nuʔməluʔpiʔi. </ts>
               <ts e="T519" id="Seg_5274" n="e" s="T518">Dĭgəttə </ts>
               <ts e="T520" id="Seg_5276" n="e" s="T519">dĭ </ts>
               <ts e="T521" id="Seg_5278" n="e" s="T520">koŋ </ts>
               <ts e="T522" id="Seg_5280" n="e" s="T521">bar </ts>
               <ts e="T523" id="Seg_5282" n="e" s="T522">il </ts>
               <ts e="T524" id="Seg_5284" n="e" s="T523">oʔbdəbi: </ts>
               <ts e="T525" id="Seg_5286" n="e" s="T524">Kanžəbəj, </ts>
               <ts e="T526" id="Seg_5288" n="e" s="T525">dĭm </ts>
               <ts e="T527" id="Seg_5290" n="e" s="T526">iləbəj! </ts>
               <ts e="T528" id="Seg_5292" n="e" s="T527">Il </ts>
               <ts e="T529" id="Seg_5294" n="e" s="T528">šobiʔi, </ts>
               <ts e="T530" id="Seg_5296" n="e" s="T529">dĭ </ts>
               <ts e="T531" id="Seg_5298" n="e" s="T530">bar </ts>
               <ts e="T532" id="Seg_5300" n="e" s="T531">bărabə </ts>
               <ts e="T533" id="Seg_5302" n="e" s="T532">bü </ts>
               <ts e="T534" id="Seg_5304" n="e" s="T533">kămnəbi. </ts>
               <ts e="T535" id="Seg_5306" n="e" s="T534">(I </ts>
               <ts e="T536" id="Seg_5308" n="e" s="T535">bü </ts>
               <ts e="T537" id="Seg_5310" n="e" s="T536">il=) </ts>
               <ts e="T538" id="Seg_5312" n="e" s="T537">Il </ts>
               <ts e="T539" id="Seg_5314" n="e" s="T538">bar </ts>
               <ts e="T540" id="Seg_5316" n="e" s="T539">nuʔməluʔbiʔi </ts>
               <ts e="T541" id="Seg_5318" n="e" s="T540">bügə. </ts>
               <ts e="T542" id="Seg_5320" n="e" s="T541">((BRK)). </ts>
               <ts e="T543" id="Seg_5322" n="e" s="T542">Dĭgəttə </ts>
               <ts e="T544" id="Seg_5324" n="e" s="T543">dĭ </ts>
               <ts e="T545" id="Seg_5326" n="e" s="T544">koŋ </ts>
               <ts e="T546" id="Seg_5328" n="e" s="T545">kirgarlaʔbə: </ts>
               <ts e="T547" id="Seg_5330" n="e" s="T546">Pʼeldə </ts>
               <ts e="T548" id="Seg_5332" n="e" s="T547">(imĭʔ-) </ts>
               <ts e="T549" id="Seg_5334" n="e" s="T548">ige,— </ts>
               <ts e="T550" id="Seg_5336" n="e" s="T549">mămbi. </ts>
               <ts e="T551" id="Seg_5338" n="e" s="T550">Măn </ts>
               <ts e="T552" id="Seg_5340" n="e" s="T551">ige </ts>
               <ts e="T553" id="Seg_5342" n="e" s="T552">tüžöjʔi, </ts>
               <ts e="T554" id="Seg_5344" n="e" s="T553">(ineʔi). </ts>
               <ts e="T555" id="Seg_5346" n="e" s="T554">Menzeŋdə </ts>
               <ts e="T556" id="Seg_5348" n="e" s="T555">mămbiem, </ts>
               <ts e="T557" id="Seg_5350" n="e" s="T556">aktʼa </ts>
               <ts e="T558" id="Seg_5352" n="e" s="T557">iʔgö, </ts>
               <ts e="T559" id="Seg_5354" n="e" s="T558">mămbi. </ts>
               <ts e="T560" id="Seg_5356" n="e" s="T559">Ĭmbidə </ts>
               <ts e="T561" id="Seg_5358" n="e" s="T560">măna </ts>
               <ts e="T562" id="Seg_5360" n="e" s="T561">ej </ts>
               <ts e="T563" id="Seg_5362" n="e" s="T562">kereʔ, </ts>
               <ts e="T564" id="Seg_5364" n="e" s="T563">tolʼkă </ts>
               <ts e="T565" id="Seg_5366" n="e" s="T564">koʔbdom </ts>
               <ts e="T566" id="Seg_5368" n="e" s="T565">izittə". </ts>
               <ts e="T567" id="Seg_5370" n="e" s="T566">((BRK)). </ts>
               <ts e="T568" id="Seg_5372" n="e" s="T567">Dĭgəttə </ts>
               <ts e="T569" id="Seg_5374" n="e" s="T568">sagər </ts>
               <ts e="T570" id="Seg_5376" n="e" s="T569">băranə </ts>
               <ts e="T571" id="Seg_5378" n="e" s="T570">bü </ts>
               <ts e="T572" id="Seg_5380" n="e" s="T571">ibi </ts>
               <ts e="T574" id="Seg_5382" n="e" s="T572">i… </ts>
               <ts e="T575" id="Seg_5384" n="e" s="T574">((BRK)). </ts>
               <ts e="T576" id="Seg_5386" n="e" s="T575">Dĭgəttə </ts>
               <ts e="T577" id="Seg_5388" n="e" s="T576">mĭluʔpi </ts>
               <ts e="T578" id="Seg_5390" n="e" s="T577">koʔbdobə, </ts>
               <ts e="T579" id="Seg_5392" n="e" s="T578">dĭ </ts>
               <ts e="T580" id="Seg_5394" n="e" s="T579">(amnolbi=) </ts>
               <ts e="T581" id="Seg_5396" n="e" s="T580">amnolbi </ts>
               <ts e="T582" id="Seg_5398" n="e" s="T581">šöjmünə </ts>
               <ts e="T583" id="Seg_5400" n="e" s="T582">i </ts>
               <ts e="T584" id="Seg_5402" n="e" s="T583">kambi </ts>
               <ts e="T585" id="Seg_5404" n="e" s="T584">maʔndə. </ts>
               <ts e="T586" id="Seg_5406" n="e" s="T585">Šide </ts>
               <ts e="T587" id="Seg_5408" n="e" s="T586">kaga </ts>
               <ts e="T588" id="Seg_5410" n="e" s="T587">(amnolaʔpi=) </ts>
               <ts e="T589" id="Seg_5412" n="e" s="T588">amnolaʔbəbi. </ts>
               <ts e="T590" id="Seg_5414" n="e" s="T589">Dĭzeŋ </ts>
               <ts e="T591" id="Seg_5416" n="e" s="T590">nezeŋdə </ts>
               <ts e="T592" id="Seg_5418" n="e" s="T591">ibiʔi. </ts>
               <ts e="T593" id="Seg_5420" n="e" s="T592">Dĭzeŋ </ts>
               <ts e="T594" id="Seg_5422" n="e" s="T593">oʔbdəbiʔi, </ts>
               <ts e="T595" id="Seg_5424" n="e" s="T594">kambiʔi </ts>
               <ts e="T596" id="Seg_5426" n="e" s="T595">dʼijenə. </ts>
               <ts e="T597" id="Seg_5428" n="e" s="T596">A </ts>
               <ts e="T598" id="Seg_5430" n="e" s="T597">nezeŋ </ts>
               <ts e="T599" id="Seg_5432" n="e" s="T598">maʔndə </ts>
               <ts e="T600" id="Seg_5434" n="e" s="T599">bar. </ts>
               <ts e="T601" id="Seg_5436" n="e" s="T600">(Nüdʼi=) </ts>
               <ts e="T602" id="Seg_5438" n="e" s="T601">Nüdʼin </ts>
               <ts e="T603" id="Seg_5440" n="e" s="T602">(amzit-) </ts>
               <ts e="T604" id="Seg_5442" n="e" s="T603">amorzittə </ts>
               <ts e="T605" id="Seg_5444" n="e" s="T604">amnəbiʔi. </ts>
               <ts e="T606" id="Seg_5446" n="e" s="T605">Dĭgəttə </ts>
               <ts e="T607" id="Seg_5448" n="e" s="T606">măndoliaʔi </ts>
               <ts e="T608" id="Seg_5450" n="e" s="T607">aspaʔən, </ts>
               <ts e="T609" id="Seg_5452" n="e" s="T608">(gittə-) </ts>
               <ts e="T610" id="Seg_5454" n="e" s="T609">girgitdə </ts>
               <ts e="T611" id="Seg_5456" n="e" s="T610">kuza </ts>
               <ts e="T612" id="Seg_5458" n="e" s="T611">amnolaʔbə. </ts>
               <ts e="T613" id="Seg_5460" n="e" s="T612">Dĭzeŋ </ts>
               <ts e="T614" id="Seg_5462" n="e" s="T613">moːʔi </ts>
               <ts e="T615" id="Seg_5464" n="e" s="T614">(со-) </ts>
               <ts e="T616" id="Seg_5466" n="e" s="T615">oʔbdəbiʔi </ts>
               <ts e="T617" id="Seg_5468" n="e" s="T616">i </ts>
               <ts e="T618" id="Seg_5470" n="e" s="T617">(aspaʔdə </ts>
               <ts e="T619" id="Seg_5472" n="e" s="T618">il-) </ts>
               <ts e="T620" id="Seg_5474" n="e" s="T619">aspaʔdə </ts>
               <ts e="T621" id="Seg_5476" n="e" s="T620">embiʔi. </ts>
               <ts e="T622" id="Seg_5478" n="e" s="T621">A </ts>
               <ts e="T623" id="Seg_5480" n="e" s="T622">dĭ </ts>
               <ts e="T624" id="Seg_5482" n="e" s="T623">mu </ts>
               <ts e="T626" id="Seg_5484" n="e" s="T624">bar </ts>
               <ts e="T627" id="Seg_5486" n="e" s="T626">(dʼo-) </ts>
               <ts e="T628" id="Seg_5488" n="e" s="T627">dʼüʔpi </ts>
               <ts e="T629" id="Seg_5490" n="e" s="T628">ibi. </ts>
               <ts e="T630" id="Seg_5492" n="e" s="T629">Dĭgəttə </ts>
               <ts e="T631" id="Seg_5494" n="e" s="T630">dĭ </ts>
               <ts e="T632" id="Seg_5496" n="e" s="T631">šobi. </ts>
               <ts e="T633" id="Seg_5498" n="e" s="T632">Ej </ts>
               <ts e="T634" id="Seg_5500" n="e" s="T633">mobi </ts>
               <ts e="T635" id="Seg_5502" n="e" s="T634">nendəsʼtə </ts>
               <ts e="T636" id="Seg_5504" n="e" s="T635">dĭ </ts>
               <ts e="T637" id="Seg_5506" n="e" s="T636">kuza. </ts>
               <ts e="T638" id="Seg_5508" n="e" s="T637">Onʼiʔ </ts>
               <ts e="T639" id="Seg_5510" n="e" s="T638">nem </ts>
               <ts e="T640" id="Seg_5512" n="e" s="T639">kutlaʔpi. </ts>
               <ts e="T641" id="Seg_5514" n="e" s="T640">Dĭgəttə </ts>
               <ts e="T642" id="Seg_5516" n="e" s="T641">šide </ts>
               <ts e="T643" id="Seg_5518" n="e" s="T642">(š-) </ts>
               <ts e="T644" id="Seg_5520" n="e" s="T643">bazoʔ </ts>
               <ts e="T645" id="Seg_5522" n="e" s="T644">nenə </ts>
               <ts e="T646" id="Seg_5524" n="e" s="T645">măndə: </ts>
               <ts e="T647" id="Seg_5526" n="e" s="T646">Ĭmbi </ts>
               <ts e="T648" id="Seg_5528" n="e" s="T647">tăn </ts>
               <ts e="T649" id="Seg_5530" n="e" s="T648">amnolaʔbəl, </ts>
               <ts e="T650" id="Seg_5532" n="e" s="T649">nendeʔ! </ts>
               <ts e="T651" id="Seg_5534" n="e" s="T650">Dĭ </ts>
               <ts e="T652" id="Seg_5536" n="e" s="T651">bar </ts>
               <ts e="T653" id="Seg_5538" n="e" s="T652">начал </ts>
               <ts e="T654" id="Seg_5540" n="e" s="T653">nendəsʼtə. </ts>
               <ts e="T655" id="Seg_5542" n="e" s="T654">Dĭ </ts>
               <ts e="T656" id="Seg_5544" n="e" s="T655">dĭʔnə </ts>
               <ts e="T657" id="Seg_5546" n="e" s="T656">baltuziʔ </ts>
               <ts e="T658" id="Seg_5548" n="e" s="T657">ulubə </ts>
               <ts e="T659" id="Seg_5550" n="e" s="T658">jaʔpi. </ts>
               <ts e="T660" id="Seg_5552" n="e" s="T659">Dĭgəttə </ts>
               <ts e="T661" id="Seg_5554" n="e" s="T660">esseŋdə </ts>
               <ts e="T662" id="Seg_5556" n="e" s="T661">kutlaːmbi. </ts>
               <ts e="T663" id="Seg_5558" n="e" s="T662">Dĭgəttə </ts>
               <ts e="T664" id="Seg_5560" n="e" s="T663">tĭ </ts>
               <ts e="T665" id="Seg_5562" n="e" s="T664">(dĭz-) </ts>
               <ts e="T666" id="Seg_5564" n="e" s="T665">dĭzeŋdə </ts>
               <ts e="T667" id="Seg_5566" n="e" s="T666">maːndə </ts>
               <ts e="T668" id="Seg_5568" n="e" s="T667">(ma-) </ts>
               <ts e="T669" id="Seg_5570" n="e" s="T668">parluʔpi. </ts>
               <ts e="T670" id="Seg_5572" n="e" s="T669">Dĭzeŋ </ts>
               <ts e="T671" id="Seg_5574" n="e" s="T670">(neze-) </ts>
               <ts e="T672" id="Seg_5576" n="e" s="T671">nezeŋdə </ts>
               <ts e="T673" id="Seg_5578" n="e" s="T672">naga. </ts>
               <ts e="T674" id="Seg_5580" n="e" s="T673">Šindidə </ts>
               <ts e="T675" id="Seg_5582" n="e" s="T674">kuʔpi </ts>
               <ts e="T676" id="Seg_5584" n="e" s="T675">essem </ts>
               <ts e="T677" id="Seg_5586" n="e" s="T676">i </ts>
               <ts e="T678" id="Seg_5588" n="e" s="T677">nezeŋdə. </ts>
               <ts e="T679" id="Seg_5590" n="e" s="T678">(Amo-) </ts>
               <ts e="T680" id="Seg_5592" n="e" s="T679">Amnobiʔi </ts>
               <ts e="T681" id="Seg_5594" n="e" s="T680">büzʼe </ts>
               <ts e="T682" id="Seg_5596" n="e" s="T681">nüketsi. </ts>
               <ts e="T683" id="Seg_5598" n="e" s="T682">Dĭzeŋ </ts>
               <ts e="T684" id="Seg_5600" n="e" s="T683">ĭmbidə </ts>
               <ts e="T685" id="Seg_5602" n="e" s="T684">nagobi, </ts>
               <ts e="T686" id="Seg_5604" n="e" s="T685">tolʼkă </ts>
               <ts e="T687" id="Seg_5606" n="e" s="T686">onʼiʔ </ts>
               <ts e="T688" id="Seg_5608" n="e" s="T687">tüžöj. </ts>
               <ts e="T689" id="Seg_5610" n="e" s="T688">Dĭzeŋ </ts>
               <ts e="T690" id="Seg_5612" n="e" s="T689">amorzittə </ts>
               <ts e="T691" id="Seg_5614" n="e" s="T690">naga </ts>
               <ts e="T692" id="Seg_5616" n="e" s="T691">ibi. </ts>
               <ts e="T693" id="Seg_5618" n="e" s="T692">Dĭgəttə </ts>
               <ts e="T694" id="Seg_5620" n="e" s="T693">tüžöjdə </ts>
               <ts e="T695" id="Seg_5622" n="e" s="T694">bătluʔpi. </ts>
               <ts e="T696" id="Seg_5624" n="e" s="T695">Nükem </ts>
               <ts e="T697" id="Seg_5626" n="e" s="T696">sürerluʔpi: </ts>
               <ts e="T698" id="Seg_5628" n="e" s="T697">Kanaʔ, </ts>
               <ts e="T699" id="Seg_5630" n="e" s="T698">a_to </ts>
               <ts e="T700" id="Seg_5632" n="e" s="T699">măna </ts>
               <ts e="T701" id="Seg_5634" n="e" s="T700">boskəndəm </ts>
               <ts e="T702" id="Seg_5636" n="e" s="T701">amga </ts>
               <ts e="T703" id="Seg_5638" n="e" s="T702">moləj. </ts>
               <ts e="T704" id="Seg_5640" n="e" s="T703">Dĭ </ts>
               <ts e="T705" id="Seg_5642" n="e" s="T704">nüke </ts>
               <ts e="T706" id="Seg_5644" n="e" s="T705">kambi, </ts>
               <ts e="T707" id="Seg_5646" n="e" s="T706">kambi. </ts>
               <ts e="T708" id="Seg_5648" n="e" s="T707">Išo </ts>
               <ts e="T709" id="Seg_5650" n="e" s="T708">maʔ </ts>
               <ts e="T710" id="Seg_5652" n="e" s="T709">kubi. </ts>
               <ts e="T711" id="Seg_5654" n="e" s="T710">Maʔdə </ts>
               <ts e="T712" id="Seg_5656" n="e" s="T711">šobi. </ts>
               <ts e="T713" id="Seg_5658" n="e" s="T712">Dĭn </ts>
               <ts e="T714" id="Seg_5660" n="e" s="T713">sʼel </ts>
               <ts e="T715" id="Seg_5662" n="e" s="T714">iʔbolaʔbə, </ts>
               <ts e="T716" id="Seg_5664" n="e" s="T715">uja </ts>
               <ts e="T717" id="Seg_5666" n="e" s="T716">iʔbolaʔbə. </ts>
               <ts e="T718" id="Seg_5668" n="e" s="T717">Dĭ </ts>
               <ts e="T719" id="Seg_5670" n="e" s="T718">mĭnzərbi </ts>
               <ts e="T720" id="Seg_5672" n="e" s="T719">bar, </ts>
               <ts e="T721" id="Seg_5674" n="e" s="T720">amorbi. </ts>
               <ts e="T722" id="Seg_5676" n="e" s="T721">Nünniet </ts>
               <ts e="T723" id="Seg_5678" n="e" s="T722">bar: </ts>
               <ts e="T724" id="Seg_5680" n="e" s="T723">šindidə </ts>
               <ts e="T725" id="Seg_5682" n="e" s="T724">šonəga. </ts>
               <ts e="T726" id="Seg_5684" n="e" s="T725">Iʔgö </ts>
               <ts e="T727" id="Seg_5686" n="e" s="T726">ular </ts>
               <ts e="T728" id="Seg_5688" n="e" s="T727">(dep-) </ts>
               <ts e="T729" id="Seg_5690" n="e" s="T728">deʔpi, </ts>
               <ts e="T730" id="Seg_5692" n="e" s="T729">tüžöjʔi, </ts>
               <ts e="T731" id="Seg_5694" n="e" s="T730">ineʔi. </ts>
               <ts e="T732" id="Seg_5696" n="e" s="T731">Dĭgəttə </ts>
               <ts e="T733" id="Seg_5698" n="e" s="T732">šobi </ts>
               <ts e="T734" id="Seg_5700" n="e" s="T733">maʔdə. </ts>
               <ts e="T735" id="Seg_5702" n="e" s="T734">Măndə: </ts>
               <ts e="T736" id="Seg_5704" n="e" s="T735">(Ka-) </ts>
               <ts e="T737" id="Seg_5706" n="e" s="T736">Kardə, </ts>
               <ts e="T738" id="Seg_5708" n="e" s="T737">aji! </ts>
               <ts e="T739" id="Seg_5710" n="e" s="T738">Aji </ts>
               <ts e="T740" id="Seg_5712" n="e" s="T739">karluʔpi, </ts>
               <ts e="T741" id="Seg_5714" n="e" s="T740">dĭ </ts>
               <ts e="T742" id="Seg_5716" n="e" s="T741">šobi, </ts>
               <ts e="T743" id="Seg_5718" n="e" s="T742">măndərla. </ts>
               <ts e="T744" id="Seg_5720" n="e" s="T743">Šindidə </ts>
               <ts e="T745" id="Seg_5722" n="e" s="T744">ibi, </ts>
               <ts e="T746" id="Seg_5724" n="e" s="T745">sʼel </ts>
               <ts e="T747" id="Seg_5726" n="e" s="T746">naga, </ts>
               <ts e="T748" id="Seg_5728" n="e" s="T747">uja </ts>
               <ts e="T749" id="Seg_5730" n="e" s="T748">naga, </ts>
               <ts e="T750" id="Seg_5732" n="e" s="T749">ipek </ts>
               <ts e="T751" id="Seg_5734" n="e" s="T750">naga. </ts>
               <ts e="T752" id="Seg_5736" n="e" s="T751">Šində </ts>
               <ts e="T753" id="Seg_5738" n="e" s="T752">dön </ts>
               <ts e="T754" id="Seg_5740" n="e" s="T753">ibi? </ts>
               <ts e="T755" id="Seg_5742" n="e" s="T754">Tibi </ts>
               <ts e="T756" id="Seg_5744" n="e" s="T755">dăk, </ts>
               <ts e="T757" id="Seg_5746" n="e" s="T756">büzʼem </ts>
               <ts e="T758" id="Seg_5748" n="e" s="T757">moləj. </ts>
               <ts e="T759" id="Seg_5750" n="e" s="T758">Ne </ts>
               <ts e="T760" id="Seg_5752" n="e" s="T759">dăk, </ts>
               <ts e="T761" id="Seg_5754" n="e" s="T760">helem </ts>
               <ts e="T762" id="Seg_5756" n="e" s="T761">moləj. </ts>
               <ts e="T763" id="Seg_5758" n="e" s="T762">Dĭgəttə </ts>
               <ts e="T764" id="Seg_5760" n="e" s="T763">dĭ </ts>
               <ts e="T765" id="Seg_5762" n="e" s="T764">nüke </ts>
               <ts e="T766" id="Seg_5764" n="e" s="T765">uʔbdəbi, </ts>
               <ts e="T767" id="Seg_5766" n="e" s="T766">šobi. </ts>
               <ts e="T768" id="Seg_5768" n="e" s="T767">Dĭ </ts>
               <ts e="T769" id="Seg_5770" n="e" s="T768">dĭm </ts>
               <ts e="T770" id="Seg_5772" n="e" s="T769">amnolbi. </ts>
               <ts e="T771" id="Seg_5774" n="e" s="T770">Amorla </ts>
               <ts e="T772" id="Seg_5776" n="e" s="T771">amnobiʔi. </ts>
               <ts e="T773" id="Seg_5778" n="e" s="T772">Dĭ </ts>
               <ts e="T774" id="Seg_5780" n="e" s="T773">surarlaʔbə: </ts>
               <ts e="T775" id="Seg_5782" n="e" s="T774">Ĭmbi </ts>
               <ts e="T776" id="Seg_5784" n="e" s="T775">tăn </ts>
               <ts e="T777" id="Seg_5786" n="e" s="T776">udal, </ts>
               <ts e="T778" id="Seg_5788" n="e" s="T777">üjül </ts>
               <ts e="T779" id="Seg_5790" n="e" s="T778">naga, </ts>
               <ts e="T780" id="Seg_5792" n="e" s="T779">kăde </ts>
               <ts e="T781" id="Seg_5794" n="e" s="T780">tăn </ts>
               <ts e="T782" id="Seg_5796" n="e" s="T781">mĭnliel? </ts>
               <ts e="T783" id="Seg_5798" n="e" s="T782">Da </ts>
               <ts e="T784" id="Seg_5800" n="e" s="T783">dăre </ts>
               <ts e="T785" id="Seg_5802" n="e" s="T784">mĭnliel. </ts>
               <ts e="T786" id="Seg_5804" n="e" s="T785">Tăn </ts>
               <ts e="T787" id="Seg_5806" n="e" s="T786">jamanə </ts>
               <ts e="T788" id="Seg_5808" n="e" s="T787">suʔmiləl? </ts>
               <ts e="T789" id="Seg_5810" n="e" s="T788">Suʔmiləm. </ts>
               <ts e="T790" id="Seg_5812" n="e" s="T789">Dĭgəttə </ts>
               <ts e="T791" id="Seg_5814" n="e" s="T790">kambiʔi. </ts>
               <ts e="T792" id="Seg_5816" n="e" s="T791">Dĭ </ts>
               <ts e="T793" id="Seg_5818" n="e" s="T792">süʔmibi, </ts>
               <ts e="T794" id="Seg_5820" n="e" s="T793">dĭ </ts>
               <ts e="T795" id="Seg_5822" n="e" s="T794">dĭʔnə </ts>
               <ts e="T796" id="Seg_5824" n="e" s="T795">baltuzi </ts>
               <ts e="T797" id="Seg_5826" n="e" s="T796">ulubə </ts>
               <ts e="T798" id="Seg_5828" n="e" s="T797">bar </ts>
               <ts e="T799" id="Seg_5830" n="e" s="T798">sajjaʔpi. </ts>
               <ts e="T800" id="Seg_5832" n="e" s="T799">Dĭgəttə </ts>
               <ts e="T801" id="Seg_5834" n="e" s="T800">bar </ts>
               <ts e="T802" id="Seg_5836" n="e" s="T801">(šama) </ts>
               <ts e="T803" id="Seg_5838" n="e" s="T802">ibi. </ts>
               <ts e="T804" id="Seg_5840" n="e" s="T803">Sʼeldə </ts>
               <ts e="T805" id="Seg_5842" n="e" s="T804">ibi, </ts>
               <ts e="T806" id="Seg_5844" n="e" s="T805">uja </ts>
               <ts e="T807" id="Seg_5846" n="e" s="T806">ibi. </ts>
               <ts e="T808" id="Seg_5848" n="e" s="T807">Puzɨrʼdə </ts>
               <ts e="T809" id="Seg_5850" n="e" s="T808">embi, </ts>
               <ts e="T810" id="Seg_5852" n="e" s="T809">dĭgəttə </ts>
               <ts e="T811" id="Seg_5854" n="e" s="T810">parluʔbi </ts>
               <ts e="T812" id="Seg_5856" n="e" s="T811">bostə </ts>
               <ts e="T813" id="Seg_5858" n="e" s="T812">büzʼendə. </ts>
               <ts e="T814" id="Seg_5860" n="e" s="T813">Maʔdə </ts>
               <ts e="T815" id="Seg_5862" n="e" s="T814">(saj- </ts>
               <ts e="T816" id="Seg_5864" n="e" s="T815">š-) </ts>
               <ts e="T817" id="Seg_5866" n="e" s="T816">sʼabi, </ts>
               <ts e="T818" id="Seg_5868" n="e" s="T817">a </ts>
               <ts e="T819" id="Seg_5870" n="e" s="T818">büzʼe </ts>
               <ts e="T820" id="Seg_5872" n="e" s="T819">kössi </ts>
               <ts e="T821" id="Seg_5874" n="e" s="T820">(uda </ts>
               <ts e="T822" id="Seg_5876" n="e" s="T821">d-) </ts>
               <ts e="T823" id="Seg_5878" n="e" s="T822">udabə </ts>
               <ts e="T825" id="Seg_5880" n="e" s="T823">bar… </ts>
               <ts e="T826" id="Seg_5882" n="e" s="T825">Udabə </ts>
               <ts e="T827" id="Seg_5884" n="e" s="T826">(kös-) </ts>
               <ts e="T828" id="Seg_5886" n="e" s="T827">kössi </ts>
               <ts e="T830" id="Seg_5888" n="e" s="T828">bar… </ts>
               <ts e="T831" id="Seg_5890" n="e" s="T830">Döm </ts>
               <ts e="T832" id="Seg_5892" n="e" s="T831">ujam </ts>
               <ts e="T833" id="Seg_5894" n="e" s="T832">amnəm, </ts>
               <ts e="T834" id="Seg_5896" n="e" s="T833">döm </ts>
               <ts e="T835" id="Seg_5898" n="e" s="T834">ujam </ts>
               <ts e="T836" id="Seg_5900" n="e" s="T835">amnəm. </ts>
               <ts e="T837" id="Seg_5902" n="e" s="T836">A </ts>
               <ts e="T838" id="Seg_5904" n="e" s="T837">dĭ </ts>
               <ts e="T839" id="Seg_5906" n="e" s="T838">barəʔluʔpi </ts>
               <ts e="T840" id="Seg_5908" n="e" s="T839">dĭʔnə. </ts>
               <ts e="T841" id="Seg_5910" n="e" s="T840">Dĭ </ts>
               <ts e="T842" id="Seg_5912" n="e" s="T841">kabarluʔpi: </ts>
               <ts e="T843" id="Seg_5914" n="e" s="T842">Vot </ts>
               <ts e="T844" id="Seg_5916" n="e" s="T843">kudaj </ts>
               <ts e="T845" id="Seg_5918" n="e" s="T844">măna </ts>
               <ts e="T846" id="Seg_5920" n="e" s="T845">mĭbi! </ts>
               <ts e="T847" id="Seg_5922" n="e" s="T846">Da, </ts>
               <ts e="T848" id="Seg_5924" n="e" s="T847">kudaj </ts>
               <ts e="T849" id="Seg_5926" n="e" s="T848">tănan </ts>
               <ts e="T850" id="Seg_5928" n="e" s="T849">mĭləj! </ts>
               <ts e="T851" id="Seg_5930" n="e" s="T850">Pi </ts>
               <ts e="T852" id="Seg_5932" n="e" s="T851">tănan </ts>
               <ts e="T853" id="Seg_5934" n="e" s="T852">mĭləj </ts>
               <ts e="T854" id="Seg_5936" n="e" s="T853">kudaj, </ts>
               <ts e="T855" id="Seg_5938" n="e" s="T854">dĭm </ts>
               <ts e="T856" id="Seg_5940" n="e" s="T855">măn </ts>
               <ts e="T857" id="Seg_5942" n="e" s="T856">mĭbiem. </ts>
               <ts e="T858" id="Seg_5944" n="e" s="T857">Dĭgəttə </ts>
               <ts e="T859" id="Seg_5946" n="e" s="T858">dĭ </ts>
               <ts e="T860" id="Seg_5948" n="e" s="T859">amorbi, </ts>
               <ts e="T861" id="Seg_5950" n="e" s="T860">kambiʔi. </ts>
               <ts e="T862" id="Seg_5952" n="e" s="T861">Dĭn </ts>
               <ts e="T863" id="Seg_5954" n="e" s="T862">ineʔi, </ts>
               <ts e="T864" id="Seg_5956" n="e" s="T863">tüžöjʔi, </ts>
               <ts e="T865" id="Seg_5958" n="e" s="T864">ularəʔi, </ts>
               <ts e="T866" id="Seg_5960" n="e" s="T865">šobiʔi </ts>
               <ts e="T867" id="Seg_5962" n="e" s="T866">dĭbər. </ts>
               <ts e="T868" id="Seg_5964" n="e" s="T867">Dĭ </ts>
               <ts e="T869" id="Seg_5966" n="e" s="T868">măndə: </ts>
               <ts e="T870" id="Seg_5968" n="e" s="T869">Pünörzittə </ts>
               <ts e="T871" id="Seg_5970" n="e" s="T870">axota! </ts>
               <ts e="T872" id="Seg_5972" n="e" s="T871">Iʔ </ts>
               <ts e="T873" id="Seg_5974" n="e" s="T872">pünöraʔ, </ts>
               <ts e="T874" id="Seg_5976" n="e" s="T873">a_to </ts>
               <ts e="T875" id="Seg_5978" n="e" s="T874">bar </ts>
               <ts e="T876" id="Seg_5980" n="e" s="T875">nuʔməluʔləʔjə! </ts>
               <ts e="T877" id="Seg_5982" n="e" s="T876">Dĭ </ts>
               <ts e="T880" id="Seg_5984" n="e" s="T877">vsʼo_taki </ts>
               <ts e="T881" id="Seg_5986" n="e" s="T880">pünörbi, </ts>
               <ts e="T882" id="Seg_5988" n="e" s="T881">i </ts>
               <ts e="T883" id="Seg_5990" n="e" s="T882">bar </ts>
               <ts e="T884" id="Seg_5992" n="e" s="T883">(inezeŋdə), </ts>
               <ts e="T885" id="Seg_5994" n="e" s="T884">ularzaŋdə </ts>
               <ts e="T886" id="Seg_5996" n="e" s="T885">i </ts>
               <ts e="T887" id="Seg_5998" n="e" s="T886">tüžöjʔjə </ts>
               <ts e="T888" id="Seg_6000" n="e" s="T887">üʔməluʔpiʔi. </ts>
               <ts e="T889" id="Seg_6002" n="e" s="T888">Dĭ </ts>
               <ts e="T890" id="Seg_6004" n="e" s="T889">davaj </ts>
               <ts e="T892" id="Seg_6006" n="e" s="T890">sütsi… </ts>
               <ts e="T893" id="Seg_6008" n="e" s="T892">((BRK)). </ts>
               <ts e="T894" id="Seg_6010" n="e" s="T893">(süt-) </ts>
               <ts e="T895" id="Seg_6012" n="e" s="T894">sütsi </ts>
               <ts e="T896" id="Seg_6014" n="e" s="T895">kămnasʼtə </ts>
               <ts e="T897" id="Seg_6016" n="e" s="T896">dĭzeŋdə. </ts>
               <ts e="T898" id="Seg_6018" n="e" s="T897">Ular, </ts>
               <ts e="T899" id="Seg_6020" n="e" s="T898">šiʔ </ts>
               <ts e="T900" id="Seg_6022" n="e" s="T899">poʔto </ts>
               <ts e="T901" id="Seg_6024" n="e" s="T900">molaʔ! </ts>
               <ts e="T902" id="Seg_6026" n="e" s="T901">Tüžöjəʔi, </ts>
               <ts e="T903" id="Seg_6028" n="e" s="T902">(šiʔ=) </ts>
               <ts e="T904" id="Seg_6030" n="e" s="T903">šiʔ </ts>
               <ts e="T905" id="Seg_6032" n="e" s="T904">pulan </ts>
               <ts e="T906" id="Seg_6034" n="e" s="T905">mogaʔ! </ts>
               <ts e="T907" id="Seg_6036" n="e" s="T906">A </ts>
               <ts e="T908" id="Seg_6038" n="e" s="T907">šiʔ, </ts>
               <ts e="T910" id="Seg_6040" n="e" s="T908">ineʔi…" </ts>
               <ts e="T911" id="Seg_6042" n="e" s="T910">Nöməlluʔpiem. </ts>
               <ts e="T912" id="Seg_6044" n="e" s="T911">((BRK)). </ts>
               <ts e="T913" id="Seg_6046" n="e" s="T912">"Ineʔi, </ts>
               <ts e="T914" id="Seg_6048" n="e" s="T913">(măm-) </ts>
               <ts e="T915" id="Seg_6050" n="e" s="T914">mămbi, </ts>
               <ts e="T916" id="Seg_6052" n="e" s="T915">pušaj </ts>
               <ts e="T917" id="Seg_6054" n="e" s="T916">izʼubrʼaʔi </ts>
               <ts e="T918" id="Seg_6056" n="e" s="T917">moluʔjəʔ. </ts>
               <ts e="T919" id="Seg_6058" n="e" s="T918">Dĭgəttə </ts>
               <ts e="T920" id="Seg_6060" n="e" s="T919">nüke </ts>
               <ts e="T921" id="Seg_6062" n="e" s="T920">i </ts>
               <ts e="T922" id="Seg_6064" n="e" s="T921">büzʼe </ts>
               <ts e="T923" id="Seg_6066" n="e" s="T922">baʔluʔbi, </ts>
               <ts e="T924" id="Seg_6068" n="e" s="T923">ĭmbidə </ts>
               <ts e="T925" id="Seg_6070" n="e" s="T924">bazoʔ </ts>
               <ts e="T926" id="Seg_6072" n="e" s="T925">naga </ts>
               <ts e="T927" id="Seg_6074" n="e" s="T926">dĭzeŋdə. </ts>
               <ts e="T928" id="Seg_6076" n="e" s="T927">Bar </ts>
               <ts e="T1073" id="Seg_6078" n="e" s="T928">kalla </ts>
               <ts e="T1071" id="Seg_6080" n="e" s="T1073">dʼürbiʔi. </ts>
               <ts e="T929" id="Seg_6082" n="e" s="T1071">((BRK)) </ts>
               <ts e="T930" id="Seg_6084" n="e" s="T929">Amnobi </ts>
               <ts e="T931" id="Seg_6086" n="e" s="T930">büzʼe, </ts>
               <ts e="T932" id="Seg_6088" n="e" s="T931">dĭn </ts>
               <ts e="T933" id="Seg_6090" n="e" s="T932">nagur </ts>
               <ts e="T934" id="Seg_6092" n="e" s="T933">koʔbdot </ts>
               <ts e="T935" id="Seg_6094" n="e" s="T934">ibi. </ts>
               <ts e="T936" id="Seg_6096" n="e" s="T935">Dĭm </ts>
               <ts e="T937" id="Seg_6098" n="e" s="T936">kăštəbiʔi </ts>
               <ts e="T1075" id="Seg_6100" n="e" s="T937">Kazan </ts>
               <ts e="T938" id="Seg_6102" n="e" s="T1075">turanə </ts>
               <ts e="T939" id="Seg_6104" n="e" s="T938">štobɨ </ts>
               <ts e="T940" id="Seg_6106" n="e" s="T939">šobi. </ts>
               <ts e="T941" id="Seg_6108" n="e" s="T940">A </ts>
               <ts e="T942" id="Seg_6110" n="e" s="T941">urgo </ts>
               <ts e="T943" id="Seg_6112" n="e" s="T942">koʔbdot: </ts>
               <ts e="T944" id="Seg_6114" n="e" s="T943">Măn </ts>
               <ts e="T945" id="Seg_6116" n="e" s="T944">kalam! </ts>
               <ts e="T946" id="Seg_6118" n="e" s="T945">Eʔbdəbə </ts>
               <ts e="T947" id="Seg_6120" n="e" s="T946">(bă-) </ts>
               <ts e="T948" id="Seg_6122" n="e" s="T947">băʔpi. </ts>
               <ts e="T949" id="Seg_6124" n="e" s="T948">Piʔmə </ts>
               <ts e="T950" id="Seg_6126" n="e" s="T949">šerbi. </ts>
               <ts e="T951" id="Seg_6128" n="e" s="T950">Kujnek </ts>
               <ts e="T952" id="Seg_6130" n="e" s="T951">šerbi. </ts>
               <ts e="T953" id="Seg_6132" n="e" s="T952">Üžü </ts>
               <ts e="T954" id="Seg_6134" n="e" s="T953">šerbi, </ts>
               <ts e="T955" id="Seg_6136" n="e" s="T954">kambi. </ts>
               <ts e="T956" id="Seg_6138" n="e" s="T955">A </ts>
               <ts e="T957" id="Seg_6140" n="e" s="T956">abat </ts>
               <ts e="T958" id="Seg_6142" n="e" s="T957">urgaːbazi </ts>
               <ts e="T959" id="Seg_6144" n="e" s="T958">abi. </ts>
               <ts e="T960" id="Seg_6146" n="e" s="T959">I </ts>
               <ts e="T961" id="Seg_6148" n="e" s="T960">dĭʔnə </ts>
               <ts e="T962" id="Seg_6150" n="e" s="T961">šonəga. </ts>
               <ts e="T963" id="Seg_6152" n="e" s="T962">Dĭ </ts>
               <ts e="T964" id="Seg_6154" n="e" s="T963">bar </ts>
               <ts e="T965" id="Seg_6156" n="e" s="T964">nereʔluʔpi </ts>
               <ts e="T966" id="Seg_6158" n="e" s="T965">i </ts>
               <ts e="T967" id="Seg_6160" n="e" s="T966">maʔndə </ts>
               <ts e="T968" id="Seg_6162" n="e" s="T967">nuʔməluʔpi. </ts>
               <ts e="T969" id="Seg_6164" n="e" s="T968">Dĭ </ts>
               <ts e="T970" id="Seg_6166" n="e" s="T969">šobi: </ts>
               <ts e="T971" id="Seg_6168" n="e" s="T970">Ĭmbi </ts>
               <ts e="T972" id="Seg_6170" n="e" s="T971">ej </ts>
               <ts e="T973" id="Seg_6172" n="e" s="T972">kambiam? </ts>
               <ts e="T974" id="Seg_6174" n="e" s="T973">(Там=) </ts>
               <ts e="T975" id="Seg_6176" n="e" s="T974">Dĭn </ts>
               <ts e="T976" id="Seg_6178" n="e" s="T975">urgaːba, </ts>
               <ts e="T977" id="Seg_6180" n="e" s="T976">măn </ts>
               <ts e="T978" id="Seg_6182" n="e" s="T977">nereʔluʔpiem, </ts>
               <ts e="T979" id="Seg_6184" n="e" s="T978">maʔndə </ts>
               <ts e="T980" id="Seg_6186" n="e" s="T979">šobiam. </ts>
               <ts e="T981" id="Seg_6188" n="e" s="T980">Dĭgəttə </ts>
               <ts e="T982" id="Seg_6190" n="e" s="T981">baška </ts>
               <ts e="T983" id="Seg_6192" n="e" s="T982">koʔbdo: </ts>
               <ts e="T984" id="Seg_6194" n="e" s="T983">Măn </ts>
               <ts e="T985" id="Seg_6196" n="e" s="T984">kalam! </ts>
               <ts e="T986" id="Seg_6198" n="e" s="T985">No, </ts>
               <ts e="T987" id="Seg_6200" n="e" s="T986">kanaʔ. </ts>
               <ts e="T988" id="Seg_6202" n="e" s="T987">Šerbi </ts>
               <ts e="T989" id="Seg_6204" n="e" s="T988">piʔməʔi, </ts>
               <ts e="T990" id="Seg_6206" n="e" s="T989">kujnek </ts>
               <ts e="T991" id="Seg_6208" n="e" s="T990">šerbi. </ts>
               <ts e="T992" id="Seg_6210" n="e" s="T991">Eʔbdəbə </ts>
               <ts e="T993" id="Seg_6212" n="e" s="T992">băʔpi. </ts>
               <ts e="T994" id="Seg_6214" n="e" s="T993">Üžübə </ts>
               <ts e="T995" id="Seg_6216" n="e" s="T994">šerbi, </ts>
               <ts e="T996" id="Seg_6218" n="e" s="T995">kambi. </ts>
               <ts e="T997" id="Seg_6220" n="e" s="T996">Dĭgəttə </ts>
               <ts e="T998" id="Seg_6222" n="e" s="T997">abat </ts>
               <ts e="T999" id="Seg_6224" n="e" s="T998">molaːmbi </ts>
               <ts e="T1000" id="Seg_6226" n="e" s="T999">urgaːbazi, </ts>
               <ts e="T1001" id="Seg_6228" n="e" s="T1000">kambi. </ts>
               <ts e="T1002" id="Seg_6230" n="e" s="T1001">Dĭ </ts>
               <ts e="T1003" id="Seg_6232" n="e" s="T1002">bar </ts>
               <ts e="T1004" id="Seg_6234" n="e" s="T1003">nereʔluʔpi, </ts>
               <ts e="T1005" id="Seg_6236" n="e" s="T1004">maʔndə </ts>
               <ts e="T1006" id="Seg_6238" n="e" s="T1005">šobi. </ts>
               <ts e="T1007" id="Seg_6240" n="e" s="T1006">Ĭmbi </ts>
               <ts e="T1008" id="Seg_6242" n="e" s="T1007">šobial? </ts>
               <ts e="T1009" id="Seg_6244" n="e" s="T1008">Da </ts>
               <ts e="T1010" id="Seg_6246" n="e" s="T1009">dĭn </ts>
               <ts e="T1011" id="Seg_6248" n="e" s="T1010">(urgaːba) </ts>
               <ts e="T1012" id="Seg_6250" n="e" s="T1011">šonəga, </ts>
               <ts e="T1013" id="Seg_6252" n="e" s="T1012">măn </ts>
               <ts e="T1014" id="Seg_6254" n="e" s="T1013">nereʔluʔpiem, </ts>
               <ts e="T1015" id="Seg_6256" n="e" s="T1014">maʔndə </ts>
               <ts e="T1016" id="Seg_6258" n="e" s="T1015">nuʔməluʔpiem. </ts>
               <ts e="T1017" id="Seg_6260" n="e" s="T1016">Kabarləj. </ts>
               <ts e="T1018" id="Seg_6262" n="e" s="T1017">((BRK)). </ts>
               <ts e="T1019" id="Seg_6264" n="e" s="T1018">Dĭgəttə </ts>
               <ts e="T1020" id="Seg_6266" n="e" s="T1019">üdʼüge </ts>
               <ts e="T1021" id="Seg_6268" n="e" s="T1020">koʔbdo: </ts>
               <ts e="T1022" id="Seg_6270" n="e" s="T1021">Măn </ts>
               <ts e="T1023" id="Seg_6272" n="e" s="T1022">kalam! </ts>
               <ts e="T1024" id="Seg_6274" n="e" s="T1023">No, </ts>
               <ts e="T1025" id="Seg_6276" n="e" s="T1024">kanaʔ. </ts>
               <ts e="T1026" id="Seg_6278" n="e" s="T1025">Tože </ts>
               <ts e="T1027" id="Seg_6280" n="e" s="T1026">dăre </ts>
               <ts e="T1028" id="Seg_6282" n="e" s="T1027">že </ts>
               <ts e="T1029" id="Seg_6284" n="e" s="T1028">eʔbdəbə </ts>
               <ts e="T1030" id="Seg_6286" n="e" s="T1029">bătluʔpi. </ts>
               <ts e="T1031" id="Seg_6288" n="e" s="T1030">Piʔməʔi </ts>
               <ts e="T1032" id="Seg_6290" n="e" s="T1031">šerbi. </ts>
               <ts e="T1033" id="Seg_6292" n="e" s="T1032">Kujnek </ts>
               <ts e="T1034" id="Seg_6294" n="e" s="T1033">šerbi. </ts>
               <ts e="T1035" id="Seg_6296" n="e" s="T1034">I </ts>
               <ts e="T1036" id="Seg_6298" n="e" s="T1035">üžü </ts>
               <ts e="T1037" id="Seg_6300" n="e" s="T1036">šerbi, </ts>
               <ts e="T1038" id="Seg_6302" n="e" s="T1037">kambi. </ts>
               <ts e="T1039" id="Seg_6304" n="e" s="T1038">Kandəga, </ts>
               <ts e="T1040" id="Seg_6306" n="e" s="T1039">kandəga. </ts>
               <ts e="T1041" id="Seg_6308" n="e" s="T1040">Urgaːba </ts>
               <ts e="T1042" id="Seg_6310" n="e" s="T1041">šonəga. </ts>
               <ts e="T1043" id="Seg_6312" n="e" s="T1042">Dĭ </ts>
               <ts e="T1044" id="Seg_6314" n="e" s="T1043">strelazi </ts>
               <ts e="T1045" id="Seg_6316" n="e" s="T1044">bar </ts>
               <ts e="T1046" id="Seg_6318" n="e" s="T1045">dĭm </ts>
               <ts e="T1047" id="Seg_6320" n="e" s="T1046">(ku-) </ts>
               <ts e="T1048" id="Seg_6322" n="e" s="T1047">(simandə=) </ts>
               <ts e="T1049" id="Seg_6324" n="e" s="T1048">simat </ts>
               <ts e="T1051" id="Seg_6326" n="e" s="T1049">bar… </ts>
               <ts e="T1052" id="Seg_6328" n="e" s="T1051">Dĭ </ts>
               <ts e="T1053" id="Seg_6330" n="e" s="T1052">strelazi </ts>
               <ts e="T1054" id="Seg_6332" n="e" s="T1053">simandə </ts>
               <ts e="T1055" id="Seg_6334" n="e" s="T1054">toʔnarbi, </ts>
               <ts e="T1056" id="Seg_6336" n="e" s="T1055">i </ts>
               <ts e="T1057" id="Seg_6338" n="e" s="T1056">simat </ts>
               <ts e="T1058" id="Seg_6340" n="e" s="T1057">(naga-) </ts>
               <ts e="T1059" id="Seg_6342" n="e" s="T1058">nagoluʔpi. </ts>
               <ts e="T1060" id="Seg_6344" n="e" s="T1059">Dĭgəttə </ts>
               <ts e="T1061" id="Seg_6346" n="e" s="T1060">kambi, </ts>
               <ts e="T1062" id="Seg_6348" n="e" s="T1061">kambi. </ts>
               <ts e="T1063" id="Seg_6350" n="e" s="T1062">Šonəga, </ts>
               <ts e="T1064" id="Seg_6352" n="e" s="T1063">nüke </ts>
               <ts e="T1065" id="Seg_6354" n="e" s="T1064">amnolaʔbə </ts>
               <ts e="T1066" id="Seg_6356" n="e" s="T1065">i </ts>
               <ts e="T1067" id="Seg_6358" n="e" s="T1066">nüken </ts>
               <ts e="T1068" id="Seg_6360" n="e" s="T1067">nʼit </ts>
               <ts e="T1069" id="Seg_6362" n="e" s="T1068">amnolaʔbə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PKZ">
            <ta e="T4" id="Seg_6363" s="T0">PKZ_196X_SU0225.001 (001)</ta>
            <ta e="T8" id="Seg_6364" s="T4">PKZ_196X_SU0225.002 (002)</ta>
            <ta e="T10" id="Seg_6365" s="T8">PKZ_196X_SU0225.003 (003)</ta>
            <ta e="T13" id="Seg_6366" s="T10">PKZ_196X_SU0225.004 (004)</ta>
            <ta e="T18" id="Seg_6367" s="T13">PKZ_196X_SU0225.005 (005)</ta>
            <ta e="T24" id="Seg_6368" s="T18">PKZ_196X_SU0225.006 (006)</ta>
            <ta e="T26" id="Seg_6369" s="T24">PKZ_196X_SU0225.007 (007)</ta>
            <ta e="T29" id="Seg_6370" s="T26">PKZ_196X_SU0225.008 (008)</ta>
            <ta e="T34" id="Seg_6371" s="T29">PKZ_196X_SU0225.009 (009)</ta>
            <ta e="T35" id="Seg_6372" s="T34">PKZ_196X_SU0225.010 (010)</ta>
            <ta e="T36" id="Seg_6373" s="T35">PKZ_196X_SU0225.011 (011)</ta>
            <ta e="T42" id="Seg_6374" s="T36">PKZ_196X_SU0225.012 (012)</ta>
            <ta e="T48" id="Seg_6375" s="T42">PKZ_196X_SU0225.013 (013)</ta>
            <ta e="T57" id="Seg_6376" s="T48">PKZ_196X_SU0225.014 (014) </ta>
            <ta e="T64" id="Seg_6377" s="T57">PKZ_196X_SU0225.015 (016)</ta>
            <ta e="T67" id="Seg_6378" s="T64">PKZ_196X_SU0225.016 (017)</ta>
            <ta e="T71" id="Seg_6379" s="T67">PKZ_196X_SU0225.017 (018)</ta>
            <ta e="T77" id="Seg_6380" s="T71">PKZ_196X_SU0225.018 (019)</ta>
            <ta e="T80" id="Seg_6381" s="T77">PKZ_196X_SU0225.019 (020)</ta>
            <ta e="T81" id="Seg_6382" s="T80">PKZ_196X_SU0225.020 (021)</ta>
            <ta e="T90" id="Seg_6383" s="T81">PKZ_196X_SU0225.021 (022)</ta>
            <ta e="T92" id="Seg_6384" s="T90">PKZ_196X_SU0225.022 (023)</ta>
            <ta e="T93" id="Seg_6385" s="T92">PKZ_196X_SU0225.023 (024)</ta>
            <ta e="T96" id="Seg_6386" s="T93">PKZ_196X_SU0225.024 (025)</ta>
            <ta e="T101" id="Seg_6387" s="T96">PKZ_196X_SU0225.025 (026)</ta>
            <ta e="T105" id="Seg_6388" s="T101">PKZ_196X_SU0225.026 (027)</ta>
            <ta e="T107" id="Seg_6389" s="T105">PKZ_196X_SU0225.027 (028)</ta>
            <ta e="T111" id="Seg_6390" s="T107">PKZ_196X_SU0225.028 (029)</ta>
            <ta e="T116" id="Seg_6391" s="T111">PKZ_196X_SU0225.029 (030)</ta>
            <ta e="T120" id="Seg_6392" s="T116">PKZ_196X_SU0225.030 (031)</ta>
            <ta e="T123" id="Seg_6393" s="T120">PKZ_196X_SU0225.031 (032)</ta>
            <ta e="T124" id="Seg_6394" s="T123">PKZ_196X_SU0225.032 (033)</ta>
            <ta e="T129" id="Seg_6395" s="T124">PKZ_196X_SU0225.033 (034)</ta>
            <ta e="T133" id="Seg_6396" s="T129">PKZ_196X_SU0225.034 (035)</ta>
            <ta e="T136" id="Seg_6397" s="T133">PKZ_196X_SU0225.035 (036)</ta>
            <ta e="T145" id="Seg_6398" s="T136">PKZ_196X_SU0225.036 (037)</ta>
            <ta e="T146" id="Seg_6399" s="T145">PKZ_196X_SU0225.037 (038)</ta>
            <ta e="T147" id="Seg_6400" s="T146">PKZ_196X_SU0225.038 (039)</ta>
            <ta e="T156" id="Seg_6401" s="T147">PKZ_196X_SU0225.039 (040)</ta>
            <ta e="T157" id="Seg_6402" s="T156">PKZ_196X_SU0225.040 (041)</ta>
            <ta e="T168" id="Seg_6403" s="T157">PKZ_196X_SU0225.041 (042)</ta>
            <ta e="T175" id="Seg_6404" s="T168">PKZ_196X_SU0225.042 (043)</ta>
            <ta e="T180" id="Seg_6405" s="T175">PKZ_196X_SU0225.043 (044)</ta>
            <ta e="T181" id="Seg_6406" s="T180">PKZ_196X_SU0225.044 (045)</ta>
            <ta e="T182" id="Seg_6407" s="T181">PKZ_196X_SU0225.045 (046)</ta>
            <ta e="T186" id="Seg_6408" s="T182">PKZ_196X_SU0225.046 (047)</ta>
            <ta e="T188" id="Seg_6409" s="T186">PKZ_196X_SU0225.047 (048)</ta>
            <ta e="T196" id="Seg_6410" s="T188">PKZ_196X_SU0225.048 (049)</ta>
            <ta e="T197" id="Seg_6411" s="T196">PKZ_196X_SU0225.049 (050)</ta>
            <ta e="T200" id="Seg_6412" s="T197">PKZ_196X_SU0225.050 (051)</ta>
            <ta e="T206" id="Seg_6413" s="T200">PKZ_196X_SU0225.051 (052)</ta>
            <ta e="T207" id="Seg_6414" s="T206">PKZ_196X_SU0225.052 (053)</ta>
            <ta e="T212" id="Seg_6415" s="T207">PKZ_196X_SU0225.053 (054)</ta>
            <ta e="T216" id="Seg_6416" s="T212">PKZ_196X_SU0225.054 (055)</ta>
            <ta e="T221" id="Seg_6417" s="T216">PKZ_196X_SU0225.055 (056)</ta>
            <ta e="T225" id="Seg_6418" s="T221">PKZ_196X_SU0225.056 (057)</ta>
            <ta e="T226" id="Seg_6419" s="T225">PKZ_196X_SU0225.057 (058)</ta>
            <ta e="T230" id="Seg_6420" s="T226">PKZ_196X_SU0225.058 (059)</ta>
            <ta e="T235" id="Seg_6421" s="T230">PKZ_196X_SU0225.059 (060)</ta>
            <ta e="T236" id="Seg_6422" s="T235">PKZ_196X_SU0225.060 (061)</ta>
            <ta e="T238" id="Seg_6423" s="T236">PKZ_196X_SU0225.061 (062)</ta>
            <ta e="T243" id="Seg_6424" s="T238">PKZ_196X_SU0225.062 (063)</ta>
            <ta e="T244" id="Seg_6425" s="T243">PKZ_196X_SU0225.063 (064)</ta>
            <ta e="T251" id="Seg_6426" s="T244">PKZ_196X_SU0225.064 (065)</ta>
            <ta e="T258" id="Seg_6427" s="T251">PKZ_196X_SU0225.065 (066)</ta>
            <ta e="T259" id="Seg_6428" s="T258">PKZ_196X_SU0225.066 (067)</ta>
            <ta e="T262" id="Seg_6429" s="T259">PKZ_196X_SU0225.067 (068)</ta>
            <ta e="T265" id="Seg_6430" s="T262">PKZ_196X_SU0225.068 (069)</ta>
            <ta e="T276" id="Seg_6431" s="T265">PKZ_196X_SU0225.069 (070)</ta>
            <ta e="T280" id="Seg_6432" s="T276">PKZ_196X_SU0225.070 (071)</ta>
            <ta e="T283" id="Seg_6433" s="T280">PKZ_196X_SU0225.071 (072)</ta>
            <ta e="T287" id="Seg_6434" s="T283">PKZ_196X_SU0225.072 (073)</ta>
            <ta e="T289" id="Seg_6435" s="T287">PKZ_196X_SU0225.073 (074)</ta>
            <ta e="T292" id="Seg_6436" s="T289">PKZ_196X_SU0225.074 (075)</ta>
            <ta e="T295" id="Seg_6437" s="T292">PKZ_196X_SU0225.075 (076)</ta>
            <ta e="T300" id="Seg_6438" s="T295">PKZ_196X_SU0225.076 (077)</ta>
            <ta e="T307" id="Seg_6439" s="T300">PKZ_196X_SU0225.077 (078)</ta>
            <ta e="T312" id="Seg_6440" s="T307">PKZ_196X_SU0225.078 (079)</ta>
            <ta e="T316" id="Seg_6441" s="T312">PKZ_196X_SU0225.079 (080)</ta>
            <ta e="T321" id="Seg_6442" s="T316">PKZ_196X_SU0225.080 (081)</ta>
            <ta e="T323" id="Seg_6443" s="T321">PKZ_196X_SU0225.081 (082)</ta>
            <ta e="T328" id="Seg_6444" s="T323">PKZ_196X_SU0225.082 (083)</ta>
            <ta e="T330" id="Seg_6445" s="T328">PKZ_196X_SU0225.083 (084)</ta>
            <ta e="T336" id="Seg_6446" s="T330">PKZ_196X_SU0225.084 (085)</ta>
            <ta e="T339" id="Seg_6447" s="T336">PKZ_196X_SU0225.085 (086)</ta>
            <ta e="T341" id="Seg_6448" s="T339">PKZ_196X_SU0225.086 (087)</ta>
            <ta e="T344" id="Seg_6449" s="T341">PKZ_196X_SU0225.087 (088)</ta>
            <ta e="T350" id="Seg_6450" s="T344">PKZ_196X_SU0225.088 (089)</ta>
            <ta e="T354" id="Seg_6451" s="T350">PKZ_196X_SU0225.089 (090)</ta>
            <ta e="T359" id="Seg_6452" s="T354">PKZ_196X_SU0225.090 (091)</ta>
            <ta e="T360" id="Seg_6453" s="T359">PKZ_196X_SU0225.091 (092)</ta>
            <ta e="T368" id="Seg_6454" s="T360">PKZ_196X_SU0225.092 (093)</ta>
            <ta e="T371" id="Seg_6455" s="T368">PKZ_196X_SU0225.093 (094)</ta>
            <ta e="T374" id="Seg_6456" s="T371">PKZ_196X_SU0225.094 (095)</ta>
            <ta e="T380" id="Seg_6457" s="T374">PKZ_196X_SU0225.095 (096)</ta>
            <ta e="T384" id="Seg_6458" s="T380">PKZ_196X_SU0225.096 (097)</ta>
            <ta e="T386" id="Seg_6459" s="T384">PKZ_196X_SU0225.097 (098)</ta>
            <ta e="T393" id="Seg_6460" s="T386">PKZ_196X_SU0225.098 (099)</ta>
            <ta e="T394" id="Seg_6461" s="T393">PKZ_196X_SU0225.099 (100)</ta>
            <ta e="T396" id="Seg_6462" s="T394">PKZ_196X_SU0225.100 (101)</ta>
            <ta e="T399" id="Seg_6463" s="T396">PKZ_196X_SU0225.101 (102)</ta>
            <ta e="T406" id="Seg_6464" s="T399">PKZ_196X_SU0225.102 (103)</ta>
            <ta e="T410" id="Seg_6465" s="T406">PKZ_196X_SU0225.103 (104)</ta>
            <ta e="T411" id="Seg_6466" s="T410">PKZ_196X_SU0225.104 (105)</ta>
            <ta e="T422" id="Seg_6467" s="T411">PKZ_196X_SU0225.105 (106) </ta>
            <ta e="T424" id="Seg_6468" s="T422">PKZ_196X_SU0225.106 (108)</ta>
            <ta e="T428" id="Seg_6469" s="T424">PKZ_196X_SU0225.107 (109)</ta>
            <ta e="T431" id="Seg_6470" s="T428">PKZ_196X_SU0225.108 (110)</ta>
            <ta e="T440" id="Seg_6471" s="T431">PKZ_196X_SU0225.109 (111) </ta>
            <ta e="T447" id="Seg_6472" s="T440">PKZ_196X_SU0225.110 (113)</ta>
            <ta e="T448" id="Seg_6473" s="T447">PKZ_196X_SU0225.111 (114)</ta>
            <ta e="T451" id="Seg_6474" s="T448">PKZ_196X_SU0225.112 (115)</ta>
            <ta e="T458" id="Seg_6475" s="T451">PKZ_196X_SU0225.113 (116)</ta>
            <ta e="T464" id="Seg_6476" s="T458">PKZ_196X_SU0225.114 (117)</ta>
            <ta e="T469" id="Seg_6477" s="T464">PKZ_196X_SU0225.115 (118)</ta>
            <ta e="T475" id="Seg_6478" s="T469">PKZ_196X_SU0225.116 (119)</ta>
            <ta e="T480" id="Seg_6479" s="T475">PKZ_196X_SU0225.117 (120)</ta>
            <ta e="T493" id="Seg_6480" s="T480">PKZ_196X_SU0225.118 (121)</ta>
            <ta e="T497" id="Seg_6481" s="T493">PKZ_196X_SU0225.119 (122)</ta>
            <ta e="T498" id="Seg_6482" s="T497">PKZ_196X_SU0225.120 (123)</ta>
            <ta e="T504" id="Seg_6483" s="T498">PKZ_196X_SU0225.121 (124)</ta>
            <ta e="T508" id="Seg_6484" s="T504">PKZ_196X_SU0225.122 (125)</ta>
            <ta e="T511" id="Seg_6485" s="T508">PKZ_196X_SU0225.123 (126)</ta>
            <ta e="T518" id="Seg_6486" s="T511">PKZ_196X_SU0225.124 (127)</ta>
            <ta e="T527" id="Seg_6487" s="T518">PKZ_196X_SU0225.125 (128)</ta>
            <ta e="T534" id="Seg_6488" s="T527">PKZ_196X_SU0225.126 (129)</ta>
            <ta e="T541" id="Seg_6489" s="T534">PKZ_196X_SU0225.127 (130)</ta>
            <ta e="T542" id="Seg_6490" s="T541">PKZ_196X_SU0225.128 (131)</ta>
            <ta e="T550" id="Seg_6491" s="T542">PKZ_196X_SU0225.129 (132) </ta>
            <ta e="T554" id="Seg_6492" s="T550">PKZ_196X_SU0225.130 (134)</ta>
            <ta e="T559" id="Seg_6493" s="T554">PKZ_196X_SU0225.131 (135)</ta>
            <ta e="T566" id="Seg_6494" s="T559">PKZ_196X_SU0225.132 (136)</ta>
            <ta e="T567" id="Seg_6495" s="T566">PKZ_196X_SU0225.133 (137)</ta>
            <ta e="T574" id="Seg_6496" s="T567">PKZ_196X_SU0225.134 (138)</ta>
            <ta e="T575" id="Seg_6497" s="T574">PKZ_196X_SU0225.135 (139)</ta>
            <ta e="T585" id="Seg_6498" s="T575">PKZ_196X_SU0225.136 (140)</ta>
            <ta e="T589" id="Seg_6499" s="T585">PKZ_196X_SU0225.137 (141)</ta>
            <ta e="T592" id="Seg_6500" s="T589">PKZ_196X_SU0225.138 (142)</ta>
            <ta e="T596" id="Seg_6501" s="T592">PKZ_196X_SU0225.139 (143)</ta>
            <ta e="T600" id="Seg_6502" s="T596">PKZ_196X_SU0225.140 (144)</ta>
            <ta e="T605" id="Seg_6503" s="T600">PKZ_196X_SU0225.141 (145)</ta>
            <ta e="T612" id="Seg_6504" s="T605">PKZ_196X_SU0225.142 (146)</ta>
            <ta e="T621" id="Seg_6505" s="T612">PKZ_196X_SU0225.143 (147)</ta>
            <ta e="T629" id="Seg_6506" s="T621">PKZ_196X_SU0225.144 (148)</ta>
            <ta e="T632" id="Seg_6507" s="T629">PKZ_196X_SU0225.145 (149)</ta>
            <ta e="T637" id="Seg_6508" s="T632">PKZ_196X_SU0225.146 (150)</ta>
            <ta e="T640" id="Seg_6509" s="T637">PKZ_196X_SU0225.147 (151)</ta>
            <ta e="T650" id="Seg_6510" s="T640">PKZ_196X_SU0225.148 (152)</ta>
            <ta e="T654" id="Seg_6511" s="T650">PKZ_196X_SU0225.149 (153)</ta>
            <ta e="T659" id="Seg_6512" s="T654">PKZ_196X_SU0225.150 (154)</ta>
            <ta e="T662" id="Seg_6513" s="T659">PKZ_196X_SU0225.151 (155)</ta>
            <ta e="T669" id="Seg_6514" s="T662">PKZ_196X_SU0225.152 (156)</ta>
            <ta e="T673" id="Seg_6515" s="T669">PKZ_196X_SU0225.153 (157)</ta>
            <ta e="T678" id="Seg_6516" s="T673">PKZ_196X_SU0225.154 (158)</ta>
            <ta e="T682" id="Seg_6517" s="T678">PKZ_196X_SU0225.155 (159)</ta>
            <ta e="T688" id="Seg_6518" s="T682">PKZ_196X_SU0225.156 (160)</ta>
            <ta e="T692" id="Seg_6519" s="T688">PKZ_196X_SU0225.157 (161)</ta>
            <ta e="T695" id="Seg_6520" s="T692">PKZ_196X_SU0225.158 (162)</ta>
            <ta e="T703" id="Seg_6521" s="T695">PKZ_196X_SU0225.159 (163)</ta>
            <ta e="T707" id="Seg_6522" s="T703">PKZ_196X_SU0225.160 (164)</ta>
            <ta e="T710" id="Seg_6523" s="T707">PKZ_196X_SU0225.161 (165)</ta>
            <ta e="T712" id="Seg_6524" s="T710">PKZ_196X_SU0225.162 (166)</ta>
            <ta e="T717" id="Seg_6525" s="T712">PKZ_196X_SU0225.163 (167)</ta>
            <ta e="T721" id="Seg_6526" s="T717">PKZ_196X_SU0225.164 (168)</ta>
            <ta e="T725" id="Seg_6527" s="T721">PKZ_196X_SU0225.165 (169)</ta>
            <ta e="T731" id="Seg_6528" s="T725">PKZ_196X_SU0225.166 (170)</ta>
            <ta e="T734" id="Seg_6529" s="T731">PKZ_196X_SU0225.167 (171)</ta>
            <ta e="T738" id="Seg_6530" s="T734">PKZ_196X_SU0225.168 (172)</ta>
            <ta e="T743" id="Seg_6531" s="T738">PKZ_196X_SU0225.169 (173)</ta>
            <ta e="T751" id="Seg_6532" s="T743">PKZ_196X_SU0225.170 (174)</ta>
            <ta e="T754" id="Seg_6533" s="T751">PKZ_196X_SU0225.171 (175)</ta>
            <ta e="T758" id="Seg_6534" s="T754">PKZ_196X_SU0225.172 (176)</ta>
            <ta e="T762" id="Seg_6535" s="T758">PKZ_196X_SU0225.173 (177)</ta>
            <ta e="T767" id="Seg_6536" s="T762">PKZ_196X_SU0225.174 (178)</ta>
            <ta e="T770" id="Seg_6537" s="T767">PKZ_196X_SU0225.175 (179)</ta>
            <ta e="T772" id="Seg_6538" s="T770">PKZ_196X_SU0225.176 (180)</ta>
            <ta e="T782" id="Seg_6539" s="T772">PKZ_196X_SU0225.177 (181)</ta>
            <ta e="T785" id="Seg_6540" s="T782">PKZ_196X_SU0225.178 (182)</ta>
            <ta e="T788" id="Seg_6541" s="T785">PKZ_196X_SU0225.179 (183)</ta>
            <ta e="T789" id="Seg_6542" s="T788">PKZ_196X_SU0225.180 (184)</ta>
            <ta e="T791" id="Seg_6543" s="T789">PKZ_196X_SU0225.181 (185)</ta>
            <ta e="T799" id="Seg_6544" s="T791">PKZ_196X_SU0225.182 (186)</ta>
            <ta e="T803" id="Seg_6545" s="T799">PKZ_196X_SU0225.183 (187)</ta>
            <ta e="T807" id="Seg_6546" s="T803">PKZ_196X_SU0225.184 (188)</ta>
            <ta e="T813" id="Seg_6547" s="T807">PKZ_196X_SU0225.185 (189)</ta>
            <ta e="T825" id="Seg_6548" s="T813">PKZ_196X_SU0225.186 (190)</ta>
            <ta e="T830" id="Seg_6549" s="T825">PKZ_196X_SU0225.187 (191)</ta>
            <ta e="T836" id="Seg_6550" s="T830">PKZ_196X_SU0225.188 (192)</ta>
            <ta e="T840" id="Seg_6551" s="T836">PKZ_196X_SU0225.189 (193)</ta>
            <ta e="T846" id="Seg_6552" s="T840">PKZ_196X_SU0225.190 (194)</ta>
            <ta e="T850" id="Seg_6553" s="T846">PKZ_196X_SU0225.191 (195)</ta>
            <ta e="T857" id="Seg_6554" s="T850">PKZ_196X_SU0225.192 (196)</ta>
            <ta e="T861" id="Seg_6555" s="T857">PKZ_196X_SU0225.193 (197)</ta>
            <ta e="T867" id="Seg_6556" s="T861">PKZ_196X_SU0225.194 (198)</ta>
            <ta e="T871" id="Seg_6557" s="T867">PKZ_196X_SU0225.195 (199)</ta>
            <ta e="T876" id="Seg_6558" s="T871">PKZ_196X_SU0225.196 (200)</ta>
            <ta e="T888" id="Seg_6559" s="T876">PKZ_196X_SU0225.197 (201)</ta>
            <ta e="T892" id="Seg_6560" s="T888">PKZ_196X_SU0225.198 (202)</ta>
            <ta e="T893" id="Seg_6561" s="T892">PKZ_196X_SU0225.199 (203)</ta>
            <ta e="T897" id="Seg_6562" s="T893">PKZ_196X_SU0225.200 (204)</ta>
            <ta e="T901" id="Seg_6563" s="T897">PKZ_196X_SU0225.201 (205)</ta>
            <ta e="T906" id="Seg_6564" s="T901">PKZ_196X_SU0225.202 (206)</ta>
            <ta e="T910" id="Seg_6565" s="T906">PKZ_196X_SU0225.203 (207)</ta>
            <ta e="T911" id="Seg_6566" s="T910">PKZ_196X_SU0225.204 (208)</ta>
            <ta e="T912" id="Seg_6567" s="T911">PKZ_196X_SU0225.205 (209)</ta>
            <ta e="T918" id="Seg_6568" s="T912">PKZ_196X_SU0225.206 (210)</ta>
            <ta e="T927" id="Seg_6569" s="T918">PKZ_196X_SU0225.207 (211)</ta>
            <ta e="T1071" id="Seg_6570" s="T927">PKZ_196X_SU0225.208 (212)</ta>
            <ta e="T935" id="Seg_6571" s="T1071">PKZ_196X_SU0225.209 (213)</ta>
            <ta e="T940" id="Seg_6572" s="T935">PKZ_196X_SU0225.210 (214)</ta>
            <ta e="T945" id="Seg_6573" s="T940">PKZ_196X_SU0225.211 (215)</ta>
            <ta e="T948" id="Seg_6574" s="T945">PKZ_196X_SU0225.212 (216)</ta>
            <ta e="T950" id="Seg_6575" s="T948">PKZ_196X_SU0225.213 (217)</ta>
            <ta e="T952" id="Seg_6576" s="T950">PKZ_196X_SU0225.214 (218)</ta>
            <ta e="T955" id="Seg_6577" s="T952">PKZ_196X_SU0225.215 (219)</ta>
            <ta e="T959" id="Seg_6578" s="T955">PKZ_196X_SU0225.216 (220)</ta>
            <ta e="T962" id="Seg_6579" s="T959">PKZ_196X_SU0225.217 (221)</ta>
            <ta e="T968" id="Seg_6580" s="T962">PKZ_196X_SU0225.218 (222)</ta>
            <ta e="T973" id="Seg_6581" s="T968">PKZ_196X_SU0225.219 (223)</ta>
            <ta e="T980" id="Seg_6582" s="T973">PKZ_196X_SU0225.220 (224)</ta>
            <ta e="T985" id="Seg_6583" s="T980">PKZ_196X_SU0225.221 (225)</ta>
            <ta e="T987" id="Seg_6584" s="T985">PKZ_196X_SU0225.222 (226)</ta>
            <ta e="T991" id="Seg_6585" s="T987">PKZ_196X_SU0225.223 (227)</ta>
            <ta e="T993" id="Seg_6586" s="T991">PKZ_196X_SU0225.224 (228)</ta>
            <ta e="T996" id="Seg_6587" s="T993">PKZ_196X_SU0225.225 (229)</ta>
            <ta e="T1001" id="Seg_6588" s="T996">PKZ_196X_SU0225.226 (230)</ta>
            <ta e="T1006" id="Seg_6589" s="T1001">PKZ_196X_SU0225.227 (231)</ta>
            <ta e="T1008" id="Seg_6590" s="T1006">PKZ_196X_SU0225.228 (232)</ta>
            <ta e="T1016" id="Seg_6591" s="T1008">PKZ_196X_SU0225.229 (233)</ta>
            <ta e="T1017" id="Seg_6592" s="T1016">PKZ_196X_SU0225.230 (234)</ta>
            <ta e="T1018" id="Seg_6593" s="T1017">PKZ_196X_SU0225.231 (235)</ta>
            <ta e="T1023" id="Seg_6594" s="T1018">PKZ_196X_SU0225.232 (236)</ta>
            <ta e="T1025" id="Seg_6595" s="T1023">PKZ_196X_SU0225.233 (237)</ta>
            <ta e="T1030" id="Seg_6596" s="T1025">PKZ_196X_SU0225.234 (238)</ta>
            <ta e="T1032" id="Seg_6597" s="T1030">PKZ_196X_SU0225.235 (239)</ta>
            <ta e="T1034" id="Seg_6598" s="T1032">PKZ_196X_SU0225.236 (240)</ta>
            <ta e="T1038" id="Seg_6599" s="T1034">PKZ_196X_SU0225.237 (241)</ta>
            <ta e="T1040" id="Seg_6600" s="T1038">PKZ_196X_SU0225.238 (242)</ta>
            <ta e="T1042" id="Seg_6601" s="T1040">PKZ_196X_SU0225.239 (243)</ta>
            <ta e="T1051" id="Seg_6602" s="T1042">PKZ_196X_SU0225.240 (244)</ta>
            <ta e="T1059" id="Seg_6603" s="T1051">PKZ_196X_SU0225.241 (245)</ta>
            <ta e="T1062" id="Seg_6604" s="T1059">PKZ_196X_SU0225.242 (246)</ta>
            <ta e="T1069" id="Seg_6605" s="T1062">PKZ_196X_SU0225.243 (247)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PKZ">
            <ta e="T4" id="Seg_6606" s="T0">Miʔ kamen šobibaʔ Abalakoftə. </ta>
            <ta e="T8" id="Seg_6607" s="T4">Miʔnʼibeʔ jakše ibi il. </ta>
            <ta e="T10" id="Seg_6608" s="T8">Šaːbibaʔ jakše. </ta>
            <ta e="T13" id="Seg_6609" s="T10">Dĭgəttə mĭmbibeʔ, mĭmbibeʔ. </ta>
            <ta e="T18" id="Seg_6610" s="T13">(Koj- kojak-) girgidəʔi büzʼeʔi kubibaʔ. </ta>
            <ta e="T24" id="Seg_6611" s="T18">Dʼăbaktərbibaʔ, dĭzeŋ ĭmbidə ej tĭmneʔi, nuzaŋ. </ta>
            <ta e="T26" id="Seg_6612" s="T24">Nuzaŋ il. </ta>
            <ta e="T29" id="Seg_6613" s="T26">Kăde dĭzeŋ amnolaʔbəʔjə! </ta>
            <ta e="T34" id="Seg_6614" s="T29">Miʔ tenəluʔpibeʔ bazo parzittə maːndə. </ta>
            <ta e="T35" id="Seg_6615" s="T34">((BRK)). </ta>
            <ta e="T36" id="Seg_6616" s="T35">((BRK)). </ta>
            <ta e="T42" id="Seg_6617" s="T36">Onʼiʔ koʔbdo, Jelʼa kambi, süt măndərdəsʼtə. </ta>
            <ta e="T48" id="Seg_6618" s="T42">I (šo-) šobi nükenə onʼiʔ, nükenə. </ta>
            <ta e="T57" id="Seg_6619" s="T48">Dĭ nu ige (dĭ) nüke, dĭ măndə: Лицо — kadəl. </ta>
            <ta e="T64" id="Seg_6620" s="T57">(A - a -) A волосья — eʔbdə. </ta>
            <ta e="T67" id="Seg_6621" s="T64">Dĭ bar parluʔpi. </ta>
            <ta e="T71" id="Seg_6622" s="T67">Bar sĭjdə ugandə jakše. </ta>
            <ta e="T77" id="Seg_6623" s="T71">(Măn- măn-) Mămbi:" Dön nüke ige. </ta>
            <ta e="T80" id="Seg_6624" s="T77">Nudla molia dʼăbaktərzittə. </ta>
            <ta e="T81" id="Seg_6625" s="T80">((BRK)). </ta>
            <ta e="T90" id="Seg_6626" s="T81">Dĭ măna nörbəbi: девушка koʔbdo ige, a парень nʼi. </ta>
            <ta e="T92" id="Seg_6627" s="T90">Dĭgəttə … </ta>
            <ta e="T93" id="Seg_6628" s="T92">((BRK)). </ta>
            <ta e="T96" id="Seg_6629" s="T93">Dĭgəttə miʔ maluʔpibeʔ. </ta>
            <ta e="T101" id="Seg_6630" s="T96">Dĭ nükenə (mĭm-) mĭmbibeʔ dʼăbaktərzittə. </ta>
            <ta e="T105" id="Seg_6631" s="T101">Nagur bʼeʔ dʼala dʼăbaktərbibaʔ. </ta>
            <ta e="T107" id="Seg_6632" s="T105">Bar ĭmbi. </ta>
            <ta e="T111" id="Seg_6633" s="T107">Nörbəbi bostə (šĭ-) šĭkətsi. </ta>
            <ta e="T116" id="Seg_6634" s="T111">Dăre mămbi:" Măn tʼotkam külambi. </ta>
            <ta e="T120" id="Seg_6635" s="T116">Šide bʼeʔ pʼe kambi. </ta>
            <ta e="T123" id="Seg_6636" s="T120">(Šindəzizdə) ej dʼăbaktəriam. </ta>
            <ta e="T124" id="Seg_6637" s="T123">((BRK)). </ta>
            <ta e="T129" id="Seg_6638" s="T124">Dĭʔnə tüj (š-) sejʔpü pʼe. </ta>
            <ta e="T133" id="Seg_6639" s="T129">Dĭ ugandə jakše nüke. </ta>
            <ta e="T136" id="Seg_6640" s="T133">Bar ĭmbit ige. </ta>
            <ta e="T145" id="Seg_6641" s="T136">Amzittə mĭlie, a ej amnal dăk, dĭ bar kurollaʔbə. </ta>
            <ta e="T146" id="Seg_6642" s="T145">((BRK)). </ta>
            <ta e="T147" id="Seg_6643" s="T146">((BRK)). </ta>
            <ta e="T156" id="Seg_6644" s="T147">Dĭgəttə dĭ üge maːʔndə iʔgö togonoria, ej sedem üjündə. </ta>
            <ta e="T157" id="Seg_6645" s="T156">((BRK)). </ta>
            <ta e="T168" id="Seg_6646" s="T157">Kazan turanə (bʼeʔ su-) bʼeʔ šide sumna, dĭ bar (ka-) kalia üjüzi. </ta>
            <ta e="T175" id="Seg_6647" s="T168">A mašinaʔi mĭllieʔi, dĭ dĭbər ej amlia. </ta>
            <ta e="T180" id="Seg_6648" s="T175">A üdʼin (ka- kan-) kaləj. </ta>
            <ta e="T181" id="Seg_6649" s="T180">((BRK)). </ta>
            <ta e="T182" id="Seg_6650" s="T181">((BRK)). </ta>
            <ta e="T186" id="Seg_6651" s="T182">"Măn üjüzi lutʼšə kalam. </ta>
            <ta e="T188" id="Seg_6652" s="T186">Aʔtʼi прямой. </ta>
            <ta e="T196" id="Seg_6653" s="T188">A mašinaʔizi ugandə kuŋgəŋ kanzittə da bar sădərlaʔbə". </ta>
            <ta e="T197" id="Seg_6654" s="T196">((BRK)). </ta>
            <ta e="T200" id="Seg_6655" s="T197">Girgit судьба nuzaŋgən. </ta>
            <ta e="T206" id="Seg_6656" s="T200">Šində išo molia dʼăbaktərzittə, ali (ej). </ta>
            <ta e="T207" id="Seg_6657" s="T206">((BRK)). </ta>
            <ta e="T212" id="Seg_6658" s="T207">(Nʼiʔnen) Abalakovan, esseŋdə možna kuzittə. </ta>
            <ta e="T216" id="Seg_6659" s="T212">Sĭre ulut, simat sagər. </ta>
            <ta e="T221" id="Seg_6660" s="T216">Не только kazak kem ige. </ta>
            <ta e="T225" id="Seg_6661" s="T221">I kem ige nuzaŋ. </ta>
            <ta e="T226" id="Seg_6662" s="T225">((BRK)). </ta>
            <ta e="T230" id="Seg_6663" s="T226">Girgit bar özerleʔbəʔi (li). </ta>
            <ta e="T235" id="Seg_6664" s="T230">Kambiʔi togonorzittə, dĭbər-döbər, vot Проня. </ta>
            <ta e="T236" id="Seg_6665" s="T235">Анджигатов. </ta>
            <ta e="T238" id="Seg_6666" s="T236">Председатель сельсовета. </ta>
            <ta e="T243" id="Seg_6667" s="T238">A bostə šĭket ej tĭmnet. </ta>
            <ta e="T244" id="Seg_6668" s="T243">((BRK)). </ta>
            <ta e="T251" id="Seg_6669" s="T244">Tolʼkă dĭ nuzaŋ šĭket onʼiʔ nüke tĭmnet. </ta>
            <ta e="T258" id="Seg_6670" s="T251">A daška šindidə ej tĭmne dĭ šĭkem. </ta>
            <ta e="T259" id="Seg_6671" s="T258">((BRK)). </ta>
            <ta e="T262" id="Seg_6672" s="T259">Tăn măna šaːmbial. </ta>
            <ta e="T265" id="Seg_6673" s="T262">Tăn măna mʼegluʔpiel. </ta>
            <ta e="T276" id="Seg_6674" s="T265">Dĭgəttə šaʔlambiam, a măn tănan măndərbiam, măndərbiam, ej (mo-) moliam măndərzittə. </ta>
            <ta e="T280" id="Seg_6675" s="T276">Ej jakše kuza amnobi. </ta>
            <ta e="T283" id="Seg_6676" s="T280">Dĭm numəjleʔpiʔi Tartʼəberdʼə. </ta>
            <ta e="T287" id="Seg_6677" s="T283">Dĭ bar šojmü dʼaʔpi. </ta>
            <ta e="T289" id="Seg_6678" s="T287">Üjüt ĭzemnie. </ta>
            <ta e="T292" id="Seg_6679" s="T289">Kanzittə ej molia. </ta>
            <ta e="T295" id="Seg_6680" s="T292">Dĭgəttə kambi, kambi. </ta>
            <ta e="T300" id="Seg_6681" s="T295">Urgaːba šonəga:" Iʔ măna (bostəzi)! </ta>
            <ta e="T307" id="Seg_6682" s="T300">No, tolʼkă sagər (mu-) băranə (amdəldə măna)". </ta>
            <ta e="T312" id="Seg_6683" s="T307">Dĭ dĭm amnolbi, bazo kandəga. </ta>
            <ta e="T316" id="Seg_6684" s="T312">Kandəga, kandəga, dĭgəttə … </ta>
            <ta e="T321" id="Seg_6685" s="T316">Dĭgəttə urgo men dʼijegə šobi. </ta>
            <ta e="T323" id="Seg_6686" s="T321">"Iʔ măna. </ta>
            <ta e="T328" id="Seg_6687" s="T323">Tolʼkă sagər măna băranə amdəldə!" </ta>
            <ta e="T330" id="Seg_6688" s="T328">Dĭ amnoldəbi. </ta>
            <ta e="T336" id="Seg_6689" s="T330">Kambi bazoʔ, šonəga, šonəga, (šo- šo-). </ta>
            <ta e="T339" id="Seg_6690" s="T336">(Li-) Lisa šolaʔbə. </ta>
            <ta e="T341" id="Seg_6691" s="T339">"Iʔ măna!" </ta>
            <ta e="T344" id="Seg_6692" s="T341">Dĭ dĭm ibi. </ta>
            <ta e="T350" id="Seg_6693" s="T344">Sagər băranə amnolbi, dĭgəttə bazo kandəga. </ta>
            <ta e="T354" id="Seg_6694" s="T350">Bü šonəga:" Iʔ măna!" </ta>
            <ta e="T359" id="Seg_6695" s="T354">Dĭgəttə dĭ bü kămnəbi dĭbər. </ta>
            <ta e="T360" id="Seg_6696" s="T359">Šobi. </ta>
            <ta e="T368" id="Seg_6697" s="T360">Dĭn bar turaʔi nugaʔi i bü ugaːndə urgo. </ta>
            <ta e="T371" id="Seg_6698" s="T368">Dĭ šöjmü sarbi. </ta>
            <ta e="T374" id="Seg_6699" s="T371">Šü embi, amnolaʔbə. </ta>
            <ta e="T380" id="Seg_6700" s="T374">Dĭm kuluʔpi koŋ:" Šində dĭn amnolaʔbə? </ta>
            <ta e="T384" id="Seg_6701" s="T380">Măn (noʔ-) noʔdə tonəʔlaʔbə. </ta>
            <ta e="T386" id="Seg_6702" s="T384">Šöjmüt amnaʔbə. </ta>
            <ta e="T393" id="Seg_6703" s="T386">Dĭn üdʼügen amnoləj, dĭgəttə bazoʔ bar tonəʔləj. </ta>
            <ta e="T394" id="Seg_6704" s="T393">Kanaʔ!" </ta>
            <ta e="T396" id="Seg_6705" s="T394">Nʼibə öʔlubi. </ta>
            <ta e="T399" id="Seg_6706" s="T396">"Ĭmbi dĭʔnə kereʔ?" </ta>
            <ta e="T406" id="Seg_6707" s="T399">Dĭ šobi (surarla-) surarlaʔbə:" Ĭmbi tănan kereʔ?" </ta>
            <ta e="T410" id="Seg_6708" s="T406">"Dĭn koŋdə koʔbdobə ilim". </ta>
            <ta e="T411" id="Seg_6709" s="T410">((BRK)). </ta>
            <ta e="T422" id="Seg_6710" s="T411">Dĭ nʼi parluʔbi, măndə: "Dĭ măndə: (pušaj dĭm=) koŋdə koʔbdobə ilim! </ta>
            <ta e="T424" id="Seg_6711" s="T422">Pušaj (kangəʔjə). </ta>
            <ta e="T428" id="Seg_6712" s="T424">A to ej jakše moləj". </ta>
            <ta e="T431" id="Seg_6713" s="T428">"(Em=) Ej mĭlem!" </ta>
            <ta e="T440" id="Seg_6714" s="T431">Dĭ šobi, (măn-) mămbi dăre: "Koŋ ej mĭliet koʔbdobə". </ta>
            <ta e="T447" id="Seg_6715" s="T440">"Ej mĭləj jakšeŋ dăk, ej jakše mĭləj. </ta>
            <ta e="T448" id="Seg_6716" s="T447">((BRK)). </ta>
            <ta e="T451" id="Seg_6717" s="T448">"Öʔleʔ дикий ineʔi. </ta>
            <ta e="T458" id="Seg_6718" s="T451">Dĭzeŋ bar dĭn šöjmündə sajnʼiʔluʔləʔjə, dĭgəttə kalla dʼürləj". </ta>
            <ta e="T464" id="Seg_6719" s="T458">Dĭgəttə dĭ urgaːba öʔlubi sagər băragəʔ. </ta>
            <ta e="T469" id="Seg_6720" s="T464">Dĭ urgaːba ineʔi bar sürerluʔpi. </ta>
            <ta e="T475" id="Seg_6721" s="T469">Dĭgəttə măndə:" Öʔlugeʔ tüžöjʔjə, pušaj tüžöjʔjə. </ta>
            <ta e="T480" id="Seg_6722" s="T475">Amnutsi bar inebə bar kutləʔjə". </ta>
            <ta e="T493" id="Seg_6723" s="T480">(Dĭ=) Dĭ bar băragə öʔlubi urgo men, (dĭ=) dĭ kambi, dĭzeŋ bar sürerlüʔbi. </ta>
            <ta e="T497" id="Seg_6724" s="T493">Dĭgəttə măndə:" Öʔlugeʔ …" </ta>
            <ta e="T498" id="Seg_6725" s="T497">((BRK)). </ta>
            <ta e="T504" id="Seg_6726" s="T498">Dĭgəttə koŋ: "(Ölia- öli-) Öʔleʔ menzeŋ! </ta>
            <ta e="T508" id="Seg_6727" s="T504">Pušaj menzeŋ dĭm sajnüzəʔləʔjə". </ta>
            <ta e="T511" id="Seg_6728" s="T508">Dĭ öʔlubi mendən. </ta>
            <ta e="T518" id="Seg_6729" s="T511">A dĭ lʼisa öʔlubi, menzeŋ lʼisagən nuʔməluʔpiʔi. </ta>
            <ta e="T527" id="Seg_6730" s="T518">Dĭgəttə dĭ koŋ bar il oʔbdəbi:" Kanžəbəj, dĭm iləbəj!" </ta>
            <ta e="T534" id="Seg_6731" s="T527">Il šobiʔi, dĭ bar bărabə bü kămnəbi. </ta>
            <ta e="T541" id="Seg_6732" s="T534">(I bü il=) Il bar nuʔməluʔbiʔi bügə. </ta>
            <ta e="T542" id="Seg_6733" s="T541">((BRK)). </ta>
            <ta e="T550" id="Seg_6734" s="T542">Dĭgəttə dĭ koŋ kirgarlaʔbə: "Pʼeldə (imĭʔ-) ige,— mămbi. </ta>
            <ta e="T554" id="Seg_6735" s="T550">Măn ige tüžöjʔi, (ineʔi). </ta>
            <ta e="T559" id="Seg_6736" s="T554">Menzeŋdə mămbiem, aktʼa iʔgö, mămbi". </ta>
            <ta e="T566" id="Seg_6737" s="T559">"Ĭmbidə măna ej kereʔ, tolʼkă koʔbdom izittə". </ta>
            <ta e="T567" id="Seg_6738" s="T566">((BRK)). </ta>
            <ta e="T574" id="Seg_6739" s="T567">Dĭgəttə sagər băranə bü ibi i … </ta>
            <ta e="T575" id="Seg_6740" s="T574">((BRK)). </ta>
            <ta e="T585" id="Seg_6741" s="T575">Dĭgəttə mĭluʔpi koʔbdobə, dĭ (amnolbi=) amnolbi šöjmünə i kambi maʔndə. </ta>
            <ta e="T589" id="Seg_6742" s="T585">Šide kaga (amnolaʔpi=) amnolaʔbəbi. </ta>
            <ta e="T592" id="Seg_6743" s="T589">Dĭzeŋ nezeŋdə ibiʔi. </ta>
            <ta e="T596" id="Seg_6744" s="T592">Dĭzeŋ oʔbdəbiʔi, kambiʔi dʼijenə. </ta>
            <ta e="T600" id="Seg_6745" s="T596">A nezeŋ maʔndə bar. </ta>
            <ta e="T605" id="Seg_6746" s="T600">(Nüdʼi=) Nüdʼin (amzit-) amorzittə amnəbiʔi. </ta>
            <ta e="T612" id="Seg_6747" s="T605">Dĭgəttə măndoliaʔi aspaʔən, (gittə-) girgitdə kuza amnolaʔbə. </ta>
            <ta e="T621" id="Seg_6748" s="T612">Dĭzeŋ moːʔi (со-) oʔbdəbiʔi i (aspaʔdə il-) aspaʔdə embiʔi. </ta>
            <ta e="T629" id="Seg_6749" s="T621">A dĭ mu bar ((PAUSE)) (dʼo-) dʼüʔpi ibi. </ta>
            <ta e="T632" id="Seg_6750" s="T629">Dĭgəttə dĭ šobi. </ta>
            <ta e="T637" id="Seg_6751" s="T632">Ej mobi nendəsʼtə dĭ kuza. </ta>
            <ta e="T640" id="Seg_6752" s="T637">Onʼiʔ nem kutlaʔpi. </ta>
            <ta e="T650" id="Seg_6753" s="T640">Dĭgəttə šide (š-) bazoʔ nenə măndə:" Ĭmbi tăn amnolaʔbəl, nendeʔ!" </ta>
            <ta e="T654" id="Seg_6754" s="T650">Dĭ bar начал nendəsʼtə. </ta>
            <ta e="T659" id="Seg_6755" s="T654">Dĭ dĭʔnə baltuziʔ ulubə jaʔpi. </ta>
            <ta e="T662" id="Seg_6756" s="T659">Dĭgəttə esseŋdə kutlaːmbi. </ta>
            <ta e="T669" id="Seg_6757" s="T662">Dĭgəttə tĭ (dĭz-) dĭzeŋdə maːndə (ma-) parluʔpi. </ta>
            <ta e="T673" id="Seg_6758" s="T669">Dĭzeŋ (neze-) nezeŋdə naga. </ta>
            <ta e="T678" id="Seg_6759" s="T673">Šindidə kuʔpi essem i nezeŋdə. </ta>
            <ta e="T682" id="Seg_6760" s="T678">(Amo-) Amnobiʔi büzʼe nüketsi. </ta>
            <ta e="T688" id="Seg_6761" s="T682">Dĭzeŋ ĭmbidə nagobi, tolʼkă onʼiʔ tüžöj. </ta>
            <ta e="T692" id="Seg_6762" s="T688">Dĭzeŋ amorzittə naga ibi. </ta>
            <ta e="T695" id="Seg_6763" s="T692">Dĭgəttə tüžöjdə bătluʔpi. </ta>
            <ta e="T703" id="Seg_6764" s="T695">Nükem sürerluʔpi:" Kanaʔ, a to măna boskəndəm amga moləj". </ta>
            <ta e="T707" id="Seg_6765" s="T703">Dĭ nüke kambi, kambi. </ta>
            <ta e="T710" id="Seg_6766" s="T707">Išo maʔ kubi. </ta>
            <ta e="T712" id="Seg_6767" s="T710">Maʔdə šobi. </ta>
            <ta e="T717" id="Seg_6768" s="T712">Dĭn sʼel iʔbolaʔbə, uja iʔbolaʔbə. </ta>
            <ta e="T721" id="Seg_6769" s="T717">Dĭ mĭnzərbi bar, amorbi. </ta>
            <ta e="T725" id="Seg_6770" s="T721">Nünniet bar: šindidə šonəga. </ta>
            <ta e="T731" id="Seg_6771" s="T725">Iʔgö ular (dep-) deʔpi, tüžöjʔi, ineʔi. </ta>
            <ta e="T734" id="Seg_6772" s="T731">Dĭgəttə šobi maʔdə. </ta>
            <ta e="T738" id="Seg_6773" s="T734">Măndə: "(Ka-) Kardə, aji!" </ta>
            <ta e="T743" id="Seg_6774" s="T738">Aji karluʔpi, dĭ šobi, măndərla. </ta>
            <ta e="T751" id="Seg_6775" s="T743">"Šindidə ibi, sʼel naga, uja naga, ipek naga. </ta>
            <ta e="T754" id="Seg_6776" s="T751">Šində dön ibi? </ta>
            <ta e="T758" id="Seg_6777" s="T754">Tibi dăk, büzʼem moləj. </ta>
            <ta e="T762" id="Seg_6778" s="T758">Ne dăk, helem moləj". </ta>
            <ta e="T767" id="Seg_6779" s="T762">Dĭgəttə dĭ nüke uʔbdəbi, šobi. </ta>
            <ta e="T770" id="Seg_6780" s="T767">Dĭ dĭm amnolbi. </ta>
            <ta e="T772" id="Seg_6781" s="T770">Amorla amnobiʔi. </ta>
            <ta e="T782" id="Seg_6782" s="T772">Dĭ surarlaʔbə:" Ĭmbi tăn udal, üjül naga, kăde tăn mĭnliel?" </ta>
            <ta e="T785" id="Seg_6783" s="T782">"Da dăre mĭnliel". </ta>
            <ta e="T788" id="Seg_6784" s="T785">"Tăn jamanə suʔmiləl?" </ta>
            <ta e="T789" id="Seg_6785" s="T788">"Suʔmiləm". </ta>
            <ta e="T791" id="Seg_6786" s="T789">Dĭgəttə kambiʔi. </ta>
            <ta e="T799" id="Seg_6787" s="T791">Dĭ süʔmibi, dĭ dĭʔnə baltuzi ulubə bar sajjaʔpi. </ta>
            <ta e="T803" id="Seg_6788" s="T799">Dĭgəttə bar (šama) ibi. </ta>
            <ta e="T807" id="Seg_6789" s="T803">Sʼeldə ibi, uja ibi. </ta>
            <ta e="T813" id="Seg_6790" s="T807">Puzɨrʼdə embi, dĭgəttə parluʔbi bostə büzʼendə. </ta>
            <ta e="T825" id="Seg_6791" s="T813">Maʔdə (saj- š-) sʼabi, a büzʼe kössi (uda d-) udabə bar … </ta>
            <ta e="T830" id="Seg_6792" s="T825">Udabə (kös-) kössi bar … </ta>
            <ta e="T836" id="Seg_6793" s="T830">"Döm ujam amnəm, döm ujam amnəm". </ta>
            <ta e="T840" id="Seg_6794" s="T836">A dĭ barəʔluʔpi dĭʔnə. </ta>
            <ta e="T846" id="Seg_6795" s="T840">Dĭ kabarluʔpi:" Vot kudaj măna mĭbi!" </ta>
            <ta e="T850" id="Seg_6796" s="T846">"Da, kudaj tănan mĭləj! </ta>
            <ta e="T857" id="Seg_6797" s="T850">Pi tănan mĭləj kudaj, dĭm măn mĭbiem". </ta>
            <ta e="T861" id="Seg_6798" s="T857">Dĭgəttə dĭ amorbi, kambiʔi. </ta>
            <ta e="T867" id="Seg_6799" s="T861">Dĭn ineʔi, tüžöjʔi, ularəʔi, šobiʔi dĭbər. </ta>
            <ta e="T871" id="Seg_6800" s="T867">Dĭ măndə:" Pünörzittə axota!" </ta>
            <ta e="T876" id="Seg_6801" s="T871">"Iʔ pünöraʔ, a to bar nuʔməluʔləʔjə!" </ta>
            <ta e="T888" id="Seg_6802" s="T876">Dĭ vsʼo_taki pünörbi, i bar (inezeŋdə), ularzaŋdə i tüžöjʔjə üʔməluʔpiʔi. </ta>
            <ta e="T892" id="Seg_6803" s="T888">Dĭ davaj sütsi … </ta>
            <ta e="T893" id="Seg_6804" s="T892">((BRK)). </ta>
            <ta e="T897" id="Seg_6805" s="T893">(süt-) sütsi kămnasʼtə dĭzeŋdə. </ta>
            <ta e="T901" id="Seg_6806" s="T897">"Ular, šiʔ poʔto molaʔ! </ta>
            <ta e="T906" id="Seg_6807" s="T901">Tüžöjəʔi, (šiʔ=) šiʔ pulan mogaʔ! </ta>
            <ta e="T910" id="Seg_6808" s="T906">A šiʔ, ineʔi …" </ta>
            <ta e="T911" id="Seg_6809" s="T910">Nöməlluʔpiem. </ta>
            <ta e="T912" id="Seg_6810" s="T911">((BRK)). </ta>
            <ta e="T918" id="Seg_6811" s="T912">"Ineʔi, (măm-) mămbi, pušaj izʼubrʼaʔi moluʔjəʔ". </ta>
            <ta e="T927" id="Seg_6812" s="T918">Dĭgəttə nüke i büzʼe baʔluʔbi, ĭmbidə bazoʔ naga dĭzeŋdə. </ta>
            <ta e="T1071" id="Seg_6813" s="T927">Bar kalla dʼürbiʔi. </ta>
            <ta e="T935" id="Seg_6814" s="T1071">Amnobi büzʼe, dĭn nagur koʔbdot ibi. </ta>
            <ta e="T940" id="Seg_6815" s="T935">Dĭm kăštəbiʔi Kazan turanə štobɨ šobi. </ta>
            <ta e="T945" id="Seg_6816" s="T940">A urgo koʔbdot:" Măn kalam!" </ta>
            <ta e="T948" id="Seg_6817" s="T945">Eʔbdəbə (bă-) băʔpi. </ta>
            <ta e="T950" id="Seg_6818" s="T948">Piʔmə šerbi. </ta>
            <ta e="T952" id="Seg_6819" s="T950">Kujnek šerbi. </ta>
            <ta e="T955" id="Seg_6820" s="T952">Üžü šerbi, kambi. </ta>
            <ta e="T959" id="Seg_6821" s="T955">A abat urgaːbazi abi. </ta>
            <ta e="T962" id="Seg_6822" s="T959">I dĭʔnə šonəga. </ta>
            <ta e="T968" id="Seg_6823" s="T962">Dĭ bar nereʔluʔpi i maʔndə nuʔməluʔpi. </ta>
            <ta e="T973" id="Seg_6824" s="T968">Dĭ šobi:" Ĭmbi ej kambiam?" </ta>
            <ta e="T980" id="Seg_6825" s="T973">"(Там=) Dĭn urgaːba, măn nereʔluʔpiem, maʔndə šobiam". </ta>
            <ta e="T985" id="Seg_6826" s="T980">Dĭgəttə baška koʔbdo:" Măn kalam!" </ta>
            <ta e="T987" id="Seg_6827" s="T985">"No, kanaʔ". </ta>
            <ta e="T991" id="Seg_6828" s="T987">Šerbi piʔməʔi, kujnek šerbi. </ta>
            <ta e="T993" id="Seg_6829" s="T991">Eʔbdəbə băʔpi. </ta>
            <ta e="T996" id="Seg_6830" s="T993">Üžübə šerbi, kambi. </ta>
            <ta e="T1001" id="Seg_6831" s="T996">Dĭgəttə abat molaːmbi urgaːbazi, kambi. </ta>
            <ta e="T1006" id="Seg_6832" s="T1001">Dĭ bar nereʔluʔpi, maʔndə šobi. </ta>
            <ta e="T1008" id="Seg_6833" s="T1006">"Ĭmbi šobial?" </ta>
            <ta e="T1016" id="Seg_6834" s="T1008">"Da dĭn (urgaːba) šonəga, măn nereʔluʔpiem, maʔndə nuʔməluʔpiem". </ta>
            <ta e="T1017" id="Seg_6835" s="T1016">Kabarləj. </ta>
            <ta e="T1018" id="Seg_6836" s="T1017">((BRK)). </ta>
            <ta e="T1023" id="Seg_6837" s="T1018">Dĭgəttə üdʼüge koʔbdo:" Măn kalam!" </ta>
            <ta e="T1025" id="Seg_6838" s="T1023">"No, kanaʔ". </ta>
            <ta e="T1030" id="Seg_6839" s="T1025">Tože dăre že eʔbdəbə bătluʔpi. </ta>
            <ta e="T1032" id="Seg_6840" s="T1030">Piʔməʔi šerbi. </ta>
            <ta e="T1034" id="Seg_6841" s="T1032">Kujnek šerbi. </ta>
            <ta e="T1038" id="Seg_6842" s="T1034">I üžü šerbi, kambi. </ta>
            <ta e="T1040" id="Seg_6843" s="T1038">Kandəga, kandəga. </ta>
            <ta e="T1042" id="Seg_6844" s="T1040">Urgaːba šonəga. </ta>
            <ta e="T1051" id="Seg_6845" s="T1042">Dĭ strelazi bar dĭm (ku-) (simandə=) simat bar … </ta>
            <ta e="T1059" id="Seg_6846" s="T1051">Dĭ strelazi simandə toʔnarbi, i simat (naga-) nagoluʔpi. </ta>
            <ta e="T1062" id="Seg_6847" s="T1059">Dĭgəttə kambi, kambi. </ta>
            <ta e="T1069" id="Seg_6848" s="T1062">Šonəga, nüke amnolaʔbə i nüken nʼit amnolaʔbə. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-PKZ">
            <ta e="T1" id="Seg_6849" s="T0">miʔ</ta>
            <ta e="T2" id="Seg_6850" s="T1">kamen</ta>
            <ta e="T3" id="Seg_6851" s="T2">šo-bi-baʔ</ta>
            <ta e="T4" id="Seg_6852" s="T3">Abalakof-tə</ta>
            <ta e="T5" id="Seg_6853" s="T4">miʔnʼibeʔ</ta>
            <ta e="T6" id="Seg_6854" s="T5">jakše</ta>
            <ta e="T7" id="Seg_6855" s="T6">i-bi</ta>
            <ta e="T8" id="Seg_6856" s="T7">il</ta>
            <ta e="T9" id="Seg_6857" s="T8">šaː-bi-baʔ</ta>
            <ta e="T10" id="Seg_6858" s="T9">jakše</ta>
            <ta e="T11" id="Seg_6859" s="T10">dĭgəttə</ta>
            <ta e="T12" id="Seg_6860" s="T11">mĭm-bi-beʔ</ta>
            <ta e="T13" id="Seg_6861" s="T12">mĭm-bi-beʔ</ta>
            <ta e="T16" id="Seg_6862" s="T15">girgid-əʔi</ta>
            <ta e="T17" id="Seg_6863" s="T16">büzʼe-ʔi</ta>
            <ta e="T18" id="Seg_6864" s="T17">ku-bi-baʔ</ta>
            <ta e="T19" id="Seg_6865" s="T18">dʼăbaktər-bi-baʔ</ta>
            <ta e="T20" id="Seg_6866" s="T19">dĭ-zeŋ</ta>
            <ta e="T21" id="Seg_6867" s="T20">ĭmbi=də</ta>
            <ta e="T22" id="Seg_6868" s="T21">ej</ta>
            <ta e="T23" id="Seg_6869" s="T22">tĭmne-ʔi</ta>
            <ta e="T24" id="Seg_6870" s="T23">nu-zaŋ</ta>
            <ta e="T25" id="Seg_6871" s="T24">nu-zaŋ</ta>
            <ta e="T26" id="Seg_6872" s="T25">il</ta>
            <ta e="T27" id="Seg_6873" s="T26">kăde</ta>
            <ta e="T28" id="Seg_6874" s="T27">dĭ-zeŋ</ta>
            <ta e="T29" id="Seg_6875" s="T28">amno-laʔbə-ʔjə</ta>
            <ta e="T30" id="Seg_6876" s="T29">miʔ</ta>
            <ta e="T31" id="Seg_6877" s="T30">tenə-luʔ-pi-beʔ</ta>
            <ta e="T32" id="Seg_6878" s="T31">bazo</ta>
            <ta e="T33" id="Seg_6879" s="T32">par-zittə</ta>
            <ta e="T34" id="Seg_6880" s="T33">ma-ndə</ta>
            <ta e="T37" id="Seg_6881" s="T36">onʼiʔ</ta>
            <ta e="T38" id="Seg_6882" s="T37">koʔbdo</ta>
            <ta e="T39" id="Seg_6883" s="T38">Jelʼa</ta>
            <ta e="T40" id="Seg_6884" s="T39">kam-bi</ta>
            <ta e="T41" id="Seg_6885" s="T40">süt</ta>
            <ta e="T42" id="Seg_6886" s="T41">măndə-r-də-sʼtə</ta>
            <ta e="T43" id="Seg_6887" s="T42">i</ta>
            <ta e="T45" id="Seg_6888" s="T44">šo-bi</ta>
            <ta e="T46" id="Seg_6889" s="T45">nüke-nə</ta>
            <ta e="T47" id="Seg_6890" s="T46">onʼiʔ</ta>
            <ta e="T48" id="Seg_6891" s="T47">nüke-nə</ta>
            <ta e="T49" id="Seg_6892" s="T48">dĭ</ta>
            <ta e="T50" id="Seg_6893" s="T49">nu</ta>
            <ta e="T51" id="Seg_6894" s="T50">i-ge</ta>
            <ta e="T52" id="Seg_6895" s="T51">dĭ</ta>
            <ta e="T53" id="Seg_6896" s="T52">nüke</ta>
            <ta e="T54" id="Seg_6897" s="T53">dĭ</ta>
            <ta e="T55" id="Seg_6898" s="T54">măn-də</ta>
            <ta e="T57" id="Seg_6899" s="T56">kadəl</ta>
            <ta e="T64" id="Seg_6900" s="T63">eʔbdə</ta>
            <ta e="T65" id="Seg_6901" s="T64">dĭ</ta>
            <ta e="T66" id="Seg_6902" s="T65">bar</ta>
            <ta e="T67" id="Seg_6903" s="T66">par-luʔ-pi</ta>
            <ta e="T68" id="Seg_6904" s="T67">bar</ta>
            <ta e="T69" id="Seg_6905" s="T68">sĭj-də</ta>
            <ta e="T70" id="Seg_6906" s="T69">ugandə</ta>
            <ta e="T71" id="Seg_6907" s="T70">jakše</ta>
            <ta e="T74" id="Seg_6908" s="T73">măm-bi</ta>
            <ta e="T75" id="Seg_6909" s="T74">dön</ta>
            <ta e="T76" id="Seg_6910" s="T75">nüke</ta>
            <ta e="T77" id="Seg_6911" s="T76">i-ge</ta>
            <ta e="T78" id="Seg_6912" s="T77">nu-d-la</ta>
            <ta e="T79" id="Seg_6913" s="T78">mo-lia</ta>
            <ta e="T80" id="Seg_6914" s="T79">dʼăbaktər-zittə</ta>
            <ta e="T82" id="Seg_6915" s="T81">dĭ</ta>
            <ta e="T83" id="Seg_6916" s="T82">măna</ta>
            <ta e="T84" id="Seg_6917" s="T83">nörbə-bi</ta>
            <ta e="T86" id="Seg_6918" s="T85">koʔbdo</ta>
            <ta e="T87" id="Seg_6919" s="T86">i-ge</ta>
            <ta e="T88" id="Seg_6920" s="T87">a</ta>
            <ta e="T90" id="Seg_6921" s="T89">nʼi</ta>
            <ta e="T92" id="Seg_6922" s="T90">dĭgəttə</ta>
            <ta e="T94" id="Seg_6923" s="T93">dĭgəttə</ta>
            <ta e="T95" id="Seg_6924" s="T94">miʔ</ta>
            <ta e="T96" id="Seg_6925" s="T95">ma-luʔ-pi-beʔ</ta>
            <ta e="T97" id="Seg_6926" s="T96">dĭ</ta>
            <ta e="T98" id="Seg_6927" s="T97">nüke-nə</ta>
            <ta e="T100" id="Seg_6928" s="T99">mĭm-bi-beʔ</ta>
            <ta e="T101" id="Seg_6929" s="T100">dʼăbaktər-zittə</ta>
            <ta e="T102" id="Seg_6930" s="T101">nagur</ta>
            <ta e="T103" id="Seg_6931" s="T102">bʼeʔ</ta>
            <ta e="T104" id="Seg_6932" s="T103">dʼala</ta>
            <ta e="T105" id="Seg_6933" s="T104">dʼăbaktər-bi-baʔ</ta>
            <ta e="T106" id="Seg_6934" s="T105">bar</ta>
            <ta e="T107" id="Seg_6935" s="T106">ĭmbi</ta>
            <ta e="T108" id="Seg_6936" s="T107">nörbə-bi</ta>
            <ta e="T109" id="Seg_6937" s="T108">bos-tə</ta>
            <ta e="T111" id="Seg_6938" s="T110">šĭkə-t-si</ta>
            <ta e="T112" id="Seg_6939" s="T111">dăre</ta>
            <ta e="T113" id="Seg_6940" s="T112">măm-bi</ta>
            <ta e="T114" id="Seg_6941" s="T113">măn</ta>
            <ta e="T115" id="Seg_6942" s="T114">tʼotka-m</ta>
            <ta e="T116" id="Seg_6943" s="T115">kü-lam-bi</ta>
            <ta e="T117" id="Seg_6944" s="T116">šide</ta>
            <ta e="T118" id="Seg_6945" s="T117">bʼeʔ</ta>
            <ta e="T119" id="Seg_6946" s="T118">pʼe</ta>
            <ta e="T120" id="Seg_6947" s="T119">kam-bi</ta>
            <ta e="T121" id="Seg_6948" s="T120">šində-ziz-də</ta>
            <ta e="T122" id="Seg_6949" s="T121">ej</ta>
            <ta e="T123" id="Seg_6950" s="T122">dʼăbaktər-ia-m</ta>
            <ta e="T125" id="Seg_6951" s="T124">dĭʔ-nə</ta>
            <ta e="T126" id="Seg_6952" s="T125">tüj</ta>
            <ta e="T128" id="Seg_6953" s="T127">sejʔpü</ta>
            <ta e="T129" id="Seg_6954" s="T128">pʼe</ta>
            <ta e="T130" id="Seg_6955" s="T129">dĭ</ta>
            <ta e="T131" id="Seg_6956" s="T130">ugandə</ta>
            <ta e="T132" id="Seg_6957" s="T131">jakše</ta>
            <ta e="T133" id="Seg_6958" s="T132">nüke</ta>
            <ta e="T134" id="Seg_6959" s="T133">bar</ta>
            <ta e="T135" id="Seg_6960" s="T134">ĭmbi-t</ta>
            <ta e="T136" id="Seg_6961" s="T135">i-ge</ta>
            <ta e="T137" id="Seg_6962" s="T136">am-zittə</ta>
            <ta e="T138" id="Seg_6963" s="T137">mĭ-lie</ta>
            <ta e="T139" id="Seg_6964" s="T138">a</ta>
            <ta e="T140" id="Seg_6965" s="T139">ej</ta>
            <ta e="T141" id="Seg_6966" s="T140">am-na-l</ta>
            <ta e="T142" id="Seg_6967" s="T141">dăk</ta>
            <ta e="T143" id="Seg_6968" s="T142">dĭ</ta>
            <ta e="T144" id="Seg_6969" s="T143">bar</ta>
            <ta e="T145" id="Seg_6970" s="T144">kurol-laʔbə</ta>
            <ta e="T148" id="Seg_6971" s="T147">dĭgəttə</ta>
            <ta e="T149" id="Seg_6972" s="T148">dĭ</ta>
            <ta e="T150" id="Seg_6973" s="T149">üge</ta>
            <ta e="T151" id="Seg_6974" s="T150">maːʔ-ndə</ta>
            <ta e="T152" id="Seg_6975" s="T151">iʔgö</ta>
            <ta e="T153" id="Seg_6976" s="T152">togonor-ia</ta>
            <ta e="T154" id="Seg_6977" s="T153">ej</ta>
            <ta e="T155" id="Seg_6978" s="T154">sedem</ta>
            <ta e="T156" id="Seg_6979" s="T155">üjü-ndə</ta>
            <ta e="T1074" id="Seg_6980" s="T157">Kazan</ta>
            <ta e="T158" id="Seg_6981" s="T1074">tura-nə</ta>
            <ta e="T159" id="Seg_6982" s="T158">bʼeʔ</ta>
            <ta e="T161" id="Seg_6983" s="T160">bʼeʔ</ta>
            <ta e="T162" id="Seg_6984" s="T161">šide</ta>
            <ta e="T163" id="Seg_6985" s="T162">sumna</ta>
            <ta e="T164" id="Seg_6986" s="T163">dĭ</ta>
            <ta e="T165" id="Seg_6987" s="T164">bar</ta>
            <ta e="T167" id="Seg_6988" s="T166">ka-lia</ta>
            <ta e="T168" id="Seg_6989" s="T167">üjü-zi</ta>
            <ta e="T169" id="Seg_6990" s="T168">a</ta>
            <ta e="T170" id="Seg_6991" s="T169">mašina-ʔi</ta>
            <ta e="T171" id="Seg_6992" s="T170">mĭl-lie-ʔi</ta>
            <ta e="T172" id="Seg_6993" s="T171">dĭ</ta>
            <ta e="T173" id="Seg_6994" s="T172">dĭbər</ta>
            <ta e="T174" id="Seg_6995" s="T173">ej</ta>
            <ta e="T175" id="Seg_6996" s="T174">am-lia</ta>
            <ta e="T176" id="Seg_6997" s="T175">a</ta>
            <ta e="T177" id="Seg_6998" s="T176">üdʼi-n</ta>
            <ta e="T180" id="Seg_6999" s="T179">ka-lə-j</ta>
            <ta e="T183" id="Seg_7000" s="T182">măn</ta>
            <ta e="T184" id="Seg_7001" s="T183">üjü-zi</ta>
            <ta e="T185" id="Seg_7002" s="T184">lutʼšə</ta>
            <ta e="T186" id="Seg_7003" s="T185">ka-la-m</ta>
            <ta e="T187" id="Seg_7004" s="T186">aʔtʼi</ta>
            <ta e="T189" id="Seg_7005" s="T188">a</ta>
            <ta e="T190" id="Seg_7006" s="T189">mašina-ʔi-zi</ta>
            <ta e="T191" id="Seg_7007" s="T190">ugandə</ta>
            <ta e="T192" id="Seg_7008" s="T191">kuŋgə-ŋ</ta>
            <ta e="T193" id="Seg_7009" s="T192">kan-zittə</ta>
            <ta e="T194" id="Seg_7010" s="T193">da</ta>
            <ta e="T195" id="Seg_7011" s="T194">bar</ta>
            <ta e="T196" id="Seg_7012" s="T195">sădər-laʔbə</ta>
            <ta e="T198" id="Seg_7013" s="T197">girgit</ta>
            <ta e="T200" id="Seg_7014" s="T199">nu-zaŋ-gən</ta>
            <ta e="T201" id="Seg_7015" s="T200">šində</ta>
            <ta e="T202" id="Seg_7016" s="T201">išo</ta>
            <ta e="T203" id="Seg_7017" s="T202">mo-lia</ta>
            <ta e="T204" id="Seg_7018" s="T203">dʼăbaktər-zittə</ta>
            <ta e="T205" id="Seg_7019" s="T204">ali</ta>
            <ta e="T206" id="Seg_7020" s="T205">ej</ta>
            <ta e="T208" id="Seg_7021" s="T207">nʼiʔnen</ta>
            <ta e="T209" id="Seg_7022" s="T208">Abalakova-n</ta>
            <ta e="T210" id="Seg_7023" s="T209">es-seŋ-də</ta>
            <ta e="T211" id="Seg_7024" s="T210">možna</ta>
            <ta e="T212" id="Seg_7025" s="T211">ku-zittə</ta>
            <ta e="T213" id="Seg_7026" s="T212">sĭre</ta>
            <ta e="T214" id="Seg_7027" s="T213">ulu-t</ta>
            <ta e="T215" id="Seg_7028" s="T214">sima-t</ta>
            <ta e="T216" id="Seg_7029" s="T215">sagər</ta>
            <ta e="T219" id="Seg_7030" s="T218">kazak</ta>
            <ta e="T220" id="Seg_7031" s="T219">kem</ta>
            <ta e="T221" id="Seg_7032" s="T220">i-ge</ta>
            <ta e="T222" id="Seg_7033" s="T221">i</ta>
            <ta e="T223" id="Seg_7034" s="T222">kem</ta>
            <ta e="T224" id="Seg_7035" s="T223">i-ge</ta>
            <ta e="T225" id="Seg_7036" s="T224">nu-zaŋ</ta>
            <ta e="T227" id="Seg_7037" s="T226">girgit</ta>
            <ta e="T228" id="Seg_7038" s="T227">bar</ta>
            <ta e="T229" id="Seg_7039" s="T228">özer-leʔbə-ʔi</ta>
            <ta e="T230" id="Seg_7040" s="T229">li</ta>
            <ta e="T231" id="Seg_7041" s="T230">kam-bi-ʔi</ta>
            <ta e="T232" id="Seg_7042" s="T231">togonor-zittə</ta>
            <ta e="T233" id="Seg_7043" s="T232">dĭbər_döbər</ta>
            <ta e="T234" id="Seg_7044" s="T233">vot</ta>
            <ta e="T239" id="Seg_7045" s="T238">a</ta>
            <ta e="T240" id="Seg_7046" s="T239">bos-tə</ta>
            <ta e="T241" id="Seg_7047" s="T240">šĭke-t</ta>
            <ta e="T242" id="Seg_7048" s="T241">ej</ta>
            <ta e="T243" id="Seg_7049" s="T242">tĭmne-t</ta>
            <ta e="T245" id="Seg_7050" s="T244">tolʼkă</ta>
            <ta e="T246" id="Seg_7051" s="T245">dĭ</ta>
            <ta e="T247" id="Seg_7052" s="T246">nu-zaŋ</ta>
            <ta e="T248" id="Seg_7053" s="T247">šĭke-t</ta>
            <ta e="T249" id="Seg_7054" s="T248">onʼiʔ</ta>
            <ta e="T250" id="Seg_7055" s="T249">nüke</ta>
            <ta e="T251" id="Seg_7056" s="T250">tĭmne-t</ta>
            <ta e="T252" id="Seg_7057" s="T251">a</ta>
            <ta e="T253" id="Seg_7058" s="T252">daška</ta>
            <ta e="T254" id="Seg_7059" s="T253">šindi=də</ta>
            <ta e="T255" id="Seg_7060" s="T254">ej</ta>
            <ta e="T256" id="Seg_7061" s="T255">tĭmne</ta>
            <ta e="T257" id="Seg_7062" s="T256">dĭ</ta>
            <ta e="T258" id="Seg_7063" s="T257">šĭke-m</ta>
            <ta e="T260" id="Seg_7064" s="T259">tăn</ta>
            <ta e="T261" id="Seg_7065" s="T260">măna</ta>
            <ta e="T262" id="Seg_7066" s="T261">šaːm-bia-l</ta>
            <ta e="T263" id="Seg_7067" s="T262">tăn</ta>
            <ta e="T264" id="Seg_7068" s="T263">măna</ta>
            <ta e="T265" id="Seg_7069" s="T264">mʼeg-luʔ-pie-l</ta>
            <ta e="T266" id="Seg_7070" s="T265">dĭgəttə</ta>
            <ta e="T267" id="Seg_7071" s="T266">šaʔ-lam-bia-m</ta>
            <ta e="T268" id="Seg_7072" s="T267">a</ta>
            <ta e="T269" id="Seg_7073" s="T268">măn</ta>
            <ta e="T270" id="Seg_7074" s="T269">tănan</ta>
            <ta e="T271" id="Seg_7075" s="T270">măndə-r-bia-m</ta>
            <ta e="T272" id="Seg_7076" s="T271">măndə-r-bia-m</ta>
            <ta e="T273" id="Seg_7077" s="T272">ej</ta>
            <ta e="T275" id="Seg_7078" s="T274">mo-lia-m</ta>
            <ta e="T276" id="Seg_7079" s="T275">măndə-r-zittə</ta>
            <ta e="T277" id="Seg_7080" s="T276">ej</ta>
            <ta e="T278" id="Seg_7081" s="T277">jakše</ta>
            <ta e="T279" id="Seg_7082" s="T278">kuza</ta>
            <ta e="T280" id="Seg_7083" s="T279">amno-bi</ta>
            <ta e="T281" id="Seg_7084" s="T280">dĭ-m</ta>
            <ta e="T282" id="Seg_7085" s="T281">numəj-leʔ-pi-ʔi</ta>
            <ta e="T283" id="Seg_7086" s="T282">Tartʼəberdʼə</ta>
            <ta e="T284" id="Seg_7087" s="T283">dĭ</ta>
            <ta e="T285" id="Seg_7088" s="T284">bar</ta>
            <ta e="T286" id="Seg_7089" s="T285">šojmü</ta>
            <ta e="T287" id="Seg_7090" s="T286">dʼaʔ-pi</ta>
            <ta e="T288" id="Seg_7091" s="T287">üjü-t</ta>
            <ta e="T289" id="Seg_7092" s="T288">ĭzem-nie</ta>
            <ta e="T290" id="Seg_7093" s="T289">kan-zittə</ta>
            <ta e="T291" id="Seg_7094" s="T290">ej</ta>
            <ta e="T292" id="Seg_7095" s="T291">mo-lia</ta>
            <ta e="T293" id="Seg_7096" s="T292">dĭgəttə</ta>
            <ta e="T294" id="Seg_7097" s="T293">kam-bi</ta>
            <ta e="T295" id="Seg_7098" s="T294">kam-bi</ta>
            <ta e="T296" id="Seg_7099" s="T295">urgaːba</ta>
            <ta e="T297" id="Seg_7100" s="T296">šonə-ga</ta>
            <ta e="T298" id="Seg_7101" s="T297">i-ʔ</ta>
            <ta e="T299" id="Seg_7102" s="T298">măna</ta>
            <ta e="T300" id="Seg_7103" s="T299">bostə-zi</ta>
            <ta e="T301" id="Seg_7104" s="T300">no</ta>
            <ta e="T302" id="Seg_7105" s="T301">tolʼkă</ta>
            <ta e="T303" id="Seg_7106" s="T302">sagər</ta>
            <ta e="T305" id="Seg_7107" s="T304">băra-nə</ta>
            <ta e="T306" id="Seg_7108" s="T305">amdəl-də</ta>
            <ta e="T307" id="Seg_7109" s="T306">măna</ta>
            <ta e="T308" id="Seg_7110" s="T307">dĭ</ta>
            <ta e="T309" id="Seg_7111" s="T308">dĭ-m</ta>
            <ta e="T310" id="Seg_7112" s="T309">amnol-bi</ta>
            <ta e="T311" id="Seg_7113" s="T310">bazo</ta>
            <ta e="T312" id="Seg_7114" s="T311">kandə-ga</ta>
            <ta e="T313" id="Seg_7115" s="T312">kandə-ga</ta>
            <ta e="T314" id="Seg_7116" s="T313">kandə-ga</ta>
            <ta e="T316" id="Seg_7117" s="T314">dĭgəttə</ta>
            <ta e="T317" id="Seg_7118" s="T316">dĭgəttə</ta>
            <ta e="T318" id="Seg_7119" s="T317">urgo</ta>
            <ta e="T319" id="Seg_7120" s="T318">men</ta>
            <ta e="T320" id="Seg_7121" s="T319">dʼije-gə</ta>
            <ta e="T321" id="Seg_7122" s="T320">šo-bi</ta>
            <ta e="T322" id="Seg_7123" s="T321">i-ʔ</ta>
            <ta e="T323" id="Seg_7124" s="T322">măna</ta>
            <ta e="T324" id="Seg_7125" s="T323">tolʼkă</ta>
            <ta e="T325" id="Seg_7126" s="T324">sagər</ta>
            <ta e="T326" id="Seg_7127" s="T325">măna</ta>
            <ta e="T327" id="Seg_7128" s="T326">băra-nə</ta>
            <ta e="T328" id="Seg_7129" s="T327">amdəl-də</ta>
            <ta e="T329" id="Seg_7130" s="T328">dĭ</ta>
            <ta e="T330" id="Seg_7131" s="T329">amnol-də-bi</ta>
            <ta e="T331" id="Seg_7132" s="T330">kam-bi</ta>
            <ta e="T332" id="Seg_7133" s="T331">bazoʔ</ta>
            <ta e="T333" id="Seg_7134" s="T332">šonə-ga</ta>
            <ta e="T334" id="Seg_7135" s="T333">šonə-ga</ta>
            <ta e="T338" id="Seg_7136" s="T337">lisa</ta>
            <ta e="T339" id="Seg_7137" s="T338">šo-laʔbə</ta>
            <ta e="T340" id="Seg_7138" s="T339">i-ʔ</ta>
            <ta e="T341" id="Seg_7139" s="T340">măna</ta>
            <ta e="T342" id="Seg_7140" s="T341">dĭ</ta>
            <ta e="T343" id="Seg_7141" s="T342">dĭ-m</ta>
            <ta e="T344" id="Seg_7142" s="T343">i-bi</ta>
            <ta e="T345" id="Seg_7143" s="T344">sagər</ta>
            <ta e="T346" id="Seg_7144" s="T345">băra-nə</ta>
            <ta e="T347" id="Seg_7145" s="T346">amnol-bi</ta>
            <ta e="T348" id="Seg_7146" s="T347">dĭgəttə</ta>
            <ta e="T349" id="Seg_7147" s="T348">bazo</ta>
            <ta e="T350" id="Seg_7148" s="T349">kandə-ga</ta>
            <ta e="T351" id="Seg_7149" s="T350">bü</ta>
            <ta e="T352" id="Seg_7150" s="T351">šonə-ga</ta>
            <ta e="T353" id="Seg_7151" s="T352">i-ʔ</ta>
            <ta e="T354" id="Seg_7152" s="T353">măna</ta>
            <ta e="T355" id="Seg_7153" s="T354">dĭgəttə</ta>
            <ta e="T356" id="Seg_7154" s="T355">dĭ</ta>
            <ta e="T357" id="Seg_7155" s="T356">bü</ta>
            <ta e="T358" id="Seg_7156" s="T357">kămnə-bi</ta>
            <ta e="T359" id="Seg_7157" s="T358">dĭbər</ta>
            <ta e="T360" id="Seg_7158" s="T359">šo-bi</ta>
            <ta e="T361" id="Seg_7159" s="T360">dĭn</ta>
            <ta e="T362" id="Seg_7160" s="T361">bar</ta>
            <ta e="T363" id="Seg_7161" s="T362">tura-ʔi</ta>
            <ta e="T364" id="Seg_7162" s="T363">nu-ga-ʔi</ta>
            <ta e="T365" id="Seg_7163" s="T364">i</ta>
            <ta e="T366" id="Seg_7164" s="T365">bü</ta>
            <ta e="T367" id="Seg_7165" s="T366">ugaːndə</ta>
            <ta e="T368" id="Seg_7166" s="T367">urgo</ta>
            <ta e="T369" id="Seg_7167" s="T368">dĭ</ta>
            <ta e="T370" id="Seg_7168" s="T369">šöjmü</ta>
            <ta e="T371" id="Seg_7169" s="T370">sar-bi</ta>
            <ta e="T372" id="Seg_7170" s="T371">šü</ta>
            <ta e="T373" id="Seg_7171" s="T372">em-bi</ta>
            <ta e="T374" id="Seg_7172" s="T373">amno-laʔbə</ta>
            <ta e="T375" id="Seg_7173" s="T374">dĭ-m</ta>
            <ta e="T376" id="Seg_7174" s="T375">ku-luʔ-pi</ta>
            <ta e="T377" id="Seg_7175" s="T376">koŋ</ta>
            <ta e="T378" id="Seg_7176" s="T377">šində</ta>
            <ta e="T379" id="Seg_7177" s="T378">dĭn</ta>
            <ta e="T380" id="Seg_7178" s="T379">amno-laʔbə</ta>
            <ta e="T381" id="Seg_7179" s="T380">măn</ta>
            <ta e="T383" id="Seg_7180" s="T382">noʔ-də</ta>
            <ta e="T384" id="Seg_7181" s="T383">tonəʔ-laʔbə</ta>
            <ta e="T385" id="Seg_7182" s="T384">šöjmü-t</ta>
            <ta e="T386" id="Seg_7183" s="T385">am-naʔbə</ta>
            <ta e="T387" id="Seg_7184" s="T386">dĭn</ta>
            <ta e="T388" id="Seg_7185" s="T387">üdʼüge-n</ta>
            <ta e="T389" id="Seg_7186" s="T388">amno-lə-j</ta>
            <ta e="T390" id="Seg_7187" s="T389">dĭgəttə</ta>
            <ta e="T391" id="Seg_7188" s="T390">bazoʔ</ta>
            <ta e="T392" id="Seg_7189" s="T391">bar</ta>
            <ta e="T393" id="Seg_7190" s="T392">tonəʔ-lə-j</ta>
            <ta e="T394" id="Seg_7191" s="T393">kan-a-ʔ</ta>
            <ta e="T395" id="Seg_7192" s="T394">nʼi-bə</ta>
            <ta e="T396" id="Seg_7193" s="T395">öʔlu-bi</ta>
            <ta e="T397" id="Seg_7194" s="T396">ĭmbi</ta>
            <ta e="T398" id="Seg_7195" s="T397">dĭʔ-nə</ta>
            <ta e="T399" id="Seg_7196" s="T398">kereʔ</ta>
            <ta e="T400" id="Seg_7197" s="T399">dĭ</ta>
            <ta e="T401" id="Seg_7198" s="T400">šo-bi</ta>
            <ta e="T403" id="Seg_7199" s="T402">surar-laʔbə</ta>
            <ta e="T404" id="Seg_7200" s="T403">ĭmbi</ta>
            <ta e="T405" id="Seg_7201" s="T404">tănan</ta>
            <ta e="T406" id="Seg_7202" s="T405">kereʔ</ta>
            <ta e="T407" id="Seg_7203" s="T406">dĭ-n</ta>
            <ta e="T408" id="Seg_7204" s="T407">koŋ-də</ta>
            <ta e="T409" id="Seg_7205" s="T408">koʔbdo-bə</ta>
            <ta e="T410" id="Seg_7206" s="T409">i-li-m</ta>
            <ta e="T412" id="Seg_7207" s="T411">dĭ</ta>
            <ta e="T413" id="Seg_7208" s="T412">nʼi</ta>
            <ta e="T414" id="Seg_7209" s="T413">par-luʔ-bi</ta>
            <ta e="T415" id="Seg_7210" s="T414">măn-də</ta>
            <ta e="T416" id="Seg_7211" s="T415">dĭ</ta>
            <ta e="T417" id="Seg_7212" s="T416">măn-də</ta>
            <ta e="T418" id="Seg_7213" s="T417">pušaj</ta>
            <ta e="T419" id="Seg_7214" s="T418">dĭ-m</ta>
            <ta e="T420" id="Seg_7215" s="T419">koŋ-də</ta>
            <ta e="T421" id="Seg_7216" s="T420">koʔbdo-bə</ta>
            <ta e="T422" id="Seg_7217" s="T421">i-li-m</ta>
            <ta e="T423" id="Seg_7218" s="T422">pušaj</ta>
            <ta e="T424" id="Seg_7219" s="T423">kan-gə-ʔjə</ta>
            <ta e="T425" id="Seg_7220" s="T424">ato</ta>
            <ta e="T426" id="Seg_7221" s="T425">ej</ta>
            <ta e="T427" id="Seg_7222" s="T426">jakše</ta>
            <ta e="T428" id="Seg_7223" s="T427">mo-lə-j</ta>
            <ta e="T430" id="Seg_7224" s="T429">ej</ta>
            <ta e="T431" id="Seg_7225" s="T430">mĭ-le-m</ta>
            <ta e="T432" id="Seg_7226" s="T431">dĭ</ta>
            <ta e="T433" id="Seg_7227" s="T432">šo-bi</ta>
            <ta e="T435" id="Seg_7228" s="T434">măm-bi</ta>
            <ta e="T436" id="Seg_7229" s="T435">dăre</ta>
            <ta e="T437" id="Seg_7230" s="T436">koŋ</ta>
            <ta e="T438" id="Seg_7231" s="T437">ej</ta>
            <ta e="T439" id="Seg_7232" s="T438">mĭ-lie-t</ta>
            <ta e="T440" id="Seg_7233" s="T439">koʔbdo-bə</ta>
            <ta e="T441" id="Seg_7234" s="T440">ej</ta>
            <ta e="T442" id="Seg_7235" s="T441">mĭ-lə-j</ta>
            <ta e="T443" id="Seg_7236" s="T442">jakše-ŋ</ta>
            <ta e="T444" id="Seg_7237" s="T443">dăk</ta>
            <ta e="T445" id="Seg_7238" s="T444">ej</ta>
            <ta e="T446" id="Seg_7239" s="T445">jakše</ta>
            <ta e="T447" id="Seg_7240" s="T446">mĭ-lə-j</ta>
            <ta e="T449" id="Seg_7241" s="T448">öʔ-leʔ</ta>
            <ta e="T451" id="Seg_7242" s="T450">ine-ʔi</ta>
            <ta e="T452" id="Seg_7243" s="T451">dĭ-zeŋ</ta>
            <ta e="T453" id="Seg_7244" s="T452">bar</ta>
            <ta e="T454" id="Seg_7245" s="T453">dĭn</ta>
            <ta e="T455" id="Seg_7246" s="T454">šöjmü-ndə</ta>
            <ta e="T456" id="Seg_7247" s="T455">saj-nʼiʔ-luʔ-lə-ʔjə</ta>
            <ta e="T457" id="Seg_7248" s="T456">dĭgəttə</ta>
            <ta e="T1072" id="Seg_7249" s="T457">kal-la</ta>
            <ta e="T458" id="Seg_7250" s="T1072">dʼür-lə-j</ta>
            <ta e="T459" id="Seg_7251" s="T458">dĭgəttə</ta>
            <ta e="T460" id="Seg_7252" s="T459">dĭ</ta>
            <ta e="T461" id="Seg_7253" s="T460">urgaːba</ta>
            <ta e="T462" id="Seg_7254" s="T461">öʔlu-bi</ta>
            <ta e="T463" id="Seg_7255" s="T462">sagər</ta>
            <ta e="T464" id="Seg_7256" s="T463">băra-gəʔ</ta>
            <ta e="T465" id="Seg_7257" s="T464">dĭ</ta>
            <ta e="T466" id="Seg_7258" s="T465">urgaːba</ta>
            <ta e="T467" id="Seg_7259" s="T466">ine-ʔi</ta>
            <ta e="T468" id="Seg_7260" s="T467">bar</ta>
            <ta e="T469" id="Seg_7261" s="T468">sürer-luʔ-pi</ta>
            <ta e="T470" id="Seg_7262" s="T469">dĭgəttə</ta>
            <ta e="T471" id="Seg_7263" s="T470">măn-də</ta>
            <ta e="T472" id="Seg_7264" s="T471">öʔlu-geʔ</ta>
            <ta e="T473" id="Seg_7265" s="T472">tüžöj-ʔjə</ta>
            <ta e="T474" id="Seg_7266" s="T473">pušaj</ta>
            <ta e="T475" id="Seg_7267" s="T474">tüžöj-ʔjə</ta>
            <ta e="T476" id="Seg_7268" s="T475">amnu-t-si</ta>
            <ta e="T477" id="Seg_7269" s="T476">bar</ta>
            <ta e="T478" id="Seg_7270" s="T477">ine-bə</ta>
            <ta e="T479" id="Seg_7271" s="T478">bar</ta>
            <ta e="T480" id="Seg_7272" s="T479">kut-lə-ʔjə</ta>
            <ta e="T481" id="Seg_7273" s="T480">dĭ</ta>
            <ta e="T482" id="Seg_7274" s="T481">dĭ</ta>
            <ta e="T483" id="Seg_7275" s="T482">bar</ta>
            <ta e="T484" id="Seg_7276" s="T483">băra-gə</ta>
            <ta e="T485" id="Seg_7277" s="T484">öʔlu-bi</ta>
            <ta e="T486" id="Seg_7278" s="T485">urgo</ta>
            <ta e="T487" id="Seg_7279" s="T486">men</ta>
            <ta e="T488" id="Seg_7280" s="T487">dĭ</ta>
            <ta e="T489" id="Seg_7281" s="T488">dĭ</ta>
            <ta e="T490" id="Seg_7282" s="T489">kam-bi</ta>
            <ta e="T491" id="Seg_7283" s="T490">dĭ-zeŋ</ta>
            <ta e="T492" id="Seg_7284" s="T491">bar</ta>
            <ta e="T493" id="Seg_7285" s="T492">sürer-lüʔ-bi</ta>
            <ta e="T494" id="Seg_7286" s="T493">dĭgəttə</ta>
            <ta e="T495" id="Seg_7287" s="T494">măn-də</ta>
            <ta e="T497" id="Seg_7288" s="T495">öʔlu-geʔ</ta>
            <ta e="T499" id="Seg_7289" s="T498">dĭgəttə</ta>
            <ta e="T500" id="Seg_7290" s="T499">koŋ</ta>
            <ta e="T503" id="Seg_7291" s="T502">öʔ-leʔ</ta>
            <ta e="T504" id="Seg_7292" s="T503">men-zeŋ</ta>
            <ta e="T505" id="Seg_7293" s="T504">pušaj</ta>
            <ta e="T506" id="Seg_7294" s="T505">men-zeŋ</ta>
            <ta e="T507" id="Seg_7295" s="T506">dĭ-m</ta>
            <ta e="T508" id="Seg_7296" s="T507">saj-nüzəʔ-lə-ʔjə</ta>
            <ta e="T509" id="Seg_7297" s="T508">dĭ</ta>
            <ta e="T510" id="Seg_7298" s="T509">öʔlu-bi</ta>
            <ta e="T511" id="Seg_7299" s="T510">men-dən</ta>
            <ta e="T512" id="Seg_7300" s="T511">a</ta>
            <ta e="T513" id="Seg_7301" s="T512">dĭ</ta>
            <ta e="T514" id="Seg_7302" s="T513">lʼisa</ta>
            <ta e="T515" id="Seg_7303" s="T514">öʔlu-bi</ta>
            <ta e="T516" id="Seg_7304" s="T515">men-zeŋ</ta>
            <ta e="T517" id="Seg_7305" s="T516">lʼisa-gən</ta>
            <ta e="T518" id="Seg_7306" s="T517">nuʔmə-luʔ-pi-ʔi</ta>
            <ta e="T519" id="Seg_7307" s="T518">dĭgəttə</ta>
            <ta e="T520" id="Seg_7308" s="T519">dĭ</ta>
            <ta e="T521" id="Seg_7309" s="T520">koŋ</ta>
            <ta e="T522" id="Seg_7310" s="T521">bar</ta>
            <ta e="T523" id="Seg_7311" s="T522">il</ta>
            <ta e="T524" id="Seg_7312" s="T523">oʔbdə-bi</ta>
            <ta e="T525" id="Seg_7313" s="T524">kan-žə-bəj</ta>
            <ta e="T526" id="Seg_7314" s="T525">dĭ-m</ta>
            <ta e="T527" id="Seg_7315" s="T526">i-lə-bəj</ta>
            <ta e="T528" id="Seg_7316" s="T527">il</ta>
            <ta e="T529" id="Seg_7317" s="T528">šo-bi-ʔi</ta>
            <ta e="T530" id="Seg_7318" s="T529">dĭ</ta>
            <ta e="T531" id="Seg_7319" s="T530">bar</ta>
            <ta e="T532" id="Seg_7320" s="T531">băra-bə</ta>
            <ta e="T533" id="Seg_7321" s="T532">bü</ta>
            <ta e="T534" id="Seg_7322" s="T533">kămnə-bi</ta>
            <ta e="T535" id="Seg_7323" s="T534">i</ta>
            <ta e="T536" id="Seg_7324" s="T535">bü</ta>
            <ta e="T537" id="Seg_7325" s="T536">il</ta>
            <ta e="T538" id="Seg_7326" s="T537">il</ta>
            <ta e="T539" id="Seg_7327" s="T538">bar</ta>
            <ta e="T540" id="Seg_7328" s="T539">nuʔmə-luʔ-bi-ʔi</ta>
            <ta e="T541" id="Seg_7329" s="T540">bü-gə</ta>
            <ta e="T543" id="Seg_7330" s="T542">dĭgəttə</ta>
            <ta e="T544" id="Seg_7331" s="T543">dĭ</ta>
            <ta e="T545" id="Seg_7332" s="T544">koŋ</ta>
            <ta e="T546" id="Seg_7333" s="T545">kirgar-laʔbə</ta>
            <ta e="T547" id="Seg_7334" s="T546">pʼel-də</ta>
            <ta e="T549" id="Seg_7335" s="T548">i-ge</ta>
            <ta e="T550" id="Seg_7336" s="T549">măm-bi</ta>
            <ta e="T551" id="Seg_7337" s="T550">măn</ta>
            <ta e="T552" id="Seg_7338" s="T551">i-ge</ta>
            <ta e="T553" id="Seg_7339" s="T552">tüžöj-ʔi</ta>
            <ta e="T554" id="Seg_7340" s="T553">ine-ʔi</ta>
            <ta e="T555" id="Seg_7341" s="T554">men-zeŋ-də</ta>
            <ta e="T556" id="Seg_7342" s="T555">măm-bie-m</ta>
            <ta e="T557" id="Seg_7343" s="T556">aktʼa</ta>
            <ta e="T558" id="Seg_7344" s="T557">iʔgö</ta>
            <ta e="T559" id="Seg_7345" s="T558">măm-bi</ta>
            <ta e="T560" id="Seg_7346" s="T559">ĭmbi=də</ta>
            <ta e="T561" id="Seg_7347" s="T560">măna</ta>
            <ta e="T562" id="Seg_7348" s="T561">ej</ta>
            <ta e="T563" id="Seg_7349" s="T562">kereʔ</ta>
            <ta e="T564" id="Seg_7350" s="T563">tolʼkă</ta>
            <ta e="T565" id="Seg_7351" s="T564">koʔbdo-m</ta>
            <ta e="T566" id="Seg_7352" s="T565">i-zittə</ta>
            <ta e="T568" id="Seg_7353" s="T567">dĭgəttə</ta>
            <ta e="T569" id="Seg_7354" s="T568">sagər</ta>
            <ta e="T570" id="Seg_7355" s="T569">băra-nə</ta>
            <ta e="T571" id="Seg_7356" s="T570">bü</ta>
            <ta e="T572" id="Seg_7357" s="T571">i-bi</ta>
            <ta e="T574" id="Seg_7358" s="T572">i</ta>
            <ta e="T576" id="Seg_7359" s="T575">dĭgəttə</ta>
            <ta e="T577" id="Seg_7360" s="T576">mĭ-luʔ-pi</ta>
            <ta e="T578" id="Seg_7361" s="T577">koʔbdo-bə</ta>
            <ta e="T579" id="Seg_7362" s="T578">dĭ</ta>
            <ta e="T580" id="Seg_7363" s="T579">amnol-bi</ta>
            <ta e="T581" id="Seg_7364" s="T580">amnol-bi</ta>
            <ta e="T582" id="Seg_7365" s="T581">šöjmü-nə</ta>
            <ta e="T583" id="Seg_7366" s="T582">i</ta>
            <ta e="T584" id="Seg_7367" s="T583">kam-bi</ta>
            <ta e="T585" id="Seg_7368" s="T584">maʔ-ndə</ta>
            <ta e="T586" id="Seg_7369" s="T585">šide</ta>
            <ta e="T587" id="Seg_7370" s="T586">kaga</ta>
            <ta e="T588" id="Seg_7371" s="T587">amno-laʔ-pi</ta>
            <ta e="T589" id="Seg_7372" s="T588">amno-laʔbə-bi</ta>
            <ta e="T590" id="Seg_7373" s="T589">dĭ-zeŋ</ta>
            <ta e="T591" id="Seg_7374" s="T590">ne-zeŋ-də</ta>
            <ta e="T592" id="Seg_7375" s="T591">i-bi-ʔi</ta>
            <ta e="T593" id="Seg_7376" s="T592">dĭ-zeŋ</ta>
            <ta e="T594" id="Seg_7377" s="T593">oʔbdə-bi-ʔi</ta>
            <ta e="T595" id="Seg_7378" s="T594">kam-bi-ʔi</ta>
            <ta e="T596" id="Seg_7379" s="T595">dʼije-nə</ta>
            <ta e="T597" id="Seg_7380" s="T596">a</ta>
            <ta e="T598" id="Seg_7381" s="T597">ne-zeŋ</ta>
            <ta e="T599" id="Seg_7382" s="T598">maʔ-ndə</ta>
            <ta e="T600" id="Seg_7383" s="T599">bar</ta>
            <ta e="T601" id="Seg_7384" s="T600">nüdʼi</ta>
            <ta e="T602" id="Seg_7385" s="T601">nüdʼi-n</ta>
            <ta e="T604" id="Seg_7386" s="T603">amor-zittə</ta>
            <ta e="T605" id="Seg_7387" s="T604">amnə-bi-ʔi</ta>
            <ta e="T606" id="Seg_7388" s="T605">dĭgəttə</ta>
            <ta e="T607" id="Seg_7389" s="T606">măndo-lia-ʔi</ta>
            <ta e="T608" id="Seg_7390" s="T607">aspa-ʔən</ta>
            <ta e="T610" id="Seg_7391" s="T609">girgit=də</ta>
            <ta e="T611" id="Seg_7392" s="T610">kuza</ta>
            <ta e="T612" id="Seg_7393" s="T611">amno-laʔbə</ta>
            <ta e="T613" id="Seg_7394" s="T612">dĭ-zeŋ</ta>
            <ta e="T614" id="Seg_7395" s="T613">moː-ʔi</ta>
            <ta e="T616" id="Seg_7396" s="T615">oʔbdə-bi-ʔi</ta>
            <ta e="T617" id="Seg_7397" s="T616">i</ta>
            <ta e="T618" id="Seg_7398" s="T617">aspaʔ-də</ta>
            <ta e="T620" id="Seg_7399" s="T619">aspaʔ-də</ta>
            <ta e="T621" id="Seg_7400" s="T620">em-bi-ʔi</ta>
            <ta e="T622" id="Seg_7401" s="T621">a</ta>
            <ta e="T623" id="Seg_7402" s="T622">dĭ</ta>
            <ta e="T624" id="Seg_7403" s="T623">mu</ta>
            <ta e="T626" id="Seg_7404" s="T624">bar</ta>
            <ta e="T628" id="Seg_7405" s="T627">dʼüʔpi</ta>
            <ta e="T629" id="Seg_7406" s="T628">i-bi</ta>
            <ta e="T630" id="Seg_7407" s="T629">dĭgəttə</ta>
            <ta e="T631" id="Seg_7408" s="T630">dĭ</ta>
            <ta e="T632" id="Seg_7409" s="T631">šo-bi</ta>
            <ta e="T633" id="Seg_7410" s="T632">ej</ta>
            <ta e="T634" id="Seg_7411" s="T633">mo-bi</ta>
            <ta e="T635" id="Seg_7412" s="T634">nendə-sʼtə</ta>
            <ta e="T636" id="Seg_7413" s="T635">dĭ</ta>
            <ta e="T637" id="Seg_7414" s="T636">kuza</ta>
            <ta e="T638" id="Seg_7415" s="T637">onʼiʔ</ta>
            <ta e="T639" id="Seg_7416" s="T638">ne-m</ta>
            <ta e="T640" id="Seg_7417" s="T639">kut-laʔ-pi</ta>
            <ta e="T641" id="Seg_7418" s="T640">dĭgəttə</ta>
            <ta e="T642" id="Seg_7419" s="T641">šide</ta>
            <ta e="T644" id="Seg_7420" s="T643">bazoʔ</ta>
            <ta e="T645" id="Seg_7421" s="T644">ne-nə</ta>
            <ta e="T646" id="Seg_7422" s="T645">măn-də</ta>
            <ta e="T647" id="Seg_7423" s="T646">ĭmbi</ta>
            <ta e="T648" id="Seg_7424" s="T647">tăn</ta>
            <ta e="T649" id="Seg_7425" s="T648">amno-laʔbə-l</ta>
            <ta e="T650" id="Seg_7426" s="T649">nende-ʔ</ta>
            <ta e="T651" id="Seg_7427" s="T650">dĭ</ta>
            <ta e="T652" id="Seg_7428" s="T651">bar</ta>
            <ta e="T654" id="Seg_7429" s="T653">nendə-sʼtə</ta>
            <ta e="T655" id="Seg_7430" s="T654">dĭ</ta>
            <ta e="T656" id="Seg_7431" s="T655">dĭʔ-nə</ta>
            <ta e="T657" id="Seg_7432" s="T656">baltu-ziʔ</ta>
            <ta e="T658" id="Seg_7433" s="T657">ulu-bə</ta>
            <ta e="T659" id="Seg_7434" s="T658">jaʔ-pi</ta>
            <ta e="T660" id="Seg_7435" s="T659">dĭgəttə</ta>
            <ta e="T661" id="Seg_7436" s="T660">es-seŋ-də</ta>
            <ta e="T662" id="Seg_7437" s="T661">kut-laːm-bi</ta>
            <ta e="T663" id="Seg_7438" s="T662">dĭgəttə</ta>
            <ta e="T664" id="Seg_7439" s="T663">tĭ</ta>
            <ta e="T666" id="Seg_7440" s="T665">dĭ-zeŋ-də</ta>
            <ta e="T667" id="Seg_7441" s="T666">ma-ndə</ta>
            <ta e="T669" id="Seg_7442" s="T668">par-luʔ-pi</ta>
            <ta e="T670" id="Seg_7443" s="T669">dĭ-zeŋ</ta>
            <ta e="T672" id="Seg_7444" s="T671">ne-zeŋ-də</ta>
            <ta e="T673" id="Seg_7445" s="T672">naga</ta>
            <ta e="T674" id="Seg_7446" s="T673">šindi=də</ta>
            <ta e="T675" id="Seg_7447" s="T674">kuʔ-pi</ta>
            <ta e="T676" id="Seg_7448" s="T675">es-sem</ta>
            <ta e="T677" id="Seg_7449" s="T676">i</ta>
            <ta e="T678" id="Seg_7450" s="T677">ne-zeŋ-də</ta>
            <ta e="T680" id="Seg_7451" s="T679">amno-bi-ʔi</ta>
            <ta e="T681" id="Seg_7452" s="T680">büzʼe</ta>
            <ta e="T682" id="Seg_7453" s="T681">nüke-t-si</ta>
            <ta e="T683" id="Seg_7454" s="T682">dĭ-zeŋ</ta>
            <ta e="T684" id="Seg_7455" s="T683">ĭmbi=də</ta>
            <ta e="T685" id="Seg_7456" s="T684">nago-bi</ta>
            <ta e="T686" id="Seg_7457" s="T685">tolʼkă</ta>
            <ta e="T687" id="Seg_7458" s="T686">onʼiʔ</ta>
            <ta e="T688" id="Seg_7459" s="T687">tüžöj</ta>
            <ta e="T689" id="Seg_7460" s="T688">dĭ-zeŋ</ta>
            <ta e="T690" id="Seg_7461" s="T689">amor-zittə</ta>
            <ta e="T691" id="Seg_7462" s="T690">naga</ta>
            <ta e="T692" id="Seg_7463" s="T691">i-bi</ta>
            <ta e="T693" id="Seg_7464" s="T692">dĭgəttə</ta>
            <ta e="T694" id="Seg_7465" s="T693">tüžöj-də</ta>
            <ta e="T695" id="Seg_7466" s="T694">băt-luʔ-pi</ta>
            <ta e="T696" id="Seg_7467" s="T695">nüke-m</ta>
            <ta e="T697" id="Seg_7468" s="T696">sürer-luʔ-pi</ta>
            <ta e="T698" id="Seg_7469" s="T697">kan-a-ʔ</ta>
            <ta e="T699" id="Seg_7470" s="T698">ato</ta>
            <ta e="T700" id="Seg_7471" s="T699">măna</ta>
            <ta e="T701" id="Seg_7472" s="T700">bos-kəndə-m</ta>
            <ta e="T702" id="Seg_7473" s="T701">amga</ta>
            <ta e="T703" id="Seg_7474" s="T702">mo-lə-j</ta>
            <ta e="T704" id="Seg_7475" s="T703">dĭ</ta>
            <ta e="T705" id="Seg_7476" s="T704">nüke</ta>
            <ta e="T706" id="Seg_7477" s="T705">kam-bi</ta>
            <ta e="T707" id="Seg_7478" s="T706">kam-bi</ta>
            <ta e="T708" id="Seg_7479" s="T707">išo</ta>
            <ta e="T709" id="Seg_7480" s="T708">maʔ</ta>
            <ta e="T710" id="Seg_7481" s="T709">ku-bi</ta>
            <ta e="T711" id="Seg_7482" s="T710">maʔ-də</ta>
            <ta e="T712" id="Seg_7483" s="T711">šo-bi</ta>
            <ta e="T713" id="Seg_7484" s="T712">dĭn</ta>
            <ta e="T714" id="Seg_7485" s="T713">sʼel</ta>
            <ta e="T715" id="Seg_7486" s="T714">iʔbo-laʔbə</ta>
            <ta e="T716" id="Seg_7487" s="T715">uja</ta>
            <ta e="T717" id="Seg_7488" s="T716">iʔbo-laʔbə</ta>
            <ta e="T718" id="Seg_7489" s="T717">dĭ</ta>
            <ta e="T719" id="Seg_7490" s="T718">mĭnzər-bi</ta>
            <ta e="T720" id="Seg_7491" s="T719">bar</ta>
            <ta e="T721" id="Seg_7492" s="T720">amor-bi</ta>
            <ta e="T722" id="Seg_7493" s="T721">nün-nie-t</ta>
            <ta e="T723" id="Seg_7494" s="T722">bar</ta>
            <ta e="T724" id="Seg_7495" s="T723">šindi=də</ta>
            <ta e="T725" id="Seg_7496" s="T724">šonə-ga</ta>
            <ta e="T726" id="Seg_7497" s="T725">iʔgö</ta>
            <ta e="T727" id="Seg_7498" s="T726">ular</ta>
            <ta e="T729" id="Seg_7499" s="T728">deʔ-pi</ta>
            <ta e="T730" id="Seg_7500" s="T729">tüžöj-ʔi</ta>
            <ta e="T731" id="Seg_7501" s="T730">ine-ʔi</ta>
            <ta e="T732" id="Seg_7502" s="T731">dĭgəttə</ta>
            <ta e="T733" id="Seg_7503" s="T732">šo-bi</ta>
            <ta e="T734" id="Seg_7504" s="T733">maʔ-də</ta>
            <ta e="T735" id="Seg_7505" s="T734">măn-də</ta>
            <ta e="T737" id="Seg_7506" s="T736">kar-də</ta>
            <ta e="T738" id="Seg_7507" s="T737">aji</ta>
            <ta e="T739" id="Seg_7508" s="T738">aji</ta>
            <ta e="T740" id="Seg_7509" s="T739">kar-luʔ-pi</ta>
            <ta e="T741" id="Seg_7510" s="T740">dĭ</ta>
            <ta e="T742" id="Seg_7511" s="T741">šo-bi</ta>
            <ta e="T743" id="Seg_7512" s="T742">măndə-r-la</ta>
            <ta e="T744" id="Seg_7513" s="T743">šindi=də</ta>
            <ta e="T745" id="Seg_7514" s="T744">i-bi</ta>
            <ta e="T746" id="Seg_7515" s="T745">sʼel</ta>
            <ta e="T747" id="Seg_7516" s="T746">naga</ta>
            <ta e="T748" id="Seg_7517" s="T747">uja</ta>
            <ta e="T749" id="Seg_7518" s="T748">naga</ta>
            <ta e="T750" id="Seg_7519" s="T749">ipek</ta>
            <ta e="T751" id="Seg_7520" s="T750">naga</ta>
            <ta e="T752" id="Seg_7521" s="T751">šində</ta>
            <ta e="T753" id="Seg_7522" s="T752">dön</ta>
            <ta e="T754" id="Seg_7523" s="T753">i-bi</ta>
            <ta e="T755" id="Seg_7524" s="T754">tibi</ta>
            <ta e="T756" id="Seg_7525" s="T755">dăk</ta>
            <ta e="T757" id="Seg_7526" s="T756">büzʼe-m</ta>
            <ta e="T758" id="Seg_7527" s="T757">mo-lə-j</ta>
            <ta e="T759" id="Seg_7528" s="T758">ne</ta>
            <ta e="T760" id="Seg_7529" s="T759">dăk</ta>
            <ta e="T761" id="Seg_7530" s="T760">hele-m</ta>
            <ta e="T762" id="Seg_7531" s="T761">mo-lə-j</ta>
            <ta e="T763" id="Seg_7532" s="T762">dĭgəttə</ta>
            <ta e="T764" id="Seg_7533" s="T763">dĭ</ta>
            <ta e="T765" id="Seg_7534" s="T764">nüke</ta>
            <ta e="T766" id="Seg_7535" s="T765">uʔbdə-bi</ta>
            <ta e="T767" id="Seg_7536" s="T766">šo-bi</ta>
            <ta e="T768" id="Seg_7537" s="T767">dĭ</ta>
            <ta e="T769" id="Seg_7538" s="T768">dĭ-m</ta>
            <ta e="T770" id="Seg_7539" s="T769">amnol-bi</ta>
            <ta e="T771" id="Seg_7540" s="T770">amor-la</ta>
            <ta e="T772" id="Seg_7541" s="T771">amno-bi-ʔi</ta>
            <ta e="T773" id="Seg_7542" s="T772">dĭ</ta>
            <ta e="T774" id="Seg_7543" s="T773">surar-laʔbə</ta>
            <ta e="T775" id="Seg_7544" s="T774">ĭmbi</ta>
            <ta e="T776" id="Seg_7545" s="T775">tăn</ta>
            <ta e="T777" id="Seg_7546" s="T776">uda-l</ta>
            <ta e="T778" id="Seg_7547" s="T777">üjü-l</ta>
            <ta e="T779" id="Seg_7548" s="T778">naga</ta>
            <ta e="T780" id="Seg_7549" s="T779">kăde</ta>
            <ta e="T781" id="Seg_7550" s="T780">tăn</ta>
            <ta e="T782" id="Seg_7551" s="T781">mĭn-lie-l</ta>
            <ta e="T783" id="Seg_7552" s="T782">da</ta>
            <ta e="T784" id="Seg_7553" s="T783">dăre</ta>
            <ta e="T785" id="Seg_7554" s="T784">mĭn-lie-l</ta>
            <ta e="T786" id="Seg_7555" s="T785">tăn</ta>
            <ta e="T787" id="Seg_7556" s="T786">jama-nə</ta>
            <ta e="T788" id="Seg_7557" s="T787">suʔmi-lə-l</ta>
            <ta e="T789" id="Seg_7558" s="T788">suʔmi-lə-m</ta>
            <ta e="T790" id="Seg_7559" s="T789">dĭgəttə</ta>
            <ta e="T791" id="Seg_7560" s="T790">kam-bi-ʔi</ta>
            <ta e="T792" id="Seg_7561" s="T791">dĭ</ta>
            <ta e="T793" id="Seg_7562" s="T792">süʔmi-bi</ta>
            <ta e="T794" id="Seg_7563" s="T793">dĭ</ta>
            <ta e="T795" id="Seg_7564" s="T794">dĭʔ-nə</ta>
            <ta e="T796" id="Seg_7565" s="T795">baltu-zi</ta>
            <ta e="T797" id="Seg_7566" s="T796">ulu-bə</ta>
            <ta e="T798" id="Seg_7567" s="T797">bar</ta>
            <ta e="T799" id="Seg_7568" s="T798">saj-jaʔ-pi</ta>
            <ta e="T800" id="Seg_7569" s="T799">dĭgəttə</ta>
            <ta e="T801" id="Seg_7570" s="T800">bar</ta>
            <ta e="T802" id="Seg_7571" s="T801">šama</ta>
            <ta e="T803" id="Seg_7572" s="T802">i-bi</ta>
            <ta e="T804" id="Seg_7573" s="T803">sʼel-də</ta>
            <ta e="T805" id="Seg_7574" s="T804">i-bi</ta>
            <ta e="T806" id="Seg_7575" s="T805">uja</ta>
            <ta e="T807" id="Seg_7576" s="T806">i-bi</ta>
            <ta e="T808" id="Seg_7577" s="T807">puzɨrʼ-də</ta>
            <ta e="T809" id="Seg_7578" s="T808">em-bi</ta>
            <ta e="T810" id="Seg_7579" s="T809">dĭgəttə</ta>
            <ta e="T811" id="Seg_7580" s="T810">par-luʔ-bi</ta>
            <ta e="T812" id="Seg_7581" s="T811">bos-tə</ta>
            <ta e="T813" id="Seg_7582" s="T812">büzʼe-ndə</ta>
            <ta e="T814" id="Seg_7583" s="T813">maʔ-də</ta>
            <ta e="T817" id="Seg_7584" s="T816">sʼa-bi</ta>
            <ta e="T818" id="Seg_7585" s="T817">a</ta>
            <ta e="T819" id="Seg_7586" s="T818">büzʼe</ta>
            <ta e="T820" id="Seg_7587" s="T819">kös-si</ta>
            <ta e="T821" id="Seg_7588" s="T820">uda</ta>
            <ta e="T823" id="Seg_7589" s="T822">uda-bə</ta>
            <ta e="T825" id="Seg_7590" s="T823">bar</ta>
            <ta e="T826" id="Seg_7591" s="T825">uda-bə</ta>
            <ta e="T828" id="Seg_7592" s="T827">kös-si</ta>
            <ta e="T830" id="Seg_7593" s="T828">bar</ta>
            <ta e="T831" id="Seg_7594" s="T830">dö-m</ta>
            <ta e="T832" id="Seg_7595" s="T831">uja-m</ta>
            <ta e="T833" id="Seg_7596" s="T832">am-nə-m</ta>
            <ta e="T834" id="Seg_7597" s="T833">dö-m</ta>
            <ta e="T835" id="Seg_7598" s="T834">uja-m</ta>
            <ta e="T836" id="Seg_7599" s="T835">am-nə-m</ta>
            <ta e="T837" id="Seg_7600" s="T836">a</ta>
            <ta e="T838" id="Seg_7601" s="T837">dĭ</ta>
            <ta e="T839" id="Seg_7602" s="T838">barəʔ-luʔ-pi</ta>
            <ta e="T840" id="Seg_7603" s="T839">dĭʔ-nə</ta>
            <ta e="T841" id="Seg_7604" s="T840">dĭ</ta>
            <ta e="T842" id="Seg_7605" s="T841">kabar-luʔ-pi</ta>
            <ta e="T843" id="Seg_7606" s="T842">vot</ta>
            <ta e="T844" id="Seg_7607" s="T843">kudaj</ta>
            <ta e="T845" id="Seg_7608" s="T844">măna</ta>
            <ta e="T846" id="Seg_7609" s="T845">mĭ-bi</ta>
            <ta e="T847" id="Seg_7610" s="T846">da</ta>
            <ta e="T848" id="Seg_7611" s="T847">kudaj</ta>
            <ta e="T849" id="Seg_7612" s="T848">tănan</ta>
            <ta e="T850" id="Seg_7613" s="T849">mĭ-lə-j</ta>
            <ta e="T851" id="Seg_7614" s="T850">pi</ta>
            <ta e="T852" id="Seg_7615" s="T851">tănan</ta>
            <ta e="T853" id="Seg_7616" s="T852">mĭ-lə-j</ta>
            <ta e="T854" id="Seg_7617" s="T853">kudaj</ta>
            <ta e="T855" id="Seg_7618" s="T854">dĭ-m</ta>
            <ta e="T856" id="Seg_7619" s="T855">măn</ta>
            <ta e="T857" id="Seg_7620" s="T856">mĭ-bie-m</ta>
            <ta e="T858" id="Seg_7621" s="T857">dĭgəttə</ta>
            <ta e="T859" id="Seg_7622" s="T858">dĭ</ta>
            <ta e="T860" id="Seg_7623" s="T859">amor-bi</ta>
            <ta e="T861" id="Seg_7624" s="T860">kam-bi-ʔi</ta>
            <ta e="T862" id="Seg_7625" s="T861">dĭn</ta>
            <ta e="T863" id="Seg_7626" s="T862">ine-ʔi</ta>
            <ta e="T864" id="Seg_7627" s="T863">tüžöj-ʔi</ta>
            <ta e="T865" id="Seg_7628" s="T864">ular-əʔi</ta>
            <ta e="T866" id="Seg_7629" s="T865">šo-bi-ʔi</ta>
            <ta e="T867" id="Seg_7630" s="T866">dĭbər</ta>
            <ta e="T868" id="Seg_7631" s="T867">dĭ</ta>
            <ta e="T869" id="Seg_7632" s="T868">măn-də</ta>
            <ta e="T870" id="Seg_7633" s="T869">pünö-r-zittə</ta>
            <ta e="T871" id="Seg_7634" s="T870">axota</ta>
            <ta e="T872" id="Seg_7635" s="T871">i-ʔ</ta>
            <ta e="T873" id="Seg_7636" s="T872">pünö-r-a-ʔ</ta>
            <ta e="T874" id="Seg_7637" s="T873">ato</ta>
            <ta e="T875" id="Seg_7638" s="T874">bar</ta>
            <ta e="T876" id="Seg_7639" s="T875">nuʔmə-luʔ-lə-ʔjə</ta>
            <ta e="T877" id="Seg_7640" s="T876">dĭ</ta>
            <ta e="T881" id="Seg_7641" s="T880">pünö-r-bi</ta>
            <ta e="T882" id="Seg_7642" s="T881">i</ta>
            <ta e="T883" id="Seg_7643" s="T882">bar</ta>
            <ta e="T884" id="Seg_7644" s="T883">ine-zeŋ-də</ta>
            <ta e="T885" id="Seg_7645" s="T884">ular-zaŋ-də</ta>
            <ta e="T886" id="Seg_7646" s="T885">i</ta>
            <ta e="T887" id="Seg_7647" s="T886">tüžöj-ʔjə</ta>
            <ta e="T888" id="Seg_7648" s="T887">üʔmə-luʔ-pi-ʔi</ta>
            <ta e="T889" id="Seg_7649" s="T888">dĭ</ta>
            <ta e="T890" id="Seg_7650" s="T889">davaj</ta>
            <ta e="T892" id="Seg_7651" s="T890">süt-si</ta>
            <ta e="T895" id="Seg_7652" s="T894">süt-si</ta>
            <ta e="T896" id="Seg_7653" s="T895">kămna-sʼtə</ta>
            <ta e="T897" id="Seg_7654" s="T896">dĭ-zeŋ-də</ta>
            <ta e="T898" id="Seg_7655" s="T897">ular</ta>
            <ta e="T899" id="Seg_7656" s="T898">šiʔ</ta>
            <ta e="T900" id="Seg_7657" s="T899">poʔto</ta>
            <ta e="T901" id="Seg_7658" s="T900">mo-laʔ</ta>
            <ta e="T902" id="Seg_7659" s="T901">tüžöj-əʔi</ta>
            <ta e="T903" id="Seg_7660" s="T902">šiʔ</ta>
            <ta e="T904" id="Seg_7661" s="T903">šiʔ</ta>
            <ta e="T905" id="Seg_7662" s="T904">pulan</ta>
            <ta e="T906" id="Seg_7663" s="T905">mo-gaʔ</ta>
            <ta e="T907" id="Seg_7664" s="T906">a</ta>
            <ta e="T908" id="Seg_7665" s="T907">šiʔ</ta>
            <ta e="T910" id="Seg_7666" s="T908">ine-ʔi</ta>
            <ta e="T911" id="Seg_7667" s="T910">nöməl-luʔ-pie-m</ta>
            <ta e="T915" id="Seg_7668" s="T914">măm-bi</ta>
            <ta e="T916" id="Seg_7669" s="T915">pušaj</ta>
            <ta e="T919" id="Seg_7670" s="T918">dĭgəttə</ta>
            <ta e="T920" id="Seg_7671" s="T919">nüke</ta>
            <ta e="T921" id="Seg_7672" s="T920">i</ta>
            <ta e="T922" id="Seg_7673" s="T921">büzʼe</ta>
            <ta e="T923" id="Seg_7674" s="T922">baʔ-luʔ-bi</ta>
            <ta e="T924" id="Seg_7675" s="T923">ĭmbi=də</ta>
            <ta e="T925" id="Seg_7676" s="T924">bazoʔ</ta>
            <ta e="T926" id="Seg_7677" s="T925">naga</ta>
            <ta e="T927" id="Seg_7678" s="T926">dĭ-zeŋ-də</ta>
            <ta e="T928" id="Seg_7679" s="T927">bar</ta>
            <ta e="T1073" id="Seg_7680" s="T928">kal-la</ta>
            <ta e="T1071" id="Seg_7681" s="T1073">dʼür-bi-ʔi</ta>
            <ta e="T930" id="Seg_7682" s="T929">amno-bi</ta>
            <ta e="T931" id="Seg_7683" s="T930">büzʼe</ta>
            <ta e="T932" id="Seg_7684" s="T931">dĭn</ta>
            <ta e="T933" id="Seg_7685" s="T932">nagur</ta>
            <ta e="T934" id="Seg_7686" s="T933">koʔbdo-t</ta>
            <ta e="T935" id="Seg_7687" s="T934">i-bi</ta>
            <ta e="T936" id="Seg_7688" s="T935">dĭ-m</ta>
            <ta e="T937" id="Seg_7689" s="T936">kăštə-bi-ʔi</ta>
            <ta e="T1075" id="Seg_7690" s="T937">Kazan</ta>
            <ta e="T938" id="Seg_7691" s="T1075">tura-nə</ta>
            <ta e="T939" id="Seg_7692" s="T938">štobɨ</ta>
            <ta e="T940" id="Seg_7693" s="T939">šo-bi</ta>
            <ta e="T941" id="Seg_7694" s="T940">a</ta>
            <ta e="T942" id="Seg_7695" s="T941">urgo</ta>
            <ta e="T943" id="Seg_7696" s="T942">koʔbdo-t</ta>
            <ta e="T944" id="Seg_7697" s="T943">măn</ta>
            <ta e="T945" id="Seg_7698" s="T944">ka-la-m</ta>
            <ta e="T946" id="Seg_7699" s="T945">eʔbdə-bə</ta>
            <ta e="T948" id="Seg_7700" s="T947">băʔ-pi</ta>
            <ta e="T949" id="Seg_7701" s="T948">piʔmə</ta>
            <ta e="T950" id="Seg_7702" s="T949">šer-bi</ta>
            <ta e="T951" id="Seg_7703" s="T950">kujnek</ta>
            <ta e="T952" id="Seg_7704" s="T951">šer-bi</ta>
            <ta e="T953" id="Seg_7705" s="T952">üžü</ta>
            <ta e="T954" id="Seg_7706" s="T953">šer-bi</ta>
            <ta e="T955" id="Seg_7707" s="T954">kam-bi</ta>
            <ta e="T956" id="Seg_7708" s="T955">a</ta>
            <ta e="T957" id="Seg_7709" s="T956">aba-t</ta>
            <ta e="T958" id="Seg_7710" s="T957">urgaːba-zi</ta>
            <ta e="T959" id="Seg_7711" s="T958">a-bi</ta>
            <ta e="T960" id="Seg_7712" s="T959">i</ta>
            <ta e="T961" id="Seg_7713" s="T960">dĭʔ-nə</ta>
            <ta e="T962" id="Seg_7714" s="T961">šonə-ga</ta>
            <ta e="T963" id="Seg_7715" s="T962">dĭ</ta>
            <ta e="T964" id="Seg_7716" s="T963">bar</ta>
            <ta e="T965" id="Seg_7717" s="T964">nereʔ-luʔ-pi</ta>
            <ta e="T966" id="Seg_7718" s="T965">i</ta>
            <ta e="T967" id="Seg_7719" s="T966">maʔ-ndə</ta>
            <ta e="T968" id="Seg_7720" s="T967">nuʔmə-luʔ-pi</ta>
            <ta e="T969" id="Seg_7721" s="T968">dĭ</ta>
            <ta e="T970" id="Seg_7722" s="T969">šo-bi</ta>
            <ta e="T971" id="Seg_7723" s="T970">ĭmbi</ta>
            <ta e="T972" id="Seg_7724" s="T971">ej</ta>
            <ta e="T973" id="Seg_7725" s="T972">kam-bia-m</ta>
            <ta e="T975" id="Seg_7726" s="T974">dĭn</ta>
            <ta e="T976" id="Seg_7727" s="T975">urgaːba</ta>
            <ta e="T977" id="Seg_7728" s="T976">măn</ta>
            <ta e="T978" id="Seg_7729" s="T977">nereʔ-luʔ-pie-m</ta>
            <ta e="T979" id="Seg_7730" s="T978">maʔ-ndə</ta>
            <ta e="T980" id="Seg_7731" s="T979">šo-bia-m</ta>
            <ta e="T981" id="Seg_7732" s="T980">dĭgəttə</ta>
            <ta e="T982" id="Seg_7733" s="T981">baška</ta>
            <ta e="T983" id="Seg_7734" s="T982">koʔbdo</ta>
            <ta e="T984" id="Seg_7735" s="T983">măn</ta>
            <ta e="T985" id="Seg_7736" s="T984">ka-la-m</ta>
            <ta e="T986" id="Seg_7737" s="T985">no</ta>
            <ta e="T987" id="Seg_7738" s="T986">kan-a-ʔ</ta>
            <ta e="T988" id="Seg_7739" s="T987">šer-bi</ta>
            <ta e="T989" id="Seg_7740" s="T988">piʔmə-ʔi</ta>
            <ta e="T990" id="Seg_7741" s="T989">kujnek</ta>
            <ta e="T991" id="Seg_7742" s="T990">šer-bi</ta>
            <ta e="T992" id="Seg_7743" s="T991">eʔbdə-bə</ta>
            <ta e="T993" id="Seg_7744" s="T992">băʔ-pi</ta>
            <ta e="T994" id="Seg_7745" s="T993">üžü-bə</ta>
            <ta e="T995" id="Seg_7746" s="T994">šer-bi</ta>
            <ta e="T996" id="Seg_7747" s="T995">kam-bi</ta>
            <ta e="T997" id="Seg_7748" s="T996">dĭgəttə</ta>
            <ta e="T998" id="Seg_7749" s="T997">aba-t</ta>
            <ta e="T999" id="Seg_7750" s="T998">mo-laːm-bi</ta>
            <ta e="T1000" id="Seg_7751" s="T999">urgaːba-zi</ta>
            <ta e="T1001" id="Seg_7752" s="T1000">kam-bi</ta>
            <ta e="T1002" id="Seg_7753" s="T1001">dĭ</ta>
            <ta e="T1003" id="Seg_7754" s="T1002">bar</ta>
            <ta e="T1004" id="Seg_7755" s="T1003">nereʔ-luʔ-pi</ta>
            <ta e="T1005" id="Seg_7756" s="T1004">maʔ-ndə</ta>
            <ta e="T1006" id="Seg_7757" s="T1005">šo-bi</ta>
            <ta e="T1007" id="Seg_7758" s="T1006">ĭmbi</ta>
            <ta e="T1008" id="Seg_7759" s="T1007">šo-bia-l</ta>
            <ta e="T1009" id="Seg_7760" s="T1008">da</ta>
            <ta e="T1010" id="Seg_7761" s="T1009">dĭn</ta>
            <ta e="T1011" id="Seg_7762" s="T1010">urgaːba</ta>
            <ta e="T1012" id="Seg_7763" s="T1011">šonə-ga</ta>
            <ta e="T1013" id="Seg_7764" s="T1012">măn</ta>
            <ta e="T1014" id="Seg_7765" s="T1013">nereʔ-luʔ-pie-m</ta>
            <ta e="T1015" id="Seg_7766" s="T1014">maʔ-ndə</ta>
            <ta e="T1016" id="Seg_7767" s="T1015">nuʔmə-luʔ-pie-m</ta>
            <ta e="T1017" id="Seg_7768" s="T1016">kabarləj</ta>
            <ta e="T1019" id="Seg_7769" s="T1018">dĭgəttə</ta>
            <ta e="T1020" id="Seg_7770" s="T1019">üdʼüge</ta>
            <ta e="T1021" id="Seg_7771" s="T1020">koʔbdo</ta>
            <ta e="T1022" id="Seg_7772" s="T1021">măn</ta>
            <ta e="T1023" id="Seg_7773" s="T1022">ka-la-m</ta>
            <ta e="T1024" id="Seg_7774" s="T1023">no</ta>
            <ta e="T1025" id="Seg_7775" s="T1024">kan-a-ʔ</ta>
            <ta e="T1026" id="Seg_7776" s="T1025">tože</ta>
            <ta e="T1027" id="Seg_7777" s="T1026">dăre</ta>
            <ta e="T1028" id="Seg_7778" s="T1027">že</ta>
            <ta e="T1029" id="Seg_7779" s="T1028">eʔbdə-bə</ta>
            <ta e="T1030" id="Seg_7780" s="T1029">băt-luʔ-pi</ta>
            <ta e="T1031" id="Seg_7781" s="T1030">piʔmə-ʔi</ta>
            <ta e="T1032" id="Seg_7782" s="T1031">šer-bi</ta>
            <ta e="T1033" id="Seg_7783" s="T1032">kujnek</ta>
            <ta e="T1034" id="Seg_7784" s="T1033">šer-bi</ta>
            <ta e="T1035" id="Seg_7785" s="T1034">i</ta>
            <ta e="T1036" id="Seg_7786" s="T1035">üžü</ta>
            <ta e="T1037" id="Seg_7787" s="T1036">šer-bi</ta>
            <ta e="T1038" id="Seg_7788" s="T1037">kam-bi</ta>
            <ta e="T1039" id="Seg_7789" s="T1038">kandə-ga</ta>
            <ta e="T1040" id="Seg_7790" s="T1039">kandə-ga</ta>
            <ta e="T1041" id="Seg_7791" s="T1040">urgaːba</ta>
            <ta e="T1042" id="Seg_7792" s="T1041">šonə-ga</ta>
            <ta e="T1043" id="Seg_7793" s="T1042">dĭ</ta>
            <ta e="T1044" id="Seg_7794" s="T1043">strela-zi</ta>
            <ta e="T1045" id="Seg_7795" s="T1044">bar</ta>
            <ta e="T1046" id="Seg_7796" s="T1045">dĭ-m</ta>
            <ta e="T1048" id="Seg_7797" s="T1047">sima-ndə</ta>
            <ta e="T1049" id="Seg_7798" s="T1048">sima-t</ta>
            <ta e="T1051" id="Seg_7799" s="T1049">bar</ta>
            <ta e="T1052" id="Seg_7800" s="T1051">dĭ</ta>
            <ta e="T1053" id="Seg_7801" s="T1052">strela-zi</ta>
            <ta e="T1054" id="Seg_7802" s="T1053">sima-ndə</ta>
            <ta e="T1055" id="Seg_7803" s="T1054">toʔ-nar-bi</ta>
            <ta e="T1056" id="Seg_7804" s="T1055">i</ta>
            <ta e="T1057" id="Seg_7805" s="T1056">sima-t</ta>
            <ta e="T1059" id="Seg_7806" s="T1058">nago-luʔ-pi</ta>
            <ta e="T1060" id="Seg_7807" s="T1059">dĭgəttə</ta>
            <ta e="T1061" id="Seg_7808" s="T1060">kam-bi</ta>
            <ta e="T1062" id="Seg_7809" s="T1061">kam-bi</ta>
            <ta e="T1063" id="Seg_7810" s="T1062">šonə-ga</ta>
            <ta e="T1064" id="Seg_7811" s="T1063">nüke</ta>
            <ta e="T1065" id="Seg_7812" s="T1064">amno-laʔbə</ta>
            <ta e="T1066" id="Seg_7813" s="T1065">i</ta>
            <ta e="T1067" id="Seg_7814" s="T1066">nüke-n</ta>
            <ta e="T1068" id="Seg_7815" s="T1067">nʼi-t</ta>
            <ta e="T1069" id="Seg_7816" s="T1068">amno-laʔbə</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PKZ">
            <ta e="T1" id="Seg_7817" s="T0">miʔ</ta>
            <ta e="T2" id="Seg_7818" s="T1">kamən</ta>
            <ta e="T3" id="Seg_7819" s="T2">šo-bi-bAʔ</ta>
            <ta e="T4" id="Seg_7820" s="T3">Abalakovo-Tə</ta>
            <ta e="T5" id="Seg_7821" s="T4">miʔnʼibeʔ</ta>
            <ta e="T6" id="Seg_7822" s="T5">jakšə</ta>
            <ta e="T7" id="Seg_7823" s="T6">i-bi</ta>
            <ta e="T8" id="Seg_7824" s="T7">il</ta>
            <ta e="T9" id="Seg_7825" s="T8">šaː-bi-bAʔ</ta>
            <ta e="T10" id="Seg_7826" s="T9">jakšə</ta>
            <ta e="T11" id="Seg_7827" s="T10">dĭgəttə</ta>
            <ta e="T12" id="Seg_7828" s="T11">mĭn-bi-bAʔ</ta>
            <ta e="T13" id="Seg_7829" s="T12">mĭn-bi-bAʔ</ta>
            <ta e="T16" id="Seg_7830" s="T15">girgit-jəʔ</ta>
            <ta e="T17" id="Seg_7831" s="T16">büzʼe-jəʔ</ta>
            <ta e="T18" id="Seg_7832" s="T17">ku-bi-bAʔ</ta>
            <ta e="T19" id="Seg_7833" s="T18">tʼăbaktər-bi-bAʔ</ta>
            <ta e="T20" id="Seg_7834" s="T19">dĭ-zAŋ</ta>
            <ta e="T21" id="Seg_7835" s="T20">ĭmbi=də</ta>
            <ta e="T22" id="Seg_7836" s="T21">ej</ta>
            <ta e="T23" id="Seg_7837" s="T22">tĭmne-jəʔ</ta>
            <ta e="T24" id="Seg_7838" s="T23">nu-zAŋ</ta>
            <ta e="T25" id="Seg_7839" s="T24">nu-zAŋ</ta>
            <ta e="T26" id="Seg_7840" s="T25">il</ta>
            <ta e="T27" id="Seg_7841" s="T26">kădaʔ</ta>
            <ta e="T28" id="Seg_7842" s="T27">dĭ-zAŋ</ta>
            <ta e="T29" id="Seg_7843" s="T28">amno-laʔbə-jəʔ</ta>
            <ta e="T30" id="Seg_7844" s="T29">miʔ</ta>
            <ta e="T31" id="Seg_7845" s="T30">tene-luʔbdə-bi-bAʔ</ta>
            <ta e="T32" id="Seg_7846" s="T31">bazoʔ</ta>
            <ta e="T33" id="Seg_7847" s="T32">par-zittə</ta>
            <ta e="T34" id="Seg_7848" s="T33">maʔ-gəndə</ta>
            <ta e="T37" id="Seg_7849" s="T36">onʼiʔ</ta>
            <ta e="T38" id="Seg_7850" s="T37">koʔbdo</ta>
            <ta e="T39" id="Seg_7851" s="T38">Jelʼa</ta>
            <ta e="T40" id="Seg_7852" s="T39">kan-bi</ta>
            <ta e="T41" id="Seg_7853" s="T40">süt</ta>
            <ta e="T42" id="Seg_7854" s="T41">măndo-r-ntə-zittə</ta>
            <ta e="T43" id="Seg_7855" s="T42">i</ta>
            <ta e="T45" id="Seg_7856" s="T44">šo-bi</ta>
            <ta e="T46" id="Seg_7857" s="T45">nüke-Tə</ta>
            <ta e="T47" id="Seg_7858" s="T46">onʼiʔ</ta>
            <ta e="T48" id="Seg_7859" s="T47">nüke-Tə</ta>
            <ta e="T49" id="Seg_7860" s="T48">dĭ</ta>
            <ta e="T50" id="Seg_7861" s="T49">nu</ta>
            <ta e="T51" id="Seg_7862" s="T50">i-gA</ta>
            <ta e="T52" id="Seg_7863" s="T51">dĭ</ta>
            <ta e="T53" id="Seg_7864" s="T52">nüke</ta>
            <ta e="T54" id="Seg_7865" s="T53">dĭ</ta>
            <ta e="T55" id="Seg_7866" s="T54">măn-ntə</ta>
            <ta e="T57" id="Seg_7867" s="T56">kadul</ta>
            <ta e="T64" id="Seg_7868" s="T63">eʔbdə</ta>
            <ta e="T65" id="Seg_7869" s="T64">dĭ</ta>
            <ta e="T66" id="Seg_7870" s="T65">bar</ta>
            <ta e="T67" id="Seg_7871" s="T66">par-luʔbdə-bi</ta>
            <ta e="T68" id="Seg_7872" s="T67">bar</ta>
            <ta e="T69" id="Seg_7873" s="T68">sĭj-də</ta>
            <ta e="T70" id="Seg_7874" s="T69">ugaːndə</ta>
            <ta e="T71" id="Seg_7875" s="T70">jakšə</ta>
            <ta e="T74" id="Seg_7876" s="T73">măn-bi</ta>
            <ta e="T75" id="Seg_7877" s="T74">dön</ta>
            <ta e="T76" id="Seg_7878" s="T75">nüke</ta>
            <ta e="T77" id="Seg_7879" s="T76">i-gA</ta>
            <ta e="T78" id="Seg_7880" s="T77">nu-də-lV</ta>
            <ta e="T79" id="Seg_7881" s="T78">mo-liA</ta>
            <ta e="T80" id="Seg_7882" s="T79">tʼăbaktər-zittə</ta>
            <ta e="T82" id="Seg_7883" s="T81">dĭ</ta>
            <ta e="T83" id="Seg_7884" s="T82">măna</ta>
            <ta e="T84" id="Seg_7885" s="T83">nörbə-bi</ta>
            <ta e="T86" id="Seg_7886" s="T85">koʔbdo</ta>
            <ta e="T87" id="Seg_7887" s="T86">i-gA</ta>
            <ta e="T88" id="Seg_7888" s="T87">a</ta>
            <ta e="T90" id="Seg_7889" s="T89">nʼi</ta>
            <ta e="T92" id="Seg_7890" s="T90">dĭgəttə</ta>
            <ta e="T94" id="Seg_7891" s="T93">dĭgəttə</ta>
            <ta e="T95" id="Seg_7892" s="T94">miʔ</ta>
            <ta e="T96" id="Seg_7893" s="T95">ma-luʔbdə-bi-bAʔ</ta>
            <ta e="T97" id="Seg_7894" s="T96">dĭ</ta>
            <ta e="T98" id="Seg_7895" s="T97">nüke-Tə</ta>
            <ta e="T100" id="Seg_7896" s="T99">mĭn-bi-bAʔ</ta>
            <ta e="T101" id="Seg_7897" s="T100">tʼăbaktər-zittə</ta>
            <ta e="T102" id="Seg_7898" s="T101">nagur</ta>
            <ta e="T103" id="Seg_7899" s="T102">biəʔ</ta>
            <ta e="T104" id="Seg_7900" s="T103">tʼala</ta>
            <ta e="T105" id="Seg_7901" s="T104">tʼăbaktər-bi-bAʔ</ta>
            <ta e="T106" id="Seg_7902" s="T105">bar</ta>
            <ta e="T107" id="Seg_7903" s="T106">ĭmbi</ta>
            <ta e="T108" id="Seg_7904" s="T107">nörbə-bi</ta>
            <ta e="T109" id="Seg_7905" s="T108">bos-də</ta>
            <ta e="T111" id="Seg_7906" s="T110">šĭkə-t-ziʔ</ta>
            <ta e="T112" id="Seg_7907" s="T111">dărəʔ</ta>
            <ta e="T113" id="Seg_7908" s="T112">măn-bi</ta>
            <ta e="T114" id="Seg_7909" s="T113">măn</ta>
            <ta e="T115" id="Seg_7910" s="T114">tʼotka-m</ta>
            <ta e="T116" id="Seg_7911" s="T115">kü-laːm-bi</ta>
            <ta e="T117" id="Seg_7912" s="T116">šide</ta>
            <ta e="T118" id="Seg_7913" s="T117">biəʔ</ta>
            <ta e="T119" id="Seg_7914" s="T118">pʼe</ta>
            <ta e="T120" id="Seg_7915" s="T119">kan-bi</ta>
            <ta e="T121" id="Seg_7916" s="T120">šində-ziʔ-də</ta>
            <ta e="T122" id="Seg_7917" s="T121">ej</ta>
            <ta e="T123" id="Seg_7918" s="T122">tʼăbaktər-liA-m</ta>
            <ta e="T125" id="Seg_7919" s="T124">dĭ-Tə</ta>
            <ta e="T126" id="Seg_7920" s="T125">tüj</ta>
            <ta e="T128" id="Seg_7921" s="T127">sejʔpü</ta>
            <ta e="T129" id="Seg_7922" s="T128">pʼe</ta>
            <ta e="T130" id="Seg_7923" s="T129">dĭ</ta>
            <ta e="T131" id="Seg_7924" s="T130">ugaːndə</ta>
            <ta e="T132" id="Seg_7925" s="T131">jakšə</ta>
            <ta e="T133" id="Seg_7926" s="T132">nüke</ta>
            <ta e="T134" id="Seg_7927" s="T133">bar</ta>
            <ta e="T135" id="Seg_7928" s="T134">ĭmbi-t</ta>
            <ta e="T136" id="Seg_7929" s="T135">i-gA</ta>
            <ta e="T137" id="Seg_7930" s="T136">am-zittə</ta>
            <ta e="T138" id="Seg_7931" s="T137">mĭ-liA</ta>
            <ta e="T139" id="Seg_7932" s="T138">a</ta>
            <ta e="T140" id="Seg_7933" s="T139">ej</ta>
            <ta e="T141" id="Seg_7934" s="T140">am-lV-l</ta>
            <ta e="T142" id="Seg_7935" s="T141">tak</ta>
            <ta e="T143" id="Seg_7936" s="T142">dĭ</ta>
            <ta e="T144" id="Seg_7937" s="T143">bar</ta>
            <ta e="T145" id="Seg_7938" s="T144">kurol-laʔbə</ta>
            <ta e="T148" id="Seg_7939" s="T147">dĭgəttə</ta>
            <ta e="T149" id="Seg_7940" s="T148">dĭ</ta>
            <ta e="T150" id="Seg_7941" s="T149">üge</ta>
            <ta e="T151" id="Seg_7942" s="T150">maʔ-gəndə</ta>
            <ta e="T152" id="Seg_7943" s="T151">iʔgö</ta>
            <ta e="T153" id="Seg_7944" s="T152">togonər-liA</ta>
            <ta e="T154" id="Seg_7945" s="T153">ej</ta>
            <ta e="T155" id="Seg_7946" s="T154">sedem</ta>
            <ta e="T156" id="Seg_7947" s="T155">üjü-gəndə</ta>
            <ta e="T1074" id="Seg_7948" s="T157">Kazan</ta>
            <ta e="T158" id="Seg_7949" s="T1074">tura-Tə</ta>
            <ta e="T159" id="Seg_7950" s="T158">biəʔ</ta>
            <ta e="T161" id="Seg_7951" s="T160">biəʔ</ta>
            <ta e="T162" id="Seg_7952" s="T161">šide</ta>
            <ta e="T163" id="Seg_7953" s="T162">sumna</ta>
            <ta e="T164" id="Seg_7954" s="T163">dĭ</ta>
            <ta e="T165" id="Seg_7955" s="T164">bar</ta>
            <ta e="T167" id="Seg_7956" s="T166">kan-liA</ta>
            <ta e="T168" id="Seg_7957" s="T167">üjü-ziʔ</ta>
            <ta e="T169" id="Seg_7958" s="T168">a</ta>
            <ta e="T170" id="Seg_7959" s="T169">mašina-jəʔ</ta>
            <ta e="T171" id="Seg_7960" s="T170">mĭn-liA-jəʔ</ta>
            <ta e="T172" id="Seg_7961" s="T171">dĭ</ta>
            <ta e="T173" id="Seg_7962" s="T172">dĭbər</ta>
            <ta e="T174" id="Seg_7963" s="T173">ej</ta>
            <ta e="T175" id="Seg_7964" s="T174">am-liA</ta>
            <ta e="T176" id="Seg_7965" s="T175">a</ta>
            <ta e="T177" id="Seg_7966" s="T176">üdʼi-n</ta>
            <ta e="T180" id="Seg_7967" s="T179">kan-lV-j</ta>
            <ta e="T183" id="Seg_7968" s="T182">măn</ta>
            <ta e="T184" id="Seg_7969" s="T183">üjü-ziʔ</ta>
            <ta e="T185" id="Seg_7970" s="T184">lutʼšə</ta>
            <ta e="T186" id="Seg_7971" s="T185">kan-lV-m</ta>
            <ta e="T187" id="Seg_7972" s="T186">aʔtʼi</ta>
            <ta e="T189" id="Seg_7973" s="T188">a</ta>
            <ta e="T190" id="Seg_7974" s="T189">mašina-jəʔ-ziʔ</ta>
            <ta e="T191" id="Seg_7975" s="T190">ugaːndə</ta>
            <ta e="T192" id="Seg_7976" s="T191">kuŋgə-ŋ</ta>
            <ta e="T193" id="Seg_7977" s="T192">kan-zittə</ta>
            <ta e="T194" id="Seg_7978" s="T193">da</ta>
            <ta e="T195" id="Seg_7979" s="T194">bar</ta>
            <ta e="T196" id="Seg_7980" s="T195">sădər-laʔbə</ta>
            <ta e="T198" id="Seg_7981" s="T197">girgit</ta>
            <ta e="T200" id="Seg_7982" s="T199">nu-zAŋ-Kən</ta>
            <ta e="T201" id="Seg_7983" s="T200">šində</ta>
            <ta e="T202" id="Seg_7984" s="T201">ĭššo</ta>
            <ta e="T203" id="Seg_7985" s="T202">mo-liA</ta>
            <ta e="T204" id="Seg_7986" s="T203">tʼăbaktər-zittə</ta>
            <ta e="T205" id="Seg_7987" s="T204">aľi</ta>
            <ta e="T206" id="Seg_7988" s="T205">ej</ta>
            <ta e="T208" id="Seg_7989" s="T207">nʼiʔnen</ta>
            <ta e="T209" id="Seg_7990" s="T208">Abalakovo-n</ta>
            <ta e="T210" id="Seg_7991" s="T209">ešši-zAŋ-Tə</ta>
            <ta e="T211" id="Seg_7992" s="T210">možna</ta>
            <ta e="T212" id="Seg_7993" s="T211">ku-zittə</ta>
            <ta e="T213" id="Seg_7994" s="T212">sĭri</ta>
            <ta e="T214" id="Seg_7995" s="T213">ulu-t</ta>
            <ta e="T215" id="Seg_7996" s="T214">sima-t</ta>
            <ta e="T216" id="Seg_7997" s="T215">sagər</ta>
            <ta e="T219" id="Seg_7998" s="T218">kazak</ta>
            <ta e="T220" id="Seg_7999" s="T219">kem</ta>
            <ta e="T221" id="Seg_8000" s="T220">i-gA</ta>
            <ta e="T222" id="Seg_8001" s="T221">i</ta>
            <ta e="T223" id="Seg_8002" s="T222">kem</ta>
            <ta e="T224" id="Seg_8003" s="T223">i-gA</ta>
            <ta e="T225" id="Seg_8004" s="T224">nu-zAŋ</ta>
            <ta e="T227" id="Seg_8005" s="T226">girgit</ta>
            <ta e="T228" id="Seg_8006" s="T227">bar</ta>
            <ta e="T229" id="Seg_8007" s="T228">özer-laʔbə-jəʔ</ta>
            <ta e="T230" id="Seg_8008" s="T229">li</ta>
            <ta e="T231" id="Seg_8009" s="T230">kan-bi-jəʔ</ta>
            <ta e="T232" id="Seg_8010" s="T231">togonər-zittə</ta>
            <ta e="T233" id="Seg_8011" s="T232">dĭbər_döbər</ta>
            <ta e="T234" id="Seg_8012" s="T233">vot</ta>
            <ta e="T239" id="Seg_8013" s="T238">a</ta>
            <ta e="T240" id="Seg_8014" s="T239">bos-də</ta>
            <ta e="T241" id="Seg_8015" s="T240">šĭkə-t</ta>
            <ta e="T242" id="Seg_8016" s="T241">ej</ta>
            <ta e="T243" id="Seg_8017" s="T242">tĭmne-t</ta>
            <ta e="T245" id="Seg_8018" s="T244">tolʼko</ta>
            <ta e="T246" id="Seg_8019" s="T245">dĭ</ta>
            <ta e="T247" id="Seg_8020" s="T246">nu-zAŋ</ta>
            <ta e="T248" id="Seg_8021" s="T247">šĭkə-t</ta>
            <ta e="T249" id="Seg_8022" s="T248">onʼiʔ</ta>
            <ta e="T250" id="Seg_8023" s="T249">nüke</ta>
            <ta e="T251" id="Seg_8024" s="T250">tĭmne-t</ta>
            <ta e="T252" id="Seg_8025" s="T251">a</ta>
            <ta e="T253" id="Seg_8026" s="T252">daška</ta>
            <ta e="T254" id="Seg_8027" s="T253">šində=də</ta>
            <ta e="T255" id="Seg_8028" s="T254">ej</ta>
            <ta e="T256" id="Seg_8029" s="T255">tĭmne</ta>
            <ta e="T257" id="Seg_8030" s="T256">dĭ</ta>
            <ta e="T258" id="Seg_8031" s="T257">šĭkə-m</ta>
            <ta e="T260" id="Seg_8032" s="T259">tăn</ta>
            <ta e="T261" id="Seg_8033" s="T260">măna</ta>
            <ta e="T262" id="Seg_8034" s="T261">šʼaːm-bi-l</ta>
            <ta e="T263" id="Seg_8035" s="T262">tăn</ta>
            <ta e="T264" id="Seg_8036" s="T263">măna</ta>
            <ta e="T265" id="Seg_8037" s="T264">mʼeg-luʔbdə-bi-l</ta>
            <ta e="T266" id="Seg_8038" s="T265">dĭgəttə</ta>
            <ta e="T267" id="Seg_8039" s="T266">šaʔ-laːm-bi-m</ta>
            <ta e="T268" id="Seg_8040" s="T267">a</ta>
            <ta e="T269" id="Seg_8041" s="T268">măn</ta>
            <ta e="T270" id="Seg_8042" s="T269">tănan</ta>
            <ta e="T271" id="Seg_8043" s="T270">măndo-r-bi-m</ta>
            <ta e="T272" id="Seg_8044" s="T271">măndo-r-bi-m</ta>
            <ta e="T273" id="Seg_8045" s="T272">ej</ta>
            <ta e="T275" id="Seg_8046" s="T274">mo-liA-m</ta>
            <ta e="T276" id="Seg_8047" s="T275">măndo-r-zittə</ta>
            <ta e="T277" id="Seg_8048" s="T276">ej</ta>
            <ta e="T278" id="Seg_8049" s="T277">jakšə</ta>
            <ta e="T279" id="Seg_8050" s="T278">kuza</ta>
            <ta e="T280" id="Seg_8051" s="T279">amno-bi</ta>
            <ta e="T281" id="Seg_8052" s="T280">dĭ-m</ta>
            <ta e="T282" id="Seg_8053" s="T281">numəj-laʔbə-bi-jəʔ</ta>
            <ta e="T283" id="Seg_8054" s="T282">tardʼžabərdʼža</ta>
            <ta e="T284" id="Seg_8055" s="T283">dĭ</ta>
            <ta e="T285" id="Seg_8056" s="T284">bar</ta>
            <ta e="T286" id="Seg_8057" s="T285">sejmü</ta>
            <ta e="T287" id="Seg_8058" s="T286">dʼabə-bi</ta>
            <ta e="T288" id="Seg_8059" s="T287">üjü-t</ta>
            <ta e="T289" id="Seg_8060" s="T288">ĭzem-liA</ta>
            <ta e="T290" id="Seg_8061" s="T289">kan-zittə</ta>
            <ta e="T291" id="Seg_8062" s="T290">ej</ta>
            <ta e="T292" id="Seg_8063" s="T291">mo-liA</ta>
            <ta e="T293" id="Seg_8064" s="T292">dĭgəttə</ta>
            <ta e="T294" id="Seg_8065" s="T293">kan-bi</ta>
            <ta e="T295" id="Seg_8066" s="T294">kan-bi</ta>
            <ta e="T296" id="Seg_8067" s="T295">urgaːba</ta>
            <ta e="T297" id="Seg_8068" s="T296">šonə-gA</ta>
            <ta e="T298" id="Seg_8069" s="T297">i-ʔ</ta>
            <ta e="T299" id="Seg_8070" s="T298">măna</ta>
            <ta e="T300" id="Seg_8071" s="T299">bostə-ziʔ</ta>
            <ta e="T301" id="Seg_8072" s="T300">no</ta>
            <ta e="T302" id="Seg_8073" s="T301">tolʼko</ta>
            <ta e="T303" id="Seg_8074" s="T302">sagər</ta>
            <ta e="T305" id="Seg_8075" s="T304">băra-Tə</ta>
            <ta e="T306" id="Seg_8076" s="T305">amdəl-t</ta>
            <ta e="T307" id="Seg_8077" s="T306">măna</ta>
            <ta e="T308" id="Seg_8078" s="T307">dĭ</ta>
            <ta e="T309" id="Seg_8079" s="T308">dĭ-m</ta>
            <ta e="T310" id="Seg_8080" s="T309">amnol-bi</ta>
            <ta e="T311" id="Seg_8081" s="T310">bazoʔ</ta>
            <ta e="T312" id="Seg_8082" s="T311">kandə-gA</ta>
            <ta e="T313" id="Seg_8083" s="T312">kandə-gA</ta>
            <ta e="T314" id="Seg_8084" s="T313">kandə-gA</ta>
            <ta e="T316" id="Seg_8085" s="T314">dĭgəttə</ta>
            <ta e="T317" id="Seg_8086" s="T316">dĭgəttə</ta>
            <ta e="T318" id="Seg_8087" s="T317">urgo</ta>
            <ta e="T319" id="Seg_8088" s="T318">men</ta>
            <ta e="T320" id="Seg_8089" s="T319">dʼije-gəʔ</ta>
            <ta e="T321" id="Seg_8090" s="T320">šo-bi</ta>
            <ta e="T322" id="Seg_8091" s="T321">i-ʔ</ta>
            <ta e="T323" id="Seg_8092" s="T322">măna</ta>
            <ta e="T324" id="Seg_8093" s="T323">tolʼko</ta>
            <ta e="T325" id="Seg_8094" s="T324">sagər</ta>
            <ta e="T326" id="Seg_8095" s="T325">măna</ta>
            <ta e="T327" id="Seg_8096" s="T326">băra-Tə</ta>
            <ta e="T328" id="Seg_8097" s="T327">amdəl-t</ta>
            <ta e="T329" id="Seg_8098" s="T328">dĭ</ta>
            <ta e="T330" id="Seg_8099" s="T329">amnol-də-bi</ta>
            <ta e="T331" id="Seg_8100" s="T330">kan-bi</ta>
            <ta e="T332" id="Seg_8101" s="T331">bazoʔ</ta>
            <ta e="T333" id="Seg_8102" s="T332">šonə-gA</ta>
            <ta e="T334" id="Seg_8103" s="T333">šonə-gA</ta>
            <ta e="T338" id="Seg_8104" s="T337">lʼisa</ta>
            <ta e="T339" id="Seg_8105" s="T338">šo-laʔbə</ta>
            <ta e="T340" id="Seg_8106" s="T339">i-ʔ</ta>
            <ta e="T341" id="Seg_8107" s="T340">măna</ta>
            <ta e="T342" id="Seg_8108" s="T341">dĭ</ta>
            <ta e="T343" id="Seg_8109" s="T342">dĭ-m</ta>
            <ta e="T344" id="Seg_8110" s="T343">i-bi</ta>
            <ta e="T345" id="Seg_8111" s="T344">sagər</ta>
            <ta e="T346" id="Seg_8112" s="T345">băra-Tə</ta>
            <ta e="T347" id="Seg_8113" s="T346">amnol-bi</ta>
            <ta e="T348" id="Seg_8114" s="T347">dĭgəttə</ta>
            <ta e="T349" id="Seg_8115" s="T348">bazoʔ</ta>
            <ta e="T350" id="Seg_8116" s="T349">kandə-gA</ta>
            <ta e="T351" id="Seg_8117" s="T350">bü</ta>
            <ta e="T352" id="Seg_8118" s="T351">šonə-gA</ta>
            <ta e="T353" id="Seg_8119" s="T352">i-ʔ</ta>
            <ta e="T354" id="Seg_8120" s="T353">măna</ta>
            <ta e="T355" id="Seg_8121" s="T354">dĭgəttə</ta>
            <ta e="T356" id="Seg_8122" s="T355">dĭ</ta>
            <ta e="T357" id="Seg_8123" s="T356">bü</ta>
            <ta e="T358" id="Seg_8124" s="T357">kămnə-bi</ta>
            <ta e="T359" id="Seg_8125" s="T358">dĭbər</ta>
            <ta e="T360" id="Seg_8126" s="T359">šo-bi</ta>
            <ta e="T361" id="Seg_8127" s="T360">dĭn</ta>
            <ta e="T362" id="Seg_8128" s="T361">bar</ta>
            <ta e="T363" id="Seg_8129" s="T362">tura-jəʔ</ta>
            <ta e="T364" id="Seg_8130" s="T363">nu-gA-jəʔ</ta>
            <ta e="T365" id="Seg_8131" s="T364">i</ta>
            <ta e="T366" id="Seg_8132" s="T365">bü</ta>
            <ta e="T367" id="Seg_8133" s="T366">ugaːndə</ta>
            <ta e="T368" id="Seg_8134" s="T367">urgo</ta>
            <ta e="T369" id="Seg_8135" s="T368">dĭ</ta>
            <ta e="T370" id="Seg_8136" s="T369">sejmü</ta>
            <ta e="T371" id="Seg_8137" s="T370">sar-bi</ta>
            <ta e="T372" id="Seg_8138" s="T371">šü</ta>
            <ta e="T373" id="Seg_8139" s="T372">hen-bi</ta>
            <ta e="T374" id="Seg_8140" s="T373">amno-laʔbə</ta>
            <ta e="T375" id="Seg_8141" s="T374">dĭ-m</ta>
            <ta e="T376" id="Seg_8142" s="T375">ku-luʔbdə-bi</ta>
            <ta e="T377" id="Seg_8143" s="T376">koŋ</ta>
            <ta e="T378" id="Seg_8144" s="T377">šində</ta>
            <ta e="T379" id="Seg_8145" s="T378">dĭn</ta>
            <ta e="T380" id="Seg_8146" s="T379">amno-laʔbə</ta>
            <ta e="T381" id="Seg_8147" s="T380">măn</ta>
            <ta e="T383" id="Seg_8148" s="T382">noʔ-Tə</ta>
            <ta e="T384" id="Seg_8149" s="T383">tonə-laʔbə</ta>
            <ta e="T385" id="Seg_8150" s="T384">sejmü-t</ta>
            <ta e="T386" id="Seg_8151" s="T385">am-laʔbə</ta>
            <ta e="T387" id="Seg_8152" s="T386">dĭn</ta>
            <ta e="T388" id="Seg_8153" s="T387">üdʼüge-n</ta>
            <ta e="T389" id="Seg_8154" s="T388">amno-lV-j</ta>
            <ta e="T390" id="Seg_8155" s="T389">dĭgəttə</ta>
            <ta e="T391" id="Seg_8156" s="T390">bazoʔ</ta>
            <ta e="T392" id="Seg_8157" s="T391">bar</ta>
            <ta e="T393" id="Seg_8158" s="T392">tonə-lV-j</ta>
            <ta e="T394" id="Seg_8159" s="T393">kan-ə-ʔ</ta>
            <ta e="T395" id="Seg_8160" s="T394">nʼi-bə</ta>
            <ta e="T396" id="Seg_8161" s="T395">öʔlu-bi</ta>
            <ta e="T397" id="Seg_8162" s="T396">ĭmbi</ta>
            <ta e="T398" id="Seg_8163" s="T397">dĭ-Tə</ta>
            <ta e="T399" id="Seg_8164" s="T398">kereʔ</ta>
            <ta e="T400" id="Seg_8165" s="T399">dĭ</ta>
            <ta e="T401" id="Seg_8166" s="T400">šo-bi</ta>
            <ta e="T403" id="Seg_8167" s="T402">surar-laʔbə</ta>
            <ta e="T404" id="Seg_8168" s="T403">ĭmbi</ta>
            <ta e="T405" id="Seg_8169" s="T404">tănan</ta>
            <ta e="T406" id="Seg_8170" s="T405">kereʔ</ta>
            <ta e="T407" id="Seg_8171" s="T406">dĭ-n</ta>
            <ta e="T408" id="Seg_8172" s="T407">koŋ-də</ta>
            <ta e="T409" id="Seg_8173" s="T408">koʔbdo-bə</ta>
            <ta e="T410" id="Seg_8174" s="T409">i-lV-m</ta>
            <ta e="T412" id="Seg_8175" s="T411">dĭ</ta>
            <ta e="T413" id="Seg_8176" s="T412">nʼi</ta>
            <ta e="T414" id="Seg_8177" s="T413">par-luʔbdə-bi</ta>
            <ta e="T415" id="Seg_8178" s="T414">măn-ntə</ta>
            <ta e="T416" id="Seg_8179" s="T415">dĭ</ta>
            <ta e="T417" id="Seg_8180" s="T416">măn-ntə</ta>
            <ta e="T418" id="Seg_8181" s="T417">pušaj</ta>
            <ta e="T419" id="Seg_8182" s="T418">dĭ-m</ta>
            <ta e="T420" id="Seg_8183" s="T419">koŋ-də</ta>
            <ta e="T421" id="Seg_8184" s="T420">koʔbdo-bə</ta>
            <ta e="T422" id="Seg_8185" s="T421">i-lV-m</ta>
            <ta e="T423" id="Seg_8186" s="T422">pušaj</ta>
            <ta e="T424" id="Seg_8187" s="T423">kan-KV-jəʔ</ta>
            <ta e="T425" id="Seg_8188" s="T424">ato</ta>
            <ta e="T426" id="Seg_8189" s="T425">ej</ta>
            <ta e="T427" id="Seg_8190" s="T426">jakšə</ta>
            <ta e="T428" id="Seg_8191" s="T427">mo-lV-j</ta>
            <ta e="T430" id="Seg_8192" s="T429">ej</ta>
            <ta e="T431" id="Seg_8193" s="T430">mĭ-lV-m</ta>
            <ta e="T432" id="Seg_8194" s="T431">dĭ</ta>
            <ta e="T433" id="Seg_8195" s="T432">šo-bi</ta>
            <ta e="T435" id="Seg_8196" s="T434">măn-bi</ta>
            <ta e="T436" id="Seg_8197" s="T435">dărəʔ</ta>
            <ta e="T437" id="Seg_8198" s="T436">koŋ</ta>
            <ta e="T438" id="Seg_8199" s="T437">ej</ta>
            <ta e="T439" id="Seg_8200" s="T438">mĭ-liA-t</ta>
            <ta e="T440" id="Seg_8201" s="T439">koʔbdo-bə</ta>
            <ta e="T441" id="Seg_8202" s="T440">ej</ta>
            <ta e="T442" id="Seg_8203" s="T441">mĭ-lV-j</ta>
            <ta e="T443" id="Seg_8204" s="T442">jakšə-ŋ</ta>
            <ta e="T444" id="Seg_8205" s="T443">tak</ta>
            <ta e="T445" id="Seg_8206" s="T444">ej</ta>
            <ta e="T446" id="Seg_8207" s="T445">jakšə</ta>
            <ta e="T447" id="Seg_8208" s="T446">mĭ-lV-j</ta>
            <ta e="T449" id="Seg_8209" s="T448">öʔ-lAʔ</ta>
            <ta e="T451" id="Seg_8210" s="T450">ine-jəʔ</ta>
            <ta e="T452" id="Seg_8211" s="T451">dĭ-zAŋ</ta>
            <ta e="T453" id="Seg_8212" s="T452">bar</ta>
            <ta e="T454" id="Seg_8213" s="T453">dĭn</ta>
            <ta e="T455" id="Seg_8214" s="T454">sejmü-gəndə</ta>
            <ta e="T456" id="Seg_8215" s="T455">săj-nʼeʔbdə-luʔbdə-lV-jəʔ</ta>
            <ta e="T457" id="Seg_8216" s="T456">dĭgəttə</ta>
            <ta e="T1072" id="Seg_8217" s="T457">kan-lAʔ</ta>
            <ta e="T458" id="Seg_8218" s="T1072">tʼür-lV-j</ta>
            <ta e="T459" id="Seg_8219" s="T458">dĭgəttə</ta>
            <ta e="T460" id="Seg_8220" s="T459">dĭ</ta>
            <ta e="T461" id="Seg_8221" s="T460">urgaːba</ta>
            <ta e="T462" id="Seg_8222" s="T461">öʔlu-bi</ta>
            <ta e="T463" id="Seg_8223" s="T462">sagər</ta>
            <ta e="T464" id="Seg_8224" s="T463">băra-gəʔ</ta>
            <ta e="T465" id="Seg_8225" s="T464">dĭ</ta>
            <ta e="T466" id="Seg_8226" s="T465">urgaːba</ta>
            <ta e="T467" id="Seg_8227" s="T466">ine-jəʔ</ta>
            <ta e="T468" id="Seg_8228" s="T467">bar</ta>
            <ta e="T469" id="Seg_8229" s="T468">sürer-luʔbdə-bi</ta>
            <ta e="T470" id="Seg_8230" s="T469">dĭgəttə</ta>
            <ta e="T471" id="Seg_8231" s="T470">măn-ntə</ta>
            <ta e="T472" id="Seg_8232" s="T471">öʔlu-KAʔ</ta>
            <ta e="T473" id="Seg_8233" s="T472">tüžöj-jəʔ</ta>
            <ta e="T474" id="Seg_8234" s="T473">pušaj</ta>
            <ta e="T475" id="Seg_8235" s="T474">tüžöj-jəʔ</ta>
            <ta e="T476" id="Seg_8236" s="T475">amnu-t-ziʔ</ta>
            <ta e="T477" id="Seg_8237" s="T476">bar</ta>
            <ta e="T478" id="Seg_8238" s="T477">ine-bə</ta>
            <ta e="T479" id="Seg_8239" s="T478">bar</ta>
            <ta e="T480" id="Seg_8240" s="T479">kut-lV-jəʔ</ta>
            <ta e="T481" id="Seg_8241" s="T480">dĭ</ta>
            <ta e="T482" id="Seg_8242" s="T481">dĭ</ta>
            <ta e="T483" id="Seg_8243" s="T482">bar</ta>
            <ta e="T484" id="Seg_8244" s="T483">băra-gəʔ</ta>
            <ta e="T485" id="Seg_8245" s="T484">öʔlu-bi</ta>
            <ta e="T486" id="Seg_8246" s="T485">urgo</ta>
            <ta e="T487" id="Seg_8247" s="T486">men</ta>
            <ta e="T488" id="Seg_8248" s="T487">dĭ</ta>
            <ta e="T489" id="Seg_8249" s="T488">dĭ</ta>
            <ta e="T490" id="Seg_8250" s="T489">kan-bi</ta>
            <ta e="T491" id="Seg_8251" s="T490">dĭ-zAŋ</ta>
            <ta e="T492" id="Seg_8252" s="T491">bar</ta>
            <ta e="T493" id="Seg_8253" s="T492">sürer-luʔbdə-bi</ta>
            <ta e="T494" id="Seg_8254" s="T493">dĭgəttə</ta>
            <ta e="T495" id="Seg_8255" s="T494">măn-ntə</ta>
            <ta e="T497" id="Seg_8256" s="T495">öʔlu-KAʔ</ta>
            <ta e="T499" id="Seg_8257" s="T498">dĭgəttə</ta>
            <ta e="T500" id="Seg_8258" s="T499">koŋ</ta>
            <ta e="T503" id="Seg_8259" s="T502">öʔ-lAʔ</ta>
            <ta e="T504" id="Seg_8260" s="T503">men-zAŋ</ta>
            <ta e="T505" id="Seg_8261" s="T504">pušaj</ta>
            <ta e="T506" id="Seg_8262" s="T505">men-zAŋ</ta>
            <ta e="T507" id="Seg_8263" s="T506">dĭ-m</ta>
            <ta e="T508" id="Seg_8264" s="T507">săj-nüzə-lV-jəʔ</ta>
            <ta e="T509" id="Seg_8265" s="T508">dĭ</ta>
            <ta e="T510" id="Seg_8266" s="T509">öʔlu-bi</ta>
            <ta e="T511" id="Seg_8267" s="T510">men-dən</ta>
            <ta e="T512" id="Seg_8268" s="T511">a</ta>
            <ta e="T513" id="Seg_8269" s="T512">dĭ</ta>
            <ta e="T514" id="Seg_8270" s="T513">lʼisa</ta>
            <ta e="T515" id="Seg_8271" s="T514">öʔlu-bi</ta>
            <ta e="T516" id="Seg_8272" s="T515">men-zAŋ</ta>
            <ta e="T517" id="Seg_8273" s="T516">lʼisa-Kən</ta>
            <ta e="T518" id="Seg_8274" s="T517">nuʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T519" id="Seg_8275" s="T518">dĭgəttə</ta>
            <ta e="T520" id="Seg_8276" s="T519">dĭ</ta>
            <ta e="T521" id="Seg_8277" s="T520">koŋ</ta>
            <ta e="T522" id="Seg_8278" s="T521">bar</ta>
            <ta e="T523" id="Seg_8279" s="T522">il</ta>
            <ta e="T524" id="Seg_8280" s="T523">oʔbdə-bi</ta>
            <ta e="T525" id="Seg_8281" s="T524">kan-žə-bəj</ta>
            <ta e="T526" id="Seg_8282" s="T525">dĭ-m</ta>
            <ta e="T527" id="Seg_8283" s="T526">i-lV-bəj</ta>
            <ta e="T528" id="Seg_8284" s="T527">il</ta>
            <ta e="T529" id="Seg_8285" s="T528">šo-bi-jəʔ</ta>
            <ta e="T530" id="Seg_8286" s="T529">dĭ</ta>
            <ta e="T531" id="Seg_8287" s="T530">bar</ta>
            <ta e="T532" id="Seg_8288" s="T531">băra-bə</ta>
            <ta e="T533" id="Seg_8289" s="T532">bü</ta>
            <ta e="T534" id="Seg_8290" s="T533">kămnə-bi</ta>
            <ta e="T535" id="Seg_8291" s="T534">i</ta>
            <ta e="T536" id="Seg_8292" s="T535">bü</ta>
            <ta e="T537" id="Seg_8293" s="T536">il</ta>
            <ta e="T538" id="Seg_8294" s="T537">il</ta>
            <ta e="T539" id="Seg_8295" s="T538">bar</ta>
            <ta e="T540" id="Seg_8296" s="T539">nuʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T541" id="Seg_8297" s="T540">bü-gəʔ</ta>
            <ta e="T543" id="Seg_8298" s="T542">dĭgəttə</ta>
            <ta e="T544" id="Seg_8299" s="T543">dĭ</ta>
            <ta e="T545" id="Seg_8300" s="T544">koŋ</ta>
            <ta e="T546" id="Seg_8301" s="T545">kirgaːr-laʔbə</ta>
            <ta e="T547" id="Seg_8302" s="T546">pʼel-də</ta>
            <ta e="T549" id="Seg_8303" s="T548">i-gA</ta>
            <ta e="T550" id="Seg_8304" s="T549">măn-bi</ta>
            <ta e="T551" id="Seg_8305" s="T550">măn</ta>
            <ta e="T552" id="Seg_8306" s="T551">i-gA</ta>
            <ta e="T553" id="Seg_8307" s="T552">tüžöj-jəʔ</ta>
            <ta e="T554" id="Seg_8308" s="T553">ine-jəʔ</ta>
            <ta e="T555" id="Seg_8309" s="T554">men-zAŋ-də</ta>
            <ta e="T556" id="Seg_8310" s="T555">măn-bi-m</ta>
            <ta e="T557" id="Seg_8311" s="T556">aktʼa</ta>
            <ta e="T558" id="Seg_8312" s="T557">iʔgö</ta>
            <ta e="T559" id="Seg_8313" s="T558">măn-bi</ta>
            <ta e="T560" id="Seg_8314" s="T559">ĭmbi=də</ta>
            <ta e="T561" id="Seg_8315" s="T560">măna</ta>
            <ta e="T562" id="Seg_8316" s="T561">ej</ta>
            <ta e="T563" id="Seg_8317" s="T562">kereʔ</ta>
            <ta e="T564" id="Seg_8318" s="T563">tolʼko</ta>
            <ta e="T565" id="Seg_8319" s="T564">koʔbdo-m</ta>
            <ta e="T566" id="Seg_8320" s="T565">i-zittə</ta>
            <ta e="T568" id="Seg_8321" s="T567">dĭgəttə</ta>
            <ta e="T569" id="Seg_8322" s="T568">sagər</ta>
            <ta e="T570" id="Seg_8323" s="T569">băra-Tə</ta>
            <ta e="T571" id="Seg_8324" s="T570">bü</ta>
            <ta e="T572" id="Seg_8325" s="T571">i-bi</ta>
            <ta e="T574" id="Seg_8326" s="T572">i</ta>
            <ta e="T576" id="Seg_8327" s="T575">dĭgəttə</ta>
            <ta e="T577" id="Seg_8328" s="T576">mĭ-luʔbdə-bi</ta>
            <ta e="T578" id="Seg_8329" s="T577">koʔbdo-bə</ta>
            <ta e="T579" id="Seg_8330" s="T578">dĭ</ta>
            <ta e="T580" id="Seg_8331" s="T579">amnol-bi</ta>
            <ta e="T581" id="Seg_8332" s="T580">amnol-bi</ta>
            <ta e="T582" id="Seg_8333" s="T581">sejmü-Tə</ta>
            <ta e="T583" id="Seg_8334" s="T582">i</ta>
            <ta e="T584" id="Seg_8335" s="T583">kan-bi</ta>
            <ta e="T585" id="Seg_8336" s="T584">maʔ-gəndə</ta>
            <ta e="T586" id="Seg_8337" s="T585">šide</ta>
            <ta e="T587" id="Seg_8338" s="T586">kaga</ta>
            <ta e="T588" id="Seg_8339" s="T587">amno-laʔbə-bi</ta>
            <ta e="T589" id="Seg_8340" s="T588">amno-laʔbə-bi</ta>
            <ta e="T590" id="Seg_8341" s="T589">dĭ-zAŋ</ta>
            <ta e="T591" id="Seg_8342" s="T590">ne-zAŋ-də</ta>
            <ta e="T592" id="Seg_8343" s="T591">i-bi-jəʔ</ta>
            <ta e="T593" id="Seg_8344" s="T592">dĭ-zAŋ</ta>
            <ta e="T594" id="Seg_8345" s="T593">oʔbdə-bi-jəʔ</ta>
            <ta e="T595" id="Seg_8346" s="T594">kan-bi-jəʔ</ta>
            <ta e="T596" id="Seg_8347" s="T595">dʼije-Tə</ta>
            <ta e="T597" id="Seg_8348" s="T596">a</ta>
            <ta e="T598" id="Seg_8349" s="T597">ne-zAŋ</ta>
            <ta e="T599" id="Seg_8350" s="T598">maʔ-gəndə</ta>
            <ta e="T600" id="Seg_8351" s="T599">bar</ta>
            <ta e="T601" id="Seg_8352" s="T600">nüdʼi</ta>
            <ta e="T602" id="Seg_8353" s="T601">nüdʼi-n</ta>
            <ta e="T604" id="Seg_8354" s="T603">amor-zittə</ta>
            <ta e="T605" id="Seg_8355" s="T604">amnə-bi-jəʔ</ta>
            <ta e="T606" id="Seg_8356" s="T605">dĭgəttə</ta>
            <ta e="T607" id="Seg_8357" s="T606">măndo-liA-jəʔ</ta>
            <ta e="T608" id="Seg_8358" s="T607">aspaʔ-Kən</ta>
            <ta e="T610" id="Seg_8359" s="T609">girgit=də</ta>
            <ta e="T611" id="Seg_8360" s="T610">kuza</ta>
            <ta e="T612" id="Seg_8361" s="T611">amno-laʔbə</ta>
            <ta e="T613" id="Seg_8362" s="T612">dĭ-zAŋ</ta>
            <ta e="T614" id="Seg_8363" s="T613">moː-jəʔ</ta>
            <ta e="T616" id="Seg_8364" s="T615">oʔbdə-bi-jəʔ</ta>
            <ta e="T617" id="Seg_8365" s="T616">i</ta>
            <ta e="T618" id="Seg_8366" s="T617">aspaʔ-də</ta>
            <ta e="T620" id="Seg_8367" s="T619">aspaʔ-də</ta>
            <ta e="T621" id="Seg_8368" s="T620">hen-bi-jəʔ</ta>
            <ta e="T622" id="Seg_8369" s="T621">a</ta>
            <ta e="T623" id="Seg_8370" s="T622">dĭ</ta>
            <ta e="T624" id="Seg_8371" s="T623">mu</ta>
            <ta e="T626" id="Seg_8372" s="T624">bar</ta>
            <ta e="T628" id="Seg_8373" s="T627">dʼüʔpi</ta>
            <ta e="T629" id="Seg_8374" s="T628">i-bi</ta>
            <ta e="T630" id="Seg_8375" s="T629">dĭgəttə</ta>
            <ta e="T631" id="Seg_8376" s="T630">dĭ</ta>
            <ta e="T632" id="Seg_8377" s="T631">šo-bi</ta>
            <ta e="T633" id="Seg_8378" s="T632">ej</ta>
            <ta e="T634" id="Seg_8379" s="T633">mo-bi</ta>
            <ta e="T635" id="Seg_8380" s="T634">nendə-zittə</ta>
            <ta e="T636" id="Seg_8381" s="T635">dĭ</ta>
            <ta e="T637" id="Seg_8382" s="T636">kuza</ta>
            <ta e="T638" id="Seg_8383" s="T637">onʼiʔ</ta>
            <ta e="T639" id="Seg_8384" s="T638">ne-m</ta>
            <ta e="T640" id="Seg_8385" s="T639">kut-lAʔ-bi</ta>
            <ta e="T641" id="Seg_8386" s="T640">dĭgəttə</ta>
            <ta e="T642" id="Seg_8387" s="T641">šide</ta>
            <ta e="T644" id="Seg_8388" s="T643">bazoʔ</ta>
            <ta e="T645" id="Seg_8389" s="T644">ne-Tə</ta>
            <ta e="T646" id="Seg_8390" s="T645">măn-ntə</ta>
            <ta e="T647" id="Seg_8391" s="T646">ĭmbi</ta>
            <ta e="T648" id="Seg_8392" s="T647">tăn</ta>
            <ta e="T649" id="Seg_8393" s="T648">amnə-laʔbə-l</ta>
            <ta e="T650" id="Seg_8394" s="T649">nendə-ʔ</ta>
            <ta e="T651" id="Seg_8395" s="T650">dĭ</ta>
            <ta e="T652" id="Seg_8396" s="T651">bar</ta>
            <ta e="T654" id="Seg_8397" s="T653">nendə-zittə</ta>
            <ta e="T655" id="Seg_8398" s="T654">dĭ</ta>
            <ta e="T656" id="Seg_8399" s="T655">dĭ-Tə</ta>
            <ta e="T657" id="Seg_8400" s="T656">baltu-ziʔ</ta>
            <ta e="T658" id="Seg_8401" s="T657">ulu-bə</ta>
            <ta e="T659" id="Seg_8402" s="T658">hʼaʔ-bi</ta>
            <ta e="T660" id="Seg_8403" s="T659">dĭgəttə</ta>
            <ta e="T661" id="Seg_8404" s="T660">ešši-zAŋ-Tə</ta>
            <ta e="T662" id="Seg_8405" s="T661">kut-laːm-bi</ta>
            <ta e="T663" id="Seg_8406" s="T662">dĭgəttə</ta>
            <ta e="T664" id="Seg_8407" s="T663">dĭ</ta>
            <ta e="T666" id="Seg_8408" s="T665">dĭ-zAŋ-Tə</ta>
            <ta e="T667" id="Seg_8409" s="T666">maʔ-gəndə</ta>
            <ta e="T669" id="Seg_8410" s="T668">par-luʔbdə-bi</ta>
            <ta e="T670" id="Seg_8411" s="T669">dĭ-zAŋ</ta>
            <ta e="T672" id="Seg_8412" s="T671">ne-zAŋ-də</ta>
            <ta e="T673" id="Seg_8413" s="T672">naga</ta>
            <ta e="T674" id="Seg_8414" s="T673">šində=də</ta>
            <ta e="T675" id="Seg_8415" s="T674">kut-bi</ta>
            <ta e="T676" id="Seg_8416" s="T675">ešši-zem</ta>
            <ta e="T677" id="Seg_8417" s="T676">i</ta>
            <ta e="T678" id="Seg_8418" s="T677">ne-zAŋ-də</ta>
            <ta e="T680" id="Seg_8419" s="T679">amno-bi-jəʔ</ta>
            <ta e="T681" id="Seg_8420" s="T680">büzʼe</ta>
            <ta e="T682" id="Seg_8421" s="T681">nüke-t-ziʔ</ta>
            <ta e="T683" id="Seg_8422" s="T682">dĭ-zAŋ</ta>
            <ta e="T684" id="Seg_8423" s="T683">ĭmbi=də</ta>
            <ta e="T685" id="Seg_8424" s="T684">naga-bi</ta>
            <ta e="T686" id="Seg_8425" s="T685">tolʼko</ta>
            <ta e="T687" id="Seg_8426" s="T686">onʼiʔ</ta>
            <ta e="T688" id="Seg_8427" s="T687">tüžöj</ta>
            <ta e="T689" id="Seg_8428" s="T688">dĭ-zAŋ</ta>
            <ta e="T690" id="Seg_8429" s="T689">amor-zittə</ta>
            <ta e="T691" id="Seg_8430" s="T690">naga</ta>
            <ta e="T692" id="Seg_8431" s="T691">i-bi</ta>
            <ta e="T693" id="Seg_8432" s="T692">dĭgəttə</ta>
            <ta e="T694" id="Seg_8433" s="T693">tüžöj-də</ta>
            <ta e="T695" id="Seg_8434" s="T694">băt-luʔbdə-bi</ta>
            <ta e="T696" id="Seg_8435" s="T695">nüke-m</ta>
            <ta e="T697" id="Seg_8436" s="T696">sürer-luʔbdə-bi</ta>
            <ta e="T698" id="Seg_8437" s="T697">kan-ə-ʔ</ta>
            <ta e="T699" id="Seg_8438" s="T698">ato</ta>
            <ta e="T700" id="Seg_8439" s="T699">măna</ta>
            <ta e="T701" id="Seg_8440" s="T700">bos-gəndə-m</ta>
            <ta e="T702" id="Seg_8441" s="T701">amka</ta>
            <ta e="T703" id="Seg_8442" s="T702">mo-lV-j</ta>
            <ta e="T704" id="Seg_8443" s="T703">dĭ</ta>
            <ta e="T705" id="Seg_8444" s="T704">nüke</ta>
            <ta e="T706" id="Seg_8445" s="T705">kan-bi</ta>
            <ta e="T707" id="Seg_8446" s="T706">kan-bi</ta>
            <ta e="T708" id="Seg_8447" s="T707">ĭššo</ta>
            <ta e="T709" id="Seg_8448" s="T708">maʔ</ta>
            <ta e="T710" id="Seg_8449" s="T709">ku-bi</ta>
            <ta e="T711" id="Seg_8450" s="T710">maʔ-Tə</ta>
            <ta e="T712" id="Seg_8451" s="T711">šo-bi</ta>
            <ta e="T713" id="Seg_8452" s="T712">dĭn</ta>
            <ta e="T714" id="Seg_8453" s="T713">sil</ta>
            <ta e="T715" id="Seg_8454" s="T714">iʔbö-laʔbə</ta>
            <ta e="T716" id="Seg_8455" s="T715">uja</ta>
            <ta e="T717" id="Seg_8456" s="T716">iʔbö-laʔbə</ta>
            <ta e="T718" id="Seg_8457" s="T717">dĭ</ta>
            <ta e="T719" id="Seg_8458" s="T718">mĭnzər-bi</ta>
            <ta e="T720" id="Seg_8459" s="T719">bar</ta>
            <ta e="T721" id="Seg_8460" s="T720">amor-bi</ta>
            <ta e="T722" id="Seg_8461" s="T721">nünə-liA-t</ta>
            <ta e="T723" id="Seg_8462" s="T722">bar</ta>
            <ta e="T724" id="Seg_8463" s="T723">šində=də</ta>
            <ta e="T725" id="Seg_8464" s="T724">šonə-gA</ta>
            <ta e="T726" id="Seg_8465" s="T725">iʔgö</ta>
            <ta e="T727" id="Seg_8466" s="T726">ular</ta>
            <ta e="T729" id="Seg_8467" s="T728">det-bi</ta>
            <ta e="T730" id="Seg_8468" s="T729">tüžöj-jəʔ</ta>
            <ta e="T731" id="Seg_8469" s="T730">ine-jəʔ</ta>
            <ta e="T732" id="Seg_8470" s="T731">dĭgəttə</ta>
            <ta e="T733" id="Seg_8471" s="T732">šo-bi</ta>
            <ta e="T734" id="Seg_8472" s="T733">maʔ-Tə</ta>
            <ta e="T735" id="Seg_8473" s="T734">măn-ntə</ta>
            <ta e="T737" id="Seg_8474" s="T736">kar-t</ta>
            <ta e="T738" id="Seg_8475" s="T737">ajə</ta>
            <ta e="T739" id="Seg_8476" s="T738">ajə</ta>
            <ta e="T740" id="Seg_8477" s="T739">kar-luʔbdə-bi</ta>
            <ta e="T741" id="Seg_8478" s="T740">dĭ</ta>
            <ta e="T742" id="Seg_8479" s="T741">šo-bi</ta>
            <ta e="T743" id="Seg_8480" s="T742">măndo-r-lAʔ</ta>
            <ta e="T744" id="Seg_8481" s="T743">šində=də</ta>
            <ta e="T745" id="Seg_8482" s="T744">i-bi</ta>
            <ta e="T746" id="Seg_8483" s="T745">sil</ta>
            <ta e="T747" id="Seg_8484" s="T746">naga</ta>
            <ta e="T748" id="Seg_8485" s="T747">uja</ta>
            <ta e="T749" id="Seg_8486" s="T748">naga</ta>
            <ta e="T750" id="Seg_8487" s="T749">ipek</ta>
            <ta e="T751" id="Seg_8488" s="T750">naga</ta>
            <ta e="T752" id="Seg_8489" s="T751">šində</ta>
            <ta e="T753" id="Seg_8490" s="T752">dön</ta>
            <ta e="T754" id="Seg_8491" s="T753">i-bi</ta>
            <ta e="T755" id="Seg_8492" s="T754">tibi</ta>
            <ta e="T756" id="Seg_8493" s="T755">tak</ta>
            <ta e="T757" id="Seg_8494" s="T756">büzʼe-m</ta>
            <ta e="T758" id="Seg_8495" s="T757">mo-lV-j</ta>
            <ta e="T759" id="Seg_8496" s="T758">ne</ta>
            <ta e="T760" id="Seg_8497" s="T759">tak</ta>
            <ta e="T761" id="Seg_8498" s="T760">hele-m</ta>
            <ta e="T762" id="Seg_8499" s="T761">mo-lV-j</ta>
            <ta e="T763" id="Seg_8500" s="T762">dĭgəttə</ta>
            <ta e="T764" id="Seg_8501" s="T763">dĭ</ta>
            <ta e="T765" id="Seg_8502" s="T764">nüke</ta>
            <ta e="T766" id="Seg_8503" s="T765">uʔbdə-bi</ta>
            <ta e="T767" id="Seg_8504" s="T766">šo-bi</ta>
            <ta e="T768" id="Seg_8505" s="T767">dĭ</ta>
            <ta e="T769" id="Seg_8506" s="T768">dĭ-m</ta>
            <ta e="T770" id="Seg_8507" s="T769">amnol-bi</ta>
            <ta e="T771" id="Seg_8508" s="T770">amor-lAʔ</ta>
            <ta e="T772" id="Seg_8509" s="T771">amnə-bi-jəʔ</ta>
            <ta e="T773" id="Seg_8510" s="T772">dĭ</ta>
            <ta e="T774" id="Seg_8511" s="T773">surar-laʔbə</ta>
            <ta e="T775" id="Seg_8512" s="T774">ĭmbi</ta>
            <ta e="T776" id="Seg_8513" s="T775">tăn</ta>
            <ta e="T777" id="Seg_8514" s="T776">uda-l</ta>
            <ta e="T778" id="Seg_8515" s="T777">üjü-l</ta>
            <ta e="T779" id="Seg_8516" s="T778">naga</ta>
            <ta e="T780" id="Seg_8517" s="T779">kădaʔ</ta>
            <ta e="T781" id="Seg_8518" s="T780">tăn</ta>
            <ta e="T782" id="Seg_8519" s="T781">mĭn-liA-l</ta>
            <ta e="T783" id="Seg_8520" s="T782">da</ta>
            <ta e="T784" id="Seg_8521" s="T783">dărəʔ</ta>
            <ta e="T785" id="Seg_8522" s="T784">mĭn-liA-l</ta>
            <ta e="T786" id="Seg_8523" s="T785">tăn</ta>
            <ta e="T787" id="Seg_8524" s="T786">jama-Tə</ta>
            <ta e="T788" id="Seg_8525" s="T787">süʔmə-lV-l</ta>
            <ta e="T789" id="Seg_8526" s="T788">süʔmə-lV-m</ta>
            <ta e="T790" id="Seg_8527" s="T789">dĭgəttə</ta>
            <ta e="T791" id="Seg_8528" s="T790">kan-bi-jəʔ</ta>
            <ta e="T792" id="Seg_8529" s="T791">dĭ</ta>
            <ta e="T793" id="Seg_8530" s="T792">süʔmə-bi</ta>
            <ta e="T794" id="Seg_8531" s="T793">dĭ</ta>
            <ta e="T795" id="Seg_8532" s="T794">dĭ-Tə</ta>
            <ta e="T796" id="Seg_8533" s="T795">baltu-ziʔ</ta>
            <ta e="T797" id="Seg_8534" s="T796">ulu-bə</ta>
            <ta e="T798" id="Seg_8535" s="T797">bar</ta>
            <ta e="T799" id="Seg_8536" s="T798">săj-hʼaʔ-bi</ta>
            <ta e="T800" id="Seg_8537" s="T799">dĭgəttə</ta>
            <ta e="T801" id="Seg_8538" s="T800">bar</ta>
            <ta e="T802" id="Seg_8539" s="T801">šama</ta>
            <ta e="T803" id="Seg_8540" s="T802">i-bi</ta>
            <ta e="T804" id="Seg_8541" s="T803">sil-də</ta>
            <ta e="T805" id="Seg_8542" s="T804">i-bi</ta>
            <ta e="T806" id="Seg_8543" s="T805">uja</ta>
            <ta e="T807" id="Seg_8544" s="T806">i-bi</ta>
            <ta e="T808" id="Seg_8545" s="T807">puzɨrʼ-də</ta>
            <ta e="T809" id="Seg_8546" s="T808">hen-bi</ta>
            <ta e="T810" id="Seg_8547" s="T809">dĭgəttə</ta>
            <ta e="T811" id="Seg_8548" s="T810">par-luʔbdə-bi</ta>
            <ta e="T812" id="Seg_8549" s="T811">bos-də</ta>
            <ta e="T813" id="Seg_8550" s="T812">büzʼe-gəndə</ta>
            <ta e="T814" id="Seg_8551" s="T813">maʔ-Tə</ta>
            <ta e="T817" id="Seg_8552" s="T816">sʼa-bi</ta>
            <ta e="T818" id="Seg_8553" s="T817">a</ta>
            <ta e="T819" id="Seg_8554" s="T818">büzʼe</ta>
            <ta e="T820" id="Seg_8555" s="T819">kös-ziʔ</ta>
            <ta e="T821" id="Seg_8556" s="T820">uda</ta>
            <ta e="T823" id="Seg_8557" s="T822">uda-bə</ta>
            <ta e="T825" id="Seg_8558" s="T823">bar</ta>
            <ta e="T826" id="Seg_8559" s="T825">uda-bə</ta>
            <ta e="T828" id="Seg_8560" s="T827">kös-ziʔ</ta>
            <ta e="T830" id="Seg_8561" s="T828">bar</ta>
            <ta e="T831" id="Seg_8562" s="T830">dö-m</ta>
            <ta e="T832" id="Seg_8563" s="T831">uja-m</ta>
            <ta e="T833" id="Seg_8564" s="T832">am-lV-m</ta>
            <ta e="T834" id="Seg_8565" s="T833">dö-m</ta>
            <ta e="T835" id="Seg_8566" s="T834">uja-m</ta>
            <ta e="T836" id="Seg_8567" s="T835">am-lV-m</ta>
            <ta e="T837" id="Seg_8568" s="T836">a</ta>
            <ta e="T838" id="Seg_8569" s="T837">dĭ</ta>
            <ta e="T839" id="Seg_8570" s="T838">barəʔ-luʔbdə-bi</ta>
            <ta e="T840" id="Seg_8571" s="T839">dĭ-Tə</ta>
            <ta e="T841" id="Seg_8572" s="T840">dĭ</ta>
            <ta e="T842" id="Seg_8573" s="T841">kabar-luʔbdə-bi</ta>
            <ta e="T843" id="Seg_8574" s="T842">vot</ta>
            <ta e="T844" id="Seg_8575" s="T843">kudaj</ta>
            <ta e="T845" id="Seg_8576" s="T844">măna</ta>
            <ta e="T846" id="Seg_8577" s="T845">mĭ-bi</ta>
            <ta e="T847" id="Seg_8578" s="T846">da</ta>
            <ta e="T848" id="Seg_8579" s="T847">kudaj</ta>
            <ta e="T849" id="Seg_8580" s="T848">tănan</ta>
            <ta e="T850" id="Seg_8581" s="T849">mĭ-lV-j</ta>
            <ta e="T851" id="Seg_8582" s="T850">pi</ta>
            <ta e="T852" id="Seg_8583" s="T851">tănan</ta>
            <ta e="T853" id="Seg_8584" s="T852">mĭ-lV-j</ta>
            <ta e="T854" id="Seg_8585" s="T853">kudaj</ta>
            <ta e="T855" id="Seg_8586" s="T854">dĭ-m</ta>
            <ta e="T856" id="Seg_8587" s="T855">măn</ta>
            <ta e="T857" id="Seg_8588" s="T856">mĭ-bi-m</ta>
            <ta e="T858" id="Seg_8589" s="T857">dĭgəttə</ta>
            <ta e="T859" id="Seg_8590" s="T858">dĭ</ta>
            <ta e="T860" id="Seg_8591" s="T859">amor-bi</ta>
            <ta e="T861" id="Seg_8592" s="T860">kan-bi-jəʔ</ta>
            <ta e="T862" id="Seg_8593" s="T861">dĭn</ta>
            <ta e="T863" id="Seg_8594" s="T862">ine-jəʔ</ta>
            <ta e="T864" id="Seg_8595" s="T863">tüžöj-jəʔ</ta>
            <ta e="T865" id="Seg_8596" s="T864">ular-jəʔ</ta>
            <ta e="T866" id="Seg_8597" s="T865">šo-bi-jəʔ</ta>
            <ta e="T867" id="Seg_8598" s="T866">dĭbər</ta>
            <ta e="T868" id="Seg_8599" s="T867">dĭ</ta>
            <ta e="T869" id="Seg_8600" s="T868">măn-ntə</ta>
            <ta e="T870" id="Seg_8601" s="T869">pünö-r-zittə</ta>
            <ta e="T871" id="Seg_8602" s="T870">axota</ta>
            <ta e="T872" id="Seg_8603" s="T871">e-ʔ</ta>
            <ta e="T873" id="Seg_8604" s="T872">pünö-r-ə-ʔ</ta>
            <ta e="T874" id="Seg_8605" s="T873">ato</ta>
            <ta e="T875" id="Seg_8606" s="T874">bar</ta>
            <ta e="T876" id="Seg_8607" s="T875">nuʔmə-luʔbdə-lV-jəʔ</ta>
            <ta e="T877" id="Seg_8608" s="T876">dĭ</ta>
            <ta e="T881" id="Seg_8609" s="T880">pünö-r-bi</ta>
            <ta e="T882" id="Seg_8610" s="T881">i</ta>
            <ta e="T883" id="Seg_8611" s="T882">bar</ta>
            <ta e="T884" id="Seg_8612" s="T883">ine-zAŋ-də</ta>
            <ta e="T885" id="Seg_8613" s="T884">ular-zAŋ-də</ta>
            <ta e="T886" id="Seg_8614" s="T885">i</ta>
            <ta e="T887" id="Seg_8615" s="T886">tüžöj-jəʔ</ta>
            <ta e="T888" id="Seg_8616" s="T887">üʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T889" id="Seg_8617" s="T888">dĭ</ta>
            <ta e="T890" id="Seg_8618" s="T889">davaj</ta>
            <ta e="T892" id="Seg_8619" s="T890">süt-ziʔ</ta>
            <ta e="T895" id="Seg_8620" s="T894">süt-ziʔ</ta>
            <ta e="T896" id="Seg_8621" s="T895">kămnə-zittə</ta>
            <ta e="T897" id="Seg_8622" s="T896">dĭ-zAŋ-Tə</ta>
            <ta e="T898" id="Seg_8623" s="T897">ular</ta>
            <ta e="T899" id="Seg_8624" s="T898">šiʔ</ta>
            <ta e="T900" id="Seg_8625" s="T899">poʔto</ta>
            <ta e="T901" id="Seg_8626" s="T900">mo-lAʔ</ta>
            <ta e="T902" id="Seg_8627" s="T901">tüžöj-jəʔ</ta>
            <ta e="T903" id="Seg_8628" s="T902">šiʔ</ta>
            <ta e="T904" id="Seg_8629" s="T903">šiʔ</ta>
            <ta e="T905" id="Seg_8630" s="T904">bulan</ta>
            <ta e="T906" id="Seg_8631" s="T905">mo-KAʔ</ta>
            <ta e="T907" id="Seg_8632" s="T906">a</ta>
            <ta e="T908" id="Seg_8633" s="T907">šiʔ</ta>
            <ta e="T910" id="Seg_8634" s="T908">ine-jəʔ</ta>
            <ta e="T911" id="Seg_8635" s="T910">nöməl-luʔbdə-bi-m</ta>
            <ta e="T915" id="Seg_8636" s="T914">măn-bi</ta>
            <ta e="T916" id="Seg_8637" s="T915">pušaj</ta>
            <ta e="T919" id="Seg_8638" s="T918">dĭgəttə</ta>
            <ta e="T920" id="Seg_8639" s="T919">nüke</ta>
            <ta e="T921" id="Seg_8640" s="T920">i</ta>
            <ta e="T922" id="Seg_8641" s="T921">büzʼe</ta>
            <ta e="T923" id="Seg_8642" s="T922">baʔ-luʔbdə-bi</ta>
            <ta e="T924" id="Seg_8643" s="T923">ĭmbi=də</ta>
            <ta e="T925" id="Seg_8644" s="T924">bazoʔ</ta>
            <ta e="T926" id="Seg_8645" s="T925">naga</ta>
            <ta e="T927" id="Seg_8646" s="T926">dĭ-zAŋ-Tə</ta>
            <ta e="T928" id="Seg_8647" s="T927">bar</ta>
            <ta e="T1073" id="Seg_8648" s="T928">kan-lAʔ</ta>
            <ta e="T1071" id="Seg_8649" s="T1073">tʼür-bi-jəʔ</ta>
            <ta e="T930" id="Seg_8650" s="T929">amno-bi</ta>
            <ta e="T931" id="Seg_8651" s="T930">büzʼe</ta>
            <ta e="T932" id="Seg_8652" s="T931">dĭn</ta>
            <ta e="T933" id="Seg_8653" s="T932">nagur</ta>
            <ta e="T934" id="Seg_8654" s="T933">koʔbdo-t</ta>
            <ta e="T935" id="Seg_8655" s="T934">i-bi</ta>
            <ta e="T936" id="Seg_8656" s="T935">dĭ-m</ta>
            <ta e="T937" id="Seg_8657" s="T936">kăštə-bi-jəʔ</ta>
            <ta e="T1075" id="Seg_8658" s="T937">Kazan</ta>
            <ta e="T938" id="Seg_8659" s="T1075">tura-Tə</ta>
            <ta e="T939" id="Seg_8660" s="T938">štobɨ</ta>
            <ta e="T940" id="Seg_8661" s="T939">šo-bi</ta>
            <ta e="T941" id="Seg_8662" s="T940">a</ta>
            <ta e="T942" id="Seg_8663" s="T941">urgo</ta>
            <ta e="T943" id="Seg_8664" s="T942">koʔbdo-t</ta>
            <ta e="T944" id="Seg_8665" s="T943">măn</ta>
            <ta e="T945" id="Seg_8666" s="T944">kan-lV-m</ta>
            <ta e="T946" id="Seg_8667" s="T945">eʔbdə-bə</ta>
            <ta e="T948" id="Seg_8668" s="T947">băt-bi</ta>
            <ta e="T949" id="Seg_8669" s="T948">piʔme</ta>
            <ta e="T950" id="Seg_8670" s="T949">šer-bi</ta>
            <ta e="T951" id="Seg_8671" s="T950">kujnek</ta>
            <ta e="T952" id="Seg_8672" s="T951">šer-bi</ta>
            <ta e="T953" id="Seg_8673" s="T952">üžü</ta>
            <ta e="T954" id="Seg_8674" s="T953">šer-bi</ta>
            <ta e="T955" id="Seg_8675" s="T954">kan-bi</ta>
            <ta e="T956" id="Seg_8676" s="T955">a</ta>
            <ta e="T957" id="Seg_8677" s="T956">aba-t</ta>
            <ta e="T958" id="Seg_8678" s="T957">urgaːba-ziʔ</ta>
            <ta e="T959" id="Seg_8679" s="T958">a-bi</ta>
            <ta e="T960" id="Seg_8680" s="T959">i</ta>
            <ta e="T961" id="Seg_8681" s="T960">dĭ-Tə</ta>
            <ta e="T962" id="Seg_8682" s="T961">šonə-gA</ta>
            <ta e="T963" id="Seg_8683" s="T962">dĭ</ta>
            <ta e="T964" id="Seg_8684" s="T963">bar</ta>
            <ta e="T965" id="Seg_8685" s="T964">nereʔ-luʔbdə-bi</ta>
            <ta e="T966" id="Seg_8686" s="T965">i</ta>
            <ta e="T967" id="Seg_8687" s="T966">maʔ-gəndə</ta>
            <ta e="T968" id="Seg_8688" s="T967">nuʔmə-luʔbdə-bi</ta>
            <ta e="T969" id="Seg_8689" s="T968">dĭ</ta>
            <ta e="T970" id="Seg_8690" s="T969">šo-bi</ta>
            <ta e="T971" id="Seg_8691" s="T970">ĭmbi</ta>
            <ta e="T972" id="Seg_8692" s="T971">ej</ta>
            <ta e="T973" id="Seg_8693" s="T972">kan-bi-m</ta>
            <ta e="T975" id="Seg_8694" s="T974">dĭn</ta>
            <ta e="T976" id="Seg_8695" s="T975">urgaːba</ta>
            <ta e="T977" id="Seg_8696" s="T976">măn</ta>
            <ta e="T978" id="Seg_8697" s="T977">nereʔ-luʔbdə-bi-m</ta>
            <ta e="T979" id="Seg_8698" s="T978">maʔ-gəndə</ta>
            <ta e="T980" id="Seg_8699" s="T979">šo-bi-m</ta>
            <ta e="T981" id="Seg_8700" s="T980">dĭgəttə</ta>
            <ta e="T982" id="Seg_8701" s="T981">baška</ta>
            <ta e="T983" id="Seg_8702" s="T982">koʔbdo</ta>
            <ta e="T984" id="Seg_8703" s="T983">măn</ta>
            <ta e="T985" id="Seg_8704" s="T984">kan-lV-m</ta>
            <ta e="T986" id="Seg_8705" s="T985">no</ta>
            <ta e="T987" id="Seg_8706" s="T986">kan-ə-ʔ</ta>
            <ta e="T988" id="Seg_8707" s="T987">šer-bi</ta>
            <ta e="T989" id="Seg_8708" s="T988">piʔme-jəʔ</ta>
            <ta e="T990" id="Seg_8709" s="T989">kujnek</ta>
            <ta e="T991" id="Seg_8710" s="T990">šer-bi</ta>
            <ta e="T992" id="Seg_8711" s="T991">eʔbdə-bə</ta>
            <ta e="T993" id="Seg_8712" s="T992">băt-bi</ta>
            <ta e="T994" id="Seg_8713" s="T993">üžü-bə</ta>
            <ta e="T995" id="Seg_8714" s="T994">šer-bi</ta>
            <ta e="T996" id="Seg_8715" s="T995">kan-bi</ta>
            <ta e="T997" id="Seg_8716" s="T996">dĭgəttə</ta>
            <ta e="T998" id="Seg_8717" s="T997">aba-t</ta>
            <ta e="T999" id="Seg_8718" s="T998">mo-laːm-bi</ta>
            <ta e="T1000" id="Seg_8719" s="T999">urgaːba-ziʔ</ta>
            <ta e="T1001" id="Seg_8720" s="T1000">kan-bi</ta>
            <ta e="T1002" id="Seg_8721" s="T1001">dĭ</ta>
            <ta e="T1003" id="Seg_8722" s="T1002">bar</ta>
            <ta e="T1004" id="Seg_8723" s="T1003">nereʔ-luʔbdə-bi</ta>
            <ta e="T1005" id="Seg_8724" s="T1004">maʔ-gəndə</ta>
            <ta e="T1006" id="Seg_8725" s="T1005">šo-bi</ta>
            <ta e="T1007" id="Seg_8726" s="T1006">ĭmbi</ta>
            <ta e="T1008" id="Seg_8727" s="T1007">šo-bi-l</ta>
            <ta e="T1009" id="Seg_8728" s="T1008">da</ta>
            <ta e="T1010" id="Seg_8729" s="T1009">dĭn</ta>
            <ta e="T1011" id="Seg_8730" s="T1010">urgaːba</ta>
            <ta e="T1012" id="Seg_8731" s="T1011">šonə-gA</ta>
            <ta e="T1013" id="Seg_8732" s="T1012">măn</ta>
            <ta e="T1014" id="Seg_8733" s="T1013">nereʔ-luʔbdə-bi-m</ta>
            <ta e="T1015" id="Seg_8734" s="T1014">maʔ-gəndə</ta>
            <ta e="T1016" id="Seg_8735" s="T1015">nuʔmə-luʔbdə-bi-m</ta>
            <ta e="T1017" id="Seg_8736" s="T1016">kabarləj</ta>
            <ta e="T1019" id="Seg_8737" s="T1018">dĭgəttə</ta>
            <ta e="T1020" id="Seg_8738" s="T1019">üdʼüge</ta>
            <ta e="T1021" id="Seg_8739" s="T1020">koʔbdo</ta>
            <ta e="T1022" id="Seg_8740" s="T1021">măn</ta>
            <ta e="T1023" id="Seg_8741" s="T1022">kan-lV-m</ta>
            <ta e="T1024" id="Seg_8742" s="T1023">no</ta>
            <ta e="T1025" id="Seg_8743" s="T1024">kan-ə-ʔ</ta>
            <ta e="T1026" id="Seg_8744" s="T1025">tože</ta>
            <ta e="T1027" id="Seg_8745" s="T1026">dărəʔ</ta>
            <ta e="T1028" id="Seg_8746" s="T1027">že</ta>
            <ta e="T1029" id="Seg_8747" s="T1028">eʔbdə-bə</ta>
            <ta e="T1030" id="Seg_8748" s="T1029">băt-luʔbdə-bi</ta>
            <ta e="T1031" id="Seg_8749" s="T1030">piʔme-jəʔ</ta>
            <ta e="T1032" id="Seg_8750" s="T1031">šer-bi</ta>
            <ta e="T1033" id="Seg_8751" s="T1032">kujnek</ta>
            <ta e="T1034" id="Seg_8752" s="T1033">šer-bi</ta>
            <ta e="T1035" id="Seg_8753" s="T1034">i</ta>
            <ta e="T1036" id="Seg_8754" s="T1035">üžü</ta>
            <ta e="T1037" id="Seg_8755" s="T1036">šer-bi</ta>
            <ta e="T1038" id="Seg_8756" s="T1037">kan-bi</ta>
            <ta e="T1039" id="Seg_8757" s="T1038">kandə-gA</ta>
            <ta e="T1040" id="Seg_8758" s="T1039">kandə-gA</ta>
            <ta e="T1041" id="Seg_8759" s="T1040">urgaːba</ta>
            <ta e="T1042" id="Seg_8760" s="T1041">šonə-gA</ta>
            <ta e="T1043" id="Seg_8761" s="T1042">dĭ</ta>
            <ta e="T1044" id="Seg_8762" s="T1043">strela-ziʔ</ta>
            <ta e="T1045" id="Seg_8763" s="T1044">bar</ta>
            <ta e="T1046" id="Seg_8764" s="T1045">dĭ-m</ta>
            <ta e="T1048" id="Seg_8765" s="T1047">sima-gəndə</ta>
            <ta e="T1049" id="Seg_8766" s="T1048">sima-t</ta>
            <ta e="T1051" id="Seg_8767" s="T1049">bar</ta>
            <ta e="T1052" id="Seg_8768" s="T1051">dĭ</ta>
            <ta e="T1053" id="Seg_8769" s="T1052">strela-ziʔ</ta>
            <ta e="T1054" id="Seg_8770" s="T1053">sima-gəndə</ta>
            <ta e="T1055" id="Seg_8771" s="T1054">toʔbdə-nar-bi</ta>
            <ta e="T1056" id="Seg_8772" s="T1055">i</ta>
            <ta e="T1057" id="Seg_8773" s="T1056">sima-t</ta>
            <ta e="T1059" id="Seg_8774" s="T1058">naga-luʔbdə-bi</ta>
            <ta e="T1060" id="Seg_8775" s="T1059">dĭgəttə</ta>
            <ta e="T1061" id="Seg_8776" s="T1060">kan-bi</ta>
            <ta e="T1062" id="Seg_8777" s="T1061">kan-bi</ta>
            <ta e="T1063" id="Seg_8778" s="T1062">šonə-gA</ta>
            <ta e="T1064" id="Seg_8779" s="T1063">nüke</ta>
            <ta e="T1065" id="Seg_8780" s="T1064">amno-laʔbə</ta>
            <ta e="T1066" id="Seg_8781" s="T1065">i</ta>
            <ta e="T1067" id="Seg_8782" s="T1066">nüke-n</ta>
            <ta e="T1068" id="Seg_8783" s="T1067">nʼi-t</ta>
            <ta e="T1069" id="Seg_8784" s="T1068">amno-laʔbə</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PKZ">
            <ta e="T1" id="Seg_8785" s="T0">we.NOM</ta>
            <ta e="T2" id="Seg_8786" s="T1">when</ta>
            <ta e="T3" id="Seg_8787" s="T2">come-PST-1PL</ta>
            <ta e="T4" id="Seg_8788" s="T3">Abalakovo-LAT</ta>
            <ta e="T5" id="Seg_8789" s="T4">we.LAT</ta>
            <ta e="T6" id="Seg_8790" s="T5">good</ta>
            <ta e="T7" id="Seg_8791" s="T6">be-PST.[3SG]</ta>
            <ta e="T8" id="Seg_8792" s="T7">people.[NOM.SG]</ta>
            <ta e="T9" id="Seg_8793" s="T8">spend.night-PST-1PL</ta>
            <ta e="T10" id="Seg_8794" s="T9">good</ta>
            <ta e="T11" id="Seg_8795" s="T10">then</ta>
            <ta e="T12" id="Seg_8796" s="T11">go-PST-1PL</ta>
            <ta e="T13" id="Seg_8797" s="T12">go-PST-1PL</ta>
            <ta e="T16" id="Seg_8798" s="T15">what.kind-PL</ta>
            <ta e="T17" id="Seg_8799" s="T16">man-PL</ta>
            <ta e="T18" id="Seg_8800" s="T17">see-PST-1PL</ta>
            <ta e="T19" id="Seg_8801" s="T18">speak-PST-1PL</ta>
            <ta e="T20" id="Seg_8802" s="T19">this-PL</ta>
            <ta e="T21" id="Seg_8803" s="T20">what.[NOM.SG]=INDEF</ta>
            <ta e="T22" id="Seg_8804" s="T21">NEG</ta>
            <ta e="T23" id="Seg_8805" s="T22">know-3PL</ta>
            <ta e="T24" id="Seg_8806" s="T23">Kamassian-PL</ta>
            <ta e="T25" id="Seg_8807" s="T24">Kamassian-PL</ta>
            <ta e="T26" id="Seg_8808" s="T25">people.[NOM.SG]</ta>
            <ta e="T27" id="Seg_8809" s="T26">how</ta>
            <ta e="T28" id="Seg_8810" s="T27">this-PL</ta>
            <ta e="T29" id="Seg_8811" s="T28">live-DUR-3PL</ta>
            <ta e="T30" id="Seg_8812" s="T29">we.NOM</ta>
            <ta e="T31" id="Seg_8813" s="T30">think-MOM-PST-1PL</ta>
            <ta e="T32" id="Seg_8814" s="T31">again</ta>
            <ta e="T33" id="Seg_8815" s="T32">return-INF.LAT</ta>
            <ta e="T34" id="Seg_8816" s="T33">tent-LAT/LOC.3SG</ta>
            <ta e="T37" id="Seg_8817" s="T36">one.[NOM.SG]</ta>
            <ta e="T38" id="Seg_8818" s="T37">girl.[NOM.SG]</ta>
            <ta e="T39" id="Seg_8819" s="T38">Elya.[NOM.SG]</ta>
            <ta e="T40" id="Seg_8820" s="T39">go-PST.[3SG]</ta>
            <ta e="T41" id="Seg_8821" s="T40">milk.[NOM.SG]</ta>
            <ta e="T42" id="Seg_8822" s="T41">look-FRQ-IPFVZ-INF.LAT</ta>
            <ta e="T43" id="Seg_8823" s="T42">and</ta>
            <ta e="T45" id="Seg_8824" s="T44">come-PST.[3SG]</ta>
            <ta e="T46" id="Seg_8825" s="T45">woman-LAT</ta>
            <ta e="T47" id="Seg_8826" s="T46">one.[NOM.SG]</ta>
            <ta e="T48" id="Seg_8827" s="T47">woman-LAT</ta>
            <ta e="T49" id="Seg_8828" s="T48">this.[NOM.SG]</ta>
            <ta e="T50" id="Seg_8829" s="T49">well</ta>
            <ta e="T51" id="Seg_8830" s="T50">be-PRS.[3SG]</ta>
            <ta e="T52" id="Seg_8831" s="T51">this.[NOM.SG]</ta>
            <ta e="T53" id="Seg_8832" s="T52">woman.[NOM.SG]</ta>
            <ta e="T54" id="Seg_8833" s="T53">this.[NOM.SG]</ta>
            <ta e="T55" id="Seg_8834" s="T54">say-IPFVZ.[3SG]</ta>
            <ta e="T57" id="Seg_8835" s="T56">face.[NOM.SG]</ta>
            <ta e="T64" id="Seg_8836" s="T63">hair.[NOM.SG]</ta>
            <ta e="T65" id="Seg_8837" s="T64">this.[NOM.SG]</ta>
            <ta e="T66" id="Seg_8838" s="T65">PTCL</ta>
            <ta e="T67" id="Seg_8839" s="T66">return-MOM-PST.[3SG]</ta>
            <ta e="T68" id="Seg_8840" s="T67">PTCL</ta>
            <ta e="T69" id="Seg_8841" s="T68">heart-NOM/GEN/ACC.3SG</ta>
            <ta e="T70" id="Seg_8842" s="T69">very</ta>
            <ta e="T71" id="Seg_8843" s="T70">good</ta>
            <ta e="T74" id="Seg_8844" s="T73">say-PST.[3SG]</ta>
            <ta e="T75" id="Seg_8845" s="T74">here</ta>
            <ta e="T76" id="Seg_8846" s="T75">woman.[NOM.SG]</ta>
            <ta e="T77" id="Seg_8847" s="T76">be-PRS.[3SG]</ta>
            <ta e="T78" id="Seg_8848" s="T77">Kamassian-TR-FUT</ta>
            <ta e="T79" id="Seg_8849" s="T78">can-PRS.[3SG]</ta>
            <ta e="T80" id="Seg_8850" s="T79">speak-INF.LAT</ta>
            <ta e="T82" id="Seg_8851" s="T81">this.[NOM.SG]</ta>
            <ta e="T83" id="Seg_8852" s="T82">I.LAT</ta>
            <ta e="T84" id="Seg_8853" s="T83">tell-PST.[3SG]</ta>
            <ta e="T86" id="Seg_8854" s="T85">girl.[NOM.SG]</ta>
            <ta e="T87" id="Seg_8855" s="T86">be-PRS.[3SG]</ta>
            <ta e="T88" id="Seg_8856" s="T87">and</ta>
            <ta e="T90" id="Seg_8857" s="T89">boy.[NOM.SG]</ta>
            <ta e="T92" id="Seg_8858" s="T90">then</ta>
            <ta e="T94" id="Seg_8859" s="T93">then</ta>
            <ta e="T95" id="Seg_8860" s="T94">we.NOM</ta>
            <ta e="T96" id="Seg_8861" s="T95">remain-MOM-PST-1PL</ta>
            <ta e="T97" id="Seg_8862" s="T96">this.[NOM.SG]</ta>
            <ta e="T98" id="Seg_8863" s="T97">woman-LAT</ta>
            <ta e="T100" id="Seg_8864" s="T99">go-PST-1PL</ta>
            <ta e="T101" id="Seg_8865" s="T100">speak-INF.LAT</ta>
            <ta e="T102" id="Seg_8866" s="T101">three.[NOM.SG]</ta>
            <ta e="T103" id="Seg_8867" s="T102">ten.[NOM.SG]</ta>
            <ta e="T104" id="Seg_8868" s="T103">day.[NOM.SG]</ta>
            <ta e="T105" id="Seg_8869" s="T104">speak-PST-1PL</ta>
            <ta e="T106" id="Seg_8870" s="T105">all</ta>
            <ta e="T107" id="Seg_8871" s="T106">what.[NOM.SG]</ta>
            <ta e="T108" id="Seg_8872" s="T107">tell-PST.[3SG]</ta>
            <ta e="T109" id="Seg_8873" s="T108">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T111" id="Seg_8874" s="T110">language-3SG-INS</ta>
            <ta e="T112" id="Seg_8875" s="T111">so</ta>
            <ta e="T113" id="Seg_8876" s="T112">say-PST.[3SG]</ta>
            <ta e="T114" id="Seg_8877" s="T113">I.NOM</ta>
            <ta e="T115" id="Seg_8878" s="T114">aunt-NOM/GEN/ACC.1SG</ta>
            <ta e="T116" id="Seg_8879" s="T115">die-RES-PST.[3SG]</ta>
            <ta e="T117" id="Seg_8880" s="T116">two.[NOM.SG]</ta>
            <ta e="T118" id="Seg_8881" s="T117">ten.[NOM.SG]</ta>
            <ta e="T119" id="Seg_8882" s="T118">year.[NOM.SG]</ta>
            <ta e="T120" id="Seg_8883" s="T119">go-PST.[3SG]</ta>
            <ta e="T121" id="Seg_8884" s="T120">who-COM-3SG</ta>
            <ta e="T122" id="Seg_8885" s="T121">NEG</ta>
            <ta e="T123" id="Seg_8886" s="T122">speak-PRS-1SG</ta>
            <ta e="T125" id="Seg_8887" s="T124">this-LAT</ta>
            <ta e="T126" id="Seg_8888" s="T125">now</ta>
            <ta e="T128" id="Seg_8889" s="T127">seven.[NOM.SG]</ta>
            <ta e="T129" id="Seg_8890" s="T128">year.[NOM.SG]</ta>
            <ta e="T130" id="Seg_8891" s="T129">this.[NOM.SG]</ta>
            <ta e="T131" id="Seg_8892" s="T130">very</ta>
            <ta e="T132" id="Seg_8893" s="T131">good</ta>
            <ta e="T133" id="Seg_8894" s="T132">woman.[NOM.SG]</ta>
            <ta e="T134" id="Seg_8895" s="T133">all</ta>
            <ta e="T135" id="Seg_8896" s="T134">what-NOM/GEN.3SG</ta>
            <ta e="T136" id="Seg_8897" s="T135">be-PRS.[3SG]</ta>
            <ta e="T137" id="Seg_8898" s="T136">eat-INF.LAT</ta>
            <ta e="T138" id="Seg_8899" s="T137">give-PRS.[3SG]</ta>
            <ta e="T139" id="Seg_8900" s="T138">and</ta>
            <ta e="T140" id="Seg_8901" s="T139">NEG</ta>
            <ta e="T141" id="Seg_8902" s="T140">eat-FUT-2SG</ta>
            <ta e="T142" id="Seg_8903" s="T141">so</ta>
            <ta e="T143" id="Seg_8904" s="T142">this.[NOM.SG]</ta>
            <ta e="T144" id="Seg_8905" s="T143">PTCL</ta>
            <ta e="T145" id="Seg_8906" s="T144">get.angry-DUR.[3SG]</ta>
            <ta e="T148" id="Seg_8907" s="T147">then</ta>
            <ta e="T149" id="Seg_8908" s="T148">this.[NOM.SG]</ta>
            <ta e="T150" id="Seg_8909" s="T149">always</ta>
            <ta e="T151" id="Seg_8910" s="T150">tent-LAT/LOC.3SG</ta>
            <ta e="T152" id="Seg_8911" s="T151">many</ta>
            <ta e="T153" id="Seg_8912" s="T152">work-PRS.[3SG]</ta>
            <ta e="T154" id="Seg_8913" s="T153">NEG</ta>
            <ta e="T155" id="Seg_8914" s="T154">hard.[NOM.SG]</ta>
            <ta e="T156" id="Seg_8915" s="T155">foot-LAT/LOC.3SG</ta>
            <ta e="T1074" id="Seg_8916" s="T157">Aginskoe</ta>
            <ta e="T158" id="Seg_8917" s="T1074">settlement-LAT</ta>
            <ta e="T159" id="Seg_8918" s="T158">ten.[NOM.SG]</ta>
            <ta e="T161" id="Seg_8919" s="T160">ten.[NOM.SG]</ta>
            <ta e="T162" id="Seg_8920" s="T161">two.[NOM.SG]</ta>
            <ta e="T163" id="Seg_8921" s="T162">five.[NOM.SG]</ta>
            <ta e="T164" id="Seg_8922" s="T163">this.[NOM.SG]</ta>
            <ta e="T165" id="Seg_8923" s="T164">PTCL</ta>
            <ta e="T167" id="Seg_8924" s="T166">go-PRS.[3SG]</ta>
            <ta e="T168" id="Seg_8925" s="T167">foot-INS</ta>
            <ta e="T169" id="Seg_8926" s="T168">and</ta>
            <ta e="T170" id="Seg_8927" s="T169">machine-PL</ta>
            <ta e="T171" id="Seg_8928" s="T170">go-PRS-3PL</ta>
            <ta e="T172" id="Seg_8929" s="T171">this.[NOM.SG]</ta>
            <ta e="T173" id="Seg_8930" s="T172">there</ta>
            <ta e="T174" id="Seg_8931" s="T173">NEG</ta>
            <ta e="T175" id="Seg_8932" s="T174">sit-PRS.[3SG]</ta>
            <ta e="T176" id="Seg_8933" s="T175">and</ta>
            <ta e="T177" id="Seg_8934" s="T176">on.foot-LOC.ADV</ta>
            <ta e="T180" id="Seg_8935" s="T179">go-FUT-3SG</ta>
            <ta e="T183" id="Seg_8936" s="T182">I.NOM</ta>
            <ta e="T184" id="Seg_8937" s="T183">foot-INS</ta>
            <ta e="T185" id="Seg_8938" s="T184">better</ta>
            <ta e="T186" id="Seg_8939" s="T185">go-FUT-1SG</ta>
            <ta e="T187" id="Seg_8940" s="T186">road.[NOM.SG]</ta>
            <ta e="T189" id="Seg_8941" s="T188">and</ta>
            <ta e="T190" id="Seg_8942" s="T189">machine-3PL-INS</ta>
            <ta e="T191" id="Seg_8943" s="T190">very</ta>
            <ta e="T192" id="Seg_8944" s="T191">far-LAT.ADV</ta>
            <ta e="T193" id="Seg_8945" s="T192">go-INF.LAT</ta>
            <ta e="T194" id="Seg_8946" s="T193">and</ta>
            <ta e="T195" id="Seg_8947" s="T194">PTCL</ta>
            <ta e="T196" id="Seg_8948" s="T195">tremble-DUR.[3SG]</ta>
            <ta e="T198" id="Seg_8949" s="T197">what.kind</ta>
            <ta e="T200" id="Seg_8950" s="T199">Kamassian-PL-LOC</ta>
            <ta e="T201" id="Seg_8951" s="T200">who.[NOM.SG]</ta>
            <ta e="T202" id="Seg_8952" s="T201">more</ta>
            <ta e="T203" id="Seg_8953" s="T202">can-PRS.[3SG]</ta>
            <ta e="T204" id="Seg_8954" s="T203">speak-INF.LAT</ta>
            <ta e="T205" id="Seg_8955" s="T204">or</ta>
            <ta e="T206" id="Seg_8956" s="T205">NEG</ta>
            <ta e="T208" id="Seg_8957" s="T207">outside</ta>
            <ta e="T209" id="Seg_8958" s="T208">Abalakovo-GEN</ta>
            <ta e="T210" id="Seg_8959" s="T209">child-PL-LAT</ta>
            <ta e="T211" id="Seg_8960" s="T210">one.can</ta>
            <ta e="T212" id="Seg_8961" s="T211">find-INF.LAT</ta>
            <ta e="T213" id="Seg_8962" s="T212">snow.[NOM.SG]</ta>
            <ta e="T214" id="Seg_8963" s="T213">head-NOM/GEN.3SG</ta>
            <ta e="T215" id="Seg_8964" s="T214">eye-NOM/GEN.3SG</ta>
            <ta e="T216" id="Seg_8965" s="T215">black.[NOM.SG]</ta>
            <ta e="T219" id="Seg_8966" s="T218">Russian</ta>
            <ta e="T220" id="Seg_8967" s="T219">blood.[NOM.SG]</ta>
            <ta e="T221" id="Seg_8968" s="T220">be-PRS.[3SG]</ta>
            <ta e="T222" id="Seg_8969" s="T221">and</ta>
            <ta e="T223" id="Seg_8970" s="T222">blood.[NOM.SG]</ta>
            <ta e="T224" id="Seg_8971" s="T223">be-PRS.[3SG]</ta>
            <ta e="T225" id="Seg_8972" s="T224">Kamassian-PL</ta>
            <ta e="T227" id="Seg_8973" s="T226">what.kind</ta>
            <ta e="T228" id="Seg_8974" s="T227">PTCL</ta>
            <ta e="T229" id="Seg_8975" s="T228">grow-DUR-3PL</ta>
            <ta e="T230" id="Seg_8976" s="T229">whether</ta>
            <ta e="T231" id="Seg_8977" s="T230">go-PST-3PL</ta>
            <ta e="T232" id="Seg_8978" s="T231">work-INF.LAT</ta>
            <ta e="T233" id="Seg_8979" s="T232">there_here</ta>
            <ta e="T234" id="Seg_8980" s="T233">look</ta>
            <ta e="T239" id="Seg_8981" s="T238">and</ta>
            <ta e="T240" id="Seg_8982" s="T239">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T241" id="Seg_8983" s="T240">tongue-NOM/GEN.3SG</ta>
            <ta e="T242" id="Seg_8984" s="T241">NEG</ta>
            <ta e="T243" id="Seg_8985" s="T242">know-3SG.O</ta>
            <ta e="T245" id="Seg_8986" s="T244">only</ta>
            <ta e="T246" id="Seg_8987" s="T245">this.[NOM.SG]</ta>
            <ta e="T247" id="Seg_8988" s="T246">Kamassian-PL</ta>
            <ta e="T248" id="Seg_8989" s="T247">tongue-NOM/GEN.3SG</ta>
            <ta e="T249" id="Seg_8990" s="T248">one.[NOM.SG]</ta>
            <ta e="T250" id="Seg_8991" s="T249">woman.[NOM.SG]</ta>
            <ta e="T251" id="Seg_8992" s="T250">know-3SG.O</ta>
            <ta e="T252" id="Seg_8993" s="T251">and</ta>
            <ta e="T253" id="Seg_8994" s="T252">else</ta>
            <ta e="T254" id="Seg_8995" s="T253">who.[NOM.SG]=INDEF</ta>
            <ta e="T255" id="Seg_8996" s="T254">NEG</ta>
            <ta e="T256" id="Seg_8997" s="T255">know.[3SG]</ta>
            <ta e="T257" id="Seg_8998" s="T256">this.[NOM.SG]</ta>
            <ta e="T258" id="Seg_8999" s="T257">language-NOM/GEN/ACC.1SG</ta>
            <ta e="T260" id="Seg_9000" s="T259">you.NOM</ta>
            <ta e="T261" id="Seg_9001" s="T260">I.LAT</ta>
            <ta e="T262" id="Seg_9002" s="T261">lie-PST-2SG</ta>
            <ta e="T263" id="Seg_9003" s="T262">you.NOM</ta>
            <ta e="T264" id="Seg_9004" s="T263">I.LAT</ta>
            <ta e="T265" id="Seg_9005" s="T264">lure-MOM-PST-2SG</ta>
            <ta e="T266" id="Seg_9006" s="T265">then</ta>
            <ta e="T267" id="Seg_9007" s="T266">hide-RES-PST-1SG</ta>
            <ta e="T268" id="Seg_9008" s="T267">and</ta>
            <ta e="T269" id="Seg_9009" s="T268">I.NOM</ta>
            <ta e="T270" id="Seg_9010" s="T269">you.DAT</ta>
            <ta e="T271" id="Seg_9011" s="T270">look-FRQ-PST-1SG</ta>
            <ta e="T272" id="Seg_9012" s="T271">look-FRQ-PST-1SG</ta>
            <ta e="T273" id="Seg_9013" s="T272">NEG</ta>
            <ta e="T275" id="Seg_9014" s="T274">can-PRS-1SG</ta>
            <ta e="T276" id="Seg_9015" s="T275">look-FRQ-INF.LAT</ta>
            <ta e="T277" id="Seg_9016" s="T276">NEG</ta>
            <ta e="T278" id="Seg_9017" s="T277">good</ta>
            <ta e="T279" id="Seg_9018" s="T278">man.[NOM.SG]</ta>
            <ta e="T280" id="Seg_9019" s="T279">live-PST.[3SG]</ta>
            <ta e="T281" id="Seg_9020" s="T280">this-ACC</ta>
            <ta e="T282" id="Seg_9021" s="T281">name-DUR-PST-3PL</ta>
            <ta e="T283" id="Seg_9022" s="T282">Tardzha_Bardzha</ta>
            <ta e="T284" id="Seg_9023" s="T283">this.[NOM.SG]</ta>
            <ta e="T285" id="Seg_9024" s="T284">PTCL</ta>
            <ta e="T286" id="Seg_9025" s="T285">mare.[NOM.SG]</ta>
            <ta e="T287" id="Seg_9026" s="T286">capture-PST.[3SG]</ta>
            <ta e="T288" id="Seg_9027" s="T287">foot-NOM/GEN.3SG</ta>
            <ta e="T289" id="Seg_9028" s="T288">hurt-PRS.[3SG]</ta>
            <ta e="T290" id="Seg_9029" s="T289">go-INF.LAT</ta>
            <ta e="T291" id="Seg_9030" s="T290">NEG</ta>
            <ta e="T292" id="Seg_9031" s="T291">can-PRS.[3SG]</ta>
            <ta e="T293" id="Seg_9032" s="T292">then</ta>
            <ta e="T294" id="Seg_9033" s="T293">go-PST.[3SG]</ta>
            <ta e="T295" id="Seg_9034" s="T294">go-PST.[3SG]</ta>
            <ta e="T296" id="Seg_9035" s="T295">bear.[NOM.SG]</ta>
            <ta e="T297" id="Seg_9036" s="T296">come-PRS.[3SG]</ta>
            <ta e="T298" id="Seg_9037" s="T297">take-IMP.2SG</ta>
            <ta e="T299" id="Seg_9038" s="T298">I.LAT</ta>
            <ta e="T300" id="Seg_9039" s="T299">self-INS</ta>
            <ta e="T301" id="Seg_9040" s="T300">well</ta>
            <ta e="T302" id="Seg_9041" s="T301">only</ta>
            <ta e="T303" id="Seg_9042" s="T302">black.[NOM.SG]</ta>
            <ta e="T305" id="Seg_9043" s="T304">sack-LAT</ta>
            <ta e="T306" id="Seg_9044" s="T305">seat-IMP.2SG.O</ta>
            <ta e="T307" id="Seg_9045" s="T306">I.ACC</ta>
            <ta e="T308" id="Seg_9046" s="T307">this.[NOM.SG]</ta>
            <ta e="T309" id="Seg_9047" s="T308">this-ACC</ta>
            <ta e="T310" id="Seg_9048" s="T309">seat-PST.[3SG]</ta>
            <ta e="T311" id="Seg_9049" s="T310">again</ta>
            <ta e="T312" id="Seg_9050" s="T311">walk-PRS.[3SG]</ta>
            <ta e="T313" id="Seg_9051" s="T312">walk-PRS.[3SG]</ta>
            <ta e="T314" id="Seg_9052" s="T313">walk-PRS.[3SG]</ta>
            <ta e="T316" id="Seg_9053" s="T314">then</ta>
            <ta e="T317" id="Seg_9054" s="T316">then</ta>
            <ta e="T318" id="Seg_9055" s="T317">big.[NOM.SG]</ta>
            <ta e="T319" id="Seg_9056" s="T318">dog.[NOM.SG]</ta>
            <ta e="T320" id="Seg_9057" s="T319">forest-ABL</ta>
            <ta e="T321" id="Seg_9058" s="T320">come-PST.[3SG]</ta>
            <ta e="T322" id="Seg_9059" s="T321">take-IMP.2SG</ta>
            <ta e="T323" id="Seg_9060" s="T322">I.LAT</ta>
            <ta e="T324" id="Seg_9061" s="T323">only</ta>
            <ta e="T325" id="Seg_9062" s="T324">black.[NOM.SG]</ta>
            <ta e="T326" id="Seg_9063" s="T325">I.LAT</ta>
            <ta e="T327" id="Seg_9064" s="T326">sack-LAT</ta>
            <ta e="T328" id="Seg_9065" s="T327">seat-IMP.2SG.O</ta>
            <ta e="T329" id="Seg_9066" s="T328">this.[NOM.SG]</ta>
            <ta e="T330" id="Seg_9067" s="T329">seat-TR-PST.[3SG]</ta>
            <ta e="T331" id="Seg_9068" s="T330">go-PST.[3SG]</ta>
            <ta e="T332" id="Seg_9069" s="T331">again</ta>
            <ta e="T333" id="Seg_9070" s="T332">come-PRS.[3SG]</ta>
            <ta e="T334" id="Seg_9071" s="T333">come-PRS.[3SG]</ta>
            <ta e="T338" id="Seg_9072" s="T337">fox.[NOM.SG]</ta>
            <ta e="T339" id="Seg_9073" s="T338">come-DUR.[3SG]</ta>
            <ta e="T340" id="Seg_9074" s="T339">take-IMP.2SG</ta>
            <ta e="T341" id="Seg_9075" s="T340">I.LAT</ta>
            <ta e="T342" id="Seg_9076" s="T341">this.[NOM.SG]</ta>
            <ta e="T343" id="Seg_9077" s="T342">this-ACC</ta>
            <ta e="T344" id="Seg_9078" s="T343">take-PST.[3SG]</ta>
            <ta e="T345" id="Seg_9079" s="T344">black.[NOM.SG]</ta>
            <ta e="T346" id="Seg_9080" s="T345">sack-LAT</ta>
            <ta e="T347" id="Seg_9081" s="T346">seat-PST.[3SG]</ta>
            <ta e="T348" id="Seg_9082" s="T347">then</ta>
            <ta e="T349" id="Seg_9083" s="T348">again</ta>
            <ta e="T350" id="Seg_9084" s="T349">walk-PRS.[3SG]</ta>
            <ta e="T351" id="Seg_9085" s="T350">water.[NOM.SG]</ta>
            <ta e="T352" id="Seg_9086" s="T351">come-PRS.[3SG]</ta>
            <ta e="T353" id="Seg_9087" s="T352">take-IMP.2SG</ta>
            <ta e="T354" id="Seg_9088" s="T353">I.LAT</ta>
            <ta e="T355" id="Seg_9089" s="T354">then</ta>
            <ta e="T356" id="Seg_9090" s="T355">this.[NOM.SG]</ta>
            <ta e="T357" id="Seg_9091" s="T356">water.[NOM.SG]</ta>
            <ta e="T358" id="Seg_9092" s="T357">pour-PST.[3SG]</ta>
            <ta e="T359" id="Seg_9093" s="T358">there</ta>
            <ta e="T360" id="Seg_9094" s="T359">come-PST.[3SG]</ta>
            <ta e="T361" id="Seg_9095" s="T360">there</ta>
            <ta e="T362" id="Seg_9096" s="T361">PTCL</ta>
            <ta e="T363" id="Seg_9097" s="T362">house-PL</ta>
            <ta e="T364" id="Seg_9098" s="T363">stand-PRS-3PL</ta>
            <ta e="T365" id="Seg_9099" s="T364">and</ta>
            <ta e="T366" id="Seg_9100" s="T365">water.[NOM.SG]</ta>
            <ta e="T367" id="Seg_9101" s="T366">very</ta>
            <ta e="T368" id="Seg_9102" s="T367">big.[NOM.SG]</ta>
            <ta e="T369" id="Seg_9103" s="T368">this.[NOM.SG]</ta>
            <ta e="T370" id="Seg_9104" s="T369">mare.[NOM.SG]</ta>
            <ta e="T371" id="Seg_9105" s="T370">bind-PST.[3SG]</ta>
            <ta e="T372" id="Seg_9106" s="T371">fire.[NOM.SG]</ta>
            <ta e="T373" id="Seg_9107" s="T372">put-PST.[3SG]</ta>
            <ta e="T374" id="Seg_9108" s="T373">sit-DUR.[3SG]</ta>
            <ta e="T375" id="Seg_9109" s="T374">this-ACC</ta>
            <ta e="T376" id="Seg_9110" s="T375">see-MOM-PST.[3SG]</ta>
            <ta e="T377" id="Seg_9111" s="T376">chief.[NOM.SG]</ta>
            <ta e="T378" id="Seg_9112" s="T377">who.[NOM.SG]</ta>
            <ta e="T379" id="Seg_9113" s="T378">there</ta>
            <ta e="T380" id="Seg_9114" s="T379">sit-DUR.[3SG]</ta>
            <ta e="T381" id="Seg_9115" s="T380">I.NOM</ta>
            <ta e="T383" id="Seg_9116" s="T382">grass-LAT</ta>
            <ta e="T384" id="Seg_9117" s="T383">tread-DUR.[3SG]</ta>
            <ta e="T385" id="Seg_9118" s="T384">mare-NOM/GEN.3SG</ta>
            <ta e="T386" id="Seg_9119" s="T385">eat-DUR.[3SG]</ta>
            <ta e="T387" id="Seg_9120" s="T386">there</ta>
            <ta e="T388" id="Seg_9121" s="T387">small-LOC.ADV</ta>
            <ta e="T389" id="Seg_9122" s="T388">sit-FUT-3SG</ta>
            <ta e="T390" id="Seg_9123" s="T389">then</ta>
            <ta e="T391" id="Seg_9124" s="T390">again</ta>
            <ta e="T392" id="Seg_9125" s="T391">PTCL</ta>
            <ta e="T393" id="Seg_9126" s="T392">tread-FUT-3SG</ta>
            <ta e="T394" id="Seg_9127" s="T393">go-EP-IMP.2SG</ta>
            <ta e="T395" id="Seg_9128" s="T394">son-ACC.3SG</ta>
            <ta e="T396" id="Seg_9129" s="T395">send-PST.[3SG]</ta>
            <ta e="T397" id="Seg_9130" s="T396">what.[NOM.SG]</ta>
            <ta e="T398" id="Seg_9131" s="T397">this-LAT</ta>
            <ta e="T399" id="Seg_9132" s="T398">one.needs</ta>
            <ta e="T400" id="Seg_9133" s="T399">this.[NOM.SG]</ta>
            <ta e="T401" id="Seg_9134" s="T400">come-PST.[3SG]</ta>
            <ta e="T403" id="Seg_9135" s="T402">ask-DUR.[3SG]</ta>
            <ta e="T404" id="Seg_9136" s="T403">what.[NOM.SG]</ta>
            <ta e="T405" id="Seg_9137" s="T404">you.DAT</ta>
            <ta e="T406" id="Seg_9138" s="T405">one.needs</ta>
            <ta e="T407" id="Seg_9139" s="T406">this-GEN</ta>
            <ta e="T408" id="Seg_9140" s="T407">chief-NOM/GEN/ACC.3SG</ta>
            <ta e="T409" id="Seg_9141" s="T408">daughter-ACC.3SG</ta>
            <ta e="T410" id="Seg_9142" s="T409">take-FUT-1SG</ta>
            <ta e="T412" id="Seg_9143" s="T411">this.[NOM.SG]</ta>
            <ta e="T413" id="Seg_9144" s="T412">boy.[NOM.SG]</ta>
            <ta e="T414" id="Seg_9145" s="T413">return-MOM-PST.[3SG]</ta>
            <ta e="T415" id="Seg_9146" s="T414">say-IPFVZ.[3SG]</ta>
            <ta e="T416" id="Seg_9147" s="T415">this.[NOM.SG]</ta>
            <ta e="T417" id="Seg_9148" s="T416">say-IPFVZ.[3SG]</ta>
            <ta e="T418" id="Seg_9149" s="T417">JUSS</ta>
            <ta e="T419" id="Seg_9150" s="T418">this-ACC</ta>
            <ta e="T420" id="Seg_9151" s="T419">chief-NOM/GEN/ACC.3SG</ta>
            <ta e="T421" id="Seg_9152" s="T420">daughter-ACC.3SG</ta>
            <ta e="T422" id="Seg_9153" s="T421">take-FUT-1SG</ta>
            <ta e="T423" id="Seg_9154" s="T422">JUSS</ta>
            <ta e="T424" id="Seg_9155" s="T423">go-IMP-3PL</ta>
            <ta e="T425" id="Seg_9156" s="T424">otherwise</ta>
            <ta e="T426" id="Seg_9157" s="T425">NEG</ta>
            <ta e="T427" id="Seg_9158" s="T426">good</ta>
            <ta e="T428" id="Seg_9159" s="T427">become-FUT-3SG</ta>
            <ta e="T430" id="Seg_9160" s="T429">NEG</ta>
            <ta e="T431" id="Seg_9161" s="T430">give-FUT-1SG</ta>
            <ta e="T432" id="Seg_9162" s="T431">this.[NOM.SG]</ta>
            <ta e="T433" id="Seg_9163" s="T432">come-PST.[3SG]</ta>
            <ta e="T435" id="Seg_9164" s="T434">say-PST.[3SG]</ta>
            <ta e="T436" id="Seg_9165" s="T435">so</ta>
            <ta e="T437" id="Seg_9166" s="T436">chief.[NOM.SG]</ta>
            <ta e="T438" id="Seg_9167" s="T437">NEG</ta>
            <ta e="T439" id="Seg_9168" s="T438">give-PRS-3SG.O</ta>
            <ta e="T440" id="Seg_9169" s="T439">daughter-ACC.3SG</ta>
            <ta e="T441" id="Seg_9170" s="T440">NEG</ta>
            <ta e="T442" id="Seg_9171" s="T441">give-FUT-3SG</ta>
            <ta e="T443" id="Seg_9172" s="T442">good-LAT.ADV</ta>
            <ta e="T444" id="Seg_9173" s="T443">so</ta>
            <ta e="T445" id="Seg_9174" s="T444">NEG</ta>
            <ta e="T446" id="Seg_9175" s="T445">good</ta>
            <ta e="T447" id="Seg_9176" s="T446">give-FUT-3SG</ta>
            <ta e="T449" id="Seg_9177" s="T448">let-2PL</ta>
            <ta e="T451" id="Seg_9178" s="T450">horse-PL</ta>
            <ta e="T452" id="Seg_9179" s="T451">this-PL</ta>
            <ta e="T453" id="Seg_9180" s="T452">PTCL</ta>
            <ta e="T454" id="Seg_9181" s="T453">there</ta>
            <ta e="T455" id="Seg_9182" s="T454">mare-LAT/LOC.3SG</ta>
            <ta e="T456" id="Seg_9183" s="T455">off-pull-MOM-FUT-3PL</ta>
            <ta e="T457" id="Seg_9184" s="T456">then</ta>
            <ta e="T1072" id="Seg_9185" s="T457">go-CVB</ta>
            <ta e="T458" id="Seg_9186" s="T1072">disappear-FUT-3SG</ta>
            <ta e="T459" id="Seg_9187" s="T458">then</ta>
            <ta e="T460" id="Seg_9188" s="T459">this.[NOM.SG]</ta>
            <ta e="T461" id="Seg_9189" s="T460">bear.[NOM.SG]</ta>
            <ta e="T462" id="Seg_9190" s="T461">send-PST.[3SG]</ta>
            <ta e="T463" id="Seg_9191" s="T462">black.[NOM.SG]</ta>
            <ta e="T464" id="Seg_9192" s="T463">sack-ABL</ta>
            <ta e="T465" id="Seg_9193" s="T464">this.[NOM.SG]</ta>
            <ta e="T466" id="Seg_9194" s="T465">bear.[NOM.SG]</ta>
            <ta e="T467" id="Seg_9195" s="T466">horse-PL</ta>
            <ta e="T468" id="Seg_9196" s="T467">PTCL</ta>
            <ta e="T469" id="Seg_9197" s="T468">drive-MOM-PST.[3SG]</ta>
            <ta e="T470" id="Seg_9198" s="T469">then</ta>
            <ta e="T471" id="Seg_9199" s="T470">say-IPFVZ.[3SG]</ta>
            <ta e="T472" id="Seg_9200" s="T471">send-IMP.2PL</ta>
            <ta e="T473" id="Seg_9201" s="T472">cow-PL</ta>
            <ta e="T474" id="Seg_9202" s="T473">JUSS</ta>
            <ta e="T475" id="Seg_9203" s="T474">cow-PL</ta>
            <ta e="T476" id="Seg_9204" s="T475">horn-3SG-INS</ta>
            <ta e="T477" id="Seg_9205" s="T476">PTCL</ta>
            <ta e="T478" id="Seg_9206" s="T477">horse-ACC.3SG</ta>
            <ta e="T479" id="Seg_9207" s="T478">PTCL</ta>
            <ta e="T480" id="Seg_9208" s="T479">kill-FUT-3PL</ta>
            <ta e="T481" id="Seg_9209" s="T480">this.[NOM.SG]</ta>
            <ta e="T482" id="Seg_9210" s="T481">this.[NOM.SG]</ta>
            <ta e="T483" id="Seg_9211" s="T482">PTCL</ta>
            <ta e="T484" id="Seg_9212" s="T483">sack-ABL</ta>
            <ta e="T485" id="Seg_9213" s="T484">send-PST.[3SG]</ta>
            <ta e="T486" id="Seg_9214" s="T485">big.[NOM.SG]</ta>
            <ta e="T487" id="Seg_9215" s="T486">dog.[NOM.SG]</ta>
            <ta e="T488" id="Seg_9216" s="T487">this.[NOM.SG]</ta>
            <ta e="T489" id="Seg_9217" s="T488">this.[NOM.SG]</ta>
            <ta e="T490" id="Seg_9218" s="T489">go-PST.[3SG]</ta>
            <ta e="T491" id="Seg_9219" s="T490">this-PL</ta>
            <ta e="T492" id="Seg_9220" s="T491">PTCL</ta>
            <ta e="T493" id="Seg_9221" s="T492">drive-MOM-PST.[3SG]</ta>
            <ta e="T494" id="Seg_9222" s="T493">then</ta>
            <ta e="T495" id="Seg_9223" s="T494">say-IPFVZ.[3SG]</ta>
            <ta e="T497" id="Seg_9224" s="T495">send-IMP.2PL</ta>
            <ta e="T499" id="Seg_9225" s="T498">then</ta>
            <ta e="T500" id="Seg_9226" s="T499">chief.[NOM.SG]</ta>
            <ta e="T503" id="Seg_9227" s="T502">let-2PL</ta>
            <ta e="T504" id="Seg_9228" s="T503">dog-PL</ta>
            <ta e="T505" id="Seg_9229" s="T504">JUSS</ta>
            <ta e="T506" id="Seg_9230" s="T505">dog-PL</ta>
            <ta e="T507" id="Seg_9231" s="T506">this-ACC</ta>
            <ta e="T508" id="Seg_9232" s="T507">off-tear-FUT-3PL</ta>
            <ta e="T509" id="Seg_9233" s="T508">this.[NOM.SG]</ta>
            <ta e="T510" id="Seg_9234" s="T509">send-PST.[3SG]</ta>
            <ta e="T511" id="Seg_9235" s="T510">dog-NOM/GEN/ACC.3PL</ta>
            <ta e="T512" id="Seg_9236" s="T511">and</ta>
            <ta e="T513" id="Seg_9237" s="T512">this.[NOM.SG]</ta>
            <ta e="T514" id="Seg_9238" s="T513">fox.[NOM.SG]</ta>
            <ta e="T515" id="Seg_9239" s="T514">send-PST.[3SG]</ta>
            <ta e="T516" id="Seg_9240" s="T515">dog-PL</ta>
            <ta e="T517" id="Seg_9241" s="T516">fox-LOC</ta>
            <ta e="T518" id="Seg_9242" s="T517">run-MOM-PST-3PL</ta>
            <ta e="T519" id="Seg_9243" s="T518">then</ta>
            <ta e="T520" id="Seg_9244" s="T519">this.[NOM.SG]</ta>
            <ta e="T521" id="Seg_9245" s="T520">chief.[NOM.SG]</ta>
            <ta e="T522" id="Seg_9246" s="T521">PTCL</ta>
            <ta e="T523" id="Seg_9247" s="T522">people.[NOM.SG]</ta>
            <ta e="T524" id="Seg_9248" s="T523">collect-PST.[3SG]</ta>
            <ta e="T525" id="Seg_9249" s="T524">go-OPT.DU/PL-1DU</ta>
            <ta e="T526" id="Seg_9250" s="T525">this-ACC</ta>
            <ta e="T527" id="Seg_9251" s="T526">take-FUT-1DU</ta>
            <ta e="T528" id="Seg_9252" s="T527">people.[NOM.SG]</ta>
            <ta e="T529" id="Seg_9253" s="T528">come-PST-3PL</ta>
            <ta e="T530" id="Seg_9254" s="T529">this.[NOM.SG]</ta>
            <ta e="T531" id="Seg_9255" s="T530">PTCL</ta>
            <ta e="T532" id="Seg_9256" s="T531">sack-ACC.3SG</ta>
            <ta e="T533" id="Seg_9257" s="T532">water.[NOM.SG]</ta>
            <ta e="T534" id="Seg_9258" s="T533">pour-PST.[3SG]</ta>
            <ta e="T535" id="Seg_9259" s="T534">and</ta>
            <ta e="T536" id="Seg_9260" s="T535">water.[NOM.SG]</ta>
            <ta e="T537" id="Seg_9261" s="T536">people.[NOM.SG]</ta>
            <ta e="T538" id="Seg_9262" s="T537">people.[NOM.SG]</ta>
            <ta e="T539" id="Seg_9263" s="T538">PTCL</ta>
            <ta e="T540" id="Seg_9264" s="T539">run-MOM-PST-3PL</ta>
            <ta e="T541" id="Seg_9265" s="T540">water-ABL</ta>
            <ta e="T543" id="Seg_9266" s="T542">then</ta>
            <ta e="T544" id="Seg_9267" s="T543">this.[NOM.SG]</ta>
            <ta e="T545" id="Seg_9268" s="T544">chief.[NOM.SG]</ta>
            <ta e="T546" id="Seg_9269" s="T545">shout-DUR.[3SG]</ta>
            <ta e="T547" id="Seg_9270" s="T546">half-NOM/GEN/ACC.3SG</ta>
            <ta e="T549" id="Seg_9271" s="T548">be-PRS.[3SG]</ta>
            <ta e="T550" id="Seg_9272" s="T549">say-PST.[3SG]</ta>
            <ta e="T551" id="Seg_9273" s="T550">I.NOM</ta>
            <ta e="T552" id="Seg_9274" s="T551">be-PRS.[3SG]</ta>
            <ta e="T553" id="Seg_9275" s="T552">cow-PL</ta>
            <ta e="T554" id="Seg_9276" s="T553">horse-PL</ta>
            <ta e="T555" id="Seg_9277" s="T554">dog-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T556" id="Seg_9278" s="T555">say-PST-1SG</ta>
            <ta e="T557" id="Seg_9279" s="T556">money.[NOM.SG]</ta>
            <ta e="T558" id="Seg_9280" s="T557">many</ta>
            <ta e="T559" id="Seg_9281" s="T558">say-PST.[3SG]</ta>
            <ta e="T560" id="Seg_9282" s="T559">what.[NOM.SG]=INDEF</ta>
            <ta e="T561" id="Seg_9283" s="T560">I.LAT</ta>
            <ta e="T562" id="Seg_9284" s="T561">NEG</ta>
            <ta e="T563" id="Seg_9285" s="T562">one.needs</ta>
            <ta e="T564" id="Seg_9286" s="T563">only</ta>
            <ta e="T565" id="Seg_9287" s="T564">daughter-ACC</ta>
            <ta e="T566" id="Seg_9288" s="T565">take-INF.LAT</ta>
            <ta e="T568" id="Seg_9289" s="T567">then</ta>
            <ta e="T569" id="Seg_9290" s="T568">black.[NOM.SG]</ta>
            <ta e="T570" id="Seg_9291" s="T569">sack-LAT</ta>
            <ta e="T571" id="Seg_9292" s="T570">water.[NOM.SG]</ta>
            <ta e="T572" id="Seg_9293" s="T571">take-PST.[3SG]</ta>
            <ta e="T574" id="Seg_9294" s="T572">and</ta>
            <ta e="T576" id="Seg_9295" s="T575">then</ta>
            <ta e="T577" id="Seg_9296" s="T576">give-MOM-PST.[3SG]</ta>
            <ta e="T578" id="Seg_9297" s="T577">daughter-ACC.3SG</ta>
            <ta e="T579" id="Seg_9298" s="T578">this.[NOM.SG]</ta>
            <ta e="T580" id="Seg_9299" s="T579">seat-PST.[3SG]</ta>
            <ta e="T581" id="Seg_9300" s="T580">seat-PST.[3SG]</ta>
            <ta e="T582" id="Seg_9301" s="T581">mare-LAT</ta>
            <ta e="T583" id="Seg_9302" s="T582">and</ta>
            <ta e="T584" id="Seg_9303" s="T583">go-PST.[3SG]</ta>
            <ta e="T585" id="Seg_9304" s="T584">tent-LAT/LOC.3SG</ta>
            <ta e="T586" id="Seg_9305" s="T585">two.[NOM.SG]</ta>
            <ta e="T587" id="Seg_9306" s="T586">brother.[NOM.SG]</ta>
            <ta e="T588" id="Seg_9307" s="T587">live-DUR-PST.[3SG]</ta>
            <ta e="T589" id="Seg_9308" s="T588">live-DUR-PST.[3SG]</ta>
            <ta e="T590" id="Seg_9309" s="T589">this-PL</ta>
            <ta e="T591" id="Seg_9310" s="T590">woman-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T592" id="Seg_9311" s="T591">be-PST-3PL</ta>
            <ta e="T593" id="Seg_9312" s="T592">this-PL</ta>
            <ta e="T594" id="Seg_9313" s="T593">collect-PST-3PL</ta>
            <ta e="T595" id="Seg_9314" s="T594">go-PST-3PL</ta>
            <ta e="T596" id="Seg_9315" s="T595">forest-LAT</ta>
            <ta e="T597" id="Seg_9316" s="T596">and</ta>
            <ta e="T598" id="Seg_9317" s="T597">woman-PL</ta>
            <ta e="T599" id="Seg_9318" s="T598">tent-LAT/LOC.3SG</ta>
            <ta e="T600" id="Seg_9319" s="T599">PTCL</ta>
            <ta e="T601" id="Seg_9320" s="T600">evening</ta>
            <ta e="T602" id="Seg_9321" s="T601">evening-LOC.ADV</ta>
            <ta e="T604" id="Seg_9322" s="T603">eat-INF.LAT</ta>
            <ta e="T605" id="Seg_9323" s="T604">sit.down-PST-3PL</ta>
            <ta e="T606" id="Seg_9324" s="T605">then</ta>
            <ta e="T607" id="Seg_9325" s="T606">look-PRS-3PL</ta>
            <ta e="T608" id="Seg_9326" s="T607">cauldron-LOC</ta>
            <ta e="T610" id="Seg_9327" s="T609">what.kind=INDEF</ta>
            <ta e="T611" id="Seg_9328" s="T610">man.[NOM.SG]</ta>
            <ta e="T612" id="Seg_9329" s="T611">sit-DUR.[3SG]</ta>
            <ta e="T613" id="Seg_9330" s="T612">this-PL</ta>
            <ta e="T614" id="Seg_9331" s="T613">nest-PL</ta>
            <ta e="T616" id="Seg_9332" s="T615">collect-PST-3PL</ta>
            <ta e="T617" id="Seg_9333" s="T616">and</ta>
            <ta e="T618" id="Seg_9334" s="T617">cauldron-NOM/GEN/ACC.3SG</ta>
            <ta e="T620" id="Seg_9335" s="T619">cauldron-NOM/GEN/ACC.3SG</ta>
            <ta e="T621" id="Seg_9336" s="T620">put-PST-3PL</ta>
            <ta e="T622" id="Seg_9337" s="T621">and</ta>
            <ta e="T623" id="Seg_9338" s="T622">this.[NOM.SG]</ta>
            <ta e="T624" id="Seg_9339" s="T623">branch.[NOM.SG]</ta>
            <ta e="T626" id="Seg_9340" s="T624">PTCL</ta>
            <ta e="T628" id="Seg_9341" s="T627">wet.[NOM.SG]</ta>
            <ta e="T629" id="Seg_9342" s="T628">be-PST.[3SG]</ta>
            <ta e="T630" id="Seg_9343" s="T629">then</ta>
            <ta e="T631" id="Seg_9344" s="T630">this.[NOM.SG]</ta>
            <ta e="T632" id="Seg_9345" s="T631">come-PST.[3SG]</ta>
            <ta e="T633" id="Seg_9346" s="T632">NEG</ta>
            <ta e="T634" id="Seg_9347" s="T633">can-PST.[3SG]</ta>
            <ta e="T635" id="Seg_9348" s="T634">burn-INF.LAT</ta>
            <ta e="T636" id="Seg_9349" s="T635">this.[NOM.SG]</ta>
            <ta e="T637" id="Seg_9350" s="T636">man.[NOM.SG]</ta>
            <ta e="T638" id="Seg_9351" s="T637">one.[NOM.SG]</ta>
            <ta e="T639" id="Seg_9352" s="T638">woman-ACC</ta>
            <ta e="T640" id="Seg_9353" s="T639">kill-CVB-PST.[3SG]</ta>
            <ta e="T641" id="Seg_9354" s="T640">then</ta>
            <ta e="T642" id="Seg_9355" s="T641">two.[NOM.SG]</ta>
            <ta e="T644" id="Seg_9356" s="T643">again</ta>
            <ta e="T645" id="Seg_9357" s="T644">woman-LAT</ta>
            <ta e="T646" id="Seg_9358" s="T645">say-IPFVZ.[3SG]</ta>
            <ta e="T647" id="Seg_9359" s="T646">what.[NOM.SG]</ta>
            <ta e="T648" id="Seg_9360" s="T647">you.NOM</ta>
            <ta e="T649" id="Seg_9361" s="T648">sit-DUR-2SG</ta>
            <ta e="T650" id="Seg_9362" s="T649">burn-IMP.2SG</ta>
            <ta e="T651" id="Seg_9363" s="T650">this.[NOM.SG]</ta>
            <ta e="T652" id="Seg_9364" s="T651">PTCL</ta>
            <ta e="T654" id="Seg_9365" s="T653">burn-INF.LAT</ta>
            <ta e="T655" id="Seg_9366" s="T654">this.[NOM.SG]</ta>
            <ta e="T656" id="Seg_9367" s="T655">this-LAT</ta>
            <ta e="T657" id="Seg_9368" s="T656">axe-INS</ta>
            <ta e="T658" id="Seg_9369" s="T657">head-ACC.3SG</ta>
            <ta e="T659" id="Seg_9370" s="T658">cut-PST.[3SG]</ta>
            <ta e="T660" id="Seg_9371" s="T659">then</ta>
            <ta e="T661" id="Seg_9372" s="T660">child-PL-LAT</ta>
            <ta e="T662" id="Seg_9373" s="T661">kill-RES-PST.[3SG]</ta>
            <ta e="T663" id="Seg_9374" s="T662">then</ta>
            <ta e="T664" id="Seg_9375" s="T663">this</ta>
            <ta e="T666" id="Seg_9376" s="T665">this-PL-LAT</ta>
            <ta e="T667" id="Seg_9377" s="T666">tent-LAT/LOC.3SG</ta>
            <ta e="T669" id="Seg_9378" s="T668">return-MOM-PST.[3SG]</ta>
            <ta e="T670" id="Seg_9379" s="T669">this-PL</ta>
            <ta e="T672" id="Seg_9380" s="T671">woman-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T673" id="Seg_9381" s="T672">NEG.EX.[3SG]</ta>
            <ta e="T674" id="Seg_9382" s="T673">who.[NOM.SG]=INDEF</ta>
            <ta e="T675" id="Seg_9383" s="T674">kill-PST.[3SG]</ta>
            <ta e="T676" id="Seg_9384" s="T675">child-ACC.PL</ta>
            <ta e="T677" id="Seg_9385" s="T676">and</ta>
            <ta e="T678" id="Seg_9386" s="T677">woman-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T680" id="Seg_9387" s="T679">live-PST-3PL</ta>
            <ta e="T681" id="Seg_9388" s="T680">man.[NOM.SG]</ta>
            <ta e="T682" id="Seg_9389" s="T681">woman-3SG-COM</ta>
            <ta e="T683" id="Seg_9390" s="T682">this-PL</ta>
            <ta e="T684" id="Seg_9391" s="T683">what.[NOM.SG]=INDEF</ta>
            <ta e="T685" id="Seg_9392" s="T684">NEG.EX-PST.[3SG]</ta>
            <ta e="T686" id="Seg_9393" s="T685">only</ta>
            <ta e="T687" id="Seg_9394" s="T686">one.[NOM.SG]</ta>
            <ta e="T688" id="Seg_9395" s="T687">cow.[NOM.SG]</ta>
            <ta e="T689" id="Seg_9396" s="T688">this-PL</ta>
            <ta e="T690" id="Seg_9397" s="T689">eat-INF.LAT</ta>
            <ta e="T691" id="Seg_9398" s="T690">NEG.EX.[3SG]</ta>
            <ta e="T692" id="Seg_9399" s="T691">be-PST.[3SG]</ta>
            <ta e="T693" id="Seg_9400" s="T692">then</ta>
            <ta e="T694" id="Seg_9401" s="T693">cow-NOM/GEN/ACC.3SG</ta>
            <ta e="T695" id="Seg_9402" s="T694">cut-MOM-PST.[3SG]</ta>
            <ta e="T696" id="Seg_9403" s="T695">woman-NOM/GEN/ACC.1SG</ta>
            <ta e="T697" id="Seg_9404" s="T696">drive-MOM-PST.[3SG]</ta>
            <ta e="T698" id="Seg_9405" s="T697">go-EP-IMP.2SG</ta>
            <ta e="T699" id="Seg_9406" s="T698">otherwise</ta>
            <ta e="T700" id="Seg_9407" s="T699">I.LAT</ta>
            <ta e="T701" id="Seg_9408" s="T700">self-LAT/LOC.3SG-NOM/GEN/ACC.1SG</ta>
            <ta e="T702" id="Seg_9409" s="T701">few</ta>
            <ta e="T703" id="Seg_9410" s="T702">become-FUT-3SG</ta>
            <ta e="T704" id="Seg_9411" s="T703">this.[NOM.SG]</ta>
            <ta e="T705" id="Seg_9412" s="T704">woman.[NOM.SG]</ta>
            <ta e="T706" id="Seg_9413" s="T705">go-PST.[3SG]</ta>
            <ta e="T707" id="Seg_9414" s="T706">go-PST.[3SG]</ta>
            <ta e="T708" id="Seg_9415" s="T707">more</ta>
            <ta e="T709" id="Seg_9416" s="T708">tent.[NOM.SG]</ta>
            <ta e="T710" id="Seg_9417" s="T709">find-PST.[3SG]</ta>
            <ta e="T711" id="Seg_9418" s="T710">tent-LAT</ta>
            <ta e="T712" id="Seg_9419" s="T711">come-PST.[3SG]</ta>
            <ta e="T713" id="Seg_9420" s="T712">there</ta>
            <ta e="T714" id="Seg_9421" s="T713">fat.[NOM.SG]</ta>
            <ta e="T715" id="Seg_9422" s="T714">lie-DUR.[3SG]</ta>
            <ta e="T716" id="Seg_9423" s="T715">meat.[NOM.SG]</ta>
            <ta e="T717" id="Seg_9424" s="T716">lie-DUR.[3SG]</ta>
            <ta e="T718" id="Seg_9425" s="T717">this.[NOM.SG]</ta>
            <ta e="T719" id="Seg_9426" s="T718">boil-PST.[3SG]</ta>
            <ta e="T720" id="Seg_9427" s="T719">PTCL</ta>
            <ta e="T721" id="Seg_9428" s="T720">eat-PST.[3SG]</ta>
            <ta e="T722" id="Seg_9429" s="T721">hear-PRS-3SG.O</ta>
            <ta e="T723" id="Seg_9430" s="T722">PTCL</ta>
            <ta e="T724" id="Seg_9431" s="T723">who.[NOM.SG]=INDEF</ta>
            <ta e="T725" id="Seg_9432" s="T724">come-PRS.[3SG]</ta>
            <ta e="T726" id="Seg_9433" s="T725">many</ta>
            <ta e="T727" id="Seg_9434" s="T726">sheep.[NOM.SG]</ta>
            <ta e="T729" id="Seg_9435" s="T728">bring-PST.[3SG]</ta>
            <ta e="T730" id="Seg_9436" s="T729">cow-PL</ta>
            <ta e="T731" id="Seg_9437" s="T730">horse-PL</ta>
            <ta e="T732" id="Seg_9438" s="T731">then</ta>
            <ta e="T733" id="Seg_9439" s="T732">come-PST.[3SG]</ta>
            <ta e="T734" id="Seg_9440" s="T733">tent-LAT</ta>
            <ta e="T735" id="Seg_9441" s="T734">say-IPFVZ.[3SG]</ta>
            <ta e="T737" id="Seg_9442" s="T736">open-IMP.2SG.O</ta>
            <ta e="T738" id="Seg_9443" s="T737">door</ta>
            <ta e="T739" id="Seg_9444" s="T738">door</ta>
            <ta e="T740" id="Seg_9445" s="T739">open-MOM-PST.[3SG]</ta>
            <ta e="T741" id="Seg_9446" s="T740">this.[NOM.SG]</ta>
            <ta e="T742" id="Seg_9447" s="T741">come-PST.[3SG]</ta>
            <ta e="T743" id="Seg_9448" s="T742">look-FRQ-CVB</ta>
            <ta e="T744" id="Seg_9449" s="T743">who.[NOM.SG]=INDEF</ta>
            <ta e="T745" id="Seg_9450" s="T744">be-PST.[3SG]</ta>
            <ta e="T746" id="Seg_9451" s="T745">fat.[NOM.SG]</ta>
            <ta e="T747" id="Seg_9452" s="T746">NEG.EX</ta>
            <ta e="T748" id="Seg_9453" s="T747">meat.[NOM.SG]</ta>
            <ta e="T749" id="Seg_9454" s="T748">NEG.EX</ta>
            <ta e="T750" id="Seg_9455" s="T749">bread.[NOM.SG]</ta>
            <ta e="T751" id="Seg_9456" s="T750">NEG.EX</ta>
            <ta e="T752" id="Seg_9457" s="T751">who.[NOM.SG]</ta>
            <ta e="T753" id="Seg_9458" s="T752">here</ta>
            <ta e="T754" id="Seg_9459" s="T753">be-PST.[3SG]</ta>
            <ta e="T755" id="Seg_9460" s="T754">man.[NOM.SG]</ta>
            <ta e="T756" id="Seg_9461" s="T755">so</ta>
            <ta e="T757" id="Seg_9462" s="T756">man-ACC</ta>
            <ta e="T758" id="Seg_9463" s="T757">become-FUT-3SG</ta>
            <ta e="T759" id="Seg_9464" s="T758">woman.[NOM.SG]</ta>
            <ta e="T760" id="Seg_9465" s="T759">so</ta>
            <ta e="T761" id="Seg_9466" s="T760">companion-NOM/GEN/ACC.1SG</ta>
            <ta e="T762" id="Seg_9467" s="T761">become-FUT-3SG</ta>
            <ta e="T763" id="Seg_9468" s="T762">then</ta>
            <ta e="T764" id="Seg_9469" s="T763">this.[NOM.SG]</ta>
            <ta e="T765" id="Seg_9470" s="T764">woman.[NOM.SG]</ta>
            <ta e="T766" id="Seg_9471" s="T765">get.up-PST.[3SG]</ta>
            <ta e="T767" id="Seg_9472" s="T766">come-PST.[3SG]</ta>
            <ta e="T768" id="Seg_9473" s="T767">this.[NOM.SG]</ta>
            <ta e="T769" id="Seg_9474" s="T768">this-ACC</ta>
            <ta e="T770" id="Seg_9475" s="T769">seat-PST.[3SG]</ta>
            <ta e="T771" id="Seg_9476" s="T770">eat-CVB</ta>
            <ta e="T772" id="Seg_9477" s="T771">sit.down-PST-3PL</ta>
            <ta e="T773" id="Seg_9478" s="T772">this.[NOM.SG]</ta>
            <ta e="T774" id="Seg_9479" s="T773">ask-DUR.[3SG]</ta>
            <ta e="T775" id="Seg_9480" s="T774">what.[NOM.SG]</ta>
            <ta e="T776" id="Seg_9481" s="T775">you.NOM</ta>
            <ta e="T777" id="Seg_9482" s="T776">hand-NOM/GEN/ACC.2SG</ta>
            <ta e="T778" id="Seg_9483" s="T777">foot-NOM/GEN/ACC.2SG</ta>
            <ta e="T779" id="Seg_9484" s="T778">NEG.EX</ta>
            <ta e="T780" id="Seg_9485" s="T779">how</ta>
            <ta e="T781" id="Seg_9486" s="T780">you.NOM</ta>
            <ta e="T782" id="Seg_9487" s="T781">go-PRS-2SG</ta>
            <ta e="T783" id="Seg_9488" s="T782">well</ta>
            <ta e="T784" id="Seg_9489" s="T783">so</ta>
            <ta e="T785" id="Seg_9490" s="T784">go-PRS-2SG</ta>
            <ta e="T786" id="Seg_9491" s="T785">you.NOM</ta>
            <ta e="T787" id="Seg_9492" s="T786">pit-LAT</ta>
            <ta e="T788" id="Seg_9493" s="T787">jump-FUT-2SG</ta>
            <ta e="T789" id="Seg_9494" s="T788">jump-FUT-1SG</ta>
            <ta e="T790" id="Seg_9495" s="T789">then</ta>
            <ta e="T791" id="Seg_9496" s="T790">go-PST-3PL</ta>
            <ta e="T792" id="Seg_9497" s="T791">this.[NOM.SG]</ta>
            <ta e="T793" id="Seg_9498" s="T792">jump-PST.[3SG]</ta>
            <ta e="T794" id="Seg_9499" s="T793">this.[NOM.SG]</ta>
            <ta e="T795" id="Seg_9500" s="T794">this-LAT</ta>
            <ta e="T796" id="Seg_9501" s="T795">axe-INS</ta>
            <ta e="T797" id="Seg_9502" s="T796">head-ACC.3SG</ta>
            <ta e="T798" id="Seg_9503" s="T797">PTCL</ta>
            <ta e="T799" id="Seg_9504" s="T798">off-cut-PST.[3SG]</ta>
            <ta e="T800" id="Seg_9505" s="T799">then</ta>
            <ta e="T801" id="Seg_9506" s="T800">PTCL</ta>
            <ta e="T802" id="Seg_9507" s="T801">%brain.[NOM.SG]</ta>
            <ta e="T803" id="Seg_9508" s="T802">take-PST.[3SG]</ta>
            <ta e="T804" id="Seg_9509" s="T803">fat-NOM/GEN/ACC.3SG</ta>
            <ta e="T805" id="Seg_9510" s="T804">take-PST.[3SG]</ta>
            <ta e="T806" id="Seg_9511" s="T805">meat.[NOM.SG]</ta>
            <ta e="T807" id="Seg_9512" s="T806">take-PST.[3SG]</ta>
            <ta e="T808" id="Seg_9513" s="T807">bladder-NOM/GEN/ACC.3SG</ta>
            <ta e="T809" id="Seg_9514" s="T808">put-PST.[3SG]</ta>
            <ta e="T810" id="Seg_9515" s="T809">then</ta>
            <ta e="T811" id="Seg_9516" s="T810">return-MOM-PST.[3SG]</ta>
            <ta e="T812" id="Seg_9517" s="T811">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T813" id="Seg_9518" s="T812">man-LAT/LOC.3SG</ta>
            <ta e="T814" id="Seg_9519" s="T813">tent-LAT</ta>
            <ta e="T817" id="Seg_9520" s="T816">climb-PST.[3SG]</ta>
            <ta e="T818" id="Seg_9521" s="T817">and</ta>
            <ta e="T819" id="Seg_9522" s="T818">man.[NOM.SG]</ta>
            <ta e="T820" id="Seg_9523" s="T819">coal-INS</ta>
            <ta e="T821" id="Seg_9524" s="T820">hand.[NOM.SG]</ta>
            <ta e="T823" id="Seg_9525" s="T822">hand-ACC.3SG</ta>
            <ta e="T825" id="Seg_9526" s="T823">PTCL</ta>
            <ta e="T826" id="Seg_9527" s="T825">hand-ACC.3SG</ta>
            <ta e="T828" id="Seg_9528" s="T827">coal-INS</ta>
            <ta e="T830" id="Seg_9529" s="T828">PTCL</ta>
            <ta e="T831" id="Seg_9530" s="T830">that-ACC</ta>
            <ta e="T832" id="Seg_9531" s="T831">meat-NOM/GEN/ACC.1SG</ta>
            <ta e="T833" id="Seg_9532" s="T832">eat-FUT-1SG</ta>
            <ta e="T834" id="Seg_9533" s="T833">that-ACC</ta>
            <ta e="T835" id="Seg_9534" s="T834">meat-NOM/GEN/ACC.1SG</ta>
            <ta e="T836" id="Seg_9535" s="T835">eat-FUT-1SG</ta>
            <ta e="T837" id="Seg_9536" s="T836">and</ta>
            <ta e="T838" id="Seg_9537" s="T837">this.[NOM.SG]</ta>
            <ta e="T839" id="Seg_9538" s="T838">throw.away-MOM-PST.[3SG]</ta>
            <ta e="T840" id="Seg_9539" s="T839">this-LAT</ta>
            <ta e="T841" id="Seg_9540" s="T840">this.[NOM.SG]</ta>
            <ta e="T842" id="Seg_9541" s="T841">grab-MOM-PST.[3SG]</ta>
            <ta e="T843" id="Seg_9542" s="T842">look</ta>
            <ta e="T844" id="Seg_9543" s="T843">God.[NOM.SG]</ta>
            <ta e="T845" id="Seg_9544" s="T844">I.LAT</ta>
            <ta e="T846" id="Seg_9545" s="T845">give-PST.[3SG]</ta>
            <ta e="T847" id="Seg_9546" s="T846">yes</ta>
            <ta e="T848" id="Seg_9547" s="T847">God.[NOM.SG]</ta>
            <ta e="T849" id="Seg_9548" s="T848">you.DAT</ta>
            <ta e="T850" id="Seg_9549" s="T849">give-FUT-3SG</ta>
            <ta e="T851" id="Seg_9550" s="T850">stone.[NOM.SG]</ta>
            <ta e="T852" id="Seg_9551" s="T851">you.DAT</ta>
            <ta e="T853" id="Seg_9552" s="T852">give-FUT-3SG</ta>
            <ta e="T854" id="Seg_9553" s="T853">God.[NOM.SG]</ta>
            <ta e="T855" id="Seg_9554" s="T854">this-ACC</ta>
            <ta e="T856" id="Seg_9555" s="T855">I.NOM</ta>
            <ta e="T857" id="Seg_9556" s="T856">give-PST-1SG</ta>
            <ta e="T858" id="Seg_9557" s="T857">then</ta>
            <ta e="T859" id="Seg_9558" s="T858">this.[NOM.SG]</ta>
            <ta e="T860" id="Seg_9559" s="T859">eat-PST.[3SG]</ta>
            <ta e="T861" id="Seg_9560" s="T860">go-PST-3PL</ta>
            <ta e="T862" id="Seg_9561" s="T861">there</ta>
            <ta e="T863" id="Seg_9562" s="T862">horse-PL</ta>
            <ta e="T864" id="Seg_9563" s="T863">cow-PL</ta>
            <ta e="T865" id="Seg_9564" s="T864">sheep-PL</ta>
            <ta e="T866" id="Seg_9565" s="T865">come-PST-3PL</ta>
            <ta e="T867" id="Seg_9566" s="T866">there</ta>
            <ta e="T868" id="Seg_9567" s="T867">this.[NOM.SG]</ta>
            <ta e="T869" id="Seg_9568" s="T868">say-IPFVZ.[3SG]</ta>
            <ta e="T870" id="Seg_9569" s="T869">fart-FRQ-INF.LAT</ta>
            <ta e="T871" id="Seg_9570" s="T870">one.wants</ta>
            <ta e="T872" id="Seg_9571" s="T871">NEG.AUX-IMP.2SG</ta>
            <ta e="T873" id="Seg_9572" s="T872">fart-FRQ-EP-CNG</ta>
            <ta e="T874" id="Seg_9573" s="T873">otherwise</ta>
            <ta e="T875" id="Seg_9574" s="T874">all</ta>
            <ta e="T876" id="Seg_9575" s="T875">run-MOM-FUT-3PL</ta>
            <ta e="T877" id="Seg_9576" s="T876">this.[NOM.SG]</ta>
            <ta e="T881" id="Seg_9577" s="T880">fart-FRQ-PST.[3SG]</ta>
            <ta e="T882" id="Seg_9578" s="T881">and</ta>
            <ta e="T883" id="Seg_9579" s="T882">PTCL</ta>
            <ta e="T884" id="Seg_9580" s="T883">horse-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T885" id="Seg_9581" s="T884">sheep-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T886" id="Seg_9582" s="T885">and</ta>
            <ta e="T887" id="Seg_9583" s="T886">cow-PL</ta>
            <ta e="T888" id="Seg_9584" s="T887">run-MOM-PST-3PL</ta>
            <ta e="T889" id="Seg_9585" s="T888">this.[NOM.SG]</ta>
            <ta e="T890" id="Seg_9586" s="T889">INCH</ta>
            <ta e="T892" id="Seg_9587" s="T890">milk-INS</ta>
            <ta e="T895" id="Seg_9588" s="T894">milk-INS</ta>
            <ta e="T896" id="Seg_9589" s="T895">pour-INF.LAT</ta>
            <ta e="T897" id="Seg_9590" s="T896">this-PL-LAT</ta>
            <ta e="T898" id="Seg_9591" s="T897">sheep.[NOM.SG]</ta>
            <ta e="T899" id="Seg_9592" s="T898">you.PL.NOM</ta>
            <ta e="T900" id="Seg_9593" s="T899">goat.[NOM.SG]</ta>
            <ta e="T901" id="Seg_9594" s="T900">become-CVB</ta>
            <ta e="T902" id="Seg_9595" s="T901">cow-PL</ta>
            <ta e="T903" id="Seg_9596" s="T902">you.PL.NOM</ta>
            <ta e="T904" id="Seg_9597" s="T903">you.PL.NOM</ta>
            <ta e="T905" id="Seg_9598" s="T904">moose.[NOM.SG]</ta>
            <ta e="T906" id="Seg_9599" s="T905">become-IMP.2PL</ta>
            <ta e="T907" id="Seg_9600" s="T906">and</ta>
            <ta e="T908" id="Seg_9601" s="T907">you.PL.NOM</ta>
            <ta e="T910" id="Seg_9602" s="T908">horse-PL</ta>
            <ta e="T911" id="Seg_9603" s="T910">forget-MOM-PST-1SG</ta>
            <ta e="T915" id="Seg_9604" s="T914">say-PST.[3SG]</ta>
            <ta e="T916" id="Seg_9605" s="T915">JUSS</ta>
            <ta e="T919" id="Seg_9606" s="T918">then</ta>
            <ta e="T920" id="Seg_9607" s="T919">woman.[NOM.SG]</ta>
            <ta e="T921" id="Seg_9608" s="T920">and</ta>
            <ta e="T922" id="Seg_9609" s="T921">man.[NOM.SG]</ta>
            <ta e="T923" id="Seg_9610" s="T922">leave-MOM-PST.[3SG]</ta>
            <ta e="T924" id="Seg_9611" s="T923">what.[NOM.SG]=INDEF</ta>
            <ta e="T925" id="Seg_9612" s="T924">again</ta>
            <ta e="T926" id="Seg_9613" s="T925">NEG.EX</ta>
            <ta e="T927" id="Seg_9614" s="T926">this-PL-LAT</ta>
            <ta e="T928" id="Seg_9615" s="T927">PTCL</ta>
            <ta e="T1073" id="Seg_9616" s="T928">go-CVB</ta>
            <ta e="T1071" id="Seg_9617" s="T1073">disappear-PST-3PL</ta>
            <ta e="T930" id="Seg_9618" s="T929">live-PST.[3SG]</ta>
            <ta e="T931" id="Seg_9619" s="T930">man.[NOM.SG]</ta>
            <ta e="T932" id="Seg_9620" s="T931">there</ta>
            <ta e="T933" id="Seg_9621" s="T932">three.[NOM.SG]</ta>
            <ta e="T934" id="Seg_9622" s="T933">daughter-NOM/GEN.3SG</ta>
            <ta e="T935" id="Seg_9623" s="T934">be-PST.[3SG]</ta>
            <ta e="T936" id="Seg_9624" s="T935">this-ACC</ta>
            <ta e="T937" id="Seg_9625" s="T936">call-PST-3PL</ta>
            <ta e="T1075" id="Seg_9626" s="T937">Aginskoe</ta>
            <ta e="T938" id="Seg_9627" s="T1075">settlement-LAT</ta>
            <ta e="T939" id="Seg_9628" s="T938">so.that</ta>
            <ta e="T940" id="Seg_9629" s="T939">come-PST.[3SG]</ta>
            <ta e="T941" id="Seg_9630" s="T940">and</ta>
            <ta e="T942" id="Seg_9631" s="T941">big.[NOM.SG]</ta>
            <ta e="T943" id="Seg_9632" s="T942">daughter-NOM/GEN.3SG</ta>
            <ta e="T944" id="Seg_9633" s="T943">I.NOM</ta>
            <ta e="T945" id="Seg_9634" s="T944">go-FUT-1SG</ta>
            <ta e="T946" id="Seg_9635" s="T945">hair-ACC.3SG</ta>
            <ta e="T948" id="Seg_9636" s="T947">cut-PST.[3SG]</ta>
            <ta e="T949" id="Seg_9637" s="T948">pants.[NOM.SG]</ta>
            <ta e="T950" id="Seg_9638" s="T949">dress-PST.[3SG]</ta>
            <ta e="T951" id="Seg_9639" s="T950">shirt.[NOM.SG]</ta>
            <ta e="T952" id="Seg_9640" s="T951">dress-PST.[3SG]</ta>
            <ta e="T953" id="Seg_9641" s="T952">cap.[NOM.SG]</ta>
            <ta e="T954" id="Seg_9642" s="T953">dress-PST.[3SG]</ta>
            <ta e="T955" id="Seg_9643" s="T954">go-PST.[3SG]</ta>
            <ta e="T956" id="Seg_9644" s="T955">and</ta>
            <ta e="T957" id="Seg_9645" s="T956">father-NOM/GEN.3SG</ta>
            <ta e="T958" id="Seg_9646" s="T957">bear-INS</ta>
            <ta e="T959" id="Seg_9647" s="T958">make-PST.[3SG]</ta>
            <ta e="T960" id="Seg_9648" s="T959">and</ta>
            <ta e="T961" id="Seg_9649" s="T960">this-LAT</ta>
            <ta e="T962" id="Seg_9650" s="T961">come-PRS.[3SG]</ta>
            <ta e="T963" id="Seg_9651" s="T962">this.[NOM.SG]</ta>
            <ta e="T964" id="Seg_9652" s="T963">PTCL</ta>
            <ta e="T965" id="Seg_9653" s="T964">frighten-MOM-PST.[3SG]</ta>
            <ta e="T966" id="Seg_9654" s="T965">and</ta>
            <ta e="T967" id="Seg_9655" s="T966">tent-LAT/LOC.3SG</ta>
            <ta e="T968" id="Seg_9656" s="T967">run-MOM-PST.[3SG]</ta>
            <ta e="T969" id="Seg_9657" s="T968">this.[NOM.SG]</ta>
            <ta e="T970" id="Seg_9658" s="T969">come-PST.[3SG]</ta>
            <ta e="T971" id="Seg_9659" s="T970">what.[NOM.SG]</ta>
            <ta e="T972" id="Seg_9660" s="T971">NEG</ta>
            <ta e="T973" id="Seg_9661" s="T972">go-PST-1SG</ta>
            <ta e="T975" id="Seg_9662" s="T974">there</ta>
            <ta e="T976" id="Seg_9663" s="T975">bear.[NOM.SG]</ta>
            <ta e="T977" id="Seg_9664" s="T976">I.NOM</ta>
            <ta e="T978" id="Seg_9665" s="T977">frighten-MOM-PST-1SG</ta>
            <ta e="T979" id="Seg_9666" s="T978">tent-LAT/LOC.3SG</ta>
            <ta e="T980" id="Seg_9667" s="T979">come-PST-1SG</ta>
            <ta e="T981" id="Seg_9668" s="T980">then</ta>
            <ta e="T982" id="Seg_9669" s="T981">another.[NOM.SG]</ta>
            <ta e="T983" id="Seg_9670" s="T982">girl.[NOM.SG]</ta>
            <ta e="T984" id="Seg_9671" s="T983">I.NOM</ta>
            <ta e="T985" id="Seg_9672" s="T984">go-FUT-1SG</ta>
            <ta e="T986" id="Seg_9673" s="T985">well</ta>
            <ta e="T987" id="Seg_9674" s="T986">go-EP-IMP.2SG</ta>
            <ta e="T988" id="Seg_9675" s="T987">dress-PST.[3SG]</ta>
            <ta e="T989" id="Seg_9676" s="T988">pants-PL</ta>
            <ta e="T990" id="Seg_9677" s="T989">shirt.[NOM.SG]</ta>
            <ta e="T991" id="Seg_9678" s="T990">dress-PST.[3SG]</ta>
            <ta e="T992" id="Seg_9679" s="T991">hair-ACC.3SG</ta>
            <ta e="T993" id="Seg_9680" s="T992">cut-PST.[3SG]</ta>
            <ta e="T994" id="Seg_9681" s="T993">cap-ACC.3SG</ta>
            <ta e="T995" id="Seg_9682" s="T994">dress-PST.[3SG]</ta>
            <ta e="T996" id="Seg_9683" s="T995">go-PST.[3SG]</ta>
            <ta e="T997" id="Seg_9684" s="T996">then</ta>
            <ta e="T998" id="Seg_9685" s="T997">father-NOM/GEN.3SG</ta>
            <ta e="T999" id="Seg_9686" s="T998">become-RES-PST.[3SG]</ta>
            <ta e="T1000" id="Seg_9687" s="T999">bear-INS</ta>
            <ta e="T1001" id="Seg_9688" s="T1000">go-PST.[3SG]</ta>
            <ta e="T1002" id="Seg_9689" s="T1001">this.[NOM.SG]</ta>
            <ta e="T1003" id="Seg_9690" s="T1002">PTCL</ta>
            <ta e="T1004" id="Seg_9691" s="T1003">frighten-MOM-PST.[3SG]</ta>
            <ta e="T1005" id="Seg_9692" s="T1004">tent-LAT/LOC.3SG</ta>
            <ta e="T1006" id="Seg_9693" s="T1005">come-PST.[3SG]</ta>
            <ta e="T1007" id="Seg_9694" s="T1006">what.[NOM.SG]</ta>
            <ta e="T1008" id="Seg_9695" s="T1007">come-PST-2SG</ta>
            <ta e="T1009" id="Seg_9696" s="T1008">and</ta>
            <ta e="T1010" id="Seg_9697" s="T1009">there</ta>
            <ta e="T1011" id="Seg_9698" s="T1010">bear.[NOM.SG]</ta>
            <ta e="T1012" id="Seg_9699" s="T1011">come-PRS.[3SG]</ta>
            <ta e="T1013" id="Seg_9700" s="T1012">I.NOM</ta>
            <ta e="T1014" id="Seg_9701" s="T1013">frighten-MOM-PST-1SG</ta>
            <ta e="T1015" id="Seg_9702" s="T1014">tent-LAT/LOC.3SG</ta>
            <ta e="T1016" id="Seg_9703" s="T1015">run-MOM-PST-1SG</ta>
            <ta e="T1017" id="Seg_9704" s="T1016">enough</ta>
            <ta e="T1019" id="Seg_9705" s="T1018">then</ta>
            <ta e="T1020" id="Seg_9706" s="T1019">small.[NOM.SG]</ta>
            <ta e="T1021" id="Seg_9707" s="T1020">girl.[NOM.SG]</ta>
            <ta e="T1022" id="Seg_9708" s="T1021">I.NOM</ta>
            <ta e="T1023" id="Seg_9709" s="T1022">go-FUT-1SG</ta>
            <ta e="T1024" id="Seg_9710" s="T1023">well</ta>
            <ta e="T1025" id="Seg_9711" s="T1024">go-EP-IMP.2SG</ta>
            <ta e="T1026" id="Seg_9712" s="T1025">also</ta>
            <ta e="T1027" id="Seg_9713" s="T1026">so</ta>
            <ta e="T1028" id="Seg_9714" s="T1027">PTCL</ta>
            <ta e="T1029" id="Seg_9715" s="T1028">hair-ACC.3SG</ta>
            <ta e="T1030" id="Seg_9716" s="T1029">cut-MOM-PST.[3SG]</ta>
            <ta e="T1031" id="Seg_9717" s="T1030">pants-PL</ta>
            <ta e="T1032" id="Seg_9718" s="T1031">dress-PST.[3SG]</ta>
            <ta e="T1033" id="Seg_9719" s="T1032">shirt.[NOM.SG]</ta>
            <ta e="T1034" id="Seg_9720" s="T1033">dress-PST.[3SG]</ta>
            <ta e="T1035" id="Seg_9721" s="T1034">and</ta>
            <ta e="T1036" id="Seg_9722" s="T1035">cap.[NOM.SG]</ta>
            <ta e="T1037" id="Seg_9723" s="T1036">dress-PST.[3SG]</ta>
            <ta e="T1038" id="Seg_9724" s="T1037">go-PST.[3SG]</ta>
            <ta e="T1039" id="Seg_9725" s="T1038">walk-PRS.[3SG]</ta>
            <ta e="T1040" id="Seg_9726" s="T1039">walk-PRS.[3SG]</ta>
            <ta e="T1041" id="Seg_9727" s="T1040">bear.[NOM.SG]</ta>
            <ta e="T1042" id="Seg_9728" s="T1041">come-PRS.[3SG]</ta>
            <ta e="T1043" id="Seg_9729" s="T1042">this.[NOM.SG]</ta>
            <ta e="T1044" id="Seg_9730" s="T1043">arrow-INS</ta>
            <ta e="T1045" id="Seg_9731" s="T1044">PTCL</ta>
            <ta e="T1046" id="Seg_9732" s="T1045">this-ACC</ta>
            <ta e="T1048" id="Seg_9733" s="T1047">eye-LAT/LOC.3SG</ta>
            <ta e="T1049" id="Seg_9734" s="T1048">eye-NOM/GEN.3SG</ta>
            <ta e="T1051" id="Seg_9735" s="T1049">PTCL</ta>
            <ta e="T1052" id="Seg_9736" s="T1051">this.[NOM.SG]</ta>
            <ta e="T1053" id="Seg_9737" s="T1052">arrow-INS</ta>
            <ta e="T1054" id="Seg_9738" s="T1053">eye-LAT/LOC.3SG</ta>
            <ta e="T1055" id="Seg_9739" s="T1054">hit-MULT-PST.[3SG]</ta>
            <ta e="T1056" id="Seg_9740" s="T1055">and</ta>
            <ta e="T1057" id="Seg_9741" s="T1056">eye-NOM/GEN.3SG</ta>
            <ta e="T1059" id="Seg_9742" s="T1058">NEG.EX-MOM-PST.[3SG]</ta>
            <ta e="T1060" id="Seg_9743" s="T1059">then</ta>
            <ta e="T1061" id="Seg_9744" s="T1060">go-PST.[3SG]</ta>
            <ta e="T1062" id="Seg_9745" s="T1061">go-PST.[3SG]</ta>
            <ta e="T1063" id="Seg_9746" s="T1062">come-PRS.[3SG]</ta>
            <ta e="T1064" id="Seg_9747" s="T1063">woman.[NOM.SG]</ta>
            <ta e="T1065" id="Seg_9748" s="T1064">live-DUR.[3SG]</ta>
            <ta e="T1066" id="Seg_9749" s="T1065">and</ta>
            <ta e="T1067" id="Seg_9750" s="T1066">woman-GEN</ta>
            <ta e="T1068" id="Seg_9751" s="T1067">son-NOM/GEN.3SG</ta>
            <ta e="T1069" id="Seg_9752" s="T1068">live-DUR.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PKZ">
            <ta e="T1" id="Seg_9753" s="T0">мы.NOM</ta>
            <ta e="T2" id="Seg_9754" s="T1">когда</ta>
            <ta e="T3" id="Seg_9755" s="T2">прийти-PST-1PL</ta>
            <ta e="T4" id="Seg_9756" s="T3">Абалаково-LAT</ta>
            <ta e="T5" id="Seg_9757" s="T4">мы.LAT</ta>
            <ta e="T6" id="Seg_9758" s="T5">хороший</ta>
            <ta e="T7" id="Seg_9759" s="T6">быть-PST.[3SG]</ta>
            <ta e="T8" id="Seg_9760" s="T7">люди.[NOM.SG]</ta>
            <ta e="T9" id="Seg_9761" s="T8">ночевать-PST-1PL</ta>
            <ta e="T10" id="Seg_9762" s="T9">хороший</ta>
            <ta e="T11" id="Seg_9763" s="T10">тогда</ta>
            <ta e="T12" id="Seg_9764" s="T11">идти-PST-1PL</ta>
            <ta e="T13" id="Seg_9765" s="T12">идти-PST-1PL</ta>
            <ta e="T16" id="Seg_9766" s="T15">какой-PL</ta>
            <ta e="T17" id="Seg_9767" s="T16">мужчина-PL</ta>
            <ta e="T18" id="Seg_9768" s="T17">видеть-PST-1PL</ta>
            <ta e="T19" id="Seg_9769" s="T18">говорить-PST-1PL</ta>
            <ta e="T20" id="Seg_9770" s="T19">этот-PL</ta>
            <ta e="T21" id="Seg_9771" s="T20">что.[NOM.SG]=INDEF</ta>
            <ta e="T22" id="Seg_9772" s="T21">NEG</ta>
            <ta e="T23" id="Seg_9773" s="T22">знать-3PL</ta>
            <ta e="T24" id="Seg_9774" s="T23">камасинец-PL</ta>
            <ta e="T25" id="Seg_9775" s="T24">камасинец-PL</ta>
            <ta e="T26" id="Seg_9776" s="T25">люди.[NOM.SG]</ta>
            <ta e="T27" id="Seg_9777" s="T26">как</ta>
            <ta e="T28" id="Seg_9778" s="T27">этот-PL</ta>
            <ta e="T29" id="Seg_9779" s="T28">жить-DUR-3PL</ta>
            <ta e="T30" id="Seg_9780" s="T29">мы.NOM</ta>
            <ta e="T31" id="Seg_9781" s="T30">думать-MOM-PST-1PL</ta>
            <ta e="T32" id="Seg_9782" s="T31">опять</ta>
            <ta e="T33" id="Seg_9783" s="T32">вернуться-INF.LAT</ta>
            <ta e="T34" id="Seg_9784" s="T33">чум-LAT/LOC.3SG</ta>
            <ta e="T37" id="Seg_9785" s="T36">один.[NOM.SG]</ta>
            <ta e="T38" id="Seg_9786" s="T37">девушка.[NOM.SG]</ta>
            <ta e="T39" id="Seg_9787" s="T38">Эля.[NOM.SG]</ta>
            <ta e="T40" id="Seg_9788" s="T39">пойти-PST.[3SG]</ta>
            <ta e="T41" id="Seg_9789" s="T40">молоко.[NOM.SG]</ta>
            <ta e="T42" id="Seg_9790" s="T41">смотреть-FRQ-IPFVZ-INF.LAT</ta>
            <ta e="T43" id="Seg_9791" s="T42">и</ta>
            <ta e="T45" id="Seg_9792" s="T44">прийти-PST.[3SG]</ta>
            <ta e="T46" id="Seg_9793" s="T45">женщина-LAT</ta>
            <ta e="T47" id="Seg_9794" s="T46">один.[NOM.SG]</ta>
            <ta e="T48" id="Seg_9795" s="T47">женщина-LAT</ta>
            <ta e="T49" id="Seg_9796" s="T48">этот.[NOM.SG]</ta>
            <ta e="T50" id="Seg_9797" s="T49">ну</ta>
            <ta e="T51" id="Seg_9798" s="T50">быть-PRS.[3SG]</ta>
            <ta e="T52" id="Seg_9799" s="T51">этот.[NOM.SG]</ta>
            <ta e="T53" id="Seg_9800" s="T52">женщина.[NOM.SG]</ta>
            <ta e="T54" id="Seg_9801" s="T53">этот.[NOM.SG]</ta>
            <ta e="T55" id="Seg_9802" s="T54">сказать-IPFVZ.[3SG]</ta>
            <ta e="T57" id="Seg_9803" s="T56">лицо.[NOM.SG]</ta>
            <ta e="T64" id="Seg_9804" s="T63">волосы.[NOM.SG]</ta>
            <ta e="T65" id="Seg_9805" s="T64">этот.[NOM.SG]</ta>
            <ta e="T66" id="Seg_9806" s="T65">PTCL</ta>
            <ta e="T67" id="Seg_9807" s="T66">вернуться-MOM-PST.[3SG]</ta>
            <ta e="T68" id="Seg_9808" s="T67">PTCL</ta>
            <ta e="T69" id="Seg_9809" s="T68">сердце-NOM/GEN/ACC.3SG</ta>
            <ta e="T70" id="Seg_9810" s="T69">очень</ta>
            <ta e="T71" id="Seg_9811" s="T70">хороший</ta>
            <ta e="T74" id="Seg_9812" s="T73">сказать-PST.[3SG]</ta>
            <ta e="T75" id="Seg_9813" s="T74">здесь</ta>
            <ta e="T76" id="Seg_9814" s="T75">женщина.[NOM.SG]</ta>
            <ta e="T77" id="Seg_9815" s="T76">быть-PRS.[3SG]</ta>
            <ta e="T78" id="Seg_9816" s="T77">камасинец-TR-FUT</ta>
            <ta e="T79" id="Seg_9817" s="T78">мочь-PRS.[3SG]</ta>
            <ta e="T80" id="Seg_9818" s="T79">говорить-INF.LAT</ta>
            <ta e="T82" id="Seg_9819" s="T81">этот.[NOM.SG]</ta>
            <ta e="T83" id="Seg_9820" s="T82">я.LAT</ta>
            <ta e="T84" id="Seg_9821" s="T83">сказать-PST.[3SG]</ta>
            <ta e="T86" id="Seg_9822" s="T85">девушка.[NOM.SG]</ta>
            <ta e="T87" id="Seg_9823" s="T86">быть-PRS.[3SG]</ta>
            <ta e="T88" id="Seg_9824" s="T87">а</ta>
            <ta e="T90" id="Seg_9825" s="T89">мальчик.[NOM.SG]</ta>
            <ta e="T92" id="Seg_9826" s="T90">тогда</ta>
            <ta e="T94" id="Seg_9827" s="T93">тогда</ta>
            <ta e="T95" id="Seg_9828" s="T94">мы.NOM</ta>
            <ta e="T96" id="Seg_9829" s="T95">остаться-MOM-PST-1PL</ta>
            <ta e="T97" id="Seg_9830" s="T96">этот.[NOM.SG]</ta>
            <ta e="T98" id="Seg_9831" s="T97">женщина-LAT</ta>
            <ta e="T100" id="Seg_9832" s="T99">идти-PST-1PL</ta>
            <ta e="T101" id="Seg_9833" s="T100">говорить-INF.LAT</ta>
            <ta e="T102" id="Seg_9834" s="T101">три.[NOM.SG]</ta>
            <ta e="T103" id="Seg_9835" s="T102">десять.[NOM.SG]</ta>
            <ta e="T104" id="Seg_9836" s="T103">день.[NOM.SG]</ta>
            <ta e="T105" id="Seg_9837" s="T104">говорить-PST-1PL</ta>
            <ta e="T106" id="Seg_9838" s="T105">весь</ta>
            <ta e="T107" id="Seg_9839" s="T106">что.[NOM.SG]</ta>
            <ta e="T108" id="Seg_9840" s="T107">сказать-PST.[3SG]</ta>
            <ta e="T109" id="Seg_9841" s="T108">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T111" id="Seg_9842" s="T110">язык-3SG-INS</ta>
            <ta e="T112" id="Seg_9843" s="T111">так</ta>
            <ta e="T113" id="Seg_9844" s="T112">сказать-PST.[3SG]</ta>
            <ta e="T114" id="Seg_9845" s="T113">я.NOM</ta>
            <ta e="T115" id="Seg_9846" s="T114">тетка-NOM/GEN/ACC.1SG</ta>
            <ta e="T116" id="Seg_9847" s="T115">умереть-RES-PST.[3SG]</ta>
            <ta e="T117" id="Seg_9848" s="T116">два.[NOM.SG]</ta>
            <ta e="T118" id="Seg_9849" s="T117">десять.[NOM.SG]</ta>
            <ta e="T119" id="Seg_9850" s="T118">год.[NOM.SG]</ta>
            <ta e="T120" id="Seg_9851" s="T119">пойти-PST.[3SG]</ta>
            <ta e="T121" id="Seg_9852" s="T120">кто-COM-3SG</ta>
            <ta e="T122" id="Seg_9853" s="T121">NEG</ta>
            <ta e="T123" id="Seg_9854" s="T122">говорить-PRS-1SG</ta>
            <ta e="T125" id="Seg_9855" s="T124">этот-LAT</ta>
            <ta e="T126" id="Seg_9856" s="T125">сейчас</ta>
            <ta e="T128" id="Seg_9857" s="T127">семь.[NOM.SG]</ta>
            <ta e="T129" id="Seg_9858" s="T128">год.[NOM.SG]</ta>
            <ta e="T130" id="Seg_9859" s="T129">этот.[NOM.SG]</ta>
            <ta e="T131" id="Seg_9860" s="T130">очень</ta>
            <ta e="T132" id="Seg_9861" s="T131">хороший</ta>
            <ta e="T133" id="Seg_9862" s="T132">женщина.[NOM.SG]</ta>
            <ta e="T134" id="Seg_9863" s="T133">весь</ta>
            <ta e="T135" id="Seg_9864" s="T134">что-NOM/GEN.3SG</ta>
            <ta e="T136" id="Seg_9865" s="T135">быть-PRS.[3SG]</ta>
            <ta e="T137" id="Seg_9866" s="T136">съесть-INF.LAT</ta>
            <ta e="T138" id="Seg_9867" s="T137">дать-PRS.[3SG]</ta>
            <ta e="T139" id="Seg_9868" s="T138">а</ta>
            <ta e="T140" id="Seg_9869" s="T139">NEG</ta>
            <ta e="T141" id="Seg_9870" s="T140">съесть-FUT-2SG</ta>
            <ta e="T142" id="Seg_9871" s="T141">так</ta>
            <ta e="T143" id="Seg_9872" s="T142">этот.[NOM.SG]</ta>
            <ta e="T144" id="Seg_9873" s="T143">PTCL</ta>
            <ta e="T145" id="Seg_9874" s="T144">рассердиться-DUR.[3SG]</ta>
            <ta e="T148" id="Seg_9875" s="T147">тогда</ta>
            <ta e="T149" id="Seg_9876" s="T148">этот.[NOM.SG]</ta>
            <ta e="T150" id="Seg_9877" s="T149">всегда</ta>
            <ta e="T151" id="Seg_9878" s="T150">чум-LAT/LOC.3SG</ta>
            <ta e="T152" id="Seg_9879" s="T151">много</ta>
            <ta e="T153" id="Seg_9880" s="T152">работать-PRS.[3SG]</ta>
            <ta e="T154" id="Seg_9881" s="T153">NEG</ta>
            <ta e="T155" id="Seg_9882" s="T154">трудный.[NOM.SG]</ta>
            <ta e="T156" id="Seg_9883" s="T155">нога-LAT/LOC.3SG</ta>
            <ta e="T1074" id="Seg_9884" s="T157">Агинское</ta>
            <ta e="T158" id="Seg_9885" s="T1074">поселение-LAT</ta>
            <ta e="T159" id="Seg_9886" s="T158">десять.[NOM.SG]</ta>
            <ta e="T161" id="Seg_9887" s="T160">десять.[NOM.SG]</ta>
            <ta e="T162" id="Seg_9888" s="T161">два.[NOM.SG]</ta>
            <ta e="T163" id="Seg_9889" s="T162">пять.[NOM.SG]</ta>
            <ta e="T164" id="Seg_9890" s="T163">этот.[NOM.SG]</ta>
            <ta e="T165" id="Seg_9891" s="T164">PTCL</ta>
            <ta e="T167" id="Seg_9892" s="T166">пойти-PRS.[3SG]</ta>
            <ta e="T168" id="Seg_9893" s="T167">нога-INS</ta>
            <ta e="T169" id="Seg_9894" s="T168">а</ta>
            <ta e="T170" id="Seg_9895" s="T169">машина-PL</ta>
            <ta e="T171" id="Seg_9896" s="T170">идти-PRS-3PL</ta>
            <ta e="T172" id="Seg_9897" s="T171">этот.[NOM.SG]</ta>
            <ta e="T173" id="Seg_9898" s="T172">там</ta>
            <ta e="T174" id="Seg_9899" s="T173">NEG</ta>
            <ta e="T175" id="Seg_9900" s="T174">сидеть-PRS.[3SG]</ta>
            <ta e="T176" id="Seg_9901" s="T175">а</ta>
            <ta e="T177" id="Seg_9902" s="T176">пешком-LOC.ADV</ta>
            <ta e="T180" id="Seg_9903" s="T179">пойти-FUT-3SG</ta>
            <ta e="T183" id="Seg_9904" s="T182">я.NOM</ta>
            <ta e="T184" id="Seg_9905" s="T183">нога-INS</ta>
            <ta e="T185" id="Seg_9906" s="T184">лучше</ta>
            <ta e="T186" id="Seg_9907" s="T185">пойти-FUT-1SG</ta>
            <ta e="T187" id="Seg_9908" s="T186">дорога.[NOM.SG]</ta>
            <ta e="T189" id="Seg_9909" s="T188">а</ta>
            <ta e="T190" id="Seg_9910" s="T189">машина-3PL-INS</ta>
            <ta e="T191" id="Seg_9911" s="T190">очень</ta>
            <ta e="T192" id="Seg_9912" s="T191">далеко-LAT.ADV</ta>
            <ta e="T193" id="Seg_9913" s="T192">пойти-INF.LAT</ta>
            <ta e="T194" id="Seg_9914" s="T193">и</ta>
            <ta e="T195" id="Seg_9915" s="T194">PTCL</ta>
            <ta e="T196" id="Seg_9916" s="T195">дрожать-DUR.[3SG]</ta>
            <ta e="T198" id="Seg_9917" s="T197">какой</ta>
            <ta e="T200" id="Seg_9918" s="T199">камасинец-PL-LOC</ta>
            <ta e="T201" id="Seg_9919" s="T200">кто.[NOM.SG]</ta>
            <ta e="T202" id="Seg_9920" s="T201">еще</ta>
            <ta e="T203" id="Seg_9921" s="T202">мочь-PRS.[3SG]</ta>
            <ta e="T204" id="Seg_9922" s="T203">говорить-INF.LAT</ta>
            <ta e="T205" id="Seg_9923" s="T204">или</ta>
            <ta e="T206" id="Seg_9924" s="T205">NEG</ta>
            <ta e="T208" id="Seg_9925" s="T207">снаружи</ta>
            <ta e="T209" id="Seg_9926" s="T208">Абалаково-GEN</ta>
            <ta e="T210" id="Seg_9927" s="T209">ребенок-PL-LAT</ta>
            <ta e="T211" id="Seg_9928" s="T210">можно</ta>
            <ta e="T212" id="Seg_9929" s="T211">найти-INF.LAT</ta>
            <ta e="T213" id="Seg_9930" s="T212">снег.[NOM.SG]</ta>
            <ta e="T214" id="Seg_9931" s="T213">голова-NOM/GEN.3SG</ta>
            <ta e="T215" id="Seg_9932" s="T214">глаз-NOM/GEN.3SG</ta>
            <ta e="T216" id="Seg_9933" s="T215">черный.[NOM.SG]</ta>
            <ta e="T219" id="Seg_9934" s="T218">русский</ta>
            <ta e="T220" id="Seg_9935" s="T219">кровь.[NOM.SG]</ta>
            <ta e="T221" id="Seg_9936" s="T220">быть-PRS.[3SG]</ta>
            <ta e="T222" id="Seg_9937" s="T221">и</ta>
            <ta e="T223" id="Seg_9938" s="T222">кровь.[NOM.SG]</ta>
            <ta e="T224" id="Seg_9939" s="T223">быть-PRS.[3SG]</ta>
            <ta e="T225" id="Seg_9940" s="T224">камасинец-PL</ta>
            <ta e="T227" id="Seg_9941" s="T226">какой</ta>
            <ta e="T228" id="Seg_9942" s="T227">PTCL</ta>
            <ta e="T229" id="Seg_9943" s="T228">расти-DUR-3PL</ta>
            <ta e="T230" id="Seg_9944" s="T229">ли</ta>
            <ta e="T231" id="Seg_9945" s="T230">пойти-PST-3PL</ta>
            <ta e="T232" id="Seg_9946" s="T231">работать-INF.LAT</ta>
            <ta e="T233" id="Seg_9947" s="T232">там_здесь</ta>
            <ta e="T234" id="Seg_9948" s="T233">вот</ta>
            <ta e="T239" id="Seg_9949" s="T238">а</ta>
            <ta e="T240" id="Seg_9950" s="T239">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T241" id="Seg_9951" s="T240">язык-NOM/GEN.3SG</ta>
            <ta e="T242" id="Seg_9952" s="T241">NEG</ta>
            <ta e="T243" id="Seg_9953" s="T242">знать-3SG.O</ta>
            <ta e="T245" id="Seg_9954" s="T244">только</ta>
            <ta e="T246" id="Seg_9955" s="T245">этот.[NOM.SG]</ta>
            <ta e="T247" id="Seg_9956" s="T246">камасинец-PL</ta>
            <ta e="T248" id="Seg_9957" s="T247">язык-NOM/GEN.3SG</ta>
            <ta e="T249" id="Seg_9958" s="T248">один.[NOM.SG]</ta>
            <ta e="T250" id="Seg_9959" s="T249">женщина.[NOM.SG]</ta>
            <ta e="T251" id="Seg_9960" s="T250">знать-3SG.O</ta>
            <ta e="T252" id="Seg_9961" s="T251">а</ta>
            <ta e="T253" id="Seg_9962" s="T252">еще</ta>
            <ta e="T254" id="Seg_9963" s="T253">кто.[NOM.SG]=INDEF</ta>
            <ta e="T255" id="Seg_9964" s="T254">NEG</ta>
            <ta e="T256" id="Seg_9965" s="T255">знать.[3SG]</ta>
            <ta e="T257" id="Seg_9966" s="T256">этот.[NOM.SG]</ta>
            <ta e="T258" id="Seg_9967" s="T257">язык-NOM/GEN/ACC.1SG</ta>
            <ta e="T260" id="Seg_9968" s="T259">ты.NOM</ta>
            <ta e="T261" id="Seg_9969" s="T260">я.LAT</ta>
            <ta e="T262" id="Seg_9970" s="T261">лгать-PST-2SG</ta>
            <ta e="T263" id="Seg_9971" s="T262">ты.NOM</ta>
            <ta e="T264" id="Seg_9972" s="T263">я.LAT</ta>
            <ta e="T265" id="Seg_9973" s="T264">приманивать-MOM-PST-2SG</ta>
            <ta e="T266" id="Seg_9974" s="T265">тогда</ta>
            <ta e="T267" id="Seg_9975" s="T266">спрятаться-RES-PST-1SG</ta>
            <ta e="T268" id="Seg_9976" s="T267">а</ta>
            <ta e="T269" id="Seg_9977" s="T268">я.NOM</ta>
            <ta e="T270" id="Seg_9978" s="T269">ты.DAT</ta>
            <ta e="T271" id="Seg_9979" s="T270">смотреть-FRQ-PST-1SG</ta>
            <ta e="T272" id="Seg_9980" s="T271">смотреть-FRQ-PST-1SG</ta>
            <ta e="T273" id="Seg_9981" s="T272">NEG</ta>
            <ta e="T275" id="Seg_9982" s="T274">мочь-PRS-1SG</ta>
            <ta e="T276" id="Seg_9983" s="T275">смотреть-FRQ-INF.LAT</ta>
            <ta e="T277" id="Seg_9984" s="T276">NEG</ta>
            <ta e="T278" id="Seg_9985" s="T277">хороший</ta>
            <ta e="T279" id="Seg_9986" s="T278">мужчина.[NOM.SG]</ta>
            <ta e="T280" id="Seg_9987" s="T279">жить-PST.[3SG]</ta>
            <ta e="T281" id="Seg_9988" s="T280">этот-ACC</ta>
            <ta e="T282" id="Seg_9989" s="T281">называть-DUR-PST-3PL</ta>
            <ta e="T283" id="Seg_9990" s="T282">Тарджа_Барджа</ta>
            <ta e="T284" id="Seg_9991" s="T283">этот.[NOM.SG]</ta>
            <ta e="T285" id="Seg_9992" s="T284">PTCL</ta>
            <ta e="T286" id="Seg_9993" s="T285">кобыла.[NOM.SG]</ta>
            <ta e="T287" id="Seg_9994" s="T286">ловить-PST.[3SG]</ta>
            <ta e="T288" id="Seg_9995" s="T287">нога-NOM/GEN.3SG</ta>
            <ta e="T289" id="Seg_9996" s="T288">болеть-PRS.[3SG]</ta>
            <ta e="T290" id="Seg_9997" s="T289">пойти-INF.LAT</ta>
            <ta e="T291" id="Seg_9998" s="T290">NEG</ta>
            <ta e="T292" id="Seg_9999" s="T291">мочь-PRS.[3SG]</ta>
            <ta e="T293" id="Seg_10000" s="T292">тогда</ta>
            <ta e="T294" id="Seg_10001" s="T293">пойти-PST.[3SG]</ta>
            <ta e="T295" id="Seg_10002" s="T294">пойти-PST.[3SG]</ta>
            <ta e="T296" id="Seg_10003" s="T295">медведь.[NOM.SG]</ta>
            <ta e="T297" id="Seg_10004" s="T296">прийти-PRS.[3SG]</ta>
            <ta e="T298" id="Seg_10005" s="T297">взять-IMP.2SG</ta>
            <ta e="T299" id="Seg_10006" s="T298">я.LAT</ta>
            <ta e="T300" id="Seg_10007" s="T299">сам-INS</ta>
            <ta e="T301" id="Seg_10008" s="T300">ну</ta>
            <ta e="T302" id="Seg_10009" s="T301">только</ta>
            <ta e="T303" id="Seg_10010" s="T302">черный.[NOM.SG]</ta>
            <ta e="T305" id="Seg_10011" s="T304">мешок-LAT</ta>
            <ta e="T306" id="Seg_10012" s="T305">сажать-IMP.2SG.O</ta>
            <ta e="T307" id="Seg_10013" s="T306">я.ACC</ta>
            <ta e="T308" id="Seg_10014" s="T307">этот.[NOM.SG]</ta>
            <ta e="T309" id="Seg_10015" s="T308">этот-ACC</ta>
            <ta e="T310" id="Seg_10016" s="T309">сажать-PST.[3SG]</ta>
            <ta e="T311" id="Seg_10017" s="T310">опять</ta>
            <ta e="T312" id="Seg_10018" s="T311">идти-PRS.[3SG]</ta>
            <ta e="T313" id="Seg_10019" s="T312">идти-PRS.[3SG]</ta>
            <ta e="T314" id="Seg_10020" s="T313">идти-PRS.[3SG]</ta>
            <ta e="T316" id="Seg_10021" s="T314">тогда</ta>
            <ta e="T317" id="Seg_10022" s="T316">тогда</ta>
            <ta e="T318" id="Seg_10023" s="T317">большой.[NOM.SG]</ta>
            <ta e="T319" id="Seg_10024" s="T318">собака.[NOM.SG]</ta>
            <ta e="T320" id="Seg_10025" s="T319">лес-ABL</ta>
            <ta e="T321" id="Seg_10026" s="T320">прийти-PST.[3SG]</ta>
            <ta e="T322" id="Seg_10027" s="T321">взять-IMP.2SG</ta>
            <ta e="T323" id="Seg_10028" s="T322">я.LAT</ta>
            <ta e="T324" id="Seg_10029" s="T323">только</ta>
            <ta e="T325" id="Seg_10030" s="T324">черный.[NOM.SG]</ta>
            <ta e="T326" id="Seg_10031" s="T325">я.LAT</ta>
            <ta e="T327" id="Seg_10032" s="T326">мешок-LAT</ta>
            <ta e="T328" id="Seg_10033" s="T327">сажать-IMP.2SG.O</ta>
            <ta e="T329" id="Seg_10034" s="T328">этот.[NOM.SG]</ta>
            <ta e="T330" id="Seg_10035" s="T329">сажать-TR-PST.[3SG]</ta>
            <ta e="T331" id="Seg_10036" s="T330">пойти-PST.[3SG]</ta>
            <ta e="T332" id="Seg_10037" s="T331">опять</ta>
            <ta e="T333" id="Seg_10038" s="T332">прийти-PRS.[3SG]</ta>
            <ta e="T334" id="Seg_10039" s="T333">прийти-PRS.[3SG]</ta>
            <ta e="T338" id="Seg_10040" s="T337">лиса.[NOM.SG]</ta>
            <ta e="T339" id="Seg_10041" s="T338">прийти-DUR.[3SG]</ta>
            <ta e="T340" id="Seg_10042" s="T339">взять-IMP.2SG</ta>
            <ta e="T341" id="Seg_10043" s="T340">я.LAT</ta>
            <ta e="T342" id="Seg_10044" s="T341">этот.[NOM.SG]</ta>
            <ta e="T343" id="Seg_10045" s="T342">этот-ACC</ta>
            <ta e="T344" id="Seg_10046" s="T343">взять-PST.[3SG]</ta>
            <ta e="T345" id="Seg_10047" s="T344">черный.[NOM.SG]</ta>
            <ta e="T346" id="Seg_10048" s="T345">мешок-LAT</ta>
            <ta e="T347" id="Seg_10049" s="T346">сажать-PST.[3SG]</ta>
            <ta e="T348" id="Seg_10050" s="T347">тогда</ta>
            <ta e="T349" id="Seg_10051" s="T348">опять</ta>
            <ta e="T350" id="Seg_10052" s="T349">идти-PRS.[3SG]</ta>
            <ta e="T351" id="Seg_10053" s="T350">вода.[NOM.SG]</ta>
            <ta e="T352" id="Seg_10054" s="T351">прийти-PRS.[3SG]</ta>
            <ta e="T353" id="Seg_10055" s="T352">взять-IMP.2SG</ta>
            <ta e="T354" id="Seg_10056" s="T353">я.LAT</ta>
            <ta e="T355" id="Seg_10057" s="T354">тогда</ta>
            <ta e="T356" id="Seg_10058" s="T355">этот.[NOM.SG]</ta>
            <ta e="T357" id="Seg_10059" s="T356">вода.[NOM.SG]</ta>
            <ta e="T358" id="Seg_10060" s="T357">лить-PST.[3SG]</ta>
            <ta e="T359" id="Seg_10061" s="T358">там</ta>
            <ta e="T360" id="Seg_10062" s="T359">прийти-PST.[3SG]</ta>
            <ta e="T361" id="Seg_10063" s="T360">там</ta>
            <ta e="T362" id="Seg_10064" s="T361">PTCL</ta>
            <ta e="T363" id="Seg_10065" s="T362">дом-PL</ta>
            <ta e="T364" id="Seg_10066" s="T363">стоять-PRS-3PL</ta>
            <ta e="T365" id="Seg_10067" s="T364">и</ta>
            <ta e="T366" id="Seg_10068" s="T365">вода.[NOM.SG]</ta>
            <ta e="T367" id="Seg_10069" s="T366">очень</ta>
            <ta e="T368" id="Seg_10070" s="T367">большой.[NOM.SG]</ta>
            <ta e="T369" id="Seg_10071" s="T368">этот.[NOM.SG]</ta>
            <ta e="T370" id="Seg_10072" s="T369">кобыла.[NOM.SG]</ta>
            <ta e="T371" id="Seg_10073" s="T370">завязать-PST.[3SG]</ta>
            <ta e="T372" id="Seg_10074" s="T371">огонь.[NOM.SG]</ta>
            <ta e="T373" id="Seg_10075" s="T372">класть-PST.[3SG]</ta>
            <ta e="T374" id="Seg_10076" s="T373">сидеть-DUR.[3SG]</ta>
            <ta e="T375" id="Seg_10077" s="T374">этот-ACC</ta>
            <ta e="T376" id="Seg_10078" s="T375">видеть-MOM-PST.[3SG]</ta>
            <ta e="T377" id="Seg_10079" s="T376">вождь.[NOM.SG]</ta>
            <ta e="T378" id="Seg_10080" s="T377">кто.[NOM.SG]</ta>
            <ta e="T379" id="Seg_10081" s="T378">там</ta>
            <ta e="T380" id="Seg_10082" s="T379">сидеть-DUR.[3SG]</ta>
            <ta e="T381" id="Seg_10083" s="T380">я.NOM</ta>
            <ta e="T383" id="Seg_10084" s="T382">трава-LAT</ta>
            <ta e="T384" id="Seg_10085" s="T383">ступать-DUR.[3SG]</ta>
            <ta e="T385" id="Seg_10086" s="T384">кобыла-NOM/GEN.3SG</ta>
            <ta e="T386" id="Seg_10087" s="T385">съесть-DUR.[3SG]</ta>
            <ta e="T387" id="Seg_10088" s="T386">там</ta>
            <ta e="T388" id="Seg_10089" s="T387">маленький-LOC.ADV</ta>
            <ta e="T389" id="Seg_10090" s="T388">сидеть-FUT-3SG</ta>
            <ta e="T390" id="Seg_10091" s="T389">тогда</ta>
            <ta e="T391" id="Seg_10092" s="T390">опять</ta>
            <ta e="T392" id="Seg_10093" s="T391">PTCL</ta>
            <ta e="T393" id="Seg_10094" s="T392">ступать-FUT-3SG</ta>
            <ta e="T394" id="Seg_10095" s="T393">пойти-EP-IMP.2SG</ta>
            <ta e="T395" id="Seg_10096" s="T394">сын-ACC.3SG</ta>
            <ta e="T396" id="Seg_10097" s="T395">посылать-PST.[3SG]</ta>
            <ta e="T397" id="Seg_10098" s="T396">что.[NOM.SG]</ta>
            <ta e="T398" id="Seg_10099" s="T397">этот-LAT</ta>
            <ta e="T399" id="Seg_10100" s="T398">нужно</ta>
            <ta e="T400" id="Seg_10101" s="T399">этот.[NOM.SG]</ta>
            <ta e="T401" id="Seg_10102" s="T400">прийти-PST.[3SG]</ta>
            <ta e="T403" id="Seg_10103" s="T402">спросить-DUR.[3SG]</ta>
            <ta e="T404" id="Seg_10104" s="T403">что.[NOM.SG]</ta>
            <ta e="T405" id="Seg_10105" s="T404">ты.DAT</ta>
            <ta e="T406" id="Seg_10106" s="T405">нужно</ta>
            <ta e="T407" id="Seg_10107" s="T406">этот-GEN</ta>
            <ta e="T408" id="Seg_10108" s="T407">вождь-NOM/GEN/ACC.3SG</ta>
            <ta e="T409" id="Seg_10109" s="T408">дочь-ACC.3SG</ta>
            <ta e="T410" id="Seg_10110" s="T409">взять-FUT-1SG</ta>
            <ta e="T412" id="Seg_10111" s="T411">этот.[NOM.SG]</ta>
            <ta e="T413" id="Seg_10112" s="T412">мальчик.[NOM.SG]</ta>
            <ta e="T414" id="Seg_10113" s="T413">вернуться-MOM-PST.[3SG]</ta>
            <ta e="T415" id="Seg_10114" s="T414">сказать-IPFVZ.[3SG]</ta>
            <ta e="T416" id="Seg_10115" s="T415">этот.[NOM.SG]</ta>
            <ta e="T417" id="Seg_10116" s="T416">сказать-IPFVZ.[3SG]</ta>
            <ta e="T418" id="Seg_10117" s="T417">JUSS</ta>
            <ta e="T419" id="Seg_10118" s="T418">этот-ACC</ta>
            <ta e="T420" id="Seg_10119" s="T419">вождь-NOM/GEN/ACC.3SG</ta>
            <ta e="T421" id="Seg_10120" s="T420">дочь-ACC.3SG</ta>
            <ta e="T422" id="Seg_10121" s="T421">взять-FUT-1SG</ta>
            <ta e="T423" id="Seg_10122" s="T422">JUSS</ta>
            <ta e="T424" id="Seg_10123" s="T423">пойти-IMP-3PL</ta>
            <ta e="T425" id="Seg_10124" s="T424">а.то</ta>
            <ta e="T426" id="Seg_10125" s="T425">NEG</ta>
            <ta e="T427" id="Seg_10126" s="T426">хороший</ta>
            <ta e="T428" id="Seg_10127" s="T427">стать-FUT-3SG</ta>
            <ta e="T430" id="Seg_10128" s="T429">NEG</ta>
            <ta e="T431" id="Seg_10129" s="T430">дать-FUT-1SG</ta>
            <ta e="T432" id="Seg_10130" s="T431">этот.[NOM.SG]</ta>
            <ta e="T433" id="Seg_10131" s="T432">прийти-PST.[3SG]</ta>
            <ta e="T435" id="Seg_10132" s="T434">сказать-PST.[3SG]</ta>
            <ta e="T436" id="Seg_10133" s="T435">так</ta>
            <ta e="T437" id="Seg_10134" s="T436">вождь.[NOM.SG]</ta>
            <ta e="T438" id="Seg_10135" s="T437">NEG</ta>
            <ta e="T439" id="Seg_10136" s="T438">дать-PRS-3SG.O</ta>
            <ta e="T440" id="Seg_10137" s="T439">дочь-ACC.3SG</ta>
            <ta e="T441" id="Seg_10138" s="T440">NEG</ta>
            <ta e="T442" id="Seg_10139" s="T441">дать-FUT-3SG</ta>
            <ta e="T443" id="Seg_10140" s="T442">хороший-LAT.ADV</ta>
            <ta e="T444" id="Seg_10141" s="T443">так</ta>
            <ta e="T445" id="Seg_10142" s="T444">NEG</ta>
            <ta e="T446" id="Seg_10143" s="T445">хороший</ta>
            <ta e="T447" id="Seg_10144" s="T446">дать-FUT-3SG</ta>
            <ta e="T449" id="Seg_10145" s="T448">пускать-2PL</ta>
            <ta e="T451" id="Seg_10146" s="T450">лошадь-PL</ta>
            <ta e="T452" id="Seg_10147" s="T451">этот-PL</ta>
            <ta e="T453" id="Seg_10148" s="T452">PTCL</ta>
            <ta e="T454" id="Seg_10149" s="T453">там</ta>
            <ta e="T455" id="Seg_10150" s="T454">кобыла-LAT/LOC.3SG</ta>
            <ta e="T456" id="Seg_10151" s="T455">от-тянуть-MOM-FUT-3PL</ta>
            <ta e="T457" id="Seg_10152" s="T456">тогда</ta>
            <ta e="T1072" id="Seg_10153" s="T457">пойти-CVB</ta>
            <ta e="T458" id="Seg_10154" s="T1072">исчезнуть-FUT-3SG</ta>
            <ta e="T459" id="Seg_10155" s="T458">тогда</ta>
            <ta e="T460" id="Seg_10156" s="T459">этот.[NOM.SG]</ta>
            <ta e="T461" id="Seg_10157" s="T460">медведь.[NOM.SG]</ta>
            <ta e="T462" id="Seg_10158" s="T461">посылать-PST.[3SG]</ta>
            <ta e="T463" id="Seg_10159" s="T462">черный.[NOM.SG]</ta>
            <ta e="T464" id="Seg_10160" s="T463">мешок-ABL</ta>
            <ta e="T465" id="Seg_10161" s="T464">этот.[NOM.SG]</ta>
            <ta e="T466" id="Seg_10162" s="T465">медведь.[NOM.SG]</ta>
            <ta e="T467" id="Seg_10163" s="T466">лошадь-PL</ta>
            <ta e="T468" id="Seg_10164" s="T467">PTCL</ta>
            <ta e="T469" id="Seg_10165" s="T468">гнать-MOM-PST.[3SG]</ta>
            <ta e="T470" id="Seg_10166" s="T469">тогда</ta>
            <ta e="T471" id="Seg_10167" s="T470">сказать-IPFVZ.[3SG]</ta>
            <ta e="T472" id="Seg_10168" s="T471">посылать-IMP.2PL</ta>
            <ta e="T473" id="Seg_10169" s="T472">корова-PL</ta>
            <ta e="T474" id="Seg_10170" s="T473">JUSS</ta>
            <ta e="T475" id="Seg_10171" s="T474">корова-PL</ta>
            <ta e="T476" id="Seg_10172" s="T475">рог-3SG-INS</ta>
            <ta e="T477" id="Seg_10173" s="T476">PTCL</ta>
            <ta e="T478" id="Seg_10174" s="T477">лошадь-ACC.3SG</ta>
            <ta e="T479" id="Seg_10175" s="T478">PTCL</ta>
            <ta e="T480" id="Seg_10176" s="T479">убить-FUT-3PL</ta>
            <ta e="T481" id="Seg_10177" s="T480">этот.[NOM.SG]</ta>
            <ta e="T482" id="Seg_10178" s="T481">этот.[NOM.SG]</ta>
            <ta e="T483" id="Seg_10179" s="T482">PTCL</ta>
            <ta e="T484" id="Seg_10180" s="T483">мешок-ABL</ta>
            <ta e="T485" id="Seg_10181" s="T484">посылать-PST.[3SG]</ta>
            <ta e="T486" id="Seg_10182" s="T485">большой.[NOM.SG]</ta>
            <ta e="T487" id="Seg_10183" s="T486">собака.[NOM.SG]</ta>
            <ta e="T488" id="Seg_10184" s="T487">этот.[NOM.SG]</ta>
            <ta e="T489" id="Seg_10185" s="T488">этот.[NOM.SG]</ta>
            <ta e="T490" id="Seg_10186" s="T489">пойти-PST.[3SG]</ta>
            <ta e="T491" id="Seg_10187" s="T490">этот-PL</ta>
            <ta e="T492" id="Seg_10188" s="T491">PTCL</ta>
            <ta e="T493" id="Seg_10189" s="T492">гнать-MOM-PST.[3SG]</ta>
            <ta e="T494" id="Seg_10190" s="T493">тогда</ta>
            <ta e="T495" id="Seg_10191" s="T494">сказать-IPFVZ.[3SG]</ta>
            <ta e="T497" id="Seg_10192" s="T495">посылать-IMP.2PL</ta>
            <ta e="T499" id="Seg_10193" s="T498">тогда</ta>
            <ta e="T500" id="Seg_10194" s="T499">вождь.[NOM.SG]</ta>
            <ta e="T503" id="Seg_10195" s="T502">пускать-2PL</ta>
            <ta e="T504" id="Seg_10196" s="T503">собака-PL</ta>
            <ta e="T505" id="Seg_10197" s="T504">JUSS</ta>
            <ta e="T506" id="Seg_10198" s="T505">собака-PL</ta>
            <ta e="T507" id="Seg_10199" s="T506">этот-ACC</ta>
            <ta e="T508" id="Seg_10200" s="T507">от-рвать-FUT-3PL</ta>
            <ta e="T509" id="Seg_10201" s="T508">этот.[NOM.SG]</ta>
            <ta e="T510" id="Seg_10202" s="T509">посылать-PST.[3SG]</ta>
            <ta e="T511" id="Seg_10203" s="T510">собака-NOM/GEN/ACC.3PL</ta>
            <ta e="T512" id="Seg_10204" s="T511">а</ta>
            <ta e="T513" id="Seg_10205" s="T512">этот.[NOM.SG]</ta>
            <ta e="T514" id="Seg_10206" s="T513">лиса.[NOM.SG]</ta>
            <ta e="T515" id="Seg_10207" s="T514">посылать-PST.[3SG]</ta>
            <ta e="T516" id="Seg_10208" s="T515">собака-PL</ta>
            <ta e="T517" id="Seg_10209" s="T516">лиса-LOC</ta>
            <ta e="T518" id="Seg_10210" s="T517">бежать-MOM-PST-3PL</ta>
            <ta e="T519" id="Seg_10211" s="T518">тогда</ta>
            <ta e="T520" id="Seg_10212" s="T519">этот.[NOM.SG]</ta>
            <ta e="T521" id="Seg_10213" s="T520">вождь.[NOM.SG]</ta>
            <ta e="T522" id="Seg_10214" s="T521">PTCL</ta>
            <ta e="T523" id="Seg_10215" s="T522">люди.[NOM.SG]</ta>
            <ta e="T524" id="Seg_10216" s="T523">собирать-PST.[3SG]</ta>
            <ta e="T525" id="Seg_10217" s="T524">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T526" id="Seg_10218" s="T525">этот-ACC</ta>
            <ta e="T527" id="Seg_10219" s="T526">взять-FUT-1DU</ta>
            <ta e="T528" id="Seg_10220" s="T527">люди.[NOM.SG]</ta>
            <ta e="T529" id="Seg_10221" s="T528">прийти-PST-3PL</ta>
            <ta e="T530" id="Seg_10222" s="T529">этот.[NOM.SG]</ta>
            <ta e="T531" id="Seg_10223" s="T530">PTCL</ta>
            <ta e="T532" id="Seg_10224" s="T531">мешок-ACC.3SG</ta>
            <ta e="T533" id="Seg_10225" s="T532">вода.[NOM.SG]</ta>
            <ta e="T534" id="Seg_10226" s="T533">лить-PST.[3SG]</ta>
            <ta e="T535" id="Seg_10227" s="T534">и</ta>
            <ta e="T536" id="Seg_10228" s="T535">вода.[NOM.SG]</ta>
            <ta e="T537" id="Seg_10229" s="T536">люди.[NOM.SG]</ta>
            <ta e="T538" id="Seg_10230" s="T537">люди.[NOM.SG]</ta>
            <ta e="T539" id="Seg_10231" s="T538">PTCL</ta>
            <ta e="T540" id="Seg_10232" s="T539">бежать-MOM-PST-3PL</ta>
            <ta e="T541" id="Seg_10233" s="T540">вода-ABL</ta>
            <ta e="T543" id="Seg_10234" s="T542">тогда</ta>
            <ta e="T544" id="Seg_10235" s="T543">этот.[NOM.SG]</ta>
            <ta e="T545" id="Seg_10236" s="T544">вождь.[NOM.SG]</ta>
            <ta e="T546" id="Seg_10237" s="T545">кричать-DUR.[3SG]</ta>
            <ta e="T547" id="Seg_10238" s="T546">половина-NOM/GEN/ACC.3SG</ta>
            <ta e="T549" id="Seg_10239" s="T548">быть-PRS.[3SG]</ta>
            <ta e="T550" id="Seg_10240" s="T549">сказать-PST.[3SG]</ta>
            <ta e="T551" id="Seg_10241" s="T550">я.NOM</ta>
            <ta e="T552" id="Seg_10242" s="T551">быть-PRS.[3SG]</ta>
            <ta e="T553" id="Seg_10243" s="T552">корова-PL</ta>
            <ta e="T554" id="Seg_10244" s="T553">лошадь-PL</ta>
            <ta e="T555" id="Seg_10245" s="T554">собака-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T556" id="Seg_10246" s="T555">сказать-PST-1SG</ta>
            <ta e="T557" id="Seg_10247" s="T556">деньги.[NOM.SG]</ta>
            <ta e="T558" id="Seg_10248" s="T557">много</ta>
            <ta e="T559" id="Seg_10249" s="T558">сказать-PST.[3SG]</ta>
            <ta e="T560" id="Seg_10250" s="T559">что.[NOM.SG]=INDEF</ta>
            <ta e="T561" id="Seg_10251" s="T560">я.LAT</ta>
            <ta e="T562" id="Seg_10252" s="T561">NEG</ta>
            <ta e="T563" id="Seg_10253" s="T562">нужно</ta>
            <ta e="T564" id="Seg_10254" s="T563">только</ta>
            <ta e="T565" id="Seg_10255" s="T564">дочь-ACC</ta>
            <ta e="T566" id="Seg_10256" s="T565">взять-INF.LAT</ta>
            <ta e="T568" id="Seg_10257" s="T567">тогда</ta>
            <ta e="T569" id="Seg_10258" s="T568">черный.[NOM.SG]</ta>
            <ta e="T570" id="Seg_10259" s="T569">мешок-LAT</ta>
            <ta e="T571" id="Seg_10260" s="T570">вода.[NOM.SG]</ta>
            <ta e="T572" id="Seg_10261" s="T571">взять-PST.[3SG]</ta>
            <ta e="T574" id="Seg_10262" s="T572">и</ta>
            <ta e="T576" id="Seg_10263" s="T575">тогда</ta>
            <ta e="T577" id="Seg_10264" s="T576">дать-MOM-PST.[3SG]</ta>
            <ta e="T578" id="Seg_10265" s="T577">дочь-ACC.3SG</ta>
            <ta e="T579" id="Seg_10266" s="T578">этот.[NOM.SG]</ta>
            <ta e="T580" id="Seg_10267" s="T579">сажать-PST.[3SG]</ta>
            <ta e="T581" id="Seg_10268" s="T580">сажать-PST.[3SG]</ta>
            <ta e="T582" id="Seg_10269" s="T581">кобыла-LAT</ta>
            <ta e="T583" id="Seg_10270" s="T582">и</ta>
            <ta e="T584" id="Seg_10271" s="T583">пойти-PST.[3SG]</ta>
            <ta e="T585" id="Seg_10272" s="T584">чум-LAT/LOC.3SG</ta>
            <ta e="T586" id="Seg_10273" s="T585">два.[NOM.SG]</ta>
            <ta e="T587" id="Seg_10274" s="T586">брат.[NOM.SG]</ta>
            <ta e="T588" id="Seg_10275" s="T587">жить-DUR-PST.[3SG]</ta>
            <ta e="T589" id="Seg_10276" s="T588">жить-DUR-PST.[3SG]</ta>
            <ta e="T590" id="Seg_10277" s="T589">этот-PL</ta>
            <ta e="T591" id="Seg_10278" s="T590">женщина-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T592" id="Seg_10279" s="T591">быть-PST-3PL</ta>
            <ta e="T593" id="Seg_10280" s="T592">этот-PL</ta>
            <ta e="T594" id="Seg_10281" s="T593">собирать-PST-3PL</ta>
            <ta e="T595" id="Seg_10282" s="T594">пойти-PST-3PL</ta>
            <ta e="T596" id="Seg_10283" s="T595">лес-LAT</ta>
            <ta e="T597" id="Seg_10284" s="T596">а</ta>
            <ta e="T598" id="Seg_10285" s="T597">женщина-PL</ta>
            <ta e="T599" id="Seg_10286" s="T598">чум-LAT/LOC.3SG</ta>
            <ta e="T600" id="Seg_10287" s="T599">PTCL</ta>
            <ta e="T601" id="Seg_10288" s="T600">вечер</ta>
            <ta e="T602" id="Seg_10289" s="T601">вечер-LOC.ADV</ta>
            <ta e="T604" id="Seg_10290" s="T603">есть-INF.LAT</ta>
            <ta e="T605" id="Seg_10291" s="T604">сесть-PST-3PL</ta>
            <ta e="T606" id="Seg_10292" s="T605">тогда</ta>
            <ta e="T607" id="Seg_10293" s="T606">смотреть-PRS-3PL</ta>
            <ta e="T608" id="Seg_10294" s="T607">котел-LOC</ta>
            <ta e="T610" id="Seg_10295" s="T609">какой=INDEF</ta>
            <ta e="T611" id="Seg_10296" s="T610">мужчина.[NOM.SG]</ta>
            <ta e="T612" id="Seg_10297" s="T611">сидеть-DUR.[3SG]</ta>
            <ta e="T613" id="Seg_10298" s="T612">этот-PL</ta>
            <ta e="T614" id="Seg_10299" s="T613">гнездо-PL</ta>
            <ta e="T616" id="Seg_10300" s="T615">собирать-PST-3PL</ta>
            <ta e="T617" id="Seg_10301" s="T616">и</ta>
            <ta e="T618" id="Seg_10302" s="T617">котел-NOM/GEN/ACC.3SG</ta>
            <ta e="T620" id="Seg_10303" s="T619">котел-NOM/GEN/ACC.3SG</ta>
            <ta e="T621" id="Seg_10304" s="T620">класть-PST-3PL</ta>
            <ta e="T622" id="Seg_10305" s="T621">а</ta>
            <ta e="T623" id="Seg_10306" s="T622">этот.[NOM.SG]</ta>
            <ta e="T624" id="Seg_10307" s="T623">ветка.[NOM.SG]</ta>
            <ta e="T626" id="Seg_10308" s="T624">PTCL</ta>
            <ta e="T628" id="Seg_10309" s="T627">влажный.[NOM.SG]</ta>
            <ta e="T629" id="Seg_10310" s="T628">быть-PST.[3SG]</ta>
            <ta e="T630" id="Seg_10311" s="T629">тогда</ta>
            <ta e="T631" id="Seg_10312" s="T630">этот.[NOM.SG]</ta>
            <ta e="T632" id="Seg_10313" s="T631">прийти-PST.[3SG]</ta>
            <ta e="T633" id="Seg_10314" s="T632">NEG</ta>
            <ta e="T634" id="Seg_10315" s="T633">мочь-PST.[3SG]</ta>
            <ta e="T635" id="Seg_10316" s="T634">жечь-INF.LAT</ta>
            <ta e="T636" id="Seg_10317" s="T635">этот.[NOM.SG]</ta>
            <ta e="T637" id="Seg_10318" s="T636">мужчина.[NOM.SG]</ta>
            <ta e="T638" id="Seg_10319" s="T637">один.[NOM.SG]</ta>
            <ta e="T639" id="Seg_10320" s="T638">женщина-ACC</ta>
            <ta e="T640" id="Seg_10321" s="T639">убить-CVB-PST.[3SG]</ta>
            <ta e="T641" id="Seg_10322" s="T640">тогда</ta>
            <ta e="T642" id="Seg_10323" s="T641">два.[NOM.SG]</ta>
            <ta e="T644" id="Seg_10324" s="T643">опять</ta>
            <ta e="T645" id="Seg_10325" s="T644">женщина-LAT</ta>
            <ta e="T646" id="Seg_10326" s="T645">сказать-IPFVZ.[3SG]</ta>
            <ta e="T647" id="Seg_10327" s="T646">что.[NOM.SG]</ta>
            <ta e="T648" id="Seg_10328" s="T647">ты.NOM</ta>
            <ta e="T649" id="Seg_10329" s="T648">сидеть-DUR-2SG</ta>
            <ta e="T650" id="Seg_10330" s="T649">жечь-IMP.2SG</ta>
            <ta e="T651" id="Seg_10331" s="T650">этот.[NOM.SG]</ta>
            <ta e="T652" id="Seg_10332" s="T651">PTCL</ta>
            <ta e="T654" id="Seg_10333" s="T653">жечь-INF.LAT</ta>
            <ta e="T655" id="Seg_10334" s="T654">этот.[NOM.SG]</ta>
            <ta e="T656" id="Seg_10335" s="T655">этот-LAT</ta>
            <ta e="T657" id="Seg_10336" s="T656">топор-INS</ta>
            <ta e="T658" id="Seg_10337" s="T657">голова-ACC.3SG</ta>
            <ta e="T659" id="Seg_10338" s="T658">резать-PST.[3SG]</ta>
            <ta e="T660" id="Seg_10339" s="T659">тогда</ta>
            <ta e="T661" id="Seg_10340" s="T660">ребенок-PL-LAT</ta>
            <ta e="T662" id="Seg_10341" s="T661">убить-RES-PST.[3SG]</ta>
            <ta e="T663" id="Seg_10342" s="T662">тогда</ta>
            <ta e="T664" id="Seg_10343" s="T663">этот</ta>
            <ta e="T666" id="Seg_10344" s="T665">этот-PL-LAT</ta>
            <ta e="T667" id="Seg_10345" s="T666">чум-LAT/LOC.3SG</ta>
            <ta e="T669" id="Seg_10346" s="T668">вернуться-MOM-PST.[3SG]</ta>
            <ta e="T670" id="Seg_10347" s="T669">этот-PL</ta>
            <ta e="T672" id="Seg_10348" s="T671">женщина-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T673" id="Seg_10349" s="T672">NEG.EX.[3SG]</ta>
            <ta e="T674" id="Seg_10350" s="T673">кто.[NOM.SG]=INDEF</ta>
            <ta e="T675" id="Seg_10351" s="T674">убить-PST.[3SG]</ta>
            <ta e="T676" id="Seg_10352" s="T675">ребенок-ACC.PL</ta>
            <ta e="T677" id="Seg_10353" s="T676">и</ta>
            <ta e="T678" id="Seg_10354" s="T677">женщина-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T680" id="Seg_10355" s="T679">жить-PST-3PL</ta>
            <ta e="T681" id="Seg_10356" s="T680">мужчина.[NOM.SG]</ta>
            <ta e="T682" id="Seg_10357" s="T681">женщина-3SG-COM</ta>
            <ta e="T683" id="Seg_10358" s="T682">этот-PL</ta>
            <ta e="T684" id="Seg_10359" s="T683">что.[NOM.SG]=INDEF</ta>
            <ta e="T685" id="Seg_10360" s="T684">NEG.EX-PST.[3SG]</ta>
            <ta e="T686" id="Seg_10361" s="T685">только</ta>
            <ta e="T687" id="Seg_10362" s="T686">один.[NOM.SG]</ta>
            <ta e="T688" id="Seg_10363" s="T687">корова.[NOM.SG]</ta>
            <ta e="T689" id="Seg_10364" s="T688">этот-PL</ta>
            <ta e="T690" id="Seg_10365" s="T689">есть-INF.LAT</ta>
            <ta e="T691" id="Seg_10366" s="T690">NEG.EX.[3SG]</ta>
            <ta e="T692" id="Seg_10367" s="T691">быть-PST.[3SG]</ta>
            <ta e="T693" id="Seg_10368" s="T692">тогда</ta>
            <ta e="T694" id="Seg_10369" s="T693">корова-NOM/GEN/ACC.3SG</ta>
            <ta e="T695" id="Seg_10370" s="T694">резать-MOM-PST.[3SG]</ta>
            <ta e="T696" id="Seg_10371" s="T695">женщина-NOM/GEN/ACC.1SG</ta>
            <ta e="T697" id="Seg_10372" s="T696">гнать-MOM-PST.[3SG]</ta>
            <ta e="T698" id="Seg_10373" s="T697">пойти-EP-IMP.2SG</ta>
            <ta e="T699" id="Seg_10374" s="T698">а.то</ta>
            <ta e="T700" id="Seg_10375" s="T699">я.LAT</ta>
            <ta e="T701" id="Seg_10376" s="T700">сам-LAT/LOC.3SG-NOM/GEN/ACC.1SG</ta>
            <ta e="T702" id="Seg_10377" s="T701">мало</ta>
            <ta e="T703" id="Seg_10378" s="T702">стать-FUT-3SG</ta>
            <ta e="T704" id="Seg_10379" s="T703">этот.[NOM.SG]</ta>
            <ta e="T705" id="Seg_10380" s="T704">женщина.[NOM.SG]</ta>
            <ta e="T706" id="Seg_10381" s="T705">пойти-PST.[3SG]</ta>
            <ta e="T707" id="Seg_10382" s="T706">пойти-PST.[3SG]</ta>
            <ta e="T708" id="Seg_10383" s="T707">еще</ta>
            <ta e="T709" id="Seg_10384" s="T708">чум.[NOM.SG]</ta>
            <ta e="T710" id="Seg_10385" s="T709">найти-PST.[3SG]</ta>
            <ta e="T711" id="Seg_10386" s="T710">чум-LAT</ta>
            <ta e="T712" id="Seg_10387" s="T711">прийти-PST.[3SG]</ta>
            <ta e="T713" id="Seg_10388" s="T712">там</ta>
            <ta e="T714" id="Seg_10389" s="T713">жир.[NOM.SG]</ta>
            <ta e="T715" id="Seg_10390" s="T714">лежать-DUR.[3SG]</ta>
            <ta e="T716" id="Seg_10391" s="T715">мясо.[NOM.SG]</ta>
            <ta e="T717" id="Seg_10392" s="T716">лежать-DUR.[3SG]</ta>
            <ta e="T718" id="Seg_10393" s="T717">этот.[NOM.SG]</ta>
            <ta e="T719" id="Seg_10394" s="T718">кипятить-PST.[3SG]</ta>
            <ta e="T720" id="Seg_10395" s="T719">PTCL</ta>
            <ta e="T721" id="Seg_10396" s="T720">есть-PST.[3SG]</ta>
            <ta e="T722" id="Seg_10397" s="T721">слышать-PRS-3SG.O</ta>
            <ta e="T723" id="Seg_10398" s="T722">PTCL</ta>
            <ta e="T724" id="Seg_10399" s="T723">кто.[NOM.SG]=INDEF</ta>
            <ta e="T725" id="Seg_10400" s="T724">прийти-PRS.[3SG]</ta>
            <ta e="T726" id="Seg_10401" s="T725">много</ta>
            <ta e="T727" id="Seg_10402" s="T726">овца.[NOM.SG]</ta>
            <ta e="T729" id="Seg_10403" s="T728">принести-PST.[3SG]</ta>
            <ta e="T730" id="Seg_10404" s="T729">корова-PL</ta>
            <ta e="T731" id="Seg_10405" s="T730">лошадь-PL</ta>
            <ta e="T732" id="Seg_10406" s="T731">тогда</ta>
            <ta e="T733" id="Seg_10407" s="T732">прийти-PST.[3SG]</ta>
            <ta e="T734" id="Seg_10408" s="T733">чум-LAT</ta>
            <ta e="T735" id="Seg_10409" s="T734">сказать-IPFVZ.[3SG]</ta>
            <ta e="T737" id="Seg_10410" s="T736">открыть-IMP.2SG.O</ta>
            <ta e="T738" id="Seg_10411" s="T737">дверь</ta>
            <ta e="T739" id="Seg_10412" s="T738">дверь</ta>
            <ta e="T740" id="Seg_10413" s="T739">открыть-MOM-PST.[3SG]</ta>
            <ta e="T741" id="Seg_10414" s="T740">этот.[NOM.SG]</ta>
            <ta e="T742" id="Seg_10415" s="T741">прийти-PST.[3SG]</ta>
            <ta e="T743" id="Seg_10416" s="T742">смотреть-FRQ-CVB</ta>
            <ta e="T744" id="Seg_10417" s="T743">кто.[NOM.SG]=INDEF</ta>
            <ta e="T745" id="Seg_10418" s="T744">быть-PST.[3SG]</ta>
            <ta e="T746" id="Seg_10419" s="T745">жир.[NOM.SG]</ta>
            <ta e="T747" id="Seg_10420" s="T746">NEG.EX</ta>
            <ta e="T748" id="Seg_10421" s="T747">мясо.[NOM.SG]</ta>
            <ta e="T749" id="Seg_10422" s="T748">NEG.EX</ta>
            <ta e="T750" id="Seg_10423" s="T749">хлеб.[NOM.SG]</ta>
            <ta e="T751" id="Seg_10424" s="T750">NEG.EX</ta>
            <ta e="T752" id="Seg_10425" s="T751">кто.[NOM.SG]</ta>
            <ta e="T753" id="Seg_10426" s="T752">здесь</ta>
            <ta e="T754" id="Seg_10427" s="T753">быть-PST.[3SG]</ta>
            <ta e="T755" id="Seg_10428" s="T754">мужчина.[NOM.SG]</ta>
            <ta e="T756" id="Seg_10429" s="T755">так</ta>
            <ta e="T757" id="Seg_10430" s="T756">мужчина-ACC</ta>
            <ta e="T758" id="Seg_10431" s="T757">стать-FUT-3SG</ta>
            <ta e="T759" id="Seg_10432" s="T758">женщина.[NOM.SG]</ta>
            <ta e="T760" id="Seg_10433" s="T759">так</ta>
            <ta e="T761" id="Seg_10434" s="T760">товарищ-NOM/GEN/ACC.1SG</ta>
            <ta e="T762" id="Seg_10435" s="T761">стать-FUT-3SG</ta>
            <ta e="T763" id="Seg_10436" s="T762">тогда</ta>
            <ta e="T764" id="Seg_10437" s="T763">этот.[NOM.SG]</ta>
            <ta e="T765" id="Seg_10438" s="T764">женщина.[NOM.SG]</ta>
            <ta e="T766" id="Seg_10439" s="T765">встать-PST.[3SG]</ta>
            <ta e="T767" id="Seg_10440" s="T766">прийти-PST.[3SG]</ta>
            <ta e="T768" id="Seg_10441" s="T767">этот.[NOM.SG]</ta>
            <ta e="T769" id="Seg_10442" s="T768">этот-ACC</ta>
            <ta e="T770" id="Seg_10443" s="T769">сажать-PST.[3SG]</ta>
            <ta e="T771" id="Seg_10444" s="T770">есть-CVB</ta>
            <ta e="T772" id="Seg_10445" s="T771">сесть-PST-3PL</ta>
            <ta e="T773" id="Seg_10446" s="T772">этот.[NOM.SG]</ta>
            <ta e="T774" id="Seg_10447" s="T773">спросить-DUR.[3SG]</ta>
            <ta e="T775" id="Seg_10448" s="T774">что.[NOM.SG]</ta>
            <ta e="T776" id="Seg_10449" s="T775">ты.NOM</ta>
            <ta e="T777" id="Seg_10450" s="T776">рука-NOM/GEN/ACC.2SG</ta>
            <ta e="T778" id="Seg_10451" s="T777">нога-NOM/GEN/ACC.2SG</ta>
            <ta e="T779" id="Seg_10452" s="T778">NEG.EX</ta>
            <ta e="T780" id="Seg_10453" s="T779">как</ta>
            <ta e="T781" id="Seg_10454" s="T780">ты.NOM</ta>
            <ta e="T782" id="Seg_10455" s="T781">идти-PRS-2SG</ta>
            <ta e="T783" id="Seg_10456" s="T782">да</ta>
            <ta e="T784" id="Seg_10457" s="T783">так</ta>
            <ta e="T785" id="Seg_10458" s="T784">идти-PRS-2SG</ta>
            <ta e="T786" id="Seg_10459" s="T785">ты.NOM</ta>
            <ta e="T787" id="Seg_10460" s="T786">яма-LAT</ta>
            <ta e="T788" id="Seg_10461" s="T787">прыгнуть-FUT-2SG</ta>
            <ta e="T789" id="Seg_10462" s="T788">прыгнуть-FUT-1SG</ta>
            <ta e="T790" id="Seg_10463" s="T789">тогда</ta>
            <ta e="T791" id="Seg_10464" s="T790">пойти-PST-3PL</ta>
            <ta e="T792" id="Seg_10465" s="T791">этот.[NOM.SG]</ta>
            <ta e="T793" id="Seg_10466" s="T792">прыгнуть-PST.[3SG]</ta>
            <ta e="T794" id="Seg_10467" s="T793">этот.[NOM.SG]</ta>
            <ta e="T795" id="Seg_10468" s="T794">этот-LAT</ta>
            <ta e="T796" id="Seg_10469" s="T795">топор-INS</ta>
            <ta e="T797" id="Seg_10470" s="T796">голова-ACC.3SG</ta>
            <ta e="T798" id="Seg_10471" s="T797">PTCL</ta>
            <ta e="T799" id="Seg_10472" s="T798">от-резать-PST.[3SG]</ta>
            <ta e="T800" id="Seg_10473" s="T799">тогда</ta>
            <ta e="T801" id="Seg_10474" s="T800">PTCL</ta>
            <ta e="T802" id="Seg_10475" s="T801">%мозг.[NOM.SG]</ta>
            <ta e="T803" id="Seg_10476" s="T802">взять-PST.[3SG]</ta>
            <ta e="T804" id="Seg_10477" s="T803">жир-NOM/GEN/ACC.3SG</ta>
            <ta e="T805" id="Seg_10478" s="T804">взять-PST.[3SG]</ta>
            <ta e="T806" id="Seg_10479" s="T805">мясо.[NOM.SG]</ta>
            <ta e="T807" id="Seg_10480" s="T806">взять-PST.[3SG]</ta>
            <ta e="T808" id="Seg_10481" s="T807">пузырь-NOM/GEN/ACC.3SG</ta>
            <ta e="T809" id="Seg_10482" s="T808">класть-PST.[3SG]</ta>
            <ta e="T810" id="Seg_10483" s="T809">тогда</ta>
            <ta e="T811" id="Seg_10484" s="T810">вернуться-MOM-PST.[3SG]</ta>
            <ta e="T812" id="Seg_10485" s="T811">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T813" id="Seg_10486" s="T812">мужчина-LAT/LOC.3SG</ta>
            <ta e="T814" id="Seg_10487" s="T813">чум-LAT</ta>
            <ta e="T817" id="Seg_10488" s="T816">влезать-PST.[3SG]</ta>
            <ta e="T818" id="Seg_10489" s="T817">а</ta>
            <ta e="T819" id="Seg_10490" s="T818">мужчина.[NOM.SG]</ta>
            <ta e="T820" id="Seg_10491" s="T819">уголь-INS</ta>
            <ta e="T821" id="Seg_10492" s="T820">рука.[NOM.SG]</ta>
            <ta e="T823" id="Seg_10493" s="T822">рука-ACC.3SG</ta>
            <ta e="T825" id="Seg_10494" s="T823">PTCL</ta>
            <ta e="T826" id="Seg_10495" s="T825">рука-ACC.3SG</ta>
            <ta e="T828" id="Seg_10496" s="T827">уголь-INS</ta>
            <ta e="T830" id="Seg_10497" s="T828">PTCL</ta>
            <ta e="T831" id="Seg_10498" s="T830">тот-ACC</ta>
            <ta e="T832" id="Seg_10499" s="T831">мясо-NOM/GEN/ACC.1SG</ta>
            <ta e="T833" id="Seg_10500" s="T832">съесть-FUT-1SG</ta>
            <ta e="T834" id="Seg_10501" s="T833">тот-ACC</ta>
            <ta e="T835" id="Seg_10502" s="T834">мясо-NOM/GEN/ACC.1SG</ta>
            <ta e="T836" id="Seg_10503" s="T835">съесть-FUT-1SG</ta>
            <ta e="T837" id="Seg_10504" s="T836">а</ta>
            <ta e="T838" id="Seg_10505" s="T837">этот.[NOM.SG]</ta>
            <ta e="T839" id="Seg_10506" s="T838">выбросить-MOM-PST.[3SG]</ta>
            <ta e="T840" id="Seg_10507" s="T839">этот-LAT</ta>
            <ta e="T841" id="Seg_10508" s="T840">этот.[NOM.SG]</ta>
            <ta e="T842" id="Seg_10509" s="T841">хватать-MOM-PST.[3SG]</ta>
            <ta e="T843" id="Seg_10510" s="T842">вот</ta>
            <ta e="T844" id="Seg_10511" s="T843">бог.[NOM.SG]</ta>
            <ta e="T845" id="Seg_10512" s="T844">я.LAT</ta>
            <ta e="T846" id="Seg_10513" s="T845">дать-PST.[3SG]</ta>
            <ta e="T847" id="Seg_10514" s="T846">да</ta>
            <ta e="T848" id="Seg_10515" s="T847">бог.[NOM.SG]</ta>
            <ta e="T849" id="Seg_10516" s="T848">ты.DAT</ta>
            <ta e="T850" id="Seg_10517" s="T849">дать-FUT-3SG</ta>
            <ta e="T851" id="Seg_10518" s="T850">камень.[NOM.SG]</ta>
            <ta e="T852" id="Seg_10519" s="T851">ты.DAT</ta>
            <ta e="T853" id="Seg_10520" s="T852">дать-FUT-3SG</ta>
            <ta e="T854" id="Seg_10521" s="T853">бог.[NOM.SG]</ta>
            <ta e="T855" id="Seg_10522" s="T854">этот-ACC</ta>
            <ta e="T856" id="Seg_10523" s="T855">я.NOM</ta>
            <ta e="T857" id="Seg_10524" s="T856">дать-PST-1SG</ta>
            <ta e="T858" id="Seg_10525" s="T857">тогда</ta>
            <ta e="T859" id="Seg_10526" s="T858">этот.[NOM.SG]</ta>
            <ta e="T860" id="Seg_10527" s="T859">есть-PST.[3SG]</ta>
            <ta e="T861" id="Seg_10528" s="T860">пойти-PST-3PL</ta>
            <ta e="T862" id="Seg_10529" s="T861">там</ta>
            <ta e="T863" id="Seg_10530" s="T862">лошадь-PL</ta>
            <ta e="T864" id="Seg_10531" s="T863">корова-PL</ta>
            <ta e="T865" id="Seg_10532" s="T864">овца-PL</ta>
            <ta e="T866" id="Seg_10533" s="T865">прийти-PST-3PL</ta>
            <ta e="T867" id="Seg_10534" s="T866">там</ta>
            <ta e="T868" id="Seg_10535" s="T867">этот.[NOM.SG]</ta>
            <ta e="T869" id="Seg_10536" s="T868">сказать-IPFVZ.[3SG]</ta>
            <ta e="T870" id="Seg_10537" s="T869">пукать-FRQ-INF.LAT</ta>
            <ta e="T871" id="Seg_10538" s="T870">хочется</ta>
            <ta e="T872" id="Seg_10539" s="T871">NEG.AUX-IMP.2SG</ta>
            <ta e="T873" id="Seg_10540" s="T872">пукать-FRQ-EP-CNG</ta>
            <ta e="T874" id="Seg_10541" s="T873">а.то</ta>
            <ta e="T875" id="Seg_10542" s="T874">весь</ta>
            <ta e="T876" id="Seg_10543" s="T875">бежать-MOM-FUT-3PL</ta>
            <ta e="T877" id="Seg_10544" s="T876">этот.[NOM.SG]</ta>
            <ta e="T881" id="Seg_10545" s="T880">пукать-FRQ-PST.[3SG]</ta>
            <ta e="T882" id="Seg_10546" s="T881">и</ta>
            <ta e="T883" id="Seg_10547" s="T882">PTCL</ta>
            <ta e="T884" id="Seg_10548" s="T883">лошадь-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T885" id="Seg_10549" s="T884">овца-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T886" id="Seg_10550" s="T885">и</ta>
            <ta e="T887" id="Seg_10551" s="T886">корова-PL</ta>
            <ta e="T888" id="Seg_10552" s="T887">бежать-MOM-PST-3PL</ta>
            <ta e="T889" id="Seg_10553" s="T888">этот.[NOM.SG]</ta>
            <ta e="T890" id="Seg_10554" s="T889">INCH</ta>
            <ta e="T892" id="Seg_10555" s="T890">молоко-INS</ta>
            <ta e="T895" id="Seg_10556" s="T894">молоко-INS</ta>
            <ta e="T896" id="Seg_10557" s="T895">лить-INF.LAT</ta>
            <ta e="T897" id="Seg_10558" s="T896">этот-PL-LAT</ta>
            <ta e="T898" id="Seg_10559" s="T897">овца.[NOM.SG]</ta>
            <ta e="T899" id="Seg_10560" s="T898">вы.NOM</ta>
            <ta e="T900" id="Seg_10561" s="T899">коза.[NOM.SG]</ta>
            <ta e="T901" id="Seg_10562" s="T900">стать-CVB</ta>
            <ta e="T902" id="Seg_10563" s="T901">корова-PL</ta>
            <ta e="T903" id="Seg_10564" s="T902">вы.NOM</ta>
            <ta e="T904" id="Seg_10565" s="T903">вы.NOM</ta>
            <ta e="T905" id="Seg_10566" s="T904">лось.[NOM.SG]</ta>
            <ta e="T906" id="Seg_10567" s="T905">стать-IMP.2PL</ta>
            <ta e="T907" id="Seg_10568" s="T906">а</ta>
            <ta e="T908" id="Seg_10569" s="T907">вы.NOM</ta>
            <ta e="T910" id="Seg_10570" s="T908">лошадь-PL</ta>
            <ta e="T911" id="Seg_10571" s="T910">забыть-MOM-PST-1SG</ta>
            <ta e="T915" id="Seg_10572" s="T914">сказать-PST.[3SG]</ta>
            <ta e="T916" id="Seg_10573" s="T915">JUSS</ta>
            <ta e="T919" id="Seg_10574" s="T918">тогда</ta>
            <ta e="T920" id="Seg_10575" s="T919">женщина.[NOM.SG]</ta>
            <ta e="T921" id="Seg_10576" s="T920">и</ta>
            <ta e="T922" id="Seg_10577" s="T921">мужчина.[NOM.SG]</ta>
            <ta e="T923" id="Seg_10578" s="T922">покинуть-MOM-PST.[3SG]</ta>
            <ta e="T924" id="Seg_10579" s="T923">что.[NOM.SG]=INDEF</ta>
            <ta e="T925" id="Seg_10580" s="T924">опять</ta>
            <ta e="T926" id="Seg_10581" s="T925">NEG.EX</ta>
            <ta e="T927" id="Seg_10582" s="T926">этот-PL-LAT</ta>
            <ta e="T928" id="Seg_10583" s="T927">PTCL</ta>
            <ta e="T1073" id="Seg_10584" s="T928">пойти-CVB</ta>
            <ta e="T1071" id="Seg_10585" s="T1073">исчезнуть-PST-3PL</ta>
            <ta e="T930" id="Seg_10586" s="T929">жить-PST.[3SG]</ta>
            <ta e="T931" id="Seg_10587" s="T930">мужчина.[NOM.SG]</ta>
            <ta e="T932" id="Seg_10588" s="T931">там</ta>
            <ta e="T933" id="Seg_10589" s="T932">три.[NOM.SG]</ta>
            <ta e="T934" id="Seg_10590" s="T933">дочь-NOM/GEN.3SG</ta>
            <ta e="T935" id="Seg_10591" s="T934">быть-PST.[3SG]</ta>
            <ta e="T936" id="Seg_10592" s="T935">этот-ACC</ta>
            <ta e="T937" id="Seg_10593" s="T936">позвать-PST-3PL</ta>
            <ta e="T1075" id="Seg_10594" s="T937">Агинское</ta>
            <ta e="T938" id="Seg_10595" s="T1075">поселение-LAT</ta>
            <ta e="T939" id="Seg_10596" s="T938">чтобы</ta>
            <ta e="T940" id="Seg_10597" s="T939">прийти-PST.[3SG]</ta>
            <ta e="T941" id="Seg_10598" s="T940">а</ta>
            <ta e="T942" id="Seg_10599" s="T941">большой.[NOM.SG]</ta>
            <ta e="T943" id="Seg_10600" s="T942">дочь-NOM/GEN.3SG</ta>
            <ta e="T944" id="Seg_10601" s="T943">я.NOM</ta>
            <ta e="T945" id="Seg_10602" s="T944">пойти-FUT-1SG</ta>
            <ta e="T946" id="Seg_10603" s="T945">волосы-ACC.3SG</ta>
            <ta e="T948" id="Seg_10604" s="T947">резать-PST.[3SG]</ta>
            <ta e="T949" id="Seg_10605" s="T948">штаны.[NOM.SG]</ta>
            <ta e="T950" id="Seg_10606" s="T949">надеть-PST.[3SG]</ta>
            <ta e="T951" id="Seg_10607" s="T950">рубашка.[NOM.SG]</ta>
            <ta e="T952" id="Seg_10608" s="T951">надеть-PST.[3SG]</ta>
            <ta e="T953" id="Seg_10609" s="T952">шапка.[NOM.SG]</ta>
            <ta e="T954" id="Seg_10610" s="T953">надеть-PST.[3SG]</ta>
            <ta e="T955" id="Seg_10611" s="T954">пойти-PST.[3SG]</ta>
            <ta e="T956" id="Seg_10612" s="T955">а</ta>
            <ta e="T957" id="Seg_10613" s="T956">отец-NOM/GEN.3SG</ta>
            <ta e="T958" id="Seg_10614" s="T957">медведь-INS</ta>
            <ta e="T959" id="Seg_10615" s="T958">делать-PST.[3SG]</ta>
            <ta e="T960" id="Seg_10616" s="T959">и</ta>
            <ta e="T961" id="Seg_10617" s="T960">этот-LAT</ta>
            <ta e="T962" id="Seg_10618" s="T961">прийти-PRS.[3SG]</ta>
            <ta e="T963" id="Seg_10619" s="T962">этот.[NOM.SG]</ta>
            <ta e="T964" id="Seg_10620" s="T963">PTCL</ta>
            <ta e="T965" id="Seg_10621" s="T964">пугать-MOM-PST.[3SG]</ta>
            <ta e="T966" id="Seg_10622" s="T965">и</ta>
            <ta e="T967" id="Seg_10623" s="T966">чум-LAT/LOC.3SG</ta>
            <ta e="T968" id="Seg_10624" s="T967">бежать-MOM-PST.[3SG]</ta>
            <ta e="T969" id="Seg_10625" s="T968">этот.[NOM.SG]</ta>
            <ta e="T970" id="Seg_10626" s="T969">прийти-PST.[3SG]</ta>
            <ta e="T971" id="Seg_10627" s="T970">что.[NOM.SG]</ta>
            <ta e="T972" id="Seg_10628" s="T971">NEG</ta>
            <ta e="T973" id="Seg_10629" s="T972">пойти-PST-1SG</ta>
            <ta e="T975" id="Seg_10630" s="T974">там</ta>
            <ta e="T976" id="Seg_10631" s="T975">медведь.[NOM.SG]</ta>
            <ta e="T977" id="Seg_10632" s="T976">я.NOM</ta>
            <ta e="T978" id="Seg_10633" s="T977">пугать-MOM-PST-1SG</ta>
            <ta e="T979" id="Seg_10634" s="T978">чум-LAT/LOC.3SG</ta>
            <ta e="T980" id="Seg_10635" s="T979">прийти-PST-1SG</ta>
            <ta e="T981" id="Seg_10636" s="T980">тогда</ta>
            <ta e="T982" id="Seg_10637" s="T981">другой.[NOM.SG]</ta>
            <ta e="T983" id="Seg_10638" s="T982">девушка.[NOM.SG]</ta>
            <ta e="T984" id="Seg_10639" s="T983">я.NOM</ta>
            <ta e="T985" id="Seg_10640" s="T984">пойти-FUT-1SG</ta>
            <ta e="T986" id="Seg_10641" s="T985">ну</ta>
            <ta e="T987" id="Seg_10642" s="T986">пойти-EP-IMP.2SG</ta>
            <ta e="T988" id="Seg_10643" s="T987">надеть-PST.[3SG]</ta>
            <ta e="T989" id="Seg_10644" s="T988">штаны-PL</ta>
            <ta e="T990" id="Seg_10645" s="T989">рубашка.[NOM.SG]</ta>
            <ta e="T991" id="Seg_10646" s="T990">надеть-PST.[3SG]</ta>
            <ta e="T992" id="Seg_10647" s="T991">волосы-ACC.3SG</ta>
            <ta e="T993" id="Seg_10648" s="T992">резать-PST.[3SG]</ta>
            <ta e="T994" id="Seg_10649" s="T993">шапка-ACC.3SG</ta>
            <ta e="T995" id="Seg_10650" s="T994">надеть-PST.[3SG]</ta>
            <ta e="T996" id="Seg_10651" s="T995">пойти-PST.[3SG]</ta>
            <ta e="T997" id="Seg_10652" s="T996">тогда</ta>
            <ta e="T998" id="Seg_10653" s="T997">отец-NOM/GEN.3SG</ta>
            <ta e="T999" id="Seg_10654" s="T998">стать-RES-PST.[3SG]</ta>
            <ta e="T1000" id="Seg_10655" s="T999">медведь-INS</ta>
            <ta e="T1001" id="Seg_10656" s="T1000">пойти-PST.[3SG]</ta>
            <ta e="T1002" id="Seg_10657" s="T1001">этот.[NOM.SG]</ta>
            <ta e="T1003" id="Seg_10658" s="T1002">PTCL</ta>
            <ta e="T1004" id="Seg_10659" s="T1003">пугать-MOM-PST.[3SG]</ta>
            <ta e="T1005" id="Seg_10660" s="T1004">чум-LAT/LOC.3SG</ta>
            <ta e="T1006" id="Seg_10661" s="T1005">прийти-PST.[3SG]</ta>
            <ta e="T1007" id="Seg_10662" s="T1006">что.[NOM.SG]</ta>
            <ta e="T1008" id="Seg_10663" s="T1007">прийти-PST-2SG</ta>
            <ta e="T1009" id="Seg_10664" s="T1008">и</ta>
            <ta e="T1010" id="Seg_10665" s="T1009">там</ta>
            <ta e="T1011" id="Seg_10666" s="T1010">медведь.[NOM.SG]</ta>
            <ta e="T1012" id="Seg_10667" s="T1011">прийти-PRS.[3SG]</ta>
            <ta e="T1013" id="Seg_10668" s="T1012">я.NOM</ta>
            <ta e="T1014" id="Seg_10669" s="T1013">пугать-MOM-PST-1SG</ta>
            <ta e="T1015" id="Seg_10670" s="T1014">чум-LAT/LOC.3SG</ta>
            <ta e="T1016" id="Seg_10671" s="T1015">бежать-MOM-PST-1SG</ta>
            <ta e="T1017" id="Seg_10672" s="T1016">хватит</ta>
            <ta e="T1019" id="Seg_10673" s="T1018">тогда</ta>
            <ta e="T1020" id="Seg_10674" s="T1019">маленький.[NOM.SG]</ta>
            <ta e="T1021" id="Seg_10675" s="T1020">девушка.[NOM.SG]</ta>
            <ta e="T1022" id="Seg_10676" s="T1021">я.NOM</ta>
            <ta e="T1023" id="Seg_10677" s="T1022">пойти-FUT-1SG</ta>
            <ta e="T1024" id="Seg_10678" s="T1023">ну</ta>
            <ta e="T1025" id="Seg_10679" s="T1024">пойти-EP-IMP.2SG</ta>
            <ta e="T1026" id="Seg_10680" s="T1025">тоже</ta>
            <ta e="T1027" id="Seg_10681" s="T1026">так</ta>
            <ta e="T1028" id="Seg_10682" s="T1027">же</ta>
            <ta e="T1029" id="Seg_10683" s="T1028">волосы-ACC.3SG</ta>
            <ta e="T1030" id="Seg_10684" s="T1029">резать-MOM-PST.[3SG]</ta>
            <ta e="T1031" id="Seg_10685" s="T1030">штаны-PL</ta>
            <ta e="T1032" id="Seg_10686" s="T1031">надеть-PST.[3SG]</ta>
            <ta e="T1033" id="Seg_10687" s="T1032">рубашка.[NOM.SG]</ta>
            <ta e="T1034" id="Seg_10688" s="T1033">надеть-PST.[3SG]</ta>
            <ta e="T1035" id="Seg_10689" s="T1034">и</ta>
            <ta e="T1036" id="Seg_10690" s="T1035">шапка.[NOM.SG]</ta>
            <ta e="T1037" id="Seg_10691" s="T1036">надеть-PST.[3SG]</ta>
            <ta e="T1038" id="Seg_10692" s="T1037">пойти-PST.[3SG]</ta>
            <ta e="T1039" id="Seg_10693" s="T1038">идти-PRS.[3SG]</ta>
            <ta e="T1040" id="Seg_10694" s="T1039">идти-PRS.[3SG]</ta>
            <ta e="T1041" id="Seg_10695" s="T1040">медведь.[NOM.SG]</ta>
            <ta e="T1042" id="Seg_10696" s="T1041">прийти-PRS.[3SG]</ta>
            <ta e="T1043" id="Seg_10697" s="T1042">этот.[NOM.SG]</ta>
            <ta e="T1044" id="Seg_10698" s="T1043">стрела-INS</ta>
            <ta e="T1045" id="Seg_10699" s="T1044">PTCL</ta>
            <ta e="T1046" id="Seg_10700" s="T1045">этот-ACC</ta>
            <ta e="T1048" id="Seg_10701" s="T1047">глаз-LAT/LOC.3SG</ta>
            <ta e="T1049" id="Seg_10702" s="T1048">глаз-NOM/GEN.3SG</ta>
            <ta e="T1051" id="Seg_10703" s="T1049">PTCL</ta>
            <ta e="T1052" id="Seg_10704" s="T1051">этот.[NOM.SG]</ta>
            <ta e="T1053" id="Seg_10705" s="T1052">стрела-INS</ta>
            <ta e="T1054" id="Seg_10706" s="T1053">глаз-LAT/LOC.3SG</ta>
            <ta e="T1055" id="Seg_10707" s="T1054">ударить-MULT-PST.[3SG]</ta>
            <ta e="T1056" id="Seg_10708" s="T1055">и</ta>
            <ta e="T1057" id="Seg_10709" s="T1056">глаз-NOM/GEN.3SG</ta>
            <ta e="T1059" id="Seg_10710" s="T1058">NEG.EX-MOM-PST.[3SG]</ta>
            <ta e="T1060" id="Seg_10711" s="T1059">тогда</ta>
            <ta e="T1061" id="Seg_10712" s="T1060">пойти-PST.[3SG]</ta>
            <ta e="T1062" id="Seg_10713" s="T1061">пойти-PST.[3SG]</ta>
            <ta e="T1063" id="Seg_10714" s="T1062">прийти-PRS.[3SG]</ta>
            <ta e="T1064" id="Seg_10715" s="T1063">женщина.[NOM.SG]</ta>
            <ta e="T1065" id="Seg_10716" s="T1064">жить-DUR.[3SG]</ta>
            <ta e="T1066" id="Seg_10717" s="T1065">и</ta>
            <ta e="T1067" id="Seg_10718" s="T1066">женщина-GEN</ta>
            <ta e="T1068" id="Seg_10719" s="T1067">сын-NOM/GEN.3SG</ta>
            <ta e="T1069" id="Seg_10720" s="T1068">жить-DUR.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PKZ">
            <ta e="T1" id="Seg_10721" s="T0">pers</ta>
            <ta e="T2" id="Seg_10722" s="T1">que</ta>
            <ta e="T3" id="Seg_10723" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_10724" s="T3">propr-n:case</ta>
            <ta e="T5" id="Seg_10725" s="T4">pers</ta>
            <ta e="T6" id="Seg_10726" s="T5">adj</ta>
            <ta e="T7" id="Seg_10727" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_10728" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_10729" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_10730" s="T9">adj</ta>
            <ta e="T11" id="Seg_10731" s="T10">adv</ta>
            <ta e="T12" id="Seg_10732" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_10733" s="T12">v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_10734" s="T15">que-n:num</ta>
            <ta e="T17" id="Seg_10735" s="T16">n-n:num</ta>
            <ta e="T18" id="Seg_10736" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_10737" s="T18">v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_10738" s="T19">dempro-n:num</ta>
            <ta e="T21" id="Seg_10739" s="T20">que-n:case=ptcl</ta>
            <ta e="T22" id="Seg_10740" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_10741" s="T22">v-v:pn</ta>
            <ta e="T24" id="Seg_10742" s="T23">n-n:num</ta>
            <ta e="T25" id="Seg_10743" s="T24">n-n:num</ta>
            <ta e="T26" id="Seg_10744" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_10745" s="T26">que</ta>
            <ta e="T28" id="Seg_10746" s="T27">dempro-n:num</ta>
            <ta e="T29" id="Seg_10747" s="T28">v-v&gt;v-v:pn</ta>
            <ta e="T30" id="Seg_10748" s="T29">pers</ta>
            <ta e="T31" id="Seg_10749" s="T30">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_10750" s="T31">adv</ta>
            <ta e="T33" id="Seg_10751" s="T32">v-v:n.fin</ta>
            <ta e="T34" id="Seg_10752" s="T33">n-n:case.poss</ta>
            <ta e="T37" id="Seg_10753" s="T36">num-n:case</ta>
            <ta e="T38" id="Seg_10754" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_10755" s="T38">propr-n:case</ta>
            <ta e="T40" id="Seg_10756" s="T39">v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_10757" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_10758" s="T41">v-v&gt;v-v&gt;v-v:n.fin</ta>
            <ta e="T43" id="Seg_10759" s="T42">conj</ta>
            <ta e="T45" id="Seg_10760" s="T44">v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_10761" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_10762" s="T46">num-n:case</ta>
            <ta e="T48" id="Seg_10763" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_10764" s="T48">dempro-n:case</ta>
            <ta e="T50" id="Seg_10765" s="T49">ptcl</ta>
            <ta e="T51" id="Seg_10766" s="T50">v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_10767" s="T51">dempro-n:case</ta>
            <ta e="T53" id="Seg_10768" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_10769" s="T53">dempro-n:case</ta>
            <ta e="T55" id="Seg_10770" s="T54">v-v&gt;v-v:pn</ta>
            <ta e="T57" id="Seg_10771" s="T56">n-n:case</ta>
            <ta e="T64" id="Seg_10772" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_10773" s="T64">dempro-n:case</ta>
            <ta e="T66" id="Seg_10774" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_10775" s="T66">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_10776" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_10777" s="T68">n-n:case.poss</ta>
            <ta e="T70" id="Seg_10778" s="T69">adv</ta>
            <ta e="T71" id="Seg_10779" s="T70">adj</ta>
            <ta e="T74" id="Seg_10780" s="T73">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_10781" s="T74">adv</ta>
            <ta e="T76" id="Seg_10782" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_10783" s="T76">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_10784" s="T77">n-n&gt;v-v:tense</ta>
            <ta e="T79" id="Seg_10785" s="T78">v-v:tense-v:pn</ta>
            <ta e="T80" id="Seg_10786" s="T79">v-v:n.fin</ta>
            <ta e="T82" id="Seg_10787" s="T81">dempro-n:case</ta>
            <ta e="T83" id="Seg_10788" s="T82">pers</ta>
            <ta e="T84" id="Seg_10789" s="T83">v-v:tense-v:pn</ta>
            <ta e="T86" id="Seg_10790" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_10791" s="T86">v-v:tense-v:pn</ta>
            <ta e="T88" id="Seg_10792" s="T87">conj</ta>
            <ta e="T90" id="Seg_10793" s="T89">n-n:case</ta>
            <ta e="T92" id="Seg_10794" s="T90">adv</ta>
            <ta e="T94" id="Seg_10795" s="T93">adv</ta>
            <ta e="T95" id="Seg_10796" s="T94">pers</ta>
            <ta e="T96" id="Seg_10797" s="T95">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T97" id="Seg_10798" s="T96">dempro-n:case</ta>
            <ta e="T98" id="Seg_10799" s="T97">n-n:case</ta>
            <ta e="T100" id="Seg_10800" s="T99">v-v:tense-v:pn</ta>
            <ta e="T101" id="Seg_10801" s="T100">v-v:n.fin</ta>
            <ta e="T102" id="Seg_10802" s="T101">num-n:case</ta>
            <ta e="T103" id="Seg_10803" s="T102">num-n:case</ta>
            <ta e="T104" id="Seg_10804" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_10805" s="T104">v-v:tense-v:pn</ta>
            <ta e="T106" id="Seg_10806" s="T105">quant</ta>
            <ta e="T107" id="Seg_10807" s="T106">que-n:case</ta>
            <ta e="T108" id="Seg_10808" s="T107">v-v:tense-v:pn</ta>
            <ta e="T109" id="Seg_10809" s="T108">refl-n:case.poss</ta>
            <ta e="T111" id="Seg_10810" s="T110">n-n:case.poss-n:case</ta>
            <ta e="T112" id="Seg_10811" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_10812" s="T112">v-v:tense-v:pn</ta>
            <ta e="T114" id="Seg_10813" s="T113">pers</ta>
            <ta e="T115" id="Seg_10814" s="T114">n-n:case.poss</ta>
            <ta e="T116" id="Seg_10815" s="T115">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T117" id="Seg_10816" s="T116">num-n:case</ta>
            <ta e="T118" id="Seg_10817" s="T117">num-n:case</ta>
            <ta e="T119" id="Seg_10818" s="T118">n-n:case</ta>
            <ta e="T120" id="Seg_10819" s="T119">v-v:tense-v:pn</ta>
            <ta e="T121" id="Seg_10820" s="T120">que-n:case-n:case.poss</ta>
            <ta e="T122" id="Seg_10821" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_10822" s="T122">v-v:tense-v:pn</ta>
            <ta e="T125" id="Seg_10823" s="T124">dempro-n:case</ta>
            <ta e="T126" id="Seg_10824" s="T125">adv</ta>
            <ta e="T128" id="Seg_10825" s="T127">num-n:case</ta>
            <ta e="T129" id="Seg_10826" s="T128">n-n:case</ta>
            <ta e="T130" id="Seg_10827" s="T129">dempro-n:case</ta>
            <ta e="T131" id="Seg_10828" s="T130">adv</ta>
            <ta e="T132" id="Seg_10829" s="T131">adj</ta>
            <ta e="T133" id="Seg_10830" s="T132">n-n:case</ta>
            <ta e="T134" id="Seg_10831" s="T133">quant</ta>
            <ta e="T135" id="Seg_10832" s="T134">que-n:case.poss</ta>
            <ta e="T136" id="Seg_10833" s="T135">v-v:tense-v:pn</ta>
            <ta e="T137" id="Seg_10834" s="T136">v-v:n.fin</ta>
            <ta e="T138" id="Seg_10835" s="T137">v-v:tense-v:pn</ta>
            <ta e="T139" id="Seg_10836" s="T138">conj</ta>
            <ta e="T140" id="Seg_10837" s="T139">ptcl</ta>
            <ta e="T141" id="Seg_10838" s="T140">v-v:tense-v:pn</ta>
            <ta e="T142" id="Seg_10839" s="T141">ptcl</ta>
            <ta e="T143" id="Seg_10840" s="T142">dempro-n:case</ta>
            <ta e="T144" id="Seg_10841" s="T143">ptcl</ta>
            <ta e="T145" id="Seg_10842" s="T144">v-v&gt;v-v:pn</ta>
            <ta e="T148" id="Seg_10843" s="T147">adv</ta>
            <ta e="T149" id="Seg_10844" s="T148">dempro-n:case</ta>
            <ta e="T150" id="Seg_10845" s="T149">adv</ta>
            <ta e="T151" id="Seg_10846" s="T150">n-n:case.poss</ta>
            <ta e="T152" id="Seg_10847" s="T151">quant</ta>
            <ta e="T153" id="Seg_10848" s="T152">v-v:tense-v:pn</ta>
            <ta e="T154" id="Seg_10849" s="T153">ptcl</ta>
            <ta e="T155" id="Seg_10850" s="T154">adj-n:case</ta>
            <ta e="T156" id="Seg_10851" s="T155">n-n:case.poss</ta>
            <ta e="T1074" id="Seg_10852" s="T157">propr</ta>
            <ta e="T158" id="Seg_10853" s="T1074">n-n:case</ta>
            <ta e="T159" id="Seg_10854" s="T158">num-n:case</ta>
            <ta e="T161" id="Seg_10855" s="T160">num-n:case</ta>
            <ta e="T162" id="Seg_10856" s="T161">num-n:case</ta>
            <ta e="T163" id="Seg_10857" s="T162">num-n:case</ta>
            <ta e="T164" id="Seg_10858" s="T163">dempro-n:case</ta>
            <ta e="T165" id="Seg_10859" s="T164">ptcl</ta>
            <ta e="T167" id="Seg_10860" s="T166">v-v:tense-v:pn</ta>
            <ta e="T168" id="Seg_10861" s="T167">n-n:case</ta>
            <ta e="T169" id="Seg_10862" s="T168">conj</ta>
            <ta e="T170" id="Seg_10863" s="T169">n-n:num</ta>
            <ta e="T171" id="Seg_10864" s="T170">v-v:tense-v:pn</ta>
            <ta e="T172" id="Seg_10865" s="T171">dempro-n:case</ta>
            <ta e="T173" id="Seg_10866" s="T172">adv</ta>
            <ta e="T174" id="Seg_10867" s="T173">ptcl</ta>
            <ta e="T175" id="Seg_10868" s="T174">v-v:tense-v:pn</ta>
            <ta e="T176" id="Seg_10869" s="T175">conj</ta>
            <ta e="T177" id="Seg_10870" s="T176">adv-adv:case</ta>
            <ta e="T180" id="Seg_10871" s="T179">v-v:tense-v:pn</ta>
            <ta e="T183" id="Seg_10872" s="T182">pers</ta>
            <ta e="T184" id="Seg_10873" s="T183">n-n:case</ta>
            <ta e="T185" id="Seg_10874" s="T184">adv</ta>
            <ta e="T186" id="Seg_10875" s="T185">v-v:tense-v:pn</ta>
            <ta e="T187" id="Seg_10876" s="T186">n-n:case</ta>
            <ta e="T189" id="Seg_10877" s="T188">conj</ta>
            <ta e="T190" id="Seg_10878" s="T189">n-n:case.poss-n:case</ta>
            <ta e="T191" id="Seg_10879" s="T190">adv</ta>
            <ta e="T192" id="Seg_10880" s="T191">adv-adv&gt;adv</ta>
            <ta e="T193" id="Seg_10881" s="T192">v-v:n.fin</ta>
            <ta e="T194" id="Seg_10882" s="T193">conj</ta>
            <ta e="T195" id="Seg_10883" s="T194">ptcl</ta>
            <ta e="T196" id="Seg_10884" s="T195">v-v&gt;v-v:pn</ta>
            <ta e="T198" id="Seg_10885" s="T197">que</ta>
            <ta e="T200" id="Seg_10886" s="T199">n-n:num-n:case</ta>
            <ta e="T201" id="Seg_10887" s="T200">que</ta>
            <ta e="T202" id="Seg_10888" s="T201">adv</ta>
            <ta e="T203" id="Seg_10889" s="T202">v-v:tense-v:pn</ta>
            <ta e="T204" id="Seg_10890" s="T203">v-v:n.fin</ta>
            <ta e="T205" id="Seg_10891" s="T204">conj</ta>
            <ta e="T206" id="Seg_10892" s="T205">ptcl</ta>
            <ta e="T208" id="Seg_10893" s="T207">adv</ta>
            <ta e="T209" id="Seg_10894" s="T208">propr-n:case</ta>
            <ta e="T210" id="Seg_10895" s="T209">n-n:num-n:case</ta>
            <ta e="T211" id="Seg_10896" s="T210">ptcl</ta>
            <ta e="T212" id="Seg_10897" s="T211">v-v:n.fin</ta>
            <ta e="T213" id="Seg_10898" s="T212">n-n:case</ta>
            <ta e="T214" id="Seg_10899" s="T213">n-n:case.poss</ta>
            <ta e="T215" id="Seg_10900" s="T214">n-n:case.poss</ta>
            <ta e="T216" id="Seg_10901" s="T215">adj-n:case</ta>
            <ta e="T219" id="Seg_10902" s="T218">n</ta>
            <ta e="T220" id="Seg_10903" s="T219">n-n:case</ta>
            <ta e="T221" id="Seg_10904" s="T220">v-v:tense-v:pn</ta>
            <ta e="T222" id="Seg_10905" s="T221">conj</ta>
            <ta e="T223" id="Seg_10906" s="T222">n-n:case</ta>
            <ta e="T224" id="Seg_10907" s="T223">v-v:tense-v:pn</ta>
            <ta e="T225" id="Seg_10908" s="T224">n-n:num</ta>
            <ta e="T227" id="Seg_10909" s="T226">que</ta>
            <ta e="T228" id="Seg_10910" s="T227">ptcl</ta>
            <ta e="T229" id="Seg_10911" s="T228">v-v&gt;v-v:pn</ta>
            <ta e="T230" id="Seg_10912" s="T229">ptcl</ta>
            <ta e="T231" id="Seg_10913" s="T230">v-v:tense-v:pn</ta>
            <ta e="T232" id="Seg_10914" s="T231">v-v:n.fin</ta>
            <ta e="T233" id="Seg_10915" s="T232">adv-adv</ta>
            <ta e="T234" id="Seg_10916" s="T233">ptcl</ta>
            <ta e="T239" id="Seg_10917" s="T238">conj</ta>
            <ta e="T240" id="Seg_10918" s="T239">refl-n:case.poss</ta>
            <ta e="T241" id="Seg_10919" s="T240">n-n:case.poss</ta>
            <ta e="T242" id="Seg_10920" s="T241">ptcl</ta>
            <ta e="T243" id="Seg_10921" s="T242">v-v:pn</ta>
            <ta e="T245" id="Seg_10922" s="T244">adv</ta>
            <ta e="T246" id="Seg_10923" s="T245">dempro-n:case</ta>
            <ta e="T247" id="Seg_10924" s="T246">n-n:num</ta>
            <ta e="T248" id="Seg_10925" s="T247">n-n:case.poss</ta>
            <ta e="T249" id="Seg_10926" s="T248">num-n:case</ta>
            <ta e="T250" id="Seg_10927" s="T249">n-n:case</ta>
            <ta e="T251" id="Seg_10928" s="T250">v-v:pn</ta>
            <ta e="T252" id="Seg_10929" s="T251">conj</ta>
            <ta e="T253" id="Seg_10930" s="T252">adv</ta>
            <ta e="T254" id="Seg_10931" s="T253">que-n:case=ptcl</ta>
            <ta e="T255" id="Seg_10932" s="T254">ptcl</ta>
            <ta e="T256" id="Seg_10933" s="T255">v-v:pn</ta>
            <ta e="T257" id="Seg_10934" s="T256">dempro-n:case</ta>
            <ta e="T258" id="Seg_10935" s="T257">n-n:case.poss</ta>
            <ta e="T260" id="Seg_10936" s="T259">pers</ta>
            <ta e="T261" id="Seg_10937" s="T260">pers</ta>
            <ta e="T262" id="Seg_10938" s="T261">v-v:tense-v:pn</ta>
            <ta e="T263" id="Seg_10939" s="T262">pers</ta>
            <ta e="T264" id="Seg_10940" s="T263">pers</ta>
            <ta e="T265" id="Seg_10941" s="T264">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T266" id="Seg_10942" s="T265">adv</ta>
            <ta e="T267" id="Seg_10943" s="T266">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T268" id="Seg_10944" s="T267">conj</ta>
            <ta e="T269" id="Seg_10945" s="T268">pers</ta>
            <ta e="T270" id="Seg_10946" s="T269">pers</ta>
            <ta e="T271" id="Seg_10947" s="T270">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T272" id="Seg_10948" s="T271">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T273" id="Seg_10949" s="T272">ptcl</ta>
            <ta e="T275" id="Seg_10950" s="T274">v-v:tense-v:pn</ta>
            <ta e="T276" id="Seg_10951" s="T275">v-v&gt;v-v:n.fin</ta>
            <ta e="T277" id="Seg_10952" s="T276">ptcl</ta>
            <ta e="T278" id="Seg_10953" s="T277">adj</ta>
            <ta e="T279" id="Seg_10954" s="T278">n-n:case</ta>
            <ta e="T280" id="Seg_10955" s="T279">v-v:tense-v:pn</ta>
            <ta e="T281" id="Seg_10956" s="T280">dempro-n:case</ta>
            <ta e="T282" id="Seg_10957" s="T281">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T283" id="Seg_10958" s="T282">propr</ta>
            <ta e="T284" id="Seg_10959" s="T283">dempro-n:case</ta>
            <ta e="T285" id="Seg_10960" s="T284">ptcl</ta>
            <ta e="T286" id="Seg_10961" s="T285">n-n:case</ta>
            <ta e="T287" id="Seg_10962" s="T286">v-v:tense-v:pn</ta>
            <ta e="T288" id="Seg_10963" s="T287">n-n:case.poss</ta>
            <ta e="T289" id="Seg_10964" s="T288">v-v:tense-v:pn</ta>
            <ta e="T290" id="Seg_10965" s="T289">v-v:n.fin</ta>
            <ta e="T291" id="Seg_10966" s="T290">ptcl</ta>
            <ta e="T292" id="Seg_10967" s="T291">v-v:tense-v:pn</ta>
            <ta e="T293" id="Seg_10968" s="T292">adv</ta>
            <ta e="T294" id="Seg_10969" s="T293">v-v:tense-v:pn</ta>
            <ta e="T295" id="Seg_10970" s="T294">v-v:tense-v:pn</ta>
            <ta e="T296" id="Seg_10971" s="T295">n-n:case</ta>
            <ta e="T297" id="Seg_10972" s="T296">v-v:tense-v:pn</ta>
            <ta e="T298" id="Seg_10973" s="T297">v-v:mood.pn</ta>
            <ta e="T299" id="Seg_10974" s="T298">pers</ta>
            <ta e="T300" id="Seg_10975" s="T299">refl-n:case</ta>
            <ta e="T301" id="Seg_10976" s="T300">ptcl</ta>
            <ta e="T302" id="Seg_10977" s="T301">adv</ta>
            <ta e="T303" id="Seg_10978" s="T302">adj-n:case</ta>
            <ta e="T305" id="Seg_10979" s="T304">n-n:case</ta>
            <ta e="T306" id="Seg_10980" s="T305">v-v:mood.pn</ta>
            <ta e="T307" id="Seg_10981" s="T306">pers</ta>
            <ta e="T308" id="Seg_10982" s="T307">dempro-n:case</ta>
            <ta e="T309" id="Seg_10983" s="T308">dempro-n:case</ta>
            <ta e="T310" id="Seg_10984" s="T309">v-v:tense-v:pn</ta>
            <ta e="T311" id="Seg_10985" s="T310">adv</ta>
            <ta e="T312" id="Seg_10986" s="T311">v-v:tense-v:pn</ta>
            <ta e="T313" id="Seg_10987" s="T312">v-v:tense-v:pn</ta>
            <ta e="T314" id="Seg_10988" s="T313">v-v:tense-v:pn</ta>
            <ta e="T316" id="Seg_10989" s="T314">adv</ta>
            <ta e="T317" id="Seg_10990" s="T316">adv</ta>
            <ta e="T318" id="Seg_10991" s="T317">adj-n:case</ta>
            <ta e="T319" id="Seg_10992" s="T318">n-n:case</ta>
            <ta e="T320" id="Seg_10993" s="T319">n-n:case</ta>
            <ta e="T321" id="Seg_10994" s="T320">v-v:tense-v:pn</ta>
            <ta e="T322" id="Seg_10995" s="T321">v-v:mood.pn</ta>
            <ta e="T323" id="Seg_10996" s="T322">pers</ta>
            <ta e="T324" id="Seg_10997" s="T323">adv</ta>
            <ta e="T325" id="Seg_10998" s="T324">adj-n:case</ta>
            <ta e="T326" id="Seg_10999" s="T325">pers</ta>
            <ta e="T327" id="Seg_11000" s="T326">n-n:case</ta>
            <ta e="T328" id="Seg_11001" s="T327">v-v:mood.pn</ta>
            <ta e="T329" id="Seg_11002" s="T328">dempro-n:case</ta>
            <ta e="T330" id="Seg_11003" s="T329">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T331" id="Seg_11004" s="T330">v-v:tense-v:pn</ta>
            <ta e="T332" id="Seg_11005" s="T331">adv</ta>
            <ta e="T333" id="Seg_11006" s="T332">v-v:tense-v:pn</ta>
            <ta e="T334" id="Seg_11007" s="T333">v-v:tense-v:pn</ta>
            <ta e="T338" id="Seg_11008" s="T337">n-n:case</ta>
            <ta e="T339" id="Seg_11009" s="T338">v-v&gt;v-v:pn</ta>
            <ta e="T340" id="Seg_11010" s="T339">v-v:mood.pn</ta>
            <ta e="T341" id="Seg_11011" s="T340">pers</ta>
            <ta e="T342" id="Seg_11012" s="T341">dempro-n:case</ta>
            <ta e="T343" id="Seg_11013" s="T342">dempro-n:case</ta>
            <ta e="T344" id="Seg_11014" s="T343">v-v:tense-v:pn</ta>
            <ta e="T345" id="Seg_11015" s="T344">adj-n:case</ta>
            <ta e="T346" id="Seg_11016" s="T345">n-n:case</ta>
            <ta e="T347" id="Seg_11017" s="T346">v-v:tense-v:pn</ta>
            <ta e="T348" id="Seg_11018" s="T347">adv</ta>
            <ta e="T349" id="Seg_11019" s="T348">adv</ta>
            <ta e="T350" id="Seg_11020" s="T349">v-v:tense-v:pn</ta>
            <ta e="T351" id="Seg_11021" s="T350">n-n:case</ta>
            <ta e="T352" id="Seg_11022" s="T351">v-v:tense-v:pn</ta>
            <ta e="T353" id="Seg_11023" s="T352">v-v:mood.pn</ta>
            <ta e="T354" id="Seg_11024" s="T353">pers</ta>
            <ta e="T355" id="Seg_11025" s="T354">adv</ta>
            <ta e="T356" id="Seg_11026" s="T355">dempro-n:case</ta>
            <ta e="T357" id="Seg_11027" s="T356">n-n:case</ta>
            <ta e="T358" id="Seg_11028" s="T357">v-v:tense-v:pn</ta>
            <ta e="T359" id="Seg_11029" s="T358">adv</ta>
            <ta e="T360" id="Seg_11030" s="T359">v-v:tense-v:pn</ta>
            <ta e="T361" id="Seg_11031" s="T360">adv</ta>
            <ta e="T362" id="Seg_11032" s="T361">ptcl</ta>
            <ta e="T363" id="Seg_11033" s="T362">n-n:num</ta>
            <ta e="T364" id="Seg_11034" s="T363">v-v:tense-v:pn</ta>
            <ta e="T365" id="Seg_11035" s="T364">conj</ta>
            <ta e="T366" id="Seg_11036" s="T365">n-n:case</ta>
            <ta e="T367" id="Seg_11037" s="T366">adv</ta>
            <ta e="T368" id="Seg_11038" s="T367">adj-n:case</ta>
            <ta e="T369" id="Seg_11039" s="T368">dempro-n:case</ta>
            <ta e="T370" id="Seg_11040" s="T369">n-n:case</ta>
            <ta e="T371" id="Seg_11041" s="T370">v-v:tense-v:pn</ta>
            <ta e="T372" id="Seg_11042" s="T371">n-n:case</ta>
            <ta e="T373" id="Seg_11043" s="T372">v-v:tense-v:pn</ta>
            <ta e="T374" id="Seg_11044" s="T373">v-v&gt;v-v:pn</ta>
            <ta e="T375" id="Seg_11045" s="T374">dempro-n:case</ta>
            <ta e="T376" id="Seg_11046" s="T375">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T377" id="Seg_11047" s="T376">n-n:case</ta>
            <ta e="T378" id="Seg_11048" s="T377">que-n:case</ta>
            <ta e="T379" id="Seg_11049" s="T378">adv</ta>
            <ta e="T380" id="Seg_11050" s="T379">v-v&gt;v-v:pn</ta>
            <ta e="T381" id="Seg_11051" s="T380">pers</ta>
            <ta e="T383" id="Seg_11052" s="T382">n-n:case</ta>
            <ta e="T384" id="Seg_11053" s="T383">v-v&gt;v-v:pn</ta>
            <ta e="T385" id="Seg_11054" s="T384">n-n:case.poss</ta>
            <ta e="T386" id="Seg_11055" s="T385">v-v&gt;v-v:pn</ta>
            <ta e="T387" id="Seg_11056" s="T386">adv</ta>
            <ta e="T388" id="Seg_11057" s="T387">adj-n:case</ta>
            <ta e="T389" id="Seg_11058" s="T388">v-v:tense-v:pn</ta>
            <ta e="T390" id="Seg_11059" s="T389">adv</ta>
            <ta e="T391" id="Seg_11060" s="T390">adv</ta>
            <ta e="T392" id="Seg_11061" s="T391">ptcl</ta>
            <ta e="T393" id="Seg_11062" s="T392">v-v:tense-v:pn</ta>
            <ta e="T394" id="Seg_11063" s="T393">v-v:ins-v:mood.pn</ta>
            <ta e="T395" id="Seg_11064" s="T394">n-n:case.poss</ta>
            <ta e="T396" id="Seg_11065" s="T395">v-v:tense-v:pn</ta>
            <ta e="T397" id="Seg_11066" s="T396">que-n:case</ta>
            <ta e="T398" id="Seg_11067" s="T397">dempro-n:case</ta>
            <ta e="T399" id="Seg_11068" s="T398">adv</ta>
            <ta e="T400" id="Seg_11069" s="T399">dempro-n:case</ta>
            <ta e="T401" id="Seg_11070" s="T400">v-v:tense-v:pn</ta>
            <ta e="T403" id="Seg_11071" s="T402">v-v&gt;v-v:pn</ta>
            <ta e="T404" id="Seg_11072" s="T403">que-n:case</ta>
            <ta e="T405" id="Seg_11073" s="T404">pers</ta>
            <ta e="T406" id="Seg_11074" s="T405">adv</ta>
            <ta e="T407" id="Seg_11075" s="T406">dempro-n:case</ta>
            <ta e="T408" id="Seg_11076" s="T407">n-n:case.poss</ta>
            <ta e="T409" id="Seg_11077" s="T408">n-n:case.poss</ta>
            <ta e="T410" id="Seg_11078" s="T409">v-v:tense-v:pn</ta>
            <ta e="T412" id="Seg_11079" s="T411">dempro-n:case</ta>
            <ta e="T413" id="Seg_11080" s="T412">n-n:case</ta>
            <ta e="T414" id="Seg_11081" s="T413">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T415" id="Seg_11082" s="T414">v-v&gt;v-v:pn</ta>
            <ta e="T416" id="Seg_11083" s="T415">dempro-n:case</ta>
            <ta e="T417" id="Seg_11084" s="T416">v-v&gt;v-v:pn</ta>
            <ta e="T418" id="Seg_11085" s="T417">ptcl</ta>
            <ta e="T419" id="Seg_11086" s="T418">dempro-n:case</ta>
            <ta e="T420" id="Seg_11087" s="T419">n-n:case.poss</ta>
            <ta e="T421" id="Seg_11088" s="T420">n-n:case.poss</ta>
            <ta e="T422" id="Seg_11089" s="T421">v-v:tense-v:pn</ta>
            <ta e="T423" id="Seg_11090" s="T422">ptcl</ta>
            <ta e="T424" id="Seg_11091" s="T423">v-v:mood-v:pn</ta>
            <ta e="T425" id="Seg_11092" s="T424">ptcl</ta>
            <ta e="T426" id="Seg_11093" s="T425">ptcl</ta>
            <ta e="T427" id="Seg_11094" s="T426">adj</ta>
            <ta e="T428" id="Seg_11095" s="T427">v-v:tense-v:pn</ta>
            <ta e="T430" id="Seg_11096" s="T429">ptcl</ta>
            <ta e="T431" id="Seg_11097" s="T430">v-v:tense-v:pn</ta>
            <ta e="T432" id="Seg_11098" s="T431">dempro-n:case</ta>
            <ta e="T433" id="Seg_11099" s="T432">v-v:tense-v:pn</ta>
            <ta e="T435" id="Seg_11100" s="T434">v-v:tense-v:pn</ta>
            <ta e="T436" id="Seg_11101" s="T435">ptcl</ta>
            <ta e="T437" id="Seg_11102" s="T436">n-n:case</ta>
            <ta e="T438" id="Seg_11103" s="T437">ptcl</ta>
            <ta e="T439" id="Seg_11104" s="T438">v-v:tense-v:pn</ta>
            <ta e="T440" id="Seg_11105" s="T439">n-n:case.poss</ta>
            <ta e="T441" id="Seg_11106" s="T440">ptcl</ta>
            <ta e="T442" id="Seg_11107" s="T441">v-v:tense-v:pn</ta>
            <ta e="T443" id="Seg_11108" s="T442">adj-adj&gt;adv</ta>
            <ta e="T444" id="Seg_11109" s="T443">ptcl</ta>
            <ta e="T445" id="Seg_11110" s="T444">ptcl</ta>
            <ta e="T446" id="Seg_11111" s="T445">adj</ta>
            <ta e="T447" id="Seg_11112" s="T446">v-v:tense-v:pn</ta>
            <ta e="T449" id="Seg_11113" s="T448">v-v:pn</ta>
            <ta e="T451" id="Seg_11114" s="T450">n-n:num</ta>
            <ta e="T452" id="Seg_11115" s="T451">dempro-n:num</ta>
            <ta e="T453" id="Seg_11116" s="T452">ptcl</ta>
            <ta e="T454" id="Seg_11117" s="T453">adv</ta>
            <ta e="T455" id="Seg_11118" s="T454">n-n:case.poss</ta>
            <ta e="T456" id="Seg_11119" s="T455">v&gt;v-v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T457" id="Seg_11120" s="T456">adv</ta>
            <ta e="T1072" id="Seg_11121" s="T457">v-v:n-fin</ta>
            <ta e="T458" id="Seg_11122" s="T1072">v-v:tense-v:pn</ta>
            <ta e="T459" id="Seg_11123" s="T458">adv</ta>
            <ta e="T460" id="Seg_11124" s="T459">dempro-n:case</ta>
            <ta e="T461" id="Seg_11125" s="T460">n-n:case</ta>
            <ta e="T462" id="Seg_11126" s="T461">v-v:tense-v:pn</ta>
            <ta e="T463" id="Seg_11127" s="T462">adj-n:case</ta>
            <ta e="T464" id="Seg_11128" s="T463">n-n:case</ta>
            <ta e="T465" id="Seg_11129" s="T464">dempro-n:case</ta>
            <ta e="T466" id="Seg_11130" s="T465">n-n:case</ta>
            <ta e="T467" id="Seg_11131" s="T466">n-n:num</ta>
            <ta e="T468" id="Seg_11132" s="T467">ptcl</ta>
            <ta e="T469" id="Seg_11133" s="T468">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T470" id="Seg_11134" s="T469">adv</ta>
            <ta e="T471" id="Seg_11135" s="T470">v-v&gt;v-v:pn</ta>
            <ta e="T472" id="Seg_11136" s="T471">v-v:mood.pn</ta>
            <ta e="T473" id="Seg_11137" s="T472">n-n:num</ta>
            <ta e="T474" id="Seg_11138" s="T473">ptcl</ta>
            <ta e="T475" id="Seg_11139" s="T474">n-n:num</ta>
            <ta e="T476" id="Seg_11140" s="T475">n-n:case.poss-n:case</ta>
            <ta e="T477" id="Seg_11141" s="T476">ptcl</ta>
            <ta e="T478" id="Seg_11142" s="T477">n-n:case.poss</ta>
            <ta e="T479" id="Seg_11143" s="T478">ptcl</ta>
            <ta e="T480" id="Seg_11144" s="T479">v-v:tense-v:pn</ta>
            <ta e="T481" id="Seg_11145" s="T480">dempro-n:case</ta>
            <ta e="T482" id="Seg_11146" s="T481">dempro-n:case</ta>
            <ta e="T483" id="Seg_11147" s="T482">ptcl</ta>
            <ta e="T484" id="Seg_11148" s="T483">n-n:case</ta>
            <ta e="T485" id="Seg_11149" s="T484">v-v:tense-v:pn</ta>
            <ta e="T486" id="Seg_11150" s="T485">adj-n:case</ta>
            <ta e="T487" id="Seg_11151" s="T486">n-n:case</ta>
            <ta e="T488" id="Seg_11152" s="T487">dempro-n:case</ta>
            <ta e="T489" id="Seg_11153" s="T488">dempro-n:case</ta>
            <ta e="T490" id="Seg_11154" s="T489">v-v:tense-v:pn</ta>
            <ta e="T491" id="Seg_11155" s="T490">dempro-n:num</ta>
            <ta e="T492" id="Seg_11156" s="T491">ptcl</ta>
            <ta e="T493" id="Seg_11157" s="T492">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T494" id="Seg_11158" s="T493">adv</ta>
            <ta e="T495" id="Seg_11159" s="T494">v-v&gt;v-v:pn</ta>
            <ta e="T497" id="Seg_11160" s="T495">v-v:mood.pn</ta>
            <ta e="T499" id="Seg_11161" s="T498">adv</ta>
            <ta e="T500" id="Seg_11162" s="T499">n-n:case</ta>
            <ta e="T503" id="Seg_11163" s="T502">v-v:pn</ta>
            <ta e="T504" id="Seg_11164" s="T503">n-n:num</ta>
            <ta e="T505" id="Seg_11165" s="T504">ptcl</ta>
            <ta e="T506" id="Seg_11166" s="T505">n-n:num</ta>
            <ta e="T507" id="Seg_11167" s="T506">dempro-n:case</ta>
            <ta e="T508" id="Seg_11168" s="T507">v&gt;v-v-v:tense-v:pn</ta>
            <ta e="T509" id="Seg_11169" s="T508">dempro-n:case</ta>
            <ta e="T510" id="Seg_11170" s="T509">v-v:tense-v:pn</ta>
            <ta e="T511" id="Seg_11171" s="T510">n-n:case.poss</ta>
            <ta e="T512" id="Seg_11172" s="T511">conj</ta>
            <ta e="T513" id="Seg_11173" s="T512">dempro-n:case</ta>
            <ta e="T514" id="Seg_11174" s="T513">n-n:case</ta>
            <ta e="T515" id="Seg_11175" s="T514">v-v:tense-v:pn</ta>
            <ta e="T516" id="Seg_11176" s="T515">n-n:num</ta>
            <ta e="T517" id="Seg_11177" s="T516">n-n:case</ta>
            <ta e="T518" id="Seg_11178" s="T517">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T519" id="Seg_11179" s="T518">adv</ta>
            <ta e="T520" id="Seg_11180" s="T519">dempro-n:case</ta>
            <ta e="T521" id="Seg_11181" s="T520">n-n:case</ta>
            <ta e="T522" id="Seg_11182" s="T521">ptcl</ta>
            <ta e="T523" id="Seg_11183" s="T522">n-n:case</ta>
            <ta e="T524" id="Seg_11184" s="T523">v-v:tense-v:pn</ta>
            <ta e="T525" id="Seg_11185" s="T524">v-v:mood-v:pn</ta>
            <ta e="T526" id="Seg_11186" s="T525">dempro-n:case</ta>
            <ta e="T527" id="Seg_11187" s="T526">v-v:tense-v:pn</ta>
            <ta e="T528" id="Seg_11188" s="T527">n-n:case</ta>
            <ta e="T529" id="Seg_11189" s="T528">v-v:tense-v:pn</ta>
            <ta e="T530" id="Seg_11190" s="T529">dempro-n:case</ta>
            <ta e="T531" id="Seg_11191" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_11192" s="T531">n-n:case.poss</ta>
            <ta e="T533" id="Seg_11193" s="T532">n-n:case</ta>
            <ta e="T534" id="Seg_11194" s="T533">v-v:tense-v:pn</ta>
            <ta e="T535" id="Seg_11195" s="T534">conj</ta>
            <ta e="T536" id="Seg_11196" s="T535">n-n:case</ta>
            <ta e="T537" id="Seg_11197" s="T536">n-n:case</ta>
            <ta e="T538" id="Seg_11198" s="T537">n.[n:case]</ta>
            <ta e="T539" id="Seg_11199" s="T538">ptcl</ta>
            <ta e="T540" id="Seg_11200" s="T539">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T541" id="Seg_11201" s="T540">n-n:case</ta>
            <ta e="T543" id="Seg_11202" s="T542">adv</ta>
            <ta e="T544" id="Seg_11203" s="T543">dempro-n:case</ta>
            <ta e="T545" id="Seg_11204" s="T544">n-n:case</ta>
            <ta e="T546" id="Seg_11205" s="T545">v-v&gt;v-v:pn</ta>
            <ta e="T547" id="Seg_11206" s="T546">n-n:case.poss</ta>
            <ta e="T549" id="Seg_11207" s="T548">v-v:tense-v:pn</ta>
            <ta e="T550" id="Seg_11208" s="T549">v-v:tense-v:pn</ta>
            <ta e="T551" id="Seg_11209" s="T550">pers</ta>
            <ta e="T552" id="Seg_11210" s="T551">v-v:tense-v:pn</ta>
            <ta e="T553" id="Seg_11211" s="T552">n-n:num</ta>
            <ta e="T554" id="Seg_11212" s="T553">n-n:num</ta>
            <ta e="T555" id="Seg_11213" s="T554">n-n:num-n:case.poss</ta>
            <ta e="T556" id="Seg_11214" s="T555">v-v:tense-v:pn</ta>
            <ta e="T557" id="Seg_11215" s="T556">n-n:case</ta>
            <ta e="T558" id="Seg_11216" s="T557">quant</ta>
            <ta e="T559" id="Seg_11217" s="T558">v-v:tense-v:pn</ta>
            <ta e="T560" id="Seg_11218" s="T559">que-n:case=ptcl</ta>
            <ta e="T561" id="Seg_11219" s="T560">pers</ta>
            <ta e="T562" id="Seg_11220" s="T561">ptcl</ta>
            <ta e="T563" id="Seg_11221" s="T562">adv</ta>
            <ta e="T564" id="Seg_11222" s="T563">adv</ta>
            <ta e="T565" id="Seg_11223" s="T564">n-n:case</ta>
            <ta e="T566" id="Seg_11224" s="T565">v-v:n.fin</ta>
            <ta e="T568" id="Seg_11225" s="T567">adv</ta>
            <ta e="T569" id="Seg_11226" s="T568">adj-n:case</ta>
            <ta e="T570" id="Seg_11227" s="T569">n-n:case</ta>
            <ta e="T571" id="Seg_11228" s="T570">n-n:case</ta>
            <ta e="T572" id="Seg_11229" s="T571">v-v:tense-v:pn</ta>
            <ta e="T574" id="Seg_11230" s="T572">conj</ta>
            <ta e="T576" id="Seg_11231" s="T575">adv</ta>
            <ta e="T577" id="Seg_11232" s="T576">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T578" id="Seg_11233" s="T577">n-n:case.poss</ta>
            <ta e="T579" id="Seg_11234" s="T578">dempro-n:case</ta>
            <ta e="T580" id="Seg_11235" s="T579">v-v:tense-v:pn</ta>
            <ta e="T581" id="Seg_11236" s="T580">v-v:tense-v:pn</ta>
            <ta e="T582" id="Seg_11237" s="T581">n-n:case</ta>
            <ta e="T583" id="Seg_11238" s="T582">conj</ta>
            <ta e="T584" id="Seg_11239" s="T583">v-v:tense-v:pn</ta>
            <ta e="T585" id="Seg_11240" s="T584">n-n:case.poss</ta>
            <ta e="T586" id="Seg_11241" s="T585">num-n:case</ta>
            <ta e="T587" id="Seg_11242" s="T586">n-n:case</ta>
            <ta e="T588" id="Seg_11243" s="T587">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T589" id="Seg_11244" s="T588">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T590" id="Seg_11245" s="T589">dempro-n:num</ta>
            <ta e="T591" id="Seg_11246" s="T590">n-n:num-n:case.poss</ta>
            <ta e="T592" id="Seg_11247" s="T591">v-v:tense-v:pn</ta>
            <ta e="T593" id="Seg_11248" s="T592">dempro-n:num</ta>
            <ta e="T594" id="Seg_11249" s="T593">v-v:tense-v:pn</ta>
            <ta e="T595" id="Seg_11250" s="T594">v-v:tense-v:pn</ta>
            <ta e="T596" id="Seg_11251" s="T595">n-n:case</ta>
            <ta e="T597" id="Seg_11252" s="T596">conj</ta>
            <ta e="T598" id="Seg_11253" s="T597">n-n:num</ta>
            <ta e="T599" id="Seg_11254" s="T598">n-n:case.poss</ta>
            <ta e="T600" id="Seg_11255" s="T599">ptcl</ta>
            <ta e="T601" id="Seg_11256" s="T600">n</ta>
            <ta e="T602" id="Seg_11257" s="T601">n-n:case</ta>
            <ta e="T604" id="Seg_11258" s="T603">v-v:n.fin</ta>
            <ta e="T605" id="Seg_11259" s="T604">v-v:tense-v:pn</ta>
            <ta e="T606" id="Seg_11260" s="T605">adv</ta>
            <ta e="T607" id="Seg_11261" s="T606">v-v:tense-v:pn</ta>
            <ta e="T608" id="Seg_11262" s="T607">n-n:case</ta>
            <ta e="T610" id="Seg_11263" s="T609">que=ptcl</ta>
            <ta e="T611" id="Seg_11264" s="T610">n-n:case</ta>
            <ta e="T612" id="Seg_11265" s="T611">v-v&gt;v-v:pn</ta>
            <ta e="T613" id="Seg_11266" s="T612">dempro-n:num</ta>
            <ta e="T614" id="Seg_11267" s="T613">n-n:num</ta>
            <ta e="T616" id="Seg_11268" s="T615">v-v:tense-v:pn</ta>
            <ta e="T617" id="Seg_11269" s="T616">conj</ta>
            <ta e="T618" id="Seg_11270" s="T617">n-n:case.poss</ta>
            <ta e="T620" id="Seg_11271" s="T619">n-n:case.poss</ta>
            <ta e="T621" id="Seg_11272" s="T620">v-v:tense-v:pn</ta>
            <ta e="T622" id="Seg_11273" s="T621">conj</ta>
            <ta e="T623" id="Seg_11274" s="T622">dempro-n:case</ta>
            <ta e="T624" id="Seg_11275" s="T623">n-n:case</ta>
            <ta e="T626" id="Seg_11276" s="T624">ptcl</ta>
            <ta e="T628" id="Seg_11277" s="T627">adj-n:case</ta>
            <ta e="T629" id="Seg_11278" s="T628">v-v:tense-v:pn</ta>
            <ta e="T630" id="Seg_11279" s="T629">adv</ta>
            <ta e="T631" id="Seg_11280" s="T630">dempro-n:case</ta>
            <ta e="T632" id="Seg_11281" s="T631">v-v:tense-v:pn</ta>
            <ta e="T633" id="Seg_11282" s="T632">ptcl</ta>
            <ta e="T634" id="Seg_11283" s="T633">v-v:tense-v:pn</ta>
            <ta e="T635" id="Seg_11284" s="T634">v-v:n.fin</ta>
            <ta e="T636" id="Seg_11285" s="T635">dempro-n:case</ta>
            <ta e="T637" id="Seg_11286" s="T636">n-n:case</ta>
            <ta e="T638" id="Seg_11287" s="T637">num-n:case</ta>
            <ta e="T639" id="Seg_11288" s="T638">n-n:case</ta>
            <ta e="T640" id="Seg_11289" s="T639">v-v:n.fin-v:tense-v:pn</ta>
            <ta e="T641" id="Seg_11290" s="T640">adv</ta>
            <ta e="T642" id="Seg_11291" s="T641">num-n:case</ta>
            <ta e="T644" id="Seg_11292" s="T643">adv</ta>
            <ta e="T645" id="Seg_11293" s="T644">n-n:case</ta>
            <ta e="T646" id="Seg_11294" s="T645">v-v&gt;v-v:pn</ta>
            <ta e="T647" id="Seg_11295" s="T646">que-n:case</ta>
            <ta e="T648" id="Seg_11296" s="T647">pers</ta>
            <ta e="T649" id="Seg_11297" s="T648">v-v&gt;v-v:pn</ta>
            <ta e="T650" id="Seg_11298" s="T649">v-v:mood.pn</ta>
            <ta e="T651" id="Seg_11299" s="T650">dempro-n:case</ta>
            <ta e="T652" id="Seg_11300" s="T651">ptcl</ta>
            <ta e="T654" id="Seg_11301" s="T653">v-v:n.fin</ta>
            <ta e="T655" id="Seg_11302" s="T654">dempro-n:case</ta>
            <ta e="T656" id="Seg_11303" s="T655">dempro-n:case</ta>
            <ta e="T657" id="Seg_11304" s="T656">n-n:case</ta>
            <ta e="T658" id="Seg_11305" s="T657">n-n:case.poss</ta>
            <ta e="T659" id="Seg_11306" s="T658">v-v:tense-v:pn</ta>
            <ta e="T660" id="Seg_11307" s="T659">adv</ta>
            <ta e="T661" id="Seg_11308" s="T660">n-n:num-n:case</ta>
            <ta e="T662" id="Seg_11309" s="T661">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T663" id="Seg_11310" s="T662">adv</ta>
            <ta e="T664" id="Seg_11311" s="T663">dempro</ta>
            <ta e="T666" id="Seg_11312" s="T665">dempro-n:num-n:case</ta>
            <ta e="T667" id="Seg_11313" s="T666">n-n:case.poss</ta>
            <ta e="T669" id="Seg_11314" s="T668">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T670" id="Seg_11315" s="T669">dempro-n:num</ta>
            <ta e="T672" id="Seg_11316" s="T671">n-n:num-n:case.poss</ta>
            <ta e="T673" id="Seg_11317" s="T672">v-v:pn</ta>
            <ta e="T674" id="Seg_11318" s="T673">que-n:case=ptcl</ta>
            <ta e="T675" id="Seg_11319" s="T674">v-v:tense-v:pn</ta>
            <ta e="T676" id="Seg_11320" s="T675">n-n:case</ta>
            <ta e="T677" id="Seg_11321" s="T676">conj</ta>
            <ta e="T678" id="Seg_11322" s="T677">n-n:num-n:case.poss</ta>
            <ta e="T680" id="Seg_11323" s="T679">v-v:tense-v:pn</ta>
            <ta e="T681" id="Seg_11324" s="T680">n-n:case</ta>
            <ta e="T682" id="Seg_11325" s="T681">n-n:case.poss-n:case</ta>
            <ta e="T683" id="Seg_11326" s="T682">dempro-n:num</ta>
            <ta e="T684" id="Seg_11327" s="T683">que-n:case=ptcl</ta>
            <ta e="T685" id="Seg_11328" s="T684">v-v:tense-v:pn</ta>
            <ta e="T686" id="Seg_11329" s="T685">adv</ta>
            <ta e="T687" id="Seg_11330" s="T686">num-n:case</ta>
            <ta e="T688" id="Seg_11331" s="T687">n-n:case</ta>
            <ta e="T689" id="Seg_11332" s="T688">dempro-n:num</ta>
            <ta e="T690" id="Seg_11333" s="T689">v-v:n.fin</ta>
            <ta e="T691" id="Seg_11334" s="T690">v-v:pn</ta>
            <ta e="T692" id="Seg_11335" s="T691">v-v:tense-v:pn</ta>
            <ta e="T693" id="Seg_11336" s="T692">adv</ta>
            <ta e="T694" id="Seg_11337" s="T693">n-n:case.poss</ta>
            <ta e="T695" id="Seg_11338" s="T694">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T696" id="Seg_11339" s="T695">n-n:case.poss</ta>
            <ta e="T697" id="Seg_11340" s="T696">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T698" id="Seg_11341" s="T697">v-v:ins-v:mood.pn</ta>
            <ta e="T699" id="Seg_11342" s="T698">ptcl</ta>
            <ta e="T700" id="Seg_11343" s="T699">pers</ta>
            <ta e="T701" id="Seg_11344" s="T700">refl-n:case.poss-n:case.poss</ta>
            <ta e="T702" id="Seg_11345" s="T701">adv</ta>
            <ta e="T703" id="Seg_11346" s="T702">v-v:tense-v:pn</ta>
            <ta e="T704" id="Seg_11347" s="T703">dempro-n:case</ta>
            <ta e="T705" id="Seg_11348" s="T704">n-n:case</ta>
            <ta e="T706" id="Seg_11349" s="T705">v-v:tense-v:pn</ta>
            <ta e="T707" id="Seg_11350" s="T706">v-v:tense-v:pn</ta>
            <ta e="T708" id="Seg_11351" s="T707">adv</ta>
            <ta e="T709" id="Seg_11352" s="T708">n-n:case</ta>
            <ta e="T710" id="Seg_11353" s="T709">v-v:tense-v:pn</ta>
            <ta e="T711" id="Seg_11354" s="T710">n-n:case</ta>
            <ta e="T712" id="Seg_11355" s="T711">v-v:tense-v:pn</ta>
            <ta e="T713" id="Seg_11356" s="T712">adv</ta>
            <ta e="T714" id="Seg_11357" s="T713">n-n:case</ta>
            <ta e="T715" id="Seg_11358" s="T714">v-v&gt;v-v:pn</ta>
            <ta e="T716" id="Seg_11359" s="T715">n-n:case</ta>
            <ta e="T717" id="Seg_11360" s="T716">v-v&gt;v-v:pn</ta>
            <ta e="T718" id="Seg_11361" s="T717">dempro-n:case</ta>
            <ta e="T719" id="Seg_11362" s="T718">v-v:tense-v:pn</ta>
            <ta e="T720" id="Seg_11363" s="T719">ptcl</ta>
            <ta e="T721" id="Seg_11364" s="T720">v-v:tense-v:pn</ta>
            <ta e="T722" id="Seg_11365" s="T721">v-v:tense-v:pn</ta>
            <ta e="T723" id="Seg_11366" s="T722">ptcl</ta>
            <ta e="T724" id="Seg_11367" s="T723">que-n:case=ptcl</ta>
            <ta e="T725" id="Seg_11368" s="T724">v-v:tense-v:pn</ta>
            <ta e="T726" id="Seg_11369" s="T725">quant</ta>
            <ta e="T727" id="Seg_11370" s="T726">n-n:case</ta>
            <ta e="T729" id="Seg_11371" s="T728">v-v:tense-v:pn</ta>
            <ta e="T730" id="Seg_11372" s="T729">n-n:num</ta>
            <ta e="T731" id="Seg_11373" s="T730">n-n:num</ta>
            <ta e="T732" id="Seg_11374" s="T731">adv</ta>
            <ta e="T733" id="Seg_11375" s="T732">v-v:tense-v:pn</ta>
            <ta e="T734" id="Seg_11376" s="T733">n-n:case</ta>
            <ta e="T735" id="Seg_11377" s="T734">v-v&gt;v-v:pn</ta>
            <ta e="T737" id="Seg_11378" s="T736">v-v:mood.pn</ta>
            <ta e="T738" id="Seg_11379" s="T737">n</ta>
            <ta e="T739" id="Seg_11380" s="T738">n</ta>
            <ta e="T740" id="Seg_11381" s="T739">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T741" id="Seg_11382" s="T740">dempro-n:case</ta>
            <ta e="T742" id="Seg_11383" s="T741">v-v:tense-v:pn</ta>
            <ta e="T743" id="Seg_11384" s="T742">v-v&gt;v-v:n.fin</ta>
            <ta e="T744" id="Seg_11385" s="T743">que-n:case=ptcl</ta>
            <ta e="T745" id="Seg_11386" s="T744">v-v:tense-v:pn</ta>
            <ta e="T746" id="Seg_11387" s="T745">n-n:case</ta>
            <ta e="T747" id="Seg_11388" s="T746">v</ta>
            <ta e="T748" id="Seg_11389" s="T747">n-n:case</ta>
            <ta e="T749" id="Seg_11390" s="T748">v</ta>
            <ta e="T750" id="Seg_11391" s="T749">n-n:case</ta>
            <ta e="T751" id="Seg_11392" s="T750">v</ta>
            <ta e="T752" id="Seg_11393" s="T751">que</ta>
            <ta e="T753" id="Seg_11394" s="T752">adv</ta>
            <ta e="T754" id="Seg_11395" s="T753">v-v:tense-v:pn</ta>
            <ta e="T755" id="Seg_11396" s="T754">n-n:case</ta>
            <ta e="T756" id="Seg_11397" s="T755">ptcl</ta>
            <ta e="T757" id="Seg_11398" s="T756">n-n:case</ta>
            <ta e="T758" id="Seg_11399" s="T757">v-v:tense-v:pn</ta>
            <ta e="T759" id="Seg_11400" s="T758">n-n:case</ta>
            <ta e="T760" id="Seg_11401" s="T759">ptcl</ta>
            <ta e="T761" id="Seg_11402" s="T760">n-n:case.poss</ta>
            <ta e="T762" id="Seg_11403" s="T761">v-v:tense-v:pn</ta>
            <ta e="T763" id="Seg_11404" s="T762">adv</ta>
            <ta e="T764" id="Seg_11405" s="T763">dempro-n:case</ta>
            <ta e="T765" id="Seg_11406" s="T764">n-n:case</ta>
            <ta e="T766" id="Seg_11407" s="T765">v-v:tense-v:pn</ta>
            <ta e="T767" id="Seg_11408" s="T766">v-v:tense-v:pn</ta>
            <ta e="T768" id="Seg_11409" s="T767">dempro-n:case</ta>
            <ta e="T769" id="Seg_11410" s="T768">dempro-n:case</ta>
            <ta e="T770" id="Seg_11411" s="T769">v-v:tense-v:pn</ta>
            <ta e="T771" id="Seg_11412" s="T770">v-v:n.fin</ta>
            <ta e="T772" id="Seg_11413" s="T771">v-v:tense-v:pn</ta>
            <ta e="T773" id="Seg_11414" s="T772">dempro-n:case</ta>
            <ta e="T774" id="Seg_11415" s="T773">v-v&gt;v-v:pn</ta>
            <ta e="T775" id="Seg_11416" s="T774">que-n:case</ta>
            <ta e="T776" id="Seg_11417" s="T775">pers</ta>
            <ta e="T777" id="Seg_11418" s="T776">n-n:case.poss</ta>
            <ta e="T778" id="Seg_11419" s="T777">n-n:case.poss</ta>
            <ta e="T779" id="Seg_11420" s="T778">v</ta>
            <ta e="T780" id="Seg_11421" s="T779">que</ta>
            <ta e="T781" id="Seg_11422" s="T780">pers</ta>
            <ta e="T782" id="Seg_11423" s="T781">v-v:tense-v:pn</ta>
            <ta e="T783" id="Seg_11424" s="T782">ptcl</ta>
            <ta e="T784" id="Seg_11425" s="T783">ptcl</ta>
            <ta e="T785" id="Seg_11426" s="T784">v-v:tense-v:pn</ta>
            <ta e="T786" id="Seg_11427" s="T785">pers</ta>
            <ta e="T787" id="Seg_11428" s="T786">n-n:case</ta>
            <ta e="T788" id="Seg_11429" s="T787">v-v:tense-v:pn</ta>
            <ta e="T789" id="Seg_11430" s="T788">v-v:tense-v:pn</ta>
            <ta e="T790" id="Seg_11431" s="T789">adv</ta>
            <ta e="T791" id="Seg_11432" s="T790">v-v:tense-v:pn</ta>
            <ta e="T792" id="Seg_11433" s="T791">dempro-n:case</ta>
            <ta e="T793" id="Seg_11434" s="T792">v-v:tense-v:pn</ta>
            <ta e="T794" id="Seg_11435" s="T793">dempro-n:case</ta>
            <ta e="T795" id="Seg_11436" s="T794">dempro-n:case</ta>
            <ta e="T796" id="Seg_11437" s="T795">n-n:case</ta>
            <ta e="T797" id="Seg_11438" s="T796">n-n:case.poss</ta>
            <ta e="T798" id="Seg_11439" s="T797">ptcl</ta>
            <ta e="T799" id="Seg_11440" s="T798">v&gt;v-v-v:tense-v:pn</ta>
            <ta e="T800" id="Seg_11441" s="T799">adv</ta>
            <ta e="T801" id="Seg_11442" s="T800">ptcl</ta>
            <ta e="T802" id="Seg_11443" s="T801">n-n:case</ta>
            <ta e="T803" id="Seg_11444" s="T802">v-v:tense-v:pn</ta>
            <ta e="T804" id="Seg_11445" s="T803">n-n:case.poss</ta>
            <ta e="T805" id="Seg_11446" s="T804">v-v:tense-v:pn</ta>
            <ta e="T806" id="Seg_11447" s="T805">n-n:case</ta>
            <ta e="T807" id="Seg_11448" s="T806">v-v:tense-v:pn</ta>
            <ta e="T808" id="Seg_11449" s="T807">n-n:case.poss</ta>
            <ta e="T809" id="Seg_11450" s="T808">v-v:tense-v:pn</ta>
            <ta e="T810" id="Seg_11451" s="T809">adv</ta>
            <ta e="T811" id="Seg_11452" s="T810">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T812" id="Seg_11453" s="T811">refl-n:case.poss</ta>
            <ta e="T813" id="Seg_11454" s="T812">n-n:case.poss</ta>
            <ta e="T814" id="Seg_11455" s="T813">n-n:case</ta>
            <ta e="T817" id="Seg_11456" s="T816">v-v:tense-v:pn</ta>
            <ta e="T818" id="Seg_11457" s="T817">conj</ta>
            <ta e="T819" id="Seg_11458" s="T818">n-n:case</ta>
            <ta e="T820" id="Seg_11459" s="T819">n-n:case</ta>
            <ta e="T821" id="Seg_11460" s="T820">n.[n:case]</ta>
            <ta e="T823" id="Seg_11461" s="T822">n-n:case.poss</ta>
            <ta e="T825" id="Seg_11462" s="T823">ptcl</ta>
            <ta e="T826" id="Seg_11463" s="T825">n-n:case.poss</ta>
            <ta e="T828" id="Seg_11464" s="T827">n-n:case</ta>
            <ta e="T830" id="Seg_11465" s="T828">ptcl</ta>
            <ta e="T831" id="Seg_11466" s="T830">dempro-n:case</ta>
            <ta e="T832" id="Seg_11467" s="T831">n-n:case.poss</ta>
            <ta e="T833" id="Seg_11468" s="T832">v-v:tense-v:pn</ta>
            <ta e="T834" id="Seg_11469" s="T833">dempro-n:case</ta>
            <ta e="T835" id="Seg_11470" s="T834">n-n:case.poss</ta>
            <ta e="T836" id="Seg_11471" s="T835">v-v:tense-v:pn</ta>
            <ta e="T837" id="Seg_11472" s="T836">conj</ta>
            <ta e="T838" id="Seg_11473" s="T837">dempro-n:case</ta>
            <ta e="T839" id="Seg_11474" s="T838">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T840" id="Seg_11475" s="T839">dempro-n:case</ta>
            <ta e="T841" id="Seg_11476" s="T840">dempro-n:case</ta>
            <ta e="T842" id="Seg_11477" s="T841">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T843" id="Seg_11478" s="T842">ptcl</ta>
            <ta e="T844" id="Seg_11479" s="T843">n-n:case</ta>
            <ta e="T845" id="Seg_11480" s="T844">pers</ta>
            <ta e="T846" id="Seg_11481" s="T845">v-v:tense-v:pn</ta>
            <ta e="T847" id="Seg_11482" s="T846">ptcl</ta>
            <ta e="T848" id="Seg_11483" s="T847">n-n:case</ta>
            <ta e="T849" id="Seg_11484" s="T848">pers</ta>
            <ta e="T850" id="Seg_11485" s="T849">v-v:tense-v:pn</ta>
            <ta e="T851" id="Seg_11486" s="T850">n-n:case</ta>
            <ta e="T852" id="Seg_11487" s="T851">pers</ta>
            <ta e="T853" id="Seg_11488" s="T852">v-v:tense-v:pn</ta>
            <ta e="T854" id="Seg_11489" s="T853">n-n:case</ta>
            <ta e="T855" id="Seg_11490" s="T854">dempro-n:case</ta>
            <ta e="T856" id="Seg_11491" s="T855">pers</ta>
            <ta e="T857" id="Seg_11492" s="T856">v-v:tense-v:pn</ta>
            <ta e="T858" id="Seg_11493" s="T857">adv</ta>
            <ta e="T859" id="Seg_11494" s="T858">dempro-n:case</ta>
            <ta e="T860" id="Seg_11495" s="T859">v-v:tense-v:pn</ta>
            <ta e="T861" id="Seg_11496" s="T860">v-v:tense-v:pn</ta>
            <ta e="T862" id="Seg_11497" s="T861">adv</ta>
            <ta e="T863" id="Seg_11498" s="T862">n-n:num</ta>
            <ta e="T864" id="Seg_11499" s="T863">n-n:num</ta>
            <ta e="T865" id="Seg_11500" s="T864">n-n:num</ta>
            <ta e="T866" id="Seg_11501" s="T865">v-v:tense-v:pn</ta>
            <ta e="T867" id="Seg_11502" s="T866">adv</ta>
            <ta e="T868" id="Seg_11503" s="T867">dempro-n:case</ta>
            <ta e="T869" id="Seg_11504" s="T868">v-v&gt;v-v:pn</ta>
            <ta e="T870" id="Seg_11505" s="T869">v-v&gt;v-v:n.fin</ta>
            <ta e="T871" id="Seg_11506" s="T870">n</ta>
            <ta e="T872" id="Seg_11507" s="T871">aux-v:mood.pn</ta>
            <ta e="T873" id="Seg_11508" s="T872">v-v&gt;v-v:ins-v:n.fin</ta>
            <ta e="T874" id="Seg_11509" s="T873">ptcl</ta>
            <ta e="T875" id="Seg_11510" s="T874">quant</ta>
            <ta e="T876" id="Seg_11511" s="T875">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T877" id="Seg_11512" s="T876">dempro-n:case</ta>
            <ta e="T881" id="Seg_11513" s="T880">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T882" id="Seg_11514" s="T881">conj</ta>
            <ta e="T883" id="Seg_11515" s="T882">ptcl</ta>
            <ta e="T884" id="Seg_11516" s="T883">n-n:num-n:case.poss</ta>
            <ta e="T885" id="Seg_11517" s="T884">n-n:num-n:case.poss</ta>
            <ta e="T886" id="Seg_11518" s="T885">conj</ta>
            <ta e="T887" id="Seg_11519" s="T886">n-n:num</ta>
            <ta e="T888" id="Seg_11520" s="T887">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T889" id="Seg_11521" s="T888">dempro-n:case</ta>
            <ta e="T890" id="Seg_11522" s="T889">ptcl</ta>
            <ta e="T892" id="Seg_11523" s="T890">n-n:case</ta>
            <ta e="T895" id="Seg_11524" s="T894">n-n:case</ta>
            <ta e="T896" id="Seg_11525" s="T895">v-v:n.fin</ta>
            <ta e="T897" id="Seg_11526" s="T896">dempro-n:num-n:case</ta>
            <ta e="T898" id="Seg_11527" s="T897">n-n:case</ta>
            <ta e="T899" id="Seg_11528" s="T898">pers</ta>
            <ta e="T900" id="Seg_11529" s="T899">n-n:case</ta>
            <ta e="T901" id="Seg_11530" s="T900">v-v:n.fin</ta>
            <ta e="T902" id="Seg_11531" s="T901">n-n:num</ta>
            <ta e="T903" id="Seg_11532" s="T902">pers</ta>
            <ta e="T904" id="Seg_11533" s="T903">pers</ta>
            <ta e="T905" id="Seg_11534" s="T904">n-n:case</ta>
            <ta e="T906" id="Seg_11535" s="T905">v-v:mood.pn</ta>
            <ta e="T907" id="Seg_11536" s="T906">conj</ta>
            <ta e="T908" id="Seg_11537" s="T907">pers</ta>
            <ta e="T910" id="Seg_11538" s="T908">n-n:num</ta>
            <ta e="T911" id="Seg_11539" s="T910">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T915" id="Seg_11540" s="T914">v-v:tense-v:pn</ta>
            <ta e="T916" id="Seg_11541" s="T915">ptcl</ta>
            <ta e="T919" id="Seg_11542" s="T918">adv</ta>
            <ta e="T920" id="Seg_11543" s="T919">n-n:case</ta>
            <ta e="T921" id="Seg_11544" s="T920">conj</ta>
            <ta e="T922" id="Seg_11545" s="T921">n-n:case</ta>
            <ta e="T923" id="Seg_11546" s="T922">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T924" id="Seg_11547" s="T923">que-n:case=ptcl</ta>
            <ta e="T925" id="Seg_11548" s="T924">adv</ta>
            <ta e="T926" id="Seg_11549" s="T925">v</ta>
            <ta e="T927" id="Seg_11550" s="T926">dempro-n:num-n:case</ta>
            <ta e="T928" id="Seg_11551" s="T927">ptcl</ta>
            <ta e="T1073" id="Seg_11552" s="T928">v-v:n-fin</ta>
            <ta e="T1071" id="Seg_11553" s="T1073">v-v:tense-v:pn</ta>
            <ta e="T930" id="Seg_11554" s="T929">v-v:tense-v:pn</ta>
            <ta e="T931" id="Seg_11555" s="T930">n-n:case</ta>
            <ta e="T932" id="Seg_11556" s="T931">adv</ta>
            <ta e="T933" id="Seg_11557" s="T932">num-n:case</ta>
            <ta e="T934" id="Seg_11558" s="T933">n-n:case.poss</ta>
            <ta e="T935" id="Seg_11559" s="T934">v-v:tense-v:pn</ta>
            <ta e="T936" id="Seg_11560" s="T935">dempro-n:case</ta>
            <ta e="T937" id="Seg_11561" s="T936">v-v:tense-v:pn</ta>
            <ta e="T1075" id="Seg_11562" s="T937">propr</ta>
            <ta e="T938" id="Seg_11563" s="T1075">n-n:case</ta>
            <ta e="T939" id="Seg_11564" s="T938">conj</ta>
            <ta e="T940" id="Seg_11565" s="T939">v-v:tense-v:pn</ta>
            <ta e="T941" id="Seg_11566" s="T940">conj</ta>
            <ta e="T942" id="Seg_11567" s="T941">adj-n:case</ta>
            <ta e="T943" id="Seg_11568" s="T942">n-n:case.poss</ta>
            <ta e="T944" id="Seg_11569" s="T943">pers</ta>
            <ta e="T945" id="Seg_11570" s="T944">v-v:tense-v:pn</ta>
            <ta e="T946" id="Seg_11571" s="T945">n-n:case.poss</ta>
            <ta e="T948" id="Seg_11572" s="T947">v-v:tense-v:pn</ta>
            <ta e="T949" id="Seg_11573" s="T948">n-n:case</ta>
            <ta e="T950" id="Seg_11574" s="T949">v-v:tense-v:pn</ta>
            <ta e="T951" id="Seg_11575" s="T950">n-n:case</ta>
            <ta e="T952" id="Seg_11576" s="T951">v-v:tense-v:pn</ta>
            <ta e="T953" id="Seg_11577" s="T952">n-n:case</ta>
            <ta e="T954" id="Seg_11578" s="T953">v-v:tense-v:pn</ta>
            <ta e="T955" id="Seg_11579" s="T954">v-v:tense-v:pn</ta>
            <ta e="T956" id="Seg_11580" s="T955">conj</ta>
            <ta e="T957" id="Seg_11581" s="T956">n-n:case.poss</ta>
            <ta e="T958" id="Seg_11582" s="T957">n-n:case</ta>
            <ta e="T959" id="Seg_11583" s="T958">v-v:tense-v:pn</ta>
            <ta e="T960" id="Seg_11584" s="T959">conj</ta>
            <ta e="T961" id="Seg_11585" s="T960">dempro-n:case</ta>
            <ta e="T962" id="Seg_11586" s="T961">v-v:tense-v:pn</ta>
            <ta e="T963" id="Seg_11587" s="T962">dempro-n:case</ta>
            <ta e="T964" id="Seg_11588" s="T963">ptcl</ta>
            <ta e="T965" id="Seg_11589" s="T964">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T966" id="Seg_11590" s="T965">conj</ta>
            <ta e="T967" id="Seg_11591" s="T966">n-n:case.poss</ta>
            <ta e="T968" id="Seg_11592" s="T967">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T969" id="Seg_11593" s="T968">dempro-n:case</ta>
            <ta e="T970" id="Seg_11594" s="T969">v-v:tense-v:pn</ta>
            <ta e="T971" id="Seg_11595" s="T970">que-n:case</ta>
            <ta e="T972" id="Seg_11596" s="T971">ptcl</ta>
            <ta e="T973" id="Seg_11597" s="T972">v-v:tense-v:pn</ta>
            <ta e="T975" id="Seg_11598" s="T974">adv</ta>
            <ta e="T976" id="Seg_11599" s="T975">n-n:case</ta>
            <ta e="T977" id="Seg_11600" s="T976">pers</ta>
            <ta e="T978" id="Seg_11601" s="T977">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T979" id="Seg_11602" s="T978">n-n:case.poss</ta>
            <ta e="T980" id="Seg_11603" s="T979">v-v:tense-v:pn</ta>
            <ta e="T981" id="Seg_11604" s="T980">adv</ta>
            <ta e="T982" id="Seg_11605" s="T981">adj-n:case</ta>
            <ta e="T983" id="Seg_11606" s="T982">n-n:case</ta>
            <ta e="T984" id="Seg_11607" s="T983">pers</ta>
            <ta e="T985" id="Seg_11608" s="T984">v-v:tense-v:pn</ta>
            <ta e="T986" id="Seg_11609" s="T985">ptcl</ta>
            <ta e="T987" id="Seg_11610" s="T986">v-v:ins-v:mood.pn</ta>
            <ta e="T988" id="Seg_11611" s="T987">v-v:tense-v:pn</ta>
            <ta e="T989" id="Seg_11612" s="T988">n-n:num</ta>
            <ta e="T990" id="Seg_11613" s="T989">n-n:case</ta>
            <ta e="T991" id="Seg_11614" s="T990">v-v:tense-v:pn</ta>
            <ta e="T992" id="Seg_11615" s="T991">n-n:case.poss</ta>
            <ta e="T993" id="Seg_11616" s="T992">v-v:tense-v:pn</ta>
            <ta e="T994" id="Seg_11617" s="T993">n-n:case.poss</ta>
            <ta e="T995" id="Seg_11618" s="T994">v-v:tense-v:pn</ta>
            <ta e="T996" id="Seg_11619" s="T995">v-v:tense-v:pn</ta>
            <ta e="T997" id="Seg_11620" s="T996">adv</ta>
            <ta e="T998" id="Seg_11621" s="T997">n-n:case.poss</ta>
            <ta e="T999" id="Seg_11622" s="T998">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1000" id="Seg_11623" s="T999">n-n:case</ta>
            <ta e="T1001" id="Seg_11624" s="T1000">v-v:tense-v:pn</ta>
            <ta e="T1002" id="Seg_11625" s="T1001">dempro-n:case</ta>
            <ta e="T1003" id="Seg_11626" s="T1002">ptcl</ta>
            <ta e="T1004" id="Seg_11627" s="T1003">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1005" id="Seg_11628" s="T1004">n-n:case.poss</ta>
            <ta e="T1006" id="Seg_11629" s="T1005">v-v:tense-v:pn</ta>
            <ta e="T1007" id="Seg_11630" s="T1006">que-n:case</ta>
            <ta e="T1008" id="Seg_11631" s="T1007">v-v:tense-v:pn</ta>
            <ta e="T1009" id="Seg_11632" s="T1008">conj</ta>
            <ta e="T1010" id="Seg_11633" s="T1009">adv</ta>
            <ta e="T1011" id="Seg_11634" s="T1010">n-n:case</ta>
            <ta e="T1012" id="Seg_11635" s="T1011">v-v:tense-v:pn</ta>
            <ta e="T1013" id="Seg_11636" s="T1012">pers</ta>
            <ta e="T1014" id="Seg_11637" s="T1013">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1015" id="Seg_11638" s="T1014">n-n:case.poss</ta>
            <ta e="T1016" id="Seg_11639" s="T1015">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1017" id="Seg_11640" s="T1016">ptcl</ta>
            <ta e="T1019" id="Seg_11641" s="T1018">adv</ta>
            <ta e="T1020" id="Seg_11642" s="T1019">adj-n:case</ta>
            <ta e="T1021" id="Seg_11643" s="T1020">n-n:case</ta>
            <ta e="T1022" id="Seg_11644" s="T1021">pers</ta>
            <ta e="T1023" id="Seg_11645" s="T1022">v-v:tense-v:pn</ta>
            <ta e="T1024" id="Seg_11646" s="T1023">ptcl</ta>
            <ta e="T1025" id="Seg_11647" s="T1024">v-v:ins-v:mood.pn</ta>
            <ta e="T1026" id="Seg_11648" s="T1025">ptcl</ta>
            <ta e="T1027" id="Seg_11649" s="T1026">ptcl</ta>
            <ta e="T1028" id="Seg_11650" s="T1027">ptcl</ta>
            <ta e="T1029" id="Seg_11651" s="T1028">n-n:case.poss</ta>
            <ta e="T1030" id="Seg_11652" s="T1029">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1031" id="Seg_11653" s="T1030">n-n:num</ta>
            <ta e="T1032" id="Seg_11654" s="T1031">v-v:tense-v:pn</ta>
            <ta e="T1033" id="Seg_11655" s="T1032">n-n:case</ta>
            <ta e="T1034" id="Seg_11656" s="T1033">v-v:tense-v:pn</ta>
            <ta e="T1035" id="Seg_11657" s="T1034">conj</ta>
            <ta e="T1036" id="Seg_11658" s="T1035">n-n:case</ta>
            <ta e="T1037" id="Seg_11659" s="T1036">v-v:tense-v:pn</ta>
            <ta e="T1038" id="Seg_11660" s="T1037">v-v:tense-v:pn</ta>
            <ta e="T1039" id="Seg_11661" s="T1038">v-v:tense-v:pn</ta>
            <ta e="T1040" id="Seg_11662" s="T1039">v-v:tense-v:pn</ta>
            <ta e="T1041" id="Seg_11663" s="T1040">n-n:case</ta>
            <ta e="T1042" id="Seg_11664" s="T1041">v-v:tense-v:pn</ta>
            <ta e="T1043" id="Seg_11665" s="T1042">dempro-n:case</ta>
            <ta e="T1044" id="Seg_11666" s="T1043">n-n:case</ta>
            <ta e="T1045" id="Seg_11667" s="T1044">ptcl</ta>
            <ta e="T1046" id="Seg_11668" s="T1045">dempro-n:case</ta>
            <ta e="T1048" id="Seg_11669" s="T1047">n-n:case.poss</ta>
            <ta e="T1049" id="Seg_11670" s="T1048">n-n:case.poss</ta>
            <ta e="T1051" id="Seg_11671" s="T1049">ptcl</ta>
            <ta e="T1052" id="Seg_11672" s="T1051">dempro-n:case</ta>
            <ta e="T1053" id="Seg_11673" s="T1052">n-n:case</ta>
            <ta e="T1054" id="Seg_11674" s="T1053">n-n:case.poss</ta>
            <ta e="T1055" id="Seg_11675" s="T1054">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1056" id="Seg_11676" s="T1055">conj</ta>
            <ta e="T1057" id="Seg_11677" s="T1056">n-n:case.poss</ta>
            <ta e="T1059" id="Seg_11678" s="T1058">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1060" id="Seg_11679" s="T1059">adv</ta>
            <ta e="T1061" id="Seg_11680" s="T1060">v-v:tense-v:pn</ta>
            <ta e="T1062" id="Seg_11681" s="T1061">v-v:tense-v:pn</ta>
            <ta e="T1063" id="Seg_11682" s="T1062">v-v:tense-v:pn</ta>
            <ta e="T1064" id="Seg_11683" s="T1063">n-n:case</ta>
            <ta e="T1065" id="Seg_11684" s="T1064">v-v&gt;v-v:pn</ta>
            <ta e="T1066" id="Seg_11685" s="T1065">conj</ta>
            <ta e="T1067" id="Seg_11686" s="T1066">n-n:case</ta>
            <ta e="T1068" id="Seg_11687" s="T1067">n-n:case.poss</ta>
            <ta e="T1069" id="Seg_11688" s="T1068">v-v&gt;v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PKZ">
            <ta e="T1" id="Seg_11689" s="T0">pers</ta>
            <ta e="T2" id="Seg_11690" s="T1">que</ta>
            <ta e="T3" id="Seg_11691" s="T2">v</ta>
            <ta e="T4" id="Seg_11692" s="T3">propr</ta>
            <ta e="T5" id="Seg_11693" s="T4">pers</ta>
            <ta e="T6" id="Seg_11694" s="T5">adj</ta>
            <ta e="T7" id="Seg_11695" s="T6">v</ta>
            <ta e="T8" id="Seg_11696" s="T7">n</ta>
            <ta e="T9" id="Seg_11697" s="T8">v</ta>
            <ta e="T10" id="Seg_11698" s="T9">adj</ta>
            <ta e="T11" id="Seg_11699" s="T10">adv</ta>
            <ta e="T12" id="Seg_11700" s="T11">v</ta>
            <ta e="T13" id="Seg_11701" s="T12">v</ta>
            <ta e="T16" id="Seg_11702" s="T15">que</ta>
            <ta e="T17" id="Seg_11703" s="T16">n</ta>
            <ta e="T18" id="Seg_11704" s="T17">v</ta>
            <ta e="T19" id="Seg_11705" s="T18">v</ta>
            <ta e="T20" id="Seg_11706" s="T19">dempro</ta>
            <ta e="T21" id="Seg_11707" s="T20">que</ta>
            <ta e="T22" id="Seg_11708" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_11709" s="T22">v</ta>
            <ta e="T24" id="Seg_11710" s="T23">n</ta>
            <ta e="T25" id="Seg_11711" s="T24">n</ta>
            <ta e="T26" id="Seg_11712" s="T25">n</ta>
            <ta e="T27" id="Seg_11713" s="T26">que</ta>
            <ta e="T28" id="Seg_11714" s="T27">dempro</ta>
            <ta e="T29" id="Seg_11715" s="T28">v</ta>
            <ta e="T30" id="Seg_11716" s="T29">pers</ta>
            <ta e="T31" id="Seg_11717" s="T30">v</ta>
            <ta e="T32" id="Seg_11718" s="T31">adv</ta>
            <ta e="T33" id="Seg_11719" s="T32">v</ta>
            <ta e="T34" id="Seg_11720" s="T33">n</ta>
            <ta e="T37" id="Seg_11721" s="T36">num</ta>
            <ta e="T38" id="Seg_11722" s="T37">n</ta>
            <ta e="T39" id="Seg_11723" s="T38">propr</ta>
            <ta e="T40" id="Seg_11724" s="T39">v</ta>
            <ta e="T41" id="Seg_11725" s="T40">n</ta>
            <ta e="T42" id="Seg_11726" s="T41">v</ta>
            <ta e="T43" id="Seg_11727" s="T42">conj</ta>
            <ta e="T45" id="Seg_11728" s="T44">v</ta>
            <ta e="T46" id="Seg_11729" s="T45">n</ta>
            <ta e="T47" id="Seg_11730" s="T46">num</ta>
            <ta e="T48" id="Seg_11731" s="T47">n</ta>
            <ta e="T49" id="Seg_11732" s="T48">dempro</ta>
            <ta e="T50" id="Seg_11733" s="T49">ptcl</ta>
            <ta e="T51" id="Seg_11734" s="T50">v</ta>
            <ta e="T52" id="Seg_11735" s="T51">dempro</ta>
            <ta e="T53" id="Seg_11736" s="T52">n</ta>
            <ta e="T54" id="Seg_11737" s="T53">dempro</ta>
            <ta e="T55" id="Seg_11738" s="T54">v</ta>
            <ta e="T57" id="Seg_11739" s="T56">n</ta>
            <ta e="T64" id="Seg_11740" s="T63">n</ta>
            <ta e="T65" id="Seg_11741" s="T64">dempro</ta>
            <ta e="T66" id="Seg_11742" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_11743" s="T66">v</ta>
            <ta e="T68" id="Seg_11744" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_11745" s="T68">n</ta>
            <ta e="T70" id="Seg_11746" s="T69">adv</ta>
            <ta e="T71" id="Seg_11747" s="T70">adj</ta>
            <ta e="T74" id="Seg_11748" s="T73">v</ta>
            <ta e="T75" id="Seg_11749" s="T74">adv</ta>
            <ta e="T76" id="Seg_11750" s="T75">n</ta>
            <ta e="T77" id="Seg_11751" s="T76">v</ta>
            <ta e="T78" id="Seg_11752" s="T77">v</ta>
            <ta e="T79" id="Seg_11753" s="T78">v</ta>
            <ta e="T80" id="Seg_11754" s="T79">v</ta>
            <ta e="T82" id="Seg_11755" s="T81">dempro</ta>
            <ta e="T83" id="Seg_11756" s="T82">pers</ta>
            <ta e="T84" id="Seg_11757" s="T83">v</ta>
            <ta e="T86" id="Seg_11758" s="T85">n</ta>
            <ta e="T87" id="Seg_11759" s="T86">v</ta>
            <ta e="T88" id="Seg_11760" s="T87">conj</ta>
            <ta e="T90" id="Seg_11761" s="T89">n</ta>
            <ta e="T92" id="Seg_11762" s="T90">adv</ta>
            <ta e="T94" id="Seg_11763" s="T93">adv</ta>
            <ta e="T95" id="Seg_11764" s="T94">pers</ta>
            <ta e="T96" id="Seg_11765" s="T95">v</ta>
            <ta e="T97" id="Seg_11766" s="T96">dempro</ta>
            <ta e="T98" id="Seg_11767" s="T97">n</ta>
            <ta e="T100" id="Seg_11768" s="T99">v</ta>
            <ta e="T101" id="Seg_11769" s="T100">v</ta>
            <ta e="T102" id="Seg_11770" s="T101">num</ta>
            <ta e="T103" id="Seg_11771" s="T102">num</ta>
            <ta e="T104" id="Seg_11772" s="T103">n</ta>
            <ta e="T105" id="Seg_11773" s="T104">v</ta>
            <ta e="T106" id="Seg_11774" s="T105">quant</ta>
            <ta e="T107" id="Seg_11775" s="T106">que</ta>
            <ta e="T108" id="Seg_11776" s="T107">v</ta>
            <ta e="T109" id="Seg_11777" s="T108">refl</ta>
            <ta e="T111" id="Seg_11778" s="T110">n</ta>
            <ta e="T112" id="Seg_11779" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_11780" s="T112">v</ta>
            <ta e="T114" id="Seg_11781" s="T113">pers</ta>
            <ta e="T115" id="Seg_11782" s="T114">n</ta>
            <ta e="T116" id="Seg_11783" s="T115">v</ta>
            <ta e="T117" id="Seg_11784" s="T116">num</ta>
            <ta e="T118" id="Seg_11785" s="T117">num</ta>
            <ta e="T119" id="Seg_11786" s="T118">n</ta>
            <ta e="T120" id="Seg_11787" s="T119">v</ta>
            <ta e="T121" id="Seg_11788" s="T120">que</ta>
            <ta e="T122" id="Seg_11789" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_11790" s="T122">v</ta>
            <ta e="T125" id="Seg_11791" s="T124">dempro</ta>
            <ta e="T126" id="Seg_11792" s="T125">adv</ta>
            <ta e="T128" id="Seg_11793" s="T127">num</ta>
            <ta e="T129" id="Seg_11794" s="T128">n</ta>
            <ta e="T130" id="Seg_11795" s="T129">dempro</ta>
            <ta e="T131" id="Seg_11796" s="T130">adv</ta>
            <ta e="T132" id="Seg_11797" s="T131">adj</ta>
            <ta e="T133" id="Seg_11798" s="T132">n</ta>
            <ta e="T134" id="Seg_11799" s="T133">quant</ta>
            <ta e="T135" id="Seg_11800" s="T134">que</ta>
            <ta e="T136" id="Seg_11801" s="T135">v</ta>
            <ta e="T137" id="Seg_11802" s="T136">v</ta>
            <ta e="T138" id="Seg_11803" s="T137">v</ta>
            <ta e="T139" id="Seg_11804" s="T138">conj</ta>
            <ta e="T140" id="Seg_11805" s="T139">ptcl</ta>
            <ta e="T141" id="Seg_11806" s="T140">v</ta>
            <ta e="T142" id="Seg_11807" s="T141">ptcl</ta>
            <ta e="T143" id="Seg_11808" s="T142">dempro</ta>
            <ta e="T144" id="Seg_11809" s="T143">ptcl</ta>
            <ta e="T145" id="Seg_11810" s="T144">v</ta>
            <ta e="T148" id="Seg_11811" s="T147">adv</ta>
            <ta e="T149" id="Seg_11812" s="T148">dempro</ta>
            <ta e="T150" id="Seg_11813" s="T149">adv</ta>
            <ta e="T151" id="Seg_11814" s="T150">n</ta>
            <ta e="T152" id="Seg_11815" s="T151">quant</ta>
            <ta e="T153" id="Seg_11816" s="T152">v</ta>
            <ta e="T154" id="Seg_11817" s="T153">ptcl</ta>
            <ta e="T155" id="Seg_11818" s="T154">adj</ta>
            <ta e="T156" id="Seg_11819" s="T155">n</ta>
            <ta e="T1074" id="Seg_11820" s="T157">propr</ta>
            <ta e="T158" id="Seg_11821" s="T1074">n</ta>
            <ta e="T159" id="Seg_11822" s="T158">num</ta>
            <ta e="T161" id="Seg_11823" s="T160">num</ta>
            <ta e="T162" id="Seg_11824" s="T161">num</ta>
            <ta e="T163" id="Seg_11825" s="T162">num</ta>
            <ta e="T164" id="Seg_11826" s="T163">dempro</ta>
            <ta e="T165" id="Seg_11827" s="T164">ptcl</ta>
            <ta e="T167" id="Seg_11828" s="T166">v</ta>
            <ta e="T168" id="Seg_11829" s="T167">n</ta>
            <ta e="T169" id="Seg_11830" s="T168">conj</ta>
            <ta e="T170" id="Seg_11831" s="T169">n</ta>
            <ta e="T171" id="Seg_11832" s="T170">v</ta>
            <ta e="T172" id="Seg_11833" s="T171">dempro</ta>
            <ta e="T173" id="Seg_11834" s="T172">adv</ta>
            <ta e="T174" id="Seg_11835" s="T173">ptcl</ta>
            <ta e="T175" id="Seg_11836" s="T174">v</ta>
            <ta e="T176" id="Seg_11837" s="T175">conj</ta>
            <ta e="T177" id="Seg_11838" s="T176">adv</ta>
            <ta e="T180" id="Seg_11839" s="T179">v</ta>
            <ta e="T183" id="Seg_11840" s="T182">pers</ta>
            <ta e="T184" id="Seg_11841" s="T183">n</ta>
            <ta e="T185" id="Seg_11842" s="T184">adv</ta>
            <ta e="T186" id="Seg_11843" s="T185">v</ta>
            <ta e="T187" id="Seg_11844" s="T186">n</ta>
            <ta e="T189" id="Seg_11845" s="T188">conj</ta>
            <ta e="T190" id="Seg_11846" s="T189">n</ta>
            <ta e="T191" id="Seg_11847" s="T190">adv</ta>
            <ta e="T192" id="Seg_11848" s="T191">adv</ta>
            <ta e="T193" id="Seg_11849" s="T192">v</ta>
            <ta e="T194" id="Seg_11850" s="T193">conj</ta>
            <ta e="T195" id="Seg_11851" s="T194">ptcl</ta>
            <ta e="T196" id="Seg_11852" s="T195">v</ta>
            <ta e="T198" id="Seg_11853" s="T197">que</ta>
            <ta e="T200" id="Seg_11854" s="T199">n</ta>
            <ta e="T201" id="Seg_11855" s="T200">que</ta>
            <ta e="T202" id="Seg_11856" s="T201">adv</ta>
            <ta e="T203" id="Seg_11857" s="T202">v</ta>
            <ta e="T204" id="Seg_11858" s="T203">v</ta>
            <ta e="T205" id="Seg_11859" s="T204">conj</ta>
            <ta e="T206" id="Seg_11860" s="T205">ptcl</ta>
            <ta e="T208" id="Seg_11861" s="T207">adv</ta>
            <ta e="T209" id="Seg_11862" s="T208">propr</ta>
            <ta e="T210" id="Seg_11863" s="T209">n</ta>
            <ta e="T211" id="Seg_11864" s="T210">ptcl</ta>
            <ta e="T212" id="Seg_11865" s="T211">v</ta>
            <ta e="T213" id="Seg_11866" s="T212">n</ta>
            <ta e="T214" id="Seg_11867" s="T213">n</ta>
            <ta e="T215" id="Seg_11868" s="T214">n</ta>
            <ta e="T216" id="Seg_11869" s="T215">adj</ta>
            <ta e="T219" id="Seg_11870" s="T218">n</ta>
            <ta e="T220" id="Seg_11871" s="T219">n</ta>
            <ta e="T221" id="Seg_11872" s="T220">v</ta>
            <ta e="T222" id="Seg_11873" s="T221">conj</ta>
            <ta e="T223" id="Seg_11874" s="T222">n</ta>
            <ta e="T224" id="Seg_11875" s="T223">v</ta>
            <ta e="T225" id="Seg_11876" s="T224">n</ta>
            <ta e="T227" id="Seg_11877" s="T226">que</ta>
            <ta e="T228" id="Seg_11878" s="T227">ptcl</ta>
            <ta e="T229" id="Seg_11879" s="T228">v</ta>
            <ta e="T230" id="Seg_11880" s="T229">ptcl</ta>
            <ta e="T231" id="Seg_11881" s="T230">v</ta>
            <ta e="T232" id="Seg_11882" s="T231">v</ta>
            <ta e="T233" id="Seg_11883" s="T232">adv</ta>
            <ta e="T234" id="Seg_11884" s="T233">ptcl</ta>
            <ta e="T239" id="Seg_11885" s="T238">conj</ta>
            <ta e="T240" id="Seg_11886" s="T239">refl</ta>
            <ta e="T241" id="Seg_11887" s="T240">n</ta>
            <ta e="T242" id="Seg_11888" s="T241">ptcl</ta>
            <ta e="T243" id="Seg_11889" s="T242">v</ta>
            <ta e="T245" id="Seg_11890" s="T244">adv</ta>
            <ta e="T246" id="Seg_11891" s="T245">dempro</ta>
            <ta e="T247" id="Seg_11892" s="T246">n</ta>
            <ta e="T248" id="Seg_11893" s="T247">n</ta>
            <ta e="T249" id="Seg_11894" s="T248">num</ta>
            <ta e="T250" id="Seg_11895" s="T249">n</ta>
            <ta e="T251" id="Seg_11896" s="T250">v</ta>
            <ta e="T252" id="Seg_11897" s="T251">conj</ta>
            <ta e="T253" id="Seg_11898" s="T252">adv</ta>
            <ta e="T254" id="Seg_11899" s="T253">que</ta>
            <ta e="T255" id="Seg_11900" s="T254">ptcl</ta>
            <ta e="T256" id="Seg_11901" s="T255">v</ta>
            <ta e="T257" id="Seg_11902" s="T256">dempro</ta>
            <ta e="T258" id="Seg_11903" s="T257">n</ta>
            <ta e="T260" id="Seg_11904" s="T259">pers</ta>
            <ta e="T261" id="Seg_11905" s="T260">pers</ta>
            <ta e="T262" id="Seg_11906" s="T261">v</ta>
            <ta e="T263" id="Seg_11907" s="T262">pers</ta>
            <ta e="T264" id="Seg_11908" s="T263">pers</ta>
            <ta e="T265" id="Seg_11909" s="T264">v</ta>
            <ta e="T266" id="Seg_11910" s="T265">adv</ta>
            <ta e="T267" id="Seg_11911" s="T266">v</ta>
            <ta e="T268" id="Seg_11912" s="T267">conj</ta>
            <ta e="T269" id="Seg_11913" s="T268">pers</ta>
            <ta e="T270" id="Seg_11914" s="T269">pers</ta>
            <ta e="T271" id="Seg_11915" s="T270">v</ta>
            <ta e="T272" id="Seg_11916" s="T271">v</ta>
            <ta e="T273" id="Seg_11917" s="T272">ptcl</ta>
            <ta e="T275" id="Seg_11918" s="T274">v</ta>
            <ta e="T276" id="Seg_11919" s="T275">v</ta>
            <ta e="T277" id="Seg_11920" s="T276">ptcl</ta>
            <ta e="T278" id="Seg_11921" s="T277">adj</ta>
            <ta e="T279" id="Seg_11922" s="T278">n</ta>
            <ta e="T280" id="Seg_11923" s="T279">v</ta>
            <ta e="T281" id="Seg_11924" s="T280">dempro</ta>
            <ta e="T282" id="Seg_11925" s="T281">v</ta>
            <ta e="T283" id="Seg_11926" s="T282">propr</ta>
            <ta e="T284" id="Seg_11927" s="T283">dempro</ta>
            <ta e="T285" id="Seg_11928" s="T284">ptcl</ta>
            <ta e="T286" id="Seg_11929" s="T285">n</ta>
            <ta e="T287" id="Seg_11930" s="T286">v</ta>
            <ta e="T288" id="Seg_11931" s="T287">n</ta>
            <ta e="T289" id="Seg_11932" s="T288">v</ta>
            <ta e="T290" id="Seg_11933" s="T289">v</ta>
            <ta e="T291" id="Seg_11934" s="T290">ptcl</ta>
            <ta e="T292" id="Seg_11935" s="T291">v</ta>
            <ta e="T293" id="Seg_11936" s="T292">adv</ta>
            <ta e="T294" id="Seg_11937" s="T293">v</ta>
            <ta e="T295" id="Seg_11938" s="T294">v</ta>
            <ta e="T296" id="Seg_11939" s="T295">n</ta>
            <ta e="T297" id="Seg_11940" s="T296">v</ta>
            <ta e="T298" id="Seg_11941" s="T297">v</ta>
            <ta e="T299" id="Seg_11942" s="T298">pers</ta>
            <ta e="T300" id="Seg_11943" s="T299">refl</ta>
            <ta e="T301" id="Seg_11944" s="T300">ptcl</ta>
            <ta e="T302" id="Seg_11945" s="T301">adv</ta>
            <ta e="T303" id="Seg_11946" s="T302">adj</ta>
            <ta e="T305" id="Seg_11947" s="T304">n</ta>
            <ta e="T306" id="Seg_11948" s="T305">v</ta>
            <ta e="T307" id="Seg_11949" s="T306">pers</ta>
            <ta e="T308" id="Seg_11950" s="T307">dempro</ta>
            <ta e="T309" id="Seg_11951" s="T308">dempro</ta>
            <ta e="T310" id="Seg_11952" s="T309">v</ta>
            <ta e="T311" id="Seg_11953" s="T310">adv</ta>
            <ta e="T312" id="Seg_11954" s="T311">v</ta>
            <ta e="T313" id="Seg_11955" s="T312">v</ta>
            <ta e="T314" id="Seg_11956" s="T313">v</ta>
            <ta e="T316" id="Seg_11957" s="T314">adv</ta>
            <ta e="T317" id="Seg_11958" s="T316">adv</ta>
            <ta e="T318" id="Seg_11959" s="T317">adj</ta>
            <ta e="T319" id="Seg_11960" s="T318">n</ta>
            <ta e="T320" id="Seg_11961" s="T319">n</ta>
            <ta e="T321" id="Seg_11962" s="T320">v</ta>
            <ta e="T322" id="Seg_11963" s="T321">v</ta>
            <ta e="T323" id="Seg_11964" s="T322">pers</ta>
            <ta e="T324" id="Seg_11965" s="T323">adv</ta>
            <ta e="T325" id="Seg_11966" s="T324">adj</ta>
            <ta e="T326" id="Seg_11967" s="T325">pers</ta>
            <ta e="T327" id="Seg_11968" s="T326">n</ta>
            <ta e="T328" id="Seg_11969" s="T327">v</ta>
            <ta e="T329" id="Seg_11970" s="T328">dempro</ta>
            <ta e="T330" id="Seg_11971" s="T329">v</ta>
            <ta e="T331" id="Seg_11972" s="T330">v</ta>
            <ta e="T332" id="Seg_11973" s="T331">adv</ta>
            <ta e="T333" id="Seg_11974" s="T332">v</ta>
            <ta e="T334" id="Seg_11975" s="T333">v</ta>
            <ta e="T338" id="Seg_11976" s="T337">n</ta>
            <ta e="T339" id="Seg_11977" s="T338">v</ta>
            <ta e="T340" id="Seg_11978" s="T339">v</ta>
            <ta e="T341" id="Seg_11979" s="T340">pers</ta>
            <ta e="T342" id="Seg_11980" s="T341">dempro</ta>
            <ta e="T343" id="Seg_11981" s="T342">dempro</ta>
            <ta e="T344" id="Seg_11982" s="T343">v</ta>
            <ta e="T345" id="Seg_11983" s="T344">adj</ta>
            <ta e="T346" id="Seg_11984" s="T345">n</ta>
            <ta e="T347" id="Seg_11985" s="T346">v</ta>
            <ta e="T348" id="Seg_11986" s="T347">adv</ta>
            <ta e="T349" id="Seg_11987" s="T348">adv</ta>
            <ta e="T350" id="Seg_11988" s="T349">v</ta>
            <ta e="T351" id="Seg_11989" s="T350">n</ta>
            <ta e="T352" id="Seg_11990" s="T351">v</ta>
            <ta e="T353" id="Seg_11991" s="T352">v</ta>
            <ta e="T354" id="Seg_11992" s="T353">pers</ta>
            <ta e="T355" id="Seg_11993" s="T354">adv</ta>
            <ta e="T356" id="Seg_11994" s="T355">dempro</ta>
            <ta e="T357" id="Seg_11995" s="T356">n</ta>
            <ta e="T358" id="Seg_11996" s="T357">v</ta>
            <ta e="T359" id="Seg_11997" s="T358">adv</ta>
            <ta e="T360" id="Seg_11998" s="T359">v</ta>
            <ta e="T361" id="Seg_11999" s="T360">adv</ta>
            <ta e="T362" id="Seg_12000" s="T361">ptcl</ta>
            <ta e="T363" id="Seg_12001" s="T362">n</ta>
            <ta e="T364" id="Seg_12002" s="T363">v</ta>
            <ta e="T365" id="Seg_12003" s="T364">conj</ta>
            <ta e="T366" id="Seg_12004" s="T365">n</ta>
            <ta e="T367" id="Seg_12005" s="T366">adv</ta>
            <ta e="T368" id="Seg_12006" s="T367">adj</ta>
            <ta e="T369" id="Seg_12007" s="T368">dempro</ta>
            <ta e="T370" id="Seg_12008" s="T369">n</ta>
            <ta e="T371" id="Seg_12009" s="T370">v</ta>
            <ta e="T372" id="Seg_12010" s="T371">n</ta>
            <ta e="T373" id="Seg_12011" s="T372">v</ta>
            <ta e="T374" id="Seg_12012" s="T373">v</ta>
            <ta e="T375" id="Seg_12013" s="T374">dempro</ta>
            <ta e="T376" id="Seg_12014" s="T375">v</ta>
            <ta e="T377" id="Seg_12015" s="T376">n</ta>
            <ta e="T378" id="Seg_12016" s="T377">que</ta>
            <ta e="T379" id="Seg_12017" s="T378">adv</ta>
            <ta e="T380" id="Seg_12018" s="T379">v</ta>
            <ta e="T381" id="Seg_12019" s="T380">pers</ta>
            <ta e="T383" id="Seg_12020" s="T382">n</ta>
            <ta e="T384" id="Seg_12021" s="T383">v</ta>
            <ta e="T385" id="Seg_12022" s="T384">n</ta>
            <ta e="T386" id="Seg_12023" s="T385">v</ta>
            <ta e="T387" id="Seg_12024" s="T386">adv</ta>
            <ta e="T388" id="Seg_12025" s="T387">adj</ta>
            <ta e="T389" id="Seg_12026" s="T388">v</ta>
            <ta e="T390" id="Seg_12027" s="T389">adv</ta>
            <ta e="T391" id="Seg_12028" s="T390">adv</ta>
            <ta e="T392" id="Seg_12029" s="T391">ptcl</ta>
            <ta e="T393" id="Seg_12030" s="T392">v</ta>
            <ta e="T394" id="Seg_12031" s="T393">v</ta>
            <ta e="T395" id="Seg_12032" s="T394">n</ta>
            <ta e="T396" id="Seg_12033" s="T395">v</ta>
            <ta e="T397" id="Seg_12034" s="T396">que</ta>
            <ta e="T398" id="Seg_12035" s="T397">dempro</ta>
            <ta e="T399" id="Seg_12036" s="T398">adv</ta>
            <ta e="T400" id="Seg_12037" s="T399">dempro</ta>
            <ta e="T401" id="Seg_12038" s="T400">v</ta>
            <ta e="T403" id="Seg_12039" s="T402">v</ta>
            <ta e="T404" id="Seg_12040" s="T403">que</ta>
            <ta e="T405" id="Seg_12041" s="T404">pers</ta>
            <ta e="T406" id="Seg_12042" s="T405">adv</ta>
            <ta e="T407" id="Seg_12043" s="T406">dempro</ta>
            <ta e="T408" id="Seg_12044" s="T407">n</ta>
            <ta e="T409" id="Seg_12045" s="T408">n</ta>
            <ta e="T410" id="Seg_12046" s="T409">v</ta>
            <ta e="T412" id="Seg_12047" s="T411">dempro</ta>
            <ta e="T413" id="Seg_12048" s="T412">n</ta>
            <ta e="T414" id="Seg_12049" s="T413">v</ta>
            <ta e="T415" id="Seg_12050" s="T414">v</ta>
            <ta e="T416" id="Seg_12051" s="T415">dempro</ta>
            <ta e="T417" id="Seg_12052" s="T416">v</ta>
            <ta e="T418" id="Seg_12053" s="T417">ptcl</ta>
            <ta e="T419" id="Seg_12054" s="T418">dempro</ta>
            <ta e="T420" id="Seg_12055" s="T419">n</ta>
            <ta e="T421" id="Seg_12056" s="T420">n</ta>
            <ta e="T422" id="Seg_12057" s="T421">v</ta>
            <ta e="T423" id="Seg_12058" s="T422">ptcl</ta>
            <ta e="T424" id="Seg_12059" s="T423">v</ta>
            <ta e="T425" id="Seg_12060" s="T424">ptcl</ta>
            <ta e="T426" id="Seg_12061" s="T425">ptcl</ta>
            <ta e="T427" id="Seg_12062" s="T426">adj</ta>
            <ta e="T428" id="Seg_12063" s="T427">v</ta>
            <ta e="T430" id="Seg_12064" s="T429">ptcl</ta>
            <ta e="T431" id="Seg_12065" s="T430">v</ta>
            <ta e="T432" id="Seg_12066" s="T431">dempro</ta>
            <ta e="T433" id="Seg_12067" s="T432">v</ta>
            <ta e="T435" id="Seg_12068" s="T434">v</ta>
            <ta e="T436" id="Seg_12069" s="T435">ptcl</ta>
            <ta e="T437" id="Seg_12070" s="T436">n</ta>
            <ta e="T438" id="Seg_12071" s="T437">ptcl</ta>
            <ta e="T439" id="Seg_12072" s="T438">v</ta>
            <ta e="T440" id="Seg_12073" s="T439">n</ta>
            <ta e="T441" id="Seg_12074" s="T440">ptcl</ta>
            <ta e="T442" id="Seg_12075" s="T441">v</ta>
            <ta e="T443" id="Seg_12076" s="T442">adv</ta>
            <ta e="T444" id="Seg_12077" s="T443">ptcl</ta>
            <ta e="T445" id="Seg_12078" s="T444">ptcl</ta>
            <ta e="T446" id="Seg_12079" s="T445">adj</ta>
            <ta e="T447" id="Seg_12080" s="T446">v</ta>
            <ta e="T449" id="Seg_12081" s="T448">v</ta>
            <ta e="T451" id="Seg_12082" s="T450">n</ta>
            <ta e="T452" id="Seg_12083" s="T451">dempro</ta>
            <ta e="T453" id="Seg_12084" s="T452">ptcl</ta>
            <ta e="T454" id="Seg_12085" s="T453">adv</ta>
            <ta e="T455" id="Seg_12086" s="T454">n</ta>
            <ta e="T456" id="Seg_12087" s="T455">v</ta>
            <ta e="T457" id="Seg_12088" s="T456">adv</ta>
            <ta e="T1072" id="Seg_12089" s="T457">v</ta>
            <ta e="T458" id="Seg_12090" s="T1072">v</ta>
            <ta e="T459" id="Seg_12091" s="T458">adv</ta>
            <ta e="T460" id="Seg_12092" s="T459">dempro</ta>
            <ta e="T461" id="Seg_12093" s="T460">n</ta>
            <ta e="T462" id="Seg_12094" s="T461">v</ta>
            <ta e="T463" id="Seg_12095" s="T462">adj</ta>
            <ta e="T464" id="Seg_12096" s="T463">n</ta>
            <ta e="T465" id="Seg_12097" s="T464">dempro</ta>
            <ta e="T466" id="Seg_12098" s="T465">n</ta>
            <ta e="T467" id="Seg_12099" s="T466">n</ta>
            <ta e="T468" id="Seg_12100" s="T467">ptcl</ta>
            <ta e="T469" id="Seg_12101" s="T468">v</ta>
            <ta e="T470" id="Seg_12102" s="T469">adv</ta>
            <ta e="T471" id="Seg_12103" s="T470">v</ta>
            <ta e="T472" id="Seg_12104" s="T471">v</ta>
            <ta e="T473" id="Seg_12105" s="T472">n</ta>
            <ta e="T474" id="Seg_12106" s="T473">ptcl</ta>
            <ta e="T475" id="Seg_12107" s="T474">n</ta>
            <ta e="T476" id="Seg_12108" s="T475">n</ta>
            <ta e="T477" id="Seg_12109" s="T476">ptcl</ta>
            <ta e="T478" id="Seg_12110" s="T477">n</ta>
            <ta e="T479" id="Seg_12111" s="T478">ptcl</ta>
            <ta e="T480" id="Seg_12112" s="T479">v</ta>
            <ta e="T481" id="Seg_12113" s="T480">dempro</ta>
            <ta e="T482" id="Seg_12114" s="T481">dempro</ta>
            <ta e="T483" id="Seg_12115" s="T482">ptcl</ta>
            <ta e="T484" id="Seg_12116" s="T483">n</ta>
            <ta e="T485" id="Seg_12117" s="T484">v</ta>
            <ta e="T486" id="Seg_12118" s="T485">adj</ta>
            <ta e="T487" id="Seg_12119" s="T486">n</ta>
            <ta e="T488" id="Seg_12120" s="T487">dempro</ta>
            <ta e="T489" id="Seg_12121" s="T488">dempro</ta>
            <ta e="T490" id="Seg_12122" s="T489">v</ta>
            <ta e="T491" id="Seg_12123" s="T490">dempro</ta>
            <ta e="T492" id="Seg_12124" s="T491">ptcl</ta>
            <ta e="T493" id="Seg_12125" s="T492">v</ta>
            <ta e="T494" id="Seg_12126" s="T493">adv</ta>
            <ta e="T495" id="Seg_12127" s="T494">v</ta>
            <ta e="T497" id="Seg_12128" s="T495">v</ta>
            <ta e="T499" id="Seg_12129" s="T498">adv</ta>
            <ta e="T500" id="Seg_12130" s="T499">n</ta>
            <ta e="T503" id="Seg_12131" s="T502">v</ta>
            <ta e="T504" id="Seg_12132" s="T503">n</ta>
            <ta e="T505" id="Seg_12133" s="T504">ptcl</ta>
            <ta e="T506" id="Seg_12134" s="T505">n</ta>
            <ta e="T507" id="Seg_12135" s="T506">dempro</ta>
            <ta e="T508" id="Seg_12136" s="T507">v</ta>
            <ta e="T509" id="Seg_12137" s="T508">dempro</ta>
            <ta e="T510" id="Seg_12138" s="T509">v</ta>
            <ta e="T511" id="Seg_12139" s="T510">n</ta>
            <ta e="T512" id="Seg_12140" s="T511">conj</ta>
            <ta e="T513" id="Seg_12141" s="T512">dempro</ta>
            <ta e="T514" id="Seg_12142" s="T513">n</ta>
            <ta e="T515" id="Seg_12143" s="T514">v</ta>
            <ta e="T516" id="Seg_12144" s="T515">n</ta>
            <ta e="T517" id="Seg_12145" s="T516">n</ta>
            <ta e="T518" id="Seg_12146" s="T517">v</ta>
            <ta e="T519" id="Seg_12147" s="T518">adv</ta>
            <ta e="T520" id="Seg_12148" s="T519">dempro</ta>
            <ta e="T521" id="Seg_12149" s="T520">n</ta>
            <ta e="T522" id="Seg_12150" s="T521">ptcl</ta>
            <ta e="T523" id="Seg_12151" s="T522">n</ta>
            <ta e="T524" id="Seg_12152" s="T523">v</ta>
            <ta e="T525" id="Seg_12153" s="T524">v</ta>
            <ta e="T526" id="Seg_12154" s="T525">dempro</ta>
            <ta e="T527" id="Seg_12155" s="T526">v</ta>
            <ta e="T528" id="Seg_12156" s="T527">n</ta>
            <ta e="T529" id="Seg_12157" s="T528">v</ta>
            <ta e="T530" id="Seg_12158" s="T529">dempro</ta>
            <ta e="T531" id="Seg_12159" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_12160" s="T531">n</ta>
            <ta e="T533" id="Seg_12161" s="T532">n</ta>
            <ta e="T534" id="Seg_12162" s="T533">v</ta>
            <ta e="T535" id="Seg_12163" s="T534">conj</ta>
            <ta e="T536" id="Seg_12164" s="T535">n</ta>
            <ta e="T537" id="Seg_12165" s="T536">n</ta>
            <ta e="T538" id="Seg_12166" s="T537">n</ta>
            <ta e="T539" id="Seg_12167" s="T538">ptcl</ta>
            <ta e="T540" id="Seg_12168" s="T539">v</ta>
            <ta e="T541" id="Seg_12169" s="T540">n</ta>
            <ta e="T543" id="Seg_12170" s="T542">adv</ta>
            <ta e="T544" id="Seg_12171" s="T543">dempro</ta>
            <ta e="T545" id="Seg_12172" s="T544">n</ta>
            <ta e="T546" id="Seg_12173" s="T545">v</ta>
            <ta e="T547" id="Seg_12174" s="T546">n</ta>
            <ta e="T549" id="Seg_12175" s="T548">v</ta>
            <ta e="T550" id="Seg_12176" s="T549">v</ta>
            <ta e="T551" id="Seg_12177" s="T550">pers</ta>
            <ta e="T552" id="Seg_12178" s="T551">v</ta>
            <ta e="T553" id="Seg_12179" s="T552">n</ta>
            <ta e="T554" id="Seg_12180" s="T553">n</ta>
            <ta e="T555" id="Seg_12181" s="T554">n</ta>
            <ta e="T556" id="Seg_12182" s="T555">v</ta>
            <ta e="T557" id="Seg_12183" s="T556">n</ta>
            <ta e="T558" id="Seg_12184" s="T557">quant</ta>
            <ta e="T559" id="Seg_12185" s="T558">v</ta>
            <ta e="T560" id="Seg_12186" s="T559">que</ta>
            <ta e="T561" id="Seg_12187" s="T560">pers</ta>
            <ta e="T562" id="Seg_12188" s="T561">ptcl</ta>
            <ta e="T563" id="Seg_12189" s="T562">adv</ta>
            <ta e="T564" id="Seg_12190" s="T563">adv</ta>
            <ta e="T565" id="Seg_12191" s="T564">n</ta>
            <ta e="T566" id="Seg_12192" s="T565">v</ta>
            <ta e="T568" id="Seg_12193" s="T567">adv</ta>
            <ta e="T569" id="Seg_12194" s="T568">adj</ta>
            <ta e="T570" id="Seg_12195" s="T569">n</ta>
            <ta e="T571" id="Seg_12196" s="T570">n</ta>
            <ta e="T572" id="Seg_12197" s="T571">v</ta>
            <ta e="T574" id="Seg_12198" s="T572">conj</ta>
            <ta e="T576" id="Seg_12199" s="T575">adv</ta>
            <ta e="T577" id="Seg_12200" s="T576">v</ta>
            <ta e="T578" id="Seg_12201" s="T577">n</ta>
            <ta e="T579" id="Seg_12202" s="T578">dempro</ta>
            <ta e="T580" id="Seg_12203" s="T579">v</ta>
            <ta e="T581" id="Seg_12204" s="T580">v</ta>
            <ta e="T583" id="Seg_12205" s="T582">conj</ta>
            <ta e="T584" id="Seg_12206" s="T583">v</ta>
            <ta e="T585" id="Seg_12207" s="T584">n</ta>
            <ta e="T586" id="Seg_12208" s="T585">num</ta>
            <ta e="T587" id="Seg_12209" s="T586">n</ta>
            <ta e="T588" id="Seg_12210" s="T587">v</ta>
            <ta e="T589" id="Seg_12211" s="T588">v</ta>
            <ta e="T590" id="Seg_12212" s="T589">dempro</ta>
            <ta e="T591" id="Seg_12213" s="T590">n</ta>
            <ta e="T592" id="Seg_12214" s="T591">v</ta>
            <ta e="T593" id="Seg_12215" s="T592">dempro</ta>
            <ta e="T594" id="Seg_12216" s="T593">v</ta>
            <ta e="T595" id="Seg_12217" s="T594">v</ta>
            <ta e="T596" id="Seg_12218" s="T595">n</ta>
            <ta e="T597" id="Seg_12219" s="T596">conj</ta>
            <ta e="T598" id="Seg_12220" s="T597">n</ta>
            <ta e="T599" id="Seg_12221" s="T598">n</ta>
            <ta e="T600" id="Seg_12222" s="T599">ptcl</ta>
            <ta e="T601" id="Seg_12223" s="T600">n</ta>
            <ta e="T602" id="Seg_12224" s="T601">n</ta>
            <ta e="T604" id="Seg_12225" s="T603">v</ta>
            <ta e="T605" id="Seg_12226" s="T604">v</ta>
            <ta e="T606" id="Seg_12227" s="T605">adv</ta>
            <ta e="T607" id="Seg_12228" s="T606">v</ta>
            <ta e="T608" id="Seg_12229" s="T607">n</ta>
            <ta e="T610" id="Seg_12230" s="T609">que</ta>
            <ta e="T611" id="Seg_12231" s="T610">n</ta>
            <ta e="T612" id="Seg_12232" s="T611">v</ta>
            <ta e="T613" id="Seg_12233" s="T612">dempro</ta>
            <ta e="T614" id="Seg_12234" s="T613">n</ta>
            <ta e="T616" id="Seg_12235" s="T615">v</ta>
            <ta e="T617" id="Seg_12236" s="T616">conj</ta>
            <ta e="T618" id="Seg_12237" s="T617">n</ta>
            <ta e="T620" id="Seg_12238" s="T619">n</ta>
            <ta e="T621" id="Seg_12239" s="T620">v</ta>
            <ta e="T622" id="Seg_12240" s="T621">conj</ta>
            <ta e="T623" id="Seg_12241" s="T622">dempro</ta>
            <ta e="T624" id="Seg_12242" s="T623">n</ta>
            <ta e="T626" id="Seg_12243" s="T624">ptcl</ta>
            <ta e="T628" id="Seg_12244" s="T627">adj</ta>
            <ta e="T629" id="Seg_12245" s="T628">v</ta>
            <ta e="T630" id="Seg_12246" s="T629">adv</ta>
            <ta e="T631" id="Seg_12247" s="T630">dempro</ta>
            <ta e="T632" id="Seg_12248" s="T631">v</ta>
            <ta e="T633" id="Seg_12249" s="T632">ptcl</ta>
            <ta e="T634" id="Seg_12250" s="T633">v</ta>
            <ta e="T635" id="Seg_12251" s="T634">v</ta>
            <ta e="T636" id="Seg_12252" s="T635">dempro</ta>
            <ta e="T637" id="Seg_12253" s="T636">n</ta>
            <ta e="T638" id="Seg_12254" s="T637">num</ta>
            <ta e="T639" id="Seg_12255" s="T638">n</ta>
            <ta e="T640" id="Seg_12256" s="T639">v</ta>
            <ta e="T641" id="Seg_12257" s="T640">adv</ta>
            <ta e="T642" id="Seg_12258" s="T641">num</ta>
            <ta e="T644" id="Seg_12259" s="T643">adv</ta>
            <ta e="T645" id="Seg_12260" s="T644">n</ta>
            <ta e="T646" id="Seg_12261" s="T645">v</ta>
            <ta e="T647" id="Seg_12262" s="T646">que</ta>
            <ta e="T648" id="Seg_12263" s="T647">pers</ta>
            <ta e="T649" id="Seg_12264" s="T648">v</ta>
            <ta e="T650" id="Seg_12265" s="T649">v</ta>
            <ta e="T651" id="Seg_12266" s="T650">dempro</ta>
            <ta e="T652" id="Seg_12267" s="T651">ptcl</ta>
            <ta e="T654" id="Seg_12268" s="T653">v</ta>
            <ta e="T655" id="Seg_12269" s="T654">dempro</ta>
            <ta e="T656" id="Seg_12270" s="T655">dempro</ta>
            <ta e="T657" id="Seg_12271" s="T656">n</ta>
            <ta e="T658" id="Seg_12272" s="T657">n</ta>
            <ta e="T659" id="Seg_12273" s="T658">v</ta>
            <ta e="T660" id="Seg_12274" s="T659">adv</ta>
            <ta e="T661" id="Seg_12275" s="T660">n</ta>
            <ta e="T662" id="Seg_12276" s="T661">v</ta>
            <ta e="T663" id="Seg_12277" s="T662">adv</ta>
            <ta e="T664" id="Seg_12278" s="T663">dempro</ta>
            <ta e="T666" id="Seg_12279" s="T665">dempro</ta>
            <ta e="T667" id="Seg_12280" s="T666">n</ta>
            <ta e="T669" id="Seg_12281" s="T668">v</ta>
            <ta e="T670" id="Seg_12282" s="T669">dempro</ta>
            <ta e="T672" id="Seg_12283" s="T671">n</ta>
            <ta e="T673" id="Seg_12284" s="T672">v</ta>
            <ta e="T674" id="Seg_12285" s="T673">que</ta>
            <ta e="T675" id="Seg_12286" s="T674">v</ta>
            <ta e="T676" id="Seg_12287" s="T675">n</ta>
            <ta e="T677" id="Seg_12288" s="T676">conj</ta>
            <ta e="T678" id="Seg_12289" s="T677">n</ta>
            <ta e="T680" id="Seg_12290" s="T679">v</ta>
            <ta e="T681" id="Seg_12291" s="T680">n</ta>
            <ta e="T682" id="Seg_12292" s="T681">n</ta>
            <ta e="T683" id="Seg_12293" s="T682">dempro</ta>
            <ta e="T684" id="Seg_12294" s="T683">que</ta>
            <ta e="T685" id="Seg_12295" s="T684">v</ta>
            <ta e="T686" id="Seg_12296" s="T685">adv</ta>
            <ta e="T687" id="Seg_12297" s="T686">num</ta>
            <ta e="T688" id="Seg_12298" s="T687">n</ta>
            <ta e="T689" id="Seg_12299" s="T688">dempro</ta>
            <ta e="T690" id="Seg_12300" s="T689">v</ta>
            <ta e="T691" id="Seg_12301" s="T690">v</ta>
            <ta e="T692" id="Seg_12302" s="T691">v</ta>
            <ta e="T693" id="Seg_12303" s="T692">adv</ta>
            <ta e="T694" id="Seg_12304" s="T693">n</ta>
            <ta e="T695" id="Seg_12305" s="T694">v</ta>
            <ta e="T696" id="Seg_12306" s="T695">n</ta>
            <ta e="T697" id="Seg_12307" s="T696">v</ta>
            <ta e="T698" id="Seg_12308" s="T697">v</ta>
            <ta e="T699" id="Seg_12309" s="T698">ptcl</ta>
            <ta e="T700" id="Seg_12310" s="T699">pers</ta>
            <ta e="T701" id="Seg_12311" s="T700">refl</ta>
            <ta e="T702" id="Seg_12312" s="T701">adv</ta>
            <ta e="T703" id="Seg_12313" s="T702">v</ta>
            <ta e="T704" id="Seg_12314" s="T703">dempro</ta>
            <ta e="T705" id="Seg_12315" s="T704">n</ta>
            <ta e="T706" id="Seg_12316" s="T705">v</ta>
            <ta e="T707" id="Seg_12317" s="T706">v</ta>
            <ta e="T708" id="Seg_12318" s="T707">adv</ta>
            <ta e="T709" id="Seg_12319" s="T708">n</ta>
            <ta e="T710" id="Seg_12320" s="T709">v</ta>
            <ta e="T711" id="Seg_12321" s="T710">n</ta>
            <ta e="T712" id="Seg_12322" s="T711">v</ta>
            <ta e="T713" id="Seg_12323" s="T712">adv</ta>
            <ta e="T714" id="Seg_12324" s="T713">n</ta>
            <ta e="T715" id="Seg_12325" s="T714">v</ta>
            <ta e="T716" id="Seg_12326" s="T715">n</ta>
            <ta e="T717" id="Seg_12327" s="T716">v</ta>
            <ta e="T718" id="Seg_12328" s="T717">dempro</ta>
            <ta e="T719" id="Seg_12329" s="T718">v</ta>
            <ta e="T720" id="Seg_12330" s="T719">ptcl</ta>
            <ta e="T721" id="Seg_12331" s="T720">v</ta>
            <ta e="T722" id="Seg_12332" s="T721">v</ta>
            <ta e="T723" id="Seg_12333" s="T722">ptcl</ta>
            <ta e="T724" id="Seg_12334" s="T723">que</ta>
            <ta e="T725" id="Seg_12335" s="T724">v</ta>
            <ta e="T726" id="Seg_12336" s="T725">quant</ta>
            <ta e="T727" id="Seg_12337" s="T726">n</ta>
            <ta e="T729" id="Seg_12338" s="T728">v</ta>
            <ta e="T730" id="Seg_12339" s="T729">n</ta>
            <ta e="T731" id="Seg_12340" s="T730">n</ta>
            <ta e="T732" id="Seg_12341" s="T731">adv</ta>
            <ta e="T733" id="Seg_12342" s="T732">v</ta>
            <ta e="T734" id="Seg_12343" s="T733">n</ta>
            <ta e="T735" id="Seg_12344" s="T734">v</ta>
            <ta e="T737" id="Seg_12345" s="T736">v</ta>
            <ta e="T738" id="Seg_12346" s="T737">n</ta>
            <ta e="T739" id="Seg_12347" s="T738">n</ta>
            <ta e="T740" id="Seg_12348" s="T739">v</ta>
            <ta e="T741" id="Seg_12349" s="T740">dempro</ta>
            <ta e="T742" id="Seg_12350" s="T741">v</ta>
            <ta e="T743" id="Seg_12351" s="T742">v</ta>
            <ta e="T744" id="Seg_12352" s="T743">que</ta>
            <ta e="T745" id="Seg_12353" s="T744">v</ta>
            <ta e="T746" id="Seg_12354" s="T745">n</ta>
            <ta e="T747" id="Seg_12355" s="T746">v</ta>
            <ta e="T748" id="Seg_12356" s="T747">n</ta>
            <ta e="T749" id="Seg_12357" s="T748">v</ta>
            <ta e="T750" id="Seg_12358" s="T749">n</ta>
            <ta e="T751" id="Seg_12359" s="T750">v</ta>
            <ta e="T752" id="Seg_12360" s="T751">que</ta>
            <ta e="T753" id="Seg_12361" s="T752">adv</ta>
            <ta e="T754" id="Seg_12362" s="T753">v</ta>
            <ta e="T755" id="Seg_12363" s="T754">n</ta>
            <ta e="T756" id="Seg_12364" s="T755">ptcl</ta>
            <ta e="T757" id="Seg_12365" s="T756">n</ta>
            <ta e="T758" id="Seg_12366" s="T757">v</ta>
            <ta e="T759" id="Seg_12367" s="T758">n</ta>
            <ta e="T760" id="Seg_12368" s="T759">ptcl</ta>
            <ta e="T761" id="Seg_12369" s="T760">n</ta>
            <ta e="T762" id="Seg_12370" s="T761">v</ta>
            <ta e="T763" id="Seg_12371" s="T762">adv</ta>
            <ta e="T764" id="Seg_12372" s="T763">dempro</ta>
            <ta e="T765" id="Seg_12373" s="T764">n</ta>
            <ta e="T766" id="Seg_12374" s="T765">v</ta>
            <ta e="T767" id="Seg_12375" s="T766">v</ta>
            <ta e="T768" id="Seg_12376" s="T767">dempro</ta>
            <ta e="T769" id="Seg_12377" s="T768">dempro</ta>
            <ta e="T770" id="Seg_12378" s="T769">v</ta>
            <ta e="T771" id="Seg_12379" s="T770">v</ta>
            <ta e="T772" id="Seg_12380" s="T771">v</ta>
            <ta e="T773" id="Seg_12381" s="T772">dempro</ta>
            <ta e="T774" id="Seg_12382" s="T773">v</ta>
            <ta e="T775" id="Seg_12383" s="T774">que</ta>
            <ta e="T776" id="Seg_12384" s="T775">pers</ta>
            <ta e="T777" id="Seg_12385" s="T776">n</ta>
            <ta e="T778" id="Seg_12386" s="T777">n</ta>
            <ta e="T779" id="Seg_12387" s="T778">v</ta>
            <ta e="T780" id="Seg_12388" s="T779">que</ta>
            <ta e="T781" id="Seg_12389" s="T780">pers</ta>
            <ta e="T782" id="Seg_12390" s="T781">v</ta>
            <ta e="T783" id="Seg_12391" s="T782">conj</ta>
            <ta e="T784" id="Seg_12392" s="T783">ptcl</ta>
            <ta e="T785" id="Seg_12393" s="T784">v</ta>
            <ta e="T786" id="Seg_12394" s="T785">pers</ta>
            <ta e="T787" id="Seg_12395" s="T786">n</ta>
            <ta e="T788" id="Seg_12396" s="T787">v</ta>
            <ta e="T789" id="Seg_12397" s="T788">v</ta>
            <ta e="T790" id="Seg_12398" s="T789">adv</ta>
            <ta e="T791" id="Seg_12399" s="T790">v</ta>
            <ta e="T792" id="Seg_12400" s="T791">dempro</ta>
            <ta e="T793" id="Seg_12401" s="T792">v</ta>
            <ta e="T794" id="Seg_12402" s="T793">dempro</ta>
            <ta e="T795" id="Seg_12403" s="T794">dempro</ta>
            <ta e="T796" id="Seg_12404" s="T795">n</ta>
            <ta e="T797" id="Seg_12405" s="T796">n</ta>
            <ta e="T798" id="Seg_12406" s="T797">ptcl</ta>
            <ta e="T799" id="Seg_12407" s="T798">v</ta>
            <ta e="T800" id="Seg_12408" s="T799">adv</ta>
            <ta e="T801" id="Seg_12409" s="T800">ptcl</ta>
            <ta e="T802" id="Seg_12410" s="T801">n</ta>
            <ta e="T803" id="Seg_12411" s="T802">v</ta>
            <ta e="T804" id="Seg_12412" s="T803">n</ta>
            <ta e="T805" id="Seg_12413" s="T804">v</ta>
            <ta e="T806" id="Seg_12414" s="T805">n</ta>
            <ta e="T807" id="Seg_12415" s="T806">v</ta>
            <ta e="T808" id="Seg_12416" s="T807">n</ta>
            <ta e="T809" id="Seg_12417" s="T808">v</ta>
            <ta e="T810" id="Seg_12418" s="T809">adv</ta>
            <ta e="T811" id="Seg_12419" s="T810">v</ta>
            <ta e="T812" id="Seg_12420" s="T811">refl</ta>
            <ta e="T813" id="Seg_12421" s="T812">n</ta>
            <ta e="T814" id="Seg_12422" s="T813">n</ta>
            <ta e="T817" id="Seg_12423" s="T816">v</ta>
            <ta e="T818" id="Seg_12424" s="T817">conj</ta>
            <ta e="T819" id="Seg_12425" s="T818">n</ta>
            <ta e="T820" id="Seg_12426" s="T819">n</ta>
            <ta e="T821" id="Seg_12427" s="T820">n</ta>
            <ta e="T823" id="Seg_12428" s="T822">n</ta>
            <ta e="T825" id="Seg_12429" s="T823">ptcl</ta>
            <ta e="T826" id="Seg_12430" s="T825">n</ta>
            <ta e="T828" id="Seg_12431" s="T827">n</ta>
            <ta e="T830" id="Seg_12432" s="T828">ptcl</ta>
            <ta e="T831" id="Seg_12433" s="T830">dempro</ta>
            <ta e="T832" id="Seg_12434" s="T831">n</ta>
            <ta e="T833" id="Seg_12435" s="T832">v</ta>
            <ta e="T834" id="Seg_12436" s="T833">dempro</ta>
            <ta e="T835" id="Seg_12437" s="T834">n</ta>
            <ta e="T836" id="Seg_12438" s="T835">v</ta>
            <ta e="T837" id="Seg_12439" s="T836">conj</ta>
            <ta e="T838" id="Seg_12440" s="T837">dempro</ta>
            <ta e="T839" id="Seg_12441" s="T838">v</ta>
            <ta e="T840" id="Seg_12442" s="T839">dempro</ta>
            <ta e="T841" id="Seg_12443" s="T840">dempro</ta>
            <ta e="T842" id="Seg_12444" s="T841">v</ta>
            <ta e="T843" id="Seg_12445" s="T842">ptcl</ta>
            <ta e="T844" id="Seg_12446" s="T843">n</ta>
            <ta e="T845" id="Seg_12447" s="T844">pers</ta>
            <ta e="T846" id="Seg_12448" s="T845">v</ta>
            <ta e="T847" id="Seg_12449" s="T846">ptcl</ta>
            <ta e="T848" id="Seg_12450" s="T847">n</ta>
            <ta e="T849" id="Seg_12451" s="T848">pers</ta>
            <ta e="T850" id="Seg_12452" s="T849">v</ta>
            <ta e="T851" id="Seg_12453" s="T850">n</ta>
            <ta e="T852" id="Seg_12454" s="T851">pers</ta>
            <ta e="T853" id="Seg_12455" s="T852">v</ta>
            <ta e="T854" id="Seg_12456" s="T853">n</ta>
            <ta e="T855" id="Seg_12457" s="T854">dempro</ta>
            <ta e="T856" id="Seg_12458" s="T855">pers</ta>
            <ta e="T857" id="Seg_12459" s="T856">v</ta>
            <ta e="T858" id="Seg_12460" s="T857">adv</ta>
            <ta e="T859" id="Seg_12461" s="T858">dempro</ta>
            <ta e="T860" id="Seg_12462" s="T859">v</ta>
            <ta e="T861" id="Seg_12463" s="T860">v</ta>
            <ta e="T862" id="Seg_12464" s="T861">adv</ta>
            <ta e="T863" id="Seg_12465" s="T862">n</ta>
            <ta e="T864" id="Seg_12466" s="T863">n</ta>
            <ta e="T865" id="Seg_12467" s="T864">n</ta>
            <ta e="T866" id="Seg_12468" s="T865">v</ta>
            <ta e="T867" id="Seg_12469" s="T866">adv</ta>
            <ta e="T868" id="Seg_12470" s="T867">dempro</ta>
            <ta e="T869" id="Seg_12471" s="T868">v</ta>
            <ta e="T870" id="Seg_12472" s="T869">v</ta>
            <ta e="T871" id="Seg_12473" s="T870">ptcl</ta>
            <ta e="T872" id="Seg_12474" s="T871">aux</ta>
            <ta e="T873" id="Seg_12475" s="T872">v</ta>
            <ta e="T874" id="Seg_12476" s="T873">ptcl</ta>
            <ta e="T875" id="Seg_12477" s="T874">quant</ta>
            <ta e="T876" id="Seg_12478" s="T875">v</ta>
            <ta e="T877" id="Seg_12479" s="T876">dempro</ta>
            <ta e="T881" id="Seg_12480" s="T880">v</ta>
            <ta e="T882" id="Seg_12481" s="T881">conj</ta>
            <ta e="T883" id="Seg_12482" s="T882">ptcl</ta>
            <ta e="T884" id="Seg_12483" s="T883">n</ta>
            <ta e="T885" id="Seg_12484" s="T884">n</ta>
            <ta e="T886" id="Seg_12485" s="T885">conj</ta>
            <ta e="T887" id="Seg_12486" s="T886">n</ta>
            <ta e="T888" id="Seg_12487" s="T887">v</ta>
            <ta e="T889" id="Seg_12488" s="T888">dempro</ta>
            <ta e="T890" id="Seg_12489" s="T889">ptcl</ta>
            <ta e="T892" id="Seg_12490" s="T890">n</ta>
            <ta e="T895" id="Seg_12491" s="T894">n</ta>
            <ta e="T896" id="Seg_12492" s="T895">v</ta>
            <ta e="T897" id="Seg_12493" s="T896">dempro</ta>
            <ta e="T898" id="Seg_12494" s="T897">n</ta>
            <ta e="T899" id="Seg_12495" s="T898">pers</ta>
            <ta e="T900" id="Seg_12496" s="T899">n</ta>
            <ta e="T901" id="Seg_12497" s="T900">v</ta>
            <ta e="T902" id="Seg_12498" s="T901">n</ta>
            <ta e="T903" id="Seg_12499" s="T902">pers</ta>
            <ta e="T904" id="Seg_12500" s="T903">pers</ta>
            <ta e="T905" id="Seg_12501" s="T904">n</ta>
            <ta e="T906" id="Seg_12502" s="T905">v</ta>
            <ta e="T907" id="Seg_12503" s="T906">conj</ta>
            <ta e="T908" id="Seg_12504" s="T907">pers</ta>
            <ta e="T910" id="Seg_12505" s="T908">n</ta>
            <ta e="T911" id="Seg_12506" s="T910">v</ta>
            <ta e="T915" id="Seg_12507" s="T914">v</ta>
            <ta e="T916" id="Seg_12508" s="T915">ptcl</ta>
            <ta e="T919" id="Seg_12509" s="T918">adv</ta>
            <ta e="T920" id="Seg_12510" s="T919">n</ta>
            <ta e="T921" id="Seg_12511" s="T920">conj</ta>
            <ta e="T922" id="Seg_12512" s="T921">n</ta>
            <ta e="T923" id="Seg_12513" s="T922">v</ta>
            <ta e="T924" id="Seg_12514" s="T923">que</ta>
            <ta e="T925" id="Seg_12515" s="T924">adv</ta>
            <ta e="T926" id="Seg_12516" s="T925">v</ta>
            <ta e="T927" id="Seg_12517" s="T926">dempro</ta>
            <ta e="T928" id="Seg_12518" s="T927">ptcl</ta>
            <ta e="T1073" id="Seg_12519" s="T928">v</ta>
            <ta e="T1071" id="Seg_12520" s="T1073">v</ta>
            <ta e="T930" id="Seg_12521" s="T929">v</ta>
            <ta e="T931" id="Seg_12522" s="T930">n</ta>
            <ta e="T932" id="Seg_12523" s="T931">adv</ta>
            <ta e="T933" id="Seg_12524" s="T932">num</ta>
            <ta e="T934" id="Seg_12525" s="T933">n</ta>
            <ta e="T935" id="Seg_12526" s="T934">v</ta>
            <ta e="T936" id="Seg_12527" s="T935">dempro</ta>
            <ta e="T937" id="Seg_12528" s="T936">v</ta>
            <ta e="T1075" id="Seg_12529" s="T937">propr</ta>
            <ta e="T938" id="Seg_12530" s="T1075">n</ta>
            <ta e="T939" id="Seg_12531" s="T938">conj</ta>
            <ta e="T940" id="Seg_12532" s="T939">v</ta>
            <ta e="T941" id="Seg_12533" s="T940">conj</ta>
            <ta e="T942" id="Seg_12534" s="T941">adj</ta>
            <ta e="T943" id="Seg_12535" s="T942">n</ta>
            <ta e="T944" id="Seg_12536" s="T943">pers</ta>
            <ta e="T945" id="Seg_12537" s="T944">v</ta>
            <ta e="T946" id="Seg_12538" s="T945">n</ta>
            <ta e="T948" id="Seg_12539" s="T947">v</ta>
            <ta e="T949" id="Seg_12540" s="T948">n</ta>
            <ta e="T950" id="Seg_12541" s="T949">v</ta>
            <ta e="T951" id="Seg_12542" s="T950">n</ta>
            <ta e="T952" id="Seg_12543" s="T951">v</ta>
            <ta e="T953" id="Seg_12544" s="T952">n</ta>
            <ta e="T954" id="Seg_12545" s="T953">v</ta>
            <ta e="T955" id="Seg_12546" s="T954">v</ta>
            <ta e="T956" id="Seg_12547" s="T955">conj</ta>
            <ta e="T957" id="Seg_12548" s="T956">n</ta>
            <ta e="T958" id="Seg_12549" s="T957">n</ta>
            <ta e="T959" id="Seg_12550" s="T958">v</ta>
            <ta e="T960" id="Seg_12551" s="T959">conj</ta>
            <ta e="T961" id="Seg_12552" s="T960">dempro</ta>
            <ta e="T962" id="Seg_12553" s="T961">v</ta>
            <ta e="T963" id="Seg_12554" s="T962">dempro</ta>
            <ta e="T964" id="Seg_12555" s="T963">ptcl</ta>
            <ta e="T965" id="Seg_12556" s="T964">v</ta>
            <ta e="T966" id="Seg_12557" s="T965">conj</ta>
            <ta e="T967" id="Seg_12558" s="T966">n</ta>
            <ta e="T968" id="Seg_12559" s="T967">v</ta>
            <ta e="T969" id="Seg_12560" s="T968">dempro</ta>
            <ta e="T970" id="Seg_12561" s="T969">v</ta>
            <ta e="T971" id="Seg_12562" s="T970">que</ta>
            <ta e="T972" id="Seg_12563" s="T971">ptcl</ta>
            <ta e="T973" id="Seg_12564" s="T972">v</ta>
            <ta e="T975" id="Seg_12565" s="T974">adv</ta>
            <ta e="T976" id="Seg_12566" s="T975">n</ta>
            <ta e="T977" id="Seg_12567" s="T976">pers</ta>
            <ta e="T978" id="Seg_12568" s="T977">v</ta>
            <ta e="T979" id="Seg_12569" s="T978">n</ta>
            <ta e="T980" id="Seg_12570" s="T979">v</ta>
            <ta e="T981" id="Seg_12571" s="T980">adv</ta>
            <ta e="T982" id="Seg_12572" s="T981">adj</ta>
            <ta e="T983" id="Seg_12573" s="T982">n</ta>
            <ta e="T984" id="Seg_12574" s="T983">pers</ta>
            <ta e="T985" id="Seg_12575" s="T984">v</ta>
            <ta e="T986" id="Seg_12576" s="T985">ptcl</ta>
            <ta e="T987" id="Seg_12577" s="T986">v</ta>
            <ta e="T988" id="Seg_12578" s="T987">v</ta>
            <ta e="T989" id="Seg_12579" s="T988">n</ta>
            <ta e="T990" id="Seg_12580" s="T989">n</ta>
            <ta e="T991" id="Seg_12581" s="T990">v</ta>
            <ta e="T992" id="Seg_12582" s="T991">n</ta>
            <ta e="T993" id="Seg_12583" s="T992">v</ta>
            <ta e="T994" id="Seg_12584" s="T993">n</ta>
            <ta e="T995" id="Seg_12585" s="T994">v</ta>
            <ta e="T996" id="Seg_12586" s="T995">v</ta>
            <ta e="T997" id="Seg_12587" s="T996">adv</ta>
            <ta e="T998" id="Seg_12588" s="T997">n</ta>
            <ta e="T999" id="Seg_12589" s="T998">v</ta>
            <ta e="T1000" id="Seg_12590" s="T999">n</ta>
            <ta e="T1001" id="Seg_12591" s="T1000">v</ta>
            <ta e="T1002" id="Seg_12592" s="T1001">dempro</ta>
            <ta e="T1003" id="Seg_12593" s="T1002">ptcl</ta>
            <ta e="T1004" id="Seg_12594" s="T1003">v</ta>
            <ta e="T1005" id="Seg_12595" s="T1004">n</ta>
            <ta e="T1006" id="Seg_12596" s="T1005">v</ta>
            <ta e="T1007" id="Seg_12597" s="T1006">que</ta>
            <ta e="T1008" id="Seg_12598" s="T1007">v</ta>
            <ta e="T1009" id="Seg_12599" s="T1008">conj</ta>
            <ta e="T1010" id="Seg_12600" s="T1009">adv</ta>
            <ta e="T1011" id="Seg_12601" s="T1010">n</ta>
            <ta e="T1012" id="Seg_12602" s="T1011">v</ta>
            <ta e="T1013" id="Seg_12603" s="T1012">pers</ta>
            <ta e="T1014" id="Seg_12604" s="T1013">v</ta>
            <ta e="T1015" id="Seg_12605" s="T1014">n</ta>
            <ta e="T1016" id="Seg_12606" s="T1015">v</ta>
            <ta e="T1017" id="Seg_12607" s="T1016">ptcl</ta>
            <ta e="T1019" id="Seg_12608" s="T1018">adv</ta>
            <ta e="T1020" id="Seg_12609" s="T1019">adj</ta>
            <ta e="T1021" id="Seg_12610" s="T1020">n</ta>
            <ta e="T1022" id="Seg_12611" s="T1021">pers</ta>
            <ta e="T1023" id="Seg_12612" s="T1022">v</ta>
            <ta e="T1024" id="Seg_12613" s="T1023">ptcl</ta>
            <ta e="T1025" id="Seg_12614" s="T1024">v</ta>
            <ta e="T1026" id="Seg_12615" s="T1025">ptcl</ta>
            <ta e="T1027" id="Seg_12616" s="T1026">ptcl</ta>
            <ta e="T1028" id="Seg_12617" s="T1027">ptcl</ta>
            <ta e="T1029" id="Seg_12618" s="T1028">n</ta>
            <ta e="T1030" id="Seg_12619" s="T1029">v</ta>
            <ta e="T1031" id="Seg_12620" s="T1030">n</ta>
            <ta e="T1032" id="Seg_12621" s="T1031">v</ta>
            <ta e="T1033" id="Seg_12622" s="T1032">n</ta>
            <ta e="T1034" id="Seg_12623" s="T1033">v</ta>
            <ta e="T1035" id="Seg_12624" s="T1034">conj</ta>
            <ta e="T1036" id="Seg_12625" s="T1035">n</ta>
            <ta e="T1037" id="Seg_12626" s="T1036">v</ta>
            <ta e="T1038" id="Seg_12627" s="T1037">v</ta>
            <ta e="T1039" id="Seg_12628" s="T1038">v</ta>
            <ta e="T1040" id="Seg_12629" s="T1039">v</ta>
            <ta e="T1041" id="Seg_12630" s="T1040">n</ta>
            <ta e="T1042" id="Seg_12631" s="T1041">v</ta>
            <ta e="T1043" id="Seg_12632" s="T1042">dempro</ta>
            <ta e="T1044" id="Seg_12633" s="T1043">n</ta>
            <ta e="T1045" id="Seg_12634" s="T1044">ptcl</ta>
            <ta e="T1046" id="Seg_12635" s="T1045">dempro</ta>
            <ta e="T1048" id="Seg_12636" s="T1047">n</ta>
            <ta e="T1049" id="Seg_12637" s="T1048">n</ta>
            <ta e="T1051" id="Seg_12638" s="T1049">ptcl</ta>
            <ta e="T1052" id="Seg_12639" s="T1051">dempro</ta>
            <ta e="T1053" id="Seg_12640" s="T1052">n</ta>
            <ta e="T1054" id="Seg_12641" s="T1053">n</ta>
            <ta e="T1055" id="Seg_12642" s="T1054">v</ta>
            <ta e="T1056" id="Seg_12643" s="T1055">conj</ta>
            <ta e="T1057" id="Seg_12644" s="T1056">n</ta>
            <ta e="T1059" id="Seg_12645" s="T1058">v</ta>
            <ta e="T1060" id="Seg_12646" s="T1059">adv</ta>
            <ta e="T1061" id="Seg_12647" s="T1060">v</ta>
            <ta e="T1062" id="Seg_12648" s="T1061">v</ta>
            <ta e="T1063" id="Seg_12649" s="T1062">v</ta>
            <ta e="T1064" id="Seg_12650" s="T1063">n</ta>
            <ta e="T1065" id="Seg_12651" s="T1064">v</ta>
            <ta e="T1066" id="Seg_12652" s="T1065">conj</ta>
            <ta e="T1067" id="Seg_12653" s="T1066">n</ta>
            <ta e="T1068" id="Seg_12654" s="T1067">n</ta>
            <ta e="T1069" id="Seg_12655" s="T1068">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PKZ">
            <ta e="T1" id="Seg_12656" s="T0">pro.h:A</ta>
            <ta e="T2" id="Seg_12657" s="T1">n:Time</ta>
            <ta e="T4" id="Seg_12658" s="T3">np:G</ta>
            <ta e="T5" id="Seg_12659" s="T4">pro.h:B</ta>
            <ta e="T8" id="Seg_12660" s="T7">np.h:A</ta>
            <ta e="T9" id="Seg_12661" s="T8">0.1.h:A</ta>
            <ta e="T11" id="Seg_12662" s="T10">adv:Time</ta>
            <ta e="T12" id="Seg_12663" s="T11">0.1.h:A</ta>
            <ta e="T13" id="Seg_12664" s="T12">0.1.h:A</ta>
            <ta e="T17" id="Seg_12665" s="T16">np.h:Th</ta>
            <ta e="T18" id="Seg_12666" s="T17">0.1.h:A</ta>
            <ta e="T19" id="Seg_12667" s="T18">0.1.h:A</ta>
            <ta e="T20" id="Seg_12668" s="T19">pro.h:E</ta>
            <ta e="T21" id="Seg_12669" s="T20">pro:Th</ta>
            <ta e="T28" id="Seg_12670" s="T27">pro.h:E</ta>
            <ta e="T30" id="Seg_12671" s="T29">pro.h:E</ta>
            <ta e="T34" id="Seg_12672" s="T33">np:G</ta>
            <ta e="T38" id="Seg_12673" s="T37">np.h:A</ta>
            <ta e="T41" id="Seg_12674" s="T40">np:Th</ta>
            <ta e="T45" id="Seg_12675" s="T44">0.3.h:A</ta>
            <ta e="T46" id="Seg_12676" s="T45">np:G</ta>
            <ta e="T48" id="Seg_12677" s="T47">np:G</ta>
            <ta e="T49" id="Seg_12678" s="T48">pro.h:Th</ta>
            <ta e="T53" id="Seg_12679" s="T52">np.h:Th</ta>
            <ta e="T65" id="Seg_12680" s="T64">pro.h:A</ta>
            <ta e="T69" id="Seg_12681" s="T68">np:Th</ta>
            <ta e="T74" id="Seg_12682" s="T73">0.3.h:A</ta>
            <ta e="T75" id="Seg_12683" s="T74">adv:L</ta>
            <ta e="T76" id="Seg_12684" s="T75">np:Th</ta>
            <ta e="T78" id="Seg_12685" s="T77">np:Th</ta>
            <ta e="T79" id="Seg_12686" s="T78">0.3.h:A</ta>
            <ta e="T82" id="Seg_12687" s="T81">pro.h:A</ta>
            <ta e="T83" id="Seg_12688" s="T82">pro.h:R</ta>
            <ta e="T86" id="Seg_12689" s="T85">np.h:Th</ta>
            <ta e="T90" id="Seg_12690" s="T89">np.h:Th</ta>
            <ta e="T92" id="Seg_12691" s="T90">adv:Time</ta>
            <ta e="T94" id="Seg_12692" s="T93">adv:Time</ta>
            <ta e="T95" id="Seg_12693" s="T94">pro.h:Th</ta>
            <ta e="T97" id="Seg_12694" s="T96">pro.h:A</ta>
            <ta e="T98" id="Seg_12695" s="T97">np:G</ta>
            <ta e="T105" id="Seg_12696" s="T104">0.1.h:A</ta>
            <ta e="T108" id="Seg_12697" s="T107">0.3.h:A</ta>
            <ta e="T109" id="Seg_12698" s="T108">pro.h:Poss</ta>
            <ta e="T111" id="Seg_12699" s="T110">np:Ins</ta>
            <ta e="T113" id="Seg_12700" s="T112">0.3.h:A</ta>
            <ta e="T114" id="Seg_12701" s="T113">pro.h:Poss</ta>
            <ta e="T115" id="Seg_12702" s="T114">np.h:P</ta>
            <ta e="T119" id="Seg_12703" s="T118">np:Th</ta>
            <ta e="T123" id="Seg_12704" s="T122">0.1.h:A</ta>
            <ta e="T126" id="Seg_12705" s="T125">adv:Time</ta>
            <ta e="T130" id="Seg_12706" s="T129">pro.h:Th</ta>
            <ta e="T134" id="Seg_12707" s="T133">np:Th</ta>
            <ta e="T138" id="Seg_12708" s="T137">0.3.h:A</ta>
            <ta e="T141" id="Seg_12709" s="T140">0.2.h:A</ta>
            <ta e="T143" id="Seg_12710" s="T142">pro.h:E</ta>
            <ta e="T148" id="Seg_12711" s="T147">adv:Time</ta>
            <ta e="T149" id="Seg_12712" s="T148">pro.h:A</ta>
            <ta e="T151" id="Seg_12713" s="T150">np:L</ta>
            <ta e="T156" id="Seg_12714" s="T155">np:B</ta>
            <ta e="T158" id="Seg_12715" s="T157">np:G</ta>
            <ta e="T164" id="Seg_12716" s="T163">pro.h:A</ta>
            <ta e="T168" id="Seg_12717" s="T167">np:Ins</ta>
            <ta e="T170" id="Seg_12718" s="T169">np:A</ta>
            <ta e="T172" id="Seg_12719" s="T171">pro.h:Th</ta>
            <ta e="T173" id="Seg_12720" s="T172">adv:L</ta>
            <ta e="T180" id="Seg_12721" s="T179">0.3.h:A</ta>
            <ta e="T183" id="Seg_12722" s="T182">pro.h:A</ta>
            <ta e="T184" id="Seg_12723" s="T183">np:Ins</ta>
            <ta e="T187" id="Seg_12724" s="T186">np:Th</ta>
            <ta e="T190" id="Seg_12725" s="T189">np:Ins</ta>
            <ta e="T196" id="Seg_12726" s="T195">0.3:Th</ta>
            <ta e="T201" id="Seg_12727" s="T200">pro.h:A</ta>
            <ta e="T209" id="Seg_12728" s="T208">np:L</ta>
            <ta e="T210" id="Seg_12729" s="T209">np.h:Th</ta>
            <ta e="T213" id="Seg_12730" s="T212">np:Th</ta>
            <ta e="T214" id="Seg_12731" s="T213">np:Th</ta>
            <ta e="T215" id="Seg_12732" s="T214">np:Th</ta>
            <ta e="T220" id="Seg_12733" s="T219">np:Th</ta>
            <ta e="T223" id="Seg_12734" s="T222">np:Th</ta>
            <ta e="T227" id="Seg_12735" s="T226">pro.h:Th</ta>
            <ta e="T231" id="Seg_12736" s="T230">0.3.h:A</ta>
            <ta e="T233" id="Seg_12737" s="T232">adv:L</ta>
            <ta e="T240" id="Seg_12738" s="T239">pro.h:Poss</ta>
            <ta e="T241" id="Seg_12739" s="T240">np:Th</ta>
            <ta e="T243" id="Seg_12740" s="T242">0.3.h:E</ta>
            <ta e="T248" id="Seg_12741" s="T247">np:Th</ta>
            <ta e="T250" id="Seg_12742" s="T249">np.h:E</ta>
            <ta e="T254" id="Seg_12743" s="T253">pro.h:E</ta>
            <ta e="T258" id="Seg_12744" s="T257">np:Th</ta>
            <ta e="T260" id="Seg_12745" s="T259">pro.h:A</ta>
            <ta e="T261" id="Seg_12746" s="T260">pro.h:R</ta>
            <ta e="T263" id="Seg_12747" s="T262">pro.h:A</ta>
            <ta e="T264" id="Seg_12748" s="T263">pro.h:R</ta>
            <ta e="T266" id="Seg_12749" s="T265">adv:Time</ta>
            <ta e="T267" id="Seg_12750" s="T266">0.1.h:A</ta>
            <ta e="T269" id="Seg_12751" s="T268">pro.h:A</ta>
            <ta e="T270" id="Seg_12752" s="T269">pro.h:Th</ta>
            <ta e="T272" id="Seg_12753" s="T271">0.1.h:A</ta>
            <ta e="T275" id="Seg_12754" s="T274">0.1.h:A</ta>
            <ta e="T279" id="Seg_12755" s="T278">np.h:E</ta>
            <ta e="T281" id="Seg_12756" s="T280">pro.h:Th</ta>
            <ta e="T282" id="Seg_12757" s="T281">0.3.h:A</ta>
            <ta e="T284" id="Seg_12758" s="T283">pro.h:A</ta>
            <ta e="T286" id="Seg_12759" s="T285">np:Th</ta>
            <ta e="T288" id="Seg_12760" s="T287">np:E</ta>
            <ta e="T292" id="Seg_12761" s="T291">0.3.h:A</ta>
            <ta e="T293" id="Seg_12762" s="T292">adv:Time</ta>
            <ta e="T294" id="Seg_12763" s="T293">0.3.h:A</ta>
            <ta e="T295" id="Seg_12764" s="T294">0.3.h:A</ta>
            <ta e="T296" id="Seg_12765" s="T295">np.h:A</ta>
            <ta e="T298" id="Seg_12766" s="T297">0.2.h:A</ta>
            <ta e="T299" id="Seg_12767" s="T298">pro.h:Th</ta>
            <ta e="T305" id="Seg_12768" s="T304">np:G</ta>
            <ta e="T306" id="Seg_12769" s="T305">0.2.h:A</ta>
            <ta e="T307" id="Seg_12770" s="T306">pro.h:Th</ta>
            <ta e="T308" id="Seg_12771" s="T307">pro.h:A</ta>
            <ta e="T309" id="Seg_12772" s="T308">pro.h:Th</ta>
            <ta e="T312" id="Seg_12773" s="T311">0.3.h:A</ta>
            <ta e="T313" id="Seg_12774" s="T312">0.3.h:A</ta>
            <ta e="T314" id="Seg_12775" s="T313">0.3.h:A</ta>
            <ta e="T316" id="Seg_12776" s="T314">adv:Time</ta>
            <ta e="T317" id="Seg_12777" s="T316">adv:Time</ta>
            <ta e="T319" id="Seg_12778" s="T318">np.h:A</ta>
            <ta e="T320" id="Seg_12779" s="T319">np:So</ta>
            <ta e="T322" id="Seg_12780" s="T321">0.2.h:A</ta>
            <ta e="T323" id="Seg_12781" s="T322">pro.h:Th</ta>
            <ta e="T326" id="Seg_12782" s="T325">pro.h:Th</ta>
            <ta e="T327" id="Seg_12783" s="T326">np:G</ta>
            <ta e="T328" id="Seg_12784" s="T327">0.2.h:A</ta>
            <ta e="T329" id="Seg_12785" s="T328">pro.h:A</ta>
            <ta e="T331" id="Seg_12786" s="T330">0.3.h:A</ta>
            <ta e="T333" id="Seg_12787" s="T332">0.3.h:A</ta>
            <ta e="T334" id="Seg_12788" s="T333">0.3.h:A</ta>
            <ta e="T338" id="Seg_12789" s="T337">np.h:A</ta>
            <ta e="T340" id="Seg_12790" s="T339">0.2.h:A</ta>
            <ta e="T341" id="Seg_12791" s="T340">pro.h:Th</ta>
            <ta e="T342" id="Seg_12792" s="T341">pro.h:A</ta>
            <ta e="T343" id="Seg_12793" s="T342">pro.h:Th</ta>
            <ta e="T346" id="Seg_12794" s="T345">np:G</ta>
            <ta e="T347" id="Seg_12795" s="T346">0.3.h:A</ta>
            <ta e="T348" id="Seg_12796" s="T347">adv:Time</ta>
            <ta e="T350" id="Seg_12797" s="T349">0.3.h:A</ta>
            <ta e="T351" id="Seg_12798" s="T350">np.h:A</ta>
            <ta e="T353" id="Seg_12799" s="T352">0.2.h:A</ta>
            <ta e="T354" id="Seg_12800" s="T353">pro.h:Th</ta>
            <ta e="T355" id="Seg_12801" s="T354">adv:Time</ta>
            <ta e="T356" id="Seg_12802" s="T355">pro.h:A</ta>
            <ta e="T357" id="Seg_12803" s="T356">np.h:Th</ta>
            <ta e="T359" id="Seg_12804" s="T358">adv:L</ta>
            <ta e="T360" id="Seg_12805" s="T359">0.3.h:A</ta>
            <ta e="T361" id="Seg_12806" s="T360">adv:L</ta>
            <ta e="T363" id="Seg_12807" s="T362">np:Th</ta>
            <ta e="T366" id="Seg_12808" s="T365">np:Th</ta>
            <ta e="T369" id="Seg_12809" s="T368">pro.h:A</ta>
            <ta e="T370" id="Seg_12810" s="T369">np:Th</ta>
            <ta e="T372" id="Seg_12811" s="T371">np:P</ta>
            <ta e="T373" id="Seg_12812" s="T372">0.3.h:A</ta>
            <ta e="T374" id="Seg_12813" s="T373">0.3.h:Th</ta>
            <ta e="T375" id="Seg_12814" s="T374">pro.h:Th</ta>
            <ta e="T377" id="Seg_12815" s="T376">np.h:A</ta>
            <ta e="T378" id="Seg_12816" s="T377">pro.h:Th</ta>
            <ta e="T379" id="Seg_12817" s="T378">adv:L</ta>
            <ta e="T381" id="Seg_12818" s="T380">pro.h:A</ta>
            <ta e="T383" id="Seg_12819" s="T382">np:P</ta>
            <ta e="T385" id="Seg_12820" s="T384">np:A</ta>
            <ta e="T387" id="Seg_12821" s="T386">adv:L</ta>
            <ta e="T389" id="Seg_12822" s="T388">0.3.h:Th</ta>
            <ta e="T390" id="Seg_12823" s="T389">adv:Time</ta>
            <ta e="T393" id="Seg_12824" s="T392">0.3.h:A</ta>
            <ta e="T394" id="Seg_12825" s="T393">0.2.h:A</ta>
            <ta e="T395" id="Seg_12826" s="T394">np.h:Th</ta>
            <ta e="T396" id="Seg_12827" s="T395">0.3.h:A</ta>
            <ta e="T397" id="Seg_12828" s="T396">pro:Th</ta>
            <ta e="T398" id="Seg_12829" s="T397">pro.h:B</ta>
            <ta e="T400" id="Seg_12830" s="T399">pro.h:A</ta>
            <ta e="T403" id="Seg_12831" s="T402">0.3.h:A</ta>
            <ta e="T404" id="Seg_12832" s="T403">pro:Th</ta>
            <ta e="T405" id="Seg_12833" s="T404">pro.h:B</ta>
            <ta e="T407" id="Seg_12834" s="T406">pro.h:Poss</ta>
            <ta e="T408" id="Seg_12835" s="T407">np.h:Poss</ta>
            <ta e="T409" id="Seg_12836" s="T408">np.h:Th</ta>
            <ta e="T410" id="Seg_12837" s="T409">0.1.h:A</ta>
            <ta e="T413" id="Seg_12838" s="T412">np.h:A</ta>
            <ta e="T415" id="Seg_12839" s="T414">0.3.h:A</ta>
            <ta e="T416" id="Seg_12840" s="T415">pro.h:A</ta>
            <ta e="T420" id="Seg_12841" s="T419">np.h:Poss</ta>
            <ta e="T421" id="Seg_12842" s="T420">np.h:Th</ta>
            <ta e="T422" id="Seg_12843" s="T421">0.1.h:A</ta>
            <ta e="T431" id="Seg_12844" s="T430">0.1.h:A</ta>
            <ta e="T432" id="Seg_12845" s="T431">pro.h:A</ta>
            <ta e="T435" id="Seg_12846" s="T434">0.3.h:A</ta>
            <ta e="T437" id="Seg_12847" s="T436">np.h:A</ta>
            <ta e="T440" id="Seg_12848" s="T439">np.h:Th</ta>
            <ta e="T442" id="Seg_12849" s="T441">0.3.h:A</ta>
            <ta e="T443" id="Seg_12850" s="T442">np:Th</ta>
            <ta e="T446" id="Seg_12851" s="T445">np:Th</ta>
            <ta e="T447" id="Seg_12852" s="T446">0.3.h:A</ta>
            <ta e="T449" id="Seg_12853" s="T448">0.2.h:A</ta>
            <ta e="T451" id="Seg_12854" s="T450">np:Th</ta>
            <ta e="T452" id="Seg_12855" s="T451">pro.h:A</ta>
            <ta e="T454" id="Seg_12856" s="T453">adv:L</ta>
            <ta e="T455" id="Seg_12857" s="T454">np:P</ta>
            <ta e="T457" id="Seg_12858" s="T456">adv:Time</ta>
            <ta e="T458" id="Seg_12859" s="T1072">0.3.h:A</ta>
            <ta e="T459" id="Seg_12860" s="T458">adv:Time</ta>
            <ta e="T460" id="Seg_12861" s="T459">pro.h:A</ta>
            <ta e="T461" id="Seg_12862" s="T460">np.h:Th</ta>
            <ta e="T464" id="Seg_12863" s="T463">np:So</ta>
            <ta e="T466" id="Seg_12864" s="T465">np.h:A</ta>
            <ta e="T467" id="Seg_12865" s="T466">np:Th</ta>
            <ta e="T470" id="Seg_12866" s="T469">adv:Time</ta>
            <ta e="T471" id="Seg_12867" s="T470">0.3.h:A</ta>
            <ta e="T472" id="Seg_12868" s="T471">0.2.h:A</ta>
            <ta e="T473" id="Seg_12869" s="T472">np:Th</ta>
            <ta e="T475" id="Seg_12870" s="T474">np:Th</ta>
            <ta e="T476" id="Seg_12871" s="T475">np:Ins</ta>
            <ta e="T478" id="Seg_12872" s="T477">np:P</ta>
            <ta e="T480" id="Seg_12873" s="T479">0.3.h:A</ta>
            <ta e="T482" id="Seg_12874" s="T481">pro.h:A</ta>
            <ta e="T484" id="Seg_12875" s="T483">np:So</ta>
            <ta e="T487" id="Seg_12876" s="T486">np.h:Th</ta>
            <ta e="T489" id="Seg_12877" s="T488">pro.h:A</ta>
            <ta e="T491" id="Seg_12878" s="T490">pro.h:A</ta>
            <ta e="T494" id="Seg_12879" s="T493">adv:Time</ta>
            <ta e="T495" id="Seg_12880" s="T494">0.3.h:A</ta>
            <ta e="T497" id="Seg_12881" s="T495">0.2.h:A</ta>
            <ta e="T499" id="Seg_12882" s="T498">adv:Time</ta>
            <ta e="T500" id="Seg_12883" s="T499">np.h:A</ta>
            <ta e="T503" id="Seg_12884" s="T502">0.2.h:A</ta>
            <ta e="T504" id="Seg_12885" s="T503">np:Th</ta>
            <ta e="T506" id="Seg_12886" s="T505">np:A</ta>
            <ta e="T507" id="Seg_12887" s="T506">pro:P</ta>
            <ta e="T509" id="Seg_12888" s="T508">pro.h:A</ta>
            <ta e="T511" id="Seg_12889" s="T510">np:Th</ta>
            <ta e="T513" id="Seg_12890" s="T512">pro.h:A</ta>
            <ta e="T514" id="Seg_12891" s="T513">np.h:Th</ta>
            <ta e="T516" id="Seg_12892" s="T515">np:A</ta>
            <ta e="T517" id="Seg_12893" s="T516">np:L</ta>
            <ta e="T519" id="Seg_12894" s="T518">adv:Time</ta>
            <ta e="T521" id="Seg_12895" s="T520">np.h:A</ta>
            <ta e="T523" id="Seg_12896" s="T522">np.h:Th</ta>
            <ta e="T525" id="Seg_12897" s="T524">0.1.h:A</ta>
            <ta e="T526" id="Seg_12898" s="T525">pro.h:Th</ta>
            <ta e="T527" id="Seg_12899" s="T526">0.1.h:A</ta>
            <ta e="T528" id="Seg_12900" s="T527">np.h:A</ta>
            <ta e="T530" id="Seg_12901" s="T529">pro.h:A</ta>
            <ta e="T532" id="Seg_12902" s="T531">np:So</ta>
            <ta e="T533" id="Seg_12903" s="T532">np:Th</ta>
            <ta e="T538" id="Seg_12904" s="T537">np.h:A</ta>
            <ta e="T541" id="Seg_12905" s="T540">np:So</ta>
            <ta e="T543" id="Seg_12906" s="T542">adv:Time</ta>
            <ta e="T545" id="Seg_12907" s="T544">np.h:A</ta>
            <ta e="T547" id="Seg_12908" s="T546">np:Th</ta>
            <ta e="T550" id="Seg_12909" s="T549">0.3.h:A</ta>
            <ta e="T551" id="Seg_12910" s="T550">pro.h:Poss</ta>
            <ta e="T553" id="Seg_12911" s="T552">np:Th</ta>
            <ta e="T554" id="Seg_12912" s="T553">np:Th</ta>
            <ta e="T555" id="Seg_12913" s="T554">np:Th</ta>
            <ta e="T556" id="Seg_12914" s="T555">0.1.h:A</ta>
            <ta e="T557" id="Seg_12915" s="T556">np:Th</ta>
            <ta e="T559" id="Seg_12916" s="T558">0.3.h:A</ta>
            <ta e="T560" id="Seg_12917" s="T559">pro:Th</ta>
            <ta e="T561" id="Seg_12918" s="T560">pro.h:B</ta>
            <ta e="T565" id="Seg_12919" s="T564">np.h:Th</ta>
            <ta e="T568" id="Seg_12920" s="T567">adv:Time</ta>
            <ta e="T570" id="Seg_12921" s="T569">np:G</ta>
            <ta e="T571" id="Seg_12922" s="T570">np:Th</ta>
            <ta e="T572" id="Seg_12923" s="T571">0.3.h:A</ta>
            <ta e="T576" id="Seg_12924" s="T575">adv:Time</ta>
            <ta e="T577" id="Seg_12925" s="T576">0.3.h:A</ta>
            <ta e="T578" id="Seg_12926" s="T577">np.h:Th</ta>
            <ta e="T579" id="Seg_12927" s="T578">pro.h:A</ta>
            <ta e="T582" id="Seg_12928" s="T581">np:G</ta>
            <ta e="T584" id="Seg_12929" s="T583">0.3.h:A</ta>
            <ta e="T585" id="Seg_12930" s="T584">np:G</ta>
            <ta e="T587" id="Seg_12931" s="T586">np.h:E</ta>
            <ta e="T590" id="Seg_12932" s="T589">pro.h:Poss</ta>
            <ta e="T591" id="Seg_12933" s="T590">np.h:Th</ta>
            <ta e="T593" id="Seg_12934" s="T592">pro.h:A</ta>
            <ta e="T595" id="Seg_12935" s="T594">0.3.h:A</ta>
            <ta e="T596" id="Seg_12936" s="T595">np:G</ta>
            <ta e="T598" id="Seg_12937" s="T597">np.h:A</ta>
            <ta e="T599" id="Seg_12938" s="T598">np:G</ta>
            <ta e="T602" id="Seg_12939" s="T601">n:Time</ta>
            <ta e="T605" id="Seg_12940" s="T604">0.3.h:A</ta>
            <ta e="T606" id="Seg_12941" s="T605">adv:Time</ta>
            <ta e="T607" id="Seg_12942" s="T606">0.3.h:A</ta>
            <ta e="T608" id="Seg_12943" s="T607">np:L</ta>
            <ta e="T611" id="Seg_12944" s="T610">np.h:Th</ta>
            <ta e="T613" id="Seg_12945" s="T612">pro.h:A</ta>
            <ta e="T614" id="Seg_12946" s="T613">np:Th</ta>
            <ta e="T620" id="Seg_12947" s="T619">np:G</ta>
            <ta e="T621" id="Seg_12948" s="T620">0.3.h:A</ta>
            <ta e="T624" id="Seg_12949" s="T623">np:Th</ta>
            <ta e="T630" id="Seg_12950" s="T629">adv:Time</ta>
            <ta e="T631" id="Seg_12951" s="T630">pro.h:A</ta>
            <ta e="T637" id="Seg_12952" s="T636">np.h:A</ta>
            <ta e="T639" id="Seg_12953" s="T638">np.h:P</ta>
            <ta e="T640" id="Seg_12954" s="T639">0.3.h:A</ta>
            <ta e="T641" id="Seg_12955" s="T640">adv:Time</ta>
            <ta e="T645" id="Seg_12956" s="T644">np.h:R</ta>
            <ta e="T646" id="Seg_12957" s="T645">0.3.h:A</ta>
            <ta e="T648" id="Seg_12958" s="T647">pro.h:Th</ta>
            <ta e="T650" id="Seg_12959" s="T649">0.2.h:A</ta>
            <ta e="T651" id="Seg_12960" s="T650">pro.h:A</ta>
            <ta e="T655" id="Seg_12961" s="T654">pro.h:A</ta>
            <ta e="T657" id="Seg_12962" s="T656">np:Ins</ta>
            <ta e="T658" id="Seg_12963" s="T657">np:P</ta>
            <ta e="T660" id="Seg_12964" s="T659">adv:Time</ta>
            <ta e="T661" id="Seg_12965" s="T660">np.h:P</ta>
            <ta e="T662" id="Seg_12966" s="T661">0.3.h:A</ta>
            <ta e="T663" id="Seg_12967" s="T662">adv:Time</ta>
            <ta e="T664" id="Seg_12968" s="T663">pro.h:A</ta>
            <ta e="T666" id="Seg_12969" s="T665">pro:G</ta>
            <ta e="T667" id="Seg_12970" s="T666">np:G</ta>
            <ta e="T670" id="Seg_12971" s="T669">pro.h:Poss</ta>
            <ta e="T672" id="Seg_12972" s="T671">np.h:Th</ta>
            <ta e="T674" id="Seg_12973" s="T673">pro.h:A</ta>
            <ta e="T676" id="Seg_12974" s="T675">np.h:P</ta>
            <ta e="T678" id="Seg_12975" s="T677">np.h:P</ta>
            <ta e="T681" id="Seg_12976" s="T680">np.h:E</ta>
            <ta e="T682" id="Seg_12977" s="T681">np.h:E</ta>
            <ta e="T683" id="Seg_12978" s="T682">pro.h:Poss</ta>
            <ta e="T684" id="Seg_12979" s="T683">pro:Th</ta>
            <ta e="T688" id="Seg_12980" s="T687">np:Th</ta>
            <ta e="T689" id="Seg_12981" s="T688">pro.h:A</ta>
            <ta e="T693" id="Seg_12982" s="T692">adv:Time</ta>
            <ta e="T694" id="Seg_12983" s="T693">np:P</ta>
            <ta e="T695" id="Seg_12984" s="T694">0.3.h:A</ta>
            <ta e="T696" id="Seg_12985" s="T695">np.h:Th</ta>
            <ta e="T697" id="Seg_12986" s="T696">0.3.h:A</ta>
            <ta e="T698" id="Seg_12987" s="T697">0.2.h:A</ta>
            <ta e="T700" id="Seg_12988" s="T699">pro.h:B</ta>
            <ta e="T702" id="Seg_12989" s="T701">np:Th</ta>
            <ta e="T705" id="Seg_12990" s="T704">np.h:A</ta>
            <ta e="T707" id="Seg_12991" s="T706">0.3.h:A</ta>
            <ta e="T709" id="Seg_12992" s="T708">np:Th</ta>
            <ta e="T710" id="Seg_12993" s="T709">0.3.h:A</ta>
            <ta e="T711" id="Seg_12994" s="T710">np:G</ta>
            <ta e="T712" id="Seg_12995" s="T711">0.3.h:A</ta>
            <ta e="T713" id="Seg_12996" s="T712">adv:L</ta>
            <ta e="T714" id="Seg_12997" s="T713">np:Th</ta>
            <ta e="T716" id="Seg_12998" s="T715">np:Th</ta>
            <ta e="T718" id="Seg_12999" s="T717">pro.h:A</ta>
            <ta e="T721" id="Seg_13000" s="T720">0.3.h:A</ta>
            <ta e="T722" id="Seg_13001" s="T721">0.3.h:E</ta>
            <ta e="T724" id="Seg_13002" s="T723">pro.h:A</ta>
            <ta e="T727" id="Seg_13003" s="T726">np:Th</ta>
            <ta e="T729" id="Seg_13004" s="T728">0.3.h:A</ta>
            <ta e="T730" id="Seg_13005" s="T729">np:Th</ta>
            <ta e="T731" id="Seg_13006" s="T730">np:Th</ta>
            <ta e="T732" id="Seg_13007" s="T731">adv:Time</ta>
            <ta e="T733" id="Seg_13008" s="T732">0.3.h:A</ta>
            <ta e="T734" id="Seg_13009" s="T733">np:G</ta>
            <ta e="T735" id="Seg_13010" s="T734">0.3.h:A</ta>
            <ta e="T737" id="Seg_13011" s="T736">0.2.h:A</ta>
            <ta e="T739" id="Seg_13012" s="T738">np:Th</ta>
            <ta e="T741" id="Seg_13013" s="T740">pro.h:A</ta>
            <ta e="T744" id="Seg_13014" s="T743">pro.h:A</ta>
            <ta e="T746" id="Seg_13015" s="T745">np:Th</ta>
            <ta e="T748" id="Seg_13016" s="T747">np:Th</ta>
            <ta e="T750" id="Seg_13017" s="T749">np:Th</ta>
            <ta e="T752" id="Seg_13018" s="T751">pro.h:Th</ta>
            <ta e="T753" id="Seg_13019" s="T752">adv:L</ta>
            <ta e="T757" id="Seg_13020" s="T756">np:Th</ta>
            <ta e="T758" id="Seg_13021" s="T757">0.3.h:P</ta>
            <ta e="T761" id="Seg_13022" s="T760">np:Th</ta>
            <ta e="T762" id="Seg_13023" s="T761">0.3.h:P</ta>
            <ta e="T763" id="Seg_13024" s="T762">adv:Time</ta>
            <ta e="T765" id="Seg_13025" s="T764">np.h:A</ta>
            <ta e="T767" id="Seg_13026" s="T766">0.3.h:A</ta>
            <ta e="T768" id="Seg_13027" s="T767">pro.h:A</ta>
            <ta e="T769" id="Seg_13028" s="T768">pro.h:Th</ta>
            <ta e="T772" id="Seg_13029" s="T771">0.3.h:A</ta>
            <ta e="T773" id="Seg_13030" s="T772">pro.h:A</ta>
            <ta e="T776" id="Seg_13031" s="T775">pro.h:Poss</ta>
            <ta e="T777" id="Seg_13032" s="T776">np:Th</ta>
            <ta e="T778" id="Seg_13033" s="T777">np:Th</ta>
            <ta e="T781" id="Seg_13034" s="T780">pro.h:A</ta>
            <ta e="T785" id="Seg_13035" s="T784">0.2.h:A</ta>
            <ta e="T786" id="Seg_13036" s="T785">pro.h:A</ta>
            <ta e="T787" id="Seg_13037" s="T786">np:G</ta>
            <ta e="T789" id="Seg_13038" s="T788">0.1.h:A</ta>
            <ta e="T790" id="Seg_13039" s="T789">adv:Time</ta>
            <ta e="T791" id="Seg_13040" s="T790">0.3.h:A</ta>
            <ta e="T792" id="Seg_13041" s="T791">pro.h:A</ta>
            <ta e="T794" id="Seg_13042" s="T793">pro.h:A</ta>
            <ta e="T796" id="Seg_13043" s="T795">np:Ins</ta>
            <ta e="T797" id="Seg_13044" s="T796">np:P</ta>
            <ta e="T800" id="Seg_13045" s="T799">adv:Time</ta>
            <ta e="T802" id="Seg_13046" s="T801">np:Th</ta>
            <ta e="T803" id="Seg_13047" s="T802">0.3.h:A</ta>
            <ta e="T804" id="Seg_13048" s="T803">np:Th</ta>
            <ta e="T805" id="Seg_13049" s="T804">0.3.h:A</ta>
            <ta e="T806" id="Seg_13050" s="T805">np:Th</ta>
            <ta e="T807" id="Seg_13051" s="T806">0.3.h:A</ta>
            <ta e="T808" id="Seg_13052" s="T807">np:G</ta>
            <ta e="T809" id="Seg_13053" s="T808">0.3.h:A</ta>
            <ta e="T810" id="Seg_13054" s="T809">adv:Time</ta>
            <ta e="T811" id="Seg_13055" s="T810">0.3.h:A</ta>
            <ta e="T813" id="Seg_13056" s="T812">np:G</ta>
            <ta e="T814" id="Seg_13057" s="T813">np:G</ta>
            <ta e="T817" id="Seg_13058" s="T816">0.3.h:A</ta>
            <ta e="T820" id="Seg_13059" s="T819">np:Ins</ta>
            <ta e="T828" id="Seg_13060" s="T827">np:Ins</ta>
            <ta e="T832" id="Seg_13061" s="T831">np:P</ta>
            <ta e="T833" id="Seg_13062" s="T832">0.1.h:A</ta>
            <ta e="T835" id="Seg_13063" s="T834">np:P</ta>
            <ta e="T836" id="Seg_13064" s="T835">0.1.h:A</ta>
            <ta e="T838" id="Seg_13065" s="T837">pro.h:A</ta>
            <ta e="T840" id="Seg_13066" s="T839">pro.h:R</ta>
            <ta e="T841" id="Seg_13067" s="T840">pro.h:A</ta>
            <ta e="T844" id="Seg_13068" s="T843">np:A</ta>
            <ta e="T845" id="Seg_13069" s="T844">pro.h:R</ta>
            <ta e="T848" id="Seg_13070" s="T847">np:A</ta>
            <ta e="T849" id="Seg_13071" s="T848">pro.h:R</ta>
            <ta e="T851" id="Seg_13072" s="T850">np:Th</ta>
            <ta e="T852" id="Seg_13073" s="T851">pro.h:R</ta>
            <ta e="T854" id="Seg_13074" s="T853">np:A</ta>
            <ta e="T855" id="Seg_13075" s="T854">np:Th</ta>
            <ta e="T856" id="Seg_13076" s="T855">pro.h:A</ta>
            <ta e="T858" id="Seg_13077" s="T857">adv:Time</ta>
            <ta e="T859" id="Seg_13078" s="T858">pro.h:A</ta>
            <ta e="T861" id="Seg_13079" s="T860">0.3.h:A</ta>
            <ta e="T862" id="Seg_13080" s="T861">adv:L</ta>
            <ta e="T863" id="Seg_13081" s="T862">np:Th</ta>
            <ta e="T864" id="Seg_13082" s="T863">np:Th</ta>
            <ta e="T865" id="Seg_13083" s="T864">np:Th</ta>
            <ta e="T867" id="Seg_13084" s="T866">adv:L</ta>
            <ta e="T868" id="Seg_13085" s="T867">pro.h:A</ta>
            <ta e="T872" id="Seg_13086" s="T871">0.2.h:A</ta>
            <ta e="T875" id="Seg_13087" s="T874">np:A</ta>
            <ta e="T877" id="Seg_13088" s="T876">pro.h:A</ta>
            <ta e="T884" id="Seg_13089" s="T883">np:Th</ta>
            <ta e="T885" id="Seg_13090" s="T884">np:Th</ta>
            <ta e="T887" id="Seg_13091" s="T886">np:Th</ta>
            <ta e="T889" id="Seg_13092" s="T888">pro.h:A</ta>
            <ta e="T895" id="Seg_13093" s="T894">np:Ins</ta>
            <ta e="T897" id="Seg_13094" s="T896">pro:G</ta>
            <ta e="T899" id="Seg_13095" s="T898">pro:P</ta>
            <ta e="T900" id="Seg_13096" s="T899">np:Th</ta>
            <ta e="T904" id="Seg_13097" s="T903">pro:P</ta>
            <ta e="T905" id="Seg_13098" s="T904">np:Th</ta>
            <ta e="T911" id="Seg_13099" s="T910">0.1.h:E</ta>
            <ta e="T915" id="Seg_13100" s="T914">0.3.h:A</ta>
            <ta e="T919" id="Seg_13101" s="T918">adv:Time</ta>
            <ta e="T920" id="Seg_13102" s="T919">np.h:A</ta>
            <ta e="T922" id="Seg_13103" s="T921">np.h:A</ta>
            <ta e="T924" id="Seg_13104" s="T923">pro:Th</ta>
            <ta e="T927" id="Seg_13105" s="T926">pro.h:Poss</ta>
            <ta e="T1071" id="Seg_13106" s="T1073">0.3.h:A</ta>
            <ta e="T931" id="Seg_13107" s="T930">np.h:E</ta>
            <ta e="T932" id="Seg_13108" s="T931">adv:L</ta>
            <ta e="T934" id="Seg_13109" s="T933">np.h:Th</ta>
            <ta e="T936" id="Seg_13110" s="T935">pro.h:Th</ta>
            <ta e="T937" id="Seg_13111" s="T936">0.3.h:A</ta>
            <ta e="T938" id="Seg_13112" s="T937">np:G</ta>
            <ta e="T940" id="Seg_13113" s="T939">0.3.h:A</ta>
            <ta e="T943" id="Seg_13114" s="T942">np.h:A</ta>
            <ta e="T944" id="Seg_13115" s="T943">pro.h:A</ta>
            <ta e="T946" id="Seg_13116" s="T945">np:P</ta>
            <ta e="T948" id="Seg_13117" s="T947">0.3.h:A</ta>
            <ta e="T949" id="Seg_13118" s="T948">np:Th</ta>
            <ta e="T950" id="Seg_13119" s="T949">0.3.h:A</ta>
            <ta e="T951" id="Seg_13120" s="T950">np:Th</ta>
            <ta e="T952" id="Seg_13121" s="T951">0.3.h:A</ta>
            <ta e="T953" id="Seg_13122" s="T952">np:Th</ta>
            <ta e="T954" id="Seg_13123" s="T953">0.3.h:A</ta>
            <ta e="T955" id="Seg_13124" s="T954">0.3.h:A</ta>
            <ta e="T957" id="Seg_13125" s="T956">np.h:P</ta>
            <ta e="T958" id="Seg_13126" s="T957">np:Th</ta>
            <ta e="T961" id="Seg_13127" s="T960">np:G</ta>
            <ta e="T962" id="Seg_13128" s="T961">0.3.h:A</ta>
            <ta e="T963" id="Seg_13129" s="T962">pro.h:E</ta>
            <ta e="T967" id="Seg_13130" s="T966">np:G</ta>
            <ta e="T968" id="Seg_13131" s="T967">0.3.h:A</ta>
            <ta e="T969" id="Seg_13132" s="T968">pro.h:A</ta>
            <ta e="T973" id="Seg_13133" s="T972">0.1.h:A</ta>
            <ta e="T975" id="Seg_13134" s="T974">adv:L</ta>
            <ta e="T976" id="Seg_13135" s="T975">np.h:Th</ta>
            <ta e="T977" id="Seg_13136" s="T976">pro.h:E</ta>
            <ta e="T979" id="Seg_13137" s="T978">np:G</ta>
            <ta e="T980" id="Seg_13138" s="T979">0.1.h:A</ta>
            <ta e="T981" id="Seg_13139" s="T980">adv:Time</ta>
            <ta e="T983" id="Seg_13140" s="T982">np.h:A</ta>
            <ta e="T984" id="Seg_13141" s="T983">pro.h:A</ta>
            <ta e="T987" id="Seg_13142" s="T986">0.2.h:A</ta>
            <ta e="T988" id="Seg_13143" s="T987">0.3.h:A</ta>
            <ta e="T989" id="Seg_13144" s="T988">np:Th</ta>
            <ta e="T990" id="Seg_13145" s="T989">np:Th</ta>
            <ta e="T991" id="Seg_13146" s="T990">0.3.h:A</ta>
            <ta e="T992" id="Seg_13147" s="T991">np:P</ta>
            <ta e="T993" id="Seg_13148" s="T992">0.3.h:A</ta>
            <ta e="T994" id="Seg_13149" s="T993">np:Th</ta>
            <ta e="T995" id="Seg_13150" s="T994">0.3.h:A</ta>
            <ta e="T996" id="Seg_13151" s="T995">0.3.h:A</ta>
            <ta e="T997" id="Seg_13152" s="T996">adv:Time</ta>
            <ta e="T998" id="Seg_13153" s="T997">np.h:P</ta>
            <ta e="T1000" id="Seg_13154" s="T999">np:Th</ta>
            <ta e="T1001" id="Seg_13155" s="T1000">0.3.h:A</ta>
            <ta e="T1002" id="Seg_13156" s="T1001">pro.h:E</ta>
            <ta e="T1005" id="Seg_13157" s="T1004">np:G</ta>
            <ta e="T1006" id="Seg_13158" s="T1005">0.3.h:A</ta>
            <ta e="T1008" id="Seg_13159" s="T1007">0.2.h:A</ta>
            <ta e="T1010" id="Seg_13160" s="T1009">adv:L</ta>
            <ta e="T1011" id="Seg_13161" s="T1010">np:A</ta>
            <ta e="T1013" id="Seg_13162" s="T1012">pro.h:E</ta>
            <ta e="T1015" id="Seg_13163" s="T1014">np:G</ta>
            <ta e="T1016" id="Seg_13164" s="T1015">0.1.h:A</ta>
            <ta e="T1019" id="Seg_13165" s="T1018">adv:Time</ta>
            <ta e="T1021" id="Seg_13166" s="T1020">np.h:A</ta>
            <ta e="T1022" id="Seg_13167" s="T1021">pro.h:A</ta>
            <ta e="T1025" id="Seg_13168" s="T1024">0.2.h:A</ta>
            <ta e="T1029" id="Seg_13169" s="T1028">np:P</ta>
            <ta e="T1030" id="Seg_13170" s="T1029">0.3.h:A</ta>
            <ta e="T1031" id="Seg_13171" s="T1030">np:Th</ta>
            <ta e="T1032" id="Seg_13172" s="T1031">0.3.h:A</ta>
            <ta e="T1033" id="Seg_13173" s="T1032">np:Th</ta>
            <ta e="T1034" id="Seg_13174" s="T1033">0.3.h:A</ta>
            <ta e="T1036" id="Seg_13175" s="T1035">np:Th</ta>
            <ta e="T1037" id="Seg_13176" s="T1036">0.3.h:A</ta>
            <ta e="T1038" id="Seg_13177" s="T1037">0.3.h:A</ta>
            <ta e="T1039" id="Seg_13178" s="T1038">0.3.h:A</ta>
            <ta e="T1040" id="Seg_13179" s="T1039">0.3.h:A</ta>
            <ta e="T1041" id="Seg_13180" s="T1040">np:A</ta>
            <ta e="T1043" id="Seg_13181" s="T1042">pro.h:A</ta>
            <ta e="T1044" id="Seg_13182" s="T1043">np:Ins</ta>
            <ta e="T1046" id="Seg_13183" s="T1045">pro.h:E</ta>
            <ta e="T1049" id="Seg_13184" s="T1048">np:L</ta>
            <ta e="T1052" id="Seg_13185" s="T1051">pro.h:A</ta>
            <ta e="T1053" id="Seg_13186" s="T1052">np:Ins</ta>
            <ta e="T1054" id="Seg_13187" s="T1053">np:G</ta>
            <ta e="T1057" id="Seg_13188" s="T1056">np:Th</ta>
            <ta e="T1060" id="Seg_13189" s="T1059">adv:Time</ta>
            <ta e="T1061" id="Seg_13190" s="T1060">0.3.h:A</ta>
            <ta e="T1062" id="Seg_13191" s="T1061">0.3.h:A</ta>
            <ta e="T1063" id="Seg_13192" s="T1062">0.3.h:A</ta>
            <ta e="T1064" id="Seg_13193" s="T1063">np.h:E</ta>
            <ta e="T1067" id="Seg_13194" s="T1066">np.h:Poss</ta>
            <ta e="T1068" id="Seg_13195" s="T1067">np.h:E</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-PKZ">
            <ta e="T1" id="Seg_13196" s="T0">pro.h:S</ta>
            <ta e="T3" id="Seg_13197" s="T2">v:pred</ta>
            <ta e="T6" id="Seg_13198" s="T5">adj:pred</ta>
            <ta e="T7" id="Seg_13199" s="T6">cop</ta>
            <ta e="T8" id="Seg_13200" s="T7">np.h:S</ta>
            <ta e="T9" id="Seg_13201" s="T8">v:pred 0.1.h:S</ta>
            <ta e="T12" id="Seg_13202" s="T11">v:pred 0.1.h:S</ta>
            <ta e="T13" id="Seg_13203" s="T12">v:pred 0.1.h:S</ta>
            <ta e="T17" id="Seg_13204" s="T16">np.h:O</ta>
            <ta e="T18" id="Seg_13205" s="T17">v:pred 0.1.h:S</ta>
            <ta e="T19" id="Seg_13206" s="T18">v:pred 0.1.h:S</ta>
            <ta e="T20" id="Seg_13207" s="T19">pro.h:S</ta>
            <ta e="T21" id="Seg_13208" s="T20">pro:O</ta>
            <ta e="T22" id="Seg_13209" s="T21">ptcl.neg</ta>
            <ta e="T23" id="Seg_13210" s="T22">v:pred</ta>
            <ta e="T28" id="Seg_13211" s="T27">pro.h:S</ta>
            <ta e="T29" id="Seg_13212" s="T28">v:pred</ta>
            <ta e="T30" id="Seg_13213" s="T29">pro.h:S</ta>
            <ta e="T31" id="Seg_13214" s="T30">v:pred</ta>
            <ta e="T33" id="Seg_13215" s="T32">v:pred</ta>
            <ta e="T38" id="Seg_13216" s="T37">np.h:S</ta>
            <ta e="T40" id="Seg_13217" s="T39">v:pred</ta>
            <ta e="T41" id="Seg_13218" s="T40">np:O</ta>
            <ta e="T42" id="Seg_13219" s="T41">s:purp</ta>
            <ta e="T45" id="Seg_13220" s="T44">v:pred 0.3.h:S</ta>
            <ta e="T49" id="Seg_13221" s="T48">pro.h:S</ta>
            <ta e="T51" id="Seg_13222" s="T50">v:pred</ta>
            <ta e="T53" id="Seg_13223" s="T52">np.h:S</ta>
            <ta e="T55" id="Seg_13224" s="T54">v:pred</ta>
            <ta e="T65" id="Seg_13225" s="T64">pro.h:S</ta>
            <ta e="T67" id="Seg_13226" s="T66">v:pred</ta>
            <ta e="T69" id="Seg_13227" s="T68">np:S</ta>
            <ta e="T71" id="Seg_13228" s="T70">adj:pred</ta>
            <ta e="T74" id="Seg_13229" s="T73">v:pred 0.3.h:S</ta>
            <ta e="T76" id="Seg_13230" s="T75">np:S</ta>
            <ta e="T77" id="Seg_13231" s="T76">v:pred</ta>
            <ta e="T78" id="Seg_13232" s="T77">np:O</ta>
            <ta e="T79" id="Seg_13233" s="T78">v:pred 0.3.h:S</ta>
            <ta e="T82" id="Seg_13234" s="T81">pro.h:S</ta>
            <ta e="T84" id="Seg_13235" s="T83">v:pred</ta>
            <ta e="T86" id="Seg_13236" s="T85">np.h:S</ta>
            <ta e="T87" id="Seg_13237" s="T86">v:pred</ta>
            <ta e="T90" id="Seg_13238" s="T89">np.h:S</ta>
            <ta e="T95" id="Seg_13239" s="T94">pro.h:S</ta>
            <ta e="T96" id="Seg_13240" s="T95">v:pred</ta>
            <ta e="T97" id="Seg_13241" s="T96">pro.h:S</ta>
            <ta e="T100" id="Seg_13242" s="T99">v:pred</ta>
            <ta e="T101" id="Seg_13243" s="T100">s:purp</ta>
            <ta e="T105" id="Seg_13244" s="T104">v:pred 0.1.h:S</ta>
            <ta e="T108" id="Seg_13245" s="T107">v:pred 0.3.h:S</ta>
            <ta e="T113" id="Seg_13246" s="T112">v:pred 0.3.h:S</ta>
            <ta e="T115" id="Seg_13247" s="T114">np.h:S</ta>
            <ta e="T116" id="Seg_13248" s="T115">v:pred</ta>
            <ta e="T119" id="Seg_13249" s="T118">np:S</ta>
            <ta e="T120" id="Seg_13250" s="T119">v:pred</ta>
            <ta e="T122" id="Seg_13251" s="T121">ptcl.neg</ta>
            <ta e="T123" id="Seg_13252" s="T122">v:pred 0.1.h:S</ta>
            <ta e="T130" id="Seg_13253" s="T129">pro.h:S</ta>
            <ta e="T133" id="Seg_13254" s="T132">n:pred</ta>
            <ta e="T134" id="Seg_13255" s="T133">np:S</ta>
            <ta e="T136" id="Seg_13256" s="T135">v:pred</ta>
            <ta e="T137" id="Seg_13257" s="T136">s:purp</ta>
            <ta e="T138" id="Seg_13258" s="T137">v:pred 0.3.h:S</ta>
            <ta e="T140" id="Seg_13259" s="T139">ptcl.neg</ta>
            <ta e="T141" id="Seg_13260" s="T140">v:pred 0.2.h:S</ta>
            <ta e="T143" id="Seg_13261" s="T142">pro.h:S</ta>
            <ta e="T145" id="Seg_13262" s="T144">v:pred</ta>
            <ta e="T149" id="Seg_13263" s="T148">pro.h:S</ta>
            <ta e="T153" id="Seg_13264" s="T152">v:pred</ta>
            <ta e="T154" id="Seg_13265" s="T153">ptcl.neg</ta>
            <ta e="T155" id="Seg_13266" s="T154">adj:pred</ta>
            <ta e="T164" id="Seg_13267" s="T163">pro.h:S</ta>
            <ta e="T167" id="Seg_13268" s="T166">v:pred</ta>
            <ta e="T170" id="Seg_13269" s="T169">np:S</ta>
            <ta e="T171" id="Seg_13270" s="T170">v:pred</ta>
            <ta e="T172" id="Seg_13271" s="T171">pro.h:S</ta>
            <ta e="T174" id="Seg_13272" s="T173">ptcl.neg</ta>
            <ta e="T175" id="Seg_13273" s="T174">v:pred</ta>
            <ta e="T180" id="Seg_13274" s="T179">v:pred 0.3.h:S</ta>
            <ta e="T183" id="Seg_13275" s="T182">pro.h:S</ta>
            <ta e="T186" id="Seg_13276" s="T185">v:pred</ta>
            <ta e="T187" id="Seg_13277" s="T186">np:S</ta>
            <ta e="T192" id="Seg_13278" s="T191">adj:pred</ta>
            <ta e="T196" id="Seg_13279" s="T195">v:pred 0.3:S</ta>
            <ta e="T201" id="Seg_13280" s="T200">pro.h:S</ta>
            <ta e="T203" id="Seg_13281" s="T202">v:pred</ta>
            <ta e="T206" id="Seg_13282" s="T205">ptcl.neg</ta>
            <ta e="T211" id="Seg_13283" s="T210">ptcl:pred</ta>
            <ta e="T213" id="Seg_13284" s="T212">n:pred</ta>
            <ta e="T214" id="Seg_13285" s="T213">np:S</ta>
            <ta e="T215" id="Seg_13286" s="T214">np:S</ta>
            <ta e="T216" id="Seg_13287" s="T215">adj:pred</ta>
            <ta e="T220" id="Seg_13288" s="T219">np:S</ta>
            <ta e="T221" id="Seg_13289" s="T220">v:pred</ta>
            <ta e="T223" id="Seg_13290" s="T222">np:S</ta>
            <ta e="T224" id="Seg_13291" s="T223">v:pred</ta>
            <ta e="T227" id="Seg_13292" s="T226">pro.h:S</ta>
            <ta e="T229" id="Seg_13293" s="T228">v:pred</ta>
            <ta e="T231" id="Seg_13294" s="T230">v:pred 0.3.h:S</ta>
            <ta e="T232" id="Seg_13295" s="T231">s:purp</ta>
            <ta e="T241" id="Seg_13296" s="T240">np:O</ta>
            <ta e="T242" id="Seg_13297" s="T241">ptcl.neg</ta>
            <ta e="T243" id="Seg_13298" s="T242">v:pred 0.3.h:S</ta>
            <ta e="T248" id="Seg_13299" s="T247">np:O</ta>
            <ta e="T250" id="Seg_13300" s="T249">np.h:S</ta>
            <ta e="T251" id="Seg_13301" s="T250">v:pred</ta>
            <ta e="T254" id="Seg_13302" s="T253">pro.h:S</ta>
            <ta e="T255" id="Seg_13303" s="T254">ptcl.neg</ta>
            <ta e="T256" id="Seg_13304" s="T255">v:pred</ta>
            <ta e="T258" id="Seg_13305" s="T257">np:O</ta>
            <ta e="T260" id="Seg_13306" s="T259">pro.h:S</ta>
            <ta e="T262" id="Seg_13307" s="T261">v:pred</ta>
            <ta e="T263" id="Seg_13308" s="T262">pro.h:S</ta>
            <ta e="T265" id="Seg_13309" s="T264">v:pred</ta>
            <ta e="T267" id="Seg_13310" s="T266">v:pred 0.1.h:S</ta>
            <ta e="T269" id="Seg_13311" s="T268">pro.h:S</ta>
            <ta e="T271" id="Seg_13312" s="T270">v:pred</ta>
            <ta e="T272" id="Seg_13313" s="T271">v:pred 0.1.h:S</ta>
            <ta e="T273" id="Seg_13314" s="T272">ptcl.neg</ta>
            <ta e="T275" id="Seg_13315" s="T274">v:pred 0.1.h:S</ta>
            <ta e="T277" id="Seg_13316" s="T276">ptcl.neg</ta>
            <ta e="T279" id="Seg_13317" s="T278">np.h:S</ta>
            <ta e="T280" id="Seg_13318" s="T279">v:pred</ta>
            <ta e="T281" id="Seg_13319" s="T280">pro.h:O</ta>
            <ta e="T282" id="Seg_13320" s="T281">v:pred 0.3.h:S</ta>
            <ta e="T284" id="Seg_13321" s="T283">pro.h:S</ta>
            <ta e="T286" id="Seg_13322" s="T285">np:O</ta>
            <ta e="T287" id="Seg_13323" s="T286">v:pred</ta>
            <ta e="T288" id="Seg_13324" s="T287">np:S</ta>
            <ta e="T289" id="Seg_13325" s="T288">v:pred</ta>
            <ta e="T291" id="Seg_13326" s="T290">ptcl.neg</ta>
            <ta e="T292" id="Seg_13327" s="T291">v:pred 0.3.h:S</ta>
            <ta e="T294" id="Seg_13328" s="T293">v:pred 0.3.h:S</ta>
            <ta e="T295" id="Seg_13329" s="T294">v:pred 0.3.h:S</ta>
            <ta e="T296" id="Seg_13330" s="T295">np.h:S</ta>
            <ta e="T297" id="Seg_13331" s="T296">v:pred</ta>
            <ta e="T298" id="Seg_13332" s="T297">v:pred 0.2.h:S</ta>
            <ta e="T299" id="Seg_13333" s="T298">pro.h:O</ta>
            <ta e="T306" id="Seg_13334" s="T305">v:pred 0.2.h:S</ta>
            <ta e="T307" id="Seg_13335" s="T306">pro.h:O</ta>
            <ta e="T308" id="Seg_13336" s="T307">pro.h:S</ta>
            <ta e="T309" id="Seg_13337" s="T308">pro.h:O</ta>
            <ta e="T310" id="Seg_13338" s="T309">v:pred</ta>
            <ta e="T312" id="Seg_13339" s="T311">v:pred 0.3.h:S</ta>
            <ta e="T313" id="Seg_13340" s="T312">v:pred 0.3.h:S</ta>
            <ta e="T314" id="Seg_13341" s="T313">v:pred 0.3.h:S</ta>
            <ta e="T319" id="Seg_13342" s="T318">np.h:S</ta>
            <ta e="T321" id="Seg_13343" s="T320">v:pred</ta>
            <ta e="T322" id="Seg_13344" s="T321">v:pred 0.2.h:S</ta>
            <ta e="T323" id="Seg_13345" s="T322">pro.h:O</ta>
            <ta e="T326" id="Seg_13346" s="T325">pro.h:O</ta>
            <ta e="T328" id="Seg_13347" s="T327">v:pred 0.2.h:S</ta>
            <ta e="T329" id="Seg_13348" s="T328">pro.h:S</ta>
            <ta e="T330" id="Seg_13349" s="T329">v:pred</ta>
            <ta e="T331" id="Seg_13350" s="T330">v:pred 0.3.h:S</ta>
            <ta e="T333" id="Seg_13351" s="T332">v:pred 0.3.h:S</ta>
            <ta e="T334" id="Seg_13352" s="T333">v:pred 0.3.h:S</ta>
            <ta e="T338" id="Seg_13353" s="T337">np.h:S</ta>
            <ta e="T339" id="Seg_13354" s="T338">v:pred</ta>
            <ta e="T340" id="Seg_13355" s="T339">v:pred 0.2.h:S</ta>
            <ta e="T341" id="Seg_13356" s="T340">pro.h:O</ta>
            <ta e="T342" id="Seg_13357" s="T341">pro.h:S</ta>
            <ta e="T343" id="Seg_13358" s="T342">pro.h:O</ta>
            <ta e="T344" id="Seg_13359" s="T343">v:pred</ta>
            <ta e="T347" id="Seg_13360" s="T346">v:pred 0.3.h:S</ta>
            <ta e="T350" id="Seg_13361" s="T349">v:pred 0.3.h:S</ta>
            <ta e="T351" id="Seg_13362" s="T350">np.h:S</ta>
            <ta e="T352" id="Seg_13363" s="T351">v:pred</ta>
            <ta e="T353" id="Seg_13364" s="T352">v:pred 0.2.h:S</ta>
            <ta e="T354" id="Seg_13365" s="T353">pro.h:O</ta>
            <ta e="T356" id="Seg_13366" s="T355">pro.h:S</ta>
            <ta e="T357" id="Seg_13367" s="T356">np.h:O</ta>
            <ta e="T358" id="Seg_13368" s="T357">v:pred</ta>
            <ta e="T360" id="Seg_13369" s="T359">v:pred 0.3.h:S</ta>
            <ta e="T363" id="Seg_13370" s="T362">np:S</ta>
            <ta e="T364" id="Seg_13371" s="T363">v:pred</ta>
            <ta e="T366" id="Seg_13372" s="T365">np:S</ta>
            <ta e="T368" id="Seg_13373" s="T367">adj:pred</ta>
            <ta e="T369" id="Seg_13374" s="T368">pro.h:S</ta>
            <ta e="T370" id="Seg_13375" s="T369">np:O</ta>
            <ta e="T371" id="Seg_13376" s="T370">v:pred</ta>
            <ta e="T372" id="Seg_13377" s="T371">np:O</ta>
            <ta e="T373" id="Seg_13378" s="T372">v:pred 0.3.h:S</ta>
            <ta e="T374" id="Seg_13379" s="T373">v:pred 0.3.h:S</ta>
            <ta e="T375" id="Seg_13380" s="T374">pro.h:O</ta>
            <ta e="T376" id="Seg_13381" s="T375">v:pred</ta>
            <ta e="T377" id="Seg_13382" s="T376">np.h:S</ta>
            <ta e="T378" id="Seg_13383" s="T377">pro.h:S</ta>
            <ta e="T380" id="Seg_13384" s="T379">v:pred</ta>
            <ta e="T381" id="Seg_13385" s="T380">pro.h:S</ta>
            <ta e="T383" id="Seg_13386" s="T382">np:O</ta>
            <ta e="T384" id="Seg_13387" s="T383">v:pred</ta>
            <ta e="T385" id="Seg_13388" s="T384">np:S</ta>
            <ta e="T386" id="Seg_13389" s="T385">v:pred</ta>
            <ta e="T389" id="Seg_13390" s="T388">v:pred 0.3.h:S</ta>
            <ta e="T393" id="Seg_13391" s="T392">v:pred 0.3.h:S</ta>
            <ta e="T394" id="Seg_13392" s="T393">v:pred 0.2.h:S</ta>
            <ta e="T395" id="Seg_13393" s="T394">np.h:O</ta>
            <ta e="T396" id="Seg_13394" s="T395">v:pred 0.3.h:S</ta>
            <ta e="T397" id="Seg_13395" s="T396">pro:S</ta>
            <ta e="T399" id="Seg_13396" s="T398">adj:pred</ta>
            <ta e="T400" id="Seg_13397" s="T399">pro.h:S</ta>
            <ta e="T401" id="Seg_13398" s="T400">v:pred</ta>
            <ta e="T403" id="Seg_13399" s="T402">v:pred 0.3.h:S</ta>
            <ta e="T404" id="Seg_13400" s="T403">pro:S</ta>
            <ta e="T406" id="Seg_13401" s="T405">adj:pred</ta>
            <ta e="T409" id="Seg_13402" s="T408">np.h:O</ta>
            <ta e="T410" id="Seg_13403" s="T409">v:pred 0.1.h:S</ta>
            <ta e="T413" id="Seg_13404" s="T412">np.h:S</ta>
            <ta e="T414" id="Seg_13405" s="T413">v:pred</ta>
            <ta e="T415" id="Seg_13406" s="T414">v:pred 0.3.h:S</ta>
            <ta e="T416" id="Seg_13407" s="T415">pro.h:S</ta>
            <ta e="T417" id="Seg_13408" s="T416">v:pred</ta>
            <ta e="T421" id="Seg_13409" s="T420">np.h:O</ta>
            <ta e="T422" id="Seg_13410" s="T421">v:pred 0.1.h:S</ta>
            <ta e="T423" id="Seg_13411" s="T422">ptcl:pred</ta>
            <ta e="T426" id="Seg_13412" s="T425">ptcl.neg</ta>
            <ta e="T427" id="Seg_13413" s="T426">adj:pred</ta>
            <ta e="T428" id="Seg_13414" s="T427">cop 0.3:S</ta>
            <ta e="T430" id="Seg_13415" s="T429">ptcl.neg</ta>
            <ta e="T431" id="Seg_13416" s="T430">v:pred 0.1.h:S</ta>
            <ta e="T432" id="Seg_13417" s="T431">pro.h:S</ta>
            <ta e="T433" id="Seg_13418" s="T432">v:pred</ta>
            <ta e="T435" id="Seg_13419" s="T434">v:pred 0.3.h:S</ta>
            <ta e="T437" id="Seg_13420" s="T436">np.h:S</ta>
            <ta e="T438" id="Seg_13421" s="T437">ptcl.neg</ta>
            <ta e="T439" id="Seg_13422" s="T438">v:pred</ta>
            <ta e="T440" id="Seg_13423" s="T439">np.h:O</ta>
            <ta e="T441" id="Seg_13424" s="T440">ptcl.neg</ta>
            <ta e="T442" id="Seg_13425" s="T441">v:pred 0.3.h:S</ta>
            <ta e="T443" id="Seg_13426" s="T442">np:O</ta>
            <ta e="T445" id="Seg_13427" s="T444">ptcl.neg</ta>
            <ta e="T446" id="Seg_13428" s="T445">np:O</ta>
            <ta e="T447" id="Seg_13429" s="T446">v:pred 0.3.h:S</ta>
            <ta e="T449" id="Seg_13430" s="T448">v:pred 0.2.h:S</ta>
            <ta e="T451" id="Seg_13431" s="T450">np:O</ta>
            <ta e="T452" id="Seg_13432" s="T451">pro.h:S</ta>
            <ta e="T455" id="Seg_13433" s="T454">np:O</ta>
            <ta e="T456" id="Seg_13434" s="T455">v:pred</ta>
            <ta e="T1072" id="Seg_13435" s="T457">conv:pred</ta>
            <ta e="T458" id="Seg_13436" s="T1072">v:pred 0.3.h:S</ta>
            <ta e="T460" id="Seg_13437" s="T459">pro.h:S</ta>
            <ta e="T461" id="Seg_13438" s="T460">np.h:O</ta>
            <ta e="T462" id="Seg_13439" s="T461">v:pred</ta>
            <ta e="T466" id="Seg_13440" s="T465">np.h:S</ta>
            <ta e="T467" id="Seg_13441" s="T466">np:O</ta>
            <ta e="T469" id="Seg_13442" s="T468">v:pred</ta>
            <ta e="T471" id="Seg_13443" s="T470">v:pred 0.3.h:S</ta>
            <ta e="T472" id="Seg_13444" s="T471">v:pred 0.2.h:S</ta>
            <ta e="T473" id="Seg_13445" s="T472">np:O</ta>
            <ta e="T474" id="Seg_13446" s="T473">ptcl:pred</ta>
            <ta e="T475" id="Seg_13447" s="T474">np:O</ta>
            <ta e="T478" id="Seg_13448" s="T477">np:O</ta>
            <ta e="T480" id="Seg_13449" s="T479">v:pred 0.3.h:S</ta>
            <ta e="T482" id="Seg_13450" s="T481">pro.h:S</ta>
            <ta e="T485" id="Seg_13451" s="T484">v:pred</ta>
            <ta e="T487" id="Seg_13452" s="T486">np.h:O</ta>
            <ta e="T489" id="Seg_13453" s="T488">pro.h:S</ta>
            <ta e="T490" id="Seg_13454" s="T489">v:pred</ta>
            <ta e="T491" id="Seg_13455" s="T490">pro.h:S</ta>
            <ta e="T493" id="Seg_13456" s="T492">v:pred</ta>
            <ta e="T495" id="Seg_13457" s="T494">v:pred 0.3.h:S</ta>
            <ta e="T497" id="Seg_13458" s="T495">v:pred 0.2.h:S</ta>
            <ta e="T500" id="Seg_13459" s="T499">np.h:S</ta>
            <ta e="T503" id="Seg_13460" s="T502">v:pred 0.2.h:S</ta>
            <ta e="T504" id="Seg_13461" s="T503">np:O</ta>
            <ta e="T505" id="Seg_13462" s="T504">ptcl:pred</ta>
            <ta e="T506" id="Seg_13463" s="T505">np:S</ta>
            <ta e="T507" id="Seg_13464" s="T506">pro:O</ta>
            <ta e="T508" id="Seg_13465" s="T507">v:pred</ta>
            <ta e="T509" id="Seg_13466" s="T508">pro.h:S</ta>
            <ta e="T510" id="Seg_13467" s="T509">v:pred</ta>
            <ta e="T511" id="Seg_13468" s="T510">np:O</ta>
            <ta e="T513" id="Seg_13469" s="T512">pro.h:S</ta>
            <ta e="T514" id="Seg_13470" s="T513">np.h:O</ta>
            <ta e="T515" id="Seg_13471" s="T514">v:pred</ta>
            <ta e="T516" id="Seg_13472" s="T515">np:S</ta>
            <ta e="T518" id="Seg_13473" s="T517">v:pred</ta>
            <ta e="T521" id="Seg_13474" s="T520">np.h:S</ta>
            <ta e="T523" id="Seg_13475" s="T522">np.h:O</ta>
            <ta e="T524" id="Seg_13476" s="T523">v:pred</ta>
            <ta e="T525" id="Seg_13477" s="T524">v:pred 0.1.h:S</ta>
            <ta e="T526" id="Seg_13478" s="T525">pro.h:O</ta>
            <ta e="T527" id="Seg_13479" s="T526">v:pred 0.1.h:S</ta>
            <ta e="T528" id="Seg_13480" s="T527">np.h:S</ta>
            <ta e="T529" id="Seg_13481" s="T528">v:pred</ta>
            <ta e="T530" id="Seg_13482" s="T529">pro.h:S</ta>
            <ta e="T533" id="Seg_13483" s="T532">np:O</ta>
            <ta e="T534" id="Seg_13484" s="T533">v:pred</ta>
            <ta e="T538" id="Seg_13485" s="T537">np.h:S</ta>
            <ta e="T540" id="Seg_13486" s="T539">v:pred</ta>
            <ta e="T545" id="Seg_13487" s="T544">np.h:S</ta>
            <ta e="T546" id="Seg_13488" s="T545">v:pred</ta>
            <ta e="T547" id="Seg_13489" s="T546">np:S</ta>
            <ta e="T549" id="Seg_13490" s="T548">v:pred</ta>
            <ta e="T550" id="Seg_13491" s="T549">v:pred 0.3.h:S</ta>
            <ta e="T552" id="Seg_13492" s="T551">v:pred</ta>
            <ta e="T553" id="Seg_13493" s="T552">np:S</ta>
            <ta e="T554" id="Seg_13494" s="T553">np:S</ta>
            <ta e="T555" id="Seg_13495" s="T554">np:S</ta>
            <ta e="T556" id="Seg_13496" s="T555">v:pred 0.1.h:S</ta>
            <ta e="T557" id="Seg_13497" s="T556">np:S</ta>
            <ta e="T559" id="Seg_13498" s="T558">v:pred 0.3.h:S</ta>
            <ta e="T560" id="Seg_13499" s="T559">pro:S</ta>
            <ta e="T562" id="Seg_13500" s="T561">ptcl.neg</ta>
            <ta e="T563" id="Seg_13501" s="T562">adj:pred</ta>
            <ta e="T565" id="Seg_13502" s="T564">np.h:O</ta>
            <ta e="T566" id="Seg_13503" s="T565">v:pred</ta>
            <ta e="T571" id="Seg_13504" s="T570">np:O</ta>
            <ta e="T572" id="Seg_13505" s="T571">v:pred 0.3.h:S</ta>
            <ta e="T577" id="Seg_13506" s="T576">v:pred 0.3.h:S</ta>
            <ta e="T578" id="Seg_13507" s="T577">np.h:O</ta>
            <ta e="T579" id="Seg_13508" s="T578">pro.h:S</ta>
            <ta e="T581" id="Seg_13509" s="T580">v:pred</ta>
            <ta e="T584" id="Seg_13510" s="T583">v:pred 0.3.h:S</ta>
            <ta e="T587" id="Seg_13511" s="T586">np.h:S</ta>
            <ta e="T589" id="Seg_13512" s="T588">v:pred</ta>
            <ta e="T591" id="Seg_13513" s="T590">np.h:S</ta>
            <ta e="T592" id="Seg_13514" s="T591">v:pred</ta>
            <ta e="T593" id="Seg_13515" s="T592">pro.h:S</ta>
            <ta e="T594" id="Seg_13516" s="T593">v:pred</ta>
            <ta e="T595" id="Seg_13517" s="T594">v:pred 0.3.h:S</ta>
            <ta e="T598" id="Seg_13518" s="T597">np.h:S</ta>
            <ta e="T604" id="Seg_13519" s="T603">s:purp</ta>
            <ta e="T605" id="Seg_13520" s="T604">v:pred 0.3.h:S</ta>
            <ta e="T607" id="Seg_13521" s="T606">v:pred 0.3.h:S</ta>
            <ta e="T611" id="Seg_13522" s="T610">np.h:S</ta>
            <ta e="T612" id="Seg_13523" s="T611">v:pred</ta>
            <ta e="T613" id="Seg_13524" s="T612">pro.h:S</ta>
            <ta e="T614" id="Seg_13525" s="T613">np:O</ta>
            <ta e="T616" id="Seg_13526" s="T615">v:pred</ta>
            <ta e="T621" id="Seg_13527" s="T620">v:pred 0.3.h:S</ta>
            <ta e="T624" id="Seg_13528" s="T623">np:S</ta>
            <ta e="T628" id="Seg_13529" s="T627">adj:pred</ta>
            <ta e="T629" id="Seg_13530" s="T628">cop</ta>
            <ta e="T631" id="Seg_13531" s="T630">pro.h:S</ta>
            <ta e="T632" id="Seg_13532" s="T631">v:pred</ta>
            <ta e="T633" id="Seg_13533" s="T632">ptcl.neg</ta>
            <ta e="T634" id="Seg_13534" s="T633">v:pred</ta>
            <ta e="T637" id="Seg_13535" s="T636">np.h:S</ta>
            <ta e="T639" id="Seg_13536" s="T638">np.h:O</ta>
            <ta e="T640" id="Seg_13537" s="T639">v:pred 0.3.h:S</ta>
            <ta e="T646" id="Seg_13538" s="T645">v:pred 0.3.h:S</ta>
            <ta e="T648" id="Seg_13539" s="T647">pro.h:S</ta>
            <ta e="T649" id="Seg_13540" s="T648">v:pred</ta>
            <ta e="T650" id="Seg_13541" s="T649">v:pred 0.2.h:S</ta>
            <ta e="T651" id="Seg_13542" s="T650">pro.h:S</ta>
            <ta e="T654" id="Seg_13543" s="T653">v:pred</ta>
            <ta e="T655" id="Seg_13544" s="T654">pro.h:S</ta>
            <ta e="T658" id="Seg_13545" s="T657">np:O</ta>
            <ta e="T659" id="Seg_13546" s="T658">v:pred</ta>
            <ta e="T661" id="Seg_13547" s="T660">np.h:O</ta>
            <ta e="T662" id="Seg_13548" s="T661">v:pred 0.3.h:S</ta>
            <ta e="T664" id="Seg_13549" s="T663">pro.h:S</ta>
            <ta e="T669" id="Seg_13550" s="T668">v:pred</ta>
            <ta e="T672" id="Seg_13551" s="T671">np.h:S</ta>
            <ta e="T673" id="Seg_13552" s="T672">v:pred</ta>
            <ta e="T674" id="Seg_13553" s="T673">pro.h:S</ta>
            <ta e="T675" id="Seg_13554" s="T674">v:pred</ta>
            <ta e="T676" id="Seg_13555" s="T675">np.h:O</ta>
            <ta e="T678" id="Seg_13556" s="T677">np.h:O</ta>
            <ta e="T680" id="Seg_13557" s="T679">v:pred</ta>
            <ta e="T681" id="Seg_13558" s="T680">np.h:S</ta>
            <ta e="T682" id="Seg_13559" s="T681">np.h:S</ta>
            <ta e="T684" id="Seg_13560" s="T683">pro:S</ta>
            <ta e="T685" id="Seg_13561" s="T684">v:pred</ta>
            <ta e="T688" id="Seg_13562" s="T687">np:S</ta>
            <ta e="T689" id="Seg_13563" s="T688">pro.h:S</ta>
            <ta e="T690" id="Seg_13564" s="T689">s:purp</ta>
            <ta e="T691" id="Seg_13565" s="T690">v:pred</ta>
            <ta e="T692" id="Seg_13566" s="T691">v:pred</ta>
            <ta e="T694" id="Seg_13567" s="T693">np:O</ta>
            <ta e="T695" id="Seg_13568" s="T694">v:pred 0.3.h:S</ta>
            <ta e="T696" id="Seg_13569" s="T695">np.h:O</ta>
            <ta e="T697" id="Seg_13570" s="T696">v:pred 0.3.h:S</ta>
            <ta e="T698" id="Seg_13571" s="T697">v:pred 0.2.h:S</ta>
            <ta e="T701" id="Seg_13572" s="T700">n:pred</ta>
            <ta e="T702" id="Seg_13573" s="T701">np:S</ta>
            <ta e="T703" id="Seg_13574" s="T702">cop</ta>
            <ta e="T705" id="Seg_13575" s="T704">np.h:S</ta>
            <ta e="T706" id="Seg_13576" s="T705">v:pred</ta>
            <ta e="T707" id="Seg_13577" s="T706">v:pred 0.3.h:S</ta>
            <ta e="T709" id="Seg_13578" s="T708">np:O</ta>
            <ta e="T710" id="Seg_13579" s="T709">v:pred 0.3.h:S</ta>
            <ta e="T712" id="Seg_13580" s="T711">v:pred 0.3.h:S</ta>
            <ta e="T714" id="Seg_13581" s="T713">np:S</ta>
            <ta e="T715" id="Seg_13582" s="T714">v:pred</ta>
            <ta e="T716" id="Seg_13583" s="T715">np:S</ta>
            <ta e="T717" id="Seg_13584" s="T716">v:pred</ta>
            <ta e="T718" id="Seg_13585" s="T717">pro.h:S</ta>
            <ta e="T719" id="Seg_13586" s="T718">v:pred</ta>
            <ta e="T721" id="Seg_13587" s="T720">v:pred 0.3.h:S</ta>
            <ta e="T722" id="Seg_13588" s="T721">v:pred 0.3.h:S</ta>
            <ta e="T724" id="Seg_13589" s="T723">pro.h:S</ta>
            <ta e="T725" id="Seg_13590" s="T724">v:pred</ta>
            <ta e="T727" id="Seg_13591" s="T726">np:O</ta>
            <ta e="T729" id="Seg_13592" s="T728">v:pred 0.3.h:S</ta>
            <ta e="T730" id="Seg_13593" s="T729">np:O</ta>
            <ta e="T731" id="Seg_13594" s="T730">np:O</ta>
            <ta e="T733" id="Seg_13595" s="T732">v:pred 0.3.h:S</ta>
            <ta e="T735" id="Seg_13596" s="T734">v:pred 0.3.h:S</ta>
            <ta e="T737" id="Seg_13597" s="T736">v:pred 0.2:S</ta>
            <ta e="T739" id="Seg_13598" s="T738">np:S</ta>
            <ta e="T740" id="Seg_13599" s="T739">v:pred</ta>
            <ta e="T741" id="Seg_13600" s="T740">pro.h:S</ta>
            <ta e="T742" id="Seg_13601" s="T741">v:pred</ta>
            <ta e="T743" id="Seg_13602" s="T742">conv:pred</ta>
            <ta e="T744" id="Seg_13603" s="T743">pro.h:S</ta>
            <ta e="T745" id="Seg_13604" s="T744">v:pred</ta>
            <ta e="T746" id="Seg_13605" s="T745">np:S</ta>
            <ta e="T747" id="Seg_13606" s="T746">v:pred</ta>
            <ta e="T748" id="Seg_13607" s="T747">np:S</ta>
            <ta e="T749" id="Seg_13608" s="T748">v:pred</ta>
            <ta e="T750" id="Seg_13609" s="T749">np:S</ta>
            <ta e="T751" id="Seg_13610" s="T750">v:pred</ta>
            <ta e="T752" id="Seg_13611" s="T751">pro.h:S</ta>
            <ta e="T754" id="Seg_13612" s="T753">v:pred</ta>
            <ta e="T755" id="Seg_13613" s="T754">n:pred</ta>
            <ta e="T757" id="Seg_13614" s="T756">n:pred</ta>
            <ta e="T758" id="Seg_13615" s="T757">cop 0.3.h:S</ta>
            <ta e="T759" id="Seg_13616" s="T758">n:pred</ta>
            <ta e="T761" id="Seg_13617" s="T760">n:pred</ta>
            <ta e="T762" id="Seg_13618" s="T761">cop 0.3.h:S</ta>
            <ta e="T765" id="Seg_13619" s="T764">np.h:S</ta>
            <ta e="T766" id="Seg_13620" s="T765">v:pred</ta>
            <ta e="T767" id="Seg_13621" s="T766">v:pred 0.3.h:S</ta>
            <ta e="T768" id="Seg_13622" s="T767">pro.h:S</ta>
            <ta e="T769" id="Seg_13623" s="T768">pro.h:O</ta>
            <ta e="T770" id="Seg_13624" s="T769">v:pred</ta>
            <ta e="T771" id="Seg_13625" s="T770">conv:pred</ta>
            <ta e="T772" id="Seg_13626" s="T771">v:pred 0.3.h:S</ta>
            <ta e="T773" id="Seg_13627" s="T772">pro.h:S</ta>
            <ta e="T774" id="Seg_13628" s="T773">v:pred</ta>
            <ta e="T777" id="Seg_13629" s="T776">np:S</ta>
            <ta e="T778" id="Seg_13630" s="T777">np:S</ta>
            <ta e="T779" id="Seg_13631" s="T778">v:pred</ta>
            <ta e="T781" id="Seg_13632" s="T780">pro.h:S</ta>
            <ta e="T782" id="Seg_13633" s="T781">v:pred</ta>
            <ta e="T785" id="Seg_13634" s="T784">v:pred 0.2.h:S</ta>
            <ta e="T786" id="Seg_13635" s="T785">pro.h:S</ta>
            <ta e="T788" id="Seg_13636" s="T787">v:pred</ta>
            <ta e="T789" id="Seg_13637" s="T788">v:pred 0.1.h:S</ta>
            <ta e="T791" id="Seg_13638" s="T790">v:pred 0.3.h:S</ta>
            <ta e="T792" id="Seg_13639" s="T791">pro.h:S</ta>
            <ta e="T793" id="Seg_13640" s="T792">v:pred</ta>
            <ta e="T794" id="Seg_13641" s="T793">pro.h:S</ta>
            <ta e="T797" id="Seg_13642" s="T796">np:O</ta>
            <ta e="T799" id="Seg_13643" s="T798">v:pred</ta>
            <ta e="T802" id="Seg_13644" s="T801">np:O</ta>
            <ta e="T803" id="Seg_13645" s="T802">v:pred 0.3.h:S</ta>
            <ta e="T804" id="Seg_13646" s="T803">np:O</ta>
            <ta e="T805" id="Seg_13647" s="T804">v:pred 0.3.h:S</ta>
            <ta e="T806" id="Seg_13648" s="T805">np:O</ta>
            <ta e="T807" id="Seg_13649" s="T806">v:pred 0.3.h:S</ta>
            <ta e="T809" id="Seg_13650" s="T808">v:pred 0.3.h:S</ta>
            <ta e="T811" id="Seg_13651" s="T810">v:pred 0.3.h:S</ta>
            <ta e="T817" id="Seg_13652" s="T816">v:pred 0.3.h:S</ta>
            <ta e="T819" id="Seg_13653" s="T818">np.h:S</ta>
            <ta e="T823" id="Seg_13654" s="T822">np:O</ta>
            <ta e="T832" id="Seg_13655" s="T831">np:O</ta>
            <ta e="T833" id="Seg_13656" s="T832">v:pred 0.1.h:S</ta>
            <ta e="T835" id="Seg_13657" s="T834">np:O</ta>
            <ta e="T836" id="Seg_13658" s="T835">v:pred 0.1.h:S</ta>
            <ta e="T838" id="Seg_13659" s="T837">pro.h:S</ta>
            <ta e="T839" id="Seg_13660" s="T838">v:pred</ta>
            <ta e="T841" id="Seg_13661" s="T840">pro.h:S</ta>
            <ta e="T842" id="Seg_13662" s="T841">v:pred</ta>
            <ta e="T843" id="Seg_13663" s="T842">v:pred</ta>
            <ta e="T844" id="Seg_13664" s="T843">np:S</ta>
            <ta e="T846" id="Seg_13665" s="T845">v:pred</ta>
            <ta e="T848" id="Seg_13666" s="T847">np:S</ta>
            <ta e="T850" id="Seg_13667" s="T849">v:pred</ta>
            <ta e="T851" id="Seg_13668" s="T850">np:O</ta>
            <ta e="T853" id="Seg_13669" s="T852">v:pred</ta>
            <ta e="T854" id="Seg_13670" s="T853">np:S</ta>
            <ta e="T855" id="Seg_13671" s="T854">np:O</ta>
            <ta e="T856" id="Seg_13672" s="T855">pro.h:S</ta>
            <ta e="T857" id="Seg_13673" s="T856">v:pred</ta>
            <ta e="T859" id="Seg_13674" s="T858">pro.h:S</ta>
            <ta e="T860" id="Seg_13675" s="T859">v:pred</ta>
            <ta e="T861" id="Seg_13676" s="T860">v:pred 0.3.h:S</ta>
            <ta e="T863" id="Seg_13677" s="T862">np:S</ta>
            <ta e="T864" id="Seg_13678" s="T863">np:S</ta>
            <ta e="T865" id="Seg_13679" s="T864">np:S</ta>
            <ta e="T866" id="Seg_13680" s="T865">v:pred</ta>
            <ta e="T868" id="Seg_13681" s="T867">pro.h:S</ta>
            <ta e="T869" id="Seg_13682" s="T868">v:pred</ta>
            <ta e="T871" id="Seg_13683" s="T870">ptcl:pred</ta>
            <ta e="T872" id="Seg_13684" s="T871">v:pred 0.2.h:S</ta>
            <ta e="T875" id="Seg_13685" s="T874">np:S</ta>
            <ta e="T876" id="Seg_13686" s="T875">v:pred</ta>
            <ta e="T877" id="Seg_13687" s="T876">pro.h:S</ta>
            <ta e="T881" id="Seg_13688" s="T880">v:pred</ta>
            <ta e="T884" id="Seg_13689" s="T883">np:S</ta>
            <ta e="T885" id="Seg_13690" s="T884">np:S</ta>
            <ta e="T887" id="Seg_13691" s="T886">np:S</ta>
            <ta e="T888" id="Seg_13692" s="T887">v:pred</ta>
            <ta e="T890" id="Seg_13693" s="T889">ptcl:pred</ta>
            <ta e="T899" id="Seg_13694" s="T898">pro:S</ta>
            <ta e="T900" id="Seg_13695" s="T899">n:pred</ta>
            <ta e="T901" id="Seg_13696" s="T900">cop</ta>
            <ta e="T904" id="Seg_13697" s="T903">pro:S</ta>
            <ta e="T905" id="Seg_13698" s="T904">n:pred</ta>
            <ta e="T906" id="Seg_13699" s="T905">cop</ta>
            <ta e="T911" id="Seg_13700" s="T910">v:pred 0.1.h:S</ta>
            <ta e="T915" id="Seg_13701" s="T914">v:pred 0.3.h:S</ta>
            <ta e="T916" id="Seg_13702" s="T915">ptcl:pred</ta>
            <ta e="T920" id="Seg_13703" s="T919">np.h:S</ta>
            <ta e="T922" id="Seg_13704" s="T921">np.h:S</ta>
            <ta e="T923" id="Seg_13705" s="T922">v:pred</ta>
            <ta e="T924" id="Seg_13706" s="T923">pro:S</ta>
            <ta e="T926" id="Seg_13707" s="T925">v:pred</ta>
            <ta e="T1073" id="Seg_13708" s="T928">conv:pred</ta>
            <ta e="T1071" id="Seg_13709" s="T1073">v:pred 0.3.h:S</ta>
            <ta e="T930" id="Seg_13710" s="T929">v:pred</ta>
            <ta e="T931" id="Seg_13711" s="T930">np.h:S</ta>
            <ta e="T934" id="Seg_13712" s="T933">np.h:S</ta>
            <ta e="T935" id="Seg_13713" s="T934">v:pred</ta>
            <ta e="T936" id="Seg_13714" s="T935">pro.h:O</ta>
            <ta e="T937" id="Seg_13715" s="T936">v:pred 0.3.h:S</ta>
            <ta e="T940" id="Seg_13716" s="T939">v:pred 0.3.h:S</ta>
            <ta e="T943" id="Seg_13717" s="T942">np.h:S</ta>
            <ta e="T944" id="Seg_13718" s="T943">pro.h:S</ta>
            <ta e="T945" id="Seg_13719" s="T944">v:pred</ta>
            <ta e="T946" id="Seg_13720" s="T945">np:O</ta>
            <ta e="T948" id="Seg_13721" s="T947">v:pred 0.3.h:S</ta>
            <ta e="T949" id="Seg_13722" s="T948">np:O</ta>
            <ta e="T950" id="Seg_13723" s="T949">v:pred 0.3.h:S</ta>
            <ta e="T951" id="Seg_13724" s="T950">np:O</ta>
            <ta e="T952" id="Seg_13725" s="T951">v:pred 0.3.h:S</ta>
            <ta e="T953" id="Seg_13726" s="T952">np:O</ta>
            <ta e="T954" id="Seg_13727" s="T953">v:pred 0.3.h:S</ta>
            <ta e="T955" id="Seg_13728" s="T954">v:pred 0.3.h:S</ta>
            <ta e="T957" id="Seg_13729" s="T956">np.h:S</ta>
            <ta e="T958" id="Seg_13730" s="T957">n:pred</ta>
            <ta e="T959" id="Seg_13731" s="T958">cop</ta>
            <ta e="T962" id="Seg_13732" s="T961">v:pred 0.3.h:S</ta>
            <ta e="T963" id="Seg_13733" s="T962">pro.h:S</ta>
            <ta e="T965" id="Seg_13734" s="T964">v:pred</ta>
            <ta e="T968" id="Seg_13735" s="T967">v:pred 0.3.h:S</ta>
            <ta e="T969" id="Seg_13736" s="T968">pro.h:S</ta>
            <ta e="T970" id="Seg_13737" s="T969">v:pred</ta>
            <ta e="T972" id="Seg_13738" s="T971">ptcl:pred</ta>
            <ta e="T973" id="Seg_13739" s="T972">v:pred 0.1.h:S</ta>
            <ta e="T976" id="Seg_13740" s="T975">np.h:S</ta>
            <ta e="T977" id="Seg_13741" s="T976">pro.h:S</ta>
            <ta e="T978" id="Seg_13742" s="T977">v:pred</ta>
            <ta e="T980" id="Seg_13743" s="T979">v:pred 0.1.h:S</ta>
            <ta e="T983" id="Seg_13744" s="T982">np.h:S</ta>
            <ta e="T984" id="Seg_13745" s="T983">pro.h:S</ta>
            <ta e="T985" id="Seg_13746" s="T984">v:pred</ta>
            <ta e="T987" id="Seg_13747" s="T986">v:pred 0.2.h:S</ta>
            <ta e="T988" id="Seg_13748" s="T987">v:pred 0.3.h:S</ta>
            <ta e="T989" id="Seg_13749" s="T988">np:O</ta>
            <ta e="T990" id="Seg_13750" s="T989">np:O</ta>
            <ta e="T991" id="Seg_13751" s="T990">v:pred 0.3.h:S</ta>
            <ta e="T992" id="Seg_13752" s="T991">np:O</ta>
            <ta e="T993" id="Seg_13753" s="T992">v:pred 0.3.h:S</ta>
            <ta e="T994" id="Seg_13754" s="T993">np:O</ta>
            <ta e="T995" id="Seg_13755" s="T994">v:pred 0.3.h:S</ta>
            <ta e="T996" id="Seg_13756" s="T995">0.3.h:S v:pred</ta>
            <ta e="T998" id="Seg_13757" s="T997">np.h:S</ta>
            <ta e="T999" id="Seg_13758" s="T998">cop</ta>
            <ta e="T1000" id="Seg_13759" s="T999">n:pred</ta>
            <ta e="T1001" id="Seg_13760" s="T1000">v:pred 0.3.h:S</ta>
            <ta e="T1002" id="Seg_13761" s="T1001">pro.h:S</ta>
            <ta e="T1004" id="Seg_13762" s="T1003">v:pred</ta>
            <ta e="T1006" id="Seg_13763" s="T1005">v:pred 0.3.h:S</ta>
            <ta e="T1008" id="Seg_13764" s="T1007">v:pred 0.2.h:S</ta>
            <ta e="T1011" id="Seg_13765" s="T1010">np:S</ta>
            <ta e="T1012" id="Seg_13766" s="T1011">v:pred</ta>
            <ta e="T1013" id="Seg_13767" s="T1012">pro.h:S</ta>
            <ta e="T1014" id="Seg_13768" s="T1013">v:pred</ta>
            <ta e="T1016" id="Seg_13769" s="T1015">v:pred 0.1.h:S</ta>
            <ta e="T1021" id="Seg_13770" s="T1020">np.h:S</ta>
            <ta e="T1022" id="Seg_13771" s="T1021">pro.h:S</ta>
            <ta e="T1023" id="Seg_13772" s="T1022">v:pred</ta>
            <ta e="T1025" id="Seg_13773" s="T1024">v:pred 0.2.h:S</ta>
            <ta e="T1029" id="Seg_13774" s="T1028">np:O</ta>
            <ta e="T1030" id="Seg_13775" s="T1029">v:pred 0.3.h:S</ta>
            <ta e="T1031" id="Seg_13776" s="T1030">np:O</ta>
            <ta e="T1032" id="Seg_13777" s="T1031">v:pred 0.3.h:S</ta>
            <ta e="T1033" id="Seg_13778" s="T1032">np:O</ta>
            <ta e="T1034" id="Seg_13779" s="T1033">v:pred 0.3.h:S</ta>
            <ta e="T1036" id="Seg_13780" s="T1035">np:O</ta>
            <ta e="T1037" id="Seg_13781" s="T1036">v:pred 0.3.h:S</ta>
            <ta e="T1038" id="Seg_13782" s="T1037">v:pred 0.3.h:S</ta>
            <ta e="T1039" id="Seg_13783" s="T1038">v:pred 0.3.h:S</ta>
            <ta e="T1040" id="Seg_13784" s="T1039">v:pred 0.3.h:S</ta>
            <ta e="T1041" id="Seg_13785" s="T1040">np:S</ta>
            <ta e="T1042" id="Seg_13786" s="T1041">v:pred</ta>
            <ta e="T1043" id="Seg_13787" s="T1042">pro.h:S</ta>
            <ta e="T1046" id="Seg_13788" s="T1045">pro.h:O</ta>
            <ta e="T1052" id="Seg_13789" s="T1051">pro.h:S</ta>
            <ta e="T1055" id="Seg_13790" s="T1054">v:pred</ta>
            <ta e="T1057" id="Seg_13791" s="T1056">np:S</ta>
            <ta e="T1059" id="Seg_13792" s="T1058">v:pred</ta>
            <ta e="T1061" id="Seg_13793" s="T1060">v:pred 0.3.h:S</ta>
            <ta e="T1062" id="Seg_13794" s="T1061">v:pred 0.3.h:S</ta>
            <ta e="T1063" id="Seg_13795" s="T1062">v:pred 0.3.h:S</ta>
            <ta e="T1064" id="Seg_13796" s="T1063">np.h:S</ta>
            <ta e="T1065" id="Seg_13797" s="T1064">v:pred</ta>
            <ta e="T1068" id="Seg_13798" s="T1067">np.h:S</ta>
            <ta e="T1069" id="Seg_13799" s="T1068">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST-PKZ" />
         <annotation name="BOR" tierref="BOR-PKZ">
            <ta e="T6" id="Seg_13800" s="T5">TURK:core</ta>
            <ta e="T10" id="Seg_13801" s="T9">TURK:core</ta>
            <ta e="T19" id="Seg_13802" s="T18">%TURK:core</ta>
            <ta e="T21" id="Seg_13803" s="T20">TURK:gram(INDEF)</ta>
            <ta e="T41" id="Seg_13804" s="T40">TURK:cult</ta>
            <ta e="T43" id="Seg_13805" s="T42">RUS:gram</ta>
            <ta e="T66" id="Seg_13806" s="T65">TURK:disc</ta>
            <ta e="T68" id="Seg_13807" s="T67">TURK:disc</ta>
            <ta e="T71" id="Seg_13808" s="T70">TURK:core</ta>
            <ta e="T80" id="Seg_13809" s="T79">%TURK:core</ta>
            <ta e="T88" id="Seg_13810" s="T87">RUS:gram</ta>
            <ta e="T101" id="Seg_13811" s="T100">%TURK:core</ta>
            <ta e="T105" id="Seg_13812" s="T104">%TURK:core</ta>
            <ta e="T106" id="Seg_13813" s="T105">TURK:disc</ta>
            <ta e="T115" id="Seg_13814" s="T114">RUS:core</ta>
            <ta e="T123" id="Seg_13815" s="T122">%TURK:core</ta>
            <ta e="T132" id="Seg_13816" s="T131">TURK:core</ta>
            <ta e="T134" id="Seg_13817" s="T133">TURK:disc</ta>
            <ta e="T139" id="Seg_13818" s="T138">RUS:gram</ta>
            <ta e="T142" id="Seg_13819" s="T141">RUS:gram</ta>
            <ta e="T144" id="Seg_13820" s="T143">TURK:disc</ta>
            <ta e="T158" id="Seg_13821" s="T1074">TAT:cult</ta>
            <ta e="T165" id="Seg_13822" s="T164">TURK:disc</ta>
            <ta e="T169" id="Seg_13823" s="T168">RUS:gram</ta>
            <ta e="T170" id="Seg_13824" s="T169">RUS:cult</ta>
            <ta e="T176" id="Seg_13825" s="T175">RUS:gram</ta>
            <ta e="T185" id="Seg_13826" s="T184">RUS:core</ta>
            <ta e="T189" id="Seg_13827" s="T188">RUS:gram</ta>
            <ta e="T190" id="Seg_13828" s="T189">RUS:cult</ta>
            <ta e="T194" id="Seg_13829" s="T193">RUS:gram</ta>
            <ta e="T195" id="Seg_13830" s="T194">TURK:disc</ta>
            <ta e="T202" id="Seg_13831" s="T201">RUS:mod</ta>
            <ta e="T204" id="Seg_13832" s="T203">%TURK:core</ta>
            <ta e="T205" id="Seg_13833" s="T204">RUS:gram</ta>
            <ta e="T211" id="Seg_13834" s="T210">RUS:mod</ta>
            <ta e="T218" id="Seg_13835" s="T217">RUS:cult</ta>
            <ta e="T222" id="Seg_13836" s="T221">RUS:gram</ta>
            <ta e="T228" id="Seg_13837" s="T227">TURK:disc</ta>
            <ta e="T230" id="Seg_13838" s="T229">RUS:gram</ta>
            <ta e="T234" id="Seg_13839" s="T233">RUS:mod</ta>
            <ta e="T239" id="Seg_13840" s="T238">RUS:gram</ta>
            <ta e="T245" id="Seg_13841" s="T244">RUS:mod</ta>
            <ta e="T252" id="Seg_13842" s="T251">RUS:gram</ta>
            <ta e="T254" id="Seg_13843" s="T253">TURK:gram(INDEF)</ta>
            <ta e="T268" id="Seg_13844" s="T267">RUS:gram</ta>
            <ta e="T278" id="Seg_13845" s="T277">TURK:core</ta>
            <ta e="T285" id="Seg_13846" s="T284">TURK:disc</ta>
            <ta e="T301" id="Seg_13847" s="T300">RUS:disc</ta>
            <ta e="T302" id="Seg_13848" s="T301">RUS:mod</ta>
            <ta e="T324" id="Seg_13849" s="T323">RUS:mod</ta>
            <ta e="T338" id="Seg_13850" s="T337">RUS:cult</ta>
            <ta e="T362" id="Seg_13851" s="T361">TURK:disc</ta>
            <ta e="T363" id="Seg_13852" s="T362">TAT:cult</ta>
            <ta e="T365" id="Seg_13853" s="T364">RUS:gram</ta>
            <ta e="T377" id="Seg_13854" s="T376">TURK:cult</ta>
            <ta e="T392" id="Seg_13855" s="T391">TURK:disc</ta>
            <ta e="T408" id="Seg_13856" s="T407">TURK:cult</ta>
            <ta e="T418" id="Seg_13857" s="T417">RUS:gram</ta>
            <ta e="T420" id="Seg_13858" s="T419">TURK:cult</ta>
            <ta e="T423" id="Seg_13859" s="T422">RUS:gram</ta>
            <ta e="T425" id="Seg_13860" s="T424">RUS:gram</ta>
            <ta e="T427" id="Seg_13861" s="T426">TURK:core</ta>
            <ta e="T437" id="Seg_13862" s="T436">TURK:cult</ta>
            <ta e="T443" id="Seg_13863" s="T442">TURK:core</ta>
            <ta e="T444" id="Seg_13864" s="T443">RUS:gram</ta>
            <ta e="T446" id="Seg_13865" s="T445">TURK:core</ta>
            <ta e="T453" id="Seg_13866" s="T452">TURK:disc</ta>
            <ta e="T468" id="Seg_13867" s="T467">TURK:disc</ta>
            <ta e="T473" id="Seg_13868" s="T472">TURK:cult</ta>
            <ta e="T474" id="Seg_13869" s="T473">RUS:gram</ta>
            <ta e="T475" id="Seg_13870" s="T474">TURK:cult</ta>
            <ta e="T477" id="Seg_13871" s="T476">TURK:disc</ta>
            <ta e="T479" id="Seg_13872" s="T478">TURK:disc</ta>
            <ta e="T483" id="Seg_13873" s="T482">TURK:disc</ta>
            <ta e="T492" id="Seg_13874" s="T491">TURK:disc</ta>
            <ta e="T500" id="Seg_13875" s="T499">TURK:cult</ta>
            <ta e="T505" id="Seg_13876" s="T504">RUS:gram</ta>
            <ta e="T512" id="Seg_13877" s="T511">RUS:gram</ta>
            <ta e="T514" id="Seg_13878" s="T513">RUS:cult</ta>
            <ta e="T517" id="Seg_13879" s="T516">RUS:cult</ta>
            <ta e="T521" id="Seg_13880" s="T520">TURK:cult</ta>
            <ta e="T522" id="Seg_13881" s="T521">TURK:disc</ta>
            <ta e="T531" id="Seg_13882" s="T530">TURK:disc</ta>
            <ta e="T535" id="Seg_13883" s="T534">RUS:gram</ta>
            <ta e="T539" id="Seg_13884" s="T538">TURK:disc</ta>
            <ta e="T545" id="Seg_13885" s="T544">TURK:cult</ta>
            <ta e="T553" id="Seg_13886" s="T552">TURK:cult</ta>
            <ta e="T560" id="Seg_13887" s="T559">TURK:gram(INDEF)</ta>
            <ta e="T564" id="Seg_13888" s="T563">RUS:mod</ta>
            <ta e="T574" id="Seg_13889" s="T572">RUS:gram</ta>
            <ta e="T583" id="Seg_13890" s="T582">RUS:gram</ta>
            <ta e="T597" id="Seg_13891" s="T596">RUS:gram</ta>
            <ta e="T600" id="Seg_13892" s="T599">TURK:disc</ta>
            <ta e="T610" id="Seg_13893" s="T609">TURK:gram(INDEF)</ta>
            <ta e="T617" id="Seg_13894" s="T616">RUS:gram</ta>
            <ta e="T622" id="Seg_13895" s="T621">RUS:gram</ta>
            <ta e="T626" id="Seg_13896" s="T624">TURK:disc</ta>
            <ta e="T652" id="Seg_13897" s="T651">TURK:disc</ta>
            <ta e="T674" id="Seg_13898" s="T673">TURK:gram(INDEF)</ta>
            <ta e="T677" id="Seg_13899" s="T676">RUS:gram</ta>
            <ta e="T684" id="Seg_13900" s="T683">TURK:gram(INDEF)</ta>
            <ta e="T686" id="Seg_13901" s="T685">RUS:mod</ta>
            <ta e="T688" id="Seg_13902" s="T687">TURK:cult</ta>
            <ta e="T694" id="Seg_13903" s="T693">TURK:cult</ta>
            <ta e="T699" id="Seg_13904" s="T698">RUS:gram</ta>
            <ta e="T708" id="Seg_13905" s="T707">RUS:mod</ta>
            <ta e="T720" id="Seg_13906" s="T719">TURK:disc</ta>
            <ta e="T723" id="Seg_13907" s="T722">TURK:disc</ta>
            <ta e="T724" id="Seg_13908" s="T723">TURK:gram(INDEF)</ta>
            <ta e="T730" id="Seg_13909" s="T729">TURK:cult</ta>
            <ta e="T744" id="Seg_13910" s="T743">TURK:gram(INDEF)</ta>
            <ta e="T756" id="Seg_13911" s="T755">RUS:gram</ta>
            <ta e="T760" id="Seg_13912" s="T759">RUS:gram</ta>
            <ta e="T783" id="Seg_13913" s="T782">RUS:disc</ta>
            <ta e="T787" id="Seg_13914" s="T786">RUS:core</ta>
            <ta e="T798" id="Seg_13915" s="T797">TURK:disc</ta>
            <ta e="T801" id="Seg_13916" s="T800">TURK:disc</ta>
            <ta e="T808" id="Seg_13917" s="T807">RUS:core</ta>
            <ta e="T818" id="Seg_13918" s="T817">RUS:gram</ta>
            <ta e="T825" id="Seg_13919" s="T823">TURK:disc</ta>
            <ta e="T830" id="Seg_13920" s="T828">TURK:disc</ta>
            <ta e="T837" id="Seg_13921" s="T836">RUS:gram</ta>
            <ta e="T843" id="Seg_13922" s="T842">RUS:mod</ta>
            <ta e="T847" id="Seg_13923" s="T846">RUS:disc</ta>
            <ta e="T864" id="Seg_13924" s="T863">TURK:cult</ta>
            <ta e="T871" id="Seg_13925" s="T870">RUS:mod</ta>
            <ta e="T874" id="Seg_13926" s="T873">RUS:gram</ta>
            <ta e="T875" id="Seg_13927" s="T874">TURK:disc</ta>
            <ta e="T882" id="Seg_13928" s="T881">RUS:gram</ta>
            <ta e="T883" id="Seg_13929" s="T882">TURK:disc</ta>
            <ta e="T886" id="Seg_13930" s="T885">RUS:gram</ta>
            <ta e="T887" id="Seg_13931" s="T886">TURK:cult</ta>
            <ta e="T890" id="Seg_13932" s="T889">RUS:gram</ta>
            <ta e="T892" id="Seg_13933" s="T890">TURK:cult</ta>
            <ta e="T895" id="Seg_13934" s="T894">TURK:cult</ta>
            <ta e="T902" id="Seg_13935" s="T901">TURK:cult</ta>
            <ta e="T907" id="Seg_13936" s="T906">RUS:gram</ta>
            <ta e="T916" id="Seg_13937" s="T915">RUS:gram</ta>
            <ta e="T921" id="Seg_13938" s="T920">RUS:gram</ta>
            <ta e="T924" id="Seg_13939" s="T923">TURK:gram(INDEF)</ta>
            <ta e="T928" id="Seg_13940" s="T927">TURK:disc</ta>
            <ta e="T938" id="Seg_13941" s="T1075">TAT:cult</ta>
            <ta e="T939" id="Seg_13942" s="T938">RUS:gram</ta>
            <ta e="T941" id="Seg_13943" s="T940">RUS:gram</ta>
            <ta e="T956" id="Seg_13944" s="T955">RUS:gram</ta>
            <ta e="T960" id="Seg_13945" s="T959">RUS:gram</ta>
            <ta e="T964" id="Seg_13946" s="T963">TURK:disc</ta>
            <ta e="T966" id="Seg_13947" s="T965">RUS:gram</ta>
            <ta e="T982" id="Seg_13948" s="T981">TURK:core</ta>
            <ta e="T986" id="Seg_13949" s="T985">RUS:disc</ta>
            <ta e="T1003" id="Seg_13950" s="T1002">TURK:disc</ta>
            <ta e="T1009" id="Seg_13951" s="T1008">RUS:gram</ta>
            <ta e="T1024" id="Seg_13952" s="T1023">RUS:disc</ta>
            <ta e="T1026" id="Seg_13953" s="T1025">RUS:mod</ta>
            <ta e="T1028" id="Seg_13954" s="T1027">RUS:disc</ta>
            <ta e="T1035" id="Seg_13955" s="T1034">RUS:gram</ta>
            <ta e="T1044" id="Seg_13956" s="T1043">RUS:core</ta>
            <ta e="T1045" id="Seg_13957" s="T1044">TURK:disc</ta>
            <ta e="T1051" id="Seg_13958" s="T1049">TURK:disc</ta>
            <ta e="T1053" id="Seg_13959" s="T1052">RUS:core</ta>
            <ta e="T1056" id="Seg_13960" s="T1055">RUS:gram</ta>
            <ta e="T1066" id="Seg_13961" s="T1065">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-PKZ" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-PKZ" />
         <annotation name="CS" tierref="CS-PKZ">
            <ta e="T56" id="Seg_13962" s="T55">RUS:int</ta>
            <ta e="T63" id="Seg_13963" s="T62">RUS:int</ta>
            <ta e="T85" id="Seg_13964" s="T84">RUS:int</ta>
            <ta e="T89" id="Seg_13965" s="T88">RUS:int</ta>
            <ta e="T188" id="Seg_13966" s="T187">RUS:int</ta>
            <ta e="T199" id="Seg_13967" s="T198">RUS:int</ta>
            <ta e="T235" id="Seg_13968" s="T234">RUS:int</ta>
            <ta e="T236" id="Seg_13969" s="T235">RUS:ext</ta>
            <ta e="T238" id="Seg_13970" s="T236">RUS:ext</ta>
            <ta e="T450" id="Seg_13971" s="T449">RUS:int</ta>
            <ta e="T653" id="Seg_13972" s="T652">RUS:int</ta>
            <ta e="T880" id="Seg_13973" s="T877">RUS:int</ta>
            <ta e="T974" id="Seg_13974" s="T973">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr-PKZ">
            <ta e="T4" id="Seg_13975" s="T0">Когда мы приехали в Абалаково,</ta>
            <ta e="T8" id="Seg_13976" s="T4">Люди к нам хорошо отнеслись.</ta>
            <ta e="T10" id="Seg_13977" s="T8">Мы хорошо переночевали.</ta>
            <ta e="T13" id="Seg_13978" s="T10">Потом мы ходили, ходили.</ta>
            <ta e="T18" id="Seg_13979" s="T13">Мы встретили каких-то мужчин.</ta>
            <ta e="T24" id="Seg_13980" s="T18">Мы поговорили, они ничего не знают, камасинцы.</ta>
            <ta e="T26" id="Seg_13981" s="T24">Камасинцы.</ta>
            <ta e="T29" id="Seg_13982" s="T26">Как они живут!</ta>
            <ta e="T34" id="Seg_13983" s="T29">Мы думали вернуться обратно домой.</ta>
            <ta e="T42" id="Seg_13984" s="T36">Одна девушка, Эля, пошла поискать молока.</ta>
            <ta e="T48" id="Seg_13985" s="T42">И она пришла к одной женщине.</ta>
            <ta e="T55" id="Seg_13986" s="T48">Эта женщина — камасинка, она говорит:</ta>
            <ta e="T57" id="Seg_13987" s="T55">Лицо — kadəl.</ta>
            <ta e="T64" id="Seg_13988" s="T57">А волосы — eʔbdə.</ta>
            <ta e="T67" id="Seg_13989" s="T64">Эта [девушка] вернулась.</ta>
            <ta e="T71" id="Seg_13990" s="T67">Ей на сердце очень хорошо [=она очень обрадовалась?]. </ta>
            <ta e="T77" id="Seg_13991" s="T71">Говорит: "Тут есть женщина.</ta>
            <ta e="T80" id="Seg_13992" s="T77">Она умеет говорить по-камасински.</ta>
            <ta e="T90" id="Seg_13993" s="T81">Она сказала мне: девушка — koʔbdo, парень — nʼi.</ta>
            <ta e="T92" id="Seg_13994" s="T90">Потом…</ta>
            <ta e="T96" id="Seg_13995" s="T93">Тогда мы [решили] остаться.</ta>
            <ta e="T101" id="Seg_13996" s="T96">Мы ходили разговаривать к этой женщине.</ta>
            <ta e="T105" id="Seg_13997" s="T101">Мы разговаривали тридцать дней.</ta>
            <ta e="T107" id="Seg_13998" s="T105">Обо всём.</ta>
            <ta e="T111" id="Seg_13999" s="T107">Она рассказывала на своем языке.</ta>
            <ta e="T116" id="Seg_14000" s="T111">Она так сказала: "Моя тетя умерла.</ta>
            <ta e="T120" id="Seg_14001" s="T116">Двадцать лет прошло.</ta>
            <ta e="T123" id="Seg_14002" s="T120">Я ни с кем не разговаривали.</ta>
            <ta e="T129" id="Seg_14003" s="T124">Ей сейчас семь[десят] лет.</ta>
            <ta e="T133" id="Seg_14004" s="T129">Она очень хорошая женщина.</ta>
            <ta e="T136" id="Seg_14005" s="T133">У нее все есть.</ta>
            <ta e="T145" id="Seg_14006" s="T136">Если она дает вам есть и вы не едите, она сердится.</ta>
            <ta e="T156" id="Seg_14007" s="T147">Она все время работает в доме, ее ногам не тяжело. [?]</ta>
            <ta e="T168" id="Seg_14008" s="T157">В Агинское, двадцать пять [километров], она идет пешком.</ta>
            <ta e="T175" id="Seg_14009" s="T168">А машины проезжают, она в них не садится.</ta>
            <ta e="T180" id="Seg_14010" s="T175">А идет пешком.</ta>
            <ta e="T186" id="Seg_14011" s="T182">"Я лучше пойду пешком.</ta>
            <ta e="T188" id="Seg_14012" s="T186">Дорога прямая.</ta>
            <ta e="T196" id="Seg_14013" s="T188">А машиной очень долго ехать, и трясет".</ta>
            <ta e="T200" id="Seg_14014" s="T197">Какая судьба у камасинцев.</ta>
            <ta e="T206" id="Seg_14015" s="T200">Кто-то еще может говорить, или нет?</ta>
            <ta e="T212" id="Seg_14016" s="T207">На (улице?), в Абалаково, можно найти детей.</ta>
            <ta e="T216" id="Seg_14017" s="T212">Голова белая, глаза черные.</ta>
            <ta e="T221" id="Seg_14018" s="T216">Не только русская кровь [у них].</ta>
            <ta e="T225" id="Seg_14019" s="T221">И камасинская кровь есть.</ta>
            <ta e="T230" id="Seg_14020" s="T226">Некоторые уже выросли.</ta>
            <ta e="T235" id="Seg_14021" s="T230">Пошли работать туда или сюда, вот Проня.</ta>
            <ta e="T236" id="Seg_14022" s="T235">Анджигатов.</ta>
            <ta e="T238" id="Seg_14023" s="T236">Председатель сельсовета.</ta>
            <ta e="T243" id="Seg_14024" s="T238">А свой язык не знает.</ta>
            <ta e="T251" id="Seg_14025" s="T244">Только одна женщина знает камасинский язык.</ta>
            <ta e="T258" id="Seg_14026" s="T251">А больше никто не знает этот язык.</ta>
            <ta e="T262" id="Seg_14027" s="T259">Ты мне соврал.</ta>
            <ta e="T265" id="Seg_14028" s="T262">Ты меня обманул.</ta>
            <ta e="T276" id="Seg_14029" s="T265">Я [=ты?] спряталась, и я искала и не могла найти.</ta>
            <ta e="T280" id="Seg_14030" s="T276">Жил-был плохой человек.</ta>
            <ta e="T283" id="Seg_14031" s="T280">Его звали Тарча-Барджа.</ta>
            <ta e="T287" id="Seg_14032" s="T283">Он купил кобылу.</ta>
            <ta e="T289" id="Seg_14033" s="T287">У нее болит нога.</ta>
            <ta e="T292" id="Seg_14034" s="T289">Она не может ходить.</ta>
            <ta e="T295" id="Seg_14035" s="T292">Потом он шел-шел.</ta>
            <ta e="T300" id="Seg_14036" s="T295">Медведь идет: "Возьми меня с собой!</ta>
            <ta e="T307" id="Seg_14037" s="T300">Только посади меня в черный мешок".</ta>
            <ta e="T312" id="Seg_14038" s="T307">Он его посадил, дальше идет.</ta>
            <ta e="T316" id="Seg_14039" s="T312">Идет, идет, потом…</ta>
            <ta e="T321" id="Seg_14040" s="T316">Большая собака [=волк] вышел из леса.</ta>
            <ta e="T323" id="Seg_14041" s="T321">"Возьми меня!</ta>
            <ta e="T328" id="Seg_14042" s="T323">Только посади меня в черный мешок".</ta>
            <ta e="T330" id="Seg_14043" s="T328">Он посадил.</ta>
            <ta e="T336" id="Seg_14044" s="T330">Пошел опять, идет, идет.</ta>
            <ta e="T339" id="Seg_14045" s="T336">Лиса идет.</ta>
            <ta e="T341" id="Seg_14046" s="T339">"Возьми меня!"</ta>
            <ta e="T344" id="Seg_14047" s="T341">Он ее взял.</ta>
            <ta e="T350" id="Seg_14048" s="T344">Посадил в черный мешок, потом дальше идет.</ta>
            <ta e="T354" id="Seg_14049" s="T350">Вода идет: "Возьми меня!"</ta>
            <ta e="T359" id="Seg_14050" s="T354">Он налил туда воду.</ta>
            <ta e="T360" id="Seg_14051" s="T359">Пришел.</ta>
            <ta e="T368" id="Seg_14052" s="T360">Там большие дома стоят, и очень большая вода [=море].</ta>
            <ta e="T371" id="Seg_14053" s="T368">Он привязил кобылу.</ta>
            <ta e="T374" id="Seg_14054" s="T371">Разложил огонь, сидит.</ta>
            <ta e="T380" id="Seg_14055" s="T374">Царь увидел его: "Кто это там сидит?</ta>
            <ta e="T384" id="Seg_14056" s="T380">Топчет мою траве.</ta>
            <ta e="T386" id="Seg_14057" s="T384">Его кобыла ест [траву].</ta>
            <ta e="T393" id="Seg_14058" s="T386">Там немного посидит, потом опять топчет.</ta>
            <ta e="T394" id="Seg_14059" s="T393">Иди!"</ta>
            <ta e="T396" id="Seg_14060" s="T394">Сына своего послал.</ta>
            <ta e="T399" id="Seg_14061" s="T396">"Что ему надо?"</ta>
            <ta e="T406" id="Seg_14062" s="T399">Тот пришел, спрашивает: "Что тебе надо?"</ta>
            <ta e="T410" id="Seg_14063" s="T406">"Я женюсь на дочери этого царя".</ta>
            <ta e="T415" id="Seg_14064" s="T411">Мальчик вернулся назад и говорит:</ta>
            <ta e="T422" id="Seg_14065" s="T415">"Он говорит: я женюсь на дочери царя.</ta>
            <ta e="T424" id="Seg_14066" s="T422">Пусть они уйдут.</ta>
            <ta e="T428" id="Seg_14067" s="T424">А то будет плохо!"</ta>
            <ta e="T431" id="Seg_14068" s="T428">"Не отдам!"</ta>
            <ta e="T436" id="Seg_14069" s="T431">Он =[мальчик] пошел, сказал так:</ta>
            <ta e="T440" id="Seg_14070" s="T436">"Царь не отдает свою дочь".</ta>
            <ta e="T447" id="Seg_14071" s="T440">"Если не отдает по-хорошему, так отдаст по плохому".</ta>
            <ta e="T451" id="Seg_14072" s="T448">"Выпустите диких лошадей.</ta>
            <ta e="T458" id="Seg_14073" s="T451">Они разорвут его кобылу, тогда он уйдет".</ta>
            <ta e="T464" id="Seg_14074" s="T458">Тогда он выпустил медведя из черного мешка.</ta>
            <ta e="T469" id="Seg_14075" s="T464">Медведь прогнал лошадей.</ta>
            <ta e="T475" id="Seg_14076" s="T469">Тогда [царь] говорит: "Выпустите коров, пусть коровы</ta>
            <ta e="T480" id="Seg_14077" s="T475">рогами убьют его лошадь".</ta>
            <ta e="T493" id="Seg_14078" s="T480">Тот выпустил волка из мешка, [волк] побежал, прогнал их.</ta>
            <ta e="T497" id="Seg_14079" s="T493">Тогда он говорит: "Выпустите…"</ta>
            <ta e="T504" id="Seg_14080" s="T498">Тогда царь: "Выпустите собак!</ta>
            <ta e="T508" id="Seg_14081" s="T504">Пусть собаки его разорвут.</ta>
            <ta e="T511" id="Seg_14082" s="T508">Он выпустил собак.</ta>
            <ta e="T518" id="Seg_14083" s="T511">А тот выпустил лису, собаки убежали [от] лисы.</ta>
            <ta e="T527" id="Seg_14084" s="T518">Тогда царь собрал людей: "Пойдем, возьмем его!"</ta>
            <ta e="T534" id="Seg_14085" s="T527">Люди пришли, он вылил воду [из] мешка.</ta>
            <ta e="T541" id="Seg_14086" s="T534">Люди убежали от воды.</ta>
            <ta e="T546" id="Seg_14087" s="T542">Тогда царь закричал:</ta>
            <ta e="T550" id="Seg_14088" s="T546">"Половину от того, что у меня есть, - сказал он.</ta>
            <ta e="T554" id="Seg_14089" s="T550">У меня есть коровы, лошади.</ta>
            <ta e="T559" id="Seg_14090" s="T554">Собаки, много денег".</ta>
            <ta e="T566" id="Seg_14091" s="T559">""Ничего мне не нужно, только дочку взять".</ta>
            <ta e="T574" id="Seg_14092" s="T567">Потом он забрал воду в мешок и…</ta>
            <ta e="T585" id="Seg_14093" s="T575">Тогда он отдал свою дочь, тот посадил [ее] на кобылу и поехал домой.</ta>
            <ta e="T589" id="Seg_14094" s="T585">Жили два брата.</ta>
            <ta e="T592" id="Seg_14095" s="T589">У них быи жены.</ta>
            <ta e="T596" id="Seg_14096" s="T592">Они собрались, пошли в лес.</ta>
            <ta e="T600" id="Seg_14097" s="T596">А женщины остались дома.</ta>
            <ta e="T605" id="Seg_14098" s="T600">Вечером они сели есть.</ta>
            <ta e="T612" id="Seg_14099" s="T605">Они увидели: в котле сидит какой-то человек.</ta>
            <ta e="T621" id="Seg_14100" s="T612">Они собрали гнезда [=ветки] и положили их в котел [=в очаг].</ta>
            <ta e="T629" id="Seg_14101" s="T621">Но эти ветки были мокрые.</ta>
            <ta e="T632" id="Seg_14102" s="T629">Потом он пришел.</ta>
            <ta e="T637" id="Seg_14103" s="T632">Этот человек не мог разжечь огонь.</ta>
            <ta e="T640" id="Seg_14104" s="T637">Он убил одну женщину.</ta>
            <ta e="T650" id="Seg_14105" s="T640">Он опять сказал женщине: "Что ты сидишь, разожги огонь!"</ta>
            <ta e="T654" id="Seg_14106" s="T650">Она начала разжигать огонь.</ta>
            <ta e="T659" id="Seg_14107" s="T654">Он разрубил ей голову топором.</ta>
            <ta e="T662" id="Seg_14108" s="T659">Потом он убил детей.</ta>
            <ta e="T669" id="Seg_14109" s="T662">Потом те (мужчины?) вернулись домой.</ta>
            <ta e="T673" id="Seg_14110" s="T669">Их жен нет.</ta>
            <ta e="T678" id="Seg_14111" s="T673">Кто-то убил детей и жен.</ta>
            <ta e="T682" id="Seg_14112" s="T678">Жили мужчина с женщиной.</ta>
            <ta e="T688" id="Seg_14113" s="T682">У них ничего не было, только одна корова.</ta>
            <ta e="T692" id="Seg_14114" s="T688">Им было нечего есть.</ta>
            <ta e="T695" id="Seg_14115" s="T692">Тогда [мужчина] зарезал корову.</ta>
            <ta e="T703" id="Seg_14116" s="T695">Он выгнал жену: "Иди, а то мне самому будет мало".</ta>
            <ta e="T707" id="Seg_14117" s="T703">Женщина шла, шла.</ta>
            <ta e="T710" id="Seg_14118" s="T707">Нашла другой чум.</ta>
            <ta e="T712" id="Seg_14119" s="T710">Зашла в чум.</ta>
            <ta e="T717" id="Seg_14120" s="T712">Там жир лежит, мясо лежит.</ta>
            <ta e="T721" id="Seg_14121" s="T717">Она сварила, поела.</ta>
            <ta e="T725" id="Seg_14122" s="T721">Слышит: кто-то идет.</ta>
            <ta e="T731" id="Seg_14123" s="T725">[Этот человек] привел много овец, коров, лошадей.</ta>
            <ta e="T734" id="Seg_14124" s="T731">Потом он зашел в чум.</ta>
            <ta e="T738" id="Seg_14125" s="T734">Говорит: "Дверь, откройся!"</ta>
            <ta e="T743" id="Seg_14126" s="T738">Дверь открылась, [женщина] вошла, смотрит:</ta>
            <ta e="T751" id="Seg_14127" s="T743">"Кто-то был [здесь], жира нет, мяса нет, хлеба нет.</ta>
            <ta e="T754" id="Seg_14128" s="T751">Кто тут был?</ta>
            <ta e="T758" id="Seg_14129" s="T754">Если мужчина - будет моим мужем.</ta>
            <ta e="T762" id="Seg_14130" s="T758">Если женщина - будет моей подругой".</ta>
            <ta e="T767" id="Seg_14131" s="T762">Тогда эта женщина встала, вышла.</ta>
            <ta e="T770" id="Seg_14132" s="T767">Та ее усадила.</ta>
            <ta e="T772" id="Seg_14133" s="T770">Они сели есть.</ta>
            <ta e="T782" id="Seg_14134" s="T772">Она спрашивает: "Почему у тебя нет ни рук, ни ног, как ты ходишь?"</ta>
            <ta e="T785" id="Seg_14135" s="T782">"Да так хожу".</ta>
            <ta e="T788" id="Seg_14136" s="T785">"Ты прыгнешь в яму?"</ta>
            <ta e="T789" id="Seg_14137" s="T788">"Прыгну".</ta>
            <ta e="T791" id="Seg_14138" s="T789">Потом они пошли.</ta>
            <ta e="T799" id="Seg_14139" s="T791">Та прыгнула, а она отрубила ей голову топором.</ta>
            <ta e="T803" id="Seg_14140" s="T799">Взяла мозг. [?]</ta>
            <ta e="T807" id="Seg_14141" s="T803">Взяла жир, взяла мясо.</ta>
            <ta e="T813" id="Seg_14142" s="T807">Положила в пузырь, потом вернулась к своему мужу.</ta>
            <ta e="T825" id="Seg_14143" s="T813">Она залезла на чум, а муж на руке углем…</ta>
            <ta e="T830" id="Seg_14144" s="T825">…руку углем…</ta>
            <ta e="T836" id="Seg_14145" s="T830">"Это мясо я съем [сегодня], это мясо я съем [завтра]".</ta>
            <ta e="T840" id="Seg_14146" s="T836">Она бросила ему [мясо].</ta>
            <ta e="T846" id="Seg_14147" s="T840">Он схватил [его]: "Вот что мне Бог дал!"</ta>
            <ta e="T850" id="Seg_14148" s="T846">"Да, Бог тебе даст!</ta>
            <ta e="T857" id="Seg_14149" s="T850">Бог тебе камень даст, это я [тебе] дала!"</ta>
            <ta e="T861" id="Seg_14150" s="T857">Потом он поел, они пошли [к тому чуму].</ta>
            <ta e="T867" id="Seg_14151" s="T861">Там лошади, коровы, овцы, они пришли туда.</ta>
            <ta e="T871" id="Seg_14152" s="T867">Он говорит: "Хочется пукнуть".</ta>
            <ta e="T876" id="Seg_14153" s="T871">"Не пукай, а то они все убегут!"</ta>
            <ta e="T888" id="Seg_14154" s="T876">Он все-таки пукнул, и все лошади, овцы и коровы убежали.</ta>
            <ta e="T892" id="Seg_14155" s="T888">Она стала молоком…</ta>
            <ta e="T897" id="Seg_14156" s="T893">молоко на них лить.</ta>
            <ta e="T901" id="Seg_14157" s="T897">"Овцы, вы станьте козами!</ta>
            <ta e="T906" id="Seg_14158" s="T901">Коровы, вы станьте лосями!</ta>
            <ta e="T910" id="Seg_14159" s="T906">А вы, лошади…"</ta>
            <ta e="T911" id="Seg_14160" s="T910">Я забыла.</ta>
            <ta e="T918" id="Seg_14161" s="T912">"Лошади, говорит, пусть станут оленями".</ta>
            <ta e="T927" id="Seg_14162" s="T918">Тогда женщина и мужчина ушли, у них опять ничего нет.</ta>
            <ta e="T1071" id="Seg_14163" s="T927">Они ушли.</ta>
            <ta e="T935" id="Seg_14164" s="T1071">Жил человек, у него были три дочери.</ta>
            <ta e="T940" id="Seg_14165" s="T935">Его вызвали в Русскую Деревню.</ta>
            <ta e="T945" id="Seg_14166" s="T940">Старшая дочь [говорит]: "Я пойду!"</ta>
            <ta e="T948" id="Seg_14167" s="T945">Она отрезала свои волосы.</ta>
            <ta e="T950" id="Seg_14168" s="T948">Надела штаны.</ta>
            <ta e="T952" id="Seg_14169" s="T950">Надела рубаху.</ta>
            <ta e="T955" id="Seg_14170" s="T952">Надела шапку, пошла.</ta>
            <ta e="T959" id="Seg_14171" s="T955">А ее отец сделался медведем.</ta>
            <ta e="T962" id="Seg_14172" s="T959">И идет к ней.</ta>
            <ta e="T968" id="Seg_14173" s="T962">Она испугалась и побежала домой.</ta>
            <ta e="T973" id="Seg_14174" s="T968">Он пришел: "Почему ты не пошла?"</ta>
            <ta e="T980" id="Seg_14175" s="T973">"Там медведь, я испугалась и пришла домой".</ta>
            <ta e="T985" id="Seg_14176" s="T980">Потом другая дочь [говорит]: "Я пойду!"</ta>
            <ta e="T987" id="Seg_14177" s="T985">"Ну иди!"</ta>
            <ta e="T991" id="Seg_14178" s="T987">Она надела штаны, надела рубаху.</ta>
            <ta e="T993" id="Seg_14179" s="T991">Отрезала себе волосы.</ta>
            <ta e="T996" id="Seg_14180" s="T993">Надела шапку и пошла.</ta>
            <ta e="T1001" id="Seg_14181" s="T996">Тогда отец превратился в медведя, пошел.</ta>
            <ta e="T1006" id="Seg_14182" s="T1001">Она испугалась, вернулась домой.</ta>
            <ta e="T1008" id="Seg_14183" s="T1006">"Почему ты пришла?"</ta>
            <ta e="T1016" id="Seg_14184" s="T1008">"Да там был медведь, я испугалась, побежала домой".</ta>
            <ta e="T1017" id="Seg_14185" s="T1016">Хватит!</ta>
            <ta e="T1023" id="Seg_14186" s="T1018">Потом младшая дочь [говорит]: "Я пойду!"</ta>
            <ta e="T1025" id="Seg_14187" s="T1023">"Ну иди!"</ta>
            <ta e="T1030" id="Seg_14188" s="T1025">Тоже так же отрезала волосы.</ta>
            <ta e="T1032" id="Seg_14189" s="T1030">Надела штаны.</ta>
            <ta e="T1034" id="Seg_14190" s="T1032">Надела рубаху.</ta>
            <ta e="T1038" id="Seg_14191" s="T1034">Надела шапку и пошла.</ta>
            <ta e="T1040" id="Seg_14192" s="T1038">Идет-идет.</ta>
            <ta e="T1042" id="Seg_14193" s="T1040">Медведь идет.</ta>
            <ta e="T1051" id="Seg_14194" s="T1042">Она его стрелой в глаз…</ta>
            <ta e="T1059" id="Seg_14195" s="T1051">Она ему стрелой в глаз попала, глаза нет.</ta>
            <ta e="T1062" id="Seg_14196" s="T1059">Потом она идет-идет.</ta>
            <ta e="T1069" id="Seg_14197" s="T1062">Идет, [там] живут женщина и ее сын.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-PKZ">
            <ta e="T4" id="Seg_14198" s="T0">When we came to Abalakovo,</ta>
            <ta e="T8" id="Seg_14199" s="T4">People were good to us.</ta>
            <ta e="T10" id="Seg_14200" s="T8">We passed a good night.</ta>
            <ta e="T13" id="Seg_14201" s="T10">Then we walked for some time.</ta>
            <ta e="T18" id="Seg_14202" s="T13">We met some men.</ta>
            <ta e="T24" id="Seg_14203" s="T18">We spoke, they don't know anything, Kamassians.</ta>
            <ta e="T26" id="Seg_14204" s="T24">Kamassian people.</ta>
            <ta e="T29" id="Seg_14205" s="T26">How do they live?</ta>
            <ta e="T34" id="Seg_14206" s="T29">We were about to go back home.</ta>
            <ta e="T42" id="Seg_14207" s="T36">One girl, Elya, went to looking for some milk.</ta>
            <ta e="T48" id="Seg_14208" s="T42">And she came to a woman.</ta>
            <ta e="T55" id="Seg_14209" s="T48">This woman is Kamassian, she says:</ta>
            <ta e="T57" id="Seg_14210" s="T55">Face is kadəl.</ta>
            <ta e="T64" id="Seg_14211" s="T57">Hair is eʔbdə.</ta>
            <ta e="T67" id="Seg_14212" s="T64">This [girl] came back.</ta>
            <ta e="T71" id="Seg_14213" s="T67">Her heart is very good [=she was very glad?].</ta>
            <ta e="T77" id="Seg_14214" s="T71">She says: "Here is a woman.</ta>
            <ta e="T80" id="Seg_14215" s="T77">She can speak Kamassian.</ta>
            <ta e="T90" id="Seg_14216" s="T81">She had said me: girl is koʔbdo, and boy is nʼi.</ta>
            <ta e="T92" id="Seg_14217" s="T90">Then…</ta>
            <ta e="T96" id="Seg_14218" s="T93">Then we [decided] to stay.</ta>
            <ta e="T101" id="Seg_14219" s="T96">We went to this woman to speak [with her].</ta>
            <ta e="T105" id="Seg_14220" s="T101">We spoke for thirty days.</ta>
            <ta e="T107" id="Seg_14221" s="T105">[About] all sorts of things.</ta>
            <ta e="T111" id="Seg_14222" s="T107">She was telling in her language.</ta>
            <ta e="T116" id="Seg_14223" s="T111">She said so: "My aunt died.</ta>
            <ta e="T120" id="Seg_14224" s="T116">Twenty years passed.</ta>
            <ta e="T123" id="Seg_14225" s="T120">I don't speak with anybody.</ta>
            <ta e="T129" id="Seg_14226" s="T124">She is now seven[ty] years old.</ta>
            <ta e="T133" id="Seg_14227" s="T129">She is a very good woman.</ta>
            <ta e="T136" id="Seg_14228" s="T133">She has all sorts of things.</ta>
            <ta e="T145" id="Seg_14229" s="T136">When she gives to eat and you don't eat, she's angry.</ta>
            <ta e="T156" id="Seg_14230" s="T147">Then she always works a lot at home, [how] this is not [too] hard for her legs. [?]</ta>
            <ta e="T168" id="Seg_14231" s="T157">To Aginskoye, twenty-five [kilometers], she goes [there] by foot.</ta>
            <ta e="T175" id="Seg_14232" s="T168">Cars pass by, she doesn't get in.</ta>
            <ta e="T180" id="Seg_14233" s="T175">She'll go by foot.</ta>
            <ta e="T186" id="Seg_14234" s="T182">"I'd rather go by foot.</ta>
            <ta e="T188" id="Seg_14235" s="T186">The road is straight.</ta>
            <ta e="T196" id="Seg_14236" s="T188">BTo go by car is very long, and it jolts."</ta>
            <ta e="T200" id="Seg_14237" s="T197">What for a lot do they have, Kamassians.</ta>
            <ta e="T206" id="Seg_14238" s="T200">Can someone still speak, or not?.</ta>
            <ta e="T212" id="Seg_14239" s="T207">Outside(?), in Abalakovo, you can find chldren.</ta>
            <ta e="T216" id="Seg_14240" s="T212">The head is white, the eyes are black.</ta>
            <ta e="T221" id="Seg_14241" s="T216">[They] have not only Russian blood.</ta>
            <ta e="T225" id="Seg_14242" s="T221">They have Kamassian blood, too.</ta>
            <ta e="T230" id="Seg_14243" s="T226">Some are grown up.</ta>
            <ta e="T235" id="Seg_14244" s="T230">They went to work, here and there, for example Pronya.</ta>
            <ta e="T236" id="Seg_14245" s="T235">Andzhigatov.</ta>
            <ta e="T238" id="Seg_14246" s="T236">Chair of the village council.</ta>
            <ta e="T243" id="Seg_14247" s="T238">And he doesn't know his own language.</ta>
            <ta e="T251" id="Seg_14248" s="T244">Only one woman knows this Kamassian language.</ta>
            <ta e="T258" id="Seg_14249" s="T251">And anybody more doesn't know this language.</ta>
            <ta e="T262" id="Seg_14250" s="T259">You lied to me.</ta>
            <ta e="T265" id="Seg_14251" s="T262">You deceived me.</ta>
            <ta e="T276" id="Seg_14252" s="T265">Then I [= you?] hid myself, and I was looking for (you?) and could't find. </ta>
            <ta e="T280" id="Seg_14253" s="T276">A bad ("not good") man lived.</ta>
            <ta e="T283" id="Seg_14254" s="T280">He was called Tarchabardzha.</ta>
            <ta e="T287" id="Seg_14255" s="T283">He caught a mare.</ta>
            <ta e="T289" id="Seg_14256" s="T287">Its foot hurts.</ta>
            <ta e="T292" id="Seg_14257" s="T289">It cannot walk.</ta>
            <ta e="T295" id="Seg_14258" s="T292">Then he went, went.</ta>
            <ta e="T300" id="Seg_14259" s="T295">A bear comes: "Take me with you!</ta>
            <ta e="T307" id="Seg_14260" s="T300">Only place me into the black bag."</ta>
            <ta e="T312" id="Seg_14261" s="T307">He seated it and goes further.</ta>
            <ta e="T316" id="Seg_14262" s="T312">He went, he went, then…</ta>
            <ta e="T321" id="Seg_14263" s="T316">Then a big dog [=wolf] came out from the forest.</ta>
            <ta e="T323" id="Seg_14264" s="T321">"Take me!</ta>
            <ta e="T328" id="Seg_14265" s="T323">Just get me into the black sack."</ta>
            <ta e="T330" id="Seg_14266" s="T328">He seated [it there].</ta>
            <ta e="T336" id="Seg_14267" s="T330">He went again, he goes and goes.</ta>
            <ta e="T339" id="Seg_14268" s="T336">A fox is coming.</ta>
            <ta e="T341" id="Seg_14269" s="T339">"Take me!"</ta>
            <ta e="T344" id="Seg_14270" s="T341">He took it.</ta>
            <ta e="T350" id="Seg_14271" s="T344">(He) sat (it) into the black sack, then he went again.</ta>
            <ta e="T354" id="Seg_14272" s="T350">Water comes: "Take me!"</ta>
            <ta e="T359" id="Seg_14273" s="T354">Then he poured the water there.</ta>
            <ta e="T360" id="Seg_14274" s="T359">He came.</ta>
            <ta e="T368" id="Seg_14275" s="T360">Houses are standing there, and a very big water [= a sea].</ta>
            <ta e="T371" id="Seg_14276" s="T368">He attached the mare.</ta>
            <ta e="T374" id="Seg_14277" s="T371">He set up fire, is sitting.</ta>
            <ta e="T380" id="Seg_14278" s="T374">The ruler saw him: "Who is sitting there?</ta>
            <ta e="T384" id="Seg_14279" s="T380">Treading on my grass.</ta>
            <ta e="T386" id="Seg_14280" s="T384">His mare is eating [the grass].</ta>
            <ta e="T393" id="Seg_14281" s="T386">He will sit there for a little while, then he will tread again.</ta>
            <ta e="T394" id="Seg_14282" s="T393">Go!"</ta>
            <ta e="T396" id="Seg_14283" s="T394">He sent his boy/son.</ta>
            <ta e="T399" id="Seg_14284" s="T396">"What does he want?"</ta>
            <ta e="T406" id="Seg_14285" s="T399">He came, asks: "What do you want?"</ta>
            <ta e="T410" id="Seg_14286" s="T406">"I will take [=marry] this ruler’s daughter."</ta>
            <ta e="T415" id="Seg_14287" s="T411">This boy came back and says:</ta>
            <ta e="T422" id="Seg_14288" s="T415">"He says: I will take the ruler’s daughter.</ta>
            <ta e="T424" id="Seg_14289" s="T422">Let them go.</ta>
            <ta e="T428" id="Seg_14290" s="T424">Otherwise it will be no good!"</ta>
            <ta e="T431" id="Seg_14291" s="T428">"I won't give [her]!"</ta>
            <ta e="T436" id="Seg_14292" s="T431">He [=the boy] came, said so:</ta>
            <ta e="T440" id="Seg_14293" s="T436">"The ruler does not give his daughter."</ta>
            <ta e="T447" id="Seg_14294" s="T440">"If he doesn’t give her for good, he'll give her for bad."</ta>
            <ta e="T451" id="Seg_14295" s="T448">"Release the wild horses.</ta>
            <ta e="T458" id="Seg_14296" s="T451">They will tear his mare to pieces, then he'll leave."</ta>
            <ta e="T464" id="Seg_14297" s="T458">Then he released the bear from the black sack.</ta>
            <ta e="T469" id="Seg_14298" s="T464">This bear chased away the horses.</ta>
            <ta e="T475" id="Seg_14299" s="T469">Then [the ruler] said: "Release the cows, let the cows.</ta>
            <ta e="T480" id="Seg_14300" s="T475">They will kill his horse with their horns."</ta>
            <ta e="T493" id="Seg_14301" s="T480">He released the big dog [= wolf] from the sack, it went, chased them away.</ta>
            <ta e="T497" id="Seg_14302" s="T493">Then he says: "Release…"</ta>
            <ta e="T504" id="Seg_14303" s="T498">Then the chief: "Release the dogs!</ta>
            <ta e="T508" id="Seg_14304" s="T504">Let the dogs tear it apart.</ta>
            <ta e="T511" id="Seg_14305" s="T508">He released their dogs.</ta>
            <ta e="T518" id="Seg_14306" s="T511">But he release the fox, the dogs ran [from] the fox.</ta>
            <ta e="T527" id="Seg_14307" s="T518">Then the ruler gathered people: "Let’s go, let’s take him!"</ta>
            <ta e="T534" id="Seg_14308" s="T527">People came, he poured the water [from] his bag.</ta>
            <ta e="T541" id="Seg_14309" s="T534">People ran away from the water.</ta>
            <ta e="T546" id="Seg_14310" s="T542">Then this ruler is shouting:</ta>
            <ta e="T550" id="Seg_14311" s="T546">"Half of what I have, he said.</ta>
            <ta e="T554" id="Seg_14312" s="T550">I have cows, horses, </ta>
            <ta e="T559" id="Seg_14313" s="T554">Dogs, a lot of money."</ta>
            <ta e="T566" id="Seg_14314" s="T559">[He said:] I don't need anything, just to take the daughter."</ta>
            <ta e="T574" id="Seg_14315" s="T567">Then he took the water into the black bag and…</ta>
            <ta e="T585" id="Seg_14316" s="T575">Then he gave his daughter, he sat [her] on the mare and went home.</ta>
            <ta e="T589" id="Seg_14317" s="T585">There lived two brothers.</ta>
            <ta e="T592" id="Seg_14318" s="T589">They had wives.</ta>
            <ta e="T596" id="Seg_14319" s="T592">They gathered [= prepared themselves], went into taiga.</ta>
            <ta e="T600" id="Seg_14320" s="T596">And the wives (were) [in] the tent / at home.</ta>
            <ta e="T605" id="Seg_14321" s="T600">In the evening they sat down to eat.</ta>
            <ta e="T612" id="Seg_14322" s="T605">Then they saw in the kettle, that a man is sitting.</ta>
            <ta e="T621" id="Seg_14323" s="T612">They gathered nests [=branches] and put them in the kettle [= in the fireplace].</ta>
            <ta e="T629" id="Seg_14324" s="T621">But these branch[es] were wet.</ta>
            <ta e="T632" id="Seg_14325" s="T629">Then he came.</ta>
            <ta e="T637" id="Seg_14326" s="T632">This man could not make fire.</ta>
            <ta e="T640" id="Seg_14327" s="T637">He killed one woman.</ta>
            <ta e="T650" id="Seg_14328" s="T640">Then again he says to the woman: "Why are you sitting, make fire!"</ta>
            <ta e="T654" id="Seg_14329" s="T650">She started lighting the fire.</ta>
            <ta e="T659" id="Seg_14330" s="T654">He cut off her head with an axe.</ta>
            <ta e="T662" id="Seg_14331" s="T659">Then he killed her children.</ta>
            <ta e="T669" id="Seg_14332" s="T662">Then those (men?) came back home.</ta>
            <ta e="T673" id="Seg_14333" s="T669">Their wives are not there.</ta>
            <ta e="T678" id="Seg_14334" s="T673">Someone killed their wives and children.</ta>
            <ta e="T682" id="Seg_14335" s="T678">[There] lived a man with his wife.</ta>
            <ta e="T688" id="Seg_14336" s="T682">They had nothing, only one cow.</ta>
            <ta e="T692" id="Seg_14337" s="T688">They had nothing to eat.</ta>
            <ta e="T695" id="Seg_14338" s="T692">Then [the man] killed his cow.</ta>
            <ta e="T703" id="Seg_14339" s="T695">He chased away the wife: "Go away, or I will have too little for myself."</ta>
            <ta e="T707" id="Seg_14340" s="T703">The woman went, went.</ta>
            <ta e="T710" id="Seg_14341" s="T707">She saw another tent.</ta>
            <ta e="T712" id="Seg_14342" s="T710">She came into the tent.</ta>
            <ta e="T717" id="Seg_14343" s="T712">There is fat lying, meat lying.</ta>
            <ta e="T721" id="Seg_14344" s="T717">She boiled, ate [it].</ta>
            <ta e="T725" id="Seg_14345" s="T721">She hears: someone is coming.</ta>
            <ta e="T731" id="Seg_14346" s="T725">[That person] brought many sheep, cows, horses.</ta>
            <ta e="T734" id="Seg_14347" s="T731">Then he came into the tent.</ta>
            <ta e="T738" id="Seg_14348" s="T734">He says: "Door, open up!"</ta>
            <ta e="T743" id="Seg_14349" s="T738">The door opened up, [she] came in, is looking:</ta>
            <ta e="T751" id="Seg_14350" s="T743">"Someone was [here], there is no fat, there is no meat, there is no bread.</ta>
            <ta e="T754" id="Seg_14351" s="T751">Who was here?</ta>
            <ta e="T758" id="Seg_14352" s="T754">If [this is] a man - he will be my husband.</ta>
            <ta e="T762" id="Seg_14353" s="T758">If [this is] a woman, she will be my companion."</ta>
            <ta e="T767" id="Seg_14354" s="T762">Then the woman got up, came (out).</ta>
            <ta e="T770" id="Seg_14355" s="T767">She seated her down.</ta>
            <ta e="T772" id="Seg_14356" s="T770">They sat eating.</ta>
            <ta e="T782" id="Seg_14357" s="T772">She asks: "Why don't you have hand, and feet, how do you walk?"</ta>
            <ta e="T785" id="Seg_14358" s="T782">"I go like this."</ta>
            <ta e="T788" id="Seg_14359" s="T785">"Will you jump into a pit?"</ta>
            <ta e="T789" id="Seg_14360" s="T788">"I will jump."</ta>
            <ta e="T791" id="Seg_14361" s="T789">Then they went.</ta>
            <ta e="T799" id="Seg_14362" s="T791">She jumped, she chopped off her head with an axe.</ta>
            <ta e="T803" id="Seg_14363" s="T799">Then she took the brain. [?]</ta>
            <ta e="T807" id="Seg_14364" s="T803">She took the fat, the meat.</ta>
            <ta e="T813" id="Seg_14365" s="T807">Puts [them] into the bladder, then she returned to her husband.</ta>
            <ta e="T825" id="Seg_14366" s="T813">She climbed on the tent, and her husband, with coal, his hand…</ta>
            <ta e="T830" id="Seg_14367" s="T825">…his hand, with coal:</ta>
            <ta e="T836" id="Seg_14368" s="T830">"I will eat that meat [today], I will eat that meat [tomorrow]."</ta>
            <ta e="T840" id="Seg_14369" s="T836">She threw [some meat] to him.</ta>
            <ta e="T846" id="Seg_14370" s="T840">He grabbed [it]: 'Look, what God gave me!"</ta>
            <ta e="T850" id="Seg_14371" s="T846">"Of course, God will give you!</ta>
            <ta e="T857" id="Seg_14372" s="T850">God will give you a stone, it's me that gave this [to you]!"</ta>
            <ta e="T861" id="Seg_14373" s="T857">Then he ate, they went [to that tent].</ta>
            <ta e="T867" id="Seg_14374" s="T861">There (were) horses, cows, sheep, they came there.</ta>
            <ta e="T871" id="Seg_14375" s="T867">He says: "I want to fart."</ta>
            <ta e="T876" id="Seg_14376" s="T871">"Don't fart, or they'll all run away!"</ta>
            <ta e="T888" id="Seg_14377" s="T876">He still farted, and all the horses, sheep and cows run away.</ta>
            <ta e="T892" id="Seg_14378" s="T888">She began, with milk…</ta>
            <ta e="T897" id="Seg_14379" s="T893">to pour milk on them.</ta>
            <ta e="T901" id="Seg_14380" s="T897">"Sheep, you become goat!</ta>
            <ta e="T906" id="Seg_14381" s="T901">Cows, you become moose!</ta>
            <ta e="T910" id="Seg_14382" s="T906">And you, horses…"</ta>
            <ta e="T911" id="Seg_14383" s="T910">I forgot.</ta>
            <ta e="T918" id="Seg_14384" s="T912">"Horses, she said, let them become deer!"</ta>
            <ta e="T927" id="Seg_14385" s="T918">Then the woman and the man left, they have nothing again.</ta>
            <ta e="T1071" id="Seg_14386" s="T927">They left.</ta>
            <ta e="T935" id="Seg_14387" s="T1071">(There) lived a man, he had three daughters.</ta>
            <ta e="T940" id="Seg_14388" s="T935">He was called to come to the Russian Village.</ta>
            <ta e="T945" id="Seg_14389" s="T940">The older daughter [says]: "I will go!"</ta>
            <ta e="T948" id="Seg_14390" s="T945">She cut off her hair.</ta>
            <ta e="T950" id="Seg_14391" s="T948">She put on trousers.</ta>
            <ta e="T952" id="Seg_14392" s="T950">She put on a shirt.</ta>
            <ta e="T955" id="Seg_14393" s="T952">She put on a hat, went.</ta>
            <ta e="T959" id="Seg_14394" s="T955">Her father turned himself into a bear.</ta>
            <ta e="T962" id="Seg_14395" s="T959">And comes to her.</ta>
            <ta e="T968" id="Seg_14396" s="T962">She got scared and ran home.</ta>
            <ta e="T973" id="Seg_14397" s="T968">He came: "Why didn't you go?"</ta>
            <ta e="T980" id="Seg_14398" s="T973">"There is a bear, I got scared and came home."</ta>
            <ta e="T985" id="Seg_14399" s="T980">Then another daughter [says]: "I will go!"</ta>
            <ta e="T987" id="Seg_14400" s="T985">"Well, go!"</ta>
            <ta e="T991" id="Seg_14401" s="T987">She put on trousers, she put on a shirt.</ta>
            <ta e="T993" id="Seg_14402" s="T991">She cut off her hair.</ta>
            <ta e="T996" id="Seg_14403" s="T993">She put on her hat, went.</ta>
            <ta e="T1001" id="Seg_14404" s="T996">Then her father turned into a bear, went.</ta>
            <ta e="T1006" id="Seg_14405" s="T1001">She got scared, came home.</ta>
            <ta e="T1008" id="Seg_14406" s="T1006">"Why did you come?"</ta>
            <ta e="T1016" id="Seg_14407" s="T1008">"Well, there was a bear, I got scared, ran home."</ta>
            <ta e="T1017" id="Seg_14408" s="T1016">Enough!</ta>
            <ta e="T1023" id="Seg_14409" s="T1018">Then the youngest daughter [says]: "I will go!"</ta>
            <ta e="T1025" id="Seg_14410" s="T1023">"Well, go!"</ta>
            <ta e="T1030" id="Seg_14411" s="T1025">She also cut off her hair like this.</ta>
            <ta e="T1032" id="Seg_14412" s="T1030">She put on trousers.</ta>
            <ta e="T1034" id="Seg_14413" s="T1032">She put on a shirt.</ta>
            <ta e="T1038" id="Seg_14414" s="T1034">She put on a hat, [and] went.</ta>
            <ta e="T1040" id="Seg_14415" s="T1038">She’s going, she’s going.</ta>
            <ta e="T1042" id="Seg_14416" s="T1040">A bear comes.</ta>
            <ta e="T1051" id="Seg_14417" s="T1042">She […] him with an arrow, into his eye, his eye…</ta>
            <ta e="T1059" id="Seg_14418" s="T1051">She shot him in the eye with an arrow, he lost his eye.</ta>
            <ta e="T1062" id="Seg_14419" s="T1059">Then she went, went.</ta>
            <ta e="T1069" id="Seg_14420" s="T1062">She comes, [there] live a woman and the woman’s son.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PKZ">
            <ta e="T4" id="Seg_14421" s="T0">Wann kamen wir nach Abalakovo.</ta>
            <ta e="T8" id="Seg_14422" s="T4">Leute waren gut zu uns.</ta>
            <ta e="T10" id="Seg_14423" s="T8">Wir verbrachten eine gute Nacht.</ta>
            <ta e="T13" id="Seg_14424" s="T10">Dann liefen wir einiger Zeit.</ta>
            <ta e="T18" id="Seg_14425" s="T13">Wir trafen (?) einige Männer.</ta>
            <ta e="T24" id="Seg_14426" s="T18">Wir sprachen, sie wußten nichts, Kamassier.</ta>
            <ta e="T26" id="Seg_14427" s="T24">Kamassische Leute.</ta>
            <ta e="T29" id="Seg_14428" s="T26">Wie wohnen sie?</ta>
            <ta e="T34" id="Seg_14429" s="T29">Wir wollten gerade wieder nach Hause gehen.</ta>
            <ta e="T42" id="Seg_14430" s="T36">Ein Mädchen kam (her?), sie suchte (nach?) etwas Milch.</ta>
            <ta e="T48" id="Seg_14431" s="T42">Und sie kam zu einer Frau.</ta>
            <ta e="T55" id="Seg_14432" s="T48">Diese Frau ist Kamassierin, sie sagt:</ta>
            <ta e="T57" id="Seg_14433" s="T55">Gesicht ist kadəl.</ta>
            <ta e="T64" id="Seg_14434" s="T57">Haar ist eʔbdə.</ta>
            <ta e="T67" id="Seg_14435" s="T64">Dieses [Mädchen] kam zurück.</ta>
            <ta e="T71" id="Seg_14436" s="T67">Sein Herz ist sehr gut [=sie war sehr froh?].</ta>
            <ta e="T77" id="Seg_14437" s="T71">Es sagt: “Hier ist eine Frau.</ta>
            <ta e="T80" id="Seg_14438" s="T77">Sie kann Kamassisch sprechen.</ta>
            <ta e="T90" id="Seg_14439" s="T81">Sie hatte mir gesagt: Mädchen ist koʔbdo, und Junge ist nʼi.</ta>
            <ta e="T92" id="Seg_14440" s="T90">Dann…</ta>
            <ta e="T96" id="Seg_14441" s="T93">Dann [entschieden] wir uns zu bleiben.</ta>
            <ta e="T101" id="Seg_14442" s="T96">Wir gingen zu dieser Frau um [mit ihr] zu sprechen.</ta>
            <ta e="T105" id="Seg_14443" s="T101">Wir sprachen dreißig Tage lang.</ta>
            <ta e="T107" id="Seg_14444" s="T105">[Über] allerlei Dinge.</ta>
            <ta e="T111" id="Seg_14445" s="T107">Sie erzählte in ihrer Sprache.</ta>
            <ta e="T116" id="Seg_14446" s="T111">Sie sagte so: “Meine Tante starb.</ta>
            <ta e="T120" id="Seg_14447" s="T116">Zwanzig Jahre vergingen.</ta>
            <ta e="T123" id="Seg_14448" s="T120">I spreche mit niemanden.</ta>
            <ta e="T129" id="Seg_14449" s="T124">Sie ist nun sieb[zig] Jahre alt.</ta>
            <ta e="T133" id="Seg_14450" s="T129">Sie ist eine sehr gute Frau.</ta>
            <ta e="T136" id="Seg_14451" s="T133">Sie hat allerlei Dinge.</ta>
            <ta e="T145" id="Seg_14452" s="T136">Wenn sie zu Essen gibt, und man isst nicht, wird sie wütend.</ta>
            <ta e="T156" id="Seg_14453" s="T147">Dann arbeitet sie immer viel zu Hause, [wie] das ist nicht [allzu] schlecht für ihre Beine. [?]</ta>
            <ta e="T168" id="Seg_14454" s="T157">Nach Aginskoye, fünfundzwanzig [Kilometer], sie geht zu Fuß [dorthin].</ta>
            <ta e="T175" id="Seg_14455" s="T168">Autos fahren vorbei, sie steigt nicht ein.</ta>
            <ta e="T180" id="Seg_14456" s="T175">Sie wird zu Fuß gehen.</ta>
            <ta e="T186" id="Seg_14457" s="T182">“Ich gehe lieber zu Fuß.</ta>
            <ta e="T188" id="Seg_14458" s="T186">Die Straße ist gerade.</ta>
            <ta e="T196" id="Seg_14459" s="T188">Mit dem Auto ist es sehr lang, und es rüttelt.“</ta>
            <ta e="T200" id="Seg_14460" s="T197">Was für ein Los haben sie, Kamassier.</ta>
            <ta e="T206" id="Seg_14461" s="T200">Kann jemand noch sprechen, oder nicht. [?]</ta>
            <ta e="T212" id="Seg_14462" s="T207">Draußen, in Abalakovo, kann man Kinder finden.</ta>
            <ta e="T216" id="Seg_14463" s="T212">Der Kopf ist weiß, die Augen sind schwarz.</ta>
            <ta e="T221" id="Seg_14464" s="T216">[Sie] haben nicht nur russisches Blut.</ta>
            <ta e="T225" id="Seg_14465" s="T221">Sie haben auch kamaassisches Blut. [?]</ta>
            <ta e="T230" id="Seg_14466" s="T226">Einige sind groß.</ta>
            <ta e="T235" id="Seg_14467" s="T230">Sie gingen hier und dort zu Arbeit, zum Beispiel Pronya.</ta>
            <ta e="T236" id="Seg_14468" s="T235">Andzhigatov.</ta>
            <ta e="T238" id="Seg_14469" s="T236">Vorsitzender des Dorfrates.</ta>
            <ta e="T243" id="Seg_14470" s="T238">Und er kennt seine eigene Sprachen nicht.</ta>
            <ta e="T251" id="Seg_14471" s="T244">Nur eine Frau kennt diese Kamassische Sprache.</ta>
            <ta e="T258" id="Seg_14472" s="T251">Und sonst jemanden kennt diese Sprache nicht.</ta>
            <ta e="T262" id="Seg_14473" s="T259">Du lügtest mich an.</ta>
            <ta e="T265" id="Seg_14474" s="T262">Du betrogst mich.</ta>
            <ta e="T276" id="Seg_14475" s="T265">Dann ich [= du?] versteckt mich, und ich suchte dich und konnte [dich] nicht finden. [?]</ta>
            <ta e="T280" id="Seg_14476" s="T276">Ein schlechter (“nicht gutter”) Mann lebte.</ta>
            <ta e="T283" id="Seg_14477" s="T280">Er wurde Tartshabardzha genannt.</ta>
            <ta e="T287" id="Seg_14478" s="T283">Er fing eine Stute.</ta>
            <ta e="T289" id="Seg_14479" s="T287">Ihr Fuß schmerzt.</ta>
            <ta e="T292" id="Seg_14480" s="T289">Sie kann nicht gehen.</ta>
            <ta e="T295" id="Seg_14481" s="T292">Dann ging er, ging.</ta>
            <ta e="T300" id="Seg_14482" s="T295">Ein Bär kommt. „Nimm mich mit!</ta>
            <ta e="T307" id="Seg_14483" s="T300">Bloß steck mich in den schwarzen Beutel.“</ta>
            <ta e="T312" id="Seg_14484" s="T307">Er setzte ihn und geht weiter.</ta>
            <ta e="T316" id="Seg_14485" s="T312">Er ging, er ging, dann…</ta>
            <ta e="T321" id="Seg_14486" s="T316">Dann kam ein großer Hund (Wolf) aus dem Wald heraus.</ta>
            <ta e="T323" id="Seg_14487" s="T321">„Nimm mich!</ta>
            <ta e="T328" id="Seg_14488" s="T323">Steck mich einfach in den schwarzen Sack.“</ta>
            <ta e="T330" id="Seg_14489" s="T328">Er setzte ihn [dorthin].</ta>
            <ta e="T336" id="Seg_14490" s="T330">Er ging wieder, er geht und geht.</ta>
            <ta e="T339" id="Seg_14491" s="T336">Ein Fuchs kommt.</ta>
            <ta e="T341" id="Seg_14492" s="T339">„Nimm mich!“</ta>
            <ta e="T344" id="Seg_14493" s="T341">Er nahm ihn.</ta>
            <ta e="T350" id="Seg_14494" s="T344">(Er) setzte (ihn) in den schwarzen Sack, dann ging er wieder.</ta>
            <ta e="T354" id="Seg_14495" s="T350">Ein See kommt: „Nimm mich!“</ta>
            <ta e="T359" id="Seg_14496" s="T354">Dann goss er das Wasser dorthin</ta>
            <ta e="T360" id="Seg_14497" s="T359">Er kam.</ta>
            <ta e="T368" id="Seg_14498" s="T360">Häuser stehen da, und ein sehr großes Wasser [= ein See].</ta>
            <ta e="T371" id="Seg_14499" s="T368">Er band die Stute an.</ta>
            <ta e="T374" id="Seg_14500" s="T371">Er machte Feuer, sitzt</ta>
            <ta e="T380" id="Seg_14501" s="T374">Der Herrscher sah ihn: „Wer sitzt da?</ta>
            <ta e="T384" id="Seg_14502" s="T380">Zertampelt mein Gras.</ta>
            <ta e="T386" id="Seg_14503" s="T384">Seine Stute frisst [das Gras].</ta>
            <ta e="T393" id="Seg_14504" s="T386">Er wird eine kleine Weile da sitzen, dann wird er wieder zertrampeln.</ta>
            <ta e="T394" id="Seg_14505" s="T393">Gehe!“</ta>
            <ta e="T396" id="Seg_14506" s="T394">Er schickte seinen Jungen/Sohn.</ta>
            <ta e="T399" id="Seg_14507" s="T396">„Was will er?“</ta>
            <ta e="T406" id="Seg_14508" s="T399">Er kam, fragt: „Was willst du?“</ta>
            <ta e="T410" id="Seg_14509" s="T406">Ich werde die Tochter des [Dorf-] Herrschers nehmen [heiraten].“</ta>
            <ta e="T415" id="Seg_14510" s="T411">Dieser Junge kam zurück und sagt:</ta>
            <ta e="T422" id="Seg_14511" s="T415">„Er sagt: Ich werde die Tochter des Herrschers nehmen!</ta>
            <ta e="T424" id="Seg_14512" s="T422">Lasse sie gehen.</ta>
            <ta e="T428" id="Seg_14513" s="T424">Sonst wird es nicht gut sein!“</ta>
            <ta e="T431" id="Seg_14514" s="T428">„Ich gebe [sie] nicht!“</ta>
            <ta e="T436" id="Seg_14515" s="T431">Er [der Junge] kam, sagte so:</ta>
            <ta e="T440" id="Seg_14516" s="T436">„Der Herrscher gibt seine Tochter nicht.“</ta>
            <ta e="T447" id="Seg_14517" s="T440">„Wenn er sie an Gutes nicht gibt, gibt er sie nicht an Schlechtes.</ta>
            <ta e="T451" id="Seg_14518" s="T448">„Lasse die wilde Pferde los.</ta>
            <ta e="T458" id="Seg_14519" s="T451">Sie werden seine Stute auseinander reißen, dann wird er gehen.“</ta>
            <ta e="T464" id="Seg_14520" s="T458">Dann ließ er den Bär aus dem Sack.</ta>
            <ta e="T469" id="Seg_14521" s="T464">Dieser Bär verjagte die Pferde.</ta>
            <ta e="T475" id="Seg_14522" s="T469">Dann sagte [der Herrscher]: „Lasse die Kühe los, die starke Kühen</ta>
            <ta e="T480" id="Seg_14523" s="T475">Sie werden sein Pferd mit ihren Hörnern töten.“</ta>
            <ta e="T493" id="Seg_14524" s="T480">Er ließ den großen Hund [Wolf] aus dem Sack, er ging, verjagte sie.</ta>
            <ta e="T497" id="Seg_14525" s="T493">Dann sagt er: „Lasse…</ta>
            <ta e="T504" id="Seg_14526" s="T498">Dann der Häuptling: „Lasse die Hunde los!</ta>
            <ta e="T508" id="Seg_14527" s="T504">Lasse die Hunde sie auseinanderreißen.</ta>
            <ta e="T511" id="Seg_14528" s="T508">Er ließ ihre Hunde los.</ta>
            <ta e="T518" id="Seg_14529" s="T511">Aber er ließ den Fuchs los, die Hunde liefen [nach] dem Fuch.</ta>
            <ta e="T527" id="Seg_14530" s="T518">Dann rief der Herrscher Leute zusammen: „Lass uns gehen, lass uns ihn schnappen!“</ta>
            <ta e="T534" id="Seg_14531" s="T527">Leute kamen, er schüttete das Wasser [aus] seinem Beutel.</ta>
            <ta e="T541" id="Seg_14532" s="T534">Leute rannten von diesem Wasser weg.</ta>
            <ta e="T546" id="Seg_14533" s="T542">Dann ruft dieser Herrscher:</ta>
            <ta e="T550" id="Seg_14534" s="T546">„Die Hälfte von was ich habe, sagte er.</ta>
            <ta e="T554" id="Seg_14535" s="T550">Ich habe Kühe, Pferde,</ta>
            <ta e="T559" id="Seg_14536" s="T554">Viel Geld.</ta>
            <ta e="T566" id="Seg_14537" s="T559">Er sagt: Ich brauch nichts, nur die Tochter nehmen.</ta>
            <ta e="T574" id="Seg_14538" s="T567">Dann holte er das Wasser wieder in den schwarzen Beutel und…</ta>
            <ta e="T585" id="Seg_14539" s="T575">Dann gab er sein Tochter, er setzte [sie] auf die Stute und ging nach Hause.</ta>
            <ta e="T589" id="Seg_14540" s="T585">Es lebten zwei Brüder.</ta>
            <ta e="T592" id="Seg_14541" s="T589">Sie hatten Frauen.</ta>
            <ta e="T596" id="Seg_14542" s="T592">Sie versammelten [= bereiteten sich vor], gingen in die Taiga.</ta>
            <ta e="T600" id="Seg_14543" s="T596">Und die Frauen (waren) [in] das Zelt / zu Hause. </ta>
            <ta e="T605" id="Seg_14544" s="T600">Am Abend setzten sie sich zum Essen hin.</ta>
            <ta e="T612" id="Seg_14545" s="T605">Dann sahen sie im Kessel, dass ein Mann sitzt.</ta>
            <ta e="T621" id="Seg_14546" s="T612">Sie sammelten Äste und legten sie in den Kessel [= in die Feuerstelle].</ta>
            <ta e="T629" id="Seg_14547" s="T621">Aber dieses Reisig war nass.</ta>
            <ta e="T632" id="Seg_14548" s="T629">Dann kam er.</ta>
            <ta e="T637" id="Seg_14549" s="T632">Dieser Mann konnte kein Feuer machen.</ta>
            <ta e="T640" id="Seg_14550" s="T637">Er tötete eine Frau.</ta>
            <ta e="T650" id="Seg_14551" s="T640">Dann sagt er wieder zu der Frau: „Warum sitzt du, mach Feuer!“</ta>
            <ta e="T654" id="Seg_14552" s="T650">Sie fing an Feuer zu zünden.</ta>
            <ta e="T659" id="Seg_14553" s="T654">Er schlug ihr mit einer Axt den Kopf ab.</ta>
            <ta e="T662" id="Seg_14554" s="T659">Dann tötete er ihre Kinder.</ta>
            <ta e="T669" id="Seg_14555" s="T662">Dann ging er [= diese Männer?] zurück zu ihnen in ihr Zelt.</ta>
            <ta e="T673" id="Seg_14556" s="T669">Ihre Frauen sind nicht da.</ta>
            <ta e="T678" id="Seg_14557" s="T673">Jemand hat ihre Frauen und Kinder getötet.</ta>
            <ta e="T682" id="Seg_14558" s="T678">[Es lebete ein Mann mit seiner Frau.]</ta>
            <ta e="T688" id="Seg_14559" s="T682">Sie hatten nichts, nur eine Kuh.</ta>
            <ta e="T692" id="Seg_14560" s="T688">Sie hatten nichts zu essen.</ta>
            <ta e="T695" id="Seg_14561" s="T692">Dann tötete [der Mann] seine Kuh.</ta>
            <ta e="T703" id="Seg_14562" s="T695">Er verjagte seine Frau: „Geh weg, sonst habe ich für mich zu wenig.“</ta>
            <ta e="T707" id="Seg_14563" s="T703">Die Frau ging, ging.</ta>
            <ta e="T710" id="Seg_14564" s="T707">Sie sah ein anderes Zelt.</ta>
            <ta e="T712" id="Seg_14565" s="T710">Sie kam ins Zelt.</ta>
            <ta e="T717" id="Seg_14566" s="T712">Es liegt Fett, Fleisch liegt.</ta>
            <ta e="T721" id="Seg_14567" s="T717">Sie kochte es, aß alles.</ta>
            <ta e="T725" id="Seg_14568" s="T721">Sie hört: jemand kommt.</ta>
            <ta e="T731" id="Seg_14569" s="T725">[Jene Person] brachte viele Schafe, Kühe, Pferde.</ta>
            <ta e="T734" id="Seg_14570" s="T731">Dann kam sie ins Zelt.</ta>
            <ta e="T738" id="Seg_14571" s="T734">Sie sagt: „Tür, öffne dich!“</ta>
            <ta e="T743" id="Seg_14572" s="T738">Die Tür offnete sich, sie kam hinein, schaut:</ta>
            <ta e="T751" id="Seg_14573" s="T743">„Jemand war [hier], es ist kein Fett, es ist kein Fleisch, es ist kein Brot.</ta>
            <ta e="T754" id="Seg_14574" s="T751">Wer war hier?</ta>
            <ta e="T758" id="Seg_14575" s="T754">Wenn [du] ein Mann [bist] – wirst du mein Mann.</ta>
            <ta e="T762" id="Seg_14576" s="T758">Wenn [du] eine Frau [bist], wirst du mein Gefährtin.“</ta>
            <ta e="T767" id="Seg_14577" s="T762">Dann stand die Frau auf, kam (heraus).</ta>
            <ta e="T770" id="Seg_14578" s="T767">Sie setzte sich nieder.</ta>
            <ta e="T772" id="Seg_14579" s="T770">Sie saß und aß.</ta>
            <ta e="T782" id="Seg_14580" s="T772">Sie fragt: „Warum hast du nicht Hand, und Füße, wie läufst du?“</ta>
            <ta e="T785" id="Seg_14581" s="T782">„So gehe ich.“</ta>
            <ta e="T788" id="Seg_14582" s="T785">„Wirst du in einen Graben springen?“</ta>
            <ta e="T789" id="Seg_14583" s="T788">„Ich springe.“</ta>
            <ta e="T791" id="Seg_14584" s="T789">Dann gingen sie.</ta>
            <ta e="T799" id="Seg_14585" s="T791">Sie sprang, sie schlug ihr mit einer Axt den Kopf ab.</ta>
            <ta e="T803" id="Seg_14586" s="T799">Dann nahm sie das Hirn (?)</ta>
            <ta e="T807" id="Seg_14587" s="T803">Sie nahm ihr Fett, [ihr] Fleisch.</ta>
            <ta e="T813" id="Seg_14588" s="T807">Steckt [sie] in die Blase, dann geht sie zu ihrem Mann zurück.</ta>
            <ta e="T825" id="Seg_14589" s="T813">Sie kletterte auf das Zelt, aber der Mann, mit Kohle, seine Hand…</ta>
            <ta e="T830" id="Seg_14590" s="T825">… sein Hand mit Kohle [geschwärzt]:</ta>
            <ta e="T836" id="Seg_14591" s="T830">„Ich werde [heute] das Fleisch essen, ich werde [morgen] das Fleisch essen. „</ta>
            <ta e="T840" id="Seg_14592" s="T836">Sie warf ihm [etwas Fleisch].</ta>
            <ta e="T846" id="Seg_14593" s="T840">Er schnappte [es]: ‚Schau, was Gott mir gegeben hat!“</ta>
            <ta e="T850" id="Seg_14594" s="T846">„Natürlich, Gott gibt dir!</ta>
            <ta e="T857" id="Seg_14595" s="T850">Gott gibt dir einen Stein, ich bin es, der dir das gab!“</ta>
            <ta e="T861" id="Seg_14596" s="T857">Dann aß er, sie gingen [zu jenem Zelt].</ta>
            <ta e="T867" id="Seg_14597" s="T861">Es (waren) Pferde, Kühe, Schafe, sie kamen dorthin.</ta>
            <ta e="T871" id="Seg_14598" s="T867">Er sagt: „Ich will furzen.“</ta>
            <ta e="T876" id="Seg_14599" s="T871">„Furze nicht, sonst laufen sie weg!“</ta>
            <ta e="T888" id="Seg_14600" s="T876">Er furzte trotzdem, und alle Pferde, Schafe und Kühe liefen weg.</ta>
            <ta e="T892" id="Seg_14601" s="T888">Sie fingen an, mit Milch…</ta>
            <ta e="T897" id="Seg_14602" s="T893">auf sie mit Milch zu schütten.</ta>
            <ta e="T901" id="Seg_14603" s="T897">„Schaf, du werdest Ziege!“</ta>
            <ta e="T906" id="Seg_14604" s="T901">Kühe: ihr werdet Elche!</ta>
            <ta e="T910" id="Seg_14605" s="T906">Und ihr, Pferde…“</ta>
            <ta e="T911" id="Seg_14606" s="T910">Ich vergesse.</ta>
            <ta e="T918" id="Seg_14607" s="T912">„Pferde, sagte sie, lass sie Rehe werden!“</ta>
            <ta e="T927" id="Seg_14608" s="T918">Dann gingen die Frau und der Mann, sie hatten wieder nichts.</ta>
            <ta e="T1071" id="Seg_14609" s="T927">Sie gingen.</ta>
            <ta e="T935" id="Seg_14610" s="T1071">(Es) lebte ein Mann, er hatte drei Töchter.</ta>
            <ta e="T940" id="Seg_14611" s="T935">Er wurde gerufen, ins Russische Haus zu kommen.</ta>
            <ta e="T945" id="Seg_14612" s="T940">Die älter Tochter [sagt]: „Ich will gehen!“</ta>
            <ta e="T948" id="Seg_14613" s="T945">Sie schnitt sich die Haare ab.</ta>
            <ta e="T950" id="Seg_14614" s="T948">Sie zog sich eine Hose an.</ta>
            <ta e="T952" id="Seg_14615" s="T950">Sie zog sich ein Hemd an.</ta>
            <ta e="T955" id="Seg_14616" s="T952">Sie setzte einen Hut auf, ging.</ta>
            <ta e="T959" id="Seg_14617" s="T955">Ihr Vater verwandelte sich in einen Bär.</ta>
            <ta e="T962" id="Seg_14618" s="T959">Und kam zu ihr.</ta>
            <ta e="T968" id="Seg_14619" s="T962">Sie bekam Angst und lief nach Hause.</ta>
            <ta e="T973" id="Seg_14620" s="T968">Er kam: „Warum bist du nicht gegangen?“</ta>
            <ta e="T980" id="Seg_14621" s="T973">„Es war ein Bär, ich bekam Angst und kam nach Hause.“</ta>
            <ta e="T985" id="Seg_14622" s="T980">Dann [sagt ]eine andere Tochter: „Ich werde gehen!“</ta>
            <ta e="T987" id="Seg_14623" s="T985">„Also, gehe!“</ta>
            <ta e="T991" id="Seg_14624" s="T987">Sie zog sich eine Hose an, sie zog sich ein Hemd an.</ta>
            <ta e="T993" id="Seg_14625" s="T991">Sie schnitt ihre Haare ab.</ta>
            <ta e="T996" id="Seg_14626" s="T993">Sie setzte einen Hut auf, ging.</ta>
            <ta e="T1001" id="Seg_14627" s="T996">Dann verwandelte sich ihr Vater in einen Bär, ging.</ta>
            <ta e="T1006" id="Seg_14628" s="T1001">Sie bekam Angst, kam nach Hause.</ta>
            <ta e="T1008" id="Seg_14629" s="T1006">„Warum bist du gekommen?“</ta>
            <ta e="T1016" id="Seg_14630" s="T1008">„Also, es war ein Bär, ich bekam Angst, lief nach Hause.“</ta>
            <ta e="T1017" id="Seg_14631" s="T1016">Genug!</ta>
            <ta e="T1023" id="Seg_14632" s="T1018">Dann [sagt] die jüngste Tochter: „Ich werde gehen!“</ta>
            <ta e="T1025" id="Seg_14633" s="T1023">„Also, gehe!“</ta>
            <ta e="T1030" id="Seg_14634" s="T1025">Sie schnitt sich auch so die Haare ab.</ta>
            <ta e="T1032" id="Seg_14635" s="T1030">Sie zog sich eine Hose an.</ta>
            <ta e="T1034" id="Seg_14636" s="T1032">Sie zog sich ein Hend an.</ta>
            <ta e="T1038" id="Seg_14637" s="T1034">Und sie setzte einen Hut auf, [und] ging.</ta>
            <ta e="T1040" id="Seg_14638" s="T1038">Sie geht, sie geht.</ta>
            <ta e="T1042" id="Seg_14639" s="T1040">Ein Bär kommt.</ta>
            <ta e="T1051" id="Seg_14640" s="T1042">Sie […] ihn mit einem Pfeil, in sein Auge, sein Auge…</ta>
            <ta e="T1059" id="Seg_14641" s="T1051">Sie schoß ihm mit einem Pfeil ins Auge, er verlor sein Auge.</ta>
            <ta e="T1062" id="Seg_14642" s="T1059">Dann ging sie, ging.</ta>
            <ta e="T1069" id="Seg_14643" s="T1062">Sie kommt, [dort] wohnen eine Frau und der Sohn der Frau.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-PKZ">
            <ta e="T4" id="Seg_14644" s="T0">[GVY] Klavdiya Plotnikova relates the story about the expedition and herself in the third person.</ta>
            <ta e="T13" id="Seg_14645" s="T10">Ходили по домам?</ta>
            <ta e="T105" id="Seg_14646" s="T101">[GVY:] or thirteen?</ta>
            <ta e="T116" id="Seg_14647" s="T111">[GVY:] kʼotkam</ta>
            <ta e="T123" id="Seg_14648" s="T120">[GVY:] -ziz- a variant of -ziʔ in a non-final position or just a mispronounciation? </ta>
            <ta e="T200" id="Seg_14649" s="T197">[GVY:] nuzaŋgəŋ</ta>
            <ta e="T212" id="Seg_14650" s="T207">[GVY:] nʼiʔinen? 'Outside' because they are sitting in a house?</ta>
            <ta e="T276" id="Seg_14651" s="T265">[GVY:] šaʔlambiam = šaʔlambial [-2SG]?</ta>
            <ta e="T280" id="Seg_14652" s="T276">[KlT:] Text 2 in Donner's collection.</ta>
            <ta e="T287" id="Seg_14653" s="T283">[GVY:] šojmu?</ta>
            <ta e="T307" id="Seg_14654" s="T300">[GVY:] Sack is always bura.</ta>
            <ta e="T424" id="Seg_14655" s="T422">[GVY:] kangəjəʔ?</ta>
            <ta e="T440" id="Seg_14656" s="T436">[GVY:] Or "ej mĭlie tĭ"</ta>
            <ta e="T451" id="Seg_14657" s="T448">[GVY:] gikej</ta>
            <ta e="T511" id="Seg_14658" s="T508">[GVY:] meŋdəŋ?</ta>
            <ta e="T518" id="Seg_14659" s="T511">[GVY:] lʼisagə (n-) nuʔməluʔpiʔi?</ta>
            <ta e="T550" id="Seg_14660" s="T546">[GVY:] The next three sentences are not fully clear. The ruler proposes TB to take a half of everything he has: cows, horses, dogs and money, but TB says that he only wants the girl.</ta>
            <ta e="T589" id="Seg_14661" s="T585">[KlT:] Text 3 from Donner's collection</ta>
            <ta e="T612" id="Seg_14662" s="T605">[GVY:] The man was on the top of the tent, on the smoke hole, and they saw his reflection in the kettle.</ta>
            <ta e="T621" id="Seg_14663" s="T612">[GVY:] moːʔi = muʔi 'branch-PL'?</ta>
            <ta e="T682" id="Seg_14664" s="T678">[KlT:] Text 9 from Donner's collection.</ta>
            <ta e="T725" id="Seg_14665" s="T721">[GVY:] šinditə</ta>
            <ta e="T743" id="Seg_14666" s="T738">[GVY:] măndərla = măndərlia [-PRS]?</ta>
            <ta e="T803" id="Seg_14667" s="T799">[GVY:] Cf. D 36a kʼama?</ta>
            <ta e="T825" id="Seg_14668" s="T813">[GVY:] The old man draws lines on the meat with his finger, smudged with coal.</ta>
            <ta e="T935" id="Seg_14669" s="T1071">[KlT:] Text 10 from Donner's collection (beginning).</ta>
            <ta e="T940" id="Seg_14670" s="T935">[GVY:] Here Kazan tura does not probably mean Aginskoye.</ta>
            <ta e="T973" id="Seg_14671" s="T968">[GVY:] kambiam = kambial [-2SG]</ta>
            <ta e="T993" id="Seg_14672" s="T991">[GVY:] bĭʔpi?</ta>
            <ta e="T1030" id="Seg_14673" s="T1025">[GVY:] bĭtluʔpi?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T1074" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T1072" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T1073" />
            <conversion-tli id="T1071" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T1075" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T979" />
            <conversion-tli id="T980" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T985" />
            <conversion-tli id="T986" />
            <conversion-tli id="T987" />
            <conversion-tli id="T988" />
            <conversion-tli id="T989" />
            <conversion-tli id="T990" />
            <conversion-tli id="T991" />
            <conversion-tli id="T992" />
            <conversion-tli id="T993" />
            <conversion-tli id="T994" />
            <conversion-tli id="T995" />
            <conversion-tli id="T996" />
            <conversion-tli id="T997" />
            <conversion-tli id="T998" />
            <conversion-tli id="T999" />
            <conversion-tli id="T1000" />
            <conversion-tli id="T1001" />
            <conversion-tli id="T1002" />
            <conversion-tli id="T1003" />
            <conversion-tli id="T1004" />
            <conversion-tli id="T1005" />
            <conversion-tli id="T1006" />
            <conversion-tli id="T1007" />
            <conversion-tli id="T1008" />
            <conversion-tli id="T1009" />
            <conversion-tli id="T1010" />
            <conversion-tli id="T1011" />
            <conversion-tli id="T1012" />
            <conversion-tli id="T1013" />
            <conversion-tli id="T1014" />
            <conversion-tli id="T1015" />
            <conversion-tli id="T1016" />
            <conversion-tli id="T1017" />
            <conversion-tli id="T1018" />
            <conversion-tli id="T1019" />
            <conversion-tli id="T1020" />
            <conversion-tli id="T1021" />
            <conversion-tli id="T1022" />
            <conversion-tli id="T1023" />
            <conversion-tli id="T1024" />
            <conversion-tli id="T1025" />
            <conversion-tli id="T1026" />
            <conversion-tli id="T1027" />
            <conversion-tli id="T1028" />
            <conversion-tli id="T1029" />
            <conversion-tli id="T1030" />
            <conversion-tli id="T1031" />
            <conversion-tli id="T1032" />
            <conversion-tli id="T1033" />
            <conversion-tli id="T1034" />
            <conversion-tli id="T1035" />
            <conversion-tli id="T1036" />
            <conversion-tli id="T1037" />
            <conversion-tli id="T1038" />
            <conversion-tli id="T1039" />
            <conversion-tli id="T1040" />
            <conversion-tli id="T1041" />
            <conversion-tli id="T1042" />
            <conversion-tli id="T1043" />
            <conversion-tli id="T1044" />
            <conversion-tli id="T1045" />
            <conversion-tli id="T1046" />
            <conversion-tli id="T1047" />
            <conversion-tli id="T1048" />
            <conversion-tli id="T1049" />
            <conversion-tli id="T1050" />
            <conversion-tli id="T1051" />
            <conversion-tli id="T1052" />
            <conversion-tli id="T1053" />
            <conversion-tli id="T1054" />
            <conversion-tli id="T1055" />
            <conversion-tli id="T1056" />
            <conversion-tli id="T1057" />
            <conversion-tli id="T1058" />
            <conversion-tli id="T1059" />
            <conversion-tli id="T1060" />
            <conversion-tli id="T1061" />
            <conversion-tli id="T1062" />
            <conversion-tli id="T1063" />
            <conversion-tli id="T1064" />
            <conversion-tli id="T1065" />
            <conversion-tli id="T1066" />
            <conversion-tli id="T1067" />
            <conversion-tli id="T1068" />
            <conversion-tli id="T1069" />
            <conversion-tli id="T1070" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-PKZ"
                          name="ref"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PKZ"
                          name="ts"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PKZ"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PKZ"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PKZ"
                          name="mb"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PKZ"
                          name="mp"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PKZ"
                          name="ge"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PKZ"
                          name="gr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PKZ"
                          name="mc"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PKZ"
                          name="ps"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PKZ"
                          name="SeR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PKZ"
                          name="SyF"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PKZ"
                          name="IST"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PKZ"
                          name="BOR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PKZ"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PKZ"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PKZ"
                          name="CS"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PKZ"
                          name="fr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PKZ"
                          name="fe"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PKZ"
                          name="fg"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PKZ"
                          name="nt"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
